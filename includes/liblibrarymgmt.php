<?php
// Using:    

/**
 * ************************************************************
 *
 * 20200505 (Cameron)
 * - add function getOverdueInfoByBorrowLogID() [case #Y173908]
 * 
 * 20200504 (Cameron)
 * - add function getOverdueBookInfo() [case #Y173908]
 *  
 * 20200428 (Cameron)
 * - add ReturnLostBook in GET_MODULE_OBJ_ARR_ADMIN() [case #Y173908]
 * - fix: don't show overdue record for late return in returnSQLforLostReport()
 * 
 * 20200424 (Cameron)
 * - fix sql when field value is "'NOW()'" in UPDATE_BOOK_UNIQUE() [case #K179441]
 * 
 * 20200420 (Cameron)
 * - add search field bookSeries and bookSeriesno in getAdvanceSearchDiv() for book page [case #Z183643]
 *  
 * 20200415 (Henry)
 * - modified function get_fine_report() to fix the penalty display problem
 * 
 * 20190617 (Cameron)
 * - add column TotalRead (by percentage filter) in Get_Class_View(), link to view details of books that match the percentage read filter
 * 
 * 20190530 (Cameron)
 * - add function changeImageOrientation()  [case #L159650]
 * 
 * 20190327 (Cameron)
 * - change column title from paid to paid_or_waive in get_fine_report()
 * - add parameter $type to Get_Class_View()
 * 
 * 20190320 (Cameron)
 * - fix column name from fine_handle_date to fine_handle_date_or_last_updated in get_fine_report() [ej5.0.9.3.1]
 * 
 * 20190318 (Cameron)
 * - revise sql to fix bug on get_fine_report() [ej5.0.9.3.1]
 * - show indicator for book that has been returned after waive payment
 * 
 * 20190306 (Cameron)
 * - add parameter $showPercentageCol and $percentage in Get_Class_View() [case #W155835]
 * 
 * 20190304 (Cameron)
 * - add filter LIBMS_OVERDUE_LOG.RecordStatus<>'DELETE' to returnSQLforLostReport() [case X157459]
 * 
 * 20181213 (Cameron)
 * - add filter LIBMS_OVERDUE_LOG.RecordStatus<>'DELETE' to get_fine_report() [case #L154330]
 * 
 * 20181205 (Cameron)
 * - add function getLatestBorrowLogStatus(), getLatestReservationLogStatus()
 * 
 * 20181003 (Cameron)
 * - modifying get_fine_report(), add column Payment Left, show all payment history of overdue log [case #T139412]
 * 
 * 20180928 (Cameron)
 * - add filter tags to get_accession_report() [case #X138957]
 * 
 * 20180921 (Cameron)
 * - add function getBookReservedCount()
 * 
 * 20180910 (Cameron)
 * - add code to prevent UserID being changed in magicQuotes_awStripslashes()
 * 
 * 20180905 (Cameron)
 * - add function getReserveBookTabs()
 * - retrieve UserID in getNextUserOfBookReservation()
 * - add parameter $notifyNextUser to REMOVE_RESERVATION(), send email notify to next ready to pick-up user
 * 
 * 20180718 (Cameron)
 * - fix bug: include CreationDate='0000-00-00 00:00:00' for EndDate condition in GET_NOT_STOCKTAKE_BOOK_INFO() [case #N142620]
 * 
 * 20180604 (Henry)
 * - added function UPDATE_ITEM_TYPES() for UPDATE_CIRCULATION_TYPE [Case#Y139668]
 * 
 * 20180508 (Cameron)
 * - fix bug: include CreationDate='0000-00-00 00:00:00' for EndDate condition in GET_UNIQUE_BOOK_AMOUNT_BY_LOCATION_PERIOD() [case #N139075] 
 * 
 * 20180426 (Cameron)
 * - add parameter $getAccountDate to GET_STOCKTAKE_LOG_BOOK_INFO() and GET_NOT_STOCKTAKE_BOOK_INFO(), retrieve AccountDate(CreationDate) [case #E89629]
 * 
 * 20180420 (Cameron)
 * - retrieve AccountDate(CreationDate) in GET_WRITEOFF_LOG_INFO() [case #W120117]
 * 
 * 20180214 (Cameron)
 * - modify Get_Borrowed_Books_Table(), don't show checkbox if login user is not allowed to borrow and return books in class management [Case #E129637]
 *
 * 20180213 (Cameron)
 * - add function getLocationCodeFromClassManagementGroup() and isLocationCodeInClassManagementGroup() [Case #E129637]
 *
 * 20180130 (Cameron)
 * - retrieve ItemSeriesNum in GET_BORROW_LOG() [case #L130405]
 *
 * 20171025 (Henry)
 * - modified get_class_mgt_can_handle_by_user(), get_class_mgt_classes() and get_class_mgt_can_handle_item() for user in different class management group [Case#Z129324]
 *
 * 20170801 (Cameron)
 * - fix: syntax error (missing ; in $(\'input.textboxnum2\').change ) in Get_Overdue_Records_Table()
 *
 * 20170630 (Henry)
 * - bug fix for $excludeWriteOffItems at GET_STOCKTAKE_LOG() [Case#J119630]
 *
 * 20170626 (Cameron)
 * - fix: don't show deleted student record (RecordStatus=0) in get_fine_report()
 *
 * 20170614 (Cameron)
 * - fix: set default $OverdueCharge to '--' and fix LOST book overdue charge in Get_Borrowed_Books_Table() [case #T117840]
 *
 * 20170607 (Cameron)
 * - fix: allow to delete setting item if it's used in deleted book only in IsCodeUsed() [case #R118150]
 *
 * 20170606 (Cameron)
 * - break report access right "overdue report" into "overdue report" and "penalty report" in GET_MODULE_OBJ_ARR_ADMIN() [case #C117984]
 *
 * 20170522 (Cameron)
 * - cast to array when use foreach in NEW_BOOK_RECORD(), apply stripslashes for BookTag in NEW_BOOK_RECORD() for php5.4
 *
 * 20170519 (Cameron)
 * - add function newline2space to replace new line / line feed character with white space character to avoid javascript error when retrieving book tags [case #Q117253]
 * - apply newline2space to NEW_BOOK_RECORD()
 *
 * 20170428 (Cameron)
 * - replace $_SERVER["REMOTE_ADDR"] with getRemoteIpAddress() in checkCurrentIP() to cater for CentOS7
 *
 * 20170406 (Cameron)
 * - modify returnSQLforLostReport() to handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 *
 * 20170330 (Cameron)
 * - modify getAdvanceSearchDiv() to handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 *
 * 20170310 (Cameron)
 * - modify replaceSymbolByHyphen() and isIncludingSymbol(), allow these symbol for code +-.()[]{} [case#K114289]
 *
 * 20170303 (Cameron)
 * - add security setting in GET_MODULE_OBJ_ARR_ADMIN()
 * - add function checkCurrentIP()
 * - add $_SESSION['LIBMS']['admin']['check_ip_for_circulation']
 *
 * 20170120 (Cameron)
 * - add function replaceSymbolByHyphen(), isIncludingSymbol()
 *
 * 20170105 (Cameron)
 * - add function IsCodeUsed() [case#C110892]
 * - add return value in REMOVE_BOOK_CATEGORY(), REMOVE_BOOK_LANGUAGE(), REMOVE_LOCATION(), REMOVE_CIRCULATION_TYPE(), REMOVE_RESOURCES_TYPE()
 * REMOVE_RESPONSIBILITY()
 * - sync ResonsibilityCode in LIBMS_BOOK when update ResonsibilityCode in UPDATE_RESPONSIBILITY()
 *
 * 20161206 (Cameron)
 * - add parameter $PurchaseByDepartment to get_accession_report() [case#K103263]
 *
 * 20161206 (Henry)
 * - add writeoff reason at function get_accession_report() [Case#L107313]
 *
 * 20161124 (Henry)
 * - add call number field at function GET_NOT_STOCKTAKE_BOOK_INFO() [Case#L96769]
 *
 * 20161102 (Cameron)
 * - fix $changed_overdue_log sql in get_fine_report(), GROUP BY OverDueLogID not need having filter
 *
 * 20161014 (Cameron)
 * - fix bug to get lost book records in returnSQLforLostReport() [case #Q105308]
 *
 * 20160930 (Cameron)
 * - add parameter $get_borrower to GET_NOT_STOCKTAKE_BOOK_INFO() and retrieve borrower in the sql [case #N99050]
 *
 * 20160922 (Cameron)
 * - add symbol ^ to indicate the overduelog has been manually changed
 *
 * 20160920 (Cameron)
 * - add "Recommend Books" tab in get_batch_edit_tabs()
 *
 * 20160901 (Cameron)
 * - fix bug: CirculationTypeCode filter should be compared to LIBMS_BOOK_UNIQUE.CirculationTypeCode first,
 * then LIBMS_BOOK.CirculationTypeCode when run report get_accession_report(), GET_STOCKTAKE_LOG_BOOK_INFO(),
 * GET_WRITEOFF_LOG_INFO(), GET_NOT_STOCKTAKE_BOOK_INFO() [case #K102034]
 *
 * 20160804 (Cameron)
 * - fix bug: apply PHPToSQL() to Subject field in sql in get_accession_report() to support filter special characters
 *
 * 20160608 (Cameron)
 * - fix bug on error when school year start date is in the future: set default value for arguments in getCloseMessage().
 *
 * 20160506 (Cameron)
 * - add filter $BookItemStatus to GET_STOCKTAKE_LOG_BOOK_INFO, GET_WRITEOFF_LOG_INFO [Case #E95397]
 *
 * 20160505 (Cameron)
 * - add total read count in Get_Student_View [Case #F95387]
 *
 * 20160503 (Cameron)
 * - add grand total row in Get_Class_View [Case #F95387]
 *
 * 20160425 (Cameron)
 * - break admin access right from "stock-take and write-off" into "stock-take" and "write-off" rights [Case#K94933]
 * in GET_MODULE_OBJ_ARR_ADMIN()
 *
 * 20160421 (Cameron)
 * - modify REMOVE_RESERVATION() logic
 *
 * 20160418 (Cameron)
 * - fix REMOVE_RESERVATION() by using array for $AssignNextUser()
 * - fix getNextUserOfBookReservation() sql, reassign next user when delete 'Ready' record and there's at least one waiting record
 *
 * 20160418 (Henry)
 * - modified function GET_STOCKTAKE_LOG_BOOK_INFO() and GET_NOT_STOCKTAKE_BOOK_INFO() to display ListPrice and PurchasePrice
 * 20160415 (Cameron)
 * - add function getNextUserOfBookReservation()
 * - modify REMOVE_RESERVATION() to assign next user in book reservation waiting list to pick up book [Case#P94465]
 *
 * [IP 7.4.1 line break]
 *
 * 20160414 (Cameron)
 * - add function BookInLoanByBookID()
 * - add return result in REMOVE_BOOK()
 *
 * 20160406 (Cameron)
 * - add function BookInLoanByUniqueID()
 *
 * 20160304 (Cameron)
 * - modify returnSQLforLostReport, don't use pass by ref & to avoid problem when encrypted code in production
 * (case #R93539)
 *
 * 20160229 (Cameron)
 * - pass $showSenderAsAdmin=true to sendModuleMail in do_email()
 *
 * 20160226 (Cameron)
 * - fix bug on update LIBMS_BOOK_UNIQUE.RecordStatus after cancel reservation of a book in REMOVE_RESERVATION()
 *
 * 20160218 (Henry)
 * - add para "$pageFile" to function getAdvanceSearchDiv()
 *
 * 20160211 (Cameron)
 * - move lost book report sql construction to returnSQLforLostReport()
 *
 * 20160211 (Henry)
 * - add function UPDATE_UNIQUE_BOOK_ID()
 *
 * 20151029 (Cameron)
 * - add parameter $withQuote to UPDATE_BOOK() and UPDATE_BOOK_UNIQUE() -- false when called from import book
 *
 * 20151005 (Cameron)
 * - fix bug on retrieving Barcode in getAdvanceSearchDiv()
 * - use htmlspecialchars rather than double stripslashes to search fields in getAdvanceSearchDiv()
 *
 * 20150923 (Cameron)
 * - add stripslashes to UPDATE_BOOK_CATEGORY()
 *
 * 20150902 (Henry)
 * - modified GET_STOCKTAKE_LOG_BOOK_INFO() to fix bug on the writeoff page [Case#T83341]
 *
 * 20150731 (Cameron)
 * - remove restriction on BookCategoryType=1 when filter BookCategory record which Language is not set in
 * get_accession_report(), GET_WRITEOFF_LOG_INFO(), GET_STOCKTAKE_LOG_BOOK_INFO() and GET_NOT_STOCKTAKE_BOOK_INFO()
 *
 * 20150727 (Cameron)
 * - disable change $code to 'NULL' if $code is empty in get_xxxFilter_html()
 *
 * 20150721 (Cameron)
 * - fix bug on SELECTFROMTABLE(): add space before "ORDER BY ..."
 *
 * 20150720 (Cameron)
 * - add checking if 'All' ($BookCategoryType ==0) is selected before apply BookCategory Filter to get_accession_report(),
 * GET_STOCKTAKE_LOG_BOOK_INFO(), GET_WRITEOFF_LOG_INFO()
 *
 * 20150715 (Henry) !!! --> including db table update pls ask Henry before upload to client site <-- !!!
 * - modified function GET_MODULE_OBJ_ARR_ADMIN() to fix the circulation navigation for admin
 * 20150708 (Henry) !!! --> including db table update pls ask Henry before upload to client site <-- !!!
 * - modified function ADD_STOCKTAKE_LOG(), UPDATE_STOCKTAKE_LOG_RECORD(),
 * GET_STOCKTAKE_LOG_BOOK_INFO(), GET_STOCKTAKE_LOG_BOOK_INFO_ORDER() and GET_BOOK_INFO_WITH_STOCKTAKE_WRITEOFF()
 * 20150707 (Henry)
 * - modified function get_current_user_right()
 *
 * 20150622 (Yuen)
 * - modified function Get_Class_Users() to show active users only!
 *
 * 20150611 (Cameron)
 * - fix bug of not able to show accession report when check all items. apply PHPToSQL for parameters in function get_accession_report()
 *
 * 20150609 (Henry)
 * - added function get_class_mgt_user_right()
 * - modified __construct() to initalzate the class mangement right
 * - added get_class_mgt_can_handle_by_user()
 * - added get_class_mgt_classes()
 *
 * 20150608 (Henry)
 * - modified function GET_STOCKTAKE_LOG_BOOK_INFO() to return Creation Date of item from DB
 *
 * 20150605 (Henry)
 * - modified GET_STOCKTAKE_LOG_BOOK_INFO(), GET_STOCKTAKE_LOG_BOOK_INFO_ORDER(), GET_NOT_STOCKTAKE_BOOK_INFO(), GET_WRITEOFF_LOG_INFO()
 * fixed some book status cannot be shown
 *
 * 20150520 (Cameron)
 * - fix bug of missing BookCategoryType in filter condition when UPDATE_BOOK_CATEGORY()
 * - apply SYNC_BOOK_CATEGORY_TO_ELIB function in UPDATE_BOOK_CATEGORY()
 *
 * 20150427 (Cameron)
 * - retrieve PurchaseDate from function GET_WRITEOFF_LOG_INFO()
 *
 * 20150323 (Cameron)
 * - Add function GET_BOOK_INFO_BY_FILTER() for retrieving BookID and CirculationType by BookTitle
 *
 * 20150310 (Henry)
 * modified GET_STOCKTAKE_LOG_BOOK_INFO(), GET_NOT_STOCKTAKE_BOOK_INFO,
 * GET_WRITEOFF_LOG_INFO() and get_accession_report() for $BookCategoryType condition
 *
 * 20150204 (Ryan)
 * Modified function getRemarks() add order by input Date
 * 20150212 (Henry)
 * modified function UPDATE_BOOK_COPY_INFO() update elib physical book if No of copy = 0
 * 20150205 (Cameron)
 * add function GET_BOOK_INFO_WITH_STOCKTAKE_WRITEOFF()
 * add return $result in ADD_WRITEOFF_LOG_RECORD()
 * pass 2nd process result to return value in function UPDATE_BOOK_COPY_INFO()
 *
 * 20150204 (Ryan)
 * to make # column not sortable in sorttable.js, add .css file using tablecounter
 * ej DM#521 - Fixed Save Remarks failed when content includes '
 * ej DM#521 - Fixed negative amount acceptable in overdue
 * 20150204 (Ryan)
 * Training Feedback - Add Book BarCode After BookName in Search User -> Borrowed/Overdue/Reserved table
 * [Case Number : 2015-0203-1543-49054]
 *
 * 20150202 (Henry)
 * modified function get_current_user_right() [Case#M74857]
 *
 * 20150123 (Henry)
 * added parameter "$BookCategoryType" to GET_STOCKTAKE_LOG_BOOK_INFO()
 *
 * 20150122 (Henry)
 * added parameter "$BookCategoryType" to get_accession_report()
 *
 * 20150121 (Henry)
 * Modified GET_STOCKTAKE_LOG() and GET_UNIQUE_BOOK_AMOUNT_BY_LOCATION_PERIOD() to INNER JOIN LIBMS_BOOK in the SQL and add paratmeter $stocktakeSchemeID
 *
 * 20150116 (Henry)
 * added parameter "Type" GET_BOOK_CATEGORY_INFO() and REMOVE_BOOK_CATEGORY()
 *
 * 20150115 (Henry)
 * added PageSettingsBookLanguage
 * added GET_BOOK_LANGUAGE_INFO(), ADD_BOOK_LANGUAGE(), UPDATE_BOOK_LANGUAGE() and REMOVE_BOOK_LANGUAGE()
 *
 * 20150114 (Henry)
 * modified function get_accession_report() to handle missing writeoff Remark
 *
 * 20141218 (Cameron)
 * Add $extraPara in get_xxxFilter_html()
 *
 * 20141217 (Henry)
 * Fixed: failed to access Circulation in KIS [case#S73012]
 *
 * 20141212 (Cameron)
 * Add $Lang['libms']['batch_edit']['edit_books'] to get_batch_edit_tabs
 *
 * 20141208 (Ryan)
 * Modified GET_MODULE_OBJ_ARR_ADMIN() too add eBook Stats
 *
 * 20141205 (Cameron)
 * Add function get_xxxFilter_html() and getBookSubjects()
 *
 * 20141203 (Ryan)
 * Add function Get_Borrowed_Books_Table(),Get_Reserved_Books_Table(),Get_Overdue_Records_Table()
 *
 * 20141203 (Ryan)
 * Add function getRemarks(), updateRemarks(), saveRemarks()
 *
 * 20141202 (Ryan)
 * Add function GET_BOOK_INFO_MULTI()
 *
 * 20141202 (Ryan)
 * Add function get_search_detail_sub_tab()
 *
 * 20141121 (Cameron)
 * Add function getToolbar()
 *
 * 20140919 (Henry)
 * Added function getAdvanceSearchDiv() (in progress)
 *
 * 20140915 (Tiffany)
 * Modified function GET_BOOK_ITEM_INFO(),UPDATE_BOOK_ITEM() to add item ItemSeriesNum
 *
 * 20140912 (Henry)
 * Added function UPDATE_BOOK_STATUS_IF_RESERVED()
 *
 * 20140908 (Henry)
 * Added item_list in get_batch_edit_tabs() (hided)
 *
 * 20140724 (Henry)
 * Added parameter $RecordStatus in GET_NOT_STOCKTAKE_BOOK_INFO()
 *
 * 20140530 (Henry)
 * Added function numberToLetter() to convert a number to letter(s) (eg. 1 -> a, 27 -> aa)
 * Modified function GET_BOOK_ITEM_SELECTION to allow excluding the book status using array of the status key
 *
 * 20140414 (Henry)
 * Modified GET_STOCKTAKE_LOG(), GET_STOCKTAKE_LOG_BOOK_INFO(), GET_STOCKTAKE_LOG_BOOK_INFO_ORDER(), GET_NOT_STOCKTAKE_BOOK_INFO(), GET_WRITEOFF_LOG_INFO()
 *
 * 20140307 (Henry)
 * added parameter "$excludeWriteOffItems" in function GET_STOCKTAKE_LOG()
 * added 'AND lbu.RecordStatus NOT IN ('WRITEOFF', 'DELETE', 'LOST')' in the sql in GET_STOCKTAKE_LOG_BOOK_INFO()
 *
 * 2014-02-19 (Yuen)
 * Modified IsSystemOpenForLoan() to support group base loan period
 *
 * 20140213 (Henry)
 * added parameter "$BookCategoryCode" in function GET_STOCKTAKE_LOG_BOOK_INFO(), GET_NOT_STOCKTAKE_BOOK_INFO() and GET_WRITEOFF_LOG_INFO()
 *
 * 20131127 (Henry)
 * $PageReportingProgressReport
 *
 * 20131127 (Henry)
 * $PageReportingWriteoffReport
 *
 * 20131125 (Henry)
 * modified function ADD_WRITEOFF_LOG_RECORD() and GET_WRITEOFF_LOG_INFO() to store/get the writeoff date and reason
 *
 * 20131122 (Henry)
 * Fix bug on GET_STOCKTAKE_LOG() and GET_NOT_STOCKTAKE_BOOK_INFO() function
 *
 * 20131114 (Henry)
 * added function get_accession_report()
 *
 * 20131113 (Henry)
 * added page $PageReportingAccessionReport
 *
 * 20131112 (Henry)
 * modified function GET_BOOK_ITEM_SELECTION to allow setting the first index text
 *
 * 20131021 (Jason)
 * modified function REMOVE_BOOK() to copy ACNO to ACNO_BAK and set ACNO to be null in item record when remove book
 *
 * 20131008 (Yuen)
 * added function get_fine_report() to generate fine/penalty report
 *
 * 20130916 (Ivan)
 * added function get_book_manage_cancel_button()
 *
 * 20130913 (Ivan)
 * modified GET_BOOK_ITEM_INFO() to retrieve PeriodicalItemID
 * added function NEW_BOOK_RECORD() to group new book logic into a function
 *
 * 20130911 (Ivan)
 * modified GET_MODULE_OBJ_ARR_ADMIN() to add Periodical left menu
 *
 * 20130527 (Rita)
 * modified GET_RESOURCES_TYPE_INFO();
 *
 * 20130522 (Rita)
 * modified GET_LOCATION_INFO();
 * added GET_STOCKTAKE_SCHEME_SQL()
 * ADD_STOCKTAKE_SCHEME_RECORD()
 * UPDATE_STOCKTAKE_SCHEME_RECORD()
 * REMOVE_STOCKTAKE_SCHEME_RECORD()
 * GET_UNIQUE_BOOK_AMOUNT_BY_LOCATION()
 * ADD_STOCKTAKE_LOG()
 * UPDATE_STOCKTAKE_LOG_RECORD()
 * DELETE_STOCKTAKE_LOG_RECORD()
 * GET_STOCKTAKE_LOG()
 * GET_STOCKTAKE_LOG_BOOK_INFO()
 * GET_NOT_STOCKTAKE_BOOK_INFO()
 * ADD_WRITEOFF_LOG_RECORD()
 * UPDATE_WRITEOFF_LOG_RECORD()
 * DELETE_WRITEOFF_LOG_RECORD()
 * GET_WRITEOFF_LOG_INFO()
 * UPDATE_UNIQUE_BOOK()
 *
 * 20120604 Ivan:
 * create this class
 *
 * ***********************************************************
 */

/**
 * Workaround magicquotes
 */
if (! defined("LIBLIBRARYMGMT_DEFINED")) // Preprocessor directive
{
    define("LIBLIBRARYMGMT_DEFINED", true);
    
    if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

        function magicQuotes_awStripslashes(&$value, $key)
        {
            if(!isset($_SESSION[$key])) {
                $value = stripslashes($value);
            }
        }
        $gpc = array(
            &$_GET,
            &$_POST,
            &$_COOKIE,
            &$_REQUEST
        );
        if (function_exists("array_walk_recursive")) {
            array_walk_recursive($gpc, "magicQuotes_awStripslashes");
        }
    }
    
    // extract to overwrite previous extracted variables
    extract($_POST);
    extract($_GET);
    
    // prevent UserID being changed
    if(session_is_registered_intranet("UserID") && session_is_registered_intranet("LOGIN_INTRANET_SESSION_USERID")){
        if($UserID != $_SESSION["LOGIN_INTRANET_SESSION_USERID"] || $_SESSION['UserID'] != $_SESSION["LOGIN_INTRANET_SESSION_USERID"]){
            $UserID = $_SESSION["LOGIN_INTRANET_SESSION_USERID"];
            $_SESSION['UserID'] = $_SESSION["LOGIN_INTRANET_SESSION_USERID"];
        }
    }
    
    /**
     * **************************************************************
     */
    $root_path_found = $_SERVER['DOCUMENT_ROOT'];
    
    if (! file_exists("{$root_path_found}/includes/global.php")) {
        $root_path_found = "../../..";
        if (! file_exists("{$root_path_found}/includes/global.php")) {
            $root_path_found = "../..";
            if (! file_exists("{$root_path_found}/includes/global.php")) {
                $root_path_found = "..";
            }
        }
    }
    include_once ("{$root_path_found}/includes/global.php");
    include_once ("{$root_path_found}/includes/libdb.php");
    include_once ("{$root_path_found}/includes/pdump/pdump.php");

    function getNameFieldWithClassNumberByLangLIBMGMT($prefix = "", $displayBarCode = true)
    {
        global $Lang, $junior_mck;
        $username_field = ($Lang['libms']['SQL']['UserNameFeild']) ? $prefix . $Lang['libms']['SQL']['UserNameFeild'] : "{$prefix}EnglishName";
        if ($displayBarCode) {
            $field_barcode = ", IF({$prefix}BarCode IS NULL OR {$prefix}BarCode='', '', CONCAT(' #', {$prefix}BarCode,''))";
        }
        $field = "CONCAT($username_field,CONVERT(IF({$prefix}ClassName IS NULL OR {$prefix}ClassName='' OR {$prefix}ClassNumber IS NULL OR {$prefix}ClassNumber = '','',CONCAT(' (',{$prefix}ClassName,'-',{$prefix}ClassNumber,')')) USING utf8) {$field_barcode} )";
        
        return $field;
    }

    class liblms extends libdb
    {

        var $db;

        var $IsModuleEnabled;

        var $system_dates;

        // var $CurrentPage;
        var $acno_default_sys_prefix;

        var $CloseMessage;

        // ------------------------------------------------------------------------------------
        function __construct()
        {
            global $eclass_prefix;
            
            // database for this module
            $this->db = $eclass_prefix . "eClass_LIBMS";
            
            $this->system_dates = null;
            
            $this->IsModuleEnabled = true;
            if (! isset($_SESSION['LIBMS']['admin']['current_right'])) {
                $this->get_current_user_right();
            }
            
            if (! isset($_SESSION['LIBMS']['admin']['class_mgt_right'])) {
                $this->get_class_mgt_user_right();
            }
            
            if (! isset($_SESSION['LIBMS']['admin']['check_ip_for_circulation'])) {
                $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] = $this->checkCurrentIP();
            }
            
            $this->acno_default_sys_prefix = "SYSDEF";
        }

        function getStartEndDate($date_type = "open")
        {
            $date_type = trim(strtolower($date_type));
            if ($this->system_dates == null) {
                $sql = "SELECT * FROM `LIBMS_SYSTEM_SETTING` WHERE name='system_open_date' OR name='system_end_date' ";
                $result = $this->returnArray($sql);
                
                for ($i = 0; $i < sizeof($result); $i ++) {
                    $this->system_dates[$result[$i]["name"]] = $result[$i]["value"];
                }
            }
            return $this->system_dates["system_" . $date_type . "_date"];
        }

        function IsSystemOpenForLoan($user_id = '')
        {
            $IsPatronSetting = false;
            
            if ($user_id != "" && $user_id > 0) {
                // get the period control (if any) of related patron group
                $sql = "SELECT if(p.StartDate='0000-00-00', '', p.StartDate) As StartDate, if(p.EndDate='0000-00-00', '', p.EndDate) As EndDate, p.GroupID					    
					 From
						LIBMS_GROUP_PERIOD AS p, LIBMS_GROUP_USER AS u
					 WHERE u.UserID='{$user_id}' AND u.GroupID=p.GroupID ";
                $rows = $this->returnResultSet($sql);
                if (sizeof($rows) > 0) {
                    $SettingObj = $rows[0];
                    $IsPatronSetting = ($SettingObj["StartDate"] != "" || $SettingObj["EndDate"] != "");
                    if ($SettingObj["StartDate"] != "" && strtotime($SettingObj["StartDate"]) > time()) {
                        $this->CloseMessage = $this->getCloseMessage($SettingObj["StartDate"], $SettingObj["EndDate"], "GROUP");
                        return false;
                    }
                    
                    if ($SettingObj["EndDate"] != "" && strtotime($SettingObj["EndDate"]) + 86400 < time()) {
                        $this->CloseMessage = $this->getCloseMessage($SettingObj["StartDate"], $SettingObj["EndDate"], "GROUP");
                        return false;
                    }
                }
            }
            
            if (! $IsPatronSetting) {
                $system_open_date = $this->getStartEndDate("open");
                if ($system_open_date != "" && strtotime($system_open_date) > time()) {
                    $this->CloseMessage = $this->getCloseMessage($system_open_date);
                    return false;
                }
                
                $system_end_date = $this->getStartEndDate("end");
                if ($system_end_date != "" && strtotime($system_end_date) + 86400 < time()) {
                    $this->CloseMessage = $this->getCloseMessage($system_open_date, $system_end_date, "SYSTEM");
                    return false;
                }
            }
            
            return true;
        }

        function getCloseMessage($system_open_date = '', $system_end_date = '', $byType = 'SYSTEM')
        {
            global $Lang;
            // $system_open_date = $this->getStartEndDate("open");
            // $system_end_date = $this->getStartEndDate("end");
            if ($system_open_date != "" && $system_end_date != "") {
                $period = $Lang["libms"]["open_time"]["system_period_full"] . " " . $Lang['libms']['settings']['stocktake']['from'] . " " . $system_open_date . " " . $Lang['libms']['settings']['stocktake']['to'] . " " . $system_end_date;
            } elseif ($system_open_date != "") {
                $period = $Lang["libms"]["open_time"]["system_period_open"] . " " . $system_open_date;
            } elseif ($system_end_date != "") {
                $period = $Lang["libms"]["open_time"]["system_period_end"] . " " . $system_end_date;
            }
            
            $firstMsg = ($byType == "SYSTEM") ? $Lang["libms"]["open_time"]["not_open_yet"] : $Lang["libms"]["open_time"]["not_open_by_group"];
            
            return $firstMsg . $period;
        }

        function GET_MODULE_OBJ_ARR_ADMIN($Current_Page = null)
        {
            global $PATH_WRT_ROOT, $plugin, $CurrentPage, $LAYOUT_SKIN, $image_path, $CurrentPageArr, $root_path_found;
            global $Lang;
            
            if (isset($Current_Page))
                $CurrentPage = $Current_Page;
            // Current Page Information init
            $PageManagement = 0;
            switch ($CurrentPage) {
                // case "PageBookManagement";
                // $PageBookManagement = true;
                // break;
                case "PageEBookLicense": // 2013-07-10 Charles Ma
                case "PageEBookTags": // 2013-10-30 Yuen
                    $PageBookManagement = true;
                    $PageEBook = true;
                    break;
                case "PageBookManagementBookList":
                    $PageBookManagement = true;
                    $PageBookManagementBookList = true;
                    break;
                case "PageBookManagementItemList":
                    $PageBookManagement = true;
                    $PageBookManagementItemList = true;
                    break;
                case "PagePeriodicalManagement":
                    $PageBookManagement = true;
                    $PagePeriodicalManagementPeriodical = true;
                    break;
                case "PageMgmtBatchEdit":
                    $PageBookManagement = true;
                    $PageMgmtBatchEdit = true;
                    break;
                case "PageBookManagementLabel":
                    $PageBookManagement = true;
                    $PageBookManagementLabel = true;
                    break;
                case "PageGroupManagement":
                    $PageGroupManagement = true;
                    break;
                case "PageBookManagementReserveList":
                    $PageBookManagement = true;
                    $PageBookManagementReserveList = true;
                    break;
                case "PageStockTake":
                    $PageStockTake = true;
                    $PageBookManagement = true;
                    break;
                case "PageWriteOff":
                    $PageWriteOff = true;
                    $PageBookManagement = true;
                    break;
                case "PageBookManagementBookImport":
                    $PageBookManagement = true;
                    $PageBookManagementBookImport = true;
                    break;
                case "PageBookManagementLabelFormat":
                    $PageBookManagement = true;
                    $PageBookManagementLabelFormat = true;
                    break;
                case "PageMgmtReturnLostBook":
                    $PageBookManagement = true;
                    $PageReturnLostBook = true;
                    break;
                    
                // ### Reports
                case "PageReportingOverdue":
                    $PageReporting = true;
                    $PageReportingOverdue = true;
                    break;
                case "PageReportingFine":
                    $PageReporting = true;
                    $PageReportingFine = true;
                    break;
                case "PageReportingCirculation":
                    $PageReporting = true;
                    $PageReportingCirculation = true;
                    break;
                case "PageReportingReaderRanking":
                    $PageReporting = true;
                    $PageReportingReaderRanking = true;
                    break;
                case "PageReportingReaderInactive":
                    $PageReporting = true;
                    $PageReportingReaderInactive = true;
                    break;
                case "PageReportingBookRanking":
                    $PageReporting = true;
                    $PageReportingBookRanking = true;
                    break;
                case "PageReportingBookReport":
                    $PageReporting = true;
                    $PageReportingBookReport = true;
                    break;
                case "PageReportingLostBookReport":
                    $PageReporting = true;
                    $PageReportingLostBookReport = true;
                    break;
                case "PageReportingAccessionReport":
                    $PageReporting = true;
                    $PageReportingAccessionReport = true;
                    break;
                case "PageReportingIntragration":
                    $PageStatistics = true;
                    $PageReportingIntragration = true;
                    break;
                case "PageReportingLending":
                    $PageStatistics = true;
                    $PageReportingLending = true;
                    break;
                case "PageReportingPenaltyReport":
                    $PageStatistics = true;
                    $PageReportingPenaltyReport = true;
                    break;
                case "PageReportingCategoryReport":
                    $PageStatistics = true;
                    $PageReportingCategoryReport = true;
                    break;
                case "PageReportingFrequenceReport":
                    $PageStatistics = true;
                    $PageReportingFrequenceReport = true;
                    break;
                case "PageeBookStatisticsReport":
                    $PageStatistics = true;
                    $PageeBookStatisticsReport = true;
                    break;
                case "PageReportingStocktakeReport":
                    $PageReporting = true;
                    $PageReportingStocktakeReport = true;
                    break;
                case "PageReportingWriteoffReport":
                    $PageReporting = true;
                    $PageReportingWriteoffReport = true;
                    break;
                case "PageReportingProgressReport":
                    $PageReporting = true;
                    $PageReportingProgressReport = true;
                    break;
                // ### Settings
                case "PageSettingsSystemSettings":
                    $PageSettings = true;
                    $PageSettingsSystemSettings = true;
                    break;
                
                case "PageSettingsHoliday":
                    $PageSettings = true;
                    $PageSettingsHolidays = true;
                    break;
                case "PageSettingsOpenTime":
                    $PageSettings = true;
                    $PageSettingsOpenTime = true;
                    break;
                case "PageSettingsSpecialOpenTime":
                    $PageSettings = true;
                    $PageSettingsSpecialOpenTime = true;
                    break;
                case "PageSettingsCirculationType":
                    $PageSettings = true;
                    $PageSettingsCirculationType = true;
                    break;
                case "PageSettingsResourcesType":
                    $PageSettings = true;
                    $PageSettingsResourcesType = true;
                    break;
                case "PageSettingsBookCategory":
                    $PageSettings = true;
                    $PageSettingsBookCategory = true;
                    break;
                case "PageSettingsBookLocation":
                    $PageSettings = true;
                    $PageSettingsBookLocation = true;
                    break;
                case "PageSettingsResponsibility":
                    $PageSettings = true;
                    $PageSettingsResponsibility = true;
                    break;
                case "PageSettingsNotification":
                    $PageSettings = true;
                    $PageSettingsNotification = true;
                    break;
                case "PageSettingsStockTake":
                    $PageSettings = true;
                    $PageSettingsStockTake = true;
                    break;
                case "PageSettingsBookLanguage":
                    $PageSettings = true;
                    $PageSettingsBookLanguage = true;
                    break;
                case "PageSettingsSecurity":
                    $PageSettings = true;
                    $PageSettingsSecurity = true;
                    break;
            }
            
            $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT . "home/library_sys/";
            // debug_r($_SESSION['LIBMS']['admin']['current_right']);
            // debug_pr($_SESSION['LIBMS']['admin']['current_right']);
            
            // Circulations Management (appear here for KIS case only)
            if ($_SESSION["platform"] == "KIS" && ($_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["borrow"] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["reserve"] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["return"] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["renew"] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["overdue"]) && $_SESSION['LIBMS']['admin']['check_ip_for_circulation']) {
                $MenuArr["BookManagement"] = array(
                    $Lang['libms']['management']['title'],
                    "#",
                    $PageBookManagement
                );
                $MenuArr["BookManagement"]["Child"]["circulations"] = array(
                    $Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["name"],
                    "javascript:newWindow('{$PATH_WRT_ROOT}home/library_sys/management/circulation/',30)",
                    false
                );
            }
            
            if ($_SESSION['LIBMS']['admin']['current_right']['admin']['book management'] || $_SESSION['LIBMS']['admin']['current_right']['admin']['group management'] || $_SESSION['LIBMS']['admin']['current_right']['admin']['stock-take'] || $_SESSION['LIBMS']['admin']['current_right']['admin']['write-off'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                
                $MenuArr["BookManagement"] = array(
                    $Lang['libms']['management']['title'],
                    "#",
                    $PageBookManagement
                );
                
                // Circulations Management (appear here for KIS case only)
                if ($_SESSION["platform"] == "KIS" && ($_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["borrow"] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["reserve"] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["return"] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["renew"] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["overdue"]) && $_SESSION['LIBMS']['admin']['check_ip_for_circulation']) {
                    $MenuArr["BookManagement"]["Child"]["circulations"] = array(
                        $Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["name"],
                        "javascript:newWindow('{$PATH_WRT_ROOT}home/library_sys/management/circulation/',30)",
                        false
                    );
                } else if (($_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["borrow"] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["reserve"] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["return"] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["renew"] || $_SESSION['LIBMS']['admin']['current_right']["circulation management"]["overdue"]) && $_SESSION['LIBMS']['admin']['check_ip_for_circulation']) {
                    $MenuArr["BookManagement"]["Child"]["circulations"] = array(
                        $Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["name"],
                        "javascript:newWindow('{$PATH_WRT_ROOT}home/library_sys/management/circulation/',31)",
                        false
                    );
                }
                
                if ($_SESSION['LIBMS']['admin']['current_right']['admin']['book management'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    
                    // Book Management
                    $MenuArr["BookManagement"]["Child"]["book_list"] = array(
                        $Lang['libms']['bookmanagement']['book_list'],
                        $PATH_WRT_ROOT . "home/library_sys/admin/book/index.php?clearCoo=1",
                        $PageBookManagementBookList
                    );
                    
                    // Item Management
                    $MenuArr["BookManagement"]["Child"]["item_list"] = array(
                        $Lang['libms']['bookmanagement']['item_list'],
                        $PATH_WRT_ROOT . "home/library_sys/admin/book/book_item_list.php?clearCoo=1",
                        $PageBookManagementItemList
                    );
                    
                    // Periodical
                    $MenuArr["BookManagement"]["Child"]["Periodical"] = array(
                        $Lang["libms"]["periodicalMgmt"]["Periodical"],
                        $PATH_WRT_ROOT . "home/library_sys/admin/periodical/?clearCoo=1",
                        $PagePeriodicalManagementPeriodical
                    );
                    
                    // Batch Edit
                    // if ($plugin["elib_plus_demo"])
                    // {
                    // $MenuArr["BookManagement"]["Child"]["batchedit"] = array($Lang['libms']['batch_edit']['menu'], $PATH_WRT_ROOT."home/library_sys/reports/demo/batch_modify_1.php", $PageMgmtBatchEdit);
                    // }
                    // else //Henry Added (20130926)
                    $MenuArr["BookManagement"]["Child"]["batchedit"] = array(
                        $Lang['libms']['batch_edit']['menu'],
                        $PATH_WRT_ROOT . "home/library_sys/admin/book/batch_modify_1.php",
                        $PageMgmtBatchEdit
                    );
                    
                    // Book Label
                    // $MenuArr["BookManagement"]["Child"]["book_import"] = array($Lang['libms']['bookmanagement']['book_import'], $PATH_WRT_ROOT."home/library_sys/admin/book/import/import_book_data.php", $PageBookManagementBookImport);
                    $MenuArr["BookManagement"]["Child"]["book_label"] = array(
                        $Lang['libms']['bookmanagement']['book_label'],
                        $PATH_WRT_ROOT . "home/library_sys/admin/book/prepare_label.php",
                        $PageBookManagementLabel
                    );
                    // $MenuArr["BookManagement"]["Child"]["book_labelformat"] = array($Lang['libms']['bookmanagement']['book_labelformat'], $PATH_WRT_ROOT."home/library_sys/admin/book/label_format.php", $PageBookManagementLabelFormat);
                    $MenuArr["BookManagement"]["Child"]["book_reserve"] = array(
                        $Lang['libms']['bookmanagement']['book_reserve'],
                        $PATH_WRT_ROOT . "home/library_sys/admin/book/reserve_book.php",
                        $PageBookManagementReserveList
                    );
                    
                    $MenuArr["BookManagement"]["Child"]["ReturnLostBook"] = array(
                        $Lang['libms']['bookmanagement']['ReturnLostBook'],
                        $PATH_WRT_ROOT . "home/library_sys/admin/book/lost_book_list.php",
                        $PageReturnLostBook
                    );
                }
                
                if ($_SESSION['LIBMS']['admin']['current_right']['admin']['group management'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    // Group Management
                    $MenuArr["BookManagement"]["Child"]["GroupManagement"] = array(
                        $Lang['libms']['group_management'],
                        $PATH_WRT_ROOT . "home/library_sys/admin/group/index.php",
                        $PageGroupManagement
                    );
                }
                
                if ($_SESSION['LIBMS']['admin']['current_right']['admin']['stock-take'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    // if ($plugin["elib_plus_demo"])
                    // {
                    // $MenuArr["BookManagement"]["Child"]["stocktake"] = array($Lang['libms']['bookmanagement']['stocktakeAndWriteOff'], $PATH_WRT_ROOT."home/library_sys/reports/demo/stock-take_mgmt01.php", $PageReportingCategoryReport);
                    // } else
                    // {
                    
                    // Stock Take - Checking to be added
                    // $MenuArr["BookManagement"]["Child"]["stocktake"] = array($Lang['libms']['bookmanagement']['stocktakeAndWriteOff'], $PATH_WRT_ROOT."home/library_sys/admin/stocktake/index.php",$PageStockTake);
                    $MenuArr["BookManagement"]["Child"]["stocktake"] = array(
                        $Lang['libms']['bookmanagement']['stock-take'],
                        $PATH_WRT_ROOT . "home/library_sys/admin/stocktake/index.php",
                        $PageStockTake
                    );
                    // }
                }
                
                if ($_SESSION['LIBMS']['admin']['current_right']['admin']['write-off'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    // write-off - Checking to be added
                    $MenuArr["BookManagement"]["Child"]["writeoff"] = array(
                        $Lang['libms']['bookmanagement']['writeOff'],
                        $PATH_WRT_ROOT . "home/library_sys/admin/stocktake/write_off.php",
                        $PageWriteOff
                    );
                }
                
                // 2013-08-19 Charles Ma
                include_once ("{$root_path_found}/includes/libelibrary_install.php");
                $Libelibinstall = new elibrary_install();
                
                // 2013-08-19 Charles Ma
                if ($Libelibinstall->retrive_license_enable() && ($_SESSION['LIBMS']['admin']['current_right']['admin']['book management'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])) {
                    
                    $MenuArr["BookManagement"]["Child"]["ebook"] = array(
                        $Lang['libms']['bookmanagement']['ebook'],
                        $PATH_WRT_ROOT . "home/library_sys/admin/book/elib_setting_license.php",
                        $PageEBook
                    );
                    // $MenuArr["BookManagement"]["Child"]["ebook_license"] = array($Lang['libms']['bookmanagement']['ebook_license'], $PATH_WRT_ROOT."home/library_sys/admin/book/elib_setting_license.php", $PageEBookLicense);
                    
                    // $MenuArr["BookManagement"]["Child"]["ebook_tags"] = array($Lang['libms']['bookmanagement']['ebook_tags'], $PATH_WRT_ROOT."home/library_sys/admin/book/elib_setting_tags.php", $PageEBookTags);
                }
                // 2013-07-10 Charles Ma END
            }
            
            // ################## Report Start ########################
            if ($_SESSION['LIBMS']['admin']['current_right']['reports'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                // Reporting
                $MenuArr["reporting"] = array(
                    $Lang['libms']['reporting']['title'],
                    "#",
                    $PageReporting
                );
                
                if ($_SESSION['LIBMS']['admin']['current_right']['reports']['accession report'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["reporting"]["Child"]["PageReportingAccessionReport"] = array(
                        $Lang["libms"]["GroupManagement"]["Right"]["reports"]["accession report"],
                        $PATH_WRT_ROOT . "home/library_sys/reports/accession_report.php",
                        $PageReportingAccessionReport
                    );
                }
                
                if ($_SESSION['LIBMS']['admin']['current_right']['reports']['book report'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["reporting"]["Child"]["PageReportingBookReport"] = array(
                        $Lang['libms']['reporting']['PageReportingBookReport'],
                        $PATH_WRT_ROOT . "home/library_sys/reports/?page=BookReport",
                        $PageReportingBookReport
                    );
                }
                
                if ($_SESSION['LIBMS']['admin']['current_right']['reports']['loan report'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["reporting"]["Child"]["PageReportingCirculation"] = array(
                        $Lang['libms']['reporting']['PageReportingCirculation'],
                        $PATH_WRT_ROOT . "home/library_sys/reports/?page=Circulation",
                        $PageReportingCirculation
                    );
                }
                
                if ($_SESSION['LIBMS']['admin']['current_right']['reports']['overdue report'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["reporting"]["Child"]["PageReportingOverdue"] = array(
                        $Lang['libms']['reporting']['PageReportingOverdue'],
                        $PATH_WRT_ROOT . "home/library_sys/reports/?page=Overdue",
                        $PageReportingOverdue
                    );
                }
                
                if ($_SESSION['LIBMS']['admin']['current_right']['reports']['penalty report'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["reporting"]["Child"]["PageReportingFine"] = array(
                        $Lang["libms"]["reporting"]["PageReportingFine"],
                        $PATH_WRT_ROOT . "home/library_sys/reports/fine_report.php",
                        $PageReportingFine
                    );
                }
                
                if ($_SESSION['LIBMS']['admin']['current_right']['reports']['lost report'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["reporting"]["Child"]["PageReportingLostBookReport"] = array(
                        $Lang['libms']['reporting']['PageReportingLostBookReport'],
                        $PATH_WRT_ROOT . "home/library_sys/admin/book/lost_book.php",
                        $PageReportingLostBookReport
                    );
                }
                if ($_SESSION['LIBMS']['admin']['current_right']['reports']['reader ranking'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["reporting"]["Child"]["PageReportingReaderRanking"] = array(
                        $Lang['libms']['reporting']['PageReportingReaderRanking'],
                        $PATH_WRT_ROOT . "home/library_sys/reports/?page=ReaderRanking",
                        $PageReportingReaderRanking
                    );
                }
                if ($_SESSION['LIBMS']['admin']['current_right']['reports']['inactive reader'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["reporting"]["Child"]["PageReportingReaderInactive"] = array(
                        $Lang['libms']['reporting']['PageReportingReaderInactive'],
                        $PATH_WRT_ROOT . "home/library_sys/reports/?page=ReaderInactive",
                        $PageReportingReaderInactive
                    );
                }
                if ($_SESSION['LIBMS']['admin']['current_right']['reports']['book ranking'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["reporting"]["Child"]["PageReportingBookRanking"] = array(
                        $Lang['libms']['reporting']['PageReportingBookRanking'],
                        $PATH_WRT_ROOT . "home/library_sys/reports/?page=BookRanking",
                        $PageReportingBookRanking
                    );
                }
                if ($_SESSION['LIBMS']['admin']['current_right']['reports']['stock-take report'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["reporting"]["Child"]["PageReportingStocktake"] = array(
                        $Lang['libms']['reporting']['stocktakeReport'],
                        $PATH_WRT_ROOT . "home/library_sys/reports/stocktake_report/index.php",
                        $PageReportingStocktakeReport
                    );
                }
                if ($_SESSION['LIBMS']['admin']['current_right']['reports']['write-off report'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["reporting"]["Child"]["PageReportingWriteoff"] = array(
                        $Lang["libms"]["GroupManagement"]["Right"]["reports"]["write-off report"],
                        $PATH_WRT_ROOT . "home/library_sys/reports/writeoff_report/index.php",
                        $PageReportingWriteoffReport
                    );
                }
                if ($_SESSION['LIBMS']['admin']['current_right']['reports']['progress report'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["reporting"]["Child"]["PageReportingProgress"] = array(
                        $Lang["libms"]["GroupManagement"]["Right"]["reports"]["progress report"],
                        $PATH_WRT_ROOT . "home/library_sys/reports/progress_report/index.php",
                        $PageReportingProgressReport
                    );
                }
                // if ($plugin["elib_plus_demo"])
                // {
                // $MenuArr["reporting"]["Child"]["stocktake"] = array($Lang['libms']['reporting']['stock-take'], $PATH_WRT_ROOT."home/library_sys/reports/demo/stock-take_report01.php", $PageReportingCategoryReport);
                // }
            }
            
            // ################## Report End ########################
            
            // debug_pr($_SESSION['LIBMS']['admin']['current_right']['statistics']);
            if ($_SESSION['LIBMS']['admin']['current_right']['statistics'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                // Statistics
                
                $MenuArr["statistics"] = array(
                    $Lang["libms"]["statistics"]["title"],
                    "#",
                    $PageStatistics
                );
                
                if ($_SESSION['LIBMS']['admin']['current_right']['statistics']['summary'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["statistics"]["Child"]["PageReportingIntragration"] = array(
                        $Lang['libms']['reporting']['PageReportingIntragration'],
                        $PATH_WRT_ROOT . "home/library_sys/reports/?page=Integrated",
                        $PageReportingIntragration
                    );
                }
                if ($_SESSION['LIBMS']['admin']['current_right']['statistics']['general stats'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["statistics"]["Child"]["PageReportingLending"] = array(
                        $Lang['libms']['reporting']['PageReportingLending'],
                        $PATH_WRT_ROOT . "home/library_sys/reports/?page=Lending",
                        $PageReportingLending
                    );
                }
                
                if ($_SESSION['LIBMS']['admin']['current_right']['statistics']['category vs class or form'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["statistics"]["Child"]["PageReportingCategoryReport"] = array(
                        $Lang['libms']['reporting']['PageReportingCategoryReport'],
                        $PATH_WRT_ROOT . "home/library_sys/reports/?page=CategoryReport",
                        $PageReportingCategoryReport
                    );
                }
                
                if ($_SESSION['LIBMS']['admin']['current_right']['statistics']['frequency by form'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    // if ($plugin["elib_plus_demo"])
                    // {
                    // $MenuArr["statistics"]["Child"]["PageReportingFrequency"] = array($Lang["libms"]["reporting"]["ReadingFrequency"], $PATH_WRT_ROOT."home/library_sys/reports/demo/stats1.php", $PageReportingFrequenceReport);
                    // }
                    $MenuArr["statistics"]["Child"]["PageReportingFrequency"] = array(
                        $Lang["libms"]["reporting"]["ReadingFrequency"],
                        $PATH_WRT_ROOT . "home/library_sys/reports/frequency_report/index.php",
                        $PageReportingFrequenceReport
                    );
                }
                
                if ($_SESSION['LIBMS']['admin']['current_right']['statistics']['ebook statistics'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["statistics"]["Child"]["PageeBookStatisticsReport"] = array(
                        $Lang["libms"]["eBookStat"]["ModuleTitle"],
                        $PATH_WRT_ROOT . "home/library_sys/reports/eBookStat/",
                        $PageeBookStatisticsReport
                    );
                }
                
                if ($_SESSION['LIBMS']['admin']['current_right']['statistics']['penalty stats'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                    $MenuArr["statistics"]["Child"]["PageReportingPenaltyReport"] = array(
                        $Lang['libms']['reporting']['PageReportingPenaltyReport'],
                        $PATH_WRT_ROOT . "home/library_sys/reports/?page=PenaltyReport",
                        $PageReportingPenaltyReport
                    );
                }
            }
            
            if ($_SESSION['LIBMS']['admin']['current_right']['admin']['settings'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
                // Settings
                $MenuArr["Settings"] = array(
                    $Lang['libms']['settings']['title'],
                    "#",
                    $PageSettings
                );
                $MenuArr["Settings"]["Child"]["system_settings"] = array(
                    $Lang["libms"]["settings"]["title"],
                    $PATH_WRT_ROOT . "home/library_sys/admin/settings/system_settings.php",
                    $PageSettingsSystemSettings
                );
                
                $MenuArr["Settings"]["Child"]["notification"] = array(
                    $Lang['libms']["settings"]['notification']['title'],
                    $PATH_WRT_ROOT . "home/library_sys/admin/settings/notification.php",
                    $PageSettingsNotification
                );
                
                // if ($plugin["elib_plus_demo"])
                // {
                // $MenuArr["Settings"]["Child"]["stocktake"] = array($Lang["libms"]["settings"]["stock-take"], $PATH_WRT_ROOT."home/library_sys/reports/demo/stock-take_setting01.php", $PageReportingCategoryReport);
                // } else
                // {
                $MenuArr["Settings"]["Child"]["stocktake"] = array(
                    $Lang["libms"]["settings"]["stock-take"],
                    $PATH_WRT_ROOT . "home/library_sys/admin/settings/stock_take.php?clearCoo=1",
                    $PageSettingsStockTake
                );
                // }
                
                $MenuArr["Settings"]["Child"]["holidays"] = array(
                    $Lang['libms']['settings']['holidays'],
                    $PATH_WRT_ROOT . "home/library_sys/admin/settings/holidays.php",
                    $PageSettingsHolidays
                );
                $MenuArr["Settings"]["Child"]["opentime"] = array(
                    $Lang['libms']['settings']['opentime'],
                    $PATH_WRT_ROOT . "home/library_sys/admin/settings/open_time.php",
                    $PageSettingsOpenTime
                );
                $MenuArr["Settings"]["Child"]["PageSettingsSpecialOpenTime"] = array(
                    $Lang['libms']['settings']['PageSettingsSpecialOpenTime'],
                    $PATH_WRT_ROOT . "home/library_sys/admin/settings/special_open_time.php",
                    $PageSettingsSpecialOpenTime
                );
                $MenuArr["Settings"]["Child"]["circulation_type"] = array(
                    $Lang['libms']['settings']['circulation_type'],
                    $PATH_WRT_ROOT . "home/library_sys/admin/settings/circulation_type.php",
                    $PageSettingsCirculationType
                );
                $MenuArr["Settings"]["Child"]["resources_type"] = array(
                    $Lang['libms']['settings']['resources_type'],
                    $PATH_WRT_ROOT . "home/library_sys/admin/settings/resources_type.php",
                    $PageSettingsResourcesType
                );
                $MenuArr["Settings"]["Child"]["book_category"] = array(
                    $Lang['libms']['settings']['book_category'],
                    $PATH_WRT_ROOT . "home/library_sys/admin/settings/book_category.php",
                    $PageSettingsBookCategory
                );
                $MenuArr["Settings"]["Child"]["book_language"] = array(
                    $Lang['libms']['settings']['book_language'],
                    $PATH_WRT_ROOT . "home/library_sys/admin/settings/book_language.php",
                    $PageSettingsBookLanguage
                );
                $MenuArr["Settings"]["Child"]["book_location"] = array(
                    $Lang['libms']['settings']['book_location'],
                    $PATH_WRT_ROOT . "home/library_sys/admin/settings/book_location.php",
                    $PageSettingsBookLocation
                );
                $MenuArr["Settings"]["Child"]["responsibility"] = array(
                    $Lang['libms']['settings']['responsibility'],
                    $PATH_WRT_ROOT . "home/library_sys/admin/settings/responsibility.php",
                    $PageSettingsResponsibility
                );
                $MenuArr["Settings"]["Child"]["security_setting"] = array(
                    $Lang['libms']['settings']['security']['title'],
                    $PATH_WRT_ROOT . "home/library_sys/admin/settings/terminal_ip_setting.php",
                    $PageSettingsSecurity
                );
            }
            
            // ## module information
            $MODULE_OBJ['title'] = $Lang['libms']['library'];
            $MODULE_OBJ['title_css'] = "menu_opened";
            $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_eLibrary.gif";
            $MODULE_OBJ['menu'] = $MenuArr;
            
            return $MODULE_OBJ;
        }

        function get_current_user_right()
        {
            $c_user_id = $_SESSION['UserID'];
            $sql = "SELECT `GroupID` FROM `LIBMS_GROUP_USER` WHERE `UserID`={$c_user_id} order by DateModified desc LIMIT 1";
            $result = $this->returnArray($sql);
            // error_log(mysql_error());
            if (! empty($result))
                $GID = $result[0][0];
            else {
                $_SESSION['LIBMS']['admin']['current_right'] = Array();
                return;
            }
            $sql = "SELECT `Section`, `Function`,`IsAllow`  FROM `LIBMS_GROUP_RIGHT` WHERE GroupID='$GID'";
            $result = $this->returnArray($sql);
            // error_log(mysql_error());
            foreach ($result as $row) {
                if ($row['IsAllow'])
                    $effective_right[$row['Section']][$row['Function']] = true;
            }
            $_SESSION['LIBMS']['admin']['current_right'] = $effective_right;
            
            if (! $_SESSION['LIBMS']['admin']['current_right']) {
                $_SESSION['LIBMS']['admin']['current_right'] = Array();
            }
            
            return;
        }

        function get_class_mgt_user_right()
        {
            $c_user_id = $_SESSION['UserID'];
            $sql = "SELECT '1' FROM `LIBMS_CLASS_MANAGEMENT_GROUP_USER` WHERE `UserID`={$c_user_id} order by DateModified desc LIMIT 1";
            $result = $this->returnArray($sql);
            // error_log(mysql_error());
            if (! empty($result))
                $GID = $result[0][0];
            else {
                $_SESSION['LIBMS']['admin']['class_mgt_right'] = Array();
                return;
            }
            $sql = "SELECT `Section`, `Function`,`IsAllow`  FROM `LIBMS_CLASS_MANAGEMENT_GROUP_RIGHT` WHERE GroupID='$GID'";
            $result = $this->returnArray($sql);
            // error_log(mysql_error());
            foreach ($result as $row) {
                if ($row['IsAllow'])
                    $effective_right[$row['Section']][$row['Function']] = true;
            }
            $_SESSION['LIBMS']['admin']['class_mgt_right'] = $effective_right;
            
            if (! $_SESSION['LIBMS']['admin']['class_mgt_right']) {
                $_SESSION['LIBMS']['admin']['class_mgt_right'] = Array();
            }
            
            return;
        }

        function get_class_mgt_can_handle_by_user($partonID)
        {
            $c_user_id = $_SESSION['UserID'];
            $sql = "SELECT `GroupID` FROM `LIBMS_CLASS_MANAGEMENT_GROUP_USER` WHERE `UserID`={$c_user_id} order by DateModified desc";
            $result = $this->returnArray($sql);
            // error_log(mysql_error());
            if (! empty($result)) {
                foreach ($result as $row) {
                    $GIDs[] = $row['GroupID'];
                }
            } else {
                $_SESSION['LIBMS']['admin']['class_mgt_right'] = Array();
                return array();
            }
            $sql = "SELECT `ClassName`  FROM `LIBMS_CLASS_MANAGEMENT_GROUP` WHERE GroupID IN ('" . implode("','", $GIDs) . "')";
            $result = $this->returnArray($sql);
            // error_log(mysql_error());
            $classArr = array();
            foreach ($result as $row) {
                if ($row['ClassName'])
                    $classArr[] = $row['ClassName'];
            }
            
            $sql = "SELECT `ClassName` FROM LIBMS_USER WHERE UserID = '" . $partonID . "'";
            $result = $this->returnArray($sql);
            
            if ($result[0][0] && in_array($result[0][0], $classArr))
                return true;
            
            return false;
        }

        function get_class_mgt_classes()
        {
            $c_user_id = $_SESSION['UserID'];
            $sql = "SELECT `GroupID` FROM `LIBMS_CLASS_MANAGEMENT_GROUP_USER` WHERE `UserID`={$c_user_id} order by DateModified desc";
            $result = $this->returnArray($sql);
            // error_log(mysql_error());
            if (! empty($result)) {
                foreach ($result as $row) {
                    $GIDs[] = $row['GroupID'];
                }
            } else {
                $_SESSION['LIBMS']['admin']['class_mgt_right'] = Array();
                return array();
            }
            $sql = "SELECT `ClassName`  FROM `LIBMS_CLASS_MANAGEMENT_GROUP` WHERE GroupID IN ('" . implode("','", $GIDs) . "')";
            $result = $this->returnArray($sql);
            // error_log(mysql_error());
            $classArr = array();
            foreach ($result as $row) {
                if ($row['ClassName'])
                    $classArr[] = $row['ClassName'];
            }
            
            return $classArr;
        }

        function get_class_mgt_can_handle_item($UniqueID)
        {
            $c_user_id = $_SESSION['UserID'];
            $sql = "SELECT `GroupID` FROM `LIBMS_CLASS_MANAGEMENT_GROUP_USER` WHERE `UserID`={$c_user_id} order by DateModified desc";
            $result = $this->returnArray($sql);
            // error_log(mysql_error());
            if (! empty($result)) {
                foreach ($result as $row) {
                    $GIDs[] = $row['GroupID'];
                }
            } else {
                $_SESSION['LIBMS']['admin']['class_mgt_right'] = Array();
                return array();
            }
            $sql = "SELECT `LocationCode`  FROM `LIBMS_CLASS_MANAGEMENT_GROUP` WHERE GroupID IN ('" . implode("','", $GIDs) . "')";
            $result = $this->returnArray($sql);
            // error_log(mysql_error());
            $locationArr = array();
            foreach ($result as $row) {
                if ($row['LocationCode'])
                    $locationArr[] = $row['LocationCode'];
            }
            $sql = "SELECT `LocationCode` FROM LIBMS_BOOK_UNIQUE WHERE UniqueID = '" . $UniqueID . "'";
            $result = $this->returnArray($sql);
            
            if ($result[0][0] && in_array($result[0][0], $locationArr))
                return true;
            
            return false;
        }

        /**
         * LIBMS_SYSTEM_SETTING
         * 
         * @param string $key            
         * @return boolean|string
         */
        function get_system_setting($key)
        {
            $ret = '';
            if (empty($key)) {
                return FALSE;
            }
            $sql = "SELECT `Value` FROM `LIBMS_SYSTEM_SETTING` WHERE name='$key'";
            $result = $this->returnArray($sql);
            // tolog($result);
            if (empty($result)) {
                return FALSE;
            }
            
            return $result[0][0];
        }

        function MODULE_AUTHENTICATION($CurrentPage)
        {
            if (! $this->IS_ENABLE($CurrentPage)) {
                die("You are not allowed to access this page of Library Management System!");
            }
        }

        // check if current user can use this module
        // please check for plugin, user's right to the function (section & function)
        function IS_ENABLE($CurrentPage)
        {
            global $plugin;
            
            if (! $this->IsModuleEnabled) {
                return false;
            } else {
                // check user's right to current page - $CurrentPage
                // better to keep user rights in Session for best performance, e.g. $_SESSION['library_rights'] = "PageBookManagement; PageGroupManagement; "
                
                return true;
            }
        }

        /**
         * *****************
         * Stocktake Start
         * *****************
         */
        function GET_STOCKTAKE_SCHEME_SQL($stocktakeSchemeID = '', $order = '')
        {
            if ($stocktakeSchemeID != '') {
                $stocktakeSchemeID_Cond = " AND StocktakeSchemeID = '" . $stocktakeSchemeID . "' ";
            }
            
            if ($order != '') {
                $order_cond = " ORDER BY EndDate DESC ";
            }
            
            $Sql = " SELECT						
					 SchemeTitle, 
					 StartDate, 
					 EndDate,
					 RecordStartDate, 
					 RecordEndDate, 
					 DATE_FORMAT(DateModified,'%Y-%m-%d %H:%i'),  			
					 CONCAT('<input type=\"checkbox\" name=\"StocktakeSchemeID[]\" value=\"', StocktakeSchemeID ,'\">') As CheckBox, 					 
					 StocktakeSchemeID
				 FROM 
					 LIBMS_STOCKTAKE_SCHEME
				 WHERE 1
				 $stocktakeSchemeID_Cond
				 $order_cond
				 ";
            
            return $Sql;
        }

        function ADD_STOCKTAKE_SCHEME_RECORD($schemeTitle, $description, $startDate, $endDate, $recordsStartDate, $recordsEndDate)
        {
            global $UserID;
            
            $Sql = " INSERT INTO LIBMS_STOCKTAKE_SCHEME
				 (SchemeTitle, Description, StartDate, EndDate, RecordStartDate, RecordEndDate, DateModified, LastModifiedBy)
				 VALUES 
				 ('" . $this->Get_Safe_Sql_Query($schemeTitle) . "' , 
					'" . $this->Get_Safe_Sql_Query($description) . "', 
					'" . $startDate . "' , 
					'" . $endDate . "' ,
					'" . $recordsStartDate . "' , 
					'" . $recordsEndDate . "' ,
					now(), 
					'" . $UserID . "')";
            
            $result = $this->db_db_query($Sql);
            return $result;
        }

        function UPDATE_STOCKTAKE_SCHEME_RECORD($stocktakeSchemeID, $schemeTitle, $description, $startDate, $endDate, $recordsStartDate, $recordsEndDate)
        {
            $Sql = " UPDATE 
					LIBMS_STOCKTAKE_SCHEME
				 SET 
					SchemeTitle = '" . $this->Get_Safe_Sql_Query($schemeTitle) . "' , 
					Description = '" . $this->Get_Safe_Sql_Query($description) . "' , 
					StartDate = '" . $startDate . "', 
					EndDate = '" . $endDate . "',
					RecordStartDate = '" . $recordsStartDate . "', 
					RecordEndDate = '" . $recordsEndDate . "',  
					DateModified = now()							 
				 WHERE 
					StocktakeSchemeID = '" . $stocktakeSchemeID . "'
				 ";
            $result = $this->db_db_query($Sql);
            return $result;
        }

        function REMOVE_STOCKTAKE_SCHEME_RECORD($stocktakeSchemeID)
        {
            $Sql = " DELETE FROM 
				 	LIBMS_STOCKTAKE_SCHEME
				 WHERE 
					StocktakeSchemeID IN ('" . implode("','", (array) $stocktakeSchemeID) . "')";
            $result = $this->db_db_query($Sql);
            // debug_pr($Sql);
            
            return $result;
        }

        function GET_UNIQUE_BOOK_AMOUNT_BY_LOCATION()
        {
            /*
             * $sql = "SELECT
             * COUNT(lbu.UniqueID) AS UniqueBookAmount, lb.LocationCode
             * FROM
             * LIBMS_BOOK_UNIQUE lbu
             * INNER JOIN
             * LIBMS_BOOK lb ON lb.BookID = lbu.BookID
             * WHERE 1
             * AND lbu.RecordStatus NOT LIKE 'DELETE'
             * GROUP BY TRIM(lb.LocationCode);
             * ";
             */
            $sql = "SELECT 
					 COUNT(lbu.UniqueID) AS UniqueBookAmount, lbu.LocationCode
				FROM 
					LIBMS_BOOK_UNIQUE lbu
				WHERE 
					 lbu.RecordStatus NOT IN ('WRITEOFF', 'DELETE', 'LOST')
				GROUP BY lbu.LocationCode; 
				";
            $results = $this->returnArray($sql);
            
            return $results;
        }

        // for the amendance of stocktake report [Start] 20140408
        function GET_UNIQUE_BOOK_AMOUNT_BY_LOCATION_PERIOD($RecordStartDate = '', $RecordEndDate = '', $stocktakeSchemeID = '')
        {
            $cond = '';
            if ($RecordEndDate)
                $cond .= " AND (lbu.CreationDate IS NULL OR lbu.CreationDate='0000-00-00 00:00:00' OR UNIX_TIMESTAMP(lbu.CreationDate) <= UNIX_TIMESTAMP('{$RecordEndDate} 23:59:59'))";
            
            if ($stocktakeSchemeID)
                $cond2 = " AND lwl2.StocktakeSchemeID = '" . $stocktakeSchemeID . "'";
            
            $cond1 = '';
            if ($RecordStartDate) {
                $cond1 = " OR lbu.UniqueID IN (
						SELECT 
							 lwl2.BookUniqueID
						FROM 
							LIBMS_WRITEOFF_LOG lwl2
						JOIN
							LIBMS_BOOK_UNIQUE lbu2
						ON 
							lwl2.BookUniqueID = lbu2.UniqueID
						WHERE
							(UNIX_TIMESTAMP(lwl2.WriteOffDate) >= UNIX_TIMESTAMP('" . $RecordStartDate . "') OR UNIX_TIMESTAMP(lwl2.DateModified) >= UNIX_TIMESTAMP('" . $RecordStartDate . "'))
							{$cond2}
					)";
            }
            $sql = "SELECT 
					 COUNT(lbu.UniqueID) AS UniqueBookAmount, lbu.LocationCode
				FROM 
					LIBMS_BOOK_UNIQUE lbu
				JOIN 
					LIBMS_BOOK lb
				ON lbu.BookID = lb.BookID
				WHERE 
					(lbu.RecordStatus NOT IN ('WRITEOFF', 'DELETE') OR lbu.RecordStatus IS NULL)
					{$cond}
					{$cond1}
				GROUP BY lbu.LocationCode; 
				";
            $results = $this->returnArray($sql);
            
            return $results;
        }

        // for the amendance of stocktake report [End] 20140408
        function ADD_STOCKTAKE_LOG($StocktakeSchemeID, $bookID, $bookUinqueID, $result = 'FOUND')
        {
            global $UserID;
            
            // get the item status
            $sql = "SELECT RecordStatus FROM LIBMS_BOOK_UNIQUE WHERE UniqueID = '" . $bookUinqueID . "'";
            $ItemStatus = $this->returnVector($sql);
            
            $Sql = " INSERT INTO LIBMS_STOCKTAKE_LOG
				 (StocktakeSchemeID, BookID, BookUniqueID, Result, RecordStatus, DateModified, LastModifiedBy)
				 VALUES 
					('" . $StocktakeSchemeID . "', 
					'" . $bookID . "' , 
					'" . $bookUinqueID . "' ,
					'" . $result . "',
					'" . $ItemStatus[0] . "',
					now(), 
					'" . $UserID . "')";
            
            $result = $this->db_db_query($Sql);
            
            return $result;
        }

        function UPDATE_STOCKTAKE_LOG_RECORD($stocktakeLogID, $StocktakeSchemeID, $bookID, $bookUinqueID, $result = 'FOUND')
        {
            global $UserID;
            
            // get the item status
            $sql = "SELECT RecordStatus FROM LIBMS_BOOK_UNIQUE WHERE UniqueID = '" . $bookUinqueID . "'";
            $ItemStatus = $this->returnVector($sql);
            
            $Sql = " UPDATE 
					LIBMS_STOCKTAKE_LOG
				 SET 
					StocktakeSchemeID = '" . $StocktakeSchemeID . "' , 
					BookID = '" . $bookID . "' , 
					BookUniqueID = '" . $bookUinqueID . "', 
					Result = '" . $result . "', 
					RecordStatus = '" . $ItemStatus[0] . "', 
					DateModified = now(),	
					LastModifiedBy = '" . $UserID . "' 							 
				 WHERE 
					StocktakeLogID = '" . $stocktakeLogID . "'
				 ";
            $result = $this->db_db_query($Sql);
            return $result;
        }

        function DELETE_STOCKTAKE_LOG_RECORD($stocktakeLogID)
        {
            $Sql = " DELETE FROM 
					LIBMS_STOCKTAKE_LOG						 
				 WHERE 
					StocktakeLogID = '" . $stocktakeLogID . "' ";
            $result = $this->db_db_query($Sql);
            
            return $result;
        }

        function GET_STOCKTAKE_LOG($stocktakeLogID = '', $StocktakeSchemeID = '', $bookUinqueID = '', $bookID = '', $locationCode = '', $excludeWriteOffItems = 0)
        {
            if ($stocktakeLogID != '') {
                $stocktakeLogID_cond = " AND lsl.StocktakeLogID = '" . $stocktakeLogID . "' ";
            }
            if ($StocktakeSchemeID != '') {
                $stocktakeSchemeID_cond = " AND lsl.StocktakeSchemeID = '" . $StocktakeSchemeID . "' ";
            }
            if ($bookUinqueID != '') {
                $bookUinqueID_cond = " AND lsl.BookUniqueID IN ('" . implode("','", (array) $bookUinqueID) . "') ";
            }
            if ($bookID != '') {
                $bookID_cond = " AND lsl.BookID IN ('" . implode("','", (array) $bookID) . "') ";
            }
            if ($locationCode != '') {
                /*
                 * $locationVariable = ",lb.LocationCode";
                 * $location_cond = " AND lb.LocationCode = '".$locationCode."' " ;
                 * $locationJoinSql = " INNER JOIN LIBMS_BOOK lb ON lb.BookID = lsl.BookID";
                 */
                $locationVariable = ",lb.LocationCode";
                $location_cond = " AND lb.LocationCode = '" . $locationCode . "' ";
                $locationJoinSql = " LEFT JOIN LIBMS_BOOK_UNIQUE lb ON lb.UniqueID = lsl.BookUniqueID";
            }
            // Henry20140430
            if ($locationCode == - 1) {
                $locationNotExist = $this->GET_LOCATION_NOT_EXIST();
                $locationVariable = ",lb.LocationCode";
                $location_cond = " AND (lb.LocationCode = '' OR lb.LocationCode IS NULL OR lb.LocationCode IN ('" . implode("','", $this->Get_Safe_Sql_Query($locationNotExist)) . "')) ";
                $locationJoinSql = " LEFT JOIN LIBMS_BOOK_UNIQUE lb ON lb.UniqueID = lsl.BookUniqueID";
            }
            if ($excludeWriteOffItems) {
                $recordStatus_cond = " AND lb.RecordStatus NOT IN ('DELETE', 'WRITEOFF') ";
                $locationJoinSql = " LEFT JOIN LIBMS_BOOK_UNIQUE lb ON lb.UniqueID = lsl.BookUniqueID INNER JOIN LIBMS_BOOK lbu ON lb.BookID = lbu.BookID";
            }
            // getNameFieldByLang('INTRANET_USER.')
            $userNameField = Get_Lang_Selection('lu.ChineseName', 'lu.EnglishName');
            $Sql = " SELECT
						DISTINCT lsl.StocktakeLogID, 
						lsl.StocktakeSchemeID, 
						lsl.BookID, 
						lsl.BookUniqueID,
						lsl.Result,					
						DATE_FORMAT(lsl.DateModified,'%Y-%m-%d %H:%i') AS DateModified, 
						lsl.LastModifiedBy,
						$userNameField AS UserName
						$locationVariable
				 FROM 
					 	LIBMS_STOCKTAKE_LOG lsl
						LEFT JOIN LIBMS_USER lu ON lu.UserID = lsl.LastModifiedBy
						$locationJoinSql
				 WHERE 1
				 		$stocktakeLogID_cond
						$stocktakeSchemeID_cond
						$bookUinqueID_cond
						$bookID_cond
						$location_cond
						$recordStatus_cond
						
				 ORDER BY
						lsl.DateModified DESC
				 ";
            $result = $this->returnArray($Sql);
            
            return $result;
        }

        function GET_STOCKTAKE_LOG_BOOK_INFO($barcode = '', $stocktakeSchemeID = '', $returnType = 'Array', $keyword = '', $ResourcesTypeCode = '', $CirculationTypeCode = '', $LocationCode = '', $order = false, $BookCategoryCode = '', $BookCategoryType = 0, $BookItemStatus = '', $getAccountDate=false)
        {
            global $Lang;
            
            $LocationName = Get_Lang_Selection("ll.DescriptionChi", "ll.DescriptionEn");
            $userNameField = Get_Lang_Selection('lu.ChineseName', 'lu.EnglishName');
            
            if ($barcode != '') {
                $barcode_cond = " AND lbu.BarCode=\"$barcode\" ";
            }
            
            if ($stocktakeSchemeID != '') {
                $stocktakeSchemeID_cond = "AND lsl.StocktakeSchemeID IN ('" . implode("','", (array) $stocktakeSchemeID) . "')";
            }
            
            // if($stocktakeStatus!=''){
            // $stocktakeStatus_cond = "AND lsl.Result =\"$stocktakeStatus\" ";
            // }
            
            $cond_keyword = '';
            if ($keyword != '') {
                $cond_keyword .= " 	AND 
								(
									lbu.BarCode Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
									OR
									lb.BookTitle Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
									OR
									$LocationName Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
									OR
									$userNameField Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
								)
							";
            }
            
            if ($ResourcesTypeCode) {
                $resourcesTypeCode_Cond = " AND (lb.ResourcesTypeCode IN ('" . implode("','", (array) $ResourcesTypeCode) . "') ";
                if (in_array('', (array) $ResourcesTypeCode)) {
                    $resourcesTypeCode_Cond .= " OR lb.ResourcesTypeCode IS NULL) ";
                } else {
                    $resourcesTypeCode_Cond .= ") ";
                }
            }
            
            if ($CirculationTypeCode) {
                $circulationTypeCode_Cond = "AND ((lbu.CirculationTypeCode<>'' AND lbu.CirculationTypeCode IS NOT NULL AND lbu.CirculationTypeCode IN ('" . implode("','", (array) $CirculationTypeCode) . "'))";
                $circulationTypeCode_Cond .= " OR (lb.CirculationTypeCode<>'' AND lb.CirculationTypeCode IS NOT NULL AND lb.CirculationTypeCode IN ('" . implode("','", (array) $CirculationTypeCode) . "') AND (lbu.CirculationTypeCode='' OR lbu.CirculationTypeCode IS NULL))";
                if (in_array('', (array) $CirculationTypeCode)) {
                    $circulationTypeCode_Cond .= " OR ((lb.CirculationTypeCode='' OR lb.CirculationTypeCode IS NULL) AND (lbu.CirculationTypeCode='' or lbu.CirculationTypeCode IS NULL))";
                }
                $circulationTypeCode_Cond .= ") ";
            } else {
                $circulationTypeCode_Cond = '';
            }
            if (($BookCategoryCode) && ($BookCategoryType != 0)) {
                $bookCategoryCode_Cond = " AND 	(lb.BookCategoryCode IN ('" . implode("','", (array) $BookCategoryCode) . "') ";
                $hasNullCatCode = 0;
                if (in_array('', (array) $BookCategoryCode)) {
                    $hasNullCatCode = 1;
                    $bookCategoryCode_Cond .= " OR lb.BookCategoryCode IS NULL) ";
                } else {
                    $bookCategoryCode_Cond .= ") ";
                }
                // Henry Added [20150122]
                if ($BookCategoryType != 0) {
                    // if(!$hasNullCatCode){
                    // //$bookCategoryCode_Cond .= " AND (lb.BookCategoryCode IN (SELECT C2.BookCategoryCode FROM LIBMS_BOOK_CATEGORY C2 WHERE C2.BookCategoryType='".$BookCategoryType."') ";
                    // $bookCategoryCode_Cond .= ' AND (lb.Language IN (SELECT lbl2.BookLanguageCode FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookCategoryType = "'.$BookCategoryType.'") OR '.$BookCategoryType.' = 1 AND (lb.Language IS NULL OR lb.Language = "")) ';
                    // }
                    // else{
                    // //$bookCategoryCode_Cond .= " OR lb.BookCategoryCode IS NULL OR lb.BookCategoryCode ='' ";
                    $bookCategoryCode_Cond .= ' AND (lb.Language IN (SELECT lbl2.BookLanguageCode FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookCategoryType = "' . $BookCategoryType . '") OR lb.Language IS NULL OR lb.Language = "") ';
                    // }
                    // $bookCategoryCode_Cond .= ") ";
                }
            }
            if ($LocationCode) {
                $locationCode_Cond = " AND 	(lbu.LocationCode IN ('" . implode("','", (array) $LocationCode) . "') ";
                if (in_array('', (array) $LocationCode)) {
                    // $locationCode_Cond .= " OR lbu.LocationCode IS NULL) ";
                    $locationNotExist = $this->GET_LOCATION_NOT_EXIST();
                    $locationCode_Cond .= " OR lbu.LocationCode IS NULL OR lbu.LocationCode IN ('" . implode("','", $this->Get_Safe_Sql_Query($locationNotExist)) . "')) ";
                } else {
                    $locationCode_Cond .= ") ";
                }
            }
            // Henry20140430
            if ($LocationCode == - 1) {
                $locationNotExist = $this->GET_LOCATION_NOT_EXIST();
                $locationCode_Cond = " AND (lbu.LocationCode = '' OR lbu.LocationCode IS NULL OR lbu.LocationCode IN ('" . implode("','", $this->Get_Safe_Sql_Query($locationNotExist)) . "')) ";
            }
            if ($order) {
                $order_Cond = "ORDER BY lbu.ACNO, lbu.BarCode";
            }
            
            if ($BookItemStatus != '') {
                $statusTable = ($returnType == 'Array') ? "lbu" : "lsl";
                $bookItemStatus_Cond = " AND " . $statusTable . ".RecordStatus='" . $BookItemStatus . "'";
            } else {
                $bookItemStatus_Cond = "";
            }
            $Sql = "SELECT 
					lbu.ACNO,";
            
            if ($getAccountDate) {
                $Sql .= "IF(lbu.CreationDate <> '' AND lbu.CreationDate<> '0000-00-00 00:00:00', DATE_FORMAT(lbu.CreationDate, '%Y-%m-%d'),'" . $Lang['General']['EmptySymbol'] . "') AS AccountDate,";
            }
            
            $Sql .= "lbu.BarCode,
					lb.BookTitle,
					IF ($LocationName IS NULL, '" . $Lang['General']['EmptySymbol'] . "', $LocationName) AS LocationName,";
            if ($returnType == 'Array') {
                $Sql .= "CASE TRIM(lbu.RecordStatus)
						    WHEN 'BORROWED' THEN '" . $Lang['libms']['book_status']['BORROWED'] . "'
						    WHEN 'LOST' THEN '" . $Lang['libms']['book_status']['LOST'] . "'
							WHEN 'NORMAL' THEN '" . $Lang['libms']['book_status']['NORMAL'] . "'
							WHEN 'REPAIRING' THEN '" . $Lang['libms']['book_status']['REPAIRING'] . "'
							WHEN 'RESERVED' THEN '" . $Lang['libms']['book_status']['RESERVED'] . "'
							WHEN 'SHELVING' THEN '" . $Lang['libms']['book_status']['SHELVING'] . "'
							WHEN 'WRITEOFF' THEN '" . $Lang['libms']['book_status']['WRITEOFF'] . "'
							WHEN 'SUSPECTEDMISSING' THEN '" . $Lang['libms']['book_status']['SUSPECTEDMISSING'] . "'
							WHEN 'DAMAGED' THEN '" . $Lang['libms']['book_status']['DAMAGED'] . "'
							WHEN 'RESTRICTED' THEN '" . $Lang['libms']['book_status']['RESTRICTED'] . "'
							WHEN 'ORDERING' THEN '" . $Lang['libms']['book_status']['ORDERING'] . "'
							ELSE '" . $Lang['General']['EmptySymbol'] . "'
						END AS CurrentRecordStatus,";
            }
            $Sql .= "CASE TRIM(lsl.RecordStatus)
					    WHEN 'BORROWED' THEN '" . $Lang['libms']['book_status']['BORROWED'] . "'
					    WHEN 'LOST' THEN '" . $Lang['libms']['book_status']['LOST'] . "'
						WHEN 'NORMAL' THEN '" . $Lang['libms']['book_status']['NORMAL'] . "'
						WHEN 'REPAIRING' THEN '" . $Lang['libms']['book_status']['REPAIRING'] . "'
						WHEN 'RESERVED' THEN '" . $Lang['libms']['book_status']['RESERVED'] . "'
						WHEN 'SHELVING' THEN '" . $Lang['libms']['book_status']['SHELVING'] . "'
						WHEN 'WRITEOFF' THEN '" . $Lang['libms']['book_status']['WRITEOFF'] . "'
						WHEN 'SUSPECTEDMISSING' THEN '" . $Lang['libms']['book_status']['SUSPECTEDMISSING'] . "'
						WHEN 'DAMAGED' THEN '" . $Lang['libms']['book_status']['DAMAGED'] . "'
						WHEN 'RESTRICTED' THEN '" . $Lang['libms']['book_status']['RESTRICTED'] . "'
						WHEN 'ORDERING' THEN '" . $Lang['libms']['book_status']['ORDERING'] . "'
						ELSE '" . $Lang['General']['EmptySymbol'] . "'
					END AS RecordStatus,
					CASE lsl.Result			
						WHEN 'FOUND' THEN '" . $Lang['libms']['stocktake_status']['FOUND'] . "'
						ELSE '---'
					END AS Result,
					DATE_FORMAT(lsl.DateModified,'%Y-%m-%d %H:%i') AS DateModified, 
					$userNameField AS UserName,
					CONCAT('<input type=\"checkbox\" name=\"BookIDUniqueID[]\" id=\"BookIDUniqueID\"  value=\"', lbu.BookID,'-', lbu.UniqueID,'-', lsl.StocktakeLogID,' \">') As CheckBox, 					
					lb.BookID, 
					lbu.UniqueID, 
					lbu.LocationCode,
					lb.ISBN,
					lb.CallNum,
					lb.CallNum2,
					lb.Publisher,
					lb.ResponsibilityBy1,
					lb.PublishYear,
					lb.Edition,
					lbu.ACNO AS BookCode,
					lbu.CreationDate,
					lbu.ListPrice,
					lbu.PurchasePrice
				FROM 
					LIBMS_BOOK_UNIQUE AS lbu
				INNER JOIN LIBMS_BOOK lb ON lb.BookID = lbu.BookID
				LEFT JOIN LIBMS_LOCATION ll ON ll.LocationCode = lbu.LocationCode
				LEFT JOIN LIBMS_STOCKTAKE_LOG lsl ON (lsl.BookUniqueID = lbu.UniqueID AND lsl.BookID = lbu.BookID)
				LEFT JOIN LIBMS_USER lu ON lu.UserID = lsl.LastModifiedBy
				LEFT JOIN LIBMS_BOOK_CATEGORY lbc ON lbc.BookCategoryCode = lb.BookCategoryCode
				WHERE 1
					AND lbu.RecordStatus NOT IN ('DELETE')
					$barcode_cond 
					$stocktakeSchemeID_cond
					$stocktakeStatus_cond									
					$resourcesTypeCode_Cond
					$circulationTypeCode_Cond
					$bookCategoryCode_Cond
					$locationCode_Cond
					$cond_keyword
					$bookItemStatus_Cond		
				GROUP BY lbu.UniqueID
				$order_Cond
				";
            
            if ($returnType == 'sql') {
                $result = $Sql;
            } else {
                $result = $this->returnArray($Sql);
            }
            return $result;
        }

        function GET_STOCKTAKE_LOG_BOOK_INFO_ORDER($barcode = '', $stocktakeSchemeID = '', $returnType = 'Array', $keyword = '', $ResourcesTypeCode = '', $CirculationTypeCode = '', $LocationCode = '', $order = false, $BookCategoryCode = '')
        {
            global $Lang;
            
            $LocationName = Get_Lang_Selection("ll.DescriptionChi", "ll.DescriptionEn");
            $userNameField = Get_Lang_Selection('lu.ChineseName', 'lu.EnglishName');
            
            if ($barcode != '') {
                $barcode_cond = " AND lbu.BarCode=\"$barcode\" ";
            }
            
            if ($stocktakeSchemeID != '') {
                $stocktakeSchemeID_cond = "AND lsl.StocktakeSchemeID IN ('" . implode("','", (array) $stocktakeSchemeID) . "')";
            }
            
            // if($stocktakeStatus!=''){
            // $stocktakeStatus_cond = "AND lsl.Result =\"$stocktakeStatus\" ";
            // }
            
            $cond_keyword = '';
            if ($keyword != '') {
                $cond_keyword .= " 	AND 
								(
									lbu.BarCode Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
									OR
									lb.BookTitle Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
									OR
									$LocationName Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
									OR
									$userNameField Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
								)
							";
            }
            
            if ($ResourcesTypeCode) {
                $resourcesTypeCode_Cond = " AND (lb.ResourcesTypeCode IN ('" . implode("','", (array) $ResourcesTypeCode) . "') ";
                if (in_array('', (array) $ResourcesTypeCode)) {
                    $resourcesTypeCode_Cond .= " OR lb.ResourcesTypeCode IS NULL) ";
                } else {
                    $resourcesTypeCode_Cond .= ") ";
                }
            }
            
            if ($CirculationTypeCode) {
                $circulationTypeCode_Cond = " AND (lb.CirculationTypeCode IN ('" . implode("','", (array) $CirculationTypeCode) . "') ";
                if (in_array('', (array) $CirculationTypeCode)) {
                    $circulationTypeCode_Cond .= " OR lb.CirculationTypeCode IS NULL) ";
                } else {
                    $circulationTypeCode_Cond .= ") ";
                }
            }
            if ($BookCategoryCode) {
                $bookCategoryCode_Cond = " AND 	(lb.BookCategoryCode IN ('" . implode("','", (array) $BookCategoryCode) . "') ";
                if (in_array('', (array) $BookCategoryCode)) {
                    $bookCategoryCode_Cond .= " OR lb.BookCategoryCode IS NULL) ";
                } else {
                    $bookCategoryCode_Cond .= ") ";
                }
            }
            if ($LocationCode) {
                $locationCode_Cond = " AND 	(lbu.LocationCode IN ('" . implode("','", (array) $LocationCode) . "') ";
                if (in_array('', (array) $LocationCode)) {
                    $locationNotExist = $this->GET_LOCATION_NOT_EXIST();
                    $locationCode_Cond .= " OR lbu.LocationCode IS NULL OR lbu.LocationCode IN ('" . implode("','", $this->Get_Safe_Sql_Query($locationNotExist)) . "')) ";
                } else {
                    $locationCode_Cond .= ") ";
                }
            }
            // Henry20140430
            if ($LocationCode == - 1) {
                $locationNotExist = $this->GET_LOCATION_NOT_EXIST();
                $locationCode_Cond = " AND (lbu.LocationCode = '' OR lbu.LocationCode IS NULL OR lbu.LocationCode IN ('" . implode("','", $this->Get_Safe_Sql_Query($locationNotExist)) . "')) ";
            }
            if ($order) {
                $order_Cond = "ORDER BY " . $order . "";
            }
            
            $Sql = "SELECT 
					lbu.ACNO,
					lbu.BarCode,
					lb.BookTitle,
					IF ($LocationName IS NULL, '" . $Lang['General']['EmptySymbol'] . "', $LocationName) AS LocationName,  
					CASE TRIM(lsl.RecordStatus)
					    WHEN 'BORROWED' THEN '" . $Lang['libms']['book_status']['BORROWED'] . "'
					    WHEN 'LOST' THEN '" . $Lang['libms']['book_status']['LOST'] . "'
						WHEN 'NORMAL' THEN '" . $Lang['libms']['book_status']['NORMAL'] . "'
						WHEN 'REPAIRING' THEN '" . $Lang['libms']['book_status']['REPAIRING'] . "'
						WHEN 'RESERVED' THEN '" . $Lang['libms']['book_status']['RESERVED'] . "'
						WHEN 'SHELVING' THEN '" . $Lang['libms']['book_status']['SHELVING'] . "'
						WHEN 'WRITEOFF' THEN '" . $Lang['libms']['book_status']['WRITEOFF'] . "'
						WHEN 'SUSPECTEDMISSING' THEN '" . $Lang['libms']['book_status']['SUSPECTEDMISSING'] . "'
						WHEN 'DAMAGED' THEN '" . $Lang['libms']['book_status']['DAMAGED'] . "'
						WHEN 'RESTRICTED' THEN '" . $Lang['libms']['book_status']['RESTRICTED'] . "'
						WHEN 'ORDERING' THEN '" . $Lang['libms']['book_status']['ORDERING'] . "'
						ELSE '" . $Lang['General']['EmptySymbol'] . "'
					END AS RecordStatus,
					CASE lsl.Result			
						WHEN 'FOUND' THEN '" . $Lang['libms']['stocktake_status']['FOUND'] . "'
						ELSE '---'
					END AS Result,
					DATE_FORMAT(lsl.DateModified,'%Y-%m-%d %H:%i') AS DateModified, 
					$userNameField AS UserName,
					CONCAT('<input type=\"checkbox\" name=\"BookIDUniqueID[]\" id=\"BookIDUniqueID\"  value=\"', lbu.BookID,'-', lbu.UniqueID,'-', lsl.StocktakeLogID,' \">') As CheckBox, 					
					lb.BookID, 
					lbu.UniqueID, 
					lbu.LocationCode,				
					lb.ISBN,
					lb.CallNum,
					lb.CallNum2,
					lb.Publisher,
					lb.ResponsibilityBy1,
					lb.PublishYear,
					lb.Edition,
					lbu.ACNO AS BookCode
				FROM 
					 LIBMS_STOCKTAKE_LOG as lsl		
				LEFT JOIN LIBMS_BOOK_UNIQUE lbu ON (lsl.BookUniqueID = lbu.UniqueID AND lsl.BookID = lbu.BookID)
				INNER JOIN LIBMS_BOOK lb ON lb.BookID = lbu.BookID
				LEFT JOIN LIBMS_LOCATION ll ON ll.LocationCode = lbu.LocationCode
				LEFT JOIN LIBMS_USER lu ON lu.UserID = lsl.LastModifiedBy
				LEFT JOIN LIBMS_BOOK_CATEGORY lbc ON lbc.BookCategoryCode = lb.BookCategoryCode
				WHERE 1
					AND lbu.RecordStatus NOT IN ('DELETE')
					$barcode_cond 
					$stocktakeSchemeID_cond
					$stocktakeStatus_cond									
					$resourcesTypeCode_Cond
					$circulationTypeCode_Cond
					$bookCategoryCode_Cond
					$locationCode_Cond
					$cond_keyword		
				GROUP BY lbu.UniqueID
				$order_Cond
				";
            
            if ($returnType == 'sql') {
                $result = $Sql;
            } else {
                $result = $this->returnArray($Sql);
            }
            return $result;
        }

        function GET_NOT_STOCKTAKE_BOOK_INFO($excludeUniqueID, $keyword = '', $ResourcesTypeCode = '', $CirculationTypeCode = '', $LocationCode = '', $returnType = 'sql', $order = false, $BookCategoryCode = '', $RecordEndDate = '', $RecordStatus = '', $BookCategoryType = 0, $get_borrower = false, $from_stock_take_process = false, $getAccountDate=false)
        {
            global $Lang;
            $LocationName = Get_Lang_Selection("ll.DescriptionChi", "ll.DescriptionEn");
            
            $cond_recordEndDate = "";
            if ($RecordEndDate != '')
                $cond_recordEndDate = " AND (lbu.CreationDate IS NULL OR lbu.CreationDate='0000-00-00 00:00:00' OR UNIX_TIMESTAMP(lbu.CreationDate) <= UNIX_TIMESTAMP('{$RecordEndDate} 23:59:59'))";
            
            $cond_keyword = '';
            if ($keyword != '') {
                $cond_keyword .= " 	AND 
								(
									lbu.BarCode Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
									OR
									lb.BookTitle Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
									OR
									$LocationName Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
								)
							";
            }
            
            if ($excludeUniqueID != '') {
                $excludeUniqueID_cond = " AND lbu.UniqueID NOT IN ('" . implode("','", (array) $excludeUniqueID) . "') ";
            }
            
            if ($ResourcesTypeCode != '') {
                $resourcesTypeCode_Cond = " AND (lb.ResourcesTypeCode IN ('" . implode("','", (array) $ResourcesTypeCode) . "') ";
                if (in_array('', (array) $ResourcesTypeCode)) {
                    $resourcesTypeCode_Cond .= " OR lb.ResourcesTypeCode IS NULL) ";
                } else {
                    $resourcesTypeCode_Cond .= ") ";
                }
            }
            if ($CirculationTypeCode != '') {
                $circulationTypeCode_Cond = "AND ((lbu.CirculationTypeCode<>'' AND lbu.CirculationTypeCode IS NOT NULL AND lbu.CirculationTypeCode IN ('" . implode("','", (array) $CirculationTypeCode) . "'))";
                $circulationTypeCode_Cond .= " OR (lb.CirculationTypeCode<>'' AND lb.CirculationTypeCode IS NOT NULL AND lb.CirculationTypeCode IN ('" . implode("','", (array) $CirculationTypeCode) . "') AND (lbu.CirculationTypeCode='' OR lbu.CirculationTypeCode IS NULL))";
                if (in_array('', (array) $CirculationTypeCode)) {
                    $circulationTypeCode_Cond .= " OR ((lb.CirculationTypeCode='' OR lb.CirculationTypeCode IS NULL) AND (lbu.CirculationTypeCode='' or lbu.CirculationTypeCode IS NULL))";
                }
                $circulationTypeCode_Cond .= ") ";
            } else {
                $circulationTypeCode_Cond = '';
            }
            
            if ($BookCategoryCode != '') {
                $bookCategoryCode_Cond = " AND 	(lb.BookCategoryCode IN ('" . implode("','", (array) $BookCategoryCode) . "') ";
                $hasNullCatCode = 0;
                if (in_array('', (array) $BookCategoryCode)) {
                    $hasNullCatCode = 1;
                    $bookCategoryCode_Cond .= " OR lb.BookCategoryCode IS NULL) ";
                } else {
                    $bookCategoryCode_Cond .= ") ";
                }
                // Henry Added [20150122]
                if ($BookCategoryType != 0) {
                    // if(!$hasNullCatCode){
                    // //$bookCategoryCode_Cond .= " AND (lb.BookCategoryCode IN (SELECT C2.BookCategoryCode FROM LIBMS_BOOK_CATEGORY C2 WHERE C2.BookCategoryType='".$BookCategoryType."') ";
                    // $bookCategoryCode_Cond .= ' AND (lb.Language IN (SELECT lbl2.BookLanguageCode FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookCategoryType = "'.$BookCategoryType.'") OR '.$BookCategoryType.' = 1 AND (lb.Language IS NULL OR lb.Language = "")) ';
                    // }
                    // else{
                    // //$bookCategoryCode_Cond .= " OR lb.BookCategoryCode IS NULL OR lb.BookCategoryCode ='' ";
                    $bookCategoryCode_Cond .= ' AND (lb.Language IN (SELECT lbl2.BookLanguageCode FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookCategoryType = "' . $BookCategoryType . '") OR lb.Language IS NULL OR lb.Language = "") ';
                    // }
                    // $bookCategoryCode_Cond .= ") ";
                }
            }
            if ($LocationCode != '') {
                $locationCode_Cond = " AND (lbu.LocationCode IN ('" . implode("','", (array) $LocationCode) . "') ";
                if (in_array('', (array) $LocationCode)) {
                    // $locationCode_Cond .= " OR lbu.LocationCode IS NULL) ";
                    $locationNotExist = $this->GET_LOCATION_NOT_EXIST();
                    $locationCode_Cond .= " OR lbu.LocationCode IS NULL OR lbu.LocationCode IN ('" . implode("','", $this->Get_Safe_Sql_Query($locationNotExist)) . "')) ";
                } else {
                    $locationCode_Cond .= ") ";
                }
            }
            if ($RecordStatus != '') {
                $recordStatus_Cond = " AND (lbu.RecordStatus IN ('" . implode("','", (array) $RecordStatus) . "') ";
                if (in_array('', (array) $RecordStatus)) {
                    $recordStatus_Cond .= " OR lbu.RecordStatus IS NULL) ";
                } else {
                    $recordStatus_Cond .= ") ";
                }
            }
            if ($order) {
                $order_Cond = "ORDER BY lbu.ACNO, lbu.BarCode";
            }
            
            if ($get_borrower) {
                $prefix = 'u.';
                $username_field = getNameFieldByLang($prefix, '', true);
                $borrowerName = "CONCAT($username_field,IF($prefix" . "ClassNumber IS NULL OR $prefix" . "ClassNumber = '','',CONCAT(' (',$prefix" . "ClassName,'-',$prefix" . "ClassNumber,')'))) AS Borrower,";
                $joinBorrower = " INNER JOIN LIBMS_BORROW_LOG bl ON bl.UniqueID=lbu.UniqueID AND bl.RecordStatus='BORROWED' INNER JOIN LIBMS_USER u ON u.UserID=bl.UserID ";
            } else {
                $borrowerName = '';
                $joinBorrower = '';
            }
            
            $callNumber = '';
            if ($from_stock_take_process) {
                $callNumber = "CONCAT_WS(  ' ', lb.`CallNum` , lb.`CallNum2` ) AS CallNumber,";
            }
            
            $Sql = "SELECT 
					lbu.ACNO,";

            if ($getAccountDate) {
                $Sql .= "IF(lbu.CreationDate <> '' AND lbu.CreationDate<> '0000-00-00 00:00:00', DATE_FORMAT(lbu.CreationDate, '%Y-%m-%d'),'" . $Lang['General']['EmptySymbol'] . "') AS AccountDate,";
            }
            
            $Sql .= "lbu.BarCode,
					{$callNumber}
					lb.BookTitle,
					IF ($LocationName IS NULL, '" . $Lang['General']['EmptySymbol'] . "', $LocationName) AS LocationName, 
					CASE lbu.RecordStatus
					    WHEN 'BORROWED' THEN '" . $Lang['libms']['book_status']['BORROWED'] . "'
					    WHEN 'LOST' THEN '" . $Lang['libms']['book_status']['LOST'] . "'
						WHEN 'NORMAL' THEN '" . $Lang['libms']['book_status']['NORMAL'] . "'
						WHEN 'REPAIRING' THEN '" . $Lang['libms']['book_status']['REPAIRING'] . "'
						WHEN 'RESERVED' THEN '" . $Lang['libms']['book_status']['RESERVED'] . "'
						WHEN 'SHELVING' THEN '" . $Lang['libms']['book_status']['SHELVING'] . "'
						WHEN 'WRITEOFF' THEN '" . $Lang['libms']['book_status']['WRITEOFF'] . "'
						WHEN 'SUSPECTEDMISSING' THEN '" . $Lang['libms']['book_status']['SUSPECTEDMISSING'] . "'
						WHEN 'DAMAGED' THEN '" . $Lang['libms']['book_status']['DAMAGED'] . "'
						WHEN 'RESTRICTED' THEN '" . $Lang['libms']['book_status']['RESTRICTED'] . "'
						WHEN 'ORDERING' THEN '" . $Lang['libms']['book_status']['ORDERING'] . "'
						ELSE '" . $Lang['General']['EmptySymbol'] . "'
					END AS RecordStatus,
					'" . $Lang['libms']['stocktake_status']['NOTTAKE'] . "' AS Result, {$borrowerName}
					'" . $Lang['General']['EmptySymbol'] . "' AS DateModified, 
					'" . $Lang['General']['EmptySymbol'] . "' AS UserName,
					CONCAT('<input type=\"checkbox\" name=\"BookIDUniqueID[]\" id=\"BookIDUniqueID\"  value=\"', lbu.BookID,'-', lbu.UniqueID,' \">') As CheckBox, 					
					lb.ISBN,
					lb.CallNum,
					lb.CallNum2,
					lb.Publisher,
					lb.ResponsibilityBy1,
					lb.PublishYear,
					lb.Edition,
					lb.BookID, 
					lbu.UniqueID, 
					lbu.LocationCode,
					lbu.ACNO AS BookCode,
					lbu.ListPrice,
					lbu.PurchasePrice
				FROM 
					LIBMS_BOOK_UNIQUE AS lbu $joinBorrower 
				INNER JOIN LIBMS_BOOK lb ON lb.BookID = lbu.BookID
				LEFT JOIN LIBMS_BOOK_CATEGORY lbc ON lbc.BookCategoryCode = lb.BookCategoryCode
				LEFT JOIN LIBMS_LOCATION ll ON ll.LocationCode = lbu.LocationCode
				WHERE 1
					AND (lbu.RecordStatus NOT IN ('DELETE','WRITEOFF') OR lbu.RecordStatus IS NULL) 
					$excludeUniqueID_cond			
					$cond_keyword
					$resourcesTypeCode_Cond
					$circulationTypeCode_Cond
					$bookCategoryCode_Cond
					$locationCode_Cond
					$recordStatus_Cond
					$cond_recordEndDate
					GROUP BY lbu.UniqueID
					$order_Cond
				 ";
            
            if ($returnType == 'sql') {
                $result = $Sql;
            } elseif ($returnType == 'array') {
                $result = $this->returnArray($Sql);
            }
            
            return $result;
        }

        /**
         * *****************
         * Stocktake End
         * ******************
         */
        
        /**
         * ************************
         * Stocktake Write-off Start
         * *************************
         */
        function ADD_WRITEOFF_LOG_RECORD($StocktakeSchemeID, $bookID, $bookUinqueID, $result = 'WRITEOFF', $previousBookStatus, $WriteoffReason = "", $WriteoffDate = "")
        {
            global $UserID;
            
            $Sql = " INSERT INTO LIBMS_WRITEOFF_LOG
				 (StocktakeSchemeID, BookID, BookUniqueID, Result, PreviousBookStatus, Reason, WriteOffDate, DateModified, LastModifiedBy)
				 VALUES 
					('" . $StocktakeSchemeID . "', 
					'" . $bookID . "' , 
					'" . $bookUinqueID . "' ,
					'" . $result . "',
					'" . $previousBookStatus . "',
					'" . $WriteoffReason . "',
					'" . $WriteoffDate . "',
					now(), 
					'" . $UserID . "')";
            
            $result = $this->db_db_query($Sql);
            return $result;
        }

        function UPDATE_WRITEOFF_LOG_RECORD($writeOffLogID, $StocktakeSchemeID, $bookID, $bookUinqueID, $result = 'WRITEOFF')
        {
            global $UserID;
            $Sql = " UPDATE 
					LIBMS_WRITEOFF_LOG
				 SET 
					StocktakeSchemeID = '" . $StocktakeSchemeID . "', 
					BookID = '" . $bookID . "', 
					BookUniqueID = '" . $bookUinqueID . "', 
					Result = '" . $result . "', 
					DateModified = now(),	
					LastModifiedBy = '" . $UserID . "' 							 
				 WHERE 
					WriteOffID = '" . $writeOffLogID . "'
				 ";
            $result = $this->db_db_query($Sql);
            return $result;
        }

        function DELETE_WRITEOFF_LOG_RECORD($writeOffLogID)
        {
            $Sql = " DELETE FROM 
					LIBMS_WRITEOFF_LOG						 
				 WHERE 
					WriteOffID = '" . $writeOffLogID . "' ";
            $result = $this->db_db_query($Sql);
            
            return $result;
        }

        function GET_WRITEOFF_LOG_INFO($stocktakeSchemeID, $bookID = '', $uniqueBookID = '', $returnType = 'Array', $keyword = '', $ResourcesTypeCode = '', $CirculationTypeCode = '', $LocationCode = '', $order = false, $orderBy = '', $BookCategoryCode = '', $StartDate = '', $EndDate = '', $BookCategoryType = 0, $BookItemStatus = '')
        {
            global $Lang;
            
            $userNameField = Get_Lang_Selection('lu.ChineseName', 'lu.EnglishName');
            $LocationName = Get_Lang_Selection("ll.DescriptionChi", "ll.DescriptionEn");
            
            $cond_keyword = '';
            if ($keyword != '') {
                $cond_keyword .= " 	AND 
								(
									lbu.BarCode Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
									OR
									lb.BookTitle Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
									OR
									$LocationName Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
									OR
									$userNameField Like '%" . $this->Get_Safe_Sql_Like_Query($keyword) . "%'
								)
							";
            }
            if ($stocktakeSchemeID != '') {
                $stocktakeSchemeID_cond = " AND lwl.StocktakeSchemeID ='" . $stocktakeSchemeID . "'";
            } else {
                $stocktakeSchemeID_cond = " AND lwl.StocktakeSchemeID IS NOT NULL ";
            }
            
            if ($bookID != '') {
                $bookID_cond = " AND lb.BookID = '" . $bookID . "' ";
            }
            
            if ($uniqueBookID != '') {
                $uniqueBookID_cond = " AND lbu.UniqueID = '" . $uniqueBookID . "' ";
            }
            
            if ($ResourcesTypeCode != '') {
                $resourcesTypeCode_Cond = " AND (lb.ResourcesTypeCode IN ('" . implode("','", (array) $ResourcesTypeCode) . "') ";
                if (in_array('', (array) $ResourcesTypeCode)) {
                    $resourcesTypeCode_Cond .= " OR lb.ResourcesTypeCode IS NULL) ";
                } else {
                    $resourcesTypeCode_Cond .= ") ";
                }
            }
            if ($CirculationTypeCode != '') {
                $circulationTypeCode_Cond = "AND ((lbu.CirculationTypeCode<>'' AND lbu.CirculationTypeCode IS NOT NULL AND lbu.CirculationTypeCode IN ('" . implode("','", (array) $CirculationTypeCode) . "'))";
                $circulationTypeCode_Cond .= " OR (lb.CirculationTypeCode<>'' AND lb.CirculationTypeCode IS NOT NULL AND lb.CirculationTypeCode IN ('" . implode("','", (array) $CirculationTypeCode) . "') AND (lbu.CirculationTypeCode='' OR lbu.CirculationTypeCode IS NULL))";
                if (in_array('', (array) $CirculationTypeCode)) {
                    $circulationTypeCode_Cond .= " OR ((lb.CirculationTypeCode='' OR lb.CirculationTypeCode IS NULL) AND (lbu.CirculationTypeCode='' or lbu.CirculationTypeCode IS NULL))";
                }
                $circulationTypeCode_Cond .= ") ";
            } else {
                $circulationTypeCode_Cond = '';
            }
            
            if (($BookCategoryCode != '') && ($BookCategoryType != 0)) {
                $bookCategoryCode_Cond = " AND 	(lb.BookCategoryCode IN ('" . implode("','", (array) $BookCategoryCode) . "') ";
                $hasNullCatCode = 0;
                if (in_array('', (array) $BookCategoryCode)) {
                    $hasNullCatCode = 1;
                    $bookCategoryCode_Cond .= " OR lb.BookCategoryCode IS NULL) ";
                } else {
                    $bookCategoryCode_Cond .= ") ";
                }
                // Henry Added [20150122]
                if ($BookCategoryType != 0) {
                    // if(!$hasNullCatCode){
                    // //$bookCategoryCode_Cond .= " AND (lb.BookCategoryCode IN (SELECT C2.BookCategoryCode FROM LIBMS_BOOK_CATEGORY C2 WHERE C2.BookCategoryType='".$BookCategoryType."') ";
                    // $bookCategoryCode_Cond .= ' AND (lb.Language IN (SELECT lbl2.BookLanguageCode FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookCategoryType = "'.$BookCategoryType.'") OR '.$BookCategoryType.' = 1 AND (lb.Language IS NULL OR lb.Language = "")) ';
                    // }
                    // else{
                    // //$bookCategoryCode_Cond .= " OR lb.BookCategoryCode IS NULL OR lb.BookCategoryCode ='' ";
                    $bookCategoryCode_Cond .= ' AND (lb.Language IN (SELECT lbl2.BookLanguageCode FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookCategoryType = "' . $BookCategoryType . '") OR lb.Language IS NULL OR lb.Language = "") ';
                    // }
                    // $bookCategoryCode_Cond .= ") ";
                }
            }
            if ($LocationCode != '') {
                $locationCode_Cond = " AND (lbu.LocationCode IN ('" . implode("','", (array) $LocationCode) . "') ";
                if (in_array('', (array) $LocationCode)) {
                    // $locationCode_Cond .= " OR lbu.LocationCode IS NULL) ";
                    $locationNotExist = $this->GET_LOCATION_NOT_EXIST();
                    $locationCode_Cond .= " OR lbu.LocationCode IS NULL OR lbu.LocationCode IN ('" . implode("','", $this->Get_Safe_Sql_Query($locationNotExist)) . "')) ";
                } else {
                    $locationCode_Cond .= ") ";
                }
            }
            // Henry20140430
            if ($LocationCode == - 1) {
                $locationNotExist = $this->GET_LOCATION_NOT_EXIST();
                $locationCode_Cond = " AND (lbu.LocationCode = '' OR lbu.LocationCode IS NULL OR lbu.LocationCode IN ('" . implode("','", $this->Get_Safe_Sql_Query($locationNotExist)) . "')) ";
            }
            if ($StartDate != '') {
                $startDate_Cond = " AND (UNIX_TIMESTAMP(lwl.DateModified) >= UNIX_TIMESTAMP('" . $StartDate . "') OR UNIX_TIMESTAMP(lwl.WriteOffDate) >= UNIX_TIMESTAMP('" . $StartDate . "') ) ";
            }
            if ($EndDate != '') {
                $endDate_Cond = " AND (UNIX_TIMESTAMP(lwl.DateModified) <= UNIX_TIMESTAMP('" . $EndDate . " 23:59:59') OR UNIX_TIMESTAMP(lwl.WriteOffDate) <= UNIX_TIMESTAMP('" . $EndDate . " 23:59:59') ) ";
            }
            if ($order) {
                $order_Cond = "ORDER BY lbu.ACNO, lbu.BarCode";
            }
            if ($orderBy != '') {
                $order_Cond = "ORDER BY " . $orderBy;
            }
            
            if ($BookItemStatus != '') {
                $bookItemStatus_Cond = " AND lbu.RecordStatus='" . $BookItemStatus . "'";
            } else {
                $bookItemStatus_Cond = "";
            }
            
            $Sql = "SELECT 
					lbu.ACNO,
					lbu.BarCode,
					lb.BookTitle,
					$LocationName AS LocationName, 
					CASE lbu.RecordStatus
					    WHEN 'BORROWED' THEN '" . $Lang['libms']['book_status']['BORROWED'] . "'
					    WHEN 'LOST' THEN '" . $Lang['libms']['book_status']['LOST'] . "'
						WHEN 'NORMAL' THEN '" . $Lang['libms']['book_status']['NORMAL'] . "'
						WHEN 'REPAIRING' THEN '" . $Lang['libms']['book_status']['REPAIRING'] . "'
						WHEN 'RESERVED' THEN '" . $Lang['libms']['book_status']['RESERVED'] . "'
						WHEN 'SHELVING' THEN '" . $Lang['libms']['book_status']['SHELVING'] . "'
						WHEN 'WRITEOFF' THEN '" . $Lang['libms']['book_status']['WRITEOFF'] . "'
						WHEN 'SUSPECTEDMISSING' THEN '" . $Lang['libms']['book_status']['SUSPECTEDMISSING'] . "'
						WHEN 'DAMAGED' THEN '" . $Lang['libms']['book_status']['DAMAGED'] . "'
						WHEN 'RESTRICTED' THEN '" . $Lang['libms']['book_status']['RESTRICTED'] . "'
						WHEN 'ORDERING' THEN '" . $Lang['libms']['book_status']['ORDERING'] . "'
						ELSE '---'
					END AS RecordStatus,
					CASE lwl.Result			
						WHEN 'WRITEOFF' THEN '" . $Lang['libms']['stocktake_status']['WRITEOFF'] . "'
						ELSE '---'
					END AS Result,
					IF(lwl.WriteOffDate IS NOT NULL AND lwl.WriteOffDate <> 0, DATE_FORMAT(lwl.WriteOffDate,'%Y-%m-%d'),DATE_FORMAT(lwl.DateModified,'%Y-%m-%d')) AS WriteOffDate,
					lwl.Reason AS Reason,
					DATE_FORMAT(lwl.DateModified,'%Y-%m-%d %H:%i') AS DateModified, 
					$userNameField AS UserName,
					CONCAT('<input type=\"checkbox\" name=\"BookIDUniqueID[]\" id=\"BookIDUniqueID[]\" value=\"', lbu.BookID,'-', lbu.UniqueID,'-', lwl.WriteOffID,' \">') As CheckBox, 					
					lb.BookID, 
					lbu.UniqueID, 
					lbu.LocationCode,
					lwl.WriteOffID,
					lwl.PreviousBookStatus,
					lb.ISBN,
					lb.CallNum,
					lb.CallNum2,
					lb.Publisher,
					lb.ResponsibilityBy1,
					lb.PublishYear,
					lb.Edition,
					lbu.ACNO AS BookCode,
					lbu.PurchaseByDepartment,
					lbu.ListPrice,
					lbu.Discount,
					lbu.PurchasePrice,
					IF(lbu.PurchaseDate IS NOT NULL AND lbu.PurchaseDate <> 0, DATE_FORMAT(lbu.PurchaseDate,'%Y-%m-%d'),'') AS PurchaseDate,
                    IF(lbu.CreationDate <> '' AND lbu.CreationDate<> '0000-00-00 00:00:00', DATE_FORMAT(lbu.CreationDate, '%Y-%m-%d'),'--') AS AccountDate
				FROM 
					LIBMS_BOOK_UNIQUE AS lbu
					INNER JOIN LIBMS_BOOK lb ON lb.BookID = lbu.BookID
					LEFT JOIN LIBMS_LOCATION ll ON ll.LocationCode = lbu.LocationCode
					LEFT JOIN LIBMS_WRITEOFF_LOG lwl ON (lwl.BookUniqueID = lbu.UniqueID AND lwl.BookID = lbu.BookID)
					LEFT JOIN LIBMS_USER lu ON lu.UserID = lwl.LastModifiedBy
				WHERE 1
					AND lbu.RecordStatus NOT LIKE 'DELETE'
					$stocktakeSchemeID_cond		
					$bookID_cond
					$uniqueBookID_cond		
					$resourcesTypeCode_Cond
					$circulationTypeCode_Cond
					$bookCategoryCode_Cond
					$startDate_Cond
					$endDate_Cond
					$locationCode_Cond
					$cond_keyword
					$bookItemStatus_Cond

				GROUP BY lbu.UniqueID
				$order_Cond
				";
            
            if ($returnType == 'Array') {
                $result = $this->returnArray($Sql);
            } else {
                $result = $Sql;
            }
            return $result;
        }

        /*
         * return book unique info with stocktake and WriteOffID
         */
        function GET_BOOK_INFO_WITH_STOCKTAKE_WRITEOFF($barcode = '', $stocktakeSchemeID = '', $excludeWriteOff = false)
        {
            global $Lang;
            
            $LocationName = Get_Lang_Selection("ll.DescriptionChi", "ll.DescriptionEn");
            
            $barcode_cond = $barcode ? " AND lbu.BarCode=\"$barcode\"" : "";
            $stocktakeSchemeID_cond = $stocktakeSchemeID ? " AND StocktakeSchemeID IN ('" . implode("','", (array) $stocktakeSchemeID) . "')" : "";
            $writeOff_cond = $excludeWriteOff ? " AND lwl.WriteOffID IS NULL" : "";
            
            $sql = "SELECT 
					lbu.ACNO,
					lbu.BarCode,
					lb.BookTitle,
					IF ($LocationName IS NULL, '" . $Lang['General']['EmptySymbol'] . "', $LocationName) AS LocationName,  
					lb.BookID, 
					lbu.UniqueID,
					lsl.RecordStatus,
					lsl.Result,
					lsl.StocktakeLogID,
					lwl.WriteOffID 
				FROM 
					LIBMS_BOOK_UNIQUE AS lbu
				INNER JOIN LIBMS_BOOK lb ON lb.BookID = lbu.BookID
				LEFT JOIN LIBMS_LOCATION ll ON ll.LocationCode = lbu.LocationCode
				LEFT JOIN (select StocktakeLogID, 
		                        StocktakeSchemeID, 
		                        BookUniqueID, 
		                        BookID, 
		                        Result,
								RecordStatus
		                  from  LIBMS_STOCKTAKE_LOG
		                  where 1
								$stocktakeSchemeID_cond) as lsl 
						ON (lsl.BookUniqueID = lbu.UniqueID AND lsl.BookID = lbu.BookID)
				LEFT JOIN LIBMS_WRITEOFF_LOG lwl ON (lwl.BookUniqueID = lbu.UniqueID AND lwl.BookID = lbu.BookID)									
				WHERE 1
					AND lbu.RecordStatus NOT IN ('DELETE')
					$barcode_cond
					$writeOff_cond
				GROUP BY lbu.UniqueID";
            
            $result = $this->returnArray($sql);
            
            return $result;
        }

        function UPDATE_UNIQUE_BOOK($bookUinqueID, $newRecordStatus)
        {
            global $UserID;
            
            $Sql = " UPDATE 
					LIBMS_BOOK_UNIQUE 
				 SET 
					RecordStatus = '" . $newRecordStatus . "',
					DateModified = now(),	
					LastModifiedBy = '" . $UserID . "' 							 
				 WHERE 
					UniqueID = '" . $bookUinqueID . "'
				 ";
            $result = $this->db_db_query($Sql);
            
            return $result;
        }

        function UPDATE_UNIQUE_BOOK_ID($bookUinqueID, $newBookID)
        {
            global $UserID;
            
            $Sql = " UPDATE 
					LIBMS_BOOK_UNIQUE 
				 SET 
					BookID = '" . $newBookID . "',
					DateModified = now(),	
					LastModifiedBy = '" . $UserID . "'						 
				 WHERE 
					UniqueID = '" . $bookUinqueID . "'
				 ";
            $result = $this->db_db_query($Sql);
            
            return $result;
        }

        function GET_UNIQUE_BOOK_STATUS($bookUinqueID, $bookID)
        {
            $Sql = " SELECT * 
				 FROM
					LIBMS_BOOK_UNIQUE 			 
				 WHERE 1
					AND UniqueID = '" . $bookUinqueID . "'
					AND BookID = '" . $bookID . "'
				 ";
            
            $result = $this->returnArray($Sql);
            return $result;
        }

        /**
         * ***********************
         * Stocktake Write-off End
         * ************************
         */
        function GET_BORROW_LOG($bookID = '', $uniqueID = '', $userID = '')
        {
            if ($bookID != '') {
                $bookID_cond = " AND lrl.BookID = '" . $bookID . "' ";
            }
            
            if ($uniqueID != '') {
                $uniqueID_cond = " AND lrl.UniqueID = '" . $uniqueID . "' ";
            }
            
            if ($userID != '') {
                $userID_cond = " AND lrl.UserID = '" . $userID . "' ";
            }
            
            $userNameField = Get_Lang_Selection('lu.ChineseName', 'lu.EnglishName');
            
            $Sql = " SELECT lrl.*, bu.ItemSeriesNum, $userNameField AS UserName
					FROM 
						LIBMS_BORROW_LOG lrl
					INNER JOIN LIBMS_BOOK_UNIQUE bu ON bu.UniqueID=lrl.UniqueID
						LEFT JOIN LIBMS_USER lu ON lu.UserID = lrl.UserID
					WHERE 1 AND lrl.RecordStatus = 'BORROWED'
						$bookID_cond
						$uniqueID_cond
						$userID_cond
				";
            
            $result = $this->returnArray($Sql);
            return $result;
        }

        // ################################################################################################################
        function GET_BOOK_CATEGORY_INFO($Code, $Type = '')
        {
            if ($Type != "") {
                $cond = " AND BookCategoryType=\"$Type\"";
            }
            $sql = "SELECT * FROM LIBMS_BOOK_CATEGORY WHERE BookCategoryCode=\"$Code\" $cond";
            return $this->returnArray($sql);
        }

        function ADD_BOOK_CATEGORY($dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            $sql = "INSERT IGNORE INTO LIBMS_BOOK_CATEGORY (";
            foreach ($fields as $field)
                $sql .= $field . ", ";
            $sql .= "DateModified, LastModifiedBy) values (";
            foreach ($values as $value)
                $sql .= "'" . $value . "', ";
            $sql .= "now(), $UserID)";
            $this->db_db_query($sql);
        }

        function UPDATE_BOOK_CATEGORY($Code = '', $dataAry = array())
        {
            global $UserID;
            
            $BookCategoryType = ($dataAry['BookCategoryType'] == 2) ? '2' : '1';
            // $oldCategory = $this->GET_BOOK_CATEGORY_INFO($Code, $BookCategoryType);
            
            foreach ($dataAry as $fields[] => $values[]);
            
            $sql = "UPDATE LIBMS_BOOK_CATEGORY SET ";
            foreach ($dataAry as $field => $value) {
                $sql .= $field . "=\"" . $value . "\", ";
                if ($field == "BookCategoryCode" && $Code != $value)
                    $this->UPDATE_BOOK_TYPES("BookCategoryCode", $Code, $value);
            }
            $sql .= "DateModified=now(), LastModifiedBy=$UserID";
            $sql .= " WHERE `BookCategoryCode`=\"$Code\"";
            $sql .= " AND BookCategoryType = '$BookCategoryType'";
            $this->db_db_query($sql);
            
            // if (count($oldCategory) > 0) {
            // $oldCategory = $oldCategory[0];
            // if (($oldCategory['BookCategoryCode'] != $Code) ||
            // ($oldCategory['DescriptionEn'] != $dataAry['DescriptionEn']) ||
            // ($oldCategory['DescriptionChi'] != $dataAry['DescriptionChi'])) {
            $newCategory = array();
            $newCategory['BookCategoryCode'] = $Code;
            $newCategory['DescriptionEn'] = stripslashes($dataAry['DescriptionEn']);
            $newCategory['DescriptionChi'] = stripslashes($dataAry['DescriptionChi']);
            $newCategory['BookCategoryType'] = $BookCategoryType;
            
            $libel = new elibrary();
            $libel->SYNC_BOOK_CATEGORY_TO_ELIB($newCategory);
            // }
            // }
            
            // dump($sql);
            // dex(mysql_error());
        }

        // function UPDATE_BOOK_CATEGORY($Code='',$dataAry=array())
        // {
        // global $UserID;
        // foreach($dataAry as $fields[]=>$values[]);
        //
        // $sql = "UPDATE LIBMS_BOOK_CATEGORY SET ";
        // foreach($dataAry as $field=>$value)
        // $sql .= $field . "=\"". $value."\", ";
        // $sql .= "DateModified=now(), LastModifiedBy=$UserID";
        // $sql .= " WHERE `BookCategoryCode`=\"$Code\"";
        // $this->db_db_query($sql);
        // //dump($sql);
        // //dex(mysql_error());
        // }
        function REMOVE_BOOK_CATEGORY($Codes = array(), $Type = '')
        {
            // global $UserID;
            if (is_array($Codes) && count($Codes) > 0) {
                $CodesList = '"' . implode('","', $Codes) . '"';
                if ($Type != "") {
                    $cond = " AND `BookCategoryType` = \"$Type\"";
                }
                
                $sql = "DELETE FROM `LIBMS_BOOK_CATEGORY` WHERE `BookCategoryCode` IN ($CodesList) $cond";
                $result = $this->db_db_query($sql);
                return $result;
                // dump($sql);
                // dex(mysql_error());
            } else {
                return false;
            }
        }

        // ################################################################################################################
        function GET_BOOK_LANGUAGE_INFO($Code)
        {
            $sql = "SELECT * FROM LIBMS_BOOK_LANGUAGE WHERE BookLanguageCode=\"$Code\"";
            return $this->returnArray($sql);
        }

        function ADD_BOOK_LANGUAGE($dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            $sql = "INSERT IGNORE INTO LIBMS_BOOK_LANGUAGE (";
            foreach ($fields as $field)
                $sql .= $field . ", ";
            $sql .= "DateModified, LastModifiedBy) values (";
            foreach ($values as $value)
                $sql .= "'" . $value . "', ";
            $sql .= "now(), $UserID)";
            $this->db_db_query($sql);
        }

        function UPDATE_BOOK_LANGUAGE($Code = '', $dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            
            $sql = "UPDATE LIBMS_BOOK_LANGUAGE SET ";
            foreach ($dataAry as $field => $value) {
                $sql .= $field . "=\"" . $value . "\", ";
                if ($field == "BookLanguageCode" && $Code != $value)
                    $this->UPDATE_BOOK_TYPES("Language", $Code, $value);
            }
            $sql .= "DateModified=now(), LastModifiedBy=$UserID";
            $sql .= " WHERE `BookLanguageCode`=\"$Code\"";
            $this->db_db_query($sql);
            // dump($sql);
            // dex(mysql_error());
        }

        function REMOVE_BOOK_LANGUAGE($Codes = array())
        {
            // global $UserID;
            if (is_array($Codes) && count($Codes) > 0) {
                $CodesList = '"' . implode('","', $Codes) . '"';
                
                $sql = "DELETE FROM `LIBMS_BOOK_LANGUAGE` WHERE `BookLanguageCode` IN ($CodesList) ";
                $result = $this->db_db_query($sql);
                return $result;
                // dump($sql);
                // dex(mysql_error());
            } else {
                return false;
            }
        }

        // ################################################################################################################
        function GET_LOCATION_INFO($Code = '', $Order = '')
        {
            if ($Code != '') {
                $location_cond = " AND LocationCode=\"$Code\" ";
            }
            
            if ($Order != '') {
                
                $order_cond = " ORDER BY LocationCode  ";
            }
            
            $sql = "SELECT 
					* 
				FROM 
					LIBMS_LOCATION 
				WHERE 1
					$location_cond
				$order_cond
				";
            return $this->returnArray($sql);
        }

        function ADD_LOCATION($dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            $sql = "INSERT IGNORE INTO LIBMS_LOCATION (";
            foreach ($fields as $field)
                $sql .= $field . ", ";
            $sql .= "DateModified, LastModifiedBy) values (";
            foreach ($values as $value)
                $sql .= "'" . $value . "', ";
            $sql .= "now(), $UserID)";
            $this->db_db_query($sql);
        }

        // function UPDATE_LOCATION($Code='',$dataAry=array())
        // {
        // global $UserID;
        //
        // foreach($dataAry as $fields[]=>$values[]);
        //
        // $sql = "UPDATE LIBMS_LOCATION SET ";
        // foreach($dataAry as $field=>$value)
        // $sql .= $field . "=\"". $value."\", ";
        // $sql .= "DateModified=now(), LastModifiedBy=$UserID";
        // $sql .= " WHERE `LocationCode`=\"$Code\"";
        // $this->db_db_query($sql);
        // //dump($sql);
        // //dex(mysql_error());
        // }
        function UPDATE_LOCATION($Code = '', $dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            
            $sql = "UPDATE LIBMS_LOCATION SET ";
            foreach ($dataAry as $field => $value) {
                $sql .= $field . "=\"" . $value . "\", ";
                if ($field == "LocationCode" && $Code != $value) {
                    $sql1 = "UPDATE LIBMS_BOOK_UNIQUE SET ";
                    $sql1 .= "LocationCode=\"" . $value . "\", ";
                    $sql1 .= "DateModified=now(), LastModifiedBy=$UserID";
                    $sql1 .= " WHERE `LocationCode`=\"$Code\"";
                    $this->db_db_query($sql1);
                }
            }
            $sql .= "DateModified=now(), LastModifiedBy=$UserID";
            $sql .= " WHERE `LocationCode`=\"$Code\"";
            $this->db_db_query($sql);
            // dump($sql);
            // dex(mysql_error());
        }

        function REMOVE_LOCATION($Codes = array())
        {
            // global $UserID;
            if (is_array($Codes) && count($Codes) > 0) {
                $CodesList = '"' . implode('","', $Codes) . '"';
                
                $sql = "DELETE FROM `LIBMS_LOCATION` WHERE `LocationCode` IN ($CodesList) ";
                $result = $this->db_db_query($sql);
                return $result;
                // dump($sql);
                // dex(mysql_error());
            } else {
                return false;
            }
        }

        function GET_LOCATION_NOT_EXIST()
        {
            $sql = 'select distinct LocationCode 
				from LIBMS_BOOK_UNIQUE lbu 
				where lbu.LocationCode NOT IN (
				select ll.LocationCode  From LIBMS_LOCATION ll)';
            
            return $this->returnVector($sql);
        }

        // ################################################################################################################
        function GET_CIRCULATION_TYPE_INFO($Code = '')
        {
            if ($Code != '') {
                $code_cond = "AND CirculationTypeCode=\"$Code\"";
            }
            $sql = "SELECT * FROM 
					LIBMS_CIRCULATION_TYPE 
				WHERE 1
					$code_cond ";
            // dump($this->returnArray($sql));
            return $this->returnArray($sql);
        }

        function UPDATE_BOOK_TYPES($Type, $Code, $NewCode)
        {
            global $UserID;
            $sql = "UPDATE LIBMS_BOOK SET ";
            $sql .= $Type . "=\"" . $NewCode . "\", ";
            $sql .= "DateModified=now(), LastModifiedBy=$UserID";
            $sql .= " WHERE `" . $Type . "`=\"$Code\"";
            $this->db_db_query($sql);
        }
        
        function UPDATE_ITEM_TYPES($Type, $Code, $NewCode)
        {
            global $UserID;
            $sql = "UPDATE LIBMS_BOOK_UNIQUE SET ";
            $sql .= $Type . "=\"" . $NewCode . "\", ";
            $sql .= "DateModified=now(), LastModifiedBy=$UserID";
            $sql .= " WHERE `" . $Type . "`=\"$Code\"";
            $this->db_db_query($sql);
        }

        function GET_CIRCULATION_TYPE_LIST($ExcludeCode)
        {
            global $Lang;
            $CirDescription = $Lang['libms']['SQL']['CirDescription'];
            $ExcludeCode[] = '-1';
            $code = implode("','", $ExcludeCode);
            $sql = "SELECT *, {$CirDescription} as CirDescription FROM LIBMS_CIRCULATION_TYPE WHERE CirculationTypeCode NOT IN ('{$code}')";
            // error_log($sql);
            $result = $this->returnArray($sql);
            // error_log(mysql_error());
            // error_log(print_r($result,true));
            return $result;
        }

        function ADD_CIRCULATION_TYPE($dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            $sql = "INSERT IGNORE INTO LIBMS_CIRCULATION_TYPE (";
            foreach ($fields as $field)
                $sql .= $field . ", ";
            $sql .= "DateModified, LastModifiedBy) values (";
            foreach ($values as $value)
                $sql .= "'" . $value . "', ";
            $sql .= "now(), $UserID)";
            $this->db_db_query($sql);
        }

        function UPDATE_CIRCULATION_TYPE($Code = '', $dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            
            $sql = "UPDATE LIBMS_CIRCULATION_TYPE SET ";
            foreach ($dataAry as $field => $value) {
                $sql .= $field . "=\"" . $value . "\", ";
                if ($field == "CirculationTypeCode" && $Code != $value){
                    $this->UPDATE_BOOK_TYPES("CirculationTypeCode", $Code, $value);
                    $this->UPDATE_ITEM_TYPES("CirculationTypeCode", $Code, $value);
                }
            }
            $sql .= "DateModified=now(), LastModifiedBy=$UserID";
            $sql .= " WHERE `CirculationTypeCode`=\"$Code\"";
            $this->db_db_query($sql);
            // dump($sql);
            // dex(mysql_error());
        }

        // function UPDATE_CIRCULATION_TYPE($Code='',$dataAry=array())
        // {
        // global $UserID;
        //
        // foreach($dataAry as $fields[]=>$values[]);
        //
        // $sql = "UPDATE LIBMS_CIRCULATION_TYPE SET ";
        // foreach($dataAry as $field=>$value)
        // $sql .= $field . "=\"". $value."\", ";
        // $sql .= "DateModified=now(), LastModifiedBy=$UserID";
        // $sql .= " WHERE `CirculationTypeCode`=\"$Code\"";
        // $this->db_db_query($sql);
        // //dump($sql);
        // //dex(mysql_error());
        // }
        function REMOVE_CIRCULATION_TYPE($Codes = array())
        {
            // global $UserID;
            if (is_array($Codes) && count($Codes) > 0) {
                $CodesList = '"' . implode('","', $Codes) . '"';
                
                $sql = "DELETE FROM `LIBMS_CIRCULATION_TYPE` WHERE `CirculationTypeCode` IN ($CodesList) ";
                $result = $this->db_db_query($sql);
                return $result;
                // dump($sql);
                // dex(mysql_error());
            } else {
                return false;
            }
        }

        // ################################################################################################################
        function GET_RESOURCES_TYPE_INFO($Code = '')
        {
            if ($Code != '') {
                $code_cond = " AND ResourcesTypeCode=\"$Code\" ";
            }
            
            $sql = "SELECT * FROM LIBMS_RESOURCES_TYPE 
				WHERE 1
				$code_cond ";
            return $this->returnArray($sql);
        }

        function ADD_RESOURCES_TYPE($dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            $sql = "INSERT IGNORE INTO LIBMS_RESOURCES_TYPE (";
            foreach ($fields as $field)
                $sql .= $field . ", ";
            $sql .= "DateModified, LastModifiedBy) values (";
            foreach ($values as $value)
                $sql .= "'" . $value . "', ";
            $sql .= "now(), $UserID)";
            $this->db_db_query($sql);
        }

        function UPDATE_RESOURCES_TYPE($Code = '', $dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            $sql = "UPDATE LIBMS_RESOURCES_TYPE SET ";
            foreach ($dataAry as $field => $value) {
                $sql .= $field . "=\"" . $value . "\", ";
                if ($field == "ResourcesTypeCode" && $Code != $value)
                    $this->UPDATE_BOOK_TYPES("ResourcesTypeCode", $Code, $value);
            }
            $sql .= "DateModified=now(), LastModifiedBy=$UserID";
            $sql .= " WHERE `ResourcesTypeCode`=\"$Code\"";
            
            $this->db_db_query($sql);
            // dump($sql);
            // dex(mysql_error());
        }

        // function UPDATE_RESOURCES_TYPE($Code='',$dataAry=array())
        // {
        // global $UserID;
        //
        // foreach($dataAry as $fields[]=>$values[]);
        //
        // # check if there is any change in the Code Field
        // # update the books when necessary
        //
        // $sql = "UPDATE LIBMS_RESOURCES_TYPE SET ";
        // foreach($dataAry as $field=>$value)
        // $sql .= $field . "=\"". $value."\", ";
        // $sql .= "DateModified=now(), LastModifiedBy=$UserID";
        // $sql .= " WHERE `ResourcesTypeCode`=\"$Code\"";
        // $this->db_db_query($sql);
        // //dump($sql);
        // //dex(mysql_error());
        // }
        function REMOVE_RESOURCES_TYPE($Codes = array())
        {
            // global $UserID;
            if (is_array($Codes) && count($Codes) > 0) {
                $CodesList = '"' . implode('","', $Codes) . '"';
                
                $sql = "DELETE FROM `LIBMS_RESOURCES_TYPE` WHERE `ResourcesTypeCode` IN ($CodesList) ";
                $result = $this->db_db_query($sql);
                return $result;
                // dump($sql);
                // dex(mysql_error());
            } else {
                return false;
            }
        }

        // ################################################################################################################
        function GET_RESPONSIBILITY_INFO($Code)
        {
            $sql = "SELECT * FROM LIBMS_RESPONSIBILITY WHERE ResponsibilityCode=\"$Code\"";
            return $this->returnArray($sql);
        }

        function ADD_RESPONSIBILITY($dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            $sql = "INSERT IGNORE INTO LIBMS_RESPONSIBILITY (";
            foreach ($fields as $field)
                $sql .= $field . ", ";
            $sql .= "DateModified, LastModifiedBy) values (";
            foreach ($values as $value)
                $sql .= "'" . $value . "', ";
            $sql .= "now(), $UserID)";
            $this->db_db_query($sql);
        }

        function UPDATE_RESPONSIBILITY($Code = '', $dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            
            $sql = "UPDATE LIBMS_RESPONSIBILITY SET ";
            foreach ($dataAry as $field => $value) {
                $sql .= $field . "=\"" . $value . "\", ";
                if ($field == "ResponsibilityCode" && $Code != $value) {
                    $this->UPDATE_BOOK_TYPES("ResponsibilityCode1", $Code, $value);
                    $this->UPDATE_BOOK_TYPES("ResponsibilityCode2", $Code, $value);
                    $this->UPDATE_BOOK_TYPES("ResponsibilityCode3", $Code, $value);
                }
            }
            $sql .= "DateModified=now(), LastModifiedBy=$UserID";
            $sql .= " WHERE `ResponsibilityCode`=\"$Code\"";
            $this->db_db_query($sql);
            // dump($sql);
            // dex(mysql_error());
        }

        function REMOVE_RESPONSIBILITY($Codes = array())
        {
            // global $UserID;
            if (is_array($Codes) && count($Codes) > 0) {
                $CodesList = '"' . implode('","', $Codes) . '"';
                
                $sql = "DELETE FROM `LIBMS_RESPONSIBILITY` WHERE `ResponsibilityCode` IN ($CodesList) ";
                $result = $this->db_db_query($sql);
                return $result;
                // dump($sql);
                // dex(mysql_error());
            } else {
                return false;
            }
        }

        // ################################################################################################################
        function BOOK_GETOPTION($table, $index, $display, $orderby, $wherestr = '')
        {
            global $Lang;
            
            if ($wherestr == '') {
                $sql = "SELECT " . $index . "," . $display . "  FROM " . $table . " order by " . $orderby;
            } else {
                $sql = "SELECT " . $index . "," . $display . "  FROM " . $table . " where " . $wherestr . " order by " . $orderby;
            }
            
            // $values = $this->returnArray($sql,null,1);
            $values = $this->returnArray($sql);
            
            return $values;
        }

        function SELECTVALUE($table, $field, $orderby, $wherestr = '1=1')
        {
            $sql = "SELECT " . $field . " FROM " . $table . " where " . $wherestr . " order by " . $orderby;
            $values = $this->returnArray($sql, null, 1);
            return $values;
        }

        function INSERT2DB($dataAry = array(), $table)
        {
            global $UserID;
            /*
             * foreach($dataAry as $fields[]=>$values[]);
             * $sql = "INSERT IGNORE INTO ". $table ." (";
             * foreach($fields as $field) $sql .= $field .", ";
             * $sql .= "DateModified, LastModifiedBy) values (";
             * foreach($values as $value) $sql .= "'". $value ."', ";
             * $sql .= "now(), $UserID)";
             */
            foreach ($dataAry as $fields => $values) {
                $fieldname .= $fields . ", ";
                
                // if($fields == 'CreationDate'){
                // # exclude single quote for the value of some fields
                // $value .= $values.", ";
                // } else {
//                $value .= "'" . $this->Get_Safe_Sql_Query($values) . "', ";
                $value .= "'" . $values . "', ";
                // }
            }
            $sql = "INSERT IGNORE INTO " . $table . " (" . $fieldname . " DateModified, LastModifiedBy) values 
				(" . $value . " now(), '" . $UserID . "') ";
            // echo $sql;
            $result = $this->db_db_query($sql);
            
            return $result;
        }

        function REMOVE_BOOK($Codes = array())
        {
            global $UserID;
            $result = array();
            if (is_array($Codes) && count($Codes) > 0) {
                $CodesList = '"' . implode('","', $Codes) . '"';
                $sql = "UPDATE `LIBMS_BOOK` SET `RecordStatus` = 'DELETE' , `BookCode_BAK`= `BookCode`, `BookCode` = NULL, 
		    			DateModified = now(), 
						LastModifiedBy = '" . $UserID . "' WHERE `BookID` IN ($CodesList); ";
                $result[] = $this->db_db_query($sql);
                $sql = "UPDATE `LIBMS_BOOK_UNIQUE` SET `RecordStatus` = 'DELETE', `Barcode_BAK`= `BarCode`, `BarCode` = NULL, `ACNO_BAK`= `ACNO`, `ACNO` = NULL, 
		    			DateModified = now(), 
						LastModifiedBy = '" . $UserID . "' WHERE `BookID` IN ($CodesList); ";
                $result[] = $this->db_db_query($sql);
                // $sql = "UPDATE `LIBMS_BOOK` SET `BookCode` = NULL WHERE `BookID` IN ($CodesList); ";
                // $this->db_db_query($sql);
            }
            return in_array(false, $result) ? false : true;
        }

        function UPDATE_BOOK_STATUS_IF_RESERVED($bookUinqueID, $newRecordStatus)
        {
            global $UserID, $Lang, $root_path_found, $intranet_session_language;
            
            // get the BookID by the BookUniqueID
            $sql = "SELECT BookID FROM LIBMS_BOOK_UNIQUE WHERE UniqueID = '" . $bookUinqueID . "'";
            list ($bookID) = current($this->returnArray($sql));
            
            // get the reserve row of the first waiting
            $sql = "Select UserID, ReservationID, BookID FROM `LIBMS_RESERVATION_LOG` WHERE BookID = '" . $bookID . "' AND RecordStatus = 'WAITING' ORDER BY ReserveTime asc";
            $reserve_row = current($this->returnArray($sql));
            
            if (! empty($reserve_row)) {
                $day_limit = $this->get_system_setting('max_book_reserved_limit');
                if ($day_limit > 0) {
                    include_once ("{$root_path_found}/home/library_sys/management/circulation/TimeManager.php");
                    $timeManager = new TimeManager(new liblms());
                    if ($this->get_system_setting("circulation_duedate_method")) {
                        if ($strToday == "") {
                            $strToday = date("Y-m-d");
                        }
                        $newExpiryDate = date('Y-m-d', $timeManager->findOpenDateSchoolDays(strtotime($strToday), $day_limit));
                    } else {
                        $newExpiryDate = date('Y-m-d', $timeManager->findOpenDate(strtotime('+' . $day_limit . ' day')));
                    }
                } else {
                    $newExpiryDate = "0000-00-00";
                }
                if ($newRecordStatus == 'NORMAL') {
                    $sql = "UPDATE LIBMS_BOOK_UNIQUE SET 
					    RecordStatus = 'RESERVED'
					    WHERE UniqueID = " . $bookUinqueID . "
						LIMIT 1";
                    $result[] = $this->db_db_query($sql);
                    
                    $sql = "UPDATE LIBMS_RESERVATION_LOG SET 
				    RecordStatus = 'READY', DateModified = now(), ExpiryDate = '" . $newExpiryDate . "'
				    WHERE ReservationID = '" . $reserve_row['ReservationID'] . "'";
                    $result[] = $this->db_db_query($sql);
                    
                    // check if need to send email
                    $result_reserve_email = $this->SELECTFROMTABLE('LIBMS_NOTIFY_SETTING', 'enable', array(
                        'name' => PHPTOSQL('reserve_email')
                    ), 1);
                    // tolog($result);
                    $reserve_email = $result_reserve_email[0][0];
                    if (! empty($reserve_email)) {
                        $result_title = $this->SELECTFROMTABLE('LIBMS_BOOK', 'BookTitle', array(
                            'BookID' => $reserve_row['BookID']
                        ), 1);
                        $title = $result_title[0][0];
                        $mail_to = array(
                            $reserve_row['UserID']
                        );
                        $mailSubject = $Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['subject'];
                        $mail_body = sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body'], $title);
                        $mail_body_with_due_date = sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body_with_due_date'], $title, $newExpiryDate);
                        if ($newExpiryDate == 0)
                            do_email($mail_to, $mailSubject, $mail_body);
                        else
                            do_email($mail_to, $mailSubject, $mail_body_with_due_date);
                    }
                } else
                    return true;
            } else {
                return true;
            }
            return in_array(false, $result) ? false : true;
        }

        // Codes are ReservationID
        function REMOVE_RESERVATION($Codes = array(), $notifyNextUser=false)
        {
            global $root_path_found, $intranet_session_language, $Lang;
            $result = array();
            if (is_array($Codes) && count($Codes) > 0) {
                // $CodesList = '"'.implode('","',$Codes).'"';
                // $sql = "UPDATE `LIBMS_RESERVATION_LOG` SET `RecordStatus` = 'DELETE' WHERE `ReservationID` IN ($CodesList); ";
                // tolog($sql);
                // $result[] = $this->db_db_query($sql);
                
                /*
                 * better not auto assign to others (2014-12-23)
                 * foreach($Codes as $aCode){
                 * $sql = "SELECT BookID from LIBMS_RESERVATION_LOG
                 * where `ReservationID` ='".$aCode."'";
                 * list($bookID) = current($this->returnArray($sql));
                 *
                 * $sql = "SELECT ReservationID FROM LIBMS_RESERVATION_LOG
                 * WHERE BookID = '".$bookID."' AND RecordStatus='WAITING'
                 * ORDER BY ReserveTime ASC LIMIT 1";
                 * list($reservation_id) = current($this->returnArray($sql));
                 *
                 * if(!$reservation_id){
                 * $sql = "UPDATE LIBMS_BOOK_UNIQUE SET
                 * RecordStatus = 'NORMAL'
                 * WHERE BookID=".$bookID." AND RecordStatus = 'RESERVED'
                 * LIMIT 1";
                 * $this->db_db_query($sql);
                 * }
                 * else{
                 * $sql = "SELECT `Value` FROM `LIBMS_SYSTEM_SETTING` WHERE name='max_book_reserved_limit'";
                 * $result1 = $this->returnArray($sql);
                 * include_once("{$root_path_found}/home/library_sys/management/circulation/TimeManager.php");
                 * $timeManager = new TimeManager(new liblms());
                 * if (empty($result1)){
                 * $day_limit = 0;
                 * }else
                 * $day_limit = $result1[0][0];
                 * if($day_limit > 0)
                 * $newExpiryDate = date('Y-m-d',$timeManager->findOpenDate(strtotime('+'.$day_limit.' day')));
                 * else
                 * $newExpiryDate = "0000-00-00";
                 * $sql = "UPDATE LIBMS_RESERVATION_LOG SET
                 * RecordStatus = 'READY', DateModified = now(), ExpiryDate = '".$newExpiryDate."'
                 * WHERE ReservationID = $reservation_id";
                 * $this->db_db_query($sql);
                 * }
                 * }
                 */
                
                // ## 2016-04-15: need to assign next user in waiting list to 'Ready'
                // ## but still need to update status in LIBMS_BOOK_UNIQUE and Number of copy in LIBMS_BOOK (2016-02-26)
                // ## BookUniqueID maybe null because when there're multiple users reserve the same book, do not know which copy
                // ## will be available first
                $AssignNextUser = array();
                foreach ($Codes as $aCode) {
                    $BookInfo = $this->SELECTFROMTABLE('LIBMS_RESERVATION_LOG', array(
                        'BookID',
                        'BookUniqueID'
                    ), array(
                        'ReservationID' => $aCode
                    ));
                    
                    $nextUserID = '';
                    if (count($BookInfo) > 0) {
                        $BookID = $BookInfo[0]['BookID'];
                        $BookUniqueID = $BookInfo[0]['BookUniqueID'];
                        
                        $rs = $this->getNextUserOfBookReservation($BookID, $Codes);
                        
                        if (count($rs) && (! isset($AssignNextUser[$BookID]) || ! $AssignNextUser[$BookID])) {
                            $AssignNextUser[$BookID] = true;
                            $DoAssignNextUser = true;
                            $reservation_id = $rs[0]['ReservationID'];
                            $nextUserID = $rs[0]['UserID'];
                        } else {
                            $DoAssignNextUser = false;
                            $reservation_id = '';
                        }
                    } else {
                        $BookID = 0;
                        $BookUniqueID = 0;
                    }
                    
                    $sql = "UPDATE `LIBMS_RESERVATION_LOG` SET `RecordStatus` = 'DELETE' WHERE `ReservationID`='$aCode'";
                    // tolog($sql);
                    $result[] = $this->db_db_query($sql);
                    
                    if ($BookID) {
                        if ($BookUniqueID) {
                            $recordStatus = "'WAITING'";
                            $bookUniqueCond = " AND UniqueID='" . $BookUniqueID . "'";
                        } else {
                            $recordStatus = "'READY','WAITING'";
                            $bookUniqueCond = "";
                        }
                        
                        $sql = "SELECT ReservationID FROM LIBMS_RESERVATION_LOG
					    WHERE BookID = '" . $BookID . "' AND RecordStatus in (" . $recordStatus . ")
					    ORDER BY ReserveTime ASC LIMIT 1";
                        $rs = $this->returnResultSet($sql);
                        
                        if (count($rs) == 0) { // no ready and waiting record, release book item status to normal
                            $sql = "UPDATE LIBMS_BOOK_UNIQUE SET 
						    RecordStatus = 'NORMAL'
						    WHERE BookID='" . $BookID . "' AND RecordStatus = 'RESERVED'" . $bookUniqueCond;
                            $result[] = $this->db_db_query($sql);
                        } else {
                            if ($DoAssignNextUser) { // assign next ready user
                                $sql = "SELECT `Value` FROM `LIBMS_SYSTEM_SETTING` WHERE name='max_book_reserved_limit'";
                                $result1 = $this->returnResultSet($sql);
                                include_once ("{$root_path_found}/home/library_sys/management/circulation/TimeManager.php");
                                $timeManager = new TimeManager(new liblms());
                                if (empty($result1)) {
                                    $day_limit = 0;
                                } else
                                    $day_limit = $result1[0]['Value'];
                                if ($day_limit > 0)
                                    $newExpiryDate = date('Y-m-d', $timeManager->findOpenDate(strtotime('+' . $day_limit . ' day')));
                                else
                                    $newExpiryDate = "0000-00-00";
                                $sql = "UPDATE LIBMS_RESERVATION_LOG SET 
							    RecordStatus = 'READY', DateModified = now(), ExpiryDate = '" . $newExpiryDate . "'";
                                if ($BookUniqueID) {
                                    $sql .= ", BookUniqueID='" . $BookUniqueID . "' ";
                                }
                                $sql .= " WHERE ReservationID = '$reservation_id'";
                                $result[] = $this->db_db_query($sql);
                                
                                if ($notifyNextUser && $nextUserID) {
                                    $sql = "SELECT BookTitle FROM LIBMS_BOOK WHERE BookID='".$BookID."'";
                                    $bookAry = $this->returnResultSet($sql);
                                    $bookTitle = count($bookAry) ? $bookAry[0]['BookTitle'] : '';
                                    
                                    if ($newExpiryDate == '0000-00-00') {
                                        $emailBody = sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body'], $bookTitle);
                                    }
                                    else {
                                        $emailBody= sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body_with_due_date'], $bookTitle, $newExpiryDate);
                                    }
                                    
                                    do_email(array(
                                        $notifyNextUser
                                    ), $Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['subject'], $emailBody);
                                    
                                }
                            }
                        }
                    }
                }
            }
            return (in_array(false, $result)) ? false : true;
        }

        /**
         * take an array of bookID and return their book info in $rs[BookID][infoARRAY]
         * *
         */
        function GET_BOOK_INFO_MULTI($Codes)
        {
            if (! empty($Codes)) {
                if (! is_array($Codes)) {
                    $Codes = array(
                        $Codes
                    );
                }
                // prepare In statement
                $CodesCond = '';
                foreach ($Codes as $Code) {
                    $CodesCond .= "'{$Code}',";
                }
                $CodesCond = rtrim($CodesCond, ',');
                
                $sql = "SELECT * FROM LIBMS_BOOK WHERE BookID in ($CodesCond)";
                $rs = $this->returnResultSet($sql);
                
                // Result Set
                $result = array();
                foreach ($rs as $book) {
                    $result[$book['BookID']] = $book;
                }
            }
            return $result;
        }

        function GET_BOOK_INFO($Code)
        {
            $sql = "SELECT * FROM LIBMS_BOOK WHERE BookID=\"$Code\" ";
            return $this->returnArray($sql);
        }

        function GET_BOOK_INFO_BY_FILTER($orderby, $filter = '1=1')
        {
            global $Lang;
            $sql = "SELECT b.*,c." . $Lang["libms"]["sql_field"]["Description"] . " AS CirculationTypeDesc FROM LIBMS_BOOK b
				LEFT JOIN LIBMS_CIRCULATION_TYPE c ON c.CirculationTypeCode=b.CirculationTypeCode
				WHERE " . $filter . " order by " . $orderby;
            
            return $this->returnArray($sql);
        }

        function GET_BARCODE($Code)
        {
            $sql = "SELECT * FROM LIBMS_BOOK_UNIQUE WHERE BookID=\"$Code\" AND RecordStatus NOT LIKE 'DELETE' ";
            return $this->returnArray($sql);
        }

        function NEW_BOOK_RECORD($dataAry, $BookCover = '', $BookTag = '')
        {
            global $intranet_root, $Lang;
            
            $successAry = array();
            if (! empty($BookCover)) {
                
                $count = $this->SELECTVALUE('LIBMS_BOOK', 'max(`BookID`) as max', 'max');
                // fixed by increasing by 1
                $book_id_predicted = $count[0][max] + 1;
                $count = (int) ($count[0][max] / 2000);
                require ($intranet_root . '/home/library_sys/admin/book/import/api/class.upload.php');
                $handle = new Upload($_FILES['BookCover']);
                
                if ($handle->uploaded) {
                    // $handle->Process($intranet_root."/home/library_sys/admin/book/import/tmp/");
                    // $handle->Process("./import/pear/file/lms_book_import/");
                    /*
                     * if (!(is_dir($PATH_WRT_ROOT."../intranetdata/file/lms/cover/".$count))){
                     * mkdir($PATH_WRT_ROOT."../intranetdata/file/lms/cover/".$count, 0775, true);
                     * }
                     * $handle->Process($PATH_WRT_ROOT."../intranetdata/file/lms/cover/".$count."/");
                     */
                    $cover_folder = $intranet_root . "/file/lms/cover/" . $count . "/" . $book_id_predicted;
                    $handle->Process($cover_folder);
                    
                    if ($handle->processed) {
                        // everything was fine !
                        // $handle->file_dst_name . '">' . $handle->file_dst_name . '</a>';
                        $uploadSuccess = true;
                        // $cover_image = str_replace("../../../../../intranetdata", "", $handle->file_dst_pathname);
                        $image_obj = new SimpleImage();
                        $image_obj->load($handle->file_dst_pathname);
                        $image_obj->resizeToMax(140, 200);
                        $image_obj->save($handle->file_dst_pathname);
                        $cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
                    } else {
                        // one error occured
                        $xmsg = $Lang['libms']['import_book']['upload_fail'] . $handle->error . $Lang['libms']['import_book']['contact_admin'];
                        $uploadSuccess = false;
                        echo $xmsg;
                    }
                    $successAry[] = $uploadSuccess;
                    $dataAry['CoverImage'] = $cover_image;
                    
                    // we delete the temporary files
                    $handle->Clean();
                }
            }
            
            // upload new end///////////////////////////////////////
            
            // //////////////////start insert table "LIBMS_htmlspecialcharsBOOK"////////////////////
            // dex($dataAry);
            
            foreach ((array) $dataAry as $key => $feild) {
                if ($feild == '') {
                    unset($dataAry[$key]);
                }
            }
            
            $this->INSERT2DB($dataAry, 'LIBMS_BOOK');
            
            // tolog (mysql_error());
            $book_id = mysql_insert_id();
            // tolog ("book_new: ".$book_id);
            
            // //////////////////end insert table "LIBMS_BOOK"////////////////////
            
            // //////////////////start insert table "LIBMS_BOOK_TAG"////////////////////
            if ($BookTag != '') {
                if (phpversion_compare('5.2') != 'ELDER') {
                    $BookTag = stripslashes($BookTag);
                }
                $TagArray = json_decode($BookTag);
                
                // foreach($TagArray as &$field)
                foreach ((array) $TagArray as $field) {
                    $field = mysql_real_escape_string(stripcslashes(newline2space($field)));
                    $tag_id = $this->SELECTVALUE("LIBMS_TAG", "TagID", "TagID", "TagName = '" . $field . "'");
                    
                    if (empty($tag_id)) {
                        $newTag['TagName'] = $field;
                        $this->INSERT2DB($newTag, 'LIBMS_TAG');
                        $tag_id = $this->SELECTVALUE("LIBMS_TAG", "TagID", "TagID", "TagName = '" . $field . "'");
                    }
                    $newBookTag['BookID'] = $book_id;
                    $newBookTag['TagID'] = $tag_id[0]['TagID'];
                    $this->INSERT2DB($newBookTag, 'LIBMS_BOOK_TAG');
                }
            }
            // //////////////////end insert table "LIBMS_BOOK_TAG"////////////////////
            
            // //////////////////start insert table "LIBMS_BOOK_UNIQUE"////////////////////
            $libel = new elibrary();
            $libel->SYNC_PHYSICAL_BOOK_TO_ELIB($book_id);
            // //////////////////end insert table "LIBMS_BOOK_UNIQUE"////////////////////
            
            return $book_id;
        }

        function UPDATE_BOOK($table, $Code = '', $dataAry = array(), $withQuote = true)
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            
            $sql = "UPDATE $table SET ";
            if ($withQuote) {
                foreach ($dataAry as $field => $value) {
                    $sql .= $field . "=\"" . $value . "\", ";
                }
            } else {
                foreach ($dataAry as $field => $value) {
                    $sql .= $field . "=" . $value . ", ";
                }
            }
            $sql .= "DateModified=now(), LastModifiedBy='$UserID'";
            $sql .= " WHERE `BookID`=\"$Code\"";
            $this->db_db_query($sql);
        }

        function UPDATE_BOOK_UNIQUE($table, $Code = '', $dataAry = array(), $withQuote = true)
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            
            $sql = "UPDATE $table SET ";
            if ($withQuote) {
                foreach ($dataAry as $field => $value) {
                    $sql .= $field . "=\"" . $value . "\", ";
                }
            } else {
                foreach ($dataAry as $field => $value) {
                    if ($value == "'NOW()'") {
                        $value = "NOW()";
                    }
                    $sql .= $field . "=" . $value . ", ";
                }
            }
            $sql .= "DateModified=now(), LastModifiedBy='$UserID'";
            $sql .= " WHERE `UniqueID`=\"$Code\"";
            $this->db_db_query($sql);
        }

        function UPDATE_BOOK_LOCATION($bookUniqueID, $locationCode)
        {
            global $UserID;
            
            $sql = "UPDATE LIBMS_BOOK_UNIQUE SET LocationCode='{$locationCode}', DateModified=now(), LastModifiedBy='{$UserID}' WHERE UniqueID='{$bookUniqueID}' ";
            
            return $this->db_db_query($sql);
        }

        function CLEAR_RECORD($TABLE, $WHERESTR = '1=1')
        {
            $sql = "DELETE FROM $TABLE WHERE $WHERESTR";
            return $this->db_db_query($sql);
        }

        function COUNT_BOOK_RECORD()
        {
            $sql = "SELECT Count(*) as count FROM LIBMS_BOOK";
            return $this->returnArray($sql);
        }

        // #####################################################################################################
        // # Book Item
        function GET_BOOK_ITEM_INFO($UniqueID)
        {
            $returnArr = array();
            if ($UniqueID != '') {
                $sql = "SELECT 
						UniqueID, ACNO, ACNO_BAK, ACNO_Prefix, ACNO_Num, BarCode, BookID, BorrowLogID, RecordStatus, 
						DateModified, LastModifiedBy, InvoiceNumber, Distributor, Discount, PurchaseDate, 
						PurchasePrice, PurchaseNote, PurchaseByDepartment, ListPrice, LocationCode, 
						if(CreationDate <> '' and CreationDate<> '0000-00-00 00:00:00', DATE_FORMAT(CreationDate, '%Y-%m-%d'), '') as CreationDate ,
						PeriodicalItemID, AccompanyMaterial, RemarkToUser, ItemSeriesNum, CirculationTypeCode
					FROM LIBMS_BOOK_UNIQUE 
					WHERE UniqueID = '" . $UniqueID . "' ";
                $returnArr = $this->returnArray($sql);
            }
            return $returnArr;
        }

        function UPDATE_BOOK_ITEM($table, $UniqueID, $dataAry = array())
        {
            global $UserID;
            $result = '';
            if ($UniqueID != '') {
                $sql = "UPDATE LIBMS_BOOK_UNIQUE SET
						ACNO = '" . $dataAry['ACNO'] . "',
						ACNO_Prefix = '" . $dataAry['ACNO_Prefix'] . "', 
						ACNO_Num = '" . $dataAry['ACNO_Num'] . "',
						BarCode = '" . $dataAry['BarCode'] . "', 
						RecordStatus = '" . $dataAry['RecordStatus'] . "', 
						Distributor = '" . $dataAry['Distributor'] . "', 
						Discount = '" . $dataAry['Discount'] . "', 
						PurchaseDate = '" . $dataAry['PurchaseDate'] . "', 
						PurchasePrice = '" . $dataAry['PurchasePrice'] . "', 
						PurchaseNote = '" . $dataAry['PurchaseNote'] . "', 
						PurchaseByDepartment = '" . $dataAry['PurchaseByDepartment'] . "', 
						ItemSeriesNum = '" . $dataAry['ItemSeriesNum'] . "', 	
						ListPrice = '" . $dataAry['ListPrice'] . "', 
		    			DateModified = now(), 
						LastModifiedBy = '" . $UserID . "' 
					WHERE 
						UniqueID = '" . $UniqueID . "' ";
                $result = $this->db_db_query($sql);
            }
            return $result;
        }

        function REMOVE_BOOK_ITEM($UniqueIDArr = array())
        {
            global $UserID;
            $result = '';
            if (is_array($UniqueIDArr) && count($UniqueIDArr) > 0) {
                $unique_id_list = implode(",", $UniqueIDArr);
                
                $sql = "UPDATE `LIBMS_BOOK_UNIQUE` SET 
						`RecordStatus` = 'DELETE' , 
						`Barcode_BAK`= `BarCode`, 
						`BarCode` = NULL, 
						`ACNO_BAK`= `ACNO`, 
						`ACNO` = NULL, 
		    			DateModified = now(), 
						LastModifiedBy = '" . $UserID . "'  
					WHERE `UniqueID` IN ($unique_id_list); ";
                $result = $this->db_db_query($sql);
            }
            return $result;
        }

        function UPDATE_BOOK_COPY_INFO($BookID)
        {
            $result = '';
            if ($BookID != '') {
                $sql = "UPDATE LIBMS_BOOK b SET  
						`NoOfCopyAvailable` = ( 
							SELECT COUNT( * ) FROM  `LIBMS_BOOK_UNIQUE` bu
							WHERE bu.`BookID` = '" . $BookID . "' AND 
								bu.`RecordStatus` IN ('NORMAL',  'SHELVING', 'RESERVED') 
						), 
						`NoOfCopy` = ( 
							SELECT COUNT( * ) FROM  `LIBMS_BOOK_UNIQUE` bu
							WHERE bu.`BookID` = '" . $BookID . "' AND 
								bu.`RecordStatus` NOT IN ('LOST',  'WRITEOFF',  'SUSPECTEDMISSING', 'DELETE')
						)
					WHERE b.BookID = '" . $BookID . "' ";
                $result = $this->db_db_query($sql);
                
                // Remove the reservation of the book if the NoOfCopy = 0 (Henry)
                $tempArr = $this->GET_BOOK_INFO($BookID);
                if ($tempArr[0]["NoOfCopy"] == 0) {
                    $sql = "UPDATE LIBMS_RESERVATION_LOG SET 
				    RecordStatus = 'DELETE', DateModified = now(), ExpiryDate = 0
				    WHERE BookID = '" . $BookID . "' AND (RecordStatus='WAITING' OR RecordStatus='READY')";
                    $result2 = $this->db_db_query($sql);
                    
                    $result = $result && $result2;
                }
                // syn physical book to eBook table
                $libel = new elibrary();
                $libel->SYNC_PHYSICAL_BOOK_TO_ELIB($BookID);
            }
            return $result;
        }

        function GET_BOOK_ITEM_SELECTION($ParTags = '', $ParDefault = "", $ParSelected = "", $FristIndexText = "", $ExcludedStatusKeyArr = array())
        {
            global $Lang, $linterface;
            $BookStatusArr = array();
            if ($FristIndexText == "")
                $BookStatusArr[] = array(
                    "",
                    "-- {$Lang['libms']['book']['select']['Book_Status']} --"
                );
            else
                $BookStatusArr[] = array(
                    "",
                    "-- {$FristIndexText} --"
                );
            if (! empty($Lang['libms']['book_status'])) {
                foreach ($Lang['libms']['book_status'] as $key => $value) {
                    if (! in_array($key, (array) $ExcludedStatusKeyArr))
                        $BookStatusArr[] = array(
                            $key,
                            $value
                        );
                }
            }
            $str = $linterface->GET_SELECTION_BOX($BookStatusArr, $ParTags, $ParDefault, $ParSelected);
            return $str;
        }

        // #####################################################################################################
        
        // Update a table
        // $table = "table_name" ;
        // $dataAry = array( 'field1' => 'data1', field2 => 'data2');
        // $condition = array( 'field1' => 'data1', field2 => 'data2');
        //
        function UPDATE2TABLE($table, $dataAry = array(), $condition = array(), $NotUpdateDateModified = false)
        {
            global $UserID;
            if (! is_array($dataAry)) {
                
                return false;
            }
            
            // foreach($dataAry as $data['fields'][]=>$data['values'][]);
            // foreach($condition as $cond['fields'][]=>$cond['values'][]);
            
            $sql = "UPDATE `{$table}` SET ";
            
            foreach ($dataAry as $field => $value) {
//                $sql .= "`" . $field . "`= '" . $this->Get_Safe_Sql_Query($value) . "', ";
                $sql .= "`" . $field . "`= " . $value . " , ";
            }
            
            $sql .= " `LastModifiedBy`='{$UserID}'";
            if (! $NotUpdateDateModified) {
                $sql .= " , `DateModified`=now() ";
            }
            
            if (! empty($condition)) {
                foreach ($condition as $field => $value) {
//                    $tmp_cond[] = "`{$field}` = '".$this->Get_Safe_Sql_Query($value)."'";
                    $tmp_cond[] = "`{$field}` = {$value}";
                }
                $sql .= " WHERE " . implode(' AND ', $tmp_cond);
            }
            $result = $this->db_db_query($sql);
            
            return $result;
        }

        // #####################################################################################################
        
        // Update a table
        // $table = "table_name" ;
        // $dataAry = array( 'field1' => 'data1', field2 => 'data2');
        // $condition = array( 'field1' => 'data1', field2 => 'data2');
        //
        function REPLACE2TABLE($table, $dataAry = array(), $condition = array())
        {
            global $UserID;
            if (! is_array($dataAry)) {
                
                return false;
            }
            
            // foreach($dataAry as $data['fields'][]=>$data['values'][]);
            // foreach($condition as $cond['fields'][]=>$cond['values'][]);
            
            $sql = "REPLACE `{$table}` SET ";
            
            foreach ($dataAry as $field => $value) {
                $sql .= "`" . $field . "`= " . $value . " , ";
            }
            $sql .= "`DateModified`=now(), `LastModifiedBy`='{$UserID}'";
            if (! empty($condition)) {
                foreach ($condition as $field => $value) {
                    $tmp_cond[] = "`{$field}` = {$value}";
                }
                $sql .= " WHERE " . implode(' AND ', $tmp_cond);
            }
            
            $result = $this->db_db_query($sql);
            $err = mysql_error();
            
            if (! empty($err)) {
                trigger_error($error_msg);
            }
            return $result;
        }

        // Update a table
        // $table = "table_name" ;
        // $dataAry = array( 'field1' => 'data1', field2 => 'data2');
        // $condition = array( 'field1' => 'data1', field2 => 'data2');
        //
        function INSERT2TABLE($table, $dataAry = array(), $condition = array(), $insertIgnore = false)
        {
            global $UserID, $is_debug;
            
            // foreach($dataAry as $data['fields'][]=>$data['values'][]);
            // foreach($condition as $cond['fields'][]=>$cond['values'][]);
            if ($insertIgnore)
                $INGORE = " INGORE ";
            
            $sql = "INSERT {$INGORE} INTO `{$table}` SET ";
            
            foreach ($dataAry as $field => $value)
                $sql .= "`" . $field . "`= " . $value . " , ";
            
            $sql .= "`DateModified`=now(), `LastModifiedBy`='{$UserID}'";
            if (! empty($condition)) {
                foreach ($condition as $field => $value)
                    $tmp_cond[] = "`{$field}` = {$value}";
                $sql .= " WHERE " . implode(' AND ', $tmp_cond);
            }
            if ($is_debug) {
                echo $sql . '<br>';
            } else {
                $result = $this->db_db_query($sql);
            }
            // tolog($sql);
            // tolog(mysql_error());
            return $result;
        }

        /**
         * SELECT FROM table
         *
         * @param unknown_type $table
         *            = 'table_name';
         * @param unknown_type $fields
         *            = array( 'field1', 'field2' ); || $fields = 'Feildname'
         * @param unknown_type $condition
         *            = array( 'field1' => 'data1', 'field2' => 'data2');
         * @param unknown_type $limits
         *            array (['offset' , ] count );
         */
        function SELECTFROMTABLE($table, $fields = '*', $condition = null, $limits = null, $mode = 0, $sortBy = null, $sortOrder = 'ASC')
        {
            global $UserID;
            
            $sql = "SELECT ";
            if (is_array($fields))
                $sql .= " `" . implode("`,`", $fields) . "` ";
            else if (empty($fields) || $fields == '*')
                $sql .= " * ";
            else
                $sql .= " `{$fields}` ";
            
            $sql .= " FROM `{$table}` ";
            if (! empty($condition)) {
                foreach ($condition as $field => $value) {
                    if (is_int($field)) {
                        $tmp_cond[] = $value;
                    } else {
                        $tmp_cond[] = "`{$field}` = {$value}";
                    }
                }
                
                $sql .= " WHERE " . implode(' AND ', $tmp_cond);
            }
            
            if (! empty($sortBy)) {
                if (! is_array($sortBy)) {
                    $sortBy = array(
                        $sortBy
                    );
                }
                $sql .= " ORDER BY `" . implode(',', $sortBy) . "` " . $sortOrder;
            }
            
            if (! empty($limits)) {
                if (is_array($limits))
                    $sql .= " LIMIT {$limits[0]},{$limits[1]}";
                else
                    $sql .= " LIMIT {$limits}";
            }
            
            $result = $this->returnArray($sql, null, $mode);
            // dump($sql);
            return $result;
        }

        // #####################################################################################################
        function GET_OPEN_TIME()
        {
            $sql = "SELECT * FROM LIBMS_OPEN_TIME";
            return $this->returnArray($sql);
        }

        function UPDATE_OPENTIME($Code = '', $dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            
            $sql = "UPDATE LIBMS_OPEN_TIME SET ";
            foreach ($dataAry as $field => $value)
                if ($value == "NULL")
                    $sql .= $field . "=" . $value . ", ";
                else
                    $sql .= $field . "=\"" . $value . "\", ";
            $sql .= "DateModified=now(), LastModifiedBy=$UserID";
            $sql .= " WHERE `OpenTimeID`=\"$Code\"";
            $this->db_db_query($sql);
            // dump($sql);
            // dex(mysql_error());
        }

        // #####################################################################################################
        function ADD_SPECIAL_OPENTIME($dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            $sql = "INSERT IGNORE INTO LIBMS_OPEN_TIME_SPECIAL (";
            foreach ($fields as $field)
                $sql .= $field . ", ";
            $sql .= "DateModified, LastModifiedBy) values (";
            foreach ($values as $value)
                if ($value == "NULL")
                    $sql .= $value . ", ";
                else
                    $sql .= "'" . $value . "', ";
            $sql .= "now(), $UserID)";
            $this->db_db_query($sql);
            // dump($sql);
            // dex(mysql_error());
        }

        function UPDATE_SPECIAL_OPENTIME($dataAry = array())
        {
            global $UserID;
            foreach ($dataAry as $records) {
                foreach ($records as $field => $value) {
                    $sql = "UPDATE LIBMS_OPEN_TIME_SPECIAL SET ";
                    if ($field == 'Code')
                        $Code = $value;
                    else {
                        if ($value == "NULL")
                            $sql .= $field . "=" . $value . ", ";
                        else
                            $sql .= $field . "=\"" . $value . "\", ";
                    }
                    $sql .= "DateModified=now(), LastModifiedBy=$UserID";
                    $sql .= " WHERE `OpenTimeSpecialID`=\"$Code\"";
                    $this->db_db_query($sql);
                }
            }
        }

        function REMOVE_SPECIAL_OPENTIME($Codes = array())
        {
            // global $UserID;
            if (is_array($Codes) && count($Codes) > 0) {
                $CodesList = '"' . implode('","', $Codes) . '"';
                
                $sql = "DELETE FROM `LIBMS_OPEN_TIME_SPECIAL` WHERE `OpenTimeSpecialID` IN ($CodesList) ";
                $this->db_db_query($sql);
                // dump($sql);
                // dex(mysql_error());
            }
        }

        // ####################################################################################################
        function ADD_LABEL_FORMAT($dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            $sql = "INSERT IGNORE INTO LIBMS_LABEL_FORMAT (";
            foreach ($fields as $field)
                $sql .= "`" . $field . "`, ";
            $sql .= "DateModified, LastModifiedBy) values (";
            foreach ($values as $value)
                if ($value == "NULL")
                    $sql .= $value . ", ";
                else
                    $sql .= "'" . $value . "', ";
            $sql .= "now(), $UserID)";
            $this->db_db_query($sql);
            // debug_pr($sql);
            // dex(mysql_error());
        }

        function GET_LABEL_FORMAT($Code)
        {
            $sql = "SELECT * FROM LIBMS_LABEL_FORMAT WHERE id=\"$Code\"";
            return $this->returnArray($sql);
        }

        function UPDATE_LABEL_FORMAT($Code, $dataAry = array())
        {
            global $UserID;
            
            foreach ($dataAry as $fields[] => $values[]);
            $sql = "UPDATE LIBMS_LABEL_FORMAT SET ";
            foreach ($dataAry as $field => $value)
                if ($value == "NULL")
                    $sql .= "`" . $field . "`" . "=" . $value . ", ";
                else
                    $sql .= "`" . $field . "`" . "=\"" . $value . "\", ";
            $sql .= "DateModified=now(), LastModifiedBy=$UserID";
            $sql .= " WHERE `id`=\"$Code\"";
            $this->db_db_query($sql);
            // dump($sql);
            // dex(mysql_error());
        }

        function REMOVE_LABEL_FORMAT($Codes = array())
        {
            // global $UserID;
            if (is_array($Codes) && count($Codes) > 0) {
                $CodesList = '"' . implode('","', $Codes) . '"';
                
                $sql = "DELETE FROM `LIBMS_LABEL_FORMAT` WHERE `id` IN ($CodesList) ";
                $this->db_db_query($sql);
                // dump($sql);
                // dex(mysql_error());
            }
        }

        // #####################################################################################################
        function GET_SPECIALOPENTIME_INFO($Code)
        {
            $sql = "SELECT * FROM LIBMS_OPEN_TIME_SPECIAL WHERE OpenTimeSpecialID in ({$Code})";
            return $this->returnArray($sql);
        }

        function isitchecked($key, $input)
        {
            if ($key == $input) {
                return " checked ";
            } else {
                return "";
            }
        }

        function isitselected($key, $input)
        {
            if ($key == $input) {
                return " selected ";
            } else {
                return "";
            }
        }

        function get_book_manage_sub_tab($FromPage, $CurTab, $BookID, $UniqueID = '')
        {
            global $Lang;
            $para = '&FromPage=' . $FromPage . '&UniqueID=' . $UniqueID . '&clearCoo=1';
            $x = '<div class="shadetabs">
				<ul>';
            $Selected = ($CurTab == "Book") ? 'selected' : '';
            $x .= '  <li class="' . $Selected . ' SubMenu">
					<a href="book_edit.php?Code=' . $BookID . $para . '">' . $Lang['libms']['bookmanagement']['book_details'] . '</a>
				   </li>';
            $Selected = ($CurTab == "Item") ? 'selected' : '';
            if ($FromPage == 'item') {
                $x .= '  <li class="' . $Selected . ' SubMenu">
					<a href="book_item_edit.php?BookID=' . $BookID . $para . '">' . $Lang['libms']['action']['item'] . '</a>
				   </li>';
            } else {
                $x .= '  <li class="' . $Selected . ' SubMenu">
					<a href="book_item_list.php?BookID=' . $BookID . $para . '">' . $Lang['libms']['action']['item'] . '</a>
				   </li>';
            }
            
            $Selected = ($CurTab == "InfoPurchased") ? 'selected' : '';
            $x .= '  <li class="' . $Selected . ' SubMenu">
					<a href="info_purchase.php?BookID=' . $BookID . $para . '">' . $Lang['libms']['bookmanagement']['purchase_details'] . '</a>
				   </li>';
            $Selected = ($CurTab == "CirculationDetails") ? 'selected' : '';
            $x .= '  <li class="' . $Selected . ' SubMenu">
					<a href="circulation_details.php?BookID=' . $BookID . $para . '">' . $Lang['libms']['bookmanagement']['circulation_details'] . '</a>
				   </li>';
            $x .= ' </ul>
		</div>';
            return $x;
        }

        function get_book_manage_cur_page($FromPage)
        {
            if (strtolower($FromPage) == 'item') {
                $CurrentPage = "PageBookManagementItemList";
            } else if (strtolower($FromPage) == 'periodical') {
                $CurrentPage = "PagePeriodicalManagement";
            } else {
                $CurrentPage = "PageBookManagementBookList";
            }
            return $CurrentPage;
        }

        function get_book_manage_cancel_button($BookID, $FromPage, $TargetUrl = '')
        {
            global $button_cancel, $linterface;
            
            $btnHtml = '';
            if (strtolower($FromPage) == 'periodical') {
                $bookInfoAry = $this->GET_BOOK_INFO($BookID);
                $periodicalBookId = $bookInfoAry[0]['PeriodicalBookID'];
                
                $btnHtml = $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='../periodical/index.php?task=registrationList&PeriodicalBookID=$periodicalBookId'", "CancelBtn", "");
            } else {
                if ($TargetUrl == '') {
                    $TargetUrl = 'book_item_list.php';
                }
                $btnHtml = $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='" . $TargetUrl . "?FromPage=" . $FromPage . "&BookID=" . $BookID . "'", "CancelBtn", "");
            }
            
            return $btnHtml;
        }

        function get_ebook_tabs($CurrentTag)
        {
            global $Lang;
            $TAGS_OBJ = array();
            $TagNow[$CurrentTag] = true;
            $TAGS_OBJ[] = array(
                $Lang['libms']['bookmanagement']['ebook_license'],
                "elib_setting_license.php",
                $TagNow["LICENSE"]
            );
            $TAGS_OBJ[] = array(
                $Lang['libms']['bookmanagement']['ebook_tags'],
                "elib_setting_tags.php",
                $TagNow["TAGS"]
            );
            
            return $TAGS_OBJ;
        }

        function get_batch_edit_tabs($CurrentTag)
        {
            global $Lang;
            $TAGS_OBJ = array();
            $TagNow[$CurrentTag] = true;
            $TAGS_OBJ[] = array(
                $Lang['libms']['batch_edit']['edit_return_date'],
                "batch_modify_1.php",
                $TagNow["DUEDATE"]
            );
            $TAGS_OBJ[] = array(
                $Lang['libms']['batch_edit']['edit_tags'],
                "batch_modify_tags.php",
                $TagNow["TAGS"]
            );
            $TAGS_OBJ[] = array(
                $Lang['libms']['batch_edit']['edit_books'],
                "batch_modify_bibliography.php",
                $TagNow["BIBLIOGRAPHY"]
            );
            $TAGS_OBJ[] = array(
                $Lang['libms']['batch_edit']['edit_items'],
                "batch_modify_item.php",
                $TagNow["ITEMDETAILS"]
            );
            $TAGS_OBJ[] = array(
                $Lang['libms']['batch_edit']['edit_recommend'],
                "batch_recommend_book_list.php",
                $TagNow["RECOMMEND"]
            );
            
            // $TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['item_list'] , "batch_modify_item.php", $TagNow["ITEM"]);
            // $TAGS_OBJ[] = array($Lang['libms']['batch_edit']['edit_subject'] , "batch_modify_subject.php", $TagNow["SUBJECT"]);
            // $TAGS_OBJ[] = array($Lang['libms']['batch_edit']['edit_penal_sum'], "batch_modify_2.php", 0);
            // $TAGS_OBJ[] = array($Lang['libms']['batch_edit']['edit_book_info'], "batch_modify_3.php", 0);
            
            return $TAGS_OBJ;
        }

        function getReserveBookTabs($currentTag)
        {
            global $Lang;
            $TAGS_OBJ = array();
            $TagNow[$currentTag] = true;
            $TAGS_OBJ[] = array(
                $Lang['libms']['bookmanagement']['book_reserve'],
                "reserve_book.php",
                $TagNow["BOOKRESERVE"]
            );
            $TAGS_OBJ[] = array(
                $Lang['libms']['bookmanagement']['expired_book_reserve'],
                "expired_reserve_book.php",
                $TagNow["EXPIRE"]
            );
            $TAGS_OBJ[] = array(
                $Lang['libms']['bookmanagement']['available_reserve'],
                "available_reserve_book.php",
                $TagNow["AVAILABLE"]
            );
            
            return $TAGS_OBJ;
        }
        
        function addTag($TagName)
        {
            global $UserID;
            
            $TagName = addslashes($TagName);
            
            $sql = "SELECT TagID FROM LIBMS_TAG WHERE TagName='{$TagName}'";
            $rows = $this->returnVector($sql);
            
            if ($rows[0] != "" && $rows[0] > 0) {
                $tag_id = $rows[0];
            } else {
                $sql = "INSERT INTO LIBMS_TAG (TagName, DateModified, LastModifiedBy) VALUES ('{$TagName}', now(), '$UserID')";
                $result = $this->db_db_query($sql);
                
                $tag_id = $this->db_insert_id();
            }
            
            return $tag_id;
        }

        function addBookTag($BookID, $TagID)
        {
            global $UserID;
            
            $sql = "SELECT TagID FROM LIBMS_BOOK_TAG WHERE BookID='{$BookID}' AND TagID='{$TagID}'";
            $rows = $this->returnVector($sql);
            
            if (sizeof($rows) == 0) {
                $sql = "INSERT INTO LIBMS_BOOK_TAG (BookID, TagID, DateModified, LastModifiedBy) VALUES ('{$BookID}', '{$TagID}', now(), '$UserID')";
                $result = $this->db_db_query($sql);
                
                return $this->db_insert_id();
            }
        }

        function updateRemoveTags($TagIDs, $NewTag = "")
        {
            // get related bookID
            $sql = "SELECT DISTINCT BookID FROM LIBMS_BOOK_TAG WHERE TagID IN ({$TagIDs}) ";
            $BookArray = $this->returnVector($sql);
            
            // delete the book and tag relationship
            $sql = "DELETE FROM LIBMS_BOOK_TAG WHERE TagID IN ({$TagIDs}) ";
            $this->db_db_query($sql);
            
            // remove the tag
            $sql = "DELETE FROM LIBMS_TAG WHERE TagID IN ({$TagIDs}) ";
            $this->db_db_query($sql);
            
            // add new tag
            if (trim($NewTag) != "") {
                // debug("Add tag ".$NewTag);
                $new_tag_id = $this->addTag($NewTag);
                // debug("tag ID = ".$new_tag_id);
            }
            
            if (is_array($BookArray) && sizeof($BookArray) > 0) {
                for ($i = 0; $i < sizeof($BookArray); $i ++) {
                    if ($new_tag_id != "" && $new_tag_id > 0) {
                        // add new tag to book
                        $this->addBookTag($BookArray[$i], $new_tag_id);
                        // debug("add new tag ({$new_tag_id}) to book ".$BookArray[$i]);
                    }
                }
                
                $book_ids = implode(", ", $BookArray);
                
                // syn physical book to eBook table
                $libel = new elibrary();
                $libel->SYNC_PHYSICAL_BOOK_TO_ELIB($book_ids);
                // debug("update physical books ".$book_ids);
            }
            
            return true;
        }

        function get_fine_report($RecordType, $RecordStatus, $RecordDate, $StartDate, $EndDate, $FindBy, $groupTarget, $rankTarget, $classnames, $studentIDs, $Task = "DISPLAY", $RecordPeriod = "")
        {
            global $Lang, $junior_mck;
            
            if ($RecordPeriod == "FINEHANDLEDATE") {
                $sql_cond .= " UNIX_TIMESTAMP(lol.DateModified)>=UNIX_TIMESTAMP('{$StartDate}') AND UNIX_TIMESTAMP(lol.DateModified)<=UNIX_TIMESTAMP('{$EndDate} 23:59:59') ";
            } else {
                $sql_cond .= " UNIX_TIMESTAMP(lol.DateCreated)>=UNIX_TIMESTAMP('{$StartDate}') AND UNIX_TIMESTAMP(lol.DateCreated)<=UNIX_TIMESTAMP('{$EndDate} 23:59:59') ";
            }
            
            if ($RecordType == "LOST") {
                $sql_cond .= " AND lbl.RecordStatus='LOST' AND lol.DaysCount IS NULL ";
            } elseif ($RecordType == "OVERDUE") {
                $sql_cond .= " AND lol.DaysCount IS NOT NULL ";
            }
            
            $sql_cond2 = "";
            if ($RecordStatus != "" && $RecordStatus != "ALL") {
                
                if ($RecordStatus == 'OUTSTANDING') {
                    $statusFilter = " AND (bal.TransDesc IS NULL OR bal.TransDesc NOT IN ('pay overdue','waive overdue'))";
                    $sql_cond .= " AND lol.RecordStatus='{$RecordStatus}' ";
                    $sql_cond2 .= $statusFilter;
                }
                else if ($RecordStatus == 'SETTLED') {
                    $statusFilter = " AND ((bal.TransDesc IS NULL AND lol.RecordStatus='SETTLED') OR (bal.TransDesc='pay overdue' AND lol.RecordStatus IN ('OUTSTANDING','SETTLED','WAIVED')))";
                    $sql_cond2 .= $statusFilter;
                }
                else if ($RecordStatus == 'WAIVED') {
                    $statusFilter = " AND (bal.TransDesc IS NULL OR bal.TransDesc IN ('waive overdue'))";
                    $sql_cond .= " AND lol.RecordStatus='{$RecordStatus}' ";
                    $sql_cond2 .= $statusFilter;
                }
                else {
                    $statusFilter = " AND (bal.TransDesc IS NULL OR bal.TransDesc<>'additional charge adjustment') ";
                }
            }
            else {
                $statusFilter = " AND (bal.TransDesc IS NULL OR bal.TransDesc<>'additional charge adjustment') ";
            }
            
            if ($RecordDate == "FINEDATE") {
                $order_cond = "DateCreated ASC, DateModified ASC, PaymentReceived DESC";      // PaymentReceived can be used to place outstanding record as last row for the same borrowed book
            } elseif ($RecordDate == "FINEHANDLEDATE") {
                $order_cond = "DateModified ASC, PaymentReceived DESC";
            }
            
            // user filter
            $condition_clause .= " AND lu.`RecordStatus`<>0"; // case #P116646
            
            if (($FindBy == 'Group') && (! empty($groupTarget))) {
                
                $impoded_groupTarget = implode("','", $groupTarget);
                $condition_clause .= " AND lu.UserID IN (SELECT UserID FROM LIBMS_GROUP_USER gu WHERE gu.UserID = lu.UserID AND gu.GroupID IN ('{$impoded_groupTarget}'))";
            }
            
            if (($FindBy == 'Student') && ($rankTarget == 'form') && (! empty($classnames))) {
                foreach ($classnames as $classname) {
                    $classname = mysql_real_escape_string($classname);
                }
                unset($classname);
                $impoded_classname = implode("','", $classnames);
                $condition_clause .= " AND lu.ClassLevel IN ('{$impoded_classname}')";
            }
            
            if (($FindBy == 'Student') && ($rankTarget == 'class') && (! empty($classnames))) {
                foreach ($classnames as $classname) {
                    $classname = mysql_real_escape_string($classname);
                }
                unset($classname);
                $impoded_classname = implode("','", $classnames);
                $condition_clause .= " AND lu.ClassName IN ('{$impoded_classname}')";
            }
            
            if (($FindBy == 'Student') && ($rankTarget == 'student') && (! empty($studentIDs))) {
                foreach ($studentIDs as $studentID) {
                    $studentID = mysql_real_escape_string($studentID);
                }
                unset($studentID);
                $impoded_studentID = implode("','", $studentIDs);
                $condition_clause .= " AND lu.UserID IN ('{$impoded_studentID}')";
            }
            
            $sql = " SELECT lb.BookTitle, 
                            IF( lbl.RecordStatus='LOST' AND lol.DaysCount IS NULL, '" . $Lang["libms"]["book_status"]["LOST"] . "', lol.DaysCount) AS OverDueDays, 
                            lol.DateCreated, 
                            lol.Payment, 
                            IFNULL(bal.TransAmount,lol.PaymentReceived) AS PaymentReceived,
                            IFNULL(bal.TransAmountAfter,lol.Payment-IFNULL(lol.PaymentReceived,0)) AS Owed,
                            lol.PaymentMethod, 
                            lol.RecordStatus, 
                            IFNULL(bal.DateModified,lol.DateModified) AS DateModified,
                            CONCAT(lu." . $Lang['libms']['SQL']['UserNameFeild'] . ", ' [', lu.BarCode,']') AS UserName, 
                            lu.ClassName, 
                            lu.ClassNumber, 
                            lbl.UniqueID, 
                            lol.LastModifiedBy, 
                            lbl.BorrowLogID, 
                            lol.IsImportData, 
                            lol.OverDueLogID,
                            bal.TransDesc,
                            lbl.RecordStatus AS BorrowStatus 
                    FROM 
                            LIBMS_OVERDUE_LOG AS lol
                    LEFT JOIN
                            ( SELECT 
                                    bal.TransAmount,
                                    bal.TransAmountAfter,
                                    bal.DateModified,
                                    bal.TransDesc,
                                    bal.TransRefID
                            FROM
                                    LIBMS_BALANCE_LOG bal 
                                    INNER JOIN LIBMS_OVERDUE_LOG lol ON (bal.TransRefID=lol.OverDueLogID AND bal.TransType='OverdueLog')
                            WHERE   1
                                    $statusFilter 
                            ) AS bal ON bal.TransRefID=lol.OverDueLogID,  
                            LIBMS_BORROW_LOG AS lbl, 
                            LIBMS_BOOK AS lb, 
                            LIBMS_USER AS lu
                    WHERE " . $sql_cond . $condition_clause . $sql_cond2.
                  " AND     lbl.BorrowLogID=lol.BorrowLogID 
                    AND     lb.BookID=lbl.BookID 
                    AND     lu.UserID=lbl.UserID 
                    AND     lol.RecordStatus<>'DELETE' ";
            
            if ($RecordStatus != 'SETTLED') {
                $sql .= " UNION ";
    
                // partial payment, unsettled record
                $sql .= " SELECT lb.BookTitle,
                                IF( lbl.RecordStatus='LOST', '" . $Lang["libms"]["book_status"]["LOST"] . "', lol.DaysCount) AS OverDueDays,
                                lol.DateCreated,
                                lol.Payment,
                                '--' AS PaymentReceived,
                                (lol.Payment-IFNULL(lol.PaymentReceived,0)) AS Owed,
                                lol.PaymentMethod,
                                lol.RecordStatus,
                                lol.DateModified,
                                CONCAT(lu." . $Lang['libms']['SQL']['UserNameFeild'] . ", ' [', lu.BarCode,']') AS UserName,
                                lu.ClassName,
                                lu.ClassNumber,
                                lbl.UniqueID,
                                lol.LastModifiedBy,
                                lbl.BorrowLogID,
                                lol.IsImportData,
                                lol.OverDueLogID,
                                '' AS TransDesc,
                                lbl.RecordStatus AS BorrowStatus
                        FROM
                                LIBMS_OVERDUE_LOG AS lol,
                                LIBMS_BORROW_LOG AS lbl,
                                LIBMS_BOOK AS lb,
                                LIBMS_USER AS lu
                        WHERE " . $sql_cond . $condition_clause .
                        " AND     lbl.BorrowLogID=lol.BorrowLogID
                        AND     lb.BookID=lbl.BookID
                        AND     lu.UserID=lbl.UserID
                        AND     lol.RecordStatus='OUTSTANDING'
                        AND     lol.PaymentReceived IS NOT NULL AND lol.PaymentReceived>0 ";
            }
            
            $sql .= "ORDER BY " . $order_cond;
            
            $results = $this->returnResultSet($sql);
            // debug($sql);
            // debug_r($results);
            
            $ClassNumberSql = (isset($junior_mck)) ? "CONVERT( ClassNumber  USING utf8)" : "ClassNumber";
            
            // get book title with ACNO
            $sql = "SELECT lbl.BorrowLogID, lbu.ACNO FROM LIBMS_BORROW_LOG AS lbl, LIBMS_BOOK_UNIQUE AS lbu WHERE lbl.UniqueID=lbu.UniqueID" . " AND UNIX_TIMESTAMP(lbl.DateModified)>=UNIX_TIMESTAMP('{$StartDate}') AND UNIX_TIMESTAMP(lbl.DateModified)<=UNIX_TIMESTAMP('{$EndDate} 23:59:59')" . "  {$sql_cond} ";
            // debug($sql);
            $rows = $this->returnArray($sql);
            $BookACNOArrary = build_assoc_array($rows);
            // debug_r($BookACNOArrary);
            
            // get user list
            $sql = "SELECT UserID, CONCAT(" . $Lang['libms']['SQL']['UserNameFeild'] . ", if(UserType='S' AND ClassName<>'', CONCAT(' (', ClassName , '-', {$ClassNumberSql}, ')'), '')) FROM LIBMS_USER ORDER BY UserID";
            $rows = $this->returnArray($sql);
            $Handlers = build_assoc_array($rows);
            // debug_r($Handlers);
            
            $sql = "SELECT OverDueLogID FROM LIBMS_OVERDUE_CHANGE_LOG GROUP BY OverDueLogID ORDER BY OverDueLogID";
            $changed_overdue_log = $this->returnVector($sql);
            
            $displayTable = '';
            $TableCSS = ($Task == "DISPLAY") ? "common_table_list view_table_list" : "common_table_list_v30  view_table_list_v30";
            $displayTable .= '<table class="' . $TableCSS . '" width="100%" align="center"  border="0" cellSpacing="0" cellPadding="4">';
            $displayTable .= '<thead>';
            $displayTable .= '<tr>';
            $displayTable .= '<th width="1%" class="tabletop tabletopnolink">';
            $displayTable .= '#';
            $displayTable .= '</th>';
            $displayTable .= '<th width="25%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["title"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["CirculationManagement"]["overdue_day"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="10%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["report"]["fine_date"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["CirculationManagement"]["payment"] . " (\$)";
            $displayTable .= '</th>';
            $displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["report"]["fine_status"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["CirculationManagement"]["paid_or_waive"] . " (\$)";
            $displayTable .= '</th>';
            $displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["CirculationManagement"]["left"]. " (\$)";
            $displayTable .= '</th>';
            $displayTable .= '<th width="10%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["user_name"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["report"]["ClassName"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["report"]["ClassNumber"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="10%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["report"]["fine_handle_date_or_last_updated"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="10%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["report"]["handled_by"];
            $displayTable .= '</th>';
            $displayTable .= '</tr>';
            $displayTable .= '</thead>';
            $displayTable .= '<tbody>';
            for ($i = 0; $i < sizeof($results); $i ++) {
                $thisBook = $results[$i];
                
                if (trim($thisBook["PaymentReceived"]) == "") {
                    $thisBook["PaymentReceived"] = "--";
                }
                if (trim($thisBook["ClassName"]) == "") {
                    $thisBook["ClassName"] = "--";
                }
                if (trim($thisBook["ClassNumber"]) == "") {
                    $thisBook["ClassNumber"] = "--";
                }
                
                $owed = ($thisBook["Owed"] == 0 || $thisBook["Owed"] == '') ? '--' : $thisBook["Owed"];
                
                $isOutStanding = false;
                $paidDate = $thisBook["DateModified"];
                $waiveAndReturn = false;
                
                if ($thisBook["RecordStatus"] == "OUTSTANDING") {
                    if (($thisBook["TransDesc"] == "pay overdue") || (($thisBook["TransDesc"] == "") && ($thisBook["PaymentReceived"] > 0))) {
                        $thisRecordStatus = $Lang["libms"]["CirculationManagement"]["paied"];
                    }
                    else {
                        $thisRecordStatus = $Lang["libms"]["report"]["fine_outstanding"];
                        $isOutStanding = true;
                    }
                } elseif ($thisBook["RecordStatus"] == "SETTLED") {
                    $thisRecordStatus = $Lang["libms"]["CirculationManagement"]["paied"];
                } elseif ($thisBook["RecordStatus"] == "WAIVED") {
                    if ($thisBook["TransDesc"] == "pay overdue") {
                        $thisRecordStatus = $Lang["libms"]["CirculationManagement"]["paied"];
                    }
                    else {
                        $thisRecordStatus = $Lang["libms"]["report"]["fine_waived"];
                        $owed = '--';
                        if ($thisBook["BorrowStatus"] == 'RETURNED') {
                            $waiveAndReturn = true;
                        }
                    }                    
                } else {
                    $thisRecordStatus = "--";
                }
                
                $rowStyle = $isOutStanding ? " style=\"background-color:#E8FFE8;\" " : "";
                
                if (trim($BookACNOArrary[$thisBook["BorrowLogID"]]) != "") {
                    $thisBook["BookTitle"] .= " [" . trim($BookACNOArrary[$thisBook["BorrowLogID"]]) . "]";
                } else {
                    // debug($thisBook["BorrowLogID"]);
                }
                
                if ($Task == "EXPORT") {
                    $changePayment = in_array($thisBook["OverDueLogID"], (array) $changed_overdue_log) ? "^" : "";
                    $ExportArr[$i][] = $thisBook["BookTitle"];
                    $ExportArr[$i][] = ($thisBook["IsImportData"] ? '*' : '') . $thisBook["OverDueDays"];
                    $ExportArr[$i][] = $thisBook["DateCreated"];
                    $ExportArr[$i][] = $thisBook["Payment"];
                    $ExportArr[$i][] = ($waiveAndReturn ? "#" : "") . $thisRecordStatus;
                    $ExportArr[$i][] = $thisBook["PaymentReceived"];
                    $ExportArr[$i][] = $owed;
                    $ExportArr[$i][] = $thisBook["UserName"];
                    $ExportArr[$i][] = $thisBook["ClassName"];
                    $ExportArr[$i][] = $thisBook["ClassNumber"];
                    $ExportArr[$i][] = $paidDate;
                    $ExportArr[$i][] = $Handlers[$thisBook["LastModifiedBy"]];
                    $ExportArr[$i][] = $changePayment;
                } else {
                    $changePayment = in_array($thisBook["OverDueLogID"], (array) $changed_overdue_log) ? "<span class=\"tabletextrequire\">^</span>" : "";
                    $displayTable .= '<tr>';
                    $displayTable .= '<td ' . $rowStyle . '>';
                    $displayTable .= ($i + 1);
                    $displayTable .= '</td>';
                    $displayTable .= '<td ' . $rowStyle . '>';
                    $displayTable .= $thisBook["BookTitle"];
                    $displayTable .= '</td>';
                    $displayTable .= '<td ' . $rowStyle . '>';
                    $displayTable .= ($thisBook["IsImportData"] ? '<font color = red>*</font>' : '') . $thisBook["OverDueDays"];
                    $displayTable .= '</td>';
                    $displayTable .= '<td ' . $rowStyle . '>';
                    $displayTable .= $thisBook["DateCreated"];
                    $displayTable .= '</td>';
                    $displayTable .= '<td ' . $rowStyle . '>';
                    $displayTable .= $thisBook["Payment"] . $changePayment;
                    $displayTable .= '</td>';
                    $displayTable .= '<td ' . $rowStyle . '>';
                    $displayTable .= ($waiveAndReturn ? '<font color = red>#</font>' : '') . $thisRecordStatus;
                    $displayTable .= '</td>';
                    $displayTable .= '<td ' . $rowStyle . '>';
                    $displayTable .= ($thisBook["PaymentReceived"] < 0) ? '<font color = red>'.$thisBook["PaymentReceived"].'</font>' : $thisBook["PaymentReceived"];
                    $displayTable .= '</td>';
                    $displayTable .= '<td ' . $rowStyle . '>';
                    $displayTable .= $owed;
                    $displayTable .= '</td>';
                    $displayTable .= '<td ' . $rowStyle . '>';
                    $displayTable .= $thisBook["UserName"];
                    $displayTable .= '</td>';
                    $displayTable .= '<td ' . $rowStyle . '>';
                    $displayTable .= $thisBook["ClassName"];
                    $displayTable .= '</td>';
                    $displayTable .= '<td ' . $rowStyle . '>';
                    $displayTable .= $thisBook["ClassNumber"];
                    $displayTable .= '</td>';
                    $displayTable .= '<td ' . $rowStyle . '>';
                    $displayTable .= $paidDate;
                    $displayTable .= '</td>';
                    $displayTable .= '<td ' . $rowStyle . '>';
                    $displayTable .= $Handlers[$thisBook["LastModifiedBy"]];
                    $displayTable .= '</td>';
                    $displayTable .= '</tr>';
                }
            }
            if (sizeof($results) == 0) {
                $displayTable .= '<tr>';
                $displayTable .= '<td colspan="13" align="center">';
                $displayTable .= $Lang['libms']['NoRecordAtThisMoment'];
                $displayTable .= '</td>';
                $displayTable .= '</tr>';
            }
            $displayTable .= '</tbody>';
            $displayTable .= '</table>';
            $displayTable .= '<div class="tabletextremark" style="text-align:left"><font color = red>*</font> ' . $Lang['libms']['report']['ForReferenceOnly'] . '</div>';
            $displayTable .= '<div class="tabletextremark" style="text-align:left"><font color = red>^</font> ' . $Lang['libms']['report']['LostBookPaymentChanged'] . '</div>';
            $displayTable .= '<div class="tabletextremark" style="text-align:left"><font color = red>#</font> ' . $Lang['libms']['report']['LostBookReturned'] . '</div>';
            $displayTable .= '<div class="tabletextremark" style="text-align:left">' . $Lang['libms']['report']['NegagivePaidDenotesCancelOverdue']. '</div>';
            if ($Task == "EXPORT") {
                return $ExportArr;
            } else {
                return $displayTable;
            }
        }

        function get_accession_report($from_acno, $to_acno, $StartDate, $EndDate, $CirculationTypeCode, $BookCategoryCode, $BookSubject, $BookLanguage, $BookStatus, $Task = "DISPLAY", $EnableLine = 0, $BookCategoryType = 0, $PurchaseByDepartment = '', $tags='')
        {
            global $Lang, $junior_mck;
            
            $type = false;
            $quotes = false;
            $from_acno = PHPToSQL($from_acno, $type, $quotes);
            $to_acno = PHPToSQL($to_acno, $type, $quotes);
            $CirculationTypeCode = PHPToSQL($CirculationTypeCode, $type, $quotes);
            $BookCategoryCode = PHPToSQL($BookCategoryCode, $type, $quotes);
            $BookSubject = PHPToSQL($BookSubject, $type, $quotes);
            $BookLanguage = PHPToSQL($BookLanguage, $type, $quotes);
            $BookStatus = PHPToSQL($BookStatus, $type, $quotes);
            
            $from_acno = ($from_acno == 'NULL') ? null : $from_acno;
            $to_acno = ($to_acno == 'NULL') ? null : $to_acno;
            $CirculationTypeCode = ($CirculationTypeCode == 'NULL') ? null : $CirculationTypeCode;
            $BookCategoryCode = ($BookCategoryCode == 'NULL') ? null : $BookCategoryCode;
            $BookSubject = ($BookSubject == 'NULL') ? null : $BookSubject;
            $BookLanguage = ($BookLanguage == 'NULL') ? null : $BookLanguage;
            $BookStatus = ($BookStatus == 'NULL') ? null : $BookStatus;
            $sql_cond = '';
            
            if (isset($CirculationTypeCode)) {
                $sql_cond .= "AND ((lbu.CirculationTypeCode<>'' AND lbu.CirculationTypeCode IS NOT NULL AND lbu.CirculationTypeCode IN ('" . implode("','", (array) $CirculationTypeCode) . "'))";
                $sql_cond .= " OR (lb.CirculationTypeCode<>'' AND lb.CirculationTypeCode IS NOT NULL AND lb.CirculationTypeCode IN ('" . implode("','", (array) $CirculationTypeCode) . "') AND (lbu.CirculationTypeCode='' OR lbu.CirculationTypeCode IS NULL))";
                if (in_array('', (array) $CirculationTypeCode)) {
                    $sql_cond .= " OR ((lb.CirculationTypeCode='' OR lb.CirculationTypeCode IS NULL) AND (lbu.CirculationTypeCode='' or lbu.CirculationTypeCode IS NULL))";
                }
                $sql_cond .= ") ";
            }
            
            if (isset($BookCategoryCode) && ($BookCategoryType != 0)) {
                $sql_cond .= " AND (lb.BookCategoryCode IN ('" . implode("','", $BookCategoryCode) . "') ";
                $hasNullCatCode = 0;
                foreach ($BookCategoryCode as $aBookCatCode) {
                    if ($aBookCatCode == '') {
                        $hasNullCatCode = 1;
                        $sql_cond .= " OR lb.BookCategoryCode IS NULL ";
                        break;
                    }
                }
                $sql_cond .= ") ";
                // Henry Added [20150122]
                if ($BookCategoryType != 0) {
                    // if(!$hasNullCatCode){
                    // //$sql_cond .= " AND (lb.BookCategoryCode IN (SELECT C2.BookCategoryCode FROM LIBMS_BOOK_CATEGORY C2 WHERE C2.BookCategoryType='".$BookCategoryType."') ";
                    // $sql_cond .= ' AND (lb.Language IN (SELECT lbl2.BookLanguageCode FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookCategoryType = "'.$BookCategoryType.'") OR '.$BookCategoryType.' = 1 AND (Language IS NULL OR Language = "")) ';
                    // }else{
                    // //$sql_cond .= " OR lb.BookCategoryCode IS NULL OR lb.BookCategoryCode ='' ";
                    $sql_cond .= ' AND (lb.Language IN (SELECT lbl2.BookLanguageCode FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookCategoryType = "' . $BookCategoryType . '") OR Language IS NULL OR Language = "") ';
                    // }
                    // $sql_cond .= ") ";
                }
            }
            
            if (isset($BookSubject)) {
                $sql_cond .= " AND (TRIM(lb.Subject) IN ('" . implode("','", PHPToSQL($BookSubject, false, false)) . "') ";
                foreach ($BookSubject as $aBookSub) {
                    if ($aBookSub == '') {
                        $sql_cond .= " OR lb.Subject IS NULL ";
                        break;
                    }
                }
                $sql_cond .= ") ";
            }
            
            if (isset($BookLanguage)) {
                $sql_cond .= " AND (TRIM(lb.Language) IN ('" . implode("','", $BookLanguage) . "') ";
                foreach ($BookLanguage as $aBookLang) {
                    if ($aBookLang == '') {
                        $sql_cond .= " OR lb.Language IS NULL ";
                        break;
                    }
                }
                $sql_cond .= ") ";
            }
            
            if (isset($BookStatus)) {
                $sql_cond .= " AND (TRIM(lbu.RecordStatus) IN ('" . implode("','", $BookStatus) . "') ";
                foreach ($BookStatus as $aBookStatus) {
                    if ($aBookStatus == '') {
                        $sql_cond .= " OR lbu.RecordStatus IS NULL OR lbu.RecordStatus = '' ";
                        break;
                    }
                }
                $sql_cond .= ") ";
            }
            
            if (isset($PurchaseByDepartment)) {
                $sql_cond .= " AND (TRIM(lbu.PurchaseByDepartment) IN ('" . implode("','", PHPToSQL($PurchaseByDepartment, false, false)) . "') ";
                foreach ($PurchaseByDepartment as $dept) {
                    if ($dept == '') {
                        $sql_cond .= " OR lbu.PurchaseByDepartment IS NULL OR lbu.PurchaseByDepartment = '' ";
                        break;
                    }
                }
                $sql_cond .= ") ";
            }
            
            if ($from_acno != "" || $to_acno != "") {
                if ($from_acno == "") {
                    $acno_cond = ' AND lbu.ACNO = "' . $to_acno . '" ';
                } else if ($to_acno == "") {
                    $acno_cond = ' AND lbu.ACNO = "' . $from_acno . '" ';
                } else
                    $acno_cond = ' AND lbu.ACNO >= "' . $from_acno . '" AND lbu.ACNO <= "' . $to_acno . '" ';
                $order_cond = "ORDER BY lbu.ACNO";
            }
            if ($StartDate != "" && $EndDate != "") {
                $accDate = ' AND UNIX_TIMESTAMP(lbu.CreationDate) >= UNIX_TIMESTAMP("' . $StartDate . '") AND UNIX_TIMESTAMP(lbu.CreationDate) <= UNIX_TIMESTAMP("' . $EndDate . ' 23:59:59") ';
                $order_cond = "ORDER BY DATE(lbu.CreationDate), ACNO";
            }
            
            if ($EnableLine == 1) {
                $select_writeoff_sql = " ,w.Reason as WriteOffReason ";
                $join_writeoff_sql = " LEFT JOIN LIBMS_WRITEOFF_LOG w ON lbu.UniqueID = w.BookUniqueID ";
            }
            
            if ($tags){ 
                $tag_sql = "INNER JOIN " .
                    "		(SELECT 		bt.BookID FROM `LIBMS_BOOK_TAG` bt
							INNER JOIN 	`LIBMS_TAG` t ON t.`TagID`=bt.`TagID`
							WHERE 		t.`TagName` LIKE '%".$this->Get_Safe_Sql_Like_Query($tags)."%'
							GROUP BY 	bt.BookID) AS tg
						ON tg.BookID=lb.BookID";
            }
            else {
                $tag_sql = "";
            }
            
            $sql = "SELECT
					DATE(lbu.CreationDate) as AccountDate, " . "lbu.ACNO, " . "lbu.BarCode, " . "lb.ResponsibilityBy1, " . "lb.ResponsibilityBy2, " . "lb.ResponsibilityBy3, " . "lb.BookTitle, " . "lb.CallNum, " . "lb.CallNum2, " . "lb.Publisher, " . "lb.PublishYear, " . "lbu.Distributor, " . "lbu.InvoiceNumber, " . "lbu.ListPrice, " . "lbu.Discount, " . "lbu.PurchasePrice, " . "lbu.PurchaseByDepartment, " . "lb.RemarkInternal, " . "TRIM(lbu.RecordStatus) AS RecordStatus
					{$select_writeoff_sql}
				FROM
					LIBMS_BOOK_UNIQUE lbu
				INNER JOIN
					LIBMS_BOOK lb ON lb.BookID = lbu.BookID 
				{$join_writeoff_sql}
				{$tag_sql}
				WHERE 1 
				AND " . "lbu.RecordStatus <> 'DELETE' 
				{$acno_cond}
				{$accDate}
				{$sql_cond}
				{$order_cond}";
            $results = $this->returnResultSet($sql);
            // debug($sql);
            // debug_r($results);
            
            $ClassNumberSql = (isset($junior_mck)) ? "CONVERT( ClassNumber  USING utf8)" : "ClassNumber";
            
            $displayTable = '';
            $TableCSS = ($Task == "DISPLAY") ? "common_table_list view_table_list" : "common_table_list_v30  view_table_list_v30";
            $displayTable .= '<table class="' . $TableCSS . '" width="100%" align="center"  border="0" cellSpacing="0" cellPadding="4">';
            $displayTable .= '<thead>';
            $displayTable .= '<tr>';
            $displayTable .= '<th width="1" class="tabletop tabletopnolink">';
            $displayTable .= '#';
            $displayTable .= '</th>';
            $displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["account_date"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="6%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["code"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="9%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["barcode"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["report"]["ResponsibilityBy"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="18%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["title"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="7%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["call_number"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["publisher"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="3%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["publish_year"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="7%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["distributor"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="7%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang['libms']['book']['invoice_number'];
            $displayTable .= '</th>';
            $displayTable .= '<th width="4%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["list_price"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="4%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["discount"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="4%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["purchase_price"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["purchase_by_department"];
            $displayTable .= '</th>';
            $displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
            $displayTable .= $Lang["libms"]["book"]["remark_internal"];
            $displayTable .= '</th>';
            $displayTable .= '</tr>';
            $displayTable .= '</thead>';
            $displayTable .= '<tbody>';
            for ($i = 0; $i < sizeof($results); $i ++) {
                $thisBook = $results[$i];
                
                if (trim($thisBook["AccountDate"]) == "") {
                    $thisBook["AccountDate"] = "--";
                }
                if (trim($thisBook["ACNO"]) == "") {
                    $thisBook["ACNO"] = "--";
                }
                if (trim($thisBook["BarCode"]) == "") {
                    $thisBook["BarCode"] = "--";
                }
                if (trim($thisBook["ResponsibilityBy1"]) == "") {
                    $thisBook["ResponsibilityBy1"] = "--";
                }
                if (trim($thisBook["ResponsibilityBy2"]) != "") {
                    $thisBook["ResponsibilityBy2"] = ", " . $thisBook["ResponsibilityBy2"];
                }
                if (trim($thisBook["ResponsibilityBy3"]) != "") {
                    $thisBook["ResponsibilityBy3"] = ", " . $thisBook["ResponsibilityBy3"];
                }
                if (trim($thisBook["BookTitle"]) == "") {
                    $thisBook["BookTitle"] = "--";
                }
                if (trim($thisBook["CallNum"]) == "") {
                    $thisBook["CallNum"] = "--";
                }
                if (trim($thisBook["Publisher"]) == "") {
                    $thisBook["Publisher"] = "--";
                }
                if (trim($thisBook["PublishYear"]) == "") {
                    $thisBook["PublishYear"] = "--";
                }
                if (trim($thisBook["Distributor"]) == "") {
                    $thisBook["Distributor"] = "--";
                }
                if (trim($thisBook["InvoiceNumber"]) == "") {
                    $thisBook["InvoiceNumber"] = "--";
                }
                if (trim($thisBook["ListPrice"]) == "") {
                    $thisBook["ListPrice"] = "--";
                }
                if (trim($thisBook["Discount"]) == "") {
                    $thisBook["Discount"] = "--";
                }
                if (trim($thisBook["PurchasePrice"]) == "") {
                    $thisBook["PurchasePrice"] = "--";
                }
                if (trim($thisBook["PurchaseByDepartment"]) == "") {
                    $thisBook["PurchaseByDepartment"] = "--";
                }
                if (trim($thisBook["RemarkInternal"]) == "") {
                    $thisBook["RemarkInternal"] = "--";
                }
                
                $rowStyle = "";
                
                if ($Task == "EXPORT") {
                    $ExportArr[$i][] = $thisBook["AccountDate"];
                    $ExportArr[$i][] = $thisBook["ACNO"];
                    $ExportArr[$i][] = $thisBook["BarCode"];
                    $ExportArr[$i][] = $thisBook["ResponsibilityBy1"] . $thisBook["ResponsibilityBy2"] . $thisBook["ResponsibilityBy3"];
                    $ExportArr[$i][] = $thisBook["BookTitle"];
                    $ExportArr[$i][] = $thisBook["CallNum"] . "  " . $thisBook["CallNum2"];
                    $ExportArr[$i][] = $thisBook["Publisher"];
                    $ExportArr[$i][] = $thisBook["PublishYear"];
                    $ExportArr[$i][] = $thisBook["Distributor"];
                    $ExportArr[$i][] = $thisBook["InvoiceNumber"];
                    $ExportArr[$i][] = $thisBook["ListPrice"];
                    $ExportArr[$i][] = $thisBook["Discount"];
                    $ExportArr[$i][] = $thisBook["PurchasePrice"];
                    $ExportArr[$i][] = $thisBook["PurchaseByDepartment"];
                    $ExportArr[$i][] = $thisBook['RecordStatus'] == "WRITEOFF" && $EnableLine == 1 ? $Lang["libms"]["book_status"]["WRITEOFF"] . ($thisBook["WriteOffReason"] ? ' (' . $thisBook["WriteOffReason"] . ')' : '') : $thisBook["RemarkInternal"];
                } else {
                    if (($i + 1) % 20 == 1 && $i > 1) {
                        $displayTable .= '<tr class="page-break20">';
                    } else
                        $displayTable .= '<tr>';
                    if ($thisBook['RecordStatus'] == "WRITEOFF" && $EnableLine == 1) {
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= ($i + 1);
                        $displayTable .= '<div style="width: 200%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $thisBook["AccountDate"];
                        $displayTable .= '<div style="width: 150%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $thisBook["ACNO"];
                        $displayTable .= '<div style="width: 150%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $thisBook["BarCode"];
                        $displayTable .= '<div style="width: 150%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $thisBook["ResponsibilityBy1"] . $thisBook["ResponsibilityBy2"] . $thisBook["ResponsibilityBy3"];
                        $displayTable .= '<div style="width: 150%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $thisBook["BookTitle"];
                        $displayTable .= '<div style="width: 150%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $thisBook["CallNum"] . "<br/>" . $thisBook["CallNum2"];
                        $displayTable .= '<div style="width: 150%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $thisBook["Publisher"];
                        $displayTable .= '<div style="width: 150%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $thisBook["PublishYear"];
                        $displayTable .= '<div style="width: 150%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $thisBook["Distributor"];
                        $displayTable .= '<div style="width: 150%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $thisBook["InvoiceNumber"];
                        $displayTable .= '<div style="width: 150%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $thisBook["ListPrice"];
                        $displayTable .= '<div style="width: 150%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $thisBook["Discount"];
                        $displayTable .= '<div style="width: 150%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $thisBook["PurchasePrice"];
                        $displayTable .= '<div style="width: 150%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $thisBook["PurchaseByDepartment"];
                        $displayTable .= '<div style="width: 150%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= '<div style="position: relative; ">';
                        $displayTable .= $Lang["libms"]["book_status"]["WRITEOFF"] . ($thisBook["WriteOffReason"] ? ' (' . $thisBook["WriteOffReason"] . ')' : '');
                        $displayTable .= '<div style="width: 100%; position: absolute; top: 9px;border-bottom:solid 1px #000 "></div></div>';
                        $displayTable .= '</td>';
                    } else {
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= ($i + 1);
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["AccountDate"];
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["ACNO"];
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["BarCode"];
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["ResponsibilityBy1"] . $thisBook["ResponsibilityBy2"] . $thisBook["ResponsibilityBy3"];
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["BookTitle"];
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["CallNum"] . "<br/>" . $thisBook["CallNum2"];
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["Publisher"];
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["PublishYear"];
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["Distributor"];
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["InvoiceNumber"];
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["ListPrice"];
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["Discount"];
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["PurchasePrice"];
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["PurchaseByDepartment"];
                        $displayTable .= '</td>';
                        $displayTable .= '<td ' . $rowStyle . '>';
                        $displayTable .= $thisBook["RemarkInternal"];
                        $displayTable .= '</td>';
                    }
                    $displayTable .= '</tr>';
                }
            }
            if (sizeof($results) == 0) {
                $displayTable .= '<tr>';
                $displayTable .= '<td colspan="16" align="center">';
                $displayTable .= $Lang['libms']['NoRecordAtThisMoment'];
                $displayTable .= '</td>';
                $displayTable .= '</tr>';
            }
            // else{
            // //for testing [start]
            // $addRow = 20 - sizeof($results)%20;
            // for($i=0; $i < $addRow; $i++){
            // $displayTable .= '<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
            // }
            // //for testing [end]
            // }
            $displayTable .= '</tbody>';
            $displayTable .= '</table>';
            
            if ($Task == "EXPORT") {
                return $ExportArr;
            } else {
                return $displayTable;
            }
        }

        // convert a number to letter(s) (eg. 1 -> a, 27 -> aa)
        function numberToLetter($number, $uppercase = false)
        {
            $number --;
            $letter = chr($number % 26 + 97);
            if ($number >= 26) {
                $letter = $this->numberToLetter(floor($number / 26), $uppercase) . $letter;
            }
            return ($uppercase ? strtoupper($letter) : $letter);
        }

        function getAdvanceSearchDiv($page = 'Book', $pageFile = '')
        {
            global $Lang, $linterface, $PATH_WRT_ROOT, $ACNO, $bookTitle, $publisher, $author, $callNumber, $ISBN, $Barcode, $bookSeries, $bookSeriesno;
            
            $ACNO = htmlspecialchars($ACNO);
            $Barcode = htmlspecialchars($Barcode);
            $bookTitle = htmlspecialchars($bookTitle);
            $author = htmlspecialchars($author);
            $callNumber = htmlspecialchars($callNumber);
            $ISBN = htmlspecialchars($ISBN);
            $publisher = htmlspecialchars($publisher);
            $bookSeries = intranet_htmlspecialchars($bookSeries);
            $bookSeriesno = intranet_htmlspecialchars($bookSeriesno);
            
            // include_once($PATH_WRT_ROOT."/includes/form_class_manage.php");
            // $lfcm = new form_class_manage();
            //
            // // get school year selection
            // $SchoolYearList = $lfcm->Get_Academic_Year_List();
            // $academic_year_selection .= '<select name="academic_year" id="academic_year">';
            // $Selected = '';
            // if(stripslashes(stripslashes($academic_year)) == -1)
            // $Selected = 'selected';
            // $academic_year_selection .= '<option value="-1" '.$Selected.'>'.$Lang['SysMgr']['FormClassMapping']['All']['AcademicYear'].'</option>';
            // for ($i=0; $i< sizeof($SchoolYearList); $i++) {
            // $SchoolYearName = Get_Lang_Selection($SchoolYearList[$i]['YearNameB5'],$SchoolYearList[$i]['YearNameEN']);
            //
            // //if (!$NoCheckCurrent) {
            // if ($SchoolYearList[$i]['CurrentSchoolYear'] == '1')
            // $AcademicYearID = $SchoolYearList[$i]['AcademicYearID'];
            // //}
            //
            // //unset($Selected);
            // if(stripslashes(stripslashes($academic_year)))
            // $Selected = (stripslashes(stripslashes($academic_year)) == $SchoolYearList[$i]['AcademicYearID'])? 'selected':'';
            //
            // else
            // $Selected = (Get_Current_Academic_Year_ID() == $SchoolYearList[$i]['AcademicYearID'])? 'selected':'';
            // $academic_year_selection .= '<option value="'.$SchoolYearList[$i]['AcademicYearID'].'" '.$Selected.'>'.$SchoolYearName.'</option>';
            // }
            // $academic_year_selection .= '</select>';
            
            if ($pageFile == '') {
                if ($page == 'Book') {
                    $pageFile = 'index';
                } else {
                    $pageFile = 'book_item_list';
                }
            }
            
            $searchDiv = '<div class="Conntent_search"><div style="float:right;">';
            // $searchDiv .= '<input type="button" value="' . $Lang['Btn']['Search'] . '" class="formsmallbutton" onclick="jsReloadCurrentRouting(0)"/> | <a href="javascript:void(0)" onclick="DisplayAdvanceSearch();" id="AdvanceSearchText"> ' . $Lang['DocRouting']['Advanced'] . '</a>';
            $searchDiv .= '<a href="javascript:void(0)" onclick="DisplayAdvanceSearch();" id="AdvanceSearchText"> ' . $Lang['libms']['bookmanagement']['Advanced'] . '</a>';
            $searchDiv .= '</div></div>';
            if ($page == 'Book') {
                $searchDiv .= '<div id="advance_search" class="advance_search" style="top:0px;left:0px;display:none;">
		                     <a href="javascript:void(0)" class="advance_search_close" onclick="HiddenAdvanceSearch();" style="float:right">' . $Lang['Btn']['Close'] . '</a>
		                     <p class="spacer"></p>
	
							<div class="advance_search_form">
								<div class="table_board">
	
									<table class="form_table">
										<tr>
			                             <td class="field_title">' . $Lang["libms"]["book"]["code"] . '</td>
			                             <td><label>
			                               <input name="ACNO" id="ACNO" type="text" class="keywords" style="width:250px" value="' . $ACNO . '" />
			                             </label></td>
			                           	</tr>
										<tr>
			                             <td class="field_title">' . $Lang['libms']['bookmanagement']['bookTitle'] . '</td>
			                             <td><label>
			                               <input name="bookTitle" id="bookTitle" type="text" class="keywords" style="width:250px" value="' . $bookTitle . '" />
			                             </label></td>
			                           	</tr>
										<tr>
			                             <td class="field_title">' . $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Author'] . '</td>
			                             <td><label>
			                               <input name="author" id="author" type="text" class="keywords" style="width:250px" value="' . $author . '" />
			                             </label></td>
			                           	</tr>
										<tr>
			                             <td class="field_title">' . $Lang["libms"]["book"]["call_number"] . '</td>
			                             <td><label>
			                               <input name="callNumber" id="callNumber" type="text" class="keywords" style="width:250px" value="' . $callNumber . '" />
			                             </label></td>
			                           	</tr>										
										<tr>
			                             <td class="field_title">' . $Lang["libms"]["book"]["ISBN"] . '</td>
			                             <td><label>
			                               <input name="ISBN" id="ISBN" type="text" class="keywords" style="width:250px" value="' . $ISBN . '" />
			                             </label></td>
			                           	</tr>
										<tr>
			                             <td class="field_title">' . $Lang["libms"]["book"]["publisher"] . '</td>
			                             <td><label>
			                               <input name="publisher" id="publisher" type="text" class="keywords" style="width:250px" value="' . $publisher . '" />
			                             </label></td>
			                           	</tr>
										<tr>
			                             <td class="field_title">' . $Lang['libms']['book']['series']. '</td>
			                             <td><label>
			                               <input name="bookSeries" id="bookSeries" type="text" class="keywords" style="width:250px" value="' . $bookSeries . '" />
			                             </label></td>
			                           	</tr>
										<tr>
			                             <td class="field_title">' . $Lang['libms']['book']['series_number']. '</td>
			                             <td><label>
			                               <input name="bookSeriesno" id="bookSeriesno" type="text" class="keywords" style="width:250px" value="' . $bookSeriesno . '" />
			                             </label></td>
			                           	</tr>
									</table>
								</div>
								<div class="edit_bottom">
										' . $linterface->GET_ACTION_BTN($Lang['Btn']['Search'], "button", "document.form1.action = '" . $pageFile . ".php?advanceSearch=1';document.form1.submit();document.form1.action = '" . $pageFile . ".php';") . '
										' . $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "HiddenAdvanceSearch()") . '
								</div>
							</div>
						</div>';
            } else {
                $searchDiv .= '<div id="advance_search" class="advance_search" style="top:0px;left:0px;display:none;">
		                     <a href="javascript:void(0)" class="advance_search_close" onclick="HiddenAdvanceSearch();" style="float:right">' . $Lang['Btn']['Close'] . '</a>
		                     <p class="spacer"></p>
	
							<div class="advance_search_form">
								<div class="table_board">
	
									<table class="form_table">
										<tr>
			                             <td class="field_title">' . $Lang["libms"]["book"]["code"] . '</td>
			                             <td><label>
			                               <input name="ACNO" id="ACNO" type="text" class="keywords" style="width:250px" value="' . $ACNO . '" />
			                             </label></td>
			                           	</tr>
										<tr>
			                             <td class="field_title">' . $Lang['libms']['bookmanagement']['barcode'] . '</td>
			                             <td><label>
			                               <input name="Barcode" id="Barcode" type="text" class="keywords" style="width:250px" value="' . $Barcode . '" />
			                             </label></td>
			                           	</tr>
										<tr>
			                             <td class="field_title">' . $Lang['libms']['bookmanagement']['bookTitle'] . '</td>
			                             <td><label>
			                               <input name="bookTitle" id="bookTitle" type="text" class="keywords" style="width:250px" value="' . $bookTitle . '" />
			                             </label></td>
			                           	</tr>
										<tr>
			                             <td class="field_title">' . $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Author'] . '</td>
			                             <td><label>
			                               <input name="author" id="author" type="text" class="keywords" style="width:250px" value="' . $author . '" />
			                             </label></td>
			                           	</tr>
										<tr>
			                             <td class="field_title">' . $Lang["libms"]["book"]["call_number"] . '</td>
			                             <td><label>
			                               <input name="callNumber" id="callNumber" type="text" class="keywords" style="width:250px" value="' . $callNumber . '" />
			                             </label></td>
			                           	</tr>										
										<tr>
			                             <td class="field_title">' . $Lang["libms"]["book"]["ISBN"] . '</td>
			                             <td><label>
			                               <input name="ISBN" id="ISBN" type="text" class="keywords" style="width:250px" value="' . $ISBN . '" />
			                             </label></td>
			                           	</tr>
										<tr>
			                             <td class="field_title">' . $Lang["libms"]["book"]["publisher"] . '</td>
			                             <td><label>
			                               <input name="publisher" id="publisher" type="text" class="keywords" style="width:250px" value="' . $publisher . '" />
			                             </label></td>
			                           	</tr>	
									</table>
								</div>
								<div class="edit_bottom">
										' . $linterface->GET_ACTION_BTN($Lang['Btn']['Search'], "button", "document.form1.action = '" . $pageFile . ".php?advanceSearch=1';document.form1.submit();document.form1.action = '" . $pageFile . ".php';") . '
										' . $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "HiddenAdvanceSearch()") . '
								</div>
							</div>
						</div>';
            }
            return $searchDiv;
        }

        // Search by User
        function get_search_detail_sub_tab($FromPage, $CurTab, $BookID, $UniqueID = '')
        {
            global $Lang;
            $para = '&FromPage=' . $FromPage . '&UniqueID=' . $UniqueID . '&clearCoo=1';
            $x = '<div class="shadetabs">
				<ul>';
            $x .= '  <li class="SubMenu">
					<a href="javascript:void(0)" class="UserSubTab" id="BorrowedInfo" data-page="Borrowed">' . $Lang["libms"]["SearchUser"]["ReturnBookTitle"] . '</a>
				   </li>';
            $x .= '  <li class="SubMenu">
					<a href="javascript:void(0)" class="UserSubTab" id="OverdueInfo" data-page="Overdue">' . $Lang["libms"]["CirculationManagement"]["left_pay"] . '</a>
				   </li>';
            $x .= '  <li class="SubMenu">
				<a href="javascript:void(0)" class="UserSubTab" id="ReservedInfo" data-page="Reserved">' . $Lang["libms"]["CirculationManagement"]["reserved"] . '</a>
			   </li>';
            
            // $x .= ' <li class="SubMenu">
            // <a href="circulation_details.php?BookID='.$BookID.$para.'">'.$Lang['libms']['bookmanagement']['circulation_details'].'</a>
            // </li>';
            $x .= ' </ul>
		</div>';
            return $x;
        }

        function saveRemarks($uID, $Remarks)
        {
            $sql = "INSERT INTO LIBMS_USER_REMARK
					(UserID, RemarkContent,InputDate, InputBy)
				VALUES
					('{$uID}', " . $this->pack_value($Remarks, 'str') . ",now(),'{$_SESSION['UserID']}');";
            
            $result = $this->db_db_query($sql);
            return $result;
        }

        function updateRemarks($uID, $Remarks)
        {
            $sql = "Update LIBMS_USER_REMARK
					set RemarkContent = " . $this->pack_value($Remarks, 'str') . ",DateModified = now(), LastModifiedBy= '{$_SESSION['UserID']}' 
				Where UserID = '{$uID}'";
            $result = $this->db_db_query($sql);
            return $result;
        }

        function getRemarks($uID)
        {
            $sql = "select * from LIBMS_USER_REMARK
				where UserID = '{$uID}' order by InputDate desc";
            $result = $this->returnArray($sql);
            
            if (count($result) == 0) {
                return false;
            } else {
                return $result;
            }
        }

        function Get_Borrowed_Books_Table($User, $curPage = 1)
        {
            global $Lang, $linterface, $root_path_found;
            
            include_once ("{$root_path_found}/home/library_sys/management/circulation/User.php");
            include_once ("{$root_path_found}/home/library_sys/management/circulation/RightRuleManager.php");
            
            $style = $curPage == 'Borrowed' ? '' : 'style="display:none;"';
            $x = '<div id="Borrowed" ' . $style . '>';
            $x .= $linterface->GET_NAVIGATION2_IP25($Lang["libms"]["Circulation"]["borrowedBooks"]) . ' <div class="table_board">
				<form name="BorrowedForm" id="BorrowedForm">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td valign="bottom"><div class="table_filter"></div></td>
					    <td valign="bottom">
					    	<div class="common_table_tool">
					    		<a href="javascript:void(0)" class="tool_approve" title="' . $Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["return"] . '" onclick="ReturnBooks();">' . $Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["return"] . '</a>
					    	</div>
					    </td>
					  </tr>
					</table>
					<table class="common_table_list">
					  <thead>
					    <tr>
					      <th class="num_check">#</th>
					      <th> ' . $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['BookTitle'] . '</th> 
					      <th> ' . $Lang["libms"]["CirculationManagement"]["borrowday"] . '</th>
					      <th> ' . $Lang["libms"]["CirculationManagement"]["dueday"] . '</th>
					      <th> ' . $Lang["libms"]["report"]["recordstatus"] . '</th>
					      <th> ' . $Lang['libms']['bookmanagement']['ReturnDate'] . '</th>
					      <th> ' . $Lang["libms"]["CirculationManagement"]["payment"] . '</th>
					      <th class="num_check" ><input type="checkbox" name="CheckAll" id="CheckAll" onclick="Set_Checkbox_Value(\'UniqueIDs[]\',this.checked);"/></th>
					      </tr>
					  </thead>
					  <tbody>';
            $nCount = 0;
            $iMax = sizeof($User->borrowedBooks);
            $bMax = sizeof($User->borrowedBooksHistory);
            if ($iMax == 0 && $bMax == 0) {
                $x .= '<tr><td align="center" colspan="100%">' . $Lang['libms']['NoRecordAtThisMoment'] . '</td></tr>';
            } else {
                $rightRuleManager = new RightRuleManager(null, $this);
                $checkboxRemark = '';
                
                for ($i = 0; $i < $iMax; $i ++) {
                    
                    $isAllowClassManagementCirculation = $rightRuleManager->isAllowClassManagementCirculation($User->borrowedBooks[$i]['LocationCode'], $circulationAction = 'borrow');
                    
                    if ($isAllowClassManagementCirculation) {
                        $checkboxCol = '<input type="checkbox" name="UniqueIDs[]" id="UniqueIDs[]" value="' . $User->borrowedBooks[$i]['UniqueID'] . '"/>';
                    } else {
                        $checkboxCol = '^';
                        $checkboxRemark = $Lang['libms']['Circulation']['RemarkNotAllowToBorrowReturnClassManagementBooks'];
                    }
                    
                    $isOverdue = (strtotime(Date('Y-m-d')) > strtotime($User->borrowedBooks[$i]['DueDate']));
                    if ($isOverdue) {
                        $x .= '		<tr class="row_suspend">';
                    } else {
                        $x .= '		<tr class="row_avaliable">';
                    }
                    $x .= '			  <td>' . ($nCount + 1) . '</td> 
							      <td>' . ($User->borrowedBooks[$i]['BookTitle'] ? $User->borrowedBooks[$i]['BookTitle'] . ' [' . $User->borrowedBooks[$i]['BarCode'] . ']' : '--') . '</td> 
							      <td>' . ($User->borrowedBooks[$i]['BorrowTime'] ? Date('Y-m-d', strtotime($User->borrowedBooks[$i]['BorrowTime'])) : '--') . '</td>
							      <td>' . ($User->borrowedBooks[$i]['DueDate'] ? $User->borrowedBooks[$i]['DueDate'] : '--') . '</td>
							      <td>' . ($isOverdue ? $Lang["libms"]["CirculationManagement"]["msg"]["overdue1"] : $Lang["libms"]["SearchUser"]["ReturnBookStatus"]['BORROWED']) . '</td>
						      	  <td>--</td>
						      	  <td>--</td>
                                  <td>' . $checkboxCol . '</td>
							    </tr>';
                    $nCount ++;
                }
                // History
                for ($i = 0; $i < $bMax; $i ++) {
                    $isOverdue = (strtotime(Date('Y-m-d', strtotime($User->borrowedBooksHistory[$i]['ReturnedTime']))) > strtotime($User->borrowedBooksHistory[$i]['DueDate']));
                    $OverdueCharge = '--';
                    switch ($User->borrowedBooksHistory[$i]['RecordStatus']) {
                        case 'LOST':
                            $class = 'row_drafted';
                            if ($User->borrowedBooksHistory[$i]['RecordStatus'] == 'WAIVED') {
                                $overdueCharge = $User->borrowedBooksHistory[$i]['PaymentReceived'];
                            } else {
                                $overdueCharge = $User->borrowedBooksHistory[$i]['Payment'];
                            }
                            $OverdueCharge = ($overdueCharge == '') ? '--' : '$' . $overdueCharge;
                            break;
                        case 'RENEWED':
                            $class = 'row_waiting';
                            break;
                        default:
                            if ($isOverdue) {
                                // $class = 'row_suspend';
                                $class = '';
                                // Show Payment depends on payment Status
                                if ($User->borrowedBooksHistory[$i]['PaymentStatus'] == 'OUTSTANDING' || $User->borrowedBooksHistory[$i]['PaymentStatus'] == 'SETTLED') {
                                    $OverdueCharge = '$' . $User->borrowedBooksHistory[$i]['Payment'];
                                } else {
                                    $OverdueCharge = (! $User->borrowedBooksHistory[$i]['PaymentReceived']) ? '--' : '$' . $User->borrowedBooksHistory[$i]['PaymentReceived'];
                                }
                            } else {
                                $class = '';
                                $OverdueCharge = '--';
                            }
                            break;
                    }
                    $x .= '		<tr class="' . $class . '">
							      <td>' . ($nCount + 1) . '</td> 
							      <td>' . ($User->borrowedBooksHistory[$i]['BookTitle'] ? $User->borrowedBooksHistory[$i]['BookTitle'] . ' [' . $User->borrowedBooksHistory[$i]['BarCode'] . ']' : '--') . '</td> 
							      <td>' . ($User->borrowedBooksHistory[$i]['BorrowTime'] ? Date('Y-m-d', strtotime($User->borrowedBooksHistory[$i]['BorrowTime'])) : '--') . '</td>
							      <td>' . ($User->borrowedBooksHistory[$i]['DueDate'] ? $User->borrowedBooksHistory[$i]['DueDate'] : '--') . '</td>
							      <td>' . ($Lang["libms"]["SearchUser"]["ReturnBookStatus"][$User->borrowedBooksHistory[$i]['RecordStatus']] ? $Lang["libms"]["SearchUser"]["ReturnBookStatus"][$User->borrowedBooksHistory[$i]['RecordStatus']] : '--') . '</td>
							      <td>' . ($User->borrowedBooksHistory[$i]['ReturnedTime'] != '0000-00-00 00:00:00' ? Date('Y-m-d', strtotime($User->borrowedBooksHistory[$i]['ReturnedTime'])) : '--') . '</td>
							      <td>' . $OverdueCharge . '</td>
							      <td>&nbsp;</td>
							    </tr>';
                    
                    $nCount ++;
                }
            }
            $x .= ' 	</tbody>
				</table>';
            
            // show remark
            if ($checkboxRemark != '') {
                $x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
                        <tr>
                            <td class="tabletextremark">' . $checkboxRemark . '</td>
                        </tr>
                    </table>';
            }
            
            $x .= '	<input type="hidden" name="uID" id="uID" value="' . base64_encode($User->userInfo['UserID']) . '">
			  </form>
				<script>
					function ReturnBooks(){ 
						if($(\'input[name="UniqueIDs[]"]:checked\').length==0){
							alert(\'' . $Lang["libms"]["tableNoCheckedCheckbox"] . '\');
							return false;
						}
						var t = confirm(\'' . $Lang["libms"]["SearchUser"]["ReturnBookConfirm"] . '\');
						if (t){
							$(\'#BorrowedDiv\').prepend(\'<center>' . $Lang["libms"]["ajaxLoading"] . '</center>\');
							$.ajax({
						        type : "POST",
						        url : "ajax_return_book.php",
						        data: $(\'#BorrowedForm\').serialize(),
						        success : function(msg) {
									var Return = JSON.parse(msg);
									$(\'#BorrowedDiv\').html(Return[1]);
									$(\'#OverdueDiv\').html(Return[2]);
									$(\'#UserBalance\').html(Return[3]);
									Get_Return_Message(Return[0]);
						        }
						    });
						}	
					}
				</script>
			</div>
		</div>';
            return $x;
        }

        // Reserved
        function Get_Reserved_Books_Table($User, $curPage = 1)
        {
            global $Lang, $linterface;
            $style = $curPage == 'Reserved' ? '' : 'style="display:none;"';
            $x = '<div id="Reserved" ' . $style . '>';
            $x .= $linterface->GET_NAVIGATION2_IP25($Lang["libms"]["CirculationManagement"]["reserved"]) . '
				<div class="table_board">
				<form name="ReservedForm" id="ReservedForm">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td valign="bottom"><div class="table_filter"></div></td>
					    <td valign="bottom">
					    	<div class="common_table_tool">
								<a href="#" class="tool_reject" title="' . $Lang["libms"]["SearchUser"]["CancelReserved"] . '" onclick="CancelReserved();">' . $Lang["libms"]["SearchUser"]["CancelReserved"] . '</a>
					    	</div>
					    </td>
					  </tr>
					</table>
					<table class="common_table_list">
					  <thead>
					    <tr>
					      <th class="num_check">#</th>
					      <th> ' . $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['BookTitle'] . '</th> 
					      <th> ' . $Lang["libms"]["CirculationManagement"]["reserveday"] . '</th>
					      <th> ' . $Lang['libms']['bookmanagement']['expired_book_reserve'] . '</th>
					      <th> ' . $Lang["libms"]["label"]["BookCode"] . '</th>					      		
					      <th> ' . $Lang["libms"]["CirculationManagement"]["status"] . '</th>
						  <th class="num_check" ><input type="checkbox" name="CheckAll" id="CheckAll" onclick="Set_Checkbox_Value(\'ReservationIDs[]\',this.checked);"/></th>		      		
					      </tr>
					  </thead>
					  <tbody>';
            $iMax = sizeof($User->reservedBooks);
            if ($iMax == 0) {
                $x .= '<tr><td align="center" colspan="100%">' . $Lang['libms']['NoRecordAtThisMoment'] . '</td></tr>';
            } else {
                for ($i = 0; $i < $iMax; $i ++) {
                    if ($User->reservedBooks[$i]['RecordStatus'] == 'WAITING') {
                        $Sataus = $Lang["libms"]["book_reserved_status"]["WAITING"];
                    } else if ($User->reservedBooks[$i]['RecordStatus'] == 'READY') {
                        $Sataus = $Lang["libms"]["book_reserved_status"]["READY"];
                    }
                    
                    $x .= '		<tr>
							      <td>' . ($i + 1) . '</td> 
							      <td>' . ($User->reservedBooks[$i]['BookTitle'] ? $User->reservedBooks[$i]['BookTitle'] . ' [' . $User->reservedBooks[$i]['BarCode'] . ']' : '--') . '</td> 
							      <td>' . ($User->reservedBooks[$i]['ReserveTime'] ? Date('Y-m-d', strtotime($User->reservedBooks[$i]['ReserveTime'])) : '--') . '</td>
							      <td>' . ($User->reservedBooks[$i]['ExpiryDate'] ? $User->reservedBooks[$i]['ExpiryDate'] : '--') . '</td>
							      <td>' . ($User->reservedBooks[$i]['BookCode'] ? $User->reservedBooks[$i]['BookCode'] : '--') . '</td>
							      <td>' . ($User->reservedBooks[$i]['RecordStatus'] ? $Sataus : '--') . '</td>
								  <td><input type="checkbox" name="ReservationIDs[]" id="ReservationIDs[]" value="' . $User->reservedBooks[$i]['ReservationID'] . '"/></td>								
							    </tr>';
                }
            }
            $x .= ' 	</tbody>
				</table>
				<input type="hidden" name="uID" id="uID" value="' . base64_encode($User->userInfo['UserID']) . '">
			  </form>
				<script>
					function CancelReserved(){
						if($(\'input[name="ReservationIDs[]"]:checked\').length==0){	
							alert(\'' . $Lang["libms"]["tableNoCheckedCheckbox"] . '\');
							return false;
						}
						var t = confirm(\'' . $Lang["libms"]["SearchUser"]["CancelReservedConfirm"] . '\');
						if (t){
							$(\'#ReservedDiv\').prepend(\'<center>' . $Lang["libms"]["ajaxLoading"] . '</center>\');
							$.ajax({
						        type : "POST",
						        url : "ajax_cancel_reserved.php",
						        data: $(\'#ReservedForm\').serialize(),
						        success : function(msg) {
									var Return = JSON.parse(msg);
									$(\'#ReservedDiv\').html(Return[1]);
									Get_Return_Message(Return[0]);
						        }
						    });
						}	
					}
				</script>
			</div>
		</div>';
            return $x;
        }

        // Overdue
        function Get_Overdue_Records_Table($User, $curPage = 1)
        {
            global $Lang, $linterface;
            $style = $curPage == 'Overdue' ? '' : 'style="display:none;"';
            $x = '<div id="Overdue" ' . $style . '>';
            $x .= $linterface->GET_NAVIGATION2_IP25($Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["overdue"]) . '
				<div class="table_board">
				<form name="OverdueForm" id="OverdueForm">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td valign="bottom"><div class="table_filter"></div></td>
					    <td valign="bottom">
					    	<div class="common_table_tool">
					    		<a href="#" class="tool_approve" title="' . $Lang["libms"]["SearchUser"]["OverduePay"] . '" onclick="PayOverdue();">' . $Lang["libms"]["SearchUser"]["OverduePay"] . '</a>
					    		<a href="#" class="tool_reject" title="' . $Lang["libms"]["report"]["fine_waived"] . '" onclick="WaiveOverdue();">' . $Lang["libms"]["report"]["fine_waived"] . '</a>
					    	</div>
					    </td>
					  </tr>
					</table>
					<table class="common_table_list">
					  <thead>
					    <tr>
					      <th class="num_check">#</th>
					      <th> ' . $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['BookTitle'] . '</th> 
					      <th> ' . $Lang["libms"]["CirculationManagement"]["overdueday"] . '</th>
					      <th> ' . $Lang["libms"]["CirculationManagement"]["payment"] . '</th>					      		
					      <th> ' . $Lang["libms"]["report"]["fine_outstanding"] . '</th>					      		
					      <th> ' . $Lang["libms"]["CirculationManagement"]["pay_now"] . '</th>					      		
					      <th class="num_check" ><input type="checkbox" name="CheckAll" id="CheckAll" onclick="Set_Checkbox_Value(\'OverdueLogIDs[]\',this.checked);"/></th>
					      </tr>
					  </thead>
					  <tbody>';
            $iMax = sizeof($User->overdueRecords);
            if ($iMax == 0) {
                $x .= '<tr><td align="center" colspan="100%">' . $Lang['libms']['NoRecordAtThisMoment'] . '</td></tr>';
            } else {
                for ($i = 0; $i < $iMax; $i ++) {
                    $amountLeft = $User->overdueRecords[$i]['Payment'] - $User->overdueRecords[$i]['PaymentReceived'];
                    $x .= '		<tr>
							      <td>' . ($i + 1) . '</td> 
							      <td>' . ($User->overdueRecords[$i]['BookTitle'] ? $User->overdueRecords[$i]['BookTitle'] . ' [' . $User->overdueRecords[$i]['BarCode'] . ']' : '--') . '</td> 
							      <td>' . ($User->overdueRecords[$i]['DaysCount'] ? $User->overdueRecords[$i]['DaysCount'] : '--') . '</td>
							      <td>' . ($User->overdueRecords[$i]['Payment'] ? '$' . $User->overdueRecords[$i]['Payment'] : '--') . '</td>
							      <td>' . ($User->overdueRecords[$i]['Payment'] ? '$' . $amountLeft : '--') . '</td>
							      <td>' . ($User->overdueRecords[$i]['Payment'] ? '$<input data-amount="' . $amountLeft . '" class="textboxnum2" type="text" name="payAmount[' . $User->overdueRecords[$i]['OverDueLogID'] . ']" id="payAmount_' . $User->overdueRecords[$i]['OverDueLogID'] . '" value="' . $amountLeft . '">' : '--') . '</td>
							      <td><input type="checkbox" name="OverdueLogIDs[]" id="OverdueLogIDs[]" value="' . $User->overdueRecords[$i]['OverDueLogID'] . '"/></td>
							    </tr>';
                }
            }
            $x .= ' 	</tbody>
				</table>
				<input type="hidden" name="uID" id="uID" value="' . base64_encode($User->userInfo['UserID']) . '">
			  </form>
				<script>
					$(\'input.textboxnum2\').change(function(){
						var amount = parseFloat($(this).attr(\'data-amount\'));
						var input = parseFloat(this.value) ;
						if(isNaN(input) || (input > amount)  || (input <0)){ 
							$(this).val(amount);
						}else{
							this.value = parseFloat(input).toFixed(2);
						}
					});

					function PayOverdue(){
						if($(\'input[name="OverdueLogIDs[]"]:checked\').length==0){		
							alert(\'' . $Lang["libms"]["tableNoCheckedCheckbox"] . '\');
							return false;
						}
						var t = confirm(\'' . $Lang["libms"]["SearchUser"]["OverduePayConfirm"] . '\');
						if (t){
							$(\'#OverdueDiv\').prepend(\'<center>' . $Lang["libms"]["ajaxLoading"] . '</center>\');
							$.ajax({
						        type : "POST",
						        url : "ajax_pay_overdue.php",
						        data: $(\'#OverdueForm\').serialize(),
						        success : function(msg) {
									var Return = JSON.parse(msg);
									$(\'#OverdueDiv\').html(Return[1]);
									$(\'#BorrowedDiv\').html(Return[2]); 
									$(\'#UserBalance\').html(Return[3]);
									Get_Return_Message(Return[0]);
						        }
						    });
						}	
					}

					function WaiveOverdue(){
						if($(\'input[name="OverdueLogIDs[]"]:checked\').length==0){
							alert(\'' . $Lang["libms"]["tableNoCheckedCheckbox"] . '\');
							return false;
						}
						var t = confirm(\'' . $Lang["libms"]["SearchUser"]["OverdueWaiveConfirm"] . '\');
						if (t){
							$(\'#OverdueDiv\').prepend(\'<center>' . $Lang["libms"]["ajaxLoading"] . '</center>\');
							$.ajax({
						        type : "POST",
						        url : "ajax_waive_overdue.php",
						        data: $(\'#OverdueForm\').serialize(),
						        success : function(msg) {
									var Return = JSON.parse(msg);
									$(\'#OverdueDiv\').html(Return[1]);
									$(\'#BorrowedDiv\').html(Return[2]);
									$(\'#UserBalance\').html(Return[3]);
									Get_Return_Message(Return[0]);
						        }
						    });
						}	
					}
				</script>
			</div>
		</div>';
            return $x;
        }

        // eBookStats
        function Get_Class_View($UsersTotalReadTimes, $StartDate, $EndDate, $showPercentageCol=false, $percentage='', $type='')
        {
            global $Lang, $linterface, $junior_mck;
            $schoolName = (isset($junior_mck)) ? iconv("big5", "utf-8//IGNORE", $_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
            $x = '
			<div class="Conntent_tool print_hide">
					<a href="javascript:ClassViewexport();" class="export">' . $Lang['Btn']['Export'] . '</a>
					<a href="javascript:void(0)" id="ClassViewPrint" class="print"> ' . $Lang['Btn']['Print'] . '</a>
			</div>
			<br style="clear:both" />
				<div class="table_board">';
            if (count($UsersTotalReadTimes) == 0) {
                $x .= '<div style="text-align:center">' . $Lang['libms']['NoRecordAtThisMoment'] . '</div>';
            } else {
                $x .= '	<table id="sortable" class="sortable common_table_list">
					  <thead>
					    <tr>
					      <th> ' . $Lang["libms"]["report"]["username"] . '</th>
					      <th> ' . $Lang['libms']['bookmanagement']['ClassName'] . '</th>
					      <th> ' . $Lang['libms']['bookmanagement']['ClassNumber'] . '</th>
					      <th> ' . $Lang["libms"]["eBookStat"]["TotalBooks"] . '</th>';
                if ($showPercentageCol) {
                    if ($type == 'up') {
                        $x .= '<th> ' . sprintf($Lang["libms"]["eBookStat"]["totalBooksByPercentage"],$percentage). '</th>';
                        $x .= '<th> ' . sprintf($Lang["libms"]["eBookStat"]["totalReadByPercentage"],$percentage). '</th>';
                    }
                    else if ($type == 'down') {
                        $x .= '<th> ' . sprintf($Lang["libms"]["eBookStat"]["totalBooksByPercentageDown"],$percentage). '</th>';
                        $x .= '<th> ' . sprintf($Lang["libms"]["eBookStat"]["totalReadByPercentageDown"],$percentage). '</th>';
                    }
                }
                    $x .= '<th> ' . $Lang["libms"]["eBookStat"]["TotalReads"] . '</th>
					      <th> ' . $Lang["libms"]["eBookStat"]["TotalReviews"] . '</th>
					      </tr>
					  </thead>
					  <tbody>';
                
                $total_Books = 0;
                $total_Reads = 0;
                $total_Reviews = 0;
                $total_PercentageCount = 0;
                $total_ReadCount = 0;
                
                foreach ((array) $UsersTotalReadTimes as $uID => $row) {
                    $x .= '<tr>
							<td>' . ($row['Name'] ? $row['Name'] : '--') . '</td>	
							<td>' . ($row['ClassName'] ? $row['ClassName'] : '--') . '</td>	
							<td>' . ($row['ClassNumber'] ? $row['ClassNumber'] : '--') . '</td>	
							<td>' . ($row['TotalBooks'] ? $row['TotalBooks'] : '0') . '</td>';
                    if ($showPercentageCol) {
                        $x .= '<td>' . ($row['PercentageCount'] ? $row['PercentageCount'] : '0') . '</td>';
                        $total_PercentageCount += $row['PercentageCount'] ? $row['PercentageCount'] : 0;
                        $x .= '<td><a class="UserProgressDetails" data-user="' . $uID . '" href="javascript:void(0);">' . ($row['ReadCount'] ? $row['ReadCount'] : '0') . '</a></td>';
                        $total_ReadCount += $row['ReadCount'] ? $row['ReadCount'] : 0;
                    }
                    $x .= '<td><a class="UserDetails" data-user="' . $uID . '" href="javascript:void(0);">' . ($row['TotalReads'] ? $row['TotalReads'] : '0') . '</a></td>	
							<td><a class="ReviewDetails" data-user="' . $uID . '" href="javascript:void(0);">' . ($row['TotalReviews'] ? $row['TotalReviews'] : '0') . '</a></td>	
						  </tr>';
                    
                    $total_Books += $row['TotalBooks'] ? $row['TotalBooks'] : 0;
                    $total_Reads += $row['TotalReads'] ? $row['TotalReads'] : 0;
                    $total_Reviews += $row['TotalReviews'] ? $row['TotalReviews'] : 0;
                }
                
                $x .= ' 	</tbody>';
                
                // # grand total row
                $x .= '<tfoot>
						  <tr>
							<td align=right colspan=4>' . $Lang["libms"]["report"]["total"] . '</td>	
							<td>' . $total_Books . '</td>';
                if ($showPercentageCol) {
                    $x .= '<td>' . $total_PercentageCount. '</td>';
                    $x .= '<td>' . $total_ReadCount. '</td>';
                }
                $x .= '<td>' . $total_Reads . '</td>
							<td>' . $total_Reviews . '</td>
						  </tr>
					  </tfoot>';
                
                $x .= '	</table>';
            }
            $x .= '	<input type="hidden" name="StartDate" id="StartDate" value="' . $StartDate . '">
				<input type="hidden" name="EndDate" id="EndDate" value="' . $EndDate . '">
				<script>
				$("#ClassViewPrint").click(function(){
					document.form1.target="_blank";
					document.form1.action = "ajax_class_view.php?action=print";
					document.form1.submit();
				});
				$(".UserDetails").click(function(){
					var uID = $(this).attr("data-user");
					var url = "ajax_student_view.php";
                    var withProgress = 0;
					getRecords(uID,url,withProgress);
				});
				$(".UserProgressDetails").click(function(){
					var uID = $(this).attr("data-user");
					var url = "ajax_student_view.php";
                    var withProgress = 1;
					getRecords(uID,url,withProgress);
				});
				$(".ReviewDetails").click(function(){
					var uID = $(this).attr("data-user");
					var url = "ajax_review_view.php";
                    var withProgress = 0;
					getRecords(uID,url,withProgress);
				});
				function getRecords(uID,url,withProgress){
					var StartDate = $("#StartDate").val(); 		
					var EndDate = $("#EndDate").val();
                    var percentage;
                    var readingFilter;
                    
                    if (withProgress == 1) {
                        readingFilter = $("input:radio[name=\'readingFilter\']:checked").val();
                        if (readingFilter == "readup") {
                            percentage = $("#percentage").val();
                        }
                        else if (readingFilter == "readdown") {
                            percentage = $("#percentageDown").val();
                        }
                    }
                    else {
                        readingFilter = "";
                        percentage = "";
                    }

					$("html, body").animate({scrollTop:$("#Content").offset().top}, "slow");
					$("#TableDiv").hide();
					$("#StudentDiv").html("<center>' . $Lang["libms"]["ajaxLoading"] . '</center>");
					$("#StudentDiv").show();
					$.ajax({
			        type : "POST",
			        url : url,
			        data: {uID : uID, StartDate : StartDate, EndDate : EndDate, readingFilter : readingFilter, percentage : percentage},
			        success : function(msg) { 
							$("#StudentDiv").html(msg);
							if(document.getElementById("sortable2") != null){
							sorttable.makeSortable(document.getElementById("sortable2"))
							}
			        }
					});	
				}

				</script>
		</div>';
            return $x;
        }

        function Get_Student_View($StudentRecords)
        {
            global $Lang, $linterface;
            global $PATH_WRT_ROOT, $LAYOUT_SKIN;
            // pending : print and export in student view
            // <div class="Conntent_tool">
            // <a href="javascript:click_export();" class="export">'.$Lang['Btn']['Export'].'</a>
            // <a href="javascript:click_print()" class="print"> '.$Lang['Btn']['Print'].'</a>
            // </div>
            // <br style="clear:both" />
            $x = '<script>
				function classView(){ $("#StudentDiv").hide(); $("#TableDiv").show()};					
			</script>';
            if (count($StudentRecords) == 0) {
                $x .= '<div style="text-align:center">' . $Lang['libms']['NoRecordAtThisMoment'] . '</div>';
            } else {
                $x .= '
				<div class="table_board">
					<table id="sortable2" class="sortable common_table_list">
					  <thead>
					    <tr>
					      <th> ' . $Lang['libms']['bookmanagement']['bookTitle'] . '</th> 
					      <th> ' . $Lang["libms"]["report"]["ResponsibilityBy"] . '</th>
					      <th> ' . $Lang["libms"]["eBookStat"]["TotalReads"] . '</th>
					      <th> ' . $Lang['libms']['bookmanagement']['progress'] . '</th>
					      <th> ' . $Lang["libms"]["eBookStat"]["LastReads"] . '</th>
					      </tr>
					  </thead>
					  <tbody>';
                $total_Reads = 0;
                foreach ((array) $StudentRecords as $row) {
                    $x .= '<tr>
							<td>' . ($row['Title'] ? $row['Title'] : '--') . '</td>	
							<td>' . ($row['Author'] ? $row['Author'] : '--') . '</td>	
							<td>' . ($row['ReadCount'] ? $row['ReadCount'] : '--') . '</td>	
							<td>' . ($row['Percentage'] ? $row['Percentage'] : '0') . '%</td>	
							<td>' . ($row['LastRead'] ? $row['LastRead'] : '0') . '</td>	
						  </tr>';
                    $total_Reads += $row['ReadCount'] ? $row['ReadCount'] : 0;
                }
                $x .= ' 	</tbody>';
                // # grand total row
                $x .= '<tfoot>
					  <tr>
						<td align=right colspan=3>' . $Lang["libms"]["report"]["total"] . '</td>	
						<td>' . $total_Reads . '</td>
						<td colspan=2></td>
					  </tr>
				  </tfoot>';
                
                $x .= '	</table>';
            }
            $x .= '	<div style="text-align:center">
					<br style="clear:both" />
					<input type="button" name="SubmitBtn" id="SubmitBtn" class="formbutton_v30 print_hide " onclick="javascript:classView();" value="' . $Lang["libms"]["eBookStat"]["Back"] . '">
				<div>
			</div>
		</div>';
            return $x;
        }

        function Get_Review_View($ReviewRecords)
        {
            global $Lang, $linterface;
            global $PATH_WRT_ROOT, $LAYOUT_SKIN;
            // <div class="Conntent_tool">
            // <a href="javascript:click_export();" class="export">'.$Lang['Btn']['Export'].'</a>
            // <a href="javascript:click_print()" class="print"> '.$Lang['Btn']['Print'].'</a>
            // </div>
            // <br style="clear:both" />
            $x = '<script>
				function classView(){ $("#StudentDiv").hide(); $("#TableDiv").show()};					
			</script>';
            if (count($ReviewRecords) == 0) {
                $x .= '<div style="text-align:center">' . $Lang['libms']['NoRecordAtThisMoment'] . '</div>';
            } else {
                $x .= ' 
				<div class="table_board">
					<table id="sortable2" class="sortable common_table_list">
					  <thead>
					    <tr>
					      <th> ' . $Lang['libms']['bookmanagement']['bookTitle'] . '</th> 
					      <th> ' . $Lang["libms"]["eBookStat"]["ReviewContent"] . '</th>
					      <th> ' . $Lang["libms"]["eBookStat"]["ReviewRating"] . '</th>
					      <th> ' . $Lang["libms"]["eBookStat"]["ReviewHelpful"] . '</th>
					      <th> ' . $Lang["libms"]["eBookStat"]["ReviewDate"] . '</th>
					      </tr>
					  </thead>
					  <tbody>';
                
                foreach ((array) $ReviewRecords as $row) {
                    $x .= '<tr>
							<td>' . ($row['Title'] ? $row['Title'] : '--') . '</td>	
							<td>' . ($row['Content'] ? nl2br($row['Content']) : '--') . '</td>	
							<td>' . ($row['Rating'] ? $row['Rating'] : '--') . '</td>	
							<td>' . ($row['HelpfulCount'] ? $row['HelpfulCount'] : '0') . '</td>	
							<td>' . ($row['DateModified'] ? $row['DateModified'] : '0') . '</td>	
						  </tr>';
                }
                $x .= ' 	</tbody>
				</table>';
            }
            $x .= '	<div style="text-align:center">
					<br style="clear:both" />
					<input type="button" name="SubmitBtn" id="SubmitBtn" class="formbutton_v30 print_hide " onclick="javascript:classView();" value="' . $Lang["libms"]["eBookStat"]["Back"] . '">
				<div>	
			</div>
		</div>';
            return $x;
        }

        function Get_Class_Users($ClassName)
        {
            // $sql = "select * from LIBMS_USER where ClassName='{$ClassName}' order by ClassNumber";
            $sql = "select * from LIBMS_USER where ClassName='{$ClassName}' and RecordStatus='1' order by ClassNumber";
            $result = $this->returnArray($sql);
            return $result;
        }

        /*
         * Create selection (option) list by a field name, support selection of multiple items
         * Note: There's no {Select All} if $multiple = ture
         * The option list:
         * {Select All}
         * {Not Defined}
         * {Others}
         * @param: $selectedVal -- a string or array of strings
         */
        function get_xxxFilter_html($table, $fieldname, $selectedVal = '', $size = 1, $multiple = false, $includeNotDefined = true, $extraPara = '')
        {
            global $Lang;
            
            if (! is_array($selectedVal)) {
                $selectedVal = array(
                    $selectedVal
                );
            }
            $multipleStr = ($multiple) ? 'multiple' : '';
            
            switch ($fieldname) {
                case "Subject":
                    $result = $this->getBookSubjects();
                    break;
                // case "Language":
                // $result = $this->getBookLanguage();
                // break;
                default:
                    $fields[] = $fieldname;
                    $fields[] = $Lang["libms"]["sql_field"]["Description"];
                    $result = $this->SELECTFROMTABLE($table, $fields);
                    break;
            }
            
            $options = '';
            if (! empty($result)) {
                $default_sel = 'selected="selected"';
                foreach ($result as $row) {
                    $code = $row[$fieldname];
                    $name = $row[$Lang["libms"]["sql_field"]["Description"]];
                    if (in_array(htmlspecialchars_decode($code, ENT_QUOTES), $selectedVal)) {
                        $selected = 'selected="selected"';
                        $default_sel = '';
                    } else {
                        $selected = '';
                    }
                    
                    $options .= <<<EOL
	<option value="{$code}" {$selected} >{$name}</option>
EOL;
                }
            }
            
            $selectAll = ($multiple) ? "" : "<option value='' {$default_sel} > -- " . $Lang["libms"]["batch_edit"]["PleaseSelect"] . " -- </option>";
            $notDefined = ($includeNotDefined) ? "<option value='" . $Lang['General']['EmptySymbol'] . "'>" . $Lang["libms"]["report"]["cannot_define"] . "</option>" : "";
            $x = <<<EOL
	<select name="{$fieldname}" id="{$fieldname}" size="{$size}" {$multipleStr} {$extraPara}>
		{$selectAll}
		{$notDefined}
		{$options}</select>
EOL;
            
            if ($multiple) {
                $x .= '<span class="tabletextremark">' . $Lang['libms']['report']['PressCtrlKey'] . '</span>';
            }
            return $x;
        }
 // end function get_xxxFilter_html()
        
        /*
         * return distinct subject from LIBMS_BOOK
         */
        function getBookSubjects()
        {
            global $intranet_session_language;
            $descriptionName = ($intranet_session_language == 'en') ? 'DescriptionEn' : 'DescriptionChi';
            $sql = "SELECT 	
						`Subject` , `Subject` as $descriptionName 
				FROM 	
						`LIBMS_BOOK` 
				WHERE 	
						(RecordStatus<>'DELETE' OR RecordStatus IS NULL)
				AND Subject IS NOT NULL
				AND Subject<>''
				GROUP BY `Subject` ORDER BY `Subject`";
            
            $rs = $this->returnArray($sql);
            return $rs;
        }
 // end function getBookSubjects()
          
        // sql for book lost report
        function returnSQLforLostReport()
        {
            global $Lang;
            
            $lost_book_price_by = $this->get_system_setting('circulation_lost_book_price_by');
            if ($lost_book_price_by == 1) {
                $priceField = "bu.`ListPrice`";
                $SndPriceField = "bu.`PurchasePrice`";
            } else {
                $priceField = "bu.`PurchasePrice`"; // if set fixed price but no record in OVERDUE_LOG, use PurchasePrice
                $SndPriceField = "bu.`ListPrice`";
            }
            
            $current_period = getPeriodOfAcademicYear(Get_Current_Academic_Year_ID());
            $today = date('Y-m-d');
            // avoid & problem when encrypted code in production
            if ($current_period) {
                $current_period['StartDate'] = $current_period['StartDate'] ? date('Y-m-d', strtotime($current_period['StartDate'])) : $today;
                $current_period['EndDate'] = $current_period['EndDate'] ? date('Y-m-d', strtotime($current_period['EndDate'])) : $today;
            } else {
                $current_period['StartDate'] = $today;
                $current_period['EndDate'] = $today;
            }
            // foreach($current_period as &$date){
            // $date = date('Y-m-d',strtotime($date));
            // }
            
            $_REQUEST['DueDateAfter'] = empty($_REQUEST['DueDateAfter']) ? $current_period['StartDate'] : $_REQUEST['DueDateAfter'];
            $_REQUEST['DueDateBefore'] = empty($_REQUEST['DueDateBefore']) ? $current_period['EndDate'] : $_REQUEST['DueDateBefore'];
            
            $conds = " bu.`RecordStatus` LIKE 'LOST' ";
            
            // $conds=" 1=1 ";
            $keyword = trim($_POST['keyword']);
            if (! get_magic_quotes_gpc()) {
                $keyword = stripslashes($keyword);
            }
            
            if ($keyword != "") {
                $unconverted_keyword_sql = mysql_real_escape_string(str_replace("\\", "\\\\", $keyword));
                $converted_keyword_sql = special_sql_str($keyword);
                if ($unconverted_keyword_sql == $converted_keyword_sql) {
                    $conds .= " AND (
				bu.`ACNO` LIKE '%$unconverted_keyword_sql%' OR
				b.`CallNum` LIKE '%$unconverted_keyword_sql%' OR
				b.`ISBN` LIKE '%$unconverted_keyword_sql%' OR
				b.`BookTitle` LIKE '%$unconverted_keyword_sql%' OR
				b.`ResponsibilityBy1` LIKE '%$unconverted_keyword_sql%' OR
				blu.`UserLogin` LIKE '%$unconverted_keyword_sql%' OR
				blu.`UserEmail` LIKE '%$unconverted_keyword_sql%' OR
				blu.`EnglishName` LIKE '%$unconverted_keyword_sql%' OR
				blu.`ChineseName` LIKE '%$unconverted_keyword_sql%' OR
				blu.`BarCode` LIKE '%$unconverted_keyword_sql%' 
				)";
                } else {
                    $conds .= " AND (
				bu.`ACNO` LIKE '%$unconverted_keyword_sql%' OR
				b.`CallNum` LIKE '%$unconverted_keyword_sql%' OR
				b.`ISBN` LIKE '%$unconverted_keyword_sql%' OR
				b.`BookTitle` LIKE '%$unconverted_keyword_sql%' OR
				b.`ResponsibilityBy1` LIKE '%$unconverted_keyword_sql%' OR
				blu.`UserLogin` LIKE '%$unconverted_keyword_sql%' OR
				blu.`UserEmail` LIKE '%$unconverted_keyword_sql%' OR
				blu.`EnglishName` LIKE '%$unconverted_keyword_sql%' OR
				blu.`ChineseName` LIKE '%$unconverted_keyword_sql%' OR
				blu.`BarCode` LIKE '%$unconverted_keyword_sql%' OR
 
				bu.`ACNO` LIKE '%$converted_keyword_sql%' OR
				b.`CallNum` LIKE '%$converted_keyword_sql%' OR
				b.`ISBN` LIKE '%$converted_keyword_sql%' OR
				b.`BookTitle` LIKE '%$converted_keyword_sql%' OR
				b.`ResponsibilityBy1` LIKE '%$converted_keyword_sql%' OR
				blu.`UserLogin` LIKE '%$converted_keyword_sql%' OR
				blu.`UserEmail` LIKE '%$converted_keyword_sql%' OR
				blu.`EnglishName` LIKE '%$converted_keyword_sql%' OR
				blu.`ChineseName` LIKE '%$converted_keyword_sql%' OR
				blu.`BarCode` LIKE '%$converted_keyword_sql%' 
				)";
                }
            }
            IF (! empty($_REQUEST['DueDateAfter'])) {
                $conds .= ' AND IF(ol.`DateCreated` is null, bu.`DateModified`, ol.`DateCreated`) >= ' . PHPToSQL($_REQUEST['DueDateAfter'] . ' 00:00:00');
            }
            IF (! empty($_REQUEST['DueDateBefore'])) {
                $conds .= ' AND IF(ol.`DateCreated` is null, bu.`DateModified`, ol.`DateCreated`) <= ' . PHPToSQL($_REQUEST['DueDateBefore'] . ' 23:59:59');
            }
            
            // $BookID_For_CONCAT = (isset($junior_mck)) ? "CONVERT(`BookID` USING utf8)" : "`BookID`";
            
            $sql = <<<EOL

SELECT 
	IF(ol.`DateCreated` is null, bu.`DateModified`, ol.`DateCreated`) as DateModified, 
	IF(blu.`{$Lang['libms']['SQL']['UserNameFeild']}` is null or blu.`{$Lang['libms']['SQL']['UserNameFeild']}` = '', ' -- ', blu.`{$Lang['libms']['SQL']['UserNameFeild']}`) as UserName ,
	bu.ACNO,
	IF((b.`CallNum`='' or b.`CallNum` is null) and (b.`CallNum2`='' or b.`CallNum2` is null), ' -- ', CONCAT_WS(  ' ', b.`CallNum` , b.`CallNum2` )) AS CallNum,
	b.`BookTitle` , 
	IF(b.`ResponsibilityBy1` = '' or b.`ResponsibilityBy1` is null, ' -- ', b.`ResponsibilityBy1`) as `ResponsibilityBy1`,
	IF(bu.PurchasePrice is null, ' -- ', bu.PurchasePrice) as PurchasePrice,
	IF(bu.ListPrice is null, ' -- ', bu.ListPrice) as ListPrice,
	IF(ol.`LostBookPriceBy` is null, IF($priceField is null or $priceField='' , IF($SndPriceField is null or $SndPriceField='', ' -- ', $SndPriceField),$priceField), IF(ol.`Payment` is null or ol.`Payment`='', ' -- ', ol.`Payment`)) as Penalty 
FROM (	SELECT bgu.* FROM (
			SELECT bl.`UniqueID`, bl.`BorrowLogID`, u.`EnglishName`, u.`ChineseName`, u.`UserLogin`, u.`BarCode`, u.`UserEmail`
			FROM  `LIBMS_BORROW_LOG` bl
			INNER JOIN `LIBMS_USER` u ON bl.`UserID`=u.`UserID`
			WHERE bl.`RecordStatus`='LOST'
			ORDER BY bl.`UniqueID`, bl.`DateModified` DESC) AS bgu 
		GROUP BY bgu.`UniqueID`
	) AS blu
INNER JOIN `LIBMS_BOOK_UNIQUE` bu ON bu.`UniqueID`=blu.`UniqueID`
INNER JOIN `LIBMS_BOOK` b ON b.`BookID`=bu.`BookID`
LEFT JOIN `LIBMS_OVERDUE_LOG` ol ON ol.`BorrowLogID`=blu.`BorrowLogID` AND ol.RecordStatus<>'DELETE' AND (ol.DaysCount IS NULL OR ol.DaysCount=0)
WHERE
	{$conds}
EOL;
            
            return $sql;
        }
 // end returnSQLforLostReport()
        
        /*
         * check if book is being borrowed by UniqueID
         * return ACNO
         */
        function BookInLoanByUniqueID($UniqueID = array())
        {
            $result = '';
            if (is_array($UniqueID) && count($UniqueID) > 0) {
                $unique_id_list = implode("','", $UniqueID);
                
                $sql = " SELECT bu.ACNO
						FROM 
							LIBMS_BORROW_LOG lrl
							INNER JOIN LIBMS_BOOK_UNIQUE bu ON bu.UniqueID=lrl.UniqueID and lrl.RecordStatus='BORROWED'
						WHERE lrl.UniqueID IN ('$unique_id_list')
					";
                
                $result = $this->returnResultSet($sql);
            }
            return $result;
        }

        /*
         * check if book is being borrowed by BookID
         * return BookTitle
         */
        function BookInLoanByBookID($BookID = array())
        {
            $result = '';
            if (is_array($BookID) && count($BookID) > 0) {
                $book_id_list = implode("','", $BookID);
                
                $sql = " SELECT DISTINCT b.BookTitle
						FROM 
							LIBMS_BORROW_LOG lrl
							INNER JOIN LIBMS_BOOK b ON b.BookID=lrl.BookID and lrl.RecordStatus='BORROWED'
						WHERE lrl.BookID IN ('$book_id_list')
					";
                
                $result = $this->returnResultSet($sql);
            }
            return $result;
        }

        /*
         * return the first user (the earliest reservation) in book reservation waiting list by BookID
         */
        function getNextUserOfBookReservation($BookID, $ReservationID = array())
        {
            $sql = "SELECT r.ReservationID,
					" . getNameFieldByLang2('u.') . " as UserName,
                    u.UserID  
				FROM 
					LIBMS_USER u,
					LIBMS_RESERVATION_LOG r,
					LIBMS_RESERVATION_LOG r2
			    WHERE
					u.UserID=r.UserID 
				AND r.BookID = '" . $BookID . "' 
				AND r.RecordStatus='WAITING'
				AND r.ReservationID NOT IN ('" . implode("','", $ReservationID) . "')
				AND r2.BookID=r.BookID
				AND r2.RecordStatus='READY'
				AND r2.ReservationID IN ('" . implode("','", $ReservationID) . "')
		    	ORDER BY r.ReserveTime ASC LIMIT 1";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        /*
         * purpose: check if $code has been used in $table or not
         * return true if it's been used, false otherwise
         * @para $code -- checked code
         * $field -- field name to store code
         * $table -- used in table
         * $bookCategoryType -- Book Category Type, for $field='BookCategoryCode' only
         */
        function IsCodeUsed($code, $field, $table, $bookCategoryType = 1)
        {
            if (is_array($code) && count($code) > 0) {
                foreach ((array) $code as $k => $v) {
                    $code[$k] = $this->Get_Safe_Sql_Query($v);
                }
                $codes = implode("','", $code);
                
                $statusCond = ($table == 'LIBMS_BOOK' || $table == 'LIBMS_BOOK_UNIQUE') ? " AND (RecordStatus<>'DELETE' OR RecordStatus='' OR RecordStatus IS NULL)" : "";
                
                switch ($field) {
                    case 'BookCategoryCode':
                        $sql = "SELECT BookLanguageCode FROM LIBMS_BOOK_LANGUAGE WHERE BookCategoryType='" . $bookCategoryType . "'";
                        $languageCode = $this->returnVector($sql);
                        if (count($languageCode) > 0) {
                            $langCond = " AND (Language IN ('" . implode("','", $this->Get_Safe_Sql_Query($languageCode)) . "') OR Language IS NULL OR Language='')";
                        } else {
                            $langCond = " AND (Language IS NULL OR Language='')";
                        }
                        $sql = "SELECT BookCategoryCode FROM LIBMS_BOOK WHERE BookCategoryCode IN ('{$codes}') {$langCond} {$statusCond} LIMIT 1";
                        break;
                    
                    case 'CirculationTypeCode':
                        $sql = "SELECT CirculationTypeCode FROM LIBMS_BOOK WHERE CirculationTypeCode IN ('{$codes}') {$statusCond} UNION
							SELECT CirculationTypeCode FROM LIBMS_BOOK_UNIQUE WHERE CirculationTypeCode IN ('{$codes}')	{$statusCond} LIMIT 1";
                        break;
                    
                    case 'ResponsibilityCode':
                        $sql = "SELECT BookID FROM LIBMS_BOOK WHERE (ResponsibilityCode1 IN ('{$codes}') OR ResponsibilityCode2 IN ('{$codes}') OR ResponsibilityCode3 IN ('{$codes}')) {$statusCond} LIMIT 1";
                        break;
                    
                    default:
                        $sql = "SELECT {$field} FROM {$table} WHERE {$field} IN ('{$codes}') {$statusCond} LIMIT 1";
                        break;
                }
                
                $rs = $this->returnResultSet($sql);
                $ret = count($rs) ? true : false;
            } else {
                $ret = false;
            }
            return $ret;
        }

        // copy from libPCM and modify
        function checkCurrentIP()
        {
            $ip_allowed = false;
            
            $table = 'LIBMS_SECURITY_SETTING';
            $field = 'SettingValue';
            $condition = array();
            $condition['SettingName'] = PHPToSQL('IPList');
            $ip_list = $this->SELECTFROMTABLE($table, $field, $condition);
            if (count($ip_list)) {
                $ip_list = $ip_list[0]['SettingValue'];
            } else {
                $ip_list = '';
            }
            // check current IP within allowed IP addresses
            $allowed_IPs = trim($ip_list);
            $ip_addresses = explode("\n", $allowed_IPs);
            for ($i = 0; $i < count($ip_addresses); $i ++) {
                $ip_addresses[$i] = trim($ip_addresses[$i]);
                if (testip($ip_addresses[$i], getRemoteIpAddress())) {
                    $ip_allowed = true;
                }
            }
            if (in_array('0.0.0.0', $ip_addresses) || $allowed_IPs == '') {
                $ip_allowed = true;
            }
            
            return $ip_allowed;
        }

        // get all LocationCode from class management group
        function getLocationCodeFromClassManagementGroup($isClassManagementUser = false)
        {
            $sql = "SELECT DISTINCT g.LocationCode
                FROM LIBMS_CLASS_MANAGEMENT_GROUP g
                INNER JOIN LIBMS_LOCATION a ON a.LocationCode=g.LocationCode";
            if ($isClassManagementUser) {
                $sql .= " INNER JOIN LIBMS_CLASS_MANAGEMENT_GROUP_USER u ON u.GroupID=g.GroupID AND u.UserID='" . $_SESSION['UserID'] . "'";
            }
            $sql .= " ORDER BY g.LocationCode";
            $rs = $this->returnResultSet($sql);
            $locationCodeAry = BuildMultiKeyAssoc($rs, $asocKey = array(
                'LocationCode'
            ), $includedDBField = array(
                'LocationCode'
            ), $singleValue = 1);
            return $locationCodeAry;
        }

        function isLocationCodeInClassManagementGroup($locationCode, $isClassManagementUser = false)
        {
            if ($locationCode != '') {
                $locationCodeAry = $this->getLocationCodeFromClassManagementGroup($isClassManagementUser);
                if (in_array($locationCode, $locationCodeAry)) {
                    return true;
                }
            }
            return false; // default
        }
        
        function getBookReservedCount($bookID) {
            if (empty($bookID)) {
                return false;
            }
            $sql = "SELECT  COUNT(*) AS NoOfReservation
                    FROM
                            LIBMS_RESERVATION_LOG
                    WHERE
                            BookID='$bookID'
                    AND
                            RecordStatus IN ('WAITING','READY')";
            $result = $this->returnResultSet($sql);
            if (count($result)) {
                $noOfReservation = $result[0]['NoOfReservation'];
            }
            else {
                $noOfReservation = 0;
            }
            
            return $noOfReservation;
        }

        // used in cancel write-off
        function getLatestBorrowLogStatus($bookUniqueID) {
            if (empty($bookUniqueID)) {
                return '';
            }
            $sql = "SELECT RecordStatus FROM LIBMS_BORROW_LOG WHERE UniqueID='$bookUniqueID' ORDER BY BorrowLogID DESC LIMIT 1";
            $result = $this->returnResultSet($sql);
            return count($result) ? $result[0]['RecordStatus'] : '';
        }
        
        // used in cancel write-off
        function getLatestReservationLogStatus($bookUniqueID) {
            if (empty($bookUniqueID)) {
                return '';
            }
            $sql = "SELECT RecordStatus FROM LIBMS_RESERVATION_LOG WHERE BookUniqueID='$bookUniqueID' ORDER BY ReservationID DESC LIMIT 1";
            $result = $this->returnResultSet($sql);
            return count($result) ? $result[0]['RecordStatus'] : '';
        }
     
        // return rotated image, swap width and height if image orientation attribute exist
        function changeImageOrientation($image, $imagePath, $width, $height)
        {
            if ($image){
                $exif = @exif_read_data($imagePath);
                
                if (!empty($exif['Orientation'])) {
                    switch ($exif['Orientation']) {
                        case 3:
                            $image = imagerotate($image, 180, 0);
                            break;
                            
                        case 6:
                            $image = imagerotate($image, -90, 0);
                            $temp_width = $width;
                            $width = $height;
                            $height = $temp_width;
                            break;
                            
                        case 8:
                            $image = imagerotate($image, 90, 0);
                            $temp_width = $width;
                            $width = $height;
                            $height = $temp_width;
                            break;
                    }
                }
            }
            return array('image'=>$image, 'width'=>$width, 'height'=>$height);
        }
        
        function getOverdueBookInfo($overdueLogID)
        {
            $sql = "SELECT
                        bl.BorrowLogID,
                        bl.UniqueID,
                        bl.UserID,
                        ol.PaymentReceived,
                        ol.RecordStatus
                    FROM
                        LIBMS_OVERDUE_LOG ol
                        INNER JOIN LIBMS_BORROW_LOG bl ON bl.BorrowLogID=ol.BorrowLogID
                    WHERE
                        ol.OverDueLogID='".$overdueLogID."'
                        AND ol.RecordStatus<>'DELETE'";
            $result = $this->returnResultSet($sql);
            return $result;
        }
        
        function getOverdueInfoByBorrowLogID($borrowLogID, $excludeOverdueLogID)
        {
            $sql = "SELECT
                        ol.OverDueLogID,
                        ol.PaymentReceived
                    FROM
                        LIBMS_OVERDUE_LOG ol
                    WHERE
                        ol.BorrowLogID='".$borrowLogID."'
                        AND ol.OverDueLogID<>'".$excludeOverdueLogID."'
                        AND ol.RecordStatus NOT IN ('DELETE','WAIVED')
                        AND ol.PaymentReceived>0";
            $result = $this->returnResultSet($sql);
            return $result;
        }
        
    }
 // end class
 
    
    
    function CCtoSQL($aString = "", $ValueType = 1)
    {
        return PHPToSQL($aString);
    }

    function PHPToSQL($Value, $type = false, $quotes = true, $link_identifier = NULL)
    {
        if (is_array($Value)) {
            foreach ($Value as $key => $item) {
                $Value[$key] = PHPToSQL($item, false, $quotes);
            }
            return $Value;
        }
        if ($Value === null) {
            $Value = 'NULL';
        } else {
            if (! empty($type)) {
                if ($type == 'DATE') {
                    if (is_numeric($Value))
                        $Value = date('Y-m-d', $Value);
                    else
                        $Value = date('Y-m-d', strtotime($Value));
                } else if ($type == 'DATETIME') {
                    $Value = date('Y-m-d H:m:s', $Value);
                }
            }
            
            // if (!is_numeric($Value)) {
            // $Value = $link_identifier->real_escape_string($Value); //mysqli
            if (empty($link_identifier)) {
                $Value = mysql_real_escape_string($Value);
            } else {
                $Value = mysql_real_escape_string($Value, $link_identifier);
            }
            if ($quotes) {
                $Value = "'" . $Value . "'";
            }
            // }
        }
        return $Value;
    }
    
    if (! function_exists('json_encode')) {

        function json_encode($data)
        {
            switch ($type = gettype($data)) {
                case 'NULL':
                    return 'null';
                case 'boolean':
                    return ($data ? 'true' : 'false');
                case 'integer':
                case 'double':
                case 'float':
                    return $data;
                case 'string':
                    $tmp = str_replace('"', '\\"', $data);
                    $tmp = str_replace("\n", '\\n', $tmp);
                    $tmp = str_replace("\t", '\\t', $tmp);
                    $tmp = str_replace("\r", '\\r', $tmp);
                    
                    return '"' . $tmp . '"';
                case 'object':
                    $data = get_object_vars($data);
                case 'array':
                    $output_index_count = 0;
                    $output_indexed = array();
                    $output_associative = array();
                    foreach ($data as $key => $value) {
                        $output_indexed[] = json_encode($value);
                        $output_associative[] = json_encode($key) . ':' . json_encode($value);
                        if ($output_index_count !== NULL && $output_index_count ++ !== $key) {
                            $output_index_count = NULL;
                        }
                    }
                    if ($output_index_count !== NULL) {
                        return '[' . implode(',', $output_indexed) . ']';
                    } else {
                        return '{' . implode(',', $output_associative) . '}';
                    }
                default:
                    return ''; // Not supported
            }
        }
    }
    
    if (! function_exists('json_decode')) {

        function json_decode($json)
        {
            $comment = false;
            $out = '$x=';
            for ($i = 0; $i < strlen($json); $i ++) {
                if (! $comment) {
                    if (($json[$i] == '{') || ($json[$i] == '[')) {
                        $out .= 'array(';
                    } elseif (($json[$i] == '}') || ($json[$i] == ']')) {
                        $out .= ')';
                    } elseif ($json[$i] == ':') {
                        $out .= '=>';
                    } elseif ($json[$i] == ',') {
                        $out .= ',';
                    } elseif ($json[$i] == '"') {
                        $out .= '"';
                    }
                    /*
                     * elseif (!preg_match('/\s/', $json[$i])) {
                     * return null;
                     * }
                     */
                } else
                    $out .= $json[$i] == '$' ? '\$' : $json[$i];
                if ($json[$i] == '"' && $json[($i - 1)] != '\\')
                    $comment = ! $comment;
            }
            eval($out . ';');
            return $x;
        }
    }

    /**
     * do_email uses libwebmail :: sendModuleMail()
     *
     * @param
     *            $userID
     * @param
     *            $subject
     * @param
     *            $body
     * @param $email_to_mode $email_to_mode=
     *            'User';
     *            $email_to_mode= 'ParentOnly';
     *            $email_to_mode= 'CCParent';
     */
    function do_email($user_id, $subject, $body, $email_to_mode = 'User')
    {
        global $Lang, $intranet_root, $junior_mck, $root_path_found;
        // #SEND MAIL
        
        include_once ("{$root_path_found}/includes/global.php");
        // error_log("liblib 1162: {$intranet_root} ");
        // error_log("liblib 1163: {$root_path_found} ");
        
        include_once ("{$root_path_found}/includes/libemail.php");
        include_once ("{$root_path_found}/includes/libsendmail.php");
        include_once ("{$root_path_found}/includes/libwebmail.php");
        
        if (! is_array($user_id)) {
            $user_id = array(
                $user_id
            );
        }
        
        // error_log("liblib 1174: do_email({$user_id}, {$subject}...) ");
        $lwebmail = new libwebmail();
        
        if ($junior_mck > 0) {
            // use correct encoding for junior
            if ($lwebmail->has_webmail) {
                $lwebmail->set_charset("utf-8");
            } else {
                $subject = convert2unicode($subject, 1, - 1);
                $body = convert2unicode($body, 1, - 1);
            }
        }
        // debug($subject);
        // debug($body);die();
        $lwebmail->sendModuleMail($user_id, $subject, $body, 1, '', $email_to_mode, false, true);
    }

    /*
     * show toolbar for export & print function
     */
    function getToolbar()
    {
        global $Lang;
        
        ob_start();
        ?>
<div id='toolbox' class="content_top_tool">
	<div class="Conntent_tool">
		<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?>
		</a> <a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?>
		</a>
	</div>
	<br style="clear: both" />
</div>
<?
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }

    /*
     * replace these symbols by hyphen for code field !"#$%&\'*,/:;<=>?@^`|~
     */
    function replaceSymbolByHyphen($str)
    {
        return preg_replace('/[!"#$%&\\\\\'*,\/:;<=>?@^`|~]/', '-', $str);
    }

    /*
     * check if a string contains following symbols or not !"#$%&\'*,/:;<=>?@^`|~
     * allow following: +-.()[]{}
     */
    function isIncludingSymbol($str)
    {
        if (preg_match('/[!"#$%&\\\\\'*,\/:;<=>?@^`|~]/', $str)) {
            return true;
        } else {
            return false;
        }
    }

    function newline2space($str)
    {
        return str_replace(array(
            "\n",
            "\r",
            "\t"
        ), array(
            " ",
            " ",
            " "
        ), $str);
    }
} // end !defined("LIBLIBRARYMGMT_DEFINED")

?>