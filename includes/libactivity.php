<?php
# using: 

#################################
#
#   Date:   2020-04-22 (Tommy)
#           modified displayActivity(), display student in class record only
#
# 	Date:	2015-12-07 (Bill)	[2015-1008-1356-44164]
#			updated libactivity(), retrieveRecord() for added field Activity Date
#
#	Date:	2011-03-03	YatWoon
#			update displayActivity(), display "Whole Year" is semester is empty
#
#################################

if (!defined("LIBACTIVITY_DEFINED"))         // Preprocessor directives
{

 define("LIBACTIVITY_DEFINED",true);

 class libactivity extends libclass{
       var $StudentActivityID;
       var $UserID;
       var $Year;
       var $Semester;
       var $ActivityName;
       var $Role;
       var $Performance;
       var $Organization;
       var $Remark;
       var $RecordType;		// 1 means Internal, 2 means External
       var $ActivityDate;

       function libactivity($StudentActivityID="")
       {
                $this->libclass();
                if ($StudentActivityID != "")
                {
                    $this->StudentActivityID = $StudentActivityID;
                    $this->retrieveRecord($this->StudentActivityID);
                }
       }
       function retrieveRecord($id)
       {
	       global $special_feature;
	       
                $fields = "UserID, Year, Semester, ActivityName, Role, Performance, Remark, Organization, ActivityDate";
                //$numField = 8;
                $numField = 9;
                if ($special_feature['activity_internal_external'])
                {
	            	$fields .= ", RecordType"; 
	            	$numField++;
                }
                $conds = "StudentActivityID = '".IntegerSafe($id)."'";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_ACTIVITY WHERE $conds";
                $result = $this->returnArray($sql,$numField);
                list(
                $this->UserID, $this->Year,$this->Semester,$this->ActivityName,$this->Role,
                $this->Performance, $this->Remark, $this->Organization, $this->ActivityDate, $this->RecordType) = $result[0];
                return $result;
       }
       function returnYears($studentid=null)
       {
                if (!is_null($studentid)) $sql = "SELECT DISTINCT Year FROM PROFILE_STUDENT_ACTIVITY WHERE UserID = '".IntegerSafe($studentid)."' ORDER BY Year DESC";
                else $sql = "SELECT DISTINCT Year FROM PROFILE_STUDENT_ACTIVITY ORDER BY Year DESC";
                
                return $this->returnVector($sql);
       }
       function returnArchiveYears($studentid)
       {
                $sql = "SELECT DISTINCT Year FROM PROFILE_ARCHIVE_ACTIVITY WHERE UserID = '".IntegerSafe($studentid)."' ORDER BY Year DESC";
                return $this->returnVector($sql);
       }
       function getActivityCountByStudent($studentid)
       {
                $sql = "SELECT COUNT(StudentActivityID) FROM PROFILE_STUDENT_ACTIVITY
                        WHERE UserID=$studentid";
                $entry = $this->returnVector($sql);
                return $entry[0];
       }
       function getActivityListByClass($classname)
       {
                $counts = array();
                $sql = "SELECT a.UserID, COUNT(a.StudentActivityID)
                        FROM PROFILE_STUDENT_ACTIVITY as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        WHERE b.ClassName = '$classname'
                        GROUP BY a.UserID";
                $result = $this->returnArray($sql,2);
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($StudentID,$actCount) = $result[$i];
                     $counts[$StudentID] = $actCount;
                }
                $studentList = $this->getStudentNameListByClassName($classname, "1");
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($StudentID, $StudentName, $classNumber) = $studentList[$i];
                     $returnResult[$i][0] = $StudentID;
                     $returnResult[$i][1] = $StudentName;
                     $returnResult[$i][2] = $classNumber;
                     $returnResult[$i][3] = $counts[$StudentID]+0;
                }
                return $returnResult;
                /*
                $studentList = $this->getClassStudentNameList($classid);
                if (sizeof($studentList)==0) return array();
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($id, $name, $classnumber) = $studentList[$i];
                     $temp = $this->getActivityCountByStudent($id);
                     $result[] = array($id,$name,$classnumber,$temp);
                }
                return $result;
                */
       }
       function getActivityByStudent($studentid, $year,$sem="")
       {
                $conds = ($year != ""? " AND Year = '$year'":"");
                $conds .= ($sem != ""? " AND Semester = '$sem'":"");
                $sql = "SELECT Year, Semester, ActivityName, Role, Performance
                               FROM PROFILE_STUDENT_ACTIVITY WHERE UserID = '".IntegerSafe($studentid)."' $conds
                               ORDER BY Year DESC, Semester, ActivityName";
                return $this->returnArray($sql,5);
       }
       function displayActivity($studentid, $year="", $student_class=array())
       {
                global $image_path, $intranet_session_language;
                global $i_ActivityNoRecord;
                global $i_ActivityNoRecord,$i_Attendance_Year,$i_ActivitySemester,$i_ActivityName,$i_ActivityRole,$i_ActivityPerformance, $Lang;
                $result = $this->getActivityByStudent($studentid, $year);

                $x = "
                  <table width='100%' border='0' cellpadding='4' cellspacing='0'>
                    <tr>
                      <td width='70' align='left' class='tablebluetop tabletopnolink'>$i_Attendance_Year</td>
                      <td width='110' align='left' class='tablebluetop tabletopnolink'>$i_ActivitySemester</td>
                      <td width='214' align='left' class='tablebluetop tabletopnolink'>$i_ActivityName</td>
                      <td width='100' align='left' class='tablebluetop tabletopnolink'>$i_ActivityRole</td>
                      <td width='100' align='left' class='tablebluetop tabletopnolink'>$i_ActivityPerformance</td>
                    </tr>\n";
                
                $class = array();
                for($i = 0; $i < sizeof($student_class); $i++){
                    $class[] = $student_class[$i][0];
                }
               
                $in_class = array();
                for($i = 0; $i < sizeof($result); $i++){
                    if(in_array($result[$i]["Year"], $class))
                        $in_class[] = $result[$i];
                }
                
                for ($i=0; $i<sizeof($in_class); $i++)
                {
                     list($year,$sem,$actName,$role,$performance) = $in_class[$i];
                     
                     $display_sem = $sem ?$sem : $Lang['General']['WholeYear'];
                     $x .= "
                      <tr class='tablebluerow". ( $i%2 + 1 )."'>
                              <td align='left' class='tabletext'>$year</td>
                              <td align='left' class='tabletext'>$display_sem</td>
                              <td align='left' class='tabletext'>$actName</td>
                              <td align='left' class='tabletext'>$role&nbsp;</td>
                              <td align='left' class='tabletext'>$performance&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($result)==0)
                {
                    $x .= "<tr><td colspan=5 align='center' class='tablebluerow2 tabletext'>$i_ActivityNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }

       function displayActivityAdmin($studentid, $current=0, $sem="")
       {
                global $image_path, $intranet_session_language;
                global $i_ActivityNoRecord,$i_ActivityYear,$i_ActivitySemester,$i_ActivityName,$i_ActivityRole,$i_ActivityPerformance;
                $result = $this->getActivityByStudent($studentid, $current,$sem);
                $x = "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_ActivityYear</td>
                      <td width=110 align=center class=tableTitle_new>$i_ActivitySemester</td>
                      <td width=150 align=center class=tableTitle_new>$i_ActivityName</td>
                      <td width=110 align=center class=tableTitle_new>$i_ActivityRole</td>
                      <td width=120 align=center class=tableTitle_new>$i_ActivityPerformance</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list($year,$sem,$actName,$role,$performance) = $result[$i];
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$actName</td>
                              <td align=center>$role&nbsp;</td>
                              <td align=center>$performance&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($result)==0)
                {
                    $x .= "<tr><td colspan=5 align=center>$i_ActivityNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }

        // ++++++ for archive student ++++++ \\

       function displayArchiveActivityAdmin($studentid, $current=0,$sem="")
       {
                global $image_path, $intranet_session_language;
                global $i_ActivityNoRecord,$i_ActivityYear,$i_ActivitySemester,$i_ActivityName,$i_ActivityRole,$i_ActivityPerformance;
                $result = $this->getArchiveActivityByStudent($studentid, $current,$sem);
                $x = "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_ActivityYear</td>
                      <td width=110 align=center class=tableTitle_new>$i_ActivitySemester</td>
                      <td width=150 align=center class=tableTitle_new>$i_ActivityName</td>
                      <td width=110 align=center class=tableTitle_new>$i_ActivityRole</td>
                      <td width=120 align=center class=tableTitle_new>$i_ActivityPerformance</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list($year,$sem,$actName,$role,$performance) = $result[$i];
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$actName</td>
                              <td align=center>$role&nbsp;</td>
                              <td align=center>$performance&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($result)==0)
                {
                    $x .= "<tr><td colspan=5 align=center>$i_ActivityNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }

       function getArchiveActivityByStudent($studentid, $year,$sem="")
       {
                $conds = ($year != ""? " AND Year = '$year'":"");
                $conds .= ($sem != ""? " AND Semester = '$sem'":"");
                $sql = "SELECT Year, Semester, ActivityName, Role, Performance
                               FROM PROFILE_ARCHIVE_ACTIVITY WHERE UserID = '".IntegerSafe($studentid)."' $conds
                               ORDER BY Year DESC, Semester, ActivityName";
                return $this->returnArray($sql,5);
       }

       // +++++ end of archive student +++++ \\

 }


} // End of directives
?>
