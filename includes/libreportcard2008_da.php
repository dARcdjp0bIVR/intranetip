<?php
#  Editing by 
/* ***************************************************
 * Modification Log
 * 	2016-08-05	Bill	[2016-0330-1524-42164]
 * 	- modified Generate_Batch_File(), Generate_Batch_File_Checkboxes_Array(), to support archive pdf report
 */
if (!defined("LIBREPORTCARD_DA"))         // Preprocessor directives
{
	define("LIBREPORTCARD_DA", true);
	
	class libreportcard_da extends libreportcard {
		
		function libreportcard_da() {
			$this->libreportcard();
		}
		
		function Get_To_Digital_Archive_Temp_File_Path() {
			global $intranet_root;
			
			return $intranet_root."/file/reportcard2008/tmp_archive";
		}
		
		function Get_To_Digital_Archive_User_Temp_File_Path() {
			return $this->Get_To_Digital_Archive_Temp_File_Path().'/u'.$_SESSION['UserID'];
		}
		
		function Check_User_Can_Apply_Digital_Archive($targetUserId) {
			global $PATH_WRT_ROOT, $plugin, $intranet_root, $intranet_session_language;	// cannot remove $intranet_root, $intranet_session_language since required in libdigitalarchive_moduleupload.php
			
			include_once($PATH_WRT_ROOT.'includes/libdigitalarchive_moduleupload.php');
			$ldamu = new libdigitalarchive_moduleupload();
			
			return ($plugin['digital_archive'] && $ldamu->User_Can_Archive_File());
		}
		
		// $ActionType = "temp" or "archive"
		function Generate_Batch_File($ReportId, $ClassIdAry, $FileNameFormat, $ActionType) {
			global $PATH_WRT_ROOT;
			global $intranet_root, $lreportcard, $eRCTemplateSetting;
			
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			include_once($PATH_WRT_ROOT.'includes/libdigitalarchive_moduleupload.php');
			$lfs = new libfilesystem();
			$ldamu = new libdigitalarchive_moduleupload();
			
			$successAry = array();
			
			$archive_path = $this->Get_To_Digital_Archive_Temp_File_Path();
			$dir_path = $this->Get_To_Digital_Archive_User_Temp_File_Path();
			
			if(file_exists($dir_path)){
				$successAry['removeOldTempFiles'] = $lfs->folder_remove_recursive($dir_path);
			}
			
			if(!is_dir($archive_path)){
				$successAry['createTempFileFolder'] = $lfs->folder_new($archive_path);
				$successAry['changeModForTempFileFolder'] = $lfs->chmod_R($archive_path,0777);
			}
			
			$successAry['createUserTempFileFolder'] = $lfs->folder_new($dir_path);
			$successAry['changeModForUserTempFileFolder'] = $lfs->chmod_R($dir_path, 0777);
			
			$studentInfoAry = $this->GET_STUDENT_BY_CLASS($ClassIdAry);
			$numOfStudent = count($studentInfoAry);
			
			$hiddenInputAry = array();
			for ($i=0; $i<$numOfStudent; $i++) {
				$_studentId = $studentInfoAry[$i]['UserID'];
				
				$_filename = $this->generateStudentReportFileName($FileNameFormat, $studentInfoAry[$i]);
				$_targetFilePath = $this->Get_To_Digital_Archive_User_Temp_File_Path().'/'.$_filename.'.html';
				
				// [2016-0330-1524-42164]
				// Set file path for PDF Report
				if($eRCTemplateSetting['Report']['ExportPDFReport']){
					$source_path = $intranet_root."/file/reportcard2008/".$lreportcard->schoolYear."/archivePDFReport/".$ReportId."/".$_studentId.".pdf";
					$_targetFilePath = $this->Get_To_Digital_Archive_User_Temp_File_Path().'/'.$_filename.'.pdf';
				}
				
				$_fileContent = '';
				if ($ActionType == 'temp') {
					$_fileContent = 'temp file';
				}
				else if ($ActionType == 'archive') {
					if($eRCTemplateSetting['Report']['ExportPDFReport']){
						// do nothing
					}
					else{
						$_reportUrl = '';
						$_reportUrl .= curPageURL($withQueryString=0, $withPageSuffix=0);
						$_reportUrl .= $this->studentArchiveReportPath.'/print_preview.php?TargetStudentID[]='.$_studentId.'&ReportID='.$ReportId.'&forArchiveToDa=1';
						$_fileContent = getHtmlByUrl($_reportUrl);
						$_fileContent = str_replace($PATH_WRT_ROOT,'',$_fileContent);
						$_fileContent = $ldamu->Embed_Image_CSS_To_HTML($_fileContent);
					}	
				}
				
				// [2016-0330-1524-42164]
				// File handling for PDF report (only for students with archived pdf report)
				if($eRCTemplateSetting['Report']['ExportPDFReport']){
					if($ActionType == 'temp' && file_exists($source_path))
						$successAry['createStudentTmpFile'][$_studentId] = $lfs->file_write($_fileContent, $_targetFilePath);
					else if($ActionType == 'archive' && file_exists($source_path))
						$successAry['createStudentTmpFile'][$_studentId] = $lfs->file_copy($source_path, $_targetFilePath);
				}
				else
					$successAry['createStudentTmpFile'][$_studentId] = $lfs->file_write($_fileContent, $_targetFilePath);
			}
			
			return in_multi_array(false, $successAry)? false : true;
		}
		
		function Generate_Batch_File_Checkboxes_Array($ClassIdAry, $FileNameFormat) {
			global $eRCTemplateSetting;
			
			$studentInfoAry = $this->GET_STUDENT_BY_CLASS($ClassIdAry);
			$numOfStudent = count($studentInfoAry);
			
			$dir_path = $this->Get_To_Digital_Archive_User_Temp_File_Path();
			
			$hiddenInputAry = array();
			for ($i=0; $i<$numOfStudent; $i++) {
				$_filename = $this->generateStudentReportFileName($FileNameFormat, $studentInfoAry[$i]);
				$_targetFilePath = $dir_path.'/'.$_filename.'.html';
				
				// File path handling for PDF report (only for students with archived pdf report)
				if($eRCTemplateSetting['Report']['ExportPDFReport']){
					$_targetFilePath = $dir_path.'/'.$_filename.'.pdf';
					if(!file_exists($_targetFilePath))
						continue;
				}
				
				$_targetFilePath_e = getEncryptedText($_targetFilePath);
				
				$hiddenInputAry[] = '<input id="Files'.$i.'" name="Files[]" type="checkbox" checked="checked" value="'.$_targetFilePath_e.'" style="display:none;">';
			}
			
			return $hiddenInputAry;
		}
		
		function generateStudentReportFileName($FileFormat, $StudentInfoAry) {
			$className = Get_Lang_Selection($StudentInfoAry['ClassTitleCh'], $StudentInfoAry['ClassTitleEn']);
			$classNumber = $StudentInfoAry['ClassNumber'];
			$studentNameEn = $StudentInfoAry['StudentNameEn'];
			$studentNameCh = $StudentInfoAry['StudentNameCh'];
			
			$classNumber = str_pad($classNumber, 2, "0", STR_PAD_LEFT);
			
			$filename = $FileFormat;
			$filename = str_replace('{classname}', $className, $filename);
			$filename = str_replace('{classnumber}', $classNumber, $filename);
			$filename = str_replace('{studentnameenglish}', $studentNameEn, $filename);
			$filename = str_replace('{studentnamechinese}', $studentNameCh, $filename);
			$filename = parseFileName($filename);
			
			return $filename;
		}
		
	} // end of class libreportcard_schedule
} // end of Preprocessor directives
?>