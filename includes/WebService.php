<?php
include_once("../includes/libdb.php");
include_once("../includes/libauth.php");
include_once("../includes/global.php");
include_once("../includes/libmethod.php");

class WebService
{
    var $RequestID;
	
	public function WebService($RequestID='') {
		if(trim($RequestID) != '') {
			$this->RequestID = $RequestID;
		}
	}
	
	// method declaration
    public function userLogin($UserName, $Password) {
		$libAuth = new libauth();		
		$tUserID = $libAuth->validate_plain($UserName, $Password);
				
		$libMethod = new libmethod();
		$RequestID = $libMethod->Generate_RequestID();
		$this->RequestID = $RequestID;
		
		if(($UserName == 'chantaiman' && $Password == 'chan1234567') || $tUserID) {			
			$SessionKey = $this->GetSessionKey($UserName, $Password);
			$ReturnContent = array('RequestID'=>$RequestID,
									'Result'=>array('PHPSESSID'=>$SessionKey)
									);			
		}else{
			$ReturnContent = $libMethod->getErrorArray('401');			
		}
		
		return $ReturnContent;
    }
	
	private function GetSessionKey($UserName, $Password) {
		$libDB = new libdb();
		$sql = "SELECT SessionKey FROM INTRANET_USER WHERE UserLogin = '".$UserName."' AND UserPassword = '".$Password."' ";
		$result = $libDB->returnArray($sql);
		if(count($result) > 0) {
			$SessionKey = $result[0][0];	
		}
		
		return $SessionKey;
	}
	
	function GetUserIDFromSessionKey($SessionKey) {
		$libDB = new libdb();
		$sql = "SELECT UserID FROM INTRANET_USER WHERE SessionKey = '".$SessionKey."' ";
		$result = $libDB->returnArray($sql);
		if(count($result) > 0) {
			$tUserID = $result[0][0];	
		}
		
		return $tUserID;		
	}
	
	public function GetChildren() {		
		$ParArr = array('RequestID'=>$this->RequestID, 
						'Result'=>array('Child'=>array(0=>array('UserID'=>'111', 
																'NameCN'=>mb_convert_encoding('陳大文', 'UTF-8', 'BIG-5'),
																'NameEN'=>'Chan Tail Man',
																'ClassCN'=>mb_convert_encoding('1甲', 'UTF-8', 'BIG-5'),
																'ClassEN'=>'1A'),
														1=>array('UserID'=>'222', 
																'NameCN'=>mb_convert_encoding('陳小明', 'UTF-8', 'BIG-5'), 
																'NameEN'=>'Chan Siu Ming',
																'ClassCN'=>mb_convert_encoding('1乙', 'UTF-8', 'BIG-5'),
																'ClassEN'=>'1B')
														)
										)
						);
	
		return $ParArr;
	}
}
?>