<?php
# modified by: 

/********************** Change Log ***********************/
#
# 	Date	:	2018-09-28 [Bill]   [2018-0809-0935-01276]
#	Details	:	modified getWordsInJS_forPaymentNotice(), to add js wording answersheet_mustPay
#
#	Date	:	2017-11-09 [Bill]	[EJ-DM#922]
#	Details :	modified getWordsInJS(), getWordsInJS_forPaymentNotice(), getWordsInJS4eDiscipline(), to add $i_Form_pls_specify_option_num
#
#	Date	:	2015-03-23 [Jason]
#	Details	:	modified getWordsInJS() to add answersheet_order
#
# 	Date	:	2011-01-14 [Henry Chow]
#	Details	:	modified getWordsInJS(), getWordsInJS_forPaymentNotice(), add "Save Draft" button
#
# 	Date	:	2010-12-25 [Henry]
#	Details	:	modified getWordsInJS_forPaymentNotice(), add wording to javascript
#
# 	Date	:	2009-12-16 [Yuen]
#	Details	:	changed wording from "Topic / Title" to "Format" for True&False, MC ... etc
#
#
/******************* End Of Change Log *******************/
if (!defined("LIBFORM_DEFINED"))         // Preprocessor directives
{

 define("LIBFORM_DEFINED",true);

 class libform extends libdb{
       var $FormID;
       var $FormName;
       var $description;
       var $recordType;
       var $recordStatus;
       var $formType;
       var $queString;
       var $question_array;

       function libform($fid="")
       {
                $this->libdb();
                if ($fid != "")
                {
                    $this->retrieveRecord($fid);
                }
       }
       function retrieveRecord($fid)
       {
                $sql = "SELECT FormName,Description,RecordType,RecordStatus,FormType,QueString
                        FROM INTRANET_FORM WHERE FormID = '$fid'";
                $result = $this->returnArray($sql,6);
                $this->FormID = $fid;
                list ($this->FormName, $this->description, $this->recordType,
                      $this->recordStatus, $this->formType,$this->queString) = $result[0];

       }
       function getConvertedString($string)
       {
                $string = str_replace('"','&quot;',addslashes($string));
                $string = preg_replace('(\r\n|\n)', "<br>", $string);
                return $string;
       }
       function getConvertedQuestion ()
       {
                return $this->getConvertedString($this->queString);
       }

       function returnTemplates()
       {
                $sql = "SELECT FormName, QueString FROM INTRANET_FORM WHERE (FormType = 0 AND RecordStatus = 1) OR (FormType=1 AND RecordStatus=0)";
                return $this->returnArray($sql,2);
       }
       function getTemplatesInJS()
       {
                $result = $this->returnTemplates();
                $x = "var form_templates = new Array();\n";
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($name,$que) = $result[$i];
                     $x .= "form_templates[$i] = new Array(\"$name\",\"$que\");";
                }
                return $x;
       }
       function getWordsInJS()
       {
                global $i_Form_answer_sheet,$i_Form_answersheet_template,$i_Form_answersheet_header,
                       $i_Form_answersheet_no,$i_Form_answersheet_type,$i_Form_answersheet_maxno,
                       $i_Form_no_options_for,$i_Form_pls_specify_type,$i_Form_pls_fill_in,$i_Form_pls_specify_option_num,
                       $i_Form_chg_title,$i_Form_chg_template,$i_Form_answersheet_tf,
                       $i_Form_answersheet_mc,$i_Form_answersheet_mo,$i_Form_answersheet_sq1,
                       $i_Form_answersheet_sq2,$i_Form_answersheet_option,$i_Form_answersheet_not_applicable,
                       $i_Form_pls_fill_in_title, $i_general_Format, $Lang;
                global $button_submit,$button_quit,$button_add,$button_cancel,$button_update, $button_save_draft;

                $x = "";
                $x .= "
					answer_sheet=\"$i_Form_answer_sheet\";
					answersheet_template=\"$i_Form_answersheet_template\";
					answersheet_header=\"$i_Form_answersheet_header\";
					answersheet_no=\"$i_Form_answersheet_no\";
					answersheet_type=\"".$i_general_Format."\";
					answersheet_maxno=\"$i_Form_answersheet_maxno\";
					no_options_for = \"$i_Form_no_options_for\";
					pls_specify_type = \"$i_Form_pls_specify_type\";
					pls_specify_option_num = \"$i_Form_pls_specify_option_num\";
					pls_fill_in = \"$i_Form_pls_fill_in\";
					pls_fill_in_title = \"$i_Form_pls_fill_in_title\";
					chg_title = \"$i_Form_chg_title\";
					chg_template = \"$i_Form_chg_template\";
					
					answersheet_tf=\"$i_Form_answersheet_tf\";
					answersheet_mc=\"$i_Form_answersheet_mc\";
					answersheet_mo=\"$i_Form_answersheet_mo\";
					answersheet_sq1=\"$i_Form_answersheet_sq1\";
					answersheet_sq2=\"$i_Form_answersheet_sq2\";
					answersheet_order=\"".$Lang['eSurvey']['OrderingType']."\";
					answersheet_option=\"$i_Form_answersheet_option\";
					answersheet_not_applicable=\"$i_Form_answersheet_not_applicable\";
					
					button_submit=\"$button_submit\";
					button_quit=\"$button_quit\";
					button_add=\"$button_add\";
					button_cancel=\"$button_cancel\";
					button_update=\"$button_update\";
					button_save_draft = \"$button_save_draft\";
				";
              return $x;
       }
       
       function getWordsInJS_forPaymentNotice()
       {
                global $i_Form_answer_sheet,$i_Form_answersheet_template,$i_Form_answersheet_header,
                       $i_Form_answersheet_no,$i_Form_answersheet_type,$i_Form_answersheet_maxno,
                       $i_Form_no_options_for,$i_Form_pls_specify_type,$i_Form_pls_fill_in,$i_Form_pls_specify_option_num,
                       $i_Form_chg_title,$i_Form_chg_template,$i_Form_answersheet_tf,
                       $i_Form_answersheet_mc,$i_Form_answersheet_mo,$i_Form_answersheet_sq1,
                       $i_Form_answersheet_sq2,$i_Form_answersheet_option,$i_Form_answersheet_not_applicable,
                       $i_Form_pls_fill_in_title, $i_general_Format;
                global $button_submit,$button_quit,$button_add,$button_cancel,$button_update, $button_save_draft, $Lang;

                $x = "";
                $x .= "
					answer_sheet=\"$i_Form_answer_sheet\";
					answersheet_template=\"$i_Form_answersheet_template\";
					answersheet_header=\"$i_Form_answersheet_header\";
					answersheet_no=\"$i_Form_answersheet_no\";
					answersheet_type=\"$i_general_Format\";
					answersheet_maxno=\"$i_Form_answersheet_maxno\";
					no_options_for = \"$i_Form_no_options_for\";
					pls_specify_type = \"$i_Form_pls_specify_type\";
					pls_specify_option_num = \"$i_Form_pls_specify_option_num\";
					pls_fill_in = \"$i_Form_pls_fill_in\";
					pls_fill_in_title = \"$i_Form_pls_fill_in_title\";
					chg_title = \"$i_Form_chg_title\";
					chg_template = \"$i_Form_chg_template\";
					
					answersheet_tf=\"$i_Form_answersheet_tf\";
					answersheet_mc=\"$i_Form_answersheet_mc\";
					answersheet_mc2=\"$i_Form_answersheet_mc\"+' 2';
					answersheet_mo=\"$i_Form_answersheet_mo\";
					answersheet_NoOfOption=\"{$Lang['ePayment']['NoOfOptions']}\";
					answersheet_option=\"{$Lang['ePayment']['Options']}\";
					
					
					answersheet_toPay = \"{$Lang['ePayment']['WhetherToPay']}\";
					answersheet_onePayOneCat = \"{$Lang['ePayment']['OnePaymentFromOneCategory']}\";
					answersheet_onePayFewCat = \"{$Lang['ePayment']['OnePaymentFromFewCategories']}\";
					answersheet_fewPayFewCat = \"{$Lang['ePayment']['SeveralPaymentFromFewCategories']}\";
					answersheet_mustPay = \"{$Lang['ePayment']['MustPayItem']}\";
					
					answersheet_sq1=\"$i_Form_answersheet_sq1\";
					answersheet_sq2=\"$i_Form_answersheet_sq2\";
					answersheet_not_applicable=\"$i_Form_answersheet_not_applicable\";
					
					button_submit=\"$button_submit\";
					button_quit=\"$button_quit\";
					button_add=\"$button_add\";
					button_cancel=\"$button_cancel\";
					button_update=\"$button_update\";
					button_save_draft = \"$button_save_draft\";
				";
              return $x;
       }
              
       function getWordsInJS4eDiscipline()
       {
                global $i_Form_answer_sheet,$i_Form_answersheet_template,$i_Form_answersheet_header,
                       $i_Form_answersheet_no,$i_Form_answersheet_type,$i_Form_answersheet_maxno,
                       $i_Form_no_options_for,$i_Form_pls_specify_type,$i_Form_pls_fill_in,$i_Form_pls_specify_option_num,
                       $i_Form_chg_title,$i_Form_chg_template,$i_Form_answersheet_tf,
                       $i_Form_answersheet_mc,$i_Form_answersheet_mo,$i_Form_answersheet_sq1,
                       $i_Form_answersheet_sq2,$i_Form_answersheet_option,$i_Form_answersheet_not_applicable;
                global $button_submit,$button_quit,$button_add,$button_cancel,$button_update;
                global $iDiscipline;
                $x = "";
                $x .= "
					answer_sheet=\"$i_Form_answer_sheet\";
					answersheet_template=\"".$iDiscipline['Form_Templates']."\";
					answersheet_header=\"$i_Form_answersheet_header\";
					answersheet_no=\"$i_Form_answersheet_no\";
					answersheet_type=\"".$i_Form_answersheet_header."\";
					answersheet_type_selection=\"".$iDiscipline['Form_answersheet_type_selection']."\";
					answersheet_maxno=\"$i_Form_answersheet_maxno\";
					no_options_for = \"$i_Form_no_options_for\";
					pls_specify_type = \"$i_Form_pls_specify_type\";
					pls_specify_option_num = \"$i_Form_pls_specify_option_num\";
					pls_fill_in = \"$i_Form_pls_fill_in\";
					chg_title = \"$i_Form_chg_title\";
					chg_template = \"$i_Form_chg_template\";
					
					answersheet_tf=\"$i_Form_answersheet_tf\";
					answersheet_mc=\"$i_Form_answersheet_mc\";
					answersheet_mo=\"$i_Form_answersheet_mo\";
					answersheet_sq1=\"$i_Form_answersheet_sq1\";
					answersheet_sq2=\"$i_Form_answersheet_sq2\";
					answersheet_option=\"".$iDiscipline['Form_answersheet_option']."\";
					answersheet_selection=\"".$iDiscipline['Form_answersheet_selection']."\";
					answersheet_not_applicable=\"$i_Form_answersheet_not_applicable\";
					
					button_submit=\"$button_submit\";
					button_quit=\"$button_quit\";
					button_add=\"$button_add\";
					button_cancel=\"$button_cancel\";
					button_update=\"$button_update\";
				";
              return $x;
       }
       function getTypeSelect($tag, $selected=0)
       {
                global $i_Form_Assessment,$i_Form_Activity;
                $typeArray = array ($i_Form_Assessment);

                $x = "<SELECT $tag>\n";
                for ($i=0; $i<sizeof($typeArray); $i++)
                {
                     $selStr = ($selected == $i? "SELECTED":"");
                     $x .= "<OPTION value=$i $selStr>".$typeArray[$i]."</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }
       function getFormTypeSelect($name, $selected=1)
       {
                global $i_Form_Templates,$i_Form_Form;
                if ($selected == 0)
                {
                    $isTemplate = "CHECKED";
                    $isForm = "";
                }
                else
                {
                    $isTemplate = "";
                    $isForm = "CHECKED";
                }
                $formtypeSelect = "<input type=radio name=$name value=0 $isTemplate>$i_Form_Templates <input type=radio name=$name value=1 $isForm>$i_Form_Form";
                return $formtypeSelect;
       }
       function getStatusSelect($name, $selected=1)
       {
                global $i_Form_Suspended,$i_Form_Approved;
                if ($selected == 0)
                {
                    $isSus = "CHECKED";
                    $isApp = "";
                }
                else
                {
                    $isSus = "";
                    $isApp = "CHECKED";
                }
                $x = "<input type=radio name=$name value=0 $isSus>$i_Form_Suspended <input type=radio name=$name value=1 $isApp>$i_Form_Approved";
                return $x;
       }
       function returnForm($type="")
       {
                if ($type === "")
                {
                    $conds = "";
                }
                else
                {
                    $conds = "AND RecordType = $type";
                }
                $sql = "SELECT FormID, FormName, Description FROM INTRANET_FORM WHERE RecordStatus = 1 AND FormType = 1 $conds";
                return $this->returnArray($sql,3);
       }
       function returnAssessmentForm()
       {
                return $this->returnForm(0);
       }
       function isFormEditable()
       {
                switch ($this->recordType)
                {
                    case 0:
                         return !$this->isFormFilled();
                         break;
                    case 1:
                         return true;
                         break;
                    default:
                         return true;
                }
       }
       function isFormFilled()
       {
                if ($this->recordType == 0)
                {
                    $sql = "SELECT COUNT(AssessmentID) FROM PROFILE_STUDENT_ASSESSMENT WHERE FormID = '".$this->FormID."'";
                    $result = $this->returnVector($sql);
                    return ($result[0]!=0);
                }
                else
                {
                    return false;
                }
       }
       function returnAnswerInJS($result)
       {
                for ($i=0; $i<sizeof($result); $i++)
                {
                     #list ($answer) = $result[$i];
                     $answer = $result[$i];
                     $answer = $this->getConvertedString($answer);
                     $x .= "myAns[myAns.length] = \"$answer\";\n";
                }
                return $x;
       }
        function splitQuestion($qStr)
        {
                 $qSeparator = "#QUE#";
                 $pSeparator = "||";
                 $oSeparator = "#OPT#";
                 $questions = explode($qSeparator,$qStr);
                 for ($i=1; $i<sizeof($questions); $i++)
                 {
                      $que = $questions[$i];
                      $parts = explode($pSeparator,$que);
                      $temp = explode(",",$parts[0]);
                      $type = $temp[0];
                      $numOp = 0;
                      $options = "";
                      switch ($type)
                      {
                              case 1:
                                   $numOp = 2;
                                   $opStr = $parts[2];
                                   $options = explode($oSeparator,$opStr);
                                   array_shift($options);
                                   break;
                              case 2:
                              case 3:
                                   $numOp = $temp[1];
                                   $opStr = $parts[2];
                                   $options = explode($oSeparator,$opStr);
                                   array_shift($options);
                                   break;
                              case 4:
                              case 5:
                              case 6:
                                   $options = array();
                      }
                      $mainQ = $parts[1];
                      $result[] = array($type, $mainQ, $options);
                 }
                 $this->question_array = $result;
                 
                 return $result;
        }
        function parseAnswerStr ($aStr)
        {
                 if ($aStr == "") return array();
                 $aSeparator = "#ANS#";
                 $answers = explode($aSeparator,$aStr);
                 for ($i=1; $i<sizeof($answers); $i++)
                 {
                      $qType = $this->question_array[$i-1][0];
                      
                      if ($qType == 2 || $qType == 1)
                      {
                          $options = $this->question_array[$i-1][2];
                          $ansPart = $options[$answers[$i]];
                      }
                      else if ($qType == 3)
                      {
                          $options = $this->question_array[$i-1][2];
                          $selected = explode(",",$answers[$i]);
                          $ansPart = "";
                          for ($j=0; $j<sizeof($selected); $j++)
                          {
                               $ansPart .= $options[$selected[$j]];
                          }
                      }
                      else if ($qType == 6)
                      {
                           continue;
                      }
                      else
                      {
                          $ansPart = $answers[$i];
                      }
                      $result[] = $ansPart;
                 }
                 return $result;
        }
 }

} // End of directives
?>