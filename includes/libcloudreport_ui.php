<?php
// Using :

########################### Change Log ###########################
/*   
 *   2018-05-18 Isaac: hided detail of usage items/ item recommendation which disable onclick trigger of legend items from displaySystemUsageList()
 */
##################################################################

class libcloudreport_ui extends libcloudreport
{
    var $capacityUnit;
    function libcloudreport_ui(){
        $this->capacityUnit='GB';
    }
    
    function include_CSS(){
        global $PATH_WRT_ROOT;
        $x ='<link rel="stylesheet" type="text/css" href="'.$PATH_WRT_ROOT.'templates/cloud/report/css/bootstrap.min.css">';
        $x.='<link rel="stylesheet" type="text/css" href="'.$PATH_WRT_ROOT.'templates/cloud/report/css/fontawesome-all.min.css">';
        $x.='<link rel="stylesheet" type="text/css" href="'.$PATH_WRT_ROOT.'templates/cloud/report/css/style.css">';
        return $x;
    }
    
    function include_JS(){
        global $PATH_WRT_ROOT;
        $x ='<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/popper.min.js"></script>';
        $x.='<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-3.3.1.min.js"></script>';
        $x.='<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/bootstrapV4.0.0.min.js"></script>';
        return $x;
    }
    
    function include_favicon(){
        global $image_path;
        $x.='<link rel="apple-touch-icon" sizes="180x180" href="'.$image_path.'favicon/apple-touch-icon.png">';
        $x.='<link rel="icon" type="image/png" sizes="32x32" href="'.$image_path.'favicon/favicon-32x32.png">';
        $x.='<link rel="icon" type="image/png" sizes="16x16" href="'.$image_path.'favicon/favicon-16x16.png">';
        return $x;
    }
    
    function include_heeader(){
        $x ='<meta charset="utf-8">';
        $x.='<meta http-equiv="X-UA-Compatible" content="IE=edge">';
        $x.='<meta name="viewport" content="width=device-width, initial-scale=1">';
        $x.='<title>eClass on the Cloud</title>';
        $x.= $this->include_CSS();
        $x.= $this->include_favicon();
        $x.='<link rel="manifest" href="favicon/site.webmanifest">';
        $x.='<link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5">';
        $x.='<meta name="msapplication-TileColor" content="#da532c">';
        $x.='<meta name="theme-color" content="#ffffff">';
        return $x;        
    }
    
    function diaplayUsageBar($systemUsedAry, $planCapacity){
        global $Lang;
        $itemNo = 1;
        $valuemax = 100;
        $x ='';
        foreach((array)$systemUsedAry as $item => $itemUsage ){
                $usagePerHundred = ($itemUsage/$planCapacity)*100;
                $bgClass = 'bg-'.$itemNo;
                $x.='<div class="progress-bar '.$bgClass.'" role="progressbar" style="width:'.$usagePerHundred.'%" aria-valuenow="$usagePerHundred" aria-valuemin="0" aria-valuemax="'.$valuemax.'" data-toggle="tooltip" title="'.$Lang['Cloud']['Report']['LegendAry'][$item].': '.$itemUsage.' '.$this->capacityUnit.'"></div>';'<div class="progress-bar '.$bgClass.'" role="progressbar" style="width:'.$usagePerHundred.'%" aria-valuenow="$usagePerHundred" aria-valuemin="0" aria-valuemax="'.$valuemax.'" data-toggle="tooltip" title="'.$item.': '.$itemUsage.' '.$this->capacityUnit.'"></div>';
                $itemNo++;
        }
        echo $x;
    }
    
    function displayCurrentPackage($planCapacity, $endDate){
        global $Lang, $intranet_session_language;
        if($intranet_session_language == 'b5'|| $intranet_session_language == 'gb'){
            $endDate = date('d m Y', strtotime($endDate));
            list($day, $month, $year) = explode(' ', $endDate);
            $endDate = $year.$Lang['Cloud']['Report']['ChineseYear'].$month.$Lang['Cloud']['Report']['ChineseMonth'].$day.$Lang['Cloud']['Report']['ChineseDay'];
        }
        $x.='<p><span class="plan-name">'.$planCapacity.' '.$this->capacityUnit.' '.$Lang['Cloud']['Report']['Plan'].'</span>  ('.$Lang['Cloud']['Report']['ValidTill'].' '.$endDate.')</p>';
        echo $x;
    }
    
    function displaystorageInfo($planCapacity, $totalStorageUsage){
        global $Lang;
        $x ='<span class="info">'.str_replace('<!--capacityUnit-->', $this->capacityUnit,str_replace('<!--usedStorage-->', $totalStorageUsage, str_replace('<!--availableStorage-->', $planCapacity, $Lang['Cloud']['Report']['Usage']))).'</span>';
        echo $x;
    }
    
    function displaySystemUsageList($systemUsedAry){
        global $Lang;
        $itemNo =1;
        foreach((array)$systemUsedAry as $item => $itemUsage ){
                $infoId = 'info'.$itemNo;
                $itemBoxClass = 'box'.$itemNo;
                if($itemUsage){
                    $x.='<a class="nav-link" aria-selected="false">';
//                     $x.='<a class="nav-link" data-toggle="pill" href="#'.$infoId.'" role="tab" aria-controls="'.$infoId.'" aria-selected="false">'; #enable  "click and show" detail of usage items
                    $x.='<span class="legend '.$itemBoxClass.'"><i class="fas fa-square"></i></span>';
                    $x.='<span>'.$Lang['Cloud']['Report']['LegendAry'][$item].'</span>';
                    $x.='<span class="space">'.($itemUsage?$itemUsage:0).' '.$this->capacityUnit.'</span>';
                    $x.='</a>';
                }
                $itemNo++;
        }
        echo $x;
    }
    
    function displayRecommendation($RecommendationItemAry){
        global $Lang;
       
        $recommandationIndex =0;
        foreach((array)$RecommendationItemAry as $RecommendationItem){
        switch($RecommendationItem){
            case "recommendation":
                $activeClassName = 'active show';
                $RecommendationContent='<h4>'.$Lang['Cloud']['Report']['RecommandationHeadingAry']['Recommendations'].'</h4>'.$Lang['Cloud']['Report']['RecommandationAry']['Recommendations'] ;
                break;
            case "intranet":
                $activeClassName='';
                $RecommendationContent='<h4>'.$Lang['Cloud']['Report']['RecommandationHeadingAry']['Intranet'] .'</h4>'.$Lang['Cloud']['Report']['RecommandationAry']['Intranet'];
                break;
            case "classroom":
                $activeClassName='';
                $RecommendationContent='<h4>'.$Lang['Cloud']['Report']['RecommandationHeadingAry']['Classroom'].'</h4>'.$Lang['Cloud']['Report']['RecommandationAry']['Classroom'];
                break;
            case "imail":
                $activeClassName='';
                $RecommendationContent='<h4>'.$Lang['Cloud']['Report']['RecommandationHeadingAry']['Imail'] .'</h4>'.$Lang['Cloud']['Report']['RecommandationAry']['Imail'];
                break;
            case "ifolder":
                $RecommendationContent='<h4>'.$Lang['Cloud']['Report']['RecommandationHeadingAry']['Ifolder'].'</h4>'.$Lang['Cloud']['Report']['RecommandationAry']['Ifolder'];
                break;
            case "mysql":
                $activeClassName='';
                $RecommendationContent=' <h4>'.$Lang['Cloud']['Report']['RecommandationHeadingAry']['Mysql'].'</h4>'.$Lang['Cloud']['Report']['RecommandationAry']['Mysql'];
                break;
            case "other":
                $activeClassName='';
                $RecommendationContent='<h4>'.$Lang['Cloud']['Report']['RecommandationHeadingAry']['Others'].'</h4>'.$Lang['Cloud']['Report']['RecommandationAry']['Others'] ;
                break;
        }
        $recommandationId = 'info'.$recommandationIndex;
        $x.='<div class="tab-pane fade '.$activeClassName.'" id="'.$recommandationId.'" role="tabpanel" aria-labelledby="'.$recommandationId.'">';
        $x.=$RecommendationContent;
        $x.='</div>';
        $recommandationIndex++;
        }
        echo $x;
    }
    
    function getAlert($alertTypeClassName, $messageLine){ 
        $alertcContent ='<i class="fas fa-exclamation-triangle"></i> '.$messageLine;
        $x ='<div class="alert '.$alertTypeClassName.' alert-dismissible fade show" role="alert">';
        $x.='<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
        $x.='<span aria-hidden="true">&times;</span>';        
        $x.='</button>';
        $x.=$alertcContent;
        $x.='</div>'; 
        return $x;
    }

}
?>