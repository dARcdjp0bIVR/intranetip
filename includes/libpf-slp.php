<?php
// [Modification Log] Using By : anna
/** 
 *********************************************************
 *  2019-01-24 Anna [#K152989] 
 *  - Modified countActivityDataNumber, countAssessmentDataNumber,countAttendanceDataNumber, countAwardDataNumber,
 *    countMeritDataNumber(), countServiceDataNumber() - hide archive student records
 *  
 *  2018-11-07 Anna
 *  - Modified GET_OLE_RECORD_LIST() - change check ole startdate to check AcaademicYearID
 *  
 *  2018-10-09 Anna
 *  - Modified GET_OLE_YEAR_RECORD_LIST - added  $sys_custom['iPf']['OLE_Report']['StartDateMappingByYearName']
 *  
 *  2018-03-06 Omas[ip.2.5.8.3.1]
 *  -modified GEN_ASSIGN_OLE_STUDENT_TABLE() - edit hours maxlength from 4 to 6
 *  
 *  2018-03-01 Pun [ip.2.5.8.3.1]
 *  - Modified getClassInfoByGroupIds(), fixed cannot display class name if the class does not have Chinese name
 *  
 *	2018-02-02 Anna
 *	- added getTwghczmSubmissionPeriodFrom()
 *
 *	2017-07-18 Simon
 *	- modified the sql in updateMeritFromIP 
 *	2017-07-14 Villa #F118453 
 *	- modified  returnOLRExportHeader() - add Program Creator
 *	- modified	returnStudentOLRExportData() - add Program Creator
 *	- modified	returnOLRExportData() - add Program Creator
 *
 *	2017-01-13 Villa 
 *	- modified returnStudentAttendanceSummary() - sync with the same function of libpf-sturce
 *	- modified displayStudentAttendanceSummary() - sync with the same function of libpf-sturce
 *
 *	2016-12-02 Villa [ip.2.5.8.1.1 ]
 * 	- modified returnPortfolioAssessmentYearSemData() - change return semester array
 *  - modified countAssessmentDataNumber() - handle with the semester change from main semester to the semester including assessment  
 *  - modified returnStudentAssessmentExportData() -  handle with the semester change from main semester to the semester including assessment  
 * 
 *	2016-07-14 Kenneth [ip.2.5.7.7.1]
 *	- modified GET_OLR_APPROVAL_SETTING_DATA2(), fix php5.4 issues
 *
 *	2016-07-05 Omas [ip.2.5.7.7.1]
 *  - modified get_OLR_SubCategory_Field_For_Record() - fix  #M98350
 * 
 *  2016-05-23 Henry HM [2016-0415-1449-22214]
 *  - Added field of "Default Hours"
 *
 *	2016-02-15 Omas [ip.2.5.7.3.1]
 *	- modified GEN_ASSIGN_OLE_STUDENT_TABLE() - fix Firefox cannot use AllStatus drop-down list -#P88084 
 *
 *	2015-11-04 Omas [ip.2.5.7.1.1]
 *	- added GET_STUDENT_COMMENT()
 *
 *	2015-05-19 Omas [#S78495]
 *	Add Import for Self Account
 *	- added getImportSelfAccountColumnProperty(),getImportSelfAccountHeader(),deleteSelfAccountImportTempData(),getSelfAccountImportTempData(),insertSelfAccountImportTempData()
 *
 *	2015-05-11 Omas [#B78258]
 *	- modified returnOLRExportHeader(),returnOLRExportData() - column SLPOrder_csv, SubCategory add $delimiter
 *
 *	2015-05-06 Bill	[2015-0324-1420-39164]
 *	- added GET_OLE_PROGRAM_TEACHER_IN_CHARGE_BY_RECORDID() - support Teacher-in-charge function
 *	- modified GEN_ASSIGN_OLE_STUDENT_TABLE() - control layout for St.Pual Cust, 
 *	- Fixed: change field name to array - prevent too many input fields
 *	
 *	2015-02-27 Bill	[2015-0227-1546-1906]
 *	- modified GEN_ASSIGN_OLE_STUDENT_TABLE()
 *	- add $sys_custom['iPf']['Mgmt']['AssignStudentStatus_DefaultApproved'] > Default Status: Approved
 *
 *	2015-02-05 Omas
 *	new RETURN_OLE_PROGRAM_ARRAY_BY_PROGRAMID_ARRAY()
 *
 *	2015-01-21	Bill
 *	modifed GET_STUDENT_OLE_INFO(), GEN_ASSIGN_OLE_STUDENT_TABLE() for St. Paul PAS Cust
 *
 *	2015-01-13	Bill
 *	modified RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX(), RETURN_OLE_RECORD_BY_RECORDID(), GET_STUDENT_OLE_INFO(), GEN_ASSIGN_OLE_STUDENT_TABLE() for Ng Wah SAS Report Cust
 *
 *	2015-01-08 	Bill [#F73433]
 *	- modified RETURN_OLE_RECORD_BY_RECORDID(), St.Paul - get student details, others - get program details
 *
 *	2014-12-23	Bill [#J62561]
 *	- modified GET_SUBMISSION_PERIOD_FORM() to return submission period of different forms
 *
 *	2014-11-04 	Bill [#E70377]
 *	- modified displayStudentAttendanceSummary(), returnStudentAttendanceSummary() to prevent double counting when the semester name is Whole Year
 *
 *	2014-10-14 	Bill [#F63354]
 *	- modified GEN_ASSIGN_OLE_STUDENT_TABLE(), INT/EXT > Assign Student > Add Record - Drop down list display default Status 
 *
 *	2014-08-28 	Bill 
 *	- modified GEN_ASSIGN_OLE_STUDENT_TABLE(), to double the width of preset table
 *
 *	2014-07-23	Bill
 *	- modified GET_OLE_RECORD_ASSIGNED(), SET_OLE_RECORD_ORDER_SLP(), RESET_OVERSET_RECORD_SLP() to support Pui Kiu Customization
 *
 *	2014-07-16	Bill [#B63946]
 *	- modified RETURN_OLE_RECORD_BY_RECORDID() to get updated data (details)
 *
 *	2014-07-03	Ivan [B63571]
 *	- add IsSAS in OLE export	
 *		modified: function returnStudentOLRExportData, returnOLRExportHeader, returnOLRExportData, returnStudentOLRExportDataAlumni
 *		deploy: ip.2.5.5.8.1.0
 *
 *	2014-06-30 	Ivan [T62371]
 *	- modified SET_DEFAULT_SELF_ACCOUNT() to remove the extra single quote
 *		deploy: ip.2.5.5.8.1.0 (deploy: ip.2.5.5.5.1.0 critical deploy #4)
 *
 *	2014-06-26	Ivan [B62760]
 *	- add sub-category in OLE export	
 *		modified: function returnStudentOLRExportData, returnOLRExportHeader, returnOLRExportData, returnStudentOLRExportDataAlumni
 *		added: function get_OLR_SubCategory_Field_For_Record
 *		deploy: ip.2.5.5.8.1.0
 *
 *  2014-06-05  Ryan 
 *	- Partial deploy after version ip.2.5.5.5.1.0
 *	- Modified GEN_ASSIGN_OLE_STUDENT_TABLE(), Bug fix in Save issue after support SIS Customization hiding columns in OLE
 * 
 *	2013-05-02	YatWoon 
 *	- Modified GET_OLE_YEAR_RECORD_LIST(), GET_OLE_RECORD_LIST(), retrieve ELE data from OLE_PROGRAM rather than OLE_STUDENT [Case#2013-0429-1205-19066]
 *
 *	2013-03-19	Ivan [2013-0319-0949-21073]
 *	- Modified returnStudentOLRExportData(), added "ApprovedBy" and "PreferredApprover"
 *	- Modified returnOLRExportHeader(), returnStudentOLRExportDataAlumni() and returnOLRExportData(), added "PreferredApprover"
 *
 *	2013-01-22	YatWoon
 *	- Modified returnStudentOLRExportData(), add "RecordStatus" [Case#2013-0306-1631-52066]
 * 
 *	2013-01-22	YatWoon
 *	- Modified returnStudentOLRExportData(), add "ClassName" and "ClassNumber" [Case#2013-0122-1440-56071]
 *
 *  2012-06-26
 *  - Modified updateModuleYearTerm(), add checking before "foreach" loop
 *
 *  2012-03-23
 *  - Modified getSelfAccountsByYearClassUserIds(), add Para $showAll to display all records of self-account
 *
 *	2012-03-16 Connie
 * 	- Add SET_SELF_ACCOUNT_DEFAULTSA(), to update self account DefaultSA
 *
 *	2012-03-16 Connie
 * 	- Add GET_SELF_ACCOUNT_ALL_DATA(), to display All fields in Self-Account
 * 
 *  2012-03-06 henry chow [2012-0215-1501-55073]
 * 	- Modified GEN_ASSIGN_OLE_STUDENT_TABLE(), display the Achievement with $linterface->GET_TEXTAREA and set the "cols" to 30
 *
 *	2012-02-22 Ivan [2012-0221-1359-42067]
 * 	- Modified getClassInfoByGroupIds(), refineUserClassInfo(), getSelfAccountsByYearClassUserIds(), getTeacherCommentsByYearClassUserIds(), fixed "ClassTitleGB" problem for oversea clients
 *
 *	2011-10-14 Connie 
 *	- Modified RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX by adding 'OP.DefaultApprover','OP.CreatorID'
 *
 * 	2011-09-05 Ivan [CRM:2011-0902-1647-38073]
 *	- Modified updateModuleYearTerm() to map the TermID by Chinese Term Name also
 *
 *	2011-06-28 Ivan [CRM:2011-0624-1210-43096]
 *	- Modified getAssessmentLastInputDate to check the last modified info in ASSESSMENT_STUDENT_MAIN_RECORD also
 *
 *	2011-06-23 Ivan
 *	- Modified GET_OLE_RECORD_LIST, GEN_OLE_CLASSLEVEL_SELECTION, GET_OLE_CLASS_LIST, changed check admin function from IS_IPF_ADMIN to IS_IPF_SUPERADMIN 
 *
 *	2011-05-12 Ivan [CRM:2011-0509-1718-52071]
 *	- Modified returnStudentOLRExportData() and returnStudentOLRExportData, to change the export OLE "School Year From" and "School Year To" columns follow the academic year of the Program now.
 *
 *	2011-03-16 Ivan
 *	- Modified GEN_ASSIGN_OLE_STUDENT_TABLE, added OleStudentRecordID in the hidden field to fix the problem of cannot edit student's details if a student has more than 1 OLE_STUDENT records in the same program
 *
 *	2011-01-07 Ivan
 *	- Modified GET_OLE_RECORD_ASSIGNED, GET_OLE_ASSIGNED_RECORD_INFO, change the status value to use $ipf_cfg config instead of number "2", "4"
 *
 *	2010-11-22 Max (201011191622)
 *	- Modified RETURN_OLE_RECORD_BY_RECORDID to get also RequestApprovedBy
 *
 *	2010-10-28 Max(201010261902)
 *	- Modified GET_SUBMISSION_PERIOD_FORM to new html layout
 *
 *����010-09-01 Max (201008310917)
 *���� Modified GET_OLE_RECORD_LIST to get data from current year
 *
 * 2010-08-30 Max (201008301721)
 * - Added function getUserGroupsInTeacherComment
 *
 * 2010-08-13 Max (201008111611)
 * - Added functions getUserGroupsInSelfAccount,getClassInfoByGroupIds,refineUserClassInfo,getSelfAccountsByStudentIds
 * - Modify EDIT_SELF_ACCOUNT
 *
 * 2010-06-30 Max (201006300902)
 * - modified GET_OLE_SUBCATEGORY() to support getting all the subcategory if no category id is given
 * - added function GET_SUBCATEGORY_SELECTION()
 *
 * 2010-05-31 Henry Yuen (201005311151)
 * - modify function returnStudentOLRExportData, 
 * - divided function returnOLRExportContent() into returnOLRExportHeader() and returnOLRExportData()
 *
 * 2010-05-20 FAI (201005201150)
 * - function GET_OLR_APPROVAL_SETTING_DATA modified , set the return value = 0 if client has not set the value before

 * 2010-04-21 Max (201004211152)
 * - function GET_OLR_APPROVAL_SETTING() modified
 * - function GET_OLR_APPROVAL_SETTING_DATA(), GET_OLR_EDITABLE_SETTING() added
 * - function GET_RECORD_APPROVAL_FORM() modified to add one checkbox to allow student approved record to be edited
 * 
 * 2010-04-09 Eric (201004091211)
 * - Add function updateModuleYearTerm(), getModuleYearID() to update ID of academic year and semester during 

 * 2010-04-07 Max (201004071139)
 * - Modify function countOLEDataNumber(), returnStudentOLRExportData() to get data according to academic year

 * 2010-04-01 Henry Y
 * support with WebSAMS and allow users to choose displayed columns

 * 2010-03-23 Max (201003231511)
 * - Pass paramenter to GEN_ASSIGN_OLE_STUDENT_TABLE() distinguish IntExt Program to roll out different table

 * 2010-02-12: Max (201002121147)
 * function getSubCategory() added

 * 2010-02-05: Shing (201002050921)
 * - Modify GEN_ASSIGN_OLE_STUDENT_TABLE() to add ID to AllRole to prevent JS errors.
 * - Modify GEN_ASSIGN_OLE_STUDENT_TABLE() to set textarea cols to 20 to make the table narrower.
 * - Modify GEN_ASSIGN_OLE_STUDENT_TABLE() to set a minimum width on role and hours.
 * - Modify GEN_ASSIGN_OLE_STUDENT_TABLE() to remove some nowrap attributes to make the table narrower in English mode.

 * 2010-01-28: Shing (201001281156)
 * - Modify GEN_ASSIGN_OLE_STUDENT_TABLE() to add ID to attachment related fields to prevent JS errors in Firefox.

 * ??
 * - Modify [RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX()] to get Period with year and term

 * 2010-01-27: Max (201001271450)
 * - Modify [RETURN_OLE_RECORD_BY_RECORDID()] not to use CAPITAL letters as retrieved field names

 * 2009-12-30: Max (200912281012)
 * - Modified [RETURN_OLE_RECORD_BY_RECORDID()] to retrieve together with ProgramID
 
 * 2009-12-09: Max (200912091211)
 * - add creator id in insert a new OLE_PROGRAM
 */

include_once('form_class_manage.php');
include_once('libportfolio2007a.php');
class libpf_slp extends libportfolio2007
{
	var $programType = "";

	function setProgramType($type = "T")
	{
		$this->programType  = $type;
	}

	function getProgramType()
	{
		return $this->programType;
	}

	# generate Year and Semester Selections in Portfolio
	function getPortfolioYearSemester($page, $year_arr, $semester_arr, $class_arr, $Year, $Semester, $ClassName){
		global $no_record_msg, $range_all;

		$year_tag = "name='Year' onChange='this.form.year_change.value=1;this.form.action=\"$page\";this.form.submit();'";
		$sem_tag = "name='Semester' onChange='this.form.sem_change.value=1;this.form.IsAnnual[1].checked=1;this.form.action=\"$page\";this.form.submit();'";
		$class_tag = "name='ClassName' onChange='this.form.action=\"$page\";this.form.submit();'";

		$year_html = (sizeof($year_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($year_arr, $year_tag, "", $Year, true, 2);
		$semester_html = (sizeof($semester_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($semester_arr, $sem_tag, "", $Semester, true, 2);
//		$class_html = (sizeof($class_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($class_arr, $class_tag, $range_all, $ClassName, true);
		$class_html = getSelectByArrayTitle($class_arr, $class_tag, $range_all, $ClassName, false, 2);

		return array($year_html, $semester_html, $class_html);
	}
	
	function getReportYearCondition($Year, $prefix="")
	{

		if($Year!="")
		{
			$SplitArray = explode("-", $Year);
			if(sizeof($SplitArray)>1)
			{
				$Year2 = trim($SplitArray[0]);
				$conds .= " AND (".$prefix."Year = '$Year' OR ".$prefix."Year = '$Year2')";
			}
			else
				$conds .= " AND ".$prefix."Year = '$Year'";
		}

		return $conds;
	}
	
	# function to match semester to current intranet semester
	function getSemesterMatchingLists($Semester, $SemesterArr)
	{
		global $ec_iPortfolio;

		$List1 = implode("|", $ec_iPortfolio['semester_name_array_1']);
		$List2 = implode("|", $ec_iPortfolio['semester_name_array_2']);
		$List3 = implode("|", $ec_iPortfolio['semester_name_array_3']);
		if($Semester==$SemesterArr[0])
		{
			$SemList = "'".implode("', '", $ec_iPortfolio['semester_name_array_1'])."'";
		}
		else if($Semester==$SemesterArr[1])
		{
			$SemList = "'".implode("', '", $ec_iPortfolio['semester_name_array_2'])."'";
		}
		else if($Semester==$SemesterArr[2])
		{
			$SemList = "'".implode("', '", $ec_iPortfolio['semester_name_array_3'])."'";
		}

		return array($SemList, $List1, $List2, $List3);
	}
	
	function returnFilteredYearClass($StudentID, $RecordType=0, $Year="")
	{
		global $eclass_db, $intranet_db;

		$conds = ($Year!="") ? $this->getReportYearCondition($Year) : "";
		$sql = "SELECT DISTINCT
    					Year,
    					ClassName
    				FROM
    					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
    				WHERE
    					UserID = '{$StudentID}'
    					$conds
    				ORDER BY
    					Year
    			";
		$AssessmentResultArr = $this->returnArray($sql);
		for($i=0; $i<sizeof($AssessmentResultArr); $i++)
		{
			list($t_year, $t_classname) = $AssessmentResultArr[$i];
			$ReturnArray[trim($t_year)] = trim($t_classname);
		}

		$conds = ($Year!="") ? $this->getReportYearCondition($Year, "Academic") : "";
		$sql = "SELECT DISTINCT
            	AcademicYear,
            	ClassName
            FROM
            	{$intranet_db}.PROFILE_CLASS_HISTORY
            WHERE
            	UserID = '{$StudentID}'
            	$conds
            ORDER BY
            	AcademicYear
				  ";
		$AcademicResultArr = $this->returnArray($sql);
		for($i=0; $i<sizeof($AcademicResultArr); $i++)
		{
			list($t_year, $t_classname) = $AcademicResultArr[$i];
			if($ReturnArray[trim($t_year)]=="")
				$ReturnArray[trim($t_year)] = trim($t_classname);
		}

		$TableName = ($RecordType==1) ? "ATTENDANCE_STUDENT" : "MERIT_STUDENT";
		$conds = ($Year!="") ? $this->getReportYearCondition($Year) : "";
		$sql = "SELECT DISTINCT
    					Year,
    					ClassName
    				FROM
    					{$eclass_db}.{$TableName}
    				WHERE
    					UserID = '{$StudentID}'
    					$conds
    				ORDER BY
    					Year
    			";
		$RecordResultArr = $this->returnArray($sql);
		for($i=0; $i<sizeof($RecordResultArr); $i++)
		{
			list($t_year, $t_classname) = $RecordResultArr[$i];
			if($ReturnArray[trim($t_year)]=="")
				$ReturnArray[trim($t_year)] = trim($t_classname);
		}
		if(is_array($ReturnArray))
		{
		  # Eric Yip : Order year by class variable
		  if($this->YearDescending)
        krsort($ReturnArray);
      else
        ksort($ReturnArray);
		}

		return $ReturnArray;
	}
	
	function displayYearSelectBox($StudentID, $ClassName,$ParChooseYear,$RecordType="")
    {
	    //$ChooseYear = $_Post['ChooseYear'];
	    //$ChooseYear = $ParChooseYear;
	    global $ActivityYearArr, $iPort;
	    
	    # get Years and ClassName according to ASSESSMENT_STUDENT_SUBJECT_RECORD
			
	    	if($RecordType==0)
	    	{
		    	$YearClassArr = $this->returnFilteredYearClass($StudentID, 0);
	    	}else if($RecordType==1)
	    	{
		    	$YearClassArr = $this->returnFilteredYearClass($StudentID, 1);
	    	}
	    	
				$x="<select name=ChooseYear class=formtextbox onChange='document.form1.submit()'>";
				
				if($PasChooseYear=="")
				{
					$x .= "<option value='' selected>".$iPort["all_school_years"]."</option>";
				}
				
				if(count($ActivityYearArr) != 0)
				{
					for($i=0; $i<sizeof($ActivityYearArr); $i++)
					{
						list($year) = $ActivityYearArr[$i];
						if($ParChooseYear  == $year)
						{
							 $x .= "<option value=\"".$year."\" selected>".$year."</option>";
						}else{
							$x .= "<option value=\"".$year."\">".$year."</option>";
						}
					}
					$x .= "</select>";	
						
					}else{
						if(is_array($YearClassArr))
						{
							foreach($YearClassArr as $year => $ClassName)
							{
	
								if($ParChooseYear  == $year)
								{
									 $x .= "<option value=\"".$year."\" selected>".$year."</option>";
								}else{
									$x .= "<option value=\"".$year."\">".$year."</option>";
								}
							}
						}
						$x .= "</select>";
					}
				return $x;
  }

#################### Merit Data ####################

	# get Year and Semester from Merit
	function returnMeritYearSemData(){
		global $intranet_db;

		$sql = "	SELECT
							DISTINCT year, year
					FROM
							{$intranet_db}.PROFILE_STUDENT_MERIT
					WHERE year IS NOT NULL AND year <> ''
					ORDER BY
							year DESC
				";
							
		$row_year = $this->returnArray($sql);
		$sql = "	SELECT
							DISTINCT semester, semester
					FROM
							{$intranet_db}.PROFILE_STUDENT_MERIT
					WHERE semester IS NOT NULL AND semester <> ''
					ORDER BY
							semester
				";
							
		$row_semester = $this->returnArray($sql);
		

		return array($row_year, $row_semester);
	}

	# generate Year and Semester Selections for Merit
	function getMeritYearSemester(){
		global $no_record_msg;
		
		$skipTitle = true;
		
		list($year_arr, $semester_arr) = $this->returnMeritYearSemData();
		
		
		$year_html = (sizeof($year_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($year_arr, "name='Year'", "", "", $skipTitle, 2);
		
		$semester_html = (sizeof($semester_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($semester_arr, "name='Semester' onChange='this.form.IsAnnual[1].checked=1'", "", "", $skipTitle, 2);
		
		return array($year_html, $semester_html);
	}
	
	function getMeritLastInputDate()
	{
		global $eclass_db;
		$sql = "SELECT ModifiedDate FROM {$eclass_db}.MERIT_STUDENT ORDER BY ModifiedDate DESC LIMIT 1";
		$row = $this->returnVector($sql);
		
		$date = (sizeof($row)==0) ? '-' : $row[0];
		return $date;
	}
	
	function returnPortfolioMeritYearSemData($Year, $Semester, $IsAnnual){
		global $eclass_db;

		# retrieve distinct years
		$sql = "	SELECT
							DISTINCT Year, Year
					FROM
							{$eclass_db}.MERIT_STUDENT
					WHERE
						Year IS NOT NULL
						AND Year <> ''
					ORDER BY
							Year DESC
				";
		$row_year = $this->returnArray($sql);

		# retrieve distinct semesters
		$Year = ($Year=="") ? $row_year[0][0] : $Year;
		$sql = "	SELECT
							DISTINCT Semester, Semester
					FROM
							{$eclass_db}.MERIT_STUDENT
					WHERE
						Semester IS NOT NULL
						AND Semester <> ''
						AND Year = '$Year'
					ORDER BY
						Semester
				";
		$row_semester = $this->returnArray($sql);

		# retrieve distinct classes
		if($IsAnnual!=1)
		{
			$Semester = ($Semester=="") ? $row_semester[0][0] : $Semester;
			$cond = " AND Semester = '$Semester'";
		}
		$sql = "	SELECT
						DISTINCT ClassName, ClassName
					FROM
						{$eclass_db}.MERIT_STUDENT
					WHERE
						ClassName IS NOT NULL
						AND ClassName <> ''
						AND Year = '$Year'
						$cond
					ORDER BY
						ClassName
				";
		$row_class = $this->returnArray($sql);

		return array($row_year, $row_semester, $row_class);
	}
	
	# count number of merit data
	function countMeritDataNumber($my_year, $my_semester, $my_class)
	{
		global $eclass_db, $intranet_db;

		$conds = ($my_semester!="") ? " AND ms.Semester = '$my_semester'" : "";
		$conds .= ($my_class!="") ? " AND ms.ClassName = '$my_class'" : "";
		$sql = "SELECT
					COUNT(DISTINCT ms.RecordID)
				FROM
					{$eclass_db}.MERIT_STUDENT as ms
					LEFT JOIN {$intranet_db}.INTRANET_USER as iu ON ms.UserID = iu.UserID
					
				WHERE
					ms.Year = '$my_year'
					AND (iu.WebSAMSRegNo IS NOT NULL)
					$conds
				";
		$row = $this->returnVector($sql);
		//LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER as iau ON ms.UserID = iau.UserID
		// OR iau.WebSAMSRegNo IS NOT NULL
		return $row[0];
	}
	
	# Normalize merit/demerit
	function returnNormalizedMerit($meritArr)
	{
		global $plugin, $intranet_db;

		if ($plugin['Discipline'])
		{
			if(is_null($this->merit_array))
			{
				# Grab upgrade rule
				$sql = "SELECT UpgradeFromType, UpgradeNum, MeritType
				               FROM {$intranet_db}.DISCIPLINE_MERIT_TYPE_SETTING
				               WHERE MeritType IS NOT NULL AND MeritType != 0
				                     AND UpgradeFromType > 0
				                     AND UpgradeNum > 0
				               ORDER BY UpgradeFromType ASC";
				$temp = $this->returnArray($sql);
				for ($i=0; $i<sizeof($temp); $i++)
				{
					list($t_fromtype, $t_num, $t_type) = $temp[$i];
					$this->merit_array[$t_fromtype] = array($t_num, $t_type);
				}
			}
			if(is_null($this->demerit_array))
			{
				$sql = "SELECT UpgradeFromType, UpgradeNum, MeritType
				               FROM {$intranet_db}.DISCIPLINE_MERIT_TYPE_SETTING
				               WHERE MeritType IS NOT NULL AND MeritType != 0
				                     AND UpgradeFromType < 0
				                     AND UpgradeNum > 0
				               ORDER BY UpgradeFromType DESC";
				$temp = $this->returnArray($sql);
				for ($i=0; $i<sizeof($temp); $i++)
				{
				     list($t_fromtype, $t_num, $t_type) = $temp[$i];
				     $this->demerit_array[$t_fromtype] = array($t_num, $t_type);
				}
			}
			$mtypeArr = $this->returnMeritTypeAbility();

			if(is_array($meritArr))
			{
				$t_m_pt = isset($meritArr["1"])?$meritArr["1"]:0;
				$t_m_min = isset($meritArr["2"])?$meritArr["2"]:0;
				$t_m_maj = isset($meritArr["3"])?$meritArr["3"]:0;
				$t_m_sup = isset($meritArr["4"])?$meritArr["4"]:0;
				$t_m_ult = isset($meritArr["5"])?$meritArr["5"]:0;
				$t_d_pt = isset($meritArr["-1"])?$meritArr["-1"]:0;
				$t_d_min = isset($meritArr["-2"])?$meritArr["-2"]:0;
				$t_d_maj = isset($meritArr["-3"])?$meritArr["-3"]:0;
				$t_d_sup = isset($meritArr["-4"])?$meritArr["-4"]:0;
				$t_d_ult = isset($meritArr["-5"])?$meritArr["-5"]:0;

				##################################
				# Merit part
				##################################
				if ($t_m_pt != 0)
				{
					$temp = $this->merit_array[1];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_m_pt >= $r_num)
						{
							$t_m_pt -= $r_num;
							if($r_type==2 && $mtypeArr[$r_type])
							{
								$t_m_min++;
							}
							else if($r_type==3 && $mtypeArr[$r_type])
							{
								$t_m_maj++;
							}
							else if($r_type==4 && $mtypeArr[$r_type])
							{
								$t_m_sup++;
							}
							else if ($r_type==5 && $mtypeArr[$r_type])
							{
								$t_m_ult++;
							}
							else # Add back if no upgrade
							{
								$t_m_pt += $r_num;
								break;
							}
						}
					}
				}
				if ($t_m_min != 0)
				{
					$temp = $this->merit_array[2];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_m_min >= $r_num)
						{
							$t_m_min -= $r_num;
							if ($r_type==3 && $mtypeArr[$r_type])
							{
								$t_m_maj++;
							}
							else if ($r_type==4 && $mtypeArr[$r_type])
							{
								$t_m_sup++;
							}
							else if ($r_type==5 && $mtypeArr[$r_type])
							{
								$t_m_ult++;
							}
							else
							{
								$t_m_min += $r_num;
								break;
							}
						}
					}
				}
				if ($t_m_maj != 0)
				{
					$temp = $this->merit_array[3];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_m_maj >= $r_num)
						{
							$t_m_maj -= $r_num;
							if ($r_type==4 && $mtypeArr[$r_type])
							{
								$t_m_sup++;
							}
							else if ($r_type==5 && $mtypeArr[$r_type])
							{
								$t_m_ult++;
							}
							else
							{
								$t_m_maj += $r_num;
								break;
							}
						}
					}
				}
				if ($t_m_sup != 0)
				{
					$temp = $this->merit_array[4];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_m_sup >= $r_num)
						{
							$t_m_sup -= $r_num;
							if ($r_type==5 && $mtypeArr[$r_type])
							{
								$t_m_ult++;
							}
							else
							{
								$t_m_sup += $r_num;
								break;
							}
						}
					}
				}
				##################################
				# Demerit part
				##################################

				if ($t_d_pt != 0)
				{
					$temp = $this->demerit_array["-1"];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_d_pt >= $r_num)
						{
							$t_d_pt -= $r_num;
							if ($r_type==-2 && $mtypeArr[$r_type])
							{
								$t_d_min++;
							}
							else if ($r_type==-3 && $mtypeArr[$r_type])
							{
								$t_d_maj++;
							}
							else if ($r_type==-4 && $mtypeArr[$r_type])
							{
								$t_d_sup++;
							}
							else if ($r_type==-5 && $mtypeArr[$r_type])
							{
								$t_d_ult++;
							}
							else
							{
								$t_d_pt += $r_num;
								break;
							}
						}
					}
				}
				if ($t_d_min != 0)
				{
					$temp = $this->demerit_array["-2"];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_d_min >= $r_num)
						{
							$t_d_min -= $r_num;
							if ($r_type==-3 && $mtypeArr[$r_type])
							{
								$t_d_maj++;
							}
							else if ($r_type==-4 && $mtypeArr[$r_type])
							{
								$t_d_sup++;
							}
							else if ($r_type==-5 && $mtypeArr[$r_type])
							{
								$t_d_ult++;
							}
							else
							{
								$t_d_min += $r_num;
								break;
							}
						}
					}
				}
				if ($t_d_maj != 0)
				{
					$temp = $this->demerit_array["-3"];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_d_maj >= $r_num)
						{
							$t_d_maj -= $r_num;
							if ($r_type==-4 && $mtypeArr[$r_type])
							{
								$t_d_sup++;
							}
							else if ($r_type==-5 && $mtypeArr[$r_type])
							{
								$t_d_ult++;
							}
							else
							{
								$t_d_maj += $r_num;
								break;
							}
						}
					}
				}
				if ($t_d_sup != 0)
				{
					$temp = $this->demerit_array["-4"];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_m_sup >= $r_num)
						{
							$t_d_sup -= $r_num;
							if ($r_type==-5 && $mtypeArr[$r_type])
							{
								$t_d_ult++;
							}
							else
							{
								$t_d_sup += $r_num;
								break;
							}
						}
					}
				}
				##################################

				if($t_m_pt > 0)
					$result["1"] = $t_m_pt;
				if($t_m_min > 0)
					$result["2"] = $t_m_min;
				if($t_m_maj > 0)
					$result["3"] = $t_m_maj;
				if($t_m_sup > 0)
					$result["4"] = $t_m_sup;
				if($t_m_ult > 0)
					$result["5"] = $t_m_ult;

				if($t_d_pt > 0)
					$result["-1"] = $t_d_pt;
				if($t_d_min > 0)
					$result["-2"] = $t_d_min;
				if($t_d_maj > 0)
					$result["-3"] = $t_d_maj;
				if($t_d_sup > 0)
					$result["-4"] = $t_d_sup;
				if($t_d_ult > 0)
					$result["-5"] = $t_d_ult;
			}
			else
				$result = $meritArr;
		}
		else
			$result = $meritArr;

		return $result;
	}
	
	#################################  Export Start   #################################################
	########## Merit Data Export ####################
	# return Merit Year and Semester for export
	# Retrieve Student Merit Export Data
	function returnStudentMeritExportData($Year, $Semester, $ClassName)
	{
		global $eclass_db, $intranet_db;
		global $i_Merit_Merit, $i_Merit_MinorCredit, $i_Merit_MajorCredit, $i_Merit_SuperCredit, $i_Merit_UltraCredit;
		global $i_Merit_BlackMark, $i_Merit_MinorDemerit, $i_Merit_MajorDemerit, $i_Merit_SuperDemerit, $i_Merit_UltraDemerit;

		$tmp_MinorCredit = addslashes($i_Merit_MinorCredit);
		$tmp_MajorCredit = addslashes($i_Merit_MajorCredit);
		$tmp_SuperCredit = addslashes($i_Merit_SuperCredit);
		$tmp_UltraCredit = addslashes($i_Merit_UltraCredit);

		$namefield1 = getNameFieldByLang2("iu.");
		$namefield2 = getNameFieldByLang2("iau.");

		$ClassNumberField = getClassNumberField("ms.");
		$conds = ($Semester=="") ? "" : " AND ms.Semester = '$Semester'";
		$conds .= ($ClassName=="") ? "" : " AND ms.ClassName = '$ClassName'";
		$sql = "SELECT
				  if(iu.WebSAMSRegNo IS NULL, iau.WebSAMSRegNo, iu.WebSAMSRegNo) as WebSAMSRegNo,
				  ms.ClassName,
				  $ClassNumberField as ClassNumber,
				  if($namefield1 IS NULL, $namefield2, $namefield1) as UserName,
				  ms.Year,
				  ms.Semester,
				  CASE ms.RecordType
					WHEN 1 THEN '$i_Merit_Merit'
					WHEN 2 THEN '$tmp_MinorCredit'
					WHEN 3 THEN '$tmp_MajorCredit'
					WHEN 4 THEN '$tmp_SuperCredit'
					WHEN 5 THEN '$tmp_UltraCredit'
					WHEN -1 THEN '$i_Merit_BlackMark'
					WHEN -2 THEN '$i_Merit_MinorDemerit'
					WHEN -3 THEN '$i_Merit_MajorDemerit'
					WHEN -4 THEN '$i_Merit_SuperDemerit'
					WHEN -5 THEN '$i_Merit_UltraDemerit'
				  ELSE '--' END AS MeritType,
				  ms.NumberOfUnit,
				  ms.Reason,
				  ms.PersonInCharge,
				  ms.Remark
			FROM
				{$eclass_db}.MERIT_STUDENT as ms
				LEFT JOIN {$intranet_db}.INTRANET_USER AS iu ON ms.UserID = iu.UserID
				LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER as iau ON ms.UserID = iau.UserID
			WHERE
				ms.Year = '$Year'
				AND (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)
				$conds
			ORDER BY
				ms.ClassName,
				ClassNumber,
				ms.Year,
				ms.Semester
			";
		$row = $this->returnArray($sql);

		return $row;
	}

	# build merit export content
	function returnMeritExportContent($Year, $Semester, $ClassName)
	{
		global $ec_iPortfolio, $no_record_msg, $sys_custom;
		global $g_encoding_unicode;

		$delimiter = "\t";
		$valueQuote = "";

		$DataArray = $this->returnStudentMeritExportData($Year, $Semester, $ClassName);

		$Content .= $valueQuote.$ec_iPortfolio['WebSAMSRegNo'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['class'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['number'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['student'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['year'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['semester'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['mtype'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['amount'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['reason'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['pic'].$valueQuote;
		$Content .= ($sys_custom['iPortfolioHideRemark']==true) ? "\r\n" : $delimiter.$valueQuote.$ec_iPortfolio['remark'].$valueQuote."\r\n";

		if(sizeof($DataArray)!=0)
		{
			for($i=0; $i<sizeof($DataArray); $i++)
			{
				$RegNo = (substr($DataArray[$i]["WebSAMSRegNo"],0,1)!="#") ? "#".$DataArray[$i]["WebSAMSRegNo"] : $DataArray[$i]["WebSAMSRegNo"];
				$Content .= $valueQuote.$RegNo.$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ClassName"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ClassNumber"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["UserName"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Year"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Semester"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["MeritType"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["NumberOfUnit"].$valueQuote.$delimiter;
				$Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Reason"]).$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["PersonInCharge"].$valueQuote;
				$Content .= ($sys_custom['iPortfolioHideRemark']==true) ? "\n" : $delimiter.$valueQuote.handleCSVExportContent($DataArray[$i]["Remark"]).$valueQuote."\r\n";
			}
		}
		else
		{
			$Content .= $valueQuote.$no_record_msg.$valueQuote."\r\n";
		}
		return $Content;
	}
	
	function returnMeritReportTitleArray()
	{
		global $ec_iPortfolio, $i_Merit_TypeArray;

		$title_array[] = $ec_iPortfolio['year'];
		$title_array[] = $ec_iPortfolio['class'];
		$title_array[] = $ec_iPortfolio['semester'];

        return array_merge($title_array,$i_Merit_TypeArray);
	}
	
	function retrieveMeritSettings()
        {
                 global $intranet_root;

				 if ($this->LangID==null)
				{
					$LangID = get_file_content($intranet_root."/file/language.txt");
					$LangID += 0;

					$this->$LangID = $LangID;
				}

                 $setting_file = "$intranet_root/file/std_profile_settings.txt";
                 $content = get_file_content($setting_file);
                 $array = explode("\n",$content);

                 # Display settings
                 $this->is_frontend_attendance_hidden = ($array[0][0]==1);
                 $this->is_printpage_attendance_hidden = ($array[0][1]==1);
                 $this->is_frontend_merit_hidden = ($array[1][0]==1);
                 $this->is_printpage_merit_hidden = ($array[1][1]==1);
                 $this->is_frontend_service_hidden = ($array[2][0]==1);
                 $this->is_printpage_service_hidden = ($array[2][1]==1);
                 $this->is_frontend_activity_hidden = ($array[3][0]==1);
                 $this->is_printpage_activity_hidden = ($array[3][1]==1);
                 $this->is_frontend_award_hidden = ($array[4][0]==1);
                 $this->is_printpage_award_hidden = ($array[4][1]==1);

                 # Field using settings
                 $field_setting_file = "$intranet_root/file/std_profile_field.txt";
                 $content = get_file_content($field_setting_file);
                 $array = explode("\n",$content);
                 $this->is_merit_disabled = ($array[0][0]==1);
                 $this->is_min_merit_disabled = ($array[0][1]==1);
                 $this->is_maj_merit_disabled = ($array[0][2]==1);
                 $this->is_sup_merit_disabled = ($array[0][3]==1);
                 $this->is_ult_merit_disabled = ($array[0][4]==1);
                 $this->is_black_disabled = ($array[1][0]==1);
                 $this->is_min_demer_disabled = ($array[1][1]==1);
                 $this->is_maj_demer_disabled = ($array[1][2]==1);
                 $this->is_sup_demer_disabled = ($array[1][3]==1);
                 $this->is_ult_demer_disabled = ($array[1][4]==1);
                 $this->col_merit = 0;
                 $this->col_demerit = 0;

                 if (!$this->is_merit_disabled) $this->col_merit++;
                 if (!$this->is_min_merit_disabled) $this->col_merit++;
                 if (!$this->is_maj_merit_disabled) $this->col_merit++;
                 if (!$this->is_sup_merit_disabled) $this->col_merit++;
                 if (!$this->is_ult_merit_disabled) $this->col_merit++;
				 if (!$this->is_black_disabled) $this->col_demerit++;
                 if (!$this->is_min_demer_disabled) $this->col_demerit++;
                 if (!$this->is_maj_demer_disabled) $this->col_demerit++;
                 if (!$this->is_sup_demer_disabled) $this->col_demerit++;
                 if (!$this->is_ult_demer_disabled) $this->col_demerit++;
        }
        
	function returnMeritTypeAbility()
	{
		# Retrieve Merit Setting
		$this->retrieveMeritSettings();

    $mtypeArr['1'] = ($this->is_merit_disabled) ? 0 : 1;
    $mtypeArr['2'] = ($this->is_min_merit_disabled) ? 0 : 1;
    $mtypeArr['3'] = ($this->is_maj_merit_disabled) ? 0 : 1;
    $mtypeArr['4'] = ($this->is_sup_merit_disabled) ? 0 : 1;
    $mtypeArr['5'] = ($this->is_ult_merit_disabled) ? 0 : 1;
    $mtypeArr['-1'] = ($this->is_black_disabled) ? 0 : 1;
    $mtypeArr['-2'] = ($this->is_min_demer_disabled) ? 0 : 1;
    $mtypeArr['-3'] = ($this->is_maj_demer_disabled) ? 0 : 1;
    $mtypeArr['-4'] = ($this->is_sup_demer_disabled) ? 0 : 1;
    $mtypeArr['-5'] = ($this->is_ult_demer_disabled) ? 0 : 1;

		return $mtypeArr;
	}
	
	function returnMeritFieldAbility()
	{
		# Retrieve Merit Setting
		$this->retrieveMeritSettings();

		$pre_array = array(0,0,0);
		$merit_disabled_array = array(  $this->is_merit_disabled, $this->is_min_merit_disabled,
                  									$this->is_maj_merit_disabled, $this->is_sup_merit_disabled,
                  									$this->is_ult_merit_disabled, $this->is_black_disabled,
                                    $this->is_min_demer_disabled, $this->is_maj_demer_disabled,
                                    $this->is_sup_demer_disabled, $this->is_ult_demer_disabled);
		$field_disabled_array = array_merge($pre_array, $merit_disabled_array);

		return $field_disabled_array;
	}
	
	# Retrieve Student Merit Summary
	function returnStudentMeritSummary($StudentID, $Year="", $Semester="")
	{
		global $eclass_db, $ec_iPortfolio;

		//$Year = $_Post['ChooseYear'];

		//$Year = '2004-2005';

		$conds = $this->getReportYearCondition($Year);

		# handle semester different problem
		list($SemesterArr, $ShortSemesterArr) = $this->getSemesterNameArrFromIP();
		list($SemList, $List1, $List2, $List3) = $this->getSemesterMatchingLists($Semester, $SemesterArr);
		if($Semester!="")
		{
			$conds .= "AND (Semester = '$Semester' OR Semester IN ({$SemList}))";
		}

		# records grouping by year and semester
		$sql = "SELECT
					Year,
					if(INSTR('{$List1}', Semester),'".$SemesterArr[0]."',if(INSTR('{$List2}',  Semester),'".$SemesterArr[1]."',if(INSTR('{$List3}',  Semester),'".$SemesterArr[2]."', Semester))),
					RecordType,
					SUM(NumberOfUnit)
				FROM
					$eclass_db.MERIT_STUDENT
				WHERE
					UserID = '$StudentID'
					$conds
				GROUP BY
					Year,
					Semester,
					RecordType
				ORDER BY
					Year DESC,
					Semester DESC,
					RecordType DESC
				";
		$row = $this->returnArray($sql);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($year, $semester, $type, $count) = $row[$i];
			$tmp_result[$year][$semester][$type] = $count;

			# get semester total
			//$result[$year][$ec_iPortfolio['whole_year']][$type] = $result[$year][$ec_iPortfolio['whole_year']][$type] + $count;
			if ($semester!=$ec_iPortfolio['whole_year'])
			{
				$tmp_result[$year][$ec_iPortfolio['whole_year']][$type] +=  $count;
			}
		}

		if(is_array($tmp_result))
		{
			foreach($tmp_result as $t_year => $t_arr1)
			{
				if(is_array($t_arr1))
				{
					foreach($t_arr1 as $t_sem => $t_arr2)
					{
						# Eric Yip : Normalize merits/ demerits
						$result[$t_year][$t_sem] = $this->returnNormalizedMerit($t_arr2);
					}
				}
			}
		}
		else
			$result = $tmp_result;

		return $result;
	}
	
	# Convert Student Merit Summary, copy from retrieveStudentConvertedMeritTypeCountByDate() in libdisciplinev12.php
	function convertStudentMeritSummary($dataAry)
  {
      include_once("libdisciplinev12.php");
      
      $ldiscipline = new libdisciplinev12();
      # Get Promotion Method
			$PromotionMethod = $ldiscipline->getDisciplineGeneralSetting("PromotionMethod");
			
      $MeritPromotionSetting = $ldiscipline->retrieveMeritPromotionInfo($PromotionMethod);
      
      if($PromotionMethod == "global")
			{
        # $dataAry[year][sem][meritType] = no. of meritType (this array is for one student)
        # $MeritPromotionSetting[meritType] = array(FromMerit(meritType), ToMerit, UpgradeNum)        
        foreach($dataAry as $year=> $semMeritArr){
          foreach($semMeritArr as $sem =>$meritDataArr){
            foreach($meritDataArr as $meritType => $thisMeritVal){

              $thisFromMerit = $MeritPromotionSetting[$meritType]["UpgradeFromType"];
    					$thisToMerit = $MeritPromotionSetting[$meritType]["MeritType"];
    					$thisUpgradeNum = $MeritPromotionSetting[$meritType]["UpgradeNum"];
              
              if(empty($thisMeritVal) || empty($thisUpgradeNum)) continue;

              $dataAry[$year][$sem][$thisToMerit] += floor($thisMeritVal/$thisUpgradeNum);
					    $dataAry[$year][$sem][$thisFromMerit] = fmod($thisMeritVal,$thisUpgradeNum);
            
            }
          }          
        }
          	
				$returnAry = $dataAry;
			}
			else
      {
        # 20091208, not support category conversion
        /*
        foreach($MeritPromotionSetting as $thisCatID =>$thisCatMeritPromotionSetting)
				{
					foreach($thisCatMeritPromotionSetting as $thisMeritPromotionSetting)
					{
						$thisFromMerit = $thisMeritPromotionSetting["UpgradeFromType"];
						$thisToMerit = $thisMeritPromotionSetting["MeritType"];
						$thisUpgradeNum = $thisMeritPromotionSetting["UpgradeNum"];
						
						if(empty($dataAry[$thisCatID][$thisFromMerit]) || empty($thisUpgradeNum)) continue;
						
						$dataAry[$thisCatID][$thisToMerit] += floor($RawDataAry[$thisCatID][$thisFromMerit]/$thisUpgradeNum);
						$dataAry[$thisCatID][$thisFromMerit] = fmod($RawDataAry[$thisCatID][$thisFromMerit],$thisUpgradeNum);
					}
				}
				$returnAry = Array();	
				foreach ($dataAry as $thisCatMeritCount)
				{
					foreach($thisCatMeritCount as $thisMeritType =>$thisMeritCount)
					{
						$returnAry[$thisMeritType] += $thisMeritCount;
					}
				}
				
				
        */        
        $returnAry = $dataAry;
      }
      
			return $returnAry;
      
  }
  	
	# Display Student Merit Summary
	function displayStudentMeritSummary($StudentID, $ClassName, $PassChooseYear="", $ParChangeLayer=false)
    {
		global $intranet_db, $ec_iPortfolio, $i_Profile_Absent, $i_Profile_Late, $i_Profile_EarlyLeave, $no_record_msg;

		# get the merit summary
		$result = $this->returnStudentMeritSummary($StudentID, $PassChooseYear);

		# get title info
        $title_array = $this->returnMeritReportTitleArray();
		$field_disabled_array = $this->returnMeritFieldAbility();
		$mtypeArr = $this->returnMeritTypeAbility();

		# get Years and ClassName according to ASSESSMENT_STUDENT_SUBJECT_RECORD
		$YearClassArr = $this->returnFilteredYearClass($StudentID, 0,$PassChooseYear);

/*
		# retrieve school semesters
		list($SemesterArr, $ShortSemesterArr) = $this->getSemesterNameArrFromIP();
		$SemesterArr[] = $ec_iPortfolio['whole_year'];
*/

		$TypeCount = 0;
	//	$x = "<table width='98%' border='0' cellpadding='0' cellspacing='0' class='table_b'>\n";
		$x = "<table width=100% border=0 cellpadding=4 cellspacing=0 bgcolor=#CCCCCC>\n";
		$x .= "<tr class='tabletop'>\n";
		//$x .= "<td width='20'>&nbsp;</td>";
		for ($i=0; $i<sizeof($title_array); $i++)
		{
			if (!$field_disabled_array[$i])
			{
				$td_style = ($i<=2) ? " width='150' align='left'" : " width='100' align='center'";
				if ($i>2)
				{
					if($i<8)
					{
                     	$summary_prefix_merit .= (($summary_prefix_merit!="")?"SePa":"") . $title_array[$i];
					}
					else
					{
						$summary_prefix_demerit .= (($summary_prefix_demerit!="")?"SePa":"") . $title_array[$i];
					}
                }
				$TypeCount++;
				$x .= "<td class='tabletopnolink' $td_style><b>".$title_array[$i]."</b></td>";
				//$td_style = ($i<=2) ? " width='150' align='left'" : " width='100' align='center'";
				//$td_style = ($i<=0) ? " width='550' align='left'" : " width='60' align='center'";
				/*
				if($i<=0)
				{
					$td_style =" width='550' align='left'";
					
				}else if($i>0 && $i <=2)
				{
					$td_style =" width='200' align='center'";
					
				}else
				{
					$td_style =" width='60' align='center'";
				}
				
				if ($i>2)
				{
					
					if($i<8)
					{
                     	$summary_prefix_merit .= (($summary_prefix_merit!="")?"SePa":"") . $title_array[$i];
					}
					else
					{
						$summary_prefix_demerit .= (($summary_prefix_demerit!="")?"SePa":"") . $title_array[$i];
					}
					
                }
				$TypeCount++;
				// The Report Title
				$x .= "<td class='tabletopnolink' $td_style><b>".$title_array[$i]."</b></td>";
				*/
			}
		}
        $x .= "</tr>\n";

        //Report Content
		if(!empty($YearClassArr))
		{
			$num = 0;

			foreach($YearClassArr as $year => $ClassName)
			{
				$CurrYear = "";
				
				# Eric Yip (20100211): Get semester array according to year name
				# (Not work for years with same name)
				unset($SemesterArr);
				$sql = "SELECT AcademicYearID FROM {$intranet_db}.ACADEMIC_YEAR WHERE YearNameEN = '".$year."'";
				$ay_id = current($this->returnVector($sql));
				$SemesterArr = array_values(getSemesters($ay_id));
				$SemesterArr[] = $ec_iPortfolio['whole_year'];
				
				for($j=0; $j<sizeof($SemesterArr); $j++)
				{
					$sem = trim($SemesterArr[$j]);
					if($year!=$CurrYear)
					{
						$bcolor = ($num%2!=0) ? "class=tablerow2" : "class=tablerow1";
						$tcolor = ($num%2!=0) ? "tablerow_total2" : "tablerow_total";
						$x .= "<tr $bcolor>\n";
						//$x .= "<td class=tabletext>&nbsp;</td>\n";
						$x .= "<td class=tabletext>$year</td>\n";
						$x .= "<td align=left class=tabletext>$ClassName</td>\n";
						$num++;
						$CurrYear = $year;
					}
					else
					{
						$x .= "<tr $bcolor>\n";
						//$x .= "<td class=tabletext>&nbsp;</td>\n";
						$x .= "<td  align=center class=tabletext>&nbsp;</td>\n";
						$x .= "<td  align=center class=tabletext>&nbsp;</td>\n";
					}
					//$stylecolor = ($num%2==0) ? "#FFFFFF" : "#DDDDDD";
					//$style = ($sem!=$ec_iPortfolio['whole_year']) ? "style='border-bottom: 1px dashed $stylecolor;'" : "";
					$style = ($sem!=$ec_iPortfolio['whole_year']) ? "class='tabletext tablerow_underline'" : "class='tabletext tablerow_underline $tcolor'";
					$summary_merit = $summary_prefix_merit;
					$summary_demerit = $summary_prefix_demerit;

					$ValueField = "";
					foreach($mtypeArr as $type => $valid)
					{
						if($valid==1)
						{
							$value = ($result[$year][$sem][$type]!='') ? $result[$year][$sem][$type] : 0;
							if($type>0)
							{
								$summary_merit .= "SePa".$value;
							}
							else
							{
								$summary_demerit .= "SePa".$value;
							}
							$ValueField .= "<td align='center' $style>".($sem==$ec_iPortfolio['whole_year'] ? "<strong>$value</strong>" : $value)."</td>\n";

							# get overall total
							if($sem==$ec_iPortfolio['whole_year'])
								$result[$ec_iPortfolio['total']][$type] = $result[$ec_iPortfolio['total']][$type] + $value;
						}
					}
					if($ParChangeLayer)
						$SemField = "<td align='left' $style><a href='#' onClick=\"jVIEW_DETAIL('merit', 'semester=$sem&year=$year&summary_merit=$summary_merit&summary_demerit=$summary_demerit')\" class='tablelink'>".($sem==$ec_iPortfolio['whole_year'] ? "<strong>$sem</strong>" : $sem)."</a></td>\n";
					else
						$SemField = "<td align='left' $style><a href='details.php?StudentID=$StudentID&ClassName=$ClassName&semester=$sem&year=$year&summary_merit=$summary_merit&summary_demerit=$summary_demerit' class='tablelink'>".($sem==$ec_iPortfolio['whole_year'] ? "<strong>$sem</strong>" : $sem)."</a></td>\n";

					$x .= $SemField.$ValueField."\n";
					$x .= "</tr>\n";
				}
			}
			//$bcolor = ($num%2!=0) ? "class=tablerow2" : "class=tablerow1";
			$bcolor = "class=tablebluebottom";
			$x .= "<tr $bcolor>\n";
			//$x .= "<td class=tabletext>&nbsp;</td>\n";
			$x .= "<td class=tabletext><strong>".$ec_iPortfolio['total']."</strong></td>\n";
			$x .= "<td class=tabletext>&nbsp;</td>\n";
			$x .= "<td class=tabletext>&nbsp;</td>\n";
			foreach($mtypeArr as $type => $valid)
			{
				if($valid==1)
				{
					$x .= "<td align='center' class='tabletext'><strong>".($result[$ec_iPortfolio['total']][$type]!='' ? $result[$ec_iPortfolio['total']][$type] : 0)."</strong></td>\n";
				}
			}
			$x .= "</tr>\n";
		}
		else
		{
			$x .= "<tr class=tablerow1 valign='middle'><td height='100' colspan='".($TypeCount+3)."' align='center' class=tabletext>".$no_record_msg."</td></tr>";
		}
		$x .= "</table>\n";

		return $x;
	}
	
	########## Update Merit Records From IP #######################
	function updateMeritFromIP($my_year, $my_semester){
		global $intranet_db, $eclass_db;
		
		# Retrieve merit data from eclassIP_dev.PROFILE_STUDENT_MERIT
		$MeritData = $this->retrieveMeritDataFromIP($my_year, $my_semester);
		
		if(sizeof($MeritData)==0)
		{
			return 0;
		}

		# Clear data from ec3dev_eclass.MERIT_STUDENT
		$this->clearMeritData($my_year, $my_semester);

		# Insert merit data into ec3dev_eclass.MERIT_STUDENT
		$fields = "(UserID, Year, Semester, MeritDate, IsAnnual, NumberOfUnit, Reason, PersonInCharge, RecordType, RecordStatus, Remark, ClassName, ClassNumber, ModifiedDate, InputDate)";

		$IsAnnual = 0;
		$count = 0;
		$lastID = "";
		for($i=0; $i<sizeof($MeritData); $i++)
		{
			$temp = $MeritData[$i];

			$values = "(".$temp['UserID'].", ";
			$values .= "'".$temp['Year']."', ";
			$values .= "'".addslashes($temp['Semester'])."', ";
			$values .= "'".$temp['MeritDate']."', ";
			$values .= $IsAnnual.", ";
			$values .= $temp['NumberOfUnit'].", ";
			$values .= "'".addslashes($temp['Reason'])."', ";
			$values .= "'".addslashes($temp['PersonInCharge'])."', ";
// 			$values .= $temp['RecordType'].", "; modified by adding the single quote to RecordType by simonyu
			$values .= "'".$temp['RecordType']."', ";
			$values .= ($temp['RecordStatus']!="") ? "'".$temp['RecordStatus']."', " : "NULL,";
			$values .= "'".addslashes($temp['Remark'])."', ";
			$values .= "'".addslashes($temp['ClassName'])."', ";
			$values .= "'".$temp['ClassNumber']."', ";
			$values .= "'".$temp['DateModified']."', now())";

			$sql = "INSERT INTO $eclass_db.MERIT_STUDENT $fields VALUES $values";
			
			$this->db_db_query($sql);
			
			$recordID = $this->db_insert_id();
			if($recordID != "" && $lastID != $recordID)
			{
				$count++;
				$lastID = $recordID;
			}
		}
		
		// Eric Yip (20100409): Update ID of academic year and term
		$this->updateModuleYearTerm("merit", $my_year, $my_semester);
		
		return $count;
	}
	
	function retrieveMeritDataFromIP($my_year, $my_semester)
	{
		global $intranet_db, $eclass_db;
		
		$ClassNumberField = getClassNumberField("a.");
		$conds = ($my_semester!="") ? " AND a.Semester = '$my_semester'" : "";
		$namefield = getNameFieldByLang2("c.");

				$sql = "SELECT
						a.UserID,
						a.Year,
						a.Semester,
						a.MeritDate,
						IF(a.PersonInCharge IS NULL OR a.PersonInCharge=0,'--',$namefield) as PersonInCharge,
						a.NumberOfUnit,
						a.Reason,
						a.RecordType,
						a.RecordStatus,
						a.Remark,
						a.ClassName,
						$ClassNumberField as ClassNumber,
						a.DateModified
					FROM
						{$intranet_db}.PROFILE_STUDENT_MERIT as a
						LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT as b ON a.UserID = b.UserID
						LEFT JOIN {$intranet_db}.INTRANET_USER as c ON a.PersonInCharge = c.UserID
					WHERE
						b.UserKey IS NOT NULL
						AND b.WebSAMSRegNo IS NOT NULL
						AND a.Year = '$my_year'
						$conds
					ORDER BY a.UserID ASC
							";
					
		$row = $this->returnArray($sql, 13);
		return $row;
	}

	function clearMeritData($my_year, $my_semester)
	{
		global $eclass_db;

		$conds = ($my_semester!="") ? " AND Semester = '$my_semester'" : "";
		$sql = "DELETE FROM $eclass_db.MERIT_STUDENT WHERE Year = '$my_year' $conds";
		$this->db_db_query($sql);
	}
	
#################### Assesssment Data ####################
	
	function getAssessmentLastInputDate()
	{
		global $eclass_db;
		$sql = "SELECT ModifiedDate FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ORDER BY ModifiedDate DESC LIMIT 1";
		$row = $this->returnVector($sql);
		$LastModified_Subject = $row[0];
		
		$sql = "SELECT ModifiedDate FROM {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD ORDER BY ModifiedDate DESC LIMIT 1";
		$row = $this->returnVector($sql);
		$LastModified_Main = $row[0];

		//$date = (sizeof($row)==0) ? '-' : $row[0];
		$date = max($LastModified_Subject, $LastModified_Main);
		$date = ($date=='')? '-' : $date;
		return $date;
	}
	
	# get Year and Semester from ASSESSMENT_STUDENT_SUBJECT_RECORD in Portfolio
	function returnPortfolioAssessmentYearSemData($Year, $Semester, $IsAnnual, $Assessment=''){
		global $eclass_db, $ec_iPortfolio;

		# retrieve distinct years
		$sql = "	SELECT
						DISTINCT Year, Year
					FROM
						{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
					WHERE
						Year IS NOT NULL
						AND Year <> ''
					ORDER BY
						Year DESC
				";
		$row_year_subject = $this->returnArray($sql);
		$row_year_subject_asso = BuildMultiKeyAssoc($row_year_subject, 'Year');
		
		$sql = "	SELECT
						DISTINCT Year, Year
					FROM
						{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
					WHERE
						Year IS NOT NULL
						AND Year <> ''
					ORDER BY
						Year DESC
				";
		$row_year_main = $this->returnArray($sql);
		$row_year_main_asso = BuildMultiKeyAssoc($row_year_subject, 'Year');
		
		$AcademicYearArr = array_values(array_unique(array_merge(Get_Array_By_Key($row_year_subject, 'Year'), Get_Array_By_Key($row_year_main, 'Year'))));
		$numOfAcademicYear = count((array)$AcademicYearArr);
		$row_year = array();
		for ($i=0; $i<$numOfAcademicYear; $i++) {
			$thisAcademicYear = $AcademicYearArr[$i];
			
			if (isset($row_year_subject_asso[$thisAcademicYear])) {
				$row_year[] = $row_year_subject_asso[$thisAcademicYear];
			}
			else if (isset($row_year_main_asso[$thisAcademicYear])) {
				$row_year[] = $row_year_main_asso[$thisAcademicYear];
			}
		}

		# retrieve distinct semesters
		$Year = ($Year=="") ? $row_year[0][0] : $Year;
		if($Assessment){
			$sql = "	SELECT
							DISTINCT IF(TermAssessment IS NULL,Semester, CONCAT(Semester,'_',TermAssessment)), IF(TermAssessment IS NULL, Semester, TermAssessment)
						FROM
							{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
						WHERE
							 Year = '$Year'
							AND	Semester IS NOT NULL
							AND Semester <> ''
						ORDER BY
							Semester
					";
		}else{
							
			$sql = "	SELECT
							DISTINCT Semester, Semester
						FROM
							{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
						WHERE
							Semester IS NOT NULL
							AND Semester <> ''
							AND Year = '$Year'
						ORDER BY
							Semester
					";
		}
		$row_semester = $this->returnArray($sql);

		# retrieve distinct classes
		if($IsAnnual!=1)
		{
			$Semester = ($Semester=="") ? $row_semester[0][0] : $Semester;
			if($Assessment){
				$Semester = explode('_',$Semester);
					if(sizeof($Semester)>1){
						$cond = " AND Semester = '$Semester[0]' AND TermAssessment ='$Semester[1]' ";
					}else{
						$cond = ($Semester[0]==$ec_iPortfolio['overall_result']) ? " AND IsAnnual = '1'" : " AND Semester = '$Semester[0]'";
					}
			}else{
				$cond = ($Semester==$ec_iPortfolio['overall_result']) ? " AND IsAnnual = '1'" : " AND Semester = '$Semester'";
			}
		}
		$sql = "	SELECT
						DISTINCT ClassName, ClassName
					FROM
						{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
					WHERE
						ClassName IS NOT NULL
						AND ClassName <> ''
						AND Year = '$Year'
						$cond
					ORDER BY
						ClassName
				";
		$row_class = $this->returnArray($sql);

		# check whether whole year records exist in the year
		$sql = "SELECT COUNT(*) FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE Year = '$Year' AND IsAnnual = '1'";
		$RecordExist = $this->returnVector($sql);
		if($RecordExist[0]>0)
		{
			$row_semester[] = array($ec_iPortfolio['overall_result'], $ec_iPortfolio['overall_result']);
		}

		return array($row_year, $row_semester, $row_class);
	}
	
	# count number of assessment data
	function countAssessmentDataNumber($my_year, $my_semester, $my_class, $report_type=0)
	{
		global $eclass_db, $intranet_db, $ec_iPortfolio;
	
		if($my_semester!="")
		{
			$my_semester = explode('_',$my_semester); //Villa
			if(sizeof($my_semester)>1){
				$conds = " AND a.Semester = '$my_semester[0]' AND a.TermAssessment ='$my_semester[1]' AND a.IsAnnual = 0 ";
			}else{
				$conds = ($my_semester[0]==$ec_iPortfolio['overall_result']) ? " AND a.IsAnnual = 1" : " AND a.Semester = '$my_semester[0]' AND a.IsAnnual = 0 AND a.TermAssessment IS NULL";
			}
		}
		$conds .= ($my_class!="") ? " AND a.ClassName = '$my_class'" : "";
		$table_name = ($report_type==1) ? "ASSESSMENT_STUDENT_MAIN_RECORD" : "ASSESSMENT_STUDENT_SUBJECT_RECORD";
		$sql = "SELECT
					COUNT(DISTINCT a.RecordID)
				FROM
					{$eclass_db}.$table_name as a
					LEFT JOIN {$intranet_db}.INTRANET_USER as iu ON a.UserID = iu.UserID
				
				WHERE
					a.Year = '$my_year'
					AND (iu.WebSAMSRegNo IS NOT NULL)
					$conds
				";
	   //	LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER as iau ON a.UserID = iau.UserID
	   // 		 OR iau.WebSAMSRegNo IS NOT NULL
		$row = $this->returnVector($sql);
		return $row[0];
	}
	
	########## Attendance Data Export ####################
	# Retrieve Assessment Export Data
	private function returnStudentAssessmentExportData($AcademicYearID, $YearTermID, $ClassNameArr, $report_type)
	{
		global $eclass_db, $intranet_db, $ec_iPortfolio, $ck_default_lang;

		$namefield1 = getNameFieldByLang2("iu.");
		$namefield2 = getNameFieldByLang2("iau.");

		$ClassNumberField = getClassNumberField("a.");
		if($YearTermID !=""){
			$YearTermID = explode('_', $YearTermID);
			if(sizeof($YearTermID)>1){
				$conds = " AND a.YearTermID = '".$YearTermID[0]."' AND a.TermAssessment  = '".$YearTermID[1]."'";
			}else{
				$conds = "AND  a.YearTermID = '".$YearTermID[0]."' AND a.TermAssessment IS NULL";
			}
			
		}else{
			$conds = " AND IFNULL(a.YearTermID, 0) = 0";
		}
// 		$conds = ($YearTermID == "") ? " AND IFNULL(a.YearTermID, 0) = 0" : " AND a.YearTermID = '".$YearTermID."'";
		$conds .= (count($ClassNameArr) > 0) ? " AND a.ClassName IN ('".implode("', '", $ClassNameArr)."')" : "";
		if($report_type==0)
		{
			//if(a.SubjectComponentCode IS NULL,a.SubjectCode,CONCAT(a.SubjectCode,' - ',a.SubjectComponentCode)) as SubjectName,
			$sql = "SELECT DISTINCT
						a.RecordID,
						IFNULL(iu.WebSAMSRegNo, iau.WebSAMSRegNo) as WebSAMSRegNo,
						a.ClassName,
						$ClassNumberField as ClassNumber,
						if($namefield1 IS NULL, $namefield2, $namefield1) as UserName,
						a.Year,
						if((a.IsAnnual=1 OR IFNULL(a.YearTermID, 0) = 0), '".$ec_iPortfolio['overall_result']."',if(a.TermAssessment IS NULL,a.Semester, a.TermAssessment )) as Semester,
						a.SubjectCode,
						a.SubjectComponentCode,
						a.Score,
						a.Grade,
						a.OrderMeritForm
					FROM
						{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
						LEFT JOIN {$intranet_db}.INTRANET_USER AS iu ON a.UserID = iu.UserID
						LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER AS iau ON a.UserID = iau.UserID
					WHERE
						a.AcademicYearID = '$AcademicYearID'
						AND (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)
						$conds
					ORDER BY
						a.ClassName,
						ClassNumber,
						a.Year,
						a.Semester,
						a.SubjectCode,
						a.SubjectComponentCode
				";
		}
		else
		{
			$sql = "SELECT
						IFNULL(iu.WebSAMSRegNo, iau.WebSAMSRegNo) as WebSAMSRegNo,
						$ClassNumberField as ClassNumber,
						a.ClassNumber,
						if($namefield1 IS NULL, $namefield2, $namefield1) as UserName,
						a.Year,
						if((a.IsAnnual=1 OR IFNULL(a.YearTermID, 0) = 0), '".$ec_iPortfolio['overall_result']."',a.Semester) as Semester,
						a.Score,
						a.Grade,
						a.OrderMeritForm
					FROM
						{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD as a
						LEFT JOIN {$intranet_db}.INTRANET_USER AS iu ON a.UserID = iu.UserID
						LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER AS iau ON a.UserID = iau.UserID
					WHERE
						a.AcademicYearID = '$AcademicYearID'
						$conds
					ORDER BY
						a.ClassName,
						ClassNumber,
						a.Year,
						a.Semester
				";
		}
		$row = $this->returnArray($sql);

		return $row;
	}

	# buid assessement export content
	function returnAssessmentExportContent($AcademicYearID, $YearTermID, $ClassNameArr, $report_type)
	{
		global $eclass_db, $ec_iPortfolio, $no_record_msg, $ec_iPortfolio_Report;
		global $g_encoding_unicode;

		$delimiter = "\t";
		$valueQuote = "";

		$DataArray = $this->returnStudentAssessmentExportData($AcademicYearID, $YearTermID, $ClassNameArr, $report_type);
		$SubjectAssoArray = $this->returnSubjectAssoArray();

		$Content .= $valueQuote.$ec_iPortfolio['WebSAMSRegNo'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['class'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['number'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['student'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['year'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['semester'].$valueQuote.$delimiter;
		$Content .= ($report_type==0) ? $valueQuote.$ec_iPortfolio_Report['subject'].$valueQuote.$delimiter : "";
		$Content .= $valueQuote.$ec_iPortfolio['score'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['grade'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['rank'].$valueQuote."\n";

		if(sizeof($DataArray)!=0)
		{
			for($i=0; $i<sizeof($DataArray); $i++)
			{
				$RegNo = (substr($DataArray[$i]["WebSAMSRegNo"],0,1)!="#") ? "#".$DataArray[$i]["WebSAMSRegNo"] : $DataArray[$i]["WebSAMSRegNo"];
				$Content .= $valueQuote.$RegNo.$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ClassName"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ClassNumber"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["UserName"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Year"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Semester"].$valueQuote.$delimiter;
				if($report_type==0)
				{
					# get subject name
					$sbj_code = $DataArray[$i]["SubjectCode"];
					$cmp_sbj_code = $DataArray[$i]["SubjectComponentCode"];
					$SubjectName = ($cmp_sbj_code=="") ? $SubjectAssoArray[trim($sbj_code)]["main"] : $SubjectAssoArray[trim($sbj_code)]["main"]." - ".$SubjectAssoArray[trim($sbj_code)][trim($cmp_sbj_code)];
					$Content .= $valueQuote.handleCSVExportContent($SubjectName).$valueQuote.$delimiter;
				}
				$Content .= $valueQuote.$DataArray[$i]["Score"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Grade"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["OrderMeritForm"].$valueQuote."\n";
			}
		}
		else
		{
			$Content .= $valueQuote.$no_record_msg.$valueQuote."\n";
		}
		return $Content;
	}

	private function returnSubjectAssoArray()
	{
    global $lpf_sturec;
	
		$row_cmp_subject = $lpf_sturec->returnAllSubjectWithComponents();
		for($i=0; $i<sizeof($row_cmp_subject); $i++)
		{
			list($sbj_code, $cmp_sbj_code, $sbj_name, $cmp_sbj_name) = $row_cmp_subject[$i];
			if($cmp_sbj_code=="")
			{
				$ReturnArray[trim($sbj_code)]["main"] = $sbj_name;
			}
			else
			{
				$ReturnArray[trim($sbj_code)][trim($cmp_sbj_code)] = $cmp_sbj_name;
			}
		}

		return $ReturnArray;
	}
	
#################### Activity Data ####################
	
	function getActivityLastInputDate()
	{
		global $eclass_db;
		$sql = "SELECT ModifiedDate FROM {$eclass_db}.ACTIVITY_STUDENT ORDER BY ModifiedDate DESC LIMIT 1";
		$row = $this->returnVector($sql);

		$date = (sizeof($row)==0) ? '-' : $row[0];
		return $date;
	}
	
	# generate Year and Semester Selections for Activity
	function getActivityYearSemester(){
		global $no_record_msg;

		list($year_arr, $semester_arr) = $this->returnActivityYearSemData();

		$year_html = (sizeof($year_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($year_arr, "name='Year'", "", "", true, 2);
		$semester_html = (sizeof($semester_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($semester_arr, "name='Semester' onChange='this.form.IsAnnual[1].checked=1'", "", "", true, 2);

		return array($year_html, $semester_html);
	}
	
	# get Year and Semester from Activity
	function returnActivityYearSemData(){
		global $intranet_db;

		$sql = "	SELECT
						DISTINCT year, year
					FROM
						{$intranet_db}.PROFILE_STUDENT_ACTIVITY
					WHERE
						year IS NOT NULL
						AND year <> ''
					ORDER BY
						year DESC
				";
		$row_year = $this->returnArray($sql);

		$sql = "	SELECT
						DISTINCT semester, semester
					FROM
						{$intranet_db}.PROFILE_STUDENT_ACTIVITY
					WHERE
						semester IS NOT NULL
						AND semester <> ''
					ORDER BY
						semester
				";
		$row_semester = $this->returnArray($sql);

		return array($row_year, $row_semester);
	}
	
	# get Year and Semester from ACTIVITY_STUDENT in Portfolio
	function returnPortfolioActivityYearSemData($Year, $Semester, $IsAnnual){
		global $eclass_db;

		# retrieve distinct years
		$sql = "	SELECT
						DISTINCT Year, Year
					FROM
						{$eclass_db}.ACTIVITY_STUDENT
					WHERE
						Year IS NOT NULL
						AND Year <> ''
					ORDER BY
						Year DESC
				";
		$row_year = $this->returnArray($sql);

		# retrieve distinct semesters
		$Year = ($Year=="") ? $row_year[0][0] : $Year;
		$sql = "	SELECT
						DISTINCT Semester, Semester
					FROM
						{$eclass_db}.ACTIVITY_STUDENT
					WHERE
						Semester IS NOT NULL
						AND Semester <> ''
						AND Year = '$Year'
					ORDER BY
						Semester
				";
		$row_semester = $this->returnArray($sql);

		# retrieve distinct classes
		if($IsAnnual!=1)
		{
			$Semester = ($Semester=="") ? $row_semester[0][0] : $Semester;
			$cond = " AND Semester = '$Semester'";
		}
		$sql = "	SELECT
						DISTINCT ClassName, ClassName
					FROM
						{$eclass_db}.ACTIVITY_STUDENT
					WHERE
						ClassName IS NOT NULL
						AND ClassName <> ''
						AND Year = '$Year'
						$cond
					ORDER BY
						ClassName
				";
		$row_class = $this->returnArray($sql);

		return array($row_year, $row_semester, $row_class);
	}
	
	# count number of activity data
	function countActivityDataNumber($my_year, $my_semester, $my_class)
	{
		global $eclass_db, $intranet_db;

		$conds = ($my_semester!="") ? " AND a.Semester = '$my_semester'" : "";
		$conds .= ($my_class!="") ? " AND a.ClassName = '$my_class'" : "";
		$sql = "SELECT
					COUNT(DISTINCT a.RecordID)
				FROM
					{$eclass_db}.ACTIVITY_STUDENT as a
					LEFT JOIN {$intranet_db}.INTRANET_USER as iu ON a.UserID = iu.UserID
					
				WHERE
					a.Year = '$my_year'
					AND (iu.WebSAMSRegNo IS NOT NULL )
					$conds
				";
		$row = $this->returnVector($sql);
		//LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER as iau ON a.UserID = iau.UserID
		//OR iau.WebSAMSRegNo IS NOT NULL

		return $row[0];
	}
	
	########## Activity Data Export ####################
	# Retrieve Student Activity Export Data
	function returnStudentActivityExportData($Year, $Semester, $ClassName)
	{
		global $eclass_db, $intranet_db;

		$namefield1 = getNameFieldByLang2("iu.");
		$namefield2 = getNameFieldByLang2("iau.");

		$ClassNumberField = getClassNumberField("act.");
		$conds = ($Semester=="") ? "" : " AND act.Semester = '$Semester'";
		$conds .= ($ClassName=="") ? "" : " AND act.ClassName = '$ClassName'";
		$sql = "SELECT
				  if(iu.WebSAMSRegNo IS NULL, iau.WebSAMSRegNo, iu.WebSAMSRegNo) as WebSAMSRegNo,
				  act.ClassName,
				  $ClassNumberField as ClassNumber,
				  if($namefield1 IS NULL, $namefield2, $namefield1) as UserName,
				  act.Year,
				  act.Semester,
				  act.ActivityName,
				  act.Role,
				  act.Performance
			FROM
				{$eclass_db}.ACTIVITY_STUDENT as act
				LEFT JOIN {$intranet_db}.INTRANET_USER AS iu ON act.UserID = iu.UserID
				LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER AS iau ON act.UserID = iau.UserID
			WHERE
				act.Year = '$Year'
				AND (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)
				$conds
			ORDER BY
				act.ClassName,
				ClassNumber,
				act.Year,
				act.Semester
			";
		$row = $this->returnArray($sql);

		return $row;
	}

	# build activity export content
	function returnActivityExportContent($Year, $Semester, $ClassName)
	{
		global $ec_iPortfolio, $no_record_msg;
		global $g_encoding_unicode;

		$delimiter = "\t";
		$valueQuote = "";

		$DataArray = $this->returnStudentActivityExportData($Year, $Semester, $ClassName);

		$Content .= $valueQuote.$ec_iPortfolio['WebSAMSRegNo'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['class'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['number'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['student'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['year'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['semester'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['activity_name'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['role'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['performance'].$valueQuote."\n";

		if(sizeof($DataArray)!=0)
		{
			for($i=0; $i<sizeof($DataArray); $i++)
			{
				$RegNo = (substr($DataArray[$i]["WebSAMSRegNo"],0,1)!="#") ? "#".$DataArray[$i]["WebSAMSRegNo"] : $DataArray[$i]["WebSAMSRegNo"];
				$Content .= $valueQuote.$RegNo.$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ClassName"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ClassNumber"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["UserName"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Year"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Semester"].$valueQuote.$delimiter;
				$Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["ActivityName"]).$valueQuote.$delimiter;
				$Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Role"]).$valueQuote.$delimiter;
				$Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Performance"]).$valueQuote."\n";
			}
		}
		else
		{
			$Content .= $valueQuote.$no_record_msg.$valueQuote."\n";
		}
		return $Content;
	}
	
	########## Update Activity Records From IP #######################
	function updateActivityFromIP($my_year, $my_semester){
		global $intranet_db, $eclass_db;

		# Retrieve merit data from eclassIP_dev.PROFILE_STUDENT_ACTIVITY
		$ActivityData = $this->retrieveActivityDataFromIP($my_year, $my_semester);

		//if(sizeof($ActivityData)==0)
		//{
		//	return 0;
		//}

		# Clear data from ec3dev_eclass.ACTIVITY_STUDENT
		$this->clearActivityData($my_year, $my_semester);

		# Insert merit data into ec3dev_eclass.ACTIVITY_STUDENT
		$fields = "(UserID, Year, Semester, ActivityName, IsAnnual, Role, Performance, Organization, Remark, ClassName, ClassNumber, ModifiedDate, InputDate)";

		$IsAnnual = 0;
		$count = 0;
		$lastID = "";
		for($i=0; $i<sizeof($ActivityData); $i++)
		{
			$temp = $ActivityData[$i];

			$values = "(".$temp['UserID'].", ";
			$values .= "'".$temp['Year']."', ";
			$values .= "'".addslashes($temp['Semester'])."', ";
			$values .= "'".addslashes($temp['ActivityName'])."', ";
			$values .= $IsAnnual.", ";
			$values .= "'".addslashes($temp['Role'])."', ";
			$values .= "'".addslashes($temp['Performance'])."', ";
			$values .= "'".addslashes($temp['Organization'])."', ";
			$values .= "'".addslashes($temp['Remark'])."', ";
			$values .= "'".addslashes($temp['ClassName'])."', ";
			$values .= "'".$temp['ClassNumber']."', ";
			$values .= "'".$temp['DateModified']."', now())";

			$sql = "INSERT INTO $eclass_db.ACTIVITY_STUDENT $fields VALUES $values";
			$this->db_db_query($sql);

			$recordID = $this->db_insert_id();
			if($recordID != "" && $lastID != $recordID)
			{
				$count++;
				$lastID = $recordID;
			}
		}
		
		// Eric Yip (20100409): Update ID of academic year and term
		$this->updateModuleYearTerm("activity", $my_year, $my_semester);
		
		return $count;
	}
	
	function clearActivityData($my_year, $my_semester)
	{
		global $eclass_db;

		$conds = ($my_semester!="") ? " AND Semester = '$my_semester'" : "";
		$sql = "DELETE FROM $eclass_db.ACTIVITY_STUDENT WHERE Year = '$my_year' $conds";
		$this->db_db_query($sql);
	}
	
	function retrieveActivityDataFromIP($my_year, $my_semester)
	{
		global $intranet_db, $eclass_db;

		$ClassNumberField = getClassNumberField("a.");
		$conds .= ($my_semester!="") ? " AND a.Semester = '$my_semester'" : "";
		$sql = "SELECT
						a.UserID,
						a.Year,
						a.Semester,
						a.ActivityName,
						a.Role,
						a.Performance,
						a.Organization,
						a.Remark,
						a.ClassName,
						$ClassNumberField as ClassNumber,
						a.DateModified
					FROM
						$intranet_db.PROFILE_STUDENT_ACTIVITY as a,
						$eclass_db.PORTFOLIO_STUDENT as b
					WHERE
			            a.UserID = b.UserID
						AND b.UserKey IS NOT NULL
						AND b.WebSAMSRegNo IS NOT NULL
						AND a.Year = '$my_year'
						$conds
					ORDER BY a.UserID ASC
							";
		$row = $this->returnArray($sql, 10);

		return $row;
	}
	
	# Get activity list of a student
	function GET_STUDENT_ACTIVITY_LIST($ParUserID){
		global $eclass_db;

		$sql =	"
							SELECT
								Year,
								ActivityName,
								if(Role IS NULL OR Role='', '--', Role) as Role,
								if(Performance IS NULL OR Performance='', '--', Performance) as Performance
							FROM
								{$eclass_db}.ACTIVITY_STUDENT
							WHERE
								UserID = '".$ParUserID."'
							ORDER BY
								Year DESC
						";
		$ReturnArr = $this->returnArray($sql);

		return $ReturnArr;
	}
	
#################### Award Data ####################
	
	function getAwardLastInputDate()
	{
		global $eclass_db;
		$sql = "SELECT ModifiedDate FROM {$eclass_db}.AWARD_STUDENT ORDER BY ModifiedDate DESC LIMIT 1";
		$row = $this->returnVector($sql);

		$date = (sizeof($row)==0) ? '-' : $row[0];
		return $date;
	}
	
	# generate Year and Semester Selections for Award
	function getAwardYearSemester(){
		global $no_record_msg;

		list($year_arr, $semester_arr) = $this->returnAwardYearSemData();

		$year_html = (sizeof($year_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($year_arr, "name='Year'", "", "", true, 2);
		$semester_html = (sizeof($semester_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($semester_arr, "name='Semester' onChange='this.form.IsAnnual[1].checked=1'", "", "", true, 2);

		return array($year_html, $semester_html);
	}
	
	# get Year and Semester from Award
	function returnAwardYearSemData(){
		global $intranet_db;

		$sql = "	SELECT
							DISTINCT year, year
					FROM
							{$intranet_db}.PROFILE_STUDENT_AWARD
					WHERE year IS NOT NULL AND year <> ''
					ORDER BY
							year DESC
				";
		$row_year = $this->returnArray($sql);

		$sql = "	SELECT
							DISTINCT semester, semester
					FROM
							{$intranet_db}.PROFILE_STUDENT_AWARD
					WHERE semester IS NOT NULL AND semester <> ''
					ORDER BY
							semester
				";
		$row_semester = $this->returnArray($sql);

		return array($row_year, $row_semester);
	}
	
	# get Year and Semester from AWARD_STUDENT in Portfolio
	function returnPortfolioAwardYearSemData($Year, $Semester, $IsAnnual){
		global $eclass_db;

		# retrieve distinct years
		$sql = "	SELECT
						DISTINCT Year, Year
					FROM
						{$eclass_db}.AWARD_STUDENT
					WHERE
						Year IS NOT NULL
						AND year <> ''
					ORDER BY
						Year DESC
				";
		$row_year = $this->returnArray($sql);

		# retrieve distinct years
		$Year = ($Year=="") ? $row_year[0][0] : $Year;
		$sql = "	SELECT
						DISTINCT Semester, Semester
					FROM
						{$eclass_db}.AWARD_STUDENT
					WHERE
						Semester IS NOT NULL
						AND Semester <> ''
						AND Year = '$Year'
					ORDER BY
						Semester
				";
		$row_semester = $this->returnArray($sql);

		# retrieve distinct classes
		if($IsAnnual!=1)
		{
			$Semester = ($Semester=="") ? $row_semester[0][0] : $Semester;
			$cond = " AND Semester = '$Semester'";
		}
		$sql = "	SELECT
						DISTINCT ClassName, ClassName
					FROM
						{$eclass_db}.AWARD_STUDENT
					WHERE
						ClassName IS NOT NULL
						AND ClassName <> ''
						AND Year = '$Year'
						$cond
					ORDER BY
						ClassName
				";
		$row_class = $this->returnArray($sql);

		return array($row_year, $row_semester, $row_class);
	}
	
	# count number of award data
	function countAwardDataNumber($my_year, $my_semester, $my_class, $status="")
	{
		global $eclass_db, $intranet_db;

		$conds = ($my_semester!="") ? " AND a.Semester = '$my_semester'" : "";
		$conds .= ($my_class!="") ? " AND a.ClassName = '$my_class'" : "";
		$conds .= ($status!="") ? " AND a.RecordStatus = '$status'" : "";
		$sql = "SELECT
					COUNT(DISTINCT a.RecordID)
				FROM
					$eclass_db.AWARD_STUDENT as a
					LEFT JOIN {$intranet_db}.INTRANET_USER as iu ON a.UserID = iu.UserID
				
				WHERE
					a.Year = '$my_year'
					AND (iu.WebSAMSRegNo IS NOT NULL)
					$conds
				";
		$row = $this->returnVector($sql);
		//	LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER as iau ON a.UserID = iau.UserID
		//OR iau.WebSAMSRegNo IS NOT NULL
		return $row[0];
	}
	
	########## Award Data Export ####################
	# Retrieve Student Award Export Data
	function returnStudentAwardExportData($Year, $Semester, $ClassName)
	{
		global $eclass_db, $intranet_db, $ec_iPortfolio;

		$namefield1 = getNameFieldByLang2("iu.");
		$namefield2 = getNameFieldByLang2("iau.");

		$ClassNumberField = getClassNumberField("aw.");
		$conds = ($Semester=="") ? "" : " AND aw.Semester = '$Semester'";
		$conds .= ($ClassName=="") ? "" : " AND aw.ClassName = '$ClassName'";
		$sql = "SELECT
					if(iu.WebSAMSRegNo IS NULL, iau.WebSAMSRegNo, iu.WebSAMSRegNo) as WebSAMSRegNo,
					aw.ClassName,
					$ClassNumberField as ClassNumber,
					if($namefield1 IS NULL, $namefield2, $namefield1) as UserName,
					aw.Year,
					aw.Semester,
					if(aw.AwardDate='0000-00-00','-',aw.AwardDate) as AwardDate,
					aw.AwardName,
					if(aw.Remark='' OR aw.Remark IS NULL, '--', aw.Remark) as Remark,
					if(aw.RecordType=1,'".$ec_iPortfolio['school_record']."','".$ec_iPortfolio['student_apply']."') as Source
				FROM
					{$eclass_db}.AWARD_STUDENT as aw
					LEFT JOIN {$intranet_db}.INTRANET_USER AS iu ON aw.UserID = iu.UserID
					LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER AS iau ON aw.UserID = iau.UserID
				WHERE
					aw.Year = '$Year'
					AND aw.RecordStatus = '2'
					AND (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)
					$conds
				ORDER BY
					aw.ClassName,
					ClassNumber,
					aw.Year,
					aw.Semester
				";
		$row = $this->returnArray($sql);

		return $row;
	}

	# build activity export content
	function returnAwardExportContent($Year, $Semester, $ClassName)
	{
		global $ec_iPortfolio, $no_record_msg, $sys_custom;
		global $g_encoding_unicode;

		$delimiter = "\t";
		$valueQuote = "";

		$DataArray = $this->returnStudentAwardExportData($Year, $Semester, $ClassName);

		$Content .= $valueQuote.$ec_iPortfolio['WebSAMSRegNo'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['class'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['number'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['student'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['year'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['semester'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['date'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['award_name'].$valueQuote.$delimiter;
		$Content .= ($sys_custom['iPortfolioHideRemark']==false) ? $valueQuote.$ec_iPortfolio['remark'].$valueQuote.$delimiter : "";
		$Content .= $valueQuote.$ec_iPortfolio['source'].$valueQuote."\n";

		if(sizeof($DataArray)!=0)
		{
			for($i=0; $i<sizeof($DataArray); $i++)
			{
				$RegNo = (substr($DataArray[$i]["WebSAMSRegNo"],0,1)!="#") ? "#".$DataArray[$i]["WebSAMSRegNo"] : $DataArray[$i]["WebSAMSRegNo"];

				$Content .= $valueQuote.$RegNo.$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ClassName"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ClassNumber"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["UserName"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Year"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Semester"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["AwardDate"].$valueQuote.$delimiter;
				$Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["AwardName"]).$valueQuote.$delimiter;
				$Content .= ($sys_custom['iPortfolioHideRemark']==false) ? $valueQuote.handleCSVExportContent($DataArray[$i]["Remark"]).$valueQuote.$delimiter : "";
				$Content .= $valueQuote.$DataArray[$i]["Source"].$valueQuote."\r\n";
			}
		}
		else
		{
			$Content .= $valueQuote.$no_record_msg.$valueQuote."\n";
		}

		return $Content."\r\n\r\n";
	}
	
	########## Update Award Records From IP #######################
	function updateAwardFromIP($my_year, $my_semester){
		global $intranet_db, $eclass_db;

		# Retrieve merit data from eclassIP_dev.PROFILE_STUDENT_AWARD
		$AwardData = $this->retrieveAwardDataFromIP($my_year, $my_semester);

		if(sizeof($AwardData)==0)
		{
			return 0;
		}

		# Clear data from ec3dev_eclass.AWARD_STUDENT
		$this->clearAwardData($my_year, $my_semester);

		# Insert merit data into ec3dev_eclass.AWARD_STUDENT
		$fields = "(UserID, Year, Semester, AwardDate, AwardName, IsAnnual, Organization, SubjectArea, Remark, RecordType, RecordStatus, ClassName, ClassNumber, ModifiedDate, InputDate)";

		$IsAnnual = 0;
		$count = 0;
		$lastID = "";
		for($i=0; $i<sizeof($AwardData); $i++)
		{
			$temp = $AwardData[$i];

			$values = "(".$temp['UserID'].", ";
			$values .= "'".$temp['Year']."', ";
			$values .= "'".addslashes($temp['Semester'])."', ";
			$values .= "'".$temp['AwardDate']."', ";
			$values .= "'".addslashes($temp['AwardName'])."', ";
			$values .= $IsAnnual.", ";
			$values .= "'".addslashes($temp['Organization'])."', ";
			$values .= "'".addslashes($temp['SubjectArea'])."', ";
			$values .= "'".addslashes($temp['Remark'])."', ";
			$values .= "'1', ";
			$values .= "'2', ";
			$values .= "'".addslashes($temp['ClassName'])."', ";
			$values .= "'".$temp['ClassNumber']."', ";
			$values .= "'".$temp['DateModified']."', now())";

			$sql = "INSERT INTO $eclass_db.AWARD_STUDENT $fields VALUES $values";
			$this->db_db_query($sql);

			$recordID = $this->db_insert_id();
			if($recordID != "" && $lastID != $recordID)
			{
				$count++;
				$lastID = $recordID;
			}
		}
		
		// Eric Yip (20100409): Update ID of academic year and term
		$this->updateModuleYearTerm("award", $my_year, $my_semester);
		
		return $count;
	}
	
	function retrieveAwardDataFromIP($my_year, $my_semester)
	{
		global $intranet_db, $eclass_db;

		$ClassNumberField = getClassNumberField("a.");
		$conds .= ($my_semester!="") ? " AND a.Semester = '$my_semester'" : "";
		$sql = "SELECT
						a.UserID,
						a.Year,
						a.Semester,
						a.AwardDate,
						a.AwardName,
						a.Organization,
						a.SubjectArea,
						a.Remark,
						a.ClassName,
						$ClassNumberField as ClassNumber,
						a.DateModified
					FROM
						$intranet_db.PROFILE_STUDENT_AWARD as a,
						$eclass_db.PORTFOLIO_STUDENT as b
					WHERE
			            a.UserID = b.UserID
						AND b.UserKey IS NOT NULL
						AND b.WebSAMSRegNo IS NOT NULL
						AND a.Year = '$my_year'
						$conds
					ORDER BY a.UserID ASC
							";
		$row = $this->returnArray($sql, 9);

		return $row;
	}
	
	function clearAwardData($my_year, $my_semester)
	{
		global $eclass_db;

		$conds .= ($my_semester!="") ? " AND Semester = '$my_semester'" : "";
		$sql = "DELETE FROM $eclass_db.AWARD_STUDENT WHERE Year = '$my_year' $conds AND RecordType <> 2";
		$this->db_db_query($sql);
	}
	
	# Get award list of a student
	function GET_STUDENT_AWARD_LIST($ParUserID)
	{
		global $eclass_db;

		$sql =	"
							SELECT
								Year,
								Semester,
								IF (AwardDate,DATE_FORMAT(AwardDate,'%Y-%m-%d'),'--') AS AwardDate,
								AwardName
							FROM
								{$eclass_db}.AWARD_STUDENT
							WHERE
								UserID = '".$ParUserID."'
							ORDER BY
								Year DESC
						";
		$ReturnArr = $this->returnArray($sql);

		return $ReturnArr;
	}
	
#################### Comment Data ####################
	
	function getCommentLastInputDate()
	{
		global $eclass_db;
		$sql = "SELECT ModifiedDate FROM {$eclass_db}.CONDUCT_STUDENT ORDER BY ModifiedDate DESC LIMIT 1";
		$row = $this->returnVector($sql);

		$date = (sizeof($row)==0) ? '-' : $row[0];
		return $date;
	}
	
	# get Year and Semester from CONDUCT_STUDENT in Portfolio
	function returnPortfolioConductYearSemData($Year, $Semester, $IsAnnual){
		global $eclass_db, $ec_iPortfolio, $intranet_db;

		# retrieve distinct years
		$sql = "	SELECT
						DISTINCT Year, Year
					FROM
						{$eclass_db}.CONDUCT_STUDENT
					WHERE
						Year IS NOT NULL
						AND Year <> ''
					ORDER BY
						Year DESC
				";
		$row_year = $this->returnArray($sql);

		# retrieve distinct semesters
		$Year = ($Year=="") ? $row_year[0][0] : $Year;
		$sql = "	SELECT
						DISTINCT Semester, Semester
					FROM
						{$eclass_db}.CONDUCT_STUDENT
					WHERE
						Semester IS NOT NULL
						AND Semester <> ''
						AND Year = '$Year'
					ORDER BY
						Semester
				";
		$row_semester = $this->returnArray($sql);

		# check whether whole year records exist in the year
		$sql = "SELECT COUNT(*) FROM {$eclass_db}.CONDUCT_STUDENT WHERE Year = '$Year' AND IsAnnual = '1'";
		$RecordExist = $this->returnVector($sql);
		if($RecordExist[0]>0)
		{
			$row_semester[] = array($ec_iPortfolio['overall_comment'], $ec_iPortfolio['overall_comment']);
		}

		if($IsAnnual!=1)
		{
			$Semester = ($Semester=="") ? $row_semester[0][0] : $Semester;
			$cond = ($Semester==$ec_iPortfolio['overall_comment']) ? " AND a.IsAnnual = '1'" : " AND a.Semester = '$Semester'";
		}
		$sql = "SELECT
					DISTINCT b.ClassName, b.ClassName
				FROM
					{$eclass_db}.CONDUCT_STUDENT as a
					LEFT JOIN {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as b ON a.UserID = b.UserID AND a.Year = b.Year
				WHERE
					b.ClassName IS NOT NULL
					AND b.ClassName <> ''
					AND a.Year = '$Year'
					$cond
				ORDER BY
					b.ClassName
				";

		$row_class = $this->returnArray($sql);

		return array($row_year, $row_semester, $row_class);
	}
	
	# count number of teacher comment data
	function countConductDataNumber($my_year, $my_semester, $my_class)
	{
		global $eclass_db, $intranet_db, $ec_iPortfolio;

		if($my_semester!="")
		{
			$conds = ($my_semester==$ec_iPortfolio['overall_comment']) ? " AND a.IsAnnual = 1" : " AND a.Semester = '$my_semester'";
		}
		$conds .= ($my_class!="") ? " AND b.ClassName = '$my_class'" : "";

		if($conds != "")
			$sql = "SELECT
						COUNT(DISTINCT a.RecordID)
					FROM
						{$eclass_db}.CONDUCT_STUDENT as a
						LEFT JOIN {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as b ON a.UserID = b.UserID AND a.Year = b.Year
						LEFT JOIN {$intranet_db}.INTRANET_USER as iu ON b.UserID = iu.UserID
						LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER as iau ON b.UserID = iau.UserID
					WHERE
						a.Year = '$my_year'
						AND b.UserID IS NOT NULL
						AND (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)
						$conds
					";
		else				
			$sql = "SELECT
						COUNT(DISTINCT a.RecordID)
					FROM
						{$eclass_db}.CONDUCT_STUDENT as a
						LEFT JOIN {$intranet_db}.INTRANET_USER as iu ON a.UserID = iu.UserID
						LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER as iau ON a.UserID = iau.UserID
					WHERE
						a.Year = '$my_year'
						AND (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)
						$conds
					";
		$row = $this->returnVector($sql);

		return $row[0];
	}
	
	function returnStudentConductExportData($Year, $Semester, $ClassName)
	{
		global $eclass_db, $intranet_db, $ec_iPortfolio;

		$namefield1 = getNameFieldByLang2("iu.");
		$namefield2 = getNameFieldByLang2("iau.");
		
		// Eric Yip (20100720): Create temporary table for class history
    $sql = "CREATE TEMPORARY TABLE tempClassHistory (UserID int(8), YearEN varchar(50), YearB5 varchar(50), ClassName varchar(50), ClassNumber varchar(50), PRIMARY KEY (UserID, YearEN)) DEFAULT CHARSET=utf8";
    $this->db_db_query($sql);
    $sql = "INSERT IGNORE INTO tempClassHistory (UserID, YearEN, YearB5, ClassName, ClassNumber) ";
    $sql .= "SELECT DISTINCT ycu.UserID, ay.YearNameEN, ay.YearNameB5, yc.ClassTitleEN, ycu.ClassNumber ";
    $sql .= "FROM {$intranet_db}.YEAR_CLASS_USER ycu ";
    $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID ";
    $sql .= "INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay ON ay.AcademicYearID = yc.AcademicYearID";
    $this->db_db_query($sql);
    $sql = "INSERT INTO tempClassHistory (UserID, YearEN, YearB5, ClassName, ClassNumber) SELECT DISTINCT UserID, Year, Year, ClassName, ClassNumber FROM {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD";
    $this->db_db_query($sql);

		if($Semester!="")
		{
			$conds = ($Semester==$ec_iPortfolio['overall_comment']) ? " AND cd.IsAnnual = 1" : " AND cd.Semester = '$Semester'";
		}
		$conds .= ($ClassName!="") ? " AND t_ch.ClassName = '$ClassName'" : "";

		if($ClassName!="")
			$sql = "SELECT DISTINCT
						if(iu.WebSAMSRegNo IS NULL, iau.WebSAMSRegNo, iu.WebSAMSRegNo) as WebSAMSRegNo,
						t_ch.ClassName,
						t_ch.ClassNumber,
						if($namefield1 IS NULL, $namefield2, $namefield1) as UserName,
						cd.Year,
						if(cd.IsAnnual=1,'".$ec_iPortfolio['overall_comment']."',cd.Semester) as Semester,
						cd.ConductGradeChar,
						cd.CommentChi,
						cd.CommentEng
					FROM
						{$eclass_db}.CONDUCT_STUDENT as cd
						LEFT JOIN tempClassHistory as t_ch ON cd.UserID = t_ch.UserID AND (cd.Year = t_ch.YearEN OR cd.Year = t_ch.YearB5)
						LEFT JOIN {$intranet_db}.INTRANET_USER AS iu ON t_ch.UserID = iu.UserID
						LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER as iau ON t_ch.UserID = iau.UserID
					WHERE
						cd.Year = '$Year'
						AND t_ch.UserID IS NOT NULL
						AND (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)
						$conds
					ORDER BY
						t_ch.ClassName,
						t_ch.ClassNumber,
						cd.Year,
						cd.Semester
				";
		else
			$sql = "SELECT DISTINCT
						if(iu.WebSAMSRegNo IS NULL, iau.WebSAMSRegNo, iu.WebSAMSRegNo) as WebSAMSRegNo,
						t_ch.ClassName,
						t_ch.ClassNumber,
						if($namefield1 IS NULL, $namefield2, $namefield1) as UserName,
						cd.Year,
						if(cd.IsAnnual=1,'".$ec_iPortfolio['overall_comment']."',cd.Semester) as Semester,
						cd.ConductGradeChar,
						cd.CommentChi,
						cd.CommentEng
					FROM
						{$eclass_db}.CONDUCT_STUDENT as cd
						LEFT JOIN tempClassHistory as t_ch ON cd.UserID = t_ch.UserID AND (cd.Year = t_ch.YearEN OR cd.Year = t_ch.YearB5)
						LEFT JOIN {$intranet_db}.INTRANET_USER AS iu ON cd.UserID = iu.UserID
						LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER as iau ON cd.UserID = iau.UserID
					WHERE
						cd.Year = '$Year'
						AND (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)
						$conds
					ORDER BY
						t_ch.ClassName,
						t_ch.ClassNumber,
						cd.Year,
						cd.Semester
				";
		$row = $this->returnArray($sql);

		return $row;
	}
	
	# build activity export content
	function returnConductExportContent($Year, $Semester, $ClassName)
	{
		global $ec_iPortfolio, $no_record_msg;
		global $g_encoding_unicode;

		$delimiter = "\t";
		$valueQuote = "";

		$DataArray = $this->returnStudentConductExportData($Year, $Semester, $ClassName);

		$Content .= $valueQuote.$ec_iPortfolio['WebSAMSRegNo'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['class'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['number'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['export']['Student'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['year'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['semester'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['conduct_grade'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['comment_chi'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['comment_eng'].$valueQuote."\r\n";

		if(sizeof($DataArray)!=0)
		{
			for($i=0; $i<sizeof($DataArray); $i++)
			{
				$RegNo = (substr($DataArray[$i]["WebSAMSRegNo"],0,1)!="#") ? "#".$DataArray[$i]["WebSAMSRegNo"] : $DataArray[$i]["WebSAMSRegNo"];
				$Content .= $valueQuote.$RegNo.$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ClassName"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ClassNumber"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["UserName"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Year"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Semester"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ConductGradeChar"].$valueQuote.$delimiter;
				$Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["CommentChi"]).$valueQuote.$delimiter;
				$Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["CommentEng"]).$valueQuote."\r\n";
			}
		}
		else
		{
			$Content .= $valueQuote.$no_record_msg.$valueQuote."\n";
		}
		return $Content;
	}
	
	# Get comment list of a student
	function GET_STUDENT_COMMENT_LIST($ParUserID)
	{
		global $eclass_db, $ec_iPortfolio, $intranet_session_language;

		$CommentField = ($intranet_session_language=="en")?"CommentEng":"CommentChi";

		$sql =	"
							SELECT DISTINCT
								Year,
								if((Semester='' OR Semester IS NULL), '".$ec_iPortfolio['overall_comment']."', Semester) AS Semester,
								ConductGradeChar,
								$CommentField as Comment
							FROM
								{$eclass_db}.CONDUCT_STUDENT
							WHERE
								UserID = '".$ParUserID."'
							ORDER BY
								Year DESC
						";
		$ReturnArr = $this->returnArray($sql);

		return $ReturnArr;
	}
	
	function GET_STUDENT_COMMENT($ParUserID,$AcademicYearID,$YearTermID='')
	{
		global $eclass_db;
		
		// for support old data do not have YearID
		$sql = "SELECT YearNameEN FROM ACADEMIC_YEAR WHERE AcademicYearID = '".$AcademicYearID."' ";
		$YearNameTemp = $this->returnVector($sql);
		$YearName = $YearNameTemp[0];
		
		$academicYear_conds = " AND ( AcademicYearID = '".$AcademicYearID."' OR Year = '".$YearName."'  )";

		if($YearTermID!=''){
			$sql = "SELECT YearTermNameEN FROM ACADEMIC_YEAR_TERM WHERE YearTermID = '".$YearTermID."' ";
			$TermNameTemp = $this->returnVector($sql);
			$TermName = $TermNameTemp[0];
			
			$yearTerm_conds = " AND ( YearTermID = '".$YearTermID."' OR Semester = '".$TermName."' )";
		}
		else{
			$yearTerm_conds = " AND ( (YearTermID = '' OR YearTermID IS NULL) AND (Semester = '' OR Semester IS NULL ) ) ";
		}
		
		$sql =	"
							SELECT 
								ConductGradeChar as Conduct,
								CommentEng as CommentEng,
								CommentChi as CommentChi,
								Year as Year,
								Semester as Semester
							FROM
								{$eclass_db}.CONDUCT_STUDENT
							WHERE
								UserID = '".$ParUserID."'
								$academicYear_conds
								$yearTerm_conds
						";
		$ReturnArr = $this->returnResultSet($sql);

		return $ReturnArr;
	}
	
#################### Attendance Data ####################
	
	function getAttendanceLastInputDate()
	{
		global $eclass_db;
		$sql = "SELECT ModifiedDate FROM {$eclass_db}.ATTENDANCE_STUDENT ORDER BY ModifiedDate DESC LIMIT 1";
		$row = $this->returnVector($sql);

		$date = (sizeof($row)==0) ? '-' : $row[0];
		return $date;
	}
	
	# generate Year and Semester Selections for Attendance
	function getAttendanceYearSemester(){
		global $no_record_msg;

		list($year_arr, $semester_arr) = $this->returnAttendanceYearSemData();

		$year_html = (sizeof($year_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($year_arr, "name='Year'", "", "", true, 2);
		$semester_html = (sizeof($semester_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($semester_arr, "name='Semester' onChange='this.form.IsAnnual[1].checked=1'", "", "",true, 2);

		return array($year_html, $semester_html);
	}
	
	# get Year and Semester from Attendance
	function returnAttendanceYearSemData(){
		global $intranet_db;

		$sql = "	SELECT
							DISTINCT year, year
					FROM
							{$intranet_db}.PROFILE_STUDENT_ATTENDANCE
					WHERE year IS NOT NULL AND year <> ''
					ORDER BY
							year DESC
				";
		$row_year = $this->returnArray($sql);

		$sql = "	SELECT
							DISTINCT semester, semester
					FROM
							{$intranet_db}.PROFILE_STUDENT_ATTENDANCE
					WHERE semester IS NOT NULL AND semester <> ''
					ORDER BY
								semester
				";
		$row_semester = $this->returnArray($sql);

		return array($row_year, $row_semester);
	}
	
	# get Year and Semester from ATTENDANCE_STUDENT in Portfolio
	function returnPortfolioAttendanceYearSemData($Year, $Semester, $IsAnnual){
		global $eclass_db;

		# retrieve distinct years
		$sql = "	SELECT
						DISTINCT Year, Year
					FROM
						{$eclass_db}.ATTENDANCE_STUDENT
					WHERE
						Year IS NOT NULL
						AND Year <> ''
					ORDER BY
						Year DESC
				";
		$row_year = $this->returnArray($sql);

		# retrieve distinct semesters
		$Year = ($Year=="") ? $row_year[0][0] : $Year;
		$sql = "	SELECT
						DISTINCT Semester, Semester
					FROM
						{$eclass_db}.ATTENDANCE_STUDENT
					WHERE
						Semester IS NOT NULL
						AND Semester <> ''
						AND Year = '$Year'
					ORDER BY
						Semester
				";
		$row_semester = $this->returnArray($sql);

		# retrieve distinct classes
		if($IsAnnual!=1)
		{
			$Semester = ($Semester=="") ? $row_semester[0][0] : $Semester;
			$cond = " AND Semester = '$Semester'";
		}
		$sql = "	SELECT
						DISTINCT ClassName, ClassName
					FROM
						{$eclass_db}.ATTENDANCE_STUDENT
					WHERE
						ClassName IS NOT NULL
						AND ClassName <> ''
						AND Year = '$Year'
						$cond
					ORDER BY
						ClassName
				";
		$row_class = $this->returnArray($sql);

		return array($row_year, $row_semester, $row_class);
	}
	
	# count number of attendance data
	function countAttendanceDataNumber($my_year, $my_semester, $my_class)
	{
		global $eclass_db, $intranet_db;

		$conds = ($my_semester!="") ? " AND a.Semester = '$my_semester'" : "";
		$conds .= ($my_class!="") ? " AND a.ClassName = '$my_class'" : "";
		$sql = "SELECT
					COUNT(DISTINCT a.RecordID)
				FROM
					{$eclass_db}.ATTENDANCE_STUDENT as a
					LEFT JOIN {$intranet_db}.INTRANET_USER as iu ON a.UserID = iu.UserID

				WHERE
					a.Year = '$my_year'
					AND (iu.WebSAMSRegNo IS NOT NULL)
					$conds
				";
		$row = $this->returnVector($sql);
		//					LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER as iau ON a.UserID = iau.UserID
		//  OR iau.WebSAMSRegNo IS NOT NULL
		return $row[0];
	}
	
	# Retrieve Student Attendance Summary
	function returnStudentAttendanceSummary($studentid, $Year="", $Semester="")
	{
		global $eclass_db, $ec_iPortfolio, $intranet_root;
	
		include_once($intranet_root."/includes/libpf-sem-map.php");
		include("$intranet_root/includes/libstudentprofile.php");
		$lstudentprofile = new libstudentprofile();
	
		$conds = $this->getReportYearCondition($Year);
	
		# handle semester different problem
		//list($SemesterArr, $ShortSemesterArr) = $this->getSemesterNameArrFromIP();
		//list($SemList, $List1, $List2, $List3) = $this->getSemesterMatchingLists($Semester, $SemesterArr);
		if($Semester!="")
		{
			//$conds .= "AND (Semester = '$Semester' OR Semester IN ({$SemList}))";
			$conds .= "AND (ats.Semester = '$Semester' OR ayt.YearTermNameEN = '$Semester' OR ayt.YearTermNameB5 = '$Semester')";
		}
	
		# records grouping by year and semester
		/*
			$sql = "SELECT
			Year,
			if(INSTR('{$List1}', Semester),'".$SemesterArr[0]."',if(INSTR('{$List2}',  Semester),'".$SemesterArr[1]."',if(INSTR('{$List3}',  Semester),'".$SemesterArr[2]."', Semester))),
			RecordType,
			DayType
			FROM
			{$eclass_db}.ATTENDANCE_STUDENT
			WHERE
			UserID = '$studentid'
			$conds
			ORDER BY
			Year DESC, Semester ASC, RecordType, DayType DESC
			";
			*/
		$sql = "SELECT
					ats.Year,
					ayt.YearTermNameEN,
					ats.RecordType,
					ats.DayType
				FROM
					{$eclass_db}.ATTENDANCE_STUDENT AS ats
				LEFT JOIN $intranet_db.ACADEMIC_YEAR_TERM AS ayt
					ON ats.YearTermID = ayt.YearTermID
				LEFT JOIN $intranet_db.ACADEMIC_YEAR AS ay
					ON ats.AcademicYearID = ay.AcademicYearID
				WHERE
					ats.UserID = '$studentid'
					$conds
				ORDER BY
					ats.Year DESC,
					ats.Semester DESC,
					ats.RecordType DESC
				";
		$row = $this->returnArray($sql);
	
		for($i=0; $i<sizeof($row); $i++)
		{
			list($year, $semester, $type, $day_type) = $row[$i];
			//$semester = SemesterTransform($semester);
			$add_value = ($type==1 && $day_type>1) ? 0.5 : 1;
			$add_value = $lstudentprofile->attendance_count_method ? $add_value : 1;
	
			$result[$year][$semester][$type] = $result[$year][$semester][$type] + $add_value;
	
			# get semester total
			$result[$year][$ec_iPortfolio['whole_year']][$type] = $result[$year][$ec_iPortfolio['whole_year']][$type] + $add_value;
		}
		return $result;
	}
// 	function returnStudentAttendanceSummary($studentid, $Year="", $Semester="")
// 	{
// 		global $eclass_db, $ec_iPortfolio, $intranet_root;

// 		include("$intranet_root/includes/libstudentprofile.php");
//         $lstudentprofile = new libstudentprofile();

// 		$conds = $this->getReportYearCondition($Year);

// 		# handle semester different problem
// 		list($SemesterArr, $ShortSemesterArr) = $this->getSemesterNameArrFromIP();
// 		list($SemList, $List1, $List2, $List3) = $this->getSemesterMatchingLists($Semester, $SemesterArr);
// 		if($Semester!="")
// 		{
// 			$conds .= "AND (Semester = '$Semester' OR Semester IN ({$SemList}))";
// 		}

// 		# records grouping by year and semester
// 		$sql = "SELECT
// 					Year,
// 					if(INSTR('{$List1}', Semester),'".$SemesterArr[0]."',if(INSTR('{$List2}',  Semester),'".$SemesterArr[1]."',if(INSTR('{$List3}',  Semester),'".$SemesterArr[2]."', Semester))),
// 					RecordType,
// 					DayType
// 				FROM
// 					{$eclass_db}.ATTENDANCE_STUDENT
// 				WHERE
// 					UserID = '$studentid'
// 					$conds
// 				ORDER BY
// 					Year DESC, Semester ASC, RecordType, DayType DESC
// 				";
// 		$row = $this->returnArray($sql);

// 		for($i=0; $i<sizeof($row); $i++)
// 		{
// 			list($year, $semester, $type, $day_type) = $row[$i];
// 			$add_value = ($type==1 && $day_type>1) ? 0.5 : 1;
// 			$add_value = $lstudentprofile->attendance_count_method ? $add_value : 1;

// 			$result[$year][$semester][$type] = $result[$year][$semester][$type] + $add_value;
			
// 			# get semester total
// 			# ensure semester name is not whole year to prevent double counting
// 			if($semester != $ec_iPortfolio['whole_year']){
// 				$result[$year][$ec_iPortfolio['whole_year']][$type] = $result[$year][$ec_iPortfolio['whole_year']][$type] + $add_value;
// 			}
// 		}
// 		return $result;
// 	}
	
	# Display Student Attendance Summary
	function displayStudentAttendanceSummary($StudentID, $ClassName, $PassChooseYear="", $ParChangeLayer=false)
    {
		global $intranet_db, $ec_iPortfolio, $i_Profile_Absent, $i_Profile_Late, $i_Profile_EarlyLeave, $no_record_msg;

		# get the attendance summary
		$result = $this->returnStudentAttendanceSummary($StudentID, $PassChooseYear);

		# get Years and ClassName according to ASSESSMENT_STUDENT_SUBJECT_RECORD
		$YearClassArr = $this->returnFilteredYearClass($StudentID, 1,$PassChooseYear);
/*
		# retrieve school semesters
		list($SemesterArr, $ShortSemesterArr) = $this->getSemesterNameArrFromIP();
		$SemesterArr[] = $ec_iPortfolio['whole_year'];
*/
		//Table Header
		$x = "<table width='100%' border='0' cellpadding='4' cellspacing='0' bgcolor=#CCCCCC>\n";
		$x .= "<tr class='tabletop' height=35>\n";
		//$x .= "<td height='35' width='5%'>&nbsp;</td>\n";
		/*
		$x .= "<td nowrap class='tabletopnolink' width='46%'><b>".$ec_iPortfolio['year']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' width='15%'><b>".$ec_iPortfolio['class']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='15%'><b>".$ec_iPortfolio['semester']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='8%'><b>".$i_Profile_Absent."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='8%'><b>".$i_Profile_Late."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='8%'><b>".$i_Profile_EarlyLeave."</b></td>";
		$x .= "</tr>\n";
		*/

		$x .= "<td nowrap class='tabletopnolink' width='20%'><b>".$ec_iPortfolio['year']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' width='10%'><b>".$ec_iPortfolio['class']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='20%'><b>".$ec_iPortfolio['semester']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='12%'><b>".$i_Profile_Absent."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='12%'><b>".$i_Profile_Late."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='12%'><b>".$i_Profile_EarlyLeave."</b></td>";
		$x .= "</tr>\n";
		
		//Table Content
		if(!empty($YearClassArr))
		{
			$num = 0;
			foreach($YearClassArr as $year => $ClassName)
			{
				$CurrYear = "";
				
				# Eric Yip (20100211): Get semester array according to year name
				# (Not work for years with same name)
				unset($SemesterArr);
				$sql = "SELECT AcademicYearID FROM {$intranet_db}.ACADEMIC_YEAR WHERE YearNameEN = '".$year."'";
				$ay_id = current($this->returnVector($sql));
				if($ay_id== ''){
					//for case , with year in the table:PROFILE_CLASS_HISTORY but without year id in t: ACADEMIC_YEAR
				}else{
					//Year exist in the ACADEMIC_YEAR , get the related semesters
						
					$SemesterArr = array_values(getSemesters($ay_id, 1, 'en'));
					// another set of sem arr for display only
					$SemesterDisplayArr = array_values(getSemesters($ay_id));
				}
				
				$SemesterArr[] = $ec_iPortfolio['whole_year'];
				$SemesterDisplayArr[] = $ec_iPortfolio['whole_year'];
// 				$SemesterArr = array_values(getSemesters($ay_id));
// 				$SemesterArr[] = $ec_iPortfolio['whole_year'];
				
				for($j=0; $j<sizeof($SemesterArr); $j++)
				{
					$sem = trim($SemesterArr[$j]);
					$sem_disp = trim($SemesterDisplayArr[$j]);
					if($year!=$CurrYear)
					{
						$bclass = ($num%2!=0) ? "class=tablerow2" : "class=tablerow1";
						$x .= "<tr $bclass>\n";
						//$x .= "<td>&nbsp;</td>\n";
						$x .= "<td class=tabletext>$year</td>\n";
						$x .= "<td class=tabletext>$ClassName</td>\n";
						$num++;
						$CurrYear = $year;
					}
					else
					{
						$x .= "<tr $bclass>\n";
						//$x .= "<td>&nbsp;</td>\n";
						$x .= "<td class=tabletext >&nbsp;</td>\n";
						$x .= "<td class=tabletext>&nbsp;</td>\n";
					}
					//$stylecolor = ($num%2==0) ? "#FFFFFF" : "#DDDDDD";
					//$style = ($sem!=$ec_iPortfolio['whole_year']) ? "style='border-bottom: 1px dashed $stylecolor;'" : "";
					//$x .= "<td align='center' class='tabletext tablerow_underline'><a href=\"javascript:newWindow('details.php?StudentID=$StudentID&ClassName=$ClassName&semester=$sem&year=$year', 19) \" class='navigation'>$sem</a></td>\n";
					if($ParChangeLayer)
						$x .= "<td align='center' class='tabletext tablerow_underline'><a href='#' onClick='jVIEW_DETAIL(\"attendance\", \"semester=$sem_disp&year=$year\")' class='navigation'>$sem_disp</a></td>\n";
					else{
					//	$x .= "<td align='center' class='tabletext tablerow_underline'><a href='details.php?StudentID=$StudentID&ClassName=$ClassName&semester=$sem&year=$year' class='tablelink'>$sem</a></td>\n";

					//since the details.php is a dead link, hide for this for temp, should be open later 20110726
					$x .= "<td align='center' class='tabletext tablerow_underline'>$sem_disp</td>\n";
					}
					
					for($type=1; $type<=3; $type++)
					{
						$value = ($result[$year][$sem][$type]!='') ? $result[$year][$sem][$type] : 0;
						$x .= "<td align='center' class='tabletext tablerow_underline'>".$value."</td>\n";

						# get overall total
						# ensure Whole Year is the last array element before adding the value to total count
						if($sem==$ec_iPortfolio['whole_year'] && $j==(sizeof($SemesterArr) - 1))
							$result[$ec_iPortfolio['total']][$type] = $result[$ec_iPortfolio['total']][$type] + $value;

					}
					$x .= "</tr>\n";
				}
			}
			$bclass = ($num%2!=0) ? "class=tablerow2" : "class=tablerow1";
			$x .= "<tr $bclass>\n";
			//$x .= "<td>&nbsp;</td>\n";
			$x .= "<td class=tabletext>&nbsp;</td>\n";
			$x .= "<td class=tabletext>&nbsp;</td>\n";
			$x .= "<td align='center' class=tabletext>[".$ec_iPortfolio['total']."]</td>\n";
			for($type=1; $type<=3; $type++)
			{
				$x .= "<td align='center' class=tabletext>".($result[$ec_iPortfolio['total']][$type]!='' ? $result[$ec_iPortfolio['total']][$type] : 0)."</td>\n";
			}
			$x .= "</tr>\n";
		}
		else
		{
			$x .= "<tr valign='middle'><td height='100' colspan='7' align='center' $bclass>".$no_record_msg."</td></tr>";
		}
        $x .= "</table>\n";

		return $x;
	}
	
	########## Update Attendance Records From IP #######################
	function updateAttendanceFromIP($my_year, $my_semester){
		global $intranet_db, $eclass_db;

		# Retrieve merit data from eclassIP_dev.PROFILE_STUDENT_ATTENDANCE
		$AttendanceData = $this->retrieveAttendanceDataFromIP($my_year, $my_semester);

		if(sizeof($AttendanceData)==0)
		{
			return 0;
		}

		# Clear data from ec3dev_eclass.ATTENDANCE_STUDENT
		$this->clearAttendanceData($my_year, $my_semester);

		# Insert merit data into ec3dev_eclass.ATTENDANCE_STUDENT
		$fields = "(UserID, Year, Semester, AttendanceDate, IsAnnual, Periods, DayType, Reason, Remark, RecordType, RecordStatus, ClassName, ClassNumber, ModifiedDate, InputDate)";

		$IsAnnual = 0;
		$count = 0;
		$lastID = "";

		for($j=0; $j<sizeof($AttendanceData); $j++)
		{
			$temp = $AttendanceData[$j];

			$values = "(".$temp['UserID'].", ";
			$values .= "'".$temp['Year']."', ";
			$values .= "'".addslashes($temp['Semester'])."', ";
			$values .= "'".$temp['AttendanceDate']."', ";
			$values .= $IsAnnual.", ";
			$values .= "'".$temp['Periods']."', ";
			$values .= "'".$temp['DayType']."', ";
			$values .= "'".addslashes($temp['Reason'])."', ";
			$values .= "'".addslashes($temp['Remark'])."', ";
			$values .= "'".$temp['RecordType']."', ";
			$values .= ($temp['RecordStatus']!="") ? "'".$temp['RecordStatus']."', " : "NULL,";
			$values .= "'".addslashes($temp['ClassName'])."', ";
			$values .= "'".$temp['ClassNumber']."', ";
			$values .= "'".$temp['DateModified']."', now())";

			$sql = "INSERT INTO $eclass_db.ATTENDANCE_STUDENT $fields VALUES $values";

			$this->db_db_query($sql);

			$recordID = $this->db_insert_id();
			if($recordID != "" && $lastID != $recordID)
			{
				$count++;
				$lastID = $recordID;
			}
		}
		
		// Eric Yip (20100409): Update ID of academic year and term
		$this->updateModuleYearTerm("attendance", $my_year, $my_semester);
		
		return $count;
	}
	
	function retrieveAttendanceDataFromIP($my_year, $my_semester)
	{
		global $intranet_db, $eclass_db;

		$ClassNumberField = getClassNumberField("a.");
		$conds = ($my_semester!="") ? " AND a.Semester = '$my_semester'" : "";
		$sql = "SELECT
						a.UserID,
						a.Year,
						a.Semester,
						a.AttendanceDate,
						a.Periods,
						a.DayType,
						a.Reason,
						a.Remark,
						a.RecordType,
						a.RecordStatus,
						a.ClassName,
						$ClassNumberField as ClassNumber,
						a.DateModified
					FROM
						$intranet_db.PROFILE_STUDENT_ATTENDANCE as a,
						$eclass_db.PORTFOLIO_STUDENT as b
					WHERE
			            a.UserID = b.UserID
						AND b.UserKey IS NOT NULL
						AND b.WebSAMSRegNo IS NOT NULL
						AND a.Year = '$my_year'
						$conds
					ORDER BY a.UserID ASC
							";
		$row = $this->returnArray($sql, 13);

		return $row;
	}
	
	function clearAttendanceData($my_year, $my_semester)
	{
		global $eclass_db;

		$conds = ($my_semester!="") ? " AND Semester = '$my_semester'" : "";
		$sql = "DELETE FROM $eclass_db.ATTENDANCE_STUDENT WHERE Year = '$my_year' $conds";
		$this->db_db_query($sql);
	}
	
	########## Attendance Data Export ####################
	# Retrieve Attendance Export Data
	function returnStudentAttendanceExportData($Year, $Semester, $ClassName)
	{
		global $eclass_db, $intranet_db;
		global $i_Profile_Absent, $i_Profile_Late, $i_Profile_EarlyLeave;
		global $i_DayTypeWholeDay, $i_DayTypeAM, $i_DayTypePM;

		$namefield1 = getNameFieldByLang2("iu.");
		$namefield2 = getNameFieldByLang2("iau.");

		$ClassNumberField = getClassNumberField("att.");
		$conds = ($Semester=="") ? "" : " AND att.Semester = '$Semester'";
		$conds .= ($ClassName=="") ? "" : " AND att.ClassName = '$ClassName'";
		$sql = "SELECT
					if(iu.WebSAMSRegNo IS NULL, iau.WebSAMSRegNo, iu.WebSAMSRegNo) as WebSAMSRegNo,
					att.ClassName,
					$ClassNumberField as ClassNumber,
					if($namefield1 IS NULL, $namefield2, $namefield1) as UserName,
					att.Year,
					att.Semester,
					att.AttendanceDate,
					CASE att.RecordType
						WHEN 1 THEN '$i_Profile_Absent'
						WHEN 2 THEN '$i_Profile_Late'
						WHEN 3 THEN '$i_Profile_EarlyLeave'
					ELSE '-' END as RecordType,
					CASE att.DayType
						WHEN 1 THEN '$i_DayTypeWholeDay'
						WHEN 2 THEN '$i_DayTypeAM'
						WHEN 3 THEN '$i_DayTypePM'
					ELSE '-' END as DayType,
					att.Reason
				FROM
					{$eclass_db}.ATTENDANCE_STUDENT as att
					LEFT JOIN {$intranet_db}.INTRANET_USER AS iu ON att.UserID = iu.UserID
					LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER AS iau ON att.UserID = iau.UserID
				WHERE
					att.Year = '$Year'
					AND (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)
					$conds
				ORDER BY
					att.ClassName,
					ClassNumber,
					att.Year,
					att.Semester
			";
		$row = $this->returnArray($sql);

		return $row;
	}

	# build activity export content
	function returnAttendanceExportContent($Year, $Semester, $ClassName)
	{
		global $ec_iPortfolio, $no_record_msg;
		global $g_encoding_unicode;

		$delimiter = "\t";
		$valueQuote = "";

		$DataArray = $this->returnStudentAttendanceExportData($Year, $Semester, $ClassName);

		$Content .= $valueQuote.$ec_iPortfolio['WebSAMSRegNo'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['class'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['number'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['student'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['year'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['semester'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['date'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['stype'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['period'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['reason'].$valueQuote."\n";

		if(sizeof($DataArray)!=0)
		{
			for($i=0; $i<sizeof($DataArray); $i++)
			{
				$RegNo = (substr($DataArray[$i]["WebSAMSRegNo"],0,1)!="#") ? "#".$DataArray[$i]["WebSAMSRegNo"] : $DataArray[$i]["WebSAMSRegNo"];
				$Content .= $valueQuote.$RegNo.$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ClassName"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ClassNumber"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["UserName"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Year"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Semester"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["AttendanceDate"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["RecordType"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["DayType"].$valueQuote.$delimiter;
				$Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Reason"]).$valueQuote."\n";
			}
		}
		else
		{
			$Content .= $valueQuote.$no_record_msg."\"\n";
		}
		return $Content;
	}
	
#################### Service Data ####################
	
	function getServiceLastInputDate()
	{
		global $eclass_db;
		$sql = "SELECT ModifiedDate FROM {$eclass_db}.SERVICE_STUDENT ORDER BY ModifiedDate DESC LIMIT 1";
		$row = $this->returnVector($sql);

		$date = (sizeof($row)==0) ? '-' : $row[0];
		return $date;
	}
	
	# generate Year and Semester Selections for Service
	function getServiceYearSemester(){
		global $no_record_msg;

		list($year_arr, $semester_arr) = $this->returnServiceYearSemData();

		$year_html = (sizeof($year_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($year_arr, "name='Year'", "", "", true, 2);
		$semester_html = (sizeof($semester_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($semester_arr, "name='Semester' onChange='this.form.IsAnnual[1].checked=1'", "", "", true, 2);

		return array($year_html, $semester_html);
	}
	
	# get Year and Semester from Service
	function returnServiceYearSemData(){
		global $intranet_db;

		$sql = "	SELECT
						DISTINCT year, year
					FROM
						{$intranet_db}.PROFILE_STUDENT_SERVICE
					WHERE
						year IS NOT NULL
						AND year <> ''
					ORDER BY
						year DESC
				";
		$row_year = $this->returnArray($sql);

		$sql = "	SELECT
						DISTINCT semester, semester
					FROM
						{$intranet_db}.PROFILE_STUDENT_SERVICE
					WHERE
						semester IS NOT NULL
						AND semester <> ''
					ORDER BY
						semester
				";
		$row_semester = $this->returnArray($sql);

		return array($row_year, $row_semester);
	}
	
  # get Year and Semester from SERVICE_STUDENT in Portfolio
	function returnPortfolioServiceYearSemData($Year, $Semester, $IsAnnual){
		global $eclass_db;

		# retrieve distinct years
		$sql = "	SELECT
						DISTINCT Year, Year
					FROM
						{$eclass_db}.SERVICE_STUDENT
					WHERE
						Year IS NOT NULL
						AND Year <> ''
					ORDER BY
						Year DESC
				";
		$row_year = $this->returnArray($sql);

		# retrieve distinct semesters
		$Year = ($Year=="") ? $row_year[0][0] : $Year;
		$sql = "	SELECT
						DISTINCT Semester, Semester
					FROM
						{$eclass_db}.SERVICE_STUDENT
					WHERE
						Semester IS NOT NULL
						AND Semester <> ''
						AND Year = '$Year'
					ORDER BY
						Semester
				";
		$row_semester = $this->returnArray($sql);

		# retrieve distinct classes
		if($IsAnnual!=1)
		{
			$Semester = ($Semester=="") ? $row_semester[0][0] : $Semester;
			$cond = " AND Semester = '$Semester'";
		}
		$sql = "	SELECT
						DISTINCT ClassName, ClassName
					FROM
						{$eclass_db}.SERVICE_STUDENT
					WHERE
						ClassName IS NOT NULL
						AND ClassName <> ''
						AND Year = '$Year'
						$cond
					ORDER BY
						ClassName
				";
		$row_class = $this->returnArray($sql);

		return array($row_year, $row_semester, $row_class);
	}
	
	# count number of service data
	function countServiceDataNumber($my_year, $my_semester, $my_class)
	{
		global $eclass_db, $intranet_db;

		$conds = ($my_semester!="") ? " AND a.Semester = '$my_semester'" : "";
		$conds .= ($my_class!="") ? " AND a.ClassName = '$my_class'" : "";
		$sql = "SELECT
					COUNT(DISTINCT a.RecordID)
				FROM
					{$eclass_db}.SERVICE_STUDENT as a
					LEFT JOIN {$intranet_db}.INTRANET_USER as iu ON a.UserID = iu.UserID
				
				WHERE
					a.Year = '$my_year'
					AND (iu.WebSAMSRegNo IS NOT NULL)
					$conds
				";
		$row = $this->returnVector($sql);
		//	LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER as iau ON a.UserID = iau.UserID
		//  OR iau.WebSAMSRegNo IS NOT NULL
		return $row[0];
	}
	
	########## Update Service Records From IP ######################
	function updateServiceFromIP($my_year, $my_semester){
		global $intranet_db, $eclass_db;

		# Retrieve merit data from eclassIP_dev.PROFILE_STUDENT_ACTIVITY
		$ServiceData = $this->retrieveServiceDataFromIP($my_year, $my_semester);

		if(sizeof($ServiceData)==0)
		{
			return 0;
		}

		# Clear data from ec3dev_eclass.ACTIVITY_STUDENT
		$this->clearServiceData($my_year, $my_semester);

		# Insert merit data into ec3dev_eclass.ACTIVITY_STUDENT
		$fields = "(UserID, Year, Semester, ServiceDate, ServiceName, Role, Performance, Organization, Remark, ClassName, ClassNumber, ModifiedDate, InputDate)";

		$IsAnnual = 0;
		$count = 0;
		$lastID = "";
		for($i=0; $i<sizeof($ServiceData); $i++)
		{
			$temp = $ServiceData[$i];

			$values = "(".$temp['UserID'].", ";
			$values .= "'".$temp['Year']."', ";
			$values .= "'".addslashes($temp['Semester'])."', ";
			$values .= "'".$temp['ServiceDate']."', ";
			$values .= "'".addslashes($temp['ServiceName'])."', ";
			$values .= "'".addslashes($temp['Role'])."', ";
			$values .= "'".addslashes($temp['Performance'])."', ";
			$values .= "'".addslashes($temp['Organization'])."', ";
			$values .= "'".addslashes($temp['Remark'])."', ";
			$values .= "'".addslashes($temp['ClassName'])."', ";
			$values .= "'".$temp['ClassNumber']."', ";
			$values .= "'".$temp['DateModified']."', now())";

			$sql = "INSERT INTO $eclass_db.SERVICE_STUDENT $fields VALUES $values";
			$this->db_db_query($sql);

			$recordID = $this->db_insert_id();
			if($recordID != "" && $lastID != $recordID)
			{
				$count++;
				$lastID = $recordID;
			}
		}
		
		// Eric Yip (20100409): Update ID of academic year and term
		$this->updateModuleYearTerm("service", $my_year, $my_semester);
		
		return $count;
	}
	
	function retrieveServiceDataFromIP($my_year, $my_semester)
	{
		global $intranet_db, $eclass_db;

		$ClassNumberField = getClassNumberField("a.");
		$conds .= ($my_semester!="") ? " AND a.Semester = '$my_semester'" : "";
		$sql = "SELECT
						a.UserID,
						a.Year,
						a.Semester,
						a.ServiceDate,
						a.ServiceName,
						a.Role,
						a.Performance,
						a.Organization,
						a.Remark,
						a.ClassName,
						$ClassNumberField as ClassNumber,
						a.DateModified
					FROM
						$intranet_db.PROFILE_STUDENT_SERVICE as a
						LEFT JOIN $eclass_db.PORTFOLIO_STUDENT as b ON a.UserID = b.UserID
					WHERE
						b.UserKey IS NOT NULL
						AND b.WebSAMSRegNo IS NOT NULL
						AND a.Year = '$my_year'
						$conds
					ORDER BY a.UserID ASC
							";
		$row = $this->returnArray($sql, 11);

		return $row;
	}
	
	function clearServiceData($my_year, $my_semester)
	{
		global $eclass_db;

		$conds = ($my_semester!="") ? " AND Semester = '$my_semester'" : "";
		$sql = "DELETE FROM $eclass_db.SERVICE_STUDENT WHERE Year = '$my_year' $conds";
		$this->db_db_query($sql);
	}
	
	########## Service Data Export ####################
	# Retrieve Student Service Export Data
	function returnStudentServiceExportData($Year, $Semester, $ClassName)
	{
		global $eclass_db, $intranet_db;

		$namefield1 = getNameFieldByLang2("iu.");
		$namefield2 = getNameFieldByLang2("iau.");

		$ClassNumberField = getClassNumberField("sv.");
		$conds = ($Semester=="") ? "" : " AND sv.Semester = '$Semester'";
		$conds .= ($ClassName=="") ? "" : " AND sv.ClassName = '$ClassName'";
		$sql = "SELECT
				  if(iu.WebSAMSRegNo IS NULL, iau.WebSAMSRegNo, iu.WebSAMSRegNo) as WebSAMSRegNo,
				  sv.ClassName,
				  $ClassNumberField as ClassNumber,
				  if($namefield1 IS NULL, $namefield2, $namefield1) as UserName,
				  sv.Year,
				  sv.Semester,
				  if(sv.ServiceDate='0000-00-00','',sv.ServiceDate) as ServiceDate,
				  sv.ServiceName,
				  sv.Role,
				  sv.Performance
			FROM
				{$eclass_db}.SERVICE_STUDENT as sv
				LEFT JOIN {$intranet_db}.INTRANET_USER as iu ON sv.UserID = iu.UserID
				LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER as iau ON sv.UserID = iau.UserID
			WHERE
				sv.Year = '$Year'
				AND (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)
				$conds
			ORDER BY
				sv.ClassName,
				ClassNumber,
				sv.Year,
				sv.Semester
			";
		$row = $this->returnArray($sql);

		return $row;
	}

	# build service export content
	function returnServiceExportContent($Year, $Semester, $ClassName)
	{
		global $ec_iPortfolio, $no_record_msg;
		global $g_encoding_unicode;

/*
		$delimiter = ($g_encoding_unicode) ? "\t" : ",";
		$valueQuote = ($g_encoding_unicode) ? "" : "\"";
*/

		$delimiter = "\t";
		$valueQuote = "";

		$DataArray = $this->returnStudentServiceExportData($Year, $Semester, $ClassName);

		$Content .= $valueQuote.$ec_iPortfolio['WebSAMSRegNo'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['class'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['number'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['student'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['year'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['semester'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['date'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['service_name'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['role'].$valueQuote.$delimiter;
		$Content .= $valueQuote.$ec_iPortfolio['performance']."\n";

		if(sizeof($DataArray)!=0)
		{
			for($i=0; $i<sizeof($DataArray); $i++)
			{
				$RegNo = (substr($DataArray[$i]["WebSAMSRegNo"],0,1)!="#") ? "#".$DataArray[$i]["WebSAMSRegNo"] : $DataArray[$i]["WebSAMSRegNo"];
				$Content .= $valueQuote.$RegNo.$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ClassName"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ClassNumber"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["UserName"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Year"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["Semester"].$valueQuote.$delimiter;
				$Content .= $valueQuote.$DataArray[$i]["ServiceDate"].$valueQuote.$delimiter;
				$Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["ServiceName"]).$valueQuote.$delimiter;
				$Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Role"]).$valueQuote.$delimiter;
				$Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Performance"])."\n";
			}
		}
		else
		{
			$Content .= $valueQuote.$no_record_msg.$valueQuote."\n";
		}
		return $Content;
	}
	
#################### OLE Data ####################
	
	function getOLELastInputDate()
	{
		global $eclass_db;
		$sql = "SELECT ModifiedDate FROM {$eclass_db}.OLE_STUDENT ORDER BY ModifiedDate DESC LIMIT 1";
		$row = $this->returnVector($sql);
		
		$date = (sizeof($row)==0) ? '-' : $row[0];
		return $date;
	}
	
#################### Student Data ####################
	
	function getStudentInfoLastInputDate()
	{
		global $eclass_db;
		$sql = "SELECT ModifiedDate FROM {$eclass_db}.PORTFOLIO_STUDENT ORDER BY ModifiedDate DESC LIMIT 1";
		$row = $this->returnVector($sql);

		$date = (sizeof($row)==0) ? '-' : $row[0];
		return $date;
	}
	
#################### Guardian Data ####################
	
	function getGuardianLastInputDate()
	{
		global $eclass_db;
		$sql = "SELECT ModifiedDate FROM {$eclass_db}.GUARDIAN_STUDENT ORDER BY ModifiedDate DESC LIMIT 1";
		$row = $this->returnVector($sql);

		$date = (sizeof($row)==0) ? '-' : $row[0];
		return $date;
	}

#################################################################
#########################      OLE      #########################
#################################################################
	
	function INSERT_STUDENT_RECORD()
	{
		global $eclass_db;
		
		$Sql = 	"
						SELECT
							Title, StartDate, EndDate, Category, Organization, Details, Remark, ELE, IntExt
						FROM
							{$eclass_db}.OLE_STUDENT
						WHERE
							(ProgramID IS NULL OR ProgramID = 0)
						GROUP BY
							Title, StartDate, EndDate
					";
		$ReturnArr1 = $this->returnArray($Sql);
		
		for ($i=0;$i<count($ReturnArr1);$i++)
		{
			$TmpProgramID = $this->RETURN_PROGRAM_ID($ReturnArr1[$i]["Title"], $ReturnArr1[$i]["StartDate"], $ReturnArr1[$i]["EndDate"], true, $ReturnArr1[$i]);
			
			$Sql2 = 	"
							UPDATE
								{$eclass_db}.OLE_STUDENT
							SET
								ProgramID = '{$TmpProgramID}'
							WHERE
								(ProgramID IS NULL OR ProgramID = 0)
								AND Title = '".addslashes($ReturnArr1[$i]["Title"])."'
								AND StartDate = '".$ReturnArr1[$i]["StartDate"]."'
								AND EndDate = '".$ReturnArr1[$i]["EndDate"]."'
						";			
			$this->db_db_query($Sql2);			
		}		
	}
	
	function GET_OLE_CATEGORY_SELECTION($tag, $Category, $all="", $ParShowAllCat=false)
	{
		global $ec_iPortfolio, $range_all;

		$CategoryArray = $this->GET_OLR_CATEGORY(0, $ParShowAllCat);

		$CategorySelection = "<SELECT $tag>";
		$CategorySelection .= ($all==1) ? "<OPTION value=''>".$ec_iPortfolio['all_category']."</OPTION>" : "";
		if(!empty($CategoryArray))
		{
			foreach($CategoryArray as $id => $title)
			{
				$CategorySelection .= "<OPTION value='".$id."' ".($Category==$id?'SELECTED':"").">".$title."</OPTION>";
			}
		}
		$CategorySelection .= "</SELECT>";

		return $CategorySelection;
	}
	
	/**
	 * Get the selection box for ole subcategory
	 * @param String $ParTag	- the html that give attribute to the selection box
	 * @param String $ParCAT	- the subcategory id that is default selected
	 * @param boolean $ParAll 	- show the 1st selection as all
	 * @return Sting 			- the html of the selection box generated
	 */
	function GET_SUBCATEGORY_SELECTION($ParTag, $ParSubCat, $ParAll="", $ParCatID) {
		global $range_all, $ec_iPortfolio;

		$DefaultELEArray = $this->GET_OLE_SUBCATEGORY($ParCatID);

		$ReturnList = "<SELECT $ParTag>";
		$ReturnList .= ($ParAll==1) ? "<OPTION value=''>".$ec_iPortfolio['all_subcategory']."</OPTION>" : "";
		if(!empty($DefaultELEArray))
		{
			foreach($DefaultELEArray as $SubCAT_ID => $CAT_NAME)
			{
				$ReturnList .= "<OPTION value='".$SubCAT_ID."' ".($ParSubCat==$SubCAT_ID?'SELECTED':"").">".$CAT_NAME."</OPTION>";
			}
		}
		$ReturnList .= "</SELECT>";

		return $ReturnList;
	}
	
	function GET_ELE_SELECTION($ParTag, $ParELE, $ParAll="")
	{
		global $range_all, $ec_iPortfolio;

		$DefaultELEArray = $this->GET_ELE();
		

		$ReturnList = "<SELECT $ParTag>";
		$ReturnList .= ($ParAll==1) ? "<OPTION value=''>".$ec_iPortfolio['all_ele']."</OPTION>" : "";
		if(!empty($DefaultELEArray))
		{
			foreach($DefaultELEArray as $ELE_ID => $ELE_NAME)
			{
				$ReturnList .= "<OPTION value='".$ELE_ID."' ".($ParELE==$ELE_ID?'SELECTED':"").">".$ELE_NAME."</OPTION>";
			}
		}
		$ReturnList .= "</SELECT>";

		return $ReturnList;
	}

	function GET_ELE($ParShowAllELE=false, $langMask="")
	{
		global $eclass_db,$intranet_session_language;
		
		$langMask = $langMask ? strtolower($langMask) : strtolower($intranet_session_language);
		$titlefield = ($langMask=="en") ? "EngTitle" : "ChiTitle";		
		$sql = "SELECT RecordID, {$titlefield}, RecordStatus, DefaultID FROM {$eclass_db}.OLE_ELE";
		if(!$ParShowAllELE)
			$sql .= " WHERE RecordStatus = 1 OR RecordStatus = 2";
		$row = $this->returnArray($sql);

		$ReturnArray = array();

		for($i=0; $i<sizeof($row); $i++)
		{
			list($RecordID, $Title, $RecordStatus, $DefaultID) = $row[$i];
			$ID = ($RecordStatus==2 || $RecordStatus==3) ? $DefaultID : "[".$RecordID."]";
			$ReturnArray[$ID] = $Title;
		}

		return $ReturnArray;
	}

	function GET_ELE_INFO_QUERY($ParArr="")
	{
		global $eclass_db;
		
		$RecordID = $ParArr["RecordID"];
		$sql = "SELECT ChiTitle, EngTitle, RecordStatus FROM {$eclass_db}.OLE_ELE WHERE RecordID = '{$RecordID}'";
		return $sql;
	}
	
	function GET_ELE_INFO($ParArr="")
	{
		$sql = $this->GET_ELE_INFO_QUERY($ParArr);
		$ReturnArr = $this->returnArray($sql);
		return $ReturnArr;
	} // end function GET ELE INFO
	
	/**
	 * @return	Array	["APPROVAL_SETTING"]["EDITABLE_SETTING"]
	 */
	function GET_OLR_APPROVAL_SETTING_DATA2($ParIntExt="") {
		global $eclass_root;


		$old_file = "$eclass_root/files/portfolio_olr_approval_setting.txt";
		$new_file = "$eclass_root/files/portfolio_olr_approval_setting_splited.txt";
		if (file_exists($new_file)) {
			$ole_approval_file = $new_file;
			$ApprovalDataString = trim(get_file_content($ole_approval_file));
			$approvalSettings = unserialize($ApprovalDataString);
		} else {
			### Parse the old setting file to new one
			$ole_approval_file = $old_file;
			$ApprovalDataString = trim(get_file_content($ole_approval_file));
			$ApprovalDataArray = explode(",",$ApprovalDataString);
			
			$needApproval = $ApprovalDataArray[0]==2?false:true;	### if value is not 2, then need approval
			
			$approvalSettings['INT']['IsApprovalNeed'] = $needApproval;
			
			if ($approvalSettings['INT']['IsApprovalNeed']) {
				$approvalSettings['INT']['Elements'] = array(
																"ClassTeacher"=>empty($ApprovalDataArray[0])?true:false,
																"Self"=>$ApprovalDataArray[0]==1?true:false  // Self mean Request specific teacher to approve
																);
			} else {
				$approvalSettings['INT']['Elements'] = array(
																"Editable"=>empty($ApprovalDataArray[1])?false:true
																);
			}
			$approvalSettings['EXT'] = $approvalSettings['INT'];	### Synchronize int ext settings since old version do not have the splited concept
		}
		
		//Php 5.4 issue -> cannot parse not string into strtolower
		if(is_string($ParIntExt)){
			$str_ParIntExt = strtolower($ParIntExt);
		}else{
			$str_ParIntExt = 'BOTH';
		}

		switch($str_ParIntExt) {
			case "int":
			$returnSettings = $approvalSettings['INT'];
			break;
			case "ext":
			$returnSettings = $approvalSettings['EXT'];
			break;
			default:
			$returnSettings = $approvalSettings;
			break;
		}
		
		return $returnSettings;
	}
	/**
	 * @return	Array	["APPROVAL_SETTING"]["EDITABLE_SETTING"]
	 */
	function GET_OLR_APPROVAL_SETTING_DATA() {
		global $eclass_root;

		$ole_approval_file = "$eclass_root/files/portfolio_olr_approval_setting.txt";
		$ApprovalDataString = trim(get_file_content($ole_approval_file));

	
		$ApprovalDataArray = explode(",",$ApprovalDataString);

		//IN CASE CLIENT HAS NOT SET UP THE CONFIG,IT WILL BE NULL OR "" , SO DEFAULT SET TO "0"	
		$returnArray["APPROVAL_SETTING"] = empty($ApprovalDataArray[0])? 0 : $ApprovalDataArray[0];
		$returnArray["EDITABLE_SETTING"] = empty($ApprovalDataArray[1])? 0 : $ApprovalDataArray[1];

		return $returnArray;
	}

	function GET_OLR_EDITABLE_SETTING() {
		$ApprovalDataArray = $this->GET_OLR_APPROVAL_SETTING_DATA();
		return $ApprovalDataArray["EDITABLE_SETTING"];		
	}
	
	function GET_OLR_APPROVAL_SETTING()
	{
		$ApprovalDataArray = $this->GET_OLR_APPROVAL_SETTING_DATA();
		return $ApprovalDataArray["APPROVAL_SETTING"];
	}
	
	// Getting the distinct year of the OLE records
	// Modified by Key (2008-10-23): Add checking for invalid year
	function GET_OLE_DISTINCT_YEAR($StudentID = "")
	{
		global $eclass_db;
		
		if ($StudentID != "")
		{
			$Cond .= " AND UserID='".$StudentID."' ";
		}
		$Sql = 	"
						SELECT
							if ((Month(a.StartDate)>=".$this->year_start["month"]."), Year(a.StartDate), Year(a.StartDate)-1) AS StartYearID, 
							if ((Month(a.StartDate)>=".$this->year_start["month"]."), Year(a.StartDate), Year(a.StartDate)-1) AS StartYear,
							a.StartDate
						FROM
							{$eclass_db}.OLE_STUDENT as a
						WHERE
							1
							$Cond
						GROUP BY
							StartYear
						ORDER BY
							StartYear		
					";
		$ReturnArr1 = $this->returnArray($Sql);
		
		$tmpArr = array();
		for($i = 0; $i < count($ReturnArr1); $i++)
		{
			if($ReturnArr1[$i][0] != -1 && $ReturnArr1[$i][1] != -1)
			$tmpArr[] = $ReturnArr1[$i];
			else
			$tmpArr[] = array("0000-0001", "N/A");
		}
		$YearArray = $tmpArr;
		
		return $YearArray;
	}
	
	function GET_OLR_Category($ParNoDefaultCategory=0, $ParShowAllCat=false)
	{
		global $eclass_db, $intranet_session_language;
		if(!$ParShowAllCat)
			$cond = ($ParNoDefaultCategory==1) ? " WHERE RecordStatus = 1" : " WHERE RecordStatus = 1 OR RecordStatus = 2";
		$titlefield = ($intranet_session_language=="en") ? "EngTitle" : "ChiTitle";
		$titlefieldOtherLang = ($intranet_session_language=="en") ? "ChiTitle" : "EngTitle";
		$sql = "SELECT RecordID, If (".$titlefield." Is Null Or ".$titlefield." = '', ".$titlefieldOtherLang.", ".$titlefield.") as Title FROM {$eclass_db}.OLE_CATEGORY {$cond} ORDER BY RecordStatus DESC, RecordID ASC";
		
		$row = $this->returnArray($sql);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($RecordID, $Title) = $row[$i];
			
			if ($ParNoDefaultCategory == 1)
			{
				$Title = ($Title == '')? $RecordID : $Title;
				if($RecordID!="") {
					$ReturnArray[trim($RecordID)] = trim($Title);
				}
			}
			else
			{
				if($RecordID!="" && $Title!="")
					$ReturnArray[trim($RecordID)] = trim($Title);
			}
		}

		return $ReturnArray;
	}
	
	function GET_OLE_SUBCATEGORY($ParCategoryID=-9, $ParNoDefaultSubCategory=0, $ParShowAllSubCat=false) {
		global $eclass_db, $intranet_session_language,$ipf_cfg;

		if(!$ParShowAllSubCat)
			$cond = ($ParNoDefaultSubCategory==1) ? " WHERE RecordStatus = '".$ipf_cfg["OLE_SUBCATEGORY_RecordStatus"]["user_public"]."'" : " WHERE (RecordStatus = '".$ipf_cfg["OLE_SUBCATEGORY_RecordStatus"]["user_public"]."' OR RecordStatus = '".$ipf_cfg["OLE_SUBCATEGORY_RecordStatus"]["default_public"]."')";
		if (empty($ParCategoryID) || $ParCategoryID == -9) {
			// do nothing
		} else {
			$cond .= " AND CATID = '".$ParCategoryID."'";
		}
		
		$titlefield = ($intranet_session_language=="en") ? "EngTitle" : "ChiTitle";		
		$sql = "SELECT SUBCATID, {$titlefield} FROM {$eclass_db}.OLE_SUBCATEGORY {$cond} ORDER BY RecordStatus DESC, SUBCATID ASC";
		$row = $this->returnArray($sql);
		for($i=0; $i<sizeof($row); $i++)
		{
			list($RecordID, $Title) = $row[$i];
			if($RecordID!="" && $Title!="")
				$ReturnArray[trim($RecordID)] = trim($Title);
		}

		return $ReturnArray;
	}
	
	// Getting the distinct class of the OLE records
	function GET_OLE_DISTINCT_CLASS()
	{
		global $eclass_db, $intranet_db;
		
		$Sql = 	"
						SELECT
							b.ClassName,
							b.ClassName
						FROM
							{$eclass_db}.OLE_STUDENT a
						INNER JOIN
							{$intranet_db}.INTRANET_USER b
						ON
							a.UserID = b.UserID		
						GROUP BY
							b.ClassName
						ORDER BY
							b.ClassName		
					";					
		$ReturnArr1 = $this->returnArray($Sql);			
		
		return $ReturnArr1;
	}
	
	function returnOLRYear($ClassName="")
	{
		global $eclass_db, $intranet_db;

		if ($ClassName!="")
		{
			$sql = "SELECT DISTINCT
						if ((Month(c.StartDate)>=".$this->year_start["month"]."), Year(c.StartDate), Year(c.StartDate)-1) AS StartYear
					FROM
						{$eclass_db}.OLE_STUDENT AS c, {$intranet_db}.INTRANET_USER AS iu
					WHERE
						c.RecordStatus in (2, 4) AND iu.ClassName = '$ClassName' AND iu.RecordType='2' AND iu.UserID=c.UserID
					ORDER BY
						StartYear DESC
					";
		} else
		{
			$sql = "SELECT DISTINCT
						if ((Month(c.StartDate)>=".$this->year_start["month"]."), Year(c.StartDate), Year(c.StartDate)-1) AS StartYear
					FROM
						{$eclass_db}.OLE_STUDENT AS c
					WHERE
						c.RecordStatus in (2, 4)
					ORDER BY
						StartYear DESC
					";
		}
		$row_year = $this->returnVector($sql);

		for($i=0; $i<sizeof($row_year); $i++)
		{
			$year = $row_year[$i];
			$year_prev = $year-1;
			$ReturnArray[] = array($year, $year);
			if(!in_array($year_prev, $row_year))
			{
				$ReturnArray[] = array($year_prev, $year_prev);
			}
		}
		//rsort($ReturnArray);

		return $ReturnArray;
	}
	
	# count number of OLE data
	function countOLEDataNumber($my_year, $intext="")
	{
		global $eclass_db, $intranet_db;

		if($my_year!="") {
			$cond = " AND ay.AcademicYearID = '".$my_year."'";
		}
		
		# filtering int/ext records
		# Note: not like other OLE functions - 1: Int, 2: Ext		
		if($intext == 1)
			$cond .= " AND op.IntExt = 'INT'";
		else if($intext == 2)
			$cond .= " AND op.IntExt = 'EXT'";
		
		$sql = "SELECT
					COUNT(DISTINCT a.RecordID)
				FROM
					$eclass_db.OLE_STUDENT as a
					LEFT JOIN {$eclass_db}.OLE_PROGRAM as op ON a.ProgramID = op.ProgramID
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay ON op.AcademicYearID = ay.AcademicYearID
					LEFT JOIN {$intranet_db}.INTRANET_USER as iu ON a.UserID = iu.UserID
					LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER as iau ON a.UserID = iau.UserID
				WHERE
					(
						((iu.WebSAMSRegNo IS NOT NULL  ) and (iu.WebSAMSRegNo != ''))OR ((iau.WebSAMSRegNo IS NOT NULL) and (trim(iau.WebSAMSRegNo) != '' ))
					)
					$cond
				";
		
		$row = $this->returnVector($sql);

		return $row[0];
	}
	
	function GET_ELE_FIELD_FOR_RECORD($prefix="")
	{
		$DefaultELEArray = $this->GET_ELE();
		if(!empty($DefaultELEArray))
		{
			$ELEFields = $prefix."ELE";
			foreach($DefaultELEArray as $ELECode => $ELETitle)
			{
				$ELEFields = "REPLACE(".$ELEFields.", '$ELECode', '$ELETitle')";
			}
			$ELEFields = "REPLACE(".$ELEFields.", ',', '<br />')";
		}

		return $ELEFields;
	}
	
	function get_OLR_Category_Field_For_Record($prefix="", $ParShowAllCat=false)
	{
		$DefaultCategoryArray = $this->GET_OLR_Category(0, $ParShowAllCat);
		if(!empty($DefaultCategoryArray))
		{
			$CategoryField = "CASE ".$prefix."Category ";
			foreach($DefaultCategoryArray as $CategoryID => $CategoryTitle)
			{
				$CategoryField .= " WHEN ".$CategoryID." THEN '".$CategoryTitle."'";
			}
			$CategoryField .= " END";
		}

		return $CategoryField;
	}
	
	function get_OLR_SubCategory_Field_For_Record($prefix="")
	{
		global $eclass_db;

//		#M98350 		
// 		$titleField = Get_Lang_Selection('TitleChi', 'EngTitle');
		$titleField = Get_Lang_Selection('ChiTitle', 'EngTitle');
		$sql = "Select SubCatID, $titleField as Title From $eclass_db.OLE_SUBCATEGORY";
		$subCatAry = $this->returnResultSet($sql);
		 
		if(!empty($subCatAry))
		{
			$CategoryField = "CASE ".$prefix."SubCategoryID ";
			$CategoryField .= " WHEN -9 THEN '' ";
			foreach($subCatAry as $i => $_categoryAry)
			{
				$CategoryField .= " WHEN ".$_categoryAry['SubCatID']." THEN '".$this->Get_Safe_Sql_Query($_categoryAry['Title'])."'";
			}
			$CategoryField .= " ELSE '' ";
			$CategoryField .= " END";
		}
		else {
			$CategoryField .= " '' ";
		}

		return $CategoryField;
	}
	
	function RETURN_OLE_PROGRAM_ARRAY_BY_PROGRAMID_ARRAY($ProgramID)
	{
		global $eclass_db;

		if(is_array($ProgramID)){
			$conds_ProgramID = implode("','",$ProgramID);
		}
		else{
			$conds_ProgramID = $ProgramID;
		}

		$sql = "SELECT
					Title				
				FROM
					{$eclass_db}.OLE_PROGRAM
				WHERE
					ProgramID IN ('$conds_ProgramID')
				";
		$result = $this->returnResultSet($sql);

		return $result;
	}
	
	function RETURN_OLE_PROGRAM_BY_PROGRAMID($ProgramID)
	{
		global $eclass_db;

		$sql = "SELECT
					if(StartDate='0000-00-00', '', StartDate),
					if(EndDate='0000-00-00', '', EndDate),
					Title,
					Category,
					ELE,
					Organization,					
					Details,
					SchoolRemarks,
					InputDate,
					ModifiedDate,
					if (MONTH(StartDate) >=".$this->year_start["month"].",
					CONCAT(YEAR(StartDate),'-',YEAR(StartDate)+1),
					CONCAT(YEAR(StartDate)-1,'-',YEAR(StartDate))) AS Period,
					IntExt					
				FROM
					{$eclass_db}.OLE_PROGRAM
				WHERE
					ProgramID = '$ProgramID'
				";
		$row = $this->returnArray($sql, 11);

		return $row[0];
	}
	
	function RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX($ProgramID)
  {
    global $intranet_db, $eclass_db;
    $nameConds = getNameFieldByLang("IU.");
    $modifiedUserName = getNameFieldByLang("IU2.");
    
    // Added - 2015-01-13: OP.SASCategory, OP.SASPoint - Ng Wah SAS Cust
    $sql = "SELECT
							if(OP.StartDate='0000-00-00', '', OP.StartDate) as StartDate,
							if(OP.EndDate='0000-00-00', '', OP.EndDate) as EndDate,
							OP.Title,
							OP.TitleChi,
							OP.Category,
							OP.SubCategoryID,
							OP.ELE,
							OP.Organization,
							OP.Details,
							OP.DetailsChi,
							OP.SchoolRemarks,
							OP.InputDate,
							OP.ModifiedDate,
							IF(AY.AcademicYearID IS NULL, '--', IF(AYT.YearTermID IS NULL, ".Get_Lang_Selection("AY.YearNameB5","AY.YearNameEN").", CONCAT(".Get_Lang_Selection("AY.YearNameB5","AY.YearNameEN").", ',&nbsp;', ".Get_Lang_Selection("AYT.YearTermNameB5", "AYT.YearTermNameEN")."))) AS Period,
							OP.IntExt,
							OP.AcademicYearID,
							OP.YearTermID,
							OP.CanJoin,
							OP.CanJoinStartDate,
							OP.CanJoinEndDate,
							$nameConds as UserName,
							OP.AUTOAPPROVE,
							OP.JoinableYear,
							OP.MaximumHours,
							OP.DefaultHours,
							OP.CompulsoryFields,
							OP.DefaultApprover,
							OP.CreatorID,
							$modifiedUserName AS ModifiedUser,
							OP.IsSAS,
							OP.IsOutsideSchool,
							OP.SASCategory,
							OP.SASPoint
						FROM
						{$eclass_db}.OLE_PROGRAM OP
						LEFT JOIN $intranet_db.INTRANET_USER IU ON OP.CREATORID = IU.USERID
						LEFT JOIN $intranet_db.INTRANET_USER IU2 ON OP.ModifyBy = IU2.USERID
						LEFT JOIN $intranet_db.ACADEMIC_YEAR AS AY ON OP.AcademicYearID = AY.AcademicYearID
						LEFT JOIN $intranet_db.ACADEMIC_YEAR_TERM AS AYT ON OP.YearTermID = AYT.YearTermID
						WHERE
							OP.ProgramID = '$ProgramID'
          ";
    $row = current($this->returnArray($sql));
  
    return $row;
  }
	
	function RETURN_RECORDID_BY_EVENT_STUDENT($ProgramID, $StudentID)
	{
		global $eclass_db, $intranet_db;
		
		$Sql = "	SELECT
						RecordID
					FROM
						".$eclass_db.".OLE_STUDENT
					WHERE	
						ProgramID = '{$ProgramID}' AND
						UserID = '{$StudentID}'
				";
				
		$ReturnArray = $this->returnVector($Sql);
		return $ReturnArray[0];
	}
	
	function RETURN_OLE_CLASS_BY_PROGRAMID($ProgramID)
	{
		global $eclass_db, $intranet_db, $UserID;

    # Eric Yip (20091102): Limit student record of non-admin
    if($this->IS_TEACHER() && !$this->IS_IPF_ADMIN())
	   $program_cond = " AND (c.CreatorID = '".$UserID."' OR a.ApprovedBy = '".$UserID."')";

		$Sql = "	SELECT
						b.ClassName, b.ClassName
					FROM
						{$eclass_db}.OLE_STUDENT a,
						{$intranet_db}.INTRANET_USER b,
						{$eclass_db}.OLE_PROGRAM as c
					WHERE
						a.ProgramID = '$ProgramID'
						AND a.UserID = b.UserID
						AND a.ProgramID = c.ProgramID
						$program_cond
					GROUP BY
						b.ClassName
				";
		$ReturnArray = $this->returnArray($Sql, 2);

		return $ReturnArray;
	}
	

	function RETURN_OLE_RECORD_BY_RECORDID($record_id)
	{
		global $eclass_db, $sys_custom;
		/*
		$sql = "
		SELECT
			IF(P.STARTDATE='0000-00-00', '', P.STARTDATE) AS StartDate,
			IF(P.ENDDATE='0000-00-00', '', P.ENDDATE) AS EndDate,
			P.Title,
			P.Category,
			P.ELE,
			S.Role,
			IF(S.HOURS=0, '', S.HOURS) AS Hours,
			P.Organization,
			S.Achievement,
			P.Details,
			S.Attachment,
			S.ApprovedBy,
			P.SchoolRemarks as remark,
			S.ProcessDate,
			S.RecordStatus,
			S.UserID,
			P.IntExt,
			P.ProgramID,
			P.SubCategoryID,
			P.MaximumHours,
			P.CompulsoryFields,
			S.RequestApprovedBy
		FROM
			$eclass_db.OLE_PROGRAM P
			LEFT JOIN $eclass_db.OLE_STUDENT S ON P.ProgramID = S.ProgramID
		WHERE
			S.RecordID = '$record_id';	
		";
		*/ 
		# Details - modified as Case #F73433 
		# St. Paul PAS - if (S.Details<>'' AND S.Details is not null, S.Details, P.Details) AS Details 
		# Ohter - P.Details AS Details 
		// Added - 2015-01-13: SASPoint - Ng Wah SAS Cust
//		# To get updated details: if (S.Details<>'' AND S.Details is not null, S.Details, P.Details) AS Details 
//		# change to P.Details AS Details
		$details_sql = $sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']? "if (S.Details<>'' AND S.Details is not null, S.Details, P.Details)" : "P.Details";
		$sql = "SELECT
			IF(P.STARTDATE='0000-00-00', '', P.STARTDATE) AS StartDate,
			IF(P.ENDDATE='0000-00-00', '', P.ENDDATE) AS EndDate,
			P.Title,
			P.Category,
			P.ELE,
			S.Role,
			IF(S.HOURS=0, '', S.HOURS) AS Hours,
			P.Organization,
			S.Achievement,
			$details_sql AS Details,
			S.Attachment,
			S.ApprovedBy,
			P.SchoolRemarks as remark,
			S.ProcessDate,
			S.RecordStatus,
			S.UserID,
			P.IntExt,
			P.ProgramID,
			P.SubCategoryID,
			P.MaximumHours,
			P.CompulsoryFields,
			S.RequestApprovedBy,
			P.IsSAS,
			P.IsOutsideSchool,
			IF(S.SASPoint !='' OR S.SASPoint IS NOT NULL, S.SASPoint, P.SASPoint) AS SASPoint
		FROM
			{$eclass_db}.OLE_STUDENT S 
			INNER JOIN {$eclass_db}.OLE_PROGRAM P ON P.ProgramID = S.ProgramID
		WHERE
			S.RecordID = '$record_id';	
		";
		$returnArray = $this->returnArray($sql) or die(mysql_error());
		return $returnArray[0];
	}

	
	// Return a program id by title, start date and end date
	function RETURN_PROGRAM_ID($Title="", $StartDate="", $EndDate="", $InsertNew=false, $DataArray=array())
	{
		global $eclass_db, $UserID;
		
		$programType = $this->getProgramType();
		$programType = ($programType == "")? "T" : $programType;  //if have not set the $programType , default set as T (created by teacher)

		if ($StartDate != "")
		{
			$Cond .= " AND StartDate='".$StartDate."' ";
		}
		if ($EndDate != "")
		{
			$Cond .= " AND EndDate='".$EndDate."' ";
		}
		
		$Sql = 	"
						SELECT
							ProgramID
						FROM
							{$eclass_db}.OLE_PROGRAM
						WHERE
							Title = '".$Title."'
							$Cond
					";
		$ReturnArr1 = $this->returnVector($Sql);			
		if (count($ReturnArr1) > 0)
		{
			return $ReturnArr1[0];
		}
		else
		{
			if ($InsertNew)
			{
				$Sql = 	"
								INSERT INTO
									{$eclass_db}.OLE_PROGRAM
									(ProgramType, Title, StartDate, EndDate, Category, Organization, Details, SchoolRemarks, 
									 ELE, InputDate, ModifiedDate, IntExt, CreatorID)
								VALUES	
									('".$programType."','".addslashes($DataArray["Title"])."','".$DataArray["StartDate"]."','".$DataArray["EndDate"]."'
									,'".addslashes($DataArray["Category"])."','".addslashes($DataArray["Organization"])."','".addslashes($DataArray["Details"])."'
									,'".addslashes($DataArray["SchoolRemarks"])."','".$DataArray["ELE"]."',now(),now(), '".$DataArray["IntExt"] . "'," . "'" . $UserID . "')
							";

				$this->db_db_query($Sql);				
				$ReturnVal = $this->db_insert_id();				
			}
			else
			{
				$ReturnVal = 0;
			}
		}
		
		return $ReturnVal;
	}
	
	function GET_OLE_PROGRAM_TEACHER_IN_CHARGE_BY_RECORDID($record_id){
		global $eclass_db, $intranet_db;
		
    	$sql = "SELECT CONCAT('U',TIC.TeacherID) AS UserID, ".getNameFieldByLang("IU.")." AS UserName 
				FROM {$eclass_db}.OLE_PROGRAM_TIC TIC INNER JOIN {$intranet_db}.INTRANET_USER AS IU ON (TIC.TeacherID = IU.UserID)
				WHERE TIC.ProgramID = '$record_id'";
				
        return $this->returnArray($sql);
	}
	
	function GET_STUDENT_OLE_INFO($ProgramID, $ParStudentArr= array())
	{
		global $eclass_db, $intranet_db;

		if (count($ParStudentArr) > 0)
		{
			$StudentImplode = implode(",",$ParStudentArr);
		}
		else
		{
			$StudentImplode = -1;
		}
		
		$namefield = getNameFieldByLang2("a.");
		
		// 2015-01-21 - Add b.Details - for St. Paul PAS CUST
		// 2015-01-12 - Add b.SASPoint - for Ng Wah SAS CUST
		$Sql = "	SELECT
						a.UserID,
						".$namefield.",
						CONCAT(a.ClassName, '-',a.ClassNumber) AS ClassNumberText,
						b.Role,
						b.Achievement,
						b.Hours,
						b.Attachment,
						b.TeacherComment,
						b.RecordStatus,
						b.RecordID,
						b.ApprovedBy,
						b.SASPoint,
						b.Details
					FROM
						".$intranet_db.".INTRANET_USER a
					LEFT JOIN
						".$eclass_db.".OLE_STUDENT b
					ON 
						b.ProgramID = '$ProgramID'
						AND a.UserID = b.UserID
					WHERE	
						a.UserID IN ({$StudentImplode})
					ORDER BY
						a.ClassName, a.ClassNumber
				";
//				debug_r($Sql);die;
		$ReturnArray = $this->returnArray($Sql, 10);

		return $ReturnArray;
		
	}
	
	# OLR category list to query condition
	function OLR_CATEGORY_TO_SQL_FIELD($prefix="")
	{
		$DefaultCategoryArray = $this->GET_OLR_CATEGORY();
		if(!empty($DefaultCategoryArray))
		{
			$CategoryField = "CASE ".$prefix."Category ";
			foreach($DefaultCategoryArray as $CategoryID => $CategoryTitle)
			{
				$CategoryField .= " WHEN ".$CategoryID." THEN '".$CategoryTitle."'";
			}
			$CategoryField .= " END";
		}

		return $CategoryField;
	}
	
	function returnOLERecordByRecordID($record_id)
	{
		global $eclass_db;

		$sql = "SELECT
					if(StartDate='0000-00-00', '', StartDate),
					if(EndDate='0000-00-00', '', EndDate),
					Title,
					Category,
					ELE,
					Role,
					if(Hours=0, '', Hours),
					Organization,
					Achievement,
					Details,
					Attachment,
					ApprovedBy,
					remark,
					ProcessDate,
					RecordStatus,
					UserID
				FROM
					{$eclass_db}.OLE_STUDENT
				WHERE
					RecordID = '$record_id'
				";
		$row = $this->returnArray($sql, 11);

		return $row[0];
	}
	
	function GetFileUpload($ParTableName, $ParFileNumberName, $ParFieldName, $ParFieldWidth=40)
	{
		global $button_more_file;

		$rx = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"".$ParTableName."\">";
		$rx .= "<tr>";
		$rx .= "<td><input class=\"file\" type=\"file\" name=\"".$ParFieldName."1\" size=\"".$ParFieldWidth."\"/><input type=\"hidden\" name=\"".$ParFieldName."1_hidden\"/></td>";
		$rx .= "</tr>";
		$rx .= "</table>";
		//$rx .= "<input id=\"addButton\" type=\"button\" onclick=\"jAddMoreFileField(this.form, '".$ParTableName."', '".$ParFileNumberName."', '".$ParFieldName."')\" value=\" + \" title=\"".$button_more_file."\" />";
		$rx .= "<a class=\"contenttool\" href=\"javascript:jAddMoreFileField(this.form, '".$ParTableName."', '".$ParFileNumberName."', '".$ParFieldName."')\">".$button_more_file."</a>";

		return $rx;
	}
	
	function CHECK_OLE_RECORDID_EXIST($RecordID)
	{
		global $eclass_db;
		$sql = "SELECT COUNT(*) FROM {$eclass_db}.OLE_STUDENT WHERE RecordID = '$RecordID'";
		$row = $this->returnVector($sql);

		$valid = ($row[0]>0) ? 1 : 0;

		return $valid;
	}
	
	# Get OLE list of a student
	function GET_STUDENT_OLE_LIST($ParUserID)
	{
		global $eclass_db, $profiles_to;

		$CategoryField = $this->OLR_CATEGORY_TO_SQL_FIELD();

		$sql =	"
							SELECT
								IF ((StartDate IS NULL OR StartDate='0000-00-00'),'--',IF(EndDate IS NULL OR EndDate='0000-00-00',StartDate,CONCAT(StartDate,' $profiles_to<br />',EndDate))) as OLEDate,
								Title,
								$CategoryField as Category,
								Role
							FROM
								{$eclass_db}.OLE_STUDENT
							WHERE
								UserID = '".$ParUserID."' AND
								(RecordStatus = '2' || RecordStatus = '4')
							ORDER BY
								StartDate DESC
						";
		$ReturnArr = $this->returnArray($sql);

		return $ReturnArr;
	}
	
	# Determine if the report is customarized
	function IS_CUSTOMARIZE_SLP_REPORT()
	{
		global $sys_custom;

		if(!is_array($sys_custom['iPortfolio_SLP_Report']) || count(array_filter($sys_custom['iPortfolio_SLP_Report'])) != 1)
			return false;
		else
			return true;
	}
	
	# Get name of the school for SLP report customarization
	# Must match with settings.php	
	function GET_CUSTOMARIZE_SCHOOL_SLP()
	{
		global $sys_custom;
		
		if(is_array($sys_custom['iPortfolio_SLP_Report']))
			return key(array_filter($sys_custom['iPortfolio_SLP_Report']));
	}

	# Get OLE records assigned
	function GET_OLE_RECORD_ASSIGNED($ParUserID, $ParIntExt=0, $PKMSSLP=0)
	{
		global $eclass_db, $ipf_cfg;
	
		//$IntExt = ($ParIntExt==0) ? "INT" : "EXT";
		$IntExt= $ParIntExt;
		
		if($PKMSSLP){
			$targetOrder = "os.PKMSSLPOrder IS NOT NULL AND";
			$IntExt = 3;
			$ordering = "os.PKMSSLPOrder";
		} else {
			$targetOrder = "os.SLPOrder IS NOT NULL AND";
			$ordering = "os.SLPOrder";
		}
		
		if($IntExt == 1)
			$conds .= " AND op.IntExt = 'EXT'";
		elseif($IntExt == 3)
			$conds .= " AND (op.IntExt = 'INT' OR op.IntExt = 'EXT')";
		else
			$conds .= " AND op.IntExt = 'INT'";
	
		$sql =	"
					SELECT
						os.RecordID as RecordID
					FROM
						{$eclass_db}.OLE_STUDENT as os
					INNER JOIN
						{$eclass_db}.OLE_PROGRAM as op
					ON
						os.ProgramID = op.ProgramID
					WHERE
						os.UserID = '".$ParUserID."' AND
						$targetOrder
						os.RecordStatus IN ('".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]."','".$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"]."') 
						{$conds}
					ORDER BY
						$ordering
				";

		$ReturnArr = $this->returnVector($sql);

		return $ReturnArr;
	}
	
	# Get OLE records assigned
	function GET_OLE_ASSIGNED_RECORD_INFO($ParUserID, $ParAcademicYearIDArr='', $ParIntExt='', $OrderByELE=0)
	{
		global $eclass_db, $intranet_db, $ipf_cfg;
		
		$conds_AcademicYearID = '';
		if ($ParAcademicYearIDArr != '')
			$conds_AcademicYearID = " And op.AcademicYearID In (".implode(',', $ParAcademicYearIDArr).") ";
			
		$conds_IntExt = '';
		if ($ParIntExt != '')
			$conds_IntExt = " And os.IntExt = '".$ParIntExt."' ";
		
		$OrderByField = ($OrderByELE)? 'os.ELE' : 'os.SLPOrder';
		
		$sql =	"Select
						os.RecordID, 
						os.StartDate, 
						op.Title, 
						os.Category as CategoryID,
						oc.EngTitle as CategoryNameEn, 
						oc.ChiTitle as CategoryNameCh,
						os.Role, 
						os.ELE, 
						os.Organization,
						ay.AcademicYearID,
						ay.YearNameEN as AcademicYearNameEn,
						ay.YearNameB5 as AcademicYearNameCh,
						os.IntExt
				From
						{$eclass_db}.OLE_STUDENT as os
						Inner Join
						{$eclass_db}.OLE_CATEGORY as oc On (os.Category = oc.RecordID)
						Inner Join
						{$eclass_db}.OLE_PROGRAM as op On (os.ProgramID = op.ProgramID)
						Inner Join
						{$intranet_db}.ACADEMIC_YEAR as ay On (op.AcademicYearID = ay.AcademicYearID)
				Where
						os.UserID = '".$ParUserID."'
						And os.SLPOrder IS NOT NULL
						And os.RecordStatus IN ('".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]."','".$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"]."')
						$conds_AcademicYearID
						$conds_IntExt
				Order By
						$OrderByField
				";
		$ReturnArr = $this->returnArray($sql);
		
		return $ReturnArr;
	}
	
	# Set order of OLE records
	function SET_OLE_RECORD_ORDER_SLP($ParUserID, $ParOLEArr, $IntExt=0, $PKMSSLP=0)
	{
		global $eclass_db;
		
		$IntExt = $IntExt;
		
		if($PKMSSLP){
			$targetSLP = "os.PKMSSLPOrder";
			$IntExt = 3;
		} else {
			$targetSLP = "os.SLPOrder";
		}
		
		if($IntExt == 1)
			$conds .= " AND op.IntExt = 'EXT'";
		elseif($IntExt == 3)
			$conds .= " AND (op.IntExt = 'INT' OR op.IntExt = 'EXT')";
		else
			$conds .= " AND op.IntExt = 'INT'";
		
	//	$IntExt = ($ParIntExt==0) ? "INT" : "EXT";
	
		
		# Reset order of OLE records for the student
		$sql =	"
							UPDATE
								{$eclass_db}.OLE_STUDENT as os
							INNER JOIN
								{$eclass_db}.OLE_PROGRAM as op
							ON
								os.ProgramID = op.ProgramID
							SET
								$targetSLP = NULL
							WHERE
								os.UserID = '".$ParUserID."'
								{$conds}
						";
		$this->db_db_query($sql);

		# Set order of OLE records
		for($i=0; $i<count($ParOLEArr); $i++)
		{
			$sql =	"
								UPDATE
									{$eclass_db}.OLE_STUDENT as os
								INNER JOIN
									{$eclass_db}.OLE_PROGRAM as op
								ON
									os.ProgramID = op.ProgramID
								SET
									$targetSLP = ".($i+1)."
								WHERE
									os.RecordID = '".$ParOLEArr[$i]."'
									{$conds}
							";

			$this->db_db_query($sql);
		}
	}

	# Get OLE extra settings
	function GET_OLE_SETTINGS_SLP($ParIntExt=0)
	{
		global $eclass_root;
	
		$portfolio_report_config_file = "$eclass_root/files/portfolio_slp_config.txt";
		if(file_exists($portfolio_report_config_file))
		{
			list($filecontent, $OLESettings, $ExtSettings) = explode("\n", trim(get_file_content($portfolio_report_config_file)));
			if($OLESettings != "")
			{
				$OLESettings = unserialize($OLESettings);
			}
			if($ExtSettings != "")
			{
				$ExtSettings = unserialize($ExtSettings);
			}
		}
		
		return ($ParIntExt == 0 ? $OLESettings : $ExtSettings);
	}
	
	function RESET_OVERSET_RECORD_SLP($ParUserID, $ParRecordLimit, $ParIntExt=0, $PKMSSLP=0)
	{
		global $eclass_db;
		
		$IntExt = ($ParIntExt==0) ? "INT" : "EXT";
		
		if($PKMSSLP){
			$targetOrder = " PKMSSLPOrder";
			$IntExtpar = " (IntExt = '"."INT"."' || IntExt = '"."EXT"."')";
		} else {
			$targetOrder = " SLPOrder";
			$IntExtpar = " IntExt = '".$IntExt."'";
		}
		
		$sql =	"
							SELECT
								RecordID
							FROM
								{$eclass_db}.OLE_STUDENT
							WHERE
								$targetOrder IS NOT NULL AND
								UserID = '".$ParUserID."' AND
								$IntExtpar
							ORDER BY
								$targetOrder
							LIMIT
								".$ParRecordLimit."
						";
		$ReturnArr = $this->returnVector($sql);

		if(count($ReturnArr) > 0)
		{
			$sql =	"
								UPDATE
									{$eclass_db}.OLE_STUDENT
								SET
									$targetOrder = NULL
								WHERE
									UserID = '".$ParUserID."' AND
									$IntExtpar AND
									RecordID NOT IN (".implode(",", $ReturnArr).")
							";
			$this->db_db_query($sql);
		}
	}
	
	# Function to retrieve OLR submitted by school
	function GET_OLR_BY_STATUS($status="")
	{
		global $eclass_db;

		$conds = " WHERE RecordStatus = '$status'";
		$sql = "SELECT DISTINCT RecordID FROM {$eclass_db}.OLE_STUDENT $conds";
		$row = $this->returnVector($sql);

		return $row;
	}

	// Get teh ELE new / edit form
	function GET_ELE_FORM($ParArr="")
	{
		global $image_path, $LAYOUT_SKIN, $iPort, $linterface, $ec_iPortfolio, $Lang;
		
		$RecordID = $ParArr["RecordID"];
		$ELEData = $ParArr["ELEData"];
		
		$EngTitle = $ELEData[0]["EngTitle"];
		$ChiTitle = $ELEData[0]["ChiTitle"];
		$Status = $ELEData[0]["RecordStatus"];
		$isDefault = ($Status == 2 || $Status == 3);
		$isActivate = ($Status == 1 || $Status == 2);
		$Activate_Value = ($isDefault) ? 2 : 1;
		$Deactivate_Value = ($isDefault) ? 3 : 0;
				
		$Action = ($RecordID != "") ? "edit" : "new";
		
		if($isActivate)
		{
			$Check_Activate = "checked";
			$Check_Deactivate = "";
		}
		else
		{
			$Check_Activate = "";
			$Check_Deactivate = "checked";
		}
		
		$x = "";
		
		$x = "<form name=\"form1\" id=\"form1\" method=\"post\" action=\"update_ele.php\">";
		
		$x .= "<table width=\"88%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
		$x .= "<tr>";
		$x .= "<td>";
		$x .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
		
		// eng title
		$x .= "<tr>";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">".$ec_iPortfolio['title']." (".$ec_iPortfolio['lang_eng'].")".":<span class=\"tabletextrequire\">*</span></span></td>";
		$x .= "<td width=\"80%\" valign=\"top\">";
		$x .= ($isDefault) ? "<input name=\"EngTitle\" type=\"hidden\" class=\"textboxtext\" value=\"{$EngTitle}\" />{$EngTitle}" : "<input name=\"EngTitle\" id=\"EngTitle\" type=\"text\" class=\"textboxtext\" value=\"".$EngTitle."\" />";
		$x .= "</td>";
		$x .= "</tr>";
		
		// chi title
		$x .= "<tr>";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">".$ec_iPortfolio['title']." (".$ec_iPortfolio['lang_chi'].")".":<span class=\"tabletextrequire\">*</span></span></td>";
		$x .= "<td width=\"80%\" valign=\"top\">";
		$x .= ($isDefault) ? "<input name=\"ChiTitle\" type=\"hidden\" class=\"textboxtext\" value=\"{$ChiTitle}\" />{$ChiTitle}" : "<input name=\"ChiTitle\" id=\"ChiTitle\" type=\"text\" class=\"textboxtext\" value=\"".$ChiTitle."\" />";
		$x .= "</td>";
		$x .= "</tr>";

		// status
		$x .= "<tr>";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">".$iPort["status"]."</span></td>";
		$x .= "<td><label for=\"grading_passfail\" class=\"tabletext\">";
		$x .= "<input name=\"status\" type=\"radio\" id=\"status_1\" value=\"{$Activate_Value}\" ".$Check_Activate." /><label for=\"status_1\">".$iPort["activate"]."</label>";
		$x .= "<input name=\"status\" type=\"radio\" id=\"status_0\" value=\"{$Deactivate_Value}\" ".$Check_Deactivate." /><label for=\"status_0\">".$iPort["deactivate"]."</label>";
		$x .= "</td>";
		$x .= "</tr>";
		
		$x .= "</table>";
		$x .= "</td>";
		$x .= "</tr>";

		
		// dot line
		$x .= "<tr>";
		$x .= "<td height=\"1\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td>";
		$x .= "</tr>";
		
		$x .= "<tr>";
		$x .= "<td align=\"right\">";
		
		// submit button
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td class=\"tabletextremark\">".$iPort["msg"]["fields_with_asterisk_are_required"]."</td>";
		$x .= "<td align=\"right\">";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "button", "jSubmit();", "btnSubmit");
		$x .= "&nbsp;";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["reset"], "button", "jReset();", "btnReset");
		$x .= "&nbsp;";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["cancel"], "button", "jCancel();", "btnCancel");
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "</tr>";
		
		$x .= "</table>";
		$x .= "<input type=\"hidden\" name=\"RecordID\" value=\"".$RecordID."\" />";
		$x .= "<input type=\"hidden\" name=\"Action\" value=\"".$Action."\" />";
		$x .= "</form>";
		
		return $x;
	} // end function get ele form
	
	// get settings -> submission period form
	function GET_SUBMISSION_PERIOD_FORM($ParArr="")
	{
		global $linterface, $iPort, $i_ClassLevelName;
		
		$today = date('Y-m-d');
		
		$sup1 = "<sup style='color:red'>*1</sup>";
		$sup2 = "<sup style='color:red'>*2</sup>";
		$sup3 = "<sup style='color:red'>*3</sup>";
		$sup4 = "<sup style='color:red'>*4</sup>";
		
		$x = "";
		$x .= "<form name=\"form1\" action=\"update_student_submission_period.php\" method=\"post\">";
		$x .= "<table width=\"80%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
		
//		$starttime = $ParArr["starttime"];
//		$sh = $ParArr["sh"];
//		$sm = $ParArr["sm"];
//		$endtime = $ParArr["endtime"];
//		$eh = $ParArr["eh"];
//		$em = $ParArr["em"];
//		$check_int = $ParArr["check_int"];
//		
//		$SetSLPPeriod_starttime = $ParArr["SetSLPPeriod_starttime"];
//		$SetSLPPeriod_sh = $ParArr["SetSLPPeriod_sh"];
//		$SetSLPPeriod_sm = $ParArr["SetSLPPeriod_sm"];
//		$SetSLPPeriod_endtime = $ParArr["SetSLPPeriod_endtime"];
//		$SetSLPPeriod_eh = $ParArr["SetSLPPeriod_eh"];
//		$SetSLPPeriod_em = $ParArr["SetSLPPeriod_em"];
//		$check_SetSLPPeriod = $ParArr["check_SetSLPPeriod"];
//		
//		$ex_starttime = $ParArr["ex_starttime"];
//		$ex_sh = $ParArr["ex_sh"];
//		$ex_sm = $ParArr["ex_sm"];
//		$ex_endtime = $ParArr["ex_endtime"];
//		$ex_eh = $ParArr["ex_eh"];
//		$ex_em = $ParArr["ex_em"];
//		$check_ext = $ParArr["check_ext"];
//		
//		$x = "";
//		$x .= "<form name=\"form1\" action=\"update_student_submission_period.php\" method=\"post\">";
//		$x .= "<table width=\"80%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
//		
//		$x .= "<tr><td>";
//		############ Start: Table for INTERNAL ############
//		$x .= $linterface->GET_NAVIGATION2($iPort["internal_record_submision_period"]);
//		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
//		
//		// Start Time
//		$x .= "<tr>";
//		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\">";
//		$x .= "<span class=\"tabletext\">".$iPort["start_time"]."</span>".$sup1;
//		$x .= "</td>";
//		$x .= "<td valign=\"top\">";
//		$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
//		$x .= "<tr>";
//		$x .= "<td><input name=\"starttime\" type=\"text\" class=\"tabletext\" value=\"".$starttime."\" /></td>";
//		$x .= "<td width=\"25\" align=\"center\">";
//		$x .= "<div style=\"display: none;\">";
//		$x .= "<input name=\"starttime\" type=\"hidden\" class=\"tabletext\" value=\"\" />";
//		$x .= $linterface->GET_CALENDAR("form1", "starttime");
//		$x .= "</div>";
//		$x .= "<td align=\"center\">";
//		$x .= returnHour('sh', $sh, 0)." : ".returnMinute('sm', $sm, 0);
//		$x .= "</td>";
//		$x .= "<td width=\"25\" align=\"center\"></td>";
//		$x .= "</tr>";
//		$x .= "</table>";
//		$x .= "</td>";
//		$x .= "</tr>";
//		
//		// End Time
//		$x .= "<tr>";
//		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\">";
//		$x .= "<span class=\"tabletext\">".$iPort["end_time"]."</span>"."</span>".$sup2;
//		$x .= "</td>";
//		$x .= "<td valign=\"top\">";
//		$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
//		$x .= "<tr>";
//		$x .= "<td><input name=\"endtime\" type=\"text\" class=\"tabletext\" value=\"".$endtime."\" /></td>";
//		$x .= "<td width=\"25\" align=\"center\">";
//		$x .= "<div style=\"display: none\">";
//		$x .= $linterface->GET_CALENDAR("form1", "endtime");
//		$x .= "</div>";
//		$x .= "<td align=\"center\">";
//		$x .= returnHour('eh', $eh, 0)." : ".returnMinute('em', $em, 0);
//		$x .= "</td>";
//		$x .= "<td width=\"25\" align=\"center\"></td>";
//		$x .= "</tr>";
//		$x .= "</table>";
//		
//		$x .= "</td>";
//		$x .= "</tr>";
//		
//		// Internal / external submit type
//		$x .= "<tr>";
//		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\">";
//		$x .= "<span class=\"tabletext\">&nbsp;</span>";
//		$x .= "</td>";
//		$x .= "<td valign=\"top\">";
//		$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
//		$x .= "<tr>";
//		$x .= "<td align=\"center\">";
//		$x .= "<input type='checkbox' name='check_int' id='check_int' $check_int /><label for='check_int'>".$iPort["allow_to_submit"]."</label>";
//		$x .= "</td>";
//		$x .= "</tr>";
//		$x .= "</table>";
//		$x .= "</td>";
//		$x .= "</tr>";
//		$x .= "</table>";
		############ End: Table for INTERNAL ############
//		$x .= "</td></tr>";
						
		$x .= "<tr><td>";
		############ Start: Table for INTERNAL ############
		$x .= $linterface->GET_NAVIGATION2($iPort["internal_record_submision_period"]);
		
		$x .= "<table class=\"common_table_list_v30 edit_table_list_v30\" cellspacing=\"0\" cellpadding=\"5\">
					<tr>
						<th>".$i_ClassLevelName."</th>
						<th>".$iPort["start_time"].$sup1."</th>
						<th>".$iPort["end_time"].$sup2."</th>
						<th>".$iPort["allow_to_submit"]."</th>
					</tr>
					<tr class=\"edit_table_head_bulk\">
	                	<th>&nbsp;</th>
	                	<th>
							<div style=\"float:left;\">".
//								"<input type=\"text\" name=\"starttime_OLE\" id=\"starttime_OLE\" value=\"".date('Y-m-d')."\" />".
//								$linterface->GET_CALENDAR("form1", "starttime_OLE").
								$linterface->GET_DATE_PICKER("starttime_OLE", $today, "", "yy-mm-dd").
								returnHour("sh_OLE", "", 0)." : ".returnMinute("sm_OLE", "", 0).
							"</div>".
							$linterface->Get_Apply_All_Icon("javascript:SetAllTime('OLE', 'start', '".count($ParArr['OLE'])."');")."
						</th>
	                    <th>
							<div style=\"float:left;\">".
//								"<input type=\"text\" name=\"endtime_OLE\" id=\"endtime_OLE\" value=\"".date('Y-m-d')."\" />".
//								$linterface->GET_CALENDAR("form1", "endtime_OLE").
								$linterface->GET_DATE_PICKER("endtime_OLE", $today, "", "yy-mm-dd").
								returnHour("eh_OLE", "", 0)." : ".returnMinute("em_OLE", "", 0).
							"</div>".
							$linterface->Get_Apply_All_Icon("javascript:SetAllTime('OLE', 'end', '".count($ParArr['OLE'])."');")."
	                    </th>
	                    <th>
							<input type=\"checkbox\" name=\"allow_submit_OLE\" id=\"allow_submit_OLE\" onclick=\"checkAll_allow_submit('OLE', '".count($ParArr['OLE'])."', this.checked)\"/>
	                    </th>
					</tr>";
					
		$i = 0;
		foreach($ParArr['OLE'] as $data)
		{
			$x .= "	<tr>
						<td>
							<span>".$data['ClassLevel']['LevelName']."</span>
							<input type=\"hidden\" name=\"LevelID_OLE[]\" id=\"LevelID_OLE_$i\" value=".$data['ClassLevel']['ClassLevelID'].">
						</td>
				    	<td>".
//							"<input type=\"text\" name=\"starttime_OLE[]\" id=\"starttime_OLE_$i\" value=\"".$data['StartDate']."\" />".
//							$linterface->GET_CALENDAR("form1", "starttime_OLE_$i").
							$linterface->GET_DATE_PICKER("starttime_OLE[]", $data['StartDate'], "", "yy-mm-dd", "", "", "", "", "starttime_OLE_$i", 0, 1).
							returnHour("sh_OLE[]", $data['StartHour'], 0)." : ".returnMinute("sm_OLE[]", $data['StartMinute'], 0)."
						</td>
					    <td>".
//							".<input type=\"text\" name=\"endtime_OLE[]\" id=\"endtime_OLE_$i\" value=\"".$data['EndDate']."\" />".
//							$linterface->GET_CALENDAR("form1", "endtime_OLE_$i").
							$linterface->GET_DATE_PICKER("endtime_OLE[]", $data['EndDate'], "", "yy-mm-dd", "", "", "", "", "endtime_OLE_$i", 0, 1).
							returnHour("eh_OLE[]", $data['EndHour'], 0)." : ".returnMinute("em_OLE[]", $data['EndMinute'], 0)."
						</td>
					    <td>
							<input type=\"checkbox\" name=\"allow_submit_OLE_$i\" id=\"allow_submit_OLE_$i\" ".($data['AllowSubmit']? "checked":"")." />
						</td>
				 	</tr>";
			$i++;
		}
		$x .= "	</table>";
		############ End: Table for INTERNAL ############
		$x .= "</td></tr>";
		
//		$x .= "<tr>";
//		$x .= "<td colspan=\"2\">&nbsp;";
//		$x .= "</td></tr>";
//		$x .= "<tr><td class=\"dotline\" colspan=\"6\"><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"10\" height=\"1\"></td></tr>";	
//		$x .= "<tr>";
//		$x .= "<td>";
//		$x .= $linterface->GET_NAVIGATION2($iPort["external_recordSetPerios"]);
//		############ Start: Table for EXTERNAL ############
//		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
//		
//		// Start Time
//		$x .= "<tr>";
//		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\">";
//		$x .= "<span class=\"tabletext\">".$iPort["start_time"]."</span>".$sup1;
//		$x .= "</td>";
//		$x .= "<td valign=\"top\">";
//		$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
//		$x .= "<tr>";
//		$x .= "<td><input name=\"ex_starttime\" type=\"text\" class=\"tabletext\" value=\"".$ex_starttime."\" /></td>";
//		$x .= "<td width=\"25\" align=\"center\">";
//		$x .= $linterface->GET_CALENDAR("form1", "ex_starttime");
//		$x .= "<td align=\"center\">";
//		$x .= returnHour('ex_sh', $ex_sh, 0)." : ".returnMinute('ex_sm', $ex_sm, 0);
//		$x .= "</td>";
//		$x .= "<td width=\"25\" align=\"center\"></td>";
//		$x .= "</tr>";
//		$x .= "</table>";
//		$x .= "</td>";
//		$x .= "</tr>";
//		
//		// End Time
//		$x .= "<tr>";
//		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\">";
//		$x .= "<span class=\"tabletext\">".$iPort["end_time"]."</span>".$sup2;
//		$x .= "</td>";
//		$x .= "<td valign=\"top\">";
//		$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
//		$x .= "<tr>";
//		$x .= "<td><input name=\"ex_endtime\" type=\"text\" class=\"tabletext\" value=\"".$ex_endtime."\" /></td>";
//		$x .= "<td width=\"25\" align=\"center\">";
//		$x .= $linterface->GET_CALENDAR("form1", "ex_endtime");
//		$x .= "<td align=\"center\">";
//		$x .= returnHour('ex_eh', $ex_eh, 0)." : ".returnMinute('ex_em', $ex_em, 0);
//		$x .= "</td>";
//		$x .= "<td width=\"25\" align=\"center\"></td>";
//		$x .= "</tr>";
//		$x .= "</table>";
//		$x .= "</td>";
//		$x .= "</tr>";
//		
//		// Internal / external submit type
//		$x .= "<tr>";
//		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\">";
//		$x .= "<span class=\"tabletext\">&nbsp;</span>";
//		$x .= "</td>";
//		$x .= "<td valign=\"top\">";
//		$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
//		$x .= "<tr>";
//		$x .= "<td align=\"center\">";
//		$x .= "<input type='checkbox' name='check_ext' id='check_ext' $check_ext /><label for='check_ext'>".$iPort["allow_to_submit"]."</label>";
//		$x .= "</td>";
//		$x .= "</tr>";
//		$x .= "</table>";
//		$x .= "</td>";
//		$x .= "</tr>";
//		$x .= "</table>";
//		############ End: Table for EXTERNAL ############		
//		$x .= "</td></tr>";

		$x .= "<tr><td colspan=\"2\">&nbsp;</td></tr>";
		$x .= "<tr><td class=\"dotline\" colspan=\"6\"><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"10\" height=\"1\"></td></tr>";	
		
		$x .= "<tr><td>";
		############ Start: Table for EXTERNAL ############		
		$x .= $linterface->GET_NAVIGATION2($iPort["external_recordSetPerios"]);
		
		$x .= "<table class=\"common_table_list_v30 edit_table_list_v30\" cellspacing=\"0\" cellpadding=\"5\">
					<tr>
						<th>".$i_ClassLevelName."</th>
						<th>".$iPort["start_time"].$sup1."</th>
						<th>".$iPort["end_time"].$sup2."</th>
						<th>".$iPort["allow_to_submit"]."</th>
					</tr>
					<tr class=\"edit_table_head_bulk\">
	                    <th>&nbsp;</th>
	                    <th>
							<div style=\"float:left;\">".
//								"<input type=\"text\" name=\"starttime_EXT\" id=\"starttime_EXT\" value=\"".date('Y-m-d')."\" />".
//								$linterface->GET_CALENDAR("form1", "starttime_EXT").
								$linterface->GET_DATE_PICKER("starttime_EXT", $today, "", "yy-mm-dd").
								returnHour("sh_EXT", "", 0)." : ".returnMinute("sm_EXT", "", 0).
							"</div>".
							$linterface->Get_Apply_All_Icon("javascript:SetAllTime('EXT', 'start', '".count($ParArr['EXT'])."');")."
						</th>
	                    <th>
							<div style=\"float:left;\">".
//								"<input type=\"text\" name=\"endtime_EXT\" id=\"endtime_EXT\" value=\"".date('Y-m-d')."\" />".
//								$linterface->GET_CALENDAR("form1", "endtime_EXT").
								$linterface->GET_DATE_PICKER("endtime_EXT", $today, "", "yy-mm-dd").
								returnHour("eh_EXT", "", 0)." : ".returnMinute("em_EXT", "", 0).
							"</div>".
							$linterface->Get_Apply_All_Icon("javascript:SetAllTime('EXT', 'end', '".count($ParArr['EXT'])."');")."
	                    </th>
	                    <th>
							<input type=\"checkbox\" name=\"allow_submit_EXT\" id=\"allow_submit_EXT\" onclick=\"checkAll_allow_submit('EXT', '".count($ParArr['EXT'])."', this.checked)\"/>
	                    </th>
					</tr>";
		
		$i = 0;
		foreach($ParArr['EXT'] as $data)
		{
			$x .= "	<tr>
						<td>
							<span>".$data['ClassLevel']['LevelName']."</span>
							<input type=\"hidden\" name=\"LevelID_EXT[]\" id=\"LevelID_EXT_$i\" value=".$data['ClassLevel']['ClassLevelID'].">
						</td>
				    	<td>".
//							"<input type=\"text\" name=\"starttime_EXT[]\" id=\"starttime_EXT_$i\" value=\"".$data['StartDate']."\" />".
//							$linterface->GET_CALENDAR("form1", "starttime_EXT_$i").
							$linterface->GET_DATE_PICKER("starttime_EXT[]", $data['StartDate'], "", "yy-mm-dd", "", "", "", "", "starttime_EXT_$i", 0, 1).
							returnHour("sh_EXT[]", $data['StartHour'], 0)." : ".returnMinute("sm_EXT[]", $data['StartMinute'], 0)."
						</td>
					    <td>".
//							"<input type=\"text\" name=\"endtime_EXT[]\" id=\"endtime_EXT_$i\" value=\"".$data['EndDate']."\" />".
//							$linterface->GET_CALENDAR("form1", "endtime_EXT_$i").
							$linterface->GET_DATE_PICKER("endtime_EXT[]", $data['EndDate'], "", "yy-mm-dd", "", "", "", "", "endtime_EXT_$i", 0, 1).
							returnHour("eh_EXT[]", $data['EndHour'], 0)." : ".returnMinute("em_EXT[]", $data['EndMinute'], 0)."
						</td>
					    <td>
							<input type=\"checkbox\" name=\"allow_submit_EXT_$i\" id=\"allow_submit_EXT_$i\" ".($data['AllowSubmit']? "checked":"")." />
						</td>
				 	</tr>";
			$i++;	 	
		}
		$x .= "	</table>";
		############ End: Table for EXTERNAL ############	
		$x .= "</td></tr>";

//		$x .= "<tr>";
//		$x .= "<td colspan=\"2\">&nbsp;";
//		$x .= "</td></tr>";
//		$x .= "<tr><td class=\"dotline\" colspan=\"6\"><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"10\" height=\"1\"></td></tr>";	
//		$x .= "<tr>";
//		$x .= "<td>";
//		$x .= $linterface->GET_NAVIGATION2($iPort["OLESetSLPRecord"]);
//		############ Start: Table for OLE (Set Record For SLP Period)  ############
//		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
//		
//		// Start Time
//		$x .= "<tr>";
//		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\">";
//		$x .= "<span class=\"tabletext\">".$iPort["start_time"]."</span>".$sup3;
//		$x .= "</td>";
//		$x .= "<td valign=\"top\">";
//		$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
//		$x .= "<tr>";
//		$x .= "<td><input name=\"SetSLPPeriod_starttime\" type=\"text\" class=\"tabletext\" value=\"".$SetSLPPeriod_starttime."\" /></td>";
//		$x .= "<td width=\"25\" align=\"center\">";
//		$x .= $linterface->GET_CALENDAR("form1", "SetSLPPeriod_starttime");
//		$x .= "<td align=\"center\">";
//		$x .= returnHour('SetSLPPeriod_sh', $SetSLPPeriod_sh, 0)." : ".returnMinute('SetSLPPeriod_sm', $SetSLPPeriod_sm, 0);
//		$x .= "</td>";
//		$x .= "<td width=\"25\" align=\"center\"></td>";
//		$x .= "</tr>";
//		$x .= "</table>";
//		$x .= "</td>";
//		$x .= "</tr>";
//		
//		// End Time
//		$x .= "<tr>";
//		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\">";
//		$x .= "<span class=\"tabletext\">".$iPort["end_time"]."</span>".$sup4;
//		$x .= "</td>";
//		$x .= "<td valign=\"top\">";
//		$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
//		$x .= "<tr>";
//		$x .= "<td><input name=\"SetSLPPeriod_endtime\" type=\"text\" class=\"tabletext\" value=\"".$SetSLPPeriod_endtime."\" /></td>";
//		$x .= "<td width=\"25\" align=\"center\">";
//		$x .= $linterface->GET_CALENDAR("form1", "SetSLPPeriod_endtime");
//		$x .= "<td align=\"center\">";
//		$x .= returnHour('SetSLPPeriod_eh', $SetSLPPeriod_eh, 0)." : ".returnMinute('SetSLPPeriod_em', $SetSLPPeriod_em, 0);
//		$x .= "</td>";
//		$x .= "<td width=\"25\" align=\"center\"></td>";
//		$x .= "</tr>";
//		$x .= "</table>";
//		$x .= "</td>";
//		$x .= "</tr>";
//		
//		// Internal / external submit type
//		$x .= "<tr>";
//		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\">";
//		$x .= "<span class=\"tabletext\">&nbsp;</span>";
//		$x .= "</td>";
//		$x .= "<td valign=\"top\">";
//		$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
//		$x .= "<tr>";
//		$x .= "<td align=\"center\">";
//		$x .= "<input type='checkbox' name='check_SetSLPPeriod' id='check_SetSLPPeriod' $check_SetSLPPeriod /><label for='check_SetSLPPeriod'>".$iPort["allow_to_submit"]."</label>";
//		$x .= "</td>";
//		$x .= "</tr>";
//		$x .= "</table>";
//		$x .= "</td>";
//		$x .= "</tr>";
//		$x .= "</table>";
//		############ End: Table for OLE (Set Record For SLP Period) ############		
//		$x .= "</td></tr>";	

		$x .= "<tr><td colspan=\"2\">&nbsp;</td></tr>";
		$x .= "<tr><td class=\"dotline\" colspan=\"6\"><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"10\" height=\"1\"></td></tr>";	
		
		$x .= "<tr><td>";
		############ Start: Table for OLE (Set Record For SLP Period)  ############
		$x .= $linterface->GET_NAVIGATION2($iPort["OLESetSLPRecord"]);
		
		$x .= "<table class=\"common_table_list_v30 edit_table_list_v30\" width=\"80%\" cellspacing=\"0\" cellpadding=\"5\">
					<tr>
						<th>".$i_ClassLevelName."</th>
						<th>".$iPort["start_time"].$sup1."</th>
						<th>".$iPort["end_time"].$sup2."</th>
						<th>".$iPort["allow_to_submit"]."</th>
					</tr>
					<tr class=\"edit_table_head_bulk\">
	                    <th>&nbsp;</th>
	                    <th>
							<div style=\"float:left;\">".
//								"<input type=\"text\" name=\"starttime_SLP\" id=\"starttime_SLP\" value=\"".date("Y-m-d")."\" />".
//								$linterface->GET_CALENDAR("form1", "starttime_SLP").
								$linterface->GET_DATE_PICKER("starttime_SLP", $today, "", "yy-mm-dd").
								returnHour("sh_SLP", "", 0)." : ".returnMinute("sm_SLP", "", 0).
							"</div>".
							$linterface->Get_Apply_All_Icon("javascript:SetAllTime('SLP', 'start', '".count($ParArr['SLP'])."');")."
						</th>
	                    <th>
							<div style=\"float:left;\">".
//								"<input type=\"text\" name=\"endtime_SLP\" id=\"endtime_SLP\" value=\"".date("Y-m-d")."\" />".
//								$linterface->GET_CALENDAR("form1", "endtime_SLP").
								$linterface->GET_DATE_PICKER("endtime_SLP", $today, "", "yy-mm-dd").
								returnHour("eh_SLP", "", 0)." : ".returnMinute("em_SLP", "", 0).
							"</div>".
							$linterface->Get_Apply_All_Icon("javascript:SetAllTime('SLP', 'end', '".count($ParArr['SLP'])."');")."
	                    </th>
	                    <th>
							<input type=\"checkbox\" name=\"allow_submit_SLP\" id=\"allow_submit_SLP\" onclick=\"checkAll_allow_submit('SLP', '".count($ParArr['SLP'])."', this.checked)\"/>
	                    </th>
					</tr>";
		
		$i = 0;
		foreach($ParArr['SLP'] as $data)
		{
			$x .= "	<tr>
						<td>
							<span>".$data['ClassLevel']['LevelName']."</span>
							<input type=\"hidden\" name=\"LevelID_SLP[]\" id=\"LevelID_SLP_$i\" value=".$data['ClassLevel']['ClassLevelID'].">
						</td>
				    	<td>".
//							"<input type=\"text\" name=\"starttime_SLP[]\" id=\"starttime_SLP_$i\" value=\"".$data['StartDate']."\" />".
//							$linterface->GET_CALENDAR("form1", "starttime_SLP_$i").
							$linterface->GET_DATE_PICKER($Name="starttime_SLP[]", $data['StartDate'], "", "yy-mm-dd", "", "", "", "", "starttime_SLP_$i", 0, 1).
							returnHour("sh_SLP[]", $data['StartHour'], 0)." : ".returnMinute("sm_SLP[]", $data['StartMinute'], 0)."
						</td>
					    <td>".
//							"<input type=\"text\" name=\"endtime_SLP[]\" id=\"endtime_SLP_$i\" value=\"".$data['EndDate']."\" />".
//							$linterface->GET_CALENDAR("form1", "endtime_SLP_$i").
							$linterface->GET_DATE_PICKER($Name="endtime_SLP[]", $data['EndDate'], "", "yy-mm-dd", "", "", "", "", "endtime_SLP_$i", 0, 1).
							returnHour("eh_SLP[]", $data['EndHour'], 0)." : ".returnMinute("em_SLP[]", $data['EndMinute'], 0)."
						</td>
					    <td>
							<input type=\"checkbox\" name=\"allow_submit_SLP_$i\" id=\"allow_submit_SLP_$i\" ".($data['AllowSubmit']? "checked":"")." />
						</td>
				 	</tr>";
		$i++;
		}
		$x .= "	</table>";
		############ End: Table for OLE (Set Record For SLP Period) ############	
		$x .= "</td></tr>";

		// dot line
		$x .= "<tr>";
		$x .= "<td height=\"1\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td>";
		$x .= "</tr>";
		
		// submit button
		$x .= "<tr>";
		$x .= "<td align=\"right\">";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr><td align=\"left\">";
		$x .= $sup1." ".$iPort["msg"]["start_time"]."<br/>";
		$x .= $sup2." ".$iPort["msg"]["end_time"]."<br/>";
		$x .= $sup3." ".$iPort["msg"]["SetSLPRecord_start_time"]."<br/>";
		$x .= $sup4." ".$iPort["msg"]["SetSLPRecord_end_time"]."<br/><br/>";
		$x .= "</td></tr>";
		$x .= "<tr>";
		$x .= "<td align=\"center\">";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "button", "jSubmit();", "btnSubmit");
		$x .= "&nbsp;";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["reset"], "button", "jReset();", "btnReset");
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td></tr>";
		
		$x .= "</table>";
		$x .= "</form>";
		
		return $x;
	} // end function get submission period form
	
	function getTwghczmSubmissionPeriodFrom($ParArr="")
	{
		global $linterface, $iPort, $i_ClassLevelName;
		
		$today = date('Y-m-d');
	
//		$ParArr['TWGHCZM'] = $ParArr['TWGHCZM']==''?array():$ParArr['TWGHCZM'];
		
		$x = "";
		$x .= "<form name=\"form1\" action=\"update_class_teacher_personal_character_period.php\" method=\"post\">";
		$x .= "<table width=\"80%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";

		$x .= "<tr><td>";
		############ Start: Table for INTERNAL ############
		$x .= $linterface->GET_NAVIGATION2($iPort["Personal_character_record_submision_period"]);
		
		$x .= "<table class=\"common_table_list_v30 edit_table_list_v30\" cellspacing=\"0\" cellpadding=\"5\">
					<tr>
						<th>".$i_ClassLevelName."</th>
						<th>".$iPort["start_time"]."</th>
						<th>".$iPort["end_time"]."</th>
						<th>".$iPort["allow_to_submit"]."</th>
					</tr>
					<tr class=\"edit_table_head_bulk\">
	                	<th>&nbsp;</th>
	                	<th>
							<div style=\"float:left;\">".
							
		$linterface->GET_DATE_PICKER("starttime_TWGHCZM", $today, "", "yy-mm-dd").
		returnHour("sh_TWGHCZM", "", 0)." : ".returnMinute("sm_TWGHCZM", "", 0).
		"</div>".
		$linterface->Get_Apply_All_Icon("javascript:SetAllTime('TWGHCZM', 'start', '".count($ParArr['TWGHCZM'])."');")."
						</th>
	                    <th>
							<div style=\"float:left;\">".
		$linterface->GET_DATE_PICKER("endtime_TWGHCZM", $today, "", "yy-mm-dd").
		returnHour("eh_TWGHCZM", "", 0)." : ".returnMinute("em_TWGHCZM", "", 0).
		"</div>".
		$linterface->Get_Apply_All_Icon("javascript:SetAllTime('TWGHCZM', 'end', '".count($ParArr['TWGHCZM'])."');")."
	                    </th>
	                    <th>
							<input type=\"checkbox\" name=\"allow_submit_TWGHCZM\" id=\"allow_submit_TWGHCZM\" onclick=\"checkAll_allow_submit('TWGHCZM', '".count($ParArr['TWGHCZM'])."', this.checked)\"/>
	                    </th>
					</tr>";

		$i = 0;
		foreach($ParArr['TWGHCZM'] as $data)
		{
			$x .= "	<tr>
						<td>
							<span>".$data['ClassLevel']['LevelName']."</span>
							<input type=\"hidden\" name=\"LevelID_TWGHCZM[]\" id=\"LevelID_TWGHCZM_$i\" value=".$data['ClassLevel']['ClassLevelID'].">
						</td>
				    	<td>".

			$linterface->GET_DATE_PICKER("starttime_TWGHCZM[]", $data['StartDate'], "", "yy-mm-dd", "", "", "", "", "starttime_TWGHCZM_$i", 0, 1).
			returnHour("sh_TWGHCZM[]", $data['StartHour'], 0)." : ".returnMinute("sm_TWGHCZM[]", $data['StartMinute'], 0)."
						</td>
					    <td>".
			$linterface->GET_DATE_PICKER("endtime_TWGHCZM[]", $data['EndDate'], "", "yy-mm-dd", "", "", "", "", "endtime_TWGHCZM_$i", 0, 1).
			returnHour("eh_TWGHCZM[]", $data['EndHour'], 0)." : ".returnMinute("em_TWGHCZM[]", $data['EndMinute'], 0)."
			</td>
			<td>
			<input type=\"checkbox\" name=\"allow_submit_TWGHCZM_$i\" id=\"allow_submit_TWGHCZM_$i\" ".($data['AllowSubmit']? "checked":"")." />
						</td>
				 	</tr>";
			$i++;
		}
		$x .= "	</table>";
		############ End: Table for INTERNAL ############
		$x .= "</td></tr>";
		
		// dot line
		$x .= "<tr>";
		$x .= "<td height=\"1\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td>";
		$x .= "</tr>";
		
		// submit button
		$x .= "<tr>";
		$x .= "<td align=\"right\">";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td align=\"center\">";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "button", "jSubmit();", "btnSubmit");
		$x .= "&nbsp;";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["reset"], "button", "jReset();", "btnReset");
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td></tr>";
		
		$x .= "</table>";
		$x .= "</form>";
		
		return $x;
	} // end function get submission period form
	
	function GET_RECORD_APPROVAL_FORM($ParArr="", $IsEditable=0)
	{
		global $linterface, $iPort, $ec_iPortfolio,$ipf_cfg;
		
		$ApprovalSetting = $ParArr["ApprovalSetting"];
		
		$check0 = "";
		$check1 = "";
		$check2 = "";
		if ($IsEditable) {
			$allowEditCheck = " checked = 'checked' ";
		} else {
			$allowEditCheck = "";
		}
		
		if($ApprovalSetting==$ipf_cfg["RECORD_APPROVAL_SETTING"]["studentSelectApprovalTeacher"] && $ApprovalSetting != "")
		$check1 = "checked";
		else if($ApprovalSetting==$ipf_cfg["RECORD_APPROVAL_SETTING"]["noApprovalNeeded"] && $ApprovalSetting != "")
		$check2 = "checked";
		else
		$check0 = "checked";
		
		$x = "";
		$x .= "<form name=\"form1\" action=\"update_record_approval.php\" method=\"post\">";
		$x .= "<table width=\"88%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
		
		$x .= "<tr><td>";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
		// option select
		$x .= "<tr>";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\">";
		$x .= "<span class=\"tabletext\">".$iPort["options_select"]."</span></td>";
		$x .= "<td width=\"80%\" valign=\"top\">";
		$x .= "<TABLE cellSpacing=\"0\" cellPadding=\"2\" border=\"0\">";
		$x .= "<TBODY>";
		$x .= "<TR>";
		$x .= "<TD noWrap><INPUT id=\"setting0\" type=\"radio\" onClick='javascript: setAllowEditDisplay();' value=\"".$ipf_cfg["RECORD_APPROVAL_SETTING"]["onlyTeachersAdminsWithApprovalRight"]."\" name=\"ApprovalSetting\" ".$check0.">";
		$x .= "<LABEL for=\"setting0\">".$ec_iPortfolio['record_approval_setting_default']."</LABEL></TD>";
		$x .= "</TR>";
		$x .= "<TR>";
		$x .= "<TD noWrap><INPUT id=\"setting1\" type=\"radio\" onClick='javascript: setAllowEditDisplay();' value=\"".$ipf_cfg["RECORD_APPROVAL_SETTING"]["studentSelectApprovalTeacher"]."\" name=\"ApprovalSetting\" ".$check1.">";
		$x .= "<LABEL for=\"setting1\">".$ec_iPortfolio['record_approval_setting_selective']."</LABEL></TD>";
		$x .= "</TR>";
		$x .= "<TR>";
		$x .= "<TD noWrap>";
		$x .= "<INPUT id=\"setting2\" type=\"radio\" onClick='javascript: setAllowEditDisplay();' value=\"".$ipf_cfg["RECORD_APPROVAL_SETTING"]["noApprovalNeeded"]."\" name=\"ApprovalSetting\" ".$check2.">";
		$x .= "<LABEL for=\"setting2\">".$ec_iPortfolio['record_approval_setting_auto']."</LABEL>";
		$x .= "<br /><span id='allowEditSpan'>&nbsp;&nbsp;&nbsp;<input type='checkbox' id='allowEdit' name='allowEdit' value='1' $allowEditCheck />
				<label for='allowEdit'>".$ec_iPortfolio['record_approval_setting_editable']."</label></span>";
		$x .= "</TD>";
		$x .= "</TR>";
		$x .= "<TR>";
		$x .= "</TBODY>";
		$x .= "</TABLE>";
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td></tr>";
		
		// dot line
		$x .= "<tr>";
		$x .= "<td height=\"1\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td>";
		$x .= "</tr>";
		
		// submit button
		$x .= "<tr>";
		$x .= "<td align=\"right\">";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td align=\"center\">";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "button", "jSubmit();", "btnSubmit");
		$x .= "&nbsp;";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["reset"], "button", "jReset();", "btnReset");
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td></tr>";
		
		$x .= "</table>";
		$x .= "</form>";
		return $x;
	}

##########################################################################
##############################  TWGHCZM Report  ##############################
##########################################################################
	
	# Get OLE year list
	function GET_OLE_YEAR_LIST(){
		global $eclass_db, $intranet_db;

		# Get dates of all OLE records
		$sql =	"
							SELECT DISTINCT
								os.StartDate
							FROM
								{$eclass_db}.PORTFOLIO_STUDENT as ps
								INNER JOIN
								{$eclass_db}.OLE_STUDENT as os
								ON
								ps.UserID = os.UserID
							Where
								os.StartDate != '0000-00-00'
						";
		$StartDateArr = $this->returnVector($sql);
		sort($StartDateArr);

		# Calculate years according to available OLE dates
		$ReturnArr = array();
		foreach($StartDateArr as $StartDate)
		{
			if($StartDate != "")
			{
				list($year, $month) = explode("-", $StartDate);
				# If the date is earlier than September, include previous year
				if($month < $this->year_start["month"])
					$year_range = ($year-1)."-".$year;
				# If the date is earlier than September, include next year
				else
					$year_range = $year."-".($year+1);

				if(!in_array($year_range, $ReturnArr))
					$ReturnArr[] = $year_range;
			}
		}

		return $ReturnArr;
	}
	
	# Get OLE class level list
	function GET_OLE_CLASSLEVEL_LIST($ParYear="", $ParYearID=''){
		global $eclass_db, $intranet_db;
		
		$conds = '';
		if ($ParYearID != '')
		{
			$conds .= " And op.AcademicYearID = '".$ParYearID."' ";
		}
		else if($ParYear != "")
		{
			# Define date range for specific year
			$YearRange = explode("-", $ParYear);
			$FirstDate = date("Y-m-d", mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"], trim($YearRange[0])));
			$LastDate = date("Y-m-d", mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"]-1, trim($YearRange[1])));
			
			$conds .=	"
									AND os.StartDate >= '".$FirstDate."'
									AND os.StartDate <= '".$LastDate."'
								";
		}

		if(!$this->IS_IPF_SUPERADMIN())
		{
			$conds .= " AND (
                    yc.ClassTitleEN IN ('".implode("','", array_merge($this->GET_CLASS_TEACHER_CLASS(), $this->GET_SUBJECT_TEACHER_CLASS()))."') OR
                    yc.ClassTitleB5 IN ('".implode("','", array_merge($this->GET_CLASS_TEACHER_CLASS(), $this->GET_SUBJECT_TEACHER_CLASS()))."')
                  )
								";
		}

		$sql =	"
							SELECT DISTINCT
								y.YearName as ClassLevel
							FROM
								{$eclass_db}.PORTFOLIO_STUDENT as ps
						";
		if ($ParYear != "" || $ParYearID != '')
		{
			$sql .=	"
								LEFT JOIN {$eclass_db}.OLE_STUDENT as os
								  ON ps.UserID = os.UserID
							";
		}
		
		if ($ParYearID != '')
		{
			$sql .=	"
								LEFT JOIN {$eclass_db}.OLE_PROGRAM as op
								  ON os.ProgramID = op.ProgramID
							";
		}
		
		$sql .=	"
								INNER JOIN {$intranet_db}.INTRANET_USER as iu
				                	ON ps.UserID = iu.UserID
				              	INNER JOIN {$intranet_db}.YEAR_CLASS_USER as ycu
				               		ON iu.UserID = ycu.UserID
				              	INNER JOIN {$intranet_db}.YEAR_CLASS as yc
					                ON ycu.YearClassID = yc.YearClassID
								LEFT JOIN {$intranet_db}.YEAR as y
					                ON yc.YearID = y.YearID
							WHERE
								(y.YearName != '' AND y.YearName IS NOT NULL) AND
								yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
								$conds
							ORDER BY
								ClassLevel
						";
		$ReturnArr = $this->returnVector($sql);

		return $ReturnArr;
	}
	
	# Get OLE class list
	function GET_OLE_CLASS_LIST($ParYear="", $ParYearName="", $ParYearID=""){
		global $eclass_db, $intranet_db;

		if($ParYearName != "")
			$conds .= " AND y.YearName = '".$ParYearName."'";
			
		if ($ParYearID != "")
		{
			$conds .= " And op.AcademicYearID = '".$ParYearID."' ";
		}
		else if ($ParYear != "")
		{
			# Define date range for specific year
			$YearRange = explode("-", $ParYear);
			$FirstDate = date("Y-m-d", mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"], trim($YearRange[0])));
			$LastDate = date("Y-m-d", mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"]-1, trim($YearRange[1])));

			$conds .=	"
									AND os.StartDate >= '".$FirstDate."'
									AND os.StartDate <= '".$LastDate."'
								";
		}
		if(!$this->IS_IPF_SUPERADMIN())
		{
			$conds .= " AND (
                    yc.ClassTitleEN IN ('".implode("','", array_merge($this->GET_CLASS_TEACHER_CLASS(), $this->GET_SUBJECT_TEACHER_CLASS()))."') OR
                    yc.ClassTitleB5 IN ('".implode("','", array_merge($this->GET_CLASS_TEACHER_CLASS(), $this->GET_SUBJECT_TEACHER_CLASS()))."')
                  )
								";
		}

		$sql =	"
							SELECT DISTINCT
								iu.ClassName
							FROM
								{$eclass_db}.PORTFOLIO_STUDENT as ps
						";
		if ($ParYear != "" || $ParYearID != '')
		{
			$sql .=	"
								LEFT JOIN {$eclass_db}.OLE_STUDENT as os
								  ON ps.UserID = os.UserID
							";
		}
		
		if ($ParYearID != '')
		{
			$sql .=	"
								LEFT JOIN {$eclass_db}.OLE_PROGRAM as op
								  ON os.ProgramID = op.ProgramID
							";
		}
		$sql .=	"
								INNER JOIN {$intranet_db}.INTRANET_USER as iu
                				ON ps.UserID = iu.UserID
								INNER JOIN {$intranet_db}.YEAR_CLASS_USER as ycu
                				ON ycu.UserID = iu.UserID
              					INNER JOIN {$intranet_db}.YEAR_CLASS as yc
                				ON yc.YearClassID = ycu.YearClassID
						";
		if($ParYearName != "")
		{
			$sql .=	"
								LEFT JOIN {$intranet_db}.YEAR as y
                  				ON yc.YearID = y.YearID
							";
		}
		$sql .=	"
							WHERE
								(iu.ClassName != '' AND iu.ClassName IS NOT NULL) AND
								yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
								$conds
							ORDER BY
								iu.ClassName
						";
		$ReturnArr = $this->returnVector($sql);

		return $ReturnArr;
	}
	
	# Get ELE list
	function GET_OLE_ELE_LIST(){
		global $eclass_db, $intranet_db, $intranet_session_language;

		$TitleLang = ($intranet_session_language=="en")?"EngTitle":"ChiTitle";

		$sql =	"
							SELECT DISTINCT
								IF(DefaultID IS NOT NULL, DefaultID, RecordID) AS DefaultID,
								$TitleLang as Title,RecordID as RecordID
							FROM
								{$eclass_db}.OLE_ELE
							WHERE
								RecordStatus = 1 OR RecordStatus = 2 
						";
		$ReturnArr = $this->returnArray($sql);

		return $ReturnArr;
	}
	
	### (2008-09-03) Modified by key - Add count program num
	# Get OLE record list
	function GET_OLE_RECORD_LIST($ParStartYear="", $ParEndYear="", $ParYearName="", $ParClass="", $ParELE="", $ParHourRange="", $ParSearchName="", $ParField="", $ParOrder="", $ParCategory="", $ParStartYearID="", $ParEndYearID=""){
		global $eclass_db, $intranet_db, $intranet_session_language;
		
		if ($ParStartYearID != '')
		{
			$FirstDate = substr(getStartDateOfAcademicYear($ParStartYearID), 0, 10);
			$AcademicYearSQL =	"SELECT DISTINCT AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE TermStart >= '".$FirstDate."'";
		}
		else if($ParStartYear != "")
		{
			# Define date range for specific year
			$YearRange = explode("-", $ParStartYear);
			$FirstDate = date("Y-m-d", mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"], trim($YearRange[0])));

			
			$AcademicYearSQL =	"SELECT DISTINCT AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE TermStart >= '".$FirstDate."'";
// 			$ole_conds .=	" AND os.StartDate >= '".$FirstDate."'";
		}
		
		
		if ($ParEndYearID != '')
		{
			$LastDate = substr(getEndDateOfAcademicYear($ParEndYearID), 0, 10);
			$AcademicYearSQL .=	"AND TermEnd <= '".$LastDate."'";
			
// 			$ole_conds .=	" AND os.StartDate <= '".$LastDate."'";
		}
		else if($ParEndYear != "")
		{
			# Define date range for specific year
			$YearRange = explode("-", $ParEndYear);
			$LastDate = date("Y-m-d", mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"]-1, trim($YearRange[1])));
			$AcademicYearSQL .=	"AND TermEnd <= '".$LastDate."'";
// 			$ole_conds .=	" AND os.StartDate <= '".$LastDate."'";
		}
		

		
		$ole_conds .= " AND (os.RecordStatus = 2 OR os.RecordStatus = 4)";
		
		# Add ELE filter criteria
		$ele_conds = '';
		if($ParELE != "")
		{
			$ELEArr = is_array($ParELE) ? $ParELE : array($ParELE);
			if(count($ELEArr) > 0)
			{
				foreach($ELEArr as $ELE)
				{
					//$ole_sub_conds[] = "INSTR(os.ELE, '[".$ELE."]')";
					$ole_sub_conds[] = "INSTR(op.ELE, '[".$ELE."]')";
				}

				$ele_conds .= " AND (".implode(" OR ", $ole_sub_conds).")";
			}
		}
		
		$AcedemicYearIDAry = $this->returnVector($AcademicYearSQL);
		if(!empty($AcedemicYearIDAry)){
		    $ele_conds .=" AND op.AcademicYearID IN ('".(implode("','",$AcedemicYearIDAry))."')";
		}


		# Add class level filter criteria
		if($ParYearName != "")
			$conds[] = "y.YearName = '".$ParYearName."'";
		# Add class filter criteria
		if($ParClass != "")
			$conds[] = "iu.ClassName = '".$ParClass."'";
		# Search name
		if($ParSearchName != "")
			$conds[] =	"
										(iu.ChineseName LIKE '%".addslashes($ParSearchName)."%' OR
										iu.EnglishName LIKE '%".addslashes($ParSearchName)."%')
									";
									
									
		if(!$this->IS_IPF_SUPERADMIN())
		{
			$conds[] =	"
										yc.ClassTitleEN IN ('".implode("','", array_merge($this->GET_CLASS_TEACHER_CLASS(), $this->GET_SUBJECT_TEACHER_CLASS()))."')
									";
		}
		
		if($ParCategory != "")
		{
			$category_conds = " os.Category = '{$ParCategory}' ";
			$conds[] = $category_conds;
		}
		else
		$category_conds = "";

		if(is_array($conds))
			$conds = " WHERE ".implode(" AND ",$conds);

		if($ParField != 3 && $ParField != 4)
		{
			# Sorting criteria by parameter
			$SortField = ($ParField==2 ? array(1,2) : array($ParField));
			$SortOrder = ($ParField==2 ? explode("|", $ParOrder) : $ParOrder);
			for($i=0; $i<count($SortField); $i++)
			{
				switch($SortField[$i])
				{
					case 0:
						$displayBy .= "Name ";
						break;
					case 1:
						$displayBy .= "ClassName ";
						break;
					case 2:
						$displayBy .= "ClassNumber ";
						break;
				}
				if($SortOrder[$i] > 0)
					$displayBy .= "ASC, ";
				else
					$displayBy .= "DESC, ";
			}
			if($displayBy != "")
				$displayBy = "ORDER BY ".substr($displayBy, 0, -2);
		}

		$NameLang = ($intranet_session_language=="en")?"iu.EnglishName":"iu.ChineseName";
		
		$currentAcademicYearID = Get_Current_Academic_Year_ID();	// 201008310917
		$sql =	"
							SELECT
								iu.UserID,
								$NameLang as Name,
								iu.ClassName,
								iu.ClassNumber,
								os.Hours,
								os.ELE,
								os.RecordStatus,
								os.ProgramID
							FROM 
								{$eclass_db}.PORTFOLIO_STUDENT as ps
								LEFT JOIN {$eclass_db}.OLE_STUDENT as os
               					ON ps.UserID = os.UserID $ole_conds
								INNER JOIN {$intranet_db}.INTRANET_USER as iu
                				ON ps.UserID = iu.UserID
              					INNER JOIN {$intranet_db}.YEAR_CLASS_USER as ycu
                				ON iu.UserID = ycu.UserID
              					INNER JOIN {$intranet_db}.YEAR_CLASS as yc
                				ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = $currentAcademicYearID
                				INNER JOIN {$eclass_db}.OLE_PROGRAM as op 
                				ON op.ProgramID = os.ProgramID $ele_conds
						";
					
		if($ParYearName != "")
		{
			$sql .=	"
								LEFT JOIN {$intranet_db}.YEAR as y
                  				ON yc.YearID = y.YearID
							";
		}
		$sql .=	"
							$conds
							$displayBy
						";

						
		$OLRArr = $this->returnArray($sql);

		//debug_r($OLRArr);

		# Reorganize data into an associative array
		foreach($OLRArr as $OLR)
		{
			if($OLR['Name'] != "")
			{
//				$ReturnArr[$OLR['Name']]['UserID'] = $OLR['UserID'];
//				$ReturnArr[$OLR['Name']]['ClassName'] = $OLR['ClassName'];
//				$ReturnArr[$OLR['Name']]['ClassNumber'] = $OLR['ClassNumber'];
				$ReturnArr[$OLR['UserID']]['Name'] = $OLR['Name'];
				$ReturnArr[$OLR['UserID']]['ClassName'] = $OLR['ClassName'];
				$ReturnArr[$OLR['UserID']]['ClassNumber'] = $OLR['ClassNumber'];

				$ELEArr = explode(",", $OLR['ELE']);
				# Filter activities which does not belong to any ELE
				if(count(array_filter($ELEArr)) > 0)
				{
					foreach($ELEArr as $ELE)
					{
//						$ReturnArr[$OLR['Name']][trim(str_replace(array("[","]"),"",$ELE))] += $OLR['Hours'];
						$ReturnArr[$OLR['UserID']][trim(str_replace(array("[","]"),"",$ELE))] += $OLR['Hours'];
					}
//					$ReturnArr[$OLR['Name']]['TotalHour'] += $OLR['Hours'];
					$ReturnArr[$OLR['UserID']]['TotalHour'] += $OLR['Hours'];
				}

				if($OLR['ProgramID'] != "" && $OLR['ProgramID'] != NULL)
				{
					if($ReturnArr[$OLR['UserID']]['ProgramNum'] == "")
					$ReturnArr[$OLR['UserID']]['ProgramNum'] = 1;
					else
					$ReturnArr[$OLR['UserID']]['ProgramNum'] += 1;
				}
			}
		}

		# Add hours range filter criteria
		if($ParHourRange != "")
		{
			if(is_array($ReturnArr))
			{
				switch($ParHourRange[0])
				{
					case 0:
						foreach($ReturnArr as $UserID => $OLEArr)
						{
							if($OLEArr['TotalHour'] >= $ParHourRange[1] && $OLEArr['TotalHour'] <= $ParHourRange[2])
								$NewReturnArr[$UserID] = $OLEArr;
						}
						$ReturnArr = $NewReturnArr;
						unset($NewReturnArr);
						break;
					case 1:
						foreach($ReturnArr as $UserID => $OLEArr)
						{
							if($OLEArr['TotalHour'] >= $ParHourRange[1])
								$NewReturnArr[$UserID] = $OLEArr;
						}
						$ReturnArr = $NewReturnArr;
						unset($NewReturnArr);
						break;
					case -1:
						foreach($ReturnArr as $UserID => $OLEArr)
						{
							if($OLEArr['TotalHour'] <= $ParHourRange[1])
								$NewReturnArr[$UserID] = $OLEArr;
						}
						$ReturnArr = $NewReturnArr;
						unset($NewReturnArr);
						break;
				}
			}
		}

		# Sorting criteria by parameter
		if($ParField == 3)
		{
			if(is_array($ReturnArr))
			{
				foreach($ReturnArr as $UserID => $StudentArr)
				{
					$ProgramNumArr[$UserID] = $StudentArr['ProgramNum'];
					$ClassNameArr[$UserID] = $StudentArr['ClassName'];
					$ClassNumberArr[$UserID] = $StudentArr['ClassNumber'];
					$UserIDArr[$UserID] = $UserID;
				}

				# Sort by program number, class name and class number
				array_multisort($ProgramNumArr, SORT_NUMERIC, ($ParOrder>0?SORT_ASC:SORT_DESC),
												$ClassNameArr, SORT_STRING, SORT_ASC,
												$ClassNumberArr, SORT_NUMERIC, SORT_ASC, $UserIDArr);

				# Reorder the return array with reference to the sorted UserID
				if(is_array($UserIDArr))
				{
					foreach($UserIDArr as $UserID)
					{
						$NewReturnArr[$UserID] = $ReturnArr[$UserID];
					}
					$ReturnArr = $NewReturnArr;
				}
			}
		}
		# Sorting criteria by parameter
		if($ParField == 4)
		{
			if(is_array($ReturnArr))
			{
				foreach($ReturnArr as $UserID => $StudentArr)
				{
					$TotalHourArr[$UserID] = $StudentArr['TotalHour'];
					$ClassNameArr[$UserID] = $StudentArr['ClassName'];
					$ClassNumberArr[$UserID] = $StudentArr['ClassNumber'];
					$UserIDArr[$UserID] = $UserID;
				}

				# Sort by total hour, class name and class number
				array_multisort($TotalHourArr, SORT_NUMERIC, ($ParOrder>0?SORT_ASC:SORT_DESC),
												$ClassNameArr, SORT_STRING, SORT_ASC,
												$ClassNumberArr, SORT_NUMERIC, SORT_ASC, $UserIDArr);

				# Reorder the return array with reference to the sorted UserID
				if(is_array($UserIDArr))
				{
					foreach($UserIDArr as $UserID)
					{
						$NewReturnArr[$UserID] = $ReturnArr[$UserID];
					}
					$ReturnArr = $NewReturnArr;
				}
			}
		}

		return $ReturnArr;
	}
	
	# Get ELE Details
	function GET_ELE_LIST(){
		global $eclass_db;
		
		$sql =	"
							SELECT
								RecordID,
								IF(DefaultID IS NULL, CONCAT('[', RecordID,']'), DefaultID) AS DefaultID
							FROM
								{$eclass_db}.OLE_ELE
							WHERE
								RecordStatus = 1 OR
								RecordStatus = 2
						";
		$ReturnArr = $this->returnArray($sql);

		return $ReturnArr;
	}
	
	function getOLRCategoryFieldForRecord($prefix="", $ParShowAllCat=false)
	{
		$DefaultCategoryArray = $this->GET_OLR_CATEGORY(0, $ParShowAllCat);
		if(!empty($DefaultCategoryArray))
		{
			//ELSE '".$ec_iPortfolio['type_others']."'
			$CategoryField = "CASE ".$prefix."Category ";
			foreach($DefaultCategoryArray as $CategoryID => $CategoryTitle)
			{
				$CategoryField .= " WHEN ".$CategoryID." THEN '".$CategoryTitle."'";
			}
			$CategoryField .= " END";
		}

		return $CategoryField;
	}
	
	function GET_OLE_RECORD($StudentID="", $Year="", $IntExt="Int")
	{
		global $eclass_db, $intranet_db, $ec_iPortfolio;

		if($Year!="")
		{
			$SplitArray = explode("-", $Year);
			if(sizeof($SplitArray)>1)
			{
        # Eric Yip (20091202): Check for dummy date
  			if($SplitArray[0] == "0000")
  			{
          $LowerDate = 0;
          $UpperDate = 0;
        }
        else
        {
  				$LowerDate = mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"], trim($SplitArray[0]));
  				$UpperDate = mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"]-1, trim($SplitArray[1]));
				}
			}
			else
			{
				$LowerDate = mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"], $Year);
				$UpperDate = mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"]-1, $Year+1);
			}
			$cond = " AND UNIX_TIMESTAMP(a.StartDate) BETWEEN ".$LowerDate." AND ".$UpperDate;
		}

		$namefield = getNameFieldWithClassNumberByLang("b.");
		$CategoryField = $this->getOLRCategoryFieldForRecord("a.");
		$sql = "SELECT
					IF ((a.StartDate IS NULL OR a.StartDate='0000-00-00'),'--',IF(a.EndDate IS NULL OR a.EndDate='0000-00-00',a.StartDate,CONCAT(a.StartDate,'|', a.EndDate))) as OLEDate,
					a.Title,
					$CategoryField,
					a.Role,
					a.Hours,
					a.Achievement,
					c.Details,
					$namefield as ApproveBy,
					IF (a.ProcessDate IS NOT NULL, DATE_FORMAT(a.ProcessDate,'%Y-%m-%d'),'--'),
					a.Remark,
					a.ModifiedDate,
					a.StartDate,
					a.ele,
					a.Organization,
					a.RecordID,
					a.SLPOrder
				FROM
					{$eclass_db}.OLE_PROGRAM as c,
					{$eclass_db}.OLE_STUDENT as a
					LEFT JOIN {$intranet_db}.INTRANET_USER as b ON a.ApprovedBy = b.UserID
				WHERE
					a.UserID = '$StudentID'
					AND (a.RecordStatus = '2' || a.RecordStatus = '4')
					AND c.ProgramID  = a.ProgramID
					AND c.IntExt = '$IntExt'
					$cond
				ORDER BY
					a.StartDate DESC
				";
		$row = $this->returnArray($sql);

		return $row;
	}
	
	### (2008-09-03) Modified by Key add count program num
	# Get OLE record list grouped by year for a student
	function GET_OLE_YEAR_RECORD_LIST($ParUserID, $ParField="", $ParOrder="")
	{
		global $eclass_db,$intranet_db,$sys_custom;

		/*
		# Get ELE list
		$ELE_list = $this->GET_ELE_LIST();
		for($i=0; $i<count($ELE_list); $i++)
		{
			$ole_sub_conds[] = "INSTR(os.ELE, '".$ELE_list[$i]['DefaultID']."')";
		}
		if(is_array($ole_sub_conds))
			$ole_conds .= " AND (".implode(" OR ", $ole_sub_conds).")";
			
		$sql =	"
							SELECT
								op.AcademicYearID,
								op.Title,
								ay.YearNameEN,
								os.StartDate,
								os.Hours,
								os.ELE,
								os.ProgramID
							FROM
								{$eclass_db}.PORTFOLIO_STUDENT as ps
							LEFT JOIN
								{$eclass_db}.OLE_STUDENT as os
							ON
								ps.UserID = os.UserID AND
								(os.RecordStatus = 2 OR os.RecordStatus = 4)
								$ole_conds
							INNER JOIN
								{$eclass_db}.OLE_PROGRAM as op
							ON
								op.ProgramID = os.ProgramID 
							LEFT JOIN
								{$intranet_db}.ACADEMIC_YEAR as ay
							ON
								ay.AcademicYearID = op.AcademicYearID	
							WHERE
								ps.UserID = '".$ParUserID."'
							ORDER BY
								os.StartDate DESC
						";
		*/
		# Get ELE list
		$ELE_list = $this->GET_ELE_LIST();
		for($i=0; $i<count($ELE_list); $i++)
		{
			$ole_sub_conds[] = "INSTR(op.ELE, '".$ELE_list[$i]['DefaultID']."')";
		}
		if(is_array($ole_sub_conds))
			$ole_conds .= " AND (".implode(" OR ", $ole_sub_conds).")";
			
		$sql =	"
							SELECT
								op.AcademicYearID,
								op.Title,
								ay.YearNameEN,
								os.StartDate,
								os.Hours,
								op.ELE,
								os.ProgramID
							FROM
								{$eclass_db}.PORTFOLIO_STUDENT as ps
							LEFT JOIN
								{$eclass_db}.OLE_STUDENT as os
							ON
								ps.UserID = os.UserID AND
								(os.RecordStatus = 2 OR os.RecordStatus = 4)
							INNER JOIN
								{$eclass_db}.OLE_PROGRAM as op
							ON
								op.ProgramID = os.ProgramID 
								$ole_conds
							LEFT JOIN
								{$intranet_db}.ACADEMIC_YEAR as ay
							ON
								ay.AcademicYearID = op.AcademicYearID	
							WHERE
								ps.UserID = '".$ParUserID."'
							ORDER BY
								os.StartDate DESC
						";
		$OLRArr = $this->returnArray($sql);

		# Reorganize data into an associative array
		foreach($OLRArr as $OLR)
		{
			if($sys_custom['iPf']['StPaulCoeduPriSchool']['Report']['StudentLearningProfile']){
				if($OLR['AcademicYearID'] != "")
				{
					$Year = $OLR['YearNameEN'];
					$ELEArr = explode(",", $OLR['ELE']);
					# Filter activities which does not belong to any ELE
					if(count(array_filter($ELEArr)) > 0)
					{
						foreach($ELEArr as $ELE)
						{
							$ReturnArr[$Year][trim(str_replace(array("[","]"),"",$ELE))] += $OLR['Hours'];
							$ReturnArr[$Year][trim(str_replace(array("[","]"),"",$ELE)).'_Activities'][] = '<tr><td nowrap="nowrap" align="left">'.(count($ReturnArr[$Year][trim(str_replace(array("[","]"),"",$ELE)).'_Activities'])+1).')&nbsp;</td><td nowrap="nowrap" align="left">'.$OLR['Title'].'&nbsp;</td><td nowrap="nowrap" align="right">'.$OLR['Hours'].'</td></tr>';
						}
						$ReturnArr[$Year]['TotalHour'] += $OLR['Hours'];
	//					if ($Year == "2009 - 2010") {
	//						echo $ELE ." : " . $OLR['Hours']." : " . $ReturnArr[$Year]['TotalHour'] . "<br/>";
	//						debug_r($ReturnArr[$Year][trim(str_replace(array("[","]"),"",$ELE))]);
	//						echo "<hr/>";
	//					}
					}
	
					if($OLR['ProgramID'] != "" && $OLR['ProgramID'] != NULL)
					{
						if($ReturnArr[$Year]['ProgramNum'] == "")
							$ReturnArr[$Year]['ProgramNum'] = 1;
						else
							$ReturnArr[$Year]['ProgramNum'] += 1;
					}
				}
			}else{
				if($OLR['StartDate'] != "")
				{
					list($yr, $mth, $d) = explode("-", $OLR['StartDate']);
					$FirstDate = $yr."-09-01";
	
					
// 					if($sys_custom['iPf']['OLE_Report']['StartDateMappingByYearName']){
					$Year = $OLR['YearNameEN'];
// 					}else{
// 					    if($OLR['StartDate'] < $FirstDate)
// 					        $Year = ($yr-1)." - ".$yr;
// 					    else
// 					        $Year = $yr." - ".($yr+1);
// 					}

					$ELEArr = explode(",", $OLR['ELE']);
					# Filter activities which does not belong to any ELE
					if(count(array_filter($ELEArr)) > 0)
					{
						foreach($ELEArr as $ELE)
						{
							$ReturnArr[$Year][trim(str_replace(array("[","]"),"",$ELE))] += $OLR['Hours'];
						}
						$ReturnArr[$Year]['TotalHour'] += $OLR['Hours'];
	//					if ($Year == "2009 - 2010") {
	//						echo $ELE ." : " . $OLR['Hours']." : " . $ReturnArr[$Year]['TotalHour'] . "<br/>";
	//						debug_r($ReturnArr[$Year][trim(str_replace(array("[","]"),"",$ELE))]);
	//						echo "<hr/>";
	//					}
					}
	
					if($OLR['ProgramID'] != "" && $OLR['ProgramID'] != NULL)
					{
						if($ReturnArr[$Year]['ProgramNum'] == "")
							$ReturnArr[$Year]['ProgramNum'] = 1;
						else
							$ReturnArr[$Year]['ProgramNum'] += 1;
					}
				}
			}
		}

		if(is_array($ReturnArr))
		{
			# Sorting criteria by parameter
			switch($ParField)
			{
				case 0:
					if($ParOrder > 0)
						ksort($ReturnArr);
					else
						krsort($ReturnArr);
					break;
				case 1:
					foreach($ReturnArr as $Year => $OLEArr)
					{
						$ProgramNumArr[$Year] = $OLEArr['ProgramNum'];
						$YearArr[$Year] = $Year;
					}

					# Sort by program number, class name and class number
					array_multisort($ProgramNumArr, SORT_NUMERIC, ($ParOrder>0?SORT_ASC:SORT_DESC), $YearArr);

					# Reorder the return array with reference to the sorted UserID
					if(is_array($YearArr))
					{
						foreach($YearArr as $Year)
						{
							$NewReturnArr[$Year] = $ReturnArr[$Year];
						}
						$ReturnArr = $NewReturnArr;
					}

					break;
				case 2:
					foreach($ReturnArr as $Year => $OLEArr)
					{
						$TotalHourArr[$Year] = $OLEArr['TotalHour'];
						$YearArr[$Year] = $Year;
					}

					# Sort by program number, class name and class number
					array_multisort($TotalHourArr, SORT_NUMERIC, ($ParOrder>0?SORT_ASC:SORT_DESC), $YearArr);

					# Reorder the return array with reference to the sorted UserID
					if(is_array($YearArr))
					{
						foreach($YearArr as $Year)
						{
							$NewReturnArr[$Year] = $ReturnArr[$Year];
						}
						$ReturnArr = $NewReturnArr;
					}

					break;
			}
		}

		return $ReturnArr;
	}
	
	# Generate selection list for OLE year 
	function GEN_OLE_YEAR_SELECTION($ParYear="", $ParTag="", $ParAllowAll=true)
	{
		global $ec_iPortfolio, $PATH_WRT_ROOT, $i_Attendance_AllYear;
	
		$YearArr = $this->GET_OLE_YEAR_LIST();
		
		for($i=0; $i<count($YearArr); $i++)
		{
			$YearArr[$i] = array($YearArr[$i], $YearArr[$i]);
		}
	
		if($ParAllowAll)
			$ReturnStr = getSelectByArrayTitle($YearArr, $ParTag, $ec_iPortfolio['school_yr'], $ParYear, false, 2);
		else
			$ReturnStr = getSelectByArray($YearArr, $ParTag, $ParYear, 0, 1, "", 2);
		
		return $ReturnStr;
	}
	
	# Generate checkbox list for OLE ELE
	function GEN_OLE_ELE_CHECKLIST()
	{
		global $ec_iPortfolio;
	
		$ELEArr = $this->GET_OLE_ELE_LIST();
		
		for($i=0; $i<count($ELEArr); $i++)
		{
			$ELEArr[$i] = "<label for=\"ele_$i\" class=\"tabletext\"><input type=\"checkbox\" name=\"ELE[]\" id=\"ele_$i\" value=\"".str_replace(array("[","]"), "", $ELEArr[$i]['DefaultID'])."\"> ".$ELEArr[$i]['Title']."</label>";
		}
	
		if(is_array($ELEArr))
			$ReturnStr = implode("<br />\n", $ELEArr);
		
		return $ReturnStr;
	}
	
	# Generate selection list for OLE class level
	function GEN_OLE_CLASSLEVEL_SELECTION($ParYear="", $ParClassLevel="", $ParTag="", $ParYearID="")
	{
		global $ec_iPortfolio;
	
		$ClassLevelArr = $this->GET_OLE_CLASSLEVEL_LIST($ParYear, $ParYearID);
		
		for($i=0; $i<count($ClassLevelArr); $i++)
		{
			$ClassLevelArr[$i] = array($ClassLevelArr[$i], $ClassLevelArr[$i]);
		}
	
		$ReturnStr = getSelectByArrayTitle($ClassLevelArr, $ParTag, $ec_iPortfolio['all_classlevel'], $ParClassLevel, false, 2);
		
		return $ReturnStr;
	}
	
	# Generate selection list for OLE class
	function GEN_OLE_CLASS_SELECTION($ParYear="", $ParClassLevel="", $ParClass="", $ParYearID="")
	{
		global $ec_iPortfolio;
	
		$ClassArr = $this->GET_OLE_CLASS_LIST($ParYear, $ParClassLevel, $ParYearID);
		
		for($i=0; $i<count($ClassArr); $i++)
		{
			$ClassArr[$i] = array($ClassArr[$i], $ClassArr[$i]);
		}
	
		$ReturnStr = getSelectByArrayTitle($ClassArr, "name='Class' onChange=\"jCHANGE_FIELD('class')\"", $ec_iPortfolio['all_class'], $ParClass, false, 2);
		
		return $ReturnStr;
	}
	
	# Generate selection list for OLE ELE
	function GEN_OLE_ELE_SELECTION($ParELE="")
	{
		global $ec_iPortfolio;
	
		$ELEArr = $this->GET_OLE_ELE_LIST();
		
		for($i=0; $i<count($ELEArr); $i++)
		{
			$ELEArr[$i] = array(str_replace(array("[","]"), "", $ELEArr[$i]['DefaultID']), $ELEArr[$i]['Title']);
		}
	
		$ReturnStr = getSelectByArrayTitle($ELEArr, "name='ELE' onChange=\"jCHANGE_FIELD('ele')\"", $ec_iPortfolio['all_ele'], $ParELE, false, 2);
		
		return $ReturnStr;
	}
	
	### (2008-09-03) Modified by key - update the table display layout
	# Generate OLE statistic table for display
	function GEN_OLE_STATISTIC_TABLE($ParPageInfoArr, $ParStartYear="", $ParEndYear="", $ParClassLevel="", $ParClass="", $ParELE="", $ParHourRange="", $ParSearchName="", $ParField="", $ParOrder="", $ParCategory="", $ParStartYearID="", $ParEndYearID="")
	{
		global $i_UserClassNumber, $i_UserClassName, $i_UserName, $ec_iPortfolio, $no_record_msg;
		global $image_path, $LAYOUT_SKIN, $iPort;

		# Get ELE list
		$ELE_list = $this->GET_OLE_ELE_LIST();
		for($i=0; $i<count($ELE_list); $i++)
		{
			$ELE_list[$i]['DefaultID'] = str_replace(array("[","]"), "", $ELE_list[$i]['DefaultID']);
			
			# Use ELE list as default if no ELE is selected
			if($ParELE == "")
				$ELEForRecord[] = $ELE_list[$i]['DefaultID'];
		}
		if($ParELE != "")
			$ELEForRecord = $ParELE;
		# Get OLE records
		$olr_list = $this->GET_OLE_RECORD_LIST($ParStartYear, $ParEndYear, $ParClassLevel, $ParClass, $ELEForRecord, $ParHourRange, $ParSearchName, $ParField, $ParOrder, $ParCategory, $ParStartYearID, $ParEndYearID);
		
		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		}
		
		# Handle multi-level sort
		if($ParField == 1 || $ParField == 2)
		{
			if($ParField == 1)
			{
				$SortOrder[0] = $ParOrder;
				$SortOrder[1] = 1;
			}
			else
			{
				$SortOrder = explode("|", $ParOrder);
				$SortOrder[1] = $SortOrder[1] > 0 ? -1 : 1;
			}

			$SortOrder = implode("|", $SortOrder);
		}
		else
			$SortOrder = "1|1";

		# Table header
		$ReturnStr =	"
										<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" bgcolor=\"#CCCCCC\">\n
											<tr class=\"tabletop\">\n
												<td rowspan=\"2\" class=\"tabletoplink\">#</td>\n
												<td rowspan=\"2\" ><a href=\"javascript:jSORT_TABLE(0, ".($ParField==0?($ParOrder>0?-1:1):1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort0','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"> ".$i_UserName." ".($ParField==0?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort0\" id=\"sort0\" valign=\"absbottom\" />":"")."</a></td>\n
												<td width=\"50\" rowspan=\"2\" ><a href=\"javascript:jSORT_TABLE(1, ".($ParField==1?($ParOrder>0?-1:1):1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort1','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$i_UserClassName." ".($ParField==1?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort1\" id=\"sort1\" valign=\"absbottom\" />":"")."</a></td>\n
												<td width=\"70\" rowspan=\"2\" ><a href=\"javascript:jSORT_TABLE(2, '".$SortOrder."')\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort2','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".(substr($ParOrder,strpos($ParOrder,"|")+1)>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$i_UserClassNumber." ".($ParField==2?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".(substr($ParOrder,strpos($ParOrder,"|")+1)>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort2\" id=\"sort2\" valign=\"absbottom\" />":"")."</a></td>\n
												<td width=\"80\" align=\"center\" rowspan=\"2\" class=\"tablebluetop seperator_left\"><a href=\"javascript:jSORT_TABLE(3, ".($ParField==3?($ParOrder>0?-1:1):1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort3','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$iPort['no_of_program']." ".($ParField==3?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort3\" id=\"sort3\" valign=\"absbottom\" />":"")."</a></td>\n
												<td width=\"80\" align=\"center\" rowspan=\"2\" class=\"tablebluetop seperator_right\"><a href=\"javascript:jSORT_TABLE(4, ".($ParField==4?($ParOrder>0?-1:1):1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort4','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['total_hours']." ".($ParField==4?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort4\" id=\"sort4\" valign=\"absbottom\" />":"")."</a></td>\n
												<td colspan=\"".(($ParELE == "")?count($ELE_list):count($ParELE))."\" align=\"center\"  class=\"tablegreentop tabletopnolink\" style=\"border-bottom:1px solid #FFFFFF\">".$ec_iPortfolio['ele']."</td>\n
											</tr>\n
											<tr class=\"tabletop\">\n
									";
									
		foreach($ELE_list as $ELE)
			if($ParELE == "" || (is_array($ParELE) && in_array($ELE['DefaultID'], $ParELE)) || (!is_array($ParELE) && $ELE['DefaultID'] == $ParELE))
				$ReturnStr .=	"<td width=\"50\" align=\"center\"  class=\"tablegreentop tabletopnolink\">".$ELE['Title']."</td>\n";
		
		$ReturnStr .=	"</tr>\n";
		
		# Table content
		$i = 0;
		if(is_array($olr_list))
		{
			foreach($olr_list as $UserID => $olr)
			{
				if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
				{
					$RowStyle = ($i%2==0)?"tablerow1":"tablerow2";
					
					$RowStyle2_left = ($i%2==0)?"tabletext tablerow_total seperator_left":"tabletext tablerow_total2 seperator_left";
					$RowStyle2_right = ($i%2==0)?"tabletext tablerow_total seperator_right":"tabletext tablerow_total2 seperator_right";
					
					$ReturnStr .=	"
													<tr class=\"".$RowStyle."\">
														<td width=\"20\" valign=\"top\" ><span class=\"tabletext\">".($i+1)."</span></td>
														<td valign=\"top\" nowrap><a href=\"javascript:newWindow('year_details.php?StudentID=".$UserID."', 94)\" class=\"tablelink\">".$olr['Name']."</a></td>
														<td align=\"center\" valign=\"top\" class=\"tabletext\">".$olr['ClassName']."</td>
														<td align=\"center\" valign=\"top\" ><span class=\"tabletext\">".$olr['ClassNumber']."</span></td>
												";
												
					$ReturnStr .= "<td align=\"center\" class=\"".$RowStyle2_left."\"><strong>".($olr['ProgramNum']==""?0:$olr['ProgramNum'])."</strong></td>";
					$ReturnStr .= "<td align=\"center\" class=\"".$RowStyle2_right."\"><strong>".($olr['TotalHour']==""?0:$olr['TotalHour'])."</strong></td>";

					foreach($ELE_list as $ELE)
					{
						if($ParELE == "" || (is_array($ParELE) && in_array($ELE['DefaultID'], $ParELE)) || (!is_array($ParELE) && $ELE['DefaultID'] == $ParELE))
						{
							$ReturnStr .=	'<td width="50" align="center"  class="tabletext">'.($olr[$ELE['DefaultID']]==""?"0":$olr[$ELE['DefaultID']])."</td>\n";
						}
					}
					
					$ReturnStr .= "</tr>";
				}
				
				$i++;
			}
		}
		
		if(count($olr_list) == 0)
		{
			$ReturnStr .=	"
											<tr>
												<td align=\"center\" class=\"tabletext\" colspan=\"".(($ParELE == ""?count($ELE_list):1)+5)."\">
												".$no_record_msg."
												</td>
											</tr>
										";
		}
		
		$ReturnStr .=	"</table>\n";
		
		return array($ReturnStr, $olr_list);
	}
	
	### (2008-09-03) Modified by key - update the display layout
	# Generate OLE statistic table grouped by year for display
	function GEN_OLE_YEAR_RECORD_LIST($ParUserID, $ParField="", $ParOrder="")
	{
		global $ec_iPortfolio, $no_record_msg;
		global $iPort, $image_path, $LAYOUT_SKIN,$sys_custom;
	
		# Get OLE records grouped by year
		$olr_list = $this->GET_OLE_YEAR_RECORD_LIST($ParUserID, $ParField, $ParOrder);
		
		# Get ELE list
		$ELE_list = $this->GET_OLE_ELE_LIST();
//	debug_r($olr_list);
//	debug_r($ELE_list);
		# Table header
		$ReturnStr =	"
										<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" bgcolor=\"#CCCCCC\">
											<tr class=\"tabletop\">
												<td rowspan=\"2\" class=\"tabletopnolink\"><a href=\"javascript:jSORT_TABLE(0, ".($ParField==0?($ParOrder>0?-1:1):-1).")\" onMouseOver=\"MM_swapImage('sort0','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" class=\"tabletoplink\">".$ec_iPortfolio['year']." ".($ParField==0?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort0\" id=\"sort0\" valign=\"absbottom\" />":"")."</a></td>
												<td rowspan=\"2\" align=\"center\" class=\"tablebluetop seperator_left\" ><a href=\"javascript:jSORT_TABLE(1, ".($ParField==1?($ParOrder>0?-1:1):-1).")\" onMouseOver=\"MM_swapImage('sort1','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" class=\"tabletoplink\">".$iPort['no_of_program']." ".($ParField==1?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort1\" id=\"sort1\" valign=\"absbottom\" />":"")."</a></td>
												<td rowspan=\"2\" align=\"center\" valign=\"middle\"  class=\"tablebluetop seperator_right\"><a href=\"#\" onMouseOver=\"MM_swapImage('sort2','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"jSORT_TABLE(2, ".($ParField==2?($ParOrder>0?-1:1):1).")\" class=\"tabletoplink\">".$ec_iPortfolio['total_hours']." ".($ParField==2?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort2\" id=\"sort2\" valign=\"absbottom\" />":"")."</a></td>
												<td colspan=\"".count($ELE_list)."\" align=\"center\"  class=\"tablegreentop tabletopnolink\" style=\"border-bottom:1px solid #FFFFFF\">".$ec_iPortfolio['ele']." (".$ec_iPortfolio['hours'].")</td>
											</tr>
											<tr class=\"tabletop\">
									";
		foreach($ELE_list as $ELE)
		{
			$ReturnStr .= '<td width="45" align="center"  class="tablegreentop tabletopnolink">'.$ELE[1]."</td>\n";
		}
		$ReturnStr .=	"</tr>\n";

		# Table content
		if(count($olr_list) > 0)
		{
			$i = 0;
			foreach($olr_list as $year => $olr)
			{
				$RowStyle = ($i%2==0)?"tablerow1":"tablerow2";
				
				$RowStyle2_left = ($i%2==0)?"tabletext tablerow_total seperator_left":"tabletext tablerow_total2 seperator_left";
				$RowStyle2_right = ($i%2==0)?"tabletext tablerow_total seperator_right":"tabletext tablerow_total2 seperator_right";
				
				$ReturnStr .=	"
								<tr id='".$olr['AcademicYearID']."' class=\"".$RowStyle."\">
								<td class=\"tabletext tablerow_title\" nowrap>".$year."</td>
								";
				$ReturnStr .= "<td align=\"center\" class=\"".$RowStyle2_left."\">".$olr['ProgramNum']."</td>";					
				$ReturnStr .= "<td align=\"center\" class=\"".$RowStyle2_right."\">".$olr['TotalHour']."</td>";
				foreach($ELE_list as $ELE)
				{
					if($sys_custom['iPf']['StPaulCoeduPriSchool']['Report']['StudentLearningProfile']){
					$ReturnStr .=	"
									<td align=\"center\" ELE='".$ELE[0]."' class=\"tabletext ELEtableCell\" activityDetails='".implode("",(array)$olr[str_replace(array("[","]"), "", $ELE[0])."_Activities"])."'>".($olr[str_replace(array("[","]"), "", $ELE[0])] == ""?0:$olr[str_replace(array("[","]"), "", $ELE[0])])."</td>
									";
					}else{
					$ReturnStr .=	"
									<td align=\"center\" class=\"tabletext\" >".($olr[str_replace(array("[","]"), "", $ELE[0])] == ""?0:$olr[str_replace(array("[","]"), "", $ELE[0])])."</td>
									";
					}
												
					$Total[str_replace(array("[","]"), "", $ELE[0])] += $olr[str_replace(array("[","]"), "", $ELE[0])];
				}
				
				$ReturnStr .= "</tr>";

				$Total['TotalHour'] += $olr['TotalHour'];
				$Total['ProgramNum'] += $olr['ProgramNum'];
				
				$i++;
			}
			
			# Total summary
			$RowStyle = ($i%2==0)?"tablerow1":"tablerow2";
			
			$RowStyle2_left = "tabletext seperator_left tablebluebottom";
			$RowStyle2_right = "tabletext seperator_right tablebluebottom";
			
			$ReturnStr .=	"
							<tr class=\"".$RowStyle."\">
							<td class=\"tabletext tablerow_title\" nowrap align=\"right\">".$iPort['sub_total']."</td>
							";
			
			$ReturnStr .= "<td align=\"center\" class=\"".$RowStyle2_left."\"><strong>".$Total['ProgramNum']."</strong></td>";				
			$ReturnStr .= "<td align=\"center\" class=\"".$RowStyle2_right."\"><strong>".$Total['TotalHour']."</strong></td>";
			
			foreach($ELE_list as $ELE)
			{
				$ReturnStr .= "<td align=\"center\" class=\"tablegreenbottom\"><strong>".($Total[str_replace(array("[","]"), "", $ELE[0])] == ""?0:$Total[str_replace(array("[","]"), "", $ELE[0])])."</strong></td>";
			}
		
			$ReturnStr .= "</tr>";
		}
		else
		{
			$ReturnStr .=	"
							<tr>
							<td align=\"center\" class=\"tabletext\" colspan=\"".(($ParELE == ""?count($ELE_list):1)+2)."\">
							".$no_record_msg."
							</td>
							</tr>
							";
		}
		
		$ReturnStr .= "</table>\n";
		
		return $ReturnStr;
	}
	
##########################################################################

	// Generate OLE summary table
	function GET_STUDENT_OLR_STAT_TABLE($ParStudentID, $ParCategory, $ParYear, $field, $order=0, $ParStatus="2,4")
	{
		global $eclass_db, $image_path, $ec_iPortfolio, $list_total;

		switch(substr($ParStatus, 0, 1))
		{
			case "1":
				$DisplayStatus = $ec_iPortfolio['pending'];
				break;
			case "2":
				$DisplayStatus = $ec_iPortfolio['approved'];
				break;
			case "3":
				$DisplayStatus = $ec_iPortfolio['rejected'];
				break;
		}

		# get the ELE code array
		$DefaultELEArray = $this->GET_ELE();
		$ELECodeArray = array_keys($DefaultELEArray);		

		# get sorting condition
		if($field==0)
			$QueryOrder = ($order==0) ? "DESC" : "ASC";
		else
			$SortingELE = ($field=="-1") ? "Overall" : $ELECodeArray[$field-1];

		$IconName = ($order==0) ? "icon_sort_d_on.gif" : "icon_sort_a_on.gif";
		

		$OrderIcon = "<img src='$image_path/{$LAYOUT_SKIN}/$IconName' hspace=2 border=0 align='absmiddle'>";
		$NewOrder = ($order==1) ? 0 : 1;
/*
		# count total explicitly as multi-ELE can be assigned to one record
		$sql = "SELECT
					if ((Month(StartDate)>8), Year(StartDate), Year(StartDate)-1) AS StartYear, 
					SUM(Hours) AS TotalHours,
					count(RecordID) AS TotalRecords
				FROM
					{$eclass_db}.OLE_STUDENT
				WHERE
					UserID = '".$ParStudentID."' AND RecordStatus in (".$ParStatus.")
				GROUP BY
					StartYear
				ORDER BY
					StartYear {$QueryOrder}
				";
		$RowTotal = $this->returnArray($sql);
		for ($i=0; $i<sizeof($RowTotal); $i++)
		{
			$TotalHours[$RowTotal[$i]['StartYear']] = $RowTotal[$i]['TotalHours'];
			$TotalRecords[$RowTotal[$i]['StartYear']] = $RowTotal[$i]['TotalRecords'];
		}
*/
		$cond = ($ParCategory!="") ? " AND os.Category = '".$ParCategory."'" : "";
		
		if($ParYear!="")
		{
			$SplitArray = explode("-", $ParYear);
			if(sizeof($SplitArray)>1)
			{
        $FirstDate = date("Y-m-d", mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"], trim($SplitArray[0])));
        $LastDate = date("Y-m-d", mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"]-1, trim($SplitArray[1])));
			}
			else
			{
        $FirstDate = date("Y-m-d", mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"], $ParYear));
        $LastDate = date("Y-m-d", mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"]-1, $ParYear+1));
			}
			$cond .= " AND os.StartDate BETWEEN '".$FirstDate."' AND '".$LastDate."'";
		}
		$cond .= " AND op.IntExt = 'INT'";

		$sql = "SELECT
					os.StartDate,
					os.ELE,
					os.Hours,
					if ((Month(os.StartDate)>8), Year(os.StartDate), Year(os.StartDate)-1) AS StartYear
				FROM
					{$eclass_db}.OLE_STUDENT as os
				INNER JOIN
					{$eclass_db}.OLE_PROGRAM as op
				ON
					os.ProgramID = op.ProgramID
				WHERE
					os.UserID = '".$ParStudentID."' AND os.RecordStatus in (".$ParStatus.")
					{$cond}
				ORDER BY
					StartDate {$QueryOrder}
				";
		$row = $this->returnArray($sql);

		$ELEHoursArray = array();
		$YearArray = array();
		for($i=0; $i<sizeof($row); $i++)
		{
			$IsInDefaultELE = false;
		
			list($StartDate, $ele, $hours, $TargetYear) = $row[$i];
			$ELEArray = explode(",", $ele);
			for($j=0; $j<sizeof($ELEArray); $j++)
			{
				$ele_code = trim($ELEArray[$j]);
				if($ele_code!="" && array_key_exists($ele_code, $DefaultELEArray))
				{
					$ELEHoursArray[$TargetYear][$ele_code] = $ELEHoursArray[$TargetYear][$ele_code] + $hours + 0;
					$ELEHoursArray[$TargetYear]["Overall"] = $ELEHoursArray[$TargetYear]["Overall"] + $hours + 0;
					$ELETotalHoursArray[$ele_code] = $ELETotalHoursArray[$ele_code] + $hours + 0;
					$ELETotalHoursArray["Overall"] = $ELETotalHoursArray["Overall"] + $hours + 0;
					
					$IsInDefaultELE = true;
				}
			}
			if($SortingELE!="")
			{
				$SortingArray[$TargetYear] = ($ELEHoursArray[$TargetYear][$SortingELE]=="") ? 0 : $ELEHoursArray[$TargetYear][$SortingELE];
			}
			else if(!in_array($TargetYear, $YearArray))
			{
				$YearArray[] = $TargetYear;
			}
			
			if($IsInDefaultELE)
			{
				$TotalHours[$TargetYear] += $hours;
			}
			$TotalRecords[$TargetYear] ++;
		}

		if($SortingELE!="" && !empty($SortingArray))
		{
			if($order==0)
				arsort($SortingArray);
			else
				asort($SortingArray);

			$YearArray = array_keys($SortingArray);
		}

		if(!empty($DefaultELEArray))
		{
			
			$StatTable = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
			$StatTable .= "<tr class=\"tabletop\">";
			$StatTable .= "<td rowspan=\"2\" class=\"tabletopnolink\">".$i_general_Year."</td>";
			if (count($DefaultELEArray) > 0)
			{
				$StatTable .= "<td colspan=\"".count($DefaultELEArray)."\" align=\"center\"  class=\"tabletopnolink\">".$ec_iPortfolio['ele']." (".$ec_iPortfolio['hours'].")</td>";
			}
			$StatTable .= "<td rowspan=\"2\" align=\"center\" valign=\"middle\"  class=\"tabletopnolink\">".$ec_iPortfolio["total_hours"]."</td>";			
			$StatTable .= "<td rowspan=\"2\" align=\"center\" valign=\"middle\"  class=\"tabletopnolink\">".$ec_iPortfolio['total_records']."<br />(".$DisplayStatus.")</td>";			
			$StatTable .= "</tr>";
			
			# Eric Yip : should be $DefaultELEArray (20081029)
			if (count($DefaultELEArray) > 0)
			{
				$StatTable .= "<tr class=\"tabletop\">";
				foreach($DefaultELEArray as $ELECode => $ELETitle)
				{
					$CodePos = array_search($ELECode, $ELECodeArray);
					$TargetField = $CodePos+1;
					$ELEDisplay = $ELETitle;
					$StatTable .= "<td align='center' class='tabletopnolink'><b>".$ELEDisplay."</b></td>";
				}
				
				$StatTable .= "</tr>";				
			}

			for($i=0; $i<sizeof($YearArray); $i++)
			{
				$Year = $YearArray[$i];
				$DataArray = $ELEHoursArray[$Year];
				
				$bgcolor = ($i%2==0) ? "#FFFFFF" : "#EEEEEE";
				$RowClass = ($i%2==0) ? "tablerow1" : "tablerow2";
				$StatTable .= "<tr class='".$RowClass."' ><td class='tabletext tablerow_title' nowrap='nowrap'>".$Year."</td>";
				foreach($DefaultELEArray as $ELECode => $ELETitle)
				{
					
					if ($DataArray[$ELECode] > 0)			
					{			
						$ELEHourLink = "<a href=\"ole_record.php?Year=$Year&ELE=$ELECode&status=".substr($ParStatus, 0, 1)."\" class=\"tablelink\">".$DataArray[$ELECode]."</a>";
						$StatTable .= "<td align='center' class='tabletext'>".$ELEHourLink."</td>";
					}
					else			
						$StatTable .= "<td align='center' class='tabletext'>0</td>";
				}
								
				if ($TotalHours[$Year] > 0)			
				{			
					$TotalHourLink = "<a href=\"ole_record.php?Year=$Year&status=".substr($ParStatus, 0, 1)."\" class=\"tablelink\">".round($TotalHours[$Year],2)."</a>";
					$StatTable .= "<td align='center' class='tabletext'>".$TotalHourLink."</td>";
				}
				else			
					$StatTable .= "<td align='center' class='tabletext'>0</td>";
								
				$TotalRecordLink = "<a href=\"ole_record.php?Year=$Year&status=".substr($ParStatus, 0, 1)."\" class=\"tablelink\">".($TotalRecords[$Year]>0?$TotalRecords[$Year]:0)."</a>";
				$StatTable .= "<td align='center' class='tabletext'>".$TotalRecordLink."</td>";

				$StatTable .= "</tr>";
				$OverallHours += $TotalHours[$Year];
				$OverallRecords += $TotalRecords[$Year];
			}

			
			// Total rows
			$bgcolor = ($i%2==0) ? "#FFFFFF" : "#EEEEEE";
			$RowClass = ($i%2==0) ? "tablerow1" : "tablerow2";
			$StatTable .= "<tr class='".$RowClass."' ><td class='tabletext' nowrap='nowrap'>".$list_total."</td>";
			foreach($DefaultELEArray as $ELECode => $ELETitle)
			{
				if ($ELETotalHoursArray[$ELECode] > 0)			
				{			
					$ELETotalHourLink = "<a href=\"ole_record.php?ELE=$ELECode&status=".substr($ParStatus, 0, 1)."\" class=\"tablelink\">".$ELETotalHoursArray[$ELECode]."</a>";
					$StatTable .= "<td align='center' class='tabletext'><b>".$ELETotalHourLink."</b></td>";
				}
				else			
					$StatTable .= "<td align='center' class='tabletext tablerow_total'><b>0</b></td>";
				
			}
			
			if ($OverallHours > 0)			
			{			
				$OverallHourLink = "<a href=\"ole_record.php?status=".substr($ParStatus, 0, 1)."\" class=\"tablelink\">".round($OverallHours, 2)."</a>";
				$StatTable .= "<td align='center' class='tabletext'><b>".$OverallHourLink."</b></td>";
			}
			else			
				$StatTable .= "<td align='center' class='tabletext tablerow_subtotal'><b>0</b></td>";
			
			
			$OverallRecordLink = "<a href=\"ole_record.php?status=".substr($ParStatus, 0, 1)."\" class=\"tablelink\">".($OverallRecords>0?$OverallRecords:0)."</a>";
			$StatTable .= "<td align='center' class='tabletext'><b>".$OverallRecordLink."</b></td>";

			$StatTable .= "</tr>";
			
			$StatTable .= "</table>";			
		}

		return $StatTable;
	}
	
##########################################################################
	
	#######################################################################
	### (2008-09-04) End Modified by Key - Add Role selection
	### (2008-10-30) Eric Yip - Comment out 3 of the status, and hide status column
	### (2015-01-12) Bill - Add SAS Credit Point input for Ng Wah SAS Cust
	### (2015-01-21) Bill - Add Details / Name of Activities input for St. Paul PAS CUST
	### (2015-02-27) Bill - $sys_custom['iPf']['Mgmt']['AssignStudentStatus_DefaultApproved'] = true > Default Status: Approved
	#######################################################################
	// Generate OLE student assign table
	function GEN_ASSIGN_OLE_STUDENT_TABLE($ParEventID, $ParStudentArray=array(), $ParIntExt=0, $doublePreset=false, $eventType=-1)
	{
		global $eclass_db, $image_path, $ec_iPortfolio, $list_total, $LAYOUT_SKIN, $PATH_WRT_ROOT;
		global $linterface, $ec_iPortfolio, $AllStatus, $iPort, $ipf_cfg, $sys_custom;

		switch($ParIntExt){
		case 1 : $TabName = 'EXT_STUDENT';
					break;
		default : $TabName = 'INT_STUDENT'; 
					break;
		}			
		
		$presetWidth = $doublePreset? '320' : '';
		
		$IPF_HIDE_COLUMN = array();
		foreach((array)$sys_custom['IPF_HIDE_COLUMN'][$TabName] as $colName=>$hide){
			$IPF_HIDE_COLUMN[$colName] = $hide;
		}
		
		$UpdateIcon = "<img src=\"".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_update.gif\" border=\"0\" align=\"absmiddle\">";
		
		// [2015-0324-1420-39164] settings to control the layout of assign student page
		// input field disable settings
		$name_award_disabled = "";
		$role_disabled = "";
		$attach_comment_disabled = "";
		// preset role settings - "Participant" and "Competitor"
		$only_PC = "";
		$exclude_PC = "";
		if($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme'] && $eventType!=-1){
			$name_award_disabled = ($eventType==0 || $eventType==1)? " disabled" : "";
			$attach_comment_disabled = $name_award_disabled;
			$role_disabled = $eventType==2? " disabled" : "";
			$only_PC = $eventType==0? " disabled" : "";
			$exclude_PC = $eventType==1? " disabled" : "";
		}
		
	################################
	# Header	
    $Table1 = "";
//    $Table1 .= "<table width=\"98%\" border=\"0\" cellpadding=\"4\" cellspacing=\"1\" bgcolor=\"#CCCCCC\" >";
    $Table1 .= "<table width=\"98%\" border=\"0\" cellpadding=\"4\" cellspacing=\"1\" >";
    $Table1 .= "<tr class=\"tabletop\">";
    $Table1 .= "<td width=\"12\" valign=\"top\">#</td>";
    $Table1 .= "<td width=\"100\" valign=\"top\" class=\"tabletopnolink\">".$ec_iPortfolio['heading']['student_name']."</td>";
    $Table1 .= "<td width=\"70\" align=\"center\" valign=\"top\"  class=\"tabletopnolink\">".$ec_iPortfolio['class_and_number']."</td>";
    if($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']){
    	$Table1 .= "<td align=\"left\" valign=\"top\" class=\"tabletopnolink\">".$ec_iPortfolio['details']."</td>";
    }
    if(!$IPF_HIDE_COLUMN['OLE_ROLE']){
    	$Table1 .= "<td width=\"120\" align=\"left\" valign=\"top\"  class=\"tabletopnolink\">".$ec_iPortfolio['ole_role']."</td>";
    }
    if(!$IPF_HIDE_COLUMN['ACHIEVEMENTS']){
    $Table1 .= "<td align=\"left\" valign=\"top\"  class=\"tabletopnolink\">".$ec_iPortfolio['achievement']."</td>";
    }
    if ($ParIntExt == 0) {
    	$Table1 .= "<td width=\"70\" align=\"left\" valign=\"top\" class=\"tabletopnolink\">".$ec_iPortfolio['hours']."</td>";
    }
    if($sys_custom['NgWah_SAS']){
    	global $Lang;
    	$Table1 .= "<td width=\"70\" align=\"left\" valign=\"top\" class=\"tabletopnolink\">".$Lang['iPortfolio']['NgWah_SAS_Settings']['Credit_pt']."</td>";
    }
    if(!$IPF_HIDE_COLUMN['ATTACHMENT']){
    $Table1 .= "<td width=\"100\" align=\"left\" valign=\"top\" class=\"tabletopnolink\">".$ec_iPortfolio['attachment']."</td>";
    }
    if(!$IPF_HIDE_COLUMN['COMMENT']){
    $Table1 .= "<td align=\"left\" valign=\"top\" class=\"tabletopnolink\">".$ec_iPortfolio['comment']."</td>";
    }
    if(!$IPF_HIDE_COLUMN['STATUS']){
    $Table1 .= "<td align=\"left\" valign=\"top\" class=\"tabletopnolink\">".$ec_iPortfolio['status']."</td>";
    }
    $Table1 .= "</tr>";
	################################	
	# Second Row Apply all 
		$Table1 .= "<tr class=\"tablebottom\">";
		$Table1 .= "<td valign=\"top\">&nbsp;</td>";
		$Table1 .= "<td valign=\"top\" nowrap=\"nowrap\"  class=\"tabletopnolink\">&nbsp;</td>";		
		$Table1 .= "<td align=\"center\" valign=\"top\"  class=\"tabletopnolink\">&nbsp;</td>";		    
    	
	    if($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']){
			$DivPAS = "";
			$DivPAS .= "<table id=\"MemberDiv\" width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" bgcolor=\"#BCBCBC\" style=\"border:1px solid #FFFFFF\">";	
			$DivPAS .= "<tr>";
			$DivPAS .= "<td>";
			$DivPAS .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$DivPAS .= "<tr>";
			$DivPAS .= "<td nowrap><span class=\"tabletext\">";
			// [2015-0324-1420-39164] change from input to textarea to support autocomplete the content of "Details / Name of Activities"
			// [2015-0324-1420-39164] disable "Details / Name of Activities" if $name_award_disabled != ""
			//$DivPAS .= "<input name=\"AllStudentDetails\" id=\"AllStudentDetails\" type=\"text\" class=\"textboxtext\" value=\"\" style=\"min-width:3em\" autocomplete=\"off\" $name_award_disabled />";
			$DivPAS .= $linterface->GET_TEXTAREA("AllStudentDetails", "", 12, 2, "", "", " style=\"min-width:3em\" autocomplete=\"off\" $name_award_disabled", "textboxtext", "AllStudentDetails");
			$DivPAS .= "</span></td>";
			// [2015-0324-1420-39164] hide apply all button if $name_award_disabled != ""
			$DivPAS .= $name_award_disabled==""?"<td width=\"12\" nowrap><a href=\"javascript:jSetValue(document.form1.AllStudentDetails.value, document.form1,'StudentDetails[]')\"><img src=\"".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_assign.gif\" border=\"0\"></a></td>":"";
			$DivPAS .= "</tr>";
			$DivPAS .= "</table>";
			$DivPAS .= "</td>";
			$DivPAS .= "</tr>";
			$DivPAS .= "</table>";
			$Table1 .= "<td align=\"left\" valign=\"top\" class=\"tabletopnolink\">".$DivPAS."</td>";
	    }
	    
	    if(!$IPF_HIDE_COLUMN['OLE_ROLE']){
		$Div1 = "";
		//$Div1 .= "<span id='MemberBtnDiv'><a href=\"javascript:displayTable('MemberBtnDiv','none');displayTable('MemberDiv','block')\">{$UpdateIcon}</a></span>";
		$Div1 .= "<table id=\"MemberDiv\" width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" bgcolor=\"#BCBCBC\" style=\"border:1px solid #FFFFFF\">";
		$Div1 .= "<tr>";
		$Div1 .= "<td>";
		$Div1 .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$Div1 .= "<tr>";
		$Div1 .= "<td nowrap><span class=\"tabletext\">";
		// [2015-0324-1420-39164] disable "Role of Participation" if $role_disabled != ""
		$Div1 .= "<input name=\"AllRole\" id=\"AllRole\" type=\"text\" class=\"textboxtext\" value=\"\" style=\"min-width:3em\" $role_disabled/>";
		$Div1 .= "</span></td>";
		// [2015-0324-1420-39164] hide apply all and preset content button if $role_disabled != ""
		if($role_disabled==""){
			$Div1 .= "<td width=\"12\" nowrap><a href=\"javascript:jSetValue(document.form1.AllRole.value, document.form1,'Role[]')\"><img src=\"".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_assign.gif\" border=\"0\"></a></td>";
			$Div1 .= "<td width=\"1\">&nbsp;</td>";
			$Div1 .= "<td width=\"12\" nowrap><div id=\"change_preset\">".GetPresetText("jRoleArr1", 1, "AllRole", "", '', $presetWidth)."</div></td>";
		}
		$Div1 .= "</tr>";
		$Div1 .= "</table>";
		$Div1 .= "</td>";
		$Div1 .= "</tr>";
		$Div1 .= "</table>";								
		$Table1 .= "<td align=\"left\" valign=\"top\"  class=\"tabletopnolink\">".$Div1."</td>";
	    }		
	
		if(!$IPF_HIDE_COLUMN['ACHIEVEMENTS']){
		$Div2 = "";
		$Div2 .= "<table id=\"MemberDiv\" width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" bgcolor=\"#BCBCBC\" style=\"border:1px solid #FFFFFF\">";		
		$Div2 .= "<tr>";
		$Div2 .= "<td>";
		$Div2 .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$Div2 .= "<tr>";
		$Div2 .= "<td nowrap><span class=\"tabletext\">";
		// [2015-0324-1420-39164] disable "Awards / Certifications / Achievements" if $name_award_disabled != ""
		$Div2 .= "<input name=\"AllAchievement\" id=\"AllAchievement\" type=\"text\" class=\"textboxtext\" value=\"\" style=\"min-width:3em\" $name_award_disabled />";
		$Div2 .= "</span></td>";
		// [2015-0324-1420-39164] hide apply all and preset content button if $name_award_disabled != ""
		if($name_award_disabled==""){
			$Div2 .= "<td width=\"12\" nowrap><a href=\"javascript:jSetValue(document.form1.AllAchievement.value, document.form1,'Achievement[]')\"><img src=\"".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_assign.gif\" border=\"0\"></a></td>";
			$Div2 .= "<td width=\"1\">&nbsp;</td>";
			$Div2 .= "<td width=\"12\" nowrap><div id=\"change_preset\">".GetPresetText("jAwardsArr1", 99999, "AllAchievement", "",'divAwardsSuggest', $presetWidth)."</div></td>";
		}
		$Div2 .= "</tr>";
		$Div2 .= "</table>";
		$Div2 .= "</td>";
		$Div2 .= "</tr>";
		$Div2 .= "</table>";										
		$Table1 .= "<td align=\"left\" valign=\"top\"  class=\"tabletopnolink\">".$Div2."</td>";		
		}
		

		$Div3 = "";
		$Div3 .= "<table id=\"MemberDiv\" width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" bgcolor=\"#BCBCBC\" style=\"border:1px solid #FFFFFF\">";		
		$Div3 .= "<tr>";
		$Div3 .= "<td>";
		$Div3 .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$Div3 .= "<tr>";
		$Div3 .= "<td nowrap><span class=\"tabletext\">";
		$Div3 .= "<input name=\"AllHours\" type=\"text\" class=\"textboxtext\" value=\"\" style=\"min-width:2em\" maxlength = \"6\"/>";
		$Div3 .= "</span></td>";
		$Div3 .= "<td width=\"12\" nowrap><a href=\"javascript:jSetValue(document.form1.AllHours.value, document.form1,'Hours[]')\"><img src=\"".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_assign.gif\" border=\"0\"></a></td>";
		$Div3 .= "</tr>";
		$Div3 .= "</table>";
		$Div3 .= "</td>";
		$Div3 .= "</tr>";
		$Div3 .= "</table>";
		if ($ParIntExt == 0) {										
			$Table1 .= "<td align=\"left\" valign=\"top\" nowrap class=\"tabletopnolink\">".$Div3."</td>";
		}							
		
	    if($sys_custom['NgWah_SAS']){
	    	$DivSAS = "";
			$DivSAS .= "<table id=\"MemberDiv\" width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" bgcolor=\"#BCBCBC\" style=\"border:1px solid #FFFFFF\">";		
			$DivSAS .= "<tr>";
			$DivSAS .= "<td>";
			$DivSAS .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$DivSAS .= "<tr>";
			$DivSAS .= "<td nowrap><span class=\"tabletext\">";
			$DivSAS .= "<input name=\"AllSASPoint\" type=\"text\" class=\"textboxtext\" value=\"\" style=\"min-width:2em\" maxlength = \"4\"/>";
			$DivSAS .= "</span></td>";
			$DivSAS .= "<td width=\"12\" nowrap><a href=\"javascript:jSetValue(document.form1.AllSASPoint.value, document.form1, 'SASPoint[]')\"><img src=\"".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_assign.gif\" border=\"0\"></a></td>";
			$DivSAS .= "</tr>";
			$DivSAS .= "</table>";
			$DivSAS .= "</td>";
			$DivSAS .= "</tr>";
			$DivSAS .= "</table>";
			$Table1 .= "<td align=\"left\" valign=\"top\" nowrap class=\"tabletopnolink\">".$DivSAS."</td>";
	    }
		
		// disable blank td while ext for hiding columns cust
		if(!$IPF_HIDE_COLUMN['ATTACHMENT']){
			$Table1 .= "<td align=\"left\" valign=\"top\" class=\"tabletopnolink\">&nbsp;</td>";
		}
		if(!$IPF_HIDE_COLUMN['COMMENT']){
		$Div4 = "";
		$Div4 .= "<table id=\"MemberDiv\" width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" bgcolor=\"#BCBCBC\" style=\"border:1px solid #FFFFFF\">";		
		$Div4 .= "<tr>";
		$Div4 .= "<td>";
		$Div4 .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$Div4 .= "<tr>";
		$Div4 .= "<td nowrap><span class=\"tabletext\">";
		// [2015-0324-1420-39164] disable "Comment" if $attach_comment_disabled != ""
		$Div4 .= "<input name=\"AllTeacherComment\" type=\"text\" class=\"textboxtext\" value=\"\" $attach_comment_disabled/>";
		$Div4 .= "</span></td>";
		// [2015-0324-1420-39164] hide apply all button if $attach_comment_disabled != ""
		$Div4 .= $attach_comment_disabled==""?"<td width=\"12\" nowrap><a href=\"javascript:jSetValue(document.form1.AllTeacherComment.value, document.form1,'TeacherComment[]')\"><img src=\"".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_assign.gif\" border=\"0\"></a></td>":"";
		$Div4 .= "</tr>";
		$Div4 .= "</table>";
		$Div4 .= "</td>";
		$Div4 .= "</tr>";
		$Div4 .= "</table>";
		$Table1 .= "<td align=\"left\" valign=\"top\" nowrap class=\"tabletopnolink\">".$Div4."</td>";
		}
		if(!$IPF_HIDE_COLUMN['STATUS']){		
		$StatusSelectionAll = "<SELECT name='AllStatus'  >";
		//$StatusSelectionAll .= "<OPTION value='' ".($AllStatus==0?"SELECTED":"")."> -- </OPTION>";
		$StatusSelectionAll .= "<OPTION value='1' ".($AllStatus==1?"SELECTED":"").">".$ec_iPortfolio['pending']."</OPTION>";
		$StatusSelectionAll .= "<OPTION value='2' ".($AllStatus==2?"SELECTED":"").">".$ec_iPortfolio['approved']."</OPTION>";
		$StatusSelectionAll .= "<OPTION value='3' ".($AllStatus==3?"SELECTED":"").">".$ec_iPortfolio['rejected']."</OPTION>";
		$StatusSelectionAll .= "<OPTION value='4' ".($AllStatus==4?"SELECTED":"").">".$ec_iPortfolio['teacher_submit_record']."</OPTION>";
		$StatusSelectionAll .= "</SELECT>";
				
		$Div5 = "";
		$Div5 .= "<table id=\"MemberDiv\" width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" bgcolor=\"#BCBCBC\" style=\"border:1px solid #FFFFFF;\">";		
		$Div5 .= "<tr>";		
		$Div5 .= "<td>";
		$Div5 .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$Div5 .= "<tr>";
// 		$Div5 .= "<td nowrap><a href=\"#\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort02','','".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_d_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><span class=\"tabletext\">";
		$Div5 .= "<td nowrap>";
		$Div5 .= $StatusSelectionAll;
		$Div5 .= "</td>";
// 		$Div5 .= "</span></a></td>";
		$Div5 .= "<td width=\"12\" nowrap><a href=\"javascript:jSetSelectedndex(document.form1.AllStatus.selectedIndex, document.form1,'Status[]')\"><img src=\"".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_assign.gif\" border=\"0\"></a></td>";
		$Div5 .= "</tr>";
		$Div5 .= "</table>";
		$Div5 .= "</td>";		
		$Div5 .= "</tr>";
		$Div5 .= "</table>";
		
		$Table1 .= "<td align=\"left\" valign=\"top\" nowrap class=\"tabletopnolink\">".$Div5."</td>";
		}

		
		
		$Table1 .= "</tr>";	
		
		
		
		for ($i=0;$i<count($ParStudentArray);$i++)
		{
			if ($i %2 == 0)
			{
				$RowClass = " class=\"tablerow1\" ";
			}
			else
			{
				$RowClass = " class=\"tablerow2\" ";
			}
				
			$TmpUserID = $ParStudentArray[$i]["UserID"];			
			$TmpUserName = $ParStudentArray[$i][1];
			$TmpUserNum = $ParStudentArray[$i]["ClassNumberText"];
			// Added: 2015-01-21
			$TmpUserDetails = $ParStudentArray[$i]["Details"];
			$TmpUserRole = $ParStudentArray[$i]["Role"];
			$TmpUserAchievement = $ParStudentArray[$i]["Achievement"];
			$TmpUserHours = $ParStudentArray[$i]["Hours"];
			// Added: 2015-01-12
			$TmpUserSASPoint = $ParStudentArray[$i]["SASPoint"];
			$TmpUserAttachment = $ParStudentArray[$i]["Attachment"];
			$TmpUserComment = $ParStudentArray[$i]["TeacherComment"];
			$TmpUserStatus = $ParStudentArray[$i]["RecordStatus"];
			$TmpRecordID = $ParStudentArray[$i]["RecordID"];

			if ($ParIntExt) {
				$INT_EXT_String = "EXT";
			} else {
				$INT_EXT_String = "INT";
			}
			
			$approvalSettings = $this->GET_OLR_APPROVAL_SETTING_DATA2($approvalSettings);
			
			# Eric Yip (20090921): Default Status if not set
//			if($TmpUserStatus == "" && $this->GET_OLR_APPROVAL_SETTING() == 2)
			# Bill (20141014): Fixed - Add "[$INT_EXT_String]" - Status - Drop down list of selected students 
			# Display Pending for Approval or Approved according to the settings of Student Learning Profile > Record Approval
//			if($TmpUserStatus == "" && !$approvalSettings['IsApprovalNeed'])
			# Bill (20150227): if $sys_custom['iPf']['Mgmt']['AssignStudentStatus_DefaultApproved'] = true, 
			# OLE/EXT assigned students - Status: Default - Approved [2015-0227-1546-1906]
			if($TmpUserStatus == "" && (!$approvalSettings[$INT_EXT_String]['IsApprovalNeed'] || $sys_custom['iPf']['Mgmt']['AssignStudentStatus_DefaultApproved']))
        		$TmpUserStatus = 2;
						
			$j = $i+1;
			
			$Table1 .= "<tr ".$RowClass.">";	
			$Table1 .= "<td valign=\"top\" ><span class=\"tabletext\">".$j."</span></td>";
			$Table1 .= "<td valign=\"top\" ><span class=\"tabletext\">".$TmpUserName."</span><input type='hidden' name='SortOleStudentRecordID[]' value='".$TmpRecordID."' /><input type='hidden' name='SortStudentID[]' value='".$TmpUserID."' /></td>";
			$Table1 .= "<td align=\"center\" valign=\"top\" class=\"tabletext\">".$TmpUserNum."</td>";
			
			if($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']){
				$Table1 .= "<td align=\"center\" valign=\"top\" class=\"tabletext\">";
				$Table1 .= "<table width=\"100%\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\">";
				$Table1 .= "<tr><td width=\"99%\" valign=\"top\">";
				// [2015-0324-1420-39164] disable "Details / Name of Activities" if $name_award_disabled != ""
				//$Table1 .= $linterface->GET_TEXTAREA("StudentDetails[]", $TmpUserDetails, 12, 5, '', '', '', '', "StudentDetails_{$i}");
				$Table1 .= $linterface->GET_TEXTAREA("StudentDetails[]", $TmpUserDetails, 12, 5, '', '', "autocomplete=\"off\" $name_award_disabled", '', "StudentDetails_{$i}");
				$Table1 .= "</td><td valign=\"top\">";
				$Table1 .= "</td></tr></table>";
				$Table1 .= "</td>";
			}
			
			if(!$IPF_HIDE_COLUMN['OLE_ROLE']){
				// Role cell
				$Table1 .= "<td align=\"center\" valign=\"top\" class=\"tabletext\">";
				
				$Table1 .= "<table width=\"100%\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\">";
				$Table1 .= "<tr><td width=\"99%\">";
				// [2015-0324-1420-39164] disable "Role of Participation" if $role_disabled != ""
				$Table1 .= "<input name=\"Role[]\" id=\"Role_$i\" type=\"text\" class=\"textboxtext\" value=\"".$TmpUserRole."\" $role_disabled/>";
				$Table1 .= "</td><td>";
				
				// [2015-0324-1420-39164] hide preset content button if $role_disabled != ""
				if($role_disabled==""){
					if($i == 0)
					{
						$Table1 .= "<div id=\"change_preset{$j}\">".GetPresetText("jRoleArr1", $i, "Role_$i", "", '', $presetWidth)."</div>";
					}
					else
					{
						$Table1 .= "<div id=\"change_preset{$j}\">".GetPresetText("jRoleArr1", $j, "Role_$i", "", '', $presetWidth)."</div>";
					}
				}
				
				$Table1 .= "</td></tr></table>";
				$Table1 .= "</td>";
			}			
			//$Table1 .= "<td align=\"center\" valign=\"top\">".$linterface->GET_TEXTAREA("Achievement[]", $TmpUserAchievement, 20)."</td>";
			if(!$IPF_HIDE_COLUMN['ACHIEVEMENTS'] ){			
				$Table1 .= "<td align=\"center\" valign=\"top\" class=\"tabletext\">";
				$Table1 .= "<table width=\"100%\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\">";
				$Table1 .= "<tr><td width=\"99%\" valign=\"top\">";
				
				//$Table1 .= "<textarea id=\"Achievement_{$i}\" class=\"textboxtext\" onfocus=\"this.rows=5;\" wrap=\"virtual\" row=\"5\" cols=\"50\" name=\"Achievement[]\">".$TmpUserAchievement."</textarea>";
				// [2015-0324-1420-39164] disable "Awards / Certifications / Achievements" if $name_award_disabled != ""
				$Table1 .= $linterface->GET_TEXTAREA("Achievement[]", $TmpUserAchievement, 30, 5, '', '', $name_award_disabled, '', "Achievement_{$i}");
				$Table1 .= "</td><td valign=\"top\">";

				$dummyLocation = 8000;//dummy div id start from 8000, the number is large enough such that it will not duplicate with other div is ok
	
				// [2015-0324-1420-39164] hide preset content button if $name_award_disabled != ""
				if($name_award_disabled==""){
					if($i == 0)
					{
						$_dummyLocation = $dummyLocation + $i;
						$Table1 .= "<div id=\"change_awardspreset{$j}\">".GetPresetText("jAwardsArr1",$_dummyLocation, "Achievement_$i", "", '', $presetWidth)."</div>";
					}
					else
					{
						$_dummyLocation = $dummyLocation + $j;
						$Table1 .= "<div id=\"change_awardspreset{$j}\">".GetPresetText("jAwardsArr1", $_dummyLocation, "Achievement_$i", "", '', $presetWidth)."</div>";
					}
				}
				$Table1 .= "</td></tr></table>";
				$Table1 .= "</td>";
			}
			
			if ($ParIntExt == 0) {
				$Table1 .= "<td align=\"center\" valign=\"top\"><input name=\"Hours[]\" type=\"text\" value=\"".$TmpUserHours."\"  maxlength = \"6\" size=\"5\"/>&nbsp;</td>";
			}
			// disable blank td for hiding columns cust			
			
			if($sys_custom['NgWah_SAS']){
				$Table1 .= "<td align=\"center\" valign=\"top\"><input name=\"SASPoint[]\" type=\"text\" value=\"".$TmpUserSASPoint."\" maxlength = \"4\" size=\"5\"/>&nbsp;</td>";
			}
			
			if(!$IPF_HIDE_COLUMN['ATTACHMENT']){
			$Table1 .= "<td align=\"center\" valign=\"top\">";
				$UploadByDiv = "";
				// Upload table cell
				/*
				if($TmpUserAttachment != "")
				{
					$AttachmentImgPath = $image_path."/".$LAYOUT_SKIN."/icon_attachment.gif";
					$AttachmentImg = "<a target='blank' href='ole_attach.php?RecordID=".$TmpRecordID."'><img border='0' src='".$AttachmentImgPath."' /></a>";
					$UploadByDiv = $AttachmentImg."&nbsp;&nbsp;&nbsp;";
				}
				*/
				// [2015-0324-1420-39164] disable "Edit / Upload" link if $attach_comment_disabled != ""
				$UploadByDiv .= $attach_comment_disabled==""? "<a href=\"javascript:jUploadFile('".$TmpUserID."','".$TmpRecordID."');\" class=\"tablelink\" >".$iPort["btn"]["edit"]." / ".$iPort["upload"]."</a>" : "";
				
				$Table1 .= "<div id=\"UploadBy_$TmpUserID\">";
				$Table1 .= $UploadByDiv;
				$Table1 .= "</div>";
//				$Table1 .= "<input type='hidden' name='UserAttachment_".$TmpUserID."' id='UserAttachment_".$TmpUserID."' value='".$TmpUserAttachment."' />";
//				$Table1 .= "<input type='hidden' name='UserRemoveAttachment_".$TmpUserID."' id='UserRemoveAttachment_".$TmpUserID."' value='' />";
//				$Table1 .= "<input type='hidden' name='RemoveItemID_".$TmpUserID."' id='RemoveItemID_".$TmpUserID."' value='' />";
//				$Table1 .= "<input type='hidden' name='AllAttachment_".$TmpUserID."' id='AllAttachment_".$TmpUserID."' value='' />";
				// Fixed: change field name to array - prevent too many input field that leads to system cannot not update student records 
				$Table1 .= "<input type='hidden' name='UserAttachment[]' id='UserAttachment_".$TmpUserID."' value='".$TmpUserAttachment."' />";
				$Table1 .= "<input type='hidden' name='UserRemoveAttachment[]' id='UserRemoveAttachment_".$TmpUserID."' value='' />";
				$Table1 .= "<input type='hidden' name='RemoveItemID[]' id='RemoveItemID_".$TmpUserID."' value='' />";
				$Table1 .= "<input type='hidden' name='AllAttachment[]' id='AllAttachment_".$TmpUserID."' value='' />";
				
				$Table1 .= "</td>";
			}
			
			if(!$IPF_HIDE_COLUMN['COMMENT']){
				// [2015-0324-1420-39164] disable "Comment" if $attach_comment_disabled != ""
				$Table1 .= "<td align=\"center\" valign=\"top\">".$linterface->GET_TEXTAREA("TeacherComment[]", $TmpUserComment, 20, 5, "", "", $attach_comment_disabled)."</td>";
			}
			
			if(!$IPF_HIDE_COLUMN['STATUS']){
				// Create a status filter
				$StatusSelection = "<SELECT name=\"Status[]\"  >";
				//$StatusSelection .= "<OPTION value='' ".($TmpUserStatus==0?"SELECTED":"")."> -- </OPTION>";
				$StatusSelection .= "<OPTION value='1' ".($TmpUserStatus==1?"SELECTED":"").">".$ec_iPortfolio['pending']."</OPTION>";
				$StatusSelection .= "<OPTION value='2' ".($TmpUserStatus==2?"SELECTED":"").">".$ec_iPortfolio['approved']."</OPTION>";
				$StatusSelection .= "<OPTION value='3' ".($TmpUserStatus==3?"SELECTED":"").">".$ec_iPortfolio['rejected']."</OPTION>";
				$StatusSelection .= "<OPTION value='4' ".($TmpUserStatus==4?"SELECTED":"").">".$ec_iPortfolio['teacher_submit_record']."</OPTION>";
				$StatusSelection .= "</SELECT>";	
											
//				$StatusSelection .= "<input type='hidden' name='SortStudentID[]' value='".$TmpUserID."' />";
//				$StatusSelection .= "<input type='hidden' name='SortOleStudentRecordID[]' value='".$TmpRecordID."' />";
				
				# Eric Yip: Hide the status column (20081030)
				$Table1 .= "<td align=\"center\" valign=\"top\">".$StatusSelection."</td>";	
			}	else {
				if($sys_custom['IPF_HIDE_COLUMN']['SIS']){
					$Table1 .= "<input type='hidden' name=\"Status[]\" value='2' />";
				}
			}
			
			
			
			
			
			// Create a approved by
//			$ApprovedBySelection = "<SELECT name=\"ApprovedBy[]\"  >";
//			//$StatusSelection .= "<OPTION value='' ".($TmpUserStatus==0?"SELECTED":"")."> -- </OPTION>";
//			$ApprovedBySelection .= "<OPTION value='1' ".($TmpUserStatus==1?"SELECTED":"").">".$ec_iPortfolio['pending']."</OPTION>";
//			$ApprovedBySelection .= "<OPTION value='2' ".($TmpUserStatus==2?"SELECTED":"").">".$ec_iPortfolio['approved']."</OPTION>";
//			$ApprovedBySelection .= "<OPTION value='3' ".($TmpUserStatus==3?"SELECTED":"").">".$ec_iPortfolio['rejected']."</OPTION>";
//			$ApprovedBySelection .= "<OPTION value='4' ".($TmpUserStatus==4?"SELECTED":"").">".$ec_iPortfolio['teacher_submit_record']."</OPTION>";
//			$ApprovedBySelection .= "</SELECT>";	
										
//			$ApprovedBySelection .= "<input type='hidden' name='SortStudentID[]' value='".$TmpUserID."' />";

				
			$Table1 .= "</tr>";
		}
		$Table1 .= "</table>";
		

		return $Table1;
	}
	
	########## OLR Data Export ####################
	# Retrieve Student OLR Export Data
	# Villa[M98353] 2016-10-06: add CATEGORY_B5, SUBCATEGORY_B5, CATEGORY_EN, SUBCATEGORY_EN
	# Henry Yuen (2010-05-31): to avoid running out of memory, add parameters recordsPerLoop and startIndex for selection
	function returnStudentOLRExportData($Year, $IntExt="", $startIndex, $recordsPerLoop)
	{
		global $eclass_db, $intranet_db, $ec_iPortfolio, $profiles_to,$ipf_cfg,$iPort;
		global $intranet_session_language;
		
		$namefield1 = getNameFieldByLang2("iu.");
		$namefield2 = getNameFieldByLang2("iau.");
		$namefield_approvedBy = getNameFieldByLang2("iu_approvedBy.");
		$namefield_preferredApprover = getNameFieldByLang2("iu_preferredApprover.");
		
		$CategoryField = $this->get_OLR_Category_Field_For_Record("op.");
		$SubCategoryField = $this->get_OLR_SubCategory_Field_For_Record("op.");
		$AcademicYearNameField = Get_Lang_Selection('ay.YearNameB5', 'ay.YearNameEN');
		$displayLang = strtoupper($intranet_session_language);
	    if($displayLang =="B5" || $displayLang == "GB") {
	    	$displayLang = "B5";
	    }
		if($Year!="") {
			$cond = " AND ay.AcademicYearID = '".$Year."'";
		}
		# Note: not like other OLE functions - 1: Int, 2: Ext
		if($IntExt == 1)
			$cond .= " AND op.IntExt = 'INT'";
		else if($IntExt == 2)
			$cond .= " AND op.IntExt = 'EXT'";

		$sql = "SELECT
					IFNULL(iu.WebSAMSRegNo, iau.WebSAMSRegNo) as WebSAMSRegNo,
					IFNULL($namefield1, $namefield2) as UserName,
					IF ((op.StartDate IS NULL OR op.StartDate='0000-00-00'),'--',IF(op.EndDate IS NULL OR op.EndDate='0000-00-00',op.StartDate, CAST(CONCAT(op.StartDate,' $profiles_to ',op.EndDate) AS CHAR) )) as OLEDate,
					ayt.YearTermName".$displayLang." as YearTermName,
					op.Title,
					$CategoryField as Category,
					$SubCategoryField as SubCategory,
					a.Role,
					a.Hours,
					op.Details,
					IF(a.ProcessDate IS NOT NULL, DATE_FORMAT(a.ProcessDate,'%Y-%m-%d'),'--') as ProcessDate,
					op.SchoolRemarks AS Remark,
					a.ELE,
					a.Achievement,
					op.Organization,
					$AcademicYearNameField as StartDate,
					$AcademicYearNameField as EndDate,
          			a.TeacherComment,
					if(op.intext = '".$ipf_cfg["OLE_TYPE_STR"]["INT"]."','".$iPort["internal_record"]."','".$iPort["external_record"]."') as 'intExtType',
					SLPOrder,
					iu.ClassName,
					iu.ClassNumber,
					CASE a.RecordStatus
						WHEN 1 THEN '".$ec_iPortfolio['pending']."'
						WHEN 2 THEN '".$ec_iPortfolio['approved']."'
						WHEN 3 THEN '".$ec_iPortfolio['rejected']."'
						WHEN 4 THEN '".$ec_iPortfolio['teacher_submit_record']."'
					ELSE '--' END as RecordStatus,
					$namefield_approvedBy as ApprovedBy,
					$namefield_preferredApprover as PreferredApprover,
					IF (op.IsSAS = 1, 'Y', 'N') as IsSAS,
					oc.ChiTitle as CATEGORY_B5,
					oc.EngTitle as CATEGORY_EN,
					osc.ChiTitle as SUBCATEGORY_B5,
					osc.EngTitle as SUBCATEGORY_EN,
					iu_Creator.EnglishName as ProgramCreator_EN,
					iu_Creator.ChineseName as ProgramCreator_B5
				FROM
					{$eclass_db}.OLE_STUDENT as a
					LEFT JOIN {$eclass_db}.OLE_PROGRAM AS op ON a.ProgramID = op.ProgramID 
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay ON op.AcademicYearID = ay.AcademicYearID
					LEFT JOIN {$intranet_db}.INTRANET_USER AS iu ON a.UserID = iu.UserID
					LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER AS iau ON a.UserID = iau.UserID
					LEFT JOIN {$intranet_db}.ACADEMIC_YEAR_TERM ayt ON op.YearTermID = ayt.YearTermID
					LEFT JOIN {$intranet_db}.INTRANET_USER AS iu_approvedBy ON a.ApprovedBy = iu_approvedBy.UserID
					LEFT JOIN {$intranet_db}.INTRANET_USER AS iu_preferredApprover ON a.RequestApprovedBy = iu_preferredApprover.UserID
					LEFT JOIN {$eclass_db}.OLE_CATEGORY AS oc ON op.Category = oc.RecordID
					LEFT JOIN {$eclass_db}.OLE_SUBCATEGORY  AS osc ON op.SubCategoryID = osc.SubCatID
					LEFT JOIN {$intranet_db}.INTRANET_USER AS iu_Creator ON op.CreatorID = iu_Creator.UserID
				WHERE
					((iu.WebSAMSRegNo IS NOT NULL and trim(iu.WebSAMSRegNo) != '')OR (iau.WebSAMSRegNo IS NOT NULL and trim(iau.WebSAMSRegNo) != ''))
					$cond
				ORDER BY
					$namefield1,
					$namefield2,
					a.StartDate
				LIMIT $startIndex, $recordsPerLoop	
			";
					
		$row = $this->returnArray($sql);
		return $row;
	}
			
	# Begin Henry Yuen (2010-05-31): divide returnOLRExportContent function into two (one for printing header and one for printing content) 		
	function returnOLRExportHeader($PostValue){
		global $ec_iPortfolio, $Lang;
		
		$delimiter = "\t";
		$valueQuote = "";		
		
		if($PostValue['WebSAMSRegNo'] != ''){ 	
			$Content .= $valueQuote.$ec_iPortfolio['export']['WebSAMSRegNo'].$valueQuote.$delimiter;
		}
		if($PostValue['Programme'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Programme'].$valueQuote.$delimiter;
		}
		if($PostValue['ProgrammeDescription'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['ProgrammeDescription'].$valueQuote.$delimiter;
		}
		if($PostValue['SchoolYearFrom'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['SchoolYearFrom'].$valueQuote.$delimiter;
		}
		if($PostValue['SchoolYearTo'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['SchoolYearTo'].$valueQuote.$delimiter;
		}
		if($PostValue['ShowSemester'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['ShowSemester'].$valueQuote.$delimiter;
		}
		if($PostValue['Role'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Role'].$valueQuote.$delimiter;
		}
		if($PostValue['Organization'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Organization'].$valueQuote.$delimiter;
		}
		if($PostValue['Awards'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Awards'].$valueQuote.$delimiter;
		}
		if($PostValue['Student'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Student'].$valueQuote.$delimiter;
		}
		if($PostValue['ClassName'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['ClassName'].$valueQuote.$delimiter;
		}
		if($PostValue['ClassNumber'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['ClassNumber'].$valueQuote.$delimiter;
		}		
		if($PostValue['Period'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Period'].$valueQuote.$delimiter;
		}
		//Villa 2016-10-06 [M98353] Add the header of category and SubCategory in En and B5 respectivity in order to replace category and SubCategory
		
// 		if($PostValue['Category'] != ''){
// 			$Content .= $valueQuote.$ec_iPortfolio['export']['Category'].$valueQuote.$delimiter;
// 		}
// 		if($PostValue['SubCategory'] != ''){
// 			$Content .= $valueQuote.$ec_iPortfolio['sub_category'].$valueQuote.$delimiter;
// 		}
		if($PostValue['Category_B5'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Category'].'('.$ec_iPortfolio['chinese'].')'.$valueQuote.$delimiter;
		}
		if($PostValue['SubCategory_B5'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['sub_category'].'('.$ec_iPortfolio['chinese'].')'.$valueQuote.$valueQuote.$delimiter;
		}
		if($PostValue['Category_EN'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Category'].'('.$ec_iPortfolio['english'].')'.$valueQuote.$delimiter;
		}
		if($PostValue['SubCategory_EN'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['sub_category'].'('.$ec_iPortfolio['english'].')'.$valueQuote.$delimiter;
		}
		
		if($PostValue['Hours'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Hours'].$valueQuote.$delimiter;
		}
		if($PostValue['ApprovedBy'] != ''){
			$Content .= $valueQuote.$Lang['iPortfolio']['auditby'].$valueQuote.$delimiter;
		}
		if($PostValue['PreferredApprover'] != ''){
			$Content .= $valueQuote.$Lang['iPortfolio']['preferred_approver'].$valueQuote.$delimiter;
		}
		if($PostValue['Status'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Status'].$valueQuote.$delimiter;
		}
		if($PostValue['ApprovedDate'] != ''){
			$Content .= $valueQuote.$Lang['iPortfolio']['auditdate'].$valueQuote.$delimiter;
		}
		if($PostValue['Remarks'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Remarks'].$valueQuote.$delimiter;
		}
		if($PostValue['ELE'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['ELE'].$valueQuote.$delimiter;
		}
		if($PostValue['Comment'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Comment'].$valueQuote.$delimiter;
		}		
		if($PostValue['intExtType'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['record_type'].$valueQuote.$delimiter;
		}		
		if($PostValue['SLPOrder'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['SLPOrder_csv'].$valueQuote.$delimiter;
		}	
		if($PostValue['IsSAS'] != ''){
			$Content .= $valueQuote.$Lang['iPortfolio']['OLE']['SAS'].$valueQuote;
		}
		if($PostValue['ProgramCreator'] != ''){
			$Content.= $valueQuote.$Lang['iPortfolio']['OLE']['ProgramCreator'].$valueQuote;
		}

		$Content .= "\n";
		
		return $Content;		
	}
		
	# build activity export content
	function returnOLRExportData($Year, $IntExt="", $PostValue, $DataArray)
	{
		global $ec_iPortfolio, $no_record_msg;

		$delimiter = "\t";
		$valueQuote = "";
		
		$DefaultELEArray = $this->GET_ELE();
		$Content = '';		
		if(sizeof($DataArray)!=0)
		{			
			for($i=0; $i<sizeof($DataArray); $i++)
			{
				//$RegNo = (substr($DataArray[$i]["WebSAMSRegNo"],0,1)!="#") ? "#".$DataArray[$i]["WebSAMSRegNo"] : $DataArray[$i]["WebSAMSRegNo"];
				$RegNo = (substr($DataArray[$i]["WebSAMSRegNo"],0,1)!="#") ? $DataArray[$i]["WebSAMSRegNo"] : substr($DataArray[$i]["WebSAMSRegNo"],1, strlen($DataArray[$i]["WebSAMSRegNo"]));
				$ELEIDArray = explode(",", $DataArray[$i]["ELE"]);

				//REMOVE ANY TAB INSIDE DATA, IF DON'T REMOVE , COLUMN WILL BE SHIFTED
				foreach($DataArray[$i] as $fieldName=>$data){
					$DataArray[$i][$fieldName] = str_replace("\t",' ',trim($data));
				}

				unset($ELEArray);
				for($m=0; $m<sizeof($ELEIDArray); $m++)
				{
					
					$tmp = trim($ELEIDArray[$m]);
					$ELEArray[] = $DefaultELEArray[$tmp];
				}
				$DisplayELE = (!empty($ELEArray)) ? implode(",", $ELEArray) : "";
	
				$t_Content = '';
				if($PostValue['WebSAMSRegNo'] != ''){ 	
					$t_Content .= $valueQuote.$RegNo.$valueQuote.$delimiter;				
				}
				if($PostValue['Programme'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Title"]).$valueQuote.$delimiter;				
				}
				if($PostValue['ProgrammeDescription'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Details"]).$valueQuote.$delimiter;				
				}
				if($PostValue['SchoolYearFrom'] != ''){
					//$t_Content .= $valueQuote.substr($DataArray[$i]["StartDate"], 0, 4).$valueQuote.$delimiter;			
					$t_Content .= $valueQuote.$DataArray[$i]["StartDate"].$valueQuote.$delimiter;
				}
				if($PostValue['SchoolYearTo'] != ''){
					//$t_Content .= $valueQuote.substr($DataArray[$i]["EndDate"], 0, 4).$valueQuote.$delimiter;
					$t_Content .= $valueQuote.$DataArray[$i]["EndDate"].$valueQuote.$delimiter;
				}
				if($PostValue['ShowSemester'] != ''){
					//$t_Content .= $valueQuote.substr($DataArray[$i]["EndDate"], 0, 4).$valueQuote.$delimiter;
					$t_Content .= $valueQuote.$DataArray[$i]["YearTermName"].$valueQuote.$delimiter;
				}
				if($PostValue['Role'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Role"]).$valueQuote.$delimiter;				
				}
				if($PostValue['Organization'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Organization"]).$valueQuote.$delimiter;				
				}
				if($PostValue['Awards'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["Achievement"].$valueQuote.$delimiter;				
				}
				if($PostValue['Student'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["UserName"].$valueQuote.$delimiter;				
				}
				if($PostValue['ClassName'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["ClassName"].$valueQuote.$delimiter;				
				}
				if($PostValue['ClassNumber'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["ClassNumber"].$valueQuote.$delimiter;				
				}
				if($PostValue['Period'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["OLEDate"].$valueQuote.$delimiter;				
				}

				//Villa 2016-10-06 [M98353] Add the content of category and SubCategory in En and B5 respectivity in order to replace category and SubCategory
// 				if($PostValue['Category'] != ''){
// 					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Category"]).$valueQuote.$delimiter;
// 				}
// 				if($PostValue['SubCategory'] != ''){
// 					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["SubCategory"]).$valueQuote.$delimiter;
// 				}
				if($PostValue['Category_B5'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["CATEGORY_B5"].$valueQuote.$delimiter;
				}
				if($PostValue['SubCategory_B5'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["SUBCATEGORY_B5"].$valueQuote.$delimiter;
				}
				if($PostValue['Category_EN'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["CATEGORY_EN"].$valueQuote.$delimiter;
				}
				if($PostValue['SubCategory_EN'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["SUBCATEGORY_EN"].$valueQuote.$delimiter;
				}
				
				if($PostValue['Hours'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["Hours"].$valueQuote.$delimiter;				
				}
				if($PostValue['ApprovedBy'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["ApprovedBy"].$valueQuote.$delimiter;				
				}
				if($PostValue['PreferredApprover'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["PreferredApprover"].$valueQuote.$delimiter;				
				}
				if($PostValue['Status'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["RecordStatus"].$valueQuote.$delimiter;				
				}
				if($PostValue['ApprovedDate'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["ProcessDate"].$valueQuote.$delimiter;				
				}
				if($PostValue['Remarks'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Remark"]).$valueQuote.$delimiter;				
				}
				if($PostValue['ELE'] != ''){
					$t_Content .= $valueQuote.$DisplayELE.$valueQuote.$delimiter;				
				}
				if($PostValue['Comment'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["TeacherComment"]).$valueQuote.$delimiter;		
				}							
				if($PostValue['intExtType'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["intExtType"]).$valueQuote.$delimiter;		
				}							
				if($PostValue['SLPOrder'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["SLPOrder"]).$valueQuote.$delimiter;		
				}							
				if($PostValue['IsSAS'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["IsSAS"]).$valueQuote;		
				}
				if($PostValue['ProgramCreator'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent(Get_Lang_Selection($DataArray[$i]["ProgramCreator_B5"], $DataArray[$i]["ProgramCreator_EN"])).$valueQuote;		
				}
				$Content .= preg_replace("/[\r\n]/", " ", $t_Content)."\n";			
			}
		}	
		else
		{
			$Content .= $valueQuote.$no_record_msg.$valueQuote."\n";
		}
 		return $Content;
	}
	
	function returnOLEnAWARDUserID($startIndex, $usersPerLoop, $studentType, $Year, $StudentID, $exportAlumni=false){
		global $eclass_db, $intranet_db;
		if($studentType){
			$conds = "INNER JOIN {$intranet_db}.INTRANET_USER as u ON u.UserID = os.UserID";
			$ayCondsOP = " AND op.AcademicYearID  in (".implode(',',(array)$Year).")";
			$ayCondsOS = " AND os.AcademicYearID  in (".implode(',',(array)$Year).")";
			if($StudentID){
				$userIDCond = " AND u.UserID in (".implode(',',$StudentID).") ";
			}
		}
		if($exportAlumni){
			if($StudentID){
				$userIDCond = " AND os.UserID in (".implode(',',$StudentID).") ";
			}
		}
		$sql = "select UserID from (
				select distinct os.UserID from {$eclass_db}.OLE_STUDENT as os 
				$conds
				LEFT JOIN {$eclass_db}.OLE_PROGRAM as op ON os.ProgramID = op.ProgramID
				INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay ON op.AcademicYearID = ay.AcademicYearID
				where 1 $ayCondsOP $userIDCond
				union
				select distinct os.UserID from {$eclass_db}.AWARD_STUDENT as os
				$conds
				where 1 $ayCondsOS $userIDCond) as T 
				limit $startIndex, $usersPerLoop";
		//	debug_r($sql);	
		return $this->returnVector($sql);
				
	}
	
	function getPersonalInfo_DGS($UserIDAry){
		global $intranet_db, $ec_iPortfolio, $profiles_to,$ipf_cfg,$iPort;
		global $intranet_session_language;
		
		$namefieldEN = getNameFieldByLang2("iu.","en");
		$namefieldEN2 = getNameFieldByLang2("iau.","en");
		$namefieldCH = getNameFieldByLang2("iu.","b5");
		$namefieldCH2 = getNameFieldByLang2("iau.","b5");
		$sql = "SELECT * FROM(
					SELECT
						iu.UserID as UserID,
						$namefieldEN as EnglishName,
						$namefieldCH as ChineseName,
						DATE_FORMAT(iu.DateOfBirth,\"%Y-%m-%d\") as DOB,
						iups.AdmissionDate as AdmissionDate,
						iu.HKID as HKID,
						iu.WebSAMSRegNo as WebSAMSRegNo,
						iu.ClassName as ClassName,
						iu.ClassNumber as ClassNumber
					FROM
						{$intranet_db}.INTRANET_USER iu
						LEFT JOIN {$intranet_db}.INTRANET_USER_PERSONAL_SETTINGS AS iups ON iups.UserID = iu.UserID
					WHERE
						iu.UserID in (".implode(',',(array)($UserIDAry)).")
					UNION
					SELECT
						iau.UserID as UserID,
						$namefieldEN2 as EnglishName,
						$namefieldCH2 as ChineseName,
						DATE_FORMAT(iau.DateOfBirth,\"%Y-%m-%d\") as DOB,
						iups.AdmissionDate as AdmissionDate,
						'' as HKID,
						iau.WebSAMSRegNo as WebSAMSRegNo,
						iau.ClassName as ClassName,
						iau.ClassNumber as ClassNumber
					FROM
						{$intranet_db}.INTRANET_ARCHIVE_USER iau
						LEFT JOIN {$intranet_db}.INTRANET_USER_PERSONAL_SETTINGS AS iups ON iups.UserID = iau.UserID
					WHERE
						iau.UserID in (".implode(',',(array)($UserIDAry)).")
				) as T order by T.ClassName, T.ClassNumber
			";
		//	debug_r($sql);
		$personaInfoAry = $this->returnArray($sql);
		return $personaInfoAry;
	}
	
	function getAwardStudent_DGS($UserIDAry, $Year=''){
		global $intranet_session_language, $intranet_db, $eclass_db;
		$displayLang = strtoupper($intranet_session_language);
	    if($displayLang =="B5" || $displayLang == "GB") {
	    	$displayLang = "B5";
	    }
		if($Year!="") {
			$cond = " AND ads.AcademicYearID in (".implode(',',(array)$Year).")";
		}
		$sql = "Select
					ads.UserID as UserID,
					ads.AwardName as AwardName,
					ay.YearName$displayLang as AwardYearName
				From	
					{$eclass_db}.AWARD_STUDENT AS ads
					LEFT JOIN {$intranet_db}.INTRANET_USER AS iu ON ads.UserID = iu.UserID
					LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER AS iau ON ads.UserID = iau.UserID
					LEFT JOIN {$intranet_db}.ACADEMIC_YEAR ay ON ay.AcademicYearID = ads.AcademicYearID
				where
					ads.UserID in (".implode(',',(array)$UserIDAry).")
					$cond
				order by		
					ads.UserID, AwardName
					";
					
		$awardAry = $this->returnArray($sql);
		return $awardAry;
	}
	
	function getOLE_DGS($UserIDAry, $Year=''){
		global $eclass_db, $intranet_db, $ec_iPortfolio, $profiles_to,$ipf_cfg,$iPort;
		global $intranet_session_language;
		$CategoryField = $this->get_OLR_Category_Field_For_Record("op.");
		$displayLang = strtoupper($intranet_session_language);
	    if($displayLang =="B5" || $displayLang == "GB") {
	    	$displayLang = "B5";
	    }
		if($Year!="") {
			$cond = " AND ay.AcademicYearID in (".implode(',',(array)$Year).")";
		}
		$sql = "SELECT
					a.UserID as UserID,
					IF ((op.StartDate IS NULL OR op.StartDate='0000-00-00'),'--',IF(op.EndDate IS NULL OR op.EndDate='0000-00-00',op.StartDate, CAST(CONCAT(op.StartDate,' $profiles_to ',op.EndDate) AS CHAR) )) as OLEDate,
					ay.YearName$displayLang as YearName,
					op.Title,
					$CategoryField as Category,
					a.Role,
					a.ELE,
					a.Achievement,
					a.IntExt,
					op.Organization
				FROM
					{$eclass_db}.OLE_STUDENT as a
					LEFT JOIN {$eclass_db}.OLE_PROGRAM AS op ON a.ProgramID = op.ProgramID 
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay ON op.AcademicYearID = ay.AcademicYearID
				WHERE
					a.UserID in (".implode(',',(array)$UserIDAry).")
					$cond
				ORDER BY
					a.UserID,
					a.IntExt,
					ay.Sequence,
					op.Title,
					$CategoryField
			";
		//	debug_r($sql);
		$OLEAry = $this->returnArray($sql);
		return $OLEAry;
	}
	
	function returnStudentOLEExportData_DGS($Year, $IntExt="", $UserIDAry)
	{
		global $eclass_db, $intranet_db, $ec_iPortfolio, $profiles_to,$ipf_cfg,$iPort;
		global $intranet_session_language;
		
		$personaInfoAry = $this->getPersonalInfo_DGS($UserIDAry);
		$awardAry = $this->getAwardStudent_DGS($UserIDAry, $Year);
		$OLEAry = $this->getOLE_DGS($UserIDAry, $Year);
		
		return array($personaInfoAry,$awardAry,$OLEAry);
	}
	
	function returnOLEExportData_DGS($Year, $IntExt="", $PostValue, $DataArray)
	{
		global $ec_iPortfolio, $no_record_msg;
		global $intranet_session_language;

		$delimiter = "\t";
		$valueQuote = "\"";
		$Content = "";
		
	    if(strtoupper($intranet_session_language)=='EN') {
			$DefaultELEArray = $this->GET_ELE(false,'en');
	    }else{
			$DefaultELEArray = $this->GET_ELE(false,'b5');
	    }
						
		$Content = '';	
		$t_UserID = '';
		
		list($personaInfoAry,$awardAryRaw,$OLEAryRaw) = $DataArray;
		
		$awardAry = array();
		$OLEAry = array();
		
		foreach((array)$awardAryRaw as $ele){
			foreach($ele as &$val){
				$val = str_replace("\t",' ',$val);
			}
			$awardAry[$ele['UserID']][] = $ele;
		}
		
		foreach((array)$OLEAryRaw as $ele){
			foreach($ele as &$val){
				$val = str_replace("\t",' ',$val);
			}
			$OLEAry[$ele['UserID']][$ele['IntExt']][] = $ele;
		}
		
		foreach((array)$personaInfoAry as $PIele){
			foreach($PIele as &$val){
				$val = str_replace("\t",' ',$val);
			}
			$t_userID = $PIele['UserID'];
			$t_EnglishName = $PIele['EnglishName']; 
			$t_ChineseName = $PIele['ChineseName'];
			$t_DOB = $PIele['DOB']; 
			$t_addmissionDate = $PIele['AdmissionDate']; 
			$t_HKID = $PIele['HKID']; 
			$RegNo = $PIele['WebSAMSRegNo'];; 
			$t_ClassName = $PIele['ClassName'];; 
			$t_ClassNo = $PIele['ClassNumber'];
			
			for($i=0,$i_max=max(count($awardAry[$t_userID]),count($OLEAry[$t_userID]['INT']),count($OLEAry[$t_userID]['EXT']));$i<$i_max;$i++){
				$t_Content = '';
				//English Name
				$t_Content .= $valueQuote.handleCSVExportContent($t_EnglishName).$valueQuote.$delimiter; 
				//Chinese Name
				$t_Content .= $valueQuote.handleCSVExportContent($t_ChineseName).$valueQuote.$delimiter;
				//Date of Birth
				$t_Content .= $valueQuote.handleCSVExportContent($t_DOB).$valueQuote.$delimiter;
				//Admission Date
				$t_Content .= $valueQuote.handleCSVExportContent($t_addmissionDate).$valueQuote.$delimiter;
				//HKID
				$t_Content .= $valueQuote.handleCSVExportContent($t_HKID).$valueQuote.$delimiter;
				//WebSAMSRegNo 	
				$t_Content .= $valueQuote.$RegNo.$valueQuote.$delimiter;
				//Award Title
				$t_Content .= $valueQuote.handleCSVExportContent($awardAry[$t_userID][$i]["AwardName"]?$awardAry[$t_userID][$i]["AwardName"]:"").$valueQuote.$delimiter;
				//Academic Year
				$t_Content .= $valueQuote.handleCSVExportContent($awardAry[$t_userID][$i]["AwardYearName"]?$awardAry[$t_userID][$i]["AwardYearName"]:"").$valueQuote.$delimiter;
				//ClassName
				$t_Content .= $valueQuote.handleCSVExportContent($t_ClassName).$valueQuote.$delimiter;
				//ClassNumber
				$t_Content .= $valueQuote.handleCSVExportContent($t_ClassNo).$valueQuote.$delimiter;
				//Award Title Ext
				$t_Content .= $valueQuote.handleCSVExportContent($OLEAry[$t_userID]['EXT'][$i]["Title"]?$OLEAry[$t_userID]['EXT'][$i]["Title"]:"").$valueQuote.$delimiter;
				//Academic Year
				$t_Content .= $valueQuote.handleCSVExportContent($OLEAry[$t_userID]['EXT'][$i]["YearName"]?$OLEAry[$t_userID]['EXT'][$i]["YearName"]:"").$valueQuote.$delimiter;
				//Role
				$t_Content .= $valueQuote.handleCSVExportContent($OLEAry[$t_userID]['EXT'][$i]["Role"]?$OLEAry[$t_userID]['EXT'][$i]["Role"]:"").$valueQuote.$delimiter;
				//Achievement
				$t_Content .= $valueQuote.handleCSVExportContent($OLEAry[$t_userID]['EXT'][$i]["Achievement"]?$OLEAry[$t_userID]['EXT'][$i]["Achievement"]:"").$valueQuote.$delimiter;
				//Category
				$t_Content .= $valueQuote.handleCSVExportContent($OLEAry[$t_userID]['EXT'][$i]["Category"]?$OLEAry[$t_userID]['EXT'][$i]["Category"]:"").$valueQuote.$delimiter;
				//Organization
				$t_Content .= $valueQuote.handleCSVExportContent($OLEAry[$t_userID]['EXT'][$i]["Organization"]?$OLEAry[$t_userID]['EXT'][$i]["Organization"]:"").$valueQuote.$delimiter;
				//OLE
				$t_Content .= $valueQuote.handleCSVExportContent($OLEAry[$t_userID]['INT'][$i]["Title"]?$OLEAry[$t_userID]['INT'][$i]["Title"]:"").$valueQuote.$delimiter;
				//Academic Year
				$t_Content .= $valueQuote.handleCSVExportContent($OLEAry[$t_userID]['INT'][$i]["YearName"]?$OLEAry[$t_userID]['INT'][$i]["YearName"]:"").$valueQuote.$delimiter;
				//Role
				$t_Content .= $valueQuote.handleCSVExportContent($OLEAry[$t_userID]['INT'][$i]["Role"]?$OLEAry[$t_userID]['INT'][$i]["Role"]:"").$valueQuote.$delimiter;
				//ELE
				$t_Content .= $valueQuote.$OLEAry[$t_userID]['INT'][$i]["ELE"].$valueQuote.$delimiter;
				//Achievement
				$t_Content .= $valueQuote.handleCSVExportContent($OLEAry[$t_userID]['INT'][$i]["Achievement"]?$OLEAry[$t_userID]['INT'][$i]["Achievement"]:"").$valueQuote.$delimiter;
				//Organization
				$t_Content .= $valueQuote.handleCSVExportContent($OLEAry[$t_userID]['INT'][$i]["Organization"]?$OLEAry[$t_userID]['INT'][$i]["Organization"]:"").$valueQuote.$delimiter;
				
				$Content .= preg_replace("/[\r\n]/", " ", $t_Content)."\n";	
			}
		}
		
 		return $Content;
	}
	
	function returnOLRExportData_WebSAMS($Year, $IntExt="", $PostValue, $DataArray)
	{
		global $ec_iPortfolio, $no_record_msg;

		$delimiter = "\t";
		$valueQuote = "\"";
		
		$DefaultELEArrayEN = $this->GET_ELE(false,'en');
		$DefaultELEArrayCH = $this->GET_ELE(false,'b5');
						
		$Content = '';		
		if(sizeof($DataArray)!=0)
		{			
			for($i=0; $i<sizeof($DataArray); $i++)
			{
				//$RegNo = (substr($DataArray[$i]["WebSAMSRegNo"],0,1)!="#") ? "#".$DataArray[$i]["WebSAMSRegNo"] : $DataArray[$i]["WebSAMSRegNo"];
				$RegNo = (substr($DataArray[$i]["WebSAMSRegNo"],0,1)!="#") ? $DataArray[$i]["WebSAMSRegNo"] : substr($DataArray[$i]["WebSAMSRegNo"],1, strlen($DataArray[$i]["WebSAMSRegNo"]));
				$ELEIDArray = explode(",", $DataArray[$i]["ELE"]);

				//REMOVE ANY TAB INSIDE DATA, IF DON'T REMOVE , COLUMN WILL BE SHIFTED
				foreach($DataArray[$i] as $fieldName=>$data){
					$DataArray[$i][$fieldName] = str_replace("\t",' ',trim($data));
				}

				$ELEArrayEN = array();
				$ELEArrayCH = array();
				for($m=0; $m<sizeof($ELEIDArray); $m++)
				{
					
					$tmp = trim($ELEIDArray[$m]);
					$ELEArrayEN[] = $DefaultELEArrayEN[$tmp];
					$ELEArrayCH[] = $DefaultELEArrayCH[$tmp];
				}
				$DisplayELEEN = (!empty($ELEArrayEN)) ? implode(",", $ELEArrayEN) : "";
				$DisplayELECH = (!empty($ELEArrayCH)) ? implode(",", $ELEArrayCH) : "";
	
				$t_Content = '';
				//WebSAMSRegNo 	
				$t_Content .= $valueQuote.$RegNo.$valueQuote.$delimiter;
				//Programme
				$programmeTitle = $this->stringLanguageSplitByWords($DataArray[$i]["Title"]);
				$t_Content .= $valueQuote.handleCSVExportContent($programmeTitle[0]).$valueQuote.$delimiter;
				$t_Content .= $valueQuote.handleCSVExportContent($programmeTitle[1]).$valueQuote.$delimiter;
				//ProgrammeDescription
				$programmeDescription = $this->stringLanguageSplitByWords($DataArray[$i]["Details"]);
				$t_Content .= $valueQuote.handleCSVExportContent($programmeDescription[0]).$valueQuote.$delimiter;
				$t_Content .= $valueQuote.handleCSVExportContent($programmeDescription[1]).$valueQuote.$delimiter;
				//SchoolYearFrom		
				$t_Content .= $valueQuote.$DataArray[$i]["StartDate"].$valueQuote.$delimiter;
				//SchoolYearTo
				$t_Content .= $valueQuote.$DataArray[$i]["EndDate"].$valueQuote.$delimiter;
				//Role
				$Role = $this->stringLanguageSplitByWords($DataArray[$i]["Role"]);
				$t_Content .= $valueQuote.handleCSVExportContent($Role[0]).$valueQuote.$delimiter;
				$t_Content .= $valueQuote.handleCSVExportContent($Role[1]).$valueQuote.$delimiter;				
				//Organization
				$Organization = $this->stringLanguageSplitByWords($DataArray[$i]["Organization"]);
				$t_Content .= $valueQuote.handleCSVExportContent($Organization[0]).$valueQuote.$delimiter;
				$t_Content .= $valueQuote.handleCSVExportContent($Organization[1]).$valueQuote.$delimiter;				
				//ELE
				$t_Content .= $valueQuote.$DisplayELEEN.$valueQuote.$delimiter;	
				$t_Content .= $valueQuote.$DisplayELECH.$valueQuote.$delimiter;		
				//Awards
				$Awards = $this->stringLanguageSplitByWords($DataArray[$i]["Achievement"]);
				$t_Content .= $valueQuote.handleCSVExportContent($Awards[0]).$valueQuote.$delimiter;		
				$t_Content .= $valueQuote.handleCSVExportContent($Awards[1]).$valueQuote.$delimiter;				
				////
				//$t_Content .= $valueQuote.$DataArray[$i]["ApprovedBy"].$valueQuote.$delimiter;				


				$Content .= preg_replace("/[\r\n]/", " ", $t_Content)."\n";			
			}
		}	
		else
		{
			$Content .= $valueQuote.$no_record_msg.$valueQuote."\n";
		}
 		return $Content;
	}
	
	/*
	# Henry Yuen (2010-05-31): deprecated method 
	# build activity export content
	function returnOLRExportContent($Year, $IntExt="", $PostValue, $startIndex, $recordsPerLoop)
	{
		global $ec_iPortfolio, $no_record_msg;

		$delimiter = "\t";
		$valueQuote = "";

		$DefaultELEArray = $this->GET_ELE();
		$DataArray = $this->returnStudentOLRExportData($Year, $IntExt, $startIndex, $recordsPerLoop);
				
		if($PostValue['WebSAMSRegNo'] != ''){ 	
			$Content .= $valueQuote.$ec_iPortfolio['export']['WebSAMSRegNo'].$valueQuote.$delimiter;
		}
		if($PostValue['Programme'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Programme'].$valueQuote.$delimiter;
		}
		if($PostValue['ProgrammeDescription'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['ProgrammeDescription'].$valueQuote.$delimiter;
		}
		if($PostValue['SchoolYearFrom'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['SchoolYearFrom'].$valueQuote.$delimiter;
		}
		if($PostValue['SchoolYearTo'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['SchoolYearTo'].$valueQuote.$delimiter;
		}
		if($PostValue['Role'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Role'].$valueQuote.$delimiter;
		}
		if($PostValue['Organization'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Organization'].$valueQuote.$delimiter;
		}
		if($PostValue['Awards'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Awards'].$valueQuote.$delimiter;
		}
		if($PostValue['Student'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Student'].$valueQuote.$delimiter;
		}
		if($PostValue['ClassName'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['ClassName'].$valueQuote.$delimiter;
		}
		if($PostValue['ClassNumber'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['ClassNumber'].$valueQuote.$delimiter;
		}		
		if($PostValue['Period'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Period'].$valueQuote.$delimiter;
		}
		if($PostValue['Category'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Category'].$valueQuote.$delimiter;
		}
		if($PostValue['Hours'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Hours'].$valueQuote.$delimiter;
		}
		if($PostValue['ApprovedBy'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['ApprovedBy'].$valueQuote.$delimiter;
		}
		if($PostValue['Status'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Status'].$valueQuote.$delimiter;
		}
		if($PostValue['ApprovedDate'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['ApprovedDate'].$valueQuote.$delimiter;
		}
		if($PostValue['Remarks'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['Remarks'].$valueQuote.$delimiter;
		}
		if($PostValue['ELE'] != ''){
			$Content .= $valueQuote.$ec_iPortfolio['export']['ELE'].$valueQuote.$delimiter;
		}		
		$Content .= "\n";
		
		if(sizeof($DataArray)!=0)
		{
			for($i=0; $i<sizeof($DataArray); $i++)
			{
				//$RegNo = (substr($DataArray[$i]["WebSAMSRegNo"],0,1)!="#") ? "#".$DataArray[$i]["WebSAMSRegNo"] : $DataArray[$i]["WebSAMSRegNo"];
				$RegNo = (substr($DataArray[$i]["WebSAMSRegNo"],0,1)!="#") ? $DataArray[$i]["WebSAMSRegNo"] : substr($DataArray[$i]["WebSAMSRegNo"],1, strlen($DataArray[$i]["WebSAMSRegNo"]));
				$ELEIDArray = explode($delimiter, $DataArray[$i]["ELE"]);
				unset($ELEArray);
				for($m=0; $m<sizeof($ELEIDArray); $m++)
				{
					$tmp = trim($ELEIDArray[$m]);
					$ELEArray[] = $DefaultELEArray[$tmp];
				}
				$DisplayELE = (!empty($ELEArray)) ? implode($delimiter, $ELEArray) : "";
	
				$t_Content = '';
				if($PostValue['WebSAMSRegNo'] != ''){ 	
					$t_Content .= $valueQuote.$RegNo.$valueQuote.$delimiter;				
				}
				if($PostValue['Programme'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Title"]).$valueQuote.$delimiter;				
				}
				if($PostValue['ProgrammeDescription'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Details"]).$valueQuote.$delimiter;				
				}
				if($PostValue['SchoolYearFrom'] != ''){
					$t_Content .= $valueQuote.substr($DataArray[$i]["StartDate"], 0, 4).$valueQuote.$delimiter;			
				}
				if($PostValue['SchoolYearTo'] != ''){
					$t_Content .= $valueQuote.substr($DataArray[$i]["EndDate"], 0, 4).$valueQuote.$delimiter;
				}
				if($PostValue['Role'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Role"]).$valueQuote.$delimiter;				
				}
				if($PostValue['Organization'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Organization"]).$valueQuote.$delimiter;				
				}
				if($PostValue['Awards'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["Achievement"].$valueQuote.$delimiter;				
				}
				if($PostValue['Student'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["UserName"].$valueQuote.$delimiter;				
				}
				if($PostValue['ClassName'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["ClassName"].$valueQuote.$delimiter;				
				}
				if($PostValue['ClassNumber'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["ClassNumber"].$valueQuote.$delimiter;				
				}
				if($PostValue['Period'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["OLEDate"].$valueQuote.$delimiter;				
				}
				if($PostValue['Category'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Category"]).$valueQuote.$delimiter;				
				}
				if($PostValue['Hours'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["Hours"].$valueQuote.$delimiter;				
				}
				if($PostValue['ApprovedBy'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["ApprovedBy"].$valueQuote.$delimiter;				
				}
				if($PostValue['Status'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["RecordStatus"].$valueQuote.$delimiter;				
				}
				if($PostValue['ApprovedDate'] != ''){
					$t_Content .= $valueQuote.$DataArray[$i]["ProcessDate"].$valueQuote.$delimiter;				
				}
				if($PostValue['Remarks'] != ''){
					$t_Content .= $valueQuote.handleCSVExportContent($DataArray[$i]["Remark"]).$valueQuote.$delimiter;				
				}
				if($PostValue['ELE'] != ''){
					$t_Content .= $valueQuote.$DisplayELE.$valueQuote;				
				}		
																		
				$Content .= preg_replace("/[\r\n]/", " ", $t_Content)."\n";
			}
		}
		else
		{
			$Content .= $valueQuote.$no_record_msg.$valueQuote."\n";
		}
//debug_r($Content);exit();
 
 		return $Content;
	}	
	*/
	# End Henry Yuen (2010-05-31): divide returnOLRExportContent function into two (one for printing header and one for printing content)	
	

############################################################################
##############################  Self Account  ##############################
############################################################################

	# Add self account
	function ADD_SELF_ACCOUNT($ParSelfAccountArr){
		global $eclass_db;

		$sql =	"
							INSERT INTO
								{$eclass_db}.SELF_ACCOUNT_STUDENT
									(UserID, Title, Details, InputDate, ModifiedDate)
							VALUES
								('".$ParSelfAccountArr['UserID']."', '".$ParSelfAccountArr['Title']."', '".$ParSelfAccountArr['Details']."', NOW(), NOW())
						";
		return $this->db_db_query($sql);
	}
	
	# Get self account list
	function GET_SELF_ACCOUNT_LIST($ParUserID, $DefaultSAOnly=0,$OrderByDefaultSA=false){
		global $eclass_db;
		
		$conds_DefaultSA = '';
		if ($DefaultSAOnly == 1)
		{
			$conds_DefaultSA = " And DefaultSA = 'SLP' ";
		}
			
		$orderBy = 'InputDate DESC';
		if($OrderByDefaultSA)
		{
			$orderBy = 'DefaultSA DESC,ModifiedDate DESC';
		}

		$sql =	"
							SELECT
								RecordID,
								Title,
								Details,
								AttachmentPath,
								ModifiedDate,
								DefaultSA
							FROM
								{$eclass_db}.SELF_ACCOUNT_STUDENT
							WHERE
								UserID = '".$ParUserID."'
								$conds_DefaultSA
							ORDER BY
								$orderBy
						";
		$ReturnArr = $this->returnArray($sql);

		return $ReturnArr;
	}
	
	# Get self account data
	function GET_SELF_ACCOUNT($ParRecordID){
		global $eclass_db;

		$sql =	"
							SELECT
								Title,
								Details,
								AttachmentPath,
								UserID
							FROM
								{$eclass_db}.SELF_ACCOUNT_STUDENT
							WHERE
								RecordID = '".$ParRecordID."'
						";
		$ReturnArr = $this->returnArray($sql);

		return $ReturnArr[0];
	}
	
	# Edit self account
	function EDIT_SELF_ACCOUNT($ParSelfAccountArr){
		global $eclass_db, $ipf_cfg;

		$sql =	"
							UPDATE
								{$eclass_db}.SELF_ACCOUNT_STUDENT
							SET
								Title = '".$ParSelfAccountArr['Title']."',
								Details = '".$ParSelfAccountArr['Details']."',
								ModifiedDate = NOW(), RecordStatus = '".$ipf_cfg["DB_SELF_ACCOUNT_STUDENT_RecordStatus"]["Normal"]."' ,ApprovedBy = NULL, ApprovedDate = NULL
							WHERE
								RecordID = '".$ParSelfAccountArr['RecordID']."'
						";
		return $this->db_db_query($sql);
	}
	
	# Edit self account
	function DELETE_SELF_ACCOUNT($ParRecordID){
		global $eclass_db;

		$sql =	"
							DELETE FROM
								{$eclass_db}.SELF_ACCOUNT_STUDENT
							WHERE
								RecordID = '".$ParRecordID."'
						";
		return $this->db_db_query($sql);
	}
	
	# Set default self account
	function SET_DEFAULT_SELF_ACCOUNT($ParUserID, $ParNewDefaultArr){
		global $eclass_db;

		# Reset all the default self account of the student
		$sql =	"
							UPDATE
								{$eclass_db}.SELF_ACCOUNT_STUDENT
							SET
								DefaultSA = NULL
							WHERE
								UserID = '".$ParUserID."'
						";
		$this->db_db_query($sql);

		# Update default self account by user input
		if(is_array($ParNewDefaultArr))
		{
			foreach($ParNewDefaultArr as $RecordID => $NewDefaultSAArr)
			{
				$NewDefaultSAStr = "'".implode(",", $NewDefaultSAArr)."'";

				$sql =	"
									UPDATE
										{$eclass_db}.SELF_ACCOUNT_STUDENT
									SET
										DefaultSA = ".$NewDefaultSAStr."
									WHERE
										RecordID = '".$RecordID."'
								";
				$this->db_db_query($sql);
			}
		}
	}
	
	# Get current default self account setting
	function GET_DEFAULT_SELF_ACCOUNT($ParUserID,$orderByDefaultSA=false)
	{
		global $eclass_db;

		$orderBy = 'InputDate DESC';
		if($orderByDefaultSA)
		{
			$orderBy = 'DefaultSA DESC , ModifiedDate DESC';
		}

		$sql =	"
							SELECT
								RecordID,
								DefaultSA
							FROM
								{$eclass_db}.SELF_ACCOUNT_STUDENT
							WHERE
								UserID = '".$ParUserID."'
							ORDER BY
								$orderBy
						";
		$TempDefaultSAArr = $this->returnArray($sql);

		for($i=0; $i<count($TempDefaultSAArr); $i++)
		{
			$ReturnArr[$TempDefaultSAArr[$i]['RecordID']] = explode(",", $TempDefaultSAArr[$i]['DefaultSA']);
		}

		return $ReturnArr;
	}

	# Import Self Account Temp Table   
	function insertSelfAccountImportTempData($ValueArr){
		
		global $eclass_db;
		
		### clear old data
		$this->deleteSelfAccountImportTempData();
		
		### insert new temp data
		$table = "{$eclass_db}.TEMP_IMPORT_SELF_ACCOUNT_STUDENT";
		$sql = "INSERT INTO 
					$table
				(UserID,RowNumber,StudentID,Title,Details,DateInput) 
				Values 
				".implode(',',(array)$ValueArr)."";
		$successAry[] = $this->db_db_query($sql);
		
		return !in_array(false, $successAry);
	}
	function getSelfAccountImportTempData() {
		
		global $eclass_db;
		
		$table = "{$eclass_db}.TEMP_IMPORT_SELF_ACCOUNT_STUDENT";
		$sql = "Select * from $table Where UserID = '".$_SESSION["UserID"]."' ";
		return $this->returnResultSet($sql);
	}
	function deleteSelfAccountImportTempData() {
		
		global $eclass_db;
		
		$successAry = array();
		$table = "{$eclass_db}.TEMP_IMPORT_SELF_ACCOUNT_STUDENT";
		
		$sql = "Delete from $table Where UserID = '".$_SESSION["UserID"]."' ";
		$successAry['DeleteTempImportCsvData'] = $this->db_db_query($sql);
		
		return !in_array(false, $successAry);
	}
	
	function getImportSelfAccountHeader(){
		global $Lang;
		
		$exportHeaderAry['En'] = $Lang['iPortfolio']['SelfAccount']['Import']['Header']['En'];
		$exportHeaderAry['Ch'] = $Lang['iPortfolio']['SelfAccount']['Import']['Header']['Ch'];
		return $exportHeaderAry;
	}
	function getImportSelfAccountColumnProperty(){
		return array(1,1,1,1,1);
	}
	
	# Generate self account table for display
	function GEN_SELF_ACCOUNT_LIST_TABLE($ParUserID, $ParValidPeriod=true){
		global $image_path, $i_LastModified, $button_edit, $button_remove, $file_path, $intranet_httppath, $LAYOUT_SKIN, $no_record_msg, $ck_memberType,$ec_iPortfolio;
		
		# Get self account list
		$self_acc_list = $this->GET_SELF_ACCOUNT_LIST($ParUserID,'',$orderByDefaultSA=true);
		
		if(count($self_acc_list) > 0)
		{
			for($i=0; $i<count($self_acc_list); $i++)
			{
				$attachment_str = "";
			
				$thisSelectedStr = ($self_acc_list[$i]['DefaultSA']=='SLP')? '('.$ec_iPortfolio['Selected'].')':'';
			
				# Build a string to display attachment
				if($self_acc_list[$i]['AttachmentPath'] != "")
				{
					$lf = new libfilesystem();
				
					$dir_path = $file_path."/file/portfolio/self_account/".$self_acc_list[$i]['AttachmentPath']."/";
					$attachment_list = $lf->return_folderlist($dir_path);

					if(is_array($attachment_list))
					{
						for($j=0; $j<count($attachment_list); $j++)
						{
							$attachment_str .= "<a href=\"/file/portfolio/self_account/".$self_acc_list[$i]['AttachmentPath']."/".basename($attachment_list[$j])."\" target=\"blank\" class=\"tablelink\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_attachment.gif\" width=\"12\" height=\"18\" border=\"0\" align=\"absmiddle\">".basename($attachment_list[$j])."</a><br />\n";
						}
					}
					
					unset($lf);
				}
				$thisDetails = $self_acc_list[$i]['Details'];
				$thisDetails = str_replace('</p>','<br>',$self_acc_list[$i]['Details']);
				$thisDetails = strip_tags($thisDetails,'<br>');
		//		$thisDetails = html_entity_decode($thisDetails);
				$lpfObj = new libportfolio();
				$thisDetails = $lpfObj->displaySelfAccountForHTML($thisDetails);

				$ReturnStr[] =	"
													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
														<tr>
															<td>
																<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																	<tr>
																		<td  class=\"sub_page_title\">".$self_acc_list[$i]['Title']."</td>
																		<td align=\"right\" valign=\"bottom\" class=\"tabletext\"><font color='red'>".$thisSelectedStr.'</font> '.$i_LastModified." ".substr($self_acc_list[$i]['ModifiedDate'],0,10)."</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td align=\"right\" class=\"stu_info_log\">
													".(($ck_memberType == "P" || !$ParValidPeriod) ? "" : "
																<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
																	<tr>
																		<td><a href=\"javascript:jEDIT_SA(".$self_acc_list[$i]['RecordID'].")\" class=\"contenttool\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> ".$button_edit."</a></td>
																		<td><a href=\"javascript:jDELETE_SA(".$self_acc_list[$i]['RecordID'].")\" class=\"contenttool\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_delete_b.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> ".$button_remove."</a></td>
																		<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\"></td>
																	</tr>
																</table>
													")."
																<table width=\"100%\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">
																	<tr>
																		<td class=\"form_field_content\">
																			".$thisDetails."
																			<br />
																			".$attachment_str."
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												";
			}
			
			$ReturnStr = implode("<br />", $ReturnStr);
		}
		else
		{
			$ReturnStr .=	"
											<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
												<tr>
													<td align=\"center\" class=\"\">
													".$no_record_msg."
													</td>
												</tr>
												<tr><td class=\"dotline\"><img src=\"/images/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>
											</table>
										";
		}
		
		return $ReturnStr;
	}
	
	# Generate default self account table for display
	# Commented out lines: for setting default self account in full report
	function GEN_DEFAULT_SELF_ACCOUNT_TABLE($ParUserID,$showLastModifiedDate = false)
	{
		global $ec_iPortfolio;
	
//		$YearArr = $this->getYearSemesterForReportPrinting();
		$DeafultArr = $this->GET_DEFAULT_SELF_ACCOUNT($ParUserID,$orderByDefaultSA=true);
	
		$cell_percent = floor(100 / (count($YearArr) + 2)); 
		
		if($showLastModifiedDate)
		{
			$cell_percent = floor(100 / (count($YearArr) + 3)); 
		}

		$ReturnStr =	"
										<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor='#CCCCCC'>
											<tr class='tabletop'>
												<td width='".$cell_percent."%' align='center' class=\"tabletopnolink\">&nbsp;</td>
												<td width='".$cell_percent."%' align='center' class=\"tabletopnolink\">".$ec_iPortfolio['student_learning_profile']."</td>
									";
		if($showLastModifiedDate)
		{
			$ReturnStr .="<td width='".$cell_percent."%' align='center' class=\"tabletopnolink\">".$ec_iPortfolio['lastModifiedDate']."</td>";
		}
//		for($i=0; $i<count($YearArr); $i++)
//			$ReturnStr .= "<td width='".$cell_percent."%' align='center' class=\"tabletext\">".$YearArr[$i]."</td>";
		$ReturnStr .=	"</tr>";
		
		if(count($DeafultArr)==0)
		{
			$ReturnStr .=	"<tr><td colspan='3' style='text-align:center;' bgcolor='#FFFFFF'>".$ec_iPortfolio['no_record_found']."</td></tr>";
		}
								
		if(is_array($DeafultArr))
		{
			$j = 0;
			foreach($DeafultArr as $RecordID => $DefaultSAArr)
			{
				$RowStyle = ($j%2 == 0) ? "tablerow1" : "tablerow2";
				
	//		list($SA_title) = $this->GET_SELF_ACCOUNT($RecordID);
	
				$thisRecordArr = $this->GET_SELF_ACCOUNT_ALL_DATA($RecordID);

				if(is_array($thisRecordArr))
				{
					$thisRecordArr = current($thisRecordArr);
				}
				$SA_title = $thisRecordArr["Title"];
				$SA_ModifiedDate = $thisRecordArr["ModifiedDate"];

				$ReturnStr .=	"
												<tr>
													<td width='".$cell_percent."%' class='".$RowStyle."'>".$SA_title."</td>
											";
				if(in_array("SLP", $DefaultSAArr))
					$ReturnStr .=	"<td width='".$cell_percent."%' align='center' class='".$RowStyle."'><input type='radio' name='SLP_ph' value='".$RecordID."' onClick='jUNCHECK_RECHECK(\"SLP_ph\", \"".$RecordID."\")' checked /></td>";
				else
					$ReturnStr .=	"<td width='".$cell_percent."%' align='center' class='".$RowStyle."'><input type='radio' name='SLP_ph' value='".$RecordID."' onClick='jUNCHECK_RECHECK(\"SLP_ph\", \"".$RecordID."\")' /></td>";


				if($showLastModifiedDate)
				{
					$ReturnStr .="<td width='".$cell_percent."%' class='".$RowStyle."'>".$SA_ModifiedDate."</td>";
				}
		
//				for($i=0; $i<count($YearArr); $i++)
//				{
//					if(in_array($YearArr[$i], $DefaultSAArr))
//						$ReturnStr .=	"<td width='".$cell_percent."%' align='center' class='".$RowStyle."'><input type='checkbox' name='Year_".$YearArr[$i]."' value='".$RecordID."' onClick='jUNCHECK_RECHECK(\"Year_".$YearArr[$i]."\", \"".$RecordID."\")' checked /></td>";
//					else
//						$ReturnStr .=	"<td width='".$cell_percent."%' align='center' class='".$RowStyle."'><input type='checkbox' name='Year_".$YearArr[$i]."' value='".$RecordID."' onClick='jUNCHECK_RECHECK(\"Year_".$YearArr[$i]."\", \"".$RecordID."\")' /></td>";
//				}

				$ReturnStr .=	"</tr>";
				
				$j++;
			}
		
		}
		
		
		$ReturnStr .=	"</table>";

		return $ReturnStr;
	}
	
	# Generate parent information table for display
	function GEN_SELF_ACCOUNT_TABLE($ParUserID)
	{
		global $ec_student_word, $ec_iPortfolio, $no_record_msg;
		global $image_path, $LAYOUT_SKIN, $button_edit, $ClassName,$file_path;
		global $slp_record_id;

		# Get self account of a student
		$self_account_arr = $this->GET_DEFAULT_SELF_ACCOUNT($ParUserID);
		if(is_array($self_account_arr))
		{
			foreach($self_account_arr AS $RecordID => $DefaultSA)
			{
				if(in_array("SLP", $DefaultSA))
				{
					$self_account_default_arr = $this->GET_SELF_ACCOUNT($RecordID);
					$slp_record_id = $RecordID; /* 20090923 */
				}
			}
		}

		$ReturnStr = "";
		$ReturnStr .= "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";
		if(isset($self_account_default_arr))
		{
			/* Edit: 20091218 */
			$attachment_str = "";
			
			# Build a string to display attachment
			if($self_account_default_arr['AttachmentPath'] != "")
			{
				$lf = new libfilesystem();				
				$dir_path = $file_path."/file/portfolio/self_account/".$self_account_default_arr['AttachmentPath']."/";					
				$attachment_list = $lf->return_folderlist($dir_path);

				if(is_array($attachment_list))
				{
					for($j=0; $j<count($attachment_list); $j++)
					{
						$attachment_str .= "<a href=\"/file/portfolio/self_account/".$self_account_default_arr['AttachmentPath']."/".basename($attachment_list[$j])."\" target=\"blank\" class=\"tablelink\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_attachment.gif\" width=\"12\" height=\"18\" border=\"0\" align=\"absmiddle\">".basename($attachment_list[$j])."</a><br />\n";
					}
				}					
				unset($lf);
			}		
			
			# Edit student info button
			$ReturnStr .= "
							<tr>
								<td align=\"right\" style=\"padding:0 10px 0 0;\">
									<a href=\"student_account_teacher_edit.php?StudentID=".$ParUserID."&RecordID=".$slp_record_id."&ClassName=".$ClassName."\"> <img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> ".$button_edit."</a>
								</td>
							</tr>
						";
			/* ----  20091218 -----*/
			$ReturnStr .=	"
											<tr>
												<td>
													<table width='100%' border='0' cellspacing='6' cellpadding='0'>
														<tr>
															<td><span class=\"sub_page_title\">(".$self_account_default_arr['Title'].")</span></td>
														</tr>
														<tr>
															<td class=\"form_field_content\">".$self_account_default_arr["Details"]."</td>
														</tr>
													</table>
												</td>
											</tr>
										";
		}
		else
		{
			$ReturnStr .=	"
											<tr>
												<td>
													<table width='100%' border='0' cellspacing='0' cellpadding='0' height='100'>
														<tr height=60>
															<td colspan=3 align=center class=\"tabletext\">&nbsp;$no_record_msg</td>
														</tr>
													</table>
												</td>
											</tr>
										";
		}
		$ReturnStr .= "</table>";

		return $ReturnStr;
	}
	
	
	function getSubCategory($ParCategoryID) {
		global $PATH_WRT_ROOT, $eclass_db, $intranet_db, $intranet_session_language;
		include_once($PATH_WRT_ROOT."includes/libdb.php");
		
		# getting subcategory by category id
		// RECORDSTATUS
		$PUBLIC = 1;
		$TITLE = ( $intranet_session_language == "en" ? "ENGTITLE":"CHITITLE" );
		$sql = "
    	SELECT SUBCATID, " . $TITLE . " AS TITLE
    	FROM " . $eclass_db . ".OLE_SUBCATEGORY
    	WHERE CATID = '" . $ParCategoryID . "'
    	AND RECORDSTATUS = '" . $PUBLIC . "'
			";
		$returnArr = $this->returnArray($sql);
		return $returnArr;
  }

  /**
  * Update AcademicYearID and YearTermID in modules
  *
  * @param String $moduleName Name of the module
  * @param String $YearName Name of the academic year
  * @param String $SemName Name of the semester
  * @return   
  *
  */
  function updateModuleYearTerm($moduleName, $YearName, $SemName="")
  {
    global $eclass_db;
  
    $ayID = $this->getModuleYearID($YearName);
    if($ayID == '') return;
    
    switch($moduleName)
    {
      case "merit":
        $moduleTable = $eclass_db.".MERIT_STUDENT";
        break;
      case "activity":
        $moduleTable = $eclass_db.".ACTIVITY_STUDENT";
        break;
      case "award":
        $moduleTable = $eclass_db.".AWARD_STUDENT";
        break;
      case "attendance":
        $moduleTable = $eclass_db.".ATTENDANCE_STUDENT";
        break;
      case "service":
        $moduleTable = $eclass_db.".SERVICE_STUDENT";
        break;
    }
    
    $sql = "UPDATE $moduleTable SET AcademicYearID = '$ayID' WHERE Year = '$YearName'";
    $this->db_db_query($sql);
  
    $lay = new academic_year($ayID);
    $ytArr = $lay->Get_Term_List(1, 'en');
    $ytChArr = $lay->Get_Term_List(1, 'ch');
  
    if(count($ytArr) > 0) {
	    foreach($ytArr as $ytID => $ytName)
	    {
	    	$ytNameCh = $ytChArr[$ytID];
	    	if ($SemName != "" && $SemName != $ytName && $SemName != $ytNameCh) continue;
	    
	    	$sql = "UPDATE $moduleTable SET YearTermID = '$ytID' WHERE AcademicYearID = '$ayID' and Semester = '$ytName'";
	    	$this->db_db_query($sql);
	    }
    }
  }
  
  function getModuleYearID($YearName)
  {
    global $intranet_db;
  
    $sql =  "SELECT AcademicYearID FROM {$intranet_db}.ACADEMIC_YEAR WHERE YearNameEN = '$YearName'";
  
    return current($this->returnVector($sql));
  }
  
  function getGraduationYearList() {
    global $intranet_db;
  
    $sql =  "SELECT DISTINCT YEAROFLEFT FROM $intranet_db.INTRANET_ARCHIVE_USER";
    $resultArray = $this->returnArray($sql);
    
    $returnArray = array();
    foreach($resultArray as $key => $element) {
    	$returnArray["{$element["YEAROFLEFT"]}"] = $element["YEAROFLEFT"];
    }

    return $returnArray;
  }
  
  	# count number of OLE data
	function countOLEDataNumberAlumni($ParYearOfLeft, $intext="")
	{
		global $eclass_db, $intranet_db;
		if (!empty($ParYearOfLeft)) {
			$cond = " AND iau.YearOfLeft = '{$ParYearOfLeft}'";
		}
		# filtering int/ext records
		# Note: not like other OLE functions - 1: Int, 2: Ext		
		if($intext == 1)
			$cond .= " AND op.IntExt = 'INT'";
		else if($intext == 2)
			$cond .= " AND op.IntExt = 'EXT'";
			
		$sql = "SELECT
					COUNT(DISTINCT a.RecordID)
				FROM
					$eclass_db.OLE_STUDENT as a
					LEFT JOIN {$eclass_db}.OLE_PROGRAM as op ON a.ProgramID = op.ProgramID
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay ON op.AcademicYearID = ay.AcademicYearID
					LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER as iau ON a.UserID = iau.UserID
				WHERE
					(iau.WebSAMSRegNo IS NOT NULL or trim(iau.WebSAMSRegNo IS NOT NULL) != '')
					$cond
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}
	

	function returnStudentOLRExportDataAlumni($Year, $IntExt="", $startIndex, $recordsPerLoop)
	{
		global $eclass_db, $intranet_db, $ec_iPortfolio, $profiles_to, $ipf_cfg, $iPort;

		$namefield2 = getNameFieldByLang2("iau.");
		$namefield_approveby = getNameFieldByLang2("c.");
		$namefield_preferredApprover = getNameFieldByLang2("iu_preferredApprover.");
		
		$CategoryField = $this->get_OLR_Category_Field_For_Record("op.");
		$SubCategoryField = $this->get_OLR_SubCategory_Field_For_Record("op.");
		$AcademicYearNameField = Get_Lang_Selection('ay.YearNameB5', 'ay.YearNameEN');
		
		if($Year!="") {
			$cond = " AND iau.YearOfLeft = '".$Year."'";
		}
		# Note: not like other OLE functions - 1: Int, 2: Ext
		if($IntExt == 1)
			$cond .= " AND op.IntExt = 'INT'";
		else if($IntExt == 2)
			$cond .= " AND op.IntExt = 'EXT'";

		$sql = "SELECT
					iau.WebSAMSRegNo as WebSAMSRegNo,
					$namefield2 as UserName,
					IF ((op.StartDate IS NULL OR op.StartDate='0000-00-00'),'--',IF(op.EndDate IS NULL OR op.EndDate='0000-00-00',op.StartDate, CAST(CONCAT(op.StartDate,' $profiles_to ',op.EndDate) AS CHAR) )) as OLEDate,
					op.Title,
					$CategoryField as Category,
					$SubCategoryField as SubCategory,
					a.Role,
					a.Hours,
					op.Details,
					$namefield_approveby as ApprovedBy,
					CASE a.RecordStatus
						WHEN 1 THEN '".$ec_iPortfolio['pending']."'
						WHEN 2 THEN '".$ec_iPortfolio['approved']."'
						WHEN 3 THEN '".$ec_iPortfolio['rejected']."'
						WHEN 4 THEN '".$ec_iPortfolio['teacher_submit_record']."'
					ELSE '--' END as RecordStatus,
					IF(a.ProcessDate IS NOT NULL, DATE_FORMAT(a.ProcessDate,'%Y-%m-%d'),'--') as ProcessDate,
					op.SchoolRemarks AS Remark,
					a.ELE,
					a.Achievement,
					op.Organization,
					/* op.StartDate, */
					/* op.EndDate, */
					$AcademicYearNameField as StartDate,
					$AcademicYearNameField as EndDate,
					iau.ClassName,
					iau.ClassNumber,a.TeacherComment,
		          	a.TeacherComment,
					if(op.intext = '".$ipf_cfg["OLE_TYPE_STR"]["INT"]."','".$iPort["internal_record"]."','".$iPort["external_record"]."') as 'intExtType',
					a.SLPOrder,
					$namefield_preferredApprover as PreferredApprover,
					IF (op.IsSAS = 1, 'Y', 'N') as IsSAS
				FROM
					{$eclass_db}.OLE_STUDENT as a
					LEFT JOIN {$eclass_db}.OLE_PROGRAM AS op ON a.ProgramID = op.ProgramID 
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay ON op.AcademicYearID = ay.AcademicYearID
					LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER AS iau ON a.UserID = iau.UserID
					LEFT JOIN {$intranet_db}.INTRANET_USER AS c ON a.ApprovedBy = c.UserID
					LEFT JOIN {$intranet_db}.INTRANET_USER AS iu_preferredApprover ON a.RequestApprovedBy = iu_preferredApprover.UserID
				WHERE
					(iau.WebSAMSRegNo IS NOT NULL)
					$cond
				ORDER BY
					$namefield2,
					a.StartDate
				LIMIT $startIndex, $recordsPerLoop	
			";

		$row = $this->returnArray($sql);

		return $row;
	}
	
	function getUserGroupsByUserIdAndFunctionRight($ParUserId=null, $ParFunctionRightArray) {
		global $eclass_db, $intranet_db;
		if (!empty($ParUserId)) {
			$cond_specifiedUserId = " AND IU.USERID = '$ParUserId'";
		}
		
		$functionRightsStringArray = array();
		$sizeOfFRArray = count($ParFunctionRightArray);
		for($i=0;$i<$sizeOfFRArray;$i++) {
			$right = $ParFunctionRightArray[$i];
			$functionRightsStringArray[] = " INSTR(mgf.function_right, '$right') > 0 ";
		}
		if (is_array($functionRightsStringArray)) {
			$functionRightsString = implode("OR",$functionRightsStringArray);
		}
		
		$sql = "SELECT DISTINCT ug.group_id 
		FROM {$this->course_db}.user_group ug 
		INNER JOIN {$this->course_db}.mgt_grouping_function mgf ON ug.group_id = mgf.group_id
			AND (
					$functionRightsString
				)
		INNER JOIN {$eclass_db}.user_course uc ON uc.user_id = ug.user_id
		INNER JOIN {$intranet_db}.INTRANET_USER IU ON uc.user_email = IU.USEREMAIL
		WHERE uc.course_id='".$this->course_id."' $cond_specifiedUserId";
//		debug_r($sql);
		$returnArray = $this->returnVector($sql);
		return $returnArray;
	}
	
	function getUserGroupsInTeacherComment($ParUserId=null) {
		$returnArray = $this->getUserGroupsByUserIdAndFunctionRight($ParUserId,array("Profile:TeacherComment"));
		return $returnArray;
	}
	
	function getUserGroupsInSelfAccount($ParUserId=null) {
		$returnArray = $this->getUserGroupsByUserIdAndFunctionRight($ParUserId,array("Profile:SelfAccount"));
		return $returnArray;
	}
	
	function getClassInfoByGroupIds($ParGroupIds) {
		global $intranet_db, $eclass_db,$intranet_session_language;
		$returnArray = array();
		if (empty($ParGroupIds)) {	// in case this is admin, no groupids
			$groupIdCond = "";
		} else {
			$groupIdCond = "AND g.group_id IN (".implode(",",$ParGroupIds).")";
			$groupsJoinCond = " 
			INNER JOIN {$this->course_db}.user_group ug ON ug.user_id = um.user_id 
			INNER JOIN {$this->course_db}.grouping g ON ug.group_id = g.group_id 
			INNER JOIN {$this->course_db}.mgt_grouping_function mgf ON g.group_id = mgf.group_id 
			";
		}
		
		## Class Name Field START ##
		$prefix = 'YC.';
		if($intranet_session_language == 'b5'){
             $firstChoice = "CLASSTITLEB5";
             $altChoice = "CLASSTITLEEN";
		}else{
             $firstChoice = "CLASSTITLEEN";
             $altChoice = "CLASSTITLEB5";
		}
        $classNameField = "IF($prefix$firstChoice IS NULL OR TRIM($prefix$firstChoice) = '',$prefix$altChoice,$prefix$firstChoice) AS $firstChoice";
		## Class Name Field END ##
		
//		if (!empty($ParGroupIds) && is_array($ParGroupIds)) {
			//$sql = "SELECT DISTINCT Y.YEARNAME, YC.CLASSTITLE".strtoupper($intranet_session_language).", YC.YEARCLASSID
			$sql = "SELECT DISTINCT Y.YEARNAME, $classNameField, YC.YEARCLASSID
			FROM {$intranet_db}.INTRANET_USER IU 
			INNER JOIN {$intranet_db}.YEAR_CLASS_USER YCU ON IU.USERID = YCU.USERID
			INNER JOIN {$intranet_db}.YEAR_CLASS YC ON YCU.YEARCLASSID = YC.YEARCLASSID
			INNER JOIN {$intranet_db}.YEAR Y ON YC.YEARID = Y.YEARID
			INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT PS ON IU.USERID = PS.USERID 
			INNER JOIN {$this->course_db}.usermaster um ON PS.COURSEUSERID = um.user_id
			$groupsJoinCond 
			WHERE
			YC.ACADEMICYEARID = '".Get_Current_Academic_Year_ID()."'
			$groupIdCond
			ORDER BY Y.SEQUENCE,YC.SEQUENCE";
			$returnArray = $this->returnArray($sql);
//			debug_r($sql);
//		}
		return $returnArray;
	}
	
	function refineUserClassInfo($ParUserClassInfo) {
		global $intranet_session_language;
		
		$classNameField = Get_Lang_Selection('CLASSTITLEB5', 'CLASSTITLEEN');
		
		$returnArray = array();
		if (!empty($ParUserClassInfo) && is_array($ParUserClassInfo)) {
			foreach($ParUserClassInfo as $key=>$element) {
				//$formClass[$element["YEARNAME"]][] = array($element["YEARCLASSID"],$element["CLASSTITLE".strtoupper($intranet_session_language)]);
				$formClass[$element["YEARNAME"]][] = array($element["YEARCLASSID"],$element[$classNameField]);
			}
		}
		$returnArray = $formClass;
		return $returnArray; 
	}
	
	function getSelfAccountsByYearClassUserIds($ParYearClassUserIds,$showAll=false) {
		global $intranet_db,$eclass_db,$ipf_cfg,$intranet_session_language;
		if (isset($ParYearClassUserIds) && !is_array($ParYearClassUserIds)) {
			$ParYearClassUserIds = array($ParYearClassUserIds);
		}
		
		//2012-0221-1359-42067
//		if (strtoupper($intranet_session_language)=="EN") {
//			$userNameField = "IU.ENGLISHNAME";
//		} else {
//			$userNameField = "IU.CHINESENAME";
//		}

		$userNameField = Get_Lang_Selection('IU.CHINESENAME', 'IU.ENGLISHNAME');
		$classNameField = Get_Lang_Selection('YC.CLASSTITLEB5', 'YC.CLASSTITLEEN');
		
		$sql = "SELECT
		SAS.RECORDID,
		SAS.DEFAULTSA,
		/* YC.CLASSTITLE".strtoupper($intranet_session_language)." AS CLASSTITLE, */
		$classNameField AS CLASSTITLE,
		IU.CLASSNUMBER,
		$userNameField AS USERNAME,
		SAS.TITLE,
		SAS.DETAILS,
		SAS.RECORDSTATUS,
		SAS.APPROVEDBY,
		SAS.APPROVEDDATE,
		IU.USERID,
		YCU.YEARCLASSUSERID,
		SAS.DEFAULTSA
		FROM {$intranet_db}.INTRANET_USER IU
		LEFT JOIN {$eclass_db}.SELF_ACCOUNT_STUDENT SAS
		ON SAS.USERID = IU.USERID
		INNER JOIN {$intranet_db}.YEAR_CLASS_USER YCU
		ON IU.USERID = YCU.USERID
		INNER JOIN {$intranet_db}.YEAR_CLASS YC
		ON YCU.YEARCLASSID = YC.YEARCLASSID
		INNER JOIN {$intranet_db}.YEAR Y
		ON YC.YEARID = Y.YEARID
		WHERE YCU.YEARCLASSUSERID IN (".implode(",",$ParYearClassUserIds).")
		ORDER BY Y.SEQUENCE, YC.SEQUENCE, IU.CLASSNUMBER";
		$resultArray = $this->returnArray($sql);

		$count = 0;
		$returnArray = array();
		if (is_array($resultArray)) {
			foreach($resultArray as $key=>$element)
			{	
				if($showAll==true)
				{
					if ($element["DEFAULTSA"] == $ipf_cfg["DB_SELF_ACCOUNT_STUDENT_DefaultSA"]["SLP"]) {
						$returnArray[$element["USERID"]][$count]["HAS_DEFAULT"] = true;
					}
					else
					{
						$returnArray[$element["USERID"]][$count]["HAS_DEFAULT"] = false;
					}
					$returnArray[$element["USERID"]][$count]["ELEMENT"] = $element;
				}
				else
				{
					if ($element["DEFAULTSA"] == $ipf_cfg["DB_SELF_ACCOUNT_STUDENT_DefaultSA"]["SLP"]) {
						$returnArray[$element["USERID"]]["HAS_DEFAULT"] = true;
						$returnArray[$element["USERID"]]["ELEMENT"] = $element;
					} else if ($returnArray[$element["USERID"]]["HAS_DEFAULT"] == true) {
						// do nothing
					} else {
						$returnArray[$element["USERID"]]["HAS_DEFAULT"] = false;
						$returnArray[$element["USERID"]]["ELEMENT"] = $element;
					}
				}
				
				$count++;			
			}
		} 
		return $returnArray;
	}
	
	function getSelfAccounts_noDefault($ParYearClassUserIds) {
		global $intranet_db,$eclass_db,$ipf_cfg,$intranet_session_language;
		if (isset($ParYearClassUserIds) && !is_array($ParYearClassUserIds)) {
			$ParYearClassUserIds = array($ParYearClassUserIds);
		}
		

		$userNameField = Get_Lang_Selection('IU.CHINESENAME', 'IU.ENGLISHNAME');
		$classNameField = Get_Lang_Selection('YC.CLASSTITLEB5', 'YC.CLASSTITLEEN');
		
		$sql = "SELECT
		SAS.RECORDID,
		SAS.DEFAULTSA,
		SAS.TITLE,
		SAS.DETAILS,
		SAS.RECORDSTATUS,
		SAS.APPROVEDBY,
		SAS.APPROVEDDATE,
		IU.USERID,
		SAS.DEFAULTSA
		FROM {$intranet_db}.INTRANET_USER IU
		LEFT JOIN {$eclass_db}.SELF_ACCOUNT_STUDENT SAS
		ON SAS.USERID = IU.USERID
		WHERE IU.USERID IN (".implode(",",$ParYearClassUserIds).")
		AND ((SAS.DEFAULTSA <> 'SLP') OR (SAS.DEFAULTSA IS Null))
		";
		
		$resultArray = $this->returnResultSet($sql);

		return $resultArray;
	}
	
	
	function saveSelfAccount($ParRecordId,$ParStudentId,$ParRecordStatus, $ParTitle, $ParDetails, $ParApprovedBy,$setDefault=1) {
		global $eclass_db,$ipf_cfg,$intranet_root;
		
		if ($ParRecordStatus) {
			$cond_approveInfo = " APPROVEDBY = $ParApprovedBy, APPROVEDDATE = NOW() ";
		} else {
			$cond_approveInfo = " APPROVEDBY = NULL, APPROVEDDATE = NULL ";
		}
		if (empty($ParRecordId)) {		
		
			$sql = "INSERT INTO {$eclass_db}.SELF_ACCOUNT_STUDENT 
					SET DEFAULTSA = '{$ipf_cfg["DB_SELF_ACCOUNT_STUDENT_DefaultSA"]["SLP"]}',
					USERID = '$ParStudentId',
					RECORDSTATUS = '$ParRecordStatus',
					TITLE='$ParTitle',
					DETAILS = '$ParDetails',
					MODIFIEDDATE = NOW(),
					$cond_approveInfo";

		} else {
			
			
			$sql = "UPDATE {$eclass_db}.SELF_ACCOUNT_STUDENT SET 
								RECORDSTATUS = '$ParRecordStatus', 
								TITLE='$ParTitle', 
								DETAILS = '$ParDetails', 
								MODIFIEDDATE = NOW(),
								$cond_approveInfo 
				WHERE USERID = '$ParStudentId' AND recordid = '$ParRecordId'";


		}

		$result = $this->db_db_query($sql);

		if($setDefault == 1){
			include_once($intranet_root."/includes/portfolio25/libpf-slp.php");
			$objSelfAccount = new libpf_self_account();
			$objSelfAccount->Set_Default_Self_Account($ParRecordId, $ParStudentId);				
		}

		return $result;
	}
	
	function getTeacherCommentsByYearClassUserIds($ParYearClassUserIds) {
		global $intranet_db,$eclass_db,$ipf_cfg,$intranet_session_language;
		if (isset($ParYearClassUserIds) && !is_array($ParYearClassUserIds)) {
			$ParYearClassUserIds = array($ParYearClassUserIds);
		}
		$userNameField = getNameFieldByLang("IU.");
		$lastUpdateByField = getNameFieldByLang("IU2.");
		$classNameField = Get_Lang_Selection('YC.CLASSTITLEB5', 'YC.CLASSTITLEEN');
		
		$sql = "SELECT
		SC.RECORDID,
		/* YC.CLASSTITLE".strtoupper($intranet_session_language)." AS CLASSTITLE, */
		$classNameField AS CLASSTITLE,
		IU.CLASSNUMBER,
		$userNameField AS USERNAME,
		SC.COMMENT,
		SC.MODIFIEDDATE,
		IF (SC.LASTUPDATEBY IS NULL || SC.LASTUPDATEBY = '','--',$lastUpdateByField) AS LASTUPDATEBY,
		IU.USERID AS STUDENTID,
		YCU.YEARCLASSUSERID
		FROM {$intranet_db}.INTRANET_USER IU
		LEFT JOIN {$eclass_db}.STUDENT_COMMENT SC
		ON SC.STUDENTID = IU.USERID
		LEFT JOIN {$intranet_db}.INTRANET_USER IU2
		ON SC.LASTUPDATEBY = IU2.USERID
		INNER JOIN {$intranet_db}.YEAR_CLASS_USER YCU
		ON IU.USERID = YCU.USERID
		INNER JOIN {$intranet_db}.YEAR_CLASS YC
		ON YCU.YEARCLASSID = YC.YEARCLASSID
		INNER JOIN {$intranet_db}.YEAR Y
		ON YC.YEARID = Y.YEARID
		WHERE YCU.YEARCLASSUSERID IN (".implode(",",$ParYearClassUserIds).")
		ORDER BY Y.SEQUENCE, YC.SEQUENCE, IU.CLASSNUMBER";
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	
	}
	
	function saveTeacherComment($ParStudentId,$ParComment,$ParUpdatedBy) {
		global $eclass_db;
		$ParComment = htmlspecialchars($ParComment);
//		debug_r($ParComment);
//		debug_r(mysql_real_escape_string(stripslashes(htmlspecialchars($ParComment))));
		$sql = "INSERT INTO {$eclass_db}.STUDENT_COMMENT SET STUDENTID = '$ParStudentId', COMMENT = '".$ParComment."', LASTUPDATEBY = '$ParUpdatedBy'
				ON DUPLICATE KEY UPDATE COMMENT = '$ParComment', LASTUPDATEBY = '$ParUpdatedBy'";
//				DEBUG_R($sql);
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	/**
	 * get the statistic of comefrom field in db in a bundle of programids
	 * @param Array(INT) $ParProgramIds - a bundle of program ids
	 */
	function getOLEProgramComeFromCount($ParProgramIds) {
		global $eclass_db;
		$returnArray = array();
		if (is_array($ParProgramIds)) {
			$sql = "SELECT COMEFROM,COUNT(*) AS COUNT FROM $eclass_db.OLE_PROGRAM WHERE PROGRAMID IN (".implode(",",$ParProgramIds).") GROUP BY COMEFROM";
			$returnArray = $this->returnArray($sql);
		}
		return $returnArray;
	}
	
	function isOLEMasterAdmin($mgmt_ele_arr){
		global $ck_function_rights, $ck_user_rights;
	
		$isAdmin = false;
		//1) the teacher is a OLE ADMIN and
		if(strstr($ck_function_rights, "Profile:OLR"))
		{
			//2a) the teacher is big admin "strstr($ck_user_rights, ":manage:")" OR
			if(strstr($ck_user_rights, ":manage:"))
			{
				$isAdmin = true;
			}
			//2b) the teacher in a group without ELE component restriction "sizeof($mgmt_ele_arr) == 0"
			else if(sizeof($mgmt_ele_arr) == 0)
			{
				$isAdmin = true;
			}
		}
		
		return $isAdmin;
	}

   /**
	* Approve OLE_STUDENT Record
	* @owner : Fai (20110426)
	* @param : INT $status , Status Value that updated to
	* @param : INT $changedBy , Record change status by whom
	* @param : Array  or a INT $recordID, a array or a Value of OLE_STUDENT.recordid that need to change the status
	* @return : boolean , if all of the record can be updated return ture, else return false;
	* 
	*/
	function updateStudentOLEStatus($status, $changedBy,$recordID){
		global $eclass_db, $UserID;

		if($changedBy < 0 || 
				(trim($changedBy) == "")){
			$changedBy = $UserID;
		}
		$strRecordID = is_array($recordID)? implode(",",$recordID): $recordID;
		$sql = "update {$eclass_db}.OLE_STUDENT set RecordStatus = '".$status."', ApprovedBy = '".$changedBy."' ,ModifiedDate = now(), ProcessDate = now() where RecordID in ({$strRecordID})";

		$objDB = new libdb();
		$ReturnVal = $objDB->db_db_query($sql);
		return $ReturnVal;
	}

   /**
	* Patch OLE_PROGRAM.academicYearID according to the academic year setting , this function is in /home/web/eclass40/intranetIP25/addon/ip25/index.php orginally
	* @owner : Fai (20100210)
	* @return : str , log for the result
	* 
	*/

	  function PatchOLEAYearID(){
		  global $eclass_db,$intranet_db;
			$li = new libdb();
		  $sql = "SELECT count(*) FROM {$eclass_db}.OLE_PROGRAM WHERE AcademicYearID IS NOT NULL AND AcademicYearID <> '' AND AcademicYearID <> 0";
		  $ex_count = current($li->returnVector($sql));
		  
		  $sql = "UPDATE {$eclass_db}.OLE_PROGRAM AS op SET ";
		  $sql .= "AcademicYearID = ";
		  $sql .= "(SELECT ay.AcademicYearID FROM {$intranet_db}.ACADEMIC_YEAR AS ay ";
		  $sql .= "INNER JOIN {$intranet_db}.ACADEMIC_YEAR_TERM AS ayt ON ay.AcademicYearID = ayt.AcademicYearID ";
		  $sql .= "WHERE op.StartDate BETWEEN ayt.TermStart AND ayt.TermEnd), ";
		  $sql .= "ModifiedDate = NOW()";
		  $sql .= "WHERE AcademicYearID IS NULL OR AcademicYearID = '' OR AcademicYearID = 0";
		  $li->db_db_query($sql);
		  $success_count = mysql_affected_rows();
		  
		  $sql = "SELECT count(*) FROM {$eclass_db}.OLE_PROGRAM";
		  $total_count = current($li->returnVector($sql));
		  
		  $sql = "SELECT ProgramID, Title, StartDate, EndDate FROM {$eclass_db}.OLE_PROGRAM WHERE AcademicYearID IS NULL OR AcademicYearID = '' OR AcademicYearID = 0";
		  $nullAYIDarr = $li->returnArray($sql);

		  # logging
		  $outstr = "Total Programme Count: ".$total_count."\n";
		  $outstr .= "With Academic-Year-ID Programme Count: ".$ex_count."\n";
		  if($success_count){
			$outstr .= "Updated Count (Including success and failure): ".$success_count."\n";
		  }
		  if(is_array($nullAYIDarr)){
			$outstr .= "Failure Count: ".count($nullAYIDarr)."\n\n";
			$outstr .= "ProgramID\tTitle\tStartDate\tEndDate\n";
			for($i=0; $i<count($nullAYIDarr); $i++)
			{
			  $outstr .= $nullAYIDarr[$i]['ProgramID']."\t";
			  $outstr .= $nullAYIDarr[$i]['Title']."\t";
			  $outstr .= $nullAYIDarr[$i]['StartDate']."\t";
			  $outstr .= $nullAYIDarr[$i]['EndDate']."\n";
			}
		  }
		  //return str log
	  return $outstr;
	  }
	  
   /**
	* Get self account all data
	* @owner : Connie (20120316)
	* @return : Self Account Array
	* 
	*/
	 
	function GET_SELF_ACCOUNT_ALL_DATA($ParRecordID='',$ParUserID=''){
		global $eclass_db;

		if($ParRecordID!='')
		{
			$cond_RecordID = " And RecordID = '$ParRecordID'";
		}

		if($ParUserID!='')
		{
			$cond_UserID = " And UserID = '$ParUserID'";
		}

		$sql =	"
							SELECT
								*
							FROM
								{$eclass_db}.SELF_ACCOUNT_STUDENT
							WHERE
								1
								$cond_RecordID
								$cond_UserID
						";
		$ReturnArr = $this->returnResultSet($sql);

		return $ReturnArr;
	}
	
	/**
	* Get self account all data by latest Modified Date
	* @owner : Connie (20120316)
	* @Para : StudentID
	* 
	*/
	 
	function SET_SELF_ACCOUNT_DEFAULTSA($ParStudentID,$ParRecordID=''){
		global $eclass_db;

		if($ParRecordID=='')
		{
			$RecordID_sql = "Select RecordID from {$eclass_db}.SELF_ACCOUNT_STUDENT where UserID = $ParStudentID order by ModifiedDate desc";
			$RecordIDArray = $this->returnResultSet($RecordID_sql);
			$ParRecordID = $RecordIDArray[0]['RecordID'];
		}
		// Reset all self account not selected
		$sql =	"
							Update
								{$eclass_db}.SELF_ACCOUNT_STUDENT
							SET
								DefaultSA = NULL
							WHERE
								UserID = '$ParStudentID'
						";
		$this-> db_db_query($sql);
		
		// Set the required record as selected one
		$sql =	"
							Update
								{$eclass_db}.SELF_ACCOUNT_STUDENT
							SET
								DefaultSA = 'SLP'
							WHERE
								UserID = '$ParStudentID'
								AND RecordID = '$ParRecordID'
						";
		$this-> db_db_query($sql);

	}
	
	function getActivityTitle($ELE, $AcademicYearID, $UserID){
		global $eclass_db;
		$sql ="Select P.Title, S.Hours From {$eclass_db}.OLE_STUDENT S INNER JOIN {$eclass_db}.OLE_PROGRAM P ON S.ProgramID = P.ProgramID where UserID = '".$UserID."' and P.ELE like '%{$ELE}%' and P.AcademicYearID = '".$AcademicYearID."'";
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}
}

?>