<?
// modifiying : Bill
class SbaStage {
		private $eclass_db;
		private $intranet_db;
		private $objDB;

		private $StageID;
		private $SchemeID;
		private $Title;
		private $Description;
		private $Sequence;
		private $Deadline;
		private $MaxScore;
		private $Weight;
		private $RecordStatus;
		private $DateInput;
		private $InputBy;
		private $DateModified;
		private $ModifyBy;
		private $objTasksAry;

		public function SbaStage($parStageId = null){  

			global $eclass_db,$intranet_db;
			$this->objDB = new libdb();
			$this->eclass_db = $eclass_db;
			$this->intranet_db = $intranet_db;

			if($parStageId != null)
			{
				$this->setStageID(intval($parStageId));
				$this->loadDataFromStorage();
			}
		}

		public function setStageID($val) {
			$this->StageID = $val;
		}
		public function getStageID() {
			return $this->StageID;
		}

		public function setSchemeID($val) {
			$this->SchemeID = $val;
		}
		public function getSchemeID() {
			return $this->SchemeID;
		}

		public function setTitle($val) {
			$this->Title = $val;
		}
		public function getTitle() {
			return $this->Title;
		}

		public function setDescription($val) {
			$this->Description = $val;
		}
		public function getDescription() {
			return $this->Description;
		}

		public function setSequence($val) {
			$this->Sequence = $val;
		}
		public function getSequence() {
			return $this->Sequence;
		}

		public function setDeadline($val) {
			$this->Deadline = $val;
		}
		public function getDeadline() {
			return $this->Deadline;
		}

		public function setMaxScore($val) {
			$this->MaxScore = $val;
		}
		public function getMaxScore() {
			return $this->MaxScore;
		}

		public function setWeight($val) {
			$this->Weight = $val;
		}
		public function getWeight() {
			return $this->Weight;
		}

		public function setRecordStatus($val) {
			$this->RecordStatus = $val;
		}
		public function getRecordStatus() {
			return $this->RecordStatus;
		}

		public function setDateInput($val) {
			$this->DateInput = $val;
		}
		public function getDateInput() {
			return $this->DateInput;
		}

		public function setInputBy($val) {
			$this->InputBy = $val;
		}
		public function getInputBy() {
			return $this->InputBy;
		}

		public function setDateModified($val) {
			$this->DateModified = $val;
		}
		public function getDateModified() {
			return $this->DateModified;
		}

		public function setModifyBy($val) {
			$this->ModifyBy = $val;
		}
		public function getModifyBy() {
			return $this->ModifyBy;
		}

		public function getObjTasksAry(){
//			if($this->objTasksAry == null){
				$objTaskMapper = new taskMapper();
				$this->setObjTasksAry($objTaskMapper->findByStageId($this->getStageID()));
//			}
			return $this->objTasksAry;
		}

		//set to private function now , no method is provided to a assign a task to a stage in this class
		private function setObjTasksAry($objTaskAry){
			$this->objTasksAry = $objTaskAry;
		}

		public function save(){

			if($this->getStageID() != null && intval($this->getStageID) > 0){
				$recordID = $this->updateRecord();
			} else {
				$recordID = $this->newRecord();
			}

			$this->setStageID($recordID);
			return $recordID;
		}


	private function newRecord(){

		$DataArr = array();

			$DataArr["StageID"]						= $this->objDB->pack_value($this->getStageID(), "int");
			$DataArr["SchemeID"]						= $this->objDB->pack_value($this->getSchemeID(), "int");
			$DataArr["Title"]						= $this->objDB->pack_value($this->getTitle(), "str");
			$DataArr["Description"]						= $this->objDB->pack_value($this->getDescription(), "str");
			$DataArr["Sequence"]						= $this->objDB->pack_value($this->getSequence(), "int");
			$DataArr["Deadline"]						= $this->objDB->pack_value($this->getDeadline(), "date");
			$DataArr["MaxScore"]						= $this->objDB->pack_value($this->getMaxScore(), "int");
			$DataArr["Weight"]						= $this->objDB->pack_value($this->getWeight(), "int");
			$DataArr["RecordStatus"]						= $this->objDB->pack_value($this->getRecordStatus(), "int");
			$DataArr["DateInput"]						= $this->objDB->pack_value($this->getDateInput(), "date");
			$DataArr["InputBy"]						= $this->objDB->pack_value($this->getInputBy(), "int");
			$DataArr["DateModified"]						= $this->objDB->pack_value($this->getDateModified(), "date");
			$DataArr["ModifyBy"]						= $this->objDB->pack_value($this->getModifyBy(), "int");

		$sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);

		$fieldStr= $sqlStrAry['sqlField'];
		$valueStr= $sqlStrAry['sqlValue'];


		$sql = 'Insert Into '.$this->intranet_db.'.IES_STAGE ('.$fieldStr.') Values ('.$valueStr.')';
//debug_r($sql);
		$success = $this->objDB->db_db_query($sql);
								
		$RecordID = $this->objDB->db_insert_id();

		$this->setStageID($RecordID);
					
		$this->loadDataFromStorage();

		return $RecordID;

	}


	private function updateRecord(){

		$DataArr = array();

		$DataArr["SchemeID"]						= $this->objDB->pack_value($this->getSchemeID(), "int");
		$DataArr["Title"]						= $this->objDB->pack_value($this->getTitle(), "str");
		$DataArr["Description"]						= $this->objDB->pack_value($this->getDescription(), "str");
		$DataArr["Sequence"]						= $this->objDB->pack_value($this->getSequence(), "int");
		$DataArr["Deadline"]						= $this->objDB->pack_value($this->getDeadline(), "date");
		$DataArr["MaxScore"]						= $this->objDB->pack_value($this->getMaxScore(), "int");
		$DataArr["Weight"]						= $this->objDB->pack_value($this->getWeight(), "int");
		$DataArr["RecordStatus"]						= $this->objDB->pack_value($this->getRecordStatus(), "int");
		$DataArr["DateModified"]						= $this->objDB->pack_value($this->getDateModified(), "date");
		$DataArr["ModifyBy"]						= $this->objDB->pack_value($this->ModifyBy(), "int");
		
		$updateDetails = "";

		foreach ($DataArr as $fieldName => $data)
		{
			$updateDetails .= $fieldName."=".$data.",";
		}

		//REMOVE LAST OCCURRENCE OF ",";
		$updateDetails = substr($updateDetails,0,-1);

		$sql = "update ".$this->intranet_db.".IES_STAGE set ".$updateDetails." where Stageid = ".$this->getStageID();

		$this->objDB->db_db_query($sql);
		return $this->getStageID();


	}
	private function loadDataFromStorage(){

		$sql = 'select StageID, SchemeID , Title , Description, Sequence , Deadline ,   MaxScore ,   Weight  ,   RecordStatus  ,  DateInput ,   InputBy,   DateModified,  ModifyBy 
				from 
					'.$this->intranet_db.'.IES_STAGE
				where 
					StageID = '.$this->getStageID();

			$result = $this->objDB->returnResultSet($sql);

		
		if(is_array($result) && count($result) == 1){
			$result = current($result);

            $this->setSchemeID($result['SchemeID']);
            $this->setTitle($result['Title']);
            $this->setDescription($result['Description']);
            $this->setSequence($result['Sequence']);
            $this->setDeadline($result['Deadline']);
            $this->setMaxScore($result['MaxScore']);
            $this->setWeight($result['Weight']);
            $this->setRecordStatus($result['RecordStatus']);
			$this->setDateInput($result['DateInput']);
			$this->setInputBy($result['InputBy']);
			$this->setDateModified($result['DateModified']);
			$this->setModifyBy($result['ModifyBy']);

			
		}else{
			return null;
		}
	}
	
	public function ReturnValidSubmissionDate($ParDate="", $ValueReturnIfEmpty="")
	{
		global $Lang;
		
		$thisDate = trim($ParDate);
		
		$thisDate = ($thisDate=="0000-00-00") ? ($ValueReturnIfEmpty? $ValueReturnIfEmpty :$Lang['SBA']['EmptyValue']) : $ParDate;
		
		return ($thisDate);
	}
	
	
	/*
	public function Delete()
	{
		$thisStageID = $this->getStageID();
		
		# Delete corresponding tasks / steps / student handin
		// call function
		$Success['DeleteCorrespondingRecord'] = true;
		
		if(in_array(false, $Success)) {		// fail to delete corresponding records
			return false;
		} else {			// success to delete correspondong records, then delete stage 
			$Success['DeleteStage'] = $this->Remove_Stage();
		}
	} 
	
	private function Remove_Stage()
	{
		$thisStageID = $this->getStageID();
		
		if($thisStageID=="") {
			return false;	
		} else {
			
		}
	}
	*/
	
}
?>
