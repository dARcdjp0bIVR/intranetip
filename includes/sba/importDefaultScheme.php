<?php
include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");
//include_once($PATH_WRT_ROOT."includes/sba/libies.php");

include_once($PATH_WRT_ROOT."includes/sba/libSba.php");
include_once($PATH_WRT_ROOT."includes/sba/libSbaScheme.php");
include_once($PATH_WRT_ROOT."includes/sba/libSbaStage.php");
include_once($PATH_WRT_ROOT."includes/sba/libSbaTask.php");
include_once($PATH_WRT_ROOT."includes/sba/libSbaStep.php");

class importSBADefaultScheme{	

	function array_stripslashes($value){
		$value = is_array($value)?array_map(array($this, 'array_stripslashes'), $value):stripslashes($value);
		return $value;
	}
	
	function checkDefaultSchemeStatus($dsCode, $dsVersion, $dsType, $dsIncludedAfterPurchaseChecking=false){
		
		global $SBADefaultSchemeSetting, $li, $needDelete, $SBADefaultSchemeIncludedAfterPurchasedSetting;

		if($dsType!='free'&&$SBADefaultSchemeSetting[$dsCode]==false){
			return array(false,'');
		}else if($dsIncludedAfterPurchaseChecking&&!in_array($dsCode,$SBADefaultSchemeIncludedAfterPurchasedSetting)){
			return array(false,'');
		}		
		//if exists and needs update
		//delete at the end
		$li = new libdb();
		$sql = "select schemeID, DefaultSchemeCode, DefaultSchemeVersion from {$intranet_db}.IES_SCHEME where DefaultSchemeCode = $dsCode";
		$result = $li->returnArray($sql);
		$needDelete = '';
		if(isset($result[0]['DefaultSchemeCode'])){
			if($result[0]['DefaultSchemeVersion']==$dsVersion){
				return array(false,'');
			}else{
				$needDelete = $result[0]['schemeID'];
			}
		}
		return array(true,$needDelete);
	}

	function startImportDefaultScheme(){
		global $intranet_root, $plugin, $SBADefaultSchemeSetting, $SBADefaultSchemeIncludedAfterPurchasedSetting;	
		
		include_once($intranet_root."/includes/sba/initSettings/defaultSchemeData.php");
		include_once($intranet_root."/includes/libfilesystem.php");
		$libfilesystem = new libfilesystem();
		$SBADefaultSchemeSetting = $plugin['SBADefaultScheme'];
		$SBADefaultSchemeIncludedAfterPurchasedSetting = isset($plugin['SBADefaultSchemeIncludedAfterPurchased'])&&is_array($plugin['SBADefaultSchemeIncludedAfterPurchased'])?$plugin['SBADefaultSchemeIncludedAfterPurchased']:array();

		$sba_thisUserID = 1;
		
		foreach($defaultScheme as $key=>$val){
			if($val['inputSchemeDataAry']['DefaultSchemeType']=='free'&&!array_key_exists($key,$SBADefaultSchemeSetting)){
				$SBADefaultSchemeSetting[$key] = true;				
			}
		}
		
		if(!empty($SBADefaultSchemeSetting)){
			foreach((array)$SBADefaultSchemeSetting as $key => $val){
				$needDelete = '';
				
				$srcScheme = $this->array_stripslashes($defaultScheme[$key]['inputSchemeDataAry']);
				
				list($needUpdate, $needDelete) = $this->checkDefaultSchemeStatus($srcScheme['DefaultSchemeCode'], $srcScheme['DefaultSchemeVersion'], $srcScheme['DefaultSchemeType'], $srcScheme['DefaultSchemeIncludedAfterPurchaseChecking']);
				if($needUpdate){
					$SuccessArr = array();
					$li = new libdb();
					$li->Start_Trans();
					$errorMsg='noerror'; // it is for checking whether there is any error.
							
					$libSba = new libSba();
				
					$newScheme = new SbaScheme();
				
					$newScheme->setTitle($srcScheme['Title']);
					$newScheme->setIntroduction($srcScheme['Introduction']);
					$newScheme->setMaxScore($srcScheme['MaxScore']);
					$newScheme->setDateInput('now()');
					$newScheme->setInputBy($sba_thisUserID);
					$newScheme->setDateModified('now()');
					$newScheme->setModifyBy($sba_thisUserID);
					$newScheme->setVersion($srcScheme['Version']);
					$newScheme->setLanguage($srcScheme['Language']);
					$newScheme->setSchemeType($srcScheme['SchemeType']);
					$newScheme->setRecordStatus($srcScheme['DefaultSchemeRecordStatus']);
					$newScheme->setDefaultSchemeCode($srcScheme['DefaultSchemeCode']);
					$newScheme->setDefaultSchemeVersion($srcScheme['DefaultSchemeVersion']);
					$newScheme->setDescription($srcScheme['Description']);
				
					$newSchemeID = $newScheme->save();
				
					// START PART 3 :HANDLE STAGE
					$stageDetailArr = $this->array_stripslashes($defaultScheme[$key]['inputStageDataAry']);
					$MarkingCriteriaDetailArr = $this->array_stripslashes($defaultScheme[$key]['inputMarkingCriteriaDataAry']);
					$objTaskMapper = new SbaTaskMapper();
					$srcTask = $this->array_stripslashes($defaultScheme[$key]['inputTasksDataAry']);
					$objStepMapper = new SbaStepMapper();
					$srcStep = $this->array_stripslashes($defaultScheme[$key]['inputStepsDataAry']);
						
					for($i=0, $i_max= count($stageDetailArr), $j=0, $j_max=count($MarkingCriteriaDetailArr), $k=0, $k_max=count($srcTask), $l=0, $l_max=count($srcStep); $i < $i_max; $i++){
						$srcStageID = $stageDetailArr[$i]['StageID'];
						$newStage = new SbaStage();
						$newStage->setSchemeID($newSchemeID);
						$newStage->setTitle($stageDetailArr[$i]['Title']);
						$newStage->setDescription($stageDetailArr[$i]['Description']);
						$newStage->setSequence($stageDetailArr[$i]['Sequence']);
						//$newStage->setDeadline($stageDetailArr[$i]['Deadline']);
						$newStage->setMaxScore($stageDetailArr[$i]['MaxScore']);
						$newStage->setWeight($stageDetailArr[$i]['Weight']);
						$newStage->setRecordStatus(1);
						$newStage->setDateInput('now()');
						$newStage->setInputBy($sba_thisUserID);
						$newStage->setDateModified('now()');
						$newStage->setModifyBy($sba_thisUserID);
						
						$newStage->save();
						$newStageID = $newStage->getStageID();
						
						//variables handling analysis steps
						$tempStepIDAry = array(); 
						$tempAnalysisStep = array();	
						
					
						// START PART 4.1 : HANDLE MARKING CRITERIA
						for(; $j<$j_max ; $j++){
							if($MarkingCriteriaDetailArr[$j]['StageID']==$srcStageID){
								$newStageMarkingCriteriaArr = array();
								$newStageMarkingCriteriaArr['StageID'] = $newStageID;		
								$newStageMarkingCriteriaArr['MarkCriteriaID'] = $MarkingCriteriaDetailArr[$j]['MarkCriteriaID'];
								$newStageMarkingCriteriaArr['task_rubric_id'] = $MarkingCriteriaDetailArr[$j]['task_rubric_id'];
								$newStageMarkingCriteriaArr['MaxScore'] = $MarkingCriteriaDetailArr[$j]['MaxScore'];
								$newStageMarkingCriteriaArr['Weight'] = $MarkingCriteriaDetailArr[$j]['Weight'];
								$newStageMarkingCriteriaArr['RecordStatus'] = 1;
								$newStageMarkingCriteriaArr['DateInput'] = 'now()';
								$newStageMarkingCriteriaArr['InputBy'] = $sba_thisUserID;
								$newStageMarkingCriteriaArr['DateModified'] = 'now()';
								$newStageMarkingCriteriaArr['ModifyBy'] = $sba_thisUserID;
								
								$newStageMarkingCriteriaID = $libSba->insertStageMarkingCriteria($newStageMarkingCriteriaArr);
							}else{
								break;
							}				
						}
						// END PART 4.1 : MARKING CRITERIA
						
						// START PART 4.2 :HANDLE TASK 
						$attachmentFromPath = $intranet_root.'/home/eLearning/sba/defaultSchemeAttachment';
						$attachmentToPathDB = '/file/sba/scheme_attachment/scheme_s'.$newSchemeID.'/';
						$attachmentToPath = $intranet_root.$attachmentToPathDB;
						for(; $k<$k_max ; $k++){
							if($srcTask[$k]['StageID']==$srcStageID){
								$srcTaskID = $srcTask[$k]['TaskID'];
								$newTask = new Task();
								$newTask->setStageID($newStageID);
								$newTask->setTitle($srcTask[$k]['Title']);
								$newTask->setCode($srcTask[$k]['Code']);
								$newTask->setSequence($srcTask[$k]['Sequence']);
								$newTask->setApproval($srcTask[$k]['Approval']);
								$newTask->setEnable(1);
								$newTask->setIntroduction($srcTask[$k]['Introduction']);
								$newTask->setDescription($srcTask[$k]['Description']);
								$newTask->setInstantEdit($srcTask[$k]['InstantEdit']);
								$newTask->setDateInput('now()');
								$newTask->setInputBy($sba_thisUserID);
								$newTask->setDateModified('now()');
								$newTask->setModifyBy($sba_thisUserID);
								### Siuwan 2014-07-28 Add attachments into scheme
								$_newAttachment = '';
								if(!empty($srcTask[$k]['Attachments'])){
									$_attachmentAry = explode(',',$srcTask[$k]['Attachments']);
									$libfilesystem->createFolder($attachmentToPath);
									for($_a=0;$_a<count($_attachmentAry);$_a++){
										$_taskFolder = $attachmentFromPath.'/'.$srcTaskID.'/'.$_attachmentAry[$_a];
										$_attachFilePath = current($libfilesystem->return_files($_taskFolder));
										if(file_exists($_attachFilePath)){
											$_attachmentName = $libfilesystem->get_file_basename($_attachFilePath);
											$_attachmentHash = sha1($newSchemeID.microtime());
											$file_id = $libSba->addAttachment($attachmentToPathDB, $_attachmentName, $_attachmentHash, $sba_thisUserID);
											$libfilesystem->file_copy($_attachFilePath, $attachmentToPath.$_attachmentHash);
											$_newAttachment .= !empty($_newAttachment)?',':'';
											$_newAttachment .= $file_id;
										}
									}
								}
								$newTask->setAttachments($_newAttachment);
								$objTaskMapper->save($newTask);
								
								$newTaskID = $newTask->getTaskID();
							
								// START PART 5 :HANDLE STEP 
								for(; $l<=$l_max ; $l++){
									if($srcStep[$l]['TaskID']==$srcTaskID&&$l<$l_max){
										$srcStepID = $srcStep[$l]['StepID'];
										$newStep = new Step();
										$newStep->setTaskID($newTaskID);
										$newStep->setTitle($srcStep[$l]['Title']);
										$newStep->setDescription($srcStep[$l]['Description']);
										$newStep->setQuestionType($srcStep[$l]['QuestionType']);
										$newStep->setQuestion($srcStep[$l]['Question']);
										$newStep->setStepNo($srcStep[$l]['StepNo']);
										$newStep->setStatus(1);
										$newStep->setSequence($srcStep[$l]['Sequence']);
										$newStep->setSaveToTask($srcStep[$l]['SaveToTask']);
										$newStep->setDateInput('now()');
										$newStep->setInputBy($sba_thisUserID);
										$newStep->setDateModified('now()');
										$newStep->setModifyBy($sba_thisUserID);
										
										$objStepMapper->save($newStep);
										
										$newStepID = $newStep->getStepID();
										
										$tempStepIDAry[$newStepID] = $srcStepID;			
										if(strtoupper(substr($srcStep[$l]['QuestionType'],-8))=='ANALYSIS'){
											$tempAnalysisStep[$newStepID] = $srcStep[$l]['Question'];
										}
									}else{
										break;
									}
								}
							}else{
								break;
							}
						}
						if(!empty($tempAnalysisStep)){
							foreach($tempAnalysisStep as $key=>$val){
								$newStep = $objStepMapper->getByStepId($key);
								$srcStepQuestionID = explode(',',$val);
								$newStep->setQuestion(implode(',',array_keys(array_intersect($tempStepIDAry,$srcStepQuestionID))));
								$objStepMapper->save($newStep);
							}
						}
					}
				
					if(!empty($schemeLangFileContent)){
//						include_once($intranet_root."includes/libfilesystem.php");
//						$libfilesystem = new libfilesystem();
						$targetPath = "/file/sba/lang/scheme/$newSchemeID/";
						$targetFullPath = str_replace('//', '/', $intranet_root.$targetPath);
						if(!file_exists($targetFullPath)){
							$resultCreatedFolder = $libfilesystem->folder_new($targetFullPath);
						}
						chmod($targetFullPath, 0755);
						file_put_contents($targetFullPath."scheme_lang.php",str_replace("\\'","'",$schemeLangFileContent));
					}
						
					if (in_array(false, $SuccessArr)){
					  	$li->RollBack_Trans();
					    $msg = "add_failed";
					}else{
						if(!empty($needDelete)){
							$libSba->deleteSchemes(array($needDelete));
						}
						$li->Commit_Trans();
					    $msg = "add";
					}
				}
			}
		}
	}
}
?>
