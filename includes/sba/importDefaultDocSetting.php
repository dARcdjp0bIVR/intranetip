<?php
class importDefaultDocSetting extends libies {
	
	var $xmlSecDoc;
	var $stageSeq;
	var $Modifier_UserID;
	var $Scheme_ary = array();
	
	public function importDefaultDocSetting(){
		global $intranet_root, $intranet_db;
		
		$this->libdb();
		
		# File Path of the Default XML
		$this->xmlSecDoc = $intranet_root."/home/eLearning/ies/admin/settings/scheme_doc_section.default.xml";
		
		# Stage Sequence to set back to Default
		$this->stageSeq = 3;
		
		# Get UserID of account broadlearning
		//$UserLogin = "broadlearning";
		//$sql = "SELECT UserID FROM {$intranet_db}.INTRANET_USER WHERE UserLogin = \"$UserLogin\"";
		//$this->Modifier_UserID = current($this->returnVector($sql));
		$this->Modifier_UserID = 0; // using above methoed is dangerous if broadlearning acc is not set, i.e. hard code UserID = 0
		
		# Get all IES Scheme SchemeID, Title and Language
		$sql = "SELECT SchemeID, Title, Language FROM {$intranet_db}.IES_SCHEME";
		$this->Scheme_ary = $this->returnArray($sql);
	}
	
	public function import(){
		global $intranet_root,$intranet_db;
		
		$Scheme_ary = $this->Scheme_ary;
		
		$this->writeLog("------------------------------Operation Start On ".date("Y-m-d H:s:i")."------------------------------\r\n");
		
		for($i=0,$j = count($Scheme_ary); $i<$j; $i++){
			$SchemeID    = $Scheme_ary[$i]["SchemeID"];
			$SchemeTitle = $Scheme_ary[$i]["Title"];
			$SchemeLang  = $Scheme_ary[$i]["Language"];
			
			# Start SQL Transaction
			$this->Start_Trans();
			
			# Delete All existing DOC settings in table IES_STAGE_DOC_SECTION, IES_STAGE_DOC_SECTION_TASK
			$sql  = "DELETE isds, isdst ";
			$sql .= "FROM {$intranet_db}.IES_STAGE_DOC_SECTION isds ";
			$sql .= "LEFT JOIN {$intranet_db}.IES_STAGE_DOC_SECTION_TASK isdst ON isds.SectionID = isdst.SectionID ";
			$sql .= "INNER JOIN {$intranet_db}.IES_STAGE stage ON isds.StageID = stage.StageID ";
			$sql .= "WHERE stage.SchemeID = {$SchemeID} AND stage.Sequence = ".$this->stageSeq;
			$this->db_db_query($sql);
			
			$result = $this->addDocSectionFromDefault($SchemeID, $SchemeLang, $this->Modifier_UserID, $this->stageSeq, $this->xmlSecDoc);

			if($result){
				$this->writeLog("[Success] Reset IES SCHEME : $SchemeTitle ($SchemeID)\r\n");
				$this->Commit_Trans();
			}
			else{
				$this->writeLog("[Fail] Reset IES SCHEME : $SchemeTitle ($SchemeID) - Roll Back Transaction");
				$this->writeLog(mysql_error());
				$this->writeLog("\r\n");
				$this->RollBack_Trans();
				$oper_success = false;
			}
		}
		
		$this->writeLog("---------------------------------------------------------------------------------------------------------");
		
		return $oper_success!=false;
	}
	
	function writeLog($ParContent, $ParShowToScreen=false) {
		global $intranet_root;
		$ParContent .= "\n";
		$today = date("Ymd");
		if ($ParShowToScreen) {
			echo $ParContent."<br/>";
		}
		
		$logPath = $intranet_root."/file/ies/log";
		if(!file_exists($logPath))
		{
		  mkdir($logPath, 0777, true);
		}
		$logFile = "$logPath/import_default_doc_settings_$today.log";
		error_log($ParContent, 3, $logFile);
	}
}
?>