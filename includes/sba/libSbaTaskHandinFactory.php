<?php

include_once($intranet_root."/includes/sba/libSbaTaskHandin.php");

class libSbaTaskHandinFactory{
	public function __construct() {
		
    }
   
    public static function createTaskHandin($parStageID, $TaskHandinType, $parTaskID=null, $parAnswerId=null){
    	global $sba_cfg, $intranet_root;
    	
    	switch($TaskHandinType){
    		case $sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea']:
				$libFile = $intranet_root.'/includes/sba/libSbaTaskHandinTEXTAREA.php';
				if(file_exists($libFile)){
					include_once($libFile);
					return new libSbaTaskHandinTEXTAREA($parStageID, $parTaskID, $parAnswerId);
				}else{
					return null;
				}
    		break;
    		default :
    			return null;
    	}
    }
}

?>