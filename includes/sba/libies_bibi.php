<?
//using:
/*
 * Modification
 * 2016-07-11	Catherine
 * -	changed ereg() to preg_match() for PHP 5.4+
 *
 * 2011-09-28   Connie
 * -	Modify function libies_bibi()
 * -    Modify function getPresentString()
 * -    Modify function getbibiDisplaybySchemeId()
 * -    Modify function getAllbibiBySchemeId()
 * -    Modify function getTypeListDisplay()
 *
 * 2011-09-26   Connie
 * -	Modify function libies_bibi()
 * -    Add function removeNullValueOfArr()
 * -    Add function ch_num();
 *
 * 2011-01-12	Thomas	201101121535
 * - 	Add function getbibiDisplaybySchemeId()
 * -	Modify function addAnswer()
 *
 * */

class libies_bibi extends libies {

	function libies_bibi(){
		global $ies_cfg, $Lang;
		$this->libdb();

		$this->tamplate['column'] = <<<html
		REPLACE_X <input type="text" class="REPLACE_CLASS" name='REPLACE_NAME' value="REPLACE_VALUE" style="margin-top:5px;"/>
html;

		$this->tamplate['column_new'] = <<<html
		REPLACE_X<input type="text" class="REPLACE_CLASS" name='REPLACE_NAME' value="REPLACE_VALUE" style="margin-top:5px;width:25%;"/>
html;


 	    /*****************************************
 	     *        old book template HERE!        *
 	     *****************************************/

		###========= all book template HERE!======
	//-----
		$this->template['chibook'] = <<<html
		<!--作者姓名-->
        <td>{$ies_cfg['bibi']['chibook']['author']}</td>
        <td>:</td>
        <td id="author">
        	<input type="text" class="input_med" name="info[author][surename][]" value="REPLACE_{$ies_cfg['bibi']['chibook']['author']}"/>
			<!--REPLACE_MORE_{$ies_cfg['bibi']['chibook']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddOneField('author','surename')">+更多作者</a>
        </td>
      </tr>

      <tr>
      <!--譯者姓名-->
        <td>{$ies_cfg['bibi']['chibook']['translator']}</td>
        <td>:</td>
        <td id='translator'>
        	<input type="text" class="input_med" name='info[translator][surename][]' value="REPLACE_{$ies_cfg['bibi']['chibook']['translator']}"/>
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['chibook']['translator']}-->
        </td>
        <td>
        	<a href="#" onclick="bibi_AddOneField('translator','surename')">+更多譯者</a>
        </td>
      </tr>

      <tr>
      <!--編者姓名-->
        <td>{$ies_cfg['bibi']['chibook']['editor']}</td>
        <td>:</td>
        <td id='editor'>
        	<input type="text" class="input_med" name='info[editor][surename][]' value="REPLACE_{$ies_cfg['bibi']['chibook']['editor']}"/>
       		<!--REPLACE_MORE_{$ies_cfg['bibi']['chibook']['editor']}-->
        </td>
        <td>
        	<a href="#" onclick="bibi_AddOneField('editor','surename')">+更多編者</a>
        </td>
      </tr>

      <tr>
      <!--年份-->
        <td>{$ies_cfg['bibi']['chibook']['year']}</td>
        <td>:</td>
        <td><input type="text" class="input_num iscompulsory" name='info[year]' value="REPLACE_{$ies_cfg['bibi']['chibook']['year']}"/></td>
      </tr>

      <tr>
      <!--書名-->
        <td>{$ies_cfg['bibi']['chibook']['bkname']}</td>
        <td>:</td>
        <td><input class="iscompulsory" type="text" name='info[bkname]' value="REPLACE_{$ies_cfg['bibi']['chibook']['bkname']}"/></td>
      </tr>

      <tr>
      <!--文章名稱-->
        <td>{$ies_cfg['bibi']['chibook']['articlename']}</td>
        <td>:</td>
        <td><input type="text" name='info[articlename]' value="REPLACE_{$ies_cfg['bibi']['chibook']['articlename']}"/></td>
      </tr>

      <tr>
      <!--版數(如再版)-->
        <td>{$ies_cfg['bibi']['chibook']['edition']}</td>
        <td>:</td>
        <td><input type="text" class="input_med" name='info[edition]' value="REPLACE_{$ies_cfg['bibi']['chibook']['edition']}"/></td>
      </tr>

      <tr>
      <!--出版地點-->
        <td>{$ies_cfg['bibi']['chibook']['location']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[location]' value="REPLACE_{$ies_cfg['bibi']['chibook']['location']}"/></td>
      </tr>

      <tr>
      <!--出版社-->
        <td>{$ies_cfg['bibi']['chibook']['pub']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[pub]' value="REPLACE_{$ies_cfg['bibi']['chibook']['pub']}"/></td>
      </tr>
      <tr>
      <!--備注-->
        <td>{$ies_cfg['bibi']['chibook']['remark']}</td>
        <td>:</td>
        <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['chibook']['remark']}</textarea></td>
      </tr>

html;
//--------------
		$this->template['chinew'] = <<<html
		<tr>
		<!--作者姓名-->
        <td>{$ies_cfg['bibi']['chinew']['author']}</td>
        <td>:</td>
        <td id='author'>
        	<input type="text" class="input_med" name='info[author][surename][]' value="REPLACE_{$ies_cfg['bibi']['chinew']['author']}"/>
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['chinew']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddOneField('author','surename')">+更多作者</a>
        </td>
      </tr>
       <tr>
       <!--日期-->
        <td>{$ies_cfg['bibi']['chinew']['date']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num iscompulsory" name='info[date][year]' value="REPLACE_{$ies_cfg['bibi']['chinew']['date']}1"/>年<input type="text" class="input_num iscompulsory" name='info[date][month]' value="REPLACE_{$ies_cfg['bibi']['chinew']['date']}2"/>月<input type="text" class="input_num iscompulsory" name='info[date][date]' value="REPLACE_{$ies_cfg['bibi']['chinew']['date']}3"/>日
        </td>
      </tr>
      <tr>
      <!--刊物名稱-->
        <td>{$ies_cfg['bibi']['chinew']['bkname']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory" name='info[bkname]' value="REPLACE_{$ies_cfg['bibi']['chinew']['bkname']}"/></td>
      </tr>
      <tr>
      <!--文章名稱-->
        <td>{$ies_cfg['bibi']['chinew']['articlename']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory" name='info[articlename]' value="REPLACE_{$ies_cfg['bibi']['chinew']['articlename']}"/></td>
      </tr>
      <tr>
      <!--期數-->
        <td>{$ies_cfg['bibi']['chinew']['fnum']}</td>
        <td>:</td>
        <td><input type="text" class="input_num" name='info[fnum]' value="REPLACE_{$ies_cfg['bibi']['chinew']['fnum']}"/></td>
      </tr>
      <tr>
      <!--頁數-->
        <td>{$ies_cfg['bibi']['chinew']['pnum']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_med" name='info[pnum]' value="REPLACE_{$ies_cfg['bibi']['chinew']['pnum']}"/>
        </td>
      </tr>
      <tr>
      <!--備注-->
        <td>{$ies_cfg['bibi']['chinew']['remark']}</td>
        <td>:</td>
        <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['chinew']['remark']}</textarea></td>
      </tr>
html;

//------
		$this->template['chinet'] = <<<html
		<tr>
		<!--作者姓名-->
        <td>{$ies_cfg['bibi']['chinet']['author']}</td>
        <td>:</td>
        <td id='author'>
        	<input type="text" class="input_med" name='info[author][surename][]' value="REPLACE_{$ies_cfg['bibi']['chinet']['author']}"/>
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['chinet']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddOneField('author','surename')">+更多作者</a>
        </td>
      </tr>
       <tr>
       <!--年份-->
        <td>{$ies_cfg['bibi']['chinet']['year']}</td>
        <td>:</td>
        <td><input type="text" class="input_num" name='info[year]' value="REPLACE_{$ies_cfg['bibi']['chinet']['year']}"/></td>
      </tr>
      <tr>
      <!--刊物名稱-->
        <td>{$ies_cfg['bibi']['chinet']['bkname']}</td>
        <td>:</td>
        <td><input type="text" name='info[bkname]' value="REPLACE_{$ies_cfg['bibi']['chinet']['bkname']}"/></td>
      </tr>
      <tr>
      <!--文章名稱-->
        <td>{$ies_cfg['bibi']['chinet']['articlename']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory" name='info[articlename]' value="REPLACE_{$ies_cfg['bibi']['chinet']['articlename']}"/></td>
      </tr>
      <tr>
      <!--檢索日期-->
        <td>{$ies_cfg['bibi']['chinet']['date']}</td>
        <td>:</td>
        <td><input type="text" class="input_num iscompulsory" name='info[date][year]' value="REPLACE_{$ies_cfg['bibi']['chinet']['date']}1"/>年<input type="text" class="input_num iscompulsory" name='info[date][month]' value="REPLACE_{$ies_cfg['bibi']['chinet']['date']}2"/>月<input type="text" class="input_num iscompulsory" name='info[date][date]' value="REPLACE_{$ies_cfg['bibi']['chinet']['date']}3"/>日</td>
      </tr>
      <tr>
      <!--網址-->
        <td>{$ies_cfg['bibi']['chinet']['website']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_med iscompulsory" name='info[website]' value="REPLACE_{$ies_cfg['bibi']['chinet']['website']}"/>
        </td>
      </tr>
      <tr>
        <!--備注-->
        <td>{$ies_cfg['bibi']['chinet']['remark']}</td>
        <td>:</td>
        <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['chinet']['remark']}</textarea></td>
      </tr>
html;

//------
	   $this->template['manual'] = <<<html

      </tr>
       <tr>
        <td colspan='3'><textarea rows="10" name='info[manual]' >REPLACE_{$ies_cfg['bibi']['manual']['manual']}</textarea></td>
      </tr>

html;

//------
		$this->template['engbook'] = <<<html
		<tr>
		<!--Author Name-->
        <td>{$ies_cfg['bibi']['engbook']['author']}</td>
        <td>:</td>
        <td id='author'>
        	<input class="input_med" type="text" name='info[author][surename][]' value="REPLACE_{$ies_cfg['bibi']['engbook']['author']}"/>
			<!--REPLACE_MORE_{$ies_cfg['bibi']['engbook']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddOneField('author','surename')">+more</a>
        </td>
      </tr>
       <tr>
       <!--Year of Publication-->
        <td>{$ies_cfg['bibi']['engbook']['year']}</td>
        <td>:</td>
        <td><input type="text" class="input_num iscompulsory" name='info[year]' value="REPLACE_{$ies_cfg['bibi']['engbook']['year']}"/></td>
      </tr>
      <tr>
      <!--Title of Chapter/Article-->
        <td>{$ies_cfg['bibi']['engbook']['articlename']}</td>
        <td>:</td>
        <td><input type="text" class="input_med" name='info[articlename]' value="REPLACE_{$ies_cfg['bibi']['engbook']['articlename']}"/></td>
      </tr>
      <tr>
      <!--Editor Name-->
        <td>{$ies_cfg['bibi']['engbook']['editor']}</td>
        <td>:</td>
        <td id='editor'>
        	Surname <input type="text" name='info[editor][surename][]' value="REPLACE_{$ies_cfg['bibi']['engbook']['editor']}1"/>&nbsp;&nbsp;Initial <input type="text" name='info[editor][initial][]' value="REPLACE_{$ies_cfg['bibi']['engbook']['editor']}2"/>
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['engbook']['editor']}-->
        </td>
        <td>
        	<a href="#" onclick="bibi_AddMore('editor')">+more</a>
        </td>
      </tr>
      <tr>
      <!--Title of Work-->
        <td>{$ies_cfg['bibi']['engbook']['title']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[title]' value="REPLACE_{$ies_cfg['bibi']['engbook']['title']}"/></td>
      </tr>
      <tr>
      <!--Edition-->
        <td>{$ies_cfg['bibi']['engbook']['edition']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="" name='info[edition]' value="REPLACE_{$ies_cfg['bibi']['engbook']['edition']}"/>ed
        </td>
      </tr>
       <tr>
       <!--Page number-->
        <td>{$ies_cfg['bibi']['engbook']['pnum']}</td>
        <td>:</td>
        <td><input type="text" class="input_med" name='info[pnum]' value="REPLACE_{$ies_cfg['bibi']['engbook']['pnum']}"/></td>
      </tr>

      <tr>
      <!--Location-->
        <td>{$ies_cfg['bibi']['engbook']['location']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory" name='info[location]' value="REPLACE_{$ies_cfg['bibi']['engbook']['location']}"/></td>
      </tr>
      <tr>
      <!--Publisher-->
        <td>{$ies_cfg['bibi']['engbook']['pub']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[pub]' value="REPLACE_{$ies_cfg['bibi']['engbook']['pub']}"/></td>
      </tr>
      <tr>
	   <!--Remark-->
	    <td>{$ies_cfg['bibi']['engbook']['remark']}</td>
	    <td>:</td>
	    <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['engbook']['remark']}</textarea></td>
	  </tr>
html;

//------

	  $this->template['engnew'] = <<<html
		<tr>
		<!--Author Name-->
        <td>{$ies_cfg['bibi']['engnew']['author']}</td>
        <td>:</td>
        <td id='author'>
        	Surname <input type="text" name='info[author][surename][]' value="REPLACE_{$ies_cfg['bibi']['engnew']['author']}1"/>&nbsp;&nbsp;Initial <input type="text" name='info[author][initial][]' value="REPLACE_{$ies_cfg['bibi']['engnew']['author']}2"/>
			<!--REPLACE_MORE_{$ies_cfg['bibi']['engnew']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddMore('author')">+more</a>
        </td>
      </tr>
       <tr>
       <!--Year of Publication-->
        <td>{$ies_cfg['bibi']['engnew']['year']}</td>
        <td>:</td>
        <td><input type="text" class="input_num iscompulsory" name='info[year]' value="REPLACE_{$ies_cfg['bibi']['engnew']['year']}"/></td>
      </tr>
      <tr>
      <!--Title of Article-->
        <td>{$ies_cfg['bibi']['engnew']['articlename']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[articlename]' value="REPLACE_{$ies_cfg['bibi']['engnew']['articlename']}"/></td>
      </tr>
      <tr>
      <!--Title of Journal-->
        <td>{$ies_cfg['bibi']['engnew']['journal']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_med iscompulsory" name='info[journal]' value="REPLACE_{$ies_cfg['bibi']['engnew']['journal']}"/>
        </td>
      </tr>
      <tr>
      <!--Volume Number-->
        <td>{$ies_cfg['bibi']['engnew']['vnum']}</td>
        <td>:</td>
        <td><input type="text" class="input_num" name='info[vnum]' value="REPLACE_{$ies_cfg['bibi']['engnew']['vnum']}"/></td>
      </tr>
      <tr>
      <!--Issue Number-->
        <td>{$ies_cfg['bibi']['engnew']['inum']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num" name='info[inum]' value="REPLACE_{$ies_cfg['bibi']['engnew']['inum']}"/>
        </td>
      </tr>
       <tr>
       <!--Page number-->
        <td>{$ies_cfg['bibi']['engnew']['pnum']}</td>
        <td>:</td>
        <td><input type="text" class="input_med" name='info[pnum]' value="REPLACE_{$ies_cfg['bibi']['engnew']['pnum']}"/></td>
      </tr>
      <tr>
	   <!--Remark-->
	    <td>{$ies_cfg['bibi']['engnew']['remark']}</td>
	    <td>:</td>
	    <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['engnew']['remark']}</textarea></td>
	  </tr>
html;

//-----

	$this->template['engnet'] = <<<html
		<tr>
		<!--Author Name-->
        <td>{$ies_cfg['bibi']['engnet']['author']}</td>
        <td>:</td>
        <td id='author'>
        	Surname <input type="text" name='info[author][surename][]' value="REPLACE_{$ies_cfg['bibi']['engnet']['author']}1"/>&nbsp;&nbsp;Initial <input type="text" name='info[author][initial][]' value="REPLACE_{$ies_cfg['bibi']['engnet']['author']}2"/>
			<!--REPLACE_MORE_{$ies_cfg['bibi']['engnet']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddMore('author')">+more</a>
        </td>
      </tr>
       <tr>
       <!--Date-->
        <td>{$ies_cfg['bibi']['engnet']['date']}</td>
        <td>:</td>
        <td><input type="text" class="input_med" name='info[date]' value="REPLACE_{$ies_cfg['bibi']['engnet']['date']}"/></td>
      </tr>
      <tr>
      <!--Title of Work-->
        <td>{$ies_cfg['bibi']['engnet']['title']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[title]' value="REPLACE_{$ies_cfg['bibi']['engnet']['title']}"/></td>
      </tr>
      <tr>
      <!--Last browsed date<br /> (mm, dd, yyyy)-->
        <td>{$ies_cfg['bibi']['engnet']['bdate']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_med iscompulsory" name='info[bdate]' value="REPLACE_{$ies_cfg['bibi']['engnet']['bdate']}"/>
        	<font color="grey">(eg: June 9, 2010)</font>
        </td>
      </tr>
      <tr>
      <!--Web address-->
        <td>{$ies_cfg['bibi']['engnet']['website']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[website]' value="REPLACE_{$ies_cfg['bibi']['engnet']['website']}"/></td>
      </tr>
       <tr>
	   <!--Remark-->
	    <td>{$ies_cfg['bibi']['engnet']['remark']}</td>
	    <td>:</td>
	    <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['engnet']['remark']}</textarea></td>
	  </tr>

html;
###END of book template

/***********END of old book template**********/



		/*****************************************
 	     *        new book template HERE!        *
 	     *****************************************/

	//-----
		$this->template['chibook_new'] = <<<html

  		<col width="18%" />
  		<col width="1%" />
  		<col width="68%" />
  		<col width="13%" />

      <tr>
      <!--出版年份-->
        <td>{$ies_cfg['bibi']['chibook_new']['year']}</td>
        <td>:</td>
        <td><input type="text" class="input_num iscompulsory" name='info[year]' value="REPLACE_{$ies_cfg['bibi']['chibook_new']['year']}"/></td>
      </tr>


 <!--     <tr>-->
       <!--文章名稱-->
<!--         <td>{$ies_cfg['bibi']['chibook_new']['articlename']}</td>
        <td>:</td>
        <td><input type="text" name='info[articlename]' value="REPLACE_{$ies_cfg['bibi']['chibook_new']['articlename']}"/></td>
      </tr> -->


      <tr>
      <!--版數(如再版)-->
        <td>{$ies_cfg['bibi']['chibook_new']['edition']}</td>
        <td>:</td>
        <td><input type="text" class="input_med" name='info[edition]' value="REPLACE_{$ies_cfg['bibi']['chibook_new']['edition']}"/>(例如：第二版)</td>
      </tr>

      <tr>
      <!--文章名稱-->
        <td>{$ies_cfg['bibi']['chibook_new']['articlename']}</td>
        <td>:</td>
        <td><input type="text" name='info[articlename]' value="REPLACE_{$ies_cfg['bibi']['chibook_new']['articlename']}"/></td>
      </tr>

      <tr>
      <!--頁數cccc-->
        <td>{$ies_cfg['bibi']['chibook_new']['pnum']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num" name='info[pnum][from]' value="REPLACE_{$ies_cfg['bibi']['chibook_new']['pnum']}1"/> 至
        	<input type="text" class="input_num" name='info[pnum][to]' value="REPLACE_{$ies_cfg['bibi']['chibook_new']['pnum']}2"/>
        </td>
      </tr>

     <tr>
      <!--出版地點-->
       <td>{$ies_cfg['bibi']['chibook_new']['location']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[location]' value="REPLACE_{$ies_cfg['bibi']['chibook_new']['location']}"/></td>
      </tr>

     <tr>
      <!--出版社-->
        <td>{$ies_cfg['bibi']['chibook_new']['pub']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name="info[pub]" value="REPLACE_{$ies_cfg['bibi']['chibook_new']['pub']}"/></td>
      </tr>


      <tr>
      <!--引文和筆記-->
        <td>{$ies_cfg['bibi']['chibook_new']['remark']}</td>
        <td>:</td>
        <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['chibook_new']['remark']}</textarea></td>
      </tr>

      <!-- 這是否以主編當作者的書籍？ -->
      <tr >
		<td id='chIsEditedBookChoice' colspan='6' class="iscompulsory">
		 <table class="innertable">
			<td>{$ies_cfg['bibi']['chibook_new']['IsEditedBook']}<font color="red">*</font></td>
       	 	<td><input id="IsEditedBook_Yes" type="radio" name="info[IsEditedBook]" value="Yes" onclick="bibi_IsEditedBookChi('Yes')"/><label for="IsEditedBook_Yes">{$Lang['IES']['chi']['Yes']}</lable></td>
        	<td><input id="IsEditedBook_No" type="radio" name="info[IsEditedBook]" value="No" onclick="bibi_IsEditedBookChi('No')"/><label for="IsEditedBook_No">{$Lang['IES']['chi']['No']}</lable></td>
        </table>
        </td>
	  <tr>

	  <tr id ="authorChiTr" class='notShow'  >
		<!--作者姓名-->
        <td>{$ies_cfg['bibi']['chibook_new']['authorChi']}</td>
        <td>:</td>
        <td id="authorChi">
        	<input id="authorChiInput" type="text" class="input_med" name="info[authorChi][surename][]" value="REPLACE_{$ies_cfg['bibi']['chibook_new']['authorChi']}"/>
			<!--REPLACE_MORE_{$ies_cfg['bibi']['chibook_new']['authorChi']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddOneField('authorChi','surename')">+更多作者</a>
        </td>
      </tr>

	 <tr id ="editorTr" class='notShow' >
      <!--編者姓名-->
        <td>{$ies_cfg['bibi']['chibook_new']['editor']}</td>
        <td>:</td>
        <td id='editor'>
        	<input id="editorInput" type="text" class="input_med" name='info[editor][surename][]' value="REPLACE_{$ies_cfg['bibi']['chibook_new']['editor']}"/>
       		<!--REPLACE_MORE_{$ies_cfg['bibi']['chibook_new']['editor']}-->
        </td>
        <td>
        	<a href="#" onclick="bibi_AddOneField('editor','surename')">+更多編者</a>
        </td>
      </tr>

      <tr id ="authorEngTr" class='notShow'>
		<!--作者姓名(英文)-->
        <td>{$ies_cfg['bibi']['chibook_new']['authorEng']}</td>
        <td>:</td>
        <td id="authorEng">
        	Surname<input type="text" style="width:25%;" name='info[authorEng][surename][]' value="REPLACE_{$ies_cfg['bibi']['chibook_new']['authorEng']}1"/> Initial<input type="text" style="width:25%;" name='info[authorEng][initial][]' value="REPLACE_{$ies_cfg['bibi']['chibook_new']['authorEng']}2"/>(e.g. J.W.)
			<!--REPLACE_MORE_{$ies_cfg['bibi']['chibook_new']['authorEng']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddMore_new('authorEng','surename')">+more</a>
        </td>
      </tr>

      <tr id ="translatorTr" class='notShow'>
      <!--譯者姓名-->
        <td>{$ies_cfg['bibi']['chibook_new']['translator']}</td>
        <td>:</td>
        <td id='translator'>
        	<input type="text" class="input_med" name='info[translator][surename][]' value="REPLACE_{$ies_cfg['bibi']['chibook_new']['translator']}"/>
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['chibook_new']['translator']}-->
        </td>
        <td>
        	<a href="#" onclick="bibi_AddOneField('translator','surename')">+更多譯者</a>
        </td>
      </tr>



      <tr id ="bookTitleTr"  class='notShow' >
      <!--書名-->
        <td>{$ies_cfg['bibi']['chibook_new']['bkname']}<font color="red">*</font></td>
        <td>:</td>
        <td><input id="ChBookAuthorInput" class="" type="text" name='info[bkname]' value="REPLACE_{$ies_cfg['bibi']['chibook_new']['bkname']}"/></td>
      </tr>


html;
//--------------

		$this->template['chireport_new'] = <<<html
	  <tr>
		<!--作者姓名-->
        <td>{$ies_cfg['bibi']['chireport_new']['author']}</td>
        <td>:</td>
        <td id="author">
        	<input type="text" class="input_med" name="info[author][surename][]" value="REPLACE_{$ies_cfg['bibi']['chireport_new']['author']}"/>
			<!--REPLACE_MORE_{$ies_cfg['bibi']['chireport_new']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddOneField('author','surename')">+更多作者</a>
        </td>
      </tr>

      <tr>
      <!--機構名稱-->
        <td>{$ies_cfg['bibi']['chireport_new']['organization']}</td>
        <td>:</td>
        <td id='organization'>
        	<input type="text" class="input_med" name="info[organization][]" value="REPLACE_{$ies_cfg['bibi']['chireport_new']['organization']}"/>
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['chireport_new']['organization']}-->
        </td>
        <td>
        	<a href="#" onclick="bibi_AddOrgan('organization')">+更多機構</a>
        </td>
      </tr>

      <tr>
      <!--出版年份-->
        <td>{$ies_cfg['bibi']['chireport_new']['year']}</td>
        <td>:</td>
        <td><input type="text" class="input_num iscompulsory" name="info[year]" value="REPLACE_{$ies_cfg['bibi']['chireport_new']['year']}"/></td>
      </tr>

      <tr>
      <!--出版月份-->
        <td>{$ies_cfg['bibi']['chireport_new']['month']}</td>
        <td>:</td>
        <td><input type="text" class="input_num" name="info[month]" value="REPLACE_{$ies_cfg['bibi']['chireport_new']['month']}"/></td>
      </tr>

      <tr>
      <!--出版日期-->
        <td>{$ies_cfg['bibi']['chireport_new']['day']}</td>
        <td>:</td>
        <td><input type="text" class="input_num" name="info[day]" value="REPLACE_{$ies_cfg['bibi']['chireport_new']['day']}"/></td>
      </tr>

      <tr>
      <!--報告名稱-->
        <td>{$ies_cfg['bibi']['chireport_new']['title']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory"  name="info[title]" value="REPLACE_{$ies_cfg['bibi']['chireport_new']['title']}"/></td>
      </tr>

      <tr>
      <!--引文和筆記-->
        <td>{$ies_cfg['bibi']['chireport_new']['remark']}</td>
        <td>:</td>
        <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['chireport_new']['remark']}</textarea></td>
      </tr>

html;
//--------------

		$this->template['chijournals_new'] = <<<html
		<tr>
		<!--作者姓名-->
        <td>{$ies_cfg['bibi']['chijournals_new']['author']}</td>
        <td>:</td>
        <td id='author'>
        	<input type="text" class="input_med iscompulsory" name='info[author][surename][]' value="REPLACE_{$ies_cfg['bibi']['chijournals_new']['author']}"/>
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['chijournals_new']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddOneField('author','surename')">+更多作者</a>
        </td>
      </tr>
       <tr>
       <!--出版年份-->
        <td>{$ies_cfg['bibi']['chijournals_new']['year']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num iscompulsory" name='info[year]' value="REPLACE_{$ies_cfg['bibi']['chijournals_new']['year']}"/>
        </td>
      </tr>
      <tr>
      <!--刊物名稱-->
        <td>{$ies_cfg['bibi']['chijournals_new']['bkname']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory" name='info[bkname]' value="REPLACE_{$ies_cfg['bibi']['chijournals_new']['bkname']}"/></td>
      </tr>
      <tr>
      <!--文章名稱-->
        <td>{$ies_cfg['bibi']['chijournals_new']['articlename']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory" name='info[articlename]' value="REPLACE_{$ies_cfg['bibi']['chijournals_new']['articlename']}"/></td>
      </tr>
      <tr>
      <!--卷數-->
        <td>{$ies_cfg['bibi']['chijournals_new']['gnum']}</td>
        <td>:</td>
        <td><input type="text" class="input_num" name='info[gnum]' value="REPLACE_{$ies_cfg['bibi']['chijournals_new']['gnum']}"/></td>
      </tr>
      <tr>
      <tr>
      <!--期數-->
        <td>{$ies_cfg['bibi']['chijournals_new']['fnum']}</td>
        <td>:</td>
        <td><input type="text" class="input_num" name='info[fnum]' value="REPLACE_{$ies_cfg['bibi']['chijournals_new']['fnum']}"/></td>
      </tr>
      <tr>
      <!--頁數-->
        <td>{$ies_cfg['bibi']['chijournals_new']['pnum']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num" name='info[pnum][from]' value="REPLACE_{$ies_cfg['bibi']['chijournals_new']['pnum']}1"/> 至
        	<input type="text" class="input_num" name='info[pnum][to]' value="REPLACE_{$ies_cfg['bibi']['chijournals_new']['pnum']}2"/>
        </td>
      </tr>
      <tr>
      <!--引文和筆記-->
        <td>{$ies_cfg['bibi']['chijournals_new']['remark']}</td>
        <td>:</td>
        <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['chijournals_new']['remark']}</textarea></td>
      </tr>

html;

//--------------

	  $this->template['chimagazine_new'] = <<<html
		<tr>
		<!--作者姓名-->
        <td>{$ies_cfg['bibi']['chimagazine_new']['author']}</td>
        <td>:</td>
        <td id='author'>
        	<input type="text" class="input_med" name='info[author][surename][]' value="REPLACE_{$ies_cfg['bibi']['chimagazine_new']['author']}"/>
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['chimagazine_new']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddOneField('author','surename')">+更多作者</a>
        </td>
      </tr>
       <tr>
       <!--出版年份-->
        <td>{$ies_cfg['bibi']['chimagazine_new']['year']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num iscompulsory" name='info[year]' value="REPLACE_{$ies_cfg['bibi']['chimagazine_new']['year']}"/>
        </td>
      </tr>

      <tr>
       <!--出版月份-->
        <td>{$ies_cfg['bibi']['chimagazine_new']['month']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num" name='info[month]' value="REPLACE_{$ies_cfg['bibi']['chimagazine_new']['month']}"/>
        </td>
      </tr>

      <tr>
       <!--出版日期-->
        <td>{$ies_cfg['bibi']['chimagazine_new']['day']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num" name='info[day]' value="REPLACE_{$ies_cfg['bibi']['chimagazine_new']['day']}"/>
        </td>
      </tr>

      <tr>
      <!--刊物名稱-->
        <td>{$ies_cfg['bibi']['chimagazine_new']['bkname']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory" name='info[bkname]' value="REPLACE_{$ies_cfg['bibi']['chimagazine_new']['bkname']}"/></td>
      </tr>
      <tr>
      <!--文章名稱-->
        <td>{$ies_cfg['bibi']['chimagazine_new']['articlename']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory" name='info[articlename]' value="REPLACE_{$ies_cfg['bibi']['chimagazine_new']['articlename']}"/></td>
      </tr>
      <tr>
      <!--期數-->
        <td>{$ies_cfg['bibi']['chimagazine_new']['fnum']}</td>
        <td>:</td>
        <td><input type="text" class="input_num" name='info[fnum]' value="REPLACE_{$ies_cfg['bibi']['chimagazine_new']['fnum']}"/></td>
      </tr>
      <tr>
      <!--頁數-->
        <td>{$ies_cfg['bibi']['chimagazine_new']['pnum']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num" name='info[pnum][from]' value="REPLACE_{$ies_cfg['bibi']['chimagazine_new']['pnum']}1"/> 至
        	<input type="text" class="input_num" name='info[pnum][to]' value="REPLACE_{$ies_cfg['bibi']['chimagazine_new']['pnum']}2"/>
        </td>
      </tr>
      <tr>
      <!--引文和筆記-->
        <td>{$ies_cfg['bibi']['chimagazine_new']['remark']}</td>
        <td>:</td>
        <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['chimagazine_new']['remark']}</textarea></td>
      </tr>

html;


//------
		$this->template['chinew_new'] = <<<html
		<tr>
		<!--作者姓名-->
        <td>{$ies_cfg['bibi']['chinew_new']['author']}</td>
        <td>:</td>
        <td id='author'>
        	<input type="text" class="input_med" name='info[author][surename][]' value="REPLACE_{$ies_cfg['bibi']['chinew_new']['author']}"/>
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['chinew_new']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddOneField('author','surename')">+更多作者</a>
        </td>
      </tr>
       <tr>
       <!--出版日期-->
        <td>{$ies_cfg['bibi']['chinew_new']['date']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num iscompulsory" name='info[date][year]' value="REPLACE_{$ies_cfg['bibi']['chinew_new']['date']}1"/>年<input type="text" class="input_num iscompulsory" name='info[date][month]' value="REPLACE_{$ies_cfg['bibi']['chinew_new']['date']}2"/>月<input type="text" class="input_num iscompulsory" name='info[date][day]' value="REPLACE_{$ies_cfg['bibi']['chinew_new']['date']}3"/>日
        </td>
      </tr>
      <tr>
      <!--報章名稱-->
        <td>{$ies_cfg['bibi']['chinew_new']['bkname']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory" name='info[bkname]' value="REPLACE_{$ies_cfg['bibi']['chinew_new']['bkname']}"/></td>
      </tr>
      <tr>
      <!--文章名稱-->
        <td>{$ies_cfg['bibi']['chinew_new']['articlename']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory" name='info[articlename]' value="REPLACE_{$ies_cfg['bibi']['chinew_new']['articlename']}"/></td>
      </tr>
      <tr>
      <!--頁數-->
        <td>{$ies_cfg['bibi']['chinew_new']['pnum']}</td>
        <td>:</td>
        <td>
        	<input type="text" style="width:8%;" class="" name='info[pnum][from]' value="REPLACE_{$ies_cfg['bibi']['chinew_new']['pnum']}1"/> 至
        	<input type="text" style="width:8%;" class="" name='info[pnum][to]' value="REPLACE_{$ies_cfg['bibi']['chinew_new']['pnum']}2"/>
        </td>
      </tr>
      <tr>
      <!--引文和筆記-->
        <td>{$ies_cfg['bibi']['chinew_new']['remark']}</td>
        <td>:</td>
        <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['chinew_new']['remark']}</textarea></td>
      </tr>

html;

//------
		$this->template['chinet_new'] = <<<html
		<tr>
		<!--作者姓名-->
        <td>{$ies_cfg['bibi']['chinet_new']['author']}</td>
        <td>:</td>
        <td id='author'>
        	<input type="text" class="input_med" name='info[author][surename][]' value="REPLACE_{$ies_cfg['bibi']['chinet_new']['author']}"/>
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['chinet_new']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddOneField('author','surename')">+更多作者</a>
        </td>

      <tr>
		<!--機構名稱-->
        <td>{$ies_cfg['bibi']['chinet_new']['organization']}</td>
        <td>:</td>
        <td id='organization'>
        	<input type="text" class="input_med" name="info[organization][]" value="REPLACE_{$ies_cfg['bibi']['chinet_new']['organization']}"/>
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['chinet_new']['organization']}-->
        </td>
        <td>
        	<a href="#" onclick="bibi_AddOrgan('organization')">+更多機構</a>
        </td>
      </tr>


      </tr>
       <tr>
       <!--年份-->
        <td>{$ies_cfg['bibi']['chinet_new']['year']}</td>
        <td>:</td>
        <td><input type="text" class="input_num" name='info[year]' value="REPLACE_{$ies_cfg['bibi']['chinet_new']['year']}"/></td>
      </tr>
      <tr>
      <!--網頁名稱-->
        <td>{$ies_cfg['bibi']['chinet_new']['articlename']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory" name='info[articlename]' value="REPLACE_{$ies_cfg['bibi']['chinet_new']['articlename']}"/></td>
      </tr>
      <tr>
      <!--瀏覽日期-->
        <td>{$ies_cfg['bibi']['chinet_new']['date']}</td>
        <td>:</td>
        <td><input type="text" class="input_num iscompulsory" name='info[date][year]' value="REPLACE_{$ies_cfg['bibi']['chinet_new']['date']}1"/>年<input type="text" class="input_num iscompulsory" name='info[date][month]' value="REPLACE_{$ies_cfg['bibi']['chinet_new']['date']}2"/>月<input type="text" class="input_num iscompulsory" name='info[date][date]' value="REPLACE_{$ies_cfg['bibi']['chinet_new']['date']}3"/>日</td>
      </tr>
      <tr>
      <!--網址-->
        <td>{$ies_cfg['bibi']['chinet_new']['website']}</td>
        <td>:</td>
        <td>
        	<input style="width:90%;"  type="text" class="input_med iscompulsory" name='info[website]' value="REPLACE_{$ies_cfg['bibi']['chinet_new']['website']}"/>
        </td>
      </tr>
      <tr>
        <!--引文和筆記-->
        <td>{$ies_cfg['bibi']['chinet_new']['remark']}</td>
        <td>:</td>
        <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['chinet_new']['remark']}</textarea></td>
      </tr>

html;

//------
	   $this->template['chiother_new'] = <<<html

      </tr>
       <tr>
        <td colspan='3'><textarea rows="10" name='info[other]' >REPLACE_{$ies_cfg['bibi']['chiother_new']['other']}</textarea></td>
      </tr>

html;

//------eng book new
		$this->template['engbook_new'] = <<<html

         <col width="18%" />
  		<col width="1%" />
  		<col width="68%" />
  		<col width="13%" />

       <tr>
       <!--Year of Publication-->
        <td>{$ies_cfg['bibi']['engbook_new']['year']}</td>
        <td>:</td>
        <td><input type="text" class="input_num iscompulsory" name='info[year]' value="REPLACE_{$ies_cfg['bibi']['engbook_new']['year']}"/></td>
      </tr>

      <!--Edition-->
        <td>{$ies_cfg['bibi']['engbook_new']['edition']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="" name='info[edition]' value="REPLACE_{$ies_cfg['bibi']['engbook_new']['edition']}"/>ed. (e.g. 1st, 2nd,3rd…)
        </td>
      </tr>
       <tr>
       <!--Page number-->
        <td>{$ies_cfg['bibi']['engbook_new']['pnum']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num" name='info[pnum][from]' value="REPLACE_{$ies_cfg['bibi']['engbook_new']['pnum']}1"/> to
        	<input type="text" class="input_num" name='info[pnum][to]' value="REPLACE_{$ies_cfg['bibi']['engbook_new']['pnum']}2"/>
        </td>
      </tr>

      <tr>
      <!--Place of publication-->
        <td>{$ies_cfg['bibi']['engbook_new']['location']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory" name='info[location]' value="REPLACE_{$ies_cfg['bibi']['engbook_new']['location']}"/></td>
      </tr>

      <tr>
		 <!--Publisher-->
      <td>{$ies_cfg['bibi']['engbook_new']['pub']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[pub]' value="REPLACE_{$ies_cfg['bibi']['engbook_new']['pub']}"/></td>
      </tr>

	  <tr>
	   <!--Remark-->
	    <td>{$ies_cfg['bibi']['engbook_new']['remark']}</td>
	    <td>:</td>
	    <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['engbook_new']['remark']}</textarea></td>
	  </tr>

	  <!-- Is this an edited book? --> <!--bbbb-->
      <tr>
        <td width='100%' id='enIsEditedBookChoice' colspan='4' class="iscompulsory">
        <table class="innertable">
        <td>{$ies_cfg['bibi']['engbook_new']['IsEditedBook']}<font color='red'>*</font></td>
        <td><input id="IsEditedBook_Yes" type="radio" name='info[IsEditedBook]' value="Yes" onclick="bibi_IsEditedBookEng('Yes')" /><label for="IsEditedBook_Yes">{$Lang['IES']['eng']['Yes']}</lable></td>
        <td><input id="IsEditedBook_No" type="radio" name='info[IsEditedBook]' value="No" onclick="bibi_IsEditedBookEng('No')" /><label for="IsEditedBook_No">{$Lang['IES']['eng']['No']}</lable></td>
        </table>
        </td>
	  <tr>

	  <tr id="bookAuthorTr" class='notShow' >
		<!--Author Name-->
        <td>{$ies_cfg['bibi']['engbook_new']['bookAuthor']}<font color="red">*</font></td>

        <td>:</td>
        <td id='bookAuthor'>
        	Surname<input id="bookAuthorInput" type="text" style="width:25%;" class="" name='info[bookAuthor][surename][]' value="REPLACE_{$ies_cfg['bibi']['engbook_new']['bookAuthor']}1"/> Initial<input type="text" style="width:25%;" name='info[bookAuthor][initial][]' value="REPLACE_{$ies_cfg['bibi']['engbook_new']['bookAuthor']}2"/>(e.g. J.W.)
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['engbook_new']['bookAuthor']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddMore_new('bookAuthor')">+more</a>
        </td>
      </tr>



    <tr id="chapterAuthorTr" class='notShow' >
		<!--Author Name-->
        <td>{$ies_cfg['bibi']['engbook_new']['chapterAuthor']}</td>
        <td>:</td>
        <td id='chapterAuthor'>
        	Surname<input type="text" style="width:25%;" name='info[chapterAuthor][surename][]' value="REPLACE_{$ies_cfg['bibi']['engbook_new']['chapterAuthor']}1"/> Initial<input type="text" style="width:25%;" name='info[chapterAuthor][initial][]' value="REPLACE_{$ies_cfg['bibi']['engbook_new']['chapterAuthor']}2"/>(e.g. J.W.)
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['engbook_new']['chapterAuthor']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddMore_new('chapterAuthor')">+more</a>
        </td>
      </tr>

      <tr id="chapterTitleTr" class='notShow' >
       <!--Book Title-->
        <td>{$ies_cfg['bibi']['engbook_new']['chapterTitle']}</td>
        <td>:</td>
        <td><input type="text" class="input_med" name='info[chapterTitle]' value="REPLACE_{$ies_cfg['bibi']['engbook_new']['chapterTitle']}"/></td>
      </tr>

      <tr id="bookEditorTr" class='notShow' >
      <!--Editor Name-->
        <td>{$ies_cfg['bibi']['engbook_new']['bookEditor']}<font color="red">*</font></td>
        <td>:</td>
        <td id='bookEditor'>
        	Surname<input id="bookEditorInput" type="text" style="width:25%;" class="" name='info[bookEditor][surename][]' value="REPLACE_{$ies_cfg['bibi']['engbook_new']['bookEditor']}1"/> Initial<input type="text" style="width:25%;" name='info[bookEditor][initial][]' value="REPLACE_{$ies_cfg['bibi']['engbook_new']['bookEditor']}2"/>(e.g. J.W.)
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['engbook_new']['bookEditor']}-->
        </td>
        <td>
        	<a href="#" onclick="bibi_AddMore_new('bookEditor')">+more</a>
        </td>
      </tr>

      <tr id="bookTitleTr" class='notShow' >
       <!--Book Title-->
        <td>{$ies_cfg['bibi']['engbook_new']['bookTitle']}<font color="red">*</font></td>
        <td>:</td>
        <td><input id='EnBooktitileInput'  type="text" class="" name='info[bookTitle]' value="REPLACE_{$ies_cfg['bibi']['engbook_new']['bookTitle']}"/></td>
      </tr>
html;

//------
		$this->template['engreport_new'] = <<<html
		<tr>
		<!--Author Name-->
        <td>{$ies_cfg['bibi']['engreport_new']['author']}</td>
        <td>:</td>
        <td id='author'>
        	Surname<input type="text" style="width:25%;" name='info[author][surename][]' value="REPLACE_{$ies_cfg['bibi']['engreport_new']['author']}1"/> Initial<input type="text" style="width:25%;" name='info[author][initial][]' value="REPLACE_{$ies_cfg['bibi']['engreport_new']['author']}2"/>(e.g. J.W.)
			<!--REPLACE_MORE_{$ies_cfg['bibi']['engreport_new']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddMore_new('author')">+more</a>
        </td>
      </tr>

      <tr>
		<!--Organization -->
        <td>{$ies_cfg['bibi']['engreport_new']['organization']}</td>
        <td>:</td>
        <td id='organization'>
        	<input type="text" class="input_med" name="info[organization][]" value="REPLACE_{$ies_cfg['bibi']['engreport_new']['organization']}"/>
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['engreport_new']['organization']}-->
        </td>
        <td>
        	<a href="#" onclick="bibi_AddOrgan('organization')">+more</a>
        </td>
      </tr>

       <tr>
       <!--Year of Publication-->
        <td>{$ies_cfg['bibi']['engreport_new']['year']}</td>
        <td>:</td>
        <td><input type="text" class="input_num iscompulsory" name='info[year]' value="REPLACE_{$ies_cfg['bibi']['engreport_new']['year']}"/></td>
      </tr>
      <tr>
      <!--Title of Report-->
        <td>{$ies_cfg['bibi']['engreport_new']['title']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[title]' value="REPLACE_{$ies_cfg['bibi']['engreport_new']['title']}"/></td>
      </tr>
      <tr>

      <tr>
      <!--Location-->
        <td>{$ies_cfg['bibi']['engreport_new']['location']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory" name='info[location]' value="REPLACE_{$ies_cfg['bibi']['engreport_new']['location']}"/></td>
      </tr>
      <tr>
      <!--Publisher-->
        <td>{$ies_cfg['bibi']['engreport_new']['pub']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[pub]' value="REPLACE_{$ies_cfg['bibi']['engreport_new']['pub']}"/></td>
      </tr>
      <tr>
	   <!--Remark-->
	    <td>{$ies_cfg['bibi']['engreport_new']['remark']}</td>
	    <td>:</td>
	    <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['engreport_new']['remark']}</textarea></td>
	  </tr>

html;
//------

	  $this->template['engjournals_new'] = <<<html
		<tr>
		<!--Author Name-->
        <td>{$ies_cfg['bibi']['engjournals_new']['author']}</td>
        <td>:</td>
        <td id='author'>
        	Surname<input type="text" style="width:25%;" name='info[author][surename][]' value="REPLACE_{$ies_cfg['bibi']['engjournals_new']['author']}1"/> Initial<input type="text" style="width:25%;" name='info[author][initial][]' value="REPLACE_{$ies_cfg['bibi']['engjournals_new']['author']}2"/>(e.g. J.W.)
			<!--REPLACE_MORE_{$ies_cfg['bibi']['engjournals_new']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddMore_new('author')">+more</a>
        </td>
      </tr>
       <tr>
       <!--Year of Publication-->
        <td>{$ies_cfg['bibi']['engjournals_new']['year']}</td>
        <td>:</td>
        <td><input type="text" class="input_num iscompulsory" name='info[year]' value="REPLACE_{$ies_cfg['bibi']['engjournals_new']['year']}"/></td>
      </tr>
      <tr>
      <!--Title of Article-->
        <td>{$ies_cfg['bibi']['engjournals_new']['articlename']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[articlename]' value="REPLACE_{$ies_cfg['bibi']['engjournals_new']['articlename']}"/></td>
      </tr>
      <tr>
      <!--Title of Journal-->
        <td>{$ies_cfg['bibi']['engjournals_new']['journal']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_med iscompulsory" name='info[journal]' value="REPLACE_{$ies_cfg['bibi']['engjournals_new']['journal']}"/>
        </td>
      </tr>
      <tr>
      <!--Volume Number-->
        <td>{$ies_cfg['bibi']['engjournals_new']['vnum']}</td>
        <td>:</td>
        <td><input type="text" class="input_num" name='info[vnum]' value="REPLACE_{$ies_cfg['bibi']['engjournals_new']['vnum']}"/></td>
      </tr>
      <tr>
      <!--Issue Number-->
        <td>{$ies_cfg['bibi']['engjournals_new']['inum']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num" name='info[inum]' value="REPLACE_{$ies_cfg['bibi']['engjournals_new']['inum']}"/>
        </td>
      </tr>
       <tr>
       <!--Page number-->
        <td>{$ies_cfg['bibi']['engjournals_new']['pnum']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num" name='info[pnum][from]' value="REPLACE_{$ies_cfg['bibi']['engjournals_new']['pnum']}1"/> to
        	<input type="text" class="input_num" name='info[pnum][to]' value="REPLACE_{$ies_cfg['bibi']['engjournals_new']['pnum']}2"/>
		</td>
      </tr>
      <tr>
	   <!--Remark-->
	    <td>{$ies_cfg['bibi']['engjournals_new']['remark']}</td>
	    <td>:</td>
	    <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['engjournals_new']['remark']}</textarea></td>
	  </tr>

html;

//-----

	  $this->template['engmagazine_new'] = <<<html
		<tr>
		<!--Author Name-->
        <td>{$ies_cfg['bibi']['engmagazine_new']['author']}</td>
        <td>:</td>
        <td id='author'>
        	Surname<input type="text" style="width:25%;" name='info[author][surename][]' value="REPLACE_{$ies_cfg['bibi']['engmagazine_new']['author']}1"/> Initial<input type="text" style="width:25%;" name='info[author][initial][]' value="REPLACE_{$ies_cfg['bibi']['engmagazine_new']['author']}2"/>(e.g. J.W.)
			<!--REPLACE_MORE_{$ies_cfg['bibi']['engmagazine_new']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddMore_new('author')">+more</a>
        </td>
      </tr>

		<tr>
	   <!--Year of Publication-->
		<td>{$ies_cfg['bibi']['engmagazine_new']['year']}</td>
		<td>:</td>
		<td><input type="text" class="input_num iscompulsory" name='info[year]' value="REPLACE_{$ies_cfg['bibi']['engmagazine_new']['year']}"/>(e.g. 2010)</td>
		</tr>

	  <tr>
	   <!--Month of Publication-->
		<td>{$ies_cfg['bibi']['engmagazine_new']['month']}</td>
		<td>:</td>
		<td><input type="text"  name='info[month]' value="REPLACE_{$ies_cfg['bibi']['engmagazine_new']['month']}"/>(e.g. June)</td>
	  </tr>

	  <tr>
	   <!--Day of Publication-->
		<td>{$ies_cfg['bibi']['engmagazine_new']['day']}</td>
		<td>:</td>
		<td><input type="text" class="input_num" name='info[day]' value="REPLACE_{$ies_cfg['bibi']['engmagazine_new']['day']}"/>(e.g. 9)</td>
	  </tr>

	  <tr>
      <!--Title of Article-->
        <td>{$ies_cfg['bibi']['engmagazine_new']['articlename']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[articlename]' value="REPLACE_{$ies_cfg['bibi']['engmagazine_new']['articlename']}"/></td>
      </tr>
      <tr>
      <!--Title of Magazine-->
        <td>{$ies_cfg['bibi']['engmagazine_new']['magazine']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_med iscompulsory" name='info[magazine]' value="REPLACE_{$ies_cfg['bibi']['engmagazine_new']['magazine']}"/>
        </td>
      </tr>
      <tr>
      <!--Volume Number-->
        <td>{$ies_cfg['bibi']['engmagazine_new']['vnum']}</td>
        <td>:</td>
        <td><input type="text" class="input_num" name='info[vnum]' value="REPLACE_{$ies_cfg['bibi']['engmagazine_new']['vnum']}"/></td>
      </tr>
      <tr>
      <!--Issue Number-->
        <td>{$ies_cfg['bibi']['engmagazine_new']['inum']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num" name='info[inum]' value="REPLACE_{$ies_cfg['bibi']['engmagazine_new']['inum']}"/>
        </td>
      </tr>
       <tr>
       <!--Page number-->
        <td>{$ies_cfg['bibi']['engmagazine_new']['pnum']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_num" name='info[pnum][from]' value="REPLACE_{$ies_cfg['bibi']['engmagazine_new']['pnum']}1"/> to
        	<input type="text" class="input_num" name='info[pnum][to]' value="REPLACE_{$ies_cfg['bibi']['engmagazine_new']['pnum']}2"/>
        </td>
      </tr>
      <tr>
	   <!--Remark-->
	    <td>{$ies_cfg['bibi']['engmagazine_new']['remark']}</td>
	    <td>:</td>
	    <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['engmagazine_new']['remark']}</textarea></td>
	  </tr>
html;

//-----

	  $this->template['engnew_new'] = <<<html
		<tr>
		<!--Author Name-->
        <td>{$ies_cfg['bibi']['engnew_new']['author']}</td>
        <td>:</td>
        <td id='author'>
        	Surname<input type="text" style="width:25%;" name='info[author][surename][]' value="REPLACE_{$ies_cfg['bibi']['engnew_new']['author']}1"/> Initial<input type="text" style="width:25%;" name='info[author][initial][]' value="REPLACE_{$ies_cfg['bibi']['engnew_new']['author']}2"/>(e.g. J.W.)
			<!--REPLACE_MORE_{$ies_cfg['bibi']['engnew_new']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddMore_new('author')">+more</a>
        </td>
      </tr>
       <tr>
       <!--Date of Publication-->
        <td>{$ies_cfg['bibi']['engnew_new']['date']}</td>
        <td>:</td>
        <td><input type="text" class="iscompulsory" name='info[date]' value="REPLACE_{$ies_cfg['bibi']['engnew_new']['date']}"/>(e.g. 2010, June 9)</td>
      </tr>
      <tr>
      <!--Title of Article-->
        <td>{$ies_cfg['bibi']['engnew_new']['articlename']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[articlename]' value="REPLACE_{$ies_cfg['bibi']['engnew_new']['articlename']}"/></td>
      </tr>
      <tr>
      <!--Title of Newspaper-->
        <td>{$ies_cfg['bibi']['engnew_new']['newspaper']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_med iscompulsory" name='info[newspaper]' value="REPLACE_{$ies_cfg['bibi']['engnew_new']['newspaper']}"/>
        </td>
      </tr>

       <tr>
       <!--Page -->
        <td>{$ies_cfg['bibi']['engnew_new']['pnum']}</td>
        <td>:</td>
        <td><input type="text" style="width:8%" class="input_med" name='info[pnum]' value="REPLACE_{$ies_cfg['bibi']['engnew_new']['pnum']}"/></td>
      </tr>
      <tr>
	   <!--Remark-->
	    <td>{$ies_cfg['bibi']['engnew_new']['remark']}</td>
	    <td>:</td>
	    <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['engnew_new']['remark']}</textarea></td>
	  </tr>

html;

//-----

	$this->template['engnet_new'] = <<<html
		<tr>
		<!--Author Name-->
        <td>{$ies_cfg['bibi']['engnet_new']['author']}</td>
        <td>:</td>
        <td id='author'>
        	Surname<input type="text" style="width:25%;" name='info[author][surename][]' value="REPLACE_{$ies_cfg['bibi']['engnet_new']['author']}1"/> Initial<input type="text" style="width:25%;" name='info[author][initial][]' value="REPLACE_{$ies_cfg['bibi']['engnet_new']['author']}2"/>(e.g. J.W.)
			<!--REPLACE_MORE_{$ies_cfg['bibi']['engnet_new']['author']}-->
		</td>
        <td>
        	<a href="#" onclick="bibi_AddMore_new('author')">+more</a>
        </td>
      </tr>

      <tr>
		<!--Organization-->
        <td>{$ies_cfg['bibi']['engnet_new']['organization']}</td>
        <td>:</td>
        <td id='organization'>
        	<input type="text" class="input_med" name="info[organization][]" value="REPLACE_{$ies_cfg['bibi']['engnet_new']['organization']}"/>
        	<!--REPLACE_MORE_{$ies_cfg['bibi']['engnet_new']['organization']}-->
        </td>
        <td>
        	<a href="#" onclick="bibi_AddOrgan('organization')">+more</a>
        </td>
      </tr>

		<tr>
	   <!--Year of Publication-->
		<td>{$ies_cfg['bibi']['engnet_new']['year']}</td>
		<td>:</td>
		<td><input type="text" class="input_num" name='info[year]' value="REPLACE_{$ies_cfg['bibi']['engnet_new']['year']}"/>(e.g. 2010)</td>
		</tr>

	  <tr>
	   <!--Month of Publication-->
		<td>{$ies_cfg['bibi']['engnet_new']['month']}</td>
		<td>:</td>
		<td><input type="text"  name='info[month]' value="REPLACE_{$ies_cfg['bibi']['engnet_new']['month']}"/>(e.g. June)</td>
	  </tr>

	  <tr>
	   <!--Day of Publication-->
		<td>{$ies_cfg['bibi']['engnet_new']['day']}</td>
		<td>:</td>
		<td><input type="text" class="input_num" name='info[day]' value="REPLACE_{$ies_cfg['bibi']['engnet_new']['day']}"/>(e.g. 9)</td>
	  </tr>

      <tr>
      <!--Title of Work-->
        <td>{$ies_cfg['bibi']['engnet_new']['title']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[title]' value="REPLACE_{$ies_cfg['bibi']['engnet_new']['title']}"/></td>
      </tr>
      <tr>
      <!--Date of retrieval -->
        <td>{$ies_cfg['bibi']['engnet_new']['bdate']}</td>
        <td>:</td>
        <td>
        	<input type="text" class="input_med iscompulsory" name='info[bdate]' value="REPLACE_{$ies_cfg['bibi']['engnet_new']['bdate']}"/>
        	<font>(e.g. June 9, 2010)</font>
        </td>
      </tr>
      <tr>
      <!--Web address-->
        <td>{$ies_cfg['bibi']['engnet_new']['website']}</td>
        <td>:</td>
        <td><input type="text" class="input_med iscompulsory" name='info[website]' value="REPLACE_{$ies_cfg['bibi']['engnet_new']['website']}"/></td>
      </tr>
       <tr>
	   <!--Remark-->
	    <td>{$ies_cfg['bibi']['engnet_new']['remark']}</td>
	    <td>:</td>
	    <td colspan='3'><textarea rows="10" name='info[remark]' >REPLACE_{$ies_cfg['bibi']['engnet_new']['remark']}</textarea></td>
	  </tr>

html;

//-------------
	   $this->template['engother_new'] = <<<html

      </tr>
       <tr>
        <td colspan='3'><textarea rows="10" name='info[other]' >REPLACE_{$ies_cfg['bibi']['engother_new']['other']}</textarea></td>
      </tr>

html;


/***********END of new book template**********/
	}

	public function getbibiTitle(){
		global $Lang;
		$html = "<h4>{$Lang['IES']['newbibi']}</h4>";
		return $html;
	}

	public function getbibiLangChoicTable($ParLang){
		if($ParLang == 'chi'){
			$chi_check = 'CHECKED';
		}
		else if($ParLang == 'eng'){
			$en_check = 'CHECKED';
		}

		$html = <<<html
				<table class="innertable">
				<col class="field_title" />
				<col class="field_title" />
				<tr><td><input type="radio" name="bibi_lang" id='bi_ch' class='lang_bt' value='chi' $chi_check/><label for='bi_ch'>中文</label></td>
    			<td><input type="radio" name="bibi_lang" id='bi_en' class='lang_bt' value='eng' $en_check/><label for='bi_en'>English</label></td></tr>
				</table>
html;
		return $html;
	}

	public function getTypeListDisplay($ParType, $ParId, $ParCurrentType,$ParDisplayOldOption=1)//$ParDisplayOldOption : if it is 1, show the old option. If it is 0, not show
	{
		global $Lang;
		//-- Start: SELECTION BOX --//
		$list_array = $this->getTypelistArray($ParType);

		$valueNamePairArray = array();

		if(is_array($list_array)){
			foreach($list_array as $key)
			{
				$IsOldData = (substr($key, 0, 7) == 'oldData');//check whether the option starts with "oldData"
				if($ParDisplayOldOption==0 && $IsOldData==true)
				{
					continue;
				}
				$valueNamePairArray[] = array($key, $Lang['IES'][$key]['fieldname']);
			}
		}

		$htmlInSelectTag = " class='{$ParId}' onchange='ch_type(this)' ";
		$selectedValue = $ParCurrentType;
		$isShowSelectAllOption = false;
		$firstOptionName = "-- {$Lang['IES']['type']} --";
		$secondOptionName = "";
		//------------------------------------------
		// Only when $firstOptionName is not set
		// 0: -- $button_select --, 2: $i_general_NotSet, 3: $i_status_all
		$firstOptionDefaultValue = 0;
		//------------------------------------------
		$quotingStyle = 1; // 1: value='', 2: value=\"\"
		return getSelectByArray($valueNamePairArray,$htmlInSelectTag,$selectedValue,$isShowSelectAllOption,$firstOptionDefaultValue,$firstOptionName,$quotingStyle);

		//-- End: SELECTION BOX --//
	}


	public function updatebibi($ParContent, $ParType = '', $ParUserID, $ParbibiID = '', $Parscheme_id='', $ParTodisplay= ''){
		global $intranet_db;
//	  	$ParContent = htmlspecialchars($ParContent, ENT_NOQUOTES);

	  	if(empty($ParbibiID) && !empty($Parscheme_id)){
	  		if($ParTodisplay != ''){
				$sql = "INSERT INTO {$intranet_db}.IES_BIBLIOGRAPHY (UserID, Content, Type, DateInput, DateModified, SchemeID, Todisplay) VALUES ('{$ParUserID}', '{$ParContent}', '{$ParType}', NOW(), NOW(), '{$Parscheme_id}', '{$ParTodisplay}')";
	  		}
	  		else{
	  			$sql = "INSERT INTO {$intranet_db}.IES_BIBLIOGRAPHY (UserID, Content, Type, DateInput, DateModified, SchemeID) VALUES ('{$ParUserID}', '{$ParContent}', '{$ParType}', NOW(), NOW(), '{$Parscheme_id}')";
	  		}
	  	}
	  	else if(!empty($ParbibiID)){
			if($ParTodisplay != ''){
				$add_to .= ", Todisplay = '$ParTodisplay'";
			}
	  		$sql = "UPDATE {$intranet_db}.IES_BIBLIOGRAPHY SET Content = '{$ParContent}', DateModified=NOW() $add_to WHERE bibliographyID = '$ParbibiID' ";
	  	}

		$result = $this->db_db_query($sql);
		if($result){
			$result = mysql_insert_id();
		}
		return $result;
	}

	public function getContentString($ParArray){
		global $ies_cfg;

		$ParArray = array_map(array($this, cleanhtml), $ParArray);

        $content = addslashes(serialize($ParArray));
 //DEBUG_r($content);
		return $content;
	}
	/**
	 * To Clean all htmlentities
	 * Only run htmlentities when the input is string
	 *
	 */
	public function cleanhtml($dirtyhtml) {
		if(is_array($dirtyhtml)){
			return array_map(array($this, cleanhtml), $dirtyhtml);
		}
		if(is_string($dirtyhtml)){
			$dirtyhtml = htmlentities($dirtyhtml, ENT_QUOTES, "UTF-8");
       		return $dirtyhtml;
		}
       	else{
       		return $dirtyhtml;
       	}
  	}

	public function parseContentString($ParContent){
		global $ies_cfg, $Lang;

//		DEBUG($ParContent);
		$array = unserialize($ParContent);
//		DEBUG_R($array);
		if(is_array($array)){
			$array = array_map(array($this, my_stripslashes), $array);
		}
		else{
			$array = $this->my_stripslashes($array);
		}
//		DEBUG_R($array);
		return $array;
	}

	public function my_stripslashes($dirtyhtml){
		if(is_array($dirtyhtml)){
			return array_map(array($this, my_stripslashes), $dirtyhtml);
		}
		if(is_string($dirtyhtml)){
			return stripslashes($dirtyhtml);
		}
       	else{
       		return stripslashes($dirtyhtml);
       	}
	}

	public function getFillTableDisplay($Partype, $ParResultArray = ''){
		global $ies_cfg, $Lang;

		$template = $this->template[$Partype];
		$org_col = $this->tamplate['column'];
		$org_col_new = $this->tamplate['column_new'];
//DEBUG_R($ParResultArray);
		######### replace the ans #############  cccc
		if(is_array($ParResultArray)){
			#******* for each field
			#******* if array, that means it may hv multi row or more then 1 column

			$oldDataArray = array();
			$oldDataArray[]="chibook";
			$oldDataArray[]="chinew";
			$oldDataArray[]="chinet";
			$oldDataArray[]="manual";
			$oldDataArray[]="engbook";
			$oldDataArray[]="engnew";
			$oldDataArray[]="engnet";


			$NameArr = array();
			$NameArr[]="author";
			$NameArr[]="authorChi";
			$NameArr[]="authorEng";
			$NameArr[]="translator";
			$NameArr[]="editor";
			$NameArr[]="bookAuthor";
			$NameArr[]="chapterAuthor";
			$NameArr[]="chapterTitle";
			$NameArr[]="bookEditor";

			foreach($ParResultArray as $key=>$value){

				if(is_array($value)){

					//=====
					#*******************#
					#*More than one row*#
					#*******************#

					if(in_array($key,$NameArr)){

						$addcol = "";

						for($colnum = 0; $colnum < sizeof($value["surename"]); $colnum++){

							if(!empty($value["initial"])){

								if($colnum == 0){
									##replace surename
									$col_order1 = "REPLACE_{$ies_cfg['bibi'][$Partype][$key]}1";
									$col_replace1 = $value["surename"][$colnum];
									$template = str_replace($col_order1, $col_replace1, $template);

									##replace initial
									$col_order2 = "REPLACE_{$ies_cfg['bibi'][$Partype][$key]}2";
									$col_replace2 = $value["initial"][$colnum];
									$template = str_replace($col_order2, $col_replace2, $template);
								}
								else{
									$col_order = array("REPLACE_X", "REPLACE_CLASS", "REPLACE_NAME","REPLACE_VALUE");
									$col_replace1 = array("Surname", "", "info[{$key}][surename][]", $value["surename"][$colnum]);

									$initalStr = "Initial";
									if(in_array($Partype,$oldDataArray))
									{
										$spaceStr="&nbsp;";
									}
									else
									{
										$org_col = $org_col_new;
										$spaceStr='';
									}
									$col_replace2 = array($initalStr, "", "info[{$key}][initial][]", $value["initial"][$colnum]);
									$addcol .= "<br />" .str_replace($col_order,$col_replace1,$org_col).$spaceStr.str_replace($col_order,$col_replace2,$org_col)."(e.g. J.W.)";
								}
							}
							else{
								if($colnum == 0){
									$col_order = "REPLACE_{$ies_cfg['bibi'][$Partype][$key]}";
									$col_replace = $value["surename"][$colnum];
									$template = str_replace($col_order, $col_replace, $template);
								}
								else{

									$col_order = array("REPLACE_X", "REPLACE_CLASS", "REPLACE_NAME","REPLACE_VALUE");
									$col_replace = array("", "input_med", "info[{$key}][surename][]", $value["surename"][$colnum]);
									$addcol .= "<br />" .str_replace($col_order,$col_replace,$org_col);

								}
							}


						}
						$add_order = "<!--REPLACE_MORE_{$ies_cfg['bibi'][$Partype][$key]}-->";

						$add_replace = $addcol;

						$template = str_replace($add_order, $add_replace, $template);
					}

					else if($key == "organization"){ //add by connie 20110928

						$addcol = "";
						for($colnum = 0,$c_MAX=sizeof($value); $colnum < $c_MAX; $colnum++){
							if($colnum == 0){
									$col_order = "REPLACE_{$ies_cfg['bibi'][$Partype][$key]}";
									$col_replace = $value[$colnum];
									$template = str_replace($col_order, $col_replace, $template);
								}
								else{
									$col_order = array("REPLACE_X", "REPLACE_CLASS", "REPLACE_NAME","REPLACE_VALUE");
									$col_replace = array("", "input_med", "info[{$key}][]", $value[$colnum]);
									$addcol .= "<br />" .str_replace($col_order,$col_replace,$org_col);
								}
							}

						$add_order = "<!--REPLACE_MORE_{$ies_cfg['bibi'][$Partype][$key]}-->";
						$add_replace = $addcol;
						$template = str_replace($add_order, $add_replace, $template);
					}
					else if($key == "IsEditedBook")
					{
						//won't do anything
					}

					#*******************#
					#*More than one col*#
					#*******************#

					else{
						$a_rownum = (sizeof($ParResultArray[$key])> 1)?1:"";
						foreach($ParResultArray[$key] as $a_key=>$a_value){
							$order = "REPLACE_{$ies_cfg['bibi'][$Partype][$key]}{$a_rownum}";

							$replace = $a_value;
							$template = str_replace($order, $replace, $template);
							$a_rownum++;
						}

					}
				}

				#***************************************************************#
				#* It can replace the template simply. if it is not an array!!!*#
				#***************************************************************#
				else{
					#$key
					$order = "REPLACE_{$ies_cfg['bibi'][$Partype][$key]}";
					$replace = $value;
					$template = str_replace($order, $replace, $template);


				}


			}

		}
		##### New Template ############
		else if(is_array($ies_cfg['bibi'][$Partype])){
			foreach($ies_cfg['bibi'][$Partype] as $key=>$value){
					##replace it with ""
					$order = array("REPLACE_{$ies_cfg['bibi'][$Partype][$key]}1", "REPLACE_{$ies_cfg['bibi'][$Partype][$key]}2", "REPLACE_{$ies_cfg['bibi'][$Partype][$key]}3", "REPLACE_{$ies_cfg['bibi'][$Partype][$key]}");

					$replace = "";
					$template = str_replace($order, $replace, $template);
					//debug_R($template);
			}
		}
		return $template;
	}

	public function delete_bibi($ParbibiId){
		global $intranet_db;


		$sql = "DELETE FROM {$intranet_db}.IES_BIBLIOGRAPHY WHERE bibliographyID = '{$ParbibiId}'";
		$result = $this->db_db_query($sql);
		return $result;
	}

	public function addAnswer($Parscheme_id, $ParUserID, $parStageSeq, $parTaskCode){

		$TaskID = $this->getTaskIDByTaskInfo($Parscheme_id, $parStageSeq, $parTaskCode);
		//DEBUG_R($TaskID);
		$onlydisplay = true;
		$bibi_string = $this->getbibiDisplaybySchemeId($Parscheme_id, $ParUserID, $onlydisplay);
		$bibi_string = html_entity_decode($bibi_string, ENT_QUOTES, "UTF-8");
		$_result = $this->UPDATE_TASK_CONTENT($TaskID, $bibi_string, 'textarea', $ParUserID);

	}

	public function getbibiArrayById($ParbibiID = ''){
		global $intranet_db;

		if(!empty($ParbibiID)){
			##sql
			$sql = "SELECT * FROM IES_BIBLIOGRAPHY WHERE bibliographyID = '{$ParbibiID}'";
		}
		else{
			#do nothing
		}
		$result = current($this->ReturnArray($sql));
		return $result;
	}

	public function getAllbibiBySchemeId($Parscheme_id, $ParUserId, $type='', $cond='', $ParDisplayOnly = false){
		global $intranet_db;
		##sql
		if(empty($type)){
			$sql = "SELECT Content, Type, DateModified,bibliographyID, Todisplay FROM IES_BIBLIOGRAPHY WHERE SchemeID = '{$Parscheme_id}' AND UserID = '$ParUserId'";
		}
		else if($type == "chi"){
			$sql = "SELECT Content, Type, DateModified,bibliographyID, Todisplay FROM IES_BIBLIOGRAPHY WHERE SchemeID = '{$Parscheme_id}' AND UserID = '$ParUserId' AND (Type like '$type%' OR Type = 'manual')";
		}
		else if($type == "eng"){
			$sql = "SELECT Content, Type, DateModified,bibliographyID, Todisplay FROM IES_BIBLIOGRAPHY WHERE SchemeID = '{$Parscheme_id}' AND UserID = '$ParUserId' AND Type like '$type%'";
		}
		else if($type == "oldDataChi"){
			$sql = "SELECT Content, Type, DateModified,bibliographyID, Todisplay FROM IES_BIBLIOGRAPHY WHERE SchemeID = '{$Parscheme_id}' AND UserID = '$ParUserId' AND Type in ('chibook','chinew','chinet','manual')";
		}
		else if($type == "oldDataEng"){
			$sql = "SELECT Content, Type, DateModified,bibliographyID, Todisplay FROM IES_BIBLIOGRAPHY WHERE SchemeID = '{$Parscheme_id}' AND UserID = '$ParUserId' AND Type in ('engbook','engnew','engnet')";
		}
		else if(!empty($type)){
			$sql = "SELECT Content, Type, DateModified,bibliographyID, Todisplay FROM IES_BIBLIOGRAPHY WHERE SchemeID = '{$Parscheme_id}' AND UserID = '$ParUserId' AND Type = '$type'";
		}

		if(!empty($cond)){
			$sql .= " And Content like '%{$cond}%'";
		}
		if($ParDisplayOnly){
			$sql .= " And Todisplay = 1";
		}
	//	$sql .="ORDER BY Content COLLATE Chinese_PRC_Stroke_ci_as";

		$result = $this->ReturnArray($sql);
		return $result;
	}

	public function getPresentString($ParStringArray, $ParType){
		switch($ParType){

			/******* old choices **********/
			case "chibook":
				return $this->chibookPresentString($ParStringArray);
			break;
			case "chinew":
				return $this->chinewPresentString($ParStringArray);
			break;
			case "chinet":
				return $this->chinetPresentString($ParStringArray);
			break;
			case "manual":
				return $this->manualPresentString($ParStringArray);
			break;
			case "engbook":
				return $this->engbookPresentString($ParStringArray);
			break;
			case "engnew":
				return $this->engnewPresentString($ParStringArray);
			break;
			case "engnet":
				return $this->engnetPresentString($ParStringArray);
			break;
			/******* old choices END**********/



			/******* new choices **********/  //add by connie 20110928
			case "chibook_new":
				return $this->chibook_newPresentString($ParStringArray);
			break;
			case "chireport_new":
				return $this->chireport_newPresentString($ParStringArray);
			break;
			case "chijournals_new":
				return $this->chijournals_newPresentString($ParStringArray);
			break;
			case "chimagazine_new":
				return $this->chimagazine_newPresentString($ParStringArray);
			break;
			case "chinew_new":
				return $this->chinew_newPresentString($ParStringArray);
			break;
			case "chinet_new":
				return $this->chinet_newPresentString($ParStringArray);
			break;
			case "chiother_new":
				return $this->chiother_newPresentString($ParStringArray);
			break;


			case "engbook_new":
				return $this->engbook_newPresentString($ParStringArray);
			break;
			case "engreport_new":
				return $this->engreport_newPresentString($ParStringArray);
			break;
			case "engjournals_new":
				return $this->engjournals_newPresentString($ParStringArray);
			break;
			case "engmagazine_new":
				return $this->engmagazine_newPresentString($ParStringArray);
			break;
			case "engnew_new":
				return $this->engnew_newPresentString($ParStringArray);
			break;
			case "engnet_new":
				return $this->engnet_newPresentString($ParStringArray);
			break;
			case "engother_new":
				return $this->engother_newPresentString($ParStringArray);
			break;
			/******* new choices END**********/
		}

	}


	/**************************
	 *  old PresentString     *
	 **************************/

	public function chibookPresentString($ParStringArray){
		global $ies_cfg;
//		DEBUG_R($ParStringArray);
		##作者姓名 （年份）。〈文章名稱〉。編者姓名編 （年份）。《書名》（第   版）。譯者姓名譯。出版地點︰出版社。
		$p_string = "";
		$rowdil = "。";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";
			for($i = 0; $i < sizeof($ParStringArray['author']["surename"]); $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i];
					$comma = "、";
				}
			}
//			if(!empty($ParStringArray['year']) && !empty($tmp_string)){
			if(!empty($ParStringArray['year'])){ // display the year although author is empty
				$tmp_string .= "（{$ParStringArray['year']}）";
				$ParStringArray['year'] = "";
			}
			if(!empty($tmp_string)){
				$p_string .= $tmp_string .$rowdil;
			}
		}
		if(!empty($ParStringArray['articlename'])){
			$p_string .= "〈{$ParStringArray['articlename']}〉".$rowdil;
		}
		if(!empty($ParStringArray['editor'])){
			$comma = "";
			$tmp_string = "";
			for($i = 0; $i < sizeof($ParStringArray['editor']["surename"]); $i++){
				if(!empty($ParStringArray['editor']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['editor']["surename"][$i];
					$comma = "、";
				}
			}
			if(!empty($tmp_string)){
				if(sizeof($ParStringArray['editor']["surename"]) > 1){
					$tmp_string .= "合編";
				}
				else{
					$tmp_string .= "編";
				}
				if(!empty($ParStringArray['year'])){
					$tmp_string .= "（{$ParStringArray['year']}）";
					$ParStringArray['year'] = "";
				}
				$p_string .= $tmp_string .$rowdil;
			}
		}
		if(!empty($ParStringArray['bkname'])){
			$p_string .= "《{$ParStringArray['bkname']}》";

		}
		if(!empty($ParStringArray['edition'])){
				$p_string .= "（第{$ParStringArray['edition']}版）";
				$p_string .= $rowdil;
		}
		else if(!empty($ParStringArray['bkname'])){
			$p_string .= $rowdil;
		}
		if(!empty($ParStringArray['translator'])){
			$comma = "";
			$tmp_string = "";
			for($i = 0; $i < sizeof($ParStringArray['translator']["surename"]); $i++){
				if(!empty($ParStringArray['translator']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['translator']["surename"][$i];
					$comma = "、";
				}
			}
			if(!empty($tmp_string)){
				if(sizeof($ParStringArray['translator']["surename"]) > 1){
					$tmp_string .= "合譯";
				}
				else{
					$tmp_string .= "譯";
				}
				$p_string .= $tmp_string. $rowdil;
			}
		}
		if(!empty($ParStringArray['location'])){
			$p_string .= $ParStringArray['location'];
			if(!empty($ParStringArray['pub'])){
				$comma = "︰";
				$end = $rowdil;
			}
			else{
				$p_string .= $rowdil;
			}
		}
		if(!empty($ParStringArray['pub'])){
			$p_string .= $comma .$ParStringArray['pub'].$end;
		}
		return $p_string;
	}



	public function chinewPresentString($ParStringArray){
		global $ies_cfg;
//		DEBUG_R($ParStringArray);
		##作者姓名 （日期）。〈文章名稱〉。《刊物名稱》，期數，頁頁數。
		$p_string = "";
		$rowdil = "。";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";
			for($i = 0; $i < sizeof($ParStringArray['author']["surename"]); $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i];
					$comma = "、";
				}
			}
			$p_string =$tmp_string;
		}

		$tmp_string = "";

		if(!empty($ParStringArray['date']['year']) ){
			$tmp_string .= "（{$ParStringArray['date']['year']}年";
		}
		if(!empty($ParStringArray['date']['month']) ){
			$tmp_string .= "{$ParStringArray['date']['month']}月";
		}
		if(!empty($ParStringArray['date']['date']) ){
			$tmp_string .= "{$ParStringArray['date']['date']}日）";
		}
		else if(!empty($ParStringArray['date']['year']) ){
			$tmp_string .= ")";
		}

		if(!empty($tmp_string)){
			$p_string .= " ".$tmp_string .$rowdil;
		}

		if(!empty($ParStringArray['articlename'])){
			$p_string .= "〈{$ParStringArray['articlename']}〉".$rowdil;
		}
		if(!empty($ParStringArray['bkname'])){
			$p_string .= "《{$ParStringArray['bkname']}》";
			$comma = "，";
		}
		if(!empty($ParStringArray['fnum'])){
			$p_string .= $comma . "{$ParStringArray['fnum']}";
			$comma = "，";
		}
		if(!empty($ParStringArray['pnum'])){
			$p_string .= $comma."頁{$ParStringArray['pnum']}";
		}
		return $p_string.$rowdil;
	}

	public function chinetPresentString($ParStringArray){
		global $ies_cfg;
//		DEBUG_R($ParStringArray);
		##作者姓名 （年份）。〈文章名稱〉。《刊物名稱》。檢索日期，網址。
		$p_string = "";
		$rowdil = "。";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";
			for($i = 0; $i < sizeof($ParStringArray['author']["surename"]); $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i];
					$comma = "、";
				}
			}
			if(!empty($ParStringArray['year']) && !empty($tmp_string)){
				$tmp_string .= "（{$ParStringArray['year']}）";
				$ParStringArray['year'] = "";
			}
			if(!empty($tmp_string)){
				$p_string .= $tmp_string .$rowdil;
			}
		}
		if(!empty($ParStringArray['articlename'])){
			$p_string .= "〈{$ParStringArray['articlename']}〉".$rowdil;
		}
		if(!empty($ParStringArray['bkname'])){
			$p_string .= "《{$ParStringArray['bkname']}》".$rowdil;
		}
		$tmp_string = "";
		if(!empty($ParStringArray['date']['year'])){
			$tmp_string .= "檢索日期{$ParStringArray['date']['year']}年";
		}
		if(!empty($ParStringArray['date']['month'])){
			$tmp_string .= "{$ParStringArray['date']['month']}月";
		}
		if(!empty($ParStringArray['date']['date'])){
			$tmp_string .= "{$ParStringArray['date']['date']}日";
		}

		if(!empty($ParStringArray['website'])){
			if(!empty($tmp_string)){
				$tmp_string .= "，";
			}
			$tmp_string .= "網址".$ParStringArray['website'];
		}
		if(!empty($tmp_string)){
			$p_string .= $tmp_string . $rowdil;
		}

		return $p_string;
	}

	public function engbookPresentString($ParStringArray){
		global $ies_cfg;
//		DEBUG_R($ParStringArray);
		##Author, A. A. (Year).  Chapter/Article.  In E. E. Editor Surname (Ed.), Title of Work (ed.) (pp. XX-XX).  Location: Publisher.
		$p_string = "";
		$rowdil = ". ";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";
			for($i = 0; $i < sizeof($ParStringArray['author']["surename"]); $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i];
					## last one

					if((sizeof($ParStringArray['author']["surename"]) - $i) == 2){
						$comma = " & ";
					}
					else{
						$comma = " , ";
					}
				}
			}
			if(!empty($ParStringArray['year']) && !empty($tmp_string)){
				$tmp_string .= "({$ParStringArray['year']})";
				$ParStringArray['year'] = "";
			}
			if(!empty($tmp_string)){
				$p_string .= $tmp_string .$rowdil;
			}
		}
		if(!empty($ParStringArray['title'])){
			$p_string .= "{$ParStringArray['title']}".$rowdil;
		}
		##===== One field here
		$tmp_string = "";
		if(!empty($ParStringArray['title'])){
			$comma = "In ";


			for($i = 0; $i < sizeof($ParStringArray['editor']["surename"]); $i++){
				if(!empty($ParStringArray['editor']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['editor']["initial"][$i]." ".$ParStringArray['editor']["surename"][$i];
					## last one

					if((sizeof($ParStringArray['editor']["surename"]) - $i) == 2){
						$comma = " & ";
					}
					else{
						$comma = ",";
					}

				}
			}
			if(!empty($tmp_string)){
				if(sizeof($ParStringArray['editor']["surename"]) > 1){
					$tmp_string .= "(Eds)";
				}
				else{
					$tmp_string .= "(Ed)";
				}
			}


		}
		if(!empty($ParStringArray['articlename'])){
			if(!empty($tmp_string)){
				$tmp_string .= ",";
			}
			$tmp_string .= "{$ies_cfg["html_open"]}i{$ies_cfg["html_close"]}".$ParStringArray['articlename']."{$ies_cfg["html_open"]}/i{$ies_cfg["html_close"]}";

		}
		if(!empty($ParStringArray['edition'])){
			$tmp_string .=  "(".$ParStringArray['edition']." ed.)";
		}

		if(!empty($ParStringArray['pnum'])){
			$tmp_string .=  "(pp. ".$ParStringArray['pnum'].")";
		}


		if(!empty($tmp_string)){
			$p_string .= $tmp_string.$rowdil;
		}
		##=======

		if(!empty($ParStringArray['location'])){
			$p_string .= $ParStringArray['location'];
			if(!empty($ParStringArray['pub'])){
				$comma = ":";
				$end = $rowdil;
			}
			else{
				$p_string .= $rowdil;
			}
		}
		if(!empty($ParStringArray['pub'])){
			$p_string .= $comma .$ParStringArray['pub'].$end;
		}
		return $p_string;
	}

	public function engnewPresentString($ParStringArray){
		global $ies_cfg;
//		DEBUG_R($ParStringArray);
		##Author, A. A., Author, B. B., & Author, C. C. (Year).  Title of Article. Title of Journal, XX (YY), XX-XX
		$p_string = "";
		$rowdil = ". ";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";
			for($i = 0; $i < sizeof($ParStringArray['author']["surename"]); $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i]." ".$ParStringArray['author']["initial"][$i];
					## last one

					if((sizeof($ParStringArray['author']["surename"]) - $i) == 2){
						$comma = " & ";
					}
					else{
						$comma = ",";
					}
				}
			}
			if(!empty($ParStringArray['year']) && !empty($tmp_string)){
				$tmp_string .= "({$ParStringArray['year']})";
				$ParStringArray['year'] = "";
			}
			if(!empty($tmp_string)){
				$p_string .= $tmp_string .$rowdil;
			}
		}
		if(!empty($ParStringArray['articlename'])){
			$p_string .= $ParStringArray['articlename'].$rowdil;
		}
		##=======one field
		$tmp_string = "";
		if(!empty($ParStringArray['journal'])){
			$tmp_string .= "{$ies_cfg["html_open"]}i{$ies_cfg["html_close"]}".$ParStringArray['journal']."{$ies_cfg["html_open"]}/i{$ies_cfg["html_close"]}";
		}
		if(!empty($ParStringArray['vnum'])){
			if(!empty($tmp_string)){
				$tmp_string .= ", ";
			}
			$tmp_string .= $ParStringArray['vnum'];
		}
		if(!empty($ParStringArray['inum'])){

			$tmp_string .= "(".$ParStringArray['inum'].")";
		}
		if(!empty($ParStringArray['pnum'])){
			if(!empty($tmp_string)){
				$tmp_string .= ", ";
			}
			$tmp_string .= $ParStringArray['pnum'];
		}
		if(!empty($tmp_string)){
			$p_string .= $tmp_string.$rowdil;
		}
		##============
		return $p_string;
	}

	public function engnetPresentString($ParStringArray){

		global $ies_cfg;
//		DEBUG_R($ParStringArray);
		##Author, A. A. (Year). Title of Work. Retrieved Date from web address
		$p_string = "";
		$rowdil = ". ";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";
			for($i = 0; $i < sizeof($ParStringArray['author']["surename"]); $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i]." ".$ParStringArray['author']["initial"][$i];
					## last one

					if((sizeof($ParStringArray['author']["surename"]) - $i) == 2){
						$comma = " & ";
					}
					else{
						$comma = ",";
					}
				}
			}
//			if(!empty($ParStringArray['date']) && !empty($tmp_string)){
			if(!empty($ParStringArray['date'])){ // display the date although author is empty
				$tmp_string .= "({$ParStringArray['date']})";
				$ParStringArray['date'] = "";
			}
			if(!empty($tmp_string)){
				$p_string .= $tmp_string .$rowdil;
			}
		}
		if(!empty($ParStringArray['title'])){
			$p_string .= "{$ies_cfg["html_open"]}i{$ies_cfg["html_close"]}"."{$ParStringArray['title']}".$rowdil."{$ies_cfg["html_open"]}/i{$ies_cfg["html_close"]}";
		}
		##== one field
		$tmp_string = "";
		if(!empty($ParStringArray['bdate'])){
			$tmp_string .= "Retrieved ".$ParStringArray['bdate'];
		}
		if(!empty($ParStringArray['website'])){
			$tmp_string .= " from ".$ParStringArray['website'];
		}
		if(!empty($tmp_string)){
			$p_string .= $tmp_string.$rowdil;
		}
		##====
		return $p_string;

	}

	public function manualPresentString($ParStringArray){
		global $ies_cfg;
//		DEBUG_R($ParStringArray);

		$p_string = "";
		$rowdil = "。";
		return $ParStringArray['manual'];
	}

	/**************************
	 *  old PresentString END *
	 **************************/



	/**
	 * $Partype String- the type of book eg: chibook, engbook
	 *
	 * @return Array
	 */
	public function getTypelistArray($Partype){
		global $ies_cfg, $Lang;
		return $ies_cfg['bibi'][$Partype];
	}

	public function getbibiDisplaybySchemeId($Parscheme_id, $ParUserID, $onlydisplay=true){ //revised by connie 20110928
		$bibiArray = $this->getAllbibiBySchemeId($Parscheme_id, $ParUserID, '', '', $onlydisplay);

		if(is_array($bibiArray)){

			$bibiStringArr=array();
			foreach($bibiArray as $key=>$value){
				$stringArray = $this->parseContentString($value['Content']);
				$bibiStringArr[]  = $this->getPresentString($stringArray, $value['Type']). "\n";
			}
			$startWithEngArr=array();
			$startWithChiArr=array();

			foreach($bibiStringArr as $key=>$value)
			{
				if(preg_match("/^[A-Z]/",$value) || preg_match("/^[a-z]/",$value))
				{
				     $startWithEngArr[]=$value;
				}
				else
				{
				     $startWithChiArr[]=$value;
				}
			}
			natcasesort($startWithEngArr);
			natsort($startWithChiArr);


			$bibi_string="";
			foreach($startWithChiArr as $key=>$value){
				$bibi_string .= $value. "\n";
			}
			foreach($startWithEngArr as $key=>$value){
				$bibi_string .= $value. "\n";
			}


		//	usort( $bibiStringArr, array( $this, 'compare' ) );
			//usort($bibiStringArr,"compare");
//			foreach($bibiStringArr as $key=>$value){
//				$bibi_string .= $value. "\n";
//			}


		}
		return $bibi_string;
	}


	/******************************************
	 *  new PresentString by connie 20110928  *
	 ******************************************/
	 public function chibook_newPresentString($ParStringArray){
		global $ies_cfg;
//		DEBUG_R($ParStringArray);
		##作者姓名 （年份）:〈文章名稱〉，編者姓名編 （年份），《書名》（第   版）。譯者姓名譯。出版地點︰出版社。

		$p_string = "";
		$rowdil = "。";


		$thisIsEditedBook = $ParStringArray['IsEditedBook'];


		if(!empty($ParStringArray['authorChi']) && $thisIsEditedBook=="No"){
			$comma = "";
			$tmp_string = "";

			$ParStringArray['authorChi']["surename"] = $this->removeNullValueOfArr($ParStringArray['authorChi']["surename"]);

			for($i = 0,$i_MAX=sizeof($ParStringArray['authorChi']["surename"]); $i < $i_MAX; $i++){
				if(!empty($ParStringArray['authorChi']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['authorChi']["surename"][$i];

					if(($i_MAX-$i)==2 && $i_MAX>=3)
					{
						$comma="和";
					}
					else
					{
						$comma = "、";
					}
				}
			}
			$p_string .= $tmp_string;
		}


		if(!empty($ParStringArray['editor']) && $thisIsEditedBook=="Yes"){
			$comma = "";
			$tmp_string = "";

			$ParStringArray['editor']["surename"] = $this->removeNullValueOfArr($ParStringArray['editor']["surename"]);

			for($i = 0,$i_MAX=sizeof($ParStringArray['editor']["surename"]); $i <$i_MAX ; $i++){
				if(!empty($ParStringArray['editor']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['editor']["surename"][$i];
					$comma = "、";
					if($i==$i_MAX-2 && $i_MAX>=3)
					{
						$comma="和";
					}
				}
			}
			if(trim($tmp_string)!=''){
				if(sizeof($ParStringArray['editor']["surename"]) > 1){
					$tmp_string .= "合編";
				}
				else{
					$tmp_string .= "編";
				}
				$p_string .= $tmp_string;
			}
		}

		if(!empty($ParStringArray['authorEng'])){

			$comma = "";
			$tmp_stringAuthorEng = "";

			if(trim($p_string)!="")
			{
				$comma = "、";
			}

			$ParStringArray['authorEng']["surename"] = $this->removeNullValueOfArr($ParStringArray['authorEng']["surename"]);

			for($i = 0,$i_MAX=sizeof($ParStringArray['authorEng']["surename"]); $i < $i_MAX; $i++){
				if(!empty($ParStringArray['authorEng']["surename"][$i])){
					$tmp_stringAuthorEng .= $comma.$ParStringArray['authorEng']["surename"][$i].", ".$ParStringArray['authorEng']["initial"][$i];
					## last one

					if(($i_MAX - $i) == 2 && $i_MAX>=3){
						$comma = "和";
					}
					else{
						$comma = "、";
					}
				}
			}

			if(!empty($tmp_stringAuthorEng)){
				$tmp_stringAuthorEng .="";
			}
			$p_string .= $tmp_stringAuthorEng;


		}

		if(!empty($ParStringArray['translator'])){
			$comma = "";
			if($p_string!='')
			{
				$comma = "，";
			}
			$tmp_string = "";

			$ParStringArray['translator']["surename"] = $this->removeNullValueOfArr($ParStringArray['translator']["surename"]);

			for($i = 0,$i_MAX=sizeof($ParStringArray['translator']["surename"]); $i < $i_MAX; $i++){
				if(!empty($ParStringArray['translator']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['translator']["surename"][$i];
					$comma = "、";
					if($i==$i_MAX-2 && $i_MAX>=3)
					{
						$comma="和";
					}
				}
			}
			if(!empty($tmp_string)){
				if(sizeof($ParStringArray['translator']["surename"]) > 1){
					$tmp_string .= "合譯";
				}
				else{
					$tmp_string .= "譯";
				}
				$p_string .= $tmp_string;
			}
		}


		$bool_Editor=false;
		if($p_string=='')
		{
			$bool_Editor=true;
		}

		if($p_string!="") // display the year if author is not empty
		{
			$tmpStr='';

			if(!empty($ParStringArray['year']))
			{
				$tmpStr .= "（{$ParStringArray['year']}）";
			}
			$p_string .= $tmpStr .":";
		}

		if(!empty($ParStringArray['articlename'])){
			$p_string .= "〈{$ParStringArray['articlename']}〉"."，";
		}

		if(!empty($ParStringArray['bkname'])){

			$p_string .= "《{$ParStringArray['bkname']}》";

			if($bool_Editor)
			{
				if(!empty($ParStringArray['year']) && $bool_Editor)
				{
					$p_string .= "（{$ParStringArray['year']}）";
					$ParStringArray['year'] = "";
				}
			}
		}
		if(!empty($ParStringArray['edition'])){
			    $numberOfEdition =$ParStringArray['edition'];
				$p_string .= "（{$numberOfEdition}）";
		}


		if(!empty($ParStringArray['pnum']['from']))
		{
			if(!empty($ParStringArray['pnum']['to']))
			{
				if($ParStringArray['pnum']['from']!=$ParStringArray['pnum']['to'])
				{
					$tmp_string =  "頁".$ParStringArray['pnum']['from']."-".$ParStringArray['pnum']['to'];
				}
				else
				{
					$tmp_string =  "頁".$ParStringArray['pnum']['from'];
				}
			}
			else
			{
				$tmp_string =  "頁".$ParStringArray['pnum']['from'];
			}
			$p_string .= "，".$tmp_string;
		}
		else
		{
			if(!empty($ParStringArray['pnum']['to']))
			{
				$tmp_string =  "頁".$ParStringArray['pnum']['to'];
				$p_string .= "，".$tmp_string;
			}
		}

		if(!empty($ParStringArray['pnum']['from']) || !empty($ParStringArray['pnum']['from'])|| !empty($ParStringArray['bkname']) || !empty($ParStringArray['edition'])){
			$p_string .= $rowdil;
		}


		if(!empty($ParStringArray['location'])){
			$p_string .= $ParStringArray['location'];
			if(!empty($ParStringArray['pub'])){
				$comma = "︰";
				$end = $rowdil;
			}
			else{
				$p_string .= $rowdil;
			}
		}
		if(!empty($ParStringArray['pub'])){
			$p_string .= $comma .$ParStringArray['pub'].$end;
		}
		return $p_string;
	}


	public function chireport_newPresentString($ParStringArray){
		global $ies_cfg;
//DEBUG_R($ParStringArray);
		##作者姓名 （日期）:〈文章名稱〉。《刊物名稱》，期數，頁-頁數。
		$p_string = "";
		$rowdil = "。";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";

			$ParStringArray['author']["surename"] = $this->removeNullValueOfArr($ParStringArray['author']["surename"]);

			for($i = 0,$i_MAX=sizeof($ParStringArray['author']["surename"]); $i<$i_MAX; $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i];
					$comma = "、";
					if($i==$i_MAX-2 && $i_MAX>=3)
					{
						$comma="和";
					}
				}
			}
			$p_string =$tmp_string;

		}

		if(!empty($ParStringArray['organization']) && ($p_string=='')){
			$comma = "";
			$tmp_string = "";

			for($i = 0,$i_MAX=sizeof($ParStringArray['organization']); $i<$i_MAX; $i++){
				if(!empty($ParStringArray['organization'][$i])){
					$tmp_string .= $comma.$ParStringArray['organization'][$i];
					$comma = "、";
					if($i==$i_MAX-2 && $i_MAX>=3)
					{
						$comma="和";
					}
				}
			}
			$p_string =$tmp_string;

		}

		$tmp_string = "";
		if($p_string!='')
		{
			if(!empty($ParStringArray['year']) ){
				$tmp_string .= "（{$ParStringArray['year']}"; //aaaa
			}
			if(!empty($ParStringArray['month']) ){
				$tmp_string .= "年{$ParStringArray['month']}月";
			}
			if(!empty($ParStringArray['day']) ){
				$tmp_string .= "{$ParStringArray['day']}日";
			}
			if(!empty($ParStringArray['year']) ){
				$tmp_string .= "）";
			}
			$p_string .=$tmp_string;
		}


		if(!empty($tmp_string)){
			$p_string .= "︰";
		}

		if(!empty($ParStringArray['title'])){
			$p_string .= "《{$ParStringArray['title']}》";
			$comma = "，";
		}
		$p_string .=$rowdil;

		if($ParStringArray['organization'][0]!='' && $ParStringArray['author']["surename"][0]!=''){
			$comma = "";
			$tmp_string = "";

			for($i = 0,$i_MAX=sizeof($ParStringArray['organization']); $i<$i_MAX; $i++){
				if(!empty($ParStringArray['organization'][$i])){
					$tmp_string .= $comma.$ParStringArray['organization'][$i];
					$comma = "、";
					if($i==$i_MAX-2 && $i_MAX>=3)
					{
						$comma="和";
					}
				}
			}
			$p_string .=$tmp_string.$rowdil;
		}

		return $p_string;
	}

	public function chijournals_newPresentString($ParStringArray){
		global $ies_cfg;
//		DEBUG_R($ParStringArray);
		##黃政傑〈1998〉:建立優良的教科書審定制度，《課程與教學季刊》，2〈4〉，頁1-16。     2是卷數，4是期數
		$p_string = "";
		$rowdil = "。";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";

			$ParStringArray['author']["surename"] = $this->removeNullValueOfArr($ParStringArray['author']["surename"]);

			for($i = 0,$i_MAX=sizeof($ParStringArray['author']["surename"]); $i <$i_MAX ; $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i];
					$comma = "、";

					if($i==$i_MAX-2 && $i_MAX>=3)
					{
						$comma="和";
					}
				}
			}
			$p_string =$tmp_string;

		}

		$tmp_string = "";

		if(!empty($ParStringArray['year']) ){
			$tmp_string .= "（{$ParStringArray['year']}）";
		}

		if(!empty($tmp_string)){
			$p_string .= " ".$tmp_string ."︰ ";
		}

		if(!empty($ParStringArray['articlename'])){
			$p_string .= "{$ParStringArray['articlename']}"."，";
		}
		if(!empty($ParStringArray['bkname'])){
			$p_string .= "《{$ParStringArray['bkname']}》";
			$comma = "，";
		}
		if(!empty($ParStringArray['gnum'])){
			$p_string .= $comma . "{$ParStringArray['gnum']}";
			$comma = "，";
		}
		if(!empty($ParStringArray['fnum'])){
			$p_string .= "〈{$ParStringArray['fnum']}〉";
			$comma = "，";
		}

		if(!empty($ParStringArray['pnum']['from']))
		{
			if(!empty($ParStringArray['pnum']['to']))
			{
				if($ParStringArray['pnum']['from']!=$ParStringArray['pnum']['to'])
				{
					$tmp_string =  "頁".$ParStringArray['pnum']['from']."-".$ParStringArray['pnum']['to'];
				}
				else
				{
					$tmp_string =  "頁".$ParStringArray['pnum']['from'];
				}
			}
			else
			{
				$tmp_string =  "頁".$ParStringArray['pnum']['from'];
			}
			$p_string .= "，".$tmp_string;
		}
		else
		{
			if(!empty($ParStringArray['pnum']['to']))
			{
				$tmp_string =  "頁".$ParStringArray['pnum']['to'];
				$p_string .= "，".$tmp_string;
			}
		}

//		if(!empty($ParStringArray['pnum'])){
//			$p_string .= $comma.$tmp_string;
//		}
		return $p_string.$rowdil;
	}

	public function chimagazine_newPresentString($ParStringArray){
		global $ies_cfg;
//DEBUG_R($ParStringArray['author']);
		##作者姓名 （日期）:〈文章名稱〉。《刊物名稱》，期數，頁-頁數。
		##〈文章名稱〉（日期）:《刊物名稱》，期數，頁-頁數。
		$p_string = "";
		$rowdil = "。";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";

			$ParStringArray['author']["surename"] = $this->removeNullValueOfArr($ParStringArray['author']["surename"]);

			for($i = 0,$i_MAX=sizeof($ParStringArray['author']["surename"]); $i < $i_MAX; $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i];
					$comma = "、";

					if($i==$i_MAX-2 && $i_MAX>=3)
					{
						$comma="和";
					}
				}
			}
			$p_string =$tmp_string;


		}


		if($p_string!="") //如果有作者名,作者後加日期
		{
			$tmp_string = "";
			if(!empty($ParStringArray['year']) ){
				$tmp_string .= "（{$ParStringArray['year']}";
			}
			if(!empty($ParStringArray['month']) ){
				$tmp_string .= "年{$ParStringArray['month']}月";
			}
			if(!empty($ParStringArray['day']) ){
				$tmp_string .= "{$ParStringArray['day']}日";
			}
		    if(!empty($ParStringArray['year']) ){
				$tmp_string .= "）";
			}
			$p_string .= " ".$tmp_string .":";
		}

		if(!empty($ParStringArray['articlename']))
		{
			if($p_string=="") //如果沒有作者名,文章後加日期
			{
				$tmp_string = "";
				if(!empty($ParStringArray['year']) ){
				$tmp_string .= "（{$ParStringArray['year']}";
				}
				if(!empty($ParStringArray['month']) ){
					$tmp_string .= "年{$ParStringArray['month']}月";
				}
				if(!empty($ParStringArray['day']) ){
					$tmp_string .= "{$ParStringArray['day']}日";
				}
				if(!empty($ParStringArray['year']) ){
					$tmp_string .= "）";
				}
				$p_string .="〈{$ParStringArray['articlename']}〉".$tmp_string ."︰";
			}
			else
			{
				$p_string .= "〈{$ParStringArray['articlename']}〉".$rowdil;
			}
		}
		if(!empty($ParStringArray['bkname'])){
			$p_string .= "《{$ParStringArray['bkname']}》";
			$comma = "，";
		}
		if(!empty($ParStringArray['fnum'])){
			$p_string .= $comma . "{$ParStringArray['fnum']}";
			$comma = "，";
		}

		if(!empty($ParStringArray['pnum']['from']))
		{
			if(!empty($ParStringArray['pnum']['to']))
			{
				if($ParStringArray['pnum']['from']!=$ParStringArray['pnum']['to'])
				{
					$tmp_string =  "頁".$ParStringArray['pnum']['from']."-".$ParStringArray['pnum']['to'];
				}
				else
				{
					$tmp_string =  "頁".$ParStringArray['pnum']['from'];
				}
			}
			else
			{
				$tmp_string =  "頁".$ParStringArray['pnum']['from'];
			}
			$p_string .= "，".$tmp_string;
		}
		else
		{
			if(!empty($ParStringArray['pnum']['to']))
			{
				$tmp_string =  "頁".$ParStringArray['pnum']['to'];
				$p_string .= "，".$tmp_string;
			}
		}

		return $p_string.$rowdil;
	}


	public function chinew_newPresentString($ParStringArray){
		global $ies_cfg;
//DEBUG_R($ParStringArray);
		##作者姓名 （出版日期）:〈文章名稱〉。《報章名稱》，頁-頁數。
		$p_string = "";
		$rowdil = "。";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";

			$ParStringArray['author']["surename"] = $this->removeNullValueOfArr($ParStringArray['author']["surename"]);

			for($i = 0,$i_MAX=sizeof($ParStringArray['author']["surename"]); $i < $i_MAX; $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i];
					$comma = "、";

					if($i==$i_MAX-2 && $i_MAX>=3)
					{
						$comma="和";
					}
				}
			}
			$p_string =$tmp_string;

			$tmp_string='';
			if($p_string!="") //如果有作者名,作者後加日期
			{
				$tmp_string = "";
				if(!empty($ParStringArray['date']['year']) ){
					$tmp_string .= "（{$ParStringArray['date']['year']}";
				}
				if(!empty($ParStringArray['date']['month']) ){
					$tmp_string .= "年{$ParStringArray['date']['month']}月";
				}
				if(!empty($ParStringArray['date']['day']) ){
					$tmp_string .= "{$ParStringArray['date']['day']}日";
				}
				if(!empty($ParStringArray['date']['year']) ){
					$tmp_string .= "）";
				}
				$p_string .= " ".$tmp_string ."︰";
			}

		}


		if(!empty($ParStringArray['articlename']) )
		{
			$tmp_string='';
			if($p_string=="") //如果沒有作者名,文章後加日期
			{
				$tmp_string = "";
				if(!empty($ParStringArray['date']['year']) ){
				$tmp_string .= "（{$ParStringArray['date']['year']}";
				}
				if(!empty($ParStringArray['date']['month']) ){
					$tmp_string .= "年{$ParStringArray['date']['month']}月";
				}
				if(!empty($ParStringArray['date']['day']) ){
					$tmp_string .= "{$ParStringArray['date']['day']}日";
				}
				if(!empty($ParStringArray['date']['year']) ){
					$tmp_string .= "）";
				}
				$p_string .="〈{$ParStringArray['articlename']}〉".$tmp_string ."︰"." ";
			}
			else
			{
				$p_string .= "{$ParStringArray['articlename']}"."，";
			}

		}


		if(!empty($ParStringArray['bkname'])){
			$p_string .= "《{$ParStringArray['bkname']}》";
			$comma = "，";
		}
		if(!empty($ParStringArray['pnum']['from']))
		{
			if(!empty($ParStringArray['pnum']['to']))
			{
				if($ParStringArray['pnum']['from']!=$ParStringArray['pnum']['to'])
				{
					$tmp_string =  "頁".$ParStringArray['pnum']['from']."-".$ParStringArray['pnum']['to'];
				}
				else
				{
					$tmp_string =  "頁".$ParStringArray['pnum']['from'];
				}
			}
			else
			{
				$tmp_string =  "頁".$ParStringArray['pnum']['from'];
			}

			$p_string .= "，".$tmp_string;
		}
		else
		{
			if(!empty($ParStringArray['pnum']['to']))
			{
				$tmp_string =  "頁".$ParStringArray['pnum']['to'];
				$p_string .= "，".$tmp_string;
			}
		}

		return $p_string.$rowdil;
	}


	public function chinet_newPresentString($ParStringArray){
		global $ies_cfg;
//		DEBUG_R($ParStringArray);
		##作者姓名 （年份）:〈網頁名稱〉。檢索日期，網址
		$p_string = "";
		$rowdil = "。";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";

			$ParStringArray['author']["surename"] = $this->removeNullValueOfArr($ParStringArray['author']["surename"]);

			for($i = 0,$i_MAX=sizeof($ParStringArray['author']["surename"]); $i < $i_MAX; $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i];
					$comma = "、";

					if($i==$i_MAX-2 && $i_MAX>=3)
					{
						$comma="和";
					}
				}
			}
			if(!empty($ParStringArray['year']) && !empty($tmp_string)){
				$tmp_string .= "（{$ParStringArray['year']}）";
			}
			if(!empty($tmp_string)){
				$p_string .= $tmp_string ."︰";
			}
		}

		if(!empty($ParStringArray['organization']) && ($p_string=='')){
			$comma = "";
			$tmp_string = "";

			for($i = 0,$i_MAX=sizeof($ParStringArray['organization']); $i<$i_MAX; $i++){
				if(!empty($ParStringArray['organization'][$i])){
					$tmp_string .= $comma.$ParStringArray['organization'][$i];
					$comma = "、";
					if($i==$i_MAX-2 && $i_MAX>=3)
					{
						$comma="和";
					}
				}
			}
			if(!empty($ParStringArray['year']) && !empty($tmp_string)){
				$tmp_string .= "({$ParStringArray['year']})";
			}
			if(!empty($tmp_string)){
				$p_string .= $tmp_string."︰";
			}
		}


		if(!empty($ParStringArray['articlename'])){
			$p_string .= "〈{$ParStringArray['articlename']}〉".$rowdil;
		}

		if(!empty($ParStringArray['bkname'])){
			$p_string .= "《{$ParStringArray['bkname']}》".$rowdil;
		}
		$tmp_string = "";
		if(!empty($ParStringArray['date']['year'])){
			$tmp_string .= "瀏覽日期：{$ParStringArray['date']['year']}年";
		}
		if(!empty($ParStringArray['date']['month'])){
			$tmp_string .= "{$ParStringArray['date']['month']}月";
		}
		if(!empty($ParStringArray['date']['date'])){
			$tmp_string .= "{$ParStringArray['date']['date']}日";
		}

		if(!empty($ParStringArray['website'])){
			if(!empty($tmp_string)){
				$tmp_string .= "，";
			}
			$tmp_string .= $ParStringArray['website'];
		}
		if(!empty($tmp_string)){
			$p_string .= $tmp_string;
		}

		return $p_string;
	}

	public function chiother_newPresentString($ParStringArray){
		global $ies_cfg;
//		DEBUG_R($ParStringArray);

		$p_string = "";
		$rowdil = "。";
		return $ParStringArray['other'];
	}

	/************* English new bbbb***********************/

	public function engbook_newPresentString($ParStringArray){

		global $ies_cfg;
//DEBUG_R($ParStringArray);
		##Author, A. A. (Year).  Chapter/Article.  In E. E. Editor Surname (Ed.), Title of Work (ed.) (pp. XX-XX).  Location: Publisher.
		$p_string = "";
		$rowdil = ". ";
		$tmp_string = "";
		$thisIsEditedBook=$ParStringArray['IsEditedBook'];

		if(!empty($ParStringArray['bookAuthor']) && $thisIsEditedBook=="No"){
			$comma = "";
			$tmp_string = "";

			for($i = 0,$i_MAX=sizeof($ParStringArray['bookAuthor']["surename"]); $i < $i_MAX; $i++){
				if(!empty($ParStringArray['bookAuthor']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['bookAuthor']["surename"][$i].", ".$ParStringArray['bookAuthor']["initial"][$i];
					## last one

					if(($i_MAX- $i) == 2){
						$comma = " & ";
					}
					else{
						$comma = ", ";
					}
				}
			}
			if(!empty($ParStringArray['year']) && !empty($tmp_string)){
				$tmp_string .= " ({$ParStringArray['year']})";
				$ParStringArray['year'] = "";
			}
			if(!empty($tmp_string)){
				$tmp_string .= $rowdil." ";
			}
		}


		if(!empty($ParStringArray['chapterAuthor']) && $thisIsEditedBook=="Yes"){
			$comma = "";
			$tmp_string = "";

			for($i = 0,$i_MAX=sizeof($ParStringArray['chapterAuthor']["surename"]); $i < $i_MAX; $i++){
				if(!empty($ParStringArray['chapterAuthor']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['chapterAuthor']["surename"][$i].", ".$ParStringArray['chapterAuthor']["initial"][$i];
					## last one

					if(($i_MAX - $i) == 2){
						$comma = " & ";
					}
					else{
						$comma = ", ";
					}

				}
			}
			if(!empty($ParStringArray['year']) && !empty($tmp_string)){
				$tmp_string .= " ({$ParStringArray['year']})";
			}
		}

		if($thisIsEditedBook=="Yes")
		{
				if(!empty($ParStringArray['chapterTitle'])){

					if($tmp_string!=''){$tmp_string.=", ";}
					$tmp_string .= $ParStringArray['chapterTitle'].". ";
				}
				else
				{
					if($tmp_string !='')
					{
						$tmp_string .='. ';
					}
				}

		}



		##===== One field here

		if(!empty($ParStringArray['bookEditor']) && $thisIsEditedBook=='Yes'){	// bookEditor


			$comma = "In ";

			if($tmp_string=='')
			{
				$comma='';
				$OnlyEditor=true;
			}
			$tmp_str = '';

			for($i = 0,$i_MAX=sizeof($ParStringArray['bookEditor']["surename"]); $i < $i_MAX; $i++){
				if(!empty($ParStringArray['bookEditor']["surename"][$i])){
					$tmp_str .= $comma.$ParStringArray['bookEditor']["initial"][$i].", ".$ParStringArray['bookEditor']["surename"][$i];
					## last one

					if(($i_MAX - $i) == 2){
						$comma = " & ";
					}
					else{
						$comma = ", ";
					}
				}
			}
			$tmp_string .= $tmp_str;

			if($tmp_str!=''){
				if(sizeof($ParStringArray['bookEditor']["surename"]) > 1){
					$tmp_string .= " (Eds.). ";
				}
				else{
					$tmp_string .= " (Ed.). ";
				}
			}

			if(!empty($ParStringArray['year']) && $OnlyEditor==true){
				$tmp_string .= " ({$ParStringArray['year']}). ";
			}

		}
		if(!empty($ParStringArray['bookTitle'])){
			$tmp_string .= "{$ies_cfg["html_open"]}i{$ies_cfg["html_close"]}".$ParStringArray['bookTitle']."{$ies_cfg["html_open"]}/i{$ies_cfg["html_close"]}";
			$hasBookTitle=true;
		}


		if(!empty($ParStringArray['edition'])){
			$tmp_string .=  " "."(".$this->addOrdinalNumberSuffix($ParStringArray['edition'])." ed.)";
		}

		if(!empty($ParStringArray['pnum']['from']))
		{
			if(!empty($ParStringArray['pnum']['to']))
			{
				if($ParStringArray['pnum']['from']==$ParStringArray['pnum']['to'])
				{
					$tmp_string .=  " (pp. ".$ParStringArray['pnum']['from'].")";
				}
				else
				{
					$tmp_string .=  " (pp. ".$ParStringArray['pnum']['from']."-".$ParStringArray['pnum']['to'].")";
				}
			}
			else
			{
				$tmp_string .=  " (pp. ".$ParStringArray['pnum']['from'].")";
			}
		}
		else
		{
			if(!empty($ParStringArray['pnum']['to']))
			{
				$tmp_string .=  " (pp. ".$ParStringArray['pnum']['to'].")";
			}
		}

		if(!empty($tmp_string)|| $hasBookTitle){
			$p_string .= $tmp_string.". ";
		}
		##=======

		if(!empty($ParStringArray['location'])){
			$p_string .= $ParStringArray['location'];
			if(!empty($ParStringArray['pub'])){
				$comma = ": ";
				$end = $rowdil;
			}
			else{
				$p_string .= $rowdil;
			}
		}
		if(!empty($ParStringArray['pub'])){
			$p_string .= $comma .$ParStringArray['pub'].$end;
		}
		return $p_string;
	}


	public function engreport_newPresentString($ParStringArray){

		global $ies_cfg;
	//	DEBUG_R($ParStringArray);
		##Author, A. A. (Year).  Chapter/Article.  Title of Report (ed.) (pp. XX-XX).  Location: Publisher.
		$p_string = "";
		$rowdil = ". ";
		$hasAuthor=true;
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";

			for($i = 0,$i_MAX=sizeof($ParStringArray['author']["surename"]); $i < $i_MAX; $i++){
				if($ParStringArray['author']["surename"][$i]!=''){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i].", ".$ParStringArray['author']["initial"][$i];
					## last one

					if(($i_MAX - $i) == 2){
						$comma = " & ";
					}
					else{
						$comma = ", ";
					}

				}
				else
				{
					$hasAuthor=false;
					break;
				}
			}
			if(!empty($ParStringArray['year']) && !empty($tmp_string) && $hasAuthor){
				$tmp_string .= " ({$ParStringArray['year']})";
				$ParStringArray['year'] = "";
			}
			if(!empty($tmp_string) && $hasAuthor){
				$p_string .= $tmp_string .$rowdil." ";
			}
		}

		if(!empty($ParStringArray['organization']) && $hasAuthor==false){

			$comma='';
			$tmp_string = "";

			for($i = 0,$i_MAX=sizeof($ParStringArray['organization']); $i < $i_MAX; $i++){
				if(!empty($ParStringArray['organization'][$i])){
					$tmp_string .= $comma.$ParStringArray['organization'][$i];
					## last one

					if(($i_MAX - $i) == 2){
						$comma = " & ";
					}
					else{
						$comma = ", ";
					}

				}
			}

			if(!empty($ParStringArray['year']) && !empty($tmp_string)){
				$tmp_string .= " ({$ParStringArray['year']})";
				$ParStringArray['year'] = "";
			}
			if(!empty($tmp_string)){
				$p_string .= $tmp_string .$rowdil." ";
			}
		}

		if(!empty($ParStringArray['title'])){
			$p_string .= "{$ies_cfg["html_open"]}i{$ies_cfg["html_close"]}"."{$ParStringArray['title']}"."{$ies_cfg["html_open"]}/i{$ies_cfg["html_close"]}".$rowdil." ";
		}
		##===== One field here
		$tmp_string = "";

		if(!empty($ParStringArray['articlename'])){
			if(!empty($tmp_string)){
				$tmp_string .= ", ";
			}
			$tmp_string .= "{$ies_cfg["html_open"]}i{$ies_cfg["html_close"]}".$ParStringArray['articlename']."{$ies_cfg["html_open"]}/i{$ies_cfg["html_close"]}";

		}

		if(!empty($tmp_string)){
			$p_string .= $tmp_string.$rowdil." ";
		}
		##=======

		if(!empty($ParStringArray['location'])){
			$p_string .= $ParStringArray['location'];
			if(!empty($ParStringArray['pub'])){
				$comma = ": ";
				$end = $rowdil." ";
			}
			else{
				$p_string .= $rowdil." ";
			}
		}
		if(!empty($ParStringArray['pub'])){
			$p_string .= $comma .$ParStringArray['pub'].$end." ";
		}
		return $p_string;
	}

	public function engjournals_newPresentString($ParStringArray){ //eng jou
		global $ies_cfg;
//		DEBUG_R($ParStringArray);
		##Author, A. A., Author, B. B., & Author, C. C. (Year).  Title of Article. Title of Journal, XX (YY), XX-XX
		$p_string = "";
		$rowdil = ". ";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";
			for($i = 0, $i_MAX=sizeof($ParStringArray['author']["surename"]); $i <$i_MAX ; $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i].", ".$ParStringArray['author']["initial"][$i];
					## last one

					if(($i_MAX - $i) == 2){
						$comma = " & ";
					}
					else{
						$comma = ", ";
					}
				}
			}
			if(!empty($ParStringArray['year']) && !empty($tmp_string)){
				$tmp_string .= " ({$ParStringArray['year']})";
				$ParStringArray['year'] = "";
			}
			if(!empty($tmp_string)){
				$p_string .= $tmp_string .$rowdil." ";
			}
		}
		if(!empty($ParStringArray['articlename'])){
			$p_string .= $ParStringArray['articlename'].$rowdil." ";
		}
		##=======one field
		$tmp_string = "";
		if(!empty($ParStringArray['journal'])){
			$tmp_string .= "{$ies_cfg["html_open"]}i{$ies_cfg["html_close"]}".$ParStringArray['journal']."{$ies_cfg["html_open"]}/i{$ies_cfg["html_close"]}";
		}
		if(!empty($ParStringArray['vnum'])){
			if(!empty($tmp_string)){
				$tmp_string .= ", ";
			}
			$tmp_string .= $ParStringArray['vnum'];
		}
		if(!empty($ParStringArray['inum'])){

			$tmp_string .= " (".$ParStringArray['inum'].")";
		}

		if(!empty($ParStringArray['pnum']['from']))
		{
			if(!empty($tmp_string)){
				$tmp_string .= ", ";
			}
			if(!empty($ParStringArray['pnum']['to']))
			{
				if($ParStringArray['pnum']['from']==$ParStringArray['pnum']['to'])
				{
					$tmp_string .=  "pp. ".$ParStringArray['pnum']['from'];
				}
				else
				{
					$tmp_string .=  "pp. ".$ParStringArray['pnum']['from']."-".$ParStringArray['pnum']['to'];
				}
			}
			else
			{
				$tmp_string .=  "pp. ".$ParStringArray['pnum']['from'];
			}
		}
		else
		{
			if(!empty($ParStringArray['pnum']['to']))
			{
				if(!empty($tmp_string)){
					$tmp_string .= ", ";
				}
				$tmp_string .=  "pp. ".$ParStringArray['pnum']['to'];
			}
		}

		if(!empty($tmp_string)){
			$p_string .= $tmp_string.$rowdil." ";
		}
		##============
		return $p_string;
	}

	public function engmagazine_newPresentString($ParStringArray){ //eng mag
		global $ies_cfg;
//		DEBUG_R($ParStringArray);
		##Author, A. A., Author, B. B., & Author, C. C. (Year).  Title of Article. Title of Journal, XX (YY), XX-XX
		$p_string = "";
		$rowdil = ". ";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";
			for($i = 0,$i_MAX=sizeof($ParStringArray['author']["surename"]) ; $i <$i_MAX ; $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i].", ".$ParStringArray['author']["initial"][$i];
					## last one

					if(($i_MAX - $i) == 2){
						$comma = " & ";
					}
					else{
						$comma = ", ";
					}
				}
			}

			if($tmp_string!='')
			{
				if(!empty($ParStringArray['year'])){
				$tmp_string .= " ({$ParStringArray['year']}";
				}
				if(!empty($ParStringArray['month'])){
					$tmp_string .= ", "."{$ParStringArray['month']}";
				}
				if(!empty($ParStringArray['day'])){
					$tmp_string .= " "."{$ParStringArray['day']}";
				}
				if(!empty($ParStringArray['year']) )
				{
					$tmp_string .= ")";
				}
				$p_string .= $tmp_string .$rowdil." ";
			}

		}
		if(!empty($ParStringArray['articlename'])){

			$tmp_string='';
			if(trim($p_string)=="") //如果沒有作者名,文章後加日期
			{
				$tmp_string = "";
				if(!empty($ParStringArray['year'])){
				$tmp_string .= " ({$ParStringArray['year']}";
				}
				if(!empty($ParStringArray['month'])){
					$tmp_string .= ", "."{$ParStringArray['month']}";
				}
				if(!empty($ParStringArray['day'])){
					$tmp_string .= " "."{$ParStringArray['day']}";
				}
				if(!empty($ParStringArray['year']) )
				{
					$tmp_string .= ")";
				}
				$p_string .="〈{$ParStringArray['articlename']}〉".$tmp_string .":"." ";
			}
			else
			{
				$p_string .= "{$ParStringArray['articlename']}".". ";
			}

		}



		##=======one field
		$tmp_string = "";
		if(!empty($ParStringArray['magazine'])){
			$tmp_string .= "{$ies_cfg["html_open"]}i{$ies_cfg["html_close"]}".$ParStringArray['magazine']."{$ies_cfg["html_open"]}/i{$ies_cfg["html_close"]}";
		}
		if(!empty($ParStringArray['vnum'])){
			if(!empty($tmp_string)){
				$tmp_string .= ", ";
			}
			$tmp_string .= $ParStringArray['vnum'];
		}
		if(!empty($ParStringArray['inum'])){

			$tmp_string .= "(".$ParStringArray['inum'].")";
		}

		if(!empty($ParStringArray['pnum']['from']))
		{
			if(!empty($tmp_string)){
				$tmp_string .= ", ";
			}
			if(!empty($ParStringArray['pnum']['to']))
			{
				if($ParStringArray['pnum']['from']==$ParStringArray['pnum']['to'])
				{
					$tmp_string .=  "pp. ".$ParStringArray['pnum']['from'];
				}
				else
				{
					$tmp_string .=  "pp. ".$ParStringArray['pnum']['from']."-".$ParStringArray['pnum']['to'];
				}
			}
			else
			{
				$tmp_string .=  "pp. ".$ParStringArray['pnum']['from'];
			}
		}
		else
		{
			if(!empty($ParStringArray['pnum']['to']))
			{
				if(!empty($tmp_string)){
					$tmp_string .= ", ";
				}
				$tmp_string .=  "pp. ".$ParStringArray['pnum']['to'];
			}
		}

		if(!empty($tmp_string)){
			$p_string .= $tmp_string.$rowdil." ";
		}
		##============
		return $p_string;
	}

	public function engnew_newPresentString($ParStringArray){
		global $ies_cfg;
//		DEBUG_R($ParStringArray);
		##Author, A, A., Author, B, B., & Author, C, C. (Date).  Title of Article. Title of newspaper, XX-XX.
		$p_string = "";
		$rowdil = ". ";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";
			for($i = 0,$i_MAX=sizeof($ParStringArray['author']["surename"]); $i < $i_MAX; $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i].", ".$ParStringArray['author']["initial"][$i];
					## last one

					if(($i_MAX- $i) == 2){
						$comma = " & ";
					}
					else{
						$comma = ", ";
					}
				}
			}
			if(!empty($ParStringArray['date']) && !empty($tmp_string)){
				$tmp_string .= " ({$ParStringArray['date']})";
				$ParStringArray['date'] = "";
			}
			if(!empty($tmp_string)){
				$p_string .= $tmp_string .$rowdil." ";
			}
		}
		if(!empty($ParStringArray['articlename'])){

			$tmp_string = "";
			if($p_string == "")
			{
				if(!empty($ParStringArray['date'])){
					$tmp_string .= " ({$ParStringArray['date']})";
				}
			}

			$p_string .="〈{$ParStringArray['articlename']}〉".$tmp_string ."."." ";
		}
		##=======one field
		$tmp_string = "";
		if(!empty($ParStringArray['newspaper'])){
			$tmp_string .= "{$ies_cfg["html_open"]}i{$ies_cfg["html_close"]}".$ParStringArray['newspaper']."{$ies_cfg["html_open"]}/i{$ies_cfg["html_close"]}";
		}

		if(!empty($ParStringArray['pnum'])){
			if(!empty($tmp_string)){
				$tmp_string .= ". ";
			}
			$tmp_string .= $ParStringArray['pnum'];
		}

		if(!empty($tmp_string)){
			$p_string .= $tmp_string.$rowdil." ";
		}
		##============
		return $p_string;
	}

	public function engnet_newPresentString($ParStringArray){ //eng net

		global $ies_cfg;
//		DEBUG_R($ParStringArray);
		##Author, A. A. (Year). Title of Work. Retrieved Date from web address
		$p_string = "";
		$rowdil = ". ";
		if(!empty($ParStringArray['author'])){
			$comma = "";
			$tmp_string = "";
			for($i = 0,$i_MAX=sizeof($ParStringArray['author']["surename"]); $i < $i_MAX; $i++){
				if(!empty($ParStringArray['author']["surename"][$i])){
					$tmp_string .= $comma.$ParStringArray['author']["surename"][$i].", ".$ParStringArray['author']["initial"][$i];
					## last one

					if(($i_MAX - $i) == 2){
						$comma = " & ";
					}
					else{
						$comma = ", ";
					}
				}
			}

			if($tmp_string!='')
			{
				if(!empty($ParStringArray['year'])){
				$tmp_string .= " ({$ParStringArray['year']}";
				}
				if(!empty($ParStringArray['month'])){
					$tmp_string .= ", "."{$ParStringArray['month']}";
				}
				if(!empty($ParStringArray['day'])){
					$tmp_string .= " "."{$ParStringArray['day']}";
				}
				if(!empty($ParStringArray['year']))
				{
					$tmp_string .= ")";
				}
				$p_string .= $tmp_string.$rowdil." ";
			}
		}

		if(!empty($ParStringArray['organization']) && $p_string==''){

			$comma='';
			$tmp_string = "";

			for($i = 0,$i_MAX=sizeof($ParStringArray['organization']); $i < $i_MAX; $i++){
				if(!empty($ParStringArray['organization'][$i])){
					$tmp_string .= $comma.$ParStringArray['organization'][$i];
					## last one

					if(($i_MAX - $i) == 2){
						$comma = " & ";
					}
					else{
						$comma = ", ";
					}

				}
			}
			if($tmp_string!='')
			{
				if(!empty($ParStringArray['year'])){
					$tmp_string .= " ({$ParStringArray['year']}";
				}
				if(!empty($ParStringArray['month'])){
					$tmp_string .= ", "."{$ParStringArray['month']}";
				}
				if(!empty($ParStringArray['day'])){
					$tmp_string .= " "."{$ParStringArray['day']}";
				}
				if(!empty($ParStringArray['year']))
				{
					$tmp_string .= ")";
				}
				$p_string .= $tmp_string.$rowdil." ";

			}
		}


		if(!empty($ParStringArray['title'])){
			$p_string .= "{$ies_cfg["html_open"]}i{$ies_cfg["html_close"]}"."{$ParStringArray['title']}".$rowdil."{$ies_cfg["html_open"]}/i{$ies_cfg["html_close"]}";
		}
		##== one field
		$tmp_string = "";
		if(!empty($ParStringArray['bdate'])){
			$tmp_string .= "Retrieved ".$ParStringArray['bdate'];
		}
		if(!empty($ParStringArray['website'])){
			$tmp_string .= ", from ".$ParStringArray['website'];
		}
		if(!empty($tmp_string)){
			$p_string .= $tmp_string;
		}
		##====
		return $p_string;

	}

	public function engother_newPresentString($ParStringArray){
		global $ies_cfg;
//		DEBUG_R($ParStringArray);

		$p_string = "";
		$rowdil = ".";
		return $ParStringArray['other'];
	}

	/**************************
	 *  new PresentString END *
	 **************************/




	/** it is used for change 1 to 1st, 4 to 4th
        * @owner : Connie (20110930)
        * @param : $ParNum (1,2,3,4.......)
        * @return : (1st, 2nd, 3rd, 4th.....)
        */
	private function addOrdinalNumberSuffix($ParNum) {

		$ParNum = (int)$ParNum;

	    if (!in_array(($ParNum % 100),array(11,12,13))){
	      switch ($ParNum % 10) {
	        // Handle 1st, 2nd, 3rd
	        case 1:  return $ParNum.'st';
	        case 2:  return $ParNum.'nd';
	        case 3:  return $ParNum.'rd';
	      }
	    }
	    return $ParNum.'th';
  	}



	/** it is used for removing the null values in Array
        * @owner : Connie (20110927)
        * @param : Array $ParArr ('',1,'',2,'','','3','')
        * @return : Array $returnArr (1,2,3),
        */

	private function removeNullValueOfArr($ParArr)
	{
		if(is_array($ParArr)){
			foreach ($ParArr as $key => $value)
			{
			  if (is_null($value) || $value=="")
			  {
				unset($ParArr[$key]);
			  }
			}
			$returnArr = array_values($ParArr);
				    return $returnArr;
		}else{
			return $ParArr;
		}
	}


}
?>