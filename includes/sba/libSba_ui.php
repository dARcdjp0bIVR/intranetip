<?php
/*******************************************
*	modification log
*		2015-06-29 Siuwan
*			- modified function getStandardRubricSet(), add order by field
*		2015-06-24 Siuwan
*			- modified function GetSbaMarkCriteriaTable(), GetMarkCriteriaTable() to display chinese or english title determined by $sba_thisSchemeLang
*/

class libSbaUI{
	function libSbaUI(){
		
	}
	
	function getTaskUI($TaskObj, $TaskHandinObj, $IsEditable=false){
		global $sba_thisStudentID, $Lang;
		
		$id_suffix = $TaskObj->getTaskID()? $TaskObj->getTaskID():"new";
		
		$studentNextTaskHandlerObj = libSbaStudentNextTaskHandler::getInstance($sba_thisStudentID);
		
		$returnStr  = "<div id='sba_task_$id_suffix' class='sba_task IES_task'>";
		
		if($IsEditable || $studentNextTaskHandlerObj->isTaskOpen($TaskObj)){ // If task is enabled or User have edit right (i.e. Teacher)
			
			###################################
			##### sba task header - Start #####
			###################################
			
			$returnStr .= "<div id='sba_task_header_$id_suffix'>";
			
			if($TaskObj->getTaskID()){ // If task is already created
				$returnStr .= $this->getTaskHeaderTitleUI($TaskObj, false);
				
				if($IsEditable){ // If task can be edited
					$returnStr .= "<div id='sba_task_header_editBtn_$id_suffix' class='stage_left_tool'>
                    				<a title='".$Lang['SBA']['EditTask']."' class='edit' href='javascript:void(0)' onclick='get_task_edit_form(".$TaskObj->getTaskID().")'><span>".$Lang['SBA']['EditTask']."</span></a>
                				   </div>";
				}
				
				$returnStr .= "<div id='sba_task_msgBox_$id_suffix' class='pretend_stu_tool' style='margin-right: 40px; margin-top: 15px; visibility:hidden'>
									<div class='below' style='margin-top:0px'>
										<span></span>
									</div>
							   </div>";
			}
			else{ // If task is not yet created
				$returnStr .= $this->getTaskHeaderTitleUI($TaskObj, true);
			}
			$returnStr .= "</div>
						   <div class='clear'></div>";
			
			####################################
			##### sba task header - Finish #####
			####################################
			
			
			####################################
			##### sba task content - Start #####
			####################################
			
			$returnStr .= "<div id='sba_task_content_$id_suffix'>";
			if($TaskObj->getTaskID()){ // If task is already created
				$returnStr .= $this->getTaskIntroStepsUI($TaskObj, $IsEditable);
				$returnStr .= $this->getTaskStdAnswerUI($TaskObj, $TaskHandinObj, $IsEditable);
			}
			else{ // If task is not yet created
				$returnStr .= "&nbsp;";
			}
			$returnStr .= "</div>
						   <div class=\"clear\"></div>";
			
			#####################################
			##### sba task content - Finish #####
			#####################################
		}
		else{ // If task is not enabled and User does not have edit right (i.e. Student)
			$returnStr .= "	<a class='task_title task_close'><span>".$TaskObj->getTitle()."</span></a>
                			<span class='content_grey_text'>(".($TaskObj->getEnable()?$Lang['SBA']['WaitingForTeacherApproval']:$Lang['IES']['NotReleased']).")</span>
                			<div class='clear'></div>";
		}
		
		$returnStr .= "</div>";
		
		return $returnStr;
	}
	
	function getTaskHeaderTitleUI($TaskObj=null, $IsClickable=false){
		global $Lang;
		if(!is_null($TaskObj) && $TaskObj->getTaskID()){
			$id_suffix  = $TaskObj->getTaskID();
			$task_id	= $TaskObj->getTaskID();
			$task_title = $TaskObj->getTitle();
		}
		else{
			$id_suffix  = "new";
			$task_id	= 0;
			$task_title = $Lang['SBA']['AddTask'];
		}
		
		if($IsClickable){
			$returnStr = "<a id='sba_task_header_title_$id_suffix' class='task_title task_add' href='javascript:void(0)' onclick='get_task_edit_form($task_id)'><span>$task_title</span></a>";
		}
		else{
			$returnStr = "<div id='sba_task_header_title_$id_suffix' class='task_title task_open'><span>$task_title</span></div>";
		}
		
		return $returnStr;
	}
	
	function getTaskIntroStepsUI($TaskObj, $IsEditable=false){
		global $sba_cfg, $Lang;
		
		$id_suffix = $TaskObj->getTaskID()? $TaskObj->getTaskID():"new";
		
		$returnStr .= "<div id='sba_task_content_introStep_$id_suffix' class='task_box'>
                    		<div id='sba_task_content_introStep_tabMenu_$id_suffix' class='task_menu'>";
        $tab_btn =  "<div class='task_menu_tab'>";
        	$tab_btn .=  "<a id='sba_task_content_introStep_tabMenu_introBtn_$id_suffix' class='current' href='javascript:void(0)' title = \"".$Lang['SBA']['AddStep']."\" onclick='task_content_introStep_tabMenu_onclick(".$TaskObj->getTaskID().", 1, 0, 0)'><span><img src='/images/2009a/SBAIES/task_tab_iconbreif.gif'>".$Lang['IES']['Introduction']."</span></a>";
		
		if($TaskObj->getInstantEdit()==$sba_cfg['DB_IES_TASK_InstantEdit']['StepEdit'] || $TaskObj->getInstantEdit()==$sba_cfg['DB_IES_TASK_InstantEdit']['InstantStepEdit']){
			$steps 	   = $TaskObj->getObjStepsAry();
			$noOfSteps = count($steps);
			
			for($i=0;$i<$noOfSteps;$i++){
				$tab_btn .= $this->getTaskIntroStepTabMenuStepBtnUI($steps[$i], false);
			}
			
			if($IsEditable){
				$tab_btn .= "	<a ".($noOfSteps? "class='add'":"")." id='sba_task_content_introStep_tabMenu_addStepBtn_$id_suffix' href='javascript:void(0)' onclick='task_content_introStep_tabMenu_onclick(".$TaskObj->getTaskID().", 0, 1, 0)'><span><img src='/images/2009a/SBAIES/task_tab_iconadd.gif' title = \"".$Lang['SBA']['AddStep']."\"><font>".($noOfSteps? "&nbsp;":$Lang['SBA']['AddStep'])."</font></span></a>";
			}
		}
		$tab_btn .=  "</div>";
		$returnStr .= "			<div class='task_menu_tool'>";
		
		if($IsEditable && ($TaskObj->getInstantEdit()==$sba_cfg['DB_IES_TASK_InstantEdit']['StepEdit'] || $TaskObj->getInstantEdit()==$sba_cfg['DB_IES_TASK_InstantEdit']['InstantStepEdit'])){
			$returnStr .= "			<a id='sba_step_reorderBtn_".$TaskObj->getTaskID()."' title='".$Lang['SBA']['ReOrderStep'] ."' class='move' href='javascript:void(0)' onclick='reorderStep(".$TaskObj->getTaskID().")' ".($noOfSteps<2? "style='display:none'":"")."><span>".$Lang['SBA']['ReOrderStep'] ."</span></a>";
		}
		
		$returnStr .= "				<a id='sba_task_content_introStep_tabMenu_collapseBtn_$id_suffix' title='".$Lang['SBA']['Collapse']."' class='collapse' href='javascript:void(0)' onclick='task_content_introStep_content_showHide(".$TaskObj->getTaskID().")'><span>".$Lang['SBA']['Collapse']."</span></a>
                        		</div>
								".$tab_btn."
							</div>                
                    		<div class='left'>
								<div class='right'>
									<div id='sba_task_content_introStep_contentBox_$id_suffix'>
										<div id='sba_task_content_introStep_content_$id_suffix'>
											".$this->getTaskIntroContentUI($TaskObj, $IsEditable, false)."
										</div>
										<div id='sba_task_content_introStep_editIntro_$id_suffix' style='display:none'></div>
										<div id='sba_task_content_introStep_editStep_$id_suffix' style='display:none'></div>
									</div>
									<div id='sba_task_content_introStep_content_collapse_$id_suffix' class='task_box_collapse' style='display:none'>&nbsp;</div>
								</div>
							</div>
                    		<div class='bottom'><div><div></div></div></div>  
							<div class='clear'></div>
                	   </div>";
		
		return $returnStr;
	}
	
	function getTaskIntroStepTabMenuStepBtnUI($StepObj, $IsCurrent=false){
		$returnStr = "<a id='sba_task_content_introStep_tabMenu_stepBtn_".$StepObj->getStepID()."' title = \"".$Lang['SBA']['AddStep']."\" class='sba_step_".$StepObj->getTaskID()." ".($IsCurrent? "current":"")."' href='javascript:void(0)' onclick='task_content_introStep_tabMenu_onclick(".$StepObj->getTaskID().", 0, 0, ".$StepObj->getStepID().")'><span>".$StepObj->getTitle()."</span></a>";
		
		return $returnStr;
	}
	
	function getTaskStdAnswerUI($TaskObj, $TaskHandinObj, $IsEditable=false){
		global $Lang;
		$id_suffix = $TaskObj->getTaskID()? $TaskObj->getTaskID():"new";
		
		$returnStr .= "<div id='sba_task_content_stdAnswer_$id_suffix' class='IES_ans_box'> 
                			<div class='writing_memo'>
                				<div class='icon_stu_edit'>".$Lang['SBA']['StudentHandin']."</div>
                    			<div class='clip'></div>
                    			<div class='top'><div><div></div></div></div>
                    			<div class='left'>
									<div class='right'>
										<div id='sba_task_content_stdAnswer_content_$id_suffix' class='mid'>
											".$this->getTaskStdAnswerContentUI($TaskObj, $TaskHandinObj, $IsEditable)."
										</div>
										<div id='sba_task_content_stdAnswer_edit_$id_suffix' class='mid' style='display:none'></div>
									</div>
								</div>
                    			<div class='bottom'><div><div></div></div></div>
                   				<div class='clear'></div>
							</div>
							<div id='sba_task_content_stdAnswer_info_$id_suffix'>
								".(!$IsEditable && $TaskHandinObj->getAnswerID()? $this->getTaskStdAnswerContentInfoUI($TaskObj, $TaskHandinObj):"")."
							</div>
                			<div class='clear'></div>
							<div id='sba_task_content_stdAnswer_commentBlock_".$TaskObj->getTaskID()."' class='IES_commentbox' style='display:none'></div>
                	   </div>";
		
		return $returnStr;
	}
	
	function getTaskStdAnswerContentUI($TaskObj, $TaskHandinObj, $IsEditable=false, $IsEditing=false){
		global $sba_cfg, $Lang;
		
		if(!$IsEditing){
			$returnStr .= "<span class='mid_text'>".($IsEditable? "&nbsp;":($TaskHandinObj->getAnswerID()? $TaskHandinObj->getDisplayAnswer():"&nbsp;"))."</span>";
		}
		
		if(!$IsEditable){
			if($IsEditing){
				$disable_btn = $TaskHandinObj->getQuestionType()==$sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea']?"disabled='disabled'":"";
				
				$returnStr .= "<form id='sba_task_stdAnswer_handinform_".$TaskObj->getTaskID()."' method='POST'>
									".$TaskHandinObj->getDisplayAnswerInput()."
							  		<div class='task_writing_bottom'>
                						<input type='button' value='".$Lang['SBA']['Btn_SaveAndSubmit']."' onclick='task_stdAnswer_handin_form_submit(".$TaskObj->getTaskID().")' onmouseout='this.className=\"formbutton\"' onmouseover='this.className=\"formbuttonon\"' class='formbutton' name='submitBtn' $disable_btn>
										<input type='button' value='".$Lang['IES']['Cancel2']."' onclick='task_stdAnswer_handin_form_cancel(".$TaskObj->getTaskID().")' onmouseout='this.className=\"formsubbutton\"' onmouseover='this.className=\"formsubbuttonon\"' class='formsubbutton' name='cancelBtn' $disable_btn>
               				  		</div>
									<input type='hidden' name='r_AnswerID' value='".$TaskHandinObj->getAnswerID()."'>
									<input type='hidden' name='r_QuestionType' value='".$TaskHandinObj->getQuestionType()."'>
							   </form>";
			}
			else{
				if($TaskHandinObj->getAnswerID()){
					$returnStr .= "<div class='writing_memo_btn'>
		                        		<a class='task_edit' href='javascript:void(0)' onclick='get_task_stdAnswer_handin_form(".$TaskObj->getTaskID().")'><span>".$Lang['SBA']['Edit']."</span></a>
		                           </div>";
				}
				else{
					$returnStr .= "<div class='task_intro_buttons'>
		                        		<a class='instant_write' href='javascript:void(0)' onclick='get_task_stdAnswer_handin_form(".$TaskObj->getTaskID().")'><span>".$Lang['SBA']['EditNow']."</span></a>
		                           </div>";
				}
			}
		}
		
		return $returnStr;
	}
	
	function getTaskStdAnswerContentInfoUI($TaskObj, $TaskHandinObj){
		global $Lang;
		
		$objIES = new libies();
		
		# check if there are any comments from teachers 
		$is_teacher_comment 	  = $objIES->getTeacherCommentLatestTime($TaskObj->getTaskID(), $TaskHandinObj->getUserID());
		$is_teacher_score_on_task = $objIES->getTeacherTaskScoreLatestTime($TaskObj->getTaskID(), $TaskHandinObj->getUserID());
		
		if(!empty($is_teacher_comment) || !empty($is_teacher_score_on_task)){
			$teach_comment_date  = '';
			if(empty($is_teacher_comment["DateInput"])) {
				//do nothing
			}
			else {
				$teach_comment_date   = '('.$is_teacher_comment["DateInput"].')';
			}
			$returnStr .= "	<div class=\"teacher_comment\"><a href=\"javascript:doGetComment(".$TaskObj->getTaskID().")\">".$Lang['IES']['TeachersFeedback']."</a>&nbsp;<span style=\"font-size:9px; color:grey\">".$teach_comment_date."</span></div><!-- if have new comment, <a> add class \"new\"-->";
		}
		else {
			$returnStr .= "	<div class=\"teacher_comment\" style=\"display:none;\"><a href=\"javascript:doGetComment(".$TaskObj->getTaskID().")\">".$Lang['IES']['TeachersFeedback']."</a></div><!-- if have new comment, <a> add class \"new\"-->";
		}
		
		$returnStr .= "<div class='edit_date'>".$Lang['IES']['DateSubmitted']." : ".$TaskHandinObj->getDateInput()." | ".$Lang['IES']['LastModifiedDate']." : ".$TaskHandinObj->getDateModified()."</div>";
		
		return $returnStr;
	}
	
	function getTaskAttachmentTableUI($attachment_ids, $is_edit, $task_id, $step_id = ''){
		global $ies_cfg, $sba_cfg, $Lang;
		$libSba = new libSba();
		
		
		$table_ui .= $is_edit? "<script>init_uploader('sba_attach_".$task_id."_".$step_id."' ,{task_id: \"$task_id\", step_id: \"$step_id\"});</script>":"";
		$table_ui .= $is_edit?"<input type='button' id='sba_attach_".$task_id."_".$step_id."' href='#' value='".$Lang['IES']['UploadFiles']."' >":"";
		$table_ui .= "<div class='attachment_".$task_id."_".$step_id."'>";
				
		if ($attachment_ids){
		    $attachments = $libSba->getAttachmentDetails($attachment_ids);
		    
		    foreach (array_reverse($attachments) as $attachment){
			$table_ui .= "<div class='attach_file file_".$attachment['FileID']."'>";
			$table_ui .= "<a class='file_name' href='./?mod=ajax&task=ajax_fileupload&ajaxAction=getFile&file_id=".$attachment['FileID']."'>".$attachment['FileName']."</a>";
			$table_ui .= $is_edit? "<a class='delete_row' href='javascript:void(0)' onclick='remove_attachfile(\"".$attachment['FileID']."\", \"".$task_id."\", \"".$step_id."\")'></a>": "";
			$table_ui .= "<input type='hidden' name='r_Attachment[]' value='".$attachment['FileID']."'/>";
			$table_ui .= "</div>";
			
		    
		    }
		    		    
		}
		

		$table_ui .= "</div>";
		

		
		return $table_ui;
	}
	
	
	function getTaskIntroContentUI($TaskObj, $IsEditable=false, $IsEditing=false){
		global $sba_cfg, $Lang;
		$returnStr .= "<div class='task_box_content'>";
		
		$task_id    = $TaskObj->getTaskID();	
		$attachment = $TaskObj->getAttachments();
		
		if($IsEditable){
			if($IsEditing){
				# Get HTML editor for Introduction
				/*
				$oFCKeditor = new FCKeditor('r_task_Introduction_'.$task_id, '100%', 200, '', 'Basic2_SBA');
				$oFCKeditor->Value = $TaskObj->getIntroduction();
				
				$editorHTML = $oFCKeditor->Create2()."<textarea name='r_task_Introduction' style='display:none'></textarea>";
				*/
				$editorHTML = getCkEditor('r_task_Introduction_'.$task_id, $TaskObj->getIntroduction(), "sba", 200, '100%');
				$editorHTML .= "<textarea name='r_task_Introduction' style='display:none'></textarea>";
				$editorHTML .= "<script>
						CKEDITOR.on('instanceReady',
						   function( evt ) {
						   		var oEditor = evt.editor;
						    	evt.editor.Name = evt.editor.name;
						    	FCKeditor_OnComplete(oEditor);
						   }
						);
						</script>";
				
				$returnStr .= "<iframe style='display:none' id='sba_task_iframe_".$TaskObj->getTaskID()."' name='sba_task_iframe_$task_id'></iframe>";
				$returnStr .= "<form id='sba_task_intro_editform_".$TaskObj->getTaskID()."' method='POST'>
							   		".$editorHTML;
				
				
				$returnStr  .= "<div class='attachments attachments_edit'>".$Lang['IES']['Attachment'].": ";		
				$returnStr .= $this->getTaskAttachmentTableUI($attachment, $IsEditing, $task_id);
				$returnStr .= "</div>";
									
				$returnStr .= "<div style='background:none; border:none; margin-top:30px;' class='task_writing_bottom'>
                						<input type='button' value='".$Lang['IES']['Save2']."' onclick='task_attachment_upload($task_id);task_intro_edit_form_submit($task_id);' onmouseout='this.className=\"formbutton\"' onmouseover='this.className=\"formbuttonon\"' class='formbutton' name='submitBtn' disabled='disabled'>
										<input type='button' value='".$Lang['IES']['Cancel2']."' onclick='task_intro_edit_form_cancel($task_id)' onmouseout='this.className=\"formsubbutton\"' onmouseover='this.className=\"formsubbuttonon\"' class='formsubbutton' name='cancelBtn' disabled='disabled'>
									</div>
							   </form>";	
									   
			}
			else{
			    				
				$returnStr .= "<div class='stage_top_tool'>
									<a title='".$Lang['SBA']['Editintroduction']."' class='edit' href='javascript:void(0)' onclick='get_task_intro_edit_form($task_id)'><span>".$Lang['SBA']['Editintroduction']."</span></a>
									<div class='clear'><br></div>
						   	   </div>
							   ".$TaskObj->getIntroduction();
				if ($attachment){
				    $returnStr  .= "<div class='attachments'>".$Lang['IES']['Attachment'].": ";		
				    $returnStr .= $this->getTaskAttachmentTableUI($attachment, $IsEditing, $task_id);
				    $returnStr .= "</div>";
				}			   
				
				
			}
		}			   
		else{
			$returnStr .= $TaskObj->getIntroduction();
			if ($attachment){
			    $returnStr  .= "<div class='attachments'>".$Lang['IES']['Attachment'].": ";		
			    $returnStr .= $this->getTaskAttachmentTableUI($attachment, $IsEditing, $task_id);
			    $returnStr .= "</div>";
			}
			
		}
		
		$returnStr .= "</div>";
		
		return $returnStr;
	}
	
	function getTaskEditUI($TaskObj){
		global $sba_cfg, $Lang;
		
		$linterface = new interface_html();
		
		$id_suffix = $TaskObj->getTaskID()? $TaskObj->getTaskID():"new";
		$form_id   = "sba_task_editform_$id_suffix";
		
		$returnStr = "<form id=\"$form_id\" method=\"POST\">
							<div class='editing_area_min'>
								<table class='form_table'>
									<colgroup>
										<col width='200'>
	                    				<col class='field_c'>
	                    			</colgroup>
									<tr>
	                    				<td>".$Lang['SBA']['TaskName']."<span class='tabletextrequire'>*</span></td>
	                    				<td>:</td>
	                    				<td>
											<input id='".$form_id."_Title' name='r_task_Title' type='text' style='width:50%' class='textbox' maxlength='45' value='".htmlspecialchars($TaskObj->getTitle())."'>
											".$linterface->Get_Form_Warning_Msg($form_id.'_Title_ErrorDiv', $Lang['SBA']['PleaseFillInTaskName'], 'WarningDiv')."
										</td>
	                  				</tr>
	                    			<tr>
	                      				<td>".$Lang['SBA']['ConsistOfSteps']."</td>
	                      				<td>:</td>
	                      				<td>
											<input id='".$form_id."_InstantEdit' name='r_task_InstantEdit' value='1' type='hidden'>
											<input id='".$form_id."_StepEdit' name='r_task_StepEdit' value='1' type='checkbox' ".($TaskObj->getInstantEdit()==$sba_cfg['DB_IES_TASK_InstantEdit']['StepEdit'] || $TaskObj->getInstantEdit()==$sba_cfg['DB_IES_TASK_InstantEdit']['InstantStepEdit']? "checked":"").">
											<label for='".$form_id."_StepEdit'>".$Lang['IES']['NeedIt']."</label>
										</td>
	                    			</tr>
	                    			<tr>
	                      				<td>".$Lang['IES']['Status']."<span class='tabletextrequire'>*</span></td>
	                      				<td>:</td>
	                      				<td>
											<input id='".$form_id."_Enable_1' name='r_task_Enable' value='1' type='radio' ".($TaskObj->getEnable()? "checked":'').">
											<label for='".$form_id."_Enable_1'>".$Lang['IES']['RecordStatusPublish']."</label>
											<input id='".$form_id."_Enable_0' name='r_task_Enable' value='0' type='radio' ".(!$TaskObj->getEnable()? "checked":'').">
											<label for='".$form_id."_Enable_0'>".$Lang['IES']['RecordStatusNotPublish']."</label>
											".$linterface->Get_Form_Warning_Msg($form_id.'_Enable_ErrorDiv', $Lang['SBA']['PleaseSelectReleaseStatus'], 'WarningDiv')."
										</td>
	                    			</tr>
									<tr ".($TaskObj->getSequence()==1? "style='display:none'":"").">
	                      				<td>".$Lang['SBA']['Setting_AllowNextTask']."</td>
	                      				<td>:</td>
	                      				<td>
											<input id='".$form_id."_Approval' name='r_task_Approval' value='1' type='checkbox' ".($TaskObj->getSequence()>1 && $TaskObj->getApproval()? "checked":'').">
											<label for='".$form_id."_Approval'>".$Lang['SBA']['NeedApprovalToAccessThisTask']."</label>
										</td>
	                    			</tr>
								</table>
	                	   </div>
						   <div class='task_writing_bottom'>
	                			<input name='sba_task_SubmitBtn' type='button' class='formbutton' value='".($TaskObj->getTaskID()? $Lang['IES']['Save2']:$Lang['SBA']['SaveAndEditIntroduction'])."' onclick='task_edit_form_submit(".$TaskObj->getTaskID().")' onmouseout='this.className=\"formbutton\"' onmouseover='this.className=\"formbuttonon\"'>";
	    
	    if($TaskObj->getTaskID()){
	    	$returnStr .= "		<input name='sba_task_DeleteBtn' type='button' class='formsubbutton' value=' ".$Lang['IES']['Delete2']." ' onclick='task_delete(".$TaskObj->getTaskID().")' onmouseout='this.className=\"formsubbutton\"' onmouseover='this.className=\"formsubbuttonon\"'>";
	    }            			
		
		$returnStr .= "			<input name='sba_task_CancelBtn' type='button' class='formsubbutton' value=' ".$Lang['IES']['Cancel2']." ' onclick='task_edit_form_cancel(".$TaskObj->getTaskID().")' onmouseout='this.className=\"formsubbutton\"' onmouseover='this.className=\"formsubbuttonon\"'>
	                		</div>
							<input id='".$form_id."_ajaxAction' name='ajaxAction' type='hidden' value='TaskEditUpdate'>
							<input id='".$form_id."_TaskID' name='task_id' type='hidden' value='".$TaskObj->getTaskID()."'>
							<input id='".$form_id."_Sequence' name='r_task_Sequence' type='hidden' value='".$TaskObj->getSequence()."'>
					  </form>";
		
		return $returnStr;
	}
	
	function getStepUI($StepObj, $StepHandinObj, $IsEditable=false){ 
		global $Lang;
		
		$id_suffix  = $StepObj->getStepID()? $StepObj->getStepID():"new";
		$task_id    = $StepObj->getTaskID();
		$step_id    = $StepObj->getStepID();
		$form_id    = "sba_step_handinForm_$id_suffix";
		$attachment = $StepObj->getAttachments();
		
		$returnStr = "	<div class='task_box_step'>";
		
		if($IsEditable){
			$returnStr .= "	<div class='stage_top_tool'>
					    <a title='".$Lang['SBA']['EditStep']."' class='edit' href='javascript:void(0)' onclick='get_step_edit_form(".$StepObj->getTaskID().", ".$StepObj->getStepID().")'><span>".$Lang['SBA']['EditStep']."</span></a>
					</div>";
		}
		
		$returnStr .= "		<div class='task_writing_top'>".$StepObj->getTitle()."</div>
					<div class='writing_area'>
					    <div class='task_content'>
                    				".$StepObj->getDescription();
		if ($attachment){
		    $returnStr .= "<div class='attachments'>".$Lang['IES']['Attachment'].": ";		
		    $returnStr .= $this->getTaskAttachmentTableUI($attachment, false, $task_id, $step_id);
		    $returnStr .= "</div>";
		}
		$returnStr .= "		    <form id='$form_id' method='POST'>
						    ".$StepHandinObj->getQuestionDisplay()."
						</form>
						".($IsEditable? "":"<div style='clear: both; color: #666666; display: block; font-size: 0.92em;'>".$Lang['SBA']['MissStepWarning']."</div>")."
					    </div>
					</div>       
				</div>";
		
		return $returnStr;
	}
	
	function getStepEditUI($StepObj, $StepHandinObj=null){
		global $sba_cfg, $Lang;
		
		$linterface = new interface_html();
		
		//$id_suffix = $StepObj->getStepID()? $StepObj->getStepID():"new";
		$task_id = $StepObj->getTaskID();
		$step_id = $StepObj->getStepID();
		$form_id = "sba_step_editform_$task_id";
		
		$attachment = $StepObj->getAttachments();
		
		
		# Get HTML editor for Introduction
		/*
		$oFCKeditor = new FCKeditor('r_step_Description_'.$task_id, '100%', 200, '', 'Basic2_SBA');
		$oFCKeditor->Value = $StepObj->getDescription();
		
		$editor = $oFCKeditor->Create2();
		*/
		$editor = getCkEditor('r_step_Description_'.$task_id, $StepObj->getDescription(), "sba", 200, '100%');
		$editor .= "<script>
				CKEDITOR.on('instanceReady',
				   function( evt ) {
				   		var oEditor = evt.editor;
				    	evt.editor.Name = evt.editor.name;
				    	FCKeditor_OnComplete(oEditor);
				   }
				);
				</script>";
		
		$returnStr = "<div class='task_box_step'>
				<form id='$form_id' method='POST'>
					  	
							<div class='task_writing_top'>
								".$Lang['SBA']['Title']." <span class='tabletextrequire'>*</span>: <input type='text' style='width:50%' class='textbox' id='".$form_id."_Title' name='r_step_Title' value='".htmlspecialchars($StepObj->getTitle())."'>
								<span id='".$form_id."_Title_ErrorDiv' class='WarningDiv tabletextrequire' style='display:none'>* ".$Lang['SBA']['PleaseFillInTitle']."</span>
							</div>
	                    	<div class='writing_area'>
	                    		<div class='task_content'>
	                        		<table class='form_table'>
	                          			<colgroup>
											<col width='70'>
											<col class='field_c'>
											<col>
											<col width='20'>
	                          			</colgroup>
										<tr>
	                            			<td>".$Lang['IES']['Introduction']."</td>
	                            			<td>:</td>
	                            			<td>
												".$editor."
												<textarea name='r_step_Description' style='display:none'></textarea>
											</td>
											<td>&nbsp;</td>
	                          			</tr>
							<tr id='".$form_id."_selectTools' ".(is_null($StepHandinObj)? "":"style='display:none'").">
	                            			<td>".$Lang['SBA']['AnswerTool']."</td>
	                            			<td>:</td>
	                            			<td>
												<select id='".$form_id."_TmpQuestionType' name='r_step_TmpQuestionType' onchange='get_step_edit_form_tool($task_id)'>
													<option value=''>-- ".$Lang['SBA']["PleaseSelect"]." --</option>";

		if(is_array($sba_cfg['DB_IES_STEP_QuestionType']) && count($sba_cfg['DB_IES_STEP_QuestionType'])>0){
			foreach($sba_cfg['DB_IES_STEP_QuestionType'] as $QuestionTypeAry){
				$QuestionType = $QuestionTypeAry[0];
				$QuestionTypeCaption = $QuestionTypeAry[1];
				
				$returnStr .= "						<option value='$QuestionType'>$QuestionTypeCaption</option>";
			}
		}
		
		$returnStr .= "							</select>
												<img id='".$form_id."_tool_loadingImg' src='/images/2009a/SBAIES/student/loading_s.gif' style='display:none'/>
												".$linterface->Get_Form_Warning_Msg($form_id.'_TmpQuestionType_ErrorDiv', $Lang['SBA']['PleaseSelectAnswerTool'], 'WarningDiv')."
											</td>
											<td>&nbsp;</td>
										</tr> 
										<tr id='".$form_id."_tool' ".(is_null($StepHandinObj)? "style='display:none'":"").">
											<td id='".$form_id."_toolCaption'>".(is_null($StepHandinObj)? "":$StepHandinObj->getCaption())."</td>
											<td>:</td>
											<td id='".$form_id."_toolDisplay'>".(is_null($StepHandinObj)? "":$StepHandinObj->getEditQuestionDisplay())."</td>
											<td><div class='table_row_tool'><a title='".$Lang['IES']['Delete2']."' class='delete' href='javascript:void(0)' onclick='delete_step_edit_form_tool($task_id)'></a></div></td>
	                        			</tr>
							<tr>
							    <td>".$Lang['IES']['Attachment']."</td>
							    <td>:</td>
							    <td><div class='attachments attachments_edit'>";
							    
		$returnStr .= $this->getTaskAttachmentTableUI($attachment, true, $task_id, $step_id);
		
		$returnStr .=			    "</div></td>
							</tr></table>
	                    		</div>
	                    	</div>       
							<div class='task_writing_bottom'>
								<div>
									<input type='button' value='".$Lang['IES']['Save2']."' onclick='step_edit_form_submit($task_id)' onmouseout='this.className=\"formbutton\"' onmouseover='this.className=\"formbuttonon\"' class='formbutton' name='submitBtn' disabled='disabled'>
									<input type='button' value='".$Lang['IES']['Delete2']."' onclick='step_delete($task_id)' onmouseout='this.className=\"formsubbutton\"' onmouseover='this.className=\"formsubbuttonon\"' class='formsubbutton' name='deleteBtn' disabled='disabled' ".($StepObj->getStepID()? "":"style='display:none'").">
									<input type='button' value='".$Lang['IES']['Cancel2']."' onclick='step_edit_form_cancel($task_id)' onmouseout='this.className=\"formsubbutton\"' onmouseover='this.className=\"formsubbuttonon\"' class='formsubbutton' name='cancelBtn' disabled='disabled'>
								</div>
							</div>
						
						<input type='hidden' id='".$form_id."_Sequence' name='r_step_Sequence' value='".$StepObj->getSequence()."'/>
						<input type='hidden' id='".$form_id."_step_id' name='step_id' value='$step_id'/>
		
					 </form></div>";
					  
		return $returnStr;
	}
	
	/* Comment out on 2012-07-25 by Thomas
	function getTaskUI($TaskObj, $TaskHandinObj, $IsEditable=false){
		
		$returnStr = "<div id=\"sba_task_".($TaskObj->getTaskID()? $TaskObj->getTaskID():"new")."\" class=\"IES_task\">";
		
		# If task is enabled or User have edit right (i.e. Teacher)
		if($TaskObj->getEnable() || $IsEditable){
			if($TaskObj->getTaskID()){
				$returnStr .= "<div class=\"task_title task_open\">
								  <span>".$TaskObj->getTitle()."</span>
							   </div>";
			}
			else{
				$returnStr .= "<a id=\"sba_new_task_btn\" class=\"task_title task_add\" href=\"javascript:void(0)\" onclick=\"sba_task_form_show()\">
								<span>新增工作</span>
							   </a>
							   <div id=\"sba_new_task_title\" class=\"task_title task_add\" style=\"display:none\">
								<span>新增工作</span>
							   </div>";
			}
			
			$returnStr .= "<div id=\"sba_task_content_".($TaskObj->getTaskID()? $TaskObj->getTaskID():"new")."\">".($TaskObj->getTaskID()? $this->getTaskDisplayUI($TaskObj, $TaskHandinObj, $IsEditable):'&nbsp;')."</div>
	                	   <div class=\"clear\"></div>";
		}
		# If task is not enabled and User does not have edit right (i.e. Student)
		else{
			$returnStr .= "	<a class=\"task_title task_close\"><span>".$TaskObj->getTitle()."</span></a>
                			<span class=\"content_grey_text\">(未開放)</span>
                			<div class=\"clear\"></div>";	
		}
		
		$returnStr .= "</div>";
		
		return $returnStr;
	}
	
	function getTaskDisplayUI($TaskObj, $TaskHandinObj, $IsEditable=false){
		global $sba_cfg;
		
		# Introdcution Button
		if($TaskObj->getIntroduction()){
			$returnStr .= "<a href=\"javascript:void(0)\" class=\"show_intro\" onclick=\"show_introduction(".$TaskObj->getTaskID().")\"><span>簡介</span></a>";
		}
		
        # Edit Button
        if($IsEditable){
        	$returnStr .= "<div class=\"stage_left_tool\">
            	        		<a title=\"編輯\" class=\"sba_task_edit_btn edit\" href=\"javascript:void(0)\" onclick=\"sba_task_form_show(".$TaskObj->getTaskID().")\"><span>編輯</span></a>
                		   </div>";
        }
		
		# Task Steps List (Circle)
		$returnStr .= $this->getTaskStepsCircleList($TaskObj, 0, $IsEditable);
        
		$returnStr .= "<div class=\"clear\"></div>";
		$returnStr .= "<div class=\"clear\"></div>";
		$returnStr .= "<div id=\"sba_task_content_IntroAnsBox_".$TaskObj->getTaskID()."\">";
		
		# Check user have submitted answer before or not
		if($TaskHandinObj->getAnswerID()){
			$returnStr .= $this->getTaskAnsBox($TaskObj, $TaskHandinObj);
		}
		else{
			$returnStr .= $this->getTaskIntroBox($TaskObj, $IsEditable);
		}
		
		$returnStr .= "</div>";
		
		# Generate the UI for Instant Edit
		/*
		if($TaskObj->getInstantEdit()==$sba_cfg['DB_IES_TASK_InstantEdit']['InstantEdit'] || $TaskObj->getInstantEdit()==$sba_cfg['DB_IES_TASK_InstantEdit']['InstantStepEdit']){
			$returnStr .= $this->getTaskAnsSheetBox($TaskObj, $TaskHandinObj, $IsEditable);
		}
		*\/
		
		return $returnStr;
	}
	
	function getTaskIntroBox($TaskObj, $IsEditable=false){
		global $sba_cfg;
		
		$returnStr .= "<div id=\"sba_task_intro_".$TaskObj->getTaskID()."\" class=\"task_intro_box\">";
		
		# Introduction
		if($TaskObj->getIntroduction()){
			$returnStr .= "	<div id=\"sba_task_intro_brief_".$TaskObj->getTaskID()."\" class=\"task_intro_brief\" style=\"display:none\">
                				<a title=\"關閉\" class=\"intro_close\" href=\"javascript:void(0)\" onclick=\"hide_introduction(".$TaskObj->getTaskID().")\">&nbsp;</a>
                				<span class=\"intro_guide\">".$TaskObj->getIntroduction()."</span>
                				<div class=\"clear\"></div>
                			</div>";
		}
		
		$returnStr .= "		<div id=\"sba_task_intro_content_".$TaskObj->getTaskID()."\" class=\"writing_memo\">
								<div class=\"icon_stu_edit\"></div>
								<div class=\"clip\"></div>
								<div class=\"top\"><div><div></div></div></div>
                    			<div class=\"left\">
									<div class=\"right\">
										<div class=\"mid\">";
										
		if(!$IsEditable){
			$returnStr .= "					<div class=\"task_intro_buttons\">
												<a style=\"float:none; margin:0 auto; width:200px; display:block\" onclick=\"sba_task_ansSheet_show(".$TaskObj->getTaskID().")\" href=\"javascript:void(0)\" class=\"instant_write\"><span>即時撰寫</span></a>
											</div>
											<br/>
											<br/>";
		}
		
		$returnStr .= "					</div>
									</div>
								</div>
                    			<div class=\"bottom\"><div><div></div></div></div>
                   				<div class=\"clear\"></div>
							</div>
                			<div class=\"clear\"></div>
					   </div>";
		
		return $returnStr;
	}
	
	function getTaskAnsBox($TaskObj, $TaskHandinObj){
		global $sba_cfg, $Lang;
		
		$objIES = new libies();
		
		$returnStr .= "<div id=\"sba_task_intro_".$TaskObj->getTaskID()."\" class=\"IES_ans_box\">";
		
		# Introduction
		if($TaskObj->getIntroduction()){
			$returnStr .= "	<div id=\"sba_task_intro_brief_".$TaskObj->getTaskID()."\" class=\"task_intro_brief\" style=\"display:none\">
                				<a title=\"關閉\" class=\"intro_close\" href=\"javascript:void(0)\" onclick=\"hide_introduction(".$TaskObj->getTaskID().")\">&nbsp;</a>
                				<span class=\"intro_guide\">".$TaskObj->getIntroduction()."</span>
                				<div class=\"clear\"></div>
                			</div>";
		}
		
		$returnStr .= "		<div id=\"sba_task_intro_content_".$TaskObj->getTaskID()."\" class=\"writing_memo\">
	                			<div class=\"icon_stu_edit\"></div>
	                    		<div class=\"clip\"></div>
	                    		<div class=\"top\"><div><div></div></div></div>
	                    		<div class=\"left\">
									<div class=\"right\">
										<div class=\"mid\">
											".($TaskHandinObj->getDisplayAnswer())."
					                        <div style=\"width:100%\">
		                        				<a class=\"task_edit\" href=\"javascript:void(0)\" onclick=\"sba_task_ansSheet_show(".$TaskObj->getTaskID().")\"><span>即時修改</span></a>
		                        			</div>                
		                    			</div>
									</div>
								</div>
	                    		<div class=\"bottom\"><div><div></div></div></div>
								<div class=\"clear\"></div>
							</div>";
		
		# check if there are any comments from teachers 
		$is_teacher_comment 	  = $objIES->getTeacherCommentLatestTime($TaskObj->getTaskID(), $TaskHandinObj->getUserID());
		$is_teacher_score_on_task = $objIES->getTeacherTaskScoreLatestTime($TaskObj->getTaskID(), $TaskHandinObj->getUserID());
		
		if(!empty($is_teacher_comment) || !empty($is_teacher_score_on_task)){
			$teach_comment_date  = '';
			if(empty($is_teacher_comment["DateInput"])) {
				//do nothing
			}
			else {
				$teach_comment_date   = '('.$is_teacher_comment["DateInput"].')';
			}
			$returnStr .= "	<div class=\"teacher_comment\"><a href=\"javascript:doGetComment(".$TaskObj->getTaskID().")\">".$Lang['IES']['TeachersFeedback']."</a><span style=\"font-size:9px; color:grey\">".$teach_comment_date."</span></div><!-- if have new comment, <a> add class \"new\"-->";
		}
		else {
			$returnStr .= "	<div class=\"teacher_comment\" style=\"display:none;\"><a href=\"javascript:doGetComment(".$TaskObj->getTaskID().")\">".$Lang['IES']['TeachersFeedback']."</a></div><!-- if have new comment, <a> add class \"new\"-->";
		}
					
		$returnStr .= "		<div class=\"edit_date\">呈交日期 : 2012-09-12 | 最後修改日期 : ".date('Y-m-d', strtotime($TaskHandinObj->getDateModified()))."</div>
                			<div class=\"clear\"></div>
							<div id=\"Comment_block_".$TaskObj->getTaskID()."\" class=\"IES_commentbox\" style=\"display:none\"></div>
                	   </div>";
                
		return $returnStr;
	}
	
	function getTaskAnsSheetBox($TaskObj, $TaskHandinObj, $IsEditable=false){
		
		$returnStr = "<form id=\"sba_task_ansSheet_".$TaskObj->getTaskID()."\">
						<div class=\"editing_area_min\"> 
							".$TaskHandinObj->getDisplayAnswerInput()."
					  	</div>
					  	<div class=\"task_writing_bottom\">
	                		<input name=\"sba_ansSheet_SubmitBtn\" type=\"button\" class=\"formbutton\" value=\" 儲存 \" ".(!$IsEditable? "onclick=\"sba_task_ansSheet_submit(".$TaskObj->getTaskID().")\"" : "")." onmouseout=\"this.className='formbutton'\" onmouseover=\"this.className='formbuttonon'\" ".($IsEditable? "disabled":"").">
							<input name=\"sba_ansSheet_CancelBtn\" type=\"button\" class=\"formsubbutton\" value=\" 取消 \" onclick=\"sba_task_ansSheet_cancel(".$TaskObj->getTaskID().")\" onmouseout=\"this.className='formsubbutton'\" onmouseover=\"this.className='formsubbuttonon'\">
        	        	</div>
						<input id=\"sba_task_ansSheet_TaskID\" name=\"task_id\" type=\"hidden\" value=\"".$TaskObj->getTaskID()."\">
						<input id=\"sba_task_ansSheet_AnswerID\" name=\"r_task_ansSheet_AnswerID\" type=\"hidden\" value=\"".$TaskHandinObj->getAnswerID()."\">
						<input id=\"sba_task_ansSheet_QuestionType\" name=\"r_task_ansSheet_QuestionType\" type=\"hidden\" value=\"".$TaskHandinObj->getQuestionType()."\">
					  </form>";
		
		return $returnStr;
	}
	
	function getTaskFormUI($TaskObj){
		global $sba_cfg, $intranet_root, $Lang;
		
		# Get HTML editor for Introduction
		$oFCKeditor = new FCKeditor('r_task_Introduction', 600, 200, '', 'Basic2_SBA');
		$oFCKeditor->Value = $TaskObj->getIntroduction();
		
		$formID = "sba_task_form_".($TaskObj->getTaskID()? $TaskObj->getTaskID():"new");
		
		$returnStr  = "<div class=\"clear\"></div>
					   <form id=\"$formID\" method=\"POST\">
							<div class=\"editing_area_min\">
								<table class=\"form_table\">
		                    		<colgroup>
										<col width=\"120\">
		                    			<col class=\"field_c\">
		                    		</colgroup>
	                  				<tr>
	                    				<td>".$Lang['SBA']['TaskName']."</td>
	                    				<td>:</td>
	                    				<td><input id=\"".$formID."_Title\" name=\"r_task_Title\" type=\"text\" style=\"width:50%\" class=\"textbox\" maxlength=\"45\" value=\"".htmlspecialchars($TaskObj->getTitle())."\"></td>
	                  				</tr>
									<tr>
		                      			<td>".$Lang['IES']['Introduction']."</td>
		                      			<td>:</td>
										<td>".$oFCKeditor->Create2()."</td>
									</tr>
		                    		<tr>
		                      			<td>".$Lang['IES']['Step']."</td>
		                      			<td>:</td>
		                      			<td>
											<input id=\"".$formID."_InstantEdit\" name=\"r_task_InstantEdit\" value=\"1\" type=\"hidden\">
											<input id=\"".$formID."_StepEdit\" name=\"r_task_StepEdit\" value=\"1\" type=\"checkbox\" ".($TaskObj->getInstantEdit()==$sba_cfg['DB_IES_TASK_InstantEdit']['StepEdit'] || $TaskObj->getInstantEdit()==$sba_cfg['DB_IES_TASK_InstantEdit']['InstantStepEdit']? "checked":"").">
											<label for=\"".$formID."_StepEdit\">".$Lang['IES']['NeedIt']."</label>
										</td>
		                    		</tr>
		                    		<tr>
		                      			<td>".$Lang['IES']['Status']."</td>
		                      			<td>:</td>
		                      			<td>
											<input id=\"".$formID."_Enable_1\" name=\"r_task_Enable\" value=\"1\" type=\"radio\" ".($TaskObj->getEnable()? "checked":'').">
											<label for=\"".$formID."_Enable_1\">".$Lang['IES']['RecordStatusPublish']."</label>
											<input id=\"".$formID."_Enable_0\" name=\"r_task_Enable\" value=\"0\" type=\"radio\" ".(!$TaskObj->getEnable()? "checked":'').">
											<label for=\"".$formID."_Enable_0\">".$Lang['IES']['RecordStatusNotPublish']."</label>
										</td>
		                    		</tr>
		                    		<tr>
		                      			<td>".$Lang['IES']['Approve2']."</td>
		                      			<td>:</td>
		                      			<td>
											<input id=\"".$formID."_Approval\" name=\"r_task_Approval\" value=\"1\" type=\"checkbox\" ".($TaskObj->getApproval()? "checked":'').">
											<label for=\"".$formID."_Approval\">".$Lang['IES']['NeedTeacherApproval']."</label>
										</td>
		                    		</tr>
	                    		</table>
	                		</div>                         
	                		<div class=\"task_writing_bottom\">
	                			<input name=\"sba_task_SubmitBtn\" type=\"button\" class=\"formbutton\" value=\" ".$Lang['IES']['Save']." \" onclick=\"sba_task_form_submit(".$TaskObj->getTaskID().")\" onmouseout=\"this.className='formbutton'\" onmouseover=\"this.className='formbuttonon'\" disabled=\"disabled\">";
	    
	    if($TaskObj->getTaskID()){
	    	$returnStr .= "		<input name=\"sba_task_DeleteBtn\" type=\"button\" class=\"formsubbutton\" value=\" ".$Lang['IES']['Delete2']." \" onclick=\"javascript:void(0)\" onmouseout=\"this.className='formsubbutton'\" onmouseover=\"this.className='formsubbuttonon'\" disabled=\"disabled\">";
	    }            			
		
		$returnStr .= "			<input name=\"sba_task_CancelBtn\" type=\"button\" class=\"formsubbutton\" value=\" ".$Lang['IES']['Cancel2']." \" onclick=\"sba_task_form_cancel(".$TaskObj->getTaskID().")\" onmouseout=\"this.className='formsubbutton'\" onmouseover=\"this.className='formsubbuttonon'\" disabled=\"disabled\">
	                		</div>
							<input id=\"".$formID."_ajaxAction\" name=\"ajaxAction\" type=\"hidden\" value=\"TaskEditUpdate\">
							<input id=\"".$formID."_TaskID\" name=\"task_id\" type=\"hidden\" value=\"".$TaskObj->getTaskID()."\">
							<input id=\"".$formID."_Sequence\" name=\"r_task_Sequence\" type=\"hidden\" value=\"".$TaskObj->getSequence()."\">
					   </form>";
		
		return $returnStr;
	}
	
	function getTaskStepsCircleList($TaskObj, $CurrentStepID=0, $IsEditable=false, $AddDummyStep=false){
		global $sba_cfg, $Lang;
		
		$returnStr = "";
		
		if($TaskObj->getInstantEdit()==$sba_cfg['DB_IES_TASK_InstantEdit']['StepEdit'] || $TaskObj->getInstantEdit()==$sba_cfg['DB_IES_TASK_InstantEdit']['InstantStepEdit']){
	        $steps = $TaskObj->getObjStepsAry();
	        $NoOfSteps = count($steps);
			
			if($NoOfSteps > 0 || $AddDummyStep){
				$returnStr .= "<ul class=\"line\">
								  <span>".$Lang['IES']['Step']."</span>";
				
				for($i=0;$i<$NoOfSteps;$i++){
					$step_title_type_str = $steps[$i]->getTitle()." [".$sba_cfg['DB_IES_STEP_QuestionType'][$steps[$i]->getQuestionType()][1]."]";
					
					$returnStr .= $i>0? "<li class=\"spacer\"></li>":"";
					$returnStr .= "<li class=\"circle\"><a title=\"{$step_title_type_str}\" href=\"javascript:void(0)\" ".($steps[$i]->getStepID()==$CurrentStepID? "class=\"current\"":"")." ".($steps[$i]->getStepID()? "onclick=\"go_step(".$TaskObj->getTaskID().", ".$steps[$i]->getStepID().")\"":"").">".($i+1)."</a></li>";
				}
				
				if($AddDummyStep){
					$returnStr .= $NoOfSteps>0? "<li class=\"spacer\"></li>":"";
					$returnStr .= "<li class=\"circle\"><a href=\"javascript:void(0)\" class=\"current\">".($NoOfSteps+1)."</a></li>";
				}
				
				$returnStr .= "</ul>";
			}
			
	        # Add steps Button
	        if($IsEditable && $returnStr==""){
				$returnStr = "	<div class=\"stage_top_tool sba_step_new_btn\" style=\"float:none\">
	                    			<a title=\"".$Lang['SBA']['AddStep']."\" class=\"add_step\" href=\"javascript:void(0)\" onclick=\"go_create_step(".$TaskObj->getTaskID().")\"><span>".$Lang['SBA']['AddStep']."</span></a>
	                    		</div>";
	        }
        }
        
        return $returnStr? "<div class=\"task_steps\">".$returnStr."</div>":"";
	}
	*/
	
	/*
	 * used in /home/eLearning/sba/content/stageEdit.php and stageNew.php 
	 */
	function GetStageEditUI($thisSchemeID, $thisStageID="", $viewOnly=0) 
	{
		global $Lang, $ies_cfg;
		
		$objIES = new libies();
		$libSba = new libSba();
		$linterface = new interface_html();
		
		if($thisStageID=="") {
			$isNewStage = true;	
		}
		
		//$schemeDetails = $objIES->GET_SCHEME_DETAIL($thisSchemeID);
		
		if($thisStageID!="") {
			$stageDetailsAry = $objIES->GET_STAGE_DETAIL($thisStageID);
		}

		$_stageTitle = intranet_htmlspecialchars($stageDetailsAry["Title"]);
		$_stage_id = $thisStageID;
		$_stageWeight = $stageDetailsAry["Weight"];
		$_stageMaxScore = $stageDetailsAry["MaxScore"];
		$_stageDescription = $stageDetailsAry["Description"];
		$_stageDeadline = ($stageDetailsAry['Deadline']=="0000-00-00") ? " " : $stageDetailsAry['Deadline'];
		$_stageRecordStatus = $stageDetailsAry['RecordStatus'] ? $stageDetailsAry['RecordStatus'] : $ies_cfg["recordStatus_notpublish"];
		
		
		$MarkCriteriaContent = $this->GetSbaMarkCriteriaTable($thisSchemeID, $thisStageID, $viewOnly, $isNewStage);
		
		$CriteriaTableContent = $MarkCriteriaContent[0];
		//$StageWeightMaxScore = $MarkCriteriaContent[1];
		
		$_RecordStatusSelect[$_stageRecordStatus] = " checked";
		
		$returnContent = '
                    
                    <table class="form_table">
                    <col width="120">
                    <col class="field_c" />   
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
						<td>'.$Lang['SBA']['StageName'].'</td><td>:</td><td>
							<input name="r_stage_name" type="text" id="r_stage_name" value="'.$_stageTitle.'" class="textbox"/>
							<div id="stageNameDiv"></div>
						</td>
					</tr>
                    <tr>
						<td>'.$Lang['SBA']['StageDescription'].'</td><td>:</td>
						<td><textarea style="width:100%; height:150px" name="r_stage_description" id="r_stage_description">'.$_stageDescription.'</textarea>
						</td>
					</tr>
					<tr>
						<td>'.$Lang['IES']['Status'].'</td><td>:</td>
						<td>
							<input type="radio" name="r_stageRecordStatus" id="stageRecordStatus[1]" value="1" '.$_RecordStatusSelect[1].'>
							<label for="stageRecordStatus[1]">'.$Lang['IES']['RecordStatusPublish'].'</label>
							<input type="radio" name="r_stageRecordStatus" id="stageRecordStatus[0]" value="0" '.$_RecordStatusSelect[0].'>
						<label for="stageRecordStatus[0]">'.$Lang['IES']['RecordStatusNotPublish'].'</label>
							<div id="stageRecordstatusDiv" style="10px red solid;"></div>
						</td>
					</tr>
                    </table>
 
                    <div class="line_dotted"></div>

					<i>'.$Lang['SBA']['ContentBelowOnlyViewableByTeacher'].'</i><br /><br />


                    <table class="form_table">
                    <col class="field_title" />
                    <col class="field_c" />                    
                    <tr><td>'.$Lang['IES']['MarkingSettings'].'</td><td>:</td><td>
						'.$CriteriaTableContent.'
						<div id="stageRubricDiv"></div>
					</td></tr>
                    <tr><td>'.$Lang['IES']['DateOfSubmission'].'</td>
                    <td>:</td><td>
						'.$linterface->GET_DATE_PICKER("r_stage_deadline",$_stageDeadline, "", "", "", "", "", "", "", "", "", "", "textbox_date").'
					</td></tr>
                    
					'.$StageWeightMaxScore.'

                    </table>
                    
                  </div>  

		';			
		
		return $returnContent;	
		
	}
	
	public function GetMarkCriteriaTable($thisSchemeID, $thisStageID="", $viewOnly=0, $isNewStage=0)
	{
		global $Lang, $sba_thisSchemeLang;
		$objIES = new libies();
		$libSba = new libSba();
		$linterface = new interface_html();
		
		$schemeDetails = $objIES->GET_SCHEME_DETAIL($thisSchemeID);
		
		$stageDetailsAry = $objIES->GET_STAGE_DETAIL($thisStageID);

		$_stageTitle = $stageDetailsAry["Title"];
		$_stage_id = $thisStageID;
		$_stageWeight = $stageDetailsAry["Weight"];
		$_stageMaxScore = $stageDetailsAry["MaxScore"];
		$_stageDescription = $stageDetailsAry["Description"];
		$_stageDeadline = $stageDetailsAry['Deadline'];
		
		$libies_static = new libies_static();
		
		if($isNewStage) {
			$stage_mark_criteria_arr = $libSba->GetDefaultMarkingCriteria();
		} else {
			$stage_mark_criteria_arr = $libies_static->getStageMarkingCriteria($thisSchemeID, $thisStageID);
		}
		//debug_pr($stage_mark_criteria_arr);
		
		$classRoomID = libies_static::getIESRurbicClassRoomID();

		//FOR SAFE , if there is no elcass
		$objRubric = new libiesrubric($classRoomID);
		$standardRubricSet = $objRubric->getStandardRubricSet(NULL,"set_title");
		
		$criteria_data_select = array();
		
		$this_content .= '<table class="common_table_list">';
		$this_content .= '<thead>
                <tr>
                  <th> '.$Lang['IES']['Criteria'].'</th>
                  <th> '.$Lang['IES']['Weight'].'</th>
                  <th> '.$Lang['IES']['MaxScore'].'</th>
                  <th> '.$Lang['IES']['Rubric'].'</th>';
        if(!$viewOnly) {
        	$this_content .= '          
				  <th>&nbsp;</th>
					';
        }
        $this_content .= '
                  <th> '.$Lang['IES']['Weight'].'</th>
                  <th> '.$Lang['IES']['MaxScore'].'</th>
                  </tr>
              </thead>
			  <tbody>
		';
		
		for($i = 0, $j = sizeof($standardRubricSet); $i< $j; $i++){
			$_tmpID = $standardRubricSet[$i]["STD_RUBRIC_SET_ID"];
			$_tmpTitle = $standardRubricSet[$i]["SET_TITLE"];
			$criteria_data_select[]  = array($_tmpID,$_tmpTitle);
		}
		
		$schemeMaxScore = $schemeDetails["MaxScore"];

		//$max_k = ($isNewStage) ? 2 : sizeof($stage_mark_criteria_arr);

		for($k = 0, $max_k = sizeof($stage_mark_criteria_arr); $k< $max_k; $k++){
			//LOOP for each marking criteria
			$_tempCriteriaID = $stage_mark_criteria_arr[$k]["MarkCriteriaID"];
			if($_tempCriteriaID == MARKING_CRITERIA_OVERALL) continue;    // No rubric for overall mark
		
			$_stageCriteriaID = $stage_mark_criteria_arr[$k]['StageMarkCriteriaID'];
			//$_criteriaTitle = $stage_mark_criteria_arr[$k]['CRITERIATITLE'];
			$_criteriaTitle = $sba_thisSchemeLang=="en"?$stage_mark_criteria_arr[$k]['TitleEng']:$stage_mark_criteria_arr[$k]['TitleChi'];
			$_criteriaMaxScore = $stage_mark_criteria_arr[$k]['MaxScore'];
			$_criteriaWeight = $stage_mark_criteria_arr[$k]['Weight'];
			$_selectName = " name = rubricCriteria[{$_stage_id}][{$_tempCriteriaID}] ";
			$_rubricDetail = $objRubric->getStageRubric($_stage_id, $_tempCriteriaID);
			$_rubricID = (sizeof($_rubricDetail) == 1 && is_array($_rubricDetail)) ? $_rubricDetail[0]["TASK_RUBRIC_ID"] : null;
			if(isset($_rubricID))
			{
				$_taskRubric = current($objRubric->getTaskRubricByRubricID($_rubricID));
				$_taskRubricTitle = $_taskRubric["title"];
			}
		
			$_criteriaMaxScoreReadonly = isset($_rubricID) ? "READONLY" : "";
			$_criteriaMaxScoreNoteReadonly = isset($_rubricID) ? '<span class="tabletextrequire">#</span>' : "";

			$_criteriaMaxScoreNoteReadonly = '';  //comment out first , need to revise the marking setting
error_log("_criteriaMaxScoreNoteReadonly [".$_criteriaMaxScoreNoteReadonly."]   <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
			$h_rubric_set = !isset($_rubricID) ? ($viewOnly ? "---" : getSelectByArray($criteria_data_select,$_selectName,$_rubricID)) : $_taskRubricTitle;
			$h_rubric_edit = isset($_rubricID) ? '<a href="index.php?mod=content&task=scheme_stage_rubric_edit&task_rubric_id='.$_rubricID.'&KeepThis=true&TB_iframe=true&height=400&width=700" class="thickbox edit_dim" title="'.$Lang['IES']['Edit'].'"></a>' : '&nbsp;';
			$h_rubric_delete = isset($_rubricID) ? '<a href="javascript:Remove_Task_Rubric('.$_rubricID.')" class="delete_dim" title="'.$Lang['IES']['Delete'].'"></a>' : "&nbsp;";
		
		                      
			if($k == 0){
				
				//k == 0 , displaying / handling first row of the table
				if($viewOnly) {
					$this_content .= '
									<tr>
			                          <td>'.$_criteriaTitle.'</td>
			                          <td>'.$_criteriaWeight.'</td>
			                          <td>'.$_criteriaMaxScore.$_criteriaMaxScoreNoteReadonly.'</td>
			                          <td>
			                          	'.$h_rubric_set.'
			                          </td>
			                          <td>'.$_stageWeight.'</td>
			                          <td>'.$_stageMaxScore.'</td>
			                        </tr>
					';
				} else {
					$this_content .= '
									<tr>
			                          <td>'.$_criteriaTitle.'<input type="hidden" name="stageCriteriaID[]" id="stageCriteriaID[]" value="'.$_stageCriteriaID.'" /></td>
			                          <td><input type="text" name="stageCriteriaWeight['.$_stageCriteriaID.']" id="stageCriteriaWeight['.$_stageCriteriaID.']" value="'.$_criteriaWeight.'" class="input_num" /></td>
			                          <td><input type="text" name="stageCriteriaMaxScore['.$_stageCriteriaID.']" id="stageCriteriaMaxScore['.$_stageCriteriaID.']" value="'.$_criteriaMaxScore.'" '.$_criteriaMaxScoreReadonly.' class="input_num" /> '.$_criteriaMaxScoreNoteReadonly.'</td>
			                          <td>
			                          	'.$h_rubric_set.'
			                          </td>
									  <td><span class="table_row_tool">'.$h_rubric_edit.' '.$h_rubric_delete.'</span></td>
			                          <td><input type="text" name="stageWeight['.$thisStageID.']" id="stageWeight['.$thisStageID.']" value="'.$_stageWeight.'" class="input_num" /></td>
			                          <td><input type="text" name="stageMaxScore['.$thisStageID.']" id="stageMaxScore['.$thisStageID.']" value="'.$_stageMaxScore.'" class="input_num" /><input type="hidden" name="MarkCriteriaID[]" id="MarkCriteriaID[]" value="'.$_tempCriteriaID.'"></td>
			                        </tr>
					';
				}
				
			}else{
				if($viewOnly) {
					$this_content .= '
			                        <tr>
			                          <td>'.$_criteriaTitle.'</td>
			                          <td>'.$_criteriaWeight.'</td>
			                          <td>'.$_criteriaMaxScore.$_criteriaMaxScoreNoteReadonly.'</td>
			                          <td>
			                          	'.$h_rubric_set.'
			                          </td>
									  <td>&nbsp;</td>
			                          <td>&nbsp;</td>
			                        </tr>
					';
				} else {
					$this_content .= '
			                        <tr>
			                          <td>'.$_criteriaTitle.'<input type="hidden" name="stageCriteriaID[]" id="stageCriteriaID[]" value="'.$_stageCriteriaID.'" /></td>
			                          <td><input type="text" name="stageCriteriaWeight['.$_stageCriteriaID.']" id="stageCriteriaWeight['.$_stageCriteriaID.']" value="'.$_criteriaWeight.'" class="input_num" /></td>
			                          <td><input type="text" name="stageCriteriaMaxScore['.$_stageCriteriaID.']" id="stageCriteriaMaxScore['.$_stageCriteriaID.']" value="'.$_criteriaMaxScore.'" '.$_criteriaMaxScoreReadonly.' class="input_num" /> '.$_criteriaMaxScoreNoteReadonly.'</td>
			                          <td>
			                          	'.$h_rubric_set.'
			                          </td>
									  <td><span class="table_row_tool">'.$h_rubric_edit.' '.$h_rubric_delete.'</span></td>
			                          <td>&nbsp;</td>
			                          <td>&nbsp;<input type="hidden" name="MarkCriteriaID[]" id="MarkCriteriaID[]" value="'.$_tempCriteriaID.'"></td>
			                        </tr>
					';
				}
			}
			
		}
		
		$this_content .= '</tbody>
						</table>';
		
		return $this_content;
	}
	
	public function GetSbaMarkCriteriaTable($thisSchemeID, $thisStageID="", $viewOnly=0, $isNewStage=0)
	{
		global $Lang, $ies_cfg, $sba_cfg, $sba_thisSchemeLang;
		$objIES = new libies();
		$libSba = new libSba();
		$linterface = new interface_html();
		$schemeDetails = $objIES->GET_SCHEME_DETAIL($thisSchemeID);
		
		if($thisStageID!="") {
			$stageDetailsAry = $objIES->GET_STAGE_DETAIL($thisStageID);
		}

		$_stageTitle = $stageDetailsAry["Title"];
		$_stage_id = $thisStageID;
		$_stageWeight = $stageDetailsAry["Weight"];
		$_stageMaxScore = $stageDetailsAry["MaxScore"];
		$_stageDescription = $stageDetailsAry["Description"];
		$_stageDeadline = $stageDetailsAry['Deadline'];
		
		$libies_static = new libies_static();
		
		if($isNewStage) {
			$stage_mark_criteria_arr = $libSba->GetDefaultMarkingCriteria();
		} else {
			$stage_mark_criteria_arr = $libies_static->getStageMarkingCriteria($thisSchemeID, $thisStageID);
		}
		//debug_pr($stage_mark_criteria_arr);
		
		$classRoomID = libies_static::getIESRurbicClassRoomID();

		//FOR SAFE , if there is no elcass
		$objRubric = new libiesrubric($classRoomID);
		$standardRubricSet = $objRubric->getStandardRubricSet(NULL,"set_title"); 
		
		$criteria_data_select = array();
		
		$this_content .= '<table class="common_table_list">';
		$this_content .= '<thead>
                <tr>';
        if($viewOnly) {
        	/*
        	$this_content .= '          
				  <th>&nbsp;</th>
					';
			*/	
        } else {
        	if($thisStageID!="") {
	        	$this_content .= '          
				  <!--<th>&nbsp;</th>-->
					';	
        	}
			$this_content .= '<th>'.$Lang['SBA']['InUseStatus'].'</th>';
        }
                
        $this_content .= '
                  <th> '.$Lang['IES']['Criteria'].'</th>
                  <th> '.$Lang['IES']['Weight'].'</th>
                  <th> '.$Lang['IES']['MaxScore'].'</th>
                  <th> '.$Lang['IES']['Rubric'].'</th>';
        //$this_content .= '<th>'.$Lang['SBA']['InUse'].'</th>';
        
        
		
		$this_content .= '
					<th>&nbsp;</th>
                  </tr>
              </thead>
			  <tbody>
		';
		
		for($i = 0, $j = sizeof($standardRubricSet); $i< $j; $i++){
			$_tmpID = $standardRubricSet[$i]["STD_RUBRIC_SET_ID"];
			$_tmpTitle = $standardRubricSet[$i]["SET_TITLE"];
			$criteria_data_select[]  = array($_tmpID,$_tmpTitle);
		}
		
		$schemeMaxScore = $schemeDetails["MaxScore"];

		//$max_k = ($isNewStage) ? 2 : sizeof($stage_mark_criteria_arr);
		
		$withCriteriaSetting = array();

		for($k = 0, $max_k = sizeof($stage_mark_criteria_arr); $k< $max_k; $k++){
			//LOOP for each marking criteria
			
			$_tempCriteriaID = $stage_mark_criteria_arr[$k]["MarkCriteriaID"];
			if($_tempCriteriaID == MARKING_CRITERIA_OVERALL) continue;    // No rubric for overall mark
		
			$_stageCriteriaID = $stage_mark_criteria_arr[$k]['StageMarkCriteriaID'];
			//$_criteriaTitle = $stage_mark_criteria_arr[$k]['CRITERIATITLE'];
			$_criteriaTitle = $sba_thisSchemeLang=="en"?$stage_mark_criteria_arr[$k]['TitleEng']:$stage_mark_criteria_arr[$k]['TitleChi'];
			$_criteriaMaxScore = $stage_mark_criteria_arr[$k]['MaxScore'];
			$_criteriaWeight = $stage_mark_criteria_arr[$k]['Weight'];
			$_criteriaStatus = $stage_mark_criteria_arr[$k]['RecordStatus'] ? $stage_mark_criteria_arr[$k]['RecordStatus'] : $ies_cfg["recordStatus_notpublish"];
			
			if($viewOnly && !$_criteriaStatus) {
				$withCriteriaSetting[] = 0;
				continue;	
			} else {
				$withCriteriaSetting[] = 1;	
			}
			
			$_selectName = " name = rubricCriteria[{$_stage_id}][{$_tempCriteriaID}] ";
			$_rubricDetail = $objRubric->getStageRubric($_stage_id, $_tempCriteriaID);
			$_rubricID = (sizeof($_rubricDetail) == 1 && is_array($_rubricDetail)) ? $_rubricDetail[0]["TASK_RUBRIC_ID"] : null;
			if(isset($_rubricID))
			{
				$_taskRubric = current($objRubric->getTaskRubricByRubricID($_rubricID));
				if(empty($_taskRubric)){
					unset($_rubricID);
				}
				$_taskRubricTitle = $_taskRubric["title"];
			}
		
			$_criteriaMaxScoreReadonly = isset($_rubricID) ? "READONLY" : "";
			$_criteriaMaxScoreNoteReadonly = isset($_rubricID) ? '<span class="tabletextrequire">#</span>' : "";

			$_criteriaMaxScoreNoteReadonly = ''; // comment first, should be revised the marking setting
			 
			$h_rubric_set = !isset($_rubricID) ? ($viewOnly ? "---" : getSelectByArray($criteria_data_select,$_selectName,$_rubricID)) : $_taskRubricTitle;
			$h_rubric_edit = isset($_rubricID) ? '<a href="index.php?mod=content&task=scheme_stage_rubric_edit&task_rubric_id='.$_rubricID.'&KeepThis=true&TB_iframe=true&height=400&width=700" class="thickbox edit_dim" title="'.$Lang['IES']['Edit'].'"></a>' : '&nbsp;';
			$h_rubric_delete = isset($_rubricID) ? '<a href="javascript:Remove_Task_Rubric('.$_rubricID.')" class="delete_dim" title="'.$Lang['IES']['Delete'].'"></a>' : "&nbsp;";
		
			# "Release status" checkbox
			$thisChecked = ($thisStageID=="" || $_criteriaStatus) ? "checked" : "";
			$_criteriaStatusCheckBox = '<input type="checkbox" name="r_stageStatus['.$k.']" id="r_stageStatus'.($k+1).'" value="1" '.$thisChecked.'>';
			$_criteriaStatusText = ($_criteriaStatus) ? $Lang['SBA']['InUse'] : $Lang['SBA']['NotInUse'];
			
			if($thisChecked=="" && $_criteriaMaxScore==$sba_cfg['MarkingCriteriaDefaultEmptyWeightMaxScore']) {
				$_criteriaMaxScore = "";
			}
			if($thisChecked=="" && $_criteriaWeight==$sba_cfg['MarkingCriteriaDefaultEmptyWeightMaxScore']) {
				$_criteriaWeight = "";
			}
			
		                      
			if($k == 0){
				
				//k == 0 , displaying / handling first row of the table
				if($viewOnly) {
					
					$thisDisplayStageWeight = $_stageWeight;
					$thisDisplayStageMaxScore = $_stageMaxScore;
					
					$this_content .= '
									<tr>
			                          <td>'.$_criteriaTitle.'</td>
			                          <td>'.$_criteriaWeight.'</td>
			                          <td>'.$_criteriaMaxScore.$_criteriaMaxScoreNoteReadonly.'</td>
			                          <td>
			                          	'.$h_rubric_set.'
			                          </td>
									
									  <td>&nbsp;</td>
			                        </tr>
					';
					
					
					
				} else {
					
					$thisDisplayStageWeight = '<input type="text" name="stageWeight['.$thisStageID.']" id="stageWeight['.$thisStageID.']" value="'.$_stageWeight.'" class="input_num" />';
					$thisDisplayStageMaxScore = '<input type="text" name="stageMaxScore['.$thisStageID.']" id="stageMaxScore['.$thisStageID.']" value="'.$_stageMaxScore.'" class="input_num" /><input type="hidden" name="MarkCriteriaID[]" id="MarkCriteriaID[]" value="'.$_tempCriteriaID.'">';
					
					$this_content .= '
									<tr>';
					$this_content .= '
									
									  <td>'.$_criteriaStatusCheckBox.'</td>
			                        
					';
									
					$this_content .= '				
			                          <td>'.$_criteriaTitle.'<input type="hidden" name="stageCriteriaID[]" id="stageCriteriaID[]" value="'.$_stageCriteriaID.'" /></td>
			                          <td><input type="text" name="stageCriteriaWeight['.$_stageCriteriaID.']" id="stageCriteriaWeight'.($k+1).'" value="'.$_criteriaWeight.'" class="input_num" /></td>
			                          <td><input type="text" name="stageCriteriaMaxScore['.$_stageCriteriaID.']" id="stageCriteriaMaxScore'.($k+1).'" value="'.$_criteriaMaxScore.'" '.$_criteriaMaxScoreReadonly.' class="input_num" /> '.$_criteriaMaxScoreNoteReadonly.'</td>
			                          <td>
			                          	'.$h_rubric_set.'
			                          </td>';
			        
					if($thisStageID!="") {
						$this_content .= '
									  <td><span class="table_row_tool">'.$h_rubric_edit.' '.$h_rubric_delete.'</span></td>';
					} else {
						$this_content .= '<td>&nbsp;</td>';	
					}
					$this_content .= '</tr>';
				}
				
			}else{
				if($viewOnly) {
					$this_content .= '
			                        <tr>
			                          <td>'.$_criteriaTitle.'</td>
			                          <td>'.$_criteriaWeight.'</td>
			                          <td>'.$_criteriaMaxScore.$_criteriaMaxScoreNoteReadonly.'</td>
			                          <td>
			                          	'.$h_rubric_set.'
			                          </td>
									
									  <td>&nbsp;</td>
			                        </tr>
					';
				} else {
					$this_content .= '
			                        <tr>';
					$this_content .= '
									
									  <td>'.$_criteriaStatusCheckBox.'</td>
			                       
					';			                        
			        $this_content .= '                
			                          <td>'.$_criteriaTitle.'<input type="hidden" name="stageCriteriaID[]" id="stageCriteriaID[]" value="'.$_stageCriteriaID.'" /></td>
			                          <td><input type="text" name="stageCriteriaWeight['.$_stageCriteriaID.']" id="stageCriteriaWeight'.($k+1).'" value="'.$_criteriaWeight.'" class="input_num" /></td>
			                          <td><input type="text" name="stageCriteriaMaxScore['.$_stageCriteriaID.']" id="stageCriteriaMaxScore'.($k+1).'" value="'.$_criteriaMaxScore.'" '.$_criteriaMaxScoreReadonly.' class="input_num" /> '.$_criteriaMaxScoreNoteReadonly.'</td>
			                          <td>
			                          	'.$h_rubric_set.'
			                          </td>';
			        
					if($thisStageID!="") {
						$this_content .= '
									  <td><span class="table_row_tool">'.$h_rubric_edit.' '.$h_rubric_delete.'</span></td>';
					} else {
						$this_content .= '<td>&nbsp;</td>';	
					}
					$this_content .= '</tr>';
				}
			}
			
		}
		//debug_pr($withCriteriaSetting);
		if(!in_array(1,$withCriteriaSetting)) {
			$this_content .= '<tr><td colspan="100%" align="center" height="40"><div align="center">'.$Lang['SBA']['NoCriteriaSettingInUse'].'</div></td></tr>';
		}
		
		$this_content .= '</tbody>
						</table>';
		
		
		$ratio_content .= '
				<tr><td>'.$Lang['IES']['Weight'].'</td>
                    <td>:</td><td>
					'.$thisDisplayStageWeight.'
					</td></tr>
				<tr><td>'.$Lang['IES']['MaxScore'].'</td>
                    <td>:</td><td>
					'.$thisDisplayStageMaxScore.'
					</td></tr>
		';
		
		$returnArray = array($this_content,$ratio_content);
		
		return $returnArray;
	}
	
	function getNavigation($nav_ary=array()){
		global $sba_cfg;
		
		$html = "";
		
		if(is_array($nav_ary) && count($nav_ary)){
			foreach($nav_ary as $item_ary){
				$Caption = $item_ary[$sba_cfg['SBA_NAVIGATION_ARY']['Caption']];
				$Link 	 = $item_ary[$sba_cfg['SBA_NAVIGATION_ARY']['Link']];
				
				$html .= $html? " &gt; ":"";
				
				if($Link){
					$html .= "<a href='$Link'>$Caption</a>";
				}
				else{
					$html .= "<span>$Caption</span>";
				}
			}
		}
		
		return $html;
	}
}
?>