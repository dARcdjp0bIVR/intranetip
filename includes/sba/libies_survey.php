<?php
/** [Modification Log] Modifying By :
 * *******************************************
 * *******************************************
 * 2020-06-16 (Pun) [ip.2.5.11.7.1] [187053]
 *  - Modified HighChartExportPngJSFunctions(), fix cannot export highchart for https
 *
 * 2019-05-06 Isaac
 * added drawHighChart_Pie(), drawHighChart_VerticalBar(), getCombineHighChart(), ansToHighChartData(), HighChartexportPngJSFunctions()
 * modified GetIndividualAnswerDisplay() added compareQ2 to id,genTableWithStyle() added compareQ2 to id
 *
 * 2012-08-17 Mick
 * 	New functions for generating csv and copying graph files, see exportMappingFiles() and exportQuestionFiles()
 * -	Modified function genTableWithStyle(), Display Question title input box if focus question is checked
 *
 * 2011-01-11	Thomas	(201101111418)
 * -	Add var $display_survey_mapping_option
 * -	Add function set_display_survey_mapping_option(), get_display_survey_mapping_option()
 * *******************************************
 */

include_once($intranet_root."/includes/sba/libSbaExport.php");

class libies_survey extends libies {

	var $display_survey_mapping_option = true;


	public function analyzeXandY($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		global $ies_cfg;
		$xType = $ParQuestionElements[$ParXQuestionNumber]["TYPE"];
		$yType = $ParQuestionElements[$ParYQuestionNumber]["TYPE"];
		if ($xType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
			if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
				$returnArray = $this->analyzeMC_vs_MC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
				$returnArray = $this->analyzeMC_vs_MultiMC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
				$returnArray = $this->analyzeMC_vs_LC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			}
		} else if ($xType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
			if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
				$returnArray = $this->analyzeMultiMC_vs_MC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
				$returnArray = $this->analyzeMultiMC_vs_MultiMC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
				$returnArray = $this->analyzeMultiMC_vs_LC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			}
		} else if ($xType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
			if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
				$returnArray = $this->analyzeLC_vs_MC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
				$returnArray = $this->analyzeLC_vs_MultiMC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
				$returnArray = $this->analyzeLC_vs_LC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			}
		}
		return $returnArray;
	}

	public function analyzeXandY2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		global $ies_cfg;
		$x_questionArray = explode(".",$ParXQuestionNumber);
		$x_question_question = $x_questionArray[0];
		$y_questionArray = explode(".",$ParYQuestionNumber);
		$y_question_question = $y_questionArray[0];



		$xType = $ParQuestionElements[$x_question_question]["TYPE"];
		$yType = $ParQuestionElements[$y_question_question]["TYPE"];
//		////debug_r($xType);
//		////debug_r($yType);
		if ($xType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
			if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
				$returnArray = $this->analyzeMC_vs_MC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
				$returnArray = $this->analyzeMC_vs_MultiMC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
				$returnArray = $this->analyzeMC_vs_LC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			}
		} else if ($xType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
			if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
				$returnArray = $this->analyzeMultiMC_vs_MC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
				$returnArray = $this->analyzeMultiMC_vs_MultiMC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
				$returnArray = $this->analyzeMultiMC_vs_LC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			}
		} else if ($xType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
			if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
				$returnArray = $this->analyzeLC_vs_MC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
				$returnArray = $this->analyzeLC_vs_MultiMC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
				$returnArray = $this->analyzeLC_vs_LC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
//				$returnArray = $this->analyzeLC_vs_LC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			}
		}
		return $returnArray;
	}

	public function analyzeXorY2($ParTableName,$ParQuestionNumber,$ParQuestionElements) {
  		global $ies_cfg;
//  		////debug_r($ParQuestionElements);
//  		echo "over here";
		switch((int)$ParQuestionElements[$ParQuestionNumber]["TYPE"]) {
			case $ies_cfg["Questionnaire"]["QuestionType"]["MC"]:
				$returnArray = $this->analyzeMC2($ParTableName,$ParQuestionNumber,$ParQuestionElements);

				break;
			case $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]:
				$returnArray = $this->analyzeMultiMC2($ParTableName,$ParQuestionNumber,$ParQuestionElements);

				break;
			# this case wont happen since the LS will be tuned as MC to process
//			case $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]:
//				$returnArray = $this->analyzeLC2($ParTableName,$ParQuestionNumber,$ParQuestionElements);
//				break;
		}


  		return $returnArray;
	}

	public function analyzeMC2($ParTableName,$ParXQuestionNumber,$ParQuestionElements) {
//		$options = $ParQuestionElements[$ParQuestionNumber]["OPTIONS"];
////		////debug_r($options);
//		if (is_array($options)) {
//			$resultArray = array();
//			foreach($options as $optionsKey => $optionsElement) {
//				$sql = "SELECT '[".$optionsElement."]' AS OPT_POS,COUNT(*) AS VALUE FROM $ParTableName WHERE INSTR(Q_$ParQuestionNumber,'[".$optionsElement."]')";
////				////debug_r($sql);
//				$resultArray = array_merge($resultArray,$this->returnArray($sql));
//			}
//		}
//		$returnArray = $resultArray;
//		return $returnArray;
////debug_r($ParQuestionElements[$ParXQuestionNumber]["OPTIONS"]);
		$X_options = $ParQuestionElements[$ParXQuestionNumber]["OPTIONS"];

		if (is_array($X_options)) {
			$resultArray = array();
			foreach($X_options as $X_optionsKey => $X_optionsElement) {
				$sql = "SELECT '[$X_optionsElement]' AS OPT_POS_1,COUNT(*) AS VALUE FROM $ParTableName WHERE INSTR(Q_1,'[$X_optionsElement]')";
				$resultArray = array_merge($resultArray,$this->returnArray($sql));
			}
		}
		$returnArray = $resultArray;
		return $returnArray;
	}
	public function analyzeMultiMC2($ParTableName,$ParQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC2($ParTableName,$ParQuestionNumber,$ParQuestionElements);
	}
	public function analyzeLC2($ParTableName,$ParQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC2($ParTableName,$ParQuestionNumber,$ParQuestionElements);
	}
	public function analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		$X_options = $ParQuestionElements[$ParXQuestionNumber]["OPTIONS"];
		$Y_options = $ParQuestionElements[$ParYQuestionNumber]["OPTIONS"];

		if (is_array($X_options)) {
			$resultArray = array();
			foreach($X_options as $X_optionsKey => $X_optionsElement) {
				if (is_array($Y_options)) {
					foreach($Y_options as $Y_optionsKey => $Y_optionsElement) {
						$sql = "SELECT '[$X_optionsElement]' AS OPT_POS_1,'[$Y_optionsElement]'OPT_POS_2,COUNT(*) AS VALUE FROM $ParTableName WHERE ( INSTR(Q_1,'[$X_optionsElement]') AND INSTR(Q_2,'[$Y_optionsElement]') ) ";
						$resultArray = array_merge($resultArray,$this->returnArray($sql));
					}
				}
			}
		}
		$returnArray = $resultArray;
		return $returnArray;
	}
	public function analyzeMC_vs_MultiMC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function analyzeMC_vs_LC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function analyzeMultiMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function analyzeMultiMC_vs_MultiMC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function analyzeMultiMC_vs_LC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function analyzeLC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function analyzeLC_vs_MultiMC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function analyzeLC_vs_LC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function generateTableXandY($ParResultArray) {
		if (is_array($ParResultArray)) {
			foreach($ParResultArray as $ParResultArrayKey => $ParResultArrayElement) {
				$header[$ParResultArrayElement["OPT_POS_1"]] = $this->trimOptions($ParResultArrayElement["OPT_POS_1"]);;
				if (isset($tableContent[$ParResultArrayElement["OPT_POS_2"]])) {
					$tableContent[$ParResultArrayElement["OPT_POS_2"]][] = $ParResultArrayElement["VALUE"];
				} else {
					$tableContent[$ParResultArrayElement["OPT_POS_2"]][] = $this->trimOptions($ParResultArrayElement["OPT_POS_2"]);
					$tableContent[$ParResultArrayElement["OPT_POS_2"]][] = $ParResultArrayElement["VALUE"];
				}

			}
		}
		$content = "";
		if (is_array($tableContent)) {
			foreach($tableContent as $tableContentKey => $tableContentElement) {
				$sizeOfTableContentElement = count($tableContentElement);
				$content .= "<tr>";
				for($i=0;$i<$sizeOfTableContentElement;$i++) {
					if ($i>0) {
						$content .= "<td>".$tableContentElement[$i]."</td>";
					} else {
						$content .= "<th class='side_hd'>".$tableContentElement[$i]."</th>";
					}
				}
				$content .= "</tr>";
			}
		}
		$html .= "
			<table class='common_table_list'><tr><th><!--TITLE--></th><th class='top_hd'>".(is_array($header)?implode("</th><th class='top_hd'>",$header):"")."</th></tr>".$content."</table>
		";
		return $html;
	}
	public function generateTableXorY($ParResultArray) {
		////debug_r($ParResultArray);
			if (is_array($ParResultArray)) {
				$tableContent = array();
				foreach($ParResultArray as $key => $element) {
					$header[] = $this->trimOptions($element["OPT_POS"]);
					$tableContent[] = $element["VALUE"];
				}
			}
			$html .= "
				<table border=1><tr><th>".implode("</th><th>",$header)."</th></tr><tr><td>".implode("</td><td>",$tableContent)."</td></tr></table>
			";
		return $html;
	}
	public function trimOptions($ParOption) {
		////debug_r($ParOption);
		return substr($ParOption,strpos($ParOption,"[")+1,strpos($ParOption,"]")-strpos($ParOption,"[")-1);
	}
	public function returnTableHeaderArray($ParAxisNum,$ParQuestionTitle,$ParQuestionOptions=null) {
		if (count($ParQuestionOptions[$ParAxisNum])>0) {
			$headerArray = $ParQuestionOptions[$ParAxisNum];
		} else {
			$headerArray = $ParQuestionTitle[$ParAxisNum];
		}
		return $headerArray;
	}
	public function getSurveyQuestion($ParSurveyID) {

		global $ies_cfg,$intranet_db;
		$sql = "SELECT QUESTION FROM {$intranet_db}.IES_SURVEY WHERE SURVEYID = '$ParSurveyID' AND SURVEYTYPE = ".$ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0];
		$returnResult = current($this->returnVector($sql));
		return $returnResult;
	}
	public function getSurveyAnswer($ParSurveyID,$ParIsValid=null) {
		global $ies_cfg,$intranet_db;
		if ($ParIsValid === true) {
			$checkValidCond = " AND ISA.STATUS = ".$ies_cfg['DB_IES_SURVEY_ANSWER_status']['Valid'];
		} else if ($ParIsValid === false) {
			$checkValidCond = " AND ISA.STATUS = ".$ies_cfg['DB_IES_SURVEY_ANSWER_status']['Invalid'];
		} else {
			$checkValidCond = "";
		}
		$sql = "SELECT ISA.ANSWER FROM {$intranet_db}.IES_SURVEY_ANSWER ISA
				INNER JOIN {$intranet_db}.IES_SURVEY ISU
					ON ISA.SURVEYID = ISU.SURVEYID
				WHERE ISU.SURVEYID = '$ParSurveyID'
					$checkValidCond";

		$returnResult = $this->returnVector($sql);
		return $returnResult;
	}
	public function getQuestionnaireTags($ParCurrentTag,$ParIsIESStudent) {

		$currentTag1 = $currentTag2 = $currentTag3 = "";
		${"currentTag".$ParCurrentTag} = " class=\"current\" ";
		if ($ParIsIESStudent) {
			$groupingTag = "<li $currentTag2><a href=\"#\" onClick=\"goGrouping()\">Grouping</a></li>";
		} else {
			// do nothing
		}
		$html = <<<HTMLEND
		    <ul class="q_tab">
		      <li $currentTag1><a href="#" onClick="goDisplay()">Overall</a></li>
		      {$groupingTag}
		      <li $currentTag3><a href="#" onClick="goGroupingList()">Grouping List</a></li>
		    </ul>
HTMLEND;
		return $html;
	}
	public function getSelectQuestionBox($ParSurveyID,$ParName="",$ParId="",$ParSelected=null,$ParShowNoCombine=false,$ParOnChangeAction="",$ParXAxis=null) {
		global $Lang,$ies_cfg;
//		error_log("getSelectQuestionBox -->".$ParSurveyID."\n", 3, "/tmp/aaa.txt");

		################################################
		##	Get the question elements
		$question = $this->getSurveyQuestion($ParSurveyID);	// This is the raw question
		$questionElements = $this->breakQuestionsString($question);
		################################################

		################################################
		##	Cannot do self mapping
		$ParSkipQuestionArray = array();
		$ParSkipQuestionArray[] = $ParXAxis;
		################################################

		################################################
		##	Construct options
		if (is_array($questionElements)) {
			$options = array();
			if ($ParShowNoCombine) {
				$options[] = "<option value=''>".$Lang['IES']['NoCombination']."</option>";
			}
			foreach($questionElements as $questionElementsKey => $questionElementsElement) {
				# Handle cannot map item
				#	- 1. those already put in $ParSkipQuestionArray
				#	- 2. question type that not available for analyze
				if (in_array($questionElementsKey,$ParSkipQuestionArray)
					|| !$this->isTypeForAnalyze($questionElementsElement["TYPE"])) {
					$options[] = "<optgroup label='".$questionElementsKey." ".$questionElementsElement["TITLE"]."' style='color:grey;'></optgroup>";
				} else {
					if ($questionElementsElement["TYPE"]==$ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
						$subquestion = $questionElementsElement["QUESTION_QUESTIONS"];
						$options[] = "<optgroup label='".$questionElementsKey." ".$questionElementsElement["TITLE"]."'>";
						if (is_array($subquestion)) {
							foreach($subquestion as $subquestionKey => $subquestionElement) {
								# Handle the selected item
								$subquestionNumber = $questionElementsKey.".".($subquestionKey+1);
								$selectedStr = "";
								if ($ParSelected) {
									if ($subquestionNumber==$ParSelected) {
										$selectedStr = " selected='selected' ";
									}
								}
								if (in_array($subquestionNumber,$ParSkipQuestionArray)) {
									$options[] = "<optgroup label='".$subquestionNumber." ".$subquestionElement."'></optgroup>";
								} else {
									$options[] = "<option value='".$subquestionNumber."' $selectedStr>".$subquestionNumber." ".$subquestionElement."</option>";
								}

							}
						}
						$options[] = "</optgroup>";
					} else {
						# Handle the selected item
						$selectedStr = "";
						if ($ParSelected) {
							if (($questionElementsKey)==$ParSelected) {
								$selectedStr = " selected='selected' ";
							}
						}
						$options[] = "<option value='".$questionElementsKey."' $selectedStr>".$questionElementsKey." ".$questionElementsElement["TITLE"]."</option>";
					}

				}

			}
		}
		################################################

		################################################
		##	Last padding for selection
		if (count($options)>0) {
			$returnStr = "<select name='$ParName' id='$ParId' onChange='".$ParOnChangeAction."'>".implode("",$options)."</select>";
		} else {
			$returnStr = $Lang['IES']['NoRecordAtThisMoment'];
		}
		################################################
		return $returnStr;
	}

	public function getSurveyQuestions($ParSurveyID, $ParIsFilterQues=false) {
		global $ies_cfg;
		$surveyDetails = $this->getSurveyDetails($ParSurveyID);
		$surveyDetails = current($surveyDetails);
		$questions = array();
		if (is_array($surveyDetails)) {
			$questionElements = $this->breakQuestionsString($surveyDetails["Question"]);
			$sizeOfQuestionElements = count($questionElements);
			for($i=1;$i<=$sizeOfQuestionElements;$i++) {
				$questionElement = $questionElements[$i];
//				if ($ParIsFilterQues) {
//					if (!$this->isTypeForAnalyze($questionElement["TYPE"])) {
//						continue;
//					}
//				}
				$questions[$i] = $i.". ".$questionElement["TITLE"];
			}

		}
		return $questions;
	}

	function saveSurveyMapping($ParXAxis,$ParYAxis,$ParMappingTitle,$ParXTitle=null,$ParYTitle=null,$ParSurveyID,$ParUserID) {
		global $intranet_db;

		if (!empty($ParXAxis)) {
			$mappingCondX .= " XMAPPING=$ParXAxis ";
		} else {
			$mappingCondX .= " XMAPPING=NULL ";
		}

		if (!empty($ParYAxis)) {
			$mappingCondY .= " YMAPPING=$ParYAxis ";
		} else {
			$mappingCondY .= " YMAPPING=NULL ";
		}

		$sql = "SELECT SURVEYMAPPINGID FROM {$intranet_db}.IES_SURVEY_MAPPING WHERE SURVEYID='$ParSurveyID' AND USERID='$ParUserID' AND ".str_replace("=NULL"," IS NULL ",$mappingCondX)." AND ".str_replace("=NULL"," IS NULL ",$mappingCondY);
//		////debug_r($sql);

		$surveyMappingID = current($this->returnVector($sql));

		$setCond .= " MAPPINGTITLE='$ParMappingTitle' ";
		if (!empty($ParXTitle)) {
			$setCond .= ",XTITLE='$ParXTitle' ";
		}
		if (!empty($ParYTitle)) {
			$setCond .= ",YTITLE='$ParYTitle' ";
		}
		if (!empty($surveyMappingID)) {
			$sql = "UPDATE {$intranet_db}.IES_SURVEY_MAPPING SET $setCond WHERE SURVEYMAPPINGID = '$surveyMappingID'";
		} else {
			$sql = "INSERT INTO {$intranet_db}.IES_SURVEY_MAPPING SET $setCond, $mappingCondX,$mappingCondY,SURVEYID='$ParSurveyID',USERID='$ParUserID',DATEINPUT = NOW() ";

		}
//		////debug_r($sql);
		$result = $this->db_db_query($sql);
		if ($result) {
			if (!empty($surveyMappingID)) {
				$result = $surveyMappingID;
			} else {
				$result = mysql_insert_id();
			}
		}
		return $result;
	}

	public function loadSurveyMapping($ParSurveyID,$ParUserID,$ParSurveyMappingID=null) {
		global $intranet_db;
		if (!empty($ParSurveyMappingID)) {
			$cond = " SURVEYMAPPINGID = $ParSurveyMappingID ";
		} else {
			$cond = " SURVEYID = '$ParSurveyID' AND USERID = '$ParUserID' ";
		}


		$sql = "SELECT SURVEYMAPPINGID,SURVEYID,USERID,XMAPPING,YMAPPING,XTITLE,YTITLE,DATEINPUT,DATEMODIFIED,MAPPINGTITLE,COMMENT,SEQUENCE
				FROM {$intranet_db}.IES_SURVEY_MAPPING
				WHERE $cond ORDER BY SEQUENCE";
//error_log($sql."\n", 3, "/tmp/aaa.txt");

		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}

	public function breakQuestionsString($ParQuestionsString) {
		global $ies_cfg;
		$questionsArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Question"],$ParQuestionsString);
		$questionElements=array();
		array_shift($questionsArray);
		if (is_array($questionsArray)) {
			foreach($questionsArray as $questionsArrayKey => $questionsArrayElement) {
				$questionNumber = $questionsArrayKey+1;
				$questionElements[$questionNumber] = libies::extractQuestionElement($questionsArrayElement);
			}
		}
		return $questionElements;
	}

	public function isTypeForAnalyze($ParType) {
		global $ies_cfg;
		return in_array($ParType,$ies_cfg["Questionnaire"]["ValidTypeForAnalyze"]);
	}


	public function getAnalysisResults2($ParQuestionElements,$ParBrokenAnswerArray, $ParXAxis,$ParYAxis="") {
		//debug_r($ParXAxis);
		//debug_r($ParYAxis);

		if (!empty($ParXAxis)||!empty($ParYAxis)) {
			$tableName = "temp_survey_answers";
			$answerTableResult = $this->createAnswerTempTable3($tableName,$ParQuestionElements,$ParBrokenAnswerArray, $ParXAxis, $ParYAxis);
		} else {
			return false;
		}
		$x_result = array();
		$y_result = array();
		$twod_result = array();
		if ($answerTableResult) {
			if (!empty($ParXAxis)) {
				$x_result = $this->analyzeXorY2($tableName,$ParXAxis,$ParQuestionElements);
			}
			if (!empty($ParYAxis)) {
				$y_result = $this->analyzeXorY2($tableName,$ParYAxis,$ParQuestionElements);
			}
			if (!empty($ParXAxis)&&!empty($ParYAxis)) {
				$twod_result = $this->analyzeXandY2($tableName,$ParXAxis,$ParYAxis,$ParQuestionElements);
			}
		}
		$returnArray = array("X_RESULT"=>$x_result,"Y_RESULT"=>$y_result,"2D_RESULT"=>$twod_result);
		return $returnArray;

	}


	/* duplicated in libies for this moment */
	public function createAnswerTempTable3($tableName,$questionsArray,$answer,$ParXAxis,$ParYAxis) {
		global $ies_cfg;
		if (is_array($questionsArray)) {
			$sizeOfQuestionArray = count($questionsArray);
			$fieldStringArray = array();
			$fieldStringArray[] = "Q_1 mediumtext NULL";
			$fieldStringArray[] = "Q_2 mediumtext NULL";
			$fieldString = implode(",",$fieldStringArray);
		}

		$sql = <<<SQLEND
			create temporary table if not exists {$tableName}
			(
			{$fieldString}
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
SQLEND;
		$createResult = $this->db_db_query($sql);
//		////debug_r($sql);

		$sql = <<<SQLEND
		delete from $tableName
SQLEND;
		$this->db_db_query($sql);

		$question_1 = explode(".",$ParXAxis);
		$question_1_question = $question_1[0];
		$question_1_subquestion = $question_1[1];
//		//debug($question_1_subquestion);
		$question_2 = explode(".",$ParYAxis);
		$question_2_question = $question_2[0];
		$question_2_subquestion = $question_2[1];

		if (empty($question_1_subquestion)) {
			$question_1_subquestion = 1;
		}
		if (empty($question_2_subquestion)) {
			$question_2_subquestion = 1;
		}

		$questionElementOptions1 = $questionsArray[$question_1_question]["OPTIONS"];
		$questionElementOptions2 = $questionsArray[$question_2_question]["OPTIONS"];
//		////debug_r($questionsArray);
		if ($createResult) {
//			////debug_r($answer);
			foreach($answer as $answerKey=>$answerElement) {
				$sql_answer1 = "";
				$sql_answer2 = "";
				if($questionsArray[$question_1_question]["TYPE"] == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]
					|| $questionsArray[$question_1_question]["TYPE"] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]){
					$sql_answer1 = "[".$questionElementOptions1[$answerElement[$question_1_question][$question_1_subquestion-1]]."]";
				}
				if($questionsArray[$question_1_question]["TYPE"] == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]){
//					////debug_r($questionElementOptions1);
					for($num = 0; $num < sizeof($answerElement[$question_1_question]); $num++){
//						////debug_r($questionElementOptions2);
						$currentAnswer = $answerElement[$question_1_question][$num];
						if ($currentAnswer) {
							$sql_answer1 .= "[".$questionElementOptions1[$num]."]";
						}
					}
				}
				if($questionsArray[$question_2_question]["TYPE"] == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]
					|| $questionsArray[$question_2_question]["TYPE"] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]){
					$sql_answer2 = "[".$questionElementOptions2[$answerElement[$question_2_question][$question_2_subquestion-1]]."]";
				}
				if($questionsArray[$question_2_question]["TYPE"] == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]){
					for($num = 0; $num < sizeof($answerElement[$question_2_question]); $num++){
//						////debug_r($questionElementOptions2);
						$currentAnswer = $answerElement[$question_2_question][$num];
						if ($currentAnswer) {
							$sql_answer2 .= "[".$questionElementOptions2[$num]."]";
						}

					}
				}
//				$sql_answer1 = $questionElementOptions1[$answerElement[$question_1_question][$question_1_subquestion-1]];
//				$sql_answer2 = $questionElementOptions2[$answerElement[$question_2_question][$question_2_subquestion-1]];
				$insertValueArray[] = "('$sql_answer1','$sql_answer2')";
			}
			if (is_array($insertValueArray) && count($insertValueArray)>0) {
				$sql = "INSERT INTO $tableName VALUES".implode(",",$insertValueArray);
				$insertResult = $this->db_db_query($sql);
			}

		}
		return $createResult;
	}

	public function generateXYSelection($ParXAxis,$ParYAxis,$ParQuestionsElement) {
		if (is_array($ParQuestionsElement)) {
			foreach($ParQuestionsElement as $questionsElementKey => $questionsElementElement) {
				$type = $questionsElementElement["TYPE"];
				$questionTitle = $questionsElementElement["TITLE"];
				$isTypeForAnalyze = $this->isTypeForAnalyze($type);
				if ($isTypeForAnalyze) {
					$x_checked = "";
					if ($ParXAxis==$questionsElementKey) {
						$x_checked = "checked = 'checked' ";
					}
					$x_axis_radio_selection_array[] = "<input type=\"radio\" class=\"radio\" name=\"x_axis\" $x_checked id=\"x_axis_$questionsElementKey\" value=\"$questionsElementKey\" />
													<label for=\"x_axis_$questionsElementKey\">$questionTitle</label>
													<input type=\"hidden\" name=\"x_axis_{$questionsElementKey}_type\" value=\"".$questionElements["TYPE"]."\" />";
					$y_checked = "";
					if ($ParYAxis==$questionsElementKey) {
						$y_checked = "checked = 'checked' ";
					}
					$y_axis_radio_selection_array[] = "<input type=\"radio\" class=\"radio\" name=\"y_axis\" $y_checked id=\"y_axis_$questionsElementKey\" value=\"$questionsElementKey\" />
													<label for=\"y_axis_$questionsElementKey\">$questionTitle</label>
													<input type=\"hidden\" name=\"y_axis_{$questionsElementKey}_type\" value=\"".$questionElements["TYPE"]."\" />";

				}
			}
		}



		if (is_array($x_axis_radio_selection_array) && is_array($y_axis_radio_selection_array)) {
			$X_Selection = implode("<br/>",$x_axis_radio_selection_array);
			$Y_Selection = implode("<br/>",$y_axis_radio_selection_array);;
		}

		$returnArray = array("X_SELECTION"=>$X_Selection,"Y_SELECTION"=>$Y_Selection);
		return $returnArray;
	}

	/*
	* $displayMode = 1  , normal display --> first version
	* $displayMode = 2  , display without a) question selection box
	*/
	public function generateCombineBox($ParSurveyID,$ParCombineNameUse,$ParCombineBoxContent,$ParXAxis,$ParYAxis=null,$ParMappingTitle="",$ParXTitle="",$ParYTitle="", $ParShowNoCombination=false,$displayMode = 1) {
		global $Lang, $linterface;

		if (empty($ParYAxis)) {
			$hiddenAxisTitle = " style='display:none' ";
		}

		##################################################
		##	select_question_combine
		$selectedQuestion = $ParYAxis;
		$showNoCombine = $ParShowNoCombination;
		$name = $ParCombineNameUse."[QuestionNum]";
		$id = $name;
		$onChangeAction="loadCombineBoxContent(this.id)";
		$select_question_combine = $this->getSelectQuestionBox($ParSurveyID,$name,$id,$selectedQuestion,$showNoCombine,$onChangeAction,$ParXAxis);
		##################################################
		$selectHtml = '';
		$removeButtonHtml = '';
if($displayMode == 1){
		$selectHtml = <<<HTMLSELECT
	        <tr><td>{$Lang['IES']['PairUpWith']}</td><td>:</td><td>{$select_question_combine}</td></tr>
HTMLSELECT;
		$removeButtonHtml = <<<HTMLREMOVEBUTTON
		<div class="btn">
		<input name="submit2" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value=" {$Lang['IES']['Delete']} " type="button" onClick="removeBox('{$ParCombineNameUse}')">
        </div>
HTMLREMOVEBUTTON;
}
		$axisTitleBox .= <<<HTMLEND
		<table class="form_table" name="{$ParCombineNameUse}[AxisTitle]" id="{$ParCombineNameUse}[AxisTitle]" {$hiddenAxisTitle}>
            <col class="field_title">
			<col class="field_c">
            <tbody><tr><td><div style="border: 2px solid rgb(204, 204, 204); width: 30px; height: 16px; background-color: rgb(194, 236, 236);"></div></td><td>:</td><td><input name="{$ParCombineNameUse}[XTitle]" id="{$ParCombineNameUse}[XTitle]" type="text" value="{$ParXTitle}"></td></tr>
            <tr><td><div style="border: 2px solid rgb(204, 204, 204); width: 30px; height: 16px; background-color: rgb(223, 249, 182);"></div></td><td>:</td><td><input name="{$ParCombineNameUse}[YTitle]" id="{$ParCombineNameUse}[YTitle]" type="text" value="{$ParYTitle}"></td></tr>
            </tbody>
        </table>
HTMLEND;
		$questionNum = "";
		$html = <<<HTMLEND
	<div class="ies_q_box q_group" id="{$ParCombineNameUse}">
        <div class="q_title">
	        <table class="form_table">
	        <col class="field_title">
			<col class="field_c">
	        <tbody><tr><td>{$Lang['IES']['PairupGroupItem']}</td><td>:</td><td><input name="{$ParCombineNameUse}[Title]" id="{$ParCombineNameUse}_Title" value="{$ParMappingTitle}" class="textbox combinationInfoTitle" type="text"></td></tr>
			{$selectHtml}
	        </tbody></table>
	        <br />
	    </div>


        <div  name="{$ParCombineNameUse}[Content]" id="{$ParCombineNameUse}[Content]">
        	<div class="q_option">
	        	{$ParCombineBoxContent}
			</div>
		</div>
		{$axisTitleBox}

			<span style="text-align:center;"><input type = "button" name="cancelSelect" value="{$Lang['IES']['SurveyTable']['string26']}" onClick="resetCombineButton()"></span>
        {$removeButtonHtml}


	</div>
HTMLEND;
		return $html;
	}

	public function getCombineBoxContent($ParSurveyID,$ParXAxis,$ParYAxis="",$ParCombineNameUse="") {
		global $Lang;

//error_log("-->getCombineBoxContent ParXAxis -- ".$ParXAxis." ParYAxis -- ".$ParYAxis."\n", 3, "/tmp/aaa.txt");

		$question = $this->getSurveyQuestion($ParSurveyID);
		$questionsElement = $this->breakQuestionsString($question);
		$og_questionsElement = $questionsElement;
		$IS_ANSWER_VALID = true;
		//debug_pr($questionsElement);
		$answer = $this->getSurveyAnswer($ParSurveyID,$IS_ANSWER_VALID);
		$broken_answer = $this->splitAnswer($answer);
		$refinedQuestionsAnswer = $this->getRefinedQuestionsAndAnswerForLS($questionsElement,$answer,$ParXAxis,$ParYAxis);
		$questionsElement = $refinedQuestionsAnswer["QUESTIONS_ARRAY"];
		$answer = $refinedQuestionsAnswer["ANSWER"];

		if (empty($refinedQuestionsAnswer["Y_AXIS"])) {
			$resultTable = $this->getPlainTable($refinedQuestionsAnswer["QUESTIONS_ARRAY"], $refinedQuestionsAnswer["ANSWER"], $refinedQuestionsAnswer["X_AXIS"]);
		} else {
			$analysisResults = $this->getAnalysisResults2($og_questionsElement,$broken_answer, $ParXAxis,$ParYAxis);
			//debug_pr($analysisResults['2D_RESULT']);
			# with subquestions (e.g.likert scale)
			if (isset($og_questionsElement[$refinedQuestionsAnswer["X_AXIS"]]["QUESTION_NUM"])) {
				$x_title = $og_questionsElement[$refinedQuestionsAnswer["X_AXIS"]]["QUESTION_QUESTIONS"][$refinedQuestionsAnswer["X_SUBQUESTION_NUMBER"]-1];

			# without subquestions (e.g.MC,MMC)
			} else {
				$x_title = $og_questionsElement[$refinedQuestionsAnswer["X_AXIS"]]["TITLE"];
			}
			# with subquestions (e.g.likert scale)
			if (isset($og_questionsElement[$refinedQuestionsAnswer["Y_AXIS"]]["QUESTION_NUM"])) {
				$y_title = $og_questionsElement[$refinedQuestionsAnswer["Y_AXIS"]]["QUESTION_QUESTIONS"][$refinedQuestionsAnswer["Y_SUBQUESTION_NUMBER"]-1];

			# without subquestions (e.g.MC,MMC)
			} else {
				$y_title = $og_questionsElement[$refinedQuestionsAnswer["Y_AXIS"]]["TITLE"];
			}


			$resultTable .= "<div>".$Lang['IES']['TableBelowShowResultOfQuestion'].":</div>";
		    $resultTable .= "<br style=\"clear:both;\" />";

			$title = "<font color=\"black\">{$y_title}/{$x_title}</font>";
			$resultTable .= str_replace("<!--TITLE-->" , $title , $this->generateTableXandY($analysisResults["2D_RESULT"]))."<br />";


		}

		return $resultTable;
	}

	function AnswerStringParse($ParXAxis, $ParYAxis, $ParAnswer, $ParQuestion){
		global $ies_cfg;
		$returnArray = array();
		////debug("IN");
		//////debug_r($ParQuestion);
		for($i = 0; $i < $ParQuestion[$ParXAxis]['QUESTION_NUM']; $i++){
				if($ParYAxis == -1){
					$AnswerArray = array();
					for($m = 0; $m < sizeof($ParAnswer); $m++){
						$answerArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer"],$ParAnswer[$m]);
						//////debug_r($answerArray);
						if($ParQuestion[$ParXAxis]['TYPE'] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"])
						{
							$allanswer = explode(",", $answerArray[$ParXAxis]);
							$answerArray[$ParXAxis] = $allanswer[$i];
						}

						$AnsString = "";
						for($k = 1; $k < sizeof($answerArray); $k++){
							$AnsString .= $ies_cfg["Questionnaire"]["Delimiter"]["Answer"].$answerArray[$k];
						}
						$AnswerArray[] = $AnsString;
					}
					$returnArray[$i][0] = $AnswerArray;
				}
				else{
					for($j = 0; $j < $ParQuestion[$ParYAxis]['QUESTION_NUM']; $j++){
						$AnswerArray = array();
						for($m = 0; $m < sizeof($ParAnswer); $m++){
							$answerArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer"],$ParAnswer[$m]);
							//////debug_r($answerArray);
							if($ParQuestion[$ParXAxis]['TYPE'] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"])
							{
								$allanswer = explode(",", $answerArray[$ParXAxis]);
								$answerArray[$ParXAxis] = $allanswer[$i];
							}
							if($ParQuestion[$ParYAxis]['TYPE'] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"])
							{
								$allanswer = explode(",", $answerArray[$ParYAxis]);
								$answerArray[$ParYAxis] = $allanswer[$j];
							}
							$AnsString = "";
							for($k = 1; $k < sizeof($answerArray); $k++){
								$AnsString .= $ies_cfg["Questionnaire"]["Delimiter"]["Answer"].$answerArray[$k];
							}
							$AnswerArray[] = $AnsString;
						}
						$returnArray[$i][$j] = $AnswerArray;
						//echo "i:$i;j:$j";
					}
				}
		}
		//////debug_r($returnArray);
		return $returnArray;
	}
	function QuestionStringParse($ParXAxis, $ParYAxis, $ParQuestion){
		global $ies_cfg;
		$returnArray = array();
		//////debug_r($ParQuestion);
		for($i = 0; $i < $ParQuestion[$ParXAxis]['QUESTION_NUM']; $i++){
			if($ParYAxis == -1){
				$temp_ques = $ParQuestion;
				if($ParQuestion[$ParXAxis]['TYPE'] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"])
				{
					$temp_ques[$ParXAxis]["TITLE"] = $ParQuestion[$ParXAxis]["QUESTION_QUESTIONS"][$i];
					$temp_ques[$ParXAxis]["TYPE"] = $ies_cfg["Questionnaire"]["QuestionType"]["MC"];

				}

				$returnArray[$i][0] = $temp_ques;
			}
			else{
				for($j = 0; $j < $ParQuestion[$ParYAxis]['QUESTION_NUM']; $j++){
					$temp_ques = $ParQuestion;
					if($ParQuestion[$ParXAxis]['TYPE'] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"])
					{
						$temp_ques[$ParXAxis]["TITLE"] = $ParQuestion[$ParXAxis]["QUESTION_QUESTIONS"][$i];
						$temp_ques[$ParXAxis]["TYPE"] = $ies_cfg["Questionnaire"]["QuestionType"]["MC"];

					}
					if($ParQuestion[$ParYAxis]['TYPE'] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"])
					{
						$temp_ques[$ParYAxis]["TITLE"] = $ParQuestion[$ParYAxis]["QUESTION_QUESTIONS"][$j];
						$temp_ques[$ParYAxis]["TYPE"] = $ies_cfg["Questionnaire"]["QuestionType"]["MC"];
					}
					$returnArray[$i][$j] = $temp_ques;
					//echo "i:$i;j:$j";
				}
			}
		}
		//////debug_r($returnArray);
		return $returnArray;
	}


	public function generateMappingTables($ParSurveyID, $ParStudentID,$ParDefaultXAxis) {
		$SurveyMapping = $this->loadSurveyMapping($ParSurveyID,$ParStudentID);
		if (is_array($SurveyMapping)) {
			$question = $this->getSurveyQuestion($ParSurveyID);
			$IS_ANSWER_VALID = true;
			$answer = $this->getSurveyAnswer($ParSurveyID,$IS_ANSWER_VALID);
			$questionsElement = $this->breakQuestionsString($question);


			foreach($SurveyMapping as $SurveyMappingKey => $SurveyMappingElement) {
				if ($SurveyMappingElement["XMAPPING"]==$ParDefaultXAxis) {
					$x_axis = $SurveyMappingElement["XMAPPING"];
					$y_axis = $SurveyMappingElement["YMAPPING"];
					if (!empty($x_axis) && empty($y_axis)) continue;
					$title = $SurveyMappingElement["MAPPINGTITLE"];
					$x_title = $SurveyMappingElement["XTITLE"];
					$y_title = $SurveyMappingElement["YTITLE"];
					$isNewTable = false;
					$combineNameUse = $this->getCombineNameUse($isNewTable,$SurveyMappingElement["SURVEYMAPPINGID"]);
					$combineBoxContent = $this->getCombineBoxContent($ParSurveyID,$x_axis,$y_axis,$combineNameUse);
//					error_log(" --> x_axis -->".$x_axis." y_axis --> ".$y_axis." title --> ".$title." x_title --> ".$x_title." y_title --> ".$y_title."\n", 3, "/tmp/aaa.txt");

					$html .= $this->generateCombineBox($ParSurveyID,$combineNameUse,$combineBoxContent,$x_axis,$y_axis,$title,$x_title,$y_title);
				}
			}
		}
		return $html;
	}

	public function getCombineNameUse($ParIsNewTable,$ParQuestionID) {
		if ($ParIsNewTable) {
			$type = "new";
		} else {
			$type = "old";
		}
		return "combinationInfo[$type][$ParQuestionID]";
	}
	public function saveMappings($ParXAxis,$ParCombinationInfo,$ParSurveyID,$ParStudentID,$ParDeleteMode = 1) {

		$oldCombinations = $ParCombinationInfo["old"];
		$newCombinations = $ParCombinationInfo["new"];
		$CurrentUpdateMappingIds = array();
		if (is_array($oldCombinations)) {
			foreach($oldCombinations as $SurveyMappingID => $oldCombinationsElement) {
				$CurrentUpdateMappingIds[] = (int)$SurveyMappingID;
				$y_axis = $oldCombinationsElement["QuestionNum"];
				$x_title = $oldCombinationsElement["XTitle"];
				$y_title = $oldCombinationsElement["YTitle"];
				$title = $oldCombinationsElement["Title"];
				$result[] = $this->updateSurveyMapping($SurveyMappingID,$ParXAxis,$y_axis,$x_title,$y_title,$title);
			}
		}

		$SurveyMappingIds = array();
		$SurveyMappingIds = $this->getSurveyMappingIdsWithSameX($ParSurveyID,$ParStudentID,$ParXAxis);
		$RecordToRemove = array_diff($SurveyMappingIds,$CurrentUpdateMappingIds);

		if($ParDeleteMode){
			if (count($RecordToRemove)>0) {
				$result[] = $this->removeSurveyMapping($RecordToRemove);
			}
		}

		if (is_array($newCombinations)) {

			foreach($newCombinations as $newCombinationsKey => $newCombinationsElement) {
				$y_axis = $newCombinationsElement["QuestionNum"];
				$x_title = $newCombinationsElement["XTitle"];
				$y_title = $newCombinationsElement["YTitle"];
				$title = $newCombinationsElement["Title"];
				$result[] = $this->newSurveyMapping($ParSurveyID,$ParStudentID,$ParXAxis,$y_axis,$x_title,$y_title,$title);
			}
		}

		return $result;
	}

	public function IsSurveyMappingExist($ParSurveyID,$ParStudentID,$ParXAxis,$ParYAxis) {
		global $intranet_db;
		if (empty($ParXAxis)) {
			$ParXAxis = "NULL";
		}
		if (empty($ParYAxis)) {
			$ParYAxis = "NULL";
		}
		$sql = "SELECT SURVEYMAPPINGID FROM {$intranet_db}.IES_SURVEY_MAPPING WHERE SURVEYID = '$ParSurveyID' AND USERID = '$ParStudentID' AND XMAPPING = '$XAxis' AND YMAPPING = '$YAxis'";
		$returnArray = $this->returnArray($sql);
		if (count($returnArray)>0) {
			return true;
		} else {
			return false;
		}
	}

	public function removeSurveyMapping($ParSurveyMappingIds) {
		global $intranet_db;
		$sql = "DELETE FROM {$intranet_db}.IES_SURVEY_MAPPING WHERE SURVEYMAPPINGID IN (".implode(",",$ParSurveyMappingIds).")";

//error_log($sql." \nlibies_survey\n", 3, "/tmp/aaa.txt");
		$result = $this->db_db_query($sql);
		return $result;
	}

	public function getSurveyMappingIdsWithSameX($ParSurveyID,$ParStudentID,$ParXAxis) {
		global $intranet_db,$ies_cfg;
		if (empty($ParXAxis)) {
			$ParXAxis = "NULL";
		}
		$sql = "SELECT SURVEYMAPPINGID FROM {$intranet_db}.IES_SURVEY_MAPPING WHERE SURVEYID = '$ParSurveyID' AND USERID = '$ParStudentID' AND XMAPPING = $ParXAxis";
		$returnVector = $this->returnVector($sql);
		return $returnVector;
	}

	public function newSurveyMapping($ParSurveyID,$ParStudentID,$ParXAxis,$ParYAxis,$ParXTitle,$ParYTitle,$ParTitle,$ParComment='') {
		global $intranet_db;
		if (empty($ParXAxis)) {
			$ParXAxis = "NULL";
		}
		else {
			$ParXAxis = "'$ParXAxis'";
		}
		if (empty($ParYAxis)) {
			$ParYAxis = "NULL";
		}
		else {
			$ParYAxis = "'$ParYAxis'";
		}

		if ($ParComment != '')
			$CommentInsert = ", COMMENT = '$ParComment' ";

		$sql = "INSERT INTO {$intranet_db}.IES_SURVEY_MAPPING
				SET SURVEYID = $ParSurveyID,
				USERID = $ParStudentID,
				XMAPPING = $ParXAxis,
				YMAPPING = $ParYAxis,
				XTITLE = '$ParXTitle',
				YTITLE = '$ParYTitle',
				MAPPINGTITLE = '$ParTitle',
				DATEINPUT = NOW(),
				DATEMODIFIED = NOW()
				$CommentInsert
				";
//error_log($sql." \nlibies_survey\n", 3, "/tmp/aaa.txt");
		$result = $this->db_db_query($sql);

		/*
		if($result == false){
			// do nothing
		}else{
			//return the last insert id
			$_lastInsertID = $this->db_insert_id();
			$result = $_lastInsertID;
		}
		*/
		return $result;
	}
	public function updateSurveyMapping($ParSurveyMappingID,$ParXAxis,$ParYAxis,$ParXTitle,$ParYTitle,$ParTitle,$ParComment='') {
		global $intranet_db;
		if (empty($ParXAxis)) {
			$ParXAxis = "NULL";
		}
		else {
			$ParXAxis = "'$ParXAxis'";
		}
		if (empty($ParYAxis)) {
			$ParYAxis = "NULL";
		}
		else {
			$ParYAxis = "'$ParYAxis'";
		}

		if ($ParComment != '')
			$CommentUpdate = ", COMMENT = '$ParComment' ";

		$sql = "UPDATE {$intranet_db}.IES_SURVEY_MAPPING
				SET
				XMAPPING = $ParXAxis,
				YMAPPING = $ParYAxis,
				XTITLE = '$ParXTitle',
				YTITLE = '$ParYTitle',
				MAPPINGTITLE = '$ParTitle',
				DATEMODIFIED = NOW()
				$CommentUpdate
				WHERE SURVEYMAPPINGID = '$ParSurveyMappingID'";
//error_log($sql." \nlibies_survey\n", 3, "/tmp/aaa.txt");

		$result = $this->db_db_query($sql);
		return $result;
	}

	public function updateSurveyComment($ParSurveyMappingID,$ParComment="") {
		global $intranet_db;
		$sql = "UPDATE {$intranet_db}.IES_SURVEY_MAPPING
				SET COMMENT = '$ParComment'
				WHERE SURVEYMAPPINGID = '$ParSurveyMappingID'";
		$result = $this->db_db_query($sql);
		return $result;
	}

	/**
	* Update table IES_SURVEY_MAPPING
	* @owner : Fai (201000922)
	* @param : integer $ParSurveyMappingID ID of the SurveyMappingID that need to update
	* @param : Array  $ParData Data that need to update
	* @return : Boolean $result Result of the update
	*
	*/
	public function updateSurvey($ParSurveyMappingID,$ParData) {
		global $intranet_db;
		$sql = "UPDATE {$intranet_db}.IES_SURVEY_MAPPING
				set XTitle = '{$ParData['xTitle']}',
				YTitle = '{$ParData['yTitle']}',
				MappingTitle = '{$ParData['title']}'
				WHERE SURVEYMAPPINGID = '$ParSurveyMappingID'";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
		$result = $this->db_db_query($sql);
		return $result;
	}


	public function getSurveyListingTable($ParSurveyID,$ParStudentID,$ParIsCheckFromIESStudent,$ParCommentEdit=false,$ForSurveyDiscovery=0) {

		global $Lang;
		$SurveyMapping = $this->loadSurveyMapping($ParSurveyID,$ParStudentID);

// 		$EditPhp = ($ForSurveyDiscovery == 1)? 'index.php?mod=survey&task=discovery_info_layer' : 'index.php?mod=survey&task=questionnaireShowTable';
		$mod = 'survey';
		$task = ($ForSurveyDiscovery == 1)? 'discovery_info_layer' : 'questionnaireShowTable';
		$ItemTitle = ($ForSurveyDiscovery == 1)? $Lang['IES']['DiscoveryName'] : $Lang['IES']['CombinationName'];

		if (is_array($SurveyMapping) && count($SurveyMapping)>0) {
		    $_SESSION['thisExportSurveyMappingIDs'] = array();
			if ($ParCommentEdit) {
// 				$URIEdit = "IsEdit=1";
			    $URIEdit = 1;
			} else {
// 				$URIEdit = "IsEdit=0";
			    $URIEdit = 0;
			}
			$count=0;
			foreach($SurveyMapping as $key => $element) {
				$surveyMappingID = $element["SURVEYMAPPINGID"];
				$encodedMappingID = base64_encode($surveyMappingID);
				$dateModified = $element["DATEMODIFIED"];
				$mappingTitle = $element["MAPPINGTITLE"];
				++$count;
                $_SESSION['thisExportSurveyMappingIDs'][] = $surveyMappingID;

				if ($ParIsCheckFromIESStudent) {
					$deleteButton = '';
					if ($ForSurveyDiscovery == 0)
						$deleteButton .= "<div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"移動\"></a></div>";
					$deleteButton .= "<div class=\"table_row_tool\"><a title=\"Delete\" class=\"delete_dim\" href=\"#\" onClick=\"removeMapping(this,'{$encodedMappingID}')\"></a></div>";
				} else {
					$deleteButton = "&nbsp;";
				}
				$content .= <<<HTMLEND
		    <tr id="{$surveyMappingID}">
		      <td>{$count}</td>
		      <td><a class="IES_file_upload" href="javascript:load_dyn_size_thickbox_ip('$mappingTitle', 'onloadThickBox(\'$mod\', \'$task\', $surveyMappingID, $URIEdit, true);', inlineID='', defaultHeight=576, defaultWidth=660);">$mappingTitle</a></td>
		        <!--  <td><a class="IES_file_upload thickbox" href="{$EditPhp}&SurveyMappingID={$surveyMappingID}&$URIEdit&KeepThis=true&amp;TB_iframe=true&amp;height=576&amp;width=660">$mappingTitle</a></td>-->
                <td>{$dateModified}</td>
		      <td class="sub_row Dragable">{$deleteButton}</td>
		    </tr>
HTMLEND;
			}
		} else {
			$content .= "<tr><td colspan='4' style='text-align:center'>{$Lang['IES']['NoRecordAtThisMoment']}</td></tr>";
		}
		$html = <<<HTMLEND
		<table class="common_table_list">
		  <thead>
		  	  <tr>
		      <th class="num_check">#</th>
		      <th>{$ItemTitle}</th>
		      <th>{$Lang['IES']['CreationDate']}</th>
		      <th>&nbsp;</th>
		    </tr>
		  </thead>
		  <tbody class="ClassDragAndDrop" id="SurveyMappingIDInSeq">
		  	{$content}
		  </tbody>
		</table>
HTMLEND;
		return $html;
	}

	function getNoCombineSurveyMapping($ParSurveyID,$ParQuestionNo,$ParStudentID) {
		global $intranet_db;
		$sql = "SELECT SURVEYMAPPINGID FROM {$intranet_db}.IES_SURVEY_MAPPING WHERE SURVEYID = {$ParSurveyID} AND XMAPPING = '{$ParQuestionNo}' AND (YMAPPING IS NULL OR YMAPPING = '') AND USERID = '{$ParStudentID}'";

		return current($this->returnVector($sql));
	}

	#===ADD
	/**
	 * GetIndividualAnswerDisplay
	 * @param INT $ParSurveyID - the Survey ID
	 *
	 * @param INT $ParQuestionNo - The question number start from 1
	 *
	 * @param INT $ParDisplayCreateNewQuestion - Option for Control the UI for question setting
	 *
	 * @return HTML code
	 */
	function GetIndividualAnswerDisplay($ParSurveyID, $ParQuestionNo, $ParIsAddButton=false, $ParStudentID=null,$ParDisplayCreateNewQuestion="1", $isComparisonQ2=false){
		global  $ies_cfg, $Lang;

		################################################
		##	Prepare resources

		$survey = $this->getStudentSurveyBySurveyId($ParSurveyID);
		$question = $survey[0]['Question'];

		$IS_ANSWER_VALID = true;
		$answer = $this->getSurveyAnswer($ParSurveyID,$IS_ANSWER_VALID);
		$chartTypeArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Question"], $survey[0]['ChartType']);
		$questionsArray = $this->breakQuestionsString($question);

		# refined the LS element
		$r = $refinedQuestionsAnswer = $this->getRefinedQuestionsAndAnswerForLS($questionsArray,$answer,$ParQuestionNo);
		$QuestionTitle = $refinedQuestionsAnswer["QUESTIONS_ARRAY"][(int)$ParQuestionNo]["TITLE"];
		$QuestionType = $refinedQuestionsAnswer["QUESTIONS_ARRAY"][(int)$ParQuestionNo]["TYPE"];
		$refinedQuestionsAnswer["QUESTIONS_ARRAY"][(int)$ParQuestionNo]["CHARTTYPE"] = $chartTypeArray[$ParQuestionNo-1];

		$table = "";
		## Coz the Qestion Number Start From 1## so <=
		if ($refinedQuestionsAnswer["X_QUESTION_NUMBER"] <= sizeof($questionsArray)) {
			$PlainTable = $this->getPlainTable($refinedQuestionsAnswer["QUESTIONS_ARRAY"], $refinedQuestionsAnswer["ANSWER"], $refinedQuestionsAnswer["X_AXIS"],$refinedQuestionsAnswer["X_SUBQUESTION_NUMBER"]);
			$highChartDataAry = $this->ansToHighChartData($refinedQuestionsAnswer["QUESTIONS_ARRAY"], $refinedQuestionsAnswer["ANSWER"], $refinedQuestionsAnswer["X_AXIS"],$refinedQuestionsAnswer["X_SUBQUESTION_NUMBER"]);
		}

		if(!empty($PlainTable)){

			if (!empty($ParStudentID)) {
				$currentSurveyMappingID = $this->getNoCombineSurveyMapping($ParSurveyID,$ParQuestionNo,$ParStudentID);
			}
			if ($ParIsAddButton && count($questionsArray)>1) {	// only one question do not show the add combination button
				$IsAddButton = true;
			} else {
				$IsAddButton = false;
			}

			if($isComparisonQ2==='true'){$isComparisonQ2= true;$ComparisonQ2='_2';}else{$isComparisonQ2=false;$ComparisonQ2='';}
			$has_chart['pie'] = $this->isQuestionTypehasPieChart($QuestionType);
			if(count($highChartDataAry)>0) {//draw bar vs draw pie
			    $highChartJs ='';
                $sizeAry = Array('width'=>320, 'height' => 400 );
			    if ($has_chart['pie']){
			        $highChartJs .= "<script>".$this->drawHighChart_Pie($highChartDataAry['pie'], $QuestionTitle, $Lang['SBA']['Quantity'], $QuestionTitle, $QuestionType, $x_legend_max, $divID = 'my_chart_'.($refinedQuestionsAnswer["X_AXIS"]-1).'_pie_Final'.$ComparisonQ2, $sizeAry)."</script>";
                }
                $highChartJs.= "<script>".$this->drawHighChart_VerticalBar($highChartDataAry['bar'], $QuestionTitle, $Lang['SBA']['Quantity'], $QuestionTitle, $QuestionType, $x_legend_max, $divID = 'my_chart_'.($refinedQuestionsAnswer["X_AXIS"]-1).'_bar_Final'.$ComparisonQ2, $sizeAry)."</script>";
			}

			$table .= $this->genTableWithStyle($PlainTable, $refinedQuestionsAnswer["QUESTIONS_ARRAY"], $refinedQuestionsAnswer["X_AXIS"],$IsAddButton,$currentSurveyMappingID,$ParDisplayCreateNewQuestion,$ParSurveyID,$ParStudentID, $ComparisonQ2);
			$table .= $highChartJs;
			/*
			$table .= "<div id='my_chart_".$ParQuestionNo."parent' style='width:100%; height:300px;position:relative;display:".$divDisplay.";'>
			<div id='my_chart_".$ParQuestionNo."Final' style='width:100%; height:100%;position:absolute;'/></div>
			</div>

			";

			*/
		}

		return $table;
	}
	function updateSurveyQuestionChartTypes($SurveyID, $chart_types){

		global $ies_cfg;

		$chart_types_str = implode($ies_cfg["Questionnaire"]["Delimiter"]["Question"], $chart_types);

		$sql = "UPDATE $intranet_db.IES_SURVEY
			SET ChartType = '$chart_types_str'
			WHERE SurveyID = '$SurveyID'";

		$this->db_db_query($sql);

	}
	function ReturnSurveyChartDiv($QuestionType, $ParQuestionNo, $display="", $chartType="bar", $ComparisonQ2="")
	{
		global $sba_cfg, $Lang;

		$width = $sba_cfg['SBA']['SurveyChart']['ChartWidth'];
		$ParQuestionNo--;
		if($display!="") {
			$divDisplay = $display;

		} else{
			$divDisplay = "inline";
		}

		if($ComparisonQ2 == '_2'){$ComparisonQ2ForJs='2';}
		$chartType = in_array($chartType, array('bar','pie'))? $chartType: 'bar';
		$has_chart['pie'] = $this->isQuestionTypehasPieChart($QuestionType);

		$charts .= "<div id='my_chart_{$ParQuestionNo}_bar_Final{$ComparisonQ2}' class='my_chart_Final'/></div>";
		$radios .= "<input ".($chartType=='bar'? 'checked': '')." class='my_chart_types_radio' type='radio' name='my_chart_types[$ParQuestionNo]{$ComparisonQ2}' value='bar' id='bar_$ParQuestionNo$ComparisonQ2' onclick='changeGraph($ParQuestionNo, 0, $ComparisonQ2ForJs)'>";
		$radios .= "<label for='bar_$ParQuestionNo{$ComparisonQ2}'>".$Lang['SBA']['Charts']['bar']."</label>";

		if ($has_chart['pie']){

		    $charts .= "<div id='my_chart_{$ParQuestionNo}_pie_Final{$ComparisonQ2}' class='my_chart_Final'/></div>";
		    $radios .= "<input ".($chartType=='pie'? 'checked': '')." class='my_chart_types_radio' type='radio' name='my_chart_types[$ParQuestionNo]{$ComparisonQ2}' value='pie' id='pie_$ParQuestionNo$ComparisonQ2' onclick='changeGraph($ParQuestionNo, 1, $ComparisonQ2ForJs)'>";
		    $radios .= "<label for='pie_$ParQuestionNo{$ComparisonQ2}'>".$Lang['SBA']['Charts']['pie']."</label>";
		}


		$html .= "<div id='my_chart_{$ParQuestionNo}parent' class='my_chart_parent' style='max-width:{$width}px;overflow:hidden;display:$divDisplay;'>";

		$html .= $radios;

		$html .= "<div class='my_chart_container' style='width:".($width*2)."px;'>";
		$html .= $charts;
		$html .= "</div></div>";


		return $html;
	}

	/**
	 * Handling the likert scale for specified subquestion required
	 */
	public function getRefinedQuestionsAndAnswerForLS($ParQuestionsArray,$ParAnswer,$ParQuestionNumberX,$ParQuestionNumberY=null) {
		$returnArray = array();
		# handling for x question, subquestion number
		$questionSubquestionNumX = explode(".",$ParQuestionNumberX);	// it is a decimal format x.yyy
		$returnArray["X_AXIS"] = $questionSubquestionNumX[0];
		$returnArray["X_SUBQUESTION_NUMBER"] = (int)$questionSubquestionNumX[1];

		# handling for y question, subquestion number
		if (!empty($ParQuestionNumberY)) {
			$questionSubquestionNumY = explode(".",$ParQuestionNumberY);	// it is a decimal format x.yyy
			$returnArray["Y_AXIS"] = $questionSubquestionNumY[0];
			$returnArray["Y_SUBQUESTION_NUMBER"] = (int)$questionSubquestionNumY[1];
		}

		# handling for x subquestion, answer extraction
		if (!empty($returnArray["X_SUBQUESTION_NUMBER"])) {	// the subquestion number i.e. yyy
			$returnArray["QUESTIONS_ARRAY"] = $this->extractWithSubquestion($ParQuestionsArray,$returnArray["X_AXIS"],$returnArray["X_SUBQUESTION_NUMBER"]);
			$returnArray["ANSWER"] = $this->filterUnwantedAnswer($ParAnswer,$returnArray["X_AXIS"],$returnArray["X_SUBQUESTION_NUMBER"]);
		} else {
			$returnArray["QUESTIONS_ARRAY"] = $ParQuestionsArray;
			$returnArray["ANSWER"] = $ParAnswer;
		}

		# handling for y subquestion, answer extraction
		if (isset($returnArray["Y_SUBQUESTION_NUMBER"])) {	// the subquestion number i.e. yyy
			$returnArray["QUESTIONS_ARRAY"] = $this->extractWithSubquestion($returnArray["QUESTIONS_ARRAY"],$returnArray["Y_AXIS"],$returnArray["Y_SUBQUESTION_NUMBER"]);
			$returnArray["ANSWER"] = $this->filterUnwantedAnswer($returnArray["ANSWER"],$returnArray["Y_AXIS"],$returnArray["Y_SUBQUESTION_NUMBER"]);
		} else {
			// do nothing, it will then equal to the return array set during handling for x subquestion, answer extraction
		}
		return $returnArray;
	}

	public function filterUnwantedAnswer($ParAnswerStringArray, $ParQuestionNumber, $ParSubquestionRequired) {
		global $ies_cfg;
		if (is_array($ParAnswerStringArray)) {
			$subquestionIndex = $ParSubquestionRequired-1;
			foreach($ParAnswerStringArray as $ParAnswerStringArrayKey => &$ParAnswerStringArrayElement) {
				# break to answers
				$explodedAnswer = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer"],$ParAnswerStringArrayElement);	// reminded that the 1st one of the array is empty
				# break options for specific answer
				$targetSubquestionAnswerArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"],$explodedAnswer[$ParQuestionNumber]);
				# assign the only required answer back
				$explodedAnswer[$ParQuestionNumber] = $targetSubquestionAnswerArray[$subquestionIndex];
				# construct back to answer
				$ParAnswerStringArrayElement = implode($ies_cfg["Questionnaire"]["Delimiter"]["Answer"],$explodedAnswer);
			}
		}
		return $ParAnswerStringArray;
	}

	/**
	 * Get the question element with specified question extracted required subquestion
	 * @param Array $ParQuestionElement - array generated by function breakQuestionsString()
	 * @param INT paramname - question number
	 * @param INT $ParSubquestionRequired - subquestion number
	 * @return Array
	 */
	public function extractWithSubquestion($ParQuestionElement,$ParQuestionNumber, $ParSubquestionRequired) {
		$targetSubquestions = $ParQuestionElement[$ParQuestionNumber]["QUESTION_QUESTIONS"];
		if (is_array($targetSubquestions)) {
			foreach($targetSubquestions as $targetSubquestionsKey => $targetSubquestionsElement) {
				$subquestionIndex = $ParSubquestionRequired-1;
				if ($targetSubquestionsKey==$subquestionIndex) {
					$newSetOfSubquestions[] = $targetSubquestionsElement;
				} else {
					// do nothing
				}
			}
		}
		$ParQuestionElement[$ParQuestionNumber]["QUESTION_QUESTIONS"] = $newSetOfSubquestions;
		$ParQuestionElement[$ParQuestionNumber]["QUESTION_NUM"] = count($newSetOfSubquestions);
		return $ParQuestionElement;
	}

	function ansToHighChartData($ParQuestionsArray=Array(), $ParAnswer, $ParQuestionNo='', $ParSubquestionNo=0){
	    global $ies_cfg;
	    if(empty($ParQuestionsArray)||$ParQuestionNo==''){
	        $resultArray = $ParAnswer;
	        $combineTableType = 'Y';
	   }else{
	       $resultArray = $this->getAnalysisResultArray($ParQuestionsArray, $ParAnswer, $ParQuestionNo);
	   }
	   $ParQuestionsArray[$ParQuestionNo]["TYPE"]? $questiontype = $ParQuestionsArray[$ParQuestionNo]["TYPE"] : '';
	   if($combineTableType){
	       $revisedAnsAry['bar']['question'] = array();
           foreach ($resultArray as $Compare_item => $qDataAry) {
               array_push($revisedAnsAry['bar']['question'], $Compare_item);
               foreach ($qDataAry as $_compare_item => $_qDataAry) {
                   $key = array_search($_compare_item, array_keys($qDataAry));
                   if (empty($revisedAnsAry['bar']['chartData'][$key])) {
                       $revisedAnsAry['bar']['chartData'][$key] = array('name' => $_compare_item, 'data' => array());
                   }
                   array_push($revisedAnsAry['bar']['chartData'][$key]['data'], intval($qDataAry[$_compare_item]));
               }
           }
           return $revisedAnsAry;
	   }else{
    	   switch ($questiontype){
    	        case $ies_cfg["Questionnaire"]["QuestionType"]["MC"]:
    	        case $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]:
    	            $qTitle = $ParQuestionsArray[$ParQuestionNo]['TITLE'];
    	            $revisedAnsAry['pie']['data'] = array();
    	            for($i=0, $i_max=count($resultArray); $i<$i_max; $i++) {
    	                list($_option, $_count) = $resultArray[$i];
    	                $revisedAnsAry['bar'][$i] = array('name'=> $_option ,'data'=>array(intval($_count)));
    	                array_push( $revisedAnsAry['pie']['data'], array('name'=> $_option , 'y'=>intval($_count)));
    	            }
    	            return $revisedAnsAry;
    	            break;

    	        case $ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]:
    	        case $ies_cfg["Questionnaire"]["QuestionType"]["FillLong"]:
    	            $ansAry = $resultArray['ANSWER'];
    	            $ansAry = array_count_values($resultArray['ANSWER']);
    	            foreach($ansAry as  $_ans=> $_count) {
    	                $revisedAnsAry['bar'][] = array('name'=> $_ans ,'data'=>array(intval($_count)));
    	            }
    	            return $revisedAnsAry;
    	            break;

    	        case $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]:
    	            $qTitle = $ParQuestionsArray[$ParQuestionNo]['TITLE'];
    	            $qArray = $ParQuestionsArray[$ParQuestionNo]['QUESTION_QUESTIONS'];
    	            $revisedAnsAry['bar']['question']=$qArray;
    	            foreach($resultArray as  $i =>$qDataAry) {
    	                for($_i=0, $i_max=count($qDataAry); $_i<$i_max; $_i++) {
    	                    list($_option, $_count) = $qDataAry[$_i];
    	                    if(empty($revisedAnsAry['bar']['chartData'][$_i]))$revisedAnsAry['bar']['chartData'][$_i]= array('name'=> $_option ,'data'=>array());
    	                    array_push($revisedAnsAry['bar']['chartData'][$_i]['data'], intval($_count));
    	               }
    	            }
    	            return $revisedAnsAry;
    	            break;

    	        case $ies_cfg["Questionnaire"]["QuestionType"]["Table"]:

    	            break;

    	    }
    	}
	}


	function getPlainTable($ParQuestionsArray, $ParAnswer, $ParQuestionNo, $ParSubquestionNo=0){
		global $ies_cfg;

		$resultArray = $this->getAnalysisResultArray($ParQuestionsArray, $ParAnswer, $ParQuestionNo);
		switch ($ParQuestionsArray[$ParQuestionNo]["TYPE"]){
				case $ies_cfg["Questionnaire"]["QuestionType"]["MC"]:

					return $this->genPlainTableForMC($resultArray);
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]:

					return $this->genPlainTableForMC($resultArray);
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]:
					return $this->genPlainTableForText($resultArray);
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["FillLong"]:
					return $this->genPlainTableForText($resultArray);
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]:
					return $this->genPlainTableForLS($resultArray, $ParQuestionNo, $ParQuestionsArray,$ParSubquestionNo);
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["Table"]:

					break;

			}
	}

	function getAnalysisResultArray($ParQuestionsArray, $ParAnswer, $ParQuestionNo){
		global $ies_cfg;

		switch ($ParQuestionsArray[$ParQuestionNo]["TYPE"]){
				case $ies_cfg["Questionnaire"]["QuestionType"]["MC"]:
					$answer = $this->splitAnswer($ParAnswer);
					$array = $this->getAnalysisResults2($ParQuestionsArray,$answer, $ParQuestionNo, null);
					return $array["X_RESULT"];
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]:
					$answer = $this->splitAnswer($ParAnswer);
					$array = $this->getAnalysisResults2($ParQuestionsArray,$answer, $ParQuestionNo, null);
					return $array["X_RESULT"];

					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]:
					return $this->getAnsTextResultArray($ParQuestionNo, $ParQuestionsArray, $ParAnswer);

					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["FillLong"]:
					return $this->getAnsTextResultArray($ParQuestionNo, $ParQuestionsArray, $ParAnswer);

					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]:
					$array = array();
					## Gen New Answer and Question
					$NewAns = $this->AnswerStringParse($ParQuestionNo, -1, $ParAnswer, $ParQuestionsArray);

					#!!!! The type of the question will be tuned as MC here for process
					$NewQuestion = $this->QuestionStringParse($ParQuestionNo, -1, $ParQuestionsArray);

					for($i = 0; $i < $ParQuestionsArray[$ParQuestionNo]["QUESTION_NUM"]; $i++){
					    $answer = $this->splitAnswer($NewAns[$i][0]);
						$ra = $this->getAnalysisResults2($NewQuestion[$i][0],$answer, $ParQuestionNo, null);
						$array[] = $ra["X_RESULT"];
					}
					return $array;
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["Table"]:

					break;

			}
	}


	//		$ParDisplayCreateNewQuestion is NEW, 20110831
	//	    $ParDisplayCreateNewQuestion = 0 , don't display the set question input
	//		$ParDisplayCreateNewQuestion = 1 (original version), display the set question input
	//		$ParDisplayCreateNewQuestion = 2 , display the question setting in a new place
	function genTableWithStyle($ParPlainTable, $ParQuestionArray, $ParQuestionNo, $ParIsAddButton=false, $ParMappingID="",$ParDisplayCreateNewQuestion = '1',$ParSurveyID,$ParStudentID, $ComparisonQ2=''){
		global $Lang;

		$normalQuestionFlag = true;
		$specialQuestionHTML = '';
		$specialQuestionHTMLDetails = '';
		$displayFlag  = false;
		$QuestionType = $ParQuestionArray[$ParQuestionNo]['TYPE'];
		$ChartType = $ParQuestionArray[$ParQuestionNo]['CHARTTYPE'];

		$subQuestionArray = array(); // store the sub question for LikertScale question

		$html .= "<div class=\"ies_q_box\">";
		$html .= "<div class=\"q_title\" id=\"title_".$ParQuestionNo."\">";
		$html .= "\n<ol start=\"{$ParQuestionNo}\">";
//		$html .= $ParQuestionArray[$ParQuestionNo]["TITLE"];

		if($ParDisplayCreateNewQuestion == 2){
		$uncheckAllSubQuestionJS = '';
			if(is_array($ParQuestionArray[$ParQuestionNo]["QUESTION_QUESTIONS"])){
				//special handle LikertScale question,
				$normalQuestionFlag = false;
				$displayWholeSetQuestionFlag = false;		//control the display of the whole set question input

				for($i = 0,$i_max = count($ParQuestionArray[$ParQuestionNo]["QUESTION_QUESTIONS"]);$i< $i_max; $i++){
					$_mTitle = '';
					$h_checked = '';

					$individaulSurveyMappingDetails = array();
					$currentSurveyMappingID = '';

					$_q_title = $ParQuestionArray[$ParQuestionNo]["QUESTION_QUESTIONS"][$i];	//$_q_title ==> question title
					$_tmp_no = $i+1;
					$_q_no	= $ParQuestionNo.'.'.$_tmp_no; //$_q_no ==> question Number
					$subQuestionArray[] = $_q_no; // store the sub question to $subQuestionArray

					$currentSurveyMappingID = $this->getNoCombineSurveyMapping($ParSurveyID,$_q_no,$ParStudentID);
					if(is_numeric($currentSurveyMappingID)){
						$individaulSurveyMappingDetails = current($this->loadSurveyMapping($ParSurveyID,$ParStudentID,$currentSurveyMappingID));
						$_mTitle  = $individaulSurveyMappingDetails['MAPPINGTITLE'];	//$_mTitle ==> mapping title
						$displayWholeSetQuestionFlag = true;
						$h_checked = ' checked ';

					}

					//hidden form field to store the DB SurveyMappingID
					$specialQuestionHTMLDetails .= '<tr><td width= "100">';
					$specialQuestionHTMLDetails .= '<input type="hidden" name="FocusQuestionSurveyMappingID['.$_q_no.']" id="FocusQuestionSurveyMappingID_'.$_q_no.'" value="'.$currentSurveyMappingID.'" />';

					//checkbox
					$specialQuestionHTMLDetails .= '<input type="checkbox" name="FocusQuestionSelected['.$_q_no.']" id="chkFocusQuestion_'.$_q_no.'" '.$h_checked.' /><label for="chkFocusQuestion_'.$_q_no.'">'.$_q_title.'</label>&nbsp;&nbsp;';

					$specialQuestionHTMLDetails .= '</td>';
					$specialQuestionHTMLDetails .= '<td>';

					$specialQuestionHTMLDetails .= '<input type="text" name="FocusQuestionTitle['.$_q_no.']" id="currentTitle_'.$_q_no.'" value="'.intranet_htmlspecialchars($_mTitle).'" onclick="selectQuestionChkBox(\''.$_q_no.'\')"/>';

					$specialQuestionHTMLDetails .= '</td></tr>';

				}

				$h_displayStyle = ($displayWholeSetQuestionFlag === true)? '': ' style="display:none" ';
//				$specialQuestionHTML .= '<span id="currentTitleDiv_'.$ParQuestionNo.'" '.$h_displayStyle.'>';
				$specialQuestionHTML .= '<table id="currentTitleDiv_'.$ParQuestionNo.'" '.$h_displayStyle.' border = "0">';
				$specialQuestionHTML .= $specialQuestionHTMLDetails;
//				$specialQuestionHTML .= '</span>';
				$specialQuestionHTML .= '</table>';

				$specialQuestionHTMLDetails = ''; // reset the variable
				//end of special handle LikertScale question,

			}else{
				//normal question
				//do nothing
			}

//			$currentSurveyMappingID = array();

			$h_checked = '';
			$h_displayStyle = '';
			$currentSurveyMappingID = '';
			$h_spanCSS = ' check_group ';
			if($normalQuestionFlag == true){
				$currentSurveyMappingID = $this->getNoCombineSurveyMapping($ParSurveyID,$ParQuestionNo,$ParStudentID);

				$h_displayStyle = ' style="display:none" ';
				$h_checked = '';
				$h_spanCSS = ' check_group ';
				if(is_numeric($currentSurveyMappingID)){
					$individaulSurveyMappingDetails = current($this->loadSurveyMapping($ParSurveyID,$ParStudentID,$currentSurveyMappingID));
					$_mTitle = $individaulSurveyMappingDetails['MAPPINGTITLE'];//$_mTitle ==> mapping title
					$h_displayStyle = '';  // reset the display style , set it to visible
					$h_checked = ' checked ';
					$h_spanCSS = 'check_group_checked';
					$is_existingQuestionFlag = 1;
				}

//				$htmlInputTitle  = '<span id="currentTitleDiv_'.$ParQuestionNo.'" '.$h_displayStyle.' >'.$Lang['IES']['CombinationName'];
				$htmlInputTitle  = '<table id="currentTitleDiv_'.$ParQuestionNo.'" '.$h_displayStyle.' ><tr><td>'.$Lang['IES']['CombinationName'].' ';

				//hidden form field to store the DB SurveyMappingID
				$htmlInputTitle  .= '<input type="hidden" name="FocusQuestionSurveyMappingID['.$ParQuestionNo.']" id="FocusQuestionSurveyMappingID_'.$ParQuestionNo.'" value="'.$currentSurveyMappingID.'" />';

				$htmlInputTitle .= '<input type="text" name="FocusQuestionTitle['.$ParQuestionNo.']" id="currentTitle_'.$ParQuestionNo.'" value="'.intranet_htmlspecialchars($_mTitle).'" onclick="selectQuestionChkBox(\''.$ParQuestionNo.'\')"/>';

//				$htmlInputTitle .= '</span>'; //close 'currentTitleDiv_'
				$htmlInputTitle .= '</td></tr></table>'; //close 'currentTitleDiv_'
			}


//			$html .= '<span class="check_group_checked">'.$htmlInputTitle;

			if($normalQuestionFlag === true){
				$chkName = 'FocusQuestionSelected['.$ParQuestionNo.']';
			}else{
				$chkName = 'SpecialFocusQuestionSelected['.$ParQuestionNo.']';
				if($displayWholeSetQuestionFlag === true){
					//this is a LikertScale question and is selected
					$h_checked = ' checked ';
					$h_spanCSS = 'check_group_checked';
					$jsSubQuestionNoStr = implode('##',$subQuestionArray);
					$uncheckAllSubQuestionJS = ' unCheckSubQuestion(\''.$jsSubQuestionNoStr.'\'); ';
				}
			}

			$html .= '<span class="'.$h_spanCSS.'" id="setQuestionControl_'.$ParQuestionNo.'">';
			$html .= '<input name="'.$chkName.'" type="checkbox" ';
			$html .= ' onClick="showHideCurrentTitle('.$ParQuestionNo.');'.$uncheckAllSubQuestionJS.'" id="chkFocusQuestion_'.$ParQuestionNo.'" '.$h_checked.'/>';
			$html .= '<label for="chkFocusQuestion_'.$ParQuestionNo.'">'.$Lang['IES']['IsFocusQuestion'].'</label>';
			$html .= '<br/>'.$htmlInputTitle.$specialQuestionHTML.'</span>';

		}

		$html .= '<li>'.$ParQuestionArray[$ParQuestionNo]["TITLE"].'<a href="javascript:void(0);" onclick="return displayStatistics('.$ParQuestionNo.')" class="hidedata" id="displayStatistics_'.$ParQuestionNo.'">'.$Lang['IES']['HideRecord'].'</a>';
		// commentd by Henry Chow on 2012-07-19, show/hide chart triggered by javascript function "displayStatistics()"
		/*
		if($QuestionType==2 || $QuestionType==3 || $QuestionType==6) {
			$html .= ' | <a href="javascript:displayChartDiv('.$ParQuestionNo.')" class="hidedata"><span id="ChartTextDiv'.$ParQuestionNo.'">'.$Lang['SBA']['HideChart'].'</span></a>';
		}
		*/
		$html .= '</li>';
		$html .= "</ol>\n";
		$html .= '<br class="clear" /><p class="spacer"></p><!-- html layout handling , don\'t remove  -->';
		$html .= "</div><div id='q_result_$ParQuestionNo$ComparisonQ2'><div>";
		$html .= "<div class=\"q_option\" id=\"q_option_".$ParQuestionNo."\">".$ParPlainTable;
		$html .= "</div>".$this->ReturnSurveyChartDiv($QuestionType, $ParQuestionNo, "", $ChartType, $ComparisonQ2)."</div></div></div>";

		/*
		if (empty($ParMappingID)) {
			$ParMappingID = -1;
		} else {
			$h_Checked = " checked='checked' ";
			$mappingDetailArray = current($this->loadSurveyMapping("","",$ParMappingID));
		}

		if($this->get_display_survey_mapping_option()){
			$h_hide = $h_Checked? "" : "display:none";

			if($ParDisplayCreateNewQuestion == 1){
				$html .= <<<HTMLEND

					<div style="text-align:left">

					<input type="checkbox" name="checkCurrent" id="checkCurrent" value="{$ParMappingID}" $h_Checked onClick="showHideCurrentTitle()"/>
						<label for="checkCurrent">{$Lang['IES']['IsFocusQuestion']}</label>

						<div id="currentTitleDiv" style="$h_hide">
						<br/>
						{$Lang['IES']['CombinationName']} :
						<input type="text" name="currentTitle" id="currentTitle" value="{$mappingDetailArray["MAPPINGTITLE"]}"/>
						</div>
					</div>
HTMLEND;
			}

		}

		if($ParDisplayCreateNewQuestion == 1){
			if ($ParIsAddButton) {
				$html .=<<<HTMLEND
				<div class="btn">
				<input type="button" value="{$Lang['IES']['newgrouping']}" onmouseout="this.className='formbutton'" onmouseover="this.className='formbuttonon'" class="formbutton" name="submit2" onClick="addCombineBox()">
				</div>
HTMLEND;
			}
		}
		*/

		return $html;
	}



	function genPlainTableForMC($ParResultArray){
		$return = "<table class=\"common_table_list\">";

		if (is_array($ParResultArray)) {
			$total = 0;
			foreach($ParResultArray as $key => $element) {
				$total += $element["VALUE"];
			}

			foreach($ParResultArray as $key => $element) {
				if($total != 0){
				$elementValue = $element['VALUE'] / $total;
				$elementValue *= 100;
				$elementValue = round($elementValue, 0);
				}
				else{
					$elementValue = 0;
				}
//					////debug_r($this->trimOptions($element["OPT_POS"]));
				$return .= "<tr>";
				$return .= "<td>".$this->trimOptions($element["OPT_POS_1"])."</td>";
				$return .= "</td><td class=\"tool_files_col\" nowrap=\"nowrap\">".$element['VALUE']."({$elementValue}%)</td>";
				$return .= "</tr>";

			}
		}
		$return .= "</table>";
		return $return;
	}

	function genPlainTableForText($ParResultArray){

		$return = "<table class=\"common_table_list\">";
		//////debug_r($ParResultArray);
		if (is_array($ParResultArray)) {


			foreach($ParResultArray["ANSWER"] as $key => $element) {

				$return .= "<tr>";
				$return .= "<td>".nl2br($element)."</td>";
				$return .= "</tr>";
			}
		}
		$return .= "</table>";
		return $return;
	}

	function genPlainTableForLS($ParResultArray, $ParQuestionNumber,$ParQuestionsArray,$ParSubquestionNumber=0){
		//debug_r($ParResultArray);
		$return = "<table class=\"common_table_list\">";
		$return .= "<tr>";
		$return .= "<th class=\"num_check\">#</th><th>問題</th>";
		for($i = 0; $i < $ParQuestionsArray[$ParQuestionNumber]["OPTION_NUM"]; $i++){
			$return .= "<th class=\"sub_row_top\">".$this->trimOptions($ParResultArray[0][$i]["OPT_POS_1"])."</th>";
		}
		$return .= "</tr>";
		for($i = 0; $i < $ParQuestionsArray[$ParQuestionNumber]["QUESTION_NUM"]; $i++){
			if (is_array($ParResultArray[$i])) {
				if (!empty($ParSubquestionNumber)) {
					$q_num = $ParSubquestionNumber;
				} else {
					$q_num = $i + 1;
				}
				$return .= "<tr>";
				$return .= "<td>{$q_num}</td>";
				$return .= "<td>".$ParQuestionsArray[$ParQuestionNumber]["QUESTION_QUESTIONS"][$i]."</td>";
				foreach($ParResultArray[$i] as $key => $element) {
					$return .= "<td class=\"tool_files_col\" nowrap=\"nowrap\">".$element['VALUE']."</td>";
				}
				$return .= "</tr>";
			}
		}
		$return .= "</table>";
		return $return;
	}


	/**
	 * Get the result of analysis
	 * @param INT $ParQuestionNo - the question number, start from 1
	 *
	 * @param Array $ParQuestionElements - array element generated by function breakQuestionsString()
	 * @param  String $ParAnswer - answer generated by function getSurveyAnswer()
	 * @return Array	- answer Format => Array["TITLE"] = title, Array["ANSWER"]=> ARRAY()
	 */
	function getAnsTextResultArray($ParQuestionNo,$ParQuestionElements,$ParAnswer){
		global $ies_cfg;
		$Answer = array();
		$title = $ParQuestionElements[$ParQuestionNo]["TITLE"];
		$Answer["TITLE"] = $title;
		$Answer["ANSWER"] = array();
		foreach($ParAnswer as $answerKey=>$answerElement) {
			$answerArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer"],$answerElement);
			$Answer["ANSWER"][] = $answerArray[$ParQuestionNo];
		}
		return $Answer;
	}

	static public function breakEncodedURI($ParKey) {
		$decodedKey = base64_decode($ParKey);
		$parameters = array();
		foreach(explode("&",$decodedKey) as $pKey => $parameter) {
			$paraNameValuePair = explode("=",$parameter);
			$paraName = $paraNameValuePair[0];
			$parVar = $paraNameValuePair[1];
			$parameters[$paraName] = $parVar;
		}
		return $parameters;
	}

	public function getFormattedSurveyTitle($ParSurveyType,$ParSurveyTypeName,$ParTitle,$ParOriginalKey,$ParIsIESStudent) {
		if (!$ParIsIESStudent) {
			global $intranet_session_language,$Lang;
			//$html_studentSelectionBox = $this->getStudentSurveySelectionBox($ParOriginalKey,$ParSurveyType);
		}
		$returnString = "<span>$ParSurveyTypeName</span>".$html_studentSelectionBox."<br/>".$ParTitle;
		return $returnString;
	}

	public function getSurveyAnsweredRespondentList($ParSurveyID){
		global $intranet_db, $ies_cfg;

		$sql = "SELECT isa.SurveyID, isa.AnswerID, isa.Respondent, date_format(isa.DateInput, '%e-%c-%Y') as InputDate, isa.Type, isa.Status, isa.RespondentNature, ".getNameFieldWithClassNumberByLang ("iu.")." AS StudentName, isa.UserID FROM {$intranet_db}.IES_SURVEY_ANSWER isa " .
				"LEFT JOIN {$intranet_db}.INTRANET_USER iu ON isa.UserID = iu.UserID WHERE isa.SurveyID = '$ParSurveyID' ORDER BY isa.DateInput DESC";
//		DEBUG($sql);
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}
	public function DeleteSurveyAnswer($ParAnswerID){
		global $intranet_db;

		$sql = "DELETE FROM IES_SURVEY_ANSWER WHERE AnswerID = '$ParAnswerID'";

		$result = $this->db_db_query($sql);
		return $result;
	}

	public function UpdateSurveyAnswerStatus($ParSurveyID, $ParRespondent, $ParStatus, $ParRespondentNature){
		global $intranet_db, $ies_cfg;
		if($ParRespondentNature != $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']){
			$sql = "Update IES_SURVEY_ANSWER SET status = '{$ParStatus}' WHERE SurveyID = '{$ParSurveyID} AND Respondent = '{$ParRespondent}' AND RespondentNature = '$ParRespondentNature'";
		}
		else{
			$sql = "Update IES_SURVEY_ANSWER SET status = '{$ParStatus}' WHERE SurveyID = '{$ParSurveyID}' AND UserID = '{$ParRespondent}' AND RespondentNature = '$ParRespondentNature'";
		}
		$result = $this->db_db_query($sql);
		return $result;
	}

	public function splitAnswer($ParAnswerArray) {
		global $ies_cfg;
		$returnArray = array();
		if(is_array($ParAnswerArray)){
			foreach($ParAnswerArray as $key=>$ParAnswer){
				$answerArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer"],$ParAnswer);
				if (is_array($answerArray)) {
					foreach($answerArray as $answerArrayKey => $answerArrayElement) {
						$eachAnswerArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"],$answerArrayElement);
						if ($answerArrayKey == 0) {
							// do nothing
						} else {
							$tempArray[$answerArrayKey] = $eachAnswerArray;
						}

					}
				}
				$returnArray[] = $tempArray;
			}

		}
		return $returnArray;
	}

	/**
	 * @param INT $ParTaskID - the taskid
	 * @param String $ParKey - the original key, URL
	 * @param String $ParSelectedValue - the selected option of the selection box
	 * @return String - HTML of a selection box
	 */
	public function getStudentSurveySelectionBox($ParKey,$ParSurveyType) {
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libuser.php");

		$brokenKey = $this->breakEncodedURI($ParKey);
		$TaskID = $brokenKey["TaskID"];
		$students = $this->getTaskStudent($TaskID);
		$parentIds = current($this->getParentsIds("TASK",$TaskID));


		$keyStudentNamePairs = array();
		if (is_array($students)) {
			foreach($students as $studentsKey => $studendId) {
				$studentStageSurvey = $this->getStudentStageSurvey($parentIds["STAGEID"],$studendId);
				$surveyId = $studentStageSurvey[$ParSurveyType];

				# replace new values into the key array
				$tempKey = $brokenKey;
				$tempKey["SurveyID"] = $surveyId;
				$tempKey["StudentID"] = $studendId;
				$newKeyArray = array();
				foreach($tempKey as $key=>$element) {
					$newKeyArray[] = $key."=".$element;
				}
				$newKey = base64_encode(implode("&",$newKeyArray));
				$libuser = new libuser($studendId);
				$keyStudentNamePairs[] = array($surveyId,$newKey,$libuser->UserNameLang());
			}
		}

		//-- Start: SELECTION BOX --//
		if (is_array($keyStudentNamePairs) && count($keyStudentNamePairs)>0) {
			$html = "<select name='student_survey_selection' onChange='goOtherStudent(this.value);'>";
			foreach($keyStudentNamePairs as $key => $element) {
				$surveyId = $element[0];
				$value = $element[1];
				$name = $element[2];
				$style  = "";
				if (empty($surveyId)) {
					$optionType = "optgroup";
					$style = " style='color: #CCCCCC'";
				} else {
					$optionType = "option";
				}
				if ($ParKey == $value) {
					$selected = "selected='selected'";
				} else {
					$selected = "";
				}
				$html .= "<$optionType $style $selected value='$value' label='$name'>";
				$html .= $name;
				$html .= "</$optionType>";
			}
			$html .= "</select>";
		} else {
			$html = "";
		}
		//-- End: SELECTION BOX --//
		return $html;
	}

	/**
	 * @author maxwong
	 * @param INT $ParSurveyID - SurveyID
	 * @param INT $ParStudentID - StudentID
	 * @param Array $ParSurveyMappingIDInSeq - array of survey mapping id in sequence
	 */
	public function updateMappingSequence($ParSurveyID,$ParStudentID,$ParSurveyMappingIDInSeq) {
		global $intranet_db;
		# Clear old sequence
		$sql = "UPDATE {$intranet_db}.IES_SURVEY_MAPPING SET SEQUENCE = NULL WHERE SURVEYID = '{$ParSurveyID}' AND USERID = '{$ParStudentID}'";
		$result["clear"] = $this->db_db_query($sql);

		if ($result["clear"]) {
			if (is_array($ParSurveyMappingIDInSeq)) {
				$i = 1;
				foreach($ParSurveyMappingIDInSeq as $key => $SurveyMappingID) {
					$sql = "UPDATE {$intranet_db}.IES_SURVEY_MAPPING SET SEQUENCE = '{$i}' WHERE SURVEYMAPPINGID = '{$SurveyMappingID}'";
					$result["updateSeq"][] = $this->db_db_query($sql);
					$i++;
				}
			}
		}
		return $result;
	}

	function set_display_survey_mapping_option($enable){
		# set to false when obj created
		$this->display_survey_mapping_option = $enable;
	}

	function get_display_survey_mapping_option(){
		return $this->display_survey_mapping_option;
	}

	/**
	* Return Default Question by the Survey Type
	* @owner : Fai (201000922)
	* @param : Integer $parSurveyType, the survey type that need to return
    * @param : Integer $parSurveyLang, the survey lang that need to return
	* @return : String , A concat value of the question type
	*
	*/

	function getDefaultQuestion($parSurveyType,$parSurveyLang = '',$parSchemeType = ''){
		global $ies_cfg,$intranet_db;



		$parSurveyLang = ($parSurveyLang == '')?$ies_cfg['DB_IES_DEFAULT_SURVEY_QUESTION']['B5']:$parSurveyLang;
		$returnStr = '';

		$sqlSchemeType = ($parSchemeType == '')? '' :  ' and SchemeType = \''.$parSchemeType.'\'';
		if(is_numeric($parSurveyType)){
			$sql  = ' select GROUP_CONCAT(QuestionStr SEPARATOR \'\') as `question`';
			$sql .= ' from '.$intranet_db.'.IES_DEFAULT_SURVEY_QUESTION ';
			$sql .= ' where ';
			$sql .= ' DefaultQuestion = '.$ies_cfg['Questionnaire']['isDefaultQuestion'].' and SurveyType = '.$parSurveyType.' ';
			$sql .= ' and LangType = \''.$parSurveyLang.'\' ';
			$sql .= $sqlSchemeType;

			$sql .= ' order by DisplayOrder';
//debug_r($sql);
error_log($sql."    e:[".mysql_error()."]<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");

			$result = $this->returnArray($sql,'',1);
			if(is_array($result) && count($result) ==1){
				$returnStr = $result[0]['question'];
			}
		}

		return $returnStr;
	}

	/**
	* Return Survey for a student
	* @owner : Fai (20120710)
	* @param : Integer $scheme_id, the scheme id
    * @param : Integer $studentID, the student ID
    * @param : Integer $SurveyType, the Survey Type (field observer, survey...)
	* @return : array , with returnResultSet Format
	*
	*/

	function getStudentSurvey($scheme_id,$studentID,$SurveyType){
		global $intranet_db;

		$sql = 'select
					SurveyID ,Title     ,Description          ,Question ,
					DateStart           ,DateEnd             ,
					SurveyType ,
					SurveyStatus ,
					InputBy ,
					DateInput           ,
					ModifiedBy ,
					DateModified        ,
					Remark1 ,Remark2 ,Remark3 ,Remark4 ,
					UserID ,SchemeID
				from
					'.$intranet_db.'.IES_SURVEY
				where
					userid = \''.$studentID.'\' and SchemeID  = \''.$scheme_id.'\' and SurveyType = \''.$SurveyType.'\'
					order by DateInput desc'
						;
		$objDB = new libdb();

		$result = $objDB->returnResultSet($sql);

		return $result;
	}


	function getStudentSurveyBySurveyId($surveyIdAry,$orderByStr = ''){
		global $intranet_db;

		if(is_array($surveyIdAry)){
			//do nothing
		}else{
			//convert to array
			$surveyIdAry  = array($surveyIdAry);
		}

		$orderByStr = ($orderByStr == '')? " order by DateInput desc , title " : $orderByStr;
		$sql = 'select
					s.SurveyID as `SurveyID`,
					s.Title    as `Title` ,
					s.Description as `Description`,
					s.Question as `Question`,
					s.DateStart as `DateStart` ,
					s.DateEnd as `DateEnd`,
					s.SurveyType  as `SurveyType `,
					s.SurveyStatus as `SurveyStatus`,
					s.InputBy as `InputBy`,
					DATE_format(s.DateInput, \'%Y-%m-%d\') as `DateInput`,
					s.ModifiedBy as `ModifiedBy`,
					DATE_format(s.DateModified, \'%Y-%m-%d\') as `DateModified`,
					s.Remark1 as `Remark1`,
					s.Remark2 as `Remark2`,
					s.Remark3 as `Remark3`,
					s.Remark4 as `Remark4` ,
					s.UserID as `UserID`,
					s.SchemeID as `SchemeID`,
					s.ChartType as `ChartType`
				from
					'.$intranet_db.'.IES_SURVEY  as s
				where
					SurveyID in ('.implode(',',$surveyIdAry).')'
				;

		$sql .= $orderByStr;

		$objDB = new libdb();

		$result = $objDB->returnResultSet($sql);
//error_log($sql."    e:[".mysql_error()."]<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
		return $result;
	}


	function  HighChartExportPngJSFunctions($type){
	    global $UserID;

	  $getSVGJS ="  Highcharts.getSVG = function(charts) {
	        var svgArr = [],
	        left = 0,
	        height = 0;

	        $.each(charts, function(i, chart) {
	            var svg = chart.getSVG();
	            svg = svg.replace('<svg', '<g transform=\"translate(' + left + ',0)\" ');
	            svg = svg.replace('</svg>', '</g>');

	            left += 600;
	            height = Math.max(height, chart.chartHeight);

	            svgArr.push(svg);
	        });

	            return '<svg height=\"' + height + '\" width=\"' + left + '\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">' + svgArr.join('') + '</svg>';
	    };";

	    /**
	     * Create a global exportCharts method that takes an array of charts as an argument,
	     * and exporting options as the second argument
	     */
      $fileName = 'Highcharts.charts[key][\'renderTo\'].id';
	  $exportChartsFunctionJS = "Highcharts.exportCharts = function(charts, fileName, key) {
          // URL to Highcharts export server
          var exportUrl = 'https://export.highcharts.com/';

          // Ajax request
          $.ajax({
            type: 'post',
            url: exportUrl,
            data: {
              svg: Highcharts.getSVG(charts),
              type: 'image/png',
              async: true
            },
            success: function(data) {
                 $.ajax({
                    type: 'post',
                    url: 'index.php',
                    data: {
                      mod: 'ajax',
                      task: 'ajax_saveChartImage',
                      imgSrc: data,
                      userId: ".$UserID.",
                      fileName: ''+fileName,
                      fileType: 'png',
                      type: '".$type."',
                      key: key
                    },
                    success: function(data) {
                        console.log(data);
                }
             });
            }
        });

       };";
	  // detinationPath: '/home/web/eclass40/intranetIP25/file/sba/tmp_chart/'+'$sba_thisStudentID/'+'$SurveyMappingID', #add path in the detination ajax instead
	  // combination id
	  $exportChartsJS=" $('#saveChartImage').click(function() {
	    $.each(Highcharts.charts,function(key, chart){
          var filename = ".$fileName.";
          Highcharts.exportCharts([chart], filename, key) ;
	    });
	  });";
// 	  console.log();
	  return "\n".$getSVGJS."\n".$exportChartsFunctionJS."\n".$exportChartsJS;
	}

/*
	function drawHighChart_HorizontalBar($data, $x_legend_text="", $y_legend_text="")
	{

	    global $PATH_WRT_ROOT;

	    include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");

	    if($x_legend_text=="") $x_legend_text = 'No. of Students';
	    if($y_legend_text=="") $y_legend_text = 'Raw Marks';

	    //get the data
	    $maxNo = 1; //decide the x axis range i.e. maximum number of student
	    $yLabels = array(); //y labels i.e. Raw Marks
	    $xValue = array(); //value along x-axis i.e. No. of Student
	    $tip = array(); //string on every tooltip i.e. remarks

	    $chart = new open_flash_chart();

	    $i = 0 ;

	    foreach($data as $option => $value){
	        $yLabels = array();
	        $xValue = array();
	        $tip = array();
	        $yLabels[] = $option;
	        //$yLabels[] = 5;
	        $xValue[] = $value["NumOfStn"];
	        //$tip[] = $value["Remarks"];
	        //$tip = intranet_htmlspecialchars($value["Remarks"].":#val#");
	        $tip[] = ($value["Remarks"].":#val#");

	        //$bar0 = new bar_glass('#333333');
	        $bar0 = new hbar('#333333');
	        $bar0->set_values( $xValue );
	        $bar0->set_tooltip( $tip );
	        //$bar0->set_key( '5.88%(64.71%)', '12' );

	        $bar0->set_visible( true );
	        $chart->add_element( $bar0 );

	        if($value["NumOfStn"] > $maxNo){
	            $maxNo = $value["NumOfStn"];
	        }

	        $i++;

	    }//end foreach
	    $maxNo += 4;

	    # flash chart


	    $x_legend = new x_legend( $x_legend_text );
	    $x_legend->set_style( '{font-size: 12px; font-weight: normal; color: #000000}' );

	    $x = new x_axis();
	    $x->set_stroke( 2 );
	    $x->set_tick_height( 2 );
	    $x->set_colour( '#999999' );
	    $x->set_grid_colour( '#CCCCCC' );
	    $x->set_range( 0, $maxNo, 1 );
	    $x->set_offset(false);

	    $y_legend = new y_legend( $y_legend_text );
	    $y_legend->set_style( '{font-size: 12px; font-weight: normal; color: #000000}' );

	    $y = new y_axis();
	    $y->set_stroke( 2 );
	    $y->set_tick_length( 2 );
	    $y->set_colour( '#999999' );
	    $y->set_grid_colour( '#CCCCCC' );
	    $y->set_labels( $yLabels );
	    $y->set_offset(true);

	    $tooltip = new tooltip();
	    $tooltip->set_static();
	    $tooltip->set_stroke( 2 );
	    $tooltip->set_body_style( '{font-size: 9px; font-weight: lighter; color: #000000}' );

	    # show/hide checkbox panel
	    $key = new key_legend();
	    $key->set_visible(false);
	    $key->set_selectable(false);

	    //$chart = new open_flash_chart();
	    $chart->set_bg_colour( '#FFFFFF' );
	    //$chart->set_title( $title );
	    $chart->set_x_legend( $x_legend );
	    $chart->set_x_axis( $x );
	    $chart->set_y_legend( $y_legend );
	    $chart->set_y_axis( $y );
	    //$chart->add_element( $bar0 );
	    $chart->set_tooltip( $tooltip );
	    $chart->set_key_legend($key);

	    return $chart;
	}
*/
	function drawChart_HorizontalBar($data, $x_legend_text="", $y_legend_text="")
	{

		global $PATH_WRT_ROOT;

		include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");

		if($x_legend_text=="") $x_legend_text = 'No. of Students';
		if($y_legend_text=="") $y_legend_text = 'Raw Marks';

		//get the data
		$maxNo = 1; //decide the x axis range i.e. maximum number of student
		$yLabels = array(); //y labels i.e. Raw Marks
		$xValue = array(); //value along x-axis i.e. No. of Student
		$tip = array(); //string on every tooltip i.e. remarks

		$chart = new open_flash_chart();

		$i = 0 ;

		foreach($data as $option => $value){
			$yLabels = array();
			$xValue = array();
			$tip = array();
			$yLabels[] = $option;
			//$yLabels[] = 5;
			$xValue[] = $value["NumOfStn"];
			//$tip[] = $value["Remarks"];
			//$tip = intranet_htmlspecialchars($value["Remarks"].":#val#");
			$tip[] = ($value["Remarks"].":#val#");

			//$bar0 = new bar_glass('#333333');
			$bar0 = new hbar('#333333');
			$bar0->set_values( $xValue );
			$bar0->set_tooltip( $tip );
			//$bar0->set_key( '5.88%(64.71%)', '12' );

			$bar0->set_visible( true );
			$chart->add_element( $bar0 );

			if($value["NumOfStn"] > $maxNo){
				$maxNo = $value["NumOfStn"];
			}

			$i++;

		}//end foreach
		$maxNo += 4;

		# flash chart


		$x_legend = new x_legend( $x_legend_text );
		$x_legend->set_style( '{font-size: 12px; font-weight: normal; color: #000000}' );

		$x = new x_axis();
		$x->set_stroke( 2 );
		$x->set_tick_height( 2 );
		$x->set_colour( '#999999' );
		$x->set_grid_colour( '#CCCCCC' );
		$x->set_range( 0, $maxNo, 1 );
		$x->set_offset(false);

		$y_legend = new y_legend( $y_legend_text );
		$y_legend->set_style( '{font-size: 12px; font-weight: normal; color: #000000}' );

		$y = new y_axis();
		$y->set_stroke( 2 );
		$y->set_tick_length( 2 );
		$y->set_colour( '#999999' );
		$y->set_grid_colour( '#CCCCCC' );
		$y->set_labels( $yLabels );
		$y->set_offset(true);
		/*
		$bar0 = new bar_glass('#333333');
		$bar0->set_values( $xValue );
		$bar0->set_tooltip( $tip );
		//$bar0->set_key( '5.88%(64.71%)', '12' );
		$bar0->set_id( 0 );
		$bar0->set_visible( true );
		*/
		$tooltip = new tooltip();
		$tooltip->set_static();
		$tooltip->set_stroke( 2 );
		$tooltip->set_body_style( '{font-size: 9px; font-weight: lighter; color: #000000}' );

		# show/hide checkbox panel
		$key = new key_legend();
		$key->set_visible(false);
		$key->set_selectable(false);

		//$chart = new open_flash_chart();
		$chart->set_bg_colour( '#FFFFFF' );
		//$chart->set_title( $title );
		$chart->set_x_legend( $x_legend );
		$chart->set_x_axis( $x );
		$chart->set_y_legend( $y_legend );
		$chart->set_y_axis( $y );
		//$chart->add_element( $bar0 );
		$chart->set_tooltip( $tooltip );
		$chart->set_key_legend($key);

		return $chart;
	}

	function drawHighChart_Pie($data, $x_legend_text="", $y_legend_text="", $QuestionTitle="", $QuestionType='', $x_legend_max="", $chartDiv, $sizeAry=""){

	    if(count($data)==0){
	        return;
	    }
	    if($QuestionType==6) {
	        return $this->drawHighChart_VerticalBar($data, $x_legend_text, $y_legend_text, $QuestionTitle, $QuestionType, $x_legend_max, $chartDiv);
	    }

	    global $PATH_WRT_ROOT, $Lang, $sba_cfg;

	    include_once($PATH_WRT_ROOT."includes/json.php");
	    $json = new JSON_obj();

	    if($x_legend_text=="") $x_legend_text = $Lang['SBA']['ChartOptions'];
	    if($y_legend_text=="") $y_legend_text = $Lang['SBA']['NoOfStudents'];
	    if($QuestionTitle=="") $QuestionTitle = $Lang['SBA']['Chart'];

// 	    $chart = "<script type='text/javascript'>";

	    $chartType = 'pie';
	    $jsonDataAry = $json->encode($data);

	    $chart .=    "var ".$chartDiv." = new
	     Highcharts.chart({
	        chart: {
                renderTo: '".$chartDiv."',
	            plotBackgroundColor: null,
	            plotBorderWidth: null,
	            plotShadow: false,
	            type: '".$chartType."',
	            height: '".$sizeAry['height']."',
	            width: '".$sizeAry['width']."'
	        },
	        title: {
	            text: '".$QuestionTitle."'
	        },
	        tooltip: {
	            pointFormat: '<b>{point.y} ({point.percentage:.1f}%)</b>'
	        },
	      plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
            credits: false,
";
// 	        series: [{
// 	            name: 'Brands',
// 	            colorByPoint: true,
// 	            data: [{
// 	                name: 'Chrome',
// 	                y: 61.41,
// 	                sliced: true,
// 	                selected: true
// 	            }, {
// 	                name: 'Internet Explorer',
// 	                y: 11.84
// 	                }, {
// 	                    name: 'Firefox',
// 	                    y: 10.85
// 	                }, {
// 	                    name: 'Edge',
// 	                    y: 4.67
// 	                    }, {
// 	                        name: 'Safari',
// 	                        y: 4.18
// 	                    }, {
// 	                        name: 'Other',
// 	                        y: 7.05
// 	                        }]
// 	        }]
// 	    });";

	    $chart .=  '"series":['.$jsonDataAry.']
            });';

// 	    $chart .= "</script>";

	    return $chart;

	}


	function drawChart_Pie($data, $x_legend_text="", $y_legend_text="", $QuestionTitle="", $QuestionType, $x_legend_max="")
	{

		global $PATH_WRT_ROOT, $Lang, $sba_cfg;

		if(count($data)==0){
		    return;
		}
		if($QuestionType==6) {
			return $this->drawChart_VerticalBar($data, $x_legend_text, $y_legend_text, $QuestionTitle, $QuestionType, $x_legend_max);
		}

		include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");

		if($QuestionTitle=="") $QuestionTitle = $Lang['SBA']['Chart'];



		$colourArr = array('#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414');


		//$bar = new hbar('#333333');
		$pie = new pie();
		$pie->set_colours($colourArr);
		$pie->set_start_angle( 35 );

		$pie_values = array();
		foreach($data as $option => $value){
		    $pie_value = new pie_value((int)$value, $option);
		    $pie_values[] = $pie_value;

		}
		$pie->set_values($pie_values);
		$title = new title($QuestionTitle);

		$chart = new open_flash_chart();
		$chart->set_title( $title );
		$chart->add_element($pie);

		return $chart;
	}

	function drawHighChart_VerticalBar($data, $x_legend_text="", $y_legend_text="", $QuestionTitle="", $QuestionType='', $x_legend_max="", $chartDiv, $sizeAry=""){

	    if(count($data)==0) return;

	    global $PATH_WRT_ROOT, $Lang, $sba_cfg;

	    include_once($PATH_WRT_ROOT."includes/json.php");
	    $json = new JSON_obj();

	    if($x_legend_text=="") $x_legend_text = $Lang['SBA']['ChartOptions'];
	    if($y_legend_text=="") $y_legend_text = $Lang['SBA']['NoOfStudents'];
	    if($QuestionTitle=="") $QuestionTitle = $Lang['SBA']['Chart'];

// 	    $chart = "<script type='text/javascript'>";
	    $chartType = 'column';
	    if($QuestionType==6){
	        $categories = $json->encode($data['question']);
	        $data = $data['chartData'];
	    }else {
	        $categories = '[""]';
	    }
	    $jsonDataAry = $json->encode($data);

	    $chart =    "var ".$chartDiv." = new
// Create the chart
	    Highcharts.chart({
	        chart: {
                renderTo: '".$chartDiv."',
	            type: '".$chartType."',
                height: '".$sizeAry['height']."',
                width: '".$sizeAry['width']."'
	        },
	        title: {
	            text: '".$QuestionTitle."'
	        },
	        xAxis: {
                title: {
	                text: '".$x_legend_text."'
	            },
              categories:".$categories."
	        },
	        yAxis: {
               title: {
	                text: '".$y_legend_text."'
	            },
	            allowDecimals: false
	        },
	        legend: {
	            enabled: true,

                verticalAlign: 'top'
	        },
	        plotOptions: {
	            series: {
	                borderWidth: 0
	            },
	        },
            credits: false,
 ";
	    $chart .=  '"series":'.$jsonDataAry.'
            });';

// 	      $chart .= "</script>";

	    return $chart;

	}

	function drawChart_VerticalBar($data, $x_legend_text="", $y_legend_text="", $QuestionTitle="", $QuestionType, $x_legend_max=""){

	    if(count($data)==0) return;

		global $PATH_WRT_ROOT, $Lang, $sba_cfg;

		include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");

		if($x_legend_text=="") $x_legend_text = $Lang['SBA']['ChartOptions'];
		if($y_legend_text=="") $y_legend_text = $Lang['SBA']['NoOfStudents'];
		if($QuestionTitle=="") $QuestionTitle = $Lang['SBA']['Chart'];

		$chart = new open_flash_chart();

		$tooltip = new tooltip();
		$tooltip->set_hover();
		$chart->set_tooltip($tooltip);

		$i = 0 ;
		$maxValue = 0;
		$valueArr = array();
		$allXLabel = array();

		$colourArr = array('#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414');

		$color_i = 0;

		$valueArr = array();
		// for each answer option

		$x_legend_min = $sba_cfg['SBA']['SurveyChart']['X_Legend_Min'];

		foreach($data as $option => $value){

			if($QuestionType==6) {
				$bar = new bar();
				$valueArr = array();

				// for each sub-question option

				$pos = 0;
				foreach($value as $subQuestion=>$count) {
					$valueArr[] = (int)$count;
					$bar->set_values($valueArr);
					$bar->set_colour($colourArr[$color_i%sizeof($colourArr)]);
					$color_i++;
					$allXLabel[$pos] = $subQuestion;
					$pos++;

					if ($count > $maxValue) {
						$maxValue = $count;
					}
				}

				//$bar->set_tooltip($option.":#val#");
				$bar->set_key($option, "12");	// this is key legend

				$x_legend_text = "";

				$x_legend_max = $x_legend_max ? $x_legend_max : $sba_cfg['SBA']['SurveyChart']['X_Legend_Max']['Matrix'];


			} else {
				//$bar = new hbar('#333333');
				$bar = new bar();
				//$bar = new pie();

				$valueArr[] = (int)$value;

				if ($value > $maxValue) {
					$maxValue = $value;
				}
				$bar->set_values($valueArr);
				$bar->set_key($option, "12");
				$bar->set_colour($colourArr[$color_i%sizeof($colourArr)]);
				$color_i++;
				$allXLabel[0] = "";
				$x_legend_text = "";

				$x_legend_max = $sba_cfg['SBA']['SurveyChart']['X_Legend_Max']['Normal'];

			}


			$chart->add_element($bar);
			$valueArr = array();

			$i++;

		}//end foreach

		if ($maxValue <= 5) {
			$maxY = 5;
		} else {
			if (substr($maxValue, strlen($maxValue)-1, 1) == "0") {		// Last digit = 0
				$maxY = $maxValue;
			} else {
				$maxY = (substr($maxValue, 0, strlen($maxValue)-1) + 1)."0";
			}
		}



		$x_legend = new x_legend( $x_legend_text );
		$x_legend->set_style( '{font-size: 12px; font-weight: normal; color: #000000}' );

		$x = new x_axis();
		$x->set_stroke( 2 );
		$x->set_tick_height( 2 );
		$x->set_colour( '#999999' );
		$x->set_grid_colour( '#CCCCCC' );
		$x->set_labels_from_array($allXLabel);
		$x->set_range($x_legend_min,$x_legend_max);

		$y_legend = new y_legend( $y_legend_text );
		$y_legend->set_style( '{font-size: 12px; font-weight: normal; color: #000000}' );

		$y = new y_axis();
		$y->set_stroke( 2 );
		$y->set_tick_length( 2 );
		$y->set_colour( '#999999' );
		$y->set_grid_colour( '#CCCCCC' );
		//$y->set_labels( $yLabels );
		$y->set_range( 0, $maxY, $maxY / 5 );
		//$y->set_range( 0, 10, 70 / 5 );



		$y->set_offset(false);

		$title = new title($QuestionTitle);


		//$chart->set_bg_colour( '#FFFFFF' );
		$chart->set_title( $title );

		$chart->set_x_legend( $x_legend );
		$chart->set_x_axis( $x );
		$chart->set_y_legend( $y_legend );
		$chart->set_y_axis( $y );


		return $chart;

	}
	/*
	function GenerateChart($chartArr, $chartWidth="", $chartHeight="")
	{
		global $intranet_httppath, $PATH_WRT_ROOT, $UserID;
		//debug_pr($chartArr);

		$chartWidth = ($chartWidth=="") ? "586" : $chartWidth;
		$chartHeight = ($chartHeight=="") ? "737" : $chartHeight;



		$curTimestamp = time();

		$returnValue = '
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'includes/flashchart_basic/js/json/json2.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'includes/flashchart_basic/js/swfobject.js"></script>
		';

		$returnValue .= '
			<script type="text/javascript">

			function ofc_ready(){
				// do nothing
			}

			function open_flash_chart_data(id){
				return JSON.stringify(dataArr[id]);
			}

			function findSWF(movieName) {
			  if (navigator.appName.indexOf("Microsoft")!= -1) {
				return window[movieName];
			  } else {
				return document[movieName];
			  }
			}

			function onAutoPostImgBinary(id){
				var swfDIV = document.getElementById("div"+id+"parent");

				while(swfDIV.hasChildNodes()){
					swfDIV.removeChild(swfDIV.firstChild);
				}
				var img = document.createElement("img");
				img.setAttribute("src", "'.$intranet_httppath.'/file/reportcard2008/tmp_chart/raw_mark_distribution/'.$UserID.'/'.$curTimestamp.'/tmp_chart_png_"+id+".png");
				img.setAttribute("width", "'.$chartWidth.'");
				img.setAttribute("height", "'.$chartHeight.'");
				swfDIV.appendChild(img);
			}


			var chartNum = '.count($chartArr).';;
			var dataArr = new Array();
		';

		foreach( (array)$chartArr as $chartID => $chartJSON ) {
			$returnValue .= "dataArr[ ".$chartID." ] = ".$chartJSON->toPrettyString()."\n";
		}

		$returnValue .= '</script>';

		$returnValue .= '
		<script type="text/javascript">
			var chartNum = '.count($chartArr).';
			for(var k=0; k<chartNum; k++){
				var flashvars = {id:k, postImgUrl:"uploadFlashImage.php?curTimestamp='.$curTimestamp.'"};
				swfobject.embedSWF("'.$PATH_WRT_ROOT.'includes/flashchart_basic/open-flash-chart.swf", "div"+k, "'.$chartWidth.'", "'.$chartHeight.'", "9.0.0", "", flashvars);
			}//end for
		</script>
		';

		return $returnValue;

	}
	*/

	function GetChartArr($ParSurveyID, $ParQuestionNo, $ParIsAddButton=false, $ParStudentID=null,$ParDisplayCreateNewQuestion = "1", $chartType = 'bar'){
		global  $ies_cfg, $Lang;

		################################################
		##	Prepare resources
		$question = $this->getSurveyQuestion($ParSurveyID);
		$IS_ANSWER_VALID = true;
		$answer = $this->getSurveyAnswer($ParSurveyID,$IS_ANSWER_VALID);
		$questionsArray = $this->breakQuestionsString($question);

		# refined the LS element
		$refinedQuestionsAnswer = $this->getRefinedQuestionsAndAnswerForLS($questionsArray,$answer,$ParQuestionNo);
		$QuestionTitle = $refinedQuestionsAnswer["QUESTIONS_ARRAY"][$ParQuestionNo]["TITLE"];
		$QuestionType = $refinedQuestionsAnswer["QUESTIONS_ARRAY"][$ParQuestionNo]["TYPE"];

		#################### Construct Chart [Start] #################################
		$ansAry = $this->getAnalysisResultArray($questionsArray, $answer, $ParQuestionNo);
		if($QuestionType==2 || $QuestionType==3 || $QuestionType==6) {


				if($QuestionType==2 || $QuestionType==3) {
					for($a=0, $a_max=count($ansAry); $a<$a_max; $a++) {
						list($_value, $_count) = $ansAry[$a];
						//$ChartDataArr[$_value]['NumOfStn'] = $_count;
						$ChartDataArr[$_value] = $_count;
					}
				} else if($QuestionType==6) {
					$subQuestionAry = $questionsArray[$ParQuestionNo]['QUESTION_QUESTIONS'];
					//$ansOptAry = $ansAry[$a];

					for($b=0, $b_max=count($subQuestionAry); $b<$b_max; $b++) {
						$subQuestion = $subQuestionAry[$b];
						$ansOptAry = $ansAry[$b];

						for($c=0, $c_max=count($ansOptAry); $c<$c_max; $c++) {
							list($_value, $_count) = $ansOptAry[$c];
							$ChartDataArr[$_value][$subQuestion] = $_count;
						}

					}
					$x_legend_max = count($subQuestionAry) + 1;

				}


			if(count($ChartDataArr)>0) {//draw bar vs draw pie
				if ($chartType=='pie'){
				    $tempChart = $this->drawChart_Pie($ChartDataArr, $QuestionTitle, $Lang['SBA']['Quantity'], $QuestionTitle, $QuestionType, $x_legend_max);
				}else{
				    $tempChart = $this->drawChart_VerticalBar($ChartDataArr, $QuestionTitle, $Lang['SBA']['Quantity'], $QuestionTitle, $QuestionType, $x_legend_max);
				}

			}

		}
		return $tempChart;
	}

	function Revise_2D_Result($Raw2DResult=array())
	{
		$ChartDataArr = array();

		$NoOfRecord = count($Raw2DResult);
		if($NoOfRecord==0) return $ChartDataArr;

		for($i=0; $i<$NoOfRecord; $i++) {
			$opt1 = $Raw2DResult[$i]['OPT_POS_1'];
			$opt2 = $Raw2DResult[$i]['OPT_POS_2'];
			$count = $Raw2DResult[$i]['VALUE'];

			$ChartDataArr[$opt2][$opt1] = $count;
		}
		return $ChartDataArr;

	}


// 	function getAnalysisResultArray($ParQuestionsArray, $ParAnswer, $ParQuestionNo){
	public function getCombineHighChart($ParSurveyID,$ParXAxis,$ParYAxis="", $ParSurveyMappingID, $ChartDivId="") {
	    global $Lang;

	    //error_log("-->getCombineBoxContent ParXAxis -- ".$ParXAxis." ParYAxis -- ".$ParYAxis."\n", 3, "/tmp/aaa.txt");

	    $question = $this->getSurveyQuestion($ParSurveyID);
	    $questionsElement = $this->breakQuestionsString($question);
	    $og_questionsElement = $questionsElement;
	    $IS_ANSWER_VALID = true;

	    $answer = $this->getSurveyAnswer($ParSurveyID,$IS_ANSWER_VALID);
	    $broken_answer = $this->splitAnswer($answer);
	    $refinedQuestionsAnswer = $this->getRefinedQuestionsAndAnswerForLS($questionsElement,$answer,$ParXAxis,$ParYAxis);
	    $questionsElement = $refinedQuestionsAnswer["QUESTIONS_ARRAY"];
	    $answer = $refinedQuestionsAnswer["ANSWER"];
	    $questionNumber = $refinedQuestionsAnswer['X_AXIS'];

	    $SurveyMapping = $this->loadSurveyMapping(null,null,$ParSurveyMappingID);
	    $currentSurveyMapping = current($SurveyMapping);
	    $QuestionTitle = $currentSurveyMapping["MAPPINGTITLE"];
	    if (empty($refinedQuestionsAnswer["Y_AXIS"])) {
	        $xAxisTitle = $refinedQuestionsAnswer["QUESTIONS_ARRAY"][$questionNumber]["TITLE"];
	        $QuestionType = $refinedQuestionsAnswer["QUESTIONS_ARRAY"][$questionNumber]["TYPE"];
	        $highChartDataAry = $this->ansToHighChartData($refinedQuestionsAnswer["QUESTIONS_ARRAY"], $refinedQuestionsAnswer["ANSWER"], $refinedQuestionsAnswer["X_AXIS"]);
	    }else{
	        $analysisResults = $this->getAnalysisResults2($og_questionsElement,$broken_answer, $ParXAxis,$ParYAxis);
	        $revised2dResult = $this->Revise_2D_Result($analysisResults["2D_RESULT"]);
	        $xAxisTitle = $QuestionTitle;
	        $QuestionType = 6;
	        $highChartDataAry = $this->ansToHighChartData(array(), $revised2dResult, '');
	    }
	    $sizeAry = Array('width'=>600, 'height' => 280 );
	    if($ChartDivId=='')$ChartDivId = 'my_chart_parent';
	    $highChartJs = "<script>".$this->drawHighChart_VerticalBar($highChartDataAry['bar'], $xAxisTitle, $Lang['SBA']['Quantity'], $QuestionTitle, $QuestionType, $x_legend_max, $ChartDivId, $sizeAry)."</script>";
	    return $highChartJs;
	}

	public function GetCombineChart($ParSurveyID,$ParXAxis,$ParYAxis,$ParSurveyMappingID)
	{
		global $Lang, $sba_cfg;

		$question = $this->getSurveyQuestion($ParSurveyID);
		$questionsElement = $this->breakQuestionsString($question);
		$og_questionsElement = $questionsElement;

		$IS_ANSWER_VALID = true;
		$answer = $this->getSurveyAnswer($ParSurveyID,$IS_ANSWER_VALID);
		$broken_answer = $this->splitAnswer($answer);

		$analysisResults = $this->getAnalysisResults2($og_questionsElement,$broken_answer, $ParXAxis,$ParYAxis);
		$revised2dResult = $this->Revise_2D_Result($analysisResults["2D_RESULT"]);

		$SurveyMapping = $this->loadSurveyMapping(null,null,$ParSurveyMappingID);
		$currentSurveyMapping = current($SurveyMapping);
		if(count($revised2dResult)>0) {			// self-defined combine chart
			//$SurveyMapping = $this->loadSurveyMapping(null,null,$ParSurveyMappingID);

			$QuestionTitle = $currentSurveyMapping["MAPPINGTITLE"];

			$thisMax = count($analysisResults['X_RESULT']) + 1;

			$tempChart = $this->drawChart_VerticalBar($revised2dResult, $x_axis=$QuestionTitle, $Lang['SBA']['Quantity'], $QuestionTitle, 6, $x_legend_max=$thisMax);

			return $tempChart;

		} else {			// this is "highlighted question"

			$thisTitle = $currentSurveyMapping["MAPPINGTITLE"];

			$thisAnswerAry = $analysisResults['X_RESULT'];
			for($i=0, $i_max=count($thisAnswerAry); $i<$i_max; $i++) {
				list($_option, $_count) = $thisAnswerAry[$i];
				$revisedAnsAry[$_option] = $_count;
			}

			$tempChart = $this->drawChart_VerticalBar($revisedAnsAry, $x_axis=$thisTitle, $Lang['SBA']['Quantity'], $thisTitle, 2, $x_legend_max="");
			return $tempChart;
		}

	}


	/*tell whether the question type has a graph*/
	public function isQuestionTypehasGraph($type){
		global $ies_cfg;

		return in_array($type,array(
					$ies_cfg["Questionnaire"]["QuestionType"]["MC"],
					$ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"],
					$ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]
				));
	}
	public function isQuestionTypehasPieChart($type){
		global $ies_cfg;


		return in_array($type,array(
					$ies_cfg["Questionnaire"]["QuestionType"]["MC"],
					$ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"],

				));
	}
	public function createTempFolder($sba_thisUserID,$SurveyID){
		global $intranet_root,$sba_cfg;

		$fs = new libfilesystem();
		$folder_prefix = $intranet_root."/file/sba/";

		### Create the tmp png folder if not exist
		if(!file_exists($folder_prefix)) {
			$fs->folder_new($folder_prefix);
			chmod($folder_prefix, 0777);
		}

		$folder_prefix .= "tmp_chart/";

		if(!file_exists($folder_prefix)) {
			$fs->folder_new($folder_prefix);
			chmod($folder_prefix, 0777);
		}

		$folder_prefix .= $sba_thisUserID."/";

		if(!file_exists($folder_prefix)) {
			$fs->folder_new($folder_prefix);
			chmod($folder_prefix, 0777);
		}

		$folder_prefix .= $SurveyID."/";

		if(!file_exists($folder_prefix)) {
			$fs->folder_new($folder_prefix);
			chmod($folder_prefix, 0777);
		}

		return $sba_cfg['SBA_SURVEY_CHART_ROOT_PATH'].$sba_thisUserID."/".$SurveyID."/";
	}

	public function genQuestionsCSV($SurveyID){
		global $ies_cfg;

		//Prepare Excel
		$SurveyDetails = $this->getSurveyDetails($SurveyID);
		$SurveyDetails = current($SurveyDetails);
		$SurveyStartDate = $SurveyDetails["DateStart"];
		$SurveyEndDate = $SurveyDetails["DateEnd"];
		$Survey_title = $SurveyDetails["Title"];
		$Survey_desc = $SurveyDetails["Description"];
		$question = $this->getSurveyQuestion($SurveyID);
		$IS_VALID = true;
		$answer = $this->getSurveyAnswer($SurveyID, $IS_VALID);

		$questionsArray = $this->breakQuestionsString($question);
		$ExportContent .= "\n".$Survey_title."\n";

		for($i = 1; $i <= sizeof($questionsArray) ; $i++){

			$resultArray = $this->getAnalysisResultArray($questionsArray, $answer, $i);
			switch ($questionsArray[$i]["TYPE"]){
				case $ies_cfg["Questionnaire"]["QuestionType"]["MC"]:
					$ExportContent .= "\n\n{$i})".$questionsArray[$i]['TITLE']."\n	".$this->GenExcelXorY($resultArray);
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]:
					$ExportContent .= "\n\n{$i})".$questionsArray[$i]['TITLE']."\n	".$this->GenExcelXorY($resultArray);
					break;
				case $ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]:
					$ExportContent .= "\n\n{$i})".$questionsArray[$i]['TITLE']."\n".$this->GenExcelText($resultArray);
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["FillLong"]:
					$ExportContent .= "\n\n{$i})".$questionsArray[$i]['TITLE']."\n".$this->GenExcelText($resultArray);
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]:
					$ExportContent .= "\n\n{$i})".$questionsArray[$i]['TITLE']."\n	".$this->genExcelForLS($resultArray, $i,$questionsArray);
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["Table"]:

					break;
			}
		}

		return $ExportContent;

	}
	public function genMappingCSV($SurveyMappingID){

		global $Lang;

		$SurveyMapping = $this->loadSurveyMapping(null,null,$SurveyMappingID);
		$currentSurveyMapping = current($SurveyMapping);
		$surveyID = $currentSurveyMapping["SURVEYID"];
		$x_axis = $currentSurveyMapping["XMAPPING"];
		$y_axis = $currentSurveyMapping["YMAPPING"];
		$x_title = $currentSurveyMapping["XTITLE"];
		$y_title = $currentSurveyMapping["YTITLE"];
		$title = $currentSurveyMapping["MAPPINGTITLE"];
		$comment = htmlspecialchars($currentSurveyMapping["COMMENT"]);
		$x_questionSubquestion = explode(".",$x_axis);
		$y_questionSubquestion = explode(".",$y_axis);
		$x_question_pos = $x_questionSubquestion[0];
		$x_subquestion_pos = $x_questionSubquestion[1];
		$y_question_pos = $y_questionSubquestion[0];
		$y_subquestion_pos = $y_questionSubquestion[1];

		$questionsElement = $this->breakQuestionsString($this->getSurveyQuestion($surveyID));
		$answer = $this->getSurveyAnswer($surveyID,true);
		$broken_answer = $this->splitAnswer($answer);
		$refinedQuestionsAnswer = $this->getRefinedQuestionsAndAnswerForLS($questionsElement,$answer,$x_axis,$y_axis);
		$og_questionsElement = $refinedQuestionsAnswer["QUESTIONS_ARRAY"];

		$ExportContent=$Lang['IES']['CombinationName']."\t".$title."\n";//Combination Name

		$x_question = $questionsElement[$x_question_pos]["TITLE"];//Combination Element 1
		if (isset($questionsElement[$x_question_pos]["QUESTION_QUESTIONS"])) {
			$x_subquestion_index = $x_subquestion_pos-1;
			$x_question.=" - ".$questionsElement[$x_question_pos]["QUESTION_QUESTIONS"][$x_subquestion_index];
		}
		$ExportContent.=$Lang['IES']['CombinationElement1']."\t".$x_question."\n";

		if (!empty($y_axis)) {//Combination Element 2
			$y_question = $questionsElement[$y_question_pos]["TITLE"];
			if (isset($questionsElement[$y_question_pos]["QUESTION_QUESTIONS"])) {
				$y_subquestion_index = $y_subquestion_pos-1;
				$y_question.=" - ".$questionsElement[$y_question_pos]["QUESTION_QUESTIONS"][$y_subquestion_index];
			}
			$ExportContent.=$Lang['IES']['CombinationElement2']."\t".$y_question."\n";

			$analysisResults = $this->getAnalysisResults2($og_questionsElement,$broken_answer, $x_axis,$y_axis);

			$ExportContent.="\n".$Lang['IES']['TableBelowShowResultOfQuestion']."\n";

			$ExportContent.="$y_question/$x_question".$this->GenExcelXandY($analysisResults["2D_RESULT"]);
			$ExportContent.="\nColumn Title:\t".$x_title;
			$ExportContent.="\nRow Title:\t".$y_title;

		}else{

		}

		if (empty($refinedQuestionsAnswer["Y_AXIS"])) {
			$resultTable = $this->getPlainTable($refinedQuestionsAnswer["QUESTIONS_ARRAY"], $refinedQuestionsAnswer["ANSWER"], $refinedQuestionsAnswer["X_AXIS"]);
		}

		return $ExportContent;

	}

	private function GenExcelXandY($ParResultArray){
	$return = "";
	global $sba_libSba;
	if (is_array($ParResultArray)) {
		foreach($ParResultArray as $ParResultArrayKey => $ParResultArrayElement) {
			$header[$ParResultArrayElement["OPT_POS_1"]] = $sba_libSba->trimOptions($ParResultArrayElement["OPT_POS_1"]);;
			if (isset($tableContent[$ParResultArrayElement["OPT_POS_2"]])) {
				$tableContent[$ParResultArrayElement["OPT_POS_2"]][] = $ParResultArrayElement["VALUE"];
			} else {
				$tableContent[$ParResultArrayElement["OPT_POS_2"]][] = $sba_libSba->trimOptions($ParResultArrayElement["OPT_POS_2"]);
				$tableContent[$ParResultArrayElement["OPT_POS_2"]][] = $ParResultArrayElement["VALUE"];
			}

		}
	}
	$content = "";
	if (is_array($tableContent)) {
		foreach($tableContent as $tableContentKey => $tableContentElement) {
			$content .= implode("	",$tableContentElement)."\n";
		}
		}
	$return .= "	".implode("	",$header)."\n".$content;

	return $return;
	}

	private function GenExcelXorY($ParResultArray){
		global $sba_libSba;

		$return = "";
		if (is_array($ParResultArray)) {
			$tableContent = array();
			foreach($ParResultArray as $key => $element) {
				$header[] = $sba_libSba->trimOptions($element["OPT_POS_1"]);
				$ExcelContent[] = $element["VALUE"];
			}
		}
		$return .= implode("\t",$header)."\n\t".implode("\t",$ExcelContent);
		return $return;
	}

	private function GenExcelText($ParResultArray){
		$return ="";

		for($i = 0; $i < sizeof($ParResultArray["ANSWER"]); $i++){
			$text = str_replace("\n", "\n\t", $ParResultArray["ANSWER"][$i]);
			$return .= "\t".$text."\n";
		}
		return $return;
	}

	private function genExcelForLS($ParResultArray, $ParQuestionNumber,$ParQuestionsArray){

		global $sba_libSba;
		$return .= "#\t問題";
		for($i = 0; $i < $ParQuestionsArray[$ParQuestionNumber]["OPTION_NUM"]; $i++){
			$return .= "\t".$sba_libSba->trimOptions($ParResultArray[0][$i]["OPT_POS_1"]);
		}

		for($i = 0; $i < $ParQuestionsArray[$ParQuestionNumber]["QUESTION_NUM"]; $i++){
			if (is_array($ParResultArray[$i])) {
				$q_num = $i + 1;
				$return .= "\n";
				$return .= "\t{$q_num}";
				$return .= "\t".$ParQuestionsArray[$ParQuestionNumber]["QUESTION_QUESTIONS"][$i];
				foreach($ParResultArray[$i] as $key => $element) {
					$return .= "\t".$element['VALUE'];
				}

			}
		}

		return $return;
	}
	/*get questions of surveys with answers*/
	public function genAnswersDOC($ParSurveyID){
		global $Lang, $ies_cfg;

		$content .= $style;
		$RespondentList = $this->getSurveyAnsweredRespondentList($ParSurveyID);
		$RespondentList = (is_array($RespondentList)) ? $RespondentList : array();

		###Survey Details
		$SurveyDetails = $this->getSurveyDetails($ParSurveyID);
		$SurveyDetails = current($SurveyDetails);
		$Survey_title = $SurveyDetails["Title"];
		$Survey_desc = $SurveyDetails["Description"];
		$Survey_question = $SurveyDetails["Question"];
		$Survey_startdate = date("Y-m-d", strtotime($SurveyDetails["DateStart"]));
		$Survey_enddate = date("Y-m-d", strtotime($SurveyDetails["DateEnd"]));
		$html_TypeName = $SurveyDetails["SurveyTypeName"];
		#########################################

		foreach($RespondentList as $value){
			$Survey_Respondent = $value['Respondent'];
			$Survey_Respondent_Nature = $value['RespondentNature'];
			$Survey_Valid = $value['Status'];
			if($Survey_Valid == $ies_cfg['DB_IES_SURVEY_ANSWER_status']['Valid']){
				$survey_ans_detail_arr = $this->getSurveyAnsDetails($ParSurveyID, $Survey_Respondent, $Survey_Respondent_Nature);

				$Survey_Answer = $survey_ans_detail_arr["Answer"];
				$Survey_DateInput = $survey_ans_detail_arr["DateInput"];
				$Survey_Validity = $survey_ans_detail_arr["Status"];
				$Survey_DateModified = date("Y-m-d", strtotime($survey_ans_detail_arr["DateModified"]));

				$AnsArry = $this->splitQuestionnaireAnswers($Survey_question, $Survey_Answer);
				###content##
				$content .= "<h3>{$Lang['IES']['Respondent']} : {$Survey_Respondent}</h3>";
				$content .= "<h3>{$Lang['IES']['QuestionnaireDescription']} : {$Survey_desc}</h3>";
				$content .= "<h3>{$Lang['IES']['InterviewDate']} : {$Survey_DateModified}</h3>";

				$content .= $this->GenWordQuestionAnswer($AnsArry, 'Answer');
				$content .= "<br clear=\"all\" style=\"page-break-before:always\" />";
			}

		}
		return $content;
	}
	/*get questions of surveys*/
	public function genQuestionsDOC($SurveyID){
		global $Lang, $ies_cfg;

		$SurveyDetails = $this->getSurveyDetails($SurveyID);
		$SurveyDetails = current($SurveyDetails);

		$Survey_question = $SurveyDetails["Question"];
		$SurveyStartDate = $SurveyDetails["DateStart"];
		$SurveyEndDate = $SurveyDetails["DateEnd"];
		$Survey_title = $SurveyDetails["Title"];
		$Survey_desc = $SurveyDetails["Description"];
		$Survey_type = $SurveyDetails["SurveyType"];

		$question_array = $this->breakQuestionsString($Survey_question);

		//***********************************CONTENT****************************************
		$content  = "";
		$content .= "<span style='font-style:Times New Roman;'>";
		$content .= "<h3>{$Lang['IES']['QuestionnaireTitle']}:".$Survey_title."</h3>";
		$content .= "<h3>{$Lang['IES']['QuestionnaireDescription']}:".$Survey_desc."</h3>";

		if($Survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]){

			$content .= "<h3>{$Lang['IES']['Questionnaire_InterviewRemark1']}:".$SurveyDetails["Remark1"]."</h3>";
			$content .= "<h3>{$Lang['IES']['Questionnaire_InterviewRemark2']}:".$SurveyDetails["Remark2"]."</h3>";
			$content .= "<h3>{$Lang['IES']['Questionnaire_InterviewRemark3']}:".$SurveyDetails["Remark3"]."</h3>";
			$content .= "<h3>{$Lang['IES']['Questionnaire_InterviewRemark4']}:".$SurveyDetails["Remark4"]."</h3>";

		}else if ($Survey_type != $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]){

			$content .= "<h3>{$Lang['IES']['Questionnaire_ObserveRemark1']}:".$SurveyDetails["Remark1"]."</h3>";
			$content .= "<h3>{$Lang['IES']['Questionnaire_ObserveRemark2']}:".$SurveyDetails["Remark2"]."</h3>";
			$content .= "<h3>{$Lang['IES']['Questionnaire_ObserveRemark3']}:".$SurveyDetails["Remark3"]."</h3>";
			$content .= "<h3>{$Lang['IES']['Questionnaire_ObserveRemark4']}:".$SurveyDetails["Remark4"]."</h3>";

		}

		$content .= "<h3>{$Lang['IES']['Respondent']}: ________________________</h3>";

		$content .= $this->GenWordQuestionAnswer($question_array, 'Question');
		$content .= "</span>";
		return $content;
	}

	/*get analysis of surveys with mappings*/
	public function genAnalysisDOC($ParSurveyID){
		global $Lang, $sba_thisStudentID, $ies_cfg;

		$folder_target = $this->folder_target;
		$header = $this->header;



		$content = "";
		$img_paths=array();
		##get survey details
		$SurveyDetails = $this->getSurveyDetails($ParSurveyID);
		$SurveyDetails = current($SurveyDetails);
		$Survey_question = $SurveyDetails["Question"];
		$Survey_title = $SurveyDetails["Title"];
		$Survey_desc = $SurveyDetails["Description"];

		#######OverALL PART
		$content .= "<h3>Overall Anaylsis</h3>";
		$questionsArray = $this->breakQuestionsString($Survey_question);
		$this->set_display_survey_mapping_option(false);
		for($i = 1; $i <= sizeof($questionsArray) ; $i++){
			$content .= $this->GetIndividualAnswerDisplay($ParSurveyID, $i);

			if ($this->isQuestionTypehasGraph($questionsArray[$i]['TYPE'])){


				$img_path=libSbaExport::getSaveFileName($ParSurveyID.'-'.$i.'-['.$questionsArray[$i]['TITLE']."]-bar.png");
				$img_paths[]=$img_path;
				$content .= "<img src='images/$img_path'/>";

				if ($this->isQuestionTypehasPieChart($questionsArray[$i]['TYPE'])){//no pie for likert
				    $img_path=libSbaExport::getSaveFileName($ParSurveyID.'-'.$i.'-['.$questionsArray[$i]['TITLE']."]-pie.png");
				    $img_paths[]=$img_path;
				    $content .= "<img src='images/$img_path'/>";
				}



			}
		}
		if (empty($content)) {
			$content .= $Lang['IES']['NoRecordAtThisMoment'];
		}
		$surveydetail_print .= "<h3>{$Lang['IES']['QuestionnaireTitle']} : {$Survey_title}</h3>";
		$surveydetail_print .= "<h3>{$Lang['IES']['QuestionnaireDescription']} : {$Survey_desc}</h3>";
		$content = $surveydetail_print.$content."<br clear=\"all\" style=\"page-break-before:always\" />";
		##############

                ######MAPPING ANAYLSIS PART####
		$SurveyMapping = $this->loadSurveyMapping($ParSurveyID,$sba_thisStudentID);
		if(is_array($SurveyMapping)){
			$content .= "<h3>Group Anaylsis</h3>";
			foreach($SurveyMapping as $num=>$value){
				$SurveyID = $value['SURVEYID'];
				$XAxis = $value['XMAPPING'];
				$YAxis = $value['YMAPPING'];
				$mappingtitle = $value['MAPPINGTITLE'];
				$content .= "<b>{$mappingtitle}<b><br />";
				$content .= $this->getCombineBoxContent($SurveyID,$XAxis,$YAxis)."<br />";

				$img_path=libSbaExport::getSaveFileName($ParSurveyID.'-Combine'.($num+1).'-['.$mappingtitle."]-Bar.png");
				$img_paths[]=$img_path;
				$content .= "<img src='images/$img_path'/>";


			}
		}
                #############
                return array($content, $img_paths);
        }

	/*choose the function to generate question format: see functions below*/
	private function GenWordQuestionAnswer($array, $mode){

		global $ies_cfg;

		$GenFunctions=array(
			$ies_cfg["Questionnaire"]["QuestionType"]["MC"]=> 'GenWordMC'.$mode,
			$ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]=> 'GenWordMP'.$mode,
			$ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]=> 'GenWordShort'.$mode,
			$ies_cfg["Questionnaire"]["QuestionType"]["FillLong"]=> 'GenWordLong'.$mode,
			$ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]=> 'GenWordLikert'.$mode,

		);
		if(!empty($array)){
			$count = 1;

			foreach($array as $value){

				$question_value= ($mode=='Answer')? $value["QUESTION"] : $value;

				$content .= "<b>".$count.". ".$question_value["TITLE"]."</b><br/>";
				$content .= $this->$GenFunctions[$question_value["TYPE"]]($value);
				$content .= "<br/>";
				$count++;
			}
		}

		return $content;
	}

	private function GenWordMCAnswer($ParArry){
		$x = "";

		for($i = 0; $i < $ParArry["QUESTION"]["OPTION_NUM"]; $i++){
			if($i == $ParArry["ANSWER"] && $ParArry["ANSWER"] != ""){
				$x .= "&#10687; ".$ParArry["QUESTION"]["OPTIONS"][$i]."<br />";
			}
			else{
				$x .= "&#9675; ".$ParArry["QUESTION"]["OPTIONS"][$i]."<br />";
			}
		}
		return $x;
	}

	private function GenWordMPAnswer($ParArry){
		global $ies_cfg;
		$x = "";
		$AnsArry = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"], $ParArry["ANSWER"]);
		for($i = 0; $i < $ParArry["QUESTION"]["OPTION_NUM"]; $i++){
			if($AnsArry[$i] == 1){
				$x .= "&#9745; ".$ParArry["QUESTION"]["OPTIONS"][$i]."<br />";
			}
			else{
				$x .= "&#9744; ".$ParArry["QUESTION"]["OPTIONS"][$i]."<br />";
			}
		}
		return $x;
	}

	private function GenWordShortAnswer($ParArry){
		global $ies_cfg;
		$x = "<br/>".$ParArry["ANSWER"]."<br/>";
		return $x;
	}

	private function GenWordLongAnswer($ParArry){
		global $ies_cfg;
		$x = "<br/>".nl2br($ParArry["ANSWER"])."<br/>";
		return $x;
	}

	private function GenWordLikertAnswer($ParArry){
		global $ies_cfg;
		$x = "";
		$AnsArry = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"], $ParArry["ANSWER"]);

		$x .= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" ><tr><td></td>";

	    for($i=0; $i < $ParArry["QUESTION"]["OPTION_NUM"]; $i++)
		{
			$x .= "<td>".$ParArry["QUESTION"]["OPTIONS"][$i]."</td>";
		}
		$x .= "</tr>";
		for($i = 0; $i < $ParArry["QUESTION"]["QUESTION_NUM"]; $i++){
			$x .= "<tr>";
			$x .= "<td>".$ParArry["QUESTION"]["QUESTION_QUESTIONS"][$i]."</td>";
			for($j = 0; $j < $ParArry["QUESTION"]["OPTION_NUM"]; $j++){
	//			DEBUG_R($AnsArry[$i]);
				if($AnsArry[$i] == $j && $AnsArry[$i] != ""){
					$x .= "<td>&#10687;</td>";
				}
				else{
					$x .= "<td>&#9675;</td>";
				}
			}
			$x .= "</tr>";

		}
		$x .= "</table>";
		return $x;
	}

	private function GenWordMCQuestion($ParArry){
		global $image_path;
		$x = "";

		for($i = 0; $i < $ParArry["OPTION_NUM"]; $i++){
			$x .= "&#9675; ".$ParArry["OPTIONS"][$i]."<br />";
		}
		return $x;
	}

	private function GenWordMPQuestion($ParArry){
		global $ies_cfg;
		$x = "";

		for($i = 0; $i < $ParArry["OPTION_NUM"]; $i++){

				$x .= "&#9744; ".$ParArry["OPTIONS"][$i]."<br />";
		}
			return $x;
		}

	private function GenWordShortQuestion($ParArry){
			global $ies_cfg;
			$x = "_________________________________________________________________________<br />";
		return $x;
	}

	private function GenWordLongQuestion($ParArry){
		global $ies_cfg;
		for($i = 0; $i < 5; $i++){
			$x .= "_________________________________________________________________________<br />";
		}

		return $x;
	}

	private function GenWordLikertQuestion($ParArry){
		global $ies_cfg;
		$x = "";

		$x .= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" width='100%'><tr><td style='border:1px solid black;'>Question</td>";

	    for($i=0; $i < $ParArry["OPTION_NUM"]; $i++)
		{
			$x .= "<td style='border:1px solid black;'>".$ParArry["OPTIONS"][$i]."</td>";
		}
		$x .= "</tr>";
		for($i = 0; $i < $ParArry["QUESTION_NUM"]; $i++){
			$x .= "<tr>";
			$x .= "<td style='border:1px solid black;'>".$ParArry["QUESTION_QUESTIONS"][$i]."</td>";
			for($j = 0; $j < $ParArry["OPTION_NUM"]; $j++){
				$x .= "<td style='border:1px solid black; text-align:center;'>&#9675;</td>";

			}
			$x .= "</tr>";

		}
		$x .= "</table>";
		return $x;
	}
}


?>
