<?

/** [Modification Log] Modifying By: Connie
 * *******************************************
 * *******************************************
 */


class libies_move_student extends libies {

	function getInvolveTable(){
		$table = array();
		$table[]  = 'IES_SCHEME_STUDENT';
		$table[]  = 'IES_STAGE_STUDENT';
		$table[]  = 'IES_STAGE_HANDIN_BATCH';
		$table[]  = 'IES_TASK_STUDENT';
		$table[]  = 'IES_TASK_HANDIN';
		$table[]  = 'IES_TASK_HANDIN_SNAPSHOT';
		$table[]  = 'IES_QUESTION_HANDIN';
		$table[]  = 'IES_MARKING';
		$table[]  = 'IES_BIBLIOGRAPHY';
		return $table;
	}
	function getBackupDate(){
		return '20110914';
	}

	function CopyStudentWithScheme($srcSchemeID,$trgSchemeID,$studentID)
	{

	$li = new libdb();

	$res = array();

	$SuccessArr = array();
	$li->Start_Trans();

	/** move data start here (by Connie)**/

	/*********************************/
	/** Update Scheme Student Handin **/
	/*********************************/
	// check student exist in the target schemeID, if exist, return error
	//copy student to the scheme first, update the schemeid with new schemeid
	//$copyResult = $li->updateStudentSchemeID($srcSchemeID, $trgSchemeID, $studentID,$UserID); //debug fai must be reopen

	//$result = $this->GET_SCHEME_DETAIL($srcSchemeID); // may useful later
	$SchemeStudentResult = $this->UpdateTableID_userID("IES_SCHEME_STUDENT","SchemeID",$srcSchemeID, $trgSchemeID, $studentID);
	$SuccessArr['SchemeStudentResult']=$SchemeStudentResult;


	/********************************/
	/** Update Scheme Bibliography **/
	/*******************************/

	$SchemeBibliArr = $this->getTableInfoBiblio('IES_BIBLIOGRAPHY',$srcSchemeID,$studentID);

	for($j = 0,$j_max = count($SchemeBibliArr); $j < $j_max; $j++){
		$BiblioIDStr = $SchemeBibliArr[$j]['bibliographyID'];
		$ModiTimeStr = $SchemeBibliArr[$j]['DateModified'];

		$stageHandinResult =  $this->UpdateID_byPK('IES_BIBLIOGRAPHY',"SchemeID",$trgSchemeID,"bibliographyID",$BiblioIDStr,$studentID);
		$ModiTimeResult = $this->UpdateID_byPK('IES_BIBLIOGRAPHY',"DateModified",$ModiTimeStr,"bibliographyID",$BiblioIDStr,$studentID);
		$SuccessArr['stageHandinResult'.$j]=$stageHandinResult;
		$SuccessArr['ModiTimeResult1'.$j]=$ModiTimeResult;

	}
	/**end part**/


	//There is a assum that the number of stage in src and trg are equal
	$srcStageInfo = $this->getTableInfoOrderBy('IES_STAGE', 'SchemeID',$srcSchemeID,'Sequence');
	$trgStageInfo = $this->getTableInfoOrderBy('IES_STAGE', 'SchemeID',$trgSchemeID,'Sequence');


	for($i = 0,$i_max = count($srcStageInfo); $i < $i_max; $i++){
		$srcStageID = $srcStageInfo[$i]['StageID'];
		$trgStageID = $trgStageInfo[$i]['StageID'];

		/*********************************/
		/** Update Stage Student Handin **/
		/*********************************/
		$StageStudentResult = $this->UpdateTableID_userID("IES_STAGE_STUDENT","StageID",$srcStageID, $trgStageID, $studentID);
		$SuccessArr['$StageStudentResult'.$i]=$StageStudentResult;

		/*******************************/
		/** Update Stage Handin batch **/
		/*******************************/
		/** Update the IES_STAGE_HANDIN_BATCH StageID**/
		$StageHandinArr = $this->getTableInfoSTAGE('IES_STAGE_HANDIN_BATCH',$srcStageID,$studentID);
		for($j = 0,$j_max = count($StageHandinArr); $j < $j_max; $j++){
			$BatchIDStr = $StageHandinArr[$j]['BatchID'];
			$ModiTimeStr = $StageHandinArr[$j]['DateModified'];

			$stageHandinResult =  $this->UpdateID_byPK('IES_STAGE_HANDIN_BATCH',"StageID",$trgStageID,"BatchID",$BatchIDStr,$studentID);
			$ModiTimeResult = $this->UpdateID_byPK('IES_STAGE_HANDIN_BATCH',"DateModified",$ModiTimeStr,"BatchID",$BatchIDStr,$studentID);

			$SuccessArr['stageHandinResult'.$i.$j]=$stageHandinResult;
			$SuccessArr['ModiTimeResult2'.$i.$j]=$ModiTimeResult;

		}
		/**end part**/

		/********************************/
		/** Update Stage Marking Score **/
		/********************************/

		/** Update the IES_MARKING StageMarkCriteriaID**/
		$srcMarkCriInfo = $this->getTableInfo('IES_STAGE_MARKING_CRITERIA', 'StageID',$srcStageID);
		$trgMarkCriInfo = $this->getTableInfo('IES_STAGE_MARKING_CRITERIA', 'StageID',$trgStageID);

		for($j = 0,$j_max = count($srcMarkCriInfo); $j < $j_max; $j++){
			$srcStageMarkCriID = $srcMarkCriInfo[$j]['StageMarkCriteriaID'];
			$trgStageMarkCriID = $trgMarkCriInfo[$j]['StageMarkCriteriaID'];

			$MarkingArr = $this->getTableInfo('IES_MARKING','StageMarkCriteriaID',$srcStageMarkCriID);

			for($k = 0,$k_max = count($MarkingArr); $k < $k_max; $k++){
				$MarkingIDStr = $MarkingArr[$k]['MarkingID'];
				$ModiTimeStr = $MarkingArr[$k]['DateModified'];

				$MarkingResult = $this->UpdateID_Marking('IES_MARKING',"StageMarkCriteriaID",$trgStageMarkCriID,"MarkingID",$MarkingIDStr);
				$ModiTimeResult = $this->UpdateID_Marking('IES_MARKING',"DateModified",$ModiTimeStr,"MarkingID",$MarkingIDStr);

				$SuccessArr['MarkingResult'.$i.$j.$k]=$MarkingResult;
				$SuccessArr['ModiTimeResult3'.$i.$j.$k]=$ModiTimeResult;

			}

		}
		/**end part**/


		$srcTaskInfo = $this->getTableInfoOrderBy('IES_TASK', 'StageID',$srcStageID,'Sequence');
		$trgTaskInfo = $this->getTableInfoOrderBy('IES_TASK', 'StageID',$trgStageID,'Sequence');
		for($j = 0,$j_max = count($srcTaskInfo); $j < $j_max; $j++){

			$srcTaskID = $srcTaskInfo[$j]['TaskID'];
			$trgTaskID = $trgTaskInfo[$j]['TaskID'];

			$TaskStudentResult = $this->UpdateTableID_userID("IES_TASK_STUDENT","TaskID",$srcTaskID, $trgTaskID, $studentID);
			$SuccessArr['TaskStudentResult'.$i.$j]=$TaskStudentResult;

			/************************/
			/** Update Task Handin **/
			/************************/

			/** Update the IES_TASK_HANDIN TaskID**/
			$TaskHandinArr = $this->getTableInfoTASK('IES_TASK_HANDIN',$srcTaskID,$studentID);
			for($k = 0,$k_max = count($TaskHandinArr); $k < $k_max; $k++){
				$AnswerIDStr = $TaskHandinArr[$k]['AnswerID'];
				$ModiTimeStr = $TaskHandinArr[$k]['DateModified'];

				$TaskHandinResult = $this->UpdateID_byPK('IES_TASK_HANDIN',"TaskID",$trgTaskID,"AnswerID",$AnswerIDStr,$studentID);
				$ModiTimeResult = $this->UpdateID_byPK('IES_TASK_HANDIN',"DateModified",$ModiTimeStr,"AnswerID",$AnswerIDStr,$studentID);

				$SuccessArr['TaskHandinResult'.$i.$j.$k]=$TaskHandinResult;
				$SuccessArr['ModiTimeResult4'.$i.$j.$k]=$ModiTimeResult;

			}
			/**end part**/

			/*********************************/
			/** Update Task Handin Snapshot **/
			/*********************************/

			/** Update the IES_TASK_HANDIN_SNAPSHOT TaskID**/
			$TaskSnapArr = $this->getTableInfoTASK('IES_TASK_HANDIN_SNAPSHOT',$srcTaskID,$studentID);
			for($k = 0,$k_max = count($TaskSnapArr); $k < $k_max; $k++){
				$SnapshotAnswerIDStr = $TaskSnapArr[$k]['SnapshotAnswerID'];
				$AnswerTimeStr = $TaskSnapArr[$k]['AnswerTime'];

				$TaskSnapResult =  $this->UpdateID_byPK('IES_TASK_HANDIN_SNAPSHOT',"TaskID",$trgTaskID,"SnapshotAnswerID",$SnapshotAnswerIDStr,$studentID);
				$AnswerTimeResult = $this->UpdateID_byPK('IES_TASK_HANDIN_SNAPSHOT',"AnswerTime",$AnswerTimeStr,"SnapshotAnswerID",$SnapshotAnswerIDStr,$studentID);

				$SuccessArr['TaskSnapResult'.$i.$j.$k]=$TaskSnapResult;
				$SuccessArr['AnswerTimeResult'.$i.$j.$k]=$AnswerTimeResult;
			}
			/**end part**/


			$srcStepInfo = $this->getTableInfoOrderBy('IES_STEP', 'TaskID',$srcTaskID,'Sequence');
			$trgStepInfo = $this->getTableInfoOrderBy('IES_STEP', 'TaskID',$trgTaskID,'Sequence');

			for($a = 0,$a_max = count($srcStepInfo); $a < $a_max; $a++){

				$srcStepID = $srcStepInfo[$a]['StepID'];
				$trgStepID = $trgStepInfo[$a]['StepID'];


				/************************/
				/** Update Step Handin **/
				/************************/
				$QuesHandinArr = $this->getTableInfoQuest('IES_QUESTION_HANDIN',$srcStepID,$studentID);
				for($k = 0,$k_max = count($QuesHandinArr); $k < $k_max; $k++){
					$AnswerIDStr = $QuesHandinArr[$k]['AnswerID'];
					$ModiTimeStr = $QuesHandinArr[$k]['DateModified'];

					$QuestHandinResult = $this->UpdateID_byPK('IES_QUESTION_HANDIN',"StepID",$trgStepID,"AnswerID",$AnswerIDStr,$studentID);
					$ModiTimeResult = $this->UpdateID_byPK('IES_QUESTION_HANDIN',"DateModified",$ModiTimeStr,"AnswerID",$AnswerIDStr,$studentID);

					$SuccessArr['$QuestHandinResult'.$i.$j.$a.$k]=$QuestHandinResult;
					$SuccessArr['ModiTimeResult5'.$i.$j.$a.$k]=$ModiTimeResult;

				}
				/**end part**/

			}

		}

	}

		/** end move**/



	  if (in_array(false, $SuccessArr))
	  {
	  	$li->RollBack_Trans();
	    $msg = "false";
	  }
	  else
	  {
	    $li->Commit_Trans();
	    $msg = "true";
	  }



		return $SuccessArr;
//	  exit();

	}

		  	/** For update table ID (by Primary Key)

        * @owner : Connie (20110914)

        * @param : $ParTableName eg.IES_TASK_HANDIN_SNAPSHOT
        * @param : $NewTableID(eg.TaskID, AnswerTime,DateModified)
        * @param : $ParNewID (eg.new TaskID, AnswerTime,DateModified)
        * @param : $ParOldTaskID (old TaskID)
        * @param : $TablePKID and $PKID (SnapshotAnswerID)
        * @param : $ParUserID eg. UserID
        * @return : true or false

        */
	  function UpdateID_byPK($ParTableName,$NewTableID,$ParNewID,$TablePKID,$PKID,$ParUserID)
	  {
	    global $intranet_db;
	    $li = new libdb();

	  	$sql = "Update {$intranet_db}.{$ParTableName} set {$NewTableID} ='".$ParNewID."' where {$TablePKID} = '{$PKID}' and UserID='".$ParUserID."'";

	  	$returnResult=$li->db_db_query($sql);

	  //	debug_r($sql);

	  	return $returnResult;
	  }


		/** For update table ID (For task handin and snapshot)

        * @owner : Connie (20110914)

        * @param : $ParTableName eg.IES_MARKING
        * @param : $NewTableID(eg.SnapshotAnswerID, AnswerTime,DateModified)
        * @param : $ParNewID (eg.new SnapshotAnswerID, AnswerTime,DateModified)
        * @param : $TablePKID and $PKID (MarkingID)
        * @param : $ParUserID eg. UserID
        * @return : true or false

        */
	  function UpdateID_Marking($ParTableName,$NewTableID,$ParNewID,$TablePKID,$PKID)
	  {
	    global $intranet_db;
	    $li = new libdb();

	  	$sql = "Update {$intranet_db}.{$ParTableName} set {$NewTableID} ='".$ParNewID."' where {$TablePKID} = '{$PKID}'";
	  	$returnResult=$li->db_db_query($sql);

	  //	debug_r($sql);

	  	return $returnResult;
	  }


	  /** Get Table details  (based on TaskID and UserID)

        * @owner : Connie (20110914)
        *
        * @param : String $ParTableName (IES_TASK_HANDIN_SNAPSHOT)

        * @param : String $ParTaskID (TaskID)

        * @param : $ParUserID (UserID)

        * @return : Array(s) of table record (there may be more than one record)

        *

        */
	  function getTableInfoSTAGE($ParTableName,$ParStageID,$ParUserID){
		global $intranet_db;
		$li = new libdb();
		$sql =  "SELECT * FROM {$intranet_db}.{$ParTableName} WHERE StageID= {$ParStageID} and UserID = '{$ParUserID}'";
		//debug_r($sql);
		$returnArr = $li->returnArray($sql);
		return $returnArr;
	  }


	   /** Get Table details  (based on TaskID and UserID)

        * @owner : Connie (20110914)
        *
        * @param : String $ParTableName (IES_TASK_HANDIN_SNAPSHOT)

        * @param : String $ParTaskID (TaskID)

        * @param : $ParUserID (UserID)

        * @return : Array(s) of table record (there may be more than one record)

        *

        */
	  function getTableInfoTASK($ParTableName,$ParTaskID,$ParUserID){
		global $intranet_db;
		$li = new libdb();
		$sql =  "SELECT * FROM {$intranet_db}.{$ParTableName} WHERE TaskID= '{$ParTaskID}' and UserID = {$ParUserID}";
		//debug_r($sql);
		$returnArr = $li->returnArray($sql);
		return $returnArr;
	  }


	  /** Get Table details  (based on TaskID and UserID)

        * @owner : Connie (20110914)
        *
        * @param : String $ParTableName (IES_TASK_HANDIN_SNAPSHOT)

        * @param : String $ParTaskID (TaskID)

        * @param : $ParUserID (UserID)

        * @return : Array(s) of table record (there may be more than one record)

        *

        */
	  function getTableInfoBiblio($ParTableName,$ParSchemeID,$ParUserID){
		global $intranet_db;
		$li = new libdb();
		$sql =  "SELECT * FROM {$intranet_db}.{$ParTableName} WHERE SchemeID= '{$ParSchemeID}' and UserID = '{$ParUserID}'";
		//debug_r($sql);
		$returnArr = $li->returnArray($sql);
		return $returnArr;
	  }


	   /** Get Table details  (based on StepID and UserID)

        * @owner : Connie (20110914)
        *
        * @param : String $ParTableName (IES_QUESTION_HANDIN)

        * @param : String $ParStepID (StepID)

        * @param : $ParUserID (UserID)

        * @return : Array(s) of table record (there may be more than one record)

        *

        */
	   function getTableInfoQuest($ParTableName,$ParStepID,$ParUserID){
		global $intranet_db;
		$li = new libdb();
		$sql =  "SELECT * FROM {$intranet_db}.{$ParTableName} WHERE StepID= '{$ParStepID}' and UserID = '{$ParUserID}'";
		//debug_r($sql);
		$returnArr = $li->returnArray($sql);
		return $returnArr;
	   }

	function updateStudentSchemeID($srcSchemeID, $trgSchemeID, $studentID,$creatorID){
		global $intranet_db;

		if(is_numeric($srcSchemeID) && is_numeric($trgSchemeID) && is_numeric($studentID)){
	//	$sql = 'update '.$intranet_db.'.IES_SCHEME_STUDENT set SchemeID = '.$trgSchemeID.' where SchemeID = '.$srcSchemeID.' and UserID = '.$studentID;

/*			$sql = 'insert into '.$intranet_db.'.IES_SCHEME_STUDENT (SchemeID , UserID,Score,DateInput,InputBy,DateModified,ModifyBy)
					select
						'.$trgSchemeID.', UserID,Score,now(),'.$creatorID.',now(),'.$creatorID.'
					from
						'.$intranet_db.'.IES_SCHEME_STUDENT
					where
						schemeID = '.$srcSchemeID.' and userid ='.$studentID.' ';
						*/

					$sql = 'insert into '.$intranet_db.'.IES_SCHEME_STUDENT (SchemeID , UserID,Score,DateInput,InputBy,DateModified,ModifyBy)
					select
						'.$trgSchemeID.', UserID,Score,now(),'.$creatorID.',now(),'.$creatorID.'
					from
						'.$intranet_db.'.IES_SCHEME_STUDENT
					where
						schemeID = \''.$srcSchemeID.'\' and userid =\''.$studentID.'\' ';
		//	debug_r($sql);

			$result  = $this->db_db_query($sql);
			$id = $this->db_insert_id();
		//	debug_r('is is '.$id);

			$e = mysql_error();
			echo 'error-->';
		//	debug_r($e);
			return $result;
		}else{
			return false;
		}
	}


	function updateStageHandInBatch($srcStageID,$trgStageID,$studentID, $creatorID){
		global $intranet_db;

		if(is_numeric($srcStageID)&&is_numeric($trgStageID)&&is_numeric($studentID)&&is_numeric($creatorID)){
			$sql = 'insert into IES_STAGE_HANDIN_BATCH (StageID,UserID ,BatchStatus,SubmissionDate,DateInput,InputBy,DateModified,ModifyBy)
					select
						'.$trgStageID.' ,UserID,BatchStatus,SubmissionDate,DateInput,InputBy,DateModified,ModifyBy
					from
						'.$intranet_db.'.IES_STAGE_HANDIN_BATCH
					where
						StageID = \''.$srcStageID.'\' and UserID = \''.$studentID.'\'';

		//	debug_r($sql);
			$result = $this->db_db_query($sql);
			return $result;
		}else{
			return false;
		}
	}
}
?>