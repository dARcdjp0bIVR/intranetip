<?php
/**
 * 2012-10-22 Mick
 *      New function getSurveyIDs() to get all Surveys
 *
 * 2012-08-22 Mick
 *	Implement getExportDisplay()
 *
 * */

include_once($intranet_root."/includes/sba/libies_survey.php");


abstract class libSbaStepHandinQuestionnaireEDIT extends libSbaStepHandin{
	


	public function __construct($parTaskID, $parStepID=0, $parQuestion='', $parAnswerId=0) {
		global $sba_cfg;

		parent::__construct($parTaskID, $parStepID, $parAnswerId);
	}
	
	public function getCaption(){
		return "Survey Edit";
	}
	
	public function setStudentAnswer($SurveyID_ary){
		global $ies_cfg;
		
		$this->setAnswer(implode($ies_cfg["answer_within_question_sperator"], $SurveyID_ary));
	}
		
	public function getQuestionDisplay(){
		global $sba_thisStudentID , $sba_allowStudentSubmitHandin,$Lang;
		$objIES = new libies();
		$objSurvey = new libies_survey();
		$SurveyType = $this->getQuestionnaireType();

		if($this->getRawAnswer() == ''){
			//do nothing
		}else{
			$surveyIdAry = explode(',',$this->getRawAnswer());
//			$survey_map_arr = $objIES->parseSurveyMapString($this->getRawAnswer());
			$result = $objSurvey->getStudentSurveyBySurveyId($surveyIdAry,$orderBy= '  order by s.DateModified desc , s.title');
		}

		$thisJsAction = 'alert(\''.$Lang['SBA']['StudentClickThisToSubmitHandin'].'\')';
		if($sba_allowStudentSubmitHandin){
			$thisJsAction = "newWindow('/home/eLearning/sba/?mod=survey&task=createForm&StepID=".$this->getStepID()."&TaskID=".$this->getTaskID()."&scheme_id=".$this->getSchemeID()."&SurveyType=".$SurveyType."',10)";
		}

		$h_studentAns = '';
		$totalHandin = 0;
		$totalHandin = count($result);
		if($totalHandin > 0){
			$stepId = $this->getStepID();
			$taskId = $this->getTaskID();
			for($i = 0;$i <  $totalHandin;$i++){
				$_title = $result[$i]['Title'];
				$_surveyID = $result[$i]['SurveyID'];
				$_DateInput = $result[$i]['DateInput'];
				
				$_surveyID = base64_encode($_surveyID);

$var = <<<HTML
					<tr>
						<td>
							<a href="javascript:void(0)" class="task_form" title="View" onclick="newWindow('/home/eLearning/sba/?mod=survey&task=createForm&SurveyID={$_surveyID}&StepID={$stepId}&TaskID={$taskId}&SurveyType={$SurveyType}',10)" >{$_title}</a>
						</td>
                        <td><span class="object_edit_date">({$Lang['IES']['LastModifiedDate']} : {$_DateInput})</span></td>
                        <td><!--<div class="table_row_tool"><a href="#" class="delete" title="{$Lang['IES']['Delete2']}"></a></div>--></td>
					</tr>
    
HTML;

					$h_studentAns .= $var;
				$h_studentAns .= "\n";
			}
		}

		$QuestionHTML = "<div class=\"IES_task_form\" style=\"margin-top:30px\">
                        	<h4 class=\"icon_stu_edit\"><strong>".$this->getQuestionTypeName()."</strong></h4>
                        	<span class=\"guide\">{$Lang['IES']['Submitted']}: {$totalHandin} </span>
                        	<div class=\"wrapper\">
								<table class=\"sub_form_table\">
								{$h_studentAns}
									<tr><td> <div class=\"stage_left_tool\"> <a href=\"javascript:void(0)\" onclick=\"{$thisJsAction}\" class=\"add\" title=\"{$Lang['SBA']['Survey']['NewSurvey']}\"><span>{$Lang['SBA']['Survey']['NewSurvey']}</span></a> </div></td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td></tr>
								</table>
                        	</div>
                		 </div>";

		return $QuestionHTML;
	}
	
	public function getEditQuestionDisplay(){
		global $Lang;

		//new support preview of this question for teacher
		$SurveyType = $this->getQuestionnaireType();

		$h_newSurveyCaption = $Lang['SBA']['Survey']['NewSurveyCaption'][$SurveyType];
		$h_newSurveyCaption = ($h_newSurveyCaption == '') ? $Lang['SBA']['Survey']['NewSurvey'] : $h_newSurveyCaption;

		$thisJsAction = "newWindow('/home/eLearning/sba/?mod=survey&task=createForm&SurveyType=".$SurveyType."',10)";
		$QuestionHTML = "<div class=\"IES_task_form\">
						<script>
						 function step_tool_validate_fail(){
							return false;
						 }
						 </script>
                        	<h4 class=\"icon_stu_edit\"><strong>".$this->getQuestionTypeName()."</strong></h4>
                        	<span class=\"guide\">{$Lang['IES']['Submitted']}:0</span>
                        	<div class=\"wrapper\">
								<table class=\"sub_form_table\">
								  <tr><td> <div class=\"stage_left_tool\"> <a href=\"javascript:void(0)\" onclick=\"{$thisJsAction}\" class=\"add\" title=\"{$Lang['SBA']['Survey']['NewSurvey']}\"><span> {$h_newSurveyCaption}</span></a> </div></td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td></tr>
								</table>

                            	<br class=\"clear\">
                        	</div>
                		 </div>
						 <input type=\"hidden\" id=\"r_step_QuestionType\" name=\"r_step_QuestionType\" value=\"".($this->getQuestionType())."\">";
		return $QuestionHTML;
	}
	
	public function getQuestionStr(){
		return null;
	}
	abstract public function getQuestionnaireType();
	abstract public function getQuestionTypeName();

	public function getDisplayAnswer(){
	//	return $this->getAnswer();
	return null;
	}
	public function getRawAnswer(){//should be an abstract function to libSbaStepHandin
		return $this->getAnswer();
	}
	public function getDisplayAnswerForMarking(){
		global $ies_cfg;
		$answer = $this->getRawAnswer();
		$surveyIdAry = array();

		if($answer != ''){
			$surveyIdAry = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"],$answer);
		}
		
		$objSurvey = new libies_survey();
		$result = $objSurvey->getStudentSurveyBySurveyId($surveyIdAry);

		$h_studentAns = ''; // store the student return ans html
		$totalHandin = count($result);
		if($totalHandin > 0){
			for($i = 0;$i <  $totalHandin;$i++){
				$_title = $result[$i]['Title'];
				$_surveyID = $result[$i]['SurveyID'];

				$_key = base64_encode("SurveyID={$_surveyID}&StudentID=".$this->getUserID()."&TaskID=".$this->getTaskID());
				
				$h_studentAns .= intval($i+1).') <a href = "javascript:void(0)" onclick="newWindow(\'/home/eLearning/sba/?mod=survey&task=viewQue&key='.$_key.'\',10)" title="View">'.$_title.'</a><br/>';
				$h_studentAns .= "\n";
			}
		}
		
		return $h_studentAns;
	}
        public function getSurveyIDs(){
                global $ies_cfg;
                $answer = $this->getRawAnswer();
		if($answer != ''){
			return explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"],$answer);
		}
                return array();
        }
}


class libSbaStepHandinSurveyEDIT extends libSbaStepHandinQuestionnaireEDIT {

	public function __construct($parTaskID,$parStepID=0, $parQuestion='', $parAnswerId=0) {
		global $sba_cfg;

		parent::__construct( $parTaskID,$parStepID, $parQuestion, $parAnswerId);
		
		$this->setQuestionType($sba_cfg['DB_IES_STEP_QuestionType']['surveyEdit'][0]);
	}
	
	public function getExportDisplay(){
		
		global $ies_cfg, $Lang;
		
		
		
		$objSurvey = new libies_survey();
		$questionaire=array();
		$num=0;
		foreach ($this->getSurveyIDs() as $surveyId){
			if ($surveyId==0) continue;
			$surveyDetails=current($objSurvey->getSurveyDetails($surveyId));
			
			$title=(++$num).'-'.$surveyId.'-['.$surveyDetails["Title"].']';
			
			$questionaire[$title]=$objSurvey->genQuestionsDOC($surveyId);//ask objSurvey to gen Question in word format
		}

		return array('questionaire'=>$questionaire);//no survey
	}
	
	public function getQuestionnaireType(){
		global $ies_cfg;
		return $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0];

	}
	public function getQuestionTypeName(){ // this function should be abstract to parent "libSbaStepHandin" not only libSbaStepHandinQuestionnaireEDIT, need to discuss with thomas
		global $sba_cfg;
		return $sba_cfg['DB_IES_STEP_QuestionType']['surveyEdit'][1];
	}
}

class libSbaStepHandinInterviewEDIT extends libSbaStepHandinQuestionnaireEDIT {

	public function __construct($parTaskID,$parStepID=0, $parQuestion='', $parAnswerId=0) {
		global $sba_cfg;
		parent::__construct($parTaskID,$parStepID, $parQuestion, $parAnswerId);
		$this->setQuestionType($sba_cfg['DB_IES_STEP_QuestionType']['interviewEdit'][0]);
	}
	
	public function getExportDisplay(){
		
		global $ies_cfg, $Lang;

		$objSurvey = new libies_survey();
		$survey=$questionaire=array();
		$num=0;
		foreach ($this->getSurveyIDs() as $surveyId){
			if ($surveyId==0) continue;
			$surveyDetails=current($objSurvey->getSurveyDetails($surveyId));
			
			$title=(++$num).'-'.$surveyId.'-['.$surveyDetails["Title"].']';
			
			$questionaire[$title]=$objSurvey->genQuestionsDOC($surveyId);//ask objSurvey to gen Question in word format
			$survey[$title]=$objSurvey->genAnswersDOC($surveyId);
		}

		return array('questionaire'=>$questionaire, 'survey'=>$survey);
	}
	
	public function getQuestionnaireType(){
		global $ies_cfg;
		return $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0];

	}

	public function getQuestionTypeName(){ // this function should be abstract to parent "libSbaStepHandin" not only libSbaStepHandinQuestionnaireEDIT, need to discuss with thomas
		global $sba_cfg;
		return $sba_cfg['DB_IES_STEP_QuestionType']['interviewEdit'][1];
	}
}

class libSbaStepHandinObserveEDIT extends libSbaStepHandinQuestionnaireEDIT {

	public function __construct($parTaskID, $parStepID=0, $parQuestion='', $parAnswerId=0) {
		global $sba_cfg;
		parent::__construct($parTaskID,$parStepID, $parQuestion, $parAnswerId);
		$this->setQuestionType($sba_cfg['DB_IES_STEP_QuestionType']['observeEdit'][0]);
	}
	
	public function getExportDisplay(){
		
		global $ies_cfg, $Lang;
		
		$answer = $this->getRawAnswer();
		if($answer != ''){
			$surveyIdAry = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"],$answer);
		}
		
		$objSurvey = new libies_survey();
		$survey=$questionaire=array();
		$num=0;
		foreach ((array)$surveyIdAry as $surveyId){
			if ($surveyId==0) continue;
			$surveyDetails=current($objSurvey->getSurveyDetails($surveyId));
			
			$title=(++$num).'-'.$surveyId.'-['.$surveyDetails["Title"].']';
			
			$questionaire[$title]=$objSurvey->genQuestionsDOC($surveyId);//ask objSurvey to gen Question in word format
			$survey[$title]=$objSurvey->genAnswersDOC($surveyId);
		}

		return array('questionaire'=>$questionaire, 'survey'=>$survey);
	}
	
	public function getQuestionnaireType(){
		global $ies_cfg;
		return $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0];

	}
	public function getQuestionTypeName(){ // this function should be abstract to parent "libSbaStepHandin" not only libSbaStepHandinQuestionnaireEDIT, need to discuss with thomas
		global $sba_cfg;
		return $sba_cfg['DB_IES_STEP_QuestionType']['observeEdit'][1];
	}
}



?>