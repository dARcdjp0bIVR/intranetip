<?
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_set WRITE";
$li->db_db_query($sql);
//standard_rubric_set - 具規範的探究方法的獨立專題探究〈2017〉評分指引〈題目界定和概念 / 知識辨識〉 
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_set (set_title,description,set_type,createdby,inputdate,modified)
			VALUES 
				('具規範的探究方法的獨立專題探究〈2017〉評分指引〈題目界定和概念 / 知識辨識〉','',2,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_set_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric - 具規範的探究方法的獨立專題探究〈2017〉評分指引〈題目界定和概念 / 知識辨識〉
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric (std_rubric_set_id,std_rubric_group_id,title,showorder,group_showorder,from_first_criteria,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_set_id."',0,'具規範的探究方法的獨立專題探究〈2017〉評分指引〈題目界定和概念 / 知識辨識〉',1,0,0,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric_detail - 具規範的探究方法的獨立專題探究〈2017〉評分指引〈題目界定和概念 / 知識辨識〉
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_detail WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_detail (std_rubric_id,std_rubric_set_id,std_rubric_group_id,score,score_from,score_to,description,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'低',1,3,'- 對該議題的認識膚淺 / 嘗試點出議題 / 探究方案〈例如探究向度〉/ 資料搜集方法，但沒有察覺 / 誤解該議題或問題的重要性，或未能就重要性解釋議題的焦點\r\n- 列舉一些或不相關的概念 / 字眼 / 事實，但未能解釋與該探究的相關性',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'中',4,6,'- 嘗試訂立探究焦點、範圍、探究方案〈例如探究向度〉/ 資料搜集方法及指出意義，但欠清晰，或對議題的重要性理解不足\r\n- 簡單指出一些概念，但不全然相關；雖嘗試解釋這些概念與該探究的相關性及如何應用在這探究上，但欠清晰；或遺漏一些主要的概念',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'高',7,9,'- 訂立及清楚闡釋探究焦點及範圍、探究方案〈例如探究向度〉/ 資料搜集方法；清晰表達議題的重要性\r\n- 清楚指出所需及極具相關性的概念 / 知識，並解釋與該探究的相關性為何及可如何應用在該探究上',0,now(),now())";
$li->db_db_query($sql);	
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric_set - 具規範的探究方法的獨立專題探究〈2017〉評分指引〈解釋和論證〉 
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_set (set_title,description,set_type,createdby,inputdate,modified)
			VALUES 
				('具規範的探究方法的獨立專題探究〈2017〉評分指引〈解釋和論證〉','',2,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_set_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric - 具規範的探究方法的獨立專題探究〈2017〉評分指引〈解釋和論證〉
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric WRITE";
$li->db_db_query($sql);

$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric (std_rubric_set_id,std_rubric_group_id,title,showorder,group_showorder,from_first_criteria,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_set_id."',0,'具規範的探究方法的獨立專題探究〈2017〉評分指引〈解釋和論證〉',1,0,0,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric_detail - 具規範的探究方法的獨立專題探究〈2017〉評分指引〈解釋和論證〉
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_detail WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_detail (std_rubric_id,std_rubric_set_id,std_rubric_group_id,score,score_from,score_to,description,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'低',1,3,'- 資料與分析之間的關係不大，或鋪陳不相關的資料 / 從狹窄的範圍找資料\r\n- 含糊地指出持份者對該議題的意見 / 見解 / 因素 / 影響，但或有不相關 / 不正確的地方\r\n- 運用膚淺的知識或概念，粗略地解釋持份者的看法，但解釋不足或有誤\r\n- 運用不相關的概念 / 知識 / 過時或並非完全相關的事實 / 資料，嘗試簡單解釋該議題與考生立場 / 選擇 / 決定，但解釋粗疏、不足，論點欠缺邏輯及佐證\r\n- 沒有回應議題的核心 / 沒有聚焦在所訂的問題上；論點偏頗 / 缺乏理據 / 含量 / 自相矛盾',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'中',4,6,'- 運用尚算有用的資料，但或資料份量不足，也或與沒有清晰地把資料與分析及論證連繫起來\r\n- 簡單解釋因素 / 影響 / 關係 / 主要持份者的看法、動態關係 / 分歧 / 議題隱含的價值觀，但欠清晰，也不深入；運用基本 / 非完全相關的概念及知識 / 事實 / 或不準確及 / 或過時的資料\r\n- 運用非完全相關的概念及知識 / 事實 / 或不準確及 / 或過時的資料來論證考生的立場 / 選擇 / 決定，在部分地方發現邏輯推理有誤及理據欠充份\r\n- 提供與議題有關的意見 / 觀點，並輔以尚算有力的論據\r\n- 就回應議題所作的分析，其範疇及深度均見局限，當中有欠詳盡的地方 / 傾向單一 / 集中某些角度',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'高',7,9,'- 應用非常有用的資料\r\n- 把分析、論證及資料的相關性緊密地連繫起來\r\n- 就極具相關性的概念及知識 / 事實 / 最新資料、清楚指出並明確及合乎邏輯地解釋有關因素 / 影響 / 關係 / 主要持份者的看法、動態關係 / 分歧 / 議題隱含的價值觀\r\n- 就極具相關性的概念及知識 / 事實 / 最新資料，全面及合乎邏輯地論證考生的立場 / 選擇 / 決定，展示高水平的邏輯推理及多角度思維力\r\n- 能提供與議題及探究問題相關且具洞察力的意見 / 觀點，並輔以有力的論據\r\n- 從不同角度作全面分析，以回應議題及探究問題',0,now(),now())
";
$li->db_db_query($sql);
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric_set - 具規範的探究方法的獨立專題探究〈2017〉評分指引〈表達與組織〉 
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_set (set_title,description,set_type,createdby,inputdate,modified)
			VALUES 
				('具規範的探究方法的獨立專題探究〈2017〉評分指引〈表達與組織〉','',2,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_set_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric - 具規範的探究方法的獨立專題探究〈2017〉評分指引〈表達與組織〉 
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric WRITE";
$li->db_db_query($sql);	
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric (std_rubric_set_id,std_rubric_group_id,title,showorder,group_showorder,from_first_criteria,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_set_id."',0,'具規範的探究方法的獨立專題探究〈2017〉評分指引〈表達與組織〉',1,0,0,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric_detail - 具規範的探究方法的獨立專題探究〈2017〉評分指引〈表達與組織〉
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_detail WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_detail (std_rubric_id,std_rubric_set_id,std_rubric_group_id,score,score_from,score_to,description,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'低',1,3,'- 報告欠組織 / 相關性 / 焦點；表達含糊\r\n- 不恰當展示資料\r\n- 未能恰當地註明資料來源',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'中',4,6,'- 雖有組織架構，但偶有欠連貫性、意念或欠完整和表達欠妥善的地方\r\n- 大致恰當使用資料〈例如以照片、圖、表及數據資料等形式的資料〉\r\n- 註明資料來源，但有遺漏 / 不準確的地方',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'高',7,9,'- 以連貫和一致的結構，簡明及有條理地傳意\r\n- 有效使用資料〈例如以照片、圖、表及數據資料等形式的資料〉\r\n- 清楚和準確註明資料來源',0,now(),now())	
";
$li->db_db_query($sql);	
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric_set - 具規範的探究方法的獨立專題探究〈2017〉評分指引〈自發性〉 
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_set (set_title,description,set_type,createdby,inputdate,modified)
			VALUES 
				('具規範的探究方法的獨立專題探究〈2017〉評分指引〈自發性〉','',2,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_set_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric - 具規範的探究方法的獨立專題探究〈2017〉評分指引〈自發性〉 
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric (std_rubric_set_id,std_rubric_group_id,title,showorder,group_showorder,from_first_criteria,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_set_id."',0,'具規範的探究方法的獨立專題探究〈2017〉評分指引〈自發性〉',1,0,0,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric_detail - 具規範的探究方法的獨立專題探究〈2017〉評分指引〈自發性〉
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_detail WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_detail (std_rubric_id,std_rubric_set_id,std_rubric_group_id,score,score_from,score_to,description,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'低',1,3,'- 在整個探究過程中均需依賴老師的額外指示\r\n- 未能自覺按時和如期完成工作\r\n- 甚少付出努力作改善',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'中',4,6,'- 在探究過程中能解決一些困難\r\n- 有時可如期完成工作／老師須時加督促\r\n- 付出一些努力以謀改善',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'高',7,9,'- 在整個探究過程中，有證據顯示學生主動、有應變能力，也能在毋須催迫下解決困難及作出反思\r\n- 懂得分配時間，並能如期完成工作\r\n- 持續改進',0,now(),now())
";
$li->db_db_query($sql);
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric_set - Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Problem Definition and Identification of Concepts/Knowledge) 
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_set WRITE";
$li->db_db_query($sql);
//Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017)
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_set (set_title,description,set_type,createdby,inputdate,modified)
			VALUES 
				('Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Problem Definition and Identification of Concepts/Knowledge)','',2,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_set_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);
				
//standard_rubric - Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Problem Definition and Identification of Concepts/Knowledge) 				
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric WRITE";
$li->db_db_query($sql);				
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric (std_rubric_set_id,std_rubric_group_id,title,showorder,group_showorder,from_first_criteria,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_set_id."',0,'Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Problem Definition and Identification of Concepts/Knowledge)',1,0,0,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric_detail - Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Problem Definition and Identification of Concepts/Knowledge) 
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_detail WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_detail (std_rubric_id,std_rubric_set_id,std_rubric_group_id,score,score_from,score_to,description,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'L',1,3,'- Shows superficial understanding of the issue/ attempt to highlight the issue concerned/ the enquiry plan (e.g. perspectives for enquiry)/ data collection method; shows little awareness to/ misinterprets the significance of the issue or problem, or is unable to explain the focus of the issue in terms of its significance\r\n- Lists some concepts/ terms/ facts, which may be irrelevant; is unable to explain the relevance of the concepts to the enquiry',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'M',4,6,'- Attempts to define the focus and scope of the enquiry and the significance, the enquiry plan (e.g. perspectives for enquiry)/ data collection method, but lacks clarity, or shows a partial understanding of the significance of the issue\r\n- Points out briefly some concepts, but may not be all relevant; attempts to explain the relevance and applicability of some of the concepts and understanding to the enquiry, but lacks clarity; may omit some key concepts',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'H',7,9,'- Defines and explains clearly the focus and scope of the enquiry, the enquiry plan (e.g. perspectives for enquiry)/ data collection method; articulates clearly the significance of the issue\r\n- Clearly identifies necessary and highly relevant concepts/knowledge and explains the relevance and applicability to the enquiry',0,now(),now())
				
";
$li->db_db_query($sql);	
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric_set - Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Explanation and Justification) 
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_set WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_set (set_title,description,set_type,createdby,inputdate,modified)
			VALUES 
				('Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Explanation and Justification)','',2,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_set_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric - Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Explanation and Justification) 				
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric (std_rubric_set_id,std_rubric_group_id,title,showorder,group_showorder,from_first_criteria,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_set_id."',0,'Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Explanation and Justification)',1,0,0,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric_detail - Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Explanation and Justification) 
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_detail WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_detail (std_rubric_id,std_rubric_set_id,std_rubric_group_id,score,score_from,score_to,description,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'L',1,3,'- Provides a weak linkage between the analysis and the information or presents irrelevant information/ information in a limited scope\r\n- Points out vaguely the stakeholders’ ideas/ thought/ factors/ impacts with regard to the issue, but some of them may not be relevant/ correct\r\n- Gives a brief and inadequate explanation or partially correct explanation of some of the stakeholders’ views, by applying a superficial level of knowledge or concepts\r\n- Attempts to provide some brief and inadequate explanations in relation to the issue and his/her standpoint/ choices/ decision, but the explanation is too partial; using irrelevant concepts/ knowledge/ out-dated or partially relevant and correct facts/ information, showing illogical arguments without evidential support\r\n- Does not address the crux of the issue/ does not focus on the problem as defined/ provides a biased/ non-substantiated/ ambiguous/ contradictory argument',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'M',4,6,'- Deploys fairly useful information, but which may not be adequate and may not be clearly linked to the analysis and justification\r\n- Gives a simple explanation of factors/ impacts/ relationships/ major viewpoints of key stakeholders, dynamic relationships/ disagreements/ embedded values in the issue, but which lacks clarity and intensity, applying basic/ partially relevant concepts and knowledge/ facts/ information, which may be incorrect and/or out-dated\r\n- Justifies the standpoint/ choices/ decision by using partially relevant concepts and knowledge/ facts/ information, which may be incorrect and/ or out-dated, carrying flaws in reasoning and lacking in evidence in parts\r\n- Provides ideas/ views which are fairly relevant to the issue concerned and the enquiry question\r\n- Addresses the issue with an analysis in limited scope and depth which lacks detail in parts/ tends to be one-sided/ focus on certain perspectives',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'H',7,9,'- Deploys highly useful information\r\n- Provides a highly relevant linkage between the analysis, justification and the information\r\n- Sharply identifies, clearly and logically articulates factors/ impacts/ relationships/ major viewpoints of key stakeholders, dynamic relationships/ disagreements/ embedded values in the issue, with highly relevant concepts and knowledge/ facts/ up-to-date information\r\n- Fully and logically justifies the standpoint/ choices/ decision with highly relevant concepts and knowledge/ facts/ up-to-date information, showing strong logical reasoning and high level of multiple-perspective thinking\r\n- Provides insightful ideas/ views, which are relevant to the issue concerned and the enquiry question, with supportive arguments\r\n- Addresses well the issue and the enquiry question with a comprehensive analysis from multiple perspectives',0,now(),now())
						
";
$li->db_db_query($sql);
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric_set - Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Presentation and Organisation) 
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_set WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_set (set_title,description,set_type,createdby,inputdate,modified)
			VALUES 
				('Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Presentation and Organisation)','',2,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_set_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric - Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Presentation and Organisation) 
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric WRITE";
$li->db_db_query($sql);	
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric (std_rubric_set_id,std_rubric_group_id,title,showorder,group_showorder,from_first_criteria,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_set_id."',0,'Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Presentation and Organisation)',1,0,0,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric_detail - Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Presentation and Organisation) 
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_detail WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_detail (std_rubric_id,std_rubric_set_id,std_rubric_group_id,score,score_from,score_to,description,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'L',1,3,'- Compiles a report with little organisation/ irrelevant ideas/ without focus; expresses his/her ideas vaguely\r\n- Presented data inappropriately\r\n- Is unable to acknowledge the sources of information appropriately',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'M',4,6,'- Provides an organised structure, but not always coherent; some ideas are disjointed and not well presented\r\n- Uses data appropriately (e.g. in the form of tables, photographs, charts and figures) in general\r\n- Acknowledges sources of information with some omissions/ inaccuracies',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'H',7,9,'- Communicates ideas in a concise and well-organised manner, with a coherent structure\r\n- Uses data effectively (e.g. in the form of tables, photographs, charts and figures)\r\n- Clearly and accurately acknowledges sources of information',0,now(),now())
								
";
$li->db_db_query($sql);
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);	

//standard_rubric_set - Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Initiative) 
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_set WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_set (set_title,description,set_type,createdby,inputdate,modified)
			VALUES 
				('Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Initiative)','',2,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_set_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric - Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Initiative) 
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric (std_rubric_set_id,std_rubric_group_id,title,showorder,group_showorder,from_first_criteria,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_set_id."',0,'Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Initiative)',1,0,0,0,now(),now())";
$li->db_db_query($sql);
$standard_rubric_id = $li->db_insert_id();
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

//standard_rubric_detail - Marking Guidelines for the Structured Enquiry Approach of IES of HKDSE Liberal Studies (2017) (Initiative) 
$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_detail WRITE";
$li->db_db_query($sql);
$sql = "INSERT INTO 
				{$classRoomDB}.standard_rubric_detail (std_rubric_id,std_rubric_set_id,std_rubric_group_id,score,score_from,score_to,description,createdby,inputdate,modified)
			VALUES 
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'L',1,3,'- Relies on teachers’ extra instructions in the process of conducting the enquiry\r\n- Is weak in time management and always fails to meet deadlines\r\n- Put in little effort in making improvements',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'M',4,6,'- Solves some problems in the process of conducting the enquiry\r\n- Meets deadlines sometimes/ needs to be under teacher’s close monitoring\r\n- Puts in some effort in making improvements',0,now(),now()),
				('".$standard_rubric_id."','".$standard_rubric_set_id."',0,'H',7,9,'- Has been proactive, resourceful, able to work with little supervision in solving problems and reflecting on their work, as evidenced in the process of conducting the enquiry\r\n- Shows time-management skills and meets deadlines\r\n- Makes continuous improvements',0,now(),now())

";
$li->db_db_query($sql);	
$sql = "UNLOCK TABLES";
$li->db_db_query($sql);

$sql = "INSERT IGNORE INTO IES_COMBO_MARKING_CRITERIA (MarkCriteriaID, TitleChi, TitleEng, isDefault, defaultMaxScore, defaultWeight, defaultSequence, DateInput, InputBy, ModifyBy) VALUES ";
$sql .= "
	(23, '題目界定和概念/知識辨識', 'Problem Definition and Identification of Concepts/Knowledge', 1, 9, 3, 3, NOW(), 1, 1),
	(24, '解釋和論證', 'Explanation and Justification', 1, 9, 5, 4, NOW(), 1, 1),
	(25, '表達與組織', 'Presentation and Organisation', 1, 9, 1, 5, NOW(), 1, 1),
	(26, '自發性', 'Initiative', 1, 9, 1, 6, NOW(), 1, 1)
";
$li->db_db_query($sql);

?>