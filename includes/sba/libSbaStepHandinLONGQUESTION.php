<?php
/**
 * 2012-08-22 Mick
 *	Implement getExportDisplay()
 *
 * */
class libSbaStepHandinLONGQUESTION extends libSbaStepHandin{
	
	private $QuestionTitle;
	
	public function __construct($parTaskID, $parStepID=0, $parQuestion='', $parAnswerId=0) {
		global $sba_cfg;
		
		parent::__construct($parTaskID, $parStepID, $parAnswerId);
		
		$this->setQuestionType($sba_cfg['DB_IES_STEP_QuestionType']['longQuestion'][0]);
		
		if(is_string($parQuestion)){
			$QuestionAry = $this->tranformQuestionStrToAry($parQuestion);
		}
		else{
			$QuestionAry = $parQuestion;
		}
		
		$this->setQuestionTitle($QuestionAry['Title']);
	}
	
	public function getCaption(){
		global $sba_cfg;
		
		return $sba_cfg['DB_IES_STEP_QuestionType']['longQuestion'][1];
	}
	
	/* This function should receive r_stdAnswer[Answer] defined in getQuestionDisplay() */
	public function setStudentAnswer($stdAnswer_ary){
		$this->setAnswer($stdAnswer_ary['Answer']);
	}
	
	public function getDisplayAnswer(){
		return $this->getAnswer();
	}
	
	public function getDisplayAnswerForMarking(){
		global $linterface;

		$_str = $this->getAnswer();
		$_str = $linterface->Get_Display_From_TextArea($_str); 
		return $_str;

	}
	
	public function getExportDisplay(){
		
		$title = '<h3>'.$this->getQuestionTitle().'</h3>';
		return array($this->getQuestionType()=>$title.$this->getAnswer());

	}
	
	public function getQuestionDisplay(){
		global $sba_allowStudentSubmitHandin, $Lang;
		$_task_id 	= $this->getTaskID()?   $this->getTaskID()   : 0;
		$_step_id 	= $this->getStepID()?   $this->getStepID()   : 0;
		$_answer_id = $this->getAnswerID()? $this->getAnswerID() : 0;
		

		$thisTextAreaJsAction = ' onclick = "alert(\''.$Lang['SBA']['StudentClickThisToSubmitHandin'].'\')" ';
		$thisJsAction = 'alert(\''.$Lang['SBA']['StudentClickThisToSubmitHandin'].'\')';
		if($sba_allowStudentSubmitHandin){
			$thisJsAction= "step_stdAnswer_handin_form_submit({$_task_id}, {$_step_id}, {$_answer_id})";
			$thisTextAreaJsAction = '';
		}
		


		$QuestionHTML = "<h4 class=\"icon_stu_edit\" style=\"margin-top:30px\">
							<span class=\"task_instruction_text\">".$this->getQuestionTitle()."</span>
						 </h4>
						 <textarea {$thisTextAreaJsAction} name=\"r_stdAnswer[Answer]\" style=\"width:100%; height:200px\">".htmlspecialchars($this->getAnswer())."</textarea>
						 <div style=\"text-align:center; margin-top:10px\">
							<input type=\"button\" value=\"".$Lang['IES']['Save2']."\" onclick=\"{$thisJsAction}\" onmouseout=\"this.className=&quot;formbutton&quot;\" onmouseover=\"this.className=&quot;formbuttonon&quot;\" class=\"formbutton\" name=\"submit2\">  
						 </div>
						 <div class=\"edit_bottom\"><span>".$Lang['IES']['LastModifiedDate']." : ".($this->getDateModified()? $this->getDateModified():'--')."</span></div>";
		return $QuestionHTML;
	}
	
	public function getEditQuestionDisplay(){
		$id_suffix = $this->getStepID()? $this->getStepID() : 'new';
		
		$linterface = new interface_html();
		
		$QuestionHTML = "<script>
						 function step_tool_validate_fail(){							 
							if($('#step_Question_Title_$id_suffix').val()==''){
								$('#step_Question_Title_{$id_suffix}_ErrorDiv').show();
								return true;
					 		}
							return false;
						 }
						 </script>
						 <h4>
							<span class=\"task_instruction_text\">
								<input type=\"text\" id=\"step_Question_Title_$id_suffix\" class=\"textbox\" name=\"r_step_Question[Title]\" value=\"".($this->getQuestionTitle()? htmlspecialchars($this->getQuestionTitle()):"")."\">
							</span>
						 </h4>
						 <textarea style=\"width:100%; height:50px\"></textarea>
						 ".$linterface->Get_Form_Warning_Msg("step_Question_Title_{$id_suffix}_ErrorDiv", "請輸入題目標題", 'WarningDiv')."
						 <input type=\"hidden\" id=\"r_step_QuestionType\" name=\"r_step_QuestionType\" value=\"".($this->getQuestionType())."\">";
		return $QuestionHTML;
	}
	
	public function getQuestionStr(){
		$QuestionStr = $this->getQuestionTitle();
		
		return $QuestionStr;
	}
	
	private function setQuestionTitle($val){
		$this->QuestionTitle = $val;
	} 
	
	private function getQuestionTitle(){
		return $this->QuestionTitle;
	}
	
	private function tranformQuestionStrToAry($QuestionStr){
		$QuestionAry['Title'] = $QuestionStr;
		
		return $QuestionAry;
	}
}

?>