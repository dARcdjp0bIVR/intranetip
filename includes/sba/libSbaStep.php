<?
# using : thomas

class SbaStepMapper{

	private $eclass_db;
	private $intranet_db;
	private $objDB;
	private $selectSql;

	public function SbaStepMapper(){
		global $eclass_db,$intranet_db;
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;

		$this->selectSql = 'select 
								step.StepID , step.TaskID , step.Title , step.Description , step.QuestionType , step.Question, step.Attachments , step.StepNo , step.Status , step.Sequence , step.SaveToTask , step.DateInput, step.InputBy , step.DateModified ,step.ModifyBy
							from 
							'.$this->intranet_db.'.IES_STEP as step where 1';
	}

	public function getByStepId($stepId){
		$sql = $this->selectSql. ' and stepid = '.$stepId;

		$resultSet = $this->objDB->returnResultSet($sql);
		if(is_array($resultSet) && sizeof($resultSet) == 1){
			return $this->createObject(current($resultSet));
		}else{
			return null;
		}
	}

	public function findByTaskId($taskId,$orderByStr = ''){
		$orderByStr = (trim($orderByStr) == '') ? ' order by step.Sequence asc ' : $orderByStr; 
		
		$sql = $this->selectSql. ' and taskid = '.$taskId.$orderByStr;

		$resultSet = $this->objDB->returnResultSet($sql);
		if(is_array($resultSet) && count($resultSet) >0){
			$returnAry = array();
			for($i = 0,$i_max = count($resultSet);$i < $i_max;$i++){
				$returnAry[] = $this->createObject($resultSet[$i]);
			}
			return $returnAry;
		}else{
			return null;
		}
		
	}

	public function findByStepIds($stepIdAry,$orderByStr = ''){
		$orderByStr = (trim($orderByStr) == '') ? ' order by step.Sequence asc ' : $orderByStr; 
		if(is_array($stepIdAry)){
			// if stepIdAry is array , skip 
		}else{
			//transfer to array
			array($stepIdAry);
		}
		$sql = $this->selectSql. ' and stepid in( '.implode(',',$stepIdAry).')'.$orderByStr;

		$resultSet = $this->objDB->returnResultSet($sql);
		if(is_array($resultSet) && count($resultSet) >0){
			$returnAry = array();
			for($i = 0,$i_max = count($resultSet);$i < $i_max;$i++){
				$returnAry[] = $this->createObject($resultSet[$i]);
			}
			return $returnAry;
		}else{
			return null;
		}
		
	}


	public function findByStageId($stageId,$orderByStr = ''){
		global $intranet_db;


		if(intval($stageId) <= 0){
			return null;
		}
		$orderByStr = (trim($orderByStr) == '') ? ' order by step.Sequence asc ' : $orderByStr; 
		$sql = 'select
					step.StepID as `StepID`, step.TaskID as `TaskID`, step.Title as `Title`, step.Description as `Description`, step.QuestionType as `QuestionType`, step.Question as `Question`, step.Attachments as `Attachments`, step.StepNo as `StepNo`, step.Status as `Status`, step.Sequence as `Sequence`, step.SaveToTask as `SaveToTask`, step.DateInput as `DateInput`, step.InputBy as `InputBy`, step.DateModified as `DateModified`,step.ModifyBy  as `ModifyBy` 
				from 
					'.$intranet_db.'.IES_STAGE as stage 
					inner join '.$intranet_db.'.IES_TASK as task on task.StageID = stage.StageID
					inner join '.$intranet_db.'.IES_STEP as step on step.TaskID = task.TaskID
				where stage.StageID = '.$stageId.'
		       ';
		$sql .= $orderByStr;

		$resultSet = $this->objDB->returnResultSet($sql);

		if(is_array($resultSet) && count($resultSet) >0){
			$returnAry = array();
			for($i = 0,$i_max = count($resultSet);$i < $i_max;$i++){
				$returnAry[] = $this->createObject($resultSet[$i]);
			}
			return $returnAry;
		}else{
			return null;
		}
	}
	public function save(step &$objStep){

		$Id = $objStep->getStepID();

		if($Id == ''){
			$objUpdate= $this->newRecord($objStep);
		}else{
			$objUpdate= $this->updateRecord($objStep);		
		}

		//assign back $objStep , for new record case (get the new insert id)
		$objStep = $objUpdate;

	}

	public function newRecord(step $objStep){
		$DataArr = array();
		$DataArr["TaskID"]	 = $this->objDB->pack_value($objStep->getTaskID(), 	"int");
		$DataArr["Title"]	 = $this->objDB->pack_value($objStep->getTitle(), 	"str");
		$DataArr["Description"]	 = $this->objDB->pack_value($objStep->getDescription(), "str");
		$DataArr["QuestionType"] = $this->objDB->pack_value($objStep->getQuestionType(),"str");
		$DataArr["Question"]	 = $this->objDB->pack_value($objStep->getQuestion(),	"str");
		$DataArr["Attachments"]	 = $this->objDB->pack_value($objStep->getAttachments(),	"str");
		$DataArr["StepNo"]	 = $this->objDB->pack_value($objStep->getStepNo(), 	"int");
		$DataArr["Status"]	 = $this->objDB->pack_value($objStep->getStatus(), 	"int");
		$DataArr["Sequence"]	 = $this->objDB->pack_value($objStep->getSequence(), 	"int");
		$DataArr["SaveToTask"]	 = $this->objDB->pack_value($objStep->getSaveToTask(), 	"int");
		$DataArr["DateInput"]	 = $this->objDB->pack_value($objStep->getDateInput(), 	"date");
		$DataArr["InputBy"]	 = $this->objDB->pack_value($objStep->getInputBy(), 	"int");
		$DataArr["DateModified"] = $this->objDB->pack_value($objStep->getDateModified(),"date");
		$DataArr["ModifyBy"]	 = $this->objDB->pack_value($objStep->getModifyBy(), 	"int");

		$sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);

		$fieldStr= $sqlStrAry['sqlField'];
		$valueStr= $sqlStrAry['sqlValue'];


		$sql = 'Insert Into '.$this->intranet_db.'.IES_STEP ('.$fieldStr.') Values ('.$valueStr.')';
		
		$success = $this->objDB->db_db_query($sql);
								
		$RecordID = $this->objDB->db_insert_id();

		$objStep->setStepID($RecordID);					
		return $objStep;
	}
	public function updateRecord(step $objStep){
		$DataArr = array();
		$DataArr["TaskID"]	 = $this->objDB->pack_value($objStep->getTaskID(), 	"int");
		$DataArr["Title"]	 = $this->objDB->pack_value($objStep->getTitle(), 	"str");
		$DataArr["Description"]	 = $this->objDB->pack_value($objStep->getDescription(), "str");
		$DataArr["QuestionType"] = $this->objDB->pack_value($objStep->getQuestionType(),"str");
		$DataArr["Question"]	 = $this->objDB->pack_value($objStep->getQuestion(),	"str");
		$DataArr["Attachments"]	 = $this->objDB->pack_value($objStep->getAttachments(),	"str");
		$DataArr["StepNo"]	 = $this->objDB->pack_value($objStep->getStepNo(), 	"int");
		$DataArr["Status"]	 = $this->objDB->pack_value($objStep->getStatus(), 	"int");
		$DataArr["Sequence"]	 = $this->objDB->pack_value($objStep->getSequence(), 	"int");
		$DataArr["SaveToTask"]	 = $this->objDB->pack_value($objStep->getSaveToTask(), 	"int");
		$DataArr["DateModified"] = $this->objDB->pack_value($objStep->getDateModified(),"date");
		$DataArr["ModifyBy"]	 = $this->objDB->pack_value($objStep->getModifyBy(), 	"int");

		foreach ($DataArr as $fieldName => $data)
		{
			$updateDetails .= $fieldName."=".$data.",";
		}

		//REMOVE LAST OCCURRENCE OF ",";
		$updateDetails = substr($updateDetails,0,-1);

		$sql = "update ".$this->intranet_db.".IES_STEP set ".$updateDetails." where Stepid = ".$objStep->getStepID();

		$this->objDB->db_db_query($sql);
		return $objStep;
	}

	private function createObject($ary){
		$obj= new Step();

		$obj->setStepID($ary['StepID']);
		$obj->setTaskID($ary['TaskID']);
		$obj->setTitle($ary['Title']);
		$obj->setDescription($ary['Description']);
		$obj->setQuestionType($ary['QuestionType']);
		$obj->setQuestion($ary['Question']);
		$obj->setAttachments($ary['Attachments']);
		$obj->setStepNo($ary['StepNo']);
		$obj->setStatus($ary['Status']);
		$obj->setSequence($ary['Sequence']);
		$obj->setSaveToTask($ary['SaveToTask']);
		$obj->setDateInput($ary['DateInput']);
		$obj->setInputBy($ary['InputBy']);
		$obj->setDateModified($ary['DateModified']);
		$obj->setModifyBy($ary['ModifyBy']);

		return $obj;	
	}


}
class step{

	private $StepID;       
	private $TaskID;       
	private $Title;
	private $Description;
	private $QuestionType;
	private $Question;
	private $Attachments;
	private $StepNo;       
	private $Status;       
	private $Sequence;     
	private $SaveToTask;   
	private $DateInput;
	private $InputBy;
	private $DateModified;
	private $ModifyBy;

	public function step($stepID = null){
		if($stepID != ''){
			$this->setStepID($stepID);
		}
	}

	public function setStepID($val) {
		$this->StepID = $val;
	}
	public function getStepID(){
		return $this->StepID;
	}

	public function setTaskID($val) {
		$this->TaskID = $val;
	}
	public function getTaskID() {
		return $this->TaskID;
	}

	public function setTitle($val) {
		$this->Title = $val;
	}
	public function getTitle() {
		return $this->Title;
	}
	
	public function setDescription($val) {
		$this->Description = $val;
	}
	public function getDescription() {
		return $this->Description;
	}
	
	public function setQuestionType($val) {
				
		$this->QuestionType = $val;
	}
	public function getQuestionType() {
		return $this->QuestionType;
	}
	
	public function setQuestion($val) {
		$this->Question = $val;
	}
	public function getQuestion() {
		return $this->Question;
	}
	
	public function setAttachments($val) {
		$this->Attachments = $val;
	}
	public function getAttachments() {
		return $this->Attachments;
	}
	
	public function setStepNo($val) {
		$this->StepNo = $val;
	}
	public function getStepNo() {
		return $this->StepNo;
	}

	public function setStatus($val) {
		$this->Status = $val;
	}
	public function getStatus() {
		return $this->Status;
	}

	public function setSequence($val) {
		$this->Sequence = $val;
	}
	public function getSequence() {
		return $this->Sequence;
	}

	public function setSaveToTask($val) {
		$this->SaveToTask = $val;
	}
	public function getSaveToTask() {
		return $this->SaveToTask;
	}

	public function setDateInput($val) {
		$this->DateInput = $val;
	}
	public function getDateInput() {
		return $this->DateInput;
	}

	public function setInputBy($val) {
		$this->InputBy = $val;
	}
	public function getInputBy() {
		return $this->InputBy;
	}

	public function setDateModified($val) {
		$this->DateModified = $val;
	}
	public function getDateModified() {
		return $this->DateModified;
	}

	public function setModifyBy($val) {
		$this->ModifyBy = $val;
	}
	public function getModifyBy() {
		return $this->ModifyBy;
	}
}
?>