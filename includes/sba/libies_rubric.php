<?php
# modifying by : fai

include_once("$intranet_root/includes/libeclassrubric.php");

class libiesrubric extends libeclassrubric {
	

	public function libiesrubric($classRoomID)
	{
		parent::libeclassrubric($classRoomID);
	}

	/** return result of a rubric id related to $stageID
	* @owner : Fai (201001016)
	* @param : A INTEGER or ARRAY OF INTEGER $stageID 
	* @return : Array of data , rubric details about the stageID
	*/
	public function getStageRubric($stageID,$MarkCriteriaID)
	{
		if($stageID=="") return array();
		
		global $intranet_db;
		$sql = "select 
					s.StageID as 'StageID', c.MarkCriteriaID as 'MARKCRITERIAID', c.task_rubric_id as 'TASK_RUBRIC_ID'
				from 
					{$intranet_db}.IES_STAGE as s 
				inner join 
					{$intranet_db}.IES_STAGE_MARKING_CRITERIA as c on s.StageID = c.StageID	
				where 
					s.StageID = {$stageID}
					and c.MarkCriteriaID = {$MarkCriteriaID};
				";
		$result = $this->returnArray($sql);
		return $result;				
	}
}

?>
