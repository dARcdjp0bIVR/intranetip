<?php
/**
 * Editing by Siuwan
 * 
 * Modification Log: 
 * 2015-06-19 (Siuwan) [ip.2.5.6.8.1] 
 * 		- added variable $ExportFormat, function generateSummaryFileNew(),getExportFormat(),setExportFormat(),getReportTableContent() to support 2017 format
 * 2013-10-24 (Charles Ma)
 * 		- modify generateSummaryFile() to remove html tag from task content to support the export of pdf format
 * 2012-08-22 (Mick)
 * 		Added export csv functions
 * 		Added export report functions
 *
 */
include_once($intranet_root."/includes/sba/libexportdoc.php");

class libSbaExport extends libexportdoc {
	private $TargetFolder;
	private $SchemeID;
	private $StageID;
	private $UserID;
	private $tasks;
	private $fs;
	private $ExportFormat;
	
	public function libSbaExport($TargetFolder, $parUserID, $parSchemeID='', $parStageID=''){
		$this->libdb();
		$this->fs  = new libfilesystem();
		$this->setTargetFolder(str_replace("//", "/",$TargetFolder));//folder with "//" , in mkdir , it will got error, so replace by "/"
		$this->setUserID($parUserID);
		$this->setSchemeID($parSchemeID);
		$this->setStageID($parStageID);
	}
	
	public function getExportFormat(){
		return $this->ExportFormat;
	}
	
	public function setExportFormat($val){
		$this->ExportFormat = $val;
	}
		
	public function getTargetFolder(){
		return $this->TargetFolder;
	}
	
	public function setTargetFolder($val){
		$this->TargetFolder = $val;
	}
	
	public function getSchemeID(){
		return $this->SchemeID;
	}
	
	public function setSchemeID($val){
		$this->SchemeID = $val;
	}
	
	public function getStageID(){
		return $this->StageID;
	}
	
	public function setStageID($val){
		$this->StageID = $val;
	}
	
	public function getUserID(){
		return $this->UserID;
	}
	
	public function setUserID($val){
		$this->UserID = $val;
	}
	
	private function renewExportDir(){
		
		$fsReoveObj = new libfilesystem();
			
		# remove original temp file
		$fsReoveObj->folder_remove_recursive($this->getTargetFolder());

		$this->fs->folder_new($this->getTargetFolder());
		$this->fs->chmod_R($this->getTargetFolder(), 0777);
	}
	
	public function exportStageReport(){
		global $sba_cfg, $Lang;
		
		# Initialize Object
		$this->libSba 	  	   = new libSba();
		$this->objTaskMapper 	   = new SbaTaskMapper();
		$this->Scheme 	   	   = new SbaScheme($this->getSchemeID());
		$this->Stage		   = new SbaStage($this->getStageID());
		$this->li		   = new libuser($this->getUserID());

		# Set Document Style
		$this->setDocumentStyle();
		
		# Set Document Header
		$this->setDocumentHeader();
		
		# Get All Task of the stage
		$this->tasks = $this->objTaskMapper->findByStageId($this->getStageID());
		
		$this->renewExportDir();
		
		if($this->getExportFormat()){
			$this->generateSummaryFileNew();
		}else{
			$this->generateSummaryFile();
		}
		foreach($this->tasks as $task){
			$this->generateTaskAttachments($task, $this->getTargetFolder());
		}
	}
		
	public function exportMappingCSV($SurveyID, $SurveyMappingIDs){
		
		global $sba_cfg, $intranet_root;
		
		$libies_survey=new libies_survey();
				
		$this->renewExportDir();
		
		foreach ($SurveyMappingIDs as $SurveyMappingID){
			$ExportContent=$libies_survey->genMappingCSV($SurveyMappingID);
			
			$file_name = $SurveyMappingID.".csv";
			$this->saveCSV($file_name, $this->getTargetFolder(), $ExportContent);
		}
		
		$this->fs->folder_copy($intranet_root.$sba_cfg['SBA_SURVEY_CHART_ROOT_PATH'].$this->getUserID()."/".$SurveyID, $this->getTargetFolder());
		
		
	}
	public function exportQuestionCSV($SurveyID){
	
		global $sba_cfg, $intranet_root;
		
		$libies_survey=new libies_survey();
		
		$this->renewExportDir();
		$ExportContent=$libies_survey->genQuestionsCSV($SurveyID);
		
		$file_name = $SurveyID.".csv";
		$this->saveCSV($file_name, $this->getTargetFolder(), $ExportContent);
		
		$this->fs->folder_copy($intranet_root.$sba_cfg['SBA_SURVEY_CHART_ROOT_PATH'].$this->getUserID()."/".$SurveyID, $this->getTargetFolder());	
						
	}
	
	private function saveCSV($file_name, $path, $content){
		
		$file_content = chr(255).chr(254).mb_convert_encoding($content,'UTF-16LE','UTF-8');//referred from php.net: fwrite()
					
		return $this->fs->file_write($file_content, $path."/$file_name");
		
	}
	
	private function saveDoc($file_name, $path, $content){
		
		$file_name 	  = libSbaExport::getSaveFileName($file_name);
		$file_name 	  = mb_convert_encoding($file_name ,"big5","utf8");
		$file_content = $this->createDoc($content, $file_name, false);
		return $this->fs->file_write($file_content, $path."/$file_name");
	}
	
	private function savePdf($file_name, $path, $content){
	    
		global $intranet_root, $Lang;
		include_once($intranet_root."/includes/tcpdf/tcpdf.php");
		
		$file_name 	  = libSbaExport::getSaveFileName($file_name);
		$file_name 	  = mb_convert_encoding($file_name ,"big5","utf8");
		$file_content 	  = $this->createPdf($content);
		$scheme_title = $this->Scheme->getTitle();
		$stage_title  = $this->Stage->getTitle();
		
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		
		$pdf->SetAuthor($this->li->UserNameLang());
		$pdf->SetTitle($scheme_title);
		$pdf->SetSubject($stage_title);
		
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->SetFont('droidsansfallback', '', 10);
		$pdf->SetHeaderFont(array('droidsansfallback', '', 8));
		$pdf->SetHeaderData('', '', '', $scheme_title.' - '.$stage_title);	

		$pages = explode('<!-- PDF_BREAK -->', $file_content);
	    
		foreach ($pages as $page){
		    
		    $pdf->AddPage();
		    $pdf->writeHTML($page, true, false, true, false, '');
		    
		}

		
		$pdf->Output($path."/$file_name", 'F');
	}
	private function generateSummaryFileNew(){
		
		global $sba_cfg, $ies_cfg, $Lang;
		$result_r = $this->get_teacher($this->getSchemeID());
		
		$html_TeachingTeacher = $html_AssessTeacher = array();
		foreach($result_r as $value){
			if($value["TeacherType"] == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"])
				$html_TeachingTeacher[] = $value['TeacherName'];
			else if($value["TeacherType"] == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"])
				$html_AssessTeacher[]   = $value['TeacherName'];	
		}

		$html_TeachingTeacher = $html_TeachingTeacher? implode(', ',$html_TeachingTeacher): "--";
		$html_AssessTeacher   = $html_AssessTeacher? implode(', ',$html_AssessTeacher): "--";
		
		if(empty($html_AssessTeacher)){ 
			$html_AssessTeacher = "--";
		}
		
		/* Start of Content*/
		
		$content_arr = array("","");
		
		for($j =0; $j < 2; $j++){
			$content = "";
			$content .= "<div class=\"WordSection1\" style=\"line-spacing:1.15\">";
			
			$content .= $this->get_span_open("","font-size:11pt; font-weight:bold;text-align:center;");
			$content .= $this->get_center_open();
				$content .= $this->get_p_open();
				$content .= $Lang['IES']['HKDSE'];
				$content .= $this->get_p_close();
				$content .= $this->get_p_open();
				$content .= $Lang['IES']['LiberalStudies'];
				$content .= $this->get_p_close();
				$content .= $this->get_p_open();
				$content .= $Lang['IES']['StageExportDoc']['string30'];
				$content .= $this->get_p_close();
				$content .= $this->get_p_open();
				$content .= $Lang['IES']['StageExportDoc']['string21'];
				$content .= $this->get_p_close();
				$content .= $this->get_p_open();
				$content .= $Lang['IES']['StageExportDoc']['string29'];
				$content .= $this->get_p_close();
			$content .= $this->get_center_close();
			$content .= $this->get_span_close();  //</span>
	
			$content .= $this->get_next_line();
			
			$content .= $this->get_span_open("","font-size:11pt;");  //<span>
			$content .= $this->get_p_open()."<b>".$Lang['SBA']['EnqiryQuestion']."︰_________________________________</b>".$this->get_p_close();
			$content .= $this->get_p_open()."<b>".$Lang['SBA']['YearOfExamination']."︰_________________________________</b>".$this->get_p_close();
			$content .= $this->get_p_open()."<b>".$Lang['SBA']['StudentName']."︰"."</b>".$this->li->UserNameLang().$this->get_p_close();
			$content .= $this->get_p_open()."<b>".$Lang['SBA']['ClassAndGroup']."︰"."</b>".$this->li->ClassName.$this->get_p_close();
			$content .= $this->get_p_open()."<b>".$Lang['SBA']['ClassNumber']."︰"."</b>".$this->li->ClassNumber.$this->get_p_close(); 
			$content .= $this->get_span_close();  //</span> 
			$content .= $this->get_next_line();
			$content .= $this->get_span_open("","font-size:11pt;");  //<span>
			$content .= $this->get_p_open()."<b>".$Lang['SBA']['ReportWordCount']."︰_________________________________</b>".$this->get_p_close();
			$content .= $this->get_span_close();  //</span> 
			$content .= $this->get_next_line();	
			
			$content .= $this->getReportNote();
	
			$content .= $this->get_page_break();	
			
			$content .= $this->getReportTableContent();
			
			$content .= $this->get_page_break();
					
			$content .= $this->get_span_open("","font-size:11pt;");
			 
			for($i=0, $i_max=count($this->tasks); $i<$i_max; $i++){
				$_objTask = $this->tasks[$i];
				
				# Get the AnswerID of the Task Handin
				$_answerID = $this->libSba->getAnswerIDByTaskIDUserID($_objTask->getTaskID(), $this->getUserID());
				
				# Get the Task Handin Object
				# Remark : Temporary set the Task Handin Type as $sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea']
				$_objTaskHandin = libSbaTaskHandinFactory::createTaskHandin($_objTask->getStageID(), $sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea'], $_objTask->getTaskID(), $_answerID);
	
				# Set Task Content (Task Title)
				$content .= $this->get_p_open("","margin-left:0cm;");  //<p>
				$content .= "<b>".$_objTask->getTitle()."</b>";  //TITLE
				$content .= $this->get_p_close();  //</p>
				$content .= $this->get_center_open();   //<center>
				$content .= $this->get_center_close();  //</center>
				
				# Set Task Content (Task Answer)
				$task_ans_content = $this->stringfilter($_objTaskHandin->getExportAnswer());
				$task_ans_content = (empty($task_ans_content))? $this->get_next_line(5):$task_ans_content;
				
				$content .= $this->get_p_open("","margin-left:0cm;");   //<p>
				$content .= $this->get_table_open("wordtable");  //<table>
				$content .= "<tr><td>";
				  			
				if($j == 0)
					$content .= $task_ans_content;   //ANS
				else
				 	$content .= strip_tags($task_ans_content, "");//ANS
				 
				$content .= "</td></tr>";
				$content .= $this->get_table_close();   //</table>
				$content .= $this->get_p_close();   //</p>
				
			}
			
			$content .= $this->get_span_close();
			$content .= "<!-- PDF_BREAK -->";
			//$content .= $this->get_next_line(5);
			//$content .= $this->getStudentDeclaration();
			
			$content .="</div>";
			
			$content_arr[$j] = $content;
		}
		
		
		/* End of Content */
					
		$this->saveDoc($this->Scheme->getTitle().".doc",$this->getTargetFolder(),$content_arr[0]);
		$this->savePdf($this->Scheme->getTitle().".pdf",$this->getTargetFolder(),$content_arr[1]);
			
		return;
	}	
	private function generateSummaryFile(){
		
		global $sba_cfg, $ies_cfg, $Lang;
		$result_r = $this->get_teacher($this->getSchemeID());
		
		$html_TeachingTeacher = $html_AssessTeacher = array();
		foreach($result_r as $value){
			if($value["TeacherType"] == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"])
				$html_TeachingTeacher[] = $value['TeacherName'];
			else if($value["TeacherType"] == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"])
				$html_AssessTeacher[]   = $value['TeacherName'];	
		}

		$html_TeachingTeacher = $html_TeachingTeacher? implode(', ',$html_TeachingTeacher): "--";
		$html_AssessTeacher   = $html_AssessTeacher? implode(', ',$html_AssessTeacher): "--";
		
		if(empty($html_AssessTeacher)){ 
			$html_AssessTeacher = "--";
		}
		
		/* Start of Content*/
		
		$content_arr = array("","");
		
		for($j =0; $j < 2; $j++){
			$content = "";
			$content .= "<div class=\"WordSection1\" style=\"line-spacing:1.15\">";
			
			$content .= $this->get_span_open("","font-size:11pt; font-weight:bold;text-align:center;");
			$content .= $this->get_center_open();
			$content .= $this->get_p_open();
			$content .= $Lang['IES']['HKDSE'];
			$content .= $this->get_p_close();
			$content .= $this->get_p_open();
			$content .= $this->Scheme->getTitle();
			$content .= $this->get_p_close();
			$content .= $this->get_p_open();
			$content .= $this->Stage->getTitle();
			$content .= $this->get_p_close();
			$content .= $this->get_center_close();
			$content .= $this->get_span_close();  //</span>
	
			$content .= $this->get_next_line();
			$content .= $this->get_center_open();  //<center>
			$content .= $this->get_table_open("wordtable"); //<table>
			$content .= "<tr><td>";
			$content .= $this->get_span_open("","font-size:11pt; font-weight:bold;");
			$content .= $this->get_p_open();
			$content .= $this->get_center_open().$Lang['IES']['StageExportDoc']['string2'].$this->get_center_close();
			$content .= $this->get_p_close(); 
			$content .= $this->get_span_close();  //</span> 
			$content .= $this->get_span_open("","font-size:11pt;"); //<span>
			$content .= $this->get_p_open();
			$content .= $_stageExportDocDeclaration;
			$content .= $this->get_p_close();
			$content .= $this->get_span_close();  //</span> 
			$content .= "</td></tr>";
			$content .= $this->get_table_close(); //</table>
			$content .= $this->get_center_close(); //</center>
			$content .= $this->get_next_line();
			$content .= $this->get_span_open("","font-size:11pt;");  //<span>
			$content .= $this->get_p_open()."<b>".$Lang['IES']['SchoolName']."︰".GET_SCHOOL_NAME()."</b>".$this-> get_p_close();
			$content .= $this->get_p_open()."<b>".$Lang['IES']['StudentName']."︰"."</b>".$this->li->UserNameLang().$this-> get_p_close();
			$content .= $this->get_p_open()."<b>".$Lang['IES']['Class']."︰"."</b>".$this->li->ClassName.$this-> get_p_close();
			$content .= $this->get_p_open()."<b>".$Lang['IES']['Instructor']."︰"."</b>".$html_TeachingTeacher.$this-> get_p_close();
			$content .= $this->get_p_open()."<b>".$Lang['IES']['DateSubmitted2']."︰"."</b>".$this-> get_p_close();
			$content .= $this->get_p_open()."<b>".$Lang['IES']['StatusSubmitted']."︰"."</b>".$Lang['IES']['StageExportDoc']['string4'].$this-> get_p_close();
			$content .= $this->get_span_close();  //</span> 
			$content .= $this->get_next_line();
			$content .= "<!-- PDF_BREAK -->";
			$content .= $this->get_span_open("","font-size:11pt;");
			 
			for($i=0, $i_max=count($this->tasks); $i<$i_max; $i++){
				$_objTask = $this->tasks[$i];
				
				# Get the AnswerID of the Task Handin
				$_answerID = $this->libSba->getAnswerIDByTaskIDUserID($_objTask->getTaskID(), $this->getUserID());
				
				# Get the Task Handin Object
				# Remark : Temporary set the Task Handin Type as $sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea']
				$_objTaskHandin = libSbaTaskHandinFactory::createTaskHandin($_objTask->getStageID(), $sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea'], $_objTask->getTaskID(), $_answerID);
	
				# Set Task Content (Task Title)
				$content .= $this->get_p_open("","margin-left:0cm;");  //<p>
				$content .= "<b>".$_objTask->getTitle()."</b>";  //TITLE
				$content .= $this->get_p_close();  //</p>
				$content .= $this->get_center_open();   //<center>
				$content .= $this->get_center_close();  //</center>
				
				# Set Task Content (Task Answer)
				$task_ans_content = $this->stringfilter($_objTaskHandin->getExportAnswer());
				$task_ans_content = (empty($task_ans_content))? $this->get_next_line(5):$task_ans_content;
				
				$content .= $this->get_p_open("","margin-left:0cm;");   //<p>
				$content .= $this->get_table_open("wordtable");  //<table>
				$content .= "<tr><td>";
				  			
				if($j == 0)
					$content .= $task_ans_content;   //ANS
				else
				 	$content .= strip_tags($task_ans_content, "");//ANS
				 
				$content .= "</td></tr>";
				$content .= $this->get_table_close();   //</table>
				$content .= $this->get_p_close();   //</p>
				
			}
			
			$content .= $this->get_span_close();
			$content .= "<!-- PDF_BREAK -->";
			//$content .= $this->get_next_line(5);
			//$content .= $this->getStudentDeclaration();
			
			$content .="</div>";
			
			$content_arr[$j] = $content;
		}
		
		
		/* End of Content */
					
		$this->saveDoc($this->Scheme->getTitle().".doc",$this->getTargetFolder(),$content_arr[0]);
		$this->savePdf($this->Scheme->getTitle().".pdf",$this->getTargetFolder(),$content_arr[1]);
			
		return;
	}
	
	
	private function generateTaskAttachments($task, $stage_dir){
				
		$task_dir=$stage_dir.'/TASK'.str_pad($task->getSequence(),2,'0',STR_PAD_LEFT).'-['.libSbaExport::getSaveFileName($task->getTitle()).']';
		$task_dir = mb_convert_encoding($task_dir ,"big5","utf8");
		
		//$task_dir
		$this->fs->folder_new($task_dir);//create task folder
		
		foreach ((array)$task->getObjStepsAry() as $step){
			$this->generateStepAttachments($step, $task_dir);
		}
		
	}
	
	private function generateStepAttachments($step, $task_dir){
		
		global $intranet_root,$sba_thisStudentID,$sba_thisMemberType;
		
		include_once($intranet_root."/includes/sba/libSbaStepHandinFactory.php");
		
		$answer_id = $sba_thisMemberType==USERTYPE_STUDENT? $this->libSba->getAnswerIDByStepIDUserID($step->getStepID(), $sba_thisStudentID) : null;
		$stepHandin=libSbaStepHandinFactory::createStepHandin($step->getTaskID(),$step->getQuestionType(),$step->getStepID(),$step->getQuestion(),$answer_id);
		
		$contents=$stepHandin->getExportDisplay();
		$file_name_prefix='STEP'.str_pad($step->getSequence(),2,'0',STR_PAD_LEFT).'-['.libSbaExport::getSaveFileName($step->getTitle()).']';
		if(!is_array($contents)){
			$contents = array($contents);
		}
		foreach ($contents as $name=>$content){
			if (is_array($content)){//questionaires type, has many surveys inside
			
				$survey_dir = '/'.$file_name_prefix.'-'.$name;
				$survey_dir = mb_convert_encoding($survey_dir ,"big5","utf8");
				$survey_dir=$task_dir.$survey_dir;
				
				$this->fs->folder_new($survey_dir);//new folder storing surveys
				
				foreach ($content as $surveyName=>$surveyContent){
					if ($surveyName=='images'){//get back images
						$image_dir='/images';
						$image_dir = mb_convert_encoding($image_dir ,"big5","utf8");
						$image_dir=$survey_dir.$image_dir;
						$this->fs->folder_new($image_dir);
						foreach ($surveyContent as $source=>$dest){
							$dest = mb_convert_encoding($dest ,"big5","utf8");
							$this->fs->file_copy($source,$image_dir.'/'.$dest);//cp images
						}					
						continue;
					}
					$file_name=$surveyName.'.doc';//create doc
					$this->saveDoc($file_name,$survey_dir,$surveyContent);
				}
			}else{
			    $file_name=$file_name_prefix.'.doc';//create doc
			    $this->saveDoc($file_name,$task_dir,$content);
			}
		}
		
		return;
	}
	
	private function setDocumentStyle(){
		$docstyle = <<<EOH
		<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				<!--
				 /* Font Definitions */
				@font-face
					{font-family:Verdana;
					panose-1:2 11 6 4 3 5 4 4 2 4;
					mso-font-charset:0;
					mso-generic-font-family:swiss;
					mso-font-pitch:variable;
					mso-font-signature:536871559 0 0 0 415 0;}
				 /* Style Definitions */
				p.MsoNormal, li.MsoNormal, div.MsoNormal
					{mso-style-parent:"";
					margin:0in;
					margin-bottom:.0001pt;
					mso-pagination:widow-orphan;
					font-size:7.5pt;
				        mso-bidi-font-size:8.0pt;
					font-family:"Verdana";
					mso-fareast-font-family:"Verdana";}
				.mytable{
					width:100%;
					border:2px solid black;
					border-collapse: separate;
					background-color: transparent;
					display: table;
				}
				.mytable th{
					background-color: #A6A6A6;
				}
				.mytable td{
					border:1px solid #A6A6A6;
				}
				.table_no_border{
					width:100%;
					border:none;
					border-collapse: collapse;
				}
				.table_no_border th{
					border:none;
					border-collapse: collapse;
				}
				.table_no_border td{
					border:none;
					border-collapse: collapse;
				}
				.wordtable{
					border:1px solid black;
					width:98%;
				}
				table{
					width:100%;
					border-collapse:collapse;
				}
				table tr th{
					background-color:grey;
					border-style:solid;
					border-width:1px;
					border-color:black;
				}
				table tr td{
					border-style:solid;
					border-width:1px;
					border-color:black;
				}
				p.small
					{mso-style-parent:"";
					margin:0in;
					margin-bottom:.0001pt;
					mso-pagination:widow-orphan;
					font-size:1.0pt;
				        mso-bidi-font-size:1.0pt;
					font-family:"Verdana";
					mso-fareast-font-family:"Verdana";}
				@page WordSection1
					{size:595.3pt 841.9pt;
					margin:72.0pt 72.0pt 72.0pt 72.0pt;
					mso-header-margin:42.55pt;
					mso-footer-margin:49.6pt;
					mso-paper-source:0;
					layout-grid:18.0pt;}
				div.WordSection1
					{page:WordSection1;}
				-->
				</style>
				<!--[if gte mso 9]><xml>
				 <o:shapedefaults v:ext="edit" spidmax="1032">
				  <o:colormenu v:ext="edit" strokecolor="none"/>
				 </o:shapedefaults></xml><![endif]--><!--[if gte mso 9]><xml>
				 <o:shapelayout v:ext="edit">
				  <o:idmap v:ext="edit" data="1"/>
				 </o:shapelayout></xml><![endif]-->
				 
EOH;
		$this->setstyle($docstyle);
	}
	
	private function setDocumentHeader(){
		$header = '<html xmlns:v="urn:schemas-microsoft-com:vml"
					xmlns:o="urn:schemas-microsoft-com:office:office"
					xmlns:w="urn:schemas-microsoft-com:office:word"
					xmlns="http://www.w3.org/TR/REC-html40">
				   <head>
						<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
						<meta name=ProgId content=Word.Document>
						<meta name=Generator content="Microsoft Word 9">
						<meta name=Originator content="Microsoft Word 9">
				   </head>';
		$this->sethtmlheader($header);
	}
	private function getReportNote(){
		global $Lang;
		
		$content = "";
		$content .= "<b>".$Lang['SBA']['StageExportDoc']['ReportNote']."</b>";
		$content .= $this->get_next_line();
		$content .= $this->get_center_open();  //<center>
		$content .= $this->get_table_open("wordtable");  //<table>
		$content .= "<tr><td style=\"border:1px solid black\">";

		$content .= $this->get_ol_open();   //<ul>
		
		foreach((array)$Lang['SBA']['StageExportDoc']['ReportNoteAry'] as $_note){
			$content .= $this->get_li_open();   //<li>
			$content .= $_note; 
			$content .= $this->get_li_close();   //</li>  			
		}
		 
		$content .= $this->get_ol_close();   //</ul>
		$content .= "</td></tr>";
	  	$content .= $this->get_table_close();    //</table>
	  	$content .= $this->get_center_close();  //</center>
	  	
	  	
		return $content;
	}
	private function getReportTableContent(){
		global $Lang;
		$content = "";
		$content .= $this->get_p_open();
		$content .= $this->get_span_open("","font-size:20pt; font-weight:bold;text-align:center;");
		$content .= $Lang['SBA']['TableOfContents'];
		$content .= $this->get_span_close();
		$content .= $this->get_p_close();
		
		$content .= $this->get_p_open();
		$content .= $this->get_center_open();  //<center>
		$content .= "<tablenoborder class=\"table_no_border\">"; 
		for($i=0, $i_max=count($this->tasks); $i<$i_max; $i++){
			$_objTask = $this->tasks[$i];
			$content .= "<tr>";
				$content .= "<td width=\"5%\">".chr(65+$i).".</td>";
				$content .= "<td width=\"85%\">".$_objTask->getTitle()."</td>";
				$content .= "<td width=\"10%\" align=\"center\">".$Lang['SBA']['Page']."</td>";
			$content .= "</tr>"; 
		}
	  	$content .= "</table>";  
	  	$content .= $this->get_center_close();  //</center>
	  	$content .= $this->get_p_close();
	  	
		return $content;
	}		
	private function getStudentDeclaration(){
		global $Lang;
		
		$content .= $this->get_p_open("","margin-left:0cm;");   //<p>
		$content .= "<b>".$Lang['IES']['StageExportDoc']['string5']."</b>";
		$content .= $this->get_p_close();
		
		$content .= $this->get_p_open();
		$content .= $this->get_center_open();  //<center>
		$content .= $this->get_table_open("wordtable");  //<table>
		$content .= "<tr><td style=\"border:1px solid black\">";
		
		$content .= $this-> get_p_open(); //<p>
		$content .= $Lang['IES']['StageExportDoc']['string6'];
		$content .= $this->get_p_close(); //</p>
		
		$content .= $this->get_ul_open();   //<ul>
		
		$content .= $this->get_li_open();   //<li>
		$content .= $this->get_p_open();  //<p>
		$content .= $Lang['IES']['StageExportDoc']['string7'];
		$content .= $this->get_p_close();  //</p>
		$content .= $this->get_li_close();   //</li>  
		
		
		$content .= $this->get_li_open();   //<li>
		$content .= $this->get_p_open();  //<p>
		$content .= $Lang['IES']['StageExportDoc']['string8'];
		$content .= $this->get_p_close();  //</p>
		$content .= $this->get_li_close();   //</li>  
	      
		
		$content .= $this->get_li_open();   //<li>
		$content .= $this->get_p_open();  //<p>
		$content .= $Lang['IES']['StageExportDoc']['string9'];
		$content .= $this->get_p_close(); //</p>
		$content .= $this->get_li_close();   //</li>  
		 
		$content .= $this->get_ul_close();   //</ul>
		$content .= $this->get_next_line();
		$content .= $this->get_p_open(); //<p>
		$content .= $Lang['IES']['StageExportDoc']['string10']."___________________________ ";
		$content .= $this->get_p_close(); //</p>
		$content .= $this->get_p_open(); //<p>
		$content .= $Lang['IES']['StageExportDoc']['string11']."_________________________";
		$content .= $this->get_p_close(); //</p>
		$content .= "</td></tr>";
	  	$content .= $this->get_table_close();    //</table>
	  	$content .= $this->get_center_close();  //</center>
	  	
	  	
		return $content;
	}
	
	public static function getSaveFileName($name) { 
	    $except = array('\\', '/', ':', '*', '?', '"', '<', '>', '|', ' '); 
	    return str_replace($except, '_', $name); 
	}
	
}
?>