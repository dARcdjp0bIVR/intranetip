<?
# using : Bill

include_once($intranet_root."/includes/sba/libSbaStep.php");
include_once($intranet_root."/includes/sba/libSbaStudentNextTaskHandler.php");

class SbaTaskMapper{
	private $eclass_db;
	private $intranet_db;
	private $objDB;
	private $selectSql;
	
	public function SbaTaskMapper(){
		global $eclass_db,$intranet_db;
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;

		$this->selectSql = 'select 
								TaskID , StageID , Title , Code , Sequence , Approval,	Enable, Introduction, Description, Attachments, InstantEdit,DateInput,InputBy,DateModified,ModifyBy 
							from 
							'.$this->intranet_db.'.IES_TASK where 1';
	}

	public function save(task &$objTask){

		$taskId = $objTask->getTaskID();

		if($taskId == ''){
			$objUpdateTask = $this->newRecord($objTask);
		}else{
			$objUpdateTask = $this->updateRecord($objTask);		
		}

		//assign back $objUpdateTask to $objTask , for new record case (get the task id)
		$objTask = $objUpdateTask;

	}

	public function getByTaskId($taskId){
		$sql = $this->selectSql. ' and taskId = '.$taskId;

		$resultSet = $this->objDB->returnResultSet($sql);
		if(is_array($resultSet) && sizeof($resultSet) == 1){
			return $this->createTaskObject(current($resultSet));
		}else{
			return null;
		}
	}

	public function findByStageId($stageId, $OrderBySequence=true){
		$sql  = $this->selectSql. ' and stageid = '.$stageId;
		$sql .= $OrderBySequence? ' order by Sequence':''; 

		$resultSet = $this->objDB->returnResultSet($sql);
		if(is_array($resultSet) && count($resultSet) >0){
			$returnAry = array();
			for($i = 0,$i_max = count($resultSet);$i < $i_max;$i++){
				$returnAry[] = $this->createTaskObject($resultSet[$i]);
			}
			return $returnAry;
		}else{
			return null;
		}
		
	}
	private function createTaskObject($ary){
		$objTask = new task();

		$objTask->setTaskID($ary['TaskID']);
		$objTask->setStageID($ary['StageID']);
		$objTask->setTitle($ary['Title']);
		$objTask->setCode($ary['Code']);
		$objTask->setSequence($ary['Sequence']);
		$objTask->setApproval($ary['Approval']);
		$objTask->setEnable($ary['Enable']);
		$objTask->setIntroduction($ary['Introduction']);
		$objTask->setDescription($ary['Description']);
		$objTask->setAttachments($ary['Attachments']);
		$objTask->setInstantEdit($ary['InstantEdit']);
		$objTask->setDateInput($ary['DateInput']);
		$objTask->setInputBy($ary['InputBy']);
		$objTask->setDateModified($ary['DateModified']);
		$objTask->setModifyBy($ary['ModifyBy']);

		return $objTask;
		
	}
	private function newRecord(task $objTask){
		$DataArr = array();
		$DataArr["StageID"]	 = $this->objDB->pack_value($objTask->getStageID(), 	 "int");
		$DataArr["Title"]	 = $this->objDB->pack_value($objTask->getTitle(), 	 "str");
		$DataArr["Code"]	 = $this->objDB->pack_value($objTask->getCode(), 	 "str"); 
		$DataArr["Sequence"]	 = $this->objDB->pack_value($objTask->getSequence(),	 "int");
		$DataArr["Enable"]	 = $this->objDB->pack_value($objTask->getEnable(), 	 "int");
		$DataArr["Approval"]	 = $this->objDB->pack_value($objTask->getApproval(), 	 "int");
		$DataArr["Introduction"] = $this->objDB->pack_value($objTask->getIntroduction(), "str");
		$DataArr["Description"]	 = $this->objDB->pack_value($objTask->getDescription(),  "str");
		$DataArr["Attachments"]	 = $this->objDB->pack_value($objTask->getAttachments(),   "str");
		$DataArr["InstantEdit"]	 = $this->objDB->pack_value($objTask->getInstantEdit(),  "int");
		$DataArr["DateInput"]	 = $this->objDB->pack_value($objTask->getDateInput(), 	 "date");
		$DataArr["InputBy"]	 = $this->objDB->pack_value($objTask->getInputBy(), 	 "int");
		$DataArr["DateModified"] = $this->objDB->pack_value($objTask->getDateModified(), "date");
		$DataArr["ModifyBy"]	 = $this->objDB->pack_value($objTask->getModifyBy(), 	 "int");

		$sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);

		$fieldStr= $sqlStrAry['sqlField'];
		$valueStr= $sqlStrAry['sqlValue'];

		$sql = 'Insert Into '.$this->intranet_db.'.IES_TASK ('.$fieldStr.') Values ('.$valueStr.')';
		
		$success = $this->objDB->db_db_query($sql);
								
		$RecordID = $this->objDB->db_insert_id();

		$objTask->setTaskID($RecordID);					
		return $objTask;
	}
	
	private function updateRecord(task $objTask){
		$DataArr = array();
		$DataArr["StageID"]	 = $this->objDB->pack_value($objTask->getStageID(), 	 "int");
		$DataArr["Title"] 	 = $this->objDB->pack_value($objTask->getTitle(), 	 "str");
		$DataArr["Code"]	 = $this->objDB->pack_value($objTask->getCode(), 	 "str"); 
		$DataArr["Sequence"]	 = $this->objDB->pack_value($objTask->getSequence(),	 "int");
		$DataArr["Enable"]	 = $this->objDB->pack_value($objTask->getEnable(), 	 "int");
		$DataArr["Approval"]	 = $this->objDB->pack_value($objTask->getApproval(), 	 "int");
		$DataArr["Introduction"] = $this->objDB->pack_value($objTask->getIntroduction(), "str");
		$DataArr["Description"]	 = $this->objDB->pack_value($objTask->getDescription(),  "str");
		$DataArr["Attachments"]	 = $this->objDB->pack_value($objTask->getAttachments(),   "str");
		$DataArr["InstantEdit"]	 = $this->objDB->pack_value($objTask->getInstantEdit(),  "int");
		$DataArr["DateModified"] = $this->objDB->pack_value($objTask->getDateModified(), "date");
		$DataArr["ModifyBy"]	 = $this->objDB->pack_value($objTask->getModifyBy(), 	 "int");

		foreach ($DataArr as $fieldName => $data)
		{
			$updateDetails .= $fieldName."=".$data.",";
		}

		//REMOVE LAST OCCURRENCE OF ",";
		$updateDetails = substr($updateDetails,0,-1);

		$sql = "update ".$this->intranet_db.".IES_TASK set ".$updateDetails." where TaskID = ".$objTask->getTaskID();

		$this->objDB->db_db_query($sql);			
		return $objTask;
	}
}
class task{

	private $TaskID;
	private $StageID;
	private $Title;
	private $Code;
	private $Sequence;
	private $Approval;
	private $Enable;
	private $Introduction;
	private $Description;
	private $Attachments;
	private $InstantEdit;
	private $DateInput;
	private $InputBy;
	private $DateModified;
	private $ModifyBy;
	private $objStepsAry;

	public function task($id = null){
		if($id != null){
			$this->setTaskID($id);
		}

	}
	public function setTaskID($val) {
		$this->TaskID = $val;
	}
	public function getTaskID() {
		return $this->TaskID;
	}

	public function setStageID($val) {
		$this->StageID = $val;
	}
	public function getStageID() {
		return $this->StageID;
	}

	public function setTitle($val) {
		$this->Title = $val;
	}
	public function getTitle() {
		return $this->Title;
	}

	public function setCode($val) {
		$this->Code = $val;
	}
	public function getCode() {
		return $this->Code;
	}

	public function setSequence($val) {
		$this->Sequence = $val;
	}
	public function getSequence() {
		return $this->Sequence;
	}

	public function setApproval($val) {
		$this->Approval = $val;
	}
	public function getApproval() {
		return $this->Approval;
	}

	public function setEnable($val) {
		$this->Enable = $val;
	}
	public function getEnable() {
		return $this->Enable;
	}
	public function setIntroduction($val) {
		$this->Introduction = $val;
	}
	public function getIntroduction() {
		return $this->Introduction;
	}
	public function setDescription($val) {
		$this->Description = $val;
	}
	public function getDescription() {
		return $this->Description;
	}
	public function setAttachments($val) {
	    
		$this->Attachments = $val;
	}
	public function getAttachments() {
		return $this->Attachments;
	}
	public function setInstantEdit($val) {
		$this->InstantEdit = $val;
	}
	public function getInstantEdit() {
		return $this->InstantEdit;
	}

	public function setDateInput($val) {
		$this->DateInput = $val;
	}
	public function getDateInput() {
		return $this->DateInput;
	}

	public function setInputBy($val) {
		$this->InputBy = $val;
	}
	public function getInputBy() {
		return $this->InputBy;
	}

	public function setDateModified($val) {
		$this->DateModified = $val;
	}
	public function getDateModified() {
		return $this->DateModified;
	}

	public function setModifyBy($val) {
		$this->ModifyBy = $val;
	}
	public function getModifyBy() {
		return $this->ModifyBy;
	}
	public function getObjStepsAry(){

		$objStepMapper = new SbaStepMapper();
		$this->setObjStepsAry($objStepMapper->findByTaskId($this->getTaskID()));
		return $this->objTasksAry;
	}

	//private now , no one can set Step with this class
	private function setObjStepsAry($objStepsAry){
		$this->objTasksAry = $objStepsAry;
	}
}
?>