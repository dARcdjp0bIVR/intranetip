<?
//edit by : henry chow
class SbaScheme {
	private $eclass_db;
	private $intranet_db;
	private $objDB;

	private $SchemeID;
	private $Title;
	private $Introduction;
	private $MaxScore;
	private $DateInput;
	private $InputBy;
	private $DateModified;
	private $ModifyBy;
	private $Version;
	private $Language;
	private $SchemeType;
	private $RecordStatus;
	private $DefaultSchemeCode;
	private $DefaultSchemeVersion;
	private $Description;

	public function SbaScheme($schemeId = null){  

		global $eclass_db,$intranet_db;
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;

		if($schemeId != null)
		{
			$this->setSchemeID(intval($schemeId));
			$this->loadDataFromStorage();
		}
	}

	public function setSchemeID($val) {
		$this->SchemeID = $val;
	}
	public function getSchemeID() {
		return $this->SchemeID;
	}

	public function setTitle($val) {
		$this->Title = $val;
	}
	public function getTitle() {
		return $this->Title;
	}



	public function setIntroduction($val) {
		$this->Introduction = $val;
	}
	public function getIntroduction() {
		return $this->Introduction;
	}




	public function setMaxScore($val) {
		$this->MaxScore = $val;
	}
	public function getMaxScore() {
		return $this->MaxScore;
	}

	public function setDateInput($val) {
		$this->DateInput = $val;
	}
	public function getDateInput() {
		return $this->DateInput;
	}

	public function setInputBy($val) {
		$this->InputBy = $val;
	}
	public function getInputBy() {
		return $this->InputBy;
	}

	public function setDateModified($val) {
		$this->DateModified = $val;
	}
	public function getDateModified() {
		return $this->DateModified;
	}

	public function setModifyBy($val) {
		$this->ModifyBy = $val;
	}
	public function getModifyBy() {
		return $this->ModifyBy;
	}

	public function setVersion($val) {
		$this->Version = $val;
	}
	public function getVersion() {
		return $this->Version;
	}

	public function setLanguage($val) {
		$this->Language = $val;
	}
	public function getLanguage() {
		return $this->Language;
	}

	public function setSchemeType($val) {
		$this->SchemeType = $val;
	}
	public function getSchemeType() {
		return $this->SchemeType;
	}

	public function setRecordStatus($val) {
		$this->RecordStatus = $val;
	}
	public function getRecordStatus() {
		return $this->RecordStatus;
	}

	public function setDefaultSchemeCode($val) {
		$this->DefaultSchemeCode = $val;
	}
	public function getDefaultSchemeCode() {
		return ($this->DefaultSchemeCode==null||$this->DefaultSchemeCode=='NULL')?0:$this->DefaultSchemeCode;
	}

	public function setDefaultSchemeVersion($val) {
		$this->DefaultSchemeVersion = $val;
	}
	public function getDefaultSchemeVersion() {
		return ($this->DefaultSchemeVersion==null||$this->DefaultSchemeVersion=='NULL')?0:$this->DefaultSchemeVersion;
	}

	public function setDescription($val) {
		$this->Description = $val;
	}
	public function getDescription() {
		return $this->Description;
	}
	
	public function save()
	{
		if(($this->getSchemeID() != "") && intval($this->getSchemeID()) > 0)
		{
			$resultId = $this->updateRecord();
		}
		else
		{	
			$resultId = $this->newRecord();
		}
		$this->setSchemeID($resultId);
//		$this->loadDataFromStorage($resultId);
		return $resultId;
	}

	private function newRecord(){

		$DataArr = array();

//		$DataArr["SchemeID"]		= $this->objDB->pack_value($this->getSchemeID(), "int");
		$DataArr["Title"]			= $this->objDB->pack_value($this->getTitle(), "str");
		$DataArr["Introduction"]	= $this->objDB->pack_value($this->getIntroduction(), "str");
		$DataArr["MaxScore"]		= $this->objDB->pack_value($this->getMaxScore(), "int");
		$DataArr["DateInput"]		= $this->objDB->pack_value($this->getDateInput(), "date");
		$DataArr["InputBy"]			= $this->objDB->pack_value($this->getInputBy(), "int");
		$DataArr["DateModified"]	= $this->objDB->pack_value($this->getDateModified(), "date");
		$DataArr["ModifyBy"]		= $this->objDB->pack_value($this->getModifyBy(), "int");
		$DataArr["Version"]			= $this->objDB->pack_value($this->getVersion(), "int");
		$DataArr["Language"]		= $this->objDB->pack_value($this->getLanguage(), "str");
		$DataArr["SchemeType"]		= $this->objDB->pack_value($this->getSchemeType(), "str");
		$DataArr["RecordStatus"]	= $this->objDB->pack_value($this->getRecordStatus(), "int");
		$DataArr["DefaultSchemeCode"]	= $this->objDB->pack_value($this->getDefaultSchemeCode(), "int");
		$DataArr["DefaultSchemeVersion"]	= $this->objDB->pack_value($this->getDefaultSchemeVersion(), "int");
		$DataArr["Description"]		= $this->objDB->pack_value($this->getDescription(), "str");

		$sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);

		$fieldStr= $sqlStrAry['sqlField'];
		$valueStr= $sqlStrAry['sqlValue'];


		$sql = 'Insert Into '.$this->intranet_db.'.IES_SCHEME ('.$fieldStr.') Values ('.$valueStr.')';

		$success = $this->objDB->db_db_query($sql);
								
		$RecordID = $this->objDB->db_insert_id();

		$this->setSchemeID($RecordID);
					
		$this->loadDataFromStorage();

		return $RecordID;

	}

	private function updateRecord(){

		$DataArr = array();

//		$DataArr["SchemeID"]		= $this->objDB->pack_value($this->getSchemeID(), "int"); //don't update while update record
		$DataArr["Title"]			= $this->objDB->pack_value($this->getTitle(), "str");
		$DataArr["Introduction"]		= $this->objDB->pack_value($this->getIntroduction(), "str");
		$DataArr["MaxScore"]		= $this->objDB->pack_value($this->getMaxScore(), "int");



	//	$DataArr["DateInput"]		= $this->objDB->pack_value($this->getDateInput(), "date"); // don't update while update record
//		$DataArr["InputBy"]			= $this->objDB->pack_value($this->getInputBy(), "int"); //don't update while update record
		$DataArr["DateModified"]	= $this->objDB->pack_value($this->getDateModified(), "date");
		$DataArr["ModifyBy"]						= $this->objDB->pack_value($this->getModifyBy(), "int");
		$DataArr["Version"]			= $this->objDB->pack_value($this->getVersion(), "int");
		$DataArr["Language"]		= $this->objDB->pack_value($this->getLanguage(), "str");
		$DataArr["SchemeType"]		= $this->objDB->pack_value($this->getSchemeType(), "str");
		$DataArr["RecordStatus"]	= $this->objDB->pack_value($this->getRecordStatus(), "int");
		$DataArr["DefaultSchemeCode"]	= $this->objDB->pack_value($this->getDefaultSchemeCode(), "int");
		$DataArr["DefaultSchemeVersion"]	= $this->objDB->pack_value($this->getDefaultSchemeVersion(), "int");
		$DataArr["Description"]		= $this->objDB->pack_value($this->getDescription(), "str");

		$updateDetails = "";

		foreach ($DataArr as $fieldName => $data)
		{
			$updateDetails .= $fieldName."=".$data.",";
		}

		//REMOVE LAST OCCURRENCE OF ",";
		$updateDetails = substr($updateDetails,0,-1);

		$sql = "update ".$this->intranet_db.".IES_SCHEME set ".$updateDetails." where SchemeId = ".$this->getSchemeID();

		$this->objDB->db_db_query($sql);

		return $this->getSchemeID();

	}
	
	private function loadDataFromStorage()
	{
		global $intranet_db;
		
		$sql = 'select 
				SchemeID, Title, Introduction,MaxScore, DateInput,InputBy, DateModified,ModifyBy,Version ,Language,SchemeType, RecordStatus, DefaultSchemeCode, DefaultSchemeVersion, Description
				from 
			'.$intranet_db.'.IES_SCHEME where schemeid = '.$this->getSchemeID();

		$result = $this->objDB->returnResultSet($sql);

		if(is_array($result) && sizeof($result) == 1){
			$result = current($result);
			$this->setSchemeID($result['SchemeID']);
			$this->setTitle($result['Title']);
			$this->setIntroduction($result['Introduction']);			
			$this->setMaxScore($result['MaxScore']);
			$this->setDateInput($result['DateInput']);
			$this->setInputBy($result['InputBy']);
			$this->setDateModified($result['DateModified']);
			$this->setModifyBy($result['ModifyBy']);
			$this->setVersion($result['Version']);
			$this->setLanguage($result['Language']);
			$this->setSchemeType($result['SchemeType']);
			$this->setRecordStatus($result['RecordStatus']);
			$this->setDefaultSchemeCode($result['DefaultSchemeCode']);
			$this->setDefaultSchemeVersion($result['DefaultSchemeVersion']);
			$this->setDescription($result['Description']);
		}		
	}	
	
	function IsEditable($thisUserID="")
	{
		/* about to edit SBA Scheme/Stage/Task/Step if $thisUserID is SBA Admin
		 * true => editable, false => not editable
		 */ 		
		
		global $UserID;
		
		if($thisUserID=="") $thisUserID = $UserID;
		
		return true;
	}
}
?>