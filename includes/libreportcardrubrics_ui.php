<?php
#  Editing by 
/*
 *  Date : 2019-07-29 (Bill)
 *          modified Get_Management_Marksheet_Import_Table()
 *  Date : 2019-03-21 (Bill)    [2018-1002-1146-08277]
 *          modified Get_Management_MarksheetRevision_UI()
 *          added Get_Management_Marksheet_Import_UI(), Get_Management_Marksheet_Import_Table(), Get_Management_Marksheet_Import_Step2_UI(), Get_Management_Marksheet_Import_Step3_UI()
 *          modified Get_Management_ECA_Service_Table()
 *          added Get_Management_ECA_Import_UI(), Get_Management_ECA_Import_Table(), Get_Management_ECA_Import_Step2_UI(), Get_Management_ECA_Import_Step3_UI()
 *          - for import functions
 *          modified Get_Module_Selection(), add parms - check if module is within date range
 *	Date : 2018-03-29 (Bill)    [2017-1102-1135-27054]
 *          modified Get_Form_Selection(), Get_Settings_CommentBank_New_Edit_UI(), Get_Statistics_ClassTopicPerformance_Flash_Chart(), to fix PHP 5.4 problem of trim()
 */

class libreportcardrubrics_ui extends interface_html {

	function libreportcardrubrics_ui()
	{
		parent::interface_html();
		
		$this->thickBoxWidth  = 750;
		$this->thickBoxHeight = 450;
		$this->toolCellWidth = 100;
	}
	
	function Include_JS_CSS($PluginArr='')
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x = '';
		
		if ($PluginArr=='' || in_array('thickbox', $PluginArr))
		{
			$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>'."\n";
			$x .= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />'."\n";
		}
		
		if ($PluginArr=='' || in_array('jeditable', $PluginArr))
			$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.jeditable.js"></script>'."\n";
		
		if ($PluginArr=='' || in_array('block_ui', $PluginArr))
			$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>'."\n";
		
		if ($PluginArr=='' || in_array('tablednd', $PluginArr))
			$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.tablednd_0_5.js"></script>'."\n";
		
		if ($PluginArr=='' || in_array('copypaste', $PluginArr))
			$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/js/copypaste.js"></script>'."\n";
		
		if ($PluginArr=='' || in_array('autocomplete', $PluginArr))
		{
			$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.js"></script>';
			$x .= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.css" type="text/css" />';
		}
		
		if ($PluginArr=='' || in_array('rubrics_css', $PluginArr))
			$x .= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/css/reportcard_rubrics.css" type="text/css" media="screen" />'."\n";
			
		if ($PluginArr=='' || in_array('jquery_ui', $PluginArr))
			$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-ui-1.7.3.custom.min.js"></script>'."\n";
			
		if ($PluginArr=='' || in_array('super_table', $PluginArr))
		{
			$x .= '<link href="'.$PATH_WRT_ROOT.'/templates/'.$LAYOUT_SKIN.'/css/superTables.css" rel="stylesheet" type="text/css">'."\n";
			$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.superTable.js"></script>'."\n";
			$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/superTables.js"></script>'."\n";
		}
		
		if ($PluginArr=='' || in_array('rubrics_js', $PluginArr))
			$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/js/reportcard_rubrics.js"></script>'."\n";
			
		if ($PluginArr=='' || in_array('cookies', $PluginArr))
			$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.cookies.2.2.0.js"></script>'."\n";
			
		return $x;
	}
	
	function Include_Template_CSS()
	{
		global $PATH_WRT_ROOT, $eRC_Rubrics_ConfigArr;
		
		$TemplateCSS = $eRC_Rubrics_ConfigArr['TemplateArr']['CSS'];
		$TemplateCSSPath = $PATH_WRT_ROOT."templates/2009a/css/reportcard_rubrics/". $TemplateCSS;
			
		if($TemplateCSS != "" && file_exists($TemplateCSSPath))
		{
			$CssLink = '<link href="'.$TemplateCSSPath.'" rel="stylesheet" type="text/css" />';
		}
		else
		{
			//$CssLink = '<link href="'.$TemplateCSSPath.'" rel="stylesheet" type="text/css" />';
		}
		
		return $CssLink;
	}
	
	# General
	function Get_Form_Stage_Selection($ID_Name, $SelectedFormStageID='', $Onchange='', $noFirst=0, $isAll=0, $firstTitle='', $TeachingFormOnly=0, $isMultiple=0)
	{
		global $lreportcard, $PATH_WRT_ROOT;
		
		if(!$lreportcard)
		{
			include_once($PATH_WRT_ROOT.'includes/libreportcardrubrics.php');
			$lreportcard = new libreportcardrubrics();
		}
		
		if($TeachingFormOnly)
			$FormStageInfoArr = $lreportcard->Get_User_Accessible_Form_Stage();
		else
			$FormStageInfoArr = $lreportcard->Get_All_Form_Stage();
		$OptionArr = BuildMultiKeyAssoc($FormStageInfoArr, "FormStageID", Get_Lang_Selection('TitleCh', 'TitleEn'), 1);
		
		$onchange = "";
		if ($Onchange != "")
			$onchange = ' onchange="'.$Onchange.'" ';
			
		if($isMultiple ==1)
			$multiple = ' multiple size="10" ';
		
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$multiple;
		
		$selection = getSelectByAssoArray($OptionArr, $selectionTags, $SelectedFormStageID, $isAll, $noFirst, $firstTitle);
	
		return $selection;	
	}
	
	function Get_Form_Selection($ID_Name, $SelectedYearID='', $Onchange='', $noFirst=0, $isAll=0, $firstTitle='', $TeachingFormOnly=0, $isMultiple=0, $FormStageID='')
	{
		global $lreportcard, $PATH_WRT_ROOT;
		
		if(!$lreportcard)
		{
			include_once($PATH_WRT_ROOT.'includes/libreportcardrubrics.php');
			$lreportcard = new libreportcardrubrics();
		}
		
		if($TeachingFormOnly) {
			$FormArr = $lreportcard->Get_User_Accessible_Form();
		}
		else {
			$FormArr = $lreportcard->GET_ALL_FORMS();
		}
		
		// if(trim($FormStageID) != '')
		if(!is_array($FormStageID) && trim($FormStageID) != '' || is_array($FormStageID))
		{
			include_once($PATH_WRT_ROOT.'includes/libformstage.php');
			foreach((array)$FormStageID as $thisFormStageID)
			{
				$lformstage = new Form_Stage($thisFormStageID);
				$Formlist = $lformstage->Get_Form_List();
				
				$thisFormIDArr = Get_Array_By_Key($Formlist, "YearID");
				$YearIDArr = array_merge((array)$YearIDArr,$thisFormIDArr); 
			} 
			
			foreach($FormArr as $thisForm)
			{
				if(!in_array($thisForm['ClassLevelID'],(array)$YearIDArr)) {
					continue;
				}
				
				$OptionArr[$thisForm['ClassLevelID']] = $thisForm['LevelName'];
			}
		}	
		else
		{
			$OptionArr = BuildMultiKeyAssoc($FormArr,"ClassLevelID","LevelName",1);	
		}
		
		$onchange = "";
		if ($Onchange != "") {
			$onchange = ' onchange="'.$Onchange.'" ';
		}
		
		if($isMultiple == 1) {
			$multiple = ' multiple size="10" ';
		}
		
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$multiple;
		$selection = getSelectByAssoArray($OptionArr, $selectionTags, $SelectedYearID, $isAll, $noFirst, $firstTitle);
        
		return $selection;
	}
	
	function Get_Class_Selection($ID_Name, $SelectedClassID='', $Onchange='', $noFirst=0, $isAll=0, $firstTitle='', $ClassLevelID='', $TeachingClassOnly=0, $isMultiple=0)
	{
		global $lreportcard, $PATH_WRT_ROOT;
		
		if(!$lreportcard)
		{
			include_once($PATH_WRT_ROOT.'includes/libreportcardrubrics.php');
			$lreportcard = new libreportcardrubrics();
		}
		
		if($TeachingClassOnly)
			$ClassArr = $lreportcard->Get_User_Accessible_Class($ClassLevelID);
		else
			$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);

		$OptionArr = BuildMultiKeyAssoc($ClassArr,array("YearName","ClassID"),"ClassName",1);
		
		$onchange = "";
		if ($Onchange != "")
			$onchange = ' onchange="'.$Onchange.'" ';
			
		$style = '';
		if ($isMultiple)
			$style = ' multiple="true" size="10"';
		
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$style;
		
		$selection = getSelectByAssoArray($OptionArr, $selectionTags, $SelectedClassID, $isAll, $noFirst, $firstTitle);
	
		return 	$selection;	
	}
	
	function Get_Settings_CurriculumPool_Tab_Array($CurrentTab)
	{
		global $Lang;
		
		$TagArr = array();
		$thisIsCurrent = ($CurrentTab == 'CurriculumPool')? true : false;
		$TagArr[] = array($Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['MenuTitle'], "curriculum_pool.php?clearCoo=1", $thisIsCurrent);
		//$thisIsCurrent = ($CurrentTab == 'CurriculumFormMappingPool')? true : false;
		//$TagArr[] = array($Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['CurriculumnFormMapping'], "curriculum_form_mapping.php?clearCoo=1", $thisIsCurrent);
		
		return $TagArr;
	}
	
	function Get_Settings_CurriculumPool_UI()
	{
		global $PATH_WRT_ROOT, $Lang, $ReportCard_Rubrics_CustomSchoolName, $eRC_Rubrics_ConfigArr;
		
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		include_once($PATH_WRT_ROOT.'includes/form_class_manage_ui.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
		
		$fcm_ui = new form_class_manage_ui();
		$scm_ui = new subject_class_mapping_ui();
		$MaxTopicLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		
		### Import Button
		$btn_Import = $this->Get_Content_Tool_v30("import","javascript:js_Import();");
		
		### Export Button
		$ExportOptionArr = array();
		$ExportOptionArr[] = array('javascript:js_Export(1);', $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['AllSubjects']);
		$ExportOptionArr[] = array('javascript:js_Export(0);', $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ThisSubject']);
		$btn_Export = $this->Get_Content_Tool_v30('export', $href="javascript:void(0);", $text="", $ExportOptionArr, $other="", $divID='ExportDiv');
				
		### Subject Selection
		$OnChange = "js_Changed_Subject_Selection(this.value);";
		$SubjectSelection = $scm_ui->Get_Subject_Selection('SubjectID', $SubjectID='', $OnChange, $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0);
		
		### Form Selection
		$OnChange = "js_Changed_Form_Selection(this.value);";
		$FormSelection = $fcm_ui->Get_Form_Selection('YearID', $SelectedYearID='', $OnChange, $noFirst=1, $isAll=1, $isMultiple=0);
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2">'."\n";
						$x .= '<div class="content_top_tool">'."\n";
							$x .= '<div class="Conntent_tool">'."\n";
								$x .= $btn_Import."\n";
								$x .= $btn_Export."\n";
							$x .= '</div>'."\n";
						$x .= '</div>'."\n";	
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";	
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div style="float:left;">'.$SubjectSelection.'</div>'."\n";
						for ($thisLevel=1; $thisLevel<$MaxTopicLevel; $thisLevel++)
						{
							$thisDivID = "Level".$thisLevel."_SelectionDiv";
							$x .= '<div style="float:left;">&nbsp;&nbsp;</div>'."\n";
							$x .= '<div id="'.$thisDivID.'" style="float:left;"></div>'."\n";
						}
						$x .= '<div style="float:left;">&nbsp;&nbsp;</div>'."\n";
						$x .= '<div style="float:left;">'.$FormSelection.'</div>'."\n";
					$x .= '</td>'."\n";
					$x .= '<td align="right">'.$SearchBox.'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr><td colspan="2"><div id="SubjectCurriculumDiv"></div></td></tr>'."\n";
			$x .= '</table>'."\n";
			
			### Topic Remarks Layer
			$x .= '<div id="TopicRemarksDiv" class="selectbox_layer" style="visibility:hidden; width:350px;"></div>'."\n";
			
			### Hidden Field (for export use)
			$x .= '<input type="hidden" id="FilterTopicInfoText" name="FilterTopicInfoText" value="" />'."\n";
			$x .= '<input type="hidden" id="ExportAllSubject" name="ExportAllSubject" value="" />'."\n";
			
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";
		return $x;
	}
	
	function Get_Settings_CurriculumPool_Table($SubjectID, $FilterLevelTopicArr='', $YearID='')
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName, $eRC_Rubrics_ConfigArr;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
		include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");

		$lreportcard_topic = new libreportcardrubrics_topic();
		
		### Get Subject Topics Info
		$YearID = ($YearID == -1)? '' : $YearID;	// $YearID = -1 means All Forms
		$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectID);
		$TopicTreeArr = $lreportcard_topic->Get_Topic_Tree($SubjectID, $ParentTopicIDArr='', $MaxLevel='', $YearID);
		
		
		### Get Topic Applicable Form Mapping
		$TopicIDArr = array_keys($TopicInfoArr);
		$TopicApplicableFormAssoArr = $lreportcard_topic->Get_Topic_Applicable_Form_Mapping($TopicIDArr);
		
		
		### Get Column Width
		$numOfLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		$Level1CodeWidth = 6;
		$Level1NameWidth = 10;
		$LastLevelFormMappingWidth = 10;
		$widthOfColumn = floor((100 - $Level1CodeWidth - $Level1NameWidth - $LastLevelFormMappingWidth) / $numOfLevel);
		
		$x = '';
		$x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
			$x .= '<col align="left" style="width: '.$Level1CodeWidth.'%;">'."\n";
			$x .= '<col align="left" style="width: '.$Level1NameWidth.'%;">'."\n";
			for ($i=0; $i<$numOfLevel-1; $i++)
			{
				$thisWidth = $widthOfColumn - 6;
				$x .= '<col align="left" style="width: 6%;">'."\n";
				$x .= '<col align="left" style="width: '.$thisWidth.'%;">'."\n";
			}
			$x .= '<col align="left" style="width: '.$LastLevelFormMappingWidth.'%;">'."\n";
			
			### Level Title
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					for ($thisLevel=1; $thisLevel<=$numOfLevel; $thisLevel++)
					{
						$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$thisLevel]['Title'];
						$thisClass = 'class="level'.$thisLevel.'_table_header"';
						$thisColspan = ($thisLevel==$numOfLevel)? 3 : 2;
						
						$x .= '<th colspan="'.$thisColspan.'" '.$thisClass.'>'."\n";
							$x .= '<span style="float:left;" class="row_content_v30">'.$thisTitle.'</span>'."\n";
							if ($thisLevel == 1 && ($FilterLevelTopicArr == '' || count($FilterLevelTopicArr) == 0))
							{
								### Level 1 Settings Button
								$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$thisLevel]['Settings'];
								$thisOnclick = "js_Get_Topic_Settings_Layer('', '$thisLevel', '', ''); return false;";
								
								$x .= '<span style="float: right;" class="table_row_tool row_content_tool">'."\n";
									$x .= $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, 'setting_row', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
								$x .= '</span>'."\n";
							}
						$x .= '</th>'."\n";
					}
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			$x .= '<tbody>'."\n";
				### Code and Name Display
				$x .= '<tr>'."\n";
					for ($thisLevel=1; $thisLevel<=$numOfLevel; $thisLevel++)
					{
						$thisClass = 'class="level'.$thisLevel.'_table_header"';
						
						$x .= '<th '.$thisClass.'>'.$Lang['eRC_Rubrics']['GeneralArr']['Code'].'</th>'."\n";
						$x .= '<th '.$thisClass.'>'.$Lang['eRC_Rubrics']['GeneralArr']['Name'].'</th>'."\n";
						if ($thisLevel==$numOfLevel) {
							$x .= '<th '.$thisClass.'>'.$Lang['eRC_Rubrics']['GeneralArr']['ApplicableForm'].'</th>'."\n";
						}
					}
				$x .= '</tr>'."\n";
				
				### Topic Code and Name Display
				if (count($TopicInfoArr) == 0)
				{
					$colspan = ($numOfLevel * 2) + 1;
					$x .= '<tr><td colspan="'.$colspan.'" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
				}
				else
				{
					$x .= $this->Get_Settings_CurriculumPool_Table_Rows($TopicTreeArr, $TopicInfoArr, $FilterLevelTopicArr, $TopicApplicableFormAssoArr);
				}	
			$x .= '<tbody>'."\n";
		$x .= '</table>'."\n";
		return $x;
	}
	
	/*
	 * Recursively generate the tabel rows
	 */
	function Get_Settings_CurriculumPool_Table_Rows($TopicTreeArr, $TopicInfoArr, $FilterLevelTopicArr, $TopicApplicableFormAssoArr)
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName, $eRC_Rubrics_ConfigArr;
		
		$MaxLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		
		$x = '';
		foreach ($TopicTreeArr as $thisTopicID => $thisLinkedTopicIDArr)
		{
			$thisLevel					= $TopicInfoArr[$thisTopicID]['Level'];
			$thisCode					= $TopicInfoArr[$thisTopicID]['TopicCode'];
			$thisParentTopicID 			= $TopicInfoArr[$thisTopicID]['ParentTopicID'];
			$thisPreviousTopicID 		= $TopicInfoArr[$thisTopicID]['PreviousLevelTopicID'];
			$thisNameEn 				= $TopicInfoArr[$thisTopicID]['TopicNameEn'];
			$thisNameCh 				= $TopicInfoArr[$thisTopicID]['TopicNameCh'];
			$thisName 					= Get_Lang_Selection($thisNameCh, $thisNameEn);
			
			if ($thisLevel > $MaxLevel)
				continue; 
				
			### $FilterLevelTopicArr != '' means have filtering selection
			### is_array($FilterLevelTopicArr[$thisLevel]) means have filtering selection in this Topic Level
			### $FilterLevelTopicArr[$thisLevel][$thisTopicID] != 1 means the filtering is not selected this Topic
			if ($FilterLevelTopicArr != '' && is_array($FilterLevelTopicArr[$thisLevel]) && $FilterLevelTopicArr[$thisLevel][$thisTopicID] != 1)
				continue;
				
			$x .= '<tr>'."\n";
			
				### Empty Cell
				for ($i=1; $i<$thisLevel; $i++)
				{
					$tmpLevel = floor($i/2) + 1;
					$thisClass = 'class="level'.$tmpLevel.'_table_content"';
					$x .= '<td '.$thisClass.'>&nbsp;</td>'."\n";
					$x .= '<td '.$thisClass.'>&nbsp;</td>'."\n";
				}
				
				# Add onmouseover event for the Cell if it is the 1st Level to show the Remarks 
				$thisID = '';
				$thisIdTag = '';
				$thisTag = '';
				if ($thisLevel == 1)
				{
					$thisID = 'TopicID_'.$thisTopicID;
					$thisIdTag = ' id="'.$thisID.'" ';
					$thisTag = ' onmouseover="js_Show_Topic_Remarks_Layer('.$thisTopicID.', \''.$thisID.'\', \'TopicRemarksDiv\');" onmouseout="js_Hide_Topic_Remarks_Layer(\'TopicRemarksDiv\');" ';
				}

				$thisClass = 'class="level'.$thisLevel.'_table_content"';
				$x .= '<td '.$thisClass.'>'.$thisCode.'</td>'."\n";
				$x .= '<td '.$thisClass.'>'."\n";
					$x .= '<div '.$thisIdTag.' '.$thisTag.'>'."\n";
						$x .= $thisName."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
				
				if ($thisLevel == $MaxLevel)
				{
					# Applicable Form settings for the last Level
					$thisApplicableFormNameArr = Get_Array_By_Key((array)$TopicApplicableFormAssoArr[$thisTopicID], 'YearName');
					$thisApplicableFormDisplay = implode(', ', $thisApplicableFormNameArr);
					
					$x .= '<td '.$thisClass.'>'."\n";
						$x .= '<span>'.$thisApplicableFormDisplay.'</span>'."\n";
						$x .= '<span style="float: right;" class="table_row_tool row_content_tool">'."\n";
							$x .= $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, 'edit', $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ToolTipArr']['EditApplicableForm'], 
																"js_Get_Applicable_Form_Settings_Layer('$thisTopicID')", $InlineID="FakeLayer", $Content="");
						$x .= '</span>'."\n";
					$x .= '</td>'."\n";
				}
				else
				{
					$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$thisLevel]['Settings'];
					$x .= '<td '.$thisClass.'>&nbsp;</td>'."\n";
					
					if ($thisLevel == $MaxLevel - 1) {
						$x .= '<td '.$thisClass.'>&nbsp;</td>'."\n";
					}
					
					$x .= '<td '.$thisClass.'>'."\n";
						### Topic Settings Button
						if ($FilterLevelTopicArr == '' || ($FilterLevelTopicArr != '' && !is_array($FilterLevelTopicArr[$thisLevel+1])))
						{
							$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$thisLevel]['Settings'];
							$thisOnclick = "js_Get_Topic_Settings_Layer('$thisTopicID', '".($thisLevel + 1)."', '$thisParentTopicID', '$thisPreviousTopicID'); return false;";
							$x .= '<span style="float: right;" class="table_row_tool row_content_tool">'."\n";
								$x .= $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, 'setting_row', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
							$x .= '</span>'."\n";
						}
						else
						{
							$x .= '&nbsp;'."\n";
						}
					$x .= '</td>'."\n";
				}
						
				for ($i=$thisLevel; $i<$MaxLevel-1; $i++)
				{
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
				}
			$x .= '</tr>'."\n";
			
			if (count($thisLinkedTopicIDArr) > 0)
				$x .= $this->Get_Settings_CurriculumPool_Table_Rows($thisLinkedTopicIDArr, $TopicInfoArr, $FilterLevelTopicArr, $TopicApplicableFormAssoArr);
		}
		return $x;
	}
	
	function Get_Settings_Topic_Layer($SubjectID, $TopicID, $Level, $ParentTopicID, $PreviousLevelTopicID)
	{
		$x = "";
		$x .= '<div id="debugArea"></div>';
		
		$x .= '<div class="edit_pop_board" style="height:410px;">';
			$x .= $this->Get_Thickbox_Return_Message_Layer();
			$x .= '<div id="LevelSettingLayer" class="edit_pop_board_write" style="height:370px;">';
				$x .= $this->Get_Settings_Topic_Table($SubjectID, $TopicID, $Level, $ParentTopicID, $PreviousLevelTopicID);
			$x .= '</div>';
		$x .= '</div>';
		
		return $x;
	}
	
	function Get_Settings_Topic_Table($SubjectID, $TopicID, $Level, $ParentTopicID, $PreviousLevelTopicID)
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
		include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");

		$lreportcard_topic = new libreportcardrubrics_topic();
		
		### Get the Selected Topic Info
		if ($Level == 1)
		{
			$PresetTopicCode = '';
			$CodeWidth= '15%';
			$NameWidth= '20%';
			$RemarksWidth= '30%';
		}
		else
		{
			$SelectedTopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectID, $TopicID, '', $TopicCode='', '', $ParentTopicID, $ReturnAsso=0);
			$PresetTopicCode = $SelectedTopicInfoArr[0]['TopicCode'];
			
			$CodeWidth= '20%';
			$NameWidth= '30%';
		}
		
		### Get Next Level Topic Info of the selected Topic
		$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectID, '', $Level, $TopicCode='', $TopicID, $ParentTopicID, $ReturnAsso=0);
		$numOfTopic = count($TopicInfoArr);
		
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();");
		
		$x = '';
		
		### Previous Level Info Display
		$x .= $this->Get_Settings_Topic_Info_Table($SubjectID, $TopicID, $ParentTopicID, $Level-1);
		$x .= '<br style="clear:both;" />'."\n";
		
		### Content Table
		$x .= '<table id="TopicInfoTable" class="common_table_list_v30" style="width:95%;" align="center">';
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th style="width:'.$CodeWidth.'">'.$Lang['eRC_Rubrics']['GeneralArr']['Code'].'</th>';
					$x .= '<th style="width:'.$NameWidth.'">'.$Lang['eRC_Rubrics']['GeneralArr']['NameEn'].'</th>';
					$x .= '<th style="width:'.$NameWidth.'">'.$Lang['eRC_Rubrics']['GeneralArr']['NameCh'].'</th>';
					
					if ($Level == 1)
						$x .= '<th style="width:'.$RemarksWidth.'">'.$Lang['eRC_Rubrics']['GeneralArr']['Remarks'].'</th>';
					
					$x .= '<th style="width:5%">&nbsp;</th>';
					$x .= '<th style="width:'.$this->toolCellWidth.'px">&nbsp;</th>';
				$x .= '</tr>';
			$x .= '</thead>';
			
			$x .= '<tbody>';
			
			# Display Topic Info
			for ($i=0; $i<$numOfTopic; $i++)
			{
				$thisTopicID = $TopicInfoArr[$i]['TopicID'];
				$thisCode = $TopicInfoArr[$i]['TopicCode'];
				$thisNameEn = $TopicInfoArr[$i]['TopicNameEn'];
				$thisNameCh = $TopicInfoArr[$i]['TopicNameCh'];
				$thisRemarks = str_replace("\n", "<br />", $TopicInfoArr[$i]['Remarks']);	// cannot use nl2br to compatible with the jeditable plugin
				
				$x .= '<tr id="'.$thisTopicID.'">';
					# Code
					$x .= '<td>';
						$x .= $this->Get_Thickbox_Edit_Div("Code_".$thisTopicID, "Code_".$thisTopicID, "jEditCode", $thisCode);
						$x .= $this->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv_".$thisTopicID);
					$x .= '</td>';
					
					# Title (English)
					$x .= '<td>';
						$x .= $this->Get_Thickbox_Edit_Div("NameEn_".$thisTopicID, "NameEn_".$thisTopicID, "jEditNameEn", $thisNameEn);
						$x .= $this->Get_Thickbox_Warning_Msg_Div("NameEnWarningDiv_".$thisTopicID);
					$x .= '</td>';
					
					# Title (Chinese)
					$x .= '<td>';
						$x .= $this->Get_Thickbox_Edit_Div("NameCh_".$thisTopicID, "NameCh_".$thisTopicID, "jEditNameCh", $thisNameCh);
						$x .= $this->Get_Thickbox_Warning_Msg_Div("NameChWarningDiv_".$thisTopicID);
					$x .= '</td>';
					
					# Remarks
					if ($Level == 1)
					{
						$x .= '<td>';
							$x .= $this->Get_Thickbox_Edit_Div("Remarks_".$thisTopicID, "Remarks_".$thisTopicID, "jEditRemarks", $thisRemarks);
						$x .= '</td>';
					}
					
					# Move Icon
					$x .= '<td class="Dragable" align="left">';
						$x .= $this->GET_LNK_MOVE("#", $Lang['Btn']['Move']);
					$x .= '</td>';
					
					# Delete Icon
					$x .= '<td align="left">';
						$x .= $this->GET_LNK_DELETE("#", $Lang['Btn']['Delete'], "js_Delete_Topic('$thisTopicID'); return false;");
					$x .= '</td>';
				$x .= '</tr>';
			}
			
				# Add Topic Button
				$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$Level]['Add'];
				$x .= '<tr id="AddTopicRow" class="nodrop nodrag">';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>&nbsp;</td>';
					if ($Level == 1)
						$x .= '<td>&nbsp;</td>';
					$x .= '<td>';
						$x .= '<div class="table_row_tool row_content_tool">';
							$x .= '<a href="#" onclick="js_Add_Temp_Topic_Row(); return false;" class="add_dim" title="'.$thisTitle.'"></a>';
						$x .= '</div>';
					$x .= '</td>';
				$x .= '</tr>';
			$x .= '</tbody>';
		$x .= '</table>';
		
		$x .= '<input type="hidden" id="Level" name="Level" value="'.$Level.'">';
		$x .= '<input type="hidden" id="TopicID" name="TopicID" value="'.$TopicID.'">';
		$x .= '<input type="hidden" id="ParentTopicID" name="ParentTopicID" value="'.$ParentTopicID.'">';
		$x .= '<input type="hidden" id="PreviousLevelTopicID" name="PreviousLevelTopicID" value="'.$PreviousLevelTopicID.'">';
		
		// For preset Code when adding Topic
		$x .= '<input type="hidden" id="PresetTopicCode" name="PresetTopicCode" value="'.intranet_htmlspecialchars($PresetTopicCode).'">';
		
		$x .= '<br />';
				
		$x .= '<div class="edit_bottom_v30">';
			# Last Updated Info Div
			$LastModifiedInfoArr = $lreportcard_topic->Get_Topic_Last_Modified($SubjectID, $Level, $TopicID);
			if (count($LastModifiedInfoArr) > 0)
			{
				$lastModifiedDate = $LastModifiedInfoArr[0]['DateModified'];
				$lastModifiedBy = $LastModifiedInfoArr[0]['LastModifiedBy'];
				$LastModifiedDisplay = Get_Last_Modified_Remark($lastModifiedDate, '', $lastModifiedBy);
				$x .= '<span>'.$LastModifiedDisplay.'</span>';
				$x .= '<p class="spacer"></p>';
			}
			
			### Close Buttom
			$x .= $CloseBtn."\n";
		$x .= '</div>';
		
		return $x;
	}
	
	function Get_Settings_Topic_Applicable_Form_Layer($TopicID)
	{
		$x = "";
		$x .= '<div id="debugArea"></div>';
		
		$x .= '<div class="edit_pop_board" style="height:410px;">';
			$x .= $this->Get_Thickbox_Return_Message_Layer();
			$x .= '<div id="ApplicableFormSettingLayer" class="edit_pop_board_write" style="height:370px;">';
				$x .= $this->Get_Settings_Topic_Applicable_Form_Table($TopicID);
			$x .= '</div>';
		$x .= '</div>';
		
		return $x;
	}
	
	function Get_Settings_Topic_Applicable_Form_Table($TopicID)
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
		include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
		
		### Get Topic Info
		$lreportcard_topic = new libreportcardrubrics_topic();
		$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectID='', $TopicID, $Level='', $TopicCode='', $PreviousLevelTopicID='', $ParentTopicID='', $ReturnAsso=0);
				
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();");
		$SaveBtn = $this->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "js_Save_Topic_Form_Mapping('$TopicID');", $id="Btn_Save");
		
		$x = '';
		
		### Topic Info Display
		$SubjectID = $TopicInfoArr[0]['SubjectID'];
		$ParentTopicID = $TopicInfoArr[0]['ParentTopicID'];
		$Level = $TopicInfoArr[0]['Level'];
		$x .= $this->Get_Settings_Topic_Info_Table($SubjectID, $TopicID, $ParentTopicID, $Level);
		
		
		### Applicable Form Mapping
		$x .= '<table class="form_table_v30">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['GeneralArr']['ApplicableForm'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<span style="float:left;">'."\n";
						$x .= $this->Get_Form_Checkbox_Table($TopicID);
					$x .= '</span>'."\n";
					$x .= '<span style="float:left;">'."\n";
						$x .= $this->Get_Thickbox_Warning_Msg_Div('SelectApplicableFormWarningDiv', $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['WarningArr']['ApplicableForm']);
					$x .= '</span>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<div class="edit_bottom_v30">';
			# Last Updated Info Div
			$LastModifiedInfoArr = $lreportcard_topic->Get_Topic_Form_Mapping_Last_Modified($TopicID);
			if (count($LastModifiedInfoArr) > 0)
			{
				$lastModifiedDate = $LastModifiedInfoArr[0]['DateModified'];
				$lastModifiedBy = $LastModifiedInfoArr[0]['LastModifiedBy'];
				$LastModifiedDisplay = Get_Last_Modified_Remark($lastModifiedDate, '', $lastModifiedBy);
				$x .= '<span>'.$LastModifiedDisplay.'</span>';
				$x .= '<p class="spacer"></p>';
			}
			
			### Buttons
			$x .= $SaveBtn."\n";
			$x .= '&nbsp;'."\n";
			$x .= $CloseBtn."\n";
		$x .= '</div>';
		
		return $x;
	}
	
	function Get_Form_Checkbox_Table($TopicID='')
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName;
		
		include_once($PATH_WRT_ROOT."includes/libformstage.php");
		$libFormStage = new Form_Stage();
		$FormStageInfoArr = $libFormStage->Get_All_Form_Stage($active=1);
		$numOfFormStage = count($FormStageInfoArr);
		
		$TopicApplicableFormIDArr = array();
		if ($TopicID != '')
		{
			include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
			include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
			include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
			
			$lreportcard_topic = new libreportcardrubrics_topic();
			$TopicApplicableFormAssoArr = $lreportcard_topic->Get_Topic_Applicable_Form_Mapping($TopicID);
			$TopicApplicableFormIDArr = Get_Array_By_Key((array)$TopicApplicableFormAssoArr[$TopicID], 'YearID');
		}
		
		$x = '';
		$x .= '<table border="0" cellpadding="0" cellspacing="0">'."\n";
			$numOfFormPerRow = 3;
			for ($i=0; $i<$numOfFormStage; $i++)
			{
				$thisFormStageID = $FormStageInfoArr[$i]['FormStageID'];
				$thisFormStageName = Get_Lang_Selection($FormStageInfoArr[$i]['TitleCh'], $FormStageInfoArr[$i]['TitleEn']);
				
				$thisObjFormStage = new Form_Stage($thisFormStageID);
				$thisFormInfoArr = $thisObjFormStage->Get_Form_List();
				$thisNumOfForm = count($thisFormInfoArr);
				
				### Stage Checkbox
				$thisCheckAllID = 'FormStageChk_'.$thisFormStageID;
				$thisCheckAllName = 'FormStageChk_'.$thisFormStageName;
				$thisFormStageClass = 'FormStageChk_'.$thisFormStageID;
				$x .= '<tr><td colspan="'.($numOfFormPerRow + 1).'" style="border:0px;">'."\n";
					$x .= $thisCheckbox = $this->Get_Checkbox($thisCheckAllID, $thisCheckAllName, $thisFormStageID, $isChecked=false, $thisClass='', $thisFormStageName, "Check_All_Options_By_Class('".$thisFormStageClass."', this.checked);");
				$x .= '</td></tr>'."\n";
				
				
				for ($j=0; $j<$thisNumOfForm; $j++)
				{
					$thisYearID = $thisFormInfoArr[$j]['YearID'];
					$thisYearName = $thisFormInfoArr[$j]['YearName'];
					
					$thisID = 'YearIDChk_'.$thisYearID;
					$thisName = 'YearIDChk[]';
					$thisChecked = (in_array($thisYearID, $TopicApplicableFormIDArr))? true : false;
					
					if ($j % $numOfFormPerRow == 0)
					{
						$x .= '<tr>'."\n";
							$x .= '<td style="width:16%;border:0px;padding:0px;">&nbsp;</td>'."\n";
					}
						
							$x .= '<td style="width:28%;border:0px;padding:0px;">'."\n";
								$x .= $this->Get_Checkbox($thisID, $thisName, $thisYearID, $thisChecked, "$thisFormStageClass FormChk", $thisYearName, "Uncheck_SelectAll('".$thisCheckAllID."', this.checked);");
							$x .= '</td>'."\n";
					
					if ($j % $numOfFormPerRow == ($numOfFormPerRow - 1) || ($j == ($thisNumOfForm - 1)))
						$x .= '</tr>'."\n";
				}
				
			}
		$x .= '</table>'."\n";
		return $x;
	}
	
	function Get_Settings_Topic_Info_Table($SubjectID, $TopicID, $ParentTopicID, $Level)
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
		include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
		include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		
		### Get Subject Info
		$ObjSubject = new subject($SubjectID);
		$SubjectName = $ObjSubject->Get_Subject_Desc();
		
		### Get Topic Info
		$lreportcard_topic = new libreportcardrubrics_topic();
		$TopicInfoArr = $lreportcard_topic->Get_Topic_Info('', '', '', $TopicCode='', $PreviousLevelTopicID='', $ParentTopicID, $ReturnAsso=1, $MaxLevel=$Level);

		### Get Topic Mapping
		$TopicTreeArr = $lreportcard_topic->Get_Topic_Tree($SubjectID, $ParentTopicID, $Level);
		
		$x = '';
		$x .= '<table class="form_table_v30">'."\n";
			
			# Subject
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $SubjectName;
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
				
			# Topic
			$x .= $this->Get_Settings_Topic_Info_Table_Row($TopicTreeArr, $TopicInfoArr, $TopicID, $ParentTopicID, $Level);
			
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Settings_Topic_Info_Table_Row($TopicTreeArr, $TopicInfoArr, $TargetTopicID)
	{
		global $Lang;
		$x = '';
		
		foreach ($TopicTreeArr as $thisTopicID => $thisLinkedTopicIDArr)
		{
			$thisLevel	= $TopicInfoArr[$thisTopicID]['Level'];
			$thisNameEn = $TopicInfoArr[$thisTopicID]['TopicNameEn'];
			$thisNameCh = $TopicInfoArr[$thisTopicID]['TopicNameCh'];
			$thisName 	= Get_Lang_Selection($thisNameCh, $thisNameEn);
			
			if (count($thisLinkedTopicIDArr) > 0)
				list($isTrue, $TmpInfoRow) = $this->Get_Settings_Topic_Info_Table_Row($thisLinkedTopicIDArr, $TopicInfoArr, $TargetTopicID);
			
			if ($thisTopicID == $TargetTopicID)
				$isTrue = true;
			
			if ($isTrue)
			{
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$thisLevel]['Title'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $thisName;
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";	
			}
			
			if ($isTrue && $thisLevel > 1)
				return array($isTrue, $x.$TmpInfoRow);
		}
		return $x.$TmpInfoRow;
	}
	
	function Get_Settings_CurriculumPool_Import_Step1_UI()
	{
		global $Lang, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
		//include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
		$libreportcard_topic = new libreportcardrubrics_topic();
		//$libSCP_ui = new subject_class_mapping_ui();
		
		### Navaigtion
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['MenuTitle'], "javascript:js_Go_Back();");
		$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
		
		### Subject Selection
		//$SubjectSelection = $libSCP_ui->Get_Subject_Selection('SubjectID', $SubjectID, $OnChange='', $noFirst=0, $Lang['SysMgr']['SubjectClassMapping']['All']['Subject'], $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0);
		
		### CSV sample
		$SampleCSVFile = "<a class='tablelink' href='javascript:js_Go_Download_CSV_Sample();'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";
		
		### Column Description
		$ColumnTitleArr = $libreportcard_topic->Get_Topic_Import_Export_Column_Title_Arr();
		$ColumnTitleArr = Get_Lang_Selection($ColumnTitleArr['Ch'], $ColumnTitleArr['En']);
		$ColumnPropertyArr = $libreportcard_topic->Get_Topic_Import_Export_Column_Property();
		$ColumnDisplay = $this->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr);
		
		### Buttons
		$ContinueBtn = $this->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();");
		$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_Back();");
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post" action="import_class_student_confirm.php" enctype="multipart/form-data">'."\n";
			$x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
			$x .= '<br style="clear:both;" />'."\n";
			$x .= $this->GET_IMPORT_STEPS($CurrStep=1);
			$x .= '<div class="table_board">'."\n";
				$x .= '<table id="html_body_frame" width="100%">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'."\n";
							$x .= '<table class="form_table_v30">'."\n";
//								$x .= '<tr>'."\n";
//									$x .= '<td class="field_title">'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
//									$x .= '<td>'.$SubjectSelection.'</td>'."\n";
//								$x .= '</tr>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['General']['SourceFile'].' <span class="tabletextremark">'.$Lang['General']['CSVFileFormat'].'</span></td>'."\n";
									$x .= '<td class="tabletext">'."\n";
										$x .= '<input class="file" type="file" name="csvfile" id="csvfile">'."\n";
										$x .= '<br />'."\n";
										$x .= '<span class="tabletextrequire">'.$Lang['General']['ImportArr']['SecondRowWarn'].'</span>'."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['General']['CSVSample'].'</td>'."\n";
									$x .= '<td>'.$SampleCSVFile.'</td>'."\n";
								$x .= '</tr>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'."\n";
										$x .= $Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']."\n";
									$x .= '</td>'."\n";
									$x .= '<td>'."\n";
										$x .= $ColumnDisplay."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td class="tabletextremark" colspan="2">'."\n";
										$x .= $this->MandatoryField();
										$x .= $this->ReferenceField();
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</table>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
			$x .= '</div>'."\n";
			
			# Buttons
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $ContinueBtn;
				$x .= "&nbsp;";
				$x .= $CancelBtn;
			$x .= '</div>'."\n";
			
			# Hidden Field
			$x .= '<input type="hidden" id="ForImportSample" name="ForImportSample" value="1">'."\n";
			
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";
			
		return $x;
	}
	
	function Get_Settings_CurriculumPool_Import_Step2_UI($TargetFilePath, $numOfCsvData)
	{
		global $Lang, $PATH_WRT_ROOT;
		
		### Navaigtion
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['MenuTitle'], "javascript:js_Cancel();");
		$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
		
		### Buttons
		$ContinueBtn = $this->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();", 'ContinueBtn', '', $Disabled=1);
		$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();");
		$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Cancel();");
		
		
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post" action="import_step3.php">'."\n";
			$x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
			$x .= '<br style="clear:both;" />'."\n";
			$x .= $this->GET_IMPORT_STEPS($CurrStep=2);
			$x .= '<div class="table_board">'."\n";
				$x .= '<table id="html_body_frame" width="100%">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'."\n";
							$x .= '<table class="form_table_v30">'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
									$x .= '<td><div id="SuccessCountDiv">'.$Lang['General']['EmptySymbol'].'</div></td>'."\n";
								$x .= '</tr>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
									$x .= '<td><div id="FailCountDiv">'.$Lang['General']['EmptySymbol'].'</div></td>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</table>'."\n";
							
							$x .= '<br style="clear:both;" />'."\n";
							
							$x .= '<div id="ErrorTableDiv"></div>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
			$x .= '</div>'."\n";
			
			# Buttons
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $ContinueBtn;
				$x .= '&nbsp;'."\n";
				$x .= $BackBtn;
				$x .= '&nbsp;'."\n";
				$x .= $CancelBtn;
			$x .= '</div>'."\n";
			
			# iFrame for validation
			$thisSrc = "ajax_validate.php?Action=Import_Topic&TargetFilePath=".$TargetFilePath;
			$x .= '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;display:none;"></iframe>'."\n";
			
			# Hidden Field for Step 3 use
			$x .= '<input type="hidden" id="numOfCsvData" name="numOfCsvData" value="'.$numOfCsvData.'" />'."\n"; 
			
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";
			
		return $x;
	}
	
	function Get_Settings_CurriculumPool_Import_Step3_UI()
	{
		global $Lang, $PATH_WRT_ROOT;
		
		### Navaigtion
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['MenuTitle'], "javascript:js_Back_To_Curriculum_Pool();");
		$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
		
		### Buttons
		$BackBtn = $this->GET_ACTION_BTN($Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['BackToCurriculumPool'], "button", "js_Back_To_Curriculum_Pool();");
		$ImportOtherBtn = $this->GET_ACTION_BTN($Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportOtherCurriculum'], "button", "js_Back_To_Import_Step1();");
				
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
			$x .= '<br style="clear:both;" />'."\n";
			$x .= $this->GET_IMPORT_STEPS($CurrStep=3);
			$x .= '<div class="table_board">'."\n";
				$x .= '<table id="html_body_frame" width="100%">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'."\n";
							$x .= '<table class="form_table_v30">'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td class="tabletext" style="text-align:center;">'."\n";
										$x .= '<span id="ImportStatusSpan"></span>'."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</table>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
			$x .= '</div>'."\n";
			
			# Buttons
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $ImportOtherBtn;
				$x .= '&nbsp;'."\n";
				$x .= $BackBtn;
			$x .= '</div>'."\n";
			
			# iFrame for importing data
			$thisSrc = "ajax_update.php?Action=Import_Topic";
			$x .= '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;display:none;"></iframe>'."\n";
						
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";
			
		return $x;
	}
	
	function Get_Settings_StudentLearningDetails_UI()
	{
		global $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName, $Lang, $lreportcard;
		global $eRC_Rubrics_ConfigArr;
	
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
		
		$MaxTopicLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		
		$scm_ui = new subject_class_mapping_ui();
		
		### Term Selection
		$CurrentYearTermArr = getCurrentAcademicYearAndYearTerm();
		$CurrentYearTermID = $CurrentYearTermArr['YearTermID'];
		
		$OnChange = "js_Changed_Term_Selection(this.value);";
		$TermSelection = $scm_ui->Get_Term_Selection('YearTermID', $lreportcard->AcademicYearID, $CurrentYearTermID, $OnChange, $NoFirst=0, $NoPastTerm=0, $displayAll=1, $AllTitle='', $PastTermOnly=0, $CompareYearTermID='');
		
		### Subject Selection
		//$OnChange = "js_Changed_Subject_Selection(this.value);";
		//$SubjectSelection = $scm_ui->Get_Subject_Selection('SubjectID', $SubjectID='', $OnChange, $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0);
		
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				### Filters
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div style="float:left;">'.$TermSelection.'</div>'."\n";
						$x .= '<div style="float:left;">&nbsp;&nbsp;</div>'."\n";
						$x .= '<div style="float:left;" id="ModuleSelectionDiv"></div>'."\n";
					//	$x .= '<br style="clear:both;" />'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
					
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div style="float:left;" id="SubjectSelectionDiv">'.$SubjectSelection.'</div>'."\n";
						$x .= '<div style="float:left;">&nbsp;&nbsp;</div>'."\n";
						for ($thisLevel=1; $thisLevel<$MaxTopicLevel; $thisLevel++)
						{
							$thisDivID = "Level".$thisLevel."_SelectionDiv";						
							$x .= '<div id="'.$thisDivID.'" class="LevelSelectionDiv" style="float:left;"></div>'."\n";
							$x .= '<div style="float:left;">&nbsp;&nbsp;</div>'."\n";
						}			
						
					$x .= '</td>'."\n";
					$x .= '<td align="right">'.$SearchBox.'</td>'."\n";
				$x .= '</tr>'."\n";
				
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div style="float:left;" id="SubjectGroupSelectionDiv"></div>'."\n";
						$x .= '<div style="float:left;">&nbsp;&nbsp;</div>'."\n";
						$x .= '<div style="float:left;" id="StudentSelectionDiv"></div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				### Warning Message and Remarks Message
				$WarningMsgBox = $this->Get_Warning_Message_Box('', $Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['WarningArr']['RecordOverwriteIfSaveForSG'])."\n";
				
				$BgColorArr = array('#FFFFFF', '#F0F0F0', '#C0C0C0');
				$numOfType = count($BgColorArr);
				$RemarksArr = array();
				for ($i=0; $i<$numOfType; $i++)
				{
					$thisBgColor = $BgColorArr[$i];
					$thisRemarks = $Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['WarningArr']['BackgroundInfoInstructionArr'][$i];
					$RemarksArr[] = '<span style="background-color:'.$thisBgColor.';">'.$thisRemarks.'</span>';
				}
				$RemarksMsgBox = $this->Get_Warning_Message_Box($Lang['General']['Remark'], implode('<br />', $RemarksArr))."\n";
				
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2" align="center">'."\n";
						$x .= '<div id="SaveSG_WarningDiv"">'."\n";
							$x .= $WarningMsgBox."\n";
							$x .= $RemarksMsgBox."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				### Content Table
				$x .= '<tr><td colspan="2"><div id="StudentLearningDetailsDiv"></div></td></tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";
		return $x;
	}
	
	function Get_Settings_StudentLearningDetails_Table($YearTermID, $SubjectID, $SubjectGroupID, $StudentID='', $ModuleID='',$FilterTopicInfoText='',$noStudent=false)
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName, $eRC_Rubrics_ConfigArr;
		global $TopicInfoArr, $ModuleInfoArr, $StudentStudyTopicInfoArr, $IsAllStudentStudyArr;		// For Table Row Generation
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
		include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		
		$lreportcard_topic = new libreportcardrubrics_topic();
		$lreportcard_module = new libreportcardrubrics_module();
			
		$topiclistArr=array();
		
		### Get Subject Topics Info
		$FilterTopicID = '';
		$FilterObjectiveID = '';
		

		$FilterTopicInfoTextArr = explode('::',$FilterTopicInfoText);
		$numOfTopicFilter = count($FilterTopicInfoTextArr);
		for ($i=0; $i<$numOfTopicFilter; $i++) {
			$thisFilterTopicInfoTextArr = $FilterTopicInfoTextArr[$i];
			$thisFilterTopicInfoTextPieces = explode(',', $thisFilterTopicInfoTextArr);
			$thisFilterTopicLevel = $thisFilterTopicInfoTextPieces[0];
			$thisFilterTopicID = $thisFilterTopicInfoTextPieces[1];
			
			$FilterTopicInfoTextArr[$i] = ($thisFilterTopicID=='')? 'nothing' : $thisFilterTopicID;
			$FilterTopicInfoTextArr[$i] = ($thisFilterTopicID=='ALL')? '' : $thisFilterTopicID;
		}

		### Get Subject Topics Info
		//$TopicTreeArr = $lreportcard_topic->Get_Topic_Tree($SubjectID);
		$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectID);	
		$TopicTreeArr = $lreportcard_topic->Get_Topic_Tree($SubjectID,$FilterTopicInfoTextArr[0]);

		$noThisTopicArr = array();	
		if(count($TopicTreeArr)==0) {
			
		}
		else {
			$_topicTree = $TopicTreeArr;
			$thisTopicTreeArr = $TopicTreeArr;
			for ($i=0; $i<$numOfTopicFilter; $i++) {
				$thisTopicLevel = $i + 1;		// array index start from 0 but level index start from 1
				$thisTopicID = $FilterTopicInfoTextArr[$i];
				
				$_topicTree = $_topicTree[$thisTopicID];
				
				if($thisTopicID=='nothing') {	
						break;
				}
				
				if($_topicTree==null) {		

				}
				else {
					$thisTopicTreeArr = $_topicTree;
								
					$j_MAX = $i;
					for ($j=$j_MAX; $j>=0; $j--) {
						$_topicID = $FilterTopicInfoTextArr[$j];
						$thisTopicTreeArr = array($_topicID=>$thisTopicTreeArr);
					}
					$TopicTreeArr= $thisTopicTreeArr;															
				}
			}
		}

					
		### Topic list Array
		$topiclistArr = array();
		$FilterTopicID =$FilterTopicInfoTextArr[0];
		
		if($FilterTopicID!='') {
			$topiclistArr = $lreportcard_topic->Get_Topic_All_Last_Level_Topic($FilterTopicID, $TopicTreeArr, $TopicInfoArr);
		}
		else {			
			$_thisArray = array();
			foreach ((array)$TopicTreeArr as $_topicID => $_topicArr) {
				$_thisArray = $lreportcard_topic->Get_Topic_All_Last_Level_Topic($_topicID, $TopicTreeArr, $TopicInfoArr);
	
				foreach ((array)$_thisArray as $_topicID => $_topicArr) {
					$topiclistArr[] = $_topicArr;
				}
			}
		}

		
		### Get Module Info
		$ModuleInfoArr = $lreportcard_module->Get_Module_Info($YearTermID, $ModuleID);
		$ModuleIDArr = Get_Array_By_Key($ModuleInfoArr, 'ModuleID');
		$numOfModule = count($ModuleInfoArr);
		
		### Check if all student in the Subject Group Study the Topic in different Modules
		// $IsAllStudentStudyArr[$ModuleID][$TopicID] = true / false
		$IsAllStudentStudyArr = $lreportcard_topic->Check_Is_All_Student_Study_Topic($SubjectID, $SubjectGroupID);
		
		### Get Term Info
		$ObjAcademicYear = new academic_year($lreportcard_topic->AcademicYearID);
		$TermInfoAssoArr = $ObjAcademicYear->Get_Term_List();
		$TermModuleCountArr = array();		// $TermModuleCountArr[$YearTermID] = num of Module in the Term
		for ($i=0; $i<$numOfModule; $i++)
		{
			$thisYearTermID = $ModuleInfoArr[$i]['YearTermID'];
			$TermModuleCountArr[$thisYearTermID]++;
		}
	
		### Get Student / Subject Group Study Topic Info
		if ($StudentID!='')
			$TargetSubjectGroupID = '';
		else
			$TargetSubjectGroupID = $SubjectGroupID;
		$StudentStudyTopicInfoArr = $lreportcard_topic->Get_Student_Study_Topic_Info($ModuleID, $SubjectID, $TopicID='', $StudentID, $TargetSubjectGroupID);
		
		### Get Column Width
		$numOfLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		$ModuleColumnWidth = 6;
		$TotalModuleWidth = $ModuleColumnWidth * $numOfModule;
		$Level1Width = 8;
		$widthOfTopicColumn = floor((100 - $TotalModuleWidth - $Level1Width) / $numOfLevel);
		$NumOfColumn = $numOfLevel + $numOfModule + 1;	// + 1 means the "Apply to All" Column
		
		### Save Button
		$btn_Save = $this->GET_ACTION_BTN($Lang['Btn']['Save'], "button", $onclick="js_Save_Study_Learning_Details();", $id="Btn_Save");
			
		
		
			
		$x = '';
		$x .= '<table id="ContentTable" class="common_table_list_v30 edit_table_list_v30">'."\n";
			### Levels
			for ($i=0; $i<$numOfLevel; $i++)
			{
				$thisWidth = ($i==0)? $Level1Width : $widthOfTopicColumn;
				$x .= '<col align="left" style="width: '.$thisWidth.'%;">'."\n";
			}
			### Modules
			for ($i=0; $i<$numOfModule; $i++)
			{
				$x .= '<col align="left" style="width: '.$ModuleColumnWidth.'%;">'."\n";
			}
			### Apply to All
			$x .= '<col align="left" style="width: '.$ModuleColumnWidth.'%;">'."\n";
			
			### Level Title
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					### Level Title
					for ($thisLevel=1; $thisLevel<=$numOfLevel; $thisLevel++)
					{
						$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$thisLevel]['Title'];
						
						$x .= '<th rowspan="2">'."\n";
							$x .= '<span style="float:left;" class="row_content_v30">'.$thisTitle.'</span>'."\n";
						$x .= '</th>'."\n";
					}
					
					### Term Title
					foreach ((array)$TermModuleCountArr as $thisYearTermID => $thisNumOfModuleInTerm)
					{
						$thisTermName = ($thisYearTermID == 0)? $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['WholeYear'] : $TermInfoAssoArr[$thisYearTermID];
						$x .= '<th colspan="'.$thisNumOfModuleInTerm.'" style="text-align:center;">'."\n";
							$x .= '<span>'.$thisTermName.'</span>'."\n";
						$x .= '</th>'."\n";
					}
					
					### Apply to all
					$thisChkID = 'ModuleAllChk';
					$thisChkOnClick = "js_Select_All_Topic_And_Module(this.checked);";
					$x .= '<th rowspan="2" class="apply_all_top" style="text-align:center;">'."\n";
						$x .= $Lang['Btn']['ApplyToAll'];
					$x .= '</th>'."\n";
				$x .= '</tr>'."\n";
					
				$x .= '<tr>'."\n";
					### Module Title
					for ($i=0; $i<$numOfModule; $i++)
					{
						$thisModuleID = $ModuleInfoArr[$i]['ModuleID'];
						
						$thisModuleTitle = Get_Lang_Selection($ModuleInfoArr[$i]['ModuleNameCh'], $ModuleInfoArr[$i]['ModuleNameEn']);
						
						$thisChkID = "ModuleAllChk_".$thisModuleID;
						$thisChkClass = "ModuleAllChk";
						$thisChkOnClick = "js_Select_All_Topic_Of_Module('$thisModuleID', this.checked);";
						
						$x .= '<th style="text-align:center;">'."\n";
							$x .= $thisModuleTitle."\n";
						$x .= '</th>'."\n";
					}
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			$x .= '<tbody>'."\n";
				### Select all button row
				$x .= '<tr class="edit_table_head_bulk">'."\n";
					# Level
					for ($thisLevel=1; $thisLevel<=$numOfLevel; $thisLevel++)
					{
						$x .= '<th>&nbsp;</th>'."\n";
					}
					
					### Module
					for ($i=0; $i<$numOfModule; $i++)
					{
						$thisModuleID = $ModuleInfoArr[$i]['ModuleID'];
						$thisChkID = "ModuleAllChk_".$thisModuleID;
						$thisChkClass = "ModuleAllChk";
						$thisChkOnClick = "js_Select_All_Topic_Of_Module('$thisModuleID', this.checked);";
						
						$x .= '<th style="text-align:center;">'."\n";
							$x .= $this->Get_Checkbox($thisChkID, $thisChkName='', $Value=1, $thisChecked=0, $thisChkClass, $Display='', $thisChkOnClick)."\n";
						$x .= '</th>'."\n";
					}
					
					# Apply to all
					$thisChkID = 'ModuleAllChk';
					$thisChkOnClick = "js_Select_All_Topic_And_Module(this.checked);";
					$x .= '<th class="apply_all_top_bulk" style="text-align:center;">'."\n";
						$x .= $this->Get_Checkbox($thisChkID, $thisChkName='', $Value=1, $thisChecked=0, $thisChkClass='', $Display='', $thisChkOnClick)."\n";
					$x .= '</th>'."\n";
				$x .= '</tr>'."\n";
			
		
				### Topic Code and Name Display
				$DisplayButton = false;
				if (count($TopicInfoArr) == 0)
				{
					$x .= '<tr><td colspan="'.$NumOfColumn.'" align="center">'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoTopicForSubject'].'</td></tr>'."\n";
				}
				else if ($numOfModule == 0)
				{
					$x .= '<tr><td colspan="'.$NumOfColumn.'" align="center">'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoModuleInTerm'].'</td></tr>'."\n";
				}
				else if ($noStudent)
				{
					$x .= '<tr><td colspan="'.$NumOfColumn.'" align="center">'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoStudent'].'</td></tr>'."\n";
				}
				else
				{
					$x .= $this->Get_Settings_StudentLearningDetails_Table_Row($SubjectID, $SubjectGroupID, $StudentID, $TopicTreeArr);
					$DisplayButton = true;
				}
					
			$x .= '<tbody>'."\n";
		$x .= '</table>'."\n";
		$x .= '<br style="clear:both" />'."\n";
		
		if (count($TopicInfoArr) > 0)
		{
			$x .= '<div class="edit_bottom_v30">';
				# Last Updated Info Div
				if (count($StudentStudyTopicInfoArr) > 0)
				{
					foreach ((array)$StudentStudyTopicInfoArr as $thisTargetID => $thisStudentStudyTopicInfoArr)
					{
						foreach ((array)$thisStudentStudyTopicInfoArr as $thisModuleID => $thisModuleStudyTopicInfoArr)
						{
							foreach ((array)$thisModuleStudyTopicInfoArr as $thisSubjectID => $thisSubjectStudentStudyTopicInfoArr)
							{
								foreach ((array)$thisSubjectStudentStudyTopicInfoArr as $thisTopicID => $thisTopicModuleStudentStudyInfoArr)
								{
									$DateModified = $thisTopicModuleStudentStudyInfoArr['DateModified'];
									$LastModifiedBy = $thisTopicModuleStudentStudyInfoArr['LastModifiedBy'];
									break;
								}
								break;
							}
							break;
						}
						break;
					}
					
					$LastModifiedDisplay = Get_Last_Modified_Remark($DateModified, '', $LastModifiedBy);
					$x .= '<span>'.$LastModifiedDisplay.'</span>';
					$x .= '<p class="spacer"></p>';
				}
				
				### Save Buttom
				if ($DisplayButton)
					$x .= $btn_Save."\n";
			$x .= '</div>';
			
			### Topic Remarks Layer
			$x .= '<div id="TopicRemarksDiv" class="selectbox_layer" style="visibility:hidden; width:350px;"></div>'."\n";
			
			$x .= '<input type="hidden" id="ModuleIDList" name="ModuleIDList" value="'.implode(',', (array)$ModuleIDArr).'" />'."\n";
			$x .= '<input type="hidden" id="TopicIDList" name="TopicIDList" value="'.implode(',', (array)$topiclistArr).'" />';
		}
		return $x;
	}
	

	function Get_Settings_StudentLearningDetails_Table_Row($SubjectID, $SubjectGroupID, $StudentID, $TopicTreeArr)
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName, $eRC_Rubrics_ConfigArr;
		global $TopicInfoArr, $ModuleInfoArr, $StudentStudyTopicInfoArr, $IsAllStudentStudyArr;	// Get From function Get_Settings_StudentLearningDetails_Table() already
		
		$MaxLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		$numOfModule = count($ModuleInfoArr);
		
		$x = '';
		foreach ($TopicTreeArr as $thisTopicID => $thisLinkedTopicIDArr)
		{
			$thisLevel					= $TopicInfoArr[$thisTopicID]['Level'];
			$thisCode					= $TopicInfoArr[$thisTopicID]['TopicCode'];
			$thisParentTopicID 			= $TopicInfoArr[$thisTopicID]['ParentTopicID'];
			$thisNameEn 				= $TopicInfoArr[$thisTopicID]['TopicNameEn'];
			$thisNameCh 				= $TopicInfoArr[$thisTopicID]['TopicNameCh'];
			$thisName 					= Get_Lang_Selection($thisNameCh, $thisNameEn);
			
			if ($thisLevel > $MaxLevel)
				continue; 
				
			$x .= '<tr>'."\n";
			
				### Empty Cell
				for ($i=1; $i<$thisLevel; $i++)
				{
					$x .= '<td>&nbsp;</td>'."\n";
				}
				
				# Add onmouseover event for the Cell if it is the 1st Level to show the Remarks 
				$thisID = '';
				$thisIdTag = '';
				$thisTag = '';
				if ($thisLevel == 1)
				{
					$thisID = 'TopicID_'.$thisTopicID;
					$thisIdTag = ' id="'.$thisID.'" ';
					$thisTag = ' onmouseover="js_Show_Topic_Remarks_Layer('.$thisTopicID.', \''.$thisID.'\', \'TopicRemarksDiv\');" onmouseout="js_Hide_Topic_Remarks_Layer(\'TopicRemarksDiv\');" ';
				}
				
				$x .= '<td>'."\n";
					$x .= '<div '.$thisIdTag.' '.$thisTag.'>'.$thisName.'</div>'."\n";
				$x .= '</td>'."\n";
				
				for ($i=$thisLevel; $i<$MaxLevel; $i++)
				{
					$x .= '<td>&nbsp;</td>'."\n";
				}
			
			if (count($thisLinkedTopicIDArr) > 0)
			{
					for ($i=0; $i<$numOfModule+1; $i++)
					{
						$x .= '<td>&nbsp;</td>'."\n";
					}
				$x .= '</tr>'."\n";
				$x .= $this->Get_Settings_StudentLearningDetails_Table_Row($SubjectID, $SubjectGroupID, $StudentID, $thisLinkedTopicIDArr);
			}
			else
			{
				if ($thisLevel == $MaxLevel)
				{
					### Last Level of Topic => Module Topic Checkboxes
					for ($i=0; $i<$numOfModule; $i++)
					{
						$thisModuleID = $ModuleInfoArr[$i]['ModuleID'];
						$thisCellID = "Cell_".$thisModuleID."_".$thisTopicID;
						$thisChkID = "Study_".$thisModuleID."_".$thisTopicID;
						$thisChkName = "StudyArr[$thisModuleID][$thisTopicID]";
						$thisChkClass = "ChkModuleID_".$thisModuleID." ChkTopicID_".$thisTopicID." ChkStudy";
						$thisChkOnClick = "js_Clicked_Checkbox($thisModuleID, $thisTopicID, this.checked);";
						
						if ($StudentID!='')
							$thisStudy = isset($StudentStudyTopicInfoArr[$StudentID][$thisModuleID][$SubjectID][$thisTopicID]);
						else if ($SubjectGroupID!='')
							$thisStudy = isset($StudentStudyTopicInfoArr[$SubjectGroupID][$thisModuleID][$SubjectID][$thisTopicID]);
						
						if ($StudentID=='')
						{
							# Subject Group View => white if all student study
							#						dark grey if no student study
							#						lite grey if not all student study this topic
							if ($IsAllStudentStudyArr[$thisModuleID][$thisTopicID] == 1)
								$thisClass = '';
							else if ($IsAllStudentStudyArr[$thisModuleID][$thisTopicID] == -1 || !isset($IsAllStudentStudyArr[$thisModuleID][$thisTopicID]))
								$thisClass = 'class="not_check_cell"';
							else
								$thisClass = 'class="special_cell"';
						}
						else
						{
							# Student => 	dark grey background if not study
							#				white background if study 
							$thisClass = ($thisStudy)? '' : 'class="not_check_cell"';
						}
							
						$x .= '<td id="'.$thisCellID.'" '.$thisClass.' style="text-align:center;">'."\n";
							$x .= $this->Get_Checkbox($thisChkID, $thisChkName, $Value=1, $thisStudy, $thisChkClass, $Display='', $thisChkOnClick)."\n";
						$x .= '</td>'."\n";
					}
					
					### Apply to all
					$thisChkID = "TopicAllChk_".$thisTopicID;
					$thisChkClass = "TopicAllChk";
					$thisChkOnClick = "js_Select_All_Module_Of_Topic('$thisTopicID', this.checked);";
					$x .= '<td style="text-align:center;">'."\n";
						$x .= $this->Get_Checkbox($thisChkID, $thisChkName='', $Value=1, $Checked='', $thisChkClass, $Display='', $thisChkOnClick)."\n";
					$x .= '</td>'."\n";
				}
				else
				{
					### Some Topics which do not have the last level => cannot set to study
					for ($i=0; $i<$numOfModule+1; $i++)
					{
						$x .= '<td>&nbsp;</td>'."\n";
					}
				}
				$x .= '</tr>'."\n";
			}
		}
		return $x;
	}
	
	function Get_Topic_Remarks_Layer($TopicID)
	{
		global $PATH_WRT_ROOT, $Lang;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
		$lreportcard_topic = new libreportcardrubrics_topic();
		
		$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectID='', $TopicID, $Level='', $TopicCode='', $PreviousLevelTopicID='', $ParentTopicID='', $ReturnAsso=0);
		
		$TopicLevel = $TopicInfoArr[0]['Level'];
		$RemarksTitle = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$TopicLevel]['Remarks'];		
		
		$TopicRemarks = ($TopicInfoArr[0]['Remarks'] == '')? $Lang['General']['EmptySymbol'] : $TopicInfoArr[0]['Remarks'];
		$TopicRemarks = nl2br($TopicRemarks);
				
		$x = '';
		$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\n";
			$x .= '<tbody>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td align="left" style="border-bottom: medium none;">'."\n";
						$x .= $RemarksTitle.": \n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td align="left" style="border-bottom: medium none;">'."\n";
						$x .= $TopicRemarks."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Settings_ModuleSubject_UI()
	{
		global $Lang, $PATH_WRT_ROOT, $lreportcard;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		
		$lreportcard_module = new libreportcardrubrics_module();
		
		# Get all Learning Category
		$libLC = new Learning_Category();
		$LC_InfoArr = $libLC->Get_All_Learning_Category($returnAsso=1);
		$numOfLC = count($LC_InfoArr);
				
		# Get All Subjects Info
		$libSubject = new Subject();
		# $SubjectInfoArr[LearningCategoryID][SubjectID] = $infoArr[Title, Code....]
		$SubjectInfoArr = $libSubject->Get_Subject_List($returnAsso=1);
		$numOfSubject = count($SubjectInfoArr);
		
		### Get Module Info
		$ModuleInfoArr = $lreportcard_module->Get_Module_Info();
		$ModuleIDArr = Get_Array_By_Key($ModuleInfoArr, 'ModuleID');
		$numOfModule = count($ModuleInfoArr);
		
		### Get Term Info
		$ObjAcademicYear = new academic_year($lreportcard->AcademicYearID);
		$TermInfoAssoArr = $ObjAcademicYear->Get_Term_List();
		$TermModuleCountArr = array();		// $TermModuleCountArr[$YearTermID] = num of Module in the Term
		for ($i=0; $i<$numOfModule; $i++)
		{
			$thisYearTermID = $ModuleInfoArr[$i]['YearTermID'];
			$TermModuleCountArr[$thisYearTermID]++;
		}
		
		### Get Module Subject Info
		// $ModuleSubjectInfoArr[$ModuleID][$SubjectID] = Info Array
		$ModuleSubjectInfoArr = $lreportcard->Get_Module_Subject();
		
		### Save Button
		$btn_Save = $this->GET_ACTION_BTN($Lang['Btn']['Save'], "button", $onclick="js_Save_Module_Subject();", $id="Btn_Save");
				
		$x = '';
		$x .= '<form id="form1" name="form1" method="post">'."\n";
		
			$x .= $this->Get_Warning_Message_Box($Lang['General']['Remark'], implode('<br />', $Lang['eRC_Rubrics']['SettingsArr']['ModuleSubjectArr']['RemarksArr']));
			
			$x .= '<table id="ContentTable" class="common_table_list_v30 edit_table_list_v30">'."\n";
				$x .= '<col align="left" style="width:10%;">'."\n";
				$x .= '<col align="left" style="width:15%;">'."\n";
				
				### Modules
				$ModuleWidth = floor(75 / ($numOfModule + 1));
				for ($i=0; $i<$numOfModule; $i++)
				{
					$x .= '<col style="width:'.$ModuleWidth.'%;text-align:center;">'."\n";
				}
				### Apply to All
				$x .= '<col style="width:'.$ModuleWidth.'%;text-align:center;">'."\n";
				
				$NumOfColumn = $numOfModule + 3;
				
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						# Learning Category
						$x .= '<th rowspan="2">'.$Lang['SysMgr']['SubjectClassMapping']['LearningCategory'].'</th>'."\n";
						
						# Subject
						$x .= '<th rowspan="2">'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</th>'."\n";
						
						### Term Title
						foreach ((array)$TermModuleCountArr as $thisYearTermID => $thisNumOfModuleInTerm)
						{
							if ($thisYearTermID == 0)
								$thisTermName = $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['WholeYear'];
							else
								$thisTermName = $TermInfoAssoArr[$thisYearTermID];
								
							$x .= '<th colspan="'.$thisNumOfModuleInTerm.'" style="text-align:center;">'."\n";
								$x .= '<span>'.$thisTermName.'</span>'."\n";
							$x .= '</th>'."\n";
						}
						
						### Apply to all
						$thisChkID = 'ApplyAllChk';
						$thisChkOnClick = "js_Select_All_Module_And_Subject(this.checked);";
						$x .= '<th class="apply_all_top" rowspan="2" style="text-align:center;">'."\n";
							$x .= $Lang['Btn']['ApplyToAll'];
						$x .= '</th>'."\n";
					$x .= '</tr>'."\n";
						
					$x .= '<tr>'."\n";
						### Module Title
						for ($i=0; $i<$numOfModule; $i++)
						{
							$thisModuleID = $ModuleInfoArr[$i]['ModuleID'];
							$thisModuleTitle = Get_Lang_Selection($ModuleInfoArr[$i]['ModuleNameCh'], $ModuleInfoArr[$i]['ModuleNameEn']);
							
							$x .= '<th style="text-align:center;">'."\n";
								$x .= $thisModuleTitle."\n";
							$x .= '</th>'."\n";
						}
					$x .= '</tr>'."\n";
					
					### Select All button
					$x .= '<tr class="edit_table_head_bulk">'."\n";
						$x .= '<th>&nbsp;</th>'."\n";
						$x .= '<th>&nbsp;</th>'."\n";

						for ($i=0; $i<$numOfModule; $i++)
						{
							$thisModuleID = $ModuleInfoArr[$i]['ModuleID'];
							$thisChkID = "ModuleAllChk_".$thisModuleID;
							$thisChkClass = "ModuleAllChk";
							$thisChkOnClick = "js_Select_All_Subject_Of_Module('$thisModuleID', this.checked);";
							
							$x .= '<th style="text-align:center;">'."\n";
								$x .= $this->Get_Checkbox($thisChkID, $thisChkName='', $Value=1, $thisChecked=0, $thisChkClass, $Display='', $thisChkOnClick)."\n";
							$x .= '</th>'."\n";
						}
						
						### Apply to all
						$thisChkID = 'ApplyAllChk';
						$thisChkOnClick = "js_Select_All_Module_And_Subject(this.checked);";
						$x .= '<th class="apply_all_top_bulk" rowspan="2" style="text-align:center;">'."\n";
							$x .= $this->Get_Checkbox($thisChkID, $thisChkName='', $Value=1, $thisChecked=0, $thisChkClass='', $Display='', $thisChkOnClick)."\n";
						$x .= '</th>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";
				
				$x .= '<tbody>'."\n";
					### Table Content
					if ($numOfLC == 0)
					{
						$x .= '<tr><td colspan="'.$NumOfColumn.'" align="center">'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoLearningCategory'].'</td></tr>'."\n";
					}
					else if ($numOfSubject == 0)
					{
						$x .= '<tr><td colspan="'.$NumOfColumn.'" align="center">'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoSubject'].'</td></tr>'."\n";
					}
					else if ($numOfModule == 0)
					{
						$x .= '<tr><td colspan="'.$NumOfColumn.'" align="center">'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoModuleInTerm'].'</td></tr>'."\n";
					}
					else
					{
						foreach((array)$LC_InfoArr as $thisLC_ID => $thisLCInfo)
						{
							$thisLCName = $thisLCInfo['Name'];
							$thisNumOfSubject = count($SubjectInfoArr[$thisLC_ID]);
							$rowspan = $thisNumOfSubject + 1;
							
							### Skip the Learning Category which has no Subject
							if ($thisNumOfSubject == 0)
								continue;
							
							
							$x .= '<tr><td rowspan="'.$rowspan.'">'.$thisLCName.'</td></tr>'."\n";
													
							foreach ((array)$SubjectInfoArr[$thisLC_ID] as $thisSubjectID => $thisSubjectInfoArr)
							{
								$thisSubjectName = Get_Lang_Selection($thisSubjectInfoArr['CH_DES'], $thisSubjectInfoArr['EN_DES']);
								$x .= '<tr>'."\n";
									$x .= '<td>'.$thisSubjectName.'</td>'."\n";
									
									for ($i=0; $i<$numOfModule; $i++)
									{
										$thisModuleID = $ModuleInfoArr[$i]['ModuleID'];
										
										$thisChkID = "ModuleSubjectChk_".$thisModuleID."_".$thisSubjectID;
										$thisChkName = "ModuleSubjectArr[".$thisModuleID."][".$thisSubjectID."]";
										$thisChkClass = "ModuleSubjectChk ModuleSubjectChk_ModuleID_$thisModuleID ModuleSubjectChk_SubjectID_$thisSubjectID";
										$thisChkOnClick = "js_Uncheck_Select_All('$thisModuleID', '$thisSubjectID', this.checked);";
										$thisChecked = (isset($ModuleSubjectInfoArr[$thisModuleID][$thisSubjectID]))? 1 : 0;
										
										$x .= '<td style="text-align:center;">'."\n";
											$x .= $this->Get_Checkbox($thisChkID, $thisChkName, $Value=1, $thisChecked, $thisChkClass, $Display='', $thisChkOnClick)."\n";
										$x .= '</td>'."\n";
									}
									
									### Apply All Button
									$thisChkID = "SubjectAllChk_".$thisSubjectID;
									$thisChkClass = "SubjectAllChk";
									$thisChkOnClick = "js_Select_All_Module_Of_Subject('$thisSubjectID', this.checked);";
									$x .= '<td style="text-align:center;">'."\n";
										$x .= $this->Get_Checkbox($thisChkID, $thisChkName='', $Value=1, $thisChecked=0, $thisChkClass, $Display='', $thisChkOnClick)."\n";
									$x .= '</td>'."\n";								
								$x .= '</tr>'."\n";
							}
						}
					}
						
				$x .= '<tbody>'."\n";
			$x .= '</table>'."\n";
			$x .= '<br style="clear:both" />'."\n";
			
			$x .= '<div class="edit_bottom_v30">'."\n";
				### Last Updated Info Div
				$LastModifiedInfoArr = $lreportcard->Get_Module_Subject_Last_Modified_Info();
				if (count($LastModifiedInfoArr) > 0)
				{
					$lastModifiedDate = $LastModifiedInfoArr[0]['DateModified'];
					$lastModifiedBy = $LastModifiedInfoArr[0]['LastModifiedBy'];
					$LastModifiedDisplay = Get_Last_Modified_Remark($lastModifiedDate, '', $lastModifiedBy);
					$x .= '<span>'.$LastModifiedDisplay.'</span>';
					$x .= '<p class="spacer"></p>';
				}
				### Save Buttom
				$x .= $btn_Save."\n";
			$x .= '</div>'."\n";
		$x .= '</form>'."\n";
		
		return $x;
	}
	
	function Get_Management_MarksheetRevision_UI()
	{
		global $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName, $lreportcard;
		global $eRC_Rubrics_ConfigArr;
		
		$MaxTopicLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
		
		$scm_ui = new subject_class_mapping_ui();
		
		### Term Selection
		$CurrentYearTermArr = getCurrentAcademicYearAndYearTerm();
		$CurrentYearTermID = $CurrentYearTermArr['YearTermID'];
		
		$OnChange = "js_Changed_Term_Selection(this.value);";
		$TermSelection = $scm_ui->Get_Term_Selection('YearTermID', $lreportcard->AcademicYearID, $CurrentYearTermID, $OnChange, $NoFirst=0, $NoPastTerm=0, $displayAll=1, $AllTitle='', $PastTermOnly=0, $CompareYearTermID='');
		
		### Subject Selection
		// $OnChange = "js_Changed_Subject_Selection(this.value);";
		// $SubjectSelection = $scm_ui->Get_Subject_Selection('SubjectID', $SubjectID='', $OnChange, $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0);
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post">'."\n";
    		
		    ### [2018-1002-1146-08277] added Import Button
    		$ImportBtn = $this->Get_Content_Tool_v30('import', "javascript:js_Go_Import()");
    		
    		$x .= '<div class="content_top_tool">'."\n";
        		$x .= '<div class="Conntent_tool">'."\n";
            		$x .= $ImportBtn."\n";
            		$x .= '<br style="clear: both" />'."\n";
        		$x .= '</div>'."\n";
    		$x .= '</div>'."\n";
    		$x .= '<br style="clear: both;">'."\n";
    		
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div style="float:left;">'.$TermSelection.'</div>'."\n";
						$x .= '<div style="float:left;">&nbsp;&nbsp;</div>'."\n";
						$x .= '<div style="float:left;" id="ModuleSelectionDiv"></div>'."\n";
				//		$x .= '<br style="clear:both;" />'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div style="float:left;" id="SubjectSelectionDiv">'.$SubjectSelection.'</div>'."\n";
						$x .= '<div style="float:left;">&nbsp;&nbsp;</div>'."\n";
						for ($thisLevel=1; $thisLevel<$MaxTopicLevel; $thisLevel++)
						{
							$thisDivID = "Level".$thisLevel."_SelectionDiv";
							$x .= '<div id="'.$thisDivID.'" class="LevelSelectionDiv" style="float:left;"></div>'."\n";
							$x .= '<div style="float:left;">&nbsp;&nbsp;</div>'."\n";
						}
					
					$x .= '</td>'."\n";
				$x .= '<td align="right">'.$SearchBox.'</td>'."\n";
				$x .= '</tr>'."\n";
				
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div style="float:left;" id="SubjectGroupSelectionDiv"></div>'."\n";
						$x .= '<div style="float:left;">&nbsp;&nbsp;</div>'."\n";
						$x .= '<div style="float:left;" id="StudentSelectionDiv"></div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				$x .= '<tr><td colspan="2"><div id="StudentMarksheetDiv"></div></td></tr>'."\n";
			$x .= '</table>'."\n";
			
			### Topic Remarks Layer
			$x .= '<div id="TopicRemarksDiv" class="selectbox_layer" style="visibility:hidden; width:350px; z-index:999;"></div>'."\n";
			
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";
		return $x;
	}
	
	function Get_Management_MarksheetRevision_Table($YearTermID, $SubjectID, $StudentID, $ModuleID='',$FilterTopicInfoText='')
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName, $image_path, $LAYOUT_SKIN, $RowIndex, $eRC_Rubrics_ConfigArr, $lreportcard;
		global $linterface;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
		include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

		$lreportcard_topic = new libreportcardrubrics_topic();
		$lreportcard_module = new libreportcardrubrics_module();
		$lreportcard_rubrics = new libreportcardrubrics_rubrics();
		
		$FilterTopicID = '';
		$FilterObjectiveID = '';		

		$FilterTopicInfoTextArr = explode(':_:',$FilterTopicInfoText);
		$numOfTopicFilter = count($FilterTopicInfoTextArr);
		for ($i=0; $i<$numOfTopicFilter; $i++) {
			$FilterTopicInfoTextArr[$i] = ($FilterTopicInfoTextArr[$i]=='ALL')? '' : $FilterTopicInfoTextArr[$i];
		}

		### Get Subject Topics Info
		$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectID);	
//		$TopicTreeArr = $lreportcard_topic->Get_Topic_Tree($SubjectID,$FilterTopicID);	
		$TopicTreeArr = $lreportcard_topic->Get_Student_Study_Topic_Tree($StudentID, $SubjectID, $ModuleID);
		
		
		$noThisTopicArr = array();		
		if(count($TopicTreeArr)==0)
		{
			$noThisTopicArr['noSubject']=true;
		}
		else
		{
			$_topicTree = $TopicTreeArr;
			$thisTopicTreeArr = $TopicTreeArr;
			for ($i=0; $i<$numOfTopicFilter; $i++) {
				$thisTopicLevel = $i + 1;		// array index start from 0 but level index start from 1
				$thisTopicID = $FilterTopicInfoTextArr[$i];
				
				$_topicTree = $_topicTree[$thisTopicID];
			
				$noThisTopicArr['noTopic_level'.$thisTopicLevel]=false;

				if($_topicTree==null)
				{
					$noThisTopicArr['noTopic_level'.$thisTopicLevel]=true;				
				}
				else
				{
					$thisTopicTreeArr = $_topicTree;
						
					$j_MAX = $i;
					for ($j=$j_MAX; $j>=0; $j--) {
						$_topicID = $FilterTopicInfoTextArr[$j];
						$thisTopicTreeArr = array($_topicID=>$thisTopicTreeArr);
					}
					$TopicTreeArr= $thisTopicTreeArr;															
				}
				
				if($thisTopicID=='')   // if it is 'ALL'
				{
					$noThisTopicArr['noTopic_level'.$thisTopicLevel]=false;	
				}
			}
		}


		### Get Module Info
		$ModuleInfoArr = $lreportcard_module->Get_Module_Info($YearTermID, $ModuleID);
		$numOfModule = count($ModuleInfoArr);
		
		### Get Term Info
		$ObjAcademicYear = new academic_year($lreportcard_topic->AcademicYearID);
		$TermInfoAssoArr = $ObjAcademicYear->Get_Term_List();
		$TermModuleCountArr = array();		// $TermModuleCountArr[$YearTermID] = num of Module in the Term
		for ($i=0; $i<$numOfModule; $i++)
		{
			$thisYearTermID = $ModuleInfoArr[$i]['YearTermID'];
			$TermModuleCountArr[$thisYearTermID]++;
		}
		
		### Get Student Study Topic Info
		// $StudentStudyTopicInfoArr[$StudentID][$ModuleID][$SubjectID][$TopicID] = InfoArr
		$StudentStudyTopicInfoArr = $lreportcard_topic->Get_Student_Study_Topic_Info($ModuleID='', $SubjectID, $TopicID='', $StudentID);
		
		### Get Subject Rubrics Info
		$StudentClassInfoArr = $lreportcard_topic->Get_Student_Class_Info($StudentID);
		$YearID = $StudentClassInfoArr[0]['YearID'];
		
		// $SubjectRubricsInfoArr[$YearID][$SubjectID][$RubricsID][$RubricsItemID] = RubricsItemInfoArr
		$SubjectRubricsInfoArr = $lreportcard_topic->Get_Subject_Rubrics_Mapping($SubjectID, $YearID);
		if (is_array($SubjectRubricsInfoArr[$YearID]))
			$tmpArrKey = array_keys((array)$SubjectRubricsInfoArr[$YearID][$SubjectID]);
		else
			$tmpArrKey = array_keys((array)$SubjectRubricsInfoArr[0][$SubjectID]);
		
		$RubricsID = $tmpArrKey[0];
		if ($RubricsID != '')
			$RubricsInfoArr = $lreportcard_rubrics->Get_Rubics_Info($RubricsID);
		
		### Get Subject Rubrics Item Info
		$SubjectRubricsItemInfoArr = $SubjectRubricsInfoArr[$YearID][$SubjectID][$RubricsID];
		
		### Get Student Subject Score
		// $StudentSubjectMarksheetScoreArr[$StudentID][$SubjectID][$TopicID][$ModuleID] = InfoArr
		$StudentSubjectMarksheetScoreArr = $lreportcard_topic->Get_Student_Marksheet_Score($SubjectID, $StudentID);
		
		### Get Column Width
		$numOfLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		$ModuleColumnWidth = 8;
		$TotalModuleColumnWidth = $ModuleColumnWidth * $numOfModule;
		$Level1Width = 8;
		$widthOfTopicColumn = floor((100 - $TotalModuleColumnWidth - $Level1Width) / $numOfLevel);
		
		$NumOfColumn = $numOfLevel + $numOfModule;
		
		### Save Button
		$btn_Save = $this->GET_ACTION_BTN($Lang['Btn']['Save'], "button", $onclick="js_Save_Marksheet();", $id="Btn_Save");
		
		$x = '';
		
		### Subject Rubrics Info
		if ($RubricsID != '')
		{
			$x .= '<table width="95%" class="tabletext" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['SettingsArr']['SubjectRubricsArr']['MenuTitle'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= Get_Lang_Selection($RubricsInfoArr[0]['RubricsCh'], $RubricsInfoArr[0]['RubricsEn']);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<br style="clear:both;" />'."\n";
		}
		
		### Marksheet Table
		$x .= '<table id="ContentTable" class="common_table_list_v30 edit_table_list_v30" width="96%">'."\n";
		
			### Levels
			for ($i=0; $i<$numOfLevel; $i++)
			{
				//$thisWidth = ($i==0)? $Level1Width : $widthOfTopicColumn;
				//$thisWidth = 10;
				$thisWidth = 5;
				$x .= '<col align="left" style="width: '.$thisWidth.'%;">'."\n";
			}
			### Modules
			for ($i=0; $i<$numOfModule; $i++)
			{
				$x .= '<col align="left" style="width: '.$ModuleColumnWidth.'%;">'."\n";
			}
			
			### Level Title
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					### Level Title
					for ($thisLevel=1; $thisLevel<=$numOfLevel; $thisLevel++)
					{
						$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$thisLevel]['Title'];
						
						$x .= '<th rowspan="2">'."\n";
							$x .= '<span style="float:left;" class="row_content_v30">'.$thisTitle.'</span>'."\n";
						$x .= '</th>'."\n";
					}
					
					### Term Title
					foreach ((array)$TermModuleCountArr as $thisYearTermID => $thisNumOfModuleInTerm)
					{
						$thisTermName = ($thisYearTermID == 0)? $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['WholeYear'] : $TermInfoAssoArr[$thisYearTermID];
						$x .= '<th colspan="'.$thisNumOfModuleInTerm.'" style="text-align:center;">'."\n";
							$x .= '<span>'.$thisTermName.'</span>'."\n";
						$x .= '</th>'."\n";
					}
				$x .= '</tr>'."\n";
				
				$x .= '<tr>'."\n";
					### Module Title
					for ($i=0; $i<$numOfModule; $i++)
					{
						$thisModuleID = $ModuleInfoArr[$i]['ModuleID'];
						$thisModuleTitle = Get_Lang_Selection($ModuleInfoArr[$i]['ModuleNameCh'], $ModuleInfoArr[$i]['ModuleNameEn']);
						$x .= '<th style="text-align:center;" nowrap>'."\n";
							$x .= $thisModuleTitle."\n";
						$x .= '</th>'."\n";
					}
				$x .= '</tr>'."\n";
				
				### Apply All Row
				$x .= '<tr class="edit_table_head_bulk">'."\n";
					# Level
					for ($thisLevel=1; $thisLevel<=$numOfLevel; $thisLevel++)
					{
						$x .= '<th>&nbsp;</th>'."\n";
					}
					
					# Module
					for ($i=0; $i<$numOfModule; $i++)
					{
						$thisModuleID = $ModuleInfoArr[$i]['ModuleID'];
						$thisModuleMarksheetSubmissionStartDate = $ModuleInfoArr[$i]['MarksheetSubmissionStartDate'];
						$thisModuleMarksheetSubmissionEndDate = $ModuleInfoArr[$i]['MarksheetSubmissionEndDate'];
						
						$thisSelectionID = "GlobalModule_".$thisModuleID;
						$thisSelectionClass = "GlobalModule_".$thisModuleID;
						$thisOnChange = "js_Update_GlobalModule_Selection(this.id, this.value);";	// for fixed header js addon
						$thisDisabled = ($lreportcard->Check_Within_DateRange($thisModuleMarksheetSubmissionStartDate, $thisModuleMarksheetSubmissionEndDate))? 0 : 1;
						$thisIconHref = "javascript:js_Apply_Mark_To_All_Topic('$thisModuleID');";
						
						$x .= '<th style="text-align:center;" nowrap>'."\n";
							$x .= $this->Get_Rubrics_Level_Selection($thisSelectionID, $thisChkName, $SelectedLevel='', $thisOnChange, $thisSelectionClass, $SubjectRubricsItemInfoArr, '', '', $thisDisabled);
							$x .= '<br />'."\n";
							$x .= '<div style="width:45%;float:left">&nbsp;</div>'."\n";	// This div is to make the "Apply All" icon to be placed in the center
							$x .= $this->Get_Apply_All_Icon($thisIconHref, $Lang['Btn']['ApplyToAll']);
						$x .= '</th>'."\n";
					}
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			$x .= '<tbody>'."\n";
				
				### Topic Code and Name Display
				$DisplayButton = false;
				$RowIndex = 0;
				if (count($TopicInfoArr) == 0)
				{
					$x .= '<tr><td colspan="'.$NumOfColumn.'" align="center">'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoTopicForSubject'].'</td></tr>'."\n";
				}
				else if ($numOfModule == 0)
				{
					$x .= '<tr><td colspan="'.$NumOfColumn.'" align="center">'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoModuleInTerm'].'</td></tr>'."\n";
				}
				else if ($RubricsID == '')
				{
					$x .= '<tr><td colspan="'.$NumOfColumn.'" align="center">'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoRubricsForSubject'].'</td></tr>'."\n";
				}
				else if(in_array(true,$noThisTopicArr))
				{
					$x .= '<tr><td colspan="'.$NumOfColumn.'" align="center">'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoThisTopicForSubject'].'</td></tr>'."\n";
				}
				else
				{
					$x .= $this->Get_Management_MarksheetRevision_Table_Row($TopicInfoArr, $TopicTreeArr, $ModuleInfoArr, $StudentStudyTopicInfoArr, $SubjectRubricsItemInfoArr, $StudentSubjectMarksheetScoreArr, $SubjectID, $StudentID);

					$DisplayButton = true;
					$NumOfRow = $RowIndex - 1;
				}
					
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		$x .= '<br style="clear:both" />'."\n";
		
		if (count($TopicInfoArr) > 0)
		{
			$x .= '<div class="edit_bottom_v30">';
				# Last Updated Info Div
				if (count($StudentSubjectMarksheetScoreArr[$StudentID][$SubjectID]) > 0)
				{
					foreach($StudentSubjectMarksheetScoreArr[$StudentID][$SubjectID] as $thisTopicID => $thisTopicMarksheetScoreArr)
					{
						foreach ($thisTopicMarksheetScoreArr as $thisModule => $thisModuleTopicMarksheetScoreArr)
						{
							$DateModified = $thisModuleTopicMarksheetScoreArr['DateModified'];
							$LastModifiedBy = $thisModuleTopicMarksheetScoreArr['LastModifiedBy'];
							break;
						}
						break;
					}
					
					$LastModifiedDisplay = Get_Last_Modified_Remark($DateModified, '', $LastModifiedBy);
					$x .= '<span>'.$LastModifiedDisplay.'</span>';
					$x .= '<p class="spacer"></p>';
				}
				
				### Save Buttom
				if ($DisplayButton)
					$x .= $btn_Save."\n";
			$x .= '</div>';
		}
		
		$x .='<div id="DescDivID" class="selectbox_layer"  style="visibility:hidden;z-index:500; width:300px; height:200px;">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">		
					<tr><td>'.$this->GET_NAVIGATION2_IP25($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['SubjectDetailsComment']) .'</td></tr>	
					<tr>
						<td>
							<div id="DescDivContent"></div>
						</td>
					</tr>
				</table>
				</div>';
		
		### Textarea for copy and paste
		$x .= '<textarea style="height: 1px; width: 1px; visibility:hidden;" id="text1" name="text1"></textarea>'."\n";
		
		### Javascript for copy and paste
		$x .= '	<script language="javascript">
					var rowx = 0, coly = 0;			//init
					var xno = '.$numOfModule.'; yno = '.$RowIndex.';		// set table size
					var startContent = "";
					var setStart = 1;
				</script>
				';
				
		### Hidden field for mark saving
		$x .= '<input type="hidden" id="RubricsID" name="RubricsID" value="'.$RubricsID.'" />'."\n";
		
		
			
		return $x;
	}
	
	function Get_Management_MarksheetRevision_Table_Row($TopicInfoArr, $TopicTreeArr, $ModuleInfoArr, $StudentStudyTopicInfoArr, $SubjectRubricsItemInfoArr, $StudentSubjectMarksheetScoreArr, $SubjectID, $StudentID)
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName, $RowIndex, $eRC_Rubrics_ConfigArr, $lreportcard;
		
		$MaxLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		$numOfModule = count($ModuleInfoArr);

		$x = '';
		foreach ((array)$TopicTreeArr as $thisTopicID => $thisLinkedTopicIDArr)
		{
			$thisLevel					= $TopicInfoArr[$thisTopicID]['Level'];
			$thisCode					= $TopicInfoArr[$thisTopicID]['TopicCode'];
			$thisParentTopicID 			= $TopicInfoArr[$thisTopicID]['ParentTopicID'];
			$thisNameEn 				= $TopicInfoArr[$thisTopicID]['TopicNameEn'];
			$thisNameCh 				= $TopicInfoArr[$thisTopicID]['TopicNameCh'];
			$thisName 					= Get_Lang_Selection($thisNameCh, $thisNameEn);
			
//debug_r($thisName);		
			if ($thisLevel > $MaxLevel)
				continue; 
				
			$x .= '<tr>'."\n";
			
				### Empty Cell
				for ($i=1; $i<$thisLevel; $i++)
				{
					$x .= '<td>&nbsp;</td>'."\n";
				}
				
				# Add onmouseover event for the Cell if it is the 1st Level to show the Remarks 
				$thisID = '';
				$thisIdTag = '';
				$thisTag = '';
				if ($thisLevel == 1)
				{
					$thisID = 'TopicID_'.$thisTopicID;
					$thisIdTag = ' id="'.$thisID.'" ';
					$thisTag = ' onmouseover="js_Show_Topic_Remarks_Layer('.$thisTopicID.', \''.$thisID.'\', \'TopicRemarksDiv\');" onmouseout="js_Hide_Topic_Remarks_Layer(\'TopicRemarksDiv\');" ';
				}
				
				$x .= '<td>'."\n";
					$x .= '<div '.$thisIdTag.' '.$thisTag.' style="width:100%; white-space:normal;">'."\n";
						$x .= $thisName."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
				
				for ($i=$thisLevel; $i<$MaxLevel; $i++)
				{
					$x .= '<td>&nbsp;</td>'."\n";
				}

			if (count($thisLinkedTopicIDArr) > 0)
			{
					for ($i=0; $i<$numOfModule; $i++)
					{
						$x .= '<td>&nbsp;</td>'."\n";
					}
				$x .= '</tr>'."\n";
				$x .= $this->Get_Management_MarksheetRevision_Table_Row($TopicInfoArr, $thisLinkedTopicIDArr, $ModuleInfoArr, $StudentStudyTopicInfoArr, $SubjectRubricsItemInfoArr, $StudentSubjectMarksheetScoreArr, $SubjectID, $StudentID, $RowIndex);
			}
			else
			{
				if ($thisLevel == $MaxLevel)
				{
					### Last Level of Topic
					for ($i=0; $i<$numOfModule; $i++)
					{
						$thisModuleID = $ModuleInfoArr[$i]['ModuleID'];
						$thisModuleMarksheetSubmissionStartDate = $ModuleInfoArr[$i]['MarksheetSubmissionStartDate'];
						$thisModuleMarksheetSubmissionEndDate = $ModuleInfoArr[$i]['MarksheetSubmissionEndDate'];
						
						$thisStudy = isset($StudentStudyTopicInfoArr[$StudentID][$thisModuleID][$SubjectID][$thisTopicID]);
						$thisRubricsItemID = $StudentSubjectMarksheetScoreArr[$StudentID][$SubjectID][$thisTopicID][$thisModuleID]['RubricsItemID'];
						$thisRemark = $StudentSubjectMarksheetScoreArr[$StudentID][$SubjectID][$thisTopicID][$thisModuleID]['Remarks'];
						$thisRemark = intranet_htmlspecialchars($thisRemark);
						$RemarkMixLength = $eRC_Rubrics_ConfigArr['MaxLength']['Marksheet']['Remarks'];
						# grey background for not checked cell
						if ($thisStudy)
						{
							$thisTdClass 		= '';
							$thisChkID 			= "mark[$RowIndex][$i]";
							$thisChkName 		= "SubjectLevelArr[$thisModuleID][$thisTopicID]";
							$thisChkClass 		= "ChkModuleID_$thisModuleID ChkTopicID_$thisTopicID";
							$thisOtherParArr 	= array(	'onpaste'=>'pasteContent(\''.$RowIndex.'\', \''.$i.'\');',
									    					'onkeyup'=>'isPasteContent(event, \''.$RowIndex.'\', \''.$i.'\');',
									    					'onkeypress'=>'isPasteContent(event, \''.$RowIndex.'\', \''.$i.'\');',
									    					'onkeydown'=>'isPasteContent(event, \''.$RowIndex.'\', \''.$i.'\');'
									    				);
							$thisDisabled = ($lreportcard->Check_Within_DateRange($thisModuleMarksheetSubmissionStartDate, $thisModuleMarksheetSubmissionEndDate))? 0 : 1;
							$thisDisabledHtml = $thisDisabled?" disabled ":"";
							
							$thisContent =$this->Get_Rubrics_Level_Selection($thisChkID, $thisChkName, $thisRubricsItemID, $OnChange='', $thisChkClass, $SubjectRubricsItemInfoArr, '', $thisOtherParArr, $thisDisabled);
							$thisContent .='<br/><input id="SubjectLevelRemarkID_'.$thisModuleID.'_'.$thisTopicID.'" class="SubjectLevelRemarkID_'.$thisModuleID.'_'.$thisTopicID.'" type="text" style="margin:1px;" name="SubjectLevelRemarkArr['.$thisModuleID.']['.$thisTopicID.']" value="'.$thisRemark.'" maxlength="'.$RemarkMixLength.'" '.$thisDisabledHtml.'/>';
							
							if($thisDisabledHtml=='') {
								$thisCommentBankLinkId = "commentIconDivID_".$thisModuleID."_".$thisTopicID;
								$thisContent .='<a id="'.$thisCommentBankLinkId.'" class="'.$thisCommentBankLinkId.'" href="javascript:void(0);" onclick="showShortCutPanel(\''.$thisCommentBankLinkId.'\',\''.$thisModuleID.'\',\''.$thisTopicID.'\');"><img width="20" border="0" height="20" title="'.$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ChooseFromCommentBank'].'" src="/images/2009a/icon_remark.gif" /></a>';
							}
						}
						else {
							$thisTdClass = 'class="not_check_cell"';
							$thisContent = '&nbsp;';
						}
							
						$x .= '<td '.$thisTdClass.' style="text-align:center;">'."\n";
							$x .= $thisContent."\n";
						$x .= '</td>'."\n";
					}
					
					// For copy and paste - selection box indexing
					$RowIndex++;
				}
				else
				{
					### Some Topics which do not have the last level => cannot set to study
					for ($i=0; $i<$numOfModule; $i++)
					{
						$x .= '<td>&nbsp;</td>'."\n";
					}
				}
				$x .= '</tr>'."\n";
			}
		}
		return $x;
	}
	
	function Get_CommentTable_for_Marksheet($ParModuleID, $ParTopicID)
	{
		$lreportcard_comment = new libreportcardrubrics_comment();
		$resultArray = $lreportcard_comment->Get_Comment_Bank_Comment($keyword='', $ParTopicID);
		
		$html='';
		$html.= '<table style="width:100%;">';
		
			if(count($resultArray)>0)
			{
				for($i=0,$i_MAX=count($resultArray);$i<$i_MAX;$i++)	{
					$thisCommentID = $resultArray[$i]['CommentID'];
					//$thisComment =intranet_htmlspecialchars ($resultArray[$i]['Comment']);
					$thisComment = Get_Lang_Selection($resultArray[$i]['CommentCh'], $resultArray[$i]['CommentEn']);
									
					$onclick = 'js_InsertCommentToTextBox(\''.$ParModuleID.'\',\''.$ParTopicID.'\',\''.$thisCommentID.'\')';
					$html .='<tr>
								<td style="border-bottom:1px #888888  solid;">
									<a class="tablelink" href="javascript:void(0);" onclick ="'.$onclick.';">'.$thisComment.'</a>
								</td>
							</tr>';
				}		
			}
			else
			{
				$html .='<tr>
							<td style="border_bottom:1px #FFFFFF solid;">--</td>
						</tr>';
			}
		$html.='</table>';

		return $html;
	}
	
	function Get_Management_MarksheetRevision_SubjectGroupView_UI()
	{
		global $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName, $lreportcard, $Lang;
		
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
		
		$scm_ui = new subject_class_mapping_ui();
		
		### View Mode Selection
		$ViewModeArr = array();
		$ViewModeArr[] = array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'], 'javascript:void();', $Img='', 1);
		$ViewModeArr[] = array($Lang['Identity']['Student'], 'javascript:void();', $Img='', 0);
		$ViewModeTab = $this->GET_CONTENT_TOP_BTN($ViewModeArr);
		
		### Term Selection
		$CurrentYearTermArr = getCurrentAcademicYearAndYearTerm();
		$CurrentYearTermID = $CurrentYearTermArr['YearTermID'];
		
		$OnChange = "js_Changed_Term_Selection(this.value);";
		$TermSelection = $scm_ui->Get_Term_Selection('YearTermID', $lreportcard->AcademicYearID, $CurrentYearTermID, $OnChange, $NoFirst=0, $NoPastTerm=0, $displayAll=0, $AllTitle='', $PastTermOnly=0, $CompareYearTermID='');
		
		### Subject Selection
		//$OnChange = "js_Changed_Subject_Selection(this.value);";
		//$SubjectSelection = $scm_ui->Get_Subject_Selection('SubjectID', $SubjectID='', $OnChange, $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0);
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= '<div class="content_top_tool">'."\n";
				$x .= $ViewModeTab;
			$x .= '</div>';
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div style="float:left;">'.$TermSelection.'</div>'."\n";
						$x .= '<div style="float:left;">&nbsp;&nbsp;</div>'."\n";
						$x .= '<div style="float:left;" id="ModuleSelectionDiv"></div>'."\n";
						$x .= '<br style="clear:both;" />'."\n";
						$x .= '<div style="float:left;" id="SubjectSelectionDiv">'.$SubjectSelection.'</div>'."\n";
						$x .= '<div style="float:left;">&nbsp;&nbsp;</div>'."\n";
						$x .= '<div style="float:left;" id="SubjectGroupSelectionDiv"></div>'."\n";
						$x .= '<div style="float:left;">&nbsp;&nbsp;</div>'."\n";
						$x .= '<div style="float:left;" id="StudentSelectionDiv"></div>'."\n";
					$x .= '</td>'."\n";
					$x .= '<td align="right">'.$SearchBox.'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr><td colspan="2"><div id="StudentMarksheetDiv"></div></td></tr>'."\n";
			$x .= '</table>'."\n";
			
			### Topic Remarks Layer
			$x .= '<div id="TopicRemarksDiv" class="selectbox_layer" style="visibility:hidden; width:350px; z-index:999;"></div>'."\n";
				
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";
		return $x;
	}
	
	function Get_Management_MarksheetRevision_SubjectGroupView_Table($YearTermID, $SubjectID, $SubjectGroupID, $ModuleID='')
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName, $image_path, $LAYOUT_SKIN, $RowIndex, $eRC_Rubrics_ConfigArr, $lreportcard;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
		include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

		$lreportcard_topic = new libreportcardrubrics_topic();
		$lreportcard_module = new libreportcardrubrics_module();
		$lreportcard_rubrics = new libreportcardrubrics_rubrics();
		
		### Get Subject Topics Info
		$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectID);
		$TopicTreeArr = $lreportcard_topic->Get_Topic_Tree($SubjectID);
		
		### Get Module Info
		$ModuleInfoArr = $lreportcard_module->Get_Module_Info($YearTermID, $ModuleID);
		$numOfModule = count($ModuleInfoArr);
		
		### Get Term Info
		$ObjAcademicYear = new academic_year($lreportcard_topic->AcademicYearID);
		$TermInfoAssoArr = $ObjAcademicYear->Get_Term_List();
		$TermModuleCountArr = array();		// $TermModuleCountArr[$YearTermID] = num of Module in the Term
		for ($i=0; $i<$numOfModule; $i++)
		{
			$thisYearTermID = $ModuleInfoArr[$i]['YearTermID'];
			$TermModuleCountArr[$thisYearTermID]++;
		}
		
		### Get Student Study Topic Info
		// $StudentStudyTopicInfoArr[$StudentID][$ModuleID][$SubjectID][$TopicID] = InfoArr
		$StudentStudyTopicInfoArr = $lreportcard_topic->Get_Student_Study_Topic_Info($ModuleID='', $SubjectID, $TopicID='', $StudentID);
		
		### Get Subject Rubrics Info
		$StudentClassInfoArr = $lreportcard_topic->Get_Student_Class_Info($StudentID);
		$YearID = $StudentClassInfoArr[0]['YearID'];
		
		// $SubjectRubricsInfoArr[$YearID][$SubjectID][$RubricsID][$RubricsItemID] = RubricsItemInfoArr
		$SubjectRubricsInfoArr = $lreportcard_topic->Get_Subject_Rubrics_Mapping($SubjectID, $YearID);
		if (is_array($SubjectRubricsInfoArr[$YearID]))
			$tmpArrKey = array_keys((array)$SubjectRubricsInfoArr[$YearID][$SubjectID]);
		else
			$tmpArrKey = array_keys((array)$SubjectRubricsInfoArr[0][$SubjectID]);
		
		$RubricsID = $tmpArrKey[0];
		if ($RubricsID != '')
			$RubricsInfoArr = $lreportcard_rubrics->Get_Rubics_Info($RubricsID);
		
		### Get Subject Rubrics Item Info
		$SubjectRubricsItemInfoArr = $SubjectRubricsInfoArr[$YearID][$SubjectID][$RubricsID];
		
		### Get Student Subject Score
		// $StudentSubjectMarksheetScoreArr[$StudentID][$SubjectID][$TopicID][$ModuleID] = InfoArr
		$StudentSubjectMarksheetScoreArr = $lreportcard_topic->Get_Student_Marksheet_Score($SubjectID, $StudentID);
		
		### Get Column Width
		$numOfLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		$ModuleColumnWidth = 8;
		$TotalModuleColumnWidth = $ModuleColumnWidth * $numOfModule;
		$Level1Width = 8;
		$widthOfTopicColumn = floor((100 - $TotalModuleColumnWidth - $Level1Width) / $numOfLevel);
		
		$NumOfColumn = $numOfLevel + $numOfModule;
		
		### Save Button
		$btn_Save = $this->GET_ACTION_BTN($Lang['Btn']['Save'], "button", $onclick="js_Save_Marksheet();", $id="Btn_Save");
				
		$x = '';
		
		### Subject Rubrics Info
		if ($RubricsID != '')
		{
			$x .= '<table width="95%" class="tabletext" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['SettingsArr']['SubjectRubricsArr']['MenuTitle'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= Get_Lang_Selection($RubricsInfoArr[0]['RubricsCh'], $RubricsInfoArr[0]['RubricsEn']);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<br style="clear:both;" />'."\n";
		}
		
		### Marksheet Table
		$x .= '<table id="ContentTable" class="common_table_list_v30 edit_table_list_v30" width="96%">'."\n";
			### Levels
			for ($i=0; $i<$numOfLevel; $i++)
			{
				//$thisWidth = ($i==0)? $Level1Width : $widthOfTopicColumn;
				$thisWidth = 10;
				$x .= '<col align="left" style="width: '.$thisWidth.'%;">'."\n";
			}
			### Modules
			for ($i=0; $i<$numOfModule; $i++)
			{
				$x .= '<col align="left" style="width: '.$ModuleColumnWidth.'%;">'."\n";
			}
			
			### Level Title
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					### Level Title
					for ($thisLevel=1; $thisLevel<=$numOfLevel; $thisLevel++)
					{
						$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$thisLevel]['Title'];
						
						$x .= '<th rowspan="2">'."\n";
							$x .= '<span style="float:left;" class="row_content_v30">'.$thisTitle.'</span>'."\n";
						$x .= '</th>'."\n";
					}
					
					### Term Title
					foreach ((array)$TermModuleCountArr as $thisYearTermID => $thisNumOfModuleInTerm)
					{
						$thisTermName = ($thisYearTermID == 0)? $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['WholeYear'] : $TermInfoAssoArr[$thisYearTermID];
						$x .= '<th colspan="'.$thisNumOfModuleInTerm.'" style="text-align:center;">'."\n";
							$x .= '<span>'.$thisTermName.'</span>'."\n";
						$x .= '</th>'."\n";
					}
				$x .= '</tr>'."\n";
				
				$x .= '<tr>'."\n";
					### Module Title
					for ($i=0; $i<$numOfModule; $i++)
					{
						$thisModuleID = $ModuleInfoArr[$i]['ModuleID'];
						$thisModuleTitle = Get_Lang_Selection($ModuleInfoArr[$i]['ModuleNameCh'], $ModuleInfoArr[$i]['ModuleNameEn']);
						$x .= '<th style="text-align:center;" nowrap>'."\n";
							$x .= $thisModuleTitle."\n";
						$x .= '</th>'."\n";
					}
				$x .= '</tr>'."\n";
				
				### Apply All Row
				$x .= '<tr class="edit_table_head_bulk">'."\n";
					# Level
					for ($thisLevel=1; $thisLevel<=$numOfLevel; $thisLevel++)
					{
						$x .= '<th>&nbsp;</th>'."\n";
					}
					
					# Module
					for ($i=0; $i<$numOfModule; $i++)
					{
						$thisModuleID = $ModuleInfoArr[$i]['ModuleID'];
						$thisModuleMarksheetSubmissionStartDate = $ModuleInfoArr[$i]['MarksheetSubmissionStartDate'];
						$thisModuleMarksheetSubmissionEndDate = $ModuleInfoArr[$i]['MarksheetSubmissionEndDate'];
						
						$thisSelectionID = "GlobalModule_".$thisModuleID;
						$thisSelectionClass = "GlobalModule_".$thisModuleID;
						$thisOnChange = "js_Update_GlobalModule_Selection(this.id, this.value);";	// for fixed header js addon
						$thisDisabled = ($lreportcard->Check_Within_DateRange($thisModuleMarksheetSubmissionStartDate, $thisModuleMarksheetSubmissionEndDate))? 0 : 1;
						$thisIconHref = "javascript:js_Apply_Mark_To_All_Topic('$thisModuleID');";
						
						$x .= '<th style="text-align:center;" nowrap>'."\n";
							$x .= $this->Get_Rubrics_Level_Selection($thisSelectionID, $thisChkName, $SelectedLevel='', $thisOnChange, $thisSelectionClass, $SubjectRubricsItemInfoArr, '', '', $thisDisabled);
							$x .= '<br />'."\n";
							$x .= '<div style="width:45%;float:left">&nbsp;</div>'."\n";	// This div is to make the "Apply All" icon to be placed in the center
							$x .= $this->Get_Apply_All_Icon($thisIconHref, $Lang['Btn']['ApplyToAll']);
						$x .= '</th>'."\n";
					}
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			$x .= '<tbody>'."\n";
				
				### Topic Code and Name Display
				$DisplayButton = false;
				$RowIndex = 0;
				if (count($TopicInfoArr) == 0)
				{
					$x .= '<tr><td colspan="'.$NumOfColumn.'" align="center">'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoTopicForSubject'].'</td></tr>'."\n";
				}
				else if ($numOfModule == 0)
				{
					$x .= '<tr><td colspan="'.$NumOfColumn.'" align="center">'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoModuleInTerm'].'</td></tr>'."\n";
				}
				else if ($RubricsID == '')
				{
					$x .= '<tr><td colspan="'.$NumOfColumn.'" align="center">'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoRubricsForSubject'].'</td></tr>'."\n";
				}
				else
				{
					$x .= $this->Get_Management_MarksheetRevision_SubjectGroupView_Table_Row($TopicInfoArr, $TopicTreeArr, $ModuleInfoArr, $StudentStudyTopicInfoArr, $SubjectRubricsItemInfoArr, $StudentSubjectMarksheetScoreArr, $SubjectID, $StudentID);
					$DisplayButton = true;
					$NumOfRow = $RowIndex - 1;
				}
					
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		$x .= '<br style="clear:both" />'."\n";
		
		if (count($TopicInfoArr) > 0)
		{
			$x .= '<div class="edit_bottom_v30">';
				# Last Updated Info Div
				if (count($StudentSubjectMarksheetScoreArr[$StudentID][$SubjectID]) > 0)
				{
					foreach($StudentSubjectMarksheetScoreArr[$StudentID][$SubjectID] as $thisTopicID => $thisTopicMarksheetScoreArr)
					{
						foreach ($thisTopicMarksheetScoreArr as $thisModule => $thisModuleTopicMarksheetScoreArr)
						{
							$DateModified = $thisModuleTopicMarksheetScoreArr['DateModified'];
							$LastModifiedBy = $thisModuleTopicMarksheetScoreArr['LastModifiedBy'];
							break;
						}
						break;
					}
					
					$LastModifiedDisplay = Get_Last_Modified_Remark($DateModified, '', $LastModifiedBy);
					$x .= '<span>'.$LastModifiedDisplay.'</span>';
					$x .= '<p class="spacer"></p>';
				}
				
				### Save Buttom
				if ($DisplayButton)
					$x .= $btn_Save."\n";
			$x .= '</div>';
		}
		
		### Textarea for copy and paste
		$x .= '<textarea style="height: 1px; width: 1px; visibility:hidden;" id="text1" name="text1"></textarea>'."\n";
		
		### Javascript for copy and paste
		$x .= '	<script language="javascript">
					var rowx = 0, coly = 0;			//init
					var xno = '.$numOfModule.'; yno = '.$RowIndex.';		// set table size
					var startContent = "";
					var setStart = 1;
				</script>
				';
				
		### Hidden field for mark saving
		$x .= '<input type="hidden" id="RubricsID" name="RubricsID" value="'.$RubricsID.'" />'."\n";
		
		return $x;
	}
	
	function Get_Management_MarksheetRevision_SubjectGroupView_Table_Row($TopicInfoArr, $TopicTreeArr, $ModuleInfoArr, $StudentStudyTopicInfoArr, $SubjectRubricsItemInfoArr, $StudentSubjectMarksheetScoreArr, $SubjectID, $StudentID)
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName, $RowIndex, $eRC_Rubrics_ConfigArr, $lreportcard;
		
		$MaxLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		$numOfModule = count($ModuleInfoArr);
		
		$x = '';
		foreach ($TopicTreeArr as $thisTopicID => $thisLinkedTopicIDArr)
		{
			$thisLevel					= $TopicInfoArr[$thisTopicID]['Level'];
			$thisCode					= $TopicInfoArr[$thisTopicID]['TopicCode'];
			$thisParentTopicID 			= $TopicInfoArr[$thisTopicID]['ParentTopicID'];
			$thisNameEn 				= $TopicInfoArr[$thisTopicID]['TopicNameEn'];
			$thisNameCh 				= $TopicInfoArr[$thisTopicID]['TopicNameCh'];
			$thisName 					= Get_Lang_Selection($thisNameCh, $thisNameEn);
			
			if ($thisLevel > $MaxLevel)
				continue; 
				
			$x .= '<tr>'."\n";
			
				### Empty Cell
				for ($i=1; $i<$thisLevel; $i++)
				{
					$x .= '<td>&nbsp;</td>'."\n";
				}
				
				# Add onmouseover event for the Cell if it is the 1st Level to show the Remarks 
				$thisID = '';
				$thisIdTag = '';
				$thisTag = '';
				if ($thisLevel == 1)
				{
					$thisID = 'TopicID_'.$thisTopicID;
					$thisIdTag = ' id="'.$thisID.'" ';
					$thisTag = ' onmouseover="js_Show_Topic_Remarks_Layer('.$thisTopicID.', \''.$thisID.'\', \'TopicRemarksDiv\');" onmouseout="js_Hide_Topic_Remarks_Layer(\'TopicRemarksDiv\');" ';
				}
				
				$x .= '<td>'."\n";
					$x .= '<div '.$thisIdTag.' '.$thisTag.' style="width:100%; white-space:normal;">'."\n";
						$x .= $thisName."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
				
				for ($i=$thisLevel; $i<$MaxLevel; $i++)
				{
					$x .= '<td>&nbsp;</td>'."\n";
				}
			
			if (count($thisLinkedTopicIDArr) > 0)
			{
					for ($i=0; $i<$numOfModule; $i++)
					{
						$x .= '<td>&nbsp;</td>'."\n";
					}
				$x .= '</tr>'."\n";
				$x .= $this->Get_Management_MarksheetRevision_SubjectGroupView_Table_Row($TopicInfoArr, $thisLinkedTopicIDArr, $ModuleInfoArr, $StudentStudyTopicInfoArr, $SubjectRubricsItemInfoArr, $StudentSubjectMarksheetScoreArr, $SubjectID, $StudentID, $RowIndex);
			}
			else
			{
				if ($thisLevel == $MaxLevel)
				{
					### Last Level of Topic
					for ($i=0; $i<$numOfModule; $i++)
					{
						$thisModuleID = $ModuleInfoArr[$i]['ModuleID'];
						$thisModuleMarksheetSubmissionStartDate = $ModuleInfoArr[$i]['MarksheetSubmissionStartDate'];
						$thisModuleMarksheetSubmissionEndDate = $ModuleInfoArr[$i]['MarksheetSubmissionEndDate'];
						
						$thisStudy = isset($StudentStudyTopicInfoArr[$StudentID][$thisModuleID][$SubjectID][$thisTopicID]);
						$thisRubricsItemID = $StudentSubjectMarksheetScoreArr[$StudentID][$SubjectID][$thisTopicID][$thisModuleID]['RubricsItemID'];
						$thisRemark = $StudentSubjectMarksheetScoreArr[$StudentID][$SubjectID][$thisTopicID][$thisModuleID]['Remarks'];
						$thisRemark = intranet_htmlspecialchars($thisRemark);
						$RemarkMixLength = $eRC_Rubrics_ConfigArr['MaxLength']['Marksheet']['Remarks'];
						# grey background for not checked cell
						if ($thisStudy)
						{
							$thisTdClass 		= '';
							$thisChkID 			= "mark[$RowIndex][$i]";
							$thisChkName 		= "SubjectLevelArr[$thisModuleID][$thisTopicID]";
							$thisChkClass 		= "ChkModuleID_$thisModuleID ChkTopicID_$thisTopicID";
							$thisOtherParArr 	= array(	'onpaste'=>'pasteContent(\''.$RowIndex.'\', \''.$i.'\');',
									    					'onkeyup'=>'isPasteContent(event, \''.$RowIndex.'\', \''.$i.'\');',
									    					'onkeypress'=>'isPasteContent(event, \''.$RowIndex.'\', \''.$i.'\');',
									    					'onkeydown'=>'isPasteContent(event, \''.$RowIndex.'\', \''.$i.'\');'
									    				);
							$thisDisabled = ($lreportcard->Check_Within_DateRange($thisModuleMarksheetSubmissionStartDate, $thisModuleMarksheetSubmissionEndDate))? 0 : 1;
							$thisDisabledHtml = $thisDisabled?" disabled ":"";
							
							$thisContent = $this->Get_Rubrics_Level_Selection($thisChkID, $thisChkName, $thisRubricsItemID, $OnChange='', $thisChkClass, $SubjectRubricsItemInfoArr, '', $thisOtherParArr, $thisDisabled); 
							$thisContent .= '<br><input type="text" style="margin:1px;" name="SubjectLevelRemarkArr['.$thisModuleID.']['.$thisTopicID.']" value="'.$thisRemark.'" maxlength="'.$RemarkMixLength.'" '.$thisDisabledHtml.'>';
						}
						else
						{
							$thisTdClass = 'class="not_check_cell"';
							$thisContent = '&nbsp;';
						}
							
						$x .= '<td '.$thisTdClass.' style="text-align:center;">'."\n";
							$x .= $thisContent."\n";
						$x .= '</td>'."\n";
					}
					
					// For copy and paste - selection box indexing
					$RowIndex++;
				}
				else
				{
					### Some Topics which do not have the last level => cannot set to study
					for ($i=0; $i<$numOfModule; $i++)
					{
						$x .= '<td>&nbsp;</td>'."\n";
					}
				}
				$x .= '</tr>'."\n";
			}
		}
		return $x;
	}
	
	
	function Get_Management_Marksheet_Import_UI()
	{
	    global $PATH_WRT_ROOT, $lreportcard, $Lang;
	    
        $backURL = 'javascript:js_Go_Back()';
	    
	    # Page Navigation
	    $PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['MenuTitle'], $backURL);
	    $PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
	    
	    $x .= '<form id="form1" name="form1" method="post" action="import_step2.php" onsubmit="return checkForm();" enctype="multipart/form-data">'."\n";
	    $x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
    	       $x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
    	    $x .= '<br>'."\n";
	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
    	       $x .= $this->GET_IMPORT_STEPS(1);
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
    	       $x .= $this->Get_Warning_Message_Box('', $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportDataInstruction']);
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
    	        $x .= $this->Get_Management_Marksheet_Import_Table();
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '</table>'."\n";
	    $x .= '<br>'."\n";
	    
	    # Buttom
	    $SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "");
	    $CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $backURL);
	    
	    $x .= '<div class="edit_bottom_v30">';
    	    $x .= $SubmitBtn."\n";
    	    $x .= $CancelBtn."\n";
	    $x .= '</div>';
	    
// 	    $x .= '<input type="hidden" id="ReportID" name="ReportID" value="'.$ReportID.'">'."\n";
// 	    $x .= '<input type="hidden" id="ClassLevelID" name="ClassLevelID" value="'.$ClassLevelID.'">'."\n";
	    $x .= '</form>'."\n";
	    
	    return $x;
	}
	
	function Get_Management_Marksheet_Import_Table()
	{
	    global $PATH_WRT_ROOT, $LAYOUT_SKIN, $lreportcard, $lreportcard_topic, $Lang;
	    
	    include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
	    include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
	    $scm_ui = new subject_class_mapping_ui();
	    
	    ### Term Selection
	    $CurrentYearTermArr = getCurrentAcademicYearAndYearTerm();
	    $YearTermID = $_POST['YearTermID']? $_POST['YearTermID'] : ($_GET['YearTermID']? $_GET['YearTermID'] : $CurrentYearTermArr['YearTermID']);
	    $OnChange = "js_Changed_Term_Selection(this.value);";
	    $TermSelection = $scm_ui->Get_Term_Selection('YearTermID', $lreportcard->AcademicYearID, $YearTermID, $OnChange, $NoFirst=0, $NoPastTerm=0, $displayAll=1, $AllTitle='', $PastTermOnly=0, $CompareYearTermID='');
	    
	    ### Class Selection
	    $StudentID = $_POST['StudentID']? $_POST['StudentID'] : $_GET['StudentID'];
	    $StudentClassInfoArr = $lreportcard_topic->Get_Student_Class_Info($StudentID);
	    $ClassID = $_POST['ClassID']? $_POST['ClassID'] : ($_GET['ClassID']? $_GET['ClassID'] : $StudentClassInfoArr[0]['YearClassID']);
	    $OnChange = "js_Changed_Class_Selection(this.value);";
	    $ClassSelection = $this->Get_Class_Selection("ClassID", $ClassID, $OnChange, $noFirst=1, $isAll=1, '', $ClassLevelID='', $TeachingClassOnly=1);
	    
	    $x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
	    $x .= '<col class="field_title">';
	    $x .= '<col class="field_c">';
	    
	    # Term
	    $x .= '<tr>'."\n";
	        $x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Term'].'</td>'."\n";
    	    $x .= '<td>'."\n";
    	        $x .= $TermSelection."\n";
    	    $x .= '</td>'."\n";
    	    $x .= '</tr>'."\n";
    	    
	    # Module
	    $x .= '<tr>'."\n";
	        $x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Module'].'</td>'."\n";
    	    $x .= '<td id="ModuleSelectionDiv"></td>'."\n";
	    $x .= '</tr>'."\n";
    	    
	    # Subject
	    $x .= '<tr>'."\n";
	        $x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['Subject'].'</td>'."\n";
    	    $x .= '<td id="SubjectSelectionDiv"></td>'."\n";
	    $x .= '</tr>'."\n";
	    
	    //# Class
	    //$x .= '<tr>'."\n";
	    //    $x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['GeneralArr']['Class'].'</td>'."\n";
    	//    $x .= '<td>'."\n";
    	//       $x .= $ClassSelection."\n";
    	//    $x .= '</td>'."\n";
	    //$x .= '</tr>'."\n";

        # Subject Group
        $x .= '<tr>'."\n";
            $x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['GeneralArr']['SubjectGroup'].'</td>'."\n";
            $x .= '<td id="SubjectGroupSelectionDiv"></td>'."\n";
        $x .= '</tr>'."\n";
	    
	    # File
	    $x .= '<tr>'."\n";
    	    $x .= '<td class="field_title" rowspan="2">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['File'].'</td>'."\n";
    	    $x .= '<td>'."\n";
    	       $x .= '<input class="file" type="file" name="userfile" id="userfile">'."\n";
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
    	       $x .= '<a class="contenttool" href="javascript:jGEN_CSV_TEMPLATE()">'.$Lang['eRC_Rubrics']['GeneralArr']['GenerateCSVFile'].'</a>'."\n";
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    
	    # Remarks
	    $x .= '<tr>'."\n";
    	    $x .= '<td class="field_title" rowspan="2">'.$Lang['eRC_Rubrics']['GeneralArr']['Remarks'].'</td>'."\n";
    	    $x .= '<td>'."\n";
    	       $x .= $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportDataRemarks'];
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    
	    $x .= '</table>'."\n";
	    
	    return $x;
	}
	
	function Get_Management_Marksheet_Import_Step2_UI($targetFilePath, $csvDataSize)
	{
	    global $Lang, $PATH_WRT_ROOT;
	    
	    # Navaigtion
	    $PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['MenuTitle'], "javascript:js_Cancel();");
	    $PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
	    
	    $x = '';
	    $x .= '<form id="form1" name="form1" method="post" action="import_step3.php">'."\n";
	    $x .= '<div class="table_board">'."\n";
	    $x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
        	    $x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
        	    $x .= '<br>'."\n";
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
    	       $x .= $this->GET_IMPORT_STEPS(2);
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
        	    $x .= '<table class="form_table_v30">'."\n";
        	    $x .= '<tr>'."\n";
            	    $x .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
            	    $x .= '<td><div id="SuccessCountDiv">'.$Lang['General']['EmptySymbol'].'</div></td>'."\n";
        	    $x .= '</tr>'."\n";
        	    $x .= '<tr>'."\n";
            	    $x .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
            	    $x .= '<td><div id="FailCountDiv">'.$Lang['General']['EmptySymbol'].'</div></td>'."\n";
        	    $x .= '</tr>'."\n";
    	        $x .= '</table>'."\n";
        	    $x .= '<br style="clear:both;" />'."\n";
        	    
        	    $x .= '<div id="ErrorTableDiv"></div>'."\n";
	        $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '</table>'."\n";
	    $x .= '</div>'."\n";
	    
	    # Button
	    $ContinueBtn = $this->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();", 'ContinueBtn', '', $Disabled=1);
	    $BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();");
	    $CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Cancel();");
	    
	    $x .= '<div class="edit_bottom_v30">'."\n";
	       $x .= $ContinueBtn;
	    $x .= '&nbsp;'."\n";
	       $x .= $BackBtn;
	    $x .= '&nbsp;'."\n";
	       $x .= $CancelBtn;
	    $x .= '</div>'."\n";
	    
	    # iFrame for Validation
	    $thisSrc = "ajax_validate.php?action=Import_Student_Marksheet&targetFilePath=".$targetFilePath."&YearTermID=".$_POST['YearTermID']."&ModuleID=".$_POST['ModuleID']."&SubjectID=".$_POST['SubjectID']."&ClassID=".$_POST['ClassID'];
	    $x .= '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%; height:300px; display:none;"></iframe>'."\n";
	    
	    # Hidden Field for Step 3 use
	    $x .= '<input type="hidden" id="YearTermID" name="YearTermID" value="'.$_POST['YearTermID'].'">'."\n";
	    $x .= '<input type="hidden" id="ModuleID" name="ModuleID" value="'.$_POST['ModuleID'].'">'."\n";
	    $x .= '<input type="hidden" id="SubjectID" name="SubjectID" value="'.$_POST['SubjectID'].'">'."\n";
	    $x .= '<input type="hidden" id="ClassID" name="ClassID" value="'.$_POST['ClassID'].'">'."\n";
	    $x .= '<input type="hidden" id="csvDataSize" name="csvDataSize" value="'.$csvDataSize.'" />'."\n";
	    
	    $x .= '</form>'."\n";
	    $x .= '<br />'."\n";
	    
	    return $x;
	}
	
	function Get_Management_Marksheet_Import_Step3_UI()
	{
	    global $Lang, $PATH_WRT_ROOT;
	    
	    # Navaigtion
	    $PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['MenuTitle'], "javascript:js_Cancel();");
	    $PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
	    
	    $x = '';
	    $x .= '<form id="form1" name="form1" method="post" action="import_step3.php">'."\n";
	    $x .= '<div class="table_board">'."\n";
	    $x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
        	    $x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
        	    $x .= '<br>'."\n";
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
    	       $x .= $this->GET_IMPORT_STEPS(3);
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
        	    $x .= '<table class="form_table_v30">'."\n";
        	    $x .= '<tr>'."\n";
            	    $x .= '<td class="tabletext" style="text-align:center;">'."\n";
            	       $x .= '<span id="ImportStatusSpan"></span>'."\n";
            	    $x .= '</td>'."\n";
        	    $x .= '</tr>'."\n";
        	    $x .= '</table>'."\n";
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '</table>'."\n";
	    $x .= '</div>'."\n";
	    $x .= '<br style="clear:both;" />'."\n";
	    
	    # Button
	    $ImportOtherBtn = $this->GET_ACTION_BTN($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportOtherMarksheetData'], "button", "js_Back_To_Import_Step1();");
	    $BackBtn = $this->GET_ACTION_BTN($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['BackToMarksheetRevision'], "button", "js_Back_To_Marksheet();");
	    
	    $x .= '<div class="edit_bottom_v30">'."\n";
    	    $x .= $ImportOtherBtn;
    	    $x .= '&nbsp;'."\n";
    	    $x .= $BackBtn;
	    $x .= '</div>'."\n";
	    
	    # iFrame for importing data
	    $thisSrc = "ajax_update.php?action=Import_Student_Marksheet&YearTermID=".$_POST['YearTermID']."&ModuleID=".$_POST['ModuleID']."&SubjectID=".$_POST['SubjectID']."&ClassID=".$_POST['ClassID']."&csvDataSize=".$_POST['csvDataSize'];
	    $x .= '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%; height:300px; display:none;"></iframe>'."\n";
	    
	    $x .= '</form>'."\n";
	    $x .= '<br />'."\n";
	    
	    return $x;
	}
	
	function Get_Topic_Selection($SubjectID, $Level, $PreviousLevelTopicID, $ID_Name, $TopicID='', $OnChange='', $all=0, $noFirst=0, $IsMultiple=0,$hasChooseOption=false,$indicator='')
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
		include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");

		$lreportcard_topic = new libreportcardrubrics_topic();
		
		//$PreviousLevelTopicID = ($PreviousLevelTopicID == 'ALL')? '' : $PreviousLevelTopicID;
		$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectID, $TopicID='', $Level, $TopicCode='', $PreviousLevelTopicID, $ParentTopicID='', $ReturnAsso=0);
		$numOfTopic = count($TopicInfoArr);
	
		$SubjectInfo = $lreportcard_topic->Get_Subject_Info($SubjectID);
		$SubjectInfoAssoc = BuildMultiKeyAssoc($SubjectInfo,"SubjectID",Get_Lang_Selection("CH_DES","EN_DES"),1);
		
		$selectionArr = array();
		
		$selection = '';
		
		if($hasChooseOption==true)
		{
			if($Level==1)
			{		
				$selectionArr[''] = Get_Selection_First_Title($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][1]['Select']);
				$selectionArr['ALL'] = Get_Selection_First_Title($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][1]['All']);
			}
			else 
			{
				$selectionArr['ALL'] =Get_Selection_First_Title($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$Level]['All']);
			}
		}
		
		if($indicator=="CommentBank")
		{
			$selectionArr[''] = Get_Selection_First_Title($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][$Level]['Select']);
		}
	
		for ($i=0; $i<$numOfTopic; $i++)
		{
			$thisSubjectName = $SubjectInfoAssoc[$TopicInfoArr[$i]['SubjectID']];
			$thisTopicID = $TopicInfoArr[$i]['TopicID'];
			$thisNameEn = $TopicInfoArr[$i]['TopicNameEn'];
			$thisNameCh = $TopicInfoArr[$i]['TopicNameCh'];
			$thisName = Get_Lang_Selection($thisNameCh, $thisNameEn);
			
		//	$selectionArr[$thisSubjectName][$thisTopicID] = $thisName;
			$selectionArr[$thisTopicID] = $thisName;
		}

		$onchange = "";
		if ($OnChange != "")
			$onchange = ' onchange="'.$OnChange.'" ';
		
		if ($IsMultiple == 1)
			$multiple = ' multiple size=10 ';
		
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.$multiple;
		$firstTitle = Get_Selection_First_Title($Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$Level]['All']);
		
//debug_r($selectionArr);
		$selection .= getSelectByAssoArray($selectionArr, $selectionTags, $TopicID, $all, $noFirst, $firstTitle); 

		return $selection;
	}
	
	function Get_Rubrics_Level_Selection($ID, $Name, $SelectedLevel='', $OnChange='', $Class='', $RubricsItemInfoArr='', $RubricsID='', $OtherParArr='', $Disabled=0)
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
		include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");

		$lreportcard_topic = new libreportcardrubrics_topic();
		
		if ($RubricsItemInfoArr == '' && $RubricsID != '')
		{
			// Get Rubrics Item Info
		}
		
		$selectionArr = array();
		foreach ((array)$RubricsItemInfoArr as $thisRubricsItemID => $thisRubricsItemInfoArr)
		{
			$thisLevelNameEn = $thisRubricsItemInfoArr['LevelNameEn'];
			$thisLevelNameCh = $thisRubricsItemInfoArr['LevelNameCh'];
			$thisName = Get_Lang_Selection($thisLevelNameCh, $thisLevelNameEn);
			
			$selectionArr[$thisRubricsItemID] = $thisName;
		}
		
		$onchange = "";
		if ($OnChange != "")
			$onchange = ' onchange="'.$OnChange.'" ';
		
		$class = "";
		if ($Class != "")
			$class = ' class="'.$Class.'" ';
		
		$disabled = "";
		if ($Disabled == 1)
			$disabled = ' disabled="1" ';	
			
			
		$OtherPar = '';
		if(is_array($OtherParArr) && count($OtherParArr) > 0){
		    foreach($OtherParArr as $parKey => $parVal){
		    	$OtherPar .= ' '.$parKey.'="'.$parVal.'" ';
		    }
	    }
		
		$selectionTags = ' id="'.$ID.'" name="'.$Name.'" '.$onchange.' '.$class.' '.$disabled.' '.$OtherPar;
		$firstTitle = Get_Selection_First_Title($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['SelectLevel']);
		
		$selection = getSelectByAssoArray($selectionArr, $selectionTags, $SelectedLevel, $all=0, $noFirst=0, $firstTitle);
		return $selection;
	}
	
	function Get_Module_Selection($ID_Name, $YearTermID='', $SelectedModuleID='', $OnChange='', $Class='', $IsAll=0, $noFirst=0, $IsMultiple=0, $AcademicYearID='', $checkIsWithinDateRange=false)
	{
		global $Lang, $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
		include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");

		$lreportcard_module = new libreportcardrubrics_module($AcademicYearID);
		$ModuleInfoArr = $lreportcard_module->Get_Module_Info($YearTermID);
		$numOfModule = count($ModuleInfoArr);
		
		$selectionArr = array();
		for($i=0; $i<$numOfModule; $i++)
		{
			$thisModuleID = $ModuleInfoArr[$i]['ModuleID'];
			$thisModuleNameEn = $ModuleInfoArr[$i]['ModuleNameEn'];
			$thisModuleNameCh = $ModuleInfoArr[$i]['ModuleNameCh'];
			$thisName = Get_Lang_Selection($thisModuleNameCh, $thisModuleNameEn);
			
			// [2018-1002-1146-08277] add schedule checking
			if($checkIsWithinDateRange)
			{
    			$thisModuleMarksheetSubmissionStartDate = $ModuleInfoArr[$i]['MarksheetSubmissionStartDate'];
    			$thisModuleMarksheetSubmissionEndDate = $ModuleInfoArr[$i]['MarksheetSubmissionEndDate'];
    			if(!$lreportcard_module->Check_Within_DateRange($thisModuleMarksheetSubmissionStartDate, $thisModuleMarksheetSubmissionEndDate)) {
                    continue;
    			}
			}
			
			$selectionArr[$thisModuleID] = $thisName;
		}
		
		$onchange = "";
		if ($OnChange != "")
			$onchange = ' onchange="'.$OnChange.'" ';
		
		$class = "";
		if ($Class != "")
			$class = ' class="'.$Class.'" ';
		
		if($IsMultiple==1)
			$multiple = ' multiple size=10 ';
		
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$class.' '.$multiple;
		
		if($IsAll==1)
			$firstTitle = Get_Selection_First_Title($Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['AllModule']);
				
		$selection = getSelectByAssoArray($selectionArr, $selectionTags, $SelectedModuleID, $IsAll, $noFirst, $firstTitle);
		return $selection;
	}
	
	function Get_Setting_Module_UI()
	{
		global $Lang;
		
		$ModuleSettingTable = $this->Get_Setting_Module_Table();
		
		$WarningTable = $this->Get_Warning_Message_Box('',$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['EditModuleWarning']);
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'.$WarningTable.'</td>';		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td id="ModuleTableSettingTD">'.$ModuleSettingTable.'</td>';		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";	
		
		return $x;
	}

############################################################################################################
# Setting > Module UI Start                                                
#
	function Get_Setting_Module_Table()
	{
		global $Lang, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
		$lreportcard_module = new libreportcardrubrics_module(); 	
		
		# Get Terms 
		$YearTermArr = getSemesters($lreportcard_module->AcademicYearID);
		
		# Build Term array
		$TermArr[0] = $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['WholeYear'];
		foreach($YearTermArr as $TermID => $TermName)
			$TermArr[$TermID] = $TermName;
		
		# Get Module Info
		$ModuleArr = $lreportcard_module->Get_Module_Info();
		$ModuleAssocArr = BuildMultiKeyAssoc($ModuleArr, array("YearTermID","ModuleID"));
		$ColSpan = 5;

		$x = '';
		$x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
			# col group
			$x .= '<col align="left" style="width: 20%;">'."\n";
			$x .= '<col align="left" style="width: 25%;">'."\n";
			$x .= '<col align="left" style="width: 15%;">'."\n";
			$x .= '<col align="left" style="width: 15%;">'."\n";
			$x .= '<col align="left" style="width: 15%;">'."\n";
			$x .= '<col align="left" style="width: '.$this->toolCellWidth.'px;">'."\n";
			
			# table head
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th>'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Term'].'</th>';
					$x .= '<th colspan="'.$ColSpan.'" class="sub_row_top">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Module'].'</th>';	
				$x .= '</tr>';
				$x .= '<tr>';
					$x .= '<th>'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Name'].'</th>';
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Name'].'</th>';
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ModuleCode'].'</th>';	
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['StartDate'].'</th>';	
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['EndDate'].'</th>';
					$x .= '<th class="sub_row_top">&nbsp;</th>';		
				$x .= '</tr>';
			$x .= '</thead>';		
			
			# table body
			$x .= '<tbody>';
			
				# loop term
				foreach((array)$TermArr as $thisTermID => $thisTerm)
				{
					$TermModule = $ModuleAssocArr[$thisTermID];
					$NumOfTermModule = count($TermModule)+2;
					
					$x .= '<tr>';
						$x .= '<td rowspan="'.$NumOfTermModule.'">'.$thisTerm.'</td>';
					$x .= '</tr>';
					
					# loop module
					foreach((array)$TermModule as $thisModuleID => $thisModuleInfo)
					{
						$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ToolTipArr']['EditModule'];
						$thisOnclick="js_Edit_Module('$thisTermID','$thisModuleID')";
						$DeleteLink = '<a href="javascript:void(0)" onclick="DeleteModule(\''.$thisModuleID.'\')" class="delete_dim" title="'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ToolTipArr']['DeleteModule'].'"></a>';
						$ThickBoxEditLink = $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth,  'edit_dim', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content=""); 
						$ModuleName = Get_Lang_Selection($thisModuleInfo['ModuleNameCh'],$thisModuleInfo['ModuleNameEn']);
						
						$x .= '<tr class="sub_row">';
							$x .= '<td>'.$ModuleName.'</td>';
							$x .= '<td>'.$thisModuleInfo['ModuleCode'].'</td>';		
							$x .= '<td>'.$thisModuleInfo['StartDate'].'&nbsp;</td>';	
							$x .= '<td>'.$thisModuleInfo['EndDate'].'&nbsp;</td>';
							$x .= '<td>';
								$x .= '<div class="table_row_tool row_content_tool">';
									$x .= $ThickBoxEditLink;
									$x .= $DeleteLink;
								$x .= '</div>';
							$x .= '</td>';			
						$x .= '</tr>';
					} // end loop module
					
					# Add module row
					$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ToolTipArr']['AddModule'];
					$thisOnclick="js_Add_Module('$thisTermID')";
					$ThickBoxLink = $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, 'add_dim', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
					
					$x .= '<tr>';
						$x .= '<td>&nbsp;</td>';	 
						$x .= '<td>&nbsp;</td>';	
						$x .= '<td>&nbsp;</td>';
						$x .= '<td>&nbsp;</td>';
						$x .= '<td>';
							$x .= '<div class="table_row_tool row_content_tool">';
								$x .= $ThickBoxLink;
							$x .= '</div>';
						$x .= '</td>';		
					$x .= '</tr>';
					
				}// end loop term
				
//				# Termless Module
//				$x .= '<tr>';
//					$x .= '<td rowspan="'.$NumOfTermModule.'">'.$thisTerm.'</td>';
//				$x .= '</tr>';
				
			$x .= '</tbody>';
		$x .= '</table>'."\n";
		
		return $x; 
		
	}
	
	function Get_Setting_Module_Edit_Layer($YearTermID='',$ModuleID='')
	{
		global $Lang, $PATH_WRT_ROOT, $eRC_Rubrics_ConfigArr;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
		$lreportcard_module = new libreportcardrubrics_module(); 	
		
		# Get Term Dates
		if($YearTermID != 0)
		{
			$TermStartDate = date("Y-m-d",strtotime(getStartDateOfAcademicYear($lreportcard_module->AcademicYearID, $YearTermID)));
			$TermEndDate = date("Y-m-d",strtotime(getEndDateOfAcademicYear($lreportcard_module->AcademicYearID, $YearTermID)));
		}
		
		# Get Btns
		$SubmitBtn = $this->Get_Action_Btn($Lang['Btn']['Submit'],"submit");
		$SubmitAndContinueBtn = $this->Get_Action_Btn($Lang['Btn']['Submit&AddMore'],"Button","SubmitTBForm(1)");
		$CloseBtn = $this->Get_Action_Btn($Lang['Btn']['Close'],"Button","js_Hide_ThickBox()");
		
		# Get Module Info
		if($ModuleID!='')
			$ModuleInfo = $lreportcard_module->Get_Module_Info($YearTermID,$ModuleID);
		
		# Get Max Length Info
		$LengthInfo = $eRC_Rubrics_ConfigArr['MaxLength'];
		
		$x .= '';
		$x .= $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<form id="TBForm" name="TBForm" onsubmit="SubmitTBForm(); return false;">'."\n";
			$x .= '<table width="100%" cellpadding=10px cellspacing=10px align="center" height="310px">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td valign="top">'."\n";	
						$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
							$x .= '<col class="field_title">';
							$x .= '<col class="field_c">';
							
							# Code
							$thisValue = intranet_htmlspecialchars($ModuleInfo[0]["ModuleCode"]);
							$MaxLength =$LengthInfo['Module']['Code'];
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ModuleCode'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<input name="ModuleCode" id="ModuleCode" class="textbox_name required" value="'.$thisValue.'" maxlength="'.$MaxLength.'">'."\n";
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnModuleCode");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							# English Name
							$thisValue = intranet_htmlspecialchars($ModuleInfo[0]["ModuleNameEn"]);
							$MaxLength =$LengthInfo['Module']['NameEn'];
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title" rowspan="2"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ModuleName'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['En'].'</span>';
									$x .= '<span class="row_content_v30"><input name="ModuleNameEn" id="ModuleNameEn" class="textbox_name required" value="'.$thisValue.'" maxlength="'.$MaxLength.'"></span>'."\n";
									$x .= $this->Spacer();
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnModuleNameEn");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							# Chinese Name
							$thisValue = intranet_htmlspecialchars($ModuleInfo[0]["ModuleNameCh"]);
							$MaxLength =$LengthInfo['Module']['NameCh'];
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['Ch'].'</span>';
									$x .= '<span class="row_content_v30"><input name="ModuleNameCh" id="ModuleNameCh" class="textbox_name required" value="'.$thisValue.'" maxlength="'.$MaxLength.'"></span>'."\n";
									$x .= $this->Spacer();
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnModuleNameCh");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							# Apply Date check box 
							if($YearTermID!=0)
							{
								$thisChecked = empty($ModuleInfo)||$ModuleInfo[0]["StartDate"]||$ModuleInfo[0]["EndDate"]?"checked":"";
								$thisDisabled = '';
							}
							else
							{
								$thisChecked = '';
								$thisDisabled = 'disabled';
							}

							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['ApplyDate'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<input id="ApplyDate" type="checkbox" onclick="DisableDate(this)" '.$thisChecked.' '.$thisDisabled.'>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							
							# Start Date
							if($YearTermID!=0)
							{
								$thisDisabled = $ModuleInfo && (!$ModuleInfo[0]["StartDate"]&&!$ModuleInfo[0]["EndDate"])?"disabled":"";
								$thisValue = $ModuleInfo[0]["StartDate"]?$ModuleInfo[0]["StartDate"]:$TermStartDate;							
							}
							else
							{
								$thisDisabled = "disabled";
								$thisValue = "";							
							}
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title" rowspan="2">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Period'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span class="sub_row_content_v30">('.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['StartDate'].')</span>';
									$x .= '<span class="row_content_v30">';
										$x .= $this->GET_DATE_PICKER("StartDate",$thisValue,$thisDisabled)."\n";
										$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnStartDate");
									$x .= '</span>';
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							
							# End Date
							if($YearTermID!=0)
							{
								$thisDisabled = $ModuleInfo && (!$ModuleInfo[0]["StartDate"]&&!$ModuleInfo[0]["EndDate"])?"disabled":"";
								$thisValue = $ModuleInfo[0]["EndDate"]?$ModuleInfo[0]["EndDate"]:$TermEndDate;
							}
							else
							{
								$thisDisabled = "disabled";
								$thisValue = "";							
							}
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span class="sub_row_content_v30">('.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['EndDate'].')</span>';
									$x .= '<span class="row_content_v30">';
										$x .= $this->GET_DATE_PICKER("EndDate",$thisValue,$thisDisabled)."\n";
										$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnEndDate");
									$x .= '</span>';
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= $this->MandatoryField();		
			
			# Btns
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SubmitBtn;
				$x .= "&nbsp;".$SubmitAndContinueBtn;
				$x .= "&nbsp;".$CloseBtn;
			$x .= '</div>'."\n";
		
		# hidden field
		$x .= '<input type="hidden" name="TermStartDate" id="TermStartDate" value="'.$TermStartDate.'">'."\n";
		$x .= '<input type="hidden" name="TermEndDate" id="TermEndDate" value="'.$TermEndDate.'">'."\n";
		$x .= '<input type="hidden" name="ModuleID" id="ModuleID" value="'.$ModuleID.'">'."\n";
		$x .= '<input type="hidden" name="YearTermID" id="YearTermID" value="'.$YearTermID.'">'."\n";
		
		$x .= '</form>';		
		$x .= $this->FOCUS_ON_LOAD("TBForm.ModuleCode");
		
		return $x;
	}
#
# Setting > Module UI End                                                
##########################################################################################################	
	
############################################################################################################
# Setting > Rubrics UI Start                                                
#
	function Get_Setting_Rubrics_UI()
	{
//		$RubricsSettingTable = $this->Get_Setting_Rubrics_Table();
		global $Lang;

		# Get Btns
		$SubmitBtn = $this->Get_Action_Btn($Lang['Btn']['Save'],"submit","","SaveBtn","disabled");
		//$ResetBtn = $this->Get_Action_Btn($Lang['Btn']['Reset'],"reset");
		
		$RubricsTableSetting = $this->Get_Ajax_Loading_Image();
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post" onsubmit="CheckForm(); return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>';
						# term hard code YearID, for future development. change to Year selection to cater year-based Rubrics mapping
						$x .= '<input id="YearID" name="YearID" value="0" type="hidden">';
					$x .= '</td>';		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td id="RubricsTableSettingTD">'.$RubricsTableSetting.'</td>';		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<br>'."\n";
						$x .= '<div class="edit_bottom_v30">'."\n";
							$x .= $SubmitBtn."\n";
							//$x .= '&nbsp;'.$ResetBtn."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";	
		
		return $x;
	}
	
	function Get_Setting_Rubrics_Table($YearID = 0)
	{
		global $Lang, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
		$lreportcard_rubrics = new libreportcardrubrics_rubrics(); 	
		
		include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		# Get Subject List
		$scm = new subject(); 	
		$SubjectList = $scm->Get_Subject_List(1);

		# Learning Category List
		$lcat = new Learning_Category();
		$LearningCatList = $lcat->Get_All_Learning_Category(1);
		
		# Get Rubrics Info
		$RubicsInfoArr = $lreportcard_rubrics->Get_Rubics_Info();
		# Prepare Array for Rubrics Selection 
		foreach($RubicsInfoArr as $thisRubricsInfo)
		{
			$thisRubricsID = $thisRubricsInfo["RubricsID"];
			$thisRubricsName = Get_Lang_Selection($thisRubricsInfo["RubricsCh"],$thisRubricsInfo["RubricsEn"])." (".$thisRubricsInfo["RubricsCode"].")";
			$RubicsOptions[] = array($thisRubricsID, $thisRubricsName);
		}
		
		# Get Subject Rubrics Mapping
		$SubjectRubicsMap = $lreportcard_rubrics->Get_Rubrics_Subject_Mapping();
		
		$x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
			# col group
			$x .= '<col align="left" style="width: 30%;">'."\n";
			$x .= '<col align="left" style="width: 30%;">'."\n";
			$x .= '<col align="left">'."\n";
			
			# table head
			# get Set All Scheme Selection
			$SelectTitle = $Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['Rubrics'];
			$SetAllSchemeSelection = '<span class="table_filter">'.getSelectByArray($RubicsOptions," id='SetAllSchemeSelection' class='RubricsSchemeSelect' ",'',0,0,$SelectTitle).'</span>';
			$SetAllSchemeSelection .= '<div class="table_row_tool"><a href="javascript:void(0);" onclick="SetAllSelection();" class="icon_batch_assign" title="'.$Lang['Btn']['ApplyToAll'].'"></a></div>';
			
			# "add" option
			$RubicsOptions[] = array('', $Lang['Btn']['New']."...");
			 
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th>'.$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['LearningCategory'].'</th>';
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['Subject'].'</th>';
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['RubricsScheme'].'</th>';
	
				$x .= '</tr>';
				$x .= '<tr class="sub_row">';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td class="sub_row_top">&nbsp;</td>';
					$x .= '<td class="sub_row_top">'.$SetAllSchemeSelection.'</td>';	
				$x .= '</tr>';
			$x .= '</thead>';	
			
			# table body
			$x .= '<tbody>';
			
				# loop Subject Category
				foreach((array)$SubjectList as $SubjCatID => $SubjectArr)
				{
					# Subject Count
					$thisSubjectCount = count($SubjectArr);
					
					if(trim($SubjCatID)=='temp')
					{
						$thisSubjectCatName = $Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['NoLearningCategory'];
					}
					else
					{
						$thisSubjectCat = $LearningCatList[$SubjCatID];
						$thisSubjectCatName = $thisSubjectCat["Name"]." (".$thisSubjectCat["Code"].")";
					}
					if(trim($SubjCatID)=='')
					$x .= '<tr>'."\n";
						$x .= '<td rowspan="'.($thisSubjectCount+1).'">';
							$x .= $thisSubjectCatName;
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
					# loop Subject
					foreach((array)$SubjectArr as $SubjectID => $SubjectData)
					{
						# get Scheme Selection
						$SchemeSelection = '<span class="table_filter">'.getSelectByArray($RubicsOptions," id='Rubrics_Scheme_$SubjectID' name='RubricsScheme[$SubjectID]' class='RubricsSchemeSelect' onchange='SchemeSelectChange(this)' ",$SubjectRubicsMap[$YearID][$SubjectID],0,0,$SelectTitle).'</span>'; 
	
						# Get Edit/Delete Btn					
						$thisOnclick="js_Edit_Rubrics('$SubjectID')";
						$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ThickBoxTitleArr']['EditRubrics'];
						$DeleteLink = '<a href="javascript:void(0)" onclick="DeleteRubrics(\''.$SubjectID.'\')" class="delete_dim" title="'.$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ToolTipArr']['DeleteRubrics'].'"></a>';
						$ThickBoxEditLink = $this->Get_Thickbox_Link(500, $this->thickBoxWidth, 'edit_dim', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
						
						$thisSubjName = Get_Lang_Selection($SubjectData["CH_DES"],$SubjectData["EN_DES"]);
						$x .= '<tr class="sub_row">'."\n";
							$x .= '<td>'.$thisSubjName." (".$SubjectData["CODEID"].")".'</td>'."\n";
							$x .= '<td>'."\n";
							$x .= $SchemeSelection;
								$x .= '<div class="table_row_tool row_content_tool">';
									$x .= $ThickBoxEditLink;
									$x .= $DeleteLink;
								$x .= '</div>';
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";	
					}
				}
			$x .= '</tbody>';
		$x .= '</table>'."\n";

		return $x;
	}
	
	function Get_Setting_Rubrics_Edit_Layer($RubricsID='')
	{
		global $Lang, $PATH_WRT_ROOT, $eRC_Rubrics_ConfigArr;
		
		# get rubrics item
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
		$lreportcard_rubrics = new libreportcardrubrics_rubrics(); 	
		
		# Get Used Rubrics Item List
		$UsedItemList = $lreportcard_rubrics->GetUsedRubricItemList();
		
		# Get Max Length Info
		$LengthInfo = $eRC_Rubrics_ConfigArr['MaxLength']; 
		
		if(trim($RubricsID) != '' )
		{
			$RubricsInfo = $lreportcard_rubrics->Get_Rubics_Info($RubricsID);
			$RubricsItemArr = $lreportcard_rubrics->Get_Rubics_Item_Info($RubricsID);
		}
		
		$x .= '';
		$x .= '<form id="TBForm" name="TBForm" onsubmit="CheckTBForm(); return false;">'."\n";
			$x .= '<div style="height:380px; overflow:auto">';
				$x .= '<table width="95%" cellpadding="2" cellspacing="2" align="center">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td height="85px">'."\n";
							# Rubrics Info Setting
							$x .= '<table width="100%" cellpadding="2" cellspacing="2" class="form_table_v30">'."\n";

								$x .= '<col class="field_title">'."\n";
								$x .= '<col class="field_c">'."\n";
							
								# Code
								$thisValue = intranet_htmlspecialchars($RubricsInfo[0]["RubricsCode"]);
								$MaxLength = $LengthInfo['Rubrics']['Code'];
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['RubricsCode'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= '<input name="RubricsCode" id="RubricsCode" class="textbox_name required" value="'.$thisValue.'" maxlength="'.$MaxLength.'">'."\n";
										$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnRubricsCode");
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
							
								# English Name
								$thisValue = intranet_htmlspecialchars($RubricsInfo[0]["RubricsEn"]);
								$MaxLength = $LengthInfo['Rubrics']['NameEn'];
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title" rowspan="2"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['RubricsName'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['En'].'</span>';
										$x .= '<span class="row_content_v30"><input name="RubricsEn" id="RubricsEn" class="textbox_name required" value="'.$thisValue.'"  maxlength="'.$MaxLength.'"></span>'."\n";
										$x .= $this->Spacer();
										$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnRubricsEn");
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								
								# Chinese Name
								$MaxLength = $LengthInfo['Rubrics']['NameCh'];
								$thisValue = intranet_htmlspecialchars($RubricsInfo[0]["RubricsCh"]);
								$x .= '<tr>'."\n";
									$x .= '<td>'."\n";
										$x .= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['Ch'].'</span>';
										$x .= '<span class="row_content_v30"><input name="RubricsCh" id="RubricsCh" class="textbox_name required" value="'.$thisValue.'"  maxlength="'.$MaxLength.'"></span>'."\n";
										$x .= $this->Spacer();
										$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnRubricsCh");
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								
							$x .= '</table>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
	
					$x .= '<tr>'."\n";
						$x .= '<td valign="top">'."\n";
	
							# Rubrics Item Info Setting
							$x .= '<table id="RubricsItemSettingTable" class="common_table_list_v30 normal_table_list">'."\n";
								$x .= '<tbody>'."\n";
									$x .= '<col align="left">'."\n";
									$x .= '<col align="center" width="5%">'."\n";
									$x .= '<col align="left" style="width: '.$this->toolCellWidth.'px;">'."\n";
									
									# thead
									$x .= '<tr class="nodrag nodrop">'."\n";				
										$x .= '<th colspan="3">'."\n";
											$x .= $Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['RubricsLevelDescription'];
										$x .= '</th>'."\n";
									$x .= '</tr>'."\n";
									
									
									# tbody
									$AnyItemUsed = false;
									$NumOfRubrics = max(2,count($RubricsItemArr)); // at least print 2
									# loop each rubrics items
									for($i=0; $i < $NumOfRubrics;$i++)
									{
											 
										if(isset($RubricsItemArr))
										{
											$TRData['RubricsItemLevel'] = $RubricsItemArr[$i]["LevelNameEn"];
											$TRData['RubricsItemDesc'] = $RubricsItemArr[$i]["DescriptionEn"];
											$TRData['RubricsItemID'] = $RubricsItemArr[$i]["RubricsItemID"];
											if(in_array($RubricsItemArr[$i]["RubricsItemID"],(array)$UsedItemList))
												$AnyItemUsed = true;
										}
										
										$x .= $this->Get_Rubrics_Item_tr($TRData);
										
									} # end loop each rubrics items
									
								$x .= '</tbody>'."\n";
							$x .= '</table>'."\n";
							#End Rubrics Item Info Setting
							
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";			
				$x .= '</table>'."\n";
			$x .= '</div>';	
			$x .= $this->MandatoryField();
			
			#hidden field
			$x .= '<input type="hidden" name="RubricsID" id="RubricsID" value="'.$RubricsID.'">'."\n";
			$x .= '<input type="hidden" id="AnyItemUsed" value="'.$AnyItemUsed.'">'."\n";
			$x .= '<input type="hidden" id="SaveAs" name="SaveAs" value="0">'."\n";
			
			# edit bottom
			# Get Btns
			$SaveBtn = $this->Get_Action_Btn($Lang['Btn']['Save'],"Button","js_Save(0)");
			$SaveAsBtn = $this->Get_Action_Btn($Lang['Btn']['SaveAs'],"Button","js_Save(1)");
			$ResetBtn = $this->Get_Action_Btn($Lang['Btn']['Reset'],"Button","js_Reset_Rubrics_Item()");
			$CloseBtn = $this->Get_Action_Btn($Lang['Btn']['Close'],"Button","js_Hide_ThickBox()");
			
			$x .= '<div class="edit_bottom_v30">';
				$x .= $SaveBtn;
				$x .= "&nbsp;".$SaveAsBtn;
				//$x .= "&nbsp;".$ResetBtn;
				$x .= "&nbsp;".$CloseBtn;
			$x .= '</div>';
			
		$x .= '</form>';
		$x .= $this->FOCUS_ON_LOAD("TBForm.RubricsCode");
				
		return $x;		
	}
	
	function Get_Rubrics_Item_tr($TRData='')
	{
		global $eRC_Rubrics_ConfigArr, $Lang;
		
		$LengthInfo = $eRC_Rubrics_ConfigArr["MaxLength"];
		
		$DeleteLink = '<a href="javascript:void(0);" onclick="DeleteRubricsItem(this)" class="delete_dim delete_btn" title="'.$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ToolTipArr']['DeleteRubricsLevel'].'"></a>';
		$AddLink = '<a href="javascript:void(0);" onclick="AddRubricsItem(this)" class="add_dim add_btn" title="'.$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ToolTipArr']['AddRubricsLevel'].'"></a>';
		$MoveLink = '<a href="javascript:void(0);" class="move_order_dim move_btn" title="'.$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['ToolTipArr']['MoveRubricsLevel'].'"></a>';
		$NameMaxLength = $LengthInfo['RubricsItem']['NameEn'];
		$DescMaxLength = $LengthInfo['RubricsItem']['Desc'];
		
		if($TRData['RubricsItemID'])
			$HiddenItemID = '<input type="hidden" name="RubricsItemID[]" value="'.$TRData['RubricsItemID'].'">';
		
		$x .= '<tr class="ItemRow">'."\n";				
			$x .= '<td>'."\n";
				$x .= '<div style="margin:2px;"><input name="RubricsItem[]" class="RubricsItem textboxnum" value="'.intranet_htmlspecialchars($TRData['RubricsItemLevel']).'" maxlength="'.$NameMaxLength.'"> <span class="OrderRemark"></span></div>'."\n";
				$x .= '<div style="margin:2px;"><input name="RubricsItemDesc[]" class="RubricsItemDesc" style="width:100%" value="'.intranet_htmlspecialchars($TRData['RubricsItemDesc']).'" maxlength="'.$DescMaxLength.'"></div>'."\n";
				$x .= '<div style="color:red;" class="ItemWarning"></div>';
				$x .= $HiddenItemID;
			$x .= '</td>'."\n";
			$x .= '<td class="Draggable">'."\n";
				$x .= '<div class="table_row_tool row_content_tool">';
					$x .= $MoveLink;
				$x .= '</div>';	
			$x .= '</td>'."\n";
			$x .= '<td>'."\n";
				$x .= '<div class="table_row_tool row_content_tool">';
					$x .= $AddLink;
					$x .= $DeleteLink;
				$x .= '</div>';
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		
		return $x;
	}
	
	function Get_Rubrics_Item_tr_for_JS()
	{
		// avoid multi line str cause JS Error 
		$TRData['RubricsItemID'] = "new";
		$JShtml = str_replace("\n","",$this->Get_Rubrics_Item_tr($TRData));
		return $JShtml;
	}
#	
# Setting > Subject Rubrics UI End                                               
############################################################################################################

############################################################################################################	
# Setting > Report Template Setting UI Start                                                
#

	function Get_Setting_Report_Template_UI($field='',$order='',$page='',$msg='')
	{
		global $Lang;
		$DBtable = $this->Get_Setting_Report_Template_DBTable($field,$order,$page);
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post" action="reportcard_template.php" onsubmit="CheckForm(); return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td align="right">'.$this->GET_SYS_MSG($msg).'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						
						# Main Table  
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div class="content_top_tool">'."\n";
										$x .= '<div class="Conntent_tool">'."\n";
											$x .= $this->GET_LNK_NEW("new.php")."\n";
										$x .= '</div>'."\n";
										$x .= '<br style="clear: both;">'."\n";
									$x .= '</div>'."\n";
									$x .= '<div class="common_table_tool">'."\n";
								        $x .= '<a class="tool_edit" href="javascript:checkEdit(document.form1,\'ReportID[]\',\'edit.php\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
										$x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'ReportID[]\',\'delete.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
									$x .= '</div>'."\n";		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= $DBtable;
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						  
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";
		
		return $x;	
	}
	
	function Get_Setting_Report_Template_DBTable($field='',$order='',$page='')
	{
		global $Lang, $page_size, $ck_settings_report_template_page_size;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		include_once("libreportcardrubrics_module.php");
		$lreportcard = new libreportcardrubrics();
		
		$field = ($field=='')? 1 : $field;
		$order = ($order=='')? 1 : $order;
		$page = ($page=='')? 1 : $page;

		if (isset($ck_settings_report_template_page_size) && $ck_settings_report_template_page_size != "") $page_size = $ck_settings_report_template_page_size;
		$li = new libdbtable2007($field,$order,$page);
		
		$sql = $lreportcard->GetReportTemplateDBTableSql();
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
		
		$li->field_array = array("ReportTitle", "ReportTypeDisplayOrder", "Module", "DateModified");
		$li->column_array = array(0,0,0,0);
		$li->wrap_array = array(0,0,0,0);
		$li->fieldorder2 = " ,Module";
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
		$li->column_list .= "<th width='35%' >".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ReportTitle'])."</th>\n";
		$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ReportType'])."</th>\n";
		$li->column_list .= "<th width='25%'>".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['MenuTitle'])."</th>\n";
		$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['LastModifiedDate'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("ReportID[]")."</th>\n";
		$li->no_col = $pos+2;
	
		$x .= $li->display();
//		debug_pr($li->built_sql());
//		debug_pr(mysql_error());
		$x .= '<input type="hidden" name="pageNo" value="'.$li->pageNo.'" />';
		$x .= '<input type="hidden" name="order" value="'.$li->order.'" />';
		$x .= '<input type="hidden" name="field" value="'.$li->field.'" />';
		$x .= '<input type="hidden" name="page_size_change" value="" />';
		$x .= '<input type="hidden" name="numPerPage" value="'.$li->page_size.'" />';
	
		return $x;
	}
	
	
	function Get_Setting_Report_Template_Edit_UI($ReportID='')
	{
		global $Lang, $eRC_Rubrics_ConfigArr;
		
		# get module list
		include_once("libreportcardrubrics_module.php");
		$lreportcard_module = new libreportcardrubrics_module();
		
		# Get Max Length Info
		$LengthInfo = $eRC_Rubrics_ConfigArr['MaxLength']['ReportTemplate']; 
		
		# Get Report Template Info 
		if($ReportID!='') # edit
		{
			$action = "edit_update.php";
			
			$ReportInfo = $lreportcard_module->GetReportTemplateInfo($ReportID);
			list($ReportTitleEn, $ReportTitleCh, $ReportType) = $ReportInfo[0];
			
			# Get Module Mapping
			$ReportModuleInfo = $lreportcard_module->GetReportTemplateModuleInfo($ReportID);
			$Key = Get_Lang_Selection("ModuleNameCh","ModuleNameEn"); 
			$ModuleNameArr = Get_Array_By_Key($ReportModuleInfo, $Key);
			
			# ReportType
			$ReportTypeDisplay = $ReportType=="T"?$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ModuleReport']:$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ConsolidatedReport']."\n";
			$ReportTypeDisplay .= '<br>';
			$ReportTypeDisplay .= implode(",",(array)$ModuleNameArr);
			
			$NavigationName = $Lang['Btn']['New'];
			
		}
		else # new
		{
			$action = "new_update.php";
			
			$ModuleInfoArr = $lreportcard_module->Get_Module_Info();
			
			# build module selection $ checkboxes
			foreach($ModuleInfoArr as $thisModuleInfo)
			{
				list($ModuleID, $ModuleCode, $ModuleNameEn ,$ModuleNameCh) = $thisModuleInfo;
				$ModuleName = Get_Lang_Selection($ModuleNameCh,$ModuleNameEn)."($ModuleCode)";
				
				$ModuleSelectOptions[] = array($ModuleID,$ModuleName);
				$ModuleCheckBoxList .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$this->Get_Checkbox($ID="ModuleCheckBox$ModuleID", $Name='ModuleID[]', $Value=$ModuleID, $isChecked=0, $Class='ConsolidatedMember', $ModuleName, $Onclick='UncheckCheckAll(this);', $Disabled=1)."<br>";
			}
			$ModuleSelection = getSelectByArray($ModuleSelectOptions," id='ModuleModuleID' name='ModuleID[]' class='ModuleMember' ",0,0,1);
		
			# build Select All checkbox
			$Display = $Lang['Btn']['SelectAll'];
			$SelectAllCheckBox = $this->Get_Checkbox($ID="CheckAllModule", $Name='', $Value='', $isChecked=0, $Class='ConsolidatedMember', $Display, $Onclick='CheckAll(this);', $Disabled=1);
		
			# $ModuleReportDisplay
			$ModuleReportDisplay .= '<div>'."\n";
				$Display = $Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ModuleReport'];
				$ModuleReportDisplay .= $this->Get_Radio_Button($ID="TermReportRadio", $Name="ReportType", $Value="T", $isChecked=1, $Class="ReportTypeRadio", $Display, $Onclick="ReportTypeChange()")."\n";
				$ModuleReportDisplay .= $ModuleSelection;
			$ModuleReportDisplay .= '</div>'."\n";	
			
			# $ConsolidatedReportDisplay
			$ConsolidatedReportDisplay .= '<div>'."\n";
				$Display = $Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ConsolidatedReport'];
				$ConsolidatedReportDisplay .= $this->Get_Radio_Button($ID="ConsolidatedReportRadio", $Name="ReportType", $Value="W", $isChecked=0, $Class="ReportTypeRadio", $Display, $Onclick="ReportTypeChange()")."<br>\n";
				$ConsolidatedReportDisplay .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$SelectAllCheckBox."<br>\n";
				$ConsolidatedReportDisplay .= $ModuleCheckBoxList;
			$ConsolidatedReportDisplay .= '</div>'."\n";
			
			$ReportTypeDisplay = $ModuleReportDisplay.$ConsolidatedReportDisplay;
			
			$ReportTypeRequired = '<span class="tabletextrequire">*</span>';
			
			$NavigationName = $Lang['Btn']['New'];
		}

		# navaigtion
	 	$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ReportCardTemplate'],"reportcard_template.php");
	 	$PAGE_NAVIGATION[] = array($NavigationName,'',1);
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post" onsubmit="return CheckForm();" action="'.$action.'">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION)."\n";
						$x .= '<br><br>'."\n";	
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						
						# main table
						$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="2" class="form_table_v30">'."\n";
							$x .= '<col class="field_title">';
							$x .= '<col class="field_c">';
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title" rowspan="2"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ReportTitle'].'</td>'."\n";		
								$x .= '<td><span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['En'].'</span><input id="ReportTitleEn" name="ReportTitleEn" value="'.intranet_htmlspecialchars($ReportTitleEn).'" class="textbox_name" maxlength="'.$LengthInfo['NameEn'].'"></td>'."\n";		
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td><span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['Ch'].'</span><input id="ReportTitleCh" name="ReportTitleCh" value="'.intranet_htmlspecialchars($ReportTitleCh).'" class="textbox_name" maxlength="'.$LengthInfo['NameCh'].'"></td>'."\n";		
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$ReportTypeRequired.$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ReportType'].'</td>'."\n";		
								$x .= '<td>'."\n";
									$x .= $ReportTypeDisplay."\n";	
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";	
						$x .= '<br>'."\n";
						$x .= $this->MandatoryField();
						# Edit bottom
						$SubmitBtn = $this->Get_Action_Btn($Lang['Btn']['Save'],"submit","","SaveBtn");
						$CancelBtn = $this->Get_Action_Btn($Lang['Btn']['Cancel'],"button","window.location='reportcard_template.php';","BackBtn");
						$x .= '<div class="edit_bottom_v30">';
							$x .= $SubmitBtn."\n";
							$x .= "&nbsp;".$CancelBtn."\n";
						$x .= '</div>';
						
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<input type="hidden" id="ReportID" name="ReportID" value="'.$ReportID.'">';	
		$x .= '</form>'."\n";
		$x .= $this->FOCUS_ON_LOAD("form1.ReportTitleEn")."\n";
		
		return $x;
	}
	
	
#	
# Setting > Report Template Setting UI End                                               
############################################################################################################

############################################################################################################	
# Setting > ECA Setting UI Start                                                
#

	function Get_Setting_ECA_Category_UI()
	{
		global $Lang;
		$CategoryTable = $this->Get_Setting_ECA_Category_Table();
		
		# navaigtion
	 	$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Category']);
				
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post" onsubmit="CheckForm(); return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
						$x .= '<br><br>';
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td id="CategoryTableTD">'."\n";
						$x .= $CategoryTable."\n";  
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";
		
		return $x;	
	}
	
	function Get_Setting_ECA_Category_Table()
	{
		global $PATH_WRT_ROOT,$Lang;

		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
		$lreportcard_eca = new libreportcardrubrics_eca(); 	
		
		$EcaCatArr = $lreportcard_eca->Get_ECA_Category_Info();
		$EcaCatArr = BuildMultiKeyAssoc((array)$EcaCatArr, array("EcaCategoryID"));
		$SubCatArr = $lreportcard_eca->Get_ECA_SubCategory_Info();
		$SubCatArr = BuildMultiKeyAssoc((array)$SubCatArr, array("EcaSubCategoryID"));
		$CatItemMap = $lreportcard_eca->Get_ECA_Category_Item_Info();
		$CatItemMap = BuildMultiKeyAssoc($CatItemMap,array("EcaCategoryID","EcaSubCategoryID","EcaItemID"));
				
		# $SettingBtn
		$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ThickBoxTitleArr']['AddEditCategory'];
		$thisOnclick = "js_Edit_Category();";
		$SettingLink = $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, 'setting_row', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
		
		$x .= '<table id="SubCategoryTable" width="100%" border="0" cellspacing="0" cellpadding="2" class="common_table_list_v30">'."\n";
			$x .= '<col width="25%">';
			$x .= '<col width="25%">';
			$x .= '<col width="15%">';
			$x .= '<col>';
			$x .= '<col width="30px" align="center">';
			$x .= '<col width="'.$this->toolCellWidth.'px">';

			$x .= '<thead>'."\n";
				$x .= '<tr class="nodrag nodrop">'."\n";
					$x .= '<th>';
						$x .= '<span style="float:left">';
							$x .= $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Category'];
						$x .= '</span>';
						$x .= '<span class="table_row_tool" style="float:right">';
							$x .= $SettingLink;
						$x .= '</span>';
					$x .= '</th>'."\n";
					$x .= '<th class="sub_row_top" colspan="5">'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Subcategory'].'</th>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr class="nodrag nodrop">'."\n";
					$x .= '<th>'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Name'].'</th>'."\n";
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Name'].'&nbsp;</th>'."\n";
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Code'].'&nbsp;</th>'."\n";
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['NumOfItem'].'&nbsp;</th>'."\n";
					$x .= '<th class="sub_row_top">&nbsp;</th>'."\n";
					$x .= '<th class="sub_row_top">&nbsp;</th>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			# Eca Category Rows 
			$MoveLink .= $this->GET_LNK_MOVE("javascript:void(0);", $Lang['Btn']['Move']);
			foreach((array)$CatItemMap as $CategoryID => $SubCatItemMap)
			{
				$x .= '<tbody class="tbody'.$CategoryID.'">'."\n";
				
					$EcaCat = $EcaCatArr[$CategoryID];
					$CategoryID = $EcaCat['EcaCategoryID'];
					$CategoryName = Get_Lang_Selection($EcaCat['EcaCategoryNameCh'],$EcaCat['EcaCategoryNameEn']);
					$CategoryName = intranet_htmlspecialchars($CategoryName);
					
					$NumOfSubCat = count(array_filter(array_keys((array)$SubCatItemMap)));
					if($NumOfSubCat>0)
						$RowSpan =  'rowspan="'.($NumOfSubCat+2).'"';;
					
					# Add Sub Cat Btn 
					$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['AddCategory'];
					$thisOnclick="js_Add_SubCategory('$CategoryID')";
					$ThickBoxLink = $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, 'add_dim', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
					
					$x .= '<tr class="nodrag nodrop">'."\n";
						$x .= '<td '.$RowSpan.'>'.$CategoryName.'</td>'."\n";
						# if no sub cat, add row btn and cat name should be in the same row
						if($NumOfSubCat==0) 
						{
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>';
								$x .= '<div class="table_row_tool row_content_tool">';
									$x .= $ThickBoxLink;
								$x .= '</div>';
							$x .= '</td>'."\n";	
						}
					$x .= '</tr>'."\n";
					
					$DisplayOrder = array(0);
					if($NumOfSubCat>0)
					{
						
						foreach((array)$SubCatItemMap as $SubCategoryID => $ItemArr)
						{
							$SubCat = $SubCatArr[$SubCategoryID];
							$SubCategoryName = Get_Lang_Selection($SubCat['EcaSubCategoryNameCh'],$SubCat['EcaSubCategoryNameEn']);
							$SubCategoryName = intranet_htmlspecialchars($SubCategoryName);
							$SubCategoryCode = intranet_htmlspecialchars($SubCat['EcaSubCategoryCode']);
							
							$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['EditSubCategory'];
							$thisOnclick="js_Edit_SubCategory('$CategoryID','$SubCategoryID')";
							$ThickBoxEditLink = $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, 'edit_dim', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content=""); 
							$ItemExist = $SubCat['NumOfItem']==0?0:1;
							$DeleteLink = '<a href="javascript:void(0)" onclick="DeleteSubCategory(\''.$SubCategoryID.'\',\''.$ItemExist.'\')" class="delete_dim" title="'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['DeleteSubCategory'].'"></a>';
							
							$x .= '<tr class="sub_row">'."\n";
								$x .= '<td>'.$SubCategoryName.'</td>'."\n";
								$x .= '<td>'.$SubCategoryCode.'</td>'."\n";
								$x .= '<td><a href="item.php?EcaCategoryID='.$CategoryID.'&EcaSubCategoryID='.$SubCategoryID.'">'.$SubCat['NumOfItem'].'</a></td>'."\n";
								$x .= '<td class="Draggable" align="center">'.$MoveLink.'</td>'."\n";
								$x .= '<td>';
									# hidden field
									$x .= '<input type="hidden" class="EcaSubCategoryID" id="EcaSubCategoryID_'.$SubCategoryID.'" name="EcaSubCategoryID[]" value="'.$SubCategoryID.'"> ';
								
									$x .= '<div class="table_row_tool row_content_tool" >';
										$x .= $ThickBoxEditLink;
										$x .= $DeleteLink;
									$x .= '</div>';
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
							
							$DisplayOrder[] = $SubCat["DisplayOrder"];
						}
					}
				
				# Add Cat Row
				# if sub cat exist, add row btn and cat name should be placed under sub cat info
				if($NumOfSubCat>0) 
				{
					$x .= '<tr class="nodrag nodrop">'."\n";
						$x .= '<td>&nbsp;</td>'."\n";
						$x .= '<td>&nbsp;</td>'."\n";
						$x .= '<td>&nbsp;</td>'."\n";
						$x .= '<td>&nbsp;</td>'."\n";
						$x .= '<td>';
							$x .= '<div class="table_row_tool row_content_tool">';
								$x .= $ThickBoxLink;
							$x .= '</div>';
						$x .= '</td>'."\n";		
					$x .= '</tr>'."\n";
				}
				
				# MaxDisplatOrder	
				$MaxDisplayOrder = max($DisplayOrder);
				$x .= '<input type="hidden" id="MaxDisplayOrder_'.$CategoryID.'" name="MaxDisplayOrder_'.$CategoryID.'" value="'.$MaxDisplayOrder.'">';
		
			$x .= '</tbody>'."\n";		
		}
		$x .= '</table>'."\n";	
		
		
		
		
		return $x;
	}
	
	function Get_Setting_ECA_Category_Edit_Layer()
	{
		global $Lang, $PATH_WRT_ROOT, $eRC_Rubrics_ConfigArr;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
		$lreportcard_eca = new libreportcardrubrics_eca(); 	
		$EcaCatArr = $lreportcard_eca->Get_ECA_Category_Info();
		
		$x .= '<br>';
		$x .= $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<div style="height:300px;">'."\n";
		$x .= '<table id="CategoryTable" width="100%" border="0" cellspacing="0" cellpadding="2" class="common_table_list_v30">'."\n";
			$x .= '<col>';
			$x .= '<col width="30%">';
			$x .= '<col width="30%">';
			$x .= '<col width="30px">';
			$x .= '<col width="'.$this->toolCellWidth.'px">';

			$x .= '<thead>'."\n";
				$x .= '<tr class="nodrag nodrop">'."\n";
					$x .= '<th>'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Code'].'</th>';
					$x .= '<th>'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['EnglishName'].'</th>';
					$x .= '<th>'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ChineseName'].'</th>';
					$x .= '<th>&nbsp;</th>';
					$x .= '<th>&nbsp;</th>';
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			$x .= '<tbody>'."\n";
			
				# Category Row
				$onMouseEvent = ' onmouseover="Show_Edit_Background(this)" onmouseout="Hide_Edit_Background(this)" ';
				$MoveLink = $this->GET_LNK_MOVE("javascript:void(0);", $Lang['Btn']['Move']);
				foreach($EcaCatArr as $EcaCat)
				{
					
					$thisEcaCatID = $EcaCat['EcaCategoryID'];
					$EditableCodeID = 'EcaCategoryCode_'.$thisEcaCatID;
					$EditableNameEnID = 'EcaCategoryNameEn_'.$thisEcaCatID;
					$EditableNameChID = 'EcaCategoryNameCh_'.$thisEcaCatID;
					
					$SubCatExist = $EcaCat['NumOfSubCategory']==0?0:1;
						$DeleteLink = '<a href="javascript:void(0)" onclick="DeleteCategory(\''.$thisEcaCatID.'\',\''.$SubCatExist.'\')" class="delete_dim" title="'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['DeleteCategory'].'"></a>';
					
					$x .= '<tr id="cat_row_'.$thisEcaCatID.'">'."\n";
						$x .= '<td><div id="'.$EditableCodeID.'" class="edit" '.$onMouseEvent.'>'.intranet_htmlspecialchars($EcaCat['EcaCategoryCode']).'</div></td>'."\n";
						$x .= '<td><div id="'.$EditableNameEnID.'" class="edit" '.$onMouseEvent.'>'.intranet_htmlspecialchars($EcaCat['EcaCategoryNameEn']).'</div></td>'."\n";
						$x .= '<td><div id="'.$EditableNameChID.'" class="edit" '.$onMouseEvent.'>'.intranet_htmlspecialchars($EcaCat['EcaCategoryNameCh']).'</div></td>'."\n";
						$x .= '<td class="Draggable">'.$MoveLink.'</td>'."\n";
						$x .= '<td>';
							$x .= '<div class="table_row_tool row_content_tool">';
								$x .= $DeleteLink;
							$x .= '</div>';
							
							# hidden field
							$x .= '<input type="hidden" class="EcaCategoryID" name="EcaCategoryID[]" value="'.$thisEcaCatID.'"> ';
						$x .= '</td>'."\n";		
					$x .= '</tr>'."\n";
					
					$DisplayOrder[] = $EcaCat['DisplayOrder'];
				}			
			
				# Add Cat Row
				$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['AddCategory'];
				$thisOnclick="js_Add_Category()";
				$ThickBoxLink = $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, 'add_dim', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
				$x .= '<tr class="nodrag nodrop add_row">'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>';
						$x .= '<div class="table_row_tool row_content_tool">';
							$x .= $ThickBoxLink;
						$x .= '</div>';
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";	
		$x .= '</div>';
		
		# Btns
		$CloseBtn = $this->Get_Action_Btn($Lang['Btn']['Close'],"Button","js_Hide_ThickBox()");
		$x .= '<div class="edit_bottom_v30">'."\n";
			$x .= $CloseBtn;
		$x .= '</div>'."\n";
		$MaxDisplayOrder = is_array($DisplayOrder)?max($DisplayOrder):0;
		$x .= '<input type="hidden" id="MaxDisplayOrder" value="'.$MaxDisplayOrder.'">';
		
		return $x;
		
	}
	
	function Get_Setting_ECA_Category_Edit_Layer_New_Row()
	{
		global $Lang;
		
		$SaveBtn = $this->GET_SMALL_BTN($Lang['Btn']['Save'],"button","js_Validate_Category()");
		$CancelBtn = $this->GET_SMALL_BTN($Lang['Btn']['Cancel'],"button","js_Cancel_Add_Category()");
		$x .= '<tr class="new_row">';
			$x .= '<td>';
				$x .= '<input id="EcaCategoryCode" name="EcaCategoryCode" class="textboxtext required">';
				$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnEcaCategoryCode", $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['Thickbox']['EcaCategoryCode']);
			$x .= '</td>';
			$x .= '<td>';
				$x .= '<input id="EcaCategoryNameEn" name="EcaCategoryNameEn" class="textboxtext required">';
				$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnEcaCategoryNameEn",$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['Thickbox']['EcaCategoryNameEn']);
			$x .= '</td>';
			$x .= '<td>';
				$x .= '<input id="EcaCategoryNameCh" name="EcaCategoryNameCh" class="textboxtext required">';
				$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnEcaCategoryNameCh",$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['Thickbox']['EcaCategoryNameCh']);
			$x .= '</td>';
			$x .= '<td>';
				$x .= '&nbsp;';
			$x .= '</td>';
			$x .= '<td>';
				$x .= $SaveBtn;
				$x .= $CancelBtn;
			$x .= '</td>';
		$x .= '</tr>';
		
		$x = str_replace("'","\'",$x);
		return $x;
	}
	
	function Get_Setting_ECA_SubCategory_Edit_Layer($CategoryID,$SubCategoryID='')
	{
		global $Lang, $PATH_WRT_ROOT, $eRC_Rubrics_ConfigArr;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
		$lreportcard_eca = new libreportcardrubrics_eca(); 	
		
		# Get Btns
		$SubmitBtn = $this->Get_Action_Btn($Lang['Btn']['Submit'],"submit");
		$SubmitAndContinueBtn = $this->Get_Action_Btn($Lang['Btn']['Submit&AddMore'],"Button","CheckTBForm(1)");
		$CloseBtn = $this->Get_Action_Btn($Lang['Btn']['Close'],"Button","js_Hide_ThickBox()");
		
		# Sub Category Info
		if(trim($SubCategoryID)!='')
			$SubCatArr = $lreportcard_eca->Get_ECA_SubCategory_Info($CategoryID,$SubCategoryID);
		
		# Get Max Length Info
		$LengthInfo = $eRC_Rubrics_ConfigArr['MaxLength']['ECACategory'];
		
		$x .= '';
		$x .= '<form id="TBForm" name="TBForm" onsubmit="CheckTBForm(); return false;">'."\n";
			$x .= '<div style="height:350px;">'."\n";
			$x .= $this->Get_Thickbox_Return_Message_Layer();
			$x .= '<table width="100%" cellpadding=10px cellspacing=10px align="center" >'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td valign="top">'."\n";	
						$x .= '<table width="100%" class="form_table_v30" cellpadding="2" >'."\n";
							$x .= '<col class="field_title">';
							$x .= '<col class="field_c">';
							
							# Code
							$thisValue = intranet_htmlspecialchars($SubCatArr[0]["EcaSubCategoryCode"]);
							$MaxLength =$LengthInfo['Code'];
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Code'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<input name="EcaCategoryCode" id="EcaCategoryCode" class="textbox_name required" value="'.$thisValue.'" maxlength="'.$MaxLength.'">'."\n";
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnEcaCategoryCode");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							# English Name
							$thisValue = intranet_htmlspecialchars($SubCatArr[0]["EcaSubCategoryNameEn"]);
							$MaxLength =$LengthInfo['NameEn'];
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title" rowspan="2"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['CategoryName'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['En'].'</span>'."\n";	
									$x .= '<span class="row_content_v30">'."\n";
										$x .= '<input name="EcaCategoryNameEn" id="EcaCategoryNameEn" class="textbox_name required" value="'.$thisValue.'" maxlength="'.$MaxLength.'">'."\n";
										$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnEcaCategoryNameEn");
									$x .= '</span>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							# Chinese Name
							$thisValue = intranet_htmlspecialchars($SubCatArr[0]["EcaSubCategoryNameCh"]);
							$MaxLength = $LengthInfo['NameCh'];
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['Ch'].'</span>'."\n";	
									$x .= '<span class="row_content_v30">'."\n";
										$x .= '<input name="EcaCategoryNameCh" id="EcaCategoryNameCh" class="textbox_name required" value="'.$thisValue.'" maxlength="'.$MaxLength.'">'."\n";
										$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnEcaCategoryNameCh");
									$x .= '</span>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '</div>'."\n";
			$x .= $this->MandatoryField();		
			
			# Btns
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SubmitBtn;
				$x .= "&nbsp;".$SubmitAndContinueBtn;
				$x .= "&nbsp;".$CloseBtn;
			$x .= '</div>'."\n";
		
		# hidden field
		$x .= '<input type="hidden" name="EcaCategoryID" id="EcaCategoryID" value="'.$CategoryID.'">'."\n";
		$x .= '<input type="hidden" name="EcaSubCategoryID" id="EcaSubCategoryID" value="'.$SubCategoryID.'">'."\n";
		
		$x .= '</form>';		
		$x .= $this->FOCUS_ON_LOAD("TBForm.EcaCategoryCode");
		return $x;		
	}

	function Get_Setting_ECA_Item_UI($EcaCategoryID='', $EcaSubCategoryID='')
	{
		global $Lang;
		
		# get selection 
		$CategorySelection = $this->Get_ECA_Category_Selection("EcaCategoryID",$EcaCategoryID,$Onchange='js_Reload_Cat_Selection();', $noFirst=1, $isAll=0);
		$SubCategorySelection = $this->Get_ECA_SubCategory_Selection("EcaSubCategoryID",$EcaCategoryID,$EcaSubCategoryID,$Onchange='js_Reload_Item_Table();', $noFirst=0, $isAll=1);
		
		# navaigtion
	 	$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Category'], "eca.php");
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Item'], "");
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post" onsubmit="CheckForm(); return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
						$x .= '<br><br>';
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<span id="CategorySelectionSpan">'."\n";
							$x .= $CategorySelection."\n";
						$x .= '</span>'."\n";
						$x .= '<span id="SubCategorySelectionSpan">'."\n";
							$x .= $SubCategorySelection."\n";
						$x .= '</span>'."\n";  
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";		
					$x .= '<td id="ItemTableTD">'."\n";
						$x .= '<!-- load by ajax -->'."\n";	 
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";
		
		return $x;	
	}
	
	function Get_Setting_ECA_Item_Table($EcaCategoryID='', $EcaSubCategoryID='')
	{
		global $PATH_WRT_ROOT, $Lang;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
		$lreportcard_eca = new libreportcardrubrics_eca(); 	
		
		$CatItemMap = $lreportcard_eca->Get_ECA_Category_Item_Info($EcaCategoryID,$EcaSubCategoryID);
		$CatItemMap = BuildMultiKeyAssoc($CatItemMap,array("EcaSubCategoryID","EcaItemID"));
		$SubCatInfo = $lreportcard_eca->Get_ECA_SubCategory_Info($EcaCategoryID);
		$SubCatInfo = BuildMultiKeyAssoc($SubCatInfo,array("EcaSubCategoryID"));

		$x .= '<table id="ItemTable" width="100%" border="0" cellspacing="0" cellpadding="2" class="common_table_list_v30">'."\n";
			$x .= '<col width="30%">';
			$x .= '<col width="30%">';
			$x .= '<col>';
			$x .= '<col width="30px" align="center">';
			$x .= '<col width="'.$this->toolCellWidth.'px">';

			$x .= '<thead>'."\n";
				$x .= '<tr class="nodrag nodrop">'."\n";
					$x .= '<th>';
							$x .= $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Subcategory'];
					$x .= '</th>'."\n";
					$x .= '<th class="sub_row_top" colspan="5">'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Item'].'</th>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr class="nodrag nodrop">'."\n";
					$x .= '<th>'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Name'].'&nbsp;</th>'."\n";
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Name'].'&nbsp;</th>'."\n";
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Code'].'&nbsp;</th>'."\n";
					$x .= '<th class="sub_row_top">&nbsp;</th>'."\n";
					$x .= '<th class="sub_row_top">&nbsp;</th>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			foreach($CatItemMap as $SubCategoryID => $ItemArr)
			{
				$x .= '<tbody class="tbody'.$SubCategoryID.'">'."\n";

					$SubCat = $SubCatInfo[$SubCategoryID];
					$SubCategoryName = Get_Lang_Selection($SubCat['EcaSubCategoryNameCh'],$SubCat['EcaCategoryNameEn']);
					$SubCategoryName = intranet_htmlspecialchars($SubCategoryName);
					
					$NumOfItem = count(array_filter(array_keys((array)$ItemArr)));
					
					if($NumOfItem>0)
						$RowSpan =  'rowspan="'.($NumOfItem+2).'"';
					
					# Add Sub Cat Btn 
					$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['AddItem'];
					$thisOnclick="js_Add_Item('$SubCategoryID')";
					$ThickBoxLink = $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, 'add_dim', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
					
					$x .= '<tr>'."\n";
						$x .= '<td '.$RowSpan.'>'.$SubCategoryName.'</td>'."\n";
						# if no sub cat, add row btn and cat name should be in the same row
						if($NumOfItem==0) 
						{
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>';
								$x .= '<div class="table_row_tool row_content_tool">';
									$x .= $ThickBoxLink;
								$x .= '</div>';
							$x .= '</td>'."\n";	
						}
					$x .= '</tr>'."\n";
					
					$DisplayOrder = array(0);
					if($NumOfItem>0)
					{
						$DisplayOrder = array(0);
						$MoveLink = $this->GET_LNK_MOVE("javascript:void(0);", $Lang['Btn']['Move']);
						foreach((array)$ItemArr as $ItemID => $ItemInfo)
						{
							$ItemName = Get_Lang_Selection($ItemInfo['EcaItemNameCh'],$ItemInfo['EcaItemNameEn']);
							$ItemName = intranet_htmlspecialchars($ItemName);
							$ItemCode = intranet_htmlspecialchars($ItemInfo['EcaItemCode']);
							
							$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['EditItem'];
							$thisOnclick="js_Edit_Item('$SubCategoryID','$ItemID')";
							$ThickBoxEditLink = $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, 'edit_dim', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content=""); 
							$DeleteLink = '<a href="javascript:void(0)" onclick="DeleteItem(\''.$ItemID.'\')" class="delete_dim" title="'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['ToolTipArr']['DeleteItem'].'"></a>';
							
							$x .= '<tr class="sub_row">'."\n";
								$x .= '<td>'.$ItemName.'</td>'."\n";
								$x .= '<td>'.$ItemCode.'</td>'."\n";
								$x .= '<td class="Draggable" align="center">'.$MoveLink.'</td>'."\n";
								$x .= '<td>';
									# hidden field
									$x .= '<input type="hidden" class="EcaItemID" name="EcaItemID[]" value="'.$ItemID.'"> ';
								
									$x .= '<div class="table_row_tool row_content_tool" >';
										$x .= $ThickBoxEditLink;
										$x .= $DeleteLink;
									$x .= '</div>';
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
							
							$DisplayOrder[] = $ItemInfo["DisplayOrder"];
						}
					}
	
					# Add Cat Row
					# if sub cat exist, add row btn and cat name should be placed under sub cat info
					if($NumOfItem>0) 
					{
						$x .= '<tr class="nodrag nodrop">'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>';
								$x .= '<div class="table_row_tool row_content_tool">';
									$x .= $ThickBoxLink;
								$x .= '</div>';
							$x .= '</td>'."\n";		
						$x .= '</tr>'."\n";
					}
					
					# MaxDisplatOrder	
					$MaxDisplayOrder = max($DisplayOrder);
					$x .= '<input type="hidden" id="MaxDisplayOrder_'.$SubCategoryID.'" name="MaxDisplayOrder_'.$SubCategoryID.'" value="'.$MaxDisplayOrder.'">';
					
					$x .= '</tbody>';
			}
			
			
			
		return $x;
				
	}
	
	function Get_Setting_ECA_Item_Edit_Layer($SubCategoryID,$ItemID='')
	{
		global $Lang, $PATH_WRT_ROOT, $eRC_Rubrics_ConfigArr;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
		$lreportcard_eca = new libreportcardrubrics_eca(); 	
		
		# Get Btns
		$SubmitBtn = $this->Get_Action_Btn($Lang['Btn']['Submit'],"submit");
		$SubmitAndContinueBtn = $this->Get_Action_Btn($Lang['Btn']['Submit&AddMore'],"Button","CheckTBForm(1)");
		$CloseBtn = $this->Get_Action_Btn($Lang['Btn']['Close'],"Button","js_Hide_ThickBox()");
		
		# Sub Category Info
		if(trim($ItemID)!='')
			$ItemArr = $lreportcard_eca->Get_ECA_Category_Item_Info('','',$ItemID);
		
		# Get Max Length Info
		$LengthInfo = $eRC_Rubrics_ConfigArr['MaxLength']['ECACategory'];
		
		$x .= '';
		$x .= '<form id="TBForm" name="TBForm" onsubmit="CheckTBForm(); return false;">'."\n";
			$x .= '<div style="height:350px;">'."\n";
			$x .= $this->Get_Thickbox_Return_Message_Layer();
			$x .= '<table width="100%" cellpadding=10px cellspacing=10px align="center">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td valign="top">'."\n";	
						$x .= '<table width="100%" class="form_table_v30" cellpadding="2">'."\n";
							
							$x .= '<col class="field_title">'."\n";
							$x .= '<col class="field_c">'."\n";
							
							# Code
							$thisValue = intranet_htmlspecialchars($ItemArr[0]["EcaItemCode"]);
							$MaxLength =$LengthInfo['Code'];
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['Code'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<input name="EcaItemCode" id="EcaItemCode" class="textbox_name required" value="'.$thisValue.'" maxlength="'.$MaxLength.'">'."\n";
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnEcaItemCode");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							# English Name
							$thisValue = intranet_htmlspecialchars($ItemArr[0]["EcaItemNameEn"]);
							$MaxLength =$LengthInfo['NameEn'];
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title" rowspan="2"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['EnglishName'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['En'].'</span>'."\n";	
									$x .= '<span class="row_content_v30">'."\n";
										$x .= '<input name="EcaItemNameEn" id="EcaItemNameEn" class="textbox_name required" value="'.$thisValue.'" maxlength="'.$MaxLength.'">'."\n";
										$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnEcaItemNameEn");
									$x .= '</span>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							# Chinese Name
							$thisValue = intranet_htmlspecialchars($ItemArr[0]["EcaItemNameCh"]);
							$MaxLength = $LengthInfo['NameCh'];
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['Ch'].'</span>'."\n";	
									$x .= '<span class="row_content_v30">'."\n";
										$x .= '<input name="EcaItemNameCh" id="EcaItemNameCh" class="textbox_name required" value="'.$thisValue.'" maxlength="'.$MaxLength.'">'."\n";
										$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnEcaItemNameCh");
									$x .= '</span>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '</div>'."\n";
			$x .= $this->MandatoryField();		
			
			# Btns
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SubmitBtn;
				$x .= "&nbsp;".$SubmitAndContinueBtn;
				$x .= "&nbsp;".$CloseBtn;
			$x .= '</div>'."\n";
		
		# hidden field
		$x .= '<input type="hidden" name="TBEcaItemID" id="TBEcaItemID" value="'.$ItemID.'">'."\n";
		$x .= '<input type="hidden" name="TBEcaSubCategoryID" id="TBEcaSubCategoryID" value="'.$SubCategoryID.'">'."\n";
		
		$x .= '</form>';	
		$x .= $this->FOCUS_ON_LOAD("TBForm.EcaItemCode");
			
		return $x;		
	}
	
	function Get_ECA_Category_Selection($ID_Name, $Selected='', $Onchange='', $noFirst=1, $isAll=0)
	{
		global $PATH_WRT_ROOT,$Lang;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
		$lreportcard_eca = new libreportcardrubrics_eca(); 
		
		$CatInfo = $lreportcard_eca->Get_ECA_Category_Info();
		
		$CategoryNameField = Get_Lang_Selection("EcaCategoryNameCh","EcaCategoryNameEn"); 
		$OptionArr = BuildMultiKeyAssoc($CatInfo,"EcaCategoryID",$CategoryNameField,1);
		
		$onchange = '';
		if ($Onchange != "")
			$onchange = 'onchange="'.$Onchange.'"';
		
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange;
		
		$AllOption = $isAll==1?$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['SelectOption']['AllCategory']:0;
		
		$Selection = getSelectByAssoArray($OptionArr,$selectionTags,$Selected,$Onchange,$noFirst,$AllOption);
		
		return $Selection;
	}
	
	
	function Get_ECA_SubCategory_Selection($ID_Name, $EcaCategoryID='', $SelectedEcaSubCategoryID='', $Onchange='', $noFirst=1, $isAll=0)
	{
		global $PATH_WRT_ROOT, $Lang;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
		$lreportcard_eca = new libreportcardrubrics_eca(); 
		
		$SubCatInfo = $lreportcard_eca->Get_ECA_SubCategory_Info($EcaCategoryID);
		
		$CategoryNameField = Get_Lang_Selection("EcaSubCategoryNameCh","EcaSubCategoryNameEn"); 
		$OptionArr = BuildMultiKeyAssoc($SubCatInfo,"EcaSubCategoryID",$CategoryNameField,1);
		
		$onchange = '';
		if ($Onchange != "")
			$onchange = 'onchange="'.$Onchange.'"';
		
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange;
		
		$AllOption = $isAll==1?$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['SelectOption']['AllSubCategory']:0;
		
		$Selection = getSelectByAssoArray($OptionArr,$selectionTags,$SelectedEcaSubCategoryID,$Onchange,$noFirst,$AllOption);
		
		return $Selection;
	}
#	
# Setting > ECA Setting UI End                                               
############################################################################################################

	function Get_Reports_ReportGeneration_UI($SettingsID='')
	{
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $Lang, $eRC_Rubrics_ConfigArr, $sys_custom;
		global $lreportcard;
		
		//include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		//include_once($PATH_WRT_ROOT.'includes/form_class_manage_ui.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
		//$fcm_ui = new form_class_manage_ui();
		$scm_ui = new subject_class_mapping_ui();
		
		if ($SettingsID != '')
		{
			$SettingsInfoArr = $lreportcard->Get_Report_Template_Settings($SettingsID);
			$SettingsName = Get_Lang_Selection($SettingsInfoArr[0]['SettingsNameCh'], $SettingsInfoArr[0]['SettingsNameEn']);
			$SettingsValue = $SettingsInfoArr[0]['SettingsValue'];
			$SettingsArr = unserialize($SettingsValue);
		}
		
		
		$x .= '<form id="form1" name="form1" method="POST" target="_blank" action="print.php">';
			$x .= '<div class="table_board">'."\n";
				$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td style="text-align:right;">'."\n";
							$x .= $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, 'tablelink', $Title=$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportCardTemplateSettings'], 'js_Reload_Settings_Layer();', $InlineID="FakeLayer", $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportCardTemplateSettings'], $LinkID='ShowSettingsLayerLink');
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'."\n";
							$x .= '<table class="form_table_v30">'."\n";
								if ($SettingsID != '')
								{
									### Settings Name
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SettingsName'].'</td>'."\n";
										$x .= '<td>'.$SettingsName.'</td>';
									$x .= '</tr>'."\n";
								}
								### Report Template Selection
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.'<span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['ReportCardTemplateArr']['MenuTitle'].'</td>'."\n";
									$x .= '<td>'.$this->Get_Report_Template_Selection('ReportID', $SettingsArr['ReportID'], 'js_Changed_Report_Selection(this.value);').'</td>';
								$x .= '</tr>'."\n";
							$x .= '</table>'."\n";
							$x .= '<br style="clear:both;" />'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
					$x .= '<tr>'."\n";
						$x .= '<td>'."\n";
						
							### Student Display Options
							# Form Stage Selection
							$thisOnChange = 'js_Changed_Form_Stage_Selection();';
							$FormStageSelection = $this->Get_Form_Stage_Selection('FormStageIDArr[]', $SettingsArr['FormStageID'], $thisOnChange, $noFirst=1, $isAll=0, $firstTitle='', $CheckClassTeacher=1, $isMultiple=1);
							
							# Form Selection
							//$thisOnChange = 'js_Changed_Form_Selection(this.value);';
							//$FormSelection = $fcm_ui->Get_Form_Selection('YearID', $SettingsArr['YearID'], $thisOnChange, $noFirst=1, $isAll=0);
							//$CheckClassTeacher = ($lreportcard->Is_Admin_User($_SESSION['UserID']))? false : true;
							//$FormSelection = $this->Get_Form_Selection('YearID', $SettingsArr['YearID'], $thisOnChange, $noFirst=1, $isAll=0, $firstTitle='', $CheckClassTeacher=1);
							
							# Select All Form Stage Btn
							$SelectAllFormStageBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('FormStageIDArr[]', 1, 'js_Changed_Form_Stage_Selection();');");
							# Select All Form Btn
							$SelectAllFormBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('YearIDArr[]', 1, 'js_Changed_Form_Selection();');");
							# Select All Class Btn
							$SelectAllClassBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('YearClassIDArr[]', 1, 'js_Changed_Class_Selection();');");
							# Select All Student Btn
							$SelectAllStudentBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentIDArr[]', 1);");
							
							# Student Subject Selection
							$StudentSubjectSelection = $scm_ui->Get_Subject_Selection("StudentSubjectIDArr[]", '', $OnChange='js_Changed_Student_Subject_Selection();', $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=1);
							
							# Select All Term Btn
							$SelectAllStudentYearTermBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentYearTermIDArr[]', 1, 'js_Changed_Student_Year_Term_Selection();')");
							# Select All Subject Btn
							$SelectAllStudentSubjectBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_With_OptGroup('StudentSubjectIDArr[]', 1, 'js_Changed_Student_Subject_Selection();')");
							# Select All Subject Group Btn
							$SelectAllStudentSubjectGroupBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentSubjectGroupIDArr[]', 1, 'js_Changed_Student_Subject_Group_Selection();')");

							if($sys_custom['UserExtraInformation'])
							{
								$ExtraInfoSelection = $this->Get_Extra_Info_Selection("ExtraItemIDArr[]",'',$OnChange="js_Changed_Student_Extra_Info_Selection()");
								$SelectAllExtraInfoBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_With_OptGroup('ExtraItemIDArr[]', 1, 'js_Changed_Student_Extra_Info_Selection();')");
							}
		
							$TableName = 'StudentSettings';
							$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['Identity']['Student']);
							$x .= '<table id="OptionTable_'.$TableName.'" class="form_table_v30">'."\n";
								# Select Students by Class / Subject Group
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SelectFrom'].'</td>'."\n";
									$x .= '<td>'."\n";
										// Class
										$thisChecked = (!isset($SettingsArr) || $SettingsArr['SelectStudentFrom'] == 'Class')? 'checked' : '';
										$thisDisplay = $Lang['SysMgr']['FormClassMapping']['Class'];
										$thisOnClick = "js_Changed_Student_Selection_Source('Class');";
										$x .= $this->Get_Radio_Button('SelectStudentFrom_Class', 'SelectStudentFrom', 'Class', $thisChecked, $Class="", $thisDisplay, $thisOnClick);
										// Subject Group
										$thisChecked = (isset($SettingsArr) && $SettingsArr['SelectStudentFrom'] == 'SubjectGroup')? 'checked' : '';
										$thisDisplay = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
										$thisOnClick = "js_Changed_Student_Selection_Source('SubjectGroup');";
										$x .= $this->Get_Radio_Button('SelectStudentFrom_SubjectGroup', 'SelectStudentFrom', 'SubjectGroup', $thisChecked, $Class="", $thisDisplay, $thisOnClick);
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								# Form Stage Selection
								$x .= '<tr class="Student_FormClassTr">'."\n";
									$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['FormStage'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= $this->Get_MultipleSelection_And_SelectAll_Div('FormStageSelectionSpan', $FormStageSelection, $SelectAllFormStageBtn);
										$x .= $this->Get_Warning_Message_Div('SelectFormStageWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['FormStage']);
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";								
								# Form Selection
								$x .= '<tr class="Student_FormClassTr">'."\n";
									$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['Form'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= $this->Get_MultipleSelection_And_SelectAll_Div('FormSelectionSpan', $SelectionHTML='', $SelectAllFormBtn);
										$x .= $this->Get_Warning_Message_Div('SelectFormWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Form']);
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								# Class Selection
								$x .= '<tr class="Student_FormClassTr">'."\n";
									$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= $this->Get_MultipleSelection_And_SelectAll_Div('ClassSelectionSpan', $SelectionHTML='', $SelectAllClassBtn);
										$x .= $this->Get_Warning_Message_Div('SelectClassWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Class']);
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								
								# Term Selection
								$x .= '<tr class="Student_SubjectGroupTr">'."\n";
									$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['General']['Term'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentYearTermSelectionSpan', $SelectionHTML='', $SelectAllStudentYearTermBtn);
										$x .= $this->Get_Warning_Message_Div('SelectStudentYearTermWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Term']);
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								# Subject Selection
								$x .= '<tr class="Student_SubjectGroupTr">'."\n";
									$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentSubjectSelectionSpan', $StudentSubjectSelection, $SelectAllStudentSubjectBtn);
										$x .= $this->Get_Warning_Message_Div('SelectStudentSubjectWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Subject']);
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";								
								# Subject Group Selection
								$x .= '<tr class="Student_SubjectGroupTr">'."\n";
									$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentSubjectGroupSelectionSpan', $SelectionHTML='', $SelectAllStudentSubjectGroupBtn);
										$x .= $this->Get_Warning_Message_Div('SelectStudentSubjectGroupWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['SubjectGroup']);
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								
								
								if($sys_custom['UserExtraInformation'])
								{
									# Extra INfo Selection
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['AccountMgmt']['UserExtraInfo'].'</td>'."\n";
										$x .= '<td>'."\n";
											$x .= '<div>'."\n";
												$x .= '<span id="ExtraInfoSpan">'.$ExtraInfoSelection.'</span>'.$SelectAllExtraInfoBtn."\n";
												$x .= '<br />'."\n";
												$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
											$x .= '</div>'."\n";
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
								}		
								# Student Selection
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['Identity']['Student'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentSelectionSpan', $SelectionHTML='', $SelectAllStudentBtn);
										$x .= $this->Get_Warning_Message_Div('SelectStudentWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Student']);
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";								
							$x .= '</table>'."\n";
							$x .= '<br style="clear:both;" />'."\n";
							$x .= '<br style="clear:both;" />'."\n";
							
							
							### Subject Display Options
							# Subject Selection
							$SubjectSelection = $scm_ui->Get_Subject_Selection("SubjectIDArr[]", '', $OnChange='', $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=1);
							# Select All Subject Btn
							$SelectAllSubjectBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_With_OptGroup('SubjectIDArr[]')");
							
							# Curriculum Remarks
							$MaxLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
							$LastTopicLevelName = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$MaxLevel]['Title'];
							$CurriculumRemarks = str_replace('<!--TopicLevelName-->', $LastTopicLevelName, $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['LastTopicLevelIsAlwaysDisplay']);
							$ModuleReportLastLevelWarningMsg = str_replace('<!--TopicLevelName-->', $LastTopicLevelName, $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['WarningArr']['ModuleReportMustSelectLastTopicLevel']);
							$ConsolidatedReportLastLevelWarningMsg = str_replace('<!--TopicLevelName-->', $LastTopicLevelName, $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['WarningArr']['ModuleMarkConsolidatedReportMustSelectLastTopicLevel']);
							
							$TableName = 'SubjectSettings';
							$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['SysMgr']['SubjectClassMapping']['Subject']);
							$x .= '<table id="OptionTable_'.$TableName.'" class="form_table_v30">'."\n";
								# Subject Selection
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= $this->Get_MultipleSelection_And_SelectAll_Div('', $SubjectSelection, $SelectAllSubjectBtn);
										$x .= $this->Get_Warning_Message_Div('SelectSubjectWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Subject']);
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								# Subject Name Display
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SubjectName'].'</td>'."\n";
									$x .= '<td>'."\n";
										$thisChecked = 'checked';
										foreach ($Lang['eRC_Rubrics']['GeneralArr']['SubjectDisplayArr'] as $thisDisplayType => $thisTypeName)
										{
											$thisChecked = ($SettingsArr['SubjectDisplay'] == $thisDisplayType)? 'checked' : $thisChecked;
											$x .= '<input type="radio" id="SubjectDisplay_'.$thisDisplayType.'" name="SubjectDisplay" value="'.$thisDisplayType.'" '.$thisChecked.'>';
											$x .= '<label for="SubjectDisplay_'.$thisDisplayType.'">'.$thisTypeName.'</label>&nbsp;'."\n";
											
											$thisChecked = '';
										}
										
										// Hidden Subject Name
										$x .= '<input type="radio" id="SubjectDisplay_Hidden" name="SubjectDisplay" value="Hidden">';
										$x .= '<label for="SubjectDisplay_Hidden">'.$Lang['General']['Hide'].'</label>&nbsp;'."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								# Subject Group Name Display
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SubjectGroupName'].'</td>'."\n";
									$x .= '<td>'."\n";
										// Show
										$thisChecked = (!isset($SettingsArr) || $SettingsArr['SubjectGroupDisplay'] == 1)? 'checked' : '';
										$x .= '<input type="radio" id="SubjectGroupDisplay_Display" name="SubjectGroupDisplay" value="1" '.$thisChecked.'>';
										$x .= '<label for="SubjectGroupDisplay_Display">'.$Lang['General']['Show'].'</label>&nbsp;'."\n";
										// Hide
										$thisChecked = (isset($SettingsArr) && $SettingsArr['SubjectGroupDisplay'] == 0)? 'checked' : '';
										$x .= '<input type="radio" id="SubjectGroupDisplay_Hidden" name="SubjectGroupDisplay" value="0" '.$thisChecked.'>';
										$x .= '<label for="SubjectGroupDisplay_Hidden">'.$Lang['General']['Hide'].'</label>&nbsp;'."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								# Curriculum Data Display
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'."\n";
										$x .= '<span class="tabletextrequire">*</span>'."\n";
										$x .= $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['CurriculumData']."\n";
									$x .= '</td>'."\n";
									$x .= '<td>'."\n";
										$x .= $this->Get_Topic_Level_Checkbox_Table($SettingsArr['TopicLevelArr'], $SettingsID);
										$x .= '<div id="SelectTopicLevelWarningDiv" style="display:none;">'."\n";
											$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['TopicLevel'].'</span>'."\n";
										$x .= '</div>'."\n";
										$x .= '<div id="ModuleReportMustHaveLastTopicLevelWarningDiv" style="display:none;">'."\n";
											$x .= '<span class="tabletextrequire">* '.$ModuleReportLastLevelWarningMsg.'</span>'."\n";
										$x .= '</div>'."\n";
										$x .= '<div id="ConsolidatedReportMustHaveLastTopicLevelWarningDiv" style="display:none;">'."\n";
											$x .= '<span class="tabletextrequire">* '.$ConsolidatedReportLastLevelWarningMsg.'</span>'."\n";
										$x .= '</div>'."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								# Curriculum Which Student need not study
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'."\n";
										$x .= $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['CurriculumWhichStudentNeedNotStudy']."\n";
									$x .= '</td>'."\n";
									$x .= '<td>'."\n";
										// Show
										$thisChecked = (!isset($SettingsArr) || $SettingsArr['TopicNeedNotStudyDisplay'] == 1)? 'checked' : '';
										$x .= '<input type="radio" id="TopicNeedNotStudyDisplay_Display" name="TopicNeedNotStudyDisplay" value="1" '.$thisChecked.'>';
										$x .= '<label for="TopicNeedNotStudyDisplay_Display">'.$Lang['General']['Show'].'</label>&nbsp;'."\n";
										// Hide
										$thisChecked = (isset($SettingsArr) && $SettingsArr['TopicNeedNotStudyDisplay'] == 0)? 'checked' : '';
										$x .= '<input type="radio" id="TopicNeedNotStudyDisplay_Hidden" name="TopicNeedNotStudyDisplay" value="0" '.$thisChecked.'>';
										$x .= '<label for="TopicNeedNotStudyDisplay_Hidden">'.$Lang['General']['Hide'].'</label>&nbsp;'."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								# Marksheet Remarks
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'."\n";
										$x .= $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MarksheetRemarks']."\n";
										$x .= '<br />'."\n";
										$x .= '<span class="tabletextremark">('.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ApplicableToModuleReportOnly'].')</span>'."\n";
									$x .= '</td>'."\n";
									$x .= '<td>'."\n";
										// Show
										$thisChecked = (!isset($SettingsArr) || $SettingsArr['MarksheetRemarksDisplay'] == 1)? 'checked' : '';
										$x .= '<input type="radio" id="MarksheetRemarksDisplay_Display" name="MarksheetRemarksDisplay" value="1" class="MarksheetRemarksDisplay" '.$thisChecked.'>';
										$x .= '<label for="MarksheetRemarksDisplay_Display">'.$Lang['General']['Show'].'</label>&nbsp;'."\n";
										// Hide
										$thisChecked = (isset($SettingsArr) && $SettingsArr['MarksheetRemarksDisplay'] == 0)? 'checked' : '';
										$x .= '<input type="radio" id="MarksheetRemarksDisplay_Hidden" name="MarksheetRemarksDisplay" value="0" class="MarksheetRemarksDisplay" '.$thisChecked.'>';
										$x .= '<label for="MarksheetRemarksDisplay_Hidden">'.$Lang['General']['Hide'].'</label>&nbsp;'."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</table>'."\n";
							$x .= '<br style="clear:both;" />'."\n";
							$x .= '<br style="clear:both;" />'."\n";
							
							
							### Mark Display Options
							$x .= '<div id="MarkOptionDiv">'."\n";
								$TableName = 'MarkOption';
								$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['eRC_Rubrics']['GeneralArr']['Mark']);
								$x .= '<table id="OptionTable_'.$TableName.'" class="form_table_v30">'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['HeaderData'].'</td>'."\n";
										$x .= '<td>'."\n";
											$x .= $this->Get_Template_Mark_Display_Type_Options($SettingsArr['MarkDisplayOption']);
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
								$x .= '</table>'."\n";
								$x .= '<br style="clear:both;" />'."\n";
								$x .= '<br style="clear:both;" />'."\n";
							$x .= '</div>'."\n";
							
							
							### Cover Page Options
							$x .= '<div id="CoverPageOptionDiv">'."\n";
								$TableName = 'CoverSettings';
								$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['CoverPage']);
								$x .= '<table id="OptionTable_'.$TableName.'" class="form_table_v30">'."\n";
									# Show / Hide Cover
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ShowCoverPage'].'</td>'."\n";
										$x .= '<td>'."\n";
											// Show
											$thisChecked = (!isset($SettingsArr) || $SettingsArr['DisplayCoverPage'] == 1)? 'checked' : '';
											$x .= $this->Get_Radio_Button($ID="DisplayCoverPage_Show", $Name="DisplayCoverPage", $Value="1", $thisChecked, $Class='', $Lang['General']['Show'], $Onclick='')."\n";
											$x .= '&nbsp;'."\n";
											// Hide
											$thisChecked = (isset($SettingsArr) && $SettingsArr['DisplayCoverPage'] == 0)? 'checked' : '';
											$x .= $this->Get_Radio_Button($ID="DisplayCoverPage_Hide", $Name="DisplayCoverPage", $Value="0", $thisChecked, $Class='', $Lang['General']['Hide'], $Onclick='')."\n";
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									
									# Report Title
									$x .= '<tr>'."\n";
										$x .= '<td id="CoverTitleTd" class="field_title" rowspan="3">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['Title'].'</td>'."\n";
									$x .= '</tr>'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td>'."\n";
											$x .= '<span class="sub_row_content_v30">('.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['Row'].' 1)</span>'."\n";
											$x .= '<span class="row_content_v30">'."\n";
										       	$x .= '<input id="CoverTitle_1" name="CoverTitleArr[]" type="text" class="textbox_name" />'."\n";
										    $x .= '</span>'."\n";
									    $x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									$x .= '<tr id="AddMoreRow_Cover">'."\n";
									    $x .= '<td>'."\n";
									    	$x .= '<div><a href="javascript:js_Add_More_Title_Row(\'Cover\');" class="tablelink">['.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['AddMore'].']</a></div>'."\n";
									    $x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									
									# Student Photo Frame
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['StudentPhotoFrame'].'</td>'."\n";
										$x .= '<td>'."\n";
											// Show
											$thisChecked = (!isset($SettingsArr) || $SettingsArr['DisplayStudentPhotoFrame'] == 1)? 'checked' : '';
											$x .= $this->Get_Radio_Button($ID="StudentPhotoFrame_Show", $Name="DisplayStudentPhotoFrame", $Value="1", $thisChecked, $Class='', $Lang['General']['Show'], $Onclick='')."\n";
											$x .= '&nbsp;'."\n";
											// Hide
											$thisChecked = (isset($SettingsArr) && $SettingsArr['DisplayStudentPhotoFrame'] == 0)? 'checked' : '';
											$x .= $this->Get_Radio_Button($ID="StudentPhotoFrame_Hide", $Name="DisplayStudentPhotoFrame", $Value="0", $thisChecked, $Class='', $Lang['General']['Hide'], $Onclick='')."\n";
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
								$x .= '</table>'."\n";
								$x .= '<br style="clear:both;" />'."\n";
								$x .= '<br style="clear:both;" />'."\n";
							$x .= '</div>'."\n";
							
														
							### Report Display Options
							$TableName = 'ReportSettings';
							$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportCardDisplay']);
							$x .= '<table id="OptionTable_'.$TableName.'" class="form_table_v30">'."\n";
								$x .= '<tbody>'."\n";
									# Report Type
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'."\n";
											$x .= $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportType']."\n";
										$x .= '</td>'."\n";
										$x .= '<td>'."\n";
											// General Report
											$thisChecked = (!isset($SettingsArr['ReportType']) || $SettingsArr['ReportType'] == 'GeneralReport')? 'checked' : '';
											$x .= '<input type="radio" id="ReportType_GeneralReport" name="ReportType" value="GeneralReport" '.$thisChecked.'>';
											$x .= '<label for="ReportType_GeneralReport">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportTypeArr']['GeneralReport'].'</label>&nbsp;'."\n";
											// Therapy Report
											$thisChecked = (isset($SettingsArr['ReportType']) && $SettingsArr['ReportType'] == 'TherapyReport')? 'checked' : '';
											$x .= '<input type="radio" id="ReportType_TherapyReport" name="ReportType" value="TherapyReport" '.$thisChecked.'>';
											$x .= '<label for="ReportType_TherapyReport">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportTypeArr']['TherapyReport'].'</label>&nbsp;'."\n";
											// Learning Report
											$thisChecked = (isset($SettingsArr['ReportType']) && $SettingsArr['ReportType'] == 'LearningReport')? 'checked' : '';
											$x .= '<input type="radio" id="ReportType_LearningReport" name="ReportType" value="LearningReport" '.$thisChecked.'>';
											$x .= '<label for="ReportType_LearningReport">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReportTypeArr']['LearningReport'].'</label>&nbsp;'."\n";
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									
									# School Name
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'."\n";
											$x .= $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SchoolName']."\n";
										$x .= '</td>'."\n";
										$x .= '<td>'."\n";
											// Show
											$thisChecked = (!isset($SettingsArr['SchoolNameDisplay']) || $SettingsArr['SchoolNameDisplay'] == 1)? 'checked' : '';
											$x .= '<input type="radio" id="SchoolNameDisplay_Display" name="SchoolNameDisplay" value="1" '.$thisChecked.'>';
											$x .= '<label for="SchoolNameDisplay_Display">'.$Lang['General']['Show'].'</label>&nbsp;'."\n";
											// Hide
											$thisChecked = (isset($SettingsArr['SchoolNameDisplay']) && $SettingsArr['SchoolNameDisplay'] == 0)? 'checked' : '';
											$x .= '<input type="radio" id="SchoolNameDisplay_Hidden" name="SchoolNameDisplay" value="0" '.$thisChecked.'>';
											$x .= '<label for="SchoolNameDisplay_Hidden">'.$Lang['General']['Hide'].'</label>&nbsp;'."\n";
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									
									# Report Title
									$x .= '<tr>'."\n";
										$x .= '<td id="ReportTitleTd" class="field_title" rowspan="3">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['Title'].'</td>'."\n";
									$x .= '</tr>'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td>'."\n";
											$x .= '<span class="sub_row_content_v30">('.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['Row'].' 1)</span>'."\n";
											$x .= '<span class="row_content_v30">'."\n";
										       	$x .= '<input id="ReportTitle_1" name="ReportTitleArr[]" type="text" class="textbox_name" />'."\n";
										    $x .= '</span>'."\n";
									    $x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									$x .= '<tr id="AddMoreRow_Report">'."\n";
									    $x .= '<td>'."\n";
									    	$x .= '<div><a href="javascript:js_Add_More_Title_Row(\'Report\');" class="tablelink">['.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['AddMore'].']</a></div>'."\n";
									    $x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									
									# Header Data
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['HeaderData'].'</td>'."\n";
										$x .= '<td>'."\n";
											$x .= $this->Get_Template_Header_Data_Info_Checkbox_Table($SettingsArr['HeaderDataFieldOrderedList'], $SettingsArr['HeaderDataFieldArr'], $SettingsID);
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									
									# Header Data Per Row
									$thisValue = ($SettingsArr['NumOfHeaderDataPerRow']=='')? 2 : $SettingsArr['NumOfHeaderDataPerRow'];
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['NumOfHeaderDataEachRow'].'</td>'."\n";
										$x .= '<td>'."\n";
											$x .= $this->GET_TEXTBOX_NUMBER('', 'NumOfHeaderDataPerRow', $thisValue);
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									
									# ECA
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['MenuTitle'].'</td>'."\n";
										$x .= '<td>'."\n";
											$x .= $this->Get_ECA_Checkbox_Table($SettingsArr['ECACategoryIDArr'], $SettingsID);
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									
									# Other Info
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['MenuTitle'].'</td>'."\n";
										$x .= '<td>'."\n";
											$x .= $this->Get_Other_Info_Checkbox_Table($SettingsArr['OtherInfoCategoryOrderedList'], $SettingsArr['OtherInfoItemOrderedList'], $SettingsArr['OtherInfoItemIDArr'], $SettingsArr['SelectAll_OtherInfoChk'], $SettingsID);
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									
									# Class Teacher Comment
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['MenuTitle'].'</td>'."\n";
										$x .= '<td>'."\n";
											// Show
											$thisChecked = (!isset($SettingsArr) || $SettingsArr['DisplayClassTeacherComment'] == 1)? 'checked' : '';
											$x .= $this->Get_Radio_Button($ID="ClassTeacherComment_Show", $Name="DisplayClassTeacherComment", $Value="1", $thisChecked, $Class='', $Lang['General']['Show'], $Onclick='')."\n";
											$x .= '&nbsp;'."\n";
											// Hide
											$thisChecked = (isset($SettingsArr) && $SettingsArr['DisplayClassTeacherComment'] == 0)? 'checked' : '';
											$x .= $this->Get_Radio_Button($ID="ClassTeacherComment_Hide", $Name="DisplayClassTeacherComment", $Value="0", $thisChecked, $Class='', $Lang['General']['Hide'], $Onclick='')."\n";
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									
									# Signature
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['Signature'].'</td>'."\n";
										$x .= '<td>'."\n";
											$x .= $this->Get_Template_Signature_Checkbox_Table($SettingsArr['SignatureFieldOrderedList'], $SettingsArr['SignatureFieldArr'], $SettingsID);
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									
									
								$x .= '</tbody>'."\n";
							$x .= '</table>'."\n";
							$x .= '<br style="clear:both;" />'."\n";
							
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
			$x .= '</div>'."\n";
			
			$x .= $this->MandatoryField()."\n";
			
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= '<p class="spacer"></p>'."\n";
				$x .= $this->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "js_Check_Form();", "submitBtn");
				$x .= '&nbsp;'."\n";
				if ($SettingsID != '')
				{
					$x .= $this->GET_ACTION_BTN($Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SettingsArr']['Save'], "button", "js_Save_Settings('$SettingsID', 1);", "submitBtn");
					$x .= '&nbsp;'."\n";
				}
				$x .= $this->GET_ACTION_BTN($Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SettingsArr']['SaveAs'], "button", "js_Save_Settings_As();", "submitBtn");
				$x .= '<p class="spacer"></p>'."\n";
			$x .= '</div>'."\n";
			
			$x .= '<input type="hidden" id="NumOfTitleRow_Cover" name="NumOfTitleRow_Cover" value="1" />'."\n"; 
			$x .= '<input type="hidden" id="NumOfTitleRow_Report" name="NumOfTitleRow_Report" value="1" />'."\n"; 
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";

		return $x;
	}
	
	function Get_Table_Show_Hide_Navigation($TableName, $SectionTitle)
	{
		global $Lang;
		
		$x = '';
		$x .= $this->GET_NAVIGATION2_IP25($SectionTitle)."&nbsp;&nbsp;";
		$x .= '<span id="spanShowOption_'.$TableName.'" style="display:none">'."\n";
			$x .= $this->Get_Show_Option_Link("javascript:js_Show_Option_Table('$TableName');");
		$x .= '</span>'."\n";
		$x .= '<span id="spanHideOption_'.$TableName.'">'."\n";
			$x .= $this->Get_Hide_Option_Link("javascript:js_Hide_Option_Table('$TableName');");
		$x .= '</span>'."\n";
		
		return $x;
	}
	
	function Get_Reports_ReportGeneration_Settings_Layer()
	{
		$x = "";
		$x .= '<div id="debugArea"></div>';
		
		$x .= '<div class="edit_pop_board" style="height:410px;">';
			$x .= $this->Get_Thickbox_Return_Message_Layer();
			$x .= '<div id="LevelSettingLayer" class="edit_pop_board_write" style="height:370px;">';
				$x .= $this->Get_Reports_ReportGeneration_Settings_Layer_Table();
			$x .= '</div>';
		$x .= '</div>';
		
		return $x;
	}
	
	function Get_Reports_ReportGeneration_Settings_Layer_Table()
	{
		global $Lang, $lreportcard;
		
		$SettingsInfoArr = $lreportcard->Get_Report_Template_Settings();
		$numOfSettings = count($SettingsInfoArr);
		
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();");
		
		$x = '';
		$x .= '<br style="clear:both;" />'."\n";
		
		### Content Table
		$x .= '<table id="SettingsTable" class="common_table_list_v30">';
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th style="width:15%">'.$Lang['eRC_Rubrics']['GeneralArr']['Code'].'</th>';
					$x .= '<th style="width:32%">'.$Lang['eRC_Rubrics']['GeneralArr']['NameEn'].'</th>';
					$x .= '<th style="width:32%">'.$Lang['eRC_Rubrics']['GeneralArr']['NameCh'].'</th>';
					$x .= '<th style="width:5%">&nbsp;</th>';
					$x .= '<th style="width:'.$this->toolCellWidth.'px">&nbsp;</th>';
				$x .= '</tr>';
			$x .= '</thead>';
			
			$x .= '<tbody>';
			
			# Display Settings Info
			for ($i=0; $i<$numOfSettings; $i++)
			{
				$thisSettingsID = $SettingsInfoArr[$i]['SettingsID'];
				$thisCode 		= $SettingsInfoArr[$i]['SettingsCode'];
				$thisNameEn 	= $SettingsInfoArr[$i]['SettingsNameEn'];
				$thisNameCh 	= $SettingsInfoArr[$i]['SettingsNameCh'];
				
				$x .= '<tr id="'.$thisSettingsID.'">';
					# Code
					$x .= '<td>';
						$x .= $this->Get_Thickbox_Edit_Div("Code_".$thisSettingsID, "Code_".$thisSettingsID, "jEditCode", $thisCode);
						$x .= $this->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv_".$thisSettingsID);
					$x .= '</td>';
					# Title (English)
					$x .= '<td>';
						$x .= $this->Get_Thickbox_Edit_Div("NameEn_".$thisSettingsID, "NameEn_".$thisSettingsID, "jEditNameEn", $thisNameEn);
						$x .= $this->Get_Thickbox_Warning_Msg_Div("NameEnWarningDiv_".$thisSettingsID);
					$x .= '</td>';
					# Title (Chinese)
					$x .= '<td>';
						$x .= $this->Get_Thickbox_Edit_Div("NameCh_".$thisSettingsID, "NameCh_".$thisSettingsID, "jEditNameCh", $thisNameCh);
						$x .= $this->Get_Thickbox_Warning_Msg_Div("NameChWarningDiv_".$thisSettingsID);
					$x .= '</td>';
					
					# Move Icon
					$x .= '<td class="Dragable" align="left">';
						$x .= $this->GET_LNK_MOVE("#", $Lang['Btn']['Move']);
					$x .= '</td>';
					
					# Delete Icon
					$x .= '<td align="left">';
						$x .= $this->GET_LNK_RESTORE("#", $Lang['Btn']['Load'], "js_Load_Settings('$thisSettingsID'); return false;");
						$x .= $this->GET_LNK_DELETE("#", $Lang['Btn']['Delete'], "js_Delete_Settings('$thisSettingsID'); return false;");
					$x .= '</td>';
				$x .= '</tr>';
			}
			
				# Add Settings Button
				$thisTitle = $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SettingsArr']['Add'];
				$x .= '<tr id="AddRow" class="nodrop nodrag">';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>';
						$x .= '<div class="table_row_tool row_content_tool">';
							$x .= '<a href="#" onclick="js_Add_Temp_Settings_Row(); return false;" class="add_dim" title="'.$thisTitle.'"></a>';
						$x .= '</div>';
					$x .= '</td>';
				$x .= '</tr>';
			$x .= '</tbody>';
		$x .= '</table>';
		
		
		$x .= '<br />';
		$x .= '<div class="edit_bottom_v30">';
			# Last Updated Info Div
			$LastModifiedInfoArr = $lreportcard->Get_Report_Template_Settings_Last_Modified_Info();
			if (count($LastModifiedInfoArr) > 0)
			{
				$lastModifiedDate = $LastModifiedInfoArr[0]['DateModified'];
				$lastModifiedBy = $LastModifiedInfoArr[0]['LastModifiedBy'];
				$LastModifiedDisplay = Get_Last_Modified_Remark($lastModifiedDate, '', $lastModifiedBy);
				$x .= '<span>'.$LastModifiedDisplay.'</span>';
				$x .= '<p class="spacer"></p>';
			}
			
			### Close Buttom
			$x .= $CloseBtn."\n";
		$x .= '</div>';
		
		return $x;
	}
	
	function Get_Management_Schedule_UI()
	{
		$x = '';
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td><div id="ScheduleDiv"></div></td>';		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";	
		
		return $x;
	}
	
	function Get_Management_Schedule_Table()
	{
		global $Lang, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
		$lreportcard_module = new libreportcardrubrics_module(); 	
		
		# Get Terms 
		$YearTermArr = getSemesters($lreportcard_module->AcademicYearID);
		
		# Build Term array
		$TermArr[0] = $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['WholeYear'];
		foreach($YearTermArr as $TermID => $TermName)
			$TermArr[$TermID] = $TermName;
		
		# Get Module Info
		$ModuleArr = $lreportcard_module->Get_Module_Info();
		$ModuleAssocArr = BuildMultiKeyAssoc($ModuleArr, array("YearTermID","ModuleID"));
		
		$ColSpan = 4;

		$x = '';
		$x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
			# col group
			$x .= '<col align="left" style="width: 20%;">'."\n";
			$x .= '<col align="left" style="width: 25%;">'."\n";
			$x .= '<col align="left" style="width: 25%;">'."\n";
			$x .= '<col align="left" style="width: 25%;">'."\n";
			$x .= '<col align="left" style="width: '.$this->toolCellWidth.'px;">'."\n";
			
			# table head
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th>'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Term'].'</th>';
					$x .= '<th colspan="'.$ColSpan.'" class="sub_row_top">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Module'].'</th>';	
				$x .= '</tr>';
				$x .= '<tr>';
					$x .= '<th rowspan="2">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Name'].'</th>';
					$x .= '<th class="sub_row_top" rowspan="2">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['Name'].'</th>';
					$x .= '<th class="sub_row_top" colspan="2" style="text-align:center;">'.$Lang['eRC_Rubrics']['ManagementArr']['ScheduleArr']['MarksheetSubmission'].'</th>';
					$x .= '<th class="sub_row_top" rowspan="2">&nbsp;</th>';
				$x .= '</tr>';
				$x .= '<tr>';
					$x .= '<th class="sub_row_top" style="text-align:center;">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['StartDate'].'</th>';
					$x .= '<th class="sub_row_top" style="text-align:center;">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['EndDate'].'</th>';
				$x .= '</tr>';
			$x .= '</thead>';		
			
			# table body
			$x .= '<tbody>';
			
				# loop term
				foreach((array)$TermArr as $thisTermID => $thisTerm)
				{
					$TermModule = $ModuleAssocArr[$thisTermID];
					$NumOfTermModule = count($TermModule);
					$NumOfRowSpan = $NumOfTermModule + 1;
					
					$x .= '<tr>';
						$x .= '<td rowspan="'.$NumOfRowSpan.'">'.$thisTerm.'</td>';
						if ($NumOfTermModule == 0)
						{
							// No Module in Term warning
							$x .= '<td colspan="4" style="text-align:center;">'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['NoModuleInTerm'].'</td>'."\n";
						}
					$x .= '</tr>';
					
					# loop module
					foreach((array)$TermModule as $thisModuleID => $thisModuleInfo)
					{
						$thisModuleInfoStartDate = $thisModuleInfo['MarksheetSubmissionStartDate'];
						$thisModuleInfoEndDate = $thisModuleInfo['MarksheetSubmissionEndDate'];
						$ModuleName = Get_Lang_Selection($thisModuleInfo['ModuleNameCh'],$thisModuleInfo['ModuleNameEn']);
						
						$thisTitle = $Lang['eRC_Rubrics']['ManagementArr']['ScheduleArr']['ToolTipArr']['EditModuleSchedule'];
						$thisOnclick = "js_Reload_Schedule_Edit_Layer('$thisModuleID')";
						$ThickBoxEditLink = $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, 'edit_dim', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content=""); 
						
						if ($lreportcard_module->Check_Within_DateRange($thisModuleInfoStartDate, $thisModuleInfoEndDate))
							$thisClass = 'class="within_schedule_cell"';
						else
							$thisClass = '';
							
						$thisModuleInfoStartDate = ($thisModuleInfoStartDate=='')? $Lang['General']['EmptySymbol'] : $thisModuleInfoStartDate;
						$thisModuleInfoEndDate = ($thisModuleInfoEndDate=='')? $Lang['General']['EmptySymbol'] : $thisModuleInfoEndDate;
							
						$x .= '<tr class="sub_row">';
							$x .= '<td>'.intranet_htmlspecialchars($ModuleName).'</td>';
							$x .= '<td '.$thisClass.' style="text-align:center;">'.$thisModuleInfoStartDate.'</td>';	
							$x .= '<td '.$thisClass.' style="text-align:center;">'.$thisModuleInfoEndDate.'</td>';
							$x .= '<td>';
								$x .= '<div class="table_row_tool row_content_tool">';
									$x .= $ThickBoxEditLink;
								$x .= '</div>';
							$x .= '</td>';
						$x .= '</tr>';
					} // end loop module
				}// end loop term
				
			$x .= '</tbody>';
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Management_Schedule_Edit_Layer($ModuleID)
	{
		global $Lang, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
		$lreportcard_module = new libreportcardrubrics_module();
		
		# Get Module Info
		$ModuleInfo = $lreportcard_module->Get_Module_Info('', $ModuleID);
		$ModuleStartDate = $ModuleInfo[0]['StartDate'];
		$ModuleEndDate = $ModuleInfo[0]['EndDate'];
		$MarksheetSubmissionStartDate = $ModuleInfo[0]['MarksheetSubmissionStartDate'];
		$MarksheetSubmissionEndDate = $ModuleInfo[0]['MarksheetSubmissionEndDate'];
		
		# Get Btns
		$SubmitBtn = $this->Get_Action_Btn($Lang['Btn']['Submit'], "Button", "js_Update_Schedule();");
		$CloseBtn = $this->Get_Action_Btn($Lang['Btn']['Close'], "Button", "js_Hide_ThickBox();");
		
		$x .= '';
		$x .= '<table class="form_table_v30">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td valign="top">'."\n";	
					$x .= '<table width="95%" class="tabletext" cellpadding="2">'."\n";
						# Apply Date check box 
						$thisChecked = ($MarksheetSubmissionStartDate || $MarksheetSubmissionEndDate)? "checked" : "";

						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ManagementArr']['ScheduleArr']['ApplySchedule'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="ApplyDate" type="checkbox" onclick="js_Update_Date_Input_Status(this.checked)" '.$thisChecked.'>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						# Start Date
						$thisDisabled = ($MarksheetSubmissionStartDate || $MarksheetSubmissionEndDate)? "" : "disabled";
						$thisValue = ($MarksheetSubmissionStartDate=='')? $ModuleStartDate : $MarksheetSubmissionStartDate;
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['StartDate'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $this->GET_DATE_PICKER("MarksheetSubmissionStartDate", $thisValue, $thisDisabled)."\n";
								$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnStartDate");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						# End Date
						$thisValue = ($MarksheetSubmissionEndDate=='')? $ModuleEndDate : $MarksheetSubmissionEndDate;
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['EndDate'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $this->GET_DATE_PICKER("MarksheetSubmissionEndDate", $thisValue, $thisDisabled)."\n";
								$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnEndDate");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
			
		$x .= '<input type="hidden" id="ModuleID" name="ModuleID" value="'.$ModuleID.'">';
		$x .= '<br style="clear:both;" />';
			
		### Buttons
		$x .= '<div class="edit_bottom_v30">';
			$x .= $SubmitBtn."&nbsp;".$CloseBtn;
		$x .= '</div>';
		
		if ($thisDisabled)
		{
			// hide the calandar icon so that the user cannot select the date
			$x .= '<script language="javascript">'."\n";
				$x .= 'js_Update_Date_Input_Status(false);'."\n";
			$x .= '</script>'."\n";
		}
		
		return $x;
	}
	
	function Get_Report_Template_Selection($ID_Name, $SelectedReportID='', $Onchange='', $noFirst=1, $isAll=0)
	{
		global $Lang, $lreportcard;
		
		$ReportInfoArr = $lreportcard->GetReportTemplateInfo();
		$numOfReport = count($ReportInfoArr);
		
		$selectArr = array();
		for ($i=0; $i<$numOfReport; $i++)
		{
			$thisReportID = $ReportInfoArr[$i]['ReportID'];
			$thisReportTitle = Get_Lang_Selection($ReportInfoArr[$i]['ReportTitleCh'], $ReportInfoArr[$i]['ReportTitleEn']);
			
			$selectArr[$thisReportID] = $thisReportTitle;
		}
		
		$onchange = '';
		if ($Onchange != "")
			$onchange = 'onchange="'.$Onchange.'"';
		
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange;
		if($isAll)
			$firstTitle = $Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['AllReport'];
		else
			$firstTitle = $Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['Report'];
		
		return getSelectByAssoArray($selectArr, $selectionTags, $SelectedReportID, $isAll, $noFirst, $firstTitle);
	}
	
	function Get_Template_Header_Data_Info_Checkbox_Table($DefaultOrderList='', $DefaultSelectedArr='', $SettingsID='')
	{
		global $eRC_Rubrics_ConfigArr;		# from /includes/erc_rubrics_config.php
		global $Lang;
		
		# Get Student Info
		$HeaderDataFieldArr = ($DefaultOrderList=='')? $eRC_Rubrics_ConfigArr['HeaderDataFieldArr'] : explode(',', $DefaultOrderList);
		$numOfHeaderDataField = count($HeaderDataFieldArr);
		
		# build Select All checkbox
		$isChecked = ($SettingsID=='' || count($DefaultSelectedArr) == $numOfHeaderDataField)? 1 : 0;
		$SelectAllCheckBox = $this->Get_Checkbox($ID="SelectAll_HeaderDataChk", $Name='', $Value='', $isChecked, $Class='', $Lang['Btn']['SelectAll'], $Onclick="Set_Checkbox_Value('HeaderDataFieldArr[]', this.checked);");
				
		$x = '';
		$x .= '<table class="sortable_table" border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= $SelectAllCheckBox."\n";
					$x .= '<br />'."\n";
					$x .= '<ul class="sortable" id="HeaderDataFieldSortable">'."\n";
						for ($i=0; $i<$numOfHeaderDataField; $i++)
						{
							$thisHeaderDataKey = $HeaderDataFieldArr[$i];
							$thisHeaderDataName = $this->Get_Customized_Lang('HeaderData', $thisHeaderDataKey);
							
							$thisID = 'HeaderDataField_'.$thisHeaderDataKey;
							$thisName = 'HeaderDataFieldArr[]';
							
							$isChecked = ($SettingsID=='' || in_array($thisHeaderDataKey, (array)$DefaultSelectedArr))? 1 : 0;
							$thisCheckbox = $this->Get_Checkbox($thisID, $thisName, $thisHeaderDataKey, $isChecked, $Class='', $thisHeaderDataName, $Onclick="js_Check_Uncheck_Check_All('SelectAll_HeaderDataChk', this.checked);");
							
							$x .= '<li id="'.$thisHeaderDataKey.'">'.$thisCheckbox."</li>\n";
						}
					$x .= '</ul>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		$HeaderKeyList = implode(',', (array)$HeaderDataFieldArr);
		$x .= '<input type="hidden" id="HeaderDataFieldOrderedList" name="HeaderDataFieldOrderedList" value="'.intranet_htmlspecialchars($HeaderKeyList).'">';
		
		return $x;
	}
	
	function Get_Topic_Level_Checkbox_Table($SelectedCurriculumArr='', $SettingsID='')
	{
		global $Lang, $eRC_Rubrics_ConfigArr;
		
		$MaxLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		
		# build Select All checkbox
		if ($SettingsID=='')
			$isChecked = 1;
		else if (count($SelectedCurriculumArr) == $MaxLevel)
			$isChecked = 1;
		else if (count($SelectedCurriculumArr) < $MaxLevel)
			$isChecked = 0;
		$SelectAllCheckBox = $this->Get_Checkbox($ID="SelectAll_TopicLevelChk", $Name='', $Value='', $isChecked, $Class='', $Lang['Btn']['SelectAll'], $Onclick="Set_Checkbox_Value('TopicLevelArr[]', this.checked);");
		
		$x = '';
		$x .= '<table border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= $SelectAllCheckBox."\n";
					$x .= '<br />'."\n";
					for ($thisLevel=1; $thisLevel<=$MaxLevel; $thisLevel++)
					{
						$thisTitle = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$thisLevel]['Title'];
						
						### The last level of the topic should always be displayed
						//if ($thisLevel == $MaxLevel)
						//	continue;
						
						if ($SettingsID=='' || in_array($thisLevel, (array)$SelectedCurriculumArr))
							$isChecked = true;
						else
							$isChecked = false;
							
						$thisID = 'TopicLevel_'.$thisLevel;
						$thisName = 'TopicLevelArr[]';
						$thisClass = 'TopicLevelChk';
						$thisCheckbox = $this->Get_Checkbox($thisID, $thisName, $thisLevel, $isChecked, $thisClass, $thisTitle, $Onclick="js_Check_Uncheck_Check_All('SelectAll_TopicLevelChk', this.checked);");
						
						$x .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$thisCheckbox."<br />\n";
					}
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		return $x;
	}
	
	function Get_Template_Mark_Display_Type_Options($SelectedOption='')
	{
		global $Lang;
		
		$x = '';
		$isChecked = 1;
		foreach ($Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MarkDisplayOptionArr'] as $thisKey => $thisValue)
		{
			$isChecked = ($SelectedOption == $thisKey)? 1 : $isChecked;
			$x .= $this->Get_Radio_Button($ID="MarkDisplay_".$thisKey, $Name="MarkDisplayOption", $Value=$thisKey, $isChecked, $Class='', $thisValue, $Onclick='')."\n";
			$x .= '<br />'."\n";
			
			$isChecked = 0;
		}
		
		return $x;
	}
	
	function Get_ECA_Checkbox_Table($CheckedCategoryIDArr='', $SettingsID='')
	{
		global $PATH_WRT_ROOT, $Lang;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
		$lreportcard_eca = new libreportcardrubrics_eca(); 	
		$ECACategoryInfoArr = $lreportcard_eca->Get_ECA_Category_Info();
		$numOfCategory = count($ECACategoryInfoArr);
		
		# build Select All checkbox
		$isChecked = ($SettingsID=='' || $numOfCategory == count($CheckedCategoryIDArr))? 1 : 0;
		$SelectAllCheckBox = $this->Get_Checkbox($ID="SelectAll_ECACategoryChk", $Name='', $Value='', $isChecked, $Class='', $Lang['Btn']['SelectAll'], $Onclick="Set_Checkbox_Value('ECACategoryIDArr[]', this.checked);");
		
		$x = '';
		$x .= '<table border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= $SelectAllCheckBox."\n";
					$x .= '<br />'."\n";
					for ($i=0; $i<$numOfCategory; $i++)
					{
						$thisCategoryID = $ECACategoryInfoArr[$i]['EcaCategoryID'];
						$thisCategoryNameEn = $ECACategoryInfoArr[$i]['EcaCategoryNameEn'];
						$thisCategoryNameCh = $ECACategoryInfoArr[$i]['EcaCategoryNameCh'];
						$thisNameDisplay = Get_Lang_Selection($thisCategoryNameCh, $thisCategoryNameEn);
						$thisNameDisplay = intranet_htmlspecialchars($thisNameDisplay);
						
						if ($SettingsID=='' || in_array($thisCategoryID, (array)$CheckedCategoryIDArr))
							$isChecked = true;
						else
							$isChecked = false;
							
						$thisID = 'ECACategoryID_'.$thisCategoryID;
						$thisName = 'ECACategoryIDArr[]';
						$thisCheckbox = $this->Get_Checkbox($thisID, $thisName, $thisCategoryID, $isChecked, $Class='', $thisNameDisplay, $Onclick="js_Check_Uncheck_Check_All('SelectAll_ECACategoryChk', this.checked);");
						
						$x .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$thisCheckbox."<br />\n";
					}
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		return $x;
	}
	
	function Get_Other_Info_Checkbox_Table($DefaultCategoryOrderList='', $DefaultItemOrderList='', $CheckedItemIDArr='', $SelectAllChecked='', $SettingsID='')
	{
		global $PATH_WRT_ROOT, $Lang, $lreportcard;
		
		$CategoryInfoArr = $lreportcard->Get_Other_Info_Category();
		$CategoryInfoArr = BuildMultiKeyAssoc($CategoryInfoArr, array("CategoryID"));
		
		$ItemInfoArr = $lreportcard->Get_OtherInfo_Item_Info();
		$CategoryItemInfoArr = BuildMultiKeyAssoc($ItemInfoArr, array("CategoryID", "ItemID"));
		
		# build Select All checkbox
		$isChecked = ($SettingsID=='' || $SelectAllChecked == 1)? 1 : 0;
		$SelectAllCheckBox = $this->Get_Checkbox($ID="SelectAll_OtherInfoChk", $Name='SelectAll_OtherInfoChk', $Value='1', $isChecked, $Class='', $Lang['Btn']['SelectAll'], $Onclick="js_Check_All_Other_Info_Checkbox(this.checked);");
				
		$x = '';
		$x .= '<table id="OtherInfoCatTable" class="sortable_table" border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<thead>';
				$x .= '<tr><td colspan="2" style="border-bottom:1px solid #FFFFFF;">'.$SelectAllCheckBox.'</td></tr>'."\n";
			$x .= '</thead>';
			$x .= '<tbody>';			
				### Loop Category
				// loop $CategoryItemInfoArr instead of $CategoryInfoArr => Show the Categorys which have Items in it only
				$thisCategoryIDArr = ($DefaultCategoryOrderList=='')? array_keys((array)$CategoryItemInfoArr) : explode(',', $DefaultCategoryOrderList);
				$thisNumOfCategory = count($thisCategoryIDArr);
				for ($i=0; $i<$thisNumOfCategory; $i++)
				{
					$thisCategoryID 			= $thisCategoryIDArr[$i];
					$thisCategoryCode 			= $CategoryInfoArr[$thisCategoryID]['CategoryCode'];
					$thisCategoryNameEn 		= $CategoryInfoArr[$thisCategoryID]['CategoryNameEn'];
					$thisCategoryNameCh 		= $CategoryInfoArr[$thisCategoryID]['CategoryNameCh'];
					$thisCategoryItemInfoArr 	= $CategoryItemInfoArr[$thisCategoryID];
					
					if (!in_array($thisCategoryCode, $lreportcard->OtherInfoTypeArr))
						continue;
										
					$thisCategoryNameDisplay = Get_Lang_Selection($thisCategoryNameCh, $thisCategoryNameEn);
					$thisCategoryNameDisplay = intranet_htmlspecialchars($thisCategoryNameDisplay);
					
					$x .= '<tr id="'.$thisCategoryID.'">'."\n";
						$x .= '<td class="Dragable move_td">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
							$x .= $thisCategoryNameDisplay;
						$x .= '</td>';
						$x .= '<td >';
							$x .= '<br />'."\n";
							$x .= '<ul class="sortable" id="OtherInfoCategory_'.$thisCategoryID.'">'."\n";
								### Loop Items
								$thisItemIDArr = ($SettingsID=='')? array_keys((array)$thisCategoryItemInfoArr) : explode(',', $DefaultItemOrderList[$thisCategoryID]);
								$thisNumOfItem = count($thisItemIDArr);
								for ($j=0; $j<$thisNumOfItem; $j++)
								{
									$thisItemID = $thisItemIDArr[$j];
									$thisItemCode = $thisCategoryItemInfoArr[$thisItemID]['ItemCode'];
									$thisItemNameEn = $thisCategoryItemInfoArr[$thisItemID]['ItemNameEn'];
									$thisItemNameCh = $thisCategoryItemInfoArr[$thisItemID]['ItemNameCh'];
									$thisItemNameDisplay = Get_Lang_Selection($thisItemNameCh, $thisItemNameEn);
									$thisItemNameDisplay = intranet_htmlspecialchars($thisItemNameDisplay);
									
									if (!in_array($thisItemCode, $lreportcard->OtherInfoItemArr[$thisCategoryCode]))
										continue;
									
									if ($SettingsID=='' || $CheckedItemIDArr[$thisCategoryID][$thisItemID] == 1)
										$isChecked = true;
									else
										$isChecked = false;
										
									$thisID = 'OtherInfoItemID_'.$thisItemNameEn;
									$thisName = 'OtherInfoItemIDArr['.$thisCategoryID.']['.$thisItemID.']';
									$thisClass = 'OtherInfoChk';
									$thisCheckbox = $this->Get_Checkbox($thisID, $thisName, '1', $isChecked, $thisClass, $thisItemNameDisplay, $Onclick="js_Check_Uncheck_Check_All('SelectAll_OtherInfoChk', this.checked);");
									
									$x .= '<li id="'.$thisItemID.'">'.$thisCheckbox."</li>\n";
								}
							$x .= '</ul>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
					$thisDisplayItemOrderedList = implode(',', $thisItemIDArr);
					$x .= '<input type="hidden" id="OtherInfoItemOrderedList_'.$thisCategoryID.'" name="OtherInfoItemOrderedList['.$thisCategoryID.']" value="'.$thisDisplayItemOrderedList.'">'."\n";
				}
			$x .= '</tbody>';
		$x .= '</table>'."\n";
		
		$CategoryIDOrderedList = implode(',', $thisCategoryIDArr);
		$x .= '<input type="hidden" id="OtherInfoCategoryOrderedList" name="OtherInfoCategoryOrderedList" value="'.$CategoryIDOrderedList.'">'."\n";
		
		return $x;
	}
	
	function Get_Template_Signature_Checkbox_Table($DefaultOrderList='', $DefaultSelectedArr='', $SettingsID='')
	{
		global $eRC_Rubrics_ConfigArr;		# from /includes/erc_rubrics_config.php
		global $Lang;
		
		# Get Signature Info
		$SignatureFieldArr = ($SettingsID=='')? $eRC_Rubrics_ConfigArr['SignatureFieldArr'] : explode(',', $DefaultOrderList);
		$numOfSignatureField = count($SignatureFieldArr);
		
		# build Select All checkbox
		$isChecked = ($SettingsID=='' || $numOfSignatureField == count($DefaultSelectedArr))? 1 : 0;
		$SelectAllCheckBox = $this->Get_Checkbox($ID="SelectAll_SignatureChk", $Name='', $Value='', $isChecked, $Class='', $Lang['Btn']['SelectAll'], $Onclick="Set_Checkbox_Value('SignatureFieldArr[]', this.checked);");
				
		$x = '';
		$x .= '<table class="sortable_table" border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= $SelectAllCheckBox."\n";
					$x .= '<br />'."\n";
					$x .= '<ul class="sortable" id="SignatureFieldSortable">'."\n";
					for ($i=0; $i<$numOfSignatureField; $i++)
					{
						$thisSignatureKey = $SignatureFieldArr[$i];
						$thisSignatureName = $this->Get_Customized_Lang('Signature', $thisSignatureKey);
						
						$thisID = 'SignatureField_'.$thisSignatureKey;
						$thisName = 'SignatureFieldArr[]';
						
						$isChecked = ($SettingsID == '' || in_array($thisSignatureKey, (array)$DefaultSelectedArr))? 1 : 0;
						$thisCheckbox = $this->Get_Checkbox($thisID, $thisName, $thisSignatureKey, $isChecked, $Class='', $thisSignatureName, $Onclick="js_Check_Uncheck_Check_All('SelectAll_SignatureChk', this.checked);");
						
						$x .= '<li id="'.$thisSignatureKey.'">'.$thisCheckbox."</li>\n";
					}
					$x .= '</ul>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		$SignatureKeyList = implode(',', (array)$SignatureFieldArr);
		$x .= '<input type="hidden" id="SignatureFieldOrderedList" name="SignatureFieldOrderedList" value="'.intranet_htmlspecialchars($SignatureKeyList).'">';
		
		return $x;
	}
	
	function Get_Customized_Lang($Category, $LangKey, $ParLang='')
	{
		global $Lang;
		
		if ($ParLang == '')
			return Get_Lang_Selection($Lang['eRC_Rubrics']['Customized'][$Category][$LangKey]['Ch'], $Lang['eRC_Rubrics']['Customized'][$Category][$LangKey]['En']);
		else if (strtolower($ParLang) == 'en')
			return $Lang['eRC_Rubrics']['Customized'][$Category][$LangKey]['En'];
		else
			return $Lang['eRC_Rubrics']['Customized'][$Category][$LangKey]['Ch'];
	}
	
#######################################################################################################
# Management > Class Teacher Comment Start
#
	
	function Get_Management_Class_Teacher_Comment_UI($ReportID='')
	{
		global  $ck_ReportCard_Rubrics_UserType, $UserID, $PATH_WRT_ROOT, $lreportcard;
		
		# Get Report Selection
		$ReportSelection = $this->Get_Report_Template_Selection("ReportID",$ReportID,"js_Reload_Comment_Table()",0,0);
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td valign="bottom">'."\n";
						$x .= '<div class="table_filter" style="padding:2px;">'.$ReportSelection.'</div>'."\n";
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td><div id="CommentTableDiv"><!-- load by ajax --></div></td>';		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";	
		
		return $x;
				
	}
	
	function Get_Management_Class_Teacher_Comment_Table($ReportID='', $ClassLevelID='')
	{
		global $Lang , $ck_ReportCard_Rubrics_UserType, $PATH_WRT_ROOT, $lreportcard_comment;
	
		if(!isset($lreportcard_comment))
		{
			include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
			$lreportcard_comment = new libreportcardrubrics_comment();
		}
		
		$FormArr = BuildMultiKeyAssoc($lreportcard_comment->Get_User_Accessible_Form(),"ClassLevelID");
		$ClassArr = BuildMultiKeyAssoc($lreportcard_comment->Get_User_Accessible_Class(),array("ClassLevelID","ClassID"));
		
		$x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
			# col group
			$x .= '<col align="left" style="width: 20%;">'."\n";
			$x .= '<col align="left" style="width: 20%;">'."\n";
			$x .= '<col align="left">'."\n";
			$x .= '<col align="left">'."\n";
			$x .= '<col align="left">'."\n";
	
			# table head
			$ColSpan = "4";
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th>'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Form'].'</th>';
					$x .= '<th colspan="'.$ColSpan.'" class="sub_row_top">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Class'].'</th>';	
				$x .= '</tr>';
				$x .= '<tr>';
					$x .= '<th >'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Name'].'</th>';
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Name'].'</th>';
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['TeacherComment'].'<br>('.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['NumOfComment'].'/'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['NumOfStudent'].')'.'</th>';
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['LastModifiedDate'].'</th>';
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['LastModifiedBy'].'</th>';
				$x .= '</tr>';
				
			$x .= '</thead>';		
			
			# table body
			$x .= '<tbody>';
			
				# loop term
				foreach((array)$ClassArr as $ClassLevelID => $ClassInfoArr)
				{
					if(!($FormInfo = $FormArr[$ClassLevelID]))
						continue;
					
					$NumOfClass = count($ClassInfoArr) + 1;
					
					$x .= '<tr>';
						$x .= '<td rowspan="'.$NumOfClass.'">'.$FormInfo['LevelName'].'</td>';
					$x .= '</tr>';
					
					# loop module
					foreach((array)$ClassInfoArr as $thisClassID => $thisClassInfo)
					{
						
						$LastModifiedInfo = $lreportcard_comment->Get_Class_Teacher_Comment_Latest_Modification($ReportID,$thisClassID);
						$thisCommentInfo = $lreportcard_comment->Get_Class_Teacher_Comment_Info($ReportID, $thisClassID);
						
						$LastModifiedDate = $LastModifiedInfo["DateModified"]?$LastModifiedInfo["DateModified"]:' - ';
						$LastModifiedBy = $LastModifiedInfo["LastModifiedBy"]?$LastModifiedInfo["LastModifiedBy"]:' - ';
						$TotalComment = $thisCommentInfo["TotalComment"]?$thisCommentInfo["TotalComment"]:'0';
						$TotalStudent = $thisCommentInfo["TotalStudent"]?$thisCommentInfo["TotalStudent"]:'0';

						$x .= '<tr class="sub_row">';
							$x .= '<td>'.$thisClassInfo['ClassName'].'</td>';
							$x .= '<td>';
								$x .= '<a href="edit.php?ReportID='.$ReportID.'&ClassID='.$thisClassID.'">'.$TotalComment.'/'.$TotalStudent."</a>";
							$x .= '</td>';
							$x .= '<td>'.$LastModifiedDate.'</td>';	
							$x .= '<td>'.$LastModifiedBy.'</td>';
						$x .= '</tr>';
					} // end loop module
				}// end loop term
				
			$x .= '</tbody>';
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Management_Class_Teacher_Comment_Edit_UI($ReportID,$ClassID)
	{
		global  $Lang, $PATH_WRT_ROOT, $lreportcard_comment;
		
		if(!isset($lreportcard_comment))
		{
			include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
			$lreportcard_comment = new libreportcardrubrics_comment();
		}
		
		
		# Report Title
		$ReportInfo = $lreportcard_comment->GetReportTemplateInfo($ReportID);
		$ReportTitleField = Get_Lang_Selection("ReportTitleCh","ReportTitleEn");
		$ReportTitle = $ReportInfo[0][$ReportTitleField];		

		# Class Title
		$ClassInfo = $lreportcard_comment->GET_CLASSES_BY_FORM('',$ClassID);
		$ClassTitleField = Get_Lang_Selection("ClassTitleCh","ClassTitleEn");
		
		# Table
		$CommentTable = $this->Get_Management_Class_Teacher_Comment_Edit_Table($ReportID,$ClassID);
		
		# Class Level Title
		$ClassLevelID = $lreportcard_comment->Get_ClassLevel_By_ClassID($ClassID);
		$YearName = $lreportcard_comment->returnClassLevel($ClassLevelID);
		
		# Buttons
		$btn_Save = $this->GET_ACTION_BTN($Lang['Btn']['Save'], "button","document.form1.submit();");
		$btn_Cancel = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button","window.location.href='index.php';");
		$btn_Import = $this->Get_Content_Tool_v30("import","import.php?ReportID=$ReportID&ClassID=$ClassID&ClassLevelID=$ClassLevelID");
		$ExportOptions[] = array("export.php?ReportID=$ReportID&ClassLevelID=$ClassLevelID",$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ExportFormData']." ($YearName)");
		$ExportOptions[] = array("export.php?ReportID=$ReportID&ClassID=$ClassID",$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ExportClassData']." (".$ClassInfo[0]["ClassName"].")");  
		$btn_Export = $this->Get_Content_Tool_v30("export","javascript:void(0);","",$ExportOptions);
		
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['MenuTitle'], "index.php?ReportID=$ReportID&ClassID=$ClassID"); 
		$PAGE_NAVIGATION[] = array($ReportTitle, ""); 
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post" action="edit_update.php">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_NAViGATION_IP25($PAGE_NAVIGATION)."\n";
						$x .= '<br><br>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<table class="form_table_v30">'."\n";
							$x .= '<col class="field_title">';
							$x .= '<col class="field_c">';
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Report'].'</td>';
								$x .= '<td>'.$ReportTitle.'</td>';		
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Class'].'</td>';
								$x .= '<td>'.$ClassInfo[0]["ClassName"].'</td>';		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";			
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div class="content_top_tool">'."\n";
							$x .= '<div class="Conntent_tool">'."\n";
								$x .= $btn_Import."\n";
								$x .= $btn_Export."\n";
							$x .= '</div>'."\n";
						$x .= '</div>'."\n";	
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";	
				$x .= '<tr>'."\n";
					$x .= '<td><div id="CommentTableDiv">'.$CommentTable.'</div></td>';		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
			$x .=  $this->ArchiveStudentField();

			$x .= '<br style="clear:both" />'."\n";
			$x .= '<div class="edit_bottom_v30">';
				### Save Buttom
					$x .= $btn_Save."\n";
					$x .= $btn_Cancel."\n";
			$x .= '</div>';
			
			# hidden field
			$x .= '<input type="hidden" name="ReportID" id="ReportID" value="'.$ReportID.'">'."\n";
		$x .= '</form>'."\n";	
		
		return $x;
				
	}
	
	function Get_Management_Class_Teacher_Comment_Edit_Table($ReportID,$ClassID)
	{
		global  $Lang, $PATH_WRT_ROOT, $lreportcard, $button_find, $i_general_or, $eRC_Rubrics_ConfigArr;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
		$lreportcard_comment = new libreportcardrubrics_comment();
		
		$StudentList = $lreportcard_comment->GET_STUDENT_BY_CLASS($ClassID,"",0,0,1);
		
		$TeacherComment = $lreportcard_comment->Get_Student_Class_Teacher_Comment($ReportID,'',0);
		$TeacherComment = BuildMultiKeyAssoc($TeacherComment,array("StudentID"),array("TeacherCommentID","Comment"));
		
		$MaxLength = $eRC_Rubrics_ConfigArr['MaxLength']['ClassTeacherComment'];
		
		$x .= '<table class="common_table_list_v30 edit_table_list_v30" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
			$x .= '<col width=10%>'."\n";
			$x .= '<col width=20%>'."\n";
			$x .= '<col width=70%>'."\n";
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ClassNo'].'</th>'."\n";
					$x .= '<th>'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Student'].'</th>'."\n";
					$x .= '<th>'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Comment'].'('.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['NumOfComments'].'/'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['NumOfStudent'].')'.'</th>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			$x .= '<tbody>'."\n";
				foreach((array)$StudentList as $key => $StudentInfo)
				{
					$StudentID = $StudentInfo['UserID'];
					
					$x .= '<tr>'."\n";
						$x .= '<td>'.$StudentInfo['ClassNumber'].'</td>'."\n";
						$x .= '<td>'.$StudentInfo['StudentName'].'</td>'."\n";
						$x .= '<td>'."\n";
						
							$CommentID = $TeacherComment[$StudentID]["TeacherCommentID"];
							$textareaName = $CommentID?"Comment[$CommentID]":"NewComment[$StudentID]";
							$textareaID = "comment".$StudentID;
							$textareaValue = stripslashes($TeacherComment[$StudentID]["Comment"]);

							$x .= $button_find;
							$x .= ' <input class="textboxnum SearchComment" type="text" name="search['.$StudentID.']" id="search_'.$StudentID.'" value="" />';
							$x .= " $i_general_or ";
							$x .= $this->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "newWindow('choose_comment.php?fieldname=$textareaID', 9)");
							$x .= '<br />';
							
							$limitTextAreaEvent = "onKeyDown='limitText(document.getElementById(\"$textareaID\"),".$MaxLength.");return noEsc();'";
							$limitTextAreaEvent .= "onKeyUp='limitText(document.getElementById(\"$textareaID\"),".$MaxLength.");'";
				
							$x .= "<textarea class=\"textboxtext\" id=\"$textareaID\" name=\"$textareaName\" cols=\"70\" rows=\"2\" wrap=\"virtual\" onFocus=\"this.rows=5;\" $limitTextAreaEvent>$textareaValue</textarea>";							
						$x .= '</td>'."\n";		
					$x .= '</tr>'."\n";
				}
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";	
		
		return $x;
		
	}
	
	function Get_Management_Class_Teacher_Comment_Import_UI($ReportID,$ClassID, $ClassLevelID='')
	{
		global  $Lang, $PATH_WRT_ROOT, $lreportcard;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
		$lreportcard_comment = new libreportcardrubrics_comment();
		
		$ReportTemplateInfo = $lreportcard->GetReportTemplateInfo($ReportID);
		$ReportTitle = Get_Lang_Selection($ReportTemplateInfo[0]['ReportTitleCh'],$ReportTemplateInfo[0]['ReportTitleEn']);
		
		# page navigation
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['MenuTitle'], "index.php?ReportID=$ReportID&ClassID=$ClassID"); 
		$PAGE_NAVIGATION[] = array($ReportTitle, "edit.php?ReportID=$ReportID&ClassID=$ClassID"); 
		$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
			
		$x .= '<form id="form1" name="form1" method="post" action="import_confirm.php" onsubmit="return checkForm();" enctype="multipart/form-data">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
						$x .= '<br><br>'."\n";
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_IMPORT_STEPS(1);
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Warning_Message_Box('', $Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportDataInstruction']);
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Management_Class_Teacher_Comment_Import_Table($ReportID,$ClassID,$ClassLevelID);
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<br><br>'."\n";
			
			$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "");
			$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location.href='edit.php?ReportID=$ReportID&ClassID=$ClassID';");
			$x .= '<div class="edit_bottom_v30">';
				### Close Buttom
				$x .= $SubmitBtn."\n";
				$x .= $CancelBtn."\n";
			$x .= '</div>';		
			$x .= '<input type="hidden" id="ReportID" name="ReportID" value="'.$ReportID.'">'."\n";
		$x .= '</form>'."\n";	
		
		return $x;
		
	}
	
	function Get_Management_Class_Teacher_Comment_Import_Table($ReportID,$ClassID='', $ClassLevelID='')
	{
		global $lreportcard, $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$ReportTemplateInfo = $lreportcard->GetReportTemplateInfo($ReportID);
		$ReportTitle = Get_Lang_Selection($ReportTemplateInfo[0]['ReportTitleCh'],$ReportTemplateInfo[0]['ReportTitleEn']);
		
		$thisOnChange = 'js_Changed_Form_Selection(this.value);';
		
		$FormSelection = $this->Get_Form_Selection("ClassLevelID", $ClassLevelID, $thisOnChange, $noFirst=1, $isAll=0, $firstTitle='', $TeachingFormOnly=1);
		$ClassSelection = $this->Get_Class_Selection("ClassID", $ClassID, '', $noFirst=0, $isAll=1, $firstTitle='', $ClassLevelID, $TeachingClassOnly=1);
		
		$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
			$x .= '<col class="field_title">';
			$x .= '<col class="field_c">';
			
			# Report 
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['GeneralArr']['Report'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $ReportTitle."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# Form 
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['GeneralArr']['Form'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $FormSelection."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# Class Name 
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['GeneralArr']['ClassName'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div id="ClassSelectionDiv">'.$ClassSelection."</div>\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# File 
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['File'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<input class="file" type="file" name="userfile" id="userfile">'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		$x .= '<a class="tablelink" href="javascript:void(0)" onclick="js_Export();">'."\n";
			$x .= '<img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">'."\n"; 
			$x .= $Lang['eRC_Rubrics']['GeneralArr']['DownloadCSVFile']."\n";
		$x .= '</a>'."\n";
		
		return $x;
	}
	
	// Copy from libreportcard2008_ui.php
	function Get_Management_ClassTeacherComment_CommentView_UI($Keyword='')
	{
		global $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN;
		
		# SearchBox
		$SearchBox = $this->Get_Search_Box_Div('Keyword', $Keyword);
		
		# Table Action Button
		$TableActionArr[] = array("javascript:void(0);", $image_path."/".$LAYOUT_SKIN."/icon_new.gif", "imgAdd", $Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['AddToStudentComment'],"js_Form_Check();");
		$ActionBtnTable = $this->Get_Table_Action_Button($TableActionArr);
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="CommentViewForm" name="CommentViewForm" method="post" onsubmit="return false;">'."\n";
			$x .= '<table width="95%" border="0" cellpadding="0" cellspacing="0">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td align="right">'.$SearchBox.'</td>'."\n";
				$x .= '</tr>'."\n";
				
				$x .= '<tr><td>&nbsp;</td></tr>';
				
				### Category List Table
				$x .= '<tr class="table-action-bar"><td align="right" valign="bottom">'.$ActionBtnTable.'</td></tr>';
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div id="CommentTableDiv">'."\n";
							$x .= $this->Get_Management_ClassTeacherComment_CommentView_Table($Keyword);
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</form>'."\n";
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Management_ClassTeacherComment_CommentView_Table($Keyword='', $ViewMode=0, $CommentIDArr='')
	{
		global  $Lang, $PATH_WRT_ROOT, $lreportcard;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
		$lreportcard_comment = new libreportcardrubrics_comment();
		
		
		### Get Comments from Comment Bank
		$CommentArr = $lreportcard_comment->Get_Comment_Bank_Comment($Keyword, 0 ,$CommentIDArr);
		$numOfComment = count($CommentArr);
		
		### Get Global Checkbox
		$CheckAllChk = $this->Get_Checkbox('CheckAll', 'CheckAll', '', $isChecked=0, $Class='', $Display='', $Onclick='js_CheckAll(this.checked);', $Disabled='');
		
		$x = '';
		$x .= '<table class="common_table_list" id="CommentContentTable">'."\n";
			## Header
			$x .= '<thead>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<th style="width:20">#</th>'."\n";
				$x .= '<th style="width:10%">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Code'].'</th>'."\n";
				$x .= '<th style="width:85%">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Comment'].'</th>'."\n";
				if (!$ViewMode)
					$x .= '<th style="width:20; text-align:center;">'.$CheckAllChk.'</th>'."\n";
			$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			if ($numOfComment == 0)
			{
				$x .= '<tr><td colspan="4" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
			}
			else
			{
				$x .= '<tbody>'."\n";
					for ($i=0; $i<$numOfComment; $i++)
					{
						$thisCommentID = $CommentArr[$i]['CommentID'];
						$thisCommentCode = $CommentArr[$i]['CommentCode'];
						$thisCommentEn = $CommentArr[$i]['CommentEn'];
						$thisCommentCh = $CommentArr[$i]['CommentCh'];
						
						$thisChkboxk = $this->Get_Checkbox('Comment_'.$thisCommentID, 'CommentIDArr[]', $thisCommentID, $isChecked=0, $Class='CommentChk', $Display='', $Onclick='js_Clicked_Comment(this.checked);', $Disabled='');
						
						$x .= '<tr>'."\n";
							$x .= '<td valign="top">'.($i + 1).'</td>'."\n";
							$x .= '<td valign="top">'.$thisCommentCode.'</td>'."\n";
							$x .= '<td valign="top">'.nl2br(Get_Lang_Selection($thisCommentCh, $thisCommentEn)).'</td>'."\n";
							if (!$ViewMode)
								$x .= '<td align="center" valign="top">'.$thisChkboxk.'</td>'."\n";
						$x .= '</tr>'."\n";
					}
				$x .= '</tbody>'."\n";
			}
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Management_ClassTeacherComment_CommentView_ChooseStudent_UI($CommentIDArr, $Keyword='')
	{
		global  $lreportcard, $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $i_general_from_class_group, $i_general_search_by_loginid, $i_general_search_by_inputformat;
		global $ck_ReportCard_Rubrics_UserType;
		
		### Navigation
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ClassTeacherComment'], "javascript:js_Go_Back_To_Comment_List()"); 
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ChooseStudent'], ""); 
		$PageNavigation = $this->GET_NAVIGATION($PAGE_NAVIGATION);
		
		
		### Get Form selection (show form which has report card only)
		$FormArr = $lreportcard->GET_ALL_FORMS($hasTemplateOnly=1);
		$YearID = $FormArr[0]['ClassLevelID'];
		$CheckClassTeacher = ($ck_ReportCard_Rubrics_UserType=="ADMIN")? 0 : 1;
		$FormSelection = $this->Get_Form_Selection('YearID', $YearID, 'js_Reload_Selection(this.value)', $noFirst=1, $isAll=0, $firstTitle='', $CheckClassTeacher);
		
		# Get Report Selection
		$ReportSelection = $this->Get_Report_Template_Selection("ReportID","","js_Reload_Comment_Table()");
		
		### Remarks Area
		$StudentSelectionRemarks = $this->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ChooseStudentRemarks'], $others="");
				
		### Choose Member Btn
		$btn_ChooseMember = $this->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "javascript:js_Select_Student_Pop_up();");
		
		### Auto-complete ClassName ClassNumber StudentName search
		$UserClassNameClassNumberInput = '';
		$UserClassNameClassNumberInput .= '<div style="float:left;">';
			$UserClassNameClassNumberInput .= '<input type="text" id="UserClassNameClassNumberSearchTb" name="UserClassNameClassNumberSearchTb" value="" />';
		$UserClassNameClassNumberInput .= '</div>';
		
		### Auto-complete login search
		$UserLoginInput = '';
		$UserLoginInput .= '<div style="float:left;">';
			$UserLoginInput .= '<input type="text" id="UserLoginSearchTb" name="UserLoginSearchTb" value="" />';
		$UserLoginInput .= '</div>';
		
		### Student Selection Box & Remove all Btn
		$MemberSelectionBox = $this->GET_SELECTION_BOX(array(), "name='SelectedUserIDArr[]' id='SelectedUserIDArr[]' class='select_studentlist' size='15' multiple='multiple'", "");
		$btn_RemoveSelected = $this->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "js_Remove_Selected_Student();");
		
		### Continue Button
		$btn_Submit = $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", $onclick="js_Add_Comment_To_Student();", $id="Btn_Submit");
				
		### Cancel Button
		$btn_Cancel = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Go_Back_To_Comment_List()", $id="Btn_Cancel");
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post" onsubmit="return false;">'."\n";
			$x .= '<table width="100%" border="0" cellpadding"0" cellspacing="0">'."\n";
				$x .= '<tr><td colspan="2" class="navigation">'.$PageNavigation.'</td></tr>'."\n";
			$x .= '<table>'."\n";
			
			$x .= '<br style="clear:both"/>'."\n";
			
			$x .= '<table width="95%" border="0" cellpadding="3" cellspacing="0">'."\n";
				### Selected Comment(s)
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2">'.$this->GET_NAVIGATION2($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['SelectedComment']).'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2">'."\n";
						$x .= $this->Get_Management_ClassTeacherComment_CommentView_Table('', $ViewMode=1, $CommentIDArr);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				$x .= '<tr><td colspan="2">&nbsp;</td></tr>'."\n";
				
				### Form and Report Card Selection
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2">'.$this->GET_NAVIGATION2($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ChooseReportCard']).'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td nowrap="nowrap" class="field_title">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Form'].'</td>'."\n";
					$x .= '<td class="tabletext">'.$FormSelection.'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td nowrap="nowrap" class="field_title">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Report'].'</td>'."\n";
					$x .= '<td class="tabletext"><div id="ReportSelectionDiv">'.$ReportSelection.'</div></td>'."\n";
				$x .= '</tr>'."\n";
				
				$x .= '<tr><td colspan="2">&nbsp;</td></tr>'."\n";
				
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2">'."\n";
						$x .= $this->GET_NAVIGATION2($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ChooseStudent']);
						$x .= '<br />'."\n";
						$x .= $StudentSelectionRemarks;
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Group Info
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2">'."\n";
						$x .= '<table width="100%" border="0" cellpadding"0" cellspacing="0" align="center">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td class="tabletext" width="40%">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ChooseStudent'].'</td>'."\n";
								$x .= '<td class="tabletext"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>'."\n";
								$x .= '<td class="tabletext" width="60%">'.$Lang['eBooking']['Settings']['ManagementGroup']['SelectedUser'].'</td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr>
										<td class="tablerow2" valign="top">
											<table width="100%" border="0" cellpadding="3" cellspacing="0">
											<tr>
												<td class="tabletext">'.$i_general_from_class_group.'</td>
											</tr>
											<tr>
												<td class="tabletext">'.$btn_ChooseMember.'</td>
											</tr>
											<tr>
												<td class="tabletext"><i>'.$Lang['General']['Or'].'</i></td>
											</tr>
											<tr>
												<td class="tabletext">
													'.$i_general_search_by_inputformat.'
													<br />
													'.$UserClassNameClassNumberInput.'
												</td>
											</tr>
											<tr>
												<td class="tabletext"><i>'.$Lang['General']['Or'].'</i></td>
											</tr>
											<tr>
												<td class="tabletext">
													'.$i_general_search_by_loginid.'
													<br />
													'.$UserLoginInput.'
												</td>
											</tr>
											</table>
										</td>
										<td class="tabletext" ><img src="'. $image_path.'/'.$LAYOUT_SKIN.'"/10x10.gif" width="10" height="1"></td>
										<td align="left" valign="top">
											<table width="100%" border="0" cellpadding="5" cellspacing="0">
											<tr>
												<td align="left">
													'. $MemberSelectionBox .'
													'.$btn_RemoveSelected.'
												</td>
											</tr>
											<tr>
												<td>
													'.$this->Get_Thickbox_Warning_Msg_Div("MemberSelectionWarningDiv", '* '.$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['SelectionContainsMember']).'
												</td>
											</tr>
											</table>
										</td>
									</tr>'."\n";
							$x .= '<tr><td>&nbsp;</td></tr>'."\n";
							$x .= '<tr><td colspan="4"><div class="edit_bottom"><br />'.$btn_Submit.'&nbsp;'.$btn_Cancel.'</div></td></tr>'."\n";
						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<input type="hidden" id="Keyword" name="Keyword" value="'.intranet_htmlspecialchars($Keyword).'" />'."\n";
			$x .= '<input type="hidden" name="CommentIDArr" id="CommentIDArr" value="'.rawurlencode(serialize($CommentIDArr)).'">'."\n";
		$x .= '</form>'."\n";
		
		return $x;
	}
#
# Management > Class Teacher Comment End
#######################################################################################################

#######################################################################################################
# Setting  > Comment Comment Start
#
	function Get_Settings_CommentBank_UI($field, $order, $page, $Keyword,$commentType='')
	{
		global $Lang,$eRC_Rubrics_ConfigArr;
		
	/*	if ($CommentType == "subject") {	
			// Subject fliter
			$SubjectArray = $lreportcard->GET_ALL_SUBJECTS(1);
			$SubjectOption = array();
			$SubjectSelect = "";
			if (sizeof($SubjectArray) > 0) {
				foreach ($SubjectArray as $tempSubjectID => $tempSubjectDetail) {
					$array_subject_data[] = $tempSubjectID;
					$array_subject_name[] = $tempSubjectDetail[0];
				}
				$SelectedSubject = (isset($SubjectID) && !empty($SubjectID)) ? $SubjectID : $SubjectOption[0][0];
				$SubjectSelect = getSelectByValueDiffName($array_subject_data, $array_subject_name , 'name="SubjectID" id="SubjectID" onchange="jsSubjectFilter()"', $SelectedSubject, 1, 0, $eReportCard['AllSubjects']);
			}
			
			$filterbar = $SubjectSelect;
			
			if (isset($SubjectID) && !empty($SubjectID)) {
				// Comment category filter
				$commentCat = $lreportcard->GET_COMMENT_CAT($SubjectID);
				$array_cat_name = $commentCat;
				$array_cat_data = $commentCat;
				$select_cat = getSelectByValueDiffName($array_cat_data, $array_cat_name, "name='cat' onChange='jsCatFilter()'",$cat,1,0, $eReportCard['AllCategories']);
				
				$filterbar .= $select_cat;
			}
		} else {
			$filterbar = "";
		}*/
		 
	//	$toolbar = $this->Get_Content_Tool_v30("new", "new.php");
	
		$MaxTopicLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];

		$toolbar = $this->Get_Content_Tool_v30("new", "edit.php?commentType=".$commentType);
		$toolbar .= $this->Get_Content_Tool_v30("import", "import.php?commentType=".$commentType);
		$toolbar .= $this->Get_Content_Tool_v30("export", "javascript:js_Go_Export();");
		
		# SearchBox
		$SearchBox = $this->Get_Search_Box_Div('Keyword', $Keyword);
		
		if ($commentType=='class') {
			$DBtable = $this->Get_Settings_CommentBank_DBTable($field, $order, $page, $Keyword,'class');
		}
		
		if($commentType=='subject')
		{
			$filterbar = '<div style="float:left;" id="SubjectSelectionDiv">'.$SubjectSelection.'</div>
							<div style="float:left;">&nbsp;&nbsp;</div>';
			for ($thisLevel=1; $thisLevel<=$MaxTopicLevel; $thisLevel++)
			{
				$thisDivID = "Level".$thisLevel."_SelectionDiv";
				$filterbar .= '<div id="'.$thisDivID.'" class="LevelSelectionDiv" style="float:left;"></div>'."\n";
				$filterbar .= '<div style="float:left;">&nbsp;&nbsp;</div>'."\n";
			}
		
			$filter_html = '<tr>
								<td>
									<div class="table_filter">
										'. $filterbar.'
										<p style="clear: both;">
									</div>
								</td>
							</tr>';
		}
		
		$actionPage = ($commentType=='class')? 'index.php' : 'index_subject.php';
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post" action="'.$actionPage.'" onsubmit="return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";

				$x .= '<tr>'."\n";	
					$x .= '<td>'."\n";
	
						# Main Table  
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div class="content_top_tool">'."\n";
										$x .= '<div class="Conntent_tool">'."\n";
											$x .= $toolbar."\n";
										$x .= '</div>'."\n";
										$x .= '<div class="Conntent_search">'."\n";
											$x .= $SearchBox."\n";
										$x .= '</div>'."\n";
										$x .= '<br style="clear: both;">'."\n";
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
								
							$x .= $filter_html;
												
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div class="common_table_tool">'."\n";
								        $x .= '<a class="tool_edit" href="javascript:checkEdit(document.form1,\'CommentID[]\',\'edit.php\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
										$x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'CommentID[]\',\'remove.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
									$x .= '</div>'."\n";		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="CommentTableDiv">'."\n";
										$x .= $DBtable;
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						  
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<input type="hidden" id="commentType" name="commentType" value="'.$commentType.'" />';
			$x .= '<input type="hidden" id="forImportSample" name="forImportSample" value="0" />';
			$x .= '<input type="hidden" id="FilterTopicString" name="FilterTopicString" value="" />';
		$x .= '</form>'."\n";

		return $x;
	}
	
	function Get_Settings_CommentBank_DBTable($field, $order, $pageNo, $Keyword,$commentType='',$subjectID='',$FilterTopicStr='')
	{
		global $Lang,$PATH_WRT_ROOT, $page_size, $ck_settings_comment_bank_page_size;
		
		include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
	
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
		$lreportcard_comment = new libreportcardrubrics_comment();
		
		# TABLE SQL
		if (isset($ck_settings_comment_bank_page_size) && $ck_settings_comment_bank_page_size != "") $page_size = $ck_settings_comment_bank_page_size;
		$pageSizeChangeEnabled = true;

		if (!isset($field) || $field=='') $field = 0;
		if (!isset($order) || $order=='') $order = 1;

		$li = new libdbtable2007($field, $order, $pageNo);

		$CommentField = Get_Lang_Selection("CommentCh","CommentEn");
		$sql = $lreportcard_comment->Get_Settings_CommentBank_DBTable_Sql($Keyword,$ForExport=0,$commentType,$subjectID,$FilterTopicStr);
	
		$li->field_array = array("CommentCode", "$CommentField");
		$li->column_array = array(0,18);
		$li->wrap_array = array(0,0);
		
		// TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
		$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Code'])."</th>\n";
		
		if($commentType=='subject')
		{
			$li->column_list .= "<th width='25%'>".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Comment'])."</th>\n";
			$li->column_list .= "<th width='65%'>".$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Subject']."</th>\n";
		}
		else
		{
			$li->column_list .= "<th width='90%'>".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Comment'])."</th>\n";
		}
		
		$li->column_list .= "<th width='1'>".$li->check("CommentID[]")."</th>\n";
				
		$li->no_col = sizeof($li->field_array)+2;	
		$li->IsColOff = "IP25_table";
		if($commentType=='subject')
		{
			$li->IsColOff = "ERC_rubrics_CommentBank_SubjectDetailsComment";
			$li->no_col = sizeof($li->field_array)+4;
		}	
		
		// temp save the export SQL first
		$li->sql = $lreportcard_comment->Get_Settings_CommentBank_DBTable_Sql($Keyword, $ForExport=1,$commentType,$subjectID,$FilterTopicStr);
		$ExportSQLTemp = $li->built_sql();
		
		
		$LimitIndex = strripos($ExportSQLTemp, "LIMIT");
		$ExportSQL = substr($ExportSQLTemp, 0, $LimitIndex);
	
		// change back to display SQL
		$li->sql = $sql;
	//debug_r($sql);	
		$x = '';
		$x .= $li->display();
		$x .= '<input type="hidden" name="pageNo" value="'.$li->pageNo.'" />'."\n";
		$x .= '<input type="hidden" name="order" value="'.$li->order.'" />'."\n";
		$x .= '<input type="hidden" name="field" value="'.$li->field.'" />'."\n";
		$x .= '<input type="hidden" name="page_size_change" value="" />'."\n";
		$x .= '<input type="hidden" name="numPerPage" value="'.$li->page_size.'" />'."\n";
		$x .= '<input type="hidden" name="ExportSQL" value="'.intranet_htmlspecialchars($ExportSQL).'" />'."\n";
		
		return $x;		
		
	}
	
	
	
	function Get_Settings_CommentBank_New_Edit_UI($CommentID='', $commentType='')
	{
		global $Lang, $eRC_Rubrics_ConfigArr, $PATH_WRT_ROOT,$commentType;
				
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
		
		$lreportcard_comment = new libreportcardrubrics_comment();	
		$lreportcard_topic = new libreportcardrubrics_topic();	
		
		$MaxTopicLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		
		if($commentType=='class') {
			$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ClassTeacherComment'],"index.php");
		}
		else if($commentType=='subject') {
			$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['SubjectDetailsComment'],"index_subject.php");
		}
		$action = "edit_update.php";
        
		//if(trim($CommentID) == '') {
		if(!is_array($CommentID) && trim($CommentID) == '') {
			//$action="new_update.php";
			$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['New']);		
		}
		else {
			$CommentID = is_array($CommentID) ? $CommentID[0] : $CommentID;
			//$action="edit_update.php";
			$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Edit']);

			$CommentInfo = $lreportcard_comment->Get_Comment_Bank_Comment('',0, $CommentID, $returnSubjectComment='');
			//list($CommentCode,$CommentEn,$CommentCh,$out_CommentID,$TopicID) = $CommentInfo[0];
			$CommentCode = $CommentInfo[0]['CommentCode'];
			$CommentEn = $CommentInfo[0]['CommentEn'];
			$CommentCh = $CommentInfo[0]['CommentCh'];
			$TopicID = $CommentInfo[0]['TopicID'];
	
			$thisTopicIDArray = array(); 
			$thisTopicIDArray[] = $TopicID;
			
			$LevelTopicIDArr = array();
			$LevelTopicIDArr = $lreportcard_topic->Get_TopicPathArr_by_LastTopicID($thisTopicIDArray,$Par_basedOnLang=true);
		}		
	
		$SubjectPath_html='';
		
		$SubjectPath_html = '<table style="width:100%">
								<tr>
									<td>
										<div style="float:left;" id="SubjectSelectionDiv"></div>
										<br/><br/>';
																							
									 for ($thisLevel=1; $thisLevel<=$MaxTopicLevel; $thisLevel++) {									 	
										$thisDivID = "Level".$thisLevel."_SelectionDiv";						
										$SubjectPath_html .= '<div id="'.$thisDivID.'" class="LevelSelectionDiv" style="float:left;"></div>';
										$SubjectPath_html .='<br/><br/>';
									 }
	    $SubjectPath_html .= '     </td>
								</tr>
							  </table>';								 
		
		$MaxLengthInfo = $eRC_Rubrics_ConfigArr['MaxLength'];
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post" action="'.$action.'" onsubmit="return checkForm();">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
			 
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
						$x .= '<br><br>';
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";			
						# form table
						$x .= '<table class="form_table_v30">'."\n";
							$x .= '<col class="field_title">';
							$x .= '<col class="field_c">';
					
							if($commentType=="subject")  //if it is "Subject Details Comment"
							{
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Topic'].'</td>';
									$x .= '<td>'."\n";
										$x .= $SubjectPath_html;
									$x .= '</td>';		
								$x .= '</tr>'."\n";
							}
							
							
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Code'].'</td>';
								$x .= '<td>'."\n";
									$x .= '<input id="CommentCode" name="CommentCode" class="textbox_name required" value="'.intranet_htmlspecialchars($CommentCode).'" maxlength="'.$MaxLengthInfo['CommentCode'].'" onkeyup="aj_CheckCode(this.value)">'."\n";
									$x .= '<div id="WarnCommentCode" class="red"></div>'."\n";
								$x .= '</td>';		
							$x .= '</tr>'."\n";
							
							// Comment En
							$textareaID = "CommentEn";
							if ($commentType=="subject") {
								$OtherPar['maxlength'] = $MaxLengthInfo['Marksheet']['Remarks'];
								$InputField = $this->GET_TEXTBOX($textareaID, $textareaID, $CommentEn, $OtherClass='', $OtherPar);
							}
							else {
								$MaxLength = $MaxLengthInfo['ClassTeacherComment'];
								$limitTextAreaEvent = "onKeyDown='limitText(document.getElementById(\"$textareaID\"),".$MaxLength.");return noEsc();'";
								$limitTextAreaEvent .= "onKeyUp='limitText(document.getElementById(\"$textareaID\"),".$MaxLength.");'";
								$InputField = $this->GET_TEXTAREA($textareaID, $CommentEn,70,5,'',0,$limitTextAreaEvent, "required");
							}
							
							
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title" rowspan="2"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['Comment'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['En'].'</span>';
									$x .= '<span class="row_content_v30">'.$InputField.'</span>'."\n";
									$x .= $this->Spacer();
									$x .= '<div id="Warn'.$textareaID.'" class="WarnRequired red"></div>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							// Comment Ch
							$textareaID = "CommentCh";
							if ($commentType=="subject") {
								$OtherPar['maxlength'] = $MaxLengthInfo['Marksheet']['Remarks'];
								$InputField = $this->GET_TEXTBOX($textareaID, $textareaID, $CommentCh, $OtherClass='', $OtherPar);
							}
							else {
								$MaxLength = $MaxLengthInfo['ClassTeacherComment'];
								$limitTextAreaEvent = "onKeyDown='limitText(document.getElementById(\"$textareaID\"),".$MaxLength.");return noEsc();'";
								$limitTextAreaEvent .= "onKeyUp='limitText(document.getElementById(\"$textareaID\"),".$MaxLength.");'";
								$InputField = $this->GET_TEXTAREA($textareaID, $CommentCh,70,5,'',0,$limitTextAreaEvent, "required");
							}
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['Ch'].'</span>';
									$x .= '<span class="row_content_v30">'.$InputField.'</span>'."\n";
									$x .= $this->Spacer();
									$x .= '<div id="Warn'.$textareaID.'" class="WarnRequired red"></div>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						
										
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
			$x .= '<br style="clear:both" />'."\n";
			
			# edit bottom
			$btn_Save = $this->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', 'checkForm();');
			$btn_Cancel = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button','window.history.back();');
			$x .= '<div class="edit_bottom_v30">';
				### Save Buttom
					$x .= $btn_Save."\n";
					$x .= $btn_Cancel."\n";
			$x .= '</div>';
			
			# hidden field

			// it is for topic path retrieved
			$x .= '<input type="hidden" name="TopicPath_SubjectID" id="TopicPath_SubjectID" value="'.$LevelTopicIDArr[$TopicID]['Subject']['SubjectID'].'"/>'."\n";

			foreach((array)$LevelTopicIDArr[$TopicID]['Level'] as $_level => $_topicInfoArr)
			{
				$this_topic_id = $_topicInfoArr['TopicID'];
				$x .= '<input type="hidden" name="TopicPath_TopicID_Level'.$_level.'" id="TopicPath_TopicID_Level'.$_level.'" value="'.$this_topic_id.'"/>'."\n";
			}

			// To get the TopicID in RC_COMMENT (to check whether it is "Class Teacher Comment")
			$x .= '<input type="hidden" name="TopicID_in_RC_COMMENT_BANK" id="TopicID_in_RC_COMMENT_BANK" value="'.$TopicID.'"/>'."\n";
			
			// to get the Level of Last TopicID (e.g  3 )
			$x .= '<input type="hidden" name="LastTopicLevel" id="LastTopicLevel" value="'.$MaxTopicLevel.'"/>'."\n";
			$x .= '<input type="hidden" name="CommentID" id="CommentID" value="'.$CommentID.'"/>'."\n";
			$x .= '<input type="hidden" name="MaxTopicLevel" id="MaxTopicLevel" value="'.$MaxTopicLevel.'"/>'."\n";
			$x .= '<input type="hidden" name="commentType" id="commentType" value="'.$commentType.'"/>'."\n";
		$x .= '</form>'."\n";	
		
		return $x;
	}
	
	function Get_Setting_CommentBank_Import_UI($commentType)
	{
		global  $Lang, $PATH_WRT_ROOT, $lreportcard;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
		$lreportcard_comment = new libreportcardrubrics_comment();
		
		# navaigtion
		if ($commentType == 'class') {
			$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ClassTeacherComment'], "index.php");
		}
		else if ($commentType == 'subject') {
			$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['SubjectDetailsComment'], "index_subject.php");
		}
	 	$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'],'',1);
			
		$x .= '<form id="form1" name="form1" method="post" action="import_confirm.php" onsubmit="return checkForm();" enctype="multipart/form-data">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";

				$x .= '<tr>'."\n";
					$x .= '<td align="right">'."\n";
						$x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
						$x .= '<br><br>'."\n";
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_IMPORT_STEPS(1);
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Warning_Message_Box('',$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportDataInstruction']);
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Setting_CommentBank_Import_Table($commentType);
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<br><br>'."\n";
			
			$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "");
			$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location.href='index.php';");
			$x .= '<div class="edit_bottom_v30">';
				### Close Buttom
				$x .= $SubmitBtn."\n";
				$x .= $CancelBtn."\n";
			$x .= '</div>';		
			
			$x .= $this->GET_HIDDEN_INPUT('commentType', 'commentType', $commentType);
		$x .= '</form>'."\n";	
		
		return $x;
		
	}
	
	function Get_Setting_CommentBank_Import_Table($commentType)
	{
		global $lreportcard, $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
			$x .= '<col class="field_title">';
			$x .= '<col class="field_c">';
			
			# File 
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['File'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<input class="file" type="file" name="userfile" id="userfile">'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		if ($commentType == 'class') {
			$x .= '<a class="tablelink" href="'.GET_CSV("sample_class_teacher_comment.csv").'" target="_blank">'."\n";
		}
		else if ($commentType == 'subject') {
			$x .= '<a class="tablelink" href="export.php?commentType='.$commentType.'&forImportSample=1">'."\n";
		}
		
			$x .= '<img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">'."\n"; 
			$x .= $Lang['eRC_Rubrics']['GeneralArr']['DownloadCSVFile']."\n";
		$x .= '</a>'."\n";
		
		return $x;
	}
	
	function Get_Settings_CommentBank_Tab($currentTab) {
		global $Lang;
		
		$TAGS_OBJ = array();
		$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ClassTeacherComment'], "index.php?CommentType=class&clearCoo=1", ($currentTab=='' || $currentTab=='class'));
		$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['SubjectDetailsComment'],"index.php?CommentType=subject&clearCoo=1", ($currentTab=='subject'));
		
		return $TAGS_OBJ;
	}
	

#
# Setting  > Comment Comment End
#######################################################################################################

#######################################################################################################
# Management  > ECA Service Start
#
	function Get_Management_ECA_Service_UI($ReportID='')
	{
		global  $ck_ReportCard_Rubrics_UserType, $UserID, $PATH_WRT_ROOT, $lreportcard;
		
		# Get Report Selection
		$ReportSelection = $this->Get_Report_Template_Selection("ReportID",$ReportID,"aj_Reload_Form_Table()",0,0);
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td valign="bottom">'."\n";
						$x .= '<div class="table_filter" style="padding:2px;">'.$ReportSelection.'</div>'."\n";
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td><div id="ECATableDiv"><!-- load by ajax --></div></td>';		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";	
		
		return $x;		
	}

	function Get_Management_ECA_Service_Table($ReportID='', $ClassLevelID='')
	{
		global $Lang, $ck_ReportCard_Rubrics_UserType, $PATH_WRT_ROOT;
	    
		if(!isset($lreportcard_comment))
		{
			include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
			$lreportcard_eca = new libreportcardrubrics_eca();
		}
		
		$FormArr = BuildMultiKeyAssoc($lreportcard_eca->Get_User_Accessible_Form(),"ClassLevelID");
		$ClassArr = BuildMultiKeyAssoc($lreportcard_eca->Get_User_Accessible_Class(),array("ClassLevelID","ClassID"));
		
		// [2018-1002-1146-08277] added import button
		if(!empty($ClassArr))
		{
		    $ImportBtn = $this->Get_Content_Tool_v30('import', $href='import.php?ReportID='.$ReportID.'&ClassLevelID='.$ClassLevelID);
    		
    		$x .= '<div class="content_top_tool">' . "\n";
        		$x .= '<div class="Conntent_tool">' . "\n";
            		$x .= $ImportBtn . "\n";
            		$x .= '<br style="clear:both" />' . "\n";
        		$x .= '</div>' . "\n";
    		$x .= '</div>' . "\n";
    		$x .= '<br style="clear: both;">' . "\n";
		}
		
		$x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
			# Col group
			$x .= '<col align="left" style="width: 20%;">'."\n";
			$x .= '<col align="left" style="width: 25%;">'."\n";
			$x .= '<col align="left">'."\n";
			$x .= '<col align="left">'."\n";
			$x .= '<col style="width: '.$this->toolCellWidth.'px">'."\n";
	
			# Table header
			$ColSpan = "4";
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th>'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Form'].'</th>';
					$x .= '<th colspan="'.$ColSpan.'" class="sub_row_top">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Class'].'</th>';	
				$x .= '</tr>';
				$x .= '<tr>';
					$x .= '<th >'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Name'].'</th>';
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Name'].'</th>';
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['LastModifiedDate'].'</th>';
					$x .= '<th class="sub_row_top">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['LastModifiedBy'].'</th>';
					$x .= '<th class="sub_row_top"></th>';
				$x .= '</tr>';
				
			$x .= '</thead>';		
			
			# Table body
			$x .= '<tbody>';
			
				# loop term
				foreach((array)$ClassArr as $ClassLevelID => $ClassInfoArr)
				{
					if(!($FormInfo = $FormArr[$ClassLevelID]))
						continue;
					
					$NumOfClass = count($ClassInfoArr) + 1;
					
					$x .= '<tr>';
						$x .= '<td rowspan="'.$NumOfClass.'">'.$FormInfo['LevelName'].'</td>';
					$x .= '</tr>';
					
					# loop module
					foreach((array)$ClassInfoArr as $thisClassID => $thisClassInfo)
					{
						
						$LastModifiedInfo = $lreportcard_eca->Get_ECA_Latest_Modification($ReportID,$thisClassID);
						$LastModifiedDate = $LastModifiedInfo["DateModified"]?$LastModifiedInfo["DateModified"]:' - ';
						$LastModifiedBy = $LastModifiedInfo["LastModifiedBy"]?$LastModifiedInfo["LastModifiedBy"]:' - ';
						
						# Edit Link 
						$EditBtn = $this->GET_LNK_EDIT("eca_edit.php?ClassID=$thisClassID&ReportID=$ReportID");
						
						$x .= '<tr class="sub_row">';
							$x .= '<td>'.$thisClassInfo['ClassName'].'</td>';
							$x .= '<td>'.$LastModifiedDate.'</td>';	
							$x .= '<td>'.$LastModifiedBy.'</td>';
							$x .= '<td>';
								$x .= '<div class="table_row_tool row_content_tool"><a href="edit.php?ReportID='.$ReportID.'&ClassID='.$thisClassID.'" class="edit_dim"></a></div>';
							$x .= '</td>';
						$x .= '</tr>';
					}
					// end loop module
				}
				// end loop term
				
			$x .= '</tbody>';
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Management_ECA_Service_Edit_UI($ReportID='', $ClassID='')
	{
		global  $Lang, $PATH_WRT_ROOT, $lreportcard_eca;
		
		if(!isset($lreportcard_eca))
		{
			include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
			$lreportcard_eca = new libreportcardrubrics_eca();
		}
		
		# Report Title
		$ReportInfo = $lreportcard_eca->GetReportTemplateInfo($ReportID);
		$ReportTitleField = Get_Lang_Selection("ReportTitleCh","ReportTitleEn");		

		# Class Title
		$ClassInfo = $lreportcard_eca->GET_CLASSES_BY_FORM('',$ClassID);
		$ClassTitleField = Get_Lang_Selection("ClassTitleCh","ClassTitleEn");
		
		# Table (Load by Ajax)
		$ECATable = $this->Get_Ajax_Loading_Image();
		
		# Class Level Title
		$ClassLevelID = $lreportcard_eca->Get_ClassLevel_By_ClassID($ClassID);
		$YearName = $lreportcard_eca->returnClassLevel($ClassLevelID);

		# Get Category Selection 
		$CategorySelection = $this->Get_ECA_Category_Selection("EcaCategoryID",$EcaCategoryID,$Onchange='js_Reload_Cat_Selection();', $noFirst=0, $isAll=1);
		$SubCategorySelection = $this->Get_Ajax_Loading_Image(); # (Load by Ajax)
		
		# Buttons
		$btn_Save = $this->GET_ACTION_BTN($Lang['Btn']['Save'], "button","document.form1.submit();");
		$btn_Cancel = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button","window.location.href='eca.php?ReportID=$ReportID';");

		# navigation
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['MenuTitle'], "eca.php"); 
		$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");

		$x = '';
		$x .= '<form id="form1" name="form1" method="post" action="edit_update.php">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION)."\n";
						$x .= '<br><br>'."\n";	
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";	
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<table class="form_table_v30">'."\n";
							$x .= '<col class="field_title">';
							$x .= '<col class="field_c">';
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Report'].'</td>';
								$x .= '<td>'.$ReportInfo[0][$ReportTitleField].'</td>';		
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Class'].'</td>';
								$x .= '<td>'.$ClassInfo[0]["ClassName"].'</td>';		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";			
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<span id="CategorySelectionSpan">'."\n";
							$x .= $CategorySelection."\n";
						$x .= '</span>'."\n";
						$x .= '<span id="SubCategorySelectionSpan">'."\n";
							$x .= $SubCategorySelection."\n";
						$x .= '</span>'."\n";  
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";	
				$x .= '<tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td><div id="EcaTableDiv">'.$ECATable.'</div></td>';		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= $this->ArchiveStudentField()."\n";	
			$x .= '<br style="clear:both" />'."\n";
			$x .= '<div class="edit_bottom_v30">';
				### Save Buttom
					$x .= $btn_Save."\n";
					$x .= $btn_Cancel."\n";
			$x .= '</div>';
			
			# hidden field
			$x .= '<input type="hidden" name="ReportID" id="ReportID" value="'.$ReportID.'">'."\n";
			$x .= '<input type="hidden" name="ClassID" id="ClassID" value="'.$ClassID.'">'."\n";
		$x .= '</form>'."\n";	
		
		return $x;		
	}
	
	function Get_Management_ECA_Edit_Table($ReportID, $ClassID, $CategoryID='', $SubCatgoryID='')
	{
		global  $Lang, $PATH_WRT_ROOT ;
		
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
		$lreportcard_eca = new libreportcardrubrics_eca();
		
		$StudentList = $lreportcard_eca->GET_STUDENT_BY_CLASS($ClassID,"",0,0,1);
		
		# Get Cat, Subcat, Item Mapping 
		$RawECACatItemList = $lreportcard_eca->Get_ECA_Category_Item_Info($CategoryID,$SubCatgoryID,$ItemID, 1);
		$ECACatItemList = BuildMultiKeyAssoc($RawECACatItemList, array("EcaCategoryID","EcaSubCategoryID","EcaItemID"));
		$ECAItemList = BuildMultiKeyAssoc($RawECACatItemList, array("EcaItemID"));
		$ItemWidth = count($ECAItemList)>0?75/count($ECAItemList)."%":0;
		
		# Get Exist Student Item Mapping
		$StudentItemArr = $lreportcard_eca->Get_Report_Student_ECA_Record($ReportID, $ClassID);
		$StudentItemArr = BuildMultiKeyAssoc($StudentItemArr , array("StudentID","EcaItemID"),"EcaItemID",1);
			
		foreach((array)$ECACatItemList as $CategoryID => $SubCatItemArr)
		{
			$NoOfItemOfCat = 0;
			
			# Get Category Info 
			$NoOfSubCat = count($SubCatItemArr);
			$CatInfo = $lreportcard_eca->Get_ECA_Category_Info($CategoryID);
			$CatName = Get_Lang_Selection($CatInfo[0]['EcaCategoryNameCh'],$CatInfo[0]['EcaCategoryNameEn']);
			$CatName = intranet_htmlspecialchars($CatName);
			
			foreach((array)$SubCatItemArr as $SubCatID => $ItemArr)
			{
				# Get SubCat Info
				$NoOfItem = count($ItemArr);
				$SubCatInfo = $lreportcard_eca->Get_ECA_SubCategory_Info($CategoryID,$SubCatID);
				$SubCatName = Get_Lang_Selection($SubCatInfo[0]['EcaSubCategoryNameCh'],$SubCatInfo[0]['EcaSubCategoryNameEn']);
				$SubCatName = intranet_htmlspecialchars($SubCatName);
				
				$SubCatNameRow .= '<th style="text-align:center" colspan="'.$NoOfItem.'">'.$SubCatName.'</th>';
				foreach((array)$ItemArr as $ItemID => $ItemInfo)
				{
					$ItemName = Get_Lang_Selection($ItemInfo['EcaItemNameCh'],$ItemInfo['EcaItemNameEn']);
					$ItemName = intranet_htmlspecialchars($ItemName);
					$ItemNameRow .= '<th style="text-align:center; width:'.$ItemWidth.'">'.$ItemName.'</th>';
				} // end loop Item
				
				$NoOfItemOfCat += $NoOfItem;
			} // end loop SubCategory
			
			$CatNameRow .= '<th style="text-align:center" colspan="'.$NoOfItemOfCat.'">'.$CatName.'</th>';
		} // end loop Category
		
		$x .= '<table id="EcaEditTable" class="common_table_list_v30 edit_table_list_v30" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th rowspan="3">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ClassNo'].'</th>'."\n";
					$x .= '<th rowspan="3">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Student'].'</th>'."\n";
					$x .= $CatNameRow."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= $SubCatNameRow."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= $ItemNameRow."\n";		
				$x .= '</tr>'."\n";		
				$x .= '<tr class="edit_table_head_bulk">'."\n";
					$x .= '<th>&nbsp;</th>'."\n";
					$x .= '<th>&nbsp;</th>'."\n";
					foreach((array)$ECAItemList as $ItemID => $ItemInfo)
					{
						$thisCheckBox = $this->Get_Checkbox("CheckAll_ItemChk_$ItemID", $Name="", $Value="", $isChecked="", $Class="", $Display='', $Onclick="CheckAll(this,$ItemID)");
						$x .= '<th style="text-align:center">'.$thisCheckBox.'</th>'."\n";	
					}
				$x .= '</tr>'."\n";		
			$x .= '</thead>'."\n";				
			$x .= '<tbody>'."\n";

				foreach((array)$StudentList as $key => $StudentInfo)
				{
					$StudentID = $StudentInfo['UserID'];
					
					$x .= '<tr>'."\n";
						$x .= '<td>'.$StudentInfo['ClassNumber'].'</td>'."\n";
						$x .= '<td>'.$StudentInfo['StudentName'].'</td>'."\n";
						
						foreach((array)$ECAItemList as $ItemID => $ItemInfo)
						{
							$isChecked = $StudentItemArr[$StudentID][$ItemID]?1:0;
							$thisCheckBox = $this->Get_Checkbox("StudentItemArr_{$StudentID}_{$ItemID}", "StudentItemArr[$StudentID][]", $Value=$ItemID, $isChecked, $Class="ItemChk_$ItemID", $Display='', $Onclick="UncheckAll($ItemID)");
							$x .= '<td style="text-align:center">'.$thisCheckBox.'</td>'."\n";	
						}
								
					$x .= '</tr>'."\n";
				}
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";	
		
		return $x;
	}
	
	function Get_Management_ECA_Import_UI($ReportID, $ClassLevelID="")
	{
	    global $PATH_WRT_ROOT, $lreportcard, $Lang;
	    
	    $backUrl = "eca.php?ReportID=".$ReportID."&ClassLevelID=".$ClassLevelID;
	    
	    # Page Navigation
	    $PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['MenuTitle'], $backUrl);
	    $PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
	    
	    $x .= '<form id="form1" name="form1" method="post" action="import_step2.php" onsubmit="return checkForm();" enctype="multipart/form-data">'."\n";
	    $x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
        	    $x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
        	    $x .= '<br>'."\n";
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
    	        $x .= $this->GET_IMPORT_STEPS(1);
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
    	        $x .= $this->Get_Warning_Message_Box('', $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportDataInstruction']);
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '<tr>'."\n";
    	    $x .= '<td>'."\n";
    	        $x .= $this->Get_Management_ECA_Import_Table($ReportID, $ClassLevelID);
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '</table>'."\n";
	    $x .= '<br>'."\n";
	    
	    # Button
	    $SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "");
	    $CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location.href='".$backUrl."'");
	    
	    $x .= '<div class="edit_bottom_v30">';
    	    $x .= $SubmitBtn."\n";
    	    $x .= '&nbsp;'."\n";
    	    $x .= $CancelBtn."\n";
	    $x .= '</div>';
	    
	    $x .= '<input type="hidden" id="ReportID" name="ReportID" value="'.$ReportID.'">'."\n";
	    $x .= '<input type="hidden" id="ClassLevelID" name="ClassLevelID" value="'.$ClassLevelID.'">'."\n";
	    $x .= '</form>'."\n";
	    
	    return $x;
	}
	
	function Get_Management_ECA_Import_Table($ReportID, $ClassLevelID='')
	{
	    global $lreportcard, $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
	    
	    $ReportTemplateInfo = $lreportcard->GetReportTemplateInfo($ReportID);
	    $ReportTitle = Get_Lang_Selection($ReportTemplateInfo[0]['ReportTitleCh'],$ReportTemplateInfo[0]['ReportTitleEn']);
	    
	    $x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
	    $x .= '<col class="field_title">';
	    $x .= '<col class="field_c">';
	    
	    # Report
	    $x .= '<tr>'."\n";
    	    $x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['GeneralArr']['Report'].'</td>'."\n";
    	    $x .= '<td>'."\n";
    	        $x .= $ReportTitle."\n";
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    
	    # File
	    $x .= '<tr>'."\n";
    	    $x .= '<td class="field_title" rowspan="2">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['File'].'</td>'."\n";
    	    $x .= '<td>'."\n";
    	        $x .= '<input class="file" type="file" name="userfile" id="userfile">'."\n";
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	    $x .= '<tr>'."\n";
	        $x .= '<td>'."\n";
	            $x .= '<a class="contenttool" href="javascript:jGEN_CSV_TEMPLATE()">'.$Lang['eRC_Rubrics']['GeneralArr']['GenerateCSVFile'].'</a>'."\n";
	        $x .= '</td>'."\n";
        $x .= '</tr>'."\n";
        
        # Remarks
        $x .= '<tr>'."\n";
	        $x .= '<td class="field_title" rowspan="2">'.$Lang['eRC_Rubrics']['GeneralArr']['Remarks'].'</td>'."\n";
	        $x .= '<td>'."\n";
	            $x .= $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportDataRemarks'];
	        $x .= '</td>'."\n";
        $x .= '</tr>'."\n";
	    
	    $x .= '</table>'."\n";
	    
	    return $x;
	}
	
	function Get_Management_ECA_Import_Step2_UI($ReportID, $ClassLevelID="", $targetFilePath, $csvDataSize)
	{
	    global $Lang, $PATH_WRT_ROOT;
	    
	    # Navaigtion
	    $PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['MenuTitle'], "javascript:js_Cancel();");
	    $PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
	    
	    $x = '';
	    $x .= '<form id="form1" name="form1" method="post" action="import_step3.php">'."\n";
	    $x .= '<div class="table_board">'."\n";
            $x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
            $x .= '<tr>'."\n";
                $x .= '<td>'."\n";
                    $x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
                    $x .= '<br>'."\n";
                $x .= '</td>'."\n";
            $x .= '</tr>'."\n";
            $x .= '<tr>'."\n";
                $x .= '<td>'."\n";
                    $x .= $this->GET_IMPORT_STEPS(2);
                $x .= '</td>'."\n";
            $x .= '</tr>'."\n";
            $x .= '<tr>'."\n";
                $x .= '<td>'."\n";
                    $x .= '<table class="form_table_v30">'."\n";
                    $x .= '<tr>'."\n";
                        $x .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
                        $x .= '<td><div id="SuccessCountDiv">'.$Lang['General']['EmptySymbol'].'</div></td>'."\n";
                    $x .= '</tr>'."\n";
                    $x .= '<tr>'."\n";
                        $x .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
                        $x .= '<td><div id="FailCountDiv">'.$Lang['General']['EmptySymbol'].'</div></td>'."\n";
                    $x .= '</tr>'."\n";
                    $x .= '</table>'."\n";
                    $x .= '<br style="clear:both;" />'."\n";
            
                    $x .= '<div id="ErrorTableDiv"></div>'."\n";
                $x .= '</td>'."\n";
            $x .= '</tr>'."\n";
            $x .= '</table>'."\n";
        $x .= '</div>'."\n";
        
        # Button
        $ContinueBtn = $this->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();", 'ContinueBtn', '', $Disabled=1);
        $BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();");
        $CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Cancel();");
	    
	    $x .= '<div class="edit_bottom_v30">'."\n";
    	    $x .= $ContinueBtn;
    	    $x .= '&nbsp;'."\n";
    	    $x .= $BackBtn;
    	    $x .= '&nbsp;'."\n";
    	    $x .= $CancelBtn;
	    $x .= '</div>'."\n";
	    
	    # iFrame for Validation
	    $thisSrc = "ajax_validate.php?action=Import_Student_ECA&targetFilePath=".$targetFilePath;
	    $x .= '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%; height:300px; display:none;"></iframe>'."\n";
	    
	    # Hidden Field for Step 3 use
	    $x .= '<input type="hidden" id="ReportID" name="ReportID" value="'.$ReportID.'">'."\n";
	    $x .= '<input type="hidden" id="ClassLevelID" name="ClassLevelID" value="'.$ClassLevelID.'">'."\n";
	    $x .= '<input type="hidden" id="csvDataSize" name="csvDataSize" value="'.$csvDataSize.'" />'."\n";
	    
	    $x .= '</form>'."\n";
	    $x .= '<br />'."\n";
	    
	    return $x;
	}
	
	function Get_Management_ECA_Import_Step3_UI($ReportID, $ClassLevelID="")
	{
	    global $Lang, $PATH_WRT_ROOT;
	    
	    # Navaigtion
	    $PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['MenuTitle'], "javascript:js_Back_To_ECA();");
	    $PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
	    
        $x = '';
        $x .= '<form id="form1" name="form1" method="post">'."\n";
        $x .= '<div class="table_board">'."\n";
            $x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
            $x .= '<tr>'."\n";
                $x .= '<td>'."\n";
                    $x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
                    $x .= '<br>'."\n";
                $x .= '</td>'."\n";
            $x .= '</tr>'."\n";
            $x .= '<tr>'."\n";
                $x .= '<td>'."\n";
                    $x .= $this->GET_IMPORT_STEPS(3);
                $x .= '</td>'."\n";
            $x .= '</tr>'."\n";
            $x .= '<tr>'."\n";
                $x .= '<td>'."\n";
                    $x .= '<table class="form_table_v30">'."\n";
                    $x .= '<tr>'."\n";
                        $x .= '<td class="tabletext" style="text-align:center;">'."\n";
                            $x .= '<span id="ImportStatusSpan"></span>'."\n";
                        $x .= '</td>'."\n";
                    $x .= '</tr>'."\n";
                    $x .= '</table>'."\n";
                $x .= '</td>'."\n";
            $x .= '</tr>'."\n";
            $x .= '</table>'."\n";
        $x .= '</div>'."\n";
        $x .= '<br style="clear:both;" />'."\n";
        
        # Button
        $ImportOtherBtn = $this->GET_ACTION_BTN($Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportOtherECAService'], "button", "js_Back_To_Import_Step1();");
        $BackBtn = $this->GET_ACTION_BTN($Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['BackToECAService'], "button", "js_Back_To_ECA();");
	    
	    $x .= '<div class="edit_bottom_v30">'."\n";
    	    $x .= $ImportOtherBtn;
    	    $x .= '&nbsp;'."\n";
    	    $x .= $BackBtn;
	    $x .= '</div>'."\n";
	    
	    # iFrame for importing data
	    $thisSrc = "ajax_update.php?action=Import_Student_ECA&ReportID=".$ReportID."&ClassLevelID=".$ClassLevelID."&csvDataSize=".$_POST['csvDataSize'];
	    $x .= '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%; height:300px; display:none;"></iframe>'."\n";
	    
	    $x .= '</form>'."\n";
	    $x .= '<br />'."\n";
	    
	    return $x;
	}
#
# Management  > ECA Service End
#######################################################################################################

#######################################################################################################
# Management  > OtherInfo Start
#
	function getOtherInfoTabObjArr($ParCategoryID='') {
		global $Lang, $PATH_WRT_ROOT, $lreportcard;
		
		# other info in DB
		$OtherInfoCategoryArr = $lreportcard->Get_Other_Info_Category();
		//$OtherInfoCategoryArr = BuildMultiKeyAssoc($OtherInfoCategoryArr, "CategoryCode");
		
		# default summary
		if(trim($ParCategoryID)=='')
			$SelectFirst = 1;
		
		$TAGS_OBJ = array();
		# tag information
		foreach($OtherInfoCategoryArr as $thisCategory)
		{
			list($CategoryID,$CategoryCode,$CategoryNameEn,$CategoryNameCh) = $thisCategory;
			
			$CategoryName = Get_Lang_Selection($CategoryNameCh,$CategoryNameEn);
			$TAGS_OBJ[] = array($CategoryName, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/management/other_info/index.php?CategoryID=$CategoryID", ($ParCategoryID==$CategoryID||$SelectFirst)?1:0);
		
			$SelectFirst = 0;
		}

		return $TAGS_OBJ;
	}

	function Get_Management_OtherInfo_UI($CategoryID='',$ReportID='', $ClassLevelID='', $ClassID='',$field='' ,$order='' ,$page='')
	{
		global $Lang;

		$OtherInfoTable = $this->Get_Management_OtherInfo_Table($CategoryID,$ReportID, $ClassLevelID, $ClassID, $field, $order, $page);
		$ReportSelection = $this->Get_Report_Template_Selection("ReportID", $ReportID, 'this.form.submit()' ,0,1);
		$AllFormTitle = $Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['AllForm'];
		$FormSelection = $this->Get_Form_Selection("ClassLevelID", $ClassLevelID, $Onchange='this.form.submit()', $noFirst=0, $isAll=1, $AllFormTitle, $TeachingFormOnly=1);
		$AllClassTitle = $Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['AllClass']; 
		$ClassSelection = $this->Get_Class_Selection("ClassID", $ClassID, $Onchange='this.form.submit();', $noFirst=0, $isAll=1, $AllClassTitle, $ClassLevelID, $TeachingClassOnly=1);
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post" action="index.php" return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
					
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td colspan="2">'."\n";
									$x .= '<div class="content_top_tool">'."\n";
											$x .= $this->GET_LNK_UPLOAD("javascript:void(0);", "js_Upload()")."\n";
										$x .= $this->Spacer()."\n";
									$x .= '</div>'."\n";
									$x .= '<br/>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr class="table-action-bar">'."\n";
								$x .= '<td valign="bottom">'."\n";
									$x .= '<div class="table_filter">'."\n";
										$x .= '<span id="ReportSelection">'.$ReportSelection.'</span>'."\n";
										$x .= '<span id="FormSelection">'.$FormSelection.'</span>'."\n";
										$x .= '<span id="ClassSelection">'.$ClassSelection.'</span>'."\n";
									$x .= '</div>'."\n";		
								$x .= '</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div class="common_table_tool">'."\n";
										$x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'CSVFile[]\',\'file_remove_update.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
									$x .= '</div>'."\n";		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td colspan="2">'."\n";
									$x .= $OtherInfoTable;
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			# hidden field
			$x .= '<input type="hidden" id="CategoryID" name="CategoryID" value="'.$CategoryID.'">'."\n";
				
		$x .= '</form><br><br>'."\n";
		
		return $x;			
	}	
	
	function Get_Management_OtherInfo_Table($CategoryID='', $ReportID='', $ClassLevelID='', $ClassID='',$field='' ,$order='' ,$page='')
	{
		global $Lang, $ck_settings_other_info_page_size, $page_size ;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		include_once("libreportcardrubrics_module.php");
		$lreportcard = new libreportcardrubrics();
		
		$field = ($field=='')? 1 : $field;
		$order = ($order=='')? 1 : $order;
		$page = ($page=='')? 1 : $page;
		
		if (isset($ck_settings_other_info_page_size) && $ck_settings_other_info_page_size != "") $page_size = $ck_settings_other_info_page_size;
		
		$li = new libdbtable2007($field,$order,$page);
		
		$sql = $lreportcard->GetOtherInfoDBTableSql($CategoryID,$ReportID,$ClassLevelID,$ClassID);
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
		
		$li->field_array = array("ReportTitle", "YearName", "ClassName", "DateInput");
		$li->column_array = array(0,0,0,0);
		$li->wrap_array = array(0,0,0,0);
		$li->fieldorder2 = ",DisplayOrder ,ClassName";
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
		$li->column_list .= "<th width='20%' >".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['GeneralArr']['Report'])."</th>\n";
		$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['GeneralArr']['Form'])."</th>\n";
		$li->column_list .= "<th width='25%'>".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['GeneralArr']['ClassName'])."</th>\n";
		$li->column_list .= "<th width='20%'>&nbsp;</th>\n";
		$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['GeneralArr']['LastModifiedDate'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("CSVFile[]")."</td>\n";
		$li->no_col = $pos+3;
	
		$li->count_mode = 1;
		
		$x .= $li->display();

		$x .= '<input type="hidden" name="pageNo" value="'.$li->pageNo.'" />';
		$x .= '<input type="hidden" name="order" value="'.$li->order.'" />';
		$x .= '<input type="hidden" name="field" value="'.$li->field.'" />';
		$x .= '<input type="hidden" name="page_size_change" value="" />';
		$x .= '<input type="hidden" name="numPerPage" value="'.$li->page_size.'" />';
	
		return $x;
	}
	
	function Get_Management_OtherInfo_Import_UI($CategoryID='',$ReportID='',$ClassLevelID='',$ClassID='',$msg='',$SysMsg='')
	{
		global $Lang;
		
		$x .= '<br style="clear:both">'."\n";
		$x .= '<form id="form1" name="form1" method="post" action="file_upload_confirm.php" onsubmit="return checkForm();" enctype="multipart/form-data">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td align="right">'."\n";
						$x .= $this->GET_SYS_MSG($msg,$SysMsg);
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_IMPORT_STEPS(1);
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Warning_Message_Box('',$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportDataInstruction']);
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Management_OtherInfo_Import_Table($CategoryID,$ReportID,$ClassLevelID,$ClassID);
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<br><br>'."\n";
			
			$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "");
			//$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location.href='index.php?CategoryID=$CategoryID&ReportID=$ReportID&ClassLevelID=$ClassLevelID&ClassID=$ClassID';");
			$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location.href='index.php?CategoryID=$CategoryID';");
			$x .= '<div class="edit_bottom_v30">';
				### Close Buttom
				$x .= $SubmitBtn."\n";
				$x .= $CancelBtn."\n";
			$x .= '</div>';		
			$x .= '<input type="hidden" id="CategoryID" name="CategoryID" value="'.$CategoryID.'">'."\n";
		$x .= '</form>'."\n";	
		
		return $x;
	}
	
	function Get_Management_OtherInfo_Import_Table($CategoryID='', $ReportID='',$ClassLevelID='', $ClassID='')
	{
		global $lreportcard, $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;

		$ReportSelection = $this->Get_Report_Template_Selection("ReportID",$ReportID,'aj_Check_Record_Exist();');
		$AllFormTitle = $Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['AllForm'];
		$FormSelection = $this->Get_Form_Selection("ClassLevelID", $ClassLevelID, $Onchange='aj_Reload_Class_Selection(); ', $noFirst=1, $isAll=0, '', $TeachingFormOnly=1);
		$AllClassTitle = $Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['AllClass']; 
		//$ClassSelection = $this->Get_Class_Selection("ClassID", $ClassID, $Onchange='aj_Check_Record_Exist();', $noFirst=0, $isAll=1, $AllClassTitle, $ClassLevelID, $TeachingClassOnly=1);
		$ClassSelection = $this->Get_Ajax_Loading_Image();
		
		$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
			$x .= '<col class="field_title">';
			$x .= '<col class="field_c">';
			
			# Report 
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['GeneralArr']['Report'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $ReportSelection."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# Form 
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['GeneralArr']['Form'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $FormSelection."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# Class Name 
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['GeneralArr']['ClassName'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<span id="ClassSelection">'.$ClassSelection.'</span>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# File 
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['File'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<input class="file" type="file" name="userfile" id="userfile">'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		$x .= '<a class="tablelink" href="javascript:void(0)" onclick="js_Export(\''.$CategoryID.'\');">'."\n";
			$x .= '<img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">'."\n"; 
			$x .= $Lang['eRC_Rubrics']['GeneralArr']['DownloadCSVFile']."\n";
		$x .= '</a>'."\n";
		
		return $x;
	}
#
# Management  > OtherInfo End
#######################################################################################################

#######################################################################################################
# Report > SubjectTeachedReport Start
#
	
	function Get_Reports_Subject_Taught_Report($SubjectTaughtArr, $InvlovedSubject,$SubjectDisplay)
	{
		global $Lang, $lreportcard;
		
		# Subject Info
		$SubjectInfo = $lreportcard->Get_Subject_Info();
		switch($SubjectDisplay)
		{
			case "Abbr": $subfix = "ABBR"; break;
			case "Desc": $subfix = "DES"; break;
			case "ShortForm": $subfix = "SNAME"; break;
		}	
		$SubjDisplayLang = Get_Lang_Selection("CH_".$subfix,"EN_".$subfix);
		
		# Student Info
		include_once("libuser.php"); 
		$StudentIDArr = array_keys($SubjectTaughtArr);
		$lu = new libuser('','',$StudentIDArr);
		
		$x .= '<table class="report_table" border=1 cellpadding="2px">'."\n";
			$x .='<tr>'."\n";
				$x .='<td>&nbsp;</td>'."\n";
				foreach((array)$InvlovedSubject as $SubjectID)
				{
					$x .='<td colspan="2">'.$SubjectInfo[$SubjectID][$SubjDisplayLang].'</td>'."\n";
					$row2 .= '<td>'.$Lang['eRC_Rubrics']['ReportsArr']['SubjectTaughtReportArr']['TaughtItem'].'</td>'."\n";
					$row2 .= '<td>'.$Lang['eRC_Rubrics']['ReportsArr']['SubjectTaughtReportArr']['TopicItem'].'</td>'."\n";
				}
			$x .='</tr>'."\n";
			
			$x .= '<td>&nbsp;</td>'.$row2;
			
			foreach((array)$SubjectTaughtArr as $StudentID => $SubjectTeachCount)
			{
				$x .='<tr>'."\n";
					$lu->LoadUserData($StudentID);
					$thisName = $lu->StandardDisplayName." (".$lu->ClassName." - ".$lu->ClassNumber.")"; //Get_Lang_Selection($lu->ChineseName,$lu->EnglishName);
					
					$x .='<td>'.$thisName.'</td>'."\n";
					foreach((array)$InvlovedSubject as $SubjectID)
					{
						$x .='<td align="right">'.$SubjectTeachCount[$SubjectID]['Taught'].'</td>'."\n";
						$x .='<td align="right">'.$SubjectTeachCount[$SubjectID]['Total'].'</td>'."\n";
					}
				$x .='</tr>'."\n";
			}
			
			
		$x .= '</table>'."\n";
		
		return $x;
	}

	function Get_Reports_Topic_Taught_Report($SubjectTaughtArr, $InvlovedTopic, $SubjectID)
	{
		global $Lang, $lreportcard, $lreportcard_topic;
		
		# Subject Info
		$SubjectInfo = $lreportcard->Get_Subject_Info();
		
		$SubjDisplayLang = Get_Lang_Selection("CH_DES","EN_DES");
		
		# Topic Info 
		$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectID,'',$TargetLevel=1);
		$TopicDisplayLang = Get_Lang_Selection("TopicNameCh","TopicNameEn");
		
//		debug_pr($TopicInfoArr);
		# Student Info
		include_once("libuser.php"); 
		$StudentIDArr = array_keys($SubjectTaughtArr);
		$lu = new libuser('','',$StudentIDArr);

		$x .= '<table class="report_table" border=1 cellpadding="2px">'."\n";
			$x .='<tr>'."\n";
				$x .='<td>'.$SubjectInfo[$SubjectID][$SubjDisplayLang].'</td>'."\n";
				foreach((array)$InvlovedTopic as $TopicID)
				{
					$x .='<td colspan="2">'.$TopicInfoArr[$TopicID][$TopicDisplayLang].'</td>'."\n";
					$row2 .= '<td>'.$Lang['eRC_Rubrics']['ReportsArr']['SubjectTaughtReportArr']['TaughtItem'].'</td>'."\n";
					$row2 .= '<td>'.$Lang['eRC_Rubrics']['ReportsArr']['SubjectTaughtReportArr']['TopicItem'].'</td>'."\n";
				}
			$x .='</tr>'."\n";
			
			$x .= '<td>&nbsp;</td>'.$row2;
			
			foreach((array)$SubjectTaughtArr as $StudentID => $SubjectTeachCount)
			{
				$x .='<tr>'."\n";
					$lu->LoadUserData($StudentID);
					$thisName = $lu->StandardDisplayName." (".$lu->ClassName." - ".$lu->ClassNumber.")"; 
					
					$x .='<td>'.$thisName.'</td>'."\n";
					foreach((array)$InvlovedTopic as $TopicID)
					{
						$thisTaught = $SubjectTeachCount[$TopicID]['Taught']?$SubjectTeachCount[$TopicID]['Taught']:0; 
						$x .='<td align="right">'.$thisTaught.'</td>'."\n";
						$x .='<td align="right">'.$SubjectTeachCount[$TopicID]['Total'].'</td>'."\n";
					}
				$x .='</tr>'."\n";
			}
			
			
		$x .= '</table>'."\n";
		
		return $x;
	}
	
#
# Report  > SubjectTeachedReport End
#######################################################################################################


	function Get_Reports_Grand_Marksheet_Index_UI()
	{
		$ctr=0;
		
		global $Lang, $eReportCard, $PATH_WRT_ROOT, $sys_custom, $image_path, $LAYOUT_SKIN ;

		//include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		//include_once($PATH_WRT_ROOT.'includes/form_class_manage_ui.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
		include_once($PATH_WRT_ROOT.'includes/libreportcardrubrics_module.php');
		
		//$fcm_ui = new form_class_manage_ui();
		$scm_ui = new subject_class_mapping_ui();
		$lreportcard_module = new libreportcardrubrics_module();
		
		### Student Display Options
		# Form Selection
		$thisOnChange = 'js_Changed_Form_Selection(this.value);';
		$FormSelection = $this->Get_Form_Selection('YearID', $SelectedYearID='', $thisOnChange, $noFirst=1, $isAll=0, $firstTitle='', $CheckClassTeacher=1);
		
		# Subject Selection
		$SubjectSelection = $scm_ui->Get_Subject_Selection("SubjectIDArr[]", '', $OnChange='ToggleDetailFilter();', $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=1);

		# build module selection $ checkboxes
		$ModuleInfoArr = $lreportcard_module->Get_Module_Info();
		foreach($ModuleInfoArr as $thisModuleInfo)
		{
			list($ModuleID, $ModuleCode, $ModuleNameEn ,$ModuleNameCh) = $thisModuleInfo;
			$ModuleName = Get_Lang_Selection($ModuleNameCh,$ModuleNameEn)."($ModuleCode)";
			
			$ModuleSelectOptions[] = array($ModuleID,$ModuleName);
		}
		$ModuleSelection = getSelectByArray($ModuleSelectOptions," id='ModuleModuleID' name='ModuleID[]' class='ModuleMember' multiple size=10 ",0,0,1);

		# Select All Btn
		$SelectAllClassBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Year_Class('YearClassIDArr[]')");
		$SelectAllModuleBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('ModuleModuleID')");
		$SelectAllSubjectBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_With_OptGroup('SubjectIDArr[]',1); ToggleDetailFilter(); ");
		
		# wrapper
		$x .= '<form id="form1" name="form1" action="print.php" method="POST" target="_blank">';

		$x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td  colspan="2" >'."\n";
				##### Main Content Start

					### Student Display Options Start
					$table_id = "table_student_option";
					
					# prepare data display options
					$DataDisplayOptions = array();
					$DataDisplayOptions["ClassName"] = $Lang['eRC_Rubrics']['GeneralArr']["Class"];
					$DataDisplayOptions["ClassNumber"] = $Lang['eRC_Rubrics']['GeneralArr']["ClassNumber"];
					$DataDisplayOptions["EnglishName"] = $Lang['eRC_Rubrics']['GeneralArr']["NameEn"];
					$DataDisplayOptions["ChineseName"] = $Lang['eRC_Rubrics']['GeneralArr']["NameCh"];
					$DataDisplayOptions["Gender"] = $Lang['eRC_Rubrics']['GeneralArr']["Gender"];
					
					$x .= $this->GET_NAVIGATION2($Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['StudentDisplayOption'])."&nbsp;&nbsp;";
					$x .= '<span id="spanShowOption'.$table_id.'" style="display:none"><a href="javascript:showOption(\''.$table_id.'\');" class="contenttool"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle">'.$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowOption'].'</a></span>';
					$x .= '<span id="spanHideOption'.$table_id.'"><a href="javascript:hideOption(\''.$table_id.'\');" class="contenttool"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle">'.$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['HideOption'].'</a></span>';
					$x .= '<table id="'.$table_id.'" width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">'."\n";
						# form selection
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['GeneralArr']['Form'].'</td>'."\n";
							$x .= '<td >'.$FormSelection.'</td>'."\n";
						$x .= '</tr>'."\n";
						# class selection
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['GeneralArr']['Class'].'</td>'."\n";
							$x .= '<td id="ClassSelectionTD">'."\n";
								$x .= '<span id="ClassSelectionSpan">'.$this->Get_Ajax_Loading_Image().'<!-- load on ajax --></span>'.$SelectAllClassBtn."\n";
								$x .= '<br />'."\n";
								$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
								$x .= '<div id="SelectClassWarningDiv" style="display:none;">'."\n";
									$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Class'].'</span>'."\n";
								$x .= '</div>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						# display format
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['DisplayFormat'].'</td>'."\n";
							$x .= '<td >';
								$x .= '<input type="radio" id="FormSummaryRadio" name="DisplayFormat" value="FormSummary" checked><label for="FormSummaryRadio">'.$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowFormSummary'].'</label><br>'."\n";
								$x .= '<input type="radio" id="ClassSummaryRadio" name="DisplayFormat" value="ClassSummary"><label for="ClassSummaryRadio">'.$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowClassSummary'].'</label>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						# data display
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['DataDisplay'].'</td>'."\n";
							$x .= '<td >';
								$x .= $this->Get_Checkbox('Std_All', 'Std_All', '', $isChecked=1, $Class='', $Lang['Btn']['SelectAll'], "jsSelectAllCheckbox(this.checked, 'StudentDataDisplayChk'); ", $Disabled='');
								$x .= '<br>'."\n";
								$i = 0;
								$x .= '<ul class="sortable">'."\n";
								foreach((array)$DataDisplayOptions as $val => $label)
								{
									$x .= '<li>'."\n";
										$x .= $this->Get_Checkbox('Std'.$val, 'StudentData[]', $val, $isChecked=1, 'StudentDataDisplayChk', $label, "jsCheckUnSelectAll(this.checked, 'Std_All');", $Disabled='');
										// add option "force the name to be displayed in one line" after the English and Chinese Name Selection
										if($val == 'EnglishName' || $val == 'ChineseName')
										{
											$val = 'Std'.trim($val).'NoWrap';
											$label = $Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['DisplayNameInOneLine'];
											$x .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
											$x .= $this->Get_Checkbox($val, $val, '1', $isChecked=0, '', $label, $onclick="", $Disabled='');
											
										}	
									$x .= '</li>'."\n";	
									$i++;
								}
								$x .= '</ul>'."\n";
								$x .= '<div id="SelectStudentInfoWarningDiv" style="display:none;">'."\n";
									$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['StudentInfo'].'</span>'."\n";
								$x .= '</div>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table><br><br>'."\n";		
					$ctr++;	
					### Student Display Options End

					### Report Display Options Start
					$table_id = "table_marksheet_option";
					
					$x .= $this->GET_NAVIGATION2($Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['MarksheetDisplayOption'])."&nbsp;&nbsp;";
					$x .= '<span id="spanShowOption'.$table_id.'" style="display:none"><a href="javascript:showOption(\''.$table_id.'\');" class="contenttool"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle">'.$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowOption'].'</a></span>';
					$x .= '<span id="spanHideOption'.$table_id.'"><a href="javascript:hideOption(\''.$table_id.'\');" class="contenttool"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle">'.$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['HideOption'].'</a></span>';
					$x .= '<table id="'.$table_id.'" width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">'."\n";
						# CSV / Html
						$x .= '<tr>'."\n";
							$x .= '<td  class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ViewFormat'].'</td>'."\n";
							$x .= '<td class="tabletext">';
								$x .= '<input type="radio" id="ViewFormatHTML" name="ViewFormat" value=0 checked "><label for="ViewFormatHTML">'.$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['HTML'].'</label>&nbsp;';
								$x .= '<input type="radio" id="ViewFormatCSV" name="ViewFormat" value=1 ><label for="ViewFormatCSV">'.$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['CSV'].'</label>';
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						# Subject selection
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['GeneralArr']['Subject'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $SubjectSelection.$SelectAllSubjectBtn."\n";
								$x .= '<br />'."\n";
								$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
								$x .= '<div id="SelectSubjectWarningDiv" style="display:none;">'."\n";
									$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Subject'].'</span>'."\n";
								$x .= '</div>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
//						# Subject Name Display
//						$x .= '<tr>'."\n";
//							$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SubjectName'].'</td>'."\n";
//							$x .= '<td>'."\n";
//								$thisChecked = 'checked';
//								foreach ($Lang['eRC_Rubrics']['GeneralArr']['SubjectDisplayArr'] as $thisDisplayType => $thisTypeName)
//								{
//									$x .= '<input type="radio" id="SubjectDisplay_'.$thisDisplayType.'" name="SubjectDisplay" value="'.$thisDisplayType.'" '.$thisChecked.'>';
//									$x .= '<label for="SubjectDisplay_'.$thisDisplayType.'">'.$thisTypeName.'</label>&nbsp;'."\n";
//									
//									$thisChecked = '';
//								}
//							$x .= '</td>'."\n";
//						$x .= '</tr>'."\n";	
						# Topic selection
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title" rowspan="2">'.$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['LearningDetailDisplay'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<div>'."\n"; 
									$x .= $this->Get_Radio_Button("TopicDisplay_All", 'TopicDisplay', 0, $isChecked=1, $Class="TopicDetailDisplay", $Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['DisplayAll'], $Onclick="ToggleDetailFilter()")."\n";
									$x .= $this->Get_Radio_Button("TopicDisplay_Custom", 'TopicDisplay', 1, $isChecked=0, $Class="TopicDetailDisplay", $Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['Custom'], $Onclick="ToggleDetailFilter()")."\n";
								$x .= '</div>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td id="TopicSelectionTD">'."\n";
								$x .= $this->Get_Ajax_Loading_Image()."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						# Topic Title
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowLearningTopicTitle'].'</td>'."\n";
							$x .= '<td >'.$this->Get_Radio_Button('DisplayTopicTitle_show', 'DisplayTopicTitle', 1, 1,'',$Lang['General']['Show']).$this->Get_Radio_Button('DisplayTopicTitle_hide', 'DisplayTopicTitle', 0, 0,'',$Lang['General']['Hide']).'</td>'."\n";
						$x .= '</tr>'."\n";
						# Objective Title
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['ShowLearningObjectiveTitle'].'</td>'."\n";
							$x .= '<td >'.$this->Get_Radio_Button('DisplayObjectiveTitle_show', 'DisplayObjectiveTitle', 1, 1,'',$Lang['General']['Show']).$this->Get_Radio_Button('DisplayObjectiveTitle_hide', 'DisplayObjectiveTitle', 0, 0,'',$Lang['General']['Hide']).'</td>'."\n";
						$x .= '</tr>'."\n";
						# module selection
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['MenuTitle'].'</td>'."\n";
						$x .= '<td >'."\n";
							$x .= $ModuleSelection.$SelectAllModuleBtn."\n";
							$x .= '<br />'."\n";
							$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
							$x .= '<div id="ModuleIDWarningDiv" style="display:none;">'."\n";
								$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Module'].'</span>'."\n";
							$x .= '</div>'."\n";
						$x .= '</td>'."\n";
							
						$x .= '</tr>'."\n";
						
					$x .= '</table>'."\n";		
					$ctr++;	
					### Report Display Options End

				##### Main Content Start
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# edit bottom 
			$GenBtn = $this->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "js_Check_Form();", "submitBtn");
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<br>'."\n";
					$x .= '<div class="edit_bottom_v30">'."\n";
						$x .= $GenBtn."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";;		
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		$x .= '</form>'."\n";		
				
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<br style="clear:both;" />'."\n";
		
		return $x;
	}
	
	function Get_Reports_Grand_Marksheet_Detail_Option($SubjectIDArr=array())
	{
		global $PATH_WRT_ROOT, $Lang, $lreportcard;
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
		$lreportcard_topic = new  libreportcardrubrics_topic();
		
		$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectIDArr);
		$TopicTree = $lreportcard_topic->Get_Topic_Tree($SubjectIDArr);
		
		
		$TopicInfoAssoc = BuildMultiKeyAssoc($TopicInfoArr, array("Level","TopicID"));
		$TopicTreeInfoAssoc = BuildMultiKeyAssoc(array_values((array)$TopicInfoAssoc[3]), array("SubjectID","ParentTopicID","TopicID"));
		$TopicTitleLang = Get_Lang_Selection("TopicNameCh","TopicNameEn");
		
		$SubjectInfo = $lreportcard->Get_Subject_Info();
		$SubjectAssoc = BuildMultiKeyAssoc($SubjectInfo,"SubjectID");
		$SubjectTitleLang = Get_Lang_Selection("CH_DES","EN_DES");
		
		$table .= '<table id="SelectLearningDetail">'."\n";
		foreach((array)$TopicTree as $SubjectID => $SubjectTopicInfoArr)
		{
			if(count($SubjectTopicInfoArr)==0) continue;
			
			$table .= '<tr><td>'."\n";	
			$table .= '<div>'.$SubjectAssoc[$SubjectID][$SubjectTitleLang].'</div>'."\n";
			$table .= '<div style="padding-left:30px;">'."\n";
			foreach((array)$SubjectTopicInfoArr as $TopicID => $ObjectiveInfoArr)	
			{
				if(count($ObjectiveInfoArr)==0) continue;
				
				$table .= '<div>'.$TopicInfoAssoc[1][$TopicID][$TopicTitleLang].'</div>'."\n";
				$table .= '<div style="padding-left:30px;">'."\n";
				foreach((array)$ObjectiveInfoArr as $ObjectiveID => $DetailInfoArr)
				{
					if(count($DetailInfoArr)==0) continue;
					$OptionClassName = 'Detail_'.$ObjectiveID;
					$CheckAllClassName = 'Check_All_Detail_'.$ObjectiveID;
					
					$table .= '<div>'.$TopicInfoAssoc[2][$ObjectiveID][$TopicTitleLang].'</div>'."\n";
					$table .= '<div style="padding-left:30px;">'."\n";
						$table .= '<div style="padding-left:5px">'."\n";
							$table .= $this->Get_Checkbox($CheckAllClassName, '', '', 1, '', $Lang['eRC_Rubrics']['GeneralArr']['SelectAll']," CheckAll(this.checked,'$OptionClassName') ")."\n";
						$table .= '</div>'."\n";
						foreach((array)$DetailInfoArr as $thisDetailID => $DetailInfo)
						{
							$table .= '<div style="float:left; padding-left:5px">'."\n";
								$table .= $this->Get_Checkbox('TopicID_'.$thisDetailID, 'TopicIDArr[]', $thisDetailID, 1, $OptionClassName." TopicDetailOption", $TopicInfoAssoc[3][$thisDetailID][$TopicTitleLang], "UnCheck(this.checked,'$CheckAllClassName')")."\n";
							$table .= '</div>'."\n";
						}
						$table .= '<br style="clear:both;" />'."\n";
					$table .= '</div>'."\n";
				}
				$table .= '</div>'."\n";
			}
			$table .= '</div>'."\n";
			$table .= '</td></tr>'."\n";
		}
		$table .= '</table>'."\n";
		$table .= '<div id="SelectLearningDetailDiv" style="display:none;">'."\n";
			$table .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['LearningDetails'].'</span>'."\n";
		$table .= '</div>'."\n";
		
		return $table;
		
	}

#######################################################################################################
# Statistics Start
#
	function Get_Statistics_General_Setting_UI()
	{
		global $Lang;
		
		$RubricsReportSettingTable = $this->Get_Statistics_General_Setting_Table(); 
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post" action="print.php" target="_blank">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'.$RubricsReportSettingTable.'</td>';		
				$x .= '</tr>'."\n";

				# edit bottom 
				$GenBtn = $this->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "js_Check_Form();", "submitBtn");
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<br>'."\n";
						$x .= '<div class="edit_bottom_v30">'."\n";
							$x .= $GenBtn."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";;		
				$x .= '</tr>'."\n";

			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";	
		
		return $x; 
	}

	function Get_Statistics_General_Setting_Table()
	{
		global $Lang, $PATH_WRT_ROOT, $sys_custom;

		//include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		//include_once($PATH_WRT_ROOT.'includes/form_class_manage_ui.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
		include_once($PATH_WRT_ROOT.'includes/libreportcardrubrics_module.php');
		
		//$fcm_ui = new form_class_manage_ui();
		$scm_ui = new subject_class_mapping_ui();
		$lreportcard_module = new libreportcardrubrics_module();
		
		
		# Form Stage Selection
		$thisOnChange = 'js_Changed_Form_Stage_Selection();';
		$FormStageSelection = $this->Get_Form_Stage_Selection('FormStageIDArr[]', '', $thisOnChange, $noFirst=1, $isAll=0, $firstTitle='', $CheckClassTeacher=1, $isMultiple=1);
		
		# Select All Btn
		$SelectAllFormStageBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('FormStageIDArr[]', 1, 'js_Changed_Form_Stage_Selection();');");
		$SelectAllFormBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('YearIDArr[]', 1, 'js_Changed_Form_Selection();');");
		$SelectAllClassBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('YearClassIDArr[]', 1, 'js_Reload_Class_Selection();')");
		$SelectAllStudentBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentIDArr[]')");
		$SelectAllModuleBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('ModuleModuleID')");
		
		# Form Selection
		//$thisOnChange = 'js_Changed_Form_Selection(this.value);';
		//$FormSelection = $fcm_ui->Get_Form_Selection('YearID', $SelectedYearID='', $thisOnChange, $noFirst=1, $isAll=0);
		//$FormSelection = $this->Get_Form_Selection('YearID', $SelectedYearID='', $thisOnChange, $noFirst=1, $isAll=0, $firstTitle='', $CheckClassTeacher=1);
		
		
		# Student Subject Selection
		$StudentSubjectSelection = $scm_ui->Get_Subject_Selection("StudentSubjectIDArr[]", '', $OnChange='js_Changed_Student_Subject_Selection();', $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=1);
		# Select All Term Btn
		$SelectAllStudentYearTermBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentYearTermIDArr[]', 1, 'js_Changed_Student_Year_Term_Selection();')");
		# Select All Subject Btn
		$SelectAllStudentSubjectBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_With_OptGroup('StudentSubjectIDArr[]', 1, 'js_Changed_Student_Subject_Selection();')");
		# Select All Subject Group Btn
		$SelectAllStudentSubjectGroupBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentSubjectGroupIDArr[]', 1, 'js_Changed_Student_Subject_Group_Selection();')");

		
		# Extra Info Selection
		if($sys_custom['UserExtraInformation'])
		{
			$ExtraInfoSelection = $this->Get_Extra_Info_Selection("ExtraItemID[]",'',$OnChange="js_Changed_Student_Extra_Info_Selection()");
			$SelectAllExtraInfoBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('ExtraItemID[]', 1, 'js_Changed_Student_Extra_Info_Selection();')");
		}
		
		
		# Subject Selection
		$SubjectSelection = $scm_ui->Get_Subject_Selection("SubjectIDArr[]", '', $OnChange='', $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=1);
		$SelectAllSubjectBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_With_OptGroup('SubjectIDArr[]')");
		
		# build module selection $ checkboxes
		$ModuleInfoArr = $lreportcard_module->Get_Module_Info();
		foreach($ModuleInfoArr as $thisModuleInfo)
		{
			list($ModuleID, $ModuleCode, $ModuleNameEn ,$ModuleNameCh) = $thisModuleInfo;
			$ModuleName = Get_Lang_Selection($ModuleNameCh,$ModuleNameEn)."($ModuleCode)";
			
			$ModuleSelectOptions[] = array($ModuleID,$ModuleName);
		}
		$ModuleSelection = getSelectByArray($ModuleSelectOptions," id='ModuleModuleID' name='ModuleID[]' class='ModuleMember' multiple size=10 ",0,0,1);
		
		$TableName = 'StudentSettings';
//		$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['Identity']['Student']);
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<table id="OptionTable_'.$TableName.'" class="form_table_v30">'."\n";
			# Select Students by Class / Subject Group
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SelectFrom'].'</td>'."\n";
				$x .= '<td>'."\n";
					// Class
					$thisDisplay = $Lang['SysMgr']['FormClassMapping']['Class'];
					$thisOnClick = "js_Changed_Student_Selection_Source('Class');";
					$x .= $this->Get_Radio_Button('SelectStudentFrom_Class', 'SelectStudentFrom', 'Class', $thisChecked=1, $Class="", $thisDisplay, $thisOnClick);
					// Subject Group
					$thisDisplay = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
					$thisOnClick = "js_Changed_Student_Selection_Source('SubjectGroup');";
					$x .= $this->Get_Radio_Button('SelectStudentFrom_SubjectGroup', 'SelectStudentFrom', 'SubjectGroup', $thisChecked=0, $Class="", $thisDisplay, $thisOnClick);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			# Form Stage Selection
			$x .= '<tr class="Student_FormClassTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['FormStage'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('FormStageSelectionSpan', $FormStageSelection, $SelectAllFormStageBtn);
					$x .= $this->Get_Warning_Message_Div('SelectFormStageWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['FormStage']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";	
			# Form Selection
			$x .= '<tr class="Student_FormClassTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['Form'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('FormSelectionSpan', $SelectionHTML='', $SelectAllFormBtn);
					$x .= $this->Get_Warning_Message_Div('SelectFormWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Form']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			# Class Selection
			$x .= '<tr class="Student_FormClassTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('ClassSelectionSpan', $SelectionHTML='', $SelectAllClassBtn);
					$x .= $this->Get_Warning_Message_Div('SelectClassWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Class']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# Term Selection
			$x .= '<tr class="Student_SubjectGroupTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['General']['Term'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentYearTermSelectionSpan', $SelectionHTML='', $SelectAllStudentYearTermBtn);
					$x .= $this->Get_Warning_Message_Div('SelectStudentYearTermWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Term']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			# Subject Selection
			$x .= '<tr class="Student_SubjectGroupTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentSubjectSelectionSpan', $StudentSubjectSelection, $SelectAllStudentSubjectBtn);
					$x .= $this->Get_Warning_Message_Div('SelectStudentSubjectWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Subject']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";								
			# Subject Group Selection
			$x .= '<tr class="Student_SubjectGroupTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentSubjectGroupSelectionSpan', $SelectionHTML='', $SelectAllStudentSubjectGroupBtn);
					$x .= $this->Get_Warning_Message_Div('SelectStudentSubjectGroupWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['SubjectGroup']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
								
			if($sys_custom['UserExtraInformation'])
			{
				# Extra INfo Selection
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['AccountMgmt']['UserExtraInfo'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div>'."\n";
							$x .= '<span id="ExtraInfoSpan">'.$ExtraInfoSelection.'</span>'.$SelectAllExtraInfoBtn."\n";
							$x .= '<br />'."\n";
							$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			}
			# Student Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['Identity']['Student'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= '<span id="StudentSelectionSpan"></span>'.$SelectAllStudentBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="SelectStudentWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Student'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			#Subject Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= '<span>'.$SubjectSelection.'</span>'.$SelectAllSubjectBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="SelectSubjectWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Subject'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			# Subject Name Display
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SubjectName'].'</td>'."\n";
				$x .= '<td>'."\n";
					$thisChecked = 'checked';
					foreach ($Lang['eRC_Rubrics']['GeneralArr']['SubjectDisplayArr'] as $thisDisplayType => $thisTypeName)
					{
						$x .= '<input type="radio" id="SubjectDisplay_'.$thisDisplayType.'" name="SubjectDisplay" value="'.$thisDisplayType.'" '.$thisChecked.'>';
						$x .= '<label for="SubjectDisplay_'.$thisDisplayType.'">'.$thisTypeName.'</label>&nbsp;'."\n";
						
						$thisChecked = '';
					}
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";			
			# Module Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['MenuTitle'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= $ModuleSelection.$SelectAllModuleBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="ModuleIDWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Module'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";

//				$x .= '<td>'.$ModuleSelection.'</td>'."\n";
			$x .= '</tr>'."\n";
														
		$x .= '</table>'."\n";
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<br style="clear:both;" />'."\n";
		
		return $x;
	}
	
	function Get_Statistics_RubricsReport_Flash_Chart($SubjectMarkDisplayArr, $SubjectDisplay, $YearID, $SubjectCode='')
	{
		global $PATH_WRT_ROOT, $Lang, $lreportcard;
		include_once($PATH_WRT_ROOT."includes/flashchart_basic_201301_in_use/php-ofc-library/open-flash-chart.php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
		
		$colourArr = array('#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414');
		
		# Get Academic Year Name
		$YearName = getCurrentAcademicYear();
		
		if(empty($SubjectCode))
		{
			# Get Subject Info
			$SubjectInfo = $lreportcard->Get_Subject_Info();
			switch($SubjectDisplay)
			{
				case "Abbr": $subfix = "ABBR"; break;
				case "Desc": $subfix = "DES"; break;
				case "ShortForm": $subfix = "SNAME"; break;
			}	
			$SubjDisplayLang = Get_Lang_Selection("CH_".$subfix,"EN_".$subfix);
		}
		
		# Get Rubrics Info
		$lreportcard_rubrics = new  libreportcardrubrics_rubrics();
		$RubricsInfo = $lreportcard_rubrics->Get_Rubics_Item_Info();
		$RubricsInfo = BuildMultiKeyAssoc($RubricsInfo, "RubricsItemID");
		$RubricsItemIDArr = array_keys($RubricsInfo);
		
		
		foreach((array)$SubjectMarkDisplayArr as $StudentID => $SubjectRubricsArr)
		{
			$chart = new open_flash_chart();
			
			$i = 0;
			$involvedSubjectArr = array();
			
			$involvedRubricsItems = empty($SubjectRubricsArr["InvolvedRubricsItems"])?array(0):array_values(array_intersect($RubricsItemIDArr , $SubjectRubricsArr["InvolvedRubricsItems"]));
			$involvedSubjectArr  = $SubjectRubricsArr["InvolvedSubject"];
			
			for($j=0; $j<count($involvedRubricsItems); $j++)
			{
				$RubricsItemID = $involvedRubricsItems[$j];
				$RubricsPercent = $SubjectRubricsArr["RubricsPercent"][$RubricsItemID];
				
				$valueArr = array();
				$x_axis_title = array();
				$ItemDisplayLang = Get_Lang_Selection("DescriptionCh","DescriptionEn");
				$RubricsName = " ";
				if(!$RubricsInfo[$RubricsItemID])
					$RubricsName .= $Lang['eRC_Rubrics']['GeneralArr']['NotAssessed'];
				else
					$RubricsName .= $RubricsInfo[$RubricsItemID][$ItemDisplayLang];
				
				foreach($involvedSubjectArr as $SubjectID)
				{
					$valueArr[]=$RubricsPercent[$SubjectID]?$RubricsPercent[$SubjectID]:0;
					
					if(empty($SubjectCode))
						$x_axis_title[] = $SubjectInfo[$SubjectID][$SubjDisplayLang];
					else
						$x_axis_title[] = $SubjectCode[$SubjectID];
				}
								
				# bar 
				
				$thisColor = $colourArr[$i];
				while(!$thisColor)
				{
					$rand = rand(0,pow(16,6));
					$tmpColor = "#".sprintf("%X",$rand);
					if(!in_array($tmpColor,$colourArr))
						$thisColor = $tmpColor;
				}
				
				$bar = new bar_stack_group();
				$bar->set_values( $valueArr );
				$bar->set_colour( $thisColor );
				$bar->set_tooltip( $RubricsName.':#val#%' );
				$bar->set_key( $RubricsName, '12' );
				$bar->set_id( $i );
				$bar->set_visible( true );
				$chart->add_element( $bar );
				$i++;
			}

			# title
			# Get Student INfo 
			$StudentInfo = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($YearID,'',$StudentID);
			$StudentName = $StudentInfo[0]["StudentName"];
			$FormName = $StudentInfo[0]["YearName"];
			$title = new title( $YearName.$Lang['eRC_Rubrics']['GeneralArr']['SchoolYear2Ch']."\n".$FormName." ".$StudentName.$Lang['eRC_Rubrics']['StatisticArr']['RubricsReportArr']['Performance'] );
			$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

			# y axis
			$y_legend = new y_legend( $Lang['General']['Percentage']);
			$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
			
			$y = new y_axis();
			$y->set_stroke( 2 );
			$y->set_tick_length( 2 );
			$y->set_colour( '#999999' );
			$y->set_grid_colour( '#CCCCCC' );
			$y->set_range( 0, 100, 20);
			$y->set_offset(true);

			# x axis
			$x_legend = new x_legend( $Lang['eRC_Rubrics']['GeneralArr']['Subject'] );
			$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
			
			$x = new x_axis();
			$x->set_stroke( 2 );
			$x->set_tick_height( 2 );
			$x->set_colour( '#999999' );
			$x->set_grid_colour( '#CCCCCC' );
			$x->set_labels_from_array( $x_axis_title );
//			$xLab = new x_axis_labels();
//			$xLab->set_labels($x_axis_title);
//			$xLab->set_vertical();
//			$x->set_labels( $xLab );
			
			# tooltip
			$tooltip = new tooltip();
			$tooltip->set_hover();

			$chart->set_bg_colour( '#FFFFFF' );
			$chart->set_title( $title );
			$chart->set_y_legend( $y_legend );
			$chart->set_y_axis( $y );
			$chart->set_x_legend( $x_legend );
			$chart->set_x_axis( $x );
			$chart->set_tooltip($tooltip);
		
			$ChartArr[$StudentID] = $chart;
			
		}
		
		return $ChartArr;
	}
	
	function Get_Statistics_RubricsReport_Statistic_Table($SubjectMarkDisplayArr, $SubjectDisplay, $SubjectCode='')
	{
		global $lreportcard, $PATH_WRT_ROOT, $Lang;
		
		# Get Subject Info
		$SubjectInfo = $lreportcard->Get_Subject_Info();
		switch($SubjectDisplay)
		{
			case "Abbr": $subfix = "ABBR"; break;
			case "Desc": $subfix = "DES"; break;
			case "ShortForm": $subfix = "SNAME"; break;
		}	
		$SubjDisplayLang = Get_Lang_Selection("CH_".$subfix,"EN_".$subfix);

		#Get Rubrics Info
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
		$lreportcard_rubrics = new  libreportcardrubrics_rubrics();
		$RubricsInfo = $lreportcard_rubrics->Get_Rubics_Item_Info();
		$RubricsInfo = BuildMultiKeyAssoc($RubricsInfo, "RubricsItemID");
		$RubricsItemIDArr = array_keys($RubricsInfo);
		$ItemDisplayLang = Get_Lang_Selection("DescriptionCh","DescriptionEn");
		
		foreach((array)$SubjectMarkDisplayArr as $StudentID => $SubjectRubricsArr)
		{
			$OverallTotal = 0;
			$SubjectTotal = array();
//			$involvedRubricsItems = $SubjectRubricsArr["InvolvedRubricsItems"];
			$involvedRubricsItems = empty($SubjectRubricsArr["InvolvedRubricsItems"])?array(0):array_values(array_intersect($RubricsItemIDArr , $SubjectRubricsArr["InvolvedRubricsItems"]));
			$involvedSubjectArr  = $SubjectRubricsArr["InvolvedSubject"];
//			debug_pr($involvedRubricsItems);
			$table = '';
			$table .= '<table class="report_table" border=1 width="800">';
				$table .= '<thead>';
					$table .= '<tr>';
						$table .= '<th>&nbsp;</th>';
						foreach($involvedSubjectArr as $SubjectID)
						{
							if(!empty($SubjectCode))
								$thisSubjectCode = "[".$SubjectCode[$SubjectID]."]<br>";
							$table .= '<th style="text-align:center" valign="top" >'.$thisSubjectCode.$SubjectInfo[$SubjectID][$SubjDisplayLang].'</th>';
						}
						$table .= '<th style="text-align:right">'.$Lang['General']['Total'] .'</th>';
					$table .= '</tr>';
				$table .= '</thead>';
			
				$table .= '<tbody>';
				$SizeOfItem =sizeof($involvedRubricsItems); 
				for($i=0; $i<$SizeOfItem; $i++)
				{
//				foreach((array)$SubjectRubricsArr["RubricsCount"] as $RubricsItemID => $RubricsCount)
//				{	
					$RubricsItemID = $involvedRubricsItems[$i];
					$RubricsCount = $SubjectRubricsArr["RubricsCount"][$RubricsItemID];
					
					if(!$RubricsInfo[$RubricsItemID])
						$RubricsName = $Lang['eRC_Rubrics']['GeneralArr']['NotAssessed'];
					else
						$RubricsName = $RubricsInfo[$RubricsItemID][$ItemDisplayLang];
					$table .= '<tr>';
						$table .= '<td>'.$RubricsName.'</td>';
						$RubricsTotal = 0;
						foreach((array)$involvedSubjectArr as $SubjectID)
						{
							$thisCount = $RubricsCount[$SubjectID]?$RubricsCount[$SubjectID]:0;
							$table .= '<td  style="text-align:right">'.$thisCount.'</td>';
							$RubricsTotal += $thisCount;
							$SubjectTotal[$SubjectID] += $thisCount;
						}
						$table .= '<td  style="text-align:right">'.$RubricsTotal.'</td>';
					$table .= '</tr>';
				}
				$table .= '</tbody>';
				$table .= '<tfoot>';
					$table .= '<tr>';
						$table .= '<td>'.$Lang['General']['Total'] .'</td>';
						foreach((array)$involvedSubjectArr as $SubjectID)
						{
							$table .= '<td style="text-align:right">'.($SubjectTotal[$SubjectID]?$SubjectTotal[$SubjectID]:0).'</td>';
							$OverallTotal += $SubjectTotal[$SubjectID];
						}
						$table .= '<td style="text-align:right">'.$OverallTotal.'</td>';	
				$table .= '</tfoot>';
			$table .= '</table>';
			
			$TableArr[$StudentID] = $table;
		}

		return $TableArr;
	}

	function Get_Statistics_RadarDiagram_Setting_UI()
	{
		global $Lang;
		
		$RubricsReportSettingTable = $this->Get_RadarDiagram_Setting_Table(); 
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post" action="print.php" target="_blank">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'.$RubricsReportSettingTable.'</td>';		
				$x .= '</tr>'."\n";

				# edit bottom 
				$GenBtn = $this->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "js_Check_Form();", "submitBtn");
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<br>'."\n";
						$x .= '<div class="edit_bottom_v30">'."\n";
							$x .= $GenBtn."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";;		
				$x .= '</tr>'."\n";

			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";	
		
		return $x; 
	}

	function Get_Statistics_Bar_Chart_Subject_Key($SubjectMarkDisplayArr, $SubjectDisplay, $SubjectCode='')
	{
		global $lreportcard, $PATH_WRT_ROOT, $Lang;
		
		# Get Subject Info
		$SubjectInfo = $lreportcard->Get_Subject_Info();
		switch($SubjectDisplay)
		{
			case "Abbr": $subfix = "ABBR"; break;
			case "Desc": $subfix = "DES"; break;
			case "ShortForm": $subfix = "SNAME"; break;
		}	
		$SubjDisplayLang = Get_Lang_Selection("CH_".$subfix,"EN_".$subfix);
		
		foreach((array)$SubjectMarkDisplayArr as $StudentID => $SubjectRubricsArr)
		{
			$involvedSubjectArr  = $SubjectRubricsArr["InvolvedSubject"];
			$td_width = 100/count($involvedSubjectArr);
			
			$table = '<table border=0 width="800">';
				$table .= '<thead>';
					$table .= '<tr>';

						foreach($involvedSubjectArr as $SubjectID)
						{
							if(!empty($SubjectCode))
								$thisSubjectCode = "[".$SubjectCode[$StudentID][$SubjectID]."]<br>";
							$table .= '<th style="text-align:center" valign="top"  width="'.$td_width.'%">'.$thisSubjectCode.$SubjectInfo[$SubjectID][$SubjDisplayLang].'</th>';
						}
					$table .= '</tr>';
				$table .= '</thead>';
			$table .= '</table>';
			
			$ReturnTable[$StudentID] = $table;
		}
		
		return $ReturnTable;
	}

	function Get_Statistics_Bar_Chart_Topic_Key($SubjectID, $TopicMarkDisplayArr, $SubjectDisplay, $TopicCode='')
	{
		global $lreportcard, $PATH_WRT_ROOT, $Lang, $lreportcard_topic;
		
		if(!isset($lreportcard_topic))
		{
			include_once("libreportcardrubrics_topic.php");
			$lreportcard_topic = new  libreportcardrubrics_topic();
		}
		
		# Get_Topic_Info
		$TopicInfo = $lreportcard_topic->Get_Topic_Info($SubjectID);
		$TopicInfo = BuildMultiKeyAssoc($TopicInfo, "TopicID");
		$TopicDisplayLang = Get_Lang_Selection("TopicNameCh","TopicNameEn");
				
		foreach((array)$TopicMarkDisplayArr as $ClassName => $TopicRubricsArr)
		{
			$involvedTopicArr  = $TopicRubricsArr["InvolvedTopic"];
			$td_width = 100/count($involvedTopicArr);
			$table = '<table border=0 width="800">';
				$table .= '<thead>';
					$table .= '<tr>';

						foreach($involvedTopicArr as $TopicID)
						{
							if(!empty($TopicCode))
								$thisTopicCode = "[".$TopicCode[$ClassName][$TopicID]."]<br>";
							$table .= '<th style="text-align:center" valign="top" width="'.$td_width.'%">'.$thisTopicCode.$TopicInfo[$TopicID][$TopicDisplayLang].'</th>';
						}
					$table .= '</tr>';
				$table .= '</thead>';
			$table .= '</table>';
			
			$ReturnTable[$ClassName] = $table;
		}
		
		return $ReturnTable;
	}
	
	function Get_RadarDiagram_Setting_Table()
	{
		global $Lang, $PATH_WRT_ROOT,$sys_custom;

		//include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		//include_once($PATH_WRT_ROOT.'includes/form_class_manage_ui.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
		include_once($PATH_WRT_ROOT.'includes/libreportcardrubrics_module.php');
		
		//$fcm_ui = new form_class_manage_ui();
		$scm_ui = new subject_class_mapping_ui();
		$lreportcard_module = new libreportcardrubrics_module();
		
		### Student Display Options
		# Select All Btn
		$SelectAllFormStageBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('FormStageIDArr[]', 1, 'js_Changed_Form_Stage_Selection();');");
		$SelectAllFormBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('YearIDArr[]', 1, 'js_Changed_Form_Selection();');");
		$SelectAllClassBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('YearClassIDArr[]', 1, 'js_Reload_Class_Selection();')");
		$SelectAllStudentBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentIDArr[]')");
		$SelectAllModuleBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('ModuleModuleID')");
		
		
		# Form Stage Selection
		$thisOnChange = 'js_Changed_Form_Stage_Selection();';
		$FormStageSelection = $this->Get_Form_Stage_Selection('FormStageIDArr[]', '', $thisOnChange, $noFirst=1, $isAll=0, $firstTitle='', $CheckClassTeacher=1, $isMultiple=1);
		
		# Form Selection
		//$thisOnChange = 'js_Changed_Form_Selection(this.value);';
		//$FormSelection = $fcm_ui->Get_Form_Selection('YearID', $SelectedYearID='', $thisOnChange, $noFirst=1, $isAll=0);
		//$FormSelection = $this->Get_Form_Selection('YearID', $SelectedYearID='', $thisOnChange, $noFirst=1, $isAll=0, $firstTitle='', $CheckClassTeacher=1);
		
		# Student Subject Selection
		$StudentSubjectSelection = $scm_ui->Get_Subject_Selection("StudentSubjectIDArr[]", '', $OnChange='js_Changed_Student_Subject_Selection();', $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=1);
		# Select All Term Btn
		$SelectAllStudentYearTermBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentYearTermIDArr[]', 1, 'js_Changed_Student_Year_Term_Selection();')");
		# Select All Subject Btn
		$SelectAllStudentSubjectBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_With_OptGroup('StudentSubjectIDArr[]', 1, 'js_Changed_Student_Subject_Selection();')");
		# Select All Subject Group Btn
		$SelectAllStudentSubjectGroupBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentSubjectGroupIDArr[]', 1, 'js_Changed_Student_Subject_Group_Selection();')");
		
		
		#Extra Info Selection
		if($sys_custom['UserExtraInformation'])
		{
			$ExtraInfoSelection = $this->Get_Extra_Info_Selection("ExtraItemID[]",'',$OnChange="js_Changed_Student_Extra_Info_Selection()");
			$SelectAllExtraInfoBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('ExtraItemID[]', 1, 'js_Changed_Student_Extra_Info_Selection();')");
		}
		
	
		# Subject Selection
		$SubjectSelection = $scm_ui->Get_Subject_Selection("SubjectIDArr[]", '', $OnChange='', $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=1);
		# Select All Subject Btn
		$SelectAllSubjectBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_With_OptGroup('SubjectIDArr[]')");
		
		
		# build module selection $ checkboxes
		$ModuleInfoArr = $lreportcard_module->Get_Module_Info();
		foreach($ModuleInfoArr as $thisModuleInfo)
		{
			list($ModuleID, $ModuleCode, $ModuleNameEn ,$ModuleNameCh) = $thisModuleInfo;
			$ModuleName = Get_Lang_Selection($ModuleNameCh,$ModuleNameEn)."($ModuleCode)";
			
			$ModuleSelectOptions[] = array($ModuleID,$ModuleName);
		}
		$ModuleSelection = getSelectByArray($ModuleSelectOptions," id='ModuleModuleID' name='ModuleID[]' class='ModuleMember' multiple size=10 ",0,0,1);
		
		$TableName = 'StudentSettings';
//		$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['Identity']['Student']);
		$x .= '<br style="clear:both;" />'."\n";
		$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['SectionTitleArr'][$TableName]);
		$x .= '<table id="OptionTable_'.$TableName.'" class="form_table_v30">'."\n";
			$x .= '<tbody>'."\n";
				# Select Students by Class / Subject Group
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SelectFrom'].'</td>'."\n";
					$x .= '<td>'."\n";
						// Class
						$thisDisplay = $Lang['SysMgr']['FormClassMapping']['Class'];
						$thisOnClick = "js_Changed_Student_Selection_Source('Class');";
						$x .= $this->Get_Radio_Button('SelectStudentFrom_Class', 'SelectStudentFrom', 'Class', $thisChecked=1, $Class="", $thisDisplay, $thisOnClick);
						// Subject Group
						$thisDisplay = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
						$thisOnClick = "js_Changed_Student_Selection_Source('SubjectGroup');";
						$x .= $this->Get_Radio_Button('SelectStudentFrom_SubjectGroup', 'SelectStudentFrom', 'SubjectGroup', $thisChecked=0, $Class="", $thisDisplay, $thisOnClick);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				# Form Stage Selection
				$x .= '<tr class="Student_FormClassTr">'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['FormStage'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_MultipleSelection_And_SelectAll_Div('FormStageSelectionSpan', $FormStageSelection, $SelectAllFormStageBtn);
						$x .= $this->Get_Warning_Message_Div('SelectFormStageWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['FormStage']);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";	
				# Form Selection
				$x .= '<tr class="Student_FormClassTr">'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['Form'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_MultipleSelection_And_SelectAll_Div('FormSelectionSpan', $SelectionHTML='', $SelectAllFormBtn);
						$x .= $this->Get_Warning_Message_Div('SelectFormWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Form']);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				# Class Selection
				$x .= '<tr class="Student_FormClassTr">'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_MultipleSelection_And_SelectAll_Div('ClassSelectionSpan', $SelectionHTML='', $SelectAllClassBtn);
						$x .= $this->Get_Warning_Message_Div('SelectClassWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Class']);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Term Selection
				$x .= '<tr class="Student_SubjectGroupTr">'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['General']['Term'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentYearTermSelectionSpan', $SelectionHTML='', $SelectAllStudentYearTermBtn);
						$x .= $this->Get_Warning_Message_Div('SelectStudentYearTermWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Term']);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				# Subject Selection
				$x .= '<tr class="Student_SubjectGroupTr">'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentSubjectSelectionSpan', $StudentSubjectSelection, $SelectAllStudentSubjectBtn);
						$x .= $this->Get_Warning_Message_Div('SelectStudentSubjectWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Subject']);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";								
				# Subject Group Selection
				$x .= '<tr class="Student_SubjectGroupTr">'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentSubjectGroupSelectionSpan', $SelectionHTML='', $SelectAllStudentSubjectGroupBtn);
						$x .= $this->Get_Warning_Message_Div('SelectStudentSubjectGroupWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['SubjectGroup']);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			
				# extra information 
				if($sys_custom['UserExtraInformation'])
				{
					# Extra INfo Selection
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['AccountMgmt']['UserExtraInfo'].'</td>'."\n";
						$x .= '<td>'."\n";
							$x .= '<div>'."\n";
								$x .= '<span id="ExtraInfoSpan">'.$ExtraInfoSelection.'</span>'.$SelectAllExtraInfoBtn."\n";
								$x .= '<br />'."\n";
								$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
							$x .= '</div>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				}				
				# Student Selection
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['Identity']['Student'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div>'."\n";
							$x .= '<span id="StudentSelectionSpan"></span>'.$SelectAllStudentBtn."\n";
							$x .= '<br />'."\n";
							$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
						$x .= '</div>'."\n";
						$x .= '<div id="SelectStudentWarningDiv" style="display:none;">'."\n";
							$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Student'].'</span>'."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</body>'."\n";
		$x .= '</table>'."\n";				
				
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<br style="clear:both;" />'."\n";		
		$TableName = 'SubjectSettings';
		$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['SectionTitleArr'][$TableName]);
		$x .= '<table id="OptionTable_'.$TableName.'" class="form_table_v30">'."\n";
			$x .= '<tbody>'."\n";
				#Subject Selection
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div>'."\n";
							$x .= '<span>'.$SubjectSelection.'</span>'.$SelectAllSubjectBtn."\n";
							$x .= '<br />'."\n";
							$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
						$x .= '</div>'."\n";
						$x .= '<div id="Min3SubjectWarningDiv" style="display:none;">'."\n";
							$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['3Subject'].'</span>'."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				# Subject Name Display
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SubjectName'].'</td>'."\n";
					$x .= '<td>'."\n";
						$thisChecked = 'checked';
						foreach ($Lang['eRC_Rubrics']['GeneralArr']['SubjectDisplayArr'] as $thisDisplayType => $thisTypeName)
						{
							$x .= '<input type="radio" id="SubjectDisplay_'.$thisDisplayType.'" name="SubjectDisplay" value="'.$thisDisplayType.'" '.$thisChecked.'>';
							$x .= '<label for="SubjectDisplay_'.$thisDisplayType.'">'.$thisTypeName.'</label>&nbsp;'."\n";
							
							$thisChecked = '';
						}
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";	
			$x .= '</body>'."\n";
		$x .= '</table>'."\n";
					
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<br style="clear:both;" />'."\n";		
		$TableName = 'ModuleSettings';
		$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['SectionTitleArr'][$TableName]);
		$x .= '<table id="OptionTable_'.$TableName.'" class="form_table_v30">'."\n";
			$x .= '<tbody>'."\n";		
				# Module Selection
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['MenuTitle'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div>'."\n";
							$x .= $ModuleSelection.$SelectAllModuleBtn."\n";
							$x .= '<br />'."\n";
							$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
						$x .= '</div>'."\n";
						$x .= '<div id="ModuleIDWarningDiv" style="display:none;">'."\n";
							$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Module'].'</span>'."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</body>'."\n";
		$x .= '</table>'."\n";
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<br style="clear:both;" />'."\n";
				
		$TableName = 'ReportSettings';
		$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['SectionTitleArr'][$TableName]);
		$x .= '<table id="OptionTable_'.$TableName.'" class="form_table_v30">'."\n";
			$x .= '<tbody>'."\n";
	
				# School Name
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'."\n";
						$x .= $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SchoolName']."\n";
					$x .= '</td>'."\n";
					$x .= '<td>'."\n";
						// Show
						$thisChecked = (!isset($SettingsArr['SchoolNameDisplay']) || $SettingsArr['SchoolNameDisplay'] == 1)? 'checked' : '';
						$x .= '<input type="radio" id="SchoolNameDisplay_Display" name="SchoolNameDisplay" value="1" '.$thisChecked.'>';
						$x .= '<label for="SchoolNameDisplay_Display">'.$Lang['General']['Show'].'</label>&nbsp;'."\n";
						// Hide
						$thisChecked = (isset($SettingsArr['SchoolNameDisplay']) && $SettingsArr['SchoolNameDisplay'] == 0)? 'checked' : '';
						$x .= '<input type="radio" id="SchoolNameDisplay_Hidden" name="SchoolNameDisplay" value="0" '.$thisChecked.'>';
						$x .= '<label for="SchoolNameDisplay_Hidden">'.$Lang['General']['Hide'].'</label>&nbsp;'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Report Title
				$x .= '<tr>'."\n";
					$x .= '<td id="ReportTitleTd" class="field_title" rowspan="3">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['Title'].'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<span class="sub_row_content_v30">('.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['Row'].' 1)</span>'."\n";
						$x .= '<span class="row_content_v30">'."\n";
					       	$x .= '<input id="ReportTitle_1" name="ReportTitleArr[]" type="text" class="textbox_name" />'."\n";
					    $x .= '</span>'."\n";
				    $x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr id="AddMoreRow_Report">'."\n";
				    $x .= '<td>'."\n";
				    	$x .= '<div><a href="javascript:js_Add_More_Title_Row(\'Report\');" class="tablelink">['.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['AddMore'].']</a></div>'."\n";
				    $x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Header Data
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['HeaderData'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Template_Header_Data_Info_Checkbox_Table($SettingsArr['HeaderDataFieldOrderedList'], $SettingsArr['HeaderDataFieldArr'], $SettingsID);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Header Data Per Row
				$thisValue = ($SettingsArr['NumOfHeaderDataPerRow']=='')? 2 : $SettingsArr['NumOfHeaderDataPerRow'];
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['NumOfHeaderDataEachRow'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_TEXTBOX_NUMBER('', 'NumOfHeaderDataPerRow', $thisValue);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";														

				# Apply page break for each student
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['StatisticArr']['RadarDiagramArr']['ApplyPageBreak'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Checkbox("ApplyPageBreak", "ApplyPageBreak", 1, $isChecked=1, $Class='', $Display='', $Onclick='', $Disabled='');
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";

			$x .= '</body>'."\n";
		$x .= '</table>'."\n";
				
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<input type="hidden" id="NumOfTitleRow_Report" name="NumOfTitleRow_Report" value="1" />'."\n";
		
		return $x;
	}
			
	function Get_Statistics_RadarDiagram($SubjectMarkDisplayArr, $SubjectDisplay, $YearID)
	{
		global $PATH_WRT_ROOT, $Lang, $lreportcard;
		include_once($PATH_WRT_ROOT."includes/flashchart_basic_201301_in_use/php-ofc-library/open-flash-chart.php");
		
		# Get Academic Year Name
		$YearName = getCurrentAcademicYear();
		
		# Get Subject Info
		$SubjectInfo = $lreportcard->Get_Subject_Info();
		switch($SubjectDisplay)
		{
			case "Abbr": $subfix = "ABBR"; break;
			case "Desc": $subfix = "DES"; break;
			case "ShortForm": $subfix = "SNAME"; break;
		}	
		$SubjDisplayLang = Get_Lang_Selection("CH_".$subfix,"EN_".$subfix);

		foreach((array)$SubjectMarkDisplayArr as $StudentID => $SubjectRubricsArr)
		{
			
			$i = 0;
			$involvedSubjectArr = array();
			
			$involvedRubricsItems = $SubjectRubricsArr["InvolvedRubricsItems"];
			$involvedSubjectArr  = $SubjectRubricsArr["InvolvedSubject"];
			
			$SubjectValue = array();
			$SubjectLabels = array();
			foreach($involvedSubjectArr as $thisSubjectID)
			{
				$SubjectValue[] = $SubjectRubricsArr["SubjectRubricsAverage"][$thisSubjectID]?$SubjectRubricsArr["SubjectRubricsAverage"][$thisSubjectID]:0;
				$SubjectLabels[] = $SubjectInfo[$thisSubjectID][$SubjDisplayLang];
			}

			# title
			# Get Student Info 
//			$StudentInfo = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($YearID,'',$StudentID);
//			$StudentName = $StudentInfo[0]["StudentName"];
//			$FormName = $StudentInfo[0]["YearName"];
//			$title = new title( $YearName.$Lang['eRC_Rubrics']['GeneralArr']['SchoolYear2Ch']."\n".$FormName." ".$StudentName.$Lang['eRC_Rubrics']['StatisticArr']['RubricsReportArr']['Performance'] );
//			$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

			global $lreportcard_rubrics;
			if(!isset($lreportcard_rubrics))
			{
				include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
				$lreportcard_rubrics = new  libreportcardrubrics_rubrics();
			}
			$MaxLevel = $lreportcard_rubrics->Get_Subjects_Max_Rubrics_Level($YearID,$involvedSubjectArr);
			$r = new radar_axis($MaxLevel);
			$r->set_colour( '#C1CBFF' );
			$r->set_grid_colour( '#C1CBFF' );
			
			$labelsArr = array();
			for($ctr=0; $ctr<= $MaxLevel; $ctr++)
				$labelsArr[] = $ctr%2==0?(string)$ctr:'';
				
			$labels = new radar_axis_labels( $labelsArr );
			$labels->set_colour( '#9F819F' );
			$r->set_labels( $labels );
			
			$spoke_labels = new radar_spoke_labels($SubjectLabels);        
			$spoke_labels->set_colour( '#9F819F' );
			$r->set_spoke_labels( $spoke_labels );
			if($MaxLevel%2==0)	
				$r->set_steps(2); 
			
			$tooltip = new tooltip();
			$tooltip->set_proximity();
			
			$line = new line_dot();
			$line->set_values($SubjectValue);
			$line->set_width( 1 );
			$line->set_colour( '#111111' );
			$line->set_dot_size(0);
			$line->set_halo_size(0);
			$line->loop();
			$line->set_id( 'component' );
			
			$chart = new open_flash_chart();
			$chart->set_bg_colour( '#ffffff' );
//			$chart->set_title( $title );
			$chart->set_radar_axis( $r );
			$chart->add_element( $line );
			$chart->set_tooltip( $tooltip );
						
			$ChartArr[$StudentID] = $chart;
		}
		
		return $ChartArr;
	}

	function Get_Statistics_ClassPerformance_Flash_Chart($ClassSubjectRubricsArr, $SubjectDisplay, $YearID, $SubjectCode='')
	{
		$YearID = 0;
		
		global $PATH_WRT_ROOT, $Lang, $lreportcard;
		include_once($PATH_WRT_ROOT."includes/flashchart_basic_201301_in_use/php-ofc-library/open-flash-chart.php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
		
		$colourArr = array('#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414');
		
		# Get Academic Year Name
		$YearName = getCurrentAcademicYear();
		
		# Get Subject Info
		if(empty($SubjectCode))
		{
			$SubjectInfo = $lreportcard->Get_Subject_Info();
			switch($SubjectDisplay)
			{
				case "Abbr": $subfix = "ABBR"; break;
				case "Desc": $subfix = "DES"; break;
				case "ShortForm": $subfix = "SNAME"; break;
			}	
			$SubjDisplayLang = Get_Lang_Selection("CH_".$subfix,"EN_".$subfix);
		}
		
		# Get Rubrics Info
		$lreportcard_rubrics = new  libreportcardrubrics_rubrics();
		$RubricsInfo = $lreportcard_rubrics->Get_Rubics_Item_Info();
		$RubricsInfo = BuildMultiKeyAssoc($RubricsInfo, "RubricsItemID");
		$SubjectRubricsInfo = $lreportcard_rubrics->Get_Subject_RubricsItem_Mapping($YearID);
		
		foreach((array)$ClassSubjectRubricsArr as $thisClassName => $SubjectRubricsArr)
		{
			$chart = new open_flash_chart();
			
			$i = 0;
			$involvedSubjectArr = array();
			
			$involvedSubjectArr  = $SubjectRubricsArr["InvolvedSubject"];
			# Order Rubrics Item by Subject > Rubrics Level
			$involvedRubricsItems = array();
			if(!empty($SubjectRubricsArr["InvolvedRubricsItems"]))
			{
				foreach($involvedSubjectArr as $thisSubjectID)
				{
					foreach($SubjectRubricsInfo[$YearID][$thisSubjectID] as $thisRubricsItemID => $thisRubricsItemInfo)
					{
						if(in_array($thisRubricsItemID,$SubjectRubricsArr["InvolvedRubricsItems"]))
						{
							$involvedRubricsItems[] = $thisRubricsItemID;
						}
					}
				}
			}
			else
				$involvedRubricsItems = array(0);
			$involvedRubricsItems = array_unique($involvedRubricsItems);
			
			for($j=0; $j<count($involvedRubricsItems); $j++)
			{
				$RubricsItemID = $involvedRubricsItems[$j];

				$valueArr = array();
				$x_axis_title = array();
				$ItemDisplayLang = Get_Lang_Selection("DescriptionCh","DescriptionEn");
				$RubricsName = " ";
				if(!$RubricsInfo[$RubricsItemID])
					$RubricsName .= $Lang['eRC_Rubrics']['GeneralArr']['NotAssessed'];
				else
					$RubricsName .= $RubricsInfo[$RubricsItemID][$ItemDisplayLang];
				
				foreach($involvedSubjectArr as $SubjectID)
				{
					$thisPercent = $SubjectRubricsArr[$SubjectID][$RubricsItemID]/ $SubjectRubricsArr[$SubjectID]['Total']*100;
					$valueArr[]=$thisPercent?$thisPercent:0;
					
					if(empty($SubjectCode))
						$x_axis_title[] = $SubjectInfo[$SubjectID][$SubjDisplayLang];
					else
						$x_axis_title[] = $SubjectCode[$thisClassName][$SubjectID];
					
//					$x_axis_title[] = $SubjectInfo[$SubjectID][$SubjDisplayLang];
				}
				
				# bar 
				
				$thisColor = $colourArr[$i];
				while(!$thisColor)
				{
					$rand = rand(0,pow(16,6));
					$tmpColor = "#".sprintf("%X",$rand);
					if(!in_array($tmpColor,$colourArr))
						$thisColor = $tmpColor;
				}
//				$thisColor = -1-$j;
				
				$bar = new bar_stack_group();
				$bar->set_values( $valueArr );
				$bar->set_colour( $thisColor );
				$bar->set_tooltip( $RubricsName.':#val#%' );
				$bar->set_key( $RubricsName, '12' );
				$bar->set_id( $i );
				$bar->set_visible( true );
				$chart->add_element( $bar );
				$i++;
			}

			# title
			$title = new title( $thisClassName);
			$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

			# y axis
			$y_legend = new y_legend( $Lang['General']['Percentage']);
			$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
			
			$y = new y_axis();
			$y->set_stroke( 2 );
			$y->set_tick_length( 2 );
			$y->set_colour( '#999999' );
			$y->set_grid_colour( '#CCCCCC' );
			$y->set_range( 0, 100, 20);
			$y->set_offset(true);

			# x axis
			$x_legend = new x_legend( $Lang['eRC_Rubrics']['GeneralArr']['Subject'] );
			$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
			
			$x = new x_axis();
			$x->set_stroke( 2 );
			$x->set_tick_height( 2 );
			$x->set_colour( '#999999' );
			$x->set_grid_colour( '#CCCCCC' );
			$x->set_labels_from_array( $x_axis_title );
//			$xLab = new x_axis_labels();
//			$xLab->set_labels($x_axis_title);
//			$xLab->set_vertical();
//			$x->set_labels( $xLab );


			# tooltip
			$tooltip = new tooltip();
			$tooltip->set_hover();

			$chart->set_bg_colour( '#FFFFFF' );
			$chart->set_title( $title );
			$chart->set_y_legend( $y_legend );
			$chart->set_y_axis( $y );
			$chart->set_x_legend( $x_legend );
			$chart->set_x_axis( $x );
			$chart->set_tooltip($tooltip);
		
			$ChartArr[$thisClassName] = $chart;
			
		}
		
		return $ChartArr;
	}

	function Get_Statistics_SubjectPerformance_Flash_Chart($ClassSubjectRubricsArr, $SubjectDisplay, $YearID)
	{
		$YearID = 0;
		
		global $PATH_WRT_ROOT, $Lang, $lreportcard;
		include_once($PATH_WRT_ROOT."includes/flashchart_basic_201301_in_use/php-ofc-library/open-flash-chart.php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
		
		$colourArr = array('#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414');
		
		# Get Academic Year Name
		$YearName = getCurrentAcademicYear();
		
		# Get Subject Info
		$SubjectInfo = $lreportcard->Get_Subject_Info();
		switch($SubjectDisplay)
		{
			case "Abbr": $subfix = "ABBR"; break;
			case "Desc": $subfix = "DES"; break;
			case "ShortForm": $subfix = "SNAME"; break;
		}	
		$SubjDisplayLang = Get_Lang_Selection("CH_".$subfix,"EN_".$subfix);

		# Get Rubrics Info
		$lreportcard_rubrics = new  libreportcardrubrics_rubrics();
//		$RubricsInfo = $lreportcard_rubrics->Get_Rubics_Item_Info();
//		$RubricsInfo = BuildMultiKeyAssoc($RubricsInfo, "RubricsItemID");
		$RubricsInfo = $lreportcard_rubrics->Get_Subject_RubricsItem_Mapping();
		
		foreach((array)$ClassSubjectRubricsArr as $SubjectID => $ClassRubricsArr)
		{
			$chart = new open_flash_chart();
			
			$i = 0;
			$involvedClassArr = array();
			
			$SubjectRubricsID = array_keys((array)$RubricsInfo[$YearID][$SubjectID]);
			$involvedRubricsItems = empty($ClassRubricsArr["InvolvedRubricsItems"])?array(0):array_values(array_intersect($SubjectRubricsID,$ClassRubricsArr["InvolvedRubricsItems"]));
//			$involvedRubricsItems = empty($ClassRubricsArr["InvolvedRubricsItems"])?array(0):$ClassRubricsArr["InvolvedRubricsItems"];
			$involvedClassArr  = $ClassRubricsArr["InvolvedClass"];

			for($j=0; $j<count($involvedRubricsItems); $j++)
			{
				$RubricsItemID = $involvedRubricsItems[$j];
				
				$valueArr = array();
				$x_axis_title = array();
				$ItemDisplayLang = Get_Lang_Selection("DescriptionCh","DescriptionEn");
				$RubricsName = " ";
				if(!$RubricsInfo[$YearID][$SubjectID][$RubricsItemID])
					$RubricsName .= $Lang['eRC_Rubrics']['GeneralArr']['NotAssessed'];
				else
					$RubricsName .= $RubricsInfo[$YearID][$SubjectID][$RubricsItemID][$ItemDisplayLang];
				
				foreach($involvedClassArr as $thisClassName)
				{
					$thisPercent = $ClassRubricsArr[$thisClassName][$RubricsItemID]/ $ClassRubricsArr[$thisClassName]['Total']*100;
					$valueArr[]=$thisPercent?$thisPercent:0;
					
					$x_axis_title[] = $thisClassName;
				}
				
				# bar 
				$thisColor = $colourArr[$i];
				while(!$thisColor)
				{
					$rand = rand(0,pow(16,6));
					$tmpColor = "#".sprintf("%X",$rand);
					if(!in_array($tmpColor,$colourArr))
						$thisColor = $tmpColor;
				}
				
				$bar = new bar_stack_group();
				$bar->set_values( $valueArr );
				$bar->set_colour( $thisColor );
				$bar->set_tooltip( $RubricsName.':#val#%' );
				$bar->set_key( $RubricsName, '12' );
				$bar->set_id( $i );
				$bar->set_visible( true );
				$chart->add_element( $bar );
				$i++;
			}

			# title
			$title = new title( $SubjectInfo[$SubjectID][$SubjDisplayLang]);
			$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

			# y axis
			$y_legend = new y_legend( $Lang['General']['Percentage']);
			$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
			
			$y = new y_axis();
			$y->set_stroke( 2 );
			$y->set_tick_length( 2 );
			$y->set_colour( '#999999' );
			$y->set_grid_colour( '#CCCCCC' );
			$y->set_range( 0, 100, 20);
			$y->set_offset(true);

			# x axis
			$x_legend = new x_legend( $Lang['eRC_Rubrics']['GeneralArr']['Class'] );
			$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
			
			$x = new x_axis();
			$x->set_stroke( 2 );
			$x->set_tick_height( 2 );
			$x->set_colour( '#999999' );
			$x->set_grid_colour( '#CCCCCC' );
			$x->set_labels_from_array( $x_axis_title );
//			$xLab = new x_axis_labels();
//			$xLab->set_labels($x_axis_title);
//			$xLab->set_vertical();
//			$x->set_labels( $xLab );


			# tooltip
			$tooltip = new tooltip();
			$tooltip->set_hover();

			$chart->set_bg_colour( '#FFFFFF' );
			$chart->set_title( $title );
			$chart->set_y_legend( $y_legend );
			$chart->set_y_axis( $y );
			$chart->set_x_legend( $x_legend );
			$chart->set_x_axis( $x );
			$chart->set_tooltip($tooltip);
		
			$ChartArr[$SubjectID] = $chart;
			
		}
		
		return $ChartArr;
	}

	function Get_Statistics_General_Setting2_UI()
	{
		global $Lang;
		
		$SettingTable = $this->Get_Statistics_General_Setting2_Table(); 
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post" action="print.php" target="_blank">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'.$SettingTable.'</td>';		
				$x .= '</tr>'."\n";

				# edit bottom 
				$GenBtn = $this->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "js_Check_Form();", "submitBtn");
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<br>'."\n";
						$x .= '<div class="edit_bottom_v30">'."\n";
							$x .= $GenBtn."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";;		
				$x .= '</tr>'."\n";

			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";	
		
		return $x; 
	}

	function Get_Statistics_General_Setting2_Table()
	{
		global $Lang, $PATH_WRT_ROOT, $sys_custom;

		//include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		//include_once($PATH_WRT_ROOT.'includes/form_class_manage_ui.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
		include_once($PATH_WRT_ROOT.'includes/libreportcardrubrics_module.php');
		
		//$fcm_ui = new form_class_manage_ui();
		$scm_ui = new subject_class_mapping_ui();
		$lreportcard_module = new libreportcardrubrics_module();
		
		### Student Display Options
		
		# Form Stage Selection
		$thisOnChange = 'js_Changed_Form_Stage_Selection();';
		$FormStageSelection = $this->Get_Form_Stage_Selection('FormStageIDArr[]', '', $thisOnChange, $noFirst=1, $isAll=0, $firstTitle='', $CheckClassTeacher=1, $isMultiple=1);
		
		# Select All Btn
		$SelectAllFormStageBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('FormStageIDArr[]', 1, 'js_Changed_Form_Stage_Selection();');");
		$SelectAllFormBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('YearIDArr[]', 1, 'js_Changed_Form_Selection();');");
		$SelectAllClassBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('YearClassIDArr[]', 1, 'js_Reload_Class_Selection();')");
		$SelectAllStudentYearTermBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentYearTermIDArr[]', 1, 'js_Changed_Student_Year_Term_Selection();')");
		$SelectAllStudentSubjectBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_With_OptGroup('StudentSubjectIDArr[]', 1, 'js_Changed_Student_Subject_Selection();')");
		$SelectAllStudentSubjectGroupBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentSubjectGroupIDArr[]', 1, 'js_Changed_Student_Subject_Group_Selection();')");
		$SelectAllStudentBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentIDArr[]')");
		$SelectAllModuleBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('ModuleModuleID')");
		$SelectAllTopicBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('TopicIDArr[]')");
		
		
		# Form Selection
		//$thisOnChange = 'js_Changed_Form_Selection(this.value);';
		//$FormSelection = $fcm_ui->Get_Form_Selection('YearID', $SelectedYearID='', $thisOnChange, $noFirst=1, $isAll=0);
		//$FormSelection = $this->Get_Form_Selection('YearID', $SelectedYearID='', $thisOnChange, $noFirst=1, $isAll=0, $firstTitle='', $CheckClassTeacher=1);
		
		# Student Subject Selection
		$StudentSubjectSelection = $scm_ui->Get_Subject_Selection("StudentSubjectIDArr[]", '', $OnChange='js_Changed_Student_Subject_Selection();', $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=1);
		
		
		# Subject Selection
		$thisOnChange = "js_Reload_Topic_Selection()";
		$SubjectSelection = $scm_ui->Get_Subject_Selection("SubjectID", '', $thisOnChange, $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0);


		#Extra Info Selection
		if($sys_custom['UserExtraInformation'])
		{
			$ExtraInfoSelection = $this->Get_Extra_Info_Selection("ExtraItemID[]",'',$OnChange="js_Changed_Student_Extra_Info_Selection()");
			$SelectAllExtraInfoBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('ExtraItemID[]', 1, 'js_Changed_Student_Extra_Info_Selection();')");
		}
		
		# build module selection $ checkboxes
		$ModuleInfoArr = $lreportcard_module->Get_Module_Info();
		foreach($ModuleInfoArr as $thisModuleInfo)
		{
			list($ModuleID, $ModuleCode, $ModuleNameEn ,$ModuleNameCh) = $thisModuleInfo;
			$ModuleName = Get_Lang_Selection($ModuleNameCh,$ModuleNameEn)."($ModuleCode)";
			
			$ModuleSelectOptions[] = array($ModuleID,$ModuleName);
		}
		$ModuleSelection = getSelectByArray($ModuleSelectOptions," id='ModuleModuleID' name='ModuleID[]' class='ModuleMember' multiple size=10 ",0,0,1);
		
		$TableName = 'StudentSettings';
//		$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['Identity']['Student']);
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<table id="OptionTable_'.$TableName.'" class="form_table_v30">'."\n";
			# Select Students by Class / Subject Group
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SelectFrom'].'</td>'."\n";
				$x .= '<td>'."\n";
					// Class
					$thisDisplay = $Lang['SysMgr']['FormClassMapping']['Class'];
					$thisOnClick = "js_Changed_Student_Selection_Source('Class');";
					$x .= $this->Get_Radio_Button('SelectStudentFrom_Class', 'SelectStudentFrom', 'Class', $thisChecked=1, $Class="", $thisDisplay, $thisOnClick);
					// Subject Group
					$thisDisplay = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
					$thisOnClick = "js_Changed_Student_Selection_Source('SubjectGroup');";
					$x .= $this->Get_Radio_Button('SelectStudentFrom_SubjectGroup', 'SelectStudentFrom', 'SubjectGroup', $thisChecked=0, $Class="", $thisDisplay, $thisOnClick);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			# Form Stage Selection
			$x .= '<tr class="Student_FormClassTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['FormStage'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('FormStageSelectionSpan', $FormStageSelection, $SelectAllFormStageBtn);
					$x .= $this->Get_Warning_Message_Div('SelectFormStageWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['FormStage']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";	
			# Form Selection
			$x .= '<tr class="Student_FormClassTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['Form'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('FormSelectionSpan', $SelectionHTML='', $SelectAllFormBtn);
					$x .= $this->Get_Warning_Message_Div('SelectFormWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Form']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			# Class Selection
			$x .= '<tr class="Student_FormClassTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('ClassSelectionSpan', $SelectionHTML='', $SelectAllClassBtn);
					$x .= $this->Get_Warning_Message_Div('SelectClassWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Class']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# Term Selection
			$x .= '<tr class="Student_SubjectGroupTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['General']['Term'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentYearTermSelectionSpan', $SelectionHTML='', $SelectAllStudentYearTermBtn);
					$x .= $this->Get_Warning_Message_Div('SelectStudentYearTermWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Term']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			# Subject Selection
			$x .= '<tr class="Student_SubjectGroupTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentSubjectSelectionSpan', $StudentSubjectSelection, $SelectAllStudentSubjectBtn);
					$x .= $this->Get_Warning_Message_Div('SelectStudentSubjectWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Subject']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";								
			# Subject Group Selection
			$x .= '<tr class="Student_SubjectGroupTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentSubjectGroupSelectionSpan', $SelectionHTML='', $SelectAllStudentSubjectGroupBtn);
					$x .= $this->Get_Warning_Message_Div('SelectStudentSubjectGroupWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['SubjectGroup']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			if($sys_custom['UserExtraInformation'])
			{
				# Extra INfo Selection
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['AccountMgmt']['UserExtraInfo'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div>'."\n";
							$x .= '<span id="ExtraInfoSpan">'.$ExtraInfoSelection.'</span>'.$SelectAllExtraInfoBtn."\n";
							$x .= '<br />'."\n";
							$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			}
			# Student Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['Identity']['Student'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= '<span id="StudentSelectionSpan"></span>'.$SelectAllStudentBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="SelectStudentWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Student'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			#Subject Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= '<span>'.$SubjectSelection.'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="SelectSubjectWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Subject'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			#Topic Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][1]['Title'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= '<span id="TopicSelectionSpan">'.$this->Get_Ajax_Loading_Image().'</span>'.$SelectAllTopicBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="SelectTopicWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['TopicLevel'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			# Module Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['MenuTitle'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= $ModuleSelection.$SelectAllModuleBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="ModuleIDWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Module'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";

//				$x .= '<td>'.$ModuleSelection.'</td>'."\n";
			$x .= '</tr>'."\n";
														
		$x .= '</table>'."\n";
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<br style="clear:both;" />'."\n";
		
		return $x;
	}
	
	function Get_Statistics_ClassTopicPerformance_Flash_Chart($SubjectID, $ClassTopicRubricsArr, $SubjectDisplay, $YearID, $TopicCode='')
	{
		$YearID = 0;
		
		global $PATH_WRT_ROOT, $Lang, $lreportcard;
		include_once($PATH_WRT_ROOT."includes/flashchart_basic_201301_in_use/php-ofc-library/open-flash-chart.php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
		
		$colourArr = array('#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414');
		
		# Get Academic Year Name
		$YearName = getCurrentAcademicYear();
		
		# Get Subject Info
		$SubjectInfo = $lreportcard->Get_Subject_Info();
		$SubjDisplayLang = Get_Lang_Selection("CH_DES","EN_DES");
		$SubjectName = $SubjectInfo[$SubjectID][$SubjDisplayLang];

		# Get Rubrics Info
		$lreportcard_rubrics = new  libreportcardrubrics_rubrics();
//		$RubricsInfo = $lreportcard_rubrics->Get_Rubics_Item_Info();
//		$RubricsInfo = BuildMultiKeyAssoc($RubricsInfo, "RubricsItemID");
		$RubricsInfo = $lreportcard_rubrics->Get_Subject_RubricsItem_Mapping($YearID, $SubjectID);
		$RubricsInfo = $RubricsInfo[$YearID][$SubjectID];
		$RubricsItemIDArr = array_keys($RubricsInfo);

		//if(trim($TopicCode) != '')
		if(!is_array($TopicCode) && trim($TopicCode) != '' || is_array($TopicCode))
		{
			# Get_Topic_Info
			$lreportcard_topic = new libreportcardrubrics_topic();
			$TopicInfo = $lreportcard_topic->Get_Topic_Info($SubjectID);
			$TopicInfo = BuildMultiKeyAssoc($TopicInfo, "TopicID");
			$TopicDisplayLang = Get_Lang_Selection("TopicNameCh","TopicNameEn");
		}
		
		foreach((array)$ClassTopicRubricsArr as $thisClassName => $TopicRubricsArr)
		{
			$chart = new open_flash_chart();
			
			$i = 0;
			$involvedTopicArr = array();
			
			$involvedRubricsItems = empty($TopicRubricsArr["InvolvedRubricsItems"])?array(0):array_values(array_intersect($RubricsItemIDArr,$TopicRubricsArr["InvolvedRubricsItems"]));
//			$involvedRubricsItems = empty($TopicRubricsArr["InvolvedRubricsItems"])?array(0):$TopicRubricsArr["InvolvedRubricsItems"];
			$involvedTopicArr  = $TopicRubricsArr["InvolvedTopic"];
			
			for($j=0; $j<count($involvedRubricsItems); $j++)
			{
				$RubricsItemID = $involvedRubricsItems[$j];
				
				$valueArr = array();
				$x_axis_title = array();
				$ItemDisplayLang = Get_Lang_Selection("DescriptionCh","DescriptionEn");
				$RubricsName = " ";
				if(!$RubricsInfo[$RubricsItemID])
					$RubricsName .= $Lang['eRC_Rubrics']['GeneralArr']['NotAssessed'];
				else
					$RubricsName .= $RubricsInfo[$RubricsItemID][$ItemDisplayLang];
				
				foreach($involvedTopicArr as $TopicID)
				{
					$thisPercent = $TopicRubricsArr[$TopicID][$RubricsItemID]/ $TopicRubricsArr[$TopicID]['Total']*100;
					$valueArr[]=$thisPercent?$thisPercent:0;
					
					//if(trim($TopicCode) != '')
					if(!is_array($TopicCode) && trim($TopicCode) != '' || is_array($TopicCode))
						$x_axis_title[] = $TopicCode[$thisClassName][$TopicID];
					else
						$x_axis_title[] = $TopicInfo[$TopicID][$TopicDisplayLang];
				}
				
				# bar 
				
				$thisColor = $colourArr[$i];
				while(!$thisColor)
				{
					$rand = rand(0,pow(16,6));
					$tmpColor = "#".sprintf("%X",$rand);
					if(!in_array($tmpColor,$colourArr))
						$thisColor = $tmpColor;
				}
				
				$bar = new bar_stack_group();
				$bar->set_values( $valueArr );
				$bar->set_colour( $thisColor );
				$bar->set_tooltip( $RubricsName.':#val#%' );
				$bar->set_key( $RubricsName, '12' );
				$bar->set_id( $i );
				$bar->set_visible( true );
				$chart->add_element( $bar );
				$i++;
			}

			# title
			$title = new title( $thisClassName);
			$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

			# y axis
			$y_legend = new y_legend( $Lang['General']['Percentage']);
			$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
			
			$y = new y_axis();
			$y->set_stroke( 2 );
			$y->set_tick_length( 2 );
			$y->set_colour( '#999999' );
			$y->set_grid_colour( '#CCCCCC' );
			$y->set_range( 0, 100, 20);
			$y->set_offset(true);

			# x axis
			$x_legend = new x_legend( $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][1]['Title'] );
			$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
			
			$x = new x_axis();
			$x->set_stroke( 2 );
			$x->set_tick_height( 2 );
			$x->set_colour( '#999999' );
			$x->set_grid_colour( '#CCCCCC' );
			$x->set_labels_from_array( $x_axis_title );
//			$xLab = new x_axis_labels();
//			$xLab->set_labels($x_axis_title);
//			$xLab->set_vertical();
//			$x->set_labels( $xLab );


			# tooltip
			$tooltip = new tooltip();
			$tooltip->set_hover();

			$chart->set_bg_colour( '#FFFFFF' );
			$chart->set_title( $title );
			$chart->set_y_legend( $y_legend );
			$chart->set_y_axis( $y );
			$chart->set_x_legend( $x_legend );
			$chart->set_x_axis( $x );
			$chart->set_tooltip($tooltip);
		
			$ChartArr[$thisClassName] = $chart;
			
		}
		
		return $ChartArr;
	}
	
	function Get_Statistics_TopicClassPerformance_Flash_Chart($SubjectID, $TopicClassRubricsArr, $SubjectDisplay, $YearID)
	{
		$YearID = 0;
		
		global $PATH_WRT_ROOT, $Lang, $lreportcard;
		include_once($PATH_WRT_ROOT."includes/flashchart_basic_201301_in_use/php-ofc-library/open-flash-chart.php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
		
		$colourArr = array('#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414');
//		$colourArr = array(-1,-2,-3,-4,-5,-6);
		
		# Get Academic Year Name
		$YearName = getCurrentAcademicYear();
		
		# Get Subject Info
		$SubjectInfo = $lreportcard->Get_Subject_Info();
		$SubjDisplayLang = Get_Lang_Selection("CH_DES","EN_DES");
		$SubjectName = $SubjectInfo[$SubjectID][$SubjDisplayLang];

		# Get Rubrics Info
		$lreportcard_rubrics = new  libreportcardrubrics_rubrics();
		$RubricsInfo = $lreportcard_rubrics->Get_Subject_RubricsItem_Mapping($YearID, $SubjectID);
		$RubricsInfo = $RubricsInfo[$YearID][$SubjectID];
		$RubricsItemIDArr = array_keys($RubricsInfo);

		# Get_Topic_Info
		$lreportcard_topic = new  libreportcardrubrics_topic();
		$TopicInfo = $lreportcard_topic->Get_Topic_Info($SubjectID);
		$TopicInfo = BuildMultiKeyAssoc($TopicInfo, "TopicID");
		$TopicDisplayLang = Get_Lang_Selection("TopicNameCh","TopicNameEn");
		
		foreach((array)$TopicClassRubricsArr as $TopicID => $ClassRubricsArr)
		{
			$chart = new open_flash_chart();
			
			$i = 0;
			$involvedClassArr = array();
			
			$involvedRubricsItems = empty($ClassRubricsArr["InvolvedRubricsItems"])?array(0):array_values(array_intersect($RubricsItemIDArr,$ClassRubricsArr["InvolvedRubricsItems"]));
			$involvedClassArr  = $ClassRubricsArr["InvolvedClass"];
			
			for($j=0; $j<count($involvedRubricsItems); $j++)
			{
				$RubricsItemID = $involvedRubricsItems[$j];
				
				$valueArr = array();
				$x_axis_title = array();
				$ItemDisplayLang = Get_Lang_Selection("DescriptionCh","DescriptionEn");
				$RubricsName = " ";
				if(!$RubricsInfo[$RubricsItemID])
					$RubricsName .= $Lang['eRC_Rubrics']['GeneralArr']['NotAssessed'];
				else
					$RubricsName .= $RubricsInfo[$RubricsItemID][$ItemDisplayLang];
				
				foreach($involvedClassArr as $thisClassName)
				{
					$thisPercent = $ClassRubricsArr[$thisClassName][$RubricsItemID]/ $ClassRubricsArr[$thisClassName]['Total']*100;
					$valueArr[]=$thisPercent?$thisPercent:0;
					
					$x_axis_title[] = $thisClassName;
				}
				
				# bar 
				
				$thisColor = $colourArr[$i];
				while(!$thisColor)
				{
					$rand = rand(0,pow(16,6));
					$tmpColor = "#".sprintf("%X",$rand);
					if(!in_array($tmpColor,$colourArr))
						$thisColor = $tmpColor;
				}
				
				$bar = new bar_stack_group();
				$bar->set_values( $valueArr );
				$bar->set_colour( $thisColor );
				$bar->set_tooltip( $RubricsName.':#val#%' );
				$bar->set_key( $RubricsName, '12' );
				$bar->set_id( $i );
				$bar->set_visible( true );
				$chart->add_element( $bar );
				$i++;
			}

			# title
			$title = new title( $TopicInfo[$TopicID][$TopicDisplayLang]);
			$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

			# y axis
			$y_legend = new y_legend( $Lang['General']['Percentage']);
			$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
			
			$y = new y_axis();
			$y->set_stroke( 2 );
			$y->set_tick_length( 2 );
			$y->set_colour( '#999999' );
			$y->set_grid_colour( '#CCCCCC' );
			$y->set_range( 0, 100, 20);
			$y->set_offset(true);

			# x axis
			$x_legend = new x_legend( $Lang['eRC_Rubrics']['GeneralArr']['Class'] );
			$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
			
			$x = new x_axis();
			$x->set_stroke( 2 );
			$x->set_tick_height( 2 );
			$x->set_colour( '#999999' );
			$x->set_grid_colour( '#CCCCCC' );
			$x->set_labels_from_array( $x_axis_title );
//			$xLab = new x_axis_labels();
//			$xLab->set_labels($x_axis_title);
//			$xLab->set_vertical();
//			$x->set_labels( $xLab );


			# tooltip
			$tooltip = new tooltip();
			$tooltip->set_hover();

			$chart->set_bg_colour( '#FFFFFF' );
			$chart->set_title( $title );
			$chart->set_y_legend( $y_legend );
			$chart->set_y_axis( $y );
			$chart->set_x_legend( $x_legend );
			$chart->set_x_axis( $x );
			$chart->set_tooltip($tooltip);
		
			$ChartArr[$TopicID] = $chart;
			
		}
		
		return $ChartArr;
	}	
#
# Statistics End
#######################################################################################################

	function Get_Reports_Class_Rubrics_Count_Report($SubjectRubricsCount, $SubjectMaxLevel,$ClassStudentCountArr)
	{
		global $Lang;
 		
		foreach((array)$SubjectRubricsCount as $thisSubjectName => $ClassRubricsCount)
		{
			//$ReportTopicEn =  $Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['AverageNumber']." ".$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['Getting']." ".$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['level']." ".$SubjectMaxLevel[$thisSubjectName];
			//$ReportTopicCh =  $Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['Getting'].$SubjectMaxLevel[$thisSubjectName].$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['level'].$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['AverageNumber'];
			//$ReportTopic = Get_Lang_Selection($ReportTopicCh,$ReportTopicEn);
			$ReportTopic = $Lang['eRC_Rubrics']['ReportsArr']['ClassRubricsCount']['ReportTitle'];

			$x .= '<table class="report_table" border="1" cellpadding=5 width="50%" style="margin:5px;">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td width="30%">'.$thisSubjectName.'</td>'."\n";
					$x .= '<td width="70%">'.$ReportTopic.'</td>'."\n";
				$x .= '</tr>'."\n";
			foreach((array)$ClassStudentCountArr as $thisClassName => $StudentCount)
			{
				$RubricsCount = $ClassRubricsCount[$thisClassName];
				$x .= '<tr>'."\n";
					$x .= '<td>'.$thisClassName.'</td>'."\n";
					$x .= '<td>'.($RubricsCount/$StudentCount).'</td>'."\n";
				$x .= '</tr>'."\n";
			}
			$x .= '</table>'."\n";
		}
		
		return $x;
	}

	function Get_Reports_Form_Rubrics_Count_Report($SubjectRubricsCount, $YearMaxLevel,$YearStudentCountArr)
	{
		
		global $Lang;
 		
 		foreach((array)$SubjectRubricsCount as $thisYearName => $ClassRubricsCount)
		{
			$thisYearMax = 5; // hard code first.
			$StudentCount = $YearStudentCountArr[$thisYearName];
			
			//$ReportTopicEn =  $Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['AverageNumber']." ".$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['Getting']." ".$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['level']." ".$thisYearMax;
			//$ReportTopicCh =  $Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['Getting'].$thisYearMax.$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['level'].$Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['AverageNumber'];
			//$ReportTopic = Get_Lang_Selection($ReportTopicCh,$ReportTopicEn);
			$ReportTopic = $Lang['eRC_Rubrics']['ReportsArr']['ClassRubricsCount']['ReportTitle'];

			$x .= '<table class="report_table" border="1" cellpadding=5 width="50%" style="margin:5px;">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td width="30%">'.$thisYearName.'</td>'."\n";
					$x .= '<td width="70%">'.$ReportTopic.'</td>'."\n";
				$x .= '</tr>'."\n";
			foreach((array)$ClassRubricsCount as $thisSubjectName => $RubricsCount)
			{
				$x .= '<tr>'."\n";
					$x .= '<td>'.$thisSubjectName.'</td>'."\n";
					$x .= '<td>'.($RubricsCount/$StudentCount).'</td>'."\n";
				$x .= '</tr>'."\n";
			}
			$x .= '</table>'."\n";
		}
		
		return $x;
	}
	
	function Get_Reports_Form_Rubrics_Count_Setting_UI()
	{
		global $Lang;
		
		$FormRubricsCountSettingTable = $this->Get_Reports_Form_Rubrics_Count_Setting_Table(); 
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post" action="print.php" target="_blank">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'.$FormRubricsCountSettingTable.'</td>';		
				$x .= '</tr>'."\n";

				# edit bottom 
				$GenBtn = $this->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "js_Check_Form();", "submitBtn");
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<br>'."\n";
						$x .= '<div class="edit_bottom_v30">'."\n";
							$x .= $GenBtn."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";;		
				$x .= '</tr>'."\n";

			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";	
		
		return $x; 
	}

	function Get_Reports_Form_Rubrics_Count_Setting_Table()
	{
		global $Lang, $PATH_WRT_ROOT, $sys_custom;

		//include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		//include_once($PATH_WRT_ROOT.'includes/form_class_manage_ui.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
		include_once($PATH_WRT_ROOT.'includes/libreportcardrubrics_module.php');
		
		//$fcm_ui = new form_class_manage_ui();
		$scm_ui = new subject_class_mapping_ui();
		$lreportcard_module = new libreportcardrubrics_module();
		
		### Student Display Options
		# Form Selection
		//$FormSelection = $fcm_ui->Get_Form_Selection('YearIDArr[]', $SelectedYearID='', $thisOnChange, $noFirst=1, $isAll=0,1);
		$FormSelection = $this->Get_Form_Selection('YearIDArr[]', $SelectedYearID='', $thisOnChange, $noFirst=1, $isAll=0, $firstTitle='', $TeachingFormOnly=1, $isMultiple=1);
		
		# Select All Btn
		$SelectAllFormBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('YearIDArr[]',1)");
		$SelectAllModuleBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('ModuleModuleID',1)");
	
		# Subject Selection
		$SubjectSelection = $scm_ui->Get_Subject_Selection("SubjectIDArr[]", '', $OnChange='', $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=1);
		$SelectAllSubjectBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_With_OptGroup('SubjectIDArr[]',1)");

		# build module selection $ checkboxes
		$ModuleInfoArr = $lreportcard_module->Get_Module_Info();
		foreach($ModuleInfoArr as $thisModuleInfo)
		{
			list($ModuleID, $ModuleCode, $ModuleNameEn ,$ModuleNameCh) = $thisModuleInfo;
			$ModuleName = Get_Lang_Selection($ModuleNameCh,$ModuleNameEn)."($ModuleCode)";
			
			$ModuleSelectOptions[] = array($ModuleID,$ModuleName);
		}
		$ModuleSelection = getSelectByArray($ModuleSelectOptions," id='ModuleModuleID' name='ModuleID[]' class='ModuleMember' multiple size=10 ",0,0,1);
		
		$TableName = 'StudentSettings';
//		$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['Identity']['Student']);
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<table id="OptionTable_'.$TableName.'" class="form_table_v30">'."\n";
			# Form Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['Form'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= '<span>'.$FormSelection.'</span>'.$SelectAllFormBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="SelectYearIDWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Form'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			#Subject Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= '<span>'.$SubjectSelection.'</span>'.$SelectAllSubjectBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="SelectSubjectWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Subject'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			# Subject Name Display
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SubjectName'].'</td>'."\n";
				$x .= '<td>'."\n";
					$thisChecked = 'checked';
					foreach ($Lang['eRC_Rubrics']['GeneralArr']['SubjectDisplayArr'] as $thisDisplayType => $thisTypeName)
					{
						$x .= '<input type="radio" id="SubjectDisplay_'.$thisDisplayType.'" name="SubjectDisplay" value="'.$thisDisplayType.'" '.$thisChecked.'>';
						$x .= '<label for="SubjectDisplay_'.$thisDisplayType.'">'.$thisTypeName.'</label>&nbsp;'."\n";
						
						$thisChecked = '';
					}
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";			
			# Module Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['MenuTitle'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= $ModuleSelection.$SelectAllModuleBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="ModuleIDWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Module'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";

//				$x .= '<td>'.$ModuleSelection.'</td>'."\n";
			$x .= '</tr>'."\n";
														
		$x .= '</table>'."\n";
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<br style="clear:both;" />'."\n";
		
		return $x;
	}

	function Get_Extra_Info_Selection($IDName, $Selected='',$ParOnChange='', $OtherTagInfo='' )
	{
		global $Lang, $lreportcard;
		
		$ExtraInfoItemArr = $lreportcard->Get_User_Extra_Info_Item();
		
		$CategoryNameLang = Get_Lang_Selection("CategoryNameCh","CategoryNameEn");
		$ItemNameLang = Get_Lang_Selection("ItemNameCh","ItemNameEn");
		
		$options = array();
		$options[] = array(0,$Lang['eRC_Rubrics']['GeneralArr']['NotClassified']);
		foreach($ExtraInfoItemArr as $ExtraItem)
		{
			$options[] = array($ExtraItem['ItemID'],$ExtraItem[$ItemNameLang],$ExtraItem[$CategoryNameLang]);
		}
		
		if(!is_array($IDName))
			$TagsInfo = " id='$IDName' name='$IDName' ";
		else
			$TagsInfo = " id='{$IDName[0]}' name='{$IDName[0]}' ";
			
		$TagsInfo.= " multiple size='10' ";
		
		if(trim($ParOnChange)!='')
			$TagsInfo .= ' onchange="'.$ParOnChange.'" ';
			
		$TagsInfo .= $OtherTagInfo;
			
		$Selection = $this->GET_SELECTION_BOX_WITH_OPTGROUP($options, $TagsInfo, $ParDefault='', $Selected);
		
		return $Selection;
	}

	function Get_Student_Selection($ID_Name, $YearClassIDArr='', $SelectedStudentID='', $Onchange='', $noFirst=0, $isMultiple=0, $isAll=0, $ExtraInfoItemArr=array(), $SubjectGroupIDArr='')
	{
		global $Lang, $lreportcard;
		
		### Get Student from Class
		if ($YearClassIDArr != '') {
			include_once("form_class_manage.php");
			$libFCM = new form_class_manage();
			$StudentInfoArr = $libFCM->Get_Student_By_Class($YearClassIDArr);
			$StudentIDFromClass = Get_Array_By_Key($StudentInfoArr, 'UserID');
		}
		
		### Get Student from Subject Group
		if ($SubjectGroupIDArr != '') {
			include_once("subject_class_mapping.php");
			$libSCM = new subject_class_mapping();
			$StudentInfoArr = $libSCM->Get_Subject_Group_Student_List($SubjectGroupIDArr);
			$StudentIDFromSubjectGroup = Get_Array_By_Key($StudentInfoArr, 'UserID');
		}
		
		### Get Student Info
		if ($YearClassIDArr != '' && $SubjectGroupIDArr != '') {
			$StudentIDArr = array_values(array_intersect($StudentIDFromClass, $StudentIDFromSubjectGroup));
		}
		else if ($YearClassIDArr != '') {
			$StudentIDArr = $StudentIDFromClass;
		}
		else if ($SubjectGroupIDArr != '') {
			$StudentIDArr = $StudentIDFromSubjectGroup;
		}
		$StudentInfoArr = $lreportcard->Get_Student_Class_Info($StudentIDArr);
		$numOfStudent = count($StudentInfoArr);
		
		if(!empty($ExtraInfoItemArr))
		{
			//$StudentIDArr = Get_Array_By_Key($StudentInfoArr,"UserID");
			$ExtraInfoUserItemArr = $lreportcard->Get_User_Extra_Info_Mapping($StudentIDArr,$ExtraInfoItemArr);
			$ExtraInfoUserArr = Get_Array_By_Key($ExtraInfoUserItemArr,"UserID");
			$ExtraInfoUserArr = array_unique($ExtraInfoUserArr);
		}	
		
		$selectArr = array();
		for ($i=0; $i<$numOfStudent; $i++)
		{
			$thisUserID = $StudentInfoArr[$i]['UserID'];
			if(!empty($ExtraInfoItemArr) && !in_array($thisUserID,$ExtraInfoUserArr))
				continue;
			
			$thisClassName = Get_Lang_Selection($StudentInfoArr[$i]['ClassTitleB5'], $StudentInfoArr[$i]['ClassTitleEN']);
			$thisClassNumber = $StudentInfoArr[$i]['ClassNumber'];
			
			$thisDisplay = $StudentInfoArr[$i]['StudentName'].' ('.$thisClassName.'-'.$thisClassNumber.')';
			$selectArr[$thisUserID] = $thisDisplay;
		}
		
		$onchange = '';
		if ($Onchange != "")
			$onchange = ' onchange="'.$Onchange.'" ';
			
		$style = '';
		if ($isMultiple)
			$style = ' multiple="true" size="10"';
			
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.$style;
		
		$firstTitle = ($isAll)? $Lang['SysMgr']['FormClassMapping']['All']['NonTeachingStaff'] : $Lang['SysMgr']['FormClassMapping']['Select']['NonTeachingStaff'];
		$firstTitle = Get_Selection_First_Title($firstTitle);
		
		return getSelectByAssoArray($selectArr, $selectionTags, $SelectedStudentID, $isAll, $noFirst, $firstTitle);
	}

	# Empty function 	
	function Get_Empty_Row($Height, $NumOfRow=1)
	{
		global $PATH_WRT_ROOT;
		
		$x = "";
		for ($i=0; $i<$NumOfRow; $i++)
		{
			$x .= "<tr><td>".$this->Get_Empty_Image($Height)."</td></tr>\n";
		}
		
		return $x;
	}
	
	function Get_Empty_Image($Height)
	{
		global $PATH_WRT_ROOT;
		
		$emptyImagePath = $this->Get_Empty_Image_Path();
		return "<img src='".$emptyImagePath."' height='".$Height."'>";
	}
	
	function Get_Empty_Image_Path()
	{
		global $PATH_WRT_ROOT;
		return $PATH_WRT_ROOT."images/2009a/10x10.gif";
	}
	
	function Get_Tick_Image()
	{
		global $image_path, $LAYOUT_SKIN;
		
		return '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ereportcard/icon_present_bw.gif" width="10" align="absmiddle">';
	}
	
	function Get_School_Logo($Height)
	{
		global $PATH_WRT_ROOT, $ReportCard_Rubrics_CustomSchoolName;
		
		$LogoPath = $PATH_WRT_ROOT."file/reportcard_rubrics/templates/".$ReportCard_Rubrics_CustomSchoolName."/logo.jpg";
		return "<img src='".$LogoPath."' height='".$Height."'>";
	}
	
	function Get_Setting_Admin_Group_UI($page='', $order='', $field='', $Keyword='')
	{
		global $Lang;
		
		# SearchBox
		$SearchBox = $this->Get_Search_Box_Div('Keyword', $Keyword);
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post" action="admin_group.php">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						
						# Main Table  
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div class="content_top_tool">'."\n";
										$x .= '<div class="Conntent_tool">'."\n";
											$x .= $this->GET_LNK_NEW("new_step1.php")."\n";
										$x .= '</div>'."\n";
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
								$x .= '<td align="right">'.$SearchBox.'</td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td colspan="2" align="right">'."\n";
									$x .= '<div class="common_table_tool">'."\n";
								        $x .= '<a class="tool_edit" href="javascript:checkEdit(document.form1,\'AdminGroupIDArr[]\',\'new_step1.php\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
										$x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'AdminGroupIDArr[]\',\'admin_group_delete.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
									$x .= '</div>'."\n";		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td colspan="2">'."\n";
									$x .= '<div id="AdminGroupDiv">'.$this->Get_Setting_Admin_Group_DBTable($page, $order, $field, $Keyword).'</div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						  
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";
		
		return $x;	
	}
	
	function Get_Setting_Admin_Group_DBTable($page='',$order='',$field='', $Keyword='')
	{
		global $Lang, $page_size, $ck_settings_admin_group_page_size;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		include_once("libreportcardrubrics_module.php");
		$lreportcard = new libreportcardrubrics();
		
		$field = ($field=='')? 1 : $field;
		$order = ($order=='')? 1 : $order;
		$page = ($page=='')? 1 : $page;

		if (isset($ck_settings_admin_group_page_size) && $ck_settings_admin_group_page_size != "") $page_size = $ck_settings_admin_group_page_size;
		$li = new libdbtable2007($field, $order, $page);
		
		$sql = $lreportcard->Get_Admin_Group_DBTable_Sql($Keyword);
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
		
		$GroupNameField = Get_Lang_Selection('ag.AdminGroupNameCh', 'ag.AdminGroupNameEn');
		$li->field_array = array("ag.AdminGroupCode", $GroupNameField, "Count(agu.UserID)", "ag.DateModified");
		$li->column_array = array(0, 0, 0, 0, 0, 0);
		$li->wrap_array = array(0, 0, 0, 0, 0, 0);
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
		$li->column_list .= "<th width='15%' >".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['GeneralArr']['Code'])."</th>\n";
		$li->column_list .= "<th width='40%'>".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['GeneralArr']['Name'])."</th>\n";
		$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['NumOfMember'])."</th>\n";
		$li->column_list .= "<th width='25%'>".$li->column_IP25($pos++, $Lang['eRC_Rubrics']['GeneralArr']['LastModifiedDate'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("AdminGroupIDArr[]")."</td>\n";
		$li->no_col = $pos + 2;
	
		$x .= $li->display();
		//debug_pr($li->built_sql());
		//debug_pr(mysql_error());
		$x .= '<input type="hidden" name="pageNo" value="'.$li->pageNo.'" />';
		$x .= '<input type="hidden" name="order" value="'.$li->order.'" />';
		$x .= '<input type="hidden" name="field" value="'.$li->field.'" />';
		$x .= '<input type="hidden" name="page_size_change" value="" />';
		$x .= '<input type="hidden" name="numPerPage" value="'.$li->page_size.'" />';
	
		return $x;
	}
	
	function Get_Setting_Admin_Group_Step1_UI($AdminGroupID='')
	{
		global $Lang, $eRC_Rubrics_ConfigArr, $lreportcard;
		
		$isEdit = ($AdminGroupID != '')? true : false;
		
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['MenuTitle'], "javascript:js_Back_To_Admin_Group_List()"); 
		if ($isEdit)
		{
			$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
			$SubmitBtnText = $Lang['Btn']['Submit'];
			
			$AdminGroupInfoArr = $lreportcard->Get_Admin_Group_Info($AdminGroupID);
			$AccessRightInfoArr = $lreportcard->Get_Admin_Group_Access_Right($AdminGroupID);
			$AccessRightInfoArr = Get_Array_By_Key($AccessRightInfoArr, 'AdminGroupRightName');
		}
		else
		{
			$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");
			$SubmitBtnText = $Lang['Btn']['Continue'];
			
			### Step Table
			$STEPS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['NewAdminGroupStepArr'][1], 1);
			$STEPS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['NewAdminGroupStepArr'][2], 0);
			$StepTable = $this->GET_STEPS_IP25($STEPS_OBJ);
		}
		
		### Navigation
		$PageNavigation = $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
		
		### Continue Button
		$btn_Continue = $this->GET_ACTION_BTN($SubmitBtnText, "button", $onclick="js_Update_Group_Info();", $id="Btn_Submit");
				
		### Cancel Button
		$btn_Cancel = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Back_To_Admin_Group_List()", $id="Btn_Cancel");
		
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post">'."\n";
		
			# Navigation & Steps
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td align="left" class="navigation">'.$PageNavigation.'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'.$StepTable.'</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			
			### Admin Group Info
			$x .= $this->GET_NAVIGATION2_IP25($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AdminGroupInfo']);
			$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
				$x .= '<col class="field_title">';
				$x .= '<col class="field_c">';
				
				# Code
				$thisValue = intranet_htmlspecialchars($AdminGroupInfoArr[0]['AdminGroupCode']);
				$MaxLength = $eRC_Rubrics_ConfigArr['MaxLength']['AdminGroup']['Code'];
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AdminGroupCode'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input name="AdminGroupCode" id="AdminGroupCode" class="textbox_name required" value="'.$thisValue.'" maxlength="'.$MaxLength.'">'."\n";
						$x .= $this->Get_Thickbox_Warning_Msg_Div("AdminGroupCodeWarningDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# English Name
				$thisValue = intranet_htmlspecialchars($AdminGroupInfoArr[0]['AdminGroupNameEn']);
				$MaxLength = $eRC_Rubrics_ConfigArr['MaxLength']['AdminGroup']['Name'];
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title" rowspan="2"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AdminGroupName'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['En'].'</span>';
						$x .= '<span class="row_content_v30"><input name="AdminGroupNameEn" id="AdminGroupNameEn" class="textbox_name required" value="'.$thisValue.'"  maxlength="'.$MaxLength.'"></span>'."\n";
						$x .= $this->Spacer();
						$x .= $this->Get_Thickbox_Warning_Msg_Div("AdminGroupNameEnWarningDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Chinese Name
				$thisValue = intranet_htmlspecialchars($AdminGroupInfoArr[0]['AdminGroupNameCh']);
				$MaxLength = $eRC_Rubrics_ConfigArr['MaxLength']['AdminGroup']['Name'];
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['Ch'].'</span>';
						$x .= '<span class="row_content_v30"><input name="AdminGroupNameCh" id="AdminGroupNameCh" class="textbox_name required" value="'.$thisValue.'"  maxlength="'.$MaxLength.'"></span>'."\n";
						$x .= $this->Spacer();
						$x .= $this->Get_Thickbox_Warning_Msg_Div("AdminGroupNameChWarningDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<br style="clear:both;" />'."\n";
			
			
			### Access Right
			$x .= $this->GET_NAVIGATION2_IP25($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AccessRight']);
			$x .= '<br style="clear:both;" />'."\n";
			
			$MenuArr = $lreportcard->Get_Left_Menu_Structure_Array($CheckAccessRight=0);
			foreach ((array)$MenuArr as $thisMenu => $thisSubMenuArr)
			{
				$thisChkID = $thisMenu.'_Chk';
				
				$x .= '<div class="able_board">'."\n";
					$x .= '<div class="form_sub_title_v30">'."\n";
						$x .= '<em>'."\n";
							$x .= '- <span class="field_title"> '.$Lang['eRC_Rubrics'][$thisMenu.'Arr']['MenuTitle'].' </span>-'."\n";
							$x .= '&nbsp;&nbsp;&nbsp;'."\n";
							$x .= $this->Get_Checkbox($thisChkID, $thisChkID, 1, $isChecked=0, $Class='', $Lang['Btn']['SelectAll'], "js_Select_All_Checkboxes('".$thisMenu."')", $Disabled='');
						$x .= '</em>'."\n";
						$x .= '<p class="spacer"></p>'."\n";
					$x .= '</div>'."\n";
					$x .= '<table class="form_table_v30">'."\n";
						$x .= '<col class="field_title">'."\n";
						$x .= '<col class="field_c">'."\n";
						
						foreach ((array)$thisSubMenuArr as $thisSubMenu => $thisSubMenuInfoArr)
						{
							$thisSubMenuTitle = $thisSubMenuInfoArr['Title'];
							$thisCheckboxValue = $thisMenu.'_'.$thisSubMenu;
							$thisChecked = (in_array($thisCheckboxValue, (array)$AccessRightInfoArr))? 'checked' : '';
							
							$x .= '<tr valign="top">'."\n";
								$x .= '<td class="field_title">'.$thisSubMenuTitle.'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<input type="checkbox" name="AccessiableMenuArr[]" class="'.$thisMenu.'" value="'.$thisCheckboxValue.'" '.$thisChecked.' />'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						}
					$x .= '</table>'."\n";
				$x .= '</div>'."\n";
			}
			
			$x .= '<div class="edit_bottom_v30">';
				$x .= $btn_Continue."&nbsp;".$btn_Cancel;
			$x .= '</div>';
			
			$x .= '<input type="hidden" id="isEdit" name="isEdit" value="'.$isEdit.'" />'."\n";
			$x .= '<input type="hidden" id="AdminGroupID" name="AdminGroupID" value="'.$AdminGroupID.'" />'."\n";
		$x .= '</form>'."\n";
		
		return $x;
	}
	
	function Get_Setting_Admin_Group_Step2_UI($AdminGroupID='', $FromNew=0)
	{
		global $Lang, $eRC_Rubrics_ConfigArr, $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT;
		
		$isEdit = ($AdminGroupID != '')? true : false;
		
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['MenuTitle'], "javascript:js_Back_To_Admin_Group_List()"); 
		if ($FromNew == 1)
		{
			$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");
			
			### Step Table
			$STEPS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['NewAdminGroupStepArr'][1], 0);
			$STEPS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['NewAdminGroupStepArr'][2], 1);
			$StepTable = $this->GET_STEPS_IP25($STEPS_OBJ);
			
			### Cancel Button
			$btn_Cancel = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Back_To_Admin_Group_List()", $id="Btn_Cancel");
		}
		else
		{
			$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['MemberList'], "javascript:js_Back_To_Member_List();");
			$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AddMember'], "");
			
			### Cancel Button
			$btn_Cancel = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Back_To_Member_List()", $id="Btn_Cancel");
		}
		
		### Navigation
		$PageNavigation = $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
		
		### Choose Member Btn
		$permitted_type = 1;
		$btn_ChooseMember = $this->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "javascript:newWindow('".$PATH_WRT_ROOT."home/common_choose/index.php?fieldname=SelectedUserIDArr[]&page_title=SelectMembers&permitted_type=$permitted_type&DisplayGroupCategory=1', 9)");
		
		### Auto-complete login search
		$UserLoginInput = '';
		$UserLoginInput .= '<div style="float:left;">';
			$UserLoginInput .= '<input type="text" id="UserSearchTb" name="UserSearchTb" value=""/>';
		$UserLoginInput .= '</div>';
		
		### Member Selection Box & Remove all Btn
		$MemberSelectionBox = $this->GET_SELECTION_BOX(array(), 'name="SelectedUserIDArr[]" id="SelectedUserIDArr[]" class="select_studentlist" size="15" multiple="multiple"', "");
		$btn_RemoveSelected = $this->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "javascript:checkOptionRemove(document.getElementById('SelectedUserIDArr[]'))");
		
		### Submit Button
		$btn_Submit = $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", $onclick="js_Add_Member();", $id="Btn_Submit");
				
		
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post">'."\n";
		
			# Navigation & Steps
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td align="left" class="navigation">'.$PageNavigation.'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'.$StepTable.'</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= $this->Get_Admin_Group_Info_Table($AdminGroupID);
			$x .= '<br style="clear:both;" />'."\n";
			
			# Add Member Info
			$x .= '<table width="99%" border="0" cellpadding"0" cellspacing="0" align="center">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="tabletext" width="40%">'.$Lang['General']['ChooseUser'].'</td>'."\n";
					$x .= '<td class="tabletext"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>'."\n";
					$x .= '<td class="tabletext" width="60%">'.$Lang['General']['SelectedUser'].'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="tablerow2" valign="top">'."\n";
						$x .= '<table width="100%" border="0" cellpadding="3" cellspacing="0">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td class="tabletext">'.$Lang['General']['FromGroup'].'</td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td class="tabletext">'.$btn_ChooseMember.'</td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td class="tabletext"><i>'.strtolower($Lang['General']['Or']).'</i></td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td class="tabletext">'."\n";
									$x .= $Lang['General']['SearchByLoginID'];
									$x .= '<br />'."\n";
									$x .= $UserLoginInput;
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
					$x .= '<td class="tabletext" ><img src="'. $image_path.'/'.$LAYOUT_SKIN.'"/10x10.gif" width="10" height="1"></td>'."\n";
					$x .= '<td align="left" valign="top">'."\n";
						$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td align="left">'."\n";
									$x .= $MemberSelectionBox;
									$x .= $btn_RemoveSelected;
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= $this->Get_Thickbox_Warning_Msg_Div("MemberSelectionWarningDiv", '* '.$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['WarningArr']['SelectionContainsMember']);
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr><td>&nbsp;</td></tr>'."\n";
				$x .= '<tr><td colspan="4"><div class="edit_bottom"><br />'.$btn_Submit.'&nbsp;'.$btn_Cancel.'</div></td></tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<input type="hidden" id="AdminGroupID" name="AdminGroupID" value="'.$AdminGroupID.'" />'."\n";
			$x .= '<input type="hidden" id="FromNew" name="FromNew" value="'.$FromNew.'" />'."\n";
		$x .= '</form>'."\n";
		
		return $x;
	}
	
	function Get_Setting_Admin_Group_Member_List_UI($AdminGroupID, $page='', $order='', $field='', $Keyword='')
	{
		global $Lang, $lreportcard;
		
		# Navigation
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AdminGroupList'], "javascript:js_Back_To_Admin_Group_List();");
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['MemberList'], "");
		$PageNavigation = $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
		
		# SearchBox
		$SearchBox = $this->Get_Search_Box_Div('Keyword_Member_List', $Keyword);
		
		# Back Button
		$btn_Back = $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", $onclick="js_Back_To_Admin_Group_List()", $id="Btn_Back");
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post" action="member_list.php">'."\n";
			# Navigation & Steps
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td align="left" class="navigation">'.$PageNavigation.'</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= $this->Get_Admin_Group_Info_Table($AdminGroupID);
			$x .= '<br style="clear:both;" />'."\n";
			
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						
						# Main Table  
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div class="content_top_tool">'."\n";
										$x .= '<div class="Conntent_tool">'."\n";
											$x .= $this->GET_LNK_ADD("new_step2.php?AdminGroupID=$AdminGroupID")."\n";
										$x .= '</div>'."\n";
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
								$x .= '<td align="right">'.$SearchBox.'</td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td colspan="2" align="right">'."\n";
									$x .= '<div class="common_table_tool">'."\n";
								        $x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'UserIDArr[]\',\'member_delete.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
									$x .= '</div>'."\n";		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td colspan="2">'."\n";
									$x .= '<div id="MemberDiv">'.$this->Get_Setting_Admin_Group_Member_List_DBTable($AdminGroupID, $page, $order, $field, $Keyword).'</div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						  
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
			
			$x .= '<div class="edit_bottom_v30">';
				$x .= $btn_Back;
			$x .= '</div>';
			
			$x .= '<input type="hidden" id="AdminGroupID" name="AdminGroupID" value="'.$AdminGroupID.'" />'."\n";
		$x .= '</form>'."\n";
		
		return $x;	
	}
	
	function Get_Setting_Admin_Group_Member_List_DBTable($AdminGroupID, $page='',$order='',$field='', $Keyword='')
	{
		global $Lang, $page_size, $ck_settings_admin_group_member_list_page_size;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		include_once("libreportcardrubrics_module.php");
		$lreportcard = new libreportcardrubrics();
		
		$field = ($field=='')? 0 : $field;
		$order = ($order=='')? 1 : $order;
		$page = ($page=='')? 1 : $page;

		if (isset($ck_settings_admin_group_member_list_page_size) && $ck_settings_admin_group_member_list_page_size != "") $page_size = $ck_settings_admin_group_member_list_page_size;
		$li = new libdbtable2007($field, $order, $page);
		
		$sql = $lreportcard->Get_Admin_Group_Member_List_DBTable_Sql($AdminGroupID, $Keyword);
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
		
		$li->field_array = array("iu.EnglishName", "iu.ChineseName");
		$li->column_array = array(0, 0, 0, 0);
		$li->wrap_array = array(0, 0, 0, 0);
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
		$li->column_list .= "<th width='47%' >".$li->column_IP25($pos++, $Lang['General']['EnglishName'])."</th>\n";
		$li->column_list .= "<th width='47%'>".$li->column_IP25($pos++, $Lang['General']['ChineseName'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("UserIDArr[]")."</td>\n";
		$li->no_col = $pos + 2;
	
		$x .= $li->display();
		//debug_pr($li->built_sql());
		//debug_pr(mysql_error());
		$x .= '<input type="hidden" name="pageNo" value="'.$li->pageNo.'" />';
		$x .= '<input type="hidden" name="order" value="'.$li->order.'" />';
		$x .= '<input type="hidden" name="field" value="'.$li->field.'" />';
		$x .= '<input type="hidden" name="page_size_change" value="" />';
		$x .= '<input type="hidden" name="numPerPage" value="'.$li->page_size.'" />';
	
		return $x;
	}
	
	function Get_Admin_Group_Info_Table($AdminGroupID)
	{
		global $Lang, $lreportcard;
		
		$AdminGroupInfoArr = $lreportcard->Get_Admin_Group_Info($AdminGroupID);
		
		$x = '';
		$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
			$x .= '<col class="field_title">';
			$x .= '<col class="field_c">';
			# Code
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AdminGroupCode'].'</td>'."\n";
				$x .= '<td>'.$AdminGroupInfoArr[0]['AdminGroupCode'].'</td>'."\n";
			$x .= '</tr>'."\n";
			# Name
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['AdminGroupName'].'</td>'."\n";
				$x .= '<td>'.Get_Lang_Selection($AdminGroupInfoArr[0]['AdminGroupNameCh'], $AdminGroupInfoArr[0]['AdminGroupNameEn']).'</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Topic_Comparison_Report_Setting_UI()
	{
		global $Lang;
		
		$RubricsReportSettingTable = $this->Get_Topic_Comparison_Report_Setting_Table(); 
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post" action="print.php" target="_blank">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'.$RubricsReportSettingTable.'</td>';		
				$x .= '</tr>'."\n";

				# edit bottom 
				$GenBtn = $this->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "js_Check_Form();", "submitBtn");
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<br>'."\n";
						$x .= '<div class="edit_bottom_v30">'."\n";
							$x .= $GenBtn."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";;		
				$x .= '</tr>'."\n";

			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";	
		
		return $x; 
	}

	function Get_Topic_Comparison_Report_Setting_Table()
	{
		global $Lang, $PATH_WRT_ROOT, $sys_custom, $lreportcard;

		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
		
		//$fcm_ui = new form_class_manage_ui();
		$scm_ui = new subject_class_mapping_ui();
		
		### Student Display Options
		# From Stage Selection  //($ID_Name, $SelectedFormStageID='', $Onchange='', $noFirst=0, $isAll=0, $firstTitle='', $TeachingFormOnly=0, $isMultiple=0)
		$thisOnChange = 'js_Changed_Form_Stage(this.value);';
		$FormStageSelection = $this->Get_Form_Stage_Selection('FormStageIDArr[]', $FormStageID, $thisOnChange, 1, 0, '', 1, 1);
		
		# Student Subject Selection
		$StudentSubjectSelection = $scm_ui->Get_Subject_Selection("StudentSubjectIDArr[]", '', $OnChange='js_Changed_Student_Subject_Selection();', $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=1);
		
		# Select All Btn
		$SelectAllFormStageBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Form_Stage('FormStageIDArr[]')");
		$SelectAllFormBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Year('YearIDArr[]')");
		$SelectAllClassBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Year_Class('YearClassIDArr[]')");
		$SelectAllStudentBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentIDArr[]')");
		$SelectAllTopicBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('TopicIDArr[]')");
		$SelectAllModuleBtn1 = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('FirstModuleID[]')");
		$SelectAllModuleBtn2 = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('SecondModuleID[]')");
		
		$SelectAllStudentSubjectBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentSubjectIDArr[]', 1, 'js_Changed_Student_Subject_Selection();')");
		$SelectAllStudentYearTermBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentYearTermIDArr[]', 1, 'js_Changed_Student_Year_Term_Selection();')");
		$SelectAllStudentSubjectGroupBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentSubjectGroupIDArr[]', 1, 'js_Changed_Student_Subject_Group_Selection();')");
		
		# Subject Selection
		$SubjectSelection = $scm_ui->Get_Subject_Selection("SubjectIDArr[]", '', $OnChange='js_Changed_Subject_Selection();', $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=1);
		$SelectAllSubjectBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Subject('SubjectIDArr[]')");

		# Extra Info Selection
		if($sys_custom['UserExtraInformation'])
		{
			$ExtraInfoSelection = $this->Get_Extra_Info_Selection("ExtraItemID[]",'',$OnChange="js_Reload_Student_Selection()");
			$SelectAllExtraInfoBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Extra_Info()");
		}
		
		# Tear Term Module Selection
		$AcademicYearSelection1 = getSelectAcademicYear('FirstAcademicYearID', $tag='onchange="js_Changed_AcademicYear_Selection( \'First\')"', $noFirst=1, $noPastYear=0, $lreportcard->AcademicYearID);
		$AcademicYearSelection2 = getSelectAcademicYear('SecondAcademicYearID', $tag='onchange="js_Changed_AcademicYear_Selection(\'Second\')"', $noFirst=1, $noPastYear=0, $lreportcard->AcademicYearID);
		
		$TableName = 'StudentSettings';
		$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['Identity']['Student']);
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<table id="OptionTable_'.$TableName.'" class="form_table_v30">'."\n";
		#	 Select Students by Class / Subject Group
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SelectFrom'].'</td>'."\n";
				$x .= '<td>'."\n";
					// Class
					
					$thisDisplay = $Lang['SysMgr']['FormClassMapping']['Class'];
					$thisOnClick = "js_Changed_Student_Selection_Source('Class');";
					$x .= $this->Get_Radio_Button('SelectStudentFrom_Class', 'SelectStudentFrom', 'Class', 'checked', $Class="", $thisDisplay, $thisOnClick);
					// Subject Group
					$thisDisplay = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
					$thisOnClick = "js_Changed_Student_Selection_Source('SubjectGroup');";
					$x .= $this->Get_Radio_Button('SelectStudentFrom_SubjectGroup', 'SelectStudentFrom', 'SubjectGroup', '', $Class="", $thisDisplay, $thisOnClick);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			# Form Stage Selection
			$x .= '<tr class="Student_FormClassTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['FormStage'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('FormStageSelectionSpan', $FormStageSelection, $SelectAllFormStageBtn);
					$x .= $this->Get_Warning_Message_Div('SelectFormStageWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['FormStage']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";								
			# Form Selection
			$x .= '<tr class="Student_FormClassTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['Form'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('YearSelectionSpan', $SelectionHTML='', $SelectAllFormBtn);
					$x .= $this->Get_Warning_Message_Div('SelectFormWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Form']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			# Class Selection
			$x .= '<tr class="Student_FormClassTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('ClassSelectionSpan', $SelectionHTML='', $SelectAllClassBtn);
					$x .= $this->Get_Warning_Message_Div('SelectClassWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Class']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# Term Selection
			$x .= '<tr class="Student_SubjectGroupTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['General']['Term'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentYearTermSelectionSpan', $SelectionHTML='', $SelectAllStudentYearTermBtn);
					$x .= $this->Get_Warning_Message_Div('SelectStudentYearTermWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Term']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			# Subject Selection
			$x .= '<tr class="Student_SubjectGroupTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentSubjectSelectionSpan', $StudentSubjectSelection, $SelectAllStudentSubjectBtn);
					$x .= $this->Get_Warning_Message_Div('SelectStudentSubjectWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Subject']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";								
			# Subject Group Selection
			$x .= '<tr class="Student_SubjectGroupTr">'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_MultipleSelection_And_SelectAll_Div('StudentSubjectGroupSelectionSpan', $SelectionHTML='', $SelectAllStudentSubjectGroupBtn);
					$x .= $this->Get_Warning_Message_Div('SelectStudentSubjectGroupWarningDiv', $Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['SubjectGroup']);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		/*
		# Form Stage Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['FormStage'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= $FormStageSelection.$SelectAllFormStageBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="SelectFormStageWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['FormStage'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		
			# Form Stage Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['FormStage'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= $FormStageSelection.$SelectAllFormStageBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="SelectFormStageWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['FormStage'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '</tr>'."\n";
			# Form Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['Form'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= '<span id="YearSelectionSpan"></span>'.$SelectAllYearBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="SelectFormWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Form'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			# Class Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= '<span id="ClassSelectionSpan"></span>'.$SelectAllClassBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="SelectClassWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Class'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			*/
			if($sys_custom['UserExtraInformation'])
			{
				# Extra INfo Selection
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['AccountMgmt']['UserExtraInfo'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div>'."\n";
							$x .= '<span id="ExtraInfoSpan">'.$ExtraInfoSelection.'</span>'.$SelectAllExtraInfoBtn."\n";
							$x .= '<br />'."\n";
							$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			}
			# Student Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['Identity']['Student'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= '<span id="StudentSelectionSpan"></span>'.$SelectAllStudentBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="SelectStudentWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Student'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		$x .= '<br style="clear:both;" />'."\n";	
		$TableName = 'SubjectSettings';
		$x .= $this->Get_Table_Show_Hide_Navigation($TableName, $Lang['SysMgr']['SubjectClassMapping']['Subject']);
		$x .= '<br style="clear:both;" />'."\n";
		$x .= '<table id="OptionTable_'.$TableName.'" class="form_table_v30">'."\n";
			#Subject Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= '<span>'.$SubjectSelection.'</span>'.$SelectAllSubjectBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="SelectSubjectWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Subject'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# Subject Name Display
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['SubjectName'].'</td>'."\n";
				$x .= '<td>'."\n";
					$thisChecked = 'checked';
					foreach ($Lang['eRC_Rubrics']['GeneralArr']['SubjectDisplayArr'] as $thisDisplayType => $thisTypeName)
					{
						$x .= '<input type="radio" id="SubjectDisplay_'.$thisDisplayType.'" name="SubjectDisplay" value="'.$thisDisplayType.'" '.$thisChecked.'>';
						$x .= '<label for="SubjectDisplay_'.$thisDisplayType.'">'.$thisTypeName.'</label>&nbsp;'."\n";
						
						$thisChecked = '';
					}
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";	
			
			#Topic Selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][3]['Title'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= '<span id="TopicSelectionSpan"></span>'.$SelectAllTopicBtn."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
					$x .= '</div>'."\n";
					$x .= '<div id="SelectTopicWarningDiv" style="display:none;">'."\n";
						$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['LearningDetails'].'</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
					
		$x .= '</table>'."\n";
		$x .= '<div class="dotline"></div>'."\n";
		$x .= '<br>'."\n";
		$x .= '<table width="100%">'."\n";
			$x .= '<tr valign="top">'."\n";
				$x .= '<td width="50%">'."\n";
					$x .= '<div class="report_option report_option_title">- '.$Lang['eRC_Rubrics']['ReportsArr']['TopicCompareReportArr']['Report1'].' - </div>'."\n";
					$x .= '<div class="table_broad">'."\n";
						$x .= '<table class="form_table_v30">'."\n";
							$x .= '<tr>'."\n"; 
								$x .= '<td class="field_title">'.$Lang['General']['SchoolYear'].'</td>';
								$x .= '<td><div style="float:left;">'.$AcademicYearSelection1.'</div><div id="FirstYearTermSelectionDiv" style="float:left;"></div></td>';
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n"; 
								$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['ReportsArr']['TopicCompareReportArr']['Module'].'</td>';
								$x .= '<td>'."\n";
									$x .= '<div id="FirstModuleSelectionDiv"></div>'."\n";
									$x .= $SelectAllModuleBtn1;
									$x .= '<br />'."\n";
									$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
									$x .= '<div id="FirstModuleIDWarningDiv" style="display:none;">'."\n";
										$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Module'].'</span>'."\n";
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n"; 
						$x .= '</table>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
				$x .= '<td width="50%">'."\n";
					$x .= '<div class="report_option report_option_title">- '.$Lang['eRC_Rubrics']['ReportsArr']['TopicCompareReportArr']['Report2'].' - </div>'."\n";
					$x .= '<div class="table_broad">'."\n";
						$x .= '<table class="form_table_v30">'."\n";
							$x .= '<tr>'."\n"; 
								$x .= '<td class="field_title">'.$Lang['General']['SchoolYear'].'</td>';
								$x .= '<td>'."\n";
									$x .= '<div style="float:left;">'.$AcademicYearSelection2.'</div><div id="SecondYearTermSelectionDiv" style="float:left;"></div>'."\n";
									$x .= $this->Spacer()."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n"; 
							$x .= '<tr>'."\n"; 
								$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eRC_Rubrics']['ReportsArr']['TopicCompareReportArr']['Module'].'</td>';
								$x .= '<td>'."\n";
									$x .= '<div id="SecondModuleSelectionDiv"></div>'."\n";
									$x .= $SelectAllModuleBtn2;
									$x .= '<br />'."\n";
									$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
									$x .= '<div id="SecondModuleIDWarningDiv" style="display:none;">'."\n";
										$x .= '<span class="tabletextrequire">* '.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Module'].'</span>'."\n";
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n"; 
							
						$x .= '</table>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		$x .= $this->MandatoryField();
		
		return $x;
	}
	
	function Get_MultipleSelection_And_SelectAll_Div($SelectionSpanID, $SelectionHTML, $SelectAllBtn)
	{
		global $Lang;
		
		$x = '';
		$x .= '<div>'."\n";
			$x .= '<span id="'.$SelectionSpanID.'">'.$SelectionHTML.'</span>'.$SelectAllBtn."\n";
			$x .= '<br />'."\n";
			$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
		$x .= '</div>'."\n";
		
		
		return $x;
	}
	
	function Get_Warning_Message_Div($WarningDivID, $WarningMsg)
	{
		$x = '';
		
		$x .= '<div id="'.$WarningDivID.'" style="display:none;">'."\n";
			$x .= '<span class="tabletextrequire">* '.$WarningMsg.'</span>'."\n";
		$x .= '</div>'."\n";
		
		return $x;
	}
}
?>