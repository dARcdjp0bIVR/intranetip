<?
# using: yat

class libgeneralsettings extends libdb
{
	function libgeneralsettings()
	{
		$this->libdb();
	}
	
	/*function updateGeneralSettings($module='', $data=array())
	{
		foreach($data as $field=>$value)
		{
			# check should insert or update
			$sql = "select count(RecordID) from GENERAL_SETTING where Module='$module' and SettingName='$field'";
			$count = $this->returnVector($sql);	
			if($count[0])
			{	# update
				$sql = "update GENERAL_SETTING set SettingValue='$value', DateModified=now(), ModifiedBy=". $_SESSION['UserID'] ." where Module='$module' and SettingName='$field'";
			}
			else
			{	# insert
				$sql = "insert into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput,InputBy) values ('$module', '$field', '$value', now(), ". $_SESSION['UserID'] .")";
			}
			$this->db_db_query($sql);
		}
	}
	
	function getGeneralSetting($module='', $SettingName='') 
	{
		$sql = "SELECT SettingName, SettingValue FROM GENERAL_SETTING WHERE Module='$module'";
		if($SettingName)
			$sql .= " and SettingName='$SettingName'";
			
		return $this->returnArray($sql);
	}*/
	
	
	function Save_General_Setting($ModuleName='',$SettingList=array()) {

		foreach ($SettingList as $SettingName => $Value) {
			$sql = 'insert into GENERAL_SETTING (
								Module,
								SettingName, 
								SettingValue, 
								DateInput,
								InputBy)
							VALUES (
								\''.$ModuleName.'\',
								\''.$SettingName.'\',
								\''.$this->Get_Safe_Sql_Query($Value).'\',
								NOW(),
								\''.$_SESSION['UserID'].'\') 
							ON DUPLICATE KEY UPDATE 
								SettingValue=\''.$this->Get_Safe_Sql_Query($Value).'\',
								DateModified=NOW(), 
								ModifiedBy=\''.$_SESSION['UserID'].'\'';

			$Result[] = $this->db_db_query($sql);
		
		}
		
		return !in_array(false,$Result);
	}
	
	/*
	$SettingName = array("'admin_user'");
	*/
	function Get_General_Setting($ModuleName='',$SettingName=array()) {
		$sql = 'select 
							SettingName, 
							SettingValue 
						from 
							GENERAL_SETTING 
						where 
							Module = \''.$ModuleName.'\' ';
		if (sizeof($SettingName) > 0)  {
			$sql .= '	and 
								SettingName in ('.implode(',',$SettingName).')';
		}
		$Setting = $this->returnArray($sql);

		for ($i=0; $i< sizeof($Setting); $i++) {
			$Return[$Setting[$i]['SettingName']] = $Setting[$i]['SettingValue'];
		}
		
		return $Return;
	}
	
	function Remove_General_Setting($ModuleName,$SettingName)
	{
		if(empty($ModuleName) || empty($SettingName)) return false;
		
		$sql = "
			DELETE FROM
				GENERAL_SETTING
			WHERE
				Module = '$ModuleName'
				AND SettingName IN ('".implode("','",(array)$SettingName)."')
		";
		
		$Result = $this->db_db_query($sql);
		return $Result;
	}
}