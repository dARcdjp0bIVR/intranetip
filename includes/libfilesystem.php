<?php
# using: 

##################### Change Log [Start] #############
#	Date:	2017-07-05 (Carlos)
#			added file_unzip_ext($file, $dest) that use python script /includes/unzip.py to unzip files in order to solve Chinese file name issue with Linux unzip command.
#
#	Date:	2015-11-10 (Omas)
#			modified clearTempFiles() to support remove folders
#
#	Date:	2014-07-28 (Carlos)
#			modified validateEmail(), validateURL(), convertURLS(), convertMail() to use preg_xxxx() functions while ereg_xxxx() functions are deprecated in PHP5.3+
#
#	Date:	2013-09-12 (Ivan)
#			modified file_unzip() to chmod to 0777 instead of "0644" (not support string actually and change to 777 for student offical upload)
#
#	Date:	2013-09-17 (Siuwan)
#			copy copy_fck_flash_image_upload_to_new_id_loc(), copy_fck_flash_image_upload_from_src_to_dest() from eclass40 for copying fckeditor content
#
#	Date:	2013-02-01 (Carlos)
#			add more general mime types to getMIMEType()
#
#	Date:	2011-12-06 (Yuen)
#			introduced clearTempFiles() to remove old files for house keeping
#
#	Date:	2011-08-10 (Carlos)
#			replaced all occurences of basename() with $this->get_file_basename() since basename() cannot handle multibyte characters if php locate setting is not suitable
#
#	Date:	2011-05-18 (Henry Chow)
#			modified file_zip(), change directory if only $to_dir != ""  
# 
#	Date:	2011-04-27 (Henry Chow)
#			modified unzipFile(), re-locate the "chdir"  
# 
#	Date:	2011-02-16 Marcus
#			modified CHECK_CSV_FILE_FORMAT, trim spaces for field checking. 
# 
#	Date:	2010-11-10 Max
#			Added a function SafeGuardFileName,rename_filename_from_utf8_to_big5,convert_basename_from_utf8_to_big5,get_file_basename to get valid filename,rename file to big5
# 
#	Date:	2010-10-06 Marcus
#			added folder_content_copy, copy content only without copy the folder itself
#
#	Date:	2010-05-28 YatWoon
#			update folder_new(), allow create dir recursive
#
#	Date:	2010-04-13 [YatWoon]
#			add function returnFlashImageInsertPath(), returnRelativePathFlashImageInsertPath(), get_fck_copy_filepath()
#			copy_fck_flash_image_upload(), deleteDirectory(), deleteAllFckImageTmpFiles()
#			for user can upload image vie fckeditor (functions are copied from eClass40 and updated)
#
##################### Change Log [End] #############

class libfilesystem {

        var $rs = array();
        var $ftime = array();
        var $consider_time;
        var $no_recursive;
        var $consider_hash;

        function libfilesystem(){
        }

        ######################################################################

        function file_escapeshellarg($file){
                return escapeshellarg($file);
        }

        function file_write($body, $file){
                $x = (($fd = fopen($file, "w")) && (fputs($fd, $body))) ? 1 : 0;
                fclose ($fd);
                return $x;
        }

        function file_read($file){
                 clearstatcache();
                 if(file_exists($file) && is_file($file) && filesize($file)!=0){
                    $x =  ($fd = fopen($file, "r")) ? fread($fd,filesize($file)) : "";
                    if ($fd)
                        fclose ($fd);
                 }
                 return $x;

        }

        function file_read_csv($file)
        {

               		global $g_encoding_unicode;
					
					## Modified by Michael Cheung (2009-10-28) - CSV are imported in UTF-8 format
					
	        		/* if($g_encoding_unicode == false)
                  	 {
							// big5
							setlocale(LC_ALL, 'zh_TW.BIG5');
	 				  } */
	        
	        			 $i = 0;
                        if(file_exists($file) && filesize($file)!=0)
                        {
                                $fp = fopen($file,"r");
                                # check if function mb_substr and mb_strlen are supported, don't use fgetcsv() for the special character sake
                                if(function_exists("mb_substr") && function_exists("mb_strlen"))
                                {
                                        while (!feof($fp))
                                        {
                                                $buffer = trim(fgets($fp, 20480));
                                                # combine contents that contain link break
                                                $LastCell = substr(strrchr($buffer, ","), 1);
                                                if(substr($LastCell, 0, 1)=="\"")
                                                {
                                                        while(!feof($fp) && substr($buffer, -1)!="\"")
                                                        {
                                                                $buffer .= "\n".trim(fgets($fp, 20480));
                                                        }
                                                }
                                                $data = $this->mb_csv_split($buffer);
                                                $x[$i++] = $data;
                                        }
                                }
                                else
                                {
                                        while($data = fgetcsv($fp,filesize($file))){
                                                $x[$i++] = $data;
                    }
                    }
                                fclose ($fp);
            }
            return $x;
        }

        function mb_substr_ed($str, $start, $length=NULL, $encoding=NULL)
        {
        	global $g_encoding_unicode, $intranet_default_lang, $intranet_default_lang_set;

        	if ($length==NULL)
        	{
        		$length = $this->mb_strlen_ed($str);
        	}

        	if (!$g_encoding_unicode && ($intranet_default_lang=="en" || $intranet_default_lang=="b5") && (!isset($intranet_default_lang_set) || (isset($intranet_default_lang_set) && !in_array("gb", $intranet_default_lang_set))) )
        	{
        		# Explicitly specify BIG5 as encoding for ANSI case if system is using either Eng or Big5
				$strNow = (@mb_substr($str, $start, $length, "BIG5"));
        		if ($strNow=="")
        		{
        			$strNow = mb_substr($str, $start, $length);
				}
				return $strNow;
        	} else
        	{
        		return mb_substr($str, $start, $length);
        	}
        }

		function mb_strlen_ed($str, $encoding=NULL)
        {
        	global $g_encoding_unicode, $intranet_default_lang, $intranet_default_lang_set;

        	if (!$g_encoding_unicode && ($intranet_default_lang=="en" || $intranet_default_lang=="b5") && (!isset($intranet_default_lang_set) || (isset($intranet_default_lang_set) && !in_array("gb", $intranet_default_lang_set))) )
        	{
        		# Explicitly specify BIG5 as encoding for ANSI case if system is using either Eng or Big5
        		$length = (@mb_strlen($str, "BIG5"));
        		if ($length=="")
        		{
        			$length = mb_strlen($str);
				}
				return $length;
        	} else
        	{
        		return mb_strlen($str);
        	}
        }

		// new method to handle special characters of csv file content
		function mb_csv_split($line, $delim = ',', $removeQuotes = true)
		{
			$fields = array();
			$fldCount = 0;
			$inQuotes = false;

			for ($i = 0; $i < $this->mb_strlen_ed($line); $i++) {
				if (!isset($fields[$fldCount])) $fields[$fldCount] = "";
				$tmp = $this->mb_substr_ed($line, $i, $this->mb_strlen_ed($delim));
				if ($tmp === $delim && !$inQuotes) {
					$fldCount++;
					$i+= $this->mb_strlen_ed($delim) - 1;
				}
				else if ($fields[$fldCount] == "" && $this->mb_substr_ed($line, $i, 1) == '"' && !$inQuotes) {
					if (!$removeQuotes) $fields[$fldCount] .= $this->mb_substr_ed($line, $i, 1);
					$inQuotes = true;
				}
				else if ($this->mb_substr_ed($line, $i, 1) == '"') {
					if ($this->mb_substr_ed($line, $i+1, 1) == '"') {
							$i++;
							$fields[$fldCount] .= $this->mb_substr_ed($line, $i, 1);
					} else {
							if (!$removeQuotes) $fields[$fldCount] .= $this->mb_substr_ed($line, $i, 1);
							$inQuotes = false;
					}
				}
				else {
					$fields[$fldCount] .= $this->mb_substr_ed($line, $i, 1);
				}
			}

			return $fields;
		}

        function file_name($file){
                $file = $this->get_file_basename($file);
                return substr($file, 0, strpos($file,"."));
        }

        function file_ext($file){
                $file = $this->get_file_basename($file);
                return strtolower(substr($file, strrpos($file,".")));
        }

        function file_remove($file)
        {
                return (file_exists($file)) ? unlink($file) : 0;
        }

        function file_copy($source, $dest){
                $dest .= (is_dir($dest)) ? "/".$this->get_file_basename($source) : "";
                //echo $source." ||| ". $dest.'<br>';
                return copy($source, $dest);
        }

        function file_rename($oldfile, $newfile){
                return rename($oldfile, $newfile);
        }
        
        function clearTempFiles($module_folder, $timeout=""){
			global $intranet_root,$ck_course_id;
			
			$path = $intranet_root.'/file/'.$module_folder;
			if ($timeout=="")
			{
				$timeout = 60*60*24; // one day by default
			}
			$curTime = time();
			
			if (file_exists($path))
			{
				$allDir = scandir($path);
				
				if (!empty($allDir))
				{
				  	foreach($allDir as $allFiles)
				  	{	
				  		if ($allFiles == '.' || $allFiles == '..')
					  		continue;

				  		if ($curTime - filemtime($path.'/'.$allFiles)>$timeout)
				  		{
							# remove file #######################
				  			//debug($path.'/'.$allFiles);
							if(is_dir($path.'/'.$allFiles)){
								// remove folders
								$this->folder_remove_recursive($path.'/'.$allFiles);
							}
							else{
								unlink($path.'/'.$allFiles);
							}
				  		}
				  	}
				}
			}
		}

        function file_unzip($file, $dest){
                if(is_file($file)) {
                        $file = $this->file_escapeshellarg($file);
                        $dest1 = $this->file_escapeshellarg($dest);
						exec("zip -F $file");
                        exec("unzip $file -d $dest1");
                        //$this->chmod_R($dest, "0644");
                        $this->chmod_R($dest, 0777);
						return 1;
                }
                return 0;
        }
        
        /*
         * Use python script /includes/unzip.py to unzip files to solve Chinese file name issue. 
         * But the output file name is not in UTF-8 encoding, if want to copy or save to database, you need to do encoding conversion first.
         */
        function file_unzip_ext($file, $dest)
        {
        	global $intranet_root;
            if(is_file($file)) {
                $file = $this->file_escapeshellarg($file);
                $dest = $this->file_escapeshellarg($dest);
				shell_exec("python ".$intranet_root."/includes/unzip.py '$file' '$dest'");
                $this->chmod_R($dest, 0777);
				return 1;
            }
            return 0;
        }

		/**
		 * @param String $file src
		 * @param String $newFileName e.g. xx.zip
		 * @param String $to_dir Dest. Path
		 */
        function file_zip($file, $dest, $to_dir="")
        {
        	
                 $file = $this->file_escapeshellarg($file);
                 $dest = $this->file_escapeshellarg($dest);
                 if($to_dir!="") {
	                 chdir("$to_dir");
                 }
                 $Str = exec("zip -r $dest $file");
        }

        function file_set_modification_time($file, $time=""){
                $time = ($time == "") ? time() : $time;
                return touch($file, $time);
        }

         function file_get_modification_time($file){
                return filemtime($file);
        }

        # Unzip File Using Shell Commands
        function unzipFile($myFilePath, $myDest, $myPassword=""){
                $result_arr = array();

                // copy to temporary file named
                $tmp_file = session_id()."_".time().".zip";
                $tmp_src = "/tmp/".$tmp_file;
                copy($myFilePath, $tmp_src);

                $dirNow = getcwd();
                // change to zip file
                
                chdir($myDest);
                exec("zip -F $myPassword \"{$tmp_src}\" ");
                exec("unzip $myPassword \"{$tmp_src}\" ", $result_arr);
				chdir($dirNow);			# change backt to original directory here
                // avoid failure of file reading
                //debug($myDest);die();
                $this->chmod_R($myDest, 0777);

                // delete temporary file
                unlink($tmp_src);
                //chdir($dirNow);		# commented by henry chow no 20110427

                return $result_arr;
        }

        function chmod_R($path, $filemode)
        {
        	//echo is_dir($path).'/'; exit;
                clearstatcache();
                if (!is_dir($path))
                {
                    return chmod($path, $filemode);
                }

                $dh = opendir($path);
                while ($file = readdir($dh))
                {
                        if ($file != '.' && $file != '..')
                        {
                                $fullpath = $path.'/'.$file;
                                chmod($fullpath, $filemode);
                                if(!is_dir($fullpath))
                                {
                                        if (!chmod($fullpath, $filemode))
                                                return FALSE;
                                } else
                                {
                                        if (!$this->chmod_R($fullpath, $filemode))
                                                return FALSE;
                                }
                        }
                }

                closedir($dh);

                if (chmod($path, $filemode))
                        return TRUE;
                else
                        return FALSE;
        }
        ######################################################################

        function folderlist($location){
                # prevent error if folder does not exist
                clearstatcache();
                if (!file_exists($location))
                {
                        return false;
                }

                $d = dir($location);
                //echo $location.'****<br>';
                while($entry=$d->read()) {
                        $filepath = $location . "/" . $entry;
                        //echo $filepath.'<br>';
                        if (is_dir($filepath) && $entry<>"." && $entry<>".."){
                                if (!$this->no_recursive)
                                {
                                	$this->return_folderlist($filepath);	
                                }
                                $this->rs[sizeof($this->rs)] = $filepath;
                        }
                        else if (is_file($filepath))
                        {
                                $this->rs[sizeof($this->rs)] = $filepath;
                                if ($this->consider_time)
                                {
                                	$this->ftime[$filepath] = filemtime($filepath);                                	
                                }
                                if ($this->consider_hash && !strstr($filepath, ".sh"))
                                {
                                	$this->fhash[$filepath] = md5_file($filepath);                                	
                                }
                        }

                }
                $d->close();
        }

        function return_folderlist($location, $clearRS=false){
                        # added by PeterHo 23-08-2006

                        // Move to return_folder to avoid repeated clearing of result set
                        //$this->rs = array();              # clear rs
                        
                        if ($clearRS) {
                        	$this->rs = array();
                        	$this->ftime = array();
                        }
                        

                        # end
                        $this->folderlist($location);

                        return $this->rs;
        }
        
        function set_get_filetime($consider_time)
        {
			$this->consider_time = $consider_time;
        }
        
        function set_get_filehash($consider_hash)
        {
			$this->consider_hash = $consider_hash;
        }
        
        
        function set_subfolder_recursive($no_recursive)
        {
			$this->no_recursive = $no_recursive;
        }
            
        
        function return_filetime()
        {
        	return $this->ftime;
        }
        
        
        
        function return_filehash()
        {
        	return $this->fhash;
        }
        
        
        function return_files($location){
                # prevent error if folder does not exist
                clearstatcache();
                if (!file_exists($location))
                {
                        return false;
                }

                $d = dir($location);
                while($entry=$d->read()) {
                        $filepath = $location . "/" . $entry;
                        if (is_file($filepath))
                            $filesArray[] = $filepath;

                }
                $d->close();
                
                return $filesArray;
        }

        function return_folder($location)
        {

                        $this->rs = array();              # clear rs

                $folders = array();
                $j = 0;
                $row = $this->return_folderlist($location);


                for($i=0; $i<sizeof($row); $i++){
                        if(is_dir($row[$i])) $folders[$j++] = $row[$i];
                }


                return $folders;
        }

        ######################################################################

        function item_remove($file){
                return (is_file($file)) ? $this->file_remove($file) : $this->folder_remove($file);
        }

        function item_copy($file, $dest){
                return (is_file($file)) ? $this->file_copy($file, $dest) : $this->folder_new($dest);
        }

        ######################################################################
        
        
        
		function createFolder($folderPath) {
				if (file_exists($folderPath))
				{
					chmod($folderPath, 0777);
					return 0;	// existing
				}
		
				$tmpPath = ($this->file_path!="") ? explode("/", str_replace($this->file_path."/", "", $folderPath)) : explode("/", $folderPath);
		
				umask(0);
				for ($i=0; $i<sizeof($tmpPath); $i++)
				{
					$pathNow = $this->buildPath($tmpPath, $i);
					if (!file_exists($pathNow))
					{
						mkdir($pathNow, 0777);
					}
				}
		
				return 1;
			}	
		function buildPath($arr, $ind) {
			$pathNow = $this->file_path;
			for ($i=0; $i<=$ind; $i++)
			{
				$pathNow .= "/".$arr[$i];
			}
			return $pathNow;
		}

        function folder_new($location){
                #umask(0);
                return (file_exists($location)) ? 0 : mkdir($location, 0777, true);
        }
        
        function folder_remove($file){
                return (file_exists($file)) ? rmdir($file) : 0;
        }

        function folder_remove_recursive($location){
                if(is_dir($location)){
                        $row = $this->return_folderlist($location);
                        for($i=0; $i<sizeof($row); $i++){
                                $this->item_remove($row[$i]);
                        }
                        $this->item_remove($location);
                }
                return 1;
        }

        function folder_copy($source, $dest){
                $row = $this->return_folderlist($source);
                $ext = str_replace(substr($source,0,strrpos($source,"/")),'',$source);
                $this->item_copy($source, $dest.$ext);
                //debug_pr($row);
                for($i=sizeof($row)-1; $i>=0; $i--){
                        $file = $row[$i];
                        if($file=="." || $file=="..") continue;
                        $ext = str_replace(substr($source,0,strrpos($source,"/")),'',$file);
                        $this->item_copy($file, $dest.$ext);
                        //echo "-->$file ||| $dest.$ext<br>";
                }
                return 1;
        }

		function folder_content_copy($source, $dest){
                $row = $this->return_folderlist($source);
                for($i=sizeof($row)-1; $i>=0; $i--){
                        $file = $row[$i];
                        if($file=="." || $file=="..") continue;
                        $ext = $this->get_file_basename($file);
                        $dest_filename = $dest."/".$ext;
                        
                        $this->lfs_copy($file, $dest_filename);
                }
                return 1;
        }
        
        function folder_size($location){
                $file_size = 0;
                $file_no = 0;
                $dir_no = 0;
                if(is_dir($location)){
                        $row1 = $this->return_folderlist($location);
                        for($i=0; $i<sizeof($row1); $i++){
                                if(is_file($row1[$i])){

                                        $file_size += filesize($row1[$i]); $file_no++;
                                }else{
                                        $dir_no++;
                                }
                        }
                }else{
                        $file_size += filesize($location); $file_no++;
                }
                $size = array($file_size, $file_no, $dir_no);
                return $size;
        }


        ###########################################################################
        function word2html ($wordfile, $dest, $output_file)
        {
                 global $external_path,$intranet_session_language;
                 $wv_path = $external_path['WV'];
                 $charset = ($intranet_session_language=="gb") ? "gb2312" : "big5";
                 exec("$wv_path/wvHtml --targetdir=$dest --charset=$charset $wordfile $output_file");
        }

        function readWordFileContent ($wordfile)
        {
                 global $intranet_root;
                 $output_file = session_id()."-".time().".html";
                 $dest = "/tmp";
                 $this->word2html($wordfile,$dest,$output_file);
                 $document = $this->file_read("$dest/$output_file");
                 return $this->filterHTMLFile($document);
        }

        function convertWordFileWImage ($wordfile, $dest, $output_file)
        {
                 global $intranet_root, $intranet_httppath;

                 # Convert File
                 $exact_dest = "$intranet_root/$dest";
                 $this->word2html($wordfile,$exact_dest,$output_file);
                 $document = $this->file_read("$exact_dest/$output_file");
                 $filtered = $this->filterHTMLFile($document);

                 # Process images directory append
                 $image_dir = "$intranet_httppath/$dest";
                 $text = str_replace("src=\"","src=\"$image_dir/",$filtered);
                 return $text;

//check file attached (check for more than one file)
/*
$image_tag = '<img';
$image_src = 'src="';
$image_src_e = '"';
while (is_integer($pos_tag = strpos($tmpTxt, $image_tag)) && is_integer($pos_f = strpos($tmpTxt, $image_src))) {
     $pos_src = $pos_f+strlen($image_src);
     $pos_src_e = strpos($tmpTxt, $image_src_e, $pos_src);
     $fileInside[] = substr($tmpTxt, $pos_src, $pos_src_e-$pos_src);
     $tmpTxt = substr($tmpTxt, 0, $pos_tag) . substr($tmpTxt, $pos_src_e+2);
}
*/
        }


        function filterHTMLFile($sTxt)
        {
                 global $im_field;

                 $xStr = "";
                 $separator_s = 'White; ">';
                 $separator_e = '</p></div>';
                 $image_tag = '<img';
                 $image_src = 'src="';
                 $image_src_e = '"';
                 $table_t0 = '<table';
                 $table_tr0 = '<tr';
                 $table_td0 = '<td';
                 $table_t1 = '</table>';
                 $table_tr1 = '</tr>';
                 $table_td1 = '</td>';
                 $op_start_tag = '<ol type="';
                 $op_end_tag = '">';
                 $ol_end_tag = '</ol>';
                 $op_tag_s = '<li value="';
                 $op_tag_e = '"><p><div';
                 $option_letter_start = 65;

                 $pos_e = 0;
                 $pos_pre = 0;
                 $option_start = false;
                 $option_found = false;
                 $op_count = 0;

                 unset($fileInside);
                 while(is_integer($pos_s = strpos($sTxt, $separator_s, $pos_e)))
                 {

                       //capture question contents
                       if (is_integer($pos_e = strpos($sTxt, $separator_e, $pos_s))) {

                       if (!$option_start && $pos_pre!=0 && $pos_e != 0 )
                       {
                                # Check whether is multiple choice answer options
                                $check_str = substr($sTxt,$pos_pre,$pos_e-$pos_pre);
                                //echo "At $pos_pre to $pos_e : ".$check_str."\n<br>\n";
                                $num_op_found = substr_count($check_str,$op_start_tag);
                                $op_start_pos = 0;
                                if ($num_op_found != 0)
                                {
                                    //echo "Found ol for $num_op_found times \n<br>\n";
                                    for ($i=0; $i<$num_op_found; $i++)
                                    {
                                         $op_start_pos = strpos($check_str,$op_start_tag,$op_start_pos+1);
                                    }

                                    $option_start = true;
                                    $end_pos = strpos($check_str,$op_end_tag,$op_start_pos);
                                    $op_type = substr($check_str,$op_start_pos+10,$end_pos-$op_start_pos-10);
                                    $option_letter_start = ord($op_type);
                                    //echo "Got type as ".htmlspecialchars($op_type)." from $op_start_pos to $end_pos\n<br>\n";
                                    /*
                                    $check_str = substr($sTxt, $op_start_pos);
                                    echo "Found start on : $op_start_pos to ".($pos_e-$pos_pre)." : $check_str\n<br>\n";
                                    echo "Left that : ".substr($sTxt,$op_start_pos);
                                    echo "\n<br>\n\n";
                                    */
                                }
                       }

                       if ($option_start)
                       {
                           $check_str = substr($sTxt,$pos_pre,$pos_e-$pos_pre);
                           # Trace </ol>
                           $end_pos = strpos($check_str, $ol_end_tag);
                           if (is_integer($end_pos))
                           {
                               $option_start = false;
                               $option_count = 0;
                           }
                           else
                           {

                                    $op_pos = strpos($sTxt, $op_tag_s, $pos_pre);
                                    if (is_integer($op_pos))
                                    {
                                        $op_end = strpos($sTxt,$op_tag_e, $op_pos);
                                        $option_found = true;
                                    }
                           }
                       }


                           //capture table tags (mind the orders)
                           if (is_integer($pos_td1 = strpos($sTxt, $table_td1, $pos_pre)))
                               if ($pos_td1<=$pos_e && $pos_td1>=$pos_pre)
                                   $xStr .= $table_td1;
                           if (is_integer($pos_tr1 = strpos($sTxt, $table_tr1, $pos_pre)))
                               if ($pos_tr1<=$pos_e && $pos_tr1>=$pos_pre)
                                   $xStr .= $table_tr1;
                           if (is_integer($pos_table1 = strpos($sTxt, $table_t1, $pos_pre)))
                               if ($pos_table1<=$pos_e && $pos_table1>=$pos_pre)
                                   $xStr .= $table_t1;

                           if (is_integer($pos_table0 = strpos($sTxt, $table_t0, $pos_pre)))
                               if ($pos_table0<=$pos_e && $pos_table0>=$pos_pre) {
                                   if (is_integer($pos_table1 = strpos($sTxt, ">", $pos_table0)))
                                       $xStr .= str_replace(' bgcolor="White"', '',substr($sTxt, $pos_table0, $pos_table1-$pos_table0+1));
                               }
                           if (is_integer($pos_tr0 = strpos($sTxt, $table_tr0, $pos_pre)))
                               if ($pos_tr0<=$pos_e && $pos_tr0>=$pos_pre) {
                                   if (is_integer($pos_tr1 = strpos($sTxt, ">", $pos_tr0)))
                                       $xStr .= str_replace(' bgcolor="White"', '',substr($sTxt, $pos_tr0, $pos_tr1-$pos_tr0+1));
                               }
                           if (is_integer($pos_td0 = strpos($sTxt, $table_td0, $pos_pre)))
                               if ($pos_td0<=$pos_e && $pos_td0>=$pos_pre) {
                                   if (is_integer($pos_td1 = strpos($sTxt, ">", $pos_td0)))
                                       $xStr .= str_replace(' bgcolor="White"', '',substr($sTxt, $pos_td0, $pos_td1-$pos_td0+1));
                               }

                           $tmpTxt = substr($sTxt, $pos_s+strlen($separator_s), $pos_e-$pos_s-strlen($separator_s));
                           if (is_integer($pos_tt = strpos($tmpTxt, "\n")))
                               if ($pos_tt==0)
                                   $tmpTxt = substr($tmpTxt, strlen("\n"));

                           if ($option_start && $option_found)
                           {
                               $tmpTxt = chr($option_letter_start+$option_count).". ".$tmpTxt;
//                               echo "Option found : $tmpTxt\n<br>\n";
                               $option_found = false;
                               $option_count++;
                           }
                           $xStr .= $tmpTxt;

                           $pos_pre = $pos_e;
                       }
                 }

                 return $xStr;
        }

        ######################################################################

        function lfs_remove($file){
                return (is_file($file)) ? $this->file_remove($file) : $this->folder_remove_recursive($file);
        }

        function lfs_copy($file, $dest){
                 if ($file=="") return 0;
                if($this->get_file_basename(dirname($file)) == $this->get_file_basename($dest)) return 0;
                                if(is_file($file))
                                        return $this->file_copy($file, $dest);
                                else if(is_dir($file))
                                        return $this->folder_copy($file, $dest);
        }

        function lfs_move($file, $dest){
                if($this->get_file_basename(dirname($file)) == $this->get_file_basename($dest)) return 0;
                $this->lfs_copy($file, $dest);
                $this->lfs_remove($file);
                return 1;
        }

        ######################################################################
        # utility functions

        function validateEmail($email) {
                //return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
                return preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $email);
        }

        function validateURL($url) {
                //return eregi("^((ht|f)tp://)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)$", $url);
                return preg_match("/^((http|ftp|https):\/\/)((([a-z0-9-]+(\.[a-z0-9-]+)*(.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((\/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)$/i", $url);
        }

        function convertURLS($text) {
                //$text = eregi_replace("((ht|f)tp://www\.|www\.)([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})((/|\?)[a-z0-9~#%&\\/'_\+=:\?\.-]*)*)", "http://www.\\3", $text);
                //$text = eregi_replace("((ht|f)tp://)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)", "<a href=\\0 target=_blank>\\0</a>", $text);
        //        $text = preg_replace("/((http|ftp|https):\/\/www\.|www\.)([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})((\/|\?)[a-z0-9~#%&\\/'_\+=:\?\.-]*)*)/i", "\\1://www.\\3", $text);
                $text = preg_replace("/((http|ftp|https):\/\/)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((\/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)/i", "<a href=\"\\0\" target=\"_blank\">\\0</a>", $text);
                return $text;
        }

        function convertMail($text) {
                //$text = eregi_replace("([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))", "<a href='mailto:\\0'>\\0</a>", $text);
                $text = preg_replace("/([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))/i", "<a href=\"mailto:\\0\">\\0</a>", $text);
                return $text;
        }

        function convertAllLinks($text) {
                $text = $this->convertURLS($text);
                $text = $this->convertMail($text);
                return $text;
        }

        ######################################################################

        function getMIMEType($filename)
        {
                 $ext = strtolower($this->file_ext($filename));
                 /*
                 if ($ext == ".gif")
                 {
                     return "image/gif";
                 }
                 else if ($ext == ".txt")
                 {
                      return "text/plain";
                 }
                 else
                 {
                     return "application/octet-stream";
                 }
                 */
                 $mime_type = "application/octet-stream";
                 switch ($ext)
				{
					case ".ogg":
						$mime_type = "audio/ogg";
						break;
					case ".js":
						$mime_type = "text/javascript";
						break;
					case ".css":
						$mime_type = "text/css";
						break;
					case ".txt":
						$mime_type = "text/plain";
						break;
					case ".doc":
						$mime_type = "application/msword";
						break;
					case ".htm":
					case ".xhtml":
					case ".html":
						$mime_type = "text/html";
						break;
					case ".bmp":
						$mime_type = "image/bmp";
						break;
					case ".jpg":
					case ".jpeg":
						$mime_type = "image/jpeg";
						break;
					case ".gif":
						$mime_type = "image/gif";
						break;
					case ".png":
						$mime_type = "image/png";
						break;
					case ".tif":
					case ".tiff":
						$mime_type = "image/tiff";
						break;
					case ".swf":
						$mime_type = "application/x-shockwave-flash";
						break;
					case ".wma":
						$mime_type = "audio/x-ms-wma";
						break;
					case ".mp3":
						$mime_type = "audio/mp3";
						break;
					case ".wav":
						$mime_type = "audio/wav";
						break;
					case ".aif":
						$mime_type = "audio/x-aiff";
						break;
					case ".mpeg":
					case ".mpg":
					case ".mp4":
						$mime_type = "video/mpeg";
						break;
					case ".midi":
					case ".mid":
						$mime_type = "audio/midi";
						break;
					case ".mov":
						$mime_type = "video/quicktime";
						break;
					default:
						$mime_type = "application/octet-stream";
						break;
				}
				return $mime_type;
        }

        function download($file_source, $file_target)
	    {
	        $rh = fopen($file_source, 'rb');
	        $wh = fopen($file_target, 'wb');
	        if ($rh===false || $wh===false) {
			// error reading or opening file
	           return true;
	        }
	        while (!feof($rh)) {
	            if (fwrite($wh, fread($rh, 1024)) === FALSE) {
	                   // 'Download error: Cannot write to file ('.$file_target.')';
	                   return true;
	               }
	        }
	        fclose($rh);
	        fclose($wh);
	        // No error
	        return false;
	    }

    function CHECK_CSV_FILE_FORMAT($ParHeaderArray, $ParDefaultHeaderArray)
    {
      # Check Title Row
      $format_wrong = false;

      for ($i=0; $i<sizeof($ParDefaultHeaderArray); $i++)
      {
        if (trim($ParHeaderArray[$i])!=trim($ParDefaultHeaderArray[$i]))
        {
          $format_wrong = true;
          break;
        }
      }
      return $format_wrong;
    }
    function SafeGuardFileName($ParString,$IsGuardStrip=false) {
    	$specialChars = array("#"," ","'",'"',"&","?");
    	if ($IsGuardStrip) {
    		array_push($specialChars,"/","\\");
    	}
		//$ParString = str_replace(array("#"," ","'",'"',"&","?"),"_",$ParString);
		$ParString = str_replace($specialChars,"_",$ParString);
		return $ParString;
	}
	
	function Copy_Import_File_To_Temp_Folder($ParFolderPath, $ParFile, $ParFileName)
	{
		global $intranet_root;
		
		$folder_prefix = $intranet_root."/file/import_temp/".$ParFolderPath;
		if (!file_exists($folder_prefix))
			$this->folder_new($folder_prefix);
		
		$FileExt = strtoupper($this->file_ext($ParFileName));
		$TargetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$FileExt;
		$TargetFilePath = stripslashes($folder_prefix."/".$TargetFileName);
		$success = $this->lfs_move($ParFile, $TargetFilePath);
		
		return ($success)? $TargetFilePath : false; 
	}
	
	function get_file_basename($file){
		$returnStr = '';
		if($file != ''){
			$tmp = explode('/', $file);
			$returnStr = trim($tmp[count($tmp)-1]);
		}
		return $returnStr;
	}
	/**
	 * Convert the basename of a given path from utf8 to big5-hkscs, and return the new path with the basename converted.
	 */
	function convert_basename_from_utf8_to_big5($path)
	{
		global $g_chinese;
		
		$final_encode = $g_chinese == "gb"?"GBK":"big5-hkscs";
		
		$base_name = $this->get_file_basename($path);
		$dir_name = dirname($path);
		if ($base_name != '.' && iconv('utf8', 'utf8', $base_name) === $base_name) {
			// Basename is of UTF8, do conversion.
			$new_base_name = iconv('utf8', $final_encode, $base_name);
			//$new_base_name = convertUTF8Big5GB($base_name);
			if ($new_base_name != '' && $new_base_name != $base_name) {
				if ($dir_name != '.') {
					return "$dir_name/$new_base_name";
				}
				else {
					return $new_base_name;
				}
			}
		}
		return $path;
	}
	
	/**
	 * Rename a file or all files/directories under a directory from utf8 to big5-hkscs.
	 */
	function rename_filename_from_utf8_to_big5($path)
	{
		// Find files/directories recursively.
		if (is_dir($path)) {
			$handle = opendir($path);
			while (false !== ($resource = readdir($handle))) {
				if (!in_array($resource, array('.', '..'))) {
					$this->rename_filename_from_utf8_to_big5("$path/$resource");
				}
			}
			closedir($handle);
		}
		
		$new_path = $this->convert_basename_from_utf8_to_big5($path);
		if ($new_path != $path) {
			// File/directory name is of UTF8, rename.
			rename($path, $new_path);
		}
	}
    #####################################################################
    #### FCK editor related [Start] (copy from eClass40 and update...)
    #####################################################################
    /**
	 *  Generate the fck flash image file path "Loaction" for saving file upload
	 * 
	 * NOTE : Change this section as required for other platforms
	 * 
	 * @param $module - econtent, forum, assessment, outline, survey, eclassfile
	 * @param $id - id of module
	 * @param $id2 - special module has 2 levels of ID , currently only Forum requires 2nd ID
	 */
	function returnFlashImageInsertPath($module, $id="", $id2=""){
		global $UserID, $cfg;
		
		##Basic structure
		//$FlashImageInsertPath = "files/".classNamingDB($ck_course_id)."/images/".$module."/";
		$FlashImageInsertPath = "file/". $module ."/images/";
		
		## Real path or Temp path 
		$FlashImageInsertPath .= !empty($id)? $cfg['fck_image_prefix'][$module]."_".$id."/": "tmp/U_".$UserID."/";
		
		return $FlashImageInsertPath;
	}
	
	/**
	 *  Generate fck flash image file path for DELETING directory
	 *  
	 *  NOTE : Change this section as required for other platforms 	
	 * */
	function returnRelativePathFlashImageInsertPath($module, $id, $id2="", $is_relative=""){
		global $cfg;
		
		##Basic structure
		//$FlashImageInsertPath = $is_relative."files/".classNamingDB($ck_course_id)."/images/".$module."/";
		$FlashImageInsertPath = "file/". $module ."/images/";
		
		## Module folder path
		$FlashImageInsertPath .= $cfg['fck_image_prefix'][$module]."_".$id;
				
		return $FlashImageInsertPath;
	}
	
	/**
	 * Get the filepaths for COPY file & SAVE to DB for copy fck function
	 * 
	 * NOTE:
	 * Change this function as required for other platforms
	 */
	function get_fck_copy_filepath($id, $rootPath, $folder_path){
		global $cfg, $UserID;
		
		## Add prefix to $id, e.g E_id -econtent , A_id - assessment , S_id - survey, O_id - outline, R_id - resource
		$id = $cfg['fck_image_prefix'][$folder_path]."_".$id;
	
		$rootPath .= ($rootPath[strlen($rootPath)-1] != '/') ? '/' : '';
	
		$aryPath = array();
// 		$aryPath[0] = $rootPath."files/".classNamingDB($ck_course_id)."/images/".$folder_path."/tmp/U_".$ck_user_id;
// 		$aryPath[1] = $rootPath."files/".classNamingDB($ck_course_id)."/images/".$folder_path."/".$id;
		$aryPath[0] = $rootPath."file/". $folder_path ."/images/tmp/U_".$UserID;
		$aryPath[1] = $rootPath."file/". $folder_path ."/images/".$id;
		
		return 	$aryPath;
	}
	
	/**
	 * Function to format string that contains fck flash image upload paths.

	 * 
	 * 1/ Copy files from tmp folder to the correct folder
	 * 2/ Replace contents' tmp_paths with real_paths
	 * 3/ Deletes the corresponding tmp files & directories
	 * 
	 * NOTE:
	 * Timestamp - Gererated when uploading file in flash code
	 * 
	 * for forum: 
	 * 	- The $id param must consist of F_$forum_id/B_$bulletin_id 
	 * 	- No need to append Prefix as it has already been appened via FCK 
	 * 
	 * For differect platforms - IP25, iPortfolio.
	 * 1/ Change prefix settings as prefered 
	 * 2/ Only need to change these paths ($source_Path, $dest_Path, $search_Path, $dest_db_Path) for function to work
	 *  
	 * @return formated string 
	 **/
	function copy_fck_flash_image_upload($id, $content, $url_relative_path="", $folder_path){
		$aryCopied = array();
		
		if(!is_array($this->aryRemoveFileList))
			$this->aryRemoveFileList = array();
			
		##########################################################
		##########################################################
		## Change this section as required for other platforms	
		# For copying files (Relative Path)
		$aryCopyPath = $this->get_fck_copy_filepath($id, $url_relative_path, $folder_path);		
		$source_Path = $aryCopyPath[0];
		$dest_Path = $aryCopyPath[1];
		
		# For storing in DB (later can access imgs in view/edit pages)
		$IP_http_root = "/";
		$aryDBPath = $this->get_fck_copy_filepath($id, $IP_http_root, $folder_path);
		$search_Path = $aryDBPath[0];
		$dest_db_Path = $aryDBPath[1];
		##########################################################
		##########################################################
		
		# Check if flash path exists in content
		$hasFlashImage = strpos($content, $search_Path);

		if($hasFlashImage !== false){
			
			$aryData = explode($search_Path, $content);
			
			# Skip first element (1st element always html plain txt)
			for($x = 1; $x<count($aryData); $x++){
				
				## Explode img path to get IMAGE NAME & Timestamp				
				$aryInfo = explode("/", $aryData[$x]);	
				$aryImage = explode('"',$aryInfo[2]);
				## The image name may have been encoded. (For copying files)
				$real_image_name = rawurldecode($aryImage[0]);
				$timestamp = $aryInfo[1];
											
				## Copy image to correct folder - /files/c_xx/images/eContent/notes_id/timestamp
				$source = $source_Path."/".$timestamp;
						
				if(file_exists($source."/".$real_image_name)){
					$final_dest = $dest_Path."/".$timestamp;
					
					## Create directory if does not exists
					if (!file_exists($final_dest) && !is_dir($final_dest)) 
						mkdir($final_dest, 0777, true);
									
					## Copy from temp folder to the Real one
					copy($source."/".$real_image_name, $final_dest."/".$real_image_name);				
					
					## Store list of temp files for removing later
					if(!in_array($source, $this->aryRemoveFileList))
						array_push($this->aryRemoveFileList, $source);					
				}			
			}		
			
			
			## Delete all files in directory specified		
			if($this->aryRemoveFileList != array()){
				foreach($this->aryRemoveFileList as $r=>$rmDir){
					if(!empty($rmDir) && is_dir($rmDir)){
						$this->deleteDirectory($rmDir);
						unset($this->aryRemoveFileList[$r]);
					}
				}
			}				
					
					/*
					echo "<BR >Search path : ".$search_Path;
					echo "<BR >Dest path : ".$dest_db_Path;
					echo "<BR >Content : ".$content;
					echo "<BR >Content NEW :".htmlspecialchars(str_replace($search_Path,$dest_db_Path, $content ));
					*/
			## Replace fck TMP flash image path with -> Real Path , for storing in DB
			return str_replace($search_Path,$dest_db_Path, $content );

		}else{
			## Content does not contain any fck flash upload paths 
			return $content;
		}
	}
	
	/**
	 * Delete directory & files recursively
	 * NOTE:
	 * 	When calling this func need to be careful that you don't delete more that what is expected
	 *  for $dir -> path+Module_ID , check that Module_ID is not empty 
	 */
	function deleteDirectory($dir) {
        if (!file_exists($dir)) return true;
        if (!is_dir($dir)) return unlink($dir);
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') continue;
            if (!$this->deleteDirectory($dir.DIRECTORY_SEPARATOR.$item)) return false;
        }
        return rmdir($dir);
    }
    
    /**
     * Clear all FCK flash image upload files in tmp folders 
     * NOTE : Called in login page (EG ec40 - portal pg), does not need to be called in every page 
     */
    function deleteAllFckImageTmpFiles(){
    	global $UserID, $cfg;
    	
//     	$classname = classNamingDB($ck_course_id);
    	$aryDeleteList = array();
    	    	
    	foreach($cfg['fck_image'] as $i=>$module){ 
    		#######################################################
    		## Change this section as required for other platforms    	
    		$FlashImageInsertPath = "../files/".$module."/images/tmp/U_".$UserID;
    		#######################################################
    		
    		if(is_dir($FlashImageInsertPath)){
    		
    			$dh  = opendir($FlashImageInsertPath);
				while (false !== ($filename = readdir($dh))) {
					
					if($filename != "." && $filename != ".." && !empty($filename)){
					    
					    ## Format Filename = 2010-02-24_15-39-09 --> 2010-02-24 15:39:09					   
					   $aryFilename = explode("_", $filename);
					   $time = str_replace("-", ":", $aryFilename[1]);
					   $new_filename = $aryFilename[0]." ".$time;
					   
					    ## EC40: Uncomment to check precisely how old the files is.
					    //$check_timestamp = time_period($new_filename);
					    //echo check_timestamp."<BR />";
					      
					    ## After 24+ hrs, files in tmp/U_xx/Timestamp is considered obsolete
					    $canDelete = $this->canDelete($new_filename, 24);
					   
					    
					    ## Directory is obsolete (Created 24+ hrs ago)
					    if($canDelete){
					    	if(is_dir($FlashImageInsertPath."/".$filename)){
					    		## Detelete directory + files in the directory					    		 
					    		$this->deleteDirectory($FlashImageInsertPath."/".$filename);
					    		array_push($aryDeleteList, $FlashImageInsertPath."/".$filename);
					    	}
					    }
					}
				}
    		}    	
    	}
    	##Uncomment for debugging
    	//debug_r($aryDeleteList);
    	
    }
    function copy_fck_flash_image_upload_to_new_id_loc($src_id, $dest_id, $content, $url_relative_path, $folder_path, $src_id2='', $dest_id2=''){
		$IP_http_root = "/";
		# For Copying files (Relative Path)
		/* Source Path */
		$arySrcCopyPath = $this->get_fck_copy_filepath($src_id, $url_relative_path, $folder_path, $src_id2);
		$source_Path = $arySrcCopyPath[1];
		
		/* Destination Path */
		$aryDestCopyPath = $this->get_fck_copy_filepath($dest_id, $url_relative_path, $folder_path, $dest_id2);
		$dest_Path = $aryDestCopyPath[1];
		
		# For Storing in DB (later can access imgs in view/edit pages)
		/* Source Path */
		$arySrcDBPath = $this->get_fck_copy_filepath($src_id, $IP_http_root, $folder_path, $src_id2);
		$search_Path = $arySrcDBPath[1];
		
		/* Destination Path */
		$arySrcDBPath = $this->get_fck_copy_filepath($dest_id, $IP_http_root, $folder_path, $dest_id2);
		$dest_db_Path = $arySrcDBPath[1];
		
		return $this->copy_fck_flash_image_upload_from_src_to_dest($content, $folder_path, $source_Path, $dest_Path, $search_Path, $dest_db_Path, false);
	}
	/*
	 * function copy_fck_flash_image_upload_from_src_to_dest() is separated from function copy_fck_flash_image_upload() 
	 * */
	function copy_fck_flash_image_upload_from_src_to_dest($content, $folder_path, $source_Path, $dest_Path, $search_Path, $dest_db_Path, $with_remove=false){
		global $cfg;
		
		if(!is_array($this->aryRemoveFileList))
			$this->aryRemoveFileList = array();
		
		
		# Check if flash path exists in content
		$hasFlashImage = strpos($content, $search_Path);

		if($hasFlashImage !== false){
			
			$aryData = explode($search_Path, $content);
			
			# Skip first element (1st element always html plain txt)
			for($x = 1; $x<count($aryData); $x++){
				
				## Explode img path to get IMAGE NAME & Timestamp				
				$aryInfo = explode("/", $aryData[$x]);			
				$aryImage = explode('"',($aryInfo[2]));
				
				## The image name may have been encoded. (For copying files)
				$real_image_name = rawurldecode($aryImage[0]);
				
				$timestamp = $aryInfo[1];
												
				## Copy image to correct folder - /files/c_xx/images/eContent/notes_id/timestamp
				 $source = $source_Path."/".$timestamp;
				if(file_exists($source."/".$real_image_name)){
					
					$final_dest = $dest_Path."/".$timestamp;
					
					if ($timestamp=='banner' && $folder_path==$cfg['fck_image']['iPortfolio']){//remove old banner images
					    $this->deleteDirectory($final_dest);
					}
											
					## Create directory if does not exists
					if (!file_exists($final_dest) && !is_dir($final_dest)) 
						mkdir($final_dest, 0777, true);
									
					## Copy from temp folder to the Real one
					copy($source."/".$real_image_name, $final_dest."/".$real_image_name);				
					
					## Store list of temp files for removing later
					if($with_remove && !in_array($source, $this->aryRemoveFileList))
						array_push($this->aryRemoveFileList, $source);					
				}
			}		
			
			## Delete all files in directory specified		
			if($with_remove && $this->aryRemoveFileList != array()){
				foreach($this->aryRemoveFileList as $r=>$rmDir){
					if(!empty($rmDir) && is_dir($rmDir)){
						$this->deleteDirectory($rmDir);
						unset($this->aryRemoveFileList[$r]);
					}
				}
			}				

			## Replace fck TMP flash image path with -> Real Path , for storing in DB
			return str_replace($search_Path, $dest_db_Path, $content);
		}
		else{
			
			## Content does not contain any fck flash upload paths 
			return $content;
		}
	}	
    #####################################################################
    #### FCK editor related [End]
    #####################################################################


}
?>