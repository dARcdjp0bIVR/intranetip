<?php
#  Editing by 
/*
 *	Date : 2018-03-29 (Bill)    [2017-1102-1135-27054]
 *          modified Get_Subject_RubricsItem_Mapping(), to fix PHP 5.4 problem of trim()
 */

class libreportcardrubrics_rubrics extends libreportcardrubrics {
	
	/**
	 * Constructor
	 */
	function libreportcardrubrics_rubrics() {
		parent:: libreportcardrubrics();
	}
	
	function Get_Rubics_Info($RubricsID = '')
	{
		if(trim($RubricsID) != '')
			$cond_RubricsID = " AND RubricsID = '$RubricsID' ";

		$TableName = $this->Get_Table_Name("RC_RUBRICS");
		$sql = "
			SELECT 
				*
			FROM 
				$TableName
			WHERE
				RecordStatus = 1
				$cond_RubricsID
		";
		
		$result = $this->returnArray($sql);
		return $result;
	}

	function Get_Rubrics_Subject_Mapping($YearID = '', $SubjectID = '')
	{
		if(trim($YearID) != '')
			$cond_YearID = " AND YearID = '$YearID' ";
		if(trim($SubjectID) != '')
			$cond_SubjectID = " AND SubjectID = '$SubjectID' ";
			
		$TableName = $this->Get_Table_Name("RC_RUBRICS_SUBJECT_MAPPING");
		
		$sql = "
			SELECT 
				*
			FROM 
				$TableName
			WHERE
				1 
				$cond_YearID
				$cond_SubjectID
		";
		
		$result = $this->returnArray($sql);
		
		foreach($result as $rec)
		{
			$returnArray[$rec["YearID"]][$rec["SubjectID"]] =  $rec["RubricsID"];
		}
		
		return $returnArray;
	}

	function Get_Subject_RubricsItem_Mapping($YearID = '', $SubjectID = '')
	{
		if(trim($YearID) != '')
			$cond_YearID = " AND rsm.YearID = '$YearID' ";
		//if(trim($SubjectID) != '')
		if(!is_array($SubjectID) && trim($SubjectID) != '' || is_array($SubjectID)) {
			$cond_SubjectID = " AND rsm.SubjectID IN (".implode(",",(array)$SubjectID).") ";
		}
			
		$RC_RUBRICS_SUBJECT_MAPPING = $this->Get_Table_Name("RC_RUBRICS_SUBJECT_MAPPING");
		$RC_RUBRICS_ITEM = $this->Get_Table_Name("RC_RUBRICS_ITEM");
		
		$sql = "
			SELECT 
				rsm.YearID,
				rsm.SubjectID,
				ri.*
			FROM 
				$RC_RUBRICS_SUBJECT_MAPPING rsm
				LEFT JOIN $RC_RUBRICS_ITEM ri ON ri.RubricsID = rsm.RubricsID
			WHERE
				1 
				$cond_YearID
				$cond_SubjectID
			ORDER BY
				rsm.RubricsID ASC, ri.Level DESC
		";
		
		$result = $this->returnArray($sql);
		
		foreach($result as $rec)
		{
			$returnArray[$rec["YearID"]][$rec["SubjectID"]][$rec["RubricsItemID"]] =  $rec;
		}
		
		return $returnArray;
	}
		
	function Get_Rubics_Item_Info($RubricsID = '', $RubricsItemID = '')
	{
		if(trim($RubricsID) != '')
			$cond_RubricsID = " AND RubricsID = '$RubricsID' ";
		if(!empty($RubricsItemID))
			$cond_RubricsItemID = " AND RubricsItemID IN ('".implode("','",(array)$RubricsItemID)."') ";
		
		$TableName = $this->Get_Table_Name("RC_RUBRICS_ITEM");
		// 2013-0124-1525-48071 - order by RubricsID ASC, Level DESC is wrong
		$sql = "
			SELECT 
				*
			FROM 
				$TableName
			WHERE
				RecordStatus = 1
				$cond_RubricsID
				$cond_RubricsItemID
			ORDER BY
				/* RubricsID ASC, */
				/* Level DESC */
				Level ASC
		";
		
		$result = $this->returnArray($sql);
		return $result;
	}	
	
	function Is_Rubrics_Code_Valid($RubricsCode, $exceptRubricsID='')
	{
		if(trim($RubricsCode) == '') return false;
		
		$RubricsCode = $this->Get_Safe_Sql_Query(trim($RubricsCode));
		
		if(trim($exceptRubricsID) != '')
			$cond_RubricsID = " AND RubricsID <> '$exceptRubricsID' ";
		
		$TableName = $this->Get_Table_Name("RC_RUBRICS");
		$sql = "
			SELECT 
				RubricsID
			FROM
				$TableName
			WHERE
				RecordStatus = 1
				AND RubricsCode = '$RubricsCode'
				$cond_RubricsID
		";
		
		$result = $this->returnVector($sql);
		return count($result)==0?true:false;
	}
	
	function InsertRubrics($RubricsDataArr)
	{
		if(!is_array($RubricsDataArr))
			return false;
			
		foreach((array)$RubricsDataArr as $key => $data)
		{
			$fieldArr[] = $key;
			if(trim($data)=='')
				$valueArr[] = "NULL";
			else
				$valueArr[] = "'".$this->Get_Safe_Sql_Query($data)."'";
		}
		
		# Record status
		$fieldArr[] = 'RecordStatus';
		$valueArr[] = '1';
		# DateInput
		$fieldArr[] = 'DateInput';
		$valueArr[] = 'now()';
		# InputBy
		$fieldArr[] = 'InputBy';
		$valueArr[] = "'".$_SESSION['UserID']."'";
		# DateModified
		$fieldArr[] = 'DateModified';
		$valueArr[] = 'now()';
		# LastModifiedBy
		$fieldArr[] = 'LastModifiedBy';
		$valueArr[] = "'".$_SESSION['UserID']."'";
		
		$fields = implode(",",$fieldArr);
		$values = implode(",",$valueArr);
		
		$TableName = $this->Get_Table_Name('RC_RUBRICS');
		$sql = "
			INSERT INTO
				$TableName
				($fields)
			VALUES
				($values)
		";
		
		$result = $this->db_db_query($sql);
		
		return $result?mysql_insert_id():false;
	}

	function InsertRubricsItem($RubricsID, $RubricsItemData)
	{	
		$fields = array("LevelNameEn","LevelNameCh","DescriptionEn","DescriptionCh","Level");
		foreach((array) $RubricsItemData as $key => $DataArr)
		{
			$values = array();
			foreach((array)$fields as $FieldName)
			{
				if($DataArr[$FieldName])
					$values[] = "'".$this->Get_Safe_Sql_Query($DataArr[$FieldName])."'";
				else
					$values[] = " NULL ";
			}
			
			# RubricsID
			$values[] = "'$RubricsID'";

			# DateInput
			$values[] = "NOW()";
			
			#InputBy
			$values[] = "'".$_SESSION['UserID']."'";
			
			#DateModified
			$values[] = "NOW()";
			 	
			#LastModifiedBy
			$values[] = "'".$_SESSION['UserID']."'";

			$valuesStr[] = "(".implode(",",$values).")";
			
		}
		
		$valueSql = implode(",",$valuesStr);
		
		# RubricsID
		$fields[] = "RubricsID";

		# DateInput
		$fields[] = "DateInput";
		
		#InputBy
		$fields[] = "InputBy";
		
		#DateModified
		$fields[] = "DateModified";
		
		#LastModifiedBy
		$fields[] = "LastModifiedBy";
		
		$fieldSql = implode(",",$fields);
		
		$TableName = $this->Get_Table_Name("RC_RUBRICS_ITEM");
		$sql = "
			INSERT INTO
				$TableName
				($fieldSql)
			VALUES
				$valueSql;
		";
		
		$result = $this->db_db_query($sql);
		
		return $result?true:false;
	}
	
	function UpdateRubrics($RubricsID, $RubricsDataArr)
	{
		if(!is_array($RubricsDataArr))
			return false;
			
		foreach((array)$RubricsDataArr as $key => $data)
		{
			if(trim($data)=='')
				$value = "NULL";
			else 
				$value = "'".$this->Get_Safe_Sql_Query($data)."'";
				
			$fieldvalueArr[] = " $key = $value ";
		}
		# DateModified
		$fieldvalueArr[] = " DateModified = NOW() ";
		
		# LastModifiedBy
		$fieldvalueArr[] = " LastModifiedBy = '".$_SESSION['UserID']."' ";
		
		$fieldvalueSql = implode(",",$fieldvalueArr);
		
		$TableName = $this->Get_Table_Name('RC_RUBRICS');
		$sql = "
			UPDATE
				$TableName
			SET
				$fieldvalueSql
			WHERE
				RubricsID = '$RubricsID'
		";
		
		$result = $this->db_db_query($sql);
		
		return $result?true:false;
	}
	
	function UpdateRubricsItem($RubricsItemID,$RubricsItemInfo)
	{
		foreach((array)$RubricsItemInfo as $key => $data)
		{
			if(trim($data)=='')
				$value = "NULL";
			else 
				$value = "'".$this->Get_Safe_Sql_Query($data)."'";
				
			$fieldvalueArr[] = " $key = $value ";
		}
		$fieldvalueSql = implode(",",$fieldvalueArr);
		
		$TableName = $this->Get_Table_Name('RC_RUBRICS_ITEM');
		$sql = "
			UPDATE
				$TableName
			SET
				$fieldvalueSql
			WHERE
				RubricsItemID = '$RubricsItemID'
		";
		
		$result = $this->db_db_query($sql);
		
		return $result?true:false;
		
	}
	
	function DeleteRubricsItem($DisableRubricsItemIDList)
	{
		$DisabeListSql = implode(",",(array)$DisableRubricsItemIDList);
		
		$TableName = $this->Get_Table_Name('RC_RUBRICS_ITEM');
		$sql = "
			UPDATE
				$TableName
			SET
				RecordStatus = 0
			WHERE
				RubricsItemID IN ($DisabeListSql)
		";
		
		$result = $this->db_db_query($sql);
		
		return $result?true:false;
			
	}
	
	function DeleteRubrics($RubricsID)
	{
		
		$this->Start_Trans();
		
		# delete rubrics
		$TableName = $this->Get_Table_Name('RC_RUBRICS');
		$sql = "
			UPDATE
				$TableName
			SET
				RecordStatus = 0
			WHERE
				RubricsID = '$RubricsID'
		";
		
		$success[] = $this->db_db_query($sql);
		
		# delete rubrics items
		$TableName = $this->Get_Table_Name('RC_RUBRICS_ITEM');
		$sql = "
			UPDATE
				$TableName
			SET
				RecordStatus = 0
			WHERE
				RubricsID = '$RubricsID'
		";
		
		$success[] = $this->db_db_query($sql);
		
		if(in_array(false,$success))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
		
	}
	
	function GetUsedRubricItemList()
	{
		$TableName = $this->Get_Table_Name('RC_MARKSHEET_SCORE');
		
		$sql = "
			SELECT
				DISTINCT RubricsItemID 
			FROM 
				$TableName
		";
		
		$result = $this->returnVector($sql);
		
		return $result;
		
	}

	function GetUsedRubricList()
	{
		$TableName = $this->Get_Table_Name('RC_MARKSHEET_SCORE');
		
		$sql = "
			SELECT
				DISTINCT RubricsID 
			FROM 
				$TableName
		";
		
		$result = $this->returnVector($sql);
		
		return $result;
		
	}

	function UpdateSubjectRubricsMap($YearID, $SubjectID, $RubricsID)
	{
		$TableName = $this->Get_Table_Name('RC_RUBRICS_SUBJECT_MAPPING');
		
		$sql = "
			UPDATE 
				$TableName
			SET
				RubricsID = '$RubricsID',
				DateModified = NOW(),
				LastModifiedBy = '".$_SESSION["UserID"]."'
			WHERE
				SubjectID = '$SubjectID'
				AND YearID = '$YearID'
		";
		
		$result = $this->db_db_query($sql);	
		
		return $result?true:false;
	}
	
	function InsertSubjectRubricsMap($YearID, $SubjectID, $RubricsID)
	{
		$TableName = $this->Get_Table_Name('RC_RUBRICS_SUBJECT_MAPPING');
		
		$sql = "
			INSERT INTO  
				$TableName
				(YearID, SubjectID, RubricsID, DateModified, LastModifiedBy)
			VALUES
				('$YearID', '$SubjectID', '$RubricsID', NOW(), NOW())
		";
		
		$result = $this->db_db_query($sql);	
		
		return $result?true:false;
	}
	
	function Get_Subjects_Max_Rubrics_Level($YearID, $SubjectIDArr=array())
	{
		$YearID = 0; // temporarily assign YearID=0, as no Form based setting for now
		if(count($SubjectIDArr) > 0)
			$cond_SubjectID = " AND rsm.SubjectID IN (".implode(",",$SubjectIDArr).") ";
		
		$RC_RUBRICS_SUBJECT_MAPPING = $this->Get_Table_Name('RC_RUBRICS_SUBJECT_MAPPING');
		$RC_RUBRICS_ITEM = $this->Get_Table_Name('RC_RUBRICS_ITEM');
		$sql = "
			SELECT 
				MAX(ri.Level) 
			FROM 
				$RC_RUBRICS_ITEM ri 
				LEFT JOIN $RC_RUBRICS_SUBJECT_MAPPING rsm ON ri.RubricsID = rsm.RubricsID 
			WHERE
				1
				AND rsm.YearID = '$YearID'
				$cond_SubjectID
			GROUP BY
				rsm.YearID
	 	";
	 	
	 	$MaxLevel = $this->returnVector($sql);
	 	
		return $MaxLevel[0];
	}
}

?>