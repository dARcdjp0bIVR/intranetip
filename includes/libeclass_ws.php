<?php
// using:

/*
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!                                                                                    !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!   Modification of this file may affect different modules.                          !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!   Please check carefully and consult team leaders after modifying it.              !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!                                                                                    !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */
### 		!!!!! Note: Required to deploy with libnotice.php if upload this file to client BEFORE ip.2.5.10.10.1	!!!!!

// ##################################################### Change Log ######################################################
//
// Date   : 2020-11-16 (Ray)
// Detail : Added WS_ApiKeyVersion
//
// Date   : 2020-11-11 (Ray)
// Detail : Modified WS_updatePaymentCallbackFromCentralServer, add PAYMENT_TNG_TRANSACTION_AMOUNT_DETAIL
//
// Date   : 2020-10-28 (Ray)
// Detail : Modified WS_updatePaymentCallbackFromCentralServer, add quota
//
// Date   : 2020-10-07 (Crystal)
// Detail : Added WS_DeleteScheduledPushMessageFromCourse function, Modified WS_SendPushMessageFromCourse() for schedule push message from pl2
//
// Date   : 2020-09-22 (Ray)
// Detail : Modified WS_SubmitBodyTemperatureRecord, WS_GetStudentBodyTemperatureRecords
//
// Date   : 2020-09-07 (Bill)   [2020-0604-1821-16170]
// Detail : Modified WS_GeteNoticeSForApp(), to check access right for different notice type
//
// Date   : 2020-08-20 (Ray)
// Detail : Modified WS_GetPaymentItemMerchantInfo, add ModuleName, ModuleTransactionID, WS_updatePaymentCallbackFromCentralServer add ePOS
//
// Date   : 2020-08-11 (Ray)
// Detail : Modified WS_updatePaymentCallbackFromCentralServer, add wechat, add WS_ForgetPasswordVersion, WS_ForgetPasswordV2
//
// Date   : 2020-08-07 (Bill)     [2020-0806-1120-10235]
// Detail : modify GetStudentInfo_eClassApp(), html_entity_decode() - set charset 'UTF-8'
// Deploy : ip.2.5.11.8.1
//
// Date   : 2020-07-14 (Ray)
// Detail : Modified WS_updatePaymentCallbackFromCentralServer, add StudentID, insert PAYMENT_TNG_TRANSACTION when PaymentID not found
//
// Date   : 2020-07-10 (Henry)
// Detail : Modified GetStudentInfo_eClassApp(), use english name as chinese name if chinese name is empty
//
// Date   : 2020-07-09 (Ray)
// Detail : Added WS_SubmitBodyTemperatureRecord, WS_GetStudentBodyTemperatureRecords
//
// Date   : 2020-07-08 (Ray)
// Detail : Added WS_GetSchoolDayCount, modified WS_GetPaymentForApp multi payment gateway
//
// Date   : 2020-07-02 (Ray)
// Detail : Modified WS_GetPaymentItemMerchantInfo ServiceProvider
//
// Date   : 2020-06-11 (Ray)
// Detila : Modified WS_GetStudentTakeLeaveRecords
//
// Date   : 2020-06-11 (Bill)
// Detila : Modified WS_GetHomeworkListForApp(), to update homework default sorting to StartDate desc > DueDate
//
// Date	  : 2020-06-09 (Ray)
// Detail : Modified WS_GetStudentTakeLeaveSettings add LeaveType, Added WS_GetSessionNumberForDate, WS_SubmitApplyLeaveRecord add TW
//
// Date   : 2020-05-06 (Ray)
// Detail : Modified WS_GetPaymentForApp add CreditType
//
// Date   : 2020-05-06 (Henry)
// Detail : Modified WS_GetDigitalChannelPhotoList()
//
// Date   : 2020-05-05 (Tommy)
// Detail : modifided WS_GetDigitalChannelPhotoList(), change isAlbumReadable() to isAlbumsReadable() for not loading too many SQL
//
// Date   : 2020-04-15 (Bill)   [2020-0310-1051-05235]
// Detail : Modified WS_GeteNoticeSForApp(), to handle url of special notice ($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'])
//
// Date	  : 2020-03-26 (Philips)
// Detail : Modified WS_GetAnnouncementForApp(), use flag $sys_custom['eClassApp']['IncludePublicGroupAnnouncement'] to include group announcement into public announcement
//			For TargetGroup = 'S' only
//
// Date   : 2020-03-26 (Thomas)
// Detail : Modified function WS_SendPushMessageFromCourse() and added function SendPushMessageFromPowerLesson2() to 
//          send push message to PowerLesson 2 target students
//
// Date	  : 2020-03-19 (Philips)
// Detail : Modified WS_GetAnnouncementForApp(), add portal announcement order into custom comparison
//			For TargetGroup = 'S' only
//
// Date   : 2020-03-03 (Thomas)
// Detail : Modified function SendPushMessageFromEcontent(), SendPushMessageFromAssessment() to fixed the problem of fail to get
//          $cfg from eclass40/system/settings/config.inc.php
// Deploy : ip.2.5.11.3.1
//
// Date   : 2020-02-18 (Thomas)
// Detail : Added function WS_SendPushMessageFromCourse(), SendPushMessageFromEcontent(), SendPushMessageFromAssessment()
//          GetCourseName(), GetUserName() and GetIntranetUserIdsOfTargetUsersOfCourseFunction() to send push message
//          to eContent and Assessment target students
// Deploy : ip.2.5.11.3.1
//
// Date   : 2020-01-03 (Ray)
// Detail : Modified WS_GetAttendanceDetailsForApp(), add $sys_custom['StudentAttendance']['KIS_AttendanceUseClassGroup']
// Deploy :
//
// Date   : 2019-12-04 (Bill)   [2019-1128-1046-36235]
// Detail : Modified WS_GetAnnouncementForApp(), improve announcement sorting logic - onTop > AnnouncementDate > AnnouncementID
// Deploy : ip.2.5.10.12.1
//
// Date   : 2019-12-03 (Thomas)
// Detail : Modified WS_GetCourseUsers(), fixed the problem of fail to retrieve user's photo
// Deploy : ip.2.5.10.12.1
//
// Date   : 2019-11-21 (Bill)   [2019-1120-1536-38066]
// Detail : Modified WS_GetAnnouncementForApp(), fixed school / group announcement ordering problem
// Deploy : ip.2.5.10.12.1
//
// Date	  : 2019-11-14 (Carlos)
// Detail :	Modified WS_GetAttendanceDetailsForApp(), display leave school time even leave status is null(due to some places miss update leave status). 
// Deploy : ip.2.5.10.12.1
//
// Date	  : 2019-10-29 (Carlos)
// Detail : Modified WS_updatePaymentCallbackFromCentralServer(), added optional parameter $parDateInput to pass in the real transaction time from payment server.
// Deploy : ip.2.5.10.12.1
//
// Date	  : 2019-10-21 (Carlos)
// Detail : Modified WS_GetPaymentItemMerchantInfo(), cater TNG do not return error if payment item does not have merchant account assigned, let it just use the first merchant account on central server because initially TNG only support one merchant account.
// Deploy : ip.2.5.10.10.1
//
// Date	  : 2019-10-18 (Carlos)
// Detail : Modified WS_GetAttendanceDetailsForApp(), fix checking of attendance mode, status and display time. 
// Deploy : ip.2.5.10.10.1
//
// Date	  : 2019-10-17 (Tommy)
// Detail : change sorting in WS_GetAnnouncementForApp() to get onTop announce
// Deploy : ip.2.5.10.10.1
//
// Date	  : 2019-10-15 (Tommy) 
// Detail : Modified WS_GetAnnouncementForApp() -> add "a.onTop DESC" to orderBy
// Deploy : ip.2.5.10.10.1
//
// Date	  : 2019-09-11 (Bill) [2019-0906-1721-48066]
// Detail : Modified WS_GetAnnouncement(), WS_GetAnnouncementForApp(), to fix php error due to call to undefined functions if get class teacher announcement
// Deploy : ip.2.5.10.10.1
//
// Date	  : 2019-09-09 (Bill) [2019-0905-1012-28207]
// Detail : Modified WS_GeteNoticeSForApp(), to allow eNotice access group members to view all notice reply
// Deploy : ip.2.5.10.10.1
//
// Date	  : 2019-09-05 (Carlos)
// Detail : Added syncPaymentDataToSchoolFromCentralServer($requestData), called in WS_GetNoticeForApp($TargetUserID, $CurrentUserID) and WS_GetPaymentForApp($TargetUserID) if using eWallet direct pay.
// Deploy : ip.2.5.10.10.1
//
// Date   : 2019-08-08 (Thomas)
// Detail : Modified WS_eClassAppValidate() to stop PowerLesson App Connection Service only if $sys_custom['power_lesson_extend'] is not set
// Deploy : ip.2.5.10.10.1
//
// Date	  : 2019-08-07 (Carlos)
// Detail : Modified WS_updatePaymentCallbackFromCentralServer() to cater more eWallet types FPS and Tap & GO.
// Deploy : ip.2.5.10.10.1
//
// Date   : 2019-08-02 (Thomas)
// Detail : Modified WS_eClassAppValidate() to stop PowerLesson App Connection Service
// Deploy : ip.2.5.10.10.1
//
// Date	  : 2019-08-01 (Carlos)
// Detail : Modified WS_GetPaymentForApp($TargetUserID), added [EnableTopUpInApp] in return result.
// Deploy : ip.2.5.10.10.1
// 
// Date   : 2019-05-14 (Bill)
// Detail : Modified GetStudentInfo_Common(), WS_GetPayment(), WS_GetStudentAttendanceInfo(), to fix SQL Injection
// Deploy : ip.2.5.10.6.1
//
// Date   : 2019-05-09 (Carlos)
// Detail : Added iCalendar api methods WS_iCalendar* . Some methods are still in progress. 
//
// Date   : 2019-05-02 (Paul)
// Detail : Apply OsCommandSafe on parameters of every directly executed command
// Deploy : ip.2.5.10.5.1
//
// Date   : 2019-05-02 (Tiffany)
// Detail : Modified WS_GetSchoolEventForApp() to let app with english language can show chinese value if the event don't have english one
// Deploy : ip.2.5.10.4.1
//
// Date   : 2019-04-08 (Cameron)
// Detail : retrieve all active user in WS_GetCommonChooseUsers() if TargetID and GroupID is empty
//
// Date   : 2019-04-02 (Cameron)
// Detail : retrieve EnglishName, ChineseName, ThumbnailUrl in WS_GetCommonChooseUsers()
//
// Date   : 2019-03-15 (Tiffany)
// Detail : Modified DigitalChannelPhotoLike() for don't know who use the wrong sql get "IsFavorite" cause can't get data for 172 site.
//
// Date   : 2019-03-07 (Cameron)
// Detail : add function WS_DigitalChannelDeleteAlbum() and WS_DigitalChannelDeletePhotosForAlbum()
//
// Date   : 2019-02-15 (Tiffany)
// Detail : Modified GetStudentInfo_eClassApp() return ClassAttendanceType, if AttendanceType not exist, use setting attendanceMode.
//
// Date   : 2019-02-11 (Ivan) [ip.2.5.10.2.1]
// Detail : Modified WS_GetPaymentForApp() to hide past payment item if using Alipay
//
// Date   : 2019-02-04 (Cameron)
// Detail : retrieve AppIsAlbumEditable and CategoryAry[]['IsUserEditable'] in WS_GetDigitalChannelPhotoList()
//
// Date	  : 2019-01-25 (Carlos)
// Detail : Modified WS_updatePaymentCallbackFromCentralServer() to support E-Wallet (Alipay, TNG) top-up. (Synced from EJ)
//
// Date   : 2019-01-23 (Cameron)
// Detail : add function WS_DigitalChannelUploadPhotoDetails(), WS_DigitalChannelPhotoView()
//
// Date   : 2019-01-21 (Cameron)
// Detail : add function getClubAndEventInChargeUser(), getClassUserOfTeacherClass(), WS_DigitalChannelGetAlbumDetails()
//
// Date   : 2019-01-18 (Cameron)
// Detail : add function WS_GetCommonChooseGroups() and WS_GetCommonChooseUsers() [!!!!! hasn't support DHL yet !!!!!]
//
// Date   : 2019-01-16 (Cameron)
// Detail : add function WS_DigitalChannelUploadAlbumDetails()
//
// Date   : 2019-01-15 (Tiffany)
// Detail : Modified WS_GetAnnouncementForApp(), WS_GetSchoolEventForApp(), WS_GetDigitalChannelPhotoList() add parameter "ParLang" to support language change
// Deploy : ip.2.5.10.1.1
//
// Date   : 2018-12-07 (Tiffany)
// Detail : Modified WS_GetPaymentForApp() let it not pass the item if it connect to a notice.
//
// Date   : 2018-12-03 (Thomas)
// Detail : Modified WS_GetCourseUsers() to fixed the problem of fail to get users if they are archived 
//
// Date   : 2018-11-13 (Tiffany)
// Detail : modified WS_GetStudentAttendanceInfo() to support $sys_custom['eClassApp']['attendanceShowNormalLeaveStatus'] to show "NL_display_status" if is "NL"
//
// Date   : 2018-11-03 (Thomas)
// Detail : Modified WS_GetCourseUsers(), skip loading user's photo if $powerlesson2_config['disableUserPhoto'] is set
//
// Date   : 2018-10-12 (Bill)
// Detail : Added WS_GetUserOfficialPhotoPath(), WS_GetUserPersonalPhotoPath(), get official / personal photo path for app
//
// Date   : 2018-09-07 (Thomas)
// Detail : Modified WS_GetCourseUsers(), use INNER JOIN instead of LEFT JOIN to improvement the sql performance
//
// Date   : 2018-08-22 (Tiffany)
// Detail : Modified WS_GetStudentTakeLeaveSettings(), add homework deliver method setting, WS_GetStudentTakeLeaveRecords and WS_SubmitApplyLeaveRecord, add methodID and method
// Deploy : ip.2.5.9.10.1.0
//
// Date   : 2018-07-18 (Tiffany)
// Detail : Modified WS_GetDigitalChannelPhotoList(), if kis use $plugin['DigitalChannels'],use DigitalChannels api
// 
// Date   : 2018-07-17 (Carlos)
// Detail : Added WS_GetPaymentItemMerchantInfo() for getting merchant account info by PaymentID (which is PAYMENT_PAYMENT_ITEMSTUDENT.PaymentID).
// 
// Date   : 2018-06-06 (Ronald)
// Detail : Add WS_CancelLeaveRecord() for cancel leave record by user
//          Modified WS_GetStudentTakeLeaveRecords(), return result with Reject Reason.
//          Modified GetStudentInfo_eClassApp, add ClassAttendanceType in return array.
//
// Date   : 2018-05-18 (Thomas)
// Detail : Modified __powerLessonApiCaller() to use if $powerlesson2_config['eclassInternalUrl'] is available
//
// Date   : 2018-05-17 (Tiffany)
// Detail : Improvement WS_GeteNoticeSForApp(), that not use the UNION ALL in sql, use array to handle the data
//
// Date   : 2018-05-15 (Cameron)
// Detail : apply $sys_custom['medical']['swapBowelAndSleep'] in WS_GetMedicalCaringForApp() and WS_GetMedicalCaringForTeacherApp()
//
// Date   : 2018-05-08 (Tiffany)
// Detail : Add settings in WS_GetStudentTakeLeaveSettings()
//
// Date   : 2018-05-08 (Cameron)
// Detail : retrieve sleep in WS_GetMedicalCaringForApp() and WS_GetMedicalCaringForTeacherApp()
//
// Date   : 2018-05-08 (Bill)   [2018-0507-1023-45110]
// Detail : Modified WS_GeteNoticeSForApp(), improved performance when view notice list (with signed count) in Teacher App
// Deploy : ip.2.5.9.5.1.0
//
// Date   : 2018-02-01 (Carlos)
// Detail : Modified WS_NewHKUFluSickLeaveRecord(), added optional paramter $from='A', non-sick leave and NA can submit 'N'.
//
// Date	  : 2018-01-26 (Carlos)
// Detail : Modified WS_updatePaymentCallbackFromCentralServer() to sign the notice for TNG payments.
//
// Date	  : 2018-01-16 (Ivan)
// Detail : Modified WS_GetMedicalCaringForTeacherApp() to add userId param and change to SSO to view record details
// Deploy : ip.2.5.9.3.1.0
//
// Date	  : 2017-12-21 (Carlos)
// Detail : Modified WS_updatePaymentCallbackFromCentralServer() to support multiple payment items for payment notice.
// Deploy : ip.2.5.9.1.1.0
//
// Date	  : 2017-12-06 (Tiffany)
// Detail : Add WS_GetHomeworkImageForApp() for homework image in app
// Deploy : ip.2.5.9.1.1.0
//
// Date	  : 2017-12-02 (Siuwan)
// Detail : Modified classNamingDB() to check $eclass_db too
// Deploy : ip.2.5.9.1.1.0
//
// Date	  : 2017-11-28 (Carlos)
// Detail : Modified WS_updatePaymentCallbackFromCentralServer() do not deduct balance for TNG transaction. 
//			*** Need to deploy with libpayment.php as Paid_Payment_Item() has added new parameter. ***
// Deploy : ip.2.5.9.1.1.0
// 
// Date	  : 2017-11-24 (Bill)	[2017-1121-1046-19206]
// Detail : Modified GetStudentInfo_eClassApp() to handle HTML entity in Chinese Name for IOS display
// Deploy : ip.2.5.9.1.1.0
//
// Date	  : 2017-11-06 (Carlos)
// Detail : Modified WS_updatePaymentCallbackFromCentralServer() to allow add back missing TNG record if there are no record.
// Deploy : ip.2.5.9.1.1.0
//
// Date   : 2017-10-19 (Ivan)
// Detail : Modified GetStudentInfo_eClassApp() to get default official photo path by function
// Deploy : ip.2.5.9.1.1.0
//
// Date   : 2017-10-17 (Siuwan)
// Detail : Modified WS_GetPL2PortalUserInfoWithAllCourses(), WS_GetPL2PortalUserInfo() to get user admin right
// Deploy : ip.2.5.8.12.1.0
//
// Date   : 2017-10-13 (Anna)
// Detail : Modified WS_GetNoticeForApp () added module enrolment
// Deploy : ip.2.5.8.10.1.0
//
// Date	  : 2017-10-09 (Ivan)
// Detail : Modified WS_GetAnnouncementForApp() to remove vertical tab special character in school news title
// Deploy : ip.2.5.8.10.1.0
//
// Date   : 2017-10-06 (Thomas)
// Detail : Modify function WS_GetCourseUsers to fix the problem of fail to get deleted user 
// Deploy : ip.2.5.8.10.1.0
//
// Date: 2017-09-25 (Henry)
// Detail : Added function WS_GetCourseTeachers(), WS_GetCoursesTeachers()
// Deploy : ip.2.5.8.10.1.0
//
// Date: 2017-09-22 (Ivan)
// Detail : modified WS_GetPaymentForApp() to hide passed deadline items for TNG integration
// Deploy : ip.2.5.8.10.1.0
//
// Date: 2017-09-21 (Henry)
// Detail : Added function WS_GetCoursesUsers()
// Deploy : ip.2.5.8.10.1.0
//
// Date: 2017-09-14 (Tiffany)
// Detail : Modified WS_GeteNoticeSForApp() to support $sys_custom['eClassTeacherApp']['showNoticeBeforeStart'] to show notice before start.
// Deploy : ip.2.5.8.10.1.0
//
// Date: 2017-09-01 (Ivan)
// Detail : Modified WS_GetNoticeForApp() to Group By NoticeID to fix SFOC has multiple notices if signed the notice multiple times
// Deploy : ip.2.5.8.10.1.0
//
// Date : 2017-08-28 (Thomas)
// Detail : Added function WS_GetPL2LessonPlanUsersUdid(), to get Lesson, User, Device Mapping for PL2 
// Deploy : ip.2.5.8.9.1.0
//
// Date : 2017-08-18 (Henry)
// Detail : Added function WS_GetPL2PortalUserInfoWithAllCourses(), to get all courses when user is teacher
// Deploy : ip.2.5.8.9.1.0
//
// Date : 2017-08-15 (Tiffany)
// Detail : Modified function WS_GetNoticeUrlForApp() to support SchoolNews and eCircular
// Deploy : ip.2.5.8.9.1.0
//
// Date : 2017-06-27 (Roy)
// Detail : added function WS_OpenGroupMessageChatroom()
// Deploy : ip.2.5.8.7.1.0
//
// Date : 2017-06-20 (Roy)
// Detail : added function WS_GetTargetableUsersForMessaging(), get user list for messaging in eclass apps
// Deploy : ip.2.5.8.7.1.0
//
// Date : 2017-05-16 (Siuwan)
// Detail : added function WS_IsPowerLessonCompatible(), check PowerLesson version compatible
// Deploy : ip.2.5.8.7.1.0
//
// Date : 2017-05-10 (Siuwan)
// Detail : Modified function WS_GetCourseUsers() and WS_GetPL2PortalUserInfo(), get user photo by calling GET_USER_PHOTO() in libuser.php
// Deploy : ip.2.5.8.7.1.0
//
// Date : 2017-05-10 (Roy) [U116274]
// Detail : Modified function WS_GetAttendanceDetailsForApp(), also apply pm waive to early leave am case
// Deploy : ip.2.5.8.7.1.0
//
// Date : 2017-05-09 (Siuwan)
// Detail : Modified function WS_GetCourseUsers() added intranet_undo_htmlspecialchars() for chineseName and englishName
// Deploy : ip.2.5.8.7.1.0
//
// Date : 2017-05-04 (Tiffany)
// Detail : Added WS_UpdateEmailForDHL
// Deploy : ip.2.5.8.6.1.0
//
// Date : 2017-05-02 (Tiffany)
// Detail : Added WS_ForgetPasswordForDHL()
// Deploy : ip.2.5.8.6.1.0
//
// Date : 2017-03-31 (Siuwan)
// Detail : added WS_ValidateLessonPin() for pin validation and then return course + users
// added WS_GetEclassGroups() to get grouping and user_group data in classroom
// added WS_GetCourseUsers(), __powerLessonApiCaller()
// modified WS_LoginCourseByLessonPin() and WS_LogoutCourseByAuthToken() to call PowerLesson api using __powerLessonApiCaller()
// Deploy : ip.2.5.8.4.1.0
//
// Date : 2017-01-05 (Thomas)
// Detail : Modified WS_GetPL2PortalUserInfo() and WS_GetCourseInfo() to fix the problem of including encoded html special characters in the response
// Deploy : ip.2.5.8.1.1.0
//
// Date : 2016-12-19 (Ivan)
// Detail : Added WS_updatePaymentCallbackFromCentralServer()
// Deploy : ip.2.5.8.1.1.0
//
// Date : 2016-11-14 (Roy)
// Detail : Modified WS_GetDigitalChannelPhotoList, init $photoFinalAry = array()
// Deploy : ip.2.5.7.12.1.0
//
// Date : 2016-11-14 (Tiffany)
// Detail : Modified WS_GeteNoticeSForApp(), add admin logic and handle settings "teacherCanSeeAllNotice".
// Deploy : ip.2.5.7.10.1.0
//
// Date : 2016-11-11 (Ronald)
// Detail : Added WS_eClassAppFileUpload_cceap()
// Deploy : ip.2.5.7.10.1.0
//
// Date : 2016-10-13 (Pun)
// Detail : Added WS_RemoveAuthToken()
// Deploy : ip.2.5.7.10.1.0
//
// Date : 2016-10-03 (Paul)
// Detail : Modified WS_eClassAppValidate(), modify checking of PL app expiry by cutting off at the expiry date 23:59:59
// Deploy : ip.2.5.7.10.1.0
//
// Date : 2016-09-21 (Pun)
// Detail : Modified WS_GetPL2PortalUserInfo(), added return 'language' field for course info
// Deploy : ip.2.5.7.10.1.0
//
// Date : 2016-09-07 (Roy)
// Detail : modified WS_GetAnnouncementForApp()
// - fix customCompare incorrect sorting logic, should get data by field name 'UNIX_TIMESTAMP(AnnouncementDate)' & 'Title';
// - do not set number of announcement limit to 500 if $special_feature['sch_announcement_display_sorting_asc'] = true
// Deploy : ip.2.5.7.10.1.0
//
// Date : 2016-09-06 (Pun)
// Detail : Added WS_RenewSessionLastUpdated()
// Deploy : ip.2.5.7.10.1.0
//
// Date : 2016-09-05 (Roy)
// Detail : modified WS_GetAnnouncementForApp(), fix customCompare redeclare problem
// Deploy : ip.2.5.7.10.1.0
//
// Date : 2016-09-02 (Henry HM)
// Detail : Added WS_IsHKUFluRecordExists()
// Deploy :
//
// Date : 2016-09-01 (Pun)
// Detail : Added WS_RenewAuthToken()
// Deploy : ip.2.5.7.10.1.0
//
// Date : 2016-08-31 (Bill) [2016-0830-1705-23207]
// Details : modified WS_GetAnnouncementForApp(), fix cannot view school announcement when Allow users to view past news = false
// Deploy : ip.2.5.7.10.1.0
//
// Date : 2016-08-26 (Thomas)
// Detail : Modified function WS_eClassAppValidate() to add HideLoadingViewIfNoInternet in the $ReturnContent
// Deploy : ip.2.5.7.10.1.0
//
// Date : 2016-08-20 (Pun)
// Detail : Added WS_GetCourseInfo()
// Deploy : ip.2.5.7.10.1.0
//
// Date : 2016-08-16 (Thomas)
// Detail : Modified WS_eClassAppValidate() to add DisableRelaunchLesson in the $ReturnContent
// Add function WS_eClassAppRelaunchLessonValidate(), to verify the lesson is able to relaunch or not
// Deploy : ip.2.5.7.7.10.0
//
// Date : 2016-08-10 (Tiffany)
// Detail : Modified WS_GeteNoticeSForApp()
//
// Date : 2016-08-09 (Pun)
// Details : Added WS_GetPL2PortalUserInfo() for PL 2.0
// Deploy : ip.2.5.7.10.1.0
//
// Date : 2016-08-09 (Henry HM)
// Details : Add api WS_GetHKUFluAgreementRecord, WS_NewHKUFluAgreementRecord, WS_GetHKUFluSickLeaveRecord, WS_NewHKUFluSickLeaveRecord
// Deploy :
//
// Date : 2016-07-25 (Paul)
// Details : modified WS_eClassAppFileUpload(), WS_eClassAppFileUploadByChunk() to remove deprecated flags in ffmpeg
// Deploy : ip.2.5.7.7.1.0
//
// Date : 2016-07-22 (Roy)
// Details : modified WS_GetStudentTakeLeaveSettings(), add hideAddAttachmentButton setting according to flag $sys_custom['eClassApp']['applyLeaveHideAddAttachmentButton']
// Deploy : ip.2.5.7.9.1.0
//
// Date : 2016-07-08 (Bill)
// Details : modified WS_GetNoticeForApp() to support eNotice improvement (notice - start date and end date with time)
// Deploy : ip.2.5.7.7.1.0
//
// Date : 2016-06-28 (Tiffany)
// Details : add getUrlData(), getUrlContents() and modified the WS_SubmitWeeklyDiaryRecord() to support URL analysis
// Deploy :
//
// Date : 2016-06-13 (Roy)
// Details : Update WS_GetELibPlusLoanBookrecord(), WS_GetELibPlusReservedBookrecord() and WS_GetELibPlusOutstandingPenalty(), return emply array if no data
// Deploy :
//
// Date : 2016-06-06 (Tiffany)
// Details : added function WS_SubmitWeeklyDiaryRecord()
// Deploy :
//
// Date : 2016-05-30 (Siuwan)
// Details : added function WS_eClassGetLessonUsersUdid()
// Deploy : ip.2.5.7.7.1.0
//
// Date : 2016-05-16 (Henry)
// Details : added WS_GetELibPlusLoanBookrecord(), WS_GetELibPlusReservedBookrecord() and WS_GetELibPlusOutstandingPenalty()
// Deploy :
//
// Date : 2016-05-13 (Bill) [2016-0513-1440-15206]
// Details : modified WS_GetAnnouncementForApp(), support Setting - Allow users to view past news
// Deploy : ip.2.5.7.7.1.0
//
// Date : 2016-05-03 (Roy)
// Details : [B95546] modified WS_GetAnnouncementForApp(), temporary set maximum number of announcement to 500 due to android parent app bug
// Deploy :
//
// Date : 2016-03-30 (Ivan)
// Details : modified WS_GetStudentAttendanceInfo() to support $sys_custom['eClassApp']['attendanceShowPresentAbsentOnly'] to show "NL_display_status"
// Deploy : ip.2.5.7.3.1.0
//
// Date : 2016-02-26 (Ivan)
// Details : modified WS_GetStudentAttendanceInfo() to support $sys_custom['eClassApp']['attendanceShowPresentAbsentOnly'] to show "late" as "on time"
// Deploy : ip.2.5.7.3.1.0
//
// Date : 2016-02-19 (Tiffany)
// Details : modified WS_GetPaymentForApp() to handle if doesn`t has "KIS_OnlyShowPaymentRecords" setting.
// Deploy : ip.2.5.7.3.1.0
//
// Date : 2016-02-17 (Ivan)
// Details : modified WS_GetPaymentForApp() to add "KIS_OnlyShowPaymentRecords" to app
// Deploy : ip.2.5.7.3.1.0
//
// Date : 2016-02-15 (Tiffany)
// Details : add trim_control_character() to trim the control cahracter in title or description.
// Deploy : ip.2.5.7.3.1.0
//
// Date : 2016-01-26
// Details : modified WS_SubmitApplyLeaveRecord() to support send push message to class teacher logic
// Deploy : ip.2.5.7.3.1.0
//
// Date : 2016-01-25 (Tiffany)
// Details : Add WS_GetMedicalCaringForTeacherApp()
// Deploy : ip2.5.7.1.1.0
//
// Date : 2016-01-05 (Ivan)
// Details : changed all function split() to explode() for PHP5.4
// Deploy : ip2.5.7.1.1.0
//
// Date : 2015-12-30 (Paul)
// Details : modified WS_eClassAppGetLesson(), hide private lesson plan from being shown on the list (student only)
// Deploy : ip2.5.7.1.1.0
//
// Date : 2015-12-16 (Roy)
// Details : modified WS_GetAttendanceDetailsForApp(), hide eAttendance time according to eclass app setting
// Deploy : ip2.5.7.1.1.0
//
// Date : 2015-12-03 (Tiffany)
// Details : Add WS_GetMedicalCaringForApp()
// Deploy: ip2.5.7.1.1.0
//
// Date : 2015-11-24 (Roy) [U89090]
// Details : Modified WS_GetHomeworkListForApp() to return workload hours to app
// Deploy: ip.2.5.6.12.1.0
//
// Date : 2015-09-30 (Ivan) [C86452]
// Details : Modified WS_GetPaymentForApp() to show payment item after item start date only
// Deploy: ip.2.5.6.10.1.0
//
// Date : 2015-09-01 (Ivan)
// Details : Modified WS_GetNoticeForApp(), WS_GeteCircularForApp(), WS_GeteNoticeSForApp() to check end date instead of start date to determine the notice's academic year
// Deploy: ip.2.5.6.10.1.0
//
// Date : 2015-08-13 (Ivan)
// Details : Modified function WS_GetPaymentForApp to cater student without payment account case
// Deploy: ip.2.5.6.7.1.0 (CD1)
//
// Date : 2015-07-24 (Thomas)
// Details : - Modified function WS_eClassAppFileUploadByChunk(), add $ReturnContent['fileVar']['path'], $ReturnContent['fileVar']['encodedPath'], $ReturnContent['fileVar']['encodedPathForFlash'], $ReturnContent['fileVar']['ecfpath']
// Deploy: ip.2.5.6.7.1.0
//
// Date : 2015-07-13 (Roy)
// Details : - improve WS_GetAttendanceDetailsForApp(), add waive checking to amStatus, pmStatus and leaveStatus
// Deploy: ip.2.5.6.7.1.0
//
// Date : 2015-07-10 (Roy)
// Details : improve WS_GetStudentAttendanceInfo(), add show / hide attendance time, leave section feature
// Deploy: ip.2.5.6.7.1.0
//
// Date : 2015-06-26 (Roy)
// Details : Add WS_GetNoticeUrlForApp()
// Deploy: ip.2.5.6.7.1.0
//
// Date : 2015-06-19 (Ivan)
// Details : Modified WS_GetSchoolEventForApp() to add sorting by event date and event title now
// Deploy: ip.2.5.6.7.1.0
//
// Date : 2015-06-19 (Qiao)
// Details : Add WS_UserChangePassword() to handle eClass App change account's password
// Deploy: ip.2.5.6.7.1.0
//
// Date : 2015-06-16 (Thomas)
// Details : Modified WS_eClassAppFileUploadByChunk() to implement a new algorithm for checking the file checksum
// Deploy: ip.2.5.6.7.1.0
//
// Date : 2015-06-16 (Ivan)
// Details : Modified WS_GetPaymentForApp() to retrieve transaction records from current academic year instead of just 90 days
// Deploy: ip.2.5.6.7.1.0
//
// Date : 2015-06-08 (Tiffany)
// Detail : Modified WS_GeteNoticeSForApp()
//
// Date : 2015-05-05 (Thomas)
// Details : Modified function WS_eClassAppValidate(), add $ReturnContent['RefreshButtonCoolDownInterval'] for customizing the cooldown period of the WebView Refresh Button
// Deploy: ip.2.5.6.5.1.0
//
// Date : 2015-03-17 (Qiao)
// Details : add WS_GetDigitalChannelPhotoList()
// Deploy: ip.2.5.6.5.1.0
//
// Date : 2015-03-17 (Thomas)
// Details : Modified WS_eClassAppValidate(), add $ReturnContent['FlippedChannelsServerPath'] for passing the server path for flipped channels to app
// Deploy: ip.2.5.6.5.1.0
//
// Date : 2015-03-13 (Thomas)
// Detail : Modified WS_CreateLessonPlanWithVideoKey(), fix the problem of getting deleted user for create lesson plan
// Deploy: ip.2.5.6.5.1.0
//
// Date : 2015-03-11 (Thomas)
// Detail : Modified WS_eClassAppValidate() to pass the $plugin['FlippedChannels'] to $ReturnContent
// Deploy: ip.2.5.6.3.1.0
//
// Date : 2015-03-05 (Ivan) [Case#K75971]
// Detail : Modified WS_GetSchoolEventForApp() to retrieve parent's group events also
// Deploy: ip.2.5.6.3.1.0
//
// Date : 2015-03-04 (Thomas)
// Detail : Modified function WS_eClassAppGetClassroom(), add parameter $withMemberType, $noWWSClassroom to filter the classrom list
//
// Date : 2015-02-24 (Qiao)
// Detail : Add function WS_GetSubjectListForApp()
//
// Date : 2015-01-27 (Ivan)
// Detail : Modified GetStudentInfo_eClassApp() to add personal photo logic in the photo display logic
// Deploy: ip.2.5.6.3.1.0
//
// Date : 2015-01-06 (Thomas)
// Detail : Modified WS_eClassAppGetStudent():
// - Hide the student that are login already in the student list after QR Code Login
// Deploy: ip.2.5.6.1.1.0
//
// Date : 2014-12-31 (Thomas)
// Detail : Modified WS_eClassAppFileUploadByChunk():
// - Use 'cat' command instead fread() and fwrite() to improve I/O performance
// Deploy: ip.2.5.6.1.1.0
//
// Date : 2014-12-10 (Roy)
// Detail : Modified WS_GetAttendanceDetailsForApp():
// - add attendance mode handling
// - remove abs() for late minutes
// Deploy: ip.2.5.6.1.1.0
//
// Date : 2014-12-08 (Ivan)
// Modified GetStudentInfo_eClassApp() to update get module access right logic
// Deploy: ip.2.5.6.1.1.0
//
// Date : 2014-11-20 (Ivan)
// Detail : Modified WS_GetHomeworkListForApp() to add subject name in the return array
// Deploy: ip.2.5.6.1.1.0
//
// Date : 2014-11-18 (Ivan)
// Detail : Added if function exists before declaring "classNamingDB" function
// Deploy: ip.2.5.6.1.1.0
//
// Date : 2014-11-14 (Thomas)
// Details : Modified function WS_eClassAppValidate(), pass the $sys_custom['power_lesson_app_webservice_timeout'] to $ReturnContent['CustTimeout'] if it is set
//
// Date : 2014-11-10 (Roy) [Y69029]
// Details : fix WS_GetAttendanceDetailsForApp(), the absent count is wrong for case WD/AM/PM count as 1
//
// Date : 2014-10-20 (Tiffany)
// Details : add function WS_GeteNoticeSForApp()
//
// Date : 2014-09-19 (Thomas)
// Details : Modified function WS_eClassAppGetLesson() to remove the extra ')' if the lesson plan without planned and post lesson date is not yet delivered
//
// Date : 2014-09-17 (Ivan)
// Details : Modified function WS_GetStudentAttendanceInfo() to update get attendance status logic
//
// Date : 2014-09-04 (Thomas)
// Details : Modified function WS_eClassAppGetClassroom() to return full classroom list if isEmbeddedPowerLesson is set in GENERAL_SETTING
//
// Date : 2014-08-26 (Ivan)
// Details : added GetMultipleRequestResult() to return multiple requests result
// added WS_GeteCircularForApp() to get teacher's eCircular
//
// Date : 2014-08-25 (Ivan)
// Details : modified WS_GetAnnouncementForApp() to return parent's school news as well
//
// Date : 2014-08-15 (Ivan)
// Details : added WS_GetCurrentServerDateTime()
//
// Date : 2014-05-05 (Ivan)
// Details : modified WS_SubmitApplyLeaveRecord() to send push notification to app after leave record is received
//
// Date : 2014-02-19 (Ivan)
// Details : Added WS_SubmitApplyLeaveRecord()
//
// Date : 2014-02-10 (Roy)
// Details : Added WS_GetAttendanceDetailsForApp()
//
// Date : 2014-01-29 (Ivan)
// Details : Added WS_GetPaymentForApp()
//
// Date : 2014-01-15 (Ivan)
// Details : Added WS_GetSchoolEventForApp()
//
// Date : 2014-01-09 (Ivan)
// Details : Added saveUserDevice(), WS_GetChildrenListForApp(), WS_GetStudentAttendanceInfo(), updatePushMessageReadStatus(), returnSchoolBanner(), WS_GetNoticeForApp(), WS_GetAnnouncementForApp(), deleteAppAccountServerFollowUp()
//
// Date : 2013-10-18 (Ivan)
// Details : Modified function WS_GetPayment() to fix the SQL error problem. Changed "a.StartDate" to "b.StartDate"
//
// Date : 2013-08-20 (Thomas)
// Details : Add function WS_eClassAppGetStudent(), WS_eClassAppQRCodeValidate() for PowerLesson App QR Code Login
//
// Date : 2013-06-27 (Yuen)
// Details : Modify function WS_GetPayment() to retrieve the records of current school year

// Date : 2013-05-10 (Thomas)
// Details : Modify function WS_eClassAppValidate(), function WS_eClassAppGetClassroom() to support WWS Project
//
// Date : 2013-02-18 (Thomas)
// Details : Modify function WS_eClassAppFileUpload(), Add $ReturnContent['fileVar']['file_size']
//
// Date : 2012-11-14 (Thomas)
// Details : rewritten function WS_eClassAppFileUpload()
//
// Date : 2012-11-07 (Yuen)
// Details : modified GetChildrenList() to support optional school badge (two more data to be returned: SchoolImage, HWDisplayMode)
// modified GetAnnouncementList() to support AttachmentURL
//
// Date : 2012-10-11 (Thomas)
// Details : Modified function WS_eClassAppGetLesson(), fix the problem of not showing the 'In Progress' lesson
//
// Date : 2012-09-18 (Thomas)
// Details : Modified function WS_eClassAppGetLesson(), add logic to handle pre-lesson and post-lesson information
//
// Date : 2012-07-23 (Yuen)
// Details : improved for handling user photos of invalid JPEG and also using default GIF
//
// Date : 2012-06-11 (Jason)
// Details : modify WS_eClassAppFileUpload() to add the checking of media conversion for FileUpload Activity
//
// Date : 2012-01-17 (Thomas)
// Details : add WS_eClassAppFileUpload(), for PowerTools App
//
// Date : 2011-07-19 (Henry Chow)
// Details : add WS_GetAnnouncement(), for API of Parent App
//
// Date : 2011-07-19 (YatWoon)
// Details : add WS_GetEvent(), WS_GetPayment(), for API of Parent App
//
// Date : 2011-07-19 (Henry Chow)
// Details : add WS_GetNotice(), for API of Parent App
//
// Date : 2010-05-25 (Henry Yuen)
// Details : support powerVoice and url types in eContent
// modify GeteBookToken
//
// Date : 20100211 (Henry Yuen)
// Details : copy from IP25
include_once ('libxml.php');
class libeclass_ws extends libdb {
	var $ck_course_id;
	function libeclass_ws() {
		$this->libdb ();
	}
	function WS_GetAnnouncement($UserLogin, $NoOfRecords, $TargetGroup) {
		global $eclass_db, $special_feature, $intranet_root, $file_path, $PATH_WRT_ROOT, $SessionID;
		
		if (trim ( $PATH_WRT_ROOT ) == "") {
			$PATH_WRT_ROOT = "../";
		}
		
		// $school_url = split("\n",get_file_content("$intranet_root/file/email_website.txt"));
		$school_url = explode ( "\n", get_file_content ( "$intranet_root/file/email_website.txt" ) );
		$school_domain = $school_url [0];
		
		if (substr ( $school_domain, - 1 ) == "/")
			$school_domain = substr ( $school_domain, 0, - 1 );
		
		include_once ("libannounce.php");
		$la = new libannounce ();
		
		include_once ("libfilesystem.php");
		$lfs = new libfilesystem ();
		
		$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$UserLogin' AND RecordStatus=1 AND RecordType=2";
		$result = $this->returnVector ( $sql );
		
		if ($result [0] == "") {
			return 104;
			exit ();
		}
		
		if ($TargetGroup == "C") { // class teacher announcement
			$sql = "SELECT a.ClassName, a.ClassID FROM INTRANET_CLASS a INNER JOIN INTRANET_USER b ON (a.ClassName = b.ClassName) WHERE b.UserLogin='" . $this->Get_Safe_Sql_Query ( $UserLogin ) . "' AND b.RecordType=2 AND b.RecordStatus=1 ORDER BY a.ClassName";
			$result = $la->returnArray ( $sql, 2 );
			// echo $sql;
			if (sizeof ( $result ) == 0) {
				return 704;
				exit ();
			}
			
			for($i = 0; $i < sizeOf ( $result ); $i ++) {
				$className [] = $result [$i] [0];
				$classID [] = $result [$i] [1];
			}
			
			// $start_date_sql = ($special_feature['portal']) ? format_date_no_year_sql("Date_Start") : format_date_sql("Date_Start");
			
			for($i = 0; $i < sizeOF ( $classID ); $i ++) {
				$sql = "SELECT count(*) FROM {$eclass_db}.course where course_id='" . $classID [$i] . "'";
				$row_check = $la->returnVector ( $sql );
				
				if ($row_check [0]) {
					$sql = "SELECT 
									announcement_id,
									message,
									Date_Start,
									'" . $className [$i] . " Class Teacher',
									'C',
									UNIX_TIMESTAMP(Date_Start)
								FROM 
									" . classNamingDB ( $classID [$i] ) . ".announcement
								WHERE
									Date_Start <= NOW() AND Date_End >= NOW()
								ORDER BY Date_Start DESC
							";
					
					$result = $la->returnArray ( $sql, 5 );
					
					if (sizeof ( $result ) == 0) {
						// return 702;
						// exit;
						// $ReturnAry['Announcement'][] = array();
						
						// return empty array if no records found
						$ReturnAry = array ();
					} else {
						
						$terminate = 0;
						$previousTimeStamp = "";
						
						for($i = 0; $i < $NoOfRecords || $terminate == 0; $i ++) {
							list ( $announceId, $msg, $dateStart, $sender, $type, $timestamp ) = $result [$i];
							
							if ($i >= $NoOfRecords && $previousTimeStamp > $timestamp) {
								$terminate = 1;
								break;
							}
							$ReturnAry ['Announcement'] [] = array (
									"AnnouncementID" => $announceId,
									"Title" => '',
									"Description" => str_replace ( "&nbsp;", " ", strip_tags ( intranet_undo_htmlspecialchars ( $msg ) ) ), // [IP2.5]
									                                                                                               // "AnnouncementDate" => str_replace(' ', 'T', $dateStart),
									"AnnouncementDate" => $dateStart,
									"PosterName" => $sender,
									"TargetGroup" => $type,
									"Attachment" => '',
									"VoiceFile" => '',
									"IsRead" => '' 
							);
							$previousTimeStamp = $timestamp;
							if ($i == sizeof ( $result ) - 1)
								break;
						}
					}
					
					return $ReturnAry;
				}
			}
		} else if ($TargetGroup == "S") { // school announcement
			
			global $special_feature, $plugin;
			
			// $name_field = "'System Admin'";
			$name_field = getNameFieldWithClassNumberByLang ( "b." ); // [IP2.5]
			
			$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='" . $this->Get_Safe_Sql_Query ( $UserLogin ) . "' AND RecordStatus=1 AND RecordType=2";
			$result = $this->returnVector ( $sql );
			
			if ($result [0] == "") {
				return 104;
			} else {
				$ThisUserID = $result [0];
			}
			
			include_once ("libuser.php");
			$lu = new libuser ( $ThisUserID );
			$Groups = $lu->returnGroupIDs ();
			
			$sql = "SELECT DISTINCT AnnouncementID FROM INTRANET_GROUPANNOUNCEMENT";
			$AnnouncementIDs = $this->db_sub_select ( $sql );
			
			if (! $special_feature ['sch_announcement_display_future']) {
				$check_shart_date = "CURDATE()>=a.AnnouncementDate AND";
			}
			
			$order = $special_feature ['sch_announcement_display_sorting_asc'] ? "ASC" : "DESC";
			
			$conds = "a.AnnouncementID NOT IN ($AnnouncementIDs) AND a.RecordStatus = '1' AND (a.EndDate is null OR ( " . $check_shart_date . " CURDATE() <= a.EndDate))";
			
			// JR20: main page
			// $start_date_sql = ($special_feature['portal']) ? format_date_no_year_sql("a.AnnouncementDate") : format_date_sql("a.AnnouncementDate");
			
			if ($plugin ['power_voice']) {
				$sql = "
								SELECT 
									a.AnnouncementID, a.Title, a.Description, a.AnnouncementDate, $name_field, 'S', IF(a.Attachment IS NOT NULL, 'with attachment', ''), UNIX_TIMESTAMP(AnnouncementDate), a.ReadFlag, a.VoiceFile, a.Attachment
	                        	FROM 
	                        		INTRANET_ANNOUNCEMENT as a
	                             		LEFT OUTER JOIN 
	                             	INTRANET_USER as b 
	                             		ON a.UserID = b.UserID
	                             		LEFT OUTER JOIN 
	                             	INTRANET_GROUP as c 
	                             		ON a.OwnerGroupID = c.GroupID
	                             	WHERE 
	                             		$conds 
	                             	ORDER BY 
	                             		a.AnnouncementDate " . $order . ", a.Title ASC
	                            ";
				
				$result = $la->returnArray ( $sql, 8 );
			} else {
				$sql = "
								SELECT 
									a.AnnouncementID, a.Title, a.Description, a.AnnouncementDate, $name_field, 'S', IF(a.Attachment IS NOT NULL, 'with attachment', ''), UNIX_TIMESTAMP(AnnouncementDate), a.ReadFlag, a.Attachment
	                        	FROM 
	                        		INTRANET_ANNOUNCEMENT as a
	                             		LEFT OUTER JOIN 
	                             	INTRANET_USER as b 
	                             		ON a.UserID = b.UserID
	                             		LEFT OUTER JOIN 
	                             	INTRANET_GROUP as c 
	                             		ON a.OwnerGroupID = c.GroupID
	                             	WHERE 
	                             		$conds 
	                             	ORDER BY 
	                             		a.AnnouncementDate " . $order . ", a.Title ASC
	                            ";
				
				$result = $la->returnArray ( $sql, 7 );
			}
			
			if (sizeof ( $Groups ) > 0)
				$row_groups = $la->returnGroupsAnnouncement_Web_Service ( $Groups );
			
			if (sizeof ( $row_groups ) > 0) {
				// $row_groups = $this->returnGroupsAnnouncement($Groups);
				$result = array_merge ( $result, $row_groups );
				// debug_r($row_school);
				// perform sorting to 2D-array
				function customCompare($one, $two) {
					global $special_feature;
					if ($special_feature ['sch_announcement_display_sorting_asc']) {
						if ($one [7] < $two [7])
							return - 1;
					} else {
						if ($one [7] > $two [7])
							return - 1;
					}
					
					return 1;
				}
				usort ( $result, "customCompare" );
			}
			
			if (sizeof ( $result ) == 0) {
				// return 703;
				// exit;
				// $ReturnAry['Announcement'][] = array();
				
				// return empty array if no records found
				$ReturnAry = array ();
			} else {
				
				$terminate = 0;
				$$previousTimeStamp = "";
				for($i = 0; $i < $NoOfRecords || $terminate == 0; $i ++) {
					
					if ($plugin ['power_voice']) {
						list ( $announceId, $title, $msg, $dateStart, $sender, $type, $attachment_flag, $timestamp, $readFlag, $voiceFile, $attachment_folder ) = $result [$i];
					} else {
						list ( $announceId, $title, $msg, $dateStart, $sender, $type, $attachment_flag, $timestamp, $readFlag, $attachment_folder ) = $result [$i];
					}
					
					if ($i >= $NoOfRecords && $previousTimeStamp > $timestamp) {
						$terminate = 1;
						break;
					}
					
					$AttachmentURL = "";
					if (trim ( $attachment_folder ) != "") {
						$attachment_path = "$file_path/file/announcement/" . $attachment_folder . $announceId;
						if (file_exists ( $attachment_path )) {
							$attachment_array = $lfs->return_files ( $attachment_path );
							if (sizeof ( $attachment_array ) == 1) {
								$AttachmentURL = $school_domain . "/home/download_attachment.php?target_e=" . getEncryptedText ( $attachment_array [0] );
							} elseif (sizeof ( $attachment_array ) > 1) {
								// list files for user to view one by one
								$Module = "Announcement";
								$AttachmentURL = $school_domain . "/home/view_files.php?key=" . EncryptModuleFilePars ( $Module, $UserLogin, $user_id, $announceId );
							}
						}
					}
					
					if ($voiceFile != "") {
						if ($AttachmentURL != "") {
							if (sizeof ( $attachment_array ) == 1) {
								// list files for user to view one by one
								$Module = "Announcement";
								$AttachmentURL = $school_domain . "/home/view_files.php?key=" . EncryptModuleFilePars ( $Module, $UserLogin, $user_id, $announceId );
							}
						} else {
							$AttachmentURL = $school_domain . "/home/download_attachment.php?target_e=" . getEncryptedText ( $voiceFile );
						}
					}
					
					if ($readFlag == "") {
						$isRead = "N";
					} else {
						$tempAry = explode ( ';;', substr ( $readFlag, 1, - 1 ) );
						$isRead = (in_array ( $ThisUserID, $tempAry )) ? "Y" : "N";
					}
					
					$delimiter = "###";
					$sql_usr = "SELECT RecordType, UserID FROM INTRANET_USER where SessionKey='$SessionID'";
					$row_usr = $this->returnArray ( $sql_usr, null, true );
					if (sizeof ( $row_usr ) > 0) {
						$CurrentUserID = $row_usr [0] ["UserID"];
					}
					$keyStr = $special_feature ['ParentApp_PrivateKey'] . $delimiter . $announceId . $delimiter . $ThisUserID . $delimiter . $CurrentUserID;
					$token = md5 ( $keyStr );
					$signURL = $school_domain . '/api/announcement_view.php?token=' . $token . '&AnnouncementID=' . $announceId . '&StudentID=' . $ThisUserID . '&CurID=' . $CurrentUserID;
					
					$tmpAry = array ();
					$tmpAry = array (
							"AnnouncementID" => $announceId,
							"Title" => $title,
							"Description" => str_replace ( "&nbsp;", " ", strip_tags ( intranet_undo_htmlspecialchars ( $msg ) ) ), // [IP2.5]
							                                                                                               // "AnnouncementDate" => str_replace(' ', 'T', $dateStart),
							"AnnouncementDate" => $dateStart,
							"PosterName" => $sender,
							"TargetGroup" => $type,
							"Attachment" => $attachment_flag,
							"AttachmentURL" => $AttachmentURL,
							"VoiceFile" => $voiceFile,
							"IsRead" => $isRead 
					);
					if ($plugin ['eClassApp']) {
						$tmpAry ['SignURL'] = $signURL;
					}
					
					$ReturnAry ['Announcement'] [] = $tmpAry;
					
					$previousTimeStamp = $timestamp;
					if ($i == sizeof ( $result ) - 1)
						break;
				}
			}
			
			return $ReturnAry;
		} else {
			return 701;
		}
	}

	function WS_GetAnnouncementForApp($TargetUserID, $CurrentUserID, $NewestDateTime, $NoOfRecords, $TargetGroup, $ParLang="")
    {
		global $eclass_db, $special_feature, $intranet_root, $file_path, $PATH_WRT_ROOT, $SessionID, $sys_custom;
		
		include_once($intranet_root.'/includes/eClassApp/libeClassApp.php');
		$libeClassApp = new libeClassApp();
		
		if (trim($PATH_WRT_ROOT) == "") {
			$PATH_WRT_ROOT = "../";
		}
		
		if ($NoOfRecords == '') {
			// get current and future year notice
			// $conds_startDate = " AND DATE(a.DateStart) >= '".getStartDateOfAcademicYear(Get_Current_Academic_Year_ID())."' ";
			
			$NoOfRecords = 9999999;
		}
		
		// $school_url = split("\n",get_file_content("$intranet_root/file/email_website.txt"));
		// $school_domain = $school_url[0];
		$school_domain = curPageURL($withQueryString=false, $withPageSuffix=false);
		
		if (substr($school_domain, -1) == "/") {
			$school_domain = substr($school_domain,0,-1);
		}
		
		include_once("libannounce.php");
		$la = new libannounce();
		
		include_once("libschoolnews.php");
		$lschoolnews = new libschoolnews();
		
		include_once("libfilesystem.php");
		$lfs = new libfilesystem();
		
		if ($TargetGroup == "C") {      // class teacher announcement
			$sql = "SELECT a.ClassName, a.ClassID FROM INTRANET_CLASS a INNER JOIN INTRANET_USER b ON (a.ClassName = b.ClassName) WHERE b.UserID='" . $TargetUserID . "' AND b.RecordType=2 ORDER BY a.ClassName";
			$result = $la->returnArray ( $sql, 2 );
			// echo $sql;
			if (sizeof ( $result ) == 0) {
				return 704;
				exit ();
			}
			
			for($i = 0; $i < sizeOf ( $result ); $i ++) {
				$className [] = $result [$i] [0];
				$classID [] = $result [$i] [1];
			}
			
			// $start_date_sql = ($special_feature['portal']) ? format_date_no_year_sql("Date_Start") : format_date_sql("Date_Start");
			
			for($i = 0; $i < sizeOF ( $classID ); $i ++) {
				$sql = "SELECT count(*) FROM {$eclass_db}.course where course_id='" . $classID [$i] . "'";
				$row_check = $la->returnVector ( $sql );
				
				if ($row_check [0]) {
					$sql = "SELECT 
								announcement_id,
								message,
								Date_Start,
								'" . $className [$i] . " Class Teacher',
								'C',
								UNIX_TIMESTAMP(Date_Start)
							FROM 
								" . classNamingDB ( $classID [$i] ) . ".announcement
							WHERE
								Date_Start <= NOW() AND Date_End >= NOW()
							ORDER BY Date_Start DESC
						";
					
					$result = $la->returnArray ( $sql, 5 );
					
					if (sizeof ( $result ) == 0) {
						// return 702;
						// exit;
						// $ReturnAry['Announcement'][] = array();
						
						// return empty array if no records found
						$ReturnAry ['Announcement'] = array ();
					} else {
						
						$terminate = 0;
						$previousTimeStamp = "";
						
						for($i = 0; $i < $NoOfRecords || $terminate == 0; $i ++) {
							list ( $announceId, $msg, $dateStart, $sender, $type, $timestamp ) = $result [$i];
							
							if ($i >= $NoOfRecords && $previousTimeStamp > $timestamp) {
								$terminate = 1;
								break;
							}
							$ReturnAry ['Announcement'] [] = array (
									"AnnouncementID" => $announceId,
									"Title" => '',
									"Description" => str_replace ( "&nbsp;", " ", strip_tags ( intranet_undo_htmlspecialchars ( $msg ) ) ), // [IP2.5]
									                                                                                               // "AnnouncementDate" => str_replace(' ', 'T', $dateStart),
									"AnnouncementDate" => $dateStart,
									"PosterName" => $sender,
									"TargetGroup" => $type,
									"Attachment" => '',
									"VoiceFile" => '',
									"IsRead" => '' 
							);
							$previousTimeStamp = $timestamp;
							if ($i == sizeof ( $result ) - 1)
								break;
						}
					}
					
					return $ReturnAry;
				}
			}
		} else if ($TargetGroup == "S") {       // school announcement
			global $special_feature, $plugin, $sys_custom;
			
			// $name_field = "'System Admin'";
			$name_field = getNameFieldWithClassNumberByLang("b."); // [IP2.5]
			
			if ($TargetUserID == "") {
				return 104;
			} else {
				$ThisUserID = $TargetUserID;
			}
			
			include_once("libuser.php");
			$lu = new libuser($ThisUserID);
			$UserLogin = $lu->UserLogin;
			$Groups = $lu->returnGroupIDs();
			
			$lu_parent = new libuser($CurrentUserID);
			$parentGroupIdAry = array();
			if ($lu_parent->isParent()) {
				$parentGroupIdAry = $lu_parent->returnGroupIDs();
				$Groups = array_values(array_unique(array_merge($Groups, $parentGroupIdAry)));
			}
			
			$sql = "SELECT DISTINCT AnnouncementID FROM INTRANET_GROUPANNOUNCEMENT";
			$AnnouncementIDs = $this->db_sub_select($sql);
			
			if (!$special_feature['sch_announcement_display_future']) {
				$check_shart_date = "CURDATE() >= a.AnnouncementDate AND";
			}
			$order = $special_feature['sch_announcement_display_sorting_asc'] ? "ASC" : "DESC";
			

			// [2016-0513-1440-15206] add checking for Setting - Allow users to view past news
			$check_end_date = $lschoolnews->allowUserToViewPastNews ? " 1 " : " CURDATE() <= a.EndDate ";
			$endDateCond = $lschoolnews->allowUserToViewPastNews ? " 1 " : " DATE(now()) <= DATE(a.EndDate) ";
			$conds = " AND a.AnnouncementID NOT IN ($AnnouncementIDs) AND a.RecordStatus = '1' AND (a.EndDate IS NULL OR (".$check_shart_date." ".$check_end_date."))";
			
			// JR20: main page
			// $start_date_sql = ($special_feature['portal']) ? format_date_no_year_sql("a.AnnouncementDate") : format_date_sql("a.AnnouncementDate");
			
			$fieldAry = array ();
			$fieldAry[] = "a.AnnouncementID";
			$fieldAry[] = "a.Title";
            $fieldAry[] = "a.TitleEng";
            $fieldAry[] = "a.Description";
            $fieldAry[] = "a.DescriptionEng";
            $fieldAry[] = "a.AnnouncementDate";
            $fieldAry[] = "a.EndDate";
			$fieldAry[] = "$name_field as AnnouncementUser";
			$fieldAry[] = "'S' as AnnouncementType";
			$fieldAry[] = "IF(a.Attachment IS NOT NULL, 'with attachment', '') as Attachment";
			// $fieldAry[] = "UNIX_TIMESTAMP(AnnouncementDate) as AnnouncementDateTs";
			$fieldAry[] = "UNIX_TIMESTAMP(AnnouncementDate)";
			$fieldAry[] = "a.ReadFlag";
			if ($plugin['power_voice']) {
				$fieldAry[] = "a.VoiceFile";
			}
			$fieldAry[] = "a.Attachment";
			$fieldAry[] = "a.OwnerGroupID";
			$fieldAry[] = "a.onTop";
			$fields = implode(', ', (array)$fieldAry);
			
			$sql = "SELECT 
						$fields
                	FROM 
                		INTRANET_ANNOUNCEMENT as a
                     	LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                     	LEFT OUTER JOIN INTRANET_GROUP as c ON a.OwnerGroupID = c.GroupID
                     WHERE 
						DATE(a.AnnouncementDate) <= DATE(now()) AND ".$endDateCond." 
						$conds
                     ORDER BY 
                     	a.onTop DESC, a.AnnouncementDate ".$order.", a.Title ASC
                    ";
			$result = $la->returnResultSet($sql);

			if (sizeof($Groups) > 0) {
				$row_groups = $la->returnGroupsAnnouncement_Web_Service($Groups, true);
			}
			
			if (sizeof($row_groups) > 0)
			{
				// $row_groups = $this->returnGroupsAnnouncement($Groups);
				$result = array_merge($result, $row_groups);
				
				
				## Get Portal Display School News
				## 2020-03-19 (Philips) [2019-1009-1539-56206]
				$skipGroupAnnouncement = ($sys_custom['eClassApp']['IncludePublicGroupAnnouncement']) ? false : true;
				$portalAnnouncementAry = $la->returnSchoolAnnouncement($CurrentUserID, $skipGroupAnnouncement);
				$portalAnnouncementIDAry = Get_Array_By_Key($portalAnnouncementAry,'AnnouncementID');
				if(!empty($portalAnnouncementIDAry)){
					foreach($result as &$row){
						$row['isPA'] = in_array($row['AnnouncementID'],$portalAnnouncementIDAry);
					}
				}
				################################# END
				// debug_r($row_school);

				// perform sorting to 2D-array
				if (!function_exists('customCompare'))
				{
					function customCompare($one, $two)
                    {
                    	global $special_feature;
                    	
                    	## 2020-03-19 (Philips) [2019-1009-1539-56206]
                    	# Portal Announcement on the top, if both not or yes, continue compare
                    	if($one['isPA'] && !$two['isPA']){
                    		return -1;
                    	} else if(!$one['isPA'] && $two['isPA']){
                    		return 1;
                    	}

						// [2019-1120-1536-38066] fix sorting problem due to group announcement without onTop
                        if(!isset($one['onTop'])) {
                            $one['onTop'] = 0;
                        }
                        if(!isset($two['onTop'])) {
                            $two['onTop'] = 0;
                        }

						if ($special_feature['sch_announcement_display_sorting_asc'])
						{
							if ($one['UNIX_TIMESTAMP(AnnouncementDate)'] < $two['UNIX_TIMESTAMP(AnnouncementDate)']) {
								return -1;
							}
							else if ($one['UNIX_TIMESTAMP(AnnouncementDate)'] == $two['UNIX_TIMESTAMP(AnnouncementDate)']) {
								if ((string)$one['Title'] < (string)$two['Title']) {
									return -1;
								}
							}
						}
						else
                        {
                            /*
						    if($one['onTop'] > $two['onTop'] && $one['EndDate'] > $two['EndDate']){
						        return -1;
						    } else {
        						if ($one['UNIX_TIMESTAMP(AnnouncementDate)'] > $two['UNIX_TIMESTAMP(AnnouncementDate)']) {
        							return -1;
        						} else if ($one['UNIX_TIMESTAMP(AnnouncementDate)'] == $two['UNIX_TIMESTAMP(AnnouncementDate)']) {
        							if ((string)$one['Title'] > (string)$two['Title']) {
        								return -1;
        							}
        						}
						    }
						    */

                            // [2019-1128-1046-36235] copy sorting logic from returnSchoolAnnouncement() in libannounce.php
                            // a.onTop DESC, a.AnnouncementDate DESC, a.AnnouncementID DESC
                            if($one['onTop'] > $two['onTop']) {
                                return -1;
                            }
                            else if ($one['onTop'] == $two['onTop']) {
                                if ($one['UNIX_TIMESTAMP(AnnouncementDate)'] > $two['UNIX_TIMESTAMP(AnnouncementDate)']) {
                                    return -1;
                                }
                                else if ($one['UNIX_TIMESTAMP(AnnouncementDate)'] == $two['UNIX_TIMESTAMP(AnnouncementDate)']) {
                                    if ($one['AnnouncementID'] > $two['AnnouncementID']) {
                                        return -1;
                                    }
                                }
                            }
						}
						
						return 1;
					}
				}
				usort($result, "customCompare");
			}
			$numOfAnnouncement = count($result);
			
			$ReturnAry = array();
			$ReturnAry['Announcement'] = array();
			for($i=0; $i<$numOfAnnouncement; $i++)
			{
				// B95546: temporary set maximum number of announcement to 500 due to android parent app bug
				if ($i > 499 && !$special_feature['sch_announcement_display_sorting_asc']) {
					break;
				}
				
				$announceId = $result[$i]['AnnouncementID'];
				$title = $libeClassApp->standardizePushMessageText(intranet_undo_htmlspecialchars($result[$i]['Title']));
                $titleEng = $libeClassApp->standardizePushMessageText(intranet_undo_htmlspecialchars($result[$i]['TitleEng']));
                $msg = $libeClassApp->standardizePushMessageText($result[$i]['Description']);
                $msgEng = $libeClassApp->standardizePushMessageText($result[$i]['DescriptionEng']);
                $dateStart = $result[$i]['AnnouncementDate'];
				$sender = $result[$i]['AnnouncementUser'];
				$type = $result[$i]['AnnouncementType'];
				$attachment_flag = $result[$i]['Attachment'];
				// $timestamp = $result[$i]['AnnouncementDateTs'];
				$timestamp = $result[$i]['UNIX_TIMESTAMP(AnnouncementDate)'];
				$readFlag = $result[$i]['ReadFlag'];
				$voiceFile = $result[$i]['VoiceFile'];
				$attachment_folder = $result[$i]['Attachment'];
				$ownerGroupId = $result[$i]['OwnerGroupID'];
				
				$targetAnnouncementUserId = $ThisUserID;
				if (in_array($ownerGroupId, (array)$parentGroupIdAry)) {
					$targetAnnouncementUserId = $CurrentUserID;
				}
				
				$AttachmentURL = "";
				if (trim($attachment_folder) != "") {
					$attachment_path = "$file_path/file/announcement/".$attachment_folder.$announceId;
					if (file_exists($attachment_path)) {
						$attachment_array = $lfs->return_files($attachment_path);
						if (sizeof($attachment_array) == 1) {
							$AttachmentURL = $school_domain."/home/download_attachment.php?target_e=".getEncryptedText($attachment_array[0]);
						} elseif (sizeof($attachment_array) > 1) {
							// list files for user to view one by one
							$Module = "Announcement";
							$AttachmentURL = $school_domain."/home/view_files.php?key=".EncryptModuleFilePars($Module, $UserLogin, $user_id, $announceId);
						}
					}
				}
				
				if ($voiceFile != "") {
					if ($AttachmentURL != "") {
						if (sizeof($attachment_array) == 1) {
							// list files for user to view one by one
							$Module = "Announcement";
							$AttachmentURL = $school_domain."/home/view_files.php?key=".EncryptModuleFilePars($Module, $UserLogin, $user_id, $announceId);
						}
					} else {
						$AttachmentURL = $school_domain."/home/download_attachment.php?target_e=".getEncryptedText($voiceFile);
					}
				}
				
				if ($readFlag == "") {
					$isRead = "N";
				} else {
					$tempAry = explode(';;', substr($readFlag, 1, -1));
					$isRead = (in_array($ThisUserID, $tempAry)) ? "Y" : "N";
				}
				
				if ($type == '') {
					$type = 'S';
				}
				
				$delimiter = "###";
				$keyStr = $special_feature['ParentApp_PrivateKey'].$delimiter.$announceId.$delimiter.$targetAnnouncementUserId.$delimiter.$CurrentUserID;
				$token = md5($keyStr);
				$signURL = $school_domain.'/api/announcement_view.php?token='.$token.'&AnnouncementID='.$announceId.'&StudentID='.$targetAnnouncementUserId.'&CurID='.$CurrentUserID;
				
				$tmpAry = array();
				$tmpAry['AnnouncementID'] = $announceId;
				if($sys_custom['SchoolNews']['DisplayEngTitleAndContent'] && $ParLang == "en" && $titleEng != "") {
				    $tmpAry['Title'] = $titleEng;
                } else {
                    $tmpAry['Title'] = $title;
                }
				$tmpAry['AnnouncementDate'] = $dateStart;
				$tmpAry['PosterName'] = $sender;
				$tmpAry['TargetGroup'] = $type;
				$tmpAry['ContentURL'] = $signURL;
				$ReturnAry['Announcement'][] = $tmpAry;
			}
			
			return $ReturnAry;
		} else {
			return 701;
		}
	}

	function WS_GetEvent($UserLogin, $NoOfRecord) {
		// retrieve UserID (student + active)
		$sql = "select UserID from INTRANET_USER where UserLogin='$UserLogin' and RecordType=2 and RecordStatus=1";
		$rs = $this->returnVector ( $sql );
		$ThisUserID = $rs ['0'];
		
		// check user exists
		if (! $ThisUserID)
			return 104;
		
		$today = date ( "Y-m-d" );
		$conds = " AND EventDate >= '$today' ";
		
		$sql1 = "SELECT DISTINCT a.EventID, a.EventDate as EventDate FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b, INTRANET_USERGROUP AS c WHERE c.UserID = " . $ThisUserID . " AND c.GroupID = b.GroupID AND b.EventID = a.EventID AND a.RecordStatus = '1' $conds ";
		$sql2 = "SELECT DISTINCT EventID, EventDate as EventDate FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType <>'2' $conds  ";
		$sql3 = "SELECT DISTINCT a.EventID, a.EventDate as EventDate FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID WHERE b.EventID IS NULL AND a.RecordType = '2' and a.RecordStatus='1' $conds ";
		
		$sql = $sql1 . " union " . $sql2 . " union " . $sql3;
		$sql .= " order by EventDate limit $NoOfRecord";
		$events = $this->returnArray ( $sql );
		
		$EventID_ary = array ();
		for($i = 0; $i < sizeof ( $events ); $i ++) {
			$EventID_ary [] = $events [$i] ['EventID'];
		}
		
		$ReturnContent ['Event'] = array ();
		
		if (sizeof ( $events )) {
			$EventID_str = implode ( ",", $EventID_ary );
			
			// # include last date event records
			$last_date = $events [sizeof ( $events ) - 1] ['EventDate'];
			// [IP2.5] get event venue according to campus location if user selected it rather than type the venue
			$sql1 = "SELECT DISTINCT a.EventID, a.EventDate as EventDate, a.RecordType, a.Title, a.Description, a.EventVenue, a.EventNature, EventLocationID FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b, INTRANET_USERGROUP AS c WHERE c.UserID = " . $ThisUserID . " AND c.GroupID = b.GroupID AND b.EventID = a.EventID AND a.RecordStatus = '1' and (a.EventID in ($EventID_str) or a.EventDate='$last_date') $conds ";
			$sql2 = "SELECT DISTINCT EventID, EventDate as EventDate, RecordType, Title, Description, EventVenue, EventNature, EventLocationID FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType <>'2' and (EventID in ($EventID_str) or EventDate='$last_date') $conds  ";
			$sql3 = "SELECT DISTINCT a.EventID, a.EventDate as EventDate, a.RecordType, a.Title, a.Description, a.EventVenue, a.EventNature, EventLocationID FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID WHERE b.EventID IS NULL AND a.RecordType = '2' and a.RecordStatus='1' and(a.EventID in ($EventID_str) or a.EventDate='$last_date') $conds ";
			$sql = $sql1 . " union " . $sql2 . " union " . $sql3;
			$sql .= "  order by EventDate";
			$events = $this->returnArray ( $sql );
			
			for($i = 0; $i < sizeof ( $events ); $i ++) {
				$event_data = array ();
				
				$event_data ['EventID'] = $events [$i] ['EventID'];
				$event_data ['Title'] = htmlspecialchars ( $events [$i] ['Title'] ); // [IP2.5] handle special characters
				$event_data ['Description'] = htmlspecialchars ( $events [$i] ['Description'] ); // [IP2.5] handle special characters
				                                                                         // $event_data['EventDate']=$events[$i]['EventDate'];
				                                                                         // $event_data['EventDate']= str_replace(" ","T",$events[$i]['EventDate']);
				$event_data ['EventDate'] = $events [$i] ['EventDate'];
				$event_data ['EventVenue'] = htmlspecialchars ( $events [$i] ['EventVenue'] );
				$event_data ['EventNature'] = htmlspecialchars ( $events [$i] ['EventNature'] );
				$event_data ['RecordType'] = $events [$i] ['RecordType'];
				
				// [IP2.5] get event venue according to campus location if user selected it rather than type the venue
				if (empty ( $events [$i] ['EventVenue'] ) && $events [$i] ['EventLocationID']) {
					include_once ("liblocation.php");
					$objRoom = new Room ( $events [$i] ['EventLocationID'] );
					$event_data ['EventVenue'] = htmlspecialchars ( $objRoom->BuildingName . " > " . $objRoom->FloorName . " > " . $objRoom->RoomName );
				}
				
				$ReturnContent ['Event'] [$i] = $event_data;
			}
		} else {
			// display <Event></Event> if result is empty
			// $ReturnContent['Event'][] = array();
			
			// return empty array if no records found
			$ReturnContent = array ();
		}
		return $ReturnContent;
	}
	function WS_GetPayment($UserLogin) {
		include_once ('libpayment.php');
		include_once ('form_class_manage.php');
		$lpayment = new libpayment ();
		
		$ReturnContent = array ();
		
		// retrieve UserID (student + active)
		$sql = "select UserID from INTRANET_USER where UserLogin='$UserLogin' and RecordType=2 and RecordStatus=1";
		$rs = $this->returnVector ( $sql );
		$ThisUserID = $rs ['0'];
		
		// check user exists
		if (! $ThisUserID)
			return 104;
			
		// check payment account exists
		$sql = "SELECT count(*) FROM PAYMENT_ACCOUNT WHERE StudentID = '$ThisUserID' ";
		$rs = $this->returnVector ( $sql );
		if ($rs [0] == 0)
			return 601;
			
			// retrieve Balance
		$balanceAry = $lpayment->checkBalance ( $ThisUserID );
		$balance = number_format ( $balanceAry ['Balance'], 2 );
		$balancedate = $balanceAry ['LastUpdated'];
		// $balancedate = str_replace(" ","T",$balanceAry['LastUpdated']);
		
		$lfcm = new form_class_manage ();
		$AcademicYearID = $lfcm->getCurrentAcademicaYearID ();
		$schoolyearObj = new academic_year ( $AcademicYearID );
		
		$year_start_date = $schoolyearObj->AcademicYearStart;
		$year_end_date = $schoolyearObj->AcademicYearEnd;
		
		if ($year_start_date != "" && $year_end_date != "") {
			// $sql_bound_by_date = " and UNIX_TIMESTAMP(a.StartDate)>=UNIX_TIMESTAMP('$year_start_date') AND UNIX_TIMESTAMP(a.StartDate)<=UNIX_TIMESTAMP('$year_end_date') ";
			$sql_bound_by_date = " and UNIX_TIMESTAMP(b.StartDate)>=UNIX_TIMESTAMP('$year_start_date') AND UNIX_TIMESTAMP(b.StartDate)<=UNIX_TIMESTAMP('$year_end_date') ";
		}
		
		// retrieve outstanding payment item
		$ary ['Payments'] = array ();
		
		$sql = "SELECT
		                b.Name, b.EndDate, a.Amount
		        FROM
		                PAYMENT_PAYMENT_ITEMSTUDENT AS a
		                LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM AS b ON a.ItemID = b.ItemID
		                LEFT OUTER JOIN INTRANET_USER AS c ON a.StudentID = c.UserID
		        WHERE
		                a.RecordStatus <> 1
		                and c.UserID = $ThisUserID $sql_bound_by_date
				ORDER BY b.EndDate                 
		                ";
		$data = $this->returnArray ( $sql );
		
		if (sizeof ( $data )) {
			for($i = 0; $i < sizeof ( $data ); $i ++) {
				$payment_item = array ();
				
				$payment_item ['PayItem'] = htmlspecialchars ( $data [$i] ['Name'] );
				$payment_item ['PayDueDate'] = $data [$i] ['EndDate'] . "T23:59:59";
				$payment_item ['Amount'] = $data [$i] ['Amount'];
				
				$ary ['Payments'] ['Payment'] [$i] = $payment_item;
			}
		} else {
			// display <Payments></Payments> if result is empty
			$ary ['Payments'] [] = array ();
		}
		
		$ReturnContent = array (
				'Balance' => $balance,
				'RetrievalDateTime' => $balancedate,
				'Payments' => $ary ['Payments'] 
		);
		
		return $ReturnContent;
	}
	function WS_GetPaymentForApp($TargetUserID) {
		global $sys_custom;
		include_once ('libpayment.php');
		$lpayment = new libpayment ();
		
		$ReturnContent = array ();
		
		// retrieve UserID (student + active)
		$sql = "select UserID from INTRANET_USER where UserID='$TargetUserID' and RecordType=2";
		$rs = $this->returnVector ( $sql );
		$ThisUserID = $rs ['0'];
		
		// check user exists
		if (! $ThisUserID)
			return 104;
			
			// check payment account exists
		$sql = "SELECT count(*) FROM PAYMENT_ACCOUNT WHERE StudentID = '$ThisUserID'";
		$rs = $this->returnVector ( $sql );
		
		$ReturnContent = array ();
		if ($rs [0] == 0) {
			// return 601;
			// no payment account
			$ReturnContent = array (
					'Balance' => '0.00',
					'BalanceRetrieveDate' => '0000-00-00 00:00:00',
					'OutstandingPaymentItems' => array (),
					'TransactionRecords' => array (),
					'KIS_OnlyShowPaymentRecords' => ($sys_custom ['ePayment'] ['KIS_OnlyShowPaymentRecords']) ? true : false 
			);
		} else {

			if($lpayment->isEWalletDirectPayEnabled()){
				$sql = "SELECT ParentID FROM INTRANET_PARENTRELATION WHERE StudentID='$ThisUserID'";
				$parent_id_ary = $this->returnVector($sql);
				if(count($parent_id_ary)>0){
					$this->syncPaymentDataToSchoolFromCentralServer(array('PayerUserID'=>$parent_id_ary));
				}
			}

			// retrieve Balance
			$balanceAry = $lpayment->checkBalance ( $ThisUserID );
			if ($balanceAry ['Balance']) {
				$balance = number_format ( $balanceAry ['Balance'], 2 );
			} else {
				$balance = number_format ( 0, 2 );
			}
			$balanceDate = $balanceAry ['LastUpdated'];
			
			$conds_endDate = '';
			//if ($sys_custom['ePayment']['TNG'] || $sys_custom['ePayment']['Alipay']) {
			if($lpayment->isEWalletDirectPayEnabled()) {
				$conds_endDate = " and DATE(now()) <= pi.EndDate ";
			}
			
			// retrieve unpaid item
			$sql = "SELECT
			                pis.PaymentID, pi.Name, pi.EndDate, pis.Amount, pi.ItemID
			        FROM
			                PAYMENT_PAYMENT_ITEMSTUDENT AS pis
			                INNER JOIN PAYMENT_PAYMENT_ITEM AS pi ON pis.ItemID = pi.ItemID
			        WHERE
			                pis.RecordStatus = 0
			                and pis.StudentID = '" . $ThisUserID . "'
			                and pi.NoticeID IS NULL
							and pi.StartDate <= DATE(now())
							$conds_endDate
					ORDER BY pi.EndDate
			                ";
			$unpaidItemAry = $this->returnResultSet ( $sql );
			$numOfUnpaidItem = count ( $unpaidItemAry );

			if($sys_custom['ePayment']['MultiPaymentGateway']) {
				$service_provider_list_rs = $lpayment->getPaymentServiceProviderMapping(false, true);
				$service_provider_list = array();
				foreach ($service_provider_list_rs as $temp => $value) {
					$service_provider_list[] = strtoupper($temp);
				}
			}

			$appPaymentItemsAry = array ();
			for($i = 0; $i < $numOfUnpaidItem; $i ++) {
				$_id = $unpaidItemAry [$i] ['PaymentID'];
				$_name = trim ( $unpaidItemAry [$i] ['Name'] );
				$_endDate = $unpaidItemAry [$i] ['EndDate'];
				$_amount = $unpaidItemAry [$i] ['Amount'];
				$_payment_gateway = array();
				if($sys_custom['ePayment']['MultiPaymentGateway']) {
					$multi_merchant_ids = $lpayment->returnPaymentItemMultiMerchantAccountID($unpaidItemAry [$i] ['ItemID']);
					if(count($multi_merchant_ids) > 0) {
						$merchant_accounts = $lpayment->getMerchantAccounts(array("AccountID" => $multi_merchant_ids));
						foreach ($merchant_accounts as $temp) {
							$sp_name = strtoupper($temp['ServiceProvider']);
							if(in_array($sp_name, $service_provider_list)) {
								$_payment_gateway[] = $sp_name;
							}
						}
					}
				}
				
				$_tmpAry = array ();
				$_tmpAry ['PaymentID'] = $_id;
				$_tmpAry ['ItemName'] = $_name;
				$_tmpAry ['PaymentDeadline'] = $_endDate;
				$_tmpAry ['Amount'] = $_amount;
				$_tmpAry ['PaymentGateway'] = $_payment_gateway;
				$appPaymentItemsAry [] = $_tmpAry;
			}
			
			// retrieve transaction records (from 90 days ago up to now)
			// B79887 - changed to retrieve current academic year records
			// $dateLimit = date('Y-m-d', strtotime('-3 month'));
			$sql = "SELECT
							potl.LogID, potl.TransactionTime, potl.TransactionType, potl.Amount, potl.BalanceAfter, potl.Details, a.RecordType
					FROM
							PAYMENT_OVERALL_TRANSACTION_LOG as potl
							LEFT JOIN PAYMENT_CREDIT_TRANSACTION a on potl.RelatedTransactionID=a.TransactionID AND potl.TransactionType=1
					WHERE
							potl.StudentID = '" . $ThisUserID . "'
							/* AND '$dateLimit' <= DATE(potl.TransactionTime) */
							AND DATE(potl.TransactionTime) <= NOW()
							AND DATE(potl.TransactionTime) >= '" . getStartDateOfAcademicYear ( Get_Current_Academic_Year_ID () ) . "'
					ORDER BY
							potl.TransactionTime desc
					";
			$transactionAry = $this->returnResultSet ( $sql );
			$numOfTransaction = count ( $transactionAry );
			
			$appTransactionAry = array ();
			for($i = 0; $i < $numOfTransaction; $i ++) {
				$_id = $transactionAry [$i] ['LogID'];
				$_transactionTime = $transactionAry [$i] ['TransactionTime'];
				$_transactionType = $transactionAry [$i] ['TransactionType'];
				$_amount = $transactionAry [$i] ['Amount'];
				$_balanceAfter = $transactionAry [$i] ['BalanceAfter'];
				$_details = trim ( $transactionAry [$i] ['Details'] );
				$_creditType = '';
				if($_transactionType == 1) {
					$_creditType = $transactionAry[$i]['RecordType'];
				}

				$_tmpAry = array ();
				$_tmpAry ['LogID'] = $_id;
				$_tmpAry ['Time'] = $_transactionTime;
				$_tmpAry ['Type'] = $_transactionType;
				$_tmpAry ['Name'] = $_details;
				$_tmpAry ['Amount'] = $_amount;
				$_tmpAry ['BalanceAfter'] = $_balanceAfter;
				$_tmpAry['CreditType'] = $_creditType;
				$appTransactionAry [] = $_tmpAry;
			}
			
			$ReturnContent = array (
					'Balance' => $balance,
					'BalanceRetrieveDate' => $balanceDate,
					'OutstandingPaymentItems' => $appPaymentItemsAry,
					'TransactionRecords' => $appTransactionAry,
					'KIS_OnlyShowPaymentRecords' => ($sys_custom ['ePayment'] ['KIS_OnlyShowPaymentRecords']) ? true : false,
					'EnableTopUpInApp' => $lpayment->Settings['EnableTopUpInApp']? true : false 
			);
		}
		
		return $ReturnContent;
	}
	function WS_GetNotice($UserLogin, $NoOfRecords) {
		global $file_path, $intranet_root, $special_feature, $SessionID, $plugin;
		
		$ReturnAry = array ();
		
		if ($NoOfRecords == '') {
			// get current and future year notice
			$conds_startDate = " AND DATE(a.DateStart) >= '" . getStartDateOfAcademicYear ( Get_Current_Academic_Year_ID () ) . "' ";
			
			$NoOfRecords = 9999999;
		}
		
		if ($UserLogin != "") { // retrieve student id by login (only return approved student account
			$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='" . $this->Get_Safe_Sql_Query ( $UserLogin ) . "' AND RecordStatus=1 AND RecordType=2";
			$result = $this->returnVector ( $sql );
			
			if ($result [0] == "") {
				return 104;
			} else {
				$ThisUserID = $result [0];
			}
			
			include_once ("libnotice.php");
			$lnotice = new libnotice ();
			
			/*
			 * $sql = "SELECT
			 * a.NoticeID, a.NoticeNumber, a.Title, a.Description, a.DateStart, a.DateEnd, IF(b.RecordStatus=2,'Y','N'), a.Module, UNIX_TIMESTAMP(a.DateStart), a.Attachment, b.StudentID
			 * FROM
			 * INTRANET_NOTICE_REPLY b LEFT JOIN INTRANET_NOTICE a
			 * ON (a.NoticeID=b.NoticeID)
			 * WHERE
			 * (a.Module IN ('DISCIPLINE', 'Payment') OR a.Module IS NULL)
			 * AND a.IsDeleted=0 AND b.StudentID='$ThisUserID' AND a.RecordStatus = 1
			 *
			 * ORDER BY a.DateStart DESC, a.NoticeID DESC";
			 */
			$sql = "SELECT 
						a.NoticeID, a.NoticeNumber, a.Title, a.Description, a.DateStart, a.DateEnd, IF(b.RecordStatus=2,'Y','N'), a.Module, UNIX_TIMESTAMP(a.DateStart), a.Attachment, b.StudentID
					FROM 
						INTRANET_NOTICE a INNER JOIN INTRANET_NOTICE_REPLY b  
							ON (a.NoticeID=b.NoticeID AND b.StudentID='$ThisUserID')
					WHERE 
						(a.Module IN ('DISCIPLINE', 'Payment') OR a.Module IS NULL)
						AND a.IsDeleted=0 AND unix_timestamp(a.DateStart)<=unix_timestamp(now()) AND a.RecordStatus=1
						$conds_startDate
					ORDER BY a.DateStart DESC, a.NoticeID DESC";
			$result = $lnotice->returnArray ( $sql );
			
			$terminate = 0;
			$previousTimeStamp = "";
			
			if (sizeof ( $result ) == 0) {
				// $ReturnAry['Notice'][] = array();
				
				// return empty array if no records found
				$ReturnAry = array ();
			} else {
				
				// $school_url = split("\n",get_file_content("$intranet_root/file/email_website.txt"));
				$school_url = explode ( "\n", get_file_content ( "$intranet_root/file/email_website.txt" ) );
				$school_domain = $school_url [0];
				
				if (substr ( $school_domain, - 1 ) == "/")
					$school_domain = substr ( $school_domain, 0, - 1 );
				
				for($i = 0; $i < $NoOfRecords || $terminate == 0; $i ++) {
					$attachment = "";
					list ( $noticeId, $noticeNumber, $title, $description, $dateStart, $dateEnd, $recordstatus, $module, $timestamp, $attachment, $studentId ) = $result [$i];
					
					if ($i >= $NoOfRecords && $previousTimeStamp > $timestamp) {
						$terminate = 1;
						break;
					}
					
					// if(file_exists("$file_path/file/notice/".$attachment)) {
					// $description .= " [with attachment]";
					// }
					// prepare token for verification
					$delimiter = "###";
					$sql_usr = "SELECT RecordType, UserID FROM INTRANET_USER where SessionKey='$SessionID'";
					$row_usr = $this->returnArray ( $sql_usr, null, true );
					if (sizeof ( $row_usr ) > 0) {
						$CurrentUserID = $row_usr [0] ["UserID"];
						/*
						 * if ($row_usr[0]["RecordType"]==3)
						 * $CurrentUserType = "P";
						 * elseif ($row_usr[0]["RecordType"]==2)
						 * $CurrentUserType = "S";
						 * elseif ($row_usr[0]["RecordType"]==1)
						 * $CurrentUserType = "T";
						 */
					}
					$keyStr = $special_feature ['ParentApp_PrivateKey'] . $delimiter . $noticeId . $delimiter . $studentId . $delimiter . $CurrentUserID;
					$token = md5 ( $keyStr );
					
					$signURL = $school_domain . '/api/enotice_sign.php?token=' . $token . '&NoticeID=' . $noticeId . '&StudentID=' . $studentId . '&CurID=' . $CurrentUserID;
					$signURL = htmlspecialchars ( $signURL );
					$title = htmlspecialchars ( $title );
					$description = htmlspecialchars ( $description );
					
					$ReturnAry ['Notice'] [] = array (
							"NoticeID" => $noticeId,
							"NoticeNumber" => $noticeNumber,
							"Title" => $title,
							// "Description" => $this->xmlspecialchars(nl2br($description)),
							"Description" => $description,
							// "DateStart" => str_replace(' ', 'T', $dateStart),
							"DateStart" => $dateStart,
							// "DateEnd" => str_replace(' ', 'T', $dateEnd),
							"DateEnd" => $dateEnd,
							"RecordStatus" => $recordstatus,
							// "SignURL" => $school_domain.'/api/enotice_sign.php?token=xkljsdlf098324092342432&NoticeID='.$noticeId.'&StudentID='.$studentId,
							"SignURL" => $signURL,
							"Module" => $module 
					);
					
					$previousTimeStamp = $timestamp;
					if ($i == sizeof ( $result ) - 1)
						break;
				}
			}
			
			return $ReturnAry;
		} else {
			return 104;
		}
	}
	function WS_GetNoticeForApp($TargetUserID, $CurrentUserID) {
		global $file_path, $intranet_root, $special_feature, $SessionID, $plugin;
		
		$ReturnAry = array ();
		$ReturnAry ['Notice'] = array ();
		
		if ($TargetUserID == '') {
			return 104;
		} else { // retrieve student id by login (only return approved student account
		       // $sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='".$this->Get_Safe_Sql_Query($UserLogin)."' AND RecordStatus=1 AND RecordType=2";
		       // $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordStatus=1 AND RecordType=2 And UserID = '".$TargetUserID."' ";
			$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType=2 And UserID = '" . $TargetUserID . "' ";
			$result = $this->returnVector ( $sql );
			
			if ($result [0] == "") {
				return 104;
			} else {
				$ThisUserID = $result [0];
			}
			
			include_once ("libnotice.php");
			$lnotice = new libnotice ();
			include_once ('libpayment.php');
			$lpayment = new libpayment ();

			if($lpayment->isEWalletDirectPayEnabled()){
				$sql = "SELECT ParentID FROM INTRANET_PARENTRELATION WHERE StudentID='$ThisUserID'";
				$parent_id_ary = $this->returnVector($sql);
				if(count($parent_id_ary)>0){
					$this->syncPaymentDataToSchoolFromCentralServer(array('PayerUserID'=>$parent_id_ary));
				}
			}

			$sql = "SELECT 
						a.NoticeID, 
						a.NoticeNumber, 
						a.Title, 
						a.Description, 
						a.DateStart, 
						a.DateEnd, 
						IF(b.RecordStatus=2,'Y','N'), 
						a.Module, 
						UNIX_TIMESTAMP(a.DateStart), 
						a.Attachment, 
						b.StudentID,
						a.TargetType
					FROM 
						INTRANET_NOTICE a INNER JOIN INTRANET_NOTICE_REPLY b  
							ON (a.NoticeID=b.NoticeID AND b.StudentID='$ThisUserID')
					WHERE 
						(a.Module IN ('DISCIPLINE', 'Payment', 'STUDENTATTENDANCE', 'eEnrolment') OR a.Module IS NULL)
						AND a.IsDeleted=0 AND unix_timestamp(a.DateStart)<=unix_timestamp(now()) AND a.RecordStatus=1
						AND a.DateEnd >= '" . getStartDateOfAcademicYear ( Get_Current_Academic_Year_ID () ) . "'
					GROUP BY a.NoticeID
					ORDER BY a.DateStart DESC, a.NoticeID DESC";
			$result = $lnotice->returnArray ( $sql );
			$numOfNotice = count ( $result );
			
			if ($numOfNotice == 0) {
				// return empty array if no records found
				// $ReturnAry = array();
			} else {
				$allowLateSigning = ($lnotice->isLateSignAllow) ? 'Y' : 'N';
				
				// $school_url = split("\n",get_file_content("$intranet_root/file/email_website.txt"));
				// $school_domain = $school_url[0];
				$school_domain = curPageURL ( $withQueryString = false, $withPageSuffix = false );
				
				if (substr ( $school_domain, - 1 ) == "/")
					$school_domain = substr ( $school_domain, 0, - 1 );
				
				for($i = 0; $i < $numOfNotice; $i ++) {
					$attachment = "";
					list($noticeId, $noticeNumber, $title, $description, $dateStart, $dateEnd, $recordstatus, $module, $timestamp, $attachment, $studentId, $targetType) = $result[$i];
					
					if ($module == null) {
						$module = '';
					}
					
					// prepare token for verification
					$delimiter = "###";
					$keyStr = $special_feature ['ParentApp_PrivateKey'] . $delimiter . $noticeId . $delimiter . $studentId . $delimiter . $CurrentUserID;
					$token = md5 ( $keyStr );
					$signURL = $school_domain . '/api/enotice_sign_eclassApp.php?token=' . $token . '&NoticeID=' . $noticeId . '&StudentID=' . $studentId . '&CurID=' . $CurrentUserID;
					
					$ReturnAry ['Notice'] [] = array (
							"NoticeID" => $noticeId,
							"NoticeNumber" => $noticeNumber,
							"Title" => $title,
							"Description" => '',
							"DateStart" => $dateStart,
							"DateEnd" => $dateEnd,
							"RecordStatus" => $recordstatus,
							"SignURL" => $signURL,
							"Module" => $module,
							"AllowLateSigning" => $allowLateSigning,
							"TargetType" => $targetType 
					);
				}
			}
			
			return $ReturnAry;
		}
	}
	function WS_GetNoticeUrlForApp($TargetUserID, $CurrentUserID, $noticeId, $Module) {
		global $intranet_root, $special_feature, $SessionID, $plugin;
		$ReturnAry = array ();
		
		if ($TargetUserID == '' || $TargetUserID == '' || $noticeId == '') {
			return 104;
		} else {
			$studentId = $TargetUserID;
			$school_domain = curPageURL ( $withQueryString = false, $withPageSuffix = false );
			
			if (substr ( $school_domain, - 1 ) == "/")
				$school_domain = substr ( $school_domain, 0, - 1 );
				
				// prepare token for verification
			$delimiter = "###";
			$keyStr = $special_feature ['ParentApp_PrivateKey'] . $delimiter . $noticeId . $delimiter . $studentId . $delimiter . $CurrentUserID;
			$token = md5 ( $keyStr );
			
	        if($Module == "SchoolNews"){
	        	$signURL = $school_domain . '/api/announcement_view.php?token=' . $token . '&AnnouncementID=' . $noticeId . '&StudentID=' . $studentId . '&CurID=' . $CurrentUserID;	        	
	        }else if($Module == "eNotice"){
	        	 $signURL = $school_domain . '/api/enotice_sign_eclassApp.php?token=' . $token . '&NoticeID=' . $noticeId . '&StudentID=' . $studentId . '&CurID=' . $CurrentUserID;
	        }else if($Module == "eCircular"){
	        	// prepare token for verification
				$keyStr = $special_feature ['ParentApp_PrivateKey'] . $delimiter . $noticeId . $delimiter . $CurrentUserID;
				$token = md5 ( $keyStr );
				$signURL = $school_domain . '/api/ecircular_sign_eclassApp.php?token=' . $token . '&CircularID=' . $noticeId . '&CurUserID=' . $CurrentUserID;				
	        }else{
	        	 //eNotice
	        	 $signURL = $school_domain . '/api/enotice_sign_eclassApp.php?token=' . $token . '&NoticeID=' . $noticeId . '&StudentID=' . $studentId . '&CurID=' . $CurrentUserID;
	        }
			$ReturnAry ['signUrl'] = $signURL;
		}
		
		return $ReturnAry;
	}
	function xmlspecialchars($text) {
		return str_replace ( '&#039;', '&apos;', htmlspecialchars ( $text, ENT_QUOTES ) );
	}
	function WS_GetEvent_TEMP($ThisUserID, $Name) {
		$ReturnContent = array ();
		// $ReturnContent['UserID'] = $ThisUserID;
		// return 104;
		$ReturnContent ['Events'] [] = array (
				'eventID' => '1',
				'eventName' => 'football22' 
		);
		return $ReturnContent;
	}
	function GetStudentInfo_Common($ThisUserIDList) {
		global $intranet_root, $plugin, $jr_image_path, $ischoolbag_website;
		
		// include_once('libthumbnail.php');
		include_once ('libfilesystem.php');
		include_once ("libimage.php");
		
		$fs = new libfilesystem ();
		
		$image_obj = new SimpleImage ();
		
		// $school_data = split("\n",get_file_content("$intranet_root/file/school_data.txt"));
		$school_data = explode ( "\n", get_file_content ( "$intranet_root/file/school_data.txt" ) );
		$school_name = $school_data [0];
		$school_org = $school_data [1];
		$school_name = htmlspecialchars ( $school_name );
		$school_org = htmlspecialchars ( $school_org );
		
		$IsSchoolImageReturned = false;
		
		$ThisUserIDStr = implode ( ',', $ThisUserIDList );
		
		$sql = "SELECT u.UserID, u.UserLogin, u.UserEmail, u.EnglishName, u.ChineseName, u.ClassNumber, u.ClassName, u.PhotoLink	             
	             FROM INTRANET_USER u 
	             WHERE u.UserID IN ($ThisUserIDStr)";
		$row = $this->returnArray ( $sql );
		
		$cardTable = 'CARD_STUDENT_DAILY_LOG_' . date ( "Y" ) . '_' . date ( "m" );
		$students = array ();
		foreach ( $row as $s ) {
			$student = array ();
			$student ['UserID'] = $s ['UserID'];
			$student ['UserEmail'] = $s ['UserEmail'];
			$student ['UserName'] = $s ['UserLogin'];
			$student ['EnglishName'] = $s ['EnglishName'];
			$student ['ChineseName'] = $s ['ChineseName'];
			$student ['SchoolName'] = $school_name;
			$student ['SchoolOrg'] = $school_org;
			$student ['ClassName'] = $s ['ClassName'];
			$student ['ClassNo'] = $s ['ClassNumber'];
			
			$student ['SchoolType'] = 'SEC'; // [IP2.5] for secondary school
			
			if (is_file ( $intranet_root . $s ['PhotoLink'] )) {
				$photo_link = $s ['PhotoLink'];
			} elseif (is_file ( $intranet_root . "/file/user_photo/" . $s ['UserLogin'] . ".jpg" )) {
				$photo_link = "/file/user_photo/" . $s ['UserLogin'] . ".jpg";
			} elseif (is_file ( $intranet_root . "/file/photo/personal/p" . $student ['UserID'] . ".jpg" )) {
				$photo_link = "/file/photo/personal/p" . $student ['UserID'] . ".jpg";
			}
			
			if (is_file ( $intranet_root . $photo_link )) {
				$photo_link_thumbnail = dirname ( $photo_link ) . '/thumbnail/' . basename ( $photo_link );
				$photo_link_local = $intranet_root . $photo_link;
				$photo_link_local_thumbnail = dirname ( $photo_link_local ) . '/thumbnail/' . basename ( $photo_link_local );
				
				if (! file_exists ( $photo_link_local_thumbnail ) or filemtime ( $photo_link_local_thumbnail ) < filemtime ( $photo_link_local )) {
					$fs->createFolder ( dirname ( $photo_link_local ) . '/thumbnail/' );
					/*
					 * $thumb=new thumbnail($photo_link_local); // generate image_file, set filename to resize
					 * $thumb->size_width(200); // set width for thumbnail, or
					 * $thumb->size_height(260); // set height for thumbnail, or
					 * //$thumb->size_auto(130); // set the biggest width or height for thumbnail
					 * $thumb->jpeg_quality(75); // [OPTIONAL] set quality for jpeg only (0 - 100) (worst - best), default = 75
					 * //$thumb->show(); // show your thumbnail
					 * $thumb->save($photo_link_local_thumbnail);
					 */
					
					@$image_obj->load ( $photo_link_local );
					@$image_obj->resizeToMax ( 200, 260 );
					@$image_obj->save ( $photo_link_local_thumbnail );
				}
				
				$photo_link = $photo_link_thumbnail;
				$photo_link_local = $photo_link_local_thumbnail;
				
				if (file_exists ( $photo_link_local )) {
					$student ['StudentPhoto'] = base64_encode ( file_get_contents ( $photo_link_local ) );
				} else {
					// loading original large file content can crash app in Andriod!!!
					// $student['StudentPhoto'] = base64_encode(file_get_contents($intranet_root. $s['PhotoLink']));
					
					$photo_link = "$jr_image_path/class_directory/sample_photo.gif";
					$photo_link_local = $intranet_root . $photo_link;
					
					// # 2011-07-28: empty when using default photo
					$student ['StudentPhoto'] = '';
				}
			} else {
				$photo_link = "$jr_image_path/class_directory/sample_photo.gif";
				$photo_link_local = $intranet_root . $photo_link;
				
				// # 2011-07-28: empty when using default photo
				$student ['StudentPhoto'] = '';
			}
			
			// $student['StudentPhotoLink'] = $ischoolbag_website.$photo_link;
			
			if ($plugin ['attendancestudent']) {
				$sql = "SELECT InSchoolTime, LeaveSchoolTime, AMStatus, PMStatus, LeaveStatus, LunchBackTime FROM $cardTable WHERE DayNumber = " . date ( 'd' ) . " and UserID = '" . $s ['UserID']."'";
				$record = $this->returnArray ( $sql );
				
				/*
				 * AMStatus, PMStatus, LeaveStatus, RecordStatus
				 *
				 * P: Present (On time)
				 * A: Absent
				 * T: Late
				 * O: Outing
				 * L: Left school
				 */
				if (count ( $record ) > 0) {
					/*
					 * if($record[0]['InSchoolTime'] != '' and $record[0]['LeaveSchoolTime'] == ''){
					 * $student['Status'] = 'I';
					 * $student['SignedDateTime'] = date('Y-m-d').' '.$record[0]['InSchoolTime'];
					 * }
					 * elseif($record[0]['InSchoolTime'] != '' and $record[0]['LeaveSchoolTime'] != ''){
					 * $student['Status'] = 'O';
					 * $student['SignedDateTime'] = date('Y-m-d').' '.$record[0]['LeaveSchoolTime'];
					 * }
					 * elseif($record[0]['InSchoolTime'] == '' and $record[0]['LeaveSchoolTime'] != ''){
					 * $student['Status'] = 'O';
					 * $student['SignedDateTime'] = date('Y-m-d').' '.$record[0]['LeaveSchoolTime'];
					 * }
					 * elseif($record[0]['InSchoolTime'] == '' and $record[0]['LeaveSchoolTime'] == ''){
					 * $student['Status'] = '';
					 * $student['SignedDateTime'] = '';
					 * }
					 */
					if ($record [0] ['LeaveStatus'] != '') {
						$student ['Status'] = "L";
						$student ['SignedDateTime'] = date ( 'Y-m-d' ) . ' ' . $record [0] ['LeaveSchoolTime'];
					} elseif (! is_null ( $record [0] ['PMStatus'] )) { // follow PM status first
						switch ($record [0] ['PMStatus']) {
							case "0" :
								$student ['Status'] = "P";
								$tiemDisplay = ($record [0] ['LunchBackTime'] != "" && $record [0] ['LunchBackTime'] > $record [0] ['InSchoolTime']) ? $record [0] ['LunchBackTime'] : $record [0] ['InSchoolTime'];
								$student ['SignedDateTime'] = date ( 'Y-m-d' ) . ' ' . $tiemDisplay;
								break;
							case "1" :
								$student ['Status'] = "A";
								$student ['SignedDateTime'] = '';
								break;
							case "2" :
								$student ['Status'] = "T";
								$tiemDisplay = ($record [0] ['LunchBackTime'] != "" && $record [0] ['LunchBackTime'] > $record [0] ['InSchoolTime']) ? $record [0] ['LunchBackTime'] : $record [0] ['InSchoolTime'];
								$student ['SignedDateTime'] = date ( 'Y-m-d' ) . ' ' . $tiemDisplay;
								break;
							case "3" :
								$student ['Status'] = "O";
								$student ['SignedDateTime'] = date ( 'Y-m-d' ) . ' ' . $record [0] ['LeaveSchoolTime'];
								break;
							default :
								$student ['Status'] = 'A';
								$student ['SignedDateTime'] = '';
						}
					} elseif (! is_null ( $record [0] ['AMStatus'] )) { // the follow AM status
						switch ($record [0] ['AMStatus']) {
							case "0" :
								$student ['Status'] = "P";
								$student ['SignedDateTime'] = date ( 'Y-m-d' ) . ' ' . $record [0] ['InSchoolTime'];
								break;
							case "1" :
								$student ['Status'] = "A";
								$student ['SignedDateTime'] = '';
								break;
							case "2" :
								$student ['Status'] = "T";
								$student ['SignedDateTime'] = date ( 'Y-m-d' ) . ' ' . $record [0] ['InSchoolTime'];
								break;
							case "3" :
								$student ['Status'] = "O";
								$student ['SignedDateTime'] = date ( 'Y-m-d' ) . ' ' . $record [0] ['LeaveSchoolTime'];
								break;
							default :
								$student ['Status'] = 'A';
								$student ['SignedDateTime'] = '';
						}
					} else {
						// default should be empty if there is no record
						$student ['Status'] = '';
						$student ['SignedDateTime'] = '';
					}
				} else {
					$student ['Status'] = '';
					$student ['SignedDateTime'] = '';
				}
			} else {
				$student ['Status'] = '';
				$student ['SignedDateTime'] = '';
			}
			// school badge (optional)
			$school_badge_jpg = "$intranet_root/file/parent_app.jpg";
			if (! $IsSchoolImageReturned && file_exists ( $school_badge_jpg )) {
				$student ['SchoolImage'] = base64_encode ( @file_get_contents ( $school_badge_jpg ) );
				$IsSchoolImageReturned = true;
			} else {
				$student ['SchoolImage'] = "";
			}
			
			$students [] = $student;
		}
		$ReturnContent ['Student'] = $students;
		return $ReturnContent;
	}
	function GetStudentInfo_eClassApp($ThisUserIDList) {
		global $intranet_root, $plugin, $eclassAppConfig,$SettingList;
		
		include_once ($intranet_root . '/includes/eClassApp/libeClassApp.php');
		$leClassApp = new libeClassApp ();
		
		// $school_data = split("\n",get_file_content("$intranet_root/file/school_data.txt"));
		$school_data = explode ( "\n", get_file_content ( "$intranet_root/file/school_data.txt" ) );
		$school_name = $school_data [0];
		$school_org = $school_data [1];
		$school_name = htmlspecialchars ( $school_name );
		$school_org = htmlspecialchars ( $school_org );
		
		// $IsSchoolImageReturned = false;
		
		$ThisUserIDStr = implode ( ',', $ThisUserIDList );
		$sql = "SELECT 
						u.UserID as IntranetUserID, 
						u.UserLogin, 
						u.EnglishName, 
						u.ChineseName,
						'' as ClassNumber,
						'' as ClassNameEn,
						'' as ClassNameCh,
						'' as OfficalPhotoPath,
						WebSAMSRegNo
	             FROM 
						INTRANET_USER as u
	             WHERE 
						u.UserID IN ($ThisUserIDStr)";
		$studentInfoAry = $this->returnResultSet ( $sql );
		$numOfStudent = count ( $studentInfoAry );
		
		$sql = "Select
						ycu.UserID,
						yc.ClassTitleEN as ClassNameEn,
						yc.ClassTitleB5 as ClassNameCh,
						yc.YearID,
						yc.YearClassID,
						ycu.ClassNumber
				From
						YEAR_CLASS as yc
						Inner Join YEAR_CLASS_USER as ycu On (yc.YearClassID = ycu.YearClassID)
				Where
						yc.AcademicYearID = '" . Get_Current_Academic_Year_ID () . "'
						And ycu.UserID IN ($ThisUserIDStr)
				";
		$studentClassAry = $this->returnResultSet ( $sql );
		$studentClassAssoAry = BuildMultiKeyAssoc ( $studentClassAry, 'UserID', $IncludedDBField = array (), $SingleValue = 0, $BuildNumericArray = 0 );
		
		// # Get form-based app access right settings
		// $accessRightAssoAry = $leClassApp->getAccessRightInfo($eclassAppConfig['appType']['Parent']);
		
		for($i = 0; $i < $numOfStudent; $i ++) {
			$_userId = $studentInfoAry [$i] ['IntranetUserID'];
			$_userLogin = $studentInfoAry [$i] ['UserLogin'];
			$_formId = $studentClassAssoAry [$_userId] ['YearID'];
			
			// ## websams reg no
			if ($studentInfoAry [$i] ['WebSAMSRegNo'] == null) {
				$studentInfoAry [$i] ['WebSAMSRegNo'] = '';
			}
			
			// ## chinese name
			if ($studentInfoAry [$i] ['ChineseName']) {
                // [2020-0806-1120-10235] html_entity_decode() - charset set to 'UTF-8'
                //$studentInfoAry [$i] ['ChineseName'] = html_entity_decode($studentInfoAry [$i] ['ChineseName']);
				$studentInfoAry [$i] ['ChineseName'] = html_entity_decode($studentInfoAry [$i] ['ChineseName'], ENT_COMPAT | ENT_HTML401, 'UTF-8');
			}
			else{
				$studentInfoAry [$i] ['ChineseName'] = $studentInfoAry [$i] ['EnglishName'];
			}
			
			// ## student class
			$studentInfoAry [$i] ['ClassNameEn'] = ($studentClassAssoAry [$_userId] ['ClassNameEn'] == null) ? '' : $studentClassAssoAry [$_userId] ['ClassNameEn'];
			$studentInfoAry [$i] ['ClassNameCh'] = ($studentClassAssoAry [$_userId] ['ClassNameCh'] == null) ? '' : $studentClassAssoAry [$_userId] ['ClassNameCh'];
			$studentInfoAry [$i] ['ClassNumber'] = ($studentClassAssoAry [$_userId] ['ClassNumber'] == null) ? 0 : $studentClassAssoAry [$_userId] ['ClassNumber'];
			
			// ## student photo
			$_officalPhotoFilename = $_userLogin . ".jpg";
			$_officialPhotoLink = "/file/user_photo/" . $_officalPhotoFilename;
			$_officialPhotoFilePath = $intranet_root . $_officialPhotoLink;
			
			$_personalPhotoFilename = "p" . $_userId . ".jpg";
			$_personalPhotoLink = "/file/photo/personal/" . $_personalPhotoFilename;
			$_personalPhotoFilePath = $intranet_root . $_personalPhotoLink;
			
			if (file_exists ( $_officialPhotoFilePath )) {
				// show official photo if exists
				$studentInfoAry [$i] ['OfficalPhotoPath'] = $_officialPhotoLink;
			} else if (file_exists ( $_personalPhotoFilePath )) {
				// show personal photo if there is no official photo
				$studentInfoAry [$i] ['OfficalPhotoPath'] = $_personalPhotoLink;
			} else {
				// show default photo if there are no official photo and personal photo
//				$studentInfoAry [$i] ['OfficalPhotoPath'] = "/images/myaccount_personalinfo/samplephoto.gif";
				
				include_once ($intranet_root . '/includes/libuser.php');
				$luser = new libuser();
				$studentInfoAry [$i] ['OfficalPhotoPath'] = $luser->returnDefaultOfficialPhotoPath();
			}

            //for add AM PM group info 1 am, 2 pm, 0 whole day
            include_once("$intranet_root/includes/form_class_manage.php");
// 				$libYear = new Year();
// 				$YearClassGroupInfo = $libYear->Get_Class_Group_Info($YearClassInfo['YearClassID']);
            $libYearClass = new year_class();
            $YearClassGroupInfo = $libYearClass->Get_Class_Group_Info($studentClassAssoAry [$_userId] ['YearClassID']);
            if($YearClassGroupInfo){
                if($YearClassGroupInfo[0]['AttendanceType']){
                    $studentInfoAry [$i] ['ClassAttendanceType'] = $YearClassGroupInfo[0]['AttendanceType'];//1 am, 2 pm, 3 whole day
                    if($studentInfoAry [$i] ['ClassAttendanceType'] == 3){
                        $studentInfoAry [$i] ['ClassAttendanceType'] = 0;
                    }
                }else{
                    include_once("$intranet_root/includes/libgeneralsettings.php");

                    $GeneralSetting = new libgeneralsettings();
                    $Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
                    $attendance_mode = $Settings['AttendanceMode'];

                    if($attendance_mode==0){
                        $studentInfoAry [$i] ['ClassAttendanceType'] = 1;
                    }else if($attendance_mode==1){
                        $studentInfoAry [$i] ['ClassAttendanceType'] = 2;
                    }else{
                        $studentInfoAry [$i] ['ClassAttendanceType'] = 0;
                    }
                }
            }
			
			// ## Form-based access right
			// if (is_array($accessRightAssoAry[$_formId])) {
			// $studentInfoAry[$i]['ModuleAccessRight'] = $accessRightAssoAry[$_formId];
			// }
			// else {
			// $studentInfoAry[$i]['ModuleAccessRight'] = null;
			// }
			$studentInfoAry [$i] ['ModuleAccessRight'] = $leClassApp->getUserModuleAccessRight ( $_userId );
			if (count ( $studentInfoAry [$i] ['ModuleAccessRight'] ) == 0) {
				$studentInfoAry [$i] ['ModuleAccessRight'] = null;
			}
		}
		
		$ReturnContent ['Student'] = $studentInfoAry;
		return $ReturnContent;
	}
	function WS_GetStudentInfo($ThisUserID, $UserName) {
		if ($ThisUserID == '' and $UserName != '') {
			$sql = 'SELECT UserID FROM INTRANET_USER WHERE UserLogin = \'' . $UserName . '\'';
			$row = $this->returnVector ( $sql );
			$ThisUserID = $row [0];
		}
		
		return $this->GetStudentInfo_Common ( array (
				$ThisUserID 
		) );
	}
	function WS_GetChildrenList($ThisUserID, $UserName) {
		global $plugin;
		
		if ($plugin ['eClassApp']) {
			return array ();
		} else {
			if ($ThisUserID != '')
				$condition = 'r.ParentID = ' . $ThisUserID;
			elseif ($UserName != '')
				$condition = 'u2.UserLogin = \'' . $UserName . '\'';
			else
				$condition = '1=0';
			
			$sql = "SELECT u.UserID, u.UserEmail, u.EnglishName, u.ChineseName, u.ClassNumber, u.ClassName, u.PhotoLink
		             FROM INTRANET_PARENTRELATION r
		             INNER JOIN INTRANET_USER u ON r.StudentID = u.UserID
		             INNER JOIN INTRANET_USER u2 ON r.ParentID = u2.UserID 
		             WHERE $condition";
			
			if ($ThisUserID != '')
				$condition2 = 'u3.RecordType = 2 AND u3.UserID = ' . $ThisUserID;
			elseif ($UserName != '')
				$condition2 = 'u3.RecordType = 2 AND u3.UserLogin = \'' . $UserName . '\'';
			else
				$condition2 = '1=0';
			
			$sql .= " UNION SELECT u3.UserID, u3.UserEmail, u3.EnglishName, u3.ChineseName, u3.ClassNumber, u3.ClassName, u3.PhotoLink
		    				FROM INTRANET_USER u3 WHERE $condition2";
			
			$row = $this->returnArray ( $sql );
			
			$studentList = array ();
			foreach ( $row as $s ) {
				$studentList [] = $s ['UserID'];
			}
			return $this->GetStudentInfo_Common ( $studentList );
		}
	}
	function WS_GetUserBasic($UserEmail) {
		global $intranet_root, $ischoolbag_website;
		$sql = "SELECT UserID, UserLogin, UserPassword, UserEmail, EnglishName, ChineseName";
		$sql .= " FROM INTRANET_USER WHERE RecordStatus = 1 AND UserEmail = '$UserEmail'";
		$row = $this->returnArray ( $sql );
		if (sizeof ( $row ) > 0) {
			$ReturnContent = array ();
			$ReturnContent ['UserID'] = $row [0] ['UserID'];
			$ReturnContent ['UserEmail'] = $row [0] ['UserEmail'];
			$ReturnContent ['EnglishName'] = $row [0] ['EnglishName'];
			$ReturnContent ['ChineseName'] = $row [0] ['ChineseName'];
		} else {
			$ReturnContent = '401';
		}
		return $ReturnContent;
	}
	function WS_IPortfolio_RestoreFile($UserEmail, $ServerPath, $FileByteStream, $EcFileAttrs) {
		global $eclass_filepath, $ischoolbag_website, $ischoolbag_eclass_url_path;
		include_once ("$eclass_filepath/src/includes/php/lib-filesystem.php");
		include_once ("$eclass_filepath/src/includes/php/lib-portfolio.php");
		include_once ("$eclass_filepath/src/includes/php/lib-notes.php");
		include_once ("$eclass_filepath/src/includes/php/lib-notes_portfolio.php");
		include_once ("$eclass_filepath/src/includes/php/lib-ischoolbag-iportfolio.php");
		
		$isb_ip = new ischoolbag_iportfolio ();
		$courseDetails = $isb_ip->returnSpecialRooms ( $UserEmail, $RoomType = 4 );
		$course_id = $courseDetails [0] ['course_id'];
		$course_user_id = $courseDetails [0] ['user_id'];
		
		if (sizeof ( $courseDetails ) != 1) {
			$ReturnContent ['IsSuccess'] = 0;
			$ReturnContent ['Reason'] = 'portfolio not existed';
			return $ReturnContent;
		}
		
		if ($course_user_id != $EcFileAttrs ['User_id']) {
			$ReturnContent ['IsSuccess'] = 0;
			$ReturnContent ['Reason'] = 'permission denied';
			return $ReturnContent;
		}
		if (! isset ( $EcFileAttrs ['FileID'] ) or trim ( $EcFileAttrs ['FileID'] ) == 0) {
			$ReturnContent ['IsSuccess'] = 0;
			$ReturnContent ['Reason'] = 'file attributes corrupted';
			return $ReturnContent;
		}
		
		$course_db = classNamingDB ( $course_id );
		
		$ServerPath = big5_stripslashes ( $ServerPath ); // for text �\, �\
		foreach ( $EcFileAttrs as $attrKey => $attrValue ) {
			if (is_array ( $attrValue ))
				continue;
			$EcFileAttrs [$attrKey] = big5_stripslashes ( addslashes ( $EcFileAttrs [$attrKey] ) );
		}
		
		$des = $eclass_filepath . '/files/' . $course_db . '/' . 'group' . '/' . $ServerPath;
		$isb_ip->createFolder ( dirname ( $des ) );
		
		// extract filebyte from cdata
		$decoded = base64_decode ( $FileByteStream );
		
		$fp = fopen ( $des, 'wb' );
		fwrite ( $fp, $decoded );
		fclose ( $fp );
		
		$result = $isb_ip->restoreEclassFiles ( $course_db, $course_user_id, $EcFileAttrs );
		if (! $result) {
			$ReturnContent ['IsSuccess'] = 0;
			$ReturnContent ['Reason'] = 'file attributes corrupted';
			return $ReturnContent;
		}
		
		$ReturnContent ['IsSuccess'] = 1;
		return $ReturnContent;
	}
	function WS_GetIPortfolio($UserEmail, $Timestamp) {
		global $eclass_filepath, $ischoolbag_website, $ischoolbag_eclass_url_path, $intranet_default_lang;
		include_once ("$eclass_filepath/src/includes/php/lib-filesystem.php");
		include_once ("$eclass_filepath/src/includes/php/lib-portfolio.php");
		include_once ("$eclass_filepath/src/includes/php/lib-notes.php");
		include_once ("$eclass_filepath/src/includes/php/lib-notes_portfolio.php");
		include_once ("$eclass_filepath/src/includes/php/lib-ischoolbag-iportfolio.php");
		
		if ($intranet_default_lang == "gb") {
			$serverEncoding = 'GBK';
		} elseif ($intranet_default_lang == "b5" or $intranet_default_lang == "en") {
			$serverEncoding = 'Big5';
		}
		
		$isb_ip = new ischoolbag_iportfolio ();
		$courseDetails = $isb_ip->returnSpecialRooms ( $UserEmail, $RoomType = 4 );
		$course_id = $courseDetails [0] ['course_id'];
		$course_user_id = $courseDetails [0] ['user_id'];
		
		$course_db = classNamingDB ( $course_id );
		$portfolioItemsAll = $isb_ip->getPortfolios ( $course_db, $course_user_id, false );
		$portfolioItemsPublished = $isb_ip->getPortfolios ( $course_db, $course_user_id, true );
		
		if (count ( $portfolioItemsAll ) == 0) {
			$HasPortfolio = 0;
		} else {
			$HasPortfolio = 1;
			
			$li_pf = new portfolio ();
			$ni = new notes_iPortfolio ( $course_id );
			
			$portfolioItems = $isb_ip->getPortfolios ( $course_db, $course_user_id, true );
			
			if (count ( $portfolioItems ) == 0) {
				$HasPublished = 0;
			} else {
				$HasPublished = 1;
				
				$LastPublishedDate = $portfolioItems [0] ['notes_published'];
				$LastPublishedDate = strtotime ( $LastPublishedDate );
				
				if ($Timestamp < $LastPublishedDate) {
					$HasPublishedUpdate = 1;
				} else {
					$HasPublishedUpdate = 0;
				}
				$PublishedDownloadPath = $ischoolbag_website . $ischoolbag_eclass_url_path . "/src/portfolio/ischoolbag_published_download.php?CourseID=$course_id&CourseUserID=$course_user_id&ServerEncoding=$serverEncoding";
			}
			
			$BackupDownloadPath = $ischoolbag_website . $ischoolbag_eclass_url_path . "/src/portfolio/ischoolbag_backup_download.php?CourseID=$course_id&CourseUserID=$course_user_id&ServerEncoding=$serverEncoding";
		}
		
		$ReturnContent ['HasPortfolio'] = $HasPortfolio;
		$ReturnContent ['HasPublished'] = $HasPublished;
		$ReturnContent ['HasPublishedUpdate'] = $HasPublished ? $HasPublishedUpdate : 0;
		$ReturnContent ['LastPublishedDate'] = $HasPublished ? $LastPublishedDate : '';
		$ReturnContent ['PublishedDownloadPath'] = $HasPublished ? intranet_htmlspecialchars ( $PublishedDownloadPath ) : '';
		$ReturnContent ['BackupDownloadPath'] = $HasPortfolio ? intranet_htmlspecialchars ( $BackupDownloadPath ) : '';
		$ReturnContent ['ServerEncoding'] = $serverEncoding;
		return $ReturnContent;
	}
	function WS_GetConfig($UserName, $Password) {
		include_once ('libauth.php');
		global $ischoolbag_USBRecovery;
		
		$libauth = new libauth ();
		
		$ThisUserID = $libauth->validate_plain ( $UserName, $Password );
		if ($ThisUserID) {
			$ReturnContent ['eclassURL'] = $ischoolbag_USBRecovery ['eclassURL'];
			$ReturnContent ['schoolCode'] = $ischoolbag_USBRecovery ['schoolCode'];
			$ReturnContent ['WS_URI'] = $ischoolbag_USBRecovery ['WS_URI'];
			$ReturnContent ['DefaultLang'] = $ischoolbag_USBRecovery ['DefaultLang'];
		} else {
			$ReturnContent = '401';
		}
		return $ReturnContent;
	}
	
	// Begin Henry Yuen (2010-06-25): Get Latest Version
	function WS_GetLearningCentre($UserEmail) {
		global $plugin, $sys_integration, $ischoolbag_website;
		global $SessionID;
		
		$ischoolbag_service_url = $ischoolbag_website . '/ischoolbag_service.php?service=goLearningCentre&Session_ID=' . $SessionID;
		$ischoolbag_service_url_subject = $ischoolbag_website . '/ischoolbag_service.php?service=goLearningCentre_Subject&Session_ID=' . $SessionID;
		
		// p2p
		$isOpen = $plugin ['p2pw'] ? 1 : 0;
		$url = $ischoolbag_service_url . '&code=p2pw';
		$url = htmlspecialchars ( $url );
		$ReturnContent ['Item'] [0] ['code'] = 'p2pw';
		$ReturnContent ['Item'] [0] ['isOpen'] = $isOpen;
		$ReturnContent ['Item'] [0] ['url'] = $isOpen ? $url : '';
		
		// tsa
		$isOpen = $plugin ['tsa'] ? 1 : 0;
		$url = $ischoolbag_service_url . '&code=tsa';
		$url = htmlspecialchars ( $url );
		$ReturnContent ['Item'] [1] ['code'] = 'tsa';
		$ReturnContent ['Item'] [1] ['isOpen'] = $isOpen;
		$ReturnContent ['Item'] [1] ['url'] = $isOpen ? $url : '';
		
		// scrabbleFC
		$isOpen = $plugin ['ScrabbleFC'] ? 1 : 0;
		$url = $ischoolbag_service_url . '&code=ScrabbleFC';
		$url = htmlspecialchars ( $url );
		$ReturnContent ['Item'] [2] ['code'] = 'ScrabbleFC';
		$ReturnContent ['Item'] [2] ['isOpen'] = $isOpen;
		$ReturnContent ['Item'] [2] ['url'] = $isOpen ? $url : '';
		
		// cp_reading_sys
		$isOpen = (isset ( $sys_integration ['cp_reading_sys'] ) && $sys_integration ['cp_reading_sys'] != "") ? 1 : 0;
		$url = $ischoolbag_service_url . '&code=cp_reading_sys';
		$url = htmlspecialchars ( $url );
		$ReturnContent ['Item'] [3] ['code'] = 'cp_reading_sys';
		$ReturnContent ['Item'] [3] ['isOpen'] = $isOpen;
		$ReturnContent ['Item'] [3] ['url'] = $isOpen ? $url : '';
		
		// bookflix_school_access
		$isOpen = (isset ( $sys_integration ['bookflix_school_access'] ) && $sys_integration ['bookflix_school_access'] != "" && isset ( $sys_integration ['bookflix_school_expiry'] )) ? 1 : 0;
		$url = $ischoolbag_service_url . '&code=bookflix_school_access';
		$url = htmlspecialchars ( $url );
		$ReturnContent ['Item'] [4] ['code'] = 'bookflix_school_access';
		$ReturnContent ['Item'] [4] ['isOpen'] = $isOpen;
		$ReturnContent ['Item'] [4] ['url'] = $isOpen ? $url : '';
		
		// synergy_yle
		$isOpen = (isset ( $sys_integration ['synergy_yle'] ) && $sys_integration ['synergy_yle'] != "") ? 1 : 0;
		$url = $ischoolbag_service_url . '&code=synergy_yle';
		$url = htmlspecialchars ( $url );
		$ReturnContent ['Item'] [5] ['code'] = 'synergy_yle';
		$ReturnContent ['Item'] [5] ['isOpen'] = $isOpen;
		$ReturnContent ['Item'] [5] ['url'] = $isOpen ? $url : '';
		
		// eLib
		$isOpen = $plugin ['eLib'] ? 1 : 0;
		$url = '';
		$url = htmlspecialchars ( $url );
		$ReturnContent ['Item'] [6] ['code'] = 'eLib';
		$ReturnContent ['Item'] [6] ['isOpen'] = $isOpen;
		$ReturnContent ['Item'] [6] ['url'] = $isOpen ? $url : '';
		
		// subjectlist
		$isOpen = 1;
		$url = $ischoolbag_service_url_subject;
		$url = htmlspecialchars ( $url );
		$ReturnContent ['Item'] [7] ['code'] = 'subjectlist';
		$ReturnContent ['Item'] [7] ['isOpen'] = $isOpen;
		$ReturnContent ['Item'] [7] ['url'] = $isOpen ? $url : '';
		
		// elp (NOT YET IMPLEMEMTED)
		$isOpen = 0;
		$url = '';
		$url = htmlspecialchars ( $url );
		$ReturnContent ['Item'] [8] ['code'] = 'elp';
		$ReturnContent ['Item'] [8] ['isOpen'] = $isOpen;
		$ReturnContent ['Item'] [8] ['url'] = $isOpen ? $url : '';
		
		// weblinks
		$isOpen = 1;
		$url = '';
		$url = htmlspecialchars ( $url );
		$ReturnContent ['Item'] [9] ['code'] = 'weblinks';
		$ReturnContent ['Item'] [9] ['isOpen'] = $isOpen;
		$ReturnContent ['Item'] [9] ['url'] = $isOpen ? $url : '';
		
		return $ReturnContent;
	}
	
	// Begin Henry Yuen (2010-06-25): Get Latest Version
	function WS_GetLatestVersion($MainVersion, $UpdaterVersion) {
		include_once ('libisbupdater.php');
		$libisbupdater = new libisbupdater ();
		
		global $intranet_root, $ischoolbag_website;
		$ReturnContent = array ();
		
		$MainVersionID = $libisbupdater->getVersionID ( 'Main', $MainVersion, $checkReleased = true );
		
		$UpdaterVersionID = $libisbupdater->getVersionID ( 'Updater', $UpdaterVersion, $checkReleased = true );
		if ($MainVersionID == '' or $UpdaterVersionID == '') {
			$ReturnContent = '409';
			return $ReturnContent;
		}
		
		$ModuleArray ['Main'] = $MainVersion;
		$ModuleArray ['Updater'] = $UpdaterVersion;
		
		foreach ( $ModuleArray as $module => $moduleVersion ) {
			$ToVersion = $libisbupdater->getUpdateVersion ( $module, $moduleVersion );
			if ($ToVersion ['Version'] != $moduleVersion) {
				$ReturnContent [$module] ['HasUpdate'] = 1;
				$ReturnContent [$module] ['ForceUpdate'] = $ToVersion ['ForceUpdate'];
				if ($module == 'Updater') {
					$ReturnContent [$module] ['NeedRestart'] = 1;
				} else {
					$ReturnContent [$module] ['NeedRestart'] = 0;
				}
				$ReturnContent [$module] ['LatestVersion'] = $ToVersion ['Version'];
				
				$packages = $libisbupdater->getUpdatePatch ( $module, $moduleVersion, $ToVersion ['Version'] );
				$c = 0;
				foreach ( $packages as $packageType => $packagePath ) {
					$ReturnContent [$module] ['Packages'] ['Package'] [$c] ['PackageType'] = $packageType;
					$ReturnContent [$module] ['Packages'] ['Package'] [$c] ['PackagePath'] = $packagePath;
					$c ++;
				}
			} else {
				$ReturnContent [$module] ['HasUpdate'] = 0;
			}
		}
		return $ReturnContent;
	}
	// End Henry Yuen (2010-06-25): Get Latest Version
	
	// Begin Henry Yuen (2010-05-03): Get eBook List
	function WS_GetEBookToken($UserEmail, $BookID, $IsUnlimited) {
		global $intranet_root, $ischoolbag_website;
		// $ReturnContent['Url'] = $ischoolbag_website.'/eBook/'. $BookID. '/book.epub';
		
		include_once ('libdownloader.php');
		$libdownloader = new libdownloader ();
		
		// authentication is not yet implemented
		
		// Begin Henry Yuen (2010-05-27): modify the download to use token mechanism
		
		// Get UserID
		$sql = "SELECT UserID FROM INTRANET_USER WHERE UserEmail = '$UserEmail'";
		$row = $this->returnVector ( $sql );
		$ThisUserID = $row [0];
		
		$filepath = $intranet_root . '/eBook/' . $BookID . '/book.epub';
		// $contentType = 'application/download';
		// $contentType = 'application/x-msdownload';
		// $contentType = 'text/plain';
		$module = 'isb';
		$timeout = 120;
		$returnError = false;
		$unlimitedDownload = $IsUnlimited == 1;
		
		$tokenpath = $libdownloader->createToken ( $libdownloader->TYPE_FILEPATH, $filename = '', $filepath, $module, $ThisUserID, $contentType, $timeout, $returnError, $unlimitedDownload );
		if ($tokenpath) {
			$path = $ischoolbag_website . '/' . $tokenpath;
			
			$urlExploded = explode ( '=', $path );
			
			$ReturnContent ['Token'] = $urlExploded [1];
			$ReturnContent ['Url'] = $path;
		} else {
			return 1;
		}
		
		// End Henry Yuen (2010-05-27): modify the download to use token mechanism
		
		return $ReturnContent;
	}
	// End Henry Yuen (2010-05-03): Get eBook List
	
	// Begin Henry Yuen (2010-05-03): Get eBook List
	function WS_GetEBookList($UserEmail, $Timestamp) {
		Global $ischoolbag_website;
		/*
		 * # eBook DB is not yet setup in EJ
		 * # return array();
		 *
		 * $conditionPublish = 'B.Publish = 1';
		 * if(trim($Timestamp) == ''){
		 * $conditionTimeStamp = '1 = 1';
		 * }
		 * else{
		 * $conditionTimeStamp = 'UNIX_TIMESTAMP(B.DateModified) > '. $Timestamp;
		 * }
		 *
		 * $sql = 'SELECT B.BookID, A.Author, B.Title, B.Preface, B.Publisher, B.Source, B.DateModified';
		 * $sql .= ' FROM INTRANET_ELIB_BOOK B JOIN INTRANET_ELIB_BOOK_AUTHOR A ON B.AuthorID = A.AuthorID';
		 * $sql .= ' WHERE '. $conditionPublish. ' AND '. $conditionTimeStamp;
		 *
		 * $row = $this->returnArray($sql);
		 * //echo $sql;
		 * $ReturnContent = array();
		 * $count = count($row);
		 * if($count > 0){
		 * for($i=0; $i < $count; $i++)
		 * {
		 * $BookID = $row[$i]['BookID'];
		 * $Author = $row[$i]['Author'];
		 * $Title = $row[$i]['Title'];
		 * $Preface = $row[$i]['Preface'];
		 * $Publisher = $row[$i]['Publisher'];
		 * $Source = $row[$i]['Source'];
		 * $DateModified = $row[$i]['DateModified'];
		 *
		 * $ReturnContent['Book'][$i]['BookID'] = $BookID;
		 * $ReturnContent['Book'][$i]['Author'] = intranet_htmlspecialchars($Author);
		 * $ReturnContent['Book'][$i]['Title'] = intranet_htmlspecialchars($Title);
		 * $ReturnContent['Book'][$i]['Preface'] = intranet_htmlspecialchars($Preface);
		 * $ReturnContent['Book'][$i]['CoverImageUrl'] = ''; // NOT YET IMPLEMENTED
		 * $ReturnContent['Book'][$i]['Publisher'] = intranet_htmlspecialchars($Publisher);
		 * $ReturnContent['Book'][$i]['Source'] = intranet_htmlspecialchars($Source);
		 * $ReturnContent['Book'][$i]['DateModified'] = $DateModified;
		 * }
		 * }
		 */
		
		$tmpReturn ['Book'] [0] ['BookID'] = '12933';
		$tmpReturn ['Book'] [0] ['Author'] = 'Elbert Hubbard';
		$tmpReturn ['Book'] [0] ['Title'] = 'Little Journeys to the Homes of the Great, Vol 1 of 14';
		$tmpReturn ['Book'] [0] ['Preface'] = 'Elbert Hubbard is dead, or should we say, has gone on his last Little Journey to the Great Beyond. But the children of his fertile brain still live and will continue to live and keep fresh the memory of their illustrious forebear.';
		$tmpReturn ['Book'] [0] ['CoverImageUrl'] = $ischoolbag_website . '/eBook/12933/bookcover.jpg';
		$tmpReturn ['Book'] [0] ['Publisher'] = 'Proofreading Team at http://www.pgdp.net';
		$tmpReturn ['Book'] [0] ['Source'] = '';
		$tmpReturn ['Book'] [0] ['DateModified'] = '';
		$tmpReturn ['Book'] [1] ['BookID'] = '19033';
		$tmpReturn ['Book'] [1] ['Author'] = 'Lewis Carroll';
		$tmpReturn ['Book'] [1] ['Title'] = 'Alice in Wonderland';
		$tmpReturn ['Book'] [1] ['Preface'] = 'It tells the story of a girl named Alice who falls down a rabbit hole into a fantasy world populated by peculiar and anthropomorphic creatures. The tale plays with logic in ways that have given the story lasting popularity with adults as well as children. It is considered to be one of the best examples of the "literary nonsense" genre, and its narrative course and structure have been enormously influential, especially in the fantasy genre.';
		$tmpReturn ['Book'] [1] ['CoverImageUrl'] = $ischoolbag_website . '/eBook/19033/bookcover.jpg';
		$tmpReturn ['Book'] [1] ['Publisher'] = 'SAM\'L GABRIEL SONS &amp; COMPANY';
		$tmpReturn ['Book'] [1] ['Source'] = '';
		$tmpReturn ['Book'] [1] ['DateModified'] = '';
		$tmpReturn ['Book'] [2] ['BookID'] = '101';
		$tmpReturn ['Book'] [2] ['Author'] = 'Lewis Carroll';
		$tmpReturn ['Book'] [2] ['Title'] = 'Book 101';
		$tmpReturn ['Book'] [2] ['Preface'] = 'It tells the story of a girl named Alice who falls down a rabbit hole into a fantasy world populated by peculiar and anthropomorphic creatures. The tale plays with logic in ways that have given the story lasting popularity with adults as well as children. It is considered to be one of the best examples of the "literary nonsense" genre, and its narrative course and structure have been enormously influential, especially in the fantasy genre.';
		$tmpReturn ['Book'] [2] ['CoverImageUrl'] = '';
		$tmpReturn ['Book'] [2] ['Publisher'] = 'SAM\'L GABRIEL SONS &amp; COMPANY';
		$tmpReturn ['Book'] [2] ['Source'] = '';
		$tmpReturn ['Book'] [2] ['DateModified'] = '';
		$tmpReturn ['Book'] [3] ['BookID'] = '102';
		$tmpReturn ['Book'] [3] ['Author'] = 'Lewis Carroll';
		$tmpReturn ['Book'] [3] ['Title'] = 'Book 102';
		$tmpReturn ['Book'] [3] ['Preface'] = 'It tells the story of a girl named Alice who falls down a rabbit hole into a fantasy world populated by peculiar and anthropomorphic creatures. The tale plays with logic in ways that have given the story lasting popularity with adults as well as children. It is considered to be one of the best examples of the "literary nonsense" genre, and its narrative course and structure have been enormously influential, especially in the fantasy genre.';
		$tmpReturn ['Book'] [3] ['CoverImageUrl'] = '';
		$tmpReturn ['Book'] [3] ['Publisher'] = 'SAM\'L GABRIEL SONS &amp; COMPANY';
		$tmpReturn ['Book'] [3] ['Source'] = '';
		$tmpReturn ['Book'] [3] ['DateModified'] = '';
		$tmpReturn ['Book'] [4] ['BookID'] = '103';
		$tmpReturn ['Book'] [4] ['Author'] = 'Lewis Carroll';
		$tmpReturn ['Book'] [4] ['Title'] = 'Book 103';
		$tmpReturn ['Book'] [4] ['Preface'] = 'It tells the story of a girl named Alice who falls down a rabbit hole into a fantasy world populated by peculiar and anthropomorphic creatures. The tale plays with logic in ways that have given the story lasting popularity with adults as well as children. It is considered to be one of the best examples of the "literary nonsense" genre, and its narrative course and structure have been enormously influential, especially in the fantasy genre.';
		$tmpReturn ['Book'] [4] ['CoverImageUrl'] = '';
		$tmpReturn ['Book'] [4] ['Publisher'] = 'SAM\'L GABRIEL SONS &amp; COMPANY';
		$tmpReturn ['Book'] [4] ['Source'] = '';
		$tmpReturn ['Book'] [4] ['DateModified'] = '';
		$tmpReturn ['Book'] [5] ['BookID'] = '104';
		$tmpReturn ['Book'] [5] ['Author'] = 'Lewis Carroll';
		$tmpReturn ['Book'] [5] ['Title'] = 'Book 104';
		$tmpReturn ['Book'] [5] ['Preface'] = 'It tells the story of a girl named Alice who falls down a rabbit hole into a fantasy world populated by peculiar and anthropomorphic creatures. The tale plays with logic in ways that have given the story lasting popularity with adults as well as children. It is considered to be one of the best examples of the "literary nonsense" genre, and its narrative course and structure have been enormously influential, especially in the fantasy genre.';
		$tmpReturn ['Book'] [5] ['CoverImageUrl'] = '';
		$tmpReturn ['Book'] [5] ['Publisher'] = 'SAM\'L GABRIEL SONS &amp; COMPANY';
		$tmpReturn ['Book'] [5] ['Source'] = '';
		$tmpReturn ['Book'] [5] ['DateModified'] = '';
		
		return $tmpReturn;
	}
	// End Henry Yuen (2010-05-03): Get eBook List
	
	// Get User Comment
	function WS_GetUserComment($CourseID, $UserEmail, $Timestamp) {
		global $eclass_db, $intranet_db;
		global $i_Campusquota_basic_identity;
		
		if (trim ( $Timestamp ) == '') {
			$conditionTimeStamp = '1 = 1';
		} else {
			$conditionTimeStamp = 'UNIX_TIMESTAMP(c.DateModified) > ' . $Timestamp;
		}
		
		$sql = "SELECT user_id FROM $eclass_db.user_course WHERE course_id = $CourseID AND user_email = '$UserEmail'";
		$row = $this->returnVector ( $sql );
		
		if (count ( $row ) > 0) {
			$User_ID = $row [0];
			
			$this->db = classNamingDB ( $CourseID );
			
			$sql = "SELECT " . getNameFieldByLang ( "iu." ) . " AS PostUserName, " . getNameFieldByLang ( "iu2." ) . " AS ReceiverUserName, c.CommentID, c.Comment, c.PostBy, c.CommentGROUP, c.Status, c.PostUserID, c.ReceiverUserID, c.DateShow, c.DateModified";
			$sql .= " FROM teacher_parent_comment c";
			$sql .= " LEFT JOIN usermaster u ON c.PostUserID = u.user_id";
			$sql .= " LEFT JOIN $intranet_db.INTRANET_USER iu ON u.user_email = iu.UserEmail";
			$sql .= " LEFT JOIN usermaster u2 ON c.ReceiverUserID = u2.user_id";
			$sql .= " LEFT JOIN $intranet_db.INTRANET_USER iu2 ON u2.user_email = iu2.UserEmail";
			$sql .= " WHERE (c.ReceiverUserID='$User_ID' OR c.PostUserID='$User_ID')";
			$sql .= " AND $conditionTimeStamp";
			$sql .= " ORDER BY c.CommentGROUP ASC, c.DateInput ASC ";
			// echo $sql;
			$row = $this->returnArray ( $sql );
			// debug_r($row);
			
			$ReturnContent = array ();
			$count = count ( $row );
			if ($count > 0) {
				for($i = 0; $i < $count; $i ++) {
					$ReturnContent ['Comment'] [$i] ['CommentID'] = $row [$i] ['CommentID'];
					
					// # 2011-08-03: Please note the following !!!
					// # the comment content is stored in DB after calling htmlspecialchar(), which is a wrong practice.
					// # so now we call undo_htmlspecialchars (to fix the previous bug) and recall the htmlspecialchars method.
					// # Although it looks ridiculous to counter the effect with one and other, it explains the idea why htmlspecialchars() is not necessary here before exporting as xml
					$ReturnContent ['Comment'] [$i] ['Content'] = intranet_undo_htmlspecialchars ( $row [$i] ['Comment'] );
					$ReturnContent ['Comment'] [$i] ['Content'] = intranet_htmlspecialchars ( $ReturnContent ['Comment'] [$i] ['Content'] );
					
					$ReturnContent ['Comment'] [$i] ['CommentGroup'] = $row [$i] ['CommentGROUP'];
					// $ReturnContent[$i]['PostUserName'] = intranet_htmlspecialchars($row[$i]['PostUserName']);
					$ReturnContent ['Comment'] [$i] ['DateShow'] = $row [$i] ['DateShow'];
					$ReturnContent ['Comment'] [$i] ['DateModified'] = $row [$i] ['DateModified'];
					
					$user_name = ($row [$i] ['PostBy'] == 'P') ? $i_Campusquota_basic_identity [3] : $row [$i] ['PostUserName'];
					$ReturnContent ['Comment'] [$i] ['PostUserDisplayName'] = intranet_htmlspecialchars ( $user_name );
				}
			}
			return $ReturnContent;
		} else {
			return 104;
		}
	}
	
	// # 2011-07-29: A special version of getUserComment method for ASL
	function WS_GetUserComment2($CourseID, $UserEmail, $Timestamp, $UserName, $NoOfRecords) {
		global $eclass_db, $intranet_db;
		global $i_Campusquota_basic_identity;
		
		if ($UserEmail == '' and $UserName != '') {
			$sql = "SELECT UserEmail FROM INTRANET_USER WHERE UserLogin = '$UserName'";
			$row = $this->returnVector ( $sql );
			$UserEmail = $row [0];
		}
		
		if (trim ( $Timestamp ) == '') {
			$conditionTimeStamp = '1 = 1';
		} else {
			$conditionTimeStamp = 'UNIX_TIMESTAMP(c.DateModified) > ' . $Timestamp;
		}
		
		if ($CourseID == '') {
			$sql = "SELECT a.course_id, a.course_code, a.course_name 
					FROM {$eclass_db}.course AS a, {$eclass_db}.user_course AS b 
					WHERE 
						a.course_id = b.course_id AND b.user_email = '$UserEmail' AND a.RoomType=0 AND 
						(b.status is NULL OR b.status NOT IN ('deleted')) 
					GROUP BY a.course_id 
					ORDER BY a.course_code, a.course_name";
			$courseList = $this->returnArray ( $sql );
		} else {
			$sql = "SELECT a.course_id, a.course_code, a.course_name 
					FROM {$eclass_db}.course AS a 
					WHERE a.course_id = $CourseID";
			$courseList = $this->returnArray ( $sql );
		}
		
		if ($UserEmail != '' and count ( $courseList ) > 0) {
			$User_ID = $row [0];
			
			// $this->db = classNamingDB($CourseID);
			
			$sql = "SELECT * FROM(";
			for($i = 0; $i < count ( $courseList ); $i ++) {
				$CourseID = $courseList [$i] ['course_id'];
				$course_db = classNamingDb ( $CourseID );
				
				$sql .= "(SELECT " . getNameFieldByLang ( "iu." ) . " AS PostUserName, " . getNameFieldByLang ( "iu2." ) . " AS ReceiverUserName, c.CommentID, c.Comment, c.PostBy, c.CommentGROUP, c.Status, c.PostUserID, c.ReceiverUserID, c.DateShow, c.DateModified, c.DateInput, $CourseID as CourseID";
				$sql .= " FROM $course_db.teacher_parent_comment c";
				$sql .= " LEFT JOIN $course_db.usermaster u ON c.PostUserID = u.user_id";
				$sql .= " LEFT JOIN $eclass_db.user_course uc ON uc.course_id = $CourseID and uc.user_id = u.user_id";
				$sql .= " LEFT JOIN $intranet_db.INTRANET_USER iu ON u.user_email = iu.UserEmail";
				$sql .= " LEFT JOIN $course_db.usermaster u2 ON c.ReceiverUserID = u2.user_id";
				$sql .= " LEFT JOIN $eclass_db.user_course uc2 ON uc2.course_id = $CourseID and uc2.user_id = u2.user_id";
				$sql .= " LEFT JOIN $intranet_db.INTRANET_USER iu2 ON u2.user_email = iu2.UserEmail";
				// $sql .= " WHERE (c.ReceiverUserID='$User_ID' OR c.PostUserID='$User_ID')";
				$sql .= " WHERE (uc.user_email='$UserEmail' OR uc2.user_email='$UserEmail')";
				$sql .= " AND $conditionTimeStamp";
				$sql .= ")";
				
				if ($i < count ( $courseList ) - 1) {
					$sql .= ' UNION ';
				}
			}
			
			$sql .= ") combine";
			$sql .= " ORDER BY CommentGROUP ASC, DateInput ASC ";
			
			if ($NoOfRecords != '')
				$sql .= " LIMIT 0, $NoOfRecords";
				
				// echo $sql;
			$row = $this->returnArray ( $sql );
			
			// # 2011-07-29: select all comments inside the commentgroup selected in previous step
			$count = count ( $row );
			$selectedCommentGrouplist = array ();
			if ($count > 0) {
				for($i = 0; $i < $count; $i ++) {
					$courseID = $row [$i] ['CourseID'];
					$commentGroup = $row [$i] ['CommentGROUP'];
					if (! isset ( $selectedCommentGrouplist [$courseID] ) or ! in_array ( $commentGroup, $selectedCommentGrouplist [$courseID] )) {
						$selectedCommentGrouplist [$courseID] [] = $commentGroup;
					}
				}
			}
			
			$sql = "SELECT * FROM(";
			$c = 0;
			foreach ( $selectedCommentGrouplist as $CourseID => $commentgrouplist ) {
				$course_db = classNamingDb ( $CourseID );
				
				$sql .= "(SELECT " . getNameFieldByLang ( "iu." ) . " AS PostUserName, " . getNameFieldByLang ( "iu2." ) . " AS ReceiverUserName, c.CommentID, c.Comment, c.PostBy, c.CommentGROUP, c.Status, c.PostUserID, c.ReceiverUserID, c.DateShow, c.DateModified, c.DateInput, $CourseID as CourseID";
				$sql .= " FROM $course_db.teacher_parent_comment c";
				$sql .= " LEFT JOIN $course_db.usermaster u ON c.PostUserID = u.user_id";
				$sql .= " LEFT JOIN $eclass_db.user_course uc ON uc.course_id = $CourseID and uc.user_id = u.user_id";
				$sql .= " LEFT JOIN $intranet_db.INTRANET_USER iu ON u.user_email = iu.UserEmail";
				$sql .= " LEFT JOIN $course_db.usermaster u2 ON c.ReceiverUserID = u2.user_id";
				$sql .= " LEFT JOIN $eclass_db.user_course uc2 ON uc2.course_id = $CourseID and uc2.user_id = u2.user_id";
				$sql .= " LEFT JOIN $intranet_db.INTRANET_USER iu2 ON u2.user_email = iu2.UserEmail";
				$sql .= " WHERE c.CommentGROUP IN ( " . implode ( ',', $commentgrouplist ) . ")";
				$sql .= " AND $conditionTimeStamp";
				$sql .= ")";
				
				if ($c ++ < count ( $selectedCommentGrouplist ) - 1) {
					$sql .= ' UNION ';
				}
			}
			
			$sql .= ") combine";
			$sql .= " ORDER BY CommentGROUP ASC, DateInput ASC ";
			
			// echo htmlspecialchars($sql);
			$row = $this->returnArray ( $sql );
			
			$CommentGroupList = array ();
			$count = count ( $row );
			if ($count > 0) {
				for($i = 0; $i < $count; $i ++) {
					$CommentItem = array ();
					
					$CommentItem ['CommentID'] = $row [$i] ['CommentID'];
					
					// # 2011-08-03: Please note the following !!!
					// # the comment content is stored in DB after calling htmlspecialchar(), which is a wrong practice.
					// # so now we call undo_htmlspecialchars (to fix the previous bug) and recall the htmlspecialchars method.
					// # Although it looks ridiculous to counter the effect with one and other, it explains the idea why htmlspecialchars() is not necessary here before exporting as xml
					$CommentItem ['Content'] = intranet_undo_htmlspecialchars ( $row [$i] ['Comment'] );
					$CommentItem ['Content'] = intranet_htmlspecialchars ( $CommentItem ['Content'] );
					
					$CommentItem ['DateShow'] = $row [$i] ['DateShow'];
					$CommentItem ['DateModified'] = $row [$i] ['DateModified'];
					$user_name = ($row [$i] ['PostBy'] == 'P') ? $i_Campusquota_basic_identity [3] : $row [$i] ['PostUserName'];
					$CommentItem ['PostUserDisplayName'] = intranet_htmlspecialchars ( $user_name );
					
					$CommentGroupList [$row [$i] ['CourseID']] [$row [$i] ['CommentGROUP']] [] = $CommentItem;
				}
			}
			
			$ReturnContent = array ();
			$c = 0;
			foreach ( $CommentGroupList as $courseID => $CommentGroupItemlist ) {
				foreach ( $CommentGroupItemlist as $commentGroupID => $commentItems ) {
					$ReturnContent ['CommentGroupItem'] [$c] ['CommentGroup'] = $commentGroupID;
					$ReturnContent ['CommentGroupItem'] [$c] ['CourseID'] = $courseID;
					$ReturnContent ['CommentGroupItem'] [$c] ['Comment'] = $commentItems;
					$c ++;
				}
			}
			return $ReturnContent;
		} else {
			return 104;
		}
	}
	
	// Submit Assignment Common
	function WS_UploadAsgn($CourseID, $UserEmail, $AsgnID, $FileName, $FileByteStream, $IsCorrection = 0) {
		global $eclass_db, $eclass_filepath;
		include_once ("$eclass_filepath/src/includes/php/lib-filemanager.php");
		include_once ("$eclass_filepath/src/includes/php/lib-user.php");
		include_once ("$eclass_filepath/src/includes/php/lib-filesystem.php");
		
		$sql = "SELECT user_id FROM $eclass_db.user_course WHERE course_id = $CourseID AND user_email = '$UserEmail'";
		$row = $this->returnVector ( $sql );
		
		if (count ( $row ) > 0) {
			$ThisUserID = $row [0];
			
			$this->db = classNamingDB ( $CourseID );
			
			$sql = "SELECT worktype, title, sheettype, answersheet FROM assignment WHERE assignment_id='$AsgnID' ";
			$row = $this->returnArray ( $sql, 4 );
			
			// debug_r($row);
			if (count ( $row ) == 0) {
				return 103; // assignment not existed
			} else {
				if ($row [0] ["answersheet"] == "===writing===") {
					return $this->UploadAsgnCompo ( $CourseID, $UserEmail, $AsgnID, $FileName, $FileByteStream, $IsCorrection );
				} elseif ($row [0] ["answersheet"] == "") {
					return $this->UploadAsgnFile ( $CourseID, $UserEmail, $AsgnID, $FileName, $FileByteStream );
				} else {
					return $this->UploadAsgnAnswersheet ( $CourseID, $UserEmail, $AsgnID, $FileName, $FileByteStream );
				}
			}
		} else {
			return 104;
		}
	}
	
	// Submit Assignment (Answersheet)
	function UploadAsgnAnswersheet($CourseID, $UserEmail, $AsgnID, $FileName, $FileByteStream) {
		global $eclass_db, $eclass_filepath;
		
		$sql = "SELECT user_id FROM $eclass_db.user_course WHERE course_id = $CourseID AND user_email = '$UserEmail'";
		$row = $this->returnVector ( $sql );
		$ThisUserID = $row [0];
		
		$this->db = classNamingDB ( $CourseID );
		
		$decoded = base64_decode ( $FileByteStream );
		// $decoded = $FileByteStream;
		
		$decoded = mysql_real_escape_string ( $decoded );
		$decoded = htmlspecialchars ( $decoded );
		// 2011-03-23: handle GBK
		// $decoded = iconv('UTF8', 'Big5//IGNORE', $decoded); // the text file content is not yet converted in index.php
		$decoded = ischoolbag_iconv ( $decoded, false );
		
		// 2010-11-26: some Big5 characters
		$decoded = big5_stripslashes ( addslashes ( $decoded ) );
		// $decoded = addslashes($decoded);
		
		$sql = "SELECT handin_id FROM handin WHERE assignment_id = $AsgnID and user_id = $ThisUserID";
		$row = $this->returnArray ( $sql );
		
		if (sizeof ( $row ) <= 0) {
			// add new
			$sql = "INSERT INTO handin (assignment_id, user_id, answer, inputdate)
					VALUES ($AsgnID, $ThisUserID, '$decoded', now()) ";
		} else {
			$handin_id = $row [0] ['handin_id'];
		}
		
		if ($handin_id) {
			// update
			$sql = "UPDATE handin SET answer='$decoded', comment = NULL, inputdate=now() WHERE handin_id=$handin_id ";
		}
		// echo $sql;
		$this->db_db_query ( $sql );
		
		$ReturnContent = array ();
		$ReturnContent ['isSuccess'] = "1";
		
		return $ReturnContent;
	}
	
	// Submit Assignment (Composition)
	function UploadAsgnCompo($CourseID, $UserEmail, $AsgnID, $FileName, $FileByteStream, $IsCorrection) {
		global $eclass_db, $eclass_filepath;
		
		if ($IsCorrection == '') {
			$IsCorrection = 0;
		}
		
		// include_once("$eclass_filepath/src/includes/php/lib-db.php");
		// $li = new libdb();
		
		$sql = "SELECT user_id FROM $eclass_db.user_course WHERE course_id = $CourseID AND user_email = '$UserEmail'";
		$row = $this->returnVector ( $sql );
		$ThisUserID = $row [0];
		
		$this->db = classNamingDB ( $CourseID );
		
		$decoded = base64_decode ( $FileByteStream );
		// $decoded = $FileByteStream;
		/*
		 * // first three byte in txt file is not content
		 * if(substr($FileName, -4, 4) == ".txt"){
		 * $decoded = substr($decoded, 3, strlen($decoded) - 3);
		 * }
		 */
		// $decoded = mysql_real_escape_string($decoded);
		$decoded = htmlspecialchars ( $decoded );
		// 2011-03-23: handle GBK
		// $decoded = iconv('UTF8', 'Big5//IGNORE', $decoded); // the text file content is not yet converted in index.php
		$decoded = ischoolbag_iconv ( $decoded, false );
		
		// 2010-11-26: some Big5 characters
		$decoded = big5_stripslashes ( addslashes ( $decoded ) );
		
		$returnWord = array ();
		$returnWord = Word_Count ( $decoded );
		$wordcount = $returnWord [word] . "|" . $returnWord [symbol];
		// echo "word: ". $returnWord[word]. "<br>";
		// echo "symbol: ". $returnWord[symbol];
		
		$sql = "SELECT handin_id, is_correction FROM handin WHERE assignment_id = $AsgnID and user_id = $ThisUserID";
		$row = $this->returnArray ( $sql );
		
		if (sizeof ( $row ) <= 0) {
			// add new
			$newCounting0 = ($wordcount != "") ? ", totalword" : "";
			$newCounting1 = ($wordcount != "") ? ", '$wordcount'" : "";
			$sql = "INSERT INTO handin (assignment_id, user_id, answer, comment, inputdate $newCounting0)
					VALUES ($AsgnID, $ThisUserID, '$decoded', 'NULL', now() $newCounting1) ";
		} else {
			$handin_id = $row [0] ['handin_id'];
			$is_correction = $row [0] ['is_correction'];
		}
		
		if ($handin_id) {
			if ($IsCorrection != $is_correction) {
				return 204; // inconsistent correction status
			}
			
			if ($is_correction) {
				// correction
				$newStatus = "correctiondate=now()";
				$sql = "UPDATE handin SET answer_correction='$decoded', $newStatus WHERE handin_id=$handin_id ";
			} else {
				// update
				$newStatus = "comment=null";
				$newCounting = ($wordcount != "") ? ", totalword='$wordcount'" : "";
				$sql = "UPDATE handin SET answer='$decoded', filename=null, $newStatus, inputdate=now() $newCounting WHERE handin_id=$handin_id ";
			}
		}
		// echo $sql;
		$this->db_db_query ( $sql );
		
		$ReturnContent = array ();
		$ReturnContent ['isSuccess'] = "1";
		
		return $ReturnContent;
	}
	
	// Submit Assignment (Upload Files)
	function UploadAsgnFile($CourseID, $UserEmail, $AsgnID, $FileName, $FileByteStream) {
		global $eclass_db, $eclass_filepath;
		include_once ("$eclass_filepath/src/includes/php/lib-filemanager.php");
		include_once ("$eclass_filepath/src/includes/php/lib-user.php");
		include_once ("$eclass_filepath/src/includes/php/lib-filesystem.php");
		
		$li = new phpduoFileSystem ();
		$fm = new fileManager ( $CourseID, 4, "" );
		$fm->file_path = "";
		
		$sql = "SELECT user_id FROM $eclass_db.user_course WHERE course_id = $CourseID AND user_email = '$UserEmail'";
		$row = $this->returnVector ( $sql );
		$ThisUserID = $row [0];
		
		$lu = new libuser ( $ThisUserID, $CourseID );
		$user_name = addslashes ( $lu->user_name () );
		
		$this->db = classNamingDB ( $CourseID );
		
		// check whether the assignment has been graded
		$sql = "SELECT grade FROM handin WHERE assignment_id = $AsgnID and user_id = $ThisUserID";
		$row = $this->returnVector ( $sql, 1 );
		if (count ( $row ) > 0) {
			if (trim ( $row [0] ) != "") {
				return 203;
			}
		}
		
		$sql = "SELECT worktype, title, sheettype FROM assignment WHERE assignment_id='$AsgnID' ";
		$row = $this->returnArray ( $sql, 3 );
		
		switch ($row [0] [0]) {
			case 1 :
				$folderName = "A" . $AsgnID;
				$folderNameType = "Assignment";
				break;
			case 2 :
				$folderName = "S" . $AsgnID;
				$folderNameType = "Survey";
				break;
			case 3 :
			case 4 :
			case 5 :
				$folderName = "P" . $AsgnID;
				$folderNameType = "Project";
				break;
			default :
				$folderName = "A" . $AsgnID;
				$folderNameType = "Assignment";
				break;
		}
		$assignment_title = $row [0] [1];
		$description = "$folderNameType (" . addslashes ( $row [0] [1] ) . ") handin";
		$sheettype = $row [0] [2];
		
		$sql = "SELECT class_number, CONCAT(class_number, ' ', lastname, firstname) FROM usermaster WHERE user_id='$ThisUserID' ";
		$row = $this->returnArray ( $sql, 1 );
		$classNo = ($row [0] [0] == "") ? 0 : str_replace ( " ", "", $row [0] [0] );
		// $classNo = ($row[0][0]=="") ? 0 : $row[0][0];
		$userName = trim ( $row [0] [1] );
		
		if (strpos ( $FileName, "." ) == 0) { // Fail
			return 105;
		}
		$upload_file_name = $FileName; // from $_POST
		
		$fm->user_name = "eClass";
		$fm->user_email = "null";
		
		// create folder
		$folderID1 = $fm->createFolderDB ( $folderNameType, $folderNameType, "", 4 );
		
		// create sub-folder (assignment id)
		$fm->folderID = $folderID1;
		$folderID2 = $fm->createFolderDB ( $folderName, addslashes ( $assignment_title ), $folderID1, 4 );
		
		// echo $folderName;
		if ($sheettype == "MF") {
			$userFolderName = "{$classNo}_ID{$ThisUserID}";
			$filename = $upload_file_name;
			$VirPath = "/" . $folderNameType . "/" . $folderName . "/" . $userFolderName . "/";
			
			// create sub-sub-folder
			$fm->folderID = $folderID2;
			$folderID3 = $fm->createFolderDB ( $userFolderName, $userName, $folderID2, 4 );
			$folderID_Final = $folderID3;
		} else {
			$filename = $classNo . "~" . $ThisUserID . "~" . $upload_file_name;
			$VirPath = "/" . $folderNameType . "/" . $folderName . "/";
			$folderID_Final = $folderID2;
		}
		
		// echo $folderID1. ", ". $folderID2. ", ". $folderID3. ", ". $folderID_Final;
		$fm->user_name = $user_name;
		$fm->user_email = $UserEmail;
		$root = $fm->getCategoryRoot ( 4, $fm->db );
		
		$FLocation = $fm->genUniqueID ( $folderID_Final, $FileName );
		$folderPath = $root . "/" . $FLocation;
		
		$location = $FLocation; // to DB
		$handin_name = $VirPath . addslashes ( $filename );
		// echo $folderPath;
		$des = "$folderPath/" . stripslashes ( $filename ); // for text �\, �\
		                                             // echo $folderPath;
		                                             // echo $folderPath;
		                                             // echo $fm->file_path;
		$fm->createFolder ( $folderPath );
		
		// extract filebyte from cdata
		$decoded = base64_decode ( $FileByteStream );
		
		$fp = fopen ( $des, 'wb' );
		fwrite ( $fp, $decoded );
		fclose ( $fp );
		
		$FileSize = filesize ( $des ) / 1024; // to KB
		                                 
		// update file record in file manager
		                                 // check exist
		$checkParent = ($folderID_Final != "") ? "ParentDirID='" . $folderID_Final . "'" : "ParentDirID is NULL ";
		$sql = "SELECT FileID FROM eclass_file ";
		$sql .= " WHERE Title='" . addslashes ( $filename ) . "' AND $checkParent AND Category='4'";
		
		// $updateNeed = false;
		$row = $this->returnArray ( $sql, 1 );
		
		$memberType = 'S'; // student
		
		$updateExist = false;
		
		if (sizeof ( $row ) == 0) {
			// create
			$fieldnames = "user_id, userName, userEmail, title, virpath, category, permission, location, size, description, ";
			$fieldnames .= " memberType, parentDirID, isDir, dateInput, dateModified, lastModifiedBy ";
			$fieldvalue = "'$ThisUserID', '$user_name', '$UserEmail', '" . addslashes ( $filename ) . "', '$VirPath', 4, 'AR', '$location', '$FileSize', '$description', ";
			$fieldvalue .= " '$memberType', $folderID_Final, 0, now(), now(), '$user_name' ";
			$sql = "INSERT INTO eclass_file ($fieldnames) values ($fieldvalue)";
			$this->db_db_query ( $sql );
		} else {
			// update
			$fileExisted = true; // need not insert another file in handin table if already existed
			$FileID = $row [0] [0];
			$update = " size='$FileSize', description='$description', dateModified=now(), lastModifiedBy='$user_name' ";
			$sql = "UPDATE eclass_file SET $update WHERE FileID='$FileID' ";
			$this->db_db_query ( $sql );
		}
		
		$fm->updateFolderSize ( $folderID_Final );
		
		$sql = "SELECT filename, handin_id FROM handin WHERE assignment_id = $AsgnID and user_id = $ThisUserID";
		$row = $this->returnArray ( $sql, 2 );
		if (count ( $row ) > 0) {
			$fname = $row [0] ['filename'];
			if ($sheettype == "MF" and $fname != "" and $fname != NULL) {
				if ($fileExisted) {
					$file_update = "filename = filename";
				} else {
					$file_update = "filename = CONCAT(filename, ' : ', '$filename'),";
				}
			} else {
				$file_update = "filename='$handin_name',";
			}
			$comment_update = ",comment=NULL";
			$sql = "UPDATE handin SET $file_update inputdate=now() $comment_update WHERE assignment_id = $AsgnID and user_id = $ThisUserID";
		} else {
			$comment_value = ",NULL";
			if ($sheettype == "MF") {
				$sql = "INSERT INTO handin (assignment_id, user_id, filename, inputdate, comment, filepath) VALUES ($AsgnID, $ThisUserID, '$filename', now() $comment_value, '$VirPath')";
			} else {
				$sql = "INSERT INTO handin (assignment_id, user_id, filename, inputdate, comment) VALUES ($AsgnID, $ThisUserID, '$handin_name', now() $comment_value)";
			}
		}
		// echo $sql;
		// $this->db_free_result();
		$this->db_db_query ( $sql );
		
		$ReturnContent = array ();
		$ReturnContent ['isSuccess'] = "1";
		
		return $ReturnContent;
	}
	
	/*
	 * # NOT USED ANYMORE - an old version of UploadAsgn (only submit file type)
	 * function WS_UploadAsgn($CourseID, $UserEmail, $AsgnID, $FileName, $FileByteStream){
	 * global $eclass_db, $eclass_filepath;
	 * include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
	 * include_once("$eclass_filepath/src/includes/php/lib-user.php");
	 * include_once("$eclass_filepath/src/includes/php/lib-filesystem.php");
	 *
	 * $sql = "SELECT user_id FROM $eclass_db.user_course WHERE course_id = $CourseID AND user_email = '$UserEmail'";
	 * $row = $this->returnVector($sql);
	 *
	 * //mkdir("/home/junior20/junior20/eclass30/files/jr20_c357/handin/adb4206691cde708e85c95d2c271ddc3_1163401634Assignment/Assignment/A161");
	 * //return 1;
	 *
	 * if(count($row) > 0){
	 * $ThisUserID = $row[0];
	 *
	 * $li = new phpduoFileSystem();
	 * $fm = new fileManager($CourseID, 4, "");
	 * $fm->file_path = "";
	 *
	 * $lu = new libuser($ThisUserID, $CourseID);
	 * $user_name = addslashes($lu->user_name());
	 *
	 * $this->db = classNamingDB($CourseID);
	 *
	 * // check whether the assignment has been graded
	 * $sql = "SELECT grade FROM handin WHERE assignment_id = $AsgnID and user_id = $ThisUserID";
	 * $row = $this->returnVector($sql, 1);
	 * if(count($row) > 0){
	 * if (trim($row[0])!=""){
	 * return 203;
	 * }
	 * }
	 *
	 * $sql = "SELECT worktype, title, sheettype FROM assignment WHERE assignment_id='$AsgnID' ";
	 * $row = $this->returnArray($sql, 3);
	 *
	 * if(count($row) == 0){
	 * return 103; // assignment not existed
	 * }
	 *
	 * switch($row[0][0]) {
	 * case 1: $folderName = "A".$AsgnID; $folderNameType="Assignment"; break;
	 * case 2: $folderName = "S".$AsgnID; $folderNameType="Survey"; break;
	 * case 3:
	 * case 4:
	 * case 5: $folderName = "P".$AsgnID; $folderNameType="Project"; break;
	 * default: $folderName = "A".$AsgnID; $folderNameType="Assignment"; break;
	 * }
	 * $assignment_title = $row[0][1];
	 * $description = "$folderNameType (".addslashes($row[0][1]).") handin";
	 * $sheettype = $row[0][2];
	 *
	 * $sql = "SELECT class_number, CONCAT(class_number, ' ', lastname, firstname) FROM usermaster WHERE user_id='$ThisUserID' ";
	 * $row = $this->returnArray($sql, 1);
	 * $classNo = ($row[0][0]=="") ? 0 : str_replace(" ", "", $row[0][0]);
	 * //$classNo = ($row[0][0]=="") ? 0 : $row[0][0];
	 * $userName = trim($row[0][1]);
	 *
	 * if(strpos($FileName, ".")==0){ // Fail
	 * return 105;
	 * }
	 * $upload_file_name = $FileName; // from $_POST
	 *
	 * $fm->user_name = "eClass";
	 * $fm->user_email = "null";
	 *
	 * // create folder
	 * $folderID1 = $fm->createFolderDB($folderNameType, $folderNameType, "", 4);
	 *
	 * //create sub-folder (assignment id)
	 * $fm->folderID = $folderID1;
	 * $folderID2 = $fm->createFolderDB($folderName, addslashes($assignment_title), $folderID1, 4);
	 *
	 * //echo $folderName;
	 * if($sheettype == "MF"){
	 * $userFolderName = "{$classNo}_ID{$ThisUserID}";
	 * $filename = $upload_file_name;
	 * $VirPath = "/".$folderNameType."/".$folderName."/". $userFolderName. "/";
	 *
	 * //create sub-sub-folder
	 * $fm->folderID = $folderID2;
	 * $folderID3 = $fm->createFolderDB($userFolderName, $userName, $folderID2, 4);
	 * $folderID_Final = $folderID3;
	 * }
	 * else{
	 * $filename = $classNo."~".$ThisUserID."~".$upload_file_name;
	 * $VirPath = "/".$folderNameType."/".$folderName."/";
	 * $folderID_Final = $folderID2;
	 * }
	 *
	 * //echo $folderID1. ", ". $folderID2. ", ". $folderID3. ", ". $folderID_Final;
	 * $fm->user_name = $user_name;
	 * $fm->user_email = $UserEmail;
	 * $root = $fm->getCategoryRoot(4, $fm->db);
	 *
	 * $FLocation = $fm->genUniqueID($folderID_Final, $FileName);
	 * $folderPath = $root."/".$FLocation;
	 *
	 * $location = $FLocation; //to DB
	 * $handin_name = $VirPath.addslashes($filename);
	 * //echo $folderPath;
	 * $des="$folderPath/".stripslashes($filename); // for text �\, �\
	 * //echo $folderPath;
	 * //echo $folderPath;
	 * //echo $fm->file_path;
	 * $fm->createFolder($folderPath);
	 *
	 * // extract filebyte from cdata
	 * $decoded = base64_decode($FileByteStream);
	 *
	 * $fp = fopen($des, 'wb');
	 * fwrite($fp, $decoded);
	 * fclose($fp);
	 *
	 * $FileSize = filesize($des)/1024; //to KB
	 *
	 * //update file record in file manager
	 * //check exist
	 * $checkParent = ($folderID_Final!="") ? "ParentDirID='".$folderID_Final."'" : "ParentDirID is NULL ";
	 * $sql = "SELECT FileID FROM eclass_file ";
	 * $sql .= " WHERE Title='".addslashes($filename)."' AND $checkParent AND Category='4'";
	 *
	 * //$updateNeed = false;
	 * $row = $this->returnArray($sql, 1);
	 *
	 * $memberType = 'S'; // student
	 *
	 * $updateExist = false;
	 *
	 * if(sizeof($row) == 0){
	 * //create
	 * $fieldnames = "user_id, userName, userEmail, title, virpath, category, permission, location, size, description, ";
	 * $fieldnames .= " memberType, parentDirID, isDir, dateInput, dateModified, lastModifiedBy ";
	 * $fieldvalue = "'$ThisUserID', '$user_name', '$UserEmail', '".addslashes($filename)."', '$VirPath', 4, 'AR', '$location', '$FileSize', '$description', ";
	 * $fieldvalue .= " '$memberType', $folderID_Final, 0, now(), now(), '$user_name' ";
	 * $sql = "INSERT INTO eclass_file ($fieldnames) values ($fieldvalue)";
	 * $this->db_db_query($sql);
	 * }
	 * else{
	 * //update
	 * $fileExisted = true; // need not insert another file in handin table if already existed
	 * $FileID = $row[0][0];
	 * $update = " size='$FileSize', description='$description', dateModified=now(), lastModifiedBy='$user_name' ";
	 * $sql = "UPDATE eclass_file SET $update WHERE FileID='$FileID' ";
	 * $this->db_db_query($sql);
	 * }
	 *
	 * $fm->updateFolderSize($folderID_Final);
	 *
	 * $sql = "SELECT filename, handin_id FROM handin WHERE assignment_id = $AsgnID and user_id = $ThisUserID";
	 * $row = $this->returnArray($sql, 2);
	 * if(count($row) > 0){
	 * $fname = $row[0]['filename'];
	 * if($sheettype == "MF" and $fname != "" and $fname != NULL){
	 * if($fileExisted){
	 * $file_update = "filename = filename";
	 * }
	 * else{
	 * $file_update = "filename = CONCAT(filename, ' : ', '$filename'),";
	 * }
	 * }
	 * else{
	 * $file_update = "filename='$handin_name',";
	 * }
	 * $comment_update = ",comment=NULL";
	 * $sql = "UPDATE handin SET $file_update inputdate=now() $comment_update WHERE assignment_id = $AsgnID and user_id = $ThisUserID";
	 * }else{
	 * $comment_value = ",NULL";
	 * if($sheettype == "MF"){
	 * $sql = "INSERT INTO handin (assignment_id, user_id, filename, inputdate, comment, filepath) VALUES ($AsgnID, $ThisUserID, '$filename', now() $comment_value, '$VirPath')";
	 * }
	 * else{
	 * $sql = "INSERT INTO handin (assignment_id, user_id, filename, inputdate, comment) VALUES ($AsgnID, $ThisUserID, '$handin_name', now() $comment_value)";
	 * }
	 * }
	 * //echo $sql;
	 * //$this->db_free_result();
	 * $this->db_db_query($sql);
	 *
	 * $ReturnContent = array();
	 * $ReturnContent['isSuccess'] = "1";
	 * }
	 * else{
	 * $ReturnContent = '104';
	 * }
	 *
	 * return $ReturnContent;
	 * }
	 */
	
	// Get Subject List
	function WS_GetCourseSubjectList($CourseID) {
		// $hw = new homework($CourseID, "");
		$sql = "SELECT SubjectID, SubjectName FROM INTRANET_SUBJECT WHERE RecordStatus=1 ORDER BY SubjectID ASC";
		// $sql = "SELECT SubjectID, SubjectName FROM INTRANET_SUBJECT ORDER BY SubjectID ASC";
		
		$row = $this->returnArray ( $sql );
		
		$ReturnArray = array ();
		if (count ( row ) > 0) {
			for($i = 0; $i < sizeof ( $row ); $i ++) {
				$SubjectID = $row [$i] [0];
				$SubjectName = $row [$i] [1];
				
				$ReturnContent ['Subject'] [$i] ['SubjectID'] = $SubjectID;
				$ReturnContent ['Subject'] [$i] ['SubjectName'] = intranet_htmlspecialchars ( $SubjectName );
			}
		}
		
		return $ReturnContent;
	}
	
	/*
	 * # NOT USED ANYMORE -- deprecated method, modified to become GetAsgnAttachment
	 * # Get Asgn detail
	 * function WS_GetAsgnDetail($CourseID, $AsgnID){
	 * global $eclass_db, $eclass_filepath, $eclass_httppath;
	 * include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
	 *
	 * $this->db = classNamingDB($CourseID);
	 *
	 * $sql = "SELECT instruction, attachment, Loading, sheettype ";
	 * $sql .= "FROM assignment ";
	 * $sql .= "WHERE assignment_id = '$AsgnID' ";
	 * $sql .= "ORDER BY deadline desc, StartDate desc LIMIT 1";
	 *
	 * $row = $this->returnArray($sql);
	 * if(count($row) > 0){
	 * $ReturnContent = array();
	 *
	 * $Instruction = $row[0][0];
	 * $Attachment = $row[0][1];
	 * $Loading = $row[0][2];
	 * $Sheettype = $row[0][3];
	 * if ((strpos($Attachment,"?\\")>0) || (strpos($Attachment,"?\\")>0))
	 * $files = split("\:",$Attachment);
	 * else
	 * $files = split("\:",stripslashes($Attachment));
	 *
	 * for($i = 0; $i < count($files); $i++){
	 * $filename = basename(trim($files[$i]));
	 * $filepath = $this->Get_File_FullPath(trim($files[$i]), $CourseID, 3);
	 * //$ReturnContent['Attachments']['File'][$i] = intranet_htmlspecialchars($filepath);
	 * $ReturnContent['Attachments']['File'][$i]['Filename'] = $filename;
	 * $ReturnContent['Attachments']['File'][$i]['Filepath'] = intranet_htmlspecialchars($filepath);
	 * }
	 *
	 * //$ReturnContent['Instruction'] = intranet_htmlspecialchars($Instruction);
	 * //$ReturnContent['Loading'] = $Loading;
	 * //$ReturnContent['Sheettype'] = $Sheettype;
	 * }
	 * else{
	 * $ReturnContent = '103';
	 * }
	 * //debug_r($ReturnContent);
	 * return $ReturnContent;
	 * }
	 */
	
	// selectType = 6 2011-07-06: a simple assignment list specially designed for ASL
	// [IP2.5] totally rewrote for homework list of IP 2.5
	function WS_GetHomeworkList($CourseID, $ThisUserID, $UserName, $HwType, $SubjectID, $Timestamp, $AsgnID, $StartDate, $Status, $Deadline) {
		global $intranet_session_language;
		
		$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '$UserName'";
		$row = $this->returnVector ( $sql );
		$childrenID = $row [0];
		
		$sql = "SELECT TypeName, TypeID FROM INTRANET_HOMEWORK_TYPE";
		$row = $this->returnArray ( $sql, null, true );
		for($i = 0; $i < sizeof ( $row ); $i ++) {
			$HWType [$row [$i] ["TypeID"]] = trim ( $row [$i] ["TypeName"] );
		}
		
		$today = date ( 'Y-m-d' );
		$yearID = Get_Current_Academic_Year_ID ();
		
		// Current Year Term
		$currentYearTerm = getAcademicYearAndYearTermByDate ( $today );
		$yearTermID = $currentYearTerm [0];
		if ($yearTermID == "")
			$yearTermID = 0;
			
			// SQL statement
		$date_conds = " AND a.DueDate >= CURDATE()";
		if ($StartDate != "") {
			$date_conds .= " AND unix_timestamp(a.StartDate)=unix_timestamp('{$StartDate}') ";
		}
		$conds = ($childrenID == '') ? "" : " AND d.UserID = $childrenID";
		$conds .= ($subjectID == '') ? " $allSubjects" : " AND a.SubjectID = $subjectID ";
		
		$conds .= ($s == '') ? "" : " AND (IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) like '%$s%'
									or a.Title like '%$s%'
									$searchByTeacher
									$searchBySubject
								)";
		
		$desc = "a.Description,";
		
		$fields = "IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject, 
				   IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup, 
				   a.Title, a.Description, 
				   a.StartDate, a.DueDate, a.Loading/2 AS LoadingIndex,  a.HandinRequired , IF(e.RecordStatus=1, 'S', IF(e.RecordStatus=-1, 'N', IF(e.RecordStatus=2, 'N','--'))) AS HandinStatus";
		
		$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID 
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS d ON d.SubjectGroupID = a.ClassGroupID
					 LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.HomeworkID = a.HomeworkID AND e.StudentID = d.UserID)";
		
		$conds .= " $date_conds AND a.AcademicYearID = $yearID";
		$conds .= " AND (a.YearTermID = $yearTermID OR a.DueDate>=CURDATE())";
		
		$sql = "SELECT $fields , a.HomeworkID, a.LastModified, a.TypeID FROM $dbtables WHERE 1 $conds ORDER BY a.StartDate desc, a.DueDate desc";
		$homeworks = $this->returnArray ( $sql, null, true );
		
		$ReturnContent = array ();
		for($i = 0; $i < sizeof ( $homeworks ); $i ++) {
			$hwobj = $homeworks [$i];
			$ReturnContent ['Asgn'] [$i] ['AsgnID'] = $hwobj ["HomeworkID"];
			$ReturnContent ['Asgn'] [$i] ['Title'] = $hwobj ["Title"];
			
			$ReturnContent ['Asgn'] [$i] ['InputDate'] = $hwobj ["LastModified"];
			$ReturnContent ['Asgn'] [$i] ['Status'] = $hwobj ["HandinStatus"];
			$ReturnContent ['Asgn'] [$i] ['SubjectName'] = $hwobj ["Subject"];
			$ReturnContent ['Asgn'] [$i] ['SubjectID'] = $hwobj ["SubjectID"];
			$ReturnContent ['Asgn'] [$i] ['WorkType'] = 0;
			$ReturnContent ['Asgn'] [$i] ['WorkTypeDesc'] = $HWType [$hwobj ["TypeID"]]; // [IP2.5]
			$ReturnContent ['Asgn'] [$i] ['StartDate'] = $hwobj ["StartDate"] . " 00:00:00";
			$ReturnContent ['Asgn'] [$i] ['Deadline'] = $hwobj ["DueDate"] . " 23:59:59";
			$ReturnContent ['Asgn'] [$i] ['Instruction'] = $hwobj ["Description"];
			$ReturnContent ['Asgn'] [$i] ['Loading'] = $hwobj ["LoadingIndex"];
			$ReturnContent ['Asgn'] [$i] ['DateModified'] = $hwobj ["LastModified"];
			/*
			 * $ReturnContent['Asgn'][$i]['Sheettype'] = $hwobj["HomeworkID"];
			 * $ReturnContent['Asgn'][$i]['AnswerSheet'] = $hwobj["HomeworkID"];
			 *
			 * if($grade != ''){
			 * $ReturnContent['Asgn'][$i]['ModelAnswer'] = intranet_htmlspecialchars($modelanswer);
			 * }
			 * else{
			 * $ReturnContent['Asgn'][$i]['ModelAnswer'] = '';
			 * }
			 */
		}
		return $ReturnContent;
		
		/*
		 *
		 * global $eclass_db;
		 *
		 * if($ThisUserID != '')
		 * $sql = "SELECT UserEmail FROM INTRANET_USER WHERE UserID = '$ThisUserID'";
		 * else
		 * $sql = "SELECT UserEmail FROM INTRANET_USER WHERE UserLogin = '$UserName'";
		 *
		 * $row = $this->returnVector($sql);
		 * $UserEmail = $row[0];
		 *
		 * ## 2011-07-28: if no time is specified in the deadline: add 23:59:59 to it
		 * if($Deadline != '' and strlen($Deadline) == 10 ){
		 * $Deadline .= ' 23:59:59';
		 * }
		 *
		 *
		 * ## 2011-07-18: if no CourseID is given, find all courses and return all assignments
		 * ## NOTE: AsgnID, SubjectID, etc... may duplicateds across courses
		 * if($CourseID == ''){
		 * $sql = "SELECT a.course_id, a.course_code, a.course_name
		 * FROM {$eclass_db}.course AS a, {$eclass_db}.user_course AS b
		 * WHERE
		 * a.course_id = b.course_id AND b.user_email = '$UserEmail' AND a.RoomType=0 AND
		 * (b.status is NULL OR b.status NOT IN ('deleted'))
		 * GROUP BY a.course_id
		 * ORDER BY a.course_code, a.course_name";
		 * $courseList = $this->returnArray($sql);
		 *
		 * $returnContent = array();
		 * foreach($courseList as $c){
		 * //$asgnList = $this->GetAsgnCommon($c['course_id'], $UserEmail, $HwType, $SubjectID, $Timestamp, 6, $AsgnID, $StartDate, $Status, $Deadline);
		 * $countAsgn = count($asgnList['Asgn']);
		 * for($i = 0; $i < $countAsgn; $i++){
		 * $asgnList['Asgn'][$i]['CourseID'] = $c['course_id'];
		 * $asgnList['Asgn'][$i]['CourseName'] = $c['course_name'];
		 *
		 * $returnContent['Asgn'][] = $asgnList['Asgn'][$i];
		 * }
		 * }
		 * return $returnContent;
		 * }
		 * else
		 * return $this->GetAsgnCommon($CourseID, $UserEmail, $HwType, $SubjectID, $Timestamp, 6, $AsgnID, $StartDate, $Status, $Deadline);
		 */
	}

	function WS_GetHomeworkListForApp($targetStudentId, $currentUserId) {
		global $special_feature, $PATH_WRT_ROOT;
		
		include_once ($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
		$libeClassApp = new libeClassApp();
		
		// get student studying subject group
		$sql = "SELECT
						st.SubjectGroupID
				FROM
						ACADEMIC_YEAR_TERM as ayt
						INNER JOIN SUBJECT_TERM as st ON (ayt.YearTermID = st.YearTermID)
						INNER JOIN SUBJECT_TERM_CLASS_USER as stu ON (st.SubjectGroupID = stu.SubjectGroupID)
				WHERE
						ayt.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
						AND stu.UserID = '".$targetStudentId."'
				";
		$subjectGroupIdAry = $this->returnVector($sql);
		
		// get homework of subject group
		$sql = "SELECT
						h.HomeworkID, 
						h.Title, 
						h.Description, 
						h.StartDate, 
						h.DueDate, 
						CASE hhl.RecordStatus
						    WHEN -1 THEN 'N'
							WHEN 1 THEN 'Y'
							WHEN 2 THEN 'Y'
						    ELSE '---'
						END as HandinStatus,
						s.EN_DES as SubjectNameEn,
						s.CH_DES as SubjectNameCh,
						h.Loading as Workload
				FROM
						INTRANET_HOMEWORK as h
						INNER JOIN ASSESSMENT_SUBJECT as s ON (h.SubjectID = s.RecordID)
						LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST hhl ON (h.HomeworkID = hhl.HomeworkID AND hhl.StudentID = '" . $targetStudentId . "')
				WHERE
						h.ClassGroupID IN ('".implode("','", (array)$subjectGroupIdAry)."')
				ORDER BY
						h.StartDate DESC, h.DueDate
						/*h.DueDate*/
				";
		$homeworkAry = $this->returnResultSet($sql);
		$numOfHomework = count($homeworkAry);
		
		$schoolDomain = curPageURL($withQueryString = false, $withPageSuffix = false);
		$returnAry = array();
		$returnAry['Homework'] = array();
		for($i=0; $i < $numOfHomework; $i++) {
			$_homeworkId = $homeworkAry[$i]['HomeworkID'];
			$_title = $homeworkAry[$i]['Title'];
			$_description = $homeworkAry[$i]['Description'];
			$_startDate = $homeworkAry[$i]['StartDate'];
			$_dueDate = $homeworkAry[$i]['DueDate'];
			$_handinStatus = $homeworkAry[$i]['HandinStatus'];
			$_subjectNameEn = $homeworkAry[$i]['SubjectNameEn'];
			$_subjectNameCh = $homeworkAry[$i]['SubjectNameCh'];
			$_workload = $homeworkAry[$i]['Workload'] / 2;
			
			// prepare token for verification
			$_delimiter = "###";
			$_fromModule = 'eHomework';
			$_keyStr = $special_feature['ParentApp_PrivateKey'] . $_delimiter . $_homeworkId . $_delimiter . $_fromModule . $_delimiter . $targetStudentId . $_delimiter . $currentUserId;
			$_token = md5($_keyStr);
			$_viewUrl = $schoolDomain . '/api/homework_view.php?token=' . $_token . '&HomeworkID=' . $_homeworkId . '&fromModule=' . $_fromModule . '&StudentID=' . $targetStudentId . '&CurID=' . $currentUserId;
			
			$_tmpAry = array();
			$_tmpAry['HomeworkID'] = $_homeworkId;
			$_tmpAry['Title'] = $libeClassApp->standardizePushMessageText($_title);
			$_tmpAry['Description'] = $libeClassApp->standardizePushMessageText($_description);
			$_tmpAry['StartDate'] = $_startDate;
			$_tmpAry['StartDateTs'] = strtotime($_startDate);
			$_tmpAry['DueDate'] = $_dueDate . ' 23:59:59';
			$_tmpAry['DueDateTs'] = strtotime($_dueDate);
			$_tmpAry['HandinStatus'] = $_handinStatus;
			$_tmpAry['ViewURL'] = $_viewUrl;
			$_tmpAry['fromModule'] = $_fromModule;
			$_tmpAry['SubjectNameEn'] = $_subjectNameEn;
			$_tmpAry['SubjectNameCh'] = $_subjectNameCh;
			$_tmpAry['Workload'] = $_workload;
			
			$returnAry['Homework'][] = $_tmpAry;
		}
		
		return $returnAry;
	}
	
	function WS_GetHomeworkImageForApp($targetUserId, $parGetAllHomework=true) {
		
		$school_domain = curPageURL ( $withQueryString = false, $withPageSuffix = false );
		
		if (substr ( $school_domain, - 1 ) == "/")
			$school_domain = substr ( $school_domain, 0, - 1 );
		
		# retrieve UserID (parent + active)
    	$sql = "SELECT ycu.YearClassID from YEAR_CLASS_USER as ycu  
					Inner JOIN YEAR_CLASS yc on ycu.YearClassID = yc.YearClassID where yc.AcademicYearID='" . Get_Current_Academic_Year_ID () . "' and ycu.UserID = $targetUserId";
    	$rs = $this->returnResultSet($sql);
    	$studentClassId = $rs[0]['YearClassID'];
    	    	
    	if($studentClassId!=""){
			// A91363 - view 1 year homework image
			if ($parGetAllHomework) {
				$dateInterval = "365";
			} else {
				$dateInterval = "7";
			}
					
    		$sql = "select Date,FilePath from INTRANET_HOMEWORK_IMAGE where CourseID = ".$studentClassId." and DATE(Date) <= DATE(NOW())
									AND DATE(Date) >= DATE_SUB(DATE(NOW()), INTERVAL $dateInterval day) ";   		
    		$FilePathAry = $this->returnResultSet($sql); 
    		$numOfFilePathAry = count($FilePathAry);   		   		
	
		    $returnAry = array();
	    	$returnAry['ClassHwImagePath'] = array();
	    	
			if ($numOfFilePathAry > 0) {
				for ($i=0; $i<$numOfFilePathAry; $i++) {

						$__Date = $FilePathAry[$i]['Date'];
						$__FilePath = $FilePathAry[$i]['FilePath'];
						
						$__returnPath = '';
						$__returnPath .= $school_domain."/".$__FilePath;
												
						$__tmpAry = array();
						$__tmpAry['date'] = $__Date;
						$__tmpAry['url'] = $__returnPath;
						$returnAry['ClassHwImagePath'][] = $__tmpAry;
				}
			}
		}
	    return $returnAry;
	}
	
	// selectType = 5
	function WS_GetAsgnMarkedFile($CourseID, $UserEmail, $HwType, $SubjectID, $Timestamp, $AsgnID) {
		return $this->GetAsgnCommon ( $CourseID, $UserEmail, $HwType, $SubjectID, $Timestamp, 5, $AsgnID );
	}
	// selectType = 4
	function WS_GetAsgnTemplate($CourseID, $UserEmail, $HwType, $SubjectID, $Timestamp, $AsgnID) {
		return $this->GetAsgnCommon ( $CourseID, $UserEmail, $HwType, $SubjectID, $Timestamp, 4, $AsgnID );
	}
	// Henry Yuen (2010-06-02), web service for getting submitted assignments
	// selectType = 3
	function WS_GetAsgnSubmitted($CourseID, $UserEmail, $HwType, $SubjectID, $Timestamp, $AsgnID) {
		return $this->GetAsgnCommon ( $CourseID, $UserEmail, $HwType, $SubjectID, $Timestamp, 3, $AsgnID );
	}
	// selectType = 2
	function WS_GetAsgnAttachment($CourseID, $UserEmail, $HwType, $SubjectID, $Timestamp, $AsgnID) {
		return $this->GetAsgnCommon ( $CourseID, $UserEmail, $HwType, $SubjectID, $Timestamp, 2, $AsgnID );
	}
	// selectType = 1
	function WS_GetUserAsgnList($CourseID, $UserEmail, $HwType, $SubjectID, $Timestamp, $AsgnID) {
		return $this->GetAsgnCommon ( $CourseID, $UserEmail, $HwType, $SubjectID, $Timestamp, 1, $AsgnID );
	}
	
	// This method is shared by GetUserAsgnList, GetUserAsgnAttachment and GetAsgnSubmitted, GetAsgnTemplate
	// Their difference is the selectType (1, 2, 3, 4), indicating selection of different fields
	// Timestamp is used to compare 1) assignment updated time (for GetUserAsngList and GetAsgnAttachment and GetAsgnTemplate)
	// 2) handin markeddate (for GetAsgnSubmitted)
	function GetAsgnCommon($CourseID, $UserEmail, $HwType, $SubjectID, $Timestamp, $SelectType, $AsgnID, $StartDate = '', $In_Status = '', $Deadline = '') {
		global $eclass_db, $eclass_filepath, $ischoolbag_website, $ischoolbag_file_path, $WV_path, $intranet_db;
		include_once ("$eclass_filepath/src/includes/php/lib-groups.php");
		include_once ("$eclass_filepath/src/includes/php/lib-filemanager.php");
		// include_once("$eclass_filepath/src/includes/php/lib_homework.php");
		
		$sql = "SELECT user_id FROM $eclass_db.user_course WHERE course_id = $CourseID AND user_email = '$UserEmail'";
		$row = $this->returnVector ( $sql );
		
		if (count ( $row ) > 0) {
			$ThisUserID = $row [0];
			
			$lo = new libgroups ( classNamingDB ( $CourseID ) );
			$exceptionList = $lo->returnFunctionIDs ( "A", $ThisUserID );
			
			$hw = new homework ( $CourseID, "" );
			// $content_show = $hw->getHWList($subject_id, $hw_type, $exceptionList);
			
			// $mark_save = "%SAVE%";
			
			if (trim ( $Timestamp ) == "") {
				$chkTimestamp = "1 = 1";
			} else {
				if ($SelectType == 1 or $SelectType == 2 or $SelectType == 4 or $SelectType == 6) {
					$chkTimestamp = "UNIX_TIMESTAMP(a.modified) > $Timestamp";
				} elseif ($SelectType == 3 or $SelectType == 5) {
					$chkTimestamp = "UNIX_TIMESTAMP(b.markeddate) > $Timestamp";
				}
			}
			
			if (trim ( $HwType ) == "") {
				// get all type
				$chkWorktype = "1 = 1";
			} else {
				$chkWorktype = "a.worktype='$HwType'";
			}
			
			if (trim ( $SubjectID ) == "") {
				// get all subject
				$chkSubjectID = "1 = 1";
			} else {
				$chkSubjectID = "a.SubjectID = '$SubjectID'";
			}
			
			if (trim ( $AsgnID ) == '') {
				// get all Asgn
				$chkAsgnID = '1 = 1';
			} else {
				$chkAsgnID = 'a.assignment_id = ' . $AsgnID;
			}
			
			if (trim ( $StartDate ) == '') {
				$chkStartDate = '1 = 1';
			} else {
				$chkStartDate = 'cast(a.StartDate as date) = \'' . $StartDate . '\'';
			}
			
			if ($In_Status == 'S') {
				$chkStatus = '(b.handin_id IS NOT NULL AND b.comment != \'%SAVE%\')';
			} elseif ($In_Status == 'N') {
				$chkStatus = '(b.handin_id IS NULL OR b.comment = \'%SAVE%\')';
			} else {
				$chkStatus = '1=1';
			}
			
			if ($Deadline == '') {
				$chkDeadline = '1=1';
			} else {
				$chkDeadline = 'a.deadline < \'' . $Deadline . '\'';
			}
			
			$hw = new homework ( $CourseID, "" );
			$hw->getSettings ();
			if ($hw->valid_date != '') {
				$chkValidDate = "UNIX_TIMESTAMP(DATE_FORMAT(a.deadline, '%Y-%m-%d')) >= UNIX_TIMESTAMP(DATE_FORMAT('" . $hw->valid_date . "', '%Y-%m-%d'))";
			} else {
				$chkValidDate = '1 = 1';
			}
			
			$student_restriction = "AND UNIX_TIMESTAMP(now())>=UNIX_TIMESTAMP(a.StartDate)";
			$SemestBoundary = $hw->getSemesterBoundary ();
			if ($SemestBoundary != "") {
				$student_restriction .= " AND UNIX_TIMESTAMP('{$SemestBoundary}')<UNIX_TIMESTAMP(a.deadline) ";
			}
			$assignment_exception = ($exceptionList != "") ? "AND a.assignment_id NOT IN ($exceptionList)" : "";
			
			$sql = "SELECT a.assignment_id, a.title, b.inputdate, b.comment, b.status, a.worktype, a.StartDate, a.deadline, a.SubjectID, a.instruction, a.loading, a.sheettype, a.modified, a.attachment, a.answersheet, b.handin_id, b.filename as submittedFiles, b.filename_mark, b.grade, b.is_correction, b.filepath AS submittedFolderPath, b.answer AS submittedAnswer, b.answer_correction AS submittedAnswerCorrection, a.template, b.answer_marked AS submittedAnswerMarked, b.markeddate, a.modelanswer, b.filename_mark, s.SubjectName";
			// $sql = "SELECT a.assignment_id, CONCAT(a.title, if(b.inputdate<>'', if(b.comment='$mark_save',' <span class=guide>($assignment_save)</span>', ' <span class=guide>($assignment_done)</span>'), if(b.status='LR', ' <font color=red>[$button_redo]</font>', ''))), a.worktype, ".format_datetime_weekday_sql('a.StartDate').", ".format_datetime_weekday_sql('a.deadline').", if(((UNIX_TIMESTAMP(now())>UNIX_TIMESTAMP(a.deadline) AND a.worktype<>14) OR (UNIX_TIMESTAMP(now())>UNIX_TIMESTAMP(a.StartDate)+86400 AND a.worktype=14)), 1, 0) ";
			$sql .= " FROM " . $hw->ec_db . ".assignment as a LEFT JOIN " . $hw->ec_db . ".handin AS b ON (b.assignment_id=a.assignment_id AND b.user_id='$ThisUserID') ";
			$sql .= ' INNER JOIN INTRANET_SUBJECT s ON a.subjectid = s.subjectid';
			$sql .= " WHERE s.RecordStatus = 1 AND $chkTimestamp AND $chkSubjectID AND $chkWorktype AND $chkAsgnID AND $chkValidDate AND $chkStartDate AND $chkStatus AND $chkDeadline AND a.status=1 AND a.StartDate IS NOT NULL $student_restriction $assignment_exception ";
			$sql .= " ORDER BY a.StartDate desc, a.deadline desc ";
			$row = $this->returnArray ( $sql, 6 );
			
			$ReturnContent = array ();
			if (count ( $row ) > 0) {
				for($i = 0; $i < sizeof ( $row ); $i ++) {
					if ($SelectType == 1) { // select assignment basic details
						$assignment_id = $row [$i] ['assignment_id'];
						$title = $row [$i] ['title'];
						$inputdate = $row [$i] ['inputdate'];
						$grade = $row [$i] ['grade'];
						$comment = $row [$i] ['comment'];
						$status = $row [$i] ['status'];
						$worktype = $row [$i] ['worktype'];
						$StartDate = $row [$i] ['StartDate'];
						$deadline = $row [$i] ['deadline'];
						$subject_id = $row [$i] ['SubjectID'];
						$instruction = $row [$i] ['instruction'];
						$loading = $row [$i] ['loading'];
						$sheettype = $row [$i] ['sheettype'];
						$modified = $row [$i] ['modified'];
						$answersheet = $row [$i] ['answersheet'];
						$modelanswer = $row [$i] ['modelanswer'];
						
						$ReturnContent ['Asgn'] [$i] ['AsgnID'] = $assignment_id;
						// # 2011-08-04: title field is already htmlspecialchars() before saving into DB <--- not a good practice tough!
						// # $ReturnContent['Asgn'][$i]['Title'] = intranet_htmlspecialchars($title);
						$ReturnContent ['Asgn'] [$i] ['Title'] = $title;
						
						$ReturnContent ['Asgn'] [$i] ['InputDate'] = $inputdate;
						$ReturnContent ['Asgn'] [$i] ['Grade'] = $grade;
						$ReturnContent ['Asgn'] [$i] ['Comment'] = $comment;
						$ReturnContent ['Asgn'] [$i] ['Status'] = $status;
						$ReturnContent ['Asgn'] [$i] ['SubjectID'] = $subject_id;
						$ReturnContent ['Asgn'] [$i] ['WorkType'] = $worktype;
						$ReturnContent ['Asgn'] [$i] ['StartDate'] = $StartDate;
						$ReturnContent ['Asgn'] [$i] ['Deadline'] = $deadline;
						$ReturnContent ['Asgn'] [$i] ['Instruction'] = $instruction;
						$ReturnContent ['Asgn'] [$i] ['Loading'] = $loading;
						$ReturnContent ['Asgn'] [$i] ['Sheettype'] = $sheettype;
						$ReturnContent ['Asgn'] [$i] ['DateModified'] = $modified;
						$ReturnContent ['Asgn'] [$i] ['AnswerSheet'] = $answersheet;
						
						if ($grade != '') {
							$ReturnContent ['Asgn'] [$i] ['ModelAnswer'] = intranet_htmlspecialchars ( $modelanswer );
						} else {
							$ReturnContent ['Asgn'] [$i] ['ModelAnswer'] = '';
						}
					} elseif ($SelectType == 2) { // select assignment attachement
						$assignment_id = $row [$i] ['assignment_id'];
						$subject_id = $row [$i] ['SubjectID'];
						$Attachment = $row [$i] ['attachment'];
						$modified = $row [$i] ['modified'];
						$answersheet = $row [$i] ['answersheet'];
						
						$ReturnContent ['Asgn'] [$i] ['AsgnID'] = $assignment_id;
						$ReturnContent ['Asgn'] [$i] ['SubjectID'] = $subject_id;
						
						if ($Attachment != "") {
							if ((strpos ( $Attachment, "?\\" ) > 0) || (strpos ( $Attachment, "?\\" ) > 0)) {
								// $files = split("\:",$Attachment);
								$files = explode ( "\:", $Attachment );
							} else {
								// $files = split("\:",stripslashes($Attachment));
								// $files = split("\:",big5_stripslashes($Attachment));
								$files = explode ( "\:", big5_stripslashes ( $Attachment ) );
							}
							for($j = 0; $j < count ( $files ); $j ++) {
								$filename = basename ( trim ( $files [$j] ) );
								$filepath = $this->Get_File_FullPath ( trim ( $files [$j] ), $CourseID, 3 );
								
								// $ReturnContent['Attachments']['File'][$i] = intranet_htmlspecialchars($filepath);
								
								// if answersheet attachment is .doc, then convert it to html
								if ($answersheet == '===writing===') { // essay type
								} elseif ($answersheet == '') { // uploadfile type
								} else { // answersheet type
									$fm = new fileManager ( $CourseID, 3, '' ); // Category 3 is not useful at the moment
									$fm->file_path = '';
									if ($fm->getFileExtention ( $filename, $isLower = 1 ) == 'doc') {
										$hashedName = md5 ( $filename . $modified );
										$hashedAttachmentFolder = classNamingDB ( $CourseID ) . "/tmp/ischoolbag/" . $hashedName;
										$tmpFolder = $eclass_filepath . '/files/' . $hashedAttachmentFolder . '/';
										// 2010-11-19: avoid chinese filename
										// $newfilename = $filename. '.html';
										$newDocFilepath = $tmpFolder . $hashedName . '.doc';
										$newfilename = $hashedName . '.html';
										// $newfilename = 'a.html';
										$localfilepath = $this->Get_File_FullPath ( trim ( $files [$j] ), $CourseID, 3, $isLocal = 1 );
										
										$fm->createFolder ( $tmpFolder );
										
										// 2010-11-22: rename file to remove chinese characters before running wvHtml command
										if (! file_exists ( $newDocFilepath )) {
											copy ( $localfilepath, $newDocFilepath );
											// $exec = "$WV_path/wvHtml --targetdir=\"$tmpFolder\" \"$localfilepath\" \"$newfilename\"";
											$exec = OsCommandSafe($WV_path)."/wvHtml --targetdir=\"".OsCommandSafe($tmpFolder)."\" \"".OsCommandSafe($newDocFilepath)."\" \"".OsCommandSafe($newfilename)."\"";
											$r = exec ( $exec );
											// echo $r. '<br>';
										}
										
										// $filename = $newfilename;
										$filename = $filename . '.html';
										$filepath = $ischoolbag_website . $ischoolbag_file_path . "/" . $hashedAttachmentFolder . '/' . $newfilename;
									}
								}
								$ReturnContent ['Asgn'] [$i] ['Attachments'] ['File'] [$j] ['Filename'] = intranet_htmlspecialchars ( $filename );
								$ReturnContent ['Asgn'] [$i] ['Attachments'] ['File'] [$j] ['Filepath'] = intranet_htmlspecialchars ( $filepath );
							}
						} else {
							$ReturnContent ['Asgn'] [$i] ['Attachments'] = "";
						}
					} elseif ($SelectType == 3) { // select submitted assignments
						include_once ('libdownloader.php');
						$libdownloader = new libdownloader ();
						
						$assignment_id = $row [$i] ['assignment_id'];
						$submittedFiles = $row [$i] ['submittedFiles'];
						$submittedAnswer = $row [$i] ['submittedAnswer'];
						$submittedAnswerMarked = $row [$i] ['submittedAnswerMarked'];
						$submittedAnswerCorrection = $row [$i] ['submittedAnswerCorrection'];
						// $filename_mark = $row[$i]['filename_mark'];
						$submittedFolderPath = $row [$i] ['submittedFolderPath'];
						$answersheet = $row [$i] ['answersheet'];
						$inputdate = $row [$i] ['inputdate'];
						$markeddate = $row [$i] ['markeddate'];
						$modelanswer = $row [$i] ['modelanswer'];
						
						$ReturnContent ['Asgn'] [$i] ['AsgnID'] = $assignment_id;
						$ReturnContent ['Asgn'] [$i] ['IsCorrection'] = $row [$i] ['is_correction'];
						$ReturnContent ['Asgn'] [$i] ['InputDate'] = $inputdate;
						// $ReturnContent['Asgn'][$i]['filename_mark'] = $filename_mark;
						$ReturnContent ['Asgn'] [$i] ['Grade'] = $row [$i] ['grade'];
						// $ReturnContent['Asgn'][$i]['is_correction'] = $row[$i]['is_correction'];
						// $ReturnContent['Asgn'][$i]['filepath'] = $row[$i]['filepath'];
						$ReturnContent ['Asgn'] [$i] ['HandinID'] = $row [$i] ['handin_id'];
						$ReturnContent ['Asgn'] [$i] ['Comment'] = $row [$i] ['comment'];
						$ReturnContent ['Asgn'] [$i] ['MarkedDate'] = $markeddate;
						
						if ($row [$i] ['grade'] != '') {
							$ReturnContent ['Asgn'] [$i] ['ModelAnswer'] = intranet_htmlspecialchars ( $modelanswer );
						} else {
							$ReturnContent ['Asgn'] [$i] ['ModelAnswer'] = '';
						}
						
						if ($answersheet == '===writing===') { // composition type homework
							if ($row [$i] ['handin_id'] != '') {
								// Get UserID
								$sql2 = "SELECT UserID FROM $intranet_db.INTRANET_USER WHERE UserEmail = '$UserEmail'";
								$row2 = $this->returnVector ( $sql2 );
								$ThisUserID = $row2 [0];
								
								$contentType = 'text/html';
								$module = 'isb';
								$timeout = 120;
								$returnError = false;
								$unlimitedDownload = false;
								
								// if($row[$i]['is_correction'] == 0 or $submittedAnswerMarked == ''){# use answer field when 1) not in correction state, 2) first time in correction state, teacher not submitted anything yet (answer_mark is empty)
								if ($submittedAnswerMarked == '') {
									$filecontent = $submittedAnswer;
								} else {
									$filecontent = $submittedAnswerMarked;
								}
								
								// $filecontent = stripslashes($filecontent);
								// 2011-03-23: handle GBK
								// $filecontent = iconv('Big5', 'UTF8', $filecontent); // convert to UTF8 (ischoolbag client use UTF8)
								$filecontent = ischoolbag_iconv ( $filecontent, true );
								
								$filecontent = intranet_undo_htmlspecialchars ( $filecontent );
								
								$filename = $CourseID . $assignment_id . 'answer.html'; // only for hashing token
								$tokenpath = $libdownloader->createToken ( $libdownloader->TYPE_FILECONTENT, $filename, $filecontent, $module, $ThisUserID, $contentType, $timeout, $returnError, $unlimitedDownload );
								
								if ($tokenpath) {
									$path = $ischoolbag_website . '/' . $tokenpath;
									$ReturnContent ['Asgn'] [$i] ['SubmittedFiles'] ['File'] [0] ['Filename'] = '';
									$ReturnContent ['Asgn'] [$i] ['SubmittedFiles'] ['File'] [0] ['Filepath'] = intranet_htmlspecialchars ( $path );
								} else {
									$ReturnContent ['Asgn'] [$i] ['SubmittedFiles'] = '';
								}
								
								if ($row [$i] ['is_correction'] > 0) {
									// use answer field if student not yet submitted any correction (answer_correction field is empty)
									if ($submittedAnswerCorrection == '') {
										$filecontent = intranet_undo_htmlspecialchars ( $submittedAnswer );
									} else {
										$filecontent = intranet_undo_htmlspecialchars ( $submittedAnswerCorrection );
									}
									// 2011-03-23: handle GBK
									// $filecontent = iconv('Big5', 'UTF8', $filecontent); // convert to UTF8 (ischoolbag client use UTF8)
									$filecontent = ischoolbag_iconv ( $filecontent, true );
									
									$filename = $CourseID . $assignment_id . 'answer_correction.html'; // only for hashing token
									$tokenpath = $libdownloader->createToken ( $libdownloader->TYPE_FILECONTENT, $filename, $filecontent, $module, $ThisUserID, $contentType, $timeout, $returnError, $unlimitedDownload );
									if ($tokenpath) {
										$path = $ischoolbag_website . '/' . $tokenpath;
										$ReturnContent ['Asgn'] [$i] ['Correction'] ['File'] [0] ['Filename'] = '';
										$ReturnContent ['Asgn'] [$i] ['Correction'] ['File'] [0] ['Filepath'] = intranet_htmlspecialchars ( $path );
									} else {
										$ReturnContent ['Asgn'] [$i] ['Correction'] = '';
									}
								}
							} else {
								$ReturnContent ['Asgn'] [$i] ['SubmittedFiles'] = '';
								$ReturnContent ['Asgn'] [$i] ['Correction'] = '';
							}
						} elseif ($answersheet == '') { // upload files type homework
							if ($submittedFiles != "") {
								if (strpos ( $submittedFiles, "?\\" ) > 0)
									// $files = split("\:",$submittedFiles);
									$files = explode ( "\:", $submittedFiles );
								else
									// $files = split("\:",big5_stripslashes($submittedFiles));
									$files = explode ( "\:", big5_stripslashes ( $submittedFiles ) );
								
								for($j = 0; $j < count ( $files ); $j ++) {
									$filename = basename ( trim ( $files [$j] ) );
									$filepath = $this->Get_File_FullPath ( $submittedFolderPath . trim ( $files [$j] ), $CourseID, 4 );
									// $ReturnContent['Attachments']['File'][$i] = intranet_htmlspecialchars($filepath);
									$ReturnContent ['Asgn'] [$i] ['SubmittedFiles'] ['File'] [$j] ['Filename'] = intranet_htmlspecialchars ( $filename );
									$ReturnContent ['Asgn'] [$i] ['SubmittedFiles'] ['File'] [$j] ['Filepath'] = intranet_htmlspecialchars ( $filepath );
								}
							} else {
								$ReturnContent ['Asgn'] [$i] ['SubmittedFiles'] = "";
							}
						}  // answersheet type
else {
							if ($row [$i] ['handin_id'] != '') {
								// Get UserID
								$sql2 = "SELECT UserID FROM $intranet_db.INTRANET_USER WHERE UserEmail = '$UserEmail'";
								$row2 = $this->returnVector ( $sql2 );
								// echo $sql2;
								// debug_r($row2);
								$ThisUserID = $row2 [0];
								
								$contentType = 'text/plain';
								$module = 'isb';
								$timeout = 120;
								$returnError = false;
								$unlimitedDownload = false;
								
								// use answer field is 1) not in correction state, 2) first time in correction state, teacher not submitted anything yet (answer_mark is empty)
								$filecontent = intranet_undo_htmlspecialchars ( $submittedAnswer );
								
								// 2011-03-23: handle GBK
								// $filecontent = iconv('Big5', 'UTF8', $filecontent); // convert to UTF8 (ischoolbag client use UTF8)
								$filecontent = ischoolbag_iconv ( $filecontent, true );
								
								$filename = $CourseID . $assignment_id . 'answer.txt'; // only for hashing token
								$tokenpath = $libdownloader->createToken ( $libdownloader->TYPE_FILECONTENT, $filename, $filecontent, $module, $ThisUserID, $contentType, $timeout, $returnError, $unlimitedDownload );
								
								if ($tokenpath) {
									$path = $ischoolbag_website . '/' . $tokenpath;
									$ReturnContent ['Asgn'] [$i] ['SubmittedFiles'] ['File'] [0] ['Filename'] = 'answer.txt';
									$ReturnContent ['Asgn'] [$i] ['SubmittedFiles'] ['File'] [0] ['Filepath'] = intranet_htmlspecialchars ( $path );
								} else {
									$ReturnContent ['Asgn'] [$i] ['SubmittedFiles'] = '';
								}
							} else {
								$ReturnContent ['Asgn'] [$i] ['SubmittedFiles'] = '';
								$ReturnContent ['Asgn'] [$i] ['Correction'] = '';
							}
						}
					} elseif ($SelectType == 4) { // select assignment template
						$assignment_id = $row [$i] ['assignment_id'];
						$subject_id = $row [$i] ['SubjectID'];
						$Template = $row [$i] ['template'];
						
						$ReturnContent ['Asgn'] [$i] ['AsgnID'] = $assignment_id;
						$ReturnContent ['Asgn'] [$i] ['SubjectID'] = $subject_id;
						
						if ($Template != "") {
							if ((strpos ( $Template, "?\\" ) > 0) || (strpos ( $Template, "?\\" ) > 0))
								// $files = split("\:",$Template);
								$files = explode ( "\:", $Template );
							else
								// $files = split("\:",big5_stripslashes($Template));
								$files = explode ( "\:", big5_stripslashes ( $Template ) );
							
							for($j = 0; $j < count ( $files ); $j ++) {
								$filename = basename ( trim ( $files [$j] ) );
								$filepath = $this->Get_File_FullPath ( trim ( $files [$j] ), $CourseID, 3 );
								$ReturnContent ['Asgn'] [$i] ['Templates'] ['File'] [$j] ['Filename'] = intranet_htmlspecialchars ( $filename );
								$ReturnContent ['Asgn'] [$i] ['Templates'] ['File'] [$j] ['Filepath'] = intranet_htmlspecialchars ( $filepath );
							}
						} else {
							$ReturnContent ['Asgn'] [$i] ['Templates'] = "";
						}
					} elseif ($SelectType == 5) { // select assignment attached file in grading
						$assignment_id = $row [$i] ['assignment_id'];
						$subject_id = $row [$i] ['SubjectID'];
						$filename_mark = $row [$i] ['filename_mark'];
						
						$ReturnContent ['Asgn'] [$i] ['AsgnID'] = $assignment_id;
						$ReturnContent ['Asgn'] [$i] ['SubjectID'] = $subject_id;
						
						if ($filename_mark != "") {
							$filename = basename ( trim ( $filename_mark ) );
							$filepath = $this->Get_File_FullPath ( trim ( $filename_mark ), $CourseID, 4 );
							$ReturnContent ['Asgn'] [$i] ['File'] ['Filename'] = intranet_htmlspecialchars ( $filename );
							$ReturnContent ['Asgn'] [$i] ['File'] ['Filepath'] = intranet_htmlspecialchars ( $filepath );
						} else {
							// $ReturnContent['Asgn'][$i]['File'] = "";
						}
					} elseif ($SelectType == 6) { // select assignment basic details
						$assignment_id = $row [$i] ['assignment_id'];
						$title = $row [$i] ['title'];
						$inputdate = $row [$i] ['inputdate'];
						$StartDate = $row [$i] ['StartDate'];
						$deadline = $row [$i] ['deadline'];
						$subject_id = $row [$i] ['SubjectID'];
						$subjectName = $row [$i] ['SubjectName'];
						$instruction = $row [$i] ['instruction'];
						$loading = $row [$i] ['loading'];
						
						$comment = $row [$i] ['comment'];
						$handin_id = $row [$i] ['handin_id'];
						if ($handin_id != '' and $comment != '%SAVE%')
							$status = 'S';
						else
							$status = 'N';
						
						$ReturnContent ['Asgn'] [$i] ['AsgnID'] = $assignment_id;
						// # 2011-08-04: title field is already htmlspecialchars() before saving into DB <--- not a good practice tough!
						// #$ReturnContent['Asgn'][$i]['Title'] = intranet_htmlspecialchars($title);
						$ReturnContent ['Asgn'] [$i] ['Title'] = $title;
						$ReturnContent ['Asgn'] [$i] ['InputDate'] = $inputdate;
						$ReturnContent ['Asgn'] [$i] ['Status'] = $status;
						$ReturnContent ['Asgn'] [$i] ['SubjectID'] = $subject_id;
						$ReturnContent ['Asgn'] [$i] ['SubjectName'] = $subjectName;
						$ReturnContent ['Asgn'] [$i] ['StartDate'] = $StartDate;
						$ReturnContent ['Asgn'] [$i] ['Deadline'] = $deadline;
						$ReturnContent ['Asgn'] [$i] ['Instruction'] = $instruction;
						$ReturnContent ['Asgn'] [$i] ['Loading'] = $loading;
						
						// $max = 17;
						// if ($loading == 0){
						// $loading_desc = '<0.5';
						// }
						// elseif ($loading < $max){
						// $loading_desc = $loading * 0.5;
						// }
						// else
						// {
						// $loading_desc = '>'. ($max-1)*0.5;
						// }
						// $ReturnContent['Asgn'][$i]['Loading'] = intranet_htmlspecialchars($loading_desc);
					}
				}
			}
		} else {
			$ReturnContent = 104;
		}
		// debug_r($ReturnContent);
		return $ReturnContent;
	}
	
	/*
	 * # NOT USED ANYMORE -- This is an old version of GetUserAsgnList
	 * function WS_GetUserAsgnList($CourseID, $UserEmail, $HwType, $SubjectID, $Timestamp){
	 * global $eclass_db, $eclass_filepath;
	 * include_once("$eclass_filepath/src/includes/php/lib-groups.php");
	 *
	 * $sql = "SELECT user_id FROM $eclass_db.user_course WHERE course_id = $CourseID AND user_email = '$UserEmail'";
	 * $row = $this->returnVector($sql);
	 *
	 * if(count($row) > 0){
	 * $ThisUserID = $row[0];
	 *
	 * $lo = new libgroups(classNamingDB($CourseID));
	 * $exceptionList = $lo->returnFunctionIDs("A",$ThisUserID);
	 *
	 * $hw = new homework($CourseID, "");
	 * //$content_show = $hw->getHWList($subject_id, $hw_type, $exceptionList);
	 *
	 * //$mark_save = "%SAVE%";
	 *
	 * if(trim($Timestamp) == ""){
	 * $chkTimestamp = "1 = 1";
	 * }
	 * else{
	 * $chkTimestamp = "UNIX_TIMESTAMP(a.modified) > $Timestamp";
	 * }
	 *
	 * if(trim($HwType) == ""){
	 * // get all type
	 * $chkWorktype = "1 = 1";
	 * }
	 * else{
	 * $chkWorktype = "a.worktype='$HwType'";
	 * }
	 *
	 * if(trim($SubjectID) == ""){
	 * // get all subject
	 * $chkSubjectID = "1 = 1";
	 * }
	 * else{
	 * $chkSubjectID = "a.SubjectID = '$SubjectID'";
	 * }
	 *
	 * $student_restriction = "AND UNIX_TIMESTAMP(now())>=UNIX_TIMESTAMP(a.StartDate)";
	 * $SemestBoundary = $hw->getSemesterBoundary();
	 * if ($SemestBoundary!="")
	 * {
	 * $student_restriction .= " AND UNIX_TIMESTAMP('{$SemestBoundary}')<UNIX_TIMESTAMP(a.deadline) ";
	 * }
	 * $assignment_exception = ($exceptionList!="") ? "AND a.assignment_id NOT IN ($exceptionList)" : "";
	 *
	 * $sql = "SELECT a.assignment_id, a.title, b.inputdate, b.comment, b.status, a.worktype, a.StartDate, a.deadline, a.SubjectID, a.instruction, a.loading, a.sheettype, a.modified ";
	 * //$sql = "SELECT a.assignment_id, CONCAT(a.title, if(b.inputdate<>'', if(b.comment='$mark_save',' <span class=guide>($assignment_save)</span>', ' <span class=guide>($assignment_done)</span>'), if(b.status='LR', ' <font color=red>[$button_redo]</font>', ''))), a.worktype, ".format_datetime_weekday_sql('a.StartDate').", ".format_datetime_weekday_sql('a.deadline').", if(((UNIX_TIMESTAMP(now())>UNIX_TIMESTAMP(a.deadline) AND a.worktype<>14) OR (UNIX_TIMESTAMP(now())>UNIX_TIMESTAMP(a.StartDate)+86400 AND a.worktype=14)), 1, 0) ";
	 * $sql .= "FROM ".$hw->ec_db.".assignment as a LEFT JOIN ".$hw->ec_db.".handin AS b ON (b.assignment_id=a.assignment_id AND b.user_id='$ThisUserID') ";
	 * $sql .= "WHERE $chkTimestamp AND $chkSubjectID AND $chkWorktype AND a.status=1 AND a.StartDate IS NOT NULL $student_restriction $assignment_exception ";
	 * $sql .= "ORDER BY a.StartDate desc, a.deadline desc ";
	 * $row = $this->returnArray($sql, 6);
	 * //echo $sql;
	 * $ReturnContent = array();
	 * if(count($row) > 0){
	 * for($i=0; $i<sizeof($row); $i++)
	 * {
	 * $assignment_id = $row[$i][0];
	 * $title = $row[$i][1];
	 * $inputdate = $row[$i][2];
	 * $comment = $row[$i][3];
	 * $status = $row[$i][4];
	 * $worktype = $row[$i][5];
	 * $StartDate = $row[$i][6];
	 * $deadline = $row[$i][7];
	 * $subject_id = $row[$i][8];
	 * $instruction = $row[$i][9];
	 * $loading = $row[$i][10];
	 * $sheettype = $row[$i][11];
	 * $modified = $row[$i][12];
	 *
	 * $ReturnContent['Asgn'][$i]['AsgnID'] = $assignment_id;
	 * $ReturnContent['Asgn'][$i]['Title'] = intranet_htmlspecialchars($title);
	 * $ReturnContent['Asgn'][$i]['InputDate'] = $inputdate;
	 * $ReturnContent['Asgn'][$i]['Comment'] = $comment;
	 * $ReturnContent['Asgn'][$i]['Status'] = $status;
	 * $ReturnContent['Asgn'][$i]['SubjectID'] = $subject_id;
	 * $ReturnContent['Asgn'][$i]['WorkType'] = $worktype;
	 * $ReturnContent['Asgn'][$i]['StartDate'] = $StartDate;
	 * $ReturnContent['Asgn'][$i]['Deadline'] = $deadline;
	 * $ReturnContent['Asgn'][$i]['Instruction'] = $instruction;
	 * $ReturnContent['Asgn'][$i]['Loading'] = $loading;
	 * $ReturnContent['Asgn'][$i]['Sheettype'] = $sheettype;
	 * $ReturnContent['Asgn'][$i]['DateModified'] = $modified;
	 * }
	 * }
	 * }
	 * else{
	 * $ReturnContent = '104';
	 * }
	 * //debug_r($ReturnContent);
	 * return $ReturnContent;
	 * }
	 */
	
	// Get all related course by User
	function WS_GetUserCourse($UserEmail) {
		global $eclass_db;
		
		if (true || $ThisUserID != '') {
			if ($UserEmail != '') {
				// refer to libportal.php - returnEClassUserIDCourseIDStandard()
				$fieldname = "a.course_id, a.course_code, a.course_name, ";
				$fieldname .= "b.user_id, b.memberType, b.lastused, b.user_course_id, b.status, a.modified";
				$sql = "SELECT $fieldname 
						FROM {$eclass_db}.course AS a, {$eclass_db}.user_course AS b 
						WHERE 
							a.course_id = b.course_id AND b.user_email = '$UserEmail' AND a.RoomType=0 AND 
							(b.status is NULL OR b.status NOT IN ('deleted')) 
						GROUP BY a.course_id 
						ORDER BY a.course_code, a.course_name";
				$row = $this->returnArray ( $sql );
				// echo $sql;
				$ReturnContent = array ();
				if (count ( $row ) > 0) {
					for($i = 0; $i < sizeof ( $row ); $i ++) {
						$course_id = $row [$i] [0];
						// $course_code = intranet_wordwrap($row[$i][1],30,"\n",1);
						// $course_name = intranet_wordwrap($row[$i][2],30,"\n",1);
						$course_code = $row [$i] [1];
						$course_name = $row [$i] [2];
						$user_id = $row [$i] [3];
						$memberType = $row [$i] [4];
						$lastused = $row [$i] [5];
						$user_course_id = $row [$i] [6];
						$status = $row [$i] [7];
						$modified = $row [$i] [8];
						
						$ReturnContent ['Course'] [$i] ['CourseID'] = $course_id;
						$ReturnContent ['Course'] [$i] ['CourseCode'] = $course_code;
						$ReturnContent ['Course'] [$i] ['CourseName'] = intranet_htmlspecialchars ( $course_name );
						$ReturnContent ['Course'] [$i] ['MemberType'] = $memberType;
						$ReturnContent ['Course'] [$i] ['UserCourseID'] = $user_course_id;
						$ReturnContent ['Course'] [$i] ['ModifiedDate'] = $modified;
					}
				}
			}
		} else {
			$ReturnContent = '401';
		}
		return $ReturnContent;
	}
	
	// eContent - Get all course notes (published)
	// Notes Section Type : 8 - FileUpload
	// Type : 1 - Text
	// Type : 2 - URL (2010-05-25)
	// Type : 6 - powervoice (2010-05-25)
	// Type : 3 - youtube (2010-08-03)
	function WS_GetUserNoteList($CourseID, $Timestamp, $SelectType) {
		$this->db = classNamingDB ( $CourseID );
		
		// echo date("Y-m-d h:j:s", $Timestamp). "<br>". date_default_timezone_get();
		// echo date("Y-m-d h:j:s", time()). "<br>";
		
		$null_time = '00-00-00 00:00:00';
		if (true || $ThisUserID != '') {
			// SUBSTRING_INDEX(ns.content, '/', -1) as filename
			$sql = "select 
						n.notes_id, ns.notes_section_id, n.title, ns.content, ns.url, 
						n.modified as n_modified, ns.modified as ns_modified, n.SubjectID, ns.notes_section_type, n.a_no, n.b_no, n.c_no, n.d_no, n.e_no
					from notes as n left join notes_section as ns on
						ns.notes_id = n.notes_id and ( (ns.notes_section_type IN (1, 6, 8, 3) and content is not NULL and content <> '')
														or ns.notes_section_type = 2 
													 ) 
					where 
						n.status = '1' ";
			
			if (trim ( $Timestamp ) != "") {
				$sql .= " and unix_timestamp(n.modified) > $Timestamp";
			}
			/*
			 * $sql .= " and ((n.starttime IS NULL OR n.starttime = ".$null_time." OR n.starttime < now() ) and
			 * (n.endtime IS NULL OR n.endtime > now() OR n.endtime = ".$null_time." ) ) "; // Within Period
			 */
			$sql .= " order by n.notes_id, n.a_no, n.b_no, n.c_no, n.d_no, n.e_no";
			// echo $sql;
			$row = $this->returnArray ( $sql );
			// Group Data in related Array format
			$ReturnContent = array ();
			
			if (count ( $row ) > 0) {
				$cur_notes_id = - 1;
				$ncount = - 1;
				$nscount = 0;
				
				for($i = 0; $i < count ( $row ); $i ++) {
					$notes_id = $row [$i] ['notes_id'];
					$title = $row [$i] ['title'];
					$notes_section_id = $row [$i] ['notes_section_id'];
					$notes_section_type = $row [$i] ['notes_section_type'];
					$content = $row [$i] ['content'];
					$url = $row [$i] ['url'];
					$n_modified = $row [$i] ['n_modified'];
					$ns_modified = $row [$i] ['ns_modified'];
					// $filename = get_file_basename($content);
					$subject_id = $row [$i] ['SubjectID'];
					$a_no = $row [$i] ['a_no'];
					$b_no = $row [$i] ['b_no'];
					$c_no = $row [$i] ['c_no'];
					$d_no = $row [$i] ['d_no'];
					$e_no = $row [$i] ['e_no'];
					
					if ($notes_id != $cur_notes_id) {
						$cur_notes_id = $notes_id;
						$ncount ++;
						$nscount = 0;
						
						$ReturnContent ['Notes'] [$ncount] ['NotesID'] = $notes_id;
						if ($SelectType != "1") {
							$ReturnContent ['Notes'] [$ncount] ['Title'] = intranet_htmlspecialchars ( $title );
							$ReturnContent ['Notes'] [$ncount] ['ModifiedDate'] = $n_modified;
							$ReturnContent ['Notes'] [$ncount] ['SubjectID'] = $subject_id;
							$ReturnContent ['Notes'] [$ncount] ['ANo'] = $a_no;
							$ReturnContent ['Notes'] [$ncount] ['BNo'] = $b_no;
							$ReturnContent ['Notes'] [$ncount] ['CNo'] = $c_no;
							$ReturnContent ['Notes'] [$ncount] ['DNo'] = $d_no;
							$ReturnContent ['Notes'] [$ncount] ['ENo'] = $e_no;
						}
					}
					
					if ($notes_section_id != '') {
						$ReturnContent ['Notes'] [$ncount] ['NotesSection'] [$nscount] ['NotesSectionID'] = $notes_section_id;
						if ($SelectType != "1") {
							$ReturnContent ['Notes'] [$ncount] ['NotesSection'] [$nscount] ['NotesSectionType'] = $notes_section_type;
							$ReturnContent ['Notes'] [$ncount] ['NotesSection'] [$nscount] ['ModifiedDate'] = $ns_modified;
							
							if ($notes_section_type == 2) { // url
								$ReturnContent ['Notes'] [$ncount] ['NotesSection'] [$nscount] ['FilePath'] = intranet_htmlspecialchars ( $url );
								$ReturnContent ['Notes'] [$ncount] ['NotesSection'] [$nscount] ['Filename'] = '';
							} elseif ($notes_section_type == 3) { // youtube
							                                  // extract the src from the youtube embed content
								preg_match ( '/\<\s*?embed\s+[^>]*?\s*src\s*=\s*"([^"]*)"([^>]*)?\>/i', $content, $matches );
								if ($matches [1] == '') {
									preg_match ( '/\<\s*?iframe\s+[^>]*?\s*src\s*=\s*"([^"]*)"([^>]*)?\>/i', $content, $matches );
								}
								
								$ReturnContent ['Notes'] [$ncount] ['NotesSection'] [$nscount] ['FilePath'] = intranet_htmlspecialchars ( $matches [1] );
								$ReturnContent ['Notes'] [$ncount] ['NotesSection'] [$nscount] ['Filename'] = '';
							} else {
								$ReturnContent ['Notes'] [$ncount] ['NotesSection'] [$nscount] ['FilePath'] = intranet_htmlspecialchars ( $this->Generate_NoteSection_FilePath ( $CourseID, $notes_id, $notes_section_id, $notes_section_type, $content ) );
								$ReturnContent ['Notes'] [$ncount] ['NotesSection'] [$nscount] ['Filename'] = intranet_htmlspecialchars ( $this->Generate_NoteSection_FileName ( $title, $notes_section_type, $content ) );
							}
							$ReturnContent ['Notes'] [$ncount] ['NotesSection'] [$nscount] ['fromCC'] = '1';
						}
						$nscount ++;
					}
				}
			}
		} else {
			$ReturnContent = '401';
		}
		return $ReturnContent;
	}
	function Generate_NoteSection_FilePath($CourseID, $notes_id, $notes_section_id, $notes_section_type, $content) {
		global $ischoolbag_website, $ischoolbag_eclass_url_path;
		
		if ($notes_section_type == 1) { // TEXT
			return $ischoolbag_website . $ischoolbag_eclass_url_path . "/src/econtent/view_note.php?CourseID=$CourseID&NoteID=$notes_id&NoteSectionID=$notes_section_id";
		} else if ($notes_section_type == 6) { // PowerVoice
			return intranet_htmlspecialchars ( $this->Get_File_FullPath ( $content, $CourseID, 0 ) );
		} else if ($notes_section_type == 8) { // UPLOAD FILE
			return intranet_htmlspecialchars ( $this->Get_File_FullPath ( $content, $CourseID, 0 ) );
		}
		return "";
	}
	function Generate_NoteSection_FileName($title, $notes_section_type, $content) {
		global $SessionID;
		if ($notes_section_type == 1) { // TEXT
		                              // Begin Henry Yuen (2010-05-10): truncate the title length to at most 248 characters
		                              // suffice ( timestamp.html) length = 16, so title length = 232
			$title = substr ( $title, 0, 231 );
			return $title . "." . time () . ".html";
			// return $title. ".". trim($SessionID). ".". time(). ".html";
			// End Henry Yuen (2010-05-10): truncate the title length to at most 248 characters
		} else if ($notes_section_type == 6) { // PowerVoice
			return get_file_basename ( $content );
		} else if ($notes_section_type == 8) { // UPLOAD FILE
			return get_file_basename ( $content );
		}
		return "";
	}
	function Get_EclassFile_Details($fpath, $CourseID, $CategoryID) {
		global $ischoolbag_file_path, $ischoolbag_website, $eclass_filepath;
		$this->db = classNamingDB ( $CourseID );
		
		include_once ("$eclass_filepath/src/includes/php/lib-filemanager.php");
		$fm = new fileManager ( $CourseID, $CategoryID, "" );
		
		$Title = get_file_basename ( $fpath );
		$sLen = strlen ( $fpath ) - strlen ( $Title );
		
		$VirPath = addslashes ( trim ( substr ( $fpath, 0, $sLen ) ) );
		$Title = addslashes ( trim ( $Title ) );
		
		if ($VirPath != "" && $VirPath != "/") {
			$VirPath = "VirPath='" . $VirPath . "'";
		} else {
			$VirPath = "VirPath is NULL";
		}
		
		// 2010-12-13: specify Category in the where clause
		// $sql = "SELECT CONCAT(ifnull(Location,''), '/', Title) FROM eclass_file WHERE
		// $VirPath AND Title='$Title' AND IsDir<>1 "; //AND Category=".$category_id."
		$sql = "SELECT FileID, CONCAT(ifnull(Location,''), '/', Title) AS Location FROM eclass_file WHERE 
				$VirPath AND Title='$Title' AND IsDir<>1 AND Category=" . $CategoryID;
		
		$row = $this->returnArray ( $sql, 2 );
		return $row;
	}
	
	// Henry Yuen (2010-07-28): added parameter $isLocal
	// if isLocal = 0, return public path for download, else return the folder path in local machine
	function Get_File_FullPath($fpath, $CourseID, $CategoryID, $isLocal = 0) {
		global $ischoolbag_file_path, $ischoolbag_website, $eclass_filepath;
		include_once ("$eclass_filepath/src/includes/php/lib-filemanager.php");
		$fm = new fileManager ( $CourseID, $CategoryID, "" );
		
		$row = $this->Get_EclassFile_Details ( $fpath, $CourseID, $CategoryID );
		if (sizeof ( $row ) > 0) {
			$location = $row [0] ['Location'];
			
			if ($isLocal == 1) {
				$link = $eclass_filepath . "/files/" . $fm->getCategoryFile ( $CategoryID, $this->db ) . "/" . $location;
			} else {
				// $tmp = split("/", $location);
				$tmp = explode ( "/", $location );
				for($j = 0; $j < sizeof ( $tmp ); $j ++) {
					$tmp [$j] = rawurlencode ( $tmp [$j] );
				}
				$location = implode ( "/", $tmp );
				$link = $ischoolbag_website . $ischoolbag_file_path . "/" . $fm->getCategoryFile ( $CategoryID, $this->db ) . "/" . $location;
			}
		}
		
		return $link;
	}
	
	// eContent - Get Note Section info
	// This method is deprecated
	function WS_GetNoteSection($NotesSectionID, $CourseID) {
		return 8; // This method is deprecated
		
		$CategoryID = 0; // category for note is 0
		
		$this->db = classNamingDB ( $CourseID );
		if (true || $ThisUserID) {
			if ($NotesSectionID != '') {
				$sql = "select content, modified from notes_section where notes_section_id = '$NotesSectionID' ";
				$row = $this->returnArray ( $sql );
				
				// Group Data in related Array format
				$ReturnContent = array ();
				if (count ( $row ) > 0) {
					$file = $row [0] ['content'];
					$modified = $row [0] ['modified'];
					$filepath = $this->Get_File_FullPath ( $file, $CourseID, $CategoryID );
					$filepath = intranet_htmlspecialchars ( $filepath );
					$ReturnContent = array (
							'FilePath' => $filepath,
							'fromCC' => 1,
							'ModifiedDate' => $modified 
					);
				}
			} else {
				$ReturnContent = '501';
			}
		} else {
			$ReturnContent = '401';
		}
		return $ReturnContent;
	}
	
	/*
	 * function WS_eClassAppFileUpload() rewritten by Thomas on 2012-11-13
	 * function WS_eClassAppFileUpload($CourseID, $UploadDir, $FileName, $FileByteStream){
	 * global $eclass_filepath, $FF_path, $plugin;
	 *
	 * $ReturnContent = array();
	 *
	 * if(strpos($FileName, ".")==0){ // Fail
	 * return 105;
	 * }
	 *
	 * if(!is_null($UploadDir)){
	 * $imgdir = $eclass_filepath."/files/".classNamingDB($CourseID)."/fileupload/".$UploadDir;
	 * }else{
	 * $imgdir = $eclass_filepath."/tool/flashupload/files/";
	 * }
	 *
	 * if(!is_null($FileByteStream)){
	 * include_once('libfilesystem.php');
	 * $fs = new libfilesystem();
	 * $fs->createFolder($imgdir);
	 *
	 * # tmp file dest path before conversion if need
	 * $FileName = stripslashes($FileName);
	 * $sourceDes = $imgdir."/".$FileName;
	 *
	 * // extract filebyte from cdata
	 * $decoded = base64_decode($FileByteStream);
	 *
	 * $fp = fopen($sourceDes, 'wb');
	 * fwrite($fp, $decoded);
	 * fclose($fp);
	 *
	 * $file_name = strtolower(substr($FileName, 0, strrpos($FileName, ".")));
	 * $ext = strtolower(substr($FileName, strrpos($FileName, ".")));
	 *
	 * $ReturnContent['source'] = $FileName;
	 *
	 * # voice, media file conversion for iPad and Android
	 * if( in_array($ext, array(".aac", ".3gpp", ".mov")) ){
	 * $cmd = (isset($FF_path) && $FF_path != '') ? $FF_path."/ffmpeg" : "/usr/local/bin/ffmpeg";
	 * $extra_cmd = '';
	 * $allowConvert = true;
	 *
	 * if($ext == ".aac") # sound file recorded by iOS
	 * {
	 * $targetFileName = $file_name.".mp3";
	 * $targetDes = $imgdir."/".$targetFileName;
	 * $cmd .= " -y -i '".$sourceDes."' ".$extra_cmd." -sameq -acodec libmp3lame -ar 22050 '".$targetDes."' ";
	 * }
	 * else if($ext == ".3ogg") # sound file recorded by iOS
	 * {
	 * $targetFileName = $file_name.".mp3";
	 * $targetDes = $imgdir."/".$targetFileName;
	 * $cmd .= " -y -i '".$sourceDes."' ".$extra_cmd." -sameq -acodec libmp3lame -ar 22050 '".$targetDes."' ";
	 * }
	 * else if($ext == ".mov") # video file recorded by iOS
	 * {
	 * $targetFileName = $file_name.".mp4";
	 * $targetDes = $imgdir."/".$targetFileName;
	 * # performance concerns: orginally, audio bit - 44100, frame size - 1280x720
	 * $cmd .= " -y -i '".$sourceDes."' -sameq -acodec libfaac -ar 22050 -vcodec mpeg4 -s 720x540 -aspect 4:3 '".$targetDes."' ";
	 *
	 * if( isset($plugin['power_lesson_media_conversion_type']) &&
	 * !in_array("mp4", $plugin['power_lesson_media_conversion_type'])
	 * ){
	 * $allowConvert = false;
	 * }
	 * }
	 *
	 * //echo $cmd.'<br><br>';
	 *
	 * $ReturnContent['fileName'] = $targetFileName;
	 * //$ReturnContent['cmd'] = $cmd;
	 *
	 * if($allowConvert == true){
	 * # convert MPV to MP4
	 * $ret = exec($cmd, $dum_out_1, $dum_out_2);
	 *
	 * if($ret==0){
	 * chmod($targetDes, 0777);
	 * $ReturnContent['isSuccess'] = "1";
	 *
	 * if(strstr($UploadDir, "PowerLesson/L_") && $ext == ".mov"){
	 * # convert file from MP4 to FLV is the plugin flag is set and match with default support media type
	 * if( !isset($plugin['power_lesson_media_conversion_type']) ||
	 * (isset($plugin['power_lesson_media_conversion_type']) && in_array("flv", $plugin['power_lesson_media_conversion_type']))
	 * ){
	 * $this->convertFileToFLV($targetDes, $imgdir, "MP4", $file_name.".flv")? "1":"0";
	 * }
	 * }
	 * } else {
	 * $ReturnContent['isSuccess'] = "0";
	 * }
	 * } else {
	 * # ignore all conversion from MOV to MP4 and MP4 to FLV
	 * $ReturnContent['isSuccess'] = "1";
	 * }
	 * } else {
	 * if(isImage($sourceDes)){
	 * list($img_width, $img_height, $img_type, $img_attr) = getimagesize($sourceDes);
	 * $ReturnContent['fileVar']['width'] = $img_width;
	 * $ReturnContent['fileVar']['height'] = $img_height;
	 * $ReturnContent['fileVar']['type'] = $img_type;
	 * $ReturnContent['fileVar']['attr'] = $img_attr;
	 * }
	 *
	 * $ReturnContent['fileName'] = $FileName;
	 * $ReturnContent['isSuccess'] = "1";
	 * }
	 *
	 * return $ReturnContent;
	 * }
	 * }
	 */
	function WS_eClassAppFileUpload($CourseID, $UploadDir, $FileName, $FileByteStream) {
		global $eclass_filepath, $FF_path, $plugin;
		
		$ReturnContent = array ();
		
		if (strpos ( $FileName, "." ) == 0) { // Fail
			return 105;
		}
		
		if (! is_null ( $UploadDir )) {
			$imgdir = $eclass_filepath . "/files/" . classNamingDB ( $CourseID ) . "/fileupload/" . $UploadDir;
		} else {
			$imgdir = $eclass_filepath . "/tool/flashupload/files/";
		}
		
		if (! is_null ( $FileByteStream )) {
			include_once ('libfilesystem.php');
			$fs = new libfilesystem ();
			$fs->createFolder ( $imgdir );
			
			// tmp file dest path before conversion if need
			$FileName = stripslashes ( $FileName );
			$sourceDes = $imgdir . "/" . $FileName;
			
			// extract filebyte from cdata
			$decoded = base64_decode ( $FileByteStream );
			
			$fp = fopen ( $sourceDes, 'wb' );
			fwrite ( $fp, $decoded );
			fclose ( $fp );
			
			$file_name = strtolower ( substr ( $FileName, 0, strrpos ( $FileName, "." ) ) );
			$ext = strtolower ( substr ( $FileName, strrpos ( $FileName, "." ) ) );
			
			$ReturnContent ['source'] = $FileName;
			
			// voice, media file conversion for iPad and Android
			if ($ext == ".3ogg" || $ext == ".aac" || $ext == ".m4a") {
				$cmd = (isset ( $FF_path ) && $FF_path != '') ? OsCommandSafe($FF_path) . "/ffmpeg" : "/usr/local/bin/ffmpeg";
				$extra_cmd = '';
				
				$targetFileName = $file_name . ".mp3";
				$targetDes = $imgdir . "/" . $targetFileName;
				// $cmd .= " -y -i '".$sourceDes."' ".$extra_cmd." -sameq -acodec libmp3lame -ar 22050 '".$targetDes."' ";
				$cmd .= " -y -i '" . OsCommandSafe($sourceDes) . "' " . OsCommandSafe($extra_cmd) . " -acodec libmp3lame -ar 22050 '" . OsCommandSafe($targetDes) . "' ";
				$ReturnContent ['fileName'] = $targetFileName;
				// $ReturnContent['cmd'] = $cmd;
				
				$ret = exec ( $cmd, $dum_out_1, $dum_out_2 );
				if ($ret == 0) {
					unlink($sourceDes);
					chmod ( $targetDes, 0777 );
					$ReturnContent ['isSuccess'] = "1";
				} else {
					$ReturnContent ['isSuccess'] = "0";
				}
			} else {
				if (isImage ( $sourceDes )) {
					list ( $img_width, $img_height, $img_type, $img_attr ) = getimagesize ( $sourceDes );
					$ReturnContent ['fileVar'] ['width'] = $img_width;
					$ReturnContent ['fileVar'] ['height'] = $img_height;
					$ReturnContent ['fileVar'] ['type'] = $img_type;
					$ReturnContent ['fileVar'] ['attr'] = $img_attr;
				}
				
				$ReturnContent ['fileVar'] ['file_size'] = filesize ( $sourceDes );
				$ReturnContent ['fileName'] = $FileName;
				$ReturnContent ['isSuccess'] = "1";
			}
			
			return $ReturnContent;
		}
	}
	function WS_eClassAppFileUpload_cceap($UploadDir, $FileName, $FileByteStream) {
		global $eclass_filepath, $FF_path, $plugin;
		
		$ReturnContent = array ();
		
		if (strpos ( $FileName, "." ) == 0) { // Fail
			return 105;
		}
		
		if (! is_null ( $UploadDir )) {
			$imgdir = $eclass_filepath . "/files/" . $UploadDir;
		} else {
			$imgdir = $eclass_filepath . "/tool/flashupload/files/";
		}
		
		if (! is_null ( $FileByteStream )) {
			include_once ('libfilesystem.php');
			$fs = new libfilesystem ();
			$fs->createFolder ( $imgdir );
			
			// tmp file dest path before conversion if need
			$FileName = stripslashes ( $FileName );
			$sourceDes = $imgdir . "/" . $FileName;
			
			// extract filebyte from cdata
			$decoded = base64_decode ( $FileByteStream );
			
			$fp = fopen ( $sourceDes, 'wb' );
			fwrite ( $fp, $decoded );
			fclose ( $fp );
			
			$file_name = substr ( $FileName, 0, strrpos ( $FileName, "." ) );
			$ext = strtolower ( substr ( $FileName, strrpos ( $FileName, "." ) ) );
			
			$ReturnContent ['source'] = $FileName;
			
			// voice, media file conversion for iPad and Android
			if ($ext == ".aac" || $ext == ".m4a") {
				$cmd = (isset ( $FF_path ) && $FF_path != '') ? OsCommandSafe($FF_path) . "/ffmpeg" : "/usr/local/bin/ffmpeg";
				$extra_cmd = '';
				
				$targetFileName = $file_name . ".mp3";
				$targetDes = $imgdir . "/" . $targetFileName;
				// $cmd .= " -y -i '".$sourceDes."' ".$extra_cmd." -sameq -acodec libmp3lame -ar 22050 '".$targetDes."' ";
				$cmd .= " -y -i '" . OsCommandSafe($sourceDes) . "' " . OsCommandSafe($extra_cmd) . " -acodec libmp3lame -ar 22050 '" . OsCommandSafe($targetDes) . "' ";
				$ReturnContent ['fileName'] = $targetFileName;
				// $ReturnContent['cmd'] = $cmd;
				
				$ret = exec ( $cmd, $dum_out_1, $dum_out_2 );
				if ($ret == 0) {
					chmod ( $targetDes, 0777 );
					$ReturnContent ['isSuccess'] = "1";
				} else {
					$ReturnContent ['isSuccess'] = "0";
				}
			} else {
				if (isImage ( $sourceDes )) {
					list ( $img_width, $img_height, $img_type, $img_attr ) = getimagesize ( $sourceDes );
					$ReturnContent ['fileVar'] ['width'] = $img_width;
					$ReturnContent ['fileVar'] ['height'] = $img_height;
					$ReturnContent ['fileVar'] ['type'] = $img_type;
					$ReturnContent ['fileVar'] ['attr'] = $img_attr;
				}
				
				$ReturnContent ['fileVar'] ['file_size'] = filesize ( $sourceDes );
				$ReturnContent ['fileName'] = $FileName;
				$ReturnContent ['isSuccess'] = "1";
			}
			
			return $ReturnContent;
		}
	}
	function WS_eClassAppFileUploadByChunk($CourseID, $UploadDir, $TmpFilePrefix, $FileName = '', $ChunkNo = 0, $TotalNoOfChunk = 0, $IsReady = false, $FileByteStream = '', $ChunkCheck = '', $AllChunkCheck = '', $FileCheck = '', $IsRetry = false) {
		global $eclass_filepath, $FF_path, $plugin;
		
		$ReturnContent = array ();
		
		if (! is_null ( $UploadDir )) {
			$imgdir = $eclass_filepath . "/files/" . classNamingDB ( $CourseID ) . "/fileupload/" . $UploadDir;
		} else {
			$imgdir = $eclass_filepath . "/tool/flashupload/files/";
		}
		
		if ($IsReady && $IsRetry && $FileName) {
			$FinalFileName = stripslashes ( $FileName );
			$FinalFileDest = $imgdir . "/" . $FinalFileName;
			
			if (is_file ( $FinalFileDest )) {
				@exec ( 'rm -f ' . OsCommandSafe($imgdir) . '/' . OsCommandSafe($TmpFilePrefix) . '_*' );
				
				if (isImage ( $FinalFileDest )) {
					list ( $img_width, $img_height, $img_type, $img_attr ) = getimagesize ( $FinalFileDest );
					$ReturnContent ['fileVar'] ['width'] = $img_width;
					$ReturnContent ['fileVar'] ['height'] = $img_height;
					$ReturnContent ['fileVar'] ['type'] = $img_type;
					$ReturnContent ['fileVar'] ['attr'] = $img_attr;
				}
				
				$ReturnContent ['fileVar'] ['path'] = 'Upload??' . $UploadDir . '/' . $FinalFileName;
				$ReturnContent ['fileVar'] ['encodedPath'] = 'Upload??' . $UploadDir . '/' . encode_filenames ( $FinalFileName );
				$ReturnContent ['fileVar'] ['encodedPathForFlash'] = 'Upload??' . $UploadDir . '/' . encode_url_for_flashvars ( $FinalFileName );
				$ReturnContent ['fileVar'] ['ecfpath'] = encrypt_string ( 'fileupload/' . $UploadDir . '/' . $FinalFileName );
				
				$ReturnContent ['fileVar'] ['file_size'] = filesize ( $FinalFileDest );
				$ReturnContent ['fileName'] = $FinalFileName;
				$ReturnContent ['isSuccess'] = "1";
				
				return $ReturnContent;
			}
		}
		
		if (! is_null ( $FileByteStream )) {
			include_once ('libfilesystem.php');
			$fs = new libfilesystem ();
			$fs->createFolder ( $imgdir );
			
			// tmp file dest path
			$ZeroPaddedChunkNo = str_pad ( $ChunkNo, strlen ( $TotalNoOfChunk ), 0, STR_PAD_LEFT );
			$CurrentChunkFileName = stripslashes ( $TmpFilePrefix . '_file_' . $ZeroPaddedChunkNo );
			$CurrentChunkFileDest = $imgdir . "/" . $CurrentChunkFileName;
			$CurrentChunkCheckFileName = stripslashes ( $TmpFilePrefix . '_check_' . $ZeroPaddedChunkNo );
			$CurrentChunkCheckFileDest = $imgdir . "/" . $CurrentChunkCheckFileName;
			
			// extract filebyte from cdata
			$decoded = base64_decode ( $FileByteStream );
			$decodedMD5 = strtolower ( md5 ( $decoded ) );
			if ($decodedMD5 == strtolower ( $ChunkCheck )) {
				if (is_file ( $CurrentChunkFileDest )) {
					unlink ( $CurrentChunkFileDest );
				}
				
				$CurrentChunkFile = fopen ( $CurrentChunkFileDest, 'wb' );
				fwrite ( $CurrentChunkFile, $decoded );
				fclose ( $CurrentChunkFile );
				
				if (is_file ( $CurrentChunkCheckFileDest )) {
					unlink ( $CurrentChunkCheckFileDest );
				}
				
				$CurrentChunkCheckFile = fopen ( $CurrentChunkCheckFileDest, 'wb' );
				fwrite ( $CurrentChunkCheckFile, $decodedMD5 );
				fclose ( $CurrentChunkCheckFile );
				
				if ($IsReady) {
					if ($FileName && strpos ( $FileName, "." ) != 0) {
						// Check all chunk exist or not
						$IsAllChunkReady = true;
						for($i = 0; $i < $TotalNoOfChunk; $i ++) {
							$ZeroPaddedChunkNo = str_pad ( $i, strlen ( $TotalNoOfChunk ), 0, STR_PAD_LEFT );
							if (! is_file ( $imgdir . "/" . $TmpFilePrefix . '_file_' . $ZeroPaddedChunkNo ) || ! is_file ( $imgdir . "/" . $TmpFilePrefix . '_check_' . $ZeroPaddedChunkNo )) {
								$IsAllChunkReady = false;
								break;
							}
						}
						
						if ($IsAllChunkReady) {
							$FinalFileName = stripslashes ( $FileName );
							$FinalFileDest = $imgdir . "/" . $FinalFileName;
							
							/*
							 * Comment out by Thomas on 2014-12-11
							 * $FinalFile = fopen($FinalFileDest, 'ab');
							 * for($i=0;$i<$TotalNoOfChunk;$i++){
							 * $ChunkFileName = stripslashes($TmpFilePrefix.'_'.$i);
							 * $ChunkFileDest = $imgdir."/".$ChunkFileName;
							 *
							 * $ChunkFile = fopen($ChunkFileDest, 'rb');
							 * $ChunkData = fread($ChunkFile, filesize($ChunkFileDest));
							 * fclose($ChunkFile);
							 *
							 * fwrite($FinalFile, $ChunkData);
							 * }
							 * fclose($FinalFile);
							 */
							
							@exec ( 'find ' . OsCommandSafe($imgdir) . ' -name "' . OsCommandSafe($TmpFilePrefix) . '_file_*" | sort | xargs cat > ' . OsCommandSafe($FinalFileDest) );
							
							$passedFileCheck = false;
							if (! empty ( $AllChunkCheck )) {
								$FinalCheckFileName = $TmpFilePrefix . '_check';
								$FinalCheckFileDest = $imgdir . "/" . $FinalCheckFileName;
								
								@exec ( 'find ' . OsCommandSafe($imgdir) . ' -name "' . OsCommandSafe($TmpFilePrefix) . '_check_*" | sort | xargs cat > ' . OsCommandSafe($FinalCheckFileDest) );
								$passedFileCheck = strtolower ( md5_file ( $FinalCheckFileDest ) ) == strtolower ( $AllChunkCheck );
							} else if (! empty ( $FileCheck )) {
								$passedFileCheck = strtolower ( md5_file ( $FinalFileDest ) ) == strtolower ( $FileCheck );
							}
							
							if ($passedFileCheck) {
								/*
								 * Comment out by Thomas in 2014-12-11
								 * for($i=0;$i<$TotalNoOfChunk;$i++){
								 * $ZeroPaddedChunkNo = str_pad($i, strlen($TotalNoOfChunk), 0, STR_PAD_LEFT);
								 * $ChunkFileName = stripslashes($TmpFilePrefix.'_'.$ZeroPaddedChunkNo);
								 * $ChunkFileDest = $imgdir."/".$ChunkFileName;
								 *
								 * unlink($ChunkFileDest);
								 * }
								 */
								
								@exec ( 'rm -f ' . OsCommandSafe($imgdir) . '/' . OsCommandSafe($TmpFilePrefix) . '_*' );
								
								$file_name = strtolower ( substr ( $FinalFileName, 0, strrpos ( $FinalFileName, "." ) ) );
								$ext = strtolower ( substr ( $FinalFileName, strrpos ( $FinalFileName, "." ) ) );
								
								// voice, media file conversion for iPad and Android
								if ($ext == ".3ogg") {
									$cmd = (isset ( $FF_path ) && $FF_path != '') ? OsCommandSafe($FF_path) . "/ffmpeg" : "/usr/local/bin/ffmpeg";
									$extra_cmd = '';
									
									$targetFileName = $file_name . ".mp3";
									$targetDes = $imgdir . "/" . $targetFileName;
									// $cmd .= " -y -i '".$FinalFileDest."' ".$extra_cmd." -sameq -acodec libmp3lame -ar 22050 '".$targetDes."' ";
									$cmd .= " -y -i '" . OsCommandSafe($FinalFileDest) . "' " . OsCommandSafe($extra_cmd) . " -acodec libmp3lame -ar 22050 '" . OsCommandSafe($targetDes) . "' ";
									
									$ReturnContent ['fileName'] = $targetFileName;
									// $ReturnContent['cmd'] = $cmd;
									
									$ret = exec ( $cmd, $dum_out_1, $dum_out_2 );
									if ($ret == 0) {
										chmod ( $targetDes, 0777 );
										$ReturnContent ['isSuccess'] = "1";
									} else {
										$ReturnContent ['isSuccess'] = "0";
									}
								} else {
									if (isImage ( $FinalFileDest )) {
										list ( $img_width, $img_height, $img_type, $img_attr ) = getimagesize ( $FinalFileDest );
										$ReturnContent ['fileVar'] ['width'] = $img_width;
										$ReturnContent ['fileVar'] ['height'] = $img_height;
										$ReturnContent ['fileVar'] ['type'] = $img_type;
										$ReturnContent ['fileVar'] ['attr'] = $img_attr;
									}
									
									$ReturnContent ['fileVar'] ['path'] = 'Upload??' . $UploadDir . '/' . $FinalFileName;
									$ReturnContent ['fileVar'] ['encodedPath'] = 'Upload??' . $UploadDir . '/' . encode_filenames ( $FinalFileName );
									$ReturnContent ['fileVar'] ['encodedPathForFlash'] = 'Upload??' . $UploadDir . '/' . encode_url_for_flashvars ( $FinalFileName );
									$ReturnContent ['fileVar'] ['ecfpath'] = encrypt_string ( 'fileupload/' . $UploadDir . '/' . $FinalFileName );
									
									$ReturnContent ['fileVar'] ['file_size'] = filesize ( $FinalFileDest );
									$ReturnContent ['fileName'] = $FinalFileName;
									$ReturnContent ['isSuccess'] = "1";
								}
							} else {
								// Error - final file corrupted
								return 109;
							}
						} else {
							// Error - some chunk missing
							return 108;
						}
					} else {
						// Error - invalid file name
						return 105;
					}
				} else {
					// Return Upload Chunk Result
					$ReturnContent ['fileName'] = $FileName;
					$ReturnContent ['isSuccess'] = "1";
				}
				
				return $ReturnContent;
			} else {
				// Error - chunk corrupted
				return 107;
			}
		} else {
			// Error - no data received
			return 106;
		}
	}
	function WS_eClassAppGetClassroomLesson($parUserID, $PowerLessonOnly = false, $UserCourseID = 0, $DisplayLang = "eng") {
		$ReturnContent = array ();
		
		$classroomAry = $this->WS_eClassAppGetClassroom ( $parUserID, $PowerLessonOnly, true, $DisplayLang );
		$lessonAry = array ();
		
		for($i = 0; $i < count ( $classroomAry ['Classrooms'] ['Classroom'] ); $i ++) {
			if ((empty ( $UserCourseID ) && $i == 0) || $UserCourseID == $classroomAry ['Classrooms'] ['Classroom'] [$i] ['UserCourseID']) {
				$lessonAry = $this->WS_eClassAppGetLesson ( $classroomAry ['Classrooms'] ['Classroom'] [$i] ['UserCourseID'], $classroomAry ['Classrooms'] ['Classroom'] [$i] ['Language'], $classroomAry );
				break;
			}
		}
		
		$ReturnContent ['NoOfClassroom'] = $classroomAry ['NoOfClassroom'];
		$ReturnContent ['Classrooms'] = $classroomAry ['Classrooms'];
		$ReturnContent ['NoOfLesson'] = $lessonAry ['NoOfLesson'];
		$ReturnContent ['Lessons'] = $lessonAry ['Lessons'];
		
		return $ReturnContent;
	}
	function WS_eClassAppGetClassroom($parUserID, $PowerLessonOnly = false, $withActiveLessonClassroom = false, $activeLessonClassroomDisplayLang = "eng", $withMemberType = "", $noWWSClassroom = false) {
		global $intranet_db, $eclass_db, $eclass40_filepath, $eclass_root, $ck_default_lang;
		
		$filepath_prefix = $eclass_root ? $eclass_root : $eclass40_filepath;
		$powerlesson_class = array ();
		$ReturnContent = array ();
		$NoOfClassroom = 0;
		$ClassroomsXML = "";
		
		if ($PowerLessonOnly) {
			$sql = "SELECT
						SettingValue
					FROM
						$intranet_db.GENERAL_SETTING
					WHERE
						Module = 'eClassSettings' AND
						SettingName = 'isEmbeddedPowerLesson'";
			$isEmbeddedPowerLesson = $this->returnVector ( $sql );
			$isEmbeddedPowerLesson = ! empty ( $isEmbeddedPowerLesson ) ? current ( $isEmbeddedPowerLesson ) == 1 : false;
			
			if ($isEmbeddedPowerLesson) {
				$PowerLessonOnly = false;
			} else {
				$plcontent = trim ( get_file_content ( $filepath_prefix . "/files/powerlesson.txt" ) );
				$powerlesson_class = explode ( ",", $plcontent );
			}
		}
		
		$sql = "SELECT
					c.course_id,
					c.course_code,
					c.course_name,
					uc.user_course_id,
					uc.memberType,
					c.language,
					" . ($noWWSClassroom ? "IF(c.RoomType=13, 1, 0)" : "0") . " AS MustIncludedInList
				FROM
					$eclass_db.course AS c INNER JOIN
					$eclass_db.user_course AS uc ON c.course_id = uc.course_id INNER JOIN
					$intranet_db.INTRANET_USER AS iu ON iu.UserEmail = uc.user_email AND iu.UserID = '$parUserID'
				WHERE
					(
						c.RoomType=0
						" . (! $noWWSClassroom ? "OR c.RoomType=13" : "") . "
					)
					AND
					(
						uc.status is NULL OR
						uc.status NOT IN ('deleted')
					)
				GROUP BY
					c.course_id
				ORDER BY
					c.course_code, c.course_name";
		$ResultAry = $this->returnArray ( $sql );
		
		if ($withActiveLessonClassroom) {
			$displayLang = $activeLessonClassroomDisplayLang ? $activeLessonClassroomDisplayLang : $ck_default_lang;
			
			if (file_exists ( $filepath_prefix . "/src/lang/lang.$displayLang.php" )) {
				include_once ($filepath_prefix . "/src/lang/lang.$displayLang.php");
			}
			
			$ClassroomAry ['Classroom'] [$NoOfClassroom] = array (
					'CourseID' => 0,
					'ClassroomTitle' => $Lang ['PowerLessonApp'] ['LessonInProgress'],
					'UserCourseID' => 0,
					'MemberType' => '',
					'Language' => $activeLessonClassroomDisplayLang 
			);
			$NoOfClassroom ++;
		}
		
		for($i = 0; $i < count ( $ResultAry ); $i ++) {
			list ( $course_id, $course_code, $course_name, $user_course_id, $memberType, $language, $MustIncludedInList ) = $ResultAry [$i];
			
			if (! $PowerLessonOnly || $MustIncludedInList || in_array ( $course_id, $powerlesson_class )) {
				
				if ($withMemberType == "" || $withMemberType == $memberType) {
					$classroomTitle = ($course_code ? $course_code . ' - ' : '') . $course_name;
					
					$ClassroomAry ['Classroom'] [$NoOfClassroom] = array (
							'CourseID' => $course_id,
							'ClassroomTitle' => $classroomTitle,
							'UserCourseID' => $user_course_id,
							'MemberType' => $memberType,
							'Language' => $language 
					);
					$NoOfClassroom ++;
				}
			}
		}
		
		$ReturnContent ['NoOfClassroom'] = $NoOfClassroom;
		$ReturnContent ['Classrooms'] = $ClassroomAry;
		
		return $ReturnContent;
	}
	function WS_eClassAppGetLesson($UserCourseID, $displayLang = "eng", $classroomAryForActiveLessonList = array()) {
		global $intranet_db, $eclass_db, $ck_default_lang, $langID, $UserID;
		global $eclass40_filepath, $eclass_root;
		
		$filepath_prefix = $eclass_root ? $eclass_root : $eclass40_filepath;
		$ReturnContent = array ();
		$NoOfLesson = 0;
		$LessonsXML = "";
		
		$displayLang = $displayLang ? $displayLang : $ck_default_lang;
		
		if (file_exists ( $filepath_prefix . "/src/lang/lang.$displayLang.php" )) {
			include ($filepath_prefix . "/src/lang/lang.$displayLang.php");
		}
		
		if ($UserCourseID) {
			$sql = "SELECT course_id, memberType, user_id FROM $eclass_db.user_course WHERE user_course_id = $UserCourseID";
			list ( $course_id, $memberType, $user_id ) = current ( $this->returnArray ( $sql ) );
			
			if ($course_id) {
				if ($memberType != 'T' && file_exists ( $filepath_prefix . "/system/settings/config.inc.php" ) && file_exists ( $filepath_prefix . "/src/includes/php/lib-groups.php" )) {
					/**
					 * **************************************************************************************
					 * Remark : Can't called get_user_lesson_plan_id() in lib-lessonplan.php
					 * Reason : In get_user_lesson_plan_id(), $lg and $cfg_eclass_lib_path are global
					 * both variable can't be defined inside this function - WS_eClassAppGetLesson
					 * as they can't pass to get_user_lesson_plan_id() as global variable
					 * Solution : Copy the content of function get_user_lesson_plan_id() to here
					 * **************************************************************************************
					 */
					include_once ($filepath_prefix . "/system/settings/config.inc.php"); // this file is not included in IP, we have to include here and pass $cfg to corresponding function
					include_once ($filepath_prefix . "/src/includes/php/lib-groups.php");
					
					$lg = new libgroups ( classNamingDB ( $course_id ) );
					
					$only_plan_id_ary = array_values ( array_merge ( $lg->Get_Lesson_Plan_Group_FunctionID ( $user_id, true, $cfg ), $lg->Get_Lesson_Plan_Individual_FunctionID ( $user_id, true, $cfg ), $lg->Get_Lesson_Plan_All_Individual_FunctionID ( true, $cfg ) ) );
				}
				
				if ($memberType == 'T' || count ( $only_plan_id_ary )) {
					if ($langID != '' && $langID != null) {
						// Checking with langID variable value
						$chi = ($langID == 1 || $langID == 2);
					} else {
						// Checking with lang session
						$chi = ($displayLang == "chib5" || $displayLang == "chigb");
					}
					
					if ($chi) {
						$firstChoice = "u.chinesename";
						$altChoice = "concat(if(u.lastname is not null, concat(u.lastname, ' '), ''), if(u.firstname is not null, u.firstname, ''))";
						
						$eng_title = "''";
						$chi_title = "if(u.titlechinese IS NULL OR TRIM(u.titlechinese) = '','',concat(u.titlechinese))";
					} else {
						$firstChoice = "concat(if(u.lastname is not null, concat(u.lastname, ' '), ''), if(u.firstname is not null, u.firstname, ''))";
						$altChoice = "u.chinesename";
						
						$eng_title = "if(u.titleenglish IS NULL OR TRIM(u.titleenglish) = '','',concat(u.titleenglish))";
						$chi_title = "''";
					}
					
					$username_field = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '', concat($eng_title,$altChoice,$chi_title), concat($eng_title,$firstChoice,$chi_title))";
					
					$sql = "SELECT
								lp.plan_id AS lessonID,
								lp.Title AS lessonTitle,
								$username_field AS lessonTeacher,
								IF(ls.session_id IS NOT NULL, 1, IF(lp.is_used IS NULL OR lp.is_used = 0, 3, 2)) as lessonStatus,
								IF(ls.session_id IS NOT NULL, '', IF(lp.is_used IS NULL OR lp.is_used = 0, lp.expect_lesson_date, lp.real_lesson_date)) as lessonDate,
								IF(ls.session_id IS NOT NULL, '', lp.prelesson_startdate) as preLessonStartDate,
								IF(ls.session_id IS NOT NULL, '', lp.prelesson_enddate) as preLessonEndDate,
								IF(ls.session_id IS NOT NULL, '', lp.postlesson_startdate) as postLessonStartDate,
								IF(ls.session_id IS NOT NULL, '', lp.postlesson_enddate) as postLessonEndDate,
								ls.session_id,
								lp.is_private as isPrivate
							FROM
								" . classNamingDB ( $course_id ) . ".usermaster AS u INNER JOIN
								" . classNamingDB ( $course_id ) . ".lesson_plan AS lp ON u.user_id = lp.teacher_id LEFT JOIN
								" . classNamingDB ( $course_id ) . ".lesson_session AS ls ON lp.plan_id = ls.lesson_plan_id
							WHERE
								" . ($memberType == 'T' ? "lp.teacher_id = $user_id" : "lp.plan_id IN (" . implode ( ',', $only_plan_id_ary ) . ")") . "
							ORDER BY
								lessonStatus ASC, lp.real_lesson_date DESC, lp.expect_lesson_date DESC, lessonTitle ASC";
					$ResultAry = $this->returnArray ( $sql );
					
					for($i = 0; $i < count ( $ResultAry ); $i ++) {
						list ( $lessonID, $lessonTitle, $lessonTeacher, $lessonStatus, $lessonDate, $preLessonStartDate, $preLessonEndDate, $postLessonStartDate, $postLessonEndDate, $lessonSession, $isPrivate ) = $ResultAry [$i];
						
						$IsIncludeLesson = false; // used to define the lesson should be shown or not
						
						switch ($lessonStatus) {
							case 1 :
								// All 'In Progress' Lesson should be shown
								$IsIncludeLesson = true;
								
								$lessonDesc = $Lang ['PowerLessonApp'] ['InProgress'];
								break;
							case 2 :
								// All 'Delivered' Lesson should be shown
								$IsIncludeLesson = ($isPrivate == '1') ? false : true;
								
								// Only show post-lesson information if the lesson is delivered
								$lessonDesc = $Lang ['PowerLessonApp'] ['Delivered'];
								$lessonDesc .= $lessonDate && $lessonDate != "0000-00-00 00:00:00" ? " - " . date ( 'Y-m-d', strtotime ( $lessonDate ) ) : "";
								$lessonDesc .= $postLessonStartDate && $postLessonStartDate != "0000-00-00 00:00:00" && $postLessonEndDate && $postLessonEndDate != "0000-00-00 00:00:00" ? " (" . $Lang ['PowerLessonApp'] ['postLessonPhase'] . ": " . $Lang ['PowerLessonApp'] ['From'] . date ( 'Y-m-d', strtotime ( $postLessonStartDate ) ) . $Lang ['PowerLessonApp'] ['To'] . date ( 'Y-m-d', strtotime ( $postLessonEndDate ) ) . ")" : "";
								break;
							case 3 :
								// 'Not Yet Delivered' Lesson should be shown if the user is Teacher or within the Pre Lesson Period
								$IsIncludeLesson = ($memberType == 'T' || ($preLessonStartDate && $preLessonStartDate != "0000-00-00 00:00:00" && $preLessonEndDate && $preLessonEndDate != "0000-00-00 00:00:00" && strtotime ( date ( 'Y-m-d', strtotime ( $preLessonStartDate ) ) . ' 00:00:00' ) <= time () && strtotime ( date ( 'Y-m-d', strtotime ( $preLessonEndDate ) ) . ' 23:59:59' ) >= time ())) && (($isPrivate != '1'));
								
								// Only show pre-lesson information if the lesson is not yet delivered
								$lessonDesc = $lessonDate && $lessonDate != "0000-00-00 00:00:00" ? $Lang ['PowerLessonApp'] ['PlannedDate'] . ": " . date ( 'Y-m-d', strtotime ( $lessonDate ) ) : "";
								$lessonDesc .= ($preLessonStartDate && $preLessonStartDate != "0000-00-00 00:00:00" && $preLessonEndDate && $preLessonEndDate != "0000-00-00 00:00:00" ? " " . $Lang ['PowerLessonApp'] ['preLessonPhase'] . ": " . $Lang ['PowerLessonApp'] ['From'] . date ( 'Y-m-d', strtotime ( $preLessonStartDate ) ) . $Lang ['PowerLessonApp'] ['To'] . date ( 'Y-m-d', strtotime ( $preLessonEndDate ) ) : "");
								$lessonDesc = $Lang ['PowerLessonApp'] ['NotYetDelivered'] . ($lessonDesc ? ' (' . trim ( $lessonDesc ) . ')' : '');
								break;
						}
						
						if ($memberType == 'T' || $IsIncludeLesson) {
							$LessonAry ['Lesson'] [$NoOfLesson] = array (
									'LessonCourseID' => $course_id,
									'LessonID' => $lessonID,
									'LessonTitle' => $lessonTitle,
									'LessonTeacher' => $lessonTeacher,
									'LessonDesc' => $lessonDesc,
									'LessonStatus' => $lessonStatus,
									'LessonSession' => $lessonSession 
							);
							$NoOfLesson ++;
						}
					}
				}
				
				$ReturnContent ['NoOfLesson'] = $NoOfLesson;
				$ReturnContent ['Lessons'] = $LessonAry;
			}
		} else {
			if (! is_array ( $classroomAryForActiveLessonList ) || count ( $classroomAryForActiveLessonList ) == 0) {
				$classroomAryForActiveLessonList = $this->WS_eClassAppGetClassroom ( $UserID, true, true );
			}
			
			$sql = "SELECT courseID, lessonID, sessionID, title, teacherUserID, teacherName, targetUserIDs FROM $eclass_db.active_lesson_plan ORDER BY modifiedDate DESC";
			$activeLessonAry = $this->returnArray ( $sql );
			
			for($i = 0; $i < count ( $activeLessonAry ); $i ++) {
				list ( $lessonCourseID, $lessonID, $lessonSession, $lessonTitle, $teacherUserID, $teacherName, $targetUserIDs ) = $activeLessonAry [$i];
				
				$teacherNameAry = $teacherName ? unserialize ( $teacherName ) : array ();
				$targetUserIDAry = $targetUserIDs ? unserialize ( $targetUserIDs ) : array ();
				
				if ($UserID == $teacherUserID || (is_array ( $targetUserIDAry ) && in_array ( $UserID, $targetUserIDAry ))) {
					for($j = 1; $j < count ( $classroomAryForActiveLessonList ['Classrooms'] ['Classroom'] ); $j ++) { // $j=1 to start for skipping active lesson classroom
						if ($lessonCourseID == $classroomAryForActiveLessonList ['Classrooms'] ['Classroom'] [$j] ['CourseID']) {
							$lessonClassroomTitle = $classroomAryForActiveLessonList ['Classrooms'] ['Classroom'] [$j] ['ClassroomTitle'];
							$lessonUserCourseID = $classroomAryForActiveLessonList ['Classrooms'] ['Classroom'] [$j] ['UserCourseID'];
							$lessonMemberType = $classroomAryForActiveLessonList ['Classrooms'] ['Classroom'] [$j] ['MemberType'];
							$lessonLanguage = $classroomAryForActiveLessonList ['Classrooms'] ['Classroom'] [$j] ['Language'];
							
							$lessonDesc = $lessonClassroomTitle;
							
							$LessonAry ['Lesson'] [$NoOfLesson] = array (
									'LessonCourseID' => $lessonCourseID,
									'LessonClassroomTitle' => $lessonClassroomTitle,
									'LessonUserCourseID' => $lessonUserCourseID,
									'LessonMemberType' => $lessonMemberType,
									'LessonLanguage' => $lessonLanguage,
									'LessonID' => $lessonID,
									'LessonTitle' => $lessonTitle,
									'LessonTeacher' => $teacherNameAry [$displayLang],
									'LessonDesc' => $lessonDesc,
									'LessonStatus' => 1,
									'LessonSession' => $lessonSession 
							);
							$NoOfLesson ++;
							break;
						}
					}
				}
			}
			
			$ReturnContent ['NoOfLesson'] = $NoOfLesson;
			$ReturnContent ['Lessons'] = $LessonAry;
		}
		
		return $ReturnContent;
	}
	function WS_eClassAppGetStudent($CourseID, $LessonID, $SessionID) {
		global $intranet_db, $eclass_db, $intranet_root, $sys_custom;
		
		$sql = "SELECT COUNT(*) FROM $eclass_db.course WHERE course_id = '$CourseID'";
		
		if ($CourseID && current ( $this->returnVector ( $sql ) )) {
			$sql = "SELECT COUNT(*) FROM " . classNamingDB ( $CourseID ) . ".lesson_plan WHERE plan_id = '$LessonID'";
			
			if ($LessonID && current ( $this->returnVector ( $sql ) )) {
				$sql = "SELECT COUNT(*) FROM " . classNamingDB ( $CourseID ) . ".lesson_session WHERE session_id = '$SessionID' AND lesson_plan_id = '$LessonID'";
				
				if ($SessionID && current ( $this->returnVector ( $sql ) )) {
					// Get Student List
					$sql = "SELECT target_type FROM " . classNamingDB ( $CourseID ) . ".lesson_plan WHERE plan_id = '$LessonID'";
					$target_type = current ( $this->returnVector ( $sql ) );
					
					if ($target_type == 1) { // Group Lesson Plan
						$sql = "SELECT
									iu.UserID, uc.user_course_id, iu.UserEmail, iu.ChineseName, iu.EnglishName, iu.RecordType, iu.PersonalPhotoLink, iu.ClassName, iu.ClassNumber, u.user_id
								FROM
									" . classNamingDB ( $CourseID ) . ".grouping_function AS gf INNER JOIN
									" . classNamingDB ( $CourseID ) . ".lesson_plan_user_group AS ug ON gf.function_type = 'PL' AND gf.group_id IS NOT NULL AND gf.function_id = '$LessonID' AND gf.group_id = ug.lesson_group_id INNER JOIN
									" . classNamingDB ( $CourseID ) . ".usermaster AS u ON u.status IS NULL AND u.memberType !='T' AND ug.user_id = u.user_id INNER JOIN
									" . $eclass_db . ".user_course AS uc ON uc.course_id = '$CourseID' AND u.user_id = uc.user_id INNER JOIN
									" . $intranet_db . ".INTRANET_USER AS iu ON uc.user_email = iu.UserEmail
								WHERE
									iu.RecordStatus = 1
								ORDER BY
									iu.ClassName, iu.ClassNumber";
						$std_result = $this->returnArray ( $sql );
					} else { // Individual Lesson Plan
						$sql = "SELECT
									iu.UserID, uc.user_course_id, iu.UserEmail, iu.ChineseName, iu.EnglishName, iu.RecordType, iu.PersonalPhotoLink, iu.ClassName, iu.ClassNumber, u.user_id
								FROM
									" . classNamingDB ( $CourseID ) . ".grouping_function AS gf INNER JOIN
									" . classNamingDB ( $CourseID ) . ".usermaster AS u ON gf.function_type = 'PL' AND gf.group_id IS NULL AND gf.function_id = '$LessonID' AND u.status IS NULL AND u.memberType !='T' AND gf.user_id = u.user_id INNER JOIN
									" . $eclass_db . ".user_course AS uc ON uc.course_id = '$CourseID' AND u.user_id = uc.user_id INNER JOIN
									" . $intranet_db . ".INTRANET_USER AS iu ON uc.user_email = iu.UserEmail
								WHERE
									iu.RecordStatus = 1
								ORDER BY
									iu.ClassName, iu.ClassNumber";
						$std_result = $this->returnArray ( $sql );
						
						if (count ( $std_result ) == 0) {
							$sql = "SELECT
										iu.UserID, uc.user_course_id, iu.UserEmail, iu.ChineseName, iu.EnglishName, iu.RecordType, iu.PersonalPhotoLink, iu.ClassName, iu.ClassNumber, u.user_id
									FROM
										" . classNamingDB ( $CourseID ) . ".usermaster AS u INNER JOIN
										" . $eclass_db . ".user_course AS uc ON u.status IS NULL AND u.memberType !='T' AND uc.course_id = '$CourseID' AND u.user_id = uc.user_id INNER JOIN
										" . $intranet_db . ".INTRANET_USER AS iu ON uc.user_email = iu.UserEmail
									WHERE
										iu.RecordStatus = 1
									ORDER BY
										iu.ClassName, iu.ClassNumber";
							$std_result = $this->returnArray ( $sql );
						}
					}
					
					if ($sys_custom ['power_lesson_app_qrcode_std_list_no_login'] && count ( $std_result ) > 0) {
						$sql = "SELECT login_stud FROM " . classNamingDB ( $CourseID ) . ".lesson_session WHERE session_id = '$SessionID' AND lesson_plan_id = '$LessonID'";
						$login_stud_serialized = current ( $this->returnVector ( $sql ) );
						
						if ($login_stud_serialized) {
							$login_stud = unserialize ( $login_stud_serialized );
							
							if (count ( $login_stud ) > 0) {
								$final_std_result = array ();
								$login_stud_user_id_ary = array_keys ( $login_stud );
								
								for($i = 0; $i < count ( $std_result ); $i ++) {
									if (! in_array ( $std_result [$i] ['user_id'], $login_stud_user_id_ary )) {
										$final_std_result [] = $std_result [$i];
									}
								}
								
								$std_result = $final_std_result;
							}
						}
					}
					
					$ReturnContent ['NoOfStudent'] = count ( $std_result );
					
					for($i = 0; $i < $ReturnContent ['NoOfStudent']; $i ++) {
						switch ($std_result [$i] ['RecordType']) {
							case 1 :
								$std_UserType = 'T';
								break;
							case 2 :
								$std_UserType = 'S';
								break;
							case 3 :
								$std_UserType = 'P';
								break;
							default :
								$std_UserType = '';
						}
						
						$ReturnContent ['Student'] [] = array (
								'UserID' => $std_result [$i] ['UserID'],
								'UserCourseID' => $std_result [$i] ['user_course_id'],
								'UserEmail' => $std_result [$i] ['UserEmail'],
								'ChineseName' => $std_result [$i] ['ChineseName'],
								'EnglishName' => $std_result [$i] ['EnglishName'],
								'UserType' => $std_UserType,
								'PersonalPhotoLink' => (is_file ( $intranet_root . $std_result [$i] ['PersonalPhotoLink'] ) ? $std_result [$i] ['PersonalPhotoLink'] : ""),
								'ClassName' => $std_result [$i] ['ClassName'],
								'ClassNumber' => $std_result [$i] ['ClassNumber'] 
						);
					}
				} else {
					$ReturnContent = '806';
				}
			} else {
				$ReturnContent = '805';
			}
		} else {
			$ReturnContent = '804';
		}
		
		return $ReturnContent;
	}
	function WS_eClassAppValidate() {
		global $plugin, $sys_custom, $intranet_root, $FlippedChannelsConfig;
		
		if ($plugin ['power_lesson'] || $plugin ['WWS_eLearningProject']) { // Remark - Allow WWS Project to access PowerLesson App
			if (isset ( $plugin ['power_lesson_app_expiry_date'] ) && $plugin ['power_lesson_app_expiry_date'] != '') {
				if (isset($sys_custom['power_lesson_extend']) && $sys_custom['power_lesson_extend']) {
                    if (strtotime ( date ( 'Y-m-d 23:59:59', strtotime ( $plugin ['power_lesson_app_expiry_date'] ) ) ) > time ()) {
                        $ReturnContent ['ExpiryDate'] = $plugin ['power_lesson_app_expiry_date'];
                        if (isset ( $sys_custom ['power_lesson_app_webservice_timeout'] )) {
                            $ReturnContent ['CustTimeout'] = $sys_custom ['power_lesson_app_webservice_timeout'];
                        }

                        if (isset ( $sys_custom ['power_lesson_app_refresh_btn_cooldown'] )) {
                            $ReturnContent ['RefreshButtonCoolDownInterval'] = $sys_custom ['power_lesson_app_refresh_btn_cooldown'];
                        }

                        // Flipped Channels Settings
                        if (isset ( $plugin ['FlippedChannels'] ) && $plugin ['FlippedChannels']) {
                            include_once ($intranet_root . '/includes/FlippedChannels/libFlippedChannels.php');
                            $lFlippedChannels = new libFlippedChannels ();
                            $centralServerPath = $lFlippedChannels->getCurCentralServerUrl ();
                            $centralServerPath = ($centralServerPath == null) ? '' : $centralServerPath;

                            $ReturnContent ['FlippedChannels'] = '1';
                            $ReturnContent ['FlippedChannelsServerPath'] = $centralServerPath;
                        } else {
                            $ReturnContent ['FlippedChannels'] = '0';
                            $ReturnContent ['FlippedChannelsServerPath'] = '';
                        }

                        // Disable Relaunching Lesson
                        if (isset ( $sys_custom ['power_lesson_app_disable_relaunch_lesson'] )) {
                            $ReturnContent ['DisableRelaunchLesson'] = $sys_custom ['power_lesson_app_disable_relaunch_lesson'] ? '1' : '0';
                        } else {
                            $ReturnContent ['DisableRelaunchLesson'] = '0';
                        }

                        // Hide Loading View If No Internet
                        if (isset ( $sys_custom ['power_lesson_app_hide_loading_view_if_no_internet'] )) {
                            $ReturnContent ['HideLoadingViewIfNoInternet'] = $sys_custom ['power_lesson_app_hide_loading_view_if_no_internet'] ? '1' : '0';
                        } else {
                            $ReturnContent ['HideLoadingViewIfNoInternet'] = '0';
                        }
                    } else {
                        $ReturnContent = '803';
                    }
                } else {
                    $ReturnContent = '803';
                }
			} else {
				$ReturnContent = '802';
			}
		} else {
			$ReturnContent = '801';
		}
		
		return $ReturnContent;
	}
	function WS_eClassAppQRCodeValidate($CourseID, $LessonID, $SessionID) {
		global $intranet_db, $eclass_db, $ck_default_lang, $langID;
		global $intranet_root, $eclass40_filepath, $eclass_root;
		
		$sql = "SELECT course_code, course_name, language FROM $eclass_db.course WHERE course_id = '$CourseID'";
		$result = $this->returnArray ( $sql );
		
		if ($CourseID && count ( $result )) {
			list ( $CourseCode, $CourseName, $Language ) = current ( $result );
			
			$filepath_prefix = $eclass_root ? $eclass_root : $eclass40_filepath;
			
			if (file_exists ( $filepath_prefix . "/src/lang/lang.$Language.php" )) {
				include_once ($filepath_prefix . "/src/lang/lang.$Language.php");
			}
			
			if ($langID != '' && $langID != null) {
				// Checking with langID variable value
				$chi = ($langID == 1 || $langID == 2);
			} else {
				// Checking with lang session
				$chi = ($Language == "chib5" || $Language == "chigb");
			}
			
			if ($chi) {
				$firstChoice = "u.chinesename";
				$altChoice = "concat(if(u.lastname is not null, concat(u.lastname, ' '), ''), if(u.firstname is not null, u.firstname, ''))";
				
				$eng_title = "''";
				$chi_title = "if(u.titlechinese IS NULL OR TRIM(u.titlechinese) = '','',concat(u.titlechinese))";
			} else {
				$firstChoice = "concat(if(u.lastname is not null, concat(u.lastname, ' '), ''), if(u.firstname is not null, u.firstname, ''))";
				$altChoice = "u.chinesename";
				
				$eng_title = "if(u.titleenglish IS NULL OR TRIM(u.titleenglish) = '','',concat(u.titleenglish))";
				$chi_title = "''";
			}
			
			$username_field = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '', concat($eng_title,$altChoice,$chi_title), concat($eng_title,$firstChoice,$chi_title))";
			
			$sql = "SELECT
						lp.Title AS LessonTitle,
						$username_field AS LessonTeacher
					FROM
						" . classNamingDB ( $CourseID ) . ".usermaster AS u INNER JOIN
						" . classNamingDB ( $CourseID ) . ".lesson_plan AS lp ON u.user_id = lp.teacher_id
					WHERE
						lp.plan_id = '$LessonID'";
			$result = $this->returnArray ( $sql );
			
			if ($LessonID && count ( $result )) {
				list ( $LessonTitle, $LessonTeacher ) = current ( $result );
				
				$sql = "SELECT COUNT(*) FROM " . classNamingDB ( $CourseID ) . ".lesson_session WHERE session_id = '$SessionID' AND lesson_plan_id = '$LessonID'";
				if ($SessionID && current ( $this->returnVector ( $sql ) )) {
					// Get School, Classroom, Lesson Information
					// list($school_name, $school_org) = split("\n",get_file_content("$intranet_root/file/school_data.txt"));
					list ( $school_name, $school_org ) = explode ( "\n", get_file_content ( "$intranet_root/file/school_data.txt" ) );
					$ReturnContent ['SchoolName'] = htmlspecialchars ( $school_name );
					$ReturnContent ['SchoolOrg'] = htmlspecialchars ( $school_org );
					$ReturnContent ['ClassroomTitle'] = ($CourseCode ? $CourseCode . ' - ' : '') . $CourseName;
					$ReturnContent ['Language'] = $Language;
					$ReturnContent ['LessonTitle'] = $LessonTitle;
					$ReturnContent ['LessonTeacher'] = $LessonTeacher;
					$ReturnContent ['LessonDesc'] = $Lang ['PowerLessonApp'] ['InProgress'];
					$ReturnContent ['LessonStatus'] = 1;
				} else {
					$ReturnContent = '806';
				}
			} else {
				$ReturnContent = '805';
			}
		} else {
			$ReturnContent = '804';
		}
		
		return $ReturnContent;
	}
	function WS_eClassAppRelaunchLessonValidate($CourseID, $LessonID, $SessionID) {
		$sql = "SELECT COUNT(*) FROM " . classNamingDB ( $CourseID ) . ".lesson_session WHERE session_id = '$SessionID' AND lesson_plan_id = '$LessonID'";
		
		if (current ( $this->returnVector ( $sql ) )) {
			$ReturnContent = array ();
		} else {
			$ReturnContent = '806';
		}
		
		return $ReturnContent;
	}
	function convertFileToFLV($fileSource, $fileDest, $ext, $fileName = '') {
		global $eclass_root;
		
		include_once ($eclass_root . '/src/tool/flvplayer/lib-flvplayer.php');
		$lflv = new flvplayer ();
		
		$FileExtArr = $lflv->getFlvConvertableFileExtensions ();
		
		$result_filename = '';
		if ($fileName != '')
			$result_filename = strtolower ( substr ( $fileName, 0, strrpos ( $fileName, "." ) ) ) . ".flv";
		
		$loc = $fileSource;
		$result_filepath = $fileDest;
		
		$ext = strtoupper ( str_replace ( ".", "", $ext ) );
		if (in_array ( strtoupper ( $ext ), $FileExtArr )) {
			$otherParams = array ();
			
			$IsSuccess = $lflv->convertToFlv ( $loc, $result_filename, $result_filepath, $otherParams );
			
			if ($IsSuccess) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
	function WS_GetChildrenListForApp($parUserLogin) {
		$sql = "SELECT UserID From INTRANET_USER Where UserLogin = '" . $parUserLogin . "'";
		$userAry = $this->returnResultSet ( $sql );
		$parentUserId = $userAry [0] ['UserID'];
		
		$sql = "SELECT 
						u.UserID
	            FROM 
						INTRANET_PARENTRELATION r
	            		INNER JOIN INTRANET_USER u ON r.StudentID = u.UserID 
	            WHERE 
						r.ParentID = '" . $parentUserId . "' 
				";
		$studentAry = $this->returnResultSet ( $sql );
		$studentIdAry = Get_Array_By_Key ( $studentAry, 'UserID' );
		
		return $this->GetStudentInfo_eClassApp ( $studentIdAry );
    }
	function WS_GetStudentAttendanceInfo($parUserId, $ParLang) {
		global $plugin, $PATH_WRT_ROOT, $eclassAppConfig, $sys_custom, $Lang;

		include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
		$leClassApp_init = new libeClassApp_init();
		$session_language = $leClassApp_init->getPageLang($ParLang);
		include_once($PATH_WRT_ROOT."lang/lang.".$session_language.".php");

		$returnAry = array ();
		$returnAry ['RecordDate'] = '';
		$returnAry ['AttendanceStatus'] = '';
		$returnAry ['ArrivalTime'] = '---';
		$returnAry ['LeaveTime'] = '---';
        $returnAry ['LunchOutTime'] = '---';
        $returnAry ['LunchBackTime'] = '---';

		if ($plugin ['attendancestudent'] && ! $sys_custom ['eClassApp'] ['hideHomepageTapCardTime'])
		{
		    $parUserId = IntegerSafe($parUserId);
		    
			include_once ($PATH_WRT_ROOT . 'includes/eClassApp/libeClassApp.php');
			$libeClassApp = new libeClassApp ();
			$eClassAppSettingsObj = $libeClassApp->getAppSettingsObj ();
			$eAttendanceCode = $eclassAppConfig ['moduleCode'] ['eAttendance'];
			$moduleAccessAry = $libeClassApp->getUserModuleAccessRight ( $parUserId );
			$enabledAttendance = $moduleAccessAry [$eAttendanceCode] ['RecordStatus'];

			if ($enabledAttendance) {
				$hostel_in_time = '';
				$hostel_out_time = '';

				if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
					include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
					$lc = new libcardstudentattend2();
					$record_params = array('RecordDate'=>date('Y-m-d'),'AcademicYearID'=>Get_Current_Academic_Year_ID(),'CategoryID'=>$lc->HostelAttendanceGroupCategory,'UserStatus'=>1);
					$record_params['UserID'] = $parUserId;
					$records = $lc->getHostelAttendanceRecords(date('Y'), date('m'), $record_params);
					if(count($records) > 0) {
						$record = $records[0];
						if($record['InTime'] != '') {
							$hostel_in_time = date("H:i", strtotime($record['InTime'])).'('.$Lang['StudentAttendance']['Hostel'].')';
						}
						if($record['OutTime'] != '') {
							$hostel_out_time = date("H:i", strtotime($record['OutTime'])).'('.$Lang['StudentAttendance']['Hostel'].')';
						}
					}
				}

				$cardTable = 'CARD_STUDENT_DAILY_LOG_' . date ( "Y" ) . '_' . date ( "m" );
				
				$sql = "SELECT InSchoolTime, LeaveSchoolTime, AMStatus, PMStatus, LeaveStatus, LunchOutTime, LunchBackTime FROM $cardTable WHERE DayNumber = " . date ( 'd' ) . " and UserID = '" . $parUserId."'";
				$record = $this->returnArray ( $sql );
				
				$inSchoolTime = $record [0] ['InSchoolTime'];
				$leaveSchoolTime = $record [0] ['LeaveSchoolTime'];
				$amStatus = $record [0] ['AMStatus'];
				$pmStatus = $record [0] ['PMStatus'];
				$leaveStatus = $record [0] ['LeaveStatus'];
                $LunchOutTime = $record [0] ['LunchOutTime'];
                $lunchBackTime = $record [0] ['LunchBackTime'];

                $returnAry ['RecordDate'] = date ( 'Y-m-d' );
				
				$attendanceLeaveSectionEnable = $eClassAppSettingsObj->getSettingsValue ( $eclassAppConfig ['INTRANET_APP_SETTINGS'] ['SettingName'] ['attendanceLeaveSectionEnable'] );
				$attendanceTimeEnable = $eClassAppSettingsObj->getSettingsValue ( $eclassAppConfig ['INTRANET_APP_SETTINGS'] ['SettingName'] ['attendanceTimeEnable'] );
				if ($attendanceTimeEnable != 0) {
					// return attendance time
					if (! is_date_empty ( $inSchoolTime )) {
						$returnAry ['ArrivalTime'] = substr ( $inSchoolTime, 0, 5 );
					} else if (! is_date_empty ( $lunchBackTime )) {
						$returnAry ['ArrivalTime'] = substr ( $lunchBackTime, 0, 5 );
					}

					if (! is_date_empty ( $leaveSchoolTime ) && $attendanceLeaveSectionEnable) {
						$returnAry ['LeaveTime'] = substr ( $leaveSchoolTime, 0, 5 );
					}

					if($sys_custom['StudentAttendance']['eclassApp']['showLunchTimeInOutAtHomepage']){
                        if (! is_date_empty ( $LunchOutTime )) {
                            $returnAry ['LunchOutTime'] = substr ( $LunchOutTime, 0, 5 );
                        }
                        if (! is_date_empty ( $lunchBackTime )) {
                            $returnAry ['LunchBackTime'] = substr ( $lunchBackTime, 0, 5 );
                        }
                    }
				} else {
					// do not return attendance time
				}

                $returnAry ['showLunchTimeInOutAtHomepage'] = isset($sys_custom['StudentAttendance']['eclassApp']['showLunchTimeInOutAtHomepage'])?$sys_custom['StudentAttendance']['eclassApp']['showLunchTimeInOutAtHomepage']:false;

                $attendanceStatusEnable = $eClassAppSettingsObj->getSettingsValue ( $eclassAppConfig ['INTRANET_APP_SETTINGS'] ['SettingName'] ['attendanceStatusEnable'] );
				if ($attendanceStatusEnable != 0) {
					/*
					 * AMStatus, PMStatus, LeaveStatus, RecordStatus
					 *
					 * P: Present (On time)
					 * A: Absent
					 * T: Late
					 * O: Outing
					 * L: Left school
					 */
					$attendanceStatus = '';
					if ($leaveStatus != '') {
						$attendanceStatus = "L";
					} elseif (! is_null ( $pmStatus )) { // follow PM status first
						switch ($pmStatus) {
							case "0" :
								$attendanceStatus = "P";
								break;
							case "1" :
								$attendanceStatus = "A";
								break;
							case "2" :
								$attendanceStatus = "T";
								break;
							case "3" :
								$attendanceStatus = "O";
								break;
							default :
								$attendanceStatus = 'A';
						}
					} elseif (! is_null ( $amStatus )) { // the follow AM status
						switch ($amStatus) {
							case "0" :
								$attendanceStatus = "P";
								break;
							case "1" :
								$attendanceStatus = "A";
								break;
							case "2" :
								$attendanceStatus = "T";
								break;
							case "3" :
								$attendanceStatus = "O";
								break;
							default :
								$attendanceStatus = 'A';
						}
					}
					if ($sys_custom ['eClassApp'] ['attendanceShowPresentAbsentOnly']) {
						if ($attendanceStatus == 'P' || $attendanceStatus == 'T') {
							$attendanceStatusArrive = 'P_include_late';
						}
					}

					$returnAry ['AttendanceStatus'] = $attendanceStatus;
					
					// ## Arrival status
					$attendanceStatusArrive = '';
					$targetArriveStatus = '';
					if (! is_null ( $amStatus )) {
						$targetArriveStatus = $amStatus;
					} else if (! is_null ( $pmStatus )) {
						$targetArriveStatus = $pmStatus;
					}
					switch ($targetArriveStatus) {
						case "0" :
							$attendanceStatusArrive = "P"; // present (on-time)
							break;
						case "1" :
							$attendanceStatusArrive = "A"; // absent
							break;
						case "2" :
							$attendanceStatusArrive = "T"; // late
							break;
						case "3" :
							$attendanceStatusArrive = "O"; // outing
							break;
						default :
							$attendanceStatusArrive = ''; // no tap card record => no status
					}
					if ($sys_custom ['eClassApp'] ['attendanceShowPresentAbsentOnly']) {
						if ($attendanceStatusArrive == 'P' || $attendanceStatusArrive == 'T') {
							$attendanceStatusArrive = 'P_include_late';
						}
					}
					$returnAry ['AttendanceStatusArrive'] = $attendanceStatusArrive;
					
					// ## Leave status
					$attendanceStatusLeave = '';
					if ($leaveStatus !== '' && $attendanceLeaveSectionEnable) {
						switch ($leaveStatus) {
							case "0" :
								$attendanceStatusLeave = "NL"; // normal leave
								break;
							case "1" :
								$attendanceStatusLeave = "ELAM"; // early leave (am)
								break;
							case "2" :
								$attendanceStatusLeave = "ELPM"; // early leave (pm)
								break;
						}
					}
					if (($sys_custom ['eClassApp'] ['attendanceShowPresentAbsentOnly']||$sys_custom['eClassApp']['attendanceShowNormalLeaveStatus']) && $attendanceStatusLeave == 'NL') {
						$attendanceStatusLeave = 'NL_display_status';
					}
					$returnAry ['AttendanceStatusLeave'] = $attendanceStatusLeave;
				} // end if $attendanceStatusEnable

				if($hostel_in_time != '') {
					if($returnAry ['ArrivalTime'] == '---') {
						$returnAry ['ArrivalTime'] = $hostel_in_time;
					} else {
						$returnAry ['ArrivalTime'] .= '<br>'.$hostel_in_time;
					}
				}

				if($hostel_out_time != '') {
					if($returnAry ['LeaveTime'] == '---') {
						$returnAry ['LeaveTime'] = $hostel_out_time;
					} else {
						$returnAry ['LeaveTime'] .= '<br>'.$hostel_out_time;
					}
				}
			} // if ($enabledAttendance)
		} // end if ($plugin['attendancestudent'])

		return $returnAry;
	}
	function WS_GetAttendanceDetailsForApp($TargetUserID, $year, $month, $numberOfMonthRequired = 1) {
		global $PATH_WRT_ROOT, $eclassAppConfig, $sys_custom;
		include_once ($PATH_WRT_ROOT . "includes/libcardstudentattend2.php");
		include_once ($PATH_WRT_ROOT . "includes/eClassApp/libeClassApp.php");
		
		$libeClassApp = new libeClassApp ();
		$eClassAppSettingsObj = $libeClassApp->getAppSettingsObj ();
		$libcard = new libcardstudentattend2 ();
		$libcard->retrieveSettings ();
		$attendance_mode = $libcard->attendance_mode;
		
		// U88786 2015-12-16 Roy
		// hide attendance time according to eclass app setting
		$eAttendanceTimeEnable = $eClassAppSettingsObj->getSettingsValue ( $eclassAppConfig ['INTRANET_APP_SETTINGS'] ['SettingName'] ['eAttendanceTimeEnable'] );
		if ($eAttendanceTimeEnable == null) {
			$eAttendanceTimeEnable = 1;
		}

		for($index = 0; $index < $numberOfMonthRequired; $index ++) {
			
			$monthLateCount = 0;
			$monthOutingCount = 0;
			$monthEarlyLeaveCount = 0;
			$monthAbsentCount = 0;
			
			$month_with_pad = str_pad ( $month, 2, "0", STR_PAD_LEFT );
			$result = $libcard->retrieveStudentMonthlyRecord ( $TargetUserID, $year, $month_with_pad, $order = 1, $get_detail = true, $attendStatusDefaultAbsent = false );
			
			if (count ( $result ) == 0) {
				$ReturnAry [$year] [$month] ['MonthlyAttendance'] = array ();
			}
			
			foreach ( $result as $attendanceRecord ) {
				list ( $dayNumber, $inSchoolTime, $inSchoolStation, $amStatus, $lunchOutTime, $lunchOutStation, $lunchBackTime, $lunchBackStation, $pmStatus, $leaveSchoolTime, $leaveStatus, $amReason, $amRemark, $pmReason, $pmRemark, $amWaive, $pmWaive, $earlyLeaveReason, $earlyLeaveWaive ) = $attendanceRecord;
				
				// skip no status cases
				if ($amStatus == null && $pmStatus == null && $leaveStatus == null) {
					continue;
				}

				$attendance_mode = $libcard->attendance_mode;
				if($sys_custom['StudentAttendance']['KIS_AttendanceUseClassGroup']) {
					include_once($PATH_WRT_ROOT . "includes/form_class_manage.php");
					$TargetDate = $year . '-' . $month . '-' . $dayNumber;
					$semInfo = getAcademicYearAndYearTermByDate($TargetDate);
					$sql = "SELECT ycu.YearClassID from YEAR_CLASS_USER as ycu  
					Inner JOIN YEAR_CLASS yc on ycu.YearClassID = yc.YearClassID where yc.AcademicYearID='" . $semInfo['AcademicYearID'] . "' and ycu.UserID = $TargetUserID";
					$rs = $this->returnResultSet($sql);
					if (is_array($rs)) {
						$studentClassId = $rs[0]['YearClassID'];
						$YearClass = new year_class();
						$ClassGroupInfo = $YearClass->Get_Class_Group_Info($studentClassId);
						if (is_array($ClassGroupInfo)) {
							$class_attendance_type = $ClassGroupInfo[0]['AttendanceType'];
							if ($class_attendance_type == '1') { //AM only
								$attendance_mode = 0;
							} else if ($class_attendance_type == '2') { //PM only
								$attendance_mode = 1;
							} else if ($class_attendance_type == '3') {
								$attendance_mode = 2;
							}
						}
					}
				}

				// define("CARD_STATUS_PRESENT",0);
				// define("CARD_STATUS_ABSENT",1);
				// define("CARD_STATUS_LATE",2);
				// define("CARD_STATUS_OUTING",3);
				// define("CARD_LEAVE_NORMAL",0);
				// define("CARD_LEAVE_AM",1);
				// define("CARD_LEAVE_PM",2);
				
				// attendance mode 0: AM Only
				// attendance mode 1: PM Only
				// attendance mode 2: WD with lunch time
				// attendance mode 3: WD without lunch time
				
				// AM time String:
				if ($leaveStatus == 1) { // leave am
					$lunchOutTime = $leaveSchoolTime;
				}
				
				if ($attendance_mode != 1) { // not PM only mode
					if ($amWaive == 1) {
						$amStatus = 0;
					}
					$amTimeString = "";
					if ($amStatus == 1) {
						$amReason = ($amReason) ? $amReason : "absent";
						$amTimeString = $amReason;
					} else if ($amStatus == 3) {
						$amReason = ($amReason) ? $amReason : "outing";
						$amTimeString = $amReason;
					} else {
					    if ($inSchoolTime != null || $lunchOutTime != null) {
							if ($attendance_mode == 0) { // AM only
								////$amTimeString = $inSchoolTime . " - " . $leaveSchoolTime;
								if ($inSchoolTime != null){
									$amTimeString .= $inSchoolTime;
								} else {
									$amTimeString .= "-";
								}
								if ($leaveSchoolTime != null && in_array($leaveStatus,array('','0','1'))){ // normal leave or AM early leave
									$amTimeString .= " - " . $leaveSchoolTime;
								} else{
									$amTimeString .= " - -";
								}
							} else if ($attendance_mode == 2) { // whole day with lunch
								////$amTimeString = $inSchoolTime . " - " . $lunchOutTime;
								if ($inSchoolTime != null){
									$amTimeString .= $inSchoolTime;
								} else {
									$amTimeString .= "-";
								}
								if ($lunchOutTime != null){
									$amTimeString .= " - " . $lunchOutTime;
								} else {
									$amTimeString .= " - -";
								}
							} else if ($attendance_mode == 3) { // whole day without lunch
								if ($inSchoolTime != null){
									$amTimeString = $inSchoolTime;
								} else {
									$amTimeString = "-";
								}
								if ($leaveStatus == 1 && $leaveSchoolTime != null){ // can be AM early leave
									$amTimeString .= " - " . $leaveSchoolTime;
								} else {
									$amTimeString .= " - -";
								}
							}
						} else {
							$amTimeString = "no data";
						}
						
						if ($eAttendanceTimeEnable != 1) {
							$amTimeString = "- - -";
						}
					}
				} else {
					$amTimeString = "- - -";
				}
				
				// PM time String:
				if ($attendance_mode != 0) { // not AM only mode
					// [U116274] Roy - also apply pm waive to early leave am case
					// pm status is waived
					if ($pmWaive == 1) {
						$pmStatus = 0;
					}
					
					if ($leaveStatus == 1) {
						// if early leave AM
						$pmTimeString = "- - -";
					} else {
						$pmTimeString = "";
						if ($pmStatus == 1) {
							$pmReason = ($pmReason) ? $pmReason : "absent";
							$pmTimeString = $pmReason;
						} else if ($pmStatus == 3) {
							$pmReason = ($pmReason) ? $pmReason : "outing";
							$pmTimeString = $pmReason;
						} else {
							if ($attendance_mode == 1) { // PM only mode
								if ($inSchoolTime != null){
									$pmTimeString = $inSchoolTime;
								} else {
									$pmTimeString = "-";
								}
								if ($leaveSchoolTime != null && in_array($leaveStatus,array('','0','2'))){ // normal leave or PM early leave
									$pmTimeString .= " - " . $leaveSchoolTime;
								} else {
									$pmTimeString .= " - -";
								}
							}else if ($lunchBackTime != null || $leaveSchoolTime != null) {
								////if ($attendance_mode == 1) { // PM only mode
									// $pmTimeString = $lunchBackTime." - ".$leaveSchoolTime;
									////$pmTimeString = $inSchoolTime . " - " . $leaveSchoolTime;
								////} else 
								if ($attendance_mode == 2) { // whole day with lunch
									////$pmTimeString = $lunchBackTime . " - " . $leaveSchoolTime;
									if ($lunchBackTime != null){
										$pmTimeString = $lunchBackTime;
									} else {
										$pmTimeString = "-";
									}
									if ($leaveSchoolTime != null && in_array($leaveStatus,array('','0','2'))){ // normal leave or PM early leave
										$pmTimeString .= " - " . $leaveSchoolTime;
									}else{
										$pmTimeString .= " - -";
									}
								} else if ($attendance_mode == 3) { // whole without lunch
									if ($lunchBackTime != null){ // can be AM absent, come back in PM session, then has lunch back time(i.e. PM in time)
										$pmTimeString = $lunchBackTime;
									}else{
										$pmTimeString = "-";
									}
									if ($leaveSchoolTime != null && in_array($leaveStatus,array('','0','2'))){ // normal leave or PM early leave
										$pmTimeString .= " - " . $leaveSchoolTime;
									}else{
										$pmTimeString .= " - -";
									}
								}
							} else {
								$pmTimeString = "no data";
							}
							
							if ($eAttendanceTimeEnable != 1) {
								$pmTimeString = "- - -";
							}
						}
					}
				} else {
					$pmTimeString = "- - -";
				}
				
				// calculate late minutes
				$amLateMinutes = 0;
				$pmLateMinutes = 0;
				$TargetDate = $year . '-' . $month . '-' . $dayNumber;
				$AttendTimeSetting = $libcard->Get_Student_Attend_Time_Setting ( $TargetDate, $TargetUserID );
				if (is_array ( $AttendTimeSetting )) {
					$amAttendTime = strtotime ( $AttendTimeSetting ["MorningTime"] );
					$pmAttendTime = strtotime ( $AttendTimeSetting ["LunchEnd"] );
					
					if ($amStatus == 2) {
						if ($attendance_mode == 0 || $attendance_mode == 2 || $attendance_mode == 3) {
							if ($inSchoolTime != null && $inSchoolTime != "-") {
								$amLateMinutes = ceil ( (strtotime ( $inSchoolTime ) - $amAttendTime) / 60 );
							}
						}
					}
					if ($pmStatus == 2) {
						if ($attendance_mode == 1) {
							if ($inSchoolTime != null && $inSchoolTime != "-") {
								$pmLateMinutes = ceil ( (strtotime ( $inSchoolTime ) - $pmAttendTime) / 60 );
							}
						} else if ($attendance_mode == 2) {
							if ($lunchBackTime != null && $lunchBackTime != "-") {
								$pmLateMinutes = ceil ( (strtotime ( $lunchBackTime ) - $pmAttendTime) / 60 );
							}
						}
					}
				}
				
				if ($eAttendanceTimeEnable != 1) {
					$amLateMinutes = - 1;
					$pmLateMinutes = - 1;
				}
				
				// Absent Count
				$GeneralSettings = new libgeneralsettings ();
				$Settings = $GeneralSettings->Get_General_Setting ( 'StudentAttendance', array (
						"'ProfileAttendCount'" 
				) );
				$absentCountSetting = $Settings ['ProfileAttendCount']; // 0: half day also count 1; 1: half day count 0.5
				
				if ($absentCountSetting == 0) {
					if ($amStatus == 1 && $pmStatus == 1) {
						$monthAbsentCount ++;
					} else if ($amStatus == 1) {
						$monthAbsentCount ++;
					} else if ($pmStatus == 1) {
						$monthAbsentCount ++;
					}
				} else if ($absentCountSetting == 1) {
					if ($amStatus == 1) {
						$monthAbsentCount = $monthAbsentCount + 0.5;
					}
					if ($pmStatus == 1) {
						$monthAbsentCount = $monthAbsentCount + 0.5;
					}
				}
				
				// Late Count
				if ($amStatus == 2) {
					$monthLateCount ++;
				}
				if ($pmStatus == 2) {
					$monthLateCount ++;
				}
				
				// Outing Count
				if ($amStatus == 3) {
					$monthOutingCount ++;
				}
				if ($pmStatus == 3) {
					$monthOutingCount ++;
				}
				
				// Earlyleave Count
				// $leaveStatus set to normal if waived
				if ($earlyLeaveWaive == 1) {
					$leaveStatus = 0;
				}
				if ($leaveStatus != 0) {
					$monthEarlyLeaveCount ++;
				}
				
				// replace null to -1
				$amStatus = ($amStatus == null) ? - 1 : $amStatus;
				$pmStatus = ($pmStatus == null) ? - 1 : $pmStatus;
				$leaveStatus = ($leaveStatus == null) ? - 1 : $leaveStatus;

				if($sys_custom['StudentAttendance']['KIS_AttendanceUseClassGroup']) {
					if($attendance_mode == 0) {
						$pmStatus = 999;
					} elseif($attendance_mode == 1) {
						$amStatus = 999;
					}
				}

				$ReturnAry [$year] [$month] ['MonthlyAttendance'] [] = array (
						"DayNumber" => $dayNumber,
						"AMStatus" => $amStatus,
						"PMStatus" => $pmStatus,
						"LeaveStatus" => $leaveStatus,
						"AMLateMinutes" => $amLateMinutes,
						"PMLateMinutes" => $pmLateMinutes,
						"AMTimeString" => $amTimeString,
						"PMTimeString" => $pmTimeString 
				);
			}
			
			$ReturnAry [$year] [$month] ['LateCount'] = $monthLateCount;
			$ReturnAry [$year] [$month] ['OutingCount'] = $monthOutingCount;
			$ReturnAry [$year] [$month] ['EarlyLeaveCount'] = $monthEarlyLeaveCount;
			$ReturnAry [$year] [$month] ['AbsentCount'] = $monthAbsentCount;
			
			// move to previous month
			if ($month > 1) {
				$month --;
			} else {
				$month = 12;
				$year --;
			}
		}
		
		return $ReturnAry;
	}
	
	// cannot replace directly to EJ because of a.EventLocationID
	function WS_GetSchoolEventForApp($TargetUserID, $StartDate, $EndDate, $ParentUserID, $ParLang="") {
		global $PATH_WRT_ROOT,$sys_custom;
		
		// retrieve UserID (student + active)
		// $sql = "select UserID, UserLogin from INTRANET_USER where UserID='".$TargetUserID."' and RecordType=2 and RecordStatus=1";
		$sql = "select UserID, UserLogin, RecordType from INTRANET_USER where UserID='" . $TargetUserID . "' and (RecordType=2 OR RecordType=1)";
		$rs = $this->returnResultSet ( $sql );
		$ThisUserID = $rs [0] ['UserID'];
		$ThisUserLogin = $rs [0] ['UserLogin'];
		$ThisRecordType = $rs [0] ['RecordType'];
		
		// check user exists
		if (! $ThisUserID) {
			return 104;
		}
		
		$conds_parentId = '';
		if ($ThisRecordType == USERTYPE_STUDENT) {
			$parentIdAry = array ();
			if ($ParentUserID == '') {
				$sql = "Select ParentID From INTRANET_PARENTRELATION Where StudentID = '" . $TargetUserID . "'";
				$parentIdAry = Get_Array_By_Key ( $this->returnResultSet ( $sql ), 'ParentID' );
			} else {
				$parentIdAry [] = $ParentUserID;
			}
			
			$numOfParent = count ( $parentIdAry );
			if ($numOfParent > 0) {
				$condsParentIdAry = array ();
				for($i = 0; $i < $numOfParent; $i ++) {
					$_parentId = $parentIdAry [$i];
					$condsParentIdAry [] = " c.UserID = '" . $_parentId . "' ";
				}
				$conds_parentId = " OR " . implode ( " OR ", $condsParentIdAry );
			}
		}
		
		// ## Get Event
		$conds_eventDate = " AND '" . $StartDate . "' <= DATE(a.EventDate) AND  DATE(a.EventDate) <= '" . $EndDate . "'";
		$conds_classGroupId = '';
		if ($this->isKIS ( $ThisUserLogin )) {
			$sql = "Select 
							yc.ClassGroupID
					From
							YEAR_CLASS_USER as ycu
							Inner Join YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
					Where
							yc.AcademicYearID = '" . Get_Current_Academic_Year_ID () . "'
					";
			$classInfoAry = $this->returnResultSet ( $sql );
			$classGroupId = $classInfoAry [0] ['ClassGroupID'];
			
			$conds_classGroupId = " AND (a.ClassGroupID = 0 || a.ClassGroupID is null || a.ClassGroupID = '" . $classGroupId . "') ";
		}

//        if($sys_custom['eClassApp']['SFOC']) {
            // // get group events which the student is a group member
            $sql1 = "SELECT DISTINCT a.EventID, a.EventDate as EventDate, a.RecordType, a.Title, a.Description, a.EventVenue, a.EventNature, a.EventLocationID, a.ClassGroupID, a.EventAssociation, EventStartTime, EventEndTime, Email, ContactNo, Website, SportType, a.TitleEng, a.EventAssociationEng, a.EventVenueEng, a.EventNatureEng, a.DescriptionEng FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b, INTRANET_USERGROUP AS c WHERE (c.UserID = " . $ThisUserID . " $conds_parentId) AND c.GroupID = b.GroupID AND b.EventID = a.EventID AND a.RecordStatus = '1' $conds_eventDate $conds_classGroupId";
            $groupMemberEventAry = $this->returnResultSet ( $sql1 );

            // get group events which the event is public for all students
            $sql3 = "SELECT DISTINCT a.EventID, a.EventDate as EventDate, a.RecordType, a.Title, a.Description, a.EventVenue, a.EventNature, a.EventLocationID, a.ClassGroupID, a.EventAssociation, EventStartTime, EventEndTime, Email, ContactNo, Website, SportType, a.TitleEng, a.EventAssociationEng, a.EventVenueEng, a.EventNatureEng, a.DescriptionEng FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID WHERE b.EventID IS NULL AND a.RecordType = '2' and a.RecordStatus='1' $conds_eventDate $conds_classGroupId";
            $publicEventAry = $this->returnResultSet ( $sql3 );

            // get all other events
            $sql2 = "SELECT DISTINCT a.EventID, a.EventDate as EventDate, a.RecordType, a.Title, a.Description, a.EventVenue, a.EventNature, a.EventLocationID, a.ClassGroupID, a.EventAssociation, EventStartTime, EventEndTime, Email, ContactNo, Website, SportType, a.TitleEng, a.EventAssociationEng, a.EventVenueEng, a.EventNatureEng, a.DescriptionEng FROM INTRANET_EVENT AS a WHERE a.RecordStatus = '1' AND a.RecordType <> '2' $conds_eventDate $conds_classGroupId";
            $nonMemberGroupEventAry = $this->returnResultSet ( $sql2 );
//        }else{
//            // // get group events which the student is a group member
//            $sql1 = "SELECT DISTINCT a.EventID, a.EventDate as EventDate, a.RecordType, a.Title, a.Description, a.EventVenue, a.EventNature, a.EventLocationID, a.ClassGroupID FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b, INTRANET_USERGROUP AS c WHERE (c.UserID = " . $ThisUserID . " $conds_parentId) AND c.GroupID = b.GroupID AND b.EventID = a.EventID AND a.RecordStatus = '1' $conds_eventDate $conds_classGroupId";
//            $groupMemberEventAry = $this->returnResultSet ( $sql1 );
//
//            // get group events which the event is public for all students
//            $sql3 = "SELECT DISTINCT a.EventID, a.EventDate as EventDate, a.RecordType, a.Title, a.Description, a.EventVenue, a.EventNature, a.EventLocationID, a.ClassGroupID FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID WHERE b.EventID IS NULL AND a.RecordType = '2' and a.RecordStatus='1' $conds_eventDate $conds_classGroupId";
//            $publicEventAry = $this->returnResultSet ( $sql3 );
//
//            // get all other events
//            $sql2 = "SELECT DISTINCT a.EventID, a.EventDate as EventDate, a.RecordType, a.Title, a.Description, a.EventVenue, a.EventNature, a.EventLocationID, a.ClassGroupID FROM INTRANET_EVENT AS a WHERE a.RecordStatus = '1' AND a.RecordType <> '2' $conds_eventDate $conds_classGroupId";
//            $nonMemberGroupEventAry = $this->returnResultSet ( $sql2 );
//        }

		
		// $sql = $sql1 . " union " . $sql2 . " union " . $sql3;
		// $sql .= " order by EventDate";
		// $eventAry = $this->returnResultSet($sql);
		// $numOfEvent = count($eventAry);
		//
		// $returnAry['Event'] = array();
		// for ($i=0; $i<$numOfEvent; $i++) {
		// $_eventId = $eventAry[$i]['EventID'];
		// $_eventDate = $eventAry[$i]['EventDate'];
		// $_recordType = $eventAry[$i]['RecordType'];
		// $_title = trim($eventAry[$i]['Title']);
		// $_description = trim($eventAry[$i]['Description']);
		// $_venue = trim($eventAry[$i]['EventVenue']);
		// $_nature = trim($eventAry[$i]['EventNature']);
		// $_locationId = $eventAry[$i]['EventLocationID'];
		//
		// switch ($_recordType) {
		// case 0: $_recordTypeText = 'SE'; // school event
		// break;
		// case 1: $_recordTypeText = 'AE'; // academic event
		// break;
		// case 2: $_recordTypeText = 'GE'; // group event
		// break;
		// case 3: $_recordTypeText = 'PH'; // public holiday
		// break;
		// case 4: $_recordTypeText = 'SH'; // school holiday
		// break;
		// default:
		// $_recordTypeText = '';
		// }
		//
		// if($_venue=='' && $_locationId > 0) {
		// include_once($PATH_WRT_ROOT."includes/liblocation.php");
		// $objRoom = new Room($_locationId);
		// $_venue = $objRoom->BuildingName ." > " . $objRoom->FloorName ." > " . $objRoom->RoomName;
		// }
		//
		// $_tmpAry = array();
		// $_tmpAry['EventID'] = $_eventId;
		// $_tmpAry['EventType'] = $_recordTypeText;
		// $_tmpAry['Date'] = $_eventDate;
		// $_tmpAry['Title'] = $_title;
		// $_tmpAry['Description'] = $_description;
		// $_tmpAry['Venue'] = $_venue;
		// $_tmpAry['Nature'] = $_nature;
		// $returnAry['Event'][] = $_tmpAry;
		// }
		
		if($sys_custom['eClassApp']['SFOC']){
		    $sql = "SELECT SportID,SportChiName,SportEngName FROM SFOC_SPORTS_TYPE";
		    $sportTypeResult = $this->returnArray($sql);
		    for($i = 0; $i < count( ( array )$sportTypeResult); $i ++){
		    	$sportTypeArray[$sportTypeResult[$i]["SportID"]] = $sportTypeResult[$i]["SportChiName"]." ".$sportTypeResult[$i]["SportEngName"];
		    }
		}
		
		$allEventAry = array_merge ( $groupMemberEventAry, $publicEventAry, $nonMemberGroupEventAry );
		$dateAry = array ();
		$titleAry = array ();
		foreach ( $allEventAry as $key => $row ) {
			$dateAry [$key] = $row ['EventDate'];
			$titleAry [$key] = $row ['Title'];
		}
		array_multisort ( $dateAry, SORT_ASC, $titleAry, SORT_ASC, $allEventAry );
		
		$allEventAssoAry = BuildMultiKeyAssoc ( $allEventAry, 'EventDate', $IncludedDBField = array (), $SingleValue = 0, $BuildNumericArray = 1 );
		// ksort($allEventAssoAry);
		$returnAry ['Event'] = array ();
		foreach ( ( array ) $allEventAssoAry as $_eventDate => $_dateEventAry ) {
			$_numOfDateEvent = count ( ( array ) $_dateEventAry );
			for($i = 0; $i < $_numOfDateEvent; $i ++) {
				$__eventId = $_dateEventAry [$i] ['EventID'];
				$__eventDate = $_dateEventAry [$i] ['EventDate'];
				$__recordType = $_dateEventAry [$i] ['RecordType'];
				$__locationId = $_dateEventAry [$i] ['EventLocationID'];
				if($ParLang=="en"){
                    if(trim ( $_dateEventAry [$i] ['TitleEng'] )!=""){
                        $__title = trim ( $_dateEventAry [$i] ['TitleEng'] );
                    }else{
                        $__title = trim ( $_dateEventAry [$i] ['Title'] );
                    }

                    if(trim ( $_dateEventAry [$i] ['DescriptionEng'] )!=""){
                        $__description = trim ( $_dateEventAry [$i] ['DescriptionEng'] );
                    }else{
                        $__description = trim ( $_dateEventAry [$i] ['Description'] );
                    }

                    if(trim ( $_dateEventAry [$i] ['EventVenueEng'] )!=""){
                        $__venue = trim ( $_dateEventAry [$i] ['EventVenueEng'] );
                    }else{
                        $__venue = trim ( $_dateEventAry [$i] ['EventVenue'] );
                    }

                    if(trim ( $_dateEventAry [$i] ['EventNatureEng'] )!=""){
                        $__nature = trim ( $_dateEventAry [$i] ['EventNatureEng'] );
                    }else{
                        $__nature = trim ( $_dateEventAry [$i] ['EventNature'] );
                    }
                }else{
                    $__title = trim ( $_dateEventAry [$i] ['Title'] );
                    $__description = trim ( $_dateEventAry [$i] ['Description'] );
                    $__venue = trim ( $_dateEventAry [$i] ['EventVenue'] );
                    $__nature = trim ( $_dateEventAry [$i] ['EventNature'] );
                }



				//for sfoc
                if($sys_custom['eClassApp']['SFOC']) {
                    if($ParLang=="en"&&trim($_dateEventAry [$i] ['EventAssociationEng'])!=""){
                        $__association = trim($_dateEventAry [$i] ['EventAssociationEng']);
                    }else{
                        $__association = trim($_dateEventAry [$i] ['EventAssociation']);
                    }
                    $__startTime = trim($_dateEventAry [$i] ['EventStartTime']);
                    $__endTime = trim($_dateEventAry [$i] ['EventEndTime']);
                    $__email = trim($_dateEventAry [$i] ['Email']);
                    $__contactNo = trim($_dateEventAry [$i] ['ContactNo']);
                    $__website = trim($_dateEventAry [$i] ['Website']);
                    $__sportTypeId = trim($_dateEventAry [$i] ['SportType']);
                    $__sportType = $sportTypeArray[$__sportTypeId] != "" ? $sportTypeArray[$__sportTypeId] : "";
                }

				switch ($__recordType) {
					case 0 :
						$__recordTypeText = 'SE'; // school event
						break;
					case 1 :
						$__recordTypeText = 'AE'; // academic event
						break;
					case 2 :
						$__recordTypeText = 'GE'; // group event
						break;
					case 3 :
						$__recordTypeText = 'PH'; // public holiday
						break;
					case 4 :
						$__recordTypeText = 'SH'; // school holiday
						break;
					default :
						$__recordTypeText = '';
				}
				
				if ($__venue == '' && $__locationId > 0) {
					include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
					$objRoom = new Room ( $__locationId );
					$__venue = $objRoom->BuildingName . " > " . $objRoom->FloorName . " > " . $objRoom->RoomName;
				}
				
				$__tmpAry = array ();
				$__tmpAry ['EventID'] = $__eventId;
				$__tmpAry ['EventType'] = $__recordTypeText;
				$__tmpAry ['Date'] = $__eventDate;
				$__tmpAry ['Title'] = $__title;
				$__tmpAry ['Description'] = $__description;
				$__tmpAry ['Venue'] = $__venue;
				$__tmpAry ['Nature'] = $__nature;

                if($sys_custom['eClassApp']['SFOC']) {
                    $__tmpAry ['Association'] = $__association;
                    $__tmpAry ['EventStartTime'] = $__startTime;
                    $__tmpAry ['EventEndTime'] = $__endTime;
                    $__tmpAry ['Email'] = $__email;
                    $__tmpAry ['ContactNo'] = $__contactNo;
                    $__tmpAry ['Website'] = $__website;
                    $__tmpAry ['SportType'] = $__sportType;
                }

				$returnAry ['Event'] [] = $__tmpAry;
			}
		}
		
		// ## Get Cycle Day
		$sql = "select RecordDate as Date, TextShort as CycleDay From INTRANET_CYCLE_DAYS where '" . $StartDate . "' <= RecordDate AND RecordDate <= '" . $EndDate . "'";
		// $cycleDayAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'RecordDate', $IncludedDBField=array('TextShort'), $SingleValue=1);
		// $returnAry['CycleDay'] = $cycleDayAssoAry;
		$returnAry ['CycleDay'] = $this->returnResultSet ( $sql );
		
		return $returnAry;
	}
	function WS_GetStudentTakeLeaveRecords($targetUserId) {
		global $sys_custom;

		// retrieve UserID (student + active)
		$sql = "select UserID, UserLogin from INTRANET_USER where UserID='" . $targetUserId . "' and RecordType=2";
		$rs = $this->returnResultSet ( $sql );
		$targetUserId = $rs [0] ['UserID'];
		
		// check user exists
		if (! $targetUserId) {
			return 104;
		}

		$more_field = '';
		$more_join = '';
		if( get_client_region() == 'zh_TW') {
			$more_field = ',r.LeaveTypeID,r.AttendanceType,r.SessionFrom,r.SessionTo,s.TypeName';
			$more_join = 'LEFT JOIN CARD_STUDENT_LEAVE_TYPE s ON r.LeaveTypeID=s.TypeID';
		} else if($sys_custom['StudentAttendance']['LeaveType']) {
			$more_field = ',r.LeaveTypeID,s.TypeName';
			$more_join = 'LEFT JOIN CARD_STUDENT_LEAVE_TYPE s ON r.LeaveTypeID=s.TypeID';
		}

		$sql = "Select
						r.RecordID, r.StudentID, r.StartDate, r.StartDateType, r.EndDate, r.EndDateType, r.Duration, r.Reason, r.RejectReason, r.ApprovalStatus, r.DocumentStatus, r.HomeworkDeliverMethodID,r.HomeworkDeliverMethod,r.InputDate
						$more_field
				From
						CARD_STUDENT_APPLY_LEAVE_RECORD as r
						$more_join
				Where
						r.StudentID = '" . $targetUserId . "'
						And r.IsDeleted = '0'
				";
		$recordAry = $this->returnResultSet ( $sql );
		$numOfRecord = count ( $recordAry );
		
		$returnAry ['applyLeaveRecords'] = array ();
		for($i = 0; $i < $numOfRecord; $i ++) {
			$_recordId = $recordAry [$i] ['RecordID'];
			$_studentId = $recordAry [$i] ['StudentID'];
			$_startDate = $recordAry [$i] ['StartDate'];
			$_startDateType = $recordAry [$i] ['StartDateType'];
			$_endDate = $recordAry [$i] ['EndDate'];
			$_endDateType = $recordAry [$i] ['EndDateType'];
			$_duration = $recordAry [$i] ['Duration'];
			$_reason = trim ( $recordAry [$i] ['Reason'] );
            $_rejectReason = trim ( $recordAry [$i] ['RejectReason'] );
			$_approvalStatus = $recordAry [$i] ['ApprovalStatus'];
			$_documentStatus = $recordAry [$i] ['DocumentStatus'];
			$_applicationDate = date ( 'Y-m-d', strtotime ( $recordAry [$i] ['InputDate'] ) );
			$_methodID= $recordAry [$i] ['HomeworkDeliverMethodID'];
			$_method= trim ( $recordAry [$i] ['HomeworkDeliverMethod'] );
			
			$_tmpAry = array ();
			$_tmpAry ['RecordID'] = $_recordId;
			$_tmpAry ['StudentID'] = $_studentId;
			$_tmpAry ['StartDate'] = $_startDate;
			$_tmpAry ['StartDateType'] = $_startDateType;
			$_tmpAry ['EndDate'] = $_endDate;
			$_tmpAry ['EndDateType'] = $_endDateType;
			$_tmpAry ['Duration'] = ($_duration) ? $_duration : '0';
			$_tmpAry ['Reason'] = $_reason;
            $_tmpAry ['RejectReason'] = $_rejectReason;
			$_tmpAry ['ApprovalStatus'] = $_approvalStatus;
			$_tmpAry ['DocumentStatus'] = $_documentStatus;
			$_tmpAry ['MethodID'] = $_methodID;
			$_tmpAry ['Method'] = $_method;
			$_tmpAry ['ApplicationDate'] = $_applicationDate;

			if( get_client_region() == 'zh_TW') {
				$_tmpAry['LeaveTypeID'] = $recordAry[$i]['LeaveTypeID'];
				$_tmpAry['LeaveTypeName'] = $recordAry[$i]['TypeName'];
				$_tmpAry['AttendanceType'] = $recordAry[$i]['AttendanceType'];
				$_tmpAry['SessionFrom'] = $recordAry[$i]['SessionFrom'];
				$_tmpAry['SessionTo'] = $recordAry[$i]['SessionTo'];
			} else if($sys_custom['StudentAttendance']['LeaveType']) {
				$_tmpAry['LeaveTypeID'] = $recordAry[$i]['LeaveTypeID'];
				$_tmpAry['LeaveTypeName'] = $recordAry[$i]['TypeName'];
			}

			$returnAry ['applyLeaveRecords'] [] = $_tmpAry;
		}
		
		$returnAry ['applyLeaveSettings'] = $this->WS_GetStudentTakeLeaveSettings ();
		
		// HKUFlu
		$hku_returnArray = $this->WS_GetHKUFluAgreementRecord ( $targetUserId );
		$returnAry ['rows_agreement'] = $hku_returnArray ['rows_agreement'];
		
		return $returnAry;
	}
	function WS_GetStudentTakeLeaveSettings() {
		global $sys_custom, $plugin;
		
		$sql = "Select SettingKey, SettingValue From CARD_STUDENT_APPLY_LEAVE_SETTING";
		$settingsAry = $this->returnResultSet ( $sql );
		$numOfSettings = count ( $settingsAry );
		
		$returnAry = array ();
		
		// default value
		$returnAry ['isRequireDocument'] = '0';
		$returnAry ['documentSubmitWithinDay'] = '0';
		$returnAry ['documentSubmitRemarks'] = '';
		$returnAry ['requiredPwd'] = ($sys_custom ['enotice_sign_with_password']) ? '1' : '0';
		$returnAry ['enableApplyLeaveCutOffTiming'] = '0';
		$returnAry ['applyLeaveCutOffTimingAM'] = '';
		$returnAry ['applyLeaveCutOffTimingPM'] = '';
		$returnAry ['daysBeforeApplyLeave'] = '-1';
		$returnAry ['maxApplyLeaveDay'] = '-1';
	    $returnAry ['HideAMPM'] = ($sys_custom['eClassApp']['ApplyLeave']['HideAMPM']) ? '1' : '0';
		$returnAry ['enableApplyLeaveHomeworkPassSetting'] = '0';
		$returnAry ['EnableLeaveType'] = ($sys_custom['StudentAttendance']['LeaveType']) ? '1' : '0';

		// P98570
		$returnAry ['hideAddAttachmentButton'] = $sys_custom ['eClassApp'] ['applyLeaveHideAddAttachmentButton'] ? '1' : '0';
		
		
		for($i = 0; $i < $numOfSettings; $i ++) {
			$_settingKey = $settingsAry [$i] ['SettingKey'];
			$_settingValue = $settingsAry [$i] ['SettingValue'];
			
			$returnAry [$_settingKey] = $_settingValue;
		}
        
		$returnAry ['daysBeforeApplyLeave'] = ($returnAry ['daysBeforeApplyLeave'] !="") ? $returnAry ['daysBeforeApplyLeave']  : '-1';
		$returnAry ['maxApplyLeaveDay'] = ($returnAry ['maxApplyLeaveDay']!="") ? $returnAry ['maxApplyLeaveDay'] : '-1';
		
		//get homework deliver method
		$sql = "Select * From APPLY_LEAVE_HOMEWORK_PASS_METHOD_SETTING where IsDelete = 0";
		$methodAry = $this->returnResultSet ( $sql );
		$numOfRecord = count ( $methodAry );
		
		for($i = 0; $i < $numOfRecord; $i ++) {
			$_methodId = $methodAry [$i] ['MethodID'];
			$_method = $methodAry [$i] ['Method'];
			$_methodEng = $methodAry [$i] ['MethodEng'];

			$_tmpAry = array ();
			$_tmpAry ['MethodID'] = $_methodId;
			$_tmpAry ['Method'] = $_method;
			$_tmpAry ['MethodEng'] = $_methodEng;

			$returnAry ['applyLeaveHomeworkMethod'][] = $_tmpAry;
		}

		if($sys_custom['StudentAttendance']['LeaveType'] || get_client_region() == 'zh_TW') {
			$sql ="SELECT TypeID,TypeName FROM CARD_STUDENT_LEAVE_TYPE ORDER BY TypeSymbol,TypeName";
			$rs = $this->returnArray($sql,2);
			$leave_type = array();
			foreach($rs as $temp) {
				$leave_type[] = array("LeaveTypeID"=>$temp['TypeID'], "LeaveTypeName"=>$temp['TypeName']);
			}
			$returnAry['leaveType'] = $leave_type;
		}

        return $returnAry;

	}
	function WS_SubmitApplyLeaveRecord($RecordID, $ParentUserID, $StudentUserID, $StartDate, $StartDateType, $EndDate, $EndDateType, $Duration, $Reason, $ImageData, $FileIDAry = '', $methodID = -1, $method = '', $LeaveTypeID='', $AttendanceType='', $SessionFrom='', $SessionTo='') {
		global $PATH_WRT_ROOT, $Lang, $eclassAppConfig, $sys_custom;
		
		include_once ($PATH_WRT_ROOT . 'includes/libapplyleave.php');
		$lapplyleave = new libapplyleave ();
		
		// retrieve UserID (parent + active)
		$sql = "select UserID, UserLogin from INTRANET_USER where UserID='" . $ParentUserID . "' and RecordType=3";
		$rs = $this->returnResultSet ( $sql );
		$ParentUserID = $rs [0] ['UserID'];
		
		// retrieve UserID (student + active)
		$sql = "select UserID, UserLogin from INTRANET_USER where UserID='" . $StudentUserID . "' and RecordType=2";
		$rs = $this->returnResultSet ( $sql );
		$StudentUserID = $rs [0] ['UserID'];
		
		// check user exists
		if (! $ParentUserID || ! $StudentUserID) {
			return 104;
		}
		
		$successAry = array ();
		
		$returnRecordId = '';
		if ($RecordID > 0) {
			$sql = "Select DocumentStatus From CARD_STUDENT_APPLY_LEAVE_RECORD Where RecordID = '" . $RecordID . "'";
			$recordAry = $this->returnResultSet ( $sql );
			$curDocumentStatus = $recordAry [0] ['DocumentStatus'];
			
			if (($curDocumentStatus == 0 || $curDocumentStatus == 3) && $ImageData) {
				// originally not yet submit or rejected document and now have document => change to pending status
				$targetDocumentStatus = 1;
			} else if (empty ( $ImageData )) {
				// no document => no need to approve document
				$targetDocumentStatus = 0;
			} else {
				// keep original document status
				$targetDocumentStatus = $curDocumentStatus;
			}

			$update_value = '';
			if(get_client_region() == 'zh_TW') {
				/*if($AttendanceType != 0) {
					$EndDate = $StartDate;
					$EndDateType = $StartDateType;
				}

				if($AttendanceType == 3) {
					$SessionFrom = '';
					$SessionTo = '';
				}*/
				//$update_value .= " ,AttendanceType='".$this->Get_Safe_Sql_Query($AttendanceType)."'";
				$update_value .= " ,LeaveTypeID='".$this->Get_Safe_Sql_Query($LeaveTypeID)."'";
				//$update_value .= " ,SessionFrom='" . $this->Get_Safe_Sql_Query($SessionFrom) . "'";
				//$update_value .= " ,SessionTo='" . $this->Get_Safe_Sql_Query($SessionTo) . "'";
			} else {
				if($sys_custom['StudentAttendance']['LeaveType']) {
					$update_value .= " ,LeaveTypeID='".$this->Get_Safe_Sql_Query($LeaveTypeID)."'";
				}
			}

			$sql = "Update 
							CARD_STUDENT_APPLY_LEAVE_RECORD
					Set
							StartDate = '" . $StartDate . "',
							StartDateType = '" . $StartDateType . "',
							EndDate = '" . $EndDate . "',
							EndDateType = '" . $EndDateType . "',
							Duration = '" . $this->Get_Safe_Sql_Query ( $Duration ) . "',
							Reason = '" . $this->Get_Safe_Sql_Query ( $Reason ) . "',
							DocumentStatus = '" . $targetDocumentStatus . "',
							DocumentStatusChangedBy = '" . $ParentUserID . "',
							HomeworkDeliverMethodID = '" . $methodID . "',
							HomeworkDeliverMethod = '" . $this->Get_Safe_Sql_Query ( $method ) . "',
							ModifiedDate = now(),
							ModifiedBy = '" . $ParentUserID . "'
							$update_value
					Where
							RecordID = '" . $RecordID . "'
					";
			$successAry ['updateLeaveRecord'] = $this->db_db_query ( $sql );
			if ($successAry ['updateLeaveRecord']) {
				$returnRecordId = $RecordID;
			}
		} else {
			// ## check double submission
			$cond = '';
			if(get_client_region() == 'zh_TW') {
				$cond .= " AND AttendanceType='".$this->Get_Safe_Sql_Query($AttendanceType)."'";
				$cond .= " AND LeaveTypeID='".$this->Get_Safe_Sql_Query($LeaveTypeID)."'";
				if($AttendanceType == 3) {
					$cond .= " AND SessionFrom='" . $this->Get_Safe_Sql_Query($SessionFrom) . "'";
					$cond .= " AND SessionTo='" . $this->Get_Safe_Sql_Query($SessionTo) . "'";
				}
			} else {
				if($sys_custom['StudentAttendance']['LeaveType']) {
					$cond .= " AND LeaveTypeID='".$this->Get_Safe_Sql_Query($LeaveTypeID)."'";
				}
			}

			$sql = "Select 
							RecordID 
					From 
							CARD_STUDENT_APPLY_LEAVE_RECORD 
					Where 
							StudentID = '" . $StudentUserID . "'
							And StartDate = '" . $StartDate . "'
							And StartDateType = '" . $StartDateType . "'
							And EndDate = '" . $EndDate . "'
							And EndDateType = '" . $EndDateType . "'
							And Duration = '" . $this->Get_Safe_Sql_Query ( $Duration ) . "'
							And Reason = '" . $this->Get_Safe_Sql_Query ( $Reason ) . "'
							And HomeworkDeliverMethodID = '" . $methodID . "'
							And HomeworkDeliverMethod = '" . $this->Get_Safe_Sql_Query ( $method ) . "'
							And InputBy = '" . $ParentUserID . "'
							$cond
					";
			$recordAry = $this->returnResultSet ( $sql );
			$returnRecordId = $recordAry [0] ['RecordID'];
			
			if ($returnRecordId == '') {
				// insert new record if not existing
				$more_field = '';
				$more_value = '';
				if(get_client_region() == 'zh_TW') {
					$more_field = ',AttendanceType,LeaveTypeID';
					$more_value = ",'".$this->Get_Safe_Sql_Query($AttendanceType)."','".$this->Get_Safe_Sql_Query($LeaveTypeID)."'";
					if($AttendanceType == 3) {
						$more_field .= ',SessionFrom,SessionTo';
						$more_value .= ",'".$this->Get_Safe_Sql_Query($SessionFrom)."','".$this->Get_Safe_Sql_Query($SessionTo)."'";
					}
				} else {
					if ($sys_custom['StudentAttendance']['LeaveType']) {
						$more_field = ',LeaveTypeID';
						$more_value = ",'".$this->Get_Safe_Sql_Query($LeaveTypeID)."'";
					}
				}
				$documentStatus = ($ImageData) ? 1 : 0;
				$sql = "Insert Into CARD_STUDENT_APPLY_LEAVE_RECORD 
							(StudentID, StartDate, StartDateType, EndDate, EndDateType, Duration, Reason, 
								ApprovalStatus, DocumentStatus, DocumentStatusChangedBy,HomeworkDeliverMethodID,HomeworkDeliverMethod, InputDate, InputBy, ModifiedDate, ModifiedBy $more_field)
						Values
							('" . $StudentUserID . "', '" . $StartDate . "', '" . $StartDateType . "', '" . $EndDate . "', '" . $EndDateType . "', '" . $this->Get_Safe_Sql_Query ( $Duration ) . "', '" . $this->Get_Safe_Sql_Query ( $Reason ) . "', 
								0, '" . $documentStatus . "', '" . $ParentUserID . "', '" . $methodID . "','" . $this->Get_Safe_Sql_Query ( $method ) . "', now(), '" . $ParentUserID . "', now(), '" . $ParentUserID . "' $more_value)
						";
				$successAry ['insertLeaveRecord'] = $this->db_db_query ( $sql );
				if ($successAry ['insertLeaveRecord']) {
					$returnRecordId = $this->db_insert_id ();
					
					// ## send notification to parent
					include_once ($PATH_WRT_ROOT . 'includes/eClassApp/libeClassApp.php');
					include_once ($PATH_WRT_ROOT . 'includes/libuser.php');
					$libeClassApp = new libeClassApp ();
					$objUser = new libuser ( $StudentUserID );
					
					$messageInfoAry = array ();
					$messageInfoAry [0] ['relatedUserIdAssoAry'] [$ParentUserID] = array (
							$StudentUserID 
					);
					
					$messageTitle = $Lang ['StudentAttendance'] ['ApplyLeaveAry'] ['NotificationAry'] ['ReceiveApplicationTitle'];
					$messageContent = $Lang ['StudentAttendance'] ['ApplyLeaveAry'] ['NotificationAry'] ['ReceiveApplicationContent'];
					$messageContent = str_replace ( '<!--studentNameEnWithClass-->', $objUser->EnglishName . ' (' . $objUser->ClassName . '-' . $objUser->ClassNumber . ')', $messageContent );
					$messageContent = str_replace ( '<!--studentNameChWithClass-->', $objUser->ChineseName . ' (' . $objUser->ClassName . '-' . $objUser->ClassNumber . ')', $messageContent );
					$messageContent = str_replace ( '<!--datetime-->', date ( 'Y-m-d H:i' ), $messageContent );
					$messageContent = str_replace ( '<!--startDateTime-->', date ( 'Y-m-d', strtotime ( $StartDate ) ) . ' (' . $StartDateType . ')', $messageContent );
					$messageContent = str_replace ( '<!--endDateTime-->', date ( 'Y-m-d', strtotime ( $EndDate ) ) . ' (' . $EndDateType . ')', $messageContent );
					$successAry ['sendPushMessage'] = $libeClassApp->sendPushMessage ( $messageInfoAry, $messageTitle, $messageContent, $isPublic = '', $recordStatus = 0 );
					
					// send push message to class teacher
					$settingAssoAry = $lapplyleave->getSettingsAry ();
					if ($settingAssoAry ['sendPushMessageToClassTeacher'] && $objUser->ClassName != '') {
						$sql = "Select
										yct.UserID
								From
										YEAR_CLASS_USER as ycu
										Inner Join YEAR_CLASS_TEACHER as yct ON (ycu.YearClassID = yct.YearClassID)
										Inner Join YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID)
										Inner Join INTRANET_USER as teacher ON (yct.UserID = teacher.UserID)
								Where
										ycu.UserID = '" . $StudentUserID . "'
										And teacher.RecordStatus = 1 
										And yc.AcademicYearID = '" . Get_Current_Academic_Year_ID () . "'
								Group By
										yct.UserID
								";
						$teacherIdAry = $libeClassApp->returnVector ( $sql );
						$numOfTeacher = count ( $teacherIdAry );
						
						$teacherIndividualMessageInfoAry = array ();
						$teacherAssoAry = array ();
						for($i = 0; $i < $numOfTeacher; $i ++) {
							$_teacherId = $teacherIdAry [$i];
							$_targetTeacherId = $libeClassApp->getDemoSiteUserId ( $_teacherId );
							
							// link the message to be related to oneself
							$teacherAssoAry [$_teacherId] = array (
									$_targetTeacherId 
							);
						}
						
						$messageTitle = $Lang ['StudentAttendance'] ['ApplyLeaveAry'] ['NotificationAry'] ['StudentLeaveApplicationTitle'];
						$messageContent = $Lang ['StudentAttendance'] ['ApplyLeaveAry'] ['NotificationAry'] ['StudentLeaveApplicationContent'];
						$messageContent = str_replace ( '<!--studentNameEnWithClass-->', $objUser->EnglishName . ' (' . $objUser->ClassName . '-' . $objUser->ClassNumber . ')', $messageContent );
						$messageContent = str_replace ( '<!--studentNameChWithClass-->', $objUser->ChineseName . ' (' . $objUser->ClassName . '-' . $objUser->ClassNumber . ')', $messageContent );
						$messageContent = str_replace ( '<!--startDateTime-->', date ( 'Y-m-d', strtotime ( $StartDate ) ) . ' (' . $StartDateType . ')', $messageContent );
						$messageContent = str_replace ( '<!--endDateTime-->', date ( 'Y-m-d', strtotime ( $EndDate ) ) . ' (' . $EndDateType . ')', $messageContent );
						$messageContent = str_replace ( '<!--reason-->', $Reason, $messageContent );
						$messageContent = str_replace ( '<!--numOfDays-->', $Duration, $messageContent );
						
						$teacherIndividualMessageInfoAry [0] ['relatedUserIdAssoAry'] = $teacherAssoAry;
						$successAry ['sendPushMessageClassTeacher'] = $libeClassApp->sendPushMessage ( $teacherIndividualMessageInfoAry, $messageTitle, $messageContent, 'N', $recordStatus = 0, $eclassAppConfig ['appType'] ['Teacher'] );
					}
				}
			}
		}
		
		$fileUpdateSuccess = '0';
		if ($returnRecordId > 0 && empty ( $ImageData )) {
			$successAry ['deleteAttachment'] = $lapplyleave->deleteLeaveRecordAttachement ( $returnRecordId );
		} else if ($returnRecordId > 0 && $ImageData) {
			include_once ($PATH_WRT_ROOT . 'includes/libfilesystem.php');
			$lfs = new libfilesystem ();
			
			if (! is_array ( $ImageData )) {
				$ImageData = array (
						$ImageData 
				);
			}
			$numOfImage = count ( $ImageData );
			
			// delete file
			if (empty ( $FileIDAry )) {
				// old API => delete all and then insert photo again
				$successAry ['deleteAttachment'] = $lapplyleave->deleteLeaveRecordAttachement ( $returnRecordId );
			} else {
				// new API => delete user removed photo only
				$curFileIdAry = Get_Array_By_Key ( $lapplyleave->getDocumentAry ( $returnRecordId ), 'FileID' );
				$deletedFileIdAry = array ();
				if ($FileIDAry != '' && is_array ( $FileIDAry )) {
					$deletedFileIdAry = array_values ( array_diff ( $curFileIdAry, $FileIDAry ) );
				}
				if (count ( $deletedFileIdAry ) > 0) {
					$successAry ['deleteRemovedAttachment'] = $lapplyleave->deleteLeaveRecordAttachement ( $returnRecordId, $deletedFileIdAry );
				}
			}
			
			for($i = 0; $i < $numOfImage; $i ++) {
				$_fileId = $FileIDAry [$i];
				$_imageData = $ImageData [$i];
				
				if ($_fileId == '' || $_fileId < 0) {
					$sql = "Insert Into CARD_STUDENT_APPLY_LEAVE_FILE (LeaveRecordID, InputDate, InputBy, ModifiedDate, ModifiedBy)
							Values ('" . $returnRecordId . "', now(), '" . $ParentUserID . "', now(), '" . $ParentUserID . "')";
					$successAry ['insertFileRecord'] = $this->db_db_query ( $sql );
					if ($successAry ['insertFileRecord']) {
						$_fileId = $this->db_insert_id ();
					}
				}
				
				if ($_fileId > 0) {
					$_folderDivision = $lapplyleave->returnFolderDivisionNum ( $_fileId );
					
					// hardcode file name now
					$_imageFileName = 'applyLeave_' . $RecordID . '_' . $_fileId . '_' . date ( 'Ymd_His' ) . '.jpg';
					
					$_fileExt = get_file_ext ( $_imageFileName );
					$_filePath = $lapplyleave->getDocumentFilePathBase () . '/eClassApp/applyLeave/' . $_folderDivision . '/' . $_fileId . $_fileExt;
					
					$_tmpFilePath = $lapplyleave->getDocumentFilePathBase () . '/eClassApp/applyLeave/' . $_folderDivision . '/';
					if (! file_exists ( $_tmpFilePath )) {
						$successAry ['createRootFolder'] [$_fileId] = $lfs->folder_new ( $_tmpFilePath );
					}
					
					$_imageBinary = base64_decode ( $_imageData );
					$successAry ['uploadFile'] = $lfs->file_write ( $_imageBinary, $_filePath );
					if ($successAry ['uploadFile']) {
						$_dbFilePath = str_replace ( $lapplyleave->getDocumentFilePathBase (), '', $_filePath );
						$sql = "Update 
										CARD_STUDENT_APPLY_LEAVE_FILE 
								Set 
										FilePath = '" . $this->Get_Safe_Sql_Query ( $_dbFilePath ) . "',
										FileName = '" . $this->Get_Safe_Sql_Query ( $_imageFileName ) . "',
										ModifiedDate = now(),
										ModifiedBy = '" . $ParentUserID . "'
								Where
										FileID = '" . $_fileId . "'
								";
						$successAry ['updateFileRecord'] [$_fileId] = $this->db_db_query ( $sql );
					}
				}
			}
			
			if ($numOfImage > 0 && ! in_array ( false, ( array ) $successAry ['updateFileRecord'] )) {
				$fileUpdateSuccess = '1';
			}
		}
		
		$returnAry = array ();
		$returnAry ['RecordID'] = $returnRecordId;
		$returnAry ['FileUpdateSuccess'] = $fileUpdateSuccess;
		
		return $returnAry;
	}

    function WS_CancelLeaveRecord($recordId) {
        global $PATH_WRT_ROOT;
        include_once ($PATH_WRT_ROOT . 'includes/libapplyleave.php');

        $lapplyleave = new libapplyleave ();

        $recordIdArray = array();
        $recordIdArray[] = $recordId;

        $result = $lapplyleave->updateApplyLeaveStatus($recordIdArray, 'ApprovalStatus', 3);

        $returnAry = array ();
        $returnAry ['CancelSuccess'] = $result;

        return $returnAry;
    }

	function WS_GetLeaveRecordImages($parRecordId) {
		global $PATH_WRT_ROOT;
		include_once ($PATH_WRT_ROOT . 'includes/libapplyleave.php');
		
		$lfs = new libfilesystem ();
		$lapplyleave = new libapplyleave ();
		
		$docAry = $lapplyleave->getDocumentAry ( $parRecordId );
		$numOfDoc = count ( $docAry );
		
		$returnAry = array ();
		$returnAry ['ImageData'] = array ();
		$returnAry ['FileID'] = array ();
		for($i = 0; $i < $numOfDoc; $i ++) {
			$_fileId = $docAry [$i] ['FileID'];
			$_docPath = $docAry [$i] ['FilePath'];
			
			$returnAry ['FileID'] [] = $_fileId;
			
			$_docFullPath = $lapplyleave->getDocumentFilePathBase () . $_docPath;
			$returnAry ['ImageData'] [] = base64_encode ( $lfs->file_read ( $_docFullPath ) );
		}
		
		return $returnAry;
	}
	function isKIS($userLogin) {
		global $DebugMode, $plugin;
		
		$isKIS = false;
		if ((strstr ( $userLogin, "kis_" ) && $DebugMode) || $plugin ["platform"] == "KIS") {
			$isKIS = true;
		}
		
		return $isKIS;
	}
	function WS_GetSchooliCal($StartDate, $EndDate, $APIKey) {
		global $PATH_WRT_ROOT, $plugin;
		
		include_once ('icalendar.php');
		$icalendar = new icalendar ();
		
		$eclassApiAuth = new libeclassapiauth ();
		$apiKeyAry = $eclassApiAuth->GetAPIKeyInfoOfCompany ( 'AVRIO' );
		$apiKey = $apiKeyAry ['APIKey'];
		$apiStatus = $apiKeyAry ['Status'];
		
		$curTimestamp = time ();
		$pastHourTimestamp = $curTimestamp - 3600;
		$nextHourTimestamp = $curTimestamp + 3600;
		
		$curHourCode = $apiKey . '_' . date ( 'YmdH', $curTimestamp );
		$curHourCodeMd5 = md5 ( $curHourCode );
		$pastHourCode = $apiKey . '_' . date ( 'YmdH', $pastHourTimestamp );
		$pastHourCodeMd5 = md5 ( $pastHourCode );
		$nextHourCode = $apiKey . '_' . date ( 'YmdH', $nextHourTimestamp );
		$nextHourCodeMd5 = md5 ( $nextHourCode );
		
		$canAccess = false;
		if ($apiStatus == 'A' && ($APIKey == $curHourCodeMd5 || $APIKey == $pastHourCodeMd5 || $APIKey == $nextHourCodeMd5)) {
			$canAccess = true;
		}
		
		$startDateTs = strtotime ( $StartDate );
		$endDateTs = strtotime ( $EndDate );
		$yearTs = 60 * 60 * 24 * 365;
		$overRange = false;
		
		if (($endDateTs - $startDateTs) > $yearTs) {
			$overRange = true;
		}
		
		if (! $plugin ['eClassApp'] || ! $canAccess) {
			return '402';
		} else if ($overRange) {
			return '102';
		} else {
			$calType = 1; // school calendar
			              
			// 0:School Events; 1:Academic Events; 3:Public Holidays; 4:School Holidays;
			              // excluded "2:Group Events;" becuase this may contains sensitive events
			$calIdAry = array (
					0,
					1,
					3,
					4 
			);
			
			$iCalData = $icalendar->db_to_icalStmt ( $calIdAry, $calType, $StartDate, $EndDate );
			
			$returnAry = array ();
			$returnAry ['iCalData'] = $iCalData;
			return $returnAry;
		}
	}
	function WS_GetCurrentServerDateTime() {
		$returnAry = array ();
		$returnAry ['curDateTime'] = date ( "Y-m-d H:i:s" );
		return $returnAry;
	}
	function WS_getMultipleRequestResult($parJsonRequestAssoAry) {
		global $PATH_WRT_ROOT;
		include_once ($PATH_WRT_ROOT . 'includes/libmethod.php');
		$lmethod = new LibMethod ();
		
		$returnAry = array ();
		foreach ( ( array ) $parJsonRequestAssoAry as $_requestName => $_requestJsonAry ) {
			$_requestNameAry = explode ( '###', $_requestName );
			$_targetMethod = $_requestNameAry [0]; // $_requestNameAry[1] is other remarks ()
			
			$returnAry [$_requestName] = $lmethod->callMethod ( $_targetMethod, $_requestJsonAry );
		}
		
		return $returnAry;
	}

	function WS_GeteCircularForApp($parUserId) {
		global $PATH_WRT_ROOT, $special_feature;
		
		$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType=1 And UserID = '" . $parUserId . "' ";
		$result = $this->returnVector ( $sql );
		
		if ($result [0] == "") {
			return 104;
		} else {
			$ThisUserID = $result [0];
		}
		
		include_once ($PATH_WRT_ROOT . 'includes/libcircular.php');
		$lcircular = new libcircular ();
		
		$sql = "SELECT 
					a.CircularID, 
					a.CircularNumber, 
					a.Title,
					a.DateStart, 
					a.DateEnd, 
					IF(c.RecordStatus=2,'Y','N') as SignStatus, 
					UNIX_TIMESTAMP(a.DateStart) as DateStartTs
				FROM 
					INTRANET_CIRCULAR as a
					Inner JOIN INTRANET_CIRCULAR_REPLY as c ON (a.CircularID = c.CircularID AND c.UserID='" . $ThisUserID . "')
				WHERE 
					a.RecordStatus = 1
					AND unix_timestamp(a.DateStart) <= unix_timestamp(now())
					AND DATE(a.DateEnd) >= DATE('" . getStartDateOfAcademicYear ( Get_Current_Academic_Year_ID () ) . "')
				ORDER BY a.DateStart DESC, a.CircularID DESC";
		$result = $lcircular->returnArray ( $sql );
		$numOfCircular = count ( $result );
		
		$ReturnAry ['eCircular'] = array ();
		if ($numOfCircular == 0) {
			// return empty array if no records found
			// $ReturnAry = array();
		} else {
			$allowLateSigning = ($lcircular->isLateSignAllow) ? 'Y' : 'N';
			
			// $school_url = split("\n",get_file_content("$intranet_root/file/email_website.txt"));
			// $school_domain = $school_url[0];
			$school_domain = curPageURL ( $withQueryString = false, $withPageSuffix = false );
			
			if (substr ( $school_domain, - 1 ) == "/")
				$school_domain = substr ( $school_domain, 0, - 1 );
			
			for($i = 0; $i < $numOfCircular; $i ++) {
				$_circularId = $result [$i] ['CircularID'];
				$_circularNumber = $result [$i] ['CircularNumber'];
				$_title = trim_control_character ( $result [$i] ['Title'] );
				$_dateStart = $result [$i] ['DateStart'];
				$_dateEnd = $result [$i] ['DateEnd'];
				$_signStatus = $result [$i] ['SignStatus'];
				$_dateStartTs = $result [$i] ['DateStartTs'];
				
				// prepare token for verification
				$_delimiter = "###";
				$_keyStr = $special_feature ['ParentApp_PrivateKey'] . $_delimiter . $_circularId . $_delimiter . $ThisUserID;
				$_token = md5 ( $_keyStr );
				$_signURL = $school_domain . '/api/ecircular_sign_eclassApp.php?token=' . $_token . '&CircularID=' . $_circularId . '&CurUserID=' . $ThisUserID;
				
				$ReturnAry ['eCircular'] [] = array (
						"CircularID" => $_circularId,
						"CircularNumber" => $_circularNumber,
						"Title" => $_title,
						"DateStart" => $_dateStart,
						"DateEnd" => $_dateEnd,
						"SignStatus" => $_signStatus,
						"SignURL" => $_signURL,
						"AllowLateSigning" => $allowLateSigning 
				);
			}
		}
		
		return $ReturnAry;
	}

	function WS_GeteNoticeSForApp($parUserId) {
		global $PATH_WRT_ROOT, $special_feature, $sys_custom;
		
		$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 1 AND UserID = '".$parUserId."' ";
		$result = $this->returnVector($sql);
		
		if ($result[0] == "") {
			return 104;
		}
		else {
			$ThisUserID = $result[0];
		}
		
		include_once($PATH_WRT_ROOT . 'includes/lib.php');
		include_once($PATH_WRT_ROOT . 'includes/libnotice.php');
		include_once($PATH_WRT_ROOT . "includes/user_right_target.php");
		include_once($PATH_WRT_ROOT . "includes/libgeneralsettings.php");
		include_once($PATH_WRT_ROOT . "includes/eClassApp/libeClassApp.php");
		
		$lnotice = new libnotice();
		$lgeneralsettings = new libgeneralsettings();
		$leClassApp = new libeClassApp();
		
		$settings_ary = $lgeneralsettings->Get_General_Setting("eNotice");
		if (!empty($settings_ary)) {
			foreach($settings_ary as $key => $data) {
				$lnotice->$key = $data;

                // [2020-0604-1821-16170] if no valid payment notice setting > apply school notice setting
                $payment_key = 'payment_'.$key;
                if(!isset($settings_ary[$payment_key]) && property_exists($lnotice, $payment_key)) {
                    $this->$payment_key = $data;
                }
			}
		} else {
			$lnotice->teacherCanSeeAllNotice = false;
		}
		
		$UserRightTarget = new user_right_target();
		$_SESSION ["SSV_USER_ACCESS"] = $UserRightTarget->Load_User_Right($parUserId);
        $isAccessGroupMember = $lnotice->isNoticeAccessGroupMember($parUserId);

        // [2020-0604-1821-16170] check access right for different notice type
        $isSchoolNoticeAccessGroupMember = $lnotice->isSchoolNoticeAccessGroupMember($parUserId);
        $isPaymentNoticeAccessGroupMember = $lnotice->isPaymentNoticeAccessGroupMember($parUserId);
        $isSchoolNoticCanAccess = $isSchoolNoticeAccessGroupMember && ($lnotice->accessGroupCanSeeAllNotice || $lnotice->teacherCanSeeAllNotice);
        $isPaymentNoticeCanAccess = $isPaymentNoticeAccessGroupMember && ($lnotice->payment_accessGroupCanSeeAllNotice || $lnotice->payment_teacherCanSeeAllNotice);

        // [2020-0310-1051-05235]
        $applySpecialNoticeChecking = $sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'];
        $specialNoticeField = $applySpecialNoticeChecking ? ' ,SpecialNotice ' : '';
		
		if($sys_custom['eClassTeacherApp']['showNoticeBeforeStart']) {
		     $condStart = "";	
		} else {
			 $condStart = "unix_timestamp(a.DateStart) <= unix_timestamp(now()) AND ";
		}

        // [2020-0604-1821-16170] check if can access all notice
        //if ($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'] || ($isAccessGroupMember && ($lnotice->accessGroupCanSeeAllNotice || $lnotice->teacherCanSeeAllNotice)))
		if ($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'] || $isSchoolNoticCanAccess || $isPaymentNoticeCanAccess)
		{
		    // [2018-0507-1023-45110] Get current year class students first
		    $sql = "SELECT
                            DISTINCT(ycu.UserID) 
                    FROM
                            YEAR_CLASS_USER as ycu 
                            INNER JOIN YEAR_CLASS yc on (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."')";
		    $student_list = $lnotice->returnVector($sql);
		    $student_list = implode("', '", $student_list);

            // [2020-0604-1821-16170] notice module filtering
		    $condModule = ' 1 ';
		    if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'] || ($isSchoolNoticCanAccess && $isPaymentNoticeCanAccess)) {
                $condModule = " a.Module IN ('DISCIPLINE', 'Payment', 'STUDENTATTENDANCE') OR a.Module IS NULL ";
            } else if ($isSchoolNoticCanAccess) {
                $condModule = " a.Module IN ('DISCIPLINE', 'STUDENTATTENDANCE') OR a.Module IS NULL ";
            } else if ($isPaymentNoticeCanAccess) {
                $condModule = " a.Module IN ('Payment') ";
            }
		    
			$sql = "SELECT
    				    a.NoticeID,
    					a.NoticeNumber,
    					a.Title,
    					a.DateStart,
    					a.DateEnd,
    				    COUNT(b.StudentID) AS AllStudents,
    				    COUNT(case when b.RecordStatus = 2 then 1 else null end) SignedStudents,
    				    0 as accessRightOrder
    				    $specialNoticeField
    				    ,Module
    				FROM
    					INTRANET_NOTICE as a
    					INNER JOIN INTRANET_NOTICE_REPLY as b ON a.NoticeID = b.NoticeID
    				WHERE
    					$condStart
    					unix_timestamp(DATE(a.DateEnd)) >= unix_timestamp(DATE('".getStartDateOfAcademicYear(Get_Current_Academic_Year_ID())."'))
    					AND ($condModule) AND a.RecordStatus=1 AND a.IsDeleted=0
    					AND b.StudentID IN ('".$student_list."')
    				GROUP BY a.NoticeID";
    		$result = $lnotice->returnArray($sql);
		}
		else
		{
		    $result3 = array();

            // [2020-0604-1821-16170] check if can access all notice
            //if ($lnotice->teacherCanSeeAllNotice)
			if ($lnotice->teacherCanSeeAllNotice || $lnotice->payment_teacherCanSeeAllNotice)
			{
			    // [2018-0507-1023-45110] Get current year class students first
				$sql = "SELECT 
                                DISTINCT(ycu.UserID) 
                        FROM
                                YEAR_CLASS_USER as ycu 
                                INNER JOIN YEAR_CLASS yc on (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."')";
				$student_list = $lnotice->returnVector($sql);
				$student_list = implode("', '", $student_list);

                // [2020-0604-1821-16170] notice module filtering
                $condModule = ' 1 ';
                if($lnotice->teacherCanSeeAllNotice && $lnotice->payment_teacherCanSeeAllNotice) {
                    $condModule = " a.Module IN ('DISCIPLINE', 'Payment', 'STUDENTATTENDANCE') OR a.Module IS NULL ";
                } else if ($lnotice->teacherCanSeeAllNotice) {
                    $condModule = " a.Module IN ('DISCIPLINE', 'STUDENTATTENDANCE') OR a.Module IS NULL ";
                } else if ($lnotice->payment_teacherCanSeeAllNotice) {
                    $condModule = " a.Module IN ('Payment') ";
                }

				$sql3 = "SELECT
	        				    a.NoticeID,
	        					a.NoticeNumber,
	        					a.Title,
	        					a.DateStart,
	        					a.DateEnd,
	        				    -1 AS AllStudents,
	        				    -1 AS SignedStudents,
	        				     3 as accessRightOrder
    				            $specialNoticeField
    				            ,Module
	        				FROM
	        					INTRANET_NOTICE as a
	        					INNER JOIN INTRANET_NOTICE_REPLY as b ON a.NoticeID = b.NoticeID
	    					WHERE
	        					$condStart
	        					unix_timestamp(DATE(a.DateEnd)) >= unix_timestamp(DATE('" . getStartDateOfAcademicYear ( Get_Current_Academic_Year_ID () ) . "'))
	        					AND ($condModule) AND a.RecordStatus=1 AND a.IsDeleted=0
						        AND b.StudentID IN ('".$student_list."')
	        				GROUP BY a.NoticeID 
	        				ORDER BY DateStart DESC, NoticeID DESC";

	        	$result3 = $lnotice->returnArray($sql3);
			}
		
			$result1 = array();		
		    // [2018-0507-1023-45110] Get current year class students first - for class teacher only
		    $sql = "SELECT 
                            DISTINCT(ycu.UserID) 
                    FROM 
                            YEAR_CLASS_USER as ycu
                            INNER JOIN YEAR_CLASS yc on (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."')
                            INNER JOIN YEAR_CLASS_TEACHER yct ON (yc.YearClassID = yct.YearClassID and yct.UserID = '".$ThisUserID."')";
		    $student_list = $lnotice->returnVector($sql);
		    $student_list = implode("', '", $student_list);
		    
			$sql1 = "SELECT
                     a.NoticeID,
                     a.NoticeNumber,
                     a.Title,
                     a.DateStart,
                     a.DateEnd,
                     COUNT(b.StudentID) AS AllStudents,
				     COUNT(case when b.RecordStatus = 2 then 1 else null end) SignedStudents,
				     1 as accessRightOrder
                     $specialNoticeField
                     ,Module
                 From
                     INTRANET_NOTICE as a
                     Inner JOIN INTRANET_NOTICE_REPLY as b ON a.NoticeID = b.NoticeID
                     Inner JOIN INTRANET_NOTICE_PIC as p ON a.NoticeID = p.NoticeID and p.PICUserID='" . $ThisUserID . "'
                 WHERE
                     $condStart
                     unix_timestamp(DATE(a.DateEnd)) >= unix_timestamp(DATE('" . getStartDateOfAcademicYear ( Get_Current_Academic_Year_ID () ) . "'))
                     AND (a.Module IN ('DISCIPLINE', 'Payment', 'STUDENTATTENDANCE') OR a.Module IS NULL) AND a.RecordStatus=1 AND a.IsDeleted=0
                GROUP BY a.NoticeID
                ORDER BY DateStart DESC, NoticeID DESC";
            $result1 = $lnotice->returnArray($sql1);
            
            $result2 = array();		          
	    	$sql2 = "SELECT
				    a.NoticeID,
					a.NoticeNumber,
					a.Title,
					a.DateStart,
					a.DateEnd,
				    COUNT(b.StudentID) AS AllStudents,
				    COUNT(case when b.RecordStatus = 2 then 1 else null end) SignedStudents,
				    2 as accessRightOrder
                    $specialNoticeField
                    ,Module
				FROM
					INTRANET_NOTICE as a
					Inner JOIN INTRANET_NOTICE_REPLY as b ON a.NoticeID = b.NoticeID
				WHERE
					$condStart
					unix_timestamp(DATE(a.DateEnd)) >= unix_timestamp(DATE('" . getStartDateOfAcademicYear ( Get_Current_Academic_Year_ID () ) . "'))
					AND (a.Module IN ('DISCIPLINE', 'Payment', 'STUDENTATTENDANCE') OR a.Module IS NULL) AND a.RecordStatus=1 AND a.IsDeleted=0
                    AND b.StudentID IN ('".$student_list."')
				GROUP BY a.NoticeID 
				ORDER BY DateStart DESC, NoticeID DESC";
			$result2 = $lnotice->returnArray($sql2);
			$result = array_merge($result1, $result2, $result3);
		}
		$numOfNotice = count($result);

		if($applySpecialNoticeChecking)
		{
            $sql_pic = "SELECT
                            DISTINCT a.NoticeID
                        From
                            INTRANET_NOTICE as a
                            Inner JOIN INTRANET_NOTICE_PIC as p ON a.NoticeID = p.NoticeID and p.PICUserID = '" . $ThisUserID . "'
                        WHERE
                            $condStart
                            unix_timestamp(DATE(a.DateEnd)) >= unix_timestamp(DATE('" . getStartDateOfAcademicYear(Get_Current_Academic_Year_ID() ) . "'))
                            AND (a.Module IN ('DISCIPLINE', 'Payment', 'STUDENTATTENDANCE') OR a.Module IS NULL) AND a.RecordStatus = 1 AND a.IsDeleted = 0";
            $pic_notice_ids = $lnotice->returnVector($sql_pic);
        }

		$ReturnAry ['eNoticeS'] = array();
		if ($numOfNotice == 0) {
			// return empty array if no records found
			// $ReturnAry = array();
		} else {
			// $school_url = split("\n",get_file_content("$intranet_root/file/email_website.txt"));
			// $school_domain = $school_url[0];
			$school_domain = curPageURL($withQueryString = false, $withPageSuffix = false);
			if (substr($school_domain, - 1 ) == "/") {
				$school_domain = substr($school_domain, 0, - 1);
            }
            
			$duplicate_noticeid_array = array();
			for($i=0; $i<$numOfNotice; $i++)
			{
				if (in_array($result[$i]['NoticeID'], $duplicate_noticeid_array)) {
					continue;
				}

				$duplicate_noticeid_array[] = $result[$i]['NoticeID'];
				$_noticeId = $result[$i]['NoticeID'];
				$_noticeNumber = $result[$i]['NoticeNumber'];
				$_title = $result[$i]['Title'];
				$_dateStart = $result[$i]['DateStart'];
				$_dateEnd = $result[$i]['DateEnd'];
				$_allStudents = $result[$i]['AllStudents'];
				$_signedStudents = $result[$i]['SignedStudents'];

                // [2020-0310-1051-05235]
                $_isSpecialNotice = false;
				if($applySpecialNoticeChecking) {
				    $_isSpecialNotice = $result[$i]['SpecialNotice'];
                }

                // [2020-0604-1821-16170]
                $_module = $result[$i]['Module'];
				$is_payment_notice = $_module == 'Payment';
				
				// prepare token for verification
				$_delimiter = "###";
				$_keyStr = $special_feature['ParentApp_PrivateKey'] . $_delimiter . $_noticeId . $_delimiter . $ThisUserID;
				// $_token = $_keyStr;
				$_token = md5($_keyStr);
				$_signURL = $school_domain . '/home/eAdmin/StudentMgmt/notice/app_view/students_list.php?token=' . $_token . '&NoticeID=' . $_noticeId . '&CurUserID=' . $ThisUserID . '&type=' . $result [$i] ['accessRightOrder'];

				// [2020-0310-1051-05235] show '*' in title & add get parm to sign_url
				if($_isSpecialNotice)
				{
                    $_title = '* '.$_title;

                    // [2020-0604-1821-16170]
                    //if ($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'] || ($isAccessGroupMember && ($lnotice->accessGroupCanSeeAllNotice || $lnotice->teacherCanSeeAllNotice))) {
                    if ($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'] || (!$is_payment_notice && $isSchoolNoticCanAccess) || ($is_payment_notice && $isPaymentNoticCanAccess)) {
                        // do nothing
                    }
                    else if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'] && (empty($pic_notice_ids) || !in_array($_noticeId, (array)$pic_notice_ids))) {
                        $_allStudents = -1;
                        $_signedStudents = -1;
                    }

                    if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'] && (empty($pic_notice_ids) || !in_array($_noticeId, (array)$pic_notice_ids))) {
                        $_signURL .= '&showTitleOnly=1';
                    }
                }

				$ReturnAry['eNoticeS'][] = array (
						"NoticeID" => $_noticeId,
						"NoticeNumber" => $leClassApp->standardizePushMessageText($_noticeNumber),
						"Title" => $leClassApp->standardizePushMessageText($_title),
						"DateStart" => $_dateStart,
						"DateEnd" => $_dateEnd,
						"AllStudents" => $_allStudents,
						"SignedStudents" => $_signedStudents,
						"SignURL" => $_signURL
				);
			}
		}
		
		return $ReturnAry;
	}

	function WS_GetSubjectListForApp() {
		$sql = "Select RecordID, EN_DES, CH_DES from ASSESSMENT_SUBJECT WHERE RecordStatus='1' AND (CMP_CODEID IS NULL OR CMP_CODEID = '')";
		$result = $this->returnArray ( $sql );
		$ReturnAry ['SubjectList'] = array ();
		for($i = 0; $i < count ( $result ); $i ++) {
			$ReturnAry ['SubjectList'] [] = array (
					'SubjectID' => $result [$i] ['RecordID'],
					'SubjectNameEn' => $result [$i] ['EN_DES'],
					'SubjectNameCh' => $result [$i] ['CH_DES'] 
			);
		}
		return $ReturnAry;
	}
	function WS_CreateLessonPlanWithVideoKey($CourseID, $LessonPlanTitle, $PreLessonStartDate, $PreLessonEndDate, $VideoTitle, $VideoKey, $TeacherUserID) {
		global $intranet_db, $eclass_db;
		
		$sql = "SELECT COUNT(*) FROM $eclass_db.course WHERE course_id = '$CourseID'";
		
		if ($CourseID && current ( $this->returnVector ( $sql ) )) {
			$sql = "SELECT
						uc.user_id, uc.memberType
					FROM
						$intranet_db.INTRANET_USER AS iu INNER JOIN
						$eclass_db.user_course AS uc ON iu.UserEmail = uc.user_email
					WHERE
						uc.course_id = '$CourseID' AND iu.UserID = '$TeacherUserID'";
			
			list ( $classroom_user_id, $classroom_member_type ) = current ( $this->returnArray ( $sql ) );
			
			if ($classroom_user_id && $classroom_member_type == 'T') {
				
				if ($LessonPlanTitle && $VideoTitle && $VideoKey) {
					$sql = "SELECT MAX(a_no) FROM  " . classNamingDB ( $CourseID ) . ".notes";
					$max_a_no = current ( $this->returnVector ( $sql ) );
					
					$sql = "INSERT INTO
								 " . classNamingDB ( $CourseID ) . ".notes
								(a_no, b_no, c_no, d_no, e_no, title, status, inputdate, modified)
							VALUES
								('" . ($max_a_no ? $max_a_no + 1 : 0) . "', '0', '0', '0', '0', '" . mysql_real_escape_string ( base64_decode ( $VideoTitle ) ) . "', '0', NOW(), NOW())";
					
					if ($this->db_db_query ( $sql )) {
						$note_id = $this->db_insert_id ();
						
						$sql = "INSERT INTO
									 " . classNamingDB ( $CourseID ) . ".notes_section
									(notes_id, notes_section_type, content, sequence_no, inputdate, modified)
								VALUES
									('$note_id', '26', '$VideoKey', '0', NOW(), NOW())"; // notes_section_type = 26 defined in /eclass40/system/settings/config.inc.php $cfg['notes_section_type']["isFlippingChannels"]
						
						if ($this->db_db_query ( $sql )) {
							if ($PreLessonStartDate && $PreLessonEndDate) {
								$fields = ", prelesson_startdate, prelesson_enddate";
								$values = ", '$PreLessonStartDate', '$PreLessonEndDate'";
								$stage_type = 2;
							} else {
								$fields = '';
								$values = '';
								$stage_type = 1;
							}
							
							$sql = "INSERT INTO
										" . classNamingDB ( $CourseID ) . ".lesson_plan
										(
										 	title, description, expect_duration, real_lesson_date, real_duration, 
										 	is_used, createdby, modifiedby, inputdate, modified, 
										 	SubjectID, teacher_id, category
											" . $fields . "
	  									)
									VALUES
										(
											'" . mysql_real_escape_string ( base64_decode ( $LessonPlanTitle ) ) . "', '', '0', '0000-00-00 00:00:00', '', 
											'0', '$classroom_user_id', '$classroom_user_id', NOW(), NOW(),
											'0', '$classroom_user_id', ''
											" . $values . "
	   									)";
							
							if ($this->db_db_query ( $sql )) {
								$lesson_plan_id = $this->db_insert_id ();
								
								$functionSerializedObjAry ['Note'] [] = $note_id;
								$functionSerializedObjAry ['DisablePowerSpeech'] = 0;
								
								$sql = "INSERT INTO
											" . classNamingDB ( $CourseID ) . ".lesson_plan_section
											(
												plan_id, title, description, section_type, function_id,
												duration, createdby, modifiedby, inputdate, modified,
												sequence_no, message, attachment, subject, stage_type, 
												section_target, functionSerializedObj, instruction
	   										)
										VALUES
											(
												'$lesson_plan_id', '" . mysql_real_escape_string ( base64_decode ( $VideoTitle ) ) . "', '', 'NOTE', '0',
												'0', '$classroom_user_id', '$classroom_user_id', NOW(), NOW(),
												'1', '', '', '', '$stage_type',
												'', '" . mysql_real_escape_string ( serialize ( $functionSerializedObjAry ) ) . "', '')";
								
								if ($this->db_db_query ( $sql )) {
									$ReturnContent ['LessonPlanID'] = $lesson_plan_id;
								} else {
									$ReturnContent = '812'; // Fail to insert lesson plan section
								}
							} else {
								$ReturnContent = '811'; // Fail to insert lesson plan
							}
						} else {
							$ReturnContent = '814'; // Fail to insert note section
						}
					} else {
						$ReturnContent = '813'; // Fail to insert note
					}
				} else {
					$ReturnContent = '102'; // Invalid parameters - $LessonPlanTitle or $VideoTitle or $VideoKey is empty
				}
			} else {
				$ReturnContent = '502'; // Permission to access this method is not granted
			}
		} else {
			$ReturnContent = '804'; // Classroom does not exist.
		}
		
		return $ReturnContent;
	}
	function WS_GetDigitalChannelPhotoList($TargetUserID, $CurrentUserID, $SchoolType, $ParLang) {
		global $PATH_WRT_ROOT, $intranet_root,$plugin;
		if ($TargetUserID != "") {
			$UserIDAry = array (
					$TargetUserID,
					$CurrentUserID 
			);
		} else {
			$UserIDAry = array (
					$CurrentUserID 
			);
		}
		$digitalChannelsEnable = isset($plugin['DigitalChannels'])?$plugin['DigitalChannels']:false;
		if ($SchoolType == "K"&&!$digitalChannelsEnable) {
			include_once ($PATH_WRT_ROOT . "includes/kis/apps/libkis_album_ws.php");
			$kis_album_ws = new kis_album_ws ();
			$albumAry = $kis_album_ws->AppGetAlbum ( $UserIDAry );
			$photoAry = $kis_album_ws->AppGetPhoto ( $UserIDAry );
			$digitalChannelReturnAry = array (
					'AlbumList' => $albumAry,
					'PhotoList' => $photoAry 
			);
			return $digitalChannelReturnAry;
		} else {
			include_once ($PATH_WRT_ROOT . "includes/global.php");
			include_once ($PATH_WRT_ROOT . "includes/libdb.php");
			include_once ($PATH_WRT_ROOT . 'includes/DigitalChannels/libdigitalchannels.php');
			$libdigitalchannels = new libdigitalchannels ();
			$categoryAry = $libdigitalchannels->AppGetCategory ( $UserIDAry );
			
			$categoryIDs = array();
			foreach((array)$categoryAry as $_categoryAry) {
			    $categoryIDs[] = $_categoryAry['CategoryID'];
			}
			$isCategoriesEditable = $libdigitalchannels->AppIsCategoriesEditable($categoryIDs, $UserIDAry);
			
			foreach((array)$categoryAry as $_idx=>$_categoryAry) {
			    $_categoryID = $_categoryAry['CategoryID'];
//			    $_isCategoryEditable = $libdigitalchannels->AppIsCategoryEditable($_categoryID, $UserIDAry);
			    $categoryAry[$_idx]['IsUserEditable'] = $isCategoriesEditable[$_categoryID];
			}
			
			$albumAry = $libdigitalchannels->AppGetAlbum ( $UserIDAry,'',$ParLang);
			$photoTypeAry = $libdigitalchannels->AppGetPhotoType ( $UserIDAry );
			$recPhotoIDArr = $photoTypeAry ['RecPhotoID'];

			$albumPhotoAry = $libdigitalchannels->AppGetPhoto ( $UserIDAry, '', '', $ParLang);
			$recPhotoAry = array ();
			if (count ( $recPhotoIDArr ) > 0) {
				$recPhotoAry = $libdigitalchannels->AppGetRecPhoto ( $UserIDAry, $recPhotoIDArr ,$ParLang);
			}
            $photoAry = array_merge ( $albumPhotoAry, $recPhotoAry );
			$reslutIDAry = array ();
			$photoFinalAry = array ();

			for($i = 0; $i < count ( $photoAry ); $i ++) {
				$tempPhotoID = $photoAry [$i] ['PhotoID'];

				if (! in_array ( $tempPhotoID, $reslutIDAry )) {
					$photoFinalAry [] = $photoAry [$i];
				}
				$reslutIDAry [] = $tempPhotoID;
			}

			$albumIDs = array();
			for($i = 0; $i < count ( $albumAry ); $i ++) {
			    $albumIDs[] = $albumAry[$i] ['AlbumID'];
			}
			$isAlbumsReadable = $libdigitalchannels->AppIsAlbumsReadable($albumIDs, $CurrentUserID);
			$isAlbumsEditable = $libdigitalchannels->AppIsAlbumsEditable($albumIDs, $CurrentUserID);
			
			for($i = 0; $i < count ( $albumAry ); $i ++) {
				$isUserReadable = $isAlbumsReadable[$albumAry[$i]['AlbumID']];
				//$isUserReadable = $libdigitalchannels->AppIsAlbumReadable ( $albumAry [$i] ['AlbumID'], $CurrentUserID );
				$albumAry [$i] ['IsUserReadable'] = $isUserReadable;
				$isUserEditable = $isAlbumsEditable[$albumAry[$i]['AlbumID']];
				//$isUserEditable = $libdigitalchannels->AppIsAlbumEditable ( $albumAry [$i] ['AlbumID'], $CurrentUserID );
				$albumAry [$i] ['IsUserEditable'] = $isUserEditable;
			}
			
			$digitalChannelReturnAry = array (
					'CategoryList' => $categoryAry,
					'AlbumList' => $albumAry,
					'PhotoList' => $photoFinalAry,
					'PhotoTypeList' => $photoTypeAry
			);

			return $digitalChannelReturnAry;
		}
	}
	function WS_DigitalChannelPhotoLike($CurrentUserID, $PhotoID) {
		global $PATH_WRT_ROOT, $intranet_root;
		include_once ($PATH_WRT_ROOT . "includes/global.php");
		include_once ($PATH_WRT_ROOT . "includes/libdb.php");
		include_once ($PATH_WRT_ROOT . 'includes/DigitalChannels/libdigitalchannels.php');
		$libdigitalchannels = new libdigitalchannels ();
		$sql = "SELECT * 
                 FROM
                      INTRANET_DC_ALBUM_PHOTO_VIEW v 
                 JOIN
                      INTRANET_DC_ALBUM_PHOTO p
				 ON
					v.PhotoID = p.AlbumPhotoID
				 WHERE 
					v.UserID = '" . $CurrentUserID . "'  AND v.PhotoID = '" . $PhotoID . "' ";
		if (count ( $this->returnVector ( $sql ) ) > 0) {
		} else {
			$sql = "INSERT INTO INTRANET_DC_ALBUM_PHOTO_VIEW (UserID, PhotoID, DateModified)
			VALUES ('$CurrentUserID','$PhotoID',NOW())";
			$this->db_db_query ( $sql );
		}
		
		$sql = "UPDATE INTRANET_DC_ALBUM_PHOTO_VIEW SET IsFavorite = IF(IsFavorite,'0','1'), DateModified = NOW() WHERE PhotoID = '$PhotoID' AND UserID ='$CurrentUserID'";
		if ($this->db_db_query ( $sql )) {
			$sql = "SELECT IsFavorite FROM INTRANET_DC_ALBUM_PHOTO_VIEW WHERE PhotoID = '$PhotoID' AND UserID ='$CurrentUserID'";
			$likeAry = $this->returnVector ( $sql );
//			$isFavorite = $likeAry [0] ['IsFavorite'];
            $isFavorite = $likeAry [0];
		} else {
			$isFavorite = - 1;
		}
		$photoFinalAry = $libdigitalchannels->AppGetPhoto ( $CurrentUserID, '', $PhotoID );
		if (count ( $photoFinalAry ) == 0) {
			$photoFinalAry = $libdigitalchannels->AppGetRecPhoto ( array (
					$CurrentUserID 
			), array (
					$PhotoID 
			) );
		}
		return array (
				'LikeResult' => $isFavorite,
				'PhotoInfo' => $photoFinalAry [0] 
		);
	}
	function WS_UserChangePassword($CurrentUserID, $OldPassword, $NewPassword, $ParUserLogin, $LangSetting = 'en') {
		global $PATH_WRT_ROOT;
		include_once ($PATH_WRT_ROOT . "includes/libauth.php");
		$lauth = new libauth ();
		$LangSetting = $LangSetting == "en" ? $LangSetting : "b5";
		$change_password_status = $lauth->ChangePassword ( $CurrentUserID, $ParUserLogin, $OldPassword, $NewPassword, $NewPassword, false, $LangSetting );
		// if($ParUserLogin == $NewPassword){
		// return array('UpdateSuccess' => $change_password_status,'SameWithUserLogin' => true);
		// }
		return array (
				'UpdateSuccess' => $change_password_status 
		);
	}
	function WS_GetMedicalCaringForApp($intranetUserID) {
	    global $PATH_WRT_ROOT, $SessionID, $special_feature, $sys_custom;
	    include_once ($PATH_WRT_ROOT . "includes/cust/common_function.php");
	    include_once ($PATH_WRT_ROOT . "includes/cust/medical/libMedical.php");
	    include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentLogLev1.php");
	    include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentLogLev2.php");
	    include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentSleepLev1.php");
	    include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentSleepLev2.php");
	    // include_once($PATH_WRT_ROOT."includes/cust/medical/medicalConfig.inc.php");
	    
	    $school_domain = curPageURL ( $withQueryString = false, $withPageSuffix = false );
	    $delimiter = "###";
	    if (substr ( $school_domain, - 1 ) == "/")
	        $school_domain = substr ( $school_domain, 0, - 1 );
	        $objMedical = new libMedical ();
	        $ts_record = strtotime ( date ( 'm/d/Y' ) );
	        $Year = date ( 'Y', $ts_record );
	        $Month = date ( 'm', $ts_record );
	        $Day = date ( 'd', $ts_record );
	        $filterYear = $Year;
	        $filterMonth = $Month;
	        $filterday = $Day;
	        $bowel_result = array ();
	        $studentlog_result = array ();
	        $sleep_result = array ();
	        
	        // get student log lev1 and lev2 for look up
	        $objStudentLogLev1 = new studentLogLev1 ();
	        $lev1Detail = $objStudentLogLev1->getActiveStatus ();
	        $lev1List = array ();
	        foreach ( $lev1Detail as $lev1 ) {
	            $lev1List [$lev1 ['Lev1ID']] = $lev1 ['Lev1Name'];
	        }
	        $objStudentLogLev2 = new studentLogLev2 ();
	        $lev2Detail = $objStudentLogLev2->getActiveStatus ();
	        $lev2List = array ();
	        foreach ( $lev2Detail as $lev2 ) {
	            $lev2List [$lev2 ['Lev2ID']] = $lev2 ['Lev2Name'];
	        }
	        
	        // get sleep status and reason for look up
	        $objStudentSleepLev1 = new studentSleepLev1();
	        $sleepStatusAry = $objStudentSleepLev1->getActiveStatus();
	        $sleepStatusAssoc = BuildMultiKeyAssoc($sleepStatusAry, array('StatusID'), array('StatusName'), $SingleValue=1);
	        
	        $objStudentSleepLev2 = new studentSleepLev2();
	        $sleepReasonAry = $objStudentSleepLev2->getActiveStatus();
	        $sleepReasonAssoc = BuildMultiKeyAssoc($sleepReasonAry, array('ReasonID'), array('ReasonName'), $SingleValue=1);
	        
	        for($i = 0; $i < 3; $i ++) {
	            // bowel
	            if ($i != 0) {
	                if ($filterMonth == "01") {
	                    $filterYear = $filterYear - 1;
	                    $filterMonth = "12";
	                } else {
	                    $filterMonth = $filterMonth - 1;
	                }
	            }
	            $filterMonth = str_pad ( $filterMonth, 2, '0', STR_PAD_LEFT );
	            // $BowelTableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"] ."{$filterYear}_{$filterMonth}";
	            $BowelTableName = "MEDICAL_STUDENT_BOWEL_LOG_" . "{$filterYear}_{$filterMonth}";
	            $bowelTableExists = hasDailyLogTableByName ( $BowelTableName );
	            if ($bowelTableExists) {
	                $fieldList = array ();
	                /*
	                 * if( $attendanceTableExists ){
	                 * // For attedance status field, 0= present and 2 = late, both mean the student @ school
	                 * $fieldList [] = "(c.AMStatus = '0' Or c.AMStatus = '2') as Attendance";
	                 * $attendanceJoinTable = "LEFT JOIN {$TableName} c ON c.UserID=b.UserID and c.DayNumber=day(b.RecordTime)";
	                 * }
	                 */
	                
	                $fieldList [] = 'b.RecordID';
	                $fieldList [] = 'date(b.RecordTime) as RecordDate';
	                $fieldList [] = 'substring(time(b.RecordTime),1,5) as RecordTime';
	                // $fieldList [] = 'b.Remarks';
	                $fieldList [] = 's.StatusName';
	                $fieldList [] = 's.Color';
	                $fieldList [] = ($sys_custom['medical']['swapBowelAndSleep'] ? '"sleep"' : '"bowel"') . ' as charactor';
	                $fieldList [] = '"" as BehaviourOne';
	                $fieldList [] = '"" as BehaviourTwo';
	                $fieldList [] = '"" as Duration';
	                // $fieldList [] = "case when u.RecordType=".USERTYPE_PARENT." then concat(u.LastUpdatedBy,' (".$Lang['medical']['general']['parent'].")') else u.LastUpdatedBy end as LastUpdatedBy";
	                // $fieldList [] = 'b.DateModified as LastUpdatedOn';
	                
	                $fieldSelected = implode ( ", ", $fieldList );
	                $sql = "SELECT {$fieldSelected}
	                FROM {$BowelTableName} b
	                LEFT JOIN MEDICAL_BOWEL_STATUS s ON b.BowelID=s.StatusID
	                {$attendanceJoinTable}
	                LEFT JOIN (SELECT " . getNameFieldByLang2 () . " As LastUpdatedBy, RecordType, UserID
	                FROM INTRANET_USER
	                ) u ON u.UserID=b.ModifyBy
	                WHERE b.UserID='$intranetUserID'
	                ORDER BY date(b.RecordTime) DESC, b.RecordTime";
	                
	                $rs = $this->returnResultSet ( $sql );
	                for($j = 0; $j < count ( $rs ); $j ++) {
	                    $keyStr = $special_feature ['ParentApp_PrivateKey'] . $delimiter . $rs [$j] ["RecordID"] . $delimiter . $rs [$j] ["RecordDate"];
	                    $token = md5 ( $keyStr );
	                    $signURL = $school_domain . '/home/eClassApp/eclassApp/medicalCaring/medicalCaring_detail.php?token=' . $token . '&RecordID=' . $rs [$j] ["RecordID"] . '&charactor=1&RecordDate=' . $rs [$j] ["RecordDate"] . '&Type=p';
	                    // $signURL = htmlspecialchars($signURL);
	                    
	                    $rs [$j] ["url"] = $signURL;
	                    $bowel_result [] = $rs [$j];
	                }
	            }
	            
	            // student log
	            /*
	             * $startDay = "{$filterYear}-{$filterMonth}-01";
	             * $endDay = date("Y-m-t", strtotime($startDay)); // last day of the month
	             * studentAttendanceToTempTable($startDay,$endDay);
	             */
	            $modifyBy = getNameFieldByLang2 ( 'IU.' );

	            $sql = "SELECT
	            MSL.RecordID,
	            MSL.RecordTime,
	            MSL.BehaviourOne,
	            MSL.BehaviourTwo,
	            MSL.Duration,
	            '' as Color,
	            '' as StatusName,
	            'log' as charactor
	            
	            FROM
	            MEDICAL_STUDENT_LOG MSL
	            
	            LEFT JOIN
	            INTRANET_USER IU
	            ON
	            MSL.ModifyBy = IU.UserID
	            WHERE
	            MSL.UserID = '{$intranetUserID}'
	            AND
	            DATE_FORMAT(MSL.RecordTime, '%Y-%m') = '{$filterYear}-{$filterMonth}'
	            ORDER BY
	            DATE(MSL.RecordTime) DESC, MSL.RecordTime ASC
	            ";
	            $rs = $this->returnResultSet ( $sql );
	            for($j = 0; $j < count ( $rs ); $j ++) {
	                $rs [$j] ["BehaviourOne"] = $lev1List [$rs [$j] ["BehaviourOne"]];
	                $rs [$j] ["BehaviourTwo"] = $lev2List [$rs [$j] ["BehaviourTwo"]];
	                $ts_record = strtotime ( $rs [$j] ["RecordTime"] );
	                $rs [$j] ["RecordDate"] = date ( 'Y-m-d', $ts_record );
	                $rs [$j] ["RecordTime"] = date ( 'H:i', $ts_record );
	                
	                $keyStr = $special_feature ['ParentApp_PrivateKey'] . $delimiter . $rs [$j] ["RecordID"] . $delimiter . $rs [$j] ["RecordDate"];
	                $token = md5 ( $keyStr );
	                $signURL = $school_domain . '/home/eClassApp/eclassApp/medicalCaring/medicalCaring_detail.php?token=' . $token . '&RecordID=' . $rs [$j] ["RecordID"] . '&charactor=2&RecordDate=' . $rs [$j] ["RecordDate"] . '&Type=p';
	                
	                $rs [$j] ["url"] = $signURL;
	                $studentlog_result [] = $rs [$j];
	            }
	            
	            // sleep
	            $sql = "SELECT
                                    s.RecordID, 
                                    s.RecordTime,
                                    s.SleepID,
                                    s.ReasonID,
                                    '' AS Duration,
                                    '' AS Color,
                                    '' AS StatusName,
                                    ". ($sys_custom['medical']['swapBowelAndSleep'] ? "'bowel'" : "'sleep'") . " AS charactor
                        FROM
                	                MEDICAL_STUDENT_SLEEP s
        	            LEFT JOIN
	                                INTRANET_USER u ON u.UserID=s.ModifyBy
	                    WHERE
	                                s.UserID = '{$intranetUserID}'
	                    AND
	                                DATE_FORMAT(s.RecordTime, '%Y-%m') = '{$filterYear}-{$filterMonth}'
	                    ORDER BY
                                    DATE(s.RecordTime) DESC, s.RecordTime ASC
	                   ";
	            $rs = $this->returnResultSet ( $sql );
	            for($j = 0, $jMax=count ( $rs ); $j < $jMax; $j ++) {
	                $rs [$j] ["BehaviourOne"] = $sleepStatusAssoc[$rs [$j] ["SleepID"]];
	                $rs [$j] ["BehaviourTwo"] = $sleepReasonAssoc[$rs [$j] ["ReasonID"]];
	                $ts_record = strtotime ( $rs [$j] ["RecordTime"] );
	                $rs [$j] ["RecordDate"] = date ( 'Y-m-d', $ts_record );
	                $rs [$j] ["RecordTime"] = date ( 'H:i', $ts_record );
	                $rs [$j] ["StatusName"] = $sleepStatusAssoc[$rs [$j] ["SleepID"]];
	                
	                $keyStr = $special_feature ['ParentApp_PrivateKey'] . $delimiter . $rs [$j] ["RecordID"] . $delimiter . $rs [$j] ["RecordDate"];
	                $token = md5 ( $keyStr );
	                $signURL = $school_domain . '/home/eClassApp/eclassApp/medicalCaring/medicalCaring_detail.php?token=' . $token . '&RecordID=' . $rs [$j] ["RecordID"] . '&charactor=3&RecordDate=' . $rs [$j] ["RecordDate"] . '&Type=p';
	                
	                $rs [$j] ["url"] = $signURL;
	                $sleep_result [] = $rs [$j];
	            }
	            
	        }     // end loop for 3 records
	        $ReturnAry ['MedicalCaring'] = array_merge ( $bowel_result, $studentlog_result, $sleep_result);
	        return $ReturnAry;
	}
	function WS_GetMedicalCaringForTeacherApp($parIntranetUserId) {
	    global $PATH_WRT_ROOT, $SessionID, $special_feature, $sys_custom;
	    include_once ($PATH_WRT_ROOT . "includes/cust/common_function.php");
	    include_once ($PATH_WRT_ROOT . "includes/cust/medical/libMedical.php");
	    include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentLogLev1.php");
	    include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentLogLev2.php");
	    include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentSleepLev1.php");
	    include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentSleepLev2.php");
	    
	    include_once ($PATH_WRT_ROOT . "includes/eClassApp/libeClassApp.php");
	    $leClassApp = new libeClassApp();
	    
	    $AcademicYearID = Get_Current_Academic_Year_ID ();
	    $school_domain = curPageURL ( $withQueryString = false, $withPageSuffix = false );
	    $delimiter = "###";
	    if (substr ( $school_domain, - 1 ) == "/")
	        $school_domain = substr ( $school_domain, 0, - 1 );
	        $objMedical = new libMedical ();
	        $ts_record = strtotime ( date ( 'm/d/Y' ) );
	        $Year = date ( 'Y', $ts_record );
	        $Month = date ( 'm', $ts_record );
	        $Day = date ( 'd', $ts_record );
	        $filterYear = $Year;
	        $filterMonth = $Month;
	        $filterday = $Day;
	        $bowel_result = array ();
	        $studentlog_result = array ();
	        $sleep_result = array ();
	        
	        // get student log lev1 and lev2 for look up
	        $objStudentLogLev1 = new studentLogLev1 ();
	        $lev1Detail = $objStudentLogLev1->getActiveStatus ();
	        $lev1List = array ();
	        foreach ( $lev1Detail as $lev1 ) {
	            $lev1List [$lev1 ['Lev1ID']] = $lev1 ['Lev1Name'];
	        }
	        $objStudentLogLev2 = new studentLogLev2 ();
	        $lev2Detail = $objStudentLogLev2->getActiveStatus ();
	        $lev2List = array ();
	        foreach ( $lev2Detail as $lev2 ) {
	            $lev2List [$lev2 ['Lev2ID']] = $lev2 ['Lev2Name'];
	        }
	        
	        // get sleep status and reason for look up
	        $objStudentSleepLev1 = new studentSleepLev1();
	        $sleepStatusAry = $objStudentSleepLev1->getActiveStatus();
	        $sleepStatusAssoc = BuildMultiKeyAssoc($sleepStatusAry, array('StatusID'), array('StatusName'), $SingleValue=1);
	        
	        $objStudentSleepLev2 = new studentSleepLev2();
	        $sleepReasonAry = $objStudentSleepLev2->getActiveStatus();
	        $sleepReasonAssoc = BuildMultiKeyAssoc($sleepReasonAry, array('ReasonID'), array('ReasonName'), $SingleValue=1);
	        
	        for($i = 0; $i < 3; $i ++) {
	            // bowel
	            if ($i != 0) {
	                if ($filterMonth == "01") {
	                    $filterYear = $filterYear - 1;
	                    $filterMonth = "12";
	                } else {
	                    $filterMonth = $filterMonth - 1;
	                }
	            }
	            $filterMonth = str_pad ( $filterMonth, 2, '0', STR_PAD_LEFT );
	            // $BowelTableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"] ."{$filterYear}_{$filterMonth}";
	            $BowelTableName = "MEDICAL_STUDENT_BOWEL_LOG_" . "{$filterYear}_{$filterMonth}";
	            $bowelTableExists = hasDailyLogTableByName ( $BowelTableName );
	            if ($bowelTableExists) {
	                $fieldList = array ();
	                /*
	                 * if( $attendanceTableExists ){
	                 * // For attedance status field, 0= present and 2 = late, both mean the student @ school
	                 * $fieldList [] = "(c.AMStatus = '0' Or c.AMStatus = '2') as Attendance";
	                 * $attendanceJoinTable = "LEFT JOIN {$TableName} c ON c.UserID=b.UserID and c.DayNumber=day(b.RecordTime)";
	                 * }
	                 */
	                
	                $fieldList [] = 'b.RecordID';
	                $fieldList [] = 'date(b.RecordTime) as RecordDate';
	                $fieldList [] = 'substring(time(b.RecordTime),1,5) as RecordTime';
	                // $fieldList [] = 'b.Remarks';
	                $fieldList [] = 's.StatusName';
	                $fieldList [] = 's.Color';
	                $fieldList [] = ($sys_custom['medical']['swapBowelAndSleep'] ? '"sleep"' : '"bowel"') . ' as charactor';
	                $fieldList [] = '"" as BehaviourOne';
	                $fieldList [] = '"" as BehaviourTwo';
	                $fieldList [] = '"" as Duration';
	                $fieldList [] = getNameFieldByLang2 ( 'IU.' ) . ' As Name';
	                $fieldList [] = $objMedical->getClassNameByLang2 ( $prefix = "YC." ) . ' as ClassName';
	                $fieldSelected = implode ( ", ", $fieldList );
	                $sql = "SELECT {$fieldSelected}
	                FROM {$BowelTableName} b
	                LEFT JOIN MEDICAL_BOWEL_STATUS s ON b.BowelID=s.StatusID
	                {$attendanceJoinTable}
	                LEFT JOIN
	                INTRANET_USER IU ON IU.UserID=b.UserID
	                Inner Join
	                YEAR_CLASS_USER YCU
	                On
	                IU.UserID = YCU.UserID
	                Inner Join
	                YEAR_CLASS YC
	                On
	                YC.YearClassID = YCU.YearClassID
	                Inner Join
	                YEAR Y
	                On
	                Y.YearID = YC.YearID
	                WHERE YC.AcademicYearID = '{$AcademicYearID}'
	                ORDER BY date(b.RecordTime) DESC, b.RecordTime";
	                
	                $rs = $this->returnResultSet ( $sql );
	                for($j = 0; $j < count ( $rs ); $j ++) {
	                    //$keyStr = $special_feature ['ParentApp_PrivateKey'] . $delimiter . $rs [$j] ["RecordID"] . $delimiter . $rs [$j] ["RecordDate"];
	                    //$token = md5 ( $keyStr );
	                    //$signURL = $school_domain . '/home/eClassApp/eclassApp/medicalCaring/medicalCaring_detail.php?token=' . $token . '&RecordID=' . $rs [$j] ["RecordID"] . '&charactor=1&RecordDate=' . $rs [$j] ["RecordDate"] . '&Type=t';
	                    // $signURL = htmlspecialchars($signURL);
	                    
	                    $signURL = $school_domain.'/api/eClassApp/sso_intranet.php?'.$leClassApp->getSSOParam($parIntranetUserId, 'medicalCaringBowelRecordDetails_v2').'&RecordID=' . $rs [$j] ["RecordID"].'&RecordDate=' . $rs [$j] ["RecordDate"];
	                    
	                    $rs [$j] ["url"] = $signURL;
	                    $bowel_result [] = $rs [$j];
	                }
	            }
	            
	            // student log
	            /*
	             * $startDay = "{$filterYear}-{$filterMonth}-01";
	             * $endDay = date("Y-m-t", strtotime($startDay)); // last day of the month
	             * studentAttendanceToTempTable($startDay,$endDay);
	             */
	            $modifyBy = getNameFieldByLang2 ( 'IU.' );

	            $sql = "SELECT
				MSL.RecordID,
				MSL.RecordTime,
				MSL.BehaviourOne,
				MSL.BehaviourTwo,
				MSL.Duration,
			    '' as Color,
			    '' as StatusName,
			    'log' as charactor," . getNameFieldByLang2 ( 'IU.' ) . " As Name," . $objMedical->getClassNameByLang2 ( $prefix = "YC." ) . " as ClassName " .
			    
			    "FROM
			    MEDICAL_STUDENT_LOG MSL
			    
			    LEFT JOIN
			    INTRANET_USER IU
			    ON
			    MSL.UserID = IU.UserID
			    Inner Join
			    YEAR_CLASS_USER YCU
			    On
			    IU.UserID = YCU.UserID
			    Inner Join
			    YEAR_CLASS YC
			    On
			    YC.YearClassID = YCU.YearClassID
			    Inner Join
			    YEAR Y
			    On
			    Y.YearID = YC.YearID
			    WHERE
			    YC.AcademicYearID = '{$AcademicYearID}'
			    AND
			    DATE_FORMAT(MSL.RecordTime, '%Y-%m') = '{$filterYear}-{$filterMonth}'
			    ORDER BY
			    DATE(MSL.RecordTime) DESC, MSL.RecordTime ASC
			    ";
	            $rs = $this->returnResultSet ( $sql );
	            for($j = 0; $j < count ( $rs ); $j ++) {
	                $rs [$j] ["BehaviourOne"] = $lev1List [$rs [$j] ["BehaviourOne"]];
	                $rs [$j] ["BehaviourTwo"] = $lev2List [$rs [$j] ["BehaviourTwo"]];
	                $ts_record = strtotime ( $rs [$j] ["RecordTime"] );
	                $rs [$j] ["RecordDate"] = date ( 'Y-m-d', $ts_record );
	                $rs [$j] ["RecordTime"] = date ( 'H:i', $ts_record );
	                
	                //$keyStr = $special_feature ['ParentApp_PrivateKey'] . $delimiter . $rs [$j] ["RecordID"] . $delimiter . $rs [$j] ["RecordDate"];
	                //$token = md5 ( $keyStr );
	                //$signURL = $school_domain . '/home/eClassApp/eclassApp/medicalCaring/medicalCaring_detail.php?token=' . $token . '&RecordID=' . $rs [$j] ["RecordID"] . '&charactor=2&RecordDate=' . $rs [$j] ["RecordDate"] . '&Type=t';
	                
	                $signURL = $school_domain.'/api/eClassApp/sso_intranet.php?'.$leClassApp->getSSOParam($parIntranetUserId, 'medicalCaringLogRecordDetails_v2').'&RecordID=' . $rs [$j] ["RecordID"];
	                
	                $rs [$j] ["url"] = $signURL;
	                $studentlog_result [] = $rs [$j];
	            }
	            
	            // sleep
	            $sql = "SELECT
                	            s.RecordID,
                	            s.RecordTime,
                	            s.SleepID,
                	            s.ReasonID,
                	            '' AS Duration,
                	            '' AS Color,
                	            '' AS StatusName,
                	            ". ($sys_custom['medical']['swapBowelAndSleep'] ? "'bowel'" : "'sleep'") . " AS charactor," . 
                	            getNameFieldByLang2 ( 'u.' ) . " As Name," . 
                	            $objMedical->getClassNameByLang2 ( $prefix = "yc." ) . " as ClassName 
	            FROM
                	            MEDICAL_STUDENT_SLEEP s
	            LEFT JOIN
	                            INTRANET_USER u ON u.UserID=s.UserID
			    INNER JOIN
			                    YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
			    INNER JOIN
			                    YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID
			    INNER JOIN      
                                YEAR y ON y.YearID = yc.YearID
			    WHERE
			                    yc.AcademicYearID = '{$AcademicYearID}'
	            AND
	                            DATE_FORMAT(s.RecordTime, '%Y-%m') = '{$filterYear}-{$filterMonth}'
	            ORDER BY
	                            DATE(s.RecordTime) DESC, s.RecordTime ASC
	            ";
	            $rs = $this->returnResultSet ( $sql );
	            
	            for($j = 0, $jMax=count ( $rs ); $j < $jMax; $j ++) {
	                $rs [$j] ["BehaviourOne"] = $sleepStatusAssoc[$rs [$j] ["SleepID"]];
	                $rs [$j] ["BehaviourTwo"] = $sleepReasonAssoc[$rs [$j] ["ReasonID"]];
	                $ts_record = strtotime ( $rs [$j] ["RecordTime"] );
	                $rs [$j] ["RecordDate"] = date ( 'Y-m-d', $ts_record );
	                $rs [$j] ["RecordTime"] = date ( 'H:i', $ts_record );
	                $rs [$j] ["StatusName"] = $sleepStatusAssoc[$rs [$j] ["SleepID"]];

	                $signURL = $school_domain.'/api/eClassApp/sso_intranet.php?'.$leClassApp->getSSOParam($parIntranetUserId, 'medicalCaringSleepRecordDetails_v2').'&RecordID=' . $rs [$j] ["RecordID"];
	                $rs [$j] ["url"] = $signURL;
	                $sleep_result [] = $rs [$j];
	            }
	            
	        }     // end loop for 3 records
	        $ReturnAry ['MedicalCaring'] = array_merge ( $bowel_result, $studentlog_result, $sleep_result );
	        return $ReturnAry;
	}
	function WS_GetWeeklyDiaryForApp($targetStudentId) {
		
		// Get Homework List
		$sql = "SELECT
						st.SubjectGroupID
				FROM
						ACADEMIC_YEAR_TERM as ayt
						INNER JOIN SUBJECT_TERM as st ON (ayt.YearTermID = st.YearTermID)
						INNER JOIN SUBJECT_TERM_CLASS_USER as stu ON (st.SubjectGroupID = stu.SubjectGroupID)
				WHERE
						ayt.AcademicYearID = '" . Get_Current_Academic_Year_ID () . "'
						AND stu.UserID = '" . $targetStudentId . "'
				";
		$subjectGroupIdAry = $this->returnVector ( $sql );
		$sql = "select ycu.YearClassID from YEAR_CLASS_USER as ycu left join YEAR_CLASS as yc on yc.YearClassID = ycu.YearClassID where yc.AcademicYearID = '" . Get_Current_Academic_Year_ID () . "' and ycu.UserID = '" . $targetStudentId . "'";
		$classID = $this->returnVector ( $sql );
		$sql = "Select h.HomeworkID,h.Title,h.Description,h.Type, h.StartDate, h.DueDate 
	   		   from INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING  as h
	   		   WHERE h.ClassGroupID IN ('" . implode ( "','", ( array ) $subjectGroupIdAry ) . "')
	   		   		or h.ClassID = '" . $classID [0] . "' and CURDATE() >= h.StartDate";
		$result = $this->returnArray ( $sql );
		
		// return $result;
		$ReturnAry ['HomeworkList'] = array ();
		for($i = 0; $i < count ( $result ); $i ++) {
			$ReturnAry ['HomeworkList'] [] = array (
					'HomeworkID' => $result [$i] ['HomeworkID'],
					'Title' => $result [$i] ['Title'],
					'Description' => $result [$i] ['Description'],
					'Type' => $result [$i] ['Type'],
					'StartDate' => $result [$i] ['StartDate'],
					'DueDate' => $result [$i] ['DueDate'] 
			);
		}
		
		// Get Article of the student
		$sql = "Select a.ArticleID,a.ExcellentArticle,a.HasAttachment,a.HomeworkID, a.SubmitTime, a.TeacherComment ,a.TeacherHasRead ,a.Title ,a.Content,a.WordCount,a.Type
	   		   from INTRANET_APP_WEEKLYDIARY_ARTICLE  as a
	   		   WHERE a.UserID = '" . $targetStudentId . "' ";
		$result = $this->returnArray ( $sql );
		$ReturnAry ['Article'] = array ();
		for($i = 0; $i < count ( $result ); $i ++) {
			/*
			 * $ReturnAry['Article'][] = array('ArticleID'=>$result[$i]['ArticleID'],
			 * 'ExcellentArticle'=>$result[$i]['ExcellentArticle'],
			 * 'HasAttachment'=>$result[$i]['HasAttachment'],
			 * 'HomeworkID'=>$result[$i]['HomeworkID'],
			 * 'SubmitTime'=>$result[$i]['SubmitTime'],
			 * 'TeacherComment'=>$result[$i]['TeacherComment'],
			 * 'TeacherHasRead'=>$result[$i]['TeacherHasRead'],
			 * 'Title'=>$result[$i]['Title'],
			 * 'WordCount'=>$result[$i]['WordCount'],
			 * 'Type'=>$result[$i]['Type'],
			 * );
			 */
			// Get Article Attachment of the student
			$sql1 = "Select a.ArticleID,a.AttachmentID,a.Description,a.DomainName, a.Ext, a.Image ,a.ImageSmall ,a.Title ,a.Type ,a.Url, a.ShowInList
	   		   from INTRANET_APP_WEEKLYDIARY_ARTICLE_ATTACHMENT  as a
	   		   WHERE a.UserID = '" . $targetStudentId . "' and a.ArticleID = '" . $result [$i] ['ArticleID'] . "'";
			$result1 = $this->returnArray ( $sql1 );
			$array_attachment = array ();
			for($j = 0; $j < count ( $result1 ); $j ++) {
				$array_attachment [] = array (
						'ArticleID' => $result1 [$j] ['ArticleID'],
						'AttachmentID' => $result1 [$j] ['AttachmentID'],
						'Description' => $result1 [$j] ['Description'],
						'DomainName' => $result1 [$j] ['DomainName'],
						'Ext' => $result1 [$j] ['Ext'],
						'ImageLarge' => $result1 [$j] ['Image'],
						'ImageSmall' => $result1 [$j] ['ImageSmall'],
						'Title' => $result1 [$j] ['Title'],
						'Type' => $result1 [$j] ['Type'],
						'Url' => $result1 [$j] ['Url'],
						'ShowInList' => $result1 [$j] ['ShowInList'] 
				);
			}
			
			$ReturnAry ['Article'] [] = array (
					'ArticleID' => $result [$i] ['ArticleID'],
					'ExcellentArticle' => $result [$i] ['ExcellentArticle'],
					'HasAttachment' => $result [$i] ['HasAttachment'],
					'HomeworkID' => $result [$i] ['HomeworkID'],
					'SubmitTime' => $result [$i] ['SubmitTime'],
					'TeacherComment' => $result [$i] ['TeacherComment'],
					'TeacherHasRead' => $result [$i] ['TeacherHasRead'],
					'Title' => $result [$i] ['Title'],
					'Content' => $result [$i] ['Content'],
					'WordCount' => $result [$i] ['WordCount'],
					'Type' => $result [$i] ['Type'],
					'Attachment' => $array_attachment 
			);
		}
		return $ReturnAry;
	}
	function WS_GetELibPlusLoanBookRecord($targetUserId) {
		global $eclass_db, $special_feature, $intranet_root, $file_path, $PATH_WRT_ROOT, $SessionID;
		
		if (trim ( $PATH_WRT_ROOT ) == "") {
			$PATH_WRT_ROOT = "../";
		}
		
		include_once ($PATH_WRT_ROOT . "includes/libelibrary.php");
		include_once ($PATH_WRT_ROOT . "includes/libelibrary_plus.php");
		
		$lelibplus = new elibrary_plus ( 0, $targetUserId );
		
		$loan_books = $lelibplus->getUserLoanBooksCurrent ( 0, 1000 );
		$permission = elibrary_plus::getUserPermission ( $targetUserId );
		
		$returnAry = array ();
		$returnAry ['LoanBooks'] = array ();
		
		foreach ( $loan_books as $num => $book ) {
			
			$renewalQuotaResult = $lelibplus->IsRenewalAllowed ( $book ['borrow_id'] );
			
			$tmpAry = array ();
			$tmpAry ['BookItemId'] = $book ['UniqueID'];
			$tmpAry ['BookId'] = $book ['id'];
			$tmpAry ['BookTitle'] = $book ['title'];
			$tmpAry ['CheckedOutDate'] = $book ['borrow_date'];
			$tmpAry ['DueDate'] = $book ['due_date'];
			$tmpAry ['RenewCount'] = $book ['renew_count'];
			
			if ($book ['overdue_days'] > 0) {
				$tmpAry ['Status'] = "overdue"; // ������ Overdue
			} else if ($permission ['renew'] && $renewalQuotaResult == 1) {
				$tmpAry ['Status'] = "can_renew"; // ����������Renew allowed
			} else if ($renewalQuotaResult == - 4) {
				$tmpAry ['Status'] = "reserved_by_other"; // ���書已被����������������� The book is reserved by other reader
			} else {
				if ($renewalQuotaResult == 0)
					$tmpAry ['Status'] = "no_renewal_allowed"; // 不可續��� No renewal allowed
				else if ($renewalQuotaResult == - 3)
					$tmpAry ['Status'] = "reached_renewal_limit"; // 已到����������������Reached your renewal limit
				else
					$tmpAry ['Status'] = "loaned"; // ���閱���Loaned
			}
			
			$physicalBook = $lelibplus->GET_PHYSICAL_BOOK_INFO ( $book ['id'] );
			$tmpAry ['CoverImagePath'] = $physicalBook ['CoverImage'];
			
			$returnAry ['LoanBooks'] [] = $tmpAry;
		}
		
		return $returnAry;
	}
	function WS_GetELibPlusReservedBookRecord($targetUserId) {
		global $eclass_db, $special_feature, $intranet_root, $file_path, $PATH_WRT_ROOT, $SessionID;
		
		if (trim ( $PATH_WRT_ROOT ) == "") {
			$PATH_WRT_ROOT = "../";
		}
		
		include_once ($PATH_WRT_ROOT . "includes/libelibrary.php");
		include_once ($PATH_WRT_ROOT . "includes/libelibrary_plus.php");
		
		$lelibplus = new elibrary_plus ( 0, $targetUserId );
		
		$reserved_books = $lelibplus->getUserReservedBooks ();
		
		$returnAry = array ();
		$returnAry ['ReservedBooks'] = array ();
		
		foreach ( $reserved_books as $num => $book ) {
			$tmpAry = array ();
			$tmpAry ['BookId'] = $book ['id'];
			$tmpAry ['BookTitle'] = $book ['title'];
			$tmpAry ['RequestedDate'] = $book ['reserve_date'];
			// $tmpAry['ReadyDate'] = ''; // No Ready Date record at DB
			$tmpAry ['Status'] = $book ['status']; // (READY, WAITING) (���可���� 待���)
			$physicalBook = $lelibplus->GET_PHYSICAL_BOOK_INFO ( $book ['id'] );
			$tmpAry ['CoverImagePath'] = $physicalBook ['CoverImage'];
			
			$returnAry ['ReservedBooks'] [] = $tmpAry;
		}
		
		return $returnAry;
	}
	function WS_GetELibPlusOutstandingPenalty($targetUserId) {
		global $eclass_db, $special_feature, $intranet_root, $file_path, $PATH_WRT_ROOT, $SessionID;
		
		if (trim ( $PATH_WRT_ROOT ) == "") {
			$PATH_WRT_ROOT = "../";
		}
		
		include_once ($PATH_WRT_ROOT . "includes/libelibrary.php");
		include_once ($PATH_WRT_ROOT . "includes/libelibrary_plus.php");
		
		$lelibplus = new elibrary_plus ( 0, $targetUserId );
		
		$penalty_current = $lelibplus->getUserPenaltyCurrent ();
		
		$overallTotalPenalty = 0;
		
		$returnAry = array ();
		$returnAry ['OutstandingPenalty'] = array ();
		
		foreach ( $penalty_current as $num => $book ) {
			$physicalBook = $lelibplus->GET_PHYSICAL_BOOK_INFO ( $book ['id'] );
			$tmpAry = array ();
			$tmpAry ['BookItemId'] = $physicalBook ['UniqueID'];
			$tmpAry ['BookId'] = $book ['id'];
			$tmpAry ['BookTitle'] = $book ['title'];
			$tmpAry ['DueDate'] = $book ['due_date'];
			$tmpAry ['ReturnDate'] = $book ['return_date'];
			$tmpAry ['Reason'] = $book ['penalty_reason']; // (LOST, {number of overdue days})(���失, 2)
			$tmpAry ['TotalPenalty'] = $book ['penalty_total'];
			$tmpAry ['OutstandingPenalty'] = $book ['penalty_owing'];
			$tmpAry ['CoverImagePath'] = $physicalBook ['CoverImage'];
			$overallTotalPenalty += $book ['penalty_owing'];
			$returnAry ['OutstandingPenalty'] [] = $tmpAry;
		}
		
		$returnAry ['OverallOutstandingPenalty'] = $overallTotalPenalty;
		
		return $returnAry;
	}
	function WS_eClassGetLessonUsersUdid($lessonUniqueID) {
		global $eclass_db, $intranet_root;
		
		$sql = "SELECT title,teacherUserID,targetUserIDs FROM " . $eclass_db . ".active_lesson_plan WHERE sessionID = '" . $lessonUniqueID . "'";
		$lessonPlanInfoAry = current ( $this->returnArray ( $sql ) );
		$returnContent = array ();
		
		if ($lessonPlanInfoAry) {
			$content = array ();
			$content ['Lesson'] = array ();
			$content ['Lesson'] ['UniqueID'] = $lessonUniqueID;
			$content ['Lesson'] ['Title'] = $lessonPlanInfoAry ['title'];
			
			$studentIdAry = unserialize ( $lessonPlanInfoAry ['targetUserIDs'] );
			$studentIdAry = is_array ( $studentIdAry ) ? $studentIdAry : array ();
			$allUserIdAry = $studentIdAry;
			$allUserIdAry [] = $lessonPlanInfoAry ['teacherUserID'];
			
			$sql = "SELECT
						userID, deviceID
					FROM
						$eclass_db.active_lesson_user_device
					WHERE
						sessionID = '" . $lessonUniqueID . "' AND userID IN ('" . implode ( "','", $allUserIdAry ) . "')";
			$userDeviceIDInfoAry = $this->returnArray ( $sql );
			
			$userDeviceIDMapping = array ();
			foreach ( $userDeviceIDInfoAry as $userDeviceIDInfo ) {
				$userDeviceIDMapping [$userDeviceIDInfo ['userID']] = $userDeviceIDInfo ['deviceID'];
			}
			
			include_once ('libuser.php');
			$objTeacherUser = new libuser ( $lessonPlanInfoAry ['teacherUserID'] );
			$content ['Teacher'] = array ();
			$content ['Teacher'] ['Udid'] = $userDeviceIDMapping [$lessonPlanInfoAry ['teacherUserID']];
			$content ['Teacher'] ['Name'] = $objTeacherUser->UserName ( 1 );
			$content ['Teacher'] ['Photo'] = (is_file ( $intranet_root . $objTeacherUser->PersonalPhotoLink ) ? $objTeacherUser->PersonalPhotoLink : "");
			
			$content ['Students'] = array ();
			if (count ( $studentIdAry ) > 0) {
				foreach ( $studentIdAry as $studentId ) {
					$objStudentUser = new libuser ( $studentId );
					$content ['Students'] [] = array (
							'Udid' => $userDeviceIDMapping [$studentId],
							'Name' => $objStudentUser->UserName ( 1 ),
							'Photo' => (is_file ( $intranet_root . $objStudentUser->PersonalPhotoLink ) ? $objStudentUser->PersonalPhotoLink : ""),
							'Gender' => $objStudentUser->Gender 
					);
				}
			}
			
			$returnContent = $content;
		} else {
			$returnContent = 806; // Session does not exist.
		}
		return $returnContent;
	}
	function WS_SubmitWeeklyDiaryRecord($HomeworkID, $parUserID, $Title, $Content, $ArticleID = 0, $NewAttachmentAry = array(), $UnchangedAttachmentAry = array(), $ShowInListID = 0) {
		global $PATH_WRT_ROOT, $Lang, $eclassAppConfig, $intranet_db_user, $intranet_root;
		
		include_once ($PATH_WRT_ROOT . 'includes/libfilesystem.php');
		include_once ($PATH_WRT_ROOT . 'includes/liblog.php');
		
		$lfs = new libfilesystem ();
		$liblog = new liblog ();
		
		// retrieve UserID (student + active)
		$sql = "select UserID, UserLogin from INTRANET_USER where UserID='" . $parUserID . "' and RecordType=2";
		$rs = $this->returnResultSet ( $sql );
		$parUserID = $rs [0] ['UserID'];
		
		// check user exists
		if (! $parUserID) {
			return 104;
		}
		$successAry = array ();
		$attachmentUpdateSuccess = '1';
		$sql = "select Type from INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING where HomeworkID='" . $HomeworkID . "'";
		$result = $this->returnVector ( $sql );
		$type = $result [0];
		
		$filePathBase = $intranet_root;
		
		if ($ArticleID != 0) {
			$sql = "Update 
							INTRANET_APP_WEEKLYDIARY_ARTICLE
					Set
							Title = '" . $this->Get_Safe_Sql_Query ( $Title ) . "',
							Content = '" . $this->Get_Safe_Sql_Query ( $Content ) . "',
							SubmitTime = now(),
							DateModified = now(),
							LastModifiedBy = '" . $parUserID . "'
					Where
							ArticleID = '" . $ArticleID . "'
					";
			$successAry ['Update_Article_Record'] = $this->db_db_query ( $sql );
			
			// delete attachment in delete array
			
			$UnchangedAttachmentAryString = implode ( "','", $UnchangedAttachmentAry );
			$UnchangedAttachmentAryString = "'" . $UnchangedAttachmentAryString . "'";
			
			$sql = "select AttachmentID from INTRANET_APP_WEEKLYDIARY_ARTICLE_ATTACHMENT where ArticleID = '" . $ArticleID . "' and AttachmentID not in (" . $UnchangedAttachmentAryString . ") ";
			$DeleteAttachmentAry = $this->returnVector ( $sql );
			$logInfoAry = array ();
			for($i = 0; $i < count ( $DeleteAttachmentAry ); $i ++) {
				$_attachmentID = $DeleteAttachmentAry [$i];
				$sql = "select Type,Image from INTRANET_APP_WEEKLYDIARY_ARTICLE_ATTACHMENT where AttachmentID = '" . $_attachmentID . "'";
				$rs = $this->returnResultSet ( $sql );
				$_type = $rs [0] ['Type'];
				
				if ($type == 1) {
					// image
					$_imagePath = $rs [0] ['Image'];
					$_fullFilePath = $filePathBase . $_imagePath;
					$_fullFilePathNew = $_fullFilePath . '.' . date ( 'Ymd_His' );
					
					if ($_imagePath != '' && file_exists ( $_fullFilePath )) {
						$_renameSuccess = $lfs->file_rename ( $_fullFilePath, $_fullFilePathNew );
					}
				}
				
				$sql = "delete from INTRANET_APP_WEEKLYDIARY_ARTICLE_ATTACHMENT where AttachmentID = '" . $_attachmentID . "' ";
				$successAry ['Delete_DB_Record'] [$_attachmentID] = $this->db_db_query ( $sql );
			}
			
			// $sql = "delete from INTRANET_APP_WEEKLYDIARY_ARTICLE_ATTACHMENT where Type = 2 and ArticleID = '".$ArticleID."' ";
			// $successAry['Delete_DB_Record']['Old_URL'] = $this->db_db_query($sql);
			
			// insert delete log
			$tmpAry = array ();
			$tmpAry ['DeleteAttachmentID'] = implode ( ',', $DeleteAttachmentAry );
			$successAry ['Log_Delete'] = $liblog->INSERT_LOG ( 'WeeklyDiary', 'Delete_Attachment', $liblog->BUILD_DETAIL ( $tmpAry ), 'INTRANET_APP_WEEKLYDIARY_ARTICLE_ATTACHMENT', '' );
		} else {
			// ## check double submission
			$sql = "select ArticleID from INTRANET_APP_WEEKLYDIARY_ARTICLE where HomeworkID='" . $HomeworkID . "' and UserID='" . $parUserID . "'";
			$rs = $this->returnResultSet ( $sql );
			$ArticleIDarray = $rs [0] ['ArticleID'];
			if ($ArticleIDarray == "") {
				$sql = "Insert Into  
							INTRANET_APP_WEEKLYDIARY_ARTICLE
						    (UserID,HomeworkID,Title,Content,SubmitTime,DateModified,LastModifiedBy,Type)
						Values
							('" . $parUserID . "', '" . $HomeworkID . "', '" . $this->Get_Safe_Sql_Query ( $Title ) . "', '" . $this->Get_Safe_Sql_Query ( $Content ) . "',  now(), now(), '" . $parUserID . "',$type)";
				
				$successAry ['insertArticle'] = $this->db_db_query ( $sql );
				if ($successAry ['insertArticle']) {
					$ArticleID = $this->db_insert_id ();
				}
			}
		}
		
		// insert new record
		if ($ArticleID != 0) {
			
			for($i = 0; $i < count ( ( array ) $NewAttachmentAry ); $i ++) {
				$_type = $NewAttachmentAry [$i] ['Type'];
				$_showInList = $NewAttachmentAry [$i] ['Default'];
				$_data = $NewAttachmentAry [$i] ['Data'];
				if ($_showInList != 1)
					$_showInList = 0;
				if ($_showInList == 1) {
					$sql = "Update 
							INTRANET_APP_WEEKLYDIARY_ARTICLE_ATTACHMENT
					Set
							ShowInList = 0,
							DateModified = now(),
							LastModifiedBy = '" . $parUserID . "'
					Where
							ArticleID = '" . $ArticleID . "'
					";
					$successAry ['Update_Attachment_ShowInList1'] = $this->db_db_query ( $sql );
				}
				if ($_type == 1) {
					// image
					$sql = "Insert Into INTRANET_APP_WEEKLYDIARY_ARTICLE_ATTACHMENT (ArticleID, Type, DateModified, LastModifiedBy,UserID,ShowInList)
						Values ('" . $ArticleID . "', '" . $_type . "',now(), '" . $parUserID . "', '" . $parUserID . "', '" . $_showInList . "')";
				} else if ($_type == 2) {
					// url
					// add http if doesn`t has
					if ($ret = parse_url ( $_data )) {
						if (! isset ( $ret ["scheme"] )) {
							$_data = "http://{$_data}";
						}
					}
					$meta_tags = getUrlData ( $_data, false );
					
					// title part
					$title = $meta_tags ["metaProperties"] ["og:title"] ["value"] != "" ? $meta_tags ["metaProperties"] ["og:title"] ["value"] : $meta_tags ["title"];
					$title = trim ( $title, "'/" );
					if ($title == "") {
						$url_info = parse_url ( $_data );
						$title = $url_info ['host'];
					}
					
					// description part
					$description = $meta_tags ["metaProperties"] ["og:description"] ["value"] != "" ? $meta_tags ["metaProperties"] ["og:description"] ["value"] : $meta_tags ["metaTags"] ["description"] ["value"];
					$description = trim ( $description, "'/" );
					// image part
					$image = $meta_tags ["metaProperties"] ["og:image"] ["value"];
					
					$sql = "Insert Into INTRANET_APP_WEEKLYDIARY_ARTICLE_ATTACHMENT (ArticleID, Type, Url,Title, Description,Image,DateModified, LastModifiedBy,UserID,ShowInList)
						Values ('" . $ArticleID . "', '" . $_type . "','" . $this->Get_Safe_Sql_Query ( $_data ) . "','" . $this->Get_Safe_Sql_Query ( $title ) . "','" . $this->Get_Safe_Sql_Query ( $description ) . "','" . $this->Get_Safe_Sql_Query ( $image ) . "',now(), '" . $parUserID . "', '" . $parUserID . "', '" . $_showInList . "')";
				}
				$successAry ['insertAttchmentRecord'] = $this->db_db_query ( $sql );
				if ($successAry ['insertAttchmentRecord']) {
					$_attachmentId = $this->db_insert_id ();
				}
				
				if ($_attachmentId > 0 && $_type == 1) {
					
					$_filePath = $filePathBase . '/file/eClassStudentApp/WeeklyDiary/' . $parUserID . '/' . $_attachmentId . '.jpg';
					
					$_tmpFilePath = $filePathBase . '/file/eClassStudentApp/WeeklyDiary/' . $parUserID . '/';
					if (! file_exists ( $_tmpFilePath )) {
						$successAry ['createRootFolder'] [$_attachmentId] = $lfs->folder_new ( $_tmpFilePath );
					}
					
					$_imageBinary = base64_decode ( $_data );
					$successAry ['uploadFile'] = $lfs->file_write ( $_imageBinary, $_filePath );
					if ($successAry ['uploadFile']) {
						$_dbFilePath = str_replace ( $filePathBase, '', $_filePath );
						$sql = "Update 
										INTRANET_APP_WEEKLYDIARY_ARTICLE_ATTACHMENT 
								Set 
										Image = '" . $this->Get_Safe_Sql_Query ( $_dbFilePath ) . "',
										ImageSmall = '" . $this->Get_Safe_Sql_Query ( $_dbFilePath ) . "',
										DateModified = now(),
										LastModifiedBy = '" . $parUserID . "'
								Where
										AttachmentID = '" . $_attachmentId . "'
								";
						$successAry ['updateAttachmentRecord'] [$_attachmentId] = $this->db_db_query ( $sql );
					}
				}
			}
			
			if ($NewAttachmentAry > 0 && in_array ( false, ( array ) $successAry ['updateAttachmentRecord'] )) {
				$attachmentUpdateSuccess = '0';
			}
		}
		if ($ShowInListID != 0) {
			
			$sql = "Update 
							INTRANET_APP_WEEKLYDIARY_ARTICLE_ATTACHMENT
					Set
							ShowInList = 1,
							DateModified = now(),
							LastModifiedBy = '" . $parUserID . "'
					Where
							AttachmentID = '" . $ShowInListID . "'
					";
			$successAry ['Update_Attachment_ShowInList'] = $this->db_db_query ( $sql );
			if ($successAry ['Update_Attachment_ShowInList']) {
				$sql = "Update 
							INTRANET_APP_WEEKLYDIARY_ARTICLE_ATTACHMENT
					Set
							ShowInList = 0,
							DateModified = now(),
							LastModifiedBy = '" . $parUserID . "'
					Where
							ArticleID = '" . $ArticleID . "' AND 
							AttachmentID <> '" . $ShowInListID . "'
					";
				$successAry ['Update_Attachment_ShowInList2'] = $this->db_db_query ( $sql );
			}
		}
		$returnAry = array ();
		$returnAry ['ArticleID'] = $ArticleID;
		$returnAry ['AttachmentUpdateSuccess'] = $attachmentUpdateSuccess;
		return $returnAry;
	}
	function WS_GetHKUFluAgreementRecord($student_id) {
		$rows_agreement = array ();
		
		$isOK = false;
		try {
			global $intranet_root;
			
			include_once $intranet_root . '/includes/HKUFlu/libHKUFlu_db.php';
			$libHKUFlu_db = new libHKUFlu_db ();
			
			$rows_agreement = $libHKUFlu_db->getAgreementByStudentID ( $student_id );
			
			$isOK = true;
		} catch ( Exception $e ) {
		}
		
		$array_return = array ();
		$array_return ['result'] = $isOK ? 'OK' : 'NOT OK';
		$array_return ['rows_agreement'] = $rows_agreement;
		return $array_return;
	}
	function WS_NewHKUFluAgreementRecord($parent_id, $student_id, $is_agreed, $parent_name) {
		$isOK = false;
		try {
			global $intranet_root;
			
			include_once $intranet_root . '/includes/HKUFlu/libHKUFlu_db.php';
			$libHKUFlu_db = new libHKUFlu_db ();
			
			$libHKUFlu_db->signAgreement ( $parent_id, $student_id, $is_agreed, $parent_name, date ( 'Y-m-d H:i:s' ) );
			$rows_agreement = $libHKUFlu_db->getAgreementByStudentID ( $student_id );
			
			$isOK = true;
		} catch ( Exception $e ) {
		}
		
		$array_return = array ();
		$array_return ['result'] = $isOK ? 'OK' : 'NOT OK';
		$array_return ['IsAgreed'] = $is_agreed;
		$array_return ['rows_agreement'] = $rows_agreement;
		return $array_return;
	}
	function WS_GetHKUFluSickLeaveRecord($student_id) {
		$rows_sick_leave = array ();
		
		$isOK = false;
		try {
			global $intranet_root;
			
			include_once $intranet_root . '/includes/HKUFlu/libHKUFlu_db.php';
			$libHKUFlu_db = new libHKUFlu_db ();
			
			$rows_sick_leave = $libHKUFlu_db->getSickLeaveByStudentIDAfterSignDate ( $student_id );
			
			$isOK = true;
		} catch ( Exception $e ) {
		}
		
		$array_return = array ();
		$array_return ['rows_sick_leave'] = $rows_sick_leave;
		
		// HKUFlu
		$array_return ['result'] = $isOK ? 'OK' : 'NOT OK';
		$hku_returnArray = $this->WS_GetHKUFluAgreementRecord ( $student_id );
		$array_return ['rows_agreement'] = $hku_returnArray ['rows_agreement'];
		
		return $array_return;
	}
	function WS_NewHKUFluSickLeaveRecord($parent_id, $student_id, $eClass_leave_id, $sick_date, $leave_date, $sick_leave_sick, $sick_leave_symptom, $is_not_sick_leave, $from='A') {
		$isOK = false;
		try {
			global $intranet_root;
			
			include_once $intranet_root . '/includes/HKUFlu/libHKUFlu_db.php';
			$libHKUFlu_db = new libHKUFlu_db ();
			
			$array_sick_code = explode ( ';', $sick_leave_sick );
			$other_sick = array_pop ( $array_sick_code );
			
			$array_symptom_code = explode ( ';', $sick_leave_symptom );
			$other_symptom = array_pop ( $array_symptom_code );
			
			if ($is_not_sick_leave == '1') {
				$libHKUFlu_db->addNonSickLeave ( $eClass_leave_id, '', $parent_id, $student_id, $leave_date, $from);
			} else {
				$libHKUFlu_db->addSickLeave ( $eClass_leave_id, '', $parent_id, $student_id, $sick_date, $leave_date, $array_symptom_code, $other_symptom, $array_sick_code, $other_sick, $from);
			}
			
			$isOK = true;
		} catch ( Exception $e ) {
		}
		
		$array_return = array ();
		$array_return ['result'] = $isOK ? 'OK' : 'NOT OK';
		return $array_return;
	}
	function WS_IsHKUFluRecordExists($student_id, $leave_date) {
		$exists = 'NO';
		
		$isOK = false;
		try {
			global $intranet_root;
			
			include_once $intranet_root . '/includes/HKUFlu/libHKUFlu_db.php';
			$libHKUFlu_db = new libHKUFlu_db ();
			
			$exists = 'NO';
			
			if (count ( $libHKUFlu_db->getSickLeaveByStudentIDAndDate ( array (
					$student_id 
			), $leave_date ) ) > 0) {
				$exists = 'YES';
			}
			
			$isOK = true;
		} catch ( Exception $e ) {
		}
		
		$array_return = array ();
		$array_return ['result'] = $isOK ? 'OK' : 'NOT OK';
		$array_return ['Exists'] = $exists;
		return $array_return;
	}
	function WS_GetPL2PortalUserInfoWithAllCourses($MemberType) {
		global $UserID; // This will init at webserviceapi/index.php
		global $eclass_db, $intranet_db;
		
		$recordType = ($MemberType == 'T') ? USERTYPE_STAFF : USERTYPE_STUDENT;
		$userInfo = array ();
		/* Get UserInfo */
		$sql = "SELECT
	        EnglishName,
	        ChineseName,
	        RecordType,
	        UserLogin
        FROM
	        {$intranet_db}.INTRANET_USER 
        WHERE
	        UserID = '{$UserID}'";
		$result = $this->returnResultSet ( $sql );
		if ($recordType != $result [0] ['RecordType']) {
			return '102';
		}
		include_once ("libuser.php");
		$lu = new libuser ();
		$userInfo ['EnglishName'] = intranet_undo_htmlspecialchars ( $result [0] ['EnglishName'] );
		$userInfo ['ChineseName'] = intranet_undo_htmlspecialchars ( $result [0] ['ChineseName'] );
		$userInfo ['RecordType'] = $result [0] ['RecordType'];
		$userPhotoAry = $lu->GET_USER_PHOTO ( $result [0] ['UserLogin'], $UserID, false );
		$userInfo ['PhotoLink'] = $userPhotoAry [1];
		
		/* User Access Right */
		$sql = "SELECT
					count(*)
				FROM
					ROLE_MEMBER rm
				INNER JOIN
					ROLE_RIGHT rr
					on
						(rm.RoleID = rr.RoleID
						and
						rm.UserID = '".$UserID."')
				WHERE
					rr.FunctionName = 'eLearning-PowerLesson2'";
		$result = current($this->returnVector ( $sql ));
		$userInfo ['isAdmin'] = $result ? 1 : 0;
		
		/* Update AuthToken */
		$sql = "SELECT 
	        uc.user_course_id as userCourseId
        FROM
	        {$eclass_db}.course c
	    INNER JOIN
	        {$eclass_db}.user_course uc
	    ON
	        c.course_id = uc.course_id
	    AND
	        uc.intranet_user_id = '{$UserID}'
	    AND
		(
			uc.status is NULL 
		OR
			uc.status NOT IN ('deleted')
		)
        AND
        (
            uc.auth_token IS NULL
        OR
            uc.auth_token = ''
        )
	    WHERE
	        c.RoomType = 0
        AND 
            uc.memberType IN ('T', 'S')";
		$userCourseIdArr = $this->returnVector ( $sql );
		$userCourseIdList = implode ( "','", $userCourseIdArr );
		
		$sql = "UPDATE
	        {$eclass_db}.user_course
        SET
	        auth_token = UUID()
        WHERE
	        user_course_id IN ('{$userCourseIdList}')";
		$result = $this->db_db_query ( $sql );
		
		$sql = "SELECT
            uc.course_id,
            uc.user_id,
            uc.auth_token
        FROM
            {$eclass_db}.user_course uc
        WHERE
	        user_course_id IN ('{$userCourseIdList}')";
		$rs = $this->returnResultSet ( $sql );
		foreach ( $rs as $r ) {
			$courseDb = classNamingDB ( $r ['course_id'] );
			$sql = "UPDATE
	            {$courseDb}.usermaster
            SET
	            auth_token = '{$r['auth_token']}'
            WHERE
                user_id = '{$r['user_id']}'";
			$result = $this->db_db_query ( $sql );
		}
		
		/* Get Courses */
		$sql = "
	        SELECT
	    	    uc.course_id as id,
	    	    uc.auth_token as authToken,
	    	    uc.user_id as userId,
	    	    uc.memberType as memberType
	        FROM
	           {$eclass_db}.user_course uc
	        WHERE
		       uc.memberType IN ('S', 'T') AND uc.intranet_user_id = '{$UserID}'
		";
		$result = $this->returnResultSet($sql);
		
		$coursesInfo = array();
		if(count($result) > 0){
			$userCoursesInfo = array();
			for($i=0; $i<count($result); $i++){
				$userCoursesInfo[$result[$i]['id']]['authToken'] = $result[$i]['authToken'];
				$userCoursesInfo[$result[$i]['id']]['userId'] = $result[$i]['userId'];
				$userCoursesInfo[$result[$i]['id']]['memberType'] = $result[$i]['memberType'];
			}
	
			$sql = "
		        SELECT
	        	    c.course_id as id,
	        	    c.course_name as name,
	        	    c.language as language
		       FROM
		           {$eclass_db}.course c
				WHERE
	               c.RoomType = 0 AND c.course_id IN 
						(SELECT uc.course_id FROM {$eclass_db}.user_course uc WHERE c.course_id = uc.course_id)
	 			ORDER BY
	               c.course_name
	        ";
		    $result = $this->returnResultSet($sql);
		    
		    for($i=0; $i<count($result); $i++){
		       $result[$i]['name'] = intranet_undo_htmlspecialchars($result[$i]['name']);
				
			   if($userInfo['RecordType'] == USERTYPE_STAFF || isset($userCoursesInfo[$result[$i]['id']])){
		           $coursesInfo[] = array(
		               "id" => $result[$i]['id'],
		               "name" => $result[$i]['name'],
		               "language" => $result[$i]['language'],
		               "authToken" => $userCoursesInfo[$result[$i]['id']]['authToken'],
		               "userId" => $userCoursesInfo[$result[$i]['id']]['userId'],
		               "memberType" => $userCoursesInfo[$result[$i]['id']]['memberType']
		           );
			   }
		    }
		}
		
		return array (
				'UserInfo' => $userInfo,
				'CourseInfo' => $coursesInfo 
		);
	}
	function WS_GetPL2PortalUserInfo($MemberType) {
		global $UserID; // This will init at webserviceapi/index.php
		global $eclass_db, $intranet_db;
		
		$recordType = ($MemberType == 'T') ? USERTYPE_STAFF : USERTYPE_STUDENT;
		$userInfo = array ();
		/* Get UserInfo */
		$sql = "SELECT
	        EnglishName,
	        ChineseName,
	        RecordType,
	        UserLogin
        FROM
	        {$intranet_db}.INTRANET_USER 
        WHERE
	        UserID = '{$UserID}'";
		$result = $this->returnResultSet ( $sql );
		if ($recordType != $result [0] ['RecordType']) {
			return '102';
		}
		include_once ("libuser.php");
		$lu = new libuser ();
		$userInfo ['EnglishName'] = intranet_undo_htmlspecialchars ( $result [0] ['EnglishName'] );
		$userInfo ['ChineseName'] = intranet_undo_htmlspecialchars ( $result [0] ['ChineseName'] );
		$userInfo ['RecordType'] = $result [0] ['RecordType'];
		$userPhotoAry = $lu->GET_USER_PHOTO ( $result [0] ['UserLogin'], $UserID, false );
		$userInfo ['PhotoLink'] = $userPhotoAry [1];

		/* User Access Right */
		$sql = "SELECT
					count(*)
				FROM
					ROLE_MEMBER rm
				INNER JOIN
					ROLE_RIGHT rr
					on
						(rm.RoleID = rr.RoleID
						and
						rm.UserID = '".$UserID."')
				WHERE
					rr.FunctionName = 'eLearning-PowerLesson2'";
		$result = current($this->returnVector ( $sql ));
		$userInfo ['isAdmin'] = $result ? 1 : 0;
		
		/* Update AuthToken */
		$sql = "SELECT 
	        uc.user_course_id as userCourseId
        FROM
	        {$eclass_db}.course c
	    INNER JOIN
	        {$eclass_db}.user_course uc
	    ON
	        c.course_id = uc.course_id
	    AND
	        uc.intranet_user_id = '{$UserID}'
	    AND
		(
			uc.status is NULL 
		OR
			uc.status NOT IN ('deleted')
		)
        AND
        (
            uc.auth_token IS NULL
        OR
            uc.auth_token = ''
        )
	    WHERE
	        c.RoomType = 0
        AND 
            uc.memberType IN ('T', 'S')";
		$userCourseIdArr = $this->returnVector ( $sql );
		$userCourseIdList = implode ( "','", $userCourseIdArr );
		
		$sql = "UPDATE
	        {$eclass_db}.user_course
        SET
	        auth_token = UUID()
        WHERE
	        user_course_id IN ('{$userCourseIdList}')";
		$result = $this->db_db_query ( $sql );
		
		$sql = "SELECT
            uc.course_id,
            uc.user_id,
            uc.auth_token
        FROM
            {$eclass_db}.user_course uc
        WHERE
	        user_course_id IN ('{$userCourseIdList}')";
		$rs = $this->returnResultSet ( $sql );
		foreach ( $rs as $r ) {
			$courseDb = classNamingDB ( $r ['course_id'] );
			$sql = "UPDATE
	            {$courseDb}.usermaster
            SET
	            auth_token = '{$r['auth_token']}'
            WHERE
                user_id = '{$r['user_id']}'";
			$result = $this->db_db_query ( $sql );
		}
		
		/* Get Courses */
		$sql = "SELECT
	        c.course_id as id,
	        c.course_name as name,
	        c.language as language,
	        uc.auth_token as authToken,
	        uc.user_id as userId,
	        uc.memberType as memberType
	    FROM
	        {$eclass_db}.course c
	    INNER JOIN
	        {$eclass_db}.user_course uc
	    ON
	        c.course_id = uc.course_id
	    AND
	        uc.intranet_user_id = '{$UserID}'
	    WHERE
	        c.RoomType = 0
        AND 
            uc.memberType IN ('T', 'S')
        ORDER BY
            c.course_name";
		$coursesInfo = $this->returnResultSet ( $sql );
		for($i = 0; $i < count ( $coursesInfo ); $i ++) {
			$coursesInfo [$i] ['name'] = intranet_undo_htmlspecialchars ( $coursesInfo [$i] ['name'] );
		}
		
		return array (
				'UserInfo' => $userInfo,
				'CourseInfo' => $coursesInfo 
		);
	}
	function WS_GetPL2LessonPlanUsersUdid($courseId, $lessonIdentifier){
	    global $intranet_root;
	    
	    include_once ('libuser.php');
	    
	    $apiUri = '/lesson/' . $lessonIdentifier . '/eclassMdm';
	    $getLessonDataApi = __powerLessonApiCaller ( $apiUri, 'GET', $courseId, false, array(), null, true);
	    
	    if($getLessonDataApi['result']){
	        # Lesson
	        $content['Lesson']             = array();
	        $content['Lesson']['UniqueID'] = $getLessonDataApi['apiResponse']['Lesson']['UniqueID'];
	        $content['Lesson']['Title']    = $getLessonDataApi['apiResponse']['Lesson']['Title']? $getLessonDataApi['apiResponse']['Lesson']['Title'] : 'Untitled lesson plan';
	        
	        # Teacher
	        $objTeacherUser = new libuser($getLessonDataApi['apiResponse']['Teacher']['IntranetUserID']);
	        $content['Teacher']          = array();
	        $content['Teacher']['Udid']  = $getLessonDataApi['apiResponse']['Teacher']['Udid'];
	        $content['Teacher']['Name']  = $objTeacherUser->UserName();
	        $content['Teacher']['Photo'] = (is_file($intranet_root.$objTeacherUser->PhotoLink)? $objTeacherUser->PhotoLink:"");
	        
	        #Students
	        $content['Students'] = array();
	        for($i=0; $i<count($getLessonDataApi['apiResponse']['Students']); $i++){
	            $objStudentUser = new libuser($getLessonDataApi['apiResponse']['Students'][$i]['IntranetUserID']);
	            $content['Students'][$i]           = array();
	            $content['Students'][$i]['Udid']   = $getLessonDataApi['apiResponse']['Students'][$i]['Udid'];
	            $content['Students'][$i]['Name']   = $objStudentUser->UserName();
	            $content['Students'][$i]['Photo']  = (is_file($intranet_root.$objStudentUser->PhotoLink)? $objStudentUser->PhotoLink:"");
	            $content['Students'][$i]['Gender'] = $objStudentUser->Gender;
	        }
	        
	        $returnContent = $content;
	    } else {
	        $returnContent = 806; // Lesson does not exist.
	    }
	    return $returnContent;
	}
	function WS_RenewAuthToken($CourseId, $UserId) {
		global $eclass_db;
		
		/* Update AuthToken */
		$sql = "SELECT
	        uc.user_course_id as userCourseId
	    FROM
	        {$eclass_db}.user_course uc
	    WHERE
	        uc.course_id = '{$CourseId}'
	    AND
	        uc.user_id = '{$UserId}'
	    AND
	    (
	        uc.status is NULL
	    OR
	        uc.status NOT IN ('deleted')
	    )
	    AND
	    (
	        uc.auth_token IS NULL
	    OR
	        uc.auth_token = ''
	    )";
		$userCourseIdArr = $this->returnVector ( $sql );
		$userCourseIdList = implode ( "','", $userCourseIdArr );
		
		if (count ( $userCourseIdArr ) == 0) {
			return array ();
		}
		
		$sql = "UPDATE
	        {$eclass_db}.user_course
	    SET
	        auth_token = UUID()
	    WHERE
	        user_course_id IN ('{$userCourseIdList}')";
		$result = $this->db_db_query ( $sql );
		
		$sql = "SELECT
    	    uc.course_id,
    	    uc.user_id,
    	    uc.auth_token
	    FROM
	        {$eclass_db}.user_course uc
	    WHERE
	        user_course_id IN ('{$userCourseIdList}')";
		$rs = $this->returnResultSet ( $sql );
		foreach ( $rs as $r ) {
			$courseDb = classNamingDB ( $r ['course_id'] );
			$sql = "UPDATE
	            {$courseDb}.usermaster
	        SET
	            auth_token = '{$r['auth_token']}'
	        WHERE
	            user_id = '{$r['user_id']}'";
			$result = $this->db_db_query ( $sql );
		}
		
		return array ();
	}
	function WS_RemoveAuthToken($CourseId, $UserId, $AuthToken) {
		global $eclass_db;
		
		/* Update AuthToken in user_course */
		$sql = "UPDATE
	        {$eclass_db}.user_course uc
	    SET
	        auth_token = NULL
	    WHERE
	        uc.course_id = '{$CourseId}'
	    AND
	        uc.user_id = '{$UserId}'
	    AND
	        uc.auth_token = '{$AuthToken}'";
		$result = $this->db_db_query ( $sql );
		
		/* Update AuthToken in usermaster */
		$courseDb = classNamingDB ( $CourseId );
		$sql = "UPDATE
            {$courseDb}.usermaster
        SET
            auth_token = NULL
        WHERE
            user_id = '{$UserId}'
        AND
            auth_token = '{$AuthToken}'";
		$result = $this->db_db_query ( $sql );
		
		return array ();
	}
	function WS_RenewSessionLastUpdated() {
		global $UserID; // This will init at webserviceapi/index.php
		$sql = "UPDATE INTRANET_USER SET SessionLastUpdated=NOW() WHERE UserID='{$UserID}'";
		$result = $this->db_db_query ( $sql );
		return array ();
	}
	function WS_updatePaymentCallbackFromCentralServer($parFromServer, $parPaymentId, $parSp, $parPayerUserId, $parPaymentStatus, $parUniqueCode, $parSpTxNo, $parErrorCode, $parNoticeID='', $parNoticePaymentID='', $parNoticeReplyAnswer='', $parStudentId='', $parPaymentAmount='', $parDateInput='', $parModuleName='', $parModuleTransactionID='', $OriginalAmount='', $NewAmount='', $AdministrativeFeeFixAmount='', $AdministrativeFeePercentageAmount='') {
		global $intranet_root, $sys_custom;
		
		include_once ($intranet_root . "/includes/libpayment.php");
		include_once($intranet_root."/includes/libpos.php");
		$libpayment = new libpayment ();
		$libpos = new libpos();

		// debug_pr('$parFromServer = '.$parFromServer);
		// debug_pr('$parPaymentId = '.$parPaymentId);
		// debug_pr('$parSp = '.$parSp);
		// debug_pr('$parPayerUserId = '.$parPayerUserId);
		// debug_pr('$parPaymentStatus = '.$parPaymentStatus);
		// debug_pr('$parUniqueCode = '.$parUniqueCode);
		// debug_pr('$parSpTxNo = '.$parSpTxNo);
		// debug_pr('$parErrorCode = '.$parErrorCode);
		
		$resultAry = array ();
		$resultAry ['status'] = '1';
		$payment_id_ary = array();

		$is_notice_payment = false;
		if($parNoticeID != '' && $parNoticePaymentID != ''){
			include_once($intranet_root."/includes/libnotice.php");
			$lnotice = new libnotice($parNoticeID);
			$is_notice_payment = true;
			$payment_id_ary = explode(",", $parNoticePaymentID);
			$notice_id_field = ',NoticeID';
			$notice_id_field_value = ",'".$parNoticeID."'";
		}elseif($parPaymentId == -2) {
			if($parModuleName == 'ePOS') {
				$payment_id_ary = array($parPaymentId);
			}
		} else {
			$payment_id_ary = array($parPaymentId);
		}
		
		$transaction_time = $parDateInput != ''? $parDateInput : date("Y-m-d H:i:s");
		
		$merchant_account_id = '';
		$payment_success = false;
		for($i=0;$i<count($payment_id_ary);$i++)
		{
			$payment_id = $payment_id_ary[$i];
			
			if($payment_id == -2) {
				$payment_record = $libpos->getAlipayTransactionByID($parModuleTransactionID);
				if (count ( $payment_record ) == 0) {
					// payment student record cannot be found
					$sql = "SELECT * FROM PAYMENT_TNG_TRANSACTION where PaymentID='$payment_id' AND SpTxNo='$parSpTxNo'";
					$existing_record = $this->returnResultSet($sql);
					if(count($existing_record)==0) {
						$updated_by = isset ($_SESSION['UserID']) ? $_SESSION['UserID'] : 'NULL';
						$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION (ModuleName, ModuleTransactionID, StudentID, FromServer,PaymentID,Sp,PayerUserID,PaymentType,PaymentStatus,OrderNo,SpTxNo,PaymentAmount,RebateAmount,NetPaymentAmount,Remark,ErrorCode,InputDate,ModifiedDate,UpdatedBy $notice_id_field) 
						VALUES ('$parModuleName','$parModuleTransactionID','$parStudentId','$parFromServer','$payment_id','$parSp','$parPayerUserId','','$parPaymentStatus','$parUniqueCode','$parSpTxNo','" . $parPaymentAmount . "','0','" . $parPaymentAmount . "','','$parErrorCode','$transaction_time','$transaction_time',$updated_by $notice_id_field_value)";
						$this->db_db_query($sql);

						$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION_AMOUNT_DETAIL (SpTxNo,OriginalAmount,NewAmount,AdministrativeFeeFixAmount,AdministrativeFeePercentageAmount)
						VALUES ('$parSpTxNo', '$OriginalAmount', '$NewAmount','$AdministrativeFeeFixAmount', '$AdministrativeFeePercentageAmount')";
						$this->db_db_query($sql);
					}
					$resultAry['status'] = '0';
					continue;
				} else {
					$student_id = $payment_record['StudentID'];

					if($parModuleName == 'ePOS') {
						include_once($intranet_root . "/includes/libgeneralsettings.php");
						$GeneralSetting = new libgeneralsettings();
						$AlipayMerchantAccountID = $GeneralSetting->Get_General_Setting('ePOS', array('"AlipayMerchantAccountID"'));
						$merchant_account_id = $AlipayMerchantAccountID;
					}
				}
			} elseif($payment_id != -1){
				$sql = "SELECT PaymentID,ItemID,StudentID,Amount,SubsidyAmount,RecordStatus FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID='$payment_id'";
				$payment_record = $this->returnResultSet ( $sql );
				
				if (count ( $payment_record ) == 0) {
					// payment student record cannot be found
					$sql = "SELECT * FROM PAYMENT_TNG_TRANSACTION where PaymentID='$payment_id' AND SpTxNo='$parSpTxNo'";
					$existing_record = $this->returnResultSet($sql);
					if(count($existing_record)==0) {
						$updated_by = isset ($_SESSION['UserID']) ? $_SESSION['UserID'] : 'NULL';
						$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION (ModuleName, ModuleTransactionID, StudentID, FromServer,PaymentID,Sp,PayerUserID,PaymentType,PaymentStatus,OrderNo,SpTxNo,PaymentAmount,RebateAmount,NetPaymentAmount,Remark,ErrorCode,InputDate,ModifiedDate,UpdatedBy $notice_id_field) 
						VALUES ('$parModuleName','$parModuleTransactionID','$parStudentId','$parFromServer','$payment_id','$parSp','$parPayerUserId','','$parPaymentStatus','$parUniqueCode','$parSpTxNo','" . $parPaymentAmount . "','0','" . $parPaymentAmount . "','','$parErrorCode','$transaction_time','$transaction_time',$updated_by $notice_id_field_value)";
						$this->db_db_query($sql);

						$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION_AMOUNT_DETAIL (SpTxNo,OriginalAmount,NewAmount,AdministrativeFeeFixAmount,AdministrativeFeePercentageAmount)
						VALUES ('$parSpTxNo', '$OriginalAmount', '$NewAmount','$AdministrativeFeeFixAmount', '$AdministrativeFeePercentageAmount')";
						$this->db_db_query($sql);
					}
					$resultAry['status'] = '0';
					continue;
					//return $resultAry;
				}
				
				//if ($payment_record [0] ['RecordStatus'] == 1) {
					// already paid ?
				//	$resultAry ['status'] = '0';
				//	return $resultAry;
				//}
				
				
				$item_id = $payment_record[0]['ItemID'];
				$student_id = $payment_record[0]['StudentID'];
				$payment_amount = $payment_record[0]['Amount'];

				$sql = "select a.AccountID from PAYMENT_MERCHANT_ACCOUNT as a
						LEFT JOIN PAYMENT_PAYMENT_ITEM_PAYMENT_GATEWAY b ON a.AccountID=b.MerchantAccountID
						where a.ServiceProvider='".strtoupper($parSp)."' and b.ItemID=$item_id";
				$merchant_record = $this->returnResultSet($sql);
				if (count ( $merchant_record ) > 0) {
					$merchant_account_id = $merchant_record[0]['AccountID'];
				}
			} elseif($payment_id == -1) {
				$student_id = $parStudentId;
				$sql = "select a.AccountID from PAYMENT_MERCHANT_ACCOUNT as a
						LEFT JOIN PAYMENT_PAYMENT_ITEM_PAYMENT_GATEWAY b ON a.AccountID=b.MerchantAccountID
						where a.ServiceProvider='".strtoupper($parSp)."' and a.RecordStatus=1";
				$merchant_record = $this->returnResultSet($sql);
				if (count ( $merchant_record ) > 0) {
					$merchant_account_id = $merchant_record[0]['AccountID'];
				}
			}
			
			$updated_by = isset ( $_SESSION['UserID'] ) ? $_SESSION['UserID'] : 'NULL';
			
			$sql = "SELECT * FROM PAYMENT_TNG_TRANSACTION where PaymentID='$payment_id' AND SpTxNo='$parSpTxNo'";
			$existing_record = $this->returnResultSet($sql);
			
			$success = true;
			if(count($existing_record)==0)
			{
				$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION (ModuleName, ModuleTransactionID, StudentID,FromServer,PaymentID,Sp,PayerUserID,PaymentType,PaymentStatus,OrderNo,SpTxNo,PaymentAmount,RebateAmount,NetPaymentAmount,Remark,ErrorCode,InputDate,ModifiedDate,UpdatedBy $notice_id_field) 
						VALUES ('$parModuleName','$parModuleTransactionID','$student_id','$parFromServer','$payment_id','$parSp','$parPayerUserId','','$parPaymentStatus','$parUniqueCode','$parSpTxNo','".($payment_amount? $payment_amount : $parPaymentAmount)."','0','".($payment_amount? $payment_amount : $parPaymentAmount)."','','$parErrorCode','$transaction_time','$transaction_time',$updated_by $notice_id_field_value)";
				$this->db_db_query ( $sql );

				$transactionID = $this->db_insert_id();

				$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION_AMOUNT_DETAIL (SpTxNo,OriginalAmount,NewAmount,AdministrativeFeeFixAmount,AdministrativeFeePercentageAmount)
						VALUES ('$parSpTxNo', '$OriginalAmount', '$NewAmount','$AdministrativeFeeFixAmount', '$AdministrativeFeePercentageAmount')";
				$this->db_db_query($sql);

				if($payment_id == -1){
					$lc_sp = strtolower($parSp);
					if($lc_sp == 'alipay'){
						$creditRecordType = 9;
					}else if($lc_sp == 'fps'){
						$creditRecordType = 10;
					}else if($lc_sp == 'tapandgo'){
						$creditRecordType = 11;
					}else if($lc_sp == 'visamaster'){
						$creditRecordType = 12;
					}else if($lc_sp == 'wechat'){
						$creditRecordType = 13;
					}else if($lc_sp == 'alipaycn'){
						$creditRecordType = 14;
					}else{
						$creditRecordType = 8;
					}
					//$creditRecordType = strtolower($parSp)=='alipay'? 9 : 8; // 9=alipay; 8=tng
					
					$result = $libpayment->addValue($parPaymentAmount, $parStudentId, $transaction_time, '', $creditRecordType, $transactionID);
					$payment_success = true;
				}
			}
			if ($payment_id != -1 && $success && in_array ( $parPaymentStatus, array (
					"1",
					"Success",
					"success" 
			) )) {
				if($payment_id == -2) {
					if($parModuleName == 'ePOS') {
						$libpos->actionAfterPaidByAlipay($parModuleTransactionID, 1, $parSpTxNo);
					}
				} else {
				if($is_notice_payment){
					$libpayment->Process_Notice_Payment_Item($item_id,$student_id,$payment_amount,$lnotice->DebitMethod,$AllowNegativeBalance=true, $DoNotDeductBalance=true);		
				}else{
					$log_id = $libpayment->Paid_Payment_Item( $item_id, $student_id, $payment_id, $___AllowNegativeBalance = true, $___ReturnTransactionLogID = true, '', true);
				}
			}
				$payment_success = true;
			} else {
				//fail
				if($payment_id == -2) {
					if ($parModuleName == 'ePOS') {
						$libpos->actionAfterPaidByAlipay($parModuleTransactionID, -1, '');
					}
				}
			}
		}
		
		if($payment_success) {
			if ($merchant_account_id != '') {
				$year = date("Y");
				$month = date("m");
				$sql = "update PAYMENT_MERCHANT_ACCOUNT_QUOTA set UsedQuota = UsedQuota + 1, UsedQuotaDate=now() where Year='$year' AND Month='$month' AND AccountID='$merchant_account_id'";
				$this->db_db_query($sql);
			}
		}
		
		if($is_notice_payment && $resultAry['status']=='0'){
			// if payment failed, unsign the notice for user to pay again
			/*$sql = "UPDATE INTRANET_NOTICE_REPLY SET Answer=NULL,
	                RecordType=NULL, RecordStatus=0, SignerID=NULL, DateModified=now() 
	                WHERE NoticeID = '$parNoticeID' AND StudentID = '$student_id'";
	        $lnotice->db_db_query($sql);*/
		}
		if($is_notice_payment && $resultAry['status'] == '1'){
			// if payment is success, sign the notice
			$sql = "UPDATE INTRANET_NOTICE_REPLY SET RecordStatus=2 WHERE NoticeID = '$parNoticeID' AND StudentID = '$student_id' AND (RecordStatus<>2 OR RecordStatus IS NULL)";
	        $lnotice->db_db_query($sql);
		}
		
		return $resultAry;
	}
	
	function WS_GetPaymentItemMerchantInfo($parFromServer,$parPaymentID, $parServiceProvider, $parModuleName, $parModuleTransactionID)
	{
		global $intranet_root, $sys_custom;
		
		include_once($intranet_root."/includes/libpayment.php");
		$libpayment = new libpayment();

		$records = array();
		if($parPaymentID == -2) {
			if($parModuleName == 'ePOS') {
				include_once($intranet_root."/includes/libgeneralsettings.php");
				$GeneralSetting = new libgeneralsettings();
				$AlipayMerchantAccountID = $GeneralSetting->Get_General_Setting('ePOS',array('"AlipayMerchantAccountID"'));
				if($AlipayMerchantAccountID['AlipayMerchantAccountID']) {
					$records = $libpayment->getMerchantAccounts(array("AccountID" => $AlipayMerchantAccountID['AlipayMerchantAccountID'],'ServiceProvider'=>$parServiceProvider));
				}
			}
		} else if($parPaymentID == -1) {
			$records = $libpayment->getMerchantAccounts(array("RecordStatus" => 1,'ServiceProvider'=>$parServiceProvider));
		} else {
		$para = array('PaymentID'=>$parPaymentID,'select_merchant_info'=>true);
		if($sys_custom['ePayment']['MultiPaymentGateway']) {
			$para['ServiceProvider'] = $parServiceProvider;
		}
		$records = $libpayment->getPaymentItemStudentRecords($para);
		}

		$returnAry = array();
		if(count($records)>0){
			$record = $records[0];
			if($record['AccountID'] == ''){
				if($libpayment->isTNGDirectPayEnabled()){
					// it is OK for TNG without Merchant account assigned to payment item, just use the first merchant account on central server
					$returnAry['AccountID'] = '';
					$returnAry['ServiceProvider'] = 'TNG';
					$returnAry['AccountName'] = '';
					$returnAry['MerchantAccount'] = '';
					$returnAry['MerchantUID'] = '';
				}else{
					$returnAry['Error'] = 1;
				}
			}else{
				$returnAry['AccountID'] = $record['AccountID'];
				$returnAry['ServiceProvider'] = $record['ServiceProvider'];
				$returnAry['AccountName'] = $record['AccountName'];
				$returnAry['MerchantAccount'] = $record['MerchantAccount'];
				$returnAry['MerchantUID'] = $record['MerchantUID'];
				$returnAry['AdministrativeFeeFixAmount'] = 0;
				$returnAry['AdministrativeFeePercentageAmount'] = 0;

				$fee_amount_checked = false;
				if($record['AdministrativeFeeFixAmount'] != '' && $record['AdministrativeFeeFixAmount'] > 0) {
					$fee_amount_checked = true;
					$returnAry['AdministrativeFeeFixAmount'] = $record['AdministrativeFeeFixAmount'];
				} else if($record['AdministrativeFeePercentageAmount'] != '' && $record['AdministrativeFeePercentageAmount'] > 0) {
					$fee_amount_checked = true;
					$returnAry['AdministrativeFeePercentageAmount'] = $record['AdministrativeFeePercentageAmount'];
				}

				$count_fee = false;
				if($fee_amount_checked) {
					if ($record['ServiceProvider'] == 'FPS') {
						$year = date("Y");
						$month = date("m");
						$sql = "SELECT * FROM PAYMENT_MERCHANT_ACCOUNT_QUOTA WHERE Year='$year' AND Month='$month' AND AccountID='" . $record['AccountID'] . "'";
						$rs_quota = $libpayment->returnArray($sql);
						if ($rs_quota) {
							$rs_quota = $rs_quota[0];
							if ($rs_quota['Quota'] == 0) {
								$count_fee = true;
							} elseif ($rs_quota['Quota'] > 0) {
								if ($rs_quota['UsedQuota'] >= $rs_quota['Quota']) {
									$count_fee = true;
								}
							}
						} else {
							$count_fee = true;
						}
					} else {
						$count_fee = true;
					}
				}

				$returnAry['CountFee'] = $count_fee;
			}
		}else{
			$returnAry['Error'] = 1;
		}
		
		return $returnAry;
	}
	
	function WS_LoginCourses() {
		global $UserID, $eclass_db;
		
		include_once ("libuser.php");
		$user = new libuser ( $UserID );
		
		$sql = "SELECT
	               uc.user_course_id, uc.course_id, uc.user_id, uc.auth_token
	            FROM
	               " . $eclass_db . ".user_course AS uc INNER JOIN
	               " . $eclass_db . ".course AS c ON uc.course_id = c.course_id
	            WHERE
	               c.RoomType = 0 AND
	               uc.user_email = '" . $user->UserEmail . "' AND
	               (
	                   uc.status is NULL OR
	                   uc.status NOT IN ('deleted')
	               )";
		$result = $this->returnArray ( $sql );
		
		$resultAry = array ();
		
		for($i = 0; $i < count ( $result ); $i ++) {
			$userCourseId = $result [$i] ['user_course_id'];
			$courseId = $result [$i] ['course_id'];
			$userId = $result [$i] ['user_id'];
			$authToken = $result [$i] ['auth_token'];
			
			if (! $authToken) {
				$sql = "UPDATE
	                       " . $eclass_db . ".user_course
	                    SET
	                       auth_token = UUID()
	                    WHERE
	                       user_course_id = '" . $userCourseId . "'";
				$this->db_db_query ( $sql );
				
				$sql = "SELECT
	                       auth_token
	                    FROM
	                       " . $eclass_db . ".user_course
	                    WHERE
	                       user_course_id = '" . $userCourseId . "'";
				$authToken = current ( $this->returnVector ( $sql ) );
				
				$sql = "UPDATE
	                       " . classNamingDB ( $courseId ) . ".usermaster
	                    SET
	                       auth_token = '" . $authToken . "'
	                    WHERE
	                       user_id = '" . $userId . "'";
				$this->db_db_query ( $sql );
			}
			
			$resultAry [$courseId] = $authToken;
		}
		
		return $resultAry;
	}
	function WS_LogoutCourses() {
		global $UserID, $eclass_db;
		
		include_once ("libuser.php");
		$user = new libuser ( $UserID );
		
		$sql = "SELECT
	               uc.user_course_id, uc.course_id, uc.user_id
	            FROM
	               " . $eclass_db . ".user_course AS uc INNER JOIN
	               " . $eclass_db . ".course AS c ON uc.course_id = c.course_id
	            WHERE
	               c.RoomType = 0 AND
	               uc.user_email = '" . $user->UserEmail . "' AND
	               (
	                   uc.status is NULL OR
	                   uc.status NOT IN ('deleted')
	               )";
		$result = $this->returnArray ( $sql );
		
		$resultAry = array ();
		
		for($i = 0; $i < count ( $result ); $i ++) {
			$userCourseId = $result [$i] ['user_course_id'];
			$courseId = $result [$i] ['course_id'];
			$userId = $result [$i] ['user_id'];
			
			$sql = "UPDATE
	                   " . $eclass_db . ".user_course
	                SET
	                   auth_token = NULL
	                WHERE
	                   user_course_id = '" . $userCourseId . "'";
			$this->db_db_query ( $sql );
			
			$sql = "UPDATE
	                   " . classNamingDB ( $courseId ) . ".usermaster
	                SET
	                   auth_token = NULL
	                WHERE
	                   user_id = '" . $userId . "'";
			$this->db_db_query ( $sql );
		}
		
		return $resultAry;
	}
	function WS_LoginCourseByLessonPin($parCourseId, $parLessonId, $parUserId, $parPin) {
		global $eclass_db;
		
		/* Validate Pin */
		$requestData = array (
				'courseId' => $parCourseId,
				'lessonId' => $parLessonId,
				'userId' => $parUserId 
		);
		$apiUri = '/lesson/' . $parPin . '/accessRight';
		$checkAccessRightApi = __powerLessonApiCaller ( $apiUri, 'POST', $parPin, true, $requestData );
		
		/* Login Course */
		$resultAry = array ();
		
		if ($checkAccessRightApi ['result']) {
			$sql = "UPDATE
	                   " . $eclass_db . ".user_course
	                SET
	                   auth_token = UUID()
	                WHERE
	                   course_id = '" . $parCourseId . "' AND user_id = '" . $parUserId . "' AND auth_token is NULL";
			$this->db_db_query ( $sql );
			
			$sql = "SELECT
	                   auth_token
	                FROM
	                   " . $eclass_db . ".user_course
	                WHERE
	                   course_id = '" . $parCourseId . "' AND user_id = '" . $parUserId . "'";
			$resultAry ['authToken'] = current ( $this->returnVector ( $sql ) );
			
			$sql = "UPDATE
	                   " . classNamingDB ( $parCourseId ) . ".usermaster
	                SET
	                   auth_token = '" . $resultAry ['authToken'] . "'
	                WHERE
	                   user_id = '" . $parUserId . "'";
			$this->db_db_query ( $sql );
			
			/* Get session key */
			$sql = "SELECT intranet_user_id FROM " . classNamingDB ( $parCourseId ) . ".usermaster WHERE user_id = '" . $parUserId . "'";
			$intranetUserId = current ( $this->returnVector ( $sql ) );
			
			$sessionKeyAry = $this->getSessionKeyLastUpdatedFromUserId ( $intranetUserId );
			if (! empty ( $sessionKeyAry ['SessionKey'] )) { // have session key, update time
				$this->updateSessionLastUpdatedByUserId ( $intranetUserId );
				$sessionKey = $sessionKeyAry ['SessionKey'];
			} else { // no session key, gen a new one and update session key
				$sessionKey = $this->generateSessionKey ();
				$this->updateSessionByUserId ( $sessionKey, $intranetUserId );
			}
			$resultAry ['sessionKey'] = $sessionKey;
			return $resultAry;
		} else {
			return 805;
		}
	}
	function WS_LogoutCourseByAuthToken($parCourseId, $parLessonId, $parAuthToken) {
		global $eclass_db;
		
		/* Validate AuthToken */
		$apiUri = '/lesson/' . $parLessonId . '/accessRight';
		$checkAccessRightApi = __powerLessonApiCaller ( $apiUri, 'GET', $parCourseId, false, array(), $parAuthToken );
		
		/* Logout Course */
		if ($checkAccessRightApi ['result']) {
			$sql = "UPDATE
	                   " . $eclass_db . ".user_course
	                SET
	                   auth_token = NULL
	                WHERE
	                   course_id = '" . $parCourseId . "' AND auth_token = '" . $parAuthToken . "'";
			$this->db_db_query ( $sql );
			
			$sql = "UPDATE
	                   " . classNamingDB ( $parCourseId ) . ".usermaster
	                SET
	                   auth_token = NULL
	                WHERE
	                   auth_token = '" . $parAuthToken . "'";
			$this->db_db_query ( $sql );
			
			return array ();
		} else {
			return 805;
		}
	}
	function WS_GetUserInfo() {
		global $UserID;
		
		include_once ("libuser.php");
		$user = new libuser ( $UserID );
		
		$resultAry = array ();
		$resultAry ['UserID'] = $user->UserID;
		$resultAry ['UserEmail'] = $user->UserEmail;
		$resultAry ['ChineseName'] = intranet_undo_htmlspecialchars ( $user->ChineseName );
		$resultAry ['EnglishName'] = intranet_undo_htmlspecialchars ( $user->EnglishName );
		$resultAry ['TitleChinese'] = intranet_undo_htmlspecialchars ( $user->TitleChinese );
		$resultAry ['TitleEnglish'] = intranet_undo_htmlspecialchars ( $user->TitleEnglish );
		$resultAry ['RecordType'] = $user->RecordType;
		$resultAry ['PhotoLink'] = $user->PhotoLink;
		
		return $resultAry;
	}
	
	function WS_ValidateLessonPin($parPin) {
		global $eclass_db, $intranet_db;
		
		/* Validate Quick Login Code */
		$apiUri = '/lesson/' . $parPin . '/quickLogin';
		$getLessonApi = __powerLessonApiCaller ( $apiUri, 'GET', $parPin, true );
		
		if ($getLessonApi ['result']) {
			/* Lesson */
			$lesson = $getLessonApi ['apiResponse'];
			
			/* Course */
			$sql = "SELECT
	               course_id, course_code, course_name, language
	            FROM
	               " . $eclass_db . ".course
	            WHERE
	               course_id = '" . $lesson ['courseId'] . "'";
			$result = current ( $this->returnArray ( $sql ) );
			$course = array ();
			$course ['id'] = $result ['course_id'];
			$course ['code'] = $result ['course_code'];
			$course ['title'] = intranet_undo_htmlspecialchars ( $result ['course_name'] );
			$course ['language'] = $result ['language'];
			
			/* Course Users */
			$users = $this->WS_GetCourseUsers ( $lesson ['courseId'] );
			
			return array (
					"course" => $course,
					"users" => $users 
			);
		} else {
			return 805;
		}
	}
	function WS_GetEclassGroups($courseId) {
		$courseDb = classNamingDB ( $courseId );
		$sql = "SELECT
	               g.group_id, g.group_name, ug.user_id
	            FROM
	               " . $courseDb . ".grouping g
	            LEFT JOIN
	               " . $courseDb . ".user_group ug ON g.group_id = ug.group_id
	            ORDER BY
	               g.group_name
	    ";
		$userGroupResults = $this->returnArray ( $sql );
		
		$eclassGroupAryByGroupId = array ();
		for($i = 0; $i < sizeof ( $userGroupResults ); $i ++) {
			if (! isset ( $eclassGroupAryByGroupId [$userGroupResults [$i] ['group_id']] )) {
				$eclassGroupAryByGroupId [$userGroupResults [$i] ['group_id']] = array ();
				$eclassGroupAryByGroupId [$userGroupResults [$i] ['group_id']] ['id'] = $userGroupResults [$i] ['group_id'];
				$eclassGroupAryByGroupId [$userGroupResults [$i] ['group_id']] ['groupName'] = intranet_undo_htmlspecialchars ( $userGroupResults [$i] ['group_name'] );
				$eclassGroupAryByGroupId [$userGroupResults [$i] ['group_id']] ['userIds'] = array ();
			}
			$eclassGroupAryByGroupId [$userGroupResults [$i] ['group_id']] ['userIds'] [] = intval ( $userGroupResults [$i] ['user_id'] );
		}
		$resultAry = array ();
		foreach ( $eclassGroupAryByGroupId as $eclassGroup ) {
			$resultAry [] = $eclassGroup;
		}
		return $resultAry;
	}
	function WS_GetCourseUsers($courseId) {
		global $intranet_db, $powerlesson2_config;
		$courseDb = classNamingDB ( $courseId );
		$userResults = array ();
		$sql = "
	        SELECT
	           u.user_id AS id,
	           u.user_email AS userEmail,
	           u.TitleEnglish AS englishTitle,
	           u.TitleChinese AS chineseTitle,
	           u.firstname AS firstName,
	           u.lastname AS lastName,
	           u.chinesename AS chineseName,
	           u.intranet_user_id AS intranetUserId,
	           u.memberType,
	           u.class_number AS classNameAndClassNumber,
	           u.status
	       FROM    
	           " . $courseDb . ".usermaster	u 
           WHERE 
               memberType IN ('T', 'S')
	    ";
		$result = $this->returnResultSet ( $sql );
		
		$intranetUserIds = array();
		for($i=0; $i < sizeof($result); $i++){
		    if(!is_null($result[$i]['intranetUserId'])){
		        $intranetUserIds[] = $result[$i]['intranetUserId'];
		    }
		}
		
		$sql = "
		    SELECT
		       UserID, UserLogin
		    FROM
	           " . $intranet_db . ".INTRANET_USER
	        WHERE
	           UserID in ('" . implode("','", $intranetUserIds) . "')
		    ";
		$intranetUserResult = $this->returnResultSet ( $sql );
		
		$intranetUserLoginMapping = array();
		for($i=0; $i < sizeof($intranetUserResult); $i++){
		    $intranetUserLoginMapping[$intranetUserResult[$i]['UserID']] = $intranetUserResult[$i]['UserLogin'];
		}
		
		include_once ("libuser.php");
		$lu = new libuser ();
		for($i = 0; $i < sizeof ( $result ); $i ++) {
			$userResults [$i] ['id'] = intval ( $result [$i] ['id'] );
			$userResults [$i] ['userEmail'] = $result [$i] ['userEmail'];
			$userResults [$i] ['englishTitle'] = intranet_undo_htmlspecialchars ( $result [$i] ['englishTitle'] );
			$userResults [$i] ['chineseTitle'] = intranet_undo_htmlspecialchars ( $result [$i] ['chineseTitle'] );
			$userResults [$i] ['firstName'] = $result [$i] ['firstName'];
			$userResults [$i] ['lastName'] = $result [$i] ['lastName'];
			$userResults [$i] ['chineseName'] = intranet_undo_htmlspecialchars ( $result [$i] ['chineseName'] );
			$userResults [$i] ['intranetUserId'] = intval ( $result [$i] ['intranetUserId'] );
			$userResults [$i] ['memberType'] = $result [$i] ['memberType'];
			
			if ($result [$i] ['memberType'] == 'S') {
				$result [$i] ['classNameAndClassNumber'] = trim ( intranet_undo_htmlspecialchars ( $result [$i] ['classNameAndClassNumber'] ) );
				if (! empty ( $result [$i] ['classNameAndClassNumber'] )) {
					$classNameAndClassNumberAry = explode ( " - ", $result [$i] ['classNameAndClassNumber'] );
					$userResults [$i] ['className'] = count ( $classNameAndClassNumberAry ) > 0 ? $classNameAndClassNumberAry [0] : null;
					$userResults [$i] ['classNumber'] = count ( $classNameAndClassNumberAry ) > 1 ? $classNameAndClassNumberAry [1] : null;
				} else {
					$userResults [$i] ['className'] = null;
					$userResults [$i] ['classNumber'] = null;
				}
			}
			$userResults [$i] ['status'] = $result [$i] ['status'];
			$userResults [$i] ['englishName'] = intranet_undo_htmlspecialchars ( trim ( $result [$i] ['firstName'] . ' ' . $result [$i] ['lastName'] ) );
			$userResults [$i] ['intranetPhotoLink'] = '';
			
			if(!$powerlesson2_config['disableUserPhoto'] && isset($intranetUserLoginMapping[$userResults[$i]['intranetUserId']])){
			    $userPhotoAry = $lu->GET_USER_PHOTO(
			        $intranetUserLoginMapping[$userResults[$i]['intranetUserId']], 
			        $userResults[$i]['intranetUserId'], 
			        false
			    );
			    
			    if(is_array($userPhotoAry)){
			        $userResults[$i]['intranetPhotoLink'] = $userPhotoAry[1];
			    }
			}
		}
		return $userResults;
	}
	function WS_GetCoursesUsers($courseIds){
		$courseIds = (array)$courseIds;
		$coursesUsers = array();
		
		for($i = 0; $i < sizeof($courseIds); $i++){
			$coursesUsers[$courseIds[$i]] = $this->WS_GetCourseUsers($courseIds[$i]);
		}
		
		return $coursesUsers;
	}
	function WS_GetCourseTeachers($courseId) {
		global $intranet_db;
		$courseDb = classNamingDB ( $courseId );
		$userResults = array ();
		$sql = "
	        SELECT
	           u.user_id AS id,
	           u.user_email AS userEmail,
	           u.TitleEnglish AS englishTitle,
	           u.TitleChinese AS chineseTitle,
	           u.firstname AS firstName,
	           u.lastname AS lastName,
	           u.chinesename AS chineseName,
	           u.intranet_user_id AS intranetUserId,
	           u.memberType,
	           u.class_number AS classNameAndClassNumber,
	           u.status
	       FROM    
	           " . $courseDb . ".usermaster	u 
           WHERE 
               memberType IN ('T')
	    ";
		$result = $this->returnResultSet ( $sql );
		include_once ("libuser.php");
		$lu = new libuser ();
		for($i = 0; $i < sizeof ( $result ); $i ++) {
			$userResults [$i] ['id'] = intval ( $result [$i] ['id'] );
			$userResults [$i] ['userEmail'] = $result [$i] ['userEmail'];
			$userResults [$i] ['englishTitle'] = intranet_undo_htmlspecialchars ( $result [$i] ['englishTitle'] );
			$userResults [$i] ['chineseTitle'] = intranet_undo_htmlspecialchars ( $result [$i] ['chineseTitle'] );
			$userResults [$i] ['firstName'] = $result [$i] ['firstName'];
			$userResults [$i] ['lastName'] = $result [$i] ['lastName'];
			$userResults [$i] ['chineseName'] = intranet_undo_htmlspecialchars ( $result [$i] ['chineseName'] );
			$userResults [$i] ['intranetUserId'] = intval ( $result [$i] ['intranetUserId'] );
			$userResults [$i] ['memberType'] = $result [$i] ['memberType'];
			$userResults [$i] ['status'] = $result [$i] ['status'];
			$userResults [$i] ['englishName'] = intranet_undo_htmlspecialchars ( trim ( $result [$i] ['firstName'] . ' ' . $result [$i] ['lastName'] ) );
			$userResults [$i] ['intranetPhotoLink'] = '';
		}
		return $userResults;
	}
	function WS_GetCoursesTeachers($courseIds){
		$courseIds = (array)$courseIds;
		$coursesTeachers = array();
		
		for($i = 0; $i < sizeof($courseIds); $i++){
			$coursesTeachers[$courseIds[$i]] = $this->WS_GetCourseTeachers($courseIds[$i]);
		}
		
		return $coursesTeachers;
	}
	function WS_ForgetPasswordForDHL($UserLogin) {
		global $intranet_root, $webmaster, $sys_custom;
		$UserLogin = htmlspecialchars ( trim ( $UserLogin ) );
		include_once ('libauth.php');
		$li = new libauth ();
		$UserInfo = $li->forgetPassword2 ( $UserLogin );
		$email = "";
		
		if ($UserInfo == 0) {
			$urlSuccess = 0;
		} else {
			
			// Send Email
			$t_UserID = $UserInfo [0];
			$UserLogin = $UserInfo [1];
			$UserPassword = $UserInfo [2];
			$UserEmail = $UserInfo [3];
			$EnglishName = $UserInfo [4];
			$ChineseName = $UserInfo [5];
			
			// Call sendmail function
			include_once ("libuser.php");
			$luser = new libuser ();
			$Key = $li->CreateResetPasswordKey ( $UserLogin );
			list ( $mailSubject, $mailBody ) = $luser->returnEmailNotificationData_HashedPw ( $UserEmail, $UserLogin, $EnglishName, $Key );
			$mailTo = $UserEmail;
			$webmaster = get_webmaster ();
			include_once ("libemail.php");
			include_once ("libsendmail.php");
			$lu = new libsendmail ();
			
			$result = $lu->send_email ( $mailTo, $mailSubject, $mailBody, "" );
			
			if ($UserEmail != '') {
				$email = substr ( $UserEmail, strpos ( $UserEmail, '@' ) + 1 );
				$urlSuccess = 1;
			} else {
				$urlSuccess = 2;
			}
		}
		$returnAry = array ();
		$returnAry ['EmailDomain'] = $email;
		$returnAry ['SendEmailSuccess'] = $urlSuccess; // 0:login not exist, 1:send success, 2:don`t have address
		return $returnAry;
	}
	function WS_UpdateEmailForDHL($parUserId, $email) {
		global $intranet_root, $sys_custom;
		
		$sql = "UPDATE INTRANET_USER SET UserEmail='$email' WHERE UserID=$parUserId ";
		$result = $this->db_db_query ( $sql );
		$returnAry = array ();
		$returnAry ['UpdateSuccess'] = $result;
		return $returnAry;
	}
	function WS_IsPowerLessonCompatible($version) {
		include_once ('PowerLesson/PowerLesson.php');
		return array (
				'result' => PowerLesson::isCompatible ( $version ),
				'minimumCompatibleVersion' => PowerLesson::getMinimumCompatibleVersion () 
		);
	}
	function WS_GetTargetableUsersForMessaging($intranetUserID) {
		include_once ("libuser.php");
		include_once ("form_class_manage.php");
		include_once ("subject_class_mapping.php");
		include_once ("libclubsenrol.php");
		
		$resultAry = array ();
		
		// check user type:
		$user = new libuser ( $intranetUserID );
		$form_class_manage = new form_class_manage ();
		$subject_class_mapping = new subject_class_mapping ();
		$libclubsenrol = new libclubsenrol ();
		
		// if user is parent:
		if ($user->isParent ()) {
			// get class teacher:
			// get subject group teacher:
			// get ECA teacher (eEnrol + Activity)
		}
		
		// if user is teacher:
		if ($user->isTeacherStaff ()) {
			// 1. from class
			// get classes:
			$classAry = $form_class_manage->Get_Class_Teacher_Class ( $intranetUserID );
			$tmpParentClassAry = array();
			
			foreach ( $classAry as $yearClassData ) {
				
				// get class students:
				$yearClassID = $yearClassData ['ClassID'];
				$yearClass = new year_class ( $yearClassID, $GetYearDetail = true, $GetClassTeacherList = false, $GetClassStudentList = true );
				$classStudentDataAry = $yearClass->ClassStudentList;
				
				$classStudentIDAry = array ();
				
				foreach ( $classStudentDataAry as $studentData ) {
					$studentID = $studentData ['UserID'];
					$parentIDAry = $user->getParentUsingParentApp ( ( array ) $studentID, $includeNoPushMessageUser = true );
					
					foreach ( $parentIDAry as $parentID ) {
						if ($resultAry [$parentID] ['RelationID'] == $studentID) {
							$resultAry [$parentID] ['RelationEn'] .= "\n" . "- " . $yearClass->ClassTitleEN;
							$resultAry [$parentID] ['RelationCh'] .= "\n" . "- " . $yearClass->ClassTitleB5;
						} else {
							$parentUser = new libuser ( $parentID );
							$resultAry [$parentID] ['UserID'] = $parentID;
							$resultAry [$parentID] ['UsernameEn'] = $parentUser->EnglishName;
							$resultAry [$parentID] ['UsernameCh'] = $parentUser->ChineseName;
							$resultAry [$parentID] ['RelationID'] = $studentID;
							$resultAry [$parentID] ['RelationEn'] = "- " . $yearClass->ClassTitleEN;
							$resultAry [$parentID] ['RelationCh'] = "- " . $yearClass->ClassTitleB5;
							$resultAry [$parentID] ['RelationType'] = "class";
						}
					}
				}
			}
			
			// 2. from subject group
			// get current year term
			//$yearTermID = $form_class_manage->getCurrentAcademicaYearID ();
			$yearTermID = getCurrentSemesterID();
			
			// get subject group students:
			$lsubject = new subject();
			$subjectGroupAry = $lsubject->Get_Subject_Group_List ( $yearTermID, $ClassLevelID = '', $SubjectGroupID = '', $intranetUserID, $returnAsso = 0, $YearClassID = '', $SubjectID = '', $InYearClassOnly = false );
			foreach ( $subjectGroupAry as $subjectGroupData ) {
				$subjectGroupID = $subjectGroupData ['SubjectGroupID'];
				
				$subjectGroup = new subject_term_class ( $subjectGroupID, $GetTeacherList = false, $GetStudentList = true, $SubjectGroupCode = '', $yearTermID );
				
				$subjectGroupStudentAry = $subjectGroup->ClassStudentList;
				
				foreach ( $subjectGroupStudentAry as $subjectGroupStudent ) {
					$studentID = $subjectGroupStudent ['UserID'];
					
					// get parent ID
					$parentIDAry = $user->getParentUsingParentApp ( ( array ) $studentID, $includeNoPushMessageUser = true );
					
					// add to result array
					foreach ( $parentIDAry as $parentID ) {
						if ($resultAry [$parentID] ['RelationID'] == $studentID) {
							$resultAry [$parentID] ['RelationEn'] .= "\n" . "- " . $subjectGroup->ClassTitleEN;
							$resultAry [$parentID] ['RelationCh'] .= "\n" . "- " . $subjectGroup->ClassTitleB5;
						} else {
							$parentUser = new libuser ( $parentID );
							$resultAry [$parentID] ['UserID'] = $parentID;
							$resultAry [$parentID] ['UsernameEn'] = $parentUser->EnglishName;
							$resultAry [$parentID] ['UsernameCh'] = $parentUser->ChineseName;
							$resultAry [$parentID] ['RelationID'] = $studentID;
							$resultAry [$parentID] ['RelationEn'] = "- " . $subjectGroup->ClassTitleEN;
							$resultAry [$parentID] ['RelationCh'] = "- " . $subjectGroup->ClassTitleB5;
							$resultAry [$parentID] ['RelationType'] = "subject";
						}
					}
				}
			}
			
			// 3. from ECA Club
			// get ECA students:
			$PICClubAry = $libclubsenrol->Get_PIC_Club ( $intranetUserID );
			if (count ( $PICClubAry ) > 0) {
				$clubsMemberInfoAry = $libclubsenrol->Get_Club_Member_Info ( $PICClubAry, array (
						'2' 
				) );
				
				foreach ( $clubsMemberInfoAry as $clubMemberAry ) {
					$clubStudentIDAry = array ();
					$clubName = "";
					foreach ( $clubMemberAry as $clubMemberInfo ) {
						$studentID = $clubMemberInfo ['StudentID'];
						$clubName = $clubMemberInfo ['GroupTitle'];
						
						// get parent ID
						$parentIDAry = $user->getParentUsingParentApp ( ( array ) $studentID, $includeNoPushMessageUser = true );
						
						// add to result array
						foreach ( $parentIDAry as $parentID ) {
							if ($resultAry [$parentID] ['RelationID'] == $studentID) {
								$resultAry [$parentID] ['RelationEn'] .= "\n" . "- " . $clubName;
								$resultAry [$parentID] ['RelationCh'] .= "\n" . "- " . $clubName;
							} else {
								$parentUser = new libuser ( $parentID );
								$resultAry [$parentID] ['UserID'] = $parentID;
								$resultAry [$parentID] ['UsernameEn'] = $parentUser->EnglishName;
								$resultAry [$parentID] ['UsernameCh'] = $parentUser->ChineseName;
								$resultAry [$parentID] ['RelationID'] = $studentID;
								$resultAry [$parentID] ['RelationEn'] = "- " . $clubName;
								$resultAry [$parentID] ['RelationCh'] = "- " . $clubName;
								$resultAry [$parentID] ['RelationType'] = "club";
							}
						}
					}
				}
			}
			
			// 4. from ECA Activity
			// get PIC activities:
			$PICActivityIDAry = $libclubsenrol->Get_PIC_Activity ( $intranetUserID );
			$activityInfoAry = $libclubsenrol->Get_Activity_Info_By_Id ( $PICActivityIDAry );
			
			// get students in each activity
			foreach ( $activityInfoAry as $activityInfo ) {
				$activityStudentDataAry = $libclubsenrol->getEventStudentInfo ( $activityInfo ['EnrolEventID'] );
				
				$activityStudentIDAry = array ();
				foreach ( $activityStudentDataAry as $activityStudentData ) {
					$studentID = $activityStudentData ['StudentID'];
					
					// get parent ID
					$parentIDAry = $user->getParentUsingParentApp ( ( array ) $studentID, $includeNoPushMessageUser = true );
					
					// add to result array
					foreach ( $parentIDAry as $parentID ) {
						if ($resultAry [$parentID] ['RelationID'] == $studentID) {
							$resultAry [$parentID] ['RelationEn'] .= "\n" . "- " . $activityInfo ['EventTitle'];
							$resultAry [$parentID] ['RelationCh'] .= "\n" . "- " . $activityInfo ['EventTitle'];
						} else {
							$parentUser = new libuser ( $parentID );
							$tempAry = array ();
							$resultAry [$parentID] ['UserID'] = $parentID;
							$resultAry [$parentID] ['UsernameEn'] = $parentUser->EnglishName;
							$resultAry [$parentID] ['UsernameCh'] = $parentUser->ChineseName;
							$resultAry [$parentID] ['RelationID'] = $studentID;
							$resultAry [$parentID] ['RelationEn'] = "- " . $activityInfo ['EventTitle'];
							$resultAry [$parentID] ['RelationCh'] = "- " . $activityInfo ['EventTitle'];
							$resultAry [$parentID] ['RelationType'] = "activity";
						}
					}
				}
			}
		}
		return $resultAry;
	}
	function WS_OpenGroupMessageChatroom($intranetUserID, $targetUserIDAry) {
		global $PATH_WRT_ROOT, $eclassAppConfig, $sys_custom;
		include_once ($PATH_WRT_ROOT . 'includes/eClassApp/groupMessage/libeClassApp_groupMessage.php');
		include_once ($PATH_WRT_ROOT . 'includes/eClassApp/libeClassApp.php');
		$leClassApp_groupMessage = new libeClassApp_groupMessage ();
		$leClassApp = new libeClassApp ();
		
		$returnAry = array ();
		$successAry = array ();
		
		$groupId = "";
		$groupCode = uniqid ( $prefix = "chatroom_" );
		$groupNameEn = $groupCode;
		$groupNameCh = $groupCode;
		$communicationMode = $eclassAppConfig ['INTRANET_APP_MESSAGE_GROUP'] ['CommunicationMode'] ['Normal'];
		$groupType = $eclassAppConfig ['INTRANET_APP_MESSAGE_GROUP'] ['GroupType'] ['UserChatroom'];
		
		$resultAry = $leClassApp_groupMessage->createOrUpdateGroupAndSyncToCloud ( $groupId, $groupCode, $groupNameEn, $groupNameCh, $communicationMode, $groupType );
		$successAry ['createGroupSuccess'] = $resultAry ['success'];
		$groupId = $resultAry ['groupId'];
		
		if ($successAry ['createGroupSuccess']) {
			// add group member to group
			$userIdAry = ( array ) $targetUserIDAry;
			$userIdAry [] = $intranetUserID;
			
			$leClassApp->Start_Trans ();
			$successAry ['saveLocal'] = $leClassApp_groupMessage->addGroupMember ( $groupId, $userIdAry );
			if ($successAry ['saveLocal']) {
				$successAry ['saveCloud'] = $leClassApp_groupMessage->saveGroupMemberInCloud ( $groupId );
			}
			
			if (in_array ( false, $successAry )) {
				$leClassApp->RollBack_Trans ();
			} else {
				$leClassApp->Commit_Trans ();
			}
		}
		
		$returnAry ['successAry'] = $successAry;
		
		return $returnAry;
	}
	
	function WS_GetUserOfficialPhotoPath($parUserId)
	{
	    global $intranet_root;
	    
	    $returnAry = array();
	    if($parUserId > 0)
	    {
    	    include_once($intranet_root.'/includes/libuser.php');
    	    $luser = new libuser($parUserId);
    	    $photoPath = $luser->PhotoLink;
    	    
    	    if(trim($photoPath) == '' || !file_exists($intranet_root.$photoPath)) {
    	        $photoPath = $luser->returnDefaultOfficialPhotoPath();
    	    }
    	    $pathPrefix = curPageURL($withQueryString=1, $withPageSuffix=0);
    	    $returnAry['photoPath'] = $pathPrefix.$photoPath;
	    }
	    
	    return $returnAry;
	}
	
	function WS_GetUserPersonalPhotoPath($parUserId)
	{
	    global $intranet_root;
	    
	    $returnAry = array();
	    if($parUserId > 0)
	    {
	        include_once($intranet_root.'/includes/libuser.php');
	        $luser = new libuser($parUserId);
	        $photoPath = $luser->PersonalPhotoLink;
	        
	        if(trim($photoPath) == '' || !file_exists($intranet_root.$photoPath)) {
	            $photoPath = $luser->returnDefaultPersonalPhotoPath();
	        }
	        $pathPrefix = curPageURL($withQueryString=1, $withPageSuffix=0);
	        $returnAry['photoPath'] = $pathPrefix.$photoPath;
	        
	        $personalPhotoBakPath = $intranet_root.'/file/photo/personal/p'. $parUserId. '.jpg.appBak';
	        $returnAry['isLinkedWithOfficialPhoto'] = (file_exists($personalPhotoBakPath))? '1' : '0';
	    }
	    
	    return $returnAry;
	}
	
	function WS_DigitalChannelUploadAlbumDetails($CurrentUserID, $CategoryCode, $AlbumID, $AlbumTitle, $AlbumDescription, $PeriodStartTime, $PeriodEndTime, $TargetGroup, $TargetGroupIDs, $PersonInChargeIDs, $AlbumChiTitle, $AlbumChiDescription, $AccessDate)
	{
	    global $PATH_WRT_ROOT, $intranet_root;
	    include_once ($intranet_root. "/includes/global.php");
	    include_once ($intranet_root. "/includes/libdb.php");
	    include_once ($intranet_root. '/includes/DigitalChannels/libdigitalchannels.php');
	    $ldc = new libdigitalchannels();
	    
	    if ($AccessDate == '') {
	        $AccessDate = date('Y-m-d');
	    }
	    
	    if ($PeriodStartTime == '') {
	        $PeriodStartTime = '0000-00-00 00:00:00';
	    }
	    
	    if ($PeriodEndTime == '') {
	        $PeriodEndTime = '0000-00-00 00:00:00';
	    }
	    
	    $params = array(
	        'CategoryCode' => $CategoryCode,
	        'title' => htmlspecialchars($AlbumTitle),
	        'description' => htmlspecialchars($AlbumDescription),
	        'place' => '',     // not used
	        'date' => $AccessDate,
	        'since' => $PeriodStartTime,
	        'until' => $PeriodEndTime,
	        'album_id' => ($AlbumID ? $AlbumID : 0),
	        'chiTitle' => standardizeFormPostValue($AlbumChiTitle),
	        'chiDescription' => standardizeFormPostValue($AlbumChiDescription)
	    );
	    
	    if (! $AlbumID) {
	        $result = $ldc->Add_Album($params, true, $CurrentUserID);
	        $AlbumID = $result;
	    } else {
	        $result = $ldc->Update_Album($params, true, $CurrentUserID);
	    }
	    $picAry = explode(',', $PersonInChargeIDs);
	    $ldc->updateAlbumPICs($picAry, $AlbumID, $CurrentUserID);
	    
	    $targetGroupIDAry = explode(',', $TargetGroupIDs);
	    $select_groups = $TargetGroup? $targetGroupIDAry: array();
	    $ldc->resetAlbumUsersGroups($TargetGroup == '1', $select_groups, $user_ids = array(), $AlbumID, $CurrentUserID);
	    
	    $ret = array('AlbumID'=>$AlbumID);     // must return array
	    return $ret;
	}

	// $CurrentUserID is not used at present
	function WS_GetTargetGroupList($CurrentUserID, $ShareGroups=array())
	{
	    global $intranet_session_language, $intranet_root, $sys_custom;
	    global $junior_mck;
	    
	    include_once($intranet_root."/includes/libgroup.php");
	    include_once($intranet_root."/includes/libgroupcategory.php");
	    
	    if($sys_custom['DHL']){
	        include_once($intranet_root.'/includes/DHL/libdhl.php');
	        $libdhl = new libdhl();
	    }
	    
	    $lg = new libgroup();
	    $lgc = new libgroupcategory();
	    
	    if($junior_mck > 0){
	        $title_field = 'Title';
	    }else{
	        $title_field = $intranet_session_language =="en" ? "Title":"TitleChinese";
	    }
	    $CurrentAcademicYearID = Get_Current_Academic_Year_ID();
	    
	    $group_category_ary = $lgc->returnCats();
	    
	    if($junior_mck > 0){ // EJ
	        foreach($group_category_ary as $i=> $r){
	            $group_category_ary[$i][1] = convert2unicode($r[1]);
	            $group_category_ary[$i]['CategoryName'] = convert2unicode($r['CategoryName']);
	        }
	    }
	    
	    # Get details of selected groups
	    $groupList = (count($ShareGroups) > 0)? $lg->returnGroup("", implode(",", (array)$ShareGroups)) : array();
	    $selectedList = BuildMultiKeyAssoc($groupList, "GroupID");

	    # List of Target Group
	    $group_ary = array();
	    if(count($group_category_ary) > 0){
	        foreach($group_category_ary as $k=>$d)
	        {
	            $pass_CurrentAcademicYearID = $d['GroupCategoryID']==0 ? "" : $CurrentAcademicYearID;
	            $group_data_ary = $lg->returnGroupsByCategory($d['GroupCategoryID'], $pass_CurrentAcademicYearID);

	            if($junior_mck > 0){ // EJ
	                foreach($group_data_ary as $i=> $r){
	                    $group_data_ary[$i][1] = convert2unicode($r[1]);
	                    $group_data_ary[$i][$title_field] = convert2unicode($r[$title_field]);
	                }
	            }
	            
	            if(count($group_data_ary) > 0){
	                foreach($group_data_ary as $k2 => $d2)
	                {
	                    # Skip if duplicate
	                    if(isset($selectedList[$d2['GroupID']]))
	                        continue;
	                    $group_ary[$d2['GroupID']] = "(".$d['CategoryName'].")".$d2[$title_field];
	                }
	            }
	        }
	    }
	    
	    if($sys_custom['DHL']){
	        $exclude_group_ids = Get_Array_By_Key($groupList,'GroupID');
	        $filter_map = array('GroupIDToDisplayTitle'=>1);
	        if(count($exclude_group_ids)>0){
	            $filter_map['ExcludeGroupID'] = $exclude_group_ids;
	        }
	        $group_ary = $libdhl->getIntranetGroupRecords($filter_map);
	    }
	    
	    $ret = array();
	    foreach((array)$group_ary as $_id=>$_name) {
	        $ret[] = array('ID'=>$_id, 'Name'=>$_name);
	    }
	        
	    return $ret;
	}

	
	/**
	 * $PermittedUserType (comma separated): 1 - teacher, 2 - student, 3 - parent, 4 - alumni	 
	 * $ExcludeType (comma separated): 'COM', $PermittedUserType, ''
	 * $FilterClubAndEventInCharge: exclude GroupCategoryID=5 in INTRANET_GROUP_CATEGORY
	 * $FilterClassTeacherClass: exclude GroupCategoryID=3 in INTRANET_GROUP_CATEGORY
	 * $DisplayInternalRecipientGroup: true - return internal recipient group id and name, false - don't return internal reciipient group id and name
	 * $IsAdmin: for DHL only 
	 * 
	 * return $targetGroupAry = array(
	 *     'Identities' => array(
	 *         [] => array(
	 *             'ID' => -1,
	 *             'Name' => $Lang['DHL']['Staff']
	 *         ),
	 *         [] => array(
	 *             'ID' => -2,
	 *             'Name' => $Lang['Identity']['Student'],
	 *             'ClassList' => array(
	 *                 'YearID' => 44
	 *                 'YearName' => G9
	 *                 'ClassTitleEN' => F9A
	 *                 'ClassTitle' => F9A
	 *             )
	 *         ),
	 *         [] => array(
	 *             'ID' => -3,
	 *             'Name' => $Lang['Identity']['Parent'],
	 *             'ClassList' => array(
	 *                 'YearID' => 44
	 *                 'YearName' => G9
	 *                 'ClassTitleEN' => F9A
	 *                 'ClassTitle' => F9A
	 *             )
	 *         ),
	 *         [] => array(
	 *             'ID' => -4,
	 *             'Name' => $Lang['Identity']['Alumni']
	 *         )
	 *         [] => array(
	 *             'ID' => -99,
	 *             'Name' => $Lang['Identity']['TeachingStaff']
	 *         ),
	 *         [] => array(
	 *             'ID' => -100,
	 *             'Name' => $Lang['Identity']['NonTeachingStaff']
	 *         )
	 *     ),
	 *     'GroupCategories' => array(
	 *         [] => array(
	 *             'ID' => $_categoryID,        
	 *             'Category' => $_categoryName,
	 *             'Groups' => array(
	 *                 'ID' => $_id,
	 *                 'Name' => $_name
	 *             ),
	 *             'Events' => array(
	 *                 'ID' => $_id,
	 *                 'Name' => $_name
	 *             )
	 *         )
	 *     ),
	 *     'InternalRecipientGroup' => array(
	 *         [] => array(
	 *             'ID' => $_id,        
	 *             'Name' => $_name
	 *         )
	 *     )
	 *         
	 */
	
	function WS_GetCommonChooseGroups($CurrentUserID, $PermittedUserType=1, $ExcludeType='', $FilterClubAndEventInCharge=false, $FilterClassTeacherClass=false, $DisplayInternalRecipientGroup=false, $IsAdmin=false)
	{
	    global $intranet_root, $sys_custom, $Lang;
	    
	    include_once($intranet_root."/includes/libgrouping.php");
	    include_once($intranet_root."/includes/libgroupcategory.php");
	    include_once($intranet_root."/includes/form_class_manage.php");
	    
	    if($sys_custom['DHL']){
	        include_once($intranet_root."/includes/DHL/libdhl.php");
	        $libdhl = new libdhl();
	    }
	    
	    $targetGroupAry = array();
	    $permittedUserTypeAry = explode(',',$PermittedUserType);
	    $excludeTypeAry = explode(',',$ExcludeType);
	    
	    if (((in_array(2,$permittedUserTypeAry) && !in_array(2,$excludeTypeAry)) || (in_array(3,$permittedUserTypeAry) && !in_array(3,$excludeTypeAry))) && !$sys_custom['DHL']) {
	        $_classListAssoc = array();
	        $fcm = new form_class_manage();
	        $AcademicYearID = Get_Current_Academic_Year_ID();
	        $ClassList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID, $YearID='', $TeachingOnly=0, $YearClassIDArr='');
	        foreach((array)$ClassList as $_classList) {
	            $_classListAssoc[] = array(
	                'YearID'=>$_classList['YearID'],
	                'YearName'=>$_classList['YearName'],
	                'ClassTitleEN'=>$_classList['ClassTitleEN'],
	                'ClassTitle'=>Get_Lang_Selection($_classList['ClassTitleB5'],$_classList['ClassTitleEN'])
	            );
	        }
	    }
	    
	    if (in_array(2,$permittedUserTypeAry) && !in_array(2,$excludeTypeAry) && !$sys_custom['DHL']) {
	        $targetGroupAry['Identities'][] = array(
	            'ID'=>'-2', 
	            'Name'=>$Lang['Identity']['Student'],
	            'ClassList'=>$_classListAssoc
	        );
	    }
	    if (in_array(3,$permittedUserTypeAry) && !in_array(3,$excludeTypeAry) && !$sys_custom['DHL']) {
	        $targetGroupAry['Identities'][] = array(
	            'ID'=>'-3', 
	            'Name'=>$Lang['Identity']['Parent'],
	            'ClassList'=>$_classListAssoc
	        );
	    }
	    if (in_array(1,$permittedUserTypeAry)) {
	        if($sys_custom['DHL']){
	            if(in_array('COM',$excludeTypeAry)){
	                $targetGroupAry['Identities'][] = array('ID'=>'-1', 'Name'=>$Lang['DHL']['Staff']);
	            }
	        }
	        else {
	            $targetGroupAry['Identities'][] = array('ID'=>'-99', 'Name'=>$Lang['Identity']['TeachingStaff']);
	            $targetGroupAry['Identities'][] = array('ID'=>'-100', 'Name'=>$Lang['Identity']['NonTeachingStaff']);
	        }
	    }
	    if (in_array(4,$permittedUserTypeAry) && !in_array(4,$excludeTypeAry) && !$sys_custom['DHL']) {
	        $targetGroupAry['Identities'][] = array('ID'=>'-4', 'Name'=>$Lang['Identity']['Alumni']);
	    }
	    
	    
	    $filerGroupAry = array();
	    if ($FilterClubAndEventInCharge) {
	        include_once($intranet_root."/includes/libclubsenrol.php");
	        $libenroll = new libclubsenrol();
	        $managedEnrolGroupIDs = $libenroll->Get_PIC_Club($CurrentUserID);
	        $managedEventIDs = $libenroll->Get_PIC_Activity($CurrentUserID);
	        foreach ((array)$managedEnrolGroupIDs as $managedEnrolGroupID) {
	            $tempGroupID = $libenroll->GET_GROUPID($managedEnrolGroupID);
	            if (!in_array($tempGroupID, $filerGroupAry)) {
	                $filerGroupAry[] = $tempGroupID;
	            }
	        }
	    }
	    
	    if ($FilterClassTeacherClass) {
	        include_once($intranet_root."/includes/form_class_manage.php");
	        $form_class_manage = new form_class_manage();
	        $classAry = $form_class_manage->Get_Class_Teacher_Class($CurrentUserID);
	        foreach ((array)$classAry as $classInfo) {
	            $filerGroupAry[] = $classInfo['GroupID'];
	        }
	    }
	    
	    
	    $filterCatIDAry = array();
	    if ($FilterClubAndEventInCharge) {
	        $filterCatIDAry[] = 5;
	    }
	    
	    if ($FilterClassTeacherClass) {
	        $filterCatIDAry[] = 3;
	    }
	    
	    $lg = new libgrouping();
	    $lgc = new libgroupcategory();
	    $catAry = $lgc->returnAllCat();
	    for ($i=0, $iMax=count($catAry); $i<$iMax; $i++)
	    {
	        list($_categoryID,$_categoryName) = $catAry[$i];
	        
	        if($sys_custom['DHL'] && preg_match('/^COM\d+$/',$_categoryID)) {
	            // please handle here
	        }
	        else if ($_categoryID>0) {
    	        if (($FilterClubAndEventInCharge || $FilterClassTeacherClass) && !in_array($_categoryID, $filterCatIDAry)) {
    	            continue;
    	        }
    	        
	            $_groupAry = $lg->returnCategoryGroups($_categoryID, true);
	            $_groupAssoc = array();
	            foreach((array)$_groupAry as $__idx=>$__groupAry) {
	                $__groupID = $__groupAry['GroupID'];
	                if ($FilterClubAndEventInCharge || $FilterClassTeacherClass) {
	                    if (!in_array($__groupID, $filerGroupAry)) {
	                        continue;
	                    }
	                }
	                
	                $_groupAssoc[$__idx]['ID'] = $__groupID;
	                $_groupAssoc[$__idx]['Name'] = $__groupAry['Title'];
	            }
	            
	            $_eventAssoc = array();
	            if (($_categoryID == 5) && $FilterClubAndEventInCharge) {
	                for ($j=0,$jMax=count($managedEventIDs); $j<$jMax; $j++) {
	                    $thisEventID = $managedEventIDs[$j];
	                    $thisEventInfo = $libenroll->GET_EVENTINFO($thisEventID);
	                    $_eventAssoc[] = array('ID'=>$thisEventID, 'Name'=>$thisEventInfo["EventTitle"]); 
	                }
	            }
	            $targetGroupAry['GroupCategories'][] = array(
	                'ID'=>$_categoryID, 
	                'Category'=>$_categoryName, 
	                'Groups'=>$_groupAssoc, 
	                'Events'=>$_eventAssoc);
	        }
	    }

	    if ($DisplayInternalRecipientGroup && !$sys_custom['DHL']) {
	        $sql = "SELECT
                	        AliasID, AliasName
        	        FROM
        	                INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL
        	        WHERE
        	                OwnerID = '".$CurrentUserID."'
        	        ORDER BY
        	                AliasName
        	        ";
    	    $aliasAry = $this->returnResultSet($sql);
    	    $aliasAssoc = array();
    	    foreach((array)$aliasAry as $_aliasAry) {
    	        $targetGroupAry['InternalRecipientGroup'][] = array(
    	            'ID'=>'i'.$_aliasAry['AliasID'], 
    	            'Name'=>$_aliasAry['AliasName']);
    	    }
	    }
	    
	    if($sys_custom['DHL'] && !in_array('COM',$excludeTypeAry)){
	        
	        $PICInfo = $libdhl->getPICResponsInfo();
	        $displayCompanyIDAry = $PICInfo['Company'];
	        $displayDivisionIDAry = $PICInfo['Division'];
	        $displayDepartmentIDAry = $PICInfo['Department'];
	        $company_records = $libdhl->getCompanyRecords();
	        $company_name_field = Get_Lang_Selection("CompanyNameChi","CompanyNameEng");
	        for($i=0,$iMax=count($company_records); $i<$iMax; $i++){
	            $company_val = $company_records[$i]['CompanyID'];
	            $company_text = $company_records[$i][$company_name_field];
	            
	            if($IsAdmin || in_array($company_val,$displayCompanyIDAry)){
	                $_name = $company_records[$i]['Description']!='' ? intranet_htmlspecialchars($company_records[$i]['Description']) : '';
	                $targetGroupAry['dhlCompany'][] = array(
	                    'ID'=>$company_val, 
	                    'Name'=>$_name, 
	                    'Text'=>intranet_htmlspecialchars($company_text));
	            }
	        }
	    }
	    
	    return $targetGroupAry;
	}
	
	function getClubAndEventInChargeUser($CurrentUserID)
	{
	    global $intranet_root;
	    
	    include_once($intranet_root."/includes/libclubsenrol.php");
	    $libenroll = new libclubsenrol();
	    $managedEnrolGroupIDs = $libenroll->Get_PIC_Club($CurrentUserID);
	    $managedEventIDs = $libenroll->Get_PIC_Activity($CurrentUserID);
	    $enrolGroupStudentIdAry = $libenroll->Get_Club_Student_List($managedEnrolGroupIDs, 2);
	    
	    $clubAndEventUser = array();
	    $eventParticipantIDAry = array();
	    // get event paticipants
	    foreach ((array)$managedEventIDs as $managedEventID) {
	        $eventParticipantInfoAry = $libenroll->Get_Activity_Participant_Info($managedEventID);
	        
	        foreach ((array)$eventParticipantInfoAry as $eventParticipantInfo) {
	            $eventParticipantIDAry[] = $eventParticipantInfo['UserID'];
	        }
	    }
	    
	    $clubAndEventUser = array_merge((array)$enrolGroupStudentIdAry, (array)$eventParticipantIDAry);
	    
	    return $clubAndEventUser;
	}
	
	function getClassUserOfTeacherClass($CurrentUserID)
	{
	    global $intranet_root;
	    include_once($intranet_root."/includes/form_class_manage.php");
	    include_once($intranet_root."/includes/libgrouping.php");
	    $form_class_manage = new form_class_manage();
	    $libgrouping = new libgrouping();
	    $classAry = $form_class_manage->Get_Class_Teacher_Class($CurrentUserID);
	    
	    foreach ((array)$classAry as $classInfo) {
	        $filerGroupAry[] = $classInfo['GroupID'];
	    }
	    
	    $classGroupStudentIdAry = array();
	    $classGroupStudentAry = $libgrouping->returnGroupUsers($filerGroupAry);
	    foreach ((array)$classGroupStudentAry as $classGroupStudent) {
	        $classGroupStudentIdAry[] = $classGroupStudent["UserID"];
	    }
	    
	    return $classGroupStudentIdAry;
	}
	
	/*
	 *     return array(
	 *         [] = array(
	 *             'ID' => $_id,
	 *             'Name' => $_name,
	 *             'EnglishName => $_EnglishName,
	 *             'ChineseName => $_ChineseName,
	 *             'ThumbnailUrl => $_photoFilePath
	 *         )
	 *     )
	 */
	function WS_GetCommonChooseUsers($CurrentUserID, $TargetID='', $GroupID='', $PermittedUserType=1, $UserRecordStatus=1, $ClassName='', $FilterAppParent=0, $FilterNotAppParent=0, $FilterAppStudent=0, $FilterAppStudentByStudentApp=0, $FilterAppTeacher= 0, $IncludeNoPushMessageUser=0, $FilterUser='', $FilterClubAndEventInCharge=false, $FilterClassTeacherClass=false, $FromDR=0, $AlumniYear='')
	{
	    global $intranet_root, $sys_custom, $plugin, $eclassAppConfig;
	    
	    include_once($intranet_root."/includes/libuser.php");
	    include_once($intranet_root."/includes/libclass.php");
	    
	    $luser = new libuser();
	    $lclass = new libclass();
	    
	    switch ($TargetID) {
	        case '-1':     // DHL Staff
	                       // not handled yet
	            break;
	            
	        case '-2':     // Student
	            $filterUserIdAry = '';
	            if ($FilterAppStudent) {
	                $filterUserIdAry = $luser->getStudentWithParentUsingParentApp($IncludeNoPushMessageUser);
	            }
	            if ($FilterAppStudentByStudentApp) {
	                $filterUserIdAry = array_values(array_unique(array_merge((array)$filterUserIdAry, $luser->getStudentWithStudentUsingStudentApp($IncludeNoPushMessageUser))));
	            }
	            if($FilterUser!= ''){
	                $filterUserIdAry = $FilterUser;
	            }
	            if ($FilterClubAndEventInCharge || $FilterClassTeacherClass) {
	                
	                $UserInClassAndClubAndEvent = array();
	                
	                if ($FilterClubAndEventInCharge) {
	                    $UserInClassAndClubAndEvent = $this->getClubAndEventInChargeUser($CurrentUserID);
	                }
	                
	                $classGroupStudentIdAry = array();
	                if ($FilterClassTeacherClass) {
	                    $classGroupStudentIdAry = $this->getClassUserOfTeacherClass($CurrentUserID);
	                    $UserInClassAndClubAndEvent = array_merge((array)$UserInClassAndClubAndEvent, (array)$classGroupStudentIdAry);
	                }
	                $filterUserIdAry = array_intersect((array)$filterUserIdAry, (array)$UserInClassAndClubAndEvent);
	            }
	        
    	        if ($ClassName) {
    	            $sortByClass = 1;
    	            $row_user = $lclass->getStudentNameListWClassNumberByClassName($ClassName, $UserRecordStatus, $filterUserIdAry);
	            }
	            else {
	                $row_user = $luser->returnUsersByIdentity($userType=2, $teaching=0, "", $UserRecordStatus, "", $filterUserIdAry);
	            }
	            
	            break;
	            
	        case '-3':     // Parent
	            $FilterAppParent = ($FilterAppParent)? $FilterAppParent : 0;
	            $sortByClass = 1;
	            # retrieve student list first
	            $StudentList = $lclass->getClassStudentList($ClassName, "1");
	            $StudentIDStr = implode(",",$StudentList);
	            
	            $ParentArr = $luser->getParent($StudentIDStr, $sortByClass, 0, $UserRecordStatus);
	            
	            if ($FilterNotAppParent) {
	                $AppParentArr = $luser->getParent($StudentIDStr, $sortByClass, 1, $UserRecordStatus);
	                
	                foreach ((array)$AppParentArr as $AppParent) {
	                    $AppParentIDArr[] = $AppParent['ParentID'];
	                }
	                
	                foreach ((array)$ParentArr as $index=>$Parent) {
	                    if (in_array($Parent['ParentID'], (array)$AppParentIDArr)) {
	                        unset($ParentArr[$index]);
	                    }
	                }
	                
	                # reorder array
	                $ParentArr = array_values($ParentArr);
	                
	            } else if ($FilterAppParent) {
	                $ParentArr = $luser->getParent($StudentIDStr, $sortByClass, 1, $UserRecordStatus);
	            } else {
	                // do nothing
	            }
	            $row_user = $ParentArr;
	            break;
	            
	        case '-4':     // Alumni
	            $row_user = $luser->returnUsersByIdentity(4,1,"",$UserRecordStatus, $AlumniYear);
	            break;
	        
	        default:
	            if ($plugin['ASLParentApp'] || $plugin['eClassApp'] || $plugin['eClassTeacherApp']) {
	                include_once($intranet_root."/includes/eClassApp/libeClassApp.php");
	                $libeClassApp = new libeClassApp();
	            }
	            
	            if ($TargetID < 0) {   // -99 - Teaching Staff, -100 - Non-Teaching Staff
	                $filterUserIdAry = '';
	                if ($FilterAppTeacher) {
	                    $eClassAppSettingsObj = $libeClassApp->getAppSettingsObj();
	                    $loggedOutTeacherCanReceivePushMessage= $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut']);
	                    $filterUserIdAry = $luser->getTeacherWithTeacherUsingTeacherApp($IncludeNoPushMessageUser, $loggedOutTeacherCanReceivePushMessage);
	                }
	                if($FilterUser != ''){
	                    $filterUserIdAry = $FilterUser;
	                }
	                
	                if (!$FilterAppTeacher && !$FilterUser) {
	                    $filterUserIdAry = '';
	                }
	                $teaching = $TargetID == -99 ? 1 : 0;
	                $row_user = $luser->returnUsersByIdentity($userType=1, $teaching, "", $UserRecordStatus, "", $filterUserIdAry);
	            }
	            else {      // Other Groups
	                $permittedUserTypeAry = explode(',',$PermittedUserType);
	                
	                if ($TargetID[0] == 'i') {	                    
	                    $aliasId = substr($TargetID, 1);
	                    $recordType = implode("','",$permittedUserTypeAry);
	                    $namefield = getNameFieldWithClassNumberByLang("b.");
	                    $sql = "SELECT
                	                    DISTINCT a.TargetID,$namefield
                	            FROM
                	                    INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as a
                	            INNER JOIN
                	                    INTRANET_USER as b
                	            ON
                	                    a.TargetID = b.UserID
                	            WHERE
                	                    a.AliasID = '$aliasId'
                	            AND
                	                    b.RecordType IN ('".$recordType."')
                				ORDER BY
                					b.EnglishName ";
	                    $row_user = $luser->returnArray($sql);
	                }
	                else {
	                    if (($TargetID == '') && ($GroupID == '')) {
	                        $name_field = getNameFieldWithClassNumberByLang("iu.", $sys_custom['hideTeacherTitle'], $sys_custom['eEnrolment']['ShowStudentNickName']);
	                        $AcadmicYearID = Get_Current_Academic_Year_ID();
	                        
	                        $sql = "SELECT
	                                       iu.UserID,
	                                       $name_field AS OutputName
	                               FROM
	                                       INTRANET_USER iu
	                                       LEFT JOIN YEAR_CLASS_USER ycu ON iu.UserID = ycu.UserID
	                                       LEFT JOIN YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '$AcadmicYearID'
	                               WHERE
                	                       RecordStatus=1
                                           AND (iu.RecordType <> 2 OR (ycu.ClassNumber <> '' AND yc.YearClassID <> '' AND ycu.ClassNumber IS NOT NULL AND yc.YearClassID IS NOT NULL))
                	               ORDER BY
                	                       iu.ClassName, iu.ClassNumber, iu.EnglishName";
	                        $row_user = $luser->returnArray($sql);
	                    }
	                    else {
    	                    include_once($intranet_root."/includes/libgrouping.php");
    	                    $libgrouping = new libgrouping();
    	                    
    	                    $filterUserIdAry = '';
    	                    if ($FilterAppStudent) {
    	                        $filterStudentUserIdAry = $luser->getStudentWithParentUsingParentApp($IncludeNoPushMessageUser);
    	                    }
    	                    if ($FilterAppStudentByStudentApp) {
    	                        $filterStudentUserIdAry = array_values(array_unique(array_merge((array)$filterStudentUserIdAry, $luser->getStudentWithStudentUsingStudentApp($IncludeNoPushMessageUser))));
    	                    }
    	                    if ($FilterAppTeacher) {
    	                        $eClassAppSettingsObj = $libeClassApp->getAppSettingsObj();
    	                        $loggedOutTeacherCanReceivePushMessage = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut']);
    	                        
    	                        $filterTeacherUserIdAry = $luser->getTeacherWithTeacherUsingTeacherApp($IncludeNoPushMessageUser, $loggedOutTeacherCanReceivePushMessage);
    	                    }
    	                    if ($FilterAppParent) {
    	                        $filterParentUserIdAry = $luser->getParentUsingParentApp('', $IncludeNoPushMessageUser);
    	                    }
    	                    
    	                    if($FilterUser != ''){
    	                        $filterUserIdAry = $FilterUser;
    	                    }
    	                    else {
    	                        $filterUserIdAry = array_values(array_unique(array_remove_empty(array_merge((array)$filterStudentUserIdAry, (array)$filterTeacherUserIdAry, (array)$filterParentUserIdAry))));
    	                    }
    	                    
    	                    if ($filterClubAndEventInCharge || $filterClassTeacherClass) {
    	                        
    	                        $UserInClassAndClubAndEvent = array();
    	                        
    	                        if ($FilterClubAndEventInCharge) {
    	                            $UserInClassAndClubAndEvent = $this->getClubAndEventInChargeUser($CurrentUserID);
    	                        }
    	                        
    	                        $classGroupStudentIdAry = array();
    	                        if ($FilterClassTeacherClass) {
    	                            $classGroupStudentIdAry = $this->getClassUserOfTeacherClass($CurrentUserID);
    	                            $UserInClassAndClubAndEvent = array_merge((array)$UserInClassAndClubAndEvent, (array)$classGroupStudentIdAry);
    	                        }
    	                        $filterUserIdAry = array_intersect((array)$filterUserIdAry, (array)$UserInClassAndClubAndEvent);
    	                    }
    	                    
    	                    if (!$FilterAppStudent && !$FilterAppStudentByStudentApp && !$FilterAppTeacher && !$FilterAppParent && !$FilterUser) {
    	                        $filterUserIdAry = '';
    	                    }
    	                    
    	                    if($FromDR == 1 && isset($sys_custom['DocRouting']['SpecialGroup']) && is_array($sys_custom['DocRouting']['SpecialGroup']) && count($sys_custom['DocRouting']['SpecialGroup'])>0)
    	                    {
    	                        $row_user = $libgrouping->returnGroupUsersInIdentity($GroupID, $permittedUserTypeAry, "", "",$UserRecordStatus, $filterUserIdAry, $sys_custom['DocRouting']['SpecialGroup']);
    	                    }else{
    	                        $row_user = $libgrouping->returnGroupUsersInIdentity($GroupID, $permittedUserTypeAry, "", "",$UserRecordStatus, $filterUserIdAry);
    	                    }
	                    }
	                }
	            }
	            break;
	    }
	    
	    $ret = array();
	    for ($i=0, $iMax=count($row_user); $i<$iMax; $i++)
	    {
	        list($_userID,$_userName) = $row_user[$i];
	        $_userPhotoAry = $luser->GET_OFFICIAL_PHOTO_BY_USER_ID($_userID);
	        if (count($_userPhotoAry)) { 
                $_photoLink = $_userPhotoAry[2];
	        }
	        else {
	            $_photoLink= '';
	        }
	        $sql = "SELECT EnglishName, ChineseName FROM INTRANET_USER WHERE UserID='".$_userID."'";
	        $_userAry = $this->returnResultSet($sql);
	        if (count($_userAry)) {
	            $_EnglishName = $_userAry[0]['EnglishName'];
	            $_ChineseName = $_userAry[0]['ChineseName'];
	        }
	        else {
	            $sql = "SELECT EnglishName, ChineseName FROM INTRANET_ARCHIVE_USER WHERE UserID='".$_userID."'";
	            $_userAry = $this->returnResultSet($sql);
	            if (count($_userAry)) {
	                $_EnglishName = $_userAry[0]['EnglishName'];
	                $_ChineseName = $_userAry[0]['ChineseName'];
	            }
	            else {
	                $_EnglishName = '';
	                $_ChineseName = '';
	            }
	        }
	        
	        $ret[] = array('ID'=>$_userID, 'Name'=>$_userName, 'EnglishName'=>$_EnglishName, 'ChineseName'=>$_ChineseName, 'ThumbnailUrl'=>$_photoLink);
	    }
	    return $ret;
	}
	
	// $CurrentUserID is not used at present
	function WS_DigitalChannelGetAlbumDetails($CurrentUserID, $AlbumID, $CategoryCode='')
	{
	    global $intranet_root, $junior_mck;
	    include_once ($intranet_root. '/includes/DigitalChannels/libdigitalchannels.php');
	    $ldc = new libdigitalchannels();
	    $albumAry = $ldc->Get_Album($AlbumID, $CategoryCode);
	    $ret = array();
	    foreach((array) $albumAry as $_albumAry) {
	        $albumDetail = array();
	        $_albumID = $_albumAry['AlbumID'];
	        $albumDetail['Description'] = $_albumAry['Description'];
	        $albumDetail['ChiDescription'] = $_albumAry['ChiDescription'];
	        $albumDetail['Title'] = $_albumAry['Title'];
	        $albumDetail['ChiTitle'] = $_albumAry['ChiTitle'];
	        $albumDetail['DateTaken'] = $_albumAry['DateTaken'];
	        $albumDetail['StartTime'] = $_albumAry['SharedSince'];
	        $albumDetail['EndTime'] = $_albumAry['SharedUntil'];
	        
	        $_shareTo = '0';   // default is private
	        $sql = "SELECT COUNT(*) AS NumShare FROM INTRANET_DC_ALBUM_USER WHERE RecordType = 'all' AND AlbumID = '" . $_albumID."'";
	        $_shareToAllAry = $this->returnResultSet($sql);
	        if (count($_shareToAllAry)) {
	            $_numShare = $_shareToAllAry[0]['NumShare'];
	            if ($_numShare) {
	                $_shareTo = '1';
	            }
	        }
	        
	        $_groupName = Get_Lang_Selection('g.TitleChinese', 'g.Title');
	        $sql = "SELECT 	    c.CategoryName,
	                            g.GroupID, ".
                                $_groupName." AS GroupTitle
	                FROM 
                                INTRANET_DC_ALBUM_USER u
	                            LEFT JOIN INTRANET_GROUP g ON g.GroupID=u.RecordID
	                            LEFT JOIN INTRANET_GROUP_CATEGORY c ON c.GroupCategoryID=g.RecordType
	                WHERE
	                            u.RecordType='group'
	                            AND u.AlbumID='" . $_albumID."'";
	        $_groupAry = $this->returnResultSet($sql);
	        $_targetGroups = array();
	        if (count($_groupAry)) {
	            if($junior_mck > 0){
	                foreach((array)$_groupAry as $__groupAry) {
	                    $__name = '('.convert2unicode($__groupAry['CategoryName']).')'.convert2unicode($__groupAry['GroupTitle']);
	                    
	                    $_targetGroups[] = array('ID'=>$__groupAry['GroupID'], 'Name'=>$__name);
	                }
	            }
	            else {
    	            foreach((array)$_groupAry as $__groupAry) {
    	                $__name = '('.$__groupAry['CategoryName'].')'.$__groupAry['GroupTitle'];
    	                $_targetGroups[] = array('ID'=>$__groupAry['GroupID'], 'Name'=>$__name);
    	            }
	            }
	            $_shareTo = '2';	            
	        }
	        
	        $_pics = array();
	        $_picAry = $ldc->getAlbumPICs($_albumID);
	        foreach((array)$_picAry as $__picAry) {
	            $_pics[] = array('ID'=>$__picAry['UserID'], 'Name'=>$__picAry['NAME']);
	        }
	        
	        $albumDetail['ShareTo'] = $_shareTo;
	        $albumDetail['TargetGroups'] = $_targetGroups;
	        $albumDetail['PersonInCharges'] = $_pics;
	        $ret[] = $albumDetail;
	    }
	    return $ret;
	}
	
	function WS_DigitalChannelUploadPhotoDetails($CurrentUserID, $AlbumID, $FileID=0, $FileName='', $FileDescription='', $FileChiDescription='', $ChunkNo=0, $TotalNoOfChunk=0, $FileByteStream='', $ChunkCheck='', $FileCheck='', $IsReady=false, $AllChunkCheck='', $IsRetry=false, $UploadDir=null, $TmpFilePrefix='tdc_') 
	{
	    global $file_path, $FF_path, $intranet_root;
	    
	    $ReturnContent = array ();
	    
	    if (! is_null ( $UploadDir )) {
	        $imgdir = $file_path. "/file/digital_channels/upload_by_app/" . $UploadDir;
	    } else {
	        $imgdir = $file_path. "/file/digital_channels/upload_by_app";
	    }
	    
	    $TmpFilePrefix .= $CurrentUserID;
	    
	    if ($IsReady && $IsRetry && $FileName) {
	        $FinalFileName = stripslashes ( $FileName );
	        $FinalFileDest = $imgdir . "/" . $FinalFileName;
	        
	        if (is_file ( $FinalFileDest )) {
	            @exec ( 'rm -f ' . OsCommandSafe($imgdir) . '/' . OsCommandSafe($TmpFilePrefix) . '_*' );
	            
	            if (isImage ( $FinalFileDest )) {
	                list ( $img_width, $img_height, $img_type, $img_attr ) = getimagesize ( $FinalFileDest );
	                $ReturnContent ['fileVar'] ['width'] = $img_width;
	                $ReturnContent ['fileVar'] ['height'] = $img_height;
	                $ReturnContent ['fileVar'] ['type'] = $img_type;
	                $ReturnContent ['fileVar'] ['attr'] = $img_attr;
	            }
	            
	            $ReturnContent ['fileVar'] ['path'] = $imgdir . '/' . $FinalFileName;
	            $ReturnContent ['fileVar'] ['encodedPath'] = $imgdir . '/' . encode_filenames ( $FinalFileName );
	            $ReturnContent ['fileVar'] ['encodedPathForFlash'] = $imgdir . '/' . encode_url_for_flashvars ( $FinalFileName );
	            $ReturnContent ['fileVar'] ['ecfpath'] = encrypt_string ( 'fileupload/' . $UploadDir . '/' . $FinalFileName );
	            
	            $ReturnContent ['fileVar'] ['file_size'] = filesize ( $FinalFileDest );
	            $ReturnContent ['fileName'] = $FinalFileName;
	            $ReturnContent ['isSuccess'] = "1";
	            
	            include_once ($intranet_root. '/includes/DigitalChannels/libdigitalchannels.php');
	            $ldc = new libdigitalchannels();
	            $addPhotoResult = $ldc->addAlbumPhotoByApp($AlbumID, $ReturnContent);
	            if ($addPhotoResult['photo_id']) {
	                $ldc->updateAlbumPhoto($addPhotoResult['photo_id'], array('description'=>$FileDescription, 'chiDescription'=>$FileChiDescription), $CurrentUserID);
	            }
	            
	            return $ReturnContent;
	        }
	    }
	    
	    if (! is_null ( $FileByteStream )) {
	        include_once ('libfilesystem.php');
	        $fs = new libfilesystem ();
	        $fs->createFolder ( $imgdir );
	        
	        // tmp file dest path
	        $ZeroPaddedChunkNo = str_pad ( $ChunkNo, strlen ( $TotalNoOfChunk ), 0, STR_PAD_LEFT );
	        $CurrentChunkFileName = stripslashes ( $TmpFilePrefix . '_file_' . $ZeroPaddedChunkNo );
	        $CurrentChunkFileDest = $imgdir . "/" . $CurrentChunkFileName;
	        $CurrentChunkCheckFileName = stripslashes ( $TmpFilePrefix . '_check_' . $ZeroPaddedChunkNo );
	        $CurrentChunkCheckFileDest = $imgdir . "/" . $CurrentChunkCheckFileName;
	        
	        // extract filebyte from cdata
	        $decoded = base64_decode ( $FileByteStream );
	        $decodedMD5 = strtolower ( md5 ( $decoded ) );
	        if ($decodedMD5 == strtolower ( $ChunkCheck )) {
	            if (is_file ( $CurrentChunkFileDest )) {
	                unlink ( $CurrentChunkFileDest );
	            }
	            
	            $CurrentChunkFile = fopen ( $CurrentChunkFileDest, 'wb' );
	            fwrite ( $CurrentChunkFile, $decoded );
	            fclose ( $CurrentChunkFile );
	            
	            if (is_file ( $CurrentChunkCheckFileDest )) {
	                unlink ( $CurrentChunkCheckFileDest );
	            }
	            
	            $CurrentChunkCheckFile = fopen ( $CurrentChunkCheckFileDest, 'wb' );
	            fwrite ( $CurrentChunkCheckFile, $decodedMD5 );
	            fclose ( $CurrentChunkCheckFile );
	            
	            if ($IsReady) {
	                if ($FileName && strpos ( $FileName, "." ) != 0) {
	                    // Check all chunk exist or not
	                    $IsAllChunkReady = true;
	                    for($i = 0; $i < $TotalNoOfChunk; $i ++) {
	                        $ZeroPaddedChunkNo = str_pad ( $i, strlen ( $TotalNoOfChunk ), 0, STR_PAD_LEFT );
	                        if (! is_file ( $imgdir . "/" . $TmpFilePrefix . '_file_' . $ZeroPaddedChunkNo ) || ! is_file ( $imgdir . "/" . $TmpFilePrefix . '_check_' . $ZeroPaddedChunkNo )) {
	                            $IsAllChunkReady = false;
	                            break;
	                        }
	                    }
	                    
	                    if ($IsAllChunkReady) {
	                        $FinalFileName = stripslashes ( $FileName );
	                        $FinalFileDest = $imgdir . "/" . $FinalFileName;
	                        
	                        @exec ( 'find ' . OsCommandSafe($imgdir) . ' -name "' . OsCommandSafe($TmpFilePrefix) . '_file_*" | sort | xargs cat > ' . OsCommandSafe($FinalFileDest) );
	                        
	                        $passedFileCheck = false;
	                        if (! empty ( $AllChunkCheck )) {
	                            $FinalCheckFileName = $TmpFilePrefix . '_check';
	                            $FinalCheckFileDest = $imgdir . "/" . $FinalCheckFileName;
	                            
	                            @exec ( 'find ' . OsCommandSafe($imgdir) . ' -name "' . OsCommandSafe($TmpFilePrefix) . '_check_*" | sort | xargs cat > ' . OsCommandSafe($FinalCheckFileDest) );
	                            $passedFileCheck = strtolower ( md5_file ( $FinalCheckFileDest ) ) == strtolower ( $AllChunkCheck );
	                        } else if (! empty ( $FileCheck )) {
	                            $passedFileCheck = strtolower ( md5_file ( $FinalFileDest ) ) == strtolower ( $FileCheck );
	                        }
	                        
	                        if ($passedFileCheck) {
	                            
	                            @exec ( 'rm -f ' . OsCommandSafe($imgdir) . '/' . OsCommandSafe($TmpFilePrefix) . '_*' );
	                            
	                            $file_name = strtolower ( substr ( $FinalFileName, 0, strrpos ( $FinalFileName, "." ) ) );
	                            $ext = strtolower ( substr ( $FinalFileName, strrpos ( $FinalFileName, "." ) ) );
	                            
	                            // voice, media file conversion for iPad and Android
	                            if ($ext == ".3ogg") {
	                                $cmd = (isset ( $FF_path ) && $FF_path != '') ? OsCommandSafe($FF_path) . "/ffmpeg" : "/usr/local/bin/ffmpeg";
	                                $extra_cmd = '';
	                                
	                                $targetFileName = $file_name . ".mp3";
	                                $targetDes = $imgdir . "/" . $targetFileName;
	                                $cmd .= " -y -i '" . OsCommandSafe($FinalFileDest) . "' " . OsCommandSafe($extra_cmd) . " -acodec libmp3lame -ar 22050 '" . OsCommandSafe($targetDes) . "' ";
	                                
	                                $ReturnContent ['fileName'] = $targetFileName;
	                                
	                                $ret = exec ( $cmd, $dum_out_1, $dum_out_2 );
	                                if ($ret == 0) {
	                                    chmod ( $targetDes, 0777 );
	                                    $ReturnContent ['isSuccess'] = "1";
	                                } else {
	                                    $ReturnContent ['isSuccess'] = "0";
	                                }
	                            } else {
	                                if (isImage ( $FinalFileDest )) {
	                                    list ( $img_width, $img_height, $img_type, $img_attr ) = getimagesize ( $FinalFileDest );
	                                    $ReturnContent ['fileVar'] ['width'] = $img_width;
	                                    $ReturnContent ['fileVar'] ['height'] = $img_height;
	                                    $ReturnContent ['fileVar'] ['type'] = $img_type;
	                                    $ReturnContent ['fileVar'] ['attr'] = $img_attr;
	                                }
	                                
	                                $ReturnContent ['fileVar'] ['path'] = $imgdir . '/' . $FinalFileName;
	                                $ReturnContent ['fileVar'] ['encodedPath'] = $imgdir . '/' . encode_filenames ( $FinalFileName );
	                                $ReturnContent ['fileVar'] ['encodedPathForFlash'] = $imgdir . '/' . encode_url_for_flashvars ( $FinalFileName );
	                                $ReturnContent ['fileVar'] ['ecfpath'] = encrypt_string ( 'fileupload/' . $UploadDir . '/' . $FinalFileName );
	                                
	                                $ReturnContent ['fileVar'] ['file_size'] = filesize ( $FinalFileDest );
	                                $ReturnContent ['fileName'] = $FinalFileName;
	                                $ReturnContent ['isSuccess'] = "1";
	                                
	                                include_once ($intranet_root. '/includes/DigitalChannels/libdigitalchannels.php');
	                                $ldc = new libdigitalchannels();
	                                $addPhotoResult = $ldc->addAlbumPhotoByApp($AlbumID, $ReturnContent);
	                                if ($addPhotoResult['photo_id']) {
	                                    $ldc->updateAlbumPhoto($addPhotoResult['photo_id'], array('description'=>$FileDescription, 'chiDescription'=>$FileChiDescription), $CurrentUserID);
	                                }
	                            }
	                        } else {
	                            // Error - final file corrupted
	                            return 109;
	                        }
	                    } else {
	                        // Error - some chunk missing
	                        return 108;
	                    }
	                } else {
	                    // Error - invalid file name
	                    return 105;
	                }
	            } else {
	                // Return Upload Chunk Result
	                $ReturnContent ['fileName'] = $FileName;
	                $ReturnContent ['isSuccess'] = "1";
	            }
	            
	            return $ReturnContent;
	        } else {
	            // Error - chunk corrupted
	            return 107;
	        }
	    } else {
	        // Error - no data received
	        return 106;
	    }
	}
	
	function WS_DigitalChannelPhotoView($CurrentUserID, $PhotoID) 
	{
	    $sql = "SELECT RecordID FROM INTRANET_DC_ALBUM_PHOTO_VIEW WHERE UserID='".$CurrentUserID."' AND PhotoID='".$PhotoID."' LIMIT 1";
	    $photoViewAry = $this->returnResultSet($sql);
	    if (count($photoViewAry)) {
	        $ret = array('Result'=>true);
	    }
	    else {
	        $sql = "INSERT INTO INTRANET_DC_ALBUM_PHOTO_VIEW (UserID, PhotoID, DateModified) VALUES ('".$CurrentUserID."','".$PhotoID."',NOW())";
	        $ret = $this->db_db_query($sql);
	        $ret = array('Result'=>$ret);
	    }
	    return $ret;
	}

	// $CurrentUserID is not used at present
	function WS_DigitalChannelDeleteAlbum($CurrentUserID, $AlbumID)
	{
	    global $intranet_root, $junior_mck;
	    include_once ($intranet_root. '/includes/DigitalChannels/libdigitalchannels.php');
	    
	    $ldc = new libdigitalchannels();
	    $ret = $ldc->Remove_Album($AlbumID);
        $ret = array('Result'=>$ret);
	    return $ret;
	}

	// $CurrentUserID is not used at present
	function WS_DigitalChannelDeletePhotosForAlbum($CurrentUserID, $AlbumID, $PhotoID)
	{
	    global $intranet_root, $junior_mck;
	    include_once ($intranet_root. '/includes/DigitalChannels/libdigitalchannels.php');
	    
	    $ldc = new libdigitalchannels();
	    $photoIDAry = explode(',', $PhotoID);
	    $ret = $ldc->removeAlbumPhotos($AlbumID, $photoIDAry);
	    $ret = array('Result'=>$ret);
	    return $ret;
	}
	
	function WS_iCalendarSystemSettings()
	{
		global $intranet_root;
		include_once($intranet_root."/includes/icalendar.php");
		
		$iCal = new icalendar();
		
		$records = $iCal->getSystemSetting();
		
		return $records;
	}
	
	function WS_iCalendarUsers()
	{
		global $intranet_root;
		include_once($intranet_root."/includes/libuser.php");
		
		$lu = new libuser();
		$records = $lu->returnUsersType(USERTYPE_STAFF);

		$result = array();
		for ($i = 0; $i < sizeof($records); $i++) {
            $user = array();

            $user["IntranetUserID"] = $records[$i]["UserID"];
            $user["NameCh"] = $records[$i]["ChineseName"];
            $user["NameEn"] = $records[$i]["EnglishName"];

            $result[] = $user;
        }

		return $result;
	}

	function WS_iCalendarUserPreference($CurrentUserID,$SettingName)
	{
		global $intranet_root;
		include_once($intranet_root."/includes/icalendar.php");
		
		$iCal = new icalendar($CurrentUserID);
		
		$records = $iCal->returnUserPref($CurrentUserID, $SettingName);
		return $records;
	}
	
	function WS_iCalendarUserCalendars($CurrentUserID)
	{
		global $intranet_root, $plugin;
		include_once($intranet_root."/includes/libuser.php");
		include_once($intranet_root."/includes/icalendar.php");
		
		$libuser = new libuser($CurrentUserID);
		$iCal = new icalendar($CurrentUserID);
		$iCal_api = new icalendar_api($CurrentUserID);
		
		$user_type = $libuser->RecordType;
		
		$calendars = array();
		
		$own_calendars = $iCal->returnViewableCalendar(true);
		$own_calendar_size = count($own_calendars);
		for($i=0;$i<$own_calendar_size;$i++){
			foreach($own_calendars[$i] as $key => $val){
				if(is_numeric($key)){
					unset($own_calendars[$i][$key]);
				}
			}
		}
		// the first own calendar should be the default user calendar
		$calendars = array_merge($calendars, $own_calendars);
		
		$school_calendars = array();
		$school_calendars[] = array("CalID"=>$iCal_api->Public_Calendar["recordType"],"Name"=>$iCal_api->Public_Calendar["name"],"CalType"=>1,"Color"=>$iCal_api->Public_Calendar["color"]);
		$school_calendars[] = array("CalID"=>$iCal_api->Group_Event["recordType"],"Name"=>$iCal_api->Group_Event["name"],"CalType"=>1,"Color"=>$iCal_api->Group_Event["color"]);
		$school_calendars[] = array("CalID"=>$iCal_api->School_Event["recordType"],"Name"=>$iCal_api->School_Event["name"],"CalType"=>1,"Color"=>$iCal_api->School_Event["color"]);
		$school_calendars[] = array("CalID"=>$iCal_api->School_Holiday["recordType"],"Name"=>$iCal_api->School_Holiday["name"],"CalType"=>1,"Color"=>$iCal_api->School_Holiday["color"]);
		$school_calendars[] = array("CalID"=>$iCal_api->Academic_Event["recordType"],"Name"=>$iCal_api->Academic_Event["name"],"CalType"=>1,"Color"=>$iCal_api->Academic_Event["color"]);
			
		$calendars = array_merge($calendars, $school_calendars);
		
		$other_calendars = $iCal->returnViewableCalendar(false,true);
		$other_calendar_size = count($other_calendars);
		
		$group_calendars = array();
		$course_calendars = array();
		$public_calendars = array();
		$shared_calendars = array();
		$system_calendars = array();
		
		for($i=0;$i<$other_calendar_size;$i++){
			foreach($other_calendars[$i] as $key => $val){
				if(is_numeric($key)){
					unset($other_calendars[$i][$key]);
				}
			}
			$cal_type = $other_calendars[$i]['CalType'];
			if($cal_type == 2){
				$group_calendars[] = $other_calendars[$i];
			}else if($cal_type == 3){
				$course_calendars[] = $other_calendars[$i];
			}else if($cal_type == 1){
				// skip school calendar
			}else{
				if(strtoupper(trim($other_calendars[$i]['Access'])) == 'P'){
					$public_calendars[] = $other_calendars[$i];
				}else{
					$shared_calendars[] = $other_calendars[$i];
				}
			}
		}
		
		if(count($group_calendars)){
			$calendars = array_merge($calendars, $group_calendars);
		}
		if(count($course_calendars)){
			$calendars = array_merge($calendars, $course_calendars);
		}
		if(count($public_calendars)){
			$calendars = array_merge($calendars, $public_calendars);
		}
		if(count($shared_calendars)){
			$calendars = array_merge($calendars, $shared_calendars);
		}
		
		if ($plugin["iCalendarFull"] && $user_type==USERTYPE_STAFF || $plugin["iCalendarFullToAllUsers"]){
			$system_calendars[] = array("Name"=>$iCal_api->Enrollment["name"],"CalID"=>0,"Color"=>$iCal_api->Enrollment["color"],"CalType"=>'5-6');
			$system_calendars[] = array("Name"=>$iCal_api->Detention["name"],"CalID"=>0,"Color"=>$iCal_api->Detention["color"],"CalType"=>$iCal_api->Detention["CalType"]);
			$system_calendars[] = array("Name"=>$iCal_api->Homework["name"],"CalID"=>0,"Color"=>$iCal_api->Homework["color"],"CalType"=>$iCal_api->Homework["CalType"]);
			
			$calendars = array_merge($calendars, $system_calendars);
		}
		
		return $calendars;
	}
	
	function WS_iCalendarInitData($CurrentUserID)
	{
		$resultAry = array();
		$resultAry['SystemSettings'] = $this->WS_iCalendarSystemSettings();
		$resultAry['Users'] = $this->WS_iCalendarUsers();
		$resultAry['UserPreference'] = $this->WS_iCalendarUserPreference($CurrentUserID,'AllSetting');
		$resultAry['UserCalendars'] = $this->WS_iCalendarUserCalendars($CurrentUserID);
		
		return $resultAry;
	}
	
	function WS_iCalendarUserEvents($CurrentUserID,$StartTimestamp,$EndTimestamp)
	{
		global $intranet_root;
		include_once($intranet_root."/includes/icalendar.php");
		
		$iCal = new icalendar($CurrentUserID);
		
		$records = $iCal->Get_All_Related_Event($StartTimestamp, $EndTimestamp, true);
		
		$results = array();

		foreach ($records as $record) {
            $results = array_merge($results, $record);
        }

		$event_ids = Get_Array_By_Key($results,'EventID');
		if(count($event_ids)){
			$event_id_to_participants = $iCal->getEventParticipants($event_ids, true);
			$record_size = count($results);
			for($i=0;$i<$record_size;$i++){
				// remove the numeric indexed values, only keep the keyed values
				foreach($results[$i] as $key => $val){
					if(is_numeric($key)){
						unset($results[$i][$key]);
					}
				}
				$results[$i]['StartDate'] = $iCal->TimestampTosqlDatetime($iCal->sqlDatetimeToTimestamp($results[$i]['EventDate'])); // convert start date to standard datetime format Y-m-d H:i:s
				$results[$i]['EndDate'] = $iCal->convertToEventEndDatetime($results[$i]['StartDate'],$results[$i]['Duration']); // calculate end date from duration and convert to standard datetime format Y-m-d H:i:s 
				$results[$i]['Participants'] = isset($event_id_to_participants[$results[$i]['EventID']])? $event_id_to_participants[$results[$i]['EventID']] : array();
			}
		}
		
		return $results;
	}
	
	function WS_iCalendarEventDetail($CurrentUserID,$EventID,$CalType)
	{
		global $intranet_root;
		include_once($intranet_root."/includes/icalendar.php");
		
		$iCal = new icalendar($CurrentUserID);
		
		$records = $iCal->getEventDetail($CurrentUserID,$EventID,$CalType);
		
		return $records;
	}
	
	// $EventDataAry : post data, if contains eventID would do edit update, otherwise insert new event
	/*
	 * new event posted data:
	   $EventDataAry = Array
		(
		    [title] => Test new event 2019-05-29
		    [eventDate] => 2019-05-29
		    [eventHr] => 9
		    [eventMin] => 00
		    [eventEndDate] => 2019-05-29
		    [eventEndHr] => 10
		    [eventEndMin] => 00
		    [repeatSelect] => NOT
		    [repeatEnd] => 2019-06-29
		    [oldRepeatPattern] => 
		    [location] => 
		    [hiddenBookingIDs] => 
		    [calID] => 4420
		    [description] => 
		    [PersonalNote] => 
		    [url] => http://
		    [access] => P
		    [viewer] => Array
		        (
		            [0] => U1790
		        )
		
		    [InviteStatus] => 0
		    [submitMode] => save
		    [CalType] => 
		    [ProgrammeID] => 
		    [otherSchoolCode] => 
		)
		
		edit event posted data:
		$EventDataAry = Array
		(
		    [title] => Test event 2019-05-27
		    [eventDate] => 2019-05-27
		    [eventHr] => 9
		    [eventMin] => 00
		    [eventEndDate] => 2019-05-27
		    [eventEndHr] => 10
		    [eventEndMin] => 00
		    [oldEventDate] => 2019-05-27
		    [oldEventTime] => 09:00:00
		    [oldDuration] => 60
		    [repeatSelect] => NOT
		    [repeatEnd] => 2019-06-29
		    [oldRepeatPattern] => 
		    [location] => 
		    [hiddenBookingIDs] => 
		    [calID] => 4420
		    [description] => 
		    [PersonalNote] => 
		    [url] => http://
		    [access] => P
		    [InviteStatus] => 0
		    [existGuestList] => Array
		        (
		            [0] => 1790
		            [1] => 2155
		            [2] => 2544
		            [3] => 2545
		        )
		
		    [removeGuestList] => Array
		        (
		            [0] => -1
		            [1] => -1
		            [2] => -1
		            [3] => -1
		        )
		
		    [calAccess] => A
		    [createdBy] => 1790
		    [response] => A
		    [eventID] => 4606
		    [submitMode] => save
		    [CalType] => 0
		    [ProgrammeID] => 
		    [otherSchoolCode] => 
		)
	 */
	function WS_iCalendarUpsertEvent($CurrentUserID,$CalType,$EventDataAry)
	{
		global $intranet_root, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/icalendar.php");
		
		$iCal = new icalendar($CurrentUserID);
		
		if(isset($EventDataAry['eventID']) && $EventDataAry['eventID'] > 0){
			$success = $iCal->editEventUpdate($CurrentUserID,$EventDataAry,$handleBookingRecords=false);
		}else{
			$success = $iCal->newEventUpdate($CurrentUserID,$EventDataAry,$handleBookingRecords=false);
		}
		
		return array('Result'=>$success);
	}
	
	// $eventID
	// isRepeatEvent: 0 or 1
	// deleteRepeatEvent : OTI=Only this event instance; ALL=All events in the series; FOL=All following events
	/* posted data
	   $EventDataAry = Array
		(
		    [eventID] => 
		    [deleteRepeatEvent] => 
		    [isRepeatEvent] => 
		    [repeatID] => 
		    [oldEventDate] => 
		)
	 */
	function WS_iCalendarRemoveEvent($CurrentUserID,$EventDataAry)
	{
		global $intranet_root;
		include_once($intranet_root."/includes/icalendar.php");
		
		$iCal = new icalendar($CurrentUserID);
		
		$success = $iCal->removeEventUpdate($CurrentUserID,$EventDataAry,$handleBookingRecords=false);
		
		return array('Result'=>$success);
	}
	
	// choose users to a calendar or choose participants to an event
	/*
	 * $OptValue: First level selection of Identity or Group
	 * $ChooseGroupID (array): Second level selection of sub-category or groups
	 * $ChooseUserID (array): Second or third level selection of users. Some $OptValue only have two levels
	 */
	function WS_iCalendarChooseUsers($CurrentUserID,$OptValue='',$ChooseGroupID=array(),$ChooseUserID=array())
	{
		global $intranet_root;
		include_once($intranet_root."/includes/icalendar.php");
		
		$iCal = new icalendar($CurrentUserID);
		
		$result = $iCal->chooseUsers($CurrentUserID, $OptValue, $ChooseGroupID, $ChooseUserID);
		
		return array('Result'=>$result);
	}
	
	function syncPaymentDataToSchoolFromCentralServer($requestData)
	{
		global $intranet_root, $sys_custom, $PATH_WRT_ROOT, $eclassAppConfig, $config_school_code;

		include_once($intranet_root.'/includes/libeclassapiauth.php');
		include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');
		include_once($intranet_root."/includes/eClassApp/libAES.php");
		include_once($intranet_root."/includes/json.php");

		$lapiAuth = new libeclassapiauth();
		$libaes = new libAES($eclassAppConfig['aesKey']);
		$jsonObj = new JSON_obj();


		$platformApiKey = $lapiAuth->GetAPIKeyByProject('IP/EJ platform');
		$apiPath = $eclassAppConfig['paymentGateway']['apiPath'];

		$school_url = 'http';
		if (checkHttpsWebProtocol()) {$school_url .= "s";}
		$school_url .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
		  	$school_url .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
		} else {
		  	$school_url .= $_SERVER["SERVER_NAME"];
		}

		$requestAry = array();
		$requestAry['RequestMethod'] = 'syncDataToSchool';
		$requestAry['APIKey'] = $platformApiKey;
		$requestAry['Request']['SchoolCode'] = $config_school_code;
		$requestAry['Request']['SchoolURL'] = $school_url;

		// $requestData: PayerUserID, ServiceProvider

		foreach($requestData as $key => $val){
			$requestAry['Request'][$key] = $val;
		}
		$requestJsonString = $jsonObj->encode($requestAry);


		$jsonAry = array();
		$jsonAry['eClassRequestEncrypted'] = $libaes->encrypt($requestJsonString);
		$postJsonString = $jsonObj->encode($jsonAry);


		session_write_close();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_URL, $apiPath);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJsonString);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		$responseJson = curl_exec($ch);
		//if($responseJson === false){
		//	debug_pr(curl_error($ch));
		//}
		curl_close($ch);


		return $responseJson;
	}
	
	function WS_SendPushMessageFromCourse($courseId, $type, $id, $lang, $toParentApp=false, $toStudentApp=false, $pushMessageDate=null)
	{
	    global $intranet_root, $eclass40_filepath, $eclass_root, $intranet_db, $eclassAppConfig, $plugin;
	    
	    $filepath_prefix = $eclass_root ? $eclass_root : $eclass40_filepath;
	    include($filepath_prefix . "/system/settings/config.inc.php");
	    include_once($intranet_root."/includes/eClassApp/libeClassApp.php");
		include_once($intranet_root."/includes/libuser.php");
	    
		// May need to send teacher app if student app is selected because intranet teacher can be student of classroom
		$appTypes = array();
		if($toParentApp){
	        $appTypes[] = $eclassAppConfig['appType']['Parent'];
		}
	    if($toStudentApp){
	        $appTypes[] = $eclassAppConfig['appType']['Teacher'];
	        $appTypes[] = $eclassAppConfig['appType']['Student'];
	    }
	    
	    if($pushMessageDate){
            $sendTimeMode = $eclassAppConfig['pushMessageSendMode']['scheduled'];
            $sendTimeString = $pushMessageDate;
        }else{
            $sendTimeMode = $eclassAppConfig['pushMessageSendMode']['now'];
            $sendTimeString = null;
        }

	    $message = null;
	    
	    switch($type){
	        case $cfg['group_function_type']['econtent']:
	            $message = $this->SendPushMessageFromEcontent($courseId, $id, $lang);
	            break;
	        case $cfg['group_function_type']['assessment']:
	            $message = $this->SendPushMessageFromAssessment($courseId, $id, $lang);
	            break;
	        case 'PL2':
	            if(!$plugin['power_lesson_2']){
                    return 302; // Invalid data type       
	            }
	            $message = $this->SendPushMessageFromPowerLesson2($courseId, $id, $lang);
	            break;
	        default:
	            return 302; // Invalid data type
	    }
	    
	    if(!is_null($message)){
	        $appTargetUserIds = array(
	            $eclassAppConfig['appType']['Teacher'] => array(), 
	            $eclassAppConfig['appType']['Student'] => array(),
	            $eclassAppConfig['appType']['Parent']  => array()
	        );
    	    $notifyMessageIds = array();
	        
	        $sql     = "SELECT UserID, RecordType FROM " . $intranet_db . ".INTRANET_USER WHERE UserID in ('" . implode("','", $message['targetUserIds']) . "')";
	        $results = $this->returnArray($sql);
	        
	        foreach($results as $result){
	            switch(intval($result['RecordType'])){
	                case 1: // Teacher
	                    $appTargetUserIds[$eclassAppConfig['appType']['Teacher']][] = intval($result['UserID']);
	                    break;
	                case 2: // Student
	                    $appTargetUserIds[$eclassAppConfig['appType']['Student']][] = intval($result['UserID']);
	                    $appTargetUserIds[$eclassAppConfig['appType']['Parent']][] = intval($result['UserID']);
	                    break;
	            }
	        }
	        
    	    $libeClassApp = new libeClassApp();
    	    
    	    foreach($appTargetUserIds as $appType => $targetUserIds){
    	        if(in_array($appType, $appTypes) && count($targetUserIds) > 0){
    	            $individualMessageInfoAry = array(
        	            array('relatedUserIdAssoAry' => array())
        	        );
        	        
    	            if($appType === $eclassAppConfig['appType']['Parent']){
    	                $luser = new libuser();
    	                $individualMessageInfoAry[0]['relatedUserIdAssoAry'] = BuildMultiKeyAssoc(
    	                    $luser->getParentStudentMappingInfo(
    	                        $targetUserIds, 
    	                        '', 
    	                        false, 
    	                        $parentStatusCheck=true
    	                    ), 
    	                    'ParentID', 
    	                    $IncludedDBField=array('StudentID'), 
    	                    $SingleValue=1, 
    	                    $BuildNumericArray=1
    	                );
    	            } else {
                        foreach($targetUserIds as $targetUserId){
        	               $individualMessageInfoAry[0]['relatedUserIdAssoAry'][$targetUserId] = array($libeClassApp->getDemoSiteUserId($targetUserId));
        	            }    	                
    	            }
        	        
        	        $notifyMessageIds[] = $libeClassApp->sendPushMessage(
            	        $individualMessageInfoAry, 
            	        $message['title'], 
            	        $message['content'], 
            	        $isPublic='N', 
            	        $recordStatus=1, 
            	        $appType,
                        $sendTimeMode,
                        $sendTimeString
            	    );
    	        }
    	    }
    	    
    	    return $notifyMessageIds;
	    } else {
	        return 815; // Operation Failed.
	    }
	}
	
    function WS_DeleteScheduledPushMessageFromCourse($messageIds)
    {
        global $intranet_root, $eclassAppConfig, $config_school_code;

        include_once($intranet_root."/includes/eClassApp/libeClassApp.php");

        $libeClassApp = new libeClassApp();

        $success = $libeClassApp->deleteScheduledPushMessageInServer(array($messageIds));

        return array('success'=>$success);

    }


	function SendPushMessageFromEcontent($courseId, $id, $lang)
	{
	    global $eclass40_filepath, $eclass_root, $UserID;
	    
	    $filepath_prefix = $eclass_root ? $eclass_root : $eclass40_filepath;
	    include($filepath_prefix . "/system/settings/config.inc.php");
		
		# Get IDs of Target User 
	    $targetUserIds = $this->GetIntranetUserIdsOfTargetUsersOfCourseFunction($courseId, $cfg['group_function_type']['econtent'], $id);
		
	    # Get Teacher Name
		$teacherName = $this->GetUserName($UserID, $lang);
	    
	    # Get Course Name
	    $courseName = $this->GetCourseName($courseId);
	    
	    # Get eContent Title
	    $sql = "SELECT title FROM " . classNamingDB($courseId) . ".notes WHERE notes_id = '" . $id . "'";
	    $title = current($this->returnVector($sql));
	    
		if (file_exists ( $filepath_prefix . "/src/lang/lang.$lang.php" )) {
			include_once ($filepath_prefix . "/src/lang/lang.$lang.php");
		}
	    
	    return array(
	        'targetUserIds' => $targetUserIds,
	        'title'         => $Lang['eContent']['pushMessageTitle'],
	        'content'       => str_replace(
	            array('[TEACHER_NAME]','[COURSE_NAME]','[TITLE]'), 
	            array($teacherName, $courseName, $title), 
	            $Lang['eContent']['pushMessageTemplate']
	        )
	    );
	}
	
	function SendPushMessageFromAssessment($courseId, $id, $lang)
	{
	    global $eclass40_filepath, $eclass_root, $UserID;
	    
	    $filepath_prefix = $eclass_root ? $eclass_root : $eclass40_filepath;
	    include($filepath_prefix . "/system/settings/config.inc.php");
		
		# Get IDs of Target User 
	    $targetUserIds = $this->GetIntranetUserIdsOfTargetUsersOfCourseFunction($courseId, $cfg['group_function_type']['assessment'], $id);
		
	    # Get Teacher Name
		$teacherName = $this->GetUserName($UserID, $lang);
	    
	    # Get Course Name
	    $courseName = $this->GetCourseName($courseId);
	    
	    # Get Assessment Title, Deadline
	    $sql = "SELECT title, enddate FROM " . classNamingDB($courseId) . ".assessment WHERE assessment_id = '" . $id . "'";
	    list($title, $deadline) = current($this->returnArray($sql));
	    
		if (file_exists ( $filepath_prefix . "/src/lang/lang.$lang.php" )) {
			include_once ($filepath_prefix . "/src/lang/lang.$lang.php");
		}
	    
	    $message = array(
	        'targetUserIds' => $targetUserIds,
	        'title'         => $Lang['Assessment']['pushMessageTitle'],
	        'content'       => str_replace(
	            array('[TEACHER_NAME]','[COURSE_NAME]','[TITLE]','[DEADLINE]'), 
	            array($teacherName, $courseName, $title, $deadline), 
	            $Lang['Assessment']['pushMessageTemplate']
	        )
	    );
	    
	    return $message;
	}
	
	function SendPushMessageFromPowerLesson2($courseId, $phaseId, $lang)
	{
	    global $intranet_root, $eclass40_filepath, $UserID;
	    
	    $filepath_prefix = $eclass_root ? $eclass_root : $eclass40_filepath;
	    
	    include_once($intranet_root . "/includes/PowerLesson/PowerLesson.php");
	    $powerlesson = new PowerLesson();
	    $message = $powerlesson->run('getPushMessageForPhase', array('courseId'=>$courseId, 'phaseId'=>$phaseId));
	    
	    if(!is_array($message)){
	        return $message;
	    }
	    
	    # Get Lesson Title (convert back to big-5 from utf-8)
	    $lessonTitle = $message['title'];
	    
	    # Get Phase Type
	    $phaseType = $message['phaseType'];
	    
	    # Get Start Date
	    $startDate = new DateTime($message['startDate']);
	    $startDate->setTimezone(new DateTimeZone(date_default_timezone_get()));
	    $startDate = $startDate->format('Y-m-d H:i');
	    
	    # Get Teacher Name
		$teacherName = $this->GetUserName($UserID, $lang);
	    
	    # Get Course Name
	    $courseName = $this->GetCourseName($courseId);
	    
		if (file_exists ( $filepath_prefix . "/src/lang/lang.$lang.php" )) {
			include_once ($filepath_prefix . "/src/lang/lang.$lang.php");
		}
	    
	    return array(
	        'targetUserIds' => $message['targetStudents'],
	        'title'         => $Lang['PowerLesson']['pushMessageTitle'],
	        'content'       => str_replace(
	            array('[TEACHER_NAME]','[COURSE_NAME]','[TITLE]','[PHASE]','[START_DATE]'), 
	            array($teacherName, $courseName, ($lessonTitle? $lessonTitle : $Lang['PowerLesson']['UntitledLesson']), $Lang['PowerLesson']['Phase'][$phaseType], $startDate), 
	            $Lang['PowerLesson']['pushMessageTemplate']
	        )
	    );
	}
	
	function GetCourseName($courseId)
	{
	    global $eclass_db;
	    
	    $sql = "SELECT course_name FROM " . $eclass_db . ".course WHERE course_id = '" . $courseId . "'";
	    $courseTitle = current($this->returnVector($sql));
	    
	    return $courseTitle;
	}
	
	function GetUserName($targetUserId, $lang)
	{
	    global $intranet_root, $intranet_session_language;
	    
		include_once($intranet_root.'/includes/libuser.php');
		
	    /* Change the $intranet_session_language temporarily */
	    $oriLang = $intranet_session_language;
	    $intranet_session_language = in_array($lang, array("chib5", "chigb"))? 'b5' : 'en';
	    
	    $user     = new libuser($targetUserId);
	    $username = $user->StandardDisplayName;
	    
	    /* Rollback the value of $intranet_session_language */
	    $intranet_session_language = $oriLang;
	    
		return trim($username);
	}
	
	function GetIntranetUserIdsOfTargetUsersOfCourseFunction($courseId, $functionType, $functionId)
	{
	    global $eclass40_filepath, $eclass_root;
	    
	    $filepath_prefix = $eclass_root ? $eclass_root : $eclass40_filepath;
		include_once($filepath_prefix . "/src/includes/php/lib-groups.php");
		
	    $courseDb = classNamingDB($courseId);
	    
	    $lgroups = new libgroups($courseDb);
	    $targetUserIds = $lgroups->returnUserListInFunction($functionType, $functionId);
	    
	    if(count($targetUserIds) > 0){
	        $condition = " AND user_id in ('" . implode("','", $targetUserIds) . "')";
	    }
	    $sql = "SELECT
                    DISTINCT intranet_user_id 
                FROM 
                    " . $courseDb . ".usermaster 
                WHERE
                    memberType = 'S' AND 
                    (status IS NULL OR status NOT IN ('deleted'))" . $condition;
	    
	    return $this->returnVector($sql);
	}

	function WS_GetSessionNumberForDate($TargetDate)
	{
		global $intranet_root;
		include_once($intranet_root."/includes/libcardstudentattend2.php");
		$TargetDate = date("Y-m-d", strtotime($TargetDate));
		$lc = new libcardstudentattend2();
		$timeslot = $lc->getDateTimeTableID($TargetDate);
		$result = array();
		if($timeslot != '') {
			$count = $lc->getDateSessionCount($timeslot);
			$result = range(1, intval($count));
		}
		return array('Result'=>$result);
	}

	function WS_GetSchoolDayCount($StartDate, $EndDate)
	{
		global $intranet_root;
		include_once($intranet_root."/includes/libcardstudentattend2.php");
		$lc = new libcardstudentattend2();
		$lc->createCountSchoolDayFunction();
		$sql = "SELECT count_schooldays('$StartDate', '$EndDate') as count";
		$rs = $lc->returnArray($sql);
		return array('Result'=>$rs[0]['count']);
	}

	function WS_SubmitBodyTemperatureRecord($ParentUserID, $StudentUserID, $TargetDate, $RecordTime, $TemperatureValue)
	{
		global $intranet_root, $sys_custom;
		include_once($intranet_root."/includes/libcardstudentattend2.php");
		$lc = new libcardstudentattend2();

		$is_today = (date("Y-m-d") == $TargetDate) ? true : false;
		if($is_today == false) {
			return array('Result' => false);
		}

		$result = false;
		$TemperatureStatus = 0;
		$Settings = $lc->Settings;
		if($Settings['StudentBodyTemperatureAbnormalValue'] == '' || $Settings['StudentBodyTemperatureAbnormalValue'] == 0) {
			$Settings['StudentBodyTemperatureAbnormalValue'] = 37.2;
		}

		if($TemperatureValue > $Settings['StudentBodyTemperatureAbnormalValue']) {
			$TemperatureStatus = 1;
		}


		$year = date("Y", strtotime($TargetDate));
		$month = date("m", strtotime($TargetDate));
		$day = date("d", strtotime($TargetDate));
		$have_attendance_record = false;

		$lc->createTable_Card_Student_Daily_Log($year,$month);

		$dailylog_tablename = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
		$sql = "SELECT 
						RecordID, 
						AMStatus,
						PMStatus
					FROM 
					$dailylog_tablename
					WHERE 
					UserID = '".$StudentUserID."' 
					AND 
					DayNumber = '".$day."'";
		$temp = $lc->returnArray($sql);

		if(!empty($temp)) {
			$temp = $temp[0];
			if($temp['AMStatus'] != '' || $temp['PMStatus'] != '') {
				$have_attendance_record = true;
			}
		}

		$modified_date = date("Y-m-d")." ".$RecordTime;
		$sql = "SELECT * FROM CARD_STUDENT_BODY_TEMPERATURE_RECORD WHERE RecordDate='$TargetDate' AND StudentID='$StudentUserID'";
		$exists_record = $lc->returnArray($sql);
		if(empty($exists_record)) {
			$sql = "INSERT INTO CARD_STUDENT_BODY_TEMPERATURE_RECORD (RecordDate, StudentID, TemperatureValue, TemperatureStatus, DateInput, ModifiedDate, Source)
			VALUES ('$TargetDate', '$StudentUserID', '$TemperatureValue', '$TemperatureStatus', now(), '$modified_date', 'P')";
			$lc->db_db_query($sql);
			$result = true;
		} else {
			$exists_record = $exists_record[0];
			$can_update = true;

			if($have_attendance_record) {
				$can_update = false;
			}

			if($exists_record['Source'] != 'P') {
				$can_update = false;
			}

			if($can_update) {
				$sql = "UPDATE CARD_STUDENT_BODY_TEMPERATURE_RECORD SET TemperatureValue='$TemperatureValue', ModifiedDate='$modified_date', TemperatureStatus='$TemperatureStatus'
					WHERE RecordID='".$exists_record['RecordID']."' AND Source='P'";
			$lc->db_db_query($sql);
				$result = true;
		}
		}

		return array('Result'=> $result);
	}


	function WS_GetStudentBodyTemperatureRecords($TargetUserID)
	{
		global $intranet_root;
		include_once($intranet_root."/includes/libcardstudentattend2.php");
		$lc = new libcardstudentattend2();

		$dateLimit = date('Y-m-d', strtotime('-3 month'));
		$sql = "SELECT RecordID,RecordDate,ModifiedDate,TemperatureValue, Source
 				FROM CARD_STUDENT_BODY_TEMPERATURE_RECORD
 				WHERE StudentID='$TargetUserID'
 				AND '$dateLimit' <= DATE(RecordDate)
 				ORDER BY RecordDate DESC";
		$rs = $lc->returnArray($sql);
		$result = array();
		foreach($rs as $data) {
			$can_edit = false;
			$have_attendance_record = false;
			if(date("Y-m-d") == $data['RecordDate']) {
				if($data['Source'] == 'P') {
					$year = date("Y", strtotime($data['RecordDate']));
					$month = date("m", strtotime($data['RecordDate']));
					$day = date("d", strtotime($data['RecordDate']));
					$lc->createTable_Card_Student_Daily_Log($year,$month);
					$dailylog_tablename = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
					$sql = "SELECT 
						RecordID, 
						AMStatus,
						PMStatus
					FROM 
					$dailylog_tablename
					WHERE 
					UserID = '".$TargetUserID."' 
					AND 
					DayNumber = '".$day."'";
					$temp = $lc->returnArray($sql);

					if(!empty($temp)) {
						$temp = $temp[0];
						if($temp['AMStatus'] != '' || $temp['PMStatus'] != '') {
							$have_attendance_record = true;
						}
					}

					if($have_attendance_record == false) {
						$can_edit = true;
					}
				}
			}

			$temp = array();
			$temp['RecordID'] = $data['RecordID'];
			$temp['RecordDate'] = $data['RecordDate'];
			$temp['RecordTime'] = date("H:i", strtotime($data['ModifiedDate']));
			$temp['TemperatureValue'] = $data['TemperatureValue'];
			$temp['CanEdit'] = $can_edit;
			$temp['Source'] = $data['Source'];
			$temp['HaveAttendanceRecord'] = $have_attendance_record;
			$result[] = $temp;
		}
		return array('Result'=> $result);
	}

	function WS_ApiKeyVersion()
	{
		return array('Result'=> 2);
	}

	function WS_ForgetPasswordVersion()
	{
		return array('Result'=> 2);
	}

	function WS_ForgetPasswordV2($UserLogin, $UserEmail) {
		global $intranet_root, $webmaster, $sys_custom;
		$UserLogin = htmlspecialchars ( trim ( $UserLogin ) );
		$UserEmail = htmlspecialchars ( trim ( $UserEmail ) );
		include_once ('libauth.php');
		$li = new libauth ();
		$UserInfo = $li->forgetPassword2 ( $UserLogin );
		$email = "";

		if ($UserInfo == 0) {
			$urlSuccess = 0;
		} else {

			// Send Email
			$t_UserID = $UserInfo [0];
			$UserLogin = $UserInfo [1];
			$UserPassword = $UserInfo [2];
			$db_UserEmail = $UserInfo [3];
			$EnglishName = $UserInfo [4];
			$ChineseName = $UserInfo [5];

			if ($db_UserEmail != '') {
				if(strtolower($UserEmail) == strtolower($db_UserEmail)) {
					// Call sendmail function
					include_once("libuser.php");
					$luser = new libuser ();
					$Key = $li->CreateResetPasswordKey($UserLogin);
					list ($mailSubject, $mailBody) = $luser->returnEmailNotificationData_HashedPw($db_UserEmail, $UserLogin, $EnglishName, $Key);
					$mailTo = $db_UserEmail;
					$webmaster = get_webmaster();
					include_once("libemail.php");
					include_once("libsendmail.php");
					$lu = new libsendmail ();

					$result = $lu->send_email($mailTo, $mailSubject, $mailBody, "");

					$email = substr($db_UserEmail, strpos($db_UserEmail, '@') + 1);
					$urlSuccess = 1;
				} else {
					$urlSuccess = 3;
				}
			} else {
				$urlSuccess = 2;
			}
		}
		$returnAry = array ();
		$returnAry ['EmailDomain'] = $email;
		$returnAry ['SendEmailSuccess'] = $urlSuccess; // 0:login not exist, 1:send success, 2:don`t have address, 3:email not match
		return $returnAry;
	}

// 	function WS_GetAllUserList($CurrentUserID)
// 	{
// 	    global $intranet_root;
// 	    include_once ($intranet_root. '/includes/libuser.php');
	    
// 	    $lu = new libuser($CurrentUserID);
	    
// 	}
	
	/*
	 * # NOT USED ANYMORE -- deprecated method
	 * # eContent - Get any Note Section Update by Course
	 * function WS_GeteContentUpdate($Timestamp, $CourseID){
	 * $this->db = classNamingDB($CourseID);
	 *
	 * $CategoryID = 0;
	 *
	 * $null_time = '00-00-00 00:00:00';
	 * if(true || $ThisUserID != '') {
	 * //SUBSTRING_INDEX(ns.content, '/', -1) as filename
	 * $sql = "select
	 * ns.notes_id, ns.notes_section_id, n.title, ns.content,
	 * n.modified as n_modified, ns.modified as ns_modified
	 * from notes as n inner join notes_section as ns on
	 * ns.notes_id = n.notes_id and ns.notes_section_type = '8' and
	 * content is not NULL and content <> ''
	 * where
	 * n.status = '1' ";
	 * $sql .= " and unix_timestamp(n.modified) > $Timestamp";
	 *
	 *
	 * //$sql .= " and ((n.starttime IS NULL OR n.starttime = ".$null_time." OR n.starttime < now() ) and
	 * // (n.endtime IS NULL OR n.endtime > now() OR n.endtime = ".$null_time." ) ) "; // Within Period
	 *
	 * $sql .= " order by n.notes_id, n.a_no, n.b_no, n.c_no, n.d_no, n.e_no";
	 * $row = $this->returnArray($sql);
	 *
	 * # Group Data in related Array format
	 * $ReturnContent = array();
	 * if(count($row) > 0){
	 * $cnt1 = 0;
	 * for($i=0; $i<count($row); $i++)
	 * {
	 * $notes_id = $row[$i]['notes_id'];
	 * $title = $row[$i]['title'];
	 * $notes_section_id = $row[$i]['notes_section_id'];
	 * $content = $row[$i]['content'];
	 * $n_modified = $row[$i]['n_modified'];
	 * $ns_modified = $row[$i]['ns_modified'];
	 * $filename = get_file_basename($content);
	 * //$filename = $row[$i][4];
	 * $filepath = intranet_htmlspecialchars($this->Get_File_FullPath($content, $CourseID));
	 *
	 * $ReturnContent['Notes'][$cnt1]['NotesID'] = $notes_id;
	 * $ReturnContent['Notes'][$cnt1]['Title'] = intranet_htmlspecialchars($title);
	 * $ReturnContent['Notes'][$cnt1]['ModifiedDate'] = $n_modified;
	 * $ReturnContent['Notes'][$cnt1]['NotesSection'][0]['NotesSectionID'] = $notes_section_id;
	 * $ReturnContent['Notes'][$cnt1]['NotesSection'][0]['ModifiedDate'] = $ns_modified;
	 * $ReturnContent['Notes'][$cnt1]['NotesSection'][0]['Content'] = intranet_htmlspecialchars($content);
	 * $ReturnContent['Notes'][$cnt1]['NotesSection'][0]['Filename'] = intranet_htmlspecialchars($filename);
	 * $ReturnContent['Notes'][$cnt1]['NotesSection'][0]['Filepath'] = intranet_htmlspecialchars($filepath);
	 *
	 * $cnt2 = 1;
	 * for($j=$i+1; $j<count($row); $j++)
	 * {
	 * $notes_section_id = $row[$j]['notes_section_id'];
	 * $content = $row[$j]['content'];
	 * $filename = get_file_basename($content);
	 * $ns_modified = $row[$j]['ns_modified'];
	 * $filepath = intranet_htmlspecialchars($this->Get_File_FullPath($content, $CourseID, $CategoryID));
	 *
	 * if($row[$j]['notes_id'] == $notes_id){
	 * $ReturnContent['Notes'][$cnt1]['NotesSection'][$cnt2]['NotesSectionID'] = $notes_section_id;
	 * $ReturnContent['Notes'][$cnt1]['NotesSection'][$cnt2]['ModifiedDate'] = $ns_modified;
	 * $ReturnContent['Notes'][$cnt1]['NotesSection'][$cnt2]['Content'] = intranet_htmlspecialchars($content);
	 * $ReturnContent['Notes'][$cnt1]['NotesSection'][$cnt2]['Filename'] = intranet_htmlspecialchars($filename);
	 * $ReturnContent['Notes'][$cnt1]['NotesSection'][$cnt2]['Filepath'] = intranet_htmlspecialchars($filepath);
	 *
	 * $cnt2++;
	 * } else {
	 * break;
	 * }
	 * }
	 *
	 * $cnt1++;
	 * $i = $j - 1;
	 * }
	 * }
	 * } else {
	 * $ReturnContent = '401';
	 * }
	 * return $ReturnContent;
	 * }
	 */
	
	/*
	 * # NOT USED ANYMORE - deprecated method
	 * # eContent - Get all notes and notes_sections by CourseID
	 * # Notes Section Type : 8 - FileUpload
	 * function WS_GetCourseNoteList($CourseID){
	 * $this->db = classNamingDB($CourseID);
	 *
	 * $null_time = '00-00-00 00:00:00';
	 * if(true || $ThisUserID != '') {
	 * //SUBSTRING_INDEX(ns.content, '/', -1) as filename
	 * $sql = "select
	 * ns.notes_id, ns.notes_section_id
	 * from notes as n inner join notes_section as ns on
	 * ns.notes_id = n.notes_id and ns.notes_section_type = '8' and
	 * content is not NULL and content <> ''
	 * where
	 * n.status = '1' ";
	 *
	 * //$sql .= " and ((n.starttime IS NULL OR n.starttime = ".$null_time." OR n.starttime < now() ) and
	 * // (n.endtime IS NULL OR n.endtime > now() OR n.endtime = ".$null_time." ) ) "; // Within Period
	 *
	 * $sql .= " order by n.notes_id, n.a_no, n.b_no, n.c_no, n.d_no, n.e_no";
	 * $row = $this->returnArray($sql);
	 * //echo $sql;
	 * # Group Data in related Array format
	 * $ReturnContent = array();
	 * if(count($row) > 0){
	 * $cnt1 = 0;
	 * for($i=0; $i<count($row); $i++)
	 * {
	 * $notes_id = $row[$i]['notes_id'];
	 * $notes_section_id = $row[$i]['notes_section_id'];
	 *
	 * $ReturnContent['Notes'][$cnt1]['NotesID'] = $notes_id;
	 * $ReturnContent['Notes'][$cnt1]['NotesSection'][0]['NotesSectionID'] = $notes_section_id;
	 *
	 * $cnt2 = 1;
	 * for($j=$i+1; $j<count($row); $j++)
	 * {
	 * $notes_section_id = $row[$j]['notes_section_id'];
	 *
	 * if($row[$j]['notes_id'] == $notes_id){
	 * $ReturnContent['Notes'][$cnt1]['NotesSection'][$cnt2]['NotesSectionID'] = $notes_section_id;
	 * $cnt2++;
	 * } else {
	 * break;
	 * }
	 * }
	 *
	 * $cnt1++;
	 * $i = $j - 1;
	 * }
	 * }
	 * } else {
	 * $ReturnContent = '401';
	 * }
	 * return $ReturnContent;
	 * }
	 */
}

/* *** Common Function *** */
if (! function_exists ( "classNamingDB" )) {
    function classNamingDB($cid) {
        global $sys_db, $eclass_db, $eclass_prefix;
        
        if ($cid == $sys_db || $cid == $eclass_db) {
            $aa = $cid;
        } else {
            $aa = $eclass_prefix . "c" . $cid;
        }
        return $aa;
    }
}
function Word_Count($text) {
	$wStr = $text;
	$wStr = preg_replace ( "/\r\n|\n|\r/", ' ', $wStr );
	// echo "wStr: ". $wStr. "<br>";
	$tmpA = explode ( ' ', $wStr );
	// echo "<pre>". $wStr. "<pre><br>";
	// echo sizeof($tmpA);
	$total = 0;
	$total_sym = 0;
	$isEngWord = false;
	for($i = 0; $i < sizeof ( $tmpA ); $i ++) {
		$tmpV = $tmpA [$i];
		// echo $tmpV. ": length = ". strlen($tmpV). "<br>";
		$isEngWord = false;
		for($j = 0; $j < strlen ( $tmpV ); $j ++) {
			// echo $j. ": ". $tmpV[$j]. "<br>";
			$charCode = ord ( $tmpV [$j] );
			// echo $tmpV[$j]. ": ". $charCode. "<br>";
			if ($charCode == 39 || $charCode == 45 || $charCode == 95 || ($charCode >= 48 && $charCode <= 57) || ($charCode >= 65 && $charCode <= 90) || ($charCode >= 97 && $charCode <= 122) || ($charCode >= 65296 && $charCode <= 65305) || ($charCode >= 65313 && $charCode <= 65338) || ($charCode >= 65345 && $charCode <= 65370)) {
				if (! $isEngWord) {
					$isEngWord = true;
					$total ++;
				}
			} else if ($charCode > 255) {
				$isEngWord = false;
				if (! ($charCode >= 65070 && $charCode <= 65375) && ! ($charCode >= 12288 && $charCode <= 12345) && ! ($charCode >= 8204 && $charCode <= 8231)) {
					$total ++;
				} else {
					$total_sym ++;
				}
			} else {
				$isEngWord = false;
				$total_sym ++;
			}
		}
	}
	
	// echo "total word: ". $total. "<br>";
	// echo "total symbol: ". $total_sym. "<br>";
	$return [word] = $total;
	$return [symbol] = $total_sym;
	
	return $return;
}
function trim_control_character($string) {
	$string = str_replace ( "\r", "", $string );
	$string = str_replace ( "\x0B", "", $string );
	return $string;
}
function getUrlData($url, $raw = false) // $raw - enable for raw display
{
	$result = false;
	
	$contents = getUrlContents ( $url );
	
	if (isset ( $contents ) && is_string ( $contents )) {
		$title = null;
		$metaTags = null;
		$metaProperties = null;
		
		preg_match ( '/<title>([^>]*)<\/title>/si', $contents, $match );
		
		if (isset ( $match ) && is_array ( $match ) && count ( $match ) > 0) {
			$title = strip_tags ( $match [1] );
		}
		
		preg_match_all ( '/<[\s]*meta[\s]*(name|property)="?' . '([^>"]*)"?[\s]*' . 'content="?([^>"]*)"?[\s]*[\/]?[\s]*>/si', $contents, $match );
		
		if (isset ( $match ) && is_array ( $match ) && count ( $match ) == 4) {
			$originals = $match [0];
			$names = $match [2];
			$values = $match [3];
			
			if (count ( $originals ) == count ( $names ) && count ( $names ) == count ( $values )) {
				$metaTags = array ();
				$metaProperties = $metaTags;
				if ($raw) {
					if (version_compare ( PHP_VERSION, '5.4.0' ) == - 1)
						$flags = ENT_COMPAT;
					else
						$flags = ENT_COMPAT | ENT_HTML401;
				}
				
				for($i = 0, $limiti = count ( $names ); $i < $limiti; $i ++) {
					if ($match [1] [$i] == 'name')
						$meta_type = 'metaTags';
					else
						$meta_type = 'metaProperties';
					if ($raw)
						${$meta_type} [$names [$i]] = array (
								'html' => htmlentities ( $originals [$i], $flags, 'UTF-8' ),
								'value' => $values [$i] 
						);
					else
						${$meta_type} [$names [$i]] = array (
								'html' => $originals [$i],
								'value' => $values [$i] 
						);
				}
			}
		}
		
		$result = array (
				'title' => $title,
				'metaTags' => $metaTags,
				'metaProperties' => $metaProperties 
		);
	}
	
	return $result;
}
function getUrlContents($url, $maximumRedirections = null, $currentRedirection = 0) {
	$result = false;
	
	$contents = file_get_contents ( $url );
	
	// handle different charset
	preg_match ( '@<meta\s+http-equiv="Content-Type"\s+content="([\w/]+)(;\s+charset=([^\s"]+))?@i', $contents, $matches );
	if (isset ( $matches [1] ))
		$mime = $matches [1];
	if (isset ( $matches [3] ))
		$charset = $matches [3];
	
	if (isset ( $charset ) && $charset != "utf-8" && $charset != "UTF-8") {
		$contents = iconv ( $charset, "utf-8", $contents );
	}
	
	// Check if we need to go somewhere else
	
	if (isset ( $contents ) && is_string ( $contents )) {
		preg_match_all ( '/<[\s]*meta[\s]*http-equiv="?REFRESH"?' . '[\s]*content="?[0-9]*;[\s]*URL[\s]*=[\s]*([^>"]*)"?' . '[\s]*[\/]?[\s]*>/si', $contents, $match );
		
		if (isset ( $match ) && is_array ( $match ) && count ( $match ) == 2 && count ( $match [1] ) == 1) {
			if (! isset ( $maximumRedirections ) || $currentRedirection < $maximumRedirections) {
				return getUrlContents ( $match [1] [0], $maximumRedirections, ++ $currentRedirection );
			}
			
			$result = false;
		} else {
			$result = $contents;
		}
	}
	
	return $contents;
}
function __powerLessonApiCaller($apiUri, $method, $courseId, $isEncryptCourseId = false, $requestData = array(), $authToken = null, $includeSystemToken = false) {
	global $web_protocol, $eclass40_httppath, $eclass_db, $config_school_code, $powerlesson2_config;
	
	include_once ('json.php');
	$jsonObj = new JSON_obj ();
	$method = strtoupper ( $method );

	/* Set apiUrl */
	if(isset($powerlesson2_config['eclassInternalUrl'])){
	    $apiBaseUrl = $powerlesson2_config['eclassInternalUrl'];
	} else {
	    $apiBaseUrl = (isset ( $web_protocol ) && preg_match ( '/^https?$/', $web_protocol ) === 1 ? $web_protocol : 'http') . '://' . $eclass40_httppath;
	}
	$apiBaseUrl = rtrim ( $apiBaseUrl, '/' ) . '/src/powerlesson/api';
	$apiUrl = $apiBaseUrl . $apiUri;
	
	/* Set headers */
	$headers = array ();
	if ($isEncryptCourseId) {
		$headers [] = 'X-EncryptCourseId: ' . $courseId;
	} else {
		$headers [] = 'X-CourseId: ' . $courseId;
	}
	if (! is_null ( $authToken )) {
		$headers [] = 'X-AuthToken: ' . $authToken;
	} elseif ($includeSystemToken) {
	    $systemToken = md5($config_school_code . $powerlesson2_config['systemTokenSalt']);
	    $headers [] = 'X-SystemToken: ' . $systemToken;
	}
	$headers [] = 'Content-Type: application/json';
	$headers [] = 'Accept: application/json';
	
	/* Set request data */
	$requestData = count ( $requestData ) ? $jsonObj->encode ( $requestData ) : '';
	
	/* Set curl option */
	
	$ch = curl_init ();
	curl_setopt ( $ch, CURLOPT_URL, $apiUrl );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
	switch ($method) {
		case 'GET' :
			break;
		case 'POST' :
			curl_setopt ( $ch, CURLOPT_POST, true );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $requestData );
			break;
		case 'PUT' :
			curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, "PUT" );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $requestData );
			$headers [] = 'Content-Length: ' . strlen ( $requestData );
			break;
		case 'DELETE' :
			curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, "DELETE" );
			break;
	}
	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
	curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 60 );
	curl_setopt ( $ch, CURLOPT_TIMEOUT, 300 );
	
	$rawResponse = curl_exec ( $ch );
	$apiResponse = $jsonObj->decode ( $rawResponse );
	$httpStatusCode = intval ( curl_getinfo ( $ch, CURLINFO_HTTP_CODE ) );
	$result = $httpStatusCode >= 200 && $httpStatusCode <= 299 ? true : false;
	
	return array (
			'result' => $result,
			'apiResponse' => $apiResponse 
	);
}