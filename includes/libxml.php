<?php
# Editing by: 
/**
 * 2020-08-31 (Cameron)
 *          fix typo: utf8 -> utf-8 for default charset in Array_To_XML() [case #X194516]
 *
 * 2020-08-28 (Cameron)
 *          add para $charset to Array_To_XML(), HandledCharacterForXML() and Parse_Array() so that it can handle big5
 *
 * 2020-08-18 (Cameron)
 *          add parameter $lineBreak to Array_To_XML() and Parse_Array()
 *
 * 2013-03-06 (Carlos)
 * 			modified HandledCharacterForXML() - htmlspecialchars() handle both double quote and single quote, after that single quote &#039; is converted to &apos; for xml
 *
 * 2012-03-22 (Yuen)
 * 			specially handle some special characters ( & and &nbsp; ) of XML if they exist in data
 *
 * xml2array() will convert the given XML text to an array in the XML structure.
 * Link: http://www.bin-co.com/php/scripts/xml2array/
 * Arguments : $contents - The XML text
 *                $get_attributes - 1 or 0. If this is 1 the function will get the attributes as well as the tag values - this results in a different array structure in the return value.
 *                $priority - Can be 'tag' or 'attribute'. This will change the way the resulting array sturcture. For 'tag', the tags are given more importance.
 * Return: The parsed XML in an array form. Use print_r() to see the resulting array structure.
 * Examples: $array =  xml2array(file_get_contents('feed.xml');
 *              $array =  xml2array(file_get_contents('feed.xml', 1, 'attribute');
 */

// function debug_r($arr) {
// echo "<i>[debug]</i>\n<PRE>\n";
// print_r($arr);
// echo "\n</PRE>\n<br>\n";
// }

class LibXML {
    function XML_ToArray($contents, $get_attributes=1, $priority = 'tag') {
        if(!$contents) return array();

        if(!function_exists('xml_parser_create')) {
            return array();
        }

        # Get the XML parser of PHP - PHP must have this module for the parser to work
        //$parser = xml_parser_create('');
        // EDIT by Henry Y: This tells the php parse that the incoming strings are in UTF-8 encoding
        $parser = xml_parser_create("UTF-8");
        xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($contents), $xml_values);
        xml_parser_free($parser);

        if(!$xml_values) return;

        # Initializations
        $xml_array = array();
        $parents = array();
        $opened_tags = array();
        $arr = array();

        $current = &$xml_array; # Refference

        # Go through the tags.
        $repeated_tag_index = array(); # Multiple tags with same name will be turned into an array
        foreach($xml_values as $data) {
            unset($attributes,$value); # Remove existing values, or there will be trouble

            /**This command will extract these variables into the foreach scope
             * tag(string), type(string), level(int), attributes(array).
             */
            extract($data); # We could use the array by itself, but this cooler.
            # It is NOT cool.
            # http://blog.php-security.org/archives/5-Tips-That-Every-PHP-Developer-Should-NOT-Know.html

            $result = array();
            $attributes_data = array();

            if(isset($value)) {
                # handle CDATA
                if (strrpos($value, "<![CDATA[") === 0) $value = substr($value, 9, -3);
                if($priority == 'tag') $result = $value;
                else $result['value'] = $value; # Put the value in a assoc array if we are in the 'Attribute' mode
            }

            # Set the attributes too.
            if(isset($attributes) and $get_attributes) {
                foreach($attributes as $attr => $val) {
                    if($priority == 'tag') $attributes_data[$attr] = $val;
                    else $result['attr'][$attr] = $val; # Set all the attributes in a array called 'attr'
                }
            }

            # See tag status and do the needed.
            if($type == "open") { # The starting of the tag '<tag>'
                $parent[$level-1] = &$current;

                if(!is_array($current) or (!in_array($tag, array_keys($current)))) { # Insert New tag
                    $current[$tag] = $result;
                    if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
                    $repeated_tag_index[$tag.'_'.$level] = 1;

                    $current = &$current[$tag];

                } else { # There was another element with the same tag name

                    if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
                        $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
                        $repeated_tag_index[$tag.'_'.$level]++;
                    } else {//This section will make the value an array if multiple tags with the same name appear together
                        $current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
                        $repeated_tag_index[$tag.'_'.$level] = 2;

                        if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                            $current[$tag]['0_attr'] = $current[$tag.'_attr'];
                            unset($current[$tag.'_attr']);
                        }

                    }
                    $last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
                    $current = &$current[$tag][$last_item_index];
                }

            } elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
                //See if the key is already taken.
                if(!isset($current[$tag])) { //New Key
                    $current[$tag] = $result;
                    $repeated_tag_index[$tag.'_'.$level] = 1;
                    if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;

                } else { //If taken, put all things inside a list(array)
                    if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

                        // ...push the new element into that array.
                        $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;

                        if($priority == 'tag' and $get_attributes and $attributes_data) {
                            $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
                        }
                        $repeated_tag_index[$tag.'_'.$level]++;

                    } else { //If it is not an array...
                        $current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
                        $repeated_tag_index[$tag.'_'.$level] = 1;
                        if($priority == 'tag' and $get_attributes) {
                            if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well

                                $current[$tag]['0_attr'] = $current[$tag.'_attr'];
                                unset($current[$tag.'_attr']);
                            }

                            if($attributes_data) {
                                $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
                            }
                        }
                        $repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
                    }
                }

            } elseif($type == 'close') { //End of tag '</tag>'
                $current = &$parent[$level-1];
            }
        }

        return($xml_array);
    }

    function XML_Encode($string)
    {
        return $string;
        //return str_replace($string, "<", "&lt;");
    }

    function XML_CreateElement($name, $attribute=NULL, $text=NULL)
    {
        $returnTag = "<" . $name;

        if (is_array($attribute))
        {
            while (list($key, $value) = each($attribute))
            {
                $returnTag .= ' ' . $this->XML_Encode($key) . '="';
                $returnTag .= $this->XML_Encode($value). '"';
            }
        }

        if ($text !== false)
        {
            $returnTag .= ">";
            $returnTag .= $this->XML_Encode($text);
            $returnTag .= "</" . $name . ">";
        }
        else
        {
            $returnTag .= " />";
        }

        return $returnTag;
    }

    function XML_OpenElement($name, $attribute=NULL)
    {
        $returnTag = "<" . $name;

        if (is_array($attribute))
        {
            while (list($key, $value) = each($attribute))
            {
                $returnTag .= ' ' . $this->XML_Encode($key) . '="';
                $returnTag .= $this->XML_Encode($value) . '"';
            }
        }
        $returnTag .= ">";

        return $returnTag;
    }

    function XML_CloseElement($name)
    {
        return "</" . $name . ">";
    }

    /*Generate the XML Result from the array parameter*/
    function Array_To_XML($ParArr, $OmitHeader=false, $lineBreak=false, $charset="utf-8") {
        if (!$OmitHeader)
            $xml_result  = '<eClassResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';

        foreach($ParArr as $key=>$val) {
            if(is_array($val)) {
                $xml_result .= $this->XML_OpenElement($key);
                if ($lineBreak) {
                    $xml_result .= "\r\n";
                }
                $xml_result .= $this->Parse_Array($val, $lineBreak, $charset);
                $xml_result .= $this->XML_CloseElement($key);
                if ($lineBreak) {
                    $xml_result .= "\r\n";
                }
            }else{
                $xml_result .= $this->XML_OpenElement($key);
                //$xml_result .= $val;
                $xml_result .= $this->HandledCharacterForXML($val, $charset);
                $xml_result .= $this->XML_CloseElement($key);
                if ($lineBreak) {
                    $xml_result .= "\r\n";
                }
            }
        }
        if (!$OmitHeader)
            $xml_result .= '</eClassResponse>';

        return $xml_result;
    }

    function EncryptedXMLWrapper($EncryptedXML="") {
        $xml_result  = '<eClassEncryptedResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $xml_result .= $EncryptedXML;
        $xml_result .= '</eClassEncryptedResponse>';

        return $xml_result;
    }

    function Parse_Array($ParArr, $lineBreak=false, $charset='utf-8') {
        $xml_result = '';

        foreach($ParArr as $key=>$val) {
            if(is_array($val)) {
                if(!$this->is_assoc($val)) {
                    $c = 0;
                    foreach($val as $key2=>$val2) {
                        if(is_array($val2)){
                            $xml_result .= $this->XML_OpenElement($key);
                            if ($lineBreak) {
                                $xml_result .= "\r\n";
                            }
                            $xml_result .= $this->Parse_Array($val2, $lineBreak, $charset);
                            $xml_result .= $this->XML_CloseElement($key);
                            if ($lineBreak) {
                                $xml_result .= "\r\n";
                            }
                        }
                        else{
                            $xml_result .= $this->XML_OpenElement($key);
                            //$xml_result .= $val2;
                            $xml_result .= $this->HandledCharacterForXML($val2, $charset);
                            $xml_result .= $this->XML_CloseElement($key);
                            if ($lineBreak) {
                                $xml_result .= "\r\n";
                            }
                        }
                        $c++;
                    }
                }else{
                    $xml_result .= $this->XML_OpenElement($key);
                    if ($lineBreak) {
                        $xml_result .= "\r\n";
                    }
                    $xml_result .= $this->Parse_Array($val, $lineBreak, $charset);
                    $xml_result .= $this->XML_CloseElement($key);
                    if ($lineBreak) {
                        $xml_result .= "\r\n";
                    }
                }
            }else{
                $xml_result .= $this->XML_OpenElement($key);
                //$xml_result .= $val;
                $xml_result .= $this->HandledCharacterForXML($val, $charset);
                $xml_result .= $this->XML_CloseElement($key);
                if ($lineBreak) {
                    $xml_result .= "\r\n";
                }
            }
        }

        return $xml_result;
    }

    function HandledCharacterForXML($gString, $charset="utf-8")
    {
        # specially handle some special characters in case they exist in data
        $gString = intranet_undo_htmlspecialchars($gString);

        $gString = htmlspecialchars($gString, ENT_QUOTES, $charset);
        $gString = str_replace("&amp;nbsp;", " ", $gString);
        $gString = str_replace("&amp;#", "&#", $gString);
        $gString = str_replace('&#039;', '&apos;', $gString);

        return $gString;


        /*
                //$gString = str_replace("&amp;", "&", $gString);
                $patterns = array (
                        "/&nbsp;/"
                        );
                $replacements = array (
                            'ss'
                            );
                $gString = preg_replace($patterns, $replacements, $gString);
                //$gString = str_replace("&amp;", "&", $gString);
                //$gString = str_replace("&", "&amp;", $gString);

                return $gString;

                */
    }

    function is_assoc($array) {
        return (is_array($array) && 0 !== count($this->array_diff_key($array, array_keys(array_keys($array)))));
    }

    function array_diff_key(){
        $arrs = func_get_args();
        $result = array_shift($arrs);
        foreach ($arrs as $array) {
            foreach ($result as $key => $v) {
                if (array_key_exists($key, $array)) {
                    unset($result[$key]);
                }
            }
        }
        return $result;
    }
}
?>