<?php
// using: rita

if (!defined("LIBLOCATION_CARD_READER_DEFINED")) {	// Preprocessor directives
	define("LIBLOCATION_CARD_READER_DEFINED", true);
	
	include_once($PATH_WRT_ROOT.'includes/libdbobject.php');
	include_once($PATH_WRT_ROOT.'includes/liblocation_config.php');
	
	class liblocation_cardReader extends libdbobject {
		private $readerId;
		private $locationId;
		private $code;
		private $nameEng;
		private $nameChi;
		private $remarks;
		private $recordStatus;
		private $isDeleted;
		private $inputDate;
		private $inputBy;
		private $modifiedDate;
		private $modifiedBy;
		
		
		public function __construct($objectId='') {
			parent::__construct('INVENTORY_LOCATION_CARD_READER', 'ReaderID', $this->returnFieldMappingAry(), $objectId);
		}
		
		
		public function setReaderId($val) {
			$this->readerId = $val;
		}
		public function getReaderId() {
			return $this->readerId;
		}
		
		public function setLocationId($val) {
			$this->locationId = $val;
		}
		public function getLocationId() {
			return $this->locationId;
		}
		
		public function setCode($val) {
			$this->code = $val;
		}
		public function getCode() {
			return $this->code;
		}
		
		public function setNameEng($val) {
			$this->nameEng = $val;
		}
		public function getNameEng() {
			return $this->nameEng;
		}
		
		public function setNameChi($val) {
			$this->nameChi = $val;
		}
		public function getNameChi() {
			return $this->nameChi;
		}
		
		public function setRemarks($val) {
			$this->remarks = $val;
		}
		public function getRemarks() {
			return $this->remarks;
		}
		
		public function setRecordStatus($val) {
			$this->recordStatus = $val;
		}
		public function getRecordStatus() {
			return $this->recordStatus;
		}
		
		public function setIsDeleted($val) {
			$this->isDeleted = $val;
		}
		public function getIsDeleted() {
			return $this->isDeleted;
		}
		
		public function setInputDate($val) {
			$this->inputDate = $val;
		}
		public function getInputDate() {
			return $this->inputDate;
		}
		
		public function setInputBy($val) {
			$this->inputBy = $val;
		}
		public function getInputBy() {
			return $this->inputBy;
		}
		
		public function setModifiedDate($val) {
			$this->modifiedDate = $val;
		}
		public function getModifiedDate() {
			return $this->modifiedDate;
		}
		
		public function setModifiedBy($val) {
			$this->modifiedBy = $val;
		}
		public function getModifiedBy() {
			return $this->modifiedBy;
		}
		
		private function returnFieldMappingAry() {
			$fieldMappingAry = array();
			$fieldMappingAry[] = array('ReaderID', 'int', 'setReaderId', 'getReaderId');
			$fieldMappingAry[] = array('LocationID', 'int', 'setLocationId', 'getLocationId');
			$fieldMappingAry[] = array('Code', 'str', 'setCode', 'getCode');
			$fieldMappingAry[] = array('NameEng', 'str', 'setNameEng', 'getNameEng');
			$fieldMappingAry[] = array('NameChi', 'str', 'setNameChi', 'getNameChi');
			$fieldMappingAry[] = array('Remarks', 'str', 'setRemarks', 'getRemarks');
			$fieldMappingAry[] = array('RecordStatus', 'int', 'setRecordStatus', 'getRecordStatus');
			$fieldMappingAry[] = array('IsDeleted', 'int', 'setIsDeleted', 'getIsDeleted');
			$fieldMappingAry[] = array('InputDate', 'date', 'setInputDate', 'getInputDate');
			$fieldMappingAry[] = array('InputBy', 'int', 'setInputBy', 'getInputBy');
			$fieldMappingAry[] = array('ModifiedDate', 'date', 'setModifiedDate', 'getModifiedDate');
			$fieldMappingAry[] = array('ModifiedBy', 'int', 'setModifiedBy', 'getModifiedBy');
			return $fieldMappingAry;
		}
			
//		public function getExtraInfoDBTable($field, $order, $page, $Keyword, $LocationID, $status) {
//			global $PATH_WRT_ROOT, $Lang, $eReportCard, $page_size, $readerInfo_page_size;
//			global $image_path;
//			
//			include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//			include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//			include_once($PATH_WRT_ROOT."includes/libdb.php");
//			include_once($PATH_WRT_ROOT."includes/lib.php");
//			$libdb = new libdb();
//			 
//			$field = ($field=='')? 0 : $field;
//			$order = ($order=='')? 1 : $order;
//			$page = ($page=='')? 1 : $page;
//	
//			if (isset($readerInfo_page_size) && $readerInfo_page_size != "") {
//				$page_size = $readerInfo_page_size;
//			}
//			$li = new libdbtable2007($field,$order,$page);
//			
//			$sql = $this->getExtraInfoDBTableSql($Keyword, $LocationID, $status);
//		
//			$li->sql = $sql;
//			$li->IsColOff = "IP25_table";		
//			$li->field_array = array("RoomNameEN", "ilcr.Code", "ilcr.NameEng", "ilcr.NameChi", "ilcr.RecordStatus");
//			$li->column_array = array(0,0);
//			$li->wrap_array = array(0,0);
//			
//			$pos = 0;
//			$li->column_list .= "<th width='10px' class='tabletoplink'>#</th>\n";
//			$li->column_list .= "<th width='30%'>".$li->column_IP25($pos++, $Lang['SysMgr']['Location']['CardReader']['Location'])."</th>\n";
//			$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $Lang['SysMgr']['Location']['CardReader']['Code'])."</th>\n";
//			$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['SysMgr']['Location']['CardReader']['ReaderName'].$Lang['General']['FormFieldLabel']['En'])."</th>\n";
//			$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['SysMgr']['Location']['CardReader']['ReaderName'].$Lang['General']['FormFieldLabel']['Ch'])."</th>\n";
//			$li->column_list .= "<th width='10%'>". $Lang['SysMgr']['Location']['CardReader']['Status']."</th>\n";
//			$li->column_list .= "<th width='30px'>".$li->check("ReaderID[]")."</th>\n";
//			$li->no_col = $pos+3;
//		
//			$x .= $li->display();
//			$x .= '<input type="hidden" name="pageNo" value="'.$li->pageNo.'" />';
//			$x .= '<input type="hidden" name="order" value="'.$li->order.'" />';
//			$x .= '<input type="hidden" name="field" value="'.$li->field.'" />';
//			$x .= '<input type="hidden" name="page_size_change" value="" />';
//			$x .= '<input type="hidden" name="numPerPage" value="'.$li->page_size.'" />';
//		
//		return $x;		
//		}
		
//		private function getExtraInfoDBTableSql($Keyword='', $LocationID, $status) {
//			global $locationConfigAry;
//			global $Lang;
//			
//			if(trim($Keyword)!='') {
//				$Keyword = trim($Keyword);
//				$Keyword = $this->objDb->Get_Safe_Sql_Like_Query($Keyword);
//				$cond_Keyword = " 
//					AND 
//					(
//						li.NameEng  LIKE '%$Keyword%'
//						Or ilcr.NameEng LIKE '%$Keyword%'
//						Or ilcr.NameChi LIKE '%$Keyword%' 
//						Or ilcr.Code LIKE '%$Keyword%'
//						Or ilb.NameEng LIKE '%$Keyword%'
//						Or ilb.NameChi LIKE '%$Keyword%'
//						Or ill.NameEng LIKE '%$Keyword%'
//						Or ill.NameChi LIKE '%$Keyword%'
//					)
//				";
//			}
//			
//			if(trim($LocationID)!='')
//			{
//				if($LocationID!='ALL'){
//					
//					$location_Cond_Keyword = "
//					AND 
//						(
//						ilcr.LocationID  = '$LocationID'
//					)";
//				}
//				else{
//					$location_Cond_Keyword ="";
//				}
//			}
//			
//			
//			if(trim($status)!='')
//			{
//				if($status!='ALL'){
//					
//					$status_Cond_Keyword = "
//					AND 
//						(
//						ilcr.RecordStatus  = '$status'
//					)";
//				}
//				else{
//					$status_Cond_Keyword ="";
//				}
//			}
//			
//			$cond_Status = '';
//			if ($status !== '') {
//				$cond_Status = " AND ilcr.RecordStatus  = '$status' ";
//			}
//			
//			$sql = "SELECT
//						CONCAT(ilb.NameEng,' > ', ill.NameEng, ' > ', li.NameEng) AS RoomNameEN,
//						ilcr.Code,
//						CONCAT('<a class=\"tablelink\" href=\"edit.php?ReaderID=', ilcr.ReaderID, '\">', ilcr.NameEng, '</a>') as NameEng,
//						CONCAT('<a class=\"tablelink\" href=\"edit.php?ReaderID=', ilcr.ReaderID, '\">', ilcr.NameChi, '</a>') as NameChi,	
//						CASE ilcr.RecordStatus
//							WHEN 0 THEN '". $Lang['General']['Disabled']."'
//							WHEN 1 THEN '". $Lang['General']['Enabled']."'
//						END	,
//						CONCAT('<input type=\"checkbox\" class=\"userIdSel\" name=\"ReaderID[]\" value=\"',ilcr.ReaderID,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\">') AS CheckBox,
//						ilcr.LocationID,
//						ilb.NameEng,
//						ilb.NameChi,
//						ill.NameEng,
//						ill.NameChi
//					FROM 
//						INVENTORY_LOCATION_CARD_READER ilcr
//						INNER JOIN INVENTORY_LOCATION  li ON ilcr.LocationID = li.LocationID
//						INNER JOIN INVENTORY_LOCATION_LEVEL ill ON ill.LocationLevelID = li.LocationLevelID
//						INNER JOIN INVENTORY_LOCATION_BUILDING  ilb ON ilb.BuildingID  = ill.BuildingID 
//					WHERE
//						ilcr.IsDeleted = '".$locationConfigAry['INVENTORY_LOCATION_CARD_READER']['IsDeleted']['notDeleted']."'
//						And li.RecordStatus = 1
//						And ill.RecordStatus = 1
//						And ilb.RecordStatus = 1
//						$cond_Keyword
//						$location_Cond_Keyword
//						$cond_Status
//						";
//			return $sql;
//		}
		
				
		function Get_Status_Selection($ID_Name, $OptSelected='', $Others='', $ParUserID='', $ShowAllToAdmin=0, $noFirst=0, $ShowIfFacilityPeriodSetInSchoolYearID=0, $excludedFacilityID=array())
		{
			global $Lang;
			global $locationConfigAry;		
		
			$SelectionBox .= "<select id='$ID_Name' name='$ID_Name' $Others>";

			$selected = $OptSelected==''?"selected":"";
			$SelectionBox .= "<option value='' $selected>".$Lang['RepairSystem']['AllInUseStatus']."</option>";
	
			$selected = $OptSelected=='1'?"selected":"";
			$SelectionBox .= "<option value='1' $selected>".$Lang['General']['Enabled']."</option>";						
				
			$selected = $OptSelected=='0'?"selected":"";
			$SelectionBox .= "<option value='0' $selected>".$Lang['General']['Disabled']."</option>";						
						
			$SelectionBox .= "</select>";
					
			return $SelectionBox;
		}
		
		
		function getCardReaderInfoByLocationId($LocationId, $TargetRecordStatusAry='') {
			global $locationConfigAry;
			
			$condsRecordStatus = '';
			if ($TargetRecordStatusAry === '') {
				// get all enabled and disabled card reader info
			}
			else {
				// get enabled / disabled 
				$condsRecordStatus = " And RecordStatus In ('".implode("','", (array)$TargetRecordStatusAry)."') ";
			}
			
			$sql = "Select
							ReaderID,
							Code,
							NameEng,
							NameChi,
							LocationID
					From 
							INVENTORY_LOCATION_CARD_READER
					Where
							LocationID In ('".implode("','", (array)$LocationId)."')
							And IsDeleted = '".$locationConfigAry['INVENTORY_LOCATION_CARD_READER']['IsDeleted']['notDeleted']."'
							$condsRecordStatus
					";
			return $this->objDb->returnResultSet($sql);
		}
		
		
		# Door Access Record Display DB Table
		public function geteBookingSettingDoorAccessDBTable($field, $order, $page, $Keyword, $LocationID, $status,$AccessRight) {
			global $PATH_WRT_ROOT, $Lang, $eReportCard, $page_size, $readerInfo_page_size;
			global $image_path;
			
			include_once($PATH_WRT_ROOT."includes/libdbtable.php");
			include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
			include_once($PATH_WRT_ROOT."includes/libdb.php");
			include_once($PATH_WRT_ROOT."includes/lib.php");
			$libdb = new libdb();
			 
			$field = ($field=='')? 0 : $field;
			$order = ($order=='')? 1 : $order;
			$page = ($page=='')? 1 : $page;
	
			if (isset($readerInfo_page_size) && $readerInfo_page_size != "") {
				$page_size = $readerInfo_page_size;
			}
			$li = new libdbtable2007($field,$order,$page);
			
			$sql = $this->geteBookingSettingDoorAccessDBTableSql($Keyword, $LocationID, $status, $AccessRight);
			
			
			$li->sql = $sql;
			$li->IsColOff = "IP25_table";	
			
			if($AccessRight=='Internal'){
				$li->field_array = array("RoomName", "ilcr.Code", "ilcr.NameEng", "ilcr.NameChi", "ilcr.RecordStatus");
			}
			else
			{
				$li->field_array = array("RoomName", "Name", "ilcr.RecordStatus");
			}
			
			$li->column_array = array(0,0);
			$li->wrap_array = array(0,0);
					
			$pos = 0;
			
			if($AccessRight=='Internal'){
				
				$li->column_list .= "<th width='10px' class='tabletoplink'>#</th>\n";
				$li->column_list .= "<th width='30%'>".$li->column_IP25($pos++, $Lang['SysMgr']['Location']['CardReader']['Location'])."</th>\n";
				$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $Lang['SysMgr']['Location']['CardReader']['Code'])."</th>\n";
				$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['SysMgr']['Location']['CardReader']['ReaderName'].$Lang['General']['FormFieldLabel']['En'])."</th>\n";
				$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['SysMgr']['Location']['CardReader']['ReaderName'].$Lang['General']['FormFieldLabel']['Ch'])."</th>\n";
				$li->column_list .= "<th width='10%'>". $Lang['SysMgr']['Location']['CardReader']['Status']."</th>\n";
				$li->column_list .= "<th width='30px'>".$li->check("ReaderID[]")."</th>\n";
				$li->no_col = $pos+3;
						
			}else{
				$li->column_list .= "<th width='5px' class='tabletoplink'>#</th>\n";
				$li->column_list .= "<th width='40%'>".$li->column_IP25($pos++, $Lang['SysMgr']['Location']['CardReader']['Location'])."</th>\n";
				$li->column_list .= "<th width='35%'>".$li->column_IP25($pos++, $Lang['SysMgr']['Location']['CardReader']['ReaderName'])."</th>\n";
				$li->column_list .= "<th width='20%'>". $Lang['SysMgr']['Location']['CardReader']['Status']."</th>\n";
				$li->column_list .= "<th width='5px'>".$li->check("ReaderID[]")."</th>\n";
				$li->no_col = $pos+3;
				
			}
			
			$x .= $li->display();
			$x .= '<input type="hidden" name="pageNo" value="'.$li->pageNo.'" />';
			$x .= '<input type="hidden" name="order" value="'.$li->order.'" />';
			$x .= '<input type="hidden" name="field" value="'.$li->field.'" />';
			$x .= '<input type="hidden" name="page_size_change" value="" />';
			$x .= '<input type="hidden" name="numPerPage" value="'.$li->page_size.'" />';
		
		return $x;		
		}
		
		
		# SQL Result for Door Access Record Display DB Table
		private function geteBookingSettingDoorAccessDBTableSql($Keyword='', $LocationID, $status, $AccessRight) {
			global $Lang, $locationConfigAry, $intranet_session_language, $image_path, $LAYOUT_SKIN;

			if(trim($Keyword)!='') {
				$Keyword = trim($Keyword);
				$Keyword = $this->objDb->Get_Safe_Sql_Like_Query($Keyword);
				$cond_Keyword = " 
					AND 
					(
						li.NameEng  LIKE '%$Keyword%'
						Or ilcr.NameEng LIKE '%$Keyword%'
						Or ilcr.NameChi LIKE '%$Keyword%' ";

				if($AccessRight=='Internal'){
					$cond_Keyword .= " Or ilcr.Code LIKE '%$Keyword%'";
				}
				
				$cond_Keyword .= " 	
						Or ilb.NameEng LIKE '%$Keyword%'
						Or ilb.NameChi LIKE '%$Keyword%'
						Or ill.NameEng LIKE '%$Keyword%'
						Or ill.NameChi LIKE '%$Keyword%'
					)
				";
			}
			
			if(trim($LocationID)!='')
			{
				if($LocationID!='ALL'){
					
					$location_Cond_Keyword = "
					AND 
						(
						ilcr.LocationID  = '$LocationID'
					)";
				}
				else{
					$location_Cond_Keyword ="";
				}
			}
			
			
			if(trim($status)!='')
			{
				if($status!='ALL'){
					
					$status_Cond_Keyword = "
					AND 
						(
						ilcr.RecordStatus  = '$status'
					)";
				}
				else{
					$status_Cond_Keyword ="";
				}
			}
			
			$cond_Status = '';
			if ($status === '' || $status === null) {
				
			}
			else {
				$cond_Status = " AND ilcr.RecordStatus  = '$status' ";
			}
			

			$selectionCondition='';
			if($AccessRight=='Internal'){
				if ($_SESSION['intranet_session_language'] == 'en') {
					$selectionCondition = "CONCAT(ilb.NameEng,' > ', ill.NameEng, ' > ', li.NameEng) AS RoomName,";
				}
				else 
				{
					$selectionCondition = "CONCAT(ilb.NameChi,' > ', ill.NameChi, ' > ', li.NameChi) AS RoomName,";
				}			
					$selectionCondition.= "ilcr.Code,
										   CONCAT('<a class=\"tablelink\" href=\"edit.php?ReaderID=', ilcr.ReaderID, '\">', ilcr.NameEng, '</a>') as NameEng,
										   CONCAT('<a class=\"tablelink\" href=\"edit.php?ReaderID=', ilcr.ReaderID, '\">', ilcr.NameChi, '</a>') as NameChi,";	
			}
			else{
					
				if ($_SESSION['intranet_session_language'] == 'en') {
					$selectionCondition = "CONCAT(ilb.NameEng,' > ', ill.NameEng, ' > ', li.NameEng) AS RoomName,";
					$selectionCondition .= "CONCAT('<a class=\"tablelink\" href=\"edit.php?ReaderID=', ilcr.ReaderID, '\">', ilcr.NameEng, '</a>') as Name,";		
				}
				else {
					$selectionCondition = "CONCAT(ilb.NameChi,' > ', ill.NameChi, ' > ', li.NameChi) AS RoomName,";
					$selectionCondition .= "CONCAT('<a class=\"tablelink\" href=\"edit.php?ReaderID=', ilcr.ReaderID, '\">', ilcr.NameChi, '</a>') as Name,";	
				}
			
			}
			
			$approvedImg = "<img src=\'{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif\' width=20 height=20 align=absmiddle title=\'".$Lang['General']['Enabled']."\'>";
			$rejectImg = "<img src=\'{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif\' width=20 height=20 align=absmiddle title=\'".$Lang['General']['Disabled']."\'>";
			
			$sql = "SELECT
						$selectionCondition
						
						CASE ilcr.RecordStatus
							WHEN 0 THEN '". $rejectImg ."'
							WHEN 1 THEN '". $approvedImg ."'
						END	,
						CONCAT('<input type=\"checkbox\" class=\"userIdSel\" name=\"ReaderID[]\" value=\"',ilcr.ReaderID,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\">') AS CheckBox,
						ilcr.LocationID,
						ilb.NameEng,
						ilb.NameChi,
						ill.NameEng,
						ill.NameChi
					FROM 
						INVENTORY_LOCATION_CARD_READER ilcr
						INNER JOIN INVENTORY_LOCATION  li ON ilcr.LocationID = li.LocationID
						INNER JOIN INVENTORY_LOCATION_LEVEL ill ON ill.LocationLevelID = li.LocationLevelID
						INNER JOIN INVENTORY_LOCATION_BUILDING  ilb ON ilb.BuildingID  = ill.BuildingID 
					WHERE
						ilcr.IsDeleted = '".$locationConfigAry['INVENTORY_LOCATION_CARD_READER']['IsDeleted']['notDeleted']."'
						And li.RecordStatus = 1
						And ill.RecordStatus = 1
						And ilb.RecordStatus = 1
						$cond_Keyword
						$location_Cond_Keyword
						$cond_Status
						";
			return $sql;
		}
		
		
		# Door Access Edit/New Page
		public function getDoorAccessEditPage($ReaderID, $LocationID, $AccessRight='') {
			global $Lang, $PATH_WRT_ROOT, $locationConfigAry;	
			include_once($PATH_WRT_ROOT."includes/libinterface.php");
			include_once($PATH_WRT_ROOT."includes/liblocation_cardReader.php");
			include_once($PATH_WRT_ROOT."includes/libebooking.php");
			include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
			
			
			$linterface 	= new interface_html();
			$extraCardReaderObj = new liblocation_cardReader($ReaderID);
			$lebooking 		= new libebooking();
			$lebooking_ui	= new libebooking_ui();
		
			## Navigation
			$PAGE_NAVIGATION = array();
			if($AccessRight=='Internal'){
				$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Location']['CardReader']['ReaderInfo'], "javascript:goBack();");
			}
			else{
				$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['FieldTitle']['DoorAccess'], "javascript:goBack();");
			}
			
			$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
			$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
			
			# Location selection box 
			$locationSelectionDisplay ="";
			$locationIDSelectionBox = $extraCardReaderObj->getLocationId();
			$locationSelectionDisplay = $lebooking_ui->Get_Booking_Location_Selection('locationIDSelectionBox', $locationIDSelectionBox, '','', '', '', '','');
			
			$action = "edit_update.php?ReaderID=$ReaderID";
			$ReportTypeRequired = '<span class="tabletextrequire">*</span>';
			$x = '';
			$x .= '<form id="form1" name="form1" method="post" onsubmit="return CheckForm();" action="'.$action.'">'."\n";
				$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'."\n";
							$x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)."\n";
							$x .= '<br><br>'."\n";	
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'."\n";
			
							##### main table ##### 
							$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="2" class="form_table_v30">'."\n";
								$x .= '<col class="field_title">';
								$x .= '<col class="field_c">';
								
								if($AccessRight=='Internal'){
								
								# Location
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$ReportTypeRequired. $Lang['SysMgr']['Location']['CardReader']['Location'].'</td>'."\n";		
									$x .= '<td>'."\n";
										$x .= $locationSelectionDisplay."\n";
											$x .= $linterface->Get_Form_Warning_Msg('locationIDWarningDiv', $Lang['SysMgr']['Location']['CardReader']['EmptyArr']['LocationID'], $Class='warningDiv');	
											$x .= $linterface->Get_Form_Warning_Msg('LocationIDInvalidWarningDiv', $Lang['SysMgr']['Location']['CardReader']['MoreThanTwo']['LocationID'], $Class='warningDiv');	
									$x .= '</td>'."\n";		
								$x .= '</tr>'."\n";
			
								# Reader ID
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$ReportTypeRequired. $Lang['SysMgr']['Location']['CardReader']['Code'].'</td>'."\n";		
									$x .= '<td>'."\n";							
										$x .= $linterface->GET_TEXTBOX_NUMBER('ReaderCode', 'ReaderCode', $extraCardReaderObj->getCode(), $OtherClass='', array('maxlength' => $locationConfigAry['INVENTORY_LOCATION_CARD_READER']['Code']['maxLength']));
										$x .= $linterface->Get_Form_Warning_Msg('readerIDWarningDiv', $Lang['SysMgr']['Location']['CardReader']['EmptyArr']['ReaderID'], $Class='warningDiv');	
										$x .= $linterface->Get_Form_Warning_Msg('readerIDExistingWarningDiv', $Lang['SysMgr']['Location']['CardReader']['EmptyArr']['ReaderIDExist'], $Class='warningDiv');	
									$x .= '</td>'."\n";		
								$x .= '</tr>'."\n";
								
								}
								
								# Reader Name
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title" rowspan="2">'.$ReportTypeRequired.$Lang['SysMgr']['Location']['CardReader']['ReaderName'].'</td>'."\n";		
									$x .= '<td><span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['En'].'</span>'."\n";	
									//GET_TEXTBOX_NAME
										$x .= $linterface->GET_TEXTBOX_NAME('ReaderNameEng', 'ReaderNameEng', $extraCardReaderObj->getNameEng(), $OtherClass='', array('maxlength' => $locationConfigAry['INVENTORY_LOCATION_CARD_READER']['NameEng']['maxLength']));
										$x .= $linterface->Get_Form_Warning_Msg('readerNameEngWarningDiv', $Lang['SysMgr']['Location']['CardReader']['EmptyArr']['ReaderNameEng'], $Class='warningDiv').''."\n";		
									$x .= '</td>'."\n";		
								$x .= '</tr>'."\n";
								
								$x .= '<tr>'."\n";
									// GET_TEXTBOX_NAME
									$x .= '<td><span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['Ch'].'</span>'."\n";	
										$x .= $linterface->GET_TEXTBOX_NAME('ReaderNameChi', 'ReaderNameChi', $extraCardReaderObj->getNameChi(), $OtherClass='', array('maxlength' => $locationConfigAry['INVENTORY_LOCATION_CARD_READER']['NameChi']['maxLength']));
										$x .= $linterface->Get_Form_Warning_Msg('readerNameEngWarningDiv', $Lang['SysMgr']['Location']['CardReader']['EmptyArr']['ReaderNameChi'], $Class='warningDiv').''."\n";		
									$x .= '</td>'."\n";	
									
								$x .= '</tr>'."\n";
								
								# Remarks
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['SysMgr']['Location']['CardReader']['Remarks'].'</td>'."\n";		
									$x .= '<td>'."\n";
										// GET_TEXTAREA
										$x .= $linterface->GET_TEXTAREA('ReaderRemarks', $extraCardReaderObj->getRemarks(), $taCols=70, $taRows=5, $OnFocus = "", $readonly = "", $other='', $class='tabletext', $taID='', $CommentMaxLength='');
									$x .= '</td>'."\n";		
								$x .= '</tr>'."\n";
						
								$status = $extraCardReaderObj->getRecordStatus();
								
								if($status == $locationConfigAry['INVENTORY_LOCATION_CARD_READER']['RecordStatus']['enabled']){
									$enabledChecked = true;
									$disabledChecked = false;
								}elseif($status == $locationConfigAry['INVENTORY_LOCATION_CARD_READER']['RecordStatus']['disabled']){
									$disabledChecked = true;
									$enabledChecked = false;
								}
												
								# Status
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$ReportTypeRequired.$Lang['SysMgr']['Location']['CardReader']['Status'].'</td>'."\n";		
									$x .= '<td>'."\n";
									
									// Get_Radio_Button
									$x .= $linterface->Get_Radio_Button('CardReaderStatusEnabled', 'CardReaderStatus', $locationConfigAry['INVENTORY_LOCATION_CARD_READER']['RecordStatus']['enabled'], $enabledChecked, $Class="", $Lang['General']['Enabled'], $Onclick="",$isDisabled=0);
									$x .= $linterface->Get_Radio_Button('CardReaderStatusDisabled', 'CardReaderStatus', $locationConfigAry['INVENTORY_LOCATION_CARD_READER']['RecordStatus']['disabled'], $disabledChecked, $Class="", $Lang['General']['Disabled'], $Onclick="",$isDisabled=0);	
										
									$x .= '</td>'."\n";		
								$x .= '</tr>'."\n";
							
							$x .= '</table>'."\n";	
							$x .= '<br>'."\n";
							$x .= $linterface->MandatoryField();
							# Edit bottom
							$SubmitBtn = $linterface->Get_Action_Btn($Lang['Btn']['Save'],"submit","","SaveBtn");
							$CancelBtn = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'],"button","window.location='index.php';","BackBtn");
							$x .= '<div class="edit_bottom_v30">';
								$x .= $SubmitBtn."\n";
								$x .= "&nbsp;".$CancelBtn."\n";
							$x .= '</div>';
							
						$x .= '</td>'."\n";		
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
				
				$exisitingLocation = $locationIDSelectionBox;
				$exisitingCode = $extraCardReaderObj->getCode();
				$x .= '<input type="hidden" id="ReportID" name="ReportID" value="'.$ReportID.'">';	
				$x .= '<input type="hidden" id="exisitingCode" name="exisitingCode" value="'.$exisitingCode.'">';	
				$x .= '<input type="hidden" id="exisitingLocation" name="exisitingLocation" value="'.$exisitingLocation.'">';	
			$x .= '</form>'."\n";
			$x .= $linterface->FOCUS_ON_LOAD("form1.ReportTitleEn")."\n";
					
			return $x;	
		}
		
		
  }  
} // End of directives
?>