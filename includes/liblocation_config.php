<?php
$locationConfigAry = array();

$locationConfigAry['INVENTORY_LOCATION_CARD_READER']['Code']['maxLength'] = 64;
$locationConfigAry['INVENTORY_LOCATION_CARD_READER']['NameEng']['maxLength'] = 255;
$locationConfigAry['INVENTORY_LOCATION_CARD_READER']['NameChi']['maxLength'] = 255;

$locationConfigAry['INVENTORY_LOCATION_CARD_READER']['RecordStatus']['enabled'] = 1;
$locationConfigAry['INVENTORY_LOCATION_CARD_READER']['RecordStatus']['disabled'] = 0;
$locationConfigAry['INVENTORY_LOCATION_CARD_READER']['IsDeleted']['deleted'] = 1;
$locationConfigAry['INVENTORY_LOCATION_CARD_READER']['IsDeleted']['notDeleted'] = 0;
?>