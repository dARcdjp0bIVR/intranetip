<?php
// Modifying by : 

##### Change Log [Start] #####
#	
#	Date	:	2013-02-25 (Yuen)
#				updated retrieveDigitalArchiveAccessRight() to follow given user_id in order to support API call (e.g. Ricoh copier) 
#
#	Date	:	2012-08-28 (YatWoon)
#				updated NO_ACCESS_RIGHT_REDIRECT(), no need "exit" if display error message
#
#	Date	:	2011-05-27 (Henry Chow)
#				modified retrieveUserAccessRight(), added retrieveDigitalArchiveAccessRightGroupID(), retrieveDigitalArchiveAccessRight()
#				add "Digital Archive" access right checking
#
#	Date	:	2011-04-13 (Henry Chow)
#				modified retrieveUserAccessRight(), assign to same "Access Right" array for 2 different cases 
#
#	Date	:	2011-04-13 (Henry Chow)
#				modified retrieveAccessRightGroupID(), if $this->Module is NULL, will not encounter this condition
#
###### Change Log [End] ######
	class libaccessright extends libdb
	{
		var $Module;
		function libaccessright($Module="")
		{
			parent::libdb();
			if ($Module != "") 
				$this->Module = $Module;
		}
		
		/*
		function IS_ADMIN_USER($Module='')
		{
			return true;	
		}
		*/
		
		// interface function needed by all function in this class
		// don't remove or comment it (kenneth chung - 2009-12-16)
		function IS_ADMIN_USER() {
		}
		
		###########################################################
		# Check user have the access right or not
		###########################################################
		function CHECK_ACCESS($access_function="", $group_right_settings=null)
		{
			global $SESSION_ACCESS_RIGHT;
			
			if($this->IS_ADMIN_USER())
			{
				return true;
			}
			else
			{
				if($access_function)
				{
					$access_function = strtoupper($access_function);
					# check the access fucntion permission later (from session)
					list($Module, $Section, $Function, $Action) = explode("-",$access_function);

					$group_right_settings = ($group_right_settings==null) ? $SESSION_ACCESS_RIGHT : $group_right_settings;
					if(isset($group_right_settings[$Module][$Section][$Function]))
					{
						if(in_array($Action, $group_right_settings[$Module][$Section][$Function])) {
	 						return true;
 						}
					}
				}
				return false;
			}
		}

		###########################################################
		# Check user have the access right or not (For Student Mode)
		# Added by Marcus 6/7/2009
		###########################################################
		function CHECK_STUDENT_ACCESS($access_function="")
		{
			global $SESSION_ACCESS_RIGHT,$PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($_SESSION["UserID"]);

			if(!$lu->isStudent())
				return false;
			else
			{
				if($access_function)
				{
					$access_function = strtoupper($access_function);
					# check the access fucntion permission later (from session)
					list($Module, $Section, $Function, $Action) = explode("-",$access_function);

					if(isset($SESSION_ACCESS_RIGHT[$Module][$Section][$Function]))
					{
						if(in_array($Action, $SESSION_ACCESS_RIGHT[$Module][$Section][$Function]))
	 						return true;
					}
				}
				return false;
			}
		}
		####################################################
		# check the user can access module
		####################################################
		function CHECK_MODULE_ACCESS($access_module="")
		{
			global $plugin, $SESSION_ACCESS_RIGHT;

			if($this->IS_ADMIN_USER())
			{
				return true;
			}
			
			$access_module = $access_module ? $access_module : $this->Module;
			
			if($access_module)
			{
				$access_module = strtoupper($access_module);

				if(isset($SESSION_ACCESS_RIGHT[$access_module]))
 						return true;
			}

			return false;
		}

		function CHECK_FUNCTION_ACCESS($access_function="")
		{
			global $plugin, $SESSION_ACCESS_RIGHT;

			if($this->IS_ADMIN_USER())
			{
				return true;
			}

			if($access_function)
			{
				$access_function = strtoupper($access_function);

				# check the access fucntion permission later (from session)
				list($Module, $Section, $Function) = explode("-",$access_function);

				if(isset($SESSION_ACCESS_RIGHT[$Module][$Section][$Function]))
 						return true;
			}

			return false;
		}

		function CHECK_SECTION_ACCESS($access_function="")
		{
			global $plugin, $SESSION_ACCESS_RIGHT;

			if($this->IS_ADMIN_USER($_SESSION['UserID']))
			{
				return true;
			}
			
			if($access_function)
			{
				$access_function = strtoupper($access_function);

				# check the access fucntion permission later (from session)
				list($Module, $Section, $Function) = explode("-",$access_function);
				
				if(isset($SESSION_ACCESS_RIGHT[$Module][$Section])) {
 						return true;
 						//echo 1;
				}
			}

			return false;
		}
		
		#Add By Marcus 30.6
		#Check Student Section Access Right For Student
		function CHECK_SECTION_ACCESS_STUDENT($access_function="")
		{
			global $plugin, $SESSION_ACCESS_RIGHT,$PATH_WRT_ROOT;;

			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($_SESSION["UserID"]);

			if(!$lu->isStudent())
				return false;
			
			if($access_function)
			{
				$access_function = strtoupper($access_function);

				# check the access fucntion permission later (from session)
				list($Module, $Section, $Function) = explode("-",$access_function);

				if(isset($SESSION_ACCESS_RIGHT[$Module][$Section]))
 						return true;
			}
			return false;
		}

		
		###########################################################
		# Check user have the access right or not
		###########################################################
		function CONTROL_ACCESS($access_function="", $msg="")
		{
			global $plugin, $PATH_WRT_ROOT,$eDiscipline;

			if((!$access_function or !$this->CHECK_ACCESS($access_function)) AND !$this->IS_ADMIN_USER())
			{
				$this->NO_ACCESS_RIGHT_REDIRECT($msg);
			}
			
			/*
			if($plugin['Disciplinev12'])
			{
				if($access_function=="Discipline-SETTINGS-eNoticeTemplate-Access")
				{
					include_once($PATH_WRT_ROOT."/includes/libnotice.php");
					$lnotice = new libnotice();
					if ($lnotice->disabled)
					{
						$this->NO_ACCESS_RIGHT_REDIRECT($eDiscipline["jsWarning"]["EnableEnotice"],"/home/admin/disciplinev12/index.php");	
					}
				}
			}
			*/
		}

		function NO_ACCESS_RIGHT_REDIRECT($msg="", $redirect_link="")
		{
			global $i_general_no_access_right;

			intranet_closedb();
			
			if($msg && !$redirect_link)
			{
				echo $msg;
				//exit;
			}
			else
			{
				echo "<script language='javascript'>";
				if($msg)
					echo "alert('". $msg ."');";
				else
					echo "alert('". $i_general_no_access_right ."');";
					
				if($redirect_link)
					echo "window.location='". $redirect_link ."'";
				else
				{
					if($_SESSION['UserID'])
						echo "history.back();";
					else
						echo "window.location='/'";
				}
				echo "</script>";
			}
		}
		
		################################################################
		# INSERT ACCESS RIGHT in "Discipline->Settings" Section
		################################################################
		function INSERT_ACCESS_RIGHT($GroupID, $Module, $array)
		{
			$Section = $this->SectionAry;
			$Function = $this->FunctionAry;
			$maxArrayLength = $this->maxArrayLength;
			
			for ($i=0; $i<sizeof($Section); $i++) {
				for($j=0;$j<sizeof($Function); $j++) {
					$SectionName = $Section[$i];
					$FunctionName = $Function[$j];
					for($k=0;$k<$maxArrayLength;$k++) {
						if($array[$SectionName][$FunctionName][$k] != '') {
							$ActionName = $array[$SectionName][$FunctionName][$k];
							$sql = "INSERT INTO ACCESS_RIGHT_GROUP_SETTING SET GroupID=$GroupID, Module='$Module', Section='$SectionName', Function='$FunctionName', Action='$ActionName', DateInput=NOW(), DateModified=NOW()";
							$this->db_db_query($sql);
						}
					}
				}
			}

		}

		function DELETE_ACCESS_RIGHT($GroupID, $GroupType, $Module="", $Section="", $Function="", $Action="")
		{
			if($GroupType=='S' || $GroupType=='T') {	// Student & Teacher
				$sql = "DELETE FROM ACCESS_RIGHT_GROUP_SETTING WHERE GroupID=$GroupID";
				return $this->db_db_query($sql);
			} else {	// $GroupType == 'A' (Admin Group)
				$Result = array();
				for($i=0;$i<sizeof($GroupID);$i++) {
					$sql = "DELETE FROM ACCESS_RIGHT_GROUP WHERE GroupID=$GroupID[$i]";
					$Result[] = $this->db_db_query($sql);
					$sql = "DELETE FROM ACCESS_RIGHT_GROUP_SETTING WHERE GroupID=$GroupID[$i]";
					$Result[] = $this->db_db_query($sql);
					$sql = "DELETE FROM ACCESS_RIGHT_GROUP_MEMBER WHERE GroupID=$GroupID[$i]";
					$Result[] = $this->db_db_query($sql);
				}
				
				return !in_array(false,$Result);
			}

		}

		function DELETE_ACCESS_RIGHT_MEMBER($GroupID, $GroupType, $UserID)
		{
			for($i=0;$i<sizeof($UserID);$i++) {
				$sql = "DELETE FROM ACCESS_RIGHT_GROUP_MEMBER WHERE GroupID=$GroupID AND UserID=$UserID[$i]";
				$this->db_db_query($sql);
			}
		}

		function GET_ACCESS_RIGHT_GROUP_ID($GroupType, $GroupTitle, $gID=0)
		{
			$sql = "SELECT GroupID FROM ACCESS_RIGHT_GROUP WHERE GroupType='$GroupType' and Module='". $this->Module."'";
			if($GroupTitle=='') {
				$sql .= " AND (GroupTitle is NULL or GroupTitle ='') ";
			} else {
				$sql .= " AND GroupTitle='$GroupTitle'";
			}
			
			$GroupID = $this->returnVector($sql);
			return $GroupID[0];
			
			/*

			if($GroupID[0] == "" && $gID==0)  {	# new access right group
				//$sql = "INSERT INTO ACCESS_RIGHT_GROUP SET GroupType='$GroupType', GroupTitle='$GroupTitle' DateInput=NOW(), DateModified=NOW(), Module='". $this->Module."'";
				#Syntax Error, Fixed by Marcus 3/6/09
				$sql = "INSERT INTO ACCESS_RIGHT_GROUP (GroupType, GroupTitle, DateInput, DateModified, Module) VALUES ('$GroupType', ".($GroupTitle?"'$GroupTitle'":"NULL").", NOW(), NOW(), '". $this->Module."')";
				$this->db_db_query($sql);
				$GroupID = $this->db_insert_id();
				
				$sql = "SELECT GroupID FROM ACCESS_RIGHT_GROUP WHERE GroupType='$GroupType' and Module='". $this->Module."'";
				if($GroupTitle=='') {
					$sql .= " AND GroupTitle is NULL";
				} else {
					$sql .= " AND GroupTitle='$GroupTitle'";
				}
				return $GroupID;
			} else {
				return $GroupID[0];
			}
			*/
		}

		function GET_GROUP_ACCESS_RIGHT_GROUP_ID($GroupType, $GroupTitle)
		{
			$sql = "SELECT GroupID FROM ACCESS_RIGHT_GROUP WHERE GroupType='$GroupType' and Module='". $this->Module."'";
			if($GroupTitle=='') {
				$sql .= " AND (GroupTitle is NULL or GroupTitle ='') ";
			} else {
				$sql .= " AND GroupTitle='$GroupTitle'";
			}
			$GroupID = $this->returnVector($sql);

			return $GroupID[0];
		}

		function VIEW_ACCESS_RIGHT($GroupID, $Module, $Section, $Function, $Action1, $Action2)
		{
			$sql = "SELECT Function, Action FROM ACCESS_RIGHT_GROUP_SETTING WHERE GroupID=$GroupID AND Module='$Module' AND Section='$Section' AND Function='$Function'";
			if($Action1 != '' || $Action2 != '') {
				$sql .= " AND (Action = '$Action1' OR Action='$Action2')";
			}
			$temp = $this->returnArray($sql,2);
			return $temp;
		}

		function NEW_ACCESS_RIGHT_GROUP($GroupType, $GroupTitle, $GroupDescription)
		{
			$sql = "INSERT INTO ACCESS_RIGHT_GROUP SET GroupType='$GroupType', GroupTitle='$GroupTitle', GroupDescription='$GroupDescription', DateInput=NOW(), DateModified=NOW(), Module='". $this->Module."'";
			$Result = $this->db_db_query($sql);
			
			if ($Result) 
				return $this->db_insert_id();
			else 
				return false;
		}

		function UPDATE_ACCESS_RIGHT_GROUP($GroupID, $GroupTitle, $GroupDescription)
		{
			$sql = "UPDATE ACCESS_RIGHT_GROUP SET GroupTitle='$GroupTitle', GroupDescription='$GroupDescription', DateModified=NOW() WHERE GroupID=$GroupID";
			return $this->db_db_query($sql);
		}

		function getAccessRightGroupInfo($GroupID)
		{
			$sql = "SELECT GroupTitle, GroupDescription, COUNT(b.UserID) as countUser FROM ACCESS_RIGHT_GROUP AS a LEFT OUTER JOIN ACCESS_RIGHT_GROUP_MEMBER as b ON a.GroupID=b.GroupID WHERE a.GroupID=$GroupID GROUP BY a.GroupID";
			$temp = $this->returnArray($sql,3);
			return $temp;
		}

		function NEW_ACCESS_GROUP_MEMBER($GroupID, $ary)
		{
			if (is_array($ary) && sizeof($ary)>0)
			{
				for($i=0;$i<sizeof($ary);$i++) {
					$sql = "INSERT INTO ACCESS_RIGHT_GROUP_MEMBER SET GroupID=$GroupID, UserID=$ary[$i], DateInput=NOW()";
					$this->db_db_query($sql);
				}
			}
		}
		
		##############################################
		# Access Right - START
		##############################################
		function retrieveUserAccessRight($this_UserID)
		{
			
			global $PATH_WRT_ROOT, $plugin;

			$this_UserID = $this_UserID ? $this_UserID : $_SESSION['UserID'];
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($this_UserID);
		
			$data = array();
			$GroupIDAry = array();
			$AccessRightAry = array();

			if($lu->isTeacherStaff() || $lu->isStudent() || $lu->isParent())
			{
				$GroupType = $lu->isTeacherStaff() ? "T" : "S";
				
				$GroupIDAry = $this->retrieveAccessRightGroupID($GroupType);
				
				# build Access Right Data Array
				foreach($GroupIDAry as $k=>$GroupID)
				{
					$AccessRightAry = $this->retrieveAccessRight($GroupID);
					foreach($AccessRightAry as $k => $dataTemp)
					{
						list($module, $section, $function, $action) = $dataTemp;
						if(isset($data[$module][$section][$function]))
						{
							if(!in_array($action, $data[$module][$section][$function]))
								$data[$module][$section][$function][] = $action;
						}
						else
							$data[$module][$section][$function][] = $action;
						}
				}
				
			}
// 			debug_pr($data);
			# check whether student is assigned to Access Right Group
			include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
			$ldiscipline = new libdisciplinev12();
			$studentAccessRightInGroup = $ldiscipline->retrieveUserAccessRightInGroup($this_UserID);
				
			if(!($lu->isStudent() || $lu->isParent()) || sizeof($studentAccessRightInGroup)>0) {
			//echo 1;
				# Group Member
				$MemberGroupIDAry = $this->retrieveAccessRightMemberGroupID($this_UserID);
				//$data = array();
				foreach($MemberGroupIDAry as $k=>$GroupID)
				{
					
					$AccessRightAry = $this->retrieveAccessRight($GroupID);
					
					foreach($AccessRightAry as $k => $dataTemp)
					{
						list($module, $section, $function, $action) = $dataTemp;
						if(isset($data[$module][$section][$function]))
						{
							if(!in_array($action, $data[$module][$section][$function]))
								$data[$module][$section][$function][] = $action;
						}
						else
							$data[$module][$section][$function][] = $action;
					}
				}
				//debug_pr($data);
			}

			if($plugin['digital_archive']) {
				if($lu->isTeacherStaff() || $lu->isStudent()) {	# Digital Archive currently only for teachers & students
					$DigitalArchiveGroupID = $this->retrieveDigitalArchiveAccessRightGroupID();

					for($i=0, $i_max=sizeof($DigitalArchiveGroupID); $i<$i_max; $i++) {
						$DigitalArchiveAccessRightAry = $this->retrieveDigitalArchiveAccessRight($DigitalArchiveGroupID[$i], $this_UserID);
						//debug("gr");debug_r($DigitalArchiveAccessRightAry);
						for($j=0, $j_max=sizeof($DigitalArchiveAccessRightAry); $j<$j_max; $j++) {
							list($module, $groupid, $roleFileType, $action) = $DigitalArchiveAccessRightAry[$j];
							if(isset($data[$module][$groupid][$roleFileType]))
							{
								if(!in_array($action, $data[$module][$groupid][$roleFileType])) {
									$data[$module][$groupid][$roleFileType][] = $action;
								}
							}
							else {
								$data[$module][$groupid][$roleFileType][] = $action;
							}	
						}
							
					}
				}
			}
			return $data;
			
		}
		
		function retrieveUserAccessRightInGroup($UserID)
		{			
			global $PATH_WRT_ROOT;

			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($UserID);
			
			$data = array();
			$GroupIDAry = array();
			$AccessRightAry = array();

			# Group Member
			$MemberGroupIDAry = $this->retrieveAccessRightMemberGroupID($UserID);
			$data = array();
			foreach($MemberGroupIDAry as $k=>$GroupID)
			{
				
				$AccessRightAry = $this->retrieveAccessRight($GroupID);
				
				foreach($AccessRightAry as $k => $dataTemp)
				{
					list($module, $section, $function, $action) = $dataTemp;
					if(isset($data[$module][$section][$function]))
					{
						if(!in_array($action, $data[$module][$section][$function]))
							$data[$module][$section][$function][] = $action;
					}
					else
						$data[$module][$section][$function][] = $action;
				}
			}
				
			return $data;
			
		}
		

		function retrieveAccessRightGroupID($GroupType='')
		{
			if($this->Module!="")
				$conds = " and Module='". $this->Module."'";
			
			$sql = "select GroupID from ACCESS_RIGHT_GROUP where GroupType='$GroupType' $conds";
			return $this->returnVector($sql);
			
		}

		function retrieveAccessRight($GroupID='')
		{
			$sql = "select UCASE(Module), UCASE(Section), UCASE(Function), UCASE(Action) from ACCESS_RIGHT_GROUP_SETTING where GroupID='$GroupID'";
			//echo $sql;
			return $this->returnArray($sql);
		}
		
		function retrieveDigitalArchiveAccessRightGroupID()
		{
			$sql = "SELECT GroupID FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP";
			return $this->returnVector($sql);	
		}
		
		function retrieveDigitalArchiveAccessRight($GroupID='', $user_id=0)
		{
			global $UserID;
			
			if ($user_id==0 || $user_id=="")
			{
				$user_id = $UserID;
			}

			$sql = "SELECT \"DIGITAL_ARCHIVE\", UCASE(CONCAT(\"GROUP\",$GroupID)), CONCAT(UCASE(Role),'_',UCASE(FileType)), UCASE(Action) FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_SETTING s INNER JOIN DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER m ON (m.GroupID=s.GroupID AND m.UserID='$user_id') WHERE s.GroupID='$GroupID'";

			return $this->returnArray($sql);
		}

		function retrieveAccessRightMemberGroupID($UserID)
		{
			$sql = "select GroupID from ACCESS_RIGHT_GROUP_MEMBER where UserID='$UserID'";
			return $this->returnVector($sql);
		}
		
		function GET_ACCESS_RIGHT_GROUP($GroupType='A')
		{
			$sql = "SELECT GroupID, GroupTitle FROM ACCESS_RIGHT_GROUP WHERE GroupType='$GroupType' and Module='". $this->Module."'";
			$result = $this->returnArray($sql);
			return $result;
		}
		

		##############################################
		# Access Right - END
		##############################################
	}
?>