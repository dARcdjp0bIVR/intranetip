<?php
class libcycle extends libfilesystem {

     var $Path;
     var $PathCycle;
     var $PathCycleStart;
     var $CycleID;
     var $DateStart;
     var $DateToday;
     var $CycleNum;
     var $CycleType;
     var $specialStart;
     var $specialEnd;
     var $countSat;

     # ------------------------------------------------------------------------------------

     function libcycle(){
          global $intranet_root;
          $this->Path = "$intranet_root/file";
          $this->PathCycle = $this->Path."/cycle.txt";
          $this->PathCycleStart = $this->Path."/cyclestart.txt";
          if (is_file($this->PathCycle))
          {
              $this->CycleID = $this->file_read($this->PathCycle);
              if (is_file($this->PathCycleStart))
              {
                  $temp = $this->file_read($this->PathCycleStart);
                  if ($temp != "")
                  {
                      $this->DateStart = strtotime($temp);
                  }
                  else
                  {
                      $this->DateStart = $this->file_get_modification_time($this->PathCycle);
                  }
              }
              else
              {
                  $this->DateStart = $this->file_get_modification_time($this->PathCycle);
              }
          }
          else
          {
              $this->CycleID = 0;
              $this->DateStart = mktime (0,0,0,date("m"),date("d"),date("Y"));
          }
          $this->DateToday = mktime (0,0,0,date("m"),date("d"),date("Y"));
          switch ($this->CycleID){
               case 0: $this->CycleNum = 0; $this->CycleType = 0; break;
               case 1: $this->CycleNum = 6; $this->CycleType = 1; break;
               case 2: $this->CycleNum = 7; $this->CycleType = 1; break;
               case 3: $this->CycleNum = 8; $this->CycleType = 1; break;
               case 4: $this->CycleNum = 6; $this->CycleType = 2; break;
               case 5: $this->CycleNum = 7; $this->CycleType = 2; break;
               case 6: $this->CycleNum = 8; $this->CycleType = 2; break;
               case 7: $this->CycleNum = 6; $this->CycleType = 3; break;
               case 8: $this->CycleNum = 7; $this->CycleType = 3; break;
               case 9: $this->CycleNum = 8; $this->CycleType = 3; break;
               case 10: $this->CycleNum = 5; $this->CycleType = 1; break;
               case 11: $this->CycleNum = 5; $this->CycleType = 2; break;
               case 12: $this->CycleNum = 5; $this->CycleType = 3; break;
               default: $this->CycleNum = 0; $this->CycleType = 0; break;
          }
          $specialfile = $this->file_read("$intranet_root/file/specialDates.txt");
          if (trim($specialfile) != "")
          {
              $specialDates = explode("\n",$specialfile);
              $this->specialStart = strtotime($specialDates[0]);
              $this->specialEnd = strtotime($specialDates[1]);
              $this->countSat = ($specialDates[0]!="" && $specialDates[1] != "");
          }
          else
          {
              $this->countSat = false;
          }

     }

     # ------------------------------------------------------------------------------------

     function getDay(){
          global $i_DayType0;
          return $i_DayType0[date("w",$this->DateToday)];
     }

     function getHolidayNumber(){
          $a = new libdb();
          $sql = "SELECT count(EventID) FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType = '2' AND (UNIX_TIMESTAMP(EventDate) BETWEEN ".$this->DateStart." AND ".$this->DateToday.")";
          $result = $a->returnVector($sql);
          return $result[0];
     }

     function getNonCycleDays()
     {
              # Special conds
              $special_conds = $this->isWeekEndSQLConds();

              $a = new libdb();
              $conds = "RecordStatus =1
              AND (UNIX_TIMESTAMP(EventDate) BETWEEN ".$this->DateStart." AND ".$this->DateToday.")
              AND (
                   (isSkipCycle IS NULL AND RecordType = 2)
                   OR
                   (isSkipCycle = 1)
              )
              AND $special_conds
              ";
              $sql = "SELECT count(distinct EventDate) FROM INTRANET_EVENT WHERE $conds";
              $result = $a->returnVector($sql);
              return $result[0];
     }
     function isWeekEndSQLConds()
     {
              if ($this->countSat)
              {
                  $conds = "(WEEKDAY(EventDate) NOT IN (5,6)
                            OR
                            (WEEKDAY(EventDate) = 5 AND
                             UNIX_TIMESTAMP(EventDate) BETWEEN ".$this->specialStart." AND ".$this->specialEnd."))";
              }
              else
              {
                  $conds = "(WEEKDAY(EventDate) NOT IN (5,6))";
              }
              return $conds;
     }

     function isWeekEnd($target_date)
     {
              if ($this->countSat)
              {
                  if ($target_date > $this->specialEnd || $target_date < $this->specialStart)
                  {
                      # Outside special range
                      return (date("w",$target_date) == 0 || date("w",$target_date) == 6);
                  }
                  else
                  {
                      # Inside special range
                      return (date("w",$target_date) == 0);
                  }

              }
              else
              {
                  return (date("w",$target_date) == 0 || date("w",$target_date) == 6);
              }
     }

     function getWeekendNumber(){
          $DateStart = $this->DateStart;
          $i = 0;
          while ($this->DateToday >= $DateStart){
                 if ($this->isWeekEnd($DateStart))
                 {
                     $i++;
                 }
                 $DateStart += 86400;
          }
          return $i;
     }

     function getDayNumber(){
          $DateStart = $this->DateStart;
          $i = 0;
          while ($this->DateToday > $DateStart){
               $i++;
               $DateStart += 86400;
          }
          return $i;
     }

     function getCycleNumber($cycle_no){
//          $x = ($this->getDayNumber() - $this->getWeekendNumber() - $this->getHolidayNumber());
          $x = ($this->getDayNumber() - $this->getWeekendNumber() - $this->getNonCycleDays());
          return ($x % $cycle_no);
     }

     function getCycle($cycle_no, $cycle_type){
          global ${"i_DayType".$cycle_type};
          $i_DayType = ${"i_DayType".$cycle_type};
          if ($this->DateToday == $this->DateStart) return $i_DayType[0];
          return ($this->DateToday > $this->DateStart) ? $i_DayType[$this->getCycleNumber($cycle_no)] : "";
     }

     # ------------------------------------------------------------------------------------

     function display(){
          $name = "";
          # Check is holiday or not
          $today = $this->DateToday;
          if ($this->isWeekEnd($today))
          {
              $name = "";
          }
          else
          {
              if ($this->CycleID >=1 && $this->CycleID <=12)
              {
                  $conds = "RecordStatus =1
                            AND (UNIX_TIMESTAMP(EventDate) = '$today')
                            AND (
                                 (isSkipCycle IS NULL AND RecordType = 2)
                                 OR
                                 (isSkipCycle = 1)
                            )";
                  $sql = "SELECT count(distinct EventDate) FROM INTRANET_EVENT WHERE $conds";
                  $a = new libdb();
                  $result = $a->returnVector($sql);
                  if ($result[0]!=0) $name = "";
                  else
                  {
                      switch ($this->CycleID){
                          case 1: $name = $this->getCycle(6,1); break;
                          case 2: $name = $this->getCycle(7,1); break;
                          case 3: $name = $this->getCycle(8,1); break;
                          case 4: $name = $this->getCycle(6,2); break;
                          case 5: $name = $this->getCycle(7,2); break;
                          case 6: $name = $this->getCycle(8,2); break;
                          case 7: $name = $this->getCycle(6,3); break;
                          case 8: $name = $this->getCycle(7,3); break;
                          case 9: $name = $this->getCycle(8,3); break;
                          case 10: $name = $this->getCycle(5,1); break;
                          case 11: $name = $this->getCycle(5,2); break;
                          case 12: $name = $this->getCycle(5,3); break;
                          default: $name = "";   //$this->getDay(); break;
                      }
                  }
              }
          }

          $weekday = $this->getDay();
          $month = date('Y-m');
          $day = date('d');

          $x = "<table width=183 border=0 cellspacing=0 cellpadding=0 background=/images/index/daily.gif>
                  <tr>
                    <td width=12 height=23><img src=/images/spacer.gif width=12 height=1></td>
                    <td width=63 height=23><img src=/images/spacer.gif width=63 height=1></td>
                    <td width=28 height=23><img src=/images/spacer.gif width=28 height=1></td>
                    <td width=76 align=center height=23 class=td_body>$month</td>
                    <td width=4 height=23><img src=/images/spacer.gif width=4 height=1></td>
                  </tr>
                  <tr>
                    <td width=12 height=46>&nbsp;</td>
                    <td width=63 align=center height=46 valign=top><font color=#FF0000 class=body>$name</font></td>
                    <td width=28 height=46>&nbsp;</td>
                    <td class=td_center_middle width=76 align=center height=46><b><font size=4 face='Verdana, Arial, Helvetica, sans-serif, �s�ө���'>$day</font></b><br>
                    <font size=2>$weekday</font></td>
                    <td width=4 height=46>&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan=5 height=3><img src=/images/spacer.gif width=183 height=3></td>
                  </tr>
               </table>";
          return $x;
     }
     function getCycleOptions($num, $cycle_type)
     {
              global ${"i_DayType".$cycle_type};
              $i_DayType = ${"i_DayType".$cycle_type};
              $x = "";
              for ($i=0; $i<$num; $i++)
              {
                   $value = $i + 7;
                   $x .= "<OPTION value=$value>".$i_DayType[$i]."</OPTION>\n";
              }
              return $x;
     }

     function getCycleDayFromDate($singleDate)
     {
              if ($this->CycleNum == 0) return "";
              if ($this->DateStart > strtotime($singleDate)) return -1;
              $this->DateToday = strtotime($singleDate);
              $start_stamp = $this->DateToday;
              $currentCycle = $this->getCycleNumber($this->CycleNum);
              $conds = "RecordStatus = 1
                        AND (UNIX_TIMESTAMP(EventDate) = ".$start_stamp.")
                        AND (
                             (isSkipCycle IS NULL AND RecordType = 2)
                             OR
                             (isSkipCycle = 1)
                            )
                        ";
              $sql = "SELECT distinct UNIX_TIMESTAMP(EventDate) FROM INTRANET_EVENT WHERE $conds";
              $a = new libdb();
              $holiday = $a->returnVector($sql);
              if ($this->isWeekEnd($start_stamp) || in_array($start_stamp, $holiday) )
              {
                  return -1;
              }
              else
              {
                  return $currentCycle;
              }
     }

     # return the cycle no of each date in $dates array, and must be in asc order
     function getCycleDaysFromDates ($dates)
     {
              #if (!is_array($dates) || sizeof($dates)) return;
              $notFullWeek = false;
              $result = array();
              if ($this->CycleNum ==0) return $dates;
              sort($dates);
              $size = sizeof($dates);
              if ($size==0) return $result;
              $start = $dates[0];
              $end = $dates[$size-1];
              $start_stamp = strtotime($start);
              $end_stamp = strtotime($end);
              $this->DateToday = $start_stamp;
              $currentCycle = $this->getCycleNumber($this->CycleNum);
              $start = $dates[0];
              $end = $dates[$size-1];
              $start_stamp = strtotime($start);
              $end_stamp = strtotime($end);
              $cycle_no = $this->CycleNum;

              if ($this->DateStart > $start_stamp)
              {
                  $currentCycle = -1;
                  $notFullWeek = true;
              }

              $special_conds = $this->isWeekEndSQLConds();
              $conds = "RecordStatus =1
                        AND (UNIX_TIMESTAMP(EventDate) BETWEEN ".$start_stamp." AND ".$end_stamp.")
                        AND (
                             (isSkipCycle IS NULL AND RecordType = 2)
                             OR
                             (isSkipCycle = 1)
                            )
                        AND $special_conds
                        ";
              $sql = "SELECT distinct UNIX_TIMESTAMP(EventDate) FROM INTRANET_EVENT WHERE $conds";
              $a = new libdb();
              $holiday = $a->returnVector($sql);
              if ($this->isWeekEnd($start_stamp) || in_array($start_stamp, $holiday) )
              {
                  $result[0] = -1;
                  //$currentCycle--;
              }
              else
              {
                  if ($this->DateStart > $start_stamp)
                  {
                      $currentCycle = -1;
                      $notFullWeek = true;
                  }
                  $result[0] = $currentCycle;
              }

              if ($size==1) return $result;
              $temp = $start_stamp;
              $prev = $start_stamp;

              for ($i=1; $i<$size; $i++)
              {
                   $curr = strtotime($dates[$i]);
                   while ($temp < $curr)
                   {
                          if ($this->isWeekEnd($temp) || in_array($temp, $holiday) )
                          {
                              $temp += 86400;
                              continue;
                          }
                          if ($temp != $prev)
                          {
                              $currentCycle++;
                              if ($currentCycle == $cycle_no) $currentCycle = 0;
                          }
                          $temp += 86400;
                   }
                   if ($temp == $curr)
                   {
                          if ($this->isWeekEnd($temp) || in_array($temp, $holiday) )
                          {
                              $result [$i] = -1;      # No cycle day
                          }
                          else
                          {
                              if ($this->DateStart > $curr)
                              {
                                  $result[$i] = -1;
                              }
                              else if ($notFullWeek)
                              {
                                   $result[$i] = $this->getCycleDayFromDate(date('Y-m-d',$curr));
                              }
                              else
                              {
                                  $currentCycle++;
                                  if ($currentCycle == $cycle_no) $currentCycle = 0;
                                  $result[$i] = $currentCycle;
                              }
                          }
                   }
                   $prev = $curr;
              }
              return $result;


     }

     # $ts is an array of timestamps
     function getCycleDaysFromTimestamps($ts)
     {
              $start = 0;
              $size = sizeof($ts);
              if ($size==0) return;
              $result = array();
              sort($ts);

              # Skip the dates before cycle start date
              while ($ts[$start] < $this->DateStart && $start < $size)
              {
                     $result[$start] = $ts[$start];
                     $start++;
              }
              if ($start >= $size-1)
              {
                  return $result;
              }

              # Get Holiday events
              $start_stamp = $ts[$start];
              $end_stamp = $ts[$size-1];

              $special_conds = $this->isWeekEndSQLConds();

              $conds = "RecordStatus =1
                        AND (UNIX_TIMESTAMP(EventDate) BETWEEN ".$start_stamp." AND ".$end_stamp.")
                        AND (
                             (isSkipCycle IS NULL AND RecordType = 2)
                             OR
                             (isSkipCycle = 1)
                            )
                        AND $special_conds
                        ";
              $sql = "SELECT distinct UNIX_TIMESTAMP(EventDate) FROM INTRANET_EVENT WHERE $conds";
              $a = new libdb();
              $holiday = $a->returnVector($sql);

              # Get day of first valid date
              $this->DateToday = $start_stamp;
              $currentCycle = $this->getCycleNumber($this->CycleNum);
              $cycle_no = $this->CycleNum;

              if ($this->isWeekEnd($start_stamp) || in_array($start_stamp, $holiday) )
              {
                  $result[$start] = -1;
                  //$currentCycle--;
              }
              else
              {
                  $result[$start] = $currentCycle;
              }

              $temp = $start_stamp;
              $prev = $start_stamp;

              for ($i=$start+1; $i<$size; $i++)
              {
                   $curr = $ts[$i];
                   while ($temp < $curr)
                   {
                          if ($this->isWeekEnd($temp) || in_array($temp, $holiday) )
                          {
                              $temp += 86400;
                              continue;
                          }
                          if ($temp != $prev)
                          {
                              $currentCycle++;
                              if ($currentCycle == $cycle_no) $currentCycle = 0;
                          }
                          $temp += 86400;
                   }
                   if ($temp == $curr)
                   {
                          if ($this->isWeekEnd($temp) || in_array($temp, $holiday) )
                          {
                              $result [$i] = -1;      # No cycle day
                          }
                          else
                          {
                              if ($this->DateStart > $curr)
                              {
                                  $result[$i] = -1;
                              }
                              else if ($notFullWeek)
                              {
                                   $result[$i] = $this->getCycleDayFromDate(date('Y-m-d',$curr));
                              }
                              else
                              {
                                  $currentCycle++;
                                  if ($currentCycle == $cycle_no) $currentCycle = 0;
                                  $result[$i] = $currentCycle;
                              }
                          }
                   }
                   $prev = $curr;
              }
              return $result;
     }
     function getCycleNameArray()
     {
              switch ($this->CycleID)
              {
                      case 1:
                      case 2:
                      case 3:
                      case 10:
                           $cycle_type = 1; break;
                      case 4:
                      case 5:
                      case 6:
                      case 11:
                           $cycle_type = 2; break;
                      case 7:
                      case 8:
                      case 9:
                      case 12:
                           $cycle_type = 3; break;
                      default: return array();
              }

          global ${"i_DayType".$cycle_type};
          $i_DayType = ${"i_DayType".$cycle_type};
          return $i_DayType;
     }

     # ------------------------------------------------------------------------------------

}
?>