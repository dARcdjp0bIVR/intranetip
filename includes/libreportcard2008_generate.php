<?php
# Editing by Bill

/*******************************************************
 *  20200727 Bill [2020-0724-1423-23073]
 *      - modified GENERATE_WHOLE_YEAR_REPORTCARD(), CALCULATE_ASSESSMENT_COLUMN_MARK(), convert excluded full marks to weighted if have excluded column weight
 *  20200720 Bill [2020-0717-1352-51206]
 *      - modified GENERATE_TERM_REPORTCARD(), GENERATE_WHOLE_YEAR_REPORTCARD(), for problems in academic year 2019-2020 only
 *          all comp subjects > weight = 0      ($eRCTemplateSetting['Report']['ReportGeneration']['ParentAlwaysCalculatedByCmpAtYear2019'])
 *  20200710 Bill [2020-0708-1350-06164]
 *      - modified GENERATE_WHOLE_YEAR_REPORTCARD(), to fill N.A. to last term report column
 *          ($eRCTemplateSetting['Report']['ReportGeneration']['SpecialYearReport_FillNAToLastColumn'] && $eRCTemplateSetting['Report']['ReportGeneration']['SpecialYearReport_ReportIdentifier'])
 *      - modified GENERATE_WHOLE_YEAR_REPORTCARD(), to disable copy special case logic
 *          ($eRCTemplateSetting['DisableCopySpecialCaseToOverallMarkLogic'])
 *      - modified UPDATE_ORDER(), to calculate subject group ranking for yearly report
 *          ($eRCTemplateSetting['Report']['ReportGeneration']['YearReportHasSubjectGroupOrdering'])
 *  20191230 Philips [2019-0704-1858-08289]
 *  	- modified  UPDATE_ORDER(), to customize Subject Stream order
 *  20190704 Bill [2019-0704-0924-14207]
 *      - modified GENERATE_WHOLE_YEAR_REPORTCARD(), to fix cannot calculate parent subject overall result
 *  20190620 Bill
 *      - modified UPDATE_ORDER(), Get_Number_Of_Counted_Mark_Student()
 *      - added Get_List_Of_Counted_Mark_Student()      ($eRCTemplateSetting['NumberOfStudentCalculationIncludedSpecialCase'] && $eRCTemplateSetting['NumberOfStudentCalculationExcludedSpecialCase'])
 *  20190221 Bill [2019-0220-1154-15207]
 *      - modified GENERATE_TERM_REPORTCARD(), set overall mark to special case if no valid grade   ($eRCTemplateSetting['Report']['ReportGeneration']['HandlingForSubjectOverallGradeInputSpecialCase'])
 *  20190215 Bill [2019-0123-1731-14066]
 *      - modified UPDATE_ORDER(), to support cust order logic - passed all subjects > num of failed subject weight (unit)   ($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst_UsingSHCC_SubjectUnit'])
 *  20190130 Bill [2019-0123-1731-14066]
 *      - modified UPDATE_ORDER(), to support cust logic
 *  20180720 Bill
 *      - modified UPDATE_ORDER(), to exclude student ranking for grand result only ($eRCTemplateSetting['ExcludeStudentRankingForGrandResultOnly'])
 *  20180703 Bill [2018-0703-0940-05164]
 *      - modified GENERATE_WHOLE_YEAR_REPORTCARD(), get 2nd column of term report for $eRCTemplateSetting['Report']['ReportGeneration']['ConsolidateAssessmentColumnType']
 *  20180420 Bill [2018-0418-1704-29206]
 *      - modified CALCULATE_COLUMN_MARK(), handle $columnNumerator and $thisNumerator calculation to prevent division by zero problem
 * 	20180213 Bill [2017-1114-1352-50206]
 * 		- modified UPDATE_ORDER(), support Form-based Ordering Method ($eRCTemplateSetting['OrderingPositionByArr'])
 * 	20180212 Bill [2017-0907-0952-53240]
 * 		- added CALCULATE_ASSESSMENT_COLUMN_MARK(), GET_EXIST_TERM_RESULT_SCORE(), support Subject Assessment Column Result Score
 * 		- modified GENERATE_TERM_REPORTCARD(), GENERATE_WHOLE_YEAR_REPORTCARD()
 * 	20180123 Bill [2018-0104-0942-28236] 
 * 		- modified GENERATE_TERM_REPORTCARD(), handle special cases in report generation
 * 	20171108 Bill [2017-1107-0932-40235]
 * 		- modified CALCULATE_COLUMN_MARK(), to apply Subject Weight in Step 3 (if set to 0)		($eRCTemplateSetting['Report']['ReportGeneration']['ApplyVerticalZeroWeightColumnGrandCalculation'])
 * 	20170807 Bill [2017-0321-1644-34240]
 * 		- modiied GENERATE_TERM_REPORTCARD(), GENERATE_WHOLE_YEAR_REPORTCARD(), to support special case handling for specific report column
 * 					($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase_ColumnOrderArr'])
 *  20170613 Ivan [2017-0630-0914-50236]
 * 		- commented customization done in [2016-0630-0911-42236]
 * 	20170613 Bill [2016-1130-1238-56240]
 * 		- modified UPDATE_ORDER(), to support report column and overall column exempt handling for parent subject ($eRCTemplateSetting['ExcludeFromOrder_ExamAbsOrExempt'])
 *  20170518 Bill [2017-0508-1128-10164]
 * 		- modified GENERATE_TERM_REPORTCARD(), GENERATE_WHOLE_YEAR_REPORTCARD(), to set grade to D for students that pass the markup exam
 * 	20170328 Bill [2016-1130-1238-56240]
 * 		- modified UPDATE_ORDER(), Get_Student_Ranking_Array(), to support all passed ranking first customized logic 
 * 	20161116 Bill [2016-1116-0911-58236]
 * 		- modified Get_Student_Ranking_Array(), Get_Number_Of_Counted_Mark_Student(), fixed return incorrect student ranking if using Weighted SD as Order Position Method
 *  20160630 Bill [2016-0624-1118-33236]
 * 		- modified GENERATE_WHOLE_YEAR_REPORTCARD(), fixed incorrect FormNoOfStudent in REPORT_RESULT when direct input overall score
 *  20160628 Bill [2015-1104-1130-08164]
 * 		- modified GENERATE_WHOLE_YEAR_REPORTCARD(), to set grade to D for students that pass the markup exam
 * 	20160316 Bill [2016-0217-0905-03207]
 * 		- modified UPDATE_SD_SCORE(), Get_Number_Of_Counted_Mark_Student(), to handle the ordering of SD scores with "+" ($eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent'])
 * 	20160219 Bill [2015-0421-1144-05164]
 * 		- modified GENERATE_TERM_REPORTCARD(), GENERATE_WHOLE_YEAR_REPORTCARD(), UPDATE_ORDER(), Get_Student_Ranking_Array(), Get_Number_Of_Counted_Mark_Student(), to support SD ordering
 * 	20150722 Bill [2015-0413-1359-48164]
 * 		- modified GENERATE_TERM_REPORTCARD() to support term sequence of BIBA progress report
 * 	20120207 Ivan [2012-0207-1111-39071]
 * 		- modified CALCULATE_COLUMN_MARK() to fix the "abs" cannot be excluded in the grand marks problem 
 * 	20111128 Marcus
 * 		- modified Get_Number_Of_Counted_Mark_Student, if $eRCTemplateSetting['SpecialCaseThatExcludedFromNoOfStudent'] isset, exclude special case of this array only 
 * 	20100111 Marcus
 * 		- modified UPDATE_ORDER, Add flag to order position by SD instead of rawmark 
 * *****************************************************/

class libreportcard2008_generate extends libreportcard {

	function libreportcard2008_generate(){
		$this->libreportcard();
	}
	
	function GENERATE_TERM_REPORTCARD($ReportID, $ClassLevelID) {
		global $PATH_WRT_ROOT, $ReportCardCustomSchoolName, $eRCTemplateSetting;
		$success = array();
		
		// get report template basic info: Semester, PercentageOnColumnWeight
		$reportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$classLevelId = $reportBasicInfo['ClassLevelID'];
		$semester = $reportBasicInfo['Semester'];
		$isMainReport = $reportBasicInfo['isMainReport'];
		$termReportType = ($isMainReport)? 'Main' : 'Extra';
		$formNumber = $this->GET_FORM_NUMBER($classLevelId, true);
		// [2015-0413-1359-48164]
		$semester_num = $this->Get_Semester_Seq_Number($semester);
		if ($eRCTemplateSetting['Settings']['AutoSelectReportType']) {
			$formNumber = $this->GET_FORM_NUMBER($classLevelId);
		}
		$reportColumnData = $this->returnReportTemplateColumnData($ReportID);
		
		// handle auto copy
		if (isset($eRCTemplateSetting['Report']['ReportGeneration']['AutoCopyMarkAry']['Term'][$termReportType][$formNumber][$semester_num])) {
			foreach ((array)$eRCTemplateSetting['Report']['ReportGeneration']['AutoCopyMarkAry']['Term'][$termReportType][$formNumber][$semester_num] as $_toReportColumnNum => $_fromReportInfoAry) {
				
				### Build from report info
				$_fromReport = $_fromReportInfoAry['fromReport'];
				$_fromReportType = $_fromReportInfoAry['fromReportType'];
				$_fromTermNum = $_fromReportInfoAry['fromTermNum'];
				$_fromReportSequence = $_fromReportInfoAry['fromReportSequence'];
				$_fromReportColumnNum = $_fromReportInfoAry['fromReportColumnNum'];
				$_fromIsMainReport = ($_fromReportType=='Main')? 1 : 0;
				
				$_fromTermId = '';
				if ($_fromReport == 'WholeYear') {
					$_fromTermId = 'F';
				}
				else {
					$_termAry = array_keys($this->returnSemesters());
					$_fromTermId = $_termAry[$_fromTermNum-1];
				}
				
				$other_field = "";
				// extra condition for BIBA MS Progress Report as each semester with 2 progress reports
				if($eRCTemplateSetting['Settings']['AutoSelectReportType'] && !$_fromIsMainReport && $formNumber > 5){
					$other_field = " (ReportSequence = '$_fromReportSequence'".(($_fromReportSequence==1 || $_fromReportSequence==3)? " OR ReportSequence IS NULL)" : ")");
				}
	
				// get report info
//				$_fromReportInfoAry = $this->returnReportTemplateBasicInfo('', '', $classLevelId, $_fromTermId, $_fromIsMainReport);
				$_fromReportInfoAry = $this->returnReportTemplateBasicInfo("", $other_field, $ClassLevelID, $_fromTermId, $_fromIsMainReport);
				$_fromReportId = $_fromReportInfoAry['ReportID'];
				$_fromReportColumnAry = $this->returnReportTemplateColumnData($_fromReportId);
				$_fromReportColumnId = $_fromReportColumnAry[$_fromReportColumnNum-1]['ReportColumnID'];
				
				### Build to report info
				$_toReportColumnId = $reportColumnData[$_toReportColumnNum-1]['ReportColumnID'];
				
				### Copy
				$success['copyMarksheet_reportNum='.($_fromReportSequence-1).'copyMarksheet_reportColumnNum='.$formNumber] = $this->Copy_Marksheet($_fromReportId, array($_fromReportColumnId), $ReportID, array($_toReportColumnId));
			}
		}
		
		// delete the consolidated data (if exist)
		$success["DELETE_REPORT_RESULT_SCORE"] = $this->DELETE_REPORT_RESULT_SCORE($ReportID);
		$success["DELETE_REPORT_RESULT"] = $this->DELETE_REPORT_RESULT($ReportID);
		$success["DELETE_NEW_FULLMARK"] = $this->DELETE_NEW_FULLMARK($ReportID);
		
		// Add all empty marks / no mark input yet to N.A.
		$success["MS_ALL_EMPTY_TO_NA"] = $this->FILL_ALL_EMPTY_SCORE_WITH_NA($ReportID, $UpdateLastModified=0);
		
		################### Preload settings & data ###################
		// load Mark Storage & Display settings: SubjectScore (0,1,2), SubjectTotal (0,1,2), GrandAverage (0,1,2)
		$storageDisplaySettings = $this->LOAD_SETTING("Storage&Display");
		
		// load Calculation settings: OrderTerm (1,2), OrderFullYear (1,2), UseWeightedMark (0,1)
		$calculationSettings = $this->LOAD_SETTING("Calculation");
		$isAbjustFullMark = $calculationSettings["AdjustToFullMarkFirst"];
		$calculationOrder = $calculationSettings["OrderTerm"];
		
		// load Absent & Exempt settings: Exempt (ExWeight, ExFullMark, Zero), Absent (ExWeight, ExFullMark, Zero)
		$absentExemptSettings = $this->LOAD_SETTING("Absent&Exempt");
		
		// Check for Combination of Special Case of Input Mark / Term Mark
		$SpecialCaseArrH = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		foreach((array)$SpecialCaseArrH as $key){
			$SpecialCaseCountArrH[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
		}
		$SpecialCaseArrPF = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
		foreach((array)$SpecialCaseArrPF as $key){
			$SpecialCaseCountArrPF[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
		}

		// [2020-0717-1352-51206]
		$parentSubjAlwaysCalculatedByCmp = $this->schoolYear == '2019' && $eRCTemplateSetting['Report']['ReportGeneration']['ParentAlwaysCalculatedByCmpAtYear2019'];

		// $reportColumnData[$i] + ["ReportColumnID"], ["ColumnTitle"], ["DefaultWeight"], 
		// ["SemesterNum"], ["IsDetails"], ["ShowPosition"], ["PositionRange"], ["DisplayOrder"]
		if (sizeof($reportColumnData) > 0) {
			for($i=0; $i<sizeof($reportColumnData); $i++) {
				$reportColumnIDList[] = $reportColumnData[$i]["ReportColumnID"];
				$reportInvolveTerms[] = $reportColumnData[$i]["SemesterNum"];
				$reportColumnTermMap[$reportColumnData[$i]["ReportColumnID"]] = $reportColumnData[$i]["SemesterNum"];
				$reportTermColumnMap[$reportColumnData[$i]["SemesterNum"]] = $reportColumnData[$i]["ReportColumnID"];
			}
		}
		
		// $classArr[$i] + ["ClassID"], ["ClassName"]
		$classArr = $this->GET_CLASSES_BY_FORM($ClassLevelID);
		for($i=0; $i<sizeof($classArr); $i++) {
			// $studentArr[#ClassID][$i] + ["UserID"], ["WebSAMSRegNo"],...
			$studentArr[$classArr[$i]["ClassID"]] = $this->GET_STUDENT_BY_CLASS($classArr[$i]["ClassID"]);
		}

		// $subjectList[#SubjectID] + 	['schemeID'], ['displayOrder'], ['scaleInput'], ['scaleDisplay'],
		//								['is_cmpSubject'], ['subjectName'], ['parentSubjectID']
		$subjectList = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,0,$ReportID);
		
		$SubjectCmpInfoAssoArr = array();
		foreach((array)$subjectList as $subjectID => $subjectInfo) {
			$SubjectCmpInfoAssoArr[$subjectID] = $this->CHECK_HAVE_CMP_SUBJECT($subjectID, $ClassLevelID, $ReportID);
		}
			
		// Weight of each of the columns (Assessment)
		$SubjectIDArr = array_keys((array)$subjectList);
		$reportColumnWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, '', $SubjectIDArr);
		$numOfWeightInfo = count($reportColumnWeightInfo);
		$reportColumnWeightAssoArr = array();
		$totalReportColumnWeightAssoArr = array();
		
		for($x=0; $x<$numOfWeightInfo; $x++){
			$thisReportColumnID = $reportColumnWeightInfo[$x]['ReportColumnID'];
			$thisSubjectID = $reportColumnWeightInfo[$x]['SubjectID'];
			$thisWeight = $reportColumnWeightInfo[$x]['Weight'];
			
			if ($thisReportColumnID != "" && $thisWeight != "") {
					
				$reportColumnWeightAssoArr[$thisSubjectID][$thisReportColumnID] = $thisWeight;
				$totalReportColumnWeightAssoArr[$thisSubjectID] += $thisWeight;
			}
		}
			
		################################################################
		
		// Loop 1: Class
		for($i=0; $i<sizeof($classArr); $i++)
		{
			// construct the $studentIDArr
			$classID = $classArr[$i]["ClassID"];
			
			$studentIDArr = array();
			$numOfClassStudent = count($studentArr[$classID]);
			for($j=0; $j<$numOfClassStudent; $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			
			// SKIP this class if no students
			if ($numOfClassStudent == 0) continue;
			
			// Horizontal -> Vertical (subject overall -> grand total)
			if ($calculationOrder == 1)
			{
				// Loop 2: Subject
				$studentSubjectOverallList = array();
				$mixCalculateByCmpArr = array();
				$studentSubjectColumnMark = array();
				$studentSubjectColumnMarkRaw = array();
				
				foreach((array)$subjectList as $subjectID => $subjectInfo)
				{
					// $schemeInfo[0] is main info, [1] is ranges info
					$schemeInfo = $this->GET_GRADING_SCHEME_INFO($subjectInfo["schemeID"]);
					$schemeType = $schemeInfo[0]["SchemeType"];
					$passWording = $schemeInfo[0]["Pass"];
					$failWording = $schemeInfo[0]["Fail"];
					$subjectFullMark = $schemeInfo[0]["FullMark"];
					
					$scaleInput = $subjectInfo["scaleInput"];
					$scaleDisplay = $subjectInfo["scaleDisplay"];
					
					// get the grading scheme range info
					//$schemeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($subjectInfo["schemeID"]);
					$schemeRangeInfo = $schemeInfo[1];
					for($m=0; $m<sizeof($schemeRangeInfo); $m++) {
						$schemeRangeIDGradeMap[$schemeRangeInfo[$m]["GradingSchemeRangeID"]] = $schemeRangeInfo[$m]["Grade"];
					}
					
					// check if it is a parent subject, if yes find info of its components subjects
					//$CmpInfoArr = $this->CHECK_HAVE_CMP_SUBJECT($subjectID, $ClassLevelID, $ReportID);
					$CmpInfoArr = $SubjectCmpInfoAssoArr[$subjectID];
					$CmpSubjectArr = $CmpInfoArr[0];
					$CmpSubjectIDArr = $CmpInfoArr[1];
					$isCmpSubject = $CmpInfoArr[2];
					$isCalculateByCmpSub = $CmpInfoArr[3];
					
					## updated on 5 Feb by Ivan - support manual input Parent Subject Marks
					## �~�ؤ��� [CRM Ref No.: 2009-0204-1056]
					## Do not calculate the parent subjetc mark from component subjects if the parent mark is input manually
					if ($eRCTemplateSetting['ManualInputParentSubjectMark'])
					{
						$isCalculateByCmpSub = 0;
					}
					
					// Weight of each of the columns (Assessment) - moved to top to save queries
//					$totalReportColumnWeight = 0;
//					$reportColumnWeight = array();
//					$reportColumnWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, "SubjectID = ".$subjectID);
//					for($x=0 ; $x<sizeof($reportColumnWeightInfo) ; $x++){
//						if ($reportColumnWeightInfo[$x]['ReportColumnID'] != "" && $reportColumnWeightInfo[$x]['Weight'] != "") {
//							$reportColumnWeight[$reportColumnWeightInfo[$x]['ReportColumnID']] = $reportColumnWeightInfo[$x]['Weight'];
//							$totalReportColumnWeight += $reportColumnWeightInfo[$x]['Weight'];
//						}
//					}
					$reportColumnWeight = $reportColumnWeightAssoArr[$subjectID];
					$totalReportColumnWeight = $totalReportColumnWeightAssoArr[$subjectID];
					
					// [2015-1104-1130-08164] Get Make-up Exam Score
					$MarkupResultAry = array();
					if($eRCTemplateSetting['ReportGeneration']['MarkupExamGrade'])
					{
						if(!empty($studentIDArr) && is_array($studentIDArr))
						{
							// Graduate Exam
							$SchoolType	= $this->GET_SCHOOL_LEVEL($ClassLevelID);
							$isSecondaryGraduate = $SchoolType=="S" && $formNumber==6;
							if($isSecondaryGraduate && $semester_num==3)
							{
								// Get Year Report
								$thisYearReportInfo = $this->returnReportTemplateBasicInfo("", $others="", $ClassLevelID, "F", $isMainReport=1);
								$thisYearReportID = $thisYearReportInfo["ReportID"];
								if($thisYearReportID)
								{
									$MarkupResultAry = $this->GET_EXTRA_SUBJECT_INFO($studentIDArr, $subjectID, $thisYearReportID);
								}
							}
						}
					}
					
					# Calculate all marks first for calculating weighted mark below (not very efficient)
					# $AllColumnMarkArr[$studentID][$reportColumnID] = #Mark
					$AllColumnMarkArr = array();
					$isCalculateByCmpSubOriginal = $isCalculateByCmpSub;
					$isAllColumnWeightZero[$subjectID] = true;
					for($n=0; $n<sizeof($reportColumnData); $n++)
					{
						$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
						
						## update on 28 Nov 2008 by Ivan
						## if the weight of all subject components are zero, use the term overall marks input by user instead of calculating from subject components
						if (!isset($isAllCmpZeroWeightAssoArr[$subjectID])) {
							 $isAllCmpZeroWeightAssoArr[$subjectID] = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $subjectID);
                        }
						$isAllCmpZeroWeightArr = $isAllCmpZeroWeightAssoArr[$subjectID];

                        // [2020-0717-1352-51206]
                        // if ($isAllCmpZeroWeightArr[$reportColumnID]) {
						if ($isAllCmpZeroWeightArr[$reportColumnID] && !$parentSubjAlwaysCalculatedByCmp) {
							$isCalculateByCmpSub = 0;
                        } else {
							$isCalculateByCmpSub = $isCalculateByCmpSubOriginal;
                        }
							
						## if all report column weights zero => use overall score (which is input by user directly)
						if ($this->IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($ReportID, $subjectID, $reportColumnID) != 1) {
							$isAllColumnWeightZero[$subjectID] = false;
                        }
						
						// get raw marks of all students in a class of this assessment
						// $marksheetScore[#studentID] + ['MarksheetScoreID'], ['SchemeID'], ['MarkType'], ['MarkRaw'], ['MarkNonNum'], ['LastModifiedUserID'] 
						if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub)) {
							$marksheetScore = $this->GET_MARKSHEET_SCORE($studentIDArr, $subjectID, $reportColumnID, $ConvertToNaIfZeroWeight=1, $reportColumnWeight[$reportColumnID]);
                        } else {
							$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID);
                        }

						foreach((array)$marksheetScore as $studentID => $scoreDetailInfo)
						{
							$scoreInfo = array();
							if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub)) {
								$scoreInfo = $scoreDetailInfo;
							}
							else {
								// skip consolidate this student if the mark is NULL
								if ($scoreDetailInfo == "")	continue;
								$scoreInfo = $this->CREATE_SCORE_INFO_ARRAY($scoreDetailInfo);
							}
							
							$AllColumnMarkArr[$studentID][$reportColumnID] = $scoreInfo['MarkNonNum'];
							if($scoreInfo['MarkType'] == "M" && $scoreInfo["MarkRaw"] != "-1") {
								$AllColumnMarkArr[$studentID][$reportColumnID] = $scoreInfo['MarkRaw'];	
							}
						}
						
						// compute the difference of available student mark and all student, assign "Exempted" to them
						$noMarkStudentID = array_diff($studentIDArr, array_keys($marksheetScore));
						if (sizeof($noMarkStudentID) > 0)
						{
							for($g=0; $g<sizeof($noMarkStudentID); $g++) {
								$AllColumnMarkArr[$noMarkStudentID[$g]][$reportColumnID] = "/";
							}
						}
					}
					
					// Loop 3: Report Column (Assessment)
					$calculationMark = array();
					for($n=0; $n<sizeof($reportColumnData); $n++)
					{
						$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
						
						if (!isset($isAllCmpZeroWeightAssoArr[$subjectID])) {
							 $isAllCmpZeroWeightAssoArr[$subjectID] = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $subjectID);
                        }
						$isAllCmpZeroWeightArr = $isAllCmpZeroWeightAssoArr[$subjectID];

                        // [2020-0717-1352-51206]
                        // if ($isAllCmpZeroWeightArr[$reportColumnID]) {
						if ($isAllCmpZeroWeightArr[$reportColumnID] && !$parentSubjAlwaysCalculatedByCmp) {
							$isCalculateByCmpSub = 0;
                        } else {
							$isCalculateByCmpSub = $isCalculateByCmpSubOriginal;
                        }

						// get raw marks of all students in a class of this assessment
						if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub))
						{
							$marksheetScore = $this->GET_MARKSHEET_SCORE($studentIDArr, $subjectID, $reportColumnID, $ConvertToNaIfZeroWeight=1, $reportColumnWeight[$reportColumnID]);
						}
						else 
						{
							$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID, "Term", 0);
						}
						
						// For term report, column is Assessment, therefore semester is NULL
						$semester = "";
						$conAssessmentMarkValues = array();
						
						// Loop 4: Student Score
						foreach((array)$marksheetScore as $studentID => $scoreDetailInfo)
						{
							$consolidateMark = "";
							$rawMark = "";
							$scoreInfo = array();
							if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub)) {
								$scoreInfo = $scoreDetailInfo;
							}
							else {
								// skip consolidate this student if the mark is NULL
								if ($scoreDetailInfo != 0 && $scoreDetailInfo == "") continue;
								$scoreInfo = $this->CREATE_SCORE_INFO_ARRAY($scoreDetailInfo);
							}
							
							if ($scoreInfo["MarkType"] == "M" || $scoreInfo["MarkType"] == "G")
							{
								if ($scaleInput == "M" && $scaleDisplay == "M")
								{
									$rawMark = $scoreInfo["MarkRaw"];
									
									if ($eRCTemplateSetting['DoNotRoundRawMarkInHorizontalCalculation'] == true)
										$consolidateMark = $scoreInfo["MarkRaw"];
									else
										$consolidateMark = $this->ROUND_MARK($scoreInfo["MarkRaw"], "SubjectScore");
										
									// check if the consolidation mark need to be converted to weight mark
									if ($calculationSettings["UseWeightedMark"] == "1")
									{
										if ($isCmpSubject) {
											$consolidateMark = $this->CONVERT_CMP_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $consolidateMark, 1, $AllColumnMarkArr[$studentID]);
										}
										else {
											$consolidateMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $consolidateMark);
										}
									}
									
									if ($eRCTemplateSetting['DoNotRoundRawMarkInHorizontalCalculation'] == true)
										$consolidateMark = $consolidateMark;
									else
										$consolidateMark = $this->ROUND_MARK($consolidateMark, "SubjectScore");
									
									// Horizontal->Vertical: need to store weighted mark to calculate subject overall
									if ($eRCTemplateSetting['DoNotRoundRawMarkInHorizontalCalculation'] == true)
										$calculationMark[$studentID][$reportColumnID] = $scoreInfo["MarkRaw"];
									else
										$calculationMark[$studentID][$reportColumnID] = $this->ROUND_MARK($scoreInfo["MarkRaw"], "SubjectScore");
									
									if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == false)
									{
										if ($isCmpSubject) {
											$calculationMark[$studentID][$reportColumnID] = $this->CONVERT_CMP_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $consolidateMark, 1, $AllColumnMarkArr[$studentID]);
										}
										else {
											$calculationMark[$studentID][$reportColumnID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $calculationMark[$studentID][$reportColumnID], 1, $AllColumnMarkArr[$studentID]);
										}
									}
								}
								else if ($scaleInput == "M" && $scaleDisplay == "G")
								{
									$rawMark = $scoreInfo["MarkRaw"];
									
									if ($eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade'])
									{
										$rawMarkToConvert = $this->ROUND_MARK($rawMark, "SubjectScore");
									}
									else
									{
										$rawMarkToConvert = $rawMark;
									}
									
									// convert to grade according to ranges
									$consolidateMark = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $rawMarkToConvert);
									
									if ($eRCTemplateSetting['DoNotRoundRawMarkInHorizontalCalculation'] == true)
										$calculationMark[$studentID][$reportColumnID] = $scoreInfo["MarkRaw"];
									else
										$calculationMark[$studentID][$reportColumnID] = $this->ROUND_MARK($scoreInfo["MarkRaw"], "SubjectScore");
									
									if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == false) {
										$calculationMark[$studentID][$reportColumnID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $calculationMark[$studentID][$reportColumnID], 1, $AllColumnMarkArr[$studentID], $debug);
									}
								}
								else if ($scaleInput == "G" && $scaleDisplay == "G")
								{
									$consolidateMark = $schemeRangeIDGradeMap[$scoreInfo["MarkNonNum"]];
									## updated on 18 Dec 2008 by Ivan
									## get the mark for GPA calculation
									$calculationMark[$studentID][$reportColumnID] = $consolidateMark;
								}
								
								// round off the weighted mark
								if ($scaleDisplay == "M" && is_numeric($consolidateMark))
								{
									if ($eRCTemplateSetting['DoNotRoundRawMarkInHorizontalCalculation'] == true)
										$consolidateMark = $consolidateMark;
									else
										$consolidateMark = $this->ROUND_MARK($consolidateMark, "SubjectScore");
								}
								
								// consolidate the mark into DB (RC_REPORT_RESULT_SCORE)
								$mark = "";
								$grade = "";
								
								// [2017-0907-0952-53240]
								//if ($scaleInput == "M" && $scaleDisplay == "G" && is_numeric($consolidateMark) && !$this->Check_If_Grade_Is_SpecialCase($consolidateMark)) {
							    if ($scaleInput == "M" && $scaleDisplay == "G" && !$this->Check_If_Grade_Is_SpecialCase($consolidateMark) && (is_numeric($consolidateMark) || $eRCTemplateSetting['Report']['ReportGeneration']['AlwaysUseMarkForVerticalCalculation']))
							    {
									$grade = $consolidateMark;
									$mark = $this->ROUND_MARK($rawMark, "SubjectScore");
									
									$studentSubjectColumnMark[$reportColumnID][$studentID][$subjectID] = $mark;
									$studentSubjectColumnMarkRaw[$reportColumnID][$studentID][$subjectID] = $rawMark;
								}
								else if ($scaleDisplay == "M" && is_numeric($consolidateMark))
								{
									$mark = $consolidateMark;
									
									// For calculate Column Grand Marks (Vertical Calculation)
									//if ($eRCTemplateSetting['DoNotRoundRawMarkInHorizontalCalculation'])
									//	$studentSubjectColumnMark[$reportColumnID][$studentID][$subjectID] = $rawMark;
									//else
									$studentSubjectColumnMark[$reportColumnID][$studentID][$subjectID] = $consolidateMark;
									$studentSubjectColumnMarkRaw[$reportColumnID][$studentID][$subjectID] = $rawMark;
								}
								else
								{
									$grade = $consolidateMark;
									$mark = $this->ROUND_MARK($rawMark, "SubjectScore");
									
									// For calculate Column Grand Marks (Vertical Calculation)
									$studentSubjectColumnMark[$reportColumnID][$studentID][$subjectID] = $consolidateMark;
									$studentSubjectColumnMarkRaw[$reportColumnID][$studentID][$subjectID] = $rawMark;
									if($eRCTemplateSetting['ReportGeneration']['HV_Calcaulation_ColumnGrand_Logic'])
									{
										$studentSubjectColumnMark[$reportColumnID][$studentID][$subjectID] = $mark;
									}
								}
								
								// [2015-1104-1130-08164] Set Grade to D for students that pass the Make-up Exam
								if($eRCTemplateSetting['ReportGeneration']['MarkupExamGrade'] && ($grade=="F" || $grade=="+"))
								{
									if(!empty($MarkupResultAry) && $isSecondaryGraduate && $semester_num==3 && $n==1)
									{
										$MarkupExamScore = $MarkupResultAry[$studentID]["Info"];
										if(!empty($MarkupExamScore) && $MarkupExamScore >= 60)
										{
											$grade = "D";
										}
									}
								}
								$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '$mark', '$grade', '$rawMark', '', '$semester', '', '', NOW(), NOW())";
							}
							else if ($scoreInfo["MarkType"] == "PF")
							{
								$consolidateMark = ($scoreInfo["MarkNonNum"] == "P") ? $passWording : $failWording;
								$PassFailMark = $consolidateMark;
								$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$PassFailMark', '', '', '$semester', '', '', NOW(), NOW())";
								
								// By default, setting Pass/Fail based subject to Exempted
								$calculationMark[$studentID][$reportColumnID] = "/";
								$studentSubjectColumnMark[$reportColumnID][$studentID][$subjectID] = '/';
								$studentSubjectColumnMarkRaw[$reportColumnID][$studentID][$subjectID] = '/';
							}
							else
							{
								$specialCase = $scoreInfo["MarkNonNum"];
								$calculationMark[$studentID][$reportColumnID] = $specialCase;
								
								$studentSubjectColumnMark[$reportColumnID][$studentID][$subjectID] = $specialCase;
								$studentSubjectColumnMarkRaw[$reportColumnID][$studentID][$subjectID] = $specialCase;
								
								$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$specialCase', '', '', '$semester', '', '', NOW(), NOW())";
								
								$specialCaseDifferentiate = array();
								// Build the $specialCaseDifferentiate Array for determining if new full mark is need to insert
								if ($schemeType == "PF") {
									$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $specialCase, $absentExemptSettings);
								}
								else {
									$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $specialCase, $absentExemptSettings);
								}
								
								if ($specialCaseDifferentiate[$specialCase]["isExFullMark"] == 1) {
									$success["INSERT_NEW_FULLMARK"] = $this->INSERT_NEW_FULLMARK(0, $ReportID, $studentID, $subjectID, $reportColumnID);
								}
							}
						} // End Loop 4: Student's Score
						
						// insert some value for newly added students, i.e. no record, too?
						
						// insert the converted mark into DB
						if (sizeof($conAssessmentMarkValues) > 0)
						{
							$conAssessmentMarkValues = implode(",", $conAssessmentMarkValues);
							$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($conAssessmentMarkValues);
						}
					} // End Loop 3: ReportColumns
					
					// consolidate existing (entered by user directly) overall mark, for the case of input & display are both Grade
					$existOverallScoreInfo = $this->GET_MARKSHEET_OVERALL_SCORE($studentIDArr, $subjectID, $ReportID);
					
					$subjectOverallValues = array();
					$consolidatedSubjectOverallStudentList = array();
					if ((sizeof($existOverallScoreInfo) > 0 && ($scaleInput != 'M' || ($scaleInput=='M' && $isAllColumnWeightZero[$subjectID] == true))))
					{
						foreach((array)$existOverallScoreInfo as $markOverallStudentID => $markOverallInfo)
						{
							$consolidatedSubjectOverallStudentList[] = $markOverallStudentID;
							if ($markOverallInfo["MarkType"] == "G") {
								$gradeToConsolidate = $schemeRangeIDGradeMap[$markOverallInfo["MarkNonNum"]];
								$subjectOverallValues[] = "('$markOverallStudentID', '$subjectID', '$ReportID', '', '', '$gradeToConsolidate', '', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
							}
							else if ($markOverallInfo["MarkType"] == "SC") {
								$subjectOverallValues[] = "('$markOverallStudentID', '$subjectID', '$ReportID', '', '', '".$markOverallInfo["MarkNonNum"]."', '', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
							}
							else if ($markOverallInfo["MarkType"] == "PF") {
								$overallPFWording = ($markOverallInfo["MarkNonNum"] == "P")? $passWording : $failWording;
								$subjectOverallValues[] = "('$markOverallStudentID', '$subjectID', '$ReportID', '', '', '".$overallPFWording."', '', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";	
							}
							else if ($markOverallInfo["MarkType"] == "M") {
								$thisScore = $markOverallInfo["MarkNonNum"];
								$thisConvertedGrade = '';
								if ($scaleInput=='M' && $scaleDisplay=='G' && is_numeric($thisScore)) {
									$thisConvertedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $thisScore);
								}
								$subjectOverallValues[] = "('$markOverallStudentID', '$subjectID', '$ReportID', '', '".$thisScore."', '".$thisConvertedGrade."', '".$thisScore."', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
								
								$studentSubjectOverallList[$markOverallStudentID][$subjectID] = $thisScore;
							}
						}
						
						if (sizeof($subjectOverallValues) > 0)
						{
							$subjectOverallValues = implode(",", $subjectOverallValues);
							$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($subjectOverallValues);
						}
					}

                    // [2020-0717-1352-51206]
					// if (in_array("1", $isAllCmpZeroWeightArr)) {
					if (in_array("1", $isAllCmpZeroWeightArr) && !$parentSubjAlwaysCalculatedByCmp)
					{
						$mixCalculateByCmp[$subjectID] = 1;
					}
					else
					{
						$mixCalculateByCmp[$subjectID] = 0;
					}
					
					#######################################
					# Added on 20080812
					# Skip calculation of subject overall for Parent Subject since it will be calculated by an alternate way
					if ($isCalculateByCmpSub && !$mixCalculateByCmp[$subjectID]) continue;
					#######################################
					
					// calculate the subject overall mark of each student
					if (sizeof($calculationMark) > 0)
					{
						$subjectOverallValues = array();
						$existSpFullMarkArr = $this->GET_NEW_FULLMARK($ReportID, '', $subjectID, '');
						$existSpFullMarkAssoArr = BuildMultiKeyAssoc($existSpFullMarkArr, array('StudentID', 'ReportColumnID'));
						unset($existSpFullMarkArr);
						
						foreach((array)$calculationMark as $markStudentID => $markList)
						{
							// skip calculation of subject overall mark since it already been entered by user directly and consolidated above
							if (in_array($markStudentID, $consolidatedSubjectOverallStudentList) && (count($CmpSubjectArr)==0 || ($mixCalculateByCmp[$subjectID]==0)) )
							{
								## updated on 18 Dec 2008 by Ivan
								## get the mark for GPA calculation
								$marksheetScore = $this->GET_MARKSHEET_OVERALL_SCORE(array($markStudentID), $subjectID, $ReportID);
								$SchemeGrade = $this->GET_GRADE_FROM_GRADING_SCHEME_RANGE($subjectInfo["schemeID"]);
								
								$thisMarkNonNum = $marksheetScore[$markStudentID]['MarkNonNum'];
								$thisOverallMark = $SchemeGrade[$thisMarkNonNum];
								
								// [2019-0220-1154-15207] set overall mark to special case if no valid grade 
								if($eRCTemplateSetting['Report']['ReportGeneration']['HandlingForSubjectOverallGradeInputSpecialCase']) {
    								if(!isset($SchemeGrade[$thisMarkNonNum]) && $thisOverallMark == '' && (in_array($thisMarkNonNum, $this->specialCasesSet1) || in_array($thisMarkNonNum, $this->specialCasesSet2))) {
    								    $thisOverallMark = $thisMarkNonNum;
    								}
								}
								
								if ($isAllColumnWeightZero[$subjectID] == false) {
									$studentSubjectOverallList[$markStudentID][$subjectID] = $thisOverallMark;
								}
								
								continue;
							}
							
							$subjectOverallMark = "";
							$subjectOverallMarkConverted = "";
							$excludeColumnWeight = 0;
							$excludeFullMark = 0;
							$specialCaseDifferentiate = array();
							$subjectColumnTotalMark = 0;
							$subjectColumnTotalWeight = 0;
							$isExcludeWeight = 0;
							
							// loop column by column and sum up the overall mark & exclude weight
							foreach((array)$markList as $markColumnID => $markColumn)
							{
								$subjectColumnTotalMark += $subjectFullMark * $reportColumnWeight[$markColumnID];
								$subjectColumnTotalWeight += $reportColumnWeight[$markColumnID];
								$thisAppliedCopySpecialCase = false;
								
								if ((is_numeric($markColumn) || strlen($markColumn)=="" && $markColumn!="") && $markColumn != -1)
								{
									// if any of the assessment marks is numeric, make sure $subjectOverallMark is initiated to zero
									$subjectOverallMark = ($subjectOverallMark == "") ? 0 : $subjectOverallMark;
									
									if ($eRCTemplateSetting['RoundSubjectMarkBeforeAddToOverallMark'])
									{
										$subjectOverallMark += $this->ROUND_MARK($markColumn, "subjectScore");
										//$subjectOverallMark += $markColumn;
									}
									else
									{
										$subjectOverallMark += $markColumn;
									}
									
									########### Added on 20080707: check whether there're any special Full Mark stored ###########
									//$existSpFullMark = $this->GET_NEW_FULLMARK($ReportID, $markStudentID, $subjectID, $markColumnID);
									$existSpFullMark = $existSpFullMarkAssoArr[$markStudentID][$markColumnID];
									if (count((array)$existSpFullMark) > 0) {
										$excludeFullMark += ($subjectFullMark-$existSpFullMark["FullMark"])*$reportColumnWeight[$markColumnID];
									}
									##################################################################
								}
								else
								{
									### If one of the column is N.A. or other special case, the overall column will be the special case also
									$thisReportColumnOrder = "";
									if(isset($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase_ColumnOrderArr'][$markColumn]['T'])) {
										$thisReportColumnOrder = $markColumnID? $this->retrieveReportTemplateColumnOrder($ReportID, $markColumnID) : "O";
									}
									if ($this->Check_Copy_Special_Case($markColumn, 'T', $markStudentID, $thisReportColumnOrder)) {
										$subjectOverallMark = $markColumn;
										$subjectOverallMarkConverted = $markColumn;
										$thisAppliedCopySpecialCase = true;
									}
									
									// Build the $specialCaseDifferentiate Array for determining the subject overall mark below
									if ($schemeType == "PF") {
										$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $markColumn, $absentExemptSettings);
									}
									else {
										$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $markColumn, $absentExemptSettings);
									}
									
									// Only concern numeric mark calculation here
									if ($specialCaseDifferentiate[$markColumn]["isExFullMark"] == 1) {
										//$success["INSERT_NEW_FULLMARK"] = $this->INSERT_NEW_FULLMARK(0, $ReportID, $markStudentID, $subjectID, $markColumnID);
										$excludeFullMark += $subjectFullMark * $reportColumnWeight[$markColumnID];
									}
									else if ($specialCaseDifferentiate[$markColumn]["isCount"] == 0) {
										// not in use since CONVERT_MARK_TO_WEIGHTED_MARK is used above
										$excludeColumnWeight += $reportColumnWeight[$markColumnID];
									}
									
									if ($specialCaseDifferentiate[$markColumn]["ExWeight"] == 1) {
										$isExcludeWeight = 1;
									}
									
									if ($thisAppliedCopySpecialCase) {
										break;
									}
								}
							}
							
							// in case of excluding full mark
							if ($excludeFullMark > 0) 
							{
								$newFullMark = $subjectFullMark - $excludeFullMark * ($subjectFullMark / $subjectColumnTotalMark);
								//$success["INSERT_NEW_FULLMARK"] = $this->INSERT_NEW_FULLMARK($newFullMark, $ReportID, $markStudentID, $subjectID);
								
								// exclude full mark
								if ($newFullMark > 0 && is_numeric($subjectOverallMark))
								{
									$subjectOverallMark = $subjectOverallMark * ($subjectFullMark / $newFullMark);
								}
							}
							// 20091009 Ivan - need to confirm the calculation with Kenneth Cheng
							// start commented: 2011-0217-1334-46128 point 6
//							else if ($excludeColumnWeight > 0)
//							{
//								//$newFullMark = $subjectFullMark * ($subjectColumnTotalWeight / $excludeColumnWeight);
//								//$success["INSERT_NEW_FULLMARK"] = $this->INSERT_NEW_FULLMARK($newFullMark, $ReportID, $markStudentID, $subjectID);
//								
//								//$subjectOverallMark = $subjectOverallMark * ($subjectColumnTotalWeight / ($subjectColumnTotalWeight - $excludeColumnWeight));
//								
//								## Recalculate the excluded weight marks of each column
//								$ReCalculateSubjectOverallMark = '';
//								foreach((array)$markList as $markColumnID => $markColumn) {
//									$thisCmpWeight = $reportColumnWeight[$markColumnID];
//									
//									if ((is_numeric($markColumn) || strlen($markColumn)=="" && $markColumn!="") && $markColumn != -1) {
//										
//										// if any of the assessment marks is numeric, make sure $subjectOverallMark is initiated to zero
//										$ReCalculateSubjectOverallMark = ($ReCalculateSubjectOverallMark == "") ? 0 : $ReCalculateSubjectOverallMark;
//										$ReCalculateSubjectOverallMark += $markColumn * ($thisCmpWeight / ($subjectColumnTotalWeight - $excludeColumnWeight));
//										
//										if ($eRCTemplateSetting['RoundSubjectMarkBeforeAddToOverallMark'])
//										{
//											$ReCalculateSubjectOverallMark = $this->ROUND_MARK($ReCalculateSubjectOverallMark, "subjectScore");
//										}
//									}
//								}
//								
//								$subjectOverallMark = $ReCalculateSubjectOverallMark;
//							}
// 							// end commented: 2011-0217-1334-46128 point 6
							
							// Subject overall mark can be calculated as number
							if (is_numeric($subjectOverallMark))
							{
								$subjectOverallMarkConverted = $subjectOverallMark;
								$subjectOverallMarkRaw = $subjectOverallMark;
								if ($scaleInput == "M" && $scaleDisplay == "G")
								{
									if ($eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade']) {
										$subjectOverallMarkToConvert = $this->ROUND_MARK($subjectOverallMark, "SubjectTotal");
									}
									else {
										$subjectOverallMarkToConvert = $subjectOverallMark;
									}
									
									$subjectOverallMarkConverted = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $subjectOverallMarkToConvert);
								}
								else
								{
									if ($totalReportColumnWeight == $excludeColumnWeight) {
										$subjectOverallMarkConverted = "/";
										$subjectOverallMarkRaw = 0;
									}
									else {
										$subjectOverallMark = $this->ROUND_MARK($subjectOverallMark, "SubjectTotal");
									}
								}
							}
							// Subject cannot be calculated as number
							else
							{
								// There's only one kind of special case in all column
								$onlySpecialCase = array_keys((array)$specialCaseDifferentiate);
								
								if (sizeof($specialCaseDifferentiate) == 1) {
									$subjectOverallMarkConverted = $onlySpecialCase[0];
								}
								else {	// How to handle different special cases mixed up together?
									// Temporary assign it to Zero 
									// $subjectOverallMark = 0; //commented by marcus, as this will cause problem for shatin methodist mid-term report.
									//$subjectOverallMark = "N.A.";
									//$subjectOverallMarkConverted = $onlySpecialCase[count($onlySpecialCase)-1]; // equals to last Special Case
									
									$thisTargetCopySpecialCase = '';
									$thisNumOfSpecialCase = count($onlySpecialCase);
									for ($i_sc=0; $i_sc<$thisNumOfSpecialCase; $i_sc++) {
										$thisSpecialCase = $onlySpecialCase[$i_sc];
										if ($this->Check_Copy_Special_Case($thisSpecialCase, 'T', $markStudentID)) {
											$thisTargetCopySpecialCase = $thisSpecialCase;
											break;
										}
									}
									
									if ($thisTargetCopySpecialCase == '')
									{
										$isTargetSpecialCase = false;
										if (isset($eRCTemplateSetting['Report']['ReportGeneration']['HandleDifferentSpecialCase'])) {
											foreach ((array)$eRCTemplateSetting['Report']['ReportGeneration']['HandleDifferentSpecialCase'] as $_checkExistSpecialCase => $_targetSpecialCase)
											{
												if (in_array($_checkExistSpecialCase, $onlySpecialCase)) {
													$subjectOverallMark = $_targetSpecialCase;
													$subjectOverallMarkConverted = $_targetSpecialCase;
													$isTargetSpecialCase = true;
													break;
												}
											}
										}
										
										// [2018-0104-0942-28236] if no matched special case, use original logic
										if(!$isTargetSpecialCase) {
											$subjectOverallMark = "N.A.";
											$subjectOverallMarkConverted = $onlySpecialCase[count($onlySpecialCase)-1]; 	// equals to last Special Case
										}
									}
									else {
										$subjectOverallMark = $thisTargetCopySpecialCase;
										$subjectOverallMarkConverted = $thisTargetCopySpecialCase;
									}
								}
								$subjectOverallMarkRaw = 0;
							}
							
							// for calculating average mark (skip component subjects)
							if (!$isCmpSubject) {
								if ($scaleInput == "M" && $scaleDisplay == "G" && !$this->Check_If_Grade_Is_SpecialCase($subjectOverallMark)) {
									$studentSubjectOverallList[$markStudentID][$subjectID] = $subjectOverallMark;
								}
								else if ($scaleInput == "M" && is_numeric($subjectOverallMark) && $subjectOverallMark!="") {
									$studentSubjectOverallList[$markStudentID][$subjectID] = $subjectOverallMark;
								}
								else {
									$studentSubjectOverallList[$markStudentID][$subjectID] = $subjectOverallMarkConverted;
								}
							}
							if (is_numeric($subjectOverallMark) && is_numeric($subjectOverallMarkConverted) && $scaleDisplay == "M") {
								$subjectOverallValues[] = "('$markStudentID', '$subjectID', '$ReportID', '', '$subjectOverallMark', '', '$subjectOverallMarkRaw', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
							}
							else {
								$subjectOverallValues[] = "('$markStudentID', '$subjectID', '$ReportID', '', '$subjectOverallMarkRaw', '$subjectOverallMarkConverted', '$subjectOverallMarkRaw', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
							}
						}
						
						// consolidate the resulting subject overall mark
						if (sizeof($subjectOverallValues) > 0) {
							$subjectOverallValues = implode(",", $subjectOverallValues);
							$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($subjectOverallValues);
						}
					}
				} // End Loop 2: Subjects
				
				### Calculate Grand Marks of each assessment (Vertical Calculation)
				if (sizeof($studentSubjectColumnMark) > 0) 
				{
					$thisTermAverageValues = array();
					foreach((array)$studentSubjectColumnMark as $thisReportColumnID => $columnMarkList) 
					{
						// calculate the term average mark (vertically across subjects) of each student
						foreach((array)$columnMarkList as $markStudentID => $markList) {
							$HVCalculation_ApplyOverallWeight = ($eRCTemplateSetting['ReportGeneration']['HV_Calcaulation_ColumnGrand_Logic'] && $thisReportColumnID > 0);
							$termAverageMark = $this->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $markStudentID, $markList, $thisReportColumnID, 0, $LessonSectionAsWeight=false, $HVCalculation_ApplyOverallWeight);
							
							// round grand total according to the same setting of average???
							$thisGrandMarksArr = $this->ROUND_GRAND_MARKS($termAverageMark);
							$tmpTotal = $thisGrandMarksArr["GrandTotal"];
							$tmpAverage = $thisGrandMarksArr["GrandAverage"];
							$tmpGpa = $thisGrandMarksArr["GradePointAverage"];
							
							$thisTermAverageValues[] = "('$markStudentID', '$ReportID', '$thisReportColumnID', '$tmpTotal', '$tmpAverage', '$tmpGpa', '', '', '', '', '', NOW(), NOW())";
						}
					}
					
					// consolidate the term average & total mark
					if (sizeof($thisTermAverageValues) > 0) {
						$thisTermAverageValues = implode(",", $thisTermAverageValues);
						$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($thisTermAverageValues);
					}
				}
				
				###################### Added on 20080812
				// Calculate subject overall mark for parent subject using consolidated marks
				$parentSubjectOverallValues = array();
				foreach((array)$subjectList as $subjectID => $subjectInfo)
				{
					// check if it is a parent subject, if yes find info of its components subjects
					//$CmpInfoArr = $this->CHECK_HAVE_CMP_SUBJECT($subjectID, $ClassLevelID, $ReportID);
					$CmpInfoArr = $SubjectCmpInfoAssoArr[$subjectID];
					$CmpSubjectArr = $CmpInfoArr[0];
					$CmpSubjectIDArr = $CmpInfoArr[1];
					$isCmpSubject = $CmpInfoArr[2];
					$isCalculateByCmpSub = $CmpInfoArr[3];
					
					## updated on 5 Feb by Ivan - support manual input Parent Subject Marks
					## �~�ؤ��� [CRM Ref No.: 2009-0204-1056]
					if ($eRCTemplateSetting['ManualInputParentSubjectMark'])
					{
						$isCalculateByCmpSub = 0;
					}
					
					if (!$isCalculateByCmpSub || $mixCalculateByCmp[$subjectID]) continue;
					
					$scaleInput = $subjectInfo["scaleInput"];
					$scaleDisplay = $subjectInfo["scaleDisplay"];
					
					if ($eRCTemplateSetting['ParentSubjectCalculationMethod'] == 2)
					{
						// Calculate Parent Overall Mark Honrizontally
						$tmpMarkArr = array();
						$tmpColumnWeightArr = array();
						$tmpTotalColumnWeight = 0;
						$numOfStudent = count($studentIDArr);
						$numOfColumn = count($reportColumnData);
						
						// Get Column Weight
						for($k=0; $k<$numOfColumn; $k++) 
						{
							$thisReportColumnID = $reportColumnData[$k]['ReportColumnID'];
							
							$OtherCondition = " SubjectID = '".$subjectID."' AND ReportColumnID = '".$thisReportColumnID."' ";
							$reportColumnWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
							$tmpColumnWeightArr[$thisReportColumnID] = $reportColumnWeightInfo[0]['Weight'];
							$tmpTotalColumnWeight += $reportColumnWeightInfo[0]['Weight'];
						}
						
						// Build Student Mark Array
						for ($j=0; $j<$numOfStudent; $j++)
						{
							$thisStudentID = $studentIDArr[$j];
							
							for($k=0; $k<$numOfColumn; $k++) 
							{
								$thisReportColumnID = $reportColumnData[$k]['ReportColumnID'];
								
								if ($eRCTemplateSetting['DoNotRoundRawMarkInHorizontalCalculation'] == true)
								{
									$tmpMarkArr[$thisStudentID][$subjectID][$thisReportColumnID] = $studentSubjectColumnMarkRaw[$thisReportColumnID][$thisStudentID][$subjectID];
								}
								else
								{
									$tmpMarkArr[$thisStudentID][$subjectID][$thisReportColumnID] = $studentSubjectColumnMark[$thisReportColumnID][$thisStudentID][$subjectID];
								}
							}
						}
						
						$success["CALCULATE_VERTICAL_SUBJECT_OVERALL"] = $this->CALCULATE_VERTICAL_SUBJECT_OVERALL($tmpMarkArr, $tmpColumnWeightArr, $ReportID, $ClassLevelID, $absentExemptSettings, $reportBasicInfo, $tmpTotalColumnWeight);
						
						################### IMPORTANT ####################################################
						# 20101206 need to prepare $studentSubjectOverallList here
						##################################################################################
						
						// 2013-0711-1349-57140 - missed the parent subject mark in the grand mark calculation
						$RC_REPORT_RESULT_SCORE = $this->DBName.".RC_REPORT_RESULT_SCORE";
						$sql = "Select StudentID, RawMark, Grade From $RC_REPORT_RESULT_SCORE Where ReportID = '".$ReportID."' And SubjectID = '".$subjectID."' And ReportColumnID = '0' And StudentID IN ('".implode("','", (array)$studentIDArr)."')";
						$resultAry = $this->returnResultSet($sql);
						$numOfResult = count((array)$resultAry);
						
						for ($j=0; $j<$numOfResult; $j++) {
							$_studentId = $resultAry[$j]['StudentID'];
							$_rawMark = $resultAry[$j]['RawMark'];
							$_grade = $resultAry[$j]['Grade'];
							
							if ($this->Check_If_Grade_Is_SpecialCase($_grade)) {
								$_targetMark = $_grade; 
							}
							else {
								$_targetMark = $_rawMark;
							}
							$studentSubjectOverallList[$_studentId][$subjectID] = $_targetMark;
						}
					}
					else
					{
						// Calculate the subject overall mark of parent subject using subject overall mark of component subjects
						$parentSubjectOverallMark = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, 0, "Term", 1, 1);
					}
					
					foreach((array)$parentSubjectOverallMark as $studentID => $parentSubjectOverall) {
						// subject overall mark can be calculated as number
						$parentSubjectOverallConverted = $parentSubjectOverall;
						$parentSubjectOverallRaw = $parentSubjectOverall;
						if (is_numeric($parentSubjectOverall)) {
							if ($scaleInput == "M" && $scaleDisplay == "G") {
								
								if ($eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade'])
								{
									$parentSubjectOverallToConvert = $this->ROUND_MARK($parentSubjectOverall, "SubjectTotal");
								}
								else
								{
									$parentSubjectOverallToConvert = $parentSubjectOverall;
								}
								
								$parentSubjectOverallConverted = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $parentSubjectOverallToConvert);
								
							} else {
								$parentSubjectOverall = $this->ROUND_MARK($parentSubjectOverall, "SubjectTotal");
							}
						}
						
						// for calculating average mark
						$studentSubjectOverallList[$studentID][$subjectID] = $parentSubjectOverall;
						
						if ($scaleDisplay=='M' && is_numeric($parentSubjectOverall) && is_numeric($parentSubjectOverallConverted))
							$parentSubjectOverallValues[] = "('$studentID', '$subjectID', '$ReportID', '', '$parentSubjectOverall', '', '$parentSubjectOverallRaw', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
						else
							$parentSubjectOverallValues[] = "('$studentID', '$subjectID', '$ReportID', '', '$parentSubjectOverallRaw', '$parentSubjectOverallConverted', '$parentSubjectOverallRaw', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
					}
				}
				// consolidate the resulting subject overall mark
				if (sizeof($parentSubjectOverallValues) > 0) {
					$parentSubjectOverallValues = implode(",", $parentSubjectOverallValues);
					$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($parentSubjectOverallValues);
				}
				
				###################### Added on 20080812 Ended
				
				
				
				// calculate average mark
				// for Horizontal to Vertical, only average mark of subject overall need to be calculated
				$reportAverageTotal = array();
				$grandValues = array();
				
				foreach((array)$studentSubjectOverallList as $studentID => $subjectOverallList) {
					
					$reportAverageTotal[$studentID] = $this->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $studentID, $subjectOverallList, "", 1);
					
					$thisGrandMarksArr = $this->ROUND_GRAND_MARKS($reportAverageTotal[$studentID]);
					$tmpTotal = $thisGrandMarksArr["GrandTotal"];
					$tmpAverage = $thisGrandMarksArr["GrandAverage"];
					$tmpGpa = $thisGrandMarksArr["GradePointAverage"];
					
					$grandValues[] = "('$studentID', '$ReportID', '', '$tmpTotal', '$tmpAverage', '$tmpGpa', '', '', '', '', '', NOW(), NOW())";
				}
				// consolidate grand total & grand average (no value for ReportColumnID)
				$grandValues = implode(",", $grandValues);
				$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($grandValues);
				
				
				
				
			// Vertical > Horizontal: All subject have a common weight (as a preview only, since these weight is actually for average mark)
			} else if ($calculationOrder == 2) {
				// get the weight (NOT really meaningful for calculating subject overall mark)
				$totalReportColumnWeight = 0;
				for($j=0; $j<sizeof($reportColumnData); $j++) {
					$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$reportColumnData[$j]["ReportColumnID"];
					$reportColumnWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
					$reportColumnWeight[$reportColumnWeightInfo[0]["ReportColumnID"]] = $reportColumnWeightInfo[0]['Weight'];
					$totalReportColumnWeight += $reportColumnWeightInfo[0]['Weight'];
				}
				
				$grandAverageTotalList = array();
				$grandAverageTotalExistWeight = array();
				
				// for storing subject marks of different columns used to calculate subject overall
				// for reference only since grand total will not be calculated by subject overall
				$subjectColumnMarkList = array();
				
				// Loop 2: ReportColumns
				for($j=0; $j<sizeof($reportColumnData); $j++) {
					$reportColumnID = $reportColumnData[$j]["ReportColumnID"];
					
					// Loop 3: Subjects
					$calculationMark = array();
					foreach((array)$subjectList as $subjectID => $subjectInfo) {
						// $schemeInfo[0] is main info, [1] is ranges info
						$schemeInfo = $this->GET_GRADING_SCHEME_INFO($subjectInfo["schemeID"]);
						
						$schemeType = $schemeInfo[0]["SchemeType"];
						$passWording = $schemeInfo[0]["Pass"];
						$failWording = $schemeInfo[0]["Fail"];
						
						$scaleInput = $subjectInfo["scaleInput"];
						$scaleDisplay = $subjectInfo["scaleDisplay"];
						
						// get the grading scheme range info
						$schemeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($subjectInfo["schemeID"]);
						for($m=0; $m<sizeof($schemeRangeInfo); $m++) {
							$schemeRangeIDGradeMap[$schemeRangeInfo[$m]["GradingSchemeRangeID"]] = $schemeRangeInfo[$m]["Grade"];
						}
						
						// check if it is a parent subject
						//$CmpInfoArr = $this->CHECK_HAVE_CMP_SUBJECT($subjectID, $ClassLevelID, $ReportID);
						$CmpInfoArr = $SubjectCmpInfoAssoArr[$subjectID];
						$CmpSubjectArr = $CmpInfoArr[0];
						$CmpSubjectIDArr = $CmpInfoArr[1];
						$isCmpSubject = $CmpInfoArr[2];
						$isCalculateByCmpSub = $CmpInfoArr[3];
						
						## updated on 5 Feb by Ivan - support manual input Parent Subject Marks
						## �~�ؤ��� [CRM Ref No.: 2009-0204-1056]
						if ($eRCTemplateSetting['ManualInputParentSubjectMark'])
						{
							$isCalculateByCmpSub = 0;
						}
						
						if (!isset($isAllCmpZeroWeightAssoArr[$subjectID])){
							 $isAllCmpZeroWeightAssoArr[$subjectID] = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $subjectID);
                        }
						$isAllCmpZeroWeightArr = $isAllCmpZeroWeightAssoArr[$subjectID];

                        // [2020-0717-1352-51206]
						// if ($isAllCmpZeroWeightArr[$reportColumnID]) {
						if ($isAllCmpZeroWeightArr[$reportColumnID] && !$parentSubjAlwaysCalculatedByCmp)
						{
							$isCalculateByCmpSub = 0;
						}
						
						// get raw marks of all students in a class of this assessment
						if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub))
							$marksheetScore = $this->GET_MARKSHEET_SCORE($studentIDArr, $subjectID, $reportColumnID);
						else
							$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID, "Term", 0);
							
						$conAssessmentMarkValues = array();
						$specialCaseDifferentiate = array();
						// Loop 4: Student's Score
						foreach((array)$marksheetScore as $studentID => $scoreDetailInfo) {
							$AllColumnMarkArr = array();
							$consolidateMark = "";
							$scoreInfo = array();
							if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub)) {
								$scoreInfo = $scoreDetailInfo;
							} else {
								$scoreInfo = $this->CREATE_SCORE_INFO_ARRAY($scoreDetailInfo);
							}
							
							$rawMark = "";
							if ($scoreInfo["MarkType"] == "M" || $scoreInfo["MarkType"] == "G") {
								if ($scaleInput == "M" && $scaleDisplay == "M") {
									$rawMark = $scoreInfo["MarkRaw"];
									
									if ($eRCTemplateSetting['DoNotRoundRawMarkInHorizontalCalculation']) {
										$consolidateMark = $scoreInfo["MarkRaw"];
									}
									else {
										$consolidateMark = $this->ROUND_MARK($scoreInfo["MarkRaw"], "SubjectScore");
									}
									
									// check if the consolidation mark need to be converted to weight mark
									if ($calculationSettings["UseWeightedMark"] == "1") {
										if ($isCmpSubject) {
											$consolidateMark = $this->CONVERT_CMP_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $consolidateMark, 2);
										} else {
											$consolidateMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $consolidateMark, 2);
										}
									}
									
									if ($eRCTemplateSetting['DoNotRoundRawMarkInHorizontalCalculation']) {
										$consolidateMark = $consolidateMark;
									}
									else {
										$consolidateMark = $this->ROUND_MARK($consolidateMark, "SubjectScore");
									}
									
									
									// Vertical -> Horizontal: need to store weighted mark to calculate subject overall
									if ($eRCTemplateSetting['DoNotRoundRawMarkInHorizontalCalculation']) {
										$calculationMark[$studentID][$subjectID] = $scoreInfo["MarkRaw"];
									}
									else {
										$calculationMark[$studentID][$subjectID] = $this->ROUND_MARK($scoreInfo["MarkRaw"], "SubjectScore");
									}
									
									#$calculationMark[$studentID][$subjectID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $calculationMark[$studentID][$subjectID], 2);
								} else if ($scaleInput == "M" && $scaleDisplay == "G") {
									$rawMark = $scoreInfo["MarkRaw"];
									
									// convert to grade according to ranges
									if ($eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade'])
									{
										$rawMarkToConvert = $this->ROUND_MARK($rawMark, "SubjectScore");
									}
									else
									{
										$rawMarkToConvert = $rawMark;
									}
									$consolidateMark = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $rawMarkToConvert);
									
									if ($eRCTemplateSetting['DoNotRoundRawMarkInHorizontalCalculation']) {
										$calculationMark[$studentID][$subjectID] = $scoreInfo["MarkRaw"];
									}
									else {
										$calculationMark[$studentID][$subjectID] = $this->ROUND_MARK($scoreInfo["MarkRaw"], "SubjectScore");
									}
									
									#$calculationMark[$studentID][$subjectID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $calculationMark[$studentID][$subjectID], 2);
								} else if ($scaleInput == "G" && $scaleDisplay == "G") {
									$consolidateMark = $schemeRangeIDGradeMap[$scoreInfo["MarkNonNum"]];
									$calculationMark[$studentID][$subjectID] = "-";
								}
								
								// round off the weighted mark
								if ($scaleDisplay == "M" && is_numeric($consolidateMark)) {
									$consolidateMark = $consolidateMark;
								}
								
								// consolidate the mark into DB (RC_REPORT_RESULT_SCORE)
								$mark = "";
								$grade = "";
								if ($scaleDisplay == "M" && is_numeric($consolidateMark)) {
									$mark = $consolidateMark;
								} else {
									$grade = $consolidateMark;
									$mark = $this->ROUND_MARK($rawMark, "SubjectScore");
								}
								
								$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '$mark', '$grade', '$rawMark', '', '$semester', '', '', NOW(), NOW())";
							} else if ($scoreInfo["MarkType"] == "PF") {
								$consolidateMark = ($scoreInfo["MarkNonNum"] == "P") ? $passWording : $failWording;
								$PassFailMark = $consolidateMark;
								$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$PassFailMark', '', '', '$semester', '', '', NOW(), NOW())";
								$calculationMark[$studentID][$subjectID] = "-";
							} else {								
								$specialCase = $scoreInfo["MarkNonNum"];
								
								$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $specialCase, $absentExemptSettings);
								
								if ($specialCase == 'N.A.' || $specialCase == '-' || $specialCase == 'abs')
								{
									$calculationMark[$studentID][$subjectID] = $specialCase;
								}
								else if ($specialCaseDifferentiate[$specialCase]["isExFullMark"] == 1) {
									$calculationMark[$studentID][$subjectID] = "*";
								} else if ($specialCaseDifferentiate[$specialCase]["isCount"] == 0) {
									$calculationMark[$studentID][$subjectID] = "/";
								} else {
									$calculationMark[$studentID][$subjectID] = $specialCaseDifferentiate[$specialCase]["Value"];
								}
								
								$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$specialCase', '', '', '$semester', '', '', NOW(), NOW())";
							}
							
						} // End Loop 4
						
						
						// insert the converted mark into DB
						if (sizeof($conAssessmentMarkValues) > 0) {
							$conAssessmentMarkValues = implode(",", $conAssessmentMarkValues);
							$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($conAssessmentMarkValues);
						}
					} // End Loop 3: Subjects
					
					if (sizeof($calculationMark) > 0) {
						$termAverageValues = array();
						$termAverageMark = "";
						// calculate the term average mark (vertically across subjects) of each student
						foreach((array)$calculationMark as $markStudentID => $markList) {
							$termAverageMark = $this->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $markStudentID, $markList, $reportColumnID, 1);
							
							foreach((array)$markList as $markSubjectID => $markSubject) {
								$subjectColumnMarkList[$markStudentID][$markSubjectID][$reportColumnID] = $markSubject;
							}
							
							// didn't consider the extreme case with no average mark can be calculated
							if (!isset($grandAverageTotalExistWeight[$markStudentID])) 
								$grandAverageTotalExistWeight[$markStudentID] = 0;
							if (!isset($grandAverageTotalList[$markStudentID]["GrandTotal"]))
								$grandAverageTotalList[$markStudentID]["GrandTotal"] = 0;
							if (!isset($grandAverageTotalList[$markStudentID]["GrandAverage"]))
								$grandAverageTotalList[$markStudentID]["GrandAverage"] = 0;
							if (!isset($grandAverageTotalList[$markStudentID]["GradePointAverage"]))
								$grandAverageTotalList[$markStudentID]["GradePointAverage"] = 0;
							
							// round grand total according to the same setting of average???
							$thisGrandMarksArr = $this->ROUND_GRAND_MARKS($termAverageMark);
							$tmpTotal = $thisGrandMarksArr["GrandTotal"];
							$tmpAverage = $thisGrandMarksArr["GrandAverage"];
							$tmpGpa = $thisGrandMarksArr["GradePointAverage"];
							
							$grandAverageTotalExistWeight[$markStudentID] += $reportColumnWeight[$reportColumnID];
							$grandAverageTotalList[$markStudentID]["GrandTotal"] += $tmpTotal * $reportColumnWeight[$reportColumnID];
							$grandAverageTotalList[$markStudentID]["GrandAverage"] += $tmpAverage * $reportColumnWeight[$reportColumnID];
							$grandAverageTotalList[$markStudentID]["GradePointAverage"] += $tmpGpa * $reportColumnWeight[$reportColumnID];
							
							$termAverageValues[] = "('$markStudentID', '$ReportID', '$reportColumnID', '$tmpTotal', '$tmpAverage', '$tmpGpa', '', '', '', '', '', NOW(), NOW())";
						}
						
						// consolidate the term average & total mark
						if (sizeof($termAverageValues) > 0) {
							$termAverageValues = implode(",", $termAverageValues);
							$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($termAverageValues);
						}
					}
				} // End Loop 2: ReportColumns
				
				// Calculation of each subject overall mark (horizontal calculation)
				$success["CALCULATE_VERTICAL_SUBJECT_OVERALL"] = $this->CALCULATE_VERTICAL_SUBJECT_OVERALL($subjectColumnMarkList, $reportColumnWeight, $ReportID, $ClassLevelID, $absentExemptSettings, $reportBasicInfo, $totalReportColumnWeight);
				
				
				// 2011-0207-1031-12073 - Ignore the Display Grade Settings
				//if ($eRCTemplateSetting['UseOverallColumnToCalculateGrandMarksForTermReport']==false || $this->isAllSubjectsDisplayGrade($ClassLevelID, $ReportID)==true)
				if ($eRCTemplateSetting['UseOverallColumnToCalculateGrandMarksForTermReport']==false)
				{
					## normal "vertical->honrizontal" calculation
					// consolidate the grand average & total mark
					if (sizeof($grandAverageTotalList) > 0) {
						$grandAverageValues = array();
						foreach((array)$grandAverageTotalList as $grandStudentID => $grandList) {
							if (isset($grandAverageTotalExistWeight[$grandStudentID])) {
								##### handle total weight <> exist weight => need to exclude weight?
							}
							
							# Round Grand Marks
							$thisGrandMarksArr = $this->ROUND_GRAND_MARKS($grandList);
							$tmpTotal = $thisGrandMarksArr["GrandTotal"];
							$tmpAverage = $thisGrandMarksArr["GrandAverage"];
							$tmpGpa = $thisGrandMarksArr["GradePointAverage"];
							
							$grandAverageValues[] = "('$grandStudentID', '$ReportID', '', '$tmpTotal', '$tmpAverage', '$tmpGpa', '', '',  '', '', '', NOW(), NOW())";
						}
						$grandAverageValues = implode(",", $grandAverageValues);
						$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($grandAverageValues);
					}
				}
				else	## added on 20 Feb 2009 by Ivan for St. Clare's Primary
				{
					## get all the clculated subject overall marks and use the "honrizontal-vertical" method to calculate Grand marks
					$studentSubjectOverallList = array();
					$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
					if (sizeof($MainSubjectArray) > 0)
						foreach((array)$MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
					
					for ($s=0; $s<sizeof($studentIDArr); $s++)
					{
						$thisStudentID = $studentIDArr[$s];
						$thisMarksArr = $this->getMarks($ReportID, $thisStudentID, "", $ParentSubjectOnly=1, $includeAdjustedMarks=1, $OrderBy='', $SubjectID='', $targetReportColumnID=0, $CheckPositionDisplay=0);
						
						foreach((array)$thisMarksArr as $thisSubjectID => $data)
						{
							if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)
								$isSub=1;
							else
								$isSub=0;
								
							if ($isSub) continue;
							
							//$thisMark = $data[0]['Mark'];
							$thisGrade = $data[0]['Grade'];
							
							if ($subjectList[$thisSubjectID]['scaleDisplay'] == 'M')
								$thisMark = $data[0]['Mark'];
							else
								$thisMark = $this->ROUND_MARK($data[0]['RawMark'], "SubjectTotal");
							
							if (in_array($thisGrade, $this->specialCasesSet1) || in_array($thisGrade, $this->specialCasesSet2))
								continue;
								
							//list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark, $data[0]['Grade']);
							
							$studentSubjectOverallList[$thisStudentID][$thisSubjectID] = $thisMark;
						}
					}
					
					// calculate average mark
					// for Horizontal to Vertical, only average mark of subject overall need to be calculated
					$reportAverageTotal = array();
					$grandValues = array();
					
					## Use the weight of the last template column as the overall weight calculation
					$lastReportColumnIDIndex = sizeof($reportColumnData) - 1;
					$lastReportColumnID = $reportColumnData[$lastReportColumnIDIndex]["ReportColumnID"];
					
					foreach((array)$studentSubjectOverallList as $studentID => $subjectOverallList) {
						$thisWeightSourceColumnID = $lastReportColumnID;
						if ($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation']) {
							$thisWeightSourceColumnID = 0;
						}
						$reportAverageTotal[$studentID] = $this->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $studentID, $subjectOverallList, $thisWeightSourceColumnID);
						
						# Round Grand Marks
						$thisGrandMarksArr = $this->ROUND_GRAND_MARKS($reportAverageTotal[$studentID]);
						$tmpTotal = $thisGrandMarksArr["GrandTotal"];
						$tmpAverage = $thisGrandMarksArr["GrandAverage"];
						$tmpGpa = $thisGrandMarksArr["GradePointAverage"];
						
						$grandValues[] = "('$studentID', '$ReportID', '', '$tmpTotal', '$tmpAverage', '$tmpGpa', '', '',  '', '', '', NOW(), NOW())";
					}
					// consolidate grand total & grand average (no value for ReportColumnID)
					$grandValues = implode(",", $grandValues);
					$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($grandValues);
				}
			} // End Condition: CalculationOrder
		} // End Loop 1: Classes
		
		// Update SD Score
		// 1. Order Position by Weighted SD 	OR 		2. Generate SD order
		if ($eRCTemplateSetting['OrderPositionMethod'] == "WeightedSD" || $eRCTemplateSetting['GenerateSDOrdering'])
			$this->UPDATE_SD_SCORE($ReportID, $ClassLevelID,"T");
			
		// Calculate Subject Overall by Assessments
		if ($eRCTemplateSetting['OverallGradeCalculatedFromAssesmentGPA']) {
			$this->UpdateOverallGradeFromAssessmentGrade($ReportID);
		}
		
		if ($eRCTemplateSetting['Calculation']['ActualAverage']) {
			$this->UpdateActualAverage($ReportID);
		}
		
		// Update order (class & form)
		$this->UPDATE_ORDER($ReportID, $ClassLevelID);
		
		// [2015-0421-1144-05164] Update SD Order (class & form)
		if($eRCTemplateSetting['GenerateSDOrdering']){
			$this->UPDATE_ORDER($ReportID, $ClassLevelID, "SDScore", "GrandSDScore", true);	
		}
		
		// Update marks for '% Range' Subjects
		// Since the calculation of '% Range' depends on the order of the student in the form, the calculation should be at last
		$reportColumnIDList[] = 0;	// update overall score also		
		for($i=0; $i<sizeof($classArr); $i++) {
			// construct the $studentIDArr
			$classID = $classArr[$i]["ClassID"];
			$studentIDArr = array();
			for($j=0; $j<sizeof($studentArr[$classID]); $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			
			// SKIP this class if no students
			if (sizeof($studentIDArr) == 0) continue;
			
			foreach((array)$subjectList as $subjectID => $subjectInfo) {
				$schemeID = $subjectInfo["schemeID"];
				$schemeInfo = $this->GET_GRADING_SCHEME_INFO($schemeID);
				$schemeMainInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($schemeID);
				
				$scaleInput = $subjectInfo["scaleInput"];
				$scaleDisplay = $subjectInfo["scaleDisplay"];
				$TopPercentage = $schemeMainInfo["TopPercentage"];
								
				// update the "Mark-to-Grade" subjects only
				if ($scaleInput == "M" && $scaleDisplay == "G" && ($TopPercentage==1 || $TopPercentage==2))
				{
					// [2015-1104-1130-08164] Get Make-up Exam Score
					$MarkupResultAry = array();
					if($eRCTemplateSetting['ReportGeneration']['MarkupExamGrade'])
					{
						if(!empty($studentIDArr) && is_array($studentIDArr))
						{
							// Graduate Exam
							$SchoolType	= $this->GET_SCHOOL_LEVEL($ClassLevelID);
							$isSecondaryGraduate = $SchoolType=="S" && $formNumber==6;
							if($isSecondaryGraduate && $semester_num==3)
							{
								// Get Year Report
								$thisYearReportInfo = $this->returnReportTemplateBasicInfo("", $others="", $ClassLevelID, "F", $isMainReport=1);
								$thisYearReportID = $thisYearReportInfo["ReportID"];
								if($thisYearReportID) {
									$MarkupResultAry = $this->GET_EXTRA_SUBJECT_INFO($studentIDArr, $subjectID, $thisYearReportID);
								}
							}
						}
					}
					
					foreach((array)$reportColumnIDList as $reportColumnID)
					{
						// get score of all students in this assessment
						$resultScoreArr = $this->GET_RESULT_SCORE($studentIDArr, $subjectID, $reportColumnID);
												
						foreach((array)$studentIDArr as $studentID)
						{
							$oldMark = $resultScoreArr[$studentID]['Mark'];
							$oldGrade = $resultScoreArr[$studentID]['Grade'];
							$thisReportResultScoreID = $resultScoreArr[$studentID]['ReportResultScoreID'];
							
							### skip the special case mark
							if (in_array($oldGrade, $this->specialCasesSet1) || in_array($oldGrade, $this->specialCasesSet2))
								continue;
							
							if ($eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade'])
							{
								$oldMark = $this->ROUND_MARK($oldMark, "SubjectScore");
							}
							
							$newGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($schemeID, $oldMark, $ReportID, $studentID, $subjectID, $ClassLevelID, $reportColumnID);
							
							// [2015-1104-1130-08164] Set Grade to D for students that pass the Make-up Exam
							if($eRCTemplateSetting['ReportGeneration']['MarkupExamGrade'] && ($newGrade=="F" || $newGrade=="+"))
							{
								if(!empty($MarkupResultAry) && $isSecondaryGraduate && $semester_num==3 && $n==1)
								{
									$MarkupExamScore = $MarkupResultAry[$studentID]["Info"];
									if(!empty($MarkupExamScore) && $MarkupExamScore >= 60)
									{
										$newGrade = "D";
									}
								}
							}
							
							$this->UPDATE_REPORT_RESULT_SCORE($ReportID, $studentID, $reportColumnID, $subjectID, $oldMark, $newGrade, $thisReportResultScoreID);
						}
					}
				}
			}
		}
		
		if ($this->applyCustomizationFlag('SpecialPassingCheckingIfRelatedScoresAreAllPassed', $ReportID)) {
			$this->UpdatePassingStatusSpecialChecking($ReportID);
		}
		
		//2016-0601-1555-49164
		if ($eRCTemplateSetting['Report']['ReportGeneration']['EffortAsFirstReportColumnGrade']) {
			$this->UpdateEffortAsFirstReportColumnGrade($ReportID);
		}
				
//			$unit=array('b','kb','mb','gb','tb','pb');
//			$this->Write_Log("memory_get_usage:".(memory_get_usage()/pow(1024,($i=floor(log(memory_get_usage(),1024))))).' '.$unit[$i]);
	}
	
	function GENERATE_WHOLE_YEAR_REPORTCARD($ReportID, $ClassLevelID) {
		global $PATH_WRT_ROOT, $ReportCardCustomSchoolName, $eRCTemplateSetting;
		$success = array();
		
		// delete the consolidated data (if exist)
		$success["DELETE_REPORT_RESULT_SCORE"] = $this->DELETE_REPORT_RESULT_SCORE($ReportID);
		$success["DELETE_REPORT_RESULT"] = $this->DELETE_REPORT_RESULT($ReportID);
		$success["DELETE_NEW_FULLMARK"] = $this->DELETE_NEW_FULLMARK($ReportID);
		
		// Add all empty marks / no mark input set to N.A.
		//$success["MS_ALL_EMPTY_TO_NA"] = $this->FILL_ALL_EMPTY_SCORE_WITH_NA($ReportID);
		
		################### Preload settings & data ###################
		// load Mark Storage & Display settings: SubjectScore (0,1,2), SubjectTotal (0,1,2), GrandAverage (0,1,2)
		$storageDisplaySettings = $this->LOAD_SETTING("Storage&Display");
		
		// load Calculation settings: OrderTerm (1,2), OrderFullYear (1,2), UseWeightedMark (0,1)
		$calculationSettings = $this->LOAD_SETTING("Calculation");
		$isAbjustFullMark = $calculationSettings["AdjustToFullMarkFirst"];
		
		// load Absent & Exempt settings: Exempt (ExWeight, ExFullMark, Zero), Absent (ExWeight, ExFullMark, Zero)
		$absentExemptSettings = $this->LOAD_SETTING("Absent&Exempt");
		
		// Get report template basic info: Semester, PercentageOnColumnWeight
		$reportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$classLevelId = $reportBasicInfo['ClassLevelID']; 
		
		// Check for Combination of Special Case of Input Mark / Term Mark
		$SpecialCaseArrH = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		foreach((array)$SpecialCaseArrH as $key){
			$SpecialCaseCountArrH[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
		}
		$SpecialCaseArrPF = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
		foreach((array)$SpecialCaseArrPF as $key){
			$SpecialCaseCountArrPF[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
		}

        // [2020-0717-1352-51206]
        $parentSubjAlwaysCalculatedByCmp = $this->schoolYear == '2019' && $eRCTemplateSetting['Report']['ReportGeneration']['ParentAlwaysCalculatedByCmpAtYear2019'];
		
		// $reportColumnData[$i] + ["ReportColumnID"], ["ColumnTitle"], ["DefaultWeight"], 
		// ["SemesterNum"], ["IsDetails"], ["ShowPosition"], ["PositionRange"], ["DisplayOrder"]
		$reportColumnData = $this->returnReportTemplateColumnData($ReportID);
		$numOfReportColumn = count($reportColumnData);
		if ($numOfReportColumn > 0)
		{
			//$totalReportColumnWeight = 0;
			for($i=0; $i<$numOfReportColumn; $i++) {
				$reportInvolveTerms[] = $reportColumnData[$i]["SemesterNum"];
				$reportColumnTermMap[$reportColumnData[$i]["ReportColumnID"]] = $reportColumnData[$i]["SemesterNum"];
				
				//$reportTermColumnMap[$reportColumnData[$i]["SemesterNum"]] = $reportColumnData[$i]["ReportColumnID"];
				if ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment']) {
					$reportTermColumnMap[$reportColumnData[$i]["SemesterNum"]][$reportColumnData[$i]["TermReportColumnID"]] = $reportColumnData[$i]["ReportColumnID"];
				}
				else {
					$reportTermColumnMap[$reportColumnData[$i]["SemesterNum"]] = $reportColumnData[$i]["ReportColumnID"];
				}
			}

			// [2020-0708-1350-06164] exclude last term report column for subject result calculation of special report
			if($eRCTemplateSetting['Report']['ReportGeneration']['SpecialYearReport_FillNAToLastColumn'] && $eRCTemplateSetting['Report']['ReportGeneration']['SpecialYearReport_ReportIdentifier'] != '')
			{
                $lastYearReportColumnIndex = $numOfReportColumn - 1;
    			$lastYearReportColumnID = $reportColumnData[$lastYearReportColumnIndex]["ReportColumnID"];
    			if(strpos($reportBasicInfo['ReportTitle'], $eRCTemplateSetting['Report']['ReportGeneration']['SpecialYearReport_ReportIdentifier']) !== false && $lastYearReportColumnID > 0)
    			{
    			    $lastYearReportSemID = $reportColumnData[$lastYearReportColumnIndex]["SemesterNum"];
    			    
    			    // Set Marksheet Score to N.A.
                    $table = $this->DBName.".RC_MARKSHEET_SCORE";
    		        $sql = "UPDATE $table SET MarkType = 'SC', MarkRaw = '-1', MarkNonNum = 'N.A.' WHERE ReportColumnID = '$lastYearReportColumnID'";
    		        $this->db_db_query($sql);
                }
            }
		}
		
		$termReportIdMappingAry = array();
		$subjectFullMarkMappingAry = array();
		for ($i=0; $i<$numOfReportColumn; $i++) {
			$_reportColumnId = $reportColumnData[$i]['ReportColumnID'];
			$_semesterNum = $reportColumnData[$i]['SemesterNum'];
			
			$_termReportInfoAry = $this->returnReportTemplateBasicInfo('', $others='', $classLevelId, $_semesterNum, $isMainReport=1);
			$_termReportId = $_termReportInfoAry['ReportID'];
			$termReportIdMappingAry[$_reportColumnId] = $_termReportId;
			
			$subjectFullMarkMappingAry[$_termReportId] = $this->returnSubjectFullMark($classLevelId, $withSub=1, $ParSubjectIDArr=array(), $ParInputScale=1, $_termReportId);
		}
		$subjectFullMarkMappingAry[$ReportID] = $this->returnSubjectFullMark($classLevelId, $withSub=1, $ParSubjectIDArr=array(), $ParInputScale=1, $ReportID);
		
		
		// $SubjectGradingSchemeAssoArr[$ReportID][$SubjectID] = InfoArr
		$SubjectGradingSchemeAssoArr = array();
		
		// whole year report
		$calculationOrder = $calculationSettings["OrderFullYear"];
		
		// what terms include in this whole year report?
		// does these terms have its own term report before?
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		$reportInvolveTermsList = implode("','", $reportInvolveTerms);
		$reportInvolveTermsList = "'".$reportInvolveTermsList."'";
		$sql = "SELECT ReportID, Semester FROM $table WHERE ClassLevelID = '$ClassLevelID' AND Semester IN ($reportInvolveTermsList) And isMainReport = 1";
		$existTermReportID = $this->returnArray($sql, 2);
		
		$searchExistTermMode = "T";
		if (sizeof($existTermReportID) == 0) {
			# search completed whole year report also
			$sql = "SELECT ReportID, Semester FROM $table WHERE ClassLevelID = '$ClassLevelID' AND Semester = 'F' AND ReportID != '$ReportID'";
			$existTermReportID = $this->returnArray($sql, 2);
			$searchExistTermMode = "W";
		}
		
		if (sizeof($existTermReportID) > 0) {
			$completedTerm = array();
			$completedTermReportID = array();
			for($i=0; $i<sizeof($existTermReportID); $i++) {
				$errArr = array();
				#######################
				# Disable the following line will enable consolidation of existing term mark even if it's not completed
				//$errArr = $this->checkCanGenernateReport($existTermReportID[$i]["ReportID"]);
				#######################
				
				// no errors = report was completed
				if (empty($errArr) && sizeof($errArr) == 0) {
					if ($existTermReportID[$i]["Semester"] == "F") {
						$existTermReportColumnData = $this->returnReportTemplateColumnData($existTermReportID[$i]["ReportID"]);
						for($j=0; $j<sizeof($existTermReportColumnData); $j++) {
							if (in_array($existTermReportColumnData[$j]["SemesterNum"], $reportInvolveTerms))
								$completedTermReportID[$existTermReportColumnData[$j]["SemesterNum"]] = $existTermReportID[$i]["ReportID"];
						}
					} else {
						$completedTermReportID[$existTermReportID[$i]["Semester"]] = $existTermReportID[$i]["ReportID"];
					}
				}
			}
			
			if (sizeof($completedTermReportID) > 0) {
				// retrieve existing term marks & consolidate them
				foreach((array)$completedTermReportID as $completedSemester => $completedReportID) {
					// skip copying if this report is an older report
					// MUST maintain the report creation order for this to work
					if ($searchExistTermMode != "T" && $ReportID < $completedReportID) {
						continue;
					}
					$tmpExistTermSubjectOverall = $this->COPY_EXIST_TERM_RESULT_SCORE($completedReportID, $completedSemester, $ReportID, $reportTermColumnMap, $searchExistTermMode, $ClassLevelID);
					$tmpExistTermAverageTotal = $this->COPY_EXIST_TERM_RESULT($completedReportID, $completedSemester, $ReportID, $reportTermColumnMap);

					// [2020-0708-1350-06164] exclude last term report column for subject result calculation of special report
					if($eRCTemplateSetting['Report']['ReportGeneration']['SpecialYearReport_FillNAToLastColumn'] && $eRCTemplateSetting['Report']['ReportGeneration']['SpecialYearReport_ReportIdentifier'] != '')
					{
					    // $lastYearReportSemID valid if is special report
    					if($lastYearReportSemID > 0 && $lastYearReportSemID == $completedSemester && $lastYearReportColumnID > 0)
    					{
    					    // Set Report Result Score to N.A.
    					    $table = $this->DBName.".RC_REPORT_RESULT_SCORE";
    					    $sql = "UPDATE $table SET Mark = '0', Grade = 'N.A.', RawMark = '0', OrderMeritClass = '-1', OrderMeritForm = '-1' WHERE ReportColumnID = '$lastYearReportColumnID'";
    					    $this->db_db_query($sql);
    					    
    					    // Update $tmpExistTermSubjectOverall data array
    					    foreach((array)$tmpExistTermSubjectOverall as $overallStudentID => $studentTmpExistTermSubjectOverall)
    					    {
    					        foreach((array)$studentTmpExistTermSubjectOverall as $overallSubjectID => $studentSubjectTmpExistTermOverall)
    					        {
    					            foreach((array)$studentSubjectTmpExistTermOverall as $overallIndex => $thisOverallInfo)
    					            {
    					                if($thisOverallInfo['ReportColumnID'] && $thisOverallInfo['ReportColumnID'] == $lastYearReportColumnID)
    					                {
    					                    $tmpExistTermSubjectOverall[$overallStudentID][$overallSubjectID][$overallIndex] = array(
    					                        "ReportColumnID" => $lastYearReportColumnID,
    					                        "Mark" => '0',
    					                        "Grade" => 'N.A.',
    					                        "RawMark" => '0',
    					                        "OrderMeritClass" => '-1',
    					                        "OrderMeritForm" => '-1'
    					                    );
    					                }
    					            }
    					        }
    					    }
					    }
					}
					
					if (is_array($tmpExistTermSubjectOverall))
						$existTermSubjectOverallArr[] = $tmpExistTermSubjectOverall;
					else
						unset($completedTermReportID[$completedSemester]);
					if (is_array($tmpExistTermAverageTotal))
						$existTermAverageTotalArr[] = $tmpExistTermAverageTotal;
				}
			}
		}
		
		$tmpSpecialCaseArrH = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		// For calculating subject overall mark in Vertical-Horizontal
		for($i=0; $i<sizeof($existTermSubjectOverallArr); $i++) {
			foreach((array)$existTermSubjectOverallArr[$i] as $tmpStudentID => $tmpSubjectInfo) {
				foreach((array)$tmpSubjectInfo as  $tmpSubjectID => $tmpSubjectOverallInfo) {
					$tmpReportColumnID = $tmpSubjectOverallInfo[0]["ReportColumnID"];
					
					# updated on 5th May 2009 by Ivan
					# use Mark instead of RawMark for calculation 
					# �t�Űǭ^��p�� - eRC - P1-4 consolidate report cannot show total result [CRM Ref No.: 2009-0504-1607]
					$tmpRawMark = $tmpSubjectOverallInfo[0]["RawMark"];
					$tmpMark = $tmpSubjectOverallInfo[0]["Mark"];
					$tmpGrade = $tmpSubjectOverallInfo[0]["Grade"];
					
					# Retrieve Subject Scheme ID & settings
					if (!isset($SubjectGradingSchemeAssoArr[$ReportID][$tmpSubjectID])) {
						$SubjectGradingSchemeAssoArr[$ReportID][$tmpSubjectID] = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 0, 0, $ReportID);
					}
					$SubjectFormGradingSettings = $SubjectGradingSchemeAssoArr[$ReportID][$tmpSubjectID]; 
					
					//$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 0, 0, $ReportID);
					$SchemeID = $SubjectFormGradingSettings['SchemeID'];
					//$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
					$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
					
					if ($this->Check_If_Grade_Is_SpecialCase($tmpGrade)==true){
						$tmpMark = $tmpGrade;
					}
					else if ($ScaleInput=="M" && $ScaleDisplay=="G"){
						$tmpMark = $tmpRawMark;
					}
					else if ($tmpGrade != ''){
						$tmpMark = $tmpGrade;
					}
					else if ($eRCTemplateSetting['UseRawMarkToCalculateSDScore'] == "WeightedSD"){
						$tmpMark = $tmpRawMark;
					}
						
					if ($eRCTemplateSetting['ConsolidatedReportUseGPAToCalculate'] && $this->Check_If_Grade_Is_SpecialCase($tmpGrade)==false) {
						$tmpMark = $this->CONVERT_GRADE_TO_GRADE_POINT_FROM_GRADING_SCHEME($SchemeID, $tmpGrade);
					}
					
					## updated on 23 Feb by Ivan - add checking special case instead of copy marks directly
					# check special case
					//list($tmpRawMark, $needStyle) = $this->checkSpCase($ReportID, $tmpSubjectID, $tmpRawMark, $tmpGrade);
					//list($tmpMark, $needStyle) = $this->checkSpCase($ReportID, $tmpSubjectID, $tmpMark, $tmpGrade);
					
					//$allSubjectColumnMarkList[$tmpStudentID][$tmpSubjectID][$tmpReportColumnID] = $tmpRawMark;
					$allSubjectColumnMarkList[$tmpStudentID][$tmpSubjectID][$tmpReportColumnID] = $tmpMark;
					
					/*
					if (is_numeric($tmpSubjectOverallInfo[0]["RawMark"]))
						$allSubjectColumnMarkList[$tmpStudentID][$tmpSubjectID][$tmpReportColumnID] = $tmpRawMark;
					*/
					
					// [2018-0703-0940-05164] must get 2nd report column
					if(!empty($eRCTemplateSetting['Report']['ReportGeneration']['ConsolidateAssessmentColumnType']))
					{
    					$tmpReportColumnID = $tmpSubjectOverallInfo[1]["ReportColumnID"];
    					$tmpRawMark = $tmpSubjectOverallInfo[1]["RawMark"];
    					$tmpMark = $tmpSubjectOverallInfo[1]["Mark"];
    					$tmpGrade = $tmpSubjectOverallInfo[1]["Grade"];
    					
    					# Retrieve Subject Scheme ID & settings
    					if (!isset($SubjectGradingSchemeAssoArr[$ReportID][$tmpSubjectID])) {
    					    $SubjectGradingSchemeAssoArr[$ReportID][$tmpSubjectID] = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 0, 0, $ReportID);
    					}
    					$SubjectFormGradingSettings = $SubjectGradingSchemeAssoArr[$ReportID][$tmpSubjectID];
    					$SchemeID = $SubjectFormGradingSettings['SchemeID'];
    					$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
    					$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
    					
    					if ($this->Check_If_Grade_Is_SpecialCase($tmpGrade)==true){
    					    $tmpMark = $tmpGrade;
    					}
					    else if ($ScaleInput=="M" && $ScaleDisplay=="G"){
					        $tmpMark = $tmpRawMark;
					    }
				        else if ($tmpGrade != '') {
				            $tmpMark = $tmpGrade;
				        }
			            else if ($eRCTemplateSetting['UseRawMarkToCalculateSDScore'] == "WeightedSD"){
			                $tmpMark = $tmpRawMark;
			            }
		                
		                if ($eRCTemplateSetting['ConsolidatedReportUseGPAToCalculate'] && $this->Check_If_Grade_Is_SpecialCase($tmpGrade)==false) {
		                    $tmpMark = $this->CONVERT_GRADE_TO_GRADE_POINT_FROM_GRADING_SCHEME($SchemeID, $tmpGrade);
		                }
		                $allSubjectColumnMarkList[$tmpStudentID][$tmpSubjectID][$tmpReportColumnID] = $tmpMark;
					}
				}
			}
		}
		
		// $classArr[$i] + ["ClassID"], ["ClassName"]
		$classArr = $this->GET_CLASSES_BY_FORM($ClassLevelID);
		
		for($i=0; $i<sizeof($classArr); $i++) {
			// $studentArr[#ClassID][$i] + ["UserID"], ["WebSAMSRegNo"],...
			$studentArr[$classArr[$i]["ClassID"]] = $this->GET_STUDENT_BY_CLASS($classArr[$i]["ClassID"]);
		}
		
		$subjectList = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,0,$ReportID);
		
		$SubjectCmpInfoAssoArr = array();
		foreach((array)$subjectList as $subjectID => $subjectInfo)
		{
			$SubjectCmpInfoAssoArr[$subjectID] = $this->CHECK_HAVE_CMP_SUBJECT($subjectID, $ClassLevelID, $ReportID);
		}
		
		
		// for use in checking promotion criteria
		//$classLevelName = $this->returnClassLevel($ClassLevelID);
		//$nextPromoteClassLevel = $this->GET_NEXT_PROMOTED_CLASSLEVEL($classLevelName);
		//$subjectCodeIDMap = $this->GET_SUBJECTS_CODEID_MAP();
		######## Hardcoded subject's CODEID required for checking promotion criteria ########
		//$requirePassSubjectCodeID = array("101", "114");
		//$requirePassSubjectCodeID = array();
		
		// Loop 1: Classes
		for($i=0; $i<sizeof($classArr); $i++) {
			// construct the $studentIDArr
			$classID = $classArr[$i]["ClassID"];
			$studentIDArr = array();
			for($j=0; $j<sizeof($studentArr[$classID]); $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			
			// skip this class if no students
			$numOfStudent = count($studentIDArr);
			if ($numOfStudent == 0) continue;
			
			### Add mark for newly added student which has no Term Report Records
			//for ($j=0; $j<$numOfStudent; $j++)
			//{
				//$thisStudentID = $studentIDArr[$j];
				foreach((array)$existTermAverageTotalArr as $thisIndex => $thisReportColumnMarkArr)
				{
					foreach((array)$thisReportColumnMarkArr as $thisReportColumnID => $thisStudentReportColumnMarkArr)
					{
						$thisTermReportStudentIDArr = array_keys((array)$thisStudentReportColumnMarkArr);
						$thisNewStudentIDArr = array_diff($studentIDArr, $thisTermReportStudentIDArr);
						$thisNewStudentIDArr = array_values($thisNewStudentIDArr);
						$numNewStudentID = count($thisNewStudentIDArr);
						
						for ($k=0; $k<$numNewStudentID; $k++)
						{
							$thisStudentID = $thisNewStudentIDArr[$k];
							
							$existTermAverageTotalArr[$thisIndex][$thisReportColumnID][$thisStudentID]['GrandTotal'] = -1;
							$existTermAverageTotalArr[$thisIndex][$thisReportColumnID][$thisStudentID]['GrandAverage'] = -1;
							$existTermAverageTotalArr[$thisIndex][$thisReportColumnID][$thisStudentID]['GPA'] = -1;
							$existTermAverageTotalArr[$thisIndex][$thisReportColumnID][$thisStudentID]['OrderMeritClass'] = -1;
							$existTermAverageTotalArr[$thisIndex][$thisReportColumnID][$thisStudentID]['OrderMeritForm'] = -1;
							$existTermAverageTotalArr[$thisIndex][$thisReportColumnID][$thisStudentID]['ClassNoOfStudent'] = '';
							$existTermAverageTotalArr[$thisIndex][$thisReportColumnID][$thisStudentID]['FormNoOfStudent'] = '';
						}
					}
				}
			//}
			
			if ($calculationOrder == 1) {
				// Loop 2: Subjects
				$studentSubjectOverallList = array();
				foreach((array)$subjectList as $subjectID => $subjectInfo) {
					// $schemeInfo[0] is main info, [1] is ranges info
					$schemeInfo = $this->GET_GRADING_SCHEME_INFO($subjectInfo["schemeID"]);
					
					$schemeType = $schemeInfo[0]["SchemeType"];
					$passWording = $schemeInfo[0]["Pass"];
					$failWording = $schemeInfo[0]["Fail"];
					$subjectFullMark = $schemeInfo[0]["FullMark"];
					
					$scaleInput = $subjectInfo["scaleInput"];
					$scaleDisplay = $subjectInfo["scaleDisplay"];
					if ($eRCTemplateSetting['ConsolidatedReportUseGPAToCalculate']) {
						$scaleInput = 'M';
					}
					
					// get the grading scheme range info
					$schemeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($subjectInfo["schemeID"]);
					$schemeRangeIDGradeMap = array();
					if (sizeof($schemeRangeInfo) > 0) {
						for($m=0; $m<sizeof($schemeRangeInfo); $m++) {
							$schemeRangeIDGradeMap[$schemeRangeInfo[$m]["GradingSchemeRangeID"]] = $schemeRangeInfo[$m]["Grade"];
						}
					}
					
					// check if it is a parent subject
					//$CmpInfoArr = $this->CHECK_HAVE_CMP_SUBJECT($subjectID, $ClassLevelID, $ReportID);
					$CmpInfoArr = $SubjectCmpInfoAssoArr[$subjectID];
					$CmpSubjectArr = $CmpInfoArr[0];
					$CmpSubjectIDArr = $CmpInfoArr[1];
					$isCmpSubject = $CmpInfoArr[2];
					$isCalculateByCmpSub = $CmpInfoArr[3];
					
					## updated on 5 Feb by Ivan - support manual input Parent Subject Marks
					## �~�ؤ��� [CRM Ref No.: 2009-0204-1056]
					## Do not calculate the parent subjetc mark from component subjects if the parent mark is input manually
					if ($eRCTemplateSetting['ManualInputParentSubjectMark'])
					{
						$isCalculateByCmpSub = 0;
					}
					
					$totalReportColumnWeight = 0;
					$reportColumnWeight = array();
					$reportColumnWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, "SubjectID = ".$subjectID);
					if (sizeof($reportColumnWeightInfo) > 0) {
						for($x=0 ; $x<sizeof($reportColumnWeightInfo) ; $x++){
							if ($reportColumnWeightInfo[$x]['ReportColumnID'] != "" && $reportColumnWeightInfo[$x]['Weight'] != "") {
								$reportColumnWeight[$reportColumnWeightInfo[$x]['ReportColumnID']] = $reportColumnWeightInfo[$x]['Weight'];
								$totalReportColumnWeight += $reportColumnWeightInfo[$x]['Weight'];
							}
						}
					}
					
					// [2015-1104-1130-08164] Get Make-up Exam Result
					$MarkupResultAry = array();
					if($eRCTemplateSetting['ReportGeneration']['MarkupExamGrade'])
					{
						if(!empty($studentIDArr) && is_array($studentIDArr))
						{
							// Get Graduate Exam Result
							$SchoolType	= $this->GET_SCHOOL_LEVEL($classLevelId);
							$FormNumber = $this->GET_FORM_NUMBER($classLevelId, 1);
							$isSecondaryGraduate = $SchoolType=="S" && $FormNumber==6;
							if($isSecondaryGraduate) {
								$GradTermID = end($reportInvolveTerms);
								$GradReportID = $completedTermReportID[$GradTermID];
								if($GradReportID > 0) {
									$MarkupResultAry = $this->getMarks($GradReportID, "", $cons=" AND b.DisplayOrder = 1 ", $ParentSubjectOnly=1);
								}
							}
							// Get Mark-up Exam Result
							else {
								$MarkupResultAry = $this->GET_EXTRA_SUBJECT_INFO($studentIDArr, $subjectID, $ReportID);
							}
						}
					}
					
					################ Construct $AllColumnMarkArr #################
					$AllColumnMarkArr = array();
					$isCalculateByCmpSubOriginal = $isCalculateByCmpSub;
					
					// exist term mark in other report
					if (sizeof($existTermSubjectOverallArr) > 0) {
						for($a=0; $a<sizeof($studentIDArr); $a++) {
							for($w=0; $w<sizeof($existTermSubjectOverallArr); $w++) {
								if (isset($existTermSubjectOverallArr[$w][$studentIDArr[$a]][$subjectID])) {
									$tmpTermOverallDetail = $existTermSubjectOverallArr[$w][$studentIDArr[$a]][$subjectID];
									for ($p=0; $p<sizeof($tmpTermOverallDetail); $p++) {
										if ($tmpTermOverallDetail[$p]["Grade"] == "" && is_numeric($tmpTermOverallDetail[$p]["Mark"])) {
											$tmpTermOverallDetail[$p]["RawMark"] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $tmpTermOverallDetail[$p]["ReportColumnID"], $subjectID, $tmpTermOverallDetail[$p]["RawMark"]);
											$tmpTermOverallMark = $this->ROUND_MARK($tmpTermOverallDetail[$p]["RawMark"], "SubjectTotal");
											$AllColumnMarkArr[$studentIDArr[$a]][$tmpTermOverallDetail[$p]["ReportColumnID"]] = $tmpTermOverallMark;
										} else {
											//if ($scaleInput == "M" && $scaleDisplay =="G")
											//{
											//	$AllColumnMarkArr[$studentIDArr[$a]][$tmpTermOverallDetail[$p]["ReportColumnID"]] = $tmpTermOverallDetail[$p]["RawMark"];
											//}
											//else
											//{
												$AllColumnMarkArr[$studentIDArr[$a]][$tmpTermOverallDetail[$p]["ReportColumnID"]] = $tmpTermOverallDetail[$p]["Grade"];
											//}
										}
									}
								}
							}
						}
					}

					for($n=0; $n<sizeof($reportColumnData); $n++) {
						$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
						
						## update on 28 Nov 2008 by Ivan
						## For main subject with components
						## if the weight of all subject components are zero, use the term overall marks input by user instead of calculating from subject components
						if (count($CmpSubjectIDArr) > 0)
						{
							//$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $subjectID);
							if (!isset($isAllCmpZeroWeightAssoArr[$subjectID])) {
								$isAllCmpZeroWeightAssoArr[$subjectID] = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $subjectID);
							}
							$isAllCmpZeroWeightArr = $isAllCmpZeroWeightAssoArr[$subjectID];

                            // [2020-0717-1352-51206]
							// if ($isAllCmpZeroWeightArr[$reportColumnID]) {
							if ($isAllCmpZeroWeightArr[$reportColumnID] && !$parentSubjAlwaysCalculatedByCmp) {
							    $isCalculateByCmpSub = 0;
							} else {
						        $isCalculateByCmpSub = $isCalculateByCmpSubOriginal;
							}
						}
					    
						// get raw marks of all students in a class of this assessment
						if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub))
						{
							$marksheetScore = $this->GET_MARKSHEET_SCORE($studentIDArr, $subjectID, $reportColumnID);
						}
						else
						{
							$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID, "FullYear");
						}
						
						if (sizeof($marksheetScore) > 0) {
							foreach((array)$marksheetScore as $studentID => $scoreDetailInfo) {
								$scoreInfo = array();
								if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub)) {
									$scoreInfo = $scoreDetailInfo;
								} else {
									$scoreInfo = $this->CREATE_SCORE_INFO_ARRAY($scoreDetailInfo);
								}
								
								$AllColumnMarkArr[$studentID][$reportColumnID] = $scoreInfo['MarkNonNum'];
					
								if($scoreInfo['MarkType'] == "M" && $scoreInfo["MarkRaw"] != "-1") {
									$AllColumnMarkArr[$studentID][$reportColumnID] = $scoreInfo['MarkRaw'];	
								}
							}
						}
					}
					
					// fill in "Exempt" for any missing score, i.e. newly added student
					$calculationMark = array();
					for($n=0; $n<sizeof($reportColumnData); $n++) {
						$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
						for($g=0; $g<sizeof($studentIDArr); $g++) {
							if (!isset($AllColumnMarkArr[$studentIDArr[$g]][$reportColumnID]) || $AllColumnMarkArr[$studentIDArr[$g]][$reportColumnID]==='')
							{
								$AllColumnMarkArr[$studentIDArr[$g]][$reportColumnID] = "N.A.";
								$calculationMark[$studentIDArr[$g]][$reportColumnID] = "N.A.";
							}
						}
					}
					#################################################

					// Loop 3: ReportColumns (Terms)
					for($n=0; $n<sizeof($reportColumnData); $n++) {
						$reportColumnID = $reportColumnData[$n]["ReportColumnID"];

						// check if this column already have consolidated data, if yes, skip consolidation of this column
						// Don't skip if the existing report is a newer report
						//if (isset($completedTermReportID[$reportColumnTermMap[$reportColumnID]]) && $completedTermReportID[$reportColumnTermMap[$reportColumnID]] < $ReportID) {
						if (isset($completedTermReportID[$reportColumnTermMap[$reportColumnID]])) {
							// retrieve consolidated data first for later usage???
							continue;
						}

						## [2019-0704-0924-14207]
						## For main subject with components, if the weight of all subject components are zero, use the term overall marks input by user instead of calculating from subject components
						if (count($CmpSubjectIDArr) > 0)
						{
						    if (!isset($isAllCmpZeroWeightAssoArr[$subjectID])) {
						        $isAllCmpZeroWeightAssoArr[$subjectID] = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $subjectID);
						    }
					        $isAllCmpZeroWeightArr = $isAllCmpZeroWeightAssoArr[$subjectID];

                            // [2020-0717-1352-51206]
						    // if ($isAllCmpZeroWeightArr[$reportColumnID]) {
					        if ($isAllCmpZeroWeightArr[$reportColumnID] && !$parentSubjAlwaysCalculatedByCmp) {
					            $isCalculateByCmpSub = 0;
					        } else {
				                $isCalculateByCmpSub = $isCalculateByCmpSubOriginal;
					        }
						}
						
						// get raw marks of all students in a class of this assessment
						if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub))
							$marksheetScore = $this->GET_MARKSHEET_SCORE($studentIDArr, $subjectID, $reportColumnID);
						else
							$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID, "FullYear");
						
						$semester = $reportColumnData[$n]["SemesterNum"]; 
						$conAssessmentMarkValues = array();
						
						// Loop 4: Student's Score
						if (sizeof($marksheetScore) > 0) {
							foreach((array)$marksheetScore as $studentID => $scoreDetailInfo) {
								$consolidateMark = "";
								$scoreInfo = array();
								if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub)) {
									$scoreInfo = $scoreDetailInfo;
								} else {
									$scoreInfo = $this->CREATE_SCORE_INFO_ARRAY($scoreDetailInfo);
								}
								$rawMark = "";
								if ($scoreInfo["MarkType"] == "M" || $scoreInfo["MarkType"] == "G") {
									if ($scaleInput == "M" && $scaleDisplay == "M") {
										$rawMark = $scoreInfo["MarkRaw"];
										
										$consolidateMark = $this->ROUND_MARK($scoreInfo["MarkRaw"], "SubjectScore");
										// check if the consolidation mark need to be converted to weight mark
										if ($calculationSettings["UseWeightedMark"] == "1") {
											if ($isCmpSubject) {
												$consolidateMark = $this->CONVERT_CMP_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $consolidateMark, 1, $AllColumnMarkArr[$studentID]);
											} else {
												$consolidateMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $consolidateMark);
											}
										}
										$consolidateMark = $this->ROUND_MARK($consolidateMark, "SubjectScore");
										
										// Horizontal -> Vertical: need to store weighted mark to calculate subject overall
										$calculationMark[$studentID][$reportColumnID] = $this->ROUND_MARK($scoreInfo["MarkRaw"], "SubjectScore");
										if ($isCmpSubject) {
											$calculationMark[$studentID][$reportColumnID] = $this->CONVERT_CMP_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $consolidateMark, 1, $AllColumnMarkArr[$studentID]);
										} else {
											$calculationMark[$studentID][$reportColumnID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $calculationMark[$studentID][$reportColumnID], 1, $AllColumnMarkArr[$studentID]);
										}
										#$calculationMark[$studentID][$reportColumnID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $calculationMark[$studentID][$reportColumnID], 1, $AllColumnMarkArr[$studentID]);
									} else if ($scaleInput == "M" && $scaleDisplay == "G") {
										$rawMark = $scoreInfo["MarkRaw"];
										
										if ($eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade'])
										{
											$rawMarkToConvert = $this->ROUND_MARK($rawMark, "SubjectScore");
										}
										else
										{
											$rawMarkToConvert = $rawMark;
										}
							
										// convert to grade according to ranges
										$consolidateMark = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $rawMarkToConvert);
										
										// Horizontal -> Vertical: need to store weighted mark to calculate subject overall
										$calculationMark[$studentID][$reportColumnID] = $this->ROUND_MARK($scoreInfo["MarkRaw"], "SubjectScore");
										$calculationMark[$studentID][$reportColumnID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $calculationMark[$studentID][$reportColumnID], 1, $AllColumnMarkArr[$studentID]);
										
									} else if ($scaleInput == "G" && $scaleDisplay == "G") {
										$consolidateMark = $schemeRangeIDGradeMap[$scoreInfo["MarkNonNum"]];
										$calculationMark[$studentID][$reportColumnID] = "-";
									}
									
									// round off the weighted mark
									if ($scaleDisplay == "M" && is_numeric($consolidateMark)) {
										$consolidateMark = $this->ROUND_MARK($consolidateMark, "SubjectScore");
									}
									
									// consolidate the mark into DB (RC_REPORT_RESULT_SCORE)
									$mark = "";
									$grade = "";
									if ($scaleDisplay == "M" && is_numeric($consolidateMark)) {
										$mark = $consolidateMark;
									} else {
										$grade = $consolidateMark;
										$mark = $this->ROUND_MARK($rawMark, "SubjectScore");
									}
									
									$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '$mark', '$grade', '$rawMark', '', '$semester', '', '', NOW(), NOW())";
								} else if ($scoreInfo["MarkType"] == "PF") {
									$consolidateMark = ($scoreInfo["MarkNonNum"] == "P") ? $passWording : $failWording;
									$PassFailMark = $consolidateMark;
									$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$PassFailMark', '', '', '$semester', '', '', NOW(), NOW())";
									$calculationMark[$studentID][$reportColumnID] = "-";
								} else {
									$specialCase = $scoreInfo["MarkNonNum"];
									$calculationMark[$studentID][$reportColumnID] = $specialCase;
									$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$specialCase', '', '', '$semester', '', '', NOW(), NOW())";
									
									$specialCaseDifferentiate = array();
									// Build the $specialCaseDifferentiate Array for determining if new full mark is need to insert
									if ($schemeType == "PF") {
										$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $specialCase, $absentExemptSettings);
									} else {
										$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $specialCase, $absentExemptSettings);
									}
									
									if ($specialCaseDifferentiate[$specialCase]["isExFullMark"] == 1) {
										$success["INSERT_NEW_FULLMARK"] = $this->INSERT_NEW_FULLMARK(0, $ReportID, $studentID, $subjectID, $reportColumnID);
									}
								}
							} // End Loop 4: Student's Score
						}
						
						// insert the converted mark into DB
						if (sizeof($conAssessmentMarkValues) > 0) {
							$conAssessmentMarkValues = implode(",", $conAssessmentMarkValues);
							$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($conAssessmentMarkValues);
						}
					} // End Loop 3: ReportColumns
					
					// consolidate existing (entered by user directly) overall mark, for the case of input & display are both Grade
					$existOverallScoreInfo = $this->GET_MARKSHEET_OVERALL_SCORE($studentIDArr, $subjectID, $ReportID);
					
					$subjectOverallValues = array();
					$consolidatedSubjectOverallStudentList = array();
					if (sizeof($existOverallScoreInfo) > 0) {
						foreach((array)$existOverallScoreInfo as $markOverallStudentID => $markOverallInfo) {
							$consolidatedSubjectOverallStudentList[] = $markOverallStudentID;

							// [2016-0624-1118-33236] Set default value for $studentSubjectOverallList
							if (!$isCmpSubject) {
								$studentSubjectOverallList[$markOverallStudentID][$subjectID] = 0;
							}
							
							if ($markOverallInfo["MarkType"] == "M") {
								$thisMark = $markOverallInfo["MarkNonNum"];
								$thisGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $thisMark, $ReportID, $markOverallStudentID, $subjectID, $ClassLevelID, 0);
								$subjectOverallValues[] = "('$markOverallStudentID', '$subjectID', '$ReportID', '', '$thisMark', '$thisGrade', '$thisMark', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
								
								// [2016-0624-1118-33236]
								if (!$isCmpSubject) {
									$studentSubjectOverallList[$markOverallStudentID][$subjectID] = $thisMark;
								}
								
							}
							else if ($markOverallInfo["MarkType"] == "G") {
								$gradeToConsolidate = $schemeRangeIDGradeMap[$markOverallInfo["MarkNonNum"]];
								$subjectOverallValues[] = "('$markOverallStudentID', '$subjectID', '$ReportID', '', '', '$gradeToConsolidate', '', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
								
								// [2016-0624-1118-33236]
								if (!$isCmpSubject) {
									$studentSubjectOverallList[$markOverallStudentID][$subjectID] = $gradeToConsolidate;
								}
								
							} else if ($markOverallInfo["MarkType"] == "SC") {
								$subjectOverallValues[] = "('$markOverallStudentID', '$subjectID', '$ReportID', '', '', '".$markOverallInfo["MarkNonNum"]."', '', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
								
								// [2016-0624-1118-33236]
								if (!$isCmpSubject) {
									$studentSubjectOverallList[$markOverallStudentID][$subjectID] = $markOverallInfo["MarkNonNum"];
								}
								
							} else if ($markOverallInfo["MarkType"] == "PF") {
								$overallPFWording = ($markOverallInfo["MarkNonNum"] == "P")? $passWording : $failWording;
								$subjectOverallValues[] = "('$markOverallStudentID', '$subjectID', '$ReportID', '', '', '".$overallPFWording."', '', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
								
								// [2016-0624-1118-33236]
								if (!$isCmpSubject) {
									$studentSubjectOverallList[$markOverallStudentID][$subjectID] = $overallPFWording;
								}
                            }
						}
						
						if (sizeof($subjectOverallValues) > 0) {
							$subjectOverallValues = implode(",", $subjectOverallValues);
							$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($subjectOverallValues);
						}
					}

                    // [2020-0717-1352-51206]
					// [2019-0704-0924-14207]
                    // if (count($CmpSubjectIDArr) > 0 && in_array("1", $isAllCmpZeroWeightArr)) {
					if (count($CmpSubjectIDArr) > 0 && in_array("1", $isAllCmpZeroWeightArr) && !$parentSubjAlwaysCalculatedByCmp)
					{
					    $mixCalculateByCmp[$subjectID] = 1;
					}
					else
					{
					    $mixCalculateByCmp[$subjectID] = 0;
					}
					
					// calculate the subject overall mark of each student
					$subjectOverallValues = array();
					
					#######################################
					# Added on 20080812
					# Skip calculation of subject overall for Parent Subject since it will be calculated by an alternate way
					// if ($isCalculateByCmpSub) continue;
					if ($isCalculateByCmpSub && !$mixCalculateByCmp[$subjectID])     continue;
					#######################################
					
					for($a=0; $a<sizeof($studentIDArr); $a++) {
						// skip calculation of subject overall mark of this student since it already been entered by user directly and consolidated above
						if (in_array($studentIDArr[$a], $consolidatedSubjectOverallStudentList))
							continue;
						$subjectOverallMark = "";
						$subjectOverallMarkConverted = "";
						$excludeColumnWeight = 0;
						$excludeFullMark = 0;
						$subjectColumnTotalMark = 0;
						$specialCaseDifferentiate = array();
						
						if (sizeof($calculationMark) > 0) {
							if (isset($calculationMark[$studentIDArr[$a]])) {
								$markList = $calculationMark[$studentIDArr[$a]];
								
								// loop column by column and sum up the overall mark & exclude weight
								foreach((array)$markList as $markColumnID => $markColumn) {
									$subjectColumnTotalMark += $subjectFullMark*$reportColumnWeight[$markColumnID];
									if (is_numeric($markColumn)) {
										// if any of the assessment marks is numeric, make sure $subjectOverallMark is initiate to zero
										$subjectOverallMark = ($subjectOverallMark == "") ? 0 : $subjectOverallMark;
										$subjectOverallMark += $markColumn;
									} else {
										if ($schemeType == "PF") {
											$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $markColumn, $absentExemptSettings);
										} else {
											$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $markColumn, $absentExemptSettings);
										}
										if ($specialCaseDifferentiate[$markColumn]["isExFullMark"] == 1) {
											$excludeFullMark += $subjectFullMark * $reportColumnWeight[$markColumnID];
										} else if ($specialCaseDifferentiate[$markColumn]["isCount"] == 0) {
											// not in use since CONVERT_MARK_TO_WEIGHTED_MARK is used above
											$excludeColumnWeight += $reportColumnWeight[$markColumnID];
										}
									}
								}
							}
						}
						
						// if there exist consolidated term overall mark (in the term report)
						$CopySubjectOverallSpecialCase = '';
						for($w=0; $w<sizeof($existTermSubjectOverallArr); $w++) {
							if (isset($existTermSubjectOverallArr[$w][$studentIDArr[$a]][$subjectID])) {
								$tmpTermOverallDetail = $existTermSubjectOverallArr[$w][$studentIDArr[$a]][$subjectID];

								for ($p=0; $p<sizeof($tmpTermOverallDetail); $p++) {
									$thisReportColumnID = $tmpTermOverallDetail[$p]["ReportColumnID"];
									$subjectColumnTotalMark += $subjectFullMark * $reportColumnWeight[$thisReportColumnID];

									
									# updated on 22 Dec 2008 by Ivan
									# added the condition " if ($scaleInput=="M" && $scaleDisplay=="G") "
									# use "RawMark" instead of "Mark" for this case if there is raw marks
									if ($scaleInput=="M" && $scaleDisplay=="G" && $tmpTermOverallDetail[$p]["RawMark"]!="" && $this->Check_If_Grade_Is_SpecialCase($tmpTermOverallDetail[$p]["Grade"])==false)
									{
										$subjectOverallMark = ($subjectOverallMark == "") ? 0 : $subjectOverallMark;
										if ($calculationSettings["UseWeightedMark"] != "1") {
											$tmpTermOverallDetail[$p]["RawMark"] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $thisReportColumnID, $subjectID, $tmpTermOverallDetail[$p]["RawMark"], 1, $AllColumnMarkArr[$studentIDArr[$a]]);
										}
										else {
											//2013-0225-1227-23140: (Internal) Follow-up by karsonyam on 2013-07-19 17:42
											$tmpTermOverallDetail[$p]["RawMark"] = $tmpTermOverallDetail[$p]["Mark"];
										}
										
										//2012-1009-1115-03140 - cater differnt full mark for term report and consolidated report
										//$subjectOverallMark += $tmpTermOverallDetail[$p]["RawMark"];
										
										$tmpTermReportID = $termReportIdMappingAry[$thisReportColumnID];
										$tmpSubjectFullMarkTerm = $subjectFullMarkMappingAry[$tmpTermReportID][$subjectID];
										$tmpSubjectFullMarkConsolidate = $subjectFullMarkMappingAry[$ReportID][$subjectID];
										
										if ($tmpSubjectFullMarkTerm > 0) {
											$tmpMark = ($tmpTermOverallDetail[$p]["RawMark"] / $tmpSubjectFullMarkTerm) * $tmpSubjectFullMarkConsolidate;
										}
										else {
											$tmpMark = $tmpTermOverallDetail[$p]["RawMark"];
										}
										
										$subjectOverallMark += $tmpMark;
									}
									else
									{
										if ($this->Check_If_Grade_Is_SpecialCase($tmpTermOverallDetail[$p]["Grade"])==false && is_numeric($tmpTermOverallDetail[$p]["RawMark"])) {
											$subjectOverallMark = ($subjectOverallMark == "") ? 0 : $subjectOverallMark;
											// only convert the mark if the existing mark is previously consolidated as Raw mark
											if ($calculationSettings["UseWeightedMark"] != "1") {
												$tmpTermOverallDetail[$p]["RawMark"] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $thisReportColumnID, $subjectID, $tmpTermOverallDetail[$p]["RawMark"], 1, $AllColumnMarkArr[$studentIDArr[$a]]);
											}
											#$subjectOverallMark += $this->ROUND_MARK($tmpTermOverallDetail[$p]["Mark"], "SubjectTotal");
											
											//2012-1009-1115-03140 - cater differnt full mark for term report and consolidated report
											//$subjectOverallMark += $tmpTermOverallDetail[$p]["RawMark"];
											
											$tmpTermReportID = $termReportIdMappingAry[$thisReportColumnID];
											$tmpSubjectFullMarkTerm = $subjectFullMarkMappingAry[$tmpTermReportID][$subjectID];
											$tmpSubjectFullMarkConsolidate = $subjectFullMarkMappingAry[$ReportID][$subjectID];
											
											if ($tmpSubjectFullMarkTerm > 0) {
												$tmpMark = ($tmpTermOverallDetail[$p]["RawMark"] / $tmpSubjectFullMarkTerm) * $tmpSubjectFullMarkConsolidate;
											}
											else {
												$tmpMark = $tmpTermOverallDetail[$p]["RawMark"];
											}
											$subjectOverallMark += $tmpMark;
											
										} else {
											$thisTmpTermOverallGrade = $tmpTermOverallDetail[$p]["Grade"];
											
											if ($thisTmpTermOverallGrade == "abs") {
												$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $thisTmpTermOverallGrade, $absentExemptSettings);
											} else if (in_array($tmpTermOverallDetail[$p]["Grade"], $SpecialCaseArrH)) {
												$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $thisTmpTermOverallGrade, $absentExemptSettings);
											}
											if ($specialCaseDifferentiate[$thisTmpTermOverallGrade]["isExFullMark"] == 1) {
                                                // [2020-0724-1423-23073] convert excluded full marks to weighted if have excluded column weight
                                                if ($calculationSettings["UseWeightedMark"] != "1") {
                                                    $excludeFullMark += $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $thisReportColumnID, $subjectID, $subjectFullMark, 1, $AllColumnMarkArr[$studentIDArr[$a]]);
                                                } else {
                                                    $excludeFullMark += $subjectFullMark * $reportColumnWeight[$thisReportColumnID];
                                                }
											} else {
												$excludeColumnWeight += $reportColumnWeight[$thisReportColumnID];
											}

											if ($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']==true)
											{
												$thisReportColumnOrder = "";
												if(isset($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase_ColumnOrderArr'][$thisTmpTermOverallGrade]['W'])) {
													$thisReportColumnOrder = $thisReportColumnID? $this->retrieveReportTemplateColumnOrder($ReportID, $thisReportColumnID) : "O";
												}

												if ($this->Check_Copy_Special_Case($thisTmpTermOverallGrade, 'W', $studentIDArr[$a], $thisReportColumnOrder))
												{
												    // Not apply copying special case logic - yearly report + defined grading scheme input type (e.g. M / G)
												    $isDisabledCopyLogic = false;
												    if(
												        $scaleInput != '' &&
												        isset($eRCTemplateSetting['DisableCopySpecialCaseToOverallMarkLogic']) &&
												        isset($eRCTemplateSetting['DisableCopySpecialCaseToOverallMarkLogic']['W']) &&
												        isset($eRCTemplateSetting['DisableCopySpecialCaseToOverallMarkLogic']['W'][$scaleInput]) &&
												        !empty($eRCTemplateSetting['DisableCopySpecialCaseToOverallMarkLogic']['W'][$scaleInput])
												        )
												    {
												        $isDisabledCopyLogic = in_array($thisTmpTermOverallGrade, (array)$eRCTemplateSetting['DisableCopySpecialCaseToOverallMarkLogic']['W'][$scaleInput]);
												    }
												    
												    if($isDisabledCopyLogic) {
												        // do nothing
												    } else {
    													$CopySubjectOverallSpecialCase = $thisTmpTermOverallGrade;
    													break;
												    }
												}
											}
										}
									}
								}
							}
						}
						
						// in case of excluding full mark
						if ($excludeFullMark > 0) {
							#$newFullMark = $subjectFullMark - $excludeFullMark;
							if ($subjectColumnTotalMark!=0)
							{
								/*
								$newFullMark = $subjectFullMark - $excludeFullMark * ($subjectFullMark / $subjectColumnTotalMark);
								
								if (sizeof($existTermSubjectOverallArr) > 0)
									$newFullMark *= sizeof($existTermSubjectOverallArr);
								*/
								//$newFullMark = $subjectFullMark - $excludeFullMark;
								//$success["INSERT_NEW_FULLMARK"] = $this->INSERT_NEW_FULLMARK($newFullMark, $ReportID, $studentIDArr[$a], $subjectID);

								$newFullMark = $subjectColumnTotalMark - $excludeFullMark;
								// exclude full mark
								if ($newFullMark > 0 && is_numeric($subjectOverallMark))
								{
									//$subjectOverallMark = $subjectOverallMark * ($subjectFullMark / $newFullMark);
									$subjectOverallMark = $subjectOverallMark * ($subjectColumnTotalMark / $newFullMark);
								}
							}
						}
						
						// Copy back the special case to the overall mark
						if ($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']==true && $CopySubjectOverallSpecialCase != '') {
							$subjectOverallMark = $CopySubjectOverallSpecialCase;
						}
						
						
						// subject overall mark can be calculated as number
						if ($subjectOverallMark !== "" && is_numeric($subjectOverallMark)) {
							$subjectOverallMarkConverted = $subjectOverallMark;
							$subjectOverallMarkRaw = $subjectOverallMark;
							
							// convert to Grade
							if ($scaleInput == "M" && $scaleDisplay == "G") {
								if ($eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade'])
								{
									$subjectOverallMarkToConvert = $this->ROUND_MARK($subjectOverallMark, "SubjectTotal");
								}
								else
								{
									$subjectOverallMarkToConvert = $subjectOverallMark;
								}
								$subjectOverallMarkConverted = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $subjectOverallMarkToConvert);
							} else {
								// adjust the subject overall mark according to remaining weight
								if ($totalReportColumnWeight == $excludeColumnWeight) {
									$subjectOverallMarkConverted = "-";
									$subjectOverallMarkRaw = 0;
								} else {
									#$subjectOverallMark = $subjectOverallMark/($totalReportColumnWeight-$excludeColumnWeight);
									$subjectOverallMark = $this->ROUND_MARK($subjectOverallMark, "SubjectTotal");
								}
							}
						} else { // subject cannot be calculated as number
							if ($CopySubjectOverallSpecialCase != '') {
								$subjectOverallMarkConverted = $CopySubjectOverallSpecialCase;
							}
							else if (sizeof($specialCaseDifferentiate) == 1) {
								$onlySpecialCase = array_keys($specialCaseDifferentiate);
								$subjectOverallMarkConverted = $onlySpecialCase[0];
							} else {
								$subjectOverallMark = 0;
							}
							$subjectOverallMarkRaw = 0;
						}
						
						// for calculating average mark
						if (!$isCmpSubject) {
							$studentSubjectOverallList[$studentIDArr[$a]][$subjectID] = $subjectOverallMark;
						}
						
						if ($scaleDisplay == "M" && is_numeric($subjectOverallMark) && is_numeric($subjectOverallMarkConverted)) {
							//if (in_array($subjectCodeIDMap[$subjectID], $requirePassSubjectCodeID)) {
							//	$promoteCriteriaSubjectMark[$studentIDArr[$a]][$subjectCodeIDMap[$subjectID]] = $subjectOverallMark;
							//}
							
							// No need to check Make-up Exam as grade will not be stored
//							if($eRCTemplateSetting['ReportGeneration']['MarkupExamGrade'] && $subjectOverallMark < 60)
//							{
//								// do nothing	
//							}
							
							$subjectOverallValues[] = "('".$studentIDArr[$a]."', '$subjectID', '$ReportID', '', '$subjectOverallMark', '', '$subjectOverallMarkRaw', '1', 'F', '', '', NOW(), NOW())";
						} 
						else {
							// [2015-1104-1130-08164] Set Grade to D for students that pass the Make-up Exam
							if($eRCTemplateSetting['ReportGeneration']['MarkupExamGrade'] && ($subjectOverallMarkConverted=="F" || $subjectOverallMarkConverted=="+"))
							{
								if(!empty($MarkupResultAry))
								{
									if($isSecondaryGraduate) {
										$MarkupExamScore = $MarkupResultAry[$studentIDArr[$a]][$subjectID];
										if(!empty($MarkupExamScore)) {
											$MarkupExamScore = reset($MarkupExamScore);
											$MarkupExamScore = $MarkupExamScore["Mark"];
										}
									}
									else {
										$MarkupExamScore = $MarkupResultAry[$studentIDArr[$a]]["Info"];
									}
									
									// Set Grade to D
									if(!empty($MarkupExamScore) && $MarkupExamScore >= 60)
									{
										$subjectOverallMarkConverted = "D";
									}
								}
							}
							$subjectOverallValues[] = "('".$studentIDArr[$a]."', '$subjectID', '$ReportID', '', '$subjectOverallMarkRaw', '$subjectOverallMarkConverted', '$subjectOverallMarkRaw', '1', 'F', '', '', NOW(), NOW())";
						}
					}
					
					// consolidate the resulting subject overall mark
					if (sizeof($subjectOverallValues) > 0) {
						$subjectOverallValues = implode(",", $subjectOverallValues);
						$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($subjectOverallValues);
					}
				} // End Loop 2: Subjects
				
				// Added on 20080812
				// Calculate subject overall mark for parent subject using consolidated marks
				$parentSubjectOverallValues = array();
				foreach((array)$subjectList as $subjectID => $subjectInfo) {
					// check if it is a parent subject, if yes find info of its components subjects
					//$CmpInfoArr = $this->CHECK_HAVE_CMP_SUBJECT($subjectID, $ClassLevelID, $ReportID);
					$CmpInfoArr = $SubjectCmpInfoAssoArr[$subjectID];
					$CmpSubjectArr = $CmpInfoArr[0];
					$CmpSubjectIDArr = $CmpInfoArr[1];
					$isCmpSubject = $CmpInfoArr[2];
					$isCalculateByCmpSub = $CmpInfoArr[3];
					
					## updated on 5 Feb by Ivan - support manual input Parent Subject Marks
					## �~�ؤ��� [CRM Ref No.: 2009-0204-1056]
					## Do not calculate the parent subjetc mark from component subjects if the parent mark is input manually
					if ($eRCTemplateSetting['ManualInputParentSubjectMark'])
					{
						$isCalculateByCmpSub = 0;
					}
					
// 					if (!$isCalculateByCmpSub) continue;
					if (!$isCalculateByCmpSub || $mixCalculateByCmp[$subjectID]) continue;
					
					$scaleInput = $subjectInfo["scaleInput"];
					$scaleDisplay = $subjectInfo["scaleDisplay"];
					if ($eRCTemplateSetting['ConsolidatedReportUseGPAToCalculate']) {
						$scaleInput = 'M';
					}
					
					// [2015-1104-1130-08164] Get Markup Exam Score
					$MarkupResultAry = array();
					if($eRCTemplateSetting['ReportGeneration']['MarkupExamGrade'])
					{
						if(!empty($studentIDArr) && is_array($studentIDArr))
						{
							// Get Graduate Exam Result
							$SchoolType	= $this->GET_SCHOOL_LEVEL($classLevelId);
							$FormNumber = $this->GET_FORM_NUMBER($classLevelId, 1);
							$isSecondaryGraduate = $SchoolType=="S" && $FormNumber==6;
							if($isSecondaryGraduate) {
								$GradTermID = end($reportInvolveTerms);
								$GradReportID = $completedTermReportID[$GradTermID];
								if($GradReportID > 0) {
									$MarkupResultAry = $this->getMarks($GradReportID, "", $cons=" AND b.DisplayOrder = 1 ", $ParentSubjectOnly=1);
								}
							}
							// Get Mark-up Exam Result
							else {
								$MarkupResultAry = $this->GET_EXTRA_SUBJECT_INFO($studentIDArr, $subjectID, $ReportID);
							}
						}
					}
					
					if ($eRCTemplateSetting['ParentSubjectCalculationMethod'] == 2)
					{
						// Calculate Parent Overall Mark Honrizontally
						$tmpMarkArr = array();
						$tmpColumnWeightArr = array();
						$tmpTotalColumnWeight = 0;
						$numOfStudent = count($studentIDArr);
						$numOfColumn = count($reportColumnData);
						
						// Get Column Weight
						for($k=0; $k<$numOfColumn; $k++) 
						{
							$thisReportColumnID = $reportColumnData[$k]['ReportColumnID'];
							
							$OtherCondition = " SubjectID = '".$subjectID."' AND ReportColumnID = '".$thisReportColumnID."' ";
							$reportColumnWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
							$tmpColumnWeightArr[$thisReportColumnID] = $reportColumnWeightInfo[0]['Weight'];
							$tmpTotalColumnWeight += $reportColumnWeightInfo[0]['Weight'];
						}
						
						// Build Student Mark Array
						for ($j=0; $j<$numOfStudent; $j++)
						{
							$thisStudentID = $studentIDArr[$j];
							
							for($k=0; $k<$numOfColumn; $k++) 
							{
								$thisReportColumnID = $reportColumnData[$k]['ReportColumnID'];
								
								$tmpMarkArr[$thisStudentID][$subjectID][$thisReportColumnID] = $allSubjectColumnMarkList[$thisStudentID][$subjectID][$thisReportColumnID];
							}
						}
						
						$success["CALCULATE_VERTICAL_SUBJECT_OVERALL"] = $this->CALCULATE_VERTICAL_SUBJECT_OVERALL($tmpMarkArr, $tmpColumnWeightArr, $ReportID, $ClassLevelID, $absentExemptSettings, $reportBasicInfo, $tmpTotalColumnWeight);
						
						// 2013-0711-1349-57140 - missed the parent subject mark in the grand mark calculation
						$RC_REPORT_RESULT_SCORE = $this->DBName.".RC_REPORT_RESULT_SCORE";
						$sql = "Select StudentID, RawMark, Grade From $RC_REPORT_RESULT_SCORE Where ReportID = '".$ReportID."' And SubjectID = '".$subjectID."' And ReportColumnID = '0' And StudentID IN ('".implode("','", (array)$studentIDArr)."')";
						$resultAry = $this->returnResultSet($sql);
						$numOfResult = count((array)$resultAry);
						
						for ($j=0; $j<$numOfResult; $j++) {
							$_studentId = $resultAry[$j]['StudentID'];
							$_rawMark = $resultAry[$j]['RawMark'];
							$_grade = $resultAry[$j]['Grade'];
							
							if ($this->Check_If_Grade_Is_SpecialCase($_grade)) {
								$_targetMark = $_grade; 
							}
							else {
								$_targetMark = $_rawMark;
							}
							$studentSubjectOverallList[$_studentId][$subjectID] = $_targetMark;
						}
					}
					else
					{
						// Calculate the subject overall mark of parent subject using subject overall mark of component subjects
						$parentSubjectOverallMark = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, 0, "", 1, 1);
					}

					
					foreach((array)$parentSubjectOverallMark as $studentID => $parentSubjectOverall) {
						
						// subject overall mark can be calculated as number
						if ($scaleInput == "M" && is_numeric($parentSubjectOverall)) {
							$parentSubjectOverallConverted = $parentSubjectOverall;
							$parentSubjectOverallRaw = $parentSubjectOverall;
							
							if ( ($scaleInput == "M") && ($scaleDisplay == "G") )
							{
								if ($eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade'])
								{
									$parentSubjectOverallToConvert = $this->ROUND_MARK($parentSubjectOverall, "SubjectTotal");
								}
								else
								{
									$parentSubjectOverallToConvert = $parentSubjectOverall;
								}
								$parentSubjectOverallConverted = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $parentSubjectOverallToConvert);
							} else {
								$parentSubjectOverall = $this->ROUND_MARK($parentSubjectOverall, "SubjectTotal");
							}
						}
						else
						{
							$parentSubjectOverallConverted = $parentSubjectOverall;
							$parentSubjectOverallRaw = 0;
						}
					
						$studentSubjectOverallList[$studentID][$subjectID] = $parentSubjectOverall;
						
						if ($scaleDisplay == "M" && is_numeric($parentSubjectOverall) && is_numeric($parentSubjectOverallConverted))
							$parentSubjectOverallValues[] = "('$studentID', '$subjectID', '$ReportID', '', '$parentSubjectOverall', '', '$parentSubjectOverallRaw', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
						else {
							// [2015-1104-1130-08164] Set Grade to D for students that pass the Make-up Exam
							if($eRCTemplateSetting['ReportGeneration']['MarkupExamGrade'] && ($parentSubjectOverallConverted=="F" || $parentSubjectOverallConverted=="+"))
							{
								if(!empty($MarkupResultAry))
								{
									if($isSecondaryGraduate) {
										$MarkupExamScore = $MarkupResultAry[$studentIDArr[$a]][$subjectID];
										if(!empty($MarkupExamScore)) {
											$MarkupExamScore = reset($MarkupExamScore);
											$MarkupExamScore = $MarkupExamScore["Mark"];
										}
									}
									else {
										$MarkupExamScore = $MarkupResultAry[$studentIDArr[$a]]["Info"];
									}
									
									// Set Grade to D
									if(!empty($MarkupExamScore) && $MarkupExamScore >= 60)
									{
										$parentSubjectOverallConverted = "D";
									}
								}
							}
							$parentSubjectOverallValues[] = "('$studentID', '$subjectID', '$ReportID', '', '$parentSubjectOverallRaw', '$parentSubjectOverallConverted', '$parentSubjectOverallRaw', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
						}
					}
				}
				
				// consolidate the resulting subject overall mark
				if (sizeof($parentSubjectOverallValues) > 0) {
					$parentSubjectOverallValues = implode(",", $parentSubjectOverallValues);
					$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($parentSubjectOverallValues);
				}
				// Added on 20080812 Ended
				
				
				
				// calculate average mark
				// for Horizontal to Vertical, only average mark of subject overall need to be calculated
				$reportAverageTotal = array();
				$grandValues = array();
				// order, gpa yet to be done....
				
				foreach((array)$studentSubjectOverallList as $studentID => $subjectOverallList) {
					$reportAverageTotal[$studentID] = $this->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $studentID, $subjectOverallList, '', 1);
					
					# Round Grand Marks
					$thisGrandMarksArr = $this->ROUND_GRAND_MARKS($reportAverageTotal[$studentID]);
					$tmpTotal = $thisGrandMarksArr["GrandTotal"];
					$tmpAverage = $thisGrandMarksArr["GrandAverage"];
					$tmpGpa = $thisGrandMarksArr["GradePointAverage"];
						
					// check if the student can be promoted based on pre-set criteria
					//if (isset($promoteCriteriaSubjectMark[$studentID])) {
					//	$isPromoted = $this->IS_PROMOTED($classLevelName, $tmpAverage, $promoteCriteriaSubjectMark[$studentID]);
					//} else {
					//	$isPromoted = $this->IS_PROMOTED($classLevelName, $tmpAverage);
					//}
					//$newClassName = ($isPromoted == 1) ? $nextPromoteClassLevel : $classLevelName;
					$grandValues[] = "('$studentID', '$ReportID', '', '$tmpTotal', '$tmpAverage', '$tmpGpa', '', '', 
										'".$this->Get_Safe_Sql_Query($newClassName)."', 
										'F', '', NOW(), NOW())";
				}
				// consolidate grand total & grand average
				// no value for ReportColumnID
				$grandValues = implode(",", $grandValues);
				$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($grandValues);
				
			// Vertical > Horizontal: All subject have a common weight (as a preview only, since these weight is actually for average mark)
			} else if ($calculationOrder == 2) {
				// get the weight (NOT really meaningful for calculating subject overall mark)
				$totalReportColumnWeight = 0;
				
				for($j=0; $j<sizeof($reportColumnData); $j++) {
					$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$reportColumnData[$j]["ReportColumnID"];
					$reportColumnWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
					$reportColumnWeight[$reportColumnWeightInfo[0]["ReportColumnID"]] = $reportColumnWeightInfo[0]['Weight'];
					$totalReportColumnWeight += $reportColumnWeightInfo[0]['Weight'];
				}
				
				$grandAverageTotalList = array();
				
				// for storing subject marks of different columns used to calculate subject overall
				// for reference only since grand total will not be calculated by subject overall
				$subjectColumnMarkList = array();
				if (sizeof($allSubjectColumnMarkList) > 0) {
					foreach((array)$allSubjectColumnMarkList as $tmpStudentID => $tmpSubjectInfo) {
						// Only using mark of student in this class
						if (in_array($tmpStudentID, $studentIDArr)) {
							$subjectColumnMarkList[$tmpStudentID] = $tmpSubjectInfo;
						}
					}
				}
				
				$grandTotalWeightArr = array();
				$grandExcludeWeightArr = array();
				// Loop 2: ReportColumns
				for($j=0; $j<sizeof($reportColumnData); $j++) {
					$reportColumnID = $reportColumnData[$j]["ReportColumnID"];
					
					$calculationMark = array();
					if (!isset($completedTermReportID[$reportColumnTermMap[$reportColumnID]])) {
						// Loop 3: Subjects
						foreach((array)$subjectList as $subjectID => $subjectInfo) {
							// $schemeInfo[0] is main info, [1] is ranges info
							$schemeInfo = $this->GET_GRADING_SCHEME_INFO($subjectInfo["schemeID"]);
							
							$schemeType = $schemeInfo[0]["SchemeType"];
							$passWording = $schemeInfo[0]["Pass"];
							$failWording = $schemeInfo[0]["Fail"];
							
							$scaleInput = $subjectInfo["scaleInput"];
							$scaleDisplay = $subjectInfo["scaleDisplay"];
							
							// get the grading scheme range info
							$schemeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($subjectInfo["schemeID"]);
							for($m=0; $m<sizeof($schemeRangeInfo); $m++) {
								$schemeRangeIDGradeMap[$schemeRangeInfo[$m]["GradingSchemeRangeID"]] = $schemeRangeInfo[$m]["Grade"];
							}
							
							// check if it is a parent subject
							//$CmpInfoArr = $this->CHECK_HAVE_CMP_SUBJECT($subjectID, $ClassLevelID, $ReportID);
							$CmpInfoArr = $SubjectCmpInfoAssoArr[$subjectID];
							$CmpSubjectArr = $CmpInfoArr[0];
							$CmpSubjectIDArr = $CmpInfoArr[1];
							$isCmpSubject = $CmpInfoArr[2];
							$isCalculateByCmpSub = $CmpInfoArr[3];
							
							## updated on 5 Feb by Ivan - support manual input Parent Subject Marks
							## �~�ؤ��� [CRM Ref No.: 2009-0204-1056]
							## Do not calculate the parent subjetc mark from component subjects if the parent mark is input manually
							if ($eRCTemplateSetting['ManualInputParentSubjectMark'])
							{
								$isCalculateByCmpSub = 0;
							}
							
							// get raw marks of all students in a class of this assessment
							if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub))
								$marksheetScore = $this->GET_MARKSHEET_SCORE($studentIDArr, $subjectID, $reportColumnID);
							else
								$marksheetScore = $this->GET_PARENT_SUBJECT_MARKSHEET_SCORE($studentIDArr, $subjectID, $CmpSubjectIDArr, $ReportID, $reportColumnID, "FullYear");
							
							// Loop 4: Student's Score
							$specialCaseDifferentiate = array();
							$conAssessmentMarkValues = array();
							
							foreach((array)$marksheetScore as $studentID => $scoreDetailInfo) {
								$AllColumnMarkArr = array();
								$consolidateMark = "";
								$scoreInfo = array();
								if (count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub)) {
									$scoreInfo = $scoreDetailInfo;
								} else {
									$scoreInfo = $this->CREATE_SCORE_INFO_ARRAY($scoreDetailInfo);
								}
								
								$rawMark = "";
								if ($scoreInfo["MarkType"] == "M" || $scoreInfo["MarkType"] == "G") {
									if ($scaleInput == "M" && $scaleDisplay == "M") {
										$rawMark = $scoreInfo["MarkRaw"];
										
										$consolidateMark = $this->ROUND_MARK($scoreInfo["MarkRaw"], "SubjectScore");
										// check if the consolidation mark need to be converted to weight mark
										if ($calculationSettings["UseWeightedMark"] == "1") {
											if ($isCmpSubject) {
												$consolidateMark = $this->CONVERT_CMP_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $consolidateMark, 2);
											} else {
												$consolidateMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $consolidateMark, 2);
											}
										}
										$consolidateMark = $this->ROUND_MARK($consolidateMark, "SubjectScore");
										
										// Vertical -> Horizontal: need to store weighted mark to calculate subject overall
										$calculationMark[$studentID][$subjectID] = $this->ROUND_MARK($scoreInfo["MarkRaw"], "SubjectScore");
										#$calculationMark[$studentID][$subjectID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $calculationMark[$studentID][$subjectID], 2);
									} else if ($scaleInput == "M" && $scaleDisplay == "G") {
										$rawMark = $scoreInfo["MarkRaw"];
										
										if ($eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade'])
										{
											$rawMarkToConvert = $this->ROUND_MARK($rawMark, "SubjectScore");
										}
										else
										{
											$rawMarkToConvert = $rawMark;
										}
								
										// convert to grade according to ranges
										$consolidateMark = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $rawMarkToConvert);
										
										// Vertical -> Horizontal: need to store weighted mark to calculate subject overall
										$calculationMark[$studentID][$subjectID] = $this->ROUND_MARK($scoreInfo["MarkRaw"], "SubjectScore");
										#$calculationMark[$studentID][$subjectID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $calculationMark[$studentID][$subjectID], 2);
									} else if ($scaleInput == "G" && $scaleDisplay == "G") {
										$consolidateMark = $schemeRangeIDGradeMap[$scoreInfo["MarkNonNum"]];
										$calculationMark[$studentID][$subjectID] = "-";
									}
									
									// round off the weighted mark
									if (is_numeric($consolidateMark)) {
										$consolidateMark = $this->ROUND_MARK($consolidateMark, "SubjectScore");
									}
									
									// consolidate the mark into DB (RC_REPORT_RESULT_SCORE)
									$mark = "";
									$grade = "";
									if (is_numeric($consolidateMark)) {
										$mark = $consolidateMark;
									} else {
										$grade = $consolidateMark;
										$mark = $this->ROUND_MARK($rawMark, "SubjectScore");
									}
									
									$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '$mark', '$grade', '$rawMark', '', '$semester', '', '', NOW(), NOW())";
								} else if ($scoreInfo["MarkType"] == "PF") {
									$consolidateMark = ($scoreInfo["MarkNonNum"] == "P") ? $passWording : $failWording;
									$PassFailMark = $consolidateMark;
									$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$PassFailMark', '', '', '$semester', '', '', NOW(), NOW())";
									$calculationMark[$studentID][$subjectID] = "-";
								} else {
									$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $scoreInfo["MarkNonNum"], $absentExemptSettings);
									$specialCase = $scoreInfo["MarkNonNum"];
									$conAssessmentMarkValues[] = "('$studentID', '$subjectID', '$ReportID', '$reportColumnID', '', '$specialCase', '', '', '$semester', '', '', NOW(), NOW())";
									
									if ($specialCaseDifferentiate[$specialCase]["isExFullMark"] == 1) {
										$calculationMark[$studentID][$subjectID] = "*";
									} else if ($specialCaseDifferentiate[$specialCase]["isCount"] == 0) {
										$calculationMark[$studentID][$subjectID] = "/";
									} else {
										$calculationMark[$studentID][$subjectID] = $specialCaseDifferentiate[$specialCase]["Value"];
									}
								}
							} // End Loop 4
							
							// insert the converted mark into DB
							if (sizeof($conAssessmentMarkValues) > 0) {
								$conAssessmentMarkValues = implode(",", $conAssessmentMarkValues);
								$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($conAssessmentMarkValues);
							}
						} // End Loop 3: Subjects
					}
					
					// calculate the term average mark (vertically across subjects) of each student
					$termAverageValues = array();
					$termAverageMark = "";
					
					if (sizeof($calculationMark) > 0) {
						foreach((array)$calculationMark as $markStudentID => $markList) {
							$termAverageMark = $this->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $markStudentID, $markList, $reportColumnID);
							
							foreach((array)$markList as $markSubjectID => $markSubject) {
								$subjectColumnMarkList[$markStudentID][$markSubjectID][$reportColumnID] = $markSubject;
							}
							
							# Round Grand Marks
							$thisGrandMarksArr = $this->ROUND_GRAND_MARKS($termAverageMark);
							$tmpTotal = $thisGrandMarksArr["GrandTotal"];
							$tmpAverage = $thisGrandMarksArr["GrandAverage"];
							$tmpGpa = $thisGrandMarksArr["GradePointAverage"];
							
							
							if (!isset($grandAverageTotalList[$markStudentID]["GrandTotal"]))
								$grandAverageTotalList[$markStudentID]["GrandTotal"] = 0;
							if (!isset($grandAverageTotalList[$markStudentID]["GrandAverage"]))
								$grandAverageTotalList[$markStudentID]["GrandAverage"] = 0;
							if (!isset($grandAverageTotalList[$markStudentID]["GradePointAverage"]))
								$grandAverageTotalList[$markStudentID]["GradePointAverage"] = 0;
							
							// didn't consider the extreme case with no average mark can be calculated
							$grandAverageTotalList[$markStudentID]["GrandTotal"] += $tmpTotal * $reportColumnWeight[$reportColumnID];
							$grandAverageTotalList[$markStudentID]["GrandAverage"] += $tmpAverage * $reportColumnWeight[$reportColumnID];
							$grandAverageTotalList[$markStudentID]["GradePointAverage"] += $tmpGpa * $reportColumnWeight[$reportColumnID];
							
							$termAverageValues[] = "('$markStudentID', '$ReportID', '$reportColumnID', '$tmpTotal', '$tmpAverage', '$tmpGpa', '', '', '', '".$reportColumnTermMap[$reportColumnID]."', '', NOW(), NOW())";
						}
					}
					
					// exist term average mark
					for($y=0; $y<sizeof($existTermAverageTotalArr); $y++) {
						if (isset($existTermAverageTotalArr[$y][$reportColumnID])) {
							$existStudentAverageTotalArr = $existTermAverageTotalArr[$y][$reportColumnID];
							
							foreach((array)$existStudentAverageTotalArr as $markStudentID => $markAverageTotalInfo) {
								if (!isset($grandAverageTotalList[$markStudentID]["GrandTotal"]))
									$grandAverageTotalList[$markStudentID]["GrandTotal"] = 0;
								if (!isset($grandAverageTotalList[$markStudentID]["GrandAverage"]))
									$grandAverageTotalList[$markStudentID]["GrandAverage"] = 0;
								if (!isset($grandAverageTotalList[$markStudentID]["GradePointAverage"]))
									$grandAverageTotalList[$markStudentID]["GradePointAverage"] = 0;
									
								### added on 17 Feb 2009 by Ivan for Tong Nam ���Z�� handling
								if (sizeof($completedTermReportID) > 0) {
									$reportCounter = 0;
									// retrieve existing term marks & consolidate them
									foreach((array)$completedTermReportID as $completedSemester => $completedReportID) {
										if ($reportCounter == $y)
										{
											$thisStudentTermFullMark = $this->returnGrandTotalFullMark($completedReportID , $markStudentID, 1);
											break;
										}
										else
										{
											$reportCounter++;
										}
									}
								}
								
								$grandTotalWeightArr[$markStudentID] += $reportColumnWeight[$reportColumnID];
								
								if ($markAverageTotalInfo["GrandTotal"]==-1 || $markAverageTotalInfo["GrandAverage"]==-1 || $markAverageTotalInfo["GPA"]==-1)
								{
									if ($eRCTemplateSetting['NoGrandCalculationIfOneOfTheColumnGrandIsExcluded'])
										(array)$NoGrandMarkArr[] = $markStudentID;
										
									$grandExcludeWeightArr[$markStudentID] += $reportColumnWeight[$reportColumnID];
								}
								# $thisStudentTermFullMark = 0 means all subjects exempted in the term report (i.e. ���Z��)
								else if ($thisStudentTermFullMark == 0)
								{
									$grandExcludeWeightArr[$markStudentID] += $reportColumnWeight[$reportColumnID];
								}
								### end of Tong Nam ���Z�� handling
								else
								{
									$grandAverageTotalList[$markStudentID]["GrandTotal"] += $markAverageTotalInfo["GrandTotal"] * $reportColumnWeight[$reportColumnID];
									$grandAverageTotalList[$markStudentID]["GrandAverage"] += $markAverageTotalInfo["GrandAverage"] * $reportColumnWeight[$reportColumnID];
									$grandAverageTotalList[$markStudentID]["GradePointAverage"] += $markAverageTotalInfo["GPA"] * $reportColumnWeight[$reportColumnID];
								}
							}
						}
					}
					
					// consolidate the term average & total mark
					if (sizeof($termAverageValues) > 0) {
						$termAverageValues = implode(",", $termAverageValues);
						$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($termAverageValues);
					}
				} // End Loop 2: ReportColumns
				
				
				
				### added on 17 Feb 2009 by Ivan for Tong Nam ���Z�� handling
				foreach((array)$grandExcludeWeightArr as $grandExcludeStudentID => $thisGrandExcludeWeight) {
					$thisGrandTotalWeight = $grandTotalWeightArr[$grandExcludeStudentID];
					
//					if (($thisGrandTotalWeight - $thisGrandExcludeWeight) == 0) {
//						continue;
//					}
					if (($thisGrandTotalWeight - $thisGrandExcludeWeight) == 0) {
						$grandAverageTotalList[$grandExcludeStudentID]["GrandTotal"] = -1;
						$grandAverageTotalList[$grandExcludeStudentID]["GrandAverage"] = -1;
						$grandAverageTotalList[$grandExcludeStudentID]["GradePointAverage"] = -1;
					}
					else {
						$grandAverageTotalList[$grandExcludeStudentID]["GrandTotal"] = $grandAverageTotalList[$grandExcludeStudentID]["GrandTotal"] / ($thisGrandTotalWeight - $thisGrandExcludeWeight);
						$grandAverageTotalList[$grandExcludeStudentID]["GrandAverage"] = $grandAverageTotalList[$grandExcludeStudentID]["GrandAverage"] / ($thisGrandTotalWeight - $thisGrandExcludeWeight);
						$grandAverageTotalList[$grandExcludeStudentID]["GradePointAverage"] = $grandAverageTotalList[$grandExcludeStudentID]["GradePointAverage"] / ($thisGrandTotalWeight - $thisGrandExcludeWeight);
					}
						
//					$grandAverageTotalList[$grandExcludeStudentID]["GrandTotal"] = $grandAverageTotalList[$grandExcludeStudentID]["GrandTotal"] / ($thisGrandTotalWeight - $thisGrandExcludeWeight);
//					$grandAverageTotalList[$grandExcludeStudentID]["GrandAverage"] = $grandAverageTotalList[$grandExcludeStudentID]["GrandAverage"] / ($thisGrandTotalWeight - $thisGrandExcludeWeight);
//					$grandAverageTotalList[$grandExcludeStudentID]["GradePointAverage"] = $grandAverageTotalList[$grandExcludeStudentID]["GradePointAverage"] / ($thisGrandTotalWeight - $thisGrandExcludeWeight);
				}
				### end of Tong Nam ���Z�� handling
				
				foreach((array)$NoGrandMarkArr as $key => $thisNoGrandStudentID)
				{
					$grandAverageTotalList[$thisNoGrandStudentID]["GrandTotal"] = -1;
					$grandAverageTotalList[$thisNoGrandStudentID]["GrandAverage"] = -1;
					$grandAverageTotalList[$thisNoGrandStudentID]["GradePointAverage"] = -1;
				}
				
				
				## For Escola Tong Nam Only
				# If the report includes 4 terms, then use the mark of the consolidated reports instead of the calculated marks from the 4 terms
				# To calculate the overall mark of each subjects
				if ( ($this->Get_Num_Of_Column($ReportID) == 4) && ($eRCTemplateSetting['UseConsolidatedReportOverallScoreToCalculateSubjectOverall']) )
				{
					$subjectColumnMarkList = array();
					$reportColumnWeight = array();
					
					# Column Weight
					$ClassLevel = $this->returnClassLevel($ClassLevelID);
					$SchoolLevel = strtoupper(substr($ClassLevel,0,1));
					
					if ($SchoolLevel == "F")
					{
						$reportColumnWeight = $eRCTemplateSetting['UseConsolidatedReportOverallScoreToCalculateSubjectOverall_ColumnWeight_Secondary'];
						$totalReportColumnWeight = array_sum($eRCTemplateSetting['UseConsolidatedReportOverallScoreToCalculateSubjectOverall_ColumnWeight_Secondary']);
					}
					else
					{
						$reportColumnWeight = $eRCTemplateSetting['UseConsolidatedReportOverallScoreToCalculateSubjectOverall_ColumnWeight_Others'];
						$totalReportColumnWeight = array_sum($eRCTemplateSetting['UseConsolidatedReportOverallScoreToCalculateSubjectOverall_ColumnWeight_Others']);
					}
					
					# Get others consolidated reportID
					$consolidatedReportIDArr = $this->Get_Consolidated_Report_List($ClassLevelID, $ReportID);
					
					# Get marks for all subjects of the consolidated report
					foreach((array)$consolidatedReportIDArr as $index => $thisReportID)
					{
						for ($s=0; $s<sizeof($studentIDArr); $s++)
						{
							$thisStudentID = $studentIDArr[$s];
							$thisMarksArr = $this->getMarks($thisReportID, $thisStudentID, "", $ParentSubjectOnly=0, $includeAdjustedMarks=1, $OrderBy='', '', 0);
							
							# ���Z�� => no stored mark => will not loop the below foreach => set as exempted by default
							foreach((array)$subjectList as $thisSubjectID => $dataArr)
							{
								$subjectColumnMarkList[$thisStudentID][$thisSubjectID][$index] = "/";
							}
							
							foreach((array)$thisMarksArr as $thisSubjectID => $data)
							{
								$thisMark = $data[0]['Mark'];
								
								list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $thisSubjectID, $thisMark, $data[0]['Grade']);
								
								if ($thisMark == "")
									$thisMark = "/";
									
								if ($thisMark == "/")
									$thisMark = "*";
								
								$subjectColumnMarkList[$thisStudentID][$thisSubjectID][$index] = $thisMark;
							}
						}
					}
				}
				
				# calculate the overall of each subject (Honrizontal Calculation)
				$success["CALCULATE_VERTICAL_SUBJECT_OVERALL"] = $this->CALCULATE_VERTICAL_SUBJECT_OVERALL($subjectColumnMarkList, $reportColumnWeight, $ReportID, $ClassLevelID, $absentExemptSettings, $reportBasicInfo, $totalReportColumnWeight);
				
				// 2011-0207-1031-12073 - Ignore the Display Grade Settings
				//if ($eRCTemplateSetting['UseOverallColumnToCalculateGrandMarksForConsolidatedReport']==false || $this->isAllSubjectsDisplayGrade($ClassLevelID, $ReportID)==true)
				if ($eRCTemplateSetting['UseOverallColumnToCalculateGrandMarksForConsolidatedReport']==false)
				{
					# Normal Calculation
					// consolidate the grand average & total mark
					if (sizeof($grandAverageTotalList) > 0) {
						$grandAverageValues = array();
						foreach((array)$grandAverageTotalList as $grandStudentID => $grandList) {
							// round grand total according to the same setting of average???
							if (!in_array($grandStudentID, $studentIDArr))
								continue;
							
							if ($eRCTemplateSetting['CustomizedConsolidatedSubjectTotalRounding'])
							{
								$tmpTotal = my_round($grandList["GrandTotal"], $eRCTemplateSetting['ConsolidatedSubjectTotalRounding']);
							}
							else
							{
								$tmpTotal = $this->ROUND_MARK($grandList["GrandTotal"], "GrandTotal");
							}
							
							$tmpAverage = $this->ROUND_MARK($grandList["GrandAverage"], "GrandAverage");
							$tmpGpa = $this->ROUND_MARK($grandList["GradePointAverage"], "GrandAverage");
							$grandAverageValues[] = "('$grandStudentID', '$ReportID', '', '$tmpTotal', '$tmpAverage', '$tmpGpa', '', '','', 'F', '', NOW(), NOW())";
						}
						$grandAverageValues = implode(",", $grandAverageValues);
						$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($grandAverageValues);
					}
				}
				else
				{
					## get all the calculated subject overall marks and use the "honrizontal-vertical" method to calculate Grand marks
					$studentSubjectOverallList = array();
					$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
					if (sizeof($MainSubjectArray) > 0)
						foreach((array)$MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
					
					for ($s=0; $s<sizeof($studentIDArr); $s++)
					{
						$thisStudentID = $studentIDArr[$s];
						
						$thisMarksArr = $this->getMarks($ReportID, $thisStudentID, "", $ParentSubjectOnly=0, $includeAdjustedMarks=1, $OrderBy='', '', 0);

						foreach((array)$thisMarksArr as $thisSubjectID => $data)
						{
							if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)
								$isSub=1;
							else
								$isSub=0;
								
							if ($isSub) continue;
							
							//$thisMark = $data[0]['Mark'];
							
							if ($this->Check_If_Grade_Is_SpecialCase($data[0]['Grade'])) {
								$thisMark = $data[0]['Grade'];
							}
							else {
								if ($subjectList[$thisSubjectID]['scaleDisplay'] == 'M')
									$thisMark = $data[0]['Mark'];
								else
									$thisMark = $this->ROUND_MARK($data[0]['RawMark'], "SubjectTotal");
							}
							//list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark, $data[0]['Grade']);
							
							$studentSubjectOverallList[$thisStudentID][$thisSubjectID] = $thisMark;
						}
					}
					
					## Use the weight of the last template column as the overall weight calculation
					$lastReportColumnIDIndex = sizeof($reportColumnData) - 1;
					$lastReportColumnID = $reportColumnData[$lastReportColumnIDIndex]["ReportColumnID"];
					
					// calculate average mark
					// for Horizontal to Vertical, only average mark of subject overall need to be calculated
					$reportAverageTotal = array();
					$grandValues = array();
					// order, gpa yet to be done....
					foreach((array)$studentSubjectOverallList as $studentID => $subjectOverallList) {
						$thisWeightSourceColumnID = $lastReportColumnID;
						if ($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation']) {
							$thisWeightSourceColumnID = 0;
						}
						
						$reportAverageTotal[$studentID] = $this->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $studentID, $subjectOverallList, $thisWeightSourceColumnID);
						
						# Round Grand Marks
						$thisGrandMarksArr = $this->ROUND_GRAND_MARKS($reportAverageTotal[$studentID]);
						$tmpTotal = $thisGrandMarksArr["GrandTotal"];
						$tmpAverage = $thisGrandMarksArr["GrandAverage"];
						$tmpGpa = $thisGrandMarksArr["GradePointAverage"];
						
						//$newClassName = ($isPromoted == 1) ? $nextPromoteClassLevel : $classLevelName;
						$grandValues[] = "('$studentID', '$ReportID', '', '$tmpTotal', '$tmpAverage', '$tmpGpa', '', '', '', 'F', '', NOW(), NOW())";
					}
					// consolidate grand total & grand average
					// no value for ReportColumnID
					$grandValues = implode(",", $grandValues);
					
					$success["INSERT_REPORT_RESULT"] = $this->INSERT_REPORT_RESULT($grandValues);
				}
			} // End Condition: CalculationOrder
		}
		
		// Update SD Score
		// 1. Order Position by Weighted SD 	OR 		2. Generate SD order
		if ($eRCTemplateSetting['OrderPositionMethod'] == "WeightedSD" || $eRCTemplateSetting['GenerateSDOrdering']) {
			$this->UPDATE_SD_SCORE($ReportID, $ClassLevelID, "W");
		}
			
		// Calculate Subject Overall by Assessments
		if ($eRCTemplateSetting['OverallGradeCalculatedFromAssesmentGPA']) {
			$this->UpdateOverallGradeFromAssessmentGrade($ReportID);
		}
		
		if ($eRCTemplateSetting['Calculation']['ActualAverage']) {
			$this->UpdateActualAverage($ReportID);
		}
		
		if ($eRCTemplateSetting['Report']['ReportGeneration']['ConsolidateAssessmentColumnCalculation']) {
			$this->CALCULATE_ASSESSMENT_COLUMN_MARK($ReportID, $ClassLevelID);
		}
		
		// Update order (class & form)
		$this->UPDATE_ORDER($ReportID, $ClassLevelID);
		
		// [2015-0421-1144-05164] Update SD Order (class & form)
		if($eRCTemplateSetting['GenerateSDOrdering']){
			$this->UPDATE_ORDER($ReportID, $ClassLevelID, "SDScore", "GrandSDScore", true);	
		}
		
		// Update marks for '% Range' Subjects
		// Since the calculation of '% Range' depends on the order of the student in the form, the calculation should be placed at last
		$numColumn = sizeof($reportColumnData);
		// update the overall column also
		$reportColumnData[$numColumn]['ReportColumnID'] = 0;
		
		for($i=0; $i<sizeof($classArr); $i++)
		{
			// construct the $studentIDArr
			$classID = $classArr[$i]["ClassID"];
			$studentIDArr = array();
			for($j=0; $j<sizeof($studentArr[$classID]); $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			
			// SKIP this class if no students
			if (sizeof($studentIDArr) == 0) continue;
			
			foreach((array)$subjectList as $subjectID => $subjectInfo)
			{
				$schemeID = $subjectInfo["schemeID"];
				$schemeInfo = $this->GET_GRADING_SCHEME_INFO($schemeID);
				$schemeMainInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($schemeID);
				
				$scaleInput = $subjectInfo["scaleInput"];
				$scaleDisplay = $subjectInfo["scaleDisplay"];
				$TopPercentage = $schemeMainInfo["TopPercentage"];
				
				// [2015-1104-1130-08164] Get Make-up Exam Result
				$MarkupResultAry = array();
				if($eRCTemplateSetting['ReportGeneration']['MarkupExamGrade'])
				{
					if(!empty($studentIDArr) && is_array($studentIDArr))
					{
						// Get Graduate Exam Result
						$SchoolType	= $this->GET_SCHOOL_LEVEL($classLevelId);
						$FormNumber = $this->GET_FORM_NUMBER($classLevelId, 1);
						$isSecondaryGraduate = $SchoolType=="S" && $FormNumber==6;
						if($isSecondaryGraduate) {
							$GradTermID = end($reportInvolveTerms);
							$GradReportID = $completedTermReportID[$GradTermID];
							if($GradReportID > 0) {
								$MarkupResultAry = $this->getMarks($GradReportID, "", $cons=" AND b.DisplayOrder = 1 ", $ParentSubjectOnly=1);
							}
						}
						// Get Mark-up Exam Result
						else {
							$MarkupResultAry = $this->GET_EXTRA_SUBJECT_INFO($studentIDArr, $subjectID, $ReportID);
						}
					}
				}
								
				// update the "Mark-to-Grade" subjects only
				if ($scaleInput == "M" && $scaleDisplay == "G" && ($TopPercentage==1 || $TopPercentage==2))
				{
					for($n=0; $n<sizeof($reportColumnData); $n++) {
						$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
						
						if ($this->applyCustomizationFlag('SpecialPassingCheckingIfRelatedScoresAreAllPassed', $ReportID) && $reportColumnID!=0) {
							continue;
						}
						
						// get score of all students in this assessment
						$resultScoreArr = $this->GET_RESULT_SCORE($studentIDArr, $subjectID, $reportColumnID);
												
						foreach((array)$studentIDArr as $studentID)
						{
							$oldMark = $resultScoreArr[$studentID]['Mark'];
							$oldGrade = $resultScoreArr[$studentID]['Grade'];
							$thisReportResultScoreID = $resultScoreArr[$studentID]['ReportResultScoreID'];
							
							### skip the special case mark
							if (in_array($oldGrade, $this->specialCasesSet1) || in_array($oldGrade, $this->specialCasesSet2))
								continue;
							
							if ($eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade'])
							{
								$oldMark = $this->ROUND_MARK($oldMark, "SubjectScore");
							}
										
							$newGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($schemeID, $oldMark, $ReportID, $studentID, $subjectID, $ClassLevelID, $reportColumnID);
							
							// [2015-1104-1130-08164] Set Grade to D for students that pass the Make-up Exam
							if($eRCTemplateSetting['ReportGeneration']['MarkupExamGrade'] && ($newGrade=="F" || $newGrade=="+"))
							{
								if(!empty($MarkupResultAry))
								{
									if($isSecondaryGraduate) {
										$MarkupExamScore = $MarkupResultAry[$studentID][$subjectID];
										if(!empty($MarkupExamScore)) {
											$MarkupExamScore = reset($MarkupExamScore);
											$MarkupExamScore = $MarkupExamScore["Mark"];
										}
									}
									else {
										$MarkupExamScore = $MarkupResultAry[$studentID]["Info"];
									}
									
									// Set Grade to D
									if(!empty($MarkupExamScore) && $MarkupExamScore >= 60)
									{
										$newGrade = "D";
									}
								}
							}
							
							$this->UPDATE_REPORT_RESULT_SCORE($ReportID, $studentID, $reportColumnID, $subjectID, $oldMark, $newGrade, $thisReportResultScoreID);
						}
					}
				}
			}
		}
		
		if ($this->applyCustomizationFlag('SpecialPassingCheckingIfRelatedScoresAreAllPassed', $ReportID)) {
			$this->UpdatePassingStatusSpecialChecking($ReportID);
		}
	}
	
	function CREATE_SCORE_INFO_ARRAY($score) {
		$scoreInfo = array();
		if (is_numeric($score)) {
			$scoreInfo["MarkRaw"] = $score;
			$scoreInfo["MarkNonNum"] = $score;
			$scoreInfo["MarkType"] = "M";
		} else {
			$scoreInfo["MarkRaw"] = "-1";
			$scoreInfo["MarkNonNum"] = $score;
			if (in_array($scoreInfo["MarkNonNum"], $this->specialCasesSet1) || in_array($scoreInfo["MarkNonNum"], $this->specialCasesSet2))
				$scoreInfo["MarkType"] = "SC";
			else
				$scoreInfo["MarkType"] = "G";
		}
		return $scoreInfo;
	}
	
	function INSERT_REPORT_RESULT_SCORE($values) {
		
		if (trim($values)=='') {
			return false;
		}
		
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$fields = "StudentID, SubjectID, ReportID, ReportColumnID, Mark, Grade, RawMark, IsOverall, Semester, OrderMeritClass, OrderMeritForm, DateInput, DateModified";
		$sql = "INSERT INTO $table ($fields) VALUES $values";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function DELETE_REPORT_RESULT_SCORE($ReportID) {
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$sql = "DELETE FROM $table WHERE ReportID = '$ReportID'";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function INSERT_REPORT_RESULT($values) {
		if (trim($values)=='') {
			return false;
		}
		
		$table = $this->DBName.".RC_REPORT_RESULT";
		$fields = "StudentID, ReportID, ReportColumnID, GrandTotal, GrandAverage, GPA, OrderMeritClass, OrderMeritForm, NewClassName, Semester, AdjustedBy, DateInput, DateModified";
		$sql = "INSERT INTO $table ($fields) VALUES $values";
		return $this->db_db_query($sql);
	}
	
	function DELETE_REPORT_RESULT($ReportID) {
		$table = $this->DBName.".RC_REPORT_RESULT";
		$sql = "DELETE FROM $table WHERE ReportID = '$ReportID'";
		return $this->db_db_query($sql);
	}
	
	function UPDATE_REPORT_RESULT($ResultID, $ActualAverage) {
		$table = $this->DBName.".RC_REPORT_RESULT";
		$sql = "UPDATE $table Set ActualAverage = '".$ActualAverage."' Where ResultID = '".$ResultID."'";
		return $this->db_db_query($sql);
	}
	
	# INSERT the new full mark of subject overall (horizontal) or assessment/term total (vertical) in case of excluding full mark
	function INSERT_NEW_FULLMARK($newFullMark, $ReportID, $StudentID, $SubjectID="", $ReportColumnID="") {
		$table = $this->DBName.".RC_REPORT_RESULT_FULLMARK";
		$fields = "StudentID, ReportID, ReportColumnID, SubjectID, FullMark, DateInput, DateModified";
		$values = "'$StudentID', '$ReportID', '$ReportColumnID', '$SubjectID', '$newFullMark', NOW(), NOW()";
		$sql = "INSERT INTO $table ($fields) VALUES ($values)";
		return $this->db_db_query($sql);
	}
	
	function DELETE_NEW_FULLMARK($ReportID) {
		$table = $this->DBName.".RC_REPORT_RESULT_FULLMARK";
		$sql = "DELETE FROM $table WHERE ReportID = '$ReportID'";
		return $this->db_db_query($sql);
	}
	
	# UPDATE the OrderMerit field of existing record in RC_REPORT_RESULT_SCORE
	# $OrderList is the sorted list of StudentID
	# $Type = "Class" or "Form"
	function UPDATE_SCORE_ORDER_MERIT($OrderList, $Type, $SubjectID, $ReportID, $ReportColumnID=0) {
		$orderField = "OrderMerit".$Type;
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$sqlPart = "UPDATE $table SET $orderField =";
		$cond = "WHERE ReportID = '$ReportID' AND ReportColumnID = '$ReportColumnID' AND SubjectID = '$SubjectID' AND StudentID =";
		
		$success = array();
		for($i=0; $i<sizeof($OrderList); $i++) {
			$order = $i+1;
			$sql = "$sqlPart '$order' $cond '".$OrderList[$i]."'";
			$success[] = $this->db_db_query($sql);
		}
		
		return !in_array(0, $success);
	}
	
	function CHECK_HAVE_CMP_SUBJECT($subjectID, $ClassLevelID,$ReportID='') {
		$PreloadArrKey = 'CHECK_HAVE_CMP_SUBJECT';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	
		// check if it is a parent subject, if yes find info of its components subjects
		$CmpSubjectArr = array();
		$CmpSubjectIDArr = array();
		$isCalculateByCmpSub = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
		$isCmpSubject = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($subjectID);
		if (!$isCmpSubject) {
			$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($subjectID, $ClassLevelID);
			if(!empty($CmpSubjectArr)){
				for($k=0 ; $k<count($CmpSubjectArr) ; $k++){
					$CmpSubjectIDArr[] = $CmpSubjectArr[$k]['SubjectID'];
					$CmpSubjectFormGradingArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$k]['SubjectID'],0,0,$ReportID);
					if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
						$isCalculateByCmpSub = 1;
				}
			}
		}
		$returnArr = array($CmpSubjectArr, $CmpSubjectIDArr, $isCmpSubject, $isCalculateByCmpSub);
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $returnArr);
		return $returnArr;
	}
	
	# Calculate the Assessment/Term total across subjects
	function CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $StudentID, $SubjectOverallList, $ReportColumnID="", $debug=0, $LessonSectionAsWeight=false, $HVCalculation_ApplyOverallWeight=false) {
		global $ReportCardCustomSchoolName, $eRCTemplateSetting;
		global $PATH_WRT_ROOT, $ReportCardCustomSchoolName;
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		
		$averageBase = 100;
		
		$calculationSettings = $this->LOAD_SETTING("Calculation");
		$isAbjustFullMark = $calculationSettings["AdjustToFullMarkFirst"];
		
		$absentExemptSettings = $this->LOAD_SETTING("Absent&Exempt");
		
		$CalculationSubjectIDArr = array_keys((array)$SubjectOverallList);
		
		// get subject gradnig info
		$subjectList = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,0,$ReportID);
		
		// get subject weight info of the column
		$tmpReportColumnID = ($ReportColumnID=='')? 0 : $ReportColumnID;
		$subjectWeight = array();
		if ($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation'] && $tmpReportColumnID===0) {
			$VerticalWeightAssoArr = $this->Get_Assessment_Subject_Vertical_Weight($ReportID, 0, $ReturnAsso=true);
			foreach((array)$VerticalWeightAssoArr[$tmpReportColumnID] as $thisSubjectID => $thisWeightInfoArr) {
				$subjectWeight[$thisSubjectID] = $thisWeightInfoArr['Weight'];
			}
			unset($VerticalWeightAssoArr);
		}
		else {
			if ($ReportColumnID == "")
				$cond = "ReportColumnID IS NULL";
			else
				$cond = "ReportColumnID = '$ReportColumnID'";
			$subjectWeightTemp = $this->returnReportTemplateSubjectWeightData($ReportID, $cond);
			
			// [2017-0505-0919-23096] H-V -> Use Vertical Weight as Subject Weight
			if($HVCalculation_ApplyOverallWeight){
				$subjectWeightTemp = $this->returnReportTemplateSubjectWeightData($ReportID, "", "", 'null');
			}
			
			// [2017-1107-0932-40235] Get Subject Weight (in Step 3)
			if($eRCTemplateSetting['Report']['ReportGeneration']['ApplyVerticalZeroWeightColumnGrandCalculation'] && $ReportColumnID != "") {
				$subjectVeritcalWeightTemp = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID IS NULL");
				$subjectVeritcalWeightTemp = BuildMultiKeyAssoc((array)$subjectVeritcalWeightTemp, array("SubjectID"));
			}
			
			for($i=0; $i<sizeof($subjectWeightTemp); $i++)
			{
				$subjectWeight[$subjectWeightTemp[$i]["SubjectID"]] = $subjectWeightTemp[$i]["Weight"];
				
				// [2017-1107-0932-40235] Set Subject Weight to 0 (if set to 0 in Step 3)
				if($eRCTemplateSetting['Report']['ReportGeneration']['ApplyVerticalZeroWeightColumnGrandCalculation'] && $ReportColumnID != "")
				{
					if($subjectWeightTemp[$i]["SubjectID"] && isset($subjectVeritcalWeightTemp[$subjectWeightTemp[$i]["SubjectID"]]) && $subjectVeritcalWeightTemp[$subjectWeightTemp[$i]["SubjectID"]]["Weight"]==0) {
						$subjectWeight[$subjectWeightTemp[$i]["SubjectID"]] = 0;
					}
				}
			}
		}
		
		if ($eRCTemplateSetting['ExcludeFromGrandCalculationIfThereAreAnySpecialCaseInColumn']) {
			// check if copy specific special case only
			$ExcludeGrandCalculationSpecialCaseArr = $eRCTemplateSetting['ExcludeFromGrandCalculationIfThereAreAnySpecialCaseInColumnArr'];
		}
		
		$TmpReportColumnID = ($ReportColumnID=='')? 0 : $ReportColumnID;
		$existSpFullMarkArr = $this->GET_NEW_FULLMARK($ReportID, $StudentID, '', $TmpReportColumnID);
		$existSpFullMarkAssoArr = BuildMultiKeyAssoc($existSpFullMarkArr, array('SubjectID'));
		unset($existSpFullMarkArr);
		
		
		$weight = 0;
		$columnTotalMark = 0;
		$schemeFullMark = 0;
		$columnFullMark = 0;
		$excludeFullMark = 0;
		$excludeWeight = 0;
		$excludeGpaWeight = 0;
		$totalWeight = 0;
		
		$columnGradePoint = 0;
		
		// for calculation of average
		$totalDenomenator = 0;
		$excludeDenomenator = 0;
		$columnNumerator = 0;
		
		$specialCaseDifferentiate = array();
		$specialCaseSetH = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		$specialCaseSetPF = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
		
		foreach((array)$subjectList as $subjectID => $subjectInfo) {
			// check if it is a parent subject
			$isCmpSubject = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($subjectID);
			if ($isCmpSubject) {
				continue;
			}
			
			$scaleInput = $subjectInfo["scaleInput"];
			$scaleDisplay = $subjectInfo["scaleDisplay"];
			
			if (in_array($subjectID, $CalculationSubjectIDArr) == false)
				continue;			
			
			# updated by Ivan on 14 Aug 2009
			# Only count input mark subject in Grand Mark
			# commented by Ivan on 07 Dec 2009
			# uccke - GPA need to count grade display subjects
			/*
			if ($scaleInput != 'M')
				continue;
			*/
			if ($eRCTemplateSetting['ExcludeDisplayGradeSubjectInGrandMark']==true && $scaleDisplay=='G')
				continue;
				
			if ($eRCTemplateSetting['ExcludeInputGradeSubjectInGrandMark']==true && $scaleInput=='G')
				continue;
			
			$weight = $subjectWeight[$subjectID];
			
			// get grading scheme
			$schemeInfo = $this->GET_GRADING_SCHEME_INFO($subjectInfo["schemeID"]);
			$schemeType = $schemeInfo[0]["SchemeType"];
			$schemeFullMark = $schemeInfo[0]["FullMark"];
			
			####### Added on 20080707: check special full mark #######
			$existSpFullMark = array();
			$ReportColumnID = ($ReportColumnID == "") ? 0 : $ReportColumnID;
			//$existSpFullMark = $this->GET_NEW_FULLMARK($ReportID, $StudentID, $subjectID, $ReportColumnID);
			$existSpFullMark = $existSpFullMarkAssoArr[$subjectID];
			if (count((array)$existSpFullMark) > 0) {
				$schemeFullMark = $existSpFullMark["FullMark"];
			}
			##########################################
			$columnFullMark += $schemeFullMark*$weight;
			
			$totalWeight += $weight;
			$totalDenomenator += ($isAbjustFullMark) ? 100*$weight : $schemeFullMark*$weight;
			
			if (isset($SubjectOverallList[$subjectID])) {
				$score = trim($SubjectOverallList[$subjectID]);
				$score = ($score=='')? 'N.A.' : $score;
				
				if (strlen($score) !== "") {
					// Numeric -> can sum it up
					if (is_numeric($score) && $score >= 0 && $schemeFullMark!=0 && $scaleInput=='M') {
						// mark will only be counted for this case
						$columnTotalMark += $score*$weight;
						
						if ($isAbjustFullMark)
						{
						    if($schemeFullMark != 0) {
                                $thisNumerator = ($score / $schemeFullMark) * $weight;
						    }
						    else {
						        $thisNumerator = 0;
						    }
						}
						else
						{
							$thisNumerator = $score * $weight;
						}
						$columnNumerator += $thisNumerator;
						
						$thisGradePoint = $this->CONVERT_MARK_TO_GRADE_POINT_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $score, $ReportID, $StudentID, $subjectID, $ClassLevelID, $ReportColumnID, 1);
						$columnGradePoint += $thisGradePoint * $weight;
						
					} else {	// either Grade or Special Case
						if ($score == "abs" || in_array($score, $specialCaseSetH)) {
							// #########Customize for SIS#########
							// when calculating grand total/average, special handling of special case
							if (($ReportColumnID == "" || $ReportColumnID == "0")) {
								
								### If one of the column is N.A. or other special case, the overall column will be the special case also
								if ($eRCTemplateSetting['ExcludeFromGrandCalculationIfThereAreAnySpecialCaseInColumn']==true)
								{
									if ((count((array)$ExcludeGrandCalculationSpecialCaseArr)==0) || (count((array)$ExcludeGrandCalculationSpecialCaseArr)>0 && in_array($score, $ExcludeGrandCalculationSpecialCaseArr)))
									{
										$returnArr["GrandTotal"] = -1;
										$returnArr["GrandAverage"] = -1;
										$returnArr["GradePointAverage"] = -1;
										return $returnArr;
									}
								}								
								
								// exclude full mark
								$thisExcludeFullMark = false;
								if ($score == "*" || ($score == "N.A." && !$eRCTemplateSetting['NaAsExcludeWeight'])) {
									$thisExcludeFullMark = true;
								}
								if ($score == '-' && $absentExemptSettings['Absent'] == 'ExFullMark') {
									$thisExcludeFullMark = true;
								}
								// 2012-0207-1111-39071
								//if ($score == "*" || $score == "N.A.") {
								if ($thisExcludeFullMark) {
									$totalDenomenator -= ($isAbjustFullMark) ? 100*$weight : $schemeFullMark*$weight;
									$excludeFullMark += $schemeFullMark * $weight;
									$excludeGpaWeight += $weight;
									
								}
								
								// exclude weight
								$thisExcludeWeight = false;
								if ($score == "/") {
									$thisExcludeWeight = true;
								}
								if ($score == '-' && $absentExemptSettings['Absent'] == 'ExWeight') {
									$thisExcludeWeight = true;
								}
								if ($score == "N.A." && $eRCTemplateSetting['NaAsExcludeWeight']) {
									$thisExcludeWeight = true;
								}
								//if ($score == "/") {
								if ($thisExcludeWeight) {
									$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $score, $absentExemptSettings);
									
									if ($eRCTemplateSetting['ExemptionEqualsZeroMarkButExemptFullMarkForOverallColumn'])
									{
										# set as mark0 for exemption
										$specialCaseDifferentiate[$score]["isCount"] = 1;
										$specialCaseDifferentiate[$score]["Value"] = 0;
										
										# exclude full mark here
										$totalDenomenator -= ($isAbjustFullMark) ? 100*$weight : $schemeFullMark*$weight;
										$excludeFullMark += $schemeFullMark*$weight;
										$totalWeight -= $weight;
									}
									
									if ($specialCaseDifferentiate[$score]["isExFullMark"] == 1) {
										$totalDenomenator -= ($isAbjustFullMark) ? 100*$weight : $schemeFullMark*$weight;
										$excludeFullMark += $schemeFullMark*$weight;
										$totalWeight -= $weight;
										
									} else if ($specialCaseDifferentiate[$score]["isCount"] == 0) {
										$excludeWeight += $weight;
										$excludeDenomenator += ($isAbjustFullMark) ? 100*$weight : $schemeFullMark*$weight;
									} else {
										
										if ($isAbjustFullMark)
										{
										    if($schemeFullMark != 0) {
                                                $thisNumerator = ($score / $schemeFullMark) * $weight;
										    }
										    else {
										        $thisNumerator = 0;
										    }
										}
										else
										{
											$thisNumerator = $score * $weight;
										}
										
										$columnTotalMark += $specialCaseDifferentiate[$score]["Value"]*$weight;
										$columnNumerator += $thisNumerator;
									}
								}
							} 
							else {
								### If one of the column is N.A. or other special case, the overall column will be the special case also
								if ($eRCTemplateSetting['ExcludeFromGrandCalculationIfThereAreAnySpecialCaseInColumn']==true)
								{
									if ((count((array)$ExcludeGrandCalculationSpecialCaseArr)==0) || (count((array)$ExcludeGrandCalculationSpecialCaseArr)>0 && in_array($score, $ExcludeGrandCalculationSpecialCaseArr)))
									{
										$returnArr["GrandTotal"] = -1;
										$returnArr["GrandAverage"] = -1;
										$returnArr["GradePointAverage"] = -1;
										return $returnArr;
									}
								}
								
								
								$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $score, $absentExemptSettings);
								
								//if ($score == "/") {
								//	$excludeWeight += $weight;
								//	$excludeDenomenator += ($isAbjustFullMark) ? $weight : $schemeFullMark*$weight;
								//} else 
								if ($specialCaseDifferentiate[$score]["isExFullMark"] == 1) {
									$totalDenomenator -= ($isAbjustFullMark) ? 100*$weight : $schemeFullMark*$weight;
									$excludeFullMark += $schemeFullMark*$weight;
									$totalWeight -= $weight;
								} else if ($specialCaseDifferentiate[$score]["isCount"] == 0) {
									$excludeWeight += $weight;
									$excludeDenomenator += ($isAbjustFullMark) ? 100*$weight : $schemeFullMark*$weight;
								} else {
									
									if ($isAbjustFullMark)
									{
									    if($schemeFullMark != 0) {
                                            $thisNumerator = ($score / $schemeFullMark) * $weight;
									    }
									    else {
									        $thisNumerator = 0;
									    }
									}
									else
									{
										$thisNumerator = $score * $weight;
									}
									
									$columnTotalMark += $specialCaseDifferentiate[$score]["Value"]*$weight;
									$columnNumerator += $thisNumerator;
								}
							}
						} else {	// score is a grade or P/F
						
							## updated on 18 Dec 2008 by Ivan
							## get the mark for GPA calculation
							# Affect GPA only. Do not affect GrandTotal and GrandAverage
							if ($scaleInput == "G")
							{
								$columnGradePoint += ($this->CONVERT_GRADE_TO_GRADE_POINT_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $score))*$weight;
							}
							else
							{
								// Exclude weight by default
								$excludeWeight += $weight;
								$excludeDenomenator += ($isAbjustFullMark) ? 100*$weight : $schemeFullMark*$weight;
							}
						}
					}
				} else {
					$excludeWeight += $weight;
					$excludeDenomenator += ($isAbjustFullMark) ? 100*$weight : $schemeFullMark*$weight;
				}
			} else {
				$excludeWeight += $weight;
				$excludeDenomenator += ($isAbjustFullMark) ? 100*$weight : $schemeFullMark*$weight;
			}
		}
		
		// insert new full mark
		$newFullMark = $columnFullMark;
		if ($excludeFullMark > 0) {
			$newFullMark = $columnFullMark - $excludeFullMark;
			$success["INSERT_NEW_FULLMARK"] = $this->INSERT_NEW_FULLMARK($newFullMark, $ReportID, $StudentID, "", $ReportColumnID);
		}
		
		$remainWeight = $totalWeight - $excludeWeight;
		$remainWeightGpa = $remainWeight - $excludeGpaWeight;
		$remainDenomenator = $totalDenomenator-$excludeDenomenator;
		
		$isAllNA = true;
		foreach ((array)$SubjectOverallList as $thisSubjectID => $thisMark)
		{
			//2016-0621-0943-14207
			$thisMark = ($thisMark=='')? 'N.A.' : $thisMark;
			if ($thisMark != 'N.A.')
			{
				$isAllNA = false;
				break;
			}
		}
		
		if ($isAllNA == true)
		{
			$returnArr["GrandTotal"] = -1;
			$returnArr["GrandAverage"] = -1;
			$returnArr["GradePointAverage"] = -1;
			return $returnArr;
		}
		
		// If all subject marks are of the same special case, assign GrandTotal & GrandAverage to that special case also
		if (sizeof($specialCaseDifferentiate) == 1) {
			$onlySpecialCase = array_keys($specialCaseDifferentiate);
			if ($specialCaseDifferentiate[$onlySpecialCase[0]]["Count"] == sizeof($subjectList)) {
				$returnArr["GrandTotal"] = $onlySpecialCase[0];
				$returnArr["GrandAverage"] = $onlySpecialCase[0];
				$returnArr["GradePointAverage"] = $onlySpecialCase[0];
				return $returnArr;
			}
		}
		
		
		$returnArr["GrandTotal"] = ($remainWeight == 0) ? 0 : $columnTotalMark*($totalWeight/$remainWeight);
		//$returnArr["GrandAverage"] = ($remainDenomenator == 0) ? 0 : (($columnNumerator/$remainDenomenator)*$averageBase);
		//2012-0716-1151-34071
		//$returnArr["GrandAverage"] = ($totalDenomenator == 0) ? 0 : (($returnArr["GrandTotal"]/$totalDenomenator)*$averageBase);
		$returnArr["GrandAverage"] = ($remainDenomenator == 0) ? 0 : (($returnArr["GrandTotal"]/$remainDenomenator)*$averageBase);
		$returnArr["GradePointAverage"] = ($remainWeightGpa == 0) ? 0 : ($columnGradePoint/$remainWeightGpa);
		
		return $returnArr;
	}
	
	function CALCULATE_VERTICAL_SUBJECT_OVERALL($subjectColumnMarkList, $reportColumnWeight, $ReportID, $ClassLevelID, $absentExemptSettings, $reportBasicInfo, $totalReportColumnWeight, $debug=0) {
		global $PATH_WRT_ROOT, $ReportCardCustomSchoolName, $eRCTemplateSetting;
		
		//$SubjectGradingSchemeAssoArr[$SubjectID] = InfoArr
		$SubjectGradingSchemeAssoArr = array();
		
		$reportColumnData = $this->returnReportTemplateColumnData($ReportID);
		$lastReportColumnID = $reportColumnData[count($reportColumnData)-1]['ReportColumnID'];
		$lastReportColumnTermID = $reportColumnData[count($reportColumnData)-1]['SemesterNum'];
		
		$SemID = $reportBasicInfo['Semester'];
		$ReportType = ($SemID=='F')? 'W' : 'T';
		
		if ($reportBasicInfo['Semester'] == "F")
			$isWholeYearReport = true;
		else
			$isWholeYearReport = false;
			
		$existSpFullMarkArr = $this->GET_NEW_FULLMARK($ReportID, '', '', '');
		$existSpFullMarkAssoArr = BuildMultiKeyAssoc($existSpFullMarkArr, array('StudentID', 'SubjectID', 'ReportColumnID'));
		unset($existSpFullMarkArr);	
		
		$StudentIDArr = array_keys((array)$subjectColumnMarkList);
		$OverallMarksheetArr = array();
		foreach((array)$subjectColumnMarkList as $studentID => $markSubjectList) {
			foreach((array)$markSubjectList as $subjectID => $markColumnList) {
				if (!isset($OverallMarksheetArr[$subjectID])) {
					$OverallMarksheetArr[$subjectID] = $this->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $subjectID, $ReportID);
				}
			}
		}
		
		$subjectOverallValues = array();
		$success = array();
		
		foreach((array)$subjectColumnMarkList as $studentID => $markSubjectList) {
			foreach((array)$markSubjectList as $subjectID => $markColumnList) {
				
				$subjectFullMark = $this->GET_SUBJECT_FULL_MARK($subjectID, $ClassLevelID,$ReportID);
				$subjectOverallMark = "";
				$subjectOverallMarkConverted = '';
				$subjectOverallMarkRaw = '';
				$excludeColumnWeight = 0;
				$excludeFullMark = 0;
				$specialCaseDifferentiate = array();
				
				if (!isset($SubjectGradingSchemeAssoArr[$subjectID])) {
					$SubjectGradingSchemeAssoArr[$subjectID] = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $subjectID, 0, 0, $ReportID);
				}
				$subjectSchemeInfo = $SubjectGradingSchemeAssoArr[$subjectID];
				
				$schemeID = $subjectSchemeInfo["SchemeID"];
				$scaleInput = $subjectSchemeInfo["ScaleInput"];
				$scaleDisplay = $subjectSchemeInfo["ScaleDisplay"];
				
				if ($eRCTemplateSetting['ConsolidatedReportUseGPAToCalculate'] && $ReportType=='W') {
					$scaleInput = 'M';
				}
					
				$schemeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($schemeID);
				$schemeRangeIDGradeMap = array();
				for($m=0; $m<sizeof($schemeRangeInfo); $m++) 
					$schemeRangeIDGradeMap[$schemeRangeInfo[$m]["GradingSchemeRangeID"]] = $schemeRangeInfo[$m]["Grade"];
					
				
				// add back score for ���Z�� not taking exam
				if ( !($this->Get_Num_Of_Column($ReportID) == 4) || !($eRCTemplateSetting['UseConsolidatedReportOverallScoreToCalculateSubjectOverall']) )
				{
					for ($i=0; $i<count($reportColumnData); $i++)
					{
						$columnID = $reportColumnData[$i]["ReportColumnID"];
						
						if (!isset($markColumnList[$columnID]))
						{
							$markColumnList[$columnID] = 'N.A.';
						}
					}
				}
				
				
				foreach((array)$markColumnList as $columnID => $mark) {
					
					if (trim($mark)==='') {
						$mark = 'N.A.';
					}
					
					if ($eRCTemplateSetting['LastColumnTermNotInSubjectGroupEqualsDropped'] && $ReportType=='W' && $columnID==$lastReportColumnID) {
						if (!$this->Is_Student_In_Subject_Group_Of_Subject($ReportID, $studentID, $subjectID, $lastReportColumnTermID)) {
							$subjectOverallMark = 'N.A.';
							$subjectOverallMarkConverted = 'N.A.';
							break;
						}
					}
					
					$columnSubjectWeight = $reportColumnWeight[$columnID];
					//2017-0630-0914-50236 - cancel the customization
					//2016-0630-0911-42236
// 					if ($ReportCardCustomSchoolName == "sha_tin_methodist") {
// 						if ($ReportID==15 && $subjectID==101) {
// 							if ($columnID==40) {
// 								$columnSubjectWeight = 0;
// 							}
// 							else if ($columnID==41) {
// 								$columnSubjectWeight = 1;
// 							}
// 						}
// 					}
										
					if (is_numeric($mark) and $scaleInput=="M") {
						// if any of the assessment marks is numeric, make sure $subjectOverallMark is initiated to zero
						$subjectOverallMark = ($subjectOverallMark == "") ? 0 : $subjectOverallMark;
						//$subjectOverallMark += $mark*$reportColumnWeight[$columnID];
						
						// rounded weighted mark before add to overall (2011-0126-1606-48073)
						//$thisWeightedMark = $mark * $reportColumnWeight[$columnID];
						$thisWeightedMark = $mark * $columnSubjectWeight;
						
						if ($eRCTemplateSetting['RoundSubjectMarkBeforeAddToOverallMark'])
							$thisWeightedMark = $this->ROUND_MARK($thisWeightedMark, "SubjectScore"); 
						$subjectOverallMark += $thisWeightedMark;
						
						//$existSpFullMark = $this->GET_NEW_FULLMARK($ReportID, $studentID, $subjectID, $columnID);
						$existSpFullMark = $existSpFullMarkAssoArr[$studentID][$subjectID][$columnID];
						if (count((array)$existSpFullMark) > 0) {
							//$excludeFullMark += ($subjectFullMark-$existSpFullMark["FullMark"])*$reportColumnWeight[$columnID];
							$excludeFullMark += ($subjectFullMark-$existSpFullMark["FullMark"])*$columnSubjectWeight;
						}
					} else {
						if($scaleInput=="M")
						{
							// Build the $specialCaseDifferentiate Array for determining the subject overall mark below
							$specialCaseDifferentiate = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseDifferentiate, $mark, $absentExemptSettings);
							
							// Only concern numeric mark calculation here
							if ($specialCaseDifferentiate[$mark]["isExFullMark"] == 1) {
								//$excludeFullMark += $subjectFullMark * $reportColumnWeight[$columnID];
								$excludeFullMark += $subjectFullMark * $columnSubjectWeight;
							}
							//2012-0706-1451-01140
							//if ($specialCaseDifferentiate[$mark]["isCount"] == 0) {
							else if ($specialCaseDifferentiate[$mark]["isCount"] == 0) {
								$excludeColumnWeight += $reportColumnWeight[$columnID];
							}
						}
						else	#$scaleDisplay=="G"
						{
							# ?
						}
					}
				}
				
				
				### If one of the column is N.A. or other special case, the overall column will be the special case also
				if ($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']==true && count($specialCaseDifferentiate) > 0)
				{
					$tmpKeyArr = array_keys($specialCaseDifferentiate);
					$numOfKey = count($tmpKeyArr);
					
					for ($i=0; $i<$numOfKey; $i++)
					{
						$thisSC = $tmpKeyArr[$i];
						
						// check if copy specific special case only
						if ($this->Check_Copy_Special_Case($thisSC, $ReportType, $studentID))
						{
							$subjectOverallMark = $thisSC;
							$subjectOverallMarkConverted = $thisSC;
							break;
						}
					}
				}
				
				// in case of excluding full mark
				if ($excludeFullMark > 0) {
					//$newFullMark = $subjectFullMark - $excludeFullMark;
					//$success["INSERT_NEW_FULLMARK"] = $this->INSERT_NEW_FULLMARK($newFullMark, $ReportID, $studentID, $subjectID);
					
					$newFullMark = $subjectFullMark - $excludeFullMark;
					//$success["INSERT_NEW_FULLMARK"] = $this->INSERT_NEW_FULLMARK($newFullMark, $ReportID, $markStudentID, $subjectID);
					
					// exclude full mark
					if ($newFullMark > 0 && is_numeric($subjectOverallMark))
					{
						$subjectOverallMark = $subjectOverallMark * ($subjectFullMark / $newFullMark);
					}
				}
				
				// 2012-0507-1530-27066 point 2 - added the if clause
				if (isset($OverallMarksheetArr[$subjectID][$studentID]['MarkNonNum'])) {
					// 2012-0531-1034-48073
					//$subjectOverallMark = $OverallMarksheetArr[$subjectID][$studentID]['MarkNonNum'];
					//$subjectOverallMarkConverted = $OverallMarksheetArr[$subjectID][$studentID]['MarkNonNum'];
					
					$thisMarkNonNum = $OverallMarksheetArr[$subjectID][$studentID]['MarkNonNum'];
					$thisMarkType = $OverallMarksheetArr[$subjectID][$studentID]['MarkType'];
					
					if ($thisMarkType == 'G') {
						if ($schemeRangeIDGradeMap[$thisMarkNonNum] != '') {
							# map mark with the corresponding grade
							$subjectOverallMark = $schemeRangeIDGradeMap[$thisMarkNonNum];
							$subjectOverallMarkConverted = $schemeRangeIDGradeMap[$thisMarkNonNum];
						}
						else {
							# Can't map grade => Use the input mark
							$subjectOverallMark = $thisMarkNonNum;
							$subjectOverallMarkConverted = $thisMarkNonNum;
						}
					}
					else {
						$subjectOverallMark = $thisMarkNonNum;
						$subjectOverallMarkConverted = $thisMarkNonNum;
					}
				}
				$subjectOverallMark = trim($subjectOverallMark);
				
				
				// subject overall mark can be calculated as number
				if ($scaleInput=='M' && is_numeric($subjectOverallMark)) {
					$subjectOverallMarkConverted = $subjectOverallMark;
					$subjectOverallMarkRaw = $subjectOverallMark;
					
					if ($scaleInput == "M" && $scaleDisplay == "G") {
						
						if ($totalReportColumnWeight == $excludeColumnWeight) {
							$subjectOverallMarkConverted = "/";
							$subjectOverallMarkRaw = 0;
						}
						else {
							if ($excludeColumnWeight > 0) {
								$activeWeightRatio = $totalReportColumnWeight / ($totalReportColumnWeight - $excludeColumnWeight);
								$subjectOverallMarkTmp = $subjectOverallMark;
								$subjectOverallMark = $subjectOverallMarkTmp * $activeWeightRatio;
								$subjectOverallMarkRaw = $subjectOverallMarkTmp * $activeWeightRatio;
							}
							
							if ($eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade']) {
								$subjectOverallMarkToConvert = $this->ROUND_MARK($subjectOverallMark, "SubjectTotal");
							}
							else {
								$subjectOverallMarkToConvert = $subjectOverallMark;
							}
							$subjectOverallMarkConverted = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectSchemeInfo["SchemeID"], $subjectOverallMarkToConvert);
							//$subjectOverallMarkConverted = $this->ROUND_MARK($subjectOverallMarkConverted, "SubjectTotal");
						}
					} else {
						if ($totalReportColumnWeight == $excludeColumnWeight) {
							$subjectOverallMarkConverted = "/";
							$subjectOverallMarkRaw = 0;
						} else {
							# updated on 12 Jan 2009 by Ivan
							# divide the overall marks by the total column weight
							if ($excludeFullMark > 0)
								$subjectOverallMarkConverted = $subjectOverallMark;
							else
								$subjectOverallMarkConverted = $subjectOverallMark / ($totalReportColumnWeight - $excludeColumnWeight);
							
							if ($isWholeYearReport && $eRCTemplateSetting['CustomizedConsolidatedSubjectTotalRounding'])
								$subjectOverallMarkConverted = my_round($subjectOverallMarkConverted, $eRCTemplateSetting['ConsolidatedSubjectTotalRounding']);
							else
								$subjectOverallMarkConverted = $this->ROUND_MARK($subjectOverallMarkConverted, "SubjectTotal");
						}
					}
				} else { // subject cannot be calculated as number  # or is input mark display grade (20080926)
					// There's only one kind of special case in all column
					$subjectOverallMarkRaw = 0;
					
					if ($subjectOverallMarkConverted == '')
					{
						# retrieve the data (column id =0)
						$ovarllTemp = $this->GET_MARKSHEET_OVERALL_SCORE(array($studentID), $subjectID, $ReportID);
						
						if ($ovarllTemp[$studentID]['MarkType'] == 'M' && $ovarllTemp[$studentID]['MarkNonNum'] != '') {
							$subjectOverallMarkRaw = $ovarllTemp[$studentID]['MarkNonNum'];
							$subjectOverallMark = $ovarllTemp[$studentID]['MarkNonNum'];
							$subjectOverallMarkConverted = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($schemeID, $subjectOverallMark, $ReportID, $studentID, $subjectID, $ClassLevelID, 0, "");
						}
						else if (sizeof($specialCaseDifferentiate) == 1) {
							$onlySpecialCase = array_keys($specialCaseDifferentiate);
							$subjectOverallMarkConverted = $onlySpecialCase[0];
							
						} else {	// How to handle different special cases mixed up together?
							// Temporary assign it to Zero 
							$subjectOverallMark = "0";
							
							if ($schemeRangeIDGradeMap[$ovarllTemp[$studentID]['MarkNonNum']] != '')
							{
								# map mark with the corresponding grade
								$subjectOverallMarkConverted = $schemeRangeIDGradeMap[$ovarllTemp[$studentID]['MarkNonNum']];
								
							}
							else
							{
								if ($ovarllTemp[$studentID]['MarkNonNum'] == '') {
									//2014-0704-1054-47140
									# Can't map grade and no input mark => use "N.A."
									$subjectOverallMarkConverted = 'N.A.';
								}
								else {
									# Can't map grade => Use the input mark
									$subjectOverallMarkConverted = $ovarllTemp[$studentID]['MarkNonNum'];
								}
							}
						}
					}
				}
				
				if ($scaleDisplay == "M" && is_numeric($subjectOverallMark) && is_numeric($subjectOverallMarkConverted))
				{
					$subjectOverallValues[] = "('$studentID', '$subjectID', '$ReportID', '', '$subjectOverallMarkConverted', '', '$subjectOverallMarkRaw', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
				}
				else
				{
					$subjectOverallValues[] = "('$studentID', '$subjectID', '$ReportID', '', '$subjectOverallMarkRaw', '$subjectOverallMarkConverted', '$subjectOverallMarkRaw', '1', '".$reportBasicInfo["Semester"]."', '', '', NOW(), NOW())";
				}
			}
		}
		
		// consolidate the resulting subject overall mark
		if (sizeof($subjectOverallValues) > 0) {
			$subjectOverallValues = implode(",", $subjectOverallValues);
			$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($subjectOverallValues);
		}
		
		return !in_array(false, $success);
	}
	
	function CALCULATE_ASSESSMENT_COLUMN_MARK($ReportID, $ClassLevelID)
	{
		global $PATH_WRT_ROOT, $ReportCardCustomSchoolName, $eRCTemplateSetting;
		
		$success = array();
		if(!$eRCTemplateSetting['Report']['ReportGeneration']['ConsolidateAssessmentColumnCalculation']) {
			return $success;
		}
		
		################### Preload Settings & Data ###################
		// Mark Storage & Display Settings : Subject Score (0,1,2), Subject Total (0,1,2), Grand Average (0,1,2)
		$storageDisplaySettings = $this->LOAD_SETTING("Storage&Display");
		
		// Calculation Settings : Order Term (1,2), Order Year (1,2), Use Weighted Mark (0,1)
		$calculationSettings = $this->LOAD_SETTING("Calculation");
		$calculationOrder = $calculationSettings["OrderFullYear"];
		$isAbjustFullMark = $calculationSettings["AdjustToFullMarkFirst"];
		
		// Absent & Exempt Settings : Exempt (Ex. Weight, Ex. Full Mark, Zero), Absent (Ex. Weight, Ex. Full Mark, Zero)
		$absentExemptSettings = $this->LOAD_SETTING("Absent&Exempt");
		
		// Special Case of Input Mark / Term Mark
		$SpecialCaseArrH = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		foreach((array)$SpecialCaseArrH as $key){
			$SpecialCaseCountArrH[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
		}
		$SpecialCaseArrPF = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
		foreach((array)$SpecialCaseArrPF as $key){
			$SpecialCaseCountArrPF[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
		}
		$tmpSpecialCaseArrH = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		
		// Get Report Template Basic Info
		$reportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$classLevelId = $reportBasicInfo['ClassLevelID'];
		$targetSemId = $reportBasicInfo['Semester'];
		if($targetSemId != "F") {
			return $success;
		}
		
		// Get Report Template Columns
		$reportColumnTypeAry = array();
		$reportColumnData = $this->returnReportTemplateColumnData($ReportID);
		$numOfReportColumn = count($reportColumnData);
		if ($numOfReportColumn > 0)
		{
			for($i=0; $i<$numOfReportColumn; $i++)
			{
				$reportInvolveTerms[] = $reportColumnData[$i]["SemesterNum"];
				$reportColumnTermMap[$reportColumnData[$i]["ReportColumnID"]] = $reportColumnData[$i]["SemesterNum"];
				
				if ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment']) {
					$reportTermColumnMap[$reportColumnData[$i]["SemesterNum"]][$reportColumnData[$i]["TermReportColumnID"]] = $reportColumnData[$i]["ReportColumnID"];
				}
				else {
					$reportTermColumnMap[$reportColumnData[$i]["SemesterNum"]] = $reportColumnData[$i]["ReportColumnID"];
				}
				
				$reportColumnOrder = $reportColumnData[$i]["DisplayOrder"];
				$reportColumnType = $eRCTemplateSetting['Report']['ReportGeneration']['ConsolidateAssessmentColumnType'][$reportColumnOrder];
				if($reportColumnType != "") {
					$reportColumnTypeAry[$reportColumnData[$i]["ReportColumnID"]] = $reportColumnType;
				}
			}
		}
		
		// Get Subject Full Marks
		$termReportIdMappingAry = array();
		$subjectFullMarkMappingAry = array();
		for ($i=0; $i<$numOfReportColumn; $i++)
		{
			$_reportColumnId = $reportColumnData[$i]['ReportColumnID'];
			$_semesterNum = $reportColumnData[$i]['SemesterNum'];
			
			$_termReportInfoAry = $this->returnReportTemplateBasicInfo('', $others='', $classLevelId, $_semesterNum, $isMainReport=1);
			$_termReportId = $_termReportInfoAry['ReportID'];
			$termReportIdMappingAry[$_reportColumnId] = $_termReportId;
			
			$subjectFullMarkMappingAry[$_termReportId] = $this->returnSubjectFullMark($classLevelId, $withSub=1, $ParSubjectIDArr=array(), $ParInputScale=1, $_termReportId);
		}
		$subjectFullMarkMappingAry[$ReportID] = $this->returnSubjectFullMark($classLevelId, $withSub=1, $ParSubjectIDArr=array(), $ParInputScale=1, $ReportID);
		
		// Get Form Students
		$allStudentArr = array();
		$classArr = $this->GET_CLASSES_BY_FORM($ClassLevelID);
		for($i=0; $i<sizeof($classArr); $i++)
		{
			$studentArr[$classArr[$i]["ClassID"]] = $this->GET_STUDENT_BY_CLASS($classArr[$i]["ClassID"]);
			$allStudentArr = array_merge($allStudentArr, $studentArr[$classArr[$i]["ClassID"]]);
		}
		
		// Get Existing Term Reports
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		$reportInvolveTermsList = implode("','", $reportInvolveTerms);
		$reportInvolveTermsList = "'".$reportInvolveTermsList."'";
		$sql = "SELECT ReportID, Semester FROM $table WHERE ClassLevelID = '$ClassLevelID' AND Semester IN ($reportInvolveTermsList) And isMainReport = 1";
		$existTermReportID = $this->returnArray($sql, 2);
		
		$searchExistTermMode = "T";
		if (sizeof($existTermReportID) == 0)
		{
			$sql = "SELECT ReportID, Semester FROM $table WHERE ClassLevelID = '$ClassLevelID' AND Semester = 'F' AND ReportID != '$ReportID'";
			$existTermReportID = $this->returnArray($sql, 2);
			$searchExistTermMode = "W";
		}
		if (sizeof($existTermReportID) > 0)
		{
			$completedTerm = array();
			$completedTermReportID = array();
			for($i=0; $i<sizeof($existTermReportID); $i++)
			{
				// No errors => Report was completed
				$errArr = array();
				if (empty($errArr) && sizeof($errArr) == 0)
				{
					if ($existTermReportID[$i]["Semester"] == "F")
					{
						$existTermReportColumnData = $this->returnReportTemplateColumnData($existTermReportID[$i]["ReportID"]);
						for($j=0; $j<sizeof($existTermReportColumnData); $j++) {
							if (in_array($existTermReportColumnData[$j]["SemesterNum"], $reportInvolveTerms)) {
								$completedTermReportID[$existTermReportColumnData[$j]["SemesterNum"]] = $existTermReportID[$i]["ReportID"];
							}
						}
					}
					else
					{
						$completedTermReportID[$existTermReportID[$i]["Semester"]] = $existTermReportID[$i]["ReportID"];
					}
				}
			}
			if (sizeof($completedTermReportID) > 0)
			{
				foreach((array)$completedTermReportID as $completedSemester => $completedReportID)
				{
					// MUST maintain Report Creation Order
					if ($searchExistTermMode != "T" && $ReportID < $completedReportID) {
						continue;
					}
					
					$tmpExistTermSubjectOverall = $this->GET_EXIST_TERM_RESULT_SCORE($completedReportID, $completedSemester, $ReportID, $reportTermColumnMap, $searchExistTermMode, $ClassLevelID);
					if (is_array($tmpExistTermSubjectOverall)) {
						$existTermSubjectOverallArr[] = $tmpExistTermSubjectOverall;
					}
					else {
						unset($completedTermReportID[$completedSemester]);
					}
				}
			}
		}
		
		// Calculate Subject Overall Mark in Vertical - Horizontal
		$SubjectGradingSchemeAssoArr = array();
		for($i=0; $i<sizeof($existTermSubjectOverallArr); $i++)
		{
			foreach((array)$existTermSubjectOverallArr[$i] as $tmpStudentID => $tmpSubjectInfo)
			{
				foreach((array)$tmpSubjectInfo as $tmpSubjectID => $tmpSubjectOverallInfo)
				{ 
					$tmpReportColumnID = $tmpSubjectOverallInfo[0]["ReportColumnID"];
					
					# Retrieve Subject Scheme & Settings
					if (!isset($SubjectGradingSchemeAssoArr[$ReportID][$tmpSubjectID])) {
						$SubjectGradingSchemeAssoArr[$ReportID][$tmpSubjectID] = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 0, 0, $ReportID);
					}
					$SubjectFormGradingSettings = $SubjectGradingSchemeAssoArr[$ReportID][$tmpSubjectID];
					$SchemeID = $SubjectFormGradingSettings['SchemeID'];
					$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
					$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
					
					# Use Mark instead of Raw Mark for Calculation
					$tmpRawMark = $tmpSubjectOverallInfo[0]["RawMark"];
					$tmpMark = $tmpSubjectOverallInfo[0]["Mark"];
					$tmpGrade = $tmpSubjectOverallInfo[0]["Grade"];
					if ($this->Check_If_Grade_Is_SpecialCase($tmpGrade)==true) {
						$tmpMark = $tmpGrade;
					}
					else if ($ScaleInput=="M" && $ScaleDisplay=="G") {
						$tmpMark = $tmpRawMark;
					}
					else if ($tmpGrade != '') {
						$tmpMark = $tmpGrade;
					}
					else if ($eRCTemplateSetting['UseRawMarkToCalculateSDScore'] == "WeightedSD") {
						$tmpMark = $tmpRawMark;
					}
					if ($eRCTemplateSetting['ConsolidatedReportUseGPAToCalculate'] && $this->Check_If_Grade_Is_SpecialCase($tmpGrade)==false) {
						$tmpMark = $this->CONVERT_GRADE_TO_GRADE_POINT_FROM_GRADING_SCHEME($SchemeID, $tmpGrade);
					}
					$allSubjectColumnMarkList[$tmpStudentID][$tmpSubjectID][$tmpReportColumnID] = $tmpMark;
				}
			}
		}
		
		// Get Subject Components
		$SubjectCmpInfoAssoArr = array();
		$subjectList = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,0,$ReportID);
		foreach((array)$subjectList as $subjectID => $subjectInfo)
		{
			$SubjectCmpInfoAssoArr[$subjectID] = $this->CHECK_HAVE_CMP_SUBJECT($subjectID, $ClassLevelID, $ReportID);
		}
		
		// Loop 1: Classes
		for($i=0; $i<sizeof($classArr); $i++)
		{
			// $studentIDArr: Class Students
			$studentIDArr = array();
			$classID = $classArr[$i]["ClassID"];
			for($j=0; $j<sizeof($studentArr[$classID]); $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			
			// Skip if no students
			$numOfStudent = count($studentIDArr);
			if ($numOfStudent == 0) continue;
			
				// Loop 2: Subjects
				$studentSubjectOverallList = array();
				foreach((array)$subjectList as $subjectID => $subjectInfo)
				{
					// $schemeInfo[0]: Main Info, [1]: Ranges Info
					$schemeInfo = $this->GET_GRADING_SCHEME_INFO($subjectInfo["schemeID"]);
					$schemeType = $schemeInfo[0]["SchemeType"];
					$passWording = $schemeInfo[0]["Pass"];
					$failWording = $schemeInfo[0]["Fail"];
					$subjectFullMark = $schemeInfo[0]["FullMark"];
					
					$scaleInput = $subjectInfo["scaleInput"];
					$scaleDisplay = $subjectInfo["scaleDisplay"];
					if ($eRCTemplateSetting['ConsolidatedReportUseGPAToCalculate']) {
						$scaleInput = 'M';
					}
					
					// Get Grading Scheme Range Info
					$schemeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($subjectInfo["schemeID"]);
					$schemeRangeIDGradeMap = array();
					if (sizeof($schemeRangeInfo) > 0)
					{
						for($m=0; $m<sizeof($schemeRangeInfo); $m++) {
							$schemeRangeIDGradeMap[$schemeRangeInfo[$m]["GradingSchemeRangeID"]] = $schemeRangeInfo[$m]["Grade"];
						}
					}
					
					// Check if is Parent Subject
					$CmpInfoArr = $SubjectCmpInfoAssoArr[$subjectID];
					$CmpSubjectArr = $CmpInfoArr[0];
					$CmpSubjectIDArr = $CmpInfoArr[1];
					$isCmpSubject = $CmpInfoArr[2];
					$isCalculateByCmpSub = $CmpInfoArr[3];
					if ($eRCTemplateSetting['ManualInputParentSubjectMark'])
					{
						$isCalculateByCmpSub = 0;
					}
					
					$reportColumnWeight = array();
					$totalReportAssessmentWeight = array(-1 => 0, -2 => 0);
					$reportColumnWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, "SubjectID = ".$subjectID);
					if (sizeof($reportColumnWeightInfo) > 0)
					{
						for($x=0; $x<sizeof($reportColumnWeightInfo); $x++)
						{
							if ($reportColumnWeightInfo[$x]['ReportColumnID'] != "" && $reportColumnWeightInfo[$x]['Weight'] != "")
							{
								$markColumnType = $reportColumnTypeAry[$reportColumnWeightInfo[$x]['ReportColumnID']];
								if($markColumnType == "") {
									continue;
								}
								
								$reportColumnWeight[$reportColumnWeightInfo[$x]['ReportColumnID']] = $reportColumnWeightInfo[$x]['Weight'];
								$totalReportAssessmentWeight[$markColumnType] += $reportColumnWeightInfo[$x]['Weight'];
							}
						}
					}
					
					################ Construct $AllColumnMarkArr #################
					$AllColumnMarkArr = array();
					
					// Existing Term Mark in other report
					if (sizeof($existTermSubjectOverallArr) > 0)
					{
						for($a=0; $a<sizeof($studentIDArr); $a++)
						{
							for($w=0; $w<sizeof($existTermSubjectOverallArr); $w++)
							{
								if (isset($existTermSubjectOverallArr[$w][$studentIDArr[$a]][$subjectID]))
								{
									$tmpTermOverallDetail = $existTermSubjectOverallArr[$w][$studentIDArr[$a]][$subjectID];
									for ($p=0; $p<sizeof($tmpTermOverallDetail); $p++)
									{
										$markColumnType = $reportColumnTypeAry[$tmpTermOverallDetail[$p]["ReportColumnID"]];
										if($markColumnType == "") {
											continue;
										}
										
										if ($tmpTermOverallDetail[$p]["Grade"] == "" && is_numeric($tmpTermOverallDetail[$p]["Mark"])) {
											$tmpTermOverallDetail[$p]["RawMark"] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $tmpTermOverallDetail[$p]["ReportColumnID"], $subjectID, $tmpTermOverallDetail[$p]["RawMark"]);
											$tmpTermOverallMark = $this->ROUND_MARK($tmpTermOverallDetail[$p]["RawMark"], "SubjectTotal");
											$AllColumnMarkArr[$studentIDArr[$a]][$markColumnType][$tmpTermOverallDetail[$p]["ReportColumnID"]] = $tmpTermOverallMark;
										}
										else {
											$AllColumnMarkArr[$studentIDArr[$a]][$markColumnType][$tmpTermOverallDetail[$p]["ReportColumnID"]] = $tmpTermOverallDetail[$p]["Grade"];
										}
									}
								}
							}
						}
					}
					
					// Get Report Subject Result Score
					for($n=0; $n<sizeof($reportColumnData); $n++)
					{
						$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
						$markColumnType = $reportColumnTypeAry[$reportColumnID];
						if($markColumnType == "") {
							continue;
						}
						
						$marksheetScore = $this->GET_RESULT_SCORE_FOR_SUBJECT_OVERALL($studentIDArr, $subjectID, $reportColumnID, $ReportID);
						if (sizeof($marksheetScore) > 0)
						{
							foreach((array)$marksheetScore as $studentID => $scoreDetailInfo)
							{
								$AllColumnMarkArr[$studentID][$markColumnType][$reportColumnID] = $scoreDetailInfo['MarkNonNum'];
								if($scoreDetailInfo['MarkType'] == "M" && $scoreDetailInfo["MarkRaw"] != "-1") {
									$AllColumnMarkArr[$studentID][$markColumnType][$reportColumnID] = $scoreDetailInfo['MarkRaw'];	
								}
							}
						}
					}
					
					// Fill in "Exempt" for any missing score
					$calculationMark = array();
					for($n=0; $n<sizeof($reportColumnData); $n++)
					{
						$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
						$markColumnType = $reportColumnTypeAry[$reportColumnID];
						if($markColumnType == "") {
							continue;
						}
						
						for($g=0; $g<sizeof($studentIDArr); $g++)
						{
							if (!isset($AllColumnMarkArr[$studentIDArr[$g]][$markColumnType][$reportColumnID]) || $AllColumnMarkArr[$studentIDArr[$g]][$markColumnType][$reportColumnID]==='')
							{
								$AllColumnMarkArr[$studentIDArr[$g]][$markColumnType][$reportColumnID] = "N.A.";
								$calculationMark[$studentIDArr[$g]][$reportColumnID] = "N.A.";
							}
						}
					}
					#################################################
					
					// Loop 3: ReportColumns (Terms)
					for($n=0; $n<sizeof($reportColumnData); $n++)
					{
						$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
						$semester = $reportColumnData[$n]["SemesterNum"]; 
						
						$markColumnType = $reportColumnTypeAry[$reportColumnID];
						if($markColumnType == "") {
							continue;
						}
						
						// Check if this column already have consolidated data, if yes => skip
						if (isset($completedTermReportID[$reportColumnTermMap[$reportColumnID]])) {
							continue;
						}
						
						// Get Report Subject Result Score
						$marksheetScore = $this->GET_RESULT_SCORE_FOR_SUBJECT_OVERALL($studentIDArr, $subjectID, $reportColumnID, $ReportID);
						
						// Loop 4: Student's Score
						if (sizeof($marksheetScore) > 0)
						{
							foreach((array)$marksheetScore as $studentID => $scoreDetailInfo)
							{
								$rawMark = "";
								$consolidateMark = "";
								
								if ($scoreDetailInfo["MarkType"] == "M" || $scoreDetailInfo["MarkType"] == "G")
								{
									// Horizontal -> Vertical: Need to store weighted mark to calculate Subject Overall
									if ($scaleInput == "M" && $scaleDisplay == "M")
									{
// 										$rawMark = $scoreDetailInfo["MarkRaw"];
// 										$consolidateMark = $this->ROUND_MARK($scoreDetailInfo["MarkRaw"], "SubjectScore");
										
// 										// Check if consolidate mark need to be converted
// 										if ($calculationSettings["UseWeightedMark"] == "1")
// 										{
// 											$consolidateMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $consolidateMark);
// 										}
// 										$consolidateMark = $this->ROUND_MARK($consolidateMark, "SubjectScore");
										
										// Horizontal -> Vertical: Need to store weighted mark to calculate Subject Overall
										$calculationMark[$studentID][$reportColumnID] = $this->ROUND_MARK($scoreDetailInfo["MarkRaw"], "SubjectScore");
										$calculationMark[$studentID][$reportColumnID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $calculationMark[$studentID][$reportColumnID], 1, $AllColumnMarkArr[$studentID][$markColumnType], false, true);
									}
									else if ($scaleInput == "M" && $scaleDisplay == "G")
									{
// 										$rawMark = $scoreDetailInfo["MarkRaw"];
// 										if ($eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade'])
// 										{
// 											$rawMarkToConvert = $this->ROUND_MARK($rawMark, "SubjectScore");
// 										}
// 										else
// 										{
// 											$rawMarkToConvert = $rawMark;
// 										}
							
// 										// Convert to Grade
// 										$consolidateMark = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $rawMarkToConvert);
										
										// Horizontal -> Vertical: Need to store weighted mark to calculate Subject Overall
										$calculationMark[$studentID][$reportColumnID] = $this->ROUND_MARK($scoreDetailInfo["MarkRaw"], "SubjectScore");
										$calculationMark[$studentID][$reportColumnID] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $reportColumnID, $subjectID, $calculationMark[$studentID][$reportColumnID], 1, $AllColumnMarkArr[$studentID][$markColumnType], false, true);
										
									}
									else if ($scaleInput == "G" && $scaleDisplay == "G")
									{
// 										$consolidateMark = $schemeRangeIDGradeMap[$scoreDetailInfo["MarkNonNum"]];
										$calculationMark[$studentID][$reportColumnID] = "-";
									}
									
									// Round off the weighted mark
									if ($scaleDisplay == "M" && is_numeric($consolidateMark)) {
										$consolidateMark = $this->ROUND_MARK($consolidateMark, "SubjectScore");
									}
								}
								else if ($scoreDetailInfo["MarkType"] == "PF")
								{
// 									$consolidateMark = ($scoreDetailInfo["MarkNonNum"] == "P") ? $passWording : $failWording;
// 									$PassFailMark = $consolidateMark;
									$calculationMark[$studentID][$reportColumnID] = "-";
								}
								else
								{
									$specialCase = $scoreDetailInfo["MarkNonNum"];
									$calculationMark[$studentID][$reportColumnID] = $specialCase;
								}
							} // End Loop 4: Student's Score
						}
					} // End Loop 3: ReportColumns
					
					// Calculate Subject Overall Mark
					$subjectOverallValues = array();
					for($a=0; $a<sizeof($studentIDArr); $a++)
					{
						$assessmentTypeAry = array(-1, -2);
						$subjectAssessmentMark = array(-1 => "", -2 => "");
						$subjectAssessmentMarkConverted = array(-1 => "", -2 => "");
						$excludeAssessmentWeight = array(-1 => 0, -2 => 0);
						$excludeAssessmentFullMark = array(-1 => 0, -2 => 0);
						$subjectAssessmentTotalMark = array(-1 => 0, -2 => 0);
						$specialCaseAssessmentDifferentiate = array(-1 => array(), -2 => array());
						
						if (sizeof($calculationMark) > 0)
						{
							if (isset($calculationMark[$studentIDArr[$a]]))
							{
								// loop columns and sum up Overall Mark & Exclude Weight
								$markList = $calculationMark[$studentIDArr[$a]];
								foreach((array)$markList as $markColumnID => $markColumn)
								{
									$markColumnType = $reportColumnTypeAry[$markColumnID];
									if($markColumnType == "") {
										continue;
									}
									
									$subjectAssessmentTotalMark[$markColumnType] += $subjectFullMark * $reportColumnWeight[$markColumnID];
									
									// if any of the assessment marks is numeric, make sure $subjectOverallMark is initiate to zero
									if (is_numeric($markColumn)) {
										$subjectAssessmentMark[$markColumnType] = ($subjectAssessmentMark[$markColumnType] == "") ? 0 : $subjectAssessmentMark[$markColumnType];
										$subjectAssessmentMark[$markColumnType] += $markColumn;
									}
									else {
										if ($schemeType == "PF") {
											$specialCaseAssessmentDifferentiate[$markColumnType] = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseAssessmentDifferentiate[$markColumnType], $markColumn, $absentExemptSettings);
										}
										else {
											$specialCaseAssessmentDifferentiate[$markColumnType] = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseAssessmentDifferentiate[$markColumnType], $markColumn, $absentExemptSettings);
										}
										
										if ($specialCaseAssessmentDifferentiate[$markColumnType][$markColumn]["isExFullMark"] == 1) {
											$excludeAssessmentFullMark[$markColumnType] += $subjectFullMark * $reportColumnWeight[$markColumnID];
										}
										else if ($specialCaseAssessmentDifferentiate[$markColumnType][$markColumn]["isCount"] == 0) {
											$excludeAssessmentWeight[$markColumnType] += $reportColumnWeight[$markColumnID];
										}
									}
								}
							}
						}
						
						// if exist Consolidated Term Overall Mark (in the term report)
						$CopySubjectOverallSpecialCase = array(-1 => "", -2 => "");
						for($w=0; $w<sizeof($existTermSubjectOverallArr); $w++)
						{
							if (isset($existTermSubjectOverallArr[$w][$studentIDArr[$a]][$subjectID]))
							{
								$tmpTermOverallDetail = $existTermSubjectOverallArr[$w][$studentIDArr[$a]][$subjectID];
								for ($p=0; $p<sizeof($tmpTermOverallDetail); $p++)
								{
									$thisReportColumnID = $tmpTermOverallDetail[$p]["ReportColumnID"];
									$markColumnType = $reportColumnTypeAry[$thisReportColumnID];
									if($markColumnType == "") {
										continue;
									}
									
									$subjectAssessmentTotalMark[$markColumnType] += $subjectFullMark * $reportColumnWeight[$thisReportColumnID];
									
									# Use Raw Mark instead of Mark
									if ($scaleInput=="M" && $scaleDisplay=="G" && $tmpTermOverallDetail[$p]["RawMark"]!="" && $this->Check_If_Grade_Is_SpecialCase($tmpTermOverallDetail[$p]["Grade"])==false)
									{
										if ($calculationSettings["UseWeightedMark"] != "1") {
											$tmpTermOverallDetail[$p]["RawMark"] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $thisReportColumnID, $subjectID, $tmpTermOverallDetail[$p]["RawMark"], 1, $AllColumnMarkArr[$studentIDArr[$a]][$markColumnType], false, true);
										}
										else {
											//2013-0225-1227-23140: (Internal) Follow-up by karsonyam on 2013-07-19 17:42
											$tmpTermOverallDetail[$p]["RawMark"] = $tmpTermOverallDetail[$p]["Mark"];
										}
										
										$tmpTermReportID = $termReportIdMappingAry[$thisReportColumnID];
										$tmpSubjectFullMarkTerm = $subjectFullMarkMappingAry[$tmpTermReportID][$subjectID];
										$tmpSubjectFullMarkConsolidate = $subjectFullMarkMappingAry[$ReportID][$subjectID];
										if ($tmpSubjectFullMarkTerm > 0) {
											$tmpMark = ($tmpTermOverallDetail[$p]["RawMark"] / $tmpSubjectFullMarkTerm) * $tmpSubjectFullMarkConsolidate;
										}
										else {
											$tmpMark = $tmpTermOverallDetail[$p]["RawMark"];
										}
										
										$subjectAssessmentMark[$markColumnType] = ($subjectAssessmentMark[$markColumnType] == "") ? 0 : $subjectAssessmentMark[$markColumnType];
										$subjectAssessmentMark[$markColumnType] += $tmpMark;
									}
									else
									{
										if ($this->Check_If_Grade_Is_SpecialCase($tmpTermOverallDetail[$p]["Grade"])==false && is_numeric($tmpTermOverallDetail[$p]["RawMark"]))
										{
											// Only convert the mark if existing mark is consolidated as Raw mark
											if ($calculationSettings["UseWeightedMark"] != "1") {
												$tmpTermOverallDetail[$p]["RawMark"] = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $thisReportColumnID, $subjectID, $tmpTermOverallDetail[$p]["RawMark"], 1, $AllColumnMarkArr[$studentIDArr[$a]][$markColumnType], false, true);
											}
											
											$tmpTermReportID = $termReportIdMappingAry[$thisReportColumnID];
											$tmpSubjectFullMarkTerm = $subjectFullMarkMappingAry[$tmpTermReportID][$subjectID];
											$tmpSubjectFullMarkConsolidate = $subjectFullMarkMappingAry[$ReportID][$subjectID];
											if ($tmpSubjectFullMarkTerm > 0) {
												$tmpMark = ($tmpTermOverallDetail[$p]["RawMark"] / $tmpSubjectFullMarkTerm) * $tmpSubjectFullMarkConsolidate;
											}
											else {
												$tmpMark = $tmpTermOverallDetail[$p]["RawMark"];
											}
											
											$subjectAssessmentMark[$markColumnType] = ($subjectAssessmentMark[$markColumnType] == "") ? 0 : $subjectAssessmentMark[$markColumnType];
											$subjectAssessmentMark[$markColumnType] += $tmpMark;
										}
										else
										{
											$thisTmpTermOverallGrade = $tmpTermOverallDetail[$p]["Grade"];
											if ($thisTmpTermOverallGrade == "abs") {
												$specialCaseAssessmentDifferentiate[$markColumnType] = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseAssessmentDifferentiate[$markColumnType], $thisTmpTermOverallGrade, $absentExemptSettings);
											}
											else if (in_array($tmpTermOverallDetail[$p]["Grade"], $SpecialCaseArrH)) {
												$specialCaseAssessmentDifferentiate[$markColumnType] = $this->DIFFERENTIATE_SPECIAL_CASE($specialCaseAssessmentDifferentiate[$markColumnType], $thisTmpTermOverallGrade, $absentExemptSettings);
											}
											
											if ($specialCaseAssessmentDifferentiate[$markColumnType][$thisTmpTermOverallGrade]["isExFullMark"] == 1) {
                                                // [2020-0724-1423-23073] convert excluded full marks to weighted if have excluded column weight
                                                if ($calculationSettings["UseWeightedMark"] != "1") {
                                                    $excludeAssessmentFullMark[$markColumnType] += $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $thisReportColumnID, $subjectID, $subjectFullMark, 1, $AllColumnMarkArr[$studentIDArr[$a]][$markColumnType], false, true);
                                                }
                                                else {
                                                    $excludeAssessmentFullMark[$markColumnType] += $subjectFullMark * $reportColumnWeight[$thisReportColumnID];
                                                }
											}
											else {
												$excludeAssessmentWeight[$markColumnType] += $reportColumnWeight[$thisReportColumnID];
											}
											
											if ($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']==true)
											{
												$thisReportColumnOrder = "";
												if(isset($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase_ColumnOrderArr'][$thisTmpTermOverallGrade]['W'])) {
													$thisReportColumnOrder = $thisReportColumnID? $this->retrieveReportTemplateColumnOrder($ReportID, $thisReportColumnID) : "O";
												}
												if ($this->Check_Copy_Special_Case($thisTmpTermOverallGrade, 'W', $studentIDArr[$a], $thisReportColumnOrder))
												{
													$CopySubjectOverallSpecialCase[$markColumnType] = $thisTmpTermOverallGrade;
													break;
												}
											}
										}
									}
								}
							}
						}
						
						// Excluding Full Mark / Weight
						foreach($assessmentTypeAry as $thisMarkColumnType)
						{
							if ($subjectAssessmentMark[$thisMarkColumnType] !== "" && is_numeric($subjectAssessmentMark[$thisMarkColumnType]))
							{
								if ($excludeAssessmentFullMark[$thisMarkColumnType] > 0)
								{
									if ($subjectAssessmentTotalMark[$thisMarkColumnType] != 0)
									{
										// Exclude Full Mark
										$newFullMark = $subjectAssessmentTotalMark[$thisMarkColumnType] - $excludeAssessmentFullMark[$thisMarkColumnType];
										if ($newFullMark > 0)
										{
											$subjectAssessmentMark[$thisMarkColumnType] = $subjectAssessmentMark[$thisMarkColumnType] * $subjectAssessmentTotalMark[$thisMarkColumnType] / $newFullMark;
										}
									}
								}
							}
							
							// Copy back Special Case
							if ($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']==true && $CopySubjectOverallSpecialCase[$thisMarkColumnType] != '') {
								$subjectAssessmentMark[$thisMarkColumnType] = $CopySubjectOverallSpecialCase[$thisMarkColumnType];
							}
							
							// Subject Overall Mark can be calculated
							if ($subjectAssessmentMark[$thisMarkColumnType] !== "" && is_numeric($subjectAssessmentMark[$thisMarkColumnType]))
							{
								$subjectAssessmentMarkConverted[$thisMarkColumnType] = $subjectAssessmentMark[$thisMarkColumnType];
								$subjectOverallMarkRaw = $subjectAssessmentMark[$thisMarkColumnType];
								
								// Convert to Grade
								if ($scaleInput == "M" && $scaleDisplay == "G")
								{
									if ($eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade'])
									{
										$subjectOverallMarkToConvert = $this->ROUND_MARK($subjectAssessmentMark[$thisMarkColumnType], "SubjectTotal");
									}
									else
									{
										$subjectOverallMarkToConvert = $subjectAssessmentMark[$thisMarkColumnType];
									}
									$subjectAssessmentMarkConverted[$thisMarkColumnType] = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($subjectInfo["schemeID"], $subjectOverallMarkToConvert);
									$subjectAssessmentMark[$thisMarkColumnType] = $this->ROUND_MARK($subjectAssessmentMark[$thisMarkColumnType], "SubjectTotal");
								}
								else
								{
									// Adjust Subject Overall Mark according to Remaining Weight
									if ($totalReportAssessmentWeight[$thisMarkColumnType]== $excludeAssessmentWeight[$thisMarkColumnType]) {
										$subjectAssessmentMarkConverted[$thisMarkColumnType] = "-";
										$subjectOverallMarkRaw = 0;
									}
									else {
										$subjectAssessmentMark[$thisMarkColumnType] = $this->ROUND_MARK($subjectAssessmentMark[$thisMarkColumnType], "SubjectTotal");
									}
								}
							}
							else
							{
								if ($CopySubjectOverallSpecialCase[$thisMarkColumnType] != '') {
									$subjectAssessmentMarkConverted[$thisMarkColumnType] = $CopySubjectOverallSpecialCase[$thisMarkColumnType];
								}
								else if (sizeof($specialCaseAssessmentDifferentiate[$thisMarkColumnType]) == 1) {
									$onlySpecialCase = array_keys($specialCaseAssessmentDifferentiate[$thisMarkColumnType]);
									$subjectAssessmentMarkConverted[$thisMarkColumnType] = $onlySpecialCase[0];
								}
								else {
									$subjectAssessmentMark[$thisMarkColumnType] = 0;
								}
								$subjectOverallMarkRaw = 0;
							}
							
							if ($scaleDisplay == "M" && is_numeric($subjectAssessmentMark[$thisMarkColumnType]) && is_numeric($subjectAssessmentMarkConverted[$thisMarkColumnType]))
							{
								
								$subjectOverallValues[] = "('".$studentIDArr[$a]."', '$subjectID', '$ReportID', '$thisMarkColumnType', '".$subjectAssessmentMark[$thisMarkColumnType]."', '', '$subjectOverallMarkRaw', '1', 'F', '', '', NOW(), NOW())";
							} 
							else
							{
								$subjectOverallValues[] = "('".$studentIDArr[$a]."', '$subjectID', '$ReportID', '$thisMarkColumnType', '".$subjectAssessmentMark[$thisMarkColumnType]."', '".$subjectAssessmentMarkConverted[$thisMarkColumnType]."', '$subjectOverallMarkRaw', '1', 'F', '', '', NOW(), NOW())";
							}
						}
					}
					
					// Consolidate the Subject Overall Mark
					if (sizeof($subjectOverallValues) > 0) {
						$subjectOverallValues = implode(",", $subjectOverallValues);
						$success["INSERT_REPORT_RESULT_SCORE"] = $this->INSERT_REPORT_RESULT_SCORE($subjectOverallValues);
					}
				} // End Loop 2: Subjects
		}
	}
	
	/* 20100312 Ivan - Performance Tunning
	 * Use the new UPDATE_ORDER() function now to change all update statements to one insert statement
	// Helper function for UPDATE_ORDER() to avoid repeat writing similiar coding blocks
	function UPDATE_ORDER_METHOD($studentScore, $orderField, $table, $cond, $idMap, $excludeList=array(), $scaleInput='') {
		global $PATH_WRT_ROOT, $ReportCardCustomSchoolName;
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		
		$orderCount = 0;
		$numberCount = 0;
		$lastMark = -1;
		$success = array();
		$noOfStudent = sizeof($studentScore);
		
		// student in the excluded list will not be counted in the number of students in class / form
		if ($eRCTemplateSetting['DoNotCountExcludedStudentInStudentNumOfClassAndForm'] && count($excludeList) > 0)
		{
			foreach((array)$studentScore as $studentID => $mark) {
				if (in_array($studentID, $excludeList)) {
					$noOfStudent--;
				}
			}
		}
		
		if ($orderField == "OrderMeritClass")
			$noOfStudentField = "ClassNoOfStudent";
		else if ($orderField == "OrderMeritForm")
			$noOfStudentField = "FormNoOfStudent";
		else if ($orderField == "OrderMeritStream")
			$noOfStudentField = "StreamNoOfStudent";
		else if ($orderField == "OrderMeritSubjectGroup")
			$noOfStudentField = "SubjectGroupNoOfStudent";
			
		// Marks in $studentScore already sorted by highest mark
		foreach((array)$studentScore as $studentID => $mark) {
			// exclude student in the list or the subject is grade input, they will not be count in the ranking
			if (in_array($studentID, $excludeList) || $scaleInput=='M') {
				$sql = "UPDATE $table SET $orderField = '-1', $noOfStudentField = '$noOfStudent' WHERE $cond = ".$idMap[$studentID];
				$success[] = $this->db_db_query($sql);
				continue;
			}
			$numberCount++;
			
			if ($eRCTemplateSetting['RankingMethod'] == '1223')
			{
				# 1,2,2,3
				if ($mark != $lastMark)
					$orderCount++;
			}
			else
			{
				# 1,2,2,4
				if ($mark == $lastMark)
				{
					$orderCount = $lastOrderCount;
				}
				else
				{
					$orderCount = $numberCount;
				}
				$lastOrderCount = $orderCount;
			}
			
			$sql = "UPDATE $table SET $orderField = '$orderCount', $noOfStudentField = '$noOfStudent' WHERE $cond = ".$idMap[$studentID];
			$success[] = $this->db_db_query($sql);
			
			$lastMark = $mark;
		}

		return !in_array(false, $success);
	}
	
	// Update OrderMeritClass & OrderMeritForm field in the tables RC_REPORT_RESULT_SCORE & RC_REPORT_RESULT
	// Run once after consolidation of score finished in GENERATE_TERM_REPORTCARD() & GENERATE_WHOLE_YEAR_REPORTCARD()
	function UPDATE_ORDER($ReportID, $ClassLevelID) {
		global $PATH_WRT_ROOT, $eRCTemplateSetting;
		//include($PATH_WRT_ROOT."includes/eRCConfig.php");
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		####### Added on 20080708 #######
		// Get the list of students who will not be count when calculating order
		$excludeStudentsList = $this->GET_EXCLUDE_ORDER_STUDENTS();
		if (!is_array($excludeStudentsList))
			$excludeStudentsList = array();
		
		// Get Report Columns (Assessment)
		$reportColumnData = $this->returnReportTemplateColumnData($ReportID);
		if (sizeof($reportColumnData) > 0) {
			//$totalReportColumnWeight = 0;
			for($i=0; $i<sizeof($reportColumnData); $i++) {
				$reportColumnIDList[] = $reportColumnData[$i]["ReportColumnID"];
				$reportInvolveTerms[] = $reportColumnData[$i]["SemesterNum"];
				$reportColumnTermMap[$reportColumnData[$i]["ReportColumnID"]] = $reportColumnData[$i]["SemesterNum"];
				$reportTermColumnMap[$reportColumnData[$i]["SemesterNum"]] = $reportColumnData[$i]["ReportColumnID"];
			}
		}
		
		// Get Classes
		$classArr = $this->GET_CLASSES_BY_FORM($ClassLevelID);
		
		for($i=0; $i<sizeof($classArr); $i++) {
			// $studentArr[#ClassID][$i] + ["UserID"], ["WebSAMSRegNo"],...
			$studentArr[$classArr[$i]["ClassID"]] = $this->GET_STUDENT_BY_CLASS($classArr[$i]["ClassID"]);
		}
		// Get Subjects
		$subjectList = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		// Init the huge array for storing mark of entire class level
		$orderClassLevelList = array();
		
		// Init the huge array for storing mark of entire stream (ClassGroup)
		$orderStreamList = array();
		
		// Init the huge array for storing mark of entire subject group
		$orderSubjectGroupList = array();
		
		// Mapping between $StudentID & $ReportResultScoreID
		$studentIDScoreIDMap = array();
		
		// Get Form Subject Average and SD
		if($eRCTemplateSetting['OrderPositionMethod'] == 'SD') 
			list($SDAry,$AvgAry) = $this->getFormSubjectSDAndAverage($ReportID);
			
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		
		// Loop 1: Classes
		// Sort class order of individual subject
		for($i=0; $i<sizeof($classArr); $i++) {
			$classID = $classArr[$i]["ClassID"];
			$StreamNo = $classArr[$i]["ClassGroupID"];
			
			$studentIDArr = array();
			for($j=0; $j<sizeof($studentArr[$classID]); $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			$studentIDArrList = implode(",", $studentIDArr);
			// Loop 2: Subjects
			foreach((array)$subjectList as $subjectID => $subjectInfo) {
				$scaleInput = $subjectInfo["ScaleInput"];
				$scaleDisplay = $subjectInfo["ScaleDisplay"];
				//if ($scaleInput != "M") continue;
				// Loop 3: Report Columns
				for($n=0; $n<sizeof($reportColumnData); $n++) {
					$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
					if (!isset($orderClassLevelList[$subjectID][$reportColumnID]))
						$orderClassLevelList[$subjectID][$reportColumnID] = array();
					if ($StreamNo && !isset($orderStreamList[$StreamNo][$subjectID][$reportColumnID]))
						$orderStreamList[$StreamNo][$subjectID][$reportColumnID] = array();
					$sql = "SELECT ReportResultScoreID, StudentID, RawMark, SDScore FROM $table ";
					$sql .= " WHERE ReportID = '$ReportID' AND ReportColumnID = '$reportColumnID' ";
					$sql .= " AND SubjectID = '$subjectID' AND StudentID IN ($studentIDArrList) ";
					$sql .= " AND Grade != 'N.A.' ";
					$result = $this->returnArray($sql, 3);
					
					// Skip if no result
					if (sizeof($result) <= 0) continue;
					$studentScore = array();
					$studentSDScore = array();
					for($j=0; $j<sizeof($result); $j++) {
						$studentIDScoreIDMap[$subjectID][$reportColumnID][$result[$j]["StudentID"]] = $result[$j]["ReportResultScoreID"];
						$studentScore[$result[$j]["StudentID"]] = $result[$j]["RawMark"];
						$studentSDScore[$result[$j]["StudentID"]] = $result[$j]["SDScore"];
					}
					if($eRCTemplateSetting['OrderPositionMethod'] == 'SD')
					{
						$SD = $SDAry[$subjectID];
						$Avg = $AvgAry[$subjectID];
						//list($SD,$Avg) = $this->getFormSubjectSDAndAverage($ReportID,$subjectID);
						foreach((array) $studentScore as $key=>$thisScore)
						{
							$studentScore[$key] = $SD==0?0:($thisScore-$Avg)/$SD;
						}
					}
					else if($eRCTemplateSetting['OrderPositionMethod'] == 'WeightedSD')
					{
						$studentScore = $studentSDScore;
					}
					arsort($studentScore);
					
					### ClassLevel ranking array
					$orderClassLevelList[$subjectID][$reportColumnID] = $orderClassLevelList[$subjectID][$reportColumnID] + $studentScore;
					
					### Class Group ranking array
					if(!empty($StreamNo))
						$orderStreamList[$StreamNo][$subjectID][$reportColumnID] = $orderStreamList[$StreamNo][$subjectID][$reportColumnID] + $studentScore;
						
					### Subject Group ranking array
					if ($ReportType == 'T')
					{
						foreach ((array)$studentScore as $thisStudentID => $thisRawMark)
						{
							$thisSubjectGroupID = $this->Get_Student_Studying_Subject_Group($SemID, $thisStudentID, $subjectID);
							
							if ($thisSubjectGroupID != '')
							{
								$orderSubjectGroupList[$thisSubjectGroupID][$subjectID][$reportColumnID][$thisStudentID] = $thisRawMark;
							}
						}
					}
					
					$this->UPDATE_ORDER_METHOD($studentScore, "OrderMeritClass", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
				} // Loop 3 End : Report Columns
			} // Loop 2 End: Subjects
		} // Loop 1 End: Classes
		
		
		// Sort class level order of individual subject
		foreach((array)$orderClassLevelList as $subjectID => $reportColumnMarkList) {
			$scaleInput = $subjectList[$subjectID]['ScaleInput'];
			foreach((array)$reportColumnMarkList as $reportColumnID => $studentMarkList) {
				// Skip if no result
				if (sizeof($studentMarkList) <= 0) continue;
				arsort($studentMarkList);
				
				$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritForm", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
			}
		}
		
		// Sort stream order of individual subject
		if (count($orderStreamList) > 0)
		{
			foreach((array)$orderStreamList as $StreamNo => $orderClassLevelList){
				foreach((array)$orderClassLevelList as $subjectID => $reportColumnMarkList) {
					$scaleInput = $subjectList[$subjectID]['ScaleInput'];
					foreach((array)$reportColumnMarkList as $reportColumnID => $studentMarkList) {
						// Skip if no result
						if (sizeof($studentMarkList) <= 0) continue;
						arsort($studentMarkList);
						$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritStream", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
					}
				}
			}
		}
		
		// Sort subject group order of individual subject
		if ($ReportType == 'T')
		{
			foreach((array)$orderSubjectGroupList as $subjectGroupID => $orderClassLevelList){
				foreach((array)$orderClassLevelList as $subjectID => $reportColumnMarkList) {
					$scaleInput = $subjectList[$subjectID]['ScaleInput'];
					foreach((array)$reportColumnMarkList as $reportColumnID => $studentMarkList) {
						// Skip if no result
						if (sizeof($studentMarkList) <= 0) continue;
						arsort($studentMarkList);
						$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritSubjectGroup", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
					}
				}
			}
		}
		
		
		
		$orderClassLevelList = array();
		$orderStreamList = array();
		$orderSubjectGroupList = array();
		
		// Loop 1: Classes
		// Sort class order of individual subject overall mark
		for($i=0; $i<sizeof($classArr); $i++) {
			$classID = $classArr[$i]["ClassID"];
			$StreamNo = $classArr[$i]["ClassGroupID"];
			$studentIDArr = array();
			for($j=0; $j<sizeof($studentArr[$classID]); $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			$studentIDArrList = implode(",", $studentIDArr);
			// Loop 2: Subjects
			foreach((array)$subjectList as $subjectID => $subjectInfo) {
				$scaleInput = $subjectInfo["ScaleInput"];
				$scaleDisplay = $subjectInfo["ScaleDisplay"];
				//if ($scaleInput != "M") continue;
				$reportColumnID = 0;
				if (!isset($orderClassLevelList[$subjectID][$reportColumnID]))
					$orderClassLevelList[$subjectID][$reportColumnID] = array();
				if ($StreamNo && !isset($orderStreamList[$StreamNo][$subjectID][$reportColumnID]))
					$orderStreamList[$StreamNo][$subjectID][$reportColumnID] = array();
				
				$sql = "SELECT ReportResultScoreID, StudentID, RawMark, SDScore FROM $table ";
				$sql .= "WHERE ReportID = '$ReportID' AND ReportColumnID = '$reportColumnID' ";
				$sql .= "AND SubjectID = '$subjectID' AND StudentID IN ($studentIDArrList)";
				$sql .= " AND Grade != 'N.A.' ";
				$result = $this->returnArray($sql, 3);
				
				// Skip if no result
				if (sizeof($result) <= 0) continue;
				$studentScore = array();
//				for($j=0; $j<sizeof($result); $j++) {
//					$studentIDScoreIDMap[$subjectID][$reportColumnID][$result[$j]["StudentID"]] = $result[$j]["ReportResultScoreID"];
//					$studentScore[$result[$j]["StudentID"]] = $result[$j]["RawMark"];
//				}
//
//				if($eRCTemplateSetting['OrderPositionMethod'] == 'SD')
//				{
//					list($SD,$Avg) = $this->getFormSubjectSDAndAverage($ReportID,$subjectID);
//					foreach((array) $studentScore as $key=>$thisScore)
//					{
//						$studentScore[$key] = $SD==0?0:($thisScore-$Avg)/$SD;
//					}
//				}

				$studentScore = array();
				$studentSDScore = array();
				for($j=0; $j<sizeof($result); $j++) {
					$studentIDScoreIDMap[$subjectID][$reportColumnID][$result[$j]["StudentID"]] = $result[$j]["ReportResultScoreID"];
					$studentScore[$result[$j]["StudentID"]] = $result[$j]["RawMark"];
					$studentSDScore[$result[$j]["StudentID"]] = $result[$j]["SDScore"];
				}
				
				if($eRCTemplateSetting['OrderPositionMethod'] == 'SD')
				{
					$SD = $SDAry[$subjectID];
					$Avg = $AvgAry[$subjectID];
					//list($SD,$Avg) = $this->getFormSubjectSDAndAverage($ReportID,$subjectID);
					foreach((array) $studentScore as $key=>$thisScore)
					{
						$studentScore[$key] = $SD==0?0:($thisScore-$Avg)/$SD;
					}
				}
				else if($eRCTemplateSetting['OrderPositionMethod'] == 'WeightedSD')
				{
					$studentScore = $studentSDScore;
				}
				arsort($studentScore);
				### Class Level ranking array
				$orderClassLevelList[$subjectID][$reportColumnID] = $orderClassLevelList[$subjectID][$reportColumnID] + $studentScore;
				
				### Class Group ranking array
				if(!empty($StreamNo))
					$orderStreamList[$StreamNo][$subjectID][$reportColumnID] = $orderStreamList[$StreamNo][$subjectID][$reportColumnID] + $studentScore;
					
				### Subject Group ranking array
				if ($ReportType == 'T')
				{
					foreach ((array)$studentScore as $thisStudentID => $thisRawMark)
					{
						$thisSubjectGroupID = $this->Get_Student_Studying_Subject_Group($SemID, $thisStudentID, $subjectID);
						
						if ($thisSubjectGroupID != '')
						{
							$orderSubjectGroupList[$thisSubjectGroupID][$subjectID][$reportColumnID][$thisStudentID] = $thisRawMark;
						}
					}
				}
				
				$this->UPDATE_ORDER_METHOD($studentScore, "OrderMeritClass", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
			} // Loop 2 End: Subjects
		} // Loop 1 End: Classes
		
		// Sort class level order of individual subject overall mark
		foreach((array)$orderClassLevelList as $subjectID => $reportColumnMarkList) {
			$reportColumnID = 0;
			$studentMarkList = $reportColumnMarkList[0];
			$scaleInput = $subjectList[$subjectID]['ScaleInput'];

			// Skip if no result
			if (sizeof($studentMarkList) <= 0) continue;
			arsort($studentMarkList);
			$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritForm", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
		}
		
		if (count($orderStreamList) > 0)
		{
			foreach((array)$orderStreamList as $StreamNo => $orderClassLevelList){
					foreach((array)$orderClassLevelList as $subjectID => $reportColumnMarkList) {
					$reportColumnID = 0;
					$studentMarkList = $reportColumnMarkList[0];
					$scaleInput = $subjectList[$subjectID]['ScaleInput'];
		
					// Skip if no result
					if (sizeof($studentMarkList) <= 0) continue;
					arsort($studentMarkList);
					$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritStream", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
				}
			}
		}
		
		if ($ReportType == 'T')
		{
			foreach((array)$orderSubjectGroupList as $subjectGroupID => $orderClassLevelList){
					foreach((array)$orderClassLevelList as $subjectID => $reportColumnMarkList) {
					$reportColumnID = 0;
					$studentMarkList = $reportColumnMarkList[0];
					$scaleInput = $subjectList[$subjectID]['ScaleInput'];
		
					// Skip if no result
					if (sizeof($studentMarkList) <= 0) continue;
					arsort($studentMarkList);
					$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritSubjectGroup", $table, "ReportResultScoreID", $studentIDScoreIDMap[$subjectID][$reportColumnID], $excludeStudentsList, $scaleInput);
				}
			}
		}
		
		
		
		// Switch table
		$table = $this->DBName.".RC_REPORT_RESULT";
		
		// Mapping between $StudentID & $ResultID
		$studentIDResultIDMap = array();
		
		// Init the huge array for storing mark of entire class level
		$orderClassLevelList = array();
		
		// Init the huge array for storing mark of entire stream (ClassGroup)
		$orderStreamList = array();
		
		// Set field to determine position
		$orderField = ($eRCTemplateSetting['OrderingPositionBy']=="")? "GrandAverage" : $eRCTemplateSetting['OrderingPositionBy'];
		
		// Sort class order of term/assessment total
		for($i=0; $i<sizeof($classArr); $i++) {
			$classID = $classArr[$i]["ClassID"];
			$StreamNo = $classArr[$i]["ClassGroupID"];
			$studentIDArr = array();
			
			for($j=0; $j<sizeof($studentArr[$classID]); $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			$studentIDArrList = implode(",", $studentIDArr);
			
			for($n=0; $n<sizeof($reportColumnData); $n++) {
				$reportColumnID = $reportColumnData[$n]["ReportColumnID"];
				// Term/assessment total : have ReportColumnID
				$sql = "SELECT ResultID, StudentID, GrandTotal, GrandAverage, GPA, GrandSDScore FROM $table ";
				$sql .= "WHERE ReportID = '$ReportID' AND StudentID IN ($studentIDArrList) ";
				$sql .= "AND ReportColumnID = '$reportColumnID'";
				$result = $this->returnArray($sql, 5);
				
				// Skip if no result
				if (sizeof($result) <= 0) continue;
				$studentScore = array();
				$studentSDScore = array();
				for($k=0; $k<sizeof($result); $k++) { 
					$studentIDResultIDMap[$reportColumnID][$result[$k]["StudentID"]] = $result[$k]["ResultID"];
					$studentScore[$result[$k]["StudentID"]] = $result[$k][$orderField];
				}
				
				if($eRCTemplateSetting['OrderPositionMethod'] == 'SD')
				{
					$SD = $SDAry[$orderField];
					$Avg = $AvgAry[$orderField];
					//list($SD,$Avg) = $this->getFormSubjectSDAndAverage($ReportID,$subjectID);
					foreach((array) $studentScore as $key=>$thisScore)
					{
						$studentScore[$key] = $SD==0?0:($thisScore-$Avg)/$SD;
					}
				}
				arsort($studentScore);
				// Init array
				if (!isset($orderClassLevelList[$reportColumnID]))
					$orderClassLevelList[$reportColumnID] = array();
				if ($StreamNo && !isset($orderStreamList[$StreamNo][$reportColumnID]))
					$orderStreamList[$StreamNo][$reportColumnID] = array();
				
				### ClassLevel ranking array
				$orderClassLevelList[$reportColumnID] = $orderClassLevelList[$reportColumnID] + $studentScore;
				
				### Class Group ranking array
				if(!empty($StreamNo))
					$orderStreamList[$StreamNo][$reportColumnID] = $orderStreamList[$StreamNo][$reportColumnID] + $studentScore;
				
				$this->UPDATE_ORDER_METHOD($studentScore, "OrderMeritClass", $table, "ResultID", $studentIDResultIDMap[$reportColumnID], $excludeStudentsList);
				
			}
		}
		
		// Sort class level order of term/assessment total
		foreach((array)$orderClassLevelList as $reportColumnID => $studentMarkList) {
			// Skip if no result
			if (sizeof($studentMarkList) <= 0) continue;
			arsort($studentMarkList);
			$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritForm", $table, "ResultID", $studentIDResultIDMap[$reportColumnID], $excludeStudentsList);
		}
		
		// Sort stream order of term/assessment total
		if (count($orderStreamList) > 0)
		{
			foreach((array)$orderStreamList as $StreamNo => $orderClassLevelList) {
				foreach((array)$orderClassLevelList as $reportColumnID => $studentMarkList) {
					// Skip if no result
					if (sizeof($studentMarkList) <= 0) continue;
					arsort($studentMarkList);
					$this->UPDATE_ORDER_METHOD($studentMarkList, "OrderMeritStream", $table, "ResultID", $studentIDResultIDMap[$reportColumnID], $excludeStudentsList);
				}
			}
		}
		
		
		// Mapping between $StudentID & $ResultID
		$studentIDResultIDMap = array();
		
		// Init the huge array for storing mark of entire class level
		$orderClassLevelList = array();

		// Init the huge array for storing mark of entire stream (ClassGroup)
		$orderStreamList = array();
		
		// Sort class order of grand total
		for($i=0; $i<sizeof($classArr); $i++) {
			$classID = $classArr[$i]["ClassID"];
			$StreamNo = $classArr[$i]["ClassGroupID"];
			$studentIDArr = array();
			for($j=0; $j<sizeof($studentArr[$classID]); $j++) {
				$studentIDArr[] = $studentArr[$classID][$j]["UserID"];
			}
			$studentIDArrList = implode(",", $studentIDArr);
			
			$sql = "SELECT ResultID, StudentID, GrandTotal, GrandAverage, GPA, GrandSDScore FROM $table ";
			$sql .= "WHERE ReportID = '$ReportID' AND StudentID IN ($studentIDArrList) ";
			$sql .= "AND (ReportColumnID IS NULL OR ReportColumnID = '' OR ReportColumnID = '0')";
			$result = $this->returnArray($sql, 5);
			
			// Skip if no result
			if (sizeof($result) <= 0) continue;
			$studentScore = array();
			for($k=0; $k<sizeof($result); $k++) {
				$studentIDResultIDMap[$result[$k]["StudentID"]] = $result[$k]["ResultID"];
				$studentScore[$result[$k]["StudentID"]] = $result[$k][$orderField];
			}
			
			if($eRCTemplateSetting['OrderPositionMethod'] == 'SD')
			{
				$SD = $SDAry[$orderField];
				$Avg = $AvgAry[$orderField];
				//list($SD,$Avg) = $this->getFormSubjectSDAndAverage($ReportID,$subjectID);
				foreach((array) $studentScore as $key=>$thisScore)
				{
					$studentScore[$key] = $SD==0?0:($thisScore-$Avg)/$SD;
				}
			}
			arsort($studentScore);
			$orderClassLevelList = $orderClassLevelList + $studentScore;
			if ($StreamNo && !isset($orderStreamList[$StreamNo]))
				$orderStreamList[$StreamNo] = array();
			if(!empty($StreamNo))
				$orderStreamList[$StreamNo] = $orderStreamList[$StreamNo] + $studentScore;
				
			$this->UPDATE_ORDER_METHOD($studentScore, "OrderMeritClass", $table, "ResultID", $studentIDResultIDMap, $excludeStudentsList);
		}
		
		// Sort class level order of grand total
		if (sizeof($orderClassLevelList) > 0) {
			arsort($orderClassLevelList);
			$this->UPDATE_ORDER_METHOD($orderClassLevelList, "OrderMeritForm", $table, "ResultID", $studentIDResultIDMap, $excludeStudentsList);
		}
		
		if (count($orderStreamList) > 0)
		{
			foreach((array)$orderStreamList as $StreamNo => $orderClassLevelList) {
				if (sizeof($orderClassLevelList) > 0) {
					arsort($orderClassLevelList);		
					$this->UPDATE_ORDER_METHOD($orderClassLevelList, "OrderMeritStream", $table, "ResultID", $studentIDResultIDMap, $excludeStudentsList);
				}
			}
		}
	}
	*/
	
	function UPDATE_ORDER($ReportID, $ClassLevelID, $subjectRankingField="", $grandRankingField="", $notReplaceExistingOrder=false)
	{
		global $eRCTemplateSetting;
		
		$SuccessArr = array();
		
		### Get Report Info
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$YearTermID = $ReportSetting['Semester'];
		$ReportType = $YearTermID == "F"? "W" : "T";
		$isMainReport = $ReportSetting['isMainReport'];
		
		### Get all Students Info
		$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ParClassID="", $ParStudentID="", $ReturnAsso=1);
		
		// Subject Stream Ordering
		if($eRCTemplateSetting['Settings']['StudentSettings_SubjectStream']){
			$studentIDAry = array_keys($StudentInfoArr);
			$ssInfoAry = $this->GET_STUDENT_SUBJECT_STREAM($studentIDAry);
			$studentssInfoAry = BuildMultiKeyAssoc($ssInfoAry, 'StudentID');
		}

		### Get all classes info
		$YearClassInfoArr = $this->GET_CLASSES_BY_FORM($ClassLevelID, "", $returnAssoName=0, $returnAssoInfo=1);
		
		### Get the list of students who will not be count when calculating order
//		$ExcludeStudentsArr = $this->GET_EXCLUDE_ORDER_STUDENTS();
//		if (!is_array($ExcludeStudentsArr))
//			$ExcludeStudentsArr = array();
		$ExcludeStudentsAssoArr = array();
		if ($ReportType == 'T') {
			$ExcludeStudentsAssoArr[0] = $this->GET_EXCLUDE_ORDER_STUDENTS($ReportID);
		}
		else {
			$termReportInfoAry = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
			$numOfTermReport = count($termReportInfoAry);
			for ($i=0; $i<$numOfTermReport; $i++) {
				$_termReportId = $termReportInfoAry[$i]['ReportID'];
				$_reportColumnId = $termReportInfoAry[$i]['ReportColumnID'];
				
				$ExcludeStudentsAssoArr[$_reportColumnId] = $this->GET_EXCLUDE_ORDER_STUDENTS($_termReportId);
			}
			$ExcludeStudentsAssoArr[0] = $this->GET_EXCLUDE_ORDER_STUDENTS($ReportID);
		}
		
		### Get Report Column for exclude ordering checking
		if($eRCTemplateSetting['ExcludeFromOrder_ExamAbsOrExempt'])
		{
			// Term Report: Get Report Column Size
			if ($ReportType=="T")
			{
				$TermReportColumnList = $this->returnReportTemplateColumnData($ReportID);
				$FirstReportColumnID = $TermReportColumnList[0]["ReportColumnID"];
				$LastReportColumnIndex = count((array)$TermReportColumnList) - 1;
			}
			// Year Report: Get Student need to excluded from ordering
			else
			{
				// Get Related Term Report
				if(empty($termReportInfoAry)) {
					$termReportInfoAry = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
				}
				$TermReportCount = count($termReportInfoAry);
				
				// loop Term Report
				$ReportColumnGradeResult = array();
				$ReportColumnGradeResult2 = array();
				$ReportColumnGradeResult3 = array();
				for($i=0; $i<$TermReportCount; $i++)
				{
					$thisTermReportID = $termReportInfoAry[$i]['ReportID'];
					$thisTermReportColumnID = $termReportInfoAry[$i]['ReportColumnID'];
					
					// Get Related Term Report Last Column
					$TermReportColumnList = (array)$this->returnReportTemplateColumnData($thisTermReportID);
					if(!empty($TermReportColumnList))
					{
					    $FirstReportColumnID = $TermReportColumnList[0]["ReportColumnID"];
						$LastReportColumn = end($TermReportColumnList);
						$LastReportColumnID = $LastReportColumn["ReportColumnID"];
						
						// Get Students with absent or exempt in Term Report Last Column
						$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
						$sql = "SELECT StudentID, SubjectID FROM ".$table."
									WHERE ReportID = '".$thisTermReportID."' AND ReportColumnID = '".$LastReportColumnID."' AND Grade IN ('+', '/')";
						$result = $this->returnArray($sql);
						$result = BuildMultiKeyAssoc($result, array("SubjectID", "StudentID"), "StudentID", 1, 1);
						
						// Store StudentID
						$ReportColumnGradeResult[$thisTermReportColumnID] = $result;
						
						// Get Students with absent in Term Report First Column
						$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
						$sql = "SELECT StudentID, SubjectID FROM ".$table."
									WHERE ReportID = '".$thisTermReportID."' AND ReportColumnID = '".$FirstReportColumnID."' AND Grade = '-' ";
						$result = $this->returnArray($sql);
						$result = BuildMultiKeyAssoc($result, array("SubjectID", "StudentID"), "StudentID", 1, 1);
						
						// Store StudentID
						$ReportColumnGradeResult2[$thisTermReportColumnID] = $result;
						
						// Get Students with absent in Term Report Last Column
						$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
						$sql = "SELECT StudentID, SubjectID FROM ".$table."
									WHERE ReportID = '".$thisTermReportID."' AND ReportColumnID = '".$LastReportColumnID."' AND Grade = '-' ";
						$result = $this->returnArray($sql);
						$result = BuildMultiKeyAssoc($result, array("SubjectID", "StudentID"), "StudentID", 1, 1);
						
						// Store StudentID
						$ReportColumnGradeResult3[$thisTermReportColumnID] = $result;
					}
				}
			}
		}
		
		### Get Subjects Grading Scheme Info
		$SubjectGradingArr = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, $WithGrandMark=1, $ReportID);
		
		### Get all subject marks (from highest to lowest mark for ranking)
		$RankingField = 'RawMark';
		if ($eRCTemplateSetting['OrderPositionMethod'] == 'WeightedSD') {
			$RankingField = 'SDScore';
		}
		
		// [2015-0421-1144-05164] Update $RankingField if $subjectRankingField is set
		if(trim($subjectRankingField)!="") {
			$RankingField = $subjectRankingField;
		}
		
		// [2015-0421-1144-05164] Update SD Order if $subjectRankingField = 'SDScore'
		$storedSubjectBySD = "";
		if(trim($subjectRankingField)!="" && $subjectRankingField=='SDScore') {
			$storedSubjectBySD = "BySD";
		}
		
		### Since the data in the table will be deleted and inserted again in a batch,
		### we should select all records here and do filtering in the later part of the code
		$subject_score_table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$sql = "Select
						*
				From
						$subject_score_table
				Where
						ReportID = '".$ReportID."'
						
				Order By
						$RankingField Desc 
				";
		$SubjectScoreArr = $this->returnArray($sql, null, $ReturnAssoOnly=1);
		if($eRCTemplateSetting['ExcludeFromOrder_ExamAbsOrExempt'] && $ReportType=="T")
		{
		    $SubjectColumnScoreAssoc = BuildMultiKeyAssoc((array)$SubjectScoreArr, array('ReportColumnID', 'StudentID', 'SubjectID'));
		}
		
		$SubjectGroupInfoArr = array();
		$SubjectScoreAssoArr = array();
		$SubjectFormMarkArr = array();
		$SubjectClassMarkArr = array();
		$SubjectSubjectGroupMarkArr = array();
		$SubjectStreamMarkArr = array();
		$SubjectExemptStudentList = array();
		
		$CountedFormStudentIDList = array();
		$CountedClassStudentIDList = array();
		$NotCountedFormStudentIDList = array();
		$NotCountedClassStudentIDList = array();
		
		foreach ((array)$SubjectScoreArr as $key => $thisSubjectScoreArr)
		{
			$thisReportResultScoreID 	= $thisSubjectScoreArr['ReportResultScoreID'];
			$thisReportColumnID 		= $thisSubjectScoreArr['ReportColumnID'];
			$thisStudentID 				= $thisSubjectScoreArr['StudentID'];
			$thisSubjectID 				= $thisSubjectScoreArr['SubjectID'];
			$thisParentSubjectID 		= $this->GET_PARENT_SUBJECT_ID($thisSubjectID);
			
			# Add records to the associate array so that all marks can be inserted again after the ranking process
			$SubjectScoreAssoArr[$thisReportResultScoreID] = $thisSubjectScoreArr;
			
			# Get the ranking determine mark
			if ($eRCTemplateSetting['OrderPositionMethod'] == 'WeightedSD') {
				$thisRankingMark = $thisSubjectScoreArr['SDScore'];
			} else {
				$thisRankingMark = $thisSubjectScoreArr['RawMark'];
			}
			
			// [2015-0421-1144-05164] Update $thisRankingMark if $subjectRankingField is set
			if(trim($subjectRankingField)!="") {
				$thisRankingMark = $thisSubjectScoreArr[$subjectRankingField];
			}
			
			$thisGrade = $thisSubjectScoreArr['Grade'];
			
			# Get Student Class Info
			$thisYearClassID = $StudentInfoArr[$thisStudentID]['YearClassID'];
			$thisClassGroupID = $YearClassInfoArr[$thisYearClassID]['ClassGroupID'];
			
			if($eRCTemplateSetting['Settings']['StudentSettings_SubjectStream']){
				$thisClassGroupID = $studentssInfoAry[$thisStudentID]['SubjectStream'];
				$thisClassGroupID = ord($thisClassGroupID);
			}
			
			# Build Form Ranking Array
			(array)$SubjectFormMarkArr[$thisSubjectID][$thisReportColumnID]['KeyIDArr'][] = $thisReportResultScoreID;
			(array)$SubjectFormMarkArr[$thisSubjectID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
			(array)$SubjectFormMarkArr[$thisSubjectID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
			(array)$SubjectFormMarkArr[$thisSubjectID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
			
			# Build Class Ranking Array
			(array)$SubjectClassMarkArr[$thisYearClassID][$thisSubjectID][$thisReportColumnID]['KeyIDArr'][] = $thisReportResultScoreID;
			(array)$SubjectClassMarkArr[$thisYearClassID][$thisSubjectID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
			(array)$SubjectClassMarkArr[$thisYearClassID][$thisSubjectID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
			(array)$SubjectClassMarkArr[$thisYearClassID][$thisSubjectID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
            
			# Build Subject Group Ranking Array for Term Report
			if ($ReportType == 'T')
			{
				if ($SubjectGroupInfoArr[$thisStudentID][$thisSubjectID] == '') {
					$SubjectGroupInfoArr[$thisStudentID][$thisSubjectID] = $this->Get_Student_Studying_Subject_Group($YearTermID, $thisStudentID, $thisSubjectID);
				}
				
				$thisSubjectGroupID = $SubjectGroupInfoArr[$thisStudentID][$thisSubjectID];
				if ($SubjectGroupInfoArr[$thisStudentID][$thisSubjectID] != '')
				{
					(array)$SubjectSubjectGroupMarkArr[$thisSubjectGroupID][$thisSubjectID][$thisReportColumnID]['KeyIDArr'][] = $thisReportResultScoreID;
					(array)$SubjectSubjectGroupMarkArr[$thisSubjectGroupID][$thisSubjectID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
					(array)$SubjectSubjectGroupMarkArr[$thisSubjectGroupID][$thisSubjectID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
					(array)$SubjectSubjectGroupMarkArr[$thisSubjectGroupID][$thisSubjectID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
				}
			}
			
			# Build Subject Group Ranking Array for Year Report
			if($eRCTemplateSetting['Report']['ReportGeneration']['YearReportHasSubjectGroupOrdering'] && $ReportType == 'W')
			{
			    if ($SubjectGroupInfoArr[$thisStudentID][$thisSubjectID] == '') {
			        for ($i=($numOfTermReport-1); $i>=0; $i--) {
			            $_termReportSemId = $termReportInfoAry[$i]['Semester'];
			            if($_termReportSemId) {
			                $_termReportSubjectGroupId = $this->Get_Student_Studying_Subject_Group($_termReportSemId, $thisStudentID, $thisSubjectID);
			                if($_termReportSubjectGroupId) {
			                    $SubjectGroupInfoArr[$thisStudentID][$thisSubjectID] = $_termReportSubjectGroupId;
			                    break;
			                }
			            }
			        }
			    }
			    
			    $thisSubjectGroupID = $SubjectGroupInfoArr[$thisStudentID][$thisSubjectID];
			    if ($SubjectGroupInfoArr[$thisStudentID][$thisSubjectID] != '')
			    {
			        (array)$SubjectSubjectGroupMarkArr[$thisSubjectGroupID][$thisSubjectID][$thisReportColumnID]['KeyIDArr'][] = $thisReportResultScoreID;
			        (array)$SubjectSubjectGroupMarkArr[$thisSubjectGroupID][$thisSubjectID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
			        (array)$SubjectSubjectGroupMarkArr[$thisSubjectGroupID][$thisSubjectID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
			        (array)$SubjectSubjectGroupMarkArr[$thisSubjectGroupID][$thisSubjectID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
			    }
			}
            
			# Build Stream Ranking Array
			if ($thisClassGroupID != '' && $thisClassGroupID != 0)
			{
				(array)$SubjectStreamMarkArr[$thisClassGroupID][$thisSubjectID][$thisReportColumnID]['KeyIDArr'][] = $thisReportResultScoreID;
				(array)$SubjectStreamMarkArr[$thisClassGroupID][$thisSubjectID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
				(array)$SubjectStreamMarkArr[$thisClassGroupID][$thisSubjectID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
				(array)$SubjectStreamMarkArr[$thisClassGroupID][$thisSubjectID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
			}
			
			# Check if students need to exempt from ordering 
			if($eRCTemplateSetting['ExcludeFromOrder_ExamAbsOrExempt'] && $thisReportColumnID != 0)
			{
				// Term Report
				if($ReportType=="T")
				{
					// Get Report Column Order
					$currentReportColumnOrder = $this->retrieveReportTemplateColumnOrder($ReportID, $thisReportColumnID);
					$isLastReportColumn = $currentReportColumnOrder==$LastReportColumnIndex;
					
					// Last Column (Exam) + Absent/Exempted
					if($isLastReportColumn && in_array($thisGrade, array("+", "-", "/")))
					{
					    if($thisGrade == "-")
					    {
					        $firstColumnGrade = $SubjectColumnScoreAssoc[$FirstReportColumnID][$thisStudentID][$thisSubjectID]['Grade'];
					        if($firstColumnGrade == "-")
					        {
					            $SubjectExemptStudentList['Overall'][$FirstReportColumnID][$thisStudentID] = true;
					            $SubjectExemptStudentList['Overall'][$thisReportColumnID][$thisStudentID] = true;
					            $SubjectExemptStudentList['Overall'][0][$thisStudentID] = true;
					            $SubjectExemptStudentList[$thisSubjectID][0][$thisStudentID] = true;
					            if(!empty($thisParentSubjectID)) {
					                $SubjectExemptStudentList[$thisParentSubjectID][0][$thisStudentID] = true;
					            }
					        }
					    }
					    else
					    {
    						$SubjectExemptStudentList['Overall'][$thisReportColumnID][$thisStudentID] = true;
    						$SubjectExemptStudentList['Overall'][0][$thisStudentID] = true;
    						$SubjectExemptStudentList[$thisSubjectID][0][$thisStudentID] = true;
    						if(!empty($thisParentSubjectID)) {
    							$SubjectExemptStudentList[$thisParentSubjectID][0][$thisStudentID] = true;
    						}
					    }
					}
				}
				// Year Report
				else
				{
				    $firstReportColumnResultAllAbsent = (array)$ReportColumnGradeResult2[$thisReportColumnID][$thisSubjectID][$thisStudentID];
				    $lastReportColumnResultAllAbsent = (array)$ReportColumnGradeResult3[$thisReportColumnID][$thisSubjectID][$thisStudentID];
				    $lastReportColumnResultExempt = (array)$ReportColumnGradeResult[$thisReportColumnID][$thisSubjectID][$thisStudentID];
				    
				    // Check if student with all absent in Related Term Report Column
				    if(!empty($firstReportColumnResultAllAbsent) && !empty($lastReportColumnResultAllAbsent))
				    {
				        $SubjectExemptStudentList['Overall'][$thisReportColumnID][$thisStudentID] = true;
				        $SubjectExemptStudentList['Overall'][0][$thisStudentID] = true;
				        $SubjectExemptStudentList[$thisSubjectID][$thisReportColumnID][$thisStudentID] = true;
				        $SubjectExemptStudentList[$thisSubjectID][0][$thisStudentID] = true;
				        if(!empty($thisParentSubjectID)) {
				            $SubjectExemptStudentList[$thisParentSubjectID][$thisReportColumnID][$thisStudentID] = true;
				            $SubjectExemptStudentList[$thisParentSubjectID][0][$thisStudentID] = true;
				        }
				    }
				    // Check if student with absent or exempt in Related Term Report Last Column
					else if(!empty($lastReportColumnResultExempt))
					{
						$SubjectExemptStudentList['Overall'][$thisReportColumnID][$thisStudentID] = true;
						$SubjectExemptStudentList['Overall'][0][$thisStudentID] = true;
						$SubjectExemptStudentList[$thisSubjectID][$thisReportColumnID][$thisStudentID] = true;
						$SubjectExemptStudentList[$thisSubjectID][0][$thisStudentID] = true;
						if(!empty($thisParentSubjectID)) {
							$SubjectExemptStudentList[$thisParentSubjectID][$thisReportColumnID][$thisStudentID] = true;
							$SubjectExemptStudentList[$thisParentSubjectID][0][$thisStudentID] = true;
						}
					}
				}
			}
		}
		
		### Get Form Ranking Info
		foreach ((array)$SubjectFormMarkArr as $thisSubjectID => $thisSubjectFormMarkArr)
		{
			$thisScaleInput = $SubjectGradingArr[$thisSubjectID]['scaleInput'];
			foreach ((array)$thisSubjectFormMarkArr as $thisReportColumnID => $thisColumnSubjectFormMarkArr)
			{
				// Get exclude ordering student list
				if ($ReportType == 'T') {
					$ExcludeStudentsArr = $ExcludeStudentsAssoArr[0];
				}
				else {
					$ExcludeStudentsArr = $ExcludeStudentsAssoArr[$thisReportColumnID];
				}
				if($eRCTemplateSetting['ExcludeStudentRankingForGrandResultOnly']){
				    $ExcludeStudentsArr = array();
				}
				$thisNumOfStudent = $this->Get_Number_Of_Counted_Mark_Student($thisColumnSubjectFormMarkArr, $ExcludeStudentsArr, $storedSubjectBySD=="BySD");
				
				// Special Handling for Number of Student for some special cases
				if(isset($eRCTemplateSetting['NumberOfStudentCalculationIncludedSpecialCase']) && isset($eRCTemplateSetting['NumberOfStudentCalculationExcludedSpecialCase']))
				{
				    if(!isset($CountedFormStudentIDList[$thisReportColumnID])) {
				        $CountedFormStudentIDList[$thisReportColumnID] = array();
				    }
				    if(!isset($NotCountedFormStudentIDList[$thisReportColumnID])) {
				        $NotCountedFormStudentIDList[$thisReportColumnID] = array();
				    }
				    
				    list($thisCountedStudentIDArr, $thisNotCountStudentIDArr) = $this->Get_List_Of_Counted_Mark_Student($thisColumnSubjectFormMarkArr, $ExcludeStudentsArr, $storedSubjectBySD=="BySD");
				    $CountedFormStudentIDList[$thisReportColumnID] = array_merge($CountedFormStudentIDList[$thisReportColumnID], $thisCountedStudentIDArr);
				    $CountedFormStudentIDList[$thisReportColumnID] = array_values(array_unique(array_filter($CountedFormStudentIDList[$thisReportColumnID])));
				    $NotCountedFormStudentIDList[$thisReportColumnID] = array_merge($NotCountedFormStudentIDList[$thisReportColumnID], $thisNotCountStudentIDArr);
				    $NotCountedFormStudentIDList[$thisReportColumnID] = array_values(array_unique(array_filter($NotCountedFormStudentIDList[$thisReportColumnID])));
				}
				
				// Add students with absent or exempt to exclude ordering student list
				if($eRCTemplateSetting['ExcludeFromOrder_ExamAbsOrExempt'] && !empty($SubjectExemptStudentList[$thisSubjectID][$thisReportColumnID])) {
					$thisExemptStudentList = (array)$SubjectExemptStudentList[$thisSubjectID][$thisReportColumnID];
					$thisExemptStudentList = array_keys($thisExemptStudentList);
					
					// Term Overall Column / Whole Year Column
					if ($ReportType=="T" && $thisReportColumnID==0 || $ReportType=="W") {
						$ExcludeStudentsArr = array_merge((array)$ExcludeStudentsArr, $thisExemptStudentList);
					}
				}
				$thisRankInfoArr = $this->Get_Student_Ranking_Array($thisColumnSubjectFormMarkArr, $ExcludeStudentsArr, $thisScaleInput, $storedSubjectBySD=="BySD");
				
				foreach((array)$thisRankInfoArr as $thisReportResultScoreID => $thisRanking)
				{
					$SubjectScoreAssoArr[$thisReportResultScoreID]['OrderMeritForm'.$storedSubjectBySD] = $thisRanking;
					$SubjectScoreAssoArr[$thisReportResultScoreID]['FormNoOfStudent'] = $thisNumOfStudent;
				}
			}
		}
		
		### Get Class Ranking Info
		foreach ((array)$SubjectClassMarkArr as $thisYearClassID => $thisClassMarkArr)
		{
			foreach ((array)$thisClassMarkArr as $thisSubjectID => $thisSubjectClassMarkArr)
			{
				$thisScaleInput = $SubjectGradingArr[$thisSubjectID]['scaleInput'];
				foreach ((array)$thisSubjectClassMarkArr as $thisReportColumnID => $thisColumnSubjectClassMarkArr)
				{
					if ($ReportType == 'T') {
						$ExcludeStudentsArr = $ExcludeStudentsAssoArr[0];
					}
					else {
						$ExcludeStudentsArr = $ExcludeStudentsAssoArr[$thisReportColumnID];
					}
					if($eRCTemplateSetting['ExcludeStudentRankingForGrandResultOnly']){
					    $ExcludeStudentsArr = array();
					}
					$thisNumOfStudent = $this->Get_Number_Of_Counted_Mark_Student($thisColumnSubjectClassMarkArr, $ExcludeStudentsArr, $storedSubjectBySD=="BySD");
					
					// Special Handling for Number of Student for some special cases
					if(isset($eRCTemplateSetting['NumberOfStudentCalculationIncludedSpecialCase']) && isset($eRCTemplateSetting['NumberOfStudentCalculationExcludedSpecialCase']))
					{
					    if(!isset($CountedClassStudentIDList[$thisReportColumnID][$thisYearClassID])) {
					        $CountedClassStudentIDList[$thisReportColumnID][$thisYearClassID] = array();
					    }
					    if(!isset($NotCountedClassStudentIDList[$thisReportColumnID][$thisYearClassID])) {
					        $NotCountedClassStudentIDList[$thisReportColumnID][$thisYearClassID] = array();
					    }
					    
					    list($thisCountedStudentIDArr, $thisNotCountStudentIDArr) = $this->Get_List_Of_Counted_Mark_Student($thisColumnSubjectClassMarkArr, $ExcludeStudentsArr, $storedSubjectBySD=="BySD");
					    $CountedClassStudentIDList[$thisReportColumnID][$thisYearClassID] = array_merge($CountedClassStudentIDList[$thisReportColumnID][$thisYearClassID], $thisCountedStudentIDArr);
					    $CountedClassStudentIDList[$thisReportColumnID][$thisYearClassID] = array_values(array_unique(array_filter($CountedClassStudentIDList[$thisReportColumnID][$thisYearClassID])));
					    $NotCountedClassStudentIDList[$thisReportColumnID][$thisYearClassID] = array_merge($NotCountedClassStudentIDList[$thisReportColumnID][$thisYearClassID], $thisNotCountStudentIDArr);
					    $NotCountedClassStudentIDList[$thisReportColumnID][$thisYearClassID] = array_values(array_unique(array_filter($NotCountedClassStudentIDList[$thisReportColumnID][$thisYearClassID])));
					}
					
					// Add students with absent or exempt to exclude ordering student list
					if($eRCTemplateSetting['ExcludeFromOrder_ExamAbsOrExempt'] && !empty($SubjectExemptStudentList[$thisSubjectID][$thisReportColumnID])) {
						$thisExemptStudentList = (array)$SubjectExemptStudentList[$thisSubjectID][$thisReportColumnID];
						$thisExemptStudentList = array_keys($thisExemptStudentList);
						
						// Term Overall Column / Whole Year Column
						if ($ReportType=="T" && $thisReportColumnID==0 || $ReportType=="W") {
							$ExcludeStudentsArr = array_merge((array)$ExcludeStudentsArr, $thisExemptStudentList);
						}
					}
					$thisRankInfoArr = $this->Get_Student_Ranking_Array($thisColumnSubjectClassMarkArr, $ExcludeStudentsArr, $thisScaleInput, $storedSubjectBySD=="BySD");
									
					foreach((array)$thisRankInfoArr as $thisReportResultScoreID => $thisRanking)
					{
						$SubjectScoreAssoArr[$thisReportResultScoreID]['OrderMeritClass'.$storedSubjectBySD] = $thisRanking;
						$SubjectScoreAssoArr[$thisReportResultScoreID]['ClassNoOfStudent'] = $thisNumOfStudent;
					} 
				}
			}
		}
		
		### Get Subject Group Ranking Info
		foreach ((array)$SubjectSubjectGroupMarkArr as $thisSubjectGroupID => $thisSubjectGroupMarkArr)
		{
			foreach ((array)$thisSubjectGroupMarkArr as $thisSubjectID => $thisSubjectSubjectGroupMarkArr)
			{
				$thisScaleInput = $SubjectGradingArr[$thisSubjectID]['scaleInput'];
				foreach ((array)$thisSubjectSubjectGroupMarkArr as $thisReportColumnID => $thisColumnSubjectSubjectGroupMarkArr)
				{
					if ($ReportType == 'T') {
						$ExcludeStudentsArr = $ExcludeStudentsAssoArr[0];
					}
					else {
						$ExcludeStudentsArr = $ExcludeStudentsAssoArr[$thisReportColumnID];
					}
					if($eRCTemplateSetting['ExcludeStudentRankingForGrandResultOnly']){
					    $ExcludeStudentsArr = array();
					}
					$thisNumOfStudent = $this->Get_Number_Of_Counted_Mark_Student($thisColumnSubjectSubjectGroupMarkArr, $ExcludeStudentsArr, $storedSubjectBySD=="BySD");
					
					// Add students with absent or exempt to exclude ordering student list
					if($eRCTemplateSetting['ExcludeFromOrder_ExamAbsOrExempt'] && !empty($SubjectExemptStudentList[$thisSubjectID][$thisReportColumnID])) {
						$thisExemptStudentList = (array)$SubjectExemptStudentList[$thisSubjectID][$thisReportColumnID];
						$thisExemptStudentList = array_keys($thisExemptStudentList);
						
						// Term Overall Column / Whole Year Column
						if ($ReportType == "T" && $thisReportColumnID == 0 || $ReportType == "W") {
							$ExcludeStudentsArr = array_merge((array)$ExcludeStudentsArr, $thisExemptStudentList);
						}
					}
					$thisRankInfoArr = $this->Get_Student_Ranking_Array($thisColumnSubjectSubjectGroupMarkArr, $ExcludeStudentsArr, $thisScaleInput, $storedSubjectBySD=="BySD");
					
					foreach((array)$thisRankInfoArr as $thisReportResultScoreID => $thisRanking)
					{
						$SubjectScoreAssoArr[$thisReportResultScoreID]['OrderMeritSubjectGroup'.$storedSubjectBySD] = $thisRanking;
						$SubjectScoreAssoArr[$thisReportResultScoreID]['SubjectGroupNoOfStudent'] = $thisNumOfStudent;
					}
				}
			}
		}
		
		### Get Subject Stream Ranking Info
		foreach ((array)$SubjectStreamMarkArr as $thisClassGroupID => $thisStreamMarkArr)
		{
			foreach ((array)$thisStreamMarkArr as $thisSubjectID => $thisSubjectStreamMarkArr)
			{
				$thisScaleInput = $SubjectGradingArr[$thisSubjectID]['scaleInput'];
				foreach ((array)$thisSubjectStreamMarkArr as $thisReportColumnID => $thisColumnSubjectStreamMarkArr)
				{
					if ($ReportType == 'T') {
						$ExcludeStudentsArr = $ExcludeStudentsAssoArr[0];
					}
					else {
						$ExcludeStudentsArr = $ExcludeStudentsAssoArr[$thisReportColumnID];
					}
					if($eRCTemplateSetting['ExcludeStudentRankingForGrandResultOnly']){
					    $ExcludeStudentsArr = array();
					}
					$thisNumOfStudent = $this->Get_Number_Of_Counted_Mark_Student($thisColumnSubjectStreamMarkArr, $ExcludeStudentsArr, $storedSubjectBySD=="BySD");
					
					// Add students with absent or exempt to exclude ordering student list
					if($eRCTemplateSetting['ExcludeFromOrder_ExamAbsOrExempt'] && !empty($SubjectExemptStudentList[$thisSubjectID][$thisReportColumnID])) {
						$thisExemptStudentList = (array)$SubjectExemptStudentList[$thisSubjectID][$thisReportColumnID];
						$thisExemptStudentList = array_keys($thisExemptStudentList);
						
						// Term Overall Column / Whole Year Column
						if ($ReportType=="T" && $thisReportColumnID==0 || $ReportType=="W") {
							$ExcludeStudentsArr = array_merge((array)$ExcludeStudentsArr, $thisExemptStudentList);
						}
					}
					$thisRankInfoArr = $this->Get_Student_Ranking_Array($thisColumnSubjectStreamMarkArr, $ExcludeStudentsArr, $thisScaleInput, $storedSubjectBySD=="BySD");
									
					foreach((array)$thisRankInfoArr as $thisReportResultScoreID => $thisRanking)
					{
						$SubjectScoreAssoArr[$thisReportResultScoreID]['OrderMeritStream'.$storedSubjectBySD] = $thisRanking;
						$SubjectScoreAssoArr[$thisReportResultScoreID]['StreamNoOfStudent'] = $thisNumOfStudent;
					} 
				}
			}
		}
		
		// [2015-0421-1144-05164] Update Ranking Info in DB
		if($notReplaceExistingOrder){
			foreach((array)$SubjectScoreAssoArr as $thisReportResultScoreID => $thisScoreInfoArr)
			{
				$thisUpdateValueArr = array();
				foreach((array)$thisScoreInfoArr as $thisDBField => $thisValue)
				{
					$stateValue = "";
					if (is_numeric($thisDBField))
						continue;
					else if ($thisDBField == 'DateInput' || $thisDBField == 'DateModified')
						$stateValue = 'now()';
					else if ($thisValue == '')
						$stateValue = 'Null';
					else
						$stateValue = "'".$thisValue."'";
						
					$thisUpdateValueArr[] = $thisDBField." = ".$stateValue;
				}
				$updateFieldStatement = implode(',', $thisUpdateValueArr);
				
				$sql = "Update 
							$subject_score_table
						Set
							$updateFieldStatement
						Where
							ReportResultScoreID = '$thisReportResultScoreID'
						";
				$SuccessArr['SubjectMark']['UpdateNewScore'][] = $this->db_db_query($sql) or die($sql.'<br />'.mysql_error());
			}
		}
		### Delete old data and insert Subject Score with Ranking Info into the Database
		else{
			$sql = "Delete From $subject_score_table Where ReportID = '".$ReportID."'";
			$SuccessArr['SubjectMark']['DeleteOldScore'] = $this->db_db_query($sql);
			
			$insertValuesArr = array();
			$currentIndex = 0;
			$tmpInsertCounter = 0;
			$maxInsertNum = 1000;
			foreach((array)$SubjectScoreAssoArr as $thisReportResultScoreID => $thisScoreInfoArr)
			{
				$thisInsertValuesArr = array();
				foreach((array)$thisScoreInfoArr as $thisDBField => $thisValue)
				{
					if (is_numeric($thisDBField))
						continue;
					else if ($thisDBField == 'DateInput' || $thisDBField == 'DateModified')
						$thisInsertValuesArr[] = 'now()';
					else if ($thisValue == '')
						$thisInsertValuesArr[] = 'Null';
					else
						$thisInsertValuesArr[] = "'".$thisValue."'";
				}
				
				if (count($insertValuesArr[$currentIndex]) > $maxInsertNum)
				{
					$currentIndex++;
					$tmpInsertCounter = 0;
				}
				
				(array)$insertValuesArr[$currentIndex][] = "(".implode(",", $thisInsertValuesArr).")";
				$tmpInsertCounter++;
			}
			 
			$numOfInsertArr = count($insertValuesArr);
			if ($numOfInsertArr > 0)
			{
				for ($i=0; $i<$numOfInsertArr; $i++)
				{
					$thisInsertValueArr = $insertValuesArr[$i];
					$insertValuesStatement = implode(',', $thisInsertValueArr);
					$sql = "Insert Into 
								$subject_score_table
							Values
								$insertValuesStatement
							";
					$SuccessArr['SubjectMark']['InsertNewScore'][$i] = $this->db_db_query($sql) or die($sql.'<br />'.mysql_error());
				}
			}
		}
		
		### Get all Grand Marks (from highest to lowest mark for ranking)
		$RankingField = ($eRCTemplateSetting['OrderingPositionBy']=="")? "GrandAverage" : $eRCTemplateSetting['OrderingPositionBy'];
		
		// [2017-1114-1352-50206] Get Form-based Ordering Method
		if(isset($eRCTemplateSetting['OrderingPositionByArr']))
		{
			$formNumber = $this->GET_FORM_NUMBER($ClassLevelID, true);
			if(isset($eRCTemplateSetting['OrderingPositionByArr'][$formNumber]) && $eRCTemplateSetting['OrderingPositionByArr'][$formNumber] != "")
			{
				$RankingField = $eRCTemplateSetting['OrderingPositionByArr'][$formNumber];
			}
		}
		
		// [2015-0421-1144-05164] Update $RankingField if $grandRankingField is set
		if(trim($grandRankingField)!="")
			$RankingField = $grandRankingField;
			
		// [2015-0421-1144-05164] Update SD Order if $grandRankingField = 'GrandSDScore'
		$storedGrandBySD = "";
		if(trim($grandRankingField)!="" && $grandRankingField=="GrandSDScore") {
			$storedGrandBySD = "BySD";
		}
			
		$grand_score_table = $this->DBName.".RC_REPORT_RESULT";
		$sql = "Select
						*
				From
						$grand_score_table
				Where
						 ReportID = '".$ReportID."'
				Order By
						$RankingField Desc 
				";
		$GrandScoreArr = $this->returnResultSet($sql);
		
		// [2019-0123-1731-14066] Get Student Subject Overall Nature
		if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst'] && $isMainReport)
		{
		    // Primary Level > Subject Component Overall Nature also needed 
		    $ParentSubjectOnly = 1;
		    if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst_UsingSHCC_SubjectUnit'])
		    {
		        //$SchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		        //$isPrimaryLevel = $SchoolType == "P";
		        $ClassLevelName = $this->returnClassLevel($ClassLevelID);
		        $isPrimaryLevel = strpos($ClassLevelName, 'Cl') !== false;
		        $ParentSubjectOnly = $isPrimaryLevel? 0 : 1;
		    }
		    
		    // Get Overall Nature
		    $StudentSubjectScoreArr = $this->getMarks($ReportID, $StudentID="", $cons="", $ParentSubjectOnly, $includeAdjustedMarks=1, $OrderBy="", $SubjectID="", $ReportColumnID=0);
		    $StudentSubjectScoreNatureArr = $this->Get_Student_Subject_Mark_Nature_Arr($ReportID, $ClassLevelID, (array)$StudentSubjectScoreArr);
		    
		    // Get Overall Nature with Subject Weight (Unit)
		    if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst_UsingSHCC_SubjectUnit']) {
		        $StudentSubjectScoreNatureExtraDataArr = $this->Get_Student_Subject_Mark_Nature_Arr($ReportID, $ClassLevelID, (array)$StudentSubjectScoreArr, $WithExtraData=true);
			}
		}
		
		$GrandScoreAssoArr = array();
		$GrandFormMarkArr = array();
		$GrandClassMarkArr = array();
		$GrandStreamMarkArr = array();
		$allPassGrandScoreArr = array();
		$withFailedGrandScoreArr = array();
		$withFailedScoreGrandScoreArr = array();
		
		foreach ((array)$GrandScoreArr as $key => $thisGrandScoreArr)
		{
			$thisResultID 			= $thisGrandScoreArr['ResultID'];
			$thisReportColumnID 	= $thisGrandScoreArr['ReportColumnID'];
			$thisStudentID 			= $thisGrandScoreArr['StudentID'];
			
			# Add records to the associate array so that all marks can be inserted again after the ranking process
			$GrandScoreAssoArr[$thisResultID] = $thisGrandScoreArr;
			
			# Get the ranking determine mark
			$thisRankingMark = $thisGrandScoreArr[$RankingField];
			$thisGrade = $thisGrandScoreArr['Grade'];
			
			# Get Student Class Info
			$thisYearClassID = $StudentInfoArr[$thisStudentID]['YearClassID'];
			$thisClassGroupID = $YearClassInfoArr[$thisYearClassID]['ClassGroupID'];
			
			if($eRCTemplateSetting['Settings']['StudentSettings_SubjectStream']){
				$thisClassGroupID = $studentssInfoAry[$thisStudentID]['SubjectStream'];
				$thisClassGroupID = ord($thisClassGroupID);
			}
			
			# Build Form Ranking Array
			(array)$GrandFormMarkArr[$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
			(array)$GrandFormMarkArr[$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
			(array)$GrandFormMarkArr[$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
			(array)$GrandFormMarkArr[$thisReportColumnID]['GradeArr'][] = $thisGrade;
			
			# Build Class Ranking Array
			(array)$GrandClassMarkArr[$thisYearClassID][$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
			(array)$GrandClassMarkArr[$thisYearClassID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
			(array)$GrandClassMarkArr[$thisYearClassID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
			(array)$GrandClassMarkArr[$thisYearClassID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
			
			# Build Stream Ranking Array
			if ($thisClassGroupID != '' && $thisClassGroupID != 0)
			{
				(array)$GrandStreamMarkArr[$thisClassGroupID][$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
				(array)$GrandStreamMarkArr[$thisClassGroupID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
				(array)$GrandStreamMarkArr[$thisClassGroupID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
				(array)$GrandStreamMarkArr[$thisClassGroupID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
			}
			
			// [2019-0123-1731-14066] Build Student Ranking Array for passed all subjects and failed subjects
			if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst'] && $isMainReport)
			{
				// Check all subjects passed or failed subjects
				$thisStudentSubjectNature = $StudentSubjectScoreNatureArr[$thisStudentID];
				$thisStudentSubjectNature = array_values_recursive((array)$thisStudentSubjectNature);
				$thisStudentPassedSubject = in_array("Pass", (array)$thisStudentSubjectNature) || in_array("Distinction", (array)$thisStudentSubjectNature);
				$thisStudentPassedSubject = $thisStudentPassedSubject && !in_array("Fail", (array)$thisStudentSubjectNature);
				$thisStudentFailedSubject = !$thisStudentPassedSubject;
				
				// Student Ranking Array: All Subjects passed
				if($thisStudentPassedSubject)
				{
					# Build Form Ranking Array
					(array)$allPassGrandScoreArr['Form'][$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
					(array)$allPassGrandScoreArr['Form'][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
					(array)$allPassGrandScoreArr['Form'][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
					(array)$allPassGrandScoreArr['Form'][$thisReportColumnID]['GradeArr'][] = $thisGrade;
					
					# Build Class Ranking Array
					(array)$allPassGrandScoreArr['Class'][$thisYearClassID][$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
					(array)$allPassGrandScoreArr['Class'][$thisYearClassID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
					(array)$allPassGrandScoreArr['Class'][$thisYearClassID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
					(array)$allPassGrandScoreArr['Class'][$thisYearClassID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
					
					# Build Stream Ranking Array
					if ($thisClassGroupID != '' && $thisClassGroupID != 0)
					{
						(array)$allPassGrandScoreArr['Stream'][$thisClassGroupID][$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
						(array)$allPassGrandScoreArr['Stream'][$thisClassGroupID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
						(array)$allPassGrandScoreArr['Stream'][$thisClassGroupID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
						(array)$allPassGrandScoreArr['Stream'][$thisClassGroupID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
					}
				}
				
				// Student Ranking Array: Failed subjects
				if($thisStudentFailedSubject)
				{
				    // Array Grouped by Failed Subject Weight (Unit)
				    if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst_UsingSHCC_SubjectUnit'])
    				{
    				    // Calculate Failed Subject Weight (Unit)
    				    $failedSubjectUnit = 0;
    				    $thisStudentSubjectNatureExtraData = $StudentSubjectScoreNatureExtraDataArr[$thisStudentID];
    				    foreach((array)$thisStudentSubjectNatureExtraData as $thisExtraDataSubjectID => $thisSubjectNatureExtraData)
    				    {
    				        // Primary Level > Skip Parent Subject in calculation
    				        if($isPrimaryLevel) {
    				            $thisExtraDataSubjectCmpArr = $this->GET_COMPONENT_SUBJECT($thisExtraDataSubjectID, $ClassLevelID, '', 'Desc', $ReportID);
    				            if(count($thisExtraDataSubjectCmpArr) > 0) {
    				                continue;
    				            }
    				        }
    				        
    				        $thisSubjectNatureExtraData = $thisSubjectNatureExtraData[0];
    				        if(!empty($thisSubjectNatureExtraData) && $thisSubjectNatureExtraData['Nature'] == 'Fail') {
    				            $failedSubjectUnit += $thisSubjectNatureExtraData['Weight']? $thisSubjectNatureExtraData['Weight'] : 0;
    				        }
    				    }
    				    
				        # Build Form Ranking Array
				        (array)$withFailedScoreGrandScoreArr[$failedSubjectUnit]['Form'][$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
				        (array)$withFailedScoreGrandScoreArr[$failedSubjectUnit]['Form'][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
				        (array)$withFailedScoreGrandScoreArr[$failedSubjectUnit]['Form'][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
				        (array)$withFailedScoreGrandScoreArr[$failedSubjectUnit]['Form'][$thisReportColumnID]['GradeArr'][] = $thisGrade;
				        
				        # Build Class Ranking Array
				        (array)$withFailedScoreGrandScoreArr[$failedSubjectUnit]['Class'][$thisYearClassID][$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
				        (array)$withFailedScoreGrandScoreArr[$failedSubjectUnit]['Class'][$thisYearClassID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
				        (array)$withFailedScoreGrandScoreArr[$failedSubjectUnit]['Class'][$thisYearClassID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
				        (array)$withFailedScoreGrandScoreArr[$failedSubjectUnit]['Class'][$thisYearClassID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
				        
				        # Build Stream Ranking Array
				        if ($thisClassGroupID != '' && $thisClassGroupID != 0)
				        {
				            (array)$withFailedScoreGrandScoreArr[$failedSubjectUnit]['Stream'][$thisClassGroupID][$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
				            (array)$withFailedScoreGrandScoreArr[$failedSubjectUnit]['Stream'][$thisClassGroupID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
				            (array)$withFailedScoreGrandScoreArr[$failedSubjectUnit]['Stream'][$thisClassGroupID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
				            (array)$withFailedScoreGrandScoreArr[$failedSubjectUnit]['Stream'][$thisClassGroupID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
				        }
    				}
    				// Normal Array
    				else
    				{
    					# Build Form Ranking Array
    					(array)$withFailedGrandScoreArr['Form'][$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
    					(array)$withFailedGrandScoreArr['Form'][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
    					(array)$withFailedGrandScoreArr['Form'][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
    					(array)$withFailedGrandScoreArr['Form'][$thisReportColumnID]['GradeArr'][] = $thisGrade;
    					
    					# Build Class Ranking Array
    					(array)$withFailedGrandScoreArr['Class'][$thisYearClassID][$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
    					(array)$withFailedGrandScoreArr['Class'][$thisYearClassID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
    					(array)$withFailedGrandScoreArr['Class'][$thisYearClassID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
    					(array)$withFailedGrandScoreArr['Class'][$thisYearClassID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
    					
    					# Build Stream Ranking Array
    					if ($thisClassGroupID != '' && $thisClassGroupID != 0)
    					{
    						(array)$withFailedGrandScoreArr['Stream'][$thisClassGroupID][$thisReportColumnID]['KeyIDArr'][] = $thisResultID;
    						(array)$withFailedGrandScoreArr['Stream'][$thisClassGroupID][$thisReportColumnID]['StudentIDArr'][] = $thisStudentID;
    						(array)$withFailedGrandScoreArr['Stream'][$thisClassGroupID][$thisReportColumnID]['MarkArr'][] = $thisRankingMark;
    						(array)$withFailedGrandScoreArr['Stream'][$thisClassGroupID][$thisReportColumnID]['GradeArr'][] = $thisGrade;
    					}
    				}
				}
			}
		}
		
		// [2019-0123-1731-14066] Sort Student Ranking Array by Failed Subject Weight (Unit)
		if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst'] && $eRCTemplateSetting['GrandOrder_AllPassedStudentFirst_UsingSHCC_SubjectUnit'] && $isMainReport) {
		    if(isset($withFailedScoreGrandScoreArr) && !empty($withFailedScoreGrandScoreArr))  {
		        ksort($withFailedScoreGrandScoreArr);
		    }
		}
		
		### Get Form Ranking Info
		foreach ((array)$GrandFormMarkArr as $thisReportColumnID => $thisGrandFormMarkArr)
		{
			if ($ReportType == 'T') {
				$ExcludeStudentsArr = $ExcludeStudentsAssoArr[0];
			}
			else {
				$ExcludeStudentsArr = $ExcludeStudentsAssoArr[$thisReportColumnID];
			}
			$thisNumOfStudent = $this->Get_Number_Of_Counted_Mark_Student($thisGrandFormMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD");
			
			// Special Handling for Number of Student for some special cases
			if(isset($eRCTemplateSetting['NumberOfStudentCalculationIncludedSpecialCase']) && isset($eRCTemplateSetting['NumberOfStudentCalculationExcludedSpecialCase']) && 
			         isset($CountedFormStudentIDList[$thisReportColumnID]) && isset($NotCountedFormStudentIDList[$thisReportColumnID])) {
			    $FormStudentIDList = array_diff((array)$CountedFormStudentIDList[$thisReportColumnID], (array)$NotCountedFormStudentIDList[$thisReportColumnID]);
			    $thisNumOfStudent = count((array)$FormStudentIDList);
			}
			
			// Add students with absent or exempt to exclude ordering student list
			if($eRCTemplateSetting['ExcludeFromOrder_ExamAbsOrExempt'] && !empty($SubjectExemptStudentList['Overall'][$thisReportColumnID])) {
				$thisExemptStudentList = (array)$SubjectExemptStudentList['Overall'][$thisReportColumnID];
				$thisExemptStudentList = array_keys($thisExemptStudentList);
				$ExcludeStudentsArr = array_merge((array)$ExcludeStudentsArr, $thisExemptStudentList);
			}
			
			// [2019-0123-1731-14066] for passed all subjects only
			$allPassedFormStudent = 0;
			if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst'] && $isMainReport) {
				$allPassedSubjectMarkArr = $allPassGrandScoreArr['Form'][$thisReportColumnID];
				if(count((array)$allPassedSubjectMarkArr) > 0) {
					$thisRankInfoArr = $this->Get_Student_Ranking_Array($allPassedSubjectMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD");
				} else {
					$thisRankInfoArr = array();
				}
			}
			else {
				$thisRankInfoArr = $this->Get_Student_Ranking_Array($thisGrandFormMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD");
			}
			
			foreach((array)$thisRankInfoArr as $thisResultID => $thisRanking)
			{
				$GrandScoreAssoArr[$thisResultID]['OrderMeritForm'.$storedGrandBySD] = $thisRanking;
				$GrandScoreAssoArr[$thisResultID]['FormNoOfStudent'] = $thisNumOfStudent;
				if($thisRanking > 0) {
					$allPassedFormStudent++;
				}
			}
			
			// [2019-0123-1731-14066] for Failed subjects
			if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst'] && $isMainReport)
			{
			    if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst_UsingSHCC_SubjectUnit'])
			    {
			        $allHandledFormStudent = $allPassedFormStudent;
			        if(isset($withFailedScoreGrandScoreArr) && !empty($withFailedScoreGrandScoreArr)) {
			            foreach((array)$withFailedScoreGrandScoreArr as $thisFailedScore => $thisFailedScoreGrandScoreArr)
			            {
			                $withFailedSubjectMarkArr = $thisFailedScoreGrandScoreArr['Form'][$thisReportColumnID];
			                if(count((array)$withFailedSubjectMarkArr) > 0)
			                {
			                    $thisRankInfoArr = $this->Get_Student_Ranking_Array($withFailedSubjectMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD", $SDScoreHandling=false, $allHandledFormStudent);
			                    foreach((array)$thisRankInfoArr as $thisResultID => $thisRanking)
			                    {
			                        $GrandScoreAssoArr[$thisResultID]['OrderMeritForm'.$storedGrandBySD] = $thisRanking;
			                        $GrandScoreAssoArr[$thisResultID]['FormNoOfStudent'] = $thisNumOfStudent;
			                        if($thisRanking > 0) {
			                            $allHandledFormStudent++;
			                        }
			                    }
			                }
			            }
			        }
			    }
			    else
			    {
    				$withFailedSubjectMarkArr = $withFailedGrandScoreArr['Form'][$thisReportColumnID];
    				if(count((array)$withFailedSubjectMarkArr) > 0) {
    					$thisRankInfoArr = $this->Get_Student_Ranking_Array($withFailedSubjectMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD", $SDScoreHandling=false, $allPassedFormStudent);
    					foreach((array)$thisRankInfoArr as $thisResultID => $thisRanking)
    					{
    						$GrandScoreAssoArr[$thisResultID]['OrderMeritForm'.$storedGrandBySD] = $thisRanking;
    						$GrandScoreAssoArr[$thisResultID]['FormNoOfStudent'] = $thisNumOfStudent;
    					}
    				}
			    }
			}
		}
		
		### Get Class Ranking Info
		foreach ((array)$GrandClassMarkArr as $thisYearClassID => $thisClassMarkArr)
		{
			foreach ((array)$thisClassMarkArr as $thisReportColumnID => $thisColumnClassMarkArr)
			{
				if ($ReportType == 'T') {
					$ExcludeStudentsArr = $ExcludeStudentsAssoArr[0];
				}
				else {
					$ExcludeStudentsArr = $ExcludeStudentsAssoArr[$thisReportColumnID];
				}
				$thisNumOfStudent = $this->Get_Number_Of_Counted_Mark_Student($thisColumnClassMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD");
				
				// Special Handling for Number of Student for some special cases
				if(isset($eRCTemplateSetting['NumberOfStudentCalculationIncludedSpecialCase']) && isset($eRCTemplateSetting['NumberOfStudentCalculationExcludedSpecialCase']) &&
				        isset($CountedClassStudentIDList[$thisReportColumnID][$thisYearClassID]) && isset($NotCountedClassStudentIDList[$thisReportColumnID][$thisYearClassID])) {
				    $ClassStudentIDList = array_diff((array)$CountedClassStudentIDList[$thisReportColumnID][$thisYearClassID], (array)$NotCountedClassStudentIDList[$thisReportColumnID][$thisYearClassID]);
				    $thisNumOfStudent = count((array)$ClassStudentIDList);
				}
				
				// Add students with absent or exempt to exclude ordering student list
				if($eRCTemplateSetting['ExcludeFromOrder_ExamAbsOrExempt'] && !empty($SubjectExemptStudentList['Overall'][$thisReportColumnID])) {
					$thisExemptStudentList = (array)$SubjectExemptStudentList['Overall'][$thisReportColumnID];
					$thisExemptStudentList = array_keys($thisExemptStudentList);
					$ExcludeStudentsArr = array_merge((array)$ExcludeStudentsArr, $thisExemptStudentList);
				}
				
				// [2019-0123-1731-14066] for passed all subjects only
				$allPassedClassStudent = 0;
				if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst'] && $isMainReport) {
					$allPassedSubjectMarkArr = $allPassGrandScoreArr['Class'][$thisYearClassID][$thisReportColumnID];
					if(count((array)$allPassedSubjectMarkArr) > 0) {
						$thisRankInfoArr = $this->Get_Student_Ranking_Array($allPassedSubjectMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD");
					} else {
						$thisRankInfoArr = array();
					}
				}
				else {
					$thisRankInfoArr = $this->Get_Student_Ranking_Array($thisColumnClassMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD");
				}
				
				foreach((array)$thisRankInfoArr as $thisResultID => $thisRanking)
				{
					$GrandScoreAssoArr[$thisResultID]['OrderMeritClass'.$storedGrandBySD] = $thisRanking;
					$GrandScoreAssoArr[$thisResultID]['ClassNoOfStudent'] = $thisNumOfStudent;
					if($thisRanking > 0) {
						$allPassedClassStudent++;
					}
				} 
				
				// [2019-0123-1731-14066] for Failed subjects
				if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst'] && $isMainReport)
				{
				    if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst_UsingSHCC_SubjectUnit'])
				    {
				        $allHandledFormStudent = $allPassedClassStudent;
				        if(isset($withFailedScoreGrandScoreArr) && !empty($withFailedScoreGrandScoreArr)) {
				            foreach((array)$withFailedScoreGrandScoreArr as $thisFailedScore => $thisFailedScoreGrandScoreArr)
				            {
				                $withFailedSubjectMarkArr = $thisFailedScoreGrandScoreArr['Class'][$thisYearClassID][$thisReportColumnID];
				                if(count((array)$withFailedSubjectMarkArr) > 0)
				                {
				                    $thisRankInfoArr = $this->Get_Student_Ranking_Array($withFailedSubjectMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD", $SDScoreHandling=false, $allHandledFormStudent);
				                    foreach((array)$thisRankInfoArr as $thisResultID => $thisRanking)
				                    {
				                        $GrandScoreAssoArr[$thisResultID]['OrderMeritClass'.$storedGrandBySD] = $thisRanking;
				                        $GrandScoreAssoArr[$thisResultID]['ClassNoOfStudent'] = $thisNumOfStudent;
				                        if($thisRanking > 0) {
				                            $allHandledFormStudent++;
				                        }
				                    }
				                }
				            }
				        }
				    }
				    else
				    {
    					$withFailedSubjectMarkArr = $withFailedGrandScoreArr['Class'][$thisYearClassID][$thisReportColumnID];
    					if(count((array)$withFailedSubjectMarkArr) > 0) {
    						$thisRankInfoArr = $this->Get_Student_Ranking_Array($withFailedSubjectMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD", $SDScoreHandling=false, $allPassedClassStudent);
    						foreach((array)$thisRankInfoArr as $thisResultID => $thisRanking)
    						{
    							$GrandScoreAssoArr[$thisResultID]['OrderMeritClass'.$storedGrandBySD] = $thisRanking;
    							$GrandScoreAssoArr[$thisResultID]['ClassNoOfStudent'] = $thisNumOfStudent;
    						} 
    					}
				    }
				}
			}
		}
		
		### Get Subject Stream Ranking Info
		foreach ((array)$GrandStreamMarkArr as $thisClassGroupID => $thisStreamMarkArr)
		{
			foreach ((array)$thisStreamMarkArr as $thisReportColumnID => $thisColumnStreamMarkArr)
			{
				if ($ReportType == 'T') {
					$ExcludeStudentsArr = $ExcludeStudentsAssoArr[0];
				}
				else {
					$ExcludeStudentsArr = $ExcludeStudentsAssoArr[$thisReportColumnID];
				}
				$thisNumOfStudent = $this->Get_Number_Of_Counted_Mark_Student($thisColumnStreamMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD");
				
				// Add students with absent or exempt to exclude ordering student list
				if($eRCTemplateSetting['ExcludeFromOrder_ExamAbsOrExempt'] && !empty($SubjectExemptStudentList['Overall'][$thisReportColumnID])) {
					$thisExemptStudentList = (array)$SubjectExemptStudentList['Overall'][$thisReportColumnID];
					$thisExemptStudentList = array_keys($thisExemptStudentList);
					$ExcludeStudentsArr = array_merge((array)$ExcludeStudentsArr, $thisExemptStudentList);
				}
				
				// [2019-0123-1731-14066] for passed all subjects only
				$allPassedStreamStudent = 0;
				if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst'] && $isMainReport) {
					$allPassedSubjectMarkArr = $allPassGrandScoreArr['Stream'][$thisClassGroupID][$thisReportColumnID];
					if(count((array)$allPassedSubjectMarkArr) > 0) {
						$thisRankInfoArr = $this->Get_Student_Ranking_Array($allPassedSubjectMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD");
					} else {
						$thisRankInfoArr = array();
					}
				}
				else {
					$thisRankInfoArr = $this->Get_Student_Ranking_Array($thisColumnStreamMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD");
				}
				
				foreach((array)$thisRankInfoArr as $thisResultID => $thisRanking)
				{
					$GrandScoreAssoArr[$thisResultID]['OrderMeritStream'.$storedGrandBySD] = $thisRanking;
					$GrandScoreAssoArr[$thisResultID]['StreamNoOfStudent'] = $thisNumOfStudent;
					if($thisRanking > 0)
						$allPassedStreamStudent++;
				}
				
				// [2019-0123-1731-14066] for Failed subjects
				if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst'] && $isMainReport)
				{
				    if($eRCTemplateSetting['GrandOrder_AllPassedStudentFirst_UsingSHCC_SubjectUnit'])
				    {
				        $allHandledFormStudent = $allPassedStreamStudent;
				        if(isset($withFailedScoreGrandScoreArr) && !empty($withFailedScoreGrandScoreArr)) {
				            foreach((array)$withFailedScoreGrandScoreArr as $thisFailedScore => $thisFailedScoreGrandScoreArr)
				            {
				                $withFailedSubjectMarkArr = $thisFailedScoreGrandScoreArr['Stream'][$thisClassGroupID][$thisReportColumnID];
				                if(count((array)$withFailedSubjectMarkArr) > 0)
				                {
				                    $thisRankInfoArr = $this->Get_Student_Ranking_Array($withFailedSubjectMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD", $SDScoreHandling=false, $allHandledFormStudent);
				                    foreach((array)$thisRankInfoArr as $thisResultID => $thisRanking)
				                    {
				                        $GrandScoreAssoArr[$thisResultID]['OrderMeritStream'.$storedGrandBySD] = $thisRanking;
				                        $GrandScoreAssoArr[$thisResultID]['StreamNoOfStudent'] = $thisNumOfStudent;
				                        if($thisRanking > 0) {
				                            $allHandledFormStudent++;
				                        }
				                    }
				                }
				            }
				        }
				    }
				    else
				    {
    					$withFailedSubjectMarkArr = $withFailedGrandScoreArr['Stream'][$thisClassGroupID][$thisReportColumnID];
    					if(count((array)$withFailedSubjectMarkArr) > 0) {
    						$thisRankInfoArr = $this->Get_Student_Ranking_Array($withFailedSubjectMarkArr, $ExcludeStudentsArr, $storedGrandBySD=="BySD", $SDScoreHandling=false, $allPassedStreamStudent);
    						foreach((array)$thisRankInfoArr as $thisResultID => $thisRanking)
    						{
    							$GrandScoreAssoArr[$thisResultID]['OrderMeritStream'.$storedGrandBySD] = $thisRanking;
    							$GrandScoreAssoArr[$thisResultID]['StreamNoOfStudent'] = $thisNumOfStudent;
    						}
    					}
				    }
				}
			}
		}
		
		// [2015-0421-1144-05164] Update Ranking Info in DB
		if($notReplaceExistingOrder){
			foreach((array)$GrandScoreAssoArr as $thisResultID => $thisScoreInfoArr)
			{
				$thisUpdateValueArr = array();
				foreach((array)$thisScoreInfoArr as $thisDBField => $thisValue)
				{
					$stateValue = "";
					if (is_numeric($thisDBField))
						continue;
					else if ($thisDBField == 'DateInput' || $thisDBField == 'DateModified')
						$stateValue = 'now()';
					else if ($thisValue == '')
						$stateValue = 'Null';
					else
						$stateValue = "'".$thisValue."'";
						
					$thisUpdateValueArr[] = $thisDBField." = ".$stateValue;
				}
				$updateFieldStatement = implode(',', $thisUpdateValueArr);
				
				$sql = "Update 
							$grand_score_table
						Set
							$updateFieldStatement
						Where
							ResultID = '$thisResultID'
						";
				$SuccessArr['SubjectMark']['UpdateNewScore'][] = $this->db_db_query($sql) or die($sql.'<br />'.mysql_error());
			}
		}
		### Delete old data and insert Subject Score with Ranking Info into the Database
		else{
			$sql = "Delete From $grand_score_table Where ReportID = '".$ReportID."'";
			$SuccessArr['GrandMark']['DeleteOldScore'] = $this->db_db_query($sql);
		
			$insertValuesArr = array();
			foreach((array)$GrandScoreAssoArr as $thisResultID => $thisScoreInfoArr)
			{
				$thisInsertValuesArr = array();
				foreach((array)$thisScoreInfoArr as $thisDBField => $thisValue)
				{
					if (is_numeric($thisDBField))
						continue;
					else if ($thisDBField == 'DateInput' || $thisDBField == 'DateModified')
						$thisInsertValuesArr[] = 'now()';
					else if ($thisValue == '')
						$thisInsertValuesArr[] = 'Null';
					else
						$thisInsertValuesArr[] = "'".$thisValue."'";
				}
				$insertValuesArr[] = "(".implode(",", $thisInsertValuesArr).")";
			}
			 
			if (count($insertValuesArr) > 0)
			{
				$insertValuesStatement = implode(',', $insertValuesArr);
				$sql = "Insert Into 
							$grand_score_table
						Values
							$insertValuesStatement
						";
				$SuccessArr['GrandMark']['InsertNewScore'] = $this->db_db_query($sql);	
			}
		}
		
		if (in_array(false, $SuccessArr))
			return false;
		else
			return true;
	}
	
	function Get_Student_Ranking_Array($MarkInfoArr, $ExcludeStudentsArr=array(), $ScaleInput='', $SDScoreHandling=false, $RankBase="")
	{
		global $eRCTemplateSetting;
		
		if ($ScaleInput == '')
			$ScaleInput = 'M';
		
		$KeyIDArr = $MarkInfoArr['KeyIDArr'];
		$StudentIDArr = $MarkInfoArr['StudentIDArr'];
		$MarkArr = $MarkInfoArr['MarkArr'];
		$GradeArr = $MarkInfoArr['GradeArr'];
		$numOfMark = count($MarkArr);
		
		$thisRank = 0;
		if($RankBase) {
			$thisRank = $RankBase;
		}
		
		$lastMark = -1;
		$sameMarkCount = 0;
		$StudentRankArr = array();
		for ($i=0; $i<$numOfMark; $i++)
		{
			$thisKeyID = $KeyIDArr[$i];
			$thisStudentID = $StudentIDArr[$i];
			$thisMark = $MarkArr[$i];
			$thisGrade = $GradeArr[$i];
			
			// exclude student in the list or the subject is not mark input, they will not be count in the ranking
			// [2016-1116-0911-58236]
			$thisSkipStudent = false;
			// 1. Student : Exclude Students from Ranking
			if (in_array($thisStudentID, (array)$ExcludeStudentsArr)) {
				$thisSkipStudent = true;
			}
			// 2. Input Scale : Grade
			if ($ScaleInput != 'M') {
				$thisSkipStudent = true;
			}
			// 3. Grade : Special Cases Exclude Ranking
			if (in_array($thisGrade, $this->Get_Exclude_Ranking_Special_Case())) {
				$thisSkipStudent = true;
			}
			// 4. Score : Invalid
			if ($eRCTemplateSetting['GenerateSDOrdering']) {
				// Pooi To : Normal Order Method AND Invalid Score (-1)
				// [2015-0421-1144-05164] added checking for SD ordering 
				if (!$SDScoreHandling && $thisMark=='-1') {
					$thisSkipStudent = true ;
				}
			}
			else {
				// Normal Order Method AND Invalid Mark (-1)
				if ($eRCTemplateSetting['OrderPositionMethod']!='WeightedSD' && $thisMark=='-1') {
					$thisSkipStudent = true ;
				}
			}
			//if (in_array($thisStudentID, (array)$ExcludeStudentsArr) || $ScaleInput != 'M' || ($eRCTemplateSetting['OrderPositionMethod']!='WeightedSD' && $thisMark=='-1') || ($eRCTemplateSetting['GenerateSDOrdering'] && !$SDScoreHandling && $thisMark=='-1') || in_array($thisGrade, $this->Get_Exclude_Ranking_Special_Case())) {
			if ($thisSkipStudent) {
				$StudentRankArr[$thisKeyID] = '-1';
				continue;
			}
			
			if ($eRCTemplateSetting['RankingMethod'] == '1223')
			{
				# 1,2,2,3
				if ($thisMark != $lastMark)
					$thisRank++;
			}
			else
			{
				# 1,2,2,4
				if ($thisMark != $lastMark)
				{
					$thisRank++;
					$thisRank += $sameMarkCount;
					$sameMarkCount = 0;
				}
				else
				{
					$sameMarkCount++;
				}
			}
			
			$lastMark = $thisMark;
			//$StudentRankArr[$thisKeyID] = $thisRank + $sameMarkCount;
			$StudentRankArr[$thisKeyID] = $thisRank;
		}
		
		return $StudentRankArr;
	}
	
	function Get_Number_Of_Counted_Mark_Student($MarkInfoArr, $ExcludeStudentsArr=array(), $SDScoreHandling=false)
	{
		global $eRCTemplateSetting;
		
		$MarkArr = $MarkInfoArr['MarkArr'];
		$GradeArr = $MarkInfoArr['GradeArr'];
		$StudentIDArr = $MarkInfoArr['StudentIDArr'];
		
		$numOfStudent = count($StudentIDArr);
		$numOfExclude = count($ExcludeStudentsArr);
		$numOfCountStudent = 0;
		
		for ($i=0; $i<$numOfStudent; $i++)
		{
			$thisStudentID = $StudentIDArr[$i];
			$thisMark = $MarkArr[$i];
			$thisGrade = $GradeArr[$i];
			
			### If the student get a N.A. score, do not count the student
			// [2016-1116-0911-58236]
			$thisSkipStudent = false;
			if ($eRCTemplateSetting['GenerateSDOrdering']) {
				// Pooi To : Normal Order Method AND Invalid Score (-1)
				// [2015-0421-1144-05164] added checking for SD ordering
				if (!$SDScoreHandling && $thisMark=='-1') {
					$thisSkipStudent = true ;
				}
			}
			else {
				// Normal Order Method AND Invalid Mark (-1)
				if ($eRCTemplateSetting['OrderPositionMethod']!='WeightedSD' && $thisMark=='-1') {
				    $thisSkipStudent = true;
				    
				    // NOT SKIP > included in Number of Student
				    if(isset($eRCTemplateSetting['NumberOfStudentCalculationIncludedSpecialCase']) && $thisGrade != '' && in_array($thisGrade, (array)$eRCTemplateSetting['NumberOfStudentCalculationIncludedSpecialCase'])) {
				        $thisSkipStudent = false;
				    }
				}
			}
			//if (($eRCTemplateSetting['OrderPositionMethod']!='WeightedSD' || !$SDScoreHandling) && $thisMark=='-1') {
			if($thisSkipStudent) {
				continue;
			}
			
			//if (($eRCTemplateSetting['OrderPositionMethod']=='WeightedSD' || $SDScoreHandling) && ($thisMark==null || $thisMark==='')) {
			if (($eRCTemplateSetting['OrderPositionMethod']=='WeightedSD' || ($eRCTemplateSetting['GenerateSDOrdering'] && $SDScoreHandling)) && ($thisMark==null || $thisMark==='')) {
				// [2016-0217-0905-03207] not skip if current grade in special case exclude settings, include student in total numbers
				if(isset($eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent']) && is_array($eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent']) && in_array($thisGrade,$eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent'])){
					// do nothing
				}
				else {
					continue;
				}
			}
			
			// 2011-1128-1135-55073 - 沙田循道衛理中學 - The Grade is not consistent in Master Report and Performance Report
			if($eRCTemplateSetting['SpecialCaseThatExcludedFromNoOfStudent'])
			{
				if(in_array($thisGrade,$eRCTemplateSetting['SpecialCaseThatExcludedFromNoOfStudent'])) {
					continue;
				}
			}
			else
			{
			    // NOT SKIP > included in Number of Student
			    if(isset($eRCTemplateSetting['NumberOfStudentCalculationIncludedSpecialCase']) && $thisGrade != '' && in_array($thisGrade, (array)$eRCTemplateSetting['NumberOfStudentCalculationIncludedSpecialCase'])) {
			        // do nothing
			    }
				else if (in_array($thisGrade, $this->Get_Exclude_Ranking_Special_Case())) {
					continue;
				}
			}
				
			### If the student is in the exclude list, do not count the student
			if ($eRCTemplateSetting['DoNotCountExcludedStudentInStudentNumOfClassAndForm'] && $numOfExclude > 0) {
				if (in_array($thisStudentID, (array)$ExcludeStudentsArr)) {
					continue;
				}
			}
			
			$numOfCountStudent++;
		}
		
		return $numOfCountStudent;
	}
	
	function Get_List_Of_Counted_Mark_Student($MarkInfoArr, $ExcludeStudentsArr=array(), $SDScoreHandling=false)
	{
	    global $eRCTemplateSetting;
	    
	    $MarkArr = $MarkInfoArr['MarkArr'];
	    $GradeArr = $MarkInfoArr['GradeArr'];
	    $StudentIDArr = $MarkInfoArr['StudentIDArr'];
	    
	    $CountedStudentIDArr = array();
	    $ExcludeStudentIDArr = array();
	    
	    $numOfStudent = count($StudentIDArr);
	    for ($i=0; $i<$numOfStudent; $i++)
	    {
	        $thisStudentID = $StudentIDArr[$i];
	        $thisMark = $MarkArr[$i];
	        $thisGrade = $GradeArr[$i];
	        
	        ### If the student get a N.A. score, do not count the student
	        // [2016-1116-0911-58236]
	        $thisSkipStudent = false;
	        if ($eRCTemplateSetting['GenerateSDOrdering']) {
	            // Pooi To : Normal Order Method AND Invalid Score (-1)
	            // [2015-0421-1144-05164] added checking for SD ordering
	            if (!$SDScoreHandling && $thisMark=='-1') {
	                $thisSkipStudent = true ;
	            }
	        }
	        else {
	            // Normal Order Method AND Invalid Mark (-1)
	            if ($eRCTemplateSetting['OrderPositionMethod']!='WeightedSD' && $thisMark=='-1') {
	                $thisSkipStudent = true;
	                
	                // NOT SKIP > included in Number of Student
	                if(isset($eRCTemplateSetting['NumberOfStudentCalculationIncludedSpecialCase']) && $thisGrade != '' && in_array($thisGrade, (array)$eRCTemplateSetting['NumberOfStudentCalculationIncludedSpecialCase'])) {
	                    $thisSkipStudent = false;
	                }
	            }
	        }
	        //if (($eRCTemplateSetting['OrderPositionMethod']!='WeightedSD' || !$SDScoreHandling) && $thisMark=='-1') {
	        if($thisSkipStudent) {
	            if(isset($eRCTemplateSetting['NumberOfStudentCalculationExcludedSpecialCase']) && $thisGrade != '' && in_array($thisGrade, (array)$eRCTemplateSetting['NumberOfStudentCalculationExcludedSpecialCase'])) {
	               $ExcludeStudentIDArr[] = $thisStudentID;
	            }
	            continue;
	        }
	        
	        //if (($eRCTemplateSetting['OrderPositionMethod']=='WeightedSD' || $SDScoreHandling) && ($thisMark==null || $thisMark==='')) {
	        if (($eRCTemplateSetting['OrderPositionMethod']=='WeightedSD' || ($eRCTemplateSetting['GenerateSDOrdering'] && $SDScoreHandling)) && ($thisMark==null || $thisMark==='')) {
	            // [2016-0217-0905-03207] not skip if current grade in special case exclude settings, include student in total numbers
	            if(isset($eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent']) && is_array($eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent']) && in_array($thisGrade,$eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent'])){
	                // do nothing
	            }
	            else {
	                continue;
	            }
	        }
	        
	        // 2011-1128-1135-55073 - 沙田循道衛理中學 - The Grade is not consistent in Master Report and Performance Report
	        if($eRCTemplateSetting['SpecialCaseThatExcludedFromNoOfStudent'])
	        {
	            if(in_array($thisGrade,$eRCTemplateSetting['SpecialCaseThatExcludedFromNoOfStudent'])) {
	                continue;
	            }
	        }
	        else
	        {
	            // NOT SKIP > included in Number of Student
	            if(isset($eRCTemplateSetting['NumberOfStudentCalculationIncludedSpecialCase']) && $thisGrade != '' && in_array($thisGrade, (array)$eRCTemplateSetting['NumberOfStudentCalculationIncludedSpecialCase'])) {
	                // do nothing
	            }
	            else if (in_array($thisGrade, $this->Get_Exclude_Ranking_Special_Case())) {
	                if(isset($eRCTemplateSetting['NumberOfStudentCalculationExcludedSpecialCase']) && $thisGrade != '' && in_array($thisGrade, (array)$eRCTemplateSetting['NumberOfStudentCalculationExcludedSpecialCase'])) {
	                    $ExcludeStudentIDArr[] = $thisStudentID;
	                }
	                continue;
	            }
	        }
	        
	        ### If the student is in the exclude list, do not count the student
	        if ($eRCTemplateSetting['DoNotCountExcludedStudentInStudentNumOfClassAndForm'] && $numOfExclude > 0) {
	            if (in_array($thisStudentID, (array)$ExcludeStudentsArr)) {
	                if(isset($eRCTemplateSetting['NumberOfStudentCalculationExcludedSpecialCase']) && $thisGrade != '' && in_array($thisGrade, (array)$eRCTemplateSetting['NumberOfStudentCalculationExcludedSpecialCase'])) {
	                    $ExcludeStudentIDArr[] = $thisStudentID;
	                }
	                continue;
	            }
	        }
	        
	        $CountedStudentIDArr[] = $thisStudentID;
	    }
	    
	    return array($CountedStudentIDArr, $ExcludeStudentIDArr);
	}
	
	function GET_EXIST_TERM_RESULT_SCORE($completedReportID, $completedSemester, $ReportID, $reportTermColumnMap, $Mode="T", $ClassLevelID='')
	{
		global $calculationSettings, $ReportCardCustomSchoolName, $eRCTemplateSetting;
		
		if (!isset($calculationSettings)) {
			$calculationSettings = $this->LOAD_SETTING("Calculation");
		}
			
		// $SubjectGradingSchemeAssoArr[$SubjectID] = Scheme InfoArr
		$SubjectGradingSchemeAssoArr = array();
		
		// select existing result from TERM report
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$sql  = "SELECT StudentID, SubjectID, Mark, Grade, RawMark, OrderMeritClass, OrderMeritForm, ReportColumnID ";
		$sql .= "FROM $table WHERE ReportID = '$completedReportID' ";
		if ($Mode == "T")
		{
			//$sql .= "AND (ReportColumnID IS NULL OR ReportColumnID = 0)";
			if ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment']) {
				$sql .= " AND ReportColumnID IN ('".implode("','", array_keys((array)$reportTermColumnMap[$completedSemester]))."') ";
			}
			else {
				$sql .= " AND Semester = '$completedSemester' AND (ReportColumnID IS NULL OR ReportColumnID = 0) ";
			}
		}
		else
		{
			$sql .= "AND ReportColumnID IS NOT NULL AND ReportColumnID != 0";
		}
		$existTermSubjectOverall = $this->returnArray($sql, 6);
		
		$existTermSubjectOverallArr = array();
		$existTermSubjectOverallValues = array();
		if (sizeof($existTermSubjectOverall) > 0)
		{
			for($i=0; $i<sizeof($existTermSubjectOverall); $i++)
			{
				$tmpStudentID = $existTermSubjectOverall[$i]["StudentID"];
				$tmpSubjectID = $existTermSubjectOverall[$i]["SubjectID"];
				$tmpMark = $existTermSubjectOverall[$i]["Mark"];
				$tmpGrade = $existTermSubjectOverall[$i]["Grade"];
				$tmpRawMark = $existTermSubjectOverall[$i]["RawMark"];
				$tmpOrderMeritClass = $existTermSubjectOverall[$i]["OrderMeritClass"];
				$tmpOrderMeritForm = $existTermSubjectOverall[$i]["OrderMeritForm"];
				
				$tmpTermReportColumnID = $existTermSubjectOverall[$i]["ReportColumnID"];
				if ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment']) {
					$tmpReportColumnID = $reportTermColumnMap[$completedSemester][$tmpTermReportColumnID];
				}
				else {
					$tmpReportColumnID = $reportTermColumnMap[$completedSemester];
				}
				
				if ($tmpMark !== "" && $calculationSettings["UseWeightedMark"] == "1") {
					$tmpMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $tmpReportColumnID, $tmpSubjectID, $tmpMark);
				}
				
				if ($eRCTemplateSetting['ConsolidatedReportUseGPAToCalculate'])
				{
					if (!isset($SubjectGradingSchemeAssoArr[$tmpSubjectID])) {
						$SubjectGradingSchemeAssoArr[$tmpSubjectID] = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 0, 0, $ReportID);
					}
					
					$SubjectFormGradingSettings = $SubjectGradingSchemeAssoArr[$tmpSubjectID]; 
					$SchemeID = $SubjectFormGradingSettings['SchemeID'];
					$tmpMark = $this->CONVERT_GRADE_TO_GRADE_POINT_FROM_GRADING_SCHEME($SchemeID, $tmpGrade);
					$tmpRawMark = $tmpMark;
				}
				
				$existTermSubjectOverallArr[$tmpStudentID][$tmpSubjectID][] = 
					array (
						"ReportColumnID" => $tmpReportColumnID,
						"Mark" => $tmpMark,
						"Grade" => $tmpGrade,
						"RawMark" => $tmpRawMark,
						"OrderMeritClass" => $tmpOrderMeritClass,
						"OrderMeritForm" => $tmpOrderMeritForm
					);
			}
		}
		
		if (sizeof($existTermSubjectOverallValues) > 0) {
			return $existTermSubjectOverallArr;
		}
		else {
			return false;
		}
	}
	
	function COPY_EXIST_TERM_RESULT_SCORE($completedReportID, $completedSemester, $ReportID, $reportTermColumnMap, $Mode="T", $ClassLevelID='') {
		global $calculationSettings, $ReportCardCustomSchoolName, $eRCTemplateSetting;
		
		if (!isset($calculationSettings))
			$calculationSettings = $this->LOAD_SETTING("Calculation");
			
		// $SubjectGradingSchemeAssoArr[$SubjectID] = Scheme InfoArr
		$SubjectGradingSchemeAssoArr = array();
		
		// select existing result from TERM report
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$sql  = "SELECT StudentID, SubjectID, Mark, Grade, RawMark, OrderMeritClass, OrderMeritForm, ReportColumnID ";
		//$sql .= "FROM $table WHERE ReportID = '$completedReportID' AND Semester = '$completedSemester' ";
		$sql .= "FROM $table WHERE ReportID = '$completedReportID' ";
		if ($Mode == "T") {
			//$sql .= "AND (ReportColumnID IS NULL OR ReportColumnID = 0)";
			if ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment']) {
				$sql .= " AND ReportColumnID IN ('".implode("','", array_keys((array)$reportTermColumnMap[$completedSemester]))."') ";
			}
			else {
				$sql .= " AND Semester = '$completedSemester' AND (ReportColumnID IS NULL OR ReportColumnID = 0) ";
			}
		}
		else {
			$sql .= "AND ReportColumnID IS NOT NULL AND ReportColumnID != 0";
		}
		$existTermSubjectOverall = $this->returnArray($sql, 6);
		
		
		$existTermSubjectOverallArr = array();
		$existTermSubjectOverallValues = array();
		if (sizeof($existTermSubjectOverall) > 0) {
			for($i=0; $i<sizeof($existTermSubjectOverall); $i++) {
				$tmpStudentID = $existTermSubjectOverall[$i]["StudentID"];
				$tmpSubjectID = $existTermSubjectOverall[$i]["SubjectID"];
				$tmpMark = $existTermSubjectOverall[$i]["Mark"];
				$tmpGrade = $existTermSubjectOverall[$i]["Grade"];
				$tmpRawMark = $existTermSubjectOverall[$i]["RawMark"];
				$tmpOrderMeritClass = $existTermSubjectOverall[$i]["OrderMeritClass"];
				$tmpOrderMeritForm = $existTermSubjectOverall[$i]["OrderMeritForm"];
				//$tmpReportColumnID = $reportTermColumnMap[$completedSemester];
				$tmpTermReportColumnID = $existTermSubjectOverall[$i]["ReportColumnID"];
				
				if ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment']) {
					$tmpReportColumnID = $reportTermColumnMap[$completedSemester][$tmpTermReportColumnID];
				}
				else {
					$tmpReportColumnID = $reportTermColumnMap[$completedSemester];
				}
				
				if ($tmpMark !== "" && $calculationSettings["UseWeightedMark"] == "1") {
					$tmpMark = $this->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $tmpReportColumnID, $tmpSubjectID, $tmpMark);
				}
				
				if ($eRCTemplateSetting['ConsolidatedReportUseGPAToCalculate']) {
					if (!isset($SubjectGradingSchemeAssoArr[$tmpSubjectID])) {
						$SubjectGradingSchemeAssoArr[$tmpSubjectID] = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 0, 0, $ReportID);
					}
					
					$SubjectFormGradingSettings = $SubjectGradingSchemeAssoArr[$tmpSubjectID]; 
					$SchemeID = $SubjectFormGradingSettings['SchemeID'];
					$tmpMark = $this->CONVERT_GRADE_TO_GRADE_POINT_FROM_GRADING_SCHEME($SchemeID, $tmpGrade);
					$tmpRawMark = $tmpMark;
				}
				
				$existTermSubjectOverallArr[$tmpStudentID][$tmpSubjectID][] = 
					array (
						"ReportColumnID" => $tmpReportColumnID,
						"Mark" => $tmpMark,
						"Grade" => $tmpGrade,
						"RawMark" => $tmpRawMark,
						"OrderMeritClass" => $tmpOrderMeritClass,
						"OrderMeritForm" => $tmpOrderMeritForm
					);
				$existTermSubjectOverallValues[] = "('$tmpStudentID', '$tmpSubjectID', '$ReportID', '$tmpReportColumnID', '$tmpMark', '$tmpGrade', '$tmpRawMark', '0', '$completedSemester', '$tmpOrderMeritClass', '$tmpOrderMeritForm', NOW(), NOW())";
			}
		}
		
		if (sizeof($existTermSubjectOverallValues) > 0) {
			$existTermSubjectOverallValues = implode(",", $existTermSubjectOverallValues);
			$success = $this->INSERT_REPORT_RESULT_SCORE($existTermSubjectOverallValues);
		} else {
			$success = false;
		}
		if ($success) {
			return $existTermSubjectOverallArr;
		} else {
			return false;
		}
	}
	
	function COPY_EXIST_TERM_RESULT($completedReportID, $completedSemester, $ReportID, $reportTermColumnMap) {
		global $eRCTemplateSetting;
		
		$table = $this->DBName.".RC_REPORT_RESULT";
		$sql  = "SELECT StudentID, GrandTotal, GrandAverage, ActualAverage, GPA, OrderMeritClass, OrderMeritForm, ClassNoOfStudent, FormNoOfStudent, ReportColumnID ";
		$sql .= "FROM $table ";
		//$sql .= "WHERE ReportID = '$completedReportID' and ReportColumnID = '0' ";
		$sql .= "WHERE ReportID = '$completedReportID'";
		
		if ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment']) {
			$sql .= " AND ReportColumnID is not null AND ReportColumnID != 0 ";
		}
		else {
			$sql .= " and ReportColumnID = '0' ";
		}
		
		$existTermAverageTotal = $this->returnArray($sql);
		
		$existTermAverageTotalArr = array();
		$existTermAverageTotalValues = array();
		for($i=0; $i<sizeof($existTermAverageTotal); $i++) {
			$tmpStudentID = $existTermAverageTotal[$i]["StudentID"];
			$tmpGrandTotal = $existTermAverageTotal[$i]["GrandTotal"];
			$tmpGrandAverage = $existTermAverageTotal[$i]["GrandAverage"];
			$tmpActualAverage = $existTermAverageTotal[$i]["ActualAverage"];
			$tmpGPA = $existTermAverageTotal[$i]["GPA"] == 0 ? "" : $existTermAverageTotal[$i]["GPA"];
			$tmpOrderMeritClass = $existTermAverageTotal[$i]["OrderMeritClass"] == 0 ? "" : $existTermAverageTotal[$i]["OrderMeritClass"];
			$tmpOrderMeritForm = $existTermAverageTotal[$i]["OrderMeritForm"] == 0 ? "" : $existTermAverageTotal[$i]["OrderMeritForm"];
			$tmpClassNoOfStudent = $existTermAverageTotal[$i]["ClassNoOfStudent"];
			$tmpFormNoOfStudent = $existTermAverageTotal[$i]["FormNoOfStudent"];
			$tmpTermReportColumnID = $existTermAverageTotal[$i]["ReportColumnID"];
			
			//$tmpReportColumnID = $reportTermColumnMap[$completedSemester];
			if ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment']) {
				$tmpReportColumnID = $reportTermColumnMap[$completedSemester][$tmpTermReportColumnID];
			}
			else {
				$tmpReportColumnID = $reportTermColumnMap[$completedSemester];
			}
			
			$existTermAverageTotalArr[$tmpReportColumnID][$tmpStudentID] = 
				array (
					"GrandTotal" => $tmpGrandTotal,
					"GrandAverage" => $tmpGrandAverage,
					"ActualAverage" => $tmpActualAverage,
					"GPA" => $tmpGPA,
					"OrderMeritClass" => $tmpOrderMeritClass,
					"OrderMeritForm" => $tmpOrderMeritForm,
					"ClassNoOfStudent" => $tmpClassNoOfStudent,
					"FormNoOfStudent" => $tmpFormNoOfStudent
				);
			$existTermAverageTotalValues[] = "('$tmpStudentID', '$ReportID', '$tmpReportColumnID', '$tmpGrandTotal', '$tmpGrandAverage', '$tmpActualAverage', '$tmpGPA', '$tmpOrderMeritClass', '$tmpClassNoOfStudent', '$tmpOrderMeritForm', '$tmpFormNoOfStudent', '', '', '$completedSemester', '',NOW(), NOW())";
		}
		
		if (sizeof($existTermAverageTotalValues) > 0) {
			$existTermAverageTotalValues = implode(",", $existTermAverageTotalValues);
			$fields = "StudentID, ReportID, ReportColumnID, GrandTotal, GrandAverage, ActualAverage, GPA, OrderMeritClass, ClassNoOfStudent, OrderMeritForm, FormNoOfStudent, Promotion, NewClassName, Semester, AdjustedBy, DateInput, DateModified";
			$sql = "INSERT INTO $table ($fields) VALUES $existTermAverageTotalValues";
			$success = $this->db_db_query($sql);
		} else {
			$success = false;
		}
		if ($success) {
			return $existTermAverageTotalArr;
		} else {
			return false;
		}
	}
	
	function ROUND_GRAND_MARKS($GrandMarkArr)
	{
		$thisGrandTotal = $GrandMarkArr["GrandTotal"];
		$thisGrandAverage = $GrandMarkArr["GrandAverage"];
		$thisGradePointAverage = $GrandMarkArr["GradePointAverage"];
		
		// round grand total according to the same setting of average???
		$thisGrandTotal = $this->ROUND_MARK($thisGrandTotal, "GrandTotal");
		$thisGrandAverage = $this->ROUND_MARK($thisGrandAverage, "GrandAverage");
		$thisGradePointAverage = $this->ROUND_MARK($thisGradePointAverage, "GrandAverage");
		
		$returnArr = array();
		$returnArr["GrandTotal"] = $thisGrandTotal;
		$returnArr["GrandAverage"] = $thisGrandAverage;
		$returnArr["GradePointAverage"] = $thisGradePointAverage;
		
		return $returnArr;
	}
	
	
	################################################################################
	#	For carmel alison SD Score calcution and Rank Ordering START	
	
	# This function is for Carmel Alison Only , Used to Calculate WeightedStandardScore
	function UPDATE_SD_SCORE($ReportID, $ClassLevelID, $ReportType)
	{
		global $PATH_WRT_ROOT, $eRCTemplateSetting;
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		
		// [2016-0217-0905-03207]
		$useSpecialCaseHandlingforOrdering = isset($eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent']) && is_array($eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent']);
		
		if($ReportType == "W")
		{
			$MarksColumnID = 0;
			
			$success["COPY_OVERALL_SDSCORE_FROM_TERM"] = $this->COPY_OVERALL_SDSCORE_FROM_TERM($ReportID);
		}
		else 
			$MarksColumnID = '';
			
		$thisMarksAry = $this->getMarks($ReportID, "","","",1,"","",$MarksColumnID);
		$SpcialCase = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		
		foreach((array)$thisMarksAry as $StudentID => $thisStudentMarks)
		{
			// [2016-0217-0905-03207]
			//if(isset($eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent'])){
			if($useSpecialCaseHandlingforOrdering){
				$excludeOrderColumnData = array();
				$availableSubjectCount = 0;
			}
			
			foreach((array) $thisStudentMarks as $thisSubjectID => $thisColumnMarks)
			{
				# Retrieve Subject Scheme ID & settings
				
				if(!isset($SubjectFormGradingSettingsArr[$ClassLevelID][$thisSubjectID]))
				{
					$SubjectFormGradingSettingsArr[$ClassLevelID][$thisSubjectID] = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID, 0, 0, $ReportID);
				}
				$SubjectFormGradingSettings = $SubjectFormGradingSettingsArr[$ClassLevelID][$thisSubjectID];
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				//$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				// [2016-0217-0905-03207] update $availableSubjectCount for main subject when $useSpecialCaseHandlingforOrdering is true
				//if(isset($eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent'])){
				if($useSpecialCaseHandlingforOrdering){
					$isSubjComponent = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID);
					if ($isSubjComponent==false)
					{
						$availableSubjectCount++;
					}
				}
								
				foreach((array) $thisColumnMarks as $thisColumnID => $thisMarks)
				{
					if ($eRCTemplateSetting['CalculateAssessmentSDScore']==false && $thisColumnID != 0)
						continue;
						
					if(!isset($SDAVGAry[$ReportID][$thisSubjectID][$thisColumnID]))
					{
						$SDAVGAry[$ReportID][$thisSubjectID][$thisColumnID] = $this->getFormSubjectSDAndAverage($ReportID,$thisSubjectID,'',$thisColumnID); 
					}
					list($SD,$AVG) = $SDAVGAry[$ReportID][$thisSubjectID][$thisColumnID];
					
					$thisReportResultScoreID = $thisMarks['ReportResultScoreID'];
					
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMarks['Grade']!='' )? $thisMarks['Grade'] : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMarks['Mark'] : "";
					$thisRawMark = $thisMarks['RawMark'];
					
					$thisTargetMark = ($eRCTemplateSetting['UseRawMarkToCalculateSDScore'])? $thisRawMark : $thisMark;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisTargetMark : $thisGrade;
					
					$StandardMark = 0;
					$StandardMarkRaw = 0;
					# calculate and save Standard Score
					if (trim($thisMark)!=='' && !in_array($thisMark,$SpcialCase) && $thisMark!= -1 && $SD>0)
					{
						$StandardMarkRaw = is_numeric($thisMark)? $thisMark : 0;
						$StandardMarkRaw = ($StandardMarkRaw - $AVG) / $SD;
						$StandardMark = my_round($StandardMarkRaw, 4);
						
						// update standardScore
						$success["UPDATE_STANDARD_SCORE"][]=$this->UPDATE_STANDARD_SCORE($ReportID, $thisColumnID, $thisSubjectID,$StudentID, $StandardMark, $StandardMarkRaw, $thisReportResultScoreID);
					}
					// [2016-0217-0905-03207] update $excludeOrderColumnData for main subject when $useSpecialCaseHandlingforOrdering is true and mark is defined special characters
					// else if(isset($eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent']) && in_array($thisMark,$eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent'])){
					else if($useSpecialCaseHandlingforOrdering && in_array($thisMark,$eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent'])){
						$isSubjComponent = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID);
						if ($isSubjComponent==false)
						{
							$excludeOrderColumnData[$thisColumnID] = $excludeOrderColumnData[$thisColumnID]? $excludeOrderColumnData[$thisColumnID] : 0;
							$excludeOrderColumnData[$thisColumnID] = $excludeOrderColumnData[$thisColumnID] + 1;
						}
						continue;
					}
					else
					{
						// [2016-0217-0905-03207] update $excludeOrderColumnData for main subject when $useSpecialCaseHandlingforOrdering is true and mark is N.A.
						// if(isset($eRCTemplateSetting['SpecialCaseThatExcludedOrderingOfStudent']) && $thisMark=="N.A."){
						if($useSpecialCaseHandlingforOrdering && $thisMark=="N.A."){
							$isSubjComponent = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID);
							if ($isSubjComponent==false)
							{
								$excludeOrderColumnData[$thisColumnID] = $excludeOrderColumnData[$thisColumnID]? $excludeOrderColumnData[$thisColumnID] : 0;
								$excludeOrderColumnData[$thisColumnID] = $excludeOrderColumnData[$thisColumnID] + 1;
							}							
						}
						
						$StandardMark = 0;
						$StandardMarkRaw = 0;
						$success["UPDATE_STANDARD_SCORE"][]=$this->UPDATE_STANDARD_SCORE($ReportID, $thisColumnID, $thisSubjectID,$StudentID, $StandardMark, $StandardMarkRaw, $thisReportResultScoreID);
						continue;
					}

					# only works when CalSetting=1
					if(!isset($SubjectWeightAry[$thisSubjectID])) // check if weight was retrieve before, to save query time
					{
						# get vertical weight, weight among subjects 
						$ColumnIDCond = "( ReportColumnID='' OR ReportColumnID IS NULL )";
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, $ColumnIDCond." and SubjectID = '$thisSubjectID' ");
						$SubjectWeightAry[$thisSubjectID] = $thisSubjectWeightData[0]['Weight'];
					}
					
					# Calculate overall SD Score (exclude component subjects)
					//$objSubject = new subject($thisSubjectID);
					//$isComponent = $objSubject->Is_Component_Subject();
					$isComponent = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID);
					if ($isComponent==false)
					{
						# calculate total weight, exclude special case
						if (!in_array($thisMark,$SpcialCase) && $thisMark!= -1)
							$TotalWeight[$StudentID][$thisColumnID] += $SubjectWeightAry[$thisSubjectID];
							 
						# sum up weighted SD Score
						$TargetStandardMark = ($eRCTemplateSetting['UseRawMarkToCalculateSDScore'])? $StandardMarkRaw : $StandardMark;
						$TotalWeightedSDScoreAry[$StudentID][$thisColumnID] +=  $TargetStandardMark * $SubjectWeightAry[$thisSubjectID];
					} 
				}
			}
			
			// [2016-0217-0905-03207] update weight and weighted SD score for special case exclude ordering
			if($useSpecialCaseHandlingforOrdering && count((array)$excludeOrderColumnData)>0 && $availableSubjectCount>0){
				//$availableSubjectCount = count($thisStudentMarks);
				foreach((array) $excludeOrderColumnData as $thisColumnID => $thisCount){
					if($thisCount == $availableSubjectCount){
						$TotalWeight[$StudentID][$thisColumnID] = 1;
						$TotalWeightedSDScoreAry[$StudentID][$thisColumnID] = -9;
					}
				}
			}
		}
		
		# Calculate GrandSDScore 
		foreach((array)$TotalWeightedSDScoreAry as $StudentID => $ColumnWeightedSDScore)
		{
			foreach((array)$ColumnWeightedSDScore as $thisColumnID => $WeightedSDScore)
			{
				if ($eRCTemplateSetting['CalculateAssessmentSDScore']==false && $thisColumnID != 0)
						continue;
						
				$thisTotalWeight = $TotalWeight[$StudentID][$thisColumnID];
				$thisAvgWeightSDScoreRaw = $WeightedSDScore / $thisTotalWeight;
				$thisAvgWeightSDScore = my_round($thisAvgWeightSDScoreRaw, 4);

				// update grandstandardScore
				$success["UPDATE_GRAND_STANDARD_SCORE"][]=$this->UPDATE_GRAND_STANDARD_SCORE($ReportID, $thisColumnID, $StudentID, $thisAvgWeightSDScore, $thisAvgWeightSDScoreRaw);
			}
		}
		
		return in_array(false,(array)$success["UPDATE_STANDARD_SCORE"]) || in_array(false,(array)$success["UPDATE_GRAND_STANDARD_SCORE"])	?false:true;	
	}
	
	function UPDATE_STANDARD_SCORE($ReportID, $ReportColumnID, $SubjectID, $StudentID, $SDScore, $RawSDScore, $ReportResultScoreID='')
	{
		$conds = '';
		if ($ReportResultScoreID != '')
			$conds .= " ReportResultScoreID = '$ReportResultScoreID' ";
		else
			$conds .= " ReportID = '$ReportID'
						AND ReportColumnID = '$ReportColumnID'
						AND SubjectID = '$SubjectID'
						AND StudentID = '$StudentID'
						";
		
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$sql = "
			UPDATE 
				$table
			SET
				SDScore = '$SDScore',
				RawSDScore = '$RawSDScore'
			WHERE
				$conds
		";

		//debug_pr($sql);
		$success = $this->db_db_query($sql) or die(mysql_error());
		
		return $success;
	
	}	
	
	function UPDATE_GRAND_STANDARD_SCORE($ReportID, $ReportColumnID, $StudentID, $GrandSDScore, $RawGrandSDScore)
	{
		$table = $this->DBName.".RC_REPORT_RESULT";
			
		$sql = "
			UPDATE 
				$table
			SET
				GrandSDScore = '$GrandSDScore',
				RawGrandSDScore = '$RawGrandSDScore'
			WHERE
				ReportID = '$ReportID'
				AND ReportColumnID = '$ReportColumnID'
				AND StudentID = '$StudentID'
		";
		//debug_pr($sql);
		$success = $this->db_db_query($sql);		
		return $success;
	
	}	
	
	function COPY_OVERALL_SDSCORE_FROM_TERM($ReportID)
	{
		# Get ReportColumnID (Used to get involved ReportID)
		$ReportColumnIDArr = $this->returnReportTemplateColumnData($ReportID);
		
		for($i=0; $i<sizeof($ReportColumnIDArr);$i++)
		{
			# Get Overall Marks Of Involved Term Report Marks (i.e. ReportColumnID = 0)
			$ReportColumnID = $ReportColumnIDArr[$i]["ReportColumnID"];
			$thisReportID = $this->getCorespondingTermReportID($ReportID,$ReportColumnID);
			$thisMarksAry = $this->getMarks($thisReportID, "","","",1,"","",0);
			
			foreach((array)$thisMarksAry as $StudentID => $thisStudentMarks)
			{
				foreach((array) $thisStudentMarks as $thisSubjectID => $thisColumnMarks)
				{
					foreach((array) $thisColumnMarks as $thisColumnID => $thisMarks)
					{
						# Copy Marks to column of Consolidate Report
						$success[] = $this->UPDATE_STANDARD_SCORE($ReportID, $ReportColumnID, $thisSubjectID, $StudentID, $thisMarks["SDScore"], $thisMarks["RawSDScore"]);	
					}
				}
			}

		}
		
		return in_array(false,$success)? false:true;
		
	} 
	
	#	For carmel alison SD Score calcution and Rank Ordering END
	################################################################################
	
	function UpdateOverallGradeFromAssessmentGrade($ReportID) {
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID =  $ReportInfoArr['ClassLevelID'];
		
		### Get Subject
		$SubjectInfoAssoArr = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, 0, $ReportID);
		$SubjectIDArr = array_keys((array)$SubjectInfoAssoArr);
		
		### Get Student
		$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
		$numOfStudent = count($StudentInfoArr);
		
		### Get Report Column
		$ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
		$numOfColumn = count($ReportColumnInfoArr);
		
		### Get Weight
		$WeightAssoArr = BuildMultiKeyAssoc($this->returnReportTemplateSubjectWeightData($ReportID, '', $SubjectIDArr), array('SubjectID', 'ReportColumnID'));
				
		### Get Marks
		//$MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Mark', 'Grade', etc...] = data
		$MarksArr = $this->getMarks($ReportID, $StudentID='', $cons='', $ParentSubjectOnly=0, $includeAdjustedMarks=0, $OrderBy='', $SubjectID='', $ReportColumnID='', $CheckPositionDisplay=0, $SubOrderArr='', $ByCache=false);
		
		// loop subjects
		$SuccessArr = array();
		foreach ((array)$SubjectInfoAssoArr as $thisSubjectID => $thisSubjectInfoArr) {
			$thisSchemeID = $thisSubjectInfoArr['schemeID'];
			$thisScaleInput = $thisSubjectInfoArr['scaleInput'];
			$thisScaleDisplay = $thisSubjectInfoArr['scaleDisplay'];
			
			// for input grade and display grade subject only
			if ($thisScaleInput=='G' && $thisScaleDisplay=='G') {
				// loop students
				for ($i=0; $i<$numOfStudent; $i++) {
					$thisStudentID = $StudentInfoArr[$i]['UserID'];
					
					$thisOverallWeightedGpa = '';
					$thisOverallGrade = '';
					$thisTotalWeight = 0;
					$thisExcludedWeight = 0;
					for ($j=0; $j<$numOfColumn; $j++) {
						$thisReportColumnID = $ReportColumnInfoArr[$j]['ReportColumnID'];
						$thisWeight = $WeightAssoArr[$thisSubjectID][$thisReportColumnID]['Weight'];
						$thisTotalWeight += $thisWeight;
						
						$thisMark = $MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Mark'];
						$thisGrade = $MarksArr[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Grade'];
						
						if ($this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
							$thisExcludedWeight += $thisWeight;
						}
						else {
							$thisGpa = $this->CONVERT_GRADE_TO_GRADE_POINT_FROM_GRADING_SCHEME($thisSchemeID, $thisGrade, $ReportID, $thisStudentID, $thisSubjectID, $ClassLevelID, $thisReportColumnID);
							$thisWeightedGpa = $thisGpa * $thisWeight;
							$thisOverallWeightedGpa += $thisWeightedGpa;
						}
					}
					
					if ($thisExcludedWeight == $thisTotalWeight) {
						$thisOverallGrade = 'N.A.';
					}
					else {
						$thisRemainingWeight = $thisTotalWeight - $thisExcludedWeight;
						if ($thisRemainingWeight > 0) {
							$thisOverallWeightedGpa = $thisOverallWeightedGpa * ($thisTotalWeight / $thisRemainingWeight);
						}
						$thisOverallWeightedGpa = my_round($thisOverallWeightedGpa, 0);
						
						$thisSchemeRangeInfoArr = $this->GET_GRADING_SCHEME_RANGE_INFO($thisSchemeID);
						$numOfRange = count($thisSchemeRangeInfoArr);
						for ($j=0; $j<$numOfRange; $j++) {
							$thisGradePoint = $thisSchemeRangeInfoArr[$j]['GradePoint'];
							$thisGrade = $thisSchemeRangeInfoArr[$j]['Grade'];
							
							if ($thisGradePoint==$thisOverallWeightedGpa) {
								$thisOverallGrade = $thisGrade;
								break;
							}
						}
					}
					
					if ($thisOverallGrade !== '') {
						$thisReportResultScoreID = $MarksArr[$thisStudentID][$thisSubjectID][0]['ReportResultScoreID'];
						
						$RC_REPORT_RESULT_SCORE = $this->DBName.".RC_REPORT_RESULT_SCORE";
						$sql = "Update $RC_REPORT_RESULT_SCORE set Grade = '".$this->Get_Safe_Sql_Query($thisOverallGrade)."' Where ReportResultScoreID = '".$thisReportResultScoreID."'";
						$SuccessArr['Student='.$thisStudentID.'_Subject='.$thisSubjectID] = $this->db_db_query($sql);
					}
				}	// end loop students
			}	// end if ($thisScaleInput=='G' && $thisScaleDisplay=='G')
		}	// end loop subjects
		
		return (in_array(false, $SuccessArr))? false : true;
	}
	
	function UpdateActualAverage($ReportID) {
		global $PATH_WRT_ROOT, $intranet_root;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$lfs = new libfilesystem();
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID =  $ReportInfoArr['ClassLevelID'];
		$YearTermID =  $ReportInfoArr['SemID'];
		$ReportType = ($YearTermID=='F')? 'W' : 'T'; 
		
		$TermStartDate 	= (is_date_empty($ReportInfoArr['TermStartDate']))? '' : $ReportInfoArr['TermStartDate'];
		$TermEndDate 	= (is_date_empty($ReportInfoArr['TermEndDate']))? '' : $ReportInfoArr['TermEndDate'];
		
		if ($ReportType == 'W') {
			$YearTermID = 0;
		}		
		
		### Get Grand Average
		$ReportColumnID = 0;
		$ResultScoreArr = $this->getReportResultScore($ReportID, $ReportColumnID, $StudentID='', $other_conds='', $debug=0, $includeAdjustedMarks=0, $CheckPositionDisplay=0, $FilterNoRanking=0, $OrderFieldArr='');
		
		### Get all classes
		$classInfoAry = $this->GET_CLASSES_BY_FORM($ClassLevelID);
		$numOfClass = count($classInfoAry);
		
		### Get all students
		$studentIdAry = Get_Array_By_Key($this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID), 'UserID');
		
		### Get Attendance Info
		//$campassionateLeaveInfoAry[$classId][$studentId]['Days Absent', 'Time Late', 'Early Leave'] = data
		$campassionateLeaveInfoAry = array();
		$truancyInfoAry = array();
		for ($i=0; $i<$numOfClass; $i++) {
			$_classId = $classInfoAry[$i]['ClassID'];
			
			// 2015-0126-0940-57207 - using $studentIDAry
//			$campassionateLeaveInfoAry[$_classId] = $this->Get_Student_Profile_Attendance_Data($YearTermID, $_classId, $TermStartDate, $TermEndDate, $ClassName='', $StudentID='', '事假');
//			$truancyInfoAry[$_classId] = $this->Get_Student_Profile_Attendance_Data($YearTermID, $_classId, $TermStartDate, $TermEndDate, $ClassName='', $StudentID='', '旷课');
			$campassionateLeaveInfoAry[$_classId] = $this->Get_Student_Profile_Attendance_Data($YearTermID, $_classId, $TermStartDate, $TermEndDate, $ClassName='', $studentIdAry, '事假');
			$truancyInfoAry[$_classId] = $this->Get_Student_Profile_Attendance_Data($YearTermID, $_classId, $TermStartDate, $TermEndDate, $ClassName='', $studentIdAry, '旷课');
			
			//2013-0405-0845-35156
			$attendaceInfoAry[$_classId] = $this->Get_Student_Profile_Attendance_Data($YearTermID, $_classId, $TermStartDate, $TermEndDate, '', $studentIdAry);
		}
		
		
		$successAry = array();
		$logContent = '';
		$logContent .= "ResultID"."\t"."StudentID"."\t"."GrandAverage"."\t"."CampassionateLeave"."\t"."Truancy"."\t"."Late"."\t"."DeductMark"."\t"."ActualAverage"."\r\n";
		foreach ((array)$ResultScoreArr as $_studentId => $_studentResultAry) {
			$_resultId = $_studentResultAry[$ReportColumnID]['ResultID'];
			$_studentId = $_studentResultAry[$ReportColumnID]['StudentID'];
			$_grandAverage = $_studentResultAry[$ReportColumnID]['GrandAverage'];
			
			$_studentInfoAry = $this->Get_Student_Class_ClassLevel_Info($_studentId);
			$_classId = $_studentInfoAry[0]['ClassID'];
			
			if ($_grandAverage == -1) {
				$_actualAverage = $_grandAverage;
				$_deductMark = 0;
				$_campassionateLeave = 0;
				$_truancy = 0;
				$_late = 0;
				
				$successAry[$_studentId] = $this->UPDATE_REPORT_RESULT($_resultId, $_actualAverage);
			}
			else {
				$_campassionateLeave = $campassionateLeaveInfoAry[$_classId][$_studentId]['Days Absent'];
				$_campassionateLeave = ($_campassionateLeave=='')? 0 : $_campassionateLeave;
				
				$_truancy = $truancyInfoAry[$_classId][$_studentId]['Days Absent'];
				$_truancy = ($_truancy=='')? 0 : $_truancy;
				
				//2013-0405-0845-35156
				$_late = $attendaceInfoAry[$_classId][$_studentId]['Time Late'];
				$_late = ($_late=='')? 0 : $_late;
				
				$_deductMark = $_campassionateLeave * 0.2 + $_truancy * 0.5 + $_late * 0.01;
				$_actualAverage = $_grandAverage - $_deductMark; 
				
				$successAry[$_studentId] = $this->UPDATE_REPORT_RESULT($_resultId, $_actualAverage);
			}
			
			$logContent .= $_resultId."\t".$_studentId."\t".$_grandAverage."\t".$_campassionateLeave."\t".$_truancy."\t".$_late."\t".$_deductMark."\t".$_actualAverage."\r\n";
		}
		
		### Log the Processs in a file
		$logFolder = $intranet_root."/file/reportcard2008/".$this->GET_ACTIVE_YEAR()."/report_generate_log";
		if(!file_exists($logFolder)) {
			$SuccessAry['log']['createLogFolder'] = $lfs->folder_new($logFolder);
		}
		$logFile = $logFolder.'/UpdateActualAverage_'.$ReportID.'_'.date('Ymd_His').'_'.$_SESSION['UserID'].'.txt';
		$successAry['log']['writeLog'] = $lfs->file_write($logContent, $logFile);
		
		return in_array(false, (array)$successAry)? false : true;
	}
	
	function UpdatePassingStatusSpecialChecking($ReportID) {
		global $PATH_WRT_ROOT, $intranet_root;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$lfs = new libfilesystem();
		
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$classLevelId =  $reportInfoAry['ClassLevelID'];
		
		$reportColumnAry = $this->returnReportTemplateColumnData($ReportID);
		$reportColumnAry[count($reportColumnAry)]['ReportColumnID'] = 0;
		$numOfColumn = count($reportColumnAry);
		
		$studentIdAry = Get_Array_By_Key($this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $classLevelId), 'UserID');
		$numOfStudent = count($studentIdAry);
		
		$subjectAry = $this->returnSubjectwOrder($classLevelId, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		$gradingSchemeAry = $this->GET_SUBJECT_FORM_GRADING($classLevelId, $SubjectID='', $withGrandResult=0, $returnAsso=1, $ReportID);
		$marksAry = $this->getMarksCommonIpEj($ReportID, $StudentID='', $cons='', $ParentSubjectOnly=0, $includeAdjustedMarks=1, $OrderBy='', $SubjectID='', $ReportColumnID='', $CheckPositionDisplay=0, $SubOrderArr='', $ByCache=false);
		$markNatureAry = $this->Get_Student_Subject_Mark_Nature_Arr($ReportID, $classLevelId, $marksAry);
		
		
		$successAry = array();
		$logContent = '';
		$logContent .= "ConvertType"."\t"."StudentID"."\t"."SubjectID"."\t"."ReportColumnID"."\t"."OriginalGrade"."\t"."NewGrade"."\t"."RecordID"."\r\n";
		
		for ($i=0; $i<$numOfStudent; $i++) {
			$_studentId = $studentIdAry[$i];
			
			### if all columns grade is pass => overall column grade is pass also
			foreach ((array)$subjectAry as $__parentSubjectId => $__cmpSubjectAry) {
				$__numOfCmpSubject = count($__cmpSubjectAry) - 1;
				$__isParentSubject = ($__numOfCmpSubject==0)? false : true;
				
				foreach ((array)$__cmpSubjectAry as $___cmpSubjectId => $___cmpSubjectName) {
					$___isCmpSubject = ($___cmpSubjectId == 0)? false : true;
					$___subjectId = ($___isCmpSubject)? $___cmpSubjectId : $__parentSubjectId;
					
					$___schemeId = $gradingSchemeAry[$___subjectId]['SchemeID'];
					$___scaleInput = $gradingSchemeAry[$___subjectId]['ScaleInput'];
					
					// skip parent subject
					// 2015-0716-0914-38222 - parent subject also need this operation
//					if ($__isParentSubject && !$___isCmpSubject) {
//						continue;
//					}
					
					// Consider input mark subject only
					if ($___scaleInput == 'M') {
						// check if overall grade is failed or not
						$___originalOverallGradeNature = $markNatureAry[$_studentId][$___subjectId][0];
						
						// do special grade conversion for failed subject only
						// check if all columns' grades are pass or not
						if ($___originalOverallGradeNature == 'Fail') {
							$___isAllColumnGradePass = true;
							
							for ($j=0; $j<$numOfColumn; $j++) {
								$____reportColumnId = $reportColumnAry[$j]['ReportColumnID'];
								
								if ($____reportColumnId == 0) {
									continue;
								}
								
								$____columnGradeNature = $markNatureAry[$_studentId][$___subjectId][$____reportColumnId];
								if ($____columnGradeNature == 'Fail') {
									$___isAllColumnGradePass = false;
									break;
								}
							}
							
							// convert the subject grade to the last passing grade if all other columns' grades are pass
							if ($___isAllColumnGradePass) {
								$___lastPassingGrade = $this->GET_GRADING_SCHEME_LAST_PASSING_GRADE($___schemeId);
								$___reportResultScoreId = $marksAry[$_studentId][$___subjectId][0]['ReportResultScoreID'];
								$___originalMark = $marksAry[$_studentId][$___subjectId][0]['Mark'];
								$___originalGrade = $marksAry[$_studentId][$___subjectId][0]['Grade'];
								
								$successAry['subjectOverallGradeChange']['student'.$_studentId.'_subject'.$___subjectId] = $this->UPDATE_REPORT_RESULT_SCORE('', '', '', '', $___originalMark, $___lastPassingGrade, $___reportResultScoreId);
								$logContent .= "subjectOverallGradeChange"."\t".$_studentId."\t".$___subjectId."\t"."0"."\t".$___originalGrade."\t".$___lastPassingGrade."\t".$___reportResultScoreId."\r\n";
							}
						}
					}
				}	// end loop component subject
			}	// end loop parent subject
			
			
			### If all component subjects are pass => parent subject is also pass
			foreach ((array)$subjectAry as $__parentSubjectId => $__cmpSubjectAry) {
				$__numOfCmpSubject = count($__cmpSubjectAry) - 1;
				$__isParentSubject = ($__numOfCmpSubject==0)? false : true;
				
				if (!$__isParentSubject) {
					continue;
				}
				
				$__schemeId = $gradingSchemeAry[$__parentSubjectId]['SchemeID'];
				$__lastPassingGrade = $this->GET_GRADING_SCHEME_LAST_PASSING_GRADE($__schemeId);
				
				for ($j=0; $j<$numOfColumn; $j++) {
					$___reportColumnId = $reportColumnAry[$j]['ReportColumnID'];
					
					// do special conversion for failed subject only
					$___originalGradeNature = $markNatureAry[$_studentId][$__parentSubjectId][$___reportColumnId];
					if ($___originalGradeNature != 'Fail') {
						continue;
					}
					
					$___isAllComponentPass = true;
					foreach ((array)$__cmpSubjectAry as $____cmpSubjectId => $____cmpSubjectName) {
						$____isCmpSubject = ($____cmpSubjectId == 0)? false : true;
						$____subjectId = ($____isCmpSubject)? $____cmpSubjectId : $__parentSubjectId;
						
						if (!$____isCmpSubject) {
							continue;
						}
						
						$____scaleInput = $gradingSchemeAry[$____subjectId]['ScaleInput'];
						if ($____scaleInput == 'M') {
							$____cmpGradeNature = $markNatureAry[$_studentId][$____subjectId][$___reportColumnId];
							
							if ($____cmpGradeNature == 'Fail') {
								$___isAllComponentPass = false;
								break;
							}
						}
					}
					
					if ($___isAllComponentPass) {
						$___reportResultScoreId = $marksAry[$_studentId][$__parentSubjectId][$___reportColumnId]['ReportResultScoreID'];
						$___originalMark = $marksAry[$_studentId][$__parentSubjectId][$___reportColumnId]['Mark'];
						$___originalGrade = $marksAry[$_studentId][$__parentSubjectId][$___reportColumnId]['Grade'];
						
						$successAry['parentSubjectGradeChange']['student'.$_studentId.'_parentSubject'.$__parentSubjectId.'_reportColumnId'.$___reportColumnId] = $this->UPDATE_REPORT_RESULT_SCORE('', '', '', '', $___originalMark, $__lastPassingGrade, $___reportResultScoreId);
						$logContent .= "parentSubjectGradeChange"."\t".$_studentId."\t".$__parentSubjectId."\t".$___reportColumnId."\t".$___originalGrade."\t".$__lastPassingGrade."\t".$___reportResultScoreId."\r\n";
					}
				}
			}
			
		}	// end loop student
		
		
		
		### Log the Processs in a file
		$logFolder = $intranet_root."/file/reportcard2008/".$this->GET_ACTIVE_YEAR()."/report_generate_log";
		if(!file_exists($logFolder)) {
			$SuccessAry['log']['createLogFolder'] = $lfs->folder_new($logFolder);
		}
		$logFile = $logFolder.'/UpdatePassingStatusSpecialChecking_'.$ReportID.'_'.date('Ymd_His').'_'.$_SESSION['UserID'].'.txt';
		$successAry['log']['writeLog'] = $lfs->file_write($logContent, $logFile);
		
		return !in_array(false, (array)$successAry);
	}
	
	function UpdateEffortAsFirstReportColumnGrade($parReportId) {
		global $PATH_WRT_ROOT, $intranet_root;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$lfs = new libfilesystem();
		
		// get report info
		$reportInfoAry = $this->returnReportTemplateBasicInfo($parReportId);
		$classLevelId =  $reportInfoAry['ClassLevelID'];
		
		// get first report column
		$reportColumnAry = $this->returnReportTemplateColumnData($parReportId);
		$firstReportColumnId = $reportColumnAry[0]['ReportColumnID'];
		
		// get student of the form
		$studentIdAry = Get_Array_By_Key($this->GET_STUDENT_BY_CLASSLEVEL($parReportId, $classLevelId), 'UserID');
		
		// get "effort" of the student
		//$extraInfoAssoAry[$StudentID][$thisSubjectID]['Info'] = 'xxx';
		$extraInfoAssoAry = $this->GET_EXTRA_SUBJECT_INFO($studentIdAry, '', $parReportId);
		
		// get mark array
		//$marksAry[$_studentId][$___subjectId][0]['ReportResultScoreID'] = 'xxx';
		$marksAry = $this->getMarksCommonIpEj($parReportId, $StudentID='', $cons='', $ParentSubjectOnly=0, $includeAdjustedMarks=1, $OrderBy='', $SubjectID='', $ReportColumnID='', $CheckPositionDisplay=0, $SubOrderArr='', $ByCache=false);
		
		$successAry = array();
		$logContent = '';
		$logContent .= "StudentID"."\t"."SubjectID"."\t"."ReportColumnID"."\t"."OriginalGrade"."\t"."NewGrade"."\t"."RecordID"."\r\n";
		
		foreach ((array)$extraInfoAssoAry as $_studentId => $_studentDataAry) {
			foreach ((array)$_studentDataAry as $__subjectId => $__studentSubjectDataAry) {
				$__effort = trim($__studentSubjectDataAry['Info']);
				
				if ($__effort != '') {
					$__recordId = $marksAry[$_studentId][$__subjectId][$firstReportColumnId]['ReportResultScoreID'];
					$__originalMark = $marksAry[$_studentId][$__subjectId][$firstReportColumnId]['Mark'];
					$__originalGrade = $marksAry[$_studentId][$__subjectId][$firstReportColumnId]['Grade'];
						
					$successAry['updateGrade']['student'.$_studentId.'_subject'.$__subjectId] = $this->UPDATE_REPORT_RESULT_SCORE('', '', '', '', $__originalMark, $__effort, $__recordId);
					$logContent .= $_studentId."\t".$__subjectId."\t".$firstReportColumnId."\t".$__originalGrade."\t".$__effort."\t".$__recordId."\r\n";
				}
			}
		}
		
		
		### Log the Processs in a file
		$logFolder = $intranet_root."/file/reportcard2008/".$this->GET_ACTIVE_YEAR()."/report_generate_log";
		if(!file_exists($logFolder)) {
			$SuccessAry['log']['createLogFolder'] = $lfs->folder_new($logFolder);
		}
		$logFile = $logFolder.'/UpdateEffortAsFirstReportColumnGrade_'.$parReportId.'_'.date('Ymd_His').'_'.$_SESSION['UserID'].'.txt';
		$successAry['log']['writeLog'] = $lfs->file_write($logContent, $logFile);
		
		return !in_array(false, (array)$successAry);
	}
}
?>