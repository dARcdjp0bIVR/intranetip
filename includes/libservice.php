<?php
# using: 

#################################
#
#   Date:   2020-04-22 (Tommy)
#           modified displayService(), display student in class record only
#
#	Date:	2011-03-03	YatWoon
#			update displayService(), display "Whole Year" is semester is empty
#
#################################

if (!defined("LIBSERVICE_DEFINED"))         // Preprocessor directives
{

 define("LIBSERVICE_DEFINED",true);

 class libservice extends libclass{
       var $StudentServiceID;
       var $UserID;
       var $Year;
       var $Semester;
       var $ServiceDate;
       var $ServiceName;
       var $Role;
       var $Performance;
       var $Organization;
       var $Remark;

       function libservice($StudentServiceID="")
       {
                $this->libclass();
                if ($StudentServiceID != "")
                {
                    $this->StudentServiceID = $StudentServiceID;
                    $this->retrieveRecord($this->StudentServiceID);
                }
       }
       function retrieveRecord($id)
       {
                $fields = "UserID, Year, Semester, ServiceDate, ServiceName, Role, Performance, Remark, Organization";
                $conds = "StudentServiceID = $id";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_SERVICE WHERE $conds";
                $result = $this->returnArray($sql,8);
                list(
                $this->UserID, $this->Year,$this->Semester,$this->ServiceDate,
                $this->ServiceName,$this->Role,
                $this->Performance, $this->Remark, $this->Organization) = $result[0];
                return $result;
       }
       function returnYears($studentid=null)
       {
       		if (!is_null($studentid)) {
          	$sql = "SELECT DISTINCT(Year) FROM PROFILE_STUDENT_SERVICE WHERE UserID = '$studentid' ORDER BY Year DESC";
          }
          else {
          	$sql = "SELECT DISTINCT(Year) FROM PROFILE_STUDENT_SERVICE ORDER BY Year DESC";
          }
                return $this->returnVector($sql);
       }
       function returnArchiveYears($studentid)
       {
                $sql = "SELECT DISTINCT(Year) FROM PROFILE_ARCHIVE_SERVICE WHERE UserID = '$studentid' ORDER BY Year DESC";
                return $this->returnVector($sql);
       }

       function getServiceCountByStudent($studentid)
       {
                $sql = "SELECT COUNT(StudentServiceID) FROM PROFILE_STUDENT_SERVICE
                        WHERE UserID=$studentid";
                $entry = $this->returnVector($sql);
                return $entry[0];
       }
       function getServiceListByClass($classname)
       {
                $counts = array();
                $sql = "SELECT a.UserID, COUNT(a.StudentServiceID)
                        FROM PROFILE_STUDENT_SERVICE as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        WHERE b.ClassName = '$classname'
                        GROUP BY a.UserID";
                $result = $this->returnArray($sql,2);
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($StudentID,$ServiceCount) = $result[$i];
                     $counts[$StudentID] = $ServiceCount;
                }
                $studentList = $this->getStudentNameListByClassName($classname, "1");
                //$returnResult = array();
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($StudentID, $StudentName, $classNumber) = $studentList[$i];
                     $returnResult[$i][0] = $StudentID;
                     $returnResult[$i][1] = $StudentName;
                     $returnResult[$i][2] = $classNumber;
                     $returnResult[$i][3] = $counts[$StudentID]+0;
                }
                return $returnResult;
                /*
                $studentList = $this->getClassStudentNameList($classid);
                if (sizeof($studentList)==0) return array();
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($id, $name, $classnumber) = $studentList[$i];
                     $temp = $this->getServiceCountByStudent($id);
                     $result[] = array($id,$name,$classnumber,$temp);
                }
                return $result;
                */
       }
       function getServiceByStudent($studentid, $year,$sem="")
       {
                $conds = ($year != ""? " AND Year = '$year'":"");
                $conds .= ($sem != ""? " AND Semester = '$sem'":"");
                $sql = "SELECT Year, Semester, IF (ServiceDate IS NULL,'-',ServiceDate),ServiceName, Role, Performance
                               FROM PROFILE_STUDENT_SERVICE WHERE UserID = $studentid $conds
                               ORDER BY Year DESC, Semester, ServiceName";
                return $this->returnArray($sql,6);
       }
       function displayService($studentid, $year="", $student_class=array())
       {
                global $image_path, $intranet_session_language;
                global $i_ServiceDate, $Lang;
                global $i_ServiceDate,$i_ServiceYear,$i_ServiceSemester,$i_ServiceDate,$i_ServiceName,$i_ServiceRole,$i_ServicePerformance;
                global $i_ServiceNoRecord;
                $result = $this->getServiceByStudent($studentid, $year);

                $x = "
                  <table width='100%' border='0' cellpadding='4' cellspacing='0'>
                    <tr>
                      <td width='70' align='left' class='tablebluetop tabletopnolink'>$i_ServiceYear</td>
                      <td width='110' align='left' class='tablebluetop tabletopnolink'>$i_ServiceSemester</td>
                      <td width='90' align='left' class='tablebluetop tabletopnolink'>$i_ServiceDate</td>
                      <td width='124' align='left' class='tablebluetop tabletopnolink'>$i_ServiceName</td>
                      <td width='100' align='left' class='tablebluetop tabletopnolink'>$i_ServiceRole</td>
                      <td width='100' align='left' class='tablebluetop tabletopnolink'>$i_ServicePerformance</td>
                    </tr>\n";
                
                $class = array();
                for($i = 0; $i < sizeof($student_class); $i++){
                    $class[] = $student_class[$i][0];
                }
                
                $in_class = array();
                for($i = 0; $i < sizeof($result); $i++){
                    if(in_array($result[$i]["Year"], $class))
                        $in_class[] = $result[$i];
                }
                
                for ($i=0; $i<sizeof($in_class); $i++)
                {
                    list($year,$sem,$sDate,$service,$role,$performance) = $in_class[$i];
                     $display_sem = $sem ?$sem : $Lang['General']['WholeYear'];
                     $x .= "
                      <tr class='tablebluerow". ( $i%2 + 1 )."'>
                              <td align='left' class='tabletext'>$year</td>
                              <td align='left' class='tabletext'>$display_sem</td>
                              <td align='left' class='tabletext'>$sDate</td>
                              <td align='left' class='tabletext'>$service</td>
                              <td align='left' class='tabletext'>$role&nbsp;</td>
                              <td align='left' class='tabletext'>$performance&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($result)==0)
                {
                    $x .= "<tr><td colspan='6' align='center' class='tablebluerow2 tabletext'>$i_ServiceNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }

       function displayServiceAdmin($studentid, $year="",$sem="")
       {
                global $image_path, $intranet_session_language;
                global $i_ServiceDate,$i_ServiceYear,$i_ServiceSemester,$i_ServiceDate,$i_ServiceName,$i_ServiceRole,$i_ServicePerformance;
                global $i_ServiceNoRecord;
                $result = $this->getServiceByStudent($studentid, $year,$sem);
                $x = "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_ServiceYear</td>
                      <td width=110 align=center class=tableTitle_new>$i_ServiceSemester</td>
                      <td width=90 align=center class=tableTitle_new>$i_ServiceDate</td>
                      <td width=110 align=center class=tableTitle_new>$i_ServiceName</td>
                      <td width=90 align=center class=tableTitle_new>$i_ServiceRole</td>
                      <td width=90 align=center class=tableTitle_new>$i_ServicePerformance</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list($year,$sem,$sDate,$service,$role,$performance) = $result[$i];
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$sDate</td>
                              <td align=center>$service</td>
                              <td align=center>$role&nbsp;</td>
                              <td align=center>$performance&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($result)==0)
                {
                    $x .= "<tr><td colspan=6 align=center>$i_ServiceNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }

        // ++++++ for archive student ++++++ \\
       function displayArchiveServiceAdmin($studentid, $year="",$sem="")
       {
                global $image_path, $intranet_session_language;
                global $i_ServiceDate,$i_ServiceYear,$i_ServiceSemester,$i_ServiceDate,$i_ServiceName,$i_ServiceRole,$i_ServicePerformance;
                global $i_ServiceNoRecord;
                $result = $this->getArchiveServiceByStudent($studentid, $year,$sem);
                $x = "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_ServiceYear</td>
                      <td width=110 align=center class=tableTitle_new>$i_ServiceSemester</td>
                      <td width=90 align=center class=tableTitle_new>$i_ServiceDate</td>
                      <td width=110 align=center class=tableTitle_new>$i_ServiceName</td>
                      <td width=90 align=center class=tableTitle_new>$i_ServiceRole</td>
                      <td width=90 align=center class=tableTitle_new>$i_ServicePerformance</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list($year,$sem,$sDate,$service,$role,$performance) = $result[$i];
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$sDate</td>
                              <td align=center>$service</td>
                              <td align=center>$role&nbsp;</td>
                              <td align=center>$performance&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($result)==0)
                {
                    $x .= "<tr><td colspan=6 align=center>$i_ServiceNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }
       function getArchiveServiceByStudent($studentid, $year,$sem="")
       {
                $conds = ($year != ""? " AND Year = '$year'":"");
                $conds .= ($sem != ""? " AND Semester = '$sem'":"");
                $sql = "SELECT Year, Semester, IF (RecordDate IS NULL,'-',RecordDate),ServiceName, Role, Performance
                               FROM PROFILE_ARCHIVE_SERVICE WHERE UserID = $studentid $conds
                               ORDER BY Year DESC, Semester, ServiceName";
                return $this->returnArray($sql,6);
       }

       // +++++ end of archive student +++++ \\
 }


} // End of directives
?>
