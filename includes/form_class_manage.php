<?
// Using :

/********************** Change Log ***********************/
#	Date	:	2020-07-22 (Philips) [#L188901]
#				modified Get_Student_By_Class(), add $ActiveOnly parameter
#
#   Date    :   2019-10-03 [Cameron]
#               remove $plugin['eSchoolBusBuilding'] as it's temp solution, just use $plugin['eSchoolBus'] to retrieve BuildingID
#
#   Date    :   2019-09-09 (Bill)   [2019-0905-1532-33207]
#               modified Get_Academic_Year_Info_By_YearName(), to cater search problem of year short name
#               e.g. 2020 return 2020-2021, 2000-2001, 2001-2002, .....
#
#   Date    :   2018-11-26 [Cameron]
#               - add function Get_Class_Building_Info()
#               - fix: retrieve BuildingID only when $plugin['eSchoolBus'] && $plugin['eSchoolBusBuilding'] are set in Get_Year_Class_Info()
#
#   Date    :   2018-11-16 [Cameron] 
#               - retrieve BuildingID in Get_Year_Class_Info() for eSchoolBus
#               - add parameter $BuildingID to Edit_Class(), Create_Class() for eSchoolBus
#
#   Date    :   2018-08-29 [Bill]   [2017-1207-0956-53277]
#               modified Get_Class_List(), Get_Year_Class_Info(), Create_Class(), Create_ClassOnly(), Edit_Class(), Edit_ClassOnly(), to support curriculum
#
#	Date	:	2018-05-08 [Tiffany]
#				modified Get_Class_Group_Info(), added AttendanceType.
#
#	Date	:	2016-11-04 [Carlos]
#				modified Get_Non_Teaching_Staff_List(), added optional parameter $OrderByFields="" for custom sorting.
#
#	Date	:	2016-05-13 [Cameron]
#				replace if(trim($YearID)!='') by if($YearID) in Get_Active_Student_List(), also do this for $YearClassID
#
#	Date	:	2016-01-19 [Carlos]
#				modified Create_ClassOnly(), added parameters $CopyClassStudents=0 and $FromYearClassID=0 to control whether copy class students.
#
#	Date	:	2015-12-18 [Kenneth]
#				modified Get_Class_StudentList() to return userLogin
#
#	Date	:	2014-11-10 [Pun]
#				Fixed Get_Class_List_By_Academic_Year() doesn't work with $YearClassIDArr
#
#	Date	:	2014-10-24 [Ivan] [ip.2.5.5.10.1]
#				modified Get_Class_Teacher_Class() to return GroupID also
#
#	Date	:	2014-09-22 [Carlos]
#				added Get_Academic_Year_And_Year_Term_By_Date($TargetDate) to find academic year and year term by a target date
#
#	Date	: 	2014-01-10 [Ivan] [Case#2014-0108-1109-48066]
#				modified Reset_Class_Numbers(), update class last modified time for edit class number action
#
#	Date 	: 	2013-03-11 [Rita]
#				added Check_Class_AcademicYear_RecordStatus()
#
#	Date	:	2012-12-21 [Ivan]
#				added Get_Term_Number() in class academic_year_term
#
#	Date	:	2012-09-26 [YatWoon]
#				updated Get_Active_Student_List(), order by Year and YearClass "Sequence" rather than ClassName+ClassNumber
# 
#	Date	: 	2012-08-01 [Ivan]
#				updated function Edit_Class(), Reset_Class_Numbers() to syn student info to library system
#
#	Date	:	2011-11-09 [YatWoon]
#				update Get_Class_List(), add teacher name ordering
#
#	Date	:	2011-10-11 [YatWoon]
#				update Get_Class_StudentList(), add parameter $WithoutStyle
#
#	Date	:	2011-09-08 [Marcus]
#				updated Get_Active_Student_List(), reduce return value to save memory
#
#	Date	:	2011-09-08 [Henry Chow]
#				updated Get_Teaching_Staff_List(), add order by "Name" <== roll back on 20110908 by Henry Chow (order by English Name)
#
#	Date	:	2011-09-05 [Ivan]
#				updated Get_Class_List_By_Academic_Year() added para $YearClassIDArr
#
#	Date	:	2011-08-29 [Henry Chow]
#				update Get_Student_Class_Info_In_AcademicYear, Add value STRN & HKID
#
#	Date	:	2011-03-04 [Marcus]
#				update Get_Student_By_Class, add return value YearClassID
#
#	Date	:	2011-02-14 [Ivan]
#				update Get_Year_Info, add FormStageID to support Form Stage Customization
#				*** PLEASE UPLOAD SCHEMA UPDATE FILE IF YOU WANT TO UPLOAD THIS FILE TO OTHER SITES ***
#
#	Date	:	2011-01-26 [Marcus]
#				update Get_All_Classes, add Order By Sequence
#
#	Date	:	2011-01-04 [Marcus]
#				update Get_Student_Class_Info_In_AcademicYear, Add value YearClassID
#
#	Date	:	2010-12-29 [Marcus]
#				update Get_Class_List_By_Academic_Year, Allow return class of multiple YearID
#
#	Date	:	2010-12-14 [YatWoon]
#				update Create_Class(), Edit_Class(), Create_ClassOnly(), store/edit class title (chinese) also
#
#	Date	:	2010-12-13 [Marcus]
#				modified  Get_All_Classes(), add return value YearName
#
#	Date	:	2010-12-2 [Marcus]
#				add  Get_Active_Student_List()
#
#	Date	:	2010-11-22 [YatWoon]
#				update Get_Form_List(), new parameter "AcademicYearID"
#
#	Date	: 	2010-11-08 [Ivan]
#	Details	:	added Subject Group Lock Logic 	
#
#	Date	: 	2010-09-29 [Marcus]
#	Details	:	modified Edit_Class(), when edit class, check user exist in the class group, if not, add user to the class group. 	
#
#	Date	: 	2010-09-16 [Ivan]
#	Details	:	added Get_All_Student() in class Year to get all students in the Form for import class student checking	
#
#	Date	: 	2010-09-10 [Kenneth Chung]
# 	Details	:	update Add_Student_In_Class(), add flow to update class name/ number on intranet_user if is current academic year and if user already have class setting
#	
#	Date	: 	2010-08-30 [Marcus]
#	Details	:	update Make_Class_Numbers(), add 2 options for classnumber ordering
#
#	Date	: 	2010-08-30 [Marcus]
#	Details	:	modified function Add_User_To_Group(), Make_Class_Numbers(), check existence before inserting User to INTRANET_USERGROUP as multiple UNIQUE KEY cannot detect NULL value as duplicate key 
#
#	Date	: 	2010-08-26 [Ivan]
#	Details	:	added function Is_FormName_Exist()
#
#	Date	: 	2010-08-12 [Ivan]
#	Details	:	modified Update_Class_History() - ignore the class history update if the class is from future academic year
#
#	Date	: 	2010-08-12 [Marcus]
#	Details	:	added Check_User_Existence_In_Academic_Year, check whether an user is active
#				added Retrieve_Academic_Year_Active_User_List, get Active UserList of academic year 
#				
#	Date	: 	2010-08-05 [Ivan]
#	Details	:	modified function Reset_Class_Numbers, Make_Class_Numbers and Clear_Class_Number
#				update ClassName ClassNumber in INTRANET_USER only if editing Current Academic Year Classes
#
#	Date	:	2010-07-12 [Marcus]
#	Detail	:	modified Get_All_Year_List, add Param $YearType, $exceptAcademicYearArr
#				$YearType == 1, Current and Future Year 
#				$YearType == 2, Current and Past Year
#				$YearType == 0 (default), All Year
#				$exceptAcademicYearArr, Array of Academic Year excluded. 
#
#	Date	:	2010-07-12 [Ivan]
#	Detail	:	added Class History logic in functions Create_Class(), Edit_Class(), Reset_Class_Numbers() and Delete_Class() 
#
#	Date	:	2010-06-04 [Yuen]
#	Detail	:	do not include icalendar.php in the very beginning to save memory;
#				instead, include inside the function when necessary
#
#	Date	:	2010-05-06 [YatWoon]
#	Detail	:	"wong siu chi" case, fixed: student cannot access eDiscipline (missing to check the normal Student permission, only check with AdminGroup)
#
#	Date	:	2010-03-23 (Fai)
#	Detail	:	IP25 Support with standalone iPortfolio $stand_alone['iPortfolio']
#
#	Date	:	2010-02-26 (Marcus)
#	Detail	:	add $_SESSION['SSV_EMAIL_LOGIN'],$_SESSION['SSV_EMAIL_PASSWORD'],$_SESSION['SSV_LOGIN_EMAIL]
#
#	Date	:	2009-12-15 (Henry)
#	Detail	:	move the $sys_custom['wscss_disciplinev12_student_access_cust'] from the bototm to eDisciplinev12 access right checking
#
/******************* End Of Change Log *******************/

include_once('libdb.php');
include_once('lib.php');
//include_once("icalendar.php"); 
//include_once("libeclass40.php");

// means Form in IP25
class Year extends libdb {
	var $YearID;
	var $YearName;
	var $WEBSAMSCode;
	var $Sequence;
	var $DateInput;
	var $InputBy;
	var $DateModified;
	var $ModifyBy;
	var $FormStageID;
	
	function Year($YearID="") {
		parent::libdb();
		if ($YearID != "") {
			$this->YearID = $YearID;
			$this->Get_Year_Info();
		}
	}
	
	function Get_Year_Info() {
		$sql = 'select 
							YearID, 
							YearName, 
							WEBSAMSCode,
							Sequence, 
							DateInput,
							InputBy, 
							DateModified,
							ModifyBy,
							RecordStatus,
							FormStageID
						From 
							YEAR 
						where 
							YearID = \''.$this->YearID.'\'';
		$Result = $this->returnArray($sql);
		
		$this->YearName = $Result[0]['YearName'];
		$this->WEBSAMSCode = $Result[0]['WEBSAMSCode'];
		$this->Sequence = $Result[0]['Sequence'];
		$this->DateInput = $Result[0]['DateInput'];
		$this->InputBy = $Result[0]['InputBy'];
		$this->RecordStatus = $Result[0]['RecordStatus'];
		$this->FormStageID = $Result[0]['FormStageID'];
		//$this->DateModified = $Result[0]['DateModified'];
		//$this->ModifyBy = $Result[0]['ModifyBy'];
	}
	
	function Get_Year_Name()
	{
		return $this->YearName;
	}
	
	function Update_Year() {
		$sql = 'update YEAR set 
							YearName = \''.$this->Get_Safe_Sql_Query($this->YearName).'\', 
							WEBSAMSCode = \''.$this->Get_Safe_Sql_Query($this->WEBSAMSCode).'\',
							RecordStatus = \''.$this->Get_Safe_Sql_Query($this->RecordStatus).'\',
							FormStageID = \''.$this->Get_Safe_Sql_Query($this->FormStageID).'\',
							ModifyBy = \''.$_SESSION['UserID'].'\' 
						Where 
							YearID = \''.$this->YearID.'\'';
		//echo $sql; die;
		return $this->db_db_query($sql);
	}
	
	function Get_All_Year_List($RecordStatus=1)
	{
		$cond_RecordStatus = '';
		if ($RecordStatus !== '')
			$cond_RecordStatus = " And RecordStatus = '".$RecordStatus."' ";
		
		$sql = 'select 
							*
						From 
							YEAR 
						Where
							1
							'.$cond_RecordStatus.'
						Order By 
							Sequence';
		
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function returnYearIDbyYearName($YearName)
	{
		$sql = 'select 
					YearID
				From 
					YEAR 
				where 
					YearName = \''.$this->Get_Safe_Sql_Query($YearName).'\'';
		$Result = $this->returnVector($sql);
		return $Result[0];
	}
	
	function Get_All_Classes($CheckClassTeacher=0, $AcademicYearID='')
	{
		if ($AcademicYearID=='')
			$AcademicYearID = Get_Current_Academic_Year_ID();
		
		if ($AcademicYearID==-1)
		{
			$sql_academic_year = "yc.AcademicYearID>0";
		} else
		{
			$sql_academic_year = "yc.AcademicYearID = '".$AcademicYearID."'";
		}
		
		$joinTable_ClassTeacher = '';
		$cond_ClassTeacher = '';
		if ($CheckClassTeacher==1)
		{
			$joinTable_ClassTeacher = "	Left Outer Join
										YEAR_CLASS_TEACHER as yct
										On (yc.YearClassID = yct.YearClassID)
										";
			$cond_ClassTeacher = " And yct.UserID = '".$_SESSION['UserID']."' ";
		}
		
		$cond_Year = '';
		if ($this->YearID != '')
			$cond_Year = " And y.YearID = '".$this->YearID."' ";
		
		$sql = "Select
						yc.YearClassID, yc.ClassTitleEN, yc.ClassTitleB5, yc.GroupID, yc.YearID, y.YearName
				From
						YEAR as y
						Inner Join
						YEAR_CLASS as yc
						On (y.YearID = yc.YearID)
						$joinTable_ClassTeacher
				Where
						$sql_academic_year
						$cond_Year
						$cond_ClassTeacher
				ORDER BY
					y.Sequence, yc.Sequence
				";

		return $this->returnArray($sql);
	}
	
	function Get_All_Student($AcademicYearID='')
	{
		if ($AcademicYearID=='')
			$AcademicYearID = Get_Current_Academic_Year_ID();
			
		$ClassNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
		$ArchiveSymbol = '*';
		$NameField = getNameFieldByLang('u.');
		$ArchiveNameField = getNameFieldByLang2('au.');
			
		$sql = "Select
						ycu.UserID, 
						ycu.YearClassID, 
						ycu.ClassNumber,
						CASE 
							WHEN au.UserID IS NOT NULL then CONCAT('<span class=\"tabletextrequire\">".$ArchiveSymbol."</span>', ".$ArchiveNameField.") 
							WHEN u.RecordStatus = 3  THEN CONCAT('<span class=\"tabletextrequire\">".$ArchiveSymbol."</span>', ".$NameField.") 
							ELSE $NameField
						END as StudentName, 
						$ClassNameField as ClassName
				From
						YEAR_CLASS_USER as ycu
						Inner Join
						YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
						Inner Join
						YEAR as y On (yc.YearID = y.YearID)
						LEFT JOIN 
						INTRANET_USER as u ON (ycu.UserID = u.UserID)
						LEFT JOIN 
						INTRANET_ARCHIVE_USER as au ON (ycu.UserID = au.UserID) 
				Where
						yc.YearID = '".$this->YearID."'
						And
						yc.AcademicYearID = '".$AcademicYearID."' 
				Order By
						y.Sequence, yc.Sequence, ycu.ClassNumber
				";
		$ResultArr = $this->returnArray($sql);
		return $ResultArr;
	}
	function Get_Year_Info_By_Name($name){
		$sql = "SELECT * FROM YEAR WHERE YearName = '$name' ";
		$ResultArr = $this->returnResultSet($sql);
		return $ResultArr[0];
	}
}

class year_class extends Year {
	var $YearClassID;
	var $AcademicYearID;
	var $YearID;
	var $ClassID;
	var $ClassTitleEN;
	var $ClassTitleB5;
	var $Sequence;
	var $DateInput;
	var $InputBy;
	var $DateModified;
	var $ModifyBy;
	var $InputByName;
	var $ModifiedByName;
	var $ClassWEBSAMSCode;
	var $CurriculumID;
	var $BuildingID;
	
	// INTRANET_GROUP ID
	var $GroupID;
	var $ClassGroupID;
	
	var $ClassTeacherList = array();
	var $ClassStudentList = array();
	
	function year_class($YearClassID="",$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=false,$GetLockedSGArr=false) {
		parent:: Year();
		
		if ($YearClassID != "") {
			$this->Get_Year_Class_Info($YearClassID);
			
			if ($GetYearDetail) {
				$this->Get_Year_Info($this->YearID);
			}
			
			if ($GetClassTeacherList) {
				$this->Get_Class_Teacher_List();
			}
			
			if ($GetClassStudentList) {
				$this->Get_Class_StudentList();
			}
			
			if ($GetLockedSGArr) {
				$this->Get_Class_Locked_SubjectGroup();
			}
		}
	}
	
	
	function Get_All_Class_List($AcademicYearID, $ParLang="")
	{
		global $intranet_session_language;

		if ($ParLang=="")
		{
			$ParLang = $intranet_session_language;
		}
			
		
		if ($ParLang == 'en')
				$targetNameField = 'ClassTitleEN';
			else
				$targetNameField = 'ClassTitleB5';
				
		$sql = "select 
							yc.YearClassID, yc.AcademicYearID, yc.YearID, yc.ClassID, yc.{$targetNameField} AS ClassTitle, yc.WEBSAMSCode
						From 
							YEAR_CLASS AS yc LEFT JOIN YEAR as y ON y.YearID=yc.YearID
						WHERE 
							yc.AcademicYearID='{$AcademicYearID}' 
						Order By 
							y.Sequence, yc.Sequence";
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function Get_Year_Class_Info($YearClassID) {
	    global $plugin;
	    
		$InputByNameField = getNameFieldByLang('u.');
		$ModifiedByNamefield = getNameFieldByLang('u1.');
		if ($plugin['eSchoolBus']) {
		    $additionalField = ", yc.BuildingID ";
		}
		else {
		    $additionalField = '';
		}
		$sql= 'Select 
						yc.YearClassID, 
						yc.AcademicYearID, 
						yc.YearID, 
						yc.ClassID, 
						yc.ClassTitleEN,
						yc.ClassTitleB5,
						yc.Sequence,
						yc.DateInput,
						yc.InputBy,
						yc.DateModified,
						yc.ModifyBy, 
						'.$InputByNameField.' as InputByName, 
						'.$ModifiedByNamefield.' as ModifiedByName, 
						yc.GroupID, 
						yc.WEBSAMSCode,
						yc.ClassGroupID,
                        yc.CurriculumID'.$additionalField.'
					From 
						YEAR_CLASS yc
						LEFT JOIN 
						INTRANET_USER u 
						on yc.ModifyBy = u.UserID 
						LEFT JOIN 
						INTRANET_USER u1 
						on yc.InputBy = u1.UserID 
					where 
						yc.YearClassID = \''.$YearClassID.'\'';
		$Result = $this->returnArray($sql);
		
		$this->YearClassID = $Result[0]['YearClassID'];
		$this->AcademicYearID = $Result[0]['AcademicYearID'];
		$this->YearID = $Result[0]['YearID'];
		$this->ClassID = $Result[0]['ClassID'];
		$this->ClassTitleEN = $Result[0]['ClassTitleEN'];
		$this->ClassTitleB5 = $Result[0]['ClassTitleB5'];
		$this->Sequence = $Result[0]['Sequence'];
		$this->DateInput = $Result[0]['DateInput'];
		$this->InputBy = $Result[0]['InputBy'];
		$this->DateModified = $Result[0]['DateModified'];
		$this->ModifyBy = $Result[0]['ModifyBy'];
		$this->InputByName = $Result[0]['InputByName'];
		$this->ModifiedByName = $Result[0]['ModifiedByName'];
		$this->GroupID = $Result[0]['GroupID'];
		$this->ClassWEBSAMSCode = $Result[0]['WEBSAMSCode'];
		$this->ClassGroupID = $Result[0]['ClassGroupID'];
		$this->CurriculumID = $Result[0]['CurriculumID'];
		
		if ($plugin['eSchoolBus']) {
		  $this->BuildingID = $Result[0]['BuildingID'];
		}
	}
	
	function Get_Class_Name($ParLang='') {
		if ($_SESSION['intranet_session_language'] == 'en' || $ParLang == 'en') {
			return (trim($this->ClassTitleEN) == "")? $this->ClassTitleB5:$this->ClassTitleEN;
		}
		else {
			return (trim($this->ClassTitleB5) == "")? $this->ClassTitleEN:$this->ClassTitleB5;
		}
	}
	
	function Get_Class_Teacher_List($NeedStyle=1) {
		$NameField = getNameFieldByLang('u.');
		$ArchiveNameField = getNameFieldByLang2('au.');
		
		if ($NeedStyle == 1)
			$StarString = '<font style="color:red;">*</font>';
		else
			$StarString = '*';
		
		$sql = 'select 
							yct.UserID, 
							CASE 
								WHEN au.UserID IS NOT NULL then CONCAT(\''.$StarString.'\','.$ArchiveNameField.') 
								ELSE '.$NameField.' 
							END as TeacherName 
						from 
							YEAR_CLASS_TEACHER as yct 
							LEFT JOIN 
							INTRANET_USER as u ON (yct.UserID = u.UserID)
							LEFT JOIN 
							INTRANET_ARCHIVE_USER as au ON (yct.UserID = au.UserID) 
						where 
							YearClassID = \''.$this->YearClassID.'\'';
		$this->ClassTeacherList = $this->returnArray($sql);
	}
	
	function Get_Class_StudentList($ArchiveSymbol='', $WithoutStyle=0) {
		$NameField = getNameFieldByLang('u.');
		$ArchiveNameField = getNameFieldByLang2('au.');
		
		if ($ArchiveSymbol == '')
			$ArchiveSymbol = '*';
		
		$SpanClass = $WithoutStyle ? $ArchiveSymbol : "<span class=\'tabletextrequire\'>". $ArchiveSymbol ."</span>";
		
		$sql = 'select 
							ycu.UserID, 
							ycu.ClassNumber,
							CASE 
								WHEN au.UserID IS NOT NULL then CONCAT(\''. $SpanClass .'\','.$ArchiveNameField.') 
								WHEN u.RecordStatus = 3  THEN CONCAT(\''. $SpanClass .'\','.$NameField.') 
								ELSE '.$NameField.' 
							END as StudentName, 
							CASE 
								WHEN au.UserID IS NOT NULL then \'1\' 
								ELSE \'0\' 
							END as ArchiveUser,
							\''.$this->Get_Class_Name().'\' as ClassName, u.UserLogin
						from 
							YEAR_CLASS_USER as ycu
							LEFT JOIN 
							INTRANET_USER as u ON (ycu.UserID = u.UserID)
							LEFT JOIN 
							INTRANET_ARCHIVE_USER as au ON (ycu.UserID = au.UserID) 
						where 
							YearClassID = \''.$this->YearClassID.'\'
						order by 
							ClassNumber';
		// echo '<pre>'.$sql.'</pre>'; 
		$this->ClassStudentList = $this->returnArray($sql);
	}
	
	function Get_Class_Locked_SubjectGroup()
	{
		$sql = "Select 
						SubjectGroupID
				From
						SUBJECT_TERM_CLASS
				Where
						LockedYearClassID = '".$this->YearClassID."'
				"; 
		$this->LockedSubjectGroupList = $this->returnArray($sql);
	}
	
	function Get_All_Class_Teacher_List() {
		$sql = 'select 
						DISTINCT(UserID)
				from 
						YEAR_CLASS_TEACHER ';
		$returnArr = $this->returnVector($sql);
		
		return $returnArr;
	}
	
	function Get_All_Class_Student_List() {
		
		$sql = 'select 
						DISTINCT(ycu.UserID)
				from 
						YEAR_CLASS_USER as ycu
						INNER JOIN
						YEAR_CLASS as yc ON ycu.YearClassID = yc.YearClassID
				order by 
						yc.Sequence, ycu.ClassNumber';
		$returnArr = $this->returnVector($sql);
		
		return $returnArr;
	}
	
	function Get_Max_ClassNumber()
	{
		$sql = "SELECT 
						Max(ClassNumber) 
				FROM 
						YEAR_CLASS_USER 
				Where 
						YearClassID = '".$this->YearClassID."'
				";
		$resultSet = $this->returnVector($sql);
		
		return $resultSet[0];
	}
	
	function Get_Active_User_List() {
		$sql = 'select 
							ycu.UserID 
						from 
							YEAR_CLASS_USER as ycu
							INNER JOIN 
							INTRANET_USER as u ON YearClassID = \''.$this->YearClassID.'\' and ycu.UserID = u.UserID';
		return $this->returnVector($sql);
	}
	
	function Get_YearClass_By_GroupID($GroupID)
	{
		$sql = "Select * FROM YEAR_CLASS Where GroupID = '".$GroupID."'";
		$YearClassInfoArr = $this->returnArray($sql);
		
		return $YearClassInfoArr;
	}
	
	function Get_Class_Group_Info($YearClassID='')
	{
		if(empty($YearClassID))
			$YearClassID = $this->YearClassID;
			
		$sql = "
			SELECT 
				b.ClassGroupID, 
				b.Code, 
				b.TitleEn, 
				b.TitleCh, 
				b.DisplayOrder, 
				b.RecordStatus,
			    b.AttendanceType
			FROM 
				YEAR_CLASS a 
				LEFT JOIN YEAR_CLASS_GROUP b ON a.ClassGroupID = b.ClassGroupID 
			WHERE 
				a.YearClassID = '$YearClassID'  
		";
		
		$ClassGroupInfo = $this->returnArray($sql);
		return $ClassGroupInfo;
		
	}
	
	function Get_Class_Building_Info($yearClassID='')
	{
	    if(empty($yearClassID)) {
	        $yearClassID = $this->YearClassID;
	    }
	    $buildingName = Get_Lang_Selection('NameChi', 'NameEng');
	    $sql = "SELECT      
                            b.BuildingID, b.".$buildingName." AS BuildingName
                FROM
                            YEAR_CLASS yc
                            
                LEFT JOIN
                            INVENTORY_LOCATION_BUILDING b ON b.BuildingID=yc.BuildingID
                WHERE
                            yc.YearClassID='".$yearClassID."'";
	    
	    $classBuildingInfo = $this->returnResultSet($sql);    
	    return $classBuildingInfo;
	}
	
	/*function Get_Existing_Teacher_List(){
		global $eclass_db;
		$sql = "select s.course_id from YEAR_CLASS as s
				inner join {$eclass_db}.course as c on
				s.course_id = c.course_id and 
				s.course_id is not null
				where 
				s.YearClassID = ".$this->YearClassID;
		$result = $this->returnVector($sql);
		$course_id = $result[0];		
		$lo = new libeclass($course_id);
		$ec_db_name = $lo->db_prefix."c$course_id";
		$sql = "SELECT user_id, LTRIM(CONCAT(lastname, ' ', firstname)) as t_name, user_email ";
		$sql .= "FROM ".$ec_db_name.".usermaster ";
		$sql .= "WHERE memberType='T' AND (status IS NULL OR status='') ";
		$sql .= "ORDER BY t_name ";
		$row_e = $lo->returnArray($sql, 3); //echo $sql;

		$sql = "SELECT UserID, EnglishName, ChineseName, UserEmail ";
		$sql .= "FROM INTRANET_USER ";
		$sql .= "WHERE RecordType = 1 ";
		$sql .= "ORDER BY EnglishName ";
		$row_i = $this->returnArray($sql, 4);
		return Array('in_class'=>$row_e,'out_class'=>$row_i);
	}
	
	function has_eclass(){
		global $eclass_db; 
		$sql ="select y.course_id from YEAR_CLASS as y inner join 
			{$eclass_db}.course as c on 
				c.course_id = y.course_id and
				y.course_id <> 0 and y.course_id is not null
				where
				y.YearClassID = ".$this->YearClassID."
			"; //echo $sql;
		$result = $this->returnVector($sql);
		return !empty($result[0]);
	}*/
	
}

class academic_year extends libdb {
	var $AcademicYearID;
	var $YearNameEN;
	var $YearNameB5;
	var $Sequence;
	var $DateInput;
	var $InputBy;
	var $DateModified;
	var $ModifyBy;
	var $IsPrevious;
	var $AcademicYearStart;
	var $AcademicYearEnd;
	var $ActiveUserList; //list of student with YEAR_CLASS.YearClassID and YEAR_CLASS_USER.ClassNumber 
	
	function academic_year($AcademicYearID="") {
		parent:: libdb();
		
		if ($AcademicYearID != "") {
			$this->Get_Academic_Year_Info($AcademicYearID);
		}
	}
	
	function Get_Academic_Year_Info($AcademicYearID) {
		$sql = 'select 
							a.AcademicYearID, 
							a.YearNameEN, 
							a.YearNameB5, 
							a.Sequence, 
							a.DateInput, 
							a.InputBy,
							a.DateModified,
							a.ModifyBy, 
							MIN(a.PreviousSchoolYear) as PreviousSchoolYear
						from 
							(select 
								ay.AcademicYearID, 
								ay.YearNameEN, 
								ay.YearNameB5, 
								ay.Sequence, 
								ay.DateInput, 
								ay.InputBy,
								ay.DateModified,
								ay.ModifyBy, 
								CASE 
									WHEN NOW() > ayt.TermEnd THEN \'1\'
						      		ELSE \'0\'
						    	END as PreviousSchoolYear 
							From 
								ACADEMIC_YEAR as ay
								left join 
								ACADEMIC_YEAR_TERM as ayt 
								on ay.AcademicYearID = ayt.AcademicYearID
							where 
								ay.AcademicYearID = \''.$AcademicYearID.'\') as a
						Group by 
							a.AcademicYearID, a.YearNameEN, a.YearNameB5, a.Sequence, a.DateInput, 
							a.InputBy, a.DateModified, a.ModifyBy';
		//echo $sql;
		$Result = $this->returnArray($sql);
		
		$this->AcademicYearID = $Result[0]['AcademicYearID'];
		$this->YearNameEN = $Result[0]['YearNameEN'];
		$this->YearNameB5 = $Result[0]['YearNameB5'];
		$this->Sequence = $Result[0]['Sequence'];
		$this->DateInput = $Result[0]['DateInput'];
		$this->InputBy = $Result[0]['InputBy'];
		$this->ModifiedDate = $Result[0]['ModifiedDate'];
		$this->ModifyBy = $Result[0]['ModifyBy'];
		$this->IsPrevious = $Result[0]['PreviousSchoolYear'];
		
		# Get Term Start and End Info
		$libFCM = new form_class_manage();
		$YearInfoArr = $libFCM->Get_Academic_Year_List($AcademicYearID);
		$this->AcademicYearStart = $YearInfoArr[0]['AcademicYearStart'];
		$this->AcademicYearEnd = $YearInfoArr[0]['AcademicYearEnd'];
	}
	
	function Get_Academic_Year_Name() {
		return Get_Lang_Selection($this->YearNameB5,$this->YearNameEN);
	}
	
	// if search '2008', find the year which name starts with '2008' or '08'
	function Get_Academic_Year_Info_By_YearName($YearName='', $ParLang='en', $BothLang=0)
	{
		// 2008 -> 08
		$yearShortForm = substr($YearName, 2, 2);
        // 2008 -> 20
        $yearCenturyForm = substr($YearName, 0, 2);
        // 08 -> 09
        $nextYearShortForm = (int)$yearShortForm;
        $nextYearShortForm = ++$nextYearShortForm;
        if($nextYearShortForm < 10) {
            $nextYearShortForm = "0$nextYearShortForm";
        }

        /*
         * 4 possible formats   [2019-0905-1532-33207]
         * 20xx
         * xx-xx
         * 20xx-xx
         * 20xx-20xx
         */
        $yearNameArr = array();
        $yearNameArr[] = "$yearCenturyForm$yearShortForm";
        $yearNameArr[] = "$yearShortForm-$nextYearShortForm";
        $yearNameArr[] = "$yearShortForm - $nextYearShortForm";
        $yearNameArr[] = "$yearCenturyForm$yearShortForm-$nextYearShortForm";
        $yearNameArr[] = "$yearCenturyForm$yearShortForm - $nextYearShortForm";
        $yearNameArr[] = "$yearCenturyForm$yearShortForm-$yearCenturyForm$nextYearShortForm";
        $yearNameArr[] = "$yearCenturyForm$yearShortForm - $yearCenturyForm$nextYearShortForm";

		if($BothLang)
		{
		    /*
			$conds = "
			YearNameEN Like '".$YearName."%' OR YearNameEN Like '".$yearShortForm."%'
			or
			YearNameB5 Like '".$YearName."%' OR YearNameB5 Like '".$yearShortForm."%'
			";
			*/
            $conds = " YearNameEN LIKE '".$YearName."%' OR YearNameB5 LIKE '".$YearName."%' ";
		}
		else
		{
			if ($ParLang == 'en') {
				$targetNameField = 'YearNameEN';
            } else {
				$targetNameField = 'YearNameB5';
            }

			// $conds = "$targetNameField Like '".$YearName."%' OR $targetNameField Like '".$yearShortForm."%'";
            $conds = "$targetNameField LIKE '".$YearName."%' ";
		}
        $conds .= " OR YearNameEN IN ('".implode("','", (array)$yearNameArr)."') ";

        $sql = "SELECT 
						*
				FROM
						ACADEMIC_YEAR
				WHERE
					    $conds
				";
		$resultSet = $this->returnArray($sql);
		
		return $resultSet;
	}
	
	# modified by marcus 20091014 , add $lang to select lang of term name
	function Get_Term_List($returnAsso=1, $ParLang='')
	{
		if($ParLang=='')
			$ParLang = $_SESSION['intranet_session_language'];
		
		if ($ParLang == 'en')
			$TermNameField = 'YearTermNameEN';
		else
			$TermNameField = 'YearTermNameB5';
			
		$yearCond = '';
		if ($this->AcademicYearID != '')
			$yearCond = " WHERE AcademicYearID = '".$this->AcademicYearID."' ";
			
		$sql = "SELECT 
						YearTermID, $TermNameField as TermName, TermStart, TermEnd
				FROM 
						ACADEMIC_YEAR_TERM 
				$yearCond
				ORDER BY 
						TermStart
				";
		$resultArr = $this->returnArray($sql);
		
		if ($returnAsso)
			$resultArr = build_assoc_array($resultArr, 'null');
			
		return $resultArr;
	}
	
	function Has_Academic_Year_Past()
	{
		/*
		$YearTermArr = $this->Get_Term_List($returnAsso=0);
		$numOfTerm = count($YearTermArr);
		
		$TermPastArr = array();
		for ($i=0; $i<$numOfTerm; $i++)
		{
			$thisYearTermID = $YearTermArr[$i]['YearTermID'];
			$ObjTerm = new academic_year_term($thisYearTermID);
			
			$TermPastArr[] = $ObjTerm->TermPassed;
		}
		
		if (in_array(false, $TermPastArr))
			return false;
		else
			return true;
		*/
		return $this->IsPrevious;
	}
	
	function Is_Future_Academic_Year()
	{
		if ($this->Has_Academic_Year_Past() == 0 && $this->AcademicYearID != Get_Current_Academic_Year_ID())
			return true;
		else
			return false;
	}
	
	# modified by marcus 20100712
	# $YearType = 1 , no past academic year, Current and Future Academic Year EXIST
	# $YearType = 2 , no future academic year, Current and Past Academic Year EXIST
	function Get_All_Year_List($ParLang="",$YearType = 0, $exceptAcademicYearIDArr = array(), $OnlyAcademicYearIDArr = array())
	{
		global $intranet_session_language;
		
// 		debug_pr($ParLang);
// 		debug_pr($YearType);
// 		debug_pr($exceptAcademicYearIDArr);
// 		debug_pr($OnlyAcademicYearIDArr);
		
		if(!$ParLang)
			$ParLang = $intranet_session_language;
				
//		if($ParLang== 'en')
//			$targetNameField = 'YearNameEN';
//		else if($ParLang== 'b5')
//			$targetNameField = 'YearNameB5';
//		$targetNameField = Get_Lang_Selection("YearNameB5","YearNameEN");	
		
		if ($ParLang == 'en')
				$targetNameField = 'YearNameEN';
			else
				$targetNameField = 'YearNameB5';
				
		/*$sql = "SELECT 
						AcademicYearID,
						$targetNameField
				FROM
						ACADEMIC_YEAR
				order by 
						Sequence
				";*/
				
		if($YearType!=0 &&  trim($YearType)!='')
		{
			$JOIN_AYT = " INNER JOIN ACADEMIC_YEAR_TERM ayt ON ay.AcademicYearID = ayt.AcademicYearID ";
			$GROUPBY = " Group By ay.AcademicYearID ";
			
			if($YearType == 1)
			{
				$COND = " AND TermEnd >= NOW() ";
			}
			else if($YearType == 2)
			{
				$COND = " AND TermStart < NOW() ";
			}
		}

		if(!empty($exceptAcademicYearIDArr))
		{
			$exceptAcademicYear = implode(",",(array)$exceptAcademicYearIDArr);
			$COND .= " AND ay.AcademicYearID NOT IN  ($exceptAcademicYear)";
		}
		
		if(!empty($OnlyAcademicYearIDArr))
		{
			$OnlyAcademicYear = implode(",",(array)$OnlyAcademicYearIDArr);
			$COND .= " AND ay.AcademicYearID IN  ($OnlyAcademicYear)";
		}
		
		$sql = "SELECT 
					ay.AcademicYearID,
					ay.".$targetNameField." as AcademicYearName
				FROM
					ACADEMIC_YEAR ay
					$JOIN_AYT
				WHERE
					1
				$COND
				$GROUPBY
				order by 
						ay.Sequence
				";				
				
		$resultSet = $this->returnArray($sql);
		
		return $resultSet;
	}
	
	function Retrieve_Academic_Year_Active_User_List()
	{
		
		if(empty($this->ActiveUserList))
		{
			
			$sql = "
				SELECT
					iu.UserID
				FROM
					INTRANET_USER iu 
					LEFT JOIN YEAR_CLASS_USER ycu ON iu.UserID = ycu.UserID
					LEFT JOIN YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->AcademicYearID."'
				WHERE
					iu.RecordStatus = 1
					AND
					(
						iu.RecordType <> 2
						OR
						(
							Trim(yc.YearClassID) <> '' 
							AND Trim(ycu.ClassNumber) <> '' 
							AND yc.YearClassID IS NOT NULL 
							AND ycu.ClassNumber IS NOT NULL 
						)
					)
			";
			$this->ActiveUserList = $this->returnVector($sql);
		}
		
		return $this->ActiveUserList;
	}
	
	function Check_User_Existence_In_Academic_Year($ParUserID)
	{
		
		return in_array($ParUserID,$this->Retrieve_Academic_Year_Active_User_List());
	}
}

class academic_year_term extends academic_year {
	var $AcademicYearID;
	var $YearTermID;
	var $TermID;
	var $YearTermNameEN;
	var $YearTermNameB5;
	var $TermStart;
	var $TermEnd;
	var $DateInput;
	var $InputBy;
	var $DateModified;
	var $ModifyBy;
	
	// flag to determin if the term has passed or not
	var $TermPassed;
	
	function academic_year_term($YearTermID="",$GetAcademicYearInfo=true) {
		parent:: academic_year();
		if ($YearTermID != "") {
			$sql = 'select 
								AcademicYearID,
								YearTermID,
								TermID,
								YearTermNameEN,
								YearTermNameB5,
								TermStart,
								TermEnd,
								DateInput,
								InputBy,
								DateModified,
								ModifyBy 
							From 
								ACADEMIC_YEAR_TERM 
							Where 
								YearTermID = \''.$YearTermID.'\'';
			$Result = $this->returnArray($sql);
			
			$this->YearTermID = $Result[0]['YearTermID'];
			$this->AcademicYearID = $Result[0]['AcademicYearID'];
			$this->TermID = $Result[0]['TermID'];
			$this->YearTermNameEN = $Result[0]['YearTermNameEN'];
			$this->YearTermNameB5 = $Result[0]['YearTermNameB5'];
			$this->TermStart = $Result[0]['TermStart'];
			$this->TermEnd = $Result[0]['TermEnd'];
			$this->DateInput = $Result[0]['DateInput'];
			$this->InputBy = $Result[0]['InputBy'];
			$this->DateModified = $Result[0]['DateModified'];
			$this->ModifyBy = $Result[0]['ModifyBy'];
			
			if ($GetAcademicYearInfo) {
				parent:: academic_year($this->AcademicYearID);
			}
			
			$TermEnd = strtotime(substr($this->TermEnd, 0, 10));	// Compare the Date only
			$this->TermPassed = ($TermEnd - strtotime(date("Y-m-d")) < 0);
		}
	}
	
	function Get_Year_Term_Name() {
		return Get_Lang_Selection($this->YearTermNameB5,$this->YearTermNameEN);
	}
	
	function Get_YearTermID_By_YearNameEn($AcademicYearID, $YearTermNameEN='')
	{
		if($YearTermNameEN)
		{
			$sql = "select 
								YearTermID
							From 
								ACADEMIC_YEAR_TERM 
							Where 
								AcademicYearID = '". $AcademicYearID. "'
								and YearTermNameEN = '". $YearTermNameEN. "'";
			$Result = $this->returnVector($sql);
			
			return $Result[0];
		}
	}
	
	function Get_YearTermID_By_Name($AcademicYearID, $TermName)
	{
		$sql = "select 
							YearTermID
						From 
							ACADEMIC_YEAR_TERM 
						Where 
							AcademicYearID = '". $AcademicYearID. "'
							and 
							(YearTermNameEN = '". $TermName. "' or YearTermNameB5 = '". $TermName. "')
			";
		$Result = $this->returnVector($sql);
		
		return $Result[0];
	}
	
	function Get_YearNameByID($YearTermID, $ParLang="en")
	{
		if ($ParLang == 'en')
			$TermNameField = 'YearTermNameEN';
		else
			$TermNameField = 'YearTermNameB5';
			
		$sql = "select 
							$TermNameField
						From 
							ACADEMIC_YEAR_TERM 
						Where 
							YearTermID = $YearTermID
			";
		$Result = $this->returnVector($sql);
		
		return $Result[0];
	}
	
	function Get_Semester_Before()
	{
		$sql = "Select
						*
				From
						ACADEMIC_YEAR_TERM
				Where
						TermEnd < '".$this->TermStart."'
				Order By
						TermEnd Desc
				";
		$pastTermArr = $this->returnArray($sql);
		
		return $pastTermArr;
	}
	
	function Get_Term_Number() {
		$sql = "Select
						YearTermID,
						YearTermNameEN,
						YearTermNameB5
				From
						ACADEMIC_YEAR_TERM
				Where
						AcademicYearID = '".$this->AcademicYearID."'
				Order By
						TermStart
				";
		$termInfoAry = $this->returnResultSet($sql);
		$numOfTerm = count($termInfoAry);
		
		$termNumber = -1;
		for ($i=0; $i<$numOfTerm; $i++) {
			$_yearTermId = $termInfoAry[$i]['YearTermID'];
			
			if ($this->YearTermID == $_yearTermId) {
				$termNumber = $i + 1;
				break;
			}
		}
		
		return $termNumber;
	}
}

class form_class_manage extends libdb {
	function form_class_manage() {
		parent::libdb();
	}
	
	function Get_Class_List_By_YearID($YearID) {
		if ($YearID != '') {
			$conds_YearID = " And yc.YearID In ('".implode("','", (array)$YearID)."') ";
		}
		
		$sql = 'Select
							yc.YearClassID, 
							yc.ClassTitleEN,
							yc.ClassTitleB5
						From
							YEAR_CLASS as yc
						Where 
							yc.AcademicYearID = \''.Get_Current_Academic_Year_ID().'\'
							'.$conds_YearID.'
						Order by 
							yc.Sequence, yc.ClassTitleEN
					 ';
		return $this->returnArray($sql);
	}
	
	function Reorder_Form_Sequence($RecordOrder) {
		$RecordOrder = explode(',',$RecordOrder);
		
		$this->Start_Trans();
		
		for ($i=0; $i< sizeof($RecordOrder); $i++) {
			$sql = 'update YEAR set 
								Sequence = \''.$i.'\' 
							Where 
								YearID = \''.$RecordOrder[$i].'\'';
			$Result['ReorderResult'.$i] = $this->db_db_query($sql);
		}
		
		if (in_array(false,$Result)) 
			$this->RollBack_Trans();
		else
			$this->Commit_Trans();
	}
	
	function Reorder_Class_Sequence($RecordOrder) {
		$RecordOrder = explode(',',$RecordOrder);
		
		$this->Start_Trans();
		
		for ($i=0; $i< sizeof($RecordOrder); $i++) {
			$sql = 'update YEAR_CLASS set 
								Sequence = \''.$i.'\' 
							Where 
								YearClassID = \''.$RecordOrder[$i].'\'';
			$Result['ReorderResult'.$i] = $this->db_db_query($sql);
		}
		
		if (in_array(false,$Result)) 
			$this->RollBack_Trans();
		else
			$this->Commit_Trans();
	}
	
	function Delete_Form($YearID) {
		$sql = 'delete from YEAR where YearID = \''.$YearID.'\'';
		
		return $this->db_db_query($sql);
	}
	
	function Create_Form($YearName,$WEBSAMSCode,$RecordStatus=1) {
		$sql = 'select max(sequence) from YEAR';
		$MaxSequence = $this->returnVector($sql);
		$NextSequence = $MaxSequence[0]+1;
		
		$sql = 'insert into YEAR (
							YearName,
							WEBSAMSCode,
							RecordStatus,
							DateInput,
							sequence,
							InputBy
							) 
						value (
							\''.$this->Get_Safe_Sql_Query($YearName).'\',
							\''.$WEBSAMSCode.'\',
							\''.$RecordStatus.'\',
							NOW(),
							\''.$NextSequence.'\',
							\''.$_SESSION['UserID'].'\'
							)';
		//echo $sql; die;
		return $this->db_db_query($sql);
	}
	
	/*
	// Get Form List 
	// EG: Form 1,Form 2
	function Get_Form_List($GetMappedClassInfo=true, $ActiveOnly=1) {
		
		$conds_RecordStatus = '';
		if ($ActiveOnly == 1)
			$conds_RecordStatus = ' And y.RecordStatus = 1 ';
		
		$sql = 'select 
							y.YearID,
							y.YearName, 
							y.Sequence, 
							y.WEBSAMSCode,
							y.RecordStatus
				';
		if ($GetMappedClassInfo) {
			$sql .= ',CASE
						    WHEN yc.YearID is NULL then \'0\'
						    ELSE \'1\'
						  END as MappedClass ';
		}
		$sql .= 'from 
							YEAR as y ';
		if ($GetMappedClassInfo) {
			$sql .= 'left outer join 
							(select YearID from 
						  YEAR_CLASS Group By YearID) as yc 
						  on y.YearID = yc.YearID ';
		}
		
		$sql .= "	Where
							1
							$conds_RecordStatus
				";
		
		$sql .= 'order by 
							Sequence, YearName';
		//echo $sql;
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	*/
	
	function Get_Form_List($GetMappedClassInfo=true, $ActiveOnly=1, $AcademicYearID='', $YearIDArr='', $WebSAMSCodeArr='') 
	{
		$conds_RecordStatus = '';
		if ($ActiveOnly == 1) {
			$conds_RecordStatus = ' And y.RecordStatus = 1 ';
		}
			
		$conds_YearID = '';
		if ($YearIDArr != '') {
			$conds_YearID = " And y.YearID In (".implode(',', (array)$YearIDArr).") ";
		}
			
		$conds_WEBSAMSCode = '';
		if ($WebSAMSCodeArr != '') {
			$conds_WEBSAMSCode = " And y.WEBSAMSCode In ('".implode("','", (array)$WebSAMSCodeArr)."') ";
		}
			
		$sql = 'select 
							distinct(y.YearID),
							y.YearName, 
							y.Sequence, 
							y.WEBSAMSCode,
							y.RecordStatus,
							y.FormStageID
				';
		if ($GetMappedClassInfo) {
			$sql .= ',CASE
						    WHEN yc.YearID is NULL then \'0\'
						    ELSE \'1\'
						  END as MappedClass ';
		}
		$sql .= 'from 
							YEAR as y ';
							
		if ($GetMappedClassInfo) {
			$sql .= 'left outer join 
							(select YearID from 
						  YEAR_CLASS Group By YearID) as yc 
						  on y.YearID = yc.YearID ';
		}
		
		if($AcademicYearID)
		{
			$sql .= 'left join YEAR_CLASS as yc1 on (y.YearID = yc1.YearID)';
		}
	
		
		$sql .= "	Where
							1
							$conds_RecordStatus
							$conds_YearID
							$conds_WEBSAMSCode
				";
		if($AcademicYearID)
		{
			$sql .= "and yc1.AcademicYearID = ". $AcademicYearID ." ";
		}
		
	
		$sql .= 'order by 
							Sequence, YearName';
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function Get_Academic_Year_List($AcademicYearIDArr='', $OrderBySequence=1, $excludeYearIDArr=array(), $NoPastYear=0, $PastAndCurrentYearOnly=0, $ExcludeCurrentYear=0, $ComparePastYearID='', $SortOrder='') {
		
		global $sys_custom;
		
		$year_conds = '';
		if ($AcademicYearIDArr!='')
			$year_conds = ' And a.AcademicYearID In ('.implode(',', (array)$AcademicYearIDArr).') ';
			
		$exclude_year_conds = '';
		if (is_array($excludeYearIDArr) && count($excludeYearIDArr) > 0)
			$exclude_year_conds = " And a.AcademicYearID Not In (".implode(',', $excludeYearIDArr).") ";
			
		if ($NoPastYear == 1)
		{
			if ($ComparePastYearID == '')
				$conds_NoPastYear = " And (NOW() < b.AcademicYearStart || (b.AcademicYearStart < NOW() And NOW() < b.AcademicYearEnd) )";
			else
			{
				$YearInfoArr = $this->Get_Academic_Year_List($ComparePastYearID);
				$YearEnd = $YearInfoArr[0]['AcademicYearEnd'];
				$conds_PastAndCurrentYearOnly = " And (b.AcademicYearStart > '$YearEnd' )";
			}
		}
						
		if ($PastAndCurrentYearOnly == 1)
		{
			if ($ComparePastYearID == '')
				$conds_PastAndCurrentYearOnly = " And (CurrentSchoolYear = 1 || NOW() > AcademicYearEnd)";
			else
			{
				$YearInfoArr = $this->Get_Academic_Year_List($ComparePastYearID);
				$YearStart = $YearInfoArr[0]['AcademicYearStart'];
				$conds_PastAndCurrentYearOnly = " And (b.AcademicYearID = '$ComparePastYearID' || b.AcademicYearEnd < '$YearStart' )";
			}
		}
			
		if ($ExcludeCurrentYear == 1)
			$conds_ExcludeCurrentYear = " And CurrentSchoolYear != 1 ";
			
			
		$order_by_fields = '';
		if ($OrderBySequence == 1)
			$order_by_fields = 'a.Sequence';
		else
		{
			$SortOrder = ($SortOrder=='')? 'Desc' : $SortOrder;
			$order_by_fields = 'a.TermStart '.$SortOrder;
		}
		
		# modified on 08-05-2017 by Frankie
		# limit to show year (Past, Current and Next)

		$showOnAcademicYear = true;
		if ($sys_custom['LivingHomeopathy']) {
			$showOnAcademicYear = false;
		}
		if ($showOnAcademicYear) {
			$yearStart = "Min(a.TermStart) as AcademicYearStart";
			$yearEnd = "Max(a.TermEnd) as AcademicYearEnd";
		} else {
			$preiodMonth = 12;
			$preYearStart = date("Y-m-d", strtotime("-" . $preiodMonth ." month"));
			$preYearEnd = date("Y-m-d", strtotime("+" . $preiodMonth ." month"));
			$yearStart = "IF (Min(a.TermStart) >= '" . $preYearStart . "', Min(a.TermStart), '" . $preYearStart . "') as AcademicYearStart";
			$yearEnd = "IF (Max(a.TermEnd) <= '" . $preYearEnd. "', Max(a.TermEnd), '" . $preYearEnd. "') as AcademicYearEnd";
		}

		# modified on 31-07-2009 by Ivan
		# Past: order by TermStart, TermEnd; Now:order by Sequence
		$sql = 'select
					*
				from
					(	select
						  	a.AcademicYearID,
							a.YearNameEN,
							a.YearNameB5,
							a.Sequence,
							MAX(a.CurrentSchoolYear) as CurrentSchoolYear, 
							' . $yearStart . ', 
							' . $yearEnd . '
						from 
							(select
								ay.AcademicYearID,
								ay.YearNameEN,
								ay.YearNameB5,
								ay.Sequence,
							  CASE
							  	WHEN CURDATE() between ayt.TermStart and ayt.TermEnd 
							  	THEN \'1\'
							    ELSE \'0\'
							  END as CurrentSchoolYear, 
							  ayt.TermStart, 
							  ayt.TermEnd
							from
								ACADEMIC_YEAR as ay
								left outer join
								ACADEMIC_YEAR_TERM as ayt
								on ay.AcademicYearID = ayt.AcademicYearID) as a
						Where
							1
							'.$year_conds.'
							'.$exclude_year_conds.'
							
						group by 
							a.AcademicYearID,
							a.YearNameEN,
							a.YearNameB5,
							a.Sequence
						order by
							'.$order_by_fields.'
					) as b
				where
					1
					'.$conds_NoPastYear.'
					'.$conds_PastAndCurrentYearOnly.'
					'.$conds_ExcludeCurrentYear.'
				';
		// echo $sql;
		
		$Result = $this->returnArray($sql);		
		return $Result;
	}
	
	function Get_All_Academic_Year() {	### Only use for listing out all possible academic year
		$sql = 'select
						  a.AcademicYearID,
							a.YearNameEN,
							a.YearNameB5,
							a.Sequence,
							MAX(a.CurrentSchoolYear) as CurrentSchoolYear, 
							Min(a.TermStart) as AcademicYearStart, 
							Max(a.TermEnd) as AcademicYearEnd
						from 
							(select
								ay.AcademicYearID,
								ay.YearNameEN,
								ay.YearNameB5,
								ay.Sequence,
							  CASE
							    WHEN CURDATE() between ayt.TermStart and ayt.TermEnd 
							    THEN \'1\'
							    ELSE \'0\'
							  END as CurrentSchoolYear, 
							  ayt.TermStart, 
							  ayt.TermEnd
							from
								ACADEMIC_YEAR as ay
								LEFT OUTER JOIN
								ACADEMIC_YEAR_TERM as ayt
								on ay.AcademicYearID = ayt.AcademicYearID) as a
						group by 
							a.AcademicYearID,
							a.YearNameEN,
							a.YearNameB5,
							a.Sequence
						order by
							a.Sequence';
		//echo $sql;
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function Get_Current_Academic_Year_And_Year_Term() {
		$sql = 'select
					ay.AcademicYearID,
					ay.YearNameEN,
					ay.YearNameB5,
					ay.Sequence,
					ayt.YearTermID,
					ayt.YearTermNameEN,
					ayt.YearTermNameB5,
					ayt.TermStart,
					ayt.TermEnd
				from
					ACADEMIC_YEAR as ay
					inner join
					ACADEMIC_YEAR_TERM as ayt
					on ay.AcademicYearID = ayt.AcademicYearID
				where CURDATE() between ayt.TermStart and ayt.TermEnd
				';
							
		//echo $sql;
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function Has_Term_Past($YearTermID)
	{
		$sql = 'Select
						YearTermID
				From
					ACADEMIC_YEAR_TERM
				Where 
						TermEnd < CURDATE()
					AND
						YearTermID = \''.$YearTermID.'\'
				';
		$Result = $this->returnVector($sql);
		
		if (count($Result) > 0)
			return 1;
		else
			return 0;
	}
	
	// get Name of Class
	// EG : A,B,C,D
	function Get_Name_Of_Class_List() {
		$sql = 'select
						  c.ClassID,
						  c.ClassNameB5,
						  c.ClassNameEN,
						  c.Sequence,
						  CASE
						    WHEN yc.ClassID is NULL then \'0\'
						    ELSE \'1\'
						  END as MappedClass
						From
						  CLASS as c
						  left outer join
						  (select ClassID from
						  YEAR_CLASS Group By ClassID) as yc
						  on c.ClassID = yc.ClassID';
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function Create_Name_Of_Class($NameOfClassEng,$NameOfClassChi) {
		$sql = 'insert into CLASS 
							(
							ClassNameEN,
							ClassNameB5,
							DateInput,
							InputBy
							) 
						value 
							(
							\''.$this->Get_Safe_Sql_Query($NameOfClassEng).'\',
							\''.$this->Get_Safe_Sql_Query($NameOfClassChi).'\',
							Now(),
							\'\'
							)';
		$Result = $this->db_db_query($sql);
		
		return $Result;
	}
	
	function Delete_Name_Of_Class($ClassID) {
		$sql = 'Delete from CLASS where ClassID = \''.$ClassID.'\'';

		$Result = $this->db_db_query($sql);
		return $Result;
	}
	
	function Rename_Name_Of_Class($ClassID,$ClassNameEn,$ClassNameB5) {
		$sql = 'update CLASS set ';
		if (trim($ClassNameEn) != "") {
			$sql .= ' ClassNameEn = \''.$this->Get_Safe_Sql_Query($ClassNameEn).'\' ';
		}
		else {
			$sql .= ' ClassNameB5 = \''.$this->Get_Safe_Sql_Query($ClassNameB5).'\' '; 
		}
		$sql .= 'where 
							ClassID = \''.$ClassID.'\'
							';
							
		$Result = $this->db_db_query($sql);
		
		return $Result;
	}
	
	function Get_Class_List($AcademicYearID,$YearID,$SearchValue='') {
		$NameField = getNameFieldByLang('u.');
		$ArchiveNameField = getNameFieldByLang2('au.');
		$sql = 'Select
							yc.YearClassID, 
							yc.ClassTitleEN,
							yc.ClassTitleB5,
							CASE 
								WHEN au.UserID IS NOT NULL then CONCAT(\'<font style="color:red;">*</font>\','.$ArchiveNameField.')
								ELSE '.$NameField.' 
							END as TeacherName, 
                            IF(ycu.StudentTotal is NULL, \'0\',ycu.StudentTotal) as NumberOfStudent,
                            yc.CurriculumID
						From
							YEAR_CLASS as yc
							left join
							YEAR_CLASS_TEACHER as yct
							on yc.YearClassID = yct.YearClassID
							left join 
							INTRANET_USER as u 
							on yct.UserID = u.UserID 
							left join 
							INTRANET_ARCHIVE_USER as au 
							on yct.UserID = au.UserID 
							left join
							(select YearClassID,count(YearClassID) as StudentTotal from YEAR_CLASS_USER group by YearClassID) as ycu
							on yc.YearClassID = ycu.YearClassID
						Where 
							yc.AcademicYearID = \''.$AcademicYearID.'\' and yc.YearID = \''.$YearID.'\'
						Order by 
							yc.Sequence, yc.ClassTitleEN, u.EnglishName
					 ';
		//echo $sql; die; 
		//debug_pr($sql);
		$Result = $this->returnArray($sql);
		
		for ($i=0; $i< sizeof($Result); $i++) {
			if ($ReturnArray[(sizeof($ReturnArray)-1)]['YearClassID'] == $Result[$i]['YearClassID']) {
				$ReturnArray[(sizeof($ReturnArray)-1)]['TeacherName'] .= ',<br>'.$Result[$i]['TeacherName'];
			}
			else {
				$ReturnArray[] = $Result[$i];
			}
		}
	
		return $ReturnArray;
	}
	
	function Get_Teaching_Staff_List() {
		$NameField = getNameFieldByLang();
		$sql = 'Select 
							UserID, 
							'.$NameField.' as Name
						From 
							INTRANET_USER 
						where 
							RecordStatus = \'1\' 
							and 
							RecordType = \'1\' 
							and 
							Teaching = \'1\'
						ORDER BY
							EnglishName
					 ';
		//echo $sql; die;
		return $this->returnArray($sql);
	}
	
	function Get_Non_Teaching_Staff_List($OrderByFields="") {
		$NameField = getNameFieldByLang();
		$sql = 'Select 
							UserID, 
							'.$NameField.' as Name
						From 
							INTRANET_USER 
						where 
							RecordStatus = \'1\' 
							and 
							RecordType = \'1\' 
							and 
							(Teaching = \'0\' OR Teaching IS NULL)
						ORDER BY
							'.$OrderByFields.' EnglishName
					 ';
		//echo $sql; die;
		return $this->returnArray($sql);
	}
	
	function Search_Student($SearchValue,$StudentSelected="",$AcademicYearID="",$YearClassID="") {
		$NameField = getNameFieldByLang('u.');
		$StudentCondition = (trim($StudentSelected) == "")? "": ' and u.UserID not in ('.$StudentSelected.') ';
		$SearchValue = $this->Get_Safe_Sql_Query($SearchValue);
		
		$this->With_Nolock_Trans();
		$sql = 'select
              u.UserID,
              '.$NameField.' as Name,
						  u.UserEmail
					  from 
					  	INTRANET_USER as u
					  where 
					  	u.RecordType = \'2\' 
              '.$StudentCondition.'
					  	and u.RecordStatus = \'1\'
					  	and (
					  		u.UserLogin like \'%'.$SearchValue.'%\'
					  		OR
					  		u.EnglishName like \'%'.$SearchValue.'%\'
					  		OR
					  		u.UserEmail like \'%'.$SearchValue.'%\'
					  		OR
					  		u.ChineseName like \'%'.$SearchValue.'%\'
					  	)
              and
              u.UserID not in (
                Select ycu.UserID
                From
                  YEAR_CLASS as yc
                  inner join
                  YEAR_CLASS_USER as ycu
    						  on ycu.YearClassID = yc.YearClassID and yc.AcademicYearID = \''.$AcademicYearID.'\' and yc.YearClassID != \''.$YearClassID.'\'
              )';
		//echo $sql; die;
		return $this->returnArray($sql);
	}
	
	function Create_Class($AcademicYearID, $YearID, $ClassTitleEN, $ClassTitleB5, $StudentSelected, $SelectedClassTeacher, $ClassNumberMethod, $WEBSAMSCode, $ClassGroupID, $CurriculumID='', $BuildingID='') 
	{
	    global $plugin;
	    
		$CurrentAcademicYearEdit = ($AcademicYearID == Get_Current_Academic_Year_ID());
		$sql = 'select max(Sequence) 
						from 
							YEAR_CLASS 
						where 
							AcademicYearID = \''.$AcademicYearID.'\' 
							and 
							YearID = \''.$YearID.'\'';
		$MaxSequence = $this->returnVector($sql);
		$NextSequence = $MaxSequence[0]+1;
		
		if ($plugin['eSchoolBus']) {
		    $additionalField = ',BuildingID';
		    $additionalValue = ",'$BuildingID'";
		}
		else {
		    $additionalField = '';
		    $additionalValue = '';
		}
		
		// insert class record
		$sql = 'Insert into YEAR_CLASS (
							AcademicYearID, 
							YearID, 
							ClassTitleEN, 
							ClassTitleB5, 
							Sequence,
							DateInput, 
							InputBy, 
							WEBSAMSCode,
							ClassGroupID,
                            CurriculumID'.$additionalField.'
							)
						value (
							\''.$AcademicYearID.'\',
							\''.$YearID.'\',
							\''.$this->Get_Safe_Sql_Query($ClassTitleEN).'\',
							\''.$this->Get_Safe_Sql_Query($ClassTitleB5).'\',
							\''.$NextSequence.'\',
							now(),
							\''.$_SESSION['UserID'].'\',
							\''.$this->Get_Safe_Sql_Query($WEBSAMSCode).'\',
							\''.$ClassGroupID.'\',
                            \''.$CurriculumID.'\''.$additionalValue.'
							)';
		$Result['InsertClass'] = $this->db_db_query($sql);
		
		if ($Result['InsertClass'])  {
			$YearClassID = $this->db_insert_id();
			
			#create eclass
		//if ($isCreateclass){
			/*$libeclass = new libeclass();
			$course_id = $libeclass->eClassAdd($WEBSAMSCode, $ClassTitleEN, '', 0, 30, $_SESSION['UserID'], '', false, $YearClassID, true);
			$lo = new libeclass($course_id);
			$iCal = new icalendar();
			$calID = $iCal->createSystemCalendar($ClassTitleEN,3,"P",'');
			$Result['create_Calendar_to_eclass'] = $calID;
			$sql = "Update course set CalID = '$calID' where course_id = '$course_id'";
			 $Result['create_Calendar_connection_to_eclass'] = $lo->db_db_query($sql);
			$allUser = array_merge($StudentSelected,$SelectedClassTeacher);
			if (count($allUser)>0){
				$sql = "SELECT UserID, UserEmail, UserPassword, ClassNumber, FirstName, LastName, ClassName, CASE Title WHEN 0 THEN 'MR' WHEN 0 THEN 'MR.' WHEN 1 THEN 'MISS.' WHEN 2 THEN 'MRS.' WHEN 3 THEN 'MS.' WHEN 4 THEN 'DR.' WHEN 5 THEN 'PROF.' END , EnglishName, ChineseName , NickName, Case when RecordType = 1 Then 'T' ELSE 'S' END as MemberType
				FROM INTRANET_USER 
				WHERE UserID IN (".implode(",", $allUser).")";
			  $row = $iCal->returnArray($sql,11);
			  for($i=0; $i<sizeof($row); $i++){
				//if($lo->max_user <> "" && $i == $lo->ticketUser()) break;
				
				$User_ID = $row[$i][0];
				$UserEmail = $row[$i][1];
				$UserPassword = $row[$i][2];
				$ClassNumber = $row[$i][3];
				$FirstName = $row[$i][4];
				$LastName = $row[$i][5];
				$ClassName = $row[$i][6];
				$Title = $row[$i][7];
				$EngName = $row[$i][8];
				$ChiName = $row[$i][9];
				$NickName = $row[$i][10];
				$MemberType = $row[$i][11];
				if ($ClassNumber=="" || $ClassName=="" || stristr($ClassNumber,$ClassName))
				  $eclassClassNumber = $ClassNumber;
				else
				  $eclassClassNumber = $ClassName ." - ".$ClassNumber;
			 $lo->eClassUserAddFullInfo($UserEmail, $Title,$FirstName,$LastName, $EngName, $ChiName, $NickName, $MemberType, $UserPassword, $eclassClassNumber,$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,$URL,$Info, "", $User_ID);
			  }
			  $lo->eClassUserNumber($lo->course_id);			  
				$Result['insert_calendar_viewer'] = $iCal->insertCalendarViewer($calID,$allUser,"W",$course_id,'C');
			}
			$sql = "update YEAR_CLASS set course_id = $course_id where YearClassID = $YearClassID";
			$Result['updateCourseID'] = $this->db_db_query($sql);*/
		//}
			
			
			// create class group
			$sql = 'select 
								GroupID 
							From 
								INTRANET_GROUP 
							where 
								Title = \''.$this->Get_Safe_Sql_Query($ClassTitleEN).'\' 
								and 
								AcademicYearID = \''.$AcademicYearID.'\'';
			$Temp = $this->returnVector($sql);
			if (sizeof($Temp) > 0) {
				$GroupID = $Temp[0];
				
				$sql = 'update INTRANET_GROUP set 
									RecordType = 3 
								where 
									GroupID = \''.$GroupID.'\'';
				$Result['UpdateIntranetGroup'] = $this->db_db_query($sql);
			}
			else {
				$sql = "INSERT INTO INTRANET_GROUP 
									(
									Title,
									TitleChinese,
									RecordType,
									StorageQuota,
									DateInput,
									DateModified, 
									AcademicYearID
									) 
								VALUES 
									(
									'".$this->Get_Safe_Sql_Query($ClassTitleEN)."',
									'".$this->Get_Safe_Sql_Query($ClassTitleB5)."',
									3,
									5,
									now(),
									now(),
									'".$AcademicYearID."'
									)";
		    $Result['InsertIntranetGroup'] = $this->db_db_query($sql);
		    
		    $GroupID = $this->db_insert_id();
		  }
		  
		  // insert class teacher
			for ($i=0; $i< sizeof($SelectedClassTeacher); $i++) {
				$sql = 'Insert into YEAR_CLASS_TEACHER (
									YearClassID, 
									UserID, 
									DateInput, 
									InputBy
									)
								Value 
									(
									\''.$YearClassID.'\',
									\''.$SelectedClassTeacher[$i].'\',
									now(),
									\''.$_SESSION['UserID'].'\'
									)';
				$Result['InsertClassTeacher'.$i] = $this->db_db_query($sql);
				
				# Add to Group
				$Result['InsertTeacherToGroup'.$i] = $this->Add_User_To_Group($GroupID, $SelectedClassTeacher[$i], '');
			}
	    
		    if ($Result['InsertIntranetGroup'] || $Result['UpdateIntranetGroup']) {
		    	$sql = 'update YEAR_CLASS set 
		    						GroupID = \''.$GroupID.'\' 
		    					where 
		    						YearClassID = \''.$YearClassID.'\'';
		    	$Result['UpdateGroupIDToClass'] = $this->db_db_query($sql);
					
					// insert class student 
					# Found out the default Role ID
					$RoleID = $this->Get_Default_Class_Group_RoleID();
					
					for ($i=0; $i< sizeof($StudentSelected); $i++) {
						$sql = 'Insert into YEAR_CLASS_USER (
											YearClassID, 
											UserID, 
											DateInput, 
											InputBy
											)
										Value 
											(
											\''.$YearClassID.'\',
											\''.$StudentSelected[$i].'\',
											now(),
											\''.$_SESSION['UserID'].'\'
											)';
						$Result['InsertClassStudent'.$i] = $this->db_db_query($sql);
						
						# Add to Group
						$Result['InsertUserToGroup'.$i] = $this->Add_User_To_Group($GroupID, $StudentSelected[$i], $RoleID);
						/*
						$sql = 'INSERT INTO INTRANET_USERGROUP (GroupID, UserID,RoleID) 
			    						VALUES (\''.$GroupID.'\', \''.$StudentSelected[$i].'\',\''.$RoleID.'\') 
			    					ON DUPLICATE KEY UPDATE 
			    						DateModified = NOW()';
	        			$Result['InsertUserToGroup'.$i] = $this->db_db_query($sql);
	        			*/
					}
					
					
					# insert class Calendar
					include_once("icalendar.php"); 
					$iCal = new icalendar();
					$cal = $this->Get_CalendarID_By_GroupID($GroupID);
					/*
					$sql = "select CalID from INTRANET_GROUP where GroupID = '$GroupID'";
					$cal = $this->returnVector($sql);
					*/
					
					// debug_r($cal);
					// exit;
					if (empty($cal)||!is_numeric($cal)){
					// echo "Create Class";
						$Result["Create_class_cal"] = $iCal->createSystemCalendar($ClassTitleEN, 2);
						if ($Result["Create_class_cal"])
							$cal = $Result["Create_class_cal"];
						$sql = "update INTRANET_GROUP set CalID = '$cal' where GroupID = '$GroupID'";
						$Result["Create_update_group_cal"] = $this->db_db_query($sql);
					}
					
					// echo "Cal: ".$cal;
					# add teacher calendar
					if (sizeof($SelectedClassTeacher)>0)
							$Result["Create_teacher_view"]=$iCal ->insertCalendarViewer($cal,$SelectedClassTeacher,'W',$GroupID,'T');
					# add student calendar
					
					if (sizeof($StudentSelected)>0)
					{
						$Result["Create_student_view"]=$iCal ->insertCalendarViewer($cal,$StudentSelected,'R',$GroupID,'E');
						$Result['SetClassNumber'] = $this->Make_Class_Numbers($YearClassID, $ClassNumberMethod);
						$Result['AddClassHistory'] = $this->Update_Class_History($YearClassID);
					}
		    }
		}
		
		
		
		return $Result;
	}
	
	/*
	 *	If you change the edit class logic in this function,
	 *	Please check if you have to change the logic of the following functions also
	 *		1. Edit_Class()
	 *		2. Edit_ClassOnly()
	 *		3. Add_Student_In_Class()
	 *		4. Add_Class_Teacher()
	 */
	function Edit_Class($YearClassID,$ClassTitleEN,$ClassTitleB5,$StudentSelected,$SelectedClassTeacher,$ClassNumberMethod,$WEBSAMSCode,$teacher_benefit,$is_user_import,$ClassGroupID,$CurriculumID='', $BuildingID='') 
	{
		global $PATH_WRT_ROOT, $sys_custom, $intranet_root, $plugin;
		
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/libuser.php');
		$libSCM = new subject_class_mapping();
		$luser = new libuser();
		
		### Get Class info
		$YearClass = new year_class($YearClassID,false,true,true,true);
		$GroupID = $YearClass->GroupID;
		$AcademicYearID = $YearClass->AcademicYearID; 
		$IsCurrentAcademicYearEdit = ($AcademicYearID == Get_Current_Academic_Year_ID());
		
		$numOfSelectedTeacher = count($SelectedClassTeacher);
		$numOfSelectedStudent = count($StudentSelected);
		
		### Get Original Class Info
		$OriginalClassTeacherIDArr = Get_Array_By_Key($YearClass->ClassTeacherList, 'UserID');
		$numOfOriginalClassTeacher = count($OriginalClassTeacherIDArr);
		$OriginalClassStudentIDArr = Get_Array_By_Key($YearClass->ClassStudentList, 'UserID');
		$numOfOriginalClassStudent = count($OriginalClassStudentIDArr);
		
		### Get Newly added Student
		$NewStudentIDArr = array_values(array_diff($StudentSelected, $OriginalClassStudentIDArr));
		$numOfNewStudent = count($NewStudentIDArr);
		
		### Get the Locked Subject Group
		$LockedSubjectGroupIDArr = Get_Array_By_Key($YearClass->LockedSubjectGroupList, 'SubjectGroupID');
		$numOfLockedSG = count($LockedSubjectGroupIDArr);
		
		### Get current academic yesr group list (for delete old class groups as INTRANET_USERGROUP has no AcademicYearID field)
		$sql = 'select GroupID From INTRANET_GROUP where RecordType = 3 And AcademicYearID = \''.$YearClass->AcademicYearID.'\'';
		$classGroupArr = $this->returnVector($sql);
		if (count($classGroupArr) > 0)
			$classGroupList = implode(',', $classGroupArr);
		else
			$classGroupList = "''";
		
		// update class record
		$sql = 'update YEAR_CLASS set 
							ClassTitleEN = \''.$this->Get_Safe_Sql_Query($ClassTitleEN).'\', 
							ClassTitleB5 = \''.$this->Get_Safe_Sql_Query($ClassTitleB5).'\', 
							WEBSAMSCode = \''.$this->Get_Safe_Sql_Query($WEBSAMSCode).'\', 
							ClassGroupID = \''.$this->Get_Safe_Sql_Query($ClassGroupID).'\',
                            CurriculumID = \''.$this->Get_Safe_Sql_Query($CurriculumID).'\',';
		if ($plugin['eSchoolBus']) {
            $sql .= ' BuildingID = \''.$this->Get_Safe_Sql_Query($BuildingID).'\',';
		}
		$sql .= ' ModifyBy = \''.$_SESSION['UserID'].'\'
						Where 
							YearClassID = \''.$YearClassID.'\'';
		$Result['UpdateClass'] = $this->db_db_query($sql);
		
		// update class group
		if ($GroupID != '')
		{
			$sql = 'update INTRANET_GROUP set 
								Title = \''.$this->Get_Safe_Sql_Query($ClassTitleEN).'\', 
								TitleChinese = \''.$this->Get_Safe_Sql_Query($ClassTitleB5).'\', 
								RecordType = 3 
							where 
								GroupID = \''.$GroupID.'\'';
			$Result['UpdateIntranetGroup'] = $this->db_db_query($sql);
		}
		
		# Found out the default Role ID
		$RoleID = $this->Get_Default_Class_Group_RoleID();
		
		
		
		### Teacher Handling	
		// delete class teacher
		$sql = 'delete from YEAR_CLASS_TEACHER where YearClassID = \''.$YearClassID.'\'';
		$Result['DeleteClassTeacher'] = $this->db_db_query($sql);
		
		// delete unselected class teacher in group
		if ($numOfOriginalClassTeacher > 0)
		{
			$SelectedClassTeacherIDList = ($numOfSelectedTeacher > 0)? implode(',', $SelectedClassTeacher) : "''";
			$OriginalClassTeacherIDList = ($numOfOriginalClassTeacher > 0)? implode(',', $OriginalClassTeacherIDArr) : "''";
			
			$sql = " delete from INTRANET_USERGROUP 
							where 	UserID Not In ($SelectedClassTeacherIDList)
									AND
									UserID In ($OriginalClassTeacherIDList)
									And
									GroupID = '".$GroupID."'
					";
			$Result['DeleteClassTeacherFromGroup'] = $this->db_db_query($sql);
		}
			
		// insert class teacher
		for ($i=0; $i< $numOfSelectedTeacher; $i++) {
			$thisUserID = $SelectedClassTeacher[$i];
			
			# Add to Class
			$sql = 'Insert into YEAR_CLASS_TEACHER (
								YearClassID, 
								UserID, 
								DateInput, 
								InputBy
								)
							Value 
								(
								\''.$YearClassID.'\',
								\''.$thisUserID.'\',
								now(),
								\''.$_SESSION['UserID'].'\'
								)';
			$Result['InsertClassTeacher'.$i] = $this->db_db_query($sql);
			
			# Add to Group
			$Result['InsertClassTeacherToGroup'.$i] = $this->Add_User_To_Group($GroupID, $thisUserID, '');
		}
		
		if($sys_custom['project']['NCS'] && $SelectedClassTeacher!=$OriginalClassTeacherIDArr){
			include_once($intranet_root."/includes/libcurdlog.php");
			$log = new libcurdlog();
			$sql = "SELECT EnglishName FROM INTRANET_USER WHERE UserID IN ('".implode("','", $OriginalClassTeacherIDArr)."')";
			$oriList = $log->returnVector($sql);
			
			$sql = "SELECT EnglishName FROM INTRANET_USER WHERE UserID IN ('".implode("','", $SelectedClassTeacher)."') AND UserID NOT IN ('".implode("','", $OriginalClassTeacherIDArr)."')";
			$newList = $log->returnVector($sql);
			
			$sql = "SELECT EnglishName FROM INTRANET_USER WHERE UserID NOT IN ('".implode("','", $SelectedClassTeacher)."') AND UserID IN ('".implode("','", $OriginalClassTeacherIDArr)."')";
			$delList = $log->returnVector($sql);
			
			$sql = "SELECT CONCAT(YearNameEN, ' ', ClassTitleEN) From YEAR_CLASS c INNER JOIN ACADEMIC_YEAR y ON c.AcademicYearID = y.AcademicYearID WHERE YearClassID='".IntegerSafe($YearClassID)."'";
			$yearClassName= current($log->returnVector($sql));
			
			$description = "";
			if(!empty($newList)){
				$description .= "Added Teacher :".implode(",", $newList);
			}
			if(!empty($delList)&&!empty($newList)){
				$description .= ";";
			}
			if(!empty($delList)){
				$description .= "Deleted Teacher :".implode(",", $delList);
			}
			
			$log->addlog("User Group", "UPDATE", "YEAR_CLASS_TEACHER", "Class", $yearClassName, implode(",", $oriList), $description, "");
			unset($log);
		}
		
		### Student Handling ###
		# added by Ivan 20090930 - delete the user from Group if deleted from the class list
		$sql = 'select 
							UserID 
						from 
							YEAR_CLASS_USER 
						where 
							YearClassID = \''.$YearClassID.'\' ';
		if ($numOfSelectedStudent > 0) {
			$sql .= ' and 
							UserID NOT IN ('.implode(',',$StudentSelected).')';
		}
		$DeletedStudentIDArr = $this->returnVector($sql);
		$numOfDeletedStudent = count($DeletedStudentIDArr);
		
		if ($numOfDeletedStudent > 0) 
		{
			$sql = 'delete from INTRANET_USERGROUP where GroupID = \''.$GroupID.'\' and UserID in ('.implode(',', $DeletedStudentIDArr).')';
			$Result['DeleteUserGroup'] = $this->db_db_query($sql);
			
			# 20101108 Ivan - Lock Logic (delete the student from SG which linked to the Class)
			if ($sys_custom['SubjectGroup']['LockLogic'] && $numOfLockedSG > 0)
				$Result['DeleteStudentFromLockedSubjectGroup'] = $libSCM->Delete_Student_From_Subject_Group($LockedSubjectGroupIDArr, $DeletedStudentIDArr);
		}
		
		# 20101108 Ivan - Lock Logic (add the student to SG which linked to the Class)
		if ($sys_custom['SubjectGroup']['LockLogic'] && $numOfLockedSG > 0 && $numOfNewStudent > 0)
			$Result['AddStudentToLockedSubjectGroup'] = $libSCM->Add_Student_To_Subject_Group($LockedSubjectGroupIDArr, $NewStudentIDArr);
		
		//if ($ClassNumberMethod == "ByName") 
		if ($ClassNumberMethod != "ByTime") 
		{
			if ($IsCurrentAcademicYearEdit) {
				// clear intranet user class info
				$Result['ClearIntranetUserClassInfo'] = $this->Clear_Class_Number($YearClassID);
			}
			
			// delete old class student 
			$sql = 'delete from YEAR_CLASS_USER where YearClassID = \''.$YearClassID.'\'';
			$Result['DeleteClassStudent'] = $this->db_db_query($sql);
			
			for ($i=0; $i<$numOfSelectedStudent; $i++) {
				$thisUserID = $StudentSelected[$i];
				
				$sql = 'Insert into YEAR_CLASS_USER (
									YearClassID, 
									UserID, 
									DateInput, 
									InputBy
									)
								Value 
									(
									\''.$YearClassID.'\',
									\''.$thisUserID.'\',
									now(),
									\''.$_SESSION['UserID'].'\'
									)';
				$Result['InsertClassStudent'.$i] = $this->db_db_query($sql);
				
				# Delete old class group record if there are any
				if ($GroupID != '')
				{
					$sql = " Delete From INTRANET_USERGROUP 
									Where 
											UserID = '".$thisUserID."' 
											And 
											RecordType = 3
											AND
											GroupID != '".$GroupID."'
											AND
											GroupID IN (".$classGroupList.") 
							";
					$Result['DeleteOldUserGroup'.$i] = $this->db_db_query($sql);
				}
				
				# add to new class group
				$Result['InsertUserToGroup'.$i] = $this->Add_User_To_Group($GroupID, $thisUserID, $RoleID);
			}
						
			if ($numOfSelectedStudent > 0) 
				$Result['SetClassNumber'] = $this->Make_Class_Numbers($YearClassID, $ClassNumberMethod);
		}
		else if ($ClassNumberMethod == "ByTime") {
			// delete removed class student 
			/*
			$sql = 'select 
								UserID 
							from 
								YEAR_CLASS_USER 
							where 
								YearClassID = \''.$YearClassID.'\' ';
			if (sizeof($StudentSelected) > 0) {
				$sql .= ' and 
								UserID NOT IN ('.implode(',',$StudentSelected).')';
			}
			$UserList = $this->returnVector($sql);
			*/
			if ($numOfDeletedStudent > 0) {
				$sql = 'delete from YEAR_CLASS_USER where YearClassID = \''.$YearClassID.'\' and UserID in ('.implode(',',$DeletedStudentIDArr).')';
				$Result['DeleteClassStudent'] = $this->db_db_query($sql);
				
				if ($IsCurrentAcademicYearEdit) {
					$sql = 'update INTRANET_USER set 
										ClassName = NULL, 
										ClassNumber = NULL 
									where 
										UserID in ('.implode(',',$DeletedStudentIDArr).')';
					$Result['ResetDeleteClassNumber'] = $this->db_db_query($sql);
				}
			}
			
			// get next class number
			$sql = "select IFNULL(max(ClassNumber)+1,1) as NextClassNumber from YEAR_CLASS_USER where YearClassID = '".$YearClassID."'";
			//debug_r($sql);
			$Temp = $this->returnVector($sql);
			$NextClassNumber = $Temp[0];
			
			for ($i=0; $i< sizeof($StudentSelected); $i++) {	
				$thisUserID = $StudentSelected[$i];
				
				//check if is new class student
				$sql = 'select 
									count(1) 
								from 
									YEAR_CLASS_USER 
								where 
									YearClassID = \''.$YearClassID.'\' 
									and 
									UserID = \''.$thisUserID.'\'
									';
				$StudentExist = $this->returnVector($sql);
				
				// already exist , check and add user in class group 
				if ($StudentExist[0] > 0) {
					$sql = "SELECT COUNT(1) FROM INTRANET_USERGROUP WHERE GroupID = '$GroupID' AND UserID= '$thisUserID' ";
					$StudentInClassGroup = $this->returnVector($sql);
					if($StudentInClassGroup[0]>0)
						continue;
					else
						$Result['InsertUserToGroup'.$i] = $this->Add_User_To_Group($GroupID, $thisUserID, $RoleID);
				}
				else {
					$sql = 'Insert into YEAR_CLASS_USER (
									YearClassID, 
									UserID, 
									DateInput, 
									InputBy, 
									ClassNumber
									)
								Value 
									(
									\''.$YearClassID.'\',
									\''.$thisUserID.'\',
									now(),
									\''.$_SESSION['UserID'].'\',
									'.$NextClassNumber.'
									)';
					$Result['InertClassStudent'.$i] = $this->db_db_query($sql);
					
					# Delete old class group record if there are any
					if ($GroupID != '')
					{
						$sql = " Delete From INTRANET_USERGROUP 
										Where 
												UserID = '".$thisUserID."' 
												And 
												RecordType = 3
												AND
												GroupID != '".$GroupID."'
												AND
												GroupID IN (".$classGroupList.")
								";
						$Result['DeleteOldUserGroup'.$i] = $this->db_db_query($sql);
					}
					
					
					# Add to Group
					$Result['InsertUserToGroup'.$i] = $this->Add_User_To_Group($GroupID, $thisUserID, $RoleID);
					
					$NextClassNumber++;
				}
			}
			
			if ($IsCurrentAcademicYearEdit) {
				if (sizeof($StudentSelected) > 0) 
					$Result['SetClassNumber'] = $this->Make_Class_Numbers($YearClassID,$ClassNumberMethod,$YearClass);
			}
		}
		
		if($sys_custom['project']['NCS'] && (!empty($NewStudentIDArr) || !empty($DeletedStudentIDArr))){
			include_once($intranet_root."/includes/libcurdlog.php");
			$log = new libcurdlog();
			$sql = "SELECT EnglishName FROM INTRANET_USER WHERE UserID IN ('".implode("','", $OriginalClassStudentIDArr)."')";
			$oriList = $log->returnVector($sql);
				
			$sql = "SELECT EnglishName FROM INTRANET_USER WHERE UserID IN ('".implode("','", $NewStudentIDArr)."')";
			$newList = $log->returnVector($sql);
				
			$sql = "SELECT EnglishName FROM INTRANET_USER WHERE UserID IN ('".implode("','", $DeletedStudentIDArr)."')";
			$delList = $log->returnVector($sql);
			
			$sql = "SELECT CONCAT(YearNameEN, ' ', ClassTitleEN) From YEAR_CLASS c INNER JOIN ACADEMIC_YEAR y ON c.AcademicYearID = y.AcademicYearID WHERE YearClassID='".IntegerSafe($YearClassID)."'";
			$yearClassName= current($log->returnVector($sql));
			
			$description = "";
			if(!empty($newList)){
				$description .= "Added Student :".implode(",", $newList);
			}
			if(!empty($delList)&&!empty($newList)){
				$description .= ";";
			}
			if(!empty($delList)){
				$description .= "Deleted Student :".implode(",", $delList);
			}
			
			$log->addlog("User Group", "UPDATE", "YEAR_CLASS_USER", "Class", $yearClassName, implode(",", $oriList), $description, "");
			unset($log);
		}
		
		# iCalendar configuration
		 // echo "Group ID:".$GroupID;
		if (!empty($GroupID) && is_numeric($GroupID)){
			$calID = $this->Get_CalendarID_By_GroupID($GroupID);
			/*
			$sql = "select CalID from INTRANET_GROUP 
					where GroupID = '$GroupID'";
			$cal = $this->returnVector($sql);
			*/
			if (!empty($calID) && is_numeric($calID)){
				include_once("icalendar.php"); 
				$iCal = new icalendar();
				
				# reomve deleted user
				if (count($SelectedClassTeacher)>0){
					$teacherSql = "'".implode("','",$SelectedClassTeacher)."'";
				}
				else
					$teacherSql = "''";
				$sql = "
				delete from CALENDAR_CALENDAR_VIEWER where
				CalID = '$calID' and 
				UserID not in ($teacherSql) and 
				GroupType = 'T'";
				$Result['Remove_non-existance_Teacher'] = $this->db_db_query($sql);	
				
				if (count($StudentSelected) > 0){
					$studentSql = "'".implode("','",$StudentSelected)."'";
				}
				else 
					$studentSql = "''";
				$sql = "
				delete from CALENDAR_CALENDAR_VIEWER where
				CalID = '$calID' and 
				UserID not in ($studentSql) and 
				GroupType = 'E' and
				UserID not in (
					select UserID from INTRANET_USERGROUP 
					where GroupID = '$GroupID'
				)";
				$Result['Remove_non-existance_Student'] = $this->db_db_query($sql);	
					
				#update calendar
				$sql = "update CALENDAR_CALENDAR 
						set Name='".$this->Get_Safe_Sql_Query($ClassTitleEN)."'
						where CalID = '$calID'";
				$Result['Update_calendar'] = $this->db_db_query($sql);
				
				#insert new viewer
				$calStudent = $iCal->getCalendarViewer($calID,"","E");
				$calTeacher = $iCal->getCalendarViewer($calID,"","T");
				$newTeacher = array_diff($SelectedClassTeacher,$calTeacher);
				$newStudent = array_diff($StudentSelected,$calStudent);
				if (count($newTeacher) > 0)
					$Result['insert_new_teacher']=$iCal->insertCalendarViewer($calID,$newTeacher,'W',$GroupID,'T');
				if (count($newStudent) > 0)
					$Result['insert_new_student']=$iCal->insertCalendarViewer($calID,$newStudent,'R',$GroupID,'E');
			}				
		}
		
		$Result['UpdateClassHistory'] = $this->Update_Class_History($YearClassID);
		$Result['synClassUserDataToModules'] = $luser->synUserDataToModules($StudentSelected);
		$Result['synDeletedUserDataToModules'] = $luser->synUserDataToModules($DeletedStudentIDArr);
		
		return $Result;
	}
	
	function Clear_Class_Number($YearClassID) {
		$YearClass = new year_class($YearClassID,false,false,false);		

		$YearClassAcademicYearID = $YearClass->AcademicYearID;
		$CurrentSchoolYearID = Get_Current_Academic_Year_ID();
		
		if ($YearClassAcademicYearID==$CurrentSchoolYearID)
		{
			// clear intranet user class info
			$sql = 'select 
								UserID 
							from 
								YEAR_CLASS_USER 
							where 
								YearClassID = \''.$YearClassID.'\'';
			$ClassStudentList = $this->returnVector($sql);
			
			if (sizeof($ClassStudentList) > 0) {
				$StudentList = implode(",",$ClassStudentList);
				
				$sql = 'update INTRANET_USER set 
									ClassName = NULL, 
									ClassNumber = NULL 
								where 
									UserID in ('.$StudentList.')';
				return $this->db_db_query($sql);
			}
		}
		
		return true;
	}
	
	function Delete_Class($YearClassID) {
		$YearClass = new year_class($YearClassID);
		$GroupID = $YearClass->GroupID;
		
		if ($YearClass->AcademicYearID == Get_Current_Academic_Year_ID()) {
			// clear intranet user class info
			$Result['ClearIntranetUserClassInfo'] = $this->Clear_Class_Number($YearClassID);
		}
		
		// delete class teacher
		$sql = 'delete from YEAR_CLASS_TEACHER where YearClassID = \''.$YearClassID.'\'';
		$Result['DeleteClassTeacher'] = $this->db_db_query($sql);
		
		// delete class student 
		$sql = 'delete from YEAR_CLASS_USER where YearClassID = \''.$YearClassID.'\'';
		$Result['DeleteClassStudent'] = $this->db_db_query($sql);
		
		// delete class
		$sql = 'delete from YEAR_CLASS where YearClassID = \''.$YearClassID.'\'';
		$Result['DeleteClass'] = $this->db_db_query($sql);
		
		// delete group
		$sql = 'delete from INTRANET_GROUP where GroupID = \''.$GroupID.'\'';
		$Result['DeleteGroup'] = $this->db_db_query($sql);
		
		// delete group member
		$sql = 'delete from INTRANET_USERGROUP where GroupID = \''.$GroupID.'\'';
		$Result['DeleteGroupMember'] = $this->db_db_query($sql);
		
		// delete class history
		$sql = 'delete from PROFILE_CLASS_HISTORY where YearClassID = \''.$YearClassID.'\'';
		$Result['DeleteClassHistory'] = $this->db_db_query($sql);
		
		// unlink Locked Subject Group
		$sql = "Update SUBJECT_TERM_CLASS Set LockedYearClassID = null Where LockedYearClassID = '".$YearClassID."'";
		$Result['UnlinkLockedSubjectGroup'] = $this->db_db_query($sql);
		
		return $Result;
	}
	
	// class number making from the create/ edit class form
	function Make_Class_Numbers($YearClassID,$ClassNumberMethod="ByName",$OldYearClass="") {
		$CurrentSchoolYearID = Get_Current_Academic_Year_ID();
		
		if ($ClassNumberMethod == "ByName") 
		{
			$sql = 'select 
								yuc.YearClassUserID, 
								yc.AcademicYearID, 
								yc.ClassTitleEN,
								u.UserID
							From 
								YEAR_CLASS_USER yuc 
								inner join 
								YEAR_CLASS yc 
								on yuc.YearClassID = yc.YearClassID 
								inner join 
								INTRANET_USER u 
								on yuc.UserID = u.UserID and yuc.YearClassID = \''.$YearClassID.'\' 
							Order By 
								u.EnglishName asc';
			$Result = $this->returnArray($sql);
			
			for ($i=0; $i< sizeof($Result); $i++) {
				$sql = 'update YEAR_CLASS_USER set 
									ClassNumber = \''.($i+1).'\' 
								where 
									YearClassUserID = \''.$Result[$i]['YearClassUserID'].'\'';
				
				$ReturnValue['SetClassNumber'.$i] = $this->db_db_query($sql);
				
				if ($Result[$i]['AcademicYearID'] == $CurrentSchoolYearID) {
					$sql = 'update INTRANET_USER set 
										ClassNumber = \''.($i+1).'\', 
										ClassName = \''.$this->Get_Safe_Sql_Query($Result[$i]['ClassTitleEN']).'\' 
									where 
										UserID = \''.$Result[$i]['UserID'].'\'';
					$ReturnValue['SetClassNumberInIntranetUser'.$i] = $this->db_db_query($sql);
				}
			}
			
			return in_array(false,$ReturnValue)? false:true;
		}
		else if ($ClassNumberMethod == "ByTime") {
			$Result = array();
			
			$YearClass = new year_class($YearClassID,false,false,false);		
			$YearClassAcademicYearID = $YearClass->AcademicYearID;
			$CurrentSchoolYearID = Get_Current_Academic_Year_ID();
			
			if ($YearClassAcademicYearID == $CurrentSchoolYearID)
			{
				if (sizeof($OldYearClass->ClassStudentList) > 0) {
					for ($i=0; $i< sizeof($OldYearClass->ClassStudentList) ; $i++) {
						$OldUserID[] = $OldYearClass->ClassStudentList[$i]['UserID'];
					}
					
					$sql = 'update INTRANET_USER set 
											ClassName = NULL,
											ClassNumber = NULL 
										Where 
											UserID in ('.implode(',',$OldUserID).')';
					$Result['ResetOldClassNumber'] = $this->db_db_query($sql);
				}
				
				$sql = 'select 
									ycu.UserID, 
									yc.ClassTitleEN, 
									ycu.ClassNumber 
								from 
									YEAR_CLASS as yc
									inner join 
									YEAR_CLASS_USER as ycu 
									on yc.YearClassID = ycu.YearClassID and yc.YearClassID = \''.$YearClassID.'\'';
				$ClassNumberInfo = $this->returnArray($sql);
				
				for ($i=0; $i< sizeof($ClassNumberInfo); $i++) {
					$sql = 'update INTRANET_USER set 
										ClassName = \''.$this->Get_Safe_Sql_Query($ClassNumberInfo[$i]['ClassTitleEN']).'\', 
										ClassNumber = \''.$ClassNumberInfo[$i]['ClassNumber'].'\' 
									where 
										UserID = \''.$ClassNumberInfo[$i]['UserID'].'\'';
					$Result['SetClassNumber'.$i] = $this->db_db_query($sql);
				}
			}
			
			return in_array(false,$Result)? false:true;
		}
		else if ($ClassNumberMethod == "ByBoysName" || $ClassNumberMethod == "ByGirlsName") 
		{
			if($ClassNumberMethod == "ByBoysName")
				$gender_order = "u.Gender desc ";
			else if($ClassNumberMethod == "ByGirlsName")
				$gender_order = "u.Gender asc ";
				
			$sql = 'select 
								yuc.YearClassUserID, 
								yc.AcademicYearID, 
								yc.ClassTitleEN,
								u.UserID,
								u.EnglishName,
								u.Gender
							From 
								YEAR_CLASS_USER yuc 
								inner join 
								YEAR_CLASS yc 
								on yuc.YearClassID = yc.YearClassID 
								inner join 
								INTRANET_USER u 
								on yuc.UserID = u.UserID and yuc.YearClassID = \''.$YearClassID.'\' 
							Order By 
								'. $gender_order .', u.EnglishName asc';
			$Result = $this->returnArray($sql);
			
			for ($i=0; $i< sizeof($Result); $i++) {
				$sql = 'update YEAR_CLASS_USER set 
									ClassNumber = \''.($i+1).'\' 
								where 
									YearClassUserID = \''.$Result[$i]['YearClassUserID'].'\'';
				
				$ReturnValue['SetClassNumber'.$i] = $this->db_db_query($sql);
				
				if ($Result[$i]['AcademicYearID'] == $CurrentSchoolYearID) {
					$sql = 'update INTRANET_USER set 
										ClassNumber = \''.($i+1).'\', 
										ClassName = \''.$this->Get_Safe_Sql_Query($Result[$i]['ClassTitleEN']).'\' 
									where 
										UserID = \''.$Result[$i]['UserID'].'\'';
					$ReturnValue['SetClassNumberInIntranetUser'.$i] = $this->db_db_query($sql);
				}
			}
			
			return in_array(false,$ReturnValue)? false:true;
		}
	}
	
	// class number reset other than from create/ edit class form
	function Reset_Class_Numbers($YearClassID,$ParUserID=array(),$ClassNumber=array()) {
		global $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/libuser.php');
		$luser = new libuser();
		
		$YearClass = new year_class($YearClassID,false,false,false);		
		$YearClassAcademicYearID = $YearClass->AcademicYearID;
		$CurrentSchoolYearID = Get_Current_Academic_Year_ID();
		
		if ($YearClassAcademicYearID == $CurrentSchoolYearID) {
			$this->Clear_Class_Number($YearClassID);
		}
		
		$sql = 'update YEAR_CLASS set DateModified = now(), ModifyBy = \''.$_SESSION['UserID'].'\' where YearClassID = \''.$YearClassID.'\'';
		$Result['ClearClassNumberInYearClassUser'] = $this->db_db_query($sql);
		
		$sql = 'update YEAR_CLASS_USER set ClassNumber = NULL where YearClassID = \''.$YearClassID.'\'';
		$Result['ClearClassNumberInYearClassUser'] = $this->db_db_query($sql);
		
		$sql = 'select UserID from YEAR_CLASS_USER where YearClassID = \''.$YearClassID.'\'';
		$yearClassUserIdAry = $this->returnVector($sql);
		
		for ($i=0; $i< sizeof($ParUserID); $i++) {
			$sql = 'update YEAR_CLASS_USER set 
								ClassNumber = \''.$ClassNumber[$i].'\' 
							where 
								YearClassID = \''.$YearClassID.'\' 
								and 
								UserID = \''.$ParUserID[$i].'\'';
			$Result['SetClassNumberInYearClassUser:'.$ParUserID[$i]] = $this->db_db_query($sql);
			
			if ($YearClassAcademicYearID == $CurrentSchoolYearID)
			{
				$sql = 'update INTRANET_USER set 
								ClassName = \''.$this->Get_Safe_Sql_Query($YearClass->ClassTitleEN).'\',
								ClassNumber = \''.$ClassNumber[$i].'\' 
							where 
								UserID = \''.$ParUserID[$i].'\'';
				$Result['SetClassNumberInINTRANET_USER:'.$ParUserID[$i]] = $this->db_db_query($sql);
					
				## Update Student class_number in eclass			
				if(sizeof($ParUserID) > 0)
				{
					include_once($PATH_WRT_ROOT."includes/libeclass40.php");
					$lc = new libeclass();
					
				    $sql = "SELECT UserEmail, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID IN (".implode(',',$ParUserID).") ";
					$row = $this->returnArray($sql);
				
					for($j=0; $j<sizeof($row); $j++)
					{
						list($email,$class_name,$class_number) = $row[$j];
						
						if(trim($class_name) !='')
						{ 
					        $lc->eClassUserUpdateClassNameIP($email,$class_name,$class_number);
						}      
					}
				}
			}
		}
		
		if ($YearClassAcademicYearID == $CurrentSchoolYearID) {
			$Result['UpdateClassHistory'] = $this->Update_Class_History($YearClassID);
			
			$successAry['synClassUserDataToModules'] = $luser->synUserDataToModules($yearClassUserIdAry);
			$successAry['synUpdatedUserDataToModules'] = $luser->synUserDataToModules($ParUserID);
		}
		
		//debug_r($Result);
		return !in_array(false,$Result);
	}
	
	function Get_Class_List_By_Academic_Year($AcademicYearID, $YearID='', $TeachingOnly=0, $YearClassIDArr='') {
		
		if ($YearID != '') {
			$cond_YearID = " And y.YearID IN (".implode(",",(array)$YearID).") ";
		}
			
		if ($TeachingOnly == 1) {
			$Inner_Join_YEAR_CLASS_TEACHER = " Inner Join YEAR_CLASS_TEACHER as yct On (yc.YearClassID = yct.YearClassID And yct.UserID = '".$_SESSION['UserID']."') ";
		}
		
		if ($YearClassIDArr != '') {
			$cond_YearClassID = " And yc.YearClassID IN ('".implode("','", (array)$YearClassIDArr)."') ";
		}
		
		$sql= 'Select
					  y.YearID,
					  y.YearName,
					  yc.YearClassID,
					  yc.ClassTitleEN,
					  yc.ClassTitleB5,
					  y.WEBSAMSCode as Form_WebSAMSCode
					From
					  ACADEMIC_YEAR as ay
					  inner join
					  YEAR_CLASS as yc
					  on ay.AcademicYearID = yc.AcademicYearID and ay.AcademicYearID = \''.$AcademicYearID.'\'
					  inner join
					  YEAR as y
					  on y.YearID = yc.YearID
					  '.$Inner_Join_YEAR_CLASS_TEACHER.'
					Where
					  1
					  '.$cond_YearID.'
					  '.$cond_YearClassID.'
					order by
					  y.sequence, y.YearName, yc.sequence, yc.ClassTitleEN
					';
		return $this->returnArray($sql);
	}
	
	function Get_Academic_Year_Term_List($AcademicYearID, $NoPastTerm=0, $PastTermOnly=0, $YearTermID_Compare='', $IncludeYearTermIDArr='', $ExcludeYearTermIDArr='') {
		$cond_endTime = '';
		if ($NoPastTerm)
			$cond_endTime = ' AND NOW() <= TermEnd ';
			
		$cond_startTime = '';
		if ($PastTermOnly)
		{
			if ($YearTermID_Compare == '')
				$cond_startTime = ' AND NOW() > TermEnd ';
			else
			{
				$objTerm = new academic_year_term($YearTermID_Compare);
				$cond_startTime = ' AND \''.$objTerm->TermStart.'\' > TermEnd ';
			}
		}
		
		$cond_IncludeYearTermID = '';
		if ($IncludeYearTermIDArr != '')
			$cond_IncludeYearTermID = " And YearTermID In ('".implode("','", (array)$IncludeYearTermIDArr)."') ";
			
		$cond_ExcludeYearTermID = '';
		if ($ExcludeYearTermIDArr != '')
			$cond_ExcludeYearTermID = " And YearTermID Not In ('".implode("','", (array)$ExcludeYearTermIDArr)."') ";
		
		
		$sql = 'Select 
							YearTermID, 
							YearTermNameEN, 
							YearTermNameB5, 
							CASE 
								WHEN CURDATE() between TermStart and TermEnd 
								THEN \'1\'
								ELSE \'0\' 
							END as CurrentTerm 
						From 
							ACADEMIC_YEAR_TERM 
						where 
							AcademicYearID = \''.$AcademicYearID.'\'
							'.$cond_endTime.'
							'.$cond_startTime.'
							'.$cond_IncludeYearTermID.'
							'.$cond_ExcludeYearTermID.'
						order by 
							TermStart
						';
		return $this->returnArray($sql);
	}
	
	function Get_Previous_Academic_Year($AcademicYearID) {
		$this->With_Nolock_Trans();
		$sql = 'select
						  YearTermList.AcademicYearID
						From
						(select
						  ay.AcademicYearID,
						  max(ayt.TermEnd) as TermEnd
						From
						  ACADEMIC_YEAR as ay
						  inner join
						  ACADEMIC_YEAR_TERM as ayt
						  on ay.AcademicYearID = ayt.AcademicYearID and ay.AcademicYearID != \''.$AcademicYearID.'\'
						Group by
						  ay.AcademicYearID) as YearTermList,
						(select
						  ay.AcademicYearID,
						  min(ayt.TermStart) as TermStart,
						  max(ayt.TermEnd) as TermEnd
						From
						  ACADEMIC_YEAR as ay
						  inner join
						  ACADEMIC_YEAR_TERM as ayt
						  on ay.AcademicYearID = ayt.AcademicYearID
						where
						  ay.AcademicYearID = \''.$AcademicYearID.'\'
						Group by
						  ay.AcademicYearID) as CurrentYearPeriod
						where
						  YearTermList.TermEnd < CurrentYearPeriod.TermStart
						order by
						  YearTermList.TermEnd desc
						Limit 1';
		$Result = $this->returnVector($sql);
		return $Result[0];
	}
	
	function Get_Avaliable_Year_Class_User($YearClassID,$PrevYearClassID,$StudentSelected,$AcademicYearID) {
		if (sizeof($StudentSelected) > 0) {
			$SelectedID = implode(',',$StudentSelected);
			$SelectedCondition = ' AND ycu.UserID not in ('.$SelectedID.') ';
		}
		$sql = 'select
						  ycu.UserID
						From
						  YEAR_CLASS_USER as ycu 
						  inner join 
						  INTRANET_USER as u 
						  on ycu.UserID = u.UserID 
						Where
						  ycu.YearClassID = \''.$PrevYearClassID.'\'
						  and
						  ycu.UserID not in
						  (select
						    UserID
						  From
						    YEAR_CLASS as yc
						    inner join
						    YEAR_CLASS_USER as ycu
						    on yc.YearClassID = ycu.YearClassID and yc.AcademicYearID = \''.$AcademicYearID.'\' and yc.YearClassID != \''.$YearClassID.'\')
						  	'.$SelectedCondition.'
						order by
						  ycu.ClassNumber';
		return $this->returnArray($sql);
	}
	
	function Check_Year_Name($YearID,$YearName) {
		if (trim($YearName) != "") {
			$sql = 'select 
								count(1) 
							from 
								YEAR 
							Where 
								YearName = \''.$YearName.'\' 
								AND 
								YearID != \''.$YearID.'\'';
			$Result = $this->returnVector($sql);
			
			return ($Result[0] == 0);
		}
		else 
			return false;
	}
	
	function getCurrentAcademicaYearID(){
		$sql = "select
					AcademicYearID
				from
					ACADEMIC_YEAR_TERM
				where
					CURDATE() between TermStart and TermEnd";
				$arrResult = $this->returnVector($sql);
	
		if(sizeof($arrResult)>0)
			return $arrResult[0];
		else
			return 0;
	}
	
	function Check_Class_Title($ClassTitle,$Language,$YearClassID="",$AcademicYearID="") {
		if ($YearClassID != "") {
			$YearClassCondition = ' and YearClassID <> \''.$YearClassID.'\' ';
		}
		$sql = 'Select 
							count(1) 
						From 
							YEAR_CLASS 
						where 
							AcademicYearID = \''.$AcademicYearID.'\' 
							'.$YearClassCondition.' 
							and 
							ClassTitle'.$Language.' = \''.$this->Get_Safe_Sql_Query($ClassTitle).'\' 
							';
		//debug_r($sql); die;
		$Result = $this->returnVector($sql);					
		
		return ($Result[0] == 0);
	}
	
	function Create_ClassOnly($AcademicYearID,$YearID,$ClassTitleEN,$ClassTitleB5,$WEBSAMSCode,$ClassGroupID,$CopyClassStudents=0,$FromYearClassID=0,$CurriculumID='') 
	{
		$sql = 'select max(Sequence) 
						from 
							YEAR_CLASS 
						where 
							AcademicYearID = \''.$AcademicYearID.'\' 
							and 
							YearID = \''.$YearID.'\'';
		$MaxSequence = $this->returnVector($sql);
		$NextSequence = $MaxSequence[0]+1;
		
		// insert class record
		$sql = 'Insert into YEAR_CLASS (
							AcademicYearID, 
							YearID, 
							ClassTitleEN, 
							ClassTitleB5, 
							Sequence,
							DateInput, 
							InputBy, 
							WEBSAMSCode,
							ClassGroupID,
                            CurriculumID
							)
						value (
							\''.$AcademicYearID.'\',
							\''.$YearID.'\',
							\''.$this->Get_Safe_Sql_Query($ClassTitleEN).'\',
							\''.$this->Get_Safe_Sql_Query($ClassTitleB5).'\',
							\''.$NextSequence.'\',
							now(),
							\''.$_SESSION['UserID'].'\',
							\''.$this->Get_Safe_Sql_Query($WEBSAMSCode).'\',
							\''.$ClassGroupID.'\',
							\''.$CurriculumID.'\'
							)';
		$Result['InsertClass'] = $this->db_db_query($sql);
		
		if ($Result['InsertClass'])  
		{
			$YearClassID = $this->db_insert_id();
			
			
			#create eclass
		//if ($isCreateclass){
			/*$libeclass = new libeclass();
			$course_id = $libeclass->eClassAdd($WEBSAMSCode, $ClassTitleEN, '', 0, 30, $_SESSION['UserID'], '', false, $YearClassID, true);
			$lo = new libeclass($course_id);
			$iCal = new icalendar();
			$calID = $iCal->createSystemCalendar($ClassTitleEN,3,"P",'');
			$Result['create_Calendar_to_eclass'] = $calID;
			$sql = "Update course set CalID = '$calID' where course_id = '$course_id'";
			 $Result['create_Calendar_connection_to_eclass'] = $lo->db_db_query($sql);
			$sql = "update YEAR_CLASS set course_id = $course_id where YearClassID = $YearClassID";
			$Result['updateCourseID'] = $this->db_db_query($sql);
		//}
			*/
			
			// create class group
			$sql = 'select 
								GroupID 
							From 
								INTRANET_GROUP 
							where 
								Title = \''.$this->Get_Safe_Sql_Query($ClassTitleEN).'\' 
								and 
								AcademicYearID = \''.$AcademicYearID.'\'';
			$Temp = $this->returnVector($sql);
			if (sizeof($Temp) > 0) {
				$GroupID = $Temp[0];
				
				$sql = 'update INTRANET_GROUP set 
									RecordType = 3 
								where 
									GroupID = \''.$GroupID.'\'';
				$Result['UpdateIntranetGroup'] = $this->db_db_query($sql) or die(mysql_error());
			}
			else {
				$sql = "INSERT INTO INTRANET_GROUP 
									(
									Title,
									TitleChinese,
									RecordType,
									StorageQuota,
									DateInput,
									DateModified, 
									AcademicYearID
									) 
								VALUES 
									(
									'".$this->Get_Safe_Sql_Query($ClassTitleEN)."',
									'".$this->Get_Safe_Sql_Query($ClassTitleB5)."',
									3,
									5,
									now(),
									now(),
									'".$AcademicYearID."'
									)";
		    $Result['InsertIntranetGroup'] = $this->db_db_query($sql);
		    
		    $GroupID = $this->db_insert_id();
		  }
	    
		    if ($Result['UpdateIntranetGroup'] || $Result['InsertIntranetGroup']) {
			    $sql = 'update YEAR_CLASS set 
		    						GroupID = \''.$GroupID.'\' 
		    					where 
		    						YearClassID = \''.$YearClassID.'\'';
		    	$Result['UpdateGroupIDToClass'] = $this->db_db_query($sql);
			}
			
			# insert class Calendar
			include_once("icalendar.php"); 
			$iCal = new icalendar();
			$cal = $this->Get_CalendarID_By_GroupID($GroupID);
			/*
			$sql = "select CalID from INTRANET_GROUP where GroupID = '$GroupID'";
			$cal = $this->returnVector($sql);
			*/
			
			// debug_r($cal);
			// exit;
			if (empty($cal)||!is_numeric($cal)){
			// echo "Create Class";
				$Result["Create_class_cal"] = $iCal->createSystemCalendar($ClassTitleEN, 2);
				if ($Result["Create_class_cal"])
					$cal = $Result["Create_class_cal"];
				$sql = "update INTRANET_GROUP set CalID = '$cal' where GroupID = '$GroupID'";
				$Result["Create_update_group_cal"] = $this->db_db_query($sql);
			}
			
			if($CopyClassStudents){
				// copy class student 
				# Found out the default Role ID
				$RoleID = $this->Get_Default_Class_Group_RoleID();
				
				$sql = "SELECT UserID,ClassNumber FROM YEAR_CLASS_USER WHERE YearClassID='$FromYearClassID'";
				$student_ary = $this->returnResultSet($sql);
				
				for ($i=0; $i< sizeof($student_ary); $i++) {
					$sql = 'Insert into YEAR_CLASS_USER (
										YearClassID, 
										UserID, 
										ClassNumber,
										DateInput, 
										InputBy
										)
									Value 
										(
										\''.$YearClassID.'\',
										\''.$student_ary[$i]['UserID'].'\',
										\''.$student_ary[$i]['ClassNumber'].'\',
										now(),
										\''.$_SESSION['UserID'].'\'
										)';
					$Result['InsertClassStudent'.$i] = $this->db_db_query($sql);
					
					# Add to Group
					$Result['InsertUserToGroup'.$i] = $this->Add_User_To_Group($GroupID, $student_ary[$i]['UserID'], $RoleID);
				}
			}
		}
	}
	
	/*
	 *	If you change the edit class logic in this function,
	 *	Please check if you have to change the logic of the following functions also
	 *		1. Edit_Class()
	 */
	function Edit_ClassOnly($YearClassID,$ClassTitleEN,$ClassTitleB5,$WEBSAMSCode,$ClassGroupID,$CurriculumID='') 
	{
		$YearClass = new year_class($YearClassID,false,false,false);
		
		// insert class record
		$sql = 'update YEAR_CLASS set 
							ClassTitleEN = \''.$this->Get_Safe_Sql_Query($ClassTitleEN).'\', 
							ClassTitleB5 = \''.$this->Get_Safe_Sql_Query($ClassTitleB5).'\', 
							WEBSAMSCode = \''.$this->Get_Safe_Sql_Query($WEBSAMSCode).'\', 
							ClassGroupID = \''.$this->Get_Safe_Sql_Query($ClassGroupID).'\', 
							CurriculumID = \''.$this->Get_Safe_Sql_Query($CurriculumID).'\', 
							ModifyBy = \''.$_SESSION['UserID'].'\'
						Where 
							YearClassID = \''.$YearClassID.'\'';
		$Result['UpdateClass'] = $this->db_db_query($sql);
		
		/*global $eclass_db;
		$has_eclass = $YearClass->has_eclass();
		# update course title
		if ($has_eclass){
			$sql = "update {$eclass_db}.course 
					set course_name = '$ClassTitleEN',
						course_code =  '$WEBSAMSCode'
					where course_id in (
						select course_id from YEAR_CLASS where 
						YearClassID = $YearClassID
					)
					";
			$this->db_db_query($sql);
		}*/
		
		// update class group
		$sql = 'select 
							GroupID 
						From 
							INTRANET_GROUP 
						where 
							Title = \''.$this->Get_Safe_Sql_Query($YearClass->ClassTitleEN).'\' 
							and 
							AcademicYearID = \''.$YearClass->AcademicYearID.'\'';
		$Temp = $this->returnVector($sql);
		
		if (sizeof($Temp) > 0) {
			$GroupID = $Temp[0];
			
			$sql = 'update INTRANET_GROUP set 
								Title = \''.$this->Get_Safe_Sql_Query($ClassTitleEN).'\', 
								TitleChinese = \''.$this->Get_Safe_Sql_Query($ClassTitleB5).'\', 
								RecordType = 3 
							where 
								GroupID = \''.$GroupID.'\'';
			$Result['UpdateIntranetGroup'] = $this->db_db_query($sql);
			if (!empty($GroupID) && is_numeric($GroupID)){
				$calID = $this->Get_CalendarID_By_GroupID($GroupID);
				/*
				$sql = "select CalID from INTRANET_GROUP 
						where GroupID = '$GroupID'";
				$calID = $this->returnVector($sql);
				*/
				$sql = "update CALENDAR_CALENDAR 
						set Name='".$this->Get_Safe_Sql_Query($ClassTitleEN)."'
						where CalID = '$calID'";
				$Result['Update_calendar'] = $this->db_db_query($sql);
			}
		}
	}
	
	function Is_ClassName_Valid($ClassName)
	{
		$ValidArr = array();
		
		$ValidArr['HasDoubleQuote'] = (strpos($ClassName, '"') > 0)? true : false;
		$ValidArr['HasSingleQuote'] = (strpos($ClassName, "'") > 0)? true : false;
		
		if (in_array(true, $ValidArr))
			return false;
		else
			return true;
	}
	
	function Is_ClassName_Exist($ClassName, $AcademicYearID='')
	{
		if ($AcademicYearID == '')
			$AcademicYearID = Get_Current_Academic_Year_ID();
			
		$sql = "Select 
						YearClassID 
				From 
						YEAR_CLASS 
				Where 
						ClassTitleEN = '".$ClassName."'
						And 
						AcademicYearID = '".$AcademicYearID."'
				";
		$resultSet = $this->returnVector($sql);
		
		if ($resultSet[0] == '')
			return false;
		else
			return true;
	}
	
	function Is_FormName_Exist($FormName)
	{
		$sql = "Select 
						YearID 
				From 
						YEAR 
				Where 
						YearName = '".$FormName."'
				";
		$resultSet = $this->returnVector($sql);
		
		if ($resultSet[0] == '')
			return false;
		else
			return true;
	}
	
	function Get_Student_Studying_Class($StudentID, $AcademicYearID='')
	{
		if ($AcademicYearID == '')
			$AcademicYearID = Get_Current_Academic_Year_ID();
		
		$sql = "SELECT
						yc.YearClassID
				FROM
						YEAR_CLASS_USER as ycu
						INNER JOIN
						YEAR_CLASS as yc
				WHERE
						ycu.YearClassID = yc.YearClassID 
					AND 
						yc.AcademicYearID = '".$AcademicYearID."'
					AND
						ycu.UserID = '".$StudentID."'
				";
		$resultSet = $this->returnVector($sql);
		
		return $resultSet[0];
	}
	
	/*
	 *	If you change the edit class logic in this function,
	 *	Please check if you have to change the logic of the following functions also
	 *		1. Edit_Class()
	 */
	function Add_Student_In_Class($YearClassID, $StudentID, $ClassNumber='', $AcademicYearID='') {
		global $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/libuser.php');
		$luser = new libuser();
		
		$ObjYearClass = new year_class($YearClassID, false, false, true);
		$ClassTitleEN = $ObjYearClass->ClassTitleEN;
		$GroupID = $ObjYearClass->GroupID;
		
		if ($AcademicYearID == '')
			$AcademicYearID = Get_Current_Academic_Year_ID();
		
		# Get Class number if not specified
		if ($ClassNumber == '')
			$ClassNumber = $ObjYearClass->Get_Max_ClassNumber() + 1;
			
		$IsCurrentAcademicYearEdit = ($AcademicYearID == Get_Current_Academic_Year_ID());
			
		# Found out the default RoleID of Group
		$RoleID = $this->Get_Default_Class_Group_RoleID();
		
		# check if the student has been assigned to class of current year first
		$oldYearClassID = $this->Get_Student_Studying_Class($StudentID, $AcademicYearID);
		
		$this->Start_Trans();
		
		if ($oldYearClassID > 0)
		{
			# update
			$ObjOldYearClass = new year_class($oldYearClassID, false, false, true);
			$oldGroupID = $ObjOldYearClass->GroupID;
			
			# update class info
			$sql = 'Update 
							YEAR_CLASS_USER
					Set
							YearClassID = \''.$YearClassID.'\',
							ClassNumber = \''.$ClassNumber.'\'
					Where
							UserID = \''.$StudentID.'\'
						And
							YearClassID = \''.$oldYearClassID.'\'
					';
			$Result['UpdateClassStudent'] = $this->db_db_query($sql);
			
			# update group info
			$sql = 'Update 
							INTRANET_USERGROUP
					Set
							GroupID = \''.$GroupID.'\',
							RoleID = \''.$RoleID.'\'
					Where
							UserID = \''.$StudentID.'\'
						And
							GroupID = \''.$oldGroupID.'\'
					';
			$Result['UpdateStudentToGroup'] = $this->db_db_query($sql);
			
			# Remove old group's iCalendar viewer
			if (!empty($oldGroupID) && is_numeric($oldGroupID)){
				$calID = $this->Get_CalendarID_By_GroupID($oldGroupID);
				/*
				$sql = "select CalID from INTRANET_GROUP 
						where GroupID = '$oldGroupID'";
				$cal = $this->returnVector($sql);
				*/
				if (!empty($calID) && is_numeric($calID)){
					//$calID = $cal[0];
					include_once("icalendar.php"); 
					$ObjOld_iCal = new icalendar();
					$Result['DeleteOldGroup_iCalendarViewer'] = $ObjOld_iCal->removeCalendarViewer($calID, $StudentID);
				}
			}
		}
		else
		{
			# insert
			
			# Add student to class
			$sql = 'Insert into YEAR_CLASS_USER (
										YearClassID,
										ClassNumber, 
										UserID, 
										DateInput, 
										InputBy
										)
									Value 
										(
										\''.$YearClassID.'\',
										\''.$ClassNumber.'\',
										\''.$StudentID.'\',
										now(),
										\''.$_SESSION['UserID'].'\'
										)';
			$Result['InsertClassStudent'] = $this->db_db_query($sql);
			
			# Add student to group
			$Result['InsertStudentToGroup'] = $this->Add_User_To_Group($GroupID, $StudentID, $RoleID);
		}
		
		## Update field in INTRANET_USER if it is a current school year
		if ($IsCurrentAcademicYearEdit)
		{
			$sql = 'update INTRANET_USER set 
								ClassNumber = \''.$ClassNumber.'\', 
								ClassName = \''.$this->Get_Safe_Sql_Query($ClassTitleEN).'\' 
							where 
								UserID = \''.$StudentID.'\'';
			$Result['SetClassNumberInIntranetUser'] = $this->db_db_query($sql);
			
			$Result['UpdateClassHistory'] = $this->Update_Class_History($YearClassID);
			
			if ($oldYearClassID > 0) {
				$Result['UpdateClassHistoryForOldClass'] = $this->Update_Class_History($oldYearClassID);
			}
			
			$Result['synUserDataToModules'] = $luser->synUserDataToModules($StudentID);
		}
		
		
		# Add iCanlendar Viewer for the student in the new group 
		if (!empty($GroupID) && is_numeric($GroupID)){
			/*
			$sql = "select CalID from INTRANET_GROUP 
					where GroupID = '$GroupID'";
			$cal = $this->returnVector($sql);
			*/
			$calID = $this->Get_CalendarID_By_GroupID($GroupID);
			
			if (!empty($calID) && is_numeric($calID)){
				//$calID = $cal[0];
				include_once("icalendar.php"); 
				$iCal = new icalendar();
				
				$Result['InsertStudentToiCalendar'] = $iCal->insertCalendarViewer($calID, $StudentID, 'R', $GroupID, 'E');
			}
		}
		
		if (in_array(false, $Result))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
	}
	
	function Delete_Student_From_Class($YearClassID, $StudentIDArr)
	{
		$ObjYearClass = new year_class($YearClassID,false,true,false);
		$AcademicYearID = $ObjYearClass->AcademicYearID;
		$GroupID = $ObjYearClass->GroupID;
		
		$StudentIDList = implode(',', (array)$StudentIDArr);
		$IsCurrentAcademicYearEdit = ($AcademicYearID == Get_Current_Academic_Year_ID());
		
		// Delete from Class
		$sql = 'delete from YEAR_CLASS_USER where YearClassID = \''.$YearClassID.'\' and UserID In ('.$StudentIDList.')';
		$Result['DeleteClassStudent'] = $this->db_db_query($sql);
		
		if ($IsCurrentAcademicYearEdit)
		{
			// intranet user
			$sql = 'update INTRANET_USER set 
								ClassNumber = Null, ClassName = Null 
							where 
								UserID In ('.$StudentIDList.')';
			$Result['SetNull_INTRANET_USER'] = $this->db_db_query($sql);
			
			// class history
			$Result['UpdateClassHistory'] = $this->Update_Class_History($YearClassID);
		}
		
		// Delete from Class Group
		$sql = " delete from INTRANET_USERGROUP 
						where 	UserID In ($StudentIDList)
								And
								GroupID = '".$GroupID."'
				";
		$Result['DeleteClassStudentFromGroup'] = $this->db_db_query($sql);
		
		
		// Delete from iCalendar
		if (!empty($GroupID) && is_numeric($GroupID)){
			$calID = $this->Get_CalendarID_By_GroupID($GroupID);
			if (!empty($calID) && is_numeric($calID)){
				//$calID = $cal[0];
				
				$sql = "
						 delete from CALENDAR_CALENDAR_VIEWER 
								where
										CalID = '$calID' and 
										UserID In ($StudentIDList) and 
										GroupType = 'T'
						";
				$Result['DeleteStudentFromICalendar'] = $this->db_db_query($sql);
			}
		}
		
		return $Result;
	}
	
	/*
	 *	If you change the edit class logic in this function,
	 *	Please check if you have to change the logic of the following functions also
	 *		1. Edit_Class()
	 */
	function Delete_Class_Teacher($YearClassID, $TeacherIDArr)
	{
		$ObjYearClass = new year_class($YearClassID,false,true,false);
		$AcademicYearID = $ObjYearClass->AcademicYearID;
		$GroupID = $ObjYearClass->GroupID;
		
		$TeacherIDList = implode(',', (array)$TeacherIDArr);
		
		// Delete from Class
		$sql = 'delete from YEAR_CLASS_TEACHER where YearClassID = \''.$YearClassID.'\' and UserID In ('.$TeacherIDList.')';
		$Result['DeleteClassTeacher'] = $this->db_db_query($sql);
		
		// Delete from Class Group
		$sql = " delete from INTRANET_USERGROUP 
						where 	UserID In ($TeacherIDList)
								And
								GroupID = '".$GroupID."'
				";
		$Result['DeleteClassTeacherFromGroup'] = $this->db_db_query($sql);
		
		
		//if (count($removeUser) > 0){
			/*global $eclass_db;
			$iCal = new icalendar();
			$sql = "select course_id from YEAR_CLASS where YearClassID = $YearClassID ";
			$resultSet = $this->returnVector($sql);
			$course_id = $resultSet[0];
			if (!empty($course_id )){
				$lo = new libeclass($course_id);
				$sql = "select CalID from {$eclass_db}.course where course_id = '$course_id'";
				$calID = $this->returnVector($sql);
				$Result['remove_calendar_viewer'] = $iCal->removeCalendarViewer($calID[0], $TeacherID);
				$sql = "select user_id from {$eclass_db}.user_course as c
				inner join INTRANET_USER as u on
					c.user_email = u.UserEmail
					where 
					c.course_id = '$course_id' and 
					u.UserID = $TeacherID
					";
				$user_id = $iCal->returnVector($sql); 
				###
				$lo->eClassUserDel($user_id);
			}*/
		//}
		
		
		// Delete from iCalendar
		if (!empty($GroupID) && is_numeric($GroupID)){
			/*
			$sql = "select CalID from INTRANET_GROUP 
					where GroupID = '$GroupID'";
			$cal = $this->returnVector($sql);
			*/
			$calID = $this->Get_CalendarID_By_GroupID($GroupID);
			if (!empty($calID) && is_numeric($calID)){
				//$calID = $cal[0];
				
				$sql = "
						 delete from CALENDAR_CALENDAR_VIEWER 
								where
										CalID = '$calID' and 
										UserID In ($TeacherIDList) and 
										GroupType = 'T'
						";
				$Result['Remove_non-existance_Teacher'] = $this->db_db_query($sql);
			}
		}
		
		return $Result;
	}
	
	/*
	 *	If you change the edit class logic in this function,
	 *	Please check if you have to change the logic of the following functions also
	 *		1. Edit_Class()
	 */
	function Add_Class_Teacher($YearClassID, $TeacherID)
	{
		$ObjYearClass = new year_class($YearClassID,false,true,false);
		$AcademicYearID = $ObjYearClass->AcademicYearID;
		$GroupID = $ObjYearClass->GroupID;
		
		# Found out the default Role ID
		$RoleID = $this->Get_Default_Class_Group_RoleID();
		
		# Add to Class
		$sql = 'Insert into YEAR_CLASS_TEACHER (
							YearClassID, 
							UserID, 
							DateInput, 
							InputBy
							)
						Values
							(
							\''.$YearClassID.'\',
							\''.$TeacherID.'\',
							now(),
							\''.$_SESSION['UserID'].'\'
							)
						ON DUPLICATE KEY UPDATE 
							DateModified = NOW()
				';
		$Result['InsertClassTeacher'] = $this->db_db_query($sql);
			
		# Add to Group
		$Result['InsertClassTeacherToGroup'] = $this->Add_User_To_Group($GroupID, $TeacherID, '');
		
		# Add to iCalendar
		 // echo "Group ID:".$GroupID;
		if (!empty($GroupID) && is_numeric($GroupID)){
			/*
			$sql = "select CalID from INTRANET_GROUP 
					where GroupID = '$GroupID'";
			$cal = $this->returnVector($sql);
			*/
			$calID = $this->Get_CalendarID_By_GroupID($GroupID);
			if (!empty($calID) && is_numeric($calID)){
				//$calID = $cal[0];
				include_once("icalendar.php"); 
				$iCal = new icalendar();
				
				#insert new viewer
				$Result['insert_new_teacher']=$iCal->insertCalendarViewer($calID, $TeacherID, 'W', $GroupID, 'T');
			}
		}
		
		return $Result;
	}
	
	# $UserID can be a single ID or an array of IDs
	function Add_User_To_Group($GroupID, $AddUserID='', $RoleID='')
	{
		$valueArr = array();
		
		$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '$GroupID'";
		$ExistUserIDArr = $this->returnVector($sql);
		
		$AddUserID = (array)$AddUserID;

		$numOfUserID = count($AddUserID);
		for ($i=0; $i<$numOfUserID; $i++)
		{
			$thisUserID = $AddUserID[$i];
			if(in_array($thisUserID,$ExistUserIDArr))
			{
				$sql = "UPDATE INTRANET_USERGROUP SET DateModified = NOW() WHERE GroupID = '".$GroupID."' AND UserID='".$thisUserID."' ";
				$this->db_db_query($sql);
			}
			else
			{
				$insertValueArr[] = " ('".$GroupID."', '".$thisUserID."', '".$RoleID."', NOW() ) ";
			}
		}

		if(count($insertValueArr)>0)
		{
			$valueList = implode(',', $insertValueArr);
			
			# Add to Group
			$sql = "INSERT INTO INTRANET_USERGROUP 
							(GroupID, UserID, RoleID, DateInput) 
						VALUES
							$valueList
						ON DUPLICATE KEY UPDATE 
							DateModified = NOW()";
			$Success = $this->db_db_query($sql);

		}
		else
			$Success = true;
		return $Success;
	}
	
	function Get_Default_Class_Group_RoleID()
	{
		# Found out the default Role ID
		include_once("libgroupcategory.php");
		$lgc = new libgroupcategory();
		$RoleID = $lgc->returnGroupCategoryDefaultRoleID(3) + 0;	# 3 = Class
		
		return $RoleID;
	}
	
	function Insert_Student_Into_Class($YearClassID, $StudentID, $ClassNumber)
	{
		$sql = "Insert into YEAR_CLASS_USER 
								(
								YearClassID, 
								UserID, 
								DateInput, 
								InputBy, 
								ClassNumber
								)
							Values 
								(
								'".$YearClassID."',
								'".$StudentID."',
								now(),
								'".$_SESSION['UserID']."',
								'".$ClassNumber."'
								)
								";
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	function Get_UserName_By_UserID($UserID)
	{
		$nameField = getNameFieldByLang();
		$sql = 'Select '.$nameField.' From INTRANET_USER Where UserID = \''.$UserID.'\' ';
		$nameArr = $this->returnVector($sql);
		$name = $nameArr[0];
		
		return $name;
	}
	
	function Get_Student_Class_Info_In_AcademicYear($StudentIDArr, $AcademicYearID)
	{
		if (count((array)$StudentIDArr) > 0)
		{
			$StudentIDList = implode(',', (array)$StudentIDArr);
			
			$sql = "SELECT 
							DISTINCT(ycu.UserID) as UserID,
							yc.ClassTitleEN,
							yc.ClassTitleB5,
							ycu.ClassNumber,
							yc.YearClassID,
							yc.YearID,
							iu.ChineseName,
							iu.EnglishName,
							iu.Gender,
							iu.DateOfBirth, 
							iu.STRN,
							iu.HKID
					FROM 
							YEAR_CLASS_USER as ycu
							INNER JOIN 
							YEAR_CLASS as yc
							ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$AcademicYearID."')
							Inner Join
							INTRANET_USER as iu On (ycu.UserID = iu.UserID)
					WHERE 
							ycu.UserID In (".$StudentIDList.")
					ORDER BY 
							yc.Sequence,
							ycu.ClassNumber 
				";
		
			$returnArr = $this->returnArray($sql);
		}
		else
			$returnArr = array();
			
		return $returnArr;
	}
	
	function Is_ClassNumber_Used_By_ClassName($ClassNumber, $ClassNameEn, $AcademicYearID)
	{
		$sql = "Select
						Distinct(ycu.UserID)
				From
						YEAR_CLASS_USER as ycu
						Inner Join
						YEAR_CLASS as yc
						On (ycu.YearClassID = yc.YearClassID)
				Where
						yc.ClassTitleEN = '".$this->Get_Safe_Sql_Query($ClassNameEn)."'
						And
						ycu.ClassNumber = '".$ClassNumber."'
						And
						yc.AcademicYearID = '".$AcademicYearID."'
				";
		$StudentArr = $this->returnVector($sql);
		
		if (count($StudentArr) > 0)
			return true;
		else
			return false;
	}
	
	function Get_CalendarID_By_GroupID($GroupID)
	{
		$sql = "select CalID from INTRANET_GROUP where GroupID = '$GroupID'";
		$CalArr = $this->returnVector($sql);
		$CalID = $CalArr[0];
		
		return $CalID;
	}

	function Get_All_Year_Class($AcademicYearID='')
	{
		if ($AcademicYearID=='')
			$AcademicYearID = Get_Current_Academic_Year_ID();
			
		$ClassNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
		$sql = "Select
						yc.YearClassID,
						$ClassNameField
				From
						YEAR_CLASS as yc
						Inner Join
						YEAR as y
						On (yc.YearID = y.YearID)
				Where
						yc.AcademicYearID = '".$AcademicYearID."'
				Order By 
						y.Sequence, yc.Sequence
				";
		$returnArr = $this->returnArray($sql);
		
		return $returnArr;
	}
	
	function Get_Student_Without_Class($AcademicYearID='', $ExcludeStudentIDArr=array())
	{
		if ($AcademicYearID=='')
			$AcademicYearID = Get_Current_Academic_Year_ID();
			
		$conds_exclude_studentid = '';
		if (is_array($ExcludeStudentIDArr) && count($ExcludeStudentIDArr) > 0)
		{
			$exclude_student_list = implode(',', $ExcludeStudentIDArr);
			$conds_exclude_studentid = " And iu.UserID Not In ($exclude_student_list) ";
		}
			
		$nameField = getNameFieldByLang('iu.');
		$sql = "Select
						iu.UserID,
						$nameField as StudentName,
						iu.UserLogin
				From
						INTRANET_USER as iu
				Where
						iu.RecordType = 2
						And
						iu.RecordStatus = 1
						And
						iu.UserID Not In
						( 	Select
									ycu.UserID
							From
									YEAR_CLASS_USER as ycu
									Inner Join
									YEAR_CLASS as yc
									On (ycu.YearClassID = yc.YearClassID)
							Where
									yc.AcademicYearID = '".$AcademicYearID."'
						)
						$conds_exclude_studentid
				Order By
						iu.EnglishName
				";
		$returnArr = $this->returnArray($sql);
		return $returnArr;
	}
	
	function Get_Student_By_Class($YearClassIDArr, $activeOnly = false) {
		
		if ($YearClassIDArr != '')
		{
			if (!is_array($YearClassIDArr))
				$YearClassIDArr = array($YearClassIDArr);
			$cond_YearClassID = " And yc.YearClassID In (".implode(',', $YearClassIDArr).") ";
		}
		
		$NameField = getNameFieldByLang2('u.');
		$ArchiveNameField = getNameFieldByLang2('au.');
		
		// 2020-07-22 (Philips) [#L188901] - not display departed student for $sys_custom['SDAS']['StudentPerformanceTracking']['ShowActiveOnly']
		if($activeOnly){
			$cond_ActiveOnly = " AND au.UserID IS NULL AND u.RecordStatus <> '3' ";
		}
		
		$sql = "Select 
						ycu.UserID, 
						ycu.ClassNumber,
						CASE 
							WHEN au.UserID IS NOT NULL then CONCAT('<font style=\"color:red;\">*</font>', $ArchiveNameField) 
							WHEN u.RecordStatus = 3  THEN CONCAT('<font style=\"color:red;\">*</font>', $NameField) 
							ELSE $NameField 
						END as StudentName, 
						CASE 
							WHEN au.UserID IS NOT NULL then '1' 
							ELSE '0'
						END as ArchiveUser,
						yc.ClassTitleEN,
						yc.ClassTitleB5,
						yc.YearClassID,
						IF(u.UserID IS NOT NULL,u.UserLogin,au.UserLogin) as UserLogin 
				From 
						YEAR_CLASS_USER as ycu
						Inner Join YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
						Inner Join YEAR as y On (yc.YearID = y.YearID)
						LEFT JOIN INTRANET_USER as u ON (ycu.UserID = u.UserID)
						LEFT JOIN INTRANET_ARCHIVE_USER as au ON (ycu.UserID = au.UserID) 
				Where 
						1
						$cond_YearClassID
						$cond_ActiveOnly
				Order by 
						y.Sequence, yc.Sequence, ycu.ClassNumber
				";
		return $this->returnArray($sql);
	}
	
	function Update_Class_History($YearClassID)
	{
		$SuccessArr = array();
		
		### Get Class Info
		$objClass = new year_class($YearClassID, $GetYearDetail=false, $GetClassTeacherList=false,$GetClassStudentList=true);
		$ClassAcademicYearID = $objClass->AcademicYearID;
		$className = $this->Get_Safe_Sql_Query($objClass->ClassTitleEN);	// always transfer English title to Class History
		
		
		### Ignore class history update for future class (If client update future class in current year, client may see the future class records in the Class History)
		### Update of class history of future class will be done in the schedule task
		$ObjClassAcademicYear = new academic_year($ClassAcademicYearID);
		if ($ObjClassAcademicYear->Is_Future_Academic_Year())
			return true;
		
		
		### Get Academic Year Info
		$AcademicYearID = $objClass->AcademicYearID;
		$objAcademicYear = new academic_year($AcademicYearID);
		$academicYearName = $this->Get_Safe_Sql_Query($objAcademicYear->YearNameEN);
		
		### Get Class Student
		$CurStudentInfoArr = $objClass->ClassStudentList;
		$CurStudentIDArr = Get_Array_By_Key($CurStudentInfoArr, 'UserID');
		$numOfStudent = count($CurStudentIDArr);
		
		### Get Current records in class history to deleted the records of the removed student
		$sql = "Select UserID From PROFILE_CLASS_HISTORY Where AcademicYearID = '$AcademicYearID' And YearClassID = '$YearClassID'";
		$PastStudentIDArr = $this->returnVector($sql);
		$numOfPastStudent = count($PastStudentIDArr);
		
		### Delete removed student in Class History
		if ($numOfPastStudent > 0)
		{
			$DeletedStudentIDArr = array_diff($PastStudentIDArr, $CurStudentIDArr);
			$DeletedStudentIDArr = array_values($DeletedStudentIDArr);
			$numOfDeletedStudent = count($DeletedStudentIDArr);
			
			if ($numOfDeletedStudent > 0)
			{
				$DeletedStudentIDList = implode(',', $DeletedStudentIDArr);
				$sql = "Delete From PROFILE_CLASS_HISTORY Where AcademicYearID = '$AcademicYearID' And YearClassID = '$YearClassID' And UserID In (".$DeletedStudentIDList.")";
				$SuccessArr['DeleteRemovedStudentInClassHistory'] = $this->db_db_query($sql);
			}
		}		
		
		for ($i=0; $i<$numOfStudent; $i++)
		{
			$thisStudentID 		= $CurStudentInfoArr[$i]['UserID'];
			$thisClassNumber	= $CurStudentInfoArr[$i]['ClassNumber'];
			
			$sql = "Select RecordID From PROFILE_CLASS_HISTORY Where UserID = '$thisStudentID' And AcademicYearID = '$AcademicYearID'";
			$thisResultArr = $this->returnArray($sql);
			$thisRecordID = $thisResultArr[0]['RecordID'];
			
			if ($thisRecordID != '')
			{
				# update
				$sql = "Update 
							PROFILE_CLASS_HISTORY 
						Set
							ClassName = '$className', ClassNumber = '$thisClassNumber', DateModified = now(), YearClassID = '$YearClassID', AcademicYear = '$academicYearName', AcademicYearID = '$AcademicYearID'
						Where
							RecordID = '$thisRecordID'
						";
				$SuccessArr['Update'][$thisStudentID] = $this->db_db_query($sql);
			}
			else
			{
				# insert
				$sql = "Insert Into PROFILE_CLASS_HISTORY
							(UserID, ClassName, ClassNumber, AcademicYear, DateInput, DateModified, YearClassID, AcademicYearID)
						Values
							('$thisStudentID', '$className', '$thisClassNumber', '$academicYearName', now(), now(), '$YearClassID', '$AcademicYearID')
						";
				$SuccessArr['Insert'][$thisStudentID] = $this->db_db_query($sql);
			}
		}
		
		if (in_array(false, $SuccessArr))
			return false;
		else
			return true;
	}
	
	function Get_Student_By_Form($AcademicYearID='', $YearIDArr='', $YearClassIDArr='')
	{
		if ($AcademicYearID=='')
			$AcademicYearID = Get_Current_Academic_Year_ID();
			
		if ($YearIDArr != '')
		{
			if (!is_array($YearIDArr))
				$YearIDArr = array($YearIDArr);
			$conds_YearID = " And yc.YearID In (".implode(',', $YearIDArr).") ";
		}
		else
		{
			$conds_YearID = " And yc.YearID = '".$this->YearID."' ";
		}
		
		if ($YearClassIDArr != '') {
			$conds_YearClassID = " And yc.YearClassID In ('".implode("','", (array)$YearClassIDArr)."') ";
		}
			
		$sql = "Select
						ycu.UserID, ycu.YearClassID, ycu.ClassNumber
				From
						YEAR_CLASS_USER as ycu
						Inner Join
						YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
				Where
						yc.AcademicYearID = '".$AcademicYearID."'
						$conds_YearID
						$conds_YearClassID
				";
		$ResultArr = $this->returnArray($sql);
		return $ResultArr;
	}
	
	function Get_Active_Student_List($YearID='',$YearClassID='',$AcademicYearID='')
	{
		if($YearID)
			$cond_YearID = " AND yc.YearID IN (".implode(",",(array)$YearID).")";
			
		if($YearClassID)
			$cond_YearClassID = " AND yc.YearClassID IN (".implode(",",(array)$YearClassID).")";
		
		if (trim($AcademicYearID)=='')
			$AcademicYearID = Get_Current_Academic_Year_ID();
			
		$ClassTitleLang = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN");
		
		$NameField = getNameFieldByLang2('iu.');
		$NameFieldWithClassNumber = getNameFieldWithClassNumberByLang('iu.');
			
		$sql = "
			SELECT
				ycu.UserID, 
				$NameField NameField,
				$NameFieldWithClassNumber NameFieldWithClassNo, 
				ycu.ClassNumber,
				yc.ClassTitleEN,
				yc.ClassTitleB5,
				$ClassTitleLang ClassTitle,
				yc.YearClassID,
				y.YearID,
				y.YearName				
			FROM
				YEAR y
				INNER JOIN YEAR_CLASS yc ON y.YearID = yc.YearID
				INNER JOIN YEAR_CLASS_USER ycu On ycu.YearClassID = yc.YearClassID
				INNER JOIN INTRANET_USER iu ON iu.UserID = ycu.UserID 
			WHERE
				iu.RecordStatus = 1
				AND iu.ClassName <> ''
				AND iu.ClassNumber <> ''
				AND iu.ClassName IS NOT NULL
				AND iu.ClassNumber IS NOT NULL
				AND yc.AcademicYearID = '".$AcademicYearID."'
				$cond_YearID
				$cond_YearClassID
			ORDER BY
				y.Sequence,
				yc.Sequence,
				iu.ClassNumber
		";	
// 		debug_pr($sql);
		/*
		iu.ClassName,
				iu.ClassNumber
				*/
		
		$ResultArr = $this->returnArray($sql,0,1);
		 
		return $ResultArr;
	}
	
	function Get_Class_Teacher_Class($ParUserID)
	{
		$AcademicYearID = Get_Current_Academic_Year_ID();
		
	 	$sql = "SELECT
	 					yc.YearClassID as ClassID,
	 					yc.ClassTitleEn as ClassName,
	 					y.YearID as ID,
						yc.GroupID
	 			FROM
	 					YEAR_CLASS_TEACHER as yct
	 					INNER JOIN
	 					YEAR_CLASS as yc
	 					ON (yct.YearClassID = yc.YearClassID AND yct.UserID = '$ParUserID' AND yc.AcademicYearID = '".$AcademicYearID."')
	 					INNER JOIN
	 					YEAR as y
	 					ON (yc.YearID = y.YearID)
	 			Order By
						y.Sequence, yc.Sequence
	 			";
	 			
		return $this->returnArray($sql);
	}
	
	function Check_Class_AcademicYear_RecordStatus($yearID){
		// added 2013-03-08			
		$sql = "SELECT 
					yc.AcademicYearID 
				FROM 
					YEAR_CLASS as yc
				INNER JOIN 
					ACADEMIC_YEAR as ay on  ay.AcademicYearID = yc.AcademicYearID
				WHERE
					yc.YearID = $yearID						
				";
		return $this->returnArray($sql);		
	}
	
	function Get_Academic_Year_And_Year_Term_By_Date($TargetDate) {
		
		$sql = "select
					ay.AcademicYearID,
					ay.YearNameEN,
					ay.YearNameB5,
					ay.Sequence,
					ayt.YearTermID,
					ayt.YearTermNameEN,
					ayt.YearTermNameB5,
					ayt.TermStart,
					ayt.TermEnd
				from
					ACADEMIC_YEAR as ay
					inner join
					ACADEMIC_YEAR_TERM as ayt
					on ay.AcademicYearID = ayt.AcademicYearID
				where '$TargetDate' between ayt.TermStart and ayt.TermEnd";
							
		//echo $sql;
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function Get_Term_By_Date_Range($parStartDate, $parEndDate) {
		$sql = "select
					ayt.YearTermID,
					ayt.YearTermNameEN,
					ayt.YearTermNameB5
				from
					ACADEMIC_YEAR_TERM as ayt
				where 
					'$parStartDate' <= DATE(ayt.TermEnd)
      				AND '$parEndDate' >= DATE(ayt.TermStart)
				order by
					ayt.TermStart 
			   ";
		return $this->returnResultSet($sql);
	}
}

?>