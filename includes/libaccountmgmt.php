<?php
# using: Henry

####### Change log [Start] #######
#
#   Date    :   2020-09-13 Henry
#               added function getStudentListByClassID(), UpsertStudentChangeClassRecords() and DeleteStudentChangeClassRecords()
#
#   Date    :   2020-08-28 Cameron
#               apply getMacauBirthPlaceID() and getMacauNationID() to convert name to id mapping when sync from account to student registry [case #X193942]
#               modify updateUserPersonalSetting(), update fields exists in PORTFOLIO_STUDENT table only
#
#	Date	:	2020-03-16 Philips [2019-1115-1452-49066]
#				modified updateUserPersonalSetting(), updateUserInfo()
#				added syncToStudentRegistry_GuardianInfo(), syncToStudentRegistry_Simple_Macau()
#
#   Date    :   2019-02-12 Paul
#               add getSpecialIdentityMapping, upsertSpecialIdentityMapping, deleteSpecialIdentityMapping for mapping principal, vice-principal, chancellor account
#
#   Date    :   2018-11-12 Cameron
#               consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#
#   Date    :   2018-10-05 (Bill)   [2018-1005-1149-13073]
#               modified getStudentListWithPhoto(), getArchiveStudentListWithPhoto(), to return UserID
#
#	Date	:	2018-08-31 (Philips)
#				modified approveUsers(), suspendUsers(), disableUserInRadiusServer(), updateUserInfo() to insert libuser account Log
#
#	Date	:	2018-08-15	(Henry)
#				modified updateUserInfo to fixed sql injection
#
#	Date	:	2018-05-10	(Carlos)
#				Log update/remove/delete actions in class destructor.
#
#	Date	:	2017-12-22 	(Cameron)
#				Hide photo and official photo management in left menu for student account in GET_MODULE_OBJ_ARR() for Amway and Oaks
#
#   Date    :   2017-12-04 Isaac
#               replace c.WebSAMSRegNo by c.UserID in printStudentListWithPhoto()
#               modified getStudentUserInfoListByClassID(), sql for no class list added gender
#
#   Date    :   2017-12-04 Isaac (function commented by Isaac)
#               changed getArchiveStudentListWithPhoto() to printNoClassStudentListWithPhoto()
#               fixed sql for printNoClassStudentListWithPhoto()
#
#	Date	:	2017-11-21 (Simon) [K130888]
#				getArchiveStudentListWithPhoto()
#
#	Date	:	2017-11-20 (Simon) [E131184]
#				printStudentListWithPhoto()
#
#	Date	:	2017-11-13 (Simon) [K130888]
#				getStudentListWithPhoto()
#
#	Date	:	2017-10-31 (Simon) [M129084]
# 				add staff photo list
#				add flag $sys_custom['eAdmin']['AllowStaffPersonalPhotoBatchUpload']
#
#	Date	:	2017-08-24	(Simon)
#				Remove the photo height
#
#	Date	:	2017-07-04	(Paul)
#				Modified updateUserInfo() to add logging in NCS project
#
#	Date	:	2017-01-09	(Carlos)
#				Improved getStudentListWithoutPhoto($ParClassID) to get no photo students from INTRANET_USER, no need to find photo files to compare which is slow.
#
#	Date	:	2015-12-30 	(Carlos)
#				$plugin['radius_server'] - Added isUserDisabledInRadiusServer($userlogin) and disableUserInRadiusServer($userid_ary, $disable).
#
#	Date	:	2015-12-15 	(Cameron)
#				Hide photo and official photo management in left menu for student account in GET_MODULE_OBJ_ARR()
#
#	Date	:	2015-12-14 	(Carlos) [ip2.5.7.1.1]
#				Moved approve and suspend users flow into approveUsers($userIdAry) and suspendUsers($userIdAry). Added radius server disable user logic.
#
#	Date	:	2015-12-11	(Omas) [ip2.5.7.1.1]
#				Modified getEmailDefaultQuota(), checkDateFormat() - replace deprecated split() by explode() for PHP 5.4
#
#	Date	:	2015-08-17	(Omas)
#				Modified getStudentUserInfoListByClassID(),getStudentPhotoDisplayTable() support showing student without class, can search keyword for student without class
#
#	Date	:	2015-05-26	(Carlos)
#				Added suspendEmailFunctions($targetUserId, $resume=false), for suspend or resume mail server auto forwarding and auto reply functions.
#
#	Date	:	2014-10-24	(Omas)
#				Modified function GET_MODULE_OBJ_ARR()
#				$MenuArr["Settings"]["Child"]["BasicSettings"] redirect to AccountMgmt/Settings/index.php?RecordType=USERTYPE
#
#   Date    :   2014-10-16 (Ryan)
#				Modified  GET_MODULE_OBJ_ARR(), getParentListByStudentID() and getStudentListByParentID() to handle SIS cut Usermanagement
#
#   Date    :   2014-10-10 (Ryan)
#				Modified getUserInfoByUserLogin() to handle case sensitive serach in PhotoUpload validation
#
#	Date	: 	2014-01-23 (Siuwan)
#				Modified getStudentListWithoutPhoto() to handle KIS file path
#
#	Date	: 	2012-10-29 (Rita Tin)
#				Add getParentListByStudentID();
#
#	Date	:	2012-08-03 (Bill Mak)
#				Add updateUserPersonalSetting(), Update/Insert fields in INTRANET_USER_PERSONAL_SETTINGS and Update PORTFOLIO_STUDENT, input array uses $array[key]=val
#				Add getUserPersonalSettingByID(), Get all the fields from INTRANET_USER_PERSONAL_SETTINGS
#
#	Date	:	2012-04-03 (YatWoon)
#				Fixed: update GET_MODULE_OBJ_ARR(), use "AND" condition for $sys_custom['UserExtraInformation'] [Case#2012-0403-1033-15066]
#
#	Date	:	2012-02-27 (Henry Chow)
#				update Get_User_Extra_Info_UI(), Insert_User_Extra_Info_Mapping(), Update_User_Extra_Info_Mapping(), Get_User_Extra_Info_Item(), Get_User_Extra_Info_Category()
#				add field "Others" in options
#
#	Date	:	2012-02-07 (YatWoon)
#				update updateUserInfo(), add checking for "md5" syntax
#
#	Date	:	2011-11-03 (YatWoon)
#				update getUserNameArrayByConds(), add "order"
#
#	Date	:	2011-09-30 (Henry Chow)
#				update getLastestYearClassOfStudentByID, get data from "PROFILE_CLASS_HISTORY"
#
#	Date	:	2011-09-17 (YatWoon)
#				update GET_MODULE_OBJ_ARR(), add alumni menu
#
#	Date	:	2011-04-07 (Henry Chow)
#				update GET_MODULE_OBJ_ARR(), allow System Admin can view the menu
#
#	Date	:	2011-03-28	YatWoon
#			add function returnEmailNotificationData(), return notification email subject & content
#
#	Date	:	2011-02-14 (Henry Chow)
#				added getOfficalPhotoMgmtMemberTable(), removeOfficalPhotoMgmtGroupMember(), getOfficalPhotoMgmtMemberID(), addOfficalPhotoMgmtGroupMember
#				assign member(staff) to update offical photos of students
#
#	Date	:	2011-01-10 (YatWoon)
#				update GET_MODULE_OBJ_ARR(), set $clearCoo
#
#	Date	:	2010-11-22 (Henry Chow)
#	Detail	:	modified getClassNameClassNoByUserID(), also return "YearClassID"
#
#	Date	:	2010-11-22 (Marcus)
#	Detail	:	Added Signature Uploaded Related Table,
#				insertSignatureLink, deleteSignatureLink,updateSignatureLink, getTeacherSignatureDBTableSql, getTeacherSignatureDBTable
#
#	Date	:	2010-11-22 (Henry Chow)
#				modified getClassNameClassNoByUserID(), also return "YearID"
#
#	Date	:	2010-11-05	Marcus
#				Add User Extra Info (Shek Wu eRC Rubrics Cust)
#
#	Data 	:	20100827 (Henry Chow)
#	Detail	:	modified getStudentListByParentID(), display student name even student does not belongs to any class
#
#	Data 	:	20100825 (Henry Chow)
#	Detail	:	modified getStudentListByParentID(), display class name of current academic year
#
#	Data 	:	20100824 (Henry Chow)
#	Detail	:	modified GET_MODULE_OBJ_ARR(), added left menu to Staff Mgmt & Parent Mgmt
#
#	Data 	:	20100812 (Henry Chow)
#	Detail	:	added getClassNameClassNoByUserID and modified getStudentListByParentID(), get Class Name & Class Number from YEAR_CLASS & YEAR_CLASS_USER
#
#	Data 	:	20100614 (Marcus)
#	Detail	:	added setIMapUserEmailQuota, getIMapUserEmail, setIMapUserEmail, getEmailDefaultQuota, getUserImailGammaQuota for gamma mail
#
#	Date	:	2010-06-03 (Henry Chow)
#	Detail	:	add getUserNameArrayByConds(), retrieve UserID, Name by UserID array
#
####### Change log [End] #######


if (!defined("LIBACCOUNTMGMT_DEFINED"))                     // Preprocessor directive
{
    define("LIBACCOUNTMGMT_DEFINED", true);

    # Teaching Type #
    define("TEACHING",				1);
    define("NONTEACHING",			0);

    # RecordStatus #
    define("STATUS_SUSPENDED",		0);
    define("STATUS_APPROVED",		1);
    define("STATUS_PENDING",		2);
    define("STATUS_GRADUATE",		3);

    # RecordType #
    define("TYPE_TEACHER",			1);
    define("TYPE_STUDENT",			2);
    define("TYPE_PARENT",			3);
    define("TYPE_ALUMNI",			4);


    class libaccountmgmt extends libdb
    {
        var $Module;

        function __destruct()
        {
            global $___ACCOUNTMGMT_LOGGED___; // prevent same page being logged more than one time
            // log down when and who do update or remove action under AccountMgmt module path
            if( !$___ACCOUNTMGMT_LOGGED___  && (strpos($_SERVER['REQUEST_URI'],'/home/eAdmin/AccountMgmt/')!==false)
                && (strpos($_SERVER['REQUEST_URI'],'update')!==false || strpos($_SERVER['REQUEST_URI'],'remove')!==false || strpos($_SERVER['REQUEST_URI'],'delete')!==false) )
            {
                $___ACCOUNTMGMT_LOGGED___ = 1;
                $log = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.(isset($_SESSION['UserID'])?$_SESSION['UserID']:0).' '.(count($_POST)>0?base64_encode(serialize($_POST)):'');
                $this->log($log);
            }
        }

        function log($log_row)
        {
            global $intranet_root, $file_path, $sys_custom;

            $num_line_to_keep = isset($sys_custom['AccountMgmtLogMaxLine'])? $sys_custom['AccountMgmtLogMaxLine'] : 200000;
            $log_file_path = $file_path."/file/account_mgmt_log";
            $log_file_path_tmp = $file_path."/file/account_mgmt_log_tmp";
            // assume $log_row does not contains single quote that would break the shell command statement
            shell_exec("echo '$log_row' >> $log_file_path"); // append to log file
            $line_count = intval(shell_exec("cat '$log_file_path' | wc -l"));
            if($line_count > $num_line_to_keep){
                shell_exec("tail -n ".($num_line_to_keep/2)." '$log_file_path' > '$log_file_path_tmp'"); // keep the last $num_line_to_keep/2 lines
                shell_exec("rm '$log_file_path'");
                shell_exec("mv '$log_file_path_tmp' '$log_file_path'");
            }
        }

        function GET_MODULE_OBJ_ARR()
        {
            global $UserID, $PATH_WRT_ROOT, $plugin, $CurrentPage, $LAYOUT_SKIN, $image_path, $CurrentPageArr, $intranet_session_language, $intranet_root;
            global $ACCESS_RIGHT_RIGHT, $Lang, $i_general_BasicSettings,$special_feature, $sys_custom;

            include_once("libaccessright.php");
            $laccessright = new libaccessright();

            # Current Page Information init
            $PageManagement				= 0;
            $PageMgmt_Account			= 0;
            $PageMgmt_Photo 			= 0;
            $PageMgmt_ChangeClassRecord = 0;
            $PageMgmt_Principal			= 0;

            $PageSettings 				= 0;
            $PageSettings_Photo 		= 0;

            switch ($CurrentPage) {
                case "Mgmt_Account":
                    $PageManagement = 1;
                    $PageMgmt_Account = 1;
                    break;
                case "Mgmt_Photo":
                    $PageManagement = 1;
                    $PageMgmt_Photo = 1;
                    break;
                case "Mgmt_ChangeClassRecord":
                    $PageManagement = 1;
                    $PageMgmt_ChangeClassRecord = 1;
                    break;
                case "Signature_Upload":
                    $PageManagement = 1;
                    $PageSignature_Upload = 1;
                    break;
                case "Mgmt_Principal":
                    $PageManagement = 1;
                    $PageMgmt_Principal = 1;
                    break;
                case "Page_Settings":
                    $PageSettings = 1;
                    $PageSettings_Basic = 1;
                    break;
                case "Settings_OfficalPhotoMgmt":
                    $PageSettings = 1;
                    $PageSettings_OfficalPhotoMgmt = 1;
                    break;
                case "Settings_UserOtherInfoMgmt":
                    $PageSettings = 1;
                    $PageSettings_UserOtherInfoMgmt = 1;
                    break;
            }

            if($CurrentPageArr['StudentMgmt']) {

                $officalPhotoMgmtMember = $this->getOfficalPhotoMgmtMemberID();
                if(sizeof($officalPhotoMgmtMember) && in_array($UserID, $officalPhotoMgmtMember))
                    $canAccessOfficalPhotoSetting = 1;

                    $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentMgmt/";

                    # Management
                    if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION['SSV_USER_ACCESS']['eAdmin-AccountMgmt'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-AccountMgmt_Student'] || $canAccessOfficalPhotoSetting) {
                        $MenuArr["Management"]	= array($Lang['Menu']['AccountMgmt']['Management'], "#", $PageManagement);
                        if(($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION['SSV_USER_ACCESS']['eAdmin-AccountMgmt'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-AccountMgmt_Student'])){
                            if($sys_custom['SISUserManagement']){
                                $MenuArr["Management"]["Child"]["MgmtAccount"] 	= array($Lang['Menu']['AccountMgmt']['Account'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentMgmt/index_sis.php?clearCoo=1", $PageMgmt_Account);
                            }else
                                $MenuArr["Management"]["Child"]["MgmtAccount"] 	= array($Lang['Menu']['AccountMgmt']['Account'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentMgmt/index.php?clearCoo=1", $PageMgmt_Account);
                        }
                        if (!$sys_custom['project']['CourseTraining']['IsEnable']) {	// hide Management Photo for Amway, Oaks, CEM
                            $MenuArr["Management"]["Child"]["MgmtPhoto"] 	= array($Lang['Personal']['OfficialPhoto'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentMgmt/photo/", $PageMgmt_Photo);
                        }
                        if(($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION['SSV_USER_ACCESS']['eAdmin-AccountMgmt'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-AccountMgmt_Student'])){
                        	$MenuArr["Management"]["Child"]["ChangeClassRecord"] = array($Lang['Personal']['ChangeClassRecord'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentMgmt/ChangeClassRecord/", $PageMgmt_ChangeClassRecord);
                        }
                    }

                    # Settings
                    if(!$sys_custom['SISUserManagement']){
                        if(($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION['SSV_USER_ACCESS']['eAdmin-AccountMgmt'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-AccountMgmt_Student'])) {
                            $MenuArr["Settings"] = array($Lang['Menu']['AccountMgmt']['Settings'], "#", $PageSettings);
                            if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION['SSV_USER_ACCESS']['eAdmin-AccountMgmt'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-AccountMgmt_Student']) {
                                //$MenuArr["Settings"]["Child"]["BasicSettings"] 	= array($i_general_BasicSettings, $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentMgmt/settings/", $PageSettings_Basic);
                                $MenuArr["Settings"]["Child"]["BasicSettings"] 	= array($i_general_BasicSettings, $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/Settings/?RecordType=2", $PageSettings_Basic);

                            }
                            if (!$sys_custom['project']['CourseTraining']['IsEnable']) {	// hide Management Official Photo for Amway, Oaks, CEM
                                $MenuArr["Settings"]["Child"]["OfficalPhotoMgmtUserSettings"] 	= array($Lang['AccountMgmt']['OfficalPhotoMgmt'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentMgmt/settings/officalPhotoMgmt/", $PageSettings_OfficalPhotoMgmt);
                            }
                            if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] && $sys_custom['UserExtraInformation'])
                                $MenuArr["Settings"]["Child"]["UserExtraInfoItemSetting"] 	= array($Lang['AccountMgmt']['UserExtraInfoMgmt'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentMgmt/settings/userOtherInfoMgmt/", $PageSettings_UserOtherInfoMgmt);
                        }
                    }

                    ### module information
                    $MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];
                    $MODULE_OBJ['title_css'] = "menu_opened";
                    $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_iAccount.gif";
            }
            else if($CurrentPageArr['ParentMgmt']) {
                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/ParentMgmt/";

                # Management
                $MenuArr["Management"]	= array($Lang['Menu']['AccountMgmt']['Management'], "#", $PageManagement);
                if($sys_custom['SISUserManagement']){
                    $MenuArr["Management"]["Child"]["MgmtAccount"] 	= array($Lang['Menu']['AccountMgmt']['Account'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/ParentMgmt/index_sis.php?clearCoo=1", $PageMgmt_Account);
                }else
                    $MenuArr["Management"]["Child"]["MgmtAccount"] 	= array($Lang['Menu']['AccountMgmt']['Account'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/ParentMgmt/index.php?clearCoo=1", $PageMgmt_Account);

                    if(!$sys_custom['SISUserManagement']){
                        $MenuArr["Settings"] = array($Lang['Menu']['AccountMgmt']['Settings'], "#", $PageSettings);
                        //$MenuArr["Settings"]["Child"]["BasicSettings"] 	= array($i_general_BasicSettings, $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/ParentMgmt/settings/", $PageSettings_Basic);
                        $MenuArr["Settings"]["Child"]["BasicSettings"] 	= array($i_general_BasicSettings, $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/Settings/?RecordType=3", $PageSettings_Basic);

                    }


                    ### module information
                    $MODULE_OBJ['title'] = $Lang['Header']['Menu']['ParentAccount'];
                    $MODULE_OBJ['title_css'] = "menu_opened";
                    $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_iAccount.gif";
            }
            else if($CurrentPageArr['StaffMgmt']) {
                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StaffMgmt/";

                # Management
                $MenuArr["Management"]	= array($Lang['Menu']['AccountMgmt']['Management'], "#", $PageManagement);
                $MenuArr["Management"]["Child"]["MgmtAccount"] 	= array($Lang['Menu']['AccountMgmt']['Account'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StaffMgmt/index.php?clearCoo=1", $PageMgmt_Account);
                if($special_feature['signature_upload'])
                    $MenuArr["Management"]["Child"]["SignatureUpload"] 	= array($Lang['AccountMgmt']['UploadSignature'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StaffMgmt/photo/photo_list.php?clearCoo=1", $PageSignature_Upload);
                # add staff photo list [M129084]
                # add flag $sys_custom['eAdmin']['AllowStaffPersonalPhotoBatchUpload'] for [M129084]
                if($sys_custom['eAdmin']['AllowStaffPersonalPhotoBatchUpload']){
                    $MenuArr["Management"]["Child"]["PersonalPhoto"] = array($Lang['Personal']['PersonalPhoto'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StaffMgmt/photo/personal_photo_list.php", $PageMgmt_Photo);
                }
                $MenuArr["Management"]["Child"]["MgmtPrincipal"] 	= array($Lang['AccountMgmt']['PrincipalMgmt'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StaffMgmt/principal/index.php", $PageMgmt_Principal);

                if (!$sys_custom['HideAccountMgmtSettings']) {
                    $MenuArr["Settings"] = array($Lang['Menu']['AccountMgmt']['Settings'], "#", $PageSettings);
                    //$MenuArr["Settings"]["Child"]["BasicSettings"] 	= array($i_general_BasicSettings, $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StaffMgmt/settings/", $PageSettings_Basic);
                    $MenuArr["Settings"]["Child"]["BasicSettings"] 	= array($i_general_BasicSettings, $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/Settings/?RecordType=1", $PageSettings_Basic);
                }

                ### module information
                $MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];
                $MODULE_OBJ['title_css'] = "menu_opened";
                $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_iAccount.gif";

            }
            else if($CurrentPageArr['AlumniMgmt']) {
                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/AlumniMgmt/";

                # Management
                $MenuArr["Management"]	= array($Lang['Menu']['AccountMgmt']['Management'], "#", $PageManagement);
                $MenuArr["Management"]["Child"]["MgmtAccount"] 	= array($Lang['Menu']['AccountMgmt']['Account'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/AlumniMgmt/index.php?clearCoo=1", $PageMgmt_Account);

                # Settings
                $MenuArr["Settings"] = array($Lang['Menu']['AccountMgmt']['Settings'], "#", $PageSettings);
                //$MenuArr["Settings"]["Child"]["BasicSettings"] 	= array($i_general_BasicSettings, $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/AlumniMgmt/settings/", $PageSettings_Basic);
                $MenuArr["Settings"]["Child"]["BasicSettings"] 	= array($i_general_BasicSettings, $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/Settings/?RecordType=4", $PageSettings_Basic);

                ### module information
                $MODULE_OBJ['title'] = $Lang['Header']['Menu']['AlumniAccount'];
                $MODULE_OBJ['title_css'] = "menu_opened";
                $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_iAccount.gif";

            }
            $MODULE_OBJ['menu'] = $MenuArr;

            return $MODULE_OBJ;
        }


        function libaccountmgmt($ID="")
        {
            $this->Module = "StudentRegistry";
            $this->libdb();
        }

        function getTeachingClassList($userid)
        {
            $AcademicYearID = Get_Current_Academic_Year_ID();

            $sql = "SELECT ".Get_Lang_Selection("ClassTitleB5","ClassTitleEN")." FROM YEAR_CLASS yc LEFT OUTER JOIN YEAR_CLASS_TEACHER yct ON (yct.YearClassID=yc.YearClassID) WHERE yc.AcademicYearID=$AcademicYearID AND yct.UserID='$userid'";
            $result = $this->returnVector($sql);
            return implode(', ', $result);
        }

        function getUserInfoByID($id)
        {
            $sql = "SELECT * FROM INTRANET_USER WHERE UserID='$id'";
            $result = $this->returnArray($sql);
            return $result[0];
        }

        function updateUserInfo($uid, $dataAry=array(), $isSync = false)
        {
            global $UserID,$intranet_root,$sys_custom;
            if((isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!="")){
                $sql = "SELECT * FROM INTRANET_USER WHERE UserID='".$uid."'";
                $origDataAry = $this->returnArray($sql);
                $beforeChanges = "";
                $afterChanges = "";
                foreach($dataAry as $key=>$val){
                    if($origDataAry[0][$key] != $val){
                        if($key=="HKJApplNo" && $val=="")continue;
                        if($key=="DateOfBirth" && $val=="")continue;
                        if($key == "HashedPass"){
                            $afterChanges .= $key." changed;";
                        }else{
                            $beforeChanges .= $key.": ".$origDataAry[0][$key].";";
                            $afterChanges .= $key.": ".$val.";";
                        }
                    }
                }
            }
            $sql = "UPDATE INTRANET_USER SET ";
            foreach($dataAry as $key=>$val)
            {
                if($key=="HashedPass")
                    $sql .= $key."=".$val.", ";
                    else
                        $sql .= $key."='".$this->Get_Safe_Sql_Query($val)."', ";
            }

            $sql .= "DateModified=NOW(), ModifyBy='$UserID' WHERE UserID='$uid'";
            $success = $this->db_db_query($sql) or die(mysql_error());
            include_once('libuser.php');
            $lu = new libuser();
            $lu->insertAccountLog($uid, 'U');
            if((isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!="")){
                include_once($intranet_root."/includes/libcurdlog.php");
                $log = new libcurdlog();
                if($afterChanges!="" && $beforeChanges!=""){
                    $log->addlog("User Settings", "UPDATE", "INTRANET_USER", "UserID", $uid, $beforeChanges, $this->Get_Safe_Sql_Query($afterChanges), $sql);
                }
                unset($log);
            }

            # 2020-03-13 (Philips) - cdsj_5_macau sync to Student Registry
            if($success && !$isSync && $sys_custom['AccountMgmt']['cdsj_5_macau']['syncAccountAndRegistry']){
            	$result = true;
            	$dataAry_Registry = array();
            	if($dataAry['STRN']) $dataAry_Registry['CODE'] = $dataAry['STRN'];
            	if(!empty($dataAry_Registry)) $result = $this->syncToStudentRegistry_Simple_Macau($dataAry_Registry, $uid);
            	if(!$result) return false;
            }

            return $success;
        }

        function getUserPersonalSettingByID($id)
        {
            $sql = "SELECT * FROM INTRANET_USER_PERSONAL_SETTINGS WHERE UserID='$id'";
            $result = $this->returnArray($sql);
            return $result[0];
        }

        function updateUserPersonalSetting($uid, $dataAry_PersonalSetting=array(), $isSync = false)
        {
            global $eclass_db, $sys_custom;
            include_once('libstudentregistry.php');
            $lsr = new libstudentregistry();

            $sql = "DESC ".$eclass_db.".PORTFOLIO_STUDENT";
            $rs = $this->returnResultSet($sql);
            $portfolioStudentFieldAry = Get_Array_By_Key($rs,'Field');

            $dataAry_PS = array();
            $dataAry_PS_2 = array();

            foreach($dataAry_PersonalSetting as $key=>$val) {
                array_push($dataAry_PS,$key,$key."_DateModified");
                array_push($dataAry_PS_2,"'".$val."'","NOW()");
            }
            $sql = "INSERT INTO INTRANET_USER_PERSONAL_SETTINGS (UserID, ".implode(',',$dataAry_PS).")";
            $sql .= "VALUES ('".$uid."',".implode(',',$dataAry_PS_2).")";

            $sql .= "ON DUPLICATE KEY UPDATE ";
            $dataAry_PS = array();
            $dataAry_PS_2 = array();
            foreach($dataAry_PersonalSetting as $key=>$val)
            {
                array_push($dataAry_PS, $key."_DateModified = CASE WHEN (ISNULL(".$key.")&&!ISNULL('".$val."'))||".$key."!='".$val."' THEN NOW() ELSE ".$key."_DateModified END");
                array_push($dataAry_PS, $key."= CASE WHEN (ISNULL(".$key.")&&!ISNULL('".$val."'))||".$key."!='".$val."' THEN '".$val."' ELSE ".$key." END");
                if (in_array($key, (array)$portfolioStudentFieldAry)) {
                    array_push($dataAry_PS_2, $key . "='" . $val . "'");
                }
            }
            $sql .= implode(', ',$dataAry_PS);
            $success = $this->db_db_query($sql);
            //sync IP table
            $sql = "UPDATE ".$eclass_db.".PORTFOLIO_STUDENT SET
					".implode(',',$dataAry_PS_2)."
					WHERE UserID = '".$uid."'";
            $success2 = $this->db_db_query($sql);


            # 2020-03-13 (Philips) - cdsj_5_macau sync to student registry
            # can only sync those records in nation list and birth place list in student registry (Macau)
            if($success && $success2 && !$isSync && $sys_custom['AccountMgmt']['cdsj_5_macau']['syncAccountAndRegistry']){
            	$dataAry = array();
            	if($dataAry_PersonalSetting['PlaceOfBirth']) $dataAry['B_PLACE'] = $lsr->getMacauBirthPlaceID($dataAry_PersonalSetting['PlaceOfBirth']);
            	if($dataAry_PersonalSetting['Nationality']) $dataAry['NATION'] = $lsr->getMacauNationID($dataAry_PersonalSetting['Nationality']);
            	if($dataAry_PersonalSetting['AdmissionDate']) $dataAry['ADMISSION_DATE'] = $dataAry_PersonalSetting['AdmissionDate'];

            	if(!empty($dataAry)) $result = $this->syncToStudentRegistry_Simple_Macau($dataAry, $uid);

            	if(!$result) return false;
            }
            return ($success&&$success2);
        }

        function getUserNCSSettingById($uid){
            $sql = "SELECT * FROM INTRANET_USER_NCS_SETTINGS WHERE UserID='".$uid."'";
            $result = $this->returnArray($sql);
            return $result[0];
        }

        function updateUserNCSSetting($uid, $dataAry_NC_settingS=array()){
            global $intranet_root;
            $dataAry_NCS = array();
            $dataAry_NCS_2 = array();
            $beforeChanges = "";
            $afterChanges = "";

            foreach($dataAry_NC_settingS as $key=>$val) {
                array_push($dataAry_NCS,$key);
                array_push($dataAry_NCS_2,"'".$val."'");
            }
            $sql = "SELECT * FROM INTRANET_USER_NCS_SETTINGS WHERE UserID='".$uid."'";
            $result = $this->returnArray($sql);
            if(empty($result)){
                $sql = "INSERT INTO INTRANET_USER_NCS_SETTINGS (UserID, ".implode(',',$dataAry_NCS).", DateInput, Modified)";
                $sql .= "VALUES ('".$uid."',".implode(',',$dataAry_NCS_2).", NOW(), NOW())";
                $action = "CREATE";
                $afterChanges = implode(',',$dataAry_NCS);
            }else{
                foreach($result as $k=>$r){
                    $beforeChanges .= $k.": ".$r.";";
                }
                $sql = "UPDATE INTRANET_USER_NCS_SETTINGS SET ";
                foreach($dataAry_NC_settingS as $key=>$val) {
                    $sql .= $key." = '".$val."',";
                    $afterChanges .= $key.": ".$val.";";
                }
                $sql .= " Modified=NOW() ";
                $sql .= " WHERE UserID='".$uid."'";
                $action = "UDPATE";
            }
            $success = $this->db_db_query($sql);
            if((isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!="")){
                include_once($intranet_root."/includes/libcurdlog.php");
                $log = new libcurdlog();
                if($afterChanges!="" && $beforeChanges!=""){
                    $log->addlog("User Settings", "UPDATE", "INTRANET_USER_NCS_SETTINGS", "UserID", $uid, $beforeChanges, $afterChanges, $sql);
                }
                unset($log);
            }
            return $success;
        }

        function checkDateFormat($RecordDate)
        {
            if(substr_count($RecordDate, "-") != 0)
                list($year, $month, $day) = explode('-',$RecordDate);
                else
                    list($year, $month, $day) = explode('/',$RecordDate);

                    if((strlen($year)==4 && strlen($month)==2 && strlen($day)==2))
                    {
                        if(checkdate($month,$day,$year))
                        {
                            return true;
                        }
                    }
                    return false;
        }

        function getGroupNameByStudentID($StudentID, $AcademicYearID="")
        {
            if($AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();

            $sql = "SELECT g.Title FROM INTRANET_GROUP g LEFT OUTER JOIN INTRANET_USERGROUP u ON (u.GroupID=g.GroupID) WHERE u.UserID='$StudentID' AND g.RecordType=2 AND g.AcademicYearID='$AcademicYearID'";
            $result = $this->returnVector($sql);
            //return implode(', ', $result);
            //echo $sql;
            return $result;
        }

        function getGroupRoleByStudentID($StudentID, $AcademicYearID="")
        {
            if($AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();

            $sql = "SELECT r.Title, g.Title FROM INTRANET_GROUP g LEFT OUTER JOIN INTRANET_USERGROUP u ON (u.GroupID=g.GroupID) LEFT OUTER JOIN INTRANET_ROLE r ON (r.RoleID=u.RoleID) WHERE u.UserID='$StudentID' AND g.RecordType=2 AND g.AcademicYearID='$AcademicYearID'";
            $result = $this->returnArray($sql,2);
            //echo $sql;
            $returnValue = "";
            for($i=0; $i<sizeof($result); $i++) {
                if($result[0][0]!="")
                    $returnValue .= ($returnValue) ? ", ".$result[0][0]."(".$result[0][1].")" : $result[0][0]."(".$result[0][1].")";
            }
            return $returnValue;
        }

        function getParentNameByStudentID($StudentID)
        {
            $name_field = getNameFieldByLang("USR.");
            $sql = "SELECT $name_field FROM INTRANET_USER USR LEFT OUTER JOIN INTRANET_PARENTRELATION pr ON (pr.ParentID=USR.UserID) LEFT OUTER JOIN INTRANET_USER u ON (u.UserID=pr.StudentID) WHERE u.UserID='$StudentID'";
            return $this->returnVector($sql);

        }

        function getStudentListByParentID($parentID)
        {
            global $sys_custom;
            /*
             $name_field = getNameFieldByLang("USR.");
             $sql = "SELECT ".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN').", ycu.ClassNumber, $name_field, USR.UserID FROM INTRANET_USER USR LEFT OUTER JOIN INTRANET_PARENTRELATION pr ON (pr.StudentID=USR.UserID) LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) WHERE pr.ParentID='$parentID' AND (yc.AcademicYearID='".Get_Current_Academic_Year_ID()."') GROUP BY ycu.UserID";
             $result = $this->returnArray($sql, 3);
             $returnValue = "";
             for($i=0; $i<sizeof($result); $i++) {
             if($returnValue!="") $returnValue .= ",<br>";
             $returnValue .= "<a href=\"javascript:goURL('../StudentMgmt/edit.php', {$result[$i][3]})\">".$result[$i][0].(($result[$i][1]!="")?"-".$result[$i][1]:"")." ".$result[$i][2]."</a>";
             }
             */
            $sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID='$parentID'";
            $childAry = $this->returnVector($sql);
            $returnValue = "";
            for($i=0; $i<sizeof($childAry); $i++) {
                $std_id = $childAry[$i];
                $stdinfo = $this->getUserInfoByID($std_id);

                $stdName = Get_Lang_Selection($stdinfo['ChineseName'], $stdinfo['EnglishName']);

                $sql = "SELECT ".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as class, ycu.ClassNumber FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (ycu.YearClassID=yc.YearClassID) WHERE ycu.UserID=$std_id AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."'";
                $temp = $this->returnArray($sql,2);
                if($returnValue!="") $returnValue .= ",<br>";
                if($sys_custom['SISUserManagement']){
                    $returnValue .= "<a href=\"javascript:goURL('../StudentMgmt/edit_sis.php', $std_id)\">".$temp[0][0].(($temp[0][1]!="")?"-".$temp[0][1]:"")." ".$stdName."</a>";
                }else
                    $returnValue .= "<a href=\"javascript:goURL('../StudentMgmt/edit.php', $std_id)\">".$temp[0][0].(($temp[0][1]!="")?"-".$temp[0][1]:"")." ".$stdName."</a>";
            }

            return $returnValue;
        }

        function getParentListByStudentID($studentID)
        {
            global $sys_custom;
            $sql = "SELECT ParentID FROM INTRANET_PARENTRELATION WHERE StudentID='$studentID'";
            $childAry = $this->returnVector($sql);
            $returnValue = "";
            for($i=0; $i<sizeof($childAry); $i++) {
                $parent_id = $childAry[$i];
                $parentInfo = $this->getUserInfoByID($parent_id);

                $parentName = Get_Lang_Selection($parentInfo['ChineseName'], $parentInfo['EnglishName']);

                $sql = "SELECT ".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as class, ycu.ClassNumber FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (ycu.YearClassID=yc.YearClassID) WHERE ycu.UserID=$parent_id AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."'";
                $temp = $this->returnArray($sql,2);
                if($returnValue!="") $returnValue .= ",<br>";
                if($sys_custom['SISUserManagement']){
                    $returnValue .= "<a href=\"javascript:goURL('../ParentMgmt/edit_sis.php', $parent_id)\">".$temp[0][0].(($temp[0][1]!="")?"-".$temp[0][1]:"")." ".$parentName."</a>";
                }else
                    $returnValue .= "<a href=\"javascript:goURL('../ParentMgmt/edit.php', $parent_id)\">".$temp[0][0].(($temp[0][1]!="")?"-".$temp[0][1]:"")." ".$parentName."</a>";
            }

            return $returnValue;
        }


        // marcus
        function getStudentListWithoutPhoto($ParClassID)
        {
            global $PATH_WRT_ROOT,$setting_path_ip_rel;

            //include_once("libfilesystem.php");
            if($_SESSION["platform"]=="KIS"){
                $user_photo_path = $PATH_WRT_ROOT.$setting_path_ip_rel."/file/user_photo";
            }else{
                $user_photo_path = $PATH_WRT_ROOT."file/user_photo";
            }
            //$lfs = new libfilesystem();

            // use linux command to get file list is faster than php folder scanning, but it is platform dependent
            //$cmd_result = trim(shell_exec("find '$user_photo_path' -maxdepth 1 -type f"));
            //$fileList = explode("\n",$cmd_result);
            /*
             $fileList = $lfs->return_folderlist($user_photo_path);
             foreach((array)$fileList as $thisfilepath)
             $thisfilename[] = $lfs->file_name($thisfilepath);
             
             $UserInfoList = $this->getStudentUserInfoListByClassID($ParClassID);
             $UserLoginList = Get_Array_By_Key($UserInfoList,"UserLogin");
             
             $StudentListWithoutPhoto = array_diff((array)$UserLoginList,(array)$thisfilename);
             */
             // now has stored student photo link in db, get it directly, no need to find file list
             $sql = "SELECT
             c.UserLogin
             FROM
             YEAR_CLASS a
             INNER JOIN YEAR_CLASS_USER b ON a.YearClassID = b.YearClassID
             INNER JOIN INTRANET_USER c ON b.UserID = c.UserID
             WHERE
             a.YearClassID = '$ParClassID' AND (c.PhotoLink IS NULL OR c.PhotoLink='')
             ORDER BY
             b.ClassNumber";
             // 		  	debug_pr($sql);
             $StudentListWithoutPhoto = $this->returnVector($sql);

             return $StudentListWithoutPhoto;
        }

        # getStudentListWithPhoto
        function getStudentListWithPhoto($ParClassID)
        {
            global $PATH_WRT_ROOT;

            $sql = "SELECT
                        c.PhotoLink, a.ClassTitleEN, c.WebSAMSRegNo, c.UserID
                    FROM
                        YEAR_CLASS a
                        INNER JOIN YEAR_CLASS_USER b ON a.YearClassID = b.YearClassID
                        INNER JOIN INTRANET_USER c ON b.UserID = c.UserID
                    WHERE
                        a.YearClassID = '$ParClassID' AND (c.PhotoLink IS NOT NULL AND c.PhotoLink !='')
                        /*AND c.WebSAMSRegNo != ''*/
                    ORDER BY
                        b.ClassNumber";
            // 			debug_pr($sql);
            $StudentPhotoInfoArr = $this->returnResultSet($sql);
            return($StudentPhotoInfoArr);
        }

        # getArchiveStudentListWithPhoto
        function getArchiveStudentListWithPhoto()
        {
            global $PATH_WRT_ROOT;

            $sql = 'SELECT
						UserID
					FROM YEAR_CLASS as yc
						INNER JOIN YEAR_CLASS_USER as ycu ON (yc.YearClassID=ycu.YearClassID)
					WHERE
                        AcademicYearID = "'.Get_Current_Academic_Year_ID().'"';
            $userWithClass = $this->returnArray($sql);
            //debug_pr($userWithClass);

            $sql = "SELECT
						c.PhotoLink, a.ClassTitleEN, c.WebSAMSRegNo, c.EnglishName, c.UserID
					FROM
						YEAR_CLASS a
						INNER JOIN YEAR_CLASS_USER b ON a.YearClassID = b.YearClassID
						INNER JOIN INTRANET_USER c ON b.UserID = c.UserID
					WHERE
						c.UserID NOT IN ('".implode("','",Get_Array_By_Key($userWithClass,'UserID'))."')
						AND c.RecordType = ".TYPE_STUDENT."
						AND (c.PhotoLink IS NOT NULL AND c.PhotoLink !='')
					ORDER BY
						b.ClassNumber ";
            // 			debug_pr($sql);
            $result = $this->returnResultSet($sql);
            return $result;
        }

        // 		function printNoClassStudentListWithPhoto(){

        // 			global $PATH_WRT_ROOT;


        // 			$field = Get_Lang_Selection("if(c.ChineseName != '', c.ChineseName, c.EnglishName) as name" ,"c.EnglishName as name");

        // 			$sql = "SELECT
        // 						 $field, c.UserLogin, c.Gender, c.WebSAMSRegNo
        // 					FROM

        // 						INTRANET_USER c
        // 					WHERE
        // 						c.UserID Not In ('".implode("','",Get_Array_By_Key($userWithClass,'UserID'))."')
        // 							AND c.RecordType = ".TYPE_STUDENT."
        // 						GROUP BY c.UserLogin
        // 						ORDER BY
        // 						c.EnglishNamec.ChineseName
        // 					";
        // // 			if($_SESSION['UserID'] == '1'){
        // //  				debug_pr($sql);
        // // 			}

        // 			$result = $this->returnResultSet($sql);
        // 			debug_pr($result);
        // 			return $result;

        // 		}

        # printStudentListWithPhoto
        function printStudentListWithPhoto($ParClassID)
        {
            global $PATH_WRT_ROOT;

            $field = Get_Lang_Selection("if(c.ChineseName != '', c.ChineseName, c.EnglishName) as name" ,"c.EnglishName as name");

            $sql = 'SELECT
						b.ClassNumber, '.$field.', c.ChineseName, c.UserLogin, a.ClassTitleEN, c.Gender, c.UserID
					FROM
						YEAR_CLASS a
						INNER JOIN YEAR_CLASS_USER b ON a.YearClassID = b.YearClassID
						INNER JOIN INTRANET_USER c ON b.UserID = c.UserID
					WHERE
						a.YearClassID = "'.$ParClassID.'"
						and AcademicYearID = "'.Get_Current_Academic_Year_ID().'"
						ORDER BY
						b.ClassNumber';

            $StudentPhotoInfoArr = $this->returnResultSet($sql);
            return($StudentPhotoInfoArr);
        }

        function getStudentUserInfoListByClassID($ParClassID,$keyword='')
        {
            if($ParClassID == ''){
                if($keyword != ''){
                    $keyword_cond = " And (
							UserLogin Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							Or EnglishName Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							Or ChineseName Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
						  )
					";
                }

                $sql = 'SELECT
							UserID
						from YEAR_CLASS as yc
						inner join YEAR_CLASS_USER as ycu ON (yc.YearClassID=ycu.YearClassID)
						where AcademicYearID = "'.Get_Current_Academic_Year_ID().'"';
                $userWithClass = $this->returnArray($sql);

                $sql = "Select UserID , EnglishName, ChineseName, UserLogin, Gender
						from INTRANET_USER
						WHERE
							UserID Not In ('".implode("','",Get_Array_By_Key($userWithClass,'UserID'))."')
							AND RecordType = ".TYPE_STUDENT."
							$keyword_cond
							ORDER BY EnglishName";
							//$userWithOutClass = $this->returnArray($sql);
							$result = $this->returnArray($sql);
            }
            else{
                $sql = "
                SELECT
                c.UserID, c.EnglishName, c.ChineseName, c.UserLogin, b.ClassNumber
                FROM
                YEAR_CLASS a
                INNER JOIN YEAR_CLASS_USER b ON a.YearClassID = b.YearClassID
                INNER JOIN INTRANET_USER c ON b.UserID = c.UserID
                WHERE
                a.YearClassID = '$ParClassID'
                ORDER BY
                b.ClassNumber";
                $result = $this->returnArray($sql);
            }
            return $result;
        }

        function getStudentPhotoDisplayTable($ParClassID,$keyword='')
        {
            global $Lang, $PATH_WRT_ROOT, $intranet_root;

            $x .= '<table width="100%" cellpadding="5" cellspacing="0" border="0">';
            $x .= '<tr class="tabletop">';
            if($ParClassID==''){
                $x .= '<td class="tabletopnolink" width="15%">#</td>';
            }
            else{
                $x .= '<td class="tabletopnolink" width="15%">'.$Lang['AccountMgmt']['ClassNo'].'</td>';
            }
            $x .= '<td class="tabletopnolink" width="35%">'.$Lang['AccountMgmt']['StudentName'].'</td>';
            $x .= '<td class="tabletopnolink" width="25%">'.$Lang['AccountMgmt']['LoginID'].'</td>';
            $x .= '<td class="tabletopnolink" width="25%">'.$Lang['AccountMgmt']['Photo'].'</td>';
            $x .= '<td class="tabletopnolink" width="10px"><input id="CheckAllPhoto" type="checkbox" onclick="CheckAll(\'UserLogin[]\')"></td>';
            $x .= '</tr>';

            $StudentList = $this->getStudentUserInfoListByClassID($ParClassID,$keyword);
            foreach((array) $StudentList as $key=>$StudentInfo)
            {
                # image
                $user_photo_path = $intranet_root."/file/user_photo/".$StudentInfo['UserLogin'].".jpg";
                if(file_exists($user_photo_path))
                {
                    $img = '<img src="/file/user_photo/'.$StudentInfo['UserLogin'].'.jpg" width="100px">';
                    $checkBox = '<input type="checkbox" name="UserLogin[]" value="'.$StudentInfo['UserLogin'].'" onclick="check_click(this)">';
                }
                else
                {
                    $img ="&nbsp;";
                    $checkBox ="&nbsp;";
                }

                #name
                $StudentName = Get_Lang_Selection($StudentInfo["ChineseName"],$StudentInfo["EnglishName"]);

                $css = (++$ctr%2==1)?"retablerow1":"retablerow2";
                $x .= '<tr class="'.$css.'">';
                if($ParClassID==''){
                    $x .= '<td class="tabletext " valign="top">'.($key+1).'</td>';
                }
                else{
                    $x .= '<td class="tabletext " valign="top">'.$StudentInfo["ClassNumber"].'</td>';
                }
                $x .= '<td class="tabletext " valign="top">'.$StudentName.'</td>';
                $x .= '<td class="tabletext " valign="top">'.$StudentInfo['UserLogin'].'</td>';
                $x .= '<td class="tabletext " valign="top">'.$img.'</td>';
                $x .= '<td class="tabletext " valign="top" align="center">'.$checkBox.'</td>';
                $x .= '</tr>';
            }
            $x .= '</table>';

            return $x;
        }

        function getPhotoMgmtIndexTable()
        {
            global $Lang, $PATH_WRT_ROOT;
            $x .= '<table width=100% cellspacing=0 cellpadding=0><tr><td>';
            $x .= '<table class="common_table_list">';
            $x .= '<tr>';
            $x .= '<th style="width:25%;">'.$Lang['AccountMgmt']['Form'].'</th>';
            $x .= '<th class="sub_row_top" style="width:25%;">'.$Lang['AccountMgmt']['Class'].'</th>';
            $x .= '<th class="sub_row_top" style="width:25%;">'.$Lang['AccountMgmt']['NoOfStudents'].'</th>';
            $x .= '<th class="sub_row_top" style="width:25%;">'.$Lang['AccountMgmt']['NoOfStudentsWithoutPhoto'].'</th>';
            $x .= '</tr>';

            // Form list start
            include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
            $fcm = new form_class_manage();
            $AcademicYearID = Get_Current_Academic_Year_ID();
            $FormList = $fcm->Get_Form_List();
            for ($i=0; $i< sizeof($FormList); $i++) {

                $ClassList = $fcm->Get_Class_List($AcademicYearID,$FormList[$i]['YearID']);
                $RowSpan = (sizeof($ClassList) > 0)? 'rowspan="'.(sizeof($ClassList)+2).'"':'';
                $FirstDisplay = (sizeof($ClassList) > 0)? 'display:none;':'';
                $x .= '<tbody>';
                $x .= '<tr>';
                $x .= '<td width="25%" '.$RowSpan.'>'.$FormList[$i]['YearName'].'</td>';
                $x .= '<td width="25%" style="'.$FirstDisplay.'">&nbsp;</td>';
                $x .= '<td width="25%" style="'.$FirstDisplay.'">&nbsp;</td>';
                $x .= '<td width="25%" style="'.$FirstDisplay.'"></td>';
                $x .= '</tr>';
                for ($j=0; $j< sizeof($ClassList); $j++) {
                    $thisClassID = $ClassList[$j]['YearClassID'];
                    $UserInfoList = $this->getStudentUserInfoListByClassID($thisClassID);
                    $NoOfStudent = count($UserInfoList);

                    $StudentListWithoutPhoto = $this->getStudentListWithoutPhoto($thisClassID);
                    $NoOfStudentWithoutPhoto = count($StudentListWithoutPhoto);
                    $x .= '<tr class="sub_row" id="'.$ClassList[$j]['YearClassID'].'">';
                    $x .= '<td><a href="photo_list.php?YearClassID='.$ClassList[$j]['YearClassID'].'">'.Get_Lang_Selection($ClassList[$j]['ClassTitleB5'],$ClassList[$j]['ClassTitleEN']).'&nbsp;</a></td>';
                    $x .= '<td>'.$NoOfStudent.'</td>';
                    $x .= '<td>'.$NoOfStudentWithoutPhoto.'</td>';
                    $x .= '</tr>';
                }
                $x .= '</tbody>';
            }
            $x .= '</table>';
            $x .= '</td></tr></table>';

            return $x;
        }

        function getUserInfoByUserLogin($UserLoginArr=array())
        {
            if(!is_array($UserLoginArr))
                $UserLoginArr = (array)$UserLoginArr;

                foreach($UserLoginArr as $tmpUserLogin)
                {
                    $tmpUserLoginStr[]= "'".$tmpUserLogin."'";
                }
                $UserLoginStr = implode(",",$tmpUserLoginStr);

                $name_field = getNameFieldByLang("");
                $sql = "
                SELECT
                UserLogin,
                ClassName,
                ClassNumber,
                $name_field as Name,
                UserID,
                RecordType
                FROM
                INTRANET_USER
                WHERE
                UserLogin COLLATE utf8_bin IN ($UserLoginStr)
                ";
                # 20141010 Ryan
                # Change UserLogin COLLATE to utf8_bin for this Query
                # to make where condition case sensitive
                # due to original collation of INTRANET_USER is utf8_general_ci
                //				WHERE
                //					UserLogin IN ($UserLoginStr)

                $result = $this->returnArray($sql);

                if(!empty($result))
                    $returnAry = BuildMultiKeyAssoc($result, "UserLogin");

                    return $returnAry;

        }

        function updatePhotoLink($ParUserLogin,$photo_path)
        {
            global $intranet_root;
            $sql = "
            UPDATE
            INTRANET_USER
            SET
            PhotoLink = '$photo_path'
            WHERE
            UserLogin = '$ParUserLogin';
            ";
            if((isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!="")){
                include_once($intranet_root."/includes/libcurdlog.php");
                $log = new libcurdlog();
                if($afterChanges!="" && $beforeChanges!=""){
                    $log->addlog("User Settings", "UPDATE", "INTRANET_USER", "UserID", $uid, "", "PhotoLink = '$photo_path'", $sql);
                }
                unset($log);
            }
            return $this->db_db_query($sql)?1:0;
        }

        function getLastestYearClassOfStudentByID($studentID, $returnFlag=1) {
            # returnFlag : 1 = AcademicYearID; 2 = YearClassID
            /*
             $sql = "SELECT DISTINCT A.AcademicYearID FROM ACADEMIC_YEAR A INNER JOIN ACADEMIC_YEAR_TERM T ON (A.AcademicYearID=T.AcademicYearID) WHERE TermStart<=NOW() ORDER BY T.TermStart DESC";
             $yearAry = $this->returnVector($sql);
             foreach($yearAry as $yearid) {
             $sql = "SELECT
             yc.AcademicYearID, yc.YearClassID
             FROM
             YEAR_CLASS yc LEFT OUTER JOIN
             YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID)
             WHERE
             ycu.UserID=$studentID AND
             yc.YearClassID IN (SELECT YearClassID FROM YEAR_CLASS WHERE AcademicYearID=$yearid)
             ";
             $result = $this->returnArray($sql, 2);
             if(sizeof($result)>0) {
             if($returnFlag==1)
             return $result[0][0];
             else
             return $result[0][1];
             }
             }
             */

             # get data from PROFILE_CLASS_HISTORY
             $sql = "SELECT AcademicYear, ClassName, ClassNumber, YearClassID, AcademicYearID FROM PROFILE_CLASS_HISTORY WHERE UserID='$studentID' ORDER BY AcademicYearID DESC LIMIT 1";
             $data = $this->returnArray($sql);

             $returnAry = array();
             if(count($data)>0) {
                 list($AcademicYear, $ClassName, $ClassNumber, $YearClassID, $AcademicYearID) = $data[0];
                 $returnAry = array(
                     "AcademicYear" => $AcademicYear,
                     "ClassName" => $ClassName,
                     "ClassNumber" => $ClassNumber,
                     "YearClassID" => $YearClassID,
                     "AcademicYearID" => $AcademicYearID
                 );
             }
             return $returnAry;
        }

        function getAcademicYearNameByYearID($yearID)
        {
            $sql = "SELECT YearNameB5, YearNameEN FROM ACADEMIC_YEAR WHERE AcademicYearID='$yearID'";
            $result = $this->returnArray($sql, 2);
            $clsName = Get_Lang_Selection($result[0][0], $result[0][1]);
            return $clsName;
        }

        function getClassNameByClassID($classID)
        {
            $sql = "SELECT ".Get_Lang_Selection("ClassTitleB5","ClassTitleEN")." FROM YEAR_CLASS WHERE YearClassID='$classID'";
            $result = $this->returnVector($sql);
            return $result[0];
        }

        function getUserNameArrayByConds($type, $conds="")
        {
            $name_field = getNameFieldByLang();
            $sql = "SELECT UserID, $name_field FROM INTRANET_USER WHERE RecordType='$type' $conds";
            $sql .= " order by EnglishName";
            return $this->returnArray($sql, 2);
        }

        function getUserImailGammaQuota($uid)
        {
            $sql = "
            SELECT
            GammaQuota
            FROM
            INTRANET_CAMPUSMAIL_USERQUOTA
            WHERE
            UserID = '$uid'
            ";

            $result = $this->returnVector($sql);

            return $result[0];
        }

        function getEmailDefaultQuota($ParUserType='')
        {
            global $intranet_root;
            include("libfilesystem.php");
            $li = new libfilesystem();
            $file_content = $li->file_read($intranet_root."/file/campusmail_set.txt");

            $EmailQuota = array();
            if ($file_content == "")
            {
                $EmailQuota[TYPE_STUDENT] = array(1,10);
                $EmailQuota[TYPE_TEACHER] = array(1,10);
                $EmailQuota[TYPE_ALUMNI] = array(1,10);
                $EmailQuota[TYPE_PARENT] = array(1,10);
            }
            else
            {
                $content = explode("\n", $file_content);

                $EmailQuota[TYPE_TEACHER] = explode(":",$content[0]);
                $EmailQuota[TYPE_STUDENT] = explode(":",$content[1]);
                $EmailQuota[TYPE_ALUMNI] = explode(":",$content[2]);
                $EmailQuota[TYPE_PARENT] = explode(":",$content[3]);
            }

            if($ParUserType=='')
                return $EmailQuota;
                else
                    return $EmailQuota[$ParUserType];
        }

        function setIMapUserEmail($ParUserID,$email='')
        {
            global $intranet_root;
            $sql = "
            UPDATE
            INTRANET_USER
            SET
            IMapUserEmail = '$email'
            WHERE
            UserID = '$ParUserID';
            ";
            if((isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!="")){
                include_once($intranet_root."/includes/libcurdlog.php");
                $log = new libcurdlog();
                if($afterChanges!="" && $beforeChanges!=""){
                    $log->addlog("User Settings", "UPDATE", "INTRANET_USER", "UserID", $uid, "", "IMapUserEmail = '$email'", $sql);
                }
                unset($log);
            }
            return $this->db_db_query($sql);

        }

        function getIMapUserEmail($ParUserID)
        {
            $sql = "
            SELECT
            IMapUserEmail
            FROM
            INTRANET_USER
            WHERE
            UserID = '$ParUserID';
            ";
            $result = $this->returnVector($sql);

            return $result[0];

        }

        function CheckTotalQuotaExist($ParUserID)
        {
            $sql = "
            SELECT
            Count(*)
            FROM
            INTRANET_CAMPUSMAIL_USERQUOTA
            WHERE
            UserID = '$ParUserID'
            ";

            $result = $this->returnVector($sql);

            return $result[0]>=1;
        }

        function setIMapUserEmailQuota($ParUserID,$Quota)
        {
            $QuotaExist = $this->CheckTotalQuotaExist($ParUserID);

            if($QuotaExist)
            {
                $sql = "
                UPDATE
                INTRANET_CAMPUSMAIL_USERQUOTA
                SET
                GammaQuota = '$Quota'
                WHERE
                UserID = '$ParUserID';
                ";
            }
            else
            {

                $sql = "
                INSERT INTO
                INTRANET_CAMPUSMAIL_USERQUOTA
                (UserID, GammaQuota)
                VALUES
                ('$ParUserID', '$Quota')";
            }

            $result =  $this->db_db_query($sql);

            return $result;

        }

        function getClassNameClassNoByUserID($user_id, $AcademicYearID="")
        {
            if($AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();
            $sql = "SELECT ".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as ClassName, ycu.ClassNumber as ClassNumber, y.YearID, yc.YearClassID FROM YEAR_CLASS yc LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID) LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID) WHERE ycu.UserID=$user_id AND yc.AcademicYearID=$AcademicYearID";
            $result = $this->returnArray($sql,3);

            return $result[0];

        }

        function Insert_User_Extra_Info_Item($CategoryID, $ItemNameEn, $ItemNameCh)
        {
            $sql = "
            SELECT
            MAX(DisplayOrder)
            FROM
            INTRANET_USER_EXTRA_INFO_ITEM
            WHERE
            CategoryID = '$CategoryID'
            ";
            $tmp = $this->returnVector($sql);
            $DisplayOrder = $tmp[0]+1;

            $ItemNameEn = $this->Get_Safe_Sql_Query($ItemNameEn);
            $ItemNameCh = $this->Get_Safe_Sql_Query($ItemNameCh);
            $sql = "
            INSERT INTO INTRANET_USER_EXTRA_INFO_ITEM
            (ItemNameEn, ItemNameCh, DisplayOrder, CategoryID, RecordStatus, DateInput, DateModified, InputBy, LastModifiedBy)
            VALUES
            ('$ItemNameEn','$ItemNameCh','$DisplayOrder','$CategoryID','1',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')
			";

            return $this->db_db_query($sql);
        }

        function Update_User_Extra_Info_Display_Order($CategoryID, $ItemIDArr)
        {
            $this->Start_Trans();
            $i = 1;

            foreach($ItemIDArr as $ItemID)
            {
                $sql = "
                UPDATE
                INTRANET_USER_EXTRA_INFO_ITEM
                SET
                DisplayOrder = $i,
                LastModifiedBy = '".$_SESSION['UserID']."',
                DateModified = NOW()
                WHERE
                ItemID = $ItemID
                AND CategoryID = $CategoryID
                ";

                $Success[] = $this->db_db_query($sql);
                $i++;
            }

            if(!in_array(false,$Success))
            {
                $this->Commit_Trans();
                return true;
            }
            else
            {
                $this->RollBack_Trans();
                return false;
            }
        }

        function Remove_User_Extra_Info_Item($ItemID)
        {
            $UpdateArr['RecordStatus'] = 0;
            $Success = $this->Update_User_Extra_Info_Item($ItemID,$UpdateArr);

            return $Success;
        }

        function Update_User_Extra_Info_Item($ItemID,$UpdateArr)
        {
            global $intranet_root;
            foreach((array)$UpdateArr as $field => $val)
            {
                $updateSqlArr[] = " $field = '".$this->Get_Safe_Sql_Query($val)."' ";
            }
            $updateSql = implode(", ",(array)$updateSqlArr);

            $sql = "
            UPDATE
            INTRANET_USER_EXTRA_INFO_ITEM
            SET
            $updateSql,
            LastModifiedBy = '".$_SESSION['UserID']."',
					DateModified = NOW()
				WHERE
					ItemID IN (".implode(",",(array)$ItemID).")
			";

            $Success = $this->db_db_query($sql);
            if((isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!="")){
                include_once($intranet_root."/includes/libcurdlog.php");
                $log = new libcurdlog();
                if($afterChanges!="" && $beforeChanges!=""){
                    $log->addlog("User Settings", "UPDATE", "INTRANET_USER_EXTRA_INFO_ITEM", "UserID", $uid, "", implode(";",(array)$updateSqlArr), $sql);
                }
                unset($log);
            }
            return $Success;
        }

        function Insert_User_Extra_Info_Category($CategoryCode, $CategoryNameEn, $CategoryNameCh, $RecordType)
        {
            $sql = "
				SELECT
					MAX(DisplayOrder)
				FROM
					INTRANET_USER_EXTRA_INFO_CATEGORY
			";
            $tmp = $this->returnVector($sql);
            $DisplayOrder = $tmp[0]+1;

            $CategoryNameEn = $this->Get_Safe_Sql_Query($CategoryNameEn);
            $CategoryNameCh = $this->Get_Safe_Sql_Query($CategoryNameCh);
            $CategoryCode = $this->Get_Safe_Sql_Query($CategoryCode);

            $sql = "
            INSERT INTO INTRANET_USER_EXTRA_INFO_CATEGORY
            (CategoryCode, CategoryNameEn, CategoryNameCh, DisplayOrder, RecordType, RecordStatus, DateInput, DateModified, InputBy, LastModifiedBy)
            VALUES
            ('$CategoryCode','$CategoryNameEn','$CategoryNameCh','$DisplayOrder','$RecordType','1',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')
			";
            $Result = $this->db_db_query($sql);

            return $Result;

        }

        function Is_Category_Code_Valid($CategoryCode, $CategoryID='')
        {
            if(trim($CategoryID)!='')
                $cond_CategoryID = " AND CategoryID <> $CategoryID ";
                $CategoryCode = $this->Get_Safe_Sql_Query($CategoryCode);
                $sql = "
                SELECT
                COUNT(*)
                FROM
                INTRANET_USER_EXTRA_INFO_CATEGORY
                WHERE
                RecordStatus = 1
                AND CategoryCode = '$CategoryCode'
                $cond_CategoryID
                ";
                $Result = $this->returnVector($sql);
                $IsValid = $Result[0]==0;

                return $IsValid;
        }

        function Update_User_Extra_Info_Category($CategoryID, $UpdateArr)
        {
            foreach((array)$UpdateArr as $field => $val)
            {
                $updateSqlArr[] = " $field = '".$this->Get_Safe_Sql_Query($val)."' ";
            }
            $updateSql = implode(", ",(array)$updateSqlArr);

            $sql = "
            UPDATE
            INTRANET_USER_EXTRA_INFO_CATEGORY
            SET
            $updateSql,
            LastModifiedBy = '".$_SESSION['UserID']."',
					DateModified = NOW()
				WHERE
					CategoryID IN (".implode(",",(array)$CategoryID).")
			";

            $Success = $this->db_db_query($sql);

            return $Success;
        }

        function Get_User_Extra_Info_Category($CategoryID='', $conds='')
        {
            if(trim($CategoryID!=''))
                $cond_CategoryID = " AND eic.CategoryID = '$CategoryID' ";

                $sql = "
                SELECT
                *
                FROM
                INTRANET_USER_EXTRA_INFO_CATEGORY eic
                WHERE
                1
                AND eic.RecordStatus = 1
                $cond_CategoryID
                $conds
                ORDER BY
                eic.DisplayOrder
                ";

                $result = $this->returnArray($sql);

                return $result;
        }

        function Get_User_Extra_Info_Item($ItemID='', $CategoryID='', $conds='')
        {
            if(trim($ItemID!=''))
                $cond_ItemID = " AND eii.ItemID = '$ItemID' ";
                if(trim($CategoryID!=''))
                    $cond_CategoryID = " AND eic.CategoryID = '$CategoryID' ";

                    $sql = "
                    SELECT
                    *
                    FROM
                    INTRANET_USER_EXTRA_INFO_ITEM eii
                    INNER JOIN INTRANET_USER_EXTRA_INFO_CATEGORY eic ON eii.CategoryID = eic.CategoryID
                    WHERE
                    1
                    AND eii.RecordStatus = 1
                    AND eic.RecordStatus = 1
                    $cond_CategoryID
                    $cond_ItemID
                    $conds
                    ORDER BY
                    eic.DisplayOrder,
                    eii.DisplayOrder
                    ";

                    $result = $this->returnArray($sql);

                    return $result;
        }

        function Get_User_Extra_Info_UI($ParUserID='', $ParExtraInfo=array(), $ExtraOthers=array(), $NewFieldAry=array())
        {
            global $Lang, $linterface, $sys_custom;

            $ExtraInfoItemArr = $this->Get_User_Extra_Info_Item();
            $ExtraInfoCategoryArr = $this->Get_User_Extra_Info_Category();

            $ExtraInfoOthersCodeArr = BuildMultiKeyAssoc($ExtraInfoItemArr, "ItemID", "ItemCode", 1);

            $ItemNameLang = Get_Lang_Selection("ItemNameCh","ItemNameEn");
            $CategoryNameLang = Get_Lang_Selection("CategoryNameCh","CategoryNameEn");

            $ExtraInfoItemArr = BuildMultiKeyAssoc($ExtraInfoItemArr, array("CategoryID","ItemID"), $ItemNameLang,1);

            if(!empty($ParExtraInfo)) {
                $ExtraInfo = $ParExtraInfo;
            } else if(trim($ParUserID)!='')
            {
                $Mapping = $this->Get_User_Extra_Info_Mapping($ParUserID);

                $ExtraInfo = Get_Array_By_Key($Mapping, "ItemID");
                $ExtraInfoOthersStoredArr = (count($ExtraOthers)==0) ? BuildMultiKeyAssoc($Mapping, "ItemID", "OtherInfo", 1) : $ExtraOthers;

            }
            if(count($ExtraOthers)>0) $ExtraInfoOthersStoredArr = $ExtraOthers;

            $x .= '<tr>'."\n";
            $x .= '<td colspan=2>'."\n";
            $x .= '<em class="form_sep_title"> - '.$Lang['AccountMgmt']['UserExtraInfo'].' -</em>';
            $x .= '</td>'."\n";
            $x .= '</tr>'."\n";

            if($sys_custom['StudentMgmt']['BlissWisdom']) {
                $prefix = $this->Get_Bliss_ExtraInfo('prefix', $ParUserID, $NewFieldAry);
                $subfix = $this->Get_Bliss_ExtraInfo('subfix', $ParUserID, $NewFieldAry);
            }

            $x .= $prefix;

            foreach((array)$ExtraInfoCategoryArr as $CategoryInfo)
            {
                if(count($ExtraInfoItemArr[$CategoryInfo['CategoryID']])==0)
                    continue;

                    $x .= '<tr>'."\n";
                    $x .= '<td class="formfieldtitle" >'."\n";
                    $x .= $CategoryInfo[$CategoryNameLang];
                    $x .= '</td>'."\n";
                    $x .= '<td>'."\n";

                    if($CategoryInfo['RecordType']==2)
                        $x .= $linterface->Get_Radio_Button("ItemNA".$CategoryInfo['CategoryID'],"ExtraInfo2_".$CategoryInfo['CategoryID']."[]","",1,"ExtraInfoChk",$Lang['General']['NotApplicable'])."<br>\n";
                        foreach((array)$ExtraInfoItemArr[$CategoryInfo['CategoryID']] as $ItemID => $ItemName)
                        {
                            $isChk = in_array($ItemID,(array)$ExtraInfo);
                            if($CategoryInfo['RecordType']==1) {
                                $x .= $linterface->Get_Checkbox("Item".$ItemID,"ExtraInfo1[]",$ItemID,$isChk,"ExtraInfoChk",$ItemName);
                            } else {
                                $x .= $linterface->Get_Radio_Button("Item".$ItemID,"ExtraInfo2_".$CategoryInfo['CategoryID']."[]",$ItemID,$isChk,"ExtraInfoChk",$ItemName);
                            }
                            if(substr($ExtraInfoOthersCodeArr[$ItemID],-2)=="_O") {
                                $x .= " ".$linterface->GET_TEXTBOX_NUMBER("ItemOther_".$ExtraInfoOthersCodeArr[$ItemID], "ExtraOthers_".$ItemID, $ExtraInfoOthersStoredArr[$ItemID]);
                            }
                            $x .= "<br>\n";
                        }
                        $x .= '</td>'."\n";
                        $x .= '</tr>'."\n";
            }

            $x .= $subfix;


            return $x;
        }

        function Insert_User_Extra_Info_Mapping($ParUserID, $UserExtraInfo, $ExtraOthers=array())
        {
            global $UserID;

            if(empty($UserExtraInfo)) return false;

            foreach((array)$UserExtraInfo as $thisItemID)
            {
                if(trim($thisItemID)!='') {
                    $thisExtraOtherInfo = ($ExtraOthers[$thisItemID]!="") ? "'".$ExtraOthers[$thisItemID]."'," : "NULL,";

                    $ValueArr[]= "($ParUserID,$thisItemID,$thisExtraOtherInfo $UserID,NOW())";
                }
            }

            if(empty($ValueArr)) return false;

            $ValueSql = implode(",",$ValueArr);

            $sql = "
            INSERT INTO INTRANET_USER_EXTRA_INFO_MAPPING
            (UserID,ItemID,OtherInfo,LastModifiedBy,DateModified)
            VALUES
            $ValueSql
            ";

            $success = $this->db_db_query($sql);

            return $success;
        }

        function Delete_User_Extra_Info_Mapping($ParUserID, $UserExtraInfo='')
        {
            if(!empty($UserExtraInfo))
                $cond_ItemID = " AND ItemID IN (".implode(",",$UserExtraInfo).")";

                $sql = "
                DELETE FROM
                INTRANET_USER_EXTRA_INFO_MAPPING
                WHERE
                UserID = $ParUserID
                $cond_ItemID
                ";

                $success = $this->db_db_query($sql);

                return $success;
        }

        function Update_User_Extra_Info_Mapping($ParUserID, $UserExtraInfo, $ExtraOthers=array())
        {
            $Success["DeleteMapping"] = $this->Delete_User_Extra_Info_Mapping($ParUserID);
            $Success["AddMapping"] = $this->Insert_User_Extra_Info_Mapping($ParUserID, $UserExtraInfo, $ExtraOthers);

            return !in_array(false,(array)$Success);
        }

        function Get_User_Extra_Info_Mapping($ParUserID='')
        {
            if(trim($ParUserID)!='')
                $cond_UserID = " AND eim.UserID = $ParUserID ";

                $sql = "
                SELECT
                *
                FROM
                INTRANET_USER_EXTRA_INFO_MAPPING eim
                INNER JOIN INTRANET_USER_EXTRA_INFO_ITEM eii ON eim.ItemID = eii.ItemID
                INNER JOIN INTRANET_USER_EXTRA_INFO_CATEGORY eic ON eii.CategoryID = eic.CategoryID
                WHERE
                1
                AND eii.RecordStatus = 1
                AND eic.RecordStatus = 1
                $cond_UserID
                ";

                $result = $this->returnArray($sql);

                return $result;
        }

        //eRC Customization (Signature Upload)
        function getTeacherSignatureDBTable($field='',$order='',$page='',$Keyword='', $Filter='')
        {
            global $Lang, $page_size, $ck_account_mgmt_teacher_signature_page_size;

            include_once("libdbtable.php");
            include_once("libdbtable2007a.php");

            $field = ($field=='')? 1 : $field;
            $order = ($order=='')? 1 : $order;
            $page = ($page=='')? 1 : $page;

            if (isset($ck_account_mgmt_teacher_signature_page_size) && $ck_account_mgmt_teacher_signature_page_size != "") $page_size = $ck_account_mgmt_teacher_signature_page_size;
            $li = new libdbtable2007($field,$order,$page);

            $sql = $this->getTeacherSignatureDBTableSql($Keyword,$Filter);

            $li->sql = $sql;
            $li->IsColOff = "IP25_table";

            $li->field_array = array("NameField", "UserLogin","SignatureLink");
            $li->column_array = array(0,0);
            $li->wrap_array = array(0,0);

            $pos = 0;
            $li->column_list .= "<th width='1%' class='tabletoplink'>#</td>\n";
            $li->column_list .= "<th width='30%'>".$li->column_IP25($pos++, $Lang['AccountMgmt']['UserName'])."</td>\n";
            $li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['AccountMgmt']['LoginID'])."</td>\n";
            $li->column_list .= "<th width='125px'>". $Lang['AccountMgmt']['Signature']."</td>\n";
            $li->column_list .= "<th width='1%'>".$li->check("UserLogin[]")."</td>\n";
            $li->no_col = $pos+3;

            $x .= $li->display();

            $x .= '<input type="hidden" name="pageNo" value="'.$li->pageNo.'" />';
            $x .= '<input type="hidden" name="order" value="'.$li->order.'" />';
            $x .= '<input type="hidden" name="field" value="'.$li->field.'" />';
            $x .= '<input type="hidden" name="page_size_change" value="" />';
            $x .= '<input type="hidden" name="numPerPage" value="'.$li->page_size.'" />';

            return $x;

        }

        function getTeacherSignatureDBTableSql($Keyword='',$Filter='')
        {
            $NameField = getNameFieldByLang("iu.");

            if(trim($Keyword)!='')
            {
                $Keyword = $this->Get_Safe_Sql_Like_Query($Keyword);
                $cond_Keyword = "
                AND
                (
                iu.UserLogin LIKE '%$Keyword%' OR
                iu.EnglishName LIKE '%$Keyword%' OR
                iu.ChineseName LIKE '%$Keyword%'
                )
                ";
            }

            if(!empty($Filter))
            {
                if($Filter==1)
                    $cond_Filter = " AND (ius.SignatureLink IS NOT NULL AND TRIM(ius.SignatureLink) <> '') ";
                    else
                        $cond_Filter = " AND (ius.SignatureLink IS NULL OR TRIM(ius.SignatureLink) = '') ";
            }

            $sql = "
            SELECT
            $NameField NameField,
            iu.UserLogin,
            IF(ius.SignatureLink IS NULL OR TRIM(ius.SignatureLink) = '',
            '&nbsp;',
            CONCAT('<img src=\"',ius.SignatureLink,'\" width=\"120\" height=\"55\">')
            ) AS SignatureLink,
            IF(ius.SignatureLink IS NULL OR TRIM(ius.SignatureLink) = '',
            '&nbsp;',
            CONCAT('<input type=\"checkbox\" name=\"UserLogin[]\" value=\"',iu.UserLogin,'\">')
            ) AS CheckBox
            FROM
            INTRANET_USER iu
            LEFT JOIN INTRANET_USER_SIGNATURE ius ON iu.UserLogin = ius.UserLogin
            WHERE
            iu.RecordType = '".TYPE_TEACHER."'
            AND iu.RecordStatus = 1
            $cond_Keyword
            $cond_Filter
            ";

            return $sql;
        }

        function updateSignatureLink($ParUserLoginSignatureMapping)
        {

            if(count($ParUserLoginSignatureMapping)==0) return 0;

            $result['Delete'] = $this->deleteSignatureLink(array_keys((array)$ParUserLoginSignatureMapping));
            $result['Insert'] = $this->insertSignatureLink($ParUserLoginSignatureMapping);

            return !in_array(0,$result)?1:0;
        }

        function deleteSignatureLink($ParUserLogin)
        {
            if(count($ParUserLogin)==0) return 0;

            $ParUserLoginSql = "'".implode("','",(array)$ParUserLogin)."'";

            $sql = "
            DELETE FROM
            INTRANET_USER_SIGNATURE
            WHERE
            UserLogin IN ($ParUserLoginSql);
            ";


            return $this->db_db_query($sql)?1:0;
        }

        function insertSignatureLink($ParUserLoginSignatureMapping)
        {
            if(count($ParUserLoginSignatureMapping)==0) return 0;

            foreach($ParUserLoginSignatureMapping as $thisUserLogin=> $thisLink)
            {
                $SqlArr[] = "('$thisUserLogin','$thisLink',NOW(),'".$_SESSION['UserID']."')";
            }
            $UserIDSql = implode(",",$SqlArr);

            $sql = "
            INSERT INTO INTRANET_USER_SIGNATURE
            (UserLogin,SignatureLink,DateInput,InputBy)
            VALUES
            $UserIDSql
            ";

            return $this->db_db_query($sql)?1:0;
        }

        function getOfficalPhotoMgmtMemberTable($Keyword="")
        {
            global $Lang, $i_no_record_exists_msg, $i_Discipline_System_Access_Right_Added_Date;

            $name_field = getNameFieldByLang("USR.");

            if($Keyword!="")
                $conds = " AND $name_field LIKE '%$Keyword%'";

                $sql = "SELECT
                $name_field as name,
                LEFT(gp.DateInput,10) as DateInput,
                USR.Teaching,
                USR.UserID
                FROM
                STUDENT_OFFICAL_PHOTO_MGMT_GROUP gp INNER JOIN
                INTRANET_USER USR ON (USR.UserID=gp.UserID)
                WHERE
                USR.RecordStatus IN (0,1,2) $conds
                ";

                $data = $this->returnArray($sql,3);

                $x .= '
					<table class="common_table_list">
					  <thead>
					    <tr>
					      <th class="num_check" width="1">#</th>
					      <th width="60%">'.$Lang['StaffAttendance']['StaffName'].'</th>
						  <th width="20%">'.$Lang['StaffAttendance']['UserType'].'</th>
					      <th width="20%">'.$i_Discipline_System_Access_Right_Added_Date.'</th>
					      <th class="num_check" width="1"><input type="checkbox" id="checkall" name="checkall" onclick="Set_Checkbox_Value(\'userID[]\',this.checked)" /></th>
					      </tr>
					  </thead>
					  <tbody>';

                for($i=0;$i<sizeof($data);$i++)
                {
                    $x .= '<tr>
					      <td>'.($i+1).'</td>
					      <td>'.$data[$i]['name'].'</td>
					      <td>'.($data[$i]['Teaching']==1?$Lang['Identity']['TeachingStaff']:$Lang['Identity']['NonTeachingStaff']).'</td>
						  <td>'.$data[$i]['DateInput'].'</td>
					      <td><input type="checkbox" id="userID[]" name="userID[]" value="'.$data[$i]['UserID'].'" /></td>
					   </tr>';
                }
                if(sizeof($data)==0) {
                    $x .= '<tr><td colspan="5" height="40" align="center">'.$i_no_record_exists_msg.'</td></tr>';
                }

                $x .= '</tbody>
				  </table>';

                return $x;
        }

        function removeOfficalPhotoMgmtGroupMember($userID=array())
        {
            if(sizeof($userID)>0) {
                $sql = "DELETE FROM STUDENT_OFFICAL_PHOTO_MGMT_GROUP WHERE UserID IN (".implode(',', $userID).")";
                return $this->db_db_query($sql);
            }
        }

        function getOfficalPhotoMgmtMemberID()
        {
            # cache the data to session to save the query running in home_header
            global $_SESSION;

            if ($_SESSION['student_official_photo_mgmt_group_ids']!="")
            {
                if ($_SESSION['student_official_photo_mgmt_group_ids']=="NULL_RECORD")
                {
                    return array();
                } else
                {
                    return explode(",", $_SESSION['student_official_photo_mgmt_group_ids']);
                }
            } else
            {
                $sql = "SELECT UserID FROM STUDENT_OFFICAL_PHOTO_MGMT_GROUP";
                $results = $this->returnVector($sql);
                $_SESSION['student_official_photo_mgmt_group_ids'] = (sizeof($results)>0) ? implode(",", $results) : "NULL_RECORD";

                return $results;
            }
        }

        function addOfficalPhotoMgmtGroupMember($userID=array())
        {
            if(sizeof($userID)>0) {
                # check duplication
                $studentAry = $this->getOfficalPhotoMgmtMemberID();
                foreach($userID as $id) {
                    if($id!="" && !in_array($id, $studentAry))
                        $ary[] = $id;
                }

                $delim = "";

                if(sizeof($ary)>0) {
                    $sql = "INSERT IGNORE INTO STUDENT_OFFICAL_PHOTO_MGMT_GROUP (UserID, DateInput) VALUES ";
                    foreach($ary as $id) {
                        $sql .= $delim."('$id', NOW())";
                        $delim = ", ";
                    }
                    return $this->db_db_query($sql);
                }
            }
        }

        function returnEmailNotificationData($email="", $userlogin="", $UserPassword="", $engname="", $chiname="", $UpdatePassword=0)
        {
            global $Lang;

            $website = get_website();
            $webmaster = get_webmaster();

            if($UpdatePassword)
            {
                $email_subject = $Lang['EmailNotification']['UpdatePassword']['Subject'];
                $email_content = $Lang['EmailNotification']['UpdatePassword']['Content'];
            }
            else
            {
                $email_subject = $Lang['EmailNotification']['Registration']['Subject'];
                $email_content = $Lang['EmailNotification']['Registration']['Content'];
            }

            $email_content = str_replace("__ENGNAME__", $engname, $email_content);
            $email_content = str_replace("__CHINAME__", $chiname, $email_content);
            $email_content = str_replace("__WEBSITE__", $website, $email_content);
            $email_content = str_replace("__EMAIL__", $email, $email_content);
            $email_content = str_replace("__LOGIN__", $userlogin, $email_content);
            $email_content = str_replace("__PASSWORD__", $UserPassword, $email_content);

            $email_footer = $Lang['EmailNotification']['Footer'];
            $email_footer = str_replace("__WEBSITE__", $website, $email_footer);
            $email_footer = str_replace("__WEBMASTER__", $webmaster, $email_footer);
            $email_content .= $email_footer;

            return array($email_subject, $email_content);
        }

        function returnEmailNotificationData_SendPassword($email="", $userlogin="", $UserPassword="", $engname="", $chiname="")
        {
            global $Lang;

            $website = get_website();
            $webmaster = get_webmaster();

            $email_subject = $Lang['EmailNotification']['SendPassword']['Subject'];
            $email_content = $Lang['EmailNotification']['SendPassword']['Content'];

            $email_content = str_replace("__ENGNAME__", $engname, $email_content);
            $email_content = str_replace("__CHINAME__", $chiname, $email_content);
            $email_content = str_replace("__WEBSITE__", $website, $email_content);
            $email_content = str_replace("__EMAIL__", $email, $email_content);
            $email_content = str_replace("__LOGIN__", $userlogin, $email_content);
            $email_content = str_replace("__PASSWORD__", $UserPassword, $email_content);

            $email_footer = $Lang['EmailNotification']['Footer'];
            $email_footer = str_replace("__WEBSITE__", $website, $email_footer);
            $email_footer = str_replace("__WEBMASTER__", $webmaster, $email_footer);
            $email_content .= $email_footer;

            return array($email_subject, $email_content);
        }

        function Get_Bliss_ExtraInfo($type, $thisUserID, $NewFieldAry=array())
        {
            global $linterface, $Lang;

            include_once("libuserpersonalsettings.php");
            $libps = new libuserpersonalsettings();

            $content = "";

            $FieldAry = $Lang['AccountMgmt']['BlissWisdomFieldAry'];
            $ResultAry = $libps->Get_Setting($thisUserID, $FieldAry);

            $thisOccupation = $NewFieldAry['Occupation'] ? $NewFieldAry['Occupation'] : $ResultAry['Occupation'];
            $thisCompany = $NewFieldAry['Company'] ? $NewFieldAry['Company'] : $ResultAry['Company'];
            $thisPassportNo = $NewFieldAry['PassportNo'] ? $NewFieldAry['PassportNo'] : $ResultAry['PassportNo'];
            $thisPassport_ValidDate = $NewFieldAry['Passport_ValidDate'] ? $NewFieldAry['Passport_ValidDate'] : $ResultAry['Passport_ValidDate'];
            if($thisPassport_ValidDate=='0000-00-00') $thisPassport_ValidDate = "";
            $thisHowToKnowBlissWisdom = $NewFieldAry['HowToKnowBlissWisdom'] ? $NewFieldAry['HowToKnowBlissWisdom'] : $ResultAry['HowToKnowBlissWisdom'];
            $thisHowToKnowAry = explode('::', $thisHowToKnowBlissWisdom);
            $thisHowToKnowOption = $thisHowToKnowAry[0];
            $thisHowToKnowOptionField = $thisHowToKnowAry[1];

            if($type=='prefix') {

                $content .= "<tr><td class='formfieldtitle'>";
                $content .= $Lang['AccountMgmt']['Occupation'];
                $content .= "</td><td align='left' class='textnum'>";
                $content .= $linterface->GET_TEXTBOX_NAME($id='Occupation', $name='Occupation', $value=$thisOccupation);
                $content .= "</tr>";

                $content .= "<tr><td class='formfieldtitle'>";
                $content .= $Lang['AccountMgmt']['Company'];
                $content .= "</td><td align='left' class='textnum'>";
                $content .= $linterface->GET_TEXTBOX_NAME($id='Company', $name='Company', $value=$thisCompany);
                $content .= "</tr>";

            } else {

                $content .= "<tr><td class='formfieldtitle'>";
                $content .= $Lang['AccountMgmt']['PassportNo'];
                $content .= "</td><td align='left' class='textnum'>";
                $content .= $linterface->GET_TEXTBOX_NAME($id='PassportNo', $name='PassportNo', $value=$thisPassportNo);
                $content .= "</tr>";

                $content .= "<tr><td class='formfieldtitle'>";
                $content .= $Lang['AccountMgmt']['PassportValidDate'];
                $content .= "</td><td align='left' class='textnum'>";
                //$content .= $linterface->GET_TEXTBOX_NAME($id='Passport_ValidDate', $name='Passport_ValidDate', $value=$thisPassport_ValidDate);
                $content .= $linterface->GET_DATE_PICKER("Passport_ValidDate", $thisPassport_ValidDate, "", "", "", "", "", "", "", "", $AllowEmpty=1);
                $content .= "</td></tr>";

                $content .= "<tr><td class='formfieldtitle'>";
                $content .= $Lang['AccountMgmt']['KnowAboutBlissWisdom'];
                $content .= "</td><td align='left' class='textnum'>";
                //$content .= $linterface->GET_TEXTBOX_NAME($id='HowToKnowBlissWisdom', $name='HowToKnowBlissWisdom', $value=$thisHowToKnowBlissWisdom);
                foreach($Lang['AccountMgmt']['KnowAboutBlissWisdomOptions'] as $_id=>$_option) {
                    $isChk = ($_id==$thisHowToKnowOption) ? true : false;
                    $content .= $linterface->Get_Radio_Button("HowToKnowBlissWisdom".$_id, "HowToKnowBlissWisdom", $_id, $isChk);
                    $content .= "<label for='HowToKnowBlissWisdom".$_id."'>".$_option."</label>";

                    if($_id==1) {
                        $content .= "&nbsp;".$linterface->GET_TEXTBOX_NUMBER('FriendName', 'FriendName', $isChk?$thisHowToKnowOptionField:"");
                    } else if($_id==6) {
                        $content .= "&nbsp;".$linterface->GET_TEXTBOX_NUMBER('HowToKnowOthersField', 'HowToKnowOthersField', $isChk?$thisHowToKnowOptionField:"");
                    }
                    $content .= "<br>";
                }

                $content .= "</tr>";

            }

            return $content;
        }

        function GET_EXTRA_INFO_ITEM_ID_BY_ITEM_NAME($ItemName='', $CategoryID='')
        {
            if($ItemName!="") {

                if($CategoryID!="") $cond = " AND CategoryID='$CategoryID'";

                $sql = "SELECT ItemID FROM INTRANET_USER_EXTRA_INFO_ITEM WHERE (ItemNameCh='".trim($ItemName)."' OR ItemNameEn='".trim($ItemName)."') $cond";
                $tempResult = $this->returnVector($sql);

                $ItemID = $tempResult[0];

                return $ItemID;
            }
        }

        function INSERT_EXTRA_INFO_MAPPING($thisUserID, $ItemID='', $OtherField="")
        {
            global $UserID;

            if(($ItemID=='' && $OtherField=='') || $thisUserID=='') return;

            $otherField = ($OtherField=='') ? "NULL" : "'$OtherField'";

            # check any existing data
            $sql = "SELECT UserItemID FROM INTRANET_USER_EXTRA_INFO_MAPPING WHERE UserID='$thisUserID' AND ItemID='$ItemID'";
            $result = $this->returnVector($sql);

            if(count($result)>0) {
                # update
                $sql = "UPDATE INTRANET_USER_EXTRA_INFO_MAPPING SET OtherInfo=$otherField, DateModified=NOW(), LastModifiedBy='$UserID' WHERE UserItemID='$thisUserID'";
            } else {
                # insert
                $sql = "INSERT INTO INTRANET_USER_EXTRA_INFO_MAPPING SET UserID='$thisUserID', ItemID='$ItemID', OtherInfo=$otherField, DateModified=NOW(), LastModifiedBy='$UserID'";
            }

            $this->db_db_query($sql);

        }

        function DELETE_EXTRA_INFO_MAPPING($thisUserID, $CategoryID)
        {
            if($thisUserID=='' && $CategoryID=='') return;

            $sql = "DELETE FROM INTRANET_USER_EXTRA_INFO_MAPPING WHERE UserID='$thisUserID' AND ItemID IN (SELECT ItemID FROM INTRANET_USER_EXTRA_INFO_ITEM WHERE CategoryID = '$CategoryID')";
            $this->db_db_query($sql);
        }

        /*
         * @Usage: To suspend or resume mail server auto email forwarding and auto reply.
         */
        function suspendEmailFunctions($targetUserId, $resume=false)
        {
            global $intranet_root, $plugin, $special_feature, $SYS_CONFIG, $webmail_info;

            include_once($intranet_root."/includes/libwebmail.php");
            include_once($intranet_root."/includes/imap_gamma.php");

            $result = array();
            if($plugin['imail_gamma']){
                include_once($intranet_root."/includes/libpwm.php");
                $lpwm = new libpwm();
                $userIdToPwd = $lpwm->getData(array($targetUserId));
                $IMap = new imap_gamma(true);

                $sql = "SELECT ImapUserEmail, UserLogin FROM INTRANET_USER WHERE UserID='".$targetUserId."'";
                $user_record = $this->returnResultSet($sql);
                if(sizeof($user_record)==0){ // user not found
                    return true;
                }
                $imap_email = trim($user_record[0]['ImapUserEmail']);
                $user_login = $user_record[0]['UserLogin'];
                $email_disabled = false;
                if($imap_email == ''){
                    $imap_email = $user_login.'@'.$SYS_CONFIG['Mail']['UserNameSubfix'];
                    $email_disabled = true; // originally the email account was disabled
                }

                // also resume or suspend email account for iMail plus postfix type email
                if(!$email_disabled) // if original email account is not in disable state, suspend or resume it
                {
                    if($resume){
                        $IMap->setUnsuspendUser($imap_email,"iMail",$userIdToPwd[$targetUserId]);
                    }else{
                        $IMap->setSuspendUser($imap_email,"iMail");
                    }
                }

                $sql = "SELECT p.ForwardedEmail, p.ForwardKeepCopy, p.AutoReplyTxt FROM MAIL_PREFERENCE as p WHERE p.MailBoxName='$imap_email'";
                $record = $this->returnResultSet($sql);
                if(sizeof($record)==0){
                    return true;
                }
                $forward_email = trim($record[0]['ForwardedEmail']);
                $reply_text = trim($record[0]['AutoReplyTxt']);
                $keep_copy = $record[0]['ForwardKeepCopy'];
                $emailList = array($imap_email);

                if($forward_email != ''){
                    if($resume){
                        $forwarding_emails_ary = explode("\n",$forward_email);
                        $IMap->removeMailForward($emailList);
                        $delimiter = "";
                        $forwarding_emails_ary_count = count($forwarding_emails_ary);
                        for($j=0;$j<$forwarding_emails_ary_count;$j++)
                        {
                            $thisEmail = $forwarding_emails_ary[$j];
                            $thisEmail = trim($thisEmail);
                            if($thisEmail == '' || $thisEmail == $imap_email){
                                continue;
                            }
                            if($forwarding_emails_ary_count > 0 && $j<($forwarding_emails_ary_count-1)){
                                $result[$thisEmail] = $IMap->setMailForward($thisEmail, 1, $emailList); // non last forwarding email must keep copy
                            }else{
                                $result[$thisEmail] = $IMap->setMailForward($thisEmail, $keep_copy, $emailList);
                            }
                        }
                    }else{
                        $result['RemoveForwardEmail'] = $IMap->removeMailForward($emailList);
                    }
                }
                if($reply_text != ''){
                    if($resume){
                        $result['ResumeAutoReply'] = $IMap->setAutoReply($reply_text, $emailList);
                    }else{
                        $result['RemoveAutoReply'] = $IMap->removeAutoReply("", $emailList);
                    }
                }
            }else if($special_feature['imail']){
                $lwebmail = new libwebmail();

                $sql = "SELECT p.ForwardedEmail, p.ForwardKeepCopy, p.AutoReplyTxt, u.UserLogin FROM INTRANET_USER as u
						INNER JOIN INTRANET_IMAIL_PREFERENCE as p ON p.UserID=u.UserID WHERE u.UserID='".$targetUserId."'";
                $record = $this->returnResultSet($sql);
                if(sizeof($record)==0){
                    return true;
                }
                $forward_email = trim($record[0]['ForwardedEmail']);

                $user_login = trim($record[0]['UserLogin']);
                $keep_copy = $record[0]['ForwardKeepCopy'];
                if($resume)
                {
                    // when resume get the saved auto reply content from db
                    $reply_text = trim($record[0]['AutoReplyTxt']);
                }else{
                    // read auto reply content from server
                    $reply_text = stripslashes($lwebmail->readAutoReply($user_login));
                }
                if($forward_email != ''){
                    if($resume){
                        $temp = explode("\n",$forward_email);
                        $delimiter = "";
                        $targetEmail="";
                        for($i=0;$i<sizeof($temp);$i++){
                            $t_mail = trim($temp[$i]);
                            if(intranet_validateEmail($t_mail)){
                                $targetEmail .=$delimiter.$t_mail;
                                $delimiter = ",";
                            }
                        }
                        $result['ResumeForwardEmail'] = $lwebmail->setMailForward($user_login, $targetEmail, $keep_copy);
                    }else{
                        $result['RemoveForwardEmail'] =  $lwebmail->removeMailForward($user_login);
                    }
                }
                if($reply_text != ''){
                    if($resume){
                        $result['ResumeAutoReply'] = $lwebmail->addAutoReply($user_login, $reply_text);
                        $sql = "UPDATE INTRANET_IMAIL_PREFERENCE SET AutoReplyTxt=NULL,DateModified=NOW() WHERE UserID='$targetUserId'";
                        $this->db_db_query($sql);
                    }else{
                        $sql = "UPDATE INTRANET_IMAIL_PREFERENCE SET AutoReplyTxt='$reply_text',DateModified=NOW() WHERE UserID='$targetUserId'";
                        $this->db_db_query($sql);
                        if($this->db_affected_rows()==0){
                            $sql = "INSERT INTO INTRANET_IMAIL_PREFERENCE (UserID,AutoReplyTxt,DateInput,DateModified) VALUES ('$targetUserId','$reply_text',NOW(),NOW())";
                            $this->db_db_query($sql);
                        }
                        $result['RemoveAutoReply'] = $lwebmail->removeAutoReply($user_login);
                    }
                }
            }

            return !in_array(false,$result);
        }

        function approveUsers($userIdAry)
        {
            global $PATH_WRT_ROOT, $plugin, $intranet_db, $intranet_root, $sys_custom;
            global $intranet_session_language;	// for liblibrarymgmt_group_manage.php

            if (!defined("LIBUSER_DEFINED")){
                include_once("libuser.php");
            }

            $luser = new libuser();
            $successAry = array();

            $dbModifiedByUserId = ($_SESSION['UserID']=='')? 'null' : $_SESSION['UserID'];
            $sql = "UPDATE INTRANET_USER SET RecordStatus = '1', DateModified = now(), ModifyBy = $dbModifiedByUserId WHERE UserID IN (".implode(",", $userIdAry).")";
            $successAry['updateIntranetUserDb'] = $this->db_db_query($sql);
            if((isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!="")){
                include_once($intranet_root."/includes/libcurdlog.php");
                $log = new libcurdlog();
                $userSql = "SELECT EnglishName FROM INTRANET_USER WHERE UserID IN ('".implode("','", $userIdAry)."')";
                $stdList = $log->returnVector($userSql);
                $log->addlog("User Settings", "UPDATE", "INTRANET_USER", "UserID", implode(",", $userIdAry), "", "APPROVE: ".implode(",",$stdList), $sql);
                unset($log);
            }

            $successAry['synUserDataToModules'] = $luser->synUserDataToModules($userIdAry);

            if($plugin['radius_server']){
                global $intranet_db_user, $intranet_db_pass, $sys_custom, $plugin, $radius_db_host, $radius_db_name, $radius_db_username, $radius_db_password;
                $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID IN (".implode(",", $userIdAry).")";
                $userlogin_list = $this->returnVector($sql);
                intranet_closedb();// close the intranet db
                $this->opendb($radius_db_host,$radius_db_username,$radius_db_password,$radius_db_name); // open the radius server db
                $sql = "DELETE FROM ".$radius_db_name.".radusergroup WHERE username IN ('".implode("','",$userlogin_list)."')";
                $successAry['removeRadiusServerDisableUserRecord'] = $this->db_db_query($sql);
                $this->closedb(); // close the radius server db
                intranet_opendb(); // reopen the intranet db
                $this->db = $intranet_db;
            }

            // Resume mail server email forwarding function and auto reply function
            foreach($userIdAry as $single_user_id)
            {
                $this->suspendEmailFunctions($single_user_id, true);
                $luser->insertAccountLog($single_user_id, 'U');
            }

            return $successAry;
        }

        function suspendUsers($userIdAry)
        {
            global $PATH_WRT_ROOT, $plugin, $intranet_db, $intranet_root, $sys_custom;
            global $intranet_session_language;	// for liblibrarymgmt_group_manage.php

            if (!defined("LIBUSER_DEFINED")){
                include_once("libuser.php");
            }

            $luser = new libuser();
            $successAry = array();

            $dbModifiedByUserId = ($_SESSION['UserID']=='')? 'null' : $_SESSION['UserID'];
            $sql = "UPDATE INTRANET_USER SET RecordStatus = '0', DateModified = now(), ModifyBy = $dbModifiedByUserId WHERE UserID IN (".implode(",", $userIdAry).")";
            $successAry['updateIntranetUserDb'] = $this->db_db_query($sql);
            if((isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!="")){
                include_once($intranet_root."/includes/libcurdlog.php");
                $log = new libcurdlog();
                $userSql = "SELECT EnglishName FROM INTRANET_USER WHERE UserID IN ('".implode("','", $userIdAry)."')";
                $stdList = $log->returnVector($userSql);
                $log->addlog("User Settings", "UPDATE", "INTRANET_USER", "UserID", implode(",", $userIdAry), "", "SUSPEND: ".implode(",",$stdList), $sql);
                unset($log);
            }

            $successAry['synUserDataToModules'] = $luser->synUserDataToModules($userIdAry);

            if($plugin['radius_server']){
                global $intranet_db_user, $intranet_db_pass, $sys_custom, $plugin, $radius_db_host, $radius_db_name, $radius_db_username, $radius_db_password;
                $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID IN (".implode(",", $userIdAry).")";
                $intranet_user_list = $this->returnVector($sql);

                intranet_closedb();// close the intranet db
                if (!defined("LIBUSER_DEFINED")){
                    include_once("libuser.php");
                }
                $lu = new libuser();
                $this->opendb($radius_db_host,$radius_db_username,$radius_db_password,$radius_db_name); // open the radius server db

                $sql = "SELECT username FROM ".$radius_db_name.".radusergroup WHERE username IN ('".implode("','", $intranet_user_list)."')";
                $radius_user_list = $this->returnVector($sql);
                $user_list = array_values(array_unique(array_diff($intranet_user_list,$radius_user_list)));
                $user_count = count($user_list);
                $insert_values = "";
                for($i=0;$i<$user_count;$i++)
                {
                    $delim = $insert_values != ''? "," : "";
                    $insert_values .= $delim."('".$user_list[$i]."','disabled_user','1')";
                }
                if($insert_values != ''){
                    $sql = "INSERT INTO ".$radius_db_name.".radusergroup (username,groupname,priority) VALUES ".$insert_values;
                    $successAry['addRadiusServerDisableUser'] = $this->db_db_query($sql);
                }

                $this->closedb(); // close the radius server db
                intranet_opendb(); // reopen the intranet db
                $this->db = $intranet_db;
            }

            // Suspend mail server email forwarding function and auto reply function
            foreach($userIdAry as $single_user_id)
            {
                $this->suspendEmailFunctions($single_user_id, false);
                $luser->insertAccountLog($single_user_id, 'U');
            }

            return $successAry;
        }

        /*
         * if $userlogin is array, would return associative array of disabled userlogin array $userloginToDisabledStatus[userlogin] = 1;
         */
        function isUserDisabledInRadiusServer($userlogin)
        {
            global $plugin, $intranet_db, $intranet_db_user, $intranet_db_pass, $sys_custom, $plugin, $radius_db_host, $radius_db_name, $radius_db_username, $radius_db_password;
            if(!$plugin['radius_server']){
                return false;
            }
            intranet_closedb();// close the intranet db
            $this->opendb($radius_db_host,$radius_db_username,$radius_db_password,$radius_db_name); // open the radius server db

            if(is_array($userlogin)){
                $userloginToDisabledStatus = array();
                $disabled_userlogin = array();
                $userlogin_chunks = array_chunk($userlogin,50);
                $userlogin_chunk_count = count($userlogin_chunks);
                for($i=0;$i<$userlogin_chunk_count;$i++){
                    $sql = "SELECT username FROM ".$radius_db_name.".radusergroup WHERE username IN ('".implode("','",$userlogin_chunks[$i])."')";
                    $tmp_disabled_userlogin = $this->returnVector($sql);
                    $disabled_userlogin = array_merge($disabled_userlogin,$tmp_disabled_userlogin);
                }
                $disabled_userlogin_count = count($disabled_userlogin);
                for($i=0;$i<$disabled_userlogin_count;$i++){
                    $userloginToDisabledStatus[$disabled_userlogin[$i]] = 1;
                }
                $result = $userloginToDisabledStatus;
            }else{
                $sql = "SELECT username FROM ".$radius_db_name.".radusergroup WHERE username='$userlogin'";
                $username_in_disable_list = $this->returnVector($sql);
                $result = count($username_in_disable_list)>0; // greater than 0 means in disable list
            }

            $this->closedb(); // close the radius server db
            intranet_opendb(); // reopen the intranet db
            $this->db = $intranet_db;

            return $result;
        }

        function disableUserInRadiusServer($userid_ary, $disable)
        {
            global $plugin, $intranet_db, $intranet_db_user, $intranet_db_pass, $sys_custom, $plugin, $radius_db_host, $radius_db_name, $radius_db_username, $radius_db_password;
            if(!$plugin['radius_server']){
                return false;
            }
            if (!defined("LIBUSER_DEFINED")){
                include_once("libuser.php");
            }
            $lu = new libuser();
            $userlogin_ary = array();
            $userid_chunks = array_chunk($userid_ary,200);
            $userid_chunk_count = count($userid_chunks);
            for($i=0;$i<$userid_chunk_count;$i++)
            {
                $userid_chunk = $userid_chunks[$i];
                $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID IN ('".implode("','",$userid_chunk)."')";
                $tmp_userlogin_ary = $this->returnVector($sql);
                foreach($userid_chunk as $uid){
                    $lu->insertAccountLog($uid, 'U');
                }
                $userlogin_ary = array_merge($userlogin_ary, $tmp_userlogin_ary);
            }

            intranet_closedb();// close the intranet db
            $this->opendb($radius_db_host,$radius_db_username,$radius_db_password,$radius_db_name); // open the radius server db

            $userlogin_chunks = array_chunk($userlogin_ary, 50);
            $chunk_count = count($userlogin_chunks);

            $result = array();
            if($disable){
                for($i=0;$i<$chunk_count;$i++){
                    $intranet_user_list = $userlogin_chunks[$i];
                    // find those users already disabled, prevent insert duplicated disable records
                    $sql = "SELECT username FROM ".$radius_db_name.".radusergroup WHERE username IN ('".implode("','", $intranet_user_list)."')";
                    $radius_user_list = $this->returnVector($sql);
                    $user_list = array_values(array_unique(array_diff($intranet_user_list,$radius_user_list)));
                    $user_count = count($user_list);
                    $insert_values = "";
                    for($i=0;$i<$user_count;$i++)
                    {
                        $delim = $insert_values != ''? "," : "";
                        $insert_values .= $delim."('".$user_list[$i]."','disabled_user','1')";
                    }
                    if($insert_values != ''){
                        $sql = "INSERT INTO ".$radius_db_name.".radusergroup (username,groupname,priority) VALUES ".$insert_values;
                        $result[] = $this->db_db_query($sql);
                    }
                }
            }else { // enable means remove users from disable list
                for($i=0;$i<$chunk_count;$i++){
                    $userlogin_list = $userlogin_chunks[$i];
                    $sql = "DELETE FROM ".$radius_db_name.".radusergroup WHERE username IN ('".implode("','",$userlogin_list)."')";
                    $result[] = $this->db_db_query($sql);
                }
            }

            $this->closedb(); // close the radius server db
            intranet_opendb(); // reopen the intranet db
            $this->db = $intranet_db;

            return !in_array(false, $result);
        }

        function getSpecialIdentityMapping($AcademicYearID="", $returnIdOnly=false){
        	// Identity
        	// 	1: Principal
        	//	2: Vice-principal
        	//	3: Chancellor
        	if($AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();
        	$name_field = getNameFieldByLang("u.");
        	$archive_name_field = getNameFieldByLang("au.");
        	$sql = "SELECT usim.UserID, usim.Identity, IFNULL($name_field, $archive_name_field) as UserName FROM INTRANET_USER_SPECIAL_IDENTITY_MAPPING usim
        			INNER JOIN INTRANET_USER u ON usim.UserID = u.UserID
        			LEFT JOIN INTRANET_ARCHIVE_USER au ON usim.UserID = au.UserID
        			WHERE usim.AcademicYearID='".$AcademicYearID."'";
        	$tmp_user_list = $this->returnArray($sql);

        	if(empty($tmp_user_list)){
        		$sql = "SELECT TermStart FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID='".$AcademicYearID."'";
        		$currTermStart = current($this->returnVector($sql));
        		$sql = "SELECT AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE TermStart < '".$currTermStart."' GROUP BY AcademicYearID ORDER BY TermStart DESC";
        		$pastYearList = $this->returnVector($sql);

        		foreach($pastYearList as $pastYearID){
        			$sql = "SELECT usim.UserID, usim.Identity, IFNULL($name_field, $archive_name_field) as UserName FROM INTRANET_USER_SPECIAL_IDENTITY_MAPPING usim
        			INNER JOIN INTRANET_USER u ON usim.UserID = u.UserID
        			LEFT JOIN INTRANET_ARCHIVE_USER au ON usim.UserID = au.UserID
        			WHERE usim.AcademicYearID='".$pastYearID."'";
        			$tmp_user_list = $this->returnArray($sql);
        			if(!empty($tmp_user_list)){
        				break;
        			}
        		}
        	}

        	$special_user_list = array();
        	if(count($tmp_user_list) > 0){
	        	foreach($tmp_user_list as $user){
	        		if(!isset($special_user_list[$user['Identity']])){
	        			$special_user_list[$user['Identity']] = array();
	        		}
	        		if($returnIdOnly){
	        			$special_user_list[$user['Identity']][] = $user['UserID'];
	        		}else{
	        			$special_user_list[$user['Identity']][] = array("id"=>$user['UserID'],"name"=>$user['UserName']);
	        		}
	        	}
        	}
        	return $special_user_list;
        }

      	function upsertSpecialIdentityMapping($AcademicYearID,$identity,$user_id){
      		$sql = "INSERT INTO INTRANET_USER_SPECIAL_IDENTITY_MAPPING (AcademicYearID, UserID, Identity, DateModified, LastModifiedBy)";
            $sql .= "VALUES ('".$AcademicYearID."','".$user_id."','".$identity."', NOW(),'".$_SESSION['UserID']."')";
            $sql .= "ON DUPLICATE KEY UPDATE Identity='".$identity."', DateModified=NOW(), LastModifiedBy='".$_SESSION['UserID']."'";
            $success = $this->db_db_query($sql);

            return $success;
      	}

      	function deleteSpecialIdentityMapping($AcademicYearID,$user_id){
      		$sql = "DELETE FROM INTRANET_USER_SPECIAL_IDENTITY_MAPPING WHERE AcademicYearID='".$AcademicYearID."' AND UserID='".$user_id."'";
            $success = $this->db_db_query($sql);

            return $success;
      	}

      	function syncToStudentRegistry_Simple_Macau($dataAry = array(), $userid, $pgAry = array()){
      		include_once('libstudentregistry.php');
      		$lsr = new libstudentregistry();
      		$result = $lsr->updateStudentRegistry_Simple_Macau($dataAry, $userid, $pgAry, $isSync = true);
      		return $result;
      	}

      	function syncToStudentRegistry_GuardianInfo($studentId, $dataAry = array()){
      		include_once('libstudentregistry.php');
      		$lsr = new libstudentregistry();
      		$lsr->removeStudentRegistry_PG($studentId, "G");
      		$lsr->insertStudentRegistry_PG($dataAry);
      	}
      	
      	function GetStudentChangeClassRecords($filterMap='')
		{
		    $cond = "";
		    if(isset($filterMap['RecordID'])){
		        if(is_array($filterMap['RecordID'])){
		            $cond .= " AND r.RecordID IN ('".implode("','",$filterMap['RecordID'])."') ";
		        }else{
		            $cond .= " AND r.RecordID='".$filterMap['RecordID']."' ";
		        }
		    }
			
			$sql = "SELECT
			            IF(u.UserID,u.EnglishName,a.EnglishName) as EnglishName,
						IF(u.UserID,u.ChineseName,a.ChineseName) as ChineseName,
			            IF(u.UserID,u.UserLogin,a.UserLogin) as UserLogin,
						r.OrgYearClassID as OrgYearClassID,
						r.CurrentYearClassID as CurrentYearClassID,
			            y1.ClassTitleEN as OrgClassName,
						r.OrgClassNumber as OrgClassNumber,
						y2.ClassTitleEN as CurrentClassName,
						r.CurrentClassNumber as CurrentClassNumber,
						r.EffectiveDate as EffectiveDate,
						r.UserID as UserID,
						r.RecordID as RecordID,
						r.AcademicYearID as AcademicYearID
			            FROM YEAR_CLASS_CHANGE_RECORD as r 
					LEFT JOIN INTRANET_USER as u ON r.UserID = u.UserID	
					LEFT JOIN INTRANET_ARCHIVE_USER as a ON r.UserID = a.UserID 
					JOIN YEAR_CLASS y1 ON y1.YearClassID = r.OrgYearClassID 
					JOIN YEAR_CLASS y2 ON y2.YearClassID = r.CurrentYearClassID
					WHERE 1 $cond";
		    
		    $records = $this->returnResultSet($sql);
		    //debug_pr($sql);
		    return $records;
		}
      	
      	function UpsertStudentChangeClassRecords($map)
      	{
		    $fields = array('RecordID','AcademicYearID','UserID','OrgYearClassID','OrgClassNumber','CurrentYearClassID','CurrentClassNumber','EffectiveDate');
		    $user_id = $_SESSION['UserID'];
		    
		    if(count($map) == 0) return false;
		    
		    if(isset($map['RecordID']) && $map['RecordID']!='' && $map['RecordID'] > 0)
		    {
		        $sql = "UPDATE YEAR_CLASS_CHANGE_RECORD SET ";
		        $sep = "";
		        foreach($map as $key => $val){
		            if(!in_array($key, $fields) || $key == 'RecordID') continue;
		            
		            $sql .= $sep."$key='".$this->Get_Safe_Sql_Query(stripslashes(trim($val)))."'";
		            $sep = ",";
		        }
		        $sql.= " ,DateModify=NOW(),ModifyBy='$user_id' WHERE RecordID='".$map['RecordID']."'";
		        $success = $this->db_db_query($sql);
		    }else{
		        $keys = '';
		        $values = '';
		        $sep = "";
		        foreach($map as $key => $val){
		            if(!in_array($key, $fields)) continue;
		            $keys .= $sep."$key";
		            $values .= $sep."'".$this->Get_Safe_Sql_Query(stripslashes(trim($val)))."'";
		            $sep = ",";
		        }
		        $keys .= ",DateInput,DateModify,InputBy,ModifyBy";
		        $values .= ",NOW(),NOW(),'$user_id','$user_id'";
		        
		        $sql = "INSERT INTO YEAR_CLASS_CHANGE_RECORD ($keys) VALUES ($values)";
		        $success = $this->db_db_query($sql);
		    }
		    
		    return $success;
		}
		
		function getStudentListByClassID($classIDAry, $selectFlag=1)
		{
			
			if(!is_array($classIDAry)) 
				$clsIDAry[0] = $classIDAry;
			else 
				$clsIDAry = $classIDAry;
				
			$academicYearID = Get_Current_Academic_Year_ID();
			
			if($selectFlag==1)
				$name_field = getNameFieldWithClassNumberByLang("USR.");
			else if($selectFlag==2)
				$name_field = "CONCAT(".Get_Lang_Selection("yc.ClassTitleB5", "ClassTitleEN").",IF(ycu.ClassNumber=NULL,'',(CONCAT('-',ycu.ClassNumber))),' ',".getNameFieldByLang("USR.").")";
			else 
				$name_field = getNameFieldWithClassNumberByLang("USR.");
			
			//$name_field = getNameFieldWithClassNumberByLang("USR.");
			
			
			
			if(sizeof($clsIDAry)>0) 
				$clsIDs = implode(',',$clsIDAry);
			
			$sql = "SELECT ycu.UserID, $name_field, ycu.ClassNumber FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) INNER JOIN INTRANET_USER USR ON (USR.UserID=ycu.UserID) WHERE USR.RecordType=2 AND USR.RecordStatus IN (0,1,2) AND yc.YearClassID IN ($clsIDs) AND yc.AcademicYearID='$academicYearID' ORDER BY yc.Sequence, ycu.ClassNumber";
			
			return $this->returnArray($sql,2);
		}
		
		function DeleteStudentChangeClassRecords($recordId)
		{
		    $sql = "DELETE FROM YEAR_CLASS_CHANGE_RECORD WHERE RecordID ";
		    if(is_array($recordId)){
		        $sql .= " IN ('".implode("','",$recordId)."')";
		    }else{
		        $sql .= " = '$recordId'";
		    }
		    $success = $this->db_db_query($sql);
		    return $success;
		}
		
    } // end of class
}        // End of directive
?>