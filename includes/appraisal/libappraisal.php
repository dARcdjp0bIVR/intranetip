<?php
// editing by Paul
/********************
 * Change Log:
 * 2019-01-15 Paul
 * 	- Add functions to deal with CPD under general settings
 * 2018-12-11 Paul
 *  - Modified getUserNameID(), show "Left" for left school teachers
 * 2018-01-05 Pun
 *  - Modified getModuleObjArr(), added cust report Learning and Teaching for all teacher
 * 2017-11-10 Paul
 * 	- Modified getSectionContent(), update to display all filled & submitted contents regardness of the batch
 * 2017-10-04 Pun
 *  - Modified getModuleObjArr(), added cust report Learning and Teaching
 ********************/

if (!defined("LIBAPPRAISAL_DEFINED")) {
    define("LIBAPPRAISAL_DEFINED", true);
    
    class libappraisal extends libdb {
        var $ModuleTitle;
        
        function libappraisal() {
            global $appraisalConfig;
            
            $this->libdb();
            $this->ModuleTitle = $appraisalConfig['moduleCode'];
        }
        
        function getModuleObjArr() {
            global $PATH_WRT_ROOT, $intranet_root, $image_path, $LAYOUT_SKIN, $CurrentPage, $sys_custom;
            global $plugin, $special_feature, $appraisalConfig, $indexVar;
            global $intranet_session_language, $Lang, $system_cust;
            
            $currentPageAry = explode($appraisalConfig['taskSeparator'], $CurrentPage);
            $pageFunction = $currentPageAry[0];
            $pageMenu = $currentPageAry[0].$appraisalConfig['taskSeparator'].$currentPageAry[1];
            
            $currentDate = date("Y-m-d");
            $sql = "SELECT CycleID,AcademicYearID,DescrChi,DescrEng FROM INTRANET_PA_C_CYCLE WHERE CycleStart <= '".$currentDate."' AND CycleClose >= '".$currentDate."';";
            $a = $this->returnResultSet($sql);
            $cycleID = ($a[0]["CycleID"]!="")?$a[0]["CycleID"]:0;
            
            // Check if any cycle has released report
            $sql = "SELECT CycleID FROM INTRANET_PA_C_CYCLE WHERE ReportReleaseDate IS NOT NULL";
            $reportRlsCycleList = $this->returnVector($sql);
            
            # Management
            $MenuArr["Management"] = array($Lang['Menu']['AccountMgmt']['Management'], "#", ($pageFunction=='management'));
            if (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]) {
                // e-Apprasial Period
                $menuTask = 'management'.$appraisalConfig['taskSeparator'].'appraisal_period'.$appraisalConfig['taskSeparator'].'list';
                $MenuArr["Management"]["Child"]["AppraisalPeriod"] = array($Lang['Appraisal']['AppraisalPeriod'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                
                
            }
            // observation Period
            $menuTask = 'management'.$appraisalConfig['taskSeparator'].'observation_period'.$appraisalConfig['taskSeparator'].'list';
            $MenuArr["Management"]["Child"]["ObservationPeriod"] = array($Lang['Appraisal']['ObservationPeriod'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
            
            // e-Apprasial Records
            $menuTask = 'management'.$appraisalConfig['taskSeparator'].'appraisal_records'.$appraisalConfig['taskSeparator'].'list';
            $MenuArr["Management"]["Child"]["AppraisalRecord"] = array($Lang['Appraisal']['AppraisalRecords'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
            
            // To-do modification
            $menuTask = 'management'.$appraisalConfig['taskSeparator'].'modification_list'.$appraisalConfig['taskSeparator'].'modification';
            $MenuArr["Management"]["Child"]["ModificationList"] = array($Lang['Appraisal']['ModificationList'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
            
            //Group Records
            if($cycleID!=0){
                $sql = "Select gm.GrpID From INTRANET_PA_C_GRPMEM gm INNER JOIN INTRANET_PA_C_GRPSUM gs ON gm.GrpID=gs.GrpID WHERE gm.UserID='".$_SESSION['UserID']."' AND gm.IsLeader=1 AND gs.CycleID='".$cycleID."'";
                $hasGroup = $this->returnVector($sql);
                if(!empty($hasGroup) && !$sys_custom['eAppraisal']['HideGroupRecords']){
                    $menuTask = 'management'.$appraisalConfig['taskSeparator'].'group_records'.$appraisalConfig['taskSeparator'].'list';
                    $MenuArr["Management"]["Child"]["GroupRecord"] = array($Lang['Appraisal']['GroupRecord'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                }
            }
            
            if (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]) {
                // import information
                $menuTask = 'management'.$appraisalConfig['taskSeparator'].'import'.$appraisalConfig['taskSeparator'].'list';
                $menuTask2 = 'management'.$appraisalConfig['taskSeparator'].'import_lnt'.$appraisalConfig['taskSeparator'].'list';
                $MenuArr["Management"]["Child"]["ImportInformation"] = array($Lang['Appraisal']['ImportInformation'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false || stripos($menuTask2, $pageMenu)!==false));
                
                # Reports
                $MenuArr["Reports"] = array($Lang['Menu']['AccountMgmt']['Reports'], "#", ($pageFunction=='reports'));
                
                // Personal Appraisal Result
                if(in_array($_SESSION['UserID'], $this->getReportDisplayPersonList(false))){
                    $menuTask = 'reports'.$appraisalConfig['taskSeparator'].'personal_result'.$appraisalConfig['taskSeparator'].'list';
                    $MenuArr["Reports"]["Child"]["ReportPersonalAppraisalResult"] = array($Lang['Appraisal']['ReportPersonalAppraisalResult'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                    
                    //Export Appraisal Reports
                    $menuTask = 'reports'.$appraisalConfig['taskSeparator'].'export_appraisal_report'.$appraisalConfig['taskSeparator'].'list';
                    $MenuArr["Reports"]["Child"]["ExportAppraisalReports"] = array($Lang['Appraisal']['ExportAppraisalReports'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                }else{
                    if(in_array($_SESSION['UserID'], $this->getReportDisplayPersonList(false)) || (!$sys_custom['eAppraisal']['TANotDisplayPeronalReport'] && !$sys_custom['eAppraisal']['TAAdminViewReportOnly'])){
                        $menuTask = 'reports'.$appraisalConfig['taskSeparator'].'personal_result'.$appraisalConfig['taskSeparator'].'list';
                        $MenuArr["Reports"]["Child"]["ReportPersonalAppraisalResult"] = array($Lang['Appraisal']['ReportPersonalAppraisalResult'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                        
                        //Export Appraisal Reports
                        $menuTask = 'reports'.$appraisalConfig['taskSeparator'].'export_appraisal_report'.$appraisalConfig['taskSeparator'].'list';
                        $MenuArr["Reports"]["Child"]["ExportAppraisalReports"] = array($Lang['Appraisal']['ExportAppraisalReports'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                    }
                }
                // Outstanding Appraisal List
                $menuTask = 'reports'.$appraisalConfig['taskSeparator'].'outstanding'.$appraisalConfig['taskSeparator'].'list';
                $MenuArr["Reports"]["Child"]["ReportOutstandingAppraisalList"] = array($Lang['Appraisal']['ReportOutstandingAppraisalList'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                // Short-cut path for average calcualtion
                //$menuTask = 'reports'.$appraisalConfig['taskSeparator'].'calculation'.$appraisalConfig['taskSeparator'].'list';
                //$MenuArr["Reports"]["Child"]["ReportCalculationAppraisal"] = array($Lang['Appraisal']['ReportCalculationAppraisal'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                // Taks Assignment Checklist
                $menuTask = 'reports'.$appraisalConfig['taskSeparator'].'task_assignment'.$appraisalConfig['taskSeparator'].'list';
                $MenuArr["Reports"]["Child"]["ReportTaskAssignmentChecklist"] = array($Lang['Appraisal']['ReportTaskAssignmentChecklist'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                
                # Settings
                $MenuArr["Settings"] = array($Lang['Menu']['AccountMgmt']['Settings'], "#", ($pageFunction=='settings'));
                // Basic Settings
                $menuTask = 'settings'.$appraisalConfig['taskSeparator'].'basic_settings'.$appraisalConfig['taskSeparator'].'list';
                $MenuArr["Settings"]["Child"]["SettingsBasicSettings"] = array($Lang['Appraisal']['SettingsBasicSettings'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                //				// Exclude User from the need to complete Appraisal Forms
                //				$menuTask = 'settings'.$appraisalConfig['taskSeparator'].'exclude_teachers'.$appraisalConfig['taskSeparator'].'list';
                //				$MenuArr["Settings"]["Child"]["SettingsExcludeTeachers"] = array($Lang['Appraisal']['SettingsExcludeTeachers'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                // Appraisal Reports Cover Setting
                $menuTask = 'settings'.$appraisalConfig['taskSeparator'].'appraisal_report_cover_setting'.$appraisalConfig['taskSeparator'].'list';
                $MenuArr["Settings"]["Child"]["SettingsAppraisalReportCoverSettings"] = array($Lang['Appraisal']['SettingsAppraisalReportCoverSettings'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                // Special Role
                $menuTask = 'settings'.$appraisalConfig['taskSeparator'].'special_roles'.$appraisalConfig['taskSeparator'].'list';
                $MenuArr["Settings"]["Child"]["SettingsSpecialRole"] = array($Lang['Appraisal']['SettingsSpecialRole'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                // Template
                $menuTask = 'settings'.$appraisalConfig['taskSeparator'].'templates'.$appraisalConfig['taskSeparator'].'list';
                $MenuArr["Settings"]["Child"]["SettingsTemplate"] = array($Lang['Appraisal']['SettingsTemplate'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
            }else{
                if(in_array($_SESSION['UserID'], $this->getReportDisplayPersonList(false)) || !empty($reportRlsCycleList) || (!$sys_custom['eAppraisal']['TANotDisplayPeronalReport'] && !$sys_custom['eAppraisal']['TAAdminViewReportOnly'])){
                    # Reports
                    $MenuArr["Reports"] = array($Lang['Menu']['AccountMgmt']['Reports'], "#", ($pageFunction=='reports'));
                    // Personal Appraisal Result
                    $menuTask = 'reports'.$appraisalConfig['taskSeparator'].'personal_result'.$appraisalConfig['taskSeparator'].'list';
                    $MenuArr["Reports"]["Child"]["ReportPersonalAppraisalResult"] = array($Lang['Appraisal']['ReportPersonalAppraisalResult'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                }
            }
            // Quantitative Appraisal List (by 惠僑)
            if($sys_custom['eAppraisal']['AverageReport'] && (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"] || !$sys_custom['eAppraisal']['TAAdminViewReportOnly'] || !empty($reportRlsCycleList))){
                $menuTask = 'reports'.$appraisalConfig['taskSeparator'].'school_result'.$appraisalConfig['taskSeparator'].'list';
                $MenuArr["Reports"]["Child"]["ReportSchoolResultlList"] = array($Lang['Appraisal']['ReportSchoolResultlList'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
                //$menuTask = 'reports'.$appraisalConfig['taskSeparator'].'personal_average'.$appraisalConfig['taskSeparator'].'list';
                //$MenuArr["Reports"]["Child"]["ReportPersonalAverageList"] = array($Lang['Appraisal']['ReportPersonalAverageList'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
            }
            
            // For 聖博德天主教小學(蒲崗村道) : 考績結果可以像「問卷調查」內顯示每題% 分佈
            if($sys_custom['eAppraisal']['SurveyStyleReport'] && (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"] || !$sys_custom['eAppraisal']['TAAdminViewReportOnly'] || !empty($reportRlsCycleList))){
                $menuTask = 'reports'.$appraisalConfig['taskSeparator'].'personal_average'.$appraisalConfig['taskSeparator'].'list';
                $MenuArr["Reports"]["Child"]["ReportPersonalAverageList"] = array($Lang['Appraisal']['ReportPersonalAverageList'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
            }
            
            // For  香港神託會培基書院
            if($sys_custom['eAppraisal']['ExportProposedAllocation'] && (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"] || !$sys_custom['eAppraisal']['TAAdminViewReportOnly'] || !empty($reportRlsCycleList))){
                $menuTask = 'reports'.$appraisalConfig['taskSeparator'].'export_proposed_allocation'.$appraisalConfig['taskSeparator'].'list';
                $MenuArr["Reports"]["Child"]["ReportExportProposedAllocation"] = array($Lang['Appraisal']['ReportExportProposedAllocation'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
            }
            
            // For 伍時暢
            if($sys_custom['eAppraisal']['WusichongStyle'] && (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"] || !$sys_custom['eAppraisal']['TAAdminViewReportOnly'] || !empty($reportRlsCycleList))){
                $menuTask = 'reports'.$appraisalConfig['taskSeparator'].'teaching_review'.$appraisalConfig['taskSeparator'].'list';
                $MenuArr["Reports"]["Child"]["ReportTeachingReview"] = array($Lang['Appraisal']['ReportTeachingReview'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
            }
            
            if($plugin['eAppraisal_settings']['LnTReport'] && (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"] || !$sys_custom['eAppraisal']['TAAdminViewReportOnly'] || !empty($reportRlsCycleList))){
                $menuTask = 'reports'.$appraisalConfig['taskSeparator'].'lnt'.$appraisalConfig['taskSeparator'].'list';
                $MenuArr["Reports"]["Child"]["LearningAndTeaching"] = array($Lang['Appraisal']['LNT']['LearningAndTeaching'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
            }

            # change page web title
            $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
            $js.= 'document.title="eClass Teacher Appraisal";'."\n";
            $js.= '</script>'."\n";
            
            ### module information
            $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT.$appraisalConfig['eAdminPath'];
            $MODULE_OBJ['title'] = $Lang['Header']['Menu']['eAppraisal'].$js;
            $MODULE_OBJ['title_css'] = "menu_opened";
            
            if($this->isEJ()){
                $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_staffappraisal.png";
            }
            else {
                $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_teacher_appraisal.png";
            }
            
            $MODULE_OBJ['menu'] = $MenuArr;
            
            return $MODULE_OBJ;
        }
        // -------------------------------------- System Checking -------------------------------------- //
        function checkAccessRight() {
            global $plugin;
            $canAccess = true;
            if (!$plugin['eAppraisal']) {
                $canAccess = false;
            } else {
                if ($_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"] && !$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["hasAppraisal"]) {
                    $canAccess = false;
                } else if (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAppraisal"]) {
                    $canAccess = false;
                }
            }
            if (!$canAccess) {
                No_Access_Right_Pop_Up();
            }
        }
        // -------------------------------------- System Checking -------------------------------------- //
        // -------------------------------------- Data Initialization -------------------------------------- //
        function setDefaultSettings(){
            global $sys_custom;
            // INTRANET_PA_S_APPROL
            $sql = "INSERT INTO INTRANET_PA_S_APPROL(NameChi,NameEng,DisplayOrder,IsActive,SysUse,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
					SELECT * FROM(SELECT '考績者' as NameChi,'Appraiser' as NameEng,1 as DisplayOrder,1 as IsActive,1 as SysUse,
					".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a
					WHERE NOT EXISTS(
						SELECT NameChi FROM INTRANET_PA_S_APPROL WHERE AppRoleID=1);";
            $this->db_db_query($sql);
            $sql = "INSERT INTO INTRANET_PA_S_APPROL(NameChi,NameEng,DisplayOrder,IsActive,SysUse,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
					SELECT * FROM(SELECT '被評者' as NameChi,'Appraisee' as NameEng,2 as DisplayOrder,1 as IsActive,1 as SysUse,
					".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a
					WHERE NOT EXISTS(
						SELECT NameChi FROM INTRANET_PA_S_APPROL WHERE AppRoleID=2);";
            $this->db_db_query($sql);
            // INTRANET_PA_S_FRMTPL
            $sql = "SELECT * FROM INTRANET_PA_S_FRMTPL WHERE TemplateType=0;";
            $a = $this->returnResultSet($sql);
            if(sizeof($a)==0){
                $sql = "INSERT INTO INTRANET_PA_S_FRMTPL(TemplateID,TemplateType,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,HdrRefChi,HdrRefEng,ObjChi,ObjEng,DescrChi,DescrEng,DisplayOrder,IsActive,NeedClass,NeedSubj,AppTgtChi,AppTgtEng,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
						SELECT * FROM(SELECT '1' as TemplateID,'0' as TemplateType,'老師考績表' as FrmTitleChi,'Teacher Appraisal Form' as FrmTitleEng,
						'表格A' as FrmCodChi,'Form A' as FrmCodEng,'表格A/老師考績表/個人資料及責任' as HdrRefChi,'Form A/Teacher Appraisal/Particulars & Duties' as HdrRefEng,
						'一般資料' as ObjChi,'General Information' as ObjEng,
						'所有職員需要填寫這表格。<br/>請參閱指引，填寫表格內容。' as DescrChi,
						'This form is to be filled in by all staff.<br/>Please refer to the Guidelines for details of form filling' as DescrEng,
						1 as DisplayOrder,1 as IsActive,
						0 as NeedClass,0 as NeedSubj,
						'被評者' as AppTgtChi,'Appraisee' as AppTgtEng,
						".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a";
                //echo $sql;;
                $this->db_db_query($sql);
            }
            // INTRANET_PA_S_FRMSEC
            $sql = "SELECT * FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=1";
            $a = $this->returnResultSet($sql);
            if(sizeof($a)==0){
                $sql = "INSERT INTO INTRANET_PA_S_FRMSEC(TemplateID,DisplayOrder,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
						SELECT * FROM(SELECT '1' as TemplateID,'1' as DisplayOrder,
						".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a";
                $this->db_db_query($sql);
            }
            // INTRANET_PA_S_SECROL
            /*$sql = "SELECT * FROM INTRANET_PA_S_SECROL WHERE SecID=1";
             $a = $this->returnResultSet($sql);
             if(sizeof($a)==0){
             $sql = "INSERT INTO INTRANET_PA_S_SECROL(AppRoleID,CanBrowse,CanFill,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
             SELECT * FROM(SELECT '2' as AppRoleID,'1' as CanBrose,'1' as CanFill,
             ".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a";
             $this->db_db_query($sql);
             }		*/
            // INTRANET_PA_S_FRMPRE
            $sql = "SELECT TemplateID, InputBy_UserID, ModifiedBy_UserID  FROM INTRANET_PA_S_FRMPRE GROUP BY TemplateID";
            $templates = $this->returnArray($sql);
            if(empty($templates)){
                $sql = "SELECT TemplateID FROM INTRANET_PA_S_FRMTPL WHERE TemplateType=0";
                $templates = $this->returnArray($sql);
            }
            foreach($templates as $tmp){
                $sql = "SELECT * FROM INTRANET_PA_S_FRMPRE WHERE TemplateID=".$tmp['TemplateID']."";
                $a = $this->returnResultSet($sql);
                if(sizeof($a)==0 || sizeof($a) < 21){
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'1' as DataTypeID,'個人資料' as DescrChi,'Personal Particulars' as DescrEng,1 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'現有工作資料' as DescrChi,'Present Job Information' as DescrEng,2 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'3' as DataTypeID,'獲得資格 / 在考績期修讀之課程' as DescrChi,'Qualification Acquired / Study-in-Progress during Appraising Period' as DescrEng,3 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'4' as DataTypeID,'每班別所教的科目' as DescrChi,'Present Teaching Duties per Class' as DescrEng,4 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'5' as DataTypeID,'每年級所教的科目' as DescrChi,'Present Teaching Duties per Form' as DescrEng,5 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'6' as DataTypeID,'現負責的行政工作' as DescrChi,'Present Administrative Duties' as DescrEng,6 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'7' as DataTypeID,'現負責的課外活動' as DescrChi,'Present Extra-Curriculum Activity Duties' as DescrEng,7 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'8' as DataTypeID,'其他負責的工作' as DescrChi,'Present Other Duties' as DescrEng,8 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'9' as DataTypeID,'缺席及病假' as DescrChi,'Absence & Lateness' as DescrEng,9 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'10' as DataTypeID,'請假記錄(有薪及無薪)' as DescrChi,'Leaves Records (Pay & No-Pay)' as DescrEng,10 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'11' as DataTypeID,'請假記錄' as DescrChi,'Leaves Records' as DescrEng,11 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'12' as DataTypeID,'遞交表格' as DescrChi,'Form Submission' as DescrEng,12 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'13' as DataTypeID,'教學／行政工作目標及評估' as DescrChi, 'Teaching / Administrative Duties Objectives and Assessment' as DescrEng,13 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'14' as DataTypeID,'現負責的學科工作' as DescrChi,'Present Academic Duties' as DescrEng,14 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'15' as DataTypeID,'本年度訓練學生參加比賽及成績' as DescrChi,'Current Year Responsible Student Training for Competition and Result' as DescrEng,15 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'16' as DataTypeID,'進修資料' as DescrChi,'CPD' as DescrEng,16 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'17' as DataTypeID,'考勤資料' as DescrChi,'Attendnace Information' as DescrEng,17 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'18' as DataTypeID,'教育工作經歷' as DescrChi,'Teaching Experience' as DescrEng,18 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'19' as DataTypeID,'學歷及教育專業訓練' as DescrChi,'Academic and Teaching Professional Training' as DescrEng,19 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'20' as DataTypeID,'病假及遲到記錄' as DescrChi,'Sick Leave & Lateness Records' as DescrEng,20 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                }
                
                // Personal Particulars default questions
                $sql = "SELECT * FROM INTRANET_PA_S_FRMPRE_SUB WHERE TemplateID=".$tmp['TemplateID']." AND DataTypeID='1' ORDER BY DataTypeID ASC, DataSubTypeID ASC";
                $a = $this->returnResultSet($sql);
                if(sizeof($a)==0 || sizeof($a) < 9){
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'1' as DataTypeID,'1' as DataSubTypeID,'姓名(英文)' as DescrChi,'Teacher (Eng.)' as DescrEng,1 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'1' as DataTypeID,'2' as DataSubTypeID,'姓名(中文)' as DescrChi,'Teacher (Chi.)' as DescrEng,2 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'1' as DataTypeID,'3' as DataSubTypeID,'性別' as DescrChi,'Gender' as DescrEng,3 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'1' as DataTypeID,'4' as DataSubTypeID,'出生日期' as DescrChi,'Date of Birth' as DescrEng,4 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'1' as DataTypeID,'5' as DataSubTypeID,'宗教' as DescrChi,'Religion' as DescrEng,5 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'1' as DataTypeID,'6' as DataSubTypeID,'婚姻狀況' as DescrChi,'Marital Status' as DescrEng,6 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'1' as DataTypeID,'7' as DataSubTypeID,'地址(英文)' as DescrChi,'Address (Eng.)' as DescrEng,7 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'1' as DataTypeID,'8' as DataSubTypeID,'地址(中文)' as DescrChi,'Address (Chi.)' as DescrEng,8 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'1' as DataTypeID,'9' as DataSubTypeID,'電話' as DescrChi,'Tel. No.' as DescrEng,9 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                }
                
                // Present Job Information default questions
                $sql = "SELECT * FROM INTRANET_PA_S_FRMPRE_SUB WHERE TemplateID='".$tmp['TemplateID']."' AND DataTypeID='2' ORDER BY DataTypeID ASC, DataSubTypeID ASC";
                $a = $this->returnResultSet($sql);
                if(sizeof($a)==0 || sizeof($a) < 12){
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'1' as DataSubTypeID,'職級' as DescrChi,'Position/Rank' as DescrEng,1 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'2' as DataSubTypeID,'出任此職級日期' as DescrChi,'Entry Date for this Position' as DescrEng,2 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'3' as DataSubTypeID,'進入本學校服務日期' as DescrChi,'Entry Date to this School' as DescrEng,3 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'4' as DataSubTypeID,'進入本團體服務日期' as DescrChi,'Entry Date to this School Organization' as DescrEng,4 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'5' as DataSubTypeID,'年資(官/津校)' as DescrChi,'Seniority (Gov./Aided)' as DescrEng,5 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'6' as DataSubTypeID,'年資(私校)' as DescrChi,'Seniority (Private)' as DescrEng,6 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'7' as DataSubTypeID,'服務本會學校總年資（包括本年度）' as DescrChi,'Seniority in this School Organization（including this year）' as DescrEng,7 as DisplayOrder,1 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'8' as DataSubTypeID,'註冊編號' as DescrChi,'Registration No.' as DescrEng,8 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'9' as DataSubTypeID,'現時薪級點' as DescrChi,'Current Salary Scale Point' as DescrEng,9 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'10' as DataSubTypeID,'評核期內署任連續三十天或以上之職位' as DescrChi,'Acting Position for more than 30 consecutive days in this Appraisal Cycle' as DescrEng,10 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'11' as DataSubTypeID,'評核期內連續三十天或以上之假期' as DescrChi,'Consecutive Holidays for more than 30 days in this Appraisal Cycle' as DescrEng,11 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                    $sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
							SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'12' as DataSubTypeID,'進入本學校服務時職級'as DescrChi,'Position on Entry Date' as DescrEng,12 as DisplayOrder,0 as Incl,
							".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
                    $this->db_db_query($sql);
                }
            }
        }
        // -------------------------------------- Data Initialization -------------------------------------- //
        // -------------------------------------- SQL Function -------------------------------------- //
        function getAcademicYearTerm($date){
            $sql = "SELECT YearTermID,AcademicYearID
					FROM (
						SELECT YearTermID,AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE '".$date."' BETWEEN TermStart AND TermEnd
					) as ayt
					";
            //echo $sql;
            return $this->returnResultSet($sql);
        }
        function getAcademicYearTermOfCurrentCycle($date){
            $sql = "SELECT YearTermID,AcademicYearID
					FROM (
						SELECT YearTermID, ayt.AcademicYearID FROM ACADEMIC_YEAR_TERM ayt
						INNER JOIN INTRANET_PA_C_CYCLE c ON ayt.AcademicYearID = c.AcademicYearID
						WHERE c.CycleStart <= '".$date."' AND c.CycleClose >= '".$date."'
					) as ayt
					";
            // echo $sql;
            return $this->returnResultSet($sql);
        }
        function getAcademicYearTermByCycleID($cycleID){
            $sql = "SELECT YearTermID,AcademicYearID
					FROM (
						SELECT YearTermID, ayt.AcademicYearID FROM ACADEMIC_YEAR_TERM ayt
						INNER JOIN INTRANET_PA_C_CYCLE c ON ayt.AcademicYearID = c.AcademicYearID
						WHERE c.CycleID='".$cycleID."'
					) as ayt
					";
            // echo $sql;
            return $this->returnResultSet($sql);
        }
        function getAllAcademicYear($academicYearID){
            //$academicYearID=($academicYearID!="")?$academicYearID:"%";
            $conds_academicYearId = "";
            if (!empty($academicYearID)) {
                $conds_academicYearId = " And AcademicYearID = '".$academicYearID."' ";
            }
            $sql="SELECT AcademicYearID,YearNameEN,YearNameB5,Sequence FROM ACADEMIC_YEAR WHERE 1 ". $conds_academicYearId." ORDER BY Sequence;";
            $result = $this->returnResultSet($sql);
            if ($this->isEJ()) {
                foreach ($result as $kk => $vv) {
                    $result[$kk]["YearNameEN"] = $this->displayChinese($vv["YearNameEN"]);
                    $result[$kk]["YearNameB5"] = $this->displayChinese($vv["YearNameB5"]);
                }
            }
            return $result;
        }
        function getAllForm(){
            if ($this->isEJ()) {
                $sql = "SELECT ClassLevelID as YearID,LevelName as YearName FROM INTRANET_CLASSLEVEL WHERE RecordStatus=1 ORDER BY LevelName ASC";
                $result = $this->returnResultSet($sql);
                foreach ($result as $kk => $vv) {
                    $result[$kk]["YearName"] = $this->displayChinese($vv["YearName"]);
                }
            }else{
                $sql = "SELECT YearID,YearName FROM YEAR WHERE RecordStatus=1 ORDER BY YearName ASC";
                $result = $this->returnResultSet($sql);
            }
            return $result;
        }
        function getAllYearClass($academicYearID){
            if ($this->isEJ()) {
                $sql = "
						SELECT YearClassID,ClassTitleEN,ClassTitleB5,YearID,Sequence
						FROM(
							SELECT ClassID as YearClassID, ClassLevelID as YearID, ClassName as ClassTitleEN, ClassName as ClassTitleB5, GroupID as Sequence FROM INTRANET_CLASS
						) as yc
						ORDER BY ClassTitleEN
						";
                // echo $sql;
                $result = $this->returnResultSet($sql);
                foreach ($result as $kk => $vv) {
                    $result[$kk]["ClassTitleB5"] = $this->displayChinese($vv["ClassTitleB5"]);
                    $result[$kk]["ClassTitleEN"] = $this->displayChinese($vv["ClassTitleEN"]);
                }
            }else{
                if($academicYearID==""){
                    $sql = "Select AcademicYearID From ACADEMIC_YEAR_TERM WHERE TermStart <= NOW() AND TermEnd >= NOW()";
                    $academicYearIDResult = $this->returnResultSet($sql);
                    $academicYearID = $academicYearIDResult[0]['AcademicYearID'];
                }
                $sql = "
						SELECT YearClassID,ClassTitleEN,ClassTitleB5,YearID,Sequence
						FROM(
							SELECT AcademicYearID FROM ACADEMIC_YEAR WHERE AcademicYearID='".IntegerSafe($academicYearID)."'
						) as ay
						INNER JOIN(
							SELECT YearClassID,AcademicYearID,YearID,ClassTitleEN,ClassTitleB5,Sequence FROM YEAR_CLASS
						) as yc ON ay.AcademicYearID=yc.AcademicYearID
						ORDER BY yc.AcademicYearID,YearID,Sequence
						";
                // echo $sql;
                $result = $this->returnResultSet($sql);
            }
            return $result;
        }
        function getAllSubject(){
            if ($this->isEJ()) {
                $sql = "
					SELECT SubjectID, SubjectName as SubjectTitleB5,SubjectName as SubjectTitleEN
					FROM INTRANET_SUBJECT WHERE RecordStatus = 1
					ORDER BY SubjectID
					";
                $result = $this->returnResultSet($sql);
                foreach ($result as $kk => $vv) {
                    $result[$kk]["SubjectTitleEN"] = $this->displayChinese($vv["SubjectTitleEN"]);
                    $result[$kk]["SubjectTitleB5"] = $this->displayChinese($vv["SubjectTitleB5"]);
                }
            }else{
                $sql = "
						SELECT RecordID as SubjectID,CH_DES as SubjectTitleB5,EN_DES as SubjectTitleEN
						FROM ASSESSMENT_SUBJECT WHERE RecordStatus = 1 and (CMP_CODEID is null || CMP_CODEID = '')
						ORDER BY DisplayOrder
						";
                $result = $this->returnResultSet($sql);
            }
            return $result;
        }
        function signatureValidation($user_ID,$password){
            // prepare to check the user password. if correct, return true
            global $intranet_authentication_method, $PATH_WRT_ROOT, $plugin, $SYS_CONFIG, $intranet_root, $appraisalConfig;
            
            include_once($intranet_root."/includes/libauth.php");
            $libauth = new libauth();
            $sql = "SELECT UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='$user_ID'";
            //echo $sql."<br/><br/>";
            $records = $libauth->returnVector($sql);
            if(count($records)==0) return false;
            
            $user_login = $records[0];
            $result = $libauth->validate($user_login, $password, $passwd_hash='', $validateonly=1);
            return $result ? true: false;
        }
        function getSectionContent($prefix,$secID,$qCatID,$recordID,$batchID,$rlsNo,$appRoleID){
            $PATH_WRT_ROOT = "../../../../";
            include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
            include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
            global $intranet_session_language, $Lang, $sys_custom,$intranet_root,$appraisalConfig;
            $file_parentPath = $intranet_root.'/file/appraisal/';
            $indexVar['libappraisal_ui'] = new libappraisal_ui();
            $libacc = new libaccountmgmt();
            
            // Get form special identity list
            $sql = "Select cycle.AcademicYearID From INTRANET_PA_C_CYCLE cycle
					INNER JOIN INTRANET_PA_T_FRMSUM sum ON cycle.CycleID = sum.CycleID
					WHERE sum.RecordID='".$recordID."'";
            $sectionAcademicYear = current($this->returnVector($sql));
            $specialIdentityList = $libacc->getSpecialIdentityMapping($sectionAcademicYear,true);
            
            // Check if the concerning section is current or not
            $sql = "SELECT GrpID,SecID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".$rlsNo."' AND RecordID='".$recordID."' AND BatchID='".$batchID."'";
            $batchData=current($this->returnResultSet($sql));
            
            // Question List
            $sql = "SELECT SecID,ipsqcat.QCatID,QID,QuesDispMode,QCodDes,Descr,QuesDisplayOrder,IsMC,IsScore,HasRmk,Rmk,IsTxtAns,TxtBoxNote,InputType,RmkLabel,IsMand,CatDisplayOrder
					FROM(
						SELECT QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,DisplayOrder as CatDisplayOrder,QuesDispMode FROM INTRANET_PA_S_QCAT WHERE SecID='".$secID."' AND QCatID='".$qCatID."'
					) as ipsqcat
					LEFT JOIN(
						SELECT QID,QCatID,".$this->getLangSQL("QCodChi","QCodEng")." as QCodDes,".$this->getLangSQL("DescrChi","DescrEng")." as Descr,DisplayOrder,InputType,IsMC,IsScore,IsTxtAns,TxtBoxNote,HasRmk,".$this->getLangSQL("RmkLabelChi","RmkLabelEng")." as Rmk,
						DisplayOrder as QuesDisplayOrder,".$this->getLangSQL("RmkLabelChi","RmkLabelEng")." as RmkLabel,IsMand
						FROM INTRANET_PA_S_QUES WHERE QCatID='".$qCatID."'
					) as ipsques ON ipsqcat.QCatID=ipsques.QCatID
					ORDER BY QuesDisplayOrder;";
            $a=$this->returnResultSet($sql);
            //echo ($sql)."<br/><br/>";
            $quesDispMode=$a[0]["QuesDispMode"];
            
            $sql = "SELECT SecID FROM INTRANET_PA_C_BATCH WHERE BatchID='".IntegerSafe($batchID)."';";
            $g=$this->returnResultSet($sql);
            $currParentSecID = $g[0]["SecID"];
            
            $sql = "SELECT BatchOrder FROM INTRANET_PA_C_BATCH WHERE BatchID='".IntegerSafe($batchID)."';";
            $g=$this->returnResultSet($sql);
            $currBatchOrder = $g[0]["BatchOrder"];
            
            // check any content in the first content
            $hasContent = false;
            $sql="SELECT SUM(LENGTH(".$this->getLangSQL("QCodChi","QCodEng").")) as content FROM INTRANET_PA_S_QUES WHERE QCatID='".$qCatID."';";
            $x=$this->returnResultSet($sql);
            if($x[0]["content"] > 0){
                $hasContent = true;
            }
            
            // check if the question is submitted or not
            $isSubmitted = false;
            $parentSecSql = "SELECT ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE SecID='".$secID."'";
            $parentSecID = current($this->returnVector($parentSecSql));
            $sql = "SELECT SubDate FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".$rlsNo."' AND RecordID='".$recordID."' AND SecID='".$parentSecID."'";
            $SubDate = current($this->returnVector($sql));
            $sql = "SELECT BatchID FROM INTRANET_PA_T_FRMSUB WHERE RecordID='".$recordID."' AND SecID='".$parentSecID."' AND appRoleID='".$appRoleID."'";
            $secBatchID = $this->returnVector($sql);
            if(!empty($SubDate) || in_array($batchID,$secBatchID)){
                $isSubmitted = true;
                
                $sql = "SELECT UserID FROM INTRANET_PA_T_FRMSUM WHERE RecordID='".$recordID."'";
                $appraiseeID = current($this->returnVector($sql));
                $sql = "SELECT FillerID FROM INTRANET_PA_T_FRMSUB WHERE RecordID='".$recordID."' AND SecID='".$parentSecID."'";
                $fillerID = current($this->returnVector($sql));
                if($fillerID != $_SESSION['UserID'] && $appraiseeID!=$fillerID){
                    // Check if there are section collaboratively contributing the same section (if not the current sec) for the corresponding appraisee
                    $sql = "SELECT DISTINCT frmsub.RecordID FROM INTRANET_PA_T_FRMSUB frmsub
                            INNER JOIN INTRANET_PA_T_FRMSUM frmsum ON frmsub.RecordID = frmsum.RecordID AND frmsum.UserID='".$appraiseeID."'
                            WHERE frmsub.GrpID='".$batchData['GrpID']."' AND frmsub.IsActive=1 AND frmsub.RecordID <> '".$recordID."' ";
                    $collaborativeForm = $this->returnVector($sql);
                    
                    // Check if the section from different record is filled by the same person
                    $collFillerList = array();
                    foreach($collaborativeForm as $idx=>$collRecordID){
                        $sql = "SELECT FillerID FROM INTRANET_PA_T_FRMSUB WHERE RecordID='".$collRecordID."' AND SecID='".$parentSecID."'";
                        $colFillerID = current($this->returnVector($sql));
                        if(!in_array($colFillerID,$collFillerList)){
                            $collFillerList[] = $colFillerID;
                        }
                    }
                    if(!in_array($fillerID,$collFillerList)){
                        $collFillerList[] = $fillerID;
                    }
                    if(count($collFillerList) <= 1){
                        unset($collaborativeForm);
                    }
                    if(!empty($collaborativeForm)){
                        $isCollaborate = true;
                    }
                }
            }
            
            // Likert
            if($quesDispMode==1){
                
                $sql = "SELECT MarkID,QCatID,".$this->getLangSQL("MarkCodChi","MarkCodEng")." as MarkCodDes,".$this->getLangSQL("DescrChi","DescrEng")." as Descr,Score,DisplayOrder,".
                    $this->getLangSQL("DescrChi","DescrEng")." as Descr
						FROM INTRANET_PA_S_QLKS WHERE QCatID='".$qCatID."'
						ORDER BY DisplayOrder";
                    //echo $sql."<br/><br/>";
                    $b=$this->returnResultSet($sql);
                    
                    $sql = "SELECT RecordID,CycleID,TemplateID FROM INTRANET_PA_T_FRMSUM WHERE RecordID='".$recordID."';";
                    $b0=$this->returnResultSet($sql);
                    $templateID=$b0[0]["TemplateID"];
                    
                    $sql = "SELECT TemplateID,IsObs,FrmTitleEng,FrmTitleChi FROM INTRANET_PA_S_FRMTPL WHERE TemplateID='".$templateID."';";
                    $b1=$this->returnResultSet($sql);
                    $isObs = $b1[0]["IsObs"];
                    
                    if($isObs==1){
                        $sql="SELECT RlsNo,RecordID,BatchID,SecID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".$rlsNo."' AND RecordID='".$recordID."' AND BatchID='".$batchID."';";
                        //echo $sql."<br/>";
                        $b2=$this->returnResultSet($sql);
                        $currParentSecID=$b2[0]["SecID"];
                    }
                    
                    if($sys_custom['eAppraisal']['WusichongStyle']==true){
                        $inF3 = false;
                        $inF5 = false;
                        $sql = "SELECT TemplateType FROM INTRANET_PA_S_FRMTPL WHERE TemplateID = '".$templateID."' AND FrmTitleEng LIKE 'F3%' AND IsActive=1";
                        $temp=$this->returnResultSet($sql);
                        if(!empty($temp)){
                            $inF3 = true;
                        }
                        $sql = "SELECT TemplateType FROM INTRANET_PA_S_FRMTPL WHERE TemplateID = '".$templateID."' AND FrmTitleEng LIKE 'F5%' AND IsActive=1";
                        $temp=$this->returnResultSet($sql);
                        if(!empty($temp)){
                            $inF5 = true;
                        }
                    }
                    
                    if($sys_custom['eAppraisal']['WusichongStyle']==true && (strstr($b1[0]['FrmTitleEng'],'F3') || (strstr($b1[0]['FrmTitleChi'],'F3')))){
                        $sql = "SELECT frmsec.SecID FROM INTRANET_PA_S_FRMSEC frmsec
							INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmsec.SecID = frmsub.SecID
							WHERE RecordID='".$recordID."' AND DisplayOrder=2";
                        $secNeedCalculation = current($this->returnVector($sql));
                        
                        if($secNeedCalculation == $parentSecID && $a[0]['CatDisplayOrder']==1){
                            $sql = "SELECT frmsum.UserID FROM INTRANET_PA_T_FRMSUB frmsub INNER JOIN INTRANET_PA_T_FRMSUM frmsum ON frmsub.RecordID = frmsum.RecordID WHERE frmsub.RecordID='".$recordID."' AND frmsub.BatchID='".$batchID."'";
                            $appraiseeID = current($this->returnVector($sql));
                            $sql = "SELECT frmsub.RecordID FROM INTRANET_PA_T_FRMSUB frmsub
								INNER JOIN INTRANET_PA_T_FRMSUM frmsum ON frmsub.RecordID = frmsum.RecordID
								INNER JOIN INTRANET_PA_S_FRMTPL frmtpl ON frmsum.TemplateID = frmtpl.TemplateID
								WHERE (FrmTitleEng LIKE '%F4%' OR FrmTitleChi LIKE '%F4%') AND frmsum.UserID='".$appraiseeID."' ORDER BY frmsub.RecordID ";
                            $f4FormRecordID = $this->returnVector($sql);
                            
                            $f4PrincipalScoreSum = 0;
                            $f4PrincipalScoreNo = 0;
                            $f4FormScoreSum = 0;
                            $f4FormScoreNo = 0;
                            foreach($f4FormRecordID as $f4Record){
                                $sql = "Select sub.FillerID, SUM(qlks.Score) as ScoreSum, COUNT(qlks.Score) as QNo From INTRANET_PA_T_SLF_DTL dtl
									INNER JOIN INTRANET_PA_T_FRMSUB  sub ON dtl.RecordID = sub.RecordID AND dtl.BatchID = sub.BatchID AND dtl.RlsNo = sub.RlsNo
									INNER JOIN INTRANET_PA_S_QLKS qlks ON dtl.MarkID = qlks.MarkID
									WHERE sub.RecordID='".$f4Record."' GROUP BY sub.RecordID ";
                                $scoreResult = current($this->returnArray($sql));
                                if(isset($specialIdentityList[$scoreResult['FillerID']]) && (in_array($scoreResult['FillerID'],$specialIdentityList['1']) || in_array($scoreResult['FillerID'],$specialIdentityList['2']))){
                                    $f4PrincipalScoreSum += ($scoreResult['ScoreSum']/$scoreResult['QNo']);
                                    $f4PrincipalScoreNo += 1;
                                }else{
                                    $f4FormScoreSum += ($scoreResult['ScoreSum']/$scoreResult['QNo']);
                                    $f4FormScoreNo += 1;
                                }
                            }
                            if($f4PrincipalScoreNo > 0 && $f4FormScoreNo > 0){
                                $f4FormScore = ($f4PrincipalScoreSum/$f4PrincipalScoreNo)*0.5 + ($f4FormScoreSum/$f4FormScoreNo)*0.5;
                            }elseif($f4PrincipalScoreNo > 0 && $f4FormScoreNo <= 0){
                                $f4FormScore = ($f4PrincipalScoreSum/$f4PrincipalScoreNo);
                            }elseif($f4PrincipalScoreNo <= 0 && $f4FormScoreNo > 0){
                                $f4FormScore = ($f4FormScoreSum/$f4FormScoreNo);
                            }
                            $f4FormScore = round($f4FormScore);
                        }
                    }
                    
                    $descWidth ="20%";
                    $content.="<tr>";
                    if($hasContent == true){
                        $content .="<th style=\"width:5%\"></th>";
                    }
                    $content .= "<th style=\"width:".$descWidth."\"></th>";
                    $dnmtr = (sizeof($b)==0)?1:sizeof($b);
                    $thWidth=60/$dnmtr;
                    for($j=0;$j<sizeof($b);$j++){
                        $content.="<th style=\"width:".floor($thWidth)."%\">".nl2br($b[$j]["Descr"])."</th>";
                    }
                    $content.="</tr>";
                    
                    for($i=0;$i<sizeof($a);$i++){
                        $hasRemark=$a[$i]["HasRmk"];
                        $rmkLabel=$a[$i]["RmkLabel"];
                        $rowSpan=($hasRemark==1)?"2":"1";
                        $intSecID=$a[$i]["SecID"];
                        //$isCurSecID=($currSecID==$intSecID)?"1":"0";
                        
                        //get the permission of writing data to the input field
                        $sql="SELECT ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE SecID='".IntegerSafe($intSecID)."';";
                        //echo $sql."<br/>";
                        $d=$this->returnResultSet($sql);
                        $parentSecID=$d[0]["ParentSecID"];
                        
                        $isCurSecID=($currParentSecID==$parentSecID)?"1":"0";
                        
                        $sql="SELECT CanFill FROM INTRANET_PA_S_SECROL WHERE SecID='".IntegerSafe($parentSecID)."' AND AppRoleID='".IntegerSafe($appRoleID)."';";
                        //echo  $sql."<br/>";
                        $e=$this->returnResultSet($sql);
                        $canFill=$e[0]["CanFill"];
                        
                        $intQusCodID=$i+1;
                        $qID=$a[$i]["QID"];
                        if($isSubmitted){
                            $sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,DATE_FORMAT(DateAns,'%Y-%m-%d') FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo='".$rlsNo."' AND RecordID='".$recordID."' AND QID='".$qID."';";
                            //echo $sql."<br/><br/>";
                            $c=$this->returnResultSet($sql);
                            $c = $this->getQuestionDataByCmpArr($c);
                            
                            $collaborateAns = array();
                            if(!empty($collaborativeForm)){
                                foreach($collaborativeForm as $cRecord){
                                    if($sys_custom['eAppraisal']['WusichongStyle']==true){
                                        $sql="SELECT dtl.RecordID,dtl.BatchID,dtl.QID,dtl.QAnsID,dtl.MarkID,dtl.Remark,qlks.Score,DATE_FORMAT(dtl.DateAns,'%Y-%m-%d') FROM INTRANET_PA_T_SLF_DTL dtl LEFT JOIN INTRANET_PA_S_QLKS qlks ON dtl.MarkID=qlks.MarkID WHERE dtl.RlsNo='".$rlsNo."' AND dtl.RecordID='".$cRecord."' AND dtl.QID='".$qID."';";
                                        //echo $sql."<br/><br/>";
                                    }else{
                                        $sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,DATE_FORMAT(DateAns,'%Y-%m-%d') FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo='".$rlsNo."' AND RecordID='".$cRecord."' AND QID='".$qID."';";
                                        //echo $sql."<br/><br/>";
                                    }
                                    //echo $sql."<br/><br/>";
                                    $recordResult=$this->returnResultSet($sql);
                                    if(!empty($recordResult)){
                                        $collaborateAns[$cRecord] = $this->getQuestionDataByCmpArr($recordResult);
                                    }
                                }
                                if(!empty($collaborateAns)){
                                    $collaborateAns[$recordID] = $c;
                                    
                                    foreach($collaborateAns as $rID=>$ansList){
                                        foreach($ansList as $index=>$ansDetail){
                                            $sql = "SELECT ".$indexVar['libappraisal']->Get_Name_Field("u.")." as FillerName, u.UserID FROM INTRANET_PA_T_FRMSUB frmsub
												INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON frmsub.FillerID=u.UserID
												WHERE RecordID='".$ansDetail['RecordID']."' AND BatchID='".$ansDetail['BatchID']."'";
                                            $filler = current($this->returnArray($sql));
                                            $fillerName = $filler['FillerName'];
                                            $fillerID = $filler['UserID'];
                                            $collaborateAns[$rID][$index]['FillerName'] = $fillerName;
                                            $collaborateAns[$rID][$index]['FillerID'] = $fillerID;
                                        }
                                    }
                                }
                            }
                        }
                        $content.="<tr>";
                        if($hasContent == true){
                            $content.="<td valign=\"top\" rowspan=\"".$rowSpan."\">".$a[$i]["QCodDes"]."</td>";
                        }
                        if(strstr($a[$i]["Descr"], '<br')){
                            $a[$i]["Descr"] = preg_replace( "/\r|\n/", "", $a[$i]["Descr"]);
                        }else{
                            $a[$i]["Descr"] = preg_replace( "/\r|\n/", "", nl2br($a[$i]["Descr"]));
                        }
                        $content.="<td rowspan=\"".$rowSpan."\">".(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"])."</td>";
                        
                        $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                        
                        if($sys_custom['eAppraisal']['WusichongStyle']==true && ($inF5 || $inF3) && !empty($collaborateAns)){
                            $totalScore = 0;
                            $totalPrincipalScore = 0;
                            $totalNonPrincipal = 0;
                            for($j=0;$j<sizeof($b);$j++){
                                if($inF5){
                                    foreach($collaborateAns as $multiC){
                                        if($multiC[0]["MarkID"]==$b[$j]["MarkID"]){
                                            $totalScore += $b[$j]["MarkCodDes"];
                                        }
                                    }
                                }elseif($inF3){
                                    foreach($collaborateAns as $multiC){
                                        if(in_array($multiC[0]['FillerID'],$specialIdentityList['1'])){
                                            if($multiC[0]["MarkID"]==$b[$j]["MarkID"]){
                                                $totalPrincipalScore = 	$b[$j]["MarkCodDes"];
                                            }
                                        }else{
                                            if($multiC[0]["MarkID"]==$b[$j]["MarkID"]){
                                                $totalScore += 	$b[$j]["MarkCodDes"];
                                                $totalNonPrincipal++;
                                            }
                                        }
                                    }
                                }
                            }
                            if($inF5){
                                $average = round($totalScore/count($collaborateAns));
                                $_SESSION['totalFormF5Score'] = $_SESSION['totalFormF5Score'] + $average;
                            }elseif($inF3){
                                $average = round(($totalScore/$totalNonPrincipal)*0.5 + $totalPrincipalScore*0.5);
                            }
                            for($j=0;$j<sizeof($b);$j++){
                                $content.="<td>";
                                $content .= ($average == $b[$j]["Score"])?"&#10004;":"";
                                $content.="</td>";
                            }
                        }else{
                            for($j=0;$j<sizeof($b);$j++){
                                $content.="<td>";
                                if($canFill==1 && $isCurSecID==1){
                                    if($sys_custom['eAppraisal']['WusichongStyle']==true && nl2br($a[$i]["Descr"])=="批改課業" && $currBatchOrder > 1){
                                        $sql = "SELECT MarkID FROM INTRANET_PA_S_QLKS WHERE QCatID='".$a[$i]["QCatID"]."' AND Score='".$f4FormScore."'";
                                        $f4FormMarkID = current($this->returnVector($sql));
                                        $content.=$indexVar['libappraisal_ui']->Get_Radio_Button("qID_".$a[$i]["QID"]."_".$j,"", $Value=$b[$j]["MarkID"], $isChecked=($f4FormMarkID==$b[$j]["MarkID"])?1:0, $Class=$isRequiredField, $Display=$b[$j]["MarkCodDes"], $Onclick="",$isDisabled=1);
                                    }else{
                                        $content.=$indexVar['libappraisal_ui']->Get_Radio_Button("qID_".$a[$i]["QID"]."_".$j,"qID_".$a[$i]["QID"], $Value=$b[$j]["MarkID"], $isChecked=($c[0]["MarkID"]==$b[$j]["MarkID"])?1:0, $Class=$isRequiredField, $Display=$b[$j]["MarkCodDes"], $Onclick="",$isDisabled=0);
                                    }
                                }
                                else{
                                    //$content.=$indexVar['libappraisal_ui']->Get_Radio_Button("qID_".$a[$i]["QID"]."_".$j,"qID_".$a[$i]["QID"], $Value=$b[$j]["MarkID"], $isChecked=($c[0]["MarkID"]==$b[$j]["MarkID"])?1:0, $Class=$isRequiredField, $Display=$b[$j]["MarkCodDes"], $Onclick="",$isDisabled=1);
                                    if(empty($collaborateAns)){
                                        $content .= ($c[0]["MarkID"]==$b[$j]["MarkID"])?"&#10004;".$b[$j]["MarkCodDes"]:"";
                                    }else{
                                        $hasTickAlready = false;
                                        foreach($collaborateAns as $multiC){
                                            $displayInCell = ($hasTickAlready==false)?"&#10004;".$b[$j]["MarkCodDes"].":<br>".$multiC[0]["FillerName"]:$multiC[0]["FillerName"];
                                            $content .= ($multiC[0]["MarkID"]==$b[$j]["MarkID"])?$displayInCell."<br>":"";
                                            if($multiC[0]["MarkID"]==$b[$j]["MarkID"]){
                                                $hasTickAlready = true;
                                            }
                                        }
                                    }
                                }
                                if($j==0){
                                    $content.=$indexVar['libappraisal_ui']->Get_Form_Warning_Msg("qID_".$a[$i]["QID"]."EmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
                                }
                                $content.="</td>";
                            }
                        }
                        $content.="</tr>";
                        //$content.="<input type=\"hidden\" id=\"MarkID_".$a[$i]["QID"]."\" name=\"MarkID_".$a[$i]["QID"]."\" value=\"".$b[$j]["MarkID"]."\">";
                        if($canFill==1 && $isCurSecID==1 && $sys_custom['eAppraisal']['WusichongStyle']==true && nl2br($a[$i]["Descr"])=="批改課業"  && $currBatchOrder > 1){
                            $content.='<input type="hidden" name="'."qID_".$a[$i]["QID"].'" value="'.$f4FormMarkID.'">';
                        }
                        if($hasRemark==1){
                            $content.="<tr>";
                            $content.="<td colspan=\"".sizeof($b)."\">";
                            if($rmkLabel!=""){
                                $content.= $rmkLabel."<br/>";
                            }
                            if($canFill==1 && $isCurSecID==1){
                                $content.=$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td>";
                            }
                            else{
                                if($hasRemark==1 && $c[0]["Remark"]!=""){
                                    if(!empty($collaborativeForm)){
                                        end($collaborateAns);
                                        $lastKey = key($collaborateAns);
                                        reset($collaborateAns);
                                        foreach($collaborateAns as $ckey=>$multiC){
                                            $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                            $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                        }
                                    }else{
                                        $content.=nl2br($c[0]["Remark"]);
                                    }
                                    $content.="</td>";
                                }
                            }
                            $content.="</tr>";
                        }
                        
                    }
                    
            }
            else if($quesDispMode==2){ // Matrix
                /*$sql = "SELECT MarkID,QCatID,".$this->getLangSQL("MarkCodChi","MarkCodEng")." as MarkCodDes,".$this->getLangSQL("DescrChi","DescrEng")." as Descr,Score,DisplayOrder,".
                 $this->getLangSQL("DescrChi","DescrEng")." as Descr
                 FROM INTRANET_PA_S_QLKS WHERE QCatID=".$qCatID."
                 ORDER BY DisplayOrder";
                 //echo $sql."<br/><br/>";
                 $b=$this->returnResultSet($sql);*/
                
                $sql = "SELECT RecordID,CycleID,TemplateID FROM INTRANET_PA_T_FRMSUM WHERE RecordID='".$recordID."';";
                $b0=$this->returnResultSet($sql);
                $templateID=$b0[0]["TemplateID"];
                
                $sql = "SELECT TemplateID,IsObs FROM INTRANET_PA_S_FRMTPL WHERE TemplateID='".$templateID."';";
                $b1=$this->returnResultSet($sql);
                $isObs = $b1[0]["IsObs"];
                
                if($isObs==1){
                    $sql="SELECT RlsNo,RecordID,BatchID,SecID FROM INTRANET_PA_T_FRMSUB WHERE '".$rlsNo."' AND RecordID='".$recordID."' AND BatchID='".$batchID."';";
                    //echo $sql."<br/>";
                    $b2=$this->returnResultSet($sql);
                    $currParentSecID=$b2[0]["SecID"];
                }
                
                $sql = "SELECT MtxFldID,QCatID,".$this->getLangSQL("FldHdrChi","FldHdrEng")." as FldHdrDes,InputType,DisplayOrder
						FROM INTRANET_PA_S_QMTX  WHERE QCatID='".$qCatID."'
						ORDER BY DisplayOrder";
                //echo $sql."<br/><br/>";
                $b=$this->returnResultSet($sql);
                $hasQuestionDisplay = false;
                for($i=0;$i<sizeof($a);$i++){
                    if($a[$i]["QCodDes"]!=""){
                        $hasQuestionDisplay = true;
                    }
                }
                
                $descWidth =100;
                $content.="<tr>";
                if($hasContent == true){
                    $content.="<th style=\"width:5%\"></th>";
                    $descWidth = 95;
                }
                $dnmtr = (sizeof($b)==0)?1:sizeof($b);
                if($hasQuestionDisplay){
                    $thWidth=$descWidth/($dnmtr+1);
                    $content.="<th style=\"width:".floor($thWidth)."%\"></th>";
                }else{
                    $thWidth=$descWidth/$dnmtr;
                    $content.="<th style=\"width:1%\"></th>";
                }
                for($j=0;$j<sizeof($b);$j++){
                    $content.="<th style=\"width:".floor($thWidth)."%\">".$b[$j]["FldHdrDes"]."</th>";
                }
                $content .= "</tr>";
                for($i=0;$i<sizeof($a);$i++){
                    $intSecID=$a[$i]["SecID"];
                    
                    $hasRemark=$a[$i]["HasRmk"];
                    $rmkLabel=$a[$i]["RmkLabel"];
                    $rowSpan=($hasRemark==1)?"2":"1";
                    //get the permission of writing data to the input field
                    $sql="SELECT ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE SecID='".IntegerSafe($intSecID)."';";
                    $d=$this->returnResultSet($sql);
                    $parentSecID=$d[0]["ParentSecID"];
                    
                    $isCurSecID=($currParentSecID==$parentSecID)?"1":"0";
                    
                    $sql="SELECT CanFill FROM INTRANET_PA_S_SECROL WHERE SecID='".IntegerSafe($parentSecID)."' AND AppRoleID='".IntegerSafe($appRoleID)."';";
                    $e=$this->returnResultSet($sql);
                    $canFill=$e[0]["CanFill"];
                    
                    $intQusCodID=$i+1;
                    $qID=$a[$i]["QID"];
                    if($isSubmitted){
                        $sql="SELECT RecordID,BatchID,QID,MtxFldID,Score,TxtAns,DateAns,Remark FROM INTRANET_PA_T_SLF_MTX  WHERE RlsNo='".$rlsNo."' AND RecordID='".$recordID."' AND QID='".$qID."';";
                        //echo $sql."<br/><br/>";
                        $c=$this->returnResultSet($sql);
                        $c = $this->getQuestionDataByCmpArr($c);
                        
                        $collaborateAns = array();
                        if(!empty($collaborativeForm)){
                            foreach($collaborativeForm as $cRecord){
                                $sql="SELECT RecordID,BatchID,QID,MtxFldID,Score,TxtAns,DateAns,Remark FROM INTRANET_PA_T_SLF_MTX WHERE RlsNo='".$rlsNo."' AND RecordID='".$cRecord."' AND QID='".$qID."';";
                                //echo $sql."<br/><br/>";
                                $recordResult=$this->returnResultSet($sql);
                                if(!empty($recordResult)){
                                    $collaborateAns[$cRecord] = $this->getQuestionDataByCmpArr($recordResult);
                                }
                            }
                            if(!empty($collaborateAns)){
                                $collaborateAns[$recordID] = $c;
                                
                                foreach($collaborateAns as $recordID=>$ansList){
                                    foreach($ansList as $index=>$ansDetail){
                                        $sql = "SELECT ".$this->Get_Name_Field("u.")." as FillerName FROM INTRANET_PA_T_FRMSUB frmsub
												INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON frmsub.FillerID=u.UserID
												WHERE RecordID='".$ansDetail['RecordID']."' AND BatchID='".$ansDetail['BatchID']."'";
                                        $fillerName = current($this->returnVector($sql));
                                        $collaborateAns[$recordID][$index]['FillerName'] = $fillerName;
                                    }
                                }
                            }
                        }
                    }
                    $content.="<tr>";
                    if($hasContent == true){
                        $content.="<td valign=\"top\" rowspan=\"".$rowSpan."\">".$a[$i]["QCodDes"]."</td>";
                    }
                    if(strstr($a[$i]["Descr"], '<br')){
                        $a[$i]["Descr"] = preg_replace( "/\r|\n/", "", $a[$i]["Descr"]);
                    }else{
                        $a[$i]["Descr"] = preg_replace( "/\r|\n/", "", nl2br($a[$i]["Descr"]));
                    }
                    $content.="<td rowspan=\"".$rowSpan."\">".(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"])."</td>";
                    $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                    for($j=0;$j<sizeof($b);$j++){
                        $content.="<td>";
                        if($b[$j]["InputType"]==0){
                            $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                            $txtAns = $c[0]["TxtAns"];
                            $txtAnsArr = explode("#$^",$txtAns);
                            if($canFill==1 && $isCurSecID==1){
                                $content.=$indexVar['libappraisal_ui']->GET_TEXTBOX("qIDTxtAns_".$a[$i]["QID"]."_".$j, "qIDTxtAns_".$a[$i]["QID"]."_".$j, $txtAnsArr[$j], $OtherClass=$isRequiredField, $OtherPar=array('maxlength'=>255))."".$unit;
                                $content.= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("qIDTxtAns_".$a[$i]["QID"]."_".$j."EmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
                            }
                            else{
                                if(empty($collaborateAns)){
                                    $content.= $txtAnsArr[$j]."".$unit;
                                }else{
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        $multiTxtAns = $multiC[0]["TxtAns"];
                                        $multiTxtAnsArr = explode("#$^",$multiTxtAns);
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".$multiTxtAnsArr[$j]."".$unit.$breakHtml;
                                    }
                                }
                            }
                        }
                        else if($b[$j]["InputType"]==1){
                            $scoreAns = $c[0]["Score"];
                            $scoreAnsArr = explode("#$^",$scoreAns);
                            $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                            if($canFill==1 && $isCurSecID==1){
                                $content.=$indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER("qIDScore_".$a[$i]["QID"]."_".$j, "qIDScore_".$a[$i]["QID"]."_".$j, $scoreAnsArr[$j], $OtherClass=$isRequiredField, $OtherPar=array('maxlength'=>255))."".$unit;
                                $content.= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("qIDScore_".$a[$i]["QID"]."_".$j."EmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
                            }
                            else{
                                if(empty($collaborateAns)){
                                    $content.= $scoreAnsArr[$j]."".$unit;
                                }else{
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        $multiTxtAns = $multiC[0]["Score"];
                                        $multiTxtAnsArr = explode("#$^",$multiTxtAns);
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".$multiTxtAnsArr[$j]."".$unit.$breakHtml;
                                    }
                                }
                            }
                        }
                        else if($b[$j]["InputType"]==2){
                            $datAns = $c[0]["DateAns"];
                            $datAnsArr = explode("#$^",$datAns);
                            if($canFill==1 && $isCurSecID==1){
                                $content.=$indexVar['libappraisal_ui']->GET_DATE_PICKER("qIDDate_".$a[$i]["QID"]."_".$j, $datAnsArr[$j], $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum ".$isRequiredField);
                                $content.= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("qIDDate_".$a[$i]["QID"]."_".$j."EmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
                            }
                            else{
                                if(empty($collaborateAns)){
                                    $content.= $datAnsArr[$j];
                                }else{
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        $multiTxtAns = $multiC[0]["DateAns"];
                                        $multiTxtAnsArr = explode("#$^",$multiTxtAns);
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".$multiTxtAnsArr[$j].$breakHtml;
                                    }
                                }
                            }
                        }
                        else if($b[$j]["InputType"]==3){
                            $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                            $txtAns = $c[0]["TxtAns"];
                            $txtAnsArr = explode("#$^",$txtAns);
                            if($canFill==1 && $isCurSecID==1){
                                $content.=$indexVar['libappraisal_ui']->GET_TEXTAREA($taName="qIDTxtAns_".$a[$i]["QID"]."_".$j, $taContents=$txtAnsArr[$j], $taCols=70, $taRows=5, $OnFocus = '$(this).height($(this).parent().height());', $readonly = "", $other='', $class=$isRequiredField, $taID="qIDTxtAns_".$a[$i]["QID"]."_".$j, $CommentMaxLength='');
                                //$content.=$indexVar['libappraisal_ui']->GET_TEXTBOX("qIDTxtAns_".$a[$i]["QID"]."_".$j, "qIDTxtAns_".$a[$i]["QID"]."_".$j, $txtAnsArr[$j], $OtherClass=$isRequiredField, $OtherPar=array('maxlength'=>255))."".$unit;
                                $content.= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("qIDTxtAns_".$a[$i]["QID"]."_".$j."EmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
                            }
                            else{
                                if(empty($collaborateAns)){
                                    $content.= nl2br($txtAnsArr[$j])."".$unit;
                                }else{
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        $multiTxtAns = $multiC[0]["TxtAns"];
                                        $multiTxtAnsArr = explode("#$^",$multiTxtAns);
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".$multiTxtAnsArr[$j].$breakHtml;
                                    }
                                }
                            }
                        }
                        $content.="<input type=\"hidden\" id=\"MtxFldID_".$a[$i]["QID"]."\" name=\"MtxFldID_".$a[$i]["QID"]."\" value=\"".$b[$j]["MtxFldID"]."\">";
                        $content.="</td>";
                    }
                    $content.="</tr>";
                    if($hasRemark==1){
                        $content.="<tr>";
                        $content.="<td colspan=\"".sizeof($b)."\">";
                        if($rmkLabel!=""){
                            $content.= $rmkLabel."<br/>";
                        }
                        if($canFill==1 && $isCurSecID==1){
                            $content.=$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td>";
                        }
                        else{
                            if(!empty($collaborativeForm)){
                                end($collaborateAns);
                                $lastKey = key($collaborateAns);
                                reset($collaborateAns);
                                foreach($collaborateAns as $ckey=>$multiC){
                                    $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                    $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                }
                            }else{
                                $content.=nl2br($c[0]["Remark"]);
                            }
                            $content .= "</td>";
                        }
                        $content.="</tr>";
                    }
                }
            }
            else if($quesDispMode==0){ // Standard Question
                for($i=0;$i<sizeof($a);$i++){
                    $intSecID=$a[$i]["SecID"];
                    
                    //get the permission of writing data to the input field
                    $sql="SELECT ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE SecID='".IntegerSafe($intSecID)."';";
                    $d=$this->returnResultSet($sql);
                    $parentSecID=$d[0]["ParentSecID"];
                    
                    $sql = "SELECT RecordID,CycleID,TemplateID FROM INTRANET_PA_T_FRMSUM WHERE RecordID='".$recordID."';";
                    $b0=$this->returnResultSet($sql);
                    $templateID=$b0[0]["TemplateID"];
                    
                    $sql = "SELECT TemplateID,IsObs FROM INTRANET_PA_S_FRMTPL WHERE TemplateID='".$templateID."';";
                    $b1=$this->returnResultSet($sql);
                    $isObs = $b1[0]["IsObs"];
                    
                    if($isObs==1){
                        $sql="SELECT RlsNo,RecordID,BatchID,SecID FROM INTRANET_PA_T_FRMSUB WHERE '".$rlsNo."' AND RecordID='".$recordID."' AND BatchID='".$batchID."';";
                        //echo $sql."<br/>";
                        $b2=$this->returnResultSet($sql);
                        $currParentSecID=$b2[0]["SecID"];
                    }
                    
                    $isCurSecID=($currParentSecID==$parentSecID)?"1":"0";
                    
                    $sql="SELECT CanFill FROM INTRANET_PA_S_SECROL WHERE SecID='".IntegerSafe($parentSecID)."' AND AppRoleID='".IntegerSafe($appRoleID)."';";
                    $e=$this->returnResultSet($sql);
                    $canFill=$e[0]["CanFill"];
                    
                    $intQusCodID=$i+1;
                    $qID=$a[$i]["QID"];
                    if(strstr($a[$i]["Descr"], '<br')){
                        $a[$i]["Descr"] = preg_replace( "/\r|\n/", "", $a[$i]["Descr"]);
                    }else{
                        $a[$i]["Descr"] = preg_replace( "/\r|\n/", "", nl2br($a[$i]["Descr"]));
                    }
                    $a[$i]["Descr"] = "<span style='line-height:200%;font-weight:bold;'>".$a[$i]["Descr"]."</span>";
                    if($isSubmitted){
                        $sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,TxtAns,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo='".$rlsNo."' AND RecordID='".$recordID."' AND QID='".$qID."';";
                        //echo $sql."<br/><br/>";;
                        $c=$this->returnResultSet($sql);
                        $c = $this->getQuestionDataByCmpArr($c);
                        
                        $collaborateAns = array();
                        if(!empty($collaborativeForm)){
                            foreach($collaborativeForm as $cRecord){
                                $sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,TxtAns,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo='".$rlsNo."' AND RecordID='".$cRecord."' AND QID='".$qID."';";
                                //echo $sql."<br/><br/>";
                                $recordResult=$this->returnResultSet($sql);
                                if(!empty($recordResult)){
                                    $collaborateAns[$cRecord] = $this->getQuestionDataByCmpArr($recordResult);
                                }
                            }
                            if(!empty($collaborateAns)){
                                $collaborateAns[$recordID] = $c;
                                
                                foreach($collaborateAns as $recordID=>$ansList){
                                    foreach($ansList as $index=>$ansDetail){
                                        $sql = "SELECT ".$this->Get_Name_Field("u.")." as FillerName FROM INTRANET_PA_T_FRMSUB frmsub
												INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON frmsub.FillerID=u.UserID
												WHERE RecordID='".$ansDetail['RecordID']."' AND BatchID='".$ansDetail['BatchID']."'";
                                        $fillerName = current($this->returnVector($sql));
                                        $collaborateAns[$recordID][$index]['FillerName'] = $fillerName;
                                    }
                                }
                            }
                        }
                    }
                    if($a[$i]["InputType"]==0){
                        $content.="<tr>";
                        $content.="<td valign=\"top\" style=\"border:none;width:5%\">";
                        $content.=$a[$i]["QCodDes"];
                        $content.="</td>";
                        $content.="<td style=\"border:none;width:95%\">";
                        $content.=(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"]);
                        $content.="</td>";
                        $content.="</tr>";
                        
                        $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($a[$i]["Rmk"])."</td></tr>";
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class=$isRequiredField, $taID='', $CommentMaxLength='')."</td></tr>";
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                if(!empty($collaborativeForm)){
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        if($multiC[0]['Remark']==""){
                                            continue;
                                        }
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                    }
                                }else{
                                    $content.=nl2br($c[0]["Remark"]);
                                }
                                $content.="</td></tr>";
                            }
                        }
                        else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class=$isRequiredField, $taID='', $CommentMaxLength='')."</td></tr>";
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                if(!empty($collaborativeForm)){
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        if($multiC[0]['Remark']==""){
                                            continue;
                                        }
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                    }
                                }else{
                                    $content.=nl2br($c[0]["Remark"]);
                                }
                                $content.="</td></tr>";
                            }
                        }
                        
                        $content .= "<tr>";
                        $content .= "<td></td><td>";
                        $content .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("qIDRmk_".$a[$i]["QID"]."EmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
                        $content .= "</td>";
                        $content .= "</tr>";
                    }
                    else if($a[$i]["InputType"]==1){
                        $sql="SELECT QAnsID,QID,".$this->getLangSQL("DescrChi","DescrEng")." as Descr,".$this->getLangSQL("QAnsChi","QAnsEng")." as QAns,DisplayOrder
						FROM INTRANET_PA_S_QANS WHERE QID='".$a[$i]["QID"]."' ORDER BY DisplayOrder;";
                        //echo $sql."<br/><br/>";
                        $b=$this->returnResultSet($sql);
                        $content.="<tr>";
                        $content.="<td valign=\"top\" style=\"border:none;width:5%\">";
                        $content.=$a[$i]["QCodDes"];
                        $content.="</td>";
                        $content.="<td style=\"border:none;width:95%\">";
                        $content.=(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"]);
                        $content.="</td>";
                        $content.="</tr>";
                        
                        if(empty($collaborateAns) || ($canFill==1 && $isCurSecID==1)){
                            $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                            for($j=0;$j<sizeof($b);$j++){
                                $content.="<tr><td style=\"border:none\"></td>";
                                $content.="<td style=\"border:none\">";
                                if($canFill==1 && $isCurSecID==1){
                                    $content.=$indexVar['libappraisal_ui']->Get_Radio_Button("qID_".$a[$i]["QID"]."_".$j, "qID_".$a[$i]["QID"], $Value=$b[$j]["QAnsID"], $isChecked=($c[0]["QAnsID"]==$b[$j]["QAnsID"])?1:0, $Class=$isRequiredField, $Display=$b[$j]["Descr"], $Onclick="",$isDisabled=0,$isIndented=1, $specialLabel=$b[$j]["QAns"]);
                                }
                                else{
                                    $content.=$indexVar['libappraisal_ui']->Get_Radio_Button("qID_".$a[$i]["QID"]."_".$j, "qID_".$a[$i]["QID"], $Value=$b[$j]["QAnsID"], $isChecked=($c[0]["QAnsID"]==$b[$j]["QAnsID"])?1:0, $Class=$isRequiredField, $Display=$b[$j]["Descr"], $Onclick="",$isDisabled=1,$isIndented=1, $specialLabel=$b[$j]["QAns"]);
                                }
                                $content.="</td>";
                                $content.="</tr>";
                            }
                        }else{
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none\"><table>";
                            end($collaborateAns);
                            $lastKey = key($collaborateAns);
                            reset($collaborateAns);
                            $content.="<tr><td style=\"border:none\"></td>";
                            foreach($collaborateAns as $ckey=>$multiC){
                                $content .= "<td style=\"border:none\">".$multiC[0]["FillerName"]."</td>";
                                if($ckey!=$lastKey){
                                    $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                }
                            }
                            $content.="</tr>";
                            
                            $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                            for($j=0;$j<sizeof($b);$j++){
                                $content.="<tr><td style=\"border:none\"></td>";
                                foreach($collaborateAns as $ckey=>$multiC){
                                    $content.="<td style=\"border:none\">";
                                    $content.=$indexVar['libappraisal_ui']->Get_Radio_Button($ckey."_qID_".$a[$i]["QID"]."_".$j, $ckey."_qID_".$a[$i]["QID"], $Value=$b[$j]["QAnsID"], $isChecked=($multiC[0]["QAnsID"]==$b[$j]["QAnsID"])?1:0, $Class=$isRequiredField, $Display=$b[$j]["Descr"], $Onclick="",$isDisabled=1,$isIndented=1, $specialLabel=$b[$j]["QAns"]);
                                    $content.="</td>";
                                    if($ckey!=$lastKey){
                                        $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                    }
                                }
                                $content.="</tr>";
                            }
                            $content.="</table></td>";
                            $content.="</tr>";
                        }
                        
                        $content .= "<tr>";
                        $content .= "<td></td><td>";
                        $content .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("qID_".$a[$i]["QID"]."EmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
                        $content .= "</td>";
                        $content .= "</tr>";
                        
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$a[$i]["Rmk"]."</td></tr>";
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                if(!empty($collaborativeForm)){
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        if($multiC[0]['Remark']==""){
                                            continue;
                                        }
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                    }
                                }else{
                                    $content.=nl2br($c[0]["Remark"]);
                                }
                                $content.="</td></tr>";
                            }
                        }
                        else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                if(!empty($collaborativeForm)){
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        if($multiC[0]['Remark']==""){
                                            continue;
                                        }
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                    }
                                }else{
                                    $content.=nl2br($c[0]["Remark"]);
                                }
                                $content.="</td></tr>";
                            }
                        }
                    }
                    else if($a[$i]["InputType"]==2){
                        $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                        $content.="<tr>";
                        $content.="<td valign=\"top\" style=\"border:none;width:5%\">";
                        $content.=$a[$i]["QCodDes"];
                        $content.="</td>";
                        $content.="<td style=\"border:none;width:95%\">";
                        $content.=(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"]);
                        $content.="</td>";
                        $content.="</tr>";
                        
                        $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                        if(empty($collaborateAns) || ($canFill==1 && $isCurSecID==1)){
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTBOX("qIDTxtAns_".$a[$i]["QID"], "qIDTxtAns_".$a[$i]["QID"], $c[0]["TxtAns"], $OtherClass='textboxUnit '.$isRequiredField, $OtherPar=array('maxlength'=>255))."".$unit."</td>";;
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$c[0]["TxtAns"]."".$unit."</td>";
                            }
                        }else{
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none\"><table>";
                            end($collaborateAns);
                            $lastKey = key($collaborateAns);
                            reset($collaborateAns);
                            $content.="<tr>";
                            foreach($collaborateAns as $ckey=>$multiC){
                                $content .= "<td style=\"border:none\">".$multiC[0]["FillerName"]."</td>";
                                if($ckey!=$lastKey){
                                    $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                }
                            }
                            $content.="</tr>";
                            
                            $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                            $content.="<tr>";
                            foreach($collaborateAns as $ckey=>$multiC){
                                $content.="<td>".$multiC[0]["TxtAns"]."".$unit."</td>";
                                if($ckey!=$lastKey){
                                    $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                }
                            }
                            $content.="</tr>";
                            $content.="</table></td>";
                            $content.="</tr>";
                        }
                        $content.="</tr>";
                        
                        $content .= "<tr>";
                        $content .= "<td></td><td>";
                        $content .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("qIDTxtAns_".$a[$i]["QID"]."EmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
                        $content .= "</td>";
                        $content .= "</tr>";
                        
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$a[$i]["Rmk"]."</td></tr>";
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                if(!empty($collaborativeForm)){
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        if($multiC[0]['Remark']==""){
                                            continue;
                                        }
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                    }
                                }else{
                                    $content.=nl2br($c[0]["Remark"]);
                                }
                                $content.="</td></tr>";
                            }
                        }
                        else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                if(!empty($collaborativeForm)){
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        if($multiC[0]['Remark']==""){
                                            continue;
                                        }
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                    }
                                }else{
                                    $content.=nl2br($c[0]["Remark"]);
                                }
                                $content.="</td></tr>";
                            }
                        }
                    }
                    else if($a[$i]["InputType"]==3){
                        $unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
                        $content.="<tr>";
                        $content.="<td valign=\"top\" style=\"border:none;width:5%\">";
                        $content.=$a[$i]["QCodDes"];
                        $content.="</td>";
                        $content.="<td style=\"border:none;width:95%\">";
                        $content.=(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"]);
                        $content.="</td>";
                        $content.="</tr>";
                        
                        $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                        if($canFill==1 && $isCurSecID==1){
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER("qIDScore_".$a[$i]["QID"], "qIDScore_".$a[$i]["QID"], $c[0]["Score"], $OtherClass=$isRequiredField, $OtherPar=array('maxlength'=>255))."".$unit."</td></tr>";
                        }
                        else{
                            if(empty($collaborateAns)){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$c[0]["Score"]."".$unit."</td></tr>";
                            }else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                $content .= "<table>";
                                end($collaborateAns);
                                $lastKey = key($collaborateAns);
                                reset($collaborateAns);
                                $content.="<tr>";
                                foreach($collaborateAns as $ckey=>$multiC){
                                    $content .= "<td style=\"border:none\">".$multiC[0]["FillerName"]."</td>";
                                    if($ckey!=$lastKey){
                                        $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                    }
                                }
                                $content.="</tr>";
                                $content.="<tr>";
                                foreach($collaborateAns as $ckey=>$multiC){
                                    $content.="<td style=\"border:none;\">".$multiC[0]["Score"]."".$unit."</td>";
                                    if($ckey!=$lastKey){
                                        $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                    }
                                }
                                $content.="</tr>";
                                $content .= "</table></td></tr>";
                            }
                            //$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$c[0]["Score"]."".$unit."</td></tr>";
                        }
                        
                        $content .= "<tr>";
                        $content .= "<td></td><td>";
                        $content .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("qIDScore_".$a[$i]["QID"]."EmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
                        $content .= "</td>";
                        $content .= "</tr>";
                        
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$a[$i]["Rmk"]."</td></tr>";
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                if(!empty($collaborativeForm)){
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        if($multiC[0]['Remark']==""){
                                            continue;
                                        }
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                    }
                                }else{
                                    $content.=nl2br($c[0]["Remark"]);
                                }
                                $content.="</td></tr>";
                            }
                        }
                        else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                if(!empty($collaborativeForm)){
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        if($multiC[0]['Remark']==""){
                                            continue;
                                        }
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                    }
                                }else{
                                    $content.=nl2br($c[0]["Remark"]);
                                }
                                $content.="</td></tr>";
                            }
                        }
                        
                    }
                    else if($a[$i]["InputType"]==4){
                        $content.="<tr>";
                        $content.="<td valign=\"top\" style=\"border:none;width:5%\">";
                        $content.=$a[$i]["QCodDes"];
                        $content.="</td>";
                        $content.="<td style=\"border:none;width:95%\">";
                        $content.=(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"]);
                        $content.="</td>";
                        $content.="</tr>";
                        
                        $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                        
                        if($canFill==1 && $isCurSecID==1){
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_DATE_PICKER("qIDDate_".$a[$i]["QID"], $c[0]["DateAns"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('AppPrdFr')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum ".$isRequiredField)."</td></tr>";
                        }
                        else{
                            if(empty($collaborateAns)){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$c[0]["DateAns"]."</td></tr>";
                            }else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;\">";
                                $content .= "<table>";
                                end($collaborateAns);
                                $lastKey = key($collaborateAns);
                                reset($collaborateAns);
                                $content.="<tr>";
                                foreach($collaborateAns as $ckey=>$multiC){
                                    $content .= "<td style=\"border:none\">".$multiC[0]["FillerName"]."</td>";
                                    if($ckey!=$lastKey){
                                        $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                    }
                                }
                                $content.="</tr>";
                                $content.="<tr>";
                                foreach($collaborateAns as $ckey=>$multiC){
                                    $content.="<td style=\"border:none;\">".$multiC[0]["DateAns"]."</td>";
                                    if($ckey!=$lastKey){
                                        $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                    }
                                }
                                $content.="</tr>";
                                $content .= "</table></td></tr>";
                            }
                        }
                        
                        $content .= "<tr>";
                        $content .= "<td></td><td>";
                        $content .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("qIDDate_".$a[$i]["QID"]."EmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
                        $content .= "</td>";
                        $content .= "</tr>";
                        
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$a[$i]["Rmk"]."</td></tr>";
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                if(!empty($collaborativeForm)){
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        if($multiC[0]['Remark']==""){
                                            continue;
                                        }
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                    }
                                }else{
                                    $content.=nl2br($c[0]["Remark"]);
                                }
                                $content.="</td></tr>";
                            }
                        }
                        else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                if(!empty($collaborativeForm)){
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        if($multiC[0]['Remark']==""){
                                            continue;
                                        }
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                    }
                                }else{
                                    $content.=nl2br($c[0]["Remark"]);
                                }
                                $content.="</td></tr>";
                            }
                        }
                    }
                    else if($a[$i]["InputType"]==5){
                        $content.="<tr>";
                        $content.="<td valign=\"top\" style=\"border:none;width:5%\">";
                        $content.=$a[$i]["QCodDes"];
                        $content.="</td>";
                        $content.="<td style=\"border:none;width:95%\">";
                        $content.=(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"]);
                        $content.="</td>";
                        $content.="</tr>";
                        
                        $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                        
                        if($canFill==1 && $isCurSecID==1){
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class=$isRequiredField, $taID='', $CommentMaxLength='')."</td></tr>";
                        }
                        else{
                            if(empty($collaborateAns)){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($c[0]["Remark"])."</td></tr>";
                            }else{
                                if($sys_custom['eAppraisal']['WusichongStyle']){
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        $content .= "<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                        $content .= $multiC[0]["FillerName"];
                                        $content .= "</td><tr>";
                                        $content .= "<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                        $content .= $indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"]."_".$ckey, $taContents=$multiC[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly=true, $other='', $class=$isRequiredField, $taID='', $CommentMaxLength='');
                                        $content .= "</td><tr>";
                                    }
                                }else{
                                    $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;\">";
                                    $content .= "<table>";
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    $content.="<tr>";
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        $content .= "<td style=\"border:none\">".$multiC[0]["FillerName"]."</td>";
                                        if($ckey!=$lastKey){
                                            $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                        }
                                    }
                                    $content.="</tr>";
                                    $content.="<tr>";
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        $content.="<td style=\"border:none;\">".nl2br($multiC[0]["Remark"])."</td>";
                                        if($ckey!=$lastKey){
                                            $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                        }
                                    }
                                    $content.="</tr>";
                                    $content .= "</table></td></tr>";
                                }
                            }
                        }
                        
                        $content .= "<tr>";
                        $content .= "<td></td><td>";
                        $content .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("qIDRmk_".$a[$i]["QID"]."EmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
                        $content .= "</td>";
                        $content .= "</tr>";
                        
                    }
                    else if($a[$i]["InputType"]==6){
                        $sql="SELECT QAnsID,QID,".$this->getLangSQL("DescrChi","DescrEng")." as Descr,".$this->getLangSQL("QAnsChi","QAnsEng")." as QAns,DisplayOrder
						FROM INTRANET_PA_S_QANS WHERE QID='".$a[$i]["QID"]."' ORDER BY DisplayOrder;";
                        //echo $sql."<br/><br/>";
                        $b=$this->returnResultSet($sql);
                        $content.="<tr>";
                        $content.="<td valign=\"top\" style=\"border:none;width:5%\">";
                        $content.=$a[$i]["QCodDes"];
                        $content.="</td>";
                        $content.="<td style=\"border:none;width:95%\">";
                        $content.=(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"]);
                        $content.="</td>";
                        $content.="</tr>";
                        
                        $mcmAnsID = explode(",",$c[0]["TxtAns"]);
                        $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                        if(empty($collaborateAns) || ($canFill==1 && $isCurSecID==1)){
                            for($j=0;$j<sizeof($b);$j++){
                                $content.="<tr><td style=\"border:none\"></td>";
                                $content.="<td style=\"border:none\">";
                                $chkBoxChecked = 0;
                                for($k=0;$k<sizeof($mcmAnsID);$k++){
                                    if($b[$j]["QAnsID"] == $mcmAnsID[$k]){
                                        $chkBoxChecked = 1;
                                        break;
                                    }
                                }
                                if($canFill==1 && $isCurSecID==1){
                                    $content.=$indexVar['libappraisal_ui']->Get_Checkbox("qID_".$a[$i]["QID"]."_".$j, "qID_".$a[$i]["QID"]."[]", $Value=$b[$j]["QAnsID"], $isChecked=$chkBoxChecked, $Class=$isRequiredField, $Display=$b[$j]["Descr"], $Onclick="",$isDisabled=0,$isIndented=1,$specialLabel=$b[$j]["QAns"]);
                                }
                                else{
                                    $content.=$indexVar['libappraisal_ui']->Get_Checkbox("qID_".$a[$i]["QID"]."_".$j, "qID_".$a[$i]["QID"]."[]", $Value=$b[$j]["QAnsID"], $isChecked=$chkBoxChecked, $Class=$isRequiredField, $Display=$b[$j]["Descr"], $Onclick="",$isDisabled=1,$isIndented=1,$specialLabel=$b[$j]["QAns"]);
                                }
                                
                                $content.="</td>";
                                $content.="</tr>";
                            }
                        }else{
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;\">";
                            $content .= "<table>";
                            end($collaborateAns);
                            $lastKey = key($collaborateAns);
                            reset($collaborateAns);
                            $content.="<tr>";
                            foreach($collaborateAns as $ckey=>$multiC){
                                $content .= "<td style=\"border:none\">".$multiC[0]["FillerName"]."</td>";
                                if($ckey!=$lastKey){
                                    $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                }
                            }
                            $content.="</tr>";
                            for($j=0;$j<sizeof($b);$j++){
                                $content.="<tr>";
                                foreach($collaborateAns as $ckey=>$multiC){
                                    $content.="<td style=\"border:none\">";
                                    $mcmAnsID = explode(",",$multiC[0]["TxtAns"]);
                                    $chkBoxChecked = 0;
                                    for($k=0;$k<sizeof($mcmAnsID);$k++){
                                        if($b[$j]["QAnsID"] == $mcmAnsID[$k]){
                                            $chkBoxChecked = 1;
                                            break;
                                        }
                                    }
                                    $content.=$indexVar['libappraisal_ui']->Get_Checkbox("qID_".$a[$i]["QID"]."_".$j, "qID_".$a[$i]["QID"]."[]", $Value=$b[$j]["QAnsID"], $isChecked=$chkBoxChecked, $Class=$isRequiredField, $Display=$b[$j]["Descr"], $Onclick="",$isDisabled=1,$isIndented=1,$specialLabel=$b[$j]["QAns"]);
                                    
                                    $content.="</td>";
                                    if($ckey!=$lastKey){
                                        $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                    }
                                }
                                $content.="</tr>";
                            }
                            $content.="<tr>";
                            
                            $content.="</tr>";
                            $content .= "</table></td></tr>";
                        }
                        
                        $content .= "<tr>";
                        $content .= "<td></td><td>";
                        $content .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("qID_".$a[$i]["QID"]."EmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
                        $content .= "</td>";
                        $content .= "</tr>";
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$a[$i]["Rmk"]."</td></tr>";
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                if(!empty($collaborativeForm)){
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        if($multiC[0]['Remark']==""){
                                            continue;
                                        }
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                    }
                                }else{
                                    $content.=nl2br($c[0]["Remark"]);
                                }
                                $content.="</td></tr>";
                            }
                        }
                        else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                if(!empty($collaborativeForm)){
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        if($multiC[0]['Remark']==""){
                                            continue;
                                        }
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                    }
                                }else{
                                    $content.=nl2br($c[0]["Remark"]);
                                }
                                $content.="</td></tr>";
                            }
                        }
                    }
                    else if($a[$i]["InputType"]==7){
                        $sql="SELECT CanFill,CanBrowse FROM INTRANET_PA_S_SECROL WHERE SecID='".IntegerSafe($parentSecID)."' AND AppRoleID='".IntegerSafe($appRoleID)."';";
                        //echo $sql."<br/><br/>";
                        $e=$this->returnResultSet($sql);
                        $canFill=$e[0]["CanFill"];
                        $canBrowse=$e[0]["CanBrowse"];
                        
                        $isRequiredField = ($a[$i]["IsMand"]==1)?"requiredField":"";
                        
                        $content.="<tr>";
                        $content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
                        $content.="<td style=\"border:none;width:95%\">";
                        $content.=(($a[$i]["IsMand"]==1)?$indexVar['libappraisal_ui']->RequiredSymbol():"").nl2br($a[$i]["Descr"]);
                        $content.="</td>";
                        $content.="</tr>";
                        $content.="<tr><td></td>";
                        $content.="<td>";
                        //content.="<input type=\"file\" name=\"qID_".$a[$i]["QID"]."\" id=\"qID_".$a[$i]["QID"]."\" class=\"file ".$isRequiredField."\">";
                        if($canFill==1 && $isCurSecID==1){
                            $content.="<input type=\"file\" name=\"qID_".$a[$i]["QID"]."\" id=\"qID_".$a[$i]["QID"]."\" class=\"file ".$isRequiredField."\">";
                            $content.="<input type=\"hidden\" id=\"hdQID_".$a[$i]["QID"]."\" name=\"hdQID_".$a[$i]["QID"]."\" value=".urlencode($c[0]["TxtAns"])."><br/>";
                        }
                        if($c[0]["TxtAns"]!="" && $canBrowse==1){
                            //							$tmpFilePath = $file_parentPath.'sdf_file/'.$batchID.'/'.$c[0]["TxtAns"];
                            //							$tmpFilePathAbs = downloadAttachmentRelativePathToAbsolutePath($tmpFilePath);
                            //							$content.="<a id='download_"."qID_".$a[$i]["QID"]."' class=\"tablelink\" href=\"/home/download_attachment.php?target_e=".getEncryptedText($tmpFilePathAbs)."\">".$Lang['Appraisal']['CycleTemplate']['DownloadFiles']."</a>";
                            if(empty($collaborateAns) || ($canFill==1 && $isCurSecID==1)){
                                $tmpFilePath = $file_parentPath.'sdf_file/'.$c[0]["BatchID"].'/'.$c[0]["TxtAns"];
                                $tmpFilePathAbs = downloadAttachmentRelativePathToAbsolutePath($tmpFilePath);
                                $fileDataArr = explode("|",$c[0]["TxtAns"]);
                                $content.=$Lang['Appraisal']['CycleTemplate']['DownloadFiles'].": <a id='download_"."qID_".$a[$i]["QID"]."' class=\"tablelink\" href=\"/home/download_attachment.php?target_e=".getEncryptedText($tmpFilePathAbs)."&filename_e=".getEncryptedText($fileDataArr[2])."\">".$fileDataArr[2]."</a>";
                            }else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;\">";
                                $content .= "<table>";
                                end($collaborateAns);
                                $lastKey = key($collaborateAns);
                                reset($collaborateAns);
                                $content.="<tr>";
                                foreach($collaborateAns as $ckey=>$multiC){
                                    $content .= "<td style=\"border:none\">".$multiC[0]["FillerName"]."</td>";
                                    if($ckey!=$lastKey){
                                        $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                    }
                                }
                                $content.="</tr>";
                                $content.="<tr>";
                                foreach($collaborateAns as $ckey=>$multiC){
                                    $content .= "<td style=\"border:none\">";
                                    $tmpFilePath = $intranet_root.'/file/appraisal/sdf_file/'.$multiC[0]["BatchID"].'/'.$multiC[0]["TxtAns"];
                                    $tmpFilePathAbs = downloadAttachmentRelativePathToAbsolutePath($tmpFilePath);
                                    $fileDataArr = explode("|",$multiC[0]["TxtAns"]);
                                    //$content.="<a id='download_"."qID_".$a[$i]["QID"]."' class=\"tablelink\" href=\"/home/download_attachment.php?target_e=".getEncryptedText($tmpFilePathAbs)."&filename_e=".getEncryptedText($fileDataArr[2])."\">".$fileDataArr[2]."</a>";
                                    $content.=$Lang['Appraisal']['CycleTemplate']['DownloadFiles'].": <a id='download_"."qID_".$a[$i]["QID"]."' class=\"tablelink\" href=\"/home/download_attachment.php?target_e=".getEncryptedText($tmpFilePathAbs)."&filename_e=".getEncryptedText($fileDataArr[2])."\">".$fileDataArr[2]."</a>";
                                    $content .= "</td>";
                                    if($ckey!=$lastKey){
                                        $content .= "<td style=\"border:none\">&nbsp;&nbsp;</td>";
                                    }
                                }
                                $content.="</tr>";
                                $content .= "</table></td></tr>";
                            }
                        }
                        
                        $content.="</td>";
                        $content.="</tr>";
                        
                        $content .= "<tr>";
                        $content .= "<td></td><td>";
                        $content .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("qID_".$a[$i]["QID"]."EmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
                        $content .= "</td>";
                        $content .= "</tr>";
                        
                        
                        if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
                            $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$a[$i]["Rmk"]."</td></tr>";
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                if(!empty($collaborativeForm)){
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        if($multiC[0]['Remark']==""){
                                            continue;
                                        }
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                    }
                                }else{
                                    $content.=nl2br($c[0]["Remark"]);
                                }
                                $content.="</td></tr>";
                            }
                        }
                        else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]==""){
                            if($canFill==1 && $isCurSecID==1){
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$indexVar['libappraisal_ui']->GET_TEXTAREA("qIDRmk_".$a[$i]["QID"], $taContents=$c[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
                            }
                            else{
                                $content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">";
                                if(!empty($collaborativeForm)){
                                    end($collaborateAns);
                                    $lastKey = key($collaborateAns);
                                    reset($collaborateAns);
                                    foreach($collaborateAns as $ckey=>$multiC){
                                        if($multiC[0]['Remark']==""){
                                            continue;
                                        }
                                        $breakHtml = ($ckey==$lastKey)?"":"<br><br>";
                                        $content.= $multiC[0]["FillerName"].":<br>".nl2br($multiC[0]['Remark']).$breakHtml;
                                    }
                                }else{
                                    $content.=nl2br($c[0]["Remark"]);
                                }
                                $content.="</td></tr>";
                            }
                        }
                    }
                    $content .= "<tr><td colspan='2' style='height:10px;'></td></tr>";
                }
            }
            //echo $content;
            return $content;
        }
        function getSectionUserRole($userID,$recordID,$batchID,$secID,$rlsNo){
            global $UserID;
            $sql = "SELECT AppRoleID FROM INTRANET_PA_C_FRMROL frmrol INNER JOIN INTRANET_PA_T_FRMSUM frmsum ON frmrol.TemplateID=frmsum.TemplateID WHERE frmrol.UserID='".IntegerSafe($UserID)."' AND frmsum.RecordID='".IntegerSafe($recordID)."'";
            $isSpecialRole = $this->returnVector($sql);
            $sql = "SELECT UserID,AprID,GrpID,IdvID FROM INTRANET_PA_T_FRMSUM WHERE RecordID='".IntegerSafe($recordID)."'";
            $normalRoles = current($this->returnArray($sql));
            $appraisee = $normalRoles['UserID'];
            $appraiser = $normalRoles['AprID'];
            $isAlsoAppraisee = false;
            $isAlsoAppraiser = false;
            $isGroup = ($normalRoles['GrpID']=="")?false:true;
            $isIdv = ($normalRoles['IdvID']=="")?false:true;
            $appRole = "";
            if(empty($isSpecialRole)){
                if($appraisee==$UserID){
                    $appRole = 2;
                }
                if($appraiser==$UserID){
                    $appRole = 1;
                }
                if($appraisee==$UserID && $appraiser==$UserID){
                    $appRole = "1,2";
                }
            }else{
                $appRole = implode(",",$isSpecialRole);
                if($appraisee==$UserID){
                    $isAlsoAppraisee = true;
                    $appRole = $appRole.",2";
                }
                if($appraiser==$UserID){
                    $isAlsoAppraiser = true;
                    $appRole = $appRole.",1";
                }
            }
            $grpInfo = array();
            if($isGroup && $appRole==""){
                $sql = "SELECT AppMode,LeaderRole FROM INTRANET_PA_C_GRPFRM grpfrm
						INNER JOIN INTRANET_PA_C_GRPMEM grpmem ON grpfrm.GrpID=grpmem.GrpID WHERE grpfrm.GrpID='".$normalRoles['GrpID']."' AND grpmem.UserID='".IntegerSafe($UserID)."' AND IsLeader=1 AND grpfrm.Incl=1";
                $grpInfo = current($this->returnArray($sql));
                switch($grpInfo['AppMode']){
                    case 1:
                        $appRole = 1;
                        break;
                    case 2:
                        $appRole = 1;
                        break;
                    case 3:
                        $appRole = 2;
                        break;
                    case 4:
                        $appRole = "1,2";
                        break;
                }
            }
            $sql = "SELECT BatchOrder FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchID='".IntegerSafe($batchID)."' ";
            $batchSecOrder = current($this->returnVector($sql));
            $sql="SELECT RecordID,iptFrm.BatchID,FillerID,ipss.AppRoleID,IF(SUM(CanBrowse)>0, 1, 0) as CanBrowse,IF(SUM(CanFill)>0,1,0) as CanFill
					FROM(
						SELECT RlsNo,RecordID,BatchID,FillerID,SecID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".IntegerSafe($rlsNo)."' AND RecordID='".IntegerSafe($recordID)."' AND BatchOrder <= '".IntegerSafe($batchSecOrder)."'
					) as iptFrm
					INNER JOIN(
						SELECT SecID,AppRoleID,CanBrowse,CanFill FROM INTRANET_PA_S_SECROL
					) as ipss ON iptFrm.SecID=ipss.SecID ";
            $sql .= " WHERE ipss.AppRoleID IN (".$appRole.") AND ipss.SecID='".$secID."' GROUP BY RecordID ";
            //echo $sql."<br/><br/>";
            $a=$this->returnResultSet($sql);
            if(isset($grpInfo) && !empty($grpInfo)){
                foreach($a as $k=>$v){
                    if($grpInfo['LeaderRole']==0){
                        $a[$k]['CanFill'] = "0";
                    }
                }
            }
            return $a;
        }
        function getFormSubmission($Lang,$userID,$name,$appRole){
            $PATH_WRT_ROOT = "../../../../";
            include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
            $indexVar['libappraisal_ui'] = new libappraisal_ui();
            
            $content .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($Lang['Appraisal']['ARFormSubmission'])."<br/>\r\n";
            $content .= "<div id=\"formSubmissionInfoLayer\">";
            $content .= "<table id=\"formSubmissionInfoTable\" class=\"form_table_v30\">";
            $content .= "<thead>";
            $content .= "<tr>";
            $content .= "</tr></thead>";
            $content .= "<tbody>";
            $content .= "<tr>";
            $content .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormFiller']."</td>";
            $content .= "<td>";
            $content .= $name;
            $content .= "</td>";
            $content .= "</tr>";
            /*$content .= "<tr>";
             $content .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormFillerRole']."</td>";
             $content .= "<td>";
             $content .= $appRole;
             $content .= "</td>";
             $content .= "</tr>";*/
            $content .= "<tr>";
            $content .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormFillerSignature']."</td>";
            $content .= "<td>";
            $content .= "<input type=\"password\" id=\"fake_signauture\" name=\"fake_signauture\" class=\"textboxtext\" style=\"display:none\">";
            $content .= "<input type=\"password\" id=\"signauture\" name=\"signauture\" class=\"textboxtext\">";
            $content .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('passwordErrorWarnDiv', $Lang['Appraisal']['ARFormInvalidSignature'], $Class='warnMsgDiv');
            $content .= "<input type=\"hidden\" id=\"fillerID\" name=\"fillerID\" value=\"".$userID."\">";
            $content .= $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSign'], "button", "goSignatureSubmit()", 'submitBtn');
            $content .= "</td>";
            $content .= "</tr>";
            $content .= "</tbody>";
            $content .= "</table></div><br/>";
            return $content;
        }
        function getModFormSubmission($Lang,$userID,$name,$appRole){
            $PATH_WRT_ROOT = "../../../../";
            include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
            $indexVar['libappraisal_ui'] = new libappraisal_ui();
            
            $content .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($Lang['Appraisal']['ARFormSubmission'])."<br/>\r\n";
            $content .= "<div id=\"formSubmissionInfoLayer\">";
            $content .= "<table id=\"formSubmissionInfoTable\" class=\"form_table_v30\">";
            $content .= "<thead>";
            $content .= "<tr>";
            $content .= "</tr></thead>";
            $content .= "<tbody>";
            $content .= "<tr>";
            $content .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormFiller']."</td>";
            $content .= "<td>";
            $content .= $name;
            $content .= "</td>";
            $content .= "</tr>";
            /*$content .= "<tr>";
             $content .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormFillerRole']."</td>";
             $content .= "<td>";
             $content .= $appRole;
             $content .= "</td>";
             $content .= "</tr>";*/
            $content .= "<tr>";
            $content .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormFillerSignature']."</td>";
            $content .= "<td>";
            $content .= "<input type=\"password\" id=\"fake_signauture\" name=\"fake_signauture\" class=\"textboxtext\" style=\"display:none\">";
            $content .= "<input type=\"password\" id=\"signauture\" name=\"signauture\" class=\"textboxtext\">";
            $content .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('passwordErrorWarnDiv', $Lang['Appraisal']['ARFormInvalidSignature'], $Class='warnMsgDiv');
            $content .= "<input type=\"hidden\" id=\"fillerID\" name=\"fillerID\" value=\"".$userID."\">";
            $content .= $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSign'], "button", "goModSignatureSubmit()", 'submitBtn');
            $content .= "</td>";
            $content .= "</tr>";
            $content .= "</tbody>";
            $content .= "</table></div><br/>";
            return $content;
        }
        function getObsFormSubmission($Lang,$obsID, $isTeacherFeedback=false){
            $PATH_WRT_ROOT = "../../../../";
            include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
            global $appraisalConfig, $UserID;
            $indexVar['libappraisal_ui'] = new libappraisal_ui();
            
            $obsIDArr=explode(",",$obsID);
            
            $content .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($Lang['Appraisal']['ARFormSubmission'])."<br/>\r\n";
            $content .= "<div id=\"formSubmissionInfoLayer\">";
            $content .= "<table id=\"formSubmissionInfoTable\" class=\"form_table_v30\">";
            $content .= "<thead>";
            $content .= "<tr>";
            $content .= "</tr></thead>";
            $content .= "<tbody>";
            if($isTeacherFeedback){
                $i = 0;
                $sql = "SELECT UserID,EnglishName,ChineseName,Gender
							FROM(
								SELECT UserID,EnglishName,ChineseName,Gender FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".IntegerSafe($_SESSION['UserID'])."'
							) as iu
							";
                //echo $sql."<br/>";
                $filler=$this->returnResultSet($sql);
                $content .= "<tr>";
                $content .= "<td class=\"field_title\">".$Lang['Appraisal']['CycleTemplate']['StfIdvName']."</td>";
                $content .= "<td>";
                $content .= Get_Lang_Selection($filler[0]["ChineseName"],$filler[0]["EnglishName"]);
                $content .= "</td>";
                $content .= "</tr>";
                $content .= "<tr>";
                $content .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormFillerSignature']."</td>";
                $content .= "<td>";
                $content .= "<input type=\"password\" id=\"fake_signauture\" name=\"fake_signauture\" class=\"textboxtext\" style=\"display:none\">";
                $content .= "<input type=\"password\" id=\"signauture_".$i."\" name=\"signauture_".$i."\" class=\"textboxtext\">";
                $content .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('passwordErrorWarnDiv_'.$i, $Lang['Appraisal']['ARFormInvalidSignature'], $Class='warnMsgDiv');
                $content .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('atLeastOneErrorWarnDiv', $Lang['Appraisal']['ObsFormAtLeastOne'], $Class='warnMsgDiv');
                $content .= "<input type=\"hidden\" id=\"fillerID_".$i."\" name=\"fillerID_".$i."\" value=\"".$filler[0]["UserID"]."\">";
                $content .= $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSign'], "button", "goSignatureSubmit()", 'submitBtn');
                $content .= "</td>";
                $content .= "</tr>";
            }else{
                for($i=0;$i<sizeof($obsIDArr);$i++){
                    $sql = "SELECT UserID,EnglishName,ChineseName,Gender
							FROM(
								SELECT UserID,EnglishName,ChineseName,Gender FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".IntegerSafe($obsIDArr[$i])."'
							) as iu
							";
                    //echo $sql."<br/>";
                    $filler=$this->returnResultSet($sql);
                    
                    $content .= "<tr>";
                    $content .= "<td class=\"field_title\">".$Lang['Appraisal']['ObsFormFiller']."</td>";
                    $content .= "<td>";
                    $content .= Get_Lang_Selection($filler[0]["ChineseName"],$filler[0]["EnglishName"]);
                    $content .= "</td>";
                    $content .= "</tr>";
                    if($UserID == $obsIDArr[$i]){
                        $content .= "<tr>";
                        $content .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormFillerSignature']."</td>";
                        $content .= "<td>";
                        $content .= "<input type=\"password\" id=\"fake_signauture\" name=\"fake_signauture\" class=\"textboxtext\" style=\"display:none\">";
                        $content .= "<input type=\"password\" id=\"signauture_".$i."\" name=\"signauture_".$i."\" class=\"textboxtext\">";
                        $content .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('passwordErrorWarnDiv_'.$i, $Lang['Appraisal']['ARFormInvalidSignature'], $Class='warnMsgDiv');
                        $content .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('atLeastOneErrorWarnDiv', $Lang['Appraisal']['ObsFormAtLeastOne'], $Class='warnMsgDiv');
                        $content .= "<input type=\"hidden\" id=\"fillerID_".$i."\" name=\"fillerID_".$i."\" value=\"".$filler[0]["UserID"]."\">";
                        $content .= $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSign'], "button", "goSignatureSubmit()", 'submitBtn');
                        $content .= "</td>";
                        $content .= "</tr>";
                    }else{
                        $content .= "<input type=\"hidden\" id=\"fake_signauture\" name=\"fake_signauture\" class=\"textboxtext\" style=\"display:none\" value=\"\">";
                        $content .= "<input type=\"hidden\" id=\"signauture_".$i."\" name=\"signauture_".$i."\" class=\"textboxtext\" value=\"\">";
                    }
                }
            }
            $content .= "</tbody>";
            $content .= "</table></div><br/>";
            return $content;
        }
        function getUserNameID($userID,$mode){
            global $appraisalConfig, $Lang;
            $name_field = $this->Get_Name_Field("USR.");
            $sql="SELECT CASE
					WHEN AUSR.UserLogin IS NOT NULL then CONCAT({$name_field}, ' (".$Lang['Appraisal']['Deleted'].")')
					ELSE {$name_field}
					END  as UserName,
					USR.UserLogin FROM ".$appraisalConfig['INTRANET_USER']." USR LEFT JOIN INTRANET_ARCHIVE_USER AUSR ON USR.UserID=AUSR.UserID WHERE USR.UserID='".IntegerSafe($userID)."';";
            $a=$this->returnResultSet($sql);
            if($mode=="nameID"){
                $name=$a[0]["UserName"]." (".$a[0]["UserLogin"].")";
            }
            else if($mode=="name"){
                $name=$a[0]["UserName"];
            }
            else if($mode=="login"){
                $name=$a[0]["UserLogin"];
            }
            return $name;
        }
        function getExemptTeacherList($displayName){
            global $appraisalConfig;
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='ExcludeAssessmentTeacher'";
            $exemptList = current($this->returnVector($sql));
            $exemptArr = explode(",",$exemptList);
            if($displayName==true){
                $exemptNameList = array();
                foreach($exemptArr as $uid){
                    if($uid<=0)continue;
                    array_push($exemptNameList, array("id"=>$uid, "name"=>$this->getUserNameID($uid, "name")));
                }
                return $exemptNameList;
            }else{
                return $exemptArr;
            }
        }
        
        function getExemptTeacherDisplay($mode="nameList"){
            $displayHTML = "";
            if($mode=="nameList"){
                $exemptNameList = $this->getExemptTeacherList(true);
                foreach($exemptNameList as $exemptTeacher){
                    $displayHTML .= $this->getUserNameID($exemptTeacher['id'], "name")."<br>";
                }
            }elseif($mode=="nameSelectionList"){
                $exemptNameList = $this->getExemptTeacherList(true);
                $idList = array();
                $nameList = array();
                foreach($exemptNameList as $exemptTeacher){
                    array_push($idList, $exemptTeacher['id']);
                    array_push($nameList, $this->getUserNameID($exemptTeacher['id'], "name"));
                }
                $displayHTML = getSelectByAssoArray($this->cnvArrToSelect($nameList,$idList), $selectionTags='id="exemptTeacherList" name="exemptTeacherList" multiple style="width:100%"', $SelectedType="", $all=0, $noFirst=1);
            }
            return $displayHTML;
        }
        
        function getPrintMcDisplayModeDisplay($mode="view"){
            global $intranet_session_language, $Lang, $sys_custom,$intranet_root,$appraisalConfig,$PATH_WRT_ROOT;
            include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
            $indexVar['libappraisal_ui'] = new libappraisal_ui();
            $displayHTML = "";
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='PrintMcDisplayMode'";
            $settingValue = current($this->returnVector($sql));
            if($mode=="view"){
                if($settingValue=="All"){
                    $displayHTML .= $Lang['Appraisal']['SettingsPrintMcDisplayModeAll'];
                }elseif($settingValue=="Checked"){
                    $displayHTML .= $Lang['Appraisal']['SettingsPrintMcDisplayModeChecked'];
                }
            }elseif($mode=="edit"){
                $displayHTML.=$indexVar['libappraisal_ui']->Get_Radio_Button("PrintMcDisplayModeAll","PrintMcDisplayMode", $Value="All", $isChecked=($settingValue=="All")?1:0, $Class="", $Display=$Lang['Appraisal']['SettingsPrintMcDisplayModeAll']);
                $displayHTML.=$indexVar['libappraisal_ui']->Get_Radio_Button("PrintMcDisplayModeChecked","PrintMcDisplayMode", $Value="Checked", $isChecked=($settingValue=="Checked")?1:0, $Class="", $Display=$Lang['Appraisal']['SettingsPrintMcDisplayModeChecked']);
            }
            return $displayHTML;
        }
        
        function getSchoolResultReportOverallSetup($cycleID=""){
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='SchoolResultReportOverallSetup'";
            $settingList = current($this->returnVector($sql));
            $settingArr = unserialize($settingList);
            if($cycleID!=""){
                if(isset($settingArr[$cycleID])){
                    return $settingArr[$cycleID];
                }else{
                    return "";
                }
            }else{
                return $settingArr;
            }
        }
        
        function setSchoolResultReportOverallSetup($cycleID, $templateIDList=array(), $sectionList=array()){
            global $PATH_WRT_ROOT;
            include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
            $lgeneralsettings = new libgeneralsettings();
            $settingArr = $this->getSchoolResultReportOverallSetup();
            $settingArr[$cycleID] = array(
                "templateList"=> $templateIDList,
                "sectionList"=>$sectionList
            );
            $settingList = serialize($settingArr);
            return $lgeneralsettings->Save_General_Setting("TeacherApprisal", array("SchoolResultReportOverallSetup"=>$settingList));
        }
        
        function getReportDisplayPersonList($displayName){
            global $appraisalConfig;
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='ReportDisplayPerson'";
            $exemptList = current($this->returnVector($sql));
            $exemptArr = explode(",",$exemptList);
            if($displayName==true){
                $exemptNameList = array();
                foreach($exemptArr as $uid){
                    if($uid<=0)continue;
                    array_push($exemptNameList, array("id"=>$uid, "name"=>$this->getUserNameID($uid, "name")));
                }
                return $exemptNameList;
            }else{
                return $exemptArr;
            }
        }
        
        function getReportDisplayPerson($mode="nameList"){
            $displayHTML = "";
            if($mode=="nameList"){
                $exemptNameList = $this->getReportDisplayPersonList(true);
                foreach($exemptNameList as $exemptTeacher){
                    $displayHTML .= $this->getUserNameID($exemptTeacher['id'], "name")."<br>";
                }
            }elseif($mode=="nameSelectionList"){
                $exemptNameList = $this->getReportDisplayPersonList(true);
                $idList = array();
                $nameList = array();
                foreach($exemptNameList as $exemptTeacher){
                    array_push($idList, $exemptTeacher['id']);
                    array_push($nameList, $this->getUserNameID($exemptTeacher['id'], "name"));
                }
                $displayHTML = getSelectByAssoArray($this->cnvArrToSelect($nameList,$idList), $selectionTags='id="reportDisplayPersonList" name="reportDisplayPersonList" multiple style="width:100%"', $SelectedType="", $all=0, $noFirst=1);
            }
            return $displayHTML;
        }
        
        function getReportDoubleCountPersonList($displayName){
            global $appraisalConfig;
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='ReportDoubleCountPerson'";
            $exemptList = current($this->returnVector($sql));
            $exemptArr = explode(",",$exemptList);
            if($displayName==true){
                $exemptNameList = array();
                foreach($exemptArr as $uid){
                    if($uid<=0)continue;
                    array_push($exemptNameList, array("id"=>$uid, "name"=>$this->getUserNameID($uid, "name")));
                }
                return $exemptNameList;
            }else{
                return $exemptArr;
            }
        }
        
        function getReportDoubleCountPerson($mode="nameList"){
            $displayHTML = "";
            if($mode=="nameList"){
                $exemptNameList = $this->getReportDoubleCountPersonList(true);
                foreach($exemptNameList as $exemptTeacher){
                    $displayHTML .= $this->getUserNameID($exemptTeacher['id'], "name")."<br>";
                }
            }elseif($mode=="nameSelectionList"){
                $exemptNameList = $this->getReportDoubleCountPersonList(true);
                $idList = array();
                $nameList = array();
                foreach($exemptNameList as $exemptTeacher){
                    array_push($idList, $exemptTeacher['id']);
                    array_push($nameList, $this->getUserNameID($exemptTeacher['id'], "name"));
                }
                $displayHTML = getSelectByAssoArray($this->cnvArrToSelect($nameList,$idList), $selectionTags='id="reportDoubleCountPersonList" name="reportDoubleCountPersonList" multiple style="width:100%"', $SelectedType="", $all=0, $noFirst=1);
            }
            return $displayHTML;
        }
        
        function getCPDSetting($mode="view"){
            global $appraisalConfig,$Lang;
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDSetting'";
            $cpdSettingList = unserialize(current($this->returnVector($sql)));
            $displayHTML = "";
            $cpdSettingList['Period'] = ($cpdSettingList['Period'])?$cpdSettingList['Period']:1;
            $cpdSettingList["SelectedYear"] = ($cpdSettingList['SelectedYear'])?$cpdSettingList['SelectedYear']:$_SESSION['CurrentSchoolYearID'];
            if($mode=="view"){
                $academicYearList=$this->getAllAcademicYear($cpdSettingList["SelectedYear"]);
                if ($_SESSION['intranet_session_language'] == 'en') {
                    $selectedYearName = $academicYearList[0]['YearNameEN'];
                }else{
                    $selectedYearName = $this->displayChinese($academicYearList[0]['YearNameB5']);
                }
                $displayHTML .= str_replace('%d',$cpdSettingList['Period'],$Lang['Appraisal']['SettingsCPDRecordsPeriod']);
                $displayHTML .= "<br>";
                $displayHTML .= $Lang['Appraisal']['SettingsCPDRecordsStarting']." ".$selectedYearName;
            }else{
                $academicYearList=$this->getAllAcademicYear("");
                $numSelField = ' <input type="number" name="cpdPeriod" value="'.$cpdSettingList['Period'].'" id="cpdPeriod" step=1 min=1> ';
                $displayHTML .= str_replace('%d',$numSelField,$Lang['Appraisal']['SettingsCPDRecordsPeriod']);
                $displayHTML .= "<br>";
                $displayHTML .= $Lang['Appraisal']['SettingsCPDRecordsStarting']." ".getSelectByAssoArray($this->cnvGeneralArrToSelect($academicYearList, "AcademicYearID","YearNameB5","YearNameEN"), $selectionTags='id="AcademicYearID" name="AcademicYearID"', $SelectedType=$cpdSettingList["SelectedYear"], $all=0, $noFirst=0);
            }
            return $displayHTML;
        }
        
        function getCPDDurationSetting($mode='view'){
            global $appraisalConfig,$Lang,$PATH_WRT_ROOT;
            include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
            $indexVar['libappraisal_ui'] = new libappraisal_ui();
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDDurationSetting'";
            $cpdDurationSettingList = current($this->returnVector($sql));
            if(empty($cpdDurationSettingList)){
                $cpdDurationSettingList = 4;
            }
            $nextMonth = $cpdDurationSettingList+1;
            if($cpdDurationSettingList==12){
                $nextMonth = 1;
            }
            $displayHTML = "";
            
            if($mode=="view"){
                $displayHTML .= "<table class=\"common_table_list\">";
                $displayHTML .= "	<tr><th colspan='2'>".$Lang['Appraisal']['SettingsCPDDurationRemarks']."</th></tr>";
                $displayHTML .= "	<tr class='columnRow' style='text-align:center;'>";
                $displayHTML .= "		<td style='width:50%'>".$Lang['Appraisal']['Month'][9]."  -  ".$Lang['Appraisal']['Month'][$cpdDurationSettingList]."</td>";
                $displayHTML .= "		<td style='width:50%'><span id='SecondPeriodStartMonth'>".$Lang['Appraisal']['Month'][$nextMonth]."</span>  -  ".$Lang['Appraisal']['Month'][8]."</td>";
                $displayHTML .= "	</tr>";
                $displayHTML .= "</table>";
            }else if($mode=="edit"){
                $periodEnd = getSelectByAssoArray($Lang['Appraisal']['Month'], $selectionTags='id="CPDDurationSetting" name="CPDDurationSetting"', $SelectedType=$cpdDurationSettingList, $all=0, $noFirst=0);
                $displayHTML .= "<table class=\"common_table_list\">";
                $displayHTML .= "	<tr><th colspan='2'>".$Lang['Appraisal']['SettingsCPDDurationRemarks']."</th></tr>";
                $displayHTML .= "	<tr class='columnRow' style='text-align:center;'>";
                $displayHTML .= "		<td style='width:50%'>".$Lang['Appraisal']['Month'][9]."  -  ".$periodEnd."</td>";
                $displayHTML .= "		<td style='width:50%'><span id='SecondPeriodStartMonth'>".$Lang['Appraisal']['Month'][$nextMonth]."</span>  -  ".$Lang['Appraisal']['Month'][8]."</td>";
                $displayHTML .= "	</tr>";
                $displayHTML .= "</table>";
            }
            return $displayHTML;
        }
        function getCPDShowingTotalHoursSetting($mode='view'){
            global $appraisalConfig,$Lang,$PATH_WRT_ROOT;
            include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
            $indexVar['libappraisal_ui'] = new libappraisal_ui();
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDShowingTotalHourSetting'";
            $cpdShowingTotalHour= current($this->returnVector($sql));
            if(empty($cpdShowingTotalHour)){
                $cpdShowingTotalHour = false;
            }
            $displayHTML = "";
            
            if($mode=="view"){
                $displayHTML .= ($cpdShowingTotalHour==true)?$Lang['Appraisal']['TemplateSample']['1']:$Lang['Appraisal']['TemplateSample']['0'];
            }else if($mode=="edit"){
                $displayHTML .= $indexVar['libappraisal_ui']->Get_Checkbox("CPDShowingTotalHourSetting", "CPDShowingTotalHourSetting", $Value=true, $isChecked=$cpdShowingTotalHour, $Class="", $Display="", $Onclick="",$isDisabled=0,$isIndented=0,$specialLabel="");
            }
            return $displayHTML;
        }
        
        function getAttendanceReasons($mode='view'){
            global $appraisalConfig,$Lang;
            $sql = "SELECT * FROM INTRANET_PA_S_ATTEDNACE_REASON";
            $reasonList = $this->returnArray($sql);
            $displayHTML = "";
            if($mode=="view"){
                $displayHTML .= "<table id='reasonTable' class='common_table_list'>";
                $displayHTML .= "<tr>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['ARFormLeaCat'].$Lang['Appraisal']['ARFormChiName']."</th>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['ARFormLeaCat'].$Lang['Appraisal']['ARFormEngName']."</th>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['ARFormAttendanceUnit']."</th>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['ARFormAttendanceListTotalAfter']."</th>";
                $displayHTML .= "</tr>";
                $idx = 0;
                foreach($reasonList as $idx=>$reason){
                    $displayHTML .= "<tr id='reasonRow_$idx' class='reasonRow'>";
                    $displayHTML .= "	<td>".$reason['TAReasonNameChi']."</td>";
                    $displayHTML .= "	<td>".$reason['TAReasonNameEng']."</td>";
                    $displayHTML .= "	<td>".$Lang['Appraisal']['ARFormAttendanceUnitList'][$reason['ReasonUnit']]."</td>";
                    $displayHTML .= "	<td>".(($reason['CountTotalBeforeThisReason']=="1")?$Lang['General']['Yes']:"")."</td>";
                    $displayHTML .= "</tr>";
                }
                $displayHTML .= "</table>";
            }elseif($mode=="edit"){
                $displayHTML .= "<table id='reasonTable' class='common_table_list'>";
                $displayHTML .= "<tr>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['ARFormLeaCat'].$Lang['Appraisal']['ARFormChiName']."</th>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['ARFormLeaCat'].$Lang['Appraisal']['ARFormEngName']."</th>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['ARFormAttendanceUnit']."</th>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['ARFormAttendanceListTotalAfter']."</th>";
                $displayHTML .= "	<th></th>";
                $displayHTML .= "</tr>";
                $idx = -1;
                $unitSelectionIdx = array();
                $unitSelectionDisplay = array();
                foreach($Lang['Appraisal']['ARFormAttendanceUnitList'] as $idx=>$displayWord){
                    array_push($unitSelectionIdx, $idx);
                    array_push($unitSelectionDisplay, $displayWord);
                }
                foreach($reasonList as $idx=>$reason){
                    $unitSelection = getSelectByAssoArray($this->cnvArrToSelect($unitSelectionDisplay,$unitSelectionIdx), $selectionTags='class="reason_unit" name="reason_unit[]"', $SelectedType=$reason['ReasonUnit'], $all=0, $noFirst=0);
                    $displayHTML .= "<tr id='reasonRow_$idx' class='reasonRow'>";
                    $displayHTML .= "	<td><input type='text' name='reason_chi[]' value='".$reason['TAReasonNameChi']."'></td>";
                    $displayHTML .= "	<td><input type='text' name='reason_eng[]' value='".$reason['TAReasonNameEng']."'></td>";
                    $displayHTML .= "	<td>";
                    $displayHTML .= $unitSelection;
                    $displayHTML .= "	</td>";
                    $displayHTML .= "	<td><input type='radio' name='reason_show_total_before' value='$idx' ".(($reason['CountTotalBeforeThisReason']=="1")?"checked":"")."></td>";
                    $displayHTML .= "	<td><span class='table_row_tool row_content_tool' style='left;'><span style='float:right;'><a href='javascript:void(0);' class='delete_dim' title='".$Lang['Btn']['Delete']."' onclick='deleteReason($idx);'></a></span></span><input type='hidden' name='is_delete[]' value=''><input type='hidden' name='reason_id[]' value='".$reason['TAReasonID']."'></td>";
                    $displayHTML .= "</tr>";
                }
                for($i=$idx+1; $i < 11+$idx; $i++){
                    $unitSelection = getSelectByAssoArray($this->cnvArrToSelect($unitSelectionDisplay,$unitSelectionIdx), $selectionTags='class="reason_unit" name="reason_unit[]"', $SelectedType="", $all=0, $noFirst=0);
                    $displayHTML .= "<tr id='reasonRow_$i' class='reasonRow' style='display:none;'>";
                    $displayHTML .= "	<td><input type='text' name='reason_chi[]' value=''></td>";
                    $displayHTML .= "	<td><input type='text' name='reason_eng[]' value=''></td>";
                    $displayHTML .= "	<td>";
                    $displayHTML .= $unitSelection;
                    $displayHTML .= "	</td>";
                    $displayHTML .= "	<td><input type='radio' name='reason_show_total_before' value='$i'></td>";
                    $displayHTML .= "	<td><span class='table_row_tool row_content_tool' style='left;'><span style='float:right;'><a href='javascript:void(0);' class='delete_dim' title='".$Lang['Btn']['Delete']."' onclick='deleteReason($i);'></a></span></span><input type='hidden' name='is_delete[]' value=''><input type='hidden' name='reason_id[]' value=''></td>";
                    $displayHTML .= "</tr>";
                }
                $displayHTML .= "<tr>";
                $displayHTML .= "	<td></td>";
                $displayHTML .= "	<td></td>";
                $displayHTML .= "	<td></td>";
                $displayHTML .= "	<td></td>";
                $displayHTML .= '	<td><div class="table_row_tool row_content_tool"><a href="javascript:void(0);" class="add_dim thickbox" title="新增" onclick="addReason(); return false;" id=""></a></div></td>';
                $displayHTML .= "</tr>";
                $displayHTML .= "</table>";
            }
            return $displayHTML;
        }
        function getAttendanceReasonMapping($mode='view'){
            global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
            if($mode=='edit'){
                $sql = "SELECT * FROM INTRANET_PA_S_ATTEDNACE_REASON";
                $reasonSet = $this->returnArray($sql);
                $reasonIdSet = array();
                $reasonNameSet = array();
                foreach($reasonSet as $reason){
                    array_push($reasonIdSet, $reason['TAReasonID']);
                    array_push($reasonNameSet, Get_Lang_Selection($reason['TAReasonNameChi'],$reason['TAReasonNameEng']));
                }
            }
            // Follow libstaffattend3_UI
            include_once($PATH_WRT_ROOT.'/includes/libstaffattend3.php');
            $CARD_STATUS_ABSENT_SUSPECTED = 7;
            $StaffAttend3 = new libstaffattend3();
            //$ReasonTypeList = $StaffAttend3->Get_Preset_Leave_Reason_Type_List($Keyword, $SortField, $InOrder);
            $ReasonTypeList = array();
            $ReasonTypeList[0] = array("TypeID" => CARD_STATUS_ABSENT,
                "ReasonTypeName" => $this->displayChinese($Lang['StaffAttendance']['Absent']));
            $ReasonTypeList[1] = array("TypeID" => CARD_STATUS_LATE,
                "ReasonTypeName" => $this->displayChinese($Lang['StaffAttendance']['Late']));
            $ReasonTypeList[2] = array("TypeID" => CARD_STATUS_EARLYLEAVE,
                "ReasonTypeName" => $this->displayChinese($Lang['StaffAttendance']['EarlyLeave']));
            $ReasonTypeList[3] = array("TypeID" => CARD_STATUS_HOLIDAY,
                "ReasonTypeName" => $this->displayChinese($Lang['StaffAttendance']['Holiday']));
            $ReasonTypeList[4] = array("TypeID" => CARD_STATUS_OUTGOING,
                "ReasonTypeName" => $this->displayChinese($Lang['StaffAttendance']['Outing']));
            
            //$StatusList = $StaffAttend3->Get_Attendance_Status_Settings();
            
            $x = '<table class="common_table_list">
				  <tbody>
				  	<tr>
	                  <th width="5%"><span class="row_content">'.$this->displayChinese($Lang['StaffAttendance']['ReasonType']).'</span>
	                      <div class="table_row_tool row_content_tool"></div>
	                  </th>
	                  <th  width="25%" class="sub_row_top"><span class="row_content">'.$this->displayChinese($Lang['StaffAttendance']['Reason']).'</span>
	                      <div class="table_row_tool row_content_tool"></div>
	                  </th>
	                  <th  width="35%" class="sub_row_top"><span class="row_content">'.$this->displayChinese($Lang['StaffAttendance']['Description']).'</span>
	                      <div class="table_row_tool row_content_tool"></div>
	                  </th>
	                  <th class="sub_row_top"><span class="row_content">'.$Lang['Appraisal']['ARFormLeaCat'].'</span>
	                      <div class="table_row_tool row_content_tool"></div>
	                  </th>
	                </tr>
				  </tbody>
				  <col/>
				  <col/>
				  <col/>
				  <tbody>';
            $reasonCountAry = $StaffAttend3->Get_Preset_Leave_Reason_Active_Count();
            for($i=0;$i<sizeof($ReasonTypeList);$i++)
            {
                $ReasonList = $StaffAttend3->Get_Preset_Leave_Reason_List($ReasonTypeList[$i]['TypeID']);
                $x .= '<tr>
	                    <td rowspan="'.(sizeof($ReasonList)+2).'">'.$ReasonTypeList[$i]['ReasonTypeName'].'</td>
	                    <td colspan="2">&nbsp;</td>
						<td>&nbsp;</td>
	                   </tr>';
                
                $sql = "SELECT ar.TAReasonID, ar.TAReasonNameChi, ar.TAReasonNameEng ,arm.Days, arm.MapID, ar.ReasonUnit FROM INTRANET_PA_S_ATTEDNACE_REASON_MAPPING arm INNER JOIN INTRANET_PA_S_ATTEDNACE_REASON ar ON arm.TAReasonID=ar.TAReasonID WHERE arm.ReasonID='0' AND arm.TypeID='".$ReasonTypeList[$i]['TypeID']."'";
                $taReason = $this->returnArray($sql);
                $taReasonName = Get_Lang_Selection($taReason[0]['TAReasonNameChi'],$taReason[0]['TAReasonNameEng']);
                // Selection Box
                if($mode == "edit"){
                    $taReasonSelection = getSelectByAssoArray($this->cnvArrToSelect($reasonNameSet,$reasonIdSet), $selectionTags='class="taReasonList" name="taReasonList[]"', $SelectedType=$taReason[0]['TAReasonID'], $all=0, $noFirst=0);
                    //$daysSelection = getSelectByAssoArray($this->cnvArrToSelect(array("0.5", "1"),array("0.5", "1")), $selectionTags='class="taReasonDaysList" name="taReasonDaysList[]"', $SelectedType=$taReason[0]['Days'], $all=0, $noFirst=0);
                    $daysSelection = '<input type="number" class="taReasonDaysList" name="taReasonDaysList[]" value="'.$taReason[0]['Days'].'" min="0" step="0.1">';
                }elseif($mode=="view"){
                    $taReasonSelection = $taReasonName;
                    $daysSelection = $taReason[0]['Days'];
                }
                
                $x .= '<tr class="sub_row">
		                    <td>'.$Lang['Appraisal']['Message']['AttendanceNoReason'].'</td>
		                    <td></td>
		                    <td>'.$taReasonSelection.'('.$daysSelection.' '.$Lang['Appraisal']['ARFormAttendanceUnitList'][$taReason[0]['ReasonUnit']].')<input type="hidden" name="reasonID[]" value="0"><input type="hidden" name="attendanceTypeID[]" value="'.$ReasonTypeList[$i]['TypeID'].'"><input type="hidden" name="mapID[]" value="'.$taReason[0]['MapID'].'"></td>
	                   	   </tr>';
                
                for($j=0;$j<sizeof($ReasonList);$j++)
                {
                    $sql = "SELECT ar.TAReasonID, ar.TAReasonNameChi, ar.TAReasonNameEng ,arm.Days, arm.MapID, ar.ReasonUnit FROM INTRANET_PA_S_ATTEDNACE_REASON_MAPPING arm INNER JOIN INTRANET_PA_S_ATTEDNACE_REASON ar ON arm.TAReasonID=ar.TAReasonID WHERE arm.ReasonID='".$ReasonList[$j]['ReasonID']."'";
                    $taReason = $this->returnArray($sql);
                    $taReasonName = Get_Lang_Selection($taReason[0]['TAReasonNameChi'],$taReason[0]['TAReasonNameEng']);
                    // Selection Box
                    if($mode == "edit"){
                        $taReasonSelection = getSelectByAssoArray($this->cnvArrToSelect($reasonNameSet,$reasonIdSet), $selectionTags='class="taReasonList" name="taReasonList[]"', $SelectedType=$taReason[0]['TAReasonID'], $all=0, $noFirst=0);
                        //$daysSelection = getSelectByAssoArray($this->cnvArrToSelect(array("0.5", "1"),array("0.5", "1")), $selectionTags='class="taReasonDaysList" name="taReasonDaysList[]"', $SelectedType=$taReason[0]['Days'], $all=0, $noFirst=0);
                        $daysSelection = '<input type="number" class="taReasonDaysList" name="taReasonDaysList[]" value="'.$taReason[0]['Days'].'" min="0" step="0.1">';
                    }elseif($mode=="view"){
                        $taReasonSelection = $taReasonName;
                        $daysSelection = $taReason[0]['Days'];
                    }
                    
                    $x .= '<tr class="sub_row">
		                    <td>'.intranet_htmlspecialchars($this->displayChinese($ReasonList[$j]['ReasonText']), true).'</td>
		                    <td>'.(trim($ReasonList[$j]['ReasonDescription'])==''?'&nbsp;':intranet_htmlspecialchars($this->displayChinese($ReasonList[$j]['ReasonDescription']), true)).'</td>
		                    <td>'.$taReasonSelection.'('.$daysSelection.' '.$Lang['Appraisal']['ARFormAttendanceUnitList'][$taReason[0]['ReasonUnit']].')<input type="hidden" name="reasonID[]" value="'.$ReasonList[$j]['ReasonID'].'"><input type="hidden" name="attendanceTypeID[]" value="'.$ReasonTypeList[$i]['TypeID'].'"><input type="hidden" name="mapID[]" value="'.$taReason[0]['MapID'].'"></td>
	                   	   </tr>';
                }
            }
            $x .='</tbody>
				  </table>';
            
            return $x;
        }
        function getAttendanceTableColumn($mode='view'){
            global $appraisalConfig,$Lang,$PATH_WRT_ROOT;
            include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
            $indexVar['libappraisal_ui'] = new libappraisal_ui();
            $sql = "SELECT * FROM INTRANET_PA_S_ATTEDNACE_COLUMN ORDER BY DisplayOrder ASC";
            $columnList = $this->returnArray($sql);
            $displayHTML = "";
            if($mode=="view"){
                $displayHTML .= "<table id='columnTable' class='common_table_list'>";
                $displayHTML .= "<tr>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['ARFormAttendanceColumn'] .$Lang['Appraisal']['ARFormChiName']."</th>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['ARFormAttendanceColumn'] .$Lang['Appraisal']['ARFormEngName']."</th>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['ARFormAttendanceColumnRelatedDate']."</th>";
                $displayHTML .= "</tr>";
                $idx = 0;
                foreach($columnList as $idx=>$col){
                    $defaultPeriodStart = (strtotime($col['PeriodStart']) < 0)?"0000-00-00":date('Y-m-d', strtotime($col['PeriodStart']));
                    $defaultPeriodEnd = (strtotime($col['PeriodEnd']) < 0)?"0000-00-00":date('Y-m-d', strtotime($col['PeriodEnd']));
                    $displayHTML .= "<tr id='columnRow_$idx' class='columnRow'>";
                    $displayHTML .= "	<td>".$col['TAColumnNameChi']."</td>";
                    $displayHTML .= "	<td>".$col['TAColumnNameEng']."</td>";
                    if(strtotime($col['PeriodStart']) < 0 && strtotime($col['PeriodEnd']) < 0){
                        $displayHTML .= "<td></td>";
                    }else{
                        $displayHTML .= "	<td>".$defaultPeriodStart." - ".$defaultPeriodEnd."</td>";
                    }
                    $displayHTML .= "</tr>";
                }
                $displayHTML .= "</table>";
            }elseif($mode=="edit"){
                $displayHTML .= "<table id='dndTable' class='common_table_list ClassDragAndDrop'>";
                $displayHTML .= "<thead><tr>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['ARFormAttendanceColumn'] .$Lang['Appraisal']['ARFormChiName']."</th>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['ARFormAttendanceColumn'] .$Lang['Appraisal']['ARFormEngName']."</th>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['ARFormAttendanceColumnRelatedDate']."</th>";
                $displayHTML .= "	<th>".$Lang['Appraisal']['TemplateSample']["DisplayOrder"]."</th>";
                $displayHTML .= "	<th></th>";
                $displayHTML .= "</tr></thead><tbody>";
                $idx = -1;
                foreach($columnList as $idx=>$col){
                    $defaultPeriodStart = (strtotime($col['PeriodStart']) < 0)?"":date('Y-m-d', strtotime($col['PeriodStart']));
                    $defaultPeriodEnd = (strtotime($col['PeriodEnd']) < 0)?"":date('Y-m-d', strtotime($col['PeriodEnd']));
                    $periodStart = $indexVar['libappraisal_ui']->GET_DATE_PICKER('periodStart_'.$idx,$defaultPeriodStart,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('periodStart[]')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
                    $periodEnd = $indexVar['libappraisal_ui']->GET_DATE_PICKER('periodEnd_'.$idx,$defaultPeriodEnd,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('periodEnd[]')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
                    $displayHTML .= "<tr id='columnRow_$idx' class='columnRow'>";
                    $displayHTML .= "	<td><input type='text' name='column_chi[]' value='".$col['TAColumnNameChi']."'></td>";
                    $displayHTML .= "	<td><input type='text' name='column_eng[]' value='".$col['TAColumnNameEng']."'></td>";
                    $displayHTML .= "	<td>".$periodStart."  -  ".$periodEnd."</td>";
                    $displayHTML .= "	<td class='Dragable'><div class='table_row_tool'><a href='#' class='move_order_dim' title='移動'></a></div></td>";
                    $displayHTML .= "	<td><span class='table_row_tool row_content_tool' style='left;'><span style='float:right;'><a href='javascript:void(0);' class='delete_dim' title='".$Lang['Btn']['Delete']."' onclick='deleteColumn($idx);'></a></span></span><input type='hidden' name='is_col_delete[]' value=''><input type='hidden' name='row_id[]' value='".$idx."'><input type='hidden' name='column_id[]' value='".$col['TAColumnID']."'></td>";
                    $displayHTML .= "</tr>";
                }
                for($i=$idx+1; $i < 11+$idx; $i++){
                    $periodStart = $indexVar['libappraisal_ui']->GET_DATE_PICKER('periodStart_'.$i,"",$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('periodStart')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
                    $periodEnd = $indexVar['libappraisal_ui']->GET_DATE_PICKER('periodEnd_'.$i,"",$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('periodEnd')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
                    $displayHTML .= "<tr id='columnRow_$i' class='columnRow' style='display:none;'>";
                    $displayHTML .= "	<td><input type='text' name='column_chi[]' value=''></td>";
                    $displayHTML .= "	<td><input type='text' name='column_eng[]' value=''></td>";
                    $displayHTML .= "	<td>".$periodStart." - ".$periodEnd."</td>";
                    $displayHTML .= "	<td class='Dragable'><div class='table_row_tool'><a href='#' class='move_order_dim' title='移動'></a></div></td>";
                    $displayHTML .= "	<td><span class='table_row_tool row_content_tool' style='left;'><span style='float:right;'><a href='javascript:void(0);' class='delete_dim' title='".$Lang['Btn']['Delete']."' onclick='deleteColumn($i);'></a></span></span><input type='hidden' name='is_col_delete[]' value=''><input type='hidden' name='row_id[]' value='".$i."'><input type='hidden' name='column_id[]' value=''></td>";
                    $displayHTML .= "</tr>";
                }
                $displayHTML .= "</tbody><tfoot><tr>";
                $displayHTML .= "	<td></td>";
                $displayHTML .= "	<td></td>";
                $displayHTML .= "	<td></td>";
                $displayHTML .= "	<td></td>";
                $displayHTML .= '	<td><div class="table_row_tool row_content_tool"><a href="javascript:void(0);" class="add_dim thickbox" title="新增" onclick="addColumn(); return false;" id=""></a></div></td>';
                $displayHTML .= "</tr></tfoot>";
                $displayHTML .= "</table>";
            }
            return $displayHTML;
        }
        function Get_Staff_Selection($ParName,$ParID,$ParIndividual=true,$ParIsMultiple=false,$ParSize=20,$ParSelectedValue="",$ParOthers="",$ParAllStaffOption=false,$ShowOptGroupLabel=true,$LoginIDAsValue=false,$Currentonly=false,$ParEmptyFirst=false){
            global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $appraisalConfig, $junior_mck, $sys_custom;
            
            $x .= '<select name="'.$ParName.'" id="'.$ParID.'" ';
            if($ParIsMultiple){
                $x .= ' multiple ';
                $ParSize = $ParSize>0?$ParSize:20;
                $x .= ' size="'.$ParSize.'" ';
            }
            $x .= $ParOthers.' >';
            if($ParEmptyFirst) $x .= '<option value="" >--'.$Lang['Appraisal']['CycleTemplate']['SelectedStaff'].'--</option>';
            if($ParAllStaffOption) $x .= '<option value="" >'.$Lang['StaffAttendance']['AllStaff'].'</option>';
            
            if($ParIndividual){
                //if($ShowOptGroupLabel) $x .= '<optgroup label="-'.$Lang['StaffAttendance']['Individuals'].'-">';
                $name_field = $this->Get_Name_Field("USR.");
                $ej_checking = "";
                if($junior_mck){
                    $name_field_deleted = $this->Get_Archive_Name_Field("AUR.");
                    if($appraisalConfig['INTRANET_USER']!="INTRANET_USER"){
                        $ej_checking = " INNER JOIN INTRANET_USER IU ON USR.UserID=IU.UserID "; // filter deleted user account
                    }
                }else{
                    $name_field_deleted = $this->Get_Archive_Name_Field("AUR.");
                }
                $typeList = "1";		# 1-teacher only
                /*
                 if($sys_custom['eAppraisal']['showCurrentTeacherOnly'] || $Currentonly==true){
                 $condSql = "";
                 if($Currentonly==true){
                 $condSql = " AND USR.RecordStatus=1 ";
                 }
                 $sql = "SELECT UserID,StaffName,ClassNumber,UserLogin,RecordStatus,EnglishName
                 FROM(
                 SELECT USR.UserID, $name_field as StaffName, ycu.ClassNumber, USR.UserLogin, USR.RecordStatus, USR.EnglishName
                 FROM ".$appraisalConfig['INTRANET_USER']." USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList) $condSql
                 GROUP BY USR.UserID
                 ) as a ORDER BY EnglishName ASC";
                 }else{
                 $sql = "SELECT UserID,StaffName,ClassNumber,UserLogin,RecordStatus,EnglishName
                 FROM(
                 SELECT USR.UserID, $name_field as StaffName, ycu.ClassNumber, USR.UserLogin, USR.RecordStatus, USR.EnglishName
                 FROM ".$appraisalConfig['INTRANET_USER']." USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList)
                 GROUP BY USR.UserID
                 UNION
                 SELECT AUR.UserID, $name_field_deleted as StaffName, ycu2.ClassNumber, AUR.UserLogin, AUR.RecordStatus, AUR.EnglishName
                 FROM INTRANET_ARCHIVE_USER AUR LEFT OUTER JOIN YEAR_CLASS_USER ycu2 ON (ycu2.UserID=AUR.UserID) WHERE AUR.RecordType IN ($typeList)
                 GROUP BY AUR.UserID
                 ) as a ORDER BY EnglishName ASC;
                 ";
                 }
                 */
                if($sys_custom['eAppraisal']['showArchieveTeacher']){
                    $sql = "SELECT UserID,StaffName,ClassNumber,UserLogin,RecordStatus,EnglishName
								FROM(
									SELECT USR.UserID, $name_field as StaffName, ycu.ClassNumber, USR.UserLogin, USR.RecordStatus, USR.EnglishName
									FROM ".$appraisalConfig['INTRANET_USER']." USR $ej_checking LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList)
									GROUP BY USR.UserID
									UNION
									SELECT AUR.UserID, $name_field_deleted as StaffName, ycu2.ClassNumber, AUR.UserLogin, AUR.RecordStatus, AUR.EnglishName
									FROM INTRANET_ARCHIVE_USER AUR LEFT OUTER JOIN YEAR_CLASS_USER ycu2 ON (ycu2.UserID=AUR.UserID) WHERE AUR.RecordType IN ($typeList)
									GROUP BY AUR.UserID
								) as a ORDER BY EnglishName ASC;
							";
                }else{
                    $condSql = "";
                    if($Currentonly==true){
                        $condSql = " AND USR.RecordStatus=1 ";
                    }
                    $sql = "SELECT UserID,StaffName,ClassNumber,UserLogin,RecordStatus,EnglishName
							FROM(
								SELECT USR.UserID, $name_field as StaffName, ycu.ClassNumber, USR.UserLogin, USR.RecordStatus, USR.EnglishName
								FROM ".$appraisalConfig['INTRANET_USER']." USR $ej_checking LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList) $condSql
								GROUP BY USR.UserID
							) as a ORDER BY EnglishName ASC";
                }
                $IndividualUserList = $this->returnArray($sql);
                for($i=0;$i<sizeof($IndividualUserList);$i++)
                {
                    if(is_array($ParSelectedValue)){
                        $selected = in_array($IndividualUserList[$i]['UserID'],$ParSelectedValue)?"selected":"";
                    }else{
                        $selected = $ParSelectedValue == $IndividualUserList[$i]['UserID']?"selected":"";
                    }
                    $value = $IndividualUserList[$i]['UserID'];
                    if($LoginIDAsValue){
                        $value = $IndividualUserList[$i]['UserLogin'];
                    }
                    $staffName = mb_convert_encoding($IndividualUserList[$i]['StaffName'], "UTF-8", array("UTF-8","BIG-5"));
                    $x .= '<option value="'.$value.'" '.$selected.'>'.intranet_htmlspecialchars($staffName,true).'</option>';
                }
            }
            $x .= '</select>';
            return $x;
        }
        function getAppRoleList(){
            $sql = "SELECT AppRoleID, ".$this->getLangSQL('NameChi','NameEng')." as RoleName FROM INTRANET_PA_S_APPROL WHERE IsActive=1 ORDER BY DisplayOrder ASC";
            $result = $this->returnArray($sql);
            return $result;
        }
        function getCycleSettingLog($filters=array()){
            global $appraisalConfig;
            if(isset($filters) && !empty($filters)){
                $conds = ' AND '.implode(" AND ", $filters);
            }
            $sql = "SELECT log.LogID, log.CycleID, log.Function, log.RecordType, log.RecordDetail, log.LogDate, ".$this->getLangSQL('u.ChineseName','u.EnglishName')." as LogName, log.LogBy FROM INTRANET_PA_C_SETTING_LOG log INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON log.LogBy=u.UserID WHERE 1 $conds ";
            $result = $this->returnArray($sql);
            return $result;
        }
        function addCycleSettingLog($data){
            global $appraisalConfig;
            $sql = "INSERT INTO INTRANET_PA_C_SETTING_LOG (CycleID, Function, RecordType, RecordDetail, LogBy, LogDate) VALUES ('".$data['CycleID']."','".$data['Function']."','".$data['RecordType']."','".$this->Get_Safe_Sql_Query($data['RecordDetail'])."','".IntegerSafe($_SESSION['UserID'])."',NOW());";
            $result = $this->db_db_query($sql);
            return $result;
        }
        function getFormFillingLog($filters=array()){
            global $appraisalConfig;
            if(isset($filters) && !empty($filters)){
                $conds = ' AND '.implode(" AND ", $filters);
            }
            $sql = "SELECT log.LogID, log.RlsNo, log.RecordID, log.BatchID, log.Function, log.RecordType, log.RecordDetail, log.LogDate, ".$this->getLangSQL('u.ChineseName','u.EnglishName')." as LogName, log.LogBy FROM INTRANET_PA_T_FILLING_LOG log INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON log.LogBy=u.UserID WHERE 1 $conds ";
            $result = $this->returnArray($sql);
            return $result;
        }
        function addFormFillingLog($data){
            global $appraisalConfig;
            $sql = "INSERT INTO INTRANET_PA_T_FILLING_LOG (RlsNo, RecordID, BatchID, Function, RecordType, RecordDetail, LogBy, LogDate) VALUES ('".$data['RlsNo']."','".$data['RecordID']."','".$data['BatchID']."','".$data['Function']."','".$data['RecordType']."','".$this->Get_Safe_Sql_Query($data['RecordDetail'])."','".IntegerSafe($_SESSION['UserID'])."',NOW());";
            
            $result = $this->db_db_query($sql);
            return $result;
        }
        // -------------------------------------- SQL Function -------------------------------------- //
        // -------------------------------------- Program Function -------------------------------------- //
        function convertMultipleRowsIntoOneRow($arr,$fieldName){
            $x = "";
            for($i=0; $i<sizeof($arr);$i++){
                if($i==sizeof($arr)-1){
                    $x .= $arr[$i][$fieldName];
                }
                else{
                    $x .= $arr[$i][$fieldName].",";
                }
            }
            if(sizeof($arr)==0){
                $x = "''";
            }
            return $x;
        }
        function getLangSQL($ChineseName,$EnglishName){
            if ($_SESSION['intranet_session_language'] == 'en') {
                $sql = "TRIM(CONCAT(IF(".$EnglishName." IS NULL OR TRIM(".$EnglishName.") = '',".$ChineseName.",".$EnglishName.")))";
            }
            else {
                $sql = "TRIM(CONCAT(IF(".$ChineseName." IS NULL OR TRIM(".$ChineseName.") = '',".$EnglishName.",".$ChineseName.")))";
            }
            return $sql;
        }
        function cnvGeneralArrToSelect($arr,$type,$ChineseName,$EnglishName){
            $oAry = array();
            for($_i=0; $_i<sizeof($arr); $_i++){
                $oAry[$arr[$_i][$type]] = Get_Lang_Selection($arr[$_i][$ChineseName],$arr[$_i][$EnglishName]);
            }
            return $oAry;;
        }
        function cnvArrToSelect($arrDes,$arrVal){
            $oAry = array();
            if($arrVal == null){
                for($_i=0; $_i<sizeof($arrDes); $_i++){
                    $oAry[$_i] = $arrDes[$_i];
                }
            }
            else{
                for($_i=0; $_i<sizeof($arrDes); $_i++){
                    $oAry[$arrVal[$_i]] = $arrDes[$_i];
                }
            }
            return $oAry;
        }
        
        function isEJ(){
            global $junior_mck;
            return $junior_mck;
        }
        
        function displayChinese($chinese, $direction=1){
            if($this->isEJ()){
                $encode = mb_detect_encoding($chinese, 'BIG5,UTF-8');
                if($direction==1){
                    if($encode=="UTF-8"){
                        return $chinese;
                    }else{
                        $result = convert2unicode($chinese,1,$direction);
                        if($result==""){
                            return $chinese;
                        }else{
                            return $result;
                        }
                    }
                }else{
                    if($encode=="BIG5"){
                        return $chinese;
                    }else{
                        $result = convert2unicode($chinese,1,$direction);
                        if($result==""){
                            return $chinese;
                        }else{
                            return $result;
                        }
                    }
                }
            }else{
                return $chinese;
            }
        }
        
        function hasAppraisal($userID) {
            /** for check with $userID later **/
            $sql = "SELECT COUNT(CycleID) as Total FROM INTRANET_PA_C_CYCLE WHERE NOW() BETWEEN CycleStart AND CycleClose";
            $result = $this->returnResultSet($sql);
            if ($result != null && count($result) > 0) {
                if ($result["0"]["Total"] > 0) {
                    return true;
                }
            }
            return false;
        }
        
        function isAdminUser() {
            return $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAppraisal"]? true : false;
        }
        
        function getAdminUser(){
            if($this->isEJ()){
                global $intranet_root;
                include_once($intranet_root."/includes/libfilesystem.php");
                $lf = new libfilesystem();
                $AdminUser = trim($lf->file_read($intranet_root."/file/eAppraisal/admin_user.txt"));
                $AdminArray = explode(",", $AdminUser);
                return $AdminArray;
            }else{
                return array();
            }
        }
        
        function getQuestionDataByCmpArr($dbResult){
            if(count($dbResult) > 1){
                $diff = array();
                for($k=1; $k < count($dbResult); $k++){
                    $cmp = array_diff($dbResult[$k], $dbResult[$k-1]);
                    if(count($cmp)==1 && isset($cmp['BatchID'])){
                        continue;
                    }
                    unset($cmp['BatchID']);
                    foreach($cmp as $key=>$value){
                        if($value != ""){
                            $diff[$key] = $value;
                        }
                    }
                }
                foreach($diff as $key=>$value){
                    $dbResult[0][$key] = $value;
                }
            }
            return $dbResult;
        }
        
        function Get_Name_Field($prefix="", $isTitleDisabled=false){
            global $Lang;
            if($this->isEJ()){
                $name_field = getNameFieldByLang($prefix, $isTitleDisabled);
                $display_status = convert2unicode($Lang['Status']['Suspended']);
            }else{
                $name_field = getNameFieldByLang($prefix, "", $isTitleDisabled);
                $display_status = $Lang['Status']['Suspended'];
            }
            $x = "CONCAT(".$name_field.",IF(".$prefix."RecordStatus<>1,'[".$display_status."]',''))";
            return $x;
        }
        
        function Get_Archive_Name_Field($prefix=""){
            global $Lang;
            $name_field = getNameFieldByLang2($prefix);
            if($this->isEJ()){
                $display_status = convert2unicode($Lang['Status']['Left']);
            }else{
                $display_status = $Lang['Status']['Left'];
            }
            $x = "CONCAT(".$name_field.",'[".$display_status."]')";
            return $x;
        }
        // -------------------------------------- Program Function -------------------------------------- //
    }
}
?>