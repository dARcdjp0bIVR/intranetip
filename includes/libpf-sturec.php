<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * 2020-11-05 (Philips) [2020-1030-0922-03073]
 * Modified getAssessmentClassHistoryArr(), use YearTerm StartDate instead of Semester name to avoid chinese term name
 * 2020-06-29 (Bill)    [2020-0612-1004-37207]
 * Modified buildInsertTempAssessmentTableSQL(), show score if Score > 0 + No Grade + No Ranking   ($sys_custom['ipf']['Mgmt']['SchoolRecordDisplayScore_EvenWithoutGradeAndRanking'])
 *
 * 2019-09-30 (Philips) [2019-0218-1220-15066]
 * Modified buildInsertTempAssessmentTableSQL(), allow Empty Grade Field
 * 
 * 2019-07-19 (Anna)
 * Modified returnStandardScore, revise sql divided into 3 sqls
 * 
 * 2018-08-06 (Bill) [2017-0901-1527-45265]
 * Modified buildInsertTempAssessmentTableSQL_SD(), to add parm to hide parent subject SD score
 * 
 * 2017-11-27 (Ivan) [J131143] [ip.2.5.9.1.1]
 * Modified getStudentAcademicResultDisplayWithAssessment() to add sorting within assessment
 * 
 * 2017-11-24 (Ivan) [X131742] [ip.2.5.9.1.1]
 * Modified generateAssessmentReport3() to support $sys_custom['iPf']['showAcademicResultWithAssessment'] for viewing alumni academic result
 * 
 * 2017-07-12 (Paul)
 * Added function returnActivityAttendanceSummary(), displayStudentActivityAttendanceSummary() for NCS project. Displaying Enrollment Attendance in SmartCard Attendance Format
 * 
 * 2017-01-20 (Villa) #P112036 
 * Modified GET_ALUMNI_LIST_DATA case 3-> ordering by English Name Only
 * 
 * 2016-10-27 (Ivan) [ip.2.5.8.1.1]
 * Modified getStudentAcademicResultDisplayWithAssessment() to add flag $sys_custom['iPf']['showAcademicResultOrderByAcademicYearDesc'] to show academic result by desc order of academic year
 * 
 * 2016-08-19 (Ivan) [ip.2.5.7.10.1]
 * Added getStudentAcademicResultDisplayWithAssessment(), getStudentSubjectAcademicResult(), getStudentMainAcademicResult(), getStudentClassHistory()
 * Modified returnStandardScore()
 * - if enabled flag $sys_custom['iPf']['showAcademicResultWithAssessment'], show academic result table with assessment result
 * 
 *  2016-05-05 (Omas)
 * Modified buildInsertTempAssessmentTableSQL_SD(), buildInsertTempAssessmentTableSQL(), returnAssessmentResult() - if no record is "TermAssessment is null" display one of the TermAssessment
 * 
 * 2016-04-19 (Omas)
 * Modified buildInsertTempAssessmentTableSQL_SD(), buildInsertTempAssessmentTableSQL(), returnAssessmentResult() - add condition TermAssessment is null
 * 
 * 2015-02-24 (Pun)
 * Modified createTempAssessmentSubjectTable() added with parent name variable
 * 
 * 2015-01-13 (Pun)
 * Fix generateAssessmentReport3() for Alumni report Academic Report blank data
 * 
 * 2014-11-12 (Pun)
 * modified GEN_ALUMNI_CLASS_SELECTION() to show only one 'No Class' <option>
 * 
 * 2014-02-19 (Ivan) [2014-0217-1046-45184]
 * modified GEN_CLASS_HISTORY_TABLE() to retrieve class history data from class history table only
 * 
 * 2013-08-27 (Ivan)
 * modified: DO_ACTIVATE_STUDENT() added KIS skip license checking logic
 * 
 * 2013-03-25 (Ivan) [2013-0325-0956-31156]
 * modified: getAssessmentClassHistoryArr() to merge YearTermID "null" and "0" data
 * 
 * 2013-03-15 (YatWoon)
 * modified returnAssessmentMainRecord(), retreive YearTermID
 * modified generateAssessmentReport3(), fixed Full report display "black" case [Case#2013-0314-1107-19073]
 * modified: returnAssessmentResult(), retrieve YearTermID
 *
 * 2012-08-13 (Bill)
 * modified DO_ACTIVATE_STUDENT() and DO_REACTIVATE_STUDENT() join tables with INTRANET_USER_PERSONAL_SETTINGS to get Nationality, PlaceOfBirth and AdmissionDate
 * 
 * 2012-02-23 (Ivan) [2012-0223-1635-28132]
 * modified returnAllSubjectWithComponents() fixed null and empty string of subject code will return duplicated records
 * modified checkSubjectScoreExist() added para $StudentID to check if score exist for a specific student
 * 
 * 2011-12-16 (Ivan) [2011-1215-1627-48073]
 * modified createTempAssessmentSubjectTable() - get active subject only now
 * 
 * 2011-11-21 (Ivan) [2011-1118-1252-58073] Point 1
 * modified buildInsertTempAssessmentTableSQL() - ignore the records which Form ranking is "0" 
 * 
 * 2011-07-15 (Ivan) [2011-0715-0957-26073]
 * modified buildInsertTempAssessmentTableSQL() - include the subject even if there subject has no ranking
 * 
 * 2011-06-03 (Ivan) [2011-0530-0857-20128] Point 2
 * modified buildInsertTempAssessmentTableSQL() - ignore the records which Form ranking is "-1" 
 * 
 * 2011-05-24 (Ivan) [2011-0509-1159-40071]
 * modified DO_ACTIVATE_STUDENT() - fixed wrong license checking -> re-activate student should not be considered in the quota checking
 * 
 * 2011-05-11 (Ivan) [2011-0511-1217-45067]
 * modified createTempAssessmentMarkTable(), createTempAssessmentSDScoreTable()
 * fixed: Academic Result failed to show Subject which has Chinese ShortName since the temp DB Table missed to set default encoding to UTF-8 
 * 
 * 2011-03-02 (Ivan) [2011-0302-1053-43067]
 * modified function checkSubjectScoreExist - fixed wrong determination of score exist
 * *******************************************
 */
include_once 'libportfolio.php';
include_once 'libportfolio2007a.php';

class libpf_sturec extends libportfolio2007
{
	# Count the number of learning portfolio newly published
	function GET_NUMBER_LP_NEW_PUBLISHED($ParYearID="", $ParUserID=""){
		global $eclass_db, $intranet_db;

		# Translate User ID of TEACHER to eclass User ID
		$CourseUserID = $this->IP_USER_ID_TO_EC_USER_ID($ParUserID);

		# If class level is specified, add condition
		$extra_cond = ($ParYearID != "") ? "AND yc.YearID = '".$ParYearID."'" : "";

		# Get data from db
		$sql =	"
							SELECT
								iu.ClassName,
								if(ptl.portfolio_tracking_id IS NOT NULL AND (UNIX_TIMESTAMP(ptl.student_publish_time)>UNIX_TIMESTAMP(ptl.teacher_view_time) OR ptl.teacher_view_time IS NULL), 'new', '') AS Status
							FROM
								{$intranet_db}.INTRANET_USER AS iu
							INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
								ON ycu.UserID = iu.UserID
							INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
								ON ycu.YearClassID = yc.YearClassID
							LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
                ON ps.UserID = iu.UserID
							LEFT JOIN ".$this->course_db.".portfolio_tracking_last AS ptl
                ON ptl.student_id = ps.CourseUserID AND
								(ptl.teacher_id = '".$CourseUserID."' OR ptl.portfolio_tracking_id IS NULL)
							WHERE
								iu.RecordType = '2' AND
								iu.RecordStatus = '1' AND
								ps.WebSAMSRegNo IS NOT NULL AND
								ps.UserKey IS NOT NULL AND
								ps.IsSuspend = 0 AND
								yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
								{$extra_cond}
						";
		$row = $this->returnArray($sql);

		# Organize data in an associative array
		for ($i=0; $i<sizeof($row); $i++)
		{
			if($row[$i]["Status"] == "new")
				$ReturnArr[$row[$i]["ClassName"]]++;
		}

		return $ReturnArr;
	}
	
	# Get number of updates of school records
	function GET_NUMBER_UPDATED_SCHOOL_RECORD($ParCompareTime="")
	{
		global $eclass_db, $intranet_db;

		$CompareTime = ($ParCompareTime == "") ? "NOW()" : "'".$ParCompareTime."'";

		$SRModuleArr = array(
													"MERIT_STUDENT",
													"ASSESSMENT_STUDENT_SUBJECT_RECORD",
													"ACTIVITY_STUDENT",
													"AWARD_STUDENT",
													"CONDUCT_STUDENT",
													"ATTENDANCE_STUDENT",
													"SERVICE_STUDENT",
													"PORTFOLIO_STUDENT",
													"GUARDIAN_STUDENT"
												);

		for($i=0; $i<count($SRModuleArr); $i++)
		{
			$sql =	"
								SELECT
									b.ClassName,
									count(a.UserID) as Count
								FROM
									{$eclass_db}.".$SRModuleArr[$i]." as a
								INNER JOIN
									{$intranet_db}.INTRANET_USER as b
								ON
									a.UserID = b.UserID
								WHERE
									a.ModifiedDate > '".$CompareTime."'
								GROUP BY
									b.ClassName
							";
			$UpdatedArr = $this->returnArray($sql);

			for($j=0; $j<count($UpdatedArr); $j++)
			{
				$ReturnArr[$UpdatedArr[$j]['ClassName']] += $UpdatedArr[$j]['Count'];
			}
		}

		return $ReturnArr;
	}
	
	function GEN_STUDENT_INFO_TABLE($ParUserID, $ParStuObj)
	{
		global $ec_iPortfolio, $ec_student_word, $profile_dob, $profile_gender, $profile_nationality, $profile_pob;

		if($ParStuObj['DateOfBirth'] == "0000-00-00")
			$stdDOB = "--";
		else
			$stdDOB = $ParStuObj['DateOfBirth'];
/*		
	<tr>
		<td height='20' class='style16'>&nbsp;</td>
		<td class='chi_content_12' nowrap='nowrap'>".$profile_pob."</td>
		<td class='chi_content_12'>:</td>
		<td class='chi_content_12' nowrap='nowrap'>".$ParStuObj['PlaceOfBirth']."</td>
		<td width='10' height='20' class='style16'>&nbsp;</td>
	</tr>
*/
		# Place of birth is missing in design
		$ReturnStr =	"
										<table width=\"100%\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">
											<tr>
												<td class=\"form_field_name\">".$ec_student_word['name_english']."</td>
												<td class=\"form_field_content\">".$ParStuObj["EnglishName"]."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$ec_student_word['name_chinese']."</td>
												<td class=\"form_field_content\">".$ParStuObj['ChineseName']."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$profile_gender."</td>
												<td class=\"form_field_content\">".$ParStuObj['Gender']."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$profile_dob."</td>
												<td class=\"form_field_content\">".$stdDOB."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$ec_student_word['registration_no']."</td>
												<td class=\"form_field_content\">".$ParStuObj['WebSAMSRegNo']."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$profile_nationality."</td>
												<td class=\"form_field_content\">".$ParStuObj['Nationality']."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$ec_iPortfolio['place_of_birth']."</td>
												<td class=\"form_field_content\">".$ParStuObj['PlaceOfBirth']."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$ec_iPortfolio['phone']."</td>
												<td class=\"form_field_content\">".($ParStuObj['HomeTelNo']==""?"--":$ParStuObj['HomeTelNo'])."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$ec_iPortfolio['address']."</td>
												<td class=\"form_field_content\">".($ParStuObj['Address']==""?"--":$ParStuObj['Address'])."</td>
											</tr>
										</table>
									";

		return $ReturnStr;
	}
	
	# Generate class history table for display
	function GEN_CLASS_HISTORY_TABLE($ParUserID, $ParYear="", $isAlumni=false)
	{
		global $no_record_msg, $ec_iPortfolio;

		# Get student class history
		$class_history = $this->GET_STUDENT_CLASS_HISTORY($ParUserID, $ParYear, $ParIncludeAcademicResult=false, $isAlumni);
		
		# Heading
		$ReturnStr =	"
										<table width=\"100%\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">
											<tr>
												<td class=\"form_field_name\">".$ec_iPortfolio['year']."</td>
												<td class=\"form_field_name\">".$ec_iPortfolio['class']."</td>
												<td class=\"form_field_name\">".$ec_iPortfolio['number']."</td>
											</tr>
									";

		if(sizeof($class_history)>0)
		{
			for($i=0; $i<sizeof($class_history); $i++)
			{
				$ReturnStr .=	"
												<tr>
													<td class=\"form_field_content\" nowrap>".$class_history[$i]['Year']."</td>
													<td class=\"form_field_content\" nowrap>".$class_history[$i]['ClassName']."</td>
													<td class=\"form_field_content\" nowrap>".$class_history[$i]['ClassNumber']."</td>
												</tr>
											";
			}
		}
		else
		{
			$ReturnStr .=	"<tr height=60><td colspan=\"3\" align=\"center\" class=\"form_field_content\">&nbsp;$no_record_msg</td></tr>";
		}
		$ReturnStr .=	"</table>";

		return $ReturnStr;
	}
	
	# Generate school house and admission date table for display
	function GEN_STUDENT_HOUSE_TABLE($ParUserID, $ParStuObj)
	{
		global $ec_iPortfolio;
		global $sys_custom;
		
		# Get student admission date
		$admission_date = $this->GET_STUDENT_ADMISSION_DATE($ParUserID);
		$admission_date = ($admission_date=="") ? '--' : $admission_date;
		
		# Customarization : hide school house
		if($sys_custom['student_info_table_no_house'])
			$houseStyle = "style='display:none'";
		else
			$houseStyle = "";

		$ReturnStr =	"
										<table width=\"100%\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">
											<tr ".$houseStyle.">
												<td class=\"form_field_name\">".$ec_iPortfolio['house']."</td>
												<td class=\"form_field_content\">".($ParStuObj['House']==''?'--':$ParStuObj['House'])."</td>
											</tr>
											<tr>
												<td class=\"form_field_name\">".$ec_iPortfolio['admission_date']."</td>
												<td class=\"form_field_content\">".$admission_date."</td>
											</tr>
										</table>
									";

		return $ReturnStr;
	}
	
	# Generate parent information table for display
	function GEN_PARENT_INFO_TABLE($ParUserID)
	{
		global $ec_student_word, $ec_iPortfolio, $no_record_msg;

		# Get parent information of a student
		$parent_obj = $this->GET_PARENT_OBJECT($ParUserID);

		$ReturnStr = "";
		$ReturnStr .= "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";

		if(sizeof($parent_obj) > 0)
		{
			for($i=0; $i<sizeof($parent_obj); $i++)
			{
				# Display two guardians per row
				if($i % 2 == 0)
					$ReturnStr .=	"<tr>\n";
				
				$p_obj = $parent_obj[$i];
	
				$ReturnStr .=	"
												<td width='50%'>
													<table width='100%' border='0' cellspacing='6' cellpadding='0'>
											";
				# Highlight "Main guardian"
				$ReturnStr .= ($p_obj["IsMain"]==1) ?	"
																								<tr>
																									<td><span class=\"sub_page_title\">(".$ec_iPortfolio['main_guardian'].")</span></td>
																								</tr>
																							" : "&nbsp;";
				$ReturnStr .=	"
													<tr>
														<td class=\"form_field_name\">".$ec_student_word['name_english']."</td>
														<td class=\"form_field_content\">".$p_obj["EnName"]."</td>
													</tr>
													<tr>
														<td class=\"form_field_name\">".$ec_student_word['name_chinese']."</td>
														<td class=\"form_field_content\">".$p_obj["ChName"]."</td>
													</tr>
													<tr>
														<td class=\"form_field_name\">".$ec_iPortfolio['relation']."</td>
														<td class=\"form_field_content\">".$p_obj["Relation"]."</td>
													</tr>
													<tr>
														<td class=\"form_field_name\">".$ec_iPortfolio['phone']."</td>
														<td class=\"form_field_content\">".($p_obj["Phone"]==""?"--":$p_obj["Phone"])."</td>
													</tr>
													<tr>
														<td class=\"form_field_name\">".$ec_iPortfolio['em_phone']."</td>
														<td class=\"form_field_content\">".($p_obj["EmPhone"]==""?"--":$p_obj["EmPhone"])."</td>
													</tr>
												</table>
											</td>
										";
				
				if($i % 2 == 1)
					$ReturnStr .=	"</tr>\n";
				else if(($i+1)==sizeof($parent_obj))
					$ReturnStr .=	"
														<td>&nbsp;</td>\n
													</tr>\n
												";
			}
		}
		else
		{
			$ReturnStr .=	"
											<tr>
												<td>
													<table width='100%' border='0' cellspacing='0' cellpadding='0' height='100'>
														<tr height=60>
															<td colspan=3 align=center class=\"tabletext\">&nbsp;$no_record_msg</td>
														</tr>
													</table>
												</td>
											</tr>
										";
		}
		$ReturnStr .= "</table>";

		return $ReturnStr;
	}
	
	# Get status of learning portfolio
	function GET_LEARNING_PORTFOLIO_STATUS($ParClassName, $ParSearchName="", $ParYearID="")
	{
		global $eclass_db, $intranet_db, $eclass_prefix;

		//$ClassNumField = getClassNumberField("iu.");
		// TABLE SQL
		$fieldname .= "iu.UserID, ";
		$fieldname .= "if(ptl.portfolio_tracking_id IS NOT NULL AND (UNIX_TIMESTAMP(ptl.student_publish_time)>UNIX_TIMESTAMP(ptl.teacher_view_time) OR ptl.teacher_view_time IS NULL), 'new', '') as Status ";

/*
		if($TargetStatus!="")
		{
			# 1 : Learning portfolio is published and teacher has not viewed
			# 2 : Learning portfolio is published and teacher has viewed
			# 3 : All learning portfolio (including published and not published)
			switch ($TargetStatus)
			{
				case 1:
					$conds = " AND ptl.portfolio_tracking_id IS NOT NULL AND (UNIX_TIMESTAMP(ptl.student_publish_time)>UNIX_TIMESTAMP(ptl.teacher_view_time) OR ptl.teacher_view_time IS NULL) ";
					break;
				case 2:
					$conds = " AND ptl.portfolio_tracking_id IS NOT NULL AND ptl.teacher_view_time IS NOT NULL AND UNIX_TIMESTAMP(ptl.student_publish_time)<=UNIX_TIMESTAMP(ptl.teacher_view_time)";
					break;
				case 3:
					$conds = " AND ptl.portfolio_tracking_id IS NULL ";
					break;
			}
		}
*/
		# Add searching criteria
		if($ParSearchName != "")
		{
			$conds .=	"
									AND
										(iu.ChineseName LIKE '%".addslashes($ParSearchName)."%' OR
										iu.EnglishName LIKE '%".addslashes($ParSearchName)."%')
								";
		}
		
		$conds .=	($ParYearID != "") ? " AND yc.YearID = '".$ParYearID."'" : "";
		
		if($ParClassName != "")
			$conds .=	" AND iu.ClassName = '".$ParClassName."'";
		# Limit to display classes taught by the user
		else if(!$this->IS_IPF_ADMIN())
		{
			$conds .=	"
									AND
										iu.ClassName IN ('".implode("','", array_merge($this->GET_CLASS_TEACHER_CLASS(), $this->GET_SUBJECT_TEACHER_CLASS()))."')
								";
		}

		$sql =	"
							SELECT
								$fieldname
							FROM
								{$intranet_db}.INTRANET_USER AS iu
							INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
  							ON iu.UserID = ycu.UserID
							INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
  							ON ycu.YearClassID = yc.YearClassID
							LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
                ON ps.UserID=iu.UserID
							LEFT JOIN ".$this->course_db.".portfolio_tracking_last AS ptl
                ON
  								ptl.student_id=ps.CourseUserID AND
  								(ptl.teacher_id='".$this->IP_USER_ID_TO_EC_USER_ID($_SESSION['UserID'])."' OR ptl.portfolio_tracking_id IS NULL)
							WHERE
								iu.RecordType='2' AND
								iu.RecordStatus = '1' AND
								ps.WebSAMSRegNo IS NOT NULL AND
								ps.UserKey IS NOT NULL AND
								ps.IsSuspend = 0 AND
								yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
								".$conds."
							ORDER BY
								iu.ClassNumber
						";

		$row = $this->returnArray($sql);

		# Reorganize data to an associative array
		for($i=0; $i<count($row); $i++)
		{
			$ReturnArr[$row[$i]['UserID']] = $row[$i]['Status'];
		}

		return $ReturnArr;
	}
	
	# Load keys
	function LOAD_KEYS_FROM_SETTING(){
		global $eclass_db;

		$keys = $this->GET_KEY_LICENSE($this->file_path."/keys.dat");

		# select last key used
		$sql =	"
							SELECT
								UserKey
							FROM
								{$eclass_db}.PORTFOLIO_STUDENT
							ORDER BY
								RecordID DESC
							LIMIT
								1
						";
		$row = $this->returnArray($sql);
		$key_last = $row[0]["UserKey"];

		$key_total_used = 0;
		$key_total = 0;
		$keys_available = array();

		# determine the usage of keys
		if (sizeof($keys['records'])>0)
		{
			foreach ($keys['records'] as $key_date => $record)
			{
				for ($i=0; $i<sizeof($record); $i++)
				{
					$key_total ++;
					# record keys not in used
					if ($key_total_used>0 || $key_last=="")
					{
						$keys_available[] = $record[$i];
					}
					if ($key_last==$record[$i])
					{
						$key_total_used = $key_total;
					}
				}
			}
		}

		# update the global object
		$this->quota["total"] = $key_total;
		$this->quota["used"] = $key_total_used;
		$this->quota["free"] = $key_total - $key_total_used;
		$this->quota["free_keys"] = $keys_available;
		$this->quota["school"] = $keys[sizeof($keys)-1];
	}
	
	# Read encrypted key details
	function GET_KEY_LICENSE($my_file_path){
		$data = array();
		if (file_exists($my_file_path))
		{
			if ($fd = fopen($my_file_path, "r"))

			{

				$data = $this->DATA_DECRYPT(fread($fd, filesize($my_file_path)));

			}

			fclose ($fd);
		}

		return $data;
	}
	
	# Decrypt key string
	function DATA_DECRYPT($string)
	{
		$key = $this->key_encrpyt;
		$result = '';
		for($i=1; $i<=strlen($string); $i++)
		{
			$char = substr($string, $i-1, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
		}
		$result = unserialize($result);

		return $result;
	}
	
	# Check if the student account is suspended
	function IS_ACCOUNT_SUSPENED($ParUserID)
	{
		global $eclass_db;

		$sql =	"
							SELECT
								IsSuspend
							FROM
								{$eclass_db}.PORTFOLIO_STUDENT
							WHERE
								UserID = '".$ParUserID."'
						";
		$Result = $this->returnVector($sql);

		if($Result[0] == 1)
			$ReturnVal = true;
		else
			$ReturnVal = false;

		return $ReturnVal;
	}

	# Generate summary table of student number for each class for display
	# Todo: "School-based scheme Updated" rows not implemented
	function GEN_CLASS_STUDENT_ACTIVATED_TABLE($ParPageInfoArr, $ParClassLevelID="", $ParField=1, $ParOrder=1, $ParUserID="", $ParCompareTime="")
	{
		global $image_path, $ec_iPortfolio, $LAYOUT_SKIN, $no_record_msg, $ck_user_rights_ext;
	
		$ReturnArr = $this->GET_CLASS_STUDENT_ACTIVATED($ParClassLevelID);
		
		//$isAllClass = ($this->IS_IPF_ADMIN() || strstr($ck_user_rights_ext, "student_info,"));
		$isAllClass = strstr($ck_user_rights_ext, "student_info,");
		if(!$isAllClass)
		{
		  # Eric Yip (20090819): Get teaching classes by access right
		  list($CT_ClassList, $ST_ClassList) = $this->GET_TEACHING_CLASS_BY_RIGHTS("student_info");
		}
		
		$LPNewArr = $this->GET_NUMBER_LP_NEW_PUBLISHED($ParClassLevelID, $ParUserID);
		$ClassStudentArrTemp = $this->GET_CLASS_STUDENT($ParClassLevelID);
		$SRNewArr = $this->GET_NUMBER_UPDATED_SCHOOL_RECORD($ParCompareTime);

		# Organize data for sorting and generating table
		if(is_array($ClassStudentArrTemp))
		{
			foreach($ClassStudentArrTemp as $ClassName => $TotalStudentNumber)
			{
				if($isAllClass || in_array($ClassName, $CT_ClassList) ||  in_array($ClassName, $ST_ClassList))
				{
					$AllClassResArr[] = array	(
																			$ClassName,
																			($SRNewArr[$ClassName]==""?0:$SRNewArr[$ClassName]),
																			"-",
																			($LPNewArr[$ClassName]==""?0:$LPNewArr[$ClassName]),
																			($ReturnArr[$ClassName]==""?0:$ReturnArr[$ClassName]),
																			$TotalStudentNumber
																		);
																		
					switch($ParField)
					{
						case 0:
							$OrderFieldArr[] = $ClassName;
							break;
						case 1:
							$OrderFieldArr[] = ($SRNewArr[$ClassName]==""?0:$SRNewArr[$ClassName]);
							break;
						case 2:
							break;
						case 3:
							$OrderFieldArr[] = ($LPNewArr[$ClassName]==""?0:$LPNewArr[$ClassName]);
							break;
						case 4:
							$OrderFieldArr[] = ($ReturnArr[$ClassName]==""?0:$ReturnArr[$ClassName]);
							break;
						case 5:
							$OrderFieldArr[] = $TotalStudentNumber;
							break;
					}
					
					$ClassStudentArr[$ClassName] = $TotalStudentNumber;
				}
			}
			
			if(is_array($OrderFieldArr))
				array_multisort($OrderFieldArr, ($ParOrder<0?SORT_DESC:SORT_ASC), $AllClassResArr);
		} 

		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		} 

		$ReturnStr =	"
										<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"1\" bgcolor=\"#CCCCCC\">\n
											<tr class=\"tabletop\">\n
												<td width=\"20\" class=\"tabletoplink\">#</td>\n
												<td width=\"25%\"><a href=\"javascript:jSORT_TABLE(0, ".($ParField==0?($ParOrder>0?-1:1):-1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort0','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['class']." ".($ParField==0?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort0\" id=\"sort0\" valign=\"absbottom\" />":"")."</a></td>\n
												<td><a href=\"javascript:jSORT_TABLE(1, ".($ParField==1?($ParOrder>0?-1:1):-1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort1','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['heading']['school_record_updated']." ".($ParField==1?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort1\" id=\"sort1\" valign=\"absbottom\" />":"")."</a></td>\n
												<td><a href=\"javascript:jSORT_TABLE(3, ".($ParField==3?($ParOrder>0?-1:1):-1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort3','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['heading']['lp_updated']." ".($ParField==3?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort3\" id=\"sort3\" valign=\"absbottom\" />":"")."</a></td>\n
												<td align=\"center\"><a href=\"javascript:jSORT_TABLE(4, ".($ParField==4?($ParOrder>0?-1:1):-1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort4','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['heading']['no_lp_active_stu']." ".($ParField==4?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort4\" id=\"sort4\" valign=\"absbottom\" />":"")."</a></td>\n
												<td align=\"center\"><a href=\"javascript:jSORT_TABLE(5, ".($ParField==5?($ParOrder>0?-1:1):-1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort5','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['heading']['no_stu']." ".($ParField==5?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort5\" id=\"sort5\" valign=\"absbottom\" />":"")."</a></td>\n
											</tr>\n
									";

		if(isset($AllClassResArr))
		{
			for($i=0; $i<count($AllClassResArr); $i++)
			{
				if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
				{
					$RowClass = ($i%2==0)?"tablerow1":"tablerow2";
					$ReturnStr .=	"
													<tr class=\"".$RowClass."\">\n
														<td valign=\"top\" class=\"tabletext\">".($i+1)."</td>\n
														<td><a href=\"school_records_class.php?ClassName=".$AllClassResArr[$i][0]."\" class=\"tablelink\">".$AllClassResArr[$i][0]."</a></td>\n
														<td align=\"center\" class=\"tabletext\">".$AllClassResArr[$i][1]."</td>\n
														<td align=\"center\" class=\"tabletext\">".$AllClassResArr[$i][3]."</td>\n
														<td align=\"center\" class=\"tabletext\">".$AllClassResArr[$i][4]."</td>\n
														<td align=\"center\" class=\"tabletext\">".$AllClassResArr[$i][5]."</td>\n
													</tr>
												";
				}
			}
		}
		else
		{
			$ReturnStr .=	"
											<tr>
												<td class=\"tabletext\" colspan=\"7\">&nbsp;$no_record_msg</td>
											</tr>
										";
		}
		
		$ReturnStr .=	"</table>\n";
		
		return array($ReturnStr, $ClassStudentArr);
	}
	
	# Generate activated student table for display
	function GEN_ACTIVATED_STUDENT_TABLE($ParPageInfoArr, $ParClassName, $ParDisplayType, $ParSearchName="", $ParClassLevelID="", $ParField=0, $ParOrder=1)
	{
		global $image_path, $intranet_session_language, $ec_iPortfolio, $LAYOUT_SKIN, $no_record_msg, $ck_function_rights, $ck_user_rights, $iPort;
	
		$RecPerRow = 5;
	
		# Get student list
		$StudentList = $this->GET_STUDENT_LIST_DATA($ParClassName, false, $ParSearchName, $ParClassLevelID, $ParField, $ParOrder);
		
		$PortfolioStatus = $this->GET_LEARNING_PORTFOLIO_STATUS($ParClassName, $ParSearchName, $ParClassLevelID);

		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		} 
		
		$ReturnStr =	"<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">\n";

		if($ParDisplayType == "thumbnail")
		{
			// Use thumbnail as display style
			for($i=0; $i<count($StudentList); $i++)
			{
				if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
				{
					# Display five students per row
					if(($i-$FirstRow) % $RecPerRow == 0)
						$ReturnStr .= "<tr>\n";
						
					# Get student object for each student in the list
					$stu_obj = $this->GET_STUDENT_OBJECT($StudentList[$i][0]);
					
					$PhotoStyle = ($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0) ? "on" : "off";
					$ActivateIcon = ($StudentList[$i][2] != "" && $StudentList[$i][3] != "") ? "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_active_ipf.gif\" width=\"16\" height=\"20\" border=\"0\" align=\"absmiddle\">" : "";

					# Set links for photo			
					if(strpos($stu_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $stu_obj['PhotoLink'] != "")
						$stu_obj['PhotoLink'] = "
																			<table border='0' cellspacing='0' cellpadding='0'>
																				<tr>
																					<td background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_01.gif' style='padding-left:10px; padding-top:10px;'>".str_replace("<!--ImageStyle-->", "", $stu_obj['PhotoLink'])."</td>
																					<td height='10' background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_02.gif'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' title='' width='10' height='10'></td>
																				</tr>
																				<tr>
																					<td height='10' background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_03.gif'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='10' height='10'></td>
																					<td height='10'><img src='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_04.gif' width='10' height='10'></td>
																				</tr>
																			</table>
																		";
					else 
						$stu_obj['PhotoLink'] = "
																			<table border='0' cellspacing='0' cellpadding='0'>
																				<tr>
																					<td background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_01.gif' style='padding-left:10px; padding-top:10px;'><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" /></td>
																					<td height='10' background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_02.gif'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' title='' width='10' height='10'></td>
																				</tr>
																				<tr>
																					<td height='10' background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_03.gif'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='10' height='10'></td>
																					<td height='10'><img src='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_04.gif' width='10' height='10'></td>
																				</tr>
																			</table>
																		";

					$ReturnStr .=	"
													<td width=\"".floor(100/$RecPerRow)."%\" align=\"center\" valign=\"top\">\n
														<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n
															<tr>\n
																<td align=\"center\">".$stu_obj['PhotoLink']."</td>\n
															</tr>\n
															<tr>\n
																<td class=\"tabletext\">".($intranet_session_language=="b5"?$stu_obj['ChineseName']:$stu_obj['EnglishName'])." ".$ActivateIcon."</td>\n
															</tr>\n
															<tr>\n
																<td>\n
																	<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n
																		<tr>\n
																			<td valign=\"top\"><a href=\"javascript:jTO_INFO('".$stu_obj['UserID']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_stu_info.gif\" alt=\"".$ec_iPortfolio['heading']['student_info']."\" width=\"20\" height=\"20\" border=\"0\"></a></td>\n
												";
					$ReturnStr .=	($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0 && $this->HAS_SCHOOL_RECORD_VIEW_RIGHT())?"<td valign=\"top\"><a href=\"javascript:jTO_SR('".$stu_obj['UserID']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_school_record.gif\" alt=\"".$ec_iPortfolio['school_record']."\" width=\"20\" height=\"20\" border=\"0\"></a></td>\n":"<td>&nbsp;</td>";
					$StatusStr = ($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)?"<td valign=\"top\"><a href=\"javascript:jTO_LP('".$stu_obj['UserID']."')\" class=\"new_alert\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_learning_portfolio.gif\" alt=\"".$ec_iPortfolio['heading']['learning_portfolio']."\" width=\"20\" height=\"20\" border=\"0\"><!--Status--></a></td>\n":"";
					$ReturnStr .= (strstr($ck_user_rights, ":web:") && $this->HAS_RIGHT("learning_sharing")) ? str_replace("<!--Status-->", "<br />".$PortfolioStatus[$StudentList[$i][0]], $StatusStr) : "";
					$ReturnStr .= (strstr($ck_user_rights, ":growth:") && $this->HAS_RIGHT("growth_scheme")) ? ($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0 ? "<td class=\"$cell_style\" align=\"center\" valign=\"top\"><a href=\"javascript:jTO_SBS('".$stu_obj['UserID']."')\" class=\"new_alert\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_sbs.gif\" alt=\"".$iPort['menu']['school_based_scheme']."\" width=\"20\" height=\"20\" border=\"0\"></a></td>" : "") : "";
					$ReturnStr .=	"
																		</tr>\n
																	</table>\n
																	<a href=\"javascript:;\"></a><br><br>\n
																</td>\n
															</tr>\n
														</table>\n
													</td>\n
												";
						
					if(($i-$FirstRow) % $RecPerRow == 4 || $i + 1 == count($StudentList) || $i + 1 == $LastRow)
						$ReturnStr .= "</tr>\n";
				}
			}
			
			if(count($StudentList) == 0)
			{
				$ReturnStr .=	"
												<tr>
													<td class=\"tabletext\">&nbsp;$no_record_msg</td>
												</tr>
											";
			}
		}
		else if($ParDisplayType == "list")
		{
			// Use list as display style
			$ReturnStr .=	"
								<tr class=\"tabletop\">
									<td><a href=\"javascript:jSORT_TABLE(3, ".($ParField==3?($ParOrder>0?-1:1):1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort3','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['class']." ".($ParField==3?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort0\" id=\"sort3\" valign=\"absbottom\" />":"")."</a></td>\n
									<td><a href=\"javascript:jSORT_TABLE(0, ".($ParField==0?($ParOrder>0?-1:1):1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort0','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['heading']['no']." ".($ParField==0?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort0\" id=\"sort0\" valign=\"absbottom\" />":"")."</a></td>\n
									<td align=\"center\" class=\"tabletoplink\">&nbsp;</td>
									<td><a href=\"javascript:jSORT_TABLE(1, ".($ParField==1?($ParOrder>0?-1:1):1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort1','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['heading']['student_name']." ".($ParField==1?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort1\" id=\"sort1\" valign=\"absbottom\" />":"")."</a></td>\n
									<td><a href=\"javascript:jSORT_TABLE(2, ".($ParField==2?($ParOrder>0?-1:1):1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort2','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['heading']['student_regno']." ".($ParField==2?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort2\" id=\"sort2\" valign=\"absbottom\" />":"")."</a></td>\n
									<td align=\"center\" class=\"tabletoplink\">".$ec_iPortfolio['heading']['student_info']."</td>
									<td align=\"center\" class=\"tabletoplink\">".$ec_iPortfolio['heading']['student_record']."</td>
							";
			$ReturnStr .=	strstr($ck_user_rights, ":web:") ? "<td align=\"center\" class=\"tabletoplink\">".$ec_iPortfolio['heading']['learning_portfolio']."</td>" : "";
			$ReturnStr .=	(strstr($ck_user_rights, ":growth:") && $this->HAS_RIGHT("growth_scheme")) ? "<td align=\"center\" class=\"tabletoplink\">".$iPort['menu']['school_based_scheme']."</td>" : "";
			//$ReturnStr .=	"<td align=\"center\" class=\"tabletoplink\">".$iPort['menu']['school_based_scheme']."</td>";
			$ReturnStr .= strstr($ck_function_rights, "Profile:Student")?"<td width=\"25\"><input type=checkbox name='checkmaster' onClick=(this.checked)?setChecked(1,document.form1,'user_id[]'):setChecked(0,document.form1,'user_id[]')></td>":"";
			$ReturnStr .=	"
								</tr>
							";
		
			for($i=0; $i<count($StudentList); $i++)
			{
				if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
				{					
					# Get student object for each student in the list
					$stu_obj = $this->GET_STUDENT_OBJECT($StudentList[$i][0]);
					
					# Set links for photo
/*					if(strpos($stu_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $stu_obj['PhotoLink'] != "")
						$stu_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "class=\"photo_border_on\"", $stu_obj['PhotoLink']);
					else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
						$stu_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" class=\"photo_border_on\" />";
					else
						$stu_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" class=\"photo_border\" />";
*/
					if(strpos($stu_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $stu_obj['PhotoLink'] != "")
//						$PhotoAction = "<a href=\"javascript:;\" class=\"contenttool\" onMouseOver=\"jSHOW_PHOTO('".trim(str_replace("#", "", $stu_obj['WebSAMSRegNo']))."')\" onMouseOut=\"jHIDE_PHOTO()\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/smartcard/icon_student_photo.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>";
						$PhotoAction = "<a href=\"javascript:;\" class=\"contenttool\" onMouseOver=\"jSHOW_PHOTO('".$stu_obj['PhotoURL']."')\" onMouseOut=\"jHIDE_PHOTO()\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/smartcard/icon_student_photo.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>";
					else
						$PhotoAction = "&nbsp;";

					if($StudentList[$i][2] != "" && $StudentList[$i][3] != "")
					{
						$cell_style = ($StudentList[$i][4] == 0) ? "row_on" : "row_off";
					
						$ReturnStr .= "
														<tr class=\"tablerow1\">\n
															<td class=\"$cell_style tabletext\">".$stu_obj['ClassName']."</td>\n
															<td class=\"$cell_style tabletext\">".($stu_obj['ClassNumber']==""?"&nbsp;":$stu_obj['ClassNumber'])."</td>\n
															<td class=\"$cell_style\" align=\"center\">".$PhotoAction."</td>\n
															<td class=\"$cell_style tabletext\">".($intranet_session_language=="b5"?($stu_obj['ChineseName']==""?"&nbsp;":$stu_obj['ChineseName']):($stu_obj['EnglishName']==""?"&nbsp;":$stu_obj['EnglishName']))."</td>\n
															<td class=\"$cell_style tabletext\">".$stu_obj['WebSAMSRegNo']." <img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_active_ipf.gif\" width=\"16\" height=\"20\" border=\"0\" align=\"absmiddle\"></td>\n
															<td class=\"$cell_style\" align=\"center\"><a href=\"javascript:jTO_INFO('".$stu_obj['UserID']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_stu_info.gif\" alt=\"".$ec_iPortfolio['heading']['student_info']."\" width=\"20\" height=\"20\" border=\"0\"></a></td>\n
													";
						$ReturnStr .= ($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0 && $this->HAS_SCHOOL_RECORD_VIEW_RIGHT())?"<td class=\"$cell_style\" align=\"center\"><a href=\"javascript:jTO_SR('".$stu_obj['UserID']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_school_record.gif\" alt=\"".$ec_iPortfolio['school_record']."\" width=\"20\" height=\"20\" border=\"0\"></a></td>\n":"<td class=\"$cell_style\">&nbsp;</td>";
						$StatusStr = ($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)?"<td class=\"$cell_style\" valign=\"top\" align=\"center\"><a href=\"javascript:jTO_LP('".$stu_obj['UserID']."')\" class=\"new_alert\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_learning_portfolio.gif\" alt=\"".$ec_iPortfolio['heading']['learning_portfolio']."\" width=\"20\" height=\"20\" border=\"0\"><!--Status--></a></td>\n":"<td class=\"$cell_style\">&nbsp;</td>";
						$ReturnStr .= (strstr($ck_user_rights, ":web:") && $this->HAS_RIGHT("learning_sharing")) ? str_replace("<!--Status-->", $PortfolioStatus[$StudentList[$i][0]], $StatusStr) : "";
						$ReturnStr .= (strstr($ck_user_rights, ":growth:") && $this->HAS_RIGHT("growth_scheme")) ? ($StudentList[$i][4] == 0 ? "<td class=\"$cell_style\" align=\"center\"><a href=\"javascript:jTO_SBS('".$stu_obj['UserID']."')\" class=\"new_alert\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_sbs.gif\" alt=\"".$iPort['menu']['school_based_scheme']."\" width=\"20\" height=\"20\" border=\"0\"></a></td>" : "<td class=\"$cell_style\">&nbsp;</td>") : "";
						//$ReturnStr .= ($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0) ? "<td class=\"$cell_style\" align=\"center\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_scheme_off.gif\" alt=\"".$iPort['menu']['school_based_scheme']."\" width=\"20\" height=\"20\" border=\"0\"></td>" : "<td class=\"$cell_style\">&nbsp;</td>";
						$ReturnStr .= strstr($ck_function_rights, "Profile:Student")?"<td class=\"$cell_style\"><input type=\"checkbox\" name=\"user_id[]\" value=\"".$stu_obj['UserID']."\"></td>":"";
						$ReturnStr .= "</tr>\n";
					}
					else
					{
						$ReturnStr .= "
														<tr class=\"tablerow1\">\n
															<td class=\"row_off tabletext\">".$stu_obj['ClassName']."</td>\n
															<td class=\"row_off tabletext\" >".($stu_obj['ClassNumber']==""?"&nbsp;":$stu_obj['ClassNumber'])."</td>\n
															<td class=\"row_off\" align=\"center\">".$PhotoAction."</td>\n
															<td class=\"row_off tabletext\">".($intranet_session_language=="b5"?($stu_obj['ChineseName']==""?"&nbsp;":$stu_obj['ChineseName']):($stu_obj['EnglishName']==""?"&nbsp;":$stu_obj['EnglishName']))."</td>\n
															<td class=\"row_off tabletext\" >".($stu_obj['WebSAMSRegNo']==""?"&nbsp;":$stu_obj['WebSAMSRegNo'])."</td>\n
															<td class=\"row_off\" align=\"center\"><a href=\"javascript:jTO_INFO('".$stu_obj['UserID']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_stu_info.gif\" alt=\"".$ec_iPortfolio['heading']['student_info']."\" width=\"20\" height=\"20\" border=\"0\"></a></td>\n
													";
						$ReturnStr .=	($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0 && $this->HAS_SCHOOL_RECORD_VIEW_RIGHT())?"<td class=\"row_off\" align=\"center\"><a href=\"javascript:jTO_SR('".$stu_obj['UserID']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_school_record.gif\" alt=\"".$ec_iPortfolio['school_record']."\" width=\"20\" height=\"20\" border=\"0\"></a></td>\n":"<td class=\"row_off\">&nbsp;</td>";
						$ReturnStr .=	(strstr($ck_user_rights, ":web:") && $this->HAS_RIGHT("learning_sharing")) ? "<td class=\"row_off\">&nbsp;</td>" : "";
						$ReturnStr .=	(strstr($ck_user_rights, ":growth:") && $this->HAS_RIGHT("growth_scheme")) ? "<td class=\"row_off\">&nbsp;</td>" : "";
						//$ReturnStr .=	"<td class=\"row_off\">&nbsp;</td>";
						$ReturnStr .=	strstr($ck_function_rights, "Profile:Student")?"<td class=\"row_off\"><input type=\"checkbox\" name=\"user_id[]\" value=\"".$stu_obj['UserID']."\"></td>":"";
						$ReturnStr .=	"</tr>\n";
					}
				}
			}
			
			if(count($StudentList) == 0)
			{
				$ReturnStr .=	"
												<tr>
													<td colspan=\"8\" class=\"tabletext\">&nbsp;$no_record_msg</td>
												</tr>
											";
			}
		}
		
		$ReturnStr .= "</table>\n";
		
		return array($ReturnStr, $StudentList, $PortfolioStatus);
	}

	# Activate iPortfolio accounts and generate activation result
	function GEN_ACTIVATE_STUDENT_RESULT($ParUserID)
	{
		global $ec_iPortfolio, $ec_guide, $namelist_class_number, $ec_student_word;

		# activation and get failed records
		list($key_index, $FailedUserArr) = $this->DO_ACTIVATE_STUDENT($ParUserID);
	
		# prepare results to be shown
		$ReturnStr = "<table border='0' cellpadding='8' cellspacing='0'>";
		$ReturnStr .= "<tr><td class='tabletext'>".$ec_iPortfolio['activation_total_success']." : </td><td class='chi_content_15'><b>".$key_index."</b></td></tr>\n";
		$ReturnStr .= "<tr><td class='tabletext'>".$ec_iPortfolio['activation_total_fail']." : </td><td class='chi_content_15'><b>".sizeof($FailedUserArr)."</b></td></tr>\n";
		$ReturnStr .= "</table>\n";

		if(is_array($FailedUserArr) && count($FailedUserArr) > 0)
		{
			$ReturnStr .=	"
											<table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n
												<tr>
													<td class=tableTitle bgcolor='#CFE6FE'>#</td>
													<td class=tableTitle bgcolor='#CFE6FE'>".$ec_iPortfolio['class']."</td>
													<td class=tableTitle bgcolor='#CFE6FE'>".$namelist_class_number."</td>
													<td class=tableTitle bgcolor='#CFE6FE'>".$ec_student_word['name_english']." / ".$ec_student_word['name_chinese']."</td>
													<td class=tableTitle bgcolor='#CFE6FE'>".$ec_guide['fail_reason']."</td>
												</tr>\n
										";
			for ($i=0; $i<sizeof($FailedUserArr); $i++)
			{
				$student_obj = $FailedUserArr[$i];
				$css_color = ($i%2==0) ? "#FFFFFF" : "#F3F3F3";
				switch($student_obj[4])
				{
					case "NO_REGNO":
						$reason_txt = $ec_iPortfolio['activation_result_no_regno'];
						break;
					case "ACTIVATED":
						$reason_txt = $ec_iPortfolio['activation_result_activated'];
						break;
					case "EMPTY_USER_KEY":
						# Todo: add reason for empty user key
					case "UNKNOWN":
					default:
						$reason_txt = $ec_guide['import_error_unknown'];
						break;
				}
				$ReturnStr .= "<tr bgcolor='$css_color'><td>".($i+1)."</td><td>".$student_obj[2]."</td><td>".$student_obj[3]."</td><td>".$student_obj[0]." / ".$student_obj[1]."</td><td>".$reason_txt."</td></tr>\n";
			}
			$ReturnStr .= "</table>";
		}
		else if($FailedUserArr == "activation_no_quota")
		{
			$ReturnStr .= "<font color='red'>".$ec_iPortfolio['activation_no_quota']."</font>";
		}
	
		return $ReturnStr;
	}

##########################################################################
# For iPortfolio Accounts Activation/Deactivation
##########################################################################

	# Activate students
	function DO_ACTIVATE_STUDENT($my_user_id){
		global $eclass_db, $intranet_db, $intranet_root;
		
		//$ClassNumberField = getClassNumberField("iu.");
		# check if quota is enough
		$sql =	"
							SELECT
								iu.UserID,
								iu.WebSAMSRegNo AS RegNo,
								ps.UserKey,
								ps.RecordID,
								us.Nationality,
								us.PlaceOfBirth,
								us.AdmissionDate,
								ps.IsSuspend,
								iu.UserEmail,
								iu.UserPassword,
								iu.ClassNumber,
								iu.FirstName,
								iu.LastName,
								iu.ClassName,
								CASE iu.Title
									WHEN 0 THEN 'MR.'
									WHEN 1 THEN 'MISS.'
									WHEN 2 THEN 'MRS.'
									WHEN 3 THEN 'MS.'
									WHEN 4 THEN 'DR.'
									WHEN 5 THEN 'PROF.'
								END AS Title,
								iu.EnglishName,
								iu.ChineseName,
								iu.NickName,
								iu.Gender
							FROM
								{$intranet_db}.INTRANET_USER AS iu
							LEFT JOIN
								{$eclass_db}.PORTFOLIO_STUDENT AS ps
							ON
								ps.UserID = iu.UserID
							LEFT JOIN
								{$intranet_db}.INTRANET_USER_PERSONAL_SETTINGS AS us
							ON
								us.UserID = iu.UserID
							WHERE
								iu.UserID IN ($my_user_id)
							ORDER BY
								iu.ClassName,
								iu.ClassNumber
						";
		$row = $this->returnArray($sql);
		$numOfReActivate = 0;
		$numOfFirstActivate = 0;
		for ($i=0; $i<sizeof($row); $i++)
		{
			$_user_key = $row[$i]["UserKey"];
			if ($_user_key == '') {
				$numOfFirstActivate++;
			}
			else {
				$numOfReActivate++;
			}
		}		
		
		// Ivan [2011-0509-1159-40071] Should not count re-activate student
		//if (sizeof($row)<=$this->quota["free"])
		if ($_SESSION["platform"]=="KIS" || ($numOfFirstActivate <= $this->quota["free"]))
		{
			$keys = $this->quota["free_keys"];
			$key_index = 0;

			$school_lang = get_file_content($intranet_root."/file/language.txt");
			$MemberType = "S";
			
			for ($i=0; $i<sizeof($row); $i++)
			{
				$user_obj = $row[$i];
				$user_key = $keys[$key_index];
				
				if ($_SESSION["platform"]=="KIS") {
					$user_key = date('Ymd_His').'_'.$user_obj["UserID"];
				}
				
				# attempt to insert if qualify (RegNo exist but not activated before)
				# 20081016 : Check if user key to be used is empty				
				if ($user_obj["UserID"] != "" && $user_obj["RegNo"] != "" && $user_obj["UserKey"] == "" && $user_key != "")
				{
					$sql =	"
										INSERT INTO
											{$eclass_db}.PORTFOLIO_STUDENT
												(UserID, WebSAMSRegNo, UserKey, Nationality, PlaceOfBirth, AdmissionDate, IsSuspend, InputDate, ModifiedDate)
										VALUES
											('".$user_obj["UserID"]."', '".$user_obj["RegNo"]."', '".$user_key."', '".$user_obj["Nationality"]."', '".$user_obj["PlaceOfBirth"]."', '".$user_obj["AdmissionDate"]."', 0, now(), now())
							";
					$this->db_db_query($sql);
					if ($this->db_affected_rows()==1)
					{
						$key_index ++;

						# import to eClass
						$tmpUserID = $user_obj["UserID"];
						$UserEmail = $user_obj["UserEmail"];
						$UserPassword = $user_obj["UserPassword"];
						$ClassNumber = $user_obj["ClassNumber"];
						$FirstName = $user_obj["FirstName"];
						$LastName = $user_obj["LastName"];
						$ClassName = $user_obj["ClassName"];
						$Title = $user_obj["Title"];
						$EngName = $user_obj["EnglishName"];
						$ChiName = $user_obj["ChineseName"];
						$NickName = $user_obj["NickName"];
						$Gender = $user_obj["Gender"];
						$eclassClassNumber = ($ClassNumber=="" || $ClassName=="" || stristr($ClassNumber, $ClassName)) ? $ClassNumber : $ClassName ." - ".$ClassNumber;
						$CourseUserID = $this->eClassUserAddFullInfo($UserEmail, $Title, $FirstName, $LastName, $EngName, $ChiName, $NickName, $MemberType, $UserPassword, $eclassClassNumber, $Gender, $ICQNo, $HomeTelNo, $FaxNo, $DateOfBirth, $Address, $Country, $URL, $Info, $ClassName, $school_lang, $tmpUserID);


						# update for course user_id
						if ($CourseUserID>0)
						{
							$sql =	"
												UPDATE
													{$eclass_db}.PORTFOLIO_STUDENT
												SET
													CourseUserID = '".$CourseUserID."'
												WHERE
													UserID = '".$user_obj["UserID"]."'
											";
							$this->db_db_query($sql);
						}
					}
					else
					{
						# failed by unknown reason
						$failed_record[] = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "UNKNOWN");
					}
				}
				else
				{
					# check regno
					if ($user_obj["RegNo"]=="")
					{
						# no reg no
						$failed_record[] = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "NO_REGNO");
					}
					else if($user_obj["UserKey"]!="")
					{
						# try to reactivate
						list($temp_key_index, $temp_failed_record) = $this->DO_REACTIVATE_STUDENT($user_obj["UserID"]);

						if(is_int($temp_key_index))
							$react_key_index += $temp_key_index;

						if(is_array($temp_failed_record))
							$failed_record[] = $temp_failed_record;

						# already activated
						// $failed_record[] = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "ACTIVATED");
					}
					else if($user_key == "")
					{
						# failed by unknown reason
						$failed_record[] = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "EMPTY_USER_KEY");
					}
					else
					{
						# failed by unknown reason
						$failed_record[] = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "UNKNOWN");
					}
				}
			}

			# Update eClass Course Usage
			$this->eClassUserNumber();
		}
		else
			$failed_record = "activation_no_quota";

		return array($key_index+$react_key_index, $failed_record);
	}
	
	# Add User to eClass Course (copied from libeclass.php)
	function eClassUserAddFullInfo($email, $title, $firstname, $lastname, $engName, $chiName, $nickname, $memberType, $password, $class_number,$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,$URL,$Info,$ClassName="", $school_lang=1, $intranetUserID=''){
		global $eclass_db;

		$email = addslashes($email);
		$title = addslashes($title);
		$firstname = addslashes($firstname);
		$lastname = addslashes($lastname);
		$engName = addslashes($engName);
		$chiName = addslashes($chiName);
		$nickname = addslashes($nickname);
		$memberType = addslashes($memberType);
		$password = ($password=="") ? "" :  addslashes($password);
		$class_number = addslashes($class_number);
		$Gender = addslashes($Gender);
		$ICQNo = addslashes($ICQNo);
		$HomeTelNo = addslashes($HomeTelNo);
		$FaxNo = addslashes($FaxNo);
		$DateOfBirth = addslashes($DateOfBirth);
		$Address = addslashes($Address);
		$Country = addslashes($Country);
		$URL = addslashes($URL);
		$Info = addslashes($Info);
		
		$intranetUserIDText = ($intranetUserID != '')? "'".$intranetUserID."'" : 'null';
		
		# - commented by Max 2010-10-20, not following school_lang to set the 1st name
		# 	put $engName->firstname, $chiName->chinesename
//		$name2copy = ($school_lang==1 || $school_lang==2) ? ($chiName!=""?$chiName:$engName) : ($engName!=""?$engName:$chiName);
//		$firstname2eclass = $name2copy;
		$firstname2eclass = $engName;
		$lastname2eclass = "";
		$chinesename2eclass = $chiName;

		$user_id = 0;

		$nickname = ($nickname=="") ? $firstname2eclass : $nickname;
		if ($this->isUserExisted($email)==0)
		{
			# USERMASTER
			$sql =	"
								INSERT INTO
									".$this->course_db.".usermaster
										(user_email, user_password, title, firstname, lastname, nickname, memberType, inputdate, class_number , gender, icq,telno,fax,dob,address,country,homepage,info,chinesename,intranet_user_id)
								VALUES
									('".$email."', '".$password."', '".$title."', '".$firstname2eclass."', '".$lastname2eclass."', '".$nickname."', '".$memberType."', NOW(), '".$class_number."','".$Gender."', '".$ICQNo."','".$HomeTelNo."','".$FaxNo."','".$DateOfBirth."','".$Address."','".$Country."','".$URL."','".$Info."','$chinesename2eclass', $intranetUserIDText)
							";
			$this->db_db_query($sql);
			$user_id = $this->db_insert_id();
			

			# USER_COURSE
			$sql =	"
								INSERT INTO
									{$eclass_db}.user_course
										(course_id, user_id, user_email, user_password, title, firstname, lastname, nickname, memberType, inputdate, pw_lastmodified, class_number , gender, icq,telno,fax,dob,address,country,homepage,info,chinesename,intranet_user_id)
								VALUES
									(".$this->course_id.", ".$user_id.", '".$email."', '".$password."', '".$title."', '".$firstname2eclass."', '".$lastname2eclass."', '".$nickname."', '".$memberType."', NOW(), NOW(), '".$class_number."','".$Gender."', '".$ICQNo."','".$HomeTelNo."','".$FaxNo."','".$DateOfBirth."','".$Address."','".$Country."','".$URL."','".$Info."','$chinesename2eclass', $intranetUserIDText)
							";
			$this->db_db_query($sql);
			
			

			# add to class group
			# format like: "2005-2006 3A"
			$group_name = getCurrentAcademicYear()." ".$ClassName;
			$this->joinEClassGroup($user_id, $group_name);
		}

		return $user_id;
	}
	
	# Update eClass User Usage
	function eClassUserNumber()
	{
		global $eclass_db;

		$sql =	"
							SELECT
								user_id
							FROM
								".$this->course_db.".usermaster
							WHERE
								(status is null OR status NOT IN ('deleted'))
						";
		$row = sizeof($this->returnArray($sql));

		$sql =	"
							UPDATE
								{$eclass_db}.course
							SET
								no_users = '".$row."'
							WHERE
								course_id = '".$this->course_id."'";
		$this->db_db_query($sql);
	}
	
	# Check the Existence of Course User (copied from libeclass.php)
	function isUserExisted($email)
	{
		$sql =	"
							SELECT
								user_id
							FROM
								".$this->course_db.".usermaster
							WHERE
								(status is null OR status NOT IN ('deleted')) AND
								user_email = '".$email."'
						";

		return sizeof($this->returnVector($sql));
	}
	
	# join eClass group
	# creat a new group if it does not exist
	function joinEClassGroup($user_id, $group_name)
	{
		# check group existence
		if (trim($group_name)=="")
		{
			return false;
		}
		$group_id = $this->getEClassGroupID($group_name);

		# add user to the group
		$sql =	"
							INSERT INTO
								".$this->course_db.".user_group
									(user_id, group_id)
							VALUES
								('".$user_id."', '".$group_id."')
						";
		$this->db_db_query($sql);

		return;
	}
	
	# Get the Group ID from eClass
	function getEClassGroupID($group_name){
		global $ec_words;

		# load all groups
		$sql =	"
							SELECT
								group_id,
								group_name
							FROM
								".$this->course_db.".grouping
							ORDER BY
								group_id
						";
		if (!is_array($groupings = $this->returnArray($sql)))
		{
			$groupings = array();
		}

		$group_id = 0;
		for ($i=0; $i<sizeof($groupings); $i++)
		{
			if (strtoupper($groupings[$i][1])==strtoupper($group_name))
			{
				$group_id = $groupings[$i][0];
				break;
			}
		}

		if ($group_id==0)
		{
			# add new group
			$group_desc = $ec_words['group_default'];
			$group_status = 1;

			$sql =	"
								INSERT INTO
									".$this->course_db.".grouping
										(group_name, group_desc, inputdate, modified, Status)
								VALUES
									('".addslashes($group_name)."', '".$group_desc."', now(), now(), ".$group_status.")
							";
			$this->db_db_query($sql);
			$group_id = $this->db_insert_id();
		}

		return $group_id;
	}
	
	# Suspend the student account(s)
	function DO_SUSPEND_STUDENT($my_user_id)
	{
		global $eclass_db, $intranet_db;

		//$ClassNumberField = getClassNumberField("iu.");
		# check if this user activated
		# check if quota is enough
		$sql =	"
							SELECT
								iu.UserID,
								iu.WebSAMSRegNo AS RegNo,
								ps.UserKey,
								ps.RecordID,
								ps.IsSuspend,
								iu.UserEmail,
								iu.UserPassword,
								iu.ClassNumber,
								iu.FirstName,
								iu.LastName,
								iu.ClassName,
								CASE iu.Title
									WHEN 0 THEN 'MR.'
									WHEN 1 THEN 'MISS.'
									WHEN 2 THEN 'MRS.'
									WHEN 3 THEN 'MS.'
									WHEN 4 THEN 'DR.'
									WHEN 5 THEN 'PROF.'
								END AS Title,
								iu.EnglishName,
								iu.ChineseName,
								iu.NickName,
								iu.Gender,
								um.user_id AS CourseUserID
							FROM
								{$intranet_db}.INTRANET_USER AS iu
							LEFT JOIN
								{$eclass_db}.PORTFOLIO_STUDENT AS ps
							ON
								ps.UserID = iu.UserID
							LEFT JOIN
								".$this->course_db.".usermaster AS um
							ON
								um.user_email = iu.useremail
							WHERE
								iu.UserID IN (".$my_user_id.") AND
								(um.status != 'deleted' OR um.status IS NULL)
							ORDER BY
								iu.ClassName,
								iu.ClassNumber
						";
		$row = $this->returnArray($sql);

		$key_index = 0;
		for($i=0; $i<sizeof($row); $i++)
		{
			$user_obj = $row[$i];
			if($user_obj["RecordID"]!="" && $user_obj["IsSuspend"]==0)
			{
				$sql = "
									UPDATE
										{$eclass_db}.PORTFOLIO_STUDENT
									SET
										IsSuspend = 1
									WHERE
										RecordID = '".$user_obj["RecordID"]."'";
				$this->db_db_query($sql);

				if ($this->db_affected_rows()==1)
					$key_index++;
			}
			else if($user_obj["RecordID"]!="" && $user_obj["IsSuspend"]==1)
			{
				$failed_record[] = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "SUSPENDED");
			}
			else if($user_obj["RecordID"]=="")
			{
				$failed_record[] = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "INACTIVE");
			}
			else
			{
				$failed_record[] = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "UNKNOWN");
			}
		}

		return array($key_index, $failed_record);
	}
	
	# Re-activate the student account
	function DO_REACTIVATE_STUDENT($my_user_id)
	{
		global $eclass_db, $intranet_db;

		//$ClassNumberField = getClassNumberField("iu.");
		# check if this user activated
		# check if quota is enough
		$sql =	"
							SELECT
								iu.UserID,
								iu.WebSAMSRegNo AS RegNo,
								ps.UserKey,
								ps.RecordID,
								us.Nationality,
								us.PlaceOfBirth,
								us.AdmissionDate,
								ps.IsSuspend,
								iu.UserEmail,
								iu.UserPassword,
								iu.ClassNumber,
								iu.FirstName,
								iu.LastName,
								iu.ClassName,
								CASE iu.Title
									WHEN 0 THEN 'MR.'
									WHEN 1 THEN 'MISS.'
									WHEN 2 THEN 'MRS.'
									WHEN 3 THEN 'MS.'
									WHEN 4 THEN 'DR.'
									WHEN 5 THEN 'PROF.'
								END AS Title,
								iu.EnglishName,
								iu.ChineseName,
								iu.NickName,
								iu.Gender,
								ps.CourseUserID,
								um.user_id,
								um.status,
								um.icq,
								um.telno,
								um.fax,
								um.dob,
								um.address,
								um.country,
								um.homepage,
								um.info
							FROM
								{$intranet_db}.INTRANET_USER AS iu
							LEFT JOIN
								{$eclass_db}.PORTFOLIO_STUDENT AS ps
							ON
								ps.UserID = iu.UserID
							LEFT JOIN
								{$intranet_db}.INTRANET_USER_PERSONAL_SETTINGS AS us
							ON
								us.UserID = iu.UserID
							LEFT JOIN
								".$this->course_db.".usermaster AS um
							ON
								um.user_email = iu.useremail
							WHERE
								iu.UserID = '".$my_user_id."' AND
								um.status IS NULL
						";
		$row = $this->returnArray($sql);

		$key_index = 0;
		for($i=0; $i<sizeof($row); $i++)
		{
			$user_obj = $row[$i];

			# check if CourseUserID in PortfolioStudent is NULL, replace it with the user_id
			if($user_obj['CourseUserID']=="" && $user_obj['user_id']!="")
			{
				$user_obj['CourseUserID'] = $user_obj['user_id'];
				$sql =	"
									UPDATE
										{$eclass_db}.PORTFOLIO_STUDENT
									SET
										CourseUserID = '".$user_obj['user_id']."'
									WHERE
										UserID = '".$user_obj['UserID']."'
								";
				$this->db_db_query($sql);
			}

			/*
			# if student is deleted, add record to user course and update the 'deleted' status to null
			if($user_obj["status"]=='deleted')
			{
				$class_number = $user_obj['ClassName']." - ".$user_obj['ClassNumber'];
				# Add record to user_course
				$sql = "INSERT INTO {$eclass_db}.user_course (course_id, user_id, user_email, user_password, title, firstname, lastname, nickname, memberType, inputdate, pw_lastmodified, class_number , gender, icq,telno,fax,dob,address,country,homepage,info) values ('".$ck_course_id."', '".$user_obj['CourseUserID']."', '".$user_obj['UserEmail']."', '".$user_obj['UserPassword']."', '".$user_obj['Title']."', '".$user_obj['FirstName']."', '".$user_obj['LastName']."', '".$user_obj['NickName']."', 'S', now(), now(), '".$class_number."', '".$user_obj['Gender']."', '".$user_obj['icq']."', '".$user_obj['telno']."', '".$user_obj['fax']."', '".$user_obj['dob']."', '".$user_obj['address']."', '".$user_obj['country']."', '".$user_obj['homepage']."', '".$user_obj['info']."')";
				$this->db_db_query($sql);

				# update the usermaster record status
				$sql = "UPDATE ".ClassNamingDB($ck_course_id).".usermaster SET status = NULL WHERE user_id = '".$user_obj['CourseUserID']."'";
				$this->db_db_query($sql);
			}
			*/

			if($user_obj["RecordID"] != "" && $user_obj["IsSuspend"] == 1)
			{
				$sql =	"
									UPDATE
										{$eclass_db}.PORTFOLIO_STUDENT
									SET
										IsSuspend = 0
									WHERE
										RecordID = '".$user_obj["RecordID"]."'
									";
				$this->db_db_query($sql);

				if ($this->db_affected_rows()==1)
					$key_index++;
			}
			else if($user_obj["RecordID"]!="" && $user_obj["IsSuspend"]==0)
			{
				$failed_record = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "ACTIVATED");
			}
			else if($user_obj["RecordID"]=="")
			{
				$failed_record = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"],  $user_obj["ClassNumber"], "INACTIVE");
			}
			else
			{
				$failed_record = Array($user_obj["EnglishName"], $user_obj["ChineseName"], $user_obj["ClassName"], $user_obj["ClassNumber"], "UNKNOWN");
			}
		}

		return array($key_index, $failed_record);

	}

	# Suspend iPortfolio accounts and generate suspension result
	function GEN_SUSPEND_STUDENT_RESULT($ParUserIDArr)
	{
		global $ec_iPortfolio, $ec_guide, $namelist_class_number, $ec_student_word, $ec_guide;
	
		$UserID = is_array($ParUserIDArr) ? implode(",", $ParUserIDArr) : $ParUserIDArr;
		
		list($key_index, $FailedUserArr) = $this->DO_SUSPEND_STUDENT($UserID);
		
		# prepare results to be shown
		$ReturnStr = "<table border='0' cellpadding='8' cellspacing='0'>";
		$ReturnStr .= "<tr><td class='chi_content_15'>".$ec_iPortfolio['suspend_total_success']." : </td><td class='chi_content_15'><b>".$key_index."</b></td></tr>\n";
		$ReturnStr .= "<tr><td class='chi_content_15'>".$ec_iPortfolio['suspend_total_fail']." : </td><td class='chi_content_15'><b>".sizeof($FailedUserArr)."</b></td></tr>\n";
		$ReturnStr .= "</table>\n";

		if (sizeof($FailedUserArr)!=0)
		{
			$ReturnStr .= "<table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
			$ReturnStr .= "<tr><td class=tableTitle bgcolor='#CFE6FE'>#</td><td class=tableTitle bgcolor='#CFE6FE'>".$ec_iPortfolio['class']."</td><td class=tableTitle bgcolor='#CFE6FE'>".$namelist_class_number."</td><td class=tableTitle bgcolor='#CFE6FE'>".$ec_student_word['name_english']."/".$ec_student_word['name_chinese']."</td><td class=tableTitle bgcolor='#CFE6FE'>".$ec_guide['fail_reason']."</td></tr>\n";
			for ($i=0; $i<sizeof($FailedUserArr); $i++)
			{
				$student_obj = $FailedUserArr[$i];
				$css_color = ($i%2==0) ? "#FFFFFF" : "#F3F3F3";
				switch($student_obj[4])
				{
					case "SUSPENDED":
									$reason_txt = $ec_iPortfolio['suspend_result_suspended'];
									break;
					case "INACTIVE":
									$reason_txt = $ec_iPortfolio['suspend_result_inactive'];
									break;
					case "UNKNOWN":
					default:
									$reason_txt = $ec_guide['import_error_unknown'];
									break;
				}
				$ReturnStr .= "<tr bgcolor='$css_color'><td>".($i+1)."</td><td>".$student_obj[2]."</td><td>".$student_obj[3]."</td><td>".$student_obj[0]."/".$student_obj[1]."</td><td>".$reason_txt."</td></tr>\n";
			}
			$ReturnStr .= "</table>";
		}
		
		return $ReturnStr;
	}

	# Generate uploaded photo student summary table
	function GEN_UPLOADED_PHOTO_STUDENTS($ParPageInfoArr, $ParRegNoArr)
	{
		global $image_path, $LAYOUT_SKIN, $ec_iPortfolio, $button_remove_all;
	
		$total_photo_num = count($ParRegNoArr);
		list($UploadedArr, $UploadedYearArr, $PhotoCount) = $this->GET_UPLOADED_PHOTO_STUDENTS($ParRegNoArr);
		$ClassActivatedNo = $this->GET_CLASS_STUDENT();
		$ClassArchivedNo = $this->GET_CLASS_STUDENT_ARCHIVED();
		
		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		}

		$SummaryTable =	"
											<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>
												<tr class='tabletop'>
													<td width='20' align='center' valign='top'><a href='#' class='tabletoplink' onMouseOver=\"MM_swapImage('sort02','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_d_on.gif',1)\" onMouseOut='MM_swapImgRestore()'>#</a></td>
													<td align='center' valign='top'><a href='#' class='tabletoplink' onMouseOver=\"MM_swapImage('sort02','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_d_on.gif',1)\" onMouseOut='MM_swapImgRestore()'>".$ec_iPortfolio['class']."</a></td>
													<td align='center' valign='top'><a href='#' class='tabletoplink' onMouseOver=\"MM_swapImage('sort02','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_d_on.gif',1)\" onMouseOut='MM_swapImgRestore()'>".$ec_iPortfolio['class_photo_number']."</a></td>
													<td width='100' align='center' valign='top' nowrap><a href='#' class='tabletoplink' onMouseOver=\"MM_swapImage('sort02','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_d_on.gif',1)\" onMouseOut='MM_swapImgRestore()' onClick=\"jREMOVE_PHOTO('ALL')\"><img src='".$image_path."/".$LAYOUT_SKIN."/icon_delete.gif' width='12' height='12' border='0' align='absmiddle'> ".$button_remove_all."</a></td>
												</tr>
										";
		$count = 0;
		
		if (is_array($ClassActivatedNo) && sizeof($ClassActivatedNo)>0)
		{
			foreach($ClassActivatedNo as $ClassName => $TotalStudent)
			{
				if($ParPageInfoArr[0] == "" || ($count >= $FirstRow && $count < $LastRow))
				{
					$RowStyle = ($count%2==0) ? "tablerow1" : "tablerow2";
					$SummaryTable .= "<tr class='".$RowStyle."'>";
					$SummaryTable .= "<td align='center' valign='top' class='tabletext'>".($count+1)."</td>";
					$SummaryTable .= "<td align='center' valign='top' class='tabletext'>".$ClassName."</td>";
					$SummaryTable .= "<td align='center' valign='top' class='tabletext'>".($UploadedArr[$ClassName]==""?0:$UploadedArr[$ClassName])."/".$TotalStudent."</td>";
					$SummaryTable .= ($UploadedArr[$ClassName]=="") ? "<td>&nbsp;</td>" : "<td align='center'><a href=\"javascript:jREMOVE_PHOTO('".$ClassName."')\" class='tabletool'><img src='".$image_path."/".$LAYOUT_SKIN."/icon_delete.gif' width='12' height='12' border='0' align='absmiddle'></a></td>";
					//<input type='button' name='delete' value='$button_remove' onClick=\"doRemove('".$ClassName."')\" class='button_g'>
					$SummaryTable .= "</tr>";
				}
				$count++;
			}
		}
		
		# archived
		$ClassArchivedCount = 0;
		if (is_array($ClassArchivedNo) && sizeof($ClassArchivedNo)>0)
		{
			foreach($ClassArchivedNo as $YearOfLeft => $TotalStudent)
			{
				if ($UploadedYearArr[$YearOfLeft]>0)
				{
					$ClassArchivedCount++;
					if($ParPageInfoArr[0] == "" || ($count >= $FirstRow && $count < $LastRow))
					{
						$RowStyle = ($count%2==0) ? "tablerow1" : "tablerow2";
						$SummaryTable .= "<tr class='".$RowStyle."'>";
						$SummaryTable .= "<td align='center' valign='top' class='tabletext'>".($count+1)."</td>";
						$SummaryTable .= "<td align='center' valign='top' class='tabletext'>".$ec_iPortfolio['student_left']." (".$YearOfLeft.")</td>";
						$SummaryTable .= "<td align='center' valign='top' class='tabletext'>".($UploadedYearArr[$YearOfLeft]==""?0:$UploadedYearArr[$YearOfLeft])."/".$TotalStudent."</td>";
						$SummaryTable .= (true || $UploadedYearArr[$YearOfLeft]=="") ? "<td>&nbsp;</td>" : "<td align='center'><a href=\"javascript:jREMOVE_PHOTO('UNKNOWN')\"><img src='".$image_path."/".$LAYOUT_SKIN."/icon_delete.gif' width='12' height='12' border='0' align='absmiddle'></a></td>";
						$SummaryTable .= "</tr>";
					}
					$count++;
				}
			}
		}
		
		$UnknownPhotoNum = $total_photo_num-$PhotoCount;
		
		if($UnknownPhotoNum>0)
		{
			if($ParPageInfoArr[0] == "" || ($count >= $FirstRow && $count < $LastRow))
			{
				$RowStyle = ($count%2==0) ? "tablerow1" : "tablerow2";
				$SummaryTable .= "<tr class='".$RowStyle."'>";
				$SummaryTable .= "<td align='center' valign='top' class='tabletext'>&nbsp;</td>";
				$SummaryTable .= "<td align='center' valign='top' class='tabletext'>".$ec_iPortfolio['unknown_photo']."</td>";
				$SummaryTable .= "<td align='center' valign='top' class='tabletext'>".$UnknownPhotoNum."</td>";
				$SummaryTable .= "<td align='center'><a href=\"javascript:jREMOVE_PHOTO('UNKNOWN')\"><img src='".$image_path."/".$LAYOUT_SKIN."/icon_delete.gif' width='12' height='12' border='0' align='absmiddle'></a></td>";
				$SummaryTable .= "</tr>";
			}
	//<input type='button' name='delete' value='$button_remove' onClick=\"doRemove('UNKNOWN')\" class='button_g'>
		}
		$SummaryTable .= "</table>";

		return array(count($ClassActivatedNo)+$ClassArchivedCount, $SummaryTable);
	}
	
	# Get student count with uploaded photo
/*
	function GET_UPLOADED_PHOTO_STUDENTS($RegNoArr)
	{
		global $eclass_db, $intranet_db;

		if(count($RegNoArr) > 0)
		{
			# generate summary table

			$RegNoStr = implode(",", $RegNoArr);
			$sql =	"
								SELECT
									iu.ClassName,
									ps.WebSAMSRegNo,
									IF(INSTR('".$RegNoStr."', Replace(BINARY ps.WebSAMSRegNo,'#',''))<>0, 1, 0)
								FROM
									".$eclass_db.".PORTFOLIO_STUDENT as ps
								INNER JOIN
									".$intranet_db.".INTRANET_USER as iu
								ON
									ps.UserID = iu.UserID
								LEFT JOIN
									".$this->course_db.".usermaster as um
								ON
									ps.CourseUserID = um.user_id
								WHERE
									iu.UserID IS NOT NULL AND
									(um.status='' OR um.status IS NULL) AND
									ps.IsSuspend = 0
								ORDER BY
									iu.ClassName
							";
			$row = $this->returnArray($sql);

			$PhotoCount = 0;
			for($i=0; $i<sizeof($row); $i++)
			{
				list($ClassName, $WebSAMSRegNo, $Uploaded) = $row[$i];
				$ClassName = trim($ClassName);
				if($Uploaded==1)
				{
					$UploadedArr[$ClassName] = $UploadedArr[$ClassName] + 1;
					$PhotoCount++;
				}
			}

			# archived students
			$sql =	"
								SELECT
									iu.YearOfLeft,
									ps.WebSAMSRegNo,
									IF(INSTR('".$RegNoStr."', Replace(BINARY ps.WebSAMSRegNo,'#',''))<>0, 1, 0)
								FROM
									".$eclass_db.".PORTFOLIO_STUDENT as ps
								LEFT JOIN
									".$intranet_db.".INTRANET_ARCHIVE_USER as iu
								ON
									ps.UserID = iu.UserID
								LEFT JOIN
									".$this->course_db.".usermaster as um
								ON
									ps.CourseUserID = um.user_id
								WHERE
									iu.UserID IS NOT NULL AND
									(um.status='' OR um.status IS NULL) AND
									ps.IsSuspend = 0
								ORDER BY
									iu.ClassName
							";
			$row = $this->returnArray($sql);

			for($i=0; $i<sizeof($row); $i++)
			{
				list($YearOfLeft, $WebSAMSRegNo, $Uploaded) = $row[$i];
				$YearOfLeft = trim($YearOfLeft);
				if($Uploaded==1)
				{
					$UploadedYearArr[$YearOfLeft] = $UploadedYearArr[$YearOfLeft] + 1;
					$PhotoCount++;
				}
			}
		}
		return array($UploadedArr, $UploadedYearArr, $PhotoCount);
	}
*/
	function GET_UPLOADED_PHOTO_STUDENTS($RegNoArr)
	{
		global $eclass_db, $intranet_db;

		if(count($RegNoArr) > 0)
		{
			# generate summary table

			$RegNoStr = implode(",", $RegNoArr);
			$sql =	"
								SELECT
									ClassName,
									count(*)
								FROM
									".$intranet_db.".INTRANET_USER
								WHERE
								  WebSAMSRegNo IS NOT NULL AND
								  WebSAMSRegNo <> '' AND
									INSTR('".$RegNoStr."', Replace(BINARY WebSAMSRegNo,'#',''))<>0
								GROUP BY
								  ClassName
								ORDER BY
									ClassName
							";
			$row = $this->returnArray($sql);

			$PhotoCount = 0;
			for($i=0; $i<sizeof($row); $i++)
			{
				list($ClassName, $Uploaded) = $row[$i];
				$UploadedArr[$ClassName] = $Uploaded;
				
				$PhotoCount += $Uploaded;
			}

			# archived students
			$sql =	"
								SELECT
									iu.YearOfLeft,
									count(*)
								FROM
									".$intranet_db.".INTRANET_ARCHIVE_USER as iu
								WHERE
									WebSAMSRegNo IS NOT NULL AND
								  WebSAMSRegNo <> '' AND
									INSTR('".$RegNoStr."', Replace(BINARY WebSAMSRegNo,'#',''))<>0
								GROUP BY
								  YearOfLeft
								ORDER BY
									YearOfLeft
							";
			$row = $this->returnArray($sql);

			for($i=0; $i<sizeof($row); $i++)
			{
				list($YearOfLeft, $Uploaded) = $row[$i];
				$YearOfLeft = trim($YearOfLeft);
				
				$UploadedYearArr[$YearOfLeft] = $Uploaded;
				$PhotoCount += $Uploaded;
		  }
		}
		return array($UploadedArr, $UploadedYearArr, $PhotoCount);
	}
	
	# Get archived student WebSAMS number with uploaded photo
	function GET_UPLOADED_PHOTO_ARCHIVE_STUDENTS_REGNO($ParRegNo)
	{
		global $eclass_db, $intranet_db;

		$RegNoList = "'".str_replace(",", "','", $ParRegNo)."'";
		$sql =	"
							SELECT
								REPLACE(ps.WebSAMSRegNo, '#', '')
							FROM
								".$eclass_db.".PORTFOLIO_STUDENT as ps
							LEFT JOIN
								".$intranet_db.".INTRANET_ARCHIVE_USER as iu
							ON
								ps.UserID = iu.UserID
							LEFT JOIN
								".$this->course_db.".usermaster as um
							ON
								ps.CourseUserID = um.user_id
							WHERE
								iu.UserID IS NOT NULL AND
								(um.status='' OR um.status IS NULL) AND
								ps.IsSuspend = 0 AND
								(SUBSTRING(ps.WebSAMSRegNo,2) IN ($RegNoList) OR
								ps.WebSAMSRegNo IN ($RegNoList))
							ORDER BY
								ps.WebSAMSRegNo
						";

		return $this->returnVector($sql);
	}
	
	# Count the number of archived student using iPortfolio
	function GET_CLASS_STUDENT_ARCHIVED(){
		global $eclass_db, $intranet_db;

		$sql =	"
							SELECT
								iu.YearOfLeft,
								COUNT(ps.RecordID) AS TotalActivated
							FROM
								{$intranet_db}.INTRANET_ARCHIVE_USER AS iu
							LEFT JOIN
								{$eclass_db}.PORTFOLIO_STUDENT AS ps
							ON
								ps.UserID = iu.UserID
							WHERE
								iu.RecordType = '2' AND
								ps.WebSAMSRegNo IS NOT NULL AND
								ps.UserKey IS NOT NULL AND
								ps.IsSuspend = 0
							GROUP BY
								iu.YearOfLeft
						";
		$row = $this->returnArray($sql);

		for ($i=0; $i<sizeof($row); $i++)
		{
			$ReturnArr[$row[$i]["YearOfLeft"]] = $row[$i]["TotalActivated"];
		}

		return $ReturnArr;
	}
	
	# Get student WebSAMS number with uploaded photo
	function GET_UPLOADED_PHOTO_STUDENTS_REGNO($ParClassName, $ParRegNo)
	{
		global $eclass_db, $intranet_db;

		$conds = ($ParClassName=="UNKNOWN") ? "" : " AND iu.ClassName = '$ParClassName' ";
		$RegNoList = "'".str_replace(",", "','", $ParRegNo)."'";
		$sql =	"
							SELECT
								REPLACE(ps.WebSAMSRegNo, '#', '')
							FROM
								".$eclass_db.".PORTFOLIO_STUDENT as ps
							INNER JOIN
								".$intranet_db.".INTRANET_USER as iu
							ON
								ps.UserID = iu.UserID
							LEFT JOIN
								".$this->course_db.".usermaster as um
							ON
								ps.CourseUserID = um.user_id
							WHERE
								iu.UserID IS NOT NULL AND
								(um.status='' OR um.status IS NULL) AND
								ps.IsSuspend = 0 AND
								(SUBSTRING(ps.WebSAMSRegNo,2) IN ($RegNoList) OR
								ps.WebSAMSRegNo IN ($RegNoList))
								$conds
							ORDER BY
								ps.WebSAMSRegNo
						";

		return $this->returnVector($sql);
	}

	# Generate parent information table for display
	function GEN_SELF_ACCOUNT_TABLE($ParUserID)
	{
		global $ec_student_word, $ec_iPortfolio, $no_record_msg;

		# Get self account of a student
		$self_account_arr = $this->GET_DEFAULT_SELF_ACCOUNT($ParUserID);
		if(is_array($self_account_arr))
		{
			foreach($self_account_arr AS $RecordID => $DefaultSA)
			{
				if(in_array("SLP", $DefaultSA))
					$self_account_default_arr = $this->GET_SELF_ACCOUNT($RecordID);
			}
		}

		$ReturnStr = "";
		$ReturnStr .= "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";
		if(isset($self_account_default_arr))
		{
			$ReturnStr .=	"
											<tr>
												<td>
													<table width='100%' border='0' cellspacing='6' cellpadding='0'>
														<tr>
															<td><span class=\"sub_page_title\">(".$self_account_default_arr['Title'].")</span></td>
														</tr>
														<tr>
															<td class=\"form_field_content\">".$self_account_default_arr["Details"]."</td>
														</tr>
													</table>
												</td>
											</tr>
										";
		}
		else
		{
			$ReturnStr .=	"
											<tr>
												<td>
													<table width='100%' border='0' cellspacing='0' cellpadding='0' height='100'>
														<tr height=60>
															<td colspan=3 align=center class=\"tabletext\">&nbsp;$no_record_msg</td>
														</tr>
													</table>
												</td>
											</tr>
										";
		}
		$ReturnStr .= "</table>";

		return $ReturnStr;
	}
	
	# Get current default self account setting
	function GET_DEFAULT_SELF_ACCOUNT($ParUserID)
	{
		global $eclass_db;

		$sql =	"
							SELECT
								RecordID,
								DefaultSA
							FROM
								{$eclass_db}.SELF_ACCOUNT_STUDENT
							WHERE
								UserID = '".$ParUserID."'
							ORDER BY
								InputDate DESC
						";
		$TempDefaultSAArr = $this->returnArray($sql);

		for($i=0; $i<count($TempDefaultSAArr); $i++)
		{
			$ReturnArr[$TempDefaultSAArr[$i]['RecordID']] = explode(",", $TempDefaultSAArr[$i]['DefaultSA']);
		}

		return $ReturnArr;
	}
	
	function displayYearSelectBox($StudentID, $ClassName,$ParChooseYear,$RecordType="")
    {
	    //$ChooseYear = $_Post['ChooseYear'];
	    //$ChooseYear = $ParChooseYear;
	    global $ActivityYearArr, $iPort;
	    
	    # get Years and ClassName according to ASSESSMENT_STUDENT_SUBJECT_RECORD
			
	    	if($RecordType==0)
	    	{
		    	$YearClassArr = $this->returnFilteredYearClass($StudentID, 0);
	    	}else if($RecordType==1)
	    	{
		    	$YearClassArr = $this->returnFilteredYearClass($StudentID, 1);
	    	}
	    	
				$x="<select name=ChooseYear class=formtextbox onChange='document.form1.submit()'>";
				
				if($PasChooseYear=="")
				{
					$x .= "<option value='' selected>".$iPort["all_school_years"]."</option>";
				}
				
				if(count($ActivityYearArr) != 0)
				{
					for($i=0; $i<sizeof($ActivityYearArr); $i++)
					{
						list($year) = $ActivityYearArr[$i];
						if($ParChooseYear  == $year)
						{
							 $x .= "<option value=\"".$year."\" selected>".$year."</option>";
						}else{
							$x .= "<option value=\"".$year."\">".$year."</option>";
						}
					}
					$x .= "</select>";	
						
					}else{
						if(is_array($YearClassArr))
						{
							foreach($YearClassArr as $year => $ClassName)
							{
	
								if($ParChooseYear  == $year)
								{
									 $x .= "<option value=\"".$year."\" selected>".$year."</option>";
								}else{
									$x .= "<option value=\"".$year."\">".$year."</option>";
								}
							}
						}
						$x .= "</select>";
					}
				return $x;
	}
	
	# get subject selection (for statistic)
	function getSubjectSelection($SubjectArr, $Tab, $SubjectCode, $SubjectComponentCode, $ShowAll=0, $ShowNoScoreSubject=0)
	{
		global $range_all;

		$ReturnSelection = "<SELECT ".$Tab.">";
		if($ShowAll)
		{
			$ReturnSelection .= "<OPTION value=''>".$range_all."</OPTION>";
		}

		for($i=0; $i<sizeof($SubjectArr); $i++)
		{
			list($s_code, $cmp_code, $s_name, $cmp_name) = $SubjectArr[$i];
			if($SubjectArr[$i]["have_score"] || $ShowNoScoreSubject==1)
			{
				if($cmp_code=="")
				{
					if($s_code==$SubjectCode)
					{
						$selected = "SELECTED";
						$ReturnSubjectName = $s_name;
					}
					else
						$selected = "";

					$ReturnSelection .= "<OPTION value='$s_code' $selected>$s_name</OPTION>";

				}
				else
				{
					if($cmp_code==$SubjectComponentCode && $s_code==$SubjectCode)
					{
						$selected = "SELECTED";
						$ReturnSubjectName = $s_name." ".$cmp_name;
					}
					else
						$selected = "";

					$ReturnSelection .= "<OPTION value='".$s_code."###".$cmp_code."' $selected>&nbsp;&nbsp;&nbsp;$cmp_name</OPTION>";
				}
			}
		}
		$ReturnSelection .= "</SELECT>";

		return array($ReturnSelection, $ReturnSubjectName);
	}

	function returnFilteredYearClass($StudentID, $RecordType=0, $Year="")
	{
		global $eclass_db, $intranet_db;

		$conds = ($Year!="") ? $this->getReportYearCondition($Year) : "";
		$sql = "SELECT DISTINCT
					Year,
					ClassName
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
				WHERE
					UserID = '{$StudentID}'
					$conds
				ORDER BY
					Year
				";
		$AssessmentResultArr = $this->returnArray($sql);
		for($i=0; $i<sizeof($AssessmentResultArr); $i++)
		{
			list($t_year, $t_classname) = $AssessmentResultArr[$i];
			$ReturnArray[trim($t_year)] = trim($t_classname);
		}

		$conds = ($Year!="") ? $this->getReportYearCondition($Year, "Academic") : "";
		$sql = "SELECT DISTINCT
					AcademicYear,
					ClassName
				FROM
					{$intranet_db}.PROFILE_CLASS_HISTORY
				WHERE
					UserID = '{$StudentID}'
					$conds
				ORDER BY
					AcademicYear
				";
		$AcademicResultArr = $this->returnArray($sql);
		for($i=0; $i<sizeof($AcademicResultArr); $i++)
		{
			list($t_year, $t_classname) = $AcademicResultArr[$i];
			if($ReturnArray[trim($t_year)]=="")
				$ReturnArray[trim($t_year)] = trim($t_classname);
		}

		$TableName = ($RecordType==1) ? "ATTENDANCE_STUDENT" : "MERIT_STUDENT";
		$conds = ($Year!="") ? $this->getReportYearCondition($Year) : "";
		$sql = "SELECT DISTINCT
					Year,
					ClassName
				FROM
					{$eclass_db}.{$TableName}
				WHERE
					UserID = '{$StudentID}'
					$conds
				ORDER BY
					Year
				";
		$RecordResultArr = $this->returnArray($sql);
		for($i=0; $i<sizeof($RecordResultArr); $i++)
		{
			list($t_year, $t_classname) = $RecordResultArr[$i];
			if($ReturnArray[trim($t_year)]=="")
				$ReturnArray[trim($t_year)] = trim($t_classname);
		}
		if(is_array($ReturnArray))
		{
		  # Eric Yip : Order year by class variable
		  if($this->YearDescending)
        krsort($ReturnArray);
      else
        ksort($ReturnArray);
		}

		return $ReturnArray;
	}
	
	# Display Student Merit Summary
	function displayStudentMeritSummary($StudentID, $ClassName, $PassChooseYear="", $ParChangeLayer=false)
    {
		global $ec_iPortfolio, $i_Profile_Absent, $i_Profile_Late, $i_Profile_EarlyLeave, $no_record_msg;

		# get the merit summary
		$result = $this->returnStudentMeritSummary($StudentID, $PassChooseYear, '', 'en');
		
		# get title info
        $title_array = $this->returnMeritReportTitleArray();
		$field_disabled_array = $this->returnMeritFieldAbility();
		$mtypeArr = $this->returnMeritTypeAbility();

		# get Years and ClassName according to ASSESSMENT_STUDENT_SUBJECT_RECORD
		$YearClassArr = $this->returnFilteredYearClass($StudentID, 0,$PassChooseYear);
/*
		# retrieve school semesters
		list($SemesterArr, $ShortSemesterArr) = $this->getSemesterNameArrFromIP();
		$SemesterArr[] = $ec_iPortfolio['whole_year'];
*/
		$TypeCount = 0;
	//	$x = "<table width='98%' border='0' cellpadding='0' cellspacing='0' class='table_b'>\n";
		$x = "<table width=100% border=0 cellpadding=4 cellspacing=0 bgcolor=#CCCCCC>\n";
		$x .= "<tr class='tabletop'>\n";
		//$x .= "<td width='20'>&nbsp;</td>";
		for ($i=0; $i<sizeof($title_array); $i++)
		{
			if (!$field_disabled_array[$i])
			{
				$td_style = ($i<=2) ? " width='150' align='left'" : " width='100' align='center'";
				if ($i>2)
				{
					if($i<8)
					{
                     	$summary_prefix_merit .= (($summary_prefix_merit!="")?"SePa":"") . $title_array[$i];
					}
					else
					{
						$summary_prefix_demerit .= (($summary_prefix_demerit!="")?"SePa":"") . $title_array[$i];
					}
                }
				$TypeCount++;
				$x .= "<td class='tabletopnolink' $td_style><b>".$title_array[$i]."</b></td>";
				//$td_style = ($i<=2) ? " width='150' align='left'" : " width='100' align='center'";
				//$td_style = ($i<=0) ? " width='550' align='left'" : " width='60' align='center'";
				/*
				if($i<=0)
				{
					$td_style =" width='550' align='left'";
					
				}else if($i>0 && $i <=2)
				{
					$td_style =" width='200' align='center'";
					
				}else
				{
					$td_style =" width='60' align='center'";
				}
				
				if ($i>2)
				{
					
					if($i<8)
					{
                     	$summary_prefix_merit .= (($summary_prefix_merit!="")?"SePa":"") . $title_array[$i];
					}
					else
					{
						$summary_prefix_demerit .= (($summary_prefix_demerit!="")?"SePa":"") . $title_array[$i];
					}
					
                }
				$TypeCount++;
				// The Report Title
				$x .= "<td class='tabletopnolink' $td_style><b>".$title_array[$i]."</b></td>";
				*/
			}
		}
        $x .= "</tr>\n";

        //Report Content
		if(!empty($YearClassArr))
		{
			$num = 0;

			foreach($YearClassArr as $year => $ClassName)
			{
				$CurrYear = "";
				
				# Eric Yip (20100211): Get semester array according to year name
				# (Not work for years with same name)
				unset($SemesterArr);
				
				$sql = "SELECT AcademicYearID FROM {$intranet_db}.ACADEMIC_YEAR WHERE YearNameEN = '".$year."'";
				$ay_id = current($this->returnVector($sql));
				
				// Must retrieve eng sem
				$SemesterArr = array();
				$SemesterDisplayArr = array();
				
				if($ay_id== ''){
					//for case , with year in the table:PROFILE_CLASS_HISTORY but without year id in t: ACADEMIC_YEAR 
				}else{
					//Year exist in the ACADEMIC_YEAR , get the related semesters
					
					$SemesterArr = array_values(getSemesters($ay_id, 1, 'en'));									
					// another set of sem arr for display only
					$SemesterDisplayArr = array_values(getSemesters($ay_id));				
				}
				$SemesterArr[] = $ec_iPortfolio['whole_year'];
				$SemesterDisplayArr[] = $ec_iPortfolio['whole_year'];
				
				for($j=0; $j<sizeof($SemesterArr); $j++)
				{
					$sem = trim($SemesterArr[$j]);
					if($year!=$CurrYear)
					{
						$bcolor = ($num%2!=0) ? "class=tablerow2" : "class=tablerow1";
						$tcolor = ($num%2!=0) ? "tablerow_total2" : "tablerow_total";
						$x .= "<tr $bcolor>\n";
						//$x .= "<td class=tabletext>&nbsp;</td>\n";
						$x .= "<td class=tabletext>$year</td>\n";
						$x .= "<td align=left class=tabletext>$ClassName</td>\n";
						$num++;
						$CurrYear = $year;
					}
					else
					{
						$x .= "<tr $bcolor>\n";
						//$x .= "<td class=tabletext>&nbsp;</td>\n";
						$x .= "<td  align=center class=tabletext>&nbsp;</td>\n";
						$x .= "<td  align=center class=tabletext>&nbsp;</td>\n";
					}
					//$stylecolor = ($num%2==0) ? "#FFFFFF" : "#DDDDDD";
					//$style = ($sem!=$ec_iPortfolio['whole_year']) ? "style='border-bottom: 1px dashed $stylecolor;'" : "";
					$style = ($sem!=$ec_iPortfolio['whole_year']) ? "class='tabletext tablerow_underline'" : "class='tabletext tablerow_underline $tcolor'";
					$summary_merit = $summary_prefix_merit;
					$summary_demerit = $summary_prefix_demerit;

					$ValueField = "";
					foreach($mtypeArr as $type => $valid)
					{
						if($valid==1)
						{
							$value = ($result[$year][$sem][$type]!='') ? $result[$year][$sem][$type] : 0;
							
							if($type>0)
							{
								$summary_merit .= "SePa".$value;
							}
							else
							{
								$summary_demerit .= "SePa".$value;
							}
							$ValueField .= "<td align='center' $style>".($sem==$ec_iPortfolio['whole_year'] ? "<strong>$value</strong>" : $value)."</td>\n";

							# get overall total
							if($sem==$ec_iPortfolio['whole_year'])
								$result[$ec_iPortfolio['total']][$type] = $result[$ec_iPortfolio['total']][$type] + $value;
						}
					}
					
					if($ParChangeLayer)
						$SemField = "<td align='left' $style><a href='#' onClick=\"jVIEW_DETAIL('merit', 'semester=".str_replace(array('"', "'"), array('&quot;', "\'"), $sem)."&year=".str_replace(array('"', "'"), array('&quot;', "\'"), $year)."&summary_merit=$summary_merit&summary_demerit=$summary_demerit')\" class='tablelink'>".($sem==$ec_iPortfolio['whole_year'] ? "<strong>$sem</strong>" : $sem)."</a></td>\n";
					else
						$SemField = "<td align='left' $style><a href='details.php?StudentID=$StudentID&ClassName=$ClassName&semester=".str_replace(array('"', "'"), array('\"', "\'"), $sem)."&year=".str_replace(array('"', "'"), array('\"', "\'"), $year)."&summary_merit=$summary_merit&summary_demerit=$summary_demerit' class='tablelink'>".($sem==$ec_iPortfolio['whole_year'] ? "<strong>$sem</strong>" : $sem)."</a></td>\n";

					$x .= $SemField.$ValueField."\n";
					$x .= "</tr>\n";
				}
			}
			//$bcolor = ($num%2!=0) ? "class=tablerow2" : "class=tablerow1";
			$bcolor = "class=tablebluebottom";
			$x .= "<tr $bcolor>\n";
			//$x .= "<td class=tabletext>&nbsp;</td>\n";
			$x .= "<td class=tabletext><strong>".$ec_iPortfolio['total']."</strong></td>\n";
			$x .= "<td class=tabletext>&nbsp;</td>\n";
			$x .= "<td class=tabletext>&nbsp;</td>\n";
			foreach($mtypeArr as $type => $valid)
			{
				if($valid==1)
				{
					$x .= "<td align='center' class='tabletext'><strong>".($result[$ec_iPortfolio['total']][$type]!='' ? $result[$ec_iPortfolio['total']][$type] : 0)."</strong></td>\n";
				}
			}
			$x .= "</tr>\n";
		}
		else
		{
			$x .= "<tr class=tablerow1 valign='middle'><td height='100' colspan='".($TypeCount+3)."' align='center' class=tabletext>".$no_record_msg."</td></tr>";
		}
		$x .= "</table>\n";

		return $x;
	}
	
	# Retrieve Student Merit Summary
	function returnStudentMeritSummary($StudentID, $Year="", $Semester="", $parLang='')
	{
		global $intranet_db, $eclass_db, $ec_iPortfolio, $intranet_root;

    include_once($intranet_root."/includes/libpf-sem-map.php");

		//$Year = $_Post['ChooseYear'];

		//$Year = '2004-2005';

		$conds = $this->getReportYearCondition($Year);

		# handle semester different problem
		//list($SemesterArr, $ShortSemesterArr) = $this->getSemesterNameArrFromIP();
		//list($SemList, $List1, $List2, $List3) = $this->getSemesterMatchingLists($Semester, $SemesterArr);
		if($Semester!="")
		{
			//$conds .= "AND (Semester = '$Semester' OR Semester IN ({$SemList}))";
			$conds .= "AND (ms.Semester = '$Semester' OR ayt.YearTermNameEN = '$Semester' OR ayt.YearTermNameB5 = '$Semester')";
		}
		
		if ($parLang == 'en') {
			$termNameField = 'YearTermNameEN';
		}
		else if ($parLang == 'b5' || $parLang == 'gb') {
			$termNameField = 'YearTermNameB5';
		}
		else {
			$termNameField = Get_Lang_Selection('YearTermNameB5', 'YearTermNameEN');
		}
		
/*
		# records grouping by year and semester
		$sql = "SELECT
					Year,
					if(INSTR('{$List1}', Semester),'".$SemesterArr[0]."',if(INSTR('{$List2}',  Semester),'".$SemesterArr[1]."',if(INSTR('{$List3}',  Semester),'".$SemesterArr[2]."', Semester))),
					RecordType,
					SUM(NumberOfUnit)
				FROM
					$eclass_db.MERIT_STUDENT
				WHERE
					UserID = '$StudentID'
					$conds
				GROUP BY
					Year,
					Semester,
					RecordType
				ORDER BY
					Year DESC,
					Semester DESC,
					RecordType DESC
				";
*/
		$sql = "SELECT
					ms.Year,
					ayt.$termNameField,
					ms.RecordType,
					SUM(ms.NumberOfUnit)
				FROM
					$eclass_db.MERIT_STUDENT AS ms
				LEFT JOIN $intranet_db.ACADEMIC_YEAR_TERM AS ayt
				  ON ms.YearTermID = ayt.YearTermID
				LEFT JOIN $intranet_db.ACADEMIC_YEAR AS ay
				  ON ms.AcademicYearID = ay.AcademicYearID
				WHERE
					ms.UserID = '$StudentID'
					$conds
				GROUP BY
					ms.Year,
					ms.Semester,
					ms.RecordType
				ORDER BY
					ms.Year DESC,
					ms.Semester DESC,
					ms.RecordType DESC
				";
		$row = $this->returnArray($sql);
		
		for($i=0; $i<sizeof($row); $i++)
		{
			list($year, $semester, $type, $count) = $row[$i];
			//$semester = SemesterTransform($semester);
			$tmp_result[$year][$semester][$type] = $count+0;

			# get semester total
			//$result[$year][$ec_iPortfolio['whole_year']][$type] = $result[$year][$ec_iPortfolio['whole_year']][$type] + $count;
			if ($semester!=$ec_iPortfolio['whole_year'])
			{
				$tmp_result[$year][$ec_iPortfolio['whole_year']][$type] +=  $count;
			}
		}

/*
    // Eric Yip (20100601): Skip normalizing merits, since the logic is differnt in IP25
    // Should follow logic in eDis, pending for implementation
		if(is_array($tmp_result))
		{
			foreach($tmp_result as $t_year => $t_arr1)
			{
				if(is_array($t_arr1))
				{
					foreach($t_arr1 as $t_sem => $t_arr2)
					{
						# Eric Yip : Normalize merits/ demerits
						$result[$t_year][$t_sem] = $this->returnNormalizedMerit($t_arr2);
					}
				}
			}
		}
		else
			$result = $tmp_result;
*/
    $result = $tmp_result;

		return $result;
	}
	
	# Normalize merit/demerit
	function returnNormalizedMerit($meritArr)
	{
		global $plugin, $intranet_db;

		if ($plugin['Discipline'] || $plugin['Disciplinev12'])
		{
			if(is_null($this->merit_array))
			{
				# Grab upgrade rule
				$sql = "SELECT UpgradeFromType, UpgradeNum, MeritType
				               FROM {$intranet_db}.DISCIPLINE_MERIT_TYPE_SETTING
				               WHERE MeritType IS NOT NULL AND MeritType != 0
				                     AND UpgradeFromType > 0
				                     AND UpgradeNum > 0
				               ORDER BY UpgradeFromType ASC";
				$temp = $this->returnArray($sql);
				for ($i=0; $i<sizeof($temp); $i++)
				{
					list($t_fromtype, $t_num, $t_type) = $temp[$i];
					$this->merit_array[$t_fromtype] = array($t_num, $t_type);
				}
			}
			if(is_null($this->demerit_array))
			{
				$sql = "SELECT UpgradeFromType, UpgradeNum, MeritType
				               FROM {$intranet_db}.DISCIPLINE_MERIT_TYPE_SETTING
				               WHERE MeritType IS NOT NULL AND MeritType != 0
				                     AND UpgradeFromType < 0
				                     AND UpgradeNum > 0
				               ORDER BY UpgradeFromType DESC";
				$temp = $this->returnArray($sql);
				for ($i=0; $i<sizeof($temp); $i++)
				{
				     list($t_fromtype, $t_num, $t_type) = $temp[$i];
				     $this->demerit_array[$t_fromtype] = array($t_num, $t_type);
				}
			}
			$mtypeArr = $this->returnMeritTypeAbility();

			if(is_array($meritArr))
			{
				$t_m_pt = isset($meritArr["1"])?$meritArr["1"]:0;
				$t_m_min = isset($meritArr["2"])?$meritArr["2"]:0;
				$t_m_maj = isset($meritArr["3"])?$meritArr["3"]:0;
				$t_m_sup = isset($meritArr["4"])?$meritArr["4"]:0;
				$t_m_ult = isset($meritArr["5"])?$meritArr["5"]:0;
				$t_d_pt = isset($meritArr["-1"])?$meritArr["-1"]:0;
				$t_d_min = isset($meritArr["-2"])?$meritArr["-2"]:0;
				$t_d_maj = isset($meritArr["-3"])?$meritArr["-3"]:0;
				$t_d_sup = isset($meritArr["-4"])?$meritArr["-4"]:0;
				$t_d_ult = isset($meritArr["-5"])?$meritArr["-5"]:0;

				##################################
				# Merit part
				##################################
				if ($t_m_pt != 0)
				{
					$temp = $this->merit_array[1];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_m_pt >= $r_num)
						{
							$t_m_pt -= $r_num;
							if($r_type==2 && $mtypeArr[$r_type])
							{
								$t_m_min++;
							}
							else if($r_type==3 && $mtypeArr[$r_type])
							{
								$t_m_maj++;
							}
							else if($r_type==4 && $mtypeArr[$r_type])
							{
								$t_m_sup++;
							}
							else if ($r_type==5 && $mtypeArr[$r_type])
							{
								$t_m_ult++;
							}
							else # Add back if no upgrade
							{
								$t_m_pt += $r_num;
								break;
							}
						}
					}
				}
				if ($t_m_min != 0)
				{
					$temp = $this->merit_array[2];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_m_min >= $r_num)
						{
							$t_m_min -= $r_num;
							if ($r_type==3 && $mtypeArr[$r_type])
							{
								$t_m_maj++;
							}
							else if ($r_type==4 && $mtypeArr[$r_type])
							{
								$t_m_sup++;
							}
							else if ($r_type==5 && $mtypeArr[$r_type])
							{
								$t_m_ult++;
							}
							else
							{
								$t_m_min += $r_num;
								break;
							}
						}
					}
				}
				if ($t_m_maj != 0)
				{
					$temp = $this->merit_array[3];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_m_maj >= $r_num)
						{
							$t_m_maj -= $r_num;
							if ($r_type==4 && $mtypeArr[$r_type])
							{
								$t_m_sup++;
							}
							else if ($r_type==5 && $mtypeArr[$r_type])
							{
								$t_m_ult++;
							}
							else
							{
								$t_m_maj += $r_num;
								break;
							}
						}
					}
				}
				if ($t_m_sup != 0)
				{
					$temp = $this->merit_array[4];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_m_sup >= $r_num)
						{
							$t_m_sup -= $r_num;
							if ($r_type==5 && $mtypeArr[$r_type])
							{
								$t_m_ult++;
							}
							else
							{
								$t_m_sup += $r_num;
								break;
							}
						}
					}
				}
				##################################
				# Demerit part
				##################################

				if ($t_d_pt != 0)
				{
					$temp = $this->demerit_array["-1"];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_d_pt >= $r_num)
						{
							$t_d_pt -= $r_num;
							if ($r_type==-2 && $mtypeArr[$r_type])
							{
								$t_d_min++;
							}
							else if ($r_type==-3 && $mtypeArr[$r_type])
							{
								$t_d_maj++;
							}
							else if ($r_type==-4 && $mtypeArr[$r_type])
							{
								$t_d_sup++;
							}
							else if ($r_type==-5 && $mtypeArr[$r_type])
							{
								$t_d_ult++;
							}
							else
							{
								$t_d_pt += $r_num;
								break;
							}
						}
					}
				}
				if ($t_d_min != 0)
				{
					$temp = $this->demerit_array["-2"];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_d_min >= $r_num)
						{
							$t_d_min -= $r_num;
							if ($r_type==-3 && $mtypeArr[$r_type])
							{
								$t_d_maj++;
							}
							else if ($r_type==-4 && $mtypeArr[$r_type])
							{
								$t_d_sup++;
							}
							else if ($r_type==-5 && $mtypeArr[$r_type])
							{
								$t_d_ult++;
							}
							else
							{
								$t_d_min += $r_num;
								break;
							}
						}
					}
				}
				if ($t_d_maj != 0)
				{
					$temp = $this->demerit_array["-3"];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_d_maj >= $r_num)
						{
							$t_d_maj -= $r_num;
							if ($r_type==-4 && $mtypeArr[$r_type])
							{
								$t_d_sup++;
							}
							else if ($r_type==-5 && $mtypeArr[$r_type])
							{
								$t_d_ult++;
							}
							else
							{
								$t_d_maj += $r_num;
								break;
							}
						}
					}
				}
				if ($t_d_sup != 0)
				{
					$temp = $this->demerit_array["-4"];
					if (is_array($temp) && sizeof($temp)==2)
					{
						list($r_num, $r_type) = $temp;
						while ($r_num > 0 && $t_m_sup >= $r_num)
						{
							$t_d_sup -= $r_num;
							if ($r_type==-5 && $mtypeArr[$r_type])
							{
								$t_d_ult++;
							}
							else
							{
								$t_d_sup += $r_num;
								break;
							}
						}
					}
				}
				##################################

				if($t_m_pt > 0)
					$result["1"] = $t_m_pt;
				if($t_m_min > 0)
					$result["2"] = $t_m_min;
				if($t_m_maj > 0)
					$result["3"] = $t_m_maj;
				if($t_m_sup > 0)
					$result["4"] = $t_m_sup;
				if($t_m_ult > 0)
					$result["5"] = $t_m_ult;

				if($t_d_pt > 0)
					$result["-1"] = $t_d_pt;
				if($t_d_min > 0)
					$result["-2"] = $t_d_min;
				if($t_d_maj > 0)
					$result["-3"] = $t_d_maj;
				if($t_d_sup > 0)
					$result["-4"] = $t_d_sup;
				if($t_d_ult > 0)
					$result["-5"] = $t_d_ult;
			}
			else
				$result = $meritArr;
		}
		else
			$result = $meritArr;

		return $result;
	}
	
	function returnActivityRecord($StudentID, $Year="", $Semester="")
	{
		global $eclass_db,$Lang;

		$conds = $this->getReportYearCondition($Year, "a.");
		$conds .= ($Semester!="") ? " AND a.Semester = '$Semester'" : "" ;
		$sql = "SELECT
					a.Year,
					if((a.Semester='' OR  a.Semester IS NULL), '".$Lang['General']['WholeYear']."', a.Semester) as `Semester`,
					a.ActivityName,
					if(a.Role='', '--', a.Role) as Role,
					if(a.Performance='', '--', a.Performance) as Performance,
					a.ModifiedDate
				FROM
					{$eclass_db}.ACTIVITY_STUDENT as a
				WHERE
					a.UserID = '$StudentID'
					$conds
				";

    # Eric Yip : Sort records
		if($this->YearDescending)
      $x[] = "a.Year DESC";
    if($this->SemesterAscending)
      $x[] = "a.Semester ASC";
    if(is_array($x) && count($x) > 0)
      $sql .= "
                ORDER BY
              ".implode(", ", $x);

		$row = $this->returnArray($sql);

		return $row;
	}
	
	function returnAwardRecord($StudentID, $Year="", $Semester="")
	{
		global $eclass_db,$Lang;

		$conds = $this->getReportYearCondition($Year, "a.");
		$conds .= ($Semester!="") ? " AND a.Semester = '$Semester'" : "" ;
		$sql = "SELECT
					a.Year,
					if((a.Semester='' OR  a.Semester IS NULL), '".$Lang['General']['WholeYear']."', a.Semester) as `Semester`,
					IF (a.AwardDate,DATE_FORMAT(a.AwardDate,'%Y-%m-%d'),'--') As AwardDate,
					a.AwardName,
					if(a.Remark='', '--', a.Remark) as Remark,
					a.ModifiedDate
				FROM
					{$eclass_db}.AWARD_STUDENT as a
				WHERE
					a.UserID = '$StudentID'
					AND a.RecordStatus = '2'
					$conds
              ";

    # Eric Yip : Sort records
		if($this->YearDescending)
      $x[] = "a.Year DESC";
    if($this->SemesterAscending)
      $x[] = "a.Semester ASC";
    if(is_array($x) && count($x) > 0)
      $sql .= "
                ORDER BY
              ".implode(", ", $x);

		$row = $this->returnArray($sql);

		return $row;
	}
	
	function returnCommentRecord($StudentID, $Year="", $Semester="")
	{
		global $eclass_db, $ec_iPortfolio, $intranet_db;

		$ClassNumberField1 = getClassNumberField("b.");
		$ClassNumberField2 = getClassNumberField("c.");
		$conds = $this->getReportYearCondition($Year, "a.");
		$conds .= ($Semester!="") ? " AND a.Semester = '$Semester'" : "" ;
		$sql = "SELECT DISTINCT
					  a.Year,
					  if((a.Semester='' OR  a.Semester IS NULL), '".$ec_iPortfolio['overall_comment']."', a.Semester) AS Semester,
					  if(b.ClassName IS NULL, IF(c.ClassName IS NULL, '--', CONCAT(c.ClassName, ' - ', $ClassNumberField2)), CONCAT(b.ClassName, ' - ', $ClassNumberField1)) AS ClassAndNumber,
					  a.ConductGradeChar,
					  a.CommentChi,
					  a.CommentEng,
					  a.ModifiedDate
				FROM
					{$eclass_db}.CONDUCT_STUDENT as a
					LEFT JOIN {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD as b ON (a.UserID = b.UserID AND a.Year = b.Year AND a.Semester = b.Semester)
					LEFT JOIN {$intranet_db}.PROFILE_CLASS_HISTORY as c ON (a.UserID = c.UserID AND (a.Year = c.AcademicYear OR INSTR(c.AcademicYear, CONCAT('-', a.Year)) > 0))
				WHERE
					a.UserID = '$StudentID'
					$conds
              ";

    # Eric Yip : Sort records
		if($this->YearDescending)
      $x[] = "a.Year DESC";
    if($this->SemesterAscending)
      $x[] = "a.Semester ASC";
    if(is_array($x) && count($x) > 0)
      $sql .= "
                ORDER BY
              ".implode(", ", $x);

		$row = $this->returnArray($sql);

		return $row;
	}
	
	# Retrieve Student Attendance Summary
	function returnStudentAttendanceSummary($studentid, $Year="", $Semester="")
	{
		global $eclass_db, $ec_iPortfolio, $intranet_root;

    include_once($intranet_root."/includes/libpf-sem-map.php");
		include("$intranet_root/includes/libstudentprofile.php");
        $lstudentprofile = new libstudentprofile();

		$conds = $this->getReportYearCondition($Year);

		# handle semester different problem
		//list($SemesterArr, $ShortSemesterArr) = $this->getSemesterNameArrFromIP();
		//list($SemList, $List1, $List2, $List3) = $this->getSemesterMatchingLists($Semester, $SemesterArr);
		if($Semester!="")
		{
			//$conds .= "AND (Semester = '$Semester' OR Semester IN ({$SemList}))";
			$conds .= "AND (ats.Semester = '$Semester' OR ayt.YearTermNameEN = '$Semester' OR ayt.YearTermNameB5 = '$Semester')";
		}

		# records grouping by year and semester
/*		
		$sql = "SELECT
					Year,
					if(INSTR('{$List1}', Semester),'".$SemesterArr[0]."',if(INSTR('{$List2}',  Semester),'".$SemesterArr[1]."',if(INSTR('{$List3}',  Semester),'".$SemesterArr[2]."', Semester))),
					RecordType,
					DayType
				FROM
					{$eclass_db}.ATTENDANCE_STUDENT
				WHERE
					UserID = '$studentid'
					$conds
				ORDER BY
					Year DESC, Semester ASC, RecordType, DayType DESC
				";
*/
		$sql = "SELECT
					ats.Year,
					ayt.YearTermNameEN,
					ats.RecordType,
					ats.DayType
				FROM
					{$eclass_db}.ATTENDANCE_STUDENT AS ats
				LEFT JOIN $intranet_db.ACADEMIC_YEAR_TERM AS ayt
				  ON ats.YearTermID = ayt.YearTermID
				LEFT JOIN $intranet_db.ACADEMIC_YEAR AS ay
				  ON ats.AcademicYearID = ay.AcademicYearID
				WHERE
					ats.UserID = '$studentid'
					$conds
				ORDER BY
					ats.Year DESC,
					ats.Semester DESC,
					ats.RecordType DESC
				";
		$row = $this->returnArray($sql);
		
		for($i=0; $i<sizeof($row); $i++)
		{
			list($year, $semester, $type, $day_type) = $row[$i];
			//$semester = SemesterTransform($semester);
			$add_value = ($type==1 && $day_type>1) ? 0.5 : 1;
			$add_value = $lstudentprofile->attendance_count_method ? $add_value : 1;

			$result[$year][$semester][$type] = $result[$year][$semester][$type] + $add_value;

			# get semester total
			$result[$year][$ec_iPortfolio['whole_year']][$type] = $result[$year][$ec_iPortfolio['whole_year']][$type] + $add_value;
		}
		return $result;
	}
	
	function returnActivityAttendanceSummary($studentid, $Year="", $Semester=""){
		global $ec_iPortfolio, $intranet_db;
		$sql = "SELECT s.UserID, s.Year, s.Semester, ea.RecordStatus 
				FROM $intranet_db.PROFILE_STUDENT_ACTIVITY s
				LEFT JOIN $intranet_db.INTRANET_ENROL_EVENTINFO  e ON e.EnrolEventID=s.eEnrolRecordID
				LEFT JOIN $intranet_db.INTRANET_ENROL_EVENT_ATTENDANCE ea ON ea.StudentID=s.UserID AND ea.EnrolEventID=e.EnrolEventID
				INNER JOIN $intranet_db.INTRANET_ENROL_EVENT_DATE d ON ea.EventDateID=d.EventDateID
				WHERE s.UserID='".$studentid."'";
		$conds = $this->getReportYearCondition($Year, "s.");
		if($Semester!="")
		{
			$conds .= "AND (s.Semester = '$Semester')";
		}
		$row = $this->returnArray($sql);
		for($i=0; $i<sizeof($row); $i++)
		{
			list($userID, $year, $semester, $type) = $row[$i];
			//$semester = SemesterTransform($semester);
			$add_value = 1;
		
			$result[$year][$semester][$type] = $result[$year][$semester][$type] + $add_value;
		
			# get semester total
			$result[$year][$ec_iPortfolio['whole_year']][$type] = $result[$year][$ec_iPortfolio['whole_year']][$type] + $add_value;
		}
		return $result;
	}
	
	// Modified by Key (12-08-2008)
	# for number of late, absent and early leave counting
	# 1 - Absent, 2 - Late, 3 - Early Leave
	function getAttendanceCountingByYearSemester($StudentID, $year, $semester="", $SemList="")
	{
		global $eclass_db, $ec_iPortfolio, $intranet_root;

    include_once("$intranet_root/includes/libpf-sem-map.php");
		include("$intranet_root/includes/libstudentprofile.php");
        $lstudentprofile = new libstudentprofile();

		# handle semester different problem
		list($SemesterArr, $ShortSemesterArr) = $this->getSemesterNameArrFromIP();
		list($SemList, $List1, $List2, $List3) = $this->getSemesterMatchingLists($semester, $SemesterArr);
		
    # Semester Mapping
    $sem_map_arr = SemesterMapping();
    
    if(is_array($sem_map_arr))
    {
      foreach($sem_map_arr AS $tar_sem => $src_sem)
      {
        if($src_sem == $semester)
        {
          $cond_sem_arr[] = $tar_sem;
        }
      }
    }
    
    if(is_array($cond_sem_arr))
    {
      $cond_sem_str = implode("', '", $cond_sem_arr);
    }

		$conds = "";
		if($semester != "" && $SemList != "")
		{
			$conds = ($semester==$ec_iPortfolio['whole_year']) ? "" : "AND (Semester IN ('$cond_sem_str') OR Semester IN ({$SemList}))";
		}
		else if($semester != "")
		{
			$conds = ($semester==$ec_iPortfolio['whole_year']) ? "" : "AND (Semester IN ('$cond_sem_str'))";
		}

		//$sql = "SELECT RecordType, COUNT(*) FROM $eclass_db.ATTENDANCE_STUDENT WHERE UserID = '$StudentID' AND Year = '$year' $conds GROUP BY RecordType ORDER BY RecordType ASC";
		//$row = $this->returnArray($sql, 2);

		$countArr[1] = 0;
		$countArr[2] = 0;
		$countArr[3] = 0;

		/*
		for($i=0; $i<sizeof($row); $i++)
		{
			list($type, $count) = $row[$i];

			$countArr[$type] = $count;
		}
		*/

		# records grouping by year and semester
		$sql = "SELECT
					Year,
					if(INSTR('{$List1}', Semester),'".$SemesterArr[0]."',if(INSTR('{$List2}',  Semester),'".$SemesterArr[1]."',if(INSTR('{$List3}',  Semester),'".$SemesterArr[2]."', Semester))),
					RecordType,
					DayType
				FROM
					{$eclass_db}.ATTENDANCE_STUDENT
				WHERE
					UserID = '$StudentID'
					$conds
				ORDER BY
					Year DESC, Semester ASC, RecordType, DayType DESC
				";
		$row = $this->returnArray($sql);


		for($i=0; $i<sizeof($row); $i++)
		{
			list($tmpYear, $tmpSemester, $type, $day_type) = $row[$i];
			$tmpSemester = SemesterTransform($tmpSemester);
			$add_value = ($type==1 && $day_type>1) ? 0.5 : 1;
			$add_value = $lstudentprofile->attendance_count_method ? $add_value : 1;

			$result[$tmpYear][$tmpSemester][$type] = $result[$tmpYear][$tmpSemester][$type] + $add_value;

			# get semester total
			$result[$tmpYear][$ec_iPortfolio['whole_year']][$type] = $result[$tmpYear][$ec_iPortfolio['whole_year']][$type] + $add_value;
		}

		for($type = 1; $type <= 3; $type++)
		{
			$countArr[$type] = $result[$year][$semester][$type];
		}

		return $countArr;
	}
	
	function getReportYearCondition($Year, $prefix="")
	{

		if($Year!="")
		{
			$SplitArray = explode("-", $Year);
			if(sizeof($SplitArray)>1)
			{
				$Year2 = trim($SplitArray[0]);
				$conds .= " AND (".$prefix."Year = '$Year' OR ".$prefix."Year = '$Year2')";
			}
			else
				$conds .= " AND ".$prefix."Year = '$Year'";
		}

		return $conds;
	}
/*
	# Get Semester Short Name and Full Name From IP
	function getSemesterNameArrFromIP()
	{
		global $intranet_root;

		$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));

		for($i=0; $i<sizeof($semester_data); $i++)
		{
			if ($semester_data[$i] != "")
			{
				$linedata = split("::",$semester_data[$i]);
				$sem = $linedata[0];
				$short_sem = preg_replace($this->semester_patterns, "", $linedata[0]);
				$semester_arr[] = trim($sem);
				$short_semester_arr[] = trim($short_sem);
			}
		}
		return array($semester_arr, $short_semester_arr);
	}
*/
	# function to match semester to current intranet semester
	function getSemesterMatchingLists($Semester, $SemesterArr)
	{
		global $ec_iPortfolio;

		$List1 = implode("|", $ec_iPortfolio['semester_name_array_1']);
		$List2 = implode("|", $ec_iPortfolio['semester_name_array_2']);
		$List3 = implode("|", $ec_iPortfolio['semester_name_array_3']);
		if($Semester==$SemesterArr[0])
		{
			$SemList = "'".implode("', '", $ec_iPortfolio['semester_name_array_1'])."'";
		}
		else if($Semester==$SemesterArr[1])
		{
			$SemList = "'".implode("', '", $ec_iPortfolio['semester_name_array_2'])."'";
		}
		else if($Semester==$SemesterArr[2])
		{
			$SemList = "'".implode("', '", $ec_iPortfolio['semester_name_array_3'])."'";
		}

		return array($SemList, $List1, $List2, $List3);
	}
	
	function returnMeritReportTitleArray()
	{
		global $ec_iPortfolio, $i_Merit_TypeArray;

		$title_array[] = $ec_iPortfolio['year'];
		$title_array[] = $ec_iPortfolio['class'];
		$title_array[] = $ec_iPortfolio['semester'];

        return array_merge($title_array,$i_Merit_TypeArray);
	}
	
	function returnMeritFieldAbility()
	{
		# Retrieve Merit Setting
		$this->retrieveMeritSettings();

		$pre_array = array(0,0,0);
		$merit_disabled_array = array($this->is_merit_disabled, $this->is_min_merit_disabled,
									$this->is_maj_merit_disabled, $this->is_sup_merit_disabled,
									$this->is_ult_merit_disabled, $this->is_black_disabled,
                                    $this->is_min_demer_disabled, $this->is_maj_demer_disabled,
                                    $this->is_sup_demer_disabled, $this->is_ult_demer_disabled);
		$field_disabled_array = array_merge($pre_array, $merit_disabled_array);

		return $field_disabled_array;
	}
	
	function retrieveMeritSettings()
        {
                 global $intranet_root;

				 if ($this->LangID==null)
				{
					$LangID = get_file_content($intranet_root."/file/language.txt");
					$LangID += 0;

					$this->$LangID = $LangID;
				}

                 $setting_file = "$intranet_root/file/std_profile_settings.txt";
                 $content = get_file_content($setting_file);
                 $array = explode("\n",$content);

                 # Display settings
                 $this->is_frontend_attendance_hidden = ($array[0][0]==1);
                 $this->is_printpage_attendance_hidden = ($array[0][1]==1);
                 $this->is_frontend_merit_hidden = ($array[1][0]==1);
                 $this->is_printpage_merit_hidden = ($array[1][1]==1);
                 $this->is_frontend_service_hidden = ($array[2][0]==1);
                 $this->is_printpage_service_hidden = ($array[2][1]==1);
                 $this->is_frontend_activity_hidden = ($array[3][0]==1);
                 $this->is_printpage_activity_hidden = ($array[3][1]==1);
                 $this->is_frontend_award_hidden = ($array[4][0]==1);
                 $this->is_printpage_award_hidden = ($array[4][1]==1);

                 # Field using settings
                 $field_setting_file = "$intranet_root/file/std_profile_field.txt";
                 $content = get_file_content($field_setting_file);
                 $array = explode("\n",$content);
                 $this->is_merit_disabled = ($array[0][0]==1);
                 $this->is_min_merit_disabled = ($array[0][1]==1);
                 $this->is_maj_merit_disabled = ($array[0][2]==1);
                 $this->is_sup_merit_disabled = ($array[0][3]==1);
                 $this->is_ult_merit_disabled = ($array[0][4]==1);
                 $this->is_black_disabled = ($array[1][0]==1);
                 $this->is_min_demer_disabled = ($array[1][1]==1);
                 $this->is_maj_demer_disabled = ($array[1][2]==1);
                 $this->is_sup_demer_disabled = ($array[1][3]==1);
                 $this->is_ult_demer_disabled = ($array[1][4]==1);
                 $this->col_merit = 0;
                 $this->col_demerit = 0;

                 if (!$this->is_merit_disabled) $this->col_merit++;
                 if (!$this->is_min_merit_disabled) $this->col_merit++;
                 if (!$this->is_maj_merit_disabled) $this->col_merit++;
                 if (!$this->is_sup_merit_disabled) $this->col_merit++;
                 if (!$this->is_ult_merit_disabled) $this->col_merit++;
				 if (!$this->is_black_disabled) $this->col_demerit++;
                 if (!$this->is_min_demer_disabled) $this->col_demerit++;
                 if (!$this->is_maj_demer_disabled) $this->col_demerit++;
                 if (!$this->is_sup_demer_disabled) $this->col_demerit++;
                 if (!$this->is_ult_demer_disabled) $this->col_demerit++;
	}

	function returnMeritTypeAbility()
	{
		# Retrieve Merit Setting
		$this->retrieveMeritSettings();

		$mtypeArr['1'] = ($this->is_merit_disabled) ? 0 : 1;
        $mtypeArr['2'] = ($this->is_min_merit_disabled) ? 0 : 1;
        $mtypeArr['3'] = ($this->is_maj_merit_disabled) ? 0 : 1;
        $mtypeArr['4'] = ($this->is_sup_merit_disabled) ? 0 : 1;
        $mtypeArr['5'] = ($this->is_ult_merit_disabled) ? 0 : 1;
        $mtypeArr['-1'] = ($this->is_black_disabled) ? 0 : 1;
        $mtypeArr['-2'] = ($this->is_min_demer_disabled) ? 0 : 1;
        $mtypeArr['-3'] = ($this->is_maj_demer_disabled) ? 0 : 1;
        $mtypeArr['-4'] = ($this->is_sup_demer_disabled) ? 0 : 1;
        $mtypeArr['-5'] = ($this->is_ult_demer_disabled) ? 0 : 1;

		return $mtypeArr;
	}
	
	# Change 2D array to 1D with the first column as index
	function build_assoc_array($array, $emptyname="build_assoc_array_null")
	{
			 for ($i=0; $i<sizeof($array); $i++)
			 {
				  list($id,$data) = $array[$i];
				  $id = trim($id);
				  $data = trim($data);
				  if ($id == "")
				  {
					  $id = $emptyname;
				  }
				  $result[$id] = $data;
			 }
			 return $result;
	}
	
	function SET_ALUMNI_COOKIE_BACKUP(&$ck_is_alumni){
		$this->ck_alumni_bak = $ck_is_alumni;
		
		$ck_is_alumni = 1;
	}
	
	# Generate summary table of alumni number for each year for display
	# Todo: "School-based scheme Updated" rows not implemented
	function GEN_ALUMNI_YEAR_LIST_TABLE($ParPageInfoArr, $ParField=1, $ParOrder=1)
	{
		global $image_path, $ec_iPortfolio, $LAYOUT_SKIN, $no_record_msg, $i_Profile_DataLeftYear;
	
		$ReturnArr = $this->GET_ALUMNI_YEAR_LIST($ParField, $ParOrder);

		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		} 

		$ReturnStr =	"
										<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"1\" bgcolor=\"#CCCCCC\">\n
											<tr class=\"tabletop\">\n
												<td width=\"20\" class=\"tabletoplink\">#</td>\n
												<td width=\"25%\"><a href=\"javascript:jSORT_TABLE(0, ".($ParField==0?($ParOrder>0?-1:1):-1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort0','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$i_Profile_DataLeftYear." ".($ParField==0?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort0\" id=\"sort0\" valign=\"absbottom\" />":"")."</a></td>\n
												<td align=\"center\"><a href=\"javascript:jSORT_TABLE(1, ".($ParField==1?($ParOrder>0?-1:1):-1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort1','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['heading']['no_lp_active_stu']." ".($ParField==1?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort1\" id=\"sort1\" valign=\"absbottom\" />":"")."</a></td>\n
											</tr>\n
									";

		if(count($ReturnArr) > 0)
		{
			$i=0;
			foreach($ReturnArr as $Year => $NoAlumni)
			{
				if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
				{
					$RowClass = ($i%2==0)?"tablerow1":"tablerow2";
					$ReturnStr .=	"
													<tr class=\"".$RowClass."\">\n
														<td valign=\"top\" class=\"tabletext\">".($i+1)."</td>\n
														<td><a href=\"school_records_alumni_year.php?Year=".$Year."\" class=\"tablelink\">".$Year."</a></td>\n
														<td align=\"center\" class=\"tabletext\">".$NoAlumni."</td>\n
													</tr>
												";
				}
				$i++;
			}
		}
		else
		{
			$ReturnStr .=	"
											<tr>
												<td class=\"tabletext\" colspan=\"3\">&nbsp;$no_record_msg</td>
											</tr>
										";
		}
		
		$ReturnStr .=	"</table>\n";
		
		return array($ReturnStr, $ReturnArr);
	}
	
	# Count the number of alumni using iPortfolio in Year-base
	function GET_ALUMNI_YEAR_LIST($ParField=1, $ParOrder=1){
		global $eclass_db, $intranet_db;

		# Get data from db
		$sql =	"
							SELECT DISTINCT
								iau.YearOfLeft,
								COUNT(ps.RecordID) AS TotalActivated
							FROM
								{$eclass_db}.PORTFOLIO_STUDENT AS ps
							INNER JOIN
								{$intranet_db}.INTRANET_ARCHIVE_USER AS iau
							ON
								ps.UserID = iau.UserID
							WHERE
								iau.RecordType = '2' AND
								ps.WebSAMSRegNo IS NOT NULL AND
								ps.UserKey IS NOT NULL AND
								ps.IsSuspend = 0
							GROUP BY
								iau.YearOfLeft
							ORDER BY
						";

		$sql .=	$ParField == 0 ? " YearOfLeft" : " TotalActivated";
		$sql .= $ParOrder == 1 ? " ASC" : " DESC";
						
		$row = $this->returnArray($sql);

		# Organize data in an associative array
		for ($i=0; $i<sizeof($row); $i++)
		{
			$ReturnArr[$row[$i]["YearOfLeft"]] = $row[$i]["TotalActivated"];
		}

		return $ReturnArr;
	}
	
	function RELOAD_ALUMNI_COOKIE_BACKUP(&$ck_is_alumni){
		$ck_is_alumni = $this->ck_alumni_bak;
	}

	# Generate activated alumni table for display
	function GEN_ALUMNI_STUDENT_TABLE($ParPageInfoArr, $ParYear, $ParDisplayType, $ParSearchName="", $ParClassName="", $ParField=0, $ParOrder=1)
	{
		global $image_path, $intranet_session_language, $ec_iPortfolio, $LAYOUT_SKIN, $no_record_msg, $ck_function_rights;
	
		$RecPerRow = 5;
	
		# Get student list
		$StudentList = $this->GET_ALUMNI_LIST_DATA($ParYear, true, $ParSearchName, $ParClassName, $ParField, $ParOrder);
		
		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		} 
		
		$ReturnStr =	"<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">\n";

		if($ParDisplayType == "thumbnail")
		{
			// Use thumbnail as display style
			for($i=0; $i<count($StudentList); $i++)
			{
				if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
				{
					# Display five students per row
					if(($i-$FirstRow) % $RecPerRow == 0)
						$ReturnStr .= "<tr>\n";
						
					# Get student object for each student in the list
					//$stu_obj = $this->GET_STUDENT_OBJECT($StudentList[$i][0]);
					$stu_obj = $StudentList[$i];
					
          # Link of official photo
    			list($photo_filepath, $photo_url) = $this->GET_OFFICIAL_PHOTO($stu_obj["WebSAMSRegNo"]);

    			# Update link of photo
    			# 1. If official photo exists, use official photo
    			# 2. If no photo exists, dipslay "no photo" message
    			if (is_file($photo_filepath))
    			{
    				$stu_obj["PhotoLink"] = "<img src=".$photo_url." border='1' width='100' height='130' <!--ImageStyle-->>";
    				$stu_obj["PhotoURL"] = $photo_url;
    			}
    			else
    			{
    				$stu_obj["PhotoLink"] = "<i>".$ec_iPortfolio['student_photo_no']."</i>";
    				$stu_obj["PhotoURL"] = "";
    			}
					
					$PhotoStyle = ($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0) ? "on" : "off";
					$ActivateIcon = ($StudentList[$i][2] != "" && $StudentList[$i][3] != "") ? "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_active_ipf.gif\" width=\"16\" height=\"20\" border=\"0\" align=\"absmiddle\">" : "";

					# Set links for photo			
					if(strpos($stu_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $stu_obj['PhotoLink'] != "")
						$stu_obj['PhotoLink'] = "
																			<table border='0' cellspacing='0' cellpadding='0'>
																				<tr>
																					<td background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_01.gif' style='padding-left:10px; padding-top:10px;'>".str_replace("<!--ImageStyle-->", "", $stu_obj['PhotoLink'])."</td>
																					<td height='10' background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_02.gif'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' title='' width='10' height='10'></td>
																				</tr>
																				<tr>
																					<td height='10' background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_03.gif'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='10' height='10'></td>
																					<td height='10'><img src='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_04.gif' width='10' height='10'></td>
																				</tr>
																			</table>
																		";
					else 
						$stu_obj['PhotoLink'] = "
																			<table border='0' cellspacing='0' cellpadding='0'>
																				<tr>
																					<td background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_01.gif' style='padding-left:10px; padding-top:10px;'><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" /></td>
																					<td height='10' background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_02.gif'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' title='' width='10' height='10'></td>
																				</tr>
																				<tr>
																					<td height='10' background='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_03.gif'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='10' height='10'></td>
																					<td height='10'><img src='".$image_path."/".$LAYOUT_SKIN."/iPortfolio/photo_frame_".$PhotoStyle."_04.gif' width='10' height='10'></td>
																				</tr>
																			</table>
																		";

					$ReturnStr .=	"
													<td width=\"".floor(100/$RecPerRow)."%\" align=\"center\" valign=\"top\">\n
														<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n
															<tr>\n
																<td align=\"center\">".$stu_obj['PhotoLink']."</td>\n
															</tr>\n
															<tr>\n
																<td class=\"tabletext\">".($intranet_session_language=="b5"?$stu_obj['ChineseName']:$stu_obj['EnglishName'])." ".$ActivateIcon."</td>\n
															</tr>\n
															<tr>\n
																<td>\n
																	<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n
																		<tr>\n
																			<td valign=\"top\"><a href=\"javascript:jTO_INFO('".$stu_obj['UserID']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_stu_info.gif\" alt=\"".$ec_iPortfolio['heading']['student_info']."\" width=\"20\" height=\"20\" border=\"0\"></a></td>\n
												";
					$ReturnStr .=	($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0 && $this->HAS_SCHOOL_RECORD_VIEW_RIGHT())?"<td valign=\"top\"><a href=\"javascript:jTO_SR('".$stu_obj['UserID']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_school_record.gif\" alt=\"".$ec_iPortfolio['school_record']."\" width=\"20\" height=\"20\" border=\"0\"></a></td>\n":"<td>&nbsp;</td>";
					$StatusStr = ($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)?"<td valign=\"top\"><a href=\"javascript:jTO_LP('".$stu_obj['UserID']."')\" class=\"new_alert\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_learning_portfolio.gif\" alt=\"".$ec_iPortfolio['heading']['learning_portfolio']."\" width=\"20\" height=\"20\" border=\"0\"><!--Status--></a></td>\n":"";
					$ReturnStr .=	str_replace("<!--Status-->", "<br />".$PortfolioStatus[$StudentList[$i][0]], $StatusStr);
					$ReturnStr .=	"
																		</tr>\n
																	</table>\n
																	<a href=\"javascript:;\"></a><br><br>\n
																</td>\n
															</tr>\n
														</table>\n
													</td>\n
												";
						
					if(($i-$FirstRow) % $RecPerRow == 4 || $i + 1 == count($StudentList) || $i + 1 == $LastRow)
						$ReturnStr .= "</tr>\n";
				}
			}
			
			if(count($StudentList) == 0)
			{
				$ReturnStr .=	"
												<tr>
													<td class=\"tabletext\">&nbsp;$no_record_msg</td>
												</tr>
											";
			}
		}
		else if($ParDisplayType == "list")
		{
			// Use list as display style
			$ReturnStr .= "
											<tr class=\"tabletop\">
												<td><a href=\"javascript:jSORT_TABLE(3, ".($ParField==3?($ParOrder>0?-1:1):1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort3','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['class']." ".($ParField==3?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort0\" id=\"sort3\" valign=\"absbottom\" />":"")."</a></td>\n
												<td><a href=\"javascript:jSORT_TABLE(0, ".($ParField==0?($ParOrder>0?-1:1):1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort0','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['heading']['no']." ".($ParField==0?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort0\" id=\"sort0\" valign=\"absbottom\" />":"")."</a></td>\n
												<td align=\"center\" class=\"tabletoplink\">&nbsp;</td>
												<td><a href=\"javascript:jSORT_TABLE(1, ".($ParField==1?($ParOrder>0?-1:1):1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort1','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['heading']['student_name']." ".($ParField==1?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort1\" id=\"sort1\" valign=\"absbottom\" />":"")."</a></td>\n
												<td><a href=\"javascript:jSORT_TABLE(2, ".($ParField==2?($ParOrder>0?-1:1):1).")\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort2','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['heading']['student_regno']." ".($ParField==2?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder>0?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort2\" id=\"sort2\" valign=\"absbottom\" />":"")."</a></td>\n
												<td align=\"center\" class=\"tabletoplink\">".$ec_iPortfolio['heading']['student_info']."</td>
												<td align=\"center\" class=\"tabletoplink\">".$ec_iPortfolio['heading']['student_record']."</td>
												<td align=\"center\" class=\"tabletoplink\">".$ec_iPortfolio['heading']['learning_portfolio']."</td>
										";
			//$ReturnStr .= strstr($ck_function_rights, "Profile:Student")?"<td width=\"25\"><input type=checkbox name='checkmaster' onClick=(this.checked)?setChecked(1,document.form1,'user_id[]'):setChecked(0,document.form1,'user_id[]')></td>":"";
			$ReturnStr .= "
											</tr>
										";
		
			for($i=0; $i<count($StudentList); $i++)
			{
				if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
				{					
					# Get student object for each student in the list
					//$stu_obj = $this->GET_STUDENT_OBJECT($StudentList[$i][0]);
					$stu_obj = $StudentList[$i];
					
          # Link of official photo
    			list($photo_filepath, $photo_url) = $this->GET_OFFICIAL_PHOTO($stu_obj["WebSAMSRegNo"]);

    			# Update link of photo
    			# 1. If official photo exists, use official photo
    			# 2. If no photo exists, dipslay "no photo" message
    			if (is_file($photo_filepath))
    			{
    				$stu_obj["PhotoLink"] = "<img src=".$photo_url." border='1' width='100' height='130' <!--ImageStyle-->>";
    				$stu_obj["PhotoURL"] = $photo_url;
    			}
    			else
    			{
    				$stu_obj["PhotoLink"] = "<i>".$ec_iPortfolio['student_photo_no']."</i>";
    				$stu_obj["PhotoURL"] = "";
    			}
					
					# Set links for photo
/*					if(strpos($stu_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $stu_obj['PhotoLink'] != "")
						$stu_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "class=\"photo_border_on\"", $stu_obj['PhotoLink']);
					else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
						$stu_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" class=\"photo_border_on\" />";
					else
						$stu_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" class=\"photo_border\" />";
*/
					if(strpos($stu_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $stu_obj['PhotoLink'] != "")
//						$PhotoAction = "<a href=\"javascript:;\" class=\"contenttool\" onMouseOver=\"jSHOW_PHOTO('".trim(str_replace("#", "", $stu_obj['WebSAMSRegNo']))."')\" onMouseOut=\"jHIDE_PHOTO()\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/smartcard/icon_student_photo.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>";
						$PhotoAction = "<a href=\"javascript:;\" class=\"contenttool\" onMouseOver=\"jSHOW_PHOTO('".$stu_obj['PhotoURL']."')\" onMouseOut=\"jHIDE_PHOTO()\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/smartcard/icon_student_photo.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>";
					else
						$PhotoAction = "&nbsp;";

					if($StudentList[$i][2] != "" && $StudentList[$i][3] != "")
					{
						$cell_style = ($StudentList[$i][4] == 0) ? "row_on" : "row_off";
					
						$ReturnStr .= "
														<tr class=\"tablerow1\">\n
															<td class=\"$cell_style tabletext\">".$stu_obj['ClassName']."</td>\n
															<td class=\"$cell_style tabletext\">".($stu_obj['ClassNumber']==""?"&nbsp;":$stu_obj['ClassNumber'])."</td>\n
															<td class=\"$cell_style\" align=\"center\">".$PhotoAction."</td>\n
															<td class=\"$cell_style tabletext\">".($intranet_session_language=="b5"?($stu_obj['ChineseName']==""?"&nbsp;":$stu_obj['ChineseName']):($stu_obj['EnglishName']==""?"&nbsp;":$stu_obj['EnglishName']))."</td>\n
															<td class=\"$cell_style tabletext\">".$stu_obj['WebSAMSRegNo']." <img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_active_ipf.gif\" width=\"16\" height=\"20\" border=\"0\" align=\"absmiddle\"></td>\n
															<td class=\"$cell_style\" align=\"center\"><a href=\"javascript:jTO_INFO('".$stu_obj['UserID']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_stu_info.gif\" alt=\"".$ec_iPortfolio['heading']['student_info']."\" width=\"20\" height=\"20\" border=\"0\"></a></td>\n
													";
						$ReturnStr .=	($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0 && $this->HAS_SCHOOL_RECORD_VIEW_RIGHT())?"<td class=\"$cell_style\" align=\"center\"><a href=\"javascript:jTO_SR('".$stu_obj['UserID']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_school_record.gif\" alt=\"".$ec_iPortfolio['school_record']."\" width=\"20\" height=\"20\" border=\"0\"></a></td>\n":"<td class=\"$cell_style\">&nbsp;</td>";
						$StatusStr = ($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)?"<td class=\"$cell_style\" valign=\"top\" align=\"center\"><a href=\"javascript:jTO_LP('".$stu_obj['UserID']."')\" class=\"new_alert\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_learning_portfolio.gif\" alt=\"".$ec_iPortfolio['heading']['learning_portfolio']."\" width=\"20\" height=\"20\" border=\"0\"><!--Status--></a></td>\n":"<td class=\"$cell_style\">&nbsp;</td>";
						$ReturnStr .=	str_replace("<!--Status-->", $PortfolioStatus[$StudentList[$i][0]], $StatusStr);
						//$ReturnStr .= strstr($ck_function_rights, "Profile:Student")?"<td class=\"$cell_style\"><input type=\"checkbox\" name=\"user_id[]\" value=\"".$stu_obj['UserID']."\"></td>":"";
						$ReturnStr .=	"</tr>\n";
					}
					else
					{
						$ReturnStr .= "
														<tr class=\"tablerow1\">\n
															<td class=\"row_off tabletext\">".$stu_obj['ClassName']."</td>\n
															<td class=\"row_off tabletext\" >".($stu_obj['ClassNumber']==""?"&nbsp;":$stu_obj['ClassNumber'])."</td>\n
															<td class=\"row_off\" align=\"center\">".$PhotoAction."</td>\n
															<td class=\"row_off tabletext\">".($intranet_session_language=="b5"?$stu_obj['ChineseName']:$stu_obj['EnglishName'])."</td>\n
															<td class=\"row_off tabletext\" >".($stu_obj['WebSAMSRegNo']==""?"&nbsp;":$stu_obj['WebSAMSRegNo'])."</td>\n
															<td class=\"row_off\" align=\"center\"><a href=\"javascript:jTO_INFO('".$stu_obj['UserID']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_stu_info.gif\" alt=\"".$ec_iPortfolio['heading']['student_info']."\" width=\"20\" height=\"20\" border=\"0\"></a></td>\n
													";
						$ReturnStr .=	($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0 && $this->HAS_SCHOOL_RECORD_VIEW_RIGHT())?"<td class=\"row_off\" align=\"center\"><a href=\"javascript:jTO_SR('".$stu_obj['UserID']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_school_record.gif\" alt=\"".$ec_iPortfolio['school_record']."\" width=\"20\" height=\"20\" border=\"0\"></a></td>\n":"<td class=\"row_off\">&nbsp;</td>";
						$ReturnStr .=	"<td class=\"row_off\">&nbsp;</td>";
						//$ReturnStr .=	strstr($ck_function_rights, "Profile:Student")?"<td class=\"row_off\"><input type=\"checkbox\" name=\"user_id[]\" value=\"".$stu_obj['UserID']."\"></td>":"";
						$ReturnStr .=	"</tr>\n";
					}
				}
			}
			
			if(count($StudentList) == 0)
			{
				$ReturnStr .=	"
												<tr>
													<td colspan=\"8\" class=\"tabletext\">&nbsp;$no_record_msg</td>
												</tr>
											";
			}
		}
		
		$ReturnStr .= "</table>\n";
		
		return array($ReturnStr, $StudentList, $PortfolioStatus);
	}
	
	# Get student list by Class Name
	function GET_ALUMNI_LIST_DATA($ParYear="", $ParActivated=true, $ParSearchName="", $ParClassName="", $ParField=0, $ParOrder=1){
		global $intranet_db, $eclass_db, $intranet_session_language;

		$conds[] = "iau.RecordType = '2'";

		if($ParClassName != "")
			$conds[] = "iau.ClassName='$ParClassName'";
			
		if($ParYear != "")
			$conds[] = "iau.YearOfLeft='$ParYear'";

		$conds[] = "ps.WebSAMSRegNo IS NOT NULL";
		$conds[] = "ps.UserKey IS NOT NULL";
		$conds[] = "ps.IsSuspend = 0";

		if($ParSearchName != "")
		{
			$conds[] =	"
										(iau.ChineseName LIKE '%".addslashes($ParSearchName)."%' OR
										iau.EnglishName LIKE '%".addslashes($ParSearchName)."%')
									";
		}
		$conds = implode(" AND ", $conds);

		# Sorting criteria by parameter
		switch($ParField)
		{
			case 0:
				$displayBy = "ClassNumber ";
				break;
			case 1:
// 				if($intranet_session_language == "b5")
// 					$displayBy = "ChineseName ";
// 				else
					$displayBy = "EnglishName ";
				break;
			case 2:
				$displayBy = "WebSAMSRegNo ";
				break;
			case 3:
				$displayBy = "ClassName ";
				break;
		}
		if($ParOrder > 0)
			$displayBy .= "ASC";
		else
			$displayBy .= "DESC";
			
			// Modified by Key [2008-11-13] , add one order for classname
			if($ParField == 3)
			{
				$displayBy .= " ,ClassNumber ASC ";
			}


		# Get the field name for name and class number
		$namefield = getNameFieldByLang2("iau.");
		$ClassNumberField = getClassNumberField("iau.");

		# Get the data from db
		$sql = "	SELECT DISTINCT
								iau.UserID,
								CONCAT(IFNULL(CONCAT('(', iau.ClassName, ' - ', $ClassNumberField, ') '), ''), {$namefield}) as DisplayName,
								ps.WebSAMSRegNo,
								ps.UserKey,
								ps.IsSuspend,
								iau.ChineseName,
								iau.EnglishName,
								iau.ClassName,
								$ClassNumberField AS ClassNumber
							FROM
								{$eclass_db}.PORTFOLIO_STUDENT AS ps
							INNER JOIN
								{$intranet_db}.INTRANET_ARCHIVE_USER AS iau
							ON
								ps.UserID = iau.UserID
							WHERE
								$conds
							ORDER BY
								$displayBy
						";

		return $this->returnArray($sql);
	}
	
	# Generate selection list for alumni year, which have activated students
	function GEN_ALUMNI_YEAR_SELECTION($ParYear="")
	{
		global $ec_iPortfolio;
	
		$AlumniYearTemp = $this->GET_ALUMNI_YEAR_LIST(0);
		
		if(is_array($AlumniYearTemp))
		{
			foreach($AlumniYearTemp as $Year => $NoStudent)
			{
				$AlumniYear[] = array($Year, $Year);
			}
		
			$ReturnStr = getSelectByArrayTitle($AlumniYear, "name='Year' id='Year' class='formtextbox' onChange='jCHANGE_FIELD(\"year\")'", $ec_iPortfolio['school_yr'], $ParYear, false, 2);
		}
		
		
		
		return $ReturnStr;
	}
	
	# Generate selection list for classes, which have activated students
	function GEN_ALUMNI_CLASS_SELECTION($ParYear="", $ParClassName="")
	{
		global $i_alert_pleaseselect, $i_general_WholeSchool, $Lang;
	
		# Get classes with activated students
		$AlumniClass = $this->GET_ALUMNI_CLASS_LIST($ParYear);
		# Reorganize array for generating selection list
		$selectOptionArr = array();
		$areadyShowNoClassOption = false;
		for($i=0; $i<count($AlumniClass); $i++)
		{
			if(!empty($AlumniClass[$i])){
				$selectOptionArr[] = array($AlumniClass[$i], $AlumniClass[$i]);
			}else{
				if(!$areadyShowNoClassOption){
					$areadyShowNoClassOption = true;
					$selectOptionArr[] = array(-1, $Lang['General']['NoClass']);
				}
			}
			
		}
		
		$ReturnStr = getSelectByArrayTitle($selectOptionArr, "name='ClassName' id='ClassName' class='formtextbox' onChange='jCHANGE_FIELD(\"classname\")'", $i_general_WholeSchool, $ParClassName, false, 2);
		
		return $ReturnStr;
	}

	# Count the number of alumni using iPortfolio in Year-base
	function GET_ALUMNI_CLASS_LIST($ParYear=""){
		global $eclass_db, $intranet_db;

		if($ParYear != "")
			$cond =  "AND iau.YearOfLeft = '".$ParYear."'";
		
		# Get data from db
		$sql =	"
							SELECT DISTINCT
								iau.ClassName
							FROM
								{$eclass_db}.PORTFOLIO_STUDENT AS ps
							INNER JOIN
								{$intranet_db}.INTRANET_ARCHIVE_USER AS iau
							ON
								ps.UserID = iau.UserID
							WHERE
								iau.RecordType = '2' AND
								ps.WebSAMSRegNo IS NOT NULL AND
								ps.UserKey IS NOT NULL AND
								ps.IsSuspend = 0
								$cond
							ORDER BY
								iau.ClassName
						";

		$ReturnArr = $this->returnVector($sql);

		return $ReturnArr;
	}
	
	# Retrieve all Subject Studied in Proper Order
	function returnAllSubjects($StudentID)
	{
		global $eclass_db, $intranet_db;

		# Retrieve all Subject Studied in Proper Order
		$sql = "SELECT
					DISTINCT SubjectCode,
					".$this->returnSqlFieldLang("ABBR","SUBJ")." AS SubjectName
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD AS ASSR
					LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS SUBJ ON SUBJ.EN_SNAME=ASSR.SubjectCode
				WHERE
					UserId='$StudentID'
					And SUBJ.RecordStatus = 1 
				ORDER
					BY SUBJ.DisplayOrder
			";
		$row = $this->returnArray($sql);

		for($i=0; $i<sizeof($row); $i++)
		{
			$row[$i]["have_score"] = $this->checkSubjectScoreExist($row[$i]["SubjectCode"], "");
		}

		return $row;
	}
	
	# Generate Assessment Report - Full Report
	function generateAssessmentReport3($StudentID, $ClassName="", $displayBy="", $displayType="", $cmp_show="", $Year="", $Semester="", $Printable="", $ParChangeLayer=false)
	{
		global $intranet_db, $eclass_db, $ec_iPortfolio, $sr_image_path, $no_record_msg, $ec_iPortfolio_Report, $intranet_root, $ck_alumni_year;
		global $sys_custom;
		
		if ($sys_custom['iPf']['showAcademicResultWithAssessment']) {
			return $this->getStudentAcademicResultDisplayWithAssessment($StudentID, $displayBy, $hideStat=true);
		}
		
	include_once("form_class_manage.php");
	$ayt = new academic_year_term();
	
		$params = "ClassName=$ClassName&StudentID=$StudentID&my_year=".$ck_alumni_year;

		$valueShow = ($displayBy=="") ? "Score" : trim($displayBy);

		switch ($displayBy)
		{
			case "Score":
				$totalTitle = $ec_iPortfolio['overall_score'];
				break;
			case "Rank":
				$totalTitle = $ec_iPortfolio['overall_rank'];
				break;
			case "StandardScore":
				$totalTitle = $ec_iPortfolio['overall_stand_score'];
				break;
			default:
				$totalTitle = $ec_iPortfolio['overall_grade'];
				break;
		}

		# retrieve all subjects with components
		$row_cmp_subject = ($displayType=="Print" && $cmp_show!=1) ? $this->returnAllSubjects($StudentID) : $this->returnAllSubjectWithComponents($StudentID);
	
		# retrieve subject semester records
		$subject_record = $this->returnAssessmentResult($StudentID, 0, $Year, $Semester);

		if($Semester=="")
		{
			# retrieve subject annual records
			$annual_subject_record = $this->returnAssessmentResult($StudentID, 1, $Year);
		}

		# retrieve school semesters
		//list($full_semester_arr, $semester_arr) = $this->getSemesterNameArrFromIP();
		list($full_Eng_semester_arr, $semester_arr) = $this->getSemesterNameArrFromIP("",1);
// 		list($full_Eng_semester_arr, $semester_arr) = $this->getSemesterNameArrFromIP();
		if(sizeof($annual_subject_record)!=0)
		{
			$semester_arr[] = trim($ec_iPortfolio['overall_result']);
		}

		$class_arr = array();
		$year_arr = array();
		for ($i=0; $i<sizeof($subject_record); $i++)
		{
			$record_obj = $subject_record[$i];
//			hdebug_r($record_obj); // importance to open for debug

			# retrieve semester with AcademicYearID and YearTermID 
			//$sem = trim(preg_replace($this->semester_patterns, "", $record_obj["Semester"]));
			$sem = $ayt->Get_YearNameByID($record_obj["YearTermID"], $_SESSION['intranet_session_language']);
			$_year = trim($record_obj["Year"]);

			//for safe strtolower for compare (2012-1231-0844-30156)
			$sem = strtolower($sem);
			//$_year = strtolower($_year);

			# check if score=0, grade=null and rank=0, the subject was not selected by this student

//			$RAW[trim($record_obj["SubjectCode"])]["NotSelected"] = ($record_obj["Score"]==0 && $record_obj["Grade"]=="" && $record_obj["OrderMeritForm"]==0) ? 1 : 0;
//			$RAW[trim($record_obj["SubjectCode"])]["NoRank"] = ($displayBy=="Rank" && $record_obj["OrderMeritForm"]<=0) ? 1 : 0;
if($record_obj['SubjectComponentCode'] == ''){
	//which is a main subject
			$RAW[trim($record_obj["SubjectCode"])][$_year][$sem]["NotSelected"] = ($record_obj["Score"]==0 && $record_obj["Grade"]=="" && $record_obj["OrderMeritForm"]==0) ? 1 : 0;
			$RAW[trim($record_obj["SubjectCode"])][$_year][$sem]["NoRank"] = ($displayBy=="Rank" && $record_obj["OrderMeritForm"]<=0) ? 1 : 0;
}



			if($record_obj["SubjectComponentCode"]=="")
			{
				$RAW[trim($record_obj["SubjectCode"])][trim($record_obj["Year"])][$sem]["major"] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
			}
			else
			{
				$RAW[trim($record_obj["SubjectCode"])][trim($record_obj["Year"])][$sem][trim($record_obj["SubjectComponentCode"])] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
			}

			$class = trim($record_obj["ClassName"]);
			if (!in_array($record_obj["Year"], $year_arr))
			{
				$year_arr[] = trim($record_obj["Year"]);
				$class_arr[] = $class;
				$sem_arr[trim($record_obj["Year"])] = array();
			}

			if(!in_array(strtolower($sem), $sem_arr[$record_obj["Year"]]))
			{
				$sem_arr[trim($record_obj["Year"])][] = strtolower($sem);
			}
		}
		for ($i=0; $i<sizeof($annual_subject_record); $i++)
		{
			$record_obj = $annual_subject_record[$i];
			if($record_obj["SubjectComponentCode"]=="")
			{
				//$RAW[trim($record_obj["SubjectCode"])][trim($record_obj["Year"])][trim($ec_iPortfolio['overall_result'])]["major"] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
				$RAW[trim($record_obj["SubjectCode"])][trim($record_obj["Year"])][strtolower(trim($ec_iPortfolio['overall_result']))]["major"] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
				$class = trim($record_obj["ClassName"]);

				if(!in_array($record_obj["Year"], $year_arr))
				{
					$year_arr[] = trim($record_obj["Year"]);
					$class_arr[] = $class;
				}
			}
			else
			{
				//$RAW[trim($record_obj["SubjectCode"])][trim($record_obj["Year"])][trim($ec_iPortfolio['overall_result'])][trim($record_obj["SubjectComponentCode"])] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
				$RAW[trim($record_obj["SubjectCode"])][trim($record_obj["Year"])][strtolower(trim($ec_iPortfolio['overall_result']))][trim($record_obj["SubjectComponentCode"])] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
			}

			if(sizeof($sem_arr[$record_obj["Year"]])==0)
			{
// 				$sem_arr[trim($record_obj["Year"])][] = trim($ec_iPortfolio['overall_result']);
			    $sem_arr[trim($record_obj["Year"])][] = strtolower(trim($ec_iPortfolio['overall_result']));
			}
			else if(!in_array(strtolower(trim($ec_iPortfolio['overall_result'])), $sem_arr[$record_obj["Year"]]))
			{
				//$sem_arr[trim($record_obj["Year"])][] = trim($ec_iPortfolio['overall_result']);
				$sem_arr[trim($record_obj["Year"])][] = strtolower(trim($ec_iPortfolio['overall_result']));
			}
		}
/*
		if(is_array($year_arr))
			sort($year_arr);
		if(is_array($class_arr))
			sort($class_arr);

		# Eric Yip (20090706): Override IP semester array
		if(is_array($sem_arr))
		{
			$max_count = 0;
			foreach($sem_arr AS $t_year => $t_sem_arr)
			{
				if(count($t_sem_arr) > $max_count)
				{
					$max_count_year = $t_year;
					$max_count = count($t_sem_arr);
				}
			}
			$semester_arr = $sem_arr[$max_count_year];
			
			if(sizeof($annual_subject_record)!=0 && !in_array($ec_iPortfolio['overall_result'], $semester_arr))
			{
				$semester_arr[] = trim($ec_iPortfolio['overall_result']);
			}
		}
*/


		# retrieve semester main records
		$main_record = $this->returnAssessmentMainRecord($StudentID, 0);

		# retrieve annual main records
		$annual_main_record = $this->returnAssessmentMainRecord($StudentID, 1);

		for ($i=0; $i<sizeof($main_record); $i++)
		{
			$record_obj = $main_record[$i];
			//$sem = preg_replace($this->semester_patterns, "", $record_obj["Semester"]);
			$sem = strtolower($ayt->Get_YearNameByID($record_obj["YearTermID"], $_SESSION['intranet_session_language']));

			$RAW_TOTAL[trim($record_obj["Year"])][trim($sem)] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
		}

		for ($i=0; $i<sizeof($annual_main_record); $i++)
		{
			$record_obj = $annual_main_record[$i];
			//$RAW_TOTAL[trim($record_obj["Year"])][trim($ec_iPortfolio['overall_result'])] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
			$RAW_TOTAL[trim($record_obj["Year"])][strtolower(trim($ec_iPortfolio['overall_result']))] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
		}

		// use white border for view and black for printing
		switch ($displayType)
		{
			case "Print":
								$bordercolor = "#000000";
								$title_bgcolor = "#FFFFFF";
								$alternative_row_color = "#FFFFFF";
								$table_class = "";
								break;
			default:
								$bordercolor = "#FFFFFF";
								$title_bgcolor = "#CFE6FE";
								$alternative_row_color = "#EEEEEE";
								$table_class = "class='table_b'";
								break;
		}
		//Table Content

		# table columns
		$rx = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' bgcolor='{$bordercolor}' $table_class ><tr><td>\n";
		if($Printable)
		{
		$rx .= "<table width='100%' border='1' cellpadding='0' cellspacing='0' align='center' class='table_print'>\n";
		}else{
		$rx .= "<table width='100%' border='0' cellpadding='4' cellspacing='0'>\n";
		}
		
		
		
		
		if($Printable)
		{
			//Choose Printable format
//			hdebug_r($RAW);//fai
			if (sizeof($RAW)>0)
		{
			//$style_bottom = "style='border-bottom: 1px dashed $stylecolor;'";
			$rx .= "<tr bgcolor='$title_bgcolor' >\n";
			$rx .= "<td>&nbsp;</td>";
			$rx .= "<td align='center' class='tbheading' width='150'>".$ec_iPortfolio_Report['year']."</td>";
			for($i=0; $i<sizeof($year_arr); $i++)
			{
				# number of semester of the year
				$semCount = (sizeof($sem_arr[$year_arr[$i]])==0) ? 0 : sizeof($sem_arr[$year_arr[$i]]);
				$rx .= "<td align='center' class='tbheading' colspan=".$semCount.">".$year_arr[$i]."</td>";
			}
			$rx .= "</tr>";

			$rx .= "<tr bgcolor='$title_bgcolor' >\n";
			$rx .= "<td align='center' class='tbheading' rowspan=2>".$ec_iPortfolio_Report['subject']."</td>";
			$rx .= "<td align='center' class='tbheading'>".$ec_iPortfolio['class']."</td>";
			for($i=0; $i<sizeof($class_arr); $i++)
			{
				$semCount = (sizeof($sem_arr[$year_arr[$i]])==0) ? 0 : sizeof($sem_arr[$year_arr[$i]]);
				$rx .= "<td align='center' class='tbheading' colspan=".$semCount.">".$class_arr[$i]."</td>";
			}
			$rx .= "</tr>";

			$rx .= "<tr bgcolor='$title_bgcolor' >\n";
			$rx .= "<td align='center' class='tbheading' width='150'>".$ec_iPortfolio_Report['term']."</td>";
			for($i=0; $i<sizeof($year_arr); $i++)
			{
				$year = $year_arr[$i];
				

				for($j=0; $j<sizeof($semester_arr); $j++)
				{
					if(sizeof($sem_arr[$year])!=0)
					{
						$_display_sem = Get_Lang_Selection($this->semEN2B5map_arr[$full_semester_arr[$j]], $semester_arr[$j]);
						

						$trgCheckSemester = strtolower(trim($semester_arr[$j]));
						$srcCheckSem_arr = array_map('strtolower', $sem_arr[$year]);
						
//						$rx .= (in_array(trim($semester_arr[$j]), $sem_arr[$year])) ? "<td align='center' nowrap><span class='tbheading'>".$_display_sem."</span>..</td>" : "";
						$rx .= (in_array($trgCheckSemester, $srcCheckSem_arr)) ? "<td align='center' nowrap><span class='tbheading'>".$_display_sem."</span></td>" : "";

					}
				}
			}
			$rx .= "</tr>";
			for($j=0; $j<sizeof($row_cmp_subject); $j++)
			{
				$subject = trim($row_cmp_subject[$j]["SubjectName"]);
				$subject_code = trim($row_cmp_subject[$j]["SubjectCode"]);
				$cmp_subject = trim($row_cmp_subject[$j]["SubjectComponentName"]);
				$cmp_subject_code = trim($row_cmp_subject[$j]["SubjectComponentCode"]);

				# To check wheter this subject have score
				$have_score = ($displayBy=="Score" || $displayBy=="StandardScore" || $displayBy=="") ? $row_cmp_subject[$j]["have_score"] : 1;



				# check whether the subject code exist
				$valid_subject_code = ($cmp_subject_code=="") ? $this->checkSubjectCode($subject_code) : $this->checkSubjectCode($cmp_subject_code);

				if($valid_subject_code==0)
				{

					continue;
				}

				// IF this is not a component subject
				if($cmp_subject_code=="")
				{
					$bgcolor = ($j%2!=0) ? "bgcolor='$alternative_row_color'" : "bgcolor='#FFFFFF'";
					$rx .= "<tr $bgcolor>\n";
					if ($displayBy=="" || $displayBy=="Grade")
					{
						$rx .= "<td height='35' colspan='2' class='tbheading' nowrap>&nbsp;".$subject."</td>";
					} else
					{
						$chart_display = ($displayType=="Print" || $have_score==0 || $RAW[$subject_code]["NotSelected"]==1 || $RAW[$subject_code]["NoRank"]==1) ? "&nbsp;" : "&nbsp;<a href='stat2.php?$params&SubjectCode=".urlencode($subject_code)."&displayBy=".$displayBy."' class='link_chi' title=".$ec_iPortfolio['display_stat_report']."><img src='$sr_image_path/gimg/icon_chart.gif' width='21' height='21' border='0' align='absmiddle'></a>";
						$rx .= "<td height='35' colspan='2' class='tbheading' nowrap>&nbsp;".$subject.$chart_display."</td>";
					}


					for($k=0; $k<sizeof($year_arr); $k++)
					{

						$year = trim($year_arr[$k]);
						$class = trim($class_arr[$k]);

						for($q=0; $q<sizeof($semester_arr); $q++)
						{
							$sem = strtolower(trim($semester_arr[$q]));
							if(in_array($sem, $sem_arr[$year]))
							{

								if($RAW[$subject_code][$year][$sem]["NotSelected"]==1 || $RAW[$subject_code][$year][$sem]["NoRank"]==1)
								//if($RAW[$subject_code]["NotSelected"]==1 || $RAW[$subject_code]["NoRank"]==1)
								{

									$value = "--";
								}
								else if ($RAW[$subject_code][$year][$sem]["major"]["Score"] == "-1")
								{

									//$value = "--";
									if($displayBy == 'Rank'){
										$value = '--';
									}else{
										$value = ($RAW[$subject_code][$year][$sem]["major"]["Grade"]=="") ? "--" : $RAW[$subject_code][$year][$sem]["major"]["Grade"];
									}

								}
								else if($displayBy=="StandardScore")
								{
									$score = $RAW[$subject_code][$year][$sem]["major"]["Score"];

									# get semester full name
// 									$sem_name = $full_semester_arr[$q];
									$sem_name = $full_Eng_semester_arr[$q];
									# retrieve subject semester standard scores of rank
									$value = $this->returnStandardScore($StudentID, $score, $year, $sem_name, $subject_code, $cmp_subject_code);

								}
								else
								{

									if($valueShow=="Score" && $have_score==0)
									{

										$value = ($RAW[$subject_code][$year][$sem]["major"]["Grade"]=="") ? "--" : $RAW[$subject_code][$year][$sem]["major"]["Grade"];
									}
									else
									{

										//$value = ($RAW[$subject_code][$year][$sem]["major"]["Score"]<=0 || $RAW[$subject_code][$year][$sem]["major"]["Grade"]=="") ? "--" : $RAW[$subject_code][$year][$sem]["major"][$valueShow];




										$value = ($RAW[$subject_code][$year][$sem]["major"][$valueShow]=="" || ($RAW[$subject_code][$year][$sem]["major"]["Score"]<=0 && $RAW[$subject_code][$year][$sem]["major"]["Grade"]=="")) ? "--" : $RAW[$subject_code][$year][$sem]["major"][$valueShow];

										//if displayBy = rank and the value is zero , set value = -- , since rank will not be ZERO 
										if($displayBy == 'Rank' && ($value == 0 || $value == -1)){
											$value = '--';
										}
									}
								}

								$rx .= "<td height='35' align='center' class='tbheading'>".$value."</td>";
							}
						}
					}
					$rx .= "</tr>\n";
				}
				else	//IF it is a component subject
				{
					$bgcolor = ($j%2!=0) ? "bgcolor='$alternative_row_color'" : "bgcolor='#FFFFFF'";
					$rx .= "<tr $bgcolor>\n";
					if ($displayBy=="" || $displayBy=="Grade")
					{
						$rx .= "<td height='35' colspan='2' class='tbheading'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$cmp_subject."&nbsp;</td>";
					} else
					{
						$chart_display = ($displayType=="Print" || $have_score==0 || $RAW[$subject_code]["NotSelected"]==1 || $RAW[$subject_code]["NoRank"]==1) ? "&nbsp;" : "&nbsp;<a href='stat2.php?$params&SubjectCode=".urlencode($subject_code)."&SubjectComponentCode=".urlencode($cmp_subject_code)."&displayBy=".$displayBy."' class='link_chi' title=".$ec_iPortfolio['display_stat_report']."><img src='$sr_image_path/gimg/icon_chart.gif' width='21' height='21' border='0' align='absmiddle'></a>";
						$rx .= "<td height='35' colspan='2' class='tbheading'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$cmp_subject.$chart_display."</td>";
					}

					for($k=0; $k<sizeof($year_arr); $k++)
					{
						$year = trim($year_arr[$k]);
						$class = trim($class_arr[$k]);

						for($q=0; $q<sizeof($semester_arr); $q++)
						{
							$sem = strtolower(trim($semester_arr[$q]));
							if(in_array($sem, $sem_arr[$year]))
							{
								if($RAW[$subject_code]["NotSelected"]==1 || $RAW[$subject_code]["NoRank"]==1)
								{
									$value = "--";
								}
								else if ($RAW[$subject_code][$year][$sem][$cmp_subject_code]["Score"] == "-1")
								{
									$value = ($RAW[$subject_code][$year][$sem][$cmp_subject_code]["Grade"]=="") ? "--" : $RAW[$subject_code][$year][$sem][$cmp_subject_code]["Grade"];
								}
								else if($displayBy=="StandardScore")
								{
									$score = $RAW[$subject_code][$year][$sem][$cmp_subject_code]["Score"];

									# get semester full name
// 									$sem_name = $full_semester_arr[$q];
									$sem_name = $full_Eng_semester_arr[$q];
									# retrieve subject semester standard scores of rank
									$value = $this->returnStandardScore($StudentID, $score, $year, $sem_name, $subject_code, $cmp_subject_code);
								}
								else
								{
									if($valueShow=="Score" && $have_score==0)
									{
										$value = ($RAW[$subject_code][$year][$sem][$cmp_subject_code]["Grade"]=="") ? "--" : $RAW[$subject_code][$year][$sem][$cmp_subject_code]["Grade"];
									}
									else
									{
										$value = ($RAW[$subject_code][$year][$sem][$cmp_subject_code][$valueShow]=="" || $RAW[$subject_code][$year][$sem][$cmp_subject_code]["Rank"]<=0 || ($RAW[$subject_code][$year][$sem][$cmp_subject_code]["Score"]<=0 && $RAW[$subject_code][$year][$sem][$cmp_subject_code]["Grade"]=="")) ? "--" : $RAW[$subject_code][$year][$sem][$cmp_subject_code][$valueShow];
									}
								}

								$rx .= "<td height='35' align='center' class='tbheading'>".$value."</td>";
							}
						}
					}
					$rx .= "</tr>\n";
				}
			}

			$bgcolor = ($j%2!=0) ? "bgcolor='$alternative_row_color'" : "bgcolor='#FFFFFF'";
			$rx .= "<tr $bgcolor>\n";
			$rx .= "<td height='35' colspan='2' align='center' class='tbheading' nowrap>[".$totalTitle."]</td>";
			for($k=0; $k<sizeof($year_arr); $k++)
			{
				$year = trim($year_arr[$k]);
				$class = trim($class_arr[$k]);

				for($q=0; $q<sizeof($semester_arr); $q++)
				{
//					$sem = trim($semester_arr[$q]);
					//Syn to all lower case to sem , in case the term name with different case
					$sem = strtolower(trim($semester_arr[$q])); 

					if(in_array($sem, $sem_arr[$year]))
					{
						if($displayBy=="StandardScore")
						{
							$score = $RAW_TOTAL[$year][$sem]["Score"];

							# get semester full name
// 							$sem_name = $full_semester_arr[$q];
							$sem_name = $full_Eng_semester_arr[$q];
							# retrieve subject annual standard scores
							$value = $this->returnMainStandardScore($StudentID, $score, $year, $sem_name);
						}
						else
						{
							$value = (($valueShow=="Grade" && $RAW_TOTAL[$year][$sem][$valueShow]!="") || $RAW_TOTAL[$year][$sem][$valueShow]>0) ? $RAW_TOTAL[$year][$sem][$valueShow] : "--";
						}
						$rx .= "<td height='35' align='center' class='tbheading'>".$value."</td>";
					}
				}
			}
			$rx .= "</tr>";

		} else
		{
			$rx .= "<tr><td align='center' height='100' bgcolor='#FFFFFF' class='tbheading'>{$no_record_msg}</td></tr>";
		}
			//End of Printable format
		}else{
		
			//Choose Not-Printable format

		if (sizeof($RAW)>0)
		{
			//$style_bottom = "style='border-bottom: 1px dashed $stylecolor;'";
			$rx .= "<tr>\n";
			$rx .= "<td align=left valign=top>&nbsp;</td>";
			$rx .= "<td align=left valign=top>&nbsp;</td>";
			//$rx .= "<td align='center' class='tbheading'>".$ec_iPortfolio_Report['year']."</td>";
			
			for($i=0; $i<sizeof($year_arr); $i++)
			{
				# number of semester of the year
				$semCount = (sizeof($sem_arr[$year_arr[$i]])==0) ? 0 : sizeof($sem_arr[$year_arr[$i]]);
				$rx .= "<td class='retabletop tabletopnolink' style='border-bottom: 1px solid rgb(255, 255, 255);' align='center' valign='top' colspan=".$semCount.">".$year_arr[$i]."<br/>".$class_arr[$i]."</td>";
			}
			
				/*
			for($i=0; $i<sizeof($class_arr); $i++)
			{
				$semCount = (sizeof($sem_arr[$year_arr[$i]])==0) ? 0 : sizeof($sem_arr[$year_arr[$i]]);
				$rx .= "<td align='center' class='tbheading' colspan=".$semCount.">".$class_arr[$i]."</td>";
			}
			*/
			
			$rx .= "</tr>";

			$rx .= "<tr>\n";
			//$rx .= "<td align='center' class='tbheading'>".$ec_iPortfolio_Report['term']."</td>";
			$rx .="<td align='right' valign='top'>&nbsp;</td>";
			$rx .="<td align='right' valign='top'>&nbsp;</td>";

			for($i=0; $i<sizeof($year_arr); $i++)
			{
				$year = $year_arr[$i];
				for($j=0; $j<sizeof($semester_arr); $j++)
				{
					if(sizeof($sem_arr[$year])!=0)
					{
						$rx .= (in_array(strtolower(trim($semester_arr[$j])), array_map('strtolower',$sem_arr[$year]))) ? "<td class='retabletop tabletopnolink' align='center' valign='top'>".$semester_arr[$j]."</td>" : "";
					}
				}
			}
			
			$rx .= "</tr>";
			for($j=0; $j<sizeof($row_cmp_subject); $j++)
			{
				$subject = trim($row_cmp_subject[$j]["SubjectName"]);
				$subject_code = trim($row_cmp_subject[$j]["SubjectCode"]);
				$cmp_subject = trim($row_cmp_subject[$j]["SubjectComponentName"]);
				$cmp_subject_code = trim($row_cmp_subject[$j]["SubjectComponentCode"]);

				# To check wheter this subject have score
				$have_score = ($displayBy=="Score" || $displayBy=="StandardScore" || $displayBy=="") ? $row_cmp_subject[$j]["have_score"] : 1;

				# check whether the subject code exist
				$valid_subject_code = ($cmp_subject_code=="") ? $this->checkSubjectCode($subject_code) : $this->checkSubjectCode($cmp_subject_code);
				if($valid_subject_code==0)
				{
					continue;
				}

				// IF this is not a component subject
				if($cmp_subject_code=="")
				{
					$retable_class = ($j%2!=0) ? "class='retablerow2 tabletext'" : "class='retablerow1 tabletext'";
					$rx .= "<tr>\n";
					if ($displayBy=="" || $displayBy=="Grade")
					{
						$rx .= "<td class='tablelist_subject'>".$subject."</td><td class='tablelist_subject'>&nbsp;</td>";
					} else
					{
						$chart_display = ($displayType=="Print" || $have_score==0 || $RAW[$subject_code]["NotSelected"]==1 || $RAW[$subject_code]["NoRank"]==1) ? "&nbsp;" : ($ParChangeLayer ? "<a href='#' onClick=\"jVIEW_DETAIL('assessment', '".base64_encode("my_year=".$ck_alumni_year."&SubjectCode=".urlencode($subject_code)."&displayBy=".$displayBy)."')\" class='link_chi' title=".$ec_iPortfolio['display_stat_report']."><img src='$sr_image_path/images/2009a/iPortfolio/icon_resultview.gif' width='20' height='20' border='0'></a>" : "<a href='stat2.php?$params&SubjectCode=".urlencode($subject_code)."&displayBy=".$displayBy."' class='link_chi' title=".$ec_iPortfolio['display_stat_report']."><img src='$sr_image_path/images/2009a/iPortfolio/icon_resultview.gif' width='20' height='20' border='0'></a>");
						$rx .= "<td class='tablelist_subject'>".$subject."</td><td class='tablelist_subject'>".$chart_display."</td>";
					}
					for($k=0; $k<sizeof($year_arr); $k++)
					{
						$year = trim($year_arr[$k]);
						$class = trim($class_arr[$k]);
						for($q=0; $q<sizeof($semester_arr); $q++)
						{
							$sem = strtolower(trim($semester_arr[$q]));
							if(in_array($sem, array_map('strtolower',$sem_arr[$year])))
							{
								if($RAW[$subject_code]["NotSelected"]==1 || $RAW[$subject_code]["NoRank"]==1)
								{
									$value = "--";
								}
								else if ($RAW[$subject_code][$year][$sem]["major"]["Score"] == "-1")
								{
									//$value = "--";
									$value = ($RAW[$subject_code][$year][$sem]["major"]["Grade"]=="") ? "--" : $RAW[$subject_code][$year][$sem]["major"]["Grade"];
								}
								else if($displayBy=="StandardScore")
								{
									$score = $RAW[$subject_code][$year][$sem]["major"]["Score"];

									# get semester full name
// 									$sem_name = $full_semester_arr[$q];
									$sem_name = $full_Eng_semester_arr[$q];
									# retrieve subject semester standard scores of rank
									$value = $this->returnStandardScore($StudentID, $score, $year, $sem_name, $subject_code, $cmp_subject_code);
								}
								else
								{
									if($valueShow=="Score" && $have_score==0)
									{
										$value = ($RAW[$subject_code][$year][$sem]["major"]["Grade"]=="") ? "--" : $RAW[$subject_code][$year][$sem]["major"]["Grade"];
									}
									else
									{
										//$value = ($RAW[$subject_code][$year][$sem]["major"]["Score"]<=0 || $RAW[$subject_code][$year][$sem]["major"]["Grade"]=="") ? "--" : $RAW[$subject_code][$year][$sem]["major"][$valueShow];
										$value = ($RAW[$subject_code][$year][$sem]["major"][$valueShow]=="" || ($RAW[$subject_code][$year][$sem]["major"]["Score"]<=0 && $RAW[$subject_code][$year][$sem]["major"]["Grade"]=="")) ? "--" : $RAW[$subject_code][$year][$sem]["major"][$valueShow];

									}
								}
								$rx .= "<td align='center' $retable_class>".$value."</td>";
							}
						}
					}
					$rx .= "</tr>\n";
				}
				else	//IF it is a component subject
				{
					$retable_class = ($j%2!=0) ? "class='retablerow2 tabletext'" : "class='retablerow1 tabletext'";
					$rx .= "<tr>\n";
					if ($displayBy=="" || $displayBy=="Grade")
					{
						$rx .= "<td class='tablelist_sub_subject' style='padding-left: 15px;'><span class='tabletext'>".$cmp_subject."</span></td><td class='tablelist_subject'>&nbsp;</td>";
					} else
					{
						//$chart_display = ($displayType=="Print" || $have_score==0 || $RAW[$subject_code]["NotSelected"]==1 || $RAW[$subject_code]["NoRank"]==1) ? "&nbsp;" : "&nbsp;<a href='stat2.php?$params&SubjectCode=".urlencode($subject_code)."&SubjectComponentCode=".urlencode($cmp_subject_code)."&displayBy=".$displayBy."' class='link_chi' title=".$ec_iPortfolio['display_stat_report']."><img src='$sr_image_path/images/2009a/iPortfolio/icon_resultview.gif' width='20' height='20' border='0'></a>";
						$chart_display = ($displayType=="Print" || $have_score==0 || $RAW[$subject_code]["NotSelected"]==1 || $RAW[$subject_code]["NoRank"]==1) ? "&nbsp;" : ($ParChangeLayer ? "<a href='#' onClick=\"jVIEW_DETAIL('assessment', '".base64_encode("$params&SubjectCode=".urlencode($subject_code)."&SubjectComponentCode=".urlencode($cmp_subject_code)."&displayBy=".$displayBy)."')\" class='link_chi' title=".$ec_iPortfolio['display_stat_report']."><img src='$sr_image_path/images/2009a/iPortfolio/icon_resultview.gif' width='20' height='20' border='0'></a>" : "&nbsp;<a href='stat2.php?$params&SubjectCode=".urlencode($subject_code)."&SubjectComponentCode=".urlencode($cmp_subject_code)."&displayBy=".$displayBy."' class='link_chi' title=".$ec_iPortfolio['display_stat_report']."><img src='$sr_image_path/images/2009a/iPortfolio/icon_resultview.gif' width='20' height='20' border='0'></a>");
						$rx .= "<td class='tablelist_sub_subject'>".$cmp_subject."</td><td class='tablelist_subject'>".$chart_display."</td>";
					}

					for($k=0; $k<sizeof($year_arr); $k++)
					{
						$year = trim($year_arr[$k]);
						$class = trim($class_arr[$k]);

						for($q=0; $q<sizeof($semester_arr); $q++)
						{
							$sem = strtolower(trim($semester_arr[$q]));
							if(in_array($sem, array_map('strtolower',$sem_arr[$year])))
							{
								if($RAW[$subject_code]["NotSelected"]==1 || $RAW[$subject_code]["NoRank"]==1)
								{
									$value = "--";
								}
								else if ($RAW[$subject_code][$year][$sem][$cmp_subject_code]["Score"] == "-1")
								{
									$value = ($RAW[$subject_code][$year][$sem][$cmp_subject_code]["Grade"]=="") ? "--" : $RAW[$subject_code][$year][$sem][$cmp_subject_code]["Grade"];
								}
								else if($displayBy=="StandardScore")
								{
									$score = $RAW[$subject_code][$year][$sem][$cmp_subject_code]["Score"];

									# get semester full name
// 									$sem_name = $full_semester_arr[$q];
									$sem_name = $full_Eng_semester_arr[$q];
									# retrieve subject semester standard scores of rank
									$value = $this->returnStandardScore($StudentID, $score, $year, $sem_name, $subject_code, $cmp_subject_code);
								}
								else
								{
									if($valueShow=="Score" && $have_score==0)
									{
										$value = ($RAW[$subject_code][$year][$sem][$cmp_subject_code]["Grade"]=="") ? "--" : $RAW[$subject_code][$year][$sem][$cmp_subject_code]["Grade"];
									}
									else
									{
										$value = ($RAW[$subject_code][$year][$sem][$cmp_subject_code][$valueShow]=="" || ($RAW[$subject_code][$year][$sem][$cmp_subject_code]["Score"]<=0 && $RAW[$subject_code][$year][$sem][$cmp_subject_code]["Grade"]=="")) ? "--" : $RAW[$subject_code][$year][$sem][$cmp_subject_code][$valueShow];
									}
								}
								$rx .= "<td align='center' $retable_class>".$value."</td>";
							}
						}
					}
					$rx .= "</tr>\n";
				}
			}

			$retable_class = ($j%2!=0) ? "class='retablerow2 tabletext'" : "class='retablerow1 tabletext'";
			$rx .= "<tr>\n";
			$rx .= "<td colspan='2' align='right'>".$totalTitle."</td>";
			for($k=0; $k<sizeof($year_arr); $k++)
			{
				$year = trim($year_arr[$k]);
				$class = trim($class_arr[$k]);

				for($q=0; $q<sizeof($semester_arr); $q++)
				{
					$sem = strtolower(trim($semester_arr[$q]));
					if(in_array($sem, array_map('strtolower',$sem_arr[$year])))
					{
						if($displayBy=="StandardScore")
						{
							$score = $RAW_TOTAL[$year][$sem]["Score"];

							# get semester full name
// 							$sem_name = $full_semester_arr[$q];
							$sem_name = $full_Eng_semester_arr[$q];
							# retrieve subject annual standard scores
							$value = $this->returnMainStandardScore($StudentID, $score, $year, $sem_name);
						}
						else
						{
							$value = (($valueShow=="Grade" && $RAW_TOTAL[$year][$sem][$valueShow]!="") || $RAW_TOTAL[$year][$sem][$valueShow]>0) ? $RAW_TOTAL[$year][$sem][$valueShow] : "--";
						}
						$rx .= "<td class='tablerow_total' align='center'>".$value."</td>";
					}
				}
			}
			$rx .= "</tr>";

		} else
		{
			$rx .= "<tr><td align='center' height='100' class='retablerow1 tabletext'>{$no_record_msg}</td></tr>";
		}
		} //End of Not-Printable format
		$rx .= "</table>\n";
		$rx .= "</td></tr></table>\n";

		return $rx;
	}
	
	function returnAllSubjectWithComponents($StudentID="", $ClassName="", $Year="")
	{
		global $eclass_db, $intranet_db;

		//StartTimer();
		$subj_namefield = $this->returnSqlFieldLang("DES","SUBJ");
		$cmp_subj_namefield = $this->returnSqlFieldLang("DES","CMP");
		if($StudentID=="" && $ClassName=="")
		{
			# Retrieve all Subject Studied in Proper Order
			$sql = "SELECT DISTINCT
						SUBJ.EN_SNAME as SubjectCode,
						if(CMP.CMP_CODEID IS NOT NULL AND CMP.CMP_CODEID <> '', CMP.EN_SNAME, '') as SubjectComponentCode,
						{$subj_namefield} AS SubjectName,
						if(CMP.CMP_CODEID IS NOT NULL AND CMP.CMP_CODEID <> '', $cmp_subj_namefield, '') AS SubjectComponentName
					FROM
						{$intranet_db}.ASSESSMENT_SUBJECT AS SUBJ
						LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS CMP ON (SUBJ.CODEID = CMP.CODEID And CMP.RecordStatus = 1)
					WHERE
						(SUBJ.EN_SNAME IS NOT NULL AND SUBJ.EN_SNAME<>'')
						AND (SUBJ.CMP_CODEID IS NULL OR SUBJ.CMP_CODEID='')
						And SUBJ.RecordStatus = 1 
					ORDER BY
						SUBJ.DisplayOrder, CMP.DisplayOrder
				";
			$row = $this->returnArray($sql);
		}
		else
		{
			if($StudentID!="")
				$conds = " AND ASSR.UserID='$StudentID' ";
			if($ClassName!="")
				$conds .= " AND ASSR.ClassName IN ({$ClassName}) ";
			if($Year!="")
				$conds .= " AND ASSR.Year = '".$Year."'";

			$sql = "SELECT DISTINCT
						ASSR.SubjectCode,
						if(ASSR.SubjectComponentCode IS NOT NULL, ASSR.SubjectComponentCode, '') as SubjectComponentCode,
						{$subj_namefield} AS SubjectName,
						if(ASSR.SubjectComponentCode IS NOT NULL, if({$cmp_subj_namefield} is null,'',{$cmp_subj_namefield}), '') AS SubjectComponentName, SUBJ.RecordID
					FROM
						{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD AS ASSR
						LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS SUBJ ON (SUBJ.EN_SNAME=ASSR.SubjectCode AND SUBJ.RecordID=ASSR.SubjectID)
						LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS CMP ON (CMP.EN_SNAME=ASSR.SubjectComponentCode AND CMP.CODEID=SUBJ.CODEID And CMP.RecordStatus = 1)
					WHERE
						(SUBJ.EN_SNAME IS NOT NULL AND SUBJ.EN_SNAME<>'') 
						AND (SUBJ.CMP_CODEID IS NULL OR SUBJ.CMP_CODEID='')
						And SUBJ.RecordStatus = 1
						$conds AND (ASSR.Grade<>'' OR ASSR.Score<>'')
					ORDER BY
						SUBJ.DisplayOrder, CMP.DisplayOrder
				";
			$row = $this->returnArray($sql);
		}
		//hdebug("Part I generated in " . StopTimer(). " seconds.");

		for($i=0; $i<sizeof($row); $i++)
		{
			$row[$i]["have_score"] = 1; //$this->checkSubjectScoreExist($row[$i]["SubjectCode"], $row[$i]["SubjectComponentCode"]);
		}

		return $row;
	}
	
	function getAssessmentChartData($data, $SubjectCode, $SubjectComponentCode, $StudentID, $filter, $displayBy, $SubjectName)
	{
		global $ec_iPortfolio;

		$showValue = ($displayBy=="Rank") ? "OrderMeritForm" : $displayBy;

		if($displayBy=="Score")
		{
			$valueTitle = $ec_iPortfolio["score"];
			$x_text = $ec_iPortfolio['score'];
		}
		else if($displayBy=="Rank")
		{
			$valueTitle = $ec_iPortfolio["rank"];
			$x_text = $ec_iPortfolio['rank'];
		}
		else if($displayBy=="StandardScore")
		{
			$valueTitle = $ec_iPortfolio["stand_score"];
			$x_text = $ec_iPortfolio['stand_score'];
		}
		$y_text = ($filter==0) ? $ec_iPortfolio['year']."(".$ec_iPortfolio['semester'].")" : $ec_iPortfolio['year'];

		$maxTotal = 100;
		$minTotal = 0;
		$yearStr = "&title=;";
		$dataStr = "&data1=".$SubjectName.";";
		# data for plotting graph
		if(sizeof($data)>0)
		{
			for($i=0; $i<sizeof($data); $i++)
			{
				if(!($displayBy=="StandardScore" && $data[$i][$showValue]=="--") && !($displayBy=="Rank" && $data[$i][$showValue]<=0))
				{
					$yearStr .= $data[$i]["YearSem"].";";
					$dataStr .= $data[$i][$showValue].";";

					$StandScoreArr[] = $data[$i]["StandardScore"];
				}
			}

			if($displayBy=="Score")
			{
				// get max score
				$maxScore = $this->getAssessmentMaxScore($SubjectCode, $filter, $SubjectComponentCode);
				$maxTotal = ($maxScore<100) ? 100 : ($maxScore-($maxScore%50)+50);
				$minTotal = 0;
			}
			else if($displayBy=="Rank")
			{
				$rankTotal = $this->getAssessmentMaxTotalRank($SubjectCode, $filter, $SubjectComponentCode);
				$rankTotal = ($rankTotal!=0) ? $rankTotal-($rankTotal%100)+100 : 0;
				$maxTotal = $rankTotal + 1;
				$minTotal = 1;
			}
			else if($displayBy=="StandardScore" && is_array($StandScoreArr))
			{
				sort($StandScoreArr);
				$minStandScore = $StandScoreArr[0];
				$maxStandScore = $StandScoreArr[sizeof($StandScoreArr)-1];
				$minTotal = floor($minStandScore);
				$maxTotal = ceil($maxStandScore);

				if($minTotal==$maxTotal)
					$minTotal = $minTotal-1;
			}
		}
		$and_code = "%26";
		$style_url = ($displayBy=="Rank") ? "ec_max=$minTotal".$and_code."ec_min=$maxTotal" : "ec_max=$maxTotal".$and_code."ec_min=$minTotal";

		$xy_text = $x_text."_".$y_text;
		$axis_url = "xy=$xy_text\n";
		$chart_data = $yearStr.$dataStr;

		return array($style_url, $axis_url, $chart_data);
	}
	
	function getAssessmentMaxTotalRank($SubjectCode, $isAnnual, $SubjectCmpCode="")
	{
		global $eclass_db;

		$conds .= ($isAnnual) ? " WHERE IsAnnual = 1" : " WHERE IsAnnual = 0";
		if($SubjectCode!="")
		{
			$conds .= ($SubjectCmpCode!="") ? " AND SubjectCode = '".$SubjectCode."' AND SubjectComponentCode = '".$SubjectCmpCode."'" : " AND SubjectCode = '".$SubjectCode."' AND SubjectComponentCode IS NULL";
		}

		$sql = "SELECT
					MAX(OrderMeritForm)
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
				$conds
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}
	
	# Retrieve Student Standard Score
	function returnMainStandardScore($StudentID, $score, $year, $sem)
	{
		global $eclass_db, $intranet_db, $ec_iPortfolio;

		$z = "--";
		if(!empty($score))
		{
			# get class level of this record
			$levelID = $this->returnStudentAssessmentClassLevel($StudentID, $year);

			$conds = " AND IC.ClassLevelID = '".$levelID."' ";
			$conds .= ($sem==$ec_iPortfolio['overall_result']) ? " AND ASMR.IsAnnual = 1 " : " AND ASMR.Semester = '$sem' ";

			$sql = "SELECT
						ASMR.Score
					FROM
						{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD as ASMR
						LEFT JOIN {$intranet_db}.INTRANET_CLASS as IC ON ASMR.ClassName = IC.ClassName
					WHERE
						ASMR.Year = '$year'
						AND (ASMR.Score > 0 OR (ASMR.Score=0 AND ASMR.Grade<>'' AND ASMR.OrderMeritForm>0))
						$conds
				";
			$ScoreArr = $this->returnVector($sql);
			$sd = round(standard_deviation($ScoreArr), 2);

			if($sd>0)
			{
				# Calculate the standard score
				$mean = array_sum($ScoreArr)/sizeof($ScoreArr);
				$z = round((($score-$mean)/$sd), 2);
			}
		}
		return $z;
	}
	
	function returnServiceRecord($StudentID, $Year="", $Semester="")
	{
		global $eclass_db,$Lang;
								
		$conds = $this->getReportYearCondition($Year, "a.");
		$conds .= ($Semester!="") ? " AND a.Semester = '$Semester'" : "" ;
		$sql = "SELECT
					 a.Year,
					 if((a.Semester='' OR  a.Semester IS NULL), '".$Lang['General']['WholeYear']."', a.Semester) as `Semester`,
					 if(a.ServiceDate='0000-00-00', '--', a.ServiceDate),
					 a.ServiceName,
					 if(a.Role='', '--', a.Role) as Role,
					 if(a.Performance='', '--', a.Performance) as Performance,
					 a.ModifiedDate
				FROM
					{$eclass_db}.SERVICE_STUDENT as a
				WHERE
					a.UserID = '$StudentID'
					$conds
              ";

    # Eric Yip : Sort records
		if($this->YearDescending)
      $x[] = "a.Year DESC";
    if($this->SemesterAscending)
      $x[] = "a.Semester ASC";
    if(is_array($x) && count($x) > 0)
      $sql .= "
                ORDER BY
              ".implode(", ", $x);

		$row = $this->returnArray($sql);

		return $row;
	}
	
	function returnOLERecord($StudentID, $Year="", $ParShowAllCat=false)
	{
		global $eclass_db, $intranet_db, $ec_iPortfolio, $profiles_to;

		if($Year!="")
		{
			$SplitArray = explode("-", $Year);
			if(sizeof($SplitArray)>1)
			{
        # Eric Yip (20091202): Check for dummy date
  			if($SplitArray[0] == "0000")
  			{
          $LowerDate = 0;
          $UpperDate = 0;
        }
        else
        {
  				$LowerDate = mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"], trim($SplitArray[0]));
  				$UpperDate = mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"]-1, trim($SplitArray[1]));
				}
			}
			else
			{
				$LowerDate = mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"], $Year);
				$UpperDate = mktime(0, 0, 0, $this->year_start["month"], $this->year_start["day"]-1, $Year+1);
			}
			$cond = " AND UNIX_TIMESTAMP(a.StartDate) BETWEEN '".$LowerDate."' AND '".$UpperDate."'";
		}

		$namefield = getNameFieldWithClassNumberByLang("b.");
		
		$lpf_slp = new libpf_slp();
		$CategoryField = $lpf_slp->getOLRCategoryFieldForRecord("a.", $ParShowAllCat);
		$sql = "SELECT
					IF ((a.StartDate IS NULL OR a.StartDate='0000-00-00'),'--',IF(a.EndDate IS NULL OR 				a.EndDate='0000-00-00',a.StartDate,CONCAT(a.StartDate,' $profiles_to<br />',a.EndDate))) as OLEDate,
					a.Title,
					$CategoryField,
					a.Role,
					a.Hours,
					a.Achievement,
					c.Details,
					$namefield as ApproveBy,
					IF (a.ProcessDate IS NOT NULL, DATE_FORMAT(a.ProcessDate,'%Y-%m-%d'),'--'),
					c.SchoolRemarks,
					a.ModifiedDate,
					a.StartDate,
					a.ele
				FROM
					{$eclass_db}.OLE_STUDENT as a
					LEFT JOIN {$intranet_db}.INTRANET_USER as b ON a.ApprovedBy = b.UserID
					INNER JOIN {$eclass_db}.OLE_PROGRAM as c ON a.ProgramID = c.ProgramID
				WHERE
					a.UserID = '$StudentID'
					AND (a.RecordStatus = '2' || a.RecordStatus = '4')
					$cond
				ORDER BY
					a.StartDate DESC
              ";

		$row = $this->returnArray($sql);

		return $row;
	}
	
	function returnSqlFieldLang($my_field, $my_prefix){
//		global $ck_default_lang;
		global $intranet_session_language;

//		if ($ck_default_lang=="chib5")
		if($intranet_session_language=="b5" || $intranet_session_language=="gb")
		{
			$rx = "{$my_prefix}.CH_{$my_field}";
		} else
		{
			$rx = "{$my_prefix}.EN_{$my_field}";
		}

		return $rx;
	}
	
	# Retrieve Student Assessment Result
	function returnAssessmentResult($StudentID, $IsAnnual, $Year="", $Semester="")
	{
		global $eclass_db, $ec_iPortfolio, $intranet_db;

		$conds = $this->getReportYearCondition($Year, "ASSR.");
		$conds .= ($Semester!="" && $IsAnnual==0) ? " AND ASSR.Semester = '$Semester'" : "" ;

		# Retrieve all RAW Records
		# Eric Yip (20090828): sort Semester with Chinese characters with specific order
		$sql = "	SELECT
							ASSR.Year, ASSR.Semester, ASSR.IsAnnual, ASSR.SubjectCode, ASSR.SubjectComponentCode, ASSR.Score, ASSR.Grade, ASSR.ClassName, ASSR.OrderMeritForm,
					       CASE LEFT(Semester,3)
                            WHEN '".$ec_iPortfolio['semester_name_array_1'][2]."' THEN 1
                            WHEN '".$ec_iPortfolio['semester_name_array_2'][2]."' THEN 2
                            WHEN '".$ec_iPortfolio['semester_name_array_3'][2]."' THEN 3
                            ELSE 4
                          END AS SemesterOrder,
                            ASSR.YearTermID,
                            ASSR.TermAssessment,
                            ASSR.SubjectID
					FROM
							{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD AS ASSR
							LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS SUBJ ON SUBJ.EN_SNAME=ASSR.SubjectCode and SUBJ.RecordID = ASSR.SubjectID
					WHERE
							UserId='$StudentID'
							AND ASSR.IsAnnual = '$IsAnnual'
							AND SUBJ.recordstatus = 1
							$conds
					ORDER BY
							ASSR.Year, ASSR.ClassName, SemesterOrder, ASSR.Semester, SUBJ.DisplayOrder, ASSR.ModifiedDate
				";

		$row_all = $this->returnArray($sql);
        
	// if no record is "ASSR.TermAssessment is NULL" then, using one of the TermAssessment to display
		if($IsAnnual != 1){
    		$row_assoArr = array();
    		foreach((array)$row_all as $rowNum => $row){
    		    $_termId = $row['YearTermID'];
    		    $_subjectId = $row['SubjectID'];
    		    $_cmpCode = $row['SubjectComponentCode'];
    		    $_TermAssessment = $row['TermAssessment'];
    		    if($_TermAssessment == ''){
    		        $row_assoArr[$_termId][$_subjectId.$_cmpCode]['TermOverall'] = $rowNum;
    		    }
    		    else{
        		    $row_assoArr[$_termId][$_subjectId.$_cmpCode]['TermAssessment'][] = $rowNum; 
    		    }
    		}
    		$resultArr = array();
    		foreach((array)$row_assoArr as $_termId => $_arr){
    		    foreach((array)$_arr as $__subjectIdCmpCode => $__resultInfo){
    		       if(isset($__resultInfo['TermOverall'])){
    		           $__rowNum = $__resultInfo['TermOverall'];
    		       }
    		       else{
    		           $__rowNum = $__resultInfo['TermAssessment'][0];
    		       }
    		       $resultArr[] = $row_all[$__rowNum]; 
    		    }
    		}
		}
		else{
		    $resultArr = $row_all;
		}
		
		return $resultArr;
	}

	# Retrieve Student Assessment Main Result
	function returnAssessmentMainRecord($StudentID, $IsAnnual)
	{
		global $eclass_db;

		# Retrieve all RAW Records
		$sql = "	SELECT
							Year, Semester, IsAnnual, Score, Grade, OrderMeritForm, YearTermID
					FROM
							{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
					WHERE
							UserId='$StudentID'
							AND IsAnnual = '$IsAnnual'
					ORDER BY
							Year, ClassName, Semester
				";
		$row_all = $this->returnArray($sql);

		return $row_all;
	}
	
	# function to retrieve subject that has no score
	function checkSubjectScoreExist($SubjectCode, $SubjectComponentCode)
	{
		global $eclass_db;

		$conds = ($SubjectComponentCode=="") ? " AND (SubjectComponentCode = '' OR SubjectComponentCode IS NULL)" : " AND SubjectComponentCode = '$SubjectComponentCode'";
		
		// 2011-03-02 Ivan [2011-0302-1053-43067]: added condition Abs in Score and change "Score = 0" to "Score = -1" to cater negative marks 
		$sql = "SELECT
					SUM(ABS(Score))
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
				WHERE
					(Score>0 OR (Score <= 0 AND Grade <> '' AND Grade IS NOT NULL && OrderMeritForm > 0))
					AND SubjectCode = '$SubjectCode'
					$conds
				";
		$row = $this->returnVector($sql);

		return ($row[0]>0) ? 1 : 0;
	}
	
	# Retrieve all Asssessment Subject Record By SubjectCode
	function returnAsssessmentSubjectRecordBySubjectCode($StudentID, $SubjectCode, $filter, $SubjectCmpCode="", $Year="")
	{
		global $eclass_db, $ec_iPortfolio;

		$conds = ($filter==0) ? " AND IsAnnual = 0 " : " AND IsAnnual = 1 ";
		$conds .= ($SubjectCmpCode=="") ? "AND SubjectCode = '".$SubjectCode."' AND SubjectComponentCode IS NULL " : "AND SubjectCode = '".$SubjectCode."' AND SubjectComponentCode = '".$SubjectCmpCode."' ";
		$conds .= ($Year=="") ? "" : "AND Year = '$Year' ";

		# Retrieve all RAW Records
		$sql = "	SELECT
						Semester,
						Year,
						Score,
						Grade,
						OrderMeritForm,
						OrderMeritFormTotal
					FROM
							{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
					WHERE
							UserId='$StudentID'
							$conds
					ORDER BY
							Year ASC
				";

				//echo $sql;
		$row = $this->returnArray($sql);

		// prepare a year semester array for sorting
		if($filter==0)
		{
			if($Year=="")
			{
				# Retrieve distinct year
				$sql = "SELECT
							DISTINCT Year
						FROM
							{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
						WHERE
							UserId='$StudentID'
							$conds
						ORDER BY
							Year ASC
						";
				$yearArr = $this->returnVector($sql);
			}
			else
				$yearArr = array($Year);

			list($full_semester_arr, $semester_arr) = $this->getSemesterNameArrFromIP();

			for($k=0; $k<sizeof($yearArr); $k++)
			{
				$yr = $yearArr[$k];
				for($m=0; $m<sizeof($semester_arr); $m++)
				{
					$sem = $semester_arr[$m];
					$YearSemArr[] = array($yr, $sem);
				}
			}
		}

		for($i=0; $i<sizeof($row); $i++)
		{
			$this_year = trim($row[$i]["Year"]);
			$this_sem = trim($row[$i]["Semester"]);
			$row[$i]["StandardScore"] = $this->returnStandardScore($StudentID, $row[$i]["Score"], $this_year, $this_sem, $SubjectCode, $SubjectCmpCode);

			if($filter==0)
			{
				$sem_name = trim(preg_replace($this->semester_patterns, "", $this_sem));
				$row[$i]["YearSem"] = $this_year."(".$sem_name.")";
				for($j=0; $j<sizeof($YearSemArr); $j++)
				{
					list($t_year, $t_sem) = $YearSemArr[$j];
					if($this_year==$t_year && $this_sem==$t_sem)
					{
						$newArr[$j] = $row[$i];
						break;
					}
				}
			}
			else
			{
				$row[$i]["YearSem"] = $row[$i]["Year"];
			}
		}

		// array sorting
		if($filter==0 && is_array($newArr))
		{
			ksort($newArr);
			$row = array_values($newArr);
		}

		return $row;
	}
	
	# Retrieve Student Standard Score
	function returnStandardScore($StudentID, $score, $year, $sem, $subject_code, $cmp_subject_code, $parSubjectId='', $parCmpSubjectId='', $parTermAssessment='')
	{
		global $eclass_db, $intranet_db, $ec_iPortfolio;

		$z = "--";
		if(!empty($score))
		{
			# get class level of this record
			$levelID = $this->returnStudentAssessmentClassLevel($StudentID, $year);

// 			$conds = " AND yc.YearID = '".$levelID."' ";
			
// 			//$conds .= ($cmp_subject_code=="") ? " AND (ASSR.SubjectComponentCode IS NULL OR ASSR.SubjectComponentCode = '')" : " AND ASSR.SubjectComponentCode = '$cmp_subject_code'";
// 			if ($parCmpSubjectId != '') {
// 				$conds .= ($parCmpSubjectId=="") ? " AND (ASSR.SubjectComponentID IS NULL OR ASSR.SubjectComponentID = 0)" : " AND ASSR.SubjectComponentID = '$parCmpSubjectId'";
// 			}
// 			else {
// 				$conds .= ($cmp_subject_code=="") ? " AND (ASSR.SubjectComponentCode IS NULL OR ASSR.SubjectComponentCode = '')" : " AND ASSR.SubjectComponentCode = '$cmp_subject_code'";
// 			}
			
// 			$conds .= ($sem==$ec_iPortfolio['overall_result']) ? " AND ASSR.IsAnnual = 1 " : " AND ASSR.Semester = '$sem' ";
			
// 			if ($parSubjectId != '') {
// 				$subjectCond = " AND ASSR.SubjectID = '$parSubjectId' ";
// 			}
// 			else {
// 				$subjectCond = " AND ASSR.SubjectCode = '$subject_code' ";
// 			}
			
// 			if ($parTermAssessment != '') {
// 				$termAssessmentCond = " AND ASSR.TermAssessment = '$parTermAssessment' ";
// 			}
// 			else {
// 				$termAssessmentCond = " AND (ASSR.TermAssessment is null or ASSR.TermAssessment = '') ";
// 			}

// 			$conds = " AND yc.YearID = '".$levelID."' ";
			
			//$conds .= ($cmp_subject_code=="") ? " AND (ASSR.SubjectComponentCode IS NULL OR ASSR.SubjectComponentCode = '')" : " AND ASSR.SubjectComponentCode = '$cmp_subject_code'";
			if ($parCmpSubjectId != '') {
			    $conds .= ($parCmpSubjectId=="") ? " AND (SubjectComponentID IS NULL OR SubjectComponentID = 0)" : " AND SubjectComponentID = '$parCmpSubjectId'";
			}
			else {
			    $conds .= ($cmp_subject_code=="") ? " AND (SubjectComponentCode IS NULL OR SubjectComponentCode = '')" : " AND SubjectComponentCode = '$cmp_subject_code'";
			}
			
			$conds .= ($sem==$ec_iPortfolio['overall_result']) ? " AND IsAnnual = 1 " : " AND Semester = '$sem' ";
			
			
			
			if ($parSubjectId != '') {
			    $subjectCond = " AND SubjectID = '$parSubjectId' ";
			}
			else {
			    $subjectCond = " AND SubjectCode = '$subject_code' ";
			}
			
			if ($parTermAssessment != '') {
			    $termAssessmentCond = " AND TermAssessment = '$parTermAssessment' ";
			}
			else {
			    $termAssessmentCond = " AND (TermAssessment is null or TermAssessment = '') ";
			}
			
			
			$sql = "SELECT EN_SNAME FROM {$intranet_db}.ASSESSMENT_SUBJECT WHERE RecordStatus = 1";
			$SubjectNameArr = $this->returnVector($sql);
			
			
			$sql = "SELECT yc.ClassTitleEN FROM ACADEMIC_YEAR as ay
                    INNER JOIN YEAR_CLASS AS yc ON yc.AcademicYearID = ay.AcademicYearID
                WHERE ay.YearNameEN = '$year' AND yc.YearID = '".$levelID."'";
			
			
			$ClassNameArr = $this->returnVector($sql);
	
			$sql = "SELECT
			             Score
			         FROM
            			{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD 
            			
                    WHERE
            			(Year = '$year')
            			$subjectCond           			
            			AND (Score > 0 OR (Score=0 AND Grade<>'' AND OrderMeritForm>0))
            			AND SubjectCode IN ('".implode("','",$SubjectNameArr)."')
                        AND ClassName IN ('".implode("','",$ClassNameArr)."')
            			$conds
            			$termAssessmentCond
			";
			
			# Retrieve ranks for calculating standard deviation
// 			$sql = "SELECT
// 						ASSR.Score
// 					FROM
// 						{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD AS ASSR
// 						INNER JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS SUBJ ON SUBJ.EN_SNAME=ASSR.SubjectCode
// 						INNER JOIN {$intranet_db}.YEAR_CLASS as yc ON ASSR.ClassName = yc.ClassTitleEN
// 						INNER JOIN {$intranet_db}.ACADEMIC_YEAR as ay ON yc.AcademicYearID = ay.AcademicYearID
// 					WHERE
// 						(ASSR.Year = '$year' OR ay.YearNameEN = '$year') 
// 						$subjectCond
// 						AND (ASSR.Score > 0 OR (ASSR.Score=0 AND ASSR.Grade<>'' AND ASSR.OrderMeritForm>0))
// 						and SUBJ.RecordStatus = 1
// 						$conds
// 						$termAssessmentCond
// 					";
			$ScoreArr = $this->returnVector($sql);

			$sd = round(standard_deviation($ScoreArr), 2);

			if($sd>0)
			{
				# Calculate the standard score
				$mean = array_sum($ScoreArr)/sizeof($ScoreArr);
				$z = round((($score-$mean)/$sd), 2);
			}
		}
		return $z;
	}
	
	function returnStudentAssessmentClassLevel($UserID, $Year)
	{
		global $intranet_db, $eclass_db;

		$sql = "SELECT DISTINCT
					b.YearID
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
					LEFT JOIN {$intranet_db}.YEAR_CLASS as b ON a.ClassName = b.ClassTitleEN
				WHERE
					a.UserID = '".$UserID."'
					AND a.Year = '".$Year."'";
		$row = $this->returnVector($sql);

		return $row[0];
	}
	
	function getAssessmentMaxScore($SubjectCode, $isAnnual, $SubjectCmpCode="")
	{
		global $eclass_db;

		$conds .= ($isAnnual) ? " WHERE IsAnnual = 1" : " WHERE IsAnnual = 0";
		if($SubjectCode!="")
		{
			$conds .= ($SubjectCmpCode!="") ? " AND SubjectCode = '".$SubjectCode."' AND SubjectComponentCode = '".$SubjectCmpCode."'" : " AND SubjectCode = '".$SubjectCode."' AND SubjectComponentCode IS NULL";
		}
		$sql = "SELECT
					MAX(Score)
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
				$conds
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}
	
	# This function retrieve english description only
	function returnSubjectDescription($SubjectCode, $SubjectComponentCode)
	{
		global $eclass_db, $intranet_db;

		if($SubjectComponentCode=="")
		{
			$sql = "SELECT
						SUBJ.EN_DES AS SubjectName
					FROM
						{$intranet_db}.ASSESSMENT_SUBJECT AS SUBJ
					WHERE
						SUBJ.EN_SNAME = '$SubjectCode'
						and SUBJ.RecordStatus = 1
					";
		}
		else
		{
			$sql = "SELECT
						CONCAT(SUBJ.EN_DES, ' ', CMP.EN_DES) AS SubjectName
					FROM
						{$intranet_db}.ASSESSMENT_SUBJECT AS SUBJ
						LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS CMP ON (CMP.CODEID=SUBJ.CODEID and CMP.RecordStatus = 1)
					WHERE
						SUBJ.EN_SNAME = '$SubjectCode'
						AND CMP.EN_SNAME = '$SubjectComponentCode'
						and SUBJ.RecordStatus = 1
					";
		}
		$ReturnArray = $this->returnVector($sql);

		return $ReturnArray[0];
	}
	
	#### Function for assessment records to check wether the subject code is valid
	function checkSubjectCode($subject_code)
	{
		global $eclass_db, $intranet_db;

		$sql = "SELECT COUNT(RecordID) FROM {$intranet_db}.ASSESSMENT_SUBJECT WHERE EN_SNAME = '$subject_code' and RecordStatus = 1";
		$row = $this->returnVector($sql);

		return $row[0];
	}
	
		# Display Student Attendance Summary
	function displayStudentAttendanceSummary($StudentID, $ClassName, $PassChooseYear="", $ParChangeLayer=false)
    {
		global $ec_iPortfolio, $i_Profile_Absent, $i_Profile_Late, $i_Profile_EarlyLeave, $no_record_msg;

		# get the attendance summary
		$result = $this->returnStudentAttendanceSummary($StudentID, $PassChooseYear);
		
		# get Years and ClassName according to ASSESSMENT_STUDENT_SUBJECT_RECORD
		$YearClassArr = $this->returnFilteredYearClass($StudentID, 1,$PassChooseYear);
/*
		# retrieve school semesters
		list($SemesterArr, $ShortSemesterArr) = $this->getSemesterNameArrFromIP();
		$SemesterArr[] = $ec_iPortfolio['whole_year'];
*/
		//Table Header
		$x = "<table width='100%' border='0' cellpadding='4' cellspacing='0' bgcolor=#CCCCCC>\n";
		$x .= "<tr class='tabletop' height=35>\n";
		//$x .= "<td height='35' width='5%'>&nbsp;</td>\n";
		/*
		$x .= "<td nowrap class='tabletopnolink' width='46%'><b>".$ec_iPortfolio['year']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' width='15%'><b>".$ec_iPortfolio['class']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='15%'><b>".$ec_iPortfolio['semester']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='8%'><b>".$i_Profile_Absent."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='8%'><b>".$i_Profile_Late."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='8%'><b>".$i_Profile_EarlyLeave."</b></td>";
		$x .= "</tr>\n";
		*/

		$x .= "<td nowrap class='tabletopnolink' width='20%'><b>".$ec_iPortfolio['year']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' width='10%'><b>".$ec_iPortfolio['class']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='20%'><b>".$ec_iPortfolio['semester']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='12%'><b>".$i_Profile_Absent."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='12%'><b>".$i_Profile_Late."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='12%'><b>".$i_Profile_EarlyLeave."</b></td>";
		$x .= "</tr>\n";
		
		//Table Content
		if(!empty($YearClassArr))
		{
			$num = 0;
			foreach($YearClassArr as $year => $ClassName)
			{
				$CurrYear = "";
				
				# Eric Yip (20100211): Get semester array according to year name
				# (Not work for years with same name)
				unset($SemesterArr);
				unset($SemesterDisplayArr);
				$sql = "SELECT AcademicYearID FROM {$intranet_db}.ACADEMIC_YEAR WHERE YearNameEN = '".$year."'";
				$ay_id = current($this->returnVector($sql));
				// Must retrieve eng sem
				$SemesterArr = array();
				if($ay_id== ''){
					//for case , with year in the table:PROFILE_CLASS_HISTORY but without year id in t: ACADEMIC_YEAR 
				}else{
					//Year exist in the ACADEMIC_YEAR , get the related semesters
					
					$SemesterArr = array_values(getSemesters($ay_id, 1, 'en'));									
					// another set of sem arr for display only
					$SemesterDisplayArr = array_values(getSemesters($ay_id));				
				}
				
				$SemesterArr[] = $ec_iPortfolio['whole_year'];
				$SemesterDisplayArr[] = $ec_iPortfolio['whole_year'];
								
				//for($j=0; $j<sizeof($SemesterArr); $j++)
				for($j=0,$j_max = count($SemesterArr); $j<$j_max; $j++)
				{
					$sem = trim($SemesterArr[$j]);
					$sem_disp = trim($SemesterDisplayArr[$j]);
					if($year!=$CurrYear)
					{
						$bclass = ($num%2!=0) ? "class=tablerow2" : "class=tablerow1";
						$x .= "<tr $bclass>\n";
						//$x .= "<td>&nbsp;</td>\n";
						$x .= "<td class=tabletext>$year</td>\n";
						$x .= "<td class=tabletext>$ClassName</td>\n";
						$num++;
						$CurrYear = $year;
					}
					else
					{
						$x .= "<tr $bclass>\n";
						//$x .= "<td>&nbsp;</td>\n";
						$x .= "<td class=tabletext >&nbsp;</td>\n";
						$x .= "<td class=tabletext>&nbsp;</td>\n";
					}
					//$stylecolor = ($num%2==0) ? "#FFFFFF" : "#DDDDDD";
					//$style = ($sem!=$ec_iPortfolio['whole_year']) ? "style='border-bottom: 1px dashed $stylecolor;'" : "";
					//$x .= "<td align='center' class='tabletext tablerow_underline'><a href=\"javascript:newWindow('details.php?StudentID=$StudentID&ClassName=$ClassName&semester=$sem&year=$year', 19) \" class='navigation'>$sem</a></td>\n";
					if($ParChangeLayer)
						$x .= "<td align='center' class='tabletext tablerow_underline'><a href='#' onClick='jVIEW_DETAIL(\"attendance\", \"semester=$sem&year=$year\")' class='tablelink'>$sem_disp</a></td>\n";
					else
						$x .= "<td align='center' class='tabletext tablerow_underline'><a href='details.php?StudentID=$StudentID&ClassName=$ClassName&semester=$sem&year=$year' class='tablelink'>$sem_disp</a></td>\n";

					
					for($type=1; $type<=3; $type++)
					{
						$value = ($result[$year][$sem][$type]!='') ? $result[$year][$sem][$type] : 0;
						$x .= "<td align='center' class='tabletext tablerow_underline'>".$value."</td>\n";

						# get overall total
						if($sem==$ec_iPortfolio['whole_year'])
							$result[$ec_iPortfolio['total']][$type] = $result[$ec_iPortfolio['total']][$type] + $value;

					}
					$x .= "</tr>\n";
				}
			}
			$bclass = ($num%2!=0) ? "class=tablerow2" : "class=tablerow1";
			$x .= "<tr $bclass>\n";
			//$x .= "<td>&nbsp;</td>\n";
			$x .= "<td class=tabletext>&nbsp;</td>\n";
			$x .= "<td class=tabletext>&nbsp;</td>\n";
			$x .= "<td align='center' class=tabletext>[".$ec_iPortfolio['total']."]</td>\n";
			for($type=1; $type<=3; $type++)
			{
				$x .= "<td align='center' class=tabletext>".($result[$ec_iPortfolio['total']][$type]!='' ? $result[$ec_iPortfolio['total']][$type] : 0)."</td>\n";
			}
			$x .= "</tr>\n";
		}
		else
		{
			$x .= "<tr valign='middle'><td height='100' colspan='7' align='center' $bclass>".$no_record_msg."</td></tr>";
		}
        $x .= "</table>\n";

		return $x;
	}
	
	# Display Student Attendance Summary
	function displayStudentActivityAttendanceSummary($StudentID, $ClassName, $PassChooseYear="", $ParChangeLayer=false)
	{
		global $ec_iPortfolio, $i_Profile_Absent, $i_Profile_Late, $i_Profile_EarlyLeave, $no_record_msg, $Lang;
	
		# get the attendance summary
		$result = $this->returnActivityAttendanceSummary($StudentID, $PassChooseYear);
	
		# get Years and ClassName according to ASSESSMENT_STUDENT_SUBJECT_RECORD
		$YearClassArr = $this->returnFilteredYearClass($StudentID, 1,$PassChooseYear);
		/*
			# retrieve school semesters
			list($SemesterArr, $ShortSemesterArr) = $this->getSemesterNameArrFromIP();
			$SemesterArr[] = $ec_iPortfolio['whole_year'];
			*/
		//Table Header
		$x = "<table width='100%' border='0' cellpadding='4' cellspacing='0' bgcolor=#CCCCCC>\n";
		$x .= "<tr class='tabletop' height=35>\n";
		//$x .= "<td height='35' width='5%'>&nbsp;</td>\n";
		/*
			$x .= "<td nowrap class='tabletopnolink' width='46%'><b>".$ec_iPortfolio['year']."</b></td>";
			$x .= "<td nowrap class='tabletopnolink' width='15%'><b>".$ec_iPortfolio['class']."</b></td>";
			$x .= "<td nowrap class='tabletopnolink' align='center' width='15%'><b>".$ec_iPortfolio['semester']."</b></td>";
			$x .= "<td nowrap class='tabletopnolink' align='center' width='8%'><b>".$i_Profile_Absent."</b></td>";
			$x .= "<td nowrap class='tabletopnolink' align='center' width='8%'><b>".$i_Profile_Late."</b></td>";
			$x .= "<td nowrap class='tabletopnolink' align='center' width='8%'><b>".$i_Profile_EarlyLeave."</b></td>";
			$x .= "</tr>\n";
			*/
	
		$x .= "<td nowrap class='tabletopnolink' width='20%'><b>".$ec_iPortfolio['year']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' width='10%'><b>".$ec_iPortfolio['class']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='20%'><b>".$ec_iPortfolio['semester']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='12%'><b>".$Lang['StudentAttendance']['Present']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='12%'><b>".$Lang['StudentAttendance']['Waived']."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='12%'><b>".$i_Profile_Absent."</b></td>";
		$x .= "<td nowrap class='tabletopnolink' align='center' width='12%'><b>".$i_Profile_Late."</b></td>";		
		$x .= "</tr>\n";
	
		//Table Content
		if(!empty($YearClassArr))
		{
			$num = 0;
			foreach($YearClassArr as $year => $ClassName)
			{
				$CurrYear = "";
	
				# Eric Yip (20100211): Get semester array according to year name
				# (Not work for years with same name)
				unset($SemesterArr);
				unset($SemesterDisplayArr);
				$sql = "SELECT AcademicYearID FROM {$intranet_db}.ACADEMIC_YEAR WHERE YearNameEN = '".$year."'";
				$ay_id = current($this->returnVector($sql));
				// Must retrieve eng sem
				$SemesterArr = array();
				if($ay_id== ''){
					//for case , with year in the table:PROFILE_CLASS_HISTORY but without year id in t: ACADEMIC_YEAR
				}else{
					//Year exist in the ACADEMIC_YEAR , get the related semesters
						
					$SemesterArr = array_values(getSemesters($ay_id, 1, 'en'));
					// another set of sem arr for display only
					$SemesterDisplayArr = array_values(getSemesters($ay_id));
				}
	
				$SemesterArr[] = $ec_iPortfolio['whole_year'];
				$SemesterDisplayArr[] = $ec_iPortfolio['whole_year'];
	
				//for($j=0; $j<sizeof($SemesterArr); $j++)
				for($j=0,$j_max = count($SemesterArr); $j<$j_max; $j++)
				{
					$sem = trim($SemesterArr[$j]);
					$sem_disp = trim($SemesterDisplayArr[$j]);
					if($year!=$CurrYear)
					{
						$bclass = ($num%2!=0) ? "class=tablerow2" : "class=tablerow1";
						$x .= "<tr $bclass>\n";
						//$x .= "<td>&nbsp;</td>\n";
						$x .= "<td class=tabletext>$year</td>\n";
						$x .= "<td class=tabletext>$ClassName</td>\n";
						$num++;
						$CurrYear = $year;
					}
					else
					{
						$x .= "<tr $bclass>\n";
						//$x .= "<td>&nbsp;</td>\n";
						$x .= "<td class=tabletext >&nbsp;</td>\n";
						$x .= "<td class=tabletext>&nbsp;</td>\n";
					}
					//$stylecolor = ($num%2==0) ? "#FFFFFF" : "#DDDDDD";
					//$style = ($sem!=$ec_iPortfolio['whole_year']) ? "style='border-bottom: 1px dashed $stylecolor;'" : "";
					//$x .= "<td align='center' class='tabletext tablerow_underline'><a href=\"javascript:newWindow('details.php?StudentID=$StudentID&ClassName=$ClassName&semester=$sem&year=$year', 19) \" class='navigation'>$sem</a></td>\n";
					if($ParChangeLayer)
						$x .= "<td align='center' class='tabletext tablerow_underline'><a href='#' onClick='jVIEW_DETAIL(\"attendance\", \"semester=$sem&year=$year\")' class='tablelink'>$sem_disp</a></td>\n";
						else
							$x .= "<td align='center' class='tabletext tablerow_underline'><a href='details.php?StudentID=$StudentID&ClassName=$ClassName&semester=$sem&year=$year' class='tablelink'>$sem_disp</a></td>\n";
	
								
							for($type=1; $type<=4; $type++)
							{
								$value = ($result[$year][$sem][$type]!='') ? $result[$year][$sem][$type] : 0;
								$x .= "<td align='center' class='tabletext tablerow_underline'>".$value."</td>\n";
	
								# get overall total
								if($sem==$ec_iPortfolio['whole_year'])
									$result[$ec_iPortfolio['total']][$type] = $result[$ec_iPortfolio['total']][$type] + $value;
	
							}
							$x .= "</tr>\n";
				}
			}
			$bclass = ($num%2!=0) ? "class=tablerow2" : "class=tablerow1";
			$x .= "<tr $bclass>\n";
			//$x .= "<td>&nbsp;</td>\n";
			$x .= "<td class=tabletext>&nbsp;</td>\n";
			$x .= "<td class=tabletext>&nbsp;</td>\n";
			$x .= "<td align='center' class=tabletext>[".$ec_iPortfolio['total']."]</td>\n";
			for($type=1; $type<=4; $type++)
			{
				$x .= "<td align='center' class=tabletext>".($result[$ec_iPortfolio['total']][$type]!='' ? $result[$ec_iPortfolio['total']][$type] : 0)."</td>\n";
			}
			$x .= "</tr>\n";
		}
		else
		{
			$x .= "<tr valign='middle'><td height='100' colspan='7' align='center' $bclass>".$no_record_msg."</td></tr>";
		}
		$x .= "</table>\n";
	
		return $x;
	}
	
######################################################################
############### New function for generating assessment ###############
######################################################################

  function generateAssessmentReport4($ParStudentID, $ParDisplayBy)
  {
    global $image_path;
  
    $subjectArr = $this->getAssessmentSubject();
    $t_recArr = $this->getAssessmentStudentRecord($ParStudentID);

    for($i=0; $i<count($t_recArr); $i++)
    {
      $t_year = $t_recArr[$i]['Year'];
      $t_semester = $t_recArr[$i]['Semester'];
      $t_class = $t_recArr[$i]['ClassName'];
      $t_subjcode = $t_recArr[$i]['SubjectCode'];
      $t_subjcomcode = $t_recArr[$i]['SubjectComponentCode'];
      $t_subjscore = $t_recArr[$i]['Score'];
      $t_subjgrade = $t_recArr[$i]['Grade'];
      $t_subjorder = $t_recArr[$i]['OrderMeritForm'];

      if(!is_array($classArr[$t_year][$t_class]) || !in_array($t_semester, $classArr[$t_year][$t_class]))
        $classArr[$t_year][$t_class][] = $t_semester;
      $recSubjArr[$t_subjcode][$t_subjcomcode] = $subjectArr[$t_subjcode][$t_subjcomcode];
      $recArr[$t_year][$t_class][$t_semester][$t_subjcode][$t_subjcomcode] =  array(
                                                                                "Score" => $t_subjscore,
                                                                                "Grade" => $t_subjgrade,
                                                                                "Rank" => $t_subjorder
                                                                              );
    }
    
    $th_style = "class='retabletop tabletopnolink' style='border-bottom: 1px solid rgb(255, 255, 255);' align='center' valign='top'";
    if(is_array($classArr))
    {
      foreach($classArr AS $t_year => $t_class_arr)
      {
        $t_class_count = count($t_class_arr, true) - count($t_class_arr);
      
        $year_row .= "<td colspan='".$t_class_count."' ".$th_style.">".$t_year."</td>";
        
        foreach($t_class_arr AS $t_class => $t_semester_arr)
        {
          $t_sem_count = count($t_semester_arr);
        
          $class_row .= "<td colspan='".$t_sem_count."' ".$th_style.">".$t_class."</td>";
          
          foreach($t_semester_arr AS $t_semester)
          {
            $semester_row .= "<td ".$th_style.">".$t_semester."</td>";
          }
        }
      }
    }
    
    if(is_array($recSubjArr))
    {
      $i = 0;
      foreach($recSubjArr AS $t_subjcode => $t_subj_com_arr)
      {
        foreach($t_subj_com_arr AS $t_subjcomcode => $t_subj_name)
        {
          $subj_row = "<tr>";
          
          if($t_subjcode == $t_subjcomcode)
          {
            $td_subj_style = "class='tablelist_subject'";
          }
          else
          {
            $td_subj_style = "class='tablelist_sub_subject'";
          }
          $subj_row .= "<td ".$td_subj_style.">".$t_subj_name."</td>";
          $subj_row .= "<td ".$td_subj_style."><a href='#'><img src='".$image_path."/2009a/iPortfolio/icon_resultview.gif' width='20' height='20' border='0'></a></td>";
          
          if(is_array($classArr))
          {
            $td_style = ($i%2 == 0) ? "retablerow1" : "retablerow2";
            foreach($classArr AS $t_year => $t_class_arr)
            {
              foreach($t_class_arr AS $t_class => $t_semester_arr)
              {
                foreach($t_semester_arr AS $t_semester)
                {
                  if(isset($recArr[$t_year][$t_class][$t_semester][$t_subjcode][$t_subjcomcode][$ParDisplayBy]))
                  {
                    $subj_row .= "<td align='center' class='".$td_style." tabletext'>".$recArr[$t_year][$t_class][$t_semester][$t_subjcode][$t_subjcomcode][$ParDisplayBy]."</td>";
                  }
                  else
                  {
                    $subj_row .= "<td align='center' class='".$td_style." tabletext'>--</td>";
                  }
                }
              }
            }
          }
          
          $subj_row .= "</tr>";
          $subj_row_arr[] = $subj_row;
          
          $i++;
        }
      }
    }
    
    $x = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
    $x .= "<tr><td colspan='2'>&nbsp;</td>".$year_row."</tr>";
    $x .= "<tr><td colspan='2'>&nbsp;</td>".$class_row."</tr>";
    $x .= "<tr><td colspan='2'>&nbsp;</td>".$semester_row."</tr>";
    $x .= implode("\n", $subj_row_arr);
    $x .= "</table>";
    
    return $x;
  }
  
  function getAssessmentStudentRecord($ParStudentID)
  {
    global $eclass_db, $ec_iPortfolio;
  
    $sql =  "
              SELECT DISTINCT
                Year,
                if(Semester = '', '".$ec_iPortfolio['overall_result']."', Semester) AS Semester,
                ClassName,
                SubjectCode,
                IF(SubjectComponentCode = '' OR SubjectComponentCode IS NULL, SubjectCode, SubjectComponentCode) AS SubjectComponentCode,
                Score,
                Grade,
                OrderMeritForm
              FROM
                {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
              WHERE 
                UserID = '".$ParStudentID."'
              ORDER BY
                Year ASC
            ";
    $returnArr = $this->returnArray($sql);

    return $returnArr;
  }
  
  function getAssessmentSubject()
  {
    global $intranet_db, $intranet_session_language;
    
    if($intranet_session_language == "b5")
      $subject_name_field = "c_subj.CH_DES";
    else
      $subject_name_field = "c_subj.EN_DES";
    
    $sql =  "
              SELECT
                m_subj.EN_SNAME AS SubjCode,
                c_subj.EN_SNAME AS SubjComCode,
                {$subject_name_field} AS SubjName
              FROM
                {$intranet_db}.ASSESSMENT_SUBJECT AS m_subj
              LEFT JOIN
                {$intranet_db}.ASSESSMENT_SUBJECT AS c_subj
              ON
                c_subj.CODEID = m_subj.CODEID and c_subj.RecordStatus = 1
              WHERE
                (m_subj.CMP_CODEID = '' OR m_subj.CMP_CODEID IS NULL)
				and m_subj.RecordStatus = 1
            ";

    $subjArr = $this->returnArray($sql);
    
    $returnArr = array();
    for($i=0; $i<count($subjArr); $i++)
    {
      $t_subj_code = $subjArr[$i]['SubjCode'];
      $t_subj_comcode = $subjArr[$i]['SubjComCode'];
      $t_subj_name = $subjArr[$i]['SubjName'];
    
      $returnArr[$t_subj_code][$t_subj_comcode] = $t_subj_name;
    }
    
    return $returnArr;
  }

  /********************************************************************************
   * Eric Yip (20100730): Functions for new method to retrieve assessment data
   * for display
   *    
   * PLEASE DON'T ADD/REMOVE/MODIFY FUNCTIONS WITHIN THIS SECTION UNLESS
   *  YOU KNOW THE FLOW CLEARLY
   * 
   * THIS IS THE START OF THIS SECTION
   ********************************************************************************/     
  function getAssessmentClassHistoryArr($ParStudentID)
  {
    global $eclass_db, $intranet_db;
  	
  	// 2013-0325-0956-31156: failed to merge YearTermID "null" and "0" 
    //$sql = "SELECT DISTINCT AcademicYearID, Year, YearTermID, Semester, ClassName ";
    $sql = " SELECT assr.AcademicYearID, assr.Year, If(assr.YearTermID is null, 0, assr.YearTermID) as YearTermID, assr.Semester, assr.ClassName ";
    $sql .= " FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr ";
    $sql .= " LEFT JOIN {$intranet_db}.ACADEMIC_YEAR_TERM ayt ON assr.YearTermID = ayt.YearTermID";
    $sql .= " WHERE assr.UserID = '$ParStudentID' ";
    $sql .= " Group By assr.AcademicYearID, assr.Year, If(assr.YearTermID is null, 0, assr.YearTermID), assr.Semester, assr.ClassName ";
    $sql .= " ORDER BY assr.Year, IF(assr.YearTermID IS NULL,'9999-99-99', ayt.TermStart)";
    $classHistoryArr = $this->returnArray($sql);
    
    return $classHistoryArr;
  }
  
  function buildInsertTempAssessmentTableSQL_SD($ParTableName, $ParSubjectID, $ParSubjectCodeField, $ParSubjectComponentCodeField, $ParStudentID, $ParAcademicYearID, $ParYearTermID, $hideParentSubjectDisplay=false)
  {
    global $eclass_db, $intranet_db;
    
    $subject_code_id_value = ($ParSubjectID=="") ? "0" : $ParSubjectID;
    $subject_code_value = ($ParSubjectCodeField=="") ? "'##Overall##'" : $ParSubjectCodeField;
    $subject_comp_code_value = ($ParSubjectComponentCodeField=="") ? "'##Overall##'" : $ParSubjectComponentCodeField;
    
    $score_field = ($ParYearTermID=="") ? "Score_{$ParAcademicYearID}" : "Score_{$ParAcademicYearID}_{$ParYearTermID}";
    $conds = "UserID = '{$ParStudentID}' AND AcademicYearID = '".$ParAcademicYearID."'";
    $conds .= ($ParYearTermID=="") ? " AND (YearTermID IS NULL OR YearTermID IN (0, ''))" : " AND YearTermID = '".$ParYearTermID."'";
    
    $sql = "INSERT INTO tempAssessmentMark (SubjectCodeID, SubjectCode, SubjectComponentCode, {$score_field}) ";
    $sql .= "SELECT {$subject_code_id_value}, {$subject_code_value}, {$subject_comp_code_value}, ";
    $sql .= "CASE ";
    if($hideParentSubjectDisplay) {
        $sql .= "WHEN (SubjectComponentID = 0 OR SubjectComponentID = '' OR SubjectComponentID IS NULL) THEN '--' ";     // [2017-0901-1527-45265]
    }
    $sql .= "WHEN Score < 0 THEN '--' ";
    $sql .= "WHEN (ABS(Score) = 0) THEN IF(IFNULL(Grade, '') = '', '--', 0) ";
    $sql .= "ELSE TRIM('.' FROM TRIM(0 FROM ROUND(Score, 1))) ";
    $sql .= "END ";
    $sql .= "FROM {$eclass_db}.{$ParTableName} asr ";
    $sql .= "WHERE {$conds} ";
    $sql .= "ON DUPLICATE KEY UPDATE {$score_field} = VALUES({$score_field})";
    $sqlArr[] = $sql;
    
    // Sub-query to get year classes of whole form
    $sql = "SELECT t_yc.YearID ";
    $sql .= "FROM {$intranet_db}.YEAR_CLASS t_yc ";
    $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER t_ycu ON t_ycu.YearClassID = t_yc.YearClassID ";
    $sql .= "WHERE t_ycu.UserID = '".$ParStudentID."' AND t_yc.AcademicYearID = '".$ParAcademicYearID."'";
    $yearID = current($this->returnVector($sql));

    $conds = "yc.YearID = '".$yearID."' AND asr.AcademicYearID = '".$ParAcademicYearID."' AND yc.AcademicYearID = '".$ParAcademicYearID."'";
//     $conds .= " AND asr.Score IS NOT NULL AND (IF(asr.Score > 0, true, IF(asr.Score < 0, false, IFNULL(asr.Grade, '') <> '')))";
    $conds .= " AND TermAssessment IS NULL AND asr.Score IS NOT NULL AND (IF(asr.Score > 0, true, IF(asr.Score < 0, false, IFNULL(asr.Grade, '') <> '')))";
    $conds .= ($ParYearTermID=="") ? " AND (asr.YearTermID IS NULL OR YearTermID IN (0, ''))" : " AND asr.YearTermID = '".$ParYearTermID."'";
    
    // if no record is  TermAssessment IS NULL show TermAssessment result
    $sql2 = "SELECT count(*) as count ";
    $sql2 .= "FROM {$eclass_db}.{$ParTableName} asr ";
    $sql2 .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON asr.UserID = ycu.UserID ";
    $sql2 .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID ";
    $sql2 .= "WHERE {$conds} ";
    $result = $this->returnResultSet($sql2);
    if($result[0]['count'] == 0){
        $conds = "yc.YearID = '".$yearID."' AND asr.AcademicYearID = '".$ParAcademicYearID."' AND yc.AcademicYearID = '".$ParAcademicYearID."'";
        $conds .= " AND asr.Score IS NOT NULL AND (IF(asr.Score > 0, true, IF(asr.Score < 0, false, IFNULL(asr.Grade, '') <> '')))";
        $conds .= ($ParYearTermID=="") ? " AND (asr.YearTermID IS NULL OR YearTermID IN (0, ''))" : " AND asr.YearTermID = '".$ParYearTermID."'";
    }
    
    $sql = "INSERT INTO tempAssessmentAvgMark (SubjectCodeID, SubjectCode, SubjectComponentCode, {$score_field}) ";
    $sql .= "SELECT {$subject_code_id_value}, {$subject_code_value}, {$subject_comp_code_value}, AVG(Score) ";
    $sql .= "FROM {$eclass_db}.{$ParTableName} asr ";
    $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON asr.UserID = ycu.UserID ";
    $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID ";
    $sql .= "WHERE {$conds} ";
    $sql .= ($ParSubjectCodeField=="") ? "" : "GROUP BY asr.SubjectCode, asr.SubjectComponentCode ";
    $sql .= "ON DUPLICATE KEY UPDATE {$score_field} = VALUES({$score_field})";
    $sqlArr[] = $sql;
    
    $sql = "INSERT INTO tempAssessmentSDMark (SubjectCodeID, SubjectCode, SubjectComponentCode, {$score_field}) ";
    $sql .= "SELECT {$subject_code_id_value}, {$subject_code_value}, {$subject_comp_code_value}, STD(Score) ";
    $sql .= "FROM {$eclass_db}.{$ParTableName} asr ";
    $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON asr.UserID = ycu.UserID ";
    $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID ";
    $sql .= "WHERE {$conds} ";
    $sql .= ($ParSubjectCodeField=="") ? "" : "GROUP BY asr.SubjectCode, asr.SubjectComponentCode ";
    $sql .= "ON DUPLICATE KEY UPDATE {$score_field} = VALUES({$score_field})";
    $sqlArr[] = $sql;

    return $sqlArr;
  }
  
  function buildInsertTempAssessmentTableSQL($ParTableName, $ParSubjectID, $ParSubjectCodeField, $ParSubjectComponentCodeField, $ParRetrieveField, $ParStudentID, $ParAcademicYearID, $ParYearTermID, $exceptEmptyGrade = false, $alwaysDisplayScoreIfValid = false)
  {
    global $eclass_db;
    
    $subject_code_id_field = ($ParSubjectID=="") ? "" : "SubjectCodeID,";
    $subject_code_id_value = ($ParSubjectID=="") ? "" : "{$ParSubjectID},";
    $score_field = ($ParYearTermID=="") ? "Score_{$ParAcademicYearID}" : "Score_{$ParAcademicYearID}_{$ParYearTermID}";
    $conds = "UserID = '{$ParStudentID}' AND AcademicYearID = '".$ParAcademicYearID."'";
    $conds .= ($ParYearTermID=="") ? " AND (YearTermID IS NULL OR YearTermID IN (0, ''))" : " AND YearTermID = '".$ParYearTermID."'";
    //$conds .= ' And (OrderMeritForm != -1 Or OrderMeritForm Is Null) ';
    //$conds .= ' And ((OrderMeritForm != -1 And OrderMeritForm != 0) Or OrderMeritForm Is Null Or Grade != \'\') ';
    $conds .= ' AND TermAssessment IS NULL ';
    if(!$exceptEmptyGrade){
    	$conds .= 'AND ((OrderMeritForm != -1 AND OrderMeritForm != 0) OR OrderMeritForm IS NULL OR Grade != \'\'';
    	// [2020-0612-1004-37207]
    	if($alwaysDisplayScoreIfValid) {
    	    $conds .= ' OR Score > 0';
        }
    	$conds .= ') ';
    }
    
    // if no record is  TermAssessment IS NULL show TermAssessment result 
    $sql2 = "SELECT {$subject_code_id_value} {$ParSubjectCodeField}, {$ParSubjectComponentCodeField}, {$ParRetrieveField} ";
    $sql2 .= "FROM {$eclass_db}.{$ParTableName} ";
    $sql2 .= "WHERE {$conds} ";
    $result = $this->returnResultSet($sql2);
    if(count($result) == 0){
        $conds = "UserID = '{$ParStudentID}' AND AcademicYearID = '".$ParAcademicYearID."'";
        $conds .= ($ParYearTermID=="") ? " AND (YearTermID IS NULL OR YearTermID IN (0, ''))" : " AND YearTermID = '".$ParYearTermID."'";
        $conds .= ' AND TermAssessment IS NOT NULL ';
        if(!$exceptEmptyGrade){
        	$conds .= ' AND ((OrderMeritForm != -1 AND OrderMeritForm != 0) OR OrderMeritForm IS NULL OR Grade != \'\'';
            // [2020-0612-1004-37207]
        	if($alwaysDisplayScoreIfValid) {
                $conds .= ' OR Score > 0';
            }
        	$conds .= ') ';
        }
    }
    
    $sql = "INSERT INTO tempAssessmentMark ({$subject_code_id_field} SubjectCode, SubjectComponentCode, {$score_field}) ";
    $sql .= "SELECT {$subject_code_id_value} {$ParSubjectCodeField}, {$ParSubjectComponentCodeField}, {$ParRetrieveField} ";
    $sql .= "FROM {$eclass_db}.{$ParTableName} ";
    $sql .= "WHERE {$conds} ";
    $sql .= "ON DUPLICATE KEY UPDATE {$score_field} = VALUES({$score_field})";

    return $sql;
  }
  
  function createTempAssessmentMarkTable(&$dbObj, $ParAssessTableField, $ParSQLArr)
  {
		$sql = "CREATE TEMPORARY TABLE tempAssessmentMark (SubjectCodeID int(11), SubjectCode varchar(20), SubjectComponentCode varchar(20) {$ParAssessTableField}, PRIMARY KEY (SubjectCode, SubjectComponentCode)) DEFAULT CHARSET=utf8";
		$dbObj->db_db_query($sql);
		
		for($i=0, $i_max=count($ParSQLArr); $i<$i_max; $i++)
		{
	      $sql = $ParSQLArr[$i];
	      $dbObj->db_db_query($sql);
	    }  
  }
  
  function createTempAssessmentSubjectTable(&$dbObj, $withParentName = false)
  {
    global $intranet_db;
  
    	if($withParentName){
    		$componentNameSQL = Get_Lang_Selection("CONCAT(m.CH_DES,' - ',c.CH_DES)", "CONCAT(m.EN_DES,' - ',c.EN_DES)");
    	}else{
    		$componentNameSQL = Get_Lang_Selection("c.CH_DES", "c.EN_DES");
    	}
    	
		$sql = "CREATE TEMPORARY TABLE tempAssessmentSubject (MainSubjCodeID int(11), MainSubjCode varchar(20), CompSubjCodeID int(11), CompSubjCode varchar(20), SubjName varchar(255), MainSubjOrder int(11), CompSubjOrder int(11), CODEID varchar(20), PRIMARY KEY(MainSubjCodeID, MainSubjCode, CompSubjCodeID, CompSubjCode)) DEFAULT CHARSET=utf8";
		$dbObj->db_db_query($sql);
		
		$sql = "INSERT IGNORE INTO tempAssessmentSubject (MainSubjCodeID, MainSubjCode, CompSubjCodeID, CompSubjCode, SubjName, MainSubjOrder, CompSubjOrder, CODEID) ";
		$sql .= "SELECT m.RecordID, m.en_sname, IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', null, c.RecordID), IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', '', c.en_sname), IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', ".Get_Lang_Selection("m.CH_DES", "m.EN_DES").", ".$componentNameSQL."), m.DisplayOrder, IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', 0, c.DisplayOrder), m.CODEID ";
		//$sql .= "SELECT m.RecordID, m.RecordID, c.CODEID, IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', '', c.RecordID), IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', ".Get_Lang_Selection("m.CH_DES", "m.EN_DES").", ".Get_Lang_Selection("c.CH_DES", "c.EN_DES")."), m.DisplayOrder, IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', 0, c.DisplayOrder) ";
	    $sql .= "FROM {$intranet_db}.ASSESSMENT_SUBJECT AS m ";
	    $sql .= "LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS c ON m.codeid = c.codeid and c.RecordStatus = 1 ";
	    $sql .= "WHERE (m.CMP_CODEID IS NULL or TRIM(m.CMP_CODEID) = '') and m.RecordStatus = 1 ";
	    $sql .= "ORDER BY m.DisplayOrder, c.DisplayOrder";
		$dbObj->db_db_query($sql);
//		debug_pr($this->returnResultSet('select * from tempAssessmentSubject'));
  }
  
  function createTempAssessmentSDScoreTable(&$dbObj, $ParAssessTableField, $ParSQLArr)
  {
		$sql = "CREATE TEMPORARY TABLE tempAssessmentMark (SubjectCodeID int(11), SubjectCode varchar(20), SubjectComponentCode varchar(20) {$ParAssessTableField}, PRIMARY KEY (SubjectCode, SubjectComponentCode)) DEFAULT CHARSET=utf8";
		$dbObj->db_db_query($sql);
		$sql = "CREATE TEMPORARY TABLE tempAssessmentAvgMark (SubjectCodeID int(11), SubjectCode varchar(20), SubjectComponentCode varchar(20) {$ParAssessTableField}, PRIMARY KEY (SubjectCode, SubjectComponentCode)) DEFAULT CHARSET=utf8";
		$dbObj->db_db_query($sql);
		$sql = "CREATE TEMPORARY TABLE tempAssessmentSDMark (SubjectCodeID int(11), SubjectCode varchar(20), SubjectComponentCode varchar(20) {$ParAssessTableField}, PRIMARY KEY (SubjectCode, SubjectComponentCode)) DEFAULT CHARSET=utf8";
		$dbObj->db_db_query($sql);
		
		for($i=0, $i_max=count($ParSQLArr); $i<$i_max; $i++)
		{
      $sql = $ParSQLArr[$i];
      $dbObj->db_db_query($sql);
    }  
  } 
  /********************************************************************************
   * THIS IS THE END OF THIS SECTION
   ********************************************************************************/
	
	function Get_Student_Award_Record($StudentIDArr='', $AcademicYearIDArr='') {
		global $eclass_db;
		
		if ($StudentIDArr != '') {
			$conds_StudentID = " And UserID In ('".implode("','", (array)$StudentIDArr)."') ";
		}
		
		if ($AcademicYearIDArr != '') {
			$conds_AcademicYearID = " And AcademicYearID In ('".implode("','", (array)$AcademicYearIDArr)."') ";
		}
		
		$AWARD_STUDENT = $eclass_db.'.AWARD_STUDENT';
		$sql = "Select
						RecordID,
						UserID,
						AcademicYearID,
						YearTermID,
						AwardName
				From 
						$AWARD_STUDENT
				Where
						1
						$conds_StudentID
						$conds_AcademicYearID
				Order By
						AwardName
				";
		return $this->returnArray($sql);
	}
	
	function getStudentAcademicResultDisplayWithAssessment($parStudentId, $parDisplayBy, $parHideStat=false) {
		global $PATH_WRT_ROOT, $Lang, $ec_iPortfolio, $image_path, $sys_custom;
		
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		$lscm = new subject_class_mapping();
		$lfcm = new form_class_manage();
		
		// get all subjects
		$subjectAry = $lscm->Get_Subject_List_With_Component();
		$numOfSubject = count($subjectAry);
		$subjectAssoAry = BuildMultiKeyAssoc($subjectAry, 'SubjectID');
		
		// get all academic years
		if ($sys_custom['iPf']['showAcademicResultOrderByAcademicYearDesc']) {
			$SortOrder = 'desc';
		}
		else {
			$SortOrder = 'asc';
		}
		$academicYearAry = $lfcm->Get_Academic_Year_List('', $OrderBySequence=false, $excludeYearIDArr=array(), $NoPastYear=0, $PastAndCurrentYearOnly=0, $ExcludeCurrentYear=0, $ComparePastYearID='', $SortOrder);
		$numOfAcademicYear = count($academicYearAry);
		
		// get all terms
		$sql = 'Select AcademicYearID, YearTermID, YearTermNameEN, YearTermNameB5 From ACADEMIC_YEAR_TERM order by TermStart';
		$yearTermAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), array('AcademicYearID', 'YearTermID'));
		foreach ((array)$yearTermAssoAry as $_academicYearId => $_termAssoAry) {
			$yearTermAssoAry[$_academicYearId][0] = array();
		}
		
		// get student academic results
		$subjectAcademicResultAry = $this->getStudentSubjectAcademicResult($parStudentId);
		$numOfSubjectRecord = count($subjectAcademicResultAry);
		$subjectMainResultAry = $this->getStudentMainAcademicResult($parStudentId);
		$numOfMainRecord = count($subjectMainResultAry);
		
		// get class history
		$classHistoryAssoAry = BuildMultiKeyAssoc($this->getStudentClassHistory($parStudentId), 'AcademicYearID');
		
		
		// consolidate year display
		$consolidatedSubjectDataAry = array();
		for ($i=0; $i<$numOfSubjectRecord; $i++) {
			$_academicYearId = $subjectAcademicResultAry[$i]['AcademicYearID'];
			$_yearTermId = $subjectAcademicResultAry[$i]['YearTermID'];
			$_termAssessment = $subjectAcademicResultAry[$i]['TermAssessment'];
			$_subjectId = $subjectAcademicResultAry[$i]['SubjectID'];
			$_subjectComponentId = $subjectAcademicResultAry[$i]['SubjectComponentID'];
			$_score = $subjectAcademicResultAry[$i]['Score'];
			$_grade = $subjectAcademicResultAry[$i]['Grade'];
			$_formRanking = $subjectAcademicResultAry[$i]['OrderMeritForm'];
			
			if ($_yearTermId == '') {
				$_yearTermId = 0;
			} 
			if ($_termAssessment == '') {
				$_termAssessment = 'zoverall';	// start with "z" to sort overall to bottom
			}
			
			$_targetSubjectId = '';
			if ($_subjectComponentId > 0) {
				$_targetSubjectId = $_subjectComponentId;
			}
			else {
				$_targetSubjectId = $_subjectId;
			}
			
			if ($_score >= 0 || $_grade != '' || $_formRanking > 0) {
				$consolidatedSubjectDataAry[$_targetSubjectId][$_academicYearId][$_yearTermId][$_termAssessment]['score'] = ($_score==-1)? '' : $_score;
				$consolidatedSubjectDataAry[$_targetSubjectId][$_academicYearId][$_yearTermId][$_termAssessment]['grade'] = $_grade;
				$consolidatedSubjectDataAry[$_targetSubjectId][$_academicYearId][$_yearTermId][$_termAssessment]['formRanking'] = ($_formRanking==-1)? '' : $_formRanking;
			}
		}
		
		for ($i=0; $i<$numOfMainRecord; $i++) {
			$_academicYearId = $subjectMainResultAry[$i]['AcademicYearID'];
			$_yearTermId = $subjectMainResultAry[$i]['YearTermID'];
			$_termAssessment = $subjectMainResultAry[$i]['TermAssessment'];
			$_score = $subjectMainResultAry[$i]['Score'];
			$_grade = $subjectMainResultAry[$i]['Grade'];
			$_formRanking = $subjectMainResultAry[$i]['OrderMeritForm'];
			
			if ($_yearTermId == '') {
				$_yearTermId = 0;
			} 
			if ($_termAssessment == '') {
				$_termAssessment = 'zoverall';	// start with "z" to sort overall to bottom
			}
			
			$_targetSubjectId = 0;
			if ($_score >= 0 || $_grade != '' || $_formRanking > 0) {
				$consolidatedSubjectDataAry[$_targetSubjectId][$_academicYearId][$_yearTermId][$_termAssessment]['score'] = ($_score==-1)? '' : $_score;
				$consolidatedSubjectDataAry[$_targetSubjectId][$_academicYearId][$_yearTermId][$_termAssessment]['grade'] = $_grade;
				$consolidatedSubjectDataAry[$_targetSubjectId][$_academicYearId][$_yearTermId][$_termAssessment]['formRanking'] = ($_formRanking==-1)? '' : $_formRanking;
			}
		}
		
		// consolidate academic year term assessment list
		$tmpYearTermAssessmentAry = array();
		foreach ((array)$consolidatedSubjectDataAry as $_subjectId => $_subjectDataAry) {
			foreach ((array)$_subjectDataAry as $__academicYearId => $__subjectAcademicYearDataAry) {
				foreach ((array)$__subjectAcademicYearDataAry as $___yearTermId => $___subjectAcademicYearTermDataAry) {
					foreach ((array)$___subjectAcademicYearTermDataAry as $____termAssessment => $____subjectAcademicYearTermAssessmentDataAry) {
						$tmpYearTermAssessmentAry[$__academicYearId][$___yearTermId][] = $____termAssessment;
					}
				}
			}
		}
		$consolidatedYearTermAssessmentAry = array();
		foreach ((array)$tmpYearTermAssessmentAry as $_academicYearId => $_academicYearDataAry) {
			foreach ((array)$_academicYearDataAry as $__yearTermId => $__yearTermDataAry) {
// 				$consolidatedYearTermAssessmentAry[$_academicYearId][$__yearTermId]= array_values(array_unique($__yearTermDataAry));
				$__sortedYearTermAssessmentAry = array_values(array_unique($__yearTermDataAry));
				sort($__sortedYearTermAssessmentAry);
				$consolidatedYearTermAssessmentAry[$_academicYearId][$__yearTermId] = $__sortedYearTermAssessmentAry;
			}
		}
		
		// add back the subject if it is a parent subject with component subject has score
		foreach ((array)$consolidatedSubjectDataAry as $_subjectId => $_subjectDataAry) {
			$_parentSubjectId = $subjectAssoAry[$_subjectId]['ParentSubjectID'];
			
			if (!isset($consolidatedSubjectDataAry[$_parentSubjectId])) {
				$consolidatedSubjectDataAry[$_parentSubjectId] = array();
			}
		}
		
		
		$x = '';
		if (count($consolidatedSubjectDataAry) == 0) {
			$x .= $Lang['General']['NoRecordAtThisMoment'];
		}
		else {
			$x .= '<table width="100%" cellspacing="1" cellpadding="5" border="0" class="table_b">'."\r\n";
				$x .= '<tbody>'."\r\n";
					// academic year and class row
					$x .= '<tr class="tabletop">'."\r\n";
						// empty column for subject 
						$x .= '<td width="15%" nowrap="nowrap" style="background:#FFFFFF">&nbsp;</td>'."\r\n";
						
						// loop academic year display
						for ($i=0; $i<$numOfAcademicYear; $i++) {	// loop $academicYearAry to maintain academic year sequence
							$_academicYearId = $academicYearAry[$i]['AcademicYearID'];
							$_academicYearName = Get_Lang_Selection($academicYearAry[$i]['YearNameB5'], $academicYearAry[$i]['YearNameEN']);
							
							if (!isset($consolidatedYearTermAssessmentAry[$_academicYearId])) {
								// no academic result for this year => no need to display
								continue;
							}
							
							// show student class
							$_nameField = Get_Lang_Selection('ClassNameCh', 'ClassNameEn');
							$_className = $classHistoryAssoAry[$_academicYearId][$_nameField];
							$_classNumber = $classHistoryAssoAry[$_academicYearId]['ClassNumber'];
							$_classDisplay = $_className;
							if ($_classNumber > 0) {
								$_classDisplay .= ' - '.$_classNumber;
							}
							
							// calculate number of assessment of the academic year
							$_assessmentCount = 0;
							foreach ((array)$consolidatedYearTermAssessmentAry[$_academicYearId] as $__yearTermId => $__assessmentAry) {
								$_assessmentCount += count($__assessmentAry);
							}
							
							$x .= '<td colspan="'.$_assessmentCount.'" nowrap="nowrap" style="width:15%; text-align:center; vertical-align:top;">'.$_academicYearName.'<br>'.$_classDisplay.'</td>'."\r\n";
						}
					$x .= '</tr>'."\r\n";
					
					// term row
					$x .= '<tr class="tabletop">'."\r\n";
						// empty column for subject 
						$x .= '<td width="15%" nowrap="nowrap" style="background:#FFFFFF">&nbsp;</td>'."\r\n";
						
						// loop academic year display
						for ($i=0; $i<$numOfAcademicYear; $i++) {	// loop $academicYearAry to maintain academic year sequence
							$_academicYearId = $academicYearAry[$i]['AcademicYearID'];
							
							if (!isset($consolidatedYearTermAssessmentAry[$_academicYearId])) {
								// no academic result for this year => no need to display
								continue;
							}
							
							// calculate number of assessment of the academic year
							foreach ((array)$yearTermAssoAry[$_academicYearId] as $__yearTermId => $__termInfoAry) {
								if (!isset($consolidatedYearTermAssessmentAry[$_academicYearId][$__yearTermId])) {
									// skip no result terms
									continue;
								}
								
								$__numOfAssessment = count((array)$consolidatedYearTermAssessmentAry[$_academicYearId][$__yearTermId]);
								
								if ($__yearTermId==0) {
									$__termName = $Lang['General']['Annual'];
								}
								else {
									$__nameField = Get_Lang_Selection('YearTermNameB5', 'YearTermNameEN');
									$__termName = $yearTermAssoAry[$_academicYearId][$__yearTermId][$__nameField];
								}
								
								$x .= '<td nowrap="nowrap" colspan="'.$__numOfAssessment.'" style="text-align:center; vertical-align:top;">'.$__termName.'</td>'."\r\n";
							}
						}
					$x .= '</tr>'."\r\n";
						
					// assessment row
					$x .= '<tr class="tabletop">'."\r\n";
						// empty column for subject 
						$x .= '<td width="15%" nowrap="nowrap" style="background:#FFFFFF">&nbsp;</td>'."\r\n";
						
						// loop academic year display
						for ($i=0; $i<$numOfAcademicYear; $i++) {	// loop $academicYearAry to maintain academic year sequence
							$_academicYearId = $academicYearAry[$i]['AcademicYearID'];
							
							if (!isset($consolidatedYearTermAssessmentAry[$_academicYearId])) {
								// no academic result for this year => no need to display
								continue;
							}
							
							// calculate number of assessment of the academic year
							foreach ((array)$yearTermAssoAry[$_academicYearId] as $__yearTermId => $__termInfoAry) {
								if (!isset($consolidatedYearTermAssessmentAry[$_academicYearId][$__yearTermId])) {
									// skip no result terms
									continue;
								}
								
								$__numOfAssessment = count((array)$consolidatedYearTermAssessmentAry[$_academicYearId][$__yearTermId]);
								for ($j=0; $j<$__numOfAssessment; $j++) {
									$___assessmentCode = $consolidatedYearTermAssessmentAry[$_academicYearId][$__yearTermId][$j];
									
									if ($___assessmentCode == 'zoverall') {
										$___assessmentName = $Lang['General']['Overall'];
									}
									else {
										$___assessmentName = $___assessmentCode;
									}
									
									$x .= '<td nowrap="nowrap" style="text-align:center; vertical-align:top;">'.$___assessmentName.'</td>'."\r\n";
								}
							}
						}
					$x .= '</tr>'."\r\n";
					
					// subject rows
					$subjectCount = 0;
					for ($i=0; $i<$numOfSubject; $i++) {	// loop $subjectAry to maintain subject sequence
						$_subjectId = $subjectAry[$i]['SubjectID'];
						$_subjectName = Get_Lang_Selection($subjectAry[$i]['SubjectDescB5'], $subjectAry[$i]['SubjectDescEN']);
						$_isComponent = $subjectAry[$i]['IsComponent'];
						
						if (!isset($consolidatedSubjectDataAry[$_subjectId])) {
							// skip no score subjects
							continue;
						}
						
						$_subjectNameClass = ($_isComponent)? 'tablelist_sub_subject' : 'tablelist_subject';
						$_subjectScoreBgColor = ($subjectCount % 2)? '#F3F3F3' : '#FFFFFF';
						
						if ($_isComponent) {
							$_mainSubjectCode = $subjectAry[$i]['ParentSubjectShortNameEN'];
							$_cmpSubjectCode = $subjectAry[$i]['SubjectShortNameEN'];
							
							$_mainSubjectId = $subjectAry[$i]['ParentSubjectID'];
							$_cmpSubjectId = $subjectAry[$i]['SubjectID'];
						}
						else {
							$_mainSubjectCode = $subjectAry[$i]['SubjectShortNameEN'];
							$_cmpSubjectCode = '';
							
							$_mainSubjectId = $subjectAry[$i]['SubjectID'];
							$_cmpSubjectId = '';
						}
						
						$x .= '<tr>'."\r\n";
							// subject name
							$x .= '<td nowrap="" class="'.$_subjectNameClass.'">'."\r\n";
								$x .= '<div style="float:left;">'.$_subjectName.'</div>'."\r\n";
								
								if ($parHideStat) {
									// hide statistics icon
								}
								else {
									$x .= '<div style="float:right;"><a href="javascript:jVIEW_DETAIL(\'assessment_report\', \''.base64_encode("MainSubjectCode=".$_mainSubjectCode."&CompSubjectCode=".$_cmpSubjectCode."&displayBy=".$parDisplayBy).'\');"><img src="'.$image_path.'/2009a/iPortfolio/icon_resultview.gif" width="20" height="20" border="0"></a></div>'."\r\n";
								}
							$x .= '</td>'."\r\n";
							
							// subject scores
							// loop academic year display
							for ($j=0; $j<$numOfAcademicYear; $j++) {	// loop $academicYearAry to maintain academic year sequence
								$__academicYearId = $academicYearAry[$j]['AcademicYearID'];
								$__academicYearNameEn = $academicYearAry[$j]['YearNameEN'];
								
								if (!isset($consolidatedYearTermAssessmentAry[$__academicYearId])) {
									// no academic result for this year => no need to display
									continue;
								}
								
								// calculate number of assessment of the academic year
								foreach ((array)$yearTermAssoAry[$__academicYearId] as $___yearTermId => $___termInfoAry) {
									if (!isset($consolidatedYearTermAssessmentAry[$__academicYearId][$___yearTermId])) {
										// skip no result terms
										continue;
									}
									
									$___numOfAssessment = count((array)$consolidatedYearTermAssessmentAry[$__academicYearId][$___yearTermId]);
									for ($k=0; $k<$___numOfAssessment; $k++) {
										$____assessmentCode = $consolidatedYearTermAssessmentAry[$__academicYearId][$___yearTermId][$k];
										
										if ($parDisplayBy == 'Score') {
											$____displayScore = $consolidatedSubjectDataAry[$_subjectId][$__academicYearId][$___yearTermId][$____assessmentCode]['score'];
										}
										else if ($parDisplayBy == 'Grade') {
											$____displayScore = $consolidatedSubjectDataAry[$_subjectId][$__academicYearId][$___yearTermId][$____assessmentCode]['grade'];
										}
										else if ($parDisplayBy == 'ScoreGrade') {
											$____score = $consolidatedSubjectDataAry[$_subjectId][$__academicYearId][$___yearTermId][$____assessmentCode]['score'];
											$____grade = $consolidatedSubjectDataAry[$_subjectId][$__academicYearId][$___yearTermId][$____assessmentCode]['grade'];
											
											if ($____score == '' && $____grade == '') {
												$____displayScore = '';
											}
											else {
												if ($____score == '') {
													$____score = $Lang['General']['EmptySymbol'];
												}
												if ($____grade == '') {
													$____grade = $Lang['General']['EmptySymbol'];
												}
												$____displayScore = $____score.' ('.$____grade.')';
											}
										}
										else if ($parDisplayBy == 'Rank') {
											$____displayScore = $consolidatedSubjectDataAry[$_subjectId][$__academicYearId][$___yearTermId][$____assessmentCode]['formRanking'];
										}
										else if ($parDisplayBy == 'StandardScore') {
											$____score = $consolidatedSubjectDataAry[$_subjectId][$__academicYearId][$___yearTermId][$____assessmentCode]['score'];
											
											if ($___yearTermId==0) {
												$___termName = $ec_iPortfolio['overall_result'];
											}
											else {
												$___termName = $yearTermAssoAry[$_academicYearId][$__yearTermId]['YearTermNameEN'];
											}
											
											$____targetAssessmentCode = ($____assessmentCode=='zoverall')? '' : $____assessmentCode;
											$____displayScore = $this->returnStandardScore($parStudentId, $____score, $__academicYearNameEn, $___termName, '', '', $_mainSubjectId, $_cmpSubjectId, $____targetAssessmentCode);
										}
										
										if ($____displayScore == '') {
											$____displayScore = $Lang['General']['EmptySymbol'];
										}
										
										$x .= '<td nowrap="" class="tabletext" style="text-align:center; vertical-align:top; background-color:'.$_subjectScoreBgColor.';">'."\r\n";
											$x .= $____displayScore;
										$x .= '</td>'."\r\n";
									}
								}
							}
						$x .= '</tr>'."\r\n";
						
						$subjectCount++;
					}
					
					// overall average
					$x .= '<tr>'."\r\n";
						// empty column for subject 
						$x .= '<td nowrap="nowrap" style="text-align:right;">'.$ec_iPortfolio['overall_score'].'</td>'."\r\n";
						
						// loop academic year display
						for ($i=0; $i<$numOfAcademicYear; $i++) {	// loop $academicYearAry to maintain academic year sequence
							$_academicYearId = $academicYearAry[$i]['AcademicYearID'];
							
							if (!isset($consolidatedYearTermAssessmentAry[$_academicYearId])) {
								// no academic result for this year => no need to display
								continue;
							}
							
							// calculate number of assessment of the academic year
							foreach ((array)$yearTermAssoAry[$_academicYearId] as $__yearTermId => $__termInfoAry) {
								if (!isset($consolidatedYearTermAssessmentAry[$_academicYearId][$__yearTermId])) {
									// skip no result terms
									continue;
								}
								
								$__numOfAssessment = count((array)$consolidatedYearTermAssessmentAry[$_academicYearId][$__yearTermId]);
								for ($j=0; $j<$__numOfAssessment; $j++) {
									$___assessmentCode = $consolidatedYearTermAssessmentAry[$_academicYearId][$__yearTermId][$j];
									
									if ($parDisplayBy == 'Score') {
										$___displayScore = $consolidatedSubjectDataAry[0][$_academicYearId][$__yearTermId][$___assessmentCode]['score'];
									}
									else if ($parDisplayBy == 'Grade') {
										$___displayScore = $consolidatedSubjectDataAry[0][$_academicYearId][$__yearTermId][$___assessmentCode]['grade'];
									}
									else if ($parDisplayBy == 'ScoreGrade') {
										$___score = $consolidatedSubjectDataAry[0][$_academicYearId][$__yearTermId][$___assessmentCode]['score'];
										$___grade = $consolidatedSubjectDataAry[0][$_academicYearId][$__yearTermId][$___assessmentCode]['grade'];
										
										if ($___score == '' && $___grade == '') {
											$___displayScore = '';
										}
										else {
											if ($___score == '') {
												$___score = $Lang['General']['EmptySymbol'];
											}
											if ($___grade == '') {
												$___grade = $Lang['General']['EmptySymbol'];
											}
											$___displayScore = $___score.' ('.$___grade.')';
										}
									}
									else if ($parDisplayBy == 'Rank') {
										$___displayScore = $consolidatedSubjectDataAry[0][$_academicYearId][$__yearTermId][$___assessmentCode]['formRanking'];
									}
									
									if ($___displayScore == '') {
										$___displayScore = $Lang['General']['EmptySymbol'];
									}
									$x .= '<td nowrap="nowrap" class="tablerow_total" style="text-align:center; vertical-align:top;">'.$___displayScore.'</td>'."\r\n";
								}
							}
						}
					$x .= '</tr>'."\r\n";
				$x .= '</tbody>'."\r\n";
			$x .= '</table>'."\r\n";
		}
		
		return $x;
	}
	
	function getStudentSubjectAcademicResult($parStudentId) {
		global $eclass_db, $intranet_db;
		
		$sql = "Select
						assr.AcademicYearID, assr.YearTermID, assr.TermAssessment, assr.SubjectID, assr.SubjectComponentID, assr.Score, assr.Grade, assr.OrderMeritForm, assr.ClassName, assr.ClassNumber
				From
						$eclass_db.ASSESSMENT_STUDENT_SUBJECT_RECORD as assr
				Where
						assr.UserID = '".$parStudentId."'
				";
		return $this->returnResultSet($sql);
	}
	
	function getStudentMainAcademicResult($parStudentId) {
		global $eclass_db, $intranet_db;
		
		$sql = "Select
						asmr.AcademicYearID, asmr.YearTermID, asmr.TermAssessment, asmr.Score, asmr.Grade, asmr.OrderMeritForm, asmr.ClassName, asmr.ClassNumber
				From
						$eclass_db.ASSESSMENT_STUDENT_MAIN_RECORD as asmr
				Where
						asmr.UserID = '".$parStudentId."'
				";
		return $this->returnResultSet($sql);
	}
	
	function getStudentClassHistory($parStudentId) {
		global $intranet_db, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		$lfcm = new form_class_manage();
		
		// class info from school settings
		$sql = "Select
						yc.AcademicYearID, yc.ClassTitleEN as ClassNameEn, yc.ClassTitleB5 as ClassNameCh, ycu.ClassNumber
				From
						$intranet_db.YEAR_CLASS_USER as ycu
						Inner Join $intranet_db.YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
				Where
						ycu.UserID = '".$parStudentId."'
				";
		$schoolSettingsAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'AcademicYearID');
		
		// class info from profile class history
		$sql = "Select
						AcademicYearID, ClassName, ClassNumber 
				From
						$intranet_db.PROFILE_CLASS_HISTORY
				Where
						UserID = '".$parStudentId."'
						And AcademicYearID > 0
				";
		$studentProfileAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'AcademicYearID');
		
		// class info from subject academic result
		$subjectAcademicResultAssoAry = BuildMultiKeyAssoc($this->getStudentSubjectAcademicResult($parStudentId), 'AcademicYearID');
		
		// class info from main academic result
		$mainAcademicResultAssoAry = BuildMultiKeyAssoc($this->getStudentMainAcademicResult($parStudentId), 'AcademicYearID');
		
		
		// get class history from school settings first, and then student profile, and then academic result data
		$academicYearAry = $lfcm->Get_Academic_Year_List();
		$numOfAcademicYear = count($academicYearAry);
		$classHistoryAry = array();
		for ($i=0; $i<$numOfAcademicYear; $i++) {
			$_academicYearId = $academicYearAry[$i]['AcademicYearID'];
			
			$_tmpAry = array();
			$_tmpAry['AcademicYearID'] = $_academicYearId;
			
			if (isset($schoolSettingsAssoAry[$_academicYearId])) {
				$_tmpAry['ClassNameEn'] = $schoolSettingsAssoAry[$_academicYearId]['ClassNameEn'];
				$_tmpAry['ClassNameCh'] = $schoolSettingsAssoAry[$_academicYearId]['ClassNameCh'];
				$_tmpAry['ClassNumber'] = $schoolSettingsAssoAry[$_academicYearId]['ClassNumber'];
			}
			else if (isset($studentProfileAssoAry[$_academicYearId])) {
				$_tmpAry['ClassNameEn'] = $studentProfileAssoAry[$_academicYearId]['ClassName'];
				$_tmpAry['ClassNameCh'] = $studentProfileAssoAry[$_academicYearId]['ClassName'];
				$_tmpAry['ClassNumber'] = $studentProfileAssoAry[$_academicYearId]['ClassNumber'];
			}
			else if (isset($subjectAcademicResultAssoAry[$_academicYearId])) {
				$_tmpAry['ClassNameEn'] = $subjectAcademicResultAssoAry[$_academicYearId]['ClassName'];
				$_tmpAry['ClassNameCh'] = $subjectAcademicResultAssoAry[$_academicYearId]['ClassName'];
				$_tmpAry['ClassNumber'] = $subjectAcademicResultAssoAry[$_academicYearId]['ClassNumber'];
			}
			else if (isset($mainAcademicResultAssoAry[$_academicYearId])) {
				$_tmpAry['ClassNameEn'] = $mainAcademicResultAssoAry[$_academicYearId]['ClassName'];
				$_tmpAry['ClassNameCh'] = $mainAcademicResultAssoAry[$_academicYearId]['ClassName'];
				$_tmpAry['ClassNumber'] = $mainAcademicResultAssoAry[$_academicYearId]['ClassNumber'];
			}
			else {
				continue;
			}
			
			$classHistoryAry[] = $_tmpAry;
		}
		
		return $classHistoryAry;
	}
}

?>