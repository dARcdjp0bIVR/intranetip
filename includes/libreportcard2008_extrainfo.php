<?php
#  Editing by Bill
/* ***************************************************
 * Modification Log
 * 	Date:	2016-08-09	(Bill)	[2016-0224-1423-31073]
 * 		- modified Get_Conduct(), to customize conduct display order
 * 		 ($eRCTemplateSetting['ClassTeacherComment_Conduct_PuiChingGradeLimit'] = true)
 * 	Date:	2016-07-21	(Bill)
 * 		- modified Get_Student_Extra_Info(), to use empty() for PHP 5.4
 */
if (!defined("LIBREPORTCARD_EXTRAINFO"))         // Preprocessor directives
{
	define("LIBREPORTCARD_EXTRAINFO", true);

	class libreportcard_extrainfo extends libreportcard {
		
		public function __construct() {
			$this->libreportcard();
		}
		
		public function Get_Setting_Conduct_DBTable_Sql()
		{
			$RC_CONDUCT = $this->DBName.".RC_CONDUCT";
			$sql = "
				SELECT 
					CONCAT('<a href=\"edit.php?ConductID[]=',ConductID,'\">', Conduct, '</a>'),
					CONCAT('<input type=\"checkbox\" name=\"ConductID[]\" value=\"', ConductID, '\" class=\"ConductChkbox\"/>')
				FROM
					$RC_CONDUCT
			";
			
			return $sql;
		}
		
		public function Get_Conduct($ConductID='')
		{
			global $eRCTemplateSetting;
			
			if(trim($ConductID)!='')
				$cond_ConductID = " AND ConductID = '$ConductID' ";
			
			// [2016-0224-1423-31073] Set order
			$orderby_Conduct = "";
			if($eRCTemplateSetting['ClassTeacherComment_Conduct_PuiChingGradeLimit']){
				$orderby_Conduct = " ORDER BY 
										MID(Conduct, 1, 1), 
										CASE MID(Conduct, 2, 1) WHEN '+' THEN 2 WHEN '' THEN 3 WHEN '-' THEN 4 ELSE 1 END";
			}
			
			$RC_CONDUCT = $this->DBName.".RC_CONDUCT";
			$sql = "
				SELECT
					ConductID,
					Conduct
				FROM
					$RC_CONDUCT
				WHERE
					1
					$cond_ConductID
				$orderby_Conduct
	  		";
	  		
	  		return $this->returnArray($sql);
		}
		
		public function Is_Conduct_Exist($Conduct, $ConductID='')
		{
			if(trim($ConductID)!='')
				$cond_Conduct = " AND ConductID <> '$ConductID' ";
			
			$RC_CONDUCT = $this->DBName.".RC_CONDUCT";
			$sql = "
				SELECT
					COUNT(*)
				FROM
					$RC_CONDUCT
				WHERE
					Conduct = '".$this->Get_Safe_Sql_Query($Conduct)."'
					$cond_Conduct
			";
			
			$count = $this->returnVector($sql);

			return $count[0]>0?1:0;	
		}
		
		public function Update_Conduct($Conduct, $ConductID)
		{
			$RC_CONDUCT = $this->DBName.".RC_CONDUCT";
			$sql = "
				UPDATE
					$RC_CONDUCT
				SET
					Conduct = '".$this->Get_Safe_Sql_Query($Conduct)."',
					DateModified = NOW(),
					LastModifiedBy = '".$_SESSION['UserID']."'
				WHERE
					ConductID = '$ConductID'
			";
			
			$Success = $this->db_db_query($sql);

			return $Success;
		}		

		public function Insert_Conduct($Conduct)
		{
			foreach((array)$Conduct as $thisConduct)
			{
				$sqlArr[] = " ('".$this->Get_Safe_Sql_Query($thisConduct)."', NOW(), '".$_SESSION['UserID']."', NOW(), '".$_SESSION['UserID']."') ";
			}
			$insertSql = implode(", ",$sqlArr);
			
			$RC_CONDUCT = $this->DBName.".RC_CONDUCT";
			$sql = "
				INSERT INTO $RC_CONDUCT
					(Conduct, DateInput, InputBy, DateModified, LastModifiedBy)
				VALUES
					$insertSql
			";
			
			$Success = $this->db_db_query($sql);

			return $Success;
		}		

		public function Delete_Conduct($ConductID)
		{
			$RC_CONDUCT = $this->DBName.".RC_CONDUCT";
			$sql = "
				DELETE FROM
					$RC_CONDUCT
				WHERE
					ConductID IN ('".implode("','",(array)$ConductID)."')
			";
			
			$Success = $this->db_db_query($sql);

			return $Success;
		}		
		
		public function Get_Student_Extra_Info($ReportID, $StudentID='')
		{
			//if(trim($StudentID)!='')
			if(!empty($StudentID))
				$cond_StudentID = " AND StudentID IN ('".implode("','",(array)$StudentID)."') ";
		
			$RC_REPORT_STUDENT_EXTRA_INFO = $this->DBName.".RC_REPORT_STUDENT_EXTRA_INFO";		
			$sql = "
				SELECT
					*
				FROM
					$RC_REPORT_STUDENT_EXTRA_INFO
				WHERE
					ReportID = '".$ReportID."'
					$cond_StudentID
			";
			
			return $this->returnArray($sql);	
		}
		
		public function Insert_Update_Extra_Info($ExtraInfoField, $ReportID, $StudentConductArr)
		{
			$StudentIDArr = array_keys($StudentConductArr); 
			$StudentExtraInfoArr = $this->Get_Student_Extra_Info($ReportID, $StudentIDArr);
			
			$ExistStudentIDArr = Get_Array_By_Key($StudentExtraInfoArr, "StudentID");
			
			foreach($StudentIDArr as $StudentID)
			{
				if(in_array($StudentID, $ExistStudentIDArr))
					$UpdateStudentData[$StudentID] = $StudentConductArr[$StudentID];
				else
					$InsertStudentData[$StudentID] = $StudentConductArr[$StudentID];
			}
			
			if(count($UpdateStudentData)>0)
				$Success['Update'] = $this->Update_Extra_Info($ExtraInfoField, $ReportID, $UpdateStudentData);
			
			if(count($InsertStudentData)>0)
				$Success['Insert'] = $this->Insert_Extra_Info($ExtraInfoField, $ReportID, $InsertStudentData);
				
			return !in_array(false, (array)$Success);		
		}
		
		 private function Update_Extra_Info($ExtraInfoField, $ReportID, $StudentData)
		 {
		 	$RC_REPORT_STUDENT_EXTRA_INFO = $this->DBName.".RC_REPORT_STUDENT_EXTRA_INFO";
		 	foreach($StudentData as $StudentID => $Val)
		 	{
		 		$sql = "
					UPDATE
						$RC_REPORT_STUDENT_EXTRA_INFO
					SET
						$ExtraInfoField = '$Val',
						DateModified = NOW(),
						LastModifiedBy = '".$_SESSION['UserID']."'
					WHERE
						ReportID = '$ReportID'
						AND StudentID = '$StudentID'
				";
				
				$SuccessArr[] = $this->db_db_query($sql);
		 	}
			$Success = !in_array(false, (array)$SuccessArr);
			
			return $Success;		 	
		 }

		 private function Insert_Extra_Info($ExtraInfoField, $ReportID, $StudentData)
		 {
		 	foreach($StudentData as $StudentID => $Val)
		 	{
		 		$SqlValArr[] = "('$ReportID', '$StudentID', '$Val', NOW(), '".$_SESSION['UserID']."', NOW(), '".$_SESSION['UserID']."')";
		 	}
		 	$SqlVal = implode(", ", $SqlValArr);
		 	
		 	$RC_REPORT_STUDENT_EXTRA_INFO = $this->DBName.".RC_REPORT_STUDENT_EXTRA_INFO";
		 	$sql = "
				INSERT INTO $RC_REPORT_STUDENT_EXTRA_INFO
					(ReportID, StudentID, $ExtraInfoField, DateInput, InputBy, DateModified, LastModifiedBy)
				VALUES
					$SqlVal
			";
			
			$Success = $this->db_db_query($sql);
			return $Success;
		 }

	} // end of class libreportcard_extrainfo
} // end of Preprocessor directives
?>