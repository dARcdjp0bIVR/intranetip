<?
/*
 * 	Need to include eGuidance_lang file before including this file
 */

## support service type
if (count($Lang['eGuidance']['settings']['FineTune']['SupportType'])) {
	foreach((array)$Lang['eGuidance']['settings']['FineTune']['SupportType'] as $k=>$v) {
		$guidance_cfg['support_service_type'][$k] = $v;
	}
}
else {
	$guidance_cfg['support_service_type']['Study'] = "Study";
	$guidance_cfg['support_service_type']['Other'] = "Emotion,Social,Self-Management";
}

?>