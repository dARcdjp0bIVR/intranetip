<?php
/*
 * Modifying:  
 *
 * 2020-06-19 Cameron
 * - add setting for $PrintSenCasePICinStudentReport, modify __construct to set it
 * 
 * 2020-05-13 Cameron
 * - change to order by SeqNo in getAllSENCaseType() and getAllSENCaseSubType() [case #M151032]
 * - add case SENType and SENSubType in getNexSeqNo() 
 * - add function updateSENTypeDisplayOrder()
 * 
 * 2020-05-12 Cameron
 * - add parameter $showLeft to getStudentInfo2(), filter out left student by default in student report [case #M151030]
 * 
 * 2020-05-11 Cameron
 * - add function isStaffSENCaseConfidentialViewerOrPIC() [case #F152835]
 * 
 * 2020-05-07 Cameron
 * - add function getSENTypeConfirmDate() [case #F152883]
 * 
 * 2020-04-24 Cameron
 * - add function getFilterSENStudents() [case #E150107]
 * 
 * 2020-03-18 Cameron
 * - add function getSearchSENCaseSubTypeCode(), getSearchSENCaseTypeCode() [case #M173271]
 * 
 * 2019-08-14 Cameron
 * - add function getClassTeacherStudent(), getSubjectTeacherStudent(), getFilterClassListOrUserList()
 *
 * 2019-08-13 Cameron
 * - add setting for these two variables $AllowClassTeacherViewSEN & $AllowSubjectTeacherViewSEN, modify __construct to set them [case #M158130]
 * - add function isClassTeacher() isSubjectTeacher(), getClassTeacherClass(), getSubjectTeacherClass()
 * - modify GET_MODULE_OBJ_ARR to allow class teacher and subject teacher to view SEN
 * - add isClassTeacher and isSubjectTeacher to getUserPermission()
 * - modify getDefaultPage(), set default page for class teacher and subject teacher
 * - add parameter $condition to getSENServiceStudent()
 * 
 * 2018-05-23 Cameron
 * - add order by SeqNo in getSENAdjustItem(), getServiceType() and getSENSupportService()
 * - modify getSENCaseAdjustment() and getSENCaseSupport() to sort by Type SeqNo and Item SeqNo
 *
 * 2018-05-21 Cameron
 * - add function getRecordIDAryByOrderString(), updateAdjustTypeDisplayOrder(), updateServiceTypeDisplayOrder(), updateServiceItemDisplayOrder(), getNexSeqNo()
 *
 * 2018-04-12 Cameron
 * - add CaseType tab to getFineTuneTabs()
 * - add parameter SettingID to getGuidanceSettingItem()
 * - add function isSENCaseUseThisCaseSubType(), isSENCaseUseThisCaseType(), getSENCaseType(), getSENCaseSubType()
 *  checkDuplicateSenCaseType(), checkDuplicateSenCaseSubType(), getAllSENCaseSubType(), getAllSENCaseType()
 * 
 * 2018-04-11 Cameron
 * - add parameter senStatus to getSENCase()
 * 
 * 2018-04-10 Cameron
 * - return STRN and DateOfBirth in getStudentInfo() [case #M130681]
 *
 * 2017-12-11 Cameron
 * - add function getUserAcademicYearHistory()
 *
 * 2017-11-20 Cameron
 * - should clear cookies when click SEN tab (in getSENTabs) and FineTune tab (in getFineTuneTabs)
 *
 * 2017-11-17 Cameron
 * - add function getClassTeacherMeetingID(), getClassTeacherImport(), getClassTeacherMeetingStudent()
 *
 * 2017-11-15 Cameron
 * - add function getStudentID()
 *
 * 2017-11-14 Cameron
 * - add function getAcademicYearList()
 *
 * 2017-11-13 Cameron
 * - modify getAcademicYearByClassName() and getClassTeacherReport() to support ej
 * - add function getPastClassList()
 *
 * 2017-11-07 Cameron
 * - modify getClassTeacherComments() to support ej
 * - move get student info code to getStudentInfo2()
 *
 * 2017-10-25 Cameron
 * - retrieve Followup in getClassTeacherComments()
 *
 * 2017-10-18 Cameron
 * - add function getStudentProblemReport()
 *
 * 2017-10-17 Cameron
 * - add StudentProblemReport in GET_MODULE_OBJ_ARR()
 *
 * 2017-10-09 Cameron
 * - fix bug: add function getDefaultPage() so that it'll redirect to the page that user has permission
 *
 * 2017-09-11 Cameron
 * - add function getClassByStudentID(), add parameter $includeUserIdAry to getStudentNameListWClassNumberByClassName()
 *
 * 2017-09-07 Cameron
 * - fix: retrieve getSelfImprove() in sendNotifyToColleague()
 * - state the path of module changed in sendNotifyToColleague()
 *
 * 2017-08-21 Cameron
 * - add function getSelfImproveByTeacherId()
 *
 * 2017-08-11 ~ 18 Cameron
 * - add functions about personal and self-improve
 *
 * 2017-08-03 Cameron
 * - add function sendNotifyToColleague()
 *
 * 2017-07-28 Cameron
 * - add function getGroupCategoryList()
 *
 * 2017-07-21 Cameron
 * - modify getFineTuneTabs(), change support tab link
 *
 * 2017-07-18 Cameron
 * - sort by EnglishName in getTeacher()
 *
 * 2017-07-10 Anna
 * - added updateGeneralSettings()
 * - added getGuidanceGeneralSetting()
 * - added GeneralSetting and Settings_AllowAccessIP
 *
 * 2017-06-29 Pun
 * - Modified GET_MODULE_OBJ_ARR(), added report tab
 * - Modified getSelectedStudent(), getSelectedTeacher(), added return field
 * - Modified getContactTeacher(), getAdvancedClassLessonResult(), getAdvancedClassStudent(), getAdvancedClassTeacher(),
 * getTherapyActivityResult(), getTherapyStudent(), getTherapyTeacher(), getGuidanceTeacher(), getSENServiceActivityResult(),
 * getSENServiceStudent(), getSENServiceTeacher(), getSENCase(), getSENCaseTeacher(), getSENCaseAdjustment(), getSENCaseSupport()
 * added allow param is an array
 * - Added getStudent(), getContactByTeacherId(), getContactByStudentId(), getAdvancedClassByStudentId(), getAdvancedClassByTeacherId(),
 * getTherapyByStudentId(), getTherapyByTeacherId(), getGuidanceByStudentId(), getGuidanceByTeacherId(), getSENServiceByStudentID(),
 * getSENServiceByTeacherID(), getSENCaseByTeacherID()
 *
 *
 * 2017-05-23 Cameron
 * - add function getStudentNameListWClassNumberByClassName() for IP
 * - add function getStudentNameByClassWithFilter()
 *
 * 2017-05-18 Cameron
 * - apply Get_Safe_Sql_Query() to INSERT2TABLE() and UPDATE2TABLE()
 * - show approved teacher(RecordStatus=1) only in getTeacher()
 *
 * 2017-02-10 Cameron
 * - create this file
 */
include_once ($intranet_root . "/includes/libaccessright.php");
if (! defined("LIBGUIDANCE_DEFINED")) // Preprocessor directives
{
    define("LIBGUIDANCE_DEFINED", true);

    class libguidance extends libaccessright
    {

        // -- Start of Class
        var $AuthenticationSetting;
        var $AllowClassTeacherViewSEN;
        var $AllowSubjectTeacherViewSEN;
        var $PrintSenCasePICinStudentReport;
        var $eg_path;

        private function GET_ADMIN_USER()
        {
            global $intranet_root, $junior_mck;
            if ($junior_mck) { // ej
                include_once ($intranet_root . "/includes/libfilesystem.php");
                $lf = new libfilesystem();
                $AdminUser = trim($lf->file_read($intranet_root . "/file/eGuidance/admin_user.txt"));
                $AdminUser = explode(',', $AdminUser);
            } else { // ip
                $sql = "SELECT DISTINCT rm.UserID FROM ROLE_RIGHT rr LEFT OUTER JOIN ROLE_MEMBER rm ON (rm.RoleID=rr.RoleID) WHERE FunctionName='eAdmin-eGuidance' AND rm.UserID IS NOT NULL";
                $AdminUser = $this->returnVector($sql);
            }
            return $AdminUser;
        }

        // is admin or not? 1 - ture, 0 - false
        private function IS_ADMIN_USER2($userID)
        {
            $admin_user = $this->GET_ADMIN_USER();
            return (is_array($admin_user) && in_array($userID, (array) $admin_user)) ? 1 : 0;
        }

        public function IS_GUIDANCE_ADMIN($userID)
        {
            return $_SESSION['SSV_USER_ACCESS']['eAdmin-eGuidance'];
        }

        public function __construct($userID = '')
        {
            global $file_path;
            
            parent::libaccessright('GUIDANCE');
            if (! isset($_SESSION['SSV_USER_ACCESS']['eAdmin-eGuidance'])) {
                $_SESSION['SSV_USER_ACCESS']['eAdmin-eGuidance'] = $this->IS_ADMIN_USER2($userID);
            }
            if (! isset($_SESSION['eGuidance']['current_right'])) {
                $rights = ($this->retrieveUserAccessRight($userID ? $userID : $_SESSION['UserID']));
                if (count($rights)) {
                    $rights = current($rights);
                    $_SESSION['eGuidance']['current_right'] = $rights;
                } else {
                    $_SESSION['eGuidance']['current_right'] = array();
                }
            }

            $settings = trim($this->getGuidanceGeneralSetting("MAX"));
            $settingAry = explode("#", $settings);
            if (count($settingAry)) {
                foreach((array)$settingAry as $_setting) {
                    $_settingAry = explode(":", $_setting);
                    if (count($_settingAry)) {
                        switch ($_settingAry[0]) {
                            case 'AuthenticationSetting':
                                $this->AuthenticationSetting = $_settingAry[1];
                                break;
                            case 'AllowClassTeacherViewSEN':
                                $this->AllowClassTeacherViewSEN= $_settingAry[1];
                                break;
                            case 'AllowSubjectTeacherViewSEN':
                                $this->AllowSubjectTeacherViewSEN= $_settingAry[1];
                                break;
                            case 'PrintSenCasePICinStudentReport':
                                $this->PrintSenCasePICinStudentReport= $_settingAry[1];
                                break;
                        }
                    }
                }
            }

            if (!isset($_SESSION['eGuidance']['settings'])) {
                $_SESSION['eGuidance']['settings']['isClassTeacher'] = $this->isClassTeacher($_SESSION['UserID']);
                $_SESSION['eGuidance']['settings']['isSubjectTeacher'] = $this->isSubjectTeacher($_SESSION['UserID']);
            }

            $this->eg_path = "$file_path/file/eGuidance/";
        }

        public function getUserPermission()
        {
            $permission['admin'] = $_SESSION['SSV_USER_ACCESS']['eAdmin-eGuidance'];
            $permission['current_right'] = $_SESSION['eGuidance']['current_right'];
            $permission['isClassTeacher'] = $_SESSION['eGuidance']['settings']['isClassTeacher'];
            $permission['isSubjectTeacher'] = $_SESSION['eGuidance']['settings']['isSubjectTeacher'];
            return $permission;
        }

        // overwrite parent class function
        function retrieveAccessRight($GroupID = '')
        {
            $sql = "select UCASE(Module), UCASE(Section), UCASE(Function), UCASE(Action) from ACCESS_RIGHT_GROUP_SETTING where GroupID='$GroupID' AND Module='" . $this->Module . "'";
            // echo $sql;
            return $this->returnArray($sql);
        }

        // overwrite parent class function
        function retrieveUserAccessRight($this_UserID)
        {
            global $PATH_WRT_ROOT;
            
            $this_UserID = $this_UserID ? $this_UserID : $_SESSION['UserID'];
            include_once ($PATH_WRT_ROOT . "includes/libuser.php");
            $lu = new libuser($this_UserID);
            
            $data = array();
            $MemberGroupIDAry = array();
            $AccessRightAry = array();
            
            if ($lu->isTeacherStaff()) {
                $MemberGroupIDAry = $this->retrieveAccessRightMemberGroupID($this_UserID);
                // build Access Right Data Array
                foreach ($MemberGroupIDAry as $k => $GroupID) {
                    $AccessRightAry = $this->retrieveAccessRight($GroupID);
                    foreach ($AccessRightAry as $k => $dataTemp) {
                        list ($module, $section, $function, $action) = $dataTemp;
                        if (isset($data[$module][$section][$function])) {
                            if (! in_array($action, $data[$module][$section][$function])) {
                                $data[$module][$section][$function][] = $action;
                            }
                        } else {
                            $data[$module][$section][$function][] = $action;
                        }
                    }
                }
            }
            return $data;
        }

        /*
         * Get MODULE_OBJ array
         */
        public function GET_MODULE_OBJ_ARR()
        {
            global $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $Lang;
            global $image_path, $plugin, $sys_custom;
            
            $allowed_IPs = trim($this->getGuidanceGeneralSetting("AllowAccessIP"));
            $ip_addresses = explode("\n", $allowed_IPs);
            
            checkCurrentIP($ip_addresses, $Lang['eGuidance']['name']);
            // Get User Access Right
            $isGuidanceAdmin = $this->IS_GUIDANCE_ADMIN($_SESSION['UserID']);
            $permit = $_SESSION['eGuidance']['current_right'];

            $isClassTeacher = $_SESSION['eGuidance']['settings']['isClassTeacher'];
            $isSubjectTeacher = $_SESSION['eGuidance']['settings']['isSubjectTeacher'];
            
            if ($this->AuthenticationSetting && $_SESSION['module_session_login']['eGuidance'] != 1) {
                $x = "";
                $x .= "<form id='form1' name='form1' method='post' action='$PATH_WRT_ROOT/home/module_access_checking.php'>";
                $x .= "<input type='hidden' name='module' value='eGuidance' />";
                $x .= "<input type='hidden' name='module_path' value='" . $_SERVER["REQUEST_URI"] . "' />";
                $x .= "</form>";
                $x .= "<script language='javascript'>";
                $x .= "document.form1.submit();";
                $x .= "</script>";
                echo $x;
                
                exit();
            }            // no need authentication - set session value
            else if (! isset($_SESSION['module_session_login']['eGuidance'])) {
                $_SESSION['module_session_login']['eGuidance'] = 1;
            }
            
            /*
             * structure:
             * $MenuArr["Section"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)");
             * $MenuArr["Section"]["Child"]["name"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)", "haveSubMenu");
             */
            
            // Menu information
            if ($_SESSION['UserType'] == USERTYPE_PARENT) {
                
                // # Current Page Information init
                // $PageSEN = 0;
                //
                // switch ($CurrentPage) {
                // // My Case Follow
                // case "CaseFollow":
                // $PageSEN = 1;
                // break;
                // }
                //
                // $MenuArr["case_follow"] = array($Lang['eGuidance']['menu']['CaseFollow'], $PATH_WRT_ROOT."home/eService/eGuidance/index.php", $PageSEN);
            } else {
                // Current Page Information init
                $PageGuidanceMgmt = 0;
                $PageContact = 0;
                $PageAdvancedClass = 0;
                $PageTherapy = 0;
                $PageGuidance = 0;
                $PageSEN = 0;
                $PageSuspend = 0;
                $PageTransfer = 0;
                $PagePersonal = 0;
                $PageSelfImprove = 0;
                $PageClassteacherMeeting = 0;
                
                $PageReport = 0;
                $PageStudentReport = 0;
                $PageTeacherReport = 0;
                $PageClassteacherReport = 0;
                $PageStudentProblemReport = 0;
                
                $PageSetting = 0;
                $PagePermissionSetting = 0;
                
                switch ($CurrentPage) {
                    
                    // Management
                    case "PageContact":
                        $PageGuidanceMgmt = 1;
                        $PageContact = 1;
                        break;
                    case "PageAdvancedClass":
                        $PageGuidanceMgmt = 1;
                        $PageAdvancedClass = 1;
                        break;
                    case "PageTherapy":
                        $PageGuidanceMgmt = 1;
                        $PageTherapy = 1;
                        break;
                    case "PageGuidance":
                        $PageGuidanceMgmt = 1;
                        $PageGuidance = 1;
                        break;
                    case "PageSEN":
                        $PageGuidanceMgmt = 1;
                        $PageSEN = 1;
                        break;
                    case "PageSuspend":
                        $PageGuidanceMgmt = 1;
                        $PageSuspend = 1;
                        break;
                    case "PageTransfer":
                        $PageGuidanceMgmt = 1;
                        $PageTransfer = 1;
                        break;
                    case "PagePersonal":
                        $PageGuidanceMgmt = 1;
                        $PagePersonal = 1;
                        break;
                    case "PageSelfImprove":
                        $PageGuidanceMgmt = 1;
                        $PageSelfImprove = 1;
                        break;
                    case "PageClassteacherMeeting":
                        $PageGuidanceMgmt = 1;
                        $PageClassteacherMeeting = 1;
                        break;
                    
                    // Report
                    case "PageStudentReport":
                        $PageReport = 1;
                        $PageStudentReport = 1;
                        break;
                    case "PageTeacherReport":
                        $PageReport = 1;
                        $PageTeacherReport = 1;
                        break;
                    case "PageClassteacherReport":
                        $PageReport = 1;
                        $PageClassteacherReport = 1;
                        break;
                    
                    case "PageStudentProblemReport":
                        $PageReport = 1;
                        $PageStudentProblemReport = 1;
                        break;
                    
                    // System Setting
                    case "PagePermissionSetting":
                        $PageSetting = 1;
                        $PagePermissionSetting = 1;
                        break;
                    
                    case "PageFineTuneSetting":
                        $PageSetting = 1;
                        $PageFineTuneSetting = 1;
                        break;
                    
                    case "GeneralSetting":
                        $PageSettings = 1;
                        $PageGeneralSetting = 1;
                        break;
                    
                    case "Settings_AllowAccessIP":
                        $PageSettings = 1;
                        $PageSettings_AllowAccessIP = 1;
                        break;
                }
                
                // Management Menu
                
                if ($isGuidanceAdmin || $permit['MGMT'] || $isClassTeacher || $isSubjectTeacher) {
                    $MenuArr["management"] = array(
                        $Lang['eGuidance']['menu']['main']['Management'],
                        "",
                        $PageGuidanceMgmt
                    );
                    if ($isGuidanceAdmin || $permit['MGMT']['CONTACT'])
                        $MenuArr["management"]["Child"]["Contact"] = array(
                            $Lang['eGuidance']['menu']['Management']['Contact'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/contact/?clearCoo=1",
                            $PageContact
                        );
                    if ($isGuidanceAdmin || $permit['MGMT']['ADVANCEDCLASS'])
                        $MenuArr["management"]["Child"]["AdvancedClass"] = array(
                            $Lang['eGuidance']['menu']['Management']['AdvancedClass'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/advanced_class/?clearCoo=1",
                            $PageAdvancedClass
                        );
                    if ($isGuidanceAdmin || $permit['MGMT']['THERAPY'])
                        $MenuArr["management"]["Child"]["Therapy"] = array(
                            $Lang['eGuidance']['menu']['Management']['Therapy'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/therapy/?clearCoo=1",
                            $PageTherapy
                        );
                    if ($isGuidanceAdmin || $permit['MGMT']['GUIDANCE'])
                        $MenuArr["management"]["Child"]["Guidance"] = array(
                            $Lang['eGuidance']['menu']['Management']['Guidance'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/guidance/?clearCoo=1",
                            $PageGuidance
                        );
                    if ($isGuidanceAdmin || $permit['MGMT']['SEN'] || $isClassTeacher || $isSubjectTeacher)
                        $MenuArr["management"]["Child"]["SEN"] = array(
                            $Lang['eGuidance']['menu']['Management']['SEN'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/sen/?clearCoo=1",
                            $PageSEN
                        );
                    if ($isGuidanceAdmin || $permit['MGMT']['SUSPEND'])
                        $MenuArr["management"]["Child"]["Suspend"] = array(
                            $Lang['eGuidance']['menu']['Management']['Suspend'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/suspend/?clearCoo=1",
                            $PageSuspend
                        );
                    if ($isGuidanceAdmin || $permit['MGMT']['TRANSFER'])
                        $MenuArr["management"]["Child"]["Transfer"] = array(
                            $Lang['eGuidance']['menu']['Management']['Transfer'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/transfer/?clearCoo=1",
                            $PageTransfer
                        );
                    if ($isGuidanceAdmin || $permit['MGMT']['PERSONAL'])
                        $MenuArr["management"]["Child"]["Personal"] = array(
                            $Lang['eGuidance']['menu']['Management']['Personal'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/personal/?clearCoo=1",
                            $PagePersonal
                        );
                    if ($plugin['eGuidance_module']['SelfImprove'] && ($isGuidanceAdmin || $permit['MGMT']['SELFIMPROVE']))
                        $MenuArr["management"]["Child"]["SelfImprove"] = array(
                            $Lang['eGuidance']['menu']['Management']['SelfImprove'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/self_improve/?clearCoo=1",
                            $PageSelfImprove
                        );
                    if ($isGuidanceAdmin || $permit['MGMT']['CLASSTEACHERMEETING'])
                        $MenuArr["management"]["Child"]["ClassteacherMeeting"] = array(
                            $Lang['eGuidance']['menu']['Management']['ClassteacherMeeting'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/classteacher_meeting/?clearCoo=1",
                            $PageClassteacherMeeting
                        );
                }
                
                // Reports Menu
                if ($isGuidanceAdmin || $permit['REPORTS']) {
                    $MenuArr["reports"] = array(
                        $Lang['eGuidance']['menu']['main']['Reports'],
                        "",
                        $PageReport
                    );
                    if ($isGuidanceAdmin || $permit['REPORTS']['STUDENTREPORT'])
                        $MenuArr["reports"]["Child"]["StudentReport"] = array(
                            $Lang['eGuidance']['menu']['Reports']['StudentReport'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/student_report/?clearCoo=1",
                            $PageStudentReport
                        );
                    if ($isGuidanceAdmin || $permit['REPORTS']['TEACHERREPORT'])
                        $MenuArr["reports"]["Child"]["TeacherReport"] = array(
                            $Lang['eGuidance']['menu']['Reports']['TeacherReport'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/teacher_report/?clearCoo=1",
                            $PageTeacherReport
                        );
                    if ($isGuidanceAdmin || $permit['REPORTS']['CLASSTEACHERREPORT'])
                        $MenuArr["reports"]["Child"]["ClassteacherReport"] = array(
                            $Lang['eGuidance']['menu']['Reports']['ClassteacherReport'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/classteacher_report/?clearCoo=1",
                            $PageClassteacherReport
                        );
                    if ($isGuidanceAdmin || $permit['REPORTS']['STUDENTPROBLEMREPORT'])
                        $MenuArr["reports"]["Child"]["StudentProblemReport"] = array(
                            $Lang['eGuidance']['menu']['Reports']['StudentProblemReport'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/studentproblem_report/?clearCoo=1",
                            $PageStudentProblemReport
                        );
                }
                
                // Settings Menu
                if ($isGuidanceAdmin || $permit['SETTINGS']) {
                    $MenuArr["guidance_setting"] = array(
                        $Lang['eGuidance']['menu']['main']['Settings'],
                        "",
                        $PageSetting
                    );
                    if ($isGuidanceAdmin || $permit['SETTINGS']['PERMISSIONSETTING'])
                        $MenuArr["guidance_setting"]["Child"]["PermissionSetting"] = array(
                            $Lang['eGuidance']['menu']['Settings']['PermissionSetting'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/settings/access_right/index.php?clearCoo=1",
                            $PagePermissionSetting
                        );
                    if ($isGuidanceAdmin || $permit['SETTINGS']['FINETUNESETTING'])
                        $MenuArr["guidance_setting"]["Child"]["FineTuneSetting"] = array(
                            $Lang['eGuidance']['menu']['Settings']['FineTuneSetting'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/settings/fine_tune/index.php?clearCoo=1",
                            $PageFineTuneSetting
                        );
                    
                    if ($isGuidanceAdmin || $permit['SETTINGS']['GENERALSETTING'])
                        $MenuArr["guidance_setting"]["Child"]["GeneralSetting"] = array(
                            $Lang['eGuidance']['menu']['Settings']['GeneralSetting'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/settings/general_settings/index.php?clearCoo=1",
                            $PageGeneralSetting
                        );
                    
                    if ($isGuidanceAdmin || $permit['SETTINGS']['IPACCESSRIGHTSETTING'])
                        $MenuArr["guidance_setting"]["Child"]["IPAccessRightSetting"] = array(
                            $Lang['eGuidance']['menu']['Settings']['IPAccessRightSetting'],
                            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/settings/allow_access_ip/index.php",
                            $PageSettings_AllowAccessIP
                        );
                }
            }

            # change page web title
            $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
            $js.= 'document.title="eClass eGuidance";'."\n";
            $js.= '</script>'."\n";
            
            // module information
            $MODULE_OBJ['title'] = $Lang['Header']['Menu']['eGuidance'].$js;
            $MODULE_OBJ['title_css'] = "menu_opened";
            $MODULE_OBJ['logo'] = $PATH_WRT_ROOT . "images/{$LAYOUT_SKIN}/leftmenu/icon_eguidance.png";
            $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/index.php";
            $MODULE_OBJ['menu'] = array();
            $MODULE_OBJ['menu'] = $MenuArr;
            
            return $MODULE_OBJ;
        }

        function getSENTabs($CurrentTag)
        {
            global $Lang;
            $TAGS_OBJ = array();
            $TagNow[$CurrentTag] = true;
            $TAGS_OBJ[] = array(
                $Lang['eGuidance']['sen']['Tab']['Service'],
                "index.php?clearCoo=1",
                $TagNow["Service"]
            );
            $TAGS_OBJ[] = array(
                $Lang['eGuidance']['sen']['Tab']['Case'],
                "case_index.php?clearCoo=1",
                $TagNow["Case"]
            );
            return $TAGS_OBJ;
        }

        // get teacher names by UserIDs
        // public function getTeacherNames($userIDs) {
        // $list = implode(",", $userIDs);
        // $nameField = getNameFieldWithLoginByLang();
        // $sql = "SELECT UserID, $nameField FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($list) AND RecordStatus = 1 ORDER BY $nameField";
        // $ret = $this->returnResultSet($sql);
        // return $ret;
        // }
        
        // return default page that a user has permission to access
        function getDefaultPage()
        {
            $page = '';
            $permission = $this->getUserPermission();
            
            if ($permission['admin']) { // module admin
                $page = "contact/index.php?clearCoo=1"; // default path
            } else {
                
                if (count($permission['current_right']['MGMT']) > 0) {
                    $keys = array_keys($permission['current_right']['MGMT']);
                    switch ($keys[0]) {
                        case 'CONTACT':
                            $page = "contact/index.php?clearCoo=1";
                            break;
                        case 'ADVANCEDCLASS':
                            $page = "advanced_class/index.php?clearCoo=1";
                            break;
                        case 'THERAPY':
                            $page = "therapy/index.php?clearCoo=1";
                            break;
                        case 'GUIDANCE':
                            $page = "guidance/index.php?clearCoo=1";
                            break;
                        case 'SEN':
                            $page = "sen/index.php?clearCoo=1";
                            break;
                        case 'SUSPEND':
                            $page = "suspend/index.php?clearCoo=1";
                            break;
                        case 'TRANSFER':
                            $page = "transfer/index.php?clearCoo=1";
                            break;
                        case 'PERSONAL':
                            $page = "personal/index.php?clearCoo=1";
                            break;
                        case 'SELFIMPROVE':
                            $page = "self_improve/index.php?clearCoo=1";
                            break;
                        case 'CLASSTEACHERMEETING':
                            $page = "classteacher_meeting/index.php?clearCoo=1";
                            break;
                    }
                } 
                else if ($permission['isClassTeacher'] || $permission['isSubjectTeacher']) {
                    $page = "sen/index.php?clearCoo=1";
                }
                else if (count($permission['current_right']['REPORTS']) > 0) {
                    $keys = array_keys($permission['current_right']['REPORTS']);
                    switch ($keys[0]) {
                        case 'STUDENTREPORT':
                            $page = "student_report/index.php";
                            break;
                        case 'TEACHERREPORT':
                            $page = "teacher_report/index.php";
                            break;
                        case 'CLASSTEACHERREPORT':
                            $page = "classteacher_report/index.php";
                            break;
                    }
                } else if (count($permission['current_right']['SETTINGS']) > 0) {
                    $keys = array_keys($permission['current_right']['SETTINGS']);
                    switch ($keys[0]) {
                        case 'PERMISSIONSETTING':
                            $page = "settings/access_right/index.php";
                            break;
                        case 'FINETUNESETTING':
                            $page = "settings/fine_tune/index.php";
                            break;
                        case 'GENERALSETTING':
                            $page = "settings/general_settings/index.php";
                            break;
                        case 'IPACCESSRIGHTSETTING':
                            $page = "settings/allow_access_ip/index.php";
                            break;
                    }
                }
            }
            
            return $page;
        }

        public function getClassByStudentID($studentID)
        {
            global $junior_mck;
            
            $rs = array();
            $name_field = getNameFieldByLang2('u.');
            if ($junior_mck) {
                $sql = "SELECT 	
								u.ClassName
						FROM 
								INTRANET_USER u 
						WHERE 
								UserID='" . $studentID . "'";
                $rs = $this->returnResultSet($sql);
            } else {
                $academicYearID = Get_Current_Academic_Year_ID();
                $sql = "SELECT 		
									yc.ClassTitleEN AS ClassName 
						FROM 
									INTRANET_USER u 
						INNER JOIN 
									YEAR_CLASS_USER ycu on ycu.UserID=u.UserID
						INNER JOIN 
									YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
						WHERE 
									yc.AcademicYearID='" . $academicYearID . "'
						AND u.UserID='" . $studentID . "'";
                $rs = $this->returnResultSet($sql);
            }
            if (count($rs)) {
                $rs = $rs[0]['ClassName'];
            } else {
                $rs = '';
            }
            return $rs;
        }

        // $teacherType = -1 denotes all teachers
        // get approved (RecordStatus=1) teacher only
        public function getTeacher($teacherType = "-1", $conds = "")
        {
            if ($teacherType == 1)
                $conds .= " AND Teaching=1";
            else if ($teacherType == 0)
                $conds .= " AND (Teaching IS NULL OR Teaching=0 OR Teaching='')";
            
            $list = array();
            $name_field = getNameFieldByLang();
            $sql = "SELECT UserID, $name_field as Name FROM INTRANET_USER WHERE RecordType=1 AND RecordStatus='1' $conds ORDER BY EnglishName";
            
            $rs = $this->returnArray($sql);
            return $rs;
        }

        public function getStudent($studentId)
        {
            $studentId = (array) $studentId;
            $studentIdSql = implode("','", $studentId);
            
            $name_field = getNameFieldWithClassNumberByLang('u.');
            $sql = "SELECT 	u.UserID, 
							{$name_field} AS Name,
							u.UserID AS StudentID
					FROM 
							INTRANET_USER u 
					WHERE 
							UserID IN ('{$studentIdSql}')";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        // use in class teacher comment import
        public function getStudentID()
        {
            $sql = "SELECT 	u.UserID,
							u.WebSAMSRegNo 
					FROM 
							INTRANET_USER u 
					WHERE 
							u.RecordType=" . USERTYPE_STUDENT;
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getStudentInfo($studentId)
        {
            global $junior_mck;
            
            $studentId = (array) $studentId;
            $studentIdSql = implode("','", $studentId);
            $rs = array();
            $name_field = getNameFieldByLang2('u.');
            if ($junior_mck) {
                $sql = "SELECT 	u.UserID AS StudentID, 
								{$name_field} AS StudentName,
								u.ClassName,
                                STRN,
								Gender,                                
                                DATE_FORMAT(DateOfBirth, '%Y-%m-%d') AS DateOfBirth
						FROM 
								INTRANET_USER u 
						WHERE 
								UserID IN ('{$studentIdSql}')";
                $rs = $this->returnResultSet($sql);
            } else {
                $academicYearID = Get_Current_Academic_Year_ID();
                $classTitle = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
                $sql = "SELECT 		u.UserID AS StudentID, 
									{$name_field} AS StudentName, 
									{$classTitle} AS ClassName,
                                    STRN, 
									Gender,
                                    DATE_FORMAT(DateOfBirth, '%Y-%m-%d') AS DateOfBirth
						FROM 
									INTRANET_USER u 
						INNER JOIN 
									YEAR_CLASS_USER ycu on ycu.UserID=u.UserID
						INNER JOIN 
									YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
						WHERE 
									yc.AcademicYearID='" . $academicYearID . "'
						AND u.UserID IN ('{$studentIdSql}')";
                $rs = $this->returnResultSet($sql);
            }
            if (count($rs)) {
                $rs = current($rs);
            }
            return $rs;
        }

        // use for student report
        public function getStudentInfo2($studentID = '', $yearClassID = '', $yearID = '', $showLeft = false)
        {
            global $junior_mck, $intranet_root;
            include_once ($intranet_root . "/includes/form_class_manage.php");
            
            if ($junior_mck) {
                $cond = '';
                $NameField = getNameFieldByLang('u.');
                
                if ($showLeft) {
                    $name_field = "IF (u.RecordStatus=3, CONCAT('*',".$NameField."), $NameField)";
                }
                else {
                    $name_field = $NameField;
                    $cond .= " AND u.RecordStatus<>3 ";
                }
                
                if ($studentID) {
                    $join = "";
                    $cond .= "AND u.UserID IN ('" . implode("','", $studentID) . "')";
                } else if ($yearClassID) {
                    $join = "INNER JOIN INTRANET_CLASS c ON c.ClassName=u.ClassName AND c.ClassID='" . $yearClassID . "'";
                } else if ($yearID) {
                    $join = "INNER JOIN (SELECT c.ClassName FROM INTRANET_CLASS c 
											INNER JOIN INTRANET_CLASSLEVEL v ON v.ClassLevelID=c.ClassLevelID AND v.ClassLevelID='" . $yearID . "') as a
								ON a.ClassName=u.ClassName";
                } else {
                    $join = "";
                }
                
                $sql = "SELECT
									u.UserID,
									$name_field as StudentName,
									u.ClassName,
									u.ClassNumber,
                                    u.STRN,
    								u.Gender,                                
                                    DATE_FORMAT(u.DateOfBirth, '%Y-%m-%d') AS DateOfBirth
						FROM
									INTRANET_USER u 
									{$join}
	                    WHERE 
									u.RecordType=" . USERTYPE_STUDENT . "
									{$cond} 
						ORDER BY u.ClassName,u.ClassNumber+0";
                $allStudent = $this->returnResultSet($sql);
            } else {
                $academicYearID = Get_Current_Academic_Year_ID();
                if ($studentID) {
                    $cond = '';
                    $NameField = getNameFieldByLang('USR.');
                    
                    if ($showLeft) {
                        $name_field = "IF (USR.RecordStatus=3, CONCAT('*',".$NameField."), $NameField)";
                    }
                    else {
                        $name_field = $NameField;
                        $cond .= " AND USR.RecordStatus<>3 ";
                    }
                    
                    $name_field = getNameFieldByLang("USR.");
                    $className = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
                    $allStudentIdSql = implode("','", $studentID);
                    $sql = "SELECT 
                                        USR.UserID, 
                                        $name_field as StudentName, 
                                        $className as ClassName, 
                                        ycu.ClassNumber,
                                        USR.STRN, 
    									USR.Gender,
                                        DATE_FORMAT(USR.DateOfBirth, '%Y-%m-%d') AS DateOfBirth
                            FROM 
                                        INTRANET_USER USR 
                            LEFT OUTER JOIN 
                                        YEAR_CLASS_USER ycu ON (USR.UserID=ycu.UserID) 
                            LEFT OUTER JOIN 
                                        YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) 
                            WHERE 
                                        USR.UserID IN ('{$allStudentIdSql}')
                                        {$cond} 
                                    AND yc.AcademicYearID='$academicYearID' 
                            order by ClassName, ClassNumber, StudentName";
                    $allStudent = $this->returnResultSet($sql);
                } else if ($yearClassID) {
                    $objYearClass = new year_class($yearClassID, $GetYearDetail = false, $GetClassTeacherList = false, $GetClassStudentList = false, $GetLockedSGArr = false);
                    $className = $objYearClass->Get_Class_Name();

                    $NameField = getNameFieldByLang('u.');
                    $ArchiveNameField = getNameFieldByLang2('au.');
                    $ArchiveSymbol = '*';
                    $SpanClass = $WithoutStyle ? $ArchiveSymbol : "<span class=\'tabletextrequire\'>". $ArchiveSymbol ."</span>";
                    $cond = $showLeft ? "" : " AND u.RecordStatus<>3 ";
                    
                    $sql = 'SELECT
                						ycu.UserID,
                						ycu.ClassNumber,
                						CASE
                							WHEN au.UserID IS NOT NULL then CONCAT(\''. $SpanClass .'\','.$ArchiveNameField.')
                							WHEN u.RecordStatus = 3  THEN CONCAT(\''. $SpanClass .'\','.$NameField.')
                							ELSE '.$NameField.'
                						END as StudentName,
                						CASE
                							WHEN au.UserID IS NOT NULL then \'1\'
                							ELSE \'0\'
                						END as ArchiveUser,
                						\''.$className.'\' as ClassName, u.UserLogin,
                                        u.STRN,
        								u.Gender,                                
                                        DATE_FORMAT(u.DateOfBirth, \'%Y-%m-%d\') AS DateOfBirth
        					FROM
                						YEAR_CLASS_USER as ycu
                			LEFT JOIN   
                                        INTRANET_USER as u ON (ycu.UserID = u.UserID)
                			LEFT JOIN
                						INTRANET_ARCHIVE_USER as au ON (ycu.UserID = au.UserID)
        					WHERE
        						        YearClassID = \''.$yearClassID.'\'
                                        '.$cond.'
        					ORDER BY
        						        ClassNumber';
                    $allStudent = $this->returnResultSet($sql);
                    
                } else if ($yearID) {
//                     $objYear = new Year($yearID);
//                     $allStudent = $objYear->Get_All_Student($academicYearID);
                        
                    $ClassNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
                    $ArchiveSymbol = '*';
                    $NameField = getNameFieldByLang('u.');
                    $ArchiveNameField = getNameFieldByLang2('au.');
                    $cond = $showLeft ? "" : " AND u.RecordStatus<>3 ";
                    
                    $sql = "SELECT
            						ycu.UserID,
            						ycu.YearClassID,
            						ycu.ClassNumber,
            						CASE
            							WHEN au.UserID IS NOT NULL then CONCAT('<span class=\"tabletextrequire\">".$ArchiveSymbol."</span>', ".$ArchiveNameField.")
            							WHEN u.RecordStatus = 3  THEN CONCAT('<span class=\"tabletextrequire\">".$ArchiveSymbol."</span>', ".$NameField.")
            							ELSE $NameField
            							END as StudentName,
            						$ClassNameField as ClassName,
                                    u.STRN,
    								u.Gender,                                
                                    DATE_FORMAT(u.DateOfBirth, '%Y-%m-%d') AS DateOfBirth
            				FROM
            							YEAR_CLASS_USER as ycu
            				INNER JOIN
            							YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
            				INNER JOIN
            							YEAR as y On (yc.YearID = y.YearID)
            				LEFT JOIN
            							INTRANET_USER as u ON (ycu.UserID = u.UserID)
            				LEFT JOIN
            							INTRANET_ARCHIVE_USER as au ON (ycu.UserID = au.UserID)
            				WHERE
            							yc.YearID = '".$yearID."'
            				AND
            						yc.AcademicYearID = '".$academicYearID."'
                                    ". $cond . "
            				ORDER BY
            						y.Sequence, yc.Sequence, ycu.ClassNumber
            				";
                	$allStudent = $this->returnResultSet($sql);
                } else {
                    $className = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
                    
                    $cond = '';
                    $NameField = getNameFieldByLang('USR.');
                    
                    if ($showLeft) {
                        $name_field = "IF (USR.RecordStatus=3, CONCAT('*',".$NameField."), $NameField)";
                    }
                    else {
                        $name_field = $NameField;
                        $cond .= " AND USR.RecordStatus<>3 ";
                    }
                    
                    $sql = "SELECT  
                                            USR.UserID, 
                                            $name_field as StudentName, 
                                            $className as ClassName, 
                                            ycu.ClassNumber
                                            USR.STRN, 
        									USR.Gender,
                                            DATE_FORMAT(USR.DateOfBirth, '%Y-%m-%d') AS DateOfBirth
                            FROM 
                                            INTRANET_USER USR 
                            LEFT OUTER JOIN 
                                            YEAR_CLASS_USER ycu ON (USR.UserID=ycu.UserID) 
                            LEFT OUTER JOIN 
                                            YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) 
                            WHERE 
                                            USR.RecordType=" . USERTYPE_STUDENT . "
                                            {$cond} 
                            AND 
                                            yc.AcademicYearID='$academicYearID' 
                            ORDER BY 
                                            ClassName, ClassNumber, StudentName";
                    $allStudent = $this->returnResultSet($sql);
                }
            }
            return $allStudent;
        }

        public function getSelectedStudent($table, $field, $value, $cond = '', $orderBy = 'Name')
        {
            $name_field = getNameFieldWithClassNumberByLang('u.');
            $sql = "SELECT 	u.UserID, 
							{$name_field} AS Name ,
							s.{$field}
					FROM 
							INTRANET_USER u 
					INNER JOIN 
							{$table} s ON s.StudentID=u.UserID
					WHERE 
							s.{$field}='{$value}' {$cond}
					ORDER BY " . $orderBy;
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSelectedTeacher($table, $field, $value, $cond = '')
        {
            $name_field = getNameFieldByLang('u.');
            $sql = "SELECT 	u.UserID, 
							{$name_field} AS Name,
							t.{$field}
					FROM 
							INTRANET_USER u 
					INNER JOIN 
							{$table} t ON t.TeacherID=u.UserID
					WHERE 
							t.{$field}='{$value}' {$cond}
					ORDER BY Name";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        // for IP, compare against ClassTitleEN, exclude left student
        public function getStudentNameListWClassNumberByClassName($className, $includeUserIdAry = '', $excludeUserIdAry = '', $assoc = false)
        {
            global $junior_mck;
            
            $conds_userId = '';
            $recordstatus = "'0','1','2'";
            
            if ($includeUserIdAry != '') {
                $conds_userId .= " AND u.UserID IN ('" . implode("','", (array) $includeUserIdAry) . "') ";
            }
            
            if ($excludeUserIdAry != '') {
                $conds_userId .= " AND u.UserID NOT IN ('" . implode("','", (array) $excludeUserIdAry) . "') ";
            }
            
            if ($junior_mck) {
                $name_field = getNameFieldWithClassNumberByLang('u.');
                $sql = "SELECT 
								u.UserID, 
								$name_field as StudentName  
						FROM 
								INTRANET_USER u 
						WHERE 
								u.RecordType = 2 
						AND 
								u.RecordStatus IN ($recordstatus) 
						AND 
								u.ClassName = '" . $this->Get_Safe_Sql_Query($className) . "' 
								$conds_userId 
						ORDER BY u.ClassNumber+0";
            } else {
                $CurrentAcademicYearID = Get_Current_Academic_Year_ID();
                $username_field = getNameFieldByLang("u.", $displayLang = "", $isTitleDisabled = true);
                
                $classNameField = 'ClassTitleEN'; // use English ClassName
                $student_name = "CONCAT($username_field,IF(ycu.ClassNumber IS NULL OR ycu.ClassNumber='','',CONCAT(' (',yc.{$classNameField},'-',ycu.ClassNumber,')'))) ";
                
                $sql = "SELECT 		u.UserID, 
									{$student_name} AS StudentName							 
						FROM 
									INTRANET_USER u 
						INNER JOIN 
									YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
						INNER JOIN 
									YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID 
						AND 	
									yc.AcademicYearID='" . $CurrentAcademicYearID . "' 
						AND		
									yc.ClassTitleEN='" . $this->Get_Safe_Sql_Query($className) . "' 
						WHERE 
									u.RecordType=2
						AND 
									u.RecordStatus IN ($recordstatus) 
									{$conds_userId}
						ORDER BY ycu.ClassNumber+0";
            }
            $rs = $assoc ? $this->returnResultSet($sql) : $this->returnArray($sql); // need returnArray for getSelectByArray function
            
            return $rs;
        }

        public function getStudentNameByClassWithFilter($className, $excludeUserIdAry = array())
        {
            global $junior_mck;
            
            if (count($excludeUserIdAry) > 0) {
                $conds_userId = " AND u.UserID NOT IN ('" . implode("','", (array) $excludeUserIdAry) . "') ";
            }
            if ($junior_mck) {
                $name_field = getNameFieldWithClassNumberByLang();
                $sql = "SELECT 	u.UserID, 
								$name_field AS StudentName 
						FROM 
								INTRANET_USER u 
						WHERE 
								RecordType=2 
							AND RecordStatus IN ('0','1','2') 
							AND ClassName = '" . $this->Get_Safe_Sql_Query($className) . "' 
							$conds_userId 
						ORDER BY ClassNumber"; // exclude left student
                
                $rs = $this->returnResultSet($sql);
            } else {
                $rs = $this->getStudentNameListWClassNumberByClassName($className, '', $excludeUserIdAry, true);
            }
            
            return $rs;
        }

        // /////////////////////////////////////////////////////////////////////////////////////////
        // Sub modules start
        
        // ## Contact functions start
        public function geteGuidanceAttachment($userID = '', $target = '', $recordID = '', $batchUploadTime = '')
        {
            $sql = "SELECT 
						FileID, 
						LastModifiedBy, 
						UploadTime,
						OriginalFileName,
						EncodeFileName 
					FROM INTRANET_GUIDANCE_ATTACHMENT 
					WHERE 1 ";
            if ($userID) {
                $sql .= " AND LastModifiedBy = '" . $userID . "'";
            }
            if ($target) {
                $sql .= " AND Target='" . $target . "'";
            }
            if ($recordID) {
                $sql .= " AND RecordID='" . $recordID . "'";
            } else if ($batchUploadTime) {
                $sql .= " AND DATE_FORMAT(BatchUploadTime,'%Y-%m-%d %H:%i:%s') = '$batchUploadTime'";
            }
            
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getContactByTeacherId($teacherID = array())
        {
            $teacherID = (array) $teacherID;
            $teacherIDSql = implode("','", $teacherID);
            
            $sql = "SELECT 
			    IGC.*,
			    IGCT.TeacherID
		    FROM 
			    INTRANET_GUIDANCE_CONTACT IGC
		    INNER JOIN
			    INTRANET_GUIDANCE_CONTACT_TEACHER IGCT
		    ON
			    IGC.ContactID = IGCT.ContactID
			AND
			    IGCT.TeacherID IN ('{$teacherIDSql}')
		    ";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getContactByStudentId($studentID = array())
        {
            $studentID = (array) $studentID;
            $studentIDSql = implode("','", $studentID);
            
            $sql = "SELECT * FROM INTRANET_GUIDANCE_CONTACT WHERE 1 ";
            if ($studentIDSql) {
                $sql .= " AND StudentID IN ('{$studentIDSql}')";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getContact($contactID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_CONTACT WHERE 1 ";
            if ($contactID) {
                $sql .= " AND ContactID='" . $contactID . "'";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getContactTeacher($contactID)
        {
            if (is_array($contactID)) {
                $contactIDSql = implode("','", $contactID);
                $cond = " OR ContactID IN ('{$contactIDSql}')";
                
                return $this->getSelectedTeacher('INTRANET_GUIDANCE_CONTACT_TEACHER', 'ContactID', '-1', $cond);
            }
            return $this->getSelectedTeacher('INTRANET_GUIDANCE_CONTACT_TEACHER', 'ContactID', $contactID);
        }

        // ## Advanced Class functions start
        public function getAdvancedClassByStudentId($studentID = array())
        {
            $studentID = (array) $studentID;
            $studentIDSql = implode("','", $studentID);
            
            $sql = "SELECT 
			    IGAC.*,
			    IGACS.StudentID
		    FROM 
			    INTRANET_GUIDANCE_ADVANCED_CLASS IGAC
		    INNER JOIN
			    INTRANET_GUIDANCE_ADVANCED_CLASS_STUDENT IGACS
		    ON
			    IGAC.ClassID = IGACS.ClassID
		    AND
			    IGACS.StudentID IN ('{$studentIDSql}')";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getAdvancedClassByTeacherId($teacherID = array())
        {
            $teacherID = (array) $teacherID;
            $teacherIDSql = implode("','", $teacherID);
            
            $sql = "SELECT 
			    IGAC.*,
			    IGACT.TeacherID
		    FROM 
			    INTRANET_GUIDANCE_ADVANCED_CLASS IGAC
		    INNER JOIN
			    INTRANET_GUIDANCE_ADVANCED_CLASS_TEACHER IGACT
		    ON
			    IGAC.ClassID = IGACT.ClassID
		    AND
			    IGACT.TeacherID IN ('{$teacherIDSql}')";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getAdvancedClass($classID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_ADVANCED_CLASS WHERE 1 ";
            if ($classID) {
                $sql .= " AND ClassID='" . $classID . "'";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getAdvancedClassLesson($classID = '', $lessonID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON WHERE 1 ";
            if (is_array($classID)) {
                $sql .= " AND ClassID IN ('" . implode("','", $classID) . "')";
            } else if ($classID) {
                $sql .= " AND ClassID='" . $classID . "'";
            }
            if ($lessonID) {
                $sql .= " AND LessonID='" . $lessonID . "'";
            }
            $sql .= " ORDER BY LessonDate";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getAdvancedClassLessonResult($lessonID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON_RESULT WHERE 1 ";
            if ($lessonID) {
                $lessonID = (array) $lessonID;
                $lessonIdSql = implode("','", $lessonID);
                $sql .= " AND LessonID IN ('{$lessonIdSql}')";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getAdvancedClassStudent($classID, $orderBy = 'Name')
        {
            $classID = (array) $classID;
            $classIDSql = implode("','", $classID);
            
            return $this->getSelectedStudent('INTRANET_GUIDANCE_ADVANCED_CLASS_STUDENT', 'ClassID', '-1', " OR ClassID IN ('{$classIDSql}')", $orderBy);
        }

        public function getAdvancedClassTeacher($classID)
        {
            if (is_array($classID)) {
                $classIDSql = implode("','", $classID);
                $cond = " OR ClassID IN ('{$classIDSql}')";
                
                return $this->getSelectedTeacher('INTRANET_GUIDANCE_ADVANCED_CLASS_TEACHER', 'ClassID', '-1', $cond);
            }
            return $this->getSelectedTeacher('INTRANET_GUIDANCE_ADVANCED_CLASS_TEACHER', 'ClassID', $classID);
        }

        // exclude $studentID
        public function getAdvancedClassStudentID($classID, $studentID = array())
        {
            $sql = "SELECT StudentID FROM INTRANET_GUIDANCE_ADVANCED_CLASS_STUDENT WHERE ClassID='" . $classID . "' AND StudentID NOT IN ('" . implode("','", (array) $studentID) . "')";
            $rs_student = $this->returnVector($sql);
            return $rs_student;
        }

        public function getAdvancedClassTeacherID($classID, $teacherID = array())
        {
            $sql = "SELECT TeacherID FROM INTRANET_GUIDANCE_ADVANCED_CLASS_TEACHER WHERE ClassID='" . $classID . "' AND TeacherID NOT IN ('" . implode("','", (array) $teacherID) . "')";
            $rs_teacher = $this->returnVector($sql);
            return $rs_teacher;
        }

        public function checkAdvancedClassStudentUsed($classID, $studentID = array())
        {
            $isUsed = false;
            $rs_student = $this->getAdvancedClassStudentID($classID, $studentID);
            if (count($rs_student) > 0) {
                $sql = "SELECT 
								r.StudentID 
						FROM
								INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON_RESULT r
						INNER JOIN 
								INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON s ON s.LessonID=r.LessonID
						WHERE 
								s.ClassID='" . $classID . "'
						AND		r.StudentID IN ('" . implode("','", $rs_student) . "')";
                $rs = $this->returnVector($sql);
                
                if (count($rs) > 0) {
                    $isUsed = true;
                }
            }
            return $isUsed;
        }

        // ## Therapy functions start
        public function getTherapyByStudentId($studentId = '')
        {
            $studentId = (array) $studentId;
            $studentIdSql = implode("','", $studentId);
            
            $sql = "SELECT 
			    IGT.*, 
			    IGTS.StudentID
		    FROM 
			    INTRANET_GUIDANCE_THERAPY IGT
		    INNER JOIN
			    INTRANET_GUIDANCE_THERAPY_STUDENT IGTS
		    ON
			    IGT.TherapyID = IGTS.TherapyID
		    AND
			    IGTS.StudentID IN ('{$studentIdSql}')";
            
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getTherapyByTeacherId($teacherId = '')
        {
            $teacherId = (array) $teacherId;
            $teacherIdSql = implode("','", $teacherId);
            
            $sql = "SELECT 
			    IGT.*, 
			    IGTT.TeacherID
		    FROM 
			    INTRANET_GUIDANCE_THERAPY IGT
		    INNER JOIN
			    INTRANET_GUIDANCE_THERAPY_TEACHER IGTT
		    ON
			    IGT.TherapyID = IGTT.TherapyID
		    AND
			    IGTT.TeacherID IN ('{$teacherIdSql}')";
            
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getTherapy($therapyID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_THERAPY WHERE 1 ";
            if ($therapyID) {
                $sql .= " AND TherapyID='" . $therapyID . "'";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getTherapyActivity($therapyID = '', $activityID = '', $orderBy = 'ActivityDate')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_THERAPY_ACTIVITY WHERE 1 ";
            if (is_array($therapyID)) {
                $sql .= " AND TherapyID IN ('" . implode("','", $therapyID) . "')";
            } else if ($therapyID) {
                $sql .= " AND TherapyID='" . $therapyID . "'";
            }
            if ($activityID) {
                $sql .= " AND ActivityID='" . $activityID . "'";
            }
            $sql .= " ORDER BY " . $orderBy;
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getTherapyActivityResult($activityID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_THERAPY_ACTIVITY_RESULT WHERE 1 ";
            if ($activityID) {
                $activityID = (array) $activityID;
                $activityIdSql = implode("','", $activityID);
                $sql .= " AND ActivityID IN ('{$activityIdSql}')";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getTherapyStudent($therapyID, $orderBy = 'Name')
        {
            $therapyID = (array) $therapyID;
            $therapyIDSql = implode("','", $therapyID);
            return $this->getSelectedStudent('INTRANET_GUIDANCE_THERAPY_STUDENT', 'TherapyID', '-1', " OR TherapyID IN ('{$therapyIDSql}')", $orderBy);
        }

        public function getTherapyTeacher($therapyID)
        {
            if (is_array($therapyID)) {
                $therapyIDSql = implode("','", $therapyID);
                $cond = " OR TherapyID IN ('{$therapyIDSql}')";
                
                return $this->getSelectedTeacher('INTRANET_GUIDANCE_THERAPY_TEACHER', 'TherapyID', '-1', $cond);
            }
            return $this->getSelectedTeacher('INTRANET_GUIDANCE_THERAPY_TEACHER', 'TherapyID', $therapyID);
        }

        // public function isTherapyHasActivity($therapyID='') {
        // $sql = "SELECT ActivityID FROM INTRANET_GUIDANCE_THERAPY_ACTIVITY WHERE TherapyID='".$therapyID."' LIMIT 1";
        // $rs = $this->returnResultSet($sql);
        // return count($rs)>0 ? true : false;
        // }
        public function getTherapyStudentID($therapyID, $studentID = array())
        {
            $sql = "SELECT StudentID FROM INTRANET_GUIDANCE_THERAPY_STUDENT WHERE TherapyID='" . $therapyID . "' AND StudentID NOT IN ('" . implode("','", (array) $studentID) . "')";
            $rs_student = $this->returnVector($sql);
            return $rs_student;
        }

        public function getTherapyTeacherID($therapyID, $teacherID = array())
        {
            $sql = "SELECT TeacherID FROM INTRANET_GUIDANCE_THERAPY_TEACHER WHERE TherapyID='" . $therapyID . "' AND TeacherID NOT IN ('" . implode("','", (array) $teacherID) . "')";
            $rs_teacher = $this->returnVector($sql);
            return $rs_teacher;
        }

        public function checkTherapyStudentUsed($therapyID, $studentID = array())
        {
            $isUsed = false;
            $rs_student = $this->getTherapyStudentID($therapyID, $studentID);
            if (count($rs_student) > 0) {
                $sql = "SELECT 
								r.StudentID 
						FROM
								INTRANET_GUIDANCE_THERAPY_ACTIVITY_RESULT r
						INNER JOIN 
								INTRANET_GUIDANCE_THERAPY_ACTIVITY s ON s.ActivityID=r.ActivityID
						WHERE 
								s.TherapyID='" . $therapyID . "'
						AND		r.StudentID IN ('" . implode("','", $rs_student) . "')";
                $rs = $this->returnVector($sql);
                
                if (count($rs) > 0) {
                    $isUsed = true;
                }
            }
            return $isUsed;
        }

        // public function checkTherapyActivityCreated($therapyID,$teacherID=array()) {
        // $isCreated = false;
        // $rs_teacher = $this->getTherapyTeacherID($therapyID,$teacherID);
        //
        // if (count($rs_teacher)>0) {
        // $sql = "SELECT ActivityID FROM INTRANET_GUIDANCE_THERAPY_ACTIVITY WHERE TherapyID='".$therapyID."'";
        // $rs = $this->returnVector($sql);
        //
        // if (count($rs)>0) {
        // $isCreated = true;
        // }
        // }
        // return $isCreated;
        // }
        
        // ## Guidance functions start
        public function getGuidanceByStudentId($studentID = '')
        {
            $studentID = (array) $studentID;
            $studentIdSql = implode("','", $studentID);
            $sql = "SELECT * FROM INTRANET_GUIDANCE_GUIDANCE WHERE 1 ";
            if ($studentIdSql) {
                $sql .= " AND StudentID IN ('{$studentIdSql}')";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getGuidanceByTeacherId($teacherID = '')
        {
            $teacherID = (array) $teacherID;
            $teacherIdSql = implode("','", $teacherID);
            $sql = "SELECT 
			    IGG.*,
			    IGGT.TeacherID
		    FROM 
			    INTRANET_GUIDANCE_GUIDANCE IGG 
		    INNER JOIN
			    INTRANET_GUIDANCE_GUIDANCE_TEACHER IGGT
		    ON
		        IGG.GuidanceID = IGGT.GuidanceID
		    AND
			    IGGT.TeacherID IN ('{$teacherIdSql}')";
            
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getGuidance($guidanceID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_GUIDANCE WHERE 1 ";
            if ($guidanceID) {
                $sql .= " AND GuidanceID='" . $guidanceID . "'";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getGuidanceTeacher($guidanceID)
        {
            if (is_array($guidanceID)) {
                $guidanceIDSql = implode("','", $guidanceID);
                $cond = " OR GuidanceID IN ('{$guidanceIDSql}')";
                
                return $this->getSelectedTeacher('INTRANET_GUIDANCE_GUIDANCE_TEACHER', 'GuidanceID', '-1', $cond);
            }
            return $this->getSelectedTeacher('INTRANET_GUIDANCE_GUIDANCE_TEACHER', 'GuidanceID', $guidanceID);
        }

        // ## SENService functions start
        public function getSENServiceByStudentID($studentID = '')
        {
            $studentID = (array) $studentID;
            $studentIdSql = implode("','", $studentID);
            $sql = "SELECT 
			    IGSS.*,
			    IGSSS.StudentID
		    FROM 
			    INTRANET_GUIDANCE_SEN_SERVICE IGSS
		    INNER JOIN
			    INTRANET_GUIDANCE_SEN_SERVICE_STUDENT IGSSS
		    ON
			    IGSS.ServiceID = IGSSS.ServiceID
			AND 
			    IGSSS.StudentID IN ('{$studentIdSql}')";
            
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSENServiceByTeacherID($teacherID = '')
        {
            $teacherID = (array) $teacherID;
            $teacherIdSql = implode("','", $teacherID);
            $sql = "SELECT 
			    IGSS.*,
			    IGSST.TeacherID
		    FROM 
			    INTRANET_GUIDANCE_SEN_SERVICE IGSS
		    INNER JOIN
			    INTRANET_GUIDANCE_SEN_SERVICE_TEACHER IGSST
		    ON
			    IGSS.ServiceID = IGSST.ServiceID
			AND 
			    IGSST.TeacherID IN ('{$teacherIdSql}')";
            
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSENService($serviceID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_SEN_SERVICE WHERE 1 ";
            if ($serviceID) {
                $sql .= " AND ServiceID='" . $serviceID . "'";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSENServiceActivity($serviceID = '', $activityID = '', $orderBy = 'ActivityDate')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY WHERE 1 ";
            if (is_array($serviceID)) {
                $sql .= " AND ServiceID IN ('" . implode("','", $serviceID) . "')";
            } else if ($serviceID) {
                $sql .= " AND ServiceID='" . $serviceID . "'";
            }
            if ($activityID) {
                $sql .= " AND ActivityID='" . $activityID . "'";
            }
            $sql .= " ORDER BY " . $orderBy;
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSENServiceActivityResult($activityID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY_RESULT WHERE 1 ";
            if ($activityID) {
                $activityID = (array) $activityID;
                $activityIDSql = implode("','", $activityID);
                $sql .= " AND ActivityID IN ('{$activityIDSql}')";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSENServiceStudent($serviceID, $orderBy = 'Name', $condition = '')
        {
            if (is_array($serviceID)) {
                $serviceIDSql = implode("','", $serviceID);
                if ($condition) {
                    $cond = " OR (ServiceID IN ('{$serviceIDSql}')".$condition.")";
                }
                else {
                    $cond = " OR ServiceID IN ('{$serviceIDSql}')";
                }
                return $this->getSelectedStudent('INTRANET_GUIDANCE_SEN_SERVICE_STUDENT', 'ServiceID', '-1', $cond, $orderBy);
            }
            return $this->getSelectedStudent('INTRANET_GUIDANCE_SEN_SERVICE_STUDENT', 'ServiceID', $serviceID, $condition, $orderBy);
        }

        public function getSENServiceTeacher($serviceID)
        {
            if (is_array($serviceID)) {
                $serviceIDSql = implode("','", $serviceID);
                $cond = " OR ServiceID IN ('{$serviceIDSql}')";
                
                return $this->getSelectedTeacher('INTRANET_GUIDANCE_SEN_SERVICE_TEACHER', 'ServiceID', '-1', $cond);
            }
            return $this->getSelectedTeacher('INTRANET_GUIDANCE_SEN_SERVICE_TEACHER', 'ServiceID', $serviceID);
        }

        // public function isSENServiceHasActivity($serviceID='') {
        // $sql = "SELECT ActivityID FROM INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY WHERE ServiceID='".$serviceID."' LIMIT 1";
        // $rs = $this->returnResultSet($sql);
        // return count($rs)>0 ? true : false;
        // }
        public function getSENServiceStudentID($serviceID, $studentID = array())
        {
            $sql = "SELECT StudentID FROM INTRANET_GUIDANCE_SEN_SERVICE_STUDENT WHERE ServiceID='" . $serviceID . "' AND StudentID NOT IN ('" . implode("','", (array) $studentID) . "')";
            $rs_student = $this->returnVector($sql);
            return $rs_student;
        }

        public function getSENServiceTeacherID($serviceID, $teacherID = array())
        {
            $sql = "SELECT TeacherID FROM INTRANET_GUIDANCE_SEN_SERVICE_TEACHER WHERE ServiceID='" . $serviceID . "' AND TeacherID NOT IN ('" . implode("','", (array) $teacherID) . "')";
            $rs_teacher = $this->returnVector($sql);
            return $rs_teacher;
        }

        public function checkSENServiceStudentUsed($serviceID, $studentID = array())
        {
            $isUsed = false;
            $rs_student = $this->getSENServiceStudentID($serviceID, $studentID);
            if (count($rs_student) > 0) {
                $sql = "SELECT 
								r.StudentID 
						FROM
								INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY_RESULT r
						INNER JOIN 
								INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY s ON s.ActivityID=r.ActivityID
						WHERE 
								s.ServiceID='" . $serviceID . "'
						AND		r.StudentID IN ('" . implode("','", $rs_student) . "')";
                $rs = $this->returnVector($sql);
                
                if (count($rs) > 0) {
                    $isUsed = true;
                }
            }
            return $isUsed;
        }

        // public function checkSENServiceActivityCreated($serviceID,$teacherID=array()) {
        // $isCreated = false;
        // $rs_teacher = $this->getSENServiceTeacherID($serviceID,$teacherID);
        //
        // if (count($rs_teacher)>0) {
        // $sql = "SELECT ActivityID FROM INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY WHERE ServiceID='".$serviceID."'";
        // $rs = $this->returnVector($sql);
        //
        // if (count($rs)>0) {
        // $isCreated = true;
        // }
        // }
        // return $isCreated;
        // }
        
        // ## SEN Case functions start
        public function getSENAdjustType($typeID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_SETTING_ADJUST_TYPE WHERE 1 ";
            if ($typeID) {
                $sql .= " AND TypeID='" . $typeID . "'";
            }
            $sql .= " ORDER BY SeqNo";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSENAdjustItem($typeID = '', $itemID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_SETTING_ADJUST_ITEM WHERE 1 ";
            if ($typeID) {
                $sql .= " AND TypeID='" . $typeID . "'";
            }
            if ($itemID) {
                $sql .= " AND ItemID='" . $itemID . "'";
            }
            $sql .= " ORDER BY TypeID, SeqNo";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSENSupportService($typeID = '', $serviceID = '', $scope = '')
        {
            $sql = "SELECT s.*, t.Type FROM INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE s ";
            $sql .= "INNER JOIN INTRANET_GUIDANCE_SETTING_SERVICE_TYPE t ON t.TypeID=s.TypeID ";
            $sql .= "WHERE 1";
            if ($typeID) {
                $sql .= " AND s.TypeID='" . $typeID . "'";
            }
            if ($serviceID) {
                $sql .= " AND s.ServiceID='" . $serviceID . "'";
            }
            if ($scope) {
                $sql .= " AND t.Type='" . $scope . "' ";
            }
            $sql .= " ORDER BY t.Type, s.TypeID, s.SeqNo";
            
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSENCase($studentID = '', $senStatus='')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_SEN_CASE WHERE 1 ";
            if ($studentID) {
                $studentID = (array) $studentID;
                $studentIDSql = implode("','", $studentID);
                $sql .= " AND StudentID IN ('{$studentIDSql}')";
            }
            if ($senStatus !== '') {
                $sql .= " AND IsSEN='".$senStatus."'";
            }
            $rs = $this->returnResultSet($sql);
            
            return $rs;
        }

        public function getSENCaseByTeacherID($teacherID = '')
        {
            $teacherID = (array) $teacherID;
            $teacherIDSql = implode("','", $teacherID);
            
            $sql = "SELECT 
			    IGSC.*,
			    IGSCT.TeacherID
		    FROM 
			    INTRANET_GUIDANCE_SEN_CASE IGSC
		    INNER JOIN 
			    INTRANET_GUIDANCE_SEN_CASE_TEACHER IGSCT
	        ON
			    IGSC.StudentID = IGSCT.StudentID
		    AND
			    IGSCT.TeacherID IN ('{$teacherIDSql}')";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSENCaseByClass($className, $excludeStudentID = '')
        {
            $AcademicYearID = Get_Current_Academic_Year_ID();
            $sql = "SELECT 	c.StudentID 
					FROM 	INTRANET_GUIDANCE_SEN_CASE c	
					INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=c.StudentID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $AcademicYearID . "'
					WHERE yc.ClassTitleEN='" . $className . "'";
            if ($excludeStudentID) {
                $sql .= " AND c.StudentID<>'" . $excludeStudentID . "'";
            }
            $rs = $this->returnVector($sql);
            return $rs;
        }

        public function getSENCaseTeacher($studentID)
        {
            if (is_array($studentID)) {
                $studentIDSql = implode("','", $studentID);
                $cond = " OR StudentID IN ('{$studentIDSql}')";
                
                return $this->getSelectedTeacher('INTRANET_GUIDANCE_SEN_CASE_TEACHER', 'StudentID', '-1', $cond);
            }
            return $this->getSelectedTeacher('INTRANET_GUIDANCE_SEN_CASE_TEACHER', 'StudentID', $studentID);
        }

        public function getSENCaseAdjustment($studentID = '', $itemID = '')
        {
            $sql = "SELECT a.* FROM INTRANET_GUIDANCE_SEN_CASE_ADJUSTMENT a ";
            $sql .= "LEFT JOIN INTRANET_GUIDANCE_SETTING_ADJUST_ITEM i ON i.ItemID=a.ItemID ";
            $sql .= "LEFT JOIN INTRANET_GUIDANCE_SETTING_ADJUST_TYPE t ON t.TypeID=i.TypeID ";
            $sql .= "WHERE 1";
            if ($studentID) {
                $studentID = (array) $studentID;
                $studentIDSql = implode("','", $studentID);
                $sql .= " AND a.StudentID IN ('{$studentIDSql}')";
            }
            if ($itemID) {
                $sql .= " AND a.ItemID='" . $itemID . "'";
            }
            $sql .= " ORDER BY a.StudentID, t.SeqNo, i.SeqNo";
            
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSENCaseSupport($studentID = '', $serviceID = '', $type = '')
        {
            $sql = "SELECT s.* FROM INTRANET_GUIDANCE_SEN_CASE_SUPPORT s ";
            $sql .= "LEFT JOIN INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE r ON r.ServiceID=s.ServiceID ";
            $sql .= "LEFT JOIN INTRANET_GUIDANCE_SETTING_SERVICE_TYPE t ON t.TypeID=r.TypeID ";
            
            $sql .= " WHERE 1";
            if ($studentID) {
                $studentID = (array) $studentID;
                $studentIDSql = implode("','", $studentID);
                $sql .= " AND s.StudentID IN ('{$studentIDSql}')";
            }
            if ($serviceID) {
                $sql .= " AND s.ServiceID='" . $serviceID . "'";
            }
            if ($type) {
                $sql .= " AND t.Type='" . $type . "'";
            }
            $sql .= " ORDER BY s.StudentID, t.SeqNo, r.SeqNo";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSENTypeConfirmDate($studentID, $senType='')
        {
            $sql = "SELECT SENType, ConfirmDate FROM INTRANET_GUIDANCE_SENTYPE_CONFIRM_DATE WHERE StudentID='".$studentID."'";
            if ($senType !== '') {
                $sql .= " AND SENType='".$this->Get_Safe_Sql_Query($senType)."'";
            }
            $rs = $this->returnResultSet($sql);
            $assoc = BuildMultiKeyAssoc($rs,array('SENType'),array('ConfirmDate'),1);
            return $assoc;
        }
        
        public function getIntranetSENItems($codeID = '', $code = '')
        {
            $sql = "SELECT * FROM INTRANET_SEN_ITEM WHERE 1 ";
            if ($codeID) {
                $sql .= " AND CodeID='" . $codeID . "'";
            }
            if ($code) {
                $sql .= " AND Code='" . $code . "'";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        // ## suspend start
        public function getSuspend($suspendID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_SUSPEND WHERE 1 ";
            if ($suspendID) {
                $sql .= " AND SuspendID='" . $suspendID . "'";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSuspendByTeacherId($teacherID = array())
        {
            $teacherID = (array) $teacherID;
            $teacherIDSql = implode("','", $teacherID);
            
            $sql = "SELECT 
			    IGC.*,
			    IGCT.TeacherID
		    FROM 
			    INTRANET_GUIDANCE_SUSPEND IGC
		    INNER JOIN
			    INTRANET_GUIDANCE_SUSPEND_TEACHER IGCT
		    ON
			    IGC.SuspendID = IGCT.SuspendID
			AND
			    IGCT.TeacherID IN ('{$teacherIDSql}')
		    ";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSuspendTeacher($suspendID)
        {
            if (is_array($suspendID)) {
                $suspendIDSql = implode("','", $suspendID);
                $cond = " OR SuspendID IN ('{$suspendIDSql}')";
                
                return $this->getSelectedTeacher('INTRANET_GUIDANCE_SUSPEND_TEACHER', 'SuspendID', '-1', $cond);
            }
            return $this->getSelectedTeacher('INTRANET_GUIDANCE_SUSPEND_TEACHER', 'SuspendID', $suspendID);
        }

        // ## transfer start
        public function getTransfer($transferID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_TRANSFER WHERE 1 ";
            if ($transferID) {
                $sql .= " AND TransferID='" . $transferID . "'";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        // public function getTransferTeacher($transferID) {
        // return $this->getSelectedTeacher('INTRANET_GUIDANCE_TRANSFER_TEACHER','TransferID',$transferID);
        // }
        
        // ## settings
        public function checkDuplicateAccessRightGroup($groupID, $groupTitle, $groupType = 'A')
        {
            $sql = "SELECT GroupID FROM ACCESS_RIGHT_GROUP WHERE Module='" . $this->Module . "' AND GroupType='" . $groupType . "'";
            if ($groupID) {
                $sql .= " AND GroupID<>'" . $groupID . "'";
            }
            $sql .= " AND GroupTitle='" . $this->Get_Safe_Sql_Like_Query(intranet_htmlspecialchars($groupTitle)) . "'";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getAccessRight($groupID)
        {
            $sql = "SELECT * FROM ACCESS_RIGHT_GROUP_SETTING WHERE GroupID='" . $groupID . "'";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function isAccessRightExist($groupID, $section, $function)
        {
            $sql = "SELECT GroupSettingID FROM ACCESS_RIGHT_GROUP_SETTING WHERE GroupID='" . $groupID . "' AND Module='" . $this->Module . "'";
            $sql .= " AND Section='" . $section . "' AND Function='" . $function . "'";
            $rs = $this->returnResultSet($sql);
            return count($rs) ? true : false;
        }

        function getFineTuneTabs($CurrentTag)
        {
            global $Lang;
            $TAGS_OBJ = array();
            $TagNow[$CurrentTag] = true;
            $TAGS_OBJ[] = array(
                $Lang['eGuidance']['settings']['FineTune']['Tab']['Arrangement'],
                "index.php?clearCoo=1",
                $TagNow["Arrangement"]
            );
            $TAGS_OBJ[] = array(
                $Lang['eGuidance']['settings']['FineTune']['Tab']['Support'],
                "service_type_index.php?clearCoo=1",
                $TagNow["Support"]
            );
            $TAGS_OBJ[] = array(
                $Lang['eGuidance']['settings']['FineTune']['Tab']['CaseType'],
                "sen_case_type_index.php?clearCoo=1",
                $TagNow["CaseType"]
            );
            
            return $TAGS_OBJ;
        }

        public function checkDuplicateAdjustType($typeID, $chineseName, $englishName)
        {
            $sql = "SELECT TypeID FROM INTRANET_GUIDANCE_SETTING_ADJUST_TYPE WHERE 1";
            if ($typeID) {
                $sql .= " AND TypeID<>'" . $typeID . "'";
            }
            $sql .= " AND (ChineseName='" . $this->Get_Safe_Sql_Like_Query(intranet_htmlspecialchars($chineseName)) . "' OR";
            $sql .= " EnglishName='" . $this->Get_Safe_Sql_Like_Query(intranet_htmlspecialchars($englishName)) . "')";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getAdjustType($typeID)
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_SETTING_ADJUST_TYPE WHERE TypeID='" . $typeID . "'";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function checkDuplicateAdjustItem($itemID, $typeID, $chineseName, $englishName)
        {
            $sql = "SELECT ItemID FROM INTRANET_GUIDANCE_SETTING_ADJUST_ITEM WHERE 1";
            $sql .= " AND TypeID='" . $typeID . "'";
            if ($itemID) {
                $sql .= " AND ItemID<>'" . $itemID . "'";
            }
            $sql .= " AND (ChineseName='" . $this->Get_Safe_Sql_Like_Query(intranet_htmlspecialchars($chineseName)) . "' OR";
            $sql .= " EnglishName='" . $this->Get_Safe_Sql_Like_Query(intranet_htmlspecialchars($englishName)) . "')";
            $rs = $this->returnResultSet($sql);
            
            return $rs;
        }

        public function getAdjustItem($itemID)
        {
            $sql = "SELECT 
							t.ChineseName as TypeChineseName, 
							t.EnglishName as TypeEnglishName, 
							i.* 
					FROM 
							INTRANET_GUIDANCE_SETTING_ADJUST_ITEM i
					LEFT JOIN 
							INTRANET_GUIDANCE_SETTING_ADJUST_TYPE t on t.TypeID=i.TypeID
					WHERE i.ItemID='" . $itemID . "'";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function checkDuplicateServiceType($typeID, $chineseName, $englishName)
        {
            $sql = "SELECT TypeID FROM INTRANET_GUIDANCE_SETTING_SERVICE_TYPE WHERE 1";
            if ($typeID) {
                $sql .= " AND TypeID<>'" . $typeID . "'";
            }
            $sql .= " AND (ChineseName='" . $this->Get_Safe_Sql_Like_Query(intranet_htmlspecialchars($chineseName)) . "' OR";
            $sql .= " EnglishName='" . $this->Get_Safe_Sql_Like_Query(intranet_htmlspecialchars($englishName)) . "')";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getServiceType($typeID = '', $type = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_SETTING_SERVICE_TYPE WHERE 1";
            if ($typeID) {
                $sql .= " AND TypeID='" . $typeID . "'";
            }
            if ($type) {
                $sql .= " AND Type='" . $type . "'";
            }
            $sql .= " ORDER BY SeqNo";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSupportService($serviceID)
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE WHERE ServiceID='" . $serviceID . "'";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function checkDuplicateSupportService($serviceID, $typeID, $chineseName, $englishName)
        {
            $sql = "SELECT ServiceID FROM INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE WHERE 1";
            $sql .= " AND TypeID='" . $typeID . "'";
            if ($serviceID) {
                $sql .= " AND ServiceID<>'" . $serviceID . "'";
            }
            $sql .= " AND (ChineseName='" . $this->Get_Safe_Sql_Like_Query(intranet_htmlspecialchars($chineseName)) . "' OR";
            $sql .= " EnglishName='" . $this->Get_Safe_Sql_Like_Query(intranet_htmlspecialchars($englishName)) . "')";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function checkDuplicateSenCaseType($settingID, $chineseName, $englishName, $code)
        {
            $sql = "SELECT SettingID FROM INTRANET_GUIDANCE_SETTING_ITEM WHERE 1";
            $sql .= " AND Form='SENCase' AND Type='SENType'";
            if ($settingID) {
                $sql .= " AND SettingID<>'" . $settingID . "'";
            }
            $sql .= " AND (ChineseName='" . $this->Get_Safe_Sql_Like_Query($chineseName) . "' OR";
            $sql .= " EnglishName='" . $this->Get_Safe_Sql_Like_Query($englishName) . "' OR";
            $sql .= " Code='" . $this->Get_Safe_Sql_Like_Query($code) . "')";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }
        
        public function checkDuplicateSenCaseSubType($para=array())
        {
            $sql = "SELECT SettingID FROM INTRANET_GUIDANCE_SETTING_ITEM WHERE 1";
            $sql .= " AND Form='SENCase' AND Type='SENSubType'";
            if ($para['SubTypeID']) {
                $sql .= " AND SettingID<>'" . $para['SubTypeID']. "'";
            }
            $sql .= " AND (Code='" . $this->Get_Safe_Sql_Like_Query($para['Code']) . "' OR (";
            $sql .= "ParentSettingID='".$para['SettingID']."'";
            $sql .= " AND (ChineseName='" . $this->Get_Safe_Sql_Like_Query($para['ChineseName']) . "' OR";
            $sql .= " EnglishName='" . $this->Get_Safe_Sql_Like_Query($para['EnglishName']) . "')))";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }
        
        // check if an adjustment type / adjustment item has been used in SEN case or not
        public function isSENCaseUseAdjustment($typeID, $itemID = '')
        {
            $sql = "SELECT 
							a.StudentID
					FROM 
							INTRANET_GUIDANCE_SEN_CASE_ADJUSTMENT a
					INNER JOIN
							INTRANET_GUIDANCE_SETTING_ADJUST_ITEM i ON i.ItemID=a.ItemID
					INNER JOIN 
							INTRANET_GUIDANCE_SETTING_ADJUST_TYPE t on t.TypeID=i.TypeID
					WHERE 1";
            if ($typeID) {
                $sql .= " AND t.TypeID='" . $typeID . "'";
            }
            if ($itemID) {
                $sql .= " AND i.ItemID='" . $itemID . "'";
            }
            $sql .= " LIMIT 1";
            $rs = $this->returnResultSet($sql);
            
            return $rs;
        }

        // check if the case sub type has been used in SEN case or not
        public function isSENCaseUseThisCaseSubType($settingID)
        {
            $setting = $this->getGuidanceSettingItem($form = '', $type = '', $code = '', $settingID);
            if (count($setting)) {
                $codeFirst = '^:'.$setting[0]['Code'].'^';
                $codeOther = '^#'.$setting[0]['Code'].'^';
                
                $sql = "SELECT
                                c.StudentID
					    FROM
							    INTRANET_GUIDANCE_SEN_CASE c
                        WHERE   (LOCATE('".$codeFirst."', CONCAT(SENType,'^'))>0
                            OR  LOCATE('".$codeOther."', CONCAT(SENType,'^'))>0 ) LIMIT 1";
                $rs = $this->returnResultSet($sql);
                return count($rs) ? true : false;
            }
            return false;
        }
        
        // check if the case type has been used in SEN case or not
        public function isSENCaseUseThisCaseType($settingID)
        {
            $setting = $this->getGuidanceSettingItem($form = '', $type = '', $code = '', $settingID);
            if (count($setting)) {
                $code = '^~'.$setting[0]['Code'].'^';
                
                $sql = "SELECT 
                                c.StudentID
					    FROM
							    INTRANET_GUIDANCE_SEN_CASE c
                        WHERE   LOCATE('".$code."', CONCAT('^~',SENType,'^'))>0 LIMIT 1";
                $rs = $this->returnResultSet($sql);
                return count($rs) ? true : false;
            }
            return false;
        }

        // check if an support service has been used in SEN case or not
        public function isSENCaseUseSupportService($typeID, $serviceID = '')
        {
            $sql = "SELECT 
							a.StudentID
					FROM 
							INTRANET_GUIDANCE_SEN_CASE_SUPPORT a
					INNER JOIN
							INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE i ON i.ServiceID=a.ServiceID
					INNER JOIN 
							INTRANET_GUIDANCE_SETTING_SERVICE_TYPE t on t.TypeID=i.TypeID
					WHERE 1";
            if ($typeID) {
                $sql .= " AND t.TypeID='" . $typeID . "'";
            }
            if ($serviceID) {
                $sql .= " AND i.ServiceID='" . $serviceID . "'";
            }
            $sql .= " LIMIT 1";
            $rs = $this->returnResultSet($sql);
            
            return $rs;
        }

        public function getPersonalCaseByClass($className, $excludeStudentID = '')
        {
            $AcademicYearID = Get_Current_Academic_Year_ID();
            $sql = "SELECT 	c.StudentID 
					FROM 	INTRANET_GUIDANCE_PERSONAL c	
					INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=c.StudentID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $AcademicYearID . "'
					WHERE yc.ClassTitleEN='" . $className . "'";
            if ($excludeStudentID) {
                $sql .= " AND c.StudentID<>'" . $excludeStudentID . "'";
            }
            $rs = $this->returnVector($sql);
            return $rs;
        }

        public function getPersonal($studentID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_PERSONAL WHERE 1 ";
            if ($studentID) {
                $studentID = (array) $studentID;
                $studentIDSql = implode("','", $studentID);
                $sql .= " AND StudentID IN ('{$studentIDSql}')";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getStudentProblemReport($filter)
        {
            global $junior_mck;
            
            $cond = '';
            if ($filter['ReferralFromType']) {
                switch ($filter['ReferralFromType']) {
                    case 'Teacher':
                        if ($filter['FromTeacher']) {
                            $cond .= " AND p.ReferralFromType='Teacher^:" . $filter['FromTeacher'] . "'";
                        } else {
                            $cond .= " AND p.ReferralFromType LIKE 'Teacher^:%'";
                        }
                        if ($filter['FromTeacherReason']) {
                            $cond .= " AND p.ReferralFromReason LIKE '%" . $this->Get_Safe_Sql_Like_Query(trim($filter['FromTeacherReason'])) . "%'";
                        }
                        break;
                    
                    case 'AcademicGroup':
                        $cond .= " AND p.ReferralFromType='AcademicGroup'";
                        if ($filter['RefAcaReason']) {
                            foreach ((array) $filter['RefAcaReason'] as $reason) {
                                if ($reason == 'AcaOther') {
                                    $cond .= " AND CONCAT('^~',p.ReferralFromReason) LIKE '%^~AcaOther^:%'";
                                    if ($filter['AcaOther']) {
                                        $cond .= " AND p.ReferralFromReason LIKE '%^:" . $this->Get_Safe_Sql_Like_Query(trim($filter['AcaOther'])) . "%'";
                                    }
                                } else {
                                    $cond .= " AND CONCAT('^~',p.ReferralFromReason,'^~') LIKE '%^~" . $reason . "^~%'";
                                }
                            }
                        }
                        break;
                    
                    case 'DisciplineGroup':
                        $cond .= " AND p.ReferralFromType='DisciplineGroup'";
                        if ($filter['RefDisReason']) {
                            foreach ((array) $filter['RefDisReason'] as $reason) {
                                if ($reason == 'DisOther') {
                                    $cond .= " AND CONCAT('^~',p.ReferralFromReason) LIKE '%^~DisOther^:%'";
                                    if ($filter['DisOther']) {
                                        $cond .= " AND p.ReferralFromReason LIKE '%^:" . $this->Get_Safe_Sql_Like_Query(trim($filter['DisOther'])) . "%'";
                                    }
                                } else {
                                    $cond .= " AND CONCAT('^~',p.ReferralFromReason,'^~') LIKE '%^~" . $reason . "^~%'";
                                }
                            }
                        }
                        break;
                    
                    case 'FromOther':
                        $cond .= " AND p.ReferralFromType='FromOther'";
                        if ($filter['FromOther']) {
                            $cond .= " AND p.ReferralFromReason LIKE '%" . $this->Get_Safe_Sql_Like_Query(trim($filter['FromOther'])) . "%'";
                        }
                        break;
                }
            }
            
            if ($filter['RadioPlaceOfBirth']) {
                if ($filter['RadioPlaceOfBirth'] == 'Hong Kong') {
                    $cond .= " AND p.PlaceOfBirth='" . $filter['RadioPlaceOfBirth'] . "'";
                } else {
                    if ($filter['PlaceOfBirth']) {
                        $cond .= " AND p.PlaceOfBirth='" . $this->Get_Safe_Sql_Query(trim($filter['PlaceOfBirth'])) . "'";
                    }
                }
            }
            
            if ($filter['YearInHongKong']) {
                $cond .= " AND p.YearInHongKong='" . $this->Get_Safe_Sql_Query($filter['YearInHongKong']) . "'";
            }
            
            if ($filter['ParentStatus']) {
                $status = $filter['ParentStatus'];
                if ($status == 'PSOther') {
                    $cond .= " AND p.ParentStatus LIKE '%$status^:%'";
                    if ($filter['ParentStatusOther']) {
                        $cond .= " AND p.ParentStatus LIKE '%^:" . $this->Get_Safe_Sql_Like_Query(trim($filter['ParentStatusOther'])) . "%'";
                    }
                } else {
                    $cond .= " AND p.ParentStatus='" . $status . "'";
                }
            }
            
            if ($filter['TotalNbrBrotherSister']) {
                $cond .= " AND (p.NbrElderBrother+p.NbrYoungerBrother+p.NbrElderSister+p.NbrYoungerSister)='" . $this->Get_Safe_Sql_Query($filter['TotalNbrBrotherSister']) . "'";
            }
            
            if ($filter['Prob_Health']) {
                foreach ((array) $filter['Prob_Health'] as $prob) {
                    if ($prob == 'Illness') {
                        $cond .= " AND CONCAT('^~',p.ProbHealth) LIKE '%^~Illness^:%'";
                        if ($filter['Illness']) {
                            $cond .= " AND p.ProbHealth LIKE '%^:" . $this->Get_Safe_Sql_Like_Query(trim($filter['Illness'])) . "%'";
                        }
                    } else {
                        $cond .= " AND CONCAT('^~',p.ProbHealth,'^~') LIKE '%^~" . $prob . "^~%'";
                    }
                }
            }
            
            if ($filter['Prob_Study']) {
                foreach ((array) $filter['Prob_Study'] as $prob) {
                    $cond .= " AND CONCAT('^~',p.ProbStudy,'^~') LIKE '%^~" . $prob . "^~%'";
                }
            }
            
            if ($filter['Prob_Peer']) {
                foreach ((array) $filter['Prob_Peer'] as $prob) {
                    $cond .= " AND CONCAT('^~',p.ProbPeer,'^~') LIKE '%^~" . $prob . "^~%'";
                }
            }
            
            if ($filter['Prob_Grow']) {
                foreach ((array) $filter['Prob_Grow'] as $prob) {
                    $cond .= " AND CONCAT('^~',p.ProbGrow,'^~') LIKE '%^~" . $prob . "^~%'";
                }
            }
            
            if ($filter['Prob_Emotion']) {
                foreach ((array) $filter['Prob_Emotion'] as $prob) {
                    $cond .= " AND CONCAT('^~',p.ProbEmotion,'^~') LIKE '%^~" . $prob . "^~%'";
                }
            }
            
            if ($filter['Prob_Sex']) {
                foreach ((array) $filter['Prob_Sex'] as $prob) {
                    $cond .= " AND CONCAT('^~',p.ProbSex,'^~') LIKE '%^~" . $prob . "^~%'";
                }
            }
            
            if ($filter['Prob_Behavior']) {
                foreach ((array) $filter['Prob_Behavior'] as $prob) {
                    if ($prob == 'CriminalOffense') {
                        $cond .= " AND CONCAT('^~',p.ProbBehavior) LIKE '%^~CriminalOffense^:%'";
                        if ($filter['CriminalOffense']) {
                            $cond .= " AND CONCAT(p.ProbBehavior,'^~') LIKE '%^:" . $this->Get_Safe_Sql_Like_Query(trim($filter['CriminalOffense'])) . "^~%'";
                        }
                    } else {
                        $cond .= " AND CONCAT('^~',p.ProbBehavior,'^~') LIKE '%^~" . $prob . "^~%'";
                    }
                }
            }
            
            if ($filter['Prob_Family']) {
                foreach ((array) $filter['Prob_Family'] as $prob) {
                    $cond .= " AND CONCAT('^~',p.ProbFamily,'^~') LIKE '%^~" . $prob . "^~%'";
                }
            }
            
            if ($filter['Prob_Other']) {
                $cond .= " AND p.ProbOther LIKE '" . $filter['Prob_Other'][0] . "%'";
                if ($filter['OtherProblem']) {
                    $cond .= " AND p.ProbOther LIKE '%^:" . trim($filter['OtherProblem']) . "%'";
                }
            }
            
            if ($filter['IsReferralToSocialWorker']) {
                $cond .= " AND p.IsReferralToSocialWorker='" . $filter['IsReferralToSocialWorker'] . "'";
            }
            
            if ($filter['ReferralToReason']) {
                foreach ((array) $filter['ReferralToReason'] as $reason) {
                    if ($reason == 'RefToOther') {
                        $cond .= " AND CONCAT('^~',p.ReferralToReason) LIKE '%^~RefToOther^:%'";
                        if ($filter['RefToOther']) {
                            $cond .= " AND p.ReferralToReason LIKE '%^:" . $this->Get_Safe_Sql_Like_Query($filter['RefToOther']) . "%'";
                        }
                    } else {
                        $cond .= " AND CONCAT('^~',p.ReferralToReason,'^~') LIKE '%^~" . $reason . "^~%'";
                    }
                }
            }
            
            $rs = array();
            $StudentNameField = getNameFieldByLang('u.');
            
            if ($junior_mck) {
                $sql = "SELECT 		u.UserID,
									u.WebSAMSRegNo,
									" . $StudentNameField . " as StudentName,
									u.ClassName,
									u.ClassNumber,
									p.ProbHealth,
									p.ProbStudy,
									p.ProbPeer,
									p.ProbGrow,
									p.ProbEmotion,
									p.ProbSex,
									p.ProbBehavior,
									p.ProbFamily,
									p.ProbOther
						FROM 
									INTRANET_USER u
 						INNER JOIN
									INTRANET_GUIDANCE_PERSONAL p ON p.StudentID=u.UserID
						WHERE		1
									{$cond}	
						ORDER BY u.WebSAMSRegNo";
                $rs = $this->returnResultSet($sql);
            } else {
                $ClassNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
                $AcademicYearID = Get_Current_Academic_Year_ID();
                $sql = "SELECT 		u.UserID,
									u.WebSAMSRegNo,
									" . $StudentNameField . " as StudentName,
									" . $ClassNameField . " as ClassName,
									ycu.ClassNumber,
									p.ProbHealth,
									p.ProbStudy,
									p.ProbPeer,
									p.ProbGrow,
									p.ProbEmotion,
									p.ProbSex,
									p.ProbBehavior,
									p.ProbFamily,
									p.ProbOther
						FROM
									INTRANET_USER u
						INNER JOIN 
									YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
						INNER JOIN 
									YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $AcademicYearID . "'
						INNER JOIN
									INTRANET_GUIDANCE_PERSONAL p ON p.StudentID=u.UserID
						WHERE		1
									{$cond}	
						ORDER BY u.WebSAMSRegNo";
                $rs = $this->returnResultSet($sql);
            }
            return $rs;
        }

        public function getSelfImprove($improveID = '')
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_SELFIMPROVE WHERE 1 ";
            if ($improveID) {
                $improveID = (array) $improveID;
                $improveIDSql = implode("','", $improveID);
                $sql .= " AND ImproveID IN ('{$improveIDSql}')";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSelfImproveByStudentId($studentID = '')
        {
            $studentID = (array) $studentID;
            $studentIdSql = implode("','", $studentID);
            $sql = "SELECT * FROM INTRANET_GUIDANCE_SELFIMPROVE WHERE 1 ";
            if ($studentIdSql) {
                $sql .= " AND StudentID IN ('{$studentIdSql}')";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSelfImproveTeacher($improveID)
        {
            if (is_array($improveID)) {
                $improveIDSql = implode("','", $improveID);
                $cond = " OR ImproveID IN ('{$improveIDSql}')";
                
                return $this->getSelectedTeacher('INTRANET_GUIDANCE_SELFIMPROVE_TEACHER', 'ImproveID', '-1', $cond);
            }
            return $this->getSelectedTeacher('INTRANET_GUIDANCE_SELFIMPROVE_TEACHER', 'ImproveID', $improveID);
        }

        public function getSelfImproveByTeacherId($teacherID = array())
        {
            $teacherID = (array) $teacherID;
            $teacherIDSql = implode("','", $teacherID);
            
            $sql = "SELECT 
			    IGS.*,
			    IGST.TeacherID
		    FROM 
			    INTRANET_GUIDANCE_SELFIMPROVE IGS
		    INNER JOIN
			    INTRANET_GUIDANCE_SELFIMPROVE_TEACHER IGST
		    ON
			    IGS.ImproveID = IGST.ImproveID
			AND
			    IGST.TeacherID IN ('{$teacherIDSql}')
		    ";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        // #### start class teacher comments
        public function getClassTeacherMeeting($meetingID)
        {
            $sql = "SELECT * FROM INTRANET_GUIDANCE_CLASSTEACHER_MEETING WHERE 1 ";
            if ($meetingID) {
                $sql .= " AND MeetingID='" . $meetingID . "'";
            }
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getClassTeacherMeetingID($para)
        {
            $sql = "SELECT MeetingID FROM INTRANET_GUIDANCE_CLASSTEACHER_MEETING WHERE 1 ";
            if ($para['MeetingDate']) {
                $sql .= " AND MeetingDate='" . $para['MeetingDate'] . "'";
            }
            if ($para['YearClassID']) { // ip
                $sql .= " AND YearClassID='" . $para['YearClassID'] . "'";
            }
            if ($para['ClassName']) { // ej
                $sql .= " AND ClassName='" . $para['ClassName'] . "'";
            }
            if ($para['YearName']) { // ej
                $sql .= " AND YearName='" . $para['YearName'] . "'";
            }
            
            $rs = $this->returnResultSet($sql);
            return count($rs) ? $rs[0]['MeetingID'] : '';
        }

        public function getClassTeacherMeetingStudent()
        {
            $sql = "SELECT 	
								m.MeetingDate,
 								c.StudentID
					FROM 
								INTRANET_GUIDANCE_CLASSTEACHER_MEETING m
					INNER JOIN
								INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT c ON c.MeetingID=m.MeetingID
					ORDER BY 	m.MeetingDate, c.StudentID";
            
            $rs = $this->returnResultSet($sql);
            $ret = count($rs) ? BuildMultiKeyAssoc($rs, array(
                'MeetingDate',
                'StudentID'
            ), array(
                'StudentID'
            ), 1) : '';
            return $ret;
        }

        public function isClassTeacherCommentExist($meetingID, $studentID)
        {
            $sql = "SELECT Comment FROM INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT WHERE MeetingID='" . $meetingID . "' AND StudentID='" . $studentID . "'";
            $rs = $this->returnResultSet($sql);
            return count($rs) ? true : false;
        }

        // $yearClassID is used for ip, $yearName and $className are for ej
        public function getClassTeacherComments($meetingID, $yearClassID = '', $yearName = '', $className = '')
        {
            global $junior_mck, $intranet_root;
            
            if ($junior_mck) {
                include_once ($intranet_root . "/includes/form_class_manage.php");
                $libFCM = new form_class_manage();
                $yearInfoArr = $libFCM->Get_Academic_Year_List(Get_Current_Academic_Year_ID());
                $yearInfoArr = current($yearInfoArr);
                $currentYearName = $yearInfoArr['YearNameEN'];
                
                if ($yearName == $currentYearName) { // current year
                    $join = "";
                    $cond = " AND u.ClassName='" . $this->Get_Safe_Sql_Query($className) . "'";
                    $class_number_field = "u.ClassNumber";
                    $orderBy = "CAST(u.ClassNumber AS UNSIGNED)+0";
                } else {
                    $join = " INNER JOIN PROFILE_CLASS_HISTORY h ON h.UserID=u.UserID AND (h.AcademicYear='" . $this->Get_Safe_Sql_Query($yearName) . "' OR LEFT(h.AcademicYear,4)='" . $this->Get_Safe_Sql_Query(substr($yearName, 0, 4)) . "') AND h.ClassName='" . $this->Get_Safe_Sql_Query($className) . "'";
                    $cond = "";
                    $class_number_field = "h.ClassNumber";
                    $orderBy = "CAST(h.ClassNumber AS UNSIGNED)+0";
                }
                $username_field = getNameFieldByLang("u.", $isTitleDisabled = true);
                $sql = "SELECT 	
									u.UserID as StudentID,
									u.WebSAMSRegNo,
									{$class_number_field} as ClassNumber,
									{$username_field} as StudentName,
									cmc.Comment,
									cmc.Followup
						FROM
									INTRANET_USER u
									{$join}
						LEFT JOIN 
									INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT cmc ON cmc.StudentID=u.UserID AND cmc.MeetingID='" . $meetingID . "' 
						WHERE	
									u.RecordType=2
						AND 
									u.RecordStatus IN ('0','1','2')
									{$cond}
						ORDER BY {$orderBy}";
            } else {
                $username_field = getNameFieldByLang("u.", $displayLang = "", $isTitleDisabled = true);
                $sql = "SELECT 	
									u.UserID as StudentID,
									u.WebSAMSRegNo,
									ycu.ClassNumber,
									{$username_field} as StudentName,
									cmc.Comment,
									cmc.Followup
						FROM
									INTRANET_USER u
						INNER JOIN 
									YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
						INNER JOIN 
									YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
						LEFT JOIN 
									INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT cmc ON cmc.StudentID=u.UserID AND cmc.MeetingID='" . $meetingID . "' 
						WHERE	
									yc.YearClassID='" . $yearClassID . "' 
						AND 
									u.RecordType=2
						AND 
									u.RecordStatus IN ('0','1','2')
						ORDER BY ycu.ClassNumber+0";
            }
            
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        /*
         * return list of academic year id by class name.
         * e.g. 5B, if current year is 2017, then the academic year list should be array(2017,2016,2015,2014,2013)
         */
        public function getAcademicYearByClassName($className)
        {
            global $junior_mck, $intranet_root;
            include_once ($intranet_root . "/includes/form_class_manage.php");
            
            $CurrentAcademicYearID = Get_Current_Academic_Year_ID();
            $prevAcademicYearID = $CurrentAcademicYearID; // initial: previous=current
            $academicYearIDList = array();
            $academicYearIDList[] = $CurrentAcademicYearID;
            $formLevel = 0;
            $ret = array();
            if ($junior_mck) {
                $sql = "SELECT 		
									v.WebSAMSLevel
						FROM
									INTRANET_CLASSLEVEL v
						INNER JOIN 
									INTRANET_CLASS c ON c.ClassLevelID=v.ClassLevelID
						WHERE	
									c.ClassName='" . $this->Get_Safe_Sql_Query($className) . "'";
                $rs = $this->returnResultSet($sql);
                
                if (count($rs) == 1) {
                    $webSAMSLevel = $rs[0]['WebSAMSLevel'];
                    if (preg_match('/^P[0-9]{1,2}/', $webSAMSLevel)) {
                        $formLevel = substr($webSAMSLevel, 1);
                        if ($formLevel > 1) {
                            $fcm = new form_class_manage();
                            for ($i = 1; $i < $formLevel; $i ++) {
                                $prevAcademicYearID = $fcm->Get_Previous_Academic_Year($prevAcademicYearID);
                                $academicYearIDList[] = $prevAcademicYearID;
                            }
                        } else {
                            $academicYearIDList[] = $prevAcademicYearID; // current academic year id only
                        }
                    }
                }
                $ay = new academic_year();
                $ret = $ay->Get_All_Year_List("", 0, array(), $academicYearIDList);
                $ret = BuildMultiKeyAssoc($ret, 'AcademicYearID', array(
                    'AcademicYearName'
                ), $SingleValue = 1);
            } else {
                $sql = "SELECT 		
									y.WEBSAMSCode
						FROM
									YEAR y
						INNER JOIN 
									YEAR_CLASS yc ON yc.YearID=y.YearID
						WHERE	
									yc.ClassTitleEN='" . $this->Get_Safe_Sql_Query($className) . "'
						AND 		yc.AcademicYearID='" . $CurrentAcademicYearID . "'";
                $rs = $this->returnResultSet($sql);
                
                if (count($rs) == 1) {
                    $webSAMSCode = $rs[0]['WEBSAMSCode'];
                    if (preg_match('/^S[0-9]{1,2}/', $webSAMSCode)) {
                        $formLevel = substr($webSAMSCode, 1);
                        if ($formLevel > 1) {
                            $fcm = new form_class_manage();
                            for ($i = 1; $i < $formLevel; $i ++) {
                                $prevAcademicYearID = $fcm->Get_Previous_Academic_Year($prevAcademicYearID);
                                $academicYearIDList[] = $prevAcademicYearID;
                            }
                        } else {
                            $academicYearIDList[] = $prevAcademicYearID; // current academic year id only
                        }
                    }
                }
                
                $ay = new academic_year();
                $ret = $ay->Get_All_Year_List("", 0, array(), $academicYearIDList);
                $ret = BuildMultiKeyAssoc($ret, 'AcademicYearID', array(
                    'AcademicYearName'
                ), $SingleValue = 1);
            }
            
            return $ret;
        }

        // for ej only
        public function getPastClassList($AcademicYear)
        {
            $sql = "SELECT DISTINCT ClassName FROM PROFILE_CLASS_HISTORY WHERE 1 ";
            if ($AcademicYear) {
                $sql .= " AND (AcademicYear='" . $this->Get_Safe_Sql_Query($AcademicYear) . "' OR LEFT(AcademicYear,4)='" . $this->Get_Safe_Sql_Query(substr($AcademicYear, 0, 4)) . "')";
            }
            $sql .= " ORDER BY ClassName";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        // for ej only
        public function getAcademicYearList()
        {
            $sql = "SELECT YearNameEN AS YearName FROM ACADEMIC_YEAR ORDER BY YearNameEN DESC";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getAcademicYearIDByName($yearName)
        {
            $sql = "SELECT AcademicYearID FROM ACADEMIC_YEAR WHERE YearNameEN='" . $this->Get_Safe_Sql_Query($yearName) . "'";
            $rs = $this->returnResultSet($sql);
            return count($rs) ? $rs[0]['AcademicYearID'] : '';
        }

        public function getClassTeacherReport($className, $academicYearIDAry)
        {
            global $junior_mck, $intranet_root;
            include_once ($intranet_root . "/includes/form_class_manage.php");
            include_once ($intranet_root . "/includes/libclass.php");
            
            $StudentNameField = getNameFieldByLang('u.');
            
            $rs = array();
            $lclass = new libclass();
            $libFCM = new form_class_manage();
            $studentIDAry = $lclass->getStudentIDByClassName(array(
                $className
            ));
            if (count($studentIDAry) && count($academicYearIDAry)) {
                $sql = array();
                if ($junior_mck) {
                    $currentAcademicYearID = Get_Current_Academic_Year_ID();
                    foreach ($academicYearIDAry as $yid) {
                        $YearInfoArr = $libFCM->Get_Academic_Year_List($yid);
                        $AcademicYearStart = $YearInfoArr[0]['AcademicYearStart'];
                        $AcademicYearEnd = $YearInfoArr[0]['AcademicYearEnd'];
                        $YearNameEN = $YearInfoArr[0]['YearNameEN'];
                        
                        if ($yid == $currentAcademicYearID) {
                            $join = "";
                            $ClassNumberField = "u.ClassNumber";
                            $ClassNameField = "u.ClassName";
                        } else {
                            $join = " INNER JOIN 
											PROFILE_CLASS_HISTORY p ON p.UserID=u.UserID AND (p.AcademicYear='" . $this->Get_Safe_Sql_Query($YearNameEN) . "' OR LEFT(p.AcademicYear,4)='" . $this->Get_Safe_Sql_Query(substr($YearNameEN, 0, 4)) . "')
									  INNER JOIN
											ACADEMIC_YEAR ay ON ay.YearNameEN=p.AcademicYear AND ay.AcademicYearID='" . $yid . "'";
                            $ClassNumberField = "p.ClassNumber";
                            $ClassNameField = "p.ClassName";
                        }
                        
                        $sql[] = "SELECT 	u.UserID,
											u.WebSAMSRegNo,
											" . $StudentNameField . " as StudentName,
											" . $ClassNameField . " as ClassName,
											" . $ClassNumberField . " as ClassNumber,
											IF(cm.MeetingDate is null or cm.MeetingDate='0000-00-00','-',cm.MeetingDate) as MeetingDate,
											cmc.Comment,
											cmc.Followup
								FROM
											INTRANET_USER u
											{$join}
								INNER JOIN
											INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT cmc ON cmc.StudentID=u.UserID
								INNER JOIN
											INTRANET_GUIDANCE_CLASSTEACHER_MEETING cm ON cm.MeetingID=cmc.MeetingID
								WHERE	
											u.UserID IN ('" . implode("','", $studentIDAry) . "')
								AND 		cm.MeetingDate BETWEEN '" . $AcademicYearStart . "' AND '" . $AcademicYearEnd . "'
								AND			cmc.Comment IS NOT NULL and cmc.Comment<>''";
                    }
                    $SQL = implode(' UNION ', $sql) . " ORDER BY WebSAMSRegNo, MeetingDate DESC";
                } else {
                    $ClassNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
                    
                    foreach ($academicYearIDAry as $yid) {
                        $YearInfoArr = $libFCM->Get_Academic_Year_List($yid);
                        $AcademicYearStart = $YearInfoArr[0]['AcademicYearStart'];
                        $AcademicYearEnd = $YearInfoArr[0]['AcademicYearEnd'];
                        
                        $sql[] = "SELECT 	u.UserID,
											u.WebSAMSRegNo,
											" . $StudentNameField . " as StudentName,
											" . $ClassNameField . " as ClassName,
											ycu.ClassNumber,
											IF(cm.MeetingDate is null or cm.MeetingDate='0000-00-00','-',cm.MeetingDate) as MeetingDate,
											cmc.Comment,
											cmc.Followup
								FROM
											INTRANET_USER u
								INNER JOIN 
											YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
								INNER JOIN 
											YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
								INNER JOIN
											INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT cmc ON cmc.StudentID=u.UserID
								INNER JOIN
											INTRANET_GUIDANCE_CLASSTEACHER_MEETING cm ON cm.MeetingID=cmc.MeetingID
								WHERE	
											u.UserID IN ('" . implode("','", $studentIDAry) . "')
								AND 		
											yc.AcademicYearID='" . $yid . "'
								AND 		cm.MeetingDate BETWEEN '" . $AcademicYearStart . "' AND '" . $AcademicYearEnd . "'
								AND			cmc.Comment IS NOT NULL and cmc.Comment<>''";
                    }
                    $SQL = implode(' UNION ', $sql) . " ORDER BY WebSAMSRegNo, MeetingDate DESC";
                }
                
                $rs = $this->returnResultSet($SQL);
                // debug_pr($SQL);
                // debug_pr($rs);
            }
            
            return $rs;
        }

        public function doClassTeacherImport()
        {
            global $junior_mck;
            
            $curentAcademicYearID = Get_Current_Academic_Year_ID();
            $sql = "SELECT DISTINCT MeetingDate FROM INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT_IMPORT WHERE LastModifiedBy='" . $_SESSION['UserID'] . "' ORDER BY MeetingDate";
            $date_rs = $this->returnResultSet($sql);
            $nrDates = count($date_rs);
            
            $result = array();
            $meeting_comment_insert = array();
            $check_meeting = array();
            
            if ($nrDates) {
                $this->Start_Trans();
                
                if ($junior_mck) {
                    for ($i = 0; $i < $nrDates; $i ++) {
                        $meetingDate = $date_rs[$i]['MeetingDate'];
                        $yearInfo = getAcademicYearInfoAndTermInfoByDate($meetingDate);
                        $academicYearID = $yearInfo[0];
                        $yearNameEN = $yearInfo[1]; // get YearNameEN
                        
                        if ($academicYearID == $curentAcademicYearID) {
                            $sql = "SELECT 
												i.StudentID,
												i.Comment,
												i.Followup,
												u.ClassName
									FROM
												INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT_IMPORT i
									INNER JOIN
												INTRANET_USER u ON u.UserID=i.StudentID
									WHERE
												i.MeetingDate='" . $meetingDate . "'
									AND			
												i.LastModifiedBy='" . $_SESSION['UserID'] . "'
									ORDER BY 	i.StudentID";
                        } else {
                            $sql = "SELECT 
												i.StudentID,
												i.Comment,
												i.Followup,
												h.ClassName
									FROM
												INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT_IMPORT i
									INNER JOIN
												INTRANET_USER u ON u.UserID=i.StudentID
									INNER JOIN
												PROFILE_CLASS_HISTORY h ON h.UserID=u.UserID 
											AND (h.AcademicYear='" . $this->Get_Safe_Sql_Query($yearNameEN) . "' 
												OR LEFT(h.AcademicYear,4)='" . $this->Get_Safe_Sql_Query(substr($yearNameEN, 0, 4)) . "')
									WHERE
												i.MeetingDate='" . $meetingDate . "'
									AND			
												i.LastModifiedBy='" . $_SESSION['UserID'] . "'
									ORDER BY 	i.StudentID";
                        }
                        
                        $meeting_rs = $this->returnResultSet($sql);
                        
                        if (count($meeting_rs)) {
                            foreach ((array) $meeting_rs as $rs) {
                                $className = $rs['ClassName'];
                                if (! isset($check_meeting[$meetingDate][$yearNameEN][$className])) {
                                    $para = array();
                                    $para['MeetingDate'] = $meetingDate;
                                    $para['YearName'] = $yearNameEN;
                                    $para['ClassName'] = $className;
                                    $meetingID = $this->getClassTeacherMeetingID($para);
                                    unset($para);
                                    
                                    if (empty($meetingID)) {
                                        $dataAry = array();
                                        $dataAry['MeetingDate'] = $meetingDate;
                                        $dataAry['YearName'] = $yearNameEN;
                                        $dataAry['ClassName'] = $className;
                                        $sql = $this->INSERT2TABLE('INTRANET_GUIDANCE_CLASSTEACHER_MEETING', $dataAry, array(), false);
                                        $res = $this->db_db_query($sql);
                                        $result[] = $res;
                                        if ($res) {
                                            $meetingID = $this->db_insert_id(); // meeting id
                                        }
                                        unset($dataAry);
                                    }
                                    $check_meeting[$meetingDate][$yearNameEN][$className] = $meetingID;
                                }
                                
                                if ($check_meeting[$meetingDate][$yearNameEN][$className]) {
                                    $meeting_comment_insert[] = " ('" . $this->Get_Safe_Sql_Query($check_meeting[$meetingDate][$yearNameEN][$className]) . "', '" . $rs['StudentID'] . "', '" . $this->Get_Safe_Sql_Query($rs['Comment']) . "', '" . $this->Get_Safe_Sql_Query($rs['Followup']) . "') ";
                                }
                            }
                        }
                    } // loop $nrDates
                } // end ej
else { // ip
                    for ($i = 0; $i < $nrDates; $i ++) {
                        $meetingDate = $date_rs[$i]['MeetingDate'];
                        $yearInfo = getAcademicYearInfoAndTermInfoByDate($meetingDate);
                        $academicYearID = $yearInfo[0];
                        
                        $sql = "SELECT 		i.StudentID,
											i.Comment,
											i.Followup,
											yc.YearClassID
								FROM
											INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT_IMPORT i
								INNER JOIN 
											YEAR_CLASS_USER ycu ON ycu.UserID=i.StudentID
								INNER JOIN 
											YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $academicYearID . "'
								WHERE
											i.MeetingDate='" . $meetingDate . "'
								AND			
											i.LastModifiedBy='" . $_SESSION['UserID'] . "'
								ORDER BY 	yc.YearClassID, i.StudentID";
                        
                        $meeting_rs = $this->returnResultSet($sql);
                        if (count($meeting_rs)) {
                            foreach ((array) $meeting_rs as $rs) {
                                $yearClassID = $rs['YearClassID'];
                                if (! isset($check_meeting[$meetingDate][$yearClassID])) {
                                    $para = array();
                                    $para['MeetingDate'] = $meetingDate;
                                    $para['YearClassID'] = $yearClassID;
                                    $meetingID = $this->getClassTeacherMeetingID($para);
                                    unset($para);
                                    
                                    if (empty($meetingID)) {
                                        $dataAry = array();
                                        $dataAry['MeetingDate'] = $meetingDate;
                                        $dataAry['YearClassID'] = $yearClassID;
                                        $sql = $this->INSERT2TABLE('INTRANET_GUIDANCE_CLASSTEACHER_MEETING', $dataAry, array(), false);
                                        $res = $this->db_db_query($sql);
                                        $result[] = $res;
                                        if ($res) {
                                            $meetingID = $this->db_insert_id(); // meeting id
                                        }
                                        unset($dataAry);
                                    }
                                    $check_meeting[$meetingDate][$yearClassID] = $meetingID;
                                }
                                
                                if ($check_meeting[$meetingDate][$yearClassID]) {
                                    $meeting_comment_insert[] = " ('" . $this->Get_Safe_Sql_Query($check_meeting[$meetingDate][$yearClassID]) . "', '" . $rs['StudentID'] . "', '" . $this->Get_Safe_Sql_Query($rs['Comment']) . "', '" . $this->Get_Safe_Sql_Query($rs['Followup']) . "') ";
                                }
                            }
                        }
                    }
                } // end ip
                
                $nrRec = count($meeting_comment_insert);
                
                if ($nrRec) {
                    $sql = "INSERT INTO INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT (MeetingID, StudentID, Comment, Followup) VALUES " . implode(', ', (array) $meeting_comment_insert);
                    $result[] = $this->db_db_query($sql);
                }
                unset($date_rs);
                
                if (! in_array(false, $result)) {
                    $this->Commit_Trans();
                    return $nrRec;
                } else {
                    $this->RollBack_Trans();
                    return false;
                }
            } // end $nrDates
else {
                return false;
            }
        }

        // get user academic year history
        // $academicYearName is passed from ej, $academicYearID is passed from ip
        public function getUserAcademicYearHistory($academicYearName = '', $academicYearID = '')
        {
            global $junior_mck;
            
            $user_rs = array();
            if ($junior_mck) {
                $academicYearPart = array();
                if (is_array($academicYearName)) {
                    foreach ((array) $academicYearName as $i => $y) {
                        $academicYearName[$i] = $this->Get_Safe_Sql_Query($y);
                        $academicYearPart[$i] = $this->Get_Safe_Sql_Query(substr($y, 0, 4));
                    }
                }
                
                $currentAcademicYearName = getCurrentAcademicYear(); // YearNameEN
                
                if (is_array($academicYearName) && is_array($academicYearPart)) {
                    $sql = "SELECT		
										h.UserID,
										h.AcademicYear 
							FROM
										PROFILE_CLASS_HISTORY h 
							WHERE		(h.AcademicYear IN ('" . implode("','", array_unique((array) $academicYearName)) . "') 
										OR LEFT(h.AcademicYear,4) IN ('" . implode("','", array_unique((array) $academicYearPart)) . "'))
							UNION
							SELECT
										u.UserID,
										{$currentAcademicYearName} as AcademicYear
							FROM
										INTRANET_USER u
							WHERE
										u.RecordType=" . USERTYPE_STUDENT . "
							ORDER BY 	UserID, AcademicYear";
                    $user_rs = $this->returnResultSet($sql);
                }
            } else {
                if ($academicYearID != '') {
                    $sql = "SELECT 		
										ycu.UserID,
										yc.AcademicYearID as AcademicYear		
							FROM
										YEAR_CLASS_USER ycu 
							INNER JOIN 
										YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID  
							WHERE
										yc.AcademicYearID IN ('" . implode("','", array_unique((array) $academicYearID)) . "')
							ORDER BY 	ycu.UserID, yc.AcademicYearID";
                    $user_rs = $this->returnResultSet($sql);
                }
            }
            
            return $user_rs;
        }

        // #### end class teacher comments
        
        // get all group category for user group and the preset Admin Group that contain staff only
        public function getGroupCategoryList($staffOnly = true)
        {
            $ret = array();
            $sql = "SELECT 
								c.GroupCategoryID, 
								c.CategoryName 
					FROM 
								INTRANET_GROUP_CATEGORY c
					WHERE  		c.GroupCategoryID NOT IN (0,2,3,4,5,6)
					ORDER BY c.CategoryName";
            $rs = $this->returnResultSet($sql);
            if ($staffOnly) {
                foreach ((array) $rs as $r) {
                    $sql = "SELECT 
										g.GroupID
							FROM 
		 								INTRANET_GROUP g 
		 					INNER JOIN 
										INTRANET_USERGROUP ug ON ug.GroupID=g.GroupID 
							INNER JOIN	
										INTRANET_USER u ON u.UserID=ug.UserID
		 					WHERE
										g.RecordType='" . $r['GroupCategoryID'] . "'
							AND			u.RecordType<>1
							AND			u.RecordStatus=1
							LIMIT 1";
                    $check_RS = $this->returnResultSet($sql);
                    if (count($check_RS) == 0) { // all User in the group are staff
                        $ret[] = $r;
                    }
                }
            }
            
            return $ret;
        }

        public function getGroupList($groupCategoryID)
        {
            $ret = array();
            $academicYearID = Get_Current_Academic_Year_ID();
            $sql = "SELECT GroupID, Title, TitleChinese FROM INTRANET_GROUP where RecordType='$groupCategoryID' AND AcademicYearID='" . $academicYearID . "' ORDER BY Title"; // order by English Title
            $rs = $this->returnResultSet($sql);
            foreach ((array) $rs as $k => $v) {
                $name = Get_Lang_Selection($v['TitleChinese'], $v['Title']);
                $ret[$v['GroupID']] = $name;
            }
            return $ret;
        }

        public function getGroupMember($groupID, $cond)
        {
            $name_field = getNameFieldByLang('u.');
            $sql = "SELECT
								u.UserID,
								$name_field AS Name
					FROM
								INTRANET_USERGROUP ug
					INNER JOIN
								INTRANET_USER u ON u.UserID = ug.UserID
					WHERE
								ug.GroupID = '" . $groupID . "'
					AND			u.RecordStatus = 1
					{$cond}
					GROUP BY	u.UserID
					ORDER BY	u.EnglishName
					";
            $rs = $this->returnResultSet($sql);
            
            return $rs;
        }

        public function getGuidanceSettingItem($form = '', $type = '', $code = '', $settingID = '')
        {
            $name = Get_Lang_Selection('ChineseName', 'EnglishName');
            
            $sql = "SELECT  Type,
							Code,
							{$name} AS Name
					FROM 
							INTRANET_GUIDANCE_SETTING_ITEM 
					WHERE 1";
            if ($form) {
                $sql .= " AND `Form`='" . $form . "'";
            }
            if ($type) {
                $sql .= " AND `Type`='" . $type . "'";
            }
            if ($code) {
                $sql .= " AND `Code`='" . $code . "'";
            }
            if ($settingID) {
                $sql .= " AND `SettingID`='" . $settingID . "'";
            }
            $sql .= " ORDER BY SeqNo";
            $rs = $this->returnResultSet($sql);
            if (count($rs)) {
                if ($form && $type) {
                    $rs = BuildMultiKeyAssoc($rs, 'Code', array(
                        'Name'
                    ), $SingleValue = 1);
                } else if ($form) {
                    $rs = BuildMultiKeyAssoc($rs, array(
                        'Type',
                        'Code'
                    ), array(
                        'Name'
                    ), $SingleValue = 1);
                }
            }
            return $rs;
        }

        public function getSENCaseType($settingID)
        {
            if ($settingID) {
                $sql = "SELECT * FROM INTRANET_GUIDANCE_SETTING_ITEM WHERE SettingID='$settingID'";
                $rs = $this->returnResultSet($sql);
                return count($rs) ? $rs[0] : '';
            }
            else {
                return '';
            }
        }

        public function getAllSENCaseType($settingID = '')
        {
            $subTypeName = Get_Lang_Selection('ChineseName', 'EnglishName');
            $sql = "SELECT
                            SettingID,
                            Code,
                            ChineseName,
                            EnglishName,
                            {$subTypeName} as Name,
                            IsWithText
                    FROM
                            INTRANET_GUIDANCE_SETTING_ITEM
                    WHERE        
                            Form='SENCase' AND Type='SENType'";
            
            if (is_array($settingID)) {
                $sql .= " AND SettingID IN ('". implode("','", $settingID) ."'";
            }
            else if ($settingID) {
                $sql .= " AND SettingID='$settingID'";
            }
            $sql .= " ORDER BY SeqNo";
            
            $rs = $this->returnResultSet($sql);
            
            return $rs;
        }
        
        public function getSENCaseSubType($subTypeID='')
        {
            $sql = "SELECT
							t.ChineseName as TypeChineseName,
							t.EnglishName as TypeEnglishName,
							i.Code,
                            i.ChineseName,
                            i.EnglishName,
                            i.ParentSettingID
					FROM
							INTRANET_GUIDANCE_SETTING_ITEM i
					LEFT JOIN
							INTRANET_GUIDANCE_SETTING_ITEM t on t.SettingID=i.ParentSettingID
					WHERE i.Form='SENCase' AND i.Type='SENSubType'";
            if ($subTypeID) {					
				$sql .= " AND i.SettingID='" . $subTypeID . "'";
            }
            $sql .= " ORDER BY t.SettingID, i.SettingID";
            $rs = $this->returnResultSet($sql);
            return count($rs) ? current($rs) : '';
        }

        public function getAllSENCaseSubType($subTypeID='', $typeID='')
        {
            $subTypeName = Get_Lang_Selection('i.ChineseName', 'i.EnglishName');
            $sql = "SELECT
                            t.Code as TypeCode,
							t.ChineseName as TypeChineseName,
							t.EnglishName as TypeEnglishName,
                            i.SettingID,
							i.Code,
                            i.ChineseName,
                            i.EnglishName,
                            {$subTypeName} as Name,
                            i.ParentSettingID
					FROM
							INTRANET_GUIDANCE_SETTING_ITEM i
					LEFT JOIN
							INTRANET_GUIDANCE_SETTING_ITEM t on t.SettingID=i.ParentSettingID
					WHERE i.Form='SENCase' AND i.Type='SENSubType'";
            if ($subTypeID) {
                $sql .= " AND i.SettingID='" . $subTypeID . "'";
            }
            if ($typeID) {
                $sql .= " AND t.SettingID='" . $typeID . "'";
            }
            
            $sql .= " ORDER BY t.SettingID, i.SeqNo";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }
        
        public function getSearchSENCaseSubTypeCode($keyword)
        {
            $sql = "SELECT
                            Code 
					FROM
							INTRANET_GUIDANCE_SETTING_ITEM 
					WHERE 
                            Form='SENCase' AND Type='SENSubType'
                            AND (EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword)."%' OR ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword)."%')
                            ORDER BY Code";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        public function getSearchSENCaseTypeCode($keyword, $matchExactly=false)
        {
            $sql = "SELECT
                            Code
					FROM
							INTRANET_GUIDANCE_SETTING_ITEM
					WHERE
                            Form='SENCase' AND Type='SENType'";
            if ($matchExactly) {
                $sql .= " AND (EnglishName='" . $this->Get_Safe_Sql_Query($keyword)."' OR ChineseName='" . $this->Get_Safe_Sql_Query($keyword)."')";
            }
            else {
                $sql .= " AND (EnglishName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword)."%' OR ChineseName LIKE '%" . $this->Get_Safe_Sql_Like_Query($keyword)."%')";
            }
            $sql .= " ORDER BY Code";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }
        
        public function getGuidanceGeneralSetting($name)
        {
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE SettingName='$name' AND Module='eGuidance'";
            $temp = $this->returnVector($sql);
            return $temp[0];
        }

        public function getRecordIDAryByOrderString($orderText, $separator = ",", $prefix = "")
        {
            if ($orderText == "") {
                return false;
            }
            $orderArr = explode($separator, $orderText);
            $orderArr = array_remove_empty($orderArr);
            $orderArr = Array_Trim($orderArr);
            
            $numOfOrder = count($orderArr);
            // display order starts from 1
            $counter = 1;
            $newOrderArr = array();
            for ($i = 0; $i < $numOfOrder; $i ++) {
                $thisID = str_replace($prefix, "", $orderArr[$i]);
                if (is_numeric($thisID)) {
                    $newOrderArr[$counter] = $thisID;
                    $counter ++;
                }
            }
            return $newOrderArr;
        }
        
        public function updateAdjustTypeDisplayOrder($displayOrderAry)
        {
            if (count($displayOrderAry) == 0) {
                return false;
            }
            
            for ($i = 1, $iMax = count($displayOrderAry); $i <= $iMax; $i ++) {
                $typeID = $displayOrderAry[$i];
                
                $sql = "UPDATE INTRANET_GUIDANCE_SETTING_ADJUST_TYPE SET SeqNo='" . $i . "' WHERE TypeID='" . $typeID . "'";
                $result['ReorderResult' . $i] = $this->db_db_query($sql);
            }
            return $result;
        }
        
        public function updateAdjustItemDisplayOrder($displayOrderAry)
        {
            if (count($displayOrderAry) == 0) {
                return false;
            }
            
            for ($i = 1, $iMax = count($displayOrderAry); $i <= $iMax; $i ++) {
                $typeID = $displayOrderAry[$i];
                
                $sql = "UPDATE INTRANET_GUIDANCE_SETTING_ADJUST_ITEM SET SeqNo='" . $i . "' WHERE ItemID='" . $typeID . "'";
                $result['ReorderResult' . $i] = $this->db_db_query($sql);
            }
            return $result;
        }
        
        public function updateServiceTypeDisplayOrder($displayOrderAry)
        {
            if (count($displayOrderAry) == 0) {
                return false;
            }
            
            for ($i = 1, $iMax = count($displayOrderAry); $i <= $iMax; $i ++) {
                $typeID = $displayOrderAry[$i];
                
                $sql = "UPDATE INTRANET_GUIDANCE_SETTING_SERVICE_TYPE SET SeqNo='" . $i . "' WHERE TypeID='" . $typeID . "'";
                $result['ReorderResult' . $i] = $this->db_db_query($sql);
            }
            return $result;
        }
        
        public function updateServiceItemDisplayOrder($displayOrderAry)
        {
            if (count($displayOrderAry) == 0) {
                return false;
            }
            
            for ($i = 1, $iMax = count($displayOrderAry); $i <= $iMax; $i ++) {
                $typeID = $displayOrderAry[$i];
                
                $sql = "UPDATE INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE SET SeqNo='" . $i . "' WHERE ServiceID='" . $typeID . "'";
                $result['ReorderResult' . $i] = $this->db_db_query($sql);
            }
            return $result;
        }

        public function updateSENTypeDisplayOrder($displayOrderAry)
        {
            if (count($displayOrderAry) == 0) {
                return false;
            }
            
            for ($i = 1, $iMax = count($displayOrderAry); $i <= $iMax; $i ++) {
                $settingID = $displayOrderAry[$i];
                
                $sql = "UPDATE INTRANET_GUIDANCE_SETTING_ITEM SET SeqNo='" . $i . "' WHERE SettingID='" . $settingID . "'";
                $result['ReorderResult' . $i] = $this->db_db_query($sql);
            }
            return $result;
        }
        
        public function getNexSeqNo($type, $typeID = '', $parentSettingID = '')
        {
            $cond = '';
            switch ($type) {
                case 'AdjustType':
                    $table = 'INTRANET_GUIDANCE_SETTING_ADJUST_TYPE';
                    break;
                case 'AdjustItem':
                    $table = 'INTRANET_GUIDANCE_SETTING_ADJUST_ITEM';
                    break;
                case 'ServiceType':
                    $table = 'INTRANET_GUIDANCE_SETTING_SERVICE_TYPE';
                    break;
                case 'ServiceItem':
                    $table = 'INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE';
                    break;
                case 'SENType':
                    $table = 'INTRANET_GUIDANCE_SETTING_ITEM';
                    $cond .= " AND Type='SENType'";
                    break;
                case 'SENSubType':
                    $table = 'INTRANET_GUIDANCE_SETTING_ITEM';
                    $cond .= " AND Type='SENSubType' AND ParentSettingID='".$parentSettingID."'";
                    break;
                    
            }
            if (! empty($table)) {
                $sql = "SELECT MAX(SeqNo)+1 AS NextSeqNo FROM " . $table . " WHERE 1 ";
                if ($typeID) {
                    $sql .= " AND TypeID='" . $typeID . "'";
                }
                $sql .= $cond;
                $rs = $this->returnResultSet($sql);
                if (count($rs)) {
                    return $rs[0]['NextSeqNo'];
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        }
        
        public function getGuidancePushMessage($recordID, $recordType)
        {
            $name_field = getNameFieldByLang("iu.");
            $name_field2 = getNameFieldByLang("iu2.");
            $sql = "SELECT 		m.NotifyMessageID,
								DATE_FORMAT(m.NotifyDateTime, '%Y-%m-%d %H:%i:%s') as NotifyDateTime,
								IF(m.NotifyUserID is not null AND m.NotifyUserID > 0, $name_field, m.NotifyUser) as NotifyUser,
								t.TargetID,
								$name_field2 as NotifyTo,
								mr.MessageStatus 
					FROM 
								INTRANET_APP_NOTIFY_MESSAGE as m
					INNER JOIN 
								INTRANET_GUIDANCE_PUSH_MESSAGE_LOG ml ON ml.NotifyMessageID=m.NotifyMessageID
					LEFT JOIN 
								INTRANET_APP_NOTIFY_MESSAGE_TARGET as t ON m.NotifyMessageID = t.NotifyMessageID
					LEFT JOIN
								INTRANET_APP_NOTIFY_MESSAGE_REFERENCE mr ON (mr.NotifyMessageID=t.NotifyMessageID AND mr.NotifyMessageTargetID=t.NotifyMessageTargetID)
					LEFT JOIN 
								INTRANET_USER as iu ON (m.NotifyUserID = iu.UserID)
					LEFT JOIN 
								INTRANET_USER as iu2 ON (t.TargetID = iu2.UserID)
					WHERE 
								ml.RecordID='" . $recordID . "'
					AND			ml.RecordType='" . $recordType . "'
					ORDER BY 	ml.DateModified DESC";
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        function getPushMessageStatusDisplay($status)
        {
            global $intranet_root, $Lang;
            include ($intranet_root . '/includes/eClassApp/eClassAppConfig.inc.php');
            
            $statusLang = '';
            if ((string) $status == (string) $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed']) {
                $statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['sendFailed'];
            } else if ((string) $status == (string) $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess']) {
                $statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['sendSuccess'];
            } else if ((string) $status == (string) $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead']) {
                $statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['userHasRead'];
            } else if ((string) $status == (string) $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend']) {
                $statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['waitingToSend'];
            } else if ((string) $status == (string) $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice']) {
                $statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['noRegisteredDevice'];
            } else if ((string) $status == (string) $eclassAppConfig['errorCode']['pushMessage']['cannotConnectToServiceProvider'] || (string) $status == (string) $eclassAppConfig['errorCode']['pushMessage']['cannotConnectToCentralServer']) {
                $statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['sendFailed'] . ' (' . $Lang['MessageCentre']['pushMessageAry']['errorStatusAry']['cannotConnectToServiceProvider'] . ')';
            }
            
            return $statusLang;
        }

        public function updateGeneralSettings($data)
        {
            $str = "";
            foreach ($data as $k => $d) {
                $str .= $k . ":" . $d . "#";
            }
            $str = substr($str, 0, strlen($str) - 1);
            
            // check should insert or update
            $sql = "select count(Module) from GENERAL_SETTING where SettingName='MAX'";
            $count = $this->returnVector($sql);
            if ($count[0]) { // update
                $sql = "update GENERAL_SETTING set SettingValue='$str', DateModified=now(), ModifiedBy=" . $_SESSION['UserID'] . " where SettingName='MAX' and Module='eGuidance'";
            } else { // insert
                $sql = "insert into GENERAL_SETTING (Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy) values ('eGuidance', 'MAX', '$str',now(), " . $_SESSION['UserID'] . ",now(), " . $_SESSION['UserID'] . ")";
            }
            $this->db_db_query($sql);
            return ($this->db_affected_rows());
        }

        // Add record to a table
        // $table = "table_name" ;
        // $dataAry = array( 'field1' => 'data1', field2 => 'data2');
        // $condition = array( 'field1' => 'data1', field2 => 'data2');
        //
        function INSERT2TABLE($table, $dataAry = array(), $condition = array(), $run = true, $insertIgnore = false, $updateDateModified = true)
        {
            global $is_debug, $junior_mck;
            
            $result = false;
            $recordID = 0;
            if ($insertIgnore)
                $IGNORE = " IGNORE ";
            
            $sql = "INSERT {$IGNORE} INTO `{$table}` SET ";
            // if ($junior_mck) {
            foreach ($dataAry as $field => $value) {
                $sql .= ($value == "now()") ? "`" . $field . "`= now()," : "`" . $field . "`= '" . $this->Get_Safe_Sql_Query($value) . "',";
            }
            // }
            // else {
            // foreach($dataAry as $field=>$value) {
            // $sql .= ($value == "now()") ? "`" . $field . "`= now()," : "`" . $field . "`= '". $value ."',";
            // }
            // }
            
            if ($updateDateModified) {
                $sql .= "`DateModified`=now(),";
            }
            $sql = substr($sql, 0, - 1); // remove last comma
            
            if (! empty($condition)) {
                // if ($junior_mck) {
                foreach ($condition as $field => $value)
                    $tmp_cond[] = "`{$field}` = '" . $this->Get_Safe_Sql_Query($value) . "'";
                // }
                // else {
                // foreach($condition as $field=>$value)
                // $tmp_cond[] = "`{$field}` = '". $value."'";
                // }
                $sql .= " WHERE " . implode(' AND ', $tmp_cond);
            }
            if ($is_debug) {
                echo $sql . '<br>';
                return;
            } else {
                if ($run) {
                    $result = $this->db_db_query($sql);
                    if ($result) {
                        $recordID = $this->db_insert_id();
                    }
                    return $recordID ? $recordID : $result;
                } else {
                    return $sql;
                }
            }
        }

        // Update a table
        // $table = "table_name" ;
        // $dataAry = array( 'field1' => 'data1', field2 => 'data2');
        // $condition = array( 'field1' => 'data1', field2 => 'data2');
        //
        function UPDATE2TABLE($table, $dataAry = array(), $condition = array(), $run = true, $updateDateModified = true)
        {
            global $UserID, $junior_mck;
            if (! is_array($dataAry)) {
                return false;
            }
            
            $sql = "UPDATE `{$table}` SET ";
            
            // if ($junior_mck) {
            foreach ($dataAry as $field => $value) {
                $sql .= ($value == "now()") ? "`" . $field . "`= now()," : "`" . $field . "`= '" . $this->Get_Safe_Sql_Query($value) . "',";
            }
            // }
            // else {
            // foreach($dataAry as $field=>$value){
            // $sql .= ($value == "now()") ? "`".$field . "`= now()," : "`".$field . "`= '". $value."',";
            // }
            // }
            
            if ($updateDateModified) {
                $sql .= "`DateModified`=now(),";
            }
            $sql = substr($sql, 0, - 1); // remove last comma
            
            if (! empty($condition)) {
                // if ($junior_mck) {
                foreach ($condition as $field => $value) {
                    $tmp_cond[] = "`{$field}` = '" . $this->Get_Safe_Sql_Query($value) . "'";
                }
                // }
                // else {
                // foreach($condition as $field => $value){
                // $tmp_cond[] = "`{$field}` = '". $value."'";
                // }
                // }
                $sql .= " WHERE " . implode(' AND ', $tmp_cond);
            }
            if ($run) {
                $result = $this->db_db_query($sql);
                return $result;
            } else {
                return $sql;
            }
        }

        function sendNotifyToColleague($recordID, $recordType, $notifierID)
        {
            global $intranet_root, $plugin, $Lang, $eclassAppConfig;
            include_once ($intranet_root . "/includes/libuser.php");
            include_once ($intranet_root . "/includes/eClassApp/libeClassApp.php");
            include_once ($intranet_root . "/includes/libwebmail.php");
            include_once ($intranet_root . "/lang/email.php");
            
            $data = array();
            if (count($notifierID)) {
                $rs = array();
                switch ($recordType) {
                    case 'ContactID':
                        $rs = $this->getContact($recordID);
                        $data['NotifyItem'] = $Lang['eGuidance']['NotifyItem']['Contact'];
                        $subject = $Lang['eGuidance']['NotifySubject']['Contact'];
                        $module = $Lang['eGuidance']['menu']['Management']['Contact'];
                        break;
                    case 'GuidanceID':
                        $rs = $this->getGuidance($recordID);
                        $data['NotifyItem'] = $Lang['eGuidance']['NotifyItem']['Guidance'];
                        $subject = $Lang['eGuidance']['NotifySubject']['Guidance'];
                        $module = $Lang['eGuidance']['menu']['Management']['Guidance'];
                        break;
                    case 'ImproveID':
                        $rs = $this->getSelfImprove($recordID);
                        $data['NotifyItem'] = $Lang['eGuidance']['NotifyItem']['SelfImprove'];
                        $subject = $Lang['eGuidance']['NotifySubject']['SelfImprove'];
                        $module = $Lang['eGuidance']['menu']['Management']['SelfImprove'];
                        break;
                    case 'PersonalID':
                        $rs = $this->getPersonal($recordID);
                        $data['NotifyItem'] = $Lang['eGuidance']['NotifyItem']['Personal'];
                        $subject = $Lang['eGuidance']['NotifySubject']['Personal'];
                        $module = $Lang['eGuidance']['menu']['Management']['Personal'];
                        break;
                    case 'SENCaseID':
                        $rs = $this->getSENCase($recordID);
                        $data['NotifyItem'] = $Lang['eGuidance']['NotifyItem']['SENCase'];
                        $subject = $Lang['eGuidance']['NotifySubject']['SENCase'];
                        $module = $Lang['eGuidance']['menu']['Management']['SEN'];
                        break;
                    case 'SuspendID':
                        $rs = $this->getSuspend($recordID);
                        $data['NotifyItem'] = $Lang['eGuidance']['NotifyItem']['Suspend'];
                        $subject = $Lang['eGuidance']['NotifySubject']['Suspend'];
                        $module = $Lang['eGuidance']['menu']['Management']['Suspend'];
                        break;
                    case 'TransferID':
                        $rs = $this->getTransfer($recordID);
                        $data['NotifyItem'] = '';
                        $subject = $Lang['eGuidance']['NotifySubject']['Referral'];
                        $module = $Lang['eGuidance']['menu']['Management']['Transfer'];
                        break;
                }
                if (count($rs)) {
                    $rs = current($rs);
                    $data['StudentID'] = $rs['StudentID'];
                    $timestamp = strtotime($rs['DateModified']);
                    $data['Year'] = date('Y', $timestamp);
                    $data['Month'] = date('n', $timestamp);
                    $data['Day'] = date('j', $timestamp);
                    $studentInfo = $this->getStudentInfo($data['StudentID']);
                    if (count($studentInfo)) {
                        $data['StudentName'] = $studentInfo['StudentName'];
                        $data['ClassName'] = $studentInfo['ClassName'];
                        
                        if ($recordType == 'TransferID') {
                            $content = $Lang['eGuidance']['NotifyReferralContent'];
                        } else {
                            $content = $Lang['eGuidance']['NotifyUpdateContent'];
                        }
                        $content = str_replace("[class]", $data['ClassName'], $content);
                        $content = str_replace("[name]", $data['StudentName'], $content);
                        $content = str_replace("[notifyItem]", $data['NotifyItem'], $content);
                        $content = str_replace("[module]", $module, $content);
                        $content = str_replace("[yy]", $data['Year'], $content);
                        $content = str_replace("[mm]", $data['Month'], $content);
                        $content = str_replace("[dd]", $data['Day'], $content);
                        
                        // send push message
                        $leClassApp = new libeClassApp();
                        if ($plugin['eClassTeacherApp'] && $leClassApp->isSchoolInLicense('T')) {
                            $appType = 'T';
                            foreach ((array) $notifierID as $nid) {
                                $individualMessageInfoAry[0]['relatedUserIdAssoAry'][$nid][] = $nid;
                            }
                            
                            $content = intranet_undo_htmlspecialchars(strip_tags(str_replace("<br>", "\n", $content)));
                            $fromModule = 'eGuidance_' . str_replace('ID', '', $recordType);
                            
                            $pushResult = $notifyMessageId = $leClassApp->sendPushMessage($individualMessageInfoAry, $subject, $content, $isPublic = 'N', $recordStatus = 1, $appType, $sendTimeMode = '', $sendTimeString = '', '', '', $fromModule, $recordID);
                            
                            $dataAry = array();
                            $dataAry['NotifyMessageID'] = $notifyMessageId;
                            $dataAry['RecordID'] = $recordID;
                            $dataAry['RecordType'] = str_replace('ID', '', $recordType);
                            $sql = $this->INSERT2TABLE('INTRANET_GUIDANCE_PUSH_MESSAGE_LOG', $dataAry);
                        } else {
                            $pushResult = false;
                        }
                        
                        // send email
                        $lwebmail = new libwebmail();
                        $emailResult = $lwebmail->sendModuleMail($notifierID, $subject, nl2br($content), 1);
                        return ($pushResult || $emailResult) ? true : false;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else { // no notifier
                return false;
            }
        }
  
        
        function isClassTeacher($userID, $checkViewSEN=true)
        {
            global $junior_mck;
            if ($checkViewSEN) {
                if (!$this->AllowClassTeacherViewSEN) {
                    return false;
                }
            }
            $userID = IntegerSafe($userID);
            if ($userID) {
                if ($junior_mck) {
                    $sql = "SELECT t.ClassID FROM INTRANET_CLASSTEACHER t INNER JOIN INTRANET_CLASS c ON c.ClassID=t.ClassID WHERE t.UserID='".$userID."' AND c.ClassID>0 LIMIT 1";
                }
                else {
                    $sql = "SELECT  
                                    c.ClassTitleEN 
                            FROM 
                                    YEAR_CLASS_TEACHER t
                                    INNER JOIN  YEAR_CLASS c ON c.YearClassID=t.YearClassID
                            WHERE
                                    t.UserID='".$userID."'
                                    AND c.AcademicYearID='".Get_Current_Academic_Year_ID()."'
                                    LIMIT 1";
                }
                $result = $this->returnResultSet($sql);
                return count($result) ? true : false;
            }
            return false;
        }

        function isSubjectTeacher($userID, $checkViewSEN=true)
        {
            global $junior_mck;
            if ($checkViewSEN) {
                if (!$this->AllowSubjectTeacherViewSEN) {
                    return false;
                }
            }
            $userID = IntegerSafe($userID);
            if ($userID) {
                if ($junior_mck) {
                    $sql = "SELECT t.ClassID FROM INTRANET_SUBJECT_TEACHER t WHERE t.UserID='" . $userID . "' AND t.ClassID>0 LIMIT 1";
                }
                else {
                    $sql = "SELECT  
                                    t.SubjectGroupID 
                            FROM 
                                    SUBJECT_TERM_CLASS_TEACHER t
                                    INNER JOIN  SUBJECT_TERM s ON s.SubjectGroupID=t.SubjectGroupID
                            WHERE
                                    t.UserID='".$userID."'
                                    AND s.YearTermID='".getCurrentSemesterID()."'
                                    LIMIT 1";
                }
                $result = $this->returnResultSet($sql);
                return count($result) ? true : false;
            }
            return false;
        }

        // for ej only
        function getClassTeacherClass($userID)
        {
            $classAry = array();
            $userID = IntegerSafe($userID);
            if ($userID) {
                $sql = "SELECT 
                                DISTINCT c.ClassName 
                        FROM 
                                INTRANET_CLASSTEACHER t 
                                INNER JOIN INTRANET_CLASS c ON c.ClassID=t.ClassID 
                        WHERE 
                                t.UserID='".$userID."' 
                                AND c.ClassID>0 
                                ORDER BY c.ClassName";
                $result = $this->returnResultSet($sql);
                if (count($result)) {
                    $classAry = Get_Array_By_Key($result,'ClassName');                    
                }
            }
            return $classAry;
        }

        // for ej only
        function getSubjectTeacherClass($userID)
        {
            $classAry = array();
            $userID = IntegerSafe($userID);
            if ($userID) {
                $sql = "SELECT
                                DISTINCT c.ClassName
                        FROM
                                INTRANET_SUBJECT_TEACHER t
                                INNER JOIN INTRANET_CLASS c ON c.ClassID=t.ClassID
                        WHERE
                                t.UserID='".$userID."'
                                AND c.ClassID>0
                                ORDER BY c.ClassName";
                $result = $this->returnResultSet($sql);
                if (count($result)) {
                    $classAry = Get_Array_By_Key($result,'ClassName');
                }
            }
            return $classAry;
        }

        // get student list (UserID) of a class teacher in current academic year
        function getClassTeacherStudent($userID)
        {
            $userIDAry = array();
            $userID = IntegerSafe($userID);
            if ($userID) {
                $sql = "SELECT  
                                DISTINCT ycu.UserID 
                        FROM 
                                YEAR_CLASS_USER ycu
                                INNER JOIN YEAR_CLASS_TEACHER yct ON yct.YearClassID=ycu.YearClassID
                                INNER JOIN YEAR_CLASS yc ON yc.YearClassID=yct.YearClassID
                        WHERE
                                yct.UserID='".$userID."'
                                AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."'
                                ORDER BY ycu.UserID";

                $result = $this->returnResultSet($sql);
                if (count($result)) {
                    $userIDAry = Get_Array_By_Key($result,'UserID');
                }
            }
            return $userIDAry;
        }

        // get student list (UserID) of a subject group teacher in current academic year term
        function getSubjectTeacherStudent($userID)
        {
            $userIDAry = array();
            $userID = IntegerSafe($userID);
            if ($userID) {
                $sql = "SELECT  
                                DISTINCT stcu.UserID 
                        FROM 
                                SUBJECT_TERM_CLASS_USER stcu
                                INNER JOIN SUBJECT_TERM_CLASS_TEACHER stct ON stct.SubjectGroupID=stcu.SubjectGroupID
                                INNER JOIN SUBJECT_TERM st ON st.SubjectGroupID=stct.SubjectGroupID
                        WHERE
                                stct.UserID='".$userID."'
                                AND st.YearTermID='".getCurrentSemesterID()."'
                                ORDER BY stcu.UserID";

                $result = $this->returnResultSet($sql);
                if (count($result)) {
                    $userIDAry = Get_Array_By_Key($result,'UserID');
                }
            }
            return $userIDAry;
        }

        // return class name list for ej and student UserID for IP
        function getFilterClassListOrUserList($permission)
        {
            global $junior_mck;
            $filterAry = array();
            if ($junior_mck) {
                if ($permission['isClassTeacher'] && $permission['isSubjectTeacher']){     // both
                    $classTeacherClassAry = $this->getClassTeacherClass($_SESSION['UserID']);
                    $subjectTeacherClassAry = $this->getSubjectTeacherClass($_SESSION['UserID']);
                    $filterAry = array_unique(array_merge($classTeacherClassAry, $subjectTeacherClassAry));
                }
                else if ($permission['isClassTeacher'] && !$permission['isSubjectTeacher']){    // class teacher only
                    $filterAry = $this->getClassTeacherClass($_SESSION['UserID']);
                }
                else if (!$permission['isClassTeacher'] && $permission['isSubjectTeacher']){    // subject teacher only
                    $filterAry = $this->getSubjectTeacherClass($_SESSION['UserID']);
                }
            }
            else {
                if ($permission['isClassTeacher'] && $permission['isSubjectTeacher']){     // both
                    $classTeacherStudentAry = $this->getClassTeacherStudent($_SESSION['UserID']);
                    $subjectTeacherStudentAry = $this->getSubjectTeacherStudent($_SESSION['UserID']);
                    $filterAry = array_unique(array_merge($classTeacherStudentAry, $subjectTeacherStudentAry));
                }
                else if ($permission['isClassTeacher'] && !$permission['isSubjectTeacher']){    // class teacher only
                    $filterAry = $this->getClassTeacherStudent($_SESSION['UserID']);
                }
                else if (!$permission['isClassTeacher'] && $permission['isSubjectTeacher']){    // subject teacher only
                    $filterAry = $this->getSubjectTeacherStudent($_SESSION['UserID']);
                }
            }
            return $filterAry;
        }
        
        function getFilterSENStudents($studentIDAry, $logicalType, $senCaseTypeFilterAry, $senCaseSubTypeFilterAry, $itemIDAry, $serviceItemIDAry, $isTierAry)
        {
            $cond = "";
            if (count($studentIDAry)) {
                $cond .= " AND c.StudentID IN ('".implode("','",(array)$studentIDAry)."')";
            }
            $nrTier = count($isTierAry);
            if ($nrTier) {
                for($i=0; $i<$nrTier; $i++) {
                    $j = $i+1;
                    if ($isTierAry[$i]) {
                        $cond .= " AND c.IsTier".$j."='".$isTierAry[$i]."'";
                    }
                }
            }
            if ($logicalType == 'and') {
                if ($senCaseTypeFilterAry != '') {
                    foreach((array)$senCaseTypeFilterAry as $_senTypeCode) {
                        $cond .= " AND CONCAT('^~',c.SENType) LIKE '%^~".$_senTypeCode."%'";
                    }
                }
                
                if ($senCaseSubTypeFilterAry != '') {
                    foreach((array)$senCaseSubTypeFilterAry as $_senSubTypeCode) {
                        $cond .= " AND (c.SENType LIKE '%^:".$_senSubTypeCode."%' OR c.SENType LIKE '%^#".$_senSubTypeCode."%')";
                    }
                }
            }
            else {
                if (($senCaseTypeFilterAry != '') || ($senCaseSubTypeFilterAry != '')) {
                    $cond .= " AND (";
                }
                
                $addOr = false;
                if ($senCaseTypeFilterAry != '') {
                    for ($i=0, $iMax=count($senCaseTypeFilterAry); $i<$iMax; $i++) {
                        $_senTypeCode = $senCaseTypeFilterAry[$i];
                        if ($i>0) {
                            $cond .= " OR ";
                        }
                        $cond .= "CONCAT('^~',c.SENType) LIKE '%^~".$_senTypeCode."%'";
                    }
                    $addOr = true;
                }
                
                if ($senCaseSubTypeFilterAry != '') {
                    for ($i=0, $iMax=count($senCaseSubTypeFilterAry); $i<$iMax; $i++) {
                        $_senSubTypeCode = $senCaseSubTypeFilterAry[$i];
                        if ($i>0 || $addOr) {
                            $cond .= " OR ";
                        }
                        $cond .= "c.SENType LIKE '%^:".$_senSubTypeCode."%' OR c.SENType LIKE '%^#".$_senSubTypeCode."%'";
                    }
                }
                
                if (($senCaseTypeFilterAry != '') || ($senCaseSubTypeFilterAry != '')) {
                    $cond .= " )";
                }
            }
            $sql = "SELECT DISTINCT StudentID FROM INTRANET_GUIDANCE_SEN_CASE c WHERE 1 ".$cond;
            $result = $this->returnResultSet($sql);
            
            if (count($result)) {
                $studentIDAry = Get_Array_By_Key($result,'StudentID');
            }
            else {
                $studentIDAry = array();        // record not match, return empty array
            }
            
            if (count($studentIDAry)) {
                if (count($itemIDAry)) {
                    $validStudentIDAry = array();
                    foreach((array)$studentIDAry as $k=>$_studentID) {
                        $sql = "SELECT
                                    DISTINCT ItemID
                            FROM
                                    INTRANET_GUIDANCE_SEN_CASE_ADJUSTMENT
                            WHERE
                                    StudentID='".$_studentID."' 
                                    ORDER BY ItemID";
                        $selectedItemAry = $this->returnResultSet($sql);
                        $selectedItemIDAry = count($selectedItemAry) ? Get_Array_By_Key($selectedItemAry,'ItemID') : array();
                        
                        $isMatch = true;
                        if (count($selectedItemIDAry)) {
                            foreach((array)$itemIDAry as $_itemID) {
                                if (!in_array($_itemID, $selectedItemIDAry)) {
                                    $isMatch = false;
                                    break;
                                }
                            }
                        }
                        else {
                            $isMatch = false;
                        }
                        if ($isMatch) {
                            $validStudentIDAry[] = $_studentID;
                        }
                    }
                    $studentIDAry = $validStudentIDAry;
                }
                
                if (count($serviceItemIDAry) && count($studentIDAry)) {
                    $validStudentIDAry = array();
                    foreach((array)$studentIDAry as $k=>$_studentID) {
                        $sql = "SELECT
                                        DISTINCT ServiceID
                                FROM
                                        INTRANET_GUIDANCE_SEN_CASE_SUPPORT
                                WHERE
                                        StudentID='".$_studentID."'
                                        ORDER BY ServiceID";
                        $selectedItemAry = $this->returnResultSet($sql);
                        $selectedItemIDAry = count($selectedItemAry) ? Get_Array_By_Key($selectedItemAry,'ServiceID') : array();

                        $isMatch = true;
                        if (count($selectedItemIDAry)) {
                            foreach((array)$serviceItemIDAry as $_itemID) {
                                if (!in_array($_itemID, $selectedItemIDAry)) {
                                    $isMatch = false;
                                    break;
                                }
                            }
                        }
                        else {
                            $isMatch = false;
                        }
                        if ($isMatch) {
                            $validStudentIDAry[] = $_studentID;
                        }
                    }
                    $studentIDAry = $validStudentIDAry;
                }
            }
            
            return $studentIDAry;
        }


        function isStaffSENCaseConfidentialViewerOrPIC($staffIDAry)
        {
            $mainSQL = "SELECT
                            c.StudentID
                    FROM
                            INTRANET_GUIDANCE_SEN_CASE c
                            INNER JOIN INTRANET_GUIDANCE_SEN_CASE_TEACHER t ON t.StudentID=c.StudentID
                    WHERE
                            c.IsConfidential=1";
            
            $result = false;
            $perBatch = 10;     // match number of staff to compare per sql
            $nrStaff = count($staffIDAry);
            if ($nrStaff > $perBatch) {
                $times = ceil($nrStaff / $perBatch);
            }
            else {
                $times = 1;
            }

            for ($i=0; $i < $times; $i++) {
                $sql = $mainSQL;
                $confidentialStaffCond = array();
                $staffCond = array();
                for ($j=0; $j<$perBatch; $j++) {
                    $index = $perBatch * $i + $j;
                    
                    if ($index >= $nrStaff) {
                        break;    
                    }
                    $staffID = $staffIDAry[$index];

                    $confidentialStaffCond[] = "CONCAT(',',c.ConfidentialViewerID,',') LIKE '%,".$staffID.",%'";
                    $staffCond[] = "t.TeacherID='".$staffID."'";
                }
                if (count($staffCond)) {
                    $sql .= " AND ((c.ConfidentialViewerID IS NOT NULL AND (" . implode(" OR ", $confidentialStaffCond) . ")) OR (" . implode(" OR ", $staffCond) . "))";
                    $sql .= " LIMIT 1";
                    $rs = $this->returnResultSet($sql);
                    
                    if (count($rs)){
                        $result = true;
                        break;
                    }
                }
            }
            return $result;
        }
        
        // -- End of Class
    }
}

?>