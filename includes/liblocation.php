<?php

// Using:


/********************************* Modification Log *************************************
 *
 * 2019-12-18 Cameron
 *      - add function getBuildingFloorRoomNameAry()
 *
 * 2019-05-14 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 *
 * 2018-08-09 (Cameron)
 *      - add function getRecordID()
 *      
 * 2018-07-31 (Cameron)
 *      - add function getEnrolCategoryList(), getEnrolLocationCategory(), addLocationCategory(), deleteLocationCategory() in liblocation class
 *      - assign RecordID after execute Insert_Record() 
 *      
 * 2016-06-01 (Kenneth)
 * 		add capacity for room
 * 		modified Insert_Record(), make field='Capacity' is NULLABLE
 * 
 * 2012-07-27 (Rita) :
 * 		add Add_Campus_Delete_Log();
 * 
 * 2012-03-15 (YatWoon) :
 *		update Check_Can_Delete(), add checking for remove location
 *
 * 2011-09-14 (YatWoon) :
 *		updated Insert_Record(), add Barcode checking, also don't check for RecordStatus 1 only
 *
 * 2011-02-22 (Yuen) : 
 * 					i. added Check_Any_Room_By_Floor() to check for any existing room of particular floor
 * 					ii. added Get_All_Floor_Cached() for getting floors using cache method
 * 					iii. adopted cached method in Get_All_Room() for saving costs on DB loading
 * 
 * 
 ****************************************************************************************/

include_once('libdb.php');
include_once('lib.php');

if (!defined("LIBLOCATION_DEFINED"))         // Preprocessor directives
{
	define("LIBLOCATION_DEFINED", true);
	define("LOCATION_STATUS_ACTIVE", 1);
	define("LOCATION_STATUS_INACTIVE", 0);
	
	
	
	class liblocation extends libdb {
		var $DBName;
		var $TableName;
		var $ID_FieldName;
		
		var $RecordID;
		var $Code;
		var $Barcode;
		var $NameEng;
		var $NameChi;
		var $DisplayOrder;
		var $RecordType;
		var $RecordStatus;
		var $DateInput;
		var $DateModified;
		var $LastModifiedBy;
		
		
		function liblocation(){
			global $intranet_db;
			parent:: libdb();
			
			//$this->DBName = 'intranet';
			$this->DBName = $intranet_db;
		}
		
		function Get_Self_Info()
		{
			$sql = "";
			$sql .= " SELECT * FROM ".$this->TableName;
			$sql .= " WHERE ".$this->ID_FieldName." = '".$this->RecordID."'";
			
			$resultSet = $this->returnArray($sql);
			$infoArr = $resultSet[0];
			
			if (count($infoArr) > 0)
			{
				foreach ($infoArr as $key => $value)
					$this->{$key} = $value;
			}
		}
		
		function Get_Name($Lang='')
		{
			$returnName = '';
			if ($Lang == '')
			{
				$returnName = $this->NameEng.' ('.$this->NameChi.')';
			}
			else if ($Lang == 'en')
			{
				$returnName = $this->NameEng;
			}
			else if ($Lang == 'b5')
			{
				$returnName = $this->NameChi;
			}
			
			return $returnName;
		}
		
		function Insert_Record($DataArr=array())
		{
			if (count($DataArr) == 0)
				return false;
				
			# Check if the record is in-active by the "Code"
			//$RecordID = $this->Is_Record_Existed($DataArr['Code'],'','',1,$DataArr['LocationLevelID'],$DataArr['BuildingID'],$DataArr['Barcode']);
			$RecordID = $this->Is_Record_Existed($DataArr['Code'],'','','',$DataArr['LocationLevelID'],$DataArr['BuildingID'],$DataArr['Barcode']);
            if ($RecordID)
			{
			    # set to active
				$objectClass = get_class($this);
				$libObject = new $objectClass($RecordID);
				$successArr['ActiviateOldRecord'] = $libObject->Activate_Record();
				$successArr['UpdateRecord'] = $libObject->Update_Record($DataArr);
				if (in_array(false, $successArr)) {
					$success = false;
				}
				else {
				    $this->RecordID = $RecordID;
					$success = true;
				}
			}
			else
			{
                ## insert data
				# DisplayOrder
				$thisDisplayOrder = $this->Get_Max_Display_Order() + 1;
				
				# Last Modified By
				$LastModifiedBy = ($_SESSION['UserID'])? '\''.$_SESSION['UserID'].'\'' : 'NULL';
				
				# set field and value string
				$fieldArr = array();
				$valueArr = array();
				foreach ($DataArr as $field => $value)
				{
					$fieldArr[] = $field;
					
					//For Null value
					if($field=='Capacity'){
						if(empty($value)){
							$_this_value_is_null = true;							
						}
					}
					if($_this_value_is_null){
						$valueArr[] = 'NULL';
					}else{
						$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
					}
					$_this_value_is_null = false;
					
				}
				## set others fields
				# DisplayOrder
				$fieldArr[] = 'DisplayOrder';
				$valueArr[] = $thisDisplayOrder;
				# Last Modified By
				$fieldArr[] = 'LastModifiedBy';
				$valueArr[] = $LastModifiedBy;
				# DateInput
				$fieldArr[] = 'DateInput';
				$valueArr[] = 'now()';
				# DateModified
				$fieldArr[] = 'DateModified';
				$valueArr[] = 'now()';
				# Set RecordStatus = active
				$fieldArr[] = 'RecordStatus';
				$valueArr[] = LOCATION_STATUS_ACTIVE;
				
				$fieldText = implode(", ", $fieldArr);
				$valueText = implode(", ", $valueArr);
				
				# Insert Record
				$this->Start_Trans();
				
				$sql = '';
				$sql .= ' INSERT INTO '.$this->TableName;
				$sql .= ' ( '.$fieldText.' ) ';
				$sql .= ' VALUES ';
				$sql .= ' ( '.$valueText.' ) ';
				
				$success = $this->db_db_query($sql) or die(mysql_error());
				if ($success) {
				    $this->RecordID = $this->db_insert_id();
				}
			}
			
			if ($success == false) { 
				$this->RollBack_Trans();
			}
			else {		    
				$this->Commit_Trans();
			}
			
			return $success;
		}
		
		function Update_Record($DataArr=array())
		{
			if (count($DataArr) == 0)
				return false;
				
			# Build field update values string
			$valueFieldText = '';
			foreach ($DataArr as $field => $value)
			{
				//For Null value
				if($field=='Capacity'){
					if(empty($value)){
						$_this_value_is_null = true;
					}
				}
				if($_this_value_is_null){
					$valueFieldText .= $field.' = NULL, ';
				}else{
					$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
				}
				$_this_value_is_null = false;
			}
			$valueFieldText .= ' DateModified = now(), ';
			$LastModifiedBy = ($_SESSION['UserID'])? '\''.$_SESSION['UserID'].'\'' : 'NULL';
			$valueFieldText .= ' LastModifiedBy = '.$LastModifiedBy.' ';
			
			$sql = '';
			$sql .= ' UPDATE '.$this->TableName;
			$sql .= ' SET '.$valueFieldText;
			$sql .= ' WHERE '.$this->ID_FieldName.' = \''.$this->RecordID.'\' ';			
			$success = $this->db_db_query($sql);
			
			return $success;
		}
			
		function Delete_Record()
		{
			$success = $this->Inactivate_Record();
			
			return $success;
		}
		
		function Inactivate_Record()
		{
			$DataArr['RecordStatus'] = LOCATION_STATUS_INACTIVE;
			$success = $this->Update_Record($DataArr);
			
			return $success;
		}
		
		function Activate_Record()
		{
		    $DataArr['RecordStatus'] = LOCATION_STATUS_ACTIVE;
			
			# assign to the last display for activated records
			$thisDisplayOrder = $this->Get_Max_Display_Order() + 1;
			$DataArr['DisplayOrder'] = $thisDisplayOrder;
			$success = $this->Update_Record($DataArr);
			return $success;
		}
		
		function Get_Update_Display_Order_Arr($OrderText, $Separator=",", $Prefix="")
		{
			if ($OrderText == "")
				return false;
				
			$orderArr = explode($Separator, $OrderText);
			$orderArr = array_remove_empty($orderArr);
			$orderArr = Array_Trim($orderArr);
			
			$numOfOrder = count($orderArr);
			# display order starts from 1
			$counter = 1;
			$newOrderArr = array();
			for ($i=0; $i<$numOfOrder; $i++)
			{
				$thisID = str_replace($Prefix, "", $orderArr[$i]);
				if (is_numeric($thisID))
				{
					$newOrderArr[$counter] = $thisID;
					$counter++;
				}
			}
			
			return $newOrderArr;
		}
		
		function Update_DisplayOrder($DisplayOrderArr=array())
		{
			if (count($DisplayOrderArr) == 0)
				return false;
				
			$this->Start_Trans();
			for ($i=1; $i<=sizeof($DisplayOrderArr); $i++) {
				$thisBuildingID = $DisplayOrderArr[$i];
				
				$sql = 'UPDATE '.$this->TableName.' SET 
									DisplayOrder = \''.$i.'\' 
								WHERE 
									'.$this->ID_FieldName.' = \''.$thisBuildingID.'\'';
				$Result['ReorderResult'.$i] = $this->db_db_query($sql);
			}
			
			if (in_array(false,$Result)) 
			{
				$this->RollBack_Trans();
				return false;
			}
			else
			{
				$this->Commit_Trans();
				return true;
			}
		}
		
		function Get_Building_Floor_Room_Name_Arr($TimetableID='', $RoomIDArr='')
		{
			$buildingTable = $this->DBName.".INVENTORY_LOCATION_BUILDING";
			$floorTable = $this->DBName.".INVENTORY_LOCATION_LEVEL";
			$roomTable = $this->DBName.".INVENTORY_LOCATION";
			
			$cond_TimetableID = '';
			if ($TimetableID != '') {
				$join_INTRANET_TIMETABLE_ROOM_ALLOCATION = "INNER JOIN
															INTRANET_TIMETABLE_ROOM_ALLOCATION as itra On (r.LocationID = itra.LocationID)";
				$cond_TimetableID = " And itra.TimetableID = '$TimetableID' ";
			}
			
			$cond_RoomID = '';
			if ($RoomIDArr != '') {
				$cond_RoomID = " And r.LocationID In ('".implode("','", (array)$RoomIDArr)."') ";
			}
			
			$sql = "SELECT 
							b.BuildingID,
							b.Code as BuildingCode,
							b.NameChi as BuildingNameChi, 
							b.NameEng as BuildingNameEng,
							f.LocationLevelID, 
							f.Code as FloorCode,
							f.NameChi as FloorNameChi, 
							f.NameEng as FloorNameEng,
							r.LocationID, 
							r.Code as RoomCode,
							r.NameChi as RoomNameChi, 
							r.NameEng as RoomNameEng
					FROM 
							$buildingTable as b
							INNER JOIN $floorTable as f ON f.BuildingID = b.BuildingID
							INNER JOIN $roomTable as r ON r.LocationLevelID = f.LocationLevelID
							$join_INTRANET_TIMETABLE_ROOM_ALLOCATION
					WHERE
							b.RecordStatus = 1
							AND f.RecordStatus = 1
							AND r.RecordStatus = 1
							$cond_TimetableID
							$cond_RoomID
					Order By
							b.DisplayOrder, f.DisplayOrder, r.DisplayOrder
					";
			$resultArr = $this->returnArray($sql);
			$numOfRoom = count($resultArr);
			
			$returnArr = array();
			for ($i=0; $i<$numOfRoom; $i++) {
				$thisLocationID = $resultArr[$i]['LocationID'];
				$returnArr[$thisLocationID] = $resultArr[$i];
			}
			
			return $returnArr;
		}

        function getBuildingFloorRoomNameAry($RoomIDArr='')
        {
            $buildingTable = $this->DBName.".INVENTORY_LOCATION_BUILDING";
            $floorTable = $this->DBName.".INVENTORY_LOCATION_LEVEL";
            $roomTable = $this->DBName.".INVENTORY_LOCATION";

            $buildingName = Get_Lang_Selection("b.NameChi","b.NameEng");
            $floorName = Get_Lang_Selection("f.NameChi","f.NameEng");
            $roomName = Get_Lang_Selection("r.NameChi","r.NameEng");

            $cond_RoomID = '';
            if ($RoomIDArr != '') {
                $cond_RoomID = " And r.LocationID In ('".implode("','", (array)$RoomIDArr)."') ";
            }

            $sql = "SELECT 
							b.BuildingID,
							b.Code as BuildingCode,
							$buildingName as BuildingName, 
							f.LocationLevelID, 
							f.Code as FloorCode,
							$floorName as FloorName, 
							r.LocationID, 
							r.Code as LocationCode,
							$roomName as LocationName 
					FROM 
							$buildingTable as b
							INNER JOIN $floorTable as f ON f.BuildingID = b.BuildingID
							INNER JOIN $roomTable as r ON r.LocationLevelID = f.LocationLevelID
					WHERE
							b.RecordStatus = 1
							AND f.RecordStatus = 1
							AND r.RecordStatus = 1
							$cond_RoomID
					Order By
							b.DisplayOrder, f.DisplayOrder, r.DisplayOrder
					";
            $resultAry = $this->returnResultSet($sql);

            return $resultAry;
        }

		function Get_Location_ID_Code_Mapping()
		{
			$buildingTable = $this->DBName.".INVENTORY_LOCATION_BUILDING";
			$floorTable = $this->DBName.".INVENTORY_LOCATION_LEVEL";
			$roomTable = $this->DBName.".INVENTORY_LOCATION";
			
			$sql = "SELECT 
							b.Code as BuildingCode,
							f.Code as FloorCode,
							r.Code as RoomCode,
							r.LocationID
					FROM 
							$buildingTable as b
							INNER JOIN
							$floorTable as f ON f.BuildingID = b.BuildingID
							INNER JOIN
							$roomTable as r ON r.LocationLevelID = f.LocationLevelID
					WHERE
							b.RecordStatus = 1
							AND
							f.RecordStatus = 1
							AND
							r.RecordStatus = 1
					";
			$resultArr = $this->returnArray($sql);
			
			$MappingArr = array();
			$numOfLocation = count($resultArr);
			for ($i=0; $i<$numOfLocation; $i++)
			{
				$thisBuildingCode = $resultArr[$i]['BuildingCode'];
				$thisFloorCode = $resultArr[$i]['FloorCode'];
				$thisRoomCode = $resultArr[$i]['RoomCode'];
				$thisLocationID = $resultArr[$i]['LocationID'];
				
				$MappingArr[$thisBuildingCode][$thisFloorCode][$thisRoomCode] = $thisLocationID;
			}
			
			return $MappingArr;
		}
		
		
		#Insert delete log of Campus item		
		function Add_Campus_Delete_Log($recordID, $RecordType){
			global $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT.'includes/liblog.php');
			
			$result = false;
			# Retrieve Information for inserting delete log
			if($RecordType == "Building"){
				$campusInfoAry = array();
				$sql = "SELECT BuildingID, Code, Barcode, NameChi, NameEng FROM INVENTORY_LOCATION_BUILDING WHERE BuildingID = '".$recordID."'";
				$campusInfoAry = $this->returnArray($sql);
				$campusCode = $campusInfoAry[0]['Code'];
				$campusBarcode = $campusInfoAry[0]['Barcode'];
				$campusNameChi = $campusInfoAry[0]['NameChi'];
				$campusNameEng = $campusInfoAry[0]['NameEng'];
				$campusBuildingID = $campusInfoAry[0]['BuildingID'];	
				$section = 'Delete_Campus_Building';
				$tableName = 'INVENTORY_LOCATION_BUILDING';
			}
			elseif($RecordType == "Floor"){
				$campusInfoAry = array();
				$sql = "Select LocationLevelID, BuildingID, Code, Barcode, NameChi, NameEng From INVENTORY_LOCATION_LEVEL Where LocationLevelID = '".$recordID."'";
				$campusInfoAry = $this->returnArray($sql);
				$campusCode = $campusInfoAry[0]['Code'];
				$campusBarcode = $campusInfoAry[0]['Barcode'];
				$campusNameChi = $campusInfoAry[0]['NameChi'];
				$campusNameEng = $campusInfoAry[0]['NameEng'];		
				$parentID = $campusInfoAry[0]['BuildingID'];
	
				$section = 'Delete_Campus_Location';
				$tableName = 'INVENTORY_LOCATION_LEVEL';
			}
			elseif($RecordType == "Room"){
				$campusInfoAry = array();
				$sql = "Select LocationID, LocationLevelID, Code, Barcode, NameChi, NameEng From INVENTORY_LOCATION Where LocationID = '".$recordID."'";
				$campusInfoAry = $this->returnArray($sql);
				$campusCode = $campusInfoAry[0]['Code'];
				$campusBarcode = $campusInfoAry[0]['Barcode'];
				$campusNameChi = $campusInfoAry[0]['NameChi'];
				$campusNameEng = $campusInfoAry[0]['NameEng'];
				$parentID = $campusInfoAry[0]['LocationLevelID'];	
				
				$section = 'Delete_Campus_Sub-location';
				$tableName = 'INVENTORY_LOCATION';
			}
			else{
				$result = false;
				return $result;
			}
			
			# Start inserting delete log
			$liblog = new liblog();
			$tmpAry = array();
			$tmpAry['ID'] = $recordID;
			$tmpAry['Code'] = $campusCode;
			$tmpAry['Barcode'] = $campusBarcode;
			$tmpAry['NameChi'] = $campusNameChi;
			$tmpAry['NameEng'] = $campusNameEng;
			$tmpAry['ParentID'] = $parentID;
			
			$SuccessArr['Log_Delete'] = $liblog->INSERT_LOG('Campus', $section, $liblog->BUILD_DETAIL($tmpAry), $tableName, $recordID);

			if($SuccessArr['Log_Delete']){		
				$result = true;
			}
			else{
				$result = false;			
			}
			return $result;	
		}
		

		function getEnrolCategoryList()
		{
		    $sql = "SELECT 
                            iec.CategoryID, iec.CategoryName
		            FROM
		                    INTRANET_ENROL_CATEGORY iec
                    ORDER BY iec.DisplayOrder";
		    $categoryAry = $this->returnResultSet($sql);
		    return $categoryAry;
		}

		function getEnrolLocationCategory($locationID)
		{
		    $sql = "SELECT
                            iec.CategoryID, iec.CategoryName
		            FROM
		                    INTRANET_ENROL_CATEGORY iec
                    INNER JOIN
                            INTRANET_ENROL_LOCATION_CATEGORY ielc ON ielc.CategoryID=iec.CategoryID
		            WHERE
                            ielc.LocationID='".$locationID."'
                    ORDER BY iec.DisplayOrder";
		    $categoryAry = $this->returnResultSet($sql);
		    return $categoryAry;
		}
		
		function addLocationCategory($locationID, $categoryID)
		{
		    $sql = "INSERT INTO INTRANET_ENROL_LOCATION_CATEGORY (LocationID, CategoryID) VALUES ('".$locationID."', '".$categoryID."')";
		    return $this->db_db_query($sql);
		}

		function deleteLocationCategory($locationID)
		{
		    $sql = "DELETE FROM INTRANET_ENROL_LOCATION_CATEGORY WHERE LocationID='".$locationID."'";
		    return $this->db_db_query($sql);
		}
		
		function getRecordID()
		{
		    return $this->RecordID;
		}
		
		
		################# Functions inheritance by child #########################
		function Get_Max_Display_Order()
		{
			return false;
		}
		# inheritance by child
		function Get_Last_Modified_Info()
		{
			return false;
		}
		
		/*
		 *	Return false if record not existed
		 *	Return RecordID if record existed
		 */
		function Is_Record_Existed($Code, $NameEng='', $NameChi='')
		{
			return false;
		}
		################# End of Functions inheritance by child #########################
	}
	
	
	
	## Building Class
	class Building extends liblocation {
		var $BuildingID;
		var $All_Building_Floors = array();
		var $All_Building_Floor_Rooms = array();
		
		function Building($BuildingID=""){
			parent:: liblocation();
			$this->TableName = $this->DBName.".INVENTORY_LOCATION_BUILDING";
			$this->ID_FieldName = "BuildingID";
			
			if ($BuildingID!="")
			{
				$this->RecordID = $BuildingID;
				$this->Get_Self_Info();
			}
		}
		
		function Get_All_Building($includeInactive=0, $showHasRoomOnly=0)
		{
			$statusCond = "";
			if (!$includeInactive)
				$statusCond = " WHERE buildingTable.RecordStatus = '".LOCATION_STATUS_ACTIVE."' ";
				
			$innerJoinCond = '';
			if ($showHasRoomOnly)
				$innerJoinCond = " 	INNER JOIN INVENTORY_LOCATION_LEVEL as floorTable ON (buildingTable.BuildingID = floorTable.BuildingID)
									INNER JOIN INVENTORY_LOCATION as roomTable ON (floorTable.LocationLevelID = roomTable.LocationLevelID) ";
				
			$table = $this->DBName.".INVENTORY_LOCATION_BUILDING as buildingTable";
			
			$sql = '';
			$sql .= " 	SELECT
								DISTINCT buildingTable.BuildingID,
								buildingTable.Code,
								buildingTable.Barcode,
								buildingTable.NameChi,
								buildingTable.NameEng,
								buildingTable.DisplayOrder,
								buildingTable.RecordStatus,
								buildingTable.DateInput,
								buildingTable.DateModified,
								buildingTable.LastModifiedBy 
						FROM ".$table;
			$sql .= $innerJoinCond;
			$sql .= $statusCond;
			$sql .= " ORDER BY buildingTable.DisplayOrder";
			$resultSet = $this->returnArray($sql);
			
			return $resultSet;
		}
		
		function Get_All_Floor($includeInactive=0)
		{
			$statusCond = "";
			if (!$includeInactive)
				$statusCond = " AND RecordStatus = '".LOCATION_STATUS_ACTIVE."' ";
				
			$table = $this->DBName.".INVENTORY_LOCATION_LEVEL";
			
			$sql = '';
			$sql .= " SELECT * FROM ".$table;
			$sql .= " WHERE BuildingID = '".$this->BuildingID."'";
			$sql .= $statusCond;
			$sql .= " ORDER BY DisplayOrder";
			
			$resultSet = $this->returnArray($sql);
			
			return $resultSet;
		}
		
		function Get_All_Floor_Cached($includeInactive=0, $BuildingID)
		{
			if (is_array($this->All_Building_Floors) && count($this->All_Building_Floors)>0)
			{
				$resultSet = $this->All_Building_Floors[$BuildingID];
			} else
			{
				$statusCond = "";
				if (!$includeInactive)
					$statusCond = " WHERE RecordStatus = '".LOCATION_STATUS_ACTIVE."' ";
					
				$table = $this->DBName.".INVENTORY_LOCATION_LEVEL";
				
				$sql = '';
				$sql .= " SELECT * FROM ".$table;
				$sql .= $statusCond;
				$sql .= " ORDER BY DisplayOrder";

				$resultSetAll = $this->returnArray($sql);
				
				for ($i=0; $i<sizeof($resultSetAll); $i++)
				{
					$resultSetConsolidated[$resultSetAll[$i]['BuildingID']][] = $resultSetAll[$i];
					if ($resultSetAll[$i]['BuildingID']==$BuildingID)
						$resultSet[] = $resultSetAll[$i];
				}
				
				$this->All_Building_Floors = $resultSetConsolidated; 
			}
			
			return $resultSet;
		}
		
		function Check_Any_Room_By_Floor($FloorID)
		{
			# load all rooms
			if (!(is_array($this->All_Building_Floor_Rooms) && count($this->All_Building_Floor_Rooms)>0))
			{
				$statusCond = "";
				if (!$includeInactive)
					$statusCond = " WHERE RecordStatus = '".LOCATION_STATUS_ACTIVE."' ";
					
				$table = $this->DBName.".INVENTORY_LOCATION";
				
				$sql = '';
				$sql .= " SELECT * FROM ".$table;
				$sql .= $statusCond;
				$sql .= " ORDER BY DisplayOrder";
				$resultSet = $this->returnArray($sql);

				for ($i=0; $i<sizeof($resultSet); $i++)
				{
					if ($resultSetConsolidated[$resultSet[$i]['LocationLevelID']]=="")
					{
						$resultSetConsolidated[$resultSet[$i]['LocationLevelID']] = 0;
					}
					$resultSetConsolidated[$resultSet[$i]['LocationLevelID']] ++;
				}
				
				$this->All_Building_Floor_Rooms = $resultSetConsolidated;
				
			} 
			# match by FloorID
			return ($this->All_Building_Floor_Rooms[$FloorID]>0);
			
		}
		
		function Is_Record_Existed($Code, $NameEng='', $NameChi='', $RecordStatus='')
		{
			
		    
		    $NameEngCond = '';
			if ($NameEng != '')
				$NameEngCond = " And NameEng = '".$this->Get_Safe_Sql_Query($NameEng)."' ";
				
			$NameChiCond = '';
			if ($NameChi != '')
				$NameChiCond = " And NameChi = '".$this->Get_Safe_Sql_Query($NameChi)."' ";
				
			$RecordStatusCode = '';
			if ($RecordStatus != '')
				$RecordStatusCode = " And RecordStatus = '".$this->Get_Safe_Sql_Query($RecordStatus)."' ";
				
			$sql = 'SELECT 
							'.$this->ID_FieldName.'
					FROM 
							'.$this->TableName.'
					WHERE 
							Code = \''.$this->Get_Safe_Sql_Query($Code).'\'
							'.$NameEngCond.'
							'.$NameChiCond.'
							'.$RecordStatusCode.'
					';
			
			$resultSet = $this->returnVector($sql);
			
			if (count($resultSet) == 0)
				return false;
			else
				return $resultSet[0];
		}
		
		function Is_Code_Existed($TargetCode, $ExcludeID='')
		{
			$TargetCode = $this->Get_Safe_Sql_Query(trim($TargetCode));
			
			$excludeBuildingCond = '';
			if ($ExcludeID != '')
				$excludeBuildingCond = " AND BuildingID != '$ExcludeID' ";
				
			# Check Building Code
			$sql = "SELECT 
							DISTINCT(BuildingID) 
					FROM 
							".$this->TableName." 
					WHERE 
							Code = '".$TargetCode."' 
							AND 
							RecordStatus = '1'
							$excludeBuildingCond
					";
			$buildingArr = $this->returnVector($sql);
			$isCodeExisted = (count($buildingArr)==0)? 0 : 1;
			
			return $isCodeExisted;
		}
		
		function Get_Max_Display_Order()
		{
			$sql = '';
			$sql .= 'SELECT max(DisplayOrder) FROM '.$this->TableName;
			$MaxDisplayOrder = $this->returnVector($sql);
			
			return $MaxDisplayOrder[0];
		}
		
		function Get_Last_Modified_Info()
		{
			if ($this->RecordID != '')
			{
				# self last modified info
				$sql = '';
				$sql .= ' SELECT * FROM '.$this->TableName;
				$sql .= ' WHERE BuildingID = \''.$this->RecordID.'\' ';
			}
			else
			{
				# all buildings last modified info
				$sql = '';
				$sql .= ' SELECT * ';
				$sql .= ' FROM '.$this->TableName;
				$sql .= ' WHERE DateModified IN 
								(SELECT MAX(DateModified) FROM '.$this->TableName.')';
			}
			
			$returnArr = $this->returnArray($sql);
			return $returnArr;
		}
		
		function Is_Barcode_Existed($TargetBarcode, $ExcludeID='')
		{
			$TargetBarcode = $this->Get_Safe_Sql_Query(trim($TargetBarcode));
			
			$excludeBuildingCond = '';
			if ($ExcludeID != '')
				$excludeBuildingCond = " AND BuildingID != '$ExcludeID' ";
				
			# Check Building Code
			$sql = "SELECT 
							DISTINCT(BuildingID) 
					FROM 
							".$this->TableName." 
					WHERE 
							Barcode = '".$TargetBarcode."' 
							AND 
							RecordStatus = '1'
							$excludeBuildingCond
					";
			$buildingArr = $this->returnVector($sql);
			$isCodeExisted = (count($buildingArr)==0)? 0 : 1;
			
			return $isCodeExisted;
		}
		
		function getRecordID()
		{
		    return $this->RecordID;
		}
		
	}
	
	
	
	## Floor Class
	class Floor extends Building {
		var $LocationLevelID;
		var $All_Building_Floor_Rooms = array();
		
		function Floor($LocationLevelID="", $BuildingID=""){
			parent:: Building();
			$this->TableName = $this->DBName.".INVENTORY_LOCATION_LEVEL";
			$this->ID_FieldName = "LocationLevelID";
			
			if ($BuildingID!="")
				$this->BuildingID = $BuildingID;
			
			if ($LocationLevelID!="")
			{
				$this->RecordID = $LocationLevelID;
				$this->Get_Self_Info();
			}
		}
		
		function Get_All_Floor($includeInactive=0)
		{
			$statusCond = "";
			if (!$includeInactive)
				$statusCond = " WHERE RecordStatus = '".LOCATION_STATUS_ACTIVE."' ";
				
			$sql = '';
			$sql .= 'SELECT * FROM '.$this->TableName;
			$sql .= $statusCond;
			$sql .= ' ORDER BY DisplayOrder';
			$resultSet = $this->returnArray($sql);
			
			return $resultSet;
		}
		
		function Get_All_Room($includeInactive=0, $LocationLevelID="")
		{
			if ($LocationLevelID=="")
			{
				$statusCond = "";
				if (!$includeInactive)
					$statusCond = " AND RecordStatus = '".LOCATION_STATUS_ACTIVE."' ";
					
				$table = $this->DBName.".INVENTORY_LOCATION";
				
				$sql = '';
				$sql .= " SELECT * FROM ".$table;
				$sql .= " WHERE LocationLevelID = '".$this->LocationLevelID."'";
				$sql .= $statusCond;
				$sql .= " ORDER BY DisplayOrder";
				$resultSet = $this->returnArray($sql);
			} else
			{
				# cache method
				if (sizeof($this->All_Building_Floor_Rooms)>0)
				{
					$resultSet = $this->All_Building_Floor_Rooms[$LocationLevelID];
				} else
				{
					$statusCond = "";
					if (!$includeInactive)
						$statusCond = " WHERE RecordStatus = '".LOCATION_STATUS_ACTIVE."' ";
						
					$table = $this->DBName.".INVENTORY_LOCATION";
					
					$sql = '';
					$sql .= " SELECT * FROM ".$table;
					$sql .= $statusCond;
					$sql .= " ORDER BY DisplayOrder";
					$resultSetAll = $this->returnArray($sql);

					for ($i=0; $i<sizeof($resultSetAll); $i++)
					{
						$resultSetConsolidated[$resultSetAll[$i]['LocationLevelID']][] = $resultSetAll[$i];
						if ($resultSetAll[$i]['LocationLevelID']==$LocationLevelID)
							$resultSet[] = $resultSetAll[$i];
					}
					$this->All_Building_Floor_Rooms = $resultSetConsolidated; 
				}
			}
			
			return $resultSet;
		}
		
		function Is_Record_Existed($Code, $NameEng='', $NameChi='', $RecordStatus='')
		{
			$NameEngCond = '';
			if ($NameEng != '')
				$NameEngCond = " And NameEng = '".$this->Get_Safe_Sql_Query($NameEng)."' ";
				
			$NameChiCond = '';
			if ($NameChi != '')
				$NameChiCond = " And NameChi = '".$this->Get_Safe_Sql_Query($NameChi)."' ";
				
			$RecordStatusCond = '';
			if ($RecordStatus != '')
				$RecordStatusCond = " And RecordStatus = '".$this->Get_Safe_Sql_Query($RecordStatus)."' ";
				
			$buildingCond = "";
			if ($this->BuildingID != '')
				$buildingCond = " AND BuildingID = '".$this->BuildingID."'";
			
			$sql = 'SELECT 
							'.$this->ID_FieldName.'
					FROM 
							'.$this->TableName.'
					WHERE 
							Code = \''.$this->Get_Safe_Sql_Query($Code).'\'
							'.$NameEngCond.'
							'.$NameChiCond.'
							'.$buildingCond.'
							'.$RecordStatusCond.'
					';
			$resultSet = $this->returnVector($sql);
			
			if (count($resultSet) == 0)
				return false;
			else
				return $resultSet[0];
		}
		
		### Check if the Code is duplicated within the same Building
		function Is_Code_Existed($TargetCode, $ExcludeID='', $BuildingID='')
		{
			$TargetCode = $this->Get_Safe_Sql_Query(trim($TargetCode));
			
			$excludeFloorCond = '';
			if ($ExcludeID != '')
			{
				$excludeFloorCond = " AND floorTable.LocationLevelID != '$ExcludeID' ";
				
				$FloorObj = new Floor($ExcludeID);
				$BuildingID = $FloorObj->BuildingID;
			}
			
			$buildingTable = $this->DBName.".INVENTORY_LOCATION_BUILDING";
			$buildingCond = '';
			if ($BuildingID != '')
				$buildingCond = " AND buildingTable.BuildingID = '$BuildingID' ";
				
			
			# Check Floor Code
			$floorTable = $this->DBName.".INVENTORY_LOCATION_LEVEL";
			$sql = "	SELECT 
									DISTINCT(floorTable.LocationLevelID) 
							FROM 
									".$this->TableName." as floorTable
									Inner Join
									".$buildingTable." as buildingTable
									On (floorTable.BuildingID = buildingTable.BuildingID)
							WHERE 
									floorTable.Code = '".$TargetCode."' 
									AND 
									floorTable.RecordStatus = '1'
									$excludeFloorCond
									$buildingCond
							";
			$floorArr = $this->returnVector($sql);
			$isCodeExisted = (count($floorArr)==0)? false : true;
			
			return $isCodeExisted;
		}
		
		function Get_Max_Display_Order()
		{
			$sql = '';
			$sql .= ' SELECT max(DisplayOrder) FROM '.$this->TableName;
			$sql .= ' WHERE BuildingID = \''.$this->BuildingID.'\' ';
			$MaxDisplayOrder = $this->returnVector($sql);
			
			return $MaxDisplayOrder[0];
		}
		
		function Get_Last_Modified_Info($BuildingID="")
		{
			if ($this->LocationLevelID != '')
			{
				# self last modified info
				$sql = '';
				$sql .= ' SELECT * FROM '.$this->TableName;
				$sql .= ' WHERE LocationLevelID = \''.$this->LocationLevelID.'\' ';
			}
			else
			{
				# all floors last modified info
				$buildingCond = '';
				if ($BuildingID != "")
					$buildingCond = ' WHERE BuildingID = \''.$BuildingID.'\' ';
					
				$sql = '';
				$sql .= ' SELECT * ';
				$sql .= ' FROM '.$this->TableName;
				$sql .= ' WHERE DateModified IN 
									(	
										SELECT MAX(DateModified) 
										FROM '.$this->TableName.'
										'.$buildingCond.'
									)
						';
			}
			
			$returnArr = $this->returnArray($sql);
			return $returnArr;
		}
		
		function Is_Barcode_Existed($TargetBarcode, $ExcludeID='', $BuildingID='')
		{
			$TargetBarcode = $this->Get_Safe_Sql_Query(trim($TargetBarcode));
			
			$excludeFloorCond = '';
			if ($ExcludeID != '')
			{
				$excludeFloorCond = " AND floorTable.LocationLevelID != '$ExcludeID' ";
				
				$FloorObj = new Floor($ExcludeID);
				$BuildingID = $FloorObj->BuildingID;
			}
			
			$buildingTable = $this->DBName.".INVENTORY_LOCATION_BUILDING";
			$buildingCond = '';
			if ($BuildingID != '')
				$buildingCond = " AND buildingTable.BuildingID = '$BuildingID' ";
				
			
			# Check Floor Code
			$floorTable = $this->DBName.".INVENTORY_LOCATION_LEVEL";
			$sql = "	SELECT 
									DISTINCT(floorTable.LocationLevelID) 
							FROM 
									".$this->TableName." as floorTable
									Inner Join
									".$buildingTable." as buildingTable
									On (floorTable.BuildingID = buildingTable.BuildingID)
							WHERE 
									floorTable.Barcode = '".$TargetBarcode."' 
									AND 
									floorTable.RecordStatus = '1'
									$excludeFloorCond
									$buildingCond
							";
			$floorArr = $this->returnVector($sql);
			$isCodeExisted = (count($floorArr)==0)? false : true;
			
			return $isCodeExisted;
		}
		
		function getRecordID()
		{
		    return $this->RecordID;
		}
		
	}
	
	
	
	## Room Class
	class Room extends Floor {
		var $LocationID;
		var $Description;
		var $BuildingName;
		var $FloorName;
		var $RoomName;
		var $Capacity;
		
		function Room($LocationID="", $LocationLevelID="", $GetFullInfo=true){
			parent:: Floor();
			$this->TableName = $this->DBName.".INVENTORY_LOCATION";
			$this->ID_FieldName = "LocationID";
			
			if ($LocationLevelID!="")
				$this->LocationLevelID = $LocationLevelID;
			
			if ($LocationID!="" && $LocationID > 0)
			{
				$this->RecordID = $LocationID;
				$this->Get_Self_Info();
				
				if ($GetFullInfo)
					$this->Get_Room_Location_Details();
			}
		}
		
		function Get_All_Room($includeInactive=0)
		{
			$statusCond = "";
			if (!$includeInactive)
				$statusCond = " WHERE RecordStatus = '".LOCATION_STATUS_ACTIVE."' ";
				
			$sql = '';
			$sql .= "SELECT * FROM ".$this->TableName;
			$sql .= $statusCond;
			$sql .= " ORDER BY DisplayOrder";
			$resultSet = $this->returnArray($sql);
			
			return $resultSet;
		}
		
		function Is_Record_Existed($Code, $NameEng='', $NameChi='', $RecordStatus='', $LocationLevelID='', $BuildingID='', $Barcode='')
		{

		    $NameEngCond = '';
			if ($NameEng != '')
				$NameEngCond = " And NameEng = '".$this->Get_Safe_Sql_Query($NameEng)."' ";
				
			$NameChiCond = '';
			if ($NameChi != '')
				$NameChiCond = " And NameChi = '".$this->Get_Safe_Sql_Query($NameChi)."' ";
				
			$RecordStatusCond = '';
			if ($RecordStatus != '')
				$RecordStatusCond = " And RecordStatus = '".$this->Get_Safe_Sql_Query($RecordStatus)."' ";

			$LocationLevelIDCond = '';
			if ($LocationLevelID!= '')
				$LocationLevelIDCond = " And LocationLevelID= '".$this->Get_Safe_Sql_Query($LocationLevelID)."' ";
				
			if ($BuildingID!= '')
				$BuildingIDCond = " And BuildingID = '".$this->Get_Safe_Sql_Query($BuildingID)."' ";

			if ($Barcode!= '')
			    $BarcodeCond = " AND Barcode like '".$this->Get_Safe_Sql_Query($Barcode)."' ";
				
			$floorCond = "";
			if ($this->LocationLevelID != '')
				$floorCond = " AND LocationLevelID = '".$this->LocationLevelID."'";
			$sql = "SELECT ".$this->ID_FieldName."
					FROM ".$this->TableName."
					WHERE Code = '".$this->Get_Safe_Sql_Query($Code)."'
                        ".$NameEngCond."
                        ".$floorCond."
						".$RecordStatusCond."
                        ".$LocationLevelIDCond."
                        ".$BarcodeCond."";
			
			//debug_pr($sql);
			
			$resultSet = $this->returnVector($sql);

			if (count($resultSet) == 0)
				return false;
			else
				return $resultSet[0];
		}
		
		### Check if the Code is duplicated within the same Building and within the same Floor
		function Is_Code_Existed($TargetCode, $ExcludeID='', $BuildingID='', $LocationLevelID='')
		{
			$TargetCode = $this->Get_Safe_Sql_Query(trim($TargetCode));
			
			$excludeRoomCond = '';
			if ($ExcludeID != '')
			{
				$excludeRoomCond = " AND roomTable.LocationID != '$ExcludeID' ";
				
				$RoomObj = new Room($ExcludeID);
				$LocationLevelID = $RoomObj->LocationLevelID;
				
				$FloorObj = new Floor($LocationLevelID);
				$BuildingID = $FloorObj->BuildingID;
			}
			
			$buildingTable = $this->DBName.".INVENTORY_LOCATION_BUILDING";
			$buildingCond = '';
			if ($BuildingID != '')
				$buildingCond = " AND buildingTable.BuildingID = '$BuildingID' ";
				
			$floorTable = $this->DBName.".INVENTORY_LOCATION_LEVEL";
			$floorCond = '';
			if ($LocationLevelID != '')
				$floorCond = " AND floorTable.LocationLevelID = '$LocationLevelID' ";
				
			# Check Room Code
			$sql = "SELECT 
							DISTINCT(roomTable.LocationID) 
					FROM 
							".$this->TableName." as roomTable
							Inner Join
							".$floorTable." as floorTable
							On (roomTable.LocationLevelID = floorTable.LocationLevelID)
							Inner Join
							".$buildingTable." as buildingTable
							On (floorTable.BuildingID = buildingTable.BuildingID)
					WHERE 
							roomTable.Code = '".$TargetCode."' 
							AND 
							roomTable.RecordStatus = '1'
							$excludeRoomCond
							$buildingCond
							$floorCond
					";
			$roomArr = $this->returnVector($sql);
			$isCodeExisted = (count($roomArr)==0)? false : true;
			
			return $isCodeExisted;
		}
		
		function Get_Max_Display_Order()
		{
			$sql = '';
			$sql .= ' SELECT max(DisplayOrder) FROM '.$this->TableName;
			$sql .= ' WHERE LocationLevelID = \''.$this->LocationLevelID.'\' ';
			$MaxDisplayOrder = $this->returnVector($sql);
			
			return $MaxDisplayOrder[0];
		}
		
		function Get_Last_Modified_Info($LocationLevelID="")
		{
			if ($this->LocationID != '')
			{
				# self last modified info
				$sql = '';
				$sql .= ' SELECT * FROM '.$this->TableName;
				$sql .= ' WHERE LocationID = \''.$this->LocationID.'\' ';
			}
			else
			{
				# all rooms last modified info
				$floorCond = '';
				if ($LocationLevelID != "")
					$floorCond = ' AND LocationLevelID = \''.$LocationLevelID.'\' ';
					
				$sql = '';
				$sql .= ' SELECT * ';
				$sql .= ' FROM '.$this->TableName;
				$sql .= ' WHERE DateModified IN 
								(SELECT MAX(DateModified) FROM '.$this->TableName.')';
				$sql .= $floorCond;
			}
			
			$returnArr = $this->returnArray($sql);
			return $returnArr;
		}
		
		function Check_Can_Delete()
		{
			######################################################
			######################################################
			######################################################
			#
			# Current checking for:
			#	1. Timetable
			#	2. location is used in School Holiday / Event
			#	3. eInventory > any item belongs to the room
			#
			######################################################
			######################################################
			######################################################
			
			# Check if the Room has been linked to other modules of CURRENT academic year only
			$curAcademicYearID = Get_Current_Academic_Year_ID();
			
			/* In case you need to check the record by terms in the academic year,
				activate the below code to get the YearTermID array and list
			$obj_AcademicYear = new academic_year($curAcademicYearID);
			$YearTermArr = $obj_AcademicYear->Get_Term_List(0);
			$numOfTerm = count($YearTermArr);
			
			# No Term Set up yet => can delete
			if ($numOfTerm == 0)
				return true;
			
			# Build YearTermID Array and List
			$YearTermIDArr = array();
			for ($i=0; $i<$numOfTerm; $i++)
				$YearTermIDArr[] = $YearTermArr[$i]['YearTermID'];
			$YearTermIDList = implode(', ', $YearTermIDArr);
			*/
			
			
			### Module Data Checking Start ###
			# Timetable
			$sql = "SELECT 
							COUNT(itra.LocationID)
					FROM
							INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
							INNER JOIN
							INTRANET_SCHOOL_TIMETABLE as ist
							ON 
							(
								itra.TimetableID = ist.TimetableID
								AND
								itra.LocationID = '".$this->LocationID."'
								AND
								ist.AcademicYearID = '$curAcademicYearID'
							)
					";
			$timeTableResultSet = $this->returnVector($sql);
			$timetableCount = $timeTableResultSet[0];
			if ($timetableCount > 0)
				return false;
			### Module Data Checking End ###
			
			### Check any location is used in School Holiday / Event ###
			$sql = "SELECT MIN(TermStart), MAX(TermEnd) FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$curAcademicYearID'";
			$result = $this->returnArray($sql,2);
			if(sizeof($result)>0){
				for($i=0; $i<sizeof($result); $i++)
				{
					list($term_start,$term_end) = $result[$i];
					$sql = "SELECT 
								count(event.EventID)
							FROM
								INTRANET_EVENT AS event INNER JOIN
								INVENTORY_LOCATION AS location ON (event.EventLocationID = location.LocationID)
							WHERE
								(event.EventDate between '$term_start' and '$term_end') AND 
								location.LocationID = '".$this->LocationID."'";
					$result2 = $this->returnVector($sql);
					if($result2[0]>0)
					{
						$CheckHoliday++;
					}
				}
			}
			if($CheckHoliday > 0)
				return false;
			### End of checking for Holiday / Event ###
			
			### Check any location is used in eInventory ###
			# single item
			$sql3 = "select count(a.ItemID)
					from 
						INVENTORY_ITEM_SINGLE_EXT as a
						left join INVENTORY_ITEM as b on b.ItemID=a.ItemID and b.RecordStatus=1
					where 
						a.LocationID='{$this->LocationID}'";
			$result3 = $this->returnVector($sql3);
			if($result3[0] > 0)	return false;
			
			# bulk item
			if(!$result3[0])
			{
				$sql3 = "select count(a.ItemID)
					from 
						INVENTORY_ITEM_BULK_LOCATION as a
						left join INVENTORY_ITEM as b on b.ItemID=a.ItemID and b.RecordStatus=1
					where 
						a.LocationID='{$this->LocationID}'";
				$result3 = $this->returnVector($sql3);
				if($result3[0] > 0)	return false;
			}
			
			### End of checking for eInventory ###
			
			# Pass all checkings => can delete
			return true;
		}
		
		function Get_Room_Location_Details()
		{
			/*
			$LocationInfoArr = $this->Get_Building_Floor_Room_Name_Arr();
			
			$thisLocationInfoArr = $LocationInfoArr[$this->RecordID];
			$this->BuildingName = Get_Lang_Selection($thisLocationInfoArr['BuildingNameChi'], $thisLocationInfoArr['BuildingNameEng']);
			$this->FloorName = Get_Lang_Selection($thisLocationInfoArr['FloorNameChi'], $thisLocationInfoArr['FloorNameEng']);
			$this->RoomName = Get_Lang_Selection($thisLocationInfoArr['RoomNameChi'], $thisLocationInfoArr['RoomNameEng']);
			*/
			
			$sql = "Select
							building.BuildingID,
							building.NameChi as BuildingNameChi,
							building.NameEng as BuildingNameEng,							
							floor.LocationLevelID,
							floor.NameChi as FloorNameChi,
							floor.NameEng as FloorNameEng,
							room.NameChi as RoomNameChi,
							room.NameEng as RoomNameEng,
							room.Capacity
					From 
							INVENTORY_LOCATION as room
							Inner Join
							INVENTORY_LOCATION_LEVEL as floor
							On (room.LocationLevelID = floor.LocationLevelID)
							Inner Join
							INVENTORY_LOCATION_BUILDING as building
							On (floor.BuildingID = building.BuildingID)
					Where
							room.LocationID = '".$this->RecordID."'
					";
			$InfoArr = $this->returnArray($sql);
			
			$this->BuildingID =  $InfoArr[0]['BuildingID'];
			$this->BuildingName = Get_Lang_Selection($InfoArr[0]['BuildingNameChi'], $InfoArr[0]['BuildingNameEng']);
			$this->LocationLevelID =  $InfoArr[0]['LocationLevelID'];
			$this->FloorName = Get_Lang_Selection($InfoArr[0]['FloorNameChi'], $InfoArr[0]['FloorNameEng']);
			$this->RoomName = Get_Lang_Selection($InfoArr[0]['RoomNameChi'], $InfoArr[0]['RoomNameEng']);
			$this->Capacity = $InfoArr[0]['Capacity'];
		}
		
		function Is_Barcode_Existed($TargetBarcode, $ExcludeID='', $BuildingID='', $LocationLevelID='')
		{
			$TargetBarcode = $this->Get_Safe_Sql_Query(trim($TargetBarcode));
			
			$excludeRoomCond = '';
			if ($ExcludeID != '')
			{
				$excludeRoomCond = " AND roomTable.LocationID != '$ExcludeID' ";
				
				$RoomObj = new Room($ExcludeID);
				$LocationLevelID = $RoomObj->LocationLevelID;
				
				$FloorObj = new Floor($LocationLevelID);
				$BuildingID = $FloorObj->BuildingID;
			}
			
			$buildingTable = $this->DBName.".INVENTORY_LOCATION_BUILDING";
			$buildingCond = '';
			if ($BuildingID != '')
				$buildingCond = " AND buildingTable.BuildingID = '$BuildingID' ";
				
			$floorTable = $this->DBName.".INVENTORY_LOCATION_LEVEL";
			$floorCond = '';
			if ($LocationLevelID != '')
				$floorCond = " AND floorTable.LocationLevelID = '$LocationLevelID' ";
				
			# Check Room Code
			$sql = "SELECT 
							DISTINCT(roomTable.LocationID) 
					FROM 
							".$this->TableName." as roomTable
							Inner Join
							".$floorTable." as floorTable
							On (roomTable.LocationLevelID = floorTable.LocationLevelID)
							Inner Join
							".$buildingTable." as buildingTable
							On (floorTable.BuildingID = buildingTable.BuildingID)
					WHERE 
							roomTable.Barcode = '".$TargetBarcode."' 
							AND 
							roomTable.RecordStatus = '1'
							$excludeRoomCond
							$buildingCond
							$floorCond
					";
			$roomArr = $this->returnVector($sql);
			$isCodeExisted = (count($roomArr)==0)? false : true;
			
			return $isCodeExisted;
		}
	
		function getRecordID()
		{
		    return $this->RecordID;
		}
		
	}

} // End of directives
?>