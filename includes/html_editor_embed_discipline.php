<?php

if (!$is_included_before)
{
        function genButton($btn_name, $btn_action){
                $rx = "<button class='btnA' onClick=\"$btn_action\" onmouseover=\"if(this.className=='btnA'){this.className='btnAOver'}\" onmouseout=\"if(this.className=='btnAOver'){this.className='btnA'}\" unselectable='on'>&nbsp; $btn_name &nbsp;</button>";
                return $rx;
        }

        //$host = ($_SERVER["HTTP_HOST"]!="") ? $_SERVER["HTTP_HOST"] : getenv("HTTP_HOST");
        //$ecHTTP = "http://$host";

        if ($editor_width=="")
        {
                $editor_width = "100%";
        }
        else
        {
            if (substr($editor_width,-1,1)!='%')
            {
                $editor_width = ($equation_editor_js!="") ? ((int) $editor_width) + 10 : ((int) $editor_width) - 10;
                $editor_width .= "px";
            }
        }

        if ($editor_height=="")
        {
            $editor_height = "200px";
        }
        else
        {
            if (substr($editor_width,-2,2)!='px')
            {
                $editor_height .= "px";
            }
        }
}
?>

<?php
if (!$is_included_before)
{
        switch ($intranet_session_language)
        {
                case "en":
                                $lang_used = "eng";
                                break;
                case "b5":
                                $lang_used = "chib5";
                                break;
                case "gb":
                                $lang_used = "chigb";
                                break;
        }

?>
<style type="text/css"><!--
  .subhead  { font-family: arial, arial; font-size: 18px; font-weight: bold; font-style: italic; }
  .backtotop     { font-family: arial, arial; font-size: xx-small;  }
  .code     { background-color: #EEEEEE; font-family: Courier New; font-size: x-small;
              margin: 5px 0px 5px 0px; padding: 5px;
              border: black 1px dotted;
            }
            
  .btnA     { height: 21px; border: 1px solid #FFFFFF; background-color: #CEECFF; margin: 0; padding: 0; }
  .btnAOver { height: 21px; border: 1px outset #FFFFFF; background-color: #F6FAFF;}
  .btnADown { height: 21px; border: 1px inset #FFFFFF; background-color: #89C7FF; }

#ToolMenu{position:absolute; top: 0px; left: 0px; z-index:2; visibility:hidden;}
--></style>



<script language="JavaScript" type="text/javascript" src="<?=$eclass_url_root?>/src/includes/js/tooltip.js"></script>
<script language="Javascript"><!-- // load htmlarea
/* Eclass Junior use <?=$eclass_httppath?> as "_editor_url" */
var ecHTTP = "<?=$ecHTTP?>";
/* ---- end of comment */

_editor_url = "<?=$eclass_url_root?>/";                     // URL to htmlarea files
var win_ie_ver = parseFloat(navigator.appVersion.split("MSIE")[1]);
if (navigator.userAgent.indexOf('Mac')        >= 0) { win_ie_ver = 0; }
if (navigator.userAgent.indexOf('Windows CE') >= 0) { win_ie_ver = 0; }
if (navigator.userAgent.indexOf('Opera')      >= 0) { win_ie_ver = 0; }


if (win_ie_ver >= 5.5) {
  document.write('<scr' + 'ipt src="' +_editor_url+ 'src/includes/js/html_editor2_edis.php?lang_used=<?=$lang_used?>&CategoryID=<?=$CategoryID?>&Title=<?=$i_Discipline_System_Discipline_Template_Auto_Fillin?>"');
  document.write(' language="Javascript1.2"></scr' + 'ipt>');
} else { document.write('<br><br><br><br><br>Please upgrade to <b>IE 6.0 or above</b> in order to use the HTML editor!'); }




var diagObj = null;
function browseFile(myAtt, dObj){
        diagObj = dObj;
        var url = "<?=$admin_url_path?>/src/course/resources/files/index.php?category=<?=$categoryID?>&attach=1&isImageOnly=1&attachment=" + myAtt;
        newWindow(url,1);
}

function getFilePath(rPath){
        diagObj.value = rPath;
        return;
}

function browseFileLink(myAtt, dObj){
        diagObj = dObj;
        var url = "<?=$admin_url_path?>/src/course/resources/files/index.php?category=<?=$categoryID?>&attach=1&fieldname=html_editor_diagObj&attachment=" + myAtt;
        newWindow(url,1);
}
// --></script>


<div id="ToolMenu"></div>
<?php
}
?>


<table border='0' cellpadding='1' cellspacing='0' width='100%'>
<tr><!--

==================================Updated!!!!!!!!!!!!==================================================================

<td bgcolor='white' style="border-left:solid 1px #8899FF; border-top:solid 1px #8899FF; border-right:solid 1px #8899FF; border-bottom:solid 1px #8899FF; ">-->
<td bgcolor='white' style="border-left:solid 1px #a5acb2; border-top:solid 1px #a5acb2; border-right:solid 1px #a5acb2; border-bottom:solid 1px #a5acb2; ">
<textarea name="<?=$obj_name?>" cols="70">
<?= $init_html_content ?>
</textarea></td></tr>
</table>

<div style='position:absolute; visibility:hidden'>
<textarea name='hidden_text'><?=$hidden_text?></textarea>
</div>

<script language="javascript1.2">

isMenu = true;
toolLeftAdd = -8;
toolTopAdd = 12;

var config = new Object();    // create new config object

config.width = "<?=$editor_width?>";
config.height = "<?=$editor_height?>";
config.bodyStyle = 'background-color: white; font-family: "Verdana"; font-size: x-small;';

// NOTE:  You can remove any of these blocks and use the default config!

config.toolbar = [
    ['fontname','fontsize','separator','HorizontalRule','Createlink' <?=$insert_image_js?>, 'InsertTable' <?=$equation_editor_js?>],
    ['linebreak'],
    ['undo','redo','separator','bold','italic','underline','strikethrough','subscript','superscript','separator','forecolor','backcolor','separator','justifyleft','justifycenter','justifyright','justifyfull','separator','OrderedList','UnOrderedList','Outdent','Indent']
];

var isInitFocus = false;
var isTextMode = "<?=$isTextMode?>";
menuPos = [null, null];
editor_generate('<?=$obj_name?>',config);

</script>

<?php
if ($html_opt_name!="")
{
        echo "<input type='hidden' name='$html_opt_name' value='1'>";
}

$is_included_before = true;
?>