<?php
#  Editing by 
/* ***************************************************
 * Modification Log
 * 20201030 Bill    [2020-0708-0950-12308]
 *      - modified Get_Class_Teacher_Comment_Csv_Header_Info(), set comment column data to reference type   ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment_as_BilingualTeacher'])
 *      - modified Get_Class_Teacher_Comment_Record(), to return additional comment
 *
 * 20130722 Ivan
 * 		- modified function Update_Class_Teacher_Comment(), added to update last modified user in the UPDATE sql statement 
 * 
 * 20130722 Roy
 * 		- added function Get_Class_Teacher_Comment_Csv_Header_Info()
 * 		- added function Get_Class_Teacher_Comment_Csv_Header_Title()
 * 		- added function Get_Class_Teacher_Comment_Csv_Header_Property()
 * 
 * 		- added function Delete_Import_Temp_Class_Teacher_Comment_Data()
 * 		- added function Insert_Import_Temp_Class_Teacher_Comment_Data()
 * 		- added function Get_Temp_Class_Teacher_Comment_Data()
 * 
 * 		- added function Get_Class_Teacher_Comment_Record()
 * 		- added function Update_Class_Teacher_Comment()
 *
 */
if (!defined("LIBREPORTCARD_COMMENT"))         // Preprocessor directives
{
	define("LIBREPORTCARD_COMMENT", true);
	class libreportcard_comment extends libreportcardcustom {
		
		public function __construct() {
			$this->libreportcardcustom();
		}
		
		public function Get_Comment_MaxLength_Info($ReportID, $SubjectIDArr='') {
			if ($SubjectIDArr !== '') {
				$conds_SubjectID = " And SubjectID In ('".implode("','", (array)$SubjectIDArr)."') ";
			}
			
			$RC_COMMENT_MAX_LENGTH = $this->DBName.".RC_COMMENT_MAX_LENGTH";
			$sql = "Select
							SettingsID, ReportID, SubjectID, MaxLength
					From
							$RC_COMMENT_MAX_LENGTH
					Where
							ReportID = '".$ReportID."'
							$conds_SubjectID
					";
			return BuildMultiKeyAssoc($this->returnResultSet($sql), 'SubjectID');
		}
		
		public function Save_Comment_MaxLength($ReportID, $SubjectID, $MaxLength) {
			$RC_COMMENT_MAX_LENGTH = $this->DBName.".RC_COMMENT_MAX_LENGTH";
			
			$sql = "Select SettingsID From $RC_COMMENT_MAX_LENGTH Where ReportID = '".$ReportID."' And SubjectID = '".$SubjectID."'";
			$infoArr = $this->returnResultSet($sql);
			$SettingsID = $infoArr[0]['SettingsID'];
			if ($SettingsID == '') {
				// insert
				$sql = "Insert Into	$RC_COMMENT_MAX_LENGTH
							(ReportID, SubjectID, MaxLength, InputDate, InputBy, ModifiedDate, ModifiedBy)
						Values
							('".$ReportID."', '".$SubjectID."', '".$MaxLength."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."')
						";
				$success = $this->db_db_query($sql);
			}
			else {
				// update
				$sql = "Update $RC_COMMENT_MAX_LENGTH
						Set MaxLength = '".$MaxLength."', ModifiedDate = now(), ModifiedBy = '".$_SESSION['UserID']."'
						Where SettingsID = '".$SettingsID."'
						";
				$success = $this->db_db_query($sql);
			}
			return $success;
		}
		
		public function Copy_Comment_Length_Settings($fromReportID, $toReportID) {
			$maxLengthArr = $this->Get_Comment_MaxLength_Info($fromReportID);
			
			$successArr = array();
			foreach((array)$maxLengthArr as $_subjectID => $_maxLengthArr) {
				$_maxLength = $_maxLengthArr['MaxLength'];
				$successArr[$_subjectID] = $this->Save_Comment_MaxLength($toReportID, $_subjectID, $_maxLength);
			}
			
			return (in_array(false, $successArr))? false : true;
		}
		
		
		public function Get_Class_Teacher_Comment_Csv_Header_Info() {
			global $eRCTemplateSetting, $Lang, $eReportCard;
			
			$InfoArr = array();
			$InfoArr[] = array("Property" => 1,
							"En" => $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['WebSAMSRegNo'],
							"Ch" => $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['WebSAMSRegNo'],
							);
			$InfoArr[] = array("Property" => 1,
							"En" => $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['ClassName'],
							"Ch" => $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['ClassName'],
							);
			$InfoArr[] = array(	"Property" => 1,
							"En" => $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['ClassNumber'],
							"Ch" => $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['ClassNumber'],
							);
			$InfoArr[] = array(	"Property" => 2,
							"En" => $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['StudentName'],
							"Ch" => $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['StudentName'],
							);
			$InfoArr[] = array(	"Property" => 1,
							"En" => $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['CommentContent'],
							"Ch" => $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['CommentContent'],
							);
			if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
				$InfoArr[] = array(	"Property" => 2,
							"En" => $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['AdditionalComment'],
							"Ch" => $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['AdditionalComment'],
							);
			}
			if ($eRCTemplateSetting['ClassTeacherComment_Conduct']) {
				$InfoArr[] = array(	"Property" => 1,
							"En" => $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['Conduct'],
							"Ch" => $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['Conduct'],
							);
			}

            // [2020-0708-0950-12308] Comment column data - Reference field(s)
            if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment_as_BilingualTeacher'])
            {
                $InfoArr[4]['Property'] = 2;
            }
			
			return $InfoArr;
		}
		
		public function Get_Class_Teacher_Comment_Csv_Header_Title()
		{
			$InfoArr = $this->Get_Class_Teacher_Comment_Csv_Header_Info();
			
			$ColumnTitleArr = array();
			$ColumnTitleArr['En'] = Get_Array_By_Key($InfoArr, 'En');
			$ColumnTitleArr['Ch'] = Get_Array_By_Key($InfoArr, 'Ch');
			
			return $ColumnTitleArr;
		}
		
		public function Get_Class_Teacher_Comment_Csv_Header_Property($forGetAllCsvContent=0)
		{
			global $oea_cfg;
			$PropertyArr = Get_Array_By_Key($this->Get_Class_Teacher_Comment_Csv_Header_Info(), 'Property');
			
			if ($forGetAllCsvContent == 1)
			{
				$numOfProperty = count($PropertyArr);
				for ($i=0; $i<$numOfProperty; $i++) {
					$PropertyArr[$i] = 1;
				}
			}
			
			return $PropertyArr;
		}
		
		public function Delete_Import_Temp_Class_Teacher_Comment_Data($ReportID)
		{
			$RC_TEMP_CLASS_TEACHER_COMNMENT = $this->DBName.'.RC_TEMP_IMPORT_CLASS_TEACHER_COMMENT';
			$sql = "Delete From $RC_TEMP_CLASS_TEACHER_COMNMENT Where ImportUserID = '".$_SESSION["UserID"]."' And ReportID = '".$ReportID."'";
			$Success = $this->db_db_query($sql);
			return $Success;
		}
		
		
		public function Insert_Import_Temp_Class_Teacher_Comment_Data($InsertValueArr)
		{
			if (count((array)$InsertValueArr) > 0)
			{
				$RC_TEMP_CLASS_TEACHER_COMNMENT = $this->DBName.'.RC_TEMP_IMPORT_CLASS_TEACHER_COMMENT';
				$sql = "Insert Into $RC_TEMP_CLASS_TEACHER_COMNMENT
							(ImportUserID, RowNumber, ClassName, ClassNumber, WebSAMSRegNo, StudentID, StudentName, CommentContent, AdditionalComment, Conduct, ReportID, InputDate)
						Values
							".implode(',', (array)$InsertValueArr)."
						";
				return $this->db_db_query($sql);
			}
			else
			{
				return false;
			}
		}
		
		public function Get_Temp_Class_Teacher_Comment_Data($ReportID, $RowNumberArr='') 
		{
			$conds_RowNumber = '';
			if ($RowNumberArr != '') {
				$conds_RowNumber = " And RowNumber In ('".implode("','", (array)$RowNumberArr)."') ";
			}
			
			$RC_TEMP_CLASS_TEACHER_COMNMENT = $this->DBName.'.RC_TEMP_IMPORT_CLASS_TEACHER_COMMENT';
			$sql = "Select * From $RC_TEMP_CLASS_TEACHER_COMNMENT Where ImportUserID = '".$_SESSION["UserID"]."' And ReportID = '".$ReportID."' $conds_RowNumber";
			return $this->returnArray($sql, null, 1);
		}
		
		
		public function Get_Class_Teacher_Comment_Record($ReportIDArr='', $StudentIDArr='', $SubjectID='', $ReturnAsso=1)
		{
			if ($ReportIDArr != '') {
				$conds_ReportID = " And ReportID In ('".implode("','", (array)$ReportIDArr)."') ";
			}
			if ($StudentIDArr != '') {
				$conds_StudentID = " And StudentID In ('".implode("','", (array)$StudentIDArr)."') ";
			}
			
			$RC_MARKSHEET_COMMENT = $this->DBName.".RC_MARKSHEET_COMMENT";
			$sql = "Select
							CommentID,
							ReportID,
							StudentID,
							Comment,
							AdditionalComment
					From
							$RC_MARKSHEET_COMMENT
					Where
							1
							$conds_ReportID
							$conds_StudentID
							And SubjectID = ''
					";
			$InfoArr = $this->returnArray($sql, null, 1);
			if ($ReturnAsso) {
				$ReturnArr = BuildMultiKeyAssoc($InfoArr, array('ReportID', 'StudentID'));
			}
			else {
				$ReturnArr = $InfoArr;
			}
			return $ReturnArr;
		}
	
		public function Update_Class_Teacher_Comment($ReportID, $DataArr, $UpdateLastModified=0)
		{
			if (count((array)$DataArr) == 0 ) {
				return false;
			}
			
			$this->Start_Trans();
			
			$RC_MARKSHEET_COMMENT = $this->DBName.".RC_MARKSHEET_COMMENT";
			$valueArr = array();
			$numOfStudent = count($DataArr);
			for ($i=0; $i<$numOfStudent; $i++) {
				$thisCommentID = $DataArr[$i]['CommentID']; 
				
				if ($thisCommentID == '') {
					// insert
					$_fieldArr = array();
					$_valueArr = array();
					$_fieldArr[] = 'ReportID';
					$_valueArr[] = $ReportID;
					foreach ($DataArr[$i] as $_field => $_value)
					{
						$_fieldArr[] = $_field;
						$_valueArr[] = "'".$this->Get_Safe_Sql_Query($_value)."'";
					}
					$_fieldArr[] = 'DateInput';
					$_valueArr[] = 'now()';
					$_fieldArr[] = 'TeacherID';
					$_valueArr[] = $_SESSION['UserID'];
					$_fieldArr[] = 'SubjectID';
					$_valueArr[] = "''";
					
					
					if ($UpdateLastModified) {
						$_fieldArr[] = 'DateModified';
						$_valueArr[] = 'now()';
					}
					
					$_fieldText = implode(", ", $_fieldArr);
					$_valueText = implode(", ", $_valueArr);
					
					### Insert new records
					$sql = "Insert Into $RC_MARKSHEET_COMMENT ( $_fieldText ) Values ( $_valueText )";
					$SuccessArr['Insert'][] = $this->db_db_query($sql);
				}
				else {
					// update
					$valueFieldText = '';
					foreach ($DataArr[$i] as $field => $value)
					{
						if ($field != 'CommentID')
							$valueFieldText .= $field." = '".$this->Get_Safe_Sql_Query($value)."', ";
					}
					
					if ($UpdateLastModified) {
						$valueFieldText .= ' DateModified = now(), TeacherID = \''.$_SESSION['UserID'].'\' ';
					}
					
					### Insert new records
					$sql = "Update $RC_MARKSHEET_COMMENT Set $valueFieldText Where CommentID = '".$thisCommentID."'";
					$SuccessArr['Update'][$thisCommentID] = $this->db_db_query($sql);
				}
			}
			
			if (in_multi_array(false, $SuccessArr))
			{
				$this->RollBack_Trans();
				return 0;
			}
			else
			{
				$this->Commit_Trans();
				return 1;
			}
		}
	}
	
	
	
}
?>