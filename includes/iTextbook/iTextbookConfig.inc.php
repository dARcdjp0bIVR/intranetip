<?php

/* Re-direct to home if $plugin['iTextbook'] is not set*/
if($plugin['iTextbook'] != true && !$Skip_iTextbook_Config_checking){
	header("Location: /");
}

$CurrentPageArr['iTextbook'] = 1;

/* Database definition */
$iTextbook_cfg['db_setting']['book']['chapter']['dilimeter']  = "|=|";
$iTextbook_cfg['db_setting']['book']['status']['deleted']     = 1;
$iTextbook_cfg['db_setting']['book']['status']['not_deleted'] = 0;
$iTextbook_cfg['db_setting']['book']['lang']['en'] = "en";
$iTextbook_cfg['db_setting']['book']['lang']['ch'] = "ch";

/* Config for amBook */
$iTextbook_cfg['ambook']['module_code']   = "ambook";
$iTextbook_cfg['ambook']['book_name'] 	  = "AmBook";
$iTextbook_cfg['ambook']['instruction']   = "You could only assign 8, 16, or 24 licenses to each student. Unit 25 is a gift unit for student who have license assigned.";
$iTextbook_cfg['ambook']['remarks'] 	  = "All the license released must be re-assigned to other students within the same class.";
$iTextbook_cfg['ambook']['cooldown']	  = 7; // in days 
$iTextbook_cfg['ambook']['TotalNoOfUnit'] = 25;
$iTextbook_cfg['ambook']['allowSelUnits'] = array(8, 16, 24);
$iTextbook_cfg['ambook']['SpecialUnit']	  = array(0=>1);
$iTextbook_cfg['ambook']['DisableUnit']	  = array();
$iTextbook_cfg['ambook']['BookLang']	  = array("en", "ch"); 	// Available Language for Book
$iTextbook_cfg['ambook']['DefaultLang']	  = "en"; 				// Default Language of Book

$iTextbook_cfg['ambook']['LicenseType']['ChapterStudent'] = true;
$iTextbook_cfg['ambook']['LicenseType']['HaveExpiryDate'] = false;

$iTextbook_cfg['ambook']['AllowCreateBook'] = true;

$iTextbook_cfg['ambook']['MaxChapters']		= 8;

$iTextbook_cfg['ambook']['ChapterName'][1]  = array("en"=>"Introduction to Science",
													"ch"=>"科學入門");
$iTextbook_cfg['ambook']['ChapterName'][2]  = array("en"=>"Measuring Length and Area",
													"ch"=>"長度和面積的量度");
$iTextbook_cfg['ambook']['ChapterName'][3]  = array("en"=>"Measuring Volume, Mass, Density & Temperature",
													"ch"=>"體積、質量、密度和溫度的量度");
$iTextbook_cfg['ambook']['ChapterName'][4]  = array("en"=>"Measuring Time, Rate and Speed",
													"ch"=>"時間、率和速率的量度");
$iTextbook_cfg['ambook']['ChapterName'][5]  = array("en"=>"Elements, Compounds and Mixtures",
													"ch"=>"元素、化合物和混合物");
$iTextbook_cfg['ambook']['ChapterName'][6]  = array("en"=>"Separation of Mixtures",
													"ch"=>"混合物的分離");
$iTextbook_cfg['ambook']['ChapterName'][7]  = array("en"=>"Solutions and Suspensions",
													"ch"=>"溶液及懸濁液");
$iTextbook_cfg['ambook']['ChapterName'][8]  = array("en"=>"Acids and Alkalis",
													"ch"=>"酸和鹼");
$iTextbook_cfg['ambook']['ChapterName'][9]  = array("en"=>"Diversity of Animals and Plants",
													"ch"=>"動物與植物的多樣性");
$iTextbook_cfg['ambook']['ChapterName'][10] = array("en"=>"Heat and its Effects",
													"ch"=>"熱及其影響");
$iTextbook_cfg['ambook']['ChapterName'][11] = array("en"=>"Forces",
													"ch"=>"力");
$iTextbook_cfg['ambook']['ChapterName'][12] = array("en"=>"Changes in Matter",
													"ch"=>"物質的變化");
$iTextbook_cfg['ambook']['ChapterName'][13] = array("en"=>"Energy Forms and Conversion",
													"ch"=>"能量形態及轉換");
$iTextbook_cfg['ambook']['ChapterName'][14] = array("en"=>"Light",
													"ch"=>"光");
$iTextbook_cfg['ambook']['ChapterName'][15] = array("en"=>"Electricity",
													"ch"=>"電學");
$iTextbook_cfg['ambook']['ChapterName'][16] = array("en"=>"Nutrition in Plants",
													"ch"=>"植物的食物");
$iTextbook_cfg['ambook']['ChapterName'][17] = array("en"=>"Respiration",
													"ch"=>"呼吸作用");
$iTextbook_cfg['ambook']['ChapterName'][18] = array("en"=>"Particulate Model of Matter",
													"ch"=>"物質的粒子模型");
$iTextbook_cfg['ambook']['ChapterName'][19] = array("en"=>"Atoms and Molecules",
													"ch"=>"原子和分子");
$iTextbook_cfg['ambook']['ChapterName'][20] = array("en"=>"Cells",
													"ch"=>"細胞");
$iTextbook_cfg['ambook']['ChapterName'][21] = array("en"=>"Nutrition in Humans",
													"ch"=>"人類的食物");
$iTextbook_cfg['ambook']['ChapterName'][22] = array("en"=>"Transport in Humans",
													"ch"=>"人類的運輸作用");
$iTextbook_cfg['ambook']['ChapterName'][23] = array("en"=>"Transport in Plants",
													"ch"=>"植物的運輸");
$iTextbook_cfg['ambook']['ChapterName'][24] = array("en"=>"The Earth's Water and Rivers",
													"ch"=>"地球上的水和河流");
$iTextbook_cfg['ambook']['ChapterName'][25] = array("en"=>"Pollution",
													"ch"=>"污染");

/* Config for Poems and Songs */
$iTextbook_cfg['poemsandsongs']['module_code'] 	 = "poemsandsongs";
$iTextbook_cfg['poemsandsongs']['book_name']   	 = "Poems and Songs";
$iTextbook_cfg['poemsandsongs']['instruction'] 	 = "";
$iTextbook_cfg['poemsandsongs']['remarks'] 	  	 = "";
$iTextbook_cfg['poemsandsongs']['cooldown']	  	 = 7; // in days 
$iTextbook_cfg['poemsandsongs']['TotalNoOfUnit'] = 1;
$iTextbook_cfg['poemsandsongs']['allowSelUnits'] = array(1);
$iTextbook_cfg['poemsandsongs']['SpecialUnit']   = array(0=>1);
$iTextbook_cfg['poemsandsongs']['DisableUnit']	 = array();
$iTextbook_cfg['poemsandsongs']['BookLang']	  	 = array("en"); 	// Available Language for Book
$iTextbook_cfg['poemsandsongs']['DefaultLang']	 = "en"; 				// Default Language of Book

$iTextbook_cfg['poemsandsongs']['LicenseType']['ChapterStudent'] = false;
$iTextbook_cfg['poemsandsongs']['LicenseType']['HaveExpiryDate'] = true;

$iTextbook_cfg['poemsandsongs']['AllowCreateBook'] = false;

$iTextbook_cfg['poemsandsongs']['MaxChapters']	 = 1;

$iTextbook_cfg['poemsandsongs']['ChapterName'][1]  = array("en"=>"Poems and Songs");
		
/* Config for Global Voices 1*/
$iTextbook_cfg['gvlistening']['module_code'] 	 = "gvlistening";
$iTextbook_cfg['gvlistening']['book_name']   	 = "Global Voices 1";
$iTextbook_cfg['gvlistening']['instruction'] 	 = "";
$iTextbook_cfg['gvlistening']['remarks'] 	  	 = "";
$iTextbook_cfg['gvlistening']['cooldown']	  	 = 7; // in days 
$iTextbook_cfg['gvlistening']['TotalNoOfUnit'] = 1;
$iTextbook_cfg['gvlistening']['allowSelUnits'] = array(1);
$iTextbook_cfg['gvlistening']['SpecialUnit']   = array(0=>1);
$iTextbook_cfg['gvlistening']['DisableUnit']	 = array();
$iTextbook_cfg['gvlistening']['BookLang']	  	 = array("en"); 	// Available Language for Book
$iTextbook_cfg['gvlistening']['DefaultLang']	 = "en"; 				// Default Language of Book

$iTextbook_cfg['gvlistening']['LicenseType']['ChapterStudent'] = false;
$iTextbook_cfg['gvlistening']['LicenseType']['HaveExpiryDate'] = true;

$iTextbook_cfg['gvlistening']['AllowCreateBook'] = false;

$iTextbook_cfg['gvlistening']['MaxChapters']	 = 1;

$iTextbook_cfg['gvlistening']['ChapterName'][1]  = array("en"=>"Global Voices 1");		

/* Config for Global Voices 2*/
$iTextbook_cfg['gvlistening2']['module_code'] 	 = "gvlistening2";
$iTextbook_cfg['gvlistening2']['book_name']   	 = "Global Voices 2";
$iTextbook_cfg['gvlistening2']['instruction'] 	 = "";
$iTextbook_cfg['gvlistening2']['remarks'] 	  	 = "";
$iTextbook_cfg['gvlistening2']['cooldown']	  	 = 7; // in days 
$iTextbook_cfg['gvlistening2']['TotalNoOfUnit'] = 1;
$iTextbook_cfg['gvlistening2']['allowSelUnits'] = array(1);
$iTextbook_cfg['gvlistening2']['SpecialUnit']   = array(0=>1);
$iTextbook_cfg['gvlistening2']['DisableUnit']	 = array();
$iTextbook_cfg['gvlistening2']['BookLang']	  	 = array("en"); 	// Available Language for Book
$iTextbook_cfg['gvlistening2']['DefaultLang']	 = "en"; 				// Default Language of Book

$iTextbook_cfg['gvlistening2']['LicenseType']['ChapterStudent'] = false;
$iTextbook_cfg['gvlistening2']['LicenseType']['HaveExpiryDate'] = true;

$iTextbook_cfg['gvlistening2']['AllowCreateBook'] = false;

$iTextbook_cfg['gvlistening2']['MaxChapters']	 = 1;

$iTextbook_cfg['gvlistening2']['ChapterName'][1]  = array("en"=>"Global Voices 2");

/* Config for Global Voices 3*/
$iTextbook_cfg['gvlistening3']['module_code'] 	 = "gvlistening3";
$iTextbook_cfg['gvlistening3']['book_name']   	 = "Global Voices 3";
$iTextbook_cfg['gvlistening3']['instruction'] 	 = "";
$iTextbook_cfg['gvlistening3']['remarks'] 	  	 = "";
$iTextbook_cfg['gvlistening3']['cooldown']	  	 = 7; // in days 
$iTextbook_cfg['gvlistening3']['TotalNoOfUnit'] = 1;
$iTextbook_cfg['gvlistening3']['allowSelUnits'] = array(1);
$iTextbook_cfg['gvlistening3']['SpecialUnit']   = array(0=>1);
$iTextbook_cfg['gvlistening3']['DisableUnit']	 = array();
$iTextbook_cfg['gvlistening3']['BookLang']	  	 = array("en"); 	// Available Language for Book
$iTextbook_cfg['gvlistening3']['DefaultLang']	 = "en"; 				// Default Language of Book

$iTextbook_cfg['gvlistening3']['LicenseType']['ChapterStudent'] = false;
$iTextbook_cfg['gvlistening3']['LicenseType']['HaveExpiryDate'] = true;

$iTextbook_cfg['gvlistening3']['AllowCreateBook'] = false;

$iTextbook_cfg['gvlistening3']['MaxChapters']	 = 1;

$iTextbook_cfg['gvlistening3']['ChapterName'][1]  = array("en"=>"Global Voices 3");
			
/* Config for Read to Succeed 1*/
$iTextbook_cfg['rsreading1']['module_code'] 	 = "rsreading1";
$iTextbook_cfg['rsreading1']['book_name']   	 = "Read to Succeed 1";
$iTextbook_cfg['rsreading1']['instruction'] 	 = "";
$iTextbook_cfg['rsreading1']['remarks'] 	  	 = "";
$iTextbook_cfg['rsreading1']['cooldown']	  	 = 7; // in days 
$iTextbook_cfg['rsreading1']['TotalNoOfUnit'] = 1;
$iTextbook_cfg['rsreading1']['allowSelUnits'] = array(1);
$iTextbook_cfg['rsreading1']['SpecialUnit']   = array(0=>1);
$iTextbook_cfg['rsreading1']['DisableUnit']	 = array();
$iTextbook_cfg['rsreading1']['BookLang']	  	 = array("en"); 	// Available Language for Book
$iTextbook_cfg['rsreading1']['DefaultLang']	 = "en"; 				// Default Language of Book
$iTextbook_cfg['rsreading1']['LicenseType']['ChapterStudent'] = false;
$iTextbook_cfg['rsreading1']['LicenseType']['HaveExpiryDate'] = true;
$iTextbook_cfg['rsreading1']['AllowCreateBook'] = false;
$iTextbook_cfg['rsreading1']['MaxChapters']	 = 1;
$iTextbook_cfg['rsreading1']['ChapterName'][1]  = array("en"=>"Read to Succeed 1");

/* Config for Read to Succeed 2*/
$iTextbook_cfg['rsreading2']['module_code'] 	 = "rsreading2";
$iTextbook_cfg['rsreading2']['book_name']   	 = "Read to Succeed 2";
$iTextbook_cfg['rsreading2']['instruction'] 	 = "";
$iTextbook_cfg['rsreading2']['remarks'] 	  	 = "";
$iTextbook_cfg['rsreading2']['cooldown']	  	 = 7; // in days 
$iTextbook_cfg['rsreading2']['TotalNoOfUnit'] = 1;
$iTextbook_cfg['rsreading2']['allowSelUnits'] = array(1);
$iTextbook_cfg['rsreading2']['SpecialUnit']   = array(0=>1);
$iTextbook_cfg['rsreading2']['DisableUnit']	 = array();
$iTextbook_cfg['rsreading2']['BookLang']	  	 = array("en"); 	// Available Language for Book
$iTextbook_cfg['rsreading2']['DefaultLang']	 = "en"; 				// Default Language of Book
$iTextbook_cfg['rsreading2']['LicenseType']['ChapterStudent'] = false;
$iTextbook_cfg['rsreading2']['LicenseType']['HaveExpiryDate'] = true;
$iTextbook_cfg['rsreading2']['AllowCreateBook'] = false;
$iTextbook_cfg['rsreading2']['MaxChapters']	 = 1;
$iTextbook_cfg['rsreading2']['ChapterName'][1]  = array("en"=>"Read to Succeed 2");

/* Config for Read to Succeed 3*/
$iTextbook_cfg['rsreading3']['module_code'] 	 = "rsreading3";
$iTextbook_cfg['rsreading3']['book_name']   	 = "Read to Succeed 3";
$iTextbook_cfg['rsreading3']['instruction'] 	 = "";
$iTextbook_cfg['rsreading3']['remarks'] 	  	 = "";
$iTextbook_cfg['rsreading3']['cooldown']	  	 = 7; // in days 
$iTextbook_cfg['rsreading3']['TotalNoOfUnit'] = 1;
$iTextbook_cfg['rsreading3']['allowSelUnits'] = array(1);
$iTextbook_cfg['rsreading3']['SpecialUnit']   = array(0=>1);
$iTextbook_cfg['rsreading3']['DisableUnit']	 = array();
$iTextbook_cfg['rsreading3']['BookLang']	  	 = array("en"); 	// Available Language for Book
$iTextbook_cfg['rsreading3']['DefaultLang']	 = "en"; 				// Default Language of Book
$iTextbook_cfg['rsreading3']['LicenseType']['ChapterStudent'] = false;
$iTextbook_cfg['rsreading3']['LicenseType']['HaveExpiryDate'] = true;
$iTextbook_cfg['rsreading3']['AllowCreateBook'] = false;
$iTextbook_cfg['rsreading3']['MaxChapters']	 = 1;
$iTextbook_cfg['rsreading3']['ChapterName'][1]  = array("en"=>"Read to Succeed 3");

/* Config for Grammar Touch 1*/
$iTextbook_cfg['grammartouch1']['module_code'] 	 = "grammartouch1";
$iTextbook_cfg['grammartouch1']['book_name']   	 = "Grammar Touch 1";
$iTextbook_cfg['grammartouch1']['instruction'] 	 = "";
$iTextbook_cfg['grammartouch1']['remarks'] 	  	 = "";
$iTextbook_cfg['grammartouch1']['cooldown']	  	 = 7; // in days 
$iTextbook_cfg['grammartouch1']['TotalNoOfUnit'] = 1;
$iTextbook_cfg['grammartouch1']['allowSelUnits'] = array(1);
$iTextbook_cfg['grammartouch1']['SpecialUnit']   = array(0=>1);
$iTextbook_cfg['grammartouch1']['DisableUnit']	 = array();
$iTextbook_cfg['grammartouch1']['BookLang']	  	 = array("en"); 	// Available Language for Book
$iTextbook_cfg['grammartouch1']['DefaultLang']	 = "en"; 				// Default Language of Book
$iTextbook_cfg['grammartouch1']['LicenseType']['ChapterStudent'] = false;
$iTextbook_cfg['grammartouch1']['LicenseType']['HaveExpiryDate'] = true;
$iTextbook_cfg['grammartouch1']['AllowCreateBook'] = false;
$iTextbook_cfg['grammartouch1']['MaxChapters']	 = 1;
$iTextbook_cfg['grammartouch1']['ChapterName'][1]  = array("en"=>"Grammar Touch 1");

/* Config for Grammar Touch 2*/
$iTextbook_cfg['grammartouch2']['module_code'] 	 = "grammartouch2";
$iTextbook_cfg['grammartouch2']['book_name']   	 = "Grammar Touch 2";
$iTextbook_cfg['grammartouch2']['instruction'] 	 = "";
$iTextbook_cfg['grammartouch2']['remarks'] 	  	 = "";
$iTextbook_cfg['grammartouch2']['cooldown']	  	 = 7; // in days 
$iTextbook_cfg['grammartouch2']['TotalNoOfUnit'] = 1;
$iTextbook_cfg['grammartouch2']['allowSelUnits'] = array(1);
$iTextbook_cfg['grammartouch2']['SpecialUnit']   = array(0=>1);
$iTextbook_cfg['grammartouch2']['DisableUnit']	 = array();
$iTextbook_cfg['grammartouch2']['BookLang']	  	 = array("en"); 	// Available Language for Book
$iTextbook_cfg['grammartouch2']['DefaultLang']	 = "en"; 				// Default Language of Book
$iTextbook_cfg['grammartouch2']['LicenseType']['ChapterStudent'] = false;
$iTextbook_cfg['grammartouch2']['LicenseType']['HaveExpiryDate'] = true;
$iTextbook_cfg['grammartouch2']['AllowCreateBook'] = false;
$iTextbook_cfg['grammartouch2']['MaxChapters']	 = 1;
$iTextbook_cfg['grammartouch2']['ChapterName'][1]  = array("en"=>"Grammar Touch 2");

/* Config for Grammar Touch 3*/
$iTextbook_cfg['grammartouch3']['module_code'] 	 = "grammartouch3";
$iTextbook_cfg['grammartouch3']['book_name']   	 = "Grammar Touch 3";
$iTextbook_cfg['grammartouch3']['instruction'] 	 = "";
$iTextbook_cfg['grammartouch3']['remarks'] 	  	 = "";
$iTextbook_cfg['grammartouch3']['cooldown']	  	 = 7; // in days 
$iTextbook_cfg['grammartouch3']['TotalNoOfUnit'] = 1;
$iTextbook_cfg['grammartouch3']['allowSelUnits'] = array(1);
$iTextbook_cfg['grammartouch3']['SpecialUnit']   = array(0=>1);
$iTextbook_cfg['grammartouch3']['DisableUnit']	 = array();
$iTextbook_cfg['grammartouch3']['BookLang']	  	 = array("en"); 	// Available Language for Book
$iTextbook_cfg['grammartouch3']['DefaultLang']	 = "en"; 				// Default Language of Book
$iTextbook_cfg['grammartouch3']['LicenseType']['ChapterStudent'] = false;
$iTextbook_cfg['grammartouch3']['LicenseType']['HaveExpiryDate'] = true;
$iTextbook_cfg['grammartouch3']['AllowCreateBook'] = false;
$iTextbook_cfg['grammartouch3']['MaxChapters']	 = 1;
$iTextbook_cfg['grammartouch3']['ChapterName'][1]  = array("en"=>"Grammar Touch 3");

/* Config for KIS Worksheets */
$iTextbook_cfg['kisworksheets']['module_code'] 	 = "kisworksheets";
$iTextbook_cfg['kisworksheets']['book_name']   	 = "KIS Worksheets";
$iTextbook_cfg['kisworksheets']['instruction'] 	 = "";
$iTextbook_cfg['kisworksheets']['remarks'] 	  	 = "";
$iTextbook_cfg['kisworksheets']['cooldown']	  	 = 7; // in days
$iTextbook_cfg['kisworksheets']['TotalNoOfUnit'] = 1;
$iTextbook_cfg['kisworksheets']['allowSelUnits'] = array(1);
$iTextbook_cfg['kisworksheets']['SpecialUnit']   = array(0=>1);
$iTextbook_cfg['kisworksheets']['DisableUnit']	 = array();
$iTextbook_cfg['kisworksheets']['BookLang']	  	 = array("en"); 	// Available Language for Book
$iTextbook_cfg['kisworksheets']['DefaultLang']	 = "en"; 				// Default Language of Book
$iTextbook_cfg['kisworksheets']['LicenseType']['ChapterStudent'] = false;
$iTextbook_cfg['kisworksheets']['LicenseType']['HaveExpiryDate'] = true;
$iTextbook_cfg['kisworksheets']['AllowCreateBook'] = false;
$iTextbook_cfg['kisworksheets']['MaxChapters']	 = 1;
$iTextbook_cfg['kisworksheets']['ChapterName'][1]  = array("en"=>"KIS Worksheets");

/* Define which module is iTextbook */
$iTextbook_cfg['iTextbook_module_code_ary'][] = $iTextbook_cfg['ambook']['module_code'];
$iTextbook_cfg['iTextbook_module_code_ary'][] = $iTextbook_cfg['poemsandsongs']['module_code'];
$iTextbook_cfg['iTextbook_module_code_ary'][] = $iTextbook_cfg['gvlistening']['module_code'];
$iTextbook_cfg['iTextbook_module_code_ary'][] = $iTextbook_cfg['gvlistening2']['module_code'];
$iTextbook_cfg['iTextbook_module_code_ary'][] = $iTextbook_cfg['gvlistening3']['module_code'];
$iTextbook_cfg['iTextbook_module_code_ary'][] = $iTextbook_cfg['rsreading1']['module_code'];
$iTextbook_cfg['iTextbook_module_code_ary'][] = $iTextbook_cfg['rsreading2']['module_code'];
$iTextbook_cfg['iTextbook_module_code_ary'][] = $iTextbook_cfg['rsreading3']['module_code'];
$iTextbook_cfg['iTextbook_module_code_ary'][] = $iTextbook_cfg['grammartouch1']['module_code'];
$iTextbook_cfg['iTextbook_module_code_ary'][] = $iTextbook_cfg['grammartouch2']['module_code'];
$iTextbook_cfg['iTextbook_module_code_ary'][] = $iTextbook_cfg['grammartouch3']['module_code'];
$iTextbook_cfg['iTextbook_module_code_ary'][] = $iTextbook_cfg['kisworksheets']['module_code'];
$iTextbook_cfg['LicenseSettingAry']['UnlimitQuota'] = 99999; //For One-Off Installation

if(file_exists($intranet_root."/includes/iTextbook/iTextbookConfig_Cust.inc.php")){
	include($intranet_root."/includes/iTextbook/iTextbookConfig_Cust.inc.php");
}
?>