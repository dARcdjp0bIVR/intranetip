<?php
// Modifing by kenneth chung

if (!defined("LIBIMPORTTEXT_DEFINED"))                     // Preprocessor directive
{
  define("LIBIMPORTTEXT_DEFINED", true);

  class libimporttext extends libdb
  {
	  	
		var $Records = array();
		var $CoreHeader = array();
		
		/*
		* Initialize
		*/
		function libimporttext()
		{
			global $intranet_root;
			$this->libdb();
		}
		
		/*
		* Check existence uploaded file
		*/
		function CHECK_FILE_UPLOADED($ParFile) {
			$ReturnVal = ($ParFile['error']==0 && file_exists($ParFile['tmp_name']));
			return $ReturnVal;
		}
		
		/*
		* Check Byte Order Mark
		*/
		function CHECK_BOM ($Buffer) {
			$charset[1] = substr($Buffer, 0, 1);
			$charset[2] = substr($Buffer, 1, 1);
			$charset[3] = substr($Buffer, 2, 1);  
			if (ord($charset[1]) == 255 && ord($charset[2]) == 254)
				return "UTF-16LE";
			else if (ord($charset[1]) == 254 && ord($charset[2]) == 255)
				return "UTF-16BE";
			else if (ord($charset[1]) == 239 && ord($charset[2]) == 187 && ord($charset[3]) == 191)
				return "UTF-8";
			else
				return "Unknown";
		}
		
		/*
		* Extract data from file
		*/
/*
remark:
shall add
$x = addslash($x);
if using the B5 mode, either at the calling side or here, which is now at the calling side
*/
		function GET_IMPORT_TXT($ParFilePath, $incluedEmptyRow=0) {
	
			global $intranet_default_lang, $import_coding, $g_encoding_unicode;			
			#$g_encoding_unicode = true;
			
			
			$Handle = @fopen($ParFilePath, 'r');
			$Buffer = fread($Handle, filesize($ParFilePath));
			$TempFile = tmpfile();
			$BytesLength = 4096; // Modified by key(2008-10-22) - enlarge the bytes length - default 2048
			
			/* Big5 encoding cannot be used in mb_convert_encoding in PHP version < 4.3.0
			if (function_exists(mb_convert_encoding)) {
				if ($this->CHECK_BOM($Buffer) == "UTF-16LE") {	// UTF-16LE
					$NewFileContent = mb_convert_encoding($Buffer, 'UTF-8', 'UTF-16LE');
					$NewFileContent = substr($NewFileContent, 2);
				} else if ($this->CHECK_BOM($Buffer) == "UTF-16BE") {
					$NewFileContent = mb_convert_encoding($Buffer, 'UTF-8', 'UTF-16BE');
					$NewFileContent = substr($NewFileContent, 2);
				} else {
					$NewFileContent = mb_convert_encoding($Buffer, 'UTF-8', 'auto');
				}
			} else
			*/
			
			// get file coding input from interface
			// if file coding is b5 / gb, change the coding to utf-8
			
			if (function_exists(iconv)) {
				
				if ($this->CHECK_BOM($Buffer) == "UTF-16LE") {	// UTF-16LE
					$NewFileContent = substr($Buffer, 2);
					$NewFileContent = iconv('UTF-16LE', 'UTF-8', $NewFileContent);
				} else if ($this->CHECK_BOM($Buffer) == "UTF-16BE") {
					$NewFileContent = substr($Buffer, 2);
					$NewFileContent = iconv('UTF-16BE', 'UTF-8', $NewFileContent);
				} else if ($this->CHECK_BOM($Buffer) == "UTF-8") {
					$NewFileContent = substr($Buffer, 3);
				} else {
					if ($import_coding == "utf") {
						$NewFileContent = $Buffer;
					} else {
						//$NewFileContent = $Buffer;
						$NewFileContent = iconv(mb_detect_encoding($Buffer,'BIG5,GB2312'), 'UTF-8//IGNORE', $Buffer);
						/*if ($intranet_default_lang == "b5") {
							$NewFileContent = iconv('BIG5', 'UTF-8//IGNORE', $Buffer);						
						} else {
							$NewFileContent = iconv('GB2312', 'UTF-8//IGNORE', $Buffer);
						}*/
					}					
				}
			} else {
				$NewFileContent = $Buffer;
			}
			
			////////////////////////////////////////////////////////////////////////////
			// For imail address testing
			////////////////////////////////////////////////////////////////////////////
			//$NewFileContent = str_replace("\"","",$NewFileContent);
			////////////////////////////////////////////////////////////////////////////
			
			# remove first 3-byte used for Byte Order Mark
			#$NewFileContent = substr($NewFileContent, 3);
			
			//if (($import_coding != "utf")||(!$g_encoding_unicode)) {
				
				// use old get csv function
				
			//} else {
			fwrite($TempFile, $NewFileContent);
	
			fseek($TempFile, 0);
			# fgetcsv() have problem with certain double quotted chinese words
			/*
			while (($Data = fgetcsv($TempFile, 2048, ","))!==FALSE)
			{
				$ReturnArr[] = $Data;
			}
			*/
			
			while (!feof($TempFile)) {
				$Data = trim(fgets($TempFile, $BytesLength));			
				//$Data = fgetcsv($TempFile, $BytesLength, ",");		 # yatwoon 20090608 (cater the last cell is empty without "")
				# 20090803 yat
				//debug_r($Data);
				# combine contents that contain link break
				
				if(trim($Data=="")) continue;
				$Data = '"' . str_replace(",","\",\"",$Data) .'"';
				//$Data = '"' . implode("\",\"",$Data) .'"';		# 20090803 yat
				
				
				$LastCell = substr(strrchr($Data, ","), 1);
        if(substr($LastCell, 0, 1)=="\"") {
          while(!feof($TempFile) && substr($Data, -1)!="\"") {
          	$Data .= "\n".trim(fgets($TempFile, $BytesLength));
          }
        }
          		
	      if(function_exists("mb_substr") && function_exists("mb_strlen")){
					$DataPieces = $this->mb_csv_split($Data);
				}else{
					#### Function mb_csv_split() is used to handle special characters
					#### and remove double quotes
					$RawDataPieces = explode(",", $Data);
					$DataPieces = array();
					for ($i=0; $i<sizeof($RawDataPieces); $i++) {
						//direct (first char not '"') or combine 
						$tmpDataPiece = $RawDataPieces[$i];
						$StrLength = strlen($tmpDataPiece);
						$Str = trim($tmpDataPiece);
						
						# remark the empty function, bug fix for extra field (yat woon 20081010)
						/*
						if (substr($Str, 0, 1) == "\"" && (substr($Str, $StrLength-1) != "\"" || substr($Str, $StrLength-2, 1) != "\"" || substr($Str, $StrLength-3, 1) != "\"")) {
							while (substr($RawDataPieces[$i+1], $StrLength-1) != "\"") {
								
							}
						}
						*/
						
						#### Disabled this block of code on 22-11-2007 by Andy Chan
						#### It cause problem whenever data are enclosed by double quotes
						# Eric Yip (20090706): Check if quotes pair up in raw data pieces before searching following data pieces
						if(!empty($RawDataPieces[$i]) && $RawDataPieces[$i][0] == '"' && $RawDataPieces[$i][strlen($RawDataPieces[$i])-1] != '"'){
							//look for combine end
							for($j=$i+1; $j<sizeof($RawDataPieces);$j++){
								$len = strlen($RawDataPieces[$j]);
								$tmpDataPiece .= ','.$RawDataPieces[$j];
								$i++;	# bug fix for extra field (yat woon 20081010)
								if($len > 0 && $RawDataPieces[$j][$len-1] == '"'){
									break;
								}
							}
						}
						
						$DataPieces[] = $this->REPLACE_DQ_IN_STR($tmpDataPiece);
					}
				}
				
				if($incluedEmptyRow)	# Yat Woon: some function need dislay the empty row [20090427]
				{
					$ReturnArr[] = $DataPieces;
				}
				else
				{
					# Eric Yip : To get rid of empty row (20080905)
					if(count(array_filter($DataPieces)) > 0)
						$ReturnArr[] = $DataPieces;
				}
			}
			
			
			fclose($TempFile);
			fclose($Handle);
			//}
//echo '<pre>';
//print_r($ReturnArr);
//echo '<pre>';
//die;	

			return $ReturnArr;
		}
		
		/*
		* Replace Double quotation mark of the string (1st character & last character)
		*/
		function REPLACE_DQ_IN_STR($Str) {
			$StrLength = strlen($Str);
			$Str = trim($Str);
			if (substr($Str, 0, 1) == "\"" && (substr($Str, $StrLength-1) == "\"" || substr($Str, $StrLength-2, 1) == "\"" || substr($Str, $StrLength-3, 1) == "\"")) {
				$Str = substr($Str, 1);
				$Str = substr($Str, 0, strlen($Str)-1);
			}
			return $Str;
		}
		
	// new method to handle special characters of csv file content
	// copy from libfilesystem.php
	/*
	## Old Version - cannot handle if there is a Double Quote in the cell element
    function mb_csv_split($line, $delim = ',', $removeQuotes = true) {
    	global $g_encoding_unicode;
    
	    $fields = array();
	    $fldCount = 0;
	    $inQuotes = false;
	    
	    # Eric Yip (20081204) : set internal encoding for mb functions
	    if($g_encoding_unicode)
	    	mb_internal_encoding("UTF-8");
	    else
	    	mb_internal_encoding("pass");
	
	    for ($i = 0; $i < mb_strlen($line); $i++) {
	      if (!isset($fields[$fldCount])) $fields[$fldCount] = "";
	      $tmp = mb_substr($line, $i, mb_strlen($delim));
		  if ($tmp === $delim && !$inQuotes) {
	        $fldCount++;
	        $i+= mb_strlen($delim) - 1;
	      }
	      else if ($fields[$fldCount] == "" && mb_substr($line, $i, 1) == '"' && !$inQuotes) {
	        if (!$removeQuotes) $fields[$fldCount] .= mb_substr($line, $i, 1);
	        $inQuotes = true;
	      }
	      else if (mb_substr($line, $i, 1) == '"') {
          if (mb_substr($line, $i+1, 1) == '"') {
            $i++;
            $fields[$fldCount] .= mb_substr($line, $i, 1);
          } else {
            if (!$removeQuotes) $fields[$fldCount] .= mb_substr($line, $i, 1);
            $inQuotes = false;
          }
	      }
	      else {
	      	$fields[$fldCount] .= mb_substr($line, $i, 1);
	      }
	    }
	    return $fields;
    }
    */
    ## Modified: by Ronald (20090331)
    ## Changed:	Now can handle if there is a double quote inside the cell element
    function mb_csv_split($line, $delim = ',', $removeQuotes = true) {
    	global $g_encoding_unicode;
    
	    $fields = array();
	    $fldCount = 0;
	    $inQuotes = false;
	    
	    # Eric Yip (20081204) : set internal encoding for mb functions
	    if($g_encoding_unicode)
	    	mb_internal_encoding("UTF-8");
	    else
	    	mb_internal_encoding("pass");
	
	    for ($i = 0; $i < mb_strlen($line); $i++) {
		    if (!isset($fields[$fldCount])) $fields[$fldCount] = "";
		    $tmp = mb_substr($line, $i, mb_strlen($delim));
		    if ($tmp === $delim && !$inQuotes) {
			    $fldCount++;
			    $i+= mb_strlen($delim) - 1;
		    }
		    else if ($fields[$fldCount] == "" && mb_substr($line, $i, 1) == '"' && !$inQuotes) {
			    if (!$removeQuotes) $fields[$fldCount] .= mb_substr($line, $i, 1);
			    //echo " A:".mb_substr($line, $i, 1);
			    $inQuotes = true;
		    }
		    else if (mb_substr($line, $i, 1) == '"') {
			    if (mb_substr($line, $i+1, 1) == '"') {
				    $i++;
				    $fields[$fldCount] .= mb_substr($line, $i, 1);
				    //echo " B:".mb_substr($line, $i, 1);
			    } else {
				    //if (!$removeQuotes) $fields[$fldCount] .= mb_substr($line, $i, 1);
				    
				    if((mb_substr($line, $i+1, 1) != $delim) && ($i!=mb_strlen($line)-1)){
					    $fields[$fldCount] .= mb_substr($line, $i, 1);
					    $inQuotes = true;
				    }
					else
					{
						$inQuotes = false;
					}
			    }
		    }
		    else {
			    $fields[$fldCount] .= mb_substr($line, $i, 1);
		    }
	    }
		//print_r($fields);
	    return $fields;
    }
		
		/*
		* Get file extension
		*/
		function GET_FILE_EXT($ParFilePath){
            $file = basename($ParFilePath);
            return strtolower(substr($ParFilePath, strrpos($ParFilePath,".")));
        }
        
        /*
		* Check file extension
		* Allow extensions: TXT, CSV, LOG
		*/
		function CHECK_FILE_EXT($ParFilePath){
            $file = basename($ParFilePath);
            $ext = strtoupper(substr($ParFilePath, strrpos($ParFilePath,".")));
            if($ext == ".CSV" || $ext == ".TXT" || $ext == ".LOG") {
            	return true;
        	} else {
        		return false;
	        }
        }
		
		
		/*
		*
		*/
		function SET_CORE_HEADER($ParCoreHeader) {
			$this->CoreHeader = $ParCoreHeader;
		}
		
		/*
		* Validate data, see if any field is unset or empty
		* (to be tested)
		*/
		function VALIDATE_DATA($ParData) {
			$CoreHeader = $this->CoreHeader;
			$CheckArray = array();
			for($i=0; $i<sizeof($ParData); $i++) {
				for ($j=0; $j<sizeof($CoreHeader); $j++) {
					if (empty($ParData[$i][$j]) || !isset($ParData[$i][$j]))
						$CheckArray[$i][$j] = TRUE;
					else
						$CheckArray[$i][$j] = FALSE;
				}
			}
			
			return $CheckArray;
		}
		
		/*
		* Validate data header based on core headers
		*  - can be strict or loose (can have more field after core headers)
		*/
		function VALIDATE_HEADER($ParData, $ParExactlyCore) {
	
			$CoreHeader = $this->CoreHeader;
			$DataHeader = $ParData[0];
	
			if ($ParExactlyCore && sizeof($DataHeader)!=sizeof($CoreHeader))
			{
				# not exactly matched
				$ReturnVal = FALSE;
			} else
			{
				$ReturnVal = TRUE;
	
				# check each header field
				for ($i=0; $i<sizeof($CoreHeader); $i++)
				{
					if (trim($CoreHeader[$i])!=trim($DataHeader[$i]))
					{
						$ReturnVal = FALSE;
						break;
					}
				}
			}
	
			return $ReturnVal;
		}
		
		
		/*
		* Check whether any valid data to add or update
		*/
		function ALLOW_SUBMISSION() {
			$Records = $this->Records;
			$ReturnVal = (sizeof($Records['valid_add'])>0 || sizeof($Records['valid_update'])>0);
			return $ReturnVal;
		}

	}	// End of class definition

}        // End of directive
?>
