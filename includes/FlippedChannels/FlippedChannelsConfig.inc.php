<?php
$FlippedChannelsConfig         = isset($FlippedChannelsConfig)? $FlippedChannelsConfig : array();
$FlippedChannelsConfig['mode'] = isset($FlippedChannelsConfig['mode'])? $FlippedChannelsConfig['mode'] : '';

switch($FlippedChannelsConfig['mode']){
    case 'DEBUG':
        $FlippedChannelsConfig['serverPath'] = 'http://videolib-dev.eclass.com.hk/';
        $FlippedChannelsConfig['serverPublicPath'] = 'https://videolib-dev.eclass.com.hk/';
        break;
    case 'TW_PRODUCTION':
        $FlippedChannelsConfig['serverPath'] = 'http://videolib.eclass.com.hk/';
        $FlippedChannelsConfig['serverPublicPath'] = 'https://videolib.eclass.com.hk/';
        break;
    default:
        $FlippedChannelsConfig['serverPath'] = 'http://videolib.eclass.com.hk/';
        $FlippedChannelsConfig['serverPublicPath'] = 'https://videolib.eclass.com.hk/';
}

?>