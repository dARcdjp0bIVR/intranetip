<?php
# using:

/******************************************
 *  modification log:
 *  2020-11-17 (Philips) - modify process_user2(), fixed KISClassType Handling
 *  2020-09-21 (Philips) - modify process_user2(), to support KISClassType Handling
 *  2020-02-26 (Bill) - modify set_row_array(), process_user2(), to support Graduation Date handling    [2020-0220-1623-28098]
 *  2019-06-24 (Carlos) - Set UserPassword field to NULL for non-hashed password authentication method such as LDAP.
 *  2018-12-31 (Isaac) - Added $sys_custom['AccountMgmt']['StudentDOBRequired'] to check import file for empty DOB
 *  2018-11-12 (Cameron) - consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
 *  2018-11-09 (Anna) - modify process_user2(), Added $plugin['StudentDataAnalysisSystem_Style'] == "tungwah" cust
 *  2018-11-01 (Bill) - modify process_user2(), added Gender input checking (input 'M' & 'F' only)    [2018-1029-1009-38066]
 *  2018-09-04 (Pun) [ip.2.5.9.10.1]
 *                      - modify process_user2(), added sync chinese name to google
 *  2018-08-22 (Cameron) [ip.2.5.9.10.1] - by pass checking Gender for HKPF in process_user2()
 *                      - modify set_row_array() to suit HKPF fields
 *  2018-08-14 (Henry) [ip.2.5.9.10.1] - modified process_user2() to trim the input WebSamsRegNo when checking with the current WebSamsRegNo
 *  2018-08-09 (Cameron) [ip.2.5.9.10.1] - add fields Grade & Duty to set_row_array(), process_user2()
 *  2018-04-06 (Pun) [ip.2.5.9.3.1] - Modified set_row_array_ncs(), fixed cannot import teacher account for NCS cust
 *  2018-04-03 (Carlos) - Skip checking WebSamsRegNo and HKJApplNo in process_user2().
 *  2018-01-03 (Carlos) - [ip.2.5.9.1.1] modified process_user2(), added password criteria checking.
 * 	2017-12-22 (Cameron) - configure for Oaks refer to Amway
 *  2017-12-20 (Carlos) - Fixed process_user2() initialize INTRANET_SYSTEM_ACCESS record only for new users, do not overwrite existing records.
 * 	2017-10-26 (Cameron) - handle import format for Amway in set_row_array()
 * 						- fix bug: should check if UserLogin is empty in set_row_array()
 *  2017-09-22 (Paul) 	- modified process_user2(), use back the original Class Name & Class Number if no Class Name and Class Number updated
 *  2017-08-18 (Carlos) - modified process_user2(), always update WebSAMSRegNo even it is empty such that can clear the data. But if iPortfolio account activated, not allow to change WebSAMSRegNo.
 *  2017-08-16 (Carlos) - replace deprecated $HTTP_SERVER_VARS["SERVER_NAME"] to $_SERVER["SERVER_NAME"].
 *  2017-07-19 (Paul) [ip.2.5.8.7.1]: add NCS import logic
 * 	2017-01-18 (HenryHM) [ip.2.5.7.10.1]: $ssoservice["Google"]["Valid"] - add GSuite Logic
 * 	2016-09-28 (Ivan) [ip.2.5.7.10.1]: modified process_user2(), update_default_group() to add teaching = 'S' supply teacher logic
 *  2016-08-09 (Carlos) - $sys_custom['BIBA_AccountMgmtCust'] added HouseholdRegister and House import fields for student, added House import field for staff.
 *  2016-05-09 (Carlos) - $sys_custom['ImportStudentAccountActionType'] added class attribute $action_type, modified process_user2(), check error for action type add/update.
 *  2015-01-13 (Carlos) - $sys_custom['StudentAccountAdditionalFields'] modified set_row_array() and process_data2(), added five fields.
 *  2015-05-12 (Omas) - modified process_user2() fixed cannot add user to usergroup
 * 	2015-03-20 (Cameron) - use fax field for recording date of baptism in set_row_array() if $sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism'] = true
 *							(applys to student and teacher only)
 *						 - add checking date format for date of baptism if it's not empty in process_user2()
 *						 - add checking date format for DOB and AdmissionDate if they are not empty in process_user2() for students
 *  2014-12-10 (Bill) 	- $sys_custom['SupplementarySmartCard'] modified set_row_array() and process_user2(), added CardID4 for student user type
 *  2014-09-03 (Carlos) - Modified open_os_accounts(), iMail plus - Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
 *	2014-08-21 (Bill)	- Modified set_row_array() to set new column HomeTelNo for import, process_user2() to edit existing HomeTelNo 
 *  2014-08-13 (Carlos) - Modified process_user2(), added checking for new account password cannot be empty
 * 	2014-07-17 (Bill)	- Add flag $plugin['eEnrollment'] for import file to set CardID - eEnrolment 
 *  2014-05-28 (Ivan) [2014-0526-1816-24071] - function process_user2() added to global $ck_course_id for libpf-sturec.php
 *  2014-02-20 (Carlos) - $sys_custom['iMailPlus']['EmailAliasName'] modified process_user2() added error feedback for EmailUserLogin
 *  2013-12-13 (fai) - Add a cust attribute "stayOverNight" with $plugin['medical']
 *  2013-12-12 (Carlos) - $sys_custom['iMailPlus']['EmailAliasName'] modified set_row_array(), process_user2() and open_os_accounts() user can set its own iMail plus account name
 *  2013-11-13 (Carlos) - $sys_custom['SupplementarySmartCard'] modified set_row_array() and process_user2(), added CardID2 and CardID3 for student user type
 *  2013-10-30 (Carlos) - modified process_user2(), set ePayment account balance to 9999999 for KIS
 *  2013-10-04 (Carlos) - modified process_user2(), when do update also validate UserLogin
 *  2013-08-13 (Carlos) - modified set_row_array(), skip staff code, WEBSAMS number, JUPAS application number for KIS
 *  2013-08-08 (Carlos) - modified set_row_array() and process_user2(), added [Address] field for student type
 * 
 * 	2013-07-17 (Ivan)
 * 		- modified process_user2() to check maximum UserID from INTRANET_USER and INTRANET_ARCHIVE_USER instead of using auto increment from DB
 * 
 *  2013-07-03	(Carlos) - fixed process_user2(), after insert a new user account, check db_insert_id() instead of db_affected_rows()
 * 
 *	2013-04-19	(Yuen)
 *		- fixed the bug that YearOfLeft cannot be updated for alumni
 *		- assign alumni to corresponding alumni group
 *
 *	2012-10-16	(YatWoon)
 *		modified process_user2(), no need to flag checking for StaffCode
 *
 *	2012-09-27	(YatWoon)
 *		modified process_user(), process_user2(), add Barcode
 *
 *	2012-09-12	(YatWoon)
 *		modified process_user(), process_user2(), skip check student name for parent import [Case#2012-0906-1719-21073]
 *
 *	2012-08-03	(Bill Mak)
 *		support import Nationality, PlaceOfBirth, AdmissionDate for student 
 * 
 * 	2012-03-08	(Ivan) [2012-0308-1629-48066]
 * 		modified set_row_array(), fixed failed to change CardID due to wrong array index
 * 		modified process_user2(), import must have English Name now 
 * 
 *  2012-02-02  (Henry Chow)
 * 		modified process_user2(), import of user account will not change the field "RecordStatus"
 * 
 *  modification log:
 *  2011-11-17  (Henry Chow)
 * 		modified process_user2() & set_row_array(), cater the "StaffCode" from import file
 * 
 *  2011-11-03  (Carlos)
 * 		modified set_row_array(), format CardID to ten digits pre-padding with zeroes
 * 
 *	2011-11-02	(YatWoon)
 *		update set_row_array(), add alumni coding
 *
 *  2011-10-21 	(Carlos)
 * 		updated process_user2(), added get quota of alumni to create iMail/iFolder account
 *
 *	2011-09-30  (Carlos)
 *		modified process_user2(), added storing two-way hashed/encrypted password
 *
 *	2011-09-30	(Henry Chow)
 *		update process_user2(), revised the imail quota setting
 *
 *	2011-09-05	(Carlos)
 *		update process_user2(), add preset quota for iMail user
 *
 *	2011-08-22	(Henry Chow)
 *		updated process_user2(), update INTRANET_USERGROUP if teaching type is changed while updating staff data
 *
 *	2011-06-23	(Fai)
 *		ADD Application for JUPAS for student account $HKJApplNo
 *		
 *	2011-03-02	(Henry Chow)
 *		update set_row_array() & process_user2()
 *		1) assign default email to import data if email is null
 *		2) skip checking on import email
 *
 *	2011-01-31	YatWoon
 *		update process_user2(), parent import skip check gender field (requested by UCCKE with case #2011-0131-1621-41073)
 *
 *	2011-01-27 Carlos
 *		fixed process_user() fail to set mail quota problem.
 *		fixed open_os_accounts() parent quota wrong index problem.
 * 
 *	20101230 (Jason)
 *		modified process_user2() to update the detail of eclass info when import user 
 *
 *	20101122 (Henry Chow)
 *		modified process_user(), add checking on "Gender" (cannot be empty)
 *
 *	20101022 Ronald
 *		modified open_os_accounts(), add a Global variable $TabID, so that it can get the quota correctly.
 *
 *	20101011 Marcus
 *		add checking to avoid empty Gender. 
 *
 *	20101007 Henry Chow
 *		modified update_default_group(), revise SQL statement, ignore the condition if $existUserGroupUser is null 
 *
 *	20100930 Henry Chow
 *		modified process_user2(),  fixed import problem (assigned "EnglishName" to "NickName")
 *
 *	20100907 Marcus
 *		fixed update_default_group, import a teaching staff as a non-teaching staff (or vise versa), the Isteaching staus and default group will not changed 
 *
 *	20100903 Marcus
 *		fixed update_default_group, which will duplicate user record in INTRANET_USERGROUP when re-import user. 
 *
 *	20100902 Ronald
 *		update open_os_accounts(), so now after import user account, Mailbox quota will not become to 0
 *
 *	20100816 Henry Chow
 *		update process_data2() & linkWithStudent(), make linkage between parent & student
 *
 *	20100604 Henry Chow
 *		update update_default_group(), update "DateInput" & "DateModified" when insert user in INTRANET_USERGROUP
 *
 *	20100528 YatWoon
 *		update set_row_array(), maybe import student csv is without CardID
 *
 * 	20100525 marcus:
 * 		- add function check_data, process_user2, process_data2, GetErrorMsgArr used in AccountMgmt import
 * 
 * ****************************************/
include_once("$intranet_root/includes/libwebmail.php");
include_once("$intranet_root/includes/libaccountmgmt.php");
include_once("$eclass_filepath/src/includes/php/lib-portfolio.php");

class libimport extends libdb {

        var $domain_name;
        var $default_status;
        var $record_status;
        var $data_array;
        var $format;
        var $recordIsValid;
        var $reason;
        var $debug_msg;
        var $teaching;

        var $queries;
        var $open_webmail;
        var $open_file_account;
		var $action_type; // $sys_custom['ImportStudentAccountActionType'] for BIBA
		
        # ------------------------------------------------------------------------------------

        var $UserLogin;
        var $UserEmail;
        var $FirstName;
        var $LastName;
        var $EnglishName;
        var $ChineseName;
        var $Title;
        var $Gender;
        var $DOB;
        var $Home;
        var $Office;
        var $Mobile;
        var $Fax;
        var $ICQ;
        var $Address;
        var $Country;
        var $ClassNumber;
        var $ClassName;
        var $ClassLevel;
        var $UserPassword;
        var $CardID;
        var $CardID2; // added on 2013-11-13 
        var $CardID3; // added on 2013-11-13
        var $CardID4; // added on 2014-12-10
		var $WebSamsRegNo;
		var $HKID;
		var $STRN;
		
		var $NonChineseSpeaking; // $sys_custom['StudentAccountAdditionalFields']
		var $SpecialEducationNeeds; // $sys_custom['StudentAccountAdditionalFields']
		var $PrimarySchool; // $sys_custom['StudentAccountAdditionalFields']
		var $University; // $sys_custom['StudentAccountAdditionalFields']
		var $Programme; // $sys_custom['StudentAccountAdditionalFields']
		
		var $HouseholdRegister; // $sys_custom['BIBA_AccountMgmtCust']
		var $House; // $sys_custom['BIBA_AccountMgmtCust']
		
		var $stayOverNight;

        var $gpLevel;

        var $lwebmail;
        var $lftp;
        var $lldap;

        var $acl_value;

        var $class_list;
        var $level_list;

        var $childrenLogin;
        var $childrenClass;
        var $childrenName;

        var $TitleEnglish;
        var $TitleChinese;

        var $os_accounts_to_open;
        var $file_quota;
        var $mail_quota;

        var $new_account_userids;

        var $iportfolio;
		var $HKJApplNo;  // Application no for JUPAS
		var $ImapUserLogin; // $plugin['imail_gamma'] + $sys_custom['iMailPlus']['EmailAliasName']
		var $EnableGSuite; //  $ssoservice["Google"]["Valid"]
		var $GoogleUserLogin; //  $ssoservice["Google"]["Valid"]
		
		var $StaffInCharge ; 	// NCS project use only
		var $DurationInHK;		// NCS project use only
		var $LanguagePriority;	// NCS project use only
		var $IsNCS;				// NCS project use only
		var $NGO;				// NCS project use only
		
		var $Grade;       // for HKPF
		var $Duty;        // for HKPF
		
        # ------------------------------------------------------------------------------------

        function libimport(){
                global $HTTP_SERVER_VARS;
                global $intranet_email_generation_domain;
                global $plugin,$personalfile_type;
                global $intranet_authentication_method;
                
                $this->libdb();
                $this->domain_name = ($intranet_email_generation_domain==""? $_SERVER["SERVER_NAME"]:$intranet_email_generation_domain);
                $this->default_status = 1; # 1 = Approved ; 0 = Suspended ; 2 = Pending ;
                $this->record_status = 9;

                $this->queries = "";
                $this->lwebmail = new libwebmail();
                
                $this->laccount = new libaccountmgmt();
               
                if($plugin['iPortfolio']){
                	$this->iportfolio = new portfolio();
                }

                if ($plugin['personalfile'])
                {
                    if ($personalfile_type == 'FTP')
                    {
                        $this->lftp = new libftp();
                    }
                }

                if ($intranet_authentication_method=='LDAP')
                {
                	include_once("libldap.php");
                    $this->lldap = new libldap();
                    if ($this->lldap->isAccountControlNeeded())
                    {
                        $this->lldap->connect();
                    }
                }


//                 $sql = "SELECT DISTINCT ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
//                 $this->class_list = $this->returnVector($sql);

                /*
                $sql = "SELECT DISTINCT LevelName FROM INTRANET_CLASSLEVEL WHERE RecordStatus = 1 ORDER BY LevelName";
                $this->level_list = $this->returnVector($sql);
                */
        }

        # ------------------------------------------------------------------------------------

        function set_format($format){
                $this->format = $format;
        }

        function set_data_array($data_array){
                for($i=0; $i<sizeof($data_array); $i++){
                        for($j=0; $j<sizeof($data_array[$i]); $j++){
                                $data_array[$i][$j] = addslashes($data_array[$i][$j]);
                        }
                        $this->recordIsValid[$i] = false;
                        $this->reason[$i] = "";
                }
                $this->data_array = $data_array;
        }

        function set_row_array($row){
                 global $TabID;
                 global $plugin, $sys_custom;
                 global $module_version;
                 global $special_feature;
                 global $intranet_email_generation_domain, $HTTP_SERVER_VARS;
                 
                 $isKIS = $_SESSION["platform"]=="KIS";
                 
                 # Check whether is an empty row
                 if (sizeof($row)==0) return false;
                 $string = implode("",$row);
                 if (trim($string)=="")
                 {
                     return false;
                 }
                
                 /*
				if ($TabID == 1)
				{
				    $file_format = array ("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","ClassNumber","ClassName");
				}
				else if ($TabID == 2)
				{
				    $file_format = array ("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","ClassNumber","ClassName");
				}
				else if ($TabID == 3)
				{
				    $file_format = array ("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","StudentLogin1","StudentEngName1","StudentClass1","StudentLogin2","StudentEngName2","StudentClass2","StudentLogin3","StudentEngName3","StudentClass3");
				}
				*/
                
                $this->UserLogin = $row[0];
                $this->UserPassword = $row[1];
                $this->UserEmail = $row[2];
                if($this->UserEmail=="") {
                	$domain_name = ($intranet_email_generation_domain==""? $_SERVER["SERVER_NAME"]:$intranet_email_generation_domain);
					$this->UserEmail = $this->UserLogin."@".$domain_name; 
                }
                $this->EnglishName = $row[3];
                $this->ChineseName = $row[4];


                # TitleEnglish, TitleChinese
                if ($TabID == 1)		# teacher / staff
                {
                	#####################################################################################################################
                	################# Please change the $counter to the corresponding value if inserted any new columns #################
                	#####################################################################################################################
                    if ($sys_custom['project']['HKPF']) {
                        $this->Remark = $row[5];
                        $this->Grade = $row[6];
                        $this->Duty = $row[7];
                    }
                    else {
                    	$this->NickName = $row[5];
                   	 	$this->Gender = $row[6];
                   		$this->Mobile = $row[7];
                   		$this->Fax = ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) ? getDefaultDateFormat($row[8]) : $row[8];
                   		$this->Barcode = $row[9];
                   		$this->Remark = $row[10];
                        $this->TitleEnglish = $row[11];
                        $this->TitleChinese = $row[12];
                        $counter = 12;
                        if(!$isKIS) {
                        	$counter++;
                        	$this->StaffCode = $row[$counter];
                        }
                        if($module_version['StaffAttendance'] || $plugin['payment'] )
                        {
                        	$counter++;
                        	$this->CardID = FormatSmartCardID($row[$counter]);
                    	}
                        
       	                if ($special_feature['ava_hkid'])
       	                {
    	   	                $counter++;
    	   	                $this->HKID = $row[$counter];
       	                }
       	                if($sys_custom['BIBA_AccountMgmtCust']){
       	                	$counter++;
       	                	$this->House = $row[$counter];
       	                }
       	                
       	                if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName']){
       	                	$counter++;
       	                	$this->ImapUserLogin = $row[$counter];
       	                }
       	                global $ssoservice;
       	                if($ssoservice["Google"]["Valid"]){
       	                	$counter++;
       	                	$this->EnableGSuite = $row[$counter]=="Y"?"Y":"N";
       	                	$counter++;
       	                	$this->GoogleUserLogin = $row[$counter];
       	                }
                    }
                }


                $hasSmartCard = ($plugin['attendancestudent'] || $plugin['payment'] || $plugin['Lunchbox'] || $plugin['eEnrollment']);
                
                # student
                if ($TabID == 2) # && isset($plugin['attendancestudent']) && $plugin['attendancestudent'])
                {	
                    if ($sys_custom['project']['CourseTraining']['IsEnable']) {
	               	 	$this->Gender = $row[5];
	               	 	// Added
	               	 	$this->Home = $row[6];
	               		$this->Mobile = $row[7];
	               		$this->Fax = $row[8];	               		
	               		$this->Remark = $row[9];
						$this->Address = $row[10];
						$this->CardID = $row[11];
						return true;
					}
                	
       	        	$this->NickName = $row[5];
               	 	$this->Gender = $row[6];
               	 	// Added
               	 	$this->Home = $row[7];
               		$this->Mobile = $row[8];
               		$this->Fax = ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) ? getDefaultDateFormat($row[9]) : $row[9];
               		$this->Barcode = $row[10];
               		$this->Remark = $row[11];
                    $this->DOB = getDefaultDateFormat($row[12]);
					$counter = 12;
/*
	                if($row[14]!=""){
		                if(substr(trim($row[14]),0,1)!="#") # invalid WebSamsRegNo
		                	return false;
		            }
*/
					if(!$isKIS) {
						$counter++;
   	                	$this->WebSamsRegNo = $row[$counter];
						$counter++;
   	                	$this->HKJApplNo = $row[$counter];
					}
   	                $counter++;
					$this->Address = $row[$counter];
					
	                if($hasSmartCard)
	                {
		                $counter++;
                    	$this->CardID = FormatSmartCardID($row[$counter]);
                    	if($sys_custom['SupplementarySmartCard']) {
	                    	$counter++;
	                    	$this->CardID2 = FormatSmartCardID($row[$counter]);
	                    	$counter++;
	                    	$this->CardID3 = FormatSmartCardID($row[$counter]);
	                    	$counter++;
	                    	$this->CardID4 = FormatSmartCardID($row[$counter]);
                    	}
                	}
                	
   	                if ($special_feature['ava_hkid'])
   	                {
	   	                $counter++;
	   	                $this->HKID = $row[$counter];
   	                }
   	                if ($special_feature['ava_strn'])
   	                {
	   	                $counter++;
	   	                $this->STRN = $row[$counter];
   	                }
   	                
   	                global $ssoservice;
   	                if($ssoservice["Google"]["Valid"]){
   	                	$counter++;
   	                	$this->EnableGSuite = $row[$counter]=="Y"?"Y":"N";
   	                }
   	                
   	                if($sys_custom['BIBA_AccountMgmtCust']){
   	                	$counter++;
   	                	$this->HouseholdRegister = $row[$counter];
   	                	$counter++;
   	                	$this->House = $row[$counter];
   	                }
   	                
   	                if($sys_custom['StudentAccountAdditionalFields']){
   	                	$counter++;
   	                	$this->NonChineseSpeaking = $row[$counter]=="Y"?"Y":"N";
   	                	$counter++;
   	                	$this->SpecialEducationNeeds = $row[$counter]=="Y"?"Y":"N";
   	                	$counter++;
   	                	$this->PrimarySchool = $row[$counter];
   	                	$counter++;
   	                	$this->University = $row[$counter];
   	                	$counter++;
   	                	$this->Programme = $row[$counter];
   	                }
   	                
					if($plugin['medical'])
					{
						$_tmpValue = $row[++$counter];

						//since user input value is Y or N , convert to 1 or 0 for save to DB
						$_tmpValue = (strtolower($_tmpValue) == 'y') ?  1 : 0;
						$this->stayOverNight = $_tmpValue;
					}
					
					// Additional Personal Setting
					$this->Nationality = $row[++$counter];
		            $this->PlaceOfBirth = $row[++$counter];
		            $this->AdmissionDate = getDefaultDateFormat($row[++$counter]);
                    $this->GraduationDate = getDefaultDateFormat($row[++$counter]);     // [2020-0220-1623-28098]
                    
                    if($plugin['SDAS_module']['KISMode']){
                    	$this->KISClassType = $row[++$counter];
                    }
		            if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah" ){
		                $this->PrimarySchoolCode = $row[++$counter];
		            }
	                /*
	                if($this->WebSamsRegNo!=""){ # add the leading "#"
		                if(substr(trim($this->WebSamsRegNo),0,1)!="#")
							$this->WebSamsRegNo = "#".$this->WebSamsRegNo;
		            }
		            */
                }
                if ($TabID == 3)		# parent
                {
               	 	$this->Gender = $row[5];
               		$this->Mobile = $row[6];
               		$this->Fax = $row[7];
               		$this->Barcode = $row[8];
               		$this->Remark = $row[9];
               		
               		$counter = 9;
               		
               		if ($special_feature['ava_hkid']) {
               			$counter++;
               			$this->HKID = $row[$counter];

               		}
               		 
                             # Locate children
                            $this->childrenLogin = array($row[$counter+1],$row[$counter+3],$row[$counter+5]);
                            $this->childrenName = array($row[$counter+2],$row[$counter+4],$row[$counter+6]);
                            
                }
               
                 if ($TabID == 4)		# alumni
                {
	                $this->NickName = $row[5];
               	 	$this->Gender = $row[6];
               		$this->Mobile = $row[7];
               		$this->Fax = $row[8];
               		$this->Barcode = $row[9];
               		$this->Remark = $row[10];
               		$this->ClassName = $row[11];
               		$this->ClassNumber = $row[12];
               		$this->YearOfLeft = $row[13];
               		$counter = 13; 
                }
                               
              
                return true;
        }

		function set_row_array_ncs($row){

			global $TabID;
			global $plugin, $sys_custom;
			global $module_version;
			global $special_feature;
			global $intranet_email_generation_domain, $HTTP_SERVER_VARS;
			 
			$isKIS = $_SESSION["platform"]=="KIS";
			 
			# Check whether is an empty row
			if (sizeof($row)==0) return false;
			$string = implode("",$row);
			if (trim($string)=="")
			{
				return false;
			}
						
			$this->UserLogin = $row[0];
			$this->UserPassword = $row[1];
			$this->UserEmail = $row[2];
			if($this->UserEmail=="") {
				$domain_name = ($intranet_email_generation_domain==""? $_SERVER["SERVER_NAME"]:$intranet_email_generation_domain);
				$this->UserEmail = $this->UserLogin."@".$domain_name;
			}
			$this->EnglishName = $row[3];
			$this->ChineseName = $row[4];
			
			
			$hasSmartCard = ($plugin['attendancestudent'] || $plugin['payment'] || $plugin['Lunchbox'] || $plugin['eEnrollment']);
			
			# teacher
			if($TabID == 1)
			{
            	$this->NickName = $row[5];
           	 	$this->Gender = $row[6];
           		$this->Mobile = $row[7];
           		$this->Fax = ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) ? getDefaultDateFormat($row[8]) : $row[8];
           		$this->Barcode = $row[9];
           		$this->Remark = $row[10];
                $this->TitleEnglish = $row[11];
                $this->TitleChinese = $row[12];
            	$this->StaffCode = $row[13];
            	$this->CardID = FormatSmartCardID($row[14]);
            	$this->HKID = $row[15];
			}
			# student
			elseif ($TabID == 2) # && isset($plugin['attendancestudent']) && $plugin['attendancestudent'])
			{
				$this->NickName = $row[5];
				$this->Gender = $row[6];
				// Added
				$this->Home = $row[7];
				$this->Mobile = $row[8];
				$this->Fax = ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) ? getDefaultDateFormat($row[9]) : $row[9];
				$this->Barcode = $row[10];
				$this->Remark = $row[11];
				$this->DOB = getDefaultDateFormat($row[12]);
				$counter = 12;
				/*
				 if($row[14]!=""){
				 if(substr(trim($row[14]),0,1)!="#") # invalid WebSamsRegNo
				 return false;
				 }
				 */
				$counter++;
				$this->WebSamsRegNo = $row[$counter];
				$counter++;
				$this->HKJApplNo = $row[$counter];
				
				$counter++;
				$this->Address = str_replace("@=@",",",$row[$counter]);
					
				$counter++;
				$this->CardID = FormatSmartCardID($row[$counter]);
				 
				$counter++;
				$this->HKID = $row[$counter];
				
				$counter++;
				$this->STRN = $row[$counter];
			
				// Additional Personal Setting
				$this->Nationality = $row[++$counter];
				$this->PlaceOfBirth = $row[++$counter];
				$this->AdmissionDate = getDefaultDateFormat($row[++$counter]);
				
				$this->StaffInCharge = $row[++$counter];
				$this->DurationInHK = $row[++$counter];
				$this->LanguagePriority = str_replace("@=@",",",$row[++$counter]);
				$this->IsNCS = $row[++$counter];
				$this->NGO = $row[++$counter];
			}
			# parent
			elseif($TabID == 3)
			{
           	 	$this->Gender = $row[5];
           		$this->Mobile = $row[6];
           		$this->Fax = $row[7];
           		$this->Barcode = $row[8];
           		$this->Remark = $row[9];
           		
           		$counter = 9;
           		
           		if ($special_feature['ava_hkid']) {
           			$counter++;
           			$this->HKID = $row[$counter];
    
           		}
           		 
                 # Locate children
                $this->childrenLogin = array($row[$counter+1],$row[$counter+3],$row[$counter+5]);
                $this->childrenName = array($row[$counter+2],$row[$counter+4],$row[$counter+6]);
                           
			}
			return true;			
		}        
        # ------------------------------------------------------------------------------------

        function update_status(){
                $sql  = "UPDATE INTRANET_USER SET RecordStatus = ".$this->default_status." WHERE RecordStatus = ".$this->record_status;
                $this->db_db_query($sql);
        }

        function update_default_group(){
                 global $TabID,$teaching;
                 $identity = $TabID;
                 if ($identity == 1)      # Teacher/Staff
                 {
                 	$source_group_id = "1,3";
                     if ($teaching == 1)    # Teaching staff -> Teacher group
                     {
                         $target_idgroup = 1;
                         $sql = "UPDATE INTRANET_USER SET Teaching = 1 WHERE RecordStatus = ".$this->record_status;
                         
                     }
                     else if ($teaching == 'S') {
                     	$sql = "UPDATE INTRANET_USER SET Teaching = 'S' WHERE RecordStatus = ".$this->record_status;
                     }
                     else                   # Non-teaching staff -> Admin staff group
                     {
                         $target_idgroup = 3;
                         $sql = "UPDATE INTRANET_USER SET Teaching = 0 WHERE RecordStatus = ".$this->record_status;
                     }
                     $this->db_db_query($sql);
                 }
                 else if ($identity == 2)     # Student -> Student group
                 {
                      $target_idgroup = 2;
                      $source_group_id = 2;
                 }
                 else if ($identity == 3)     # Parent -> Parent group
                 {
                      $target_idgroup = 4;
                      $source_group_id = 4;
                 }
                 
                 if ($identity == 1 && $teaching == 'S') {
                 	// no need process identity group for Supply Teacher
                 }
                 else {
                 	$sql = "
						SELECT 
							DISTINCT iu.UserID
						FROM 
							INTRANET_USER iu 
							LEFT JOIN INTRANET_USERGROUP iug ON iu.UserID = iug.UserID 
						 	INNER JOIN INTRANET_GROUP ig ON ig.GroupID = iug.GroupID AND ig.GroupID IN (".$source_group_id.")
						WHERE 
							iu.RecordStatus = ".$this->record_status."
					";
		
	                 $existUserGroupUser = $this->returnVector($sql);
					 $existUserGroupUserSql = implode(",",$existUserGroupUser);  
	    			
	    			if(sizeof($existUserGroupUser)>0) {
	    				
						 $sql = "UPDATE INTRANET_USERGROUP SET GroupID='$target_idgroup' , DateModified=NOW() WHERE GroupID IN (".$source_group_id.")  AND UserID IN ($existUserGroupUserSql)";
		//				 $new_id = $this->db_insert_id();
		//				 $sql = "UPDATE INTRANET_USERGROUP SET DateInput=NOW(), DateModified=MOW() WHERE UserGroupID=$new_id";
		
						 $this->db_db_query($sql);
						 
						 // skip insert for existing user
						 $cond1 = " AND UserID NOT IN ($existUserGroupUserSql) ";
	    			}
	    				
	                $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) SELECT $target_idgroup, UserID, NOW(), NOW() FROM INTRANET_USER WHERE RecordStatus = ".$this->record_status.$cond1;
	                $this->db_db_query($sql);
	
	        /*
	                global $GroupID;
	                for($i=0; $i<sizeof($GroupID); $i++){
	                        if ($this->recordIsValid[$i])
	                        {
	                            $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID) SELECT ".$GroupID[$i].", UserID FROM INTRANET_USER WHERE RecordStatus = ".$this->record_status;
	                            $this->db_db_query($sql);
	                        }
	                }
	                */
                 }
        }

        function process_user(){
                global $intranet_db,$plugin,$personalfile_type, $intranet_root;
                global $TabID;              # User RecordType
                global $intranet_authentication_method, $intranet_password_salt;
                global $personalfile_type, $i_license_sys_msg_none;
                global $account_license_student, $special_feature, $Lang;
                $recordType = $TabID;

                $file_content = get_file_content($intranet_root."/file/account_file_quota.txt");
                if ($file_content == "")
                {
                    $userquota = array(10,10,10);
                }
                else
                {
                    $userquota = explode("\n", $file_content);
                }
                $this->file_quota = $userquota[$recordType-1];

                $file_content = get_file_content($intranet_root."/file/account_mail_quota.txt");
                if ($file_content == "")
                {
                    $userquota = array(10,10,10);
                }
                else
                {
                    $userquota = explode("\n", $file_content);
                }
                $this->mail_quota = $userquota[$recordType-1];

                $row = $this->data_array;
                
                $acl = 0;
                $acl += ($this->open_webmail? 1:0);
                $acl += ($this->open_file_account? 2: 0);
                $this->acl_value = $acl;

                $lc = new libeclass();
				$license_student_left = 9999;
				if (isset($account_license_student) && $account_license_student>0 && $TabID==2)
				{
					
					$license_student_used = $lc->getLicenseUsed();
					$license_student_left = $account_license_student - $license_student_used;
				}

                for($i=0; $i<sizeof($row); $i++){
                        $mail_failed = 0;
                        $file_failed = 0;

                        if (!$this->set_row_array($row[$i]))
                        {
                             $this->recordIsValid[$i]=false;
                             $this->reason[$i]="";
                             continue;
                        }
                        /*
                        # update user record
                        if ($this->ClassName != "" && !in_array($this->ClassName,$this->class_list))
                        {
                            $this->recordIsValid[$i]=false;
                            $this->reason[$i] = "Invalid ClassName for Student";
                            continue;
                        }*/


                        ####################################################################
                        # Check whether it is an old account
                        $sql = "SELECT UserEmail,UserID,RecordType FROM INTRANET_USER WHERE UserLogin = '".$this->UserLogin."'";
                        $temp = $this->returnArray($sql,3);
                        if (sizeof($temp)!=0)      # Account update
                        {
                            $isNewAccount = false;
                            if($this->UserEmail == "" ||$this->validateEmail($this->UserEmail) )
                            {
                               $login_valid = true;
                            }
                            else
                            {
                                $login_valid = false;
                            }
                            list($old_email,$recordUserID,$user_type) = $temp[0];
                        }
                        else
                        {
                            $isNewAccount = true;
                            if ($this->validateLogin($this->UserLogin) && ($this->UserEmail == "" ||$this->validateEmail($this->UserEmail) ))
                            {
                                $login_valid = true;
                            }
                            else
                            {
                                $login_valid = false;
                            }
                        }

                        ### check user type
                        if(!$isNewAccount){
	                        if($user_type != $recordType){ ## the usertype of the existing user is different from the import usertype
		                        $this->recordIsValid[$i]=false;
		                        $this->reason[$i] ="The User Identity in the System is different from the import User Identity";
		                        continue;
		                    }
	                    }
	                    
						###### checking on Gender ######
	                    if(trim($this->Gender) == "")
	                    {
							$this->recordIsValid[$i]=false;
	                		$this->reason[$i] = $Lang['AccountMgmt']['ErrorMsg']['GenderCannotBeEmpty'];
	                		continue;
	                    }	 						


                        ####### handle Smart Card ID ###############
                        if($login_valid && trim($this->CardID)!=""){
	                        $sql="SELECT UserID FROM INTRANET_USER WHERE CardID='".$this->CardID."' AND UserLogin!='".$this->UserLogin."'";
	                        $cardid_temp = $this->returnVector($sql);
	                        if(sizeof($cardid_temp)!=0){  # card id exists
		                        $this->recordIsValid[$i]=false;
		                        $this->reason[$i] = "Smart Card ID has been used by other user.";
		                        continue;
		                    }
	                    }
	                    
	                    # check HKID is existing or not (if include HKID field)
						if($special_feature['ava_hkid'])
						{
							if($this->HKID != "")
							{
								$sql="SELECT UserID FROM INTRANET_USER WHERE HKID='".$this->HKID."' AND UserLogin!='".$this->UserLogin."'";
								$hkid_temp = $this->returnVector($sql);
								if(sizeof($hkid_temp)!=0){ ## HKID exists
									$this->recordIsValid[$i]=false;
			                        $this->reason[$i] = "HKID has been used by other user.";
			                        continue;
								}
							}
						}
						
						# check STRN is existing or not (if include STRN field)
						if($special_feature['ava_strn'])
						{
							if($this->STRN != "")
							{
								$sql="SELECT UserID FROM INTRANET_USER WHERE STRN='".$this->STRN."' AND UserLogin!='".$this->UserLogin."'";
								$strn_temp = $this->returnVector($sql);
								if(sizeof($strn_temp)!=0){ ## STRN exists
									$this->recordIsValid[$i]=false;
			                        $this->reason[$i] = "STRN has been used by other user.";
			                        continue;
								}
							}
						}

                        $iportfolio_activated = false;
                        ####### handle Websams Reg No ##############
                        if($TabID==2 && $login_valid && trim($this->WebSamsRegNo)!=""){
                                # check websams regno format
				                if(substr(trim($this->WebSamsRegNo),0,1)!="#"){ # invalid WebSamsRegNo
					                	$this->recordIsValid[$i]=false;
					                	$this->reason[$i] ="Please add '#' in front of WebSamsRegNo";
					                	continue;
					            }

					            if(strlen(trim($this->WebSamsRegNo))>1)
					            {
	  		                        $sql="SELECT UserID FROM INTRANET_USER WHERE WebSamsRegNo='".$this->WebSamsRegNo."' AND UserLogin!='".$this->UserLogin."'";
			                        $websams_temp = $this->returnVector($sql);
			                        if(sizeof($websams_temp)!=0){ 		# WebSamsRegNo exists
			                                   $this->recordIsValid[$i]=false;
	                            			   $this->reason[$i] = "WebSamsRegNo has been used by other user.";
	                            			   continue;
				                    }
			                    }

	                        if($isNewAccount){
		                        # do nothing
		                    }else{
       	                        $sql = "SELECT UserID,WebSamsRegNo FROM INTRANET_USER WHERE UserLogin = '".$this->UserLogin."'";
      	                        $websams_temp = $this->returnArray($sql,2);
								list($temp_userid,$temp_websams_regno)=$websams_temp[0];

      	                        if($plugin['iPortfolio']){
           	                        $iportfolio_data = $this->iportfolio->returnAllActivatedStudent();
	      	                        for($x=0;$x<sizeof($iportfolio_data);$x++){
		      	                        if($temp_userid == $iportfolio_data[$x][0]){ # iPortfolio activated
			      	                        $iportfolio_activated = true;
			      	                        break;
			      	                    }
		      	                    }
		      	                }
	      	                    if($iportfolio_activated && $temp_websams_regno!=$this->WebSamsRegNo ){
   		                                   $this->recordIsValid[$i]=false;
                            			   $this->reason[$i] = "WebSAMS Registration Number is not allowed to change.";
                            			   continue;

	      	                    }
		                    }

	                    }

                         ####################################################################

                        #if( $this->validateLogin($this->UserLogin) && ($this->UserEmail == "" ||$this->validateEmail($this->UserEmail) )){
                        if ($login_valid)
                        {
                            if ($isNewAccount)
                            {
                                $target_email = ($this->UserEmail==""? $this->UserLogin . "@".$this->domain_name : $this->UserEmail);
                                $target_email = substr($target_email,0,70);
                                $target_password = ($this->UserPassword ==""? intranet_random_passwd(8): $this->UserPassword);
                                if ($intranet_authentication_method == "HASH")
                                {
                                    $password_field = ",UserPassword,HashedPass ";
                                    $password_value = ",NULL,MD5('".$this->UserLogin.$target_password.$intranet_password_salt."') ";
                                }
                                /*
                                else if ($intranet_authentication_method == "LDAP")
                                {
                                     $password_field = "";
                                     $password_value = "";
                                }*/
                                else
                                {
                                    $password_field = ", UserPassword ";
                                    $password_value = ", NULL ";
                                }
                                
                                # check email is duplicate or not (20080911 - Yat Woon)
                                $temp_sql = "select UserID from INTRANET_USER where UserEmail='$target_email'";
                                $temp_result = $this->returnArray($temp_sql);
                                $dupUserID = $temp_result[0][0];
                                if($dupUserID)
                                {
	                                $this->recordIsValid[$i]=false;
                            		$this->reason[$i] = "Email is already exists.";
                                }
                                else
                                {
									# if WebSams = #, then empty the field
									$thisWebSamsRegNo = (trim($this->WebSamsRegNo)=="#") ? "" : trim($this->WebSamsRegNo);
	             
									/*
									$sql = "INSERT INTO INTRANET_USER (UserLogin, UserEmail
	                                        $password_field, FirstName, LastName, EnglishName,
	                                        ChineseName, NickName, Title, Gender, DateOfBirth,
	                                        HomeTelNo, OfficeTelNo, MobileTelNo, FaxNo, ICQNo,
	                                        Address, Country, ClassNumber, ClassName, Teaching,
	                                        CardID, TitleEnglish, TitleChinese,
	                                        RecordStatus, RecordType, WebSAMSRegNo, ";
	                                        */
									$sql = "INSERT INTO INTRANET_USER (UserLogin, UserEmail
	                                        $password_field, FirstName, LastName, EnglishName,
	                                        ChineseName, NickName, Title, Gender, DateOfBirth,
	                                        HomeTelNo, OfficeTelNo, MobileTelNo, FaxNo, ICQNo,
	                                        Address, Country, Teaching,
	                                        CardID, TitleEnglish, TitleChinese,
	                                        RecordStatus, RecordType, WebSAMSRegNo, ";
	                                        
									if($special_feature['ava_hkid'])
									{
										$sql .= "HKID, ";								
									}
									if($special_feature['ava_strn'])
									{
										$sql .= "STRN, ";								
									}
									$sql .= "Barcode, DateInput, DateModified)
	                                        VALUES ('".$this->UserLogin."', '".$target_email."'
	                                        $password_value , '".$this->FirstName."'
	                                        , '".$this->LastName."', '".$this->EnglishName."'
	                                        , '".$this->ChineseName."', ''
	                                        , '".$this->Title."', '".$this->Gender."', '".$this->DOB."'
	                                        , '".$this->Home."', '".$this->Office."', '".$this->Mobile."'
	                                        , '".$this->Fax."', '".$this->ICQ."', '".$this->Address."'
	                                        , '".$this->Country."', '".$this->teaching."'
	                                        , '".$this->CardID."', '".$this->TitleEnglish."', '".$this->TitleChinese."'
	                                        , '".$this->record_status."',$recordType, '$thisWebSamsRegNo', ";
	                                if($special_feature['ava_hkid'])
									{
										$sql .= "'".$this->HKID."',";					
									}       
									if($special_feature['ava_strn'])
									{
										$sql .= "'".$this->STRN."',";					
									}   
									
									$sql .= "'".$this->Barcode."', now(), now())";debug_r($sql);
									
	//                                $sql = "INSERT INTO INTRANET_USER (UserLogin, UserEmail, UserPassword, FirstName, LastName, EnglishName, ChineseName, Title, Gender, DateOfBirth, HomeTelNo, OfficeTelNo, MobileTelNo, FaxNo, ICQNo, Address, Country, ClassNumber, ClassName, ClassLevel, RecordStatus, DateInput, DateModified) VALUES ('".$this->UserLogin."', '".$target_email."', '".$target_password."', '".$this->FirstName."', '".$this->LastName."', '".$this->EnglishName."', '".$this->ChineseName."', '".$this->EnglishName."', '".$this->Title."', '".$this->Gender."', '".$this->DOB."', '".$this->Home."', '".$this->Office."', '".$this->Mobile."', '".$this->Fax."', '".$this->ICQ."', '".$this->Address."', '".$this->Country."', '".$this->ClassNumber."', '".$this->ClassName."', '".$this->ClassLevel."', '".$this->record_status."', now(), now())";
	                                $this->db_db_query($sql);
	                                $recordUserID = $this->db_insert_id();
	                                if ($this->db_affected_rows() > 0)
	                                {
	                                    $this->recordIsValid[$i]=true;
	                                    $this->queries .= "$sql <br> \n";
	                                    $this->debug_msg .= "Insert success <br>\n";
	                                    # ------------------
	                                    # Mark accounts instead
	                                    $temp_array_record = array($i,$this->UserLogin,$target_password,$recordUserID);
	                                    $this->os_accounts_to_open[] = $temp_array_record;
	                                    # ----------------
	                                    if ($intranet_authentication_method=='LDAP')
	                                    {
	                                        if ($this->lldap->isAccountControlNeeded())
	                                        {
	                                            $this->lldap->openAccount($this->UserLogin,$target_password);
	                                        }
	                                    }
	                                    if ($TabID == 2)
	                                    {
	                                         $license_student_left--;
	                                    }
	                                }
                                }
                            }
                            else
                            {
                                /*
                                    $sql = "SELECT UserEmail,UserID FROM INTRANET_USER WHERE UserLogin = '".$this->UserLogin."'";
                                    $result = $this->returnArray($sql,2);
                                    $old_email = $result[0][0];
                                    $recordUserID = $result[0][1];
                                    */
                                $this->debug_msg .= "Try update <br>\n";
                                # Try Update, as the user is already exists

                                # if WebSams = #, then empty the field
								$thisWebSamsRegNo = (trim($this->WebSamsRegNo)=="#") ? "" : trim($this->WebSamsRegNo);
									
                                $update_fields = "";
                                $update_fields .= "FirstName = '".$this->FirstName ."',";
                                $update_fields .= "LastName = '".$this->LastName ."',";
                                $update_fields .= "ChineseName = '".$this->ChineseName ."',";
                                $update_fields .= "EnglishName = '".$this->EnglishName ."',";
                                #$update_fields .= "NickName = '".$this->EnglishName ."',";
                                $update_fields .= "Title = '".$this->Title ."',";
                                $update_fields .= "Gender = '".$this->Gender ."',";
                                $update_fields .= "MobileTelNo = '".$this->Mobile ."',";
                                //$update_fields .= "ClassNumber = '".$this->ClassNumber ."',";
                                //$update_fields .= "ClassName = '".$this->ClassName ."',";
                                #$update_fields .= "ClassLevel = '".$this->ClassLevel ."',";
                                $update_fields .= "RecordStatus = '".$this->record_status ."',";
                                $update_fields .= "Teaching = '".$this->teaching ."',";
                                $update_fields .= "CardID = '".$this->CardID ."',";
                                $update_fields .= "TitleEnglish = '".$this->TitleEnglish ."',";
                                $update_fields .= "TitleChinese = '".$this->TitleChinese ."',";
                                if(!$iportfolio_activated && trim($this->WebSamsRegNo) !=""){
                                	$update_fields .= "WebSAMSRegNo = '".$thisWebSamsRegNo ."',";
	                            }
	                            if($special_feature['ava_hkid'])
	                            {
		                            $update_fields .= "HKID = '".$this->HKID ."',";
	                            }
	                            if($special_feature['ava_strn'])
	                            {
		                            $update_fields .= "STRN = '".$this->STRN ."',";
	                            }
	                            $update_fields .= "Barcode = '".$this->Barcode ."',";
                                $update_fields .= "DateModified = now()";
                                # update email if not empty
                                if ($this->UserEmail != "")
                                {
                                    $update_fields .= ",UserEmail = '".$this->UserEmail ."'";
                                }
                                if ($this->UserPassword !="")
                                {
                                    #$target_password = $this->UserPassword;
                                    if ($intranet_authentication_method == "HASH")
                                    {
                                        $update_fields .= ",UserPassword=NULL,HashedPass = MD5('".$this->UserLogin.$this->UserPassword.$intranet_password_salt."')";
                                    }/*
                                        else if ($intranet_authentication_method == "LDAP")
                                        {
                                        }*/
                                    else
                                    {
                                        $update_fields .= ",UserPassword = NULL ";
                                    }
                                }
                                else
                                {
                                    $target_password = "";
                                }
                                if ($this->DOB != "")
                                {
                                    $update_fields .= ",DateOfBirth = '".$this->DOB."'";
                                }

                                $conds = "UserLogin = '".$this->UserLogin."'";

                                $sql = "UPDATE INTRANET_USER SET $update_fields WHERE $conds";
                                debug_pr($sql);
                                $this->db_db_query($sql);
                                $this->queries .= "$sql <br> \n";
                                if ($this->db_affected_rows()> 0)
                                {
                                    $this->recordIsValid[$i] = true;
                                    #$lc->eClassUserUpdateBriefProfile($old_email, $this->Title, $this->EnglishName, $this->UserPassword,$this->ClassName,$this->ClassNumber, $this->UserEmail, $this->Gender);
                                    $lc->eClassUserUpdateInfoImport($old_email, $this->Title, $this->EnglishName,$this->ChineseName,$this->FirstName,$this->LastName, $this->UserPassword,$this->ClassName,$this->ClassNumber, $this->UserEmail, $this->Gender);
                                    //  $lc->eClassUserUpdateProfile($old_email, $this->Title, $this->FirstName, $this->LastName, $this->FirstName, $target_password,$this->ClassName,$this->ClassNumber, $this->UserEmail);
                                    $this->debug_msg .= "Update success <br>\n";
                                    $this->db = $intranet_db;

                                    if ($this->UserPassword != "")
                                    {
                                        // Update account password

                                        # Email
                                        $this->lwebmail->change_password($this->UserLogin,$this->UserPassword,"iMail");

                                        # Files
                                        if ($plugin['personalfile'])
                                        {
                                            if ($personalfile_type == 'FTP')
                                            {
                                                $this->lftp->changePassword($this->UserLogin,$this->UserPassword,"iFolder");
                                            }
                                        }

                                        # LDAP
                                        if ($intranet_authentication_method=='LDAP')
                                        {
                                            if ($this->lldap->isPasswordChangeNeeded())
                                            {
                                                $this->lldap->changePassword($this->UserLogin,$this->UserPassword,"iFolder");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    $this->recordIsValid[$i] = false;
                                    $this->reason [$i] = "Data updates failed.";
                                    $this->debug_msg .= "All failed <br>\n";
                                    # add to error
                                }

                            }

                            # After Insert/Update
                            # Update ACL
                            if ($recordUserID != "")
                            {
                                $acl_value = $this->acl_value;
                                //$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = $acl_value WHERE UserID = $recordUserID";
                                //$this->db_db_query($sql);
                                //if ($this->db_affected_rows()==0)
                                //{
                                    $sql = "INSERT IGNORE INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($recordUserID, $acl_value)";
                                  	$this->db_db_query($sql);
                                //}
                                # Store UserID in array
                                $this->new_account_userids[] = $recordUserID;
                            }
                            else
                            {
                            }


                            # Create Parent-Student relationship
                            if ($recordType==3 && $this->recordIsValid[$i])
                            {

                                # Find UserID of Parent
                                $sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '".$this->UserLogin."'";
                                $temp = $this->returnVector($sql);
                                $parentID = $temp[0];

                                # Remove Old connection first
                                $sql = "DELETE FROM INTRANET_PARENTRELATION WHERE ParentID = $parentID";
                                $this->db_db_query($sql);
                                # Locate children
                                /*
                                $childrenLogin = array($row[$i][13],$row[$i][16],$row[$i][19]);
                                $childrenName = array($row[$i][14],$row[$i][17],$row[$i][20]);
                                $childrenClass = array($row[$i][15],$row[$i][18],$row[$i][21]);
                                */
                                $conds = "";
                                $delimiter = "";
                                for ($j=0; $j<3; $j++)
                                {
                                     if ($this->childrenLogin[$j]!= "")
                                     {
//                                          $conds .= "$delimiter (UserLogin = '".$this->childrenLogin[$j]."'
//                                                     AND TRIM(EnglishName) = TRIM('".$this->childrenName[$j]."')) ";
										 $conds .= "$delimiter (UserLogin = '".$this->childrenLogin[$j]."') ";
                                         $delimiter = "OR";
                                     }
                                }

                                if ($conds == "")
                                {
                                    continue;
                                }
                                else
                                {
                                    $conds = "AND ($conds)";
                                    $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 $conds";
                                    $childrens = $this->returnVector($sql);
                                    # Remove current relation
                                    $sql = "DELETE FROM INTRANET_PARENTRELATION WHERE ParentID = $parentID";
                                    $this->db_db_query($sql);

                                    # Make insert query
                                    $sql = "INSERT INTO INTRANET_PARENTRELATION (ParentID,StudentID) VALUES ";
                                    $delimiter = "";
                                    for ($j=0; $j<sizeof($childrens); $j++)
                                    {
                                         $sql .= "$delimiter ($parentID, ".$childrens[$j].")";
                                         $delimiter = ",";
                                    }
                                    $this->db_db_query($sql);
                                }

                            }
                                /*
                                # update group - class name
                                if($this->ClassName <> ""){
                                   $sql = "INSERT INTO INTRANET_GROUP (Title, RecordType, DateInput, DateModified) VALUES ('".$this->ClassName."', 3, now(), now())";
                                   $this->db_db_query($sql);
                                }
                                # update group - class level
                                if($this->ClassLevel <> ""){
                                   $sql = "INSERT INTO INTRANET_GROUP (Title, RecordType, DateInput, DateModified) VALUES ('".$this->gpLevel."', 3, now(), now())";
                                   $this->db_db_query($sql);
                                }
                                */
                        }
                        else
                        {
                            $this->recordIsValid[$i]=false;
                            $this->reason[$i] = "Invalid Login/Email";
                        }

                        if ( $license_student_left<=0 && $TabID==2 && $i!=sizeof($row)-1 )
                        {
                             for ($j=$i+1; $j<sizeof($row); $j++)
                             {
                                  $this->recordIsValid[$j]=false;
                                  $this->reason[$j] = $i_license_sys_msg_none;
                             }
                             $i=sizeof($row);
                        }
                }
                # Open Money accounts
                if ($plugin['payment'] && ($recordType==2 || $recordType==1))
                {
                    $sql = "INSERT IGNORE INTO PAYMENT_ACCOUNT (StudentID, Balance)
                            SELECT UserID , 0 FROM INTRANET_USER WHERE RecordType = 2 OR RecordType = 1 ";
                    $this->db_db_query($sql);
                }
        }

		# modified process_user, used in AccountMgmt
 		function process_user2($CheckOnly=0){
 				include_once("libauth.php");
				
                global $intranet_db,$plugin,$personalfile_type, $intranet_root, $eclass_db;
                global $TabID,$ceesConfig,$config_school_code;              # User RecordType
                global $intranet_authentication_method, $intranet_password_salt;
                global $personalfile_type, $i_license_sys_msg_none;
                global $account_license_student, $special_feature;
                global $Lang, $startExtraInfo, $sys_custom, $SYS_CONFIG, $system_reserved_account, $ck_course_id, $ssoservice;
                #Siuwan 20130828 Set $ck_course_id before init $lpf_sturec
				include_once('libpf-sturec.php');
				$sql = "Select course_id From $eclass_db.course Where RoomType = 4";
				$ary = $this->returnVector($sql);
				$ck_course_id = $ary[0];
				$lpf_sturec = new libpf_sturec();
                include_once("libuser.php");
        		$lu = new libuser();
        		$nextUserId = $lu->returnNextUserId();
                
                $isKIS = $_SESSION["platform"]=="KIS";
                
                $recordType = $TabID;
                
				$dbModifiedByUserId = ($_SESSION['UserID']=='')? 'null' : $_SESSION['UserID'];
				
				$libauth = new libauth();
				


				$SettingNameArr = array();
				$SettingNameArr[] = 'EnablePasswordPolicy_'.$recordType;
				$SettingArr = $lu->RetrieveUserInfoSettingInArrary($SettingNameArr);
				$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$recordType];
				if ($PasswordLength<6) $PasswordLength = 6;
				
				$acl = 0;
                $file_content = get_file_content($intranet_root."/file/account_file_quota.txt");
                if ($file_content == "")
                {
                	// 1 Staff, 2 Student, 3 Parent, 4 Alumni
                    $userquota = array(10,10,10,10);
                }
                else
                {
                    $userquota = explode("\n", $file_content);
                }
                $this->file_quota = $userquota[$recordType-1];
	
				if($plugin['imail_gamma'])
				{
					include_once("imap_gamma.php");
					$IMap = new imap_gamma(1);
					
					$this->mail_quota = $IMap->default_quota[$recordType-1]; 
				}
				else
				{
					//campusmail_set
	                $file_content = get_file_content($intranet_root."/file/account_mail_quota.txt");
	                //$file_content = get_file_content($intranet_root."/file/campusmail_set.txt");
	                if ($file_content == "")
	                {
	                	// 1 Staff, 2 Student, 3 Parent, 4 Alumni
	                    $userquota = array(10,10,10,10);
	                    
	                }
	                else
	                {
	                    $userquota = explode("\n", $file_content);
	                    /*
	                    $userquota = explode("\n", $file_content);
                         
                        $quotaline = $userquota[$TabID-1];
                        
                        $temp = explode(":",$quotaline);
                        $quota = $temp[1];
                        if ($quota== "") $quota = 10;
                        $this->mail_quota = $quota;
                        */
	                }
	                $this->mail_quota = $userquota[$recordType-1];
	                
	                $file_content = get_file_content($intranet_root."/file/campusmail_set.txt");
				    if ($file_content == "")
				    {
				        $quota = 10;
				    }
				    else
				    {
				        $userquota = explode("\n", $file_content);
			            //$quotaline = $userquota[TYPE_TEACHER-1];
			            if($TabID==3){
				        	$quotaline = $userquota[$TabID]; // Parent
				        }elseif($TabID==4){
				        	$quotaline = $userquota[$TabID-2]; // Alumni
				        }else{
				        	$quotaline = $userquota[$TabID-1]; // Staff or Student
				        }
				        $temp = explode(":",$quotaline);
				        $quota = $temp[1];
				    }
				}
				$acl += ($this->open_webmail? 1:0);
                $row = $this->data_array;
                
               
                
                
                $acl += ($this->open_file_account? 2: 0);
                $this->acl_value = $acl;

                $lc = new libeclass();
				$license_student_left = 9999;
				if (isset($account_license_student) && $account_license_student>0 && $TabID==2)
				{
					
					$license_student_used = $lc->getLicenseUsed();
					$license_student_left = $account_license_student - $license_student_used;
				}

				
				if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah" && $TabID==2){
				    
				    ## get primary school code from central
				    include_once('cees/libcees.php');	 
				    include_once('cees/ceesConfig.inc.php');
				    $lcees = new libcees();
				    $SchoolInfoAry = $lcees->getAllSchoolInfoFromCentralServer('P');
				  
				    $SchoolCodeAry = array();
				    foreach((array)$SchoolInfoAry as $SchoolInfo){
				        $SchoolCodeAry[] = $SchoolInfo['SchoolCode'];
				    }
			
				}
				
				// initial array to store email/hkid/websams/strn/smartcardid in order to check uniqueness
				$ImportedEmail = array();
				$ImportedHKID = array();
				$ImportedSTRN = array();
				$ImportedWebSams = array();
				$ImportedSmartCard = array();
				
                for($i=0; $i<sizeof($row); $i++){
                	
                        $mail_failed = 0;
                        $file_failed = 0;
 						if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
 							if (!$this->set_row_array_ncs($row[$i]))
 							{
 								$this->recordIsValid[$i]=false;
 								$this->reason[$i]="";
 								continue;
 							}
 						}else{
	                        if (!$this->set_row_array($row[$i]))
	                        {
	                             $this->recordIsValid[$i]=false;
	                             $this->reason[$i]="";
	                             continue;
	                        }
 						}
                        /*
                        # update user record
                        if ($this->ClassName != "" && !in_array($this->ClassName,$this->class_list))
                        {
                            $this->recordIsValid[$i]=false;
                            $this->reason[$i] = "Invalid ClassName for Student";
                            continue;
                        }*/

						
						if (trim($this->UserLogin) == '') {
                            $this->recordIsValid[$i]=false;
                            $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['MissedLogin'];
						}

                        ####################################################################
                        # Check whether it is an old account
                        $sql = "SELECT UserEmail,UserID,RecordType FROM INTRANET_USER WHERE UserLogin = '".$this->UserLogin."'";
                        $temp = $this->returnArray($sql,3);
                        if (sizeof($temp)!=0)      # Account update
                        {
                            $isNewAccount = false;
                            //if($this->UserEmail == "" || $this->validateEmail($this->UserEmail) )
                            if(!$this->validateLogin($this->UserLogin))
                            {
                            	$login_valid = false;
	                        	$email_valid = false;
	                            $this->recordIsValid[$i]=false;
	                            $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['InvalidLoginEmail'];
                            }else if($this->UserEmail != "" )
                            {
                               $email_valid = true;
                            }
                            else
	                        {
	                        	$email_valid = false;
	                            $this->recordIsValid[$i]=false;
	                            $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['InvalidEmail'];
	                          //  continue;
	                        }
	                        if(/*$sys_custom['ImportStudentAccountActionType'] && */ $recordType == USERTYPE_STUDENT && $this->action_type == 1){ // action type : add
	                        	$this->recordIsValid[$i]=false;
	                        	$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['UserAccountExistForAddAction'];
	                        }
	                        
	                        list($old_email,$recordUserID,$user_type) = $temp[0];
                        }
                        else
                        {
                            $isNewAccount = true;
                            
                            //if ($this->validateLogin($this->UserLogin) && ($this->UserEmail == "" || $this->validateEmail($this->UserEmail) ))
                            if ($this->validateLogin($this->UserLogin) && $this->UserEmail!="")
                            {
                                $login_valid = true;
                                $email_valid = true;
                            }
                            else
	                        {
	                        	$login_valid = false;
	                        	$email_valid = false;
	                            $this->recordIsValid[$i]=false;
	                            $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['InvalidLoginEmail'];
		                        
	                          //  continue;
	                        }
	                        
	                        if(/*$sys_custom['ImportStudentAccountActionType'] && */ $recordType == USERTYPE_STUDENT && $this->action_type == 2){ // action type : update
	                        	$this->recordIsValid[$i]=false;
	                        	$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['UserAccountNotExistForUpdateAction'];
	                        }
	                    }
	                    
	                    if($isNewAccount && trim($this->UserPassword) == ""){
	                    	$this->recordIsValid[$i]=false;
	                        $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['NewAccountPasswordCannotBeEmpty'];
	                    }
	                    
	                    // check password
	                    if($sys_custom['UseStrongPassword'] && trim($this->UserPassword) != ''){
	                    	$check_password_result = $libauth->CheckPasswordCriteria($this->UserPassword,$this->UserLogin,$PasswordLength);
							if(!in_array(1,$check_password_result)){
								$this->recordIsValid[$i]=false;
								foreach($check_password_result as $check_password_error_code){
	                       			$this->reason[$i][] = $Lang['AccountMgmt']['PasswordCheckingWarnings'][$check_password_error_code];
								}
							}
	                    }
	                    
	                    # check have English Name or not
	                    if (trim($this->EnglishName) == '') {
	                    	$this->recordIsValid[$i]=false;
                        	$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['MissedEnglishName'];
	                    }

						# check email existence
						if($email_valid && $this->UserEmail != "")
						{
							# check whether the email are duplicated in current import 
							if(in_array($this->UserEmail,$ImportedEmail))
							{
								$this->recordIsValid[$i]=false;
		                		$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['EmailUsedInImport'];
							}	
							else
							{	
								$ImportedEmail[] = $this->UserEmail;
			                    
			                    # check whether email is used by other user
			                    $target_email = ($this->UserEmail==""? $this->UserLogin . "@".$this->domain_name : $this->UserEmail);
			                    $target_email = substr($target_email,0,70);
			                	$temp_sql = "select UserID from INTRANET_USER where UserEmail='$target_email' AND UserLogin <> '$this->UserLogin'";
			                    $temp_result = $this->returnArray($temp_sql);
			                    $dupUserID = $temp_result[0][0];
			
			                    if($dupUserID)
			                    {
			                        $this->recordIsValid[$i]=false;
			                		$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['EmailUsed'];
			                		// continue;
			                    }
							}
						}

						### check quota
	                    if ($license_student_left<=0 && $TabID==2 && $isNewAccount )
                        {
                              $this->recordIsValid[$i]=false;
                              $this->reason[$i][] = $i_license_sys_msg_none;
                        }

                        ### check user type
                        if(!$isNewAccount){
	                        if($user_type != $recordType){ ## the usertype of the existing user is different from the import usertype
		                        $this->recordIsValid[$i]=false;
		                        $this->reason[$i][] =$Lang['AccountMgmt']['ErrorMsg']['DifferentUserType'];
		                     //   continue;
		                    }
	                    }

                        ####### handle Smart Card ID ###############
                        if(trim($this->CardID)!="" && $TabID!=4){
                        	if(in_array($this->CardID,$ImportedSmartCard))
							{
								$this->recordIsValid[$i]=false;
		                		$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsedInImport'];
							}	
							else
							{	
								$ImportedSmartCard[] = $this->CardID;
		                        $sql="SELECT UserID FROM INTRANET_USER WHERE (CardID='".$this->CardID."'".($sys_custom['SupplementarySmartCard']?" OR CardID2='".$this->CardID."' OR CardID3='".$this->CardID."' OR CardID4='".$this->CardID."'":"").") AND UserLogin!='".$this->UserLogin."'";
		                        $cardid_temp = $this->returnVector($sql);
		                        if(sizeof($cardid_temp)!=0){  # card id exists
			                        $this->recordIsValid[$i]=false;
			                        $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsed'];
			                      //  continue;
			                    }
							}
	                    }
	                    if($sys_custom['SupplementarySmartCard'] && $TabID==2 && trim($this->CardID2)!="" && $TabID!=4){ // Student Only
                        	if(in_array($this->CardID2,$ImportedSmartCard))
							{
								$this->recordIsValid[$i]=false;
		                		$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsedInImport'];
							}	
							else
							{	
								$ImportedSmartCard[] = $this->CardID2;
		                        $sql="SELECT UserID FROM INTRANET_USER WHERE (CardID='".$this->CardID2."' OR CardID2='".$this->CardID2."' OR CardID3='".$this->CardID2."' OR CardID4='".$this->CardID2."') AND UserLogin!='".$this->UserLogin."'";
		                        $cardid_temp = $this->returnVector($sql);
		                        if(sizeof($cardid_temp)!=0){  # card id exists
			                        $this->recordIsValid[$i]=false;
			                        $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsed'];
			                      //  continue;
			                    }
							}
	                    }
	                    if($sys_custom['SupplementarySmartCard'] && $TabID==2 && trim($this->CardID3)!="" && $TabID!=4){ // Student Only
                        	if(in_array($this->CardID3,$ImportedSmartCard))
							{
								$this->recordIsValid[$i]=false;
		                		$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsedInImport'];
							}	
							else
							{	
								$ImportedSmartCard[] = $this->CardID3;
		                        $sql="SELECT UserID FROM INTRANET_USER WHERE (CardID='".$this->CardID3."' OR CardID2='".$this->CardID3."' OR CardID3='".$this->CardID3."' OR CardID4='".$this->CardID3."') AND UserLogin!='".$this->UserLogin."'";
		                        $cardid_temp = $this->returnVector($sql);
		                        if(sizeof($cardid_temp)!=0){  # card id exists
			                        $this->recordIsValid[$i]=false;
			                        $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsed'];
			                      //  continue;
			                    }
							}
	                    }
	                    if($sys_custom['SupplementarySmartCard'] && $TabID==2 && trim($this->CardID4)!="" && $TabID!=4){ // Student Only
                        	if(in_array($this->CardID4,$ImportedSmartCard))
							{
								$this->recordIsValid[$i]=false;
		                		$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsedInImport'];
							}	
							else
							{	
								$ImportedSmartCard[] = $this->CardID4;
		                        $sql="SELECT UserID FROM INTRANET_USER WHERE (CardID='".$this->CardID4."' OR CardID2='".$this->CardID4."' OR CardID3='".$this->CardID4."' OR CardID4='".$this->CardID4."') AND UserLogin!='".$this->UserLogin."'";
		                        $cardid_temp = $this->returnVector($sql);
		                        if(sizeof($cardid_temp)!=0){  # card id exists
			                        $this->recordIsValid[$i]=false;
			                        $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsed'];
			                      //  continue;
			                    }
							}
	                    }
	                    
	                    if($TabID != 3 && !$sys_custom['project']['HKPF'])
	                    {
    	                    if(trim($this->Gender) == "")
    	                    {
    							$this->recordIsValid[$i] = false;
    	                		$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['GenderCannotBeEmpty'];
    	                    }
    	                    // [2018-1029-1009-38066] Gender checking (M / F)
    	                    else if(trim($this->Gender) != "M" && trim($this->Gender) != "F")
    	                    {
    	                        $this->recordIsValid[$i] = false;
    	                        $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['InvalidGenderFormat'];
    	                    }
	                    }
						
						if(!$isKIS)
						{
							# check HKJApplNo is existing or not 
							if(trim($this->HKJApplNo) == "" || $this->HKJApplNo == 0){
								//skip checking
							}else{
								$sql = "select userid from INTRANET_USER WHERE HKJApplNo=".$this->HKJApplNo." AND UserLogin!='".$this->UserLogin."'";
	
								$HKJApplNo_temp = $this->returnVector($sql);
								if(sizeof($HKJApplNo_temp) != 0 ){ ## HKJApplNo exists
									$this->recordIsValid[$i]=false;
					                $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['HKJApplNoUserd'];				            
								}
							}
						}

	                    # check HKID is existing or not (if include HKID field)
						if($special_feature['ava_hkid'])
						{
							if($this->HKID != "")
							{
								if(in_array($this->HKID,$ImportedHKID))
								{	
									$this->recordIsValid[$i]=false;
			                		$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['HKIDUsedInImport'];
								}	
								else
								{	
									$ImportedHKID[] = $this->HKID;
									$sql="SELECT UserID FROM INTRANET_USER WHERE HKID='".$this->HKID."' AND UserLogin!='".$this->UserLogin."'";
									$hkid_temp = $this->returnVector($sql);
									if(sizeof($hkid_temp)!=0){ ## HKID exists
										$this->recordIsValid[$i]=false;
				                        $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['HKIDUsed'];
				                      //  continue;
									}
								}
							}
						}
						
						# check STRN is existing or not (if include STRN field)
						if($special_feature['ava_strn'])
						{
							if($this->STRN != "")
							{
								if(in_array($this->STRN,$ImportedSTRN))
								{
									$this->recordIsValid[$i]=false;
			                		$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['STRNUsedInImport'];
								}	
								else
								{	
									$ImportedSTRN[] = $this->STRN;
									$sql="SELECT UserID FROM INTRANET_USER WHERE STRN='".$this->STRN."' AND UserLogin!='".$this->UserLogin."'";
									$strn_temp = $this->returnVector($sql);
									
									if(sizeof($strn_temp)!=0){ ## STRN exists
										$this->recordIsValid[$i]=false;
				                        $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['STRNUsed'];
				                       // continue;
									}
								}
							}
						}

						if($sys_custom['BIBA_AccountMgmtCust'] && in_array($TabID,array(TYPE_STUDENT,TYPE_TEACHER))){
							if($this->House != ""){
								$AcademicYearID = Get_Current_Academic_Year_ID();
								$house_ary = explode(",",$this->House);
								if(count($house_ary)>0)
								{
									foreach($house_ary as $house)
									{
										$sql = "SELECT COUNT(1) FROM INTRANET_GROUP WHERE AcademicYearID='$AcademicYearID' AND RecordType='4' AND (Title='".$this->Get_Safe_Sql_Query($house)."' OR TitleChinese='".$this->Get_Safe_Sql_Query($house)."')";
										$house_title_record = $this->returnVector($sql);
										if($house_title_record[0] == 0){
											$this->recordIsValid[$i]=false;
						                    $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['HouseNotFound'];
										}
									}
								}
							}
						}

                        $iportfolio_activated = false;
                        ####### handle Websams Reg No ##############
                        if($TabID==2 && !$isKIS /* &&  trim($this->WebSamsRegNo)!="" */){
                                # check websams regno format
				                if(trim($this->WebSamsRegNo)!="" && substr(trim($this->WebSamsRegNo),0,1)!="#"){ # invalid WebSamsRegNo
					                	$this->recordIsValid[$i]=false;
					                	$this->reason[$i][] =$Lang['AccountMgmt']['ErrorMsg']['PleaseAddSymbol'];
					                	//continue;
					            }
								
					            if(strlen(trim($this->WebSamsRegNo))>1)
					            {
					            	if(in_array($this->WebSamsRegNo,$ImportedWebSams))
									{
										$this->recordIsValid[$i]=false;
				                		$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['WebSamsRegNoUsedInImport'];
									}	
									else
									{	
										$ImportedWebSams[] = $this->WebSamsRegNo;
		  		                        $sql="SELECT UserID FROM INTRANET_USER WHERE WebSamsRegNo='".$this->WebSamsRegNo."' AND UserLogin!='".$this->UserLogin."'";
				                        $websams_temp = $this->returnVector($sql);
				                        if(sizeof($websams_temp)!=0){ 		# WebSamsRegNo exists
				                                   $this->recordIsValid[$i]=false;
		                            			   $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['WebSamsRegNoUsed'];
		                            			   //continue;
					                    }
									}
			                    }

	                        if($isNewAccount){
		                        # do nothing
		                    }else{
       	                        $sql = "SELECT UserID,WebSamsRegNo FROM INTRANET_USER WHERE UserLogin = '".$this->UserLogin."'";
      	                        $websams_temp = $this->returnArray($sql,2);
								list($temp_userid,$temp_websams_regno)=$websams_temp[0];
								
      	                        if($plugin['iPortfolio']){
           	                        $iportfolio_data = $this->iportfolio->returnAllActivatedStudent();
	      	                        for($x=0;$x<sizeof($iportfolio_data);$x++){
		      	                        if($temp_userid == $iportfolio_data[$x][0]){ # iPortfolio activated
			      	                        $iportfolio_activated = true;
			      	                        break;
			      	                    }
		      	                    }
		      	                }
	      	                    if($iportfolio_activated && trim($temp_websams_regno)!=trim($this->WebSamsRegNo) ){
   		                                   $this->recordIsValid[$i]=false;
                            			   $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['WebSamsRegNoCannotChange'];
                            			  // continue;

	      	                    }
		                    }

	                    }

						//	check whether available license is enough for new account
 						if ($isNewAccount)
                        {
	                        if (empty($this->reason[$i]) &&  $TabID==2)   
	                        	$license_student_left--;
                        }
                        
                        if($TabID==3) // check children existence
                        {
	                        for ($j=0; $j<3; $j++)
	                        {
	                             if ($this->childrenLogin[$j]!= "")
	                             {
// 	                                 $conds = " (UserLogin = '".$this->childrenLogin[$j]."'
// 	                                            AND TRIM(EnglishName) = TRIM('".$this->childrenName[$j]."')) ";
	                                  $conds = " (UserLogin = '".$this->childrenLogin[$j]."') ";
			                        $sql = "SELECT count(*) FROM INTRANET_USER WHERE RecordType = 2 AND ($conds)";
			                        $result = $this->returnVector($sql);

									if(empty($result))
			                        {
		                               $this->recordIsValid[$i]=false;
		                			   $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['CannotFindStudent']." ".$this->childrenLogin[$j];
			                        }		
	                             }
	                        }
                        }
                        
                        if($sys_custom['iMailPlus']['EmailAliasName'] && $TabID==1 && $this->ImapUserLogin!="" && $this->ImapUserLogin != $this->UserLogin)
                        {
                        	if(!intranet_validateEmail($this->ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'])){
                        		$this->recordIsValid[$i]=false;
		                		$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['InvalidEmail'].": ".$this->ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
                        	}else if(in_array($this->ImapUserLogin, (array)$system_reserved_account)){
                        		$this->recordIsValid[$i]=false;
		                		$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['EmailUsed'].": ".$this->ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
                        	}else if($IMap->is_user_exist($this->ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'])){
                        		$this->recordIsValid[$i]=false;
		                		$this->reason[$i][] = $this->ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'].$Lang['AccountMgmt']['UsedByOtherUser'];
                        	}else{
                        		// Check any conflict with existing userlogin
								$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='".$this->ImapUserLogin."'";
								$temp = $this->returnVector($sql);
                        		
                        		if(sizeof($temp)>0){
                        			$this->recordIsValid[$i]=false;
		                			$this->reason[$i][] = $this->ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'].$Lang['AccountMgmt']['UsedByOtherUser'];
                        		}
                        	}
                        } 
                        
	                    # check if DateOfBaptism is valid date or not
	                    if ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism'] && (trim($this->Fax) != '') && (trim($this->Fax) != '0000-00-00')) {
	                    	if (!checkDateIsValid($this->Fax)) {
		                    	$this->recordIsValid[$i]=false;
	                        	$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['InvalidDateFormat'];
	                    	}
	                    }
                        
                        # check if DOB & Admission Date & Graduation Date is valid date or not if they are not empty
                        if($TabID == 2){	// for student only
                            if($sys_custom['AccountMgmt']['StudentDOBRequired']){
                                if($this->DOB == '' || empty($this->DOB)){
                                    $this->recordIsValid[$i] = false;
                                    $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['MissedDOB'];
                                }
                            }

                        	if ((trim($this->DOB) != '') && (trim($this->DOB) != '0000-00-00')) {
		                    	if (!checkDateIsValid($this->DOB)) {
			                    	$this->recordIsValid[$i] = false;
		                        	$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['InvalidDateFormat'];
		                    	}
                        	}
                        	
                        	if ((trim($this->AdmissionDate) != '') && (trim($this->AdmissionDate) != '0000-00-00')) {
		                    	if (!checkDateIsValid($this->AdmissionDate)) {
			                    	$this->recordIsValid[$i] = false;
		                        	$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['InvalidDateFormat'];
	                        	    //echo '3'.$this->AdmissionDate.']';
		                    	}
                        	}

                        	// [2020-0220-1623-28098] Checking for Graduation Date
                            if ((trim($this->GraduationDate) != '') && (trim($this->GraduationDate) != '0000-00-00')) {
                                if (!checkDateIsValid($this->GraduationDate)) {
                                    $this->recordIsValid[$i] = false;
                                    $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['InvalidDateFormat'];
                                    //echo '3'.$this->GraduationDate.']';
                                }
                            }
                        }
                        
                        // check Primary School Code is avaible or not 
                        if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah" && $TabID == 2){               
                            if(in_array($this->PrimarySchoolCode, $SchoolCodeAry) || $this->PrimarySchoolCode ==''){                              
                           }else{
                               $this->recordIsValid[$i]=false;
                               $this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['PrimarySchoolCodeNotFound'];
                           }
                            
                        }
                        
                        /*if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!="" && $TabID == 2){
                            $langs = explode(",",$this->LanguagePriority);
                            foreach($langs as $lang){
                                $lang = trim(strtoupper($lang));
                                if(!in_array($lang, $Lang['AccountMgmt']['LanguageCode'])){
                            		$this->recordIsValid[$i]=false;
                            		$this->reason[$i][] = $Lang['AccountMgmt']['ErrorMsg']['LanguageCodeWrong'];
                                }
                            }
                        }*/
                        
						if($CheckOnly==1) // skip insert
							continue;
                         ####################################################################

                        #if( $this->validateLogin($this->UserLogin) && ($this->UserEmail == "" ||$this->validateEmail($this->UserEmail) )){
                        if (empty($this->reason[$i]))
                        {
                            if ($isNewAccount)
                            {
                                $target_email = ($this->UserEmail==""? $this->UserLogin . "@".$this->domain_name : $this->UserEmail);
                                $target_email = substr($target_email,0,70);
                                $target_password = ($this->UserPassword ==""? intranet_random_passwd(8): $this->UserPassword);
                                if ($intranet_authentication_method == "HASH")
                                {
                                    $password_field = ",UserPassword,HashedPass ";
                                    $password_value = ",NULL,MD5('".$this->UserLogin.$target_password.$intranet_password_salt."') ";
                                }
                                /*
                                else if ($intranet_authentication_method == "LDAP")
                                {
                                     $password_field = "";
                                     $password_value = "";
                                }*/
                                else
                                {
                                    $password_field = ", UserPassword";
                                    $password_value = ", NULL ";
                                }
                                
                                
                                
//                                # check email is duplicate or not (20080911 - Yat Woon)
//                                $temp_sql = "select UserID from INTRANET_USER where UserEmail='$target_email'";
//                                $temp_result = $this->returnArray($temp_sql);
//                                $dupUserID = $temp_result[0][0];
//                                if($dupUserID)
//                                {
//	                                $this->recordIsValid[$i]=false;
//                            		$this->reason[$i] = "Email is already exists.";
//                                }
//                                else
//                                {
									# if WebSams = #, then empty the field
									$thisWebSamsRegNo = (trim($this->WebSamsRegNo)=="#") ? "" : trim($this->WebSamsRegNo);
	             
									/*
									$sql = "INSERT INTO INTRANET_USER (UserLogin, UserEmail
	                                        $password_field, FirstName, LastName, EnglishName,
	                                        ChineseName, NickName, Title, Gender, DateOfBirth,
	                                        HomeTelNo, OfficeTelNo, MobileTelNo, FaxNo, ICQNo,
	                                        Address, Country, ClassNumber, ClassName, Teaching,
	                                        CardID, TitleEnglish, TitleChinese,
	                                        RecordStatus, RecordType, WebSAMSRegNo, ";
	                                        */
									$sql = "INSERT INTO INTRANET_USER (UserID, UserLogin, UserEmail
	                                        $password_field, EnglishName,
	                                        ChineseName, NickName, Gender, DateOfBirth, Remark, 
	                                        HomeTelNo, OfficeTelNo, MobileTelNo, FaxNo, ICQNo,
	                                        Country, Teaching,
	                                        CardID,".($sys_custom['SupplementarySmartCard'] && $TabID==2?"CardID2,CardID3,CardID4,":"")." TitleEnglish, TitleChinese,
	                                        RecordStatus, RecordType, WebSAMSRegNo, HKJApplNo, ";
	                                if($TabID == 2){
	                                	$sql .= " Address, ";
	                                }
	                                //if($special_feature['StaffCode']) {
                                		$sql .= " StaffCode, ";	
                                	//}        
									if($special_feature['ava_hkid'])
									{
										$sql .= "HKID, ";								
									}
									if($special_feature['ava_strn'])
									{
										$sql .= "STRN, ";								
									}
									if($ssoservice["Google"]["Valid"])
									{
										$sql .= "GoogleUserLogin, ";								
									}
									if($TabID==2 && $sys_custom['StudentAccountAdditionalFields']){
										$sql .= " NonChineseSpeaking,SpecialEducationNeeds,PrimarySchool,University,Programme, ";
									}
									if($sys_custom['BIBA_AccountMgmtCust'] && $TabID==TYPE_STUDENT){
										$sql .= " HouseholdRegister, ";
									}
									if ($sys_custom['project']['HKPF'] && $TabID == TYPE_TEACHER) {
									    $sql .= " Grade, Duty, ";
									}
									
									if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah" && $TabID == TYPE_STUDENT){
									    
									    $sql .= " PrimarySchoolCode, ";
									}
									$sql .= "Barcode, DateInput, DateModified, InputBy, ModifyBy)
	                                        VALUES ('".$nextUserId."', '".$this->UserLogin."', '".$target_email."'
	                                        $password_value , '".$this->EnglishName."'
	                                        , '".$this->ChineseName."', '".$this->NickName."'
	                                        , '".$this->Gender."', '".$this->DOB."', '".$this->Remark."'
	                                        , '".$this->Home."', '".$this->Office."', '".$this->Mobile."'
	                                        , '".$this->Fax."', '".$this->ICQ."'
	                                        , '".$this->Country."', '".$this->teaching."'
	                                        , '".$this->CardID."', ".($sys_custom['SupplementarySmartCard'] && $TabID==2?("'".$this->CardID2."','".$this->CardID3."','".$this->CardID4."',"):"")." '".$this->TitleEnglish."', '".$this->TitleChinese."'
	                                        , '".$this->record_status."',$recordType, '$thisWebSamsRegNo','".$this->HKJApplNo."',";
	                                if($TabID == 2){
	                                	$sql .= " '".$this->Address."', ";
	                                }
	                                //if($special_feature['StaffCode']) {
                                		$sql .= "'".$this->StaffCode."', ";	
                                	//}
	                                if($special_feature['ava_hkid'])
									{
										$sql .= "'".$this->HKID."',";					
									}       
									if($special_feature['ava_strn'])
									{
										$sql .= "'".$this->STRN."',";					
									}
									if($ssoservice["Google"]["Valid"])
									{
										$sql .= "'".$this->GoogleUserLogin."',";							
									}
									if($TabID==2 && $sys_custom['StudentAccountAdditionalFields']){
										$sql .= "'".$this->NonChineseSpeaking."',";
										$sql .= "'".$this->SpecialEducationNeeds."',";
										$sql .= "'".$this->PrimarySchool."',";
										$sql .= "'".$this->University."',";
										$sql .= "'".$this->Programme."',";
									}
									if($sys_custom['BIBA_AccountMgmtCust'] && $TabID==TYPE_STUDENT){
										$sql .= "'".$this->HouseholdRegister."',";
									}
									if ($sys_custom['project']['HKPF'] && $TabID == TYPE_TEACHER) {
									    $sql .= "'".$this->Grade."','".$this->Duty."',";
									}
									if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah" && $TabID == TYPE_STUDENT){
									    $sql .= "'".$this->PrimarySchoolCode."',";;
									    
									}
									$sql .= "'".$this->Barcode."', now(), now(), $dbModifiedByUserId, $dbModifiedByUserId)";
	//                                $sql = "INSERT INTO INTRANET_USER (UserLogin, UserEmail, UserPassword, FirstName, LastName, EnglishName, ChineseName, Title, Gender, DateOfBirth, HomeTelNo, OfficeTelNo, MobileTelNo, FaxNo, ICQNo, Address, Country, ClassNumber, ClassName, ClassLevel, RecordStatus, DateInput, DateModified) VALUES ('".$this->UserLogin."', '".$target_email."', '".$target_password."', '".$this->FirstName."', '".$this->LastName."', '".$this->EnglishName."', '".$this->ChineseName."', '".$this->EnglishName."', '".$this->Title."', '".$this->Gender."', '".$this->DOB."', '".$this->Home."', '".$this->Office."', '".$this->Mobile."', '".$this->Fax."', '".$this->ICQ."', '".$this->Address."', '".$this->Country."', '".$this->ClassNumber."', '".$this->ClassName."', '".$this->ClassLevel."', '".$this->record_status."', now(), now())";

	                                $success = $this->db_db_query($sql);
	                                //$recordUserID = $this->db_insert_id();
	                                if ($success) {
	                                	$recordUserID = $nextUserId;
	                                	$nextUserId++;
	                                }
	                                else {
	                                	continue;
	                                }
	                                
									if ($TabID == 2)
									{
										if($plugin['medical'])
										{
											$this->laccount->updateUserPersonalSetting($recordUserID,array('stayOverNight'=>$this->stayOverNight));
										}
										
										if($plugin['SDAS_module']['KISMode'])
										{
											$this->laccount->updateUserPersonalSetting($recordUserID,array('KISClassType'=>$this->KISClassType));
										}

										// [2020-0220-1623-28098] Update Graduation Date
                                        //$this->laccount->updateUserPersonalSetting($recordUserID,array('Nationality'=>$this->Nationality,'PlaceOfBirth'=>$this->PlaceOfBirth,'AdmissionDate'=>$this->AdmissionDate));
										$this->laccount->updateUserPersonalSetting($recordUserID,array('Nationality'=>$this->Nationality,'PlaceOfBirth'=>$this->PlaceOfBirth,'AdmissionDate'=>$this->AdmissionDate,'GraduationDate'=>$this->GraduationDate));
									}

	                                if($TabID == 4)
	                                {
		                                global $alumni_GroupID;

		                             	# add back Classname, Classnumber and YearOfLeft
		                             	$sql = "update INTRANET_USER set 
		                             				ClassName= '".$this->ClassName."',
		                             				ClassNumber= '".$this->ClassNumber."',
		                             				YearOfLeft= '".$this->YearOfLeft ."',
													DateModified= now(),
													ModifyBy= $dbModifiedByUserId
		                             			where
		                             				UserID=$recordUserID";
		                             	 $this->db_db_query($sql);
		                             	 
										# add to alumni group
										$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES ($alumni_GroupID, $recordUserID, now(), now())";
// 										debug_pr($sql);
										$this->db_db_query($sql) or die(mysql_error());
										
										# add to corresponding alumni group according to YearOfLeft
										$lu->AddOrUpdateAlumniYearGroup($recordUserID, trim($this->YearOfLeft));
	                                }

	                                //if ($this->db_affected_rows() > 0)
	                                if($recordUserID > 0)
	                                {
	                                    $this->recordIsValid[$i]=true;
	                                    $this->queries .= "$sql <br> \n";
	                                    $this->debug_msg .= "Insert success <br>\n";
	                                    # ------------------
	                                    # Mark accounts instead
	                                    $temp_array_record = array($i,$this->UserLogin,$target_password,$recordUserID);
	                                    if($TabID==1 && $plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName']){
	                                    	$temp_array_record[] = $this->EmailUserLogin;
	                                    }
	                                    
	                                    $this->os_accounts_to_open[] = $temp_array_record;
	                                    # ----------------
	                                    if ($intranet_authentication_method=='LDAP')
	                                    {
	                                        if ($this->lldap->isAccountControlNeeded())
	                                        {
	                                            $this->lldap->openAccount($this->UserLogin,$target_password);
	                                        }
	                                    }
	                                    if ($TabID == 2)
	                                    {
	                                         //$license_student_left--;
											 # Activate IPF for KIS students
											if($plugin['iPortfolio']&&$_SESSION['platform']=='KIS') {
												$sql = "Update INTRANET_USER Set WebSAMSRegNo = concat('#', UserLogin) Where UserID ='$recordUserID'";
												$this->db_db_query($sql);
												$lpf_sturec->DO_ACTIVATE_STUDENT($recordUserID); 
											}
	                                    }
	                                    
	                                    // Log down encrypted password
	                                    $libauth->UpdateEncryptedPassword($recordUserID,$target_password);
	                                    # Preset quota if iMail module is available
	                                    if($special_feature['imail']){
	                                    	//$sql_setquota = "INSERT INTO INTRANET_CAMPUSMAIL_USERQUOTA (UserID, Quota) VALUES ('$recordUserID', '".$this->mail_quota."')";
	                                    	$sql_setquota = "INSERT INTO INTRANET_CAMPUSMAIL_USERQUOTA (UserID, Quota) VALUES ('$recordUserID', '".$quota."')";
	                                    	$this->db_db_query($sql_setquota);
	                                    	$sql_setusedquota = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed) VALUES ('$recordUserID', 0)";
	                                    	$this->db_db_query($sql_setusedquota);
	                                    }
										
										if($sys_custom['BIBA_AccountMgmtCust'] && in_array($TabID,array(TYPE_STUDENT,TYPE_TEACHER))){
		                                	if($this->House != ''){
		                                		$AcademicYearID = Get_Current_Academic_Year_ID();
		                                		$house_ary = explode(",",$this->House);
		                                		if(count($house_ary)>0)
		                                		{
			                                		foreach($house_ary as $house)
			                                		{
				                                		$sql = "SELECT GroupID FROM INTRANET_GROUP WHERE AcademicYearID='".$AcademicYearID."' AND RecordType='4' AND (Title='".$house."' OR TitleChinese='".$house."')";
				                                		$house_group_record = $this->returnResultSet($sql);
				                                		if(count($house_group_record)==1){
				                                			$sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID='$recordUserID' AND GroupID='".$house_group_record[0]['GroupID']."'";	
					                                		$this->db_db_query($sql);
					                                		
					                                		$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES ('".$house_group_record[0]['GroupID']."', '".$recordUserID."', now(), now())";
					                                		$this->db_db_query($sql);
				                                		}
			                                		}
		                                		}
		                                	}
		                                }
	                                }
                                //}
                                
                                # Extra Info
                                if($sys_custom['UserExtraInformationImport']) {
                                	include_once("libaccountmgmt.php");
                                	$laccount = new libaccountmgmt();
                                	$ExtraInfoCatInfo = $laccount->Get_User_Extra_Info_Category();
									$totalCategory = count($ExtraInfoCatInfo);
									//$startExtraInfo
									$pointer = $startExtraInfo+1;
									
									for($a=0; $a<$totalCategory; $a++) {
										$thisCategoryInfo = $ExtraInfoCatInfo[$a];
										$sql = "Select * From INTRANET_USER_EXTRA_INFO_ITEM WHERE RecordStatus=1 AND CategoryID='".$thisCategoryInfo['CategoryID']."'";	
										$itemAry = $laccount->returnArray($sql);
										$itemAssoArray = array();
										for($b=0; $b<count($itemAry); $b++) {
											$itemAssoArray[trim($itemAry[$b]['ItemCode'])] = trim($itemAry[$b]['ItemID']);	
										}
										
										$importedItem = trim($row[$i][$pointer]);
										
										if($importedItem!="") {
											
											$importedItemAry = explode(',', $importedItem);
											for($p=0; $p<count($importedItemAry); $p++) {
												$thisImportedItemName = trim($importedItemAry[$p]);
												
												// exclude data of option "OTHERS"
												$_tempImportedItem = explode('::', $thisImportedItemName);
												$_tempItemName = trim($_tempImportedItem[0]);
												$_tempOtherField = trim($_tempImportedItem[1]);
												
												// get item code
												$sql = "SELECT ItemCode FROM INTRANET_USER_EXTRA_INFO_ITEM WHERE (ItemNameCh='$_tempItemName' OR ItemNameEn='$_tempItemName') AND CategoryID='".$thisCategoryInfo['CategoryID']."'";
												$ItemCode = $this->returnVector($sql);
												$thisItemCode = $ItemCode[0];
												
												$thisItemID = $laccount->GET_EXTRA_INFO_ITEM_ID_BY_ITEM_NAME($_tempItemName, $thisCategoryInfo['CategoryID']);
												
												if(isset($itemAssoArray[$thisItemCode])) {
													$laccount->INSERT_EXTRA_INFO_MAPPING($recordUserID, $itemID=$thisItemID, $OtherField=$_tempOtherField);	
												}	
											}	
										}
										$pointer++;
									}
                                }
                                
                                # Extra Info 
                                if($sys_custom['StudentMgmt']['BlissWisdom']) {
                                	include_once("libuserpersonalsettings.php");
                                	$libps = new libuserpersonalsettings();
                                	
                                	$Occupation = trim($row[$i][$pointer]); $pointer++;
                                	$Company = trim($row[$i][$pointer]); $pointer++;
                                	$PassportNo = trim($row[$i][$pointer]); $pointer++;
                                	$Passport_ValidDate = trim($row[$i][$pointer]); $pointer++;
                                	$HowToKnowBlissWisdom = explode('::',trim($row[$i][$pointer])); $pointer++;
                                	
                                	$HowToValue = $HowToKnowBlissWisdom[0];
                                	$HowToOthers = $HowToKnowBlissWisdom[1];
                                	
                                	foreach($Lang['AccountMgmt']['KnowAboutBlissWisdomOptions'] as $_id=>$_option) {
                                		if($HowToValue==$_option) {
                                			$HowToID = $_id;
                                			break;	
                                		}
                                	}
                                	
                                	$FieldAry = array('Occupation'=>$Occupation, 'Company'=>$Company, 'PassportNo'=>$PassportNo, 'Passport_ValidDate'=>$Passport_ValidDate, 'HowToKnowBlissWisdom'=>$HowToID.'::'.$HowToOthers);
//                                	debug_pr($FieldAry);
                                	$libps->Save_Setting($recordUserID, $FieldAry);
                                	
                                }
                                if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!="" && $TabID == 2){
                                    $otherLangArr = array();
                                    $langPriority = array();
                                    foreach($Lang['AccountMgmt']['LanguageCode'] as $language => $code){
                                        $langPriority[$language] = 0;
                                    }
                                    $langs = explode(",",$this->LanguagePriority);
                                    foreach($langs as $lang){
                                        $isNormalLang = false;
                                        $lang = trim(strtoupper($lang));
                                        foreach($Lang['AccountMgmt']['LanguageCode'] as $language => $code){
                                            if($code == $lang){
                                                $langPriority[$language] = 1;
                                                $isNormalLang = true;
                                            }
                                        }
                                        if(!$isNormalLang){
                                            $otherLangArr[] = $lang;
                                        }
                                    }
                                	$langStr = "Chinese: {$langPriority['Chinese']}, English: {$langPriority['English']}, Urdu: {$langPriority['Urdu']}, Nepali: {$langPriority['Nepali']}, Hindi: {$langPriority['Hindi']}, Tagalog: {$langPriority['Tagalog']}";
                                	if(count($otherLangArr)){
                                	    $langStr .= ", Others: " . implode(',', $otherLangArr);
                                	}
                                    
                                	$IsNCSStr = ($this->IsNCS=='Y')?'1':(($this->IsNCS=='N')?"0":"");
                                	$ncs_sql = "INSERT INTO INTRANET_USER_NCS_SETTINGS VALUES (
                                				'".$recordUserID."',
                                				'".$this->StaffInCharge."',
                                				'".$this->DurationInHK."',
                                				'".$langStr."',
                                				'".$IsNCSStr."',
                                				'".$this->NGO."',
                                				NOW(),
                                				NOW()		
                                			)";
                                	$this->db_db_query($ncs_sql);
                                }

								if ($TabID == 2)
								{
									if($plugin['medical'])
									{
										$this->laccount->updateUserPersonalSetting($recordUserID,array('stayOverNight'=>$this->stayOverNight));
									}

                                    // [2020-0220-1623-28098] Update Graduation Date
                                    //$this->laccount->updateUserPersonalSetting($recordUserID,array('Nationality'=>$this->Nationality,'PlaceOfBirth'=>$this->PlaceOfBirth,'AdmissionDate'=>$this->AdmissionDate));
									$this->laccount->updateUserPersonalSetting($recordUserID,array('Nationality'=>$this->Nationality,'PlaceOfBirth'=>$this->PlaceOfBirth,'AdmissionDate'=>$this->AdmissionDate,'GraduationDate'=>$this->GraduationDate));
								}
                            }
                            else
                            {
                            	
                                /*
                                    $sql = "SELECT UserEmail,UserID FROM INTRANET_USER WHERE UserLogin = '".$this->UserLogin."'";
                                    $result = $this->returnArray($sql,2);
                                    $old_email = $result[0][0];
                                    $recordUserID = $result[0][1];
                                */
                                $this->debug_msg .= "Try update <br>\n";
                                # Try Update, as the user is already exists

                                # if WebSams = #, then empty the field
								$thisWebSamsRegNo = (trim($this->WebSamsRegNo)=="#") ? "" : trim($this->WebSamsRegNo);
									
                                $update_fields = "";
                                #$update_fields .= "FirstName = '".$this->FirstName ."',";
                                #$update_fields .= "LastName = '".$this->LastName ."',";
                                $update_fields .= "ChineseName = '".$this->ChineseName ."',";
                                $update_fields .= "EnglishName = '".$this->EnglishName ."',";
                                $update_fields .= "NickName = '".$this->NickName ."',";
                                #$update_fields .= "Title = '".$this->Title ."',";
                                $update_fields .= "Gender = '".$this->Gender ."',";
                                $update_fields .= "Remark = '".$this->Remark ."',";
                                $update_fields .= "HomeTelNo = '".$this->Home ."',";
                                $update_fields .= "MobileTelNo = '".$this->Mobile ."',";
                                $update_fields .= "FaxNo = '".$this->Fax ."',";
                                //$update_fields .= "ClassNumber = '".$this->ClassNumber ."',";
                                //$update_fields .= "ClassName = '".$this->ClassName ."',";
                                #$update_fields .= "ClassLevel = '".$this->ClassLevel ."',";
                                //$update_fields .= "RecordStatus = '".$this->record_status ."',";
                                $update_fields .= "Teaching = '".$this->teaching ."',";
                                $update_fields .= "CardID = '".$this->CardID ."',";
                                if($sys_custom['SupplementarySmartCard'] && $TabID == 2){
                                	$update_fields .= "CardID2 = '".$this->CardID2 ."',";
                                	$update_fields .= "CardID3 = '".$this->CardID3 ."',";
                                	$update_fields .= "CardID4 = '".$this->CardID4 ."',";
                                }
                                $update_fields .= "TitleEnglish = '".$this->TitleEnglish ."',";
                                $update_fields .= "TitleChinese = '".$this->TitleChinese ."',";
								$update_fields .= "HKJApplNo = '".$this->HKJApplNo."',";
								if($TabID == 2) {
									$update_fields .= "Address = '".$this->Address."',";
								}
								//if($special_feature['StaffCode']) {
                                	$update_fields .= "StaffCode = '".$this->StaffCode."',";
                                //}        
                                if(!$iportfolio_activated /* && trim($this->WebSamsRegNo) !=""*/){
                                	$update_fields .= "WebSAMSRegNo = '".$thisWebSamsRegNo ."',";
	                            }
	                            if($special_feature['ava_hkid'])
	                            {
		                            $update_fields .= "HKID = '".$this->HKID ."',";
	                            }
	                            if($special_feature['ava_strn'])
	                            {
		                            $update_fields .= "STRN = '".$this->STRN ."',";
	                            }
	                            if($TabID==2 && $sys_custom['StudentAccountAdditionalFields']){
	                            	$update_fields .= " NonChineseSpeaking = '".$this->NonChineseSpeaking ."',";
	                            	$update_fields .= " SpecialEducationNeeds = '".$this->SpecialEducationNeeds ."',";
	                            	$update_fields .= " PrimarySchool = '".$this->PrimarySchool ."',";
	                            	$update_fields .= " University = '".$this->University ."',";
	                            	$update_fields .= " Programme = '".$this->Programme ."',";
	                            }
	                            if($sys_custom['BIBA_AccountMgmtCust'] && $TabID == TYPE_STUDENT){
	                            	$update_fields .= " HouseholdRegister='".$this->HouseholdRegister."', ";
	                            }
	                            if ($sys_custom['project']['HKPF'] && $TabID == TYPE_TEACHER) {
	                                $update_fields .= " Grade='".$this->Grade."', ";
	                                $update_fields .= " Duty='".$this->Duty."', ";
	                            }
	                            
	                            if($ssoservice["Google"]["Valid"])
								{
									$update_fields .= " GoogleUserLogin = '".$this->GoogleUserLogin ."', ";						
								}
	                            
	                            if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah" && $TabID == TYPE_STUDENT){
	                                $update_fields.= "PrimarySchoolCode = '".$this->PrimarySchoolCode."',";	                               
	                            }
	                            $update_fields .= "Barcode = '".$this->Barcode ."',";
                                $update_fields .= "DateModified = now(), ";
                                $update_fields .= "ModifyBy = $dbModifiedByUserId";
                                # update email if not empty
                                if ($this->UserEmail != "")
                                {
                                    $update_fields .= ",UserEmail = '".$this->UserEmail ."'";
                                }
                                if ($this->UserPassword !="")
                                {
                                    #$target_password = $this->UserPassword;
                                    if ($intranet_authentication_method == "HASH")
                                    {
                                        $update_fields .= ",UserPassword=NULL,HashedPass = MD5('".$this->UserLogin.$this->UserPassword.$intranet_password_salt."') ";
                                    }/*
                                        else if ($intranet_authentication_method == "LDAP")
                                        {
                                        }*/
                                    else
                                    {
                                        $update_fields .= ",UserPassword = NULL ";
                                    }
                                }
                                else
                                {
                                    $target_password = "";
                                }
                                if ($this->DOB != "")
                                {
                                    $update_fields .= ",DateOfBirth = '".$this->DOB."'";
                                }
                                
                                $conds = "UserLogin = '".$this->UserLogin."'";

								$sql = "UPDATE INTRANET_USER SET $update_fields WHERE $conds";
                                $this->db_db_query($sql);

                                if($TabID==4)
                                {
	                                global $alumni_GroupID;
	                             	# add back Classname, Classnumber and YearOfLeft
	                             	$sql = "update INTRANET_USER set 
	                             				ClassName= '".$this->ClassName."',
	                             				ClassNumber= '".$this->ClassNumber."',
	                             				YearOfLeft= '".$this->YearOfLeft ."',
												DateModified= now(),
												ModifyBy= $dbModifiedByUserId
	                             			where
	                             				UserID=$recordUserID";
	                             	 $this->db_db_query($sql);
										
										# add to corresponding alumni group according to YearOfLeft
										$lu->AddOrUpdateAlumniYearGroup($recordUserID, trim($this->YearOfLeft));
                                }

                                $this->queries .= "$sql <br> \n";
                                if ($this->db_affected_rows()> 0)
                                {
                                    $this->recordIsValid[$i] = true;
                                    
                                    # check whether need to update the member list in GROUP setting or not (for staff only)
									if($TabID==1) {	# teacher
										$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='".$this->UserLogin."'";
										$_result = $this->returnVector($sql);
										$t_userId = $_result[0];
									
	                                	$other_idgroup = ($this->teaching==1) ? 3 : 1;
		                                $target_idgroup = ($this->teaching==1) ? 1 : 3;
		                                
		                                if ($this->teaching == 'S') {
		                                	$sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID='$t_userId' AND GroupID in (1, 3)";	
			                                $this->db_db_query($sql);
		                                }
		                                else {
		                                	$sql = "SELECT COUNT(*) FROM INTRANET_USERGROUP WHERE GroupID='$target_idgroup' AND UserID='$t_userId'";
			                                $_result = $this->returnVector($sql);
			                                
			                                if($_result[0]==0) {
			                                	$sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID='$t_userId' AND GroupID='$other_idgroup'";	
			                                	$this->db_db_query($sql);
			                                	
			                                	//2015-05-12
			                                	//$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES ($target_idgroup, $uid, now(), now())";
			                                	$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES ($target_idgroup, $t_userId, now(), now())";
			                                	$this->db_db_query($sql);
			                                	
			                                }
		                                }
	                                }
	                                
	                                if($sys_custom['BIBA_AccountMgmtCust'] && in_array($TabID,array(TYPE_STUDENT,TYPE_TEACHER))){
	                                	if($this->House != ''){
	                                		$AcademicYearID = Get_Current_Academic_Year_ID();
	                                		$house_ary = explode(",", $this->House);
	                                		if(count($house_ary)>0)
	                                		{
	                                			foreach($house_ary as $house)
	                                			{
			                                		$sql = "SELECT GroupID FROM INTRANET_GROUP WHERE AcademicYearID='".$AcademicYearID."' AND RecordType='4' AND (Title='".$house."' OR TitleChinese='".$house."')";
			                                		$house_group_record = $this->returnResultSet($sql);
			                                		if(count($house_group_record)==1){
			                                			$sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID='$recordUserID' AND GroupID='".$house_group_record[0]['GroupID']."'";	
				                                		$this->db_db_query($sql);
				                                		
				                                		$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES ('".$house_group_record[0]['GroupID']."', '".$recordUserID."', now(), now())";
				                                		$this->db_db_query($sql);
			                                		}
	                                			}
	                                		}
	                                	}
	                                }
	                                
	                                #Prevent emptying the Class Name Class Number in user_course and usermaster
	                                if($TabID==2){
	                                	$toInit = false;
	                                	$sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserLogin='".$this->UserLogin."'";
	                                	$_result = current($this->returnArray($sql));
	                                	if($this->ClassName == ""  && $_result[0] != ""){
	                                		$this->ClassName = $_result[0];
	                                		$toInit = true;
	                                	}
	                                	if($this->ClassNumber == ""  && $_result[1] != ""){
	                                		$this->ClassNumber = $_result[1];
	                                		$toInit = true;
	                                	}
	                                }
	                                
                                    #$lc->eClassUserUpdateBriefProfile($old_email, $this->Title, $this->EnglishName, $this->UserPassword,$this->ClassName,$this->ClassNumber, $this->UserEmail, $this->Gender);
                                    $lc->eClassUserUpdateInfoImport($old_email, $this->Title, $this->EnglishName,$this->ChineseName,$this->FirstName,$this->LastName, $this->UserPassword,$this->ClassName,$this->ClassNumber, $this->UserEmail, $this->Gender, $this->DOB, $this->NickName, $this->TitleEnglish, $this->TitleChinese);
                                    if($TabID==2 && $toInit==true){
                                    	$this->ClassName = "";
                                    	$this->ClassNumber = "";
                                    }
                                    //  $lc->eClassUserUpdateProfile($old_email, $this->Title, $this->FirstName, $this->LastName, $this->FirstName, $target_password,$this->ClassName,$this->ClassNumber, $this->UserEmail);
                                    $this->debug_msg .= "Update success <br>\n";
                                    $this->db = $intranet_db;
									
									if($sys_custom['iMailPlus']['EmailAliasName'] && $TabID==1 && $this->ImapUserLogin!="" && $this->ImapUserLogin != $this->UserLogin && intranet_validateEmail($this->ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'])){
										// Check any conflict with existing userlogin
										$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='".$this->ImapUserLogin."'";
										$temp = $this->returnVector($sql);
										
										// Get existing iMail plus account name
										$sql = "SELECT ImapUserEmail,ImapUserLogin FROM INTRANET_USER WHERE UserID='".$recordUserID."'";
										$temp2 = $this->returnArray($sql);
										$tmp_ImapUserEmail = trim($temp2[0]['ImapUserEmail']);
										$tmp_ImapUserLogin = trim($temp2[0]['ImapUserLogin']);
										$tmp_newemail = $this->ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
										$tmp_account_exist = $IMap->is_user_exist($tmp_newemail);
										
										if(sizeof($temp)==0 && !in_array($this->ImapUserLogin, (array)$system_reserved_account) 
											&& $tmp_ImapUserEmail!=$tmp_newemail && !$tmp_account_exist){
											if($tmp_ImapUserLogin != ''){
												$IMap->changeAccountName($tmp_ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'],$tmp_newemail);
												$IMap->Change_Preference_Mailbox($tmp_ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'],$tmp_newemail);
											}else if($tmp_ImapUserEmail != ''){
												$IMap->changeAccountName($tmp_ImapUserEmail,$tmp_newemail);
												$IMap->Change_Preference_Mailbox($tmp_ImapUserEmail,$tmp_newemail);
											}
											$sql = "UPDATE INTRANET_USER SET ImapUserEmail='$tmp_newemail', ImapUserLogin='".$this->ImapUserLogin."' WHERE UserID='".$recordUserID."'";
											$this->db_db_query($sql);
										}
									}
									
                                    if ($this->UserPassword != "")
                                    {
                                    	// Log down encrypted password
                                    	$libauth->UpdateEncryptedPassword($recordUserID, $this->UserPassword);
                                        // Update account password
                                        # Email
                        				if($plugin['imail_gamma'])
										{
											//global $SYS_CONFIG;
											//include_once("imap_gamma.php");
											//$IMap = new imap_gamma(1);
											
											$IMapEmail = trim($this->UserLogin)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
											if($sys_custom['iMailPlus']['EmailAliasName'] && $TabID==1 && $this->ImapUserLogin!="" && $this->ImapUserLogin != $this->UserLogin && intranet_validateEmail($this->ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'])){
												$sql = "SELECT ImapUserEmail,ImapUserLogin FROM INTRANET_USER WHERE UserID='".$recordUserID."'";
												$temp = $this->returnArray($sql);
												if($temp[0]['ImapUserEmail']!=''){
													$IMapEmail = $temp[0]['ImapUserEmail'];
												}else if($temp[0]['ImapUserLogin'] != ''){
													$IMapEmail = $temp[0]['ImapUserLogin']."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
												}
											}
											$IMap->change_password($IMapEmail,$this->UserPassword);
										}
										else
                                        	$this->lwebmail->change_password($this->UserLogin,$this->UserPassword,"iMail");

                                        # Files
                                        if ($plugin['personalfile'])
                                        {
                                            if ($personalfile_type == 'FTP')
                                            {
                                                $this->lftp->changePassword($this->UserLogin,$this->UserPassword,"iFolder");
                                            }
                                        }

                                        # LDAP
                                        if ($intranet_authentication_method=='LDAP')
                                        {
                                            if ($this->lldap->isPasswordChangeNeeded())
                                            {
                                                $this->lldap->changePassword($this->UserLogin,$this->UserPassword,"iFolder");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    $this->recordIsValid[$i] = false;
                                    $this->reason [$i] = "Data updates failed.";
                                    $this->debug_msg .= "All failed <br>\n";
                                    # add to error
                                }
                               
                                # Extra Info
                                if($sys_custom['UserExtraInformationImport']) {
                                	include_once("libaccountmgmt.php");
                                	$laccount = new libaccountmgmt();
                                	$ExtraInfoCatInfo = $laccount->Get_User_Extra_Info_Category();
									$totalCategory = count($ExtraInfoCatInfo);
									//$startExtraInfo
									$pointer = $startExtraInfo+1;
									
									for($a=0; $a<$totalCategory; $a++) {
										$thisCategoryInfo = $ExtraInfoCatInfo[$a];
										$sql = "Select * From INTRANET_USER_EXTRA_INFO_ITEM WHERE RecordStatus=1 AND CategoryID='".$thisCategoryInfo['CategoryID']."'";	
										$itemAry = $laccount->returnArray($sql);
										$itemAssoArray = array();
										for($b=0; $b<count($itemAry); $b++) {
											$itemAssoArray[trim($itemAry[$b]['ItemCode'])] = trim($itemAry[$b]['ItemID']);	
										}
										
										$importedItem = trim($row[$i][$pointer]);
										$laccount->DELETE_EXTRA_INFO_MAPPING($recordUserID, $Category=$thisCategoryInfo['CategoryID']);
										if($importedItem!="") {
											
											
											$importedItemAry = explode(',', $importedItem);
											for($p=0; $p<count($importedItemAry); $p++) {
												$thisImportedItemName = trim($importedItemAry[$p]);
												
												// exclude data of option "OTHERS"
												$_tempImportedItem = explode('::', $thisImportedItemName);
												$_tempItemName = trim($_tempImportedItem[0]);
												$_tempOtherField = trim($_tempImportedItem[1]);
												
												// get item code
												$sql = "SELECT ItemCode FROM INTRANET_USER_EXTRA_INFO_ITEM WHERE (ItemNameCh='$_tempItemName' OR ItemNameEn='$_tempItemName') AND CategoryID='".$thisCategoryInfo['CategoryID']."'";
												$ItemCode = $this->returnVector($sql);
												$thisItemCode = $ItemCode[0];
												
												$thisItemID = $laccount->GET_EXTRA_INFO_ITEM_ID_BY_ITEM_NAME($_tempItemName, $thisCategoryInfo['CategoryID']);
												
												if(isset($itemAssoArray[$thisItemCode])) {
													$laccount->INSERT_EXTRA_INFO_MAPPING($recordUserID, $itemID=$thisItemID, $OtherField=$_tempOtherField);	
												}	
											}	
										}
										$pointer++;
									}
                                }
                                 
                                # Extra Info 
                                if($sys_custom['StudentMgmt']['BlissWisdom']) {
                                	include_once("libuserpersonalsettings.php");
                                	$libps = new libuserpersonalsettings();
                                	
                                	$Occupation = trim($row[$i][$pointer]); $pointer++;
                                	$Company = trim($row[$i][$pointer]); $pointer++;
                                	$PassportNo = trim($row[$i][$pointer]); $pointer++;
                                	$Passport_ValidDate = trim($row[$i][$pointer]); $pointer++;
                                	$HowToKnowBlissWisdom = explode('::',trim($row[$i][$pointer])); $pointer++;
                                	
                                	$HowToValue = $HowToKnowBlissWisdom[0];
                                	$HowToOthers = $HowToKnowBlissWisdom[1];
                                	
                                	foreach($Lang['AccountMgmt']['KnowAboutBlissWisdomOptions'] as $_id=>$_option) {
                                		if($HowToValue==$_option) {
                                			$HowToID = $_id;
                                			break;	
                                		}
                                	}
                                	
                                	//echo $Occupation.'/'.$Company.'/'.$PassportNo.'/'.$Passport_ValidDate.'/'.$HowToKnowBlissWisdom.'<br>';
                                	$FieldAry = array('Occupation'=>$Occupation, 'Company'=>$Company, 'PassportNo'=>$PassportNo, 'Passport_ValidDate'=>$Passport_ValidDate, 'HowToKnowBlissWisdom'=>$HowToID.'::'.$HowToOthers);
//                                	debug_pr($FieldAry);
                                	$libps->Save_Setting($recordUserID, $FieldAry);
                                }
                                if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!="" && $TabID == 2){
                                    $otherLangArr = array();
                                    $langPriority = array();
                                    foreach($Lang['AccountMgmt']['LanguageCode'] as $language => $code){
                                        $langPriority[$language] = 0;
                                    }
                                    $langs = explode(",",$this->LanguagePriority);
                                    foreach($langs as $lang){
                                        $isNormalLang = false;
                                        $lang = trim(strtoupper($lang));
                                        foreach($Lang['AccountMgmt']['LanguageCode'] as $language => $code){
                                            if($code == $lang){
                                                $langPriority[$language] = 1;
                                                $isNormalLang = true;
                                            }
                                        }
                                        if(!$isNormalLang){
                                            $otherLangArr[] = $lang;
                                        }
                                    }
                                	$langStr = "Chinese: {$langPriority['Chinese']}, English: {$langPriority['English']}, Urdu: {$langPriority['Urdu']}, Nepali: {$langPriority['Nepali']}, Hindi: {$langPriority['Hindi']}, Tagalog: {$langPriority['Tagalog']}";
                                	if(count($otherLangArr)){
                                	    $langStr .= ", Others: " . implode(',', $otherLangArr);
                                	}
                                    
                                	$IsNCSStr = ($this->IsNCS=='Y')?'1':(($this->IsNCS=='N')?"0":"");
                                	$ncs_sql = "UPDATE
                                	    INTRANET_USER_NCS_SETTINGS
                            	    SET
                                	    StaffInCharge = '{$this->StaffInCharge}',
                                	    DurationInHK = '{$this->DurationInHK}',
                                	    LanguagePriority = '{$langStr}',
                                	    IsNCS = '{$IsNCSStr}',
                                	    NGO = '{$this->NGO}',
                                	    Modified = NOW()
                            	    WHERE 
                                	    UserID = '{$recordUserID}'";
                                	$this->db_db_query($ncs_sql);
                                }

								if ($TabID == 2)
								{
									if($plugin['medical'])
									{
										$this->laccount->updateUserPersonalSetting($recordUserID,array('stayOverNight'=>$this->stayOverNight));
									}
									
									if($plugin['SDAS_module']['KISMode'])
									{
										$this->laccount->updateUserPersonalSetting($recordUserID,array('KISClassType'=>$this->KISClassType));
									}

									// [2020-0220-1623-28098] Update Graduation Date
									//$this->laccount->updateUserPersonalSetting($recordUserID,array('Nationality'=>$this->Nationality,'PlaceOfBirth'=>$this->PlaceOfBirth,'AdmissionDate'=>$this->AdmissionDate));
                                    $this->laccount->updateUserPersonalSetting($recordUserID,array('Nationality'=>$this->Nationality,'PlaceOfBirth'=>$this->PlaceOfBirth,'AdmissionDate'=>$this->AdmissionDate,'GraduationDate'=>$this->GraduationDate));
								}
                            }

                            # After Insert/Update
                            # Update ACL
                            if ($recordUserID != "")
                            {
                                $acl_value = $this->acl_value;
                                //$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = $acl_value WHERE UserID = $recordUserID";
                                //$this->db_db_query($sql);
                                //if ($this->db_affected_rows()==0)
                                //{
                                    $sql = "INSERT IGNORE INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($recordUserID, $acl_value)";
                                  	$this->db_db_query($sql);
                                //}
                                # Store UserID in array
                                $this->new_account_userids[] = $recordUserID;
                            }
                            else
                            {
                            }


                            # Create Parent-Student relationship
                            if ($recordType==3 && $this->recordIsValid[$i])
                            {

                                # Find UserID of Parent
                                $sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '".$this->UserLogin."'";
                                $temp = $this->returnVector($sql);
                                $parentID = $temp[0];

                                # Remove Old connection first
                                $sql = "DELETE FROM INTRANET_PARENTRELATION WHERE ParentID = $parentID";
                                $this->db_db_query($sql);
                                # Locate children
                                /*
                                $childrenLogin = array($row[$i][13],$row[$i][16],$row[$i][19]);
                                $childrenName = array($row[$i][14],$row[$i][17],$row[$i][20]);
                                $childrenClass = array($row[$i][15],$row[$i][18],$row[$i][21]);
                                */
                                $conds = "";
                                $delimiter = "";
                                for ($j=0; $j<3; $j++)
                                {
                                     if ($this->childrenLogin[$j]!= "")
                                     {
//                                          $conds .= "$delimiter (UserLogin = '".$this->childrenLogin[$j]."'
//                                                     AND TRIM(EnglishName) = TRIM('".$this->childrenName[$j]."')) ";
                                        $conds .= "$delimiter (UserLogin = '".$this->childrenLogin[$j]."') ";
                                         $delimiter = "OR";
                                     }
                                }

                                if ($conds == "")
                                {
                                    continue;
                                }
                                else
                                {
                                    $conds = "AND ($conds)";
                                    $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 $conds";
                                    $childrens = $this->returnVector($sql);
									//echo sizeof($childrens).'|';
                                    # Remove current relation
                                    $sql = "DELETE FROM INTRANET_PARENTRELATION WHERE ParentID = $parentID";
                                    $this->db_db_query($sql);

                                    # Make insert query
                                    $sql = "INSERT INTO INTRANET_PARENTRELATION (ParentID,StudentID) VALUES ";
                                    $delimiter = "";
                                    for ($j=0; $j<sizeof($childrens); $j++)
                                    {
                                         $sql .= "$delimiter ($parentID, ".$childrens[$j].")";
                                         $delimiter = ",";
                                    }
                                   
                       				//echo $sql.'<br>';            
                                    $this->db_db_query($sql);
                                }
								
                            }
                                /*
                                # update group - class name
                                if($this->ClassName <> ""){
                                   $sql = "INSERT INTO INTRANET_GROUP (Title, RecordType, DateInput, DateModified) VALUES ('".$this->ClassName."', 3, now(), now())";
                                   $this->db_db_query($sql);
                                }
                                # update group - class level
                                if($this->ClassLevel <> ""){
                                   $sql = "INSERT INTO INTRANET_GROUP (Title, RecordType, DateInput, DateModified) VALUES ('".$this->gpLevel."', 3, now(), now())";
                                   $this->db_db_query($sql);
                                }
                                */
                            global $ssoservice;
//							error_reporting(E_ALL);
//							ini_set('display_errors', 1);
                        	if($ssoservice["Google"]["Valid"] && ($TabID==1 || $TabID==2 && $ssoservice["Google"]['mode']['student'])){
								include_once("google_api/libgoogleapi.php");
								include_once("sso/libSSO_db.php");
								include_once("sso/libGoogleSSO.php");
								$google_api = new libGoogleAPI(0);
								$libSSO_db = new libSSO_db();
								$libGoogleSSO = new libGoogleSSO();
								
//                                 $gmail_input_array = array(
//                                     'gmail_0' => ($this->EnableGSuite=='Y'?'1':'0')
//                                 );
								
								$gmail_input_array = array();
								for($a=0;$a<count($ssoservice["Google"]['api']);$a++){
								    $gmail_input_array['gmail_'.$a] = $this->EnableGSuite=='Y'?'1':'0';
								}
								if (count($ssoservice["Google"]['api'])>0) {
								    $array_error_message = $libGoogleSSO->syncUserFromEClassToGoogle($recordUserID, $this->UserLogin, 1, $this->EnglishName, $this->UserPassword, $gmail_input_array, $this->ChineseName);
								    
								    if($ssoservice["Google"]['mode']['user_and_group']){
								        include_once("libaccountmgmt.php");
								        $laccount = new libaccountmgmt();
								        $libGoogleSSO->syncGroupForUserFromEClassToGoogle($laccount, $this->UserLogin, $gmail_input_array, ($TabID==1?array(1):array(2)));
								    }
								}
                        	}
                        }

                }
                # Open Money accounts
                if ($plugin['payment'] && ($recordType==2 || $recordType==1))
                {
                	if($_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance']){ 
                		$default_balance = "9999999";
                	}else{
                		$default_balance = "0";
                	}
                    $sql = "INSERT IGNORE INTO PAYMENT_ACCOUNT (StudentID, Balance)
                            SELECT UserID , $default_balance FROM INTRANET_USER WHERE RecordType = 2 OR RecordType = 1 ";
                    $this->db_db_query($sql);
                }
        }

        function process_grouping(){
                if($this->format == 1){   # CSV
                        $row = $this->data_array;
                        for($i=0; $i<sizeof($row); $i++){
                                if ($this->recordIsValid[$i])
                                {
                                    $this->set_row_array($row[$i]);
                                    /*
                                    if ($this->ClassName != "")
                                    {
                                        $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID) SELECT a.GroupID, b.UserID FROM INTRANET_GROUP AS a, INTRANET_USER AS b WHERE (a.Title = '".$this->ClassName."') AND (b.UserLogin = '".$this->UserLogin."')";
                                        $this->queries .= "$sql <br>\n";
                                        $this->db_db_query($sql);
                                    }
                                    */
                                }
                        }
                } else {
                        /*
                        $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID) SELECT a.GroupID, b.UserID FROM INTRANET_GROUP AS a, INTRANET_USER AS b WHERE a.Title = b.ClassName AND b.RecordStatus = ".$this->record_status;
                        $this->db_db_query($sql);
                        */
                }
        }

        function open_os_accounts()
        {
                 global $personalfile_type, $intranet_root, $plugin, $sys_custom, $SYS_CONFIG, $special_feature, $TabID, $system_reserved_account;
                
                include_once("imap_gamma.php");
				include_once("libaccountmgmt.php");
				$IMap = new imap_gamma(1);
				$laccount = new libaccountmgmt();
                 for ($i=0; $i<sizeof($this->os_accounts_to_open); $i++)
                 {
                      list($lineno,$login,$passwd, $uid) = $this->os_accounts_to_open[$i];
                      if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName'] && $TabID==1){
                 	  		$emailuserlogin = $this->os_accounts_to_open[$i][4];
                 	  }
                      if ($this->open_file_account)
                      {
                          if ($personalfile_type=='FTP')
                          {
                              if ($this->lftp->isAccountManageable)
                              {
                                  $file_failed = ($this->lftp->open_account($login,$passwd,"iFolder")? 0:1);
                                  $this->lftp->setTotalQuota($login,$this->file_quota,"iFolder");
                              }
                          }
                      }
                      if ($this->open_webmail)
                      {
	                      	if($plugin['imail_gamma'])
							{
								$open_account = true;
								$IMapEmail = trim($login)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
								if($sys_custom['iMailPlus']['EmailAliasName'] && $TabID==1 && $emailuserlogin!="" && $emailuserlogin != $login && intranet_validateEmail($emailuserlogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'])){
									// Check any conflict with existing userlogin
									$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$emailuserlogin'";
									$temp = $this->returnVector($sql);
									
									// Get existing iMail plus account name
									$sql = "SELECT ImapUserEmail,ImapUserLogin FROM INTRANET_USER WHERE UserID='$uid'";
									$temp2 = $this->returnArray($sql);
									$tmp_ImapUserEmail = trim($temp2[0]['ImapUserEmail']);
									$tmp_ImapUserLogin = trim($temp2[0]['ImapUserLogin']);
									$tmp_newemail = $emailuserlogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
									$tmp_account_exist = $IMap->is_user_exist($tmp_newemail);
									
									if(sizeof($temp)==0 && !in_array($emailuserlogin, (array)$system_reserved_account) 
										&& $tmp_ImapUserEmail!=$tmp_newemail && !$tmp_account_exist){
										if($tmp_ImapUserLogin != ''){
											$IMap->changeAccountName($tmp_ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'],$tmp_newemail);
											$open_account = false;
											$IMap->Change_Preference_Mailbox($tmp_ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'],$tmp_newemail);
										}else if($tmp_ImapUserEmail != ''){
											$IMap->changeAccountName($tmp_ImapUserEmail,$tmp_newemail);
											$open_account = false;
											$IMap->Change_Preference_Mailbox($tmp_ImapUserEmail,$tmp_newemail);
										}else{
											// open account
											$open_account = true;
										}
										$IMapEmail = $tmp_newemail;
										$sql = "UPDATE INTRANET_USER SET ImapUserEmail='$IMapEmail', ImapUserLogin='$emailuserlogin' WHERE UserID='$uid'";
										$this->db_db_query($sql);
									}else{
										$open_account = false;
									}
								}
								
								//include_once("imap_gamma.php");
								//include_once("libaccountmgmt.php");
								//$IMap = new imap_gamma(1);
								//$laccount = new libaccountmgmt();
								if($open_account && $IMap->open_account($IMapEmail,$passwd))
								{
									$laccount->setIMapUserEmail($uid,$IMapEmail);
									$IMap->SetTotalQuota($IMapEmail,$this->mail_quota,$uid);
									
									// Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
									$internal_only = $IMap->internal_mail_only; // index 0 is staff, 1 is student, 2 is parent, 3 is alumni
									if($internal_only[$TabID-1]){ // shift index by one
										$IMap->addGroupBlockExternal(array($IMapEmail));
										$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 1 WHERE UserID = '$uid' AND ACL IN (1,3)";
								    	$this->db_db_query($sql);
									}
								}
							}
							else
							{
								# webmail quota
								$mail_failed = ($this->lwebmail->open_account($login,$passwd,"iMail")? 0 : 1 );
								$this->lwebmail->setTotalQuota($login,$this->mail_quota,"iMail");
								
								# iMail Quota
								if ($special_feature['imail'])
								{
								    # Get Default Quota
								    // line 0 Staff, line 1 Student, line 2 Alumni, line 3 Parent
								    $file_content = get_file_content($intranet_root."/file/campusmail_set.txt");
								    if ($file_content == "")
								    {
								        $quota = 10;
								    }
								    else
								    {
								    	## [0] => teacher [1] => student [2] => alumni [3] => parent
								        $userquota = explode("\n", $file_content);
								        if($TabID==3){
								        	$quotaline = $userquota[$TabID]; // Parent
								        }elseif($TabID==4){
								        	$quotaline = $userquota[$TabID-2]; // Alumni
								        }else{
								        	$quotaline = $userquota[$TabID-1]; // Staff or Student
								        }
							            //$quotaline = ($TabID==3?$userquota[$TabID]:$userquota[$TabID-1]);
								        $temp = explode(":",$quotaline);
								        $quota = $temp[1];
								    }
								    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USERQUOTA (UserID, Quota) VALUES ('$uid', '$quota') ON DUPLICATE KEY UPDATE Quota = '$quota'";
								    $this->db_db_query($sql);
								    
								    $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL_USED_STORAGE WHERE UserID = '$uid'";
								    $recordExist = $this->returnVector($sql);
								    if($recordExist[0] == 0) {
								    	$sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed) VALUES ('$uid', 0) ON DUPLICATE KEY UPDATE QuotaUsed = 0";
								    	$this->db_db_query($sql);
								    }
								}
							}
                      }
                 }
        }
        function updateMailQuota()
        {
                 global $special_feature,$intranet_root,$TabID;
                 # iMail Quota
                 if ($special_feature['imail'])
                 {
                     # Get Default Quota
                     $file_content = get_file_content($intranet_root."/file/campusmail_set.txt");
                     if ($file_content == "")
                     {
                         $quota = 10;
                     }
                     else
                     {
                         $userquota = explode("\n", $file_content);
						 if($TabID==3){
				        	$quotaline = $userquota[$TabID]; // Parent
				         }elseif($TabID==4){
				        	$quotaline = $userquota[$TabID-2]; // Alumni
				         }else{
				        	$quotaline = $userquota[$TabID-1]; // Staff or Student
				         }
                         $temp = explode(":",$quotaline);
                         $quota = $temp[1];
                         if ($quota== "") $quota = 10;
                     }
                     
                     //$sql = "SELECT UserID, IFNULL(SUM(AttachmentSize),0) FROM INTRANET_CAMPUSMAIL WHERE RecordStatus = ".$this->record_status;
                     $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordStatus = ".$this->record_status;
                     $targetUserArray = $this->returnVector($sql);
                     
                     if(sizeof($targetUserArray)>0) {
                     	$targetUser = implode(",",$targetUserArray);
                     }
                     $sql = "SELECT UserID, IFNULL(SUM(AttachmentSize),0) FROM INTRANET_CAMPUSMAIL WHERE UserID IN ($targetUser) GROUP BY UserID";
                     $result = $this->returnArray($sql,2);
                     if(sizeof($result)>0){
						for($i=0; $i<sizeof($result); $i++){
							list($thisUserID, $UsedQuota) = $result[$i];
							$sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed) VALUES ($thisUserID, $UsedQuota) ON DUPLICATE KEY UPDATE QuotaUsed = $UsedQuota";
							$this->db_db_query($sql);
						}
					 }
                 }
        }
        # ------------------------------------------------------------------------------------

		function check_data($data){
                 global $intranet_authentication_method;
                $this->set_data_array($data);
                $this->process_user2(1);	# 1 = only for checking

		}
		
        function process_data($data){
                 global $intranet_authentication_method;
                $this->set_data_array($data);
                
                $this->process_user();
                $this->updateMailQuota();
                $this->process_grouping();
                $this->update_default_group();
                $this->update_status();
                $this->UpdateRole_UserGroup();

                if ($this->open_webmail || $this->open_file_account)
                {
                    $this->open_os_accounts();
                }

                if ($intranet_authentication_method=='LDAP')
                {
                    if ($this->lldap->isAccountControlNeeded())
                    {
                        $this->lldap->close();
                    }
                }
        }
        
         function process_data2($data)
         {
         		global $intranet_authentication_method;
                $this->set_data_array($data);
                $this->process_user2();
                $this->updateMailQuota();
                $this->process_grouping();
                $this->update_default_group();
                $this->update_status();
                $this->UpdateRole_UserGroup();
                //$this->linkWithStudent();

                if ($this->open_webmail || $this->open_file_account)
                {
                    $this->open_os_accounts();
                }

                if ($intranet_authentication_method=='LDAP')
                {
                    if ($this->lldap->isAccountControlNeeded())
                    {
                        $this->lldap->close();
                    }
                }
        }

        function getErrorMsg() {
                 $x = "";
                 $array = $this->data_array;
                 $first = true;
                 
                 for ($i=0; $i<sizeof($array); $i++)
                 {
                      if (!$this->recordIsValid[$i])
                      {
                           if ($this->reason[$i]=="") continue;
                           if ($first)
                           {
                               $x = "<TABLE width=95%>";
                               $x .= "<TR><TD>Line Number</TD><TD>UserLogin</TD><TD>UserEmail</TD><TD>Reason</TD></TR>\n";
                               $first = false;
                           }
                           $x .= "<TR><TD>". ($i+1) ."</td>";
                           $row = $array[$i];
                           $x .= "<TD>".$row[0]."</TD>";
                           $x .= "<TD>".$row[2]."</TD>";
                           $x .= "<TD>".$this->reason[$i]."</TD>";
                           $x .= "</TR>\n";
                           
                      }
                 }
                 if ($x!="")
                 {
                     $x .= "</TABLE>";
                 }
                 return $x;
        }

		function getErrorMsgArr() {
                 $array = $this->data_array;
                 for ($i=0; $i<sizeof($array); $i++)
                 {
                      if (!$this->recordIsValid[$i])
                      {
                           if ($this->reason[$i]=="") continue;
                           $row = $array[$i];
                           $msgAry[$i] = array($row[0],$row[2],$this->reason[$i]);
                      }
                 }
                 return $msgAry;
        }
        function getQueries() {
                 return $this->queries;
        }
        
        function linkWithStudent()
        {
        	include_once("libuser.php");
        	$lu = new libuser();
        	//debug_pr($this->data_array);
			for($k=0; $k<sizeof($this->data_array); $k++) {
				$dataAry = $this->data_array[$k];
				//debug_pr($dataAry);
				$p = 0;
				unset($studentLoginAry);
				
				$studentLoginAry = array();
				if($dataAry[9]!="") 
					$studentLoginAry[] = $dataAry[9];

				if($dataAry[10]!="") 
					$studentLoginAry[] = $dataAry[10];
					
				if($dataAry[12]!="") 
					$studentLoginAry[] = $dataAry[12];
				//debug_pr($studentLoginAry);
				for($i=0; $i<sizeof($studentLoginAry); $i++) {
					$lstudent = $lu->returnUser("", $studentLoginAry[$i]);
					$lparent = $lu->returnUser("", $dataAry[0]);
					
					$sql = "INSERT INTO INTRANET_PARENTRELATION (ParentID, StudentID) VALUES ('".$lparent[0]['UserID']."', '".$lstudent[0]['UserID']."')";
					$this->db_dB_query($sql);
					
				}
				
        	}
        	
        }
		
        // replacement of str_getcsv for PHP version < 5.3
        function comma_parse($str){
        	$dst = array();
        
        	$state = false;
        	$buffer = '';
        	for ($i = 0, $lim = strlen($str); $i < $lim; $i += 1) {
        		$char = $str[$i];
        
        		switch ($char) {
        			case ',':
        				if (!$state) {
        					$dst[] = $buffer;
        					$buffer = '';
        				}else{
        	    $buffer .= '@=@';
        				}
        				break;
        
        			case '"':
        				$state = !$state;
        				break;
        
        			default:
        				$buffer .= $char;
        		}
        	}
        
        	$dst[] = $buffer;
        
        	return $dst;
        
        }

        # ------------------------------------------------------------------------------------

}
?>