<?php
class libelc extends libdb {
	
	/*
	* Initialize
	*/
	function libelc() {
	
		$this->libdb();
	}
	
	/*
	* Get MODULE_OBJ array
	*/
	function GET_MODULE_OBJ_ARR()
	{
		global $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $image_path;
		### wordings
		global $i_SpecialRoom_ReadingRoom, $i_SpecialRoom_ELP, $i_eClass_ssr;
		global $CurrentPageArr;
		global $ip20TopMenu, $access2readingrm, $access2elprm, $access2ssrm, $access2specialrm;
                
		$CurrentPageArr['eLC'] = 1;
                
		# Menu information
		if ($access2readingrm)
			$MenuArr["ReadingRoom"] = array($i_SpecialRoom_ReadingRoom, "javascript:open_readingrm()", 0);
		
		if ($access2elprm)
			$MenuArr["ELP"] = array($i_SpecialRoom_ELP, "javascript:newWindow('/home/eclass/index_elp.php',8)", 0);
		
		if ($access2ssrm)		
			$MenuArr["ssr"] = array($i_eClass_ssr, "javascript:newWindow('/home/eclass/index_ssr.php',8)", 0);
		
		if (!$access2specialrm)
			$MenuArr["nothing"] = array(" ", "#", 0); 
			
		# module information
		$MODULE_OBJ['title'] = $ip20TopMenu['eLC'];
		$MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_eclass.gif";
		$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eclass/index_elc.php";
		$MODULE_OBJ['menu'] = $MenuArr;
				
		return $MODULE_OBJ;
	}
	
	
	function displayUserEClass($SpecialRmArr) {
		global $i_no_record_exists_msg, $i_frontpage_eclass_courses, $i_frontpage_eclass_lastlogin, $i_frontpage_eclass_whatsnews;
		global $i_status_suspended, $image_path, $LAYOUT_SKIN;

		$x .= "<table width=\"50%\" border=\"0\" cellspacing=\"0\" cellpadding=\"8\" align=\"center\">";		
		if(sizeof($SpecialRmArr)==0)
		{
			$x .= "
					<tr>
						<td width=\"100%\" class=\"indextabclassiconoff\" align='center'><br /><br /><br /><br /><br />$i_no_record_exists_msg</td>
					</tr>
					";			
		}
		else
		{
			
			for ($i = 0; $i < sizeof($SpecialRmArr); $i++) {				
				
				$course_link = $SpecialRmArr[$i][0];
				$course_name = $SpecialRmArr[$i][1];
							
				$class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_class.gif\" >";
				$RmTitle = "<a href=\"".$course_link."\" class=\"tabletoplink\">".$course_name."</a>";
                
				$x .= "<tr>\n";                
				$x .= "<td align='center' width='50%' valign='top'>". $this->MyeClassTable($class_icon, $RmTitle) ."</td>";
				$x .= "</tr>\n";
			}
		}       
		$x .= "</table>";         
		return $x;
     }
	
     
     function MyeClassTable($class_icon, $RmTitle) {
		global $image_path, $LAYOUT_SKIN;
                
		$x = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
		$x .= "<tr>";
		$x .= "<td width='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board01.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='26'></td>";
		$x .= "<td background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board02.gif'><table width='95%' border='0' cellpadding='5' cellspacing='0'>";
		$x .= "<tr>";
		$x .= "<td width='19'>$class_icon</td>";
		$x .= "<td>$RmTitle</td>";
		$x .= "</tr>";
		$x .= "</table></td>";
		$x .= "<td width='12' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board03.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='12' height='26'></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board04.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board04.gif' width='10' height='11'></td>";
		$x .= "<td background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board05.gif'><table width='95%' border='0' cellpadding='5' cellspacing='0'>";
		$x .= "<tr>";
		$x .= "<td>";
		$x .= $whatsnew;
		$x .="</td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td align='right' class='tabletext'>$lastlogin</td>";
		$x .= "</tr>";
		$x .= "</table></td>";
		$x .= "<td width='12' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board06.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board06.gif' width='12' height='12'></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width='10' height='10'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board07.gif' width='10' height='10'></td>";
		$x .= "<td height='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board08.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board08.gif' width='10' height='10'></td>";
		$x .= "<td width='12' height='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board09.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board09.gif' width='12' height='10'></td>";
		$x .= "</tr>";
		$x .= "</table>";
                
		return $x;        
	}
     
}
?>