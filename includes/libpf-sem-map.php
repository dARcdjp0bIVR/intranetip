<?php


function GetSemesterPattern(){
  //2014-0409-1536-04054
//  $sem_mapping[1] = array('1', '一', '上', 'first', '1st Semester');
//  $sem_mapping[2] = array('2', '二', '下', 'second', '2nd Semester');
  $sem_mapping[1] = array('1', '一', '上', 'first', '1st Semester', 'First Term');
  $sem_mapping[2] = array('2', '二', '下', 'second', '2nd Semester', 'Second Term');

  return $sem_mapping;
}

function SemesterTransform($ParSem)
{
  global $eclass_db, $intranet_db;

  $t_libdb = new libdb();
  $sem_mapping = GetSemesterPattern();

  $sql =  "
            SELECT DISTINCT
              YearTermNameEN
            FROM
              {$intranet_db}.ACADEMIC_YEAR_TERM
          ";
  $t_target_sem_arr = $t_libdb->returnVector($sql);

  for($i=0; $i<count($t_target_sem_arr); $i++)
  {
    $t_sem = $t_target_sem_arr[$i];

    for($j=1; $j<=count($sem_mapping); $j++)
    {
      if(strposa($t_sem, $sem_mapping[$j]) !== false)
      {
        $target_sem_arr[$j][] = $t_sem;
      }
    }
  }

  for($j=1; $j<=count($sem_mapping); $j++)
  {
    if(strposa($ParSem, $sem_mapping[$j]) !== false)
    {
      $result_sem = $target_sem_arr[$j][0];
    }
  }
  
  return $result_sem;
}

function SemesterMapping()
{
  global $eclass_db, $intranet_db;

  $t_libdb = new libdb();
  $sem_mapping = GetSemesterPattern();

  $sql =  "SELECT DISTINCT Semester FROM {$eclass_db}.ACTIVITY_STUDENT";
  $source_sem_arr = $t_libdb->returnVector($sql);
  $sql =  "SELECT DISTINCT Semester FROM {$eclass_db}.ATTENDANCE_STUDENT";
  array_splice($source_sem_arr, count($source_sem_arr), 0, $t_libdb->returnVector($sql));
  $sql =  "SELECT DISTINCT Semester FROM {$eclass_db}.AWARD_STUDENT";
  array_splice($source_sem_arr, count($source_sem_arr), 0, $t_libdb->returnVector($sql));
  $sql =  "SELECT DISTINCT Semester FROM {$eclass_db}.CONDUCT_STUDENT";
  array_splice($source_sem_arr, count($source_sem_arr), 0, $t_libdb->returnVector($sql));
  $sql =  "SELECT DISTINCT Semester FROM {$eclass_db}.MERIT_STUDENT";
  array_splice($source_sem_arr, count($source_sem_arr), 0, $t_libdb->returnVector($sql));
  $sql =  "SELECT DISTINCT Semester FROM {$eclass_db}.SERVICE_STUDENT";
  array_splice($source_sem_arr, count($source_sem_arr), 0, $t_libdb->returnVector($sql));
  $source_sem_arr = array_unique($source_sem_arr);

  $sql =  "
            SELECT DISTINCT
              YearTermNameEN
            FROM
              {$intranet_db}.ACADEMIC_YEAR_TERM
          ";
  $t_target_sem_arr = $t_libdb->returnVector($sql);

  for($i=0; $i<count($t_target_sem_arr); $i++)
  {
    $t_sem = $t_target_sem_arr[$i];

    for($j=1; $j<=count($sem_mapping); $j++)
    {
      if(strposa($t_sem, $sem_mapping[$j]) !== false)
      {
        $target_sem_arr[$j][] = $t_sem;
      }
    }
  }

  for($i=0; $i<count($source_sem_arr); $i++)
  {
    $source_sem = $source_sem_arr[$i];

    for($j=1; $j<=count($sem_mapping); $j++)
    {
      if(strposa($source_sem, $sem_mapping[$j]) !== false)
      {
        $result_sem_arr[$source_sem] = $target_sem_arr[$j][0];
      }
    }
  }

  return $result_sem_arr;
}

function strposa($haystack ,$needles=array(),$offset=0){
  $chr = array();
  foreach($needles as $needle){
    if (strpos($haystack,$needle,$offset) !== false) {
      $chr[] = strpos($haystack,$needle,$offset);
    }
  }
  if(empty($chr)) return false;
  return min($chr);
}

/**
 * Initially created for libpf_report::getYearSemesterForReportPrinting() to handle mismatch between OLE years and system years in a client site, uccke.
 * For example, while OLE years are of the format "2008-2009", system years may be of the format "08-09".
 * @return A mapping from OLE year to system year, which may be different for each client, like:
 * array(
 * 		// OLE year => system year.
 * 		"2005-2006" => "05-06",
 * 		"2006-2007" => "06-07",
 * 		"2007-2008" => "07-08",
 * 		"2008-2009" => "08-09",
 * 		"2009-2010" => "09-10"
 * )
 * Return null if mapping is not defined for a client.
 */
function getCustomOLEYearToSystemYearMapping()
{
	switch (CFG_SCHOOL_CODE) {
		case "UCCKE":
			return array(
				"2005-2006" => "05-06",
				"2006-2007" => "06-07",
				"2007-2008" => "07-08",
		 		"2008-2009" => "08-09",
				"2009-2010" => "09-10"
			);
		case "HoLap":
			return array(
				"2005-2006" => "2005-06",
				"2006-2007" => "2006-07",
				"2007-2008" => "2007-08",
		 		"2008-2009" => "2008-09",
				"2009-2010" => "2009-10"
			);
		default:
			return null;
	}
}

/**
 * Convert an array of OLE years to system years, based on the mapping returned by getCustomOLEYearToSystemYearMapping().
 * If getCustomOLEYearToSystemYearMapping() returns null, this function just returns the OLE years unchanged.
 * @param $OLEYearArray an array of OLE years, like array("2008-2009", "2009-2010").
 * @return an array of system years, like array("08-09", "09-10"). If no mapping is defined for an OLE year, no conversion will be performed.
 */
function convertListOfOLEYearToSystemYear($OLEYearArray)
{
	$ole_year_to_system_year_mapping = getCustomOLEYearToSystemYearMapping();
	
	if (!isset($ole_year_to_system_year_mapping)) return $OLEYearArray;
	if (count($OLEYearArray) == 0) return $OLEYearArray;
	
	$system_years = array();
	foreach ($OLEYearArray as $ole_year) {
		$system_year = $ole_year_to_system_year_mapping[$ole_year];
		$system_years[] = isset($system_year) ? $system_year : $ole_year;
	}
	return $system_years;
}

/**
 * Convert a system year to OLE year, based on the mapping returned by getCustomOLEYearToSystemYearMapping().
 * This parameter is not an array as in convertListOfOLEYearToSystemYear(), although it does a reverse conversion for convertListOfOLEYearToSystemYear().
 * If getCustomOLEYearToSystemYearMapping() returns null, this function just returns the system year unchanged.
 * @param $SystemYear a system year, like "08-09". The format may be different for each client.
 * @return an OLE year, like "2008-2009". If no mapping is defined for an OLE year, no conversion will be performed.
 */
function convertSystemYearToOLEYear($SystemYear)
{
	$ole_year_to_system_year_mapping = getCustomOLEYearToSystemYearMapping();
	
	if (!isset($ole_year_to_system_year_mapping)) return $SystemYear;
	
	$system_year_to_ole_year_mapping = array_flip($ole_year_to_system_year_mapping);
	$ole_year = $system_year_to_ole_year_mapping[$SystemYear];
	return isset($ole_year) ? $ole_year : $SystemYear;
}

?>