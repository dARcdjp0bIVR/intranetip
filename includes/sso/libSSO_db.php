<?php

/* Change Log:
 *  2019-09-18 Paul
 *  - Editied insertGroup to fix the issue of status saving title value and title storing status value
 *  2018.10.04 Chris
 *  - Edited and added in deleteSSOGroupIdentifier Variable
 * 2018.9.20 Chris
 *  - Edited and added in SSOGroupTitle Variable
 * 2016-11-22: (Henry HM) Create File
 */
/*SSO Identifier Legend
 *      a = Active GSuite account and enabled
 *      s= Suspended Gsuite Account and enabled
 *      n = Gsuite never enabled
 *      x= eclass account deletd
 * */

class libSSO_db extends libdb{
    
    // Business Function
    //User
    public function isUserHaveSSOIdentifiersAndReturnWithStatus($userID, $google_api){
        $ssoAccounts = $this->getSSOAccount($userID);
        $is_account_record_exist = false;
        $account_status = 'n';
        foreach($ssoAccounts as $ssoAccount){
            //`UserID`,`SSOAccountIdentifier`,`SSOAccountStatus`
            if(strpos($ssoAccount['SSOAccountIdentifier'],$google_api->getDomain()) !== false){
                $account_status = $ssoAccount['SSOAccountStatus'];
                $is_account_record_exist = true;
            }
        }
        return array(
            'is_account_record_exist' => $is_account_record_exist,
            'account_status' => $account_status
        );
    }
    
    public function addSSOIdentifier($userID, $ssoAccountIdentifier,$ssoAccountStatus='a'){
        $this->insert($userID, $ssoAccountIdentifier, $ssoAccountStatus);
    }
    
    public function suspendSSOIdentifier($userID, $ssoAccountIdentifier){
        $this->updateStatusByUserId($userID, $ssoAccountIdentifier,'s');
    }
    
    public function resumeSSOIdentifier($userID, $ssoAccountIdentifier){
        $this->updateStatusByUserId($userID, $ssoAccountIdentifier,'a');
    }
    
    public function deleteSSOIdentifier($userID, $ssoAccountIdentifier){
        $this->updateStatusByUserId($userID, $ssoAccountIdentifier,'x');
    }
    
    public function getSSOAccount($userID){
        return $this->selectByUserId($userID);
    }
    
    public function getSSOIdentifier($userID, $ssoAccountIdentifier){
        return $this->selectBySSOAccountIdentifier($userID, $ssoAccountIdentifier);
    }
    
    //Group
    public function isGroupHaveSSOIdentifiersAndReturnWithStatus($groupID, $google_api){
        $ssoGroups = $this->getSSOGroup($groupID);
        $is_group_record_exist = false;
        $gorup_status = 'n';
        foreach($ssoGroups as $ssoGroup){
            //GroupID SSOGroupIdentifier SSOGroupStatus
            if(strpos($ssoGroup['SSOGroupIdentifier'],$google_api->getDomain()) !== false){
                $gorup_status = $ssoGroup['SSOGroupStatus'];
                $is_group_record_exist = true;
            }
        }
        return array(
            'is_group_record_exist' => $is_group_record_exist,
            'gorup_status' => $gorup_status
        );
    }
    
    public function addSSOGroupIdentifier($groupID, $ssoGroupIdentifier,$SSOGroupTitle,$ssoGroupStatus='a'){//edit
        $rows_group = $this->selectGroupByGroupId($groupID);
        if(count($rows_group)>0){
            $row_group = $rows_group[0];
            $this->updateGroup($row_group['GroupID'], $ssoGroupIdentifier,$ssoGroupStatus,$SSOGroupTitle);
        }else{
            $this->insertGroup($groupID, $ssoGroupIdentifier, $ssoGroupStatus,$SSOGroupTitle);
        }
    }
    
    public function getSSOGroup($groupID){
        return $this->selectGroupByGroupId($groupID);
    }
    
    public function getSSOGroupIdentifier($groupID, $ssoGroupIdentifier){
        return $this->selectGroupBySSOGroupIdentifier($groupID, $ssoGroupIdentifier);
    }
    public function deleteSSOGroupIdentifier($groupID, $ssoGroupIdentifier,$SSOGroupTitle,$ssoGroupStatus='x'){
        $this->updateGroup($groupID, $ssoGroupIdentifier,$ssoGroupStatus,$SSOGroupTitle);
    }
    
    // Database Function
    //User
    private function insert($userID, $ssoAccountIdentifier, $ssoAccountStatus){
        $loginID = $_SESSION['UserID'];
        $sql = 'INSERT INTO `INTRANET_SSO_ACCOUNT`(`UserID`,`SSOAccountIdentifier`,`SSOAccountStatus`,`ModitiedDate`,`ModitiedBy`)' .
            ' VALUES(\''.$this->s($userID).'\',\''.$this->s($ssoAccountIdentifier).'\',\''.$this->s($ssoAccountStatus).'\',now(),\''.$this->s($loginID).'\');';
        $this->db_db_query($sql);
        
        $this->logHistory($userID, $ssoAccountIdentifier, $ssoAccountStatus);
    }
    
    private function selectByUserId($userID){
        $sql = 'SELECT `UserID`,`SSOAccountIdentifier`,`SSOAccountStatus`,`ModitiedDate`,`ModitiedBy` FROM `INTRANET_SSO_ACCOUNT` WHERE `UserID` = \''.$this->s($userID).'\';';
        return $this->returnResultSet($sql);
    }
    
    private function selectBySSOAccountIdentifier($userID, $ssoAccountIdentifier){
        $sql = 'SELECT `UserID`,`SSOAccountIdentifier`,`SSOAccountStatus`,`ModitiedDate`,`ModitiedBy` FROM `INTRANET_SSO_ACCOUNT` WHERE `UserID` = \''.$userID.'\' AND `SSOAccountIdentifier` = \''.$this->s($ssoAccountIdentifier).'\';';
        return $this->returnResultSet($sql);
    }
    
    private function updateStatusByUserId($userID, $ssoAccountIdentifier,$ssoAccountStatus){
        $loginID = $_SESSION['UserID'];
        $sql = 'UPDATE `INTRANET_SSO_ACCOUNT` SET `SSOAccountStatus` = \''.$this->s($ssoAccountStatus).'\',`ModitiedDate` = now(), `ModitiedBy`=\''.$this->s($loginID).'\' WHERE `SSOAccountIdentifier` = \''.$this->s($ssoAccountIdentifier).'\';';
        $this->db_db_query($sql);
        
        $this->logHistory($userID, $ssoAccountIdentifier, $ssoAccountStatus);
    }
    
    private function delete($userID){
        //no SQL Delete
    }
    
    private function logHistory($userID, $ssoAccountIdentifier, $ssoAccountStatus){
        $loginID = $_SESSION['UserID'];
        $sql = 'INSERT INTO `INTRANET_SSO_ACCOUNT_HISTORY`(`UserID`,`SSOAccountIdentifier`,`SSOAccountStatus`,`ModitiedDate`,`ModitiedBy`)' .
            ' VALUES(\''.$this->s($userID).'\',\''.$this->s($ssoAccountIdentifier).'\',\''.$this->s($ssoAccountStatus).'\',now(),\''.$this->s($loginID).'\');';
        $this->db_db_query($sql);
    }
    
    //Group
    
    private function insertGroup($groupID, $ssoGroupIdentifier, $ssoGroupStatus, $SSOGroupTitle){// edited
        $loginID = $_SESSION['UserID'];
        $sql = 'INSERT INTO `INTRANET_SSO_GROUP`(`GroupID`,`SSOGroupIdentifier`,`SSOGroupTitle`,`SSOGroupStatus`,`ModitiedDate`,`ModitiedBy`)' .
            ' VALUES(\''.$this->s($groupID).'\',\''.$this->s($ssoGroupIdentifier).'\',\''.$this->s($SSOGroupTitle).'\',\''.$this->s($ssoGroupStatus).'\',now(),\''.$this->s($loginID).'\');';
        $this->db_db_query($sql);
    }
    
    private function updateGroup($groupID, $ssoGroupIdentifier, $ssoGroupStatus, $SSOGroupTitle){//edited
        $loginID = $_SESSION['UserID'];
        $sql = 'UPDATE `INTRANET_SSO_GROUP` SET `SSOGroupIdentifier`=\''.$this->s($ssoGroupIdentifier).'\',`SSOGroupStatus`=\''.$this->s($ssoGroupStatus).'\',`SSOGroupTitle`=\''.$this->s($SSOGroupTitle).'\',`ModitiedDate`=now(),`ModitiedBy`=\''.$this->s($loginID).'\' WHERE `GroupID`=\''.$this->s($groupID).'\';';
        $this->db_db_query($sql);
    }
    
    private function selectGroupByGroupId($groupID){
        $sql = 'SELECT `GroupID`,`SSOGroupIdentifier`,`SSOGroupStatus`,`ModitiedDate`,`ModitiedBy`,`SSOGroupTitle` FROM `INTRANET_SSO_GROUP` WHERE `GroupID` = \''.$this->s($groupID).'\';';
        return $this->returnResultSet($sql);
    }
    
    private function selectGroupBySSOGroupIdentifier($groupID, $ssoGroupIdentifier){
        $sql = 'SELECT `GroupID`,`SSOGroupIdentifier`,`SSOGroupStatus`,`ModitiedDate`,`ModitiedBy`,`SSOGroupTitle` FROM `INTRANET_SSO_GROUP` WHERE `GroupID` = \''.$groupID.'\' AND `SSOGroupIdentifier` = \''.$this->s($ssoGroupIdentifier).'\';';
        return $this->returnResultSet($sql);
    }
    
    /* Shortcut Functions */
    
    // Get_Safe_Sql_Query()
    public function s($value){
        return $this->Get_Safe_Sql_Query($value);
    }
    
    /* END - Shortcut Functions */
}

?>