<?php
// using : Frankie
/*
 * Change Log:
 *      2018-07-04 Frankie
 *          - SSO from another site for DSI initial 
 */
if (!defined("LIBSSOCOMMON_DEFINED"))
{
	define("LIBSSOCOMMON_DEFINED", true);
	
	class libSsoCommon extends libdb
	{
	    
	    /**
	     * check Access Right
	     * @return boolean
	     */
	    public function checkAccessRight()
	    {
	        if (!$this->config['ssoEnv']["enabled"])
	        {
	            return false;
	        }
	        return true;
	    }
	    
		/**
		 * get HTTP HOST
		 * 
		 * @return string
		 */
	    public function getHttpHost()
	    {
	        if (function_exists('checkHttpsWebProtocol'))
	        {
	            return (checkHttpsWebProtocol() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];
	        } else {
	            return (isset($_SERVER["HTTPS"]) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];
	        }
	    }
	    
		/**
		 * JSON Encode
		 *
		 * @param string $string
		 * @return string
		 */
		public function jsonEncode($string = "")
		{
		    return $this->json->encode($string);
		}
		
		/**
		 * JSON Decode
		 *
		 * @param string $string
		 * @return string
		 */
		public function jsonDecode($string = "")
		{
		    return $this->json->decode($string);
		}
		
		/**
		 * get SSO Dsi Config by Key
		 *
		 * @param string $key
		 * @return unknown|string
		 */
		public function getConfig($key='')
		{
		    if ($key == "all")
		    {
		        return $this->config;
		    } else {
		        return isset($this->config[$key]) ? $this->config[$key] : "";
		    }
		}
		
		/**
		 * Get Real Client IP
		 *
		 * @param unknown $default
		 * @param number $filter_options
		 * @return string|null
		 */
		public function getClientRealIp($default = NULL, $filter_options = 12582912)
		{
		    $ipaddress = null;
		    $ipaddress = '';
		    if (isset($_SERVER['HTTP_CLIENT_IP']))
		        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		            else if(isset($_SERVER['HTTP_X_FORWARDED']))
		                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		                else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
		                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		                    else if(isset($_SERVER['HTTP_FORWARDED']))
		                        $ipaddress = $_SERVER['HTTP_FORWARDED'];
		                        else if(isset($_SERVER['REMOTE_ADDR']))
		                            $ipaddress = $_SERVER['REMOTE_ADDR'];
		    
		    return $ip;
		}
		
		public function generateRandomString($length = 10) {
		    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < $length; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    return $randomString;
		}
		
		public function httpRequest($url, $param, $type)
		{
		    $ch = curl_init();

		    if (strtolower($type) == "post")
		    {
		        curl_setopt($ch, CURLOPT_URL, $url);
		        curl_setopt($ch, CURLOPT_POST, true);
		        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param));
		    } else {
		        if (strpos($url, '?') !== false) {
		            $url .= "&" . http_build_query($param);
		        } else {
		            $url .= "?" . http_build_query($param);
		        }
		        curl_setopt($ch, CURLOPT_URL, $url);
		    }
		    curl_setopt($ch, CURLOPT_HEADER, false);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    $output = curl_exec($ch);
		    $err    = curl_errno($ch);
		    $errmsg = curl_error($ch);
		    if(!$output){
		        return 'Error: "' . $errmsg. '" - Code: ' . $err;
		    }
		    curl_close($ch);
		    return true;
		}
		
		public function checkSsoSysConfig($request)
		{
		    $valid_sso_id = true;
		    $valid_sso_domain = true;

		    if ($this->config['ssoEnv']['valid_sso_id'])
		    {
		        if (!isset($request["client_id"]) || !in_array($request["client_id"], $this->config['ssoEnv']['sso_id_list']))
	            {
	                $valid_sso_id = false;
		        }
		    }
		    $hostInfo = parse_url($request['redirect_url']);
		    if ($this->config['ssoEnv']['valid_domain'])
		    {
		        if (!isset($hostInfo["host"]) || !in_array($hostInfo["host"], $this->config['ssoEnv']['domain_list']))
		        {
		            $valid_sso_id = false;
		        }
		    }
		    return $valid_sso_id && $valid_sso_domain;
		}
	}
}