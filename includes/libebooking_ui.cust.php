<?php
# using:

include_once('libebooking.php');
include_once('libuser.php');

class libebooking_ui_wkf extends libebooking_ui {
	var $thickBoxWidth;
	var $thickBoxHeight;
	
	function libebooking_ui_wkf()
	{
		parent::libebooking_ui();
		$this->thickBoxHeight = '600px';
		$this->thickBoxWidth = '600px';
	}
	
	function Show_WKF_Custom_Report_Export($LocationID, $ManagementGroupID, $BookingStatus, $BookingDateType)
	{
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $Lang;

		include_once("libinventory.php");
		$linventory = new libinventory();
			
		$curr_date = date("Y-m-d");
				
		list($StartDate, $EndDate) = $this->Get_DateRange_By_DateType($BookingDateType);
		
		if($BookingStatus != "") {
			$BookingStatus = explode(",",$BookingStatus);
		}
		
		if($LocationID == "ALL")
		{
			$sql = "SELECT LocationID FROM INVENTORY_LOCATION WHERE RecordStatus = 1 AND AllowBooking = 1";
			$LocationID = $this->returnVector($sql);
		}
		else
			$LocationID = explode(",",$LocationID);
		
		$arrResult = $this->Get_Facility_Booking_Record(LIBEBOOKING_FACILITY_TYPE_ROOM, $LocationID, $StartDate, $EndDate, $BookingStatus,'',1,1,'',$ManagementGroupID, NULL, " br.Date ASC, br.StartTime ASC, br.StartTime ASC ");

		$tempRows = array(
			$Lang['eBooking']['General']['Export']['FieldTitle']['BookedBy'],
			$Lang['eBooking']['General']['Export']['FieldTitle']['RequestDate'],
			$Lang['eBooking']['General']['Export']['FieldTitle']['Date'],
			$Lang['eBooking']['General']['Export']['FieldTitle']['Time'],
			$Lang['eBooking']['General']['Export']['FieldTitle']['Day'],
			$Lang['eBooking']['General']['Export']['FieldTitle']['Venue'],
			$Lang['eBooking']['General']['Export']['FieldTitle']['ActivityName'],
			$Lang['eBooking']['General']['Export']['FieldTitle']['PIC'],
			$Lang['eBooking']['General']['Export']['FieldTitle']['Attandance'],
			$Lang['eBooking']['General']['Export']['FieldTitle']['Details']
		);
		$Rows[] = $tempRows; 
		
		if(sizeof($arrResult) > 0)
		{
			for($i=0; $i<sizeof($arrResult); $i++)
			{
				 $booking_id = $arrResult[$i]['BookingID'];
				 $room_id = $arrResult[$i]['FacilityID'];
				 $booking_date_str = $arrResult[$i]['Date'];
				 $booking_start_time = $arrResult[$i]['StartTime'];
				 $booking_end_time = $arrResult[$i]['EndTime'];
				 $booking_related_to = $arrResult[$i]['RelatedTo'];
				 $booking_remark = $arrResult[$i]['Remark'];
				 $booking_request_by = $arrResult[$i]['RequestedByUserName'];
				 $booking_request_date = $arrResult[$i]['RequestDate'];
				 $booking_responsible_ppl = $arrResult[$i]['ReponsibleUserName'];
				 $booking_pic = $arrResult[$i]['PICUserName'];
				 $booking_process_date = $arrResult[$i]['ProcessDate'];
				 $booking_status = $arrResult[$i]['BookingStatus'];
				 $room_name = $arrResult[$i]['FacilityName'];						

				$ts = strtotime($booking_date_str);
				$display_date = date("j-M-y",$ts);
				$weekday = date("l",$ts);
				
				# Get Cust Remark Field
				$CustRemarks = $this->returnCustRemark($booking_related_to);
				$ActivityName = $CustRemarks['ActivityName'];
				$PIC = $CustRemarks['PIC'];
				$Attandance = $CustRemarks['Attendance'];
				
				$tempRows = array(
					$booking_request_by,
					$booking_request_date,
					$display_date,
					date("g:ia",strtotime($booking_start_time))." - ".date("g:ia",strtotime($booking_end_time)),
					$weekday,
					$room_name,
					$ActivityName,
					$PIC,
					$Attandance,
					$booking_remark
				);
				$Rows[] = $tempRows;
			}
		}
		else
		{
			$Rows[] = array($Lang['General']['NoRecordAtThisMoment']);
		}
		
		return $Rows;
	}
	
	function DisplayCustRemarkFields($RelatedToID)
	{
		global $sys_custom;
		
		$data = $this->returnCustRemark($RelatedToID);
		
		if($sys_custom['eBooking_Cust_Remark']['WongKamFai'])
		{
			$x .= "<input type='radio' name='ActivityType' value='SS' id='SS' ". ($data['ActivityType']=="SS" ? "checked" : "") ."> <lable for='SS'>SS</lable> ";
			$x .= "<input type='radio' name='ActivityType' value='PS' id='PS' ". ($data['ActivityType']=="PS" ? "checked" : "") ."> <lable for='PS'>PS</lable> ";
			$x .= "<input type='radio' name='ActivityType' value='CO' id='CO' ". ($data['ActivityType']=="CO" ? "checked" : "") ."> <lable for='CO'>CO</lable> ";
			$x .= "<br>";
			
			$x .= "<table width='100%'>";
			$x .= "<tr><td nowrap><span class='tabletextrequire'>*</span> Name of Activity:</td><td width='100%'><input type='text' name='ActivityName' id='ActivityName' class='textbox_name' value='". $data['ActivityName'] ."'></td></tr>";
			$x .= "<tr><td nowrap><span class='tabletextrequire'>*</span> Class / Expected No. of Participants:</td><td width='100%'><input type='text' name='Attendance' id='Attendance' class='textbox_name' value='". $data['Attendance'] ."'></td></tr>";
			$x .= "<tr><td nowrap><span class='tabletextrequire'>*</span> Person-in-charge:</td><td width='100%'><input type='text' name='PIC' id='PIC' class='textbox_name' value='". $data['PIC'] ."'></td></tr>";
			$x .= "</table>";
			$x .= "<br>";
			
			$x .= "No. of Staff Required on Activity Day<br>";
			$x .= "<table width='100%'><tr>";
			$x .= "<td width='25%'>IT Technician: <input type='text' name='IT_Teachnician' id='IT_Teachnician' class='textboxnum2' value='". ( $data['IT_Teachnician'] ?  $data['IT_Teachnician'] : 0)  ."'></td>";
			$x .= "<td width='25%'>Photographer: <input type='text' name='Photographer' id='Photographer' class='textboxnum2' value='". ( $data['Photographer'] ?  $data['Photographer'] : 0)  ."'></td>";
			$x .= "<td width='25%'>Clerical Staff: <input type='text' name='Clerical' id='Clerical' class='textboxnum2' value='". ( $data['Clerical'] ?  $data['Clerical'] : 0)  ."'></td>";
			$x .= "<td width='25%'>Janitor: <input type='text' name='Janitor' id='Janitor' class='textboxnum2' value='". ( $data['Janitor'] ?  $data['Janitor'] : 0)  ."'></td>";
			$x .= "</tr></table>";
			$x .= "<br>";
			
			$x .= "REQUIREMENT:<br>";
			$x .= "<table width='100%'>";
			$x .= "<tr>";
			$x .= "<td width='20%'><input type='checkbox' name='PhotoTaking' value='1' id='PhotoTaking' ". ($data['PhotoTaking']=="1" ? "checked" : "") ." > <lable for='PhotoTaking'>Photo Taking</lable>";
			$x .= "<td width='20%'><input type='checkbox' name='VideoShooting' value='1' id='VideoShooting' ". ($data['VideoShooting']=="1" ? "checked" : "") ."> <lable for='VideoShooting'>Video Shooting</lable>";
			$x .= "<td width='20%'><input type='checkbox' name='Projector' value='1' id='Projector' ". ($data['Projector']=="1" ? "checked" : "") ."> <lable for='Projector'>Projector</lable>";
			$x .= "<td width='20%'><input type='checkbox' name='Visualizer' value='1' id='Visualizer' ". ($data['Visualizer']=="1" ? "checked" : "") ."> <lable for='Visualizer'>Visualizer</lable>";
			$x .= "<td width='20%'><input type='checkbox' name='Microphone' value='1' id='Microphone' ". ($data['Microphone']=="1" ? "checked" : "") ."> <lable for='Microphone'>Microphone</lable>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td width='20%'><input type='checkbox' name='MobilePASystem' value='1' id='MobilePASystem' ". ($data['MobilePASystem']=="1" ? "checked" : "") ."> <lable for='MobilePASystem'>Mobile PA System</lable>";
			$x .= "<td width='20%'><input type='checkbox' name='Laptop' value='1' id='Laptop' ". ($data['Laptop']=="1" ? "checked" : "") ."> <lable for='Laptop'>Laptop</lable>";
			$x .= "<td width='20%'><input type='checkbox' name='Signage' value='1' id='Signage' ". ($data['Signage']=="1" ? "checked" : "") ."> <lable for='Signage'>Signage</lable>";
			$x .= "<td width='20%'><input type='checkbox' name='Souvenir' value='1' id='Souvenir' ". ($data['Souvenir']=="1" ? "checked" : "") ."> <lable for='Souvenir'>Souvenir</lable>";
			$x .= "<td width='20%'><input type='checkbox' name='Stationery' value='1' id='Stationery' ". ($data['Stationery']=="1" ? "checked" : "") ."> <lable for='Stationery'>Stationery</lable>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td width='20%'><input type='checkbox' name='Flower' value='1' id='Flower' ". ($data['Flower']=="1" ? "checked" : "") ."> <lable for='Flower'>Flowe and/or Plants</lable>";
			$x .= "<td width='20%'><input type='checkbox' name='CateringServicer' value='1' id='CateringServicer' ". ($data['CateringServicer']=="1" ? "checked" : "") ."> <lable for='CateringServicer'>Catering Service</lable>";
			$x .= "<td width='20%'><input type='checkbox' name='Others' value='1' id='Others' ". ($data['Others']=="1" ? "checked" : "") ."> <lable for='Others'>Others</lable>";
			$x .= "</tr>";
			$x .= "</table>";
		}	
		
		$x .= "<br>";
		return $x;
	}
	
	
	
	
}
?>