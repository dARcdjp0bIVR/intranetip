<?php
if (!defined("LIBKANHANSMS_DEFINED"))                     // Preprocessor directive
{
define("LIBKANHANSMS_DEFINED", true);

include_once("$intranet_root/includes/libhttpclient.php");
include_once("$intranet_root/includes/libwebmail.php");
include_once("$intranet_root/lang/email.php");
class libkanhansms {

      var $service_host;
      var $service_port;
      var $service_action;
      var $data_action;
      var $usage_action;
      var $status_action;
      var $login;
      var $password;
      var $school_code;
      var $simulate_only;
      var $cancel_previous_action;
      var $start_transaction;
      var $end_transaction;

        function libkanhansms ()
        {
                 global $sms_kanhan_school_code, $sms_kanhan_host, $sms_kanhan_port, $sms_kanhan_login, $sms_kanhan_passwd,
                        $sms_kanhan_api_send_request, $sms_kanhan_api_get_reply, $sms_kanhan_api_get_usage, $sms_kanhan_api_get_status,
                        $sms_kanhan_simulate_only, $sms_kanhan_api_cancel_previous, $sms_kanhan_api_start_transaction, $sms_kanhan_api_end_transaction;
                 $this->service_host = $sms_kanhan_host;
                 $this->service_port = $sms_kanhan_port;
                 $this->service_action = $sms_kanhan_api_send_request;
                 $this->data_action = $sms_kanhan_api_get_reply;
                 $this->usage_action = $sms_kanhan_api_get_usage;
                 $this->status_action = $sms_kanhan_api_get_status;
                 $this->login = $sms_kanhan_login;
                 $this->password = $sms_kanhan_passwd;
                 $this->school_code = $sms_kanhan_school_code;
                 $this->simulate_only = $sms_kanhan_simulate_only;
                 
                 $this->cancel_previous_action = $sms_kanhan_api_cancel_previous;
                 $this->start_transaction_action = $sms_kanhan_api_start_transaction;
                 $this->end_transaction_action = $sms_kanhan_api_end_transaction;

                 if ($this->school_code == "")
                 {
                     die("Bad Configuration. Please contact eClass support. <support@broadlearning.com>");
                 }

                        /*
                 $this->service_host = "www.efaxonline.com";
                 $this->service_port = 80;
                 $this->service_action = "/sms/SendSmsBroadLearning.php";
                 $this->data_action = "/sms/DownloadReportBroadLearning.php";
                 $this->login = "eclass";
                 $this->password = "eclass";
                 $this->school_code = "1";
                 */
        }

        function sendSMS($recipient, $message, $message_code="", $delivery_time="", $isReplyMessage)
        {
	        	 global $sms_kanhan_debug_email, $sms_kanhan_debug_mobile, $webmaster;
	        	 
	             // add debug mobile
	        	 if($sms_kanhan_debug_mobile!=""){
		        	 $recipient.=",".$sms_kanhan_debug_mobile;
		        	 $message_code.=",0";
		         }
		         
                 $message = stripslashes($message);
                 $post_data = "login_id=".$this->login . "&password=".$this->password."&recipient=".$recipient;
                 $post_data .= "&school_code=".$this->school_code;
                 $post_data .= "&message=".urlencode($message)."&message_code=".$message_code."&delivery_time=".$delivery_time;

                 if ($this->simulate_only)
                 {
                     global $intranet_root;
                     $logpath = "$intranet_root/file/kanhan.send";
                     $content = get_file_content($logpath);
                     write_file_content ($content."\n".$post_data,$logpath);

                     return "SIMULATE";
                 }
                 else                 
                 {
	                 // send mail
			         if($sms_kanhan_debug_email!=""){
		        	 	$lwebmail = new libwebmail();
		        	 	$subject ="Send SMS Notification";
		        	 	$mail_message = "Message : $message\n<BR>";
		        	 	$mail_message.= "Recipient: $recipient\n<BR>";
		        	 	$mail_message.= "School Code : ".$this->school_code."\n<BR>";
		        	 	if($delivery_time!=""){
			        	 	$mail_message.= "Delivery Time : ".$delivery_time."\n<BR>";
			        	}
			        	if($lwebmail->has_webmail){
			         		$lwebmail->sendMail($subject,$mail_message,$webmaster,array($sms_kanhan_debug_email),array(),array(),"","",$webmaster,$webmaster);
			         	}
			         }
	              
                     # Try to connect to service host
                     $lhttp = new libhttpclient($this->service_host,$this->service_port);
                     if (!$lhttp->connect()){    # Exit if failed to connect
   
                          return false;
                     }
                    if($isReplyMessage){      
						//cancel the previous sms
						$target_url = "http://".$this->service_host.$this->cancel_previous_action."?login_id=".$this->login."&password=".$this->password."&school_code=".$this->school_code;
	                 	$fp = fopen($target_url,"r");
						fclose($fp);
						
						//start transaction
						$target_url = "http://".$this->service_host.$this->start_transaction_action."?login_id=".$this->login."&password=".$this->password."&school_code=".$this->school_code;
						$fp = fopen($target_url,"r");
						fclose($fp);
					}
					//send sms
                     $lhttp->set_path($this->service_action);
                     $lhttp->post_data = $post_data;
                     $response = $lhttp->send_request();
                     
                     # Put in log for debug
                     global $intranet_root;
                     write_file_content ($post_data,"$intranet_root/file/kanhan.send");
                     write_file_content ($response,"$intranet_root/file/kanhan.log");

                     # Filter the HTTP protocol message and extract the XML document only
                     $pos = strpos($response,"<?xml ");
                     if ($pos===false)
                     {
                                $pos = strpos($response,"<?XML ");
                     }
                     $response = substr($response,$pos);
                     $records = XML2Obj($response);
                     
                     if($isReplyMessage){
	                     //close transaction
	                     $target_url = "http://".$this->service_host.$this->end_transaction_action."?login_id=".$this->login."&password=".$this->password."&school_code=".$this->school_code;
	                 	 $fp = fopen($target_url,"r");
	                     fclose($fp);
                 	}
                     return $records;

                 }

        }
        
        # recipeintData : list of array(phone,message,message code,delivery time)
        function sendBulkSMS($recipientData,$isReplyMessage)
        {
	        	 global $sms_kanhan_debug_email, $sms_kanhan_debug_mobile, $webmaster;

		        if($recipientData=="" || sizeof($recipientData)<=0){
			        return false;
			    }

	             // add debug mobile
	        	 if($sms_kanhan_debug_mobile!=""){
		        	 //$recipient.=",".$sms_kanhan_debug_mobile;
		        	 //$recipientData[] = ($sms_kanhan_debug_mobile, $message, $message_code,$delivery_time) 
		         }
		         			    			    
			    if (!$this->simulate_only){
				    
			             # Try to connect to service host
			             $lhttp = new libhttpclient($this->service_host,$this->service_port);
			             if (!$lhttp->connect()){    # Exit if failed to connect
			                  return false;
			             }
			        	if($isReplyMessage){
							//cancel the previous sms
							$target_url = "http://".$this->service_host.$this->cancel_previous_action."?login_id=".$this->login."&password=".$this->password."&school_code=".$this->school_code;
			             	$fp = fopen($target_url,"r");
							fclose($fp);
							
							//start transaction
							$target_url = "http://".$this->service_host.$this->start_transaction_action."?login_id=".$this->login."&password=".$this->password."&school_code=".$this->school_code;
							$fp = fopen($target_url,"r");
							fclose($fp);
						}
				}
								
				$records = array();
				
				for($i=0;$i<sizeof($recipientData);$i++){
					list($recipient, $message, $message_code,$delivery_time) = $recipientData[$i];
                 	$message = stripslashes($message);
                 	$post_data = "login_id=".$this->login . "&password=".$this->password."&recipient=".$recipient;
                 	$post_data .= "&school_code=".$this->school_code;
                 	$post_data .= "&message=".urlencode($message)."&message_code=".$message_code."&delivery_time=".$delivery_time;

	                 if ($this->simulate_only)
	                 {
	                     global $intranet_root;
	                     $logpath = "$intranet_root/file/kanhan.send";
	                     $content = get_file_content($logpath);
	                     write_file_content ($content."\n".$post_data,$logpath);
	
	                     // return "SIMULATE";
	                     $records[] = "SIMULATE";
	                 }
	                 else                 
    	             {
 

                          
	   					//send sms
	                     $lhttp->set_path($this->service_action);
	                     $lhttp->post_data = $post_data;
	                     $response = $lhttp->send_request();
                     
	                     # Put in log for debug
	                     global $intranet_root;
	                     write_file_content ($post_data,"$intranet_root/file/kanhan.send");
	                     write_file_content ($response,"$intranet_root/file/kanhan.log");

	                     # Filter the HTTP protocol message and extract the XML document only
	                     $pos = strpos($response,"<?xml ");
	                     if ($pos===false)
	                     {
	                                $pos = strpos($response,"<?XML ");
	                     }
	                     $response = substr($response,$pos);
	                     $records[] = XML2Obj($response);
	                     

                 	 }
                 	 
 
             	}
             	if(!$this->simulate_only){
	             		if($isReplyMessage){
	       	         	     //close transaction
			                 $target_url = "http://".$this->service_host.$this->end_transaction_action."?login_id=".$this->login."&password=".$this->password."&school_code=".$this->school_code;
			             	 $fp = fopen($target_url,"r");
			                 fclose($fp);
						}
	            }
 	
                return $records;


        }
		
		function retrieveReply($start, $end)
        {
            $start = str_replace(" ", "%20", $start);
            $end = str_replace(" ", "%20", $end);

		    $target_url = "http://".$this->service_host.$this->data_action."?login_id=".$this->login."&password=".$this->password."&start_date=".$start."&end_date=".$end;
			$fp = fopen($target_url,"r");
			$content = "";
            while (!feof($fp))
            {
				$content .= fgets($fp, 4096);
            }
            fclose($fp);

            $lines = explode("\n",$content);
            $data = array();
            for ($i=0; $i<sizeof($lines); $i++)
            {
                $raw = explode(",",$lines[$i]);
                $data[$i] = $raw;
            }
            return $data;
        }

        
        function retrieveMessageCount($start, $end)
		{
			$target_url = "http://".$this->service_host.$this->usage_action
				."?login_id=".$this->login
				."&password=".$this->password
				."&school_code=".$this->school_code
				."&start_date=".urlencode($start)
				."&end_date=".urlencode($end);
				
                 $fp = fopen($target_url,"r");

                 if (!$fp)
                 {
                      # Server Down
                      return 0;
                 }
                 
                 $data = array();
				while (!feof($fp)) 
				{
					$data[] = fgets($fp, 4096);
				}
				fclose($fp);
				
				list($scode, $c) = split(",",$data[1]);

				return trim($c);
		}


}
function XML2Obj ($xDoc)
{
         $data = $xDoc;
         $parser = xml_parser_create();
         xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
         xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,1);
         xml_parse_into_struct($parser,$data,$values,$tags);
         xml_parser_free($parser);

         // loop through the structures
         foreach ($tags as $key=>$val)
         {
                  if ($key == "item")   # Specified in KanHan Document, each record is inside tag <msg>
                  {
                      $recordranges = $val;
                      // each contiguous pair of array entries are the
                      // lower and upper range for each record definition
                      for ($i=0; $i < count($recordranges); $i+=2)
                      {
                           $offset = $recordranges[$i] + 1;
                           $len = $recordranges[$i + 1] - $offset;
                           $tdb[] = parseRecord(array_slice($values, $offset, $len));
                      }
                  }
                  else
                  {
                      continue;
                  }
         }
         return $tdb;

}
function parseRecord($mvalues) {
    for ($i=0; $i < count($mvalues); $i++)
        $mol[$mvalues[$i]["tag"]] = $mvalues[$i]["value"];
    return new kanhanSMSRecord($mol);
}

# Data Structure only
class kanhanSMSRecord
{
      var $recipient;
      var $reference_id;
      var $message_code;

      function kanhanSMSRecord($aa)
      {
               foreach ($aa as $k=>$v)
                        $this->$k = $aa[$k];
      }
}

}        // End of directive
?>