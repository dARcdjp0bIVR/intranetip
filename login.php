<?php
## Using by :

/* ******************** Start of Important Note ********************* */
/*
 * 	Since 16 Jun 2015, this file is updated to support PHP 5.4 or above and backward compatibile to php 5.16
*  There are several big changes. Below 3 dependencies are removed from php 5.4
* 		1. global_register
* 		2. session_register
* 		3. magic quote support
*
* 	And corresponding solutions are used to replicate the magic quotes and globel register. For Details, please check lib.php
*
* 	The Backup file:
* 		146:31002/login.php.201506161553
*/
/* ********************* End of Important Note ********************** */

/********************** Change Log ***********************/
#   Date    :   2020-09-09 Pun [ip.2.5.11.9.1] Add back flipped channels flag checking (revert 2017-03-20 Pun)
#   Date    :   2020-08-05 Bill [ip.2.5.11.8.1] skip login secure token checking if using app to access module webview     [2020-0805-0918-34235]
#   Date    :   2020-06-26 Cameron Add $OnlyModule='eInventoryStocktake'
#   Date    :   2020-03-23 Bill [ip.2.5.11.3.1] Handle LDAP webview / sso access problem  [2020-0221-1443-40207]    (*** need to upload wtih libeClassApp.php)
#   Date    :   2020-02-13 Tiffany add eLearningTimetable for app sso control
#   Date    :   2020-02-13 Tiffany [ip.2.5.11.3.1] add AppType for iMail SSO
#   Date    :   2020-02-12 Cameron [ip.2.5.11.3.1] add eInventoryApp for eClass teacher app
#   Date    :   2020-02-11 Bill [ip.2.5.11.3.1] Handle eClassApp webview access for client using LDAP   [2020-0203-1544-25105]
#   Date    :   2019-10-17 Tiffany add iMail in sso control
#   Date    :   2019-09-18 Paul [ip.2.5.10.10.1] for Google SSO login $ssoservice["Google"]["Valid"], retrieve hashed directly after passing the time-limited login token
#   Date    :   2019-08-21 Tiffany [ip.2.5.10.10.1] modify ePosParent to ePOS
#   Date    :   2019-07-23 Cameron - also set $_SESSION['intranet_elibrary_usertype'] if $plugin['library_management_system'] is set [case #M165440]
#	Date	:	2019-07-16 Tiffany - Added SchoolInfoPDF to sso control
#	Date	:	2019-07-16 Tiffany - Added eEnrolmentStudentView to sso control
#	Date	:	2019-06-04 Carlos - Added flag $sys_custom['LoginForceCsrfTokenChecking'] to force check CSRF token even client use own customized login page.
#   Date    :   2019-05-28 Anna[ip.2.5.10.6.1] added  $_SESSION['eClassAppSSO_standaloneModule'] for $sys_custom['OnlineRegistry'], app view dont't need change to register page
#   Date    :   2019-05-13 Anna Added IntegerSafe and single quoteto UserID to avoid sql injection
#   Date    :   2019-03-26 Cameron [ip.2.5.10.4.1] don't set target_url to empty for kis parent login [#Y140316]
#	Date	:	2019-02-20 Carlos [ip.2.5.10.2.1] Exclude broadlearning from LDAP authentication.
#	Date	:	2019-01-04 Carlos [ip.2.5.10.2.1] Modified LDAP authentication to support validate user with domain. Need to deploy with /includes/libldap.php
#   Date    :   2018-10-08 Anna[ip.2.5.9.7.1] CD#4- fix shoe blank page when IsConsecutiveUnsuccessfulLogins
#	Date	:	2018-09-07 Carlos [ip.2.5.9.7.1] CD#2 - added a session var "LOGIN_INTRANET_SESSION_USERID" to save the real UserID. Then can double check UserID in intranet_auth().
#   Date    :   2018-08-21 Cameron [ip.2.5.9.10.1] - HKPF bypass strong password rule that redirect to home/iaccount/account/login_password.php
#   Date    :   2018-07-11 Ivan [ip.2.5.9.10.1] - added eEnrolment App SSO logic
#   Date    :   2018-05-15 Cameron [ip.2.5.9.5.1] - add medicalCaringSleep, medicalCaringSleep_v2 and medicalCaringSleepRecordDetails_v2,
#               add flag control $sys_custom['medical']['swapBowelAndSleep'] to medicalCaringBowel_v2
#	Date	:	2018-05-08 Siuwan [ip.2.5.9.5.1] - added $_SESSION["FlippedChannelsServerPublicPath"]
#	Date	:	2018-04-06 Pun [ip.2.5.9.5.1] - Fix school website cannot login to portal for KIS
#	Date	:	2018-03-27 Carlos [ip.2.5.9.3.1.1] - marked $skipCheckingStrongPassword to skip strong password policy checking for LDAP, Cust login, App, KIS, etc.
#	Date	:	2018-03-09 Ivan [ip.2.5.9.3.1] - skip strong password policy checking for eClass App SSO pages
#	Date	:	2018-01-15 Carlos [ip.2.5.9.1.1] - Use $sys_custom['UseStrongPassword'] to control use strong password.
#	Date	:	2018-01-04 Carlos [ip.2.5.9.1.1] - if user password does not fulfil password safety criteria, set $_SESSION['FORCE_CHANGE_PASSWORD'] and redirect to /home/iaccount/account/login_password.php to force user change password.
#   Date	:   2018-01-03 Ivan [ip.2.5.9.1.1]
#               added cases to support SSO for "medicalCaringBowel_v2" and "medicalCaringLog_v2"
#	Date	:	2017-10-31 Bill [ip.2.5.9.1.1]		[2017-0403-1552-54240]
#				added eDis HOY Member Checking ($sys_custom['eDiscipline']['HOY_Access_GM'])
#	Date	:	2017-08-25 Ivan [ip.2.5.8.7.1 CD3]
#				added teacher app ediscipline SSO logic
#	Date	:	2017-06-22 HenryHM
#				added classroom in $OnlyModule switch cases
#	Date	:	2017-05-11 Pun
#				added redirect to iPortfolio for cust STEM Learning Portfolio on
#	Date	:	2017-03-20 Cameron
#				add eGuidance
#	Date	:	2017-03-20 Pun [ip.2.5.8.4.1]
#				removed flipped channel module flag checking
#	Date	:	2017-01-18 Ronald [ip.2.5.8.1.1]
#				add KISworksheet logic
#	Date	:	2016-10-03 Jason [ip.2.5.7.10.1]
#				support login by Google Account, $ssoservice["Google"]["Valid"]
#				support the redirection of google app like gmail
#	Date	:	2016-09-05 Pun [ip.2.5.7.10.1]
#				Change retrieveSessionKey load from libdb getSessionKeyLastUpdatedFromUserId()
#	Date	:	2016-05-20 (Paul) [ip.2.5.7.7.1] - add new PowerFlip flag for PowerFlip Cloud SaaS
#	Date	:	2016-05-03 (Paul) [ip.2.5.7.7.1] - customization of login flow for CC EAP project
#	Date	:	2016-03-21 (Carlos) [ip.2.5.7.4.1] - for LDAP login, can skip synchronizing password to iMail plus and iFolder as the index page would handle it.
#	Date	:	2016-03-09 (Paul) [ip.2.5.7.3.1] - added PowerFlip flag for eClass
#	Date	:	2015-12-11 (Ivan) [ip.2.5.7.1.1] - added app single SSO logic
#	Date	:	2015-10-30 (Bill)	- support API server for HKEdCity EdConnect
#	Date	:	2015-10-02 (Bill)	- support login eClass using HKEdCity Account when $ssoservice["HKEdCity"]["Valid"] = true
#	Date	:	2015-09-09 (Carlos) - added $sys_custom['target_url_for_login'] for customized login page client to redirect to absolute link.
#	Date	:	2015-09-01 (Carlos) - exclude checking login token if customized login page /templates/login.customized.php exist.
#
#	Date	:	2015-08-25 (Bill)	[2015-0825-1031-39071]
#				fixed user with access right cannot find eDisicipline in header > eAdmin if not an eDiscipline admin
#				- use session_register_intranet() for SESSION_ACCESS_RIGHT, to perform eDiscipline access right checking in UPDATE_CONTROL_VARIABLE()
#
#	Date	:	2015-08-17 (Carlos) Modified LDAP auth logic to search ldap servers with nslookup.
#									If UserLogin contains @domain, strip it.
#
#	Date	:	2015-08-13 (Carlos) Cater customized login page from school site.
#				2015-07-16 (Carlos) [ip2.5.6.7.1.0]
#				check secure token to prevent CSRF.
#
#	Date	: 	2015-06-16 (Jason) [ip.2.5.6.7.1.0]
#				support php 5.4 or above
#
#	Date	:	2015-03-25	(Siuwan) [ip.2.5.6.5.1.0]
#				added redirect case for global voices and read to succeed publisher, set in $special_feature['iTextbookUserAccountLimit']
#
#	Date	:	2015-03-16	(Ivan) [ip.2.5.6.5.1.0]
#				added session "FlippedChannelsServerPath" to store flipped channels central server path
#
#	Date	:	2014-11-05	(Charles Ma) 20141105-emag-provider [ej.5.0.4.11.1]
#				special redirect for emag user account
#
#	Date	:	2014-10-27	(Adam)
#				Add Case In $OnlyModule Selection so that it can be directed to eNotice through SSO
#
#	Date	:	2014-10-24	(Omas)
#				Add Checking if enable alert change email settings redirecting New user to edit email
#
#	Date	:	2013-09-09 (Ivan) [2013-0903-1525-07169]
#				Fixed: eBooking single sign on check the hash password directly
#
#	Date	:	2013-07-09 (Yuen)
#				Online Reg completed
#
#	Date	:	2013-04-12 (Yuen)
#				Force to change password if it is the same as UserLogin
#
#	Date	:	2013-03-28 (YatWoon)
#				add $sys_custom['OnlineRegistry'] (requested by UCCKE)
#
#	Date	:	2013-02-19 (YatWoon)
#				For eDiscipline approval right logic revise
#				If user hasn't "approve" in access rights setting, but in approval group, don't set the APPROVAL session ($UserAccessRight['DISCIPLINE']['MGMT']['AWARD_PUNISHMENT']).
#
#	Date	: 	2013-01-17	Thomas
#				Change variable $cached_hh_id from "eClassUser".$_SESSION['UserID'] to "U".$_SESSION['UserID']."T".$_SESSION['UserType']."P".$_SESSION['eclass_session_password']."L".strlen($intranet_root)
#				which sync the value in home_header.php and logout.php
#
#	Date	:	2013-01-17	Thomas
#				Add session variable $_SESSION['ck_from_pl'] to indicate the login is come from PowerLesson Login Page
#
#	Date	:	2012-12-20	YatWoon
#				Add $AccessLoginPage, allow user to still access the login page during the maintenance
#
# 	Date	:	2012-11-07 (Carlos)
#				sync user password to iMail plus account for LDAP authentication method
#
#	Date	:	2012-08-20 (Fai)
#				create eclass user for those user with iPF access right but no account
#
#	Date	:	2012-07-17 (Jason)
#				register the 190 Project - Wong Wah San eLearning Project session ck_wws_elearning_project
#
#	Date	:	2012-07-12 (Yuen)
#				Introduced failed attempt control to login
#
#	Date	: 	2012-06-11 [Jason]
#				register the powerlesson media conversion type into session ck_power_lesson_media_conversion_type
#
#	Date	: 	2012-05-23 [Jason]
#				add powerlesson expiry date to the session - ck_power_lesson_expiry_date
#
#	Date	:	2012-05-03 [Yuen]
#				skip uncessary codes for being called by PowerLesson App so that performance can be optimized
#
#	Date	:	2011-02-23 [Marcus]
#				added $elcass_server_url in session to store the site url, for eClassStore
#
#	Date	:	2011-12-15 [Kelvin]
#				added $ss_intranet_plugin['lslp_asp_server'] so eclass econtent can retrieve the server url
#
#	Date	:	2011-12-14 [Yuen]
#				redirect to password change page if password policy is enabled and the user password does not fulfill it
#
#	Date	:	2011-12-09 [Yuen]
#				disabled checking with support.broadlearning.com
#
#	Date	:	2011-10-26 [Ivan]
#				Added $_SESSION['EnglishName'] and $_SESSION['ChineseName']

#	Date	:	2011-10-07 [YatWoon]
#				Fixed: missing to check user status for LDAP [Case#2011-0922-1425-07014]
#
#	Date	:	2011-09-30 [Carlos]
#				Added two-way hashed password in case of school use hash password, we can reverse hashed password for use e.g. creating mail account
#
#	Date 	: 	2011-09-09 [Jason]
#				Add Session for PowerLesson, PowerBoard
#
#	Date	:	2011-08-30 [Henry Chow]
#				set $_SESSION["SSV_USER_ACCESS"]["eAdmin-InvoiceMgmtSystem"] = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"]
#
#	Date	:	2011-08-23 [YatWoon]
#				Add alumni login
#
#	Date	:	2011-06-07 [Thomas]
#				Add iTextbook to SESSION
#
# 	Date	: 	2011-05-18 [Yuen]
#				Support single-sign-on from EJ for eBooking
#
# 	Date	: 	2011-05-17 [Yuen]
#				Reset cached data (Cache Lite)
#
#	Date	:	2011-05-06 (Henry Chow)
#				eDis : user in "Approval Group" should automatically have "Approval" right even not in any Access Right Group / the group does not have "Approval" right
#
#	Date	:	2011-05-05 (Yuen)
#				customized the mapping to demo accounts (s & t) for demo sites (for plain-text password only)
#
#	Date	:	2011-04-01 (Henry Chow)
#				checking of $_SESSION['SESSION_ACCESS_RIGHT'] should be independance to "eDis" module
#
#	Date	:	2011-03-11 (Henry Chow)
#				allow student to access eAdmin > eDis if $ldisciplinev12->AllowToAssignStudentToAccessRightGroup is true
#
#	Date	:	2011-03-02 FAI
#				Add a session $_SESSION['SSV_isiPortfolio_standalone'] = true if it is a standalone iPortfolio
#
#	Date	:	2011-02-09 Marcus
#				Clear Reading Scheme Session when user re-login
#
#	Date	:	2010-12-14 YatWoon
#				Check force default lang, Customization [PJ-2010-1331]
#
#	Date	:	2010-09-28 [Kelvin]
#				Add sesstion for LER
#
#	Date	:	2010-09-07 [Kelvin]
#				Add sesstion for LSLP
#
#	Date	:	2010-09-03 [YatWoon]
#				Add sesstion for LastLang selection
#
#	Date	: 	2010-08-18 [Sandy]
#	Detail	: 	Added session for PowerSpeech
#
#	Date	:	2010-07-01 [Yuen]
#	Detail	:	Fixed to update session login when using LDAP login method
#
#	Date	:	2010-06-30 [Marcus]
#	Detail	:	Check gamma mail permission of identity
#
#	Date	:	2010-06-02 [Yuen]
#	Detail	:	don't load lang files to save 2MB memory
#
#	Date	:	2010-05-06 [YatWoon]
#	Detail	:	"wong siu chi" case, fixed: student cannot access eDiscipline (missing to check the normal Student permission, only check with AdminGroup)
#
#	Date	:	2010-03-23 (Fai)
#	Detail	:	IP25 Support with standalone iPortfolio $stand_alone['iPortfolio']
#
#	Date	:	2010-02-26 (Marcus)
#	Detail	:	add $_SESSION['SSV_EMAIL_LOGIN'],$_SESSION['SSV_EMAIL_PASSWORD'],$_SESSION['SSV_LOGIN_EMAIL]
#
#	Date	:	2009-12-15 (Henry)
#	Detail	:	move the $sys_custom['wscss_disciplinev12_student_access_cust'] from the bototm to eDisciplinev12 access right checking
#
/******************* End Of Change Log *******************/

@ini_set('memory_limit', -1);

##########
# remark of IP20
# Kenneth:
# For any change in the logic of removing mail records and attachment, pls also apply changes to
# /home/imail/trash_remove.php and /home/imail/empty_trash.php
##########


//$DebugMode = true;
//$debugMemory = true;
//$debugMemoryUserId = 1664;
//$debugUserLogin = 'ivanko_t';

//die();

@session_start();

$PATH_WRT_ROOT = "./";


$target_e = (isset($_GET['target_e']) && $_GET['target_e'] != '') ? $_GET['target_e'] : "";

$myVersion = '5.4';
$sys_arr = explode(".", phpversion());
$my_arr = explode(".", $myVersion);
$result = "SAME";
for ($i=0; $i<sizeof($my_arr); $i++)
{
	if ($my_arr[$i]>$sys_arr[$i])
	{
		$result = "ELDER";
		break;
	} elseif ($my_arr[$i]<$sys_arr[$i])
	{
		$result = "LATER";
		break;
	}
}
switch($result){
	case 'SAME':
	case 'LATER':
		// This replicates register_globals in PHP 5.4+
		foreach (array_merge($_GET, $_POST) as $global_register_key => $global_register_val) {
			global $$global_register_key;
			$$global_register_key = $global_register_val;
		}
		break;
	default:
		break;

}

## SSO from Another Site ##
if (isset($_SESSION["ECLASS_SSO"]["client_credentials"]))
{
    // 5 min expired
    $expireAfter = 5;
    $secondsInactive = time() - $_SESSION["ECLASS_SSO"]["client_credentials"]["expiredTime"];
    $expireAfterSeconds = $expireAfter * 60;
    if($secondsInactive <= $expireAfterSeconds)
    {
        $client_credentials = $_SESSION["ECLASS_SSO"]["client_credentials"];
    }
    if (isset($_SESSION["ECLASS_SSO"]["client_credentials"]["target_e"]))
    {
        $target_e = $_SESSION["ECLASS_SSO"]["client_credentials"]["target_e"];
    }
}

# reset
if ($target_e == '' && !($OnlyModule!="" && $UserLogin=="" && $UserPassword=="" && $_SESSION['tmp_UserLogin']!="" && $_SESSION['tmp_UserPassword']!=""))
{
	@session_unset();
}

## SSO from Another Site ##
if (isset($client_credentials))
{
    $_SESSION["ECLASS_SSO"]["client_credentials"] = $client_credentials;

}


# don't load any language wordings
$NoLangWordings = true;


include_once("includes/global.php");

if (is_file("$intranet_root/servermaintenance.php") && !$iServerMaintenance && !$AccessLoginPage)
{
	header("Location: servermaintenance.php");
	exit();
}

$accumulateMemory = 0;
if ($debugMemory && $UserLogin==$debugUserLogin) {
	$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	$accumulateMemory = convert_size(memory_get_usage(true));
	debug_pr('start = '.$memoryDiff.' / '.$accumulateMemory);
}
include_once("includes/libdb.php");
if ($debugMemory && $UserLogin==$debugUserLogin) {
	$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	$accumulateMemory = convert_size(memory_get_usage(true));
	debug_pr('After libdb = '.$memoryDiff.' / '.$accumulateMemory);
}
include_once("includes/libauth.php");
if ($debugMemory && $UserLogin==$debugUserLogin) {
	$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	$accumulateMemory = convert_size(memory_get_usage(true));
	debug_pr('After libauth = '.$memoryDiff.' / '.$accumulateMemory);
}
include_once("includes/libuser.php");
if ($debugMemory && $UserLogin==$debugUserLogin) {
	$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	$accumulateMemory = convert_size(memory_get_usage(true));
	debug_pr('After libuser = '.$memoryDiff.' / '.$accumulateMemory);
}
include_once("includes/user_right_target.php");
if ($debugMemory && $UserLogin==$debugUserLogin) {
	$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	$accumulateMemory = convert_size(memory_get_usage(true));
	debug_pr('After user_right_target = '.$memoryDiff.' / '.$accumulateMemory);
}
include_once("includes/libldap.php");
if ($debugMemory && $UserLogin==$debugUserLogin) {
	$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	$accumulateMemory = convert_size(memory_get_usage(true));
	debug_pr('After libldap = '.$memoryDiff.' / '.$accumulateMemory);
}
include_once("includes/libeclass40.php");
if ($debugMemory && $UserLogin==$debugUserLogin) {
	$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	$accumulateMemory = convert_size(memory_get_usage(true));
	debug_pr('After libeclass40 = '.$memoryDiff.' / '.$accumulateMemory);
}
// include_once("includes/libfilesystem.php");

intranet_opendb();

$remote_ip_address = getRemoteIpAddress();

########################################################################
### SSO ################################################################
########################################################################
//include_once("includes/libuser.php");
if(isset($target_e) && $target_e != ''){
	DecryptModuleFilePars($target_e);	// extract ssoUserLogin, ssoAuthType, ssoSrvType,
	if($ssoAuthType == 'SSOLOGIN'){
		$UserLogin = $ssoUserLogin;

		$lu = new libuser();
		$userInfo = $lu->returnUser("", $UserLogin);
		if (count($userInfo) > 0) {
			if(trim($userInfo[0]['UserPassword']) == '') {
				if($intranet_authentication_method=="HASH"){
					//				include_once("includes/libauth.php");
					$lauth = new libauth();
					$hasedPw = $lauth->GetUserDecryptedPassword($userInfo[0]['UserID']);
					$UserPassword = $hasedPw;
					if($ssoservice["Google"]["Valid"]==true){
					    $sql = "SELECT HashedPass FROM INTRANET_USER WHERE UserLogin='".$UserLogin."' AND RecordStatus=1";
					    $_SESSION['tmp_UserPassword_Hash'] = current($lauth->returnVector($sql));
					}
				}
			}
		}
	}
}

########################################################################
### SSO ################################################################
########################################################################

$is_customized_login = is_file($intranet_root.'/templates/login.customized.php') || strpos($_SERVER['HTTP_REFERER'],'login.customized.php') || !strpos($_SERVER['HTTP_REFERER'], $_SERVER['SERVER_NAME']); // either customized page or login from school page
if($sys_custom['LoginForceCsrfTokenChecking']){
	$is_customized_login = false;
}

// [2020-0805-0918-34235] no need to check secure token if redirect from sso_intranet.php
if(isset($_SESSION['tmp_appSSO_skipSecureTokenCheck']) && $_SESSION['tmp_appSSO_skipSecureTokenCheck'] && $OnlyModule != "") {
    session_unregister_intranet('tmp_appSSO_skipSecureTokenCheck');
    $is_customized_login = true;
}

if($sys_custom['non_eclass_PaGamO']){
	$is_customized_login = true;
}
if($sys_custom['project']['centennialcollege'] && $is_customized_login != true){
	include_once("cc_eap/settings.php");
	if($sys_custom['centennialcollege']['dev']['on']){
		$is_customized_login = true;
	}
}
if(
		# checking ssoAuthType (from target_e) could support more SSO service in the future
		# for Google SSO, it could check     !$_SESSION['SignInWithGoogle']    directly
		$ssoAuthType != 'SSOLOGIN' &&
		(
				$is_customized_login===false ||
				($is_customized_login!==false && isset($_REQUEST['securetoken']))
				)
		)
{
	include_once("includes/SecureToken.php");
	$SecureToken = new SecureToken();
	if(!$SecureToken->CheckToken($_REQUEST['securetoken'])){
		header("Location: /");
		exit();
	}
}

$li = new libauth();
//$UserLogin = htmlspecialchars(trim($UserLogin));
//$UserPassword = htmlspecialchars(trim($UserPassword));

// if use LDAP, perform action to strip any @domain from UserLogin
if ((isset($UseLDAP) && $UseLDAP) || $intranet_authentication_method == 'LDAP') {
	$ldap_at_pos = strpos($UserLogin,'@');
	if($ldap_at_pos > 0)
	{
		$UserLogin = substr($UserLogin,0, $ldap_at_pos);
	}
}

if (isset($_SESSION['tmp_From_eClassApp']) && $_SESSION['tmp_From_eClassApp']) {
    $tmpFromeClassAppSSO = true;
    $skipCheckingStrongPassword = true;

	// app SSO logic from /intranetIP/api/eClassApp/sso_intranet.php
	include_once("includes/libeclassapiauth.php");
	$lapiAuth = new libeclassapiauth();
    $project_name = $lapiAuth->GetProjectByApiKey($_SESSION['tmp_APIKey']);
	if ($_SESSION['tmp_SchoolCode'] == $config_school_code && ($project_name == 'eClass App' || $project_name == 'eClass App (Teacher)' || $project_name == 'eClass App (Student)')) {
		// request from same school code and valid API Key => from trusted app => get userlogin and password from session
		$UserLogin = $_SESSION['tmp_UserLogin'];

		if ($intranet_authentication_method == "HASH")
		{
			$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '".$UserLogin."' AND HashedPass = '".$_SESSION['tmp_UserPassword']."'";
			$row = $li->returnVector($sql);
			$tmp_user_id = $row[0];

			if ($tmp_user_id > 0 && $tmp_user_id != "")
			{
				$_SESSION['tmp_UserPassword_Hash'] = $_SESSION['tmp_UserPassword'];
				$_SESSION['tmp_UserPassword'] = $li->GetUserDecryptedPassword($tmp_user_id);
			}
		}
        // [2020-0203-1544-25105] Handle eClassApp webview access for client using LDAP
        else if ($intranet_authentication_method == "LDAP")
        {
            // Get users using user login
            $sql = "SELECT UserID, HashedPass FROM INTRANET_USER WHERE UserLogin = '".htmlspecialchars($UserLogin)."' ";
            $row = $li->returnArray($sql);
            if(!empty($row))
            {
                foreach((array)$row as $_row)
                {
                    $tmp_user_id = $_row['UserID'];
                    if ($tmp_user_id > 0 && $tmp_user_id != "")
                    {
                        // [2020-0221-1443-40207] checking for empty hashed pass
                        $emptyPassAllowed = false;
                        if($_row['HashedPass'] == '' && $_SESSION['tmp_UserPassword'] != '') {
                            include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
                            $leClassApp = new libeClassApp();

                            $userPass = $leClassApp->getUserPassInfo($tmp_user_id, $UserLogin);
                            $emptyPassAllowed = $userPass == $_SESSION['tmp_UserPassword'];
                        }

                        /*
                         *  Update $_SESSION
                         *  1. if match hashed pass     [2020-0203-1544-25105]
                         *  2. if hashed pass is empty  [2020-0221-1443-40207]
                         */
                        // if($_row['HashedPass'] == $_SESSION['tmp_UserPassword'])
                        if(($_row['HashedPass'] == $_SESSION['tmp_UserPassword']) || $emptyPassAllowed)
                        {
                            $_SESSION['tmp_UserPassword_Hash'] = $_SESSION['tmp_UserPassword'];
                            $_SESSION['tmp_UserPassword'] = $li->GetUserDecryptedPassword($tmp_user_id);
                        }
                    }
                }
            }
        }
		$UserPassword = $_SESSION['tmp_UserPassword'];
	}

	session_register_intranet("eClassAppSSO_standaloneModule", 1);
	if ($OnlyModule == 'iCalendar') {
		session_register_intranet("eClassAppSSO_standaloneModule_iCalendar", 1);
	}

	$_SESSION['tmp_UserLogin'] = "";
	unset($_SESSION['tmp_UserLogin']);
	$_SESSION['tmp_UserPassword'] = "";
	unset($_SESSION['tmp_UserPassword']);
	$_SESSION['tmp_From_eClassApp'] = "";
	unset($_SESSION['tmp_From_eClassApp']);
	$_SESSION['tmp_APIKey'] = "";
	unset($_SESSION['tmp_APIKey']);
	$_SESSION['tmp_SchoolCode'] = "";
	unset($_SESSION['tmp_SchoolCode']);
}
else {
	// original SSO logic
	// for silent logon since 2011-05-18
	if (($sys_custom['SingleSignOnIP25eBookingFrom'] || (is_array($sys_custom['SingleSignOnIP25PL']) && sizeof($sys_custom['SingleSignOnIP25PL'])>0) || $sys_custom['project']['HKPF']) && $OnlyModule!="" && $UserLogin=="" && $UserPassword=="" && $_SESSION['tmp_UserLogin']!="" && $_SESSION['tmp_UserPassword']!="")
	{
		$skipCheckingStrongPassword = true;

		# get login info from SESSION
		$UserLogin = $_SESSION['tmp_UserLogin'];

		if ($intranet_authentication_method=="HASH")
		{
			$sql = "SELECT UserID FROM INTRANET_USER Where UserLogin='".$UserLogin."' AND HashedPass='".$_SESSION['tmp_UserPassword']."'";
			$row = $li->returnVector($sql);
			$tmp_user_id = $row[0];

			if ($tmp_user_id>0 && $tmp_user_id!="")
			{
				$_SESSION['tmp_UserPassword_Hash'] = $_SESSION['tmp_UserPassword'];
				$_SESSION['tmp_UserPassword'] = $li->GetUserDecryptedPassword($tmp_user_id);
			}
		}
		$UserPassword = $_SESSION['tmp_UserPassword'];

		# reset
		$_SESSION['tmp_UserLogin'] = "";
		unset($_SESSION['tmp_UserLogin']);
		$_SESSION['tmp_UserPassword'] = "";
		unset($_SESSION['tmp_UserPassword']);
	}
}

if($sys_custom['project']['centennialcollege'] && $source=='cc_eap'){
	$skipCheckingStrongPassword = true;
	if(isset($cc_source)&&$cc_source=='CAS'){
		$UserPassword = '';
		$_SESSION['tmp_UserPassword_Hash'] = md5($UserLogin.$intranet_password_salt);
	}else{
		$email = $UserLogin;
		$sql = "SELECT UserLogin,EnglishName FROM INTRANET_USER WHERE UserEmail='".$email."'";
		$UserIDList = $li->returnArray($sql);
		$UserLogin = $UserIDList[0]['UserLogin'];
		$UserName = $UserIDList[0]['EnglishName'];
		$_SESSION['tmp_UserPassword_Hash'] = md5($UserLogin.$UserPassword.$intranet_password_salt);
	}
	if($is_from_cc_eap_app == true){
		$_SESSION['ck_is_from_cc_eap_app'] = true;
	}
}

$UserLogin = htmlspecialchars($UserLogin);
$UserPassword = htmlspecialchars($UserPassword);

#Checking New user for redirect to the page alert changing email
$sql = "Select RecordType, LastUsed From INTRANET_USER Where Userlogin = '".$UserLogin."' ";
$CheckNewUser = $li->returnArray($sql, 2);
if (!empty($CheckNewUser)){
	if($CheckNewUser[0]['LastUsed'] == null){
		$sql = "Select SettingName, SettingValue From GENERAL_SETTING Where Module='UserInfoSettings' AND SettingName='ChangeEmailAlert_".$CheckNewUser[0]['RecordType']."' ";
		$CheckSettings =  $li->returnArray($sql);
		if ( $CheckSettings[0]['SettingValue'] == 1 ){
			$NewUserUrl = "home/iaccount/account/contact_info.php?user=newuser";
		}
	}
}








if (strtolower($UserLogin)!='broadlearning' && ((isset($UseLDAP) && $UseLDAP) || $intranet_authentication_method == 'LDAP'))
{
	$skipCheckingStrongPassword = true;
	# Different users using different LDAP dn string
	if ($ldap_user_type_mode)
	{
		# Get User Type
		$sql = "SELECT UserID, RecordType FROM INTRANET_USER WHERE UserLogin = '$UserLogin'";
		$temp = $li->returnArray($sql,2);

		list($t_id, $t_user_type) = $temp[0];

		if (!is_array($temp) || sizeof($temp)==0 || $t_user_type == "" || $t_user_type < 1 || $t_user_type > 4)
		{
			$url = ($url=="") ? "/" : $url;
			$url = str_replace(array('http://','https://',$_SERVER['HTTP_HOST']),array('','',''),$url);
			if($url[0] != '/') $url = '/'.$url;
			header("Location: $url");
			exit();
		}

		switch ($t_user_type)
		{
			case 1: $special_ldap_dn = $ldap_teacher_base_dn; break;
			case 2: $special_ldap_dn = $ldap_student_base_dn; break;
			case 3: $special_ldap_dn = $ldap_parent_base_dn; break;
			default: $special_ldap_dn = $ldap_teacher_base_dn;
		}
	}

	$lldap = new libldap();
	// find all ldap servers with nslookup
	if(isset($ldap_tcp_dns) && $ldap_tcp_dns != ''){
		$tmp_ldap_hosts = $lldap->getLdapHosts($ldap_tcp_dns);
	}else{
		$tmp_ldap_hosts = array(array($ldap_host, $ldap_port));
	}
	$is_ldap_connected = false;
	// connect to one of the ldap servers that is available
	for($i=0;$i<count($tmp_ldap_hosts);$i++){
		$lldap->changeToHost($tmp_ldap_hosts[$i][0], $tmp_ldap_hosts[$i][1]);
		$is_ldap_connected = $lldap->connect();
		if($is_ldap_connected) break;
	}

	if ($is_ldap_connected && (($lldap->validate_try_all_ou($UserLogin,$UserPassword) && $lldap->validate($UserLogin,$UserPassword)) || $lldap->validateDomainUser($UserLogin,$UserPassword)))
	{
		$sql = "SELECT UserID, UserEmail, ImapUserEmail FROM INTRANET_USER WHERE UserLogin = '$UserLogin' and RecordStatus=1";
		$result = $li->returnArray($sql,3);
		$ID = $result[0][0]+0;
		if ($ID!=0)
		{
			//$sql = "UPDATE INTRANET_USER SET LastUsed = now(), UserPassword = '$UserPassword' WHERE UserID = '$ID' ";
			//# 2010-07-01 [Yuen]
			//$session_key = $li->generateSessionKey();
			//$sql = "UPDATE INTRANET_USER SET LastUsed = now(), SessionKey = '$session_key', SessionLastUpdated = now()  WHERE UserID = '$ID' ";
			//$li->db_db_query($sql);

			$sql = "UPDATE INTRANET_USER SET LastUsed = now() WHERE UserID = '$ID'";
			$li->db_db_query($sql);

			$results = $li->getSessionKeyLastUpdatedFromUserId($ID);
			$session_key = trim($results["SessionKey"]);
			if ($session_key=="")
			{
				$session_key = $li->generateSessionKey();
				$li->updateSessionByUserId($session_key, $ID);
			}
			$li->updateSessionLastUpdatedByUserId($ID);

			//  $leclass = new libeclass();
			//   $UserEmail = $result[0][1];
			//  $leclass->eClassUserUpdatePassword($UserEmail,$UserPassword);

			/*
			 # Syn mail server
			 if ($webmail_not_config_to_ldap)
			 {
			 include_once("includes/libwebmail.php");
			 $lwebmail = new libwebmail();
			 $lwebmail->change_password($UserLogin, $UserPassword);
			 }
			 */

			# Syn mail server
			include_once("includes/libwebmail.php");
			include_once("includes/imap_gamma.php");

			if($plugin['imail_gamma']){
				/*
				 $IMap = new imap_gamma(1);
				 $imap_user_email = trim($result[0][2]);
				 if($imap_user_email != '' && $IMap->is_user_exist($imap_user_email)){
				 $IMap->change_password($imap_user_email, $UserPassword);
				 }
				 */
			}else{
				$lwebmail = new libwebmail();
				if ($lwebmail->has_webmail)
				{
					$lwebmail->change_password($UserLogin, $UserPassword);
				}
			}
			/*
			 # Syn FTP Server
			 include_once("includes/libftp.php");
			 if ($plugin['personalfile'])
			 {
			 $lftp = new libftp();
			 if ($lftp->isFTP)
			 {
			 $lftp->changePassword($UserLogin, $UserPassword);
			 }
			 }
			 */
			# AeroDrive login variables
			$tmp_username = $UserLogin;
			session_register_intranet("tmp_username",$UserLogin);
			$tmp_password = $UserPassword;
			session_register_intranet("tmp_password",$UserPassword);

			$url = '/';
		}
	}else{
		$ID = 0;
	}
}
else if (strstr($target_url, "PowerLessonFromApp") && (isset($PL_cid) && $PL_cid!='') && (isset($PL_lid) && $PL_lid!='') && (isset($PL_lsid) && $PL_lsid!='') && (isset($PL_ucid) && $PL_ucid!='')) # PowerLesson App QR Code Login
{
	$skipCheckingStrongPassword = true;
	$ID = $li->validate_PowerLessonAppQRCodeLogin($PL_cid, $PL_lid, $PL_lsid, $PL_ucid);

	if($ID){
	    $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID = '".IntegerSafe($ID)."'";
		$UserLogin = current($li->returnVector($sql));
		$UserPassword = $li->GetUserDecryptedPassword($ID);
	}
	else{
		header("Location: appqrcodeloginfail://");
		die();
	}
}else    # Normal authentication
{

	// Using HKEdCity account to login
	/*
	* 1. $ssoservice["HKEdCity"]["Valid"] = true
	* 2. $HKEdCity - Set
	*/
	if($ssoservice["HKEdCity"]["Valid"] && $_GET["HKEdCity"]!=""){

		include_once("includes/libhkedcity.php");
		$lhkedcity = new libhkedcity();

		# Get passed data from $_GET
		$HKEdCityData = getDecryptedText($_GET["HKEdCity"], $lhkedcity->getHKEdCityKey());
		list($code1, $code2, $code3, $receive_userid, $receive_action, $storedUserIP) = explode("||",$HKEdCityData);

		# Verification
		// Gen code for Verification
		$curTimestamp = time();
		$prekey = $lhkedcity->getHKEdCityKey("login");
		$curHourCode = $prekey.'_'.date('YmdH', $curTimestamp);
		$curHourCode = getEncryptedText(md5($curHourCode), $prekey);

		// Code, IP and Action checking for Security
		if(($curHourCode!=$code1 && $curHourCode!=$code2 && $curHourCode!=$code3) || $storedUserIP!=$remote_ip_address || $receive_action!="login"){
			header("Location: /");
			exit();
		}

		// Check if HKEdCityID valid - get UserID and UserLogin
		$HKEdCityUser = new libuser();
		list($ID, $UserLogin) = $HKEdCityUser->Get_User_By_HKEdCityUserLogin($receive_userid);
		// return empty UserID or UserLogin, redirect to homepage
		if($ID=="" || $UserLogin==""){
			header("Location: /templates/?err=5");
			exit();
		}

		// get UserPassword
		include_once("includes/libpwm.php");
		$libpwn_obj = new libpwm();
		$pwd = $libpwn_obj->getData($ID);
		$UserPassword = $pwd[$ID];
		// return empty UserPassword, redirect to homepage
		if($UserPassword==""){
			header("Location: /");
			exit();
		}
	}
	// ADFS SSO Login
	else if ($ssoservice["ADFS"]["Valid"] && $_POST['adfsUserLogin']) {
	    $adfsUserLogin = trim($_POST['adfsUserLogin']);

	    //simplesamlphp
	    #### production [start] ####
//         $as = new SimpleSAML_Auth_Simple('eclass-sp');
//         $as->requireAuth();
//         $attributes = $as->getAttributes();

//         $upn = $attributes['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn'][0];
//         $array_upn = explode('@',$upn);
//         $username = $array_upn[0];
	    #### production [end] ####

	    #### dev [start] ####
// 	    $username = 'kis_k1t';
	    #### dev [end] ####

	    $username = 'kis_k1t';

	    if ($adfsUserLogin == $username) {
	        $_SESSION['username'] = $username;

	        $UserLogin = $username;

	        $sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '$UserLogin'";
	        $tmpUserId = current($li->returnVector($sql));

	        if ($tmpUserId > 0) {
	            $ID = $tmpUserId;
	        }
	    }



// 	    $_SESSION['return_path'] = $_SERVER["HTTP_REFERER"];

// 	    //SimpleSAMLphp
// 	    require_once './sso/simplesamlphp/lib/_autoload.php';
// 	    $as = new SimpleSAML_Auth_Simple('eclass-sp');
// 	    $as->requireAuth();
// 	    $attributes = $as->getAttributes();

// 	    print_r($attributes);

// 	    $upn = $attributes['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn'][0];
// 	    $array_upn = explode('@',$upn);
// 	    $username = $array_upn[0];
// 	    $_SESSION['username'] = $username;

// 	    header('Location: '.$_SESSION['return_path']);




// 		$server_script='https://104.214.141.165:886/login.adfs_server.php';

// 		$current=time();
// 		$context  = stream_context_create(
// 			array('https' =>
//     			    array(
//         			        'method'  => 'POST',
//     			        'header'  => 'Content-type: application/x-www-form-urlencoded',
//     			        'content' => http_build_query(
//         					    array(
//             					        'a' => $current,
//         					        'b' => 'get'
//         					    )
//     					)
// 			    )
// 			)
// 		);
// 		$result = file_get_contents($server_script, false, $context);

// 		if($result!=''){
// 			$current_11 = $current * 11;
// 			$result_decoded = base64_decode($result);
// 			$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
// 			$iv = substr($result_decoded, 0, $iv_size);
// 			$data_encrypted = substr($result_decoded, $iv_size);
// 			$data = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128,$current_11,$data_encrypted, MCRYPT_MODE_CBC,$iv));

// 			$UserLogin = $data;

// 			$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '$UserLogin'";
// 			$ID = current($li->returnVector($sql));

// 			// get UserPassword
// 			include_once("includes/libpwm.php");
// 			$libpwn_obj = new libpwm();
// 			$pwd = $libpwn_obj->getData($ID);
// 			$UserPassword = $pwd[$ID];
// 			// return empty UserPassword, redirect to homepage
// 			if($UserPassword==""){
// 				header("Location: /");
// 				exit();
// 			}
// 		}else{
// 			header("Location: ".$server_script);
// 		}
    }
	// Normal Situation
	else{
		$ID = $li->validate($UserLogin, $UserPassword, $_SESSION['tmp_UserPassword_Hash']);
    }

	# mapping to demo accounts (s & t)

	if ($sys_custom['demo_site_account_mapping'] && $ID>0)
	{
		$Map2DemoAccount = false;
		if (strlen($UserLogin)==strpos($UserLogin, "_t")+2)
		{
			$Map2DemoAccount = true;
			$demo_account_login = "t";
		} elseif (strlen($UserLogin)==strpos($UserLogin, "_s")+2)
		{
			$Map2DemoAccount = true;
			$demo_account_login = "s";
		} elseif (strlen($UserLogin)==strpos($UserLogin, "_p")+2)
		{
			$Map2DemoAccount = true;
			$demo_account_login = "p";
		}


		if ($Map2DemoAccount)
		{
			$sql_demo = "SELECT UserID FROM INTRANET_USER where UserLogin='".$demo_account_login."'";
			$demo_account = $li->returnVector($sql_demo);
			$ID = trim($demo_account[0]);

			# get password by decryption
			if ($ID!="" && $ID>0)
			{
				# change to use demo account
				$UserLogin = $demo_account_login;
				$UserPassword = $li->GetUserDecryptedPassword($ID);
			}
		}
	}
}
if ($debugMemory && $UserLogin==$debugUserLogin) {
	$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	$accumulateMemory = convert_size(memory_get_usage(true));
	debug_pr('After auth pwd = '.$memoryDiff.' / '.$accumulateMemory);
}

if($ID==0){

	# record the failure attempt (IP, datetime, loginid, password)
	$li->HandleFailure($UserLogin, getEncryptedText($UserPassword), $session_key, $remote_ip_address);

	if ($li->IsConsecutiveUnsuccessfulLogins($UserLogin, $remote_ip_address))
	{
		// echo "As you have already attempted logins but failed for too many times, please try again later (i.e. ".$li->LoginLockDuration." mins).";
		intranet_closedb();

// 		header("Location error_page.php?errno=11&duration=" . $li->LoginLockDuration);
		header("Location:error_page.php?errno=11&duration=" . $li->LoginLockDuration);
		die();
	}

	intranet_closedb();

	if (isset($_SESSION["ECLASS_SSO"]["client_credentials"]))
	{
	    $url = '/sso/eclass/?task=dsilogin&err=1';
	    header("Location: $url");
	    exit;
	}

	$url = ($url=="") ? "/" : $url;

	if (isset($target_url) && !empty($target_url)){
		$target_url = str_replace(array('http://','https://',$_SERVER['HTTP_HOST']),array('','',''),$target_url);
		if($target_url[0] != '/') $target_url = '/'.$target_url;
		if(isset($sys_custom['target_url_for_login'])) $target_url = $sys_custom['target_url_for_login'];
		header("Location: $target_url");
	}
	else{
		$url = str_replace(array('http://','https://',$_SERVER['HTTP_HOST']),array('','',''),$url);
		if($url[0] != '/') $url = '/'.$url;
		header("Location: $url");
	}
	exit;
} else
{
	if ($DirectLink!="")
	{
		$target_url = $DirectLink;
	}
	if ($li->IsConsecutiveUnsuccessfulLogins($UserLogin, $remote_ip_address))
	{
		// echo "As you have already attempted logins but failed for too many times, please try again later (i.e. ".$li->LoginLockDuration." mins).";
		intranet_closedb();
// 		header("Location error_page.php?errno=11&duration=" . $li->LoginLockDuration);

		header("Location:error_page.php?errno=11&duration=" . $li->LoginLockDuration);
		die();
	}

	//session_register("UserID");
	//session_register("eclass_session_password");
	//$UserID = $ID;
	session_register_intranet('UserID', $ID);
	session_register_intranet('LOGIN_INTRANET_SESSION_USERID', $ID);
	$_SESSION['eclass_session_password'] = $UserPassword;

	if ((strstr($UserLogin,"kis_") && $DebugMode) || $plugin["platform"]=="KIS")
	{
		$skipCheckingStrongPassword = true;
		$_SESSION["platform"] = ($plugin["platform"]!="") ? $plugin["platform"] : "KIS";
		$_SESSION["platform_version"] = ($plugin["platform_version"]!="") ? $plugin["platform_version"] : "1.0";
	} else
	{
		$_SESSION["platform"] = "IP";
		$_SESSION["platform_version"] = "2.5";
	}

	# Store eClassURL
	//session_register("elcass_server_url");
	//$elcass_server_url = $_SERVER['HTTP_HOST'];
	session_register_intranet('elcass_server_url', $_SERVER['HTTP_HOST']);


	$ss_intranet_plugin['power_speech'] 		= $plugin['power_speech'];
	$ss_intranet_plugin['power_speech_expire'] 	= $plugin['power_speech_expire'];

	$ss_intranet_plugin['elib_power_speech'] 		= $plugin['elib_power_speech'];
	$ss_intranet_plugin['elib_power_speech_expire'] 	= $plugin['elib_power_speech_expire'];

	## Power speech sessions
	// commented by Yuen - session_register("plugin");
	//session_register("tts_server_url");
	//session_register("tts_user");
	$_SESSION['tts_server_url'] = $tts_server_url;
	$_SESSION['tts_user'] = $tts_user;
	$_SESSION['config_school_code'] = $config_school_code;

	# 190 Project - Wong Wah San eLearning Project
	if(isset($plugin['WWS_eLearningProject']) && $plugin['WWS_eLearningProject'] == true)
	{
		$_SESSION['ck_wws_elearning_project'] = $plugin['WWS_eLearningProject'];
	}

	# PowerLesson
	if(isset($plugin['power_lesson']) && $plugin['power_lesson'] == true)
	{
		$_SESSION['ck_power_lesson'] = $plugin['power_lesson'];
		$_SESSION['ck_from_pl'] 	 = $from_pl;
	}

	if(isset($plugin['power_lesson_media_conversion_type']) && is_array($plugin['power_lesson_media_conversion_type']))
	{
		$_SESSION['ck_power_lesson_media_conversion_type'] = $plugin['power_lesson_media_conversion_type'];
	}

	# PowerFlip
	if((isset($plugin['PowerFlip']) && $plugin['PowerFlip'] == true)||(isset($plugin['PowerFlip_lite']) && $plugin['PowerFlip_lite'] == true))
	{
		$_SESSION['ck_power_flip'] = ($plugin['PowerFlip'] || $plugin['PowerFlip_lite']);
		$_SESSION['ck_from_pf'] = $from_pf;
	}

	# PowerBoard
	if(isset($plugin['power_board']) && $plugin['power_board'] == true)
	{
		$_SESSION['ck_power_board'] = $plugin['power_board'];
	}

	# quick login for PowerLesson App
	if (strstr($target_url, "PowerLessonFromApp"))
	{
		//session_register("ss_intranet_plugin");
		$_SESSION['ss_intranet_plugin'] = $ss_intranet_plugin;

		# prepare eClass login info
		$lu = new libuser($UserID);

		# check if session key from DB is new (i.e. updated within 15mins)
		$results = $lu->getSessionKeyLastUpdatedFromUserId($UserID);
		$session_key = trim($results["SessionKey"]);
		if ($session_key=="")
		{
			$session_key = $lu->generateSessionKey();
			$lu->updateSession($session_key);
		}

		## remove the '/' at the end of the $eclass_url_root as powerlesson needs to retrieve the action frim url path
		if($eclass_url_root!="" && $eclass_url_root{strlen($eclass_url_root)-1} == "/"){
			$eclass_url_root = substr($eclass_url_root, 0, strlen($eclass_url_root)-1);
		}

		if($eclass_httppath_first!="" && $eclass_httppath_first{strlen($eclass_httppath_first)-1} == "/"){
			$eclass_httppath_first = substr($eclass_httppath_first, 0, strlen($eclass_httppath_first)-1);
		}

		//DEFAULT THE URL IS eclass40
		$redirect_url_root = $eclass_url_root;
		$redirect_httppath_first = $eclass_httppath_first;

		$url = ($eclass_httppath_first == "") ? $redirect_url_root : $eclass_httppath_first;
		$url = str_replace("/home/eLearning/login.php", $url."/checkpl.php", str_replace("uc_id=", "user_course_id=", $target_url)) . "&eclasskey=".$session_key."&AppVersion=".$AppVersion;

		//session_register("eClassMode");
		//$eClassMode = "I";
		header("Location: $url");
		die();
	}

	# For non-iPad Login use for PowerLesson only
	if(isset($plugin['power_lesson_expiry_date']))
	{
		$_SESSION['ck_power_lesson_expiry_date'] = $plugin['power_lesson_expiry_date'];
	}

	// Load User Right and Targeting Permission
	$UserRightTarget = new user_right_target();
	$_SESSION['SSV_USER_ACCESS'] = $UserRightTarget->Load_User_Right($UserID);
	$_SESSION['SSV_USER_TARGET'] = $UserRightTarget->Load_User_Target($UserID);
	$lu = new libuser($UserID);

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After user_right_target = '.$memoryDiff.' / '.$accumulateMemory);
	}

	if($lu->isTeacherStaff() && $plugin['iPortfolio']){
		$iPFCourseID = getEClassRoomID($iPFRoomType = 4);
		$objEclass = new libeclass($iPFCourseID);
		if($_SESSION['SSV_USER_ACCESS']['other-iPortfolio']  || $_SESSION["platform"]=="KIS") {
			//If the teacher is now has iPortfolio Admin right,
			//check if EClass acc exists and Admin Right functionable
			//If no EClass acc exists, create one
			//If Admin Right not functionable, add a record to table group_function and make it functionable
			$ipfEclassAccountExistInEclass = false;
			$aryUserInfo = 	$objEclass->returnEClass40User();
			$i_max = count($aryUserInfo);
			if($i_max > 0){
				for($i = 0, $i_max = count($aryUserInfo) ;$i < $i_max; $i++){
					$_user_email = $aryUserInfo[$i]['user_email'];

					if(trim($_user_email) == $lu->UserEmail){
						$ipfEclassAccountExistInEclass = true;
						break;
					}
				}
			}
			if($ipfEclassAccountExistInEclass==false){
				$objEclass->eClassUserAddFullInfoByUserID($UserID,$_isTeacherType = 'T');
			}
			$objEclass->setUserAdminRightforIPF($lu->UserEmail);
		}else{
			//If the teacher is now has no ipf Admin Right,
			//double check and remove the admin right
			$objEclass->checknRemoveAdminRightforIPF($lu->UserEmail);
		}
	}

	if($_SESSION["platform"]=="KIS"){
		$sql = "Select course_id From {$eclass_db}.course Where RoomType = 7 and course_code = 'kisworksheets'";
		$worksheetsCourseID = current($li->returnVector($sql));
		if($worksheetsCourseID !=''){
			$plugin['iTextbook'] = true;
			$objEclass = new libeclass($worksheetsCourseID);

			$wsEclassAccountExistInEclass = false;
			$aryUserInfo = 	$objEclass->returnEClass40User();
			$i_max = count($aryUserInfo);
			if($i_max > 0){
				for($i = 0, $i_max = count($aryUserInfo) ;$i < $i_max; $i++){
					$_user_email = $aryUserInfo[$i]['user_email'];

					if(trim($_user_email) == $lu->UserEmail){
						$wsEclassAccountExistInEclass = true;
						break;
					}
				}
			}
			if($wsEclassAccountExistInEclass==false){
				$_user_email = $lu->UserEmail;
				if($lu->isTeacherStaff()){
					$objEclass->eClassUserAddFullInfoByUserID($UserID,$_isTeacherType = 'T');
				}else{
					$objEclass->eClassUserAddFullInfoByUserID($UserID,$_isStudentType = 'S');
				}
			}

			$sql = "Select 
						uc.user_course_id, 
						t.BookID
					From 
						{$eclass_db}.user_course as uc
					left join 
						ITEXTBOOK_BOOK as t
					on 
						BookType = 'kisworksheets'
					Where 
						uc.course_id = '$worksheetsCourseID' and uc.user_email = '$_user_email'";

			$worksheetsAry = current($li->returnArray($sql));
			$_SESSION['KIS_Worksheets']['User_Course_ID'] = $worksheetsAry['user_course_id'];
			$_SESSION['KIS_Worksheets']['BookID'] = $worksheetsAry['BookID'];
		}
	}

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After iPf classroom = '.$memoryDiff.' / '.$accumulateMemory);
	}



	# Store two-way hashed password
	$li->UpdateEncryptedPassword($UserID, $UserPassword);

	# Insert Login Record
	$sql = "INSERT INTO INTRANET_LOGIN_SESSION (UserID,ClientHost,StartTime,DateModified) VALUES ('$UserID', '".$remote_ip_address."', now(), now())";
	$log_success = $li->db_db_query($sql);
	//session_register("iSession_LoginSessionID");
	//$iSession_LoginSessionID = $li->db_insert_id();
	session_register_intranet('iSession_LoginSessionID', $li->db_insert_id());

	//     $lu = new libuser($UserID);

	//     session_register("UserType");
	//     session_register("isTeaching");
	//     session_register("LastLang");
	//     $UserType = $lu->RecordType;
	//     $isTeaching = $lu->teaching;
	//	   $LastLang = $lu->LastLang;
	session_register_intranet('UserType', $lu->RecordType);
	session_register_intranet('isTeaching', $lu->teaching);
	session_register_intranet('LastLang', $lu->LastLang);
	$_SESSION['EnglishName'] = $lu->EnglishName;
	$_SESSION['ChineseName'] = $lu->ChineseName;

	if ($tmpFromeClassAppSSO || $skipCheckingStrongPassword) {
	    // skip strong password checking for eClass App SSO pages
	}
	else {
	    if($UserType>=1 && $UserType<=3)
	    {
	        # hide SessionID for security reason
	        //$url = "home/index.php?".session_name()."=".session_id();
	        $url = "home/index.php";
	        // Force to change password if it is the same as UserLogin

	        /* if ($lu->RetrieveUserInfoSetting("CanUpdate", "Password") && ($lu->RetrieveUserInfoSetting("Enable", "PasswordPolicy") || ($UserLogin!="" && $UserPassword!="" && $UserLogin==$UserPassword) ))
	         {
	         if ($lu->CheckPasswordSafe()<1 || ($UserLogin!="" && $UserPassword!="" && $UserLogin==$UserPassword))
	         {
	         $url = "home/iaccount/account/login_password.php?FromLogin=1";
	         }
	         }
	         */
	        $PasswordSettings = $lu->RetrieveUserInfoSettingInArrary(array('EnablePasswordPolicy_'.$lu->RecordType));
	        $PasswordLength = $PasswordSettings['EnablePasswordPolicy_'.$lu->RecordType];
	        if($PasswordLength<6) $PasswordLength = 6;

	        if($sys_custom['UseStrongPassword']){
	            $checkPasswordCriteriaResult = $li->CheckPasswordCriteria($UserPassword,$lu->UserLogin,$PasswordLength);
	        }

	        if (!$sys_custom['project']['HKPF']) {     // HKPF bypass the rule
    	        if (($UserLogin!="" && $UserPassword!="" && $UserLogin==$UserPassword) || $lu->CheckPasswordSafe()<1 || ($sys_custom['UseStrongPassword'] && !in_array(1,$checkPasswordCriteriaResult)) )
    	        {
    	            if($sys_custom['UseStrongPassword'] && !in_array(1,$checkPasswordCriteriaResult)){
    	                $_SESSION['FORCE_CHANGE_PASSWORD'] = 1;
    	            }
    	            $url = "home/iaccount/account/login_password.php?FromLogin=1";
    	        }
	        }
	    }
	}


	if($UserType==4 && $special_feature['alumni'])
	{
		include("./plugins/alumni_conf.php");
		//session_register("AlumniURL");
		$url = "home/eCommunity/group/index.php?GroupID=$alumni_GroupID";
		//$AlumniURL = $url;
		session_register_intranet('AlumniURL', $url);
	}

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After alumni = '.$memoryDiff.' / '.$accumulateMemory);
	}

	#### Check force default lang (with flag checking)
	if(isset($sys_custom['default_lang_cust'][$UserType]))
	{
		if($sys_custom['default_lang_cust'][$UserType]=="default")
			$LastLang = $_SESSION['intranet_default_lang'];
			else
				$LastLang = $sys_custom['default_lang_cust'][$UserType];
	}

	# for imail gamma
	if($plugin['imail_gamma'])
	{
		include_once("includes/imap_gamma.php");
		$IMap = new imap_gamma($skipLogin = 1);
		if($IMap->CheckIdentityPermission($UserType))
		{
			$_SESSION['SSV_EMAIL_LOGIN'] = $lu->ImapUserEmail;
			$_SESSION['SSV_EMAIL_PASSWORD'] = $UserPassword;
			$_SESSION['SSV_LOGIN_EMAIL'] = $lu->ImapUserEmail;
		}
	}

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After imap_gamma = '.$memoryDiff.' / '.$accumulateMemory);
	}

	/* temp remark
	 if ($stand_alone['SmartCardAttendance'])
	 {
	 $url = "home/profile/smartcard/";
	 header("Location: $url");
	 exit();
	 }
	 */


	if ($stand_alone['iPortfolio'])
	{
		$_SESSION['SSV_isiPortfolio_standalone'] = true;
		$url = ($iportfolio_version>=2.5) ? "home/portfolio/" : "home/eclass/index_portfolio.php";
		//header("Location: $url");
		//exit();
	}

	# special redirect for publisher user account
	if (is_array($special_feature['eBookUserAccountLimit'][$UserID]) || is_array($special_feature['eBookAuthorAccountLimit'][$UserID]))
	{
		$url = "/home/eLearning/elibrary/";
	}

	# special redirect for emag user account - 20141105-emag-provider
	if ($special_feature['emagUserAccountLimit'][$UserID])
	{
		$url = "/home/eLearning/elibplus/";
	}

	# special redirect for itextbook-gv/rs user account
	if (isset($special_feature['iTextbookUserAccountLimit']) && in_array($UserID,$special_feature['iTextbookUserAccountLimit']) && $HTTP_SERVER_VARS["HTTP_HOST"] == "show-s.eclasscloud.hk")
	{
		$url = "/home/eLearning/iTextbook/special_entrance.php";
	}
	# Clear ComposeFolder
	//     $composeFolder = "";
	//     session_unregister("composeFolder");
	session_unregister_intranet('composeFolder', "");

	# extra session for classroom use
	if($plugin['lslp']==true)
	{
		include_once("includes/liblslp.php");
		include_once("includes/libasp.php");
		$lslp = new lslp();
		$asp = new libasp('LSLP');
		if($lslp->isInLicencePeriod())
		{
			$ss_intranet_plugin['lslp'] 				= true;
			$ss_intranet_plugin['lslp_asp_server'] 		= $asp->ASP_Server;
		}

	}

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After LSLP = '.$memoryDiff.' / '.$accumulateMemory);
	}


	if($plugin['ler']==true)
	{
		$ss_intranet_plugin['ler'] 		= true;
	}
	//$ss_intranet_plugin['tts_server_url'] 	= $tts_server_url;
	//$ss_intranet_plugin['tts_user'] 	= $tts_user;

	//session_register("ss_intranet_plugin");
	$_SESSION['ss_intranet_plugin'] = $ss_intranet_plugin;

	# elibrary
    if ((isset($plugin['eLib']) && $plugin['eLib']) || (isset($plugin['library_management_system']) && $plugin['library_management_system']))
	{
		//session_register('intranet_elibrary_usertype');
		if($lu->isTeacherStaff())
		{
			$_SESSION['intranet_elibrary_usertype'] = "TEACHER";

			include_once("includes/libelibrary.php");
			$lib = new elibrary();
			$IsAdmin = $lib->IS_ADMIN_USER($_SESSION['UserID']);

			//session_register('intranet_elibrary_admin');
			$_SESSION['intranet_elibrary_admin'] = $IsAdmin;
		}
		else
		{
			$_SESSION['intranet_elibrary_usertype'] = "STUDENT";
		}
	}

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After eLib = '.$memoryDiff.' / '.$accumulateMemory);
	}

	#iPortfolio 2.5
	if (isset($plugin['iPortfolio']) && $plugin['iPortfolio'])
	{
		$IsAdmin = 0;  //init IsAdmin is false
		//session_register('intranet_iportfolio_usertype');
		if($lu->isTeacherStaff())
		{
			$_SESSION['intranet_iportfolio_usertype'] = "TEACHER";


			$AdminUser = trim(get_file_content($intranet_root."/file/iportfolio/admin_user.txt"));
			if(!empty($AdminUser))
			{
				$AdminArray = explode(",", $AdminUser);
				$IsAdmin = (in_array($_SESSION['UserID'], $AdminArray)) ? 1 : 0;
			}

			//session_register('intranet_iportfolio_admin');
			$_SESSION['intranet_iportfolio_admin'] = $IsAdmin;
			//$_SESSION['intranet_iportfolio_admin'] = $_SESSION['SSV_USER_ACCESS']['other-iPortfolio'];
		}
		else
		{
			$_SESSION['intranet_iportfolio_usertype'] = "STUDENT";
		}
	}

	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After iPortfolio = '.$memoryDiff.' / '.$accumulateMemory);
	}


	# eBooking
	if(isset($plugin['eBooking']) && $plugin['eBooking'])
	{
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eBooking"]) {
			//session_register('inventory');
			//session_register('role');
			$_SESSION["eBooking"]["role"] = "ADMIN";
		}else{
			include_once("includes/libebooking.php");
			$lebooking = new libebooking();

			if( ($lebooking->IsManagementGroupMember($UserID)) && ($lebooking->IsFollowUpGroupMemeber($UserID)) )
			{
				$_SESSION["eBooking"]["role"] = "MANAGEMENT_AND_FOLLOW_UP_GROUP_MEMBER";
			}
			else if($lebooking->IsManagementGroupMember($UserID))
			{
				$_SESSION["eBooking"]["role"] = "MANAGEMENT_GROUP_MEMBER";
			}
			else if($lebooking->IsFollowUpGroupMemeber($UserID))
			{
				$_SESSION["eBooking"]["role"] = "FOLLOW_UP_GROUP_MEMBER";
			}
			else
			{
				$_SESSION["eBooking"]["role"] = "GENERAL_USER";
			}
		}
	}
	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After eBooking = '.$memoryDiff.' / '.$accumulateMemory);
	}


	# ambook
	if(isset($plugin['ambook']) && $plugin['ambook'])
	{
		$ss_intranet_plugin['ambook'] = true;
	}

	# iTextbook
	if(isset($plugin['iTextbook']) && $plugin['iTextbook'])
	{
		include_once("includes/iTextbook/libitextbook.php");
		$objiTextbook = new libitextbook();

		if($lu->isParent()){
			$total_iTextBook_arr = array();
			$total_iTextBook_book_arr = array();
			$children_arr = $lu->getChildren();
			if(sizeof($children_arr)){
				foreach($children_arr as $key=>$children_UserID){
					$itextbook_arr = $objiTextbook->format_user_session_array($children_UserID);
					$itextbook_book_arr = $objiTextbook->format_user_book_session_array($children_UserID);
					foreach($itextbook_arr as $book_type => $books){
						if($total_iTextBook_arr[$book_type] == ""){
							$total_iTextBook_arr[$book_type] = array();
						}
						foreach($books as $book_key=>$bookID){
							if(!in_array($bookID, $total_iTextBook_arr[$book_type])){
								array_push($total_iTextBook_arr[$book_type],$bookID);
							}
						}
						sort($total_iTextBook_arr[$book_type]);
					}
					foreach($itextbook_book_arr as $book_book_type => $book_books){
						if($total_iTextBook_book_arr[$book_book_type] == ""){
							$total_iTextBook_book_arr[$book_book_type] = array();
						}
						foreach($book_books as $book_key=>$bookID){
							if(!$total_iTextBook_book_arr[$book_book_type][$book_key]){
								$total_iTextBook_book_arr[$book_book_type][$book_key] = $bookID;
								$total_iTextBook_book_arr[$book_book_type][$book_key]["children_UserID"] = $children_UserID;
							}
						}
					}
				}
			}
			//session_register('iTextbook');
			$_SESSION["iTextbook"] = $total_iTextBook_arr;
			//session_register('iTextbook_Book');
			$_SESSION["iTextbook_Book"] = $total_iTextBook_book_arr;
		}else{
			//session_register('iTextbook');
			$_SESSION["iTextbook"] = $objiTextbook->format_user_session_array($UserID, $lu->isTeacherStaff());
			//session_register('iTextbook_Book');
			$_SESSION["iTextbook_Book"] = $objiTextbook->format_user_book_session_array($UserID, $lu->isTeacherStaff());
		}
	}
	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After iTextbook = '.$memoryDiff.' / '.$accumulateMemory);
	}

	# eInventory
	if (isset($plugin['Inventory']) && $plugin['Inventory']) {
		include_once("includes/libinventory.php");
		$linventory = new libinventory();
		//session_register('inventory');
		//session_register('role');
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"]) {
			$_SESSION["SSV_USER_ACCESS"]["eAdmin-InvoiceMgmtSystem"] = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"];
			$_SESSION["inventory"]["role"] = "ADMIN";
		} else {
			# Check if user is in any eInventory Group
			$inventory_admin_group_list = $linventory->getInventoryAdminGroup();
			if($inventory_admin_group_list != "") {
				$arr_usertype = $linventory->getGroupUserType($inventory_admin_group_list);
				if(sizeof($arr_usertype)>0) {
					for($i=0; $i<sizeof($arr_usertype); $i++) {
						list($group_id, $user_type) = $arr_usertype[$i];
						if($user_type == MANAGEMENT_GROUP_ADMIN) {
							$group_admin++;
						}
					}
				}
				if($group_admin > 0)
					$_SESSION["inventory"]["role"] = "LEADER";
					else
						$_SESSION["inventory"]["role"] = "MEMBER";
			}
		}
	}
	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After Inventory = '.$memoryDiff.' / '.$accumulateMemory);
	}


	# eGuidance
	if (isset($plugin['eGuidance']) && $plugin['eGuidance']) {
		include_once("includes/eGuidance/libguidance.php");
		$libguidance = new libguidance($_SESSION['UserID']);
	}
	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After eGuidance = '.$memoryDiff.' / '.$accumulateMemory);
	}


	include_once("includes/libaccessright.php");
	$laccessright = new libaccessright();
	$UserAccessRight = $laccessright->retrieveUserAccessRight($UserID);
	//$_SESSION['SESSION_ACCESS_RIGHT'] = $UserAccessRight;
	session_register_intranet("SESSION_ACCESS_RIGHT", $UserAccessRight);
	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After libaccessright = '.$memoryDiff.' / '.$accumulateMemory);
	}


	# temp remark
	# eDiscipline v1.2
	# 20081122 - Check the access right and stored in session
	//if ($plugin['Disciplinev12'])
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"] || $plugin['Disciplinev12'])
	{
		include_once("includes/libdisciplinev12.php");
		$ldisciplinev12 = new libdisciplinev12();

		//session_register("SESSION_ACCESS_RIGHT");

		# eDiscipline - Enhancements [CRM Ref No.: 2009-0907-1157] (Perfect can access Management Page)
		# allow student to access eAdmin > eDis if student is assigned to access right group (2011-03-11 by Henry Chow)
		if($sys_custom['wscss_disciplinev12_student_access_cust'] || $ldisciplinev12->AllowToAssignStudentToAccessRightGroup)
		{
			# Check student in eDis Group or not
			$studentAccessRightInGroup = $ldisciplinev12->retrieveUserAccessRightInGroup($UserID);

			# Check user with normal access right
			$UserAccessRight = $ldisciplinev12->retrieveUserAccessRight($UserID);

			if(sizeof($studentAccessRightInGroup) > 0)
			{
				//$tempAry = $studentAccessRightInGroup;
				foreach($studentAccessRightInGroup as $key => $val) {
					foreach($val as $key1=>$val1) {
						//$tempAry[$key][$key1] = $val1;
						$UserAccessRight[$key][$key1] = $val1;
					}
				}

				$_SESSION["SSV_PRIVILEGE"]["disciplinev12"]["has_access_right"] = true;
			}
			//$_SESSION['SESSION_ACCESS_RIGHT'] = $UserAccessRight;
		}
		else {
			# SESSION_ACCESS_RIGHT should be independance to "eDis" Module
			$UserAccessRight = $ldisciplinev12->retrieveUserAccessRight($UserID);
			//$_SESSION['SESSION_ACCESS_RIGHT'] = $UserAccessRight;
		}

		# User already in "Approval Group", should also have "Approval" right even not in any eDis Right Group / Group without "Approval" right
		$useApprovalGroup = $ldisciplinev12->useApprovalGroup();		# Applied "Approval Group" in Setting
		$InApprovalGroup = $ldisciplinev12->InApprovalGroup();			# Check whether user in eDis Approval Group or not
		if($useApprovalGroup && $InApprovalGroup) {
			$UserAccessRight['DISCIPLINE']['MGMT']['AWARD_PUNISHMENT'][] = "VIEW";
			//$UserAccessRight['DISCIPLINE']['MGMT']['AWARD_PUNISHMENT'][] = "APPROVAL";
			//$UserAccessRight['DISCIPLINE']['MGMT']['GOODCONDUCT_MISCONDUCT'][] = "VIEW";
			//$UserAccessRight['DISCIPLINE']['MGMT']['GOODCONDUCT_MISCONDUCT'][] = "APPROVAL";
		}

		# [2017-0403-1552-54240] Check User in HOY Group or not
		if($sys_custom['eDiscipline']['HOY_Access_GM'])
		{
			$HOYGroupList = $ldisciplinev12->GET_HOY_GROUP_MEMBER($UserID);
			$inHOYGroup = !empty($HOYGroupList);

			// Set GM Access Right for HOY Group Member
			if($inHOYGroup) {
				$UserAccessRight['DISCIPLINE']['MGMT']['GOODCONDUCT_MISCONDUCT'][] = "VIEW";
//				$UserAccessRight['DISCIPLINE']['MGMT']['GOODCONDUCT_MISCONDUCT'][] = "NEW";
//				$UserAccessRight['DISCIPLINE']['MGMT']['GOODCONDUCT_MISCONDUCT'][] = "APPROVAL";
//				$UserAccessRight['DISCIPLINE']['MGMT']['GOODCONDUCT_MISCONDUCT'][] = "EDITALL";
//				$UserAccessRight['DISCIPLINE']['MGMT']['GOODCONDUCT_MISCONDUCT'][] = "DELETEALL";
			}
			$_SESSION["SSV_PRIVILEGE"]["IN_HOY_GROUP"] = $inHOYGroup;
		}

		// [2015-0825-1031-39071] fixed: User with access right cannot find eDis in Header > eAdmin if not an eDis Admin
		//$_SESSION['SESSION_ACCESS_RIGHT'] = $UserAccessRight;
		session_register_intranet("SESSION_ACCESS_RIGHT", $UserAccessRight);
	}
	if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
		$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
		$accumulateMemory = convert_size(memory_get_usage(true));
		debug_pr('After eDis = '.$memoryDiff.' / '.$accumulateMemory);
	}

	# for ReadingScheme
	if($plugin['ReadingScheme'])
	{
		unset($_SESSION['READING_SCHEME_SESSION']);
	}

	# for Flipped Channels
	if ($plugin['FlippedChannels']) { // Removed plugin flag checking for all school to use CS Channel
		include_once("includes/FlippedChannels/libFlippedChannels.php");
		$lFlippedChannels = new libFlippedChannels();

		$centralServerPath = $lFlippedChannels->getCurCentralServerUrl();
		$centralServerPath = ($centralServerPath==null)? '' : $centralServerPath;

		$flippedChannelsServerPublicPath = $lFlippedChannels->getServerPublicPath();
		$flippedChannelsServerPublicPath = ($flippedChannelsServerPublicPath === null) ? '' : $flippedChannelsServerPublicPath;

		//session_register("FlippedChannelsServerPath");
		$_SESSION["FlippedChannelsServerPath"] = $centralServerPath;
		$_SESSION["FlippedChannelsServerPublicPath"] = $flippedChannelsServerPublicPath;
	}



	/* temp remark
	 # delete mails in trash for the days more than the user specified days
	 if($special_feature['imail']){
	 $lib = new libdb();
	 # Step 1: Extract expired mails in trash
	 $sql_trash_day="SELECT DaysInTrash FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='$UserID'";
	 $temp = $lib->returnVector($sql_trash_day);
	 $trash_day = $temp[0]+0;
	 if($trash_day>0){
	 $expiry_day = date('Y-m-d',time()-60*60*24*$trash_day);
	 $sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND Deleted = 1 AND DATE_FORMAT(DateModified,'%Y-%m-%d')<'$expiry_day'";
	 //echo $sql;
	 $targetID = $lib->returnVector($sql);
	 }
	 if (sizeof($targetID)!=0){
	 $CampusMailIDStr = implode(",",$targetID);

	 # Step 2: Extract Attachment paths
	 $sql = "SELECT Attachment FROM INTRANET_CAMPUSMAIL WHERE MailType = 1 AND IsAttachment = 1 AND Deleted = 1 AND Attachment != '' AND UserID = $UserID AND CampusMailID IN ($CampusMailIDStr)";
	 $to_remove_attachmentpaths_int = $lib->returnVector($sql);
	 $sql = "SELECT Attachment FROM INTRANET_CAMPUSMAIL WHERE MailType = 2 AND IsAttachment = 1 AND Deleted = 1 AND Attachment != '' AND UserID = $UserID AND CampusMailID IN ($CampusMailIDStr)";
	 $to_remove_attachmentpaths_ext = $lib->returnVector($sql);


	 # Step 3: Remove reply records
	 # Remove the replies for notification mail sent
	 $sql = "DELETE FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID IN (".$CampusMailIDStr.")";
	 $lib->db_db_query($sql);

	 # Step 4: Remove Mail DB records
	 $sql = "DELETE FROM INTRANET_CAMPUSMAIL WHERE Deleted = 1 AND UserID = $UserID AND CampusMailID IN ($CampusMailIDStr)";
	 $lib->db_db_query($sql);

	 # Step 5: Update Notification
	 $sql = "UPDATE INTRANET_CAMPUSMAIL SET Notification = 0 WHERE CampusMailFromID IN ($CampusMailIDStr)";
	 $lib->db_db_query($sql);

	 # Step 6: Search for attachment path (Internal)
	 $lf = new libfilesystem();
	 unset($list_path_string_to_remove);
	 $delim = "";
	 for ($i=0; $i<sizeof($to_remove_attachmentpaths_int); $i++)
	 {
	 $target_path = $to_remove_attachmentpaths_int[$i];
	 if ($target_path != "")
	 {
	 $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE Attachment = '$target_path'";
	 $temp = $lib->returnVector($sql);
	 if (is_array($temp) && sizeof($temp)==1 && $temp[0]==0)
	 {
	 # Remove Database records
	 $list_path_string_to_remove .= "$delim'$target_path'";
	 $delim = ",";

	 # Remove actual files
	 $full_path = "$file_path/file/mail/".$target_path;
	 if ($bug_tracing['imail_remove_attachment'])
	 {
	 $command ="mv $full_path ".$full_path."_bak";
	 exec($command);
	 }
	 else
	 {
	 $lf->lfs_remove($full_path);
	 }
	 }

	 }
	 }

	 # Step 7: Remove Attachment path (External Mail)
	 for ($i=0; $i<sizeof($to_remove_attachmentpaths_ext); $i++)
	 {
	 $target_path = $to_remove_attachmentpaths_ext;
	 if ($target_path != "")
	 {
	 # Remove Database records
	 $list_path_string_to_remove .= "$delim'$target_path'";
	 $delim = ",";

	 # Remove actual files
	 $full_path = "$file_path/file/mail/".$target_path;
	 if ($bug_tracing['imail_remove_attachment'])
	 {
	 $command ="mv $full_path ".$full_path."_bak";
	 exec($command);
	 }
	 else
	 {
	 $lf->lfs_remove($full_path);
	 }
	 }
	 }


	 # Remove Database Records
	 if ($list_path_string_to_remove != "")
	 {
	 $sql = "DELETE FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE AttachmentPath IN ($list_path_string_to_remove)";
	 $lib->db_db_query($sql);
	 }


	 }
	 }
	 # end delete mails in trash more than the user input days
	 */

}

# eDiscipline - only staff can access Discipline module
if($lu->RecordType==1 && $plugin['Discipline'])
{
	include_once ("includes/libdiscipline.php");
	$LibDiscipline = new libdiscipline();
}
if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
	$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	$accumulateMemory = convert_size(memory_get_usage(true));
	debug_pr('After eDis 2 = '.$memoryDiff.' / '.$accumulateMemory);
}

# eDisciplinev12 - only staff can access Discipline module (temp!!)
if($lu->RecordType==1 && $plugin['Disciplinev12'])
{
	include_once ("includes/libdisciplinev12.php");
	$LibDisciplinev12 = new libdisciplinev12();
}
if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
	$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	$accumulateMemory = convert_size(memory_get_usage(true));
	debug_pr('After eDis 3 = '.$memoryDiff.' / '.$accumulateMemory);
}

if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
	$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	$accumulateMemory = convert_size(memory_get_usage(true));
	debug_pr('Before UPDATE_CONTROL_VARIABLE = '.$memoryDiff.' / '.$accumulateMemory);
}

UPDATE_CONTROL_VARIABLE();

if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
	$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	$accumulateMemory = convert_size(memory_get_usage(true));
	debug_pr('After UPDATE_CONTROL_VARIABLE = '.$memoryDiff.' / '.$accumulateMemory);
}
/*
 # eDiscipline - enhancements [CRM Ref No.: 2009-0907-1157] (Perfect can access Management page)
 if($sys_custom['wscss_disciplinev12_student_access_cust']) {
 if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"] || $plugin['Disciplinev12']) {
 include_once("includes/libdisciplinev12.php");
 $ldisciplinev12 = new libdisciplinev12();

 $studentAccessRightInGroup = $ldisciplinev12->retrieveUserAccessRightInGroup($UserID);
 //debug_pr($studentAccessRightInGroup);

 if(sizeof($studentAccessRightInGroup)>0) {
 $tempAry = $studentAccessRightInGroup;
 //debug_pr($tempAry);
 foreach($_SESSION['SESSION_ACCESS_RIGHT'] as $key=>$val) {
 foreach($val as $key1=>$val1) {
 $tempAry[$key][$key1] = $val1;
 }

 }
 $_SESSION['SESSION_ACCESS_RIGHT'] = $tempAry;
 $_SESSION["SSV_PRIVILEGE"]["disciplinev12"]["has_access_right"] = true;
 }
 }
 }
 */

//$lu->db_show_debug_log();


unset($li, $lldap, $leclass, $lwebmail, $lftp, $UserRightTarget, $lu, $LibDiscipline);
unset($lib, $lebooking, $linventory, $ldisciplinev12);

# clear cache
include_once("includes/cache_lite/Lite.php");
//$cached_hh_id = "U".$_SESSION['UserID']."T".$_SESSION['UserType']."P".$_SESSION['eclass_session_password']."L".strlen($intranet_root);
$cached_hh_id = CacheUniqueID($_SESSION['UserID']);
$options = array('cacheDir' => '/tmp/', 'lifeTime'=>3600);
$Cache_Lite = new Cache_Lite($options);
if ($cached_hh_data = $Cache_Lite->get($cached_hh_id))
{
	$Cache_Lite->remove($cached_hh_id);
}


if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
	$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	$accumulateMemory = convert_size(memory_get_usage(true));
	debug_pr('After Cache_Lite = '.$memoryDiff.' / '.$accumulateMemory);
}



# for testing
/*
 global $extra_setting;
 if ($extra_setting['show_process_time'])
 {
 $ProcessTime = StopTimer(2, true);
 $memory_benchmrk = "memory used in this page: <font color='yellow' size='3'>".number_format((memory_get_usage())/1024)."KB (".number_format((memory_get_usage())/1024/1024, 1)."MB)";
 $ProcessTimeShow = ($ProcessTime>1) ? "<font color='red' size='4'>".$ProcessTime."</font>" : "<font color='yellow' size='3'>".$ProcessTime."</font>";
 $ProcessTimeShowHTML = "<table width='100%' style='border:1px dotted #BBBBBB' border='0' cellpadding='0' cellspacing='0' id='ProcessTimeTable'><tr><td align='center' bgcolor='#000000'><font color='white' size='2'>Page generated in {$ProcessTimeShow} seconds.  $memory_benchmrk </font></td></tr></table>\n";
 $ProcessTimeShowHTML .= "<script language='javascript'>\nsetTimeout(\"displayTable('ProcessTimeTable', 'none')\", 9000);\n</script>\n";
 echo "Sorry, testing now. Please click <a href='/home'>Home</a> in order to access IP 2.5. ".$ProcessTimeShowHTML;
 }
 */



if ($OnlyModule!="" && ($_SESSION['eClassAppSSO_standaloneModule'] || ($sys_custom['SingleSignOnIP25eBookingFrom']!=""  || (is_array($sys_custom['SingleSignOnIP25PL']) && sizeof($sys_custom['SingleSignOnIP25PL'])>0))))
{
	if ($OnlyModule=="IP25eBooking")
	{
		$_SESSION['SSV_eBooking_standalone'] = true;
		$url = "/home/eService/eBooking/";
	}
	if ($OnlyModule=="IP25eBookingAdmin")
	{
		$url = "/home/eAdmin/ResourcesMgmt/eBooking";
	}

	switch ($OnlyModule) {
		case 'StaffAttendance':
			$url = "/home/eAdmin/StaffMgmt/attendance/";
			break;
		case 'eBooking':
			$url = "/home/eService/eBooking/";
			break;
		case 'eDiscipline':
			$url = "/home/eService/disciplinev12/";
			break;
		case 'eInventory':
			$url = "/home/eAdmin/ResourcesMgmt/eInventory/";
			break;
		case 'eBookingAdmin':
			$url = "/home/eAdmin/ResourcesMgmt/eBooking/";
			break;
		case 'eLearning':
		case 'IP25PowerLesson':
			$url = "/home/eLearning/eclass/";
			break;
		case 'eBookingAdmin':
			$url = "/home/eAdmin/ResourcesMgmt/eBooking/management/booking_request.php";
			break;
		case 'StaffAttendanceAdmin':
			$url = "/home/eAdmin/StaffMgmt/attendance/Management/Attendance/";
			break;
		case 'eDisiplineAdmin':
			$url = "/home/eAdmin/StudentMgmt/disciplinev12/overview/";
			break;
		case 'eInventoryAdmin':
			$url = "/home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php";
			break;
		case 'eLearningAdmin':
			$url = "/home/eLearning/eclass/organize/";
			break;
		case 'eNotice':
			$url = "/home/eService/notice/";
			break;
		case 'iCalendar':
			$url = "/home/iCalendar/";
			break;
		case 'medicalCaringBowel':
		    if ($sys_custom['medical']['swapBowelAndSleep']) {
		        $url = "/home/eAdmin/StudentMgmt/medical/?t=management.sleep";
		    }
		    else {
		        $url = "/home/eAdmin/StudentMgmt/medical/?t=management.bowel";
		    }
			break;
		case 'medicalCaringBowel_v2':
		    include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
		    $leClassApp = new libeClassApp();
		    $medicalToken = $leClassApp->getUrlToken($UserID, $UserLogin);
		    if ($sys_custom['medical']['swapBowelAndSleep']) {
		        $url = '/home/eClassApp/common/web_module/?token='.$medicalToken.'&uid='.$UserID.'&ul='.$UserLogin.'&parLang='.$_SESSION['intranet_hardcode_lang'].'#medical/sleep/management/edit';
		    }
		    else {
		        $url = '/home/eClassApp/common/web_module/?token='.$medicalToken.'&uid='.$UserID.'&ul='.$UserLogin.'&parLang='.$_SESSION['intranet_hardcode_lang'].'#medical/bowel/management/edit';
		    }
		    break;
		case 'medicalCaringSleep':
		    if ($sys_custom['medical']['swapBowelAndSleep']) {
		        $url = "/home/eAdmin/StudentMgmt/medical/?t=management.bowel";
		    }
		    else {
		        $url = "/home/eAdmin/StudentMgmt/medical/?t=management.sleep";
		    }
		    break;
		case 'medicalCaringSleep_v2':
		    include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
		    $leClassApp = new libeClassApp();
		    $medicalToken = $leClassApp->getUrlToken($UserID, $UserLogin);
		    if ($sys_custom['medical']['swapBowelAndSleep']) {
		        $url = '/home/eClassApp/common/web_module/?token='.$medicalToken.'&uid='.$UserID.'&ul='.$UserLogin.'&parLang='.$_SESSION['intranet_hardcode_lang'].'#medical/bowel/management/edit';
		    }
		    else {
		        $url = '/home/eClassApp/common/web_module/?token='.$medicalToken.'&uid='.$UserID.'&ul='.$UserLogin.'&parLang='.$_SESSION['intranet_hardcode_lang'].'#medical/sleep/management/edit';
		    }
		    break;

        case 'medicalCaringLog':
            $url = "/home/eAdmin/StudentMgmt/medical/?t=management.studentLog";
            break;
        case 'medicalCaringLog_v2':
            include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
            $leClassApp = new libeClassApp();
            $medicalToken = $leClassApp->getUrlToken($UserID, $UserLogin);

            $url = '/home/eClassApp/common/web_module/?token='.$medicalToken.'&uid='.$UserID.'&ul='.$UserLogin.'&parLang='.$_SESSION['intranet_hardcode_lang'].'#medical/student_log/management/edit';
            break;
        case 'classroom':
            $url = "/home/classrooms.php?app_lang=".$_SESSION['intranet_hardcode_lang'];
            break;
        case 'eDisciplineApp':
        	include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
        	$leClassApp = new libeClassApp();
        	$eDisToken = $leClassApp->getUrlToken($UserID, $UserLogin);

        	$url = '/home/eClassApp/common/web_module/?token='.$eDisToken.'&uid='.$UserID.'&ul='.$UserLogin.'&parLang='.$_SESSION['intranet_hardcode_lang'].'#ediscipline/goodconduct_misconduct/management/mylist';
        	break;
        case 'iMailApp':
            if ($sys_custom['eClassApp']['iMail']['AstriUI']) {
                $url = '/home/astri/demo/auth?token=' . $_SESSION['tmp_ApiToken'] . '&parLang=' . $_SESSION['intranet_hardcode_lang'];

                $_SESSION['tmp_ApiToken'] = "";
                unset($_SESSION['tmp_ApiToken']);
            }else{
                $url = '/home/imail_gamma/app_view/email_list.php?TargetFolderName=INBOX&parLang='.$_SESSION['intranet_hardcode_lang'].'&AppType='.$_SESSION['tmp_appSSO_appType'].'';
                session_unregister_intranet('tmp_appSSO_appType');
            }
            break;
        case 'iPfApp':
        	$url = '/home/portfolio/';
        	session_register_intranet("iPortfolioFromApp", 1);
            $_SESSION['ck_current_children_id'] = $_SESSION['tmp_appSSO_studentId'];
            session_unregister_intranet('tmp_appSSO_studentId');
            break;
        case 'medicalCaringBowelRecordDetails_v2':
        	include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
        	$leClassApp = new libeClassApp();
        	$medicalToken = $leClassApp->getUrlToken($UserID, $UserLogin);

        	$url = '/home/eClassApp/common/web_module/?token='.$medicalToken.'&uid='.$UserID.'&ul='.$UserLogin.'&parLang='.$_SESSION['intranet_hardcode_lang'].'#medical/bowel/management/view&RecordID='.$_SESSION['tmp_appSSO_recordID'].'&RecordTime='.$_SESSION['tmp_appSSO_recordDate'];
        	session_unregister_intranet('tmp_appSSO_recordID');
        	session_unregister_intranet('tmp_appSSO_recordDate');
        	break;
        case 'medicalCaringLogRecordDetails_v2':
        	include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
        	$leClassApp = new libeClassApp();
        	$medicalToken = $leClassApp->getUrlToken($UserID, $UserLogin);

        	$url = '/home/eClassApp/common/web_module/?token='.$medicalToken.'&uid='.$UserID.'&ul='.$UserLogin.'&parLang='.$_SESSION['intranet_hardcode_lang'].'#medical/student_log/management/view&RecordID='.$_SESSION['tmp_appSSO_recordID'];
        	session_unregister_intranet('tmp_appSSO_recordID');
        	break;
        case 'medicalCaringSleepRecordDetails_v2':
            include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
            $leClassApp = new libeClassApp();
            $medicalToken = $leClassApp->getUrlToken($UserID, $UserLogin);

            $url = '/home/eClassApp/common/web_module/?token='.$medicalToken.'&uid='.$UserID.'&ul='.$UserLogin.'&parLang='.$_SESSION['intranet_hardcode_lang'].'#medical/sleep/management/view&RecordID='.$_SESSION['tmp_appSSO_recordID'];
            session_unregister_intranet('tmp_appSSO_recordID');
            break;
        case 'eSchoolBus':
            $url = '/home/eClassApp/common/eSchoolBus/index.php?studentId='.$_SESSION['tmp_appSSO_studentId'].'&parLang='.$_SESSION['intranet_hardcode_lang'];
            session_unregister_intranet('tmp_appSSO_studentId');
            break;
        case 'eEnrolmentUserView':
            $url = '/home/eClassApp/common/eEnrolment/index.php?studentId='.$_SESSION['tmp_appSSO_studentId'].'&parLang='.$_SESSION['intranet_hardcode_lang'];
            session_unregister_intranet('tmp_appSSO_studentId');
            break;
        case 'eSchoolBusTeacher':
            $url = '/home/eClassApp/common/eSchoolBus/index.php?parLang='.$_SESSION['intranet_hardcode_lang'];
            break;
        case 'eEnrolmentStudentView':
            $url = '/home/eClassApp/common/eEnrolment/index.php?studentId='.$_SESSION['tmp_appSSO_studentId'].'&parLang='.$_SESSION['intranet_hardcode_lang'].'&isStudentApp=1';
            session_unregister_intranet('tmp_appSSO_studentId');
            break;
        case 'SchoolInfoPDF':
            include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
            $leClassApp = new libeClassApp();
            $pdfToken = $leClassApp->getUrlToken($UserID, $_SESSION['tmp_appSSO_recordID']);
            $url = 'home/eClassApp/common/school_info/pdfjs_viewer_custom/web/viewer.php?MenuID='.$_SESSION['tmp_appSSO_recordID'].'&token='.$pdfToken;
            session_unregister_intranet('tmp_appSSO_recordID');
            break;
        case 'ePOS':
            $url = '/home/eClassApp/common/ePOS/index.php?studentId='.$_SESSION['tmp_appSSO_studentId'].'&parLang='.$_SESSION['intranet_hardcode_lang'].'&fromApp=1';
            session_unregister_intranet('tmp_appSSO_studentId');
            break;
        case 'eInventoryApp':
            $url = '/home/eClassApp/common/eInventory/index.php?parLang='.$_SESSION['intranet_hardcode_lang'];
            session_unregister_intranet('tmp_appSSO_studentId');
            break;
        case 'eInventoryStocktake':
            $url = '/home/eClassApp/common/eInventory/?task=teacherApp.stocktake.stocktake_progress&parLang='.$_SESSION['intranet_hardcode_lang'].'&clearCoo=1';
            break;
        case 'eLearningTimetable':
            $url = '/home/eLearning/timetable/index.php?studentId='.$_SESSION['tmp_appSSO_studentId'].'&parLang='.$_SESSION['intranet_hardcode_lang'];
            session_unregister_intranet('tmp_appSSO_studentId');
            break;
	}
}

## SSO from Another Site ##
if (isset($_POST["_SsoFAS_"]) && $SsoFromAnotherSite['enabled']) {
    $url = "/sso/eclass/?task=login";
}

# UCCKE requirement
if($sys_custom['OnlineRegistry'] && $_SESSION['eClassAppSSO_standaloneModule'] !='1')
{
	include_once("includes/libstudentregistry.php");
	$lsr = new libstudentregistry();
	if($lsr->checkParentFillOnlineReg())
	{
		$url = "/home/eService/StudentRegistry/online_reg/index.php";
		# reset it in order to jump to online reg page
		if (!$sys_custom['StudentRegistry']['Kentville']) {
		    $target_url = "";
		}
	}
}

if ($debugMemory && $_SESSION['UserID']==$debugMemoryUserId) {
	$memoryDiff = convert_size(memory_get_usage(true)) - $accumulateMemory;
	$accumulateMemory = convert_size(memory_get_usage(true));
	debug_pr('After libstudentregistry = '.$memoryDiff.' / '.$accumulateMemory);
}


intranet_closedb();

if($sys_custom['project']['centennialcollege'] && $source=='cc_eap'){
	$target_url = '';
	# SESSION register for redirecting flag to Classroom
	$_SESSION['ck_project_cceap'] = $sys_custom['project']['centennialcollege'];
	if($cc_source == 'CAS'){
		$_SESSION['ck_project_cceap_role'] = "CENTENNIAL";
	}else{
		$_SESSION['ck_project_cceap_role'] = "WALKIN";
	}
	$url = '/cc_eap/register_eclass.php';
}

if($sys_custom['lp_stem_learning_scheme']){
    $url = '/home/portfolio/';
}

if($sys_custom['non_eclass_PaGamO']){
	$url = ($UserLogin=="broadlearning")?"/home/index.php":"/home/eLearning/LTI/Pagamo/";
}

if (isset($target_url) && !empty($target_url))
{
	$target_url = str_replace(array('http://','https://',$_SERVER['HTTP_HOST']),array('','',''),$target_url);
	if($target_url[0] != '/') $target_url = '/'.$target_url;
	if(isset($sys_custom['target_url_for_login'])) $target_url = $sys_custom['target_url_for_login'];
	header("Location: $target_url");
}else
{
    if ($url=='' && $_SESSION["platform"]=="KIS")
	{
		/* Comment by Pun, don't know why there is these code
		if ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
		{
			header("location: /home/eAdmin/AccountMgmt/StaffMgmt/");
		} else
		{
			header("location: /kis/");
		}*/
		header("location: /kis/");

	} else
	{
		if ($NewUserUrl){
			header("Location:$NewUserUrl");
			exit();
		}
		$url = str_replace(array('http://','https://',$_SERVER['HTTP_HOST']),array('','',''),$url);
		if($url[0] != '/') $url = '/'.$url;

		if(isset($ssoSrvType) && $ssoSrvType != ''){
			switch($ssoSrvType){
				case 'gmail':
					$url = "https://mail.google.com/a/".$_SESSION['SignInWithGoogle_UserEmailSuffix'];
					break;
				default:
					break;
			}
		}

		if ($sys_custom['PowerClass']&& $_SESSION['eClassAppSSO_standaloneModule'] !='1') {
			$url = "home/PowerClass/index.php";
		}

		header("Location: $url");
	}
}
exit();
?>