<?php

///authentication by eClass login///
include_once('../../includes/global.php');
include_once('../../includes/libdb.php');

echo "Welcome to Google service :".$_POST['myuser'];

@session_start();
if ($_POST['myuser']!=null)
{
	$ssousername=$_POST['myuser'];
	//session_register("ssousername");
	$_SESSION['ssousername'] = $ssousername;
}

if (!session_is_registered_intranet("ssousername"))
{
//	die("sso username session not registered");
	die("Invalid Credential.");	
//	$from_url = $_SERVER['SERVER_NAME']; 
//	$from_url .= $_SERVER['PHP_SELF'];
//	header("location: http://www.lamhonkwong.edu.hk/google/self/login.php?from=$from_url");
}
?>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <link href="css/style.css" type="text/css" rel="stylesheet"/>
  <title>Single Sign-On Service for Google Apps - Step 2: Google generates SAML Request</title>
	<script language="Javascript">
	function autoSubmit(){
	     document.start.submit();
	}
	</script>
</head>
<BODY onload="autoSubmit()">
<?php
	require_once('saml_util.php');
	/**
	 * Creates a SAML authentication request.
	 * @param string $acsURL The URL to the SSO ACS
	 * @param string $providerName The domain name of the identity requestor
	 * @return string
	 */
	function createAuthnRequest($acsURL, $providerName) {
	  $tml = file_get_contents('templates/AuthnRequestTemplate.xml');	  
	  $tml = str_replace('<PROVIDER_NAME>', $providerName, $tml); 
	  $tml = str_replace('<AUTHN_ID>', samlCreateId(), $tml); 
	  $tml = str_replace('<ACS_URL>', $acsURL, $tml); 
	  $tml = str_replace('<ISSUE_INSTANT>', samlGetDateTime(time()), $tml); 
	  
	  return $tml;
	}
	
	/**
	 * Assembles a URL containing the identity provider URL, the encoded
	 * SAML auth request, and the RelayState.
	 * @param string $ssoURL
	 * @param string $authnRequest
	 * @param string $relayStateURL
	 * @return string
	 */
	function computeURL($ssoURL, $authnRequest, $relayStateURL) {
	  $url = $ssoURL;
	  $url .= '?SAMLRequest=' . samlEncodeMessage($authnRequest);
	  $url .= '&RelayState=' . urlencode($relayStateURL);
	  return $url;
	}
	
	$action = 'Generate SAML Request';
	$returnPage = '2nd.php';	
	$ssoURL = '4_process_response.php';	
	$providerName = 'google.com';
	$domainName = $singlesignonservice["google"]["domain_name"]; //'lamhonkwong.edu.hk';
	
	switch($_POST['service']){
		case 'mail':
			$relayStateURL = 'https://mail.google.com/a/' . $domainName;
		break;
		default:
			$relayStateURL = '';
		break;
	}
	//$relayStateURL = 'https://mail.google.com/a/' . $domainName;
	
	$acsURI = 'https://www.google.com/a/' . $domainName . '/acs';
	
	$authnRequest = '';
	$redirectURL = '';
	$error = '';
	
	$authnRequest = createAuthnRequest($acsURI, $providerName);
	if ($authnRequest == NULL) {
	  	$error = 'Error: unable to locate request template';
	};
	
	$redirectURL = computeURL($ssoURL, $authnRequest, $relayStateURL);
	
	if ($error != null) {
	?>
    	<p><center><font color="red"><b><?= $error ?></b></font></center><p>
	<?php
    } else {
      if ($authnRequest != null && $redirectURL != null) {
		?>
			<FORM name="start" METHOD=POST ACTION="<?php echo $redirectURL; ?>">
			<INPUT TYPE="hidden" name="username" value="<?php echo $ssousername; ?>">
			</FORM>
		<?php
//	  echo $redirectURL;
      }
    }  
?>
</body>
</html>