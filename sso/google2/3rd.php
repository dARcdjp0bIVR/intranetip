<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta http-equiv="cache-control" content="no-cache, no-store">
  <meta http-equiv="pragma" content="no-cache">
  <link href="css/style.css" type="text/css" rel="stylesheet"/>
  <title>Single Sign-On Service for Google Apps - eclass</title>
</head>
<script language="JavaScript">
  function submit_now(s,r) {
    document.acsForm.SAMLResponse.value=s;
    document.acsForm.RelayState.value=r;
    document.acsForm.submit();
  }
</script>
<?php
 if ($samlResponse == null)
 {

	echo '<script language="Javascript">
	function autoSubmit(){
     document.IdentityProviderForm.submit();
	}
	</script>';
 }
 else
 {
	echo '<script language="Javascript">
	function autoSubmit(){
     document.acsForm.submit();
	}
	</script>';
 }
 echo "<h3>".$username."</h3>";
 ?>
<BODY onload="autoSubmit()">
<?php
/*
  if ($username == null) {
    $username = 'chongsk';
  }
*/
  //If SAML parameters still null, then authnRequest has not yet been
  //received by the Identity Provider, so user should not be logged in.
  if ( $SAMLRequest == null ) {
?>
 
<?php
  } else {  
    
    if ($error != "") {
?>
    <p><font color="red"><b><?php echo $error; ?></b></font><p>
<?php
  die();}
?>
<form name="IdentityProviderForm" action="4_process_response.php" method="post">
	<input type="hidden" name="SAMLRequest" value="<?php echo $SAMLRequest; ?>"/>
	<input type="hidden" name="RelayState" value="<?php echo htmlentities($relayState); ?>"/>
	<input type="hidden" name="returnPage" value="identity_provider.php">
	<input type="hidden" name="samlAction" value="Generate SAML Response">
	<input type="hidden" name="username" value="<?php echo $username; ?>">
</form>
  <?php 
    if ($samlResponse != null) {
      if ($username != null) {
  ?>
    <!-- This is a hidden form that POSTs the SAML response to the ACS. -->
    <form name="acsForm" action="<?php echo $acsURL; ?>" method="post">
<!--     <form name="acsForm" action="<?php echo $acsURL; ?>" method="post" 
	target="_blank"> -->
    <div style="display: none">
    <textarea rows=10 cols=80 name="SAMLResponse"><?php echo $samlResponse; ?>
	</textarea>
    <textarea rows=10 cols=80 name="RelayState"><?php echo htmlentities($relayStateURL); ?>
	</textarea>
    </div>
    </form>
  <?php
      } else {
  ?> 
    <p><span style="font-weight:bold;color:red">
     You must enter a valid username and password to log in.
    </span></p>
  <?php
      }
  ?>
    <span id="samlResponseDisplay">
    <div>Generated and Signed SAML Response</div>
    </span>
  <?php
      }
    }
  ?>
</body>
</html>
