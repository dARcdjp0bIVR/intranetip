<?php
// using :
/*
 * Change Log:
 *      2020-01-08 Frankie
 *          - add IGNORE_SECURITY_CHECK variable to ignore lib security convert function
 *
 * 		2018-06-14 Frankie
 * 			- SSO for DSI initial
 */

$PATH_WRT_ROOT = "../../";

define("IGNORE_SECURITY_CHECK", true);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

include_once($PATH_WRT_ROOT."includes/sso_dsi/libssodsi.php");
include_once($PATH_WRT_ROOT."includes/sso_dsi/libssodsi_ui.php");

$home_header_no_EmulateIE7 = true;

if (!isset($singlesignonservice['dsi'])) {
    No_Access_Right_Pop_Up();
}

if(isset($junior_mck)){
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.utf8.php");
	$g_encoding_unicode = true; // Change encoding from Big5 to UTF-8
	$module_custom['isUTF8'] = true;
}

## Customization extends
$indexVar['libSsoDsi'] = new libSsoDsi();
$indexVar['libSsoDsi_ui'] = new libSsoDsi_ui();

$indexVar['curUserId'] = $_SESSION['UserID'];

$taskSeparator = $indexVar['libSsoDsi']->getConfig('taskSeparator');

function custHandleFormPost($item, $key) {

	$replaceArr = array(
		"'" => "&#39;",
		'"' => "&#34;"
	);
	return str_replace(array_keys($replaceArr), array_values($replaceArr), $item);
}
intranet_auth();
intranet_opendb();

$indexVar['libSsoDsi']->checkAccessRight();

### handle all http post/get value by urldeocde, stripslashes, trim
array_walk_recursive($_POST, 'custHandleFormPost');
array_walk_recursive($_GET, 'custHandleFormPost');

if ($junior_mck) {
	$is_magic_quotes_active = (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) || function_exists("magicQuotes_addslashes");

	function magicQuotes_stripSlashes(&$item, $key)
	{
		$item = stripslashes($item);
	}
	if (count($_POST) > 0 && $is_magic_quotes_active) {
		$gpc = array(&$_POST, &$_GET);
		array_walk_recursive($gpc, 'magicQuotes_stripSlashes');
	}
}

### get system message
$returnMsgKey = $_GET['returnMsgKey'];
$indexVar['returnMsgLang'] = $Lang['General']['ReturnMessage'][$returnMsgKey];


### include corresponding php files
$indexVar['task'] = $_POST['task']? $_POST['task'] : $_GET['task'];

// load normal pages
if ($indexVar['task']=='') {
	// go to module index page if not defined
	$indexVar['task'] = 'auth.index';
}

$indexVar['controllerScript'] = 'controllers/'.str_replace($taskSeparator, '/', $indexVar['task']).'.php';

if (file_exists($indexVar['controllerScript'])){
	include_once($indexVar['controllerScript']);

	$indexVar['viewScript'] = 'views/'.str_replace($taskSeparator, '/', $indexVar['task']).'.tmpl.php';
	if (file_exists($indexVar['viewScript'])){

		// normal script => with template script
		include_once($indexVar['viewScript']);

		if( strpos($indexVar['task'], 'print') !== false || strpos($indexVar['task'], 'thickbox') !== false) {
			// print page and thickbox no footer
		}
		else {
		    // $indexVar['libSsoDsi_ui']->echoModuleLayoutStop();
		}
	}
	else {
		// update or ajax script => without template script
	}
}
else {
	No_Access_Right_Pop_Up();
}

// initial SLRS module default setting
$indexVar['libSsoDsi']->setDefaultSettings();

intranet_closedb();
?>
