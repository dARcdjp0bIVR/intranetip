<?php
// using :
/*
 * Change Log:
 * 2018-06-14 Frankie SSO for DSI initial
 */
if (!isset($indexVar['libSsoDsi']) ||
    !isset($_SESSION["UserID"]) ||
    $_SESSION["UserID"] <= 0)
{
    No_Access_Right_Pop_Up();
    exit;
}

/*
 * get configs
 */
$ssoDsiConfigs = $indexVar['libSsoDsi']->getConfig('all');

/*
 * generate Public key
 */
$eclass_public_key = md5(base64_encode($ssoDsiConfigs["passport_client_id"] . $ssoDsiConfigs['hex_key']));


$tokenInfo = $indexVar['libSsoDsi']->getTokenByUserID($_SESSION["UserID"]);

if ($tokenInfo !== false) {
    $access_token = $tokenInfo['AToken'];
    $refresh_token= $tokenInfo['RToken'];
    $user_id = sprintf("%020d", $_SESSION["UserID"]);
    
    $user_id = $indexVar['libSsoDsi']->db_encrypt_decrypt('encrypt', $user_id, $ssoDsiConfigs['hex_key'], false);
    $token = $indexVar['libSsoDsi']->db_encrypt_decrypt('encrypt', $access_token, $ssoDsiConfigs['hex_key'], false);
    $ftoken = $indexVar['libSsoDsi']->db_encrypt_decrypt('encrypt', $refresh_token, $ssoDsiConfigs['hex_key'], false);
    $ip_encrypt = $indexVar['libSsoDsi']->db_encrypt_decrypt('encrypt', $indexVar['libSsoDsi']->get_client_real_ip(), $ssoDsiConfigs['hex_key'], false);
    $current_link = $indexVar['libSsoDsi']->db_encrypt_decrypt('encrypt', $ssoDsiConfigs['receive_toekn'], $ssoDsiConfigs['hex_key'], false);
    $auth_link = $indexVar['libSsoDsi']->db_encrypt_decrypt('encrypt', $ssoDsiConfigs['auth_link'], $ssoDsiConfigs['hex_key'], false);
    
    $param = array (
        'action' => $ssoDsiConfigs['url'] . "/eclass/login-dsi",
        'fields' => array(
            'ut' => $user_id,
            'id' => $ip_encrypt,
            'lk' => $current_link,
            'token' => $token,
            'ctoken' => $ftoken,
            'callback' => $auth_link
        )
    );
    header("Location: " . $param["action"] . "?" . http_build_query($param["fields"]));
    exit;
} else {
    header("Location: " . $ssoDsiConfigs['auth_link']);
    exit;
}
