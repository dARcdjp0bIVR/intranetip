<?php
// using :
/*
 * Change Log:
 * 2018-06-14 Frankie SSO for DSI initial
 */

if (!isset($indexVar['libSsoDsi']) ||
    !isset($_GET["ut"]) || empty($_GET["ut"]) ||  
    !isset($_GET["json"]) || empty($_GET["json"]) ||
    !isset($_GET["checksum"]) || empty($_GET["checksum"]))
{
    No_Access_Right_Pop_Up();
    exit;
}

// if (!$indexVar['libSsoDsi']->verifyReferer($_SERVER['HTTP_REFERER'])) {
//     No_Access_Right_Pop_Up();
//     exit;
// }

/*
 * get configs
 */
$ssoDsiConfigs = $indexVar['libSsoDsi']->getConfig('all');

/*
 * generate Public key
 */
$eclass_public_key = md5(base64_encode($ssoDsiConfigs["passport_client_id"] . $ssoDsiConfigs['hex_key']));

/*
 * generate Pack Hex key
 */
$pack_key = pack('H*', $ssoDsiConfigs['pack_key']);

$responseArr = array(
    'ut' => $_GET['ut'],
    'json' => $_GET["json"],
    'checksum' => $_GET["checksum"]
);

$user_id = (int) $indexVar['libSsoDsi']->db_encrypt_decrypt('decrypt', $responseArr['ut'], $ssoDsiConfigs['hex_key'], false);
$json = $indexVar['libSsoDsi']->db_encrypt_decrypt('decrypt', $responseArr['json'], $ssoDsiConfigs['hex_key'], false);

$checkcode = md5($responseArr['json'] . $responseArr['ut'] . $eclass_public_key);

if ($checkcode == $responseArr['checksum'])
{
    
    if ($indexVar['libSsoDsi']->insertOrUpdateTokenInfo($user_id, $indexVar['libSsoDsi']->jsonDecode($json, true))) {
        header("Location: " . $ssoDsiConfigs['token_login']);
    } else {
        header("Location: " . $ssoDsiConfigs['auth_link']);
    }
} else {
    No_Access_Right_Pop_Up();
    exit;
}
exit;
?>