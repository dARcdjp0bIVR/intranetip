<?php
// using :
/*
 * Change Log:
 *      2020-01-08 Frankie
 *          - add IGNORE_SECURITY_CHECK variable to ignore lib security convert function
 * 
 *      2018-07-04 Frankie
 *          - SSO from another site for DSI initial
 */

$PATH_WRT_ROOT = dirname(dirname(dirname(__FILE__))) . "/";

define("IGNORE_SECURITY_CHECK", true);

if ($_GET['task'] == 'client_credentials')
{
    @session_start();
    session_destroy();
}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

include_once($PATH_WRT_ROOT."includes/sso/libSsoServer.php");
include_once($PATH_WRT_ROOT."includes/sso/libSsoServer_ui.php");

$home_header_no_EmulateIE7 = true;

if(isset($junior_mck)){
    include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.utf8.php");
    $g_encoding_unicode = true; // Change encoding from Big5 to UTF-8
    $module_custom['isUTF8'] = true;
}

## Customization extends
$indexVar['libSsoServer'] = new libSsoServer();
$indexVar['libSsoServer_ui'] = new libSsoServer_ui();

$indexVar['curUserId'] = (isset($_SESSION['UserID'])) ? $_SESSION['UserID'] : null;

$taskSeparator = $indexVar['libSsoServer']->getConfig('taskSeparator');

function custHandleFormPost($item, $key) {

    $replaceArr = array(
        "'" => "&#39;",
        '"' => "&#34;"
    );
    return str_replace(array_keys($replaceArr), array_values($replaceArr), $item);
}

// intranet_auth();
intranet_opendb();

$indexVar['libSsoServer']->checkAccessRight();

### handle all http post/get value by urldeocde, stripslashes, trim
array_walk_recursive($_POST, 'custHandleFormPost');
array_walk_recursive($_GET, 'custHandleFormPost');

if ($junior_mck) {
    $is_magic_quotes_active = (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) || function_exists("magicQuotes_addslashes");

    function magicQuotes_stripSlashes(&$item, $key)
    {
        $item = stripslashes($item);
    }
    if (count($_POST) > 0 && $is_magic_quotes_active) {
        $gpc = array(&$_POST, &$_GET);
        array_walk_recursive($gpc, 'magicQuotes_stripSlashes');
    }
}

### get system message
$returnMsgKey = isset($_GET['returnMsgKey']) ? $_GET['returnMsgKey'] : '';
$indexVar['returnMsgLang'] = isset($Lang['General']['ReturnMessage'][$returnMsgKey]) ? $Lang['General']['ReturnMessage'][$returnMsgKey] : '';


### include corresponding php files
$indexVar['task'] = isset($_POST['task']) ? $_POST['task'] : (isset($_GET['task']) ? $_GET['task'] : '');

// load normal pages
if (empty($indexVar['task'])) {
    // go to module index page if not defined
    $indexVar['task'] = 'index';
}

$indexVar['controllerScript'] = 'controllers/'.str_replace($taskSeparator, '/', $indexVar['task']).'.php';

if (file_exists($indexVar['controllerScript'])){
    include_once($indexVar['controllerScript']);
    $indexVar['viewScript'] = 'views/'.str_replace($taskSeparator, '/', $indexVar['task']).'.tmpl.php';
    if (file_exists($indexVar['viewScript'])){

        // normal script => with template script
        include_once($indexVar['viewScript']);

        if( strpos($indexVar['task'], 'print') !== false || strpos($indexVar['task'], 'thickbox') !== false) {
            // print page and thickbox no footer
        }
        else {
            // $indexVar['libSsoDsi_ui']->echoModuleLayoutStop();
        }
    }
    else {
        // update or ajax script => without template script
    }
}
else {
    No_Access_Right_Pop_Up();
}

// initial SLRS module default setting
$indexVar['libSsoDsi']->setDefaultSettings();

intranet_closedb();
?>
