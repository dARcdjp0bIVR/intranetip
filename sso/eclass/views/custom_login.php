<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $_view['page_title']; ?></title>
    <link rel="stylesheet" type="text/css" href="./assets/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="./assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" type="text/css" href="./assets/css/login-dsi.css" />
    <link rel="apple-touch-icon" sizes="180x180" href="./assets/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="./assets/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="./assets/images/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="./assets/images/favicon/manifest.json">
    <link rel="mask-icon" href="./assets/images/favicon/safari-pinned-tab.svg" color="#00abb6">
    <script type="text/javascript" src="/templates/jquery/jquery-3.3.1.min.js"></script>
    <meta name="theme-color" content="#ffffff">
  </head>
<body class="login">
	<div id="wrapper">
		<div class="login-box">
			<span id="logo">
				<?php if (empty($_view['login_title'])) { ?>
					<img src="<?php echo $_view['login_logo']; ?>" width="150">
				<?php } else { ?>
					<span class="logo-left"><img src="<?php echo $_view['login_logo']; ?>" width="150"></span>
					<span class="logo-right"><?php echo $_view['login_title']; ?></span>
				<?php } ?>
			</span>
			<div class="login-form">
				<form class="form-horizontal" name="form1" id='form1' action="/login.php" method="post">
					<?php echo $_view['SecureToken']; ?>
					<input type='hidden' name='_SsoFAS_' value='<?php echo $_SESSION["ECLASS_SSO"]["client_credentials"]["client_id"]; ?>'>
                    <div role="alert" class="alert alert-success"><?php echo $_view['prompt_msg']; ?></div>
                    <span class="text-label"><?php echo $_view['login_id']; ?></span>
                    <input class="" type="text" type="text" name="UserLogin" id="UserLogin" />
                    <span class="text-label"><?php echo $_view['password']; ?></span>
                    <input class="" type="password" name="UserPassword" id="UserPassword" />
                	<button class="btn btn-orange" onClick=''><?php echo $_view['login_button']; ?></button>
    				<div class="alert alert-danger" role="alert" id='errormsg' <?php if (!(isset($_view['show_error_msg']) && $_view['show_error_msg'])) { ?> style='display: none;' <?php } ?> >
	                   <!--<span class="icon-alert"></span>-->
                   	<?php echo $_view['error_msg']; ?>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="bg">
	    <!-- <img src="../../images/login-bg.jpg"> changed to svg tag because of IE-->
		<svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svgBlur">
    		<filter id="svgBlurFilter">
            	<feGaussianBlur in="SourceGraphic" stdDeviation="5" />
          	</filter>
          	<image xlink:href="./assets/images/login-bg-ipej.jpg" x="0" y="0" height="100%" width="100%" filter="url(#svgBlurFilter)" preserveAspectRatio="xMinYMax slice"/>
    	</svg>
    </div>
</div>
<script language='javascript'>

	var dsiLoginJS = {
		vrs: {
			show_error: <?php echo $_view['show_error_msg'] ? 'true': 'false'; ?>
		},
		ltr: {
			submitHandler: function(e) {
				e.preventDefault();
				if (!dsiLoginJS.cfn.checkText($('#UserLogin').val()) || !dsiLoginJS.cfn.checkText($('#UserPassword').val()))
				{
					$('#errormsg').show();					
				} else {
					$('#form1').unbind('submit', dsiLoginJS.ltr.submitHandler);
					$('#form1').submit();
				}
			}
		},
		cfn: {
			checkText: function(text) {
				if (text != '' && typeof text != null)
				{
					return true;
				} else {
					return false;
				}
			},
			init: function() {
				$('#form1').bind('submit', dsiLoginJS.ltr.submitHandler);
			}
		}	
	};
	$(document).ready(function(e) {
		dsiLoginJS.cfn.init();
	});
</script>
</body>
</html>