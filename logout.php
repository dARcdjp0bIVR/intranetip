<?php
## Using By : 

/********************** Change Log ***********************/
#
#   Date    :   2020-07-18 [Bill] [ip.2.5.11.5.1]   [2020-0710-1443-08073]
#               clear cookies related to session
#               clear session from globals
#               unregister session - LOGIN_INTRANET_SESSION_USERID
#               Commented - session start & regenerate
#
#	Date	:	2018-08-08 [Carlos]
#				Use session_regenerate_id(true) to generate a new session id for each new login.
#
#	Date	:	2016-09-05 [Pun] [ip.2.5.7.10.1]
#				Remove clear sessionKey logic
#
#	Date	:	2015-12-14 [Ivan] [ip.2.5.7.1.1]
#				added app single SSO iCal logic to remove the related session when logout
#
#	Date	:	2015-11-30 [Carlos]
#				replace session_unregister() with session_unregister_intranet() for PHP 5.4
#
#	Date	:	2014-02-03 [Ivan]
#				added eClass App housekeeping logic
#
# 	Date	: 	2012-07-27 [Yuen]
#				introduced house keeping logic and handled SMS refresh
#
# 	Date	: 	2011-05-20 [Yuen]
#				Reset cached data (Cache Lite)
#  
/******************* End Of Change Log *******************/

error_reporting(E_ERROR);

$ck_intranet_justlogin = 0;
setcookie("ck_intranet_justlogin",0,time()-3600,"/");

include_once("includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();

$li = new libdb();

# for safety clear session key!!
/* Don't clear session key now * /
$sql = "UPDATE INTRANET_USER SET SessionKey = '' WHERE UserID = ".$_SESSION["UserID"];
$li->db_db_query($sql);
/* */

include_once($PATH_WRT_ROOT."includes/cache_lite/Lite.php");
//$cached_hh_id = "U".$_SESSION['UserID']."T".$_SESSION['UserType']."P".$_SESSION['eclass_session_password']."L".strlen($intranet_root);
$cached_hh_id = CacheUniqueID($_SESSION['UserID']);

$options = array('cacheDir' => '/tmp/', 'lifeTime'=>7200);
$Cache_Lite = new Cache_Lite($options);
/* Test if there is a valid cache-entry for this key */
if ($cached_hh_data = $Cache_Lite->get($cached_hh_id)) {
	$Cache_Lite->remove($cached_hh_id);
}

if (!IsHouseKeepingDoneToday())
{	
	include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
	
	$libsms = new libsmsv2();
	$libsms->Refresh_SMS_Status();
	
	if ($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {
		include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
		$leClassApp = new libeClassApp();
		
		$success = $leClassApp->doHouseKeeping();
	}
	
	LogHouseKeeping();
}

update_login_session();

session_unregister_intranet("UserID");
session_unregister_intranet("eclass_session_password");
session_unregister_intranet('SSV_PRIVILEGE');
session_unregister_intranet("iSession_LoginSessionID");
session_unregister_intranet("UserType");
session_unregister_intranet("isTeaching");
session_unregister_intranet("Is_First_Login");

session_unregister_intranet("tmp_username");
session_unregister_intranet("tmp_password");
session_unregister_intranet("BLNews");
session_unregister_intranet("intranet_session_language");
session_unregister_intranet("SSV_HTTP_HOST");
session_unregister_intranet("eClassAppSSO_standaloneModule");
session_unregister_intranet("eClassAppSSO_standaloneModule_iCalendar");

session_unregister_intranet("ck_course_id");
session_unregister_intranet("ck_user_id");
session_unregister_intranet("ck_login_session_id");
session_unregister_intranet("ck_user_course_id");

// [2020-0710-1443-08073] added to clear LOGIN_INTRANET_SESSION_USERID
session_unregister_intranet("LOGIN_INTRANET_SESSION_USERID");

/*
# add back later
session_unregister("composeFolder");
session_unregister("noticeAttFolder");
session_unregister("iNewCampusMail");
session_unregister("intranet_iMail_checked");
session_unregister('intranet_discipline_right');
session_unregister('intranet_discipline_acl');
session_unregister('intranet_sports_right');
session_unregister('intranet_servicemgmt_adminlevel');
session_unregister('intranet_inventory_admin');
session_unregister('intranet_inventory_groupadmin');
session_unregister('SESSION_ACCESS_RIGHT');
*/

intranet_closedb();

if(session_id())
{
    // [2020-0710-1443-08073] clear cookies related to session
    if(isset($_COOKIE[session_name()])) {
        setcookie(session_name(), '', time()-3600, '/');
    }
    $_SESSION = array();    // clear session from globals
	session_destroy();	    // clear session from disk

    // [2020-0710-1443-08073] commented - session start & regenerate
	// session_start();
	// session_regenerate_id(true);    // generate a new session id for each new login to avoid being attacked with the same session id
}

if ($IsTimeout)
{
	header ("Location: /templates/timeout.php?timeoutat=".time()."&lg=$lg");
}
elseif (is_file("$intranet_root/templates/exit.customized.php"))
{
    header ("Location: /templates/exit.customized.php");
}
elseif ($target_url)
{
    header("Location: $target_url");
}
else
{
    header("Location: index.php");
}
?>