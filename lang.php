<?php
# using: 

########### Change Log [Start]
#
#   Date    :   2019-03-27 [Bill]   [2019-0228-1554-51235]
#               fixed: cannot redirect to correct pages if using HTTP/1.0
#
#	Date	: 	2018-10-19 [Paul]
#				Add security checking on the redirection url to prevent malicious use of the redirection features on external links
#
#	Date	: 	2015-08-12 [Jason]
#				Support php 5.4+ > modify session_register()
#
# 	Date	: 	2011-05-17 [Yuen]
#				Reset cached data (Cache Lite)  
#
#	Date    :	2010-09-03 YatWoon
#			    save last lang for user
#
#####################################

include(realpath("includes/global.php"));
switch($lang){
	case "en": $l = "en"; break;
	case "b5": $l = "b5"; break;
	case "gb": $l = "gb"; break;
	default: $l = $intranet_default_lang; break;
}

if(checkHttpsWebProtocol()){
	$psuedo_url = "https://".$_SERVER['HTTP_HOST']."/";
}else{
	$psuedo_url = "http://".$_SERVER['HTTP_HOST']."/";
}

if(isset($UserID))
{
	include_once(realpath("includes/libdb.php"));
	intranet_auth();
	intranet_opendb();
	
	$li = new libdb();
    
	$sql = "update INTRANET_USER set LastLang='$l' where UserID='$UserID'";
	$li->db_db_query($sql);
	$_SESSION['LastLang'] = $l;
}

session_register_intranet("intranet_session_language", $l);
//$intranet_session_language = $l;

$url = (isset($url)) ? "./".str_replace($psuedo_url, "", $url) : $_SERVER["HTTP_REFERER"];
if(!realpath($url)){
	$url = $psuedo_url;
}

# clear cache
include_once(realpath("includes/cache_lite/Lite.php"));
//$cached_hh_id = "U".$_SESSION['UserID']."T".$_SESSION['UserType']."P".$_SESSION['eclass_session_password']."L".strlen($intranet_root);
$cached_hh_id = CacheUniqueID($_SESSION['UserID']);
$options = array('cacheDir' => '/tmp/', 'lifeTime'=>3600);
$Cache_Lite = new Cache_Lite($options);
if ($cached_hh_data = $Cache_Lite->get($cached_hh_id))
{
	$Cache_Lite->remove($cached_hh_id);
}

intranet_closedb();

// Security Check on the redirect destination as to prevent malicious use of the features 	
$psuedo_url_headers = @get_headers($psuedo_url.$url);
$real_path_checking = realpath($url);

// [2019-0228-1554-51235] added checking for HTTP/1.0
//if(!$psuedo_url_headers || $psuedo_url_headers[0] == 'HTTP/1.1 404 Not Found' || strpos($url, 'lang.php') !== false) {
if(!$psuedo_url_headers || $psuedo_url_headers[0] == 'HTTP/1.0 404 Not Found' || $psuedo_url_headers[0] == 'HTTP/1.1 404 Not Found' || strpos($url, 'lang.php') !== false) {
	$exists = false;
	$url = ($_SERVER["HTTP_REFERER"]=="")?$psuedo_url:$_SERVER["HTTP_REFERER"];
}else{
	$exists = true;
}
header("Location: $url");
?>