<?php
# Modifing by : 

@session_start();

# don't load any language wordings
$NoLangWordings = true;
include_once("includes/global.php");

if (is_file("$intranet_root/servermaintenance.php") && !$iServerMaintenance)
{
    header("Location: servermaintenance.php");
    exit();
}

include_once("includes/libdb.php");
include_once("includes/libeclass.php");
intranet_opendb();

$UserLogin = htmlspecialchars(urldecode($_REQUEST["UserLogin"]));
$eClassKey = $_REQUEST['eClassKey'];
$target_url = urldecode($_REQUEST['target_url']);
$FromSite = urldecode($_REQUEST['FromSite']);

# check session for login
if ($eClassKey!="" && $FromSite!="") {
$fp = fopen($FromSite."/check.php?eclasskey=".$eClassKey,"r");
$content = fgets($fp,1024);
$data = explode(",",$content);

	if ($data[0]==1) {
		include_once("includes/libuser.php");
		$lu = new libuser("",$UserLogin);
		
		if ($lu->UserID != $_SESSION['UserID']) { 
			if(trim($lu->UserPassword) == '') {
				if($intranet_authentication_method=="HASH"){
					include_once("includes/libauth.php");
					$lauth = new libauth();
					
					$lu->UserPassword = $lauth->GetUserDecryptedPassword($lu->UserID);
				}
			}
			
			$ToUrl = "login.php?UserLogin=".$UserLogin."&UserPassword=".$lu->UserPassword."&target_url=".urlencode($target_url);
		}else{ 
			$ToUrl = $target_url;
		}
		header("Location: ".$ToUrl);
	}
	else {
		echo "<b>Sorry, you cannot access this site. Please visit it from eClass IP 2.5.</b>";
		die();
	}
}
?>