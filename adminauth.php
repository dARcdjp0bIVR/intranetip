<?php
/*
 * ########e Please edit with UTF-8 encoding. ##########
 * 2016-01-19 (Carlos): This script was run after logged out from admin console /adminlogout.php for non IE browsers, to force re-authenticate login to clear previous cached credentials. 
 * 						*** This is a workaround for Chrome because even close Chrome browser does not help to clear cached credentials. No way to log out for Chrome brings security problem. *** 
 */
include_once("includes/global.php");

$_SESSION['ADMIN_AUTHENTICATED'] += 1;
if($_SESSION['ADMIN_AUTHENTICATED'] > 1 && $_SERVER['PHP_AUTH_USER'] == $_SESSION['ADMIN_USER'] && $_SERVER['PHP_AUTH_PW'] == $_SESSION['ADMIN_PASS']){
	$_SESSION['ADMIN_AUTHENTICATED'] = 0;
	$_SESSION['ADMIN_USER'] = '';
	$_SESSION['ADMIN_PASS'] = '';
	unset($_SESSION['ADMIN_AUTHENTICATED']);
	unset($_SESSION['ADMIN_USER']);
	unset($_SESSION['ADMIN_PASS']);
	header("Location: /admin");
	exit;
}else{
	header('WWW-Authenticate: Basic realm="Admin"');
	header('HTTP/1.0 401 Unauthorized');
}

header("Content-Type:text/html;charset=".returnCharset());
if ($intranet_session_language=="b5")
{
    $response_msg = "你已經登出 eClass 綜合平台管理中心.<br><br>
基於安全理由, 請關閉所有瀏覽器視窗以完全登出.";
}
else
{
    $response_msg = "You have already logged out eClass IP Admin Console.<br><br>

To safe keep your account information, you are advised to close all your browsers instantly.";
}
echo $response_msg;

exit;

?>