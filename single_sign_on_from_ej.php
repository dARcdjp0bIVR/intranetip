<?php

############### Change Log [Start]
#
#	Date:	2016-10-07 Yuen
#			Customization: store $_SESSION['LoginPrefix'] for other usage
#
#	Date:	2015-11-13 Yuen
#			Customization: single-sign-on to PowerLesson of IP 2.5
#
#	Date:	2011-05-18 Yuen
#			Customization: single-sign-on to eBooking of IP 2.5
#
#
###############################

# don't load any language wordings
$NoLangWordings = true;

include_once("includes/global.php");
include_once("includes/libdb.php");

if (strstr($eClassURL, $sys_custom['SingleSignOnIP25eBookingFrom']))
{
	$AccessAllow = true;
} elseif (is_array($sys_custom['SingleSignOnIP25PL']) && sizeof($sys_custom['SingleSignOnIP25PL'])>0)
{
	for ($i=0; $i<sizeof($sys_custom['SingleSignOnIP25PL']); $i++)
	{
		$settingRow = $sys_custom['SingleSignOnIP25PL'][$i];
		if (strstr($eClassURL,$settingRow[0]))
		{
			$AccessAllow = true;
			$LoginPrefix = trim($settingRow[1]);
			break;
		}
	}
}



# check if permitted
if (!$AccessAllow)
{
	echo "Sorry, your site does not have the access right.";
	die();
}


intranet_opendb();


# check session for login
if ($eclasskey!="" && $eClassURL!="")
{
	/*
		$fp = @fopen("{$eClassURL}/check.php?eclasskey=$eclasskey","r");
		$content = fgets($fp,1024);
		*/


	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_URL, "{$eClassURL}/check.php?eclasskey=$eclasskey");
	$content = curl_exec($ch);

	$data = explode(",",$content);
	list($IsFound, $remote_userlogin) = $data;
}
session_start();

//store loginprefix for other usage
if($sys_custom['sahk_login_without_prefix']){
	$_SESSION['LoginPrefix'] = $LoginPrefix;
}

$remote_userlogin = $LoginPrefix . $remote_userlogin;


# proceed sign-on and set the standalone module
if ($IsFound && $remote_userlogin!="")
{
	//header("location: login.php?UserLogin=".$remote_userlogin);
	$UserLogin = $remote_userlogin;

	if ($intranet_authentication_method!="LDAP")
	{
		$libuser = new libdb();
		$sql = "SELECT UserPassword, HashedPass  FROM INTRANET_USER Where UserLogin='".$UserLogin."'";
		$row = $libuser->returnArray($sql);

		list($UserPassword, $HashPassword) = $row[0];

		if ($UserPassword!="" || $HashPassword!="")
		{
			$account_found = true;
			//header("location: login.php?UserLogin=".$UserLogin."&UserPassword=".$UserPassword);
			$_SESSION['tmp_UserLogin'] = $UserLogin;
			if ($intranet_authentication_method=="HASH")
			{
				$_SESSION['tmp_UserPassword'] = $HashPassword;
			} else
			{
				$_SESSION['tmp_UserPassword'] = $UserPassword;
			}
			header("location: login.php?OnlyModule=".$module);
		}
	} else
	{
		$account_found = true;
		header("location: login.php?UserLogin=".$UserLogin);
	}
} else
{
	$account_found = false;
}

if (!$account_found)
{
	echo "Sorry, this single-sign-on requires the same user account among the system in eClass Junior (primary school) and IP 2.5 (secondary school).<br />Please contact your system admin to create one in IP 2.5 and then try again.";
	die();
}


intranet_closedb();
?>