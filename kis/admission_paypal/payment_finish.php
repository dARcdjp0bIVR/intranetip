<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
intranet_opendb();
$li = new libdb();
//debug_pr($_REQUEST);
echo 'Now Loading...';
if($st == 'Completed'){
$data = array('cmd' => '_notify-synch','tx' => $tx,'at' => $admission_cfg['paypal_signature']);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$admission_cfg['paypal_url']);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_FAILONERROR,1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
//	curl_setopt($ch, CURLOPT_CONNECTITIMEOUT, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 15);
	$retValue = curl_exec($ch); 
	curl_close($ch);
	if($retValue != ""){
		$retArr = explode("\n",$retValue);
		if($retArr[0] == 'SUCCESS'){
			$success = 1;
			for($i=1;$i<count($retArr);$i++){
				list($key,$value) = explode("=",$retArr[$i]);
				$value=urldecode(str_replace('+',' ',$value));
				$field[$key] = iconv('Big5', 'UTF-8', $value);
			}
			//debug_pr($field);
			$sql = "
				INSERT INTO 
					ADMISSION_PAYMENT_INFO
					(ApplicationID, payment_status, txn_id, payer_id, receiver_id, payer_email,receiver_email, mc_currency, mc_gross, mc_fee,payment_date,item_name,inputdate)
				VALUES
					(
						'".$field['custom']."',
						'".$field['payment_status']."',
						'".$field['txn_id']."',
						'".$field['payer_id']."',
						'".$field['receiver_id']."',
						'".$field['payer_email']."',
						'".$field['receiver_email']."',
						'".$field['mc_currency']."',
						'".$field['mc_gross']."',
						'".$field['mc_fee']."',
						'".$field['payment_date']."',
						'".$field['item_name']."',
						NOW()
					)
			";
			//$result = $li->db_db_query($sql);
			if($result)
				//$payment_id = $li->db_insert_id();
			
			$sql = "
				UPDATE 
					ADMISSION_APPLICATION_STATUS s 
				INNER JOIN 
					ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID 
				SET	
	      			s.Status = '".$admission_cfg['Status']['paymentsettled']."',
	      			s.DateModified = NOW()
	     		WHERE 
					o.ApplicationID IN (".$field['custom'].")
	    	";
	    	//$li->db_db_query($sql);
			
		}else{
			
			$success = 0;
		}
	}
}else{
	$success = 0;
}
intranet_closedb();

$html = '
	<script>
		/*window.opener.focusAndGo();
		window.close();*/
		window.location.href="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/index.php?token='.MD5($field['custom']?$field['custom']:$cm).'";
	</script>
';

echo $html;
?>