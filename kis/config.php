<?
// Editing by 
/*
 * 2020-09-18 Philips
 *  add Power Portfolio for all users
 * 2020-05-11 Philips [ip.2.5.11.5.1.0]
 *  add CEES for Teacher
 * 2020-01-13 Tommy [ip.2.5.10.10.1.2]
 *  fix: eEnrolment promission
 * 2019-07-18 Henry [ip.2.5.10.10.1]
 *  add flag to hide Calendar and SchoolNews
 * 2019-06-25 Tommy [ip.2.5.10.10.1]
 *  add eEnrolment for student and teacher
 *  add iCalendar for all user
 *  add eCircular for teacher
 * 2019-03-29 Cameron [ip.2.5.10.4.1]
 *  add student registry for Kentville Parent
 * 2019-02-01 Carlos [ip.2.5.10.2.1]
 *  introduced a new flag $sys_custom['KIS_HideCampusMail'] for hiding Campus Mail.
 *  introduced a new flag $sys_custom['KIS_HidePhotoAlbum'] for hiding Photo Album.
 * 2018-05-30 Cameron [ip2.5.9.7.1] 
 *  add digitial channels
 * 2017-09-22 Ivan [ip.2.5.8.10.1.0]
 *  modified: enable Account Mgmt generally for $stand_alone['eAdmission']
 * 2015-09-18 Ivan [ip.2.5.6.10.1.0]
 * 	modified: updated icon ordering as module name alphabetical order
 */

//-- eAdmission Settings for 146 site only (do not upload to 149) [start]
if ($plugin['eAdmission_devMode'] && ($_SERVER['SERVER_NAME'] == '192.168.0.146' || $_SERVER['SERVER_NAME'] == '192.168.0.171')){
    function resetAdmissionFlag(){
        global $sys_custom;
        $admissionSchool = array(
            'AOGKG',
            'CHIUCHUNKG',
            'CREATIVE',
            'CSM',
            'HKUGAPS',
            'ICMS',
            'KTLMSKG',
            'KTLSKS',
            'MGF',
            'MINGWAI',
            'MINGWAINP',
            'MINGWAIPE',
            'MUNSANG',
            'RMKG',
            'SHCK',
            'SSGC',
            'STCC',
            'TBCPK',
            'TSUENWANBCKG',
            'UCCKE',
            'YLSYK',
        );
        foreach($admissionSchool as $school){
    		$sys_custom['KIS_Admission'][$school]['Settings'] = false;
        }
        return $sys_custom;
    }
    
	if($UserID == '4004'){ // for kis_henrychan
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'hkugaps.eclasscloud.hk';
		$sys_custom['KIS_Admission']['HKUGAPS']['Settings'] = true;
//		$setting_path_ip_rel = 'ssgc.eclass.hk';
//		$sys_custom['KIS_Admission']['SSGC']['Settings'] = true;
	}
	if($UserID == '26458'){ //for kis_rmkg
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'rmkg.eclass.hk';
		$sys_custom['KIS_Admission']['RMKG']['Settings'] = true;
	}
	if($UserID == '26457'){ //for kis_tsuenwanbckg
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'tsuenwanbckg.eclass.hk';
		$sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings'] = true;
	}
	if($UserID == '26459'){ //for kis_ktlmskg
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'ktlms.eclasscloud.hk';
		$sys_custom['KIS_Admission']['KTLMSKG']['Settings'] = true;
	}
	if($UserID == '26511'){ //for kis_uccke
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'uccke.eclasscloud.hk';
		$sys_custom['KIS_Admission']['UCCKE']['Settings'] = true;
	}
	if($UserID == '26775'){ //for kis_mosgraceful
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'mosgraceful.eclass.hk';
		$sys_custom['KIS_Admission']['MGF']['Settings'] = true;
	}
	if($UserID == '26776'){ //for kis_tbck
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'tbck.eclass.hk';
		$sys_custom['KIS_Admission']['TBCPK']['Settings'] = true;
	}
	if($UserID == '26777'){ //for kis_ylsyk
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'ylsyk.eclass.hk';
		$sys_custom['KIS_Admission']['YLSYK']['Settings'] = true;
	}
	if($UserID == '26791'){ //for kis_mingwaipe
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'pe-mingwai.eclass.hk';
		$sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] = true;
	}
	if($UserID == '26792'){ //for kis_mingwainp
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'mingwai.eclass.hk';
		$sys_custom['KIS_Admission']['MINGWAI']['Settings'] = true;
	}
	if($UserID == '26798'){ //for kis_munsang
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'eclassk.munsang.edu.hk';
		$sys_custom['KIS_Admission']['MUNSANG']['Settings'] = true;
	}
	if($UserID == '26810'){ //for kis_ktlsks
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'ktlsks.eclasscloud.hk';
		$sys_custom['KIS_Admission']['KTLSKS']['Settings'] = true;
	}
	if($UserID == '26931'){ //for kis_sinmeng
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'sinmeng.eclass.hk';
		$sys_custom['KIS_Admission']['CSM']['Settings'] = true;
	}
	if($UserID == '27027'){ //for kis_shck
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'shck.eclass.hk';
		$sys_custom['KIS_Admission']['SHCK']['Settings'] = true;
	}
	if($UserID == '27028'){ //for kis_aogkg
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'aogkg.eclass.hk';
		$sys_custom['KIS_Admission']['AOGKG']['Settings'] = true;
	}
	if($UserID == '27029'){ //for kis_ssgc
	    resetAdmissionFlag();
		$setting_path_ip_rel = 'ssgc.eclass.hk';
		$sys_custom['KIS_Admission']['SSGC']['Settings'] = true;
	}
	if($UserID == '27077'){ //for kis_creative
	    resetAdmissionFlag();
        $kis_admission_school = $setting_path_ip_rel = 'creativekt.eclass.hk';
		$sys_custom['KIS_Admission']['CREATIVE']['Settings'] = true;
	}
}
//-- eAdmission Settings for 146 site only (do not upload to 149) [end]

if($stand_alone['eAdmission'])
{
	$kis_config['apps'][] = 'admission';
	$kis_config['apps'][] = 'schoolsettings';
	$kis_config['background'][kis::$user_types['teacher']] 	= 'teacher';
	$kis_config['apps'][] = 'accountmanage';
	
	$kis_config['blankphoto'] = '/images/kis/blank.jpg';
}
else
{
	// available apps of user types //
	$kis_config['apps'][] = 'accountmanage';	//Account Management
	$kis_config['apps'][] = 'borrowrecord';		//Borrow Record
	$kis_config['apps'][] = 'digitalarchive';	// Digital Archive
	$kis_config['apps'][] = 'digitalrouting';	// Digital Routing
	if(!$sys_custom['KIS_HideCampusMail']){
		$kis_config['apps'][] = 'message';			//Campus Mail
	}
	$kis_config['apps'][] = 'econtent';			//Content Resources
	
	if($plugin['eAdmission']){
		$kis_config['apps'][] = 'admission';	//eAdmission
	}
	
	$kis_config['apps'][] = 'eattendance';		//eAttendance
	$kis_config['apps'][] = 'staffattendance';	//eAttendance (Staff)
	$kis_config['apps'][] = 'ebooking';			//eBooking
	
	if ($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {
		$kis_config['apps'][] = 'eclassapp';	//eClass App
	}
	
	$kis_config['apps'][] = 'einventory';		//eInventory
	$kis_config['apps'][] = 'elibrary';			//eLibrary Plus
	$kis_config['apps'][] = 'enotice';			//eNotice
	$kis_config['apps'][] = 'epayment';			//ePayment
	$kis_config['apps'][] = 'pos';				//ePOS
	
	if ($plugin['eSchoolBus']) {
	    $kis_config['apps'][] = 'eschoolbus';	//eSchoolBus
	}
	
	$kis_config['apps'][] = 'iportfolio';		//iPortfolio
	
	// digital channels and album are mutual exclusive
	if ($plugin['DigitalChannels']) {
	    $kis_config['apps'][] = 'digitalchannels';	//Digital Channels
	}
	else if(!$sys_custom['KIS_HidePhotoAlbum']){
	    $kis_config['apps'][] = 'album';			//Photo Album
	}
	
	if ($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {
		$kis_config['apps'][] = 'pushmessage';	//Push Notification (App)
	}
	
	if(!$sys_custom['KIS_HideCalendar']){
		$kis_config['apps'][] = 'calendar';			//School Calendar
	}
	if(!$sys_custom['KIS_HideSchoolNews']){
		$kis_config['apps'][] = 'schoolnews';		//School News
	}
	$kis_config['apps'][] = 'schoolsettings';	//School Settings
	$kis_config['apps'][] = 'website';			//School Website
	$kis_config['apps'][] = 'shop';				//Shop
	$kis_config['apps'][] = 'sms';				//SMS Center
	
	if(!$sys_custom['kis_hide_worksheet'] && !($sys_custom['kis_worksheet_pdf_only'] && $_SESSION['UserType']!='1')){
		$kis_config['apps'][] = 'worksheets';	//worksheets
	}
	
	if($sys_custom['KIS_SchoolSettings']['EnableSubjectSetting'] && $sys_custom['KIS_eHomework']['EnableModule']) {
		$kis_config['apps'][] = 'homework';			// eHomework
	}
	
// 	if($plugin['eEnrollment'] && $_SESSION["USER_BASIC_INFO"]["is_class_teacher"] && $sys_custom['KIS_eEnrolment']['EnableModule'] || ($_SESSION['UserType']=='2' || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"])){
	if($plugin['eEnrollment'] && $sys_custom['KIS_eEnrolment']['EnableModule'] && ($_SESSION["USER_BASIC_INFO"]["is_class_teacher"] || $_SESSION['UserType']=='2' || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"] || ($_SESSION['UserType'] == USERTYPE_PARENT && !$sys_custom['eEnrolment']['HideEnrolForParentView']) 
	    || ($_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_enrol_admin"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_enrol_master"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_pic"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_activity_pic"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_helper"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_activity_helper"] )
	    )){
	    $kis_config['apps'][] = 'eenrolment';		//eEnrolment
 	}
 	
 	if(!$plugin['DisableCalendar'] && $sys_custom['KIS_iCalendar']['EnableModule']){
 	    $kis_config['apps'][] = 'icalendar';		//iCalendar
 	}
  
 	if(!$sys_custom['KIS_HideeCircular'] &&	$special_feature['circular'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || (!$_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] && ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] ||  $_SESSION['UserType']==USERTYPE_STAFF)))){
 	    	$kis_config['apps'][] = 'ecircular';	//eCircular
 	}
	
	// for parent only
	if ($plugin['AccountMgmt_StudentRegistry'] && $sys_custom['StudentRegistry']['Kentville'] && $_SESSION['ParentFillOnlineReg'] && $_SESSION['UserType']=='3') {
	    $kis_config['apps'][] = 'studentregistry';	//Student Registry
	}

	if ($plugin['StudentDataAnalysisSystem'] && $plugin['StudentDataAnalysisSystem_Style'] == "tungwah" && $plugin['SDAS_module']['KISMode']) {
	    $kis_config['apps'][] = 'cees';             //CEES (KIS version)
    }
    
    if ($plugin['PowerPortfolio']) {
    	$kis_config['apps'][] = 'powerportfolio';   //Power Portfolio for KIS
    }
	
	// econtents //
	$kis_config['econtent']['chinese']['strokecards'][] = array('title'=>'numbers', 'image'=>'card_02', 'href'=>'/kis/resources/econtent/chinese/strokecards/content_flash_card_chn.htm');
	$kis_config['econtent']['chinese']['strokecards'][] = array('title'=>'body', 	'image'=>'card_02');
	$kis_config['econtent']['chinese']['strokecards'][] = array('title'=>'family', 	'image'=>'card_02');
	$kis_config['econtent']['chinese']['strokecards'][] = array('title'=>'weather', 'image'=>'card_02');
	$kis_config['econtent']['chinese']['strokecards'][] = array('title'=>'sports', 	'image'=>'card_02');
	
	$kis_config['econtent']['english']['flashcards'][]  = array('title'=>'body',  	'image'=>'card_01', 'href'=>'/kis/resources/econtent/english/target_vocab/content_portal_eng.php');
	$kis_config['econtent']['english']['flashcards'][]  = array('title'=>'family',  	'image'=>'card_01');
	$kis_config['econtent']['english']['flashcards'][]  = array('title'=>'weather',  'image'=>'card_01');
	$kis_config['econtent']['english']['flashcards'][]  = array('title'=>'numbers',  'image'=>'card_01');
	$kis_config['econtent']['english']['flashcards'][]  = array('title'=>'sports',  	'image'=>'card_01');
	$kis_config['econtent']['english']['storybooks'][]  = array('title'=>'book_01', 	'image'=>'book_01');
	$kis_config['econtent']['english']['storybooks'][]  = array('title'=>'book_02', 	'image'=>'book_02');
	
	$kis_config['econtent']['math']['addition'][] 	    = array('title'=>'add_01', 'image'=>'math', 'href'=>'/kis/resources/econtent/math/num_calculate.html');
	$kis_config['econtent']['math']['subtraction'][] 	    = array('title'=>'sub_01', 'image'=>'math');
	$kis_config['econtent']['math']['addition'][] 	    = array('title'=>'add_02', 'image'=>'math');
	$kis_config['econtent']['math']['subtraction'][] 	    = array('title'=>'sub_02', 'image'=>'math');
						   
	$kis_config['econtent']['putonghua'] 		    = array();
	
	$kis_config['econtent']['others'] 		    = array();
	
	$kis_config['background'][kis::$user_types['parent']] 	= 'parent';
	$kis_config['background'][kis::$user_types['student']] 	= 'student';
	$kis_config['background'][kis::$user_types['teacher']] 	= 'teacher';
	
	$kis_config['iportfolio']['guardian']['quota'] 	= 2;
	
	$kis_config['blankphoto'] = '/images/kis/blank.jpg';
	
	$kis_config['eAdmission']['ReservedAttachmentNames'] = array('personal_photo','birth_cert','immunisation_record');
}
?>