<?php 
# using by Henry
/***************************************************************
*	modification log
*
*	2015-03-18 Henry
*	- added twins_array	
*
*	2015-03-17 Henry
*	- added filtering birthday range
*
*	2015-02-03 Henry
*	- support multi twins
*	- add sequence to ballot table
****************************************************************/
set_time_limit(120);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."kis/init.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
require_once($PATH_WRT_ROOT."includes/admission/sinmeng.eclass.hk/config.php");
require_once($PATH_WRT_ROOT."includes/admission/sinmeng.eclass.hk/libadmission_cust.php");
require_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");

intranet_auth();
intranet_opendb();

//$gpNum;
//$type;
//$YearID;

$libadmission_cust = new admission_cust();
$fcm = new form_class_manage();

$AcademicYearID = $_REQUEST['SchoolYearID']!=''?  $_REQUEST['SchoolYearID'] : $libadmission_cust->schoolYearID;
$SchoolYearID = $AcademicYearID;
$academic_year_obj  = new academic_year($AcademicYearID);
$academic_year_name = $academic_year_obj->YearNameB5;

if($type == '1'){ // full day
	$apply_day_type = '3';
}else if($type == '2'){ // AM 
	$apply_day_type = '1';
}else if($type == '3'){ // PM
	$apply_day_type = '2';
}

// Ballot Group Quota Info
$BALLOT_GROUPS = array(
					'3' => array( // assume only have three classes
								array('Total'=>20, 'Special'=>0, 'SpecialSpare'=>0, 'General'=>20, 'GeneralSpare'=>10), // BallotGroupIndex 0 嬰兒班
								array('Total'=>72, 'Special'=>0, 'SpecialSpare'=>0, 'General'=>72, 'GeneralSpare'=>20), // BallotGroupIndex 1 幼兒班1
								array('Total'=>38, 'Special'=>0, 'SpecialSpare'=>0, 'General'=>38, 'GeneralSpare'=>20) // BallotGroupIndex 2 幼兒班2
							), // Whole Day
					'1' => array(
								array('Total'=>30, 'General'=>30, 'GeneralSpare'=>15, 'GeneralSpareForAddr'=>15) // 幼兒班2
							), // Half Day AM
					'2' => array(
								array('Total'=>30, 'General'=>30, 'GeneralSpare'=>15, 'GeneralSpareForAddr'=>15) // 幼兒班2
							) // Half Day PM
				);
				
// ApplicationID in Ballot Group result
$BALLOT_APPLICANTS = array(
						'3' => array( 
									array('Total'=>array(), 'Special'=> array(), 'SpecialSpare'=> array(), 'General'=> array(), 'GeneralSpare'=>array(), 'GeneralSpareForAddr'=>array()), // BallotGroupIndex 0
									array('Total'=>array(), 'Special'=> array(), 'SpecialSpare'=> array(), 'General'=> array(), 'GeneralSpare'=>array(), 'GeneralSpareForAddr'=>array()), // BallotGroupIndex 1
									array('Total'=>array(), 'Special'=>array(), 'SpecialSpare'=>array(), 'General'=>array(), 'GeneralSpare'=>array(), 'GeneralSpareForAddr'=>array()) // BallotGroupIndex 2
								), // Whole Day
						'1' => array(
									array('Total'=>array(), 'General'=>array(), 'GeneralSpare'=>array(), 'GeneralSpareForAddr'=>array())
								), // Half Day AM
						'2' => array(
									array('Total'=>array(), 'General'=>array(), 'GeneralSpare'=>array(), 'GeneralSpareForAddr'=>array())
								) // Half Day PM
					);

//filter out the applicant out of the birthday range
$birthday_range = array(
						'1' => array('2019-10-01', '2020-03-31'),
						'2' => '',
						'3' => array('2018-01-01', '2018-12-31')
					);
//$birthday_range = array(
//						'1' => array('2014-05-01', '2014-10-31'),
//						'2' => '',
//						'3' => array('2013-01-01', '2013-04-30')
//					);

// -- customization for 幼兒班1 [start]
$split_two_group = array(
						array('Total'=>26, 'Special'=>0, 'SpecialSpare'=>0, 'General'=>26, 'GeneralSpare'=>5, 'From'=>'2019-06-01', 'To'=>'2019-09-30'), //first group
						array('Total'=>46, 'Special'=>0, 'SpecialSpare'=>0, 'General'=>46, 'GeneralSpare'=>15, 'From'=>'2019-01-01', 'To'=>'2019-05-31')//,  //second group
						//array('Total'=>7, 'Special'=>3, 'SpecialSpare'=>2, 'General'=>4, 'GeneralSpare'=>3, 'From'=>'2014-10-01', 'To'=>'2014-12-31')	  //third group
					);
// -- customization for 幼兒班1 [end]

$twins_array = array();

function insertBallotLog($SchoolYearID,$YearID,$ApplicationID,$BallotGroupType,$BallotGroupIndex,$SpecialNormal,$Status,$Sequence)
{
	global $libadmission_cust;
	$sql = "INSERT INTO ADMISSION_APPLICATION_BALLOT_LOG 
			(SchoolYearID,YearID,ApplicationID,BallotGroupType,BallotGroupIndex,SpecialNormal,Status,Sequence,DateInput,InputBy)
			 VALUES ('$SchoolYearID','$YearID','$ApplicationID','$BallotGroupType','$BallotGroupIndex','$SpecialNormal','$Status','$Sequence',NOW(),'".$_SESSION['UserID']."')";
	$insert_success = $libadmission_cust->db_db_query($sql);
	
	$sql = "UPDATE ADMISSION_APPLICATION_STATUS SET Status='$Status' WHERE SchoolYearID='$SchoolYearID' AND ApplicationID='$ApplicationID'";
	$update_success = $libadmission_cust->db_db_query($sql);
	
	return $insert_success && $update_success;
}


// Get year form 
$sql = "SELECT y.YearID 
		FROM YEAR as y 
		INNER JOIN YEAR_CLASS as yc ON yc.YearID=y.YearID 
		LEFT JOIN ADMISSION_OTHERS_INFO as i ON i.ApplyLevel=y.YearID AND i.ApplyYear=yc.AcademicYearID 
		WHERE yc.AcademicYearID='$AcademicYearID'
		GROUP BY y.YearID 
		ORDER BY y.Sequence";
$form_list = $libadmission_cust->returnResultSet($sql);
$form_list_count = count($form_list);

$yearIdToBallotGroupIndex = array();
$indexToYearId = array();
for($i=0;$i<$form_list_count;$i++){
	$yearIdToBallotGroupIndex[$form_list[$i]['YearID']] = $i;
	$indexToYearId[$i] = $form_list[$i]['YearID'];
}

$year_index = $yearIdToBallotGroupIndex[$YearID];
if($gpNum == '3' && ($type == '2' || $type == '3')){
	$year_index = '0';
}

$YEAR_WHOLE_DAY_QUOTA = $BALLOT_GROUPS[$apply_day_type][$year_index]['GeneralSpare'];



$allocated_application_id = array(); // applicants that get whole day slot
//$half_day_applicants = array(); // applicants that firt choice is not whole day or applicants that cannot get first choice of whole day 
//$half_day_application_id = array();
//$allocated_half_day_application_id = array();


// initialize ballot result with existing result
$sql = "SELECT * FROM ADMISSION_APPLICATION_BALLOT_LOG WHERE SchoolYearID='$SchoolYearID' AND YearID='$YearID'";
$records = $libadmission_cust->returnResultSet($sql);
$record_count = count($records);
$sequence = 1;

for($i=0;$i<$record_count;$i++){
	$application_id = $records[$i]['ApplicationID'];
	$ballot_group_type = $records[$i]['BallotGroupType'];
	$ballot_group_index = $records[$i]['BallotGroupIndex'];
	$special_normal = $records[$i]['SpecialNormal'];
	$status = $records[$i]['Status'];
	
	$field = '';
	if($special_normal == '1') // special
	{
		$field .= 'Special';
	}else{ // 2
		$field .= 'General';
	}
	if($status == $admission_cfg['Status']['reserve']){
		$field .= 'Spare';
		$BALLOT_APPLICANTS[$ballot_group_type][$ballot_group_index]['Total'][] = $application_id;
	}
	
	$BALLOT_APPLICANTS[$ballot_group_type][$ballot_group_index][$field][] = $application_id;
	if($ballot_group_type == $apply_day_type && $field == 'GeneralSpare'){
		$allocated_application_id[] = $application_id;
	}else if($ballot_group_type == '1' || $ballot_group_type == '2'){
		$half_day_application_id[] = $application_id;
		$allocated_half_day_application_id[] = $application_id;
	}
}

if($gpNum == '3' && ($type == '2' || $type == '3')){
	$first_choice = $type == '2'? '1' : '2';
	// AM or PM
	$choice_condition = "AND (s.DOB >= '".$birthday_range[3][0]."' AND s.DOB <= '".$birthday_range[3][1]."') AND s.Address IN (1,2) limit ".$BALLOT_GROUPS[$apply_day_type][$year_index]['GeneralSpareForAddr']." ";//AND s.Address IN (1,2)
	$choice_condition2 = "AND (s.DOB >= '".$birthday_range[3][0]."' AND s.DOB <= '".$birthday_range[3][1]."') AND s.Address NOT IN (1,2) ";
	
	$choice_condition_ary = array(" AND o.ApplyDayType1='$first_choice' ".$choice_condition,
								" AND o.ApplyDayType1='$first_choice' ".$choice_condition2,
								" AND o.ApplyDayType1<>'".($type == '2'?'2':'1')."' AND o.ApplyDayType2='$first_choice' ".$choice_condition,
								" AND o.ApplyDayType1<>'".($type == '2'?'2':'1')."' AND o.ApplyDayType2='$first_choice' ".$choice_condition2,
								" AND o.ApplyDayType1<>'".($type == '2'?'2':'1')."' AND o.ApplyDayType3='$first_choice' ".$choice_condition,
								" AND o.ApplyDayType1<>'".($type == '2'?'2':'1')."' AND o.ApplyDayType3='$first_choice' ".$choice_condition2,
								" AND o.ApplyDayType1<>'".($type == '2'?'2':'1')."' ");
}else{
	//--filtering birthday range [start]
	$choice_condition = '';
	if($gpNum == '1' && $birthday_range[1]){
		$choice_condition = "AND (s.DOB >= '".$birthday_range[1][0]."' AND s.DOB <= '".$birthday_range[1][1]."') AND s.Address IN (1) ";
		$choice_condition2 = "AND (s.DOB >= '".$birthday_range[1][0]."' AND s.DOB <= '".$birthday_range[1][1]."') AND s.Address IN (2) ";
	}
	else if($gpNum == '2' && $birthday_range[2]){
		$choice_condition = "AND (s.DOB >= '".$birthday_range[2][0]."' AND s.DOB <= '".$birthday_range[2][1]."') AND s.Address IN (1) ";
		$choice_condition2 = "AND (s.DOB >= '".$birthday_range[2][0]."' AND s.DOB <= '".$birthday_range[2][1]."') AND s.Address  IN (2) ";
	}
	else if($gpNum == '3' && $birthday_range[3]){
		$choice_condition = "AND (s.DOB >= '".$birthday_range[3][0]."' AND s.DOB <= '".$birthday_range[3][1]."') AND s.Address IN (1) ";
		$choice_condition2 = "AND (s.DOB >= '".$birthday_range[3][0]."' AND s.DOB <= '".$birthday_range[3][1]."') AND s.Address IN (2) ";
	}
	//--filtering birthday range [end]
	
	$first_choice = '3';// full day
	$choice_condition_ary = array(" AND o.ApplyDayType1='$first_choice' ".$choice_condition," AND o.ApplyDayType1='$first_choice' ".$choice_condition2," AND o.ApplyDayType2='$first_choice' ".$choice_condition," AND o.ApplyDayType2='$first_choice' ".$choice_condition2," AND o.ApplyDayType3='$first_choice' ".$choice_condition," AND o.ApplyDayType3='$first_choice' ".$choice_condition2," ");
	
	// handle 幼兒班1 3 group with address proity
	if($gpNum == '2' && $split_two_group){
		$choice_condition = "AND (s.DOB >= '".$split_two_group[0]['From']."' AND s.DOB <= '".$split_two_group[0]['To']."') AND s.Address IN (1) ";//AND s.Address IN (1,2)
		$choice_condition2 = "AND (s.DOB >= '".$split_two_group[1]['From']."' AND s.DOB <= '".$split_two_group[1]['To']."') AND s.Address IN (1) ";//AND s.Address IN (1,2)
		$choice_condition3 = "AND (s.DOB >= '".$split_two_group[0]['From']."' AND s.DOB <= '".$split_two_group[0]['To']."') AND s.Address NOT IN (1) ";
//		$choice_condition3 = "AND (s.DOB >= '".$split_two_group[0]['From']."' AND s.DOB <= '".$split_two_group[0]['To']."') AND s.Address NOT IN (1) ";
		$choice_condition4 = "AND (s.DOB >= '".$split_two_group[1]['From']."' AND s.DOB <= '".$split_two_group[1]['To']."') AND s.Address NOT IN (1) ";
		//$choice_condition6 = "AND (s.DOB >= '".$split_two_group[2]['From']."' AND s.DOB <= '".$split_two_group[2]['To']."') ";
	
		$choice_condition_ary = array(" AND o.ApplyDayType1='$first_choice' ".$choice_condition, 
									" AND o.ApplyDayType1='$first_choice' ".$choice_condition2, 
									" AND o.ApplyDayType1='$first_choice' ".$choice_condition3, 
									" AND o.ApplyDayType1='$first_choice' ".$choice_condition4, 
									" AND o.ApplyDayType2='$first_choice' ".$choice_condition, 
									" AND o.ApplyDayType2='$first_choice' ".$choice_condition2,
									" AND o.ApplyDayType2='$first_choice' ".$choice_condition3, 
									" AND o.ApplyDayType2='$first_choice' ".$choice_condition4,  
									" AND o.ApplyDayType3='$first_choice' ".$choice_condition,
									" AND o.ApplyDayType3='$first_choice' ".$choice_condition2,
									" AND o.ApplyDayType3='$first_choice' ".$choice_condition3,
									" AND o.ApplyDayType3='$first_choice' ".$choice_condition4," ");
	
	}
}

$pre_allocated_application_id_count = 0;

foreach($choice_condition_ary as $k => $condition)
{
	// handle 幼兒班1 3 group with address proity
	if($gpNum == '2'){
		/*if($k == 4){
			$YEAR_WHOLE_DAY_QUOTA = $split_two_group[2]['SpecialSpare'] + count($allocated_application_id);
		}
		else if($k == 2){
			$YEAR_WHOLE_DAY_QUOTA = $split_two_group[1]['SpecialSpare'] + count($allocated_application_id);
		}
		else if($k == 5){
			$YEAR_WHOLE_DAY_QUOTA = $split_two_group[2]['SpecialSpare'] + count($allocated_application_id) - $pre_allocated_application_id_count;
		}
		else if($k == 3){
			$YEAR_WHOLE_DAY_QUOTA = $split_two_group[1]['SpecialSpare'] + count($allocated_application_id) - $pre_allocated_application_id_count;
		}
		else*/ 
		if($k == 1){
			$YEAR_WHOLE_DAY_QUOTA = $split_two_group[1]['GeneralSpare'] + count($allocated_application_id);
			$allocatedGroup[0] += count($allocated_application_id) - $pre_allocated_application_id_count;
		}
		else if($k == 0){
			$YEAR_WHOLE_DAY_QUOTA = $split_two_group[0]['GeneralSpare'];
		}
//		else if($k == 2 || $k == 5 || $k == 8){
//			$YEAR_WHOLE_DAY_QUOTA = $split_two_group[2]['General'] - $allocatedGroup[2] + count($allocated_application_id) /*- $pre_allocated_application_id_count*/;
//			$allocatedGroup[1] += count($allocated_application_id) - $pre_allocated_application_id_count;
//		}
		else if($k == 1 || $k == 3 || $k == 5){
			$YEAR_WHOLE_DAY_QUOTA = $split_two_group[1]['GeneralSpare'] - $allocatedGroup[1] + count($allocated_application_id) /*- $pre_allocated_application_id_count*/;
			$allocatedGroup[0] += count($allocated_application_id) - $pre_allocated_application_id_count;
		}
		else if($k == 0 || $k == 2 || $k == 4){
			$YEAR_WHOLE_DAY_QUOTA = $split_two_group[0]['GeneralSpare'] - $allocatedGroup[0] + count($allocated_application_id) /*- $pre_allocated_application_id_count*/;
			$allocatedGroup[1] += count($allocated_application_id) - $pre_allocated_application_id_count;
		}
		else{
			$YEAR_WHOLE_DAY_QUOTA = $BALLOT_GROUPS[$apply_day_type][$year_index]['GeneralSpare'];
		}
		
	}
//	if($gpNum == '2')
		$pre_allocated_application_id_count = count($allocated_application_id);
	
	$sql = "SELECT 
				s.ApplicationID,
				s.ChineseName,
				s.EnglishName,
				s.IsTwins,
				s.IsTwinsApplied,
				s.TwinsApplicationID,
				p.lsSingleParents,
				o.ApplyDayType1,
				o.ApplyDayType2,
				o.ApplyDayType3,
				o.ApplyLevel,
				a.Status 
			FROM ADMISSION_STU_INFO as s 
			INNER JOIN ADMISSION_PG_INFO as p ON p.ApplicationID=s.ApplicationID 
			INNER JOIN ADMISSION_OTHERS_INFO as o ON o.ApplicationID=s.ApplicationID 
			LEFT JOIN ADMISSION_APPLICATION_STATUS as a ON a.ApplicationID=s.ApplicationID 
			WHERE o.ApplyYear='$SchoolYearID' AND o.ApplyLevel='$YearID' AND a.Status='".$admission_cfg['Status']['pending']."' ";
	/*
	if($gpNum == '3' && ($type == '2' || $type == '3')){
		$first_choice = $type == '2'? '1' : '2';
		$sql.=" AND o.ApplyDayType1='$first_choice' ";	// AM or PM
	}else{
		$sql.=" AND o.ApplyDayType1='3' ";	// full day
	}
	*/
	$conditionArr = explode("limit ",$condition);
	$sql.= $conditionArr[0];
	$sql.= "GROUP BY s.ApplicationID 
			ORDER BY s.ApplicationID";
	if($conditionArr[1]){
		$sql.= " limit ".$conditionArr[1];
	}
	$applicant_list = $libadmission_cust->returnResultSet($sql);
	$applicant_count = count($applicant_list);
	
	// draw first choice
	while( count($BALLOT_APPLICANTS[$apply_day_type][$year_index]['GeneralSpare']) < $BALLOT_GROUPS[$apply_day_type][$year_index]['GeneralSpare'] && count($allocated_application_id) < ($applicant_count + $pre_allocated_application_id_count) && count($allocated_application_id) < $YEAR_WHOLE_DAY_QUOTA){ // loop until all applicants are allocated AND there are still empty slots
		// pick up a applicant
		$rand_index = mt_rand(0,$applicant_count-1); // mt_rand() is boundary inclusive
		$application_id = $applicant_list[$rand_index]['ApplicationID'];
		
		if(in_array($application_id,$allocated_application_id) || !$application_id){
			continue;
		}
		
		$status = $applicant_list[$rand_index]['Status']; 
		$year_id = $applicant_list[$rand_index]['ApplyLevel'];
		//$apply_day_type = $applicant_list[$rand_index][$apply_day_type_field];
		//$apply_day_type = '3';
		$is_single_parent = $applicant_list[$rand_index]['lsSingleParents'] == 'Y';
		$is_twins = $applicant_list[$rand_index]['IsTwins'] == 'Y' && $applicant_list[$rand_index]['IsTwinsApplied'] == 'Y' && $applicant_list[$rand_index]['TwinsApplicationID']!='';
		$twins_application_id = $applicant_list[$rand_index]['TwinsApplicationID'];
		
		//code her for searching the twins application id [Start]
		$twins_application_id = array();
		if($applicant_list[$rand_index]['TwinsApplicationID']){
			$twins_application_id[] = $applicant_list[$rand_index]['TwinsApplicationID'];
		}
		$shouldLoop = 1;
		$loopLimit = 0;
		while($shouldLoop && $loopLimit < 1000){
			$shouldLoop = 0;
			$loopLimit++;
			for($i=0; $i < $applicant_count; $i++){
				if($applicant_list[$rand_index]['TwinsApplicationID'] == $applicant_list[$i]['ApplicationID'] && $applicant_list[$i]['TwinsApplicationID'] && !in_array($applicant_list[$i]['TwinsApplicationID'],$twins_application_id)){
					$twins_application_id[] = $applicant_list[$i]['TwinsApplicationID'];
					
					unset($applicant_list[$i]);
				}
				if($applicant_list[$i]['TwinsApplicationID'] == $applicant_list[$rand_index]['ApplicationID'] && $applicant_list[$i]['ApplicationID'] && !in_array($applicant_list[$i]['ApplicationID'],$twins_application_id)){
					$twins_application_id[] = $applicant_list[$i]['ApplicationID'];
					$is_twins = $applicant_list[$i]['IsTwins'] == 'Y' && $applicant_list[$i]['IsTwinsApplied'] == 'Y' && $applicant_list[$i]['TwinsApplicationID']!='';
				
					$applicant_list[$rand_index]['ApplicationID'] = $applicant_list[$i]['ApplicationID'];
					$shouldLoop = 1;
					unset($applicant_list[$i]);
				}
			}
		}
		//code her for searching the twins application id [End]
		
		//if($apply_day_type == '3'){ // first choice is WHOLE DAY
		
		$ballot_group_index = $yearIdToBallotGroupIndex[$year_id];
		if($gpNum == '3' && ($type == '2' || $type == '3')){
			$ballot_group_index = '0';
		}
		
		if(isset($BALLOT_GROUPS[$apply_day_type][$ballot_group_index])){
			
				// general
				
				$max_general = $BALLOT_GROUPS[$apply_day_type][$ballot_group_index]['General'];
				$max_general_spare = $BALLOT_GROUPS[$apply_day_type][$ballot_group_index]['GeneralSpare'];
				$general_arr = $BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['General'];
				$general_spare_arr = $BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['GeneralSpare'];
				
				//if(!$is_single_parent){
					if(count($general_spare_arr) < $max_general_spare && !in_array($application_id,$general_spare_arr)){
						// no more general slots but have spare general slots
						$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['GeneralSpare'][] = $application_id;
						
						// process twins together
	//					if($is_twins && !in_array($twins_application_id, $allocated_application_id)){
	//						$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['GeneralSpare'][] = $twins_application_id;
	//						$allocated_application_id[] = $twins_application_id;
	//						insertBallotLog($SchoolYearID,$year_id,$twins_application_id,$apply_day_type,$ballot_group_index,'2',$admission_cfg['Status']['reserve']);
	//					}
						
						if($is_twins)
							$twins_array[] = $application_id;
						
						$allocated_application_id[] = $application_id;
						insertBallotLog($SchoolYearID,$year_id,$application_id,$apply_day_type,$ballot_group_index,'2',$admission_cfg['Status']['reserve'],$sequence++);
					
						for($i=0; $i < count($twins_application_id); $i++){
							if(/*$is_twins && */!in_array($twins_application_id[$i], $allocated_application_id)){
								$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['GeneralSpare'][] = $twins_application_id[$i];
								$allocated_application_id[] = $twins_application_id[$i];
								insertBallotLog($SchoolYearID,$year_id,$twins_application_id[$i],$apply_day_type,$ballot_group_index,'2',$admission_cfg['Status']['reserve'],$sequence++);
							
								$twins_array[] = $twins_application_id[$i];
							}
						}
					}
				//}
				
		}
			
		usleep(10);
	}
}

$js_draw .= "var draw_page_num = 0;\n";
$js_draw .= "var draw_arr = [];\n";
$draw_count = count($BALLOT_APPLICANTS[$apply_day_type][$year_index]['GeneralSpare']);
for($i=0;$i<$draw_count;$i++){
	$js_draw .= "draw_arr.push('"/*.(in_array($BALLOT_APPLICANTS[$apply_day_type][$year_index]['GeneralSpare'][$i], $twins_array)?'<font size="5">*</font>':"")*/.$BALLOT_APPLICANTS[$apply_day_type][$year_index]['GeneralSpare'][$i]."');\n";
}

intranet_closedb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: 善明托兒所  <?=$academic_year_name?> 收生抽籤 ::</title>
<link href="css/content.css" rel="stylesheet" type="text/css" />
<script src="script/action.js" type="text/JavaScript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
</head>

<script language="JavaScript" type="text/JavaScript">
/*
gpNum : 1=baby , 2=child_1 , 3=child_2
type  : 1=fullday , 2=morning , 3=afternoon
part  : 1=special regular , 2=special backup , 3=normal regular , 4=normal backup
showType : "firstShow"=balloon break , "continue"=no balloon
isEnd : true=show EndBtn , false=show NextBtn

Use Set_Gp_Type_Part() to input the group and type, which user selected at page_3
Part is only for this page,
each group_type has 4 parts.
*/

<?=$js_draw?>
var draw_array = [];

function NextDrawPage()
{
	$('div.draw_obj').remove();
	
	draw_page_num += 1;
	var sub_arr = draw_array[draw_page_num];
	SetShowGp(sub_arr);
	if(draw_page_num == draw_array.length-1){
		$('#endGp').show();
		$('#nextGp').hide();
	}
}

function PrevDrawPage()
{
	$('div.draw_obj').remove();
	
	draw_page_num -= 1;
	var sub_arr = draw_array[draw_page_num];
	SetShowGp(sub_arr);
	if(draw_page_num == 0){
		$('#endGp').hide();
	}
}

$(document).ready( function() {
	Set_Gp_Type_Part(<?=$gpNum?>, <?=$type?>, 4);
	SetPartTotal("<?=count($BALLOT_APPLICANTS[$apply_day_type][$year_index]['GeneralSpare'])?>");
	
	/* This array could have 1~8 numbers.
		if more than 8 numbers, please use Next button and
		show the others at the next page
		next page do NOT need break balloon.
		
		When one part is shown, use Next button again, to show the next part.
		Next part need to show break balloon.
		Until 4 parts are shown, use End button.
		End button will go to page_3, and user will select the next group and type.
	*/
	var showArr = [];
	SetShowGp(showArr);
	SetShowType("firstShow", true);
	
	var draw_sub_array = [];
	for(var i=0;i<draw_arr.length;i++){
		draw_sub_array.push(draw_arr[i]);
		if(draw_sub_array.length >= 8 || i == (draw_arr.length-1)){
			draw_array.push(draw_sub_array);
			draw_sub_array = [];
		}
	}
	draw_page_num = 0;
	$('div.draw_obj').remove();
	if(draw_array.length > 0){
		SetShowGp(draw_array[draw_page_num]);
	}
	if(draw_array.length > 1){
		$('#endGp').hide();
		$('#nextGp').show();
	}
});

function CallNext() {
	/* your next button action here */
	//ToPage("page_6_demo_5");
	if(draw_page_num < (draw_array.length-1)){
		// flip to next page
		NextDrawPage();
	}
}

function CallBack() {
	/* your back button action here */
	//ToPage('page_3');
	var formObj = document.getElementById('form1');
	formObj.action = 'step3.php';
	formObj.submit();
}

</script>

<body>
<form id="form1" name="form1" method="post" action="">
<div class="page6_bg bg">
	<div id="part" class="p6_part">
		<div id="part_num" class="text"></div>
	</div>
	<div id="type" class="p6_type"></div>
	<div id="gp" class="p6_gp"></div>
	<div id="info_gp" class="p6_info_gp">
		<div id="showGp" class="draw_gp">
			<!--
			--------- generate at function SetShowGp() ---------
			<div class="draw_obj">
				<div class="text">csm14a0012</div>
			</div>
			<div class="draw_obj">
				<div class="text">csm14a0012</div>
			</div>
			<div class="draw_obj">
				<div class="text">csm14a0012</div>
			</div>
			<div class="draw_obj">
				<div class="text">csm14a0012</div>
			</div>
			<div class="draw_obj">
				<div class="text">csm14a0012</div>
			</div>
			<div class="draw_obj">
				<div class="text">csm14a0012</div>
			</div>
			<div class="draw_obj">
				<div class="text">csm14a0012</div>
			</div>
			<div class="draw_obj">
				<div class="text">csm14a0012</div>
			</div>
			-->
		</div>
		
		<div id="endGp" class="end_gp" style="display:none">
			<div class="end_line"></div>
			<div class="btn_back" onClick="CallBack()"></div>
		</div>
		
		<div id="nextGp" class="end_gp" style="display:none">
			<div class="btn_next" onClick="CallNext()"></div>
		</div>
	</div>
	<div id="balloonGp" class="p6_fx_gp">
		<div id="ballNormal" class="p6_balloon normal t1"></div>
		<div id="ballBreak" class="p6_balloon break t1"></div>
		<div id="bee" class="p6_bee stay"></div>
	</div>
	<div id="btn_area" class="click_area" onclick="DoBreak()"></div>
</div>
<input type="hidden" id="gpNum" name="gpNum" value="<?=$gpNum?>" />
<input type="hidden" id="type" name="type" value="<?=$type?>" />
<input type="hidden" id="SchoolYearID" name="SchoolYearID" value="<?=$AcademicYearID?>" />
<input type="hidden" id="YearID" name="YearID" value="<?=$YearID?>" />
</form>
</body>
</html>