<?php 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."kis/init.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
require_once($PATH_WRT_ROOT."includes/admission/sinmeng.eclass.hk/libadmission_cust.php");
require_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");

intranet_auth();
intranet_opendb();

$libadmission_cust = new admission_cust();
$fcm = new form_class_manage();

$AcademicYearID = $libadmission_cust->schoolYearID;
$SchoolYearID = $AcademicYearID;
$academic_year_obj  = new academic_year($AcademicYearID);
$academic_year_name = $academic_year_obj->YearNameB5;

$create_table_sql = "CREATE TABLE IF NOT EXISTS ADMISSION_APPLICATION_BALLOT_LOG (
						RecordID int(11) NOT NULL auto_increment,
						SchoolYearID int(11) NOT NULL,
						YearID int(11) NOT NULL,
						ApplicationID varchar(100) NOT NULL,
						BallotGroupType tinyint COMMENT '3:Whole Day,1:AM,2:PM', 
						BallotGroupIndex tinyint COMMENT '0:First class, 1:Second class, 2:Third class',
						SpecialNormal tinyint COMMENT '1:Special, 2:Normal',
						Status tinyint, 
						DateInput datetime,
						InputBy int(11),
						PRIMARY KEY (RecordID),
						INDEX ISchoolYearID(SchoolYearID),
						INDEX IYearID(YearID),
						INDEX IApplicationID(ApplicationID) 
					) ENGINE=InnoDB DEFAULT CHARSET=utf8";				
$libadmission_cust->db_db_query($create_table_sql);

// Get year form 
$sql = "SELECT y.YearID 
		FROM YEAR as y 
		INNER JOIN YEAR_CLASS as yc ON yc.YearID=y.YearID 
		LEFT JOIN ADMISSION_OTHERS_INFO as i ON i.ApplyLevel=y.YearID AND i.ApplyYear=yc.AcademicYearID 
		WHERE yc.AcademicYearID='$AcademicYearID'
		GROUP BY y.YearID 
		ORDER BY y.Sequence";
$form_list = $libadmission_cust->returnResultSet($sql);
$form_list_count = count($form_list);

$yearIdToBallotGroupIndex = array();
$indexToYearId = array();
for($i=0;$i<$form_list_count;$i++){
	$yearIdToBallotGroupIndex[$form_list[$i]['YearID']] = $i;
	$indexToYearId[$i] = $form_list[$i]['YearID'];
}

// ApplicationID in Ballot Group result
$BALLOT_APPLICANTS = array(
						'3' => array( 
									array('Total'=>array(), 'Special'=> array(), 'SpecialSpare'=> array(), 'General'=> array(), 'GeneralSpare'=>array()), // BallotGroupIndex 0
									array('Total'=>array(), 'Special'=> array(), 'SpecialSpare'=> array(), 'General'=> array(), 'GeneralSpare'=>array()), // BallotGroupIndex 1
									array('Total'=>array(), 'Special'=>array(), 'SpecialSpare'=>array(), 'General'=>array(), 'GeneralSpare'=>array()) // BallotGroupIndex 2
								), // Whole Day
						'1' => array(
									array('Total'=>array(), 'General'=>array(), 'GeneralSpare'=>array())
								), // Half Day AM
						'2' => array(
									array('Total'=>array(), 'General'=>array(), 'GeneralSpare'=>array())
								) // Half Day PM
					);
					
$sql = "SELECT * FROM ADMISSION_APPLICATION_BALLOT_LOG WHERE SchoolYearID='$SchoolYearID'";
$records = $libadmission_cust->returnResultSet($sql);
$record_count = count($records);
for($i=0;$i<$record_count;$i++){
	$application_id = $records[$i]['ApplicationID'];
	$ballot_group_type = $records[$i]['BallotGroupType'];
	$ballot_group_index = $records[$i]['BallotGroupIndex'];
	$special_normal = $records[$i]['SpecialNormal'];
	$status = $records[$i]['Status'];
	
	$field = '';
	if($special_normal == '1') // special
	{
		$field .= 'Special';
	}else{ // 2
		$field .= 'General';
	}
	if($status == $admission_cfg['Status']['reserve']){
		$field .= 'Spare';
		$BALLOT_APPLICANTS[$ballot_group_type][$ballot_group_index]['Total'][] = $application_id;
	}
	
	$BALLOT_APPLICANTS[$ballot_group_type][$ballot_group_index][$field][] = $application_id;
}

$js_disable = '';
if(count($BALLOT_APPLICANTS['3']['0']['Special'])>0 || count($BALLOT_APPLICANTS['3']['0']['SpecialSpare'])>0 || 
	count($BALLOT_APPLICANTS['3']['0']['General'])>0 || count($BALLOT_APPLICANTS['3']['0']['GeneralSpare'])>0)
{
	$js_disable .= 'DisableBtn(1, 1);'."\n";
}

if(count($BALLOT_APPLICANTS['3']['1']['Special'])>0 || count($BALLOT_APPLICANTS['3']['1']['SpecialSpare'])>0 || 
	count($BALLOT_APPLICANTS['3']['1']['General'])>0 || count($BALLOT_APPLICANTS['3']['1']['GeneralSpare'])>0)
{
	$js_disable .= 'DisableBtn(2, 1);'."\n";
}

if(count($BALLOT_APPLICANTS['3']['2']['Special'])>0 || count($BALLOT_APPLICANTS['3']['2']['SpecialSpare'])>0 || 
	count($BALLOT_APPLICANTS['3']['2']['General'])>0 || count($BALLOT_APPLICANTS['3']['2']['GeneralSpare'])>0)
{
	$js_disable .= 'DisableBtn(3, 1);'."\n";
}

if(count($BALLOT_APPLICANTS['1']['0']['General'])>0 || count($BALLOT_APPLICANTS['1']['0']['GeneralSpare'])>0)
{
	$js_disable .= 'DisableBtn(3, 2);'."\n";
}

if(count($BALLOT_APPLICANTS['2']['0']['General'])>0 || count($BALLOT_APPLICANTS['2']['0']['GeneralSpare'])>0)
{
	$js_disable .= 'DisableBtn(3, 3);'."\n";
}

intranet_closedb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: 善明托兒所 <?=$academic_year_name?> 收生抽籤 ::</title>
<link href="css/content.css" rel="stylesheet" type="text/css" />
<script src="script/action.js" type="text/JavaScript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
</head>

<script language="JavaScript" type="text/JavaScript">
/*
gpNum : 1=baby , 2=child_1 , 3=child_2
type  : 1=fullday , 2=morning , 3=afternoon

Please disable the button which user have already clicked once.
*/
$(document).ready( function() {
<?=$js_disable?>
});

function GotoNext(gpNum, type, yearID) {
	/* your group_type button action here */
	//console.log(gpNum, type);
	//ToPage("page_4");
	$('#gpNum').val(gpNum);
	$('#type').val(type);
	$('#YearID').val(yearID);
	var formObj =  document.getElementById('form1');
	//formObj.action = 'step4.php';
	formObj.submit();
}

</script>

<body>
<form id="form1" name="form1" method="post" action="step4.php">
<div class="page3_bg">
	<div class="p3_select_gp posL posV1">
		<btngp id="btn_1_1" onClick="GotoNext(1, 1, <?=$indexToYearId[0]?>)"><div class="fullday"></div></btngp>
	</div>
	<div class="p3_select_gp posM posV1">
		<btngp id="btn_2_1" onClick="GotoNext(2, 1, <?=$indexToYearId[1]?>)"><div class="fullday"></div></btngp>
	</div>
	<div class="p3_select_gp posR">
		<btngp id="btn_3_1" onClick="GotoNext(3, 1, <?=$indexToYearId[2]?>)"><div class="fullday"></div></btngp>
	</div>
</div>
<input type="hidden" id="gpNum" name="gpNum" value="" />
<input type="hidden" id="type" name="type" value="" />
<input type="hidden" id="SchoolYearID" name="SchoolYearID" value="<?=$AcademicYearID?>" />
<input type="hidden" id="YearID" name="YearID" value="" />
</form>
</body>
</html>