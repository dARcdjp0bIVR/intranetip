<?php 
# using by Henry
/***************************************************************
*	modification log
*
*	2015-03-17 Henry
*	- exclude the cancelled records
*
*	2015-02-03 Henry
*	- modified 
****************************************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."kis/init.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
require_once($PATH_WRT_ROOT."includes/admission/sinmeng.eclass.hk/libadmission_cust.php");
require_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");

intranet_auth();
intranet_opendb();

//$gpNum;
//$type;
//$YearID;

$libadmission_cust = new admission_cust();
$fcm = new form_class_manage();

$AcademicYearID = $_REQUEST['SchoolYearID']!=''?  $_REQUEST['SchoolYearID'] : $libadmission_cust->schoolYearID;
$academic_year_obj  = new academic_year($AcademicYearID);
$academic_year_name = $academic_year_obj->YearNameB5;

$sql = "SELECT COUNT(DISTINCT i.RecordID) as NumberOfApplicant  
		FROM YEAR as y 
		INNER JOIN YEAR_CLASS as yc ON yc.YearID=y.YearID 
		LEFT JOIN ADMISSION_OTHERS_INFO as i ON i.ApplyLevel=y.YearID AND i.ApplyYear=yc.AcademicYearID 
		LEFT JOIN ADMISSION_APPLICATION_STATUS as s ON s.ApplicationID = i.ApplicationID
		WHERE yc.AcademicYearID='$AcademicYearID' AND y.YearID='$YearID' AND s.Status<>'5'
		GROUP BY y.YearID 
		ORDER BY y.Sequence";

$record = $libadmission_cust->returnResultSet($sql);
$number_of_applicant = $record[0]['NumberOfApplicant'];

//Get the number of application that is drawn before
$sql = "SELECT COUNT(*) as NumberOfApplicant FROM ADMISSION_APPLICATION_BALLOT_LOG WHERE SchoolYearID='$SchoolYearID' AND YearID='$YearID'";
$drawnCount = $libadmission_cust->returnResultSet($sql);
$number_of_applicant_drawn = $drawnCount[0]['NumberOfApplicant'];
$number_of_applicant_not_drawn = $number_of_applicant - $number_of_applicant_drawn;

// Get year form 
$sql = "SELECT y.YearID 
		FROM YEAR as y 
		INNER JOIN YEAR_CLASS as yc ON yc.YearID=y.YearID 
		LEFT JOIN ADMISSION_OTHERS_INFO as i ON i.ApplyLevel=y.YearID AND i.ApplyYear=yc.AcademicYearID 
		WHERE yc.AcademicYearID='$SchoolYearID'
		GROUP BY y.YearID 
		ORDER BY y.Sequence";
$form_list = $libadmission_cust->returnResultSet($sql);
$form_list_count = count($form_list);

$yearIdToBallotGroupIndex = array();
$indexToYearId = array();
for($i=0;$i<$form_list_count;$i++){
	$yearIdToBallotGroupIndex[$form_list[$i]['YearID']] = $i;
	$indexToYearId[$i] = $form_list[$i]['YearID'];
}

intranet_closedb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: 善明托兒所 <?=$academic_year_name?> 收生抽籤 ::</title>
<link href="css/content.css" rel="stylesheet" type="text/css" />
<script src="script/action.js" type="text/JavaScript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
</head>

<script language="JavaScript" type="text/JavaScript">
/*
gpNum : 1=baby , 2=child_1 , 3=child_2
type  : 1=fullday , 2=morning , 3=afternoon

Use SetText() to input the big number.
Use Set_Gp_Type() to input the group and type, which user selected at page_3
*/
$(document).ready( function() {
	SetText(<?=$number_of_applicant_not_drawn?>);
	Set_Gp_Type(<?=$gpNum?>, <?=$type?>);
});

function CallContinue() {
	/* your continue button action here */
	//ToPage("page_5");
	document.getElementById('form1').submit();
}

</script>

<body>
<form id="form1" name="form1" method="post" action="step5.php">
<div class="page4_bg">
	<div class="p4_img_gp">
		<div id="type" class="p4_type"></div>
		<div id="gp" class="p4_gp"></div>
	</div>
	<div class="p4_pos_shift">
		<div class="p2_text_person"></div>
		<div class="p2_cloud"></div>
		<div class="count_text">
			<div id="text_4" class="t4">4</div>
			<div id="text_3" class="t3">5</div>
			<div id="text_2" class="t2">7</div>
			<div id="text_1" class="t1">0</div>
		</div>
	</div>
	<div class="btn_contin basicBtn" onClick="CallContinue()"></div>
</div>
<input type="hidden" id="gpNum" name="gpNum" value="<?=$gpNum?>" />
<input type="hidden" id="type" name="type" value="<?=$type?>" />
<input type="hidden" id="SchoolYearID" name="SchoolYearID" value="<?=$AcademicYearID?>" />
<input type="hidden" id="YearID" name="YearID" value="<?=$YearID?>" />
</form>
</body>
</html>