<?php
// Editing by 
/***************************************************************
*	modification log
*
*	2015-03-19 Henry
*	- exclude the cancelled records
*
****************************************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."kis/init.php");
//include_once($PATH_WRT_ROOT."includes/global.php");
//include_once($PATH_WRT_ROOT."includes/libdb.php");
require_once($PATH_WRT_ROOT."includes/admission/sinmeng.eclass.hk/libadmission_cust.php");
//include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
//include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
require_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");

intranet_auth();
intranet_opendb();

$libadmission_cust = new admission_cust();
$li = new libdb();
/*
$create_table_sql = "CREATE TABLE IF NOT EXISTS ADMISSION_APPLICATION_BALLOT_LOG (
						RecordID int(11) NOT NULL auto_increment,
						SchoolYearID int(11) NOT NULL,
						YearID int(11) NOT NULL,
						ApplicationID varchar(100) NOT NULL,
						BallotGroupType tinyint COMMENT '3:Whole Day,1:AM,2:PM', 
						BallotGroupIndex tinyint COMMENT '0:First class, 1:Second class, 2:Third class',
						SpecialNormal tinyint COMMENT '1:Special, 2:Normal',
						Status tinyint, 
						DateInput datetime,
						InputBy int(11),
						PRIMARY KEY (RecordID),
						INDEX ISchoolYearID(SchoolYearID),
						INDEX IYearID(YearID),
						INDEX IApplicationID(ApplicationID) 
					) ENGINE=InnoDB DEFAULT CHARSET=utf8";				
$li->db_db_query($create_table_sql);
*/

//$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
$AcademicYearID = $libadmission_cust->schoolYearID;

$sql = "SELECT y.YearID, y.YearName, yc.AcademicYearID, COUNT(DISTINCT i.RecordID) as NumberOfApplicant  
		FROM YEAR as y 
		INNER JOIN YEAR_CLASS as yc ON yc.YearID=y.YearID 
		LEFT JOIN ADMISSION_OTHERS_INFO as i ON i.ApplyLevel=y.YearID AND i.ApplyYear=yc.AcademicYearID 
		LEFT JOIN ADMISSION_APPLICATION_STATUS as s ON s.ApplicationID = i.ApplicationID
		WHERE yc.AcademicYearID='$AcademicYearID' AND s.Status<>'5'
		GROUP BY y.YearID 
		ORDER BY y.Sequence";

$form_list = $li->returnResultSet($sql);
$form_list_count = count($form_list);

$css = ' style="border:1px solid black;"';

$x .= '<h1>善明托兒所 - 入學抽籤</h1>';
$x .= '<h2>檢視結果</h2>';
$x .= '<form id="form1" name="form1" method="post" action="">';
$x .= '<table border="0" cellspacing="0" cellpadding="3" border-collapse="collapse"'.$css.'>';
	$x .= '<tr><th'.$css.'>#</th><th'.$css.'>級別</th><th'.$css.'>申請人數</th>';
	$x .= '<th'.$css.'>';
	//$x .= '<input type="button" id="ClearAllButton" name="ClearAllButton" value="清除所有抽籤數據" onclick="ClearBallotResult('.$AcademicYearID.',\'\');" />';
	$x .= '<input type="button" id="ViewResultButton" name="ViewResultButton" value="檢視抽籤結果" onclick="ViewBallotResult('.$AcademicYearID.');" />';
	$x .= '</th></tr>';
	for($i=0;$i<$form_list_count;$i++){
		$x .= '<tr>';
			$x .= '<td'.$css.'>'.($i+1).'</td><td'.$css.'>'.$form_list[$i]['YearName'].'</td><td'.$css.'>'.$form_list[$i]['NumberOfApplicant'].'</td>';
			$x .= '<td'.$css.'>';
				//$x .= '<input type="button" name="BallotButton_'.$form_list[$i]['YearID'].'" value="抽籤" onclick="DoBallot('.$form_list[$i]['AcademicYearID'].','.$form_list[$i]['YearID'].');" />&nbsp;';
				$x .= '<input type="button" name="ClearButton_'.$form_list[$i]['YearID'].'" value="清除抽籤數據" onclick="ClearBallotResult('.$form_list[$i]['AcademicYearID'].','.$form_list[$i]['YearID'].');"/>';
			$x .= '&nbsp;</td>';
		$x .= '</tr>';
	}
$x .= '</table><br>';
$x .= '<input type="button" id="ClearAllButton" name="ClearAllButton" value="清除所有結果" onclick="ClearBallotResult('.$AcademicYearID.',\'\');" />';
$x .= '</form>';

$x .= '<div id="DivMsg" style="display:none;"></div><div id="BallotResultDiv"></div>';

?>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>:: eClass KIS ::</title>
<script type="text/javascript" src="/templates/jquery/jquery-1.8.3.min.js"></script>
</head>
<body>
<script type="text/javascript" language="JavaScript">
var is_processing = false;
var timer_id = -1;
function DoBallot(SchoolYearID,YearID)
{
	if(!is_processing){
		is_processing = true;
		$('#DivMsg').html('抽籤進行中......').show();
		$.post(
			'ballot_process.php',
			{
				'SchoolYearID': SchoolYearID,
				'YearID': YearID 
			},
			function(data){
				if(timer_id > 0) clearTimeout(timer_id);
				$('#BallotResultDiv').html(data);
				is_processing = false;
				$('#DivMsg').html('').hide();
			}
		);
	}else{
		alert('抱歉，系統正忙於處理之前的任務，請耐心等待。');
	}
}

function ClearBallotResult(_SchoolYearID,_YearID)
{
	var SchoolYearID = _SchoolYearID || '';
	var YearID = _YearID || '';
	if(!is_processing){
		if(confirm('你確定要清除抽籤數據?'))
		{
			is_processing = true;
			$('#DivMsg').html('正在清除抽籤數據......').show();
			$.post(
				'ballot_process.php',
				{
					'ClearResult': 1,
					'SchoolYearID': SchoolYearID,
					'YearID': YearID 
				},
				function(data){
					if(timer_id > 0) clearTimeout(timer_id);
					$('#BallotResultDiv').html(data);
					is_processing = false;
					$('#DivMsg').html('').hide();
					timer_id = setTimeout(function(){
						$('#BallotResultDiv').html('');
					},5000);
				}
			);
		}
	}else{
		alert('抱歉，系統正忙於處理之前的任務，請耐心等待。');
	}
}

function ViewBallotResult(_SchoolYearID)
{
	var SchoolYearID = _SchoolYearID || '';
	if(!is_processing){
		is_processing = true;
		$('#DivMsg').html('正在取得抽籤結果......').show();
		$.post(
			'ballot_process.php',
			{
				'ViewResult': 1,
				'ShowChoice': 1,
				'SchoolYearID': SchoolYearID 
			},
			function(data){
				if(timer_id > 0) clearTimeout(timer_id);
				$('#BallotResultDiv').html(data);
				is_processing = false;
				$('#DivMsg').html('').hide();
			}
		);
	}else{
		alert('抱歉，系統正忙於處理之前的任務，請耐心等待。');
	}
}
</script>
<?=$x?>
</body>
</html>
<?php
intranet_closedb();
?>