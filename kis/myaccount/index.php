<?
# using: yat

###############################
#
#	Date:	2013-08-06	YatWoon
#			add eAdmission standalone checking
###############################

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT.'kis/init.php');

$kis_data['user_detail'] = $libkis->getUserDetail();

if($stand_alone['eAdmission'])
{
	# as "accountmanage" is not included in stand alone settings, so need extra call the class 
	include_once($PATH_WRT_ROOT."includes/kis/apps/libkis_accountmanage.php");	
	$libkis_accountmanage = new kis_accountmanage($UserID,'','',array('target_user_id'=>$kis_user['id']));
	
	# as "accountmanage" is not included in stand alone settings, so need extra call the lang file 
	include_once($PATH_WRT_ROOT.'lang/kis/apps/lang_accountmanage_'. $intranet_session_language .'.php');
}
else
{
	$libkis_accountmanage = $libkis->loadApp('accountmanage', array('target_user_id'=>$kis_user['id']));
}

$kis_data['permission'] = $libkis_accountmanage -> getUserTypePermission($kis_user['type']);
if($stand_alone['eAdmission'])
{
	$menu_items = array('contactinfo','','changepassword');
	$default_main_template = "contactinfo";
}
else
{
	$menu_items = $kis_data['permission']['CanUpdatePassword']? array('personalinfo','','contactinfo','','changepassword'): array('personalinfo','','contactinfo');
	$default_main_template = "personalinfo";
}

if ($kis_user['type'] == kis::$user_types['teacher'] && $_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] && $module_version['StaffAttendance'] == 3.0) {
	$staffAttendanceRecordAry = array();
	$staffAttendanceRecordAry['isPopUp'] = true;
	$staffAttendanceRecordAry['name'] = 'staffPersonalAttendance';
	$staffAttendanceRecordAry['href'] = 'home/smartcard/StaffAttendance/attendance_record.php';
	$menu_items[] = '';
	$menu_items[] = $staffAttendanceRecordAry;
}

switch ($q[0]){
    
    case 'contactinfo':
		$main_template = 'contactinfo';
    break;

    case 'changepassword':
		if ($kis_data['permission']['CanUpdatePassword']){
		    $kis_data['token'] = $_SESSION['kis']['password_token'] = md5(time().mt_rand());
		    $main_template = 'changepassword';
		}else{
		    $main_template = 'personalinfo';
		}
	
    break;

    default:
		$main_template = $default_main_template;
    break;
 
}


?>
<div class="content_board_menu">
<? kis_ui::loadLeftMenu($menu_items, $q[0], "#/myaccount/");?>
    <div class="main_content">
    <? kis_ui::loadTemplate($main_template, $kis_data); ?>
    </div><p class="spacer"></p>
</div>