$(function () {
  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  $(document).scrollTop(getCookie('scroll'));

  $(document).ready(function () {
    $('.menu .item').tab();
    var updateField = function ($modal) {
      $modal.find('.ui.sticky').sticky({
        context: '#mainDiv',
        offset: 45,
      });
      $modal.find('.remarkPopup').popup();
      $modal.find('select.dropdown').dropdown();
      $modal.find('.ui.checkbox').checkbox();
      $modal.find('.menu .item').tab();
      $modal.find('.ui.accordion').accordion();
      $modal.find('.sortFieldContainer').sortable();
    }

    $('#AcademicYearID').change(function () {
      $('#settingsForm').attr('action', '').submit();
    });

    $('#export').click(function () {
      var $form = $('#settingsForm');
      $form.find('[name="Action"]').val('Export');
      $form.attr('target', '_blank');
      $form.attr('action', 'update.php').submit();
      $form.attr('target', '_self');
    });

    $('#import').click(function () {
      $('#importModal input[name="AcademicYearID"]').val($('#AcademicYearID').val());
      $('#importModal').modal({
        closable: false,
        onApprove: function () {
          document.cookie = "scroll=" + $(document).scrollTop();
          $(this).find('form').addClass('loading').submit();
        }
      }).modal('show');
      updateField($('#importModal'));
    });

    $('#deleteCurrentYear').click(function (e) {
      if (!confirm('Are you sure to delete?')) {
        e.preventDefault();
        return false;
      }

      var $form = $('#settingsForm');
      $form.find('[name="Action"]').val('DeleteAllData');
      $form.attr('action', 'update.php').submit();
    });


    $('.reorderField').click(function () {
      var groupId = $(this).data('groupId');
      $('.reorderModal[data-group-id="' + groupId + '"]').modal({
        closable: false,
        onApprove: function () {
          document.cookie = "scroll=" + $(document).scrollTop();
          $(this).find('form').addClass('loading').submit();
        }
      }).modal('show');
      updateField($('.reorderModal[data-group-id="' + groupId + '"]'));
    });

    $('.editGroup').click(function () {
      var groupId = $(this).data('groupId');
      $('.groupModal[data-group-id="' + groupId + '"]').modal({
        closable: false,
        onApprove: function () {
          document.cookie = "scroll=" + $(document).scrollTop();
          $(this).find('form').addClass('loading').submit();
        }
      }).modal('show');
      updateField($('.groupModal[data-group-id="' + groupId + '"]'));
    });

    $('.deleteGroupForm').submit(function (e) {
      if (!confirm('Are you sure to delete?')) {
        e.preventDefault();
        return false;
      }
    });


    $('.editField').click(function () {
      var groupId = $(this).data('groupId');
      var fieldId = $(this).data('fieldId');
      $('.fieldModal[data-group-id="' + groupId + '"][data-field-id="' + fieldId + '"]').modal({
        closable: false,
        onApprove: function () {
          document.cookie = "scroll=" + $(document).scrollTop();
          $(this).find('form').addClass('loading').submit();
        }
      }).modal('show');
      updateField($('.fieldModal[data-group-id="' + groupId + '"][data-field-id="' + fieldId + '"]'));
    });

    $('.deleteFieldForm').submit(function (e) {
      if (!confirm('Are you sure to delete?')) {
        e.preventDefault();
        return false;
      }
    });

    $('[name="DbTableName"]').change(function () {
      var table = $(this).val();
      var $select = $(this).closest('.fields').find('[name="DbFieldName"]');
      $select.find('option').each(function (index) {
        var relatedTables = $(this).data('tables').split(',');
        if (relatedTables.indexOf(table) > -1) {
          $(this).show();
        } else {
          $(this).hide();
          if ($(this).is(':selected')) {
            $select.val('');
          }
        }
      });
    }).change();
  });
});
