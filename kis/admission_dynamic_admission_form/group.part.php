<?php

$groups = $dafs->getCurrentAcademicYearGroups();

?>
<h1>
    Group
    <button class="ui icon button editGroup blue" data-group-id="0">
    	<i class="plus icon"></i>
    </button>
    <!--button class="ui icon button">
    	<i class="arrows alternate vertical icon"></i>
    </button-->
</h1>

<table class="ui celled table">
	<thead>
		<tr>
    		<th>GroupID</th>
    		<th>TitleB5</th>
    		<th>TitleEn</th>
    		<th>TitleGb</th>
    		<th>Type</th>
    		<th>OtherAttribute</th>
    		<th>Status</th>
    		<th></th>
    	</tr>
	</thead>
	<tbody>
    	<?php foreach($groups as $group): ?>
    		<tr>
    			<td data-label="GroupID">
    				<?=$group['GroupID'] ?>
        			<?php if($group['Remark']): ?>
            			<div class="ui icon remarkPopup" data-html="<?=nl2br(intranet_htmlspecialchars($group['Remark'])) ?>" style="display:inline-block">
        					<i class="info circle icon violet big"></i>
        				</div>
    				<?php endif; ?>
    			</td>
    			<td data-label="TitleB5"><?=$group['TitleB5'] ?></td>
    			<td data-label="TitleEn"><?=$group['TitleEn'] ?></td>
    			<td data-label="TitleGb"><?=$group['TitleGb'] ?></td>
    			<td data-label="Type"><?=$group['Type'] ?></td>
    			<td data-label="OtherAttribute"><?=nl2br(stripslashes($group['OtherAttribute'])) ?></td>
    			<td data-label="Status">
    				<i class="eye large icon <?=($group['Status'])?'green':'slash grey' ?>"></i>
    			</td>
    			<td data-label="">
        			<button class="ui icon button editGroup blue" data-group-id="<?=$group['GroupID'] ?>">
        				<i class="edit icon"></i>
        			</button>
        			<form class="deleteGroupForm" method="POST" action="update.php" style="display: inline-block;">
						<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID ?>" />
        				<input type="hidden" name="Action" value="DeleteGroup" />
        				<input type="hidden" name="GroupID" value="<?=$group['GroupID'] ?>" />
            			<button type="submit" class="ui icon button red">
            				<i class="trash icon"></i>
            			</button>
        			</form>
    			</td>
    		</tr>
		<?php endforeach; ?>
	</tbody>
</table>


<div class="ui modal groupModal" data-group-id="0">
	<i class="close icon"></i>
	<div class="header">
		Add Group
	</div>
	<div class="content">
		<form class="ui form groupForm" method="POST" action="update.php">
			<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID ?>" />
			<input type="hidden" name="Action" value="UpdateGroup" />
			<input type="hidden" name="GroupID" value="0" />
        	<div class="ui form">
            	<div class="three fields">
            		<div class="field">
            			<label>TitleB5</label>
            			<input type="text" name="TitleB5" value="">
            		</div>
            		<div class="field">
            			<label>TitleEn</label>
            			<input type="text" name="TitleEn" value="">
            		</div>
            		<div class="field">
            			<label>TitleGb</label>
            			<input type="text" name="TitleGb" value="">
            		</div>
            	</div>
                <div class="three fields">
                    <div class="field">
                        <label>LabelB5</label>
                        <input type="text" name="LabelB5" value="">
                    </div>
                    <div class="field">
                        <label>LabelEn</label>
                        <input type="text" name="LabelEn" value="">
                    </div>
                    <div class="field">
                        <label>LabelGb</label>
                        <input type="text" name="LabelGb" value="">
                    </div>
                </div>
            	<div class="field">
        			<label>Type</label>
        			<?=getSelectByArray(
        			    \AdmissionSystem\DynamicAdmissionFormSystem::listGroupTypes(),
        			    ' name="Type" class="ui fluid dropdown"',
        			    \AdmissionSystem\DynamicAdmissionFormSystem::GROUP_TYPE_NORMAL,
        			    0,1
    			    ) ?>
            	</div>
            	<div class="field">
            		<label>OtherAttribute</label>
            		<textarea name="OtherAttribute" rows="2"></textarea>
            	</div>
            	<div class="field">
            		<label>Remark</label>
            		<textarea name="Remark" rows="2"></textarea>
            	</div>
            	<div class="inline field">
            		<div class="ui toggle checkbox">
            			<input type="checkbox" name="Status" class="hidden" value="1" checked>
            			<label>Active</label>
            		</div>
            	</div>
            </div>
		</form>
	</div>
	<div class="actions">
		<div class="ui black deny button">
			Cancel
		</div>
		<div class="ui positive labeled icon button save">
			Save
			<i class="save icon"></i>
		</div>
	</div>
</div>

<?php foreach($groups as $group): ?>
	<div class="ui modal groupModal" data-group-id="<?=$group['GroupID'] ?>">
    	<i class="close icon"></i>
    	<div class="header">
    		Edit Group (GroupID: <?=$group['GroupID'] ?>)
    	</div>
    	<div class="content">
			<form class="ui form groupForm" method="POST" action="update.php">
				<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID ?>" />
				<input type="hidden" name="Action" value="UpdateGroup" />
				<input type="hidden" name="GroupID" value="<?=$group['GroupID'] ?>" />
            	<div class="ui form">
                	<div class="three fields">
                		<div class="field">
                			<label>TitleB5</label>
                			<input type="text" name="TitleB5" value="<?=intranet_htmlspecialchars($group['TitleB5']) ?>">
                		</div>
                		<div class="field">
                			<label>TitleEn</label>
                			<input type="text" name="TitleEn" value="<?=intranet_htmlspecialchars($group['TitleEn']) ?>">
                		</div>
                		<div class="field">
                			<label>TitleGb</label>
                			<input type="text" name="TitleGb" value="<?=intranet_htmlspecialchars($group['TitleGb']) ?>">
                		</div>
                	</div>
                    <div class="three fields">
                        <div class="field">
                            <label>LabelB5</label>
                            <input type="text" name="LabelB5" value="<?=intranet_htmlspecialchars($group['LabelB5']) ?>">
                        </div>
                        <div class="field">
                            <label>LabelEn</label>
                            <input type="text" name="LabelEn" value="<?=intranet_htmlspecialchars($group['LabelEn']) ?>">
                        </div>
                        <div class="field">
                            <label>LabelGb</label>
                            <input type="text" name="LabelGb" value="<?=intranet_htmlspecialchars($group['LabelGb']) ?>">
                        </div>
                    </div>
                	<div class="field">
            			<label>Type</label>
            			<?=getSelectByArray(
            			    \AdmissionSystem\DynamicAdmissionFormSystem::listGroupTypes(),
            			    ' name="Type" class="ui fluid dropdown"',
            			    $group['Type'],
            			    0,1
        			    ) ?>
                	</div>
                	<div class="field">
                		<label>OtherAttribute</label>
                		<textarea name="OtherAttribute" rows="3"><?=intranet_htmlspecialchars($group['OtherAttribute']) ?></textarea>
                	</div>
                	<div class="field">
                		<label>Remark</label>
                		<textarea name="Remark" rows="3"><?=intranet_htmlspecialchars($group['Remark']) ?></textarea>
                	</div>
                	<div class="fields three">
                		<div class="inline field">
                			<label>Sequence</label>
                			<input type="text" name="Sequence" value="<?=intranet_htmlspecialchars($group['Sequence']) ?>">
                		</div>
                	</div>
                	<div class="inline field">
                		<div class="ui toggle checkbox">
                			<input type="checkbox" name="Status" class="hidden" value="1" <?=($group['Status'])?'checked':'' ?>>
                			<label>Active</label>
                		</div>
                	</div>
                </div>
			</form>
    	</div>
    	<div class="actions">
    		<div class="ui black deny button">
    			Cancel
    		</div>
    		<div class="ui positive labeled icon button save">
    			Save
    			<i class="save icon"></i>
    		</div>
    	</div>
    </div>
<?php endforeach; ?>