<?php
//using Henry

/********************
 * 
 * Log :
 * Date		2014-03-14 [Henry]
 * 			File Created
 * 
 ********************/

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

//for the export
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();

$ExportArr = array();
$exportColumn = array();

$isKIS = $_SESSION["platform"]=="KIS";

if($_SESSION['UserType']==USERTYPE_STAFF){
	//from user management export function [Start]
	if ($TabID == 3)	# parent
     {
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","Gender","Mobile","Fax","Barcode","Remarks");
         
         if($special_feature['ava_hkid'])
         {
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
		$exportColumn = array_merge($exportColumn, array("StudentLogin1","StudentEngName1","StudentLogin2","StudentEngName2","StudentLogin3","StudentEngName3"));
      }
      else if ($TabID == 2)		# student
     {
         if(!$isKIS) {
         	$x .= ",WebSAMSRegNo,HKJApplNo";
         }
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Mobile","Fax","Barcode","Remarks","DOB");
		 if(!$isKIS) {
		 	$exportColumn = array_merge($exportColumn, array("WebSAMSRegNo","HKJApplNo"));
		 }
		 $exportColumn = array_merge($exportColumn,array("Address"));
        if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']))
		{
			$exportColumn = array_merge($exportColumn, array("CardID"));
			if($sys_custom['SupplementarySmartCard']){
				$exportColumn = array_merge($exportColumn, array("CardID2","CardID3"));
			}
		}		
         if($special_feature['ava_hkid'])
         {
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
         
         if($special_feature['ava_strn'])
         {
			$exportColumn = array_merge($exportColumn, array("STRN"));
         }
         if($plugin['medical'])
         {
         	$exportColumn = array_merge($exportColumn, array("StayOverNight"));
         }
		 $exportColumn = array_merge($exportColumn, array("Nationality", "PlaceOfBirth", "AdmissionDate"));
     }
	//from user management export function [End]
	if($sys_custom['KIS_Admission']['ICMS']['Settings']){
		$exportColumn = array("Application#","StudentEnglishName","InterviewDate","InterviewTime", "InterviewLocation");
	}
	else if($sys_custom['KIS_Admission']['MUNSANG']['Settings'] || $sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['UCCKE']['Settings']){
		$exportColumn = array("Application#","StudentEnglishName","StudrntChineseName",($sys_custom['KIS_Admission']['UCCKE']['Settings']?"HKIDNo":"BirthCertNo"),"InterviewDate","InterviewTime", "InterviewLocation");
	}
	else
		$exportColumn = array("Application#","StudentEnglishName","StudrntChineseName","BirthCertNo","InterviewDate","InterviewTime");
	
	$applicationAry = explode(',',$_REQUEST['applicationIds']);
	$schoolYearID = $_REQUEST['schoolYearID'];
	$classLevelID = $_REQUEST['classLevelID'];
	$selectStatus = $_REQUEST['selectStatus'];
	//all data
	$result = $lac->getExportDataForImportInterview('', $schoolYearID,$selectStatus,$classLevelID);
	$ExportArr = $result;
//	$applicationCnt = count($applicationAry);
//	for($i=0;$i<$applicationCnt;$i++){
//		$result = $lac->getExportDataForImportInterview($applicationAry[$i], $schoolYearID);
//		$ExportArr[$i] = $result;
//	}
	//get the clss level name
	$classLevel = $lac->getClassLevel();
	$otherInfo = current($lac->getApplicationOthersInfo($schoolYearID,'','',$applicationAry[0]));
	$formname = $classLevel[$classLevelID];
		
	$filename = "admission_data_for_import_interview_info(".$formname.").csv";

	//generate csv content
	$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "9");
	
	//Output The File To User Browser
	$lexport->EXPORT_FILE($filename, $export_content);
}
else{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	 $laccessright = new libaccessright();
	 $laccessright->NO_ACCESS_RIGHT_REDIRECT();
	 exit;
}
?>