<?php
// modifying by: 

/**
 * ******************
 * Log :
 * Date 2018-09-03 [Pun]
 * added $sys_custom['KIS_Admission']['AdmissionFormVersion'] to load new admission form
 *
 * Date 2018-01-24 [Pun]
 * added call getWholeApplicationForm() to load contents
 *
 * Date 2017-09-15 [Pun]
 * added load all language
 *
 * Date 2015-09-15 [Henry]
 * added checking !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || $_GET['token']
 *
 * Date 2015-01-20 [Henry]
 * reload page by user will redirect to finish page for CSM cust
 *
 * Date 2014-07-25 [Henry]
 * Add Error Message
 *
 * Date 2014-06-17 [Henry]
 * Allow to use the Integrated Central Server for admission
 * `*
 * Date 2014-01-15 [Carlos]
 * Modified other attachments section to follow attachment settings
 * `*
 * Date 2013-10-08 [Henry]
 * File Created
 *
 * ******************
 */
$PATH_WRT_ROOT = "../../";

include_once ("{$PATH_WRT_ROOT}includes/global.php");
include_once ("{$PATH_WRT_ROOT}includes/libdb.php");
include_once ("{$PATH_WRT_ROOT}includes/libinterface.php");
include_once ("{$PATH_WRT_ROOT}includes/kis/libkis.php");
include_once ("{$PATH_WRT_ROOT}includes/kis/libkis_ui.php");
include_once ("{$PATH_WRT_ROOT}includes/kis/libkis_utility.php");
include_once ("{$PATH_WRT_ROOT}includes/kis/libkis_apps.php");
// include_once($PATH_WRT_ROOT."includes/json.php");

include_once ("../config.php");

if($sys_custom['KIS_Admission']['AdmissionFormVersion']){
    if($_GET['token']){
        header("Location: ../admission_form{$sys_custom['KIS_Admission']['AdmissionFormVersion']}/?token={$_GET['token']}");
    }else{
        header("Location: ../admission_form{$sys_custom['KIS_Admission']['AdmissionFormVersion']}");
    }
    exit;
}

// for the customization
include_once ("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once ("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/libadmission_cust.php");
include_once ("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/libadmission_ui_cust.php");

intranet_opendb();

// ### Lang START ####
if ($intranet_session_language == 'en') {
    $intranet_session_language = 'b5';
    include ("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
    unset($Lang);
    unset($kis_lang);

    $intranet_session_language = 'en';
    include ("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
} else {
    $intranet_session_language = 'en';
    include ("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
    unset($Lang);
    unset($kis_lang);

    $intranet_session_language = 'b5';
    include ("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
}
// ### Lang END ####

// $libjson = new JSON_obj();
$libkis = new kis('');
$lauc = new admission_ui_cust();
$lac = new admission_cust();
$li = new interface_html();

if (! $plugin['eAdmission']) {
    include_once ("{$PATH_WRT_ROOT}includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT("", "../");
    exit();
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool();

// $libkis_admission = $libkis->loadApp('admission');
// Get the index content
$basic_settings = $lac->getBasicSettings();

if ($lac->schoolYearID)
    $instruction = $basic_settings['generalInstruction'];
else {
    // $instruction = $lac->displayWarningMsg('notyetsetnextschoolyear');
    $instruction = '<fieldset class="warning_box">
						<legend>' . $Lang['Admission']['warning'] . '</legend>
						<ul>
							<li>' . $Lang['Admission']['msg']['noclasslevelapply'] . '</li>
						</ul>
					</fieldset>';
}

$religion_selection = $lac->displayPresetCodeSelection("RELIGION", "StudentReligion", $result[0]['RELIGION']);
// $religion_selected = $lac->returnPresetCodeName("RELIGION", $formData['StudentReligion']);

// # preview form function
$allowToPreview = $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod();

// -------------------------------------------------------
// # developing token
// $sys_custom['KIS_Admission']['IntegratedCentralServer'] = true;
$isValidForAdmission = true;
if (($_SESSION["platform"] != "KIS" || ! $_SESSION["UserID"] || ! $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || $_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer'] && ! $allowToPreview) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // set URL and other appropriate options

    // curl_setopt($ch, CURLOPT_URL, "http://192.168.0.146:31002/test/queue/reserved.php?token=f590bac6181c28fda4ec6812e7f2ae4f92&FromKIS=1");
    // # the host name should be dynamic
    if ($sys_custom['KIS_Admission']['MUNSANG']['Settings']) {
        $formCode = 'QUOTA_FOR_ALL';
    } else {
        $formCode = $_REQUEST['sus_status'];
    }

    if ($sys_custom['KIS_Admission']['MUNSANG']['Settings']) {
        $fromReceived = count($lac->getApplicationOthersInfo($lac->schoolYearID));
        curl_setopt($ch, CURLOPT_URL, $admission_cfg['IntegratedCentralServer'] . "reserved.php?token=" . $_GET['token'] . "&FromKIS=1&FormCode=" . $formCode . "&FilledBy=" . $fromReceived);
    } else {
        curl_setopt($ch, CURLOPT_URL, $admission_cfg['IntegratedCentralServer'] . "reserved.php?token=" . $_GET['token'] . "&FromKIS=1");
    }

    // grab URL and pass it to the browser

    $data_from_checking = curl_exec($ch);

    $data_collected = unserialize($data_from_checking);
    // debug_pr($data_collected);

    // if($data_collected['InDate'] == date('Y-m-d') && $data_collected['timeFrom'] >= date('H:i') && $data_collected['timeTo'] <= date('H:i')){
    // $isValidForAdmission = true;
    // }
    // debug_r($isValidForAdmission);
    // debug_pr(date('H:i'));

    if ((! $data_collected['AllowApplyNow'] || $data_collected['ErrorCode'] != '' || $lac->hasToken($_GET['token']) > 0) && ($_SESSION["platform"] != "KIS" || ! $_SESSION["UserID"] || ! $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || $_GET['token'])) {
        $isValidForAdmission = false;
        // header("location: ".$PATH_WRT_ROOT."/kis/admission_form/access_deny.php?err=".$data_collected['ErrorCode']);
        if ($_GET['token'] && $lac->hasToken($_GET['token']) > 0) {
            $data_collected['ErrorMsg'] = "閣下已遞交網上申請表，申請通知電郵已發送，請檢查閣下在申請表填寫的電郵。";
            $data_collected['ErrorMsg'] .= "<br/>You have applied the admission! Please check your Email to get the admission Notification!";
            if ($sys_custom['KIS_Admission']['HKUGAPS']['Settings']) {
                $data_collected['ErrorMsg'] = '因付款程序尚未完成，是次申請經已失效，<a href="http://eadmission.eclasscloud.hk/?af=hkugaps">請按此返回首頁重新報名。</a>';
                $data_collected['ErrorMsg'] .= '<br/>Due to payment failure, the application process has expired. <a href="http://eadmission.eclasscloud.hk/?af=hkugaps">Please return to home page and retry.</a>';
            }
            // Henry Added Start[20150120]
            if ($sys_custom['KIS_Admission']['CSM']['Settings']) {
                $otherInfoResult = $lac->getOtherInfoByToken($_GET['token']);
                $id = urlencode(getEncryptedText("ApplicationID=" . $otherInfoResult['ApplicationID'] . "&sus_status=" . $otherInfoResult['ApplyYear'] . "&SchoolYearID=" . $otherInfoResult['ApplyLevel'], $admission_cfg['FilePathKey']));
                header("Location: finish.php?id=" . $id);
                exit();
            }
            // Henry Added End[20150120]
        } else
            if ($data_collected['QuotaReached']) {
                if ($sys_custom['KIS_Admission']['MUNSANG']['Settings']) {
                    header("Location: " . $admission_cfg['IntegratedCentralServer'] . '?af=' . $_SERVER['HTTP_HOST']);
                    exit();
                    // $data_collected['ErrorMsg'] = "2015-2016年度幼兒班申請已完結，多謝報讀民生書院幼稚園。<br/>";
                    // $data_collected['ErrorMsg'] .= "The application period for nursery class of 2015－2016 is now closed. Thank you for applying Munsang College Kindergarten.";
                }
            }

        if($data_collected['ErrorMsg'] == 'access denied'){
        	$data_collected['ErrorMsg'] = "這個網頁無法使用，請重新嘗試。";
            $data_collected['ErrorMsg'] .= "<br/>You cannot access this page directly. Please try again!";
        }
        $main_content = '<div class="admission_board">';
        $main_content .= '<div class="admission_complete_msg"><h1>' . $data_collected['ErrorMsg'] . '</h1></div>';
        $main_content .= '</div>';
    }
} else {
    $data_collected['ApplyFor'] = '';
    $data_collected['IDNumber'] = '';
}
// --------------------------------------------------------
if ($isValidForAdmission || ! $sys_custom['KIS_Admission']['IntegratedCentralServer']) {

    $main_content = '<form id="form1" name="form1" method="POST" action="confirm_update.php" onSubmit="return submitForm()" ENCTYPE="multipart/form-data">';

    if (method_exists($lauc, 'getWholeApplicationForm')) {
        $ApplyFor = '';
        $BirthCertNo = '';
        if ($sys_custom['KIS_Admission']['IntegratedCentralServer']) {
            $ApplyFor = $data_collected['ApplyFor'];
            $BirthCertNo = $data_collected['IDNumber'];
        }
        // $main_content .= $lauc->getWholeApplicationForm($instruction, $ApplyFor, $BirthCertNo);

        $indexContent = $lauc->getIndexContent($instruction, $ApplyFor, $IsUpdate = false);
        $main_content .= <<<HTML
            <div id="step_index" style="display:auto">
                <input type="hidden" id="birthCertNo" value="{$BirthCertNo}" />
                {$indexContent}
            </div>
            <div id="formContainer"></div>
HTML;
    } else {
        // The index page
        $main_content .= "<div id='step_index' style='display:auto'>";
        if ($sys_custom['KIS_Admission']['IntegratedCentralServer'])
            $main_content .= $lauc->getIndexContent($instruction, $data_collected['ApplyFor']);
        else
            $main_content .= $lauc->getIndexContent($instruction);
        $main_content .= "</div>";

        // The instruction page
        $main_content .= "<div id='step_instruction' style='display:none'>";
        $main_content .= "</div>";

        // The input info page
        $main_content .= "<div id='step_input_form' style='display:none'>";
        if ($sys_custom['KIS_Admission']['IntegratedCentralServer'])
            $main_content .= $lauc->getApplicationForm($data_collected['IDNumber']);
        else
            $main_content .= $lauc->getApplicationForm();
        $main_content .= "</div>";

        // The docs upload page
        $main_content .= "<div id='step_docs_upload' style='display:none'>";
        $main_content .= $lauc->getDocsUploadForm();
        $main_content .= "</div>";

        // The docs upload page
        $main_content .= "<div id='step_confirm' style='display:none'>";
        $main_content .= "</div>";
    }

    // Henry modifying 20131028
    $main_content .= '<input type="hidden" name="token" id="token" value="' . $_GET['token'] . '"/>';

    $main_content .= '</form>';
}
include_once ("common_tmpl.php");
include_once ("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/commonJs.php");
?>


