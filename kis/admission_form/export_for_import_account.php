<?php
//using Henry

/********************
 * 
 * Log :
 * 
 * Date		2015-01-19 [Henry]
 * 			export all data by status
 * 
 * Date		2014-03-14 [Henry]
 * 			File Created
 * 
 ********************/

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

//for the export
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();

$ExportArr = array();
$exportColumn = array();

$isKIS = $_SESSION["platform"]=="KIS";

if($_SESSION['UserType']==USERTYPE_STAFF){
	//from user management export function [Start]
	if ($TabID == 3)	# parent
     {

         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","Gender","Mobile","Fax","Barcode","Remarks");
     	
         if($special_feature['ava_hkid'])
         {
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
		$exportColumn = array_merge($exportColumn, array("StudentLogin1","StudentEngName1","StudentLogin2","StudentEngName2","StudentLogin3","StudentEngName3"));
     	
		if($setting_path_ip_rel == 'hkugaps.eclasscloud.hk'){
			$exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","Gender","Mobile","Fax","Barcode","Remarks");
		}
		
     }
      else if ($TabID == 2)		# student
     {
         if(!$isKIS) {
         	$x .= ",WebSAMSRegNo,HKJApplNo";
         }
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Mobile","Fax","Barcode","Remarks","DOB");
         if(!$isKIS) {
		 	$exportColumn = array_merge($exportColumn, array("WebSAMSRegNo","HKJApplNo"));
		 }
		 $exportColumn = array_merge($exportColumn,array("Address"));
        if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']))
		{
			$exportColumn = array_merge($exportColumn, array("CardID"));
			if($sys_custom['SupplementarySmartCard']){
				$exportColumn = array_merge($exportColumn, array("CardID2","CardID3"));
			}
		}		
         if($special_feature['ava_hkid'])
         {
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
         
         if($special_feature['ava_strn'])
         {
			$exportColumn = array_merge($exportColumn, array("STRN"));
         }
         if($plugin['medical'])
         {
         	$exportColumn = array_merge($exportColumn, array("StayOverNight"));
         }
		 $exportColumn = array_merge($exportColumn, array("Nationality", "PlaceOfBirth", "AdmissionDate"));
		 
		 if($setting_path_ip_rel == 'hkugaps.eclasscloud.hk'){
		 	$exportColumn = array("UserLogin*","Password","UserEmail","EnglishName*","ChineseName*","ClassName","Class Number","NickName","Gender","STRN","WebSAMSRegNo","HKID","Smart Card ID","Barcode","DOB","PlaceOfBirth","Nationality","Spoken Language at Home","AdmissionDate","Admission Class Level","Last Kindergarten Attended","Last Primary School Attended","Date of Graduate","Date of Left","Secondary School","Address","Home Tel","Mobile","Contact Email","SEN (code)","NCS","New From Mainland","Cross-boundary Student","Subsidy","Remarks");
		 }
     }
	//from user management export function [End]
	
	if(!$applicationAry){
		$applicationAry = array();
		if($sys_custom['KIS_Admission']['CSM']['Settings'])
			$result = $lac->getApplicationStatus($schoolYearID,$classLevelID,$applicationID='',$recordID='');
		else
			$result = $lac->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID='',$recordID='');
		foreach($result as $aResult){
			if(!$sys_custom['KIS_Admission']['CSM']['Settings'] || $selectStatus == $aResult['statusCode'] || $selectStatus == '')
				$applicationAry[] = $aResult['RecordID'];
		}
	}
	//all data
	$applicationCnt = count($applicationAry);
	
	for($i=0;$i<$applicationCnt;$i++){
		$result = $lac->getExportDataForImportAccount($schoolYearID,'','',$applicationAry[$i], $TabID); //should impelement to function getExportDataForImportAccount
		foreach ($result as $aResult){
			$ExportArr[] = $aResult;
		}
		
	}
	//get the clss level name
	$classLevel = $lac->getClassLevel();
	$otherInfo = current($lac->getApplicationOthersInfo($schoolYearID,'','',$applicationAry[0]));
	$formname = $classLevel[$otherInfo['classLevelID']];
		
	if($TabID == 3)
		$filename = "admission_data_for_import_parent_account(".$formname.").csv";
	else if($TabID == 2)	
		$filename = "admission_data_for_import_student_account(".$formname.").csv";
	
	//generate csv content
	$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "9");
		
	//Output The File To User Browser
	if($setting_path_ip_rel == 'hkugaps.eclasscloud.hk'){
		$lexport->EXPORT_FILE($filename, $export_content/*,false,false,'Big5'*/);
	}else{
		$lexport->EXPORT_FILE($filename, $export_content);
	}

}
else{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	 $laccessright = new libaccessright();
	 $laccessright->NO_ACCESS_RIGHT_REDIRECT();
	 exit;
}
?>