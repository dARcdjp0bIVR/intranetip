<?php
# modifying by: 
 
/********************
 * Log :
 * Date		2014-01-15 [Carlos]
 * 			Modified other attachments section to follow attachment settings
 * 
 * Date		2013-10-09 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

intranet_opendb();

$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$lfs		=new libfilesystem();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool(); 

//$libkis_admission = $libkis->loadApp('admission');

//$fileData = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['hidden_file'])));
//$formData = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['hidden_data'])));
//$otherFileData = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['hidden_other_file'])));
//$tempFolderPath = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['tempFolderPath'])));



//For Debugging only
//$lfs->folder_remove_recursive($admission_cfg['FilePath']."/intranetdata");
//$lfs->lfs_remove($folderPath);
	//$lfs->folder_new($folderPath."/t001");


//$lfs->folder_new($tempFolderPath.'/other_files');

//--------------------------------------
//debug_pr("Update is ".$lac->insertApplicationStudentInfo($formData,''));

//------------The final things---------------------
//should imprement the generation of the $applicationID
//$applicationID = uniqid();
$success = true;
if($sys_custom['KIS_Admission']['ICMS']['Settings'])
	$applicationID =$lac->newApplicationNumber2($_REQUEST['SchoolYearID'], $_REQUEST['sus_status']);
else
	$applicationID =$lac->newApplicationNumber2($_REQUEST['SchoolYearID']);
$recordID = mysql_insert_id();
	
if(!$applicationID)
	$success = false;

if(!$lac->insertApplicationAllInfo("",$_REQUEST,$applicationID))
	$success = false;

if(!$recordID)
	$recordID = mysql_insert_id();
	
//$folderPath =$admission_cfg['FilePath']."/".$recordID;
$folderPath =$file_path."/file/admission/".$recordID;

list($filename,$ext) = explode('.',$_FILES['StudentPersonalPhoto']['name']);
$timestamp = date("YmdHis");
$filename = base64_encode($filename.'_'.$timestamp);

//Henry Modifying 20131028
//if(!rename($admission_cfg['FilePath']."/temp/".$_REQUEST['tempFolderName'].'/personal_photo/personal_photo'.$ext, $folderPath.'/personal_photo/'.$filename.'.'.$ext))
if(!$lac->uploadAttachment("personal_photo",$_FILES['StudentPersonalPhoto'], $folderPath.'/personal_photo', $filename))
	$success = false;
else{
	$AttachmentType = "personal_photo";
	$AttachmentName = $filename.'.'.strtolower(getFileExtention($_FILES['StudentPersonalPhoto']['name']));
	//$lac->insertAttachmentRecord($AttachmentType,$AttachmentName, $applicationID);
	$data = array();
	$data['recordID'] = $recordID;
	$data['attachment_type'] = $AttachmentType;
	$data['attachment_name'] = $AttachmentName;
	$lac->saveApplicationAttachment($data);
}

if(!$sys_custom['KIS_Admission']['TBCPK']['Settings']){
	$attachment_settings = $lac->getAttachmentSettings();
	$file_index = 0;
	while(isset($_FILES['OtherFile'.$file_index]) && isset($attachment_settings[$file_index]))
	{
		list($filename,$ext) = explode('.',$_FILES['OtherFile'.$file_index]['name']);
		$timestamp = date("YmdHis");
		$filename = base64_encode($filename.'_'.$timestamp);
		
		if(!$lac->uploadAttachment($attachment_settings[$file_index]['AttachmentName'],$_FILES['OtherFile'.$file_index], $folderPath.'/other_files', $filename))
			$success = false;
		else{
			$AttachmentType = $attachment_settings[$file_index]['AttachmentName'];
			$AttachmentName = $filename.'.'.strtolower(getFileExtention($_FILES['OtherFile'.$file_index]['name']));
			
			$data = array();
			$data['recordID'] = $recordID;
			$data['attachment_type'] = $AttachmentType;
			$data['attachment_name'] = $AttachmentName;
			$lac->saveApplicationAttachment($data);
		}
		$file_index+=1;
	}
}
else{
	list($filename,$ext) = explode('.',$_FILES['OtherFile']['name']);
	$timestamp = date("YmdHis");
	$filename = base64_encode($filename.'_'.$timestamp);
	
	//Henry Modifying 20131028
	//if(!rename($admission_cfg['FilePath']."/temp/".$_REQUEST['tempFolderName'].'/other_files/birth_cert'.$ext, $folderPath.'/other_files/'.$filename.'.'.$ext))	
	if(!$lac->uploadAttachment("birth_cert",$_FILES['OtherFile'], $folderPath.'/other_files', $filename))
		$success = false;
	else{
		$AttachmentType = "birth_cert";
		$AttachmentName = $filename.'.'.strtolower(getFileExtention($_FILES['OtherFile']['name']));
		//$lac->insertAttachmentRecord($AttachmentType,$AttachmentName, $applicationID);
		$data = array();
		$data['recordID'] = $recordID;
		$data['attachment_type'] = $AttachmentType;
		$data['attachment_name'] = $AttachmentName;
		$lac->saveApplicationAttachment($data);
	}
	
	list($filename,$ext) = explode('.',$_FILES['OtherFile1']['name']);
	$timestamp = date("YmdHis");
	$filename = base64_encode($filename.'_'.$timestamp);
	
	//Henry Modifying 20131028
	//if(!rename($admission_cfg['FilePath']."/temp/".$_REQUEST['tempFolderName'].'/other_files/immunisation_record'.$ext, $folderPath.'/other_files/'.$filename.'.'.$ext))
	if(!$lac->uploadAttachment("immunisation_record",$_FILES['OtherFile1'], $folderPath.'/other_files', $filename))
		$success = false;
	else{
		$AttachmentType = "immunisation_record";
		$AttachmentName = $filename.'.'.strtolower(getFileExtention($_FILES['OtherFile1']['name']));
		//$lac->insertAttachmentRecord($AttachmentType,$AttachmentName, $applicationID);
		$data = array();
		$data['recordID'] = $recordID;
		$data['attachment_type'] = $AttachmentType;
		$data['attachment_name'] = $AttachmentName;
		$lac->saveApplicationAttachment($data);
	}
}

if($success){
	$id = urlencode(getEncryptedText("ApplicationID=".$applicationID."&sus_status=".$_REQUEST['sus_status']."&SchoolYearID=".$_REQUEST['SchoolYearID'],$admission_cfg['FilePathKey']));
	//header("Location: finish.php?ApplicationID=".$applicationID."&sus_status=".$_REQUEST['sus_status']);
	header("Location: finish.php?id=".$id);
}
else{
	$id = urlencode(getEncryptedText("sus_status=".$_REQUEST['sus_status'],$admission_cfg['FilePathKey']));
	//header("Location: finish.php?sus_status=".$_REQUEST['sus_status']);
	header("Location: finish.php?id=".$id);
}
//$lac->uploadAttachment($tempFolderPath, $folderPath);
//--------------------------------------

//--------------------------------------
////should copy the file from temp
//debug_pr("Success to move personal_photo to server?\n".$lfs->lfs_copy($tempFolderPath."/personal_photo", $folderPath."/t001"));
//debug_pr("Success to move other_files to server?\n".$lfs->lfs_copy($tempFolderPath."/other_files", $folderPath."/t001"));
//
/////////////////$lac->uploadAttachment($tempFolderPath, $folderPath);
//debug_pr("Success to remove temp folder?\n".$lfs->folder_remove_recursive($tempFolderPath));
//
//debug_pr("The selected class id is ".$_REQUEST['hidden_class']);
//
//debug_pr("The temp folder path is ".$tempFolderPath);
//
//debug_pr("Infomation in hidden variable");
//debug_pr($formData);
//debug_pr($fileData);
//debug_pr($otherFileData);
//--------------------------------------

//debug_pr($_REQUEST['hidden_file']);
//debug_pr($otherFileData['OtherFile']['tmp_name']);

//debug_pr("Success to upload photo file to temp?\n".move_uploaded_file($otherFileData['OtherFile']['tmp_name'], $PATH_WRT_ROOT."file/kis/admission/".$otherFileData['OtherFile']['name']));      

?>

