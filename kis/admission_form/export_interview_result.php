<?php
//using Henry

/********************
 *
 * Log :
 *
 * Date		2015-08-19 [Henry]
 * 			Copy from export_form.php
 *
 * Date		2015-01-19 [Henry]
 * 			export all data by status
 *
 * Date		2013-12-31 [Henry]
 * 			File Created
 *
 ********************/

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

//for the export
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();
$libkis 	= new kis('');
$libkis_admission = $libkis->loadApp('admission');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$lexport = new libexporttext();

$ExportArr = array();
$exportColumn = array();

if($_SESSION['UserType']==USERTYPE_STAFF){
	# Define Column Title
//	$exportColumn = $lac->getExportInterviewResultHeader();

//	debug_pr($schoolYearID);
//	debug_pr($classLevelID);
//	debug_pr($applicationAry);
//	debug_pr($selectStatus);
//	debug_pr($selectInterviewRound);
//	debug_pr($selectFormQuestionID);
//	debug_pr($selectFormAnswerID);

	if(!$applicationAry){
		$applicationAry = array();
		$result = $lac->getApplicationStatus($schoolYearID,$classLevelID,$applicationID='',$recordID='');
		foreach($result as $aResult){
			if($selectStatus == $aResult['statusCode'] || $selectStatus == '')
				$applicationAry[] = $aResult['RecordID'];
		}
	}
	//all data
	$applicationCnt = count($applicationAry);

	//for($i=0;$i<$applicationCnt;$i++){
		$result = $lac->getExportInterviewResultHeaderData($schoolYearID,$classLevelID,'',$applicationAry, $selectStatus, $selectInterviewStatus, $selectInterviewRound, $selectFormQuestionID, $selectFormAnswerID, $selectInterviewDate);
		//$ExportArr[] = $result[1];
	//}
		$filename = "interview_result_form(".$result[1][0][0].").csv";
	//generate csv content
	$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($result[1], $result[0], "\t", "\r\n", "\t", 0, "9");

	//Output The File To User Browser
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>
