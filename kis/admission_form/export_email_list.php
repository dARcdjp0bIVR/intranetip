<?php
//using Henry

/********************
 * 
 * Log :
 * Date		2016-10-24 [Henry]
 * 			File Created
 * 
 ********************/

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

//for the export
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();
$libkis 	= new kis('');
$libkis_admission = $libkis->loadApp('admission');
$lac		= new admission_cust();

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();

$ExportArr = array();
$exportColumn = array();

if($_SESSION['UserType']==USERTYPE_STAFF){
	# Define Column Title
	$exportColumn[0][] = $kis_lang['form'];
    $exportColumn[0][] = $kis_lang['subject'];
    $exportColumn[0][] = $kis_lang['applicationno'];
    $exportColumn[0][] = $kis_lang['chinesename'];
    $exportColumn[0][] = $kis_lang['englishname'];
    $exportColumn[0][] = $kis_lang['emailaddress'];
    $exportColumn[0][] = $kis_lang['status'];
    $exportColumn[0][] = $kis_lang['sentstatus'];
    $exportColumn[0][] = $kis_lang['lastsenton'];
    $exportColumn[0][] = $kis_lang['acknowledgementtime'];
	
	$classLevel = $lac->getClassLevel();
	
	for($i=0;$i<count($emailAry);$i++){
		$recordID = $emailAry[$i];
		$emailRecordDetail = $libkis_admission->getEmailRecordDetail($recordID);
		$applicationIdAry = $emailRecordDetail['applicationIds'];
		$emailReceivers = $libkis_admission->getEmailReceivers($applicationIdAry,$recordID);
		
		$dataArray[$i][] = $classLevel[$emailReceivers[$i]['YearID']];
		$dataArray[$i][] = $emailReceivers[$i]['Subject'];
		$dataArray[$i][] = $emailReceivers[$i]['ApplicationNo'];
		$dataArray[$i][] = $emailReceivers[$i]['ChineseName'];
		$dataArray[$i][] = $emailReceivers[$i]['EnglishName'];
		$dataArray[$i][] = $emailReceivers[$i]['Email'];
		$dataArray[$i][] = $emailReceivers[$i]['Status'];
		$dataArray[$i][] = $emailReceivers[$i]['SentStatus']=='1'?$kis_lang['success']:$kis_lang['failed'];
		$dataArray[$i][] = $emailReceivers[$i]['DateModified'];
		$dataArray[$i][] = $emailReceivers[$i]['AcknowledgeDate'];
	}

	$filename = "email_list.csv";
	
	//generate csv content
	$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($dataArray, $exportColumn, "\t", "\r\n", "\t", 0, "9");
	
	//Output The File To User Browser
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>