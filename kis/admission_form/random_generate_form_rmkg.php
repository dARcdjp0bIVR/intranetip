<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2015-01-20 [Henry]
 * 			reload page by user will redirect to finish page for CSM cust
 * 
 * Date		2014-07-25 [Henry]
 * 			Add Error Message
 * 
 * Date		2014-06-17 [Henry]
 * 			Allow to use the Integrated Central Server for admission
`* 
 * Date		2014-01-15 [Carlos]
 * 			Modified other attachments section to follow attachment settings
`* 
 * Date		2013-10-08 [Henry]
 * 			File Created
 * 
 ********************/
 
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");

include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");


intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

$formData = array();

$formData['sus_status'] = '77'; //(30, 45, 75, 76, 77) should be modified
$formData['Agree'] = '1';
$formData['ApplyClass'] = '2';
$formData['OthersIsConsiderAlternative'] = 'Y';
$formData['studentssurname_b5'] = '陳';
$formData['studentsfirstname_b5'] = '大亨';
$formData['studentssurname_en'] = 'Chan';
$formData['studentsfirstname_en'] = 'Tai Hung';
$formData['FormStudentInfo_DOB'] = '2012-03-14';//should be modified
$formData['FormStudentInfo_StuGender'] = 'M';
$formData['FormStudentInfo_POB'] = '香港';
$formData['FormStudentInfo_Nationality'] = '中國';
$formData['StudentBirthCertNo'] = 'J1241243'; //should be modified
$formData['FormStudentInfo_HomeTel'] = '23452345';
$formData['HomeAddrRoom'] = '504';
$formData['HomeAddrFloor'] = '5';
$formData['HomeAddrBlock'] = 'A';
$formData['HomeAddrBlding'] = 'Tai Hai';
$formData['HomeAddrStreet'] = 'na';
$formData['HomeAddrDistrict'] = '3';
$formData['PrimaryEmail'] = 'henrychan@g4.broadlearning.com';
$formData['SecondaryEmail'] = 'henrychan@broadlearning.com';
$formData['G1ChineseName'] = 'Chan Tai Tai';
$formData['G2ChineseName'] = 'Chan Ma Ma';
$formData['G1Nationality'] = '中國';
$formData['G2Nationality'] = '中國';
$formData['G1Occupation'] = '老闆';
$formData['G2Occupation'] = '大老闆';
$formData['G1CompanyName'] = 'Boss Company';
$formData['G2CompanyName'] = 'Big Boss Company';
$formData['G1WorkNo'] = '23004322';
$formData['G2WorkNo'] = '25443367';
$formData['G1MobileNo'] = '65433456';
$formData['G2MobileNo'] = '98766789';
$formData['StuMotherTongue'] = '2';
$formData['StuSecondTongue'] = '4';
$formData['StuThirdTongue'] = '1';
$formData['IsKinderU'] = '1';//should be modified
$formData['OthersRelativeStudiedName1'] = 'Chan Tai Tai';//should be modified
$formData['OthersRelativeAge1'] = '36';//should be modified
$formData['OthersRelativeGender1'] = 'M';//should be modified
$formData['OthersRelativeClass1'] = '1|||3';//should be modified
$formData['OthersRelativeStudiedYear1'] = '2012-2013';//should be modified
$formData['OthersRelativeStudiedName2'] = '';
$formData['OthersRelativeAge2'] = '';
$formData['OthersRelativeClass2'] = '';
$formData['OthersRelativeStudiedYear2'] = '';
$formData['OthersRelativeStudiedName3'] = '';
$formData['OthersRelativeAge3'] = '';
$formData['OthersRelativeClass3'] = '';
$formData['OthersRelativeStudiedYear3'] = '';
$formData['OthersRelativeStudiedName4'] = '';
$formData['OthersRelativeAge4'] = '';
$formData['OthersRelativeClass4'] = '';
$formData['OthersRelativeStudiedYear4'] = '';

for($i=0; $i < 490; $i++){
	
	$strings = array('30', '45', '75', '76', '77');
	$key = array_rand($strings);
	$formData['sus_status'] = $strings[$key];
	
	$strings = array('1', '2');
	$key = array_rand($strings);
	$formData['ApplyClass'] = $strings[$key];
	
	$formData['FormStudentInfo_DOB'] = rand_date('2012-01-01', '2014-12-31');
	
	$strings = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T','U', 'V', 'W', 'X', 'Y', 'Z');
	$key = array_rand($strings);
	$prefix = $strings[$key];
	$digits = 7;
	$number = rand(pow(10, $digits-1), pow(10, $digits)-1);
	$formData['StudentBirthCertNo'] = $prefix.$number;
	
	$strings = array('0', '1');
	$key = array_rand($strings);
	$formData['IsKinderU'] = $strings[$key];
	
	$strings = array('0', '1');
	$key = array_rand($strings);
	if( $strings[$key] == '0'){
		$formData['OthersRelativeStudiedName1'] = 'Chan Tai Tai';//should be modified
		$formData['OthersRelativeAge1'] = '36';//should be modified
		$formData['OthersRelativeGender1'] = 'M';//should be modified
		$formData['OthersRelativeClass1'] = '1|||3';//should be modified
		$formData['OthersRelativeStudiedYear1'] = '2012-2013';//should be modified
	}
	else{
		$formData['OthersRelativeStudiedName1'] = '';//should be modified
		$formData['OthersRelativeAge1'] = '';//should be modified
		$formData['OthersRelativeGender1'] = '';//should be modified
		$formData['OthersRelativeClass1'] = '';//should be modified
		$formData['OthersRelativeStudiedYear1'] = '';//should be modified
	}
	
	debug_pr($formData);
	
//	$success = true;
//	
//	$applicationID =$lac->newApplicationNumber2($formData['SchoolYearID']);
//	$recordID = mysql_insert_id();
//		
//	if(!$applicationID)
//		$success = false;
//	
//	if(!$lac->insertApplicationAllInfo("",$formData,$applicationID))
//		$success = false;
	
//	debug_pr($success);
//	debug_pr($applicationID);
}

function rand_date($min_date, $max_date) {
    /* Gets 2 dates as string, earlier and later date.
       Returns date in between them.
    */

    $min_epoch = strtotime($min_date);
    $max_epoch = strtotime($max_date);

    $rand_epoch = rand($min_epoch, $max_epoch);

    return date('Y-m-d', $rand_epoch);
}
?>
<html>
	<head>
		<title>Random Generate Form for RMKG</title>
	</head>
	<body>
		Random Generate Form for RMKG!!!
	</body>
</html>