<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2013-10-08 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/settings.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");
include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/libadmission_ui_cust.php");

intranet_opendb();

$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();
if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool(); 

$libkis_admission = $libkis->loadApp('admission');

$basic_settings = $libkis_admission->getBasicSettings("12");

if($libkis_admission->schoolYearID)
	$instruction = $basic_settings['firstPageContent'];
else
	$instruction = $libkis_admission->displayWarningMsg('notyetsetnextschoolyear');


$main_content = '<form name="form1" method="POST" action="confirm2.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     

//The instruction page	
$main_content .= "<div id='step_instruction' style='display:auto'>";
$main_content .= $lauc->getInstructionContent($instruction);
//$main_content .=$li->GET_ACTION_BTN('Back', "button", "goto('step_instruction','step_input_form')", "SubmitBtn", "", 0, "formbutton");
$main_content .=$li->GET_ACTION_BTN('Next', "button", "goto('step_instruction','step_input_form')", "SubmitBtn", "", 0, "formbutton");
$main_content .= "</div>";

//The input info page
$main_content .= "<div id='step_input_form' style='display:none'>";
$main_content .= $lauc->getApplicationForm();
$main_content .=$li->GET_ACTION_BTN('Back', "button", "goto('step_input_form','step_instruction')", "SubmitBtn", "", 0, "formbutton");
$main_content .=$li->GET_ACTION_BTN('Next', "button", "goto('step_input_form','step_docs_upload')", "SubmitBtn", "", 0, "formbutton");
$main_content .= "</div>";

//The docs upload page
$main_content .= "<div id='step_docs_upload' style='display:none'>";
$main_content .= $lauc->getDocsUploadForm();
$main_content .=$li->GET_ACTION_BTN('Back', "button", "goto('step_docs_upload','step_input_form')", "SubmitBtn", "", 0, "formbutton");
//$main_content .=$li->GET_ACTION_BTN('Next', "button", "goto('step_docs_upload','step_confirm')", "SubmitBtn", "", 0, "formbutton");
$main_content .= "</div>";

$main_content .= '</form>';

////The confirm page
//$main_content .= "<div id='step_confirm' style='display:none'>";
//$main_content .= $lauc->getConfirmPageContent();
//$main_content .=$li->GET_ACTION_BTN('Next', "button", "goto('step_confirm','step_instruction')", "SubmitBtn", "", 0, "formbutton");
//$main_content .= "</div>";

debug_pr("The selected class id is ".$_REQUEST['hidden_class']);

include_once("common_tmpl.php");
?>
<script type="text/javascript">
function goto(current,page){
	document.getElementById(current).style.display = "none";
	document.getElementById(page).style.display = "";
}
function checkForm(form1) {
	//For debugging only
	return true;
	
//	form1.hidden_data.value = $("form").serialize();
//	form1.hidden_file.value = form1.StudentPersonalPhoto.files[0];
//	alert(form1.hidden_data.value);
	
	//alert('File name is' + form1.StudentPersonalPhoto.files[0].name + '\nFile size is' + form1.StudentPersonalPhoto.files[0].size);
	
	var studentDateOfBirth = form1.StudentDateOfBirth.value;
	//var othersApplyDate = form1.OthersApplyDate.value;
	var today = '<?=date('Y-m-d')?>';
	if(form1.StudentPersonalPhoto.files[0]){
	var studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
	var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
	}
	var maxFileSize = 10 * 1024 * 1024;
	var hasPG = 1;

	if(form1.StudentEngName.value==''){
		alert("<?= $i_alert_pleasefillin?> Student English Name");	
		form1.StudentEngName.focus();
		return false;
	} else if(form1.StudentChiName.value==''){
		alert("<?= $i_alert_pleasefillin?> Student Chinese Name");	
		form1.StudentChiName.focus();
		return false;
	} else if($('input:radio[name=StudentGender]:checked').val() == null){
		alert("<?= $i_alert_pleasefillin?> Student Gender");	
		form1.StudentGender[0].focus();
		return false;
	} else if(form1.StudentDateOfBirth.value==''  || studentDateOfBirth >= today){
		if(studentDateOfBirth >= today){
			alert("Invalid Birthday");
		}
		else{
			alert("<?= $i_alert_pleasefillin?> Student Date of Birth");	
		}
		
		form1.StudentDateOfBirth.focus();
		return false;
	} else if(form1.StudentPlaceOfBirth.value==''){
		alert("<?= $i_alert_pleasefillin?> Student Place Of Birth");	
		form1.StudentPlaceOfBirth.focus();
		return false;
	} else if(form1.StudentBirthCertNo.value==''){
		alert("<?= $i_alert_pleasefillin?> Student Birth Cert No");	
		form1.StudentBirthCertNo.focus();
		return false;
	} else if(form1.StudentHomeAddress.value==''){
		alert("<?= $i_alert_pleasefillin?> Student Home Address");	
		form1.StudentHomeAddress.focus();
		return false;
	} else if(form1.StudentPersonalPhoto.value==''){
		alert("<?= $i_alert_pleasefillin?> Student Personal Photo");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
		alert("<?= $i_alert_pleasefillin?> Invalid file format");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(studentPhotoSize > maxFileSize){
		alert("<?= $i_alert_pleasefillin?> File too large");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(form1.G1Relationship.value==0 && form1.G2Relationship.value==0 && form1.G3Relationship.value==0){
			alert("<?= $i_alert_pleasefillin?> G1Relationship");
			form1.G1Relationship.focus();
			return false;
	} else if(form1.G1EnglishName.value=='' || form1.G1ChineseName.value=='' || form1.G1Occupation.value=='' || form1.G1CompanyNo.value=='' && form1.G1MobileNo.value==''){
		hasPG = 0;
		if(form1.G1EnglishName.value==''){
			alert("<?= $i_alert_pleasefillin?> G1EnglishName");
			form1.G1EnglishName.focus();
			return false;
		}
		else if(form1.G1ChineseName.value==''){
			alert("<?= $i_alert_pleasefillin?> G1ChineseName");
			form1.G1ChineseName.focus();
			return false;
		}
		else if(form1.G1Occupation.value==''){
			alert("<?= $i_alert_pleasefillin?> G1Occupation");
			form1.G1Occupation.focus();
			return false;
		}
		else if(form1.G1CompanyNo.value=='' && form1.G1MobileNo.value==''){
			alert("<?= $i_alert_pleasefillin?> G1CompanyNo or G1MobileNo");
			form1.G1CompanyNo.focus();
			return false;
		}
	} if(form1.G2EnglishName.value=='' || form1.G2ChineseName.value=='' || form1.G2Occupation.value=='' || form1.G2CompanyNo.value=='' && form1.G2MobileNo.value==''){
		hasPG = 0;
		if(form1.G2EnglishName.value==''){
			alert("<?= $i_alert_pleasefillin?> G2EnglishName");
			form1.G2EnglishName.focus();
			return false;
		}
		else if(form1.G2ChineseName.value==''){
			alert("<?= $i_alert_pleasefillin?> G2ChineseName");
			form1.G2ChineseName.focus();
			return false;
		}
		else if(form1.G2Occupation.value==''){
			alert("<?= $i_alert_pleasefillin?> G2Occupation");
			form1.G2Occupation.focus();
			return false;
		}
		else if(form1.G2CompanyNo.value=='' && form1.G2MobileNo.value==''){
			alert("<?= $i_alert_pleasefillin?> G2CompanyNo or G2MobileNo");
			form1.G2CompanyNo.focus();
			return false;
		}
	} if(form1.G3Relationship.value=='' || form1.G3EnglishName.value=='' || form1.G3ChineseName.value=='' || form1.G3Occupation.value=='' || form1.G3CompanyNo.value=='' && form1.G3MobileNo.value==''){
		hasPG = 0;
		if(form1.G3EnglishName.value==''){
			alert("<?= $i_alert_pleasefillin?> G3EnglishName");
			form1.G3EnglishName.focus();
			return false;
		}
		else if(form1.G3ChineseName.value==''){
			alert("<?= $i_alert_pleasefillin?> G3ChineseName");
			form1.G3ChineseName.focus();
			return false;
		}
		else if(form1.G3Relationship.value==''){
			alert("<?= $i_alert_pleasefillin?> G3Relationship");
			form1.G3Relationship.focus();
			return false;
		}
		else if(form1.G3Occupation.value==''){
			alert("<?= $i_alert_pleasefillin?> G3Occupation");
			form1.G3Occupation.focus();
			return false;
		}
		else if(form1.G3CompanyNo.value=='' && form1.G3MobileNo.value==''){
			alert("<?= $i_alert_pleasefillin?> G3CompanyNo or G3MobileNo");
			form1.G3CompanyNo.focus();
			return false;
		}
	}
	if(hasPG == 0){
		alert("<?= $i_alert_pleasefillin?> At least one guardian!");
		form1.G1EnglishName.focus();
		return false;
	}
	
	 if(form1.OthersFamilyStatus_EB.value==''){
		alert("<?= $i_alert_pleasefillin?> OthersFamilyStatus_EB");
		form1.OthersFamilyStatus_EB.focus();
		return false;
	} else if(form1.OthersFamilyStatus_ES.value==''){
		alert("<?= $i_alert_pleasefillin?> OthersFamilyStatus_ES");
		form1.OthersFamilyStatus_ES.focus();
		return false;
	} else if(form1.OthersFamilyStatus_YB.value==''){
		alert("<?= $i_alert_pleasefillin?> OthersFamilyStatus_YB");
		form1.OthersFamilyStatus_YB.focus();
		return false;
	} else if(form1.OthersFamilyStatus_YS.value==''){
		alert("<?= $i_alert_pleasefillin?> OthersFamilyStatus_YS");
		form1.OthersFamilyStatus_YS.focus();
		return false;
	} else if(form1.OthersApplyYear.value==''){
		alert("<?= $i_alert_pleasefillin?> Invalid Year");
		form1.OthersApplyYear.focus();
		return false;
	} else if(form1.OthersApplyMonth.value==''){
		alert("<?= $i_alert_pleasefillin?> Invalid Month");
		form1.OthersApplyMonth.focus();
		return false;
	} else if($('input:radio[name=OthersApplyTerm]:checked').val() == null){
		alert("<?= $i_alert_pleasefillin?> OthersApplyTerm");
		form1.OthersApplyTerm[0].focus();
		return false;
	} else if(form1.OthersApplyDayType1.value != null && form1.OthersApplyDayType1.value == ""){
		alert("<?= $i_alert_pleasefillin?> OthersApplyDayType1");
		form1.OthersApplyDayType1.focus();
		return false;
	} else if(form1.OthersApplyDayType2.value != null && form1.OthersApplyDayType2.value == ""){
		alert("<?= $i_alert_pleasefillin?> OthersApplyDayType2");
		form1.OthersApplyDayType2.focus();
		return false;
	} else if(form1.OthersApplyDayType3.value != null && form1.OthersApplyDayType3.value == ""){
		alert("<?= $i_alert_pleasefillin?> OthersApplyDayType3");
		form1.OthersApplyDayType3.focus();
		return false;
	} else if(form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value || form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value || form1.OthersApplyDayType1.value == form1.OthersApplyDayType3.value){
		alert("<?= $i_alert_pleasefillin?> Wrong Priory");
		form1.OthersApplyDayType1.focus();
		return false;
	} else if($('input:radio[name=OthersNeedSchoolBus]:checked').val() == null){
		alert("<?= $i_alert_pleasefillin?> OthersNeedSchoolBus");
		form1.OthersNeedSchoolBus[0].focus();
		return false;
	} else if($('input:radio[name=OthersNeedSchoolBus]:checked').val() == '1'){
		if(form1.OthersSchoolBusPlace.value==''){
			alert("<?= $i_alert_pleasefillin?> OthersSchoolBusPlace");
			form1.OthersSchoolBusPlace.focus();
			return false;
		}
	} if($('input:radio[name=OthersKnowUsBy]:checked').val() == null){
		alert("<?= $i_alert_pleasefillin?> OthersKnowUsBy");
		form1.OthersKnowUsBy[0].focus();
		return false;
	} else if($('input:radio[name=OthersKnowUsBy]:checked').val() == '5' && form1.txtOthersKnowUsBy5.value==''){
		alert("<?= $i_alert_pleasefillin?> txtOthersKnowUsBy5");
		form1.txtOthersKnowUsBy5.focus();
		return false;			
	} else if($('input:radio[name=OthersKnowUsBy]:checked').val() == '6' && form1.txtOthersKnowUsBy6.value==''){
		alert("<?= $i_alert_pleasefillin?> txtOthersKnowUsBy6");
		form1.txtOthersKnowUsBy6.focus();
		return false;			
	} else if($('input:radio[name=OthersKnowUsBy]:checked').val() == '7' && form1.txtOthersKnowUsBy7.value==''){
		alert("<?= $i_alert_pleasefillin?> txtOthersKnowUsBy7");
		form1.txtOthersKnowUsBy7.focus();
		return false;			
	} 
	//else {
		return true;
	//}
}
function submit2(){
	$('#setItem').val(1);
	var obj = document.form1;
	if(!checkForm(obj)){
		return false;
	}
	obj.submit();
}
</script>