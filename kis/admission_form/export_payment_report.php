<?php
//using Henry

/********************
 * 
 * Log :
 * Date		2013-12-31 [Henry]
 * 			File Created
 * 
 ********************/

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");

//for the export
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();
$libkis 	= new kis('');
$lac		= new admission_cust();

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();

$ExportArr = array();
$exportColumn = array();

$exportColumn[0][] = $kis_lang['paymentreport'].' ('.$kis_lang['from'].' '.$start_date.' '.$kis_lang['to'].' '.$end_date.')';

if($_SESSION['UserType']==USERTYPE_STAFF){
	# Define Column Title
	//$exportColumn = $lac->getExportHeader();
	
	$exportColumn[5][] = $kis_lang['date'];
	$exportColumn[5][] = $kis_lang['applicationno'];
	$exportColumn[5][] = $kis_lang['studentname'];
	$exportColumn[5][] = $kis_lang['form'];
	$exportColumn[5][] = $kis_lang['paymentamount'];
	$exportColumn[5][] = $kis_lang['paymentmethod'];
	$exportColumn[5][] = $kis_lang['importRemarks'];
	$exportColumn[5][] = $kis_lang['Admission']['PaymentsStatus'];
	$exportColumn[5][] = $kis_lang['parentorguardian'];
	$exportColumn[5][] = $kis_lang['phoneno'];
	$exportColumn[5][] = $kis_lang['Admission']['RMKG']['schoolyear'];
	$exportColumn[5][] = $kis_lang['Admission']['applicationstatus'];

	$data = array(
		'startDate'=>$start_date,
		'endDate'=>$end_date,
		'keyword'=>$keyword,
		'page'=>$page,
		'amount'=>$amount,
		'order'=>$order,
		'sortby'=>$sortby
	);

	list($total,$paymentReportDetailsAry) = $lac->getPaymentReportDetails($data);
	$count = count($paymentReportDetailsAry);
	$paymentReportDetails = array();
	for($i=0;$i<$count;$i++){
		$_applicationId = (is_numeric($paymentReportDetailsAry[$i]['application_id'])?$paymentReportDetailsAry[$i]['application_id'].' ':$paymentReportDetailsAry[$i]['application_id']);	
		
		$paymentReportDetails[$_applicationId]['date_input'] = $paymentReportDetailsAry[$i]['date_input'];
		$paymentReportDetails[$_applicationId]['student_name'] = $paymentReportDetailsAry[$i]['student_name'];
		$paymentReportDetails[$_applicationId]['year_name'] = $paymentReportDetailsAry[$i]['year_name'];
		$paymentReportDetails[$_applicationId]['payment_amount'] = $paymentReportDetailsAry[$i]['payment_amount'];
		$paymentReportDetails[$_applicationId]['txn_id'] = $paymentReportDetailsAry[$i]['txn_id'];
		$paymentReportDetails[$_applicationId]['remarks'] = $paymentReportDetailsAry[$i]['remarks'];
		$paymentReportDetails[$_applicationId]['parent_name'][] = $paymentReportDetailsAry[$i]['parent_name']; 
		$paymentReportDetails[$_applicationId]['parent_phone'][] = $paymentReportDetailsAry[$i]['parent_phone'];
		$paymentReportDetails[$_applicationId]['apply_year'] = $paymentReportDetailsAry[$i]['apply_year'];
		$paymentReportDetails[$_applicationId]['application_status'] = $paymentReportDetailsAry[$i]['application_status'];
		$paymentReportDetails[$_applicationId]['payment_status'] = $paymentReportDetailsAry[$i]['payment_status']; 																										
	}
	$idx = 0;
	$totalAmount = 0;
	$totalLoanAmount = 0;
	if($paymentReportDetails){
		foreach($paymentReportDetails as $_applicationId => $_paymentReportDetailsAry){					
			$_dateInput = $_paymentReportDetailsAry['date_input'];
			$_studentName = $_paymentReportDetailsAry['student_name'];
			$_yearName = $_paymentReportDetailsAry['year_name'];
			$_paymentAmount = $_paymentReportDetailsAry['payment_amount'];
			$_txn_id = $_paymentReportDetailsAry['txn_id'];
			$_remarks = $_paymentReportDetailsAry['remarks'];
			$_parentName = implode(', ',array_filter($_paymentReportDetailsAry['parent_name']));	
			$_parentPhone = implode(', ',array_filter($_paymentReportDetailsAry['parent_phone']));
			$_admission_year = getAcademicYearByAcademicYearID($_paymentReportDetailsAry['apply_year']);
			$_application_status = $kis_lang['Admission']['Status'][$_paymentReportDetailsAry['application_status']];
			$_payment_status = $_paymentReportDetailsAry['payment_status'];
			
			if($admission_cfg['Status'][$_paymentReportDetailsAry['application_status']] >= $admission_cfg['Status']['paymentsettled'])
				$totalAmount += $_paymentAmount;
			else{
				$_paymentAmount = 0 - $_paymentAmount;
				$totalLoanAmount += $_paymentAmount;
			}
						
			$dataArray[$idx][] = substr($_dateInput, 0, 10);
			$dataArray[$idx][] = $_applicationId;
			$dataArray[$idx][] = $sys_custom['KIS_Admission']['ICMS']['Settings'] || $sys_custom['KIS_Admission']['CSM']['Settings'] || $sys_custom['KIS_Admission']['MUNSANG']['Settings']?str_replace(",", " ", $_studentName):$_studentName;
			$dataArray[$idx][] = $_yearName;
			$dataArray[$idx][] = $_paymentAmount;
			$dataArray[$idx][] = $_txn_id?'Paypal':'--';
			$dataArray[$idx][] = $_remarks?$_remarks:'--';
			$dataArray[$idx][] = $_payment_status?$_payment_status:'--';
			$dataArray[$idx][] = $_parentName?($sys_custom['KIS_Admission']['ICMS']['Settings']?str_replace(",", " ", $_parentName):$_parentName):'--';
			$dataArray[$idx][] =  $_parentPhone?$_parentPhone:'--';
			$dataArray[$idx][] = $_admission_year;
			$dataArray[$idx][] = $_application_status;
			
			$idx++;

		}
	}
	$exportColumn[1][] = '';
	$exportColumn[2][] = $kis_lang['totalpaymentamount'];
	$exportColumn[2][] = $totalAmount;
	$exportColumn[3][] = $kis_lang['totalloanamount'];
	$exportColumn[3][] = $totalLoanAmount;
	$exportColumn[4][] = '';
	
	$filename = "payment_report.csv";
	
	//generate csv content
	$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($dataArray, $exportColumn, "\t", "\r\n", "\t", 0, "9");
	
	//Output The File To User Browser
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>