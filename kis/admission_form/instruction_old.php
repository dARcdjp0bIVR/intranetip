<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2013-10-08 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."includes/settings.php");
//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/libadmission_ui_cust.php");


intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

//$lang		= $intranet_session_language;

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool(); 

$libkis_admission = $libkis->loadApp('admission');
$basic_settings = $libkis_admission->getBasicSettings();

if($libkis_admission->schoolYearID)
	$instruction = $basic_settings['generalInstruction'];
else
	$instruction = $libkis_admission->displayWarningMsg('notyetsetnextschoolyear');
	
$main_content = $lauc->getInstructionContent($instruction);

debug_pr("The selected class id is ".$_REQUEST['sus_status']);

include_once("common_tmpl.php");
?>

