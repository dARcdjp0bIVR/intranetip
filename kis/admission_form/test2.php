<?php
# modifying by: 
 
/********************
 * Log :
 * Date		2014-01-15 [Carlos]
 * 			Modified other attachments section to follow attachment settings
 * 
 * Date		2013-10-09 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

intranet_opendb();

$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$lfs		=new libfilesystem();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool(); 

//$libkis_admission = $libkis->loadApp('admission');

//$fileData = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['hidden_file'])));
//$formData = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['hidden_data'])));
//$otherFileData = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['hidden_other_file'])));
//$tempFolderPath = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['tempFolderPath'])));



//For Debugging only
//$lfs->folder_remove_recursive($admission_cfg['FilePath']."/intranetdata");
//$lfs->lfs_remove($folderPath);
	//$lfs->folder_new($folderPath."/t001");


//$lfs->folder_new($tempFolderPath.'/other_files');

//--------------------------------------
//debug_pr("Update is ".$lac->insertApplicationStudentInfo($formData,''));

//------------The final things---------------------
//should imprement the generation of the $applicationID
//$applicationID = uniqid();
$success = true;

echo $lac->newApplicationNumber2('20', '9');
?>