<?php
# modifying by: Pun
 
/********************
 * Log :
 * Date		2013-10-21 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";



include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");


#### Lang START ####
if($intranet_session_language == 'en'){
    $intranet_session_language = 'b5';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
    unset($Lang);
    unset($kis_lang);

    $intranet_session_language = 'en';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
}else{
    $intranet_session_language = 'en';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
    unset($Lang);
    unset($kis_lang);

    $intranet_session_language = 'b5';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
}
#### Lang END ####
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");



intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();
$lfs		=new libfilesystem();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$libkis_admission = $libkis->loadApp('admission');

////////////////////////////////////////// Henry Modifying
////upload the file to temp folder
////The path to store the uploaded files temporary
//$tempFolderPath =$admission_cfg['FilePath']."/temp/".uniqid();
//
////For debugging only
////$lfs->folder_remove_recursive($admission_cfg['FilePath']."/temp");
//
////Create a temp folder
//while($lfs->folder_new($tempFolderPath.'/personal_photo/') == 0){
//	$tempFolderPath =$admission_cfg['FilePath']."/temp/".uniqid();
//}
////////////////////////////////////////// Henry Modifying

$fileData = $_REQUEST;
$formData = $_REQUEST;

echo $lauc->getInputFormPageContent();
//Henry Modifying 20131028
//list($filename,$ext) = explode('.',$_REQUEST['StudentPersonalPhoto']);
//echo '<a href="/file/admission/temp/'.$_REQUEST['tempFolderName'].'/personal_photo/personal_photo.'.$ext.'" target="_blank">'.$_REQUEST['StudentPersonalPhoto'].'</a><br/>';
//list($filename,$ext) = explode('.',$_REQUEST['OtherFile']);
//echo '<a href="/file/admission/temp/'.$_REQUEST['tempFolderName'].'/other_files/birth_cert.'.$ext.'" target="_blank">'.$_REQUEST['OtherFile'].'</a><br/>';
//list($filename,$ext) = explode('.',$_REQUEST['OtherFile1']);
//echo '<a href="/file/admission/temp/'.$_REQUEST['tempFolderName'].'/other_files/immunisation_record.'.$ext.'" target="_blank">'.$_REQUEST['OtherFile1'].'</a>';
//echo debug_pr($_REQUEST).debug_pr($_FILES);
?>