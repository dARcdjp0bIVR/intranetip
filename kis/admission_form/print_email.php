<?php
//using 

/**
 * ******************
 * Log :
 * Date	2019-07-11 [Henry]
 * Ignore time checking for getDecryptedText
 * ******************
 */

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

//for tde customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

intranet_opendb();
$libkis 	= new kis('');
$libkis_admission = $libkis->loadApp('admission');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

if($_GET['id']){
	parse_str(getDecryptedText(urldecode($_REQUEST['id']),$admission_cfg['FilePathKey'], 1000000), $output);
//	debug_pr($output);
//	die();
	if($output['RecordID'] == '' && $output['TemplateID'] == ''){
		echo '内容尚未設定。 The content has not been set.';
		die();
	}
	if($output['TemplateID'] > 0){
		$lauc->getPDFContentForEmail('', $output['ApplicationID'], '', '', $output['TemplateID']);
	}
	else {
		$lauc->getPDFContentForEmail($output['RecordID'],$output['ApplicationID']);
	}
}
else if($_SESSION['UserType']==USERTYPE_STAFF){
	//debug_pr($_REQUEST);
	//
	$Message = stripslashes(rawurldecode($Message));
	//$receiverAry = $libkis_admission->getEmailReceivers($applicationIds,$recordID);
	//$replaced_text = $libkis_admission->replaceEmailVariables($message, $receiverAry[0]['ChineseName'], $receiverAry[0]['EnglishName'], $receiverAry[0]['ApplicationNo'], $receiverAry[0]['Status'], $receiverAry[0]['InterviewDate'], $receiverAry[0]['InterviewLocation'],$receiverAry[0]['InterviewSettingID'], $receiverAry[0]['InterviewSettingID2'], $receiverAry[0]['InterviewSettingID3'], $receiverAry[0]['BriefingDate'], $receiverAry[0]['BriefingInfo'], $receiverAry[0]['LangSpokenAtHome']);
	//debug_pr($replaced_text);
	if($applicationIds != ''){
		$applicationIds = explode(',',$applicationIds);
	}
	if($applicant_id != ''){
		$applicationIds = explode(',',$applicant_id);
	}
	$lauc->getPDFContentForEmail($recordID, $applicationIds);
}
?>