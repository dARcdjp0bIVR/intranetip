<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");

/*===========================================================================*/
/*	PHP Barcode Image Generator v1.0 [9/28/2000]
	Copyright (C)2000 by Charles J. Scheffold - cs@wsia.fm

	This code is hereby released into the public domain.
	Use it, abuse it, just don't get caught using it for something stupid.

	The only barcode type currently supported is Code 3 of 9. Don't ask about 
	adding support for others! This is a script I wrote for	my own use. I do 
	plan to add more types as time permits but currently I only require 
	Code 3 of 9 for my purposes. Just about every scanner on the market today
	can read it.

	PARAMETERS:
	-----------
	$barcode	= [required] The barcode you want to generate

	$type		= (default=0) It's 0 for Code 3 of 9 (the only one supported)
	
	$width		= (default=160) Width of image in pixels. The image MUST be wide
				  enough to handle the length of the given value. The default
				  value will probably be able to display about 6 digits. If you
				  get an error message, make it wider!

	$height		= (default=80) Height of image in pixels
	
	$format		= (default=jpeg) Can be "jpeg", "png", or "gif"
	
	$quality	= (default=100) For JPEG only: ranges from 0-100

	$text		= (default=1) 0 to disable text below barcode, >=1 to enable

	NOTE: You must have GD-1.8 or higher compiled into PHP
	in order to use PNG and JPEG. GIF images only work with
	GD-1.5 and lower. (http://www.boutell.com)

	ANOTHER NOTE: If you actually intend to print the barcodes 
	and scan them with a scanner, I highly recommend choosing 
	JPEG with a quality of 100. Most browsers can't seem to print 
	a PNG without mangling it beyond recognition. 

	USAGE EXAMPLES FOR ANY PLAIN OLD HTML DOCUMENT:
	-----------------------------------------------

	<IMG SRC="barcode.php?code=HELLO&quality=75">

	<IMG SRC="barcode.php?code=123456&width=320&height=200">
		
	
*/
/*=============================================================================*/

//-----------------------------------------------------------------------------
// Startup code
//-----------------------------------------------------------------------------

if (!isset ($text)) $text = 1;
if (empty ($quality)) $quality = 100;
if (empty ($width)) $width = 160;
if (empty ($height)) $height = 80;
if (!empty ($format)) $format = strtoupper ($format);

//$linventory	= new libinventory();
//$BarcodeMaxLength = $linventory->getBarcodeMaxLength();
$width = $width + 10 * 10;

switch ($type)
{
	default:
		$type = 1;
	case 1:
		Barcode39 ($barcode, $width, $height, $quality, $format, $text);
		break;		
}

//-----------------------------------------------------------------------------
// Generate a Code 3 of 9 barcode
//-----------------------------------------------------------------------------
function Barcode39 ($barcode, $width, $height, $quality, $format, $text)
{
	switch ($format)
	{
		default:
			$format = "PNG";
		case "JPEG": 
			header ("Content-type: image/jpeg");
			break;
		case "PNG":
			header ("Content-type: image/png");
			break;
		case "GIF":
			header ("Content-type: image/gif");
			break;
	}
	
	$im = ImageCreate ($width, $height)
    or die ("Cannot Initialize new GD image stream");
	$White = ImageColorAllocate ($im, 255, 255, 255);
	$Black = ImageColorAllocate ($im, 0, 0, 0);
	//ImageColorTransparent ($im, $White);
	ImageInterLace ($im, 1);


	$NarrowRatio = 20;
	$WideRatio = 55;
	$QuietRatio = 35;

	$nChars = (strlen($barcode)+2) * ((6 * $NarrowRatio) + (3 * $WideRatio) + ($QuietRatio));
	$Pixels = $width / $nChars;
	$NarrowBar = (int)(20 * $Pixels);
	$WideBar = (int)(55 * $Pixels);
	$QuietBar = (int)(35 * $Pixels);

	$ActualWidth = (($NarrowBar * 6) + ($WideBar*3) + $QuietBar) * (strlen ($barcode)+2);
	
	if (($NarrowBar == 0) || ($NarrowBar == $WideBar) || ($NarrowBar == $QuietBar) || ($WideBar == 0) || ($WideBar == $QuietBar) || ($QuietBar == 0))
	{
		ImageString ($im, 1, 0, 0, "Image is too small!", $Black);
		OutputImage ($im, $format, $quality);
		exit;
	}
	
	$CurrentBarX = (int)(($width - $ActualWidth) / 2);
	$Color = $White;
	$BarcodeFull = "*".strtoupper(rawurldecode($barcode))."*";
	settype ($BarcodeFull, "string");
	
	if ($text != 0)
	{
		$FontNum = 3;
		$FontHeight = ImageFontHeight ($FontNum);
		$FontWidth = ImageFontWidth ($FontNum);
		$CenterLoc = (int)(($width-1) / 2) - (int)(($FontWidth * strlen($BarcodeFull)) / 2);
		ImageString ($im, $FontNum, $CenterLoc, $height-$FontHeight, "$BarcodeFull", $Black);
	}

	for ($i=0; $i<strlen($BarcodeFull); $i++)
	{
		$StripeCode = Code39 ($BarcodeFull[$i]);

		for ($n=0; $n < 9; $n++)
		{
			if ($Color == $White) $Color = $Black;
			else $Color = $White;

			switch ($StripeCode[$n])
			{
				case '0':
					ImageFilledRectangle ($im, $CurrentBarX, 0, $CurrentBarX+$NarrowBar, $height-1-$FontHeight-2, $Color);
					$CurrentBarX += $NarrowBar;
					break;

				case '1':
					ImageFilledRectangle ($im, $CurrentBarX, 0, $CurrentBarX+$WideBar, $height-1-$FontHeight-2, $Color);
					$CurrentBarX += $WideBar;
					break;
			}
		}

		$Color = $White;
		ImageFilledRectangle ($im, $CurrentBarX, 0, $CurrentBarX+$QuietBar, $height-1-$FontHeight-2, $Color);
		$CurrentBarX += $QuietBar;
	}

	OutputImage ($im, $format, $quality);
}

//-----------------------------------------------------------------------------
// Output an image to the browser
//-----------------------------------------------------------------------------
function OutputImage ($im, $format, $quality)
{
	switch ($format)
	{
		case "JPEG": 
			ImageJPEG ($im, "", $quality);
			break;
		case "PNG":
			ImagePNG ($im);
			break;
		case "GIF":
			ImageGIF ($im);
			break;
	}
}

//-----------------------------------------------------------------------------
// Returns the Code 3 of 9 value for a given ASCII character
//-----------------------------------------------------------------------------
function Code39 ($Asc)
{
	switch ($Asc)
	{
		case ' ':
			return "0110001000";	
		case '-':
			return "0100001010";
		case '$':
			return "0101010000"; 		
		case '%':
			return "0001010100"; 
		case '*':
			return "0100101000"; // * Start/Stop
		case '+':
			return "0100010100"; 
		case '|':
			return "0100001010"; 
		case '.':
			return "1100001000"; 
		case '/':
			return "0101000100"; 
		case '0':
			return "0001101000"; 
		case '1':
			return "1001000010"; 
		case '2':
			return "0011000010"; 
		case '3':
			return "1011000000"; 
		case '4':
			return "0001100010"; 
		case '5':
			return "1001100000"; 
		case '6':
			return "0011100000"; 
		case '7':
			return "0001001010"; 
		case '8':
			return "1001001000"; 
		case '9':
			return "0011001000"; 
		case 'A':
			return "1000010010"; 
		case 'B':
			return "0010010010"; 
		case 'C':
			return "1010010000";
		case 'D':
			return "0000110010";
		case 'E':
			return "1000110000";
		case 'F':
			return "0010110000";
		case 'G':
			return "0000011010";
		case 'H':
			return "1000011000";
		case 'I':
			return "0010011000";
		case 'J':
			return "0000111000";
		case 'K':
			return "1000000110";
		case 'L':
			return "0010000110";
		case 'M':
			return "1010000100";
		case 'N':
			return "0000100110";
		case 'O':
			return "1000100100";
		case 'P':
			return "0010100100";
		case 'Q':
			return "0000001110";
		case 'R':
			return "1000001100";
		case 'S':
			return "0010001100";
		case 'T':
			return "0000101100";
		case 'U':
			return "1100000010";
		case 'V':
			return "0110000010";
		case 'W':
			return "1110000000";
		case 'X':
			return "0100100010";
		case 'Y':
			return "1100100000";
		case 'Z':
			return "0110100000";
		default:
			return "0110001000"; 	## treat as 'space' ##
	}
}

?>
