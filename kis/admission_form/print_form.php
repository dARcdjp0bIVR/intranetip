<?php
//using: 

/**
 * ******************
 * Log :
 * Date: 2020-07-02 [Tommy]
 * added interview print for $sys_custom['KIS_Admission']['MINGWAI']['Settings'] and $sys_custom['KIS_Admission']['MINGWAIPE']['Settings']
 * 
 * Date	2019-07-11 [Henry]
 * Ignore time checking for getDecryptedText
 * ******************
 */
 
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

//for tde customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

intranet_opendb();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

$isPDF = $admission_cfg['PrintByPDF'];

$encryptMode = ($_SESSION['UserType']==USERTYPE_STAFF)&&!$_GET['id']?0:1;
//$libkis_admission = $libkis->loadApp('admission');
//$query = 'ApplicationID=SW002/test';
//$privateKey =12345678;
//debug_pr(base64_encode($query));
//debug_pr(base64_decode(base64_encode($query)));
//

if($encryptMode){
	parse_str(getDecryptedText(urldecode($_REQUEST['id']),$admission_cfg['FilePathKey'], 1000000), $output);
	if($output['ApplicationID']=='0'){
		die();
	}
}
	

if($output['lang']){
    $form_lang = $output['lang'];
    
    if($encryptMode && $admission_cfg['MultipleLang']){
        $intranet_session_language = $lauc->getAdmissionLang($output['ApplicationID'], true);
        include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
        include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
        include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");
    }
}

//resend email to applicant
if($_REQUEST['email_resend']){
	$applicationSetting = $lac->getApplicationSetting($output['SchoolYearID']);
	$classLevelID = current($lac->getApplicationOthersInfo($output['SchoolYearID'],'',$output['ApplicationID'],''));
	$lastContent = $applicationSetting[$classLevelID['classLevelID']]['LastPageContent'];
	if($sys_custom['KIS_Admission']['MGF']['Settings'])
		$mail_content = $lauc->getFinishPageEmailContent($output['ApplicationID'], $lastContent, $output['SchoolYearID'], $_REQUEST['sus_status']);
	else
		$mail_content = $lauc->getFinishPageEmailContent($output['ApplicationID'], $lastContent, $output['SchoolYearID']);
	
	$lac->sendMailToNewApplicantWithReceiver($output['ApplicationID'],'',$mail_content, $_REQUEST['email_resend']);
//	debug_pr('Mail sent to '.$_REQUEST['email_resend']);
}
if(!$isPDF){
echo $lauc->getPrintPageCss();
?>
<!--<style type="text/css">
.tg-left { text-align: left; } .tg-right { text-align: right; } .tg-center { text-align: center; }
.tg-bf { font-weight: bold; } .tg-it { font-style: italic; }
.tg-table-plain { border-collapse: collapse; border-spacing: 0; font-size: 70%; font: inherit; width:720px;}
.tg-table-plain td { border: 1px #555 solid; padding: 5px; vertical-align: top; font-size: 13px;}
.print_field_title { background: #EFEFEF}
.print_field_title_main { background:#D7D7D7}
.input_content { background:#FFF; padding:1px 5px; margin-left:5px; margin-right:10px; border-radius:3px;}
.print_field_row1 { width:120px}
.print_field_row2 { width:28px}
.print_field_row3 { width:80px}
.print_field_title_remark { background:#B9B9B9; width:290px;}
.print_field_title_parent{ width:181px;}
@media print
{    
    .print_hide, .print_hide *
    {
        display: none !important;
    }
}
</style>-->

<?
}
$printButton = '<div id="printOption" style="float: right"><table width="90%" align="center" class="print_hide" border="0">
			<tr>
				<td align="right">'.$lauc->GET_ACTION_BTN($kis_lang['print'], "button", "javascript:window.print();").'</td>
			</tr>
		</table></div>';
if(!$isPDF)
echo $printButton;

if($isPDF){
	if($encryptMode){
		$lauc->getPDFContent($output['SchoolYearID'],$output['ApplicationID'], $output['Type'],$form_lang);
	}
	else{
	    if(($sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings']) && sizeof($interviewAry) > 0){
	        $applicationIDAry = $lac->getApplicationByInterviewSetting($interviewAry);
	    }else{
		    $applicationIDAry = $lac->getApplicationNumber($applicationAry);
	    }
		$lauc->getPDFContent($schoolYearID,$applicationIDAry,($type?$type:'Admin'),$form_lang);
	}
}
else{
	//html
	if($encryptMode){
		if($sys_custom['KIS_Admission']['ICMS']['Settings']){
			$x = '<table width="100%" border="0" cellpadding="2" cellspacing="0" valign="top">';
			$x .= '<tr><td>';
			$x .= $lauc->getPrintPageContent($output['SchoolYearID'],$output['ApplicationID'], $output['Type']);
			$x .= '</td></tr>';
			$x .= '</table>';
			echo $x;
		}
		else
			echo $lauc->getPrintPageContent($output['SchoolYearID'],$output['ApplicationID'], $output['Type'],$form_lang);
	}else{
		
		$applicationCnt = count($applicationAry);
		$x = '';
		for($i=0;$i<$applicationCnt;$i++){
			if($i<$applicationCnt-1){
				$page_break = ' style="page-break-after:always" ';
			}else{
				$page_break = '';
			}
			$result = current($lac->getApplicationOthersInfo($schoolYearID,'','',$applicationAry[$i]));
			$x .= '<table width="100%" border="0" cellpadding="2" cellspacing="0" valign="top" '.$page_break.'>';
			$x .= '<tr><td>';
			$x .= $lauc->getPrintPageContent($schoolYearID,$result['applicationID'], $type);
			$x .= '</td></tr>';
			$x .= '</table>';
		}
		echo $x;
		
	}
}

if(!$isPDF){
echo $printButton;
?>
<!--<table class="tg-table-plain">
  <tr>
    <td>中文姓名</td>
    <td></td>
    <td>英文姓名</td>
    <td colspan="2"></td>
    <td rowspan="6">相片</td>
  </tr>
  <tr class="tg-even">
    <td>出生日期</td>
    <td colspan="2"> 年 月 日</td>
    <td>性別</td>
    <td></td>
  </tr>
  <tr>
    <td>出世紙號碼</td>
    <td colspan="2"></td>
    <td>出生地點</td>
    <td></td>
  </tr>
  <tr class="tg-even">
    <td>籍貫</td>
    <td colspan="2"> 省 縣</td>
    <td>住宅電話</td>
    <td></td>
  </tr>
  <tr>
    <td>地址</td>
    <td colspan="4"></td>
  </tr>
  <tr class="tg-even">
    <td>電郵</td>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td>宗教</td>
    <td colspan="2"></td>
    <td>所屬教會</td>
    <td colspan="2"></td>
  </tr>
  <tr class="tg-even">
    <td>曾就讀學校名稱</td>
    <td colspan="2"></td>
    <td>曾就讀級別</td>
    <td colspan="2">初班/幼兒班/低班/高班</td>
  </tr>
</table>

<table class="tg-table-plain">
  <tr>
    <td colspan="2">家長資料</td>
    <td>父親</td>
    <td>母親</td>
    <td>監護人（請註明關係）</td>
  </tr>
  <tr class="tg-even">
    <td colspan="2">姓名</td>
    <td></td>
    <td></td>
    <td>( )</td>
  </tr>
  <tr>
    <td colspan="2">職業</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr class="tg-even">
    <td rowspan="4">辦<br/>事<br/>處</td>
    <td>名稱</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>職位</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr class="tg-even">
    <td rowspan="2">地址</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr class="tg-even">
    <td colspan="2" rowspan="2">聯絡電話</td>
    <td rowspan="2">辦公室：<br/>手  提：</td>
    <td rowspan="2">辦公室：<br/>手  提：</td>
    <td rowspan="2">辦公室：<br/>手  提：</td>
  </tr>
  <tr>
  </tr>
  <tr class="tg-even">
    <td colspan="2">家庭狀況</td>
    <td colspan="3">兄＿＿＿人 姊＿＿＿人 弟＿＿＿人 妹＿＿＿人</td>
  </tr>
</table>
<table class="tg-table-plain">
  <tr>
    <td colspan="4">現欲申請 年___月（上／下學期）入讀：<br/>
（請在適當位置加；如兩者皆選，請以1、2表示優先次序）</td>
    <td>附 註（由校方填寫）</td>
  </tr>
  <tr class="tg-even">
    <td>下午<br/>
初班□</td>
    <td colspan="3">上午班□ 下午班□ 全日班□<br/>
幼兒班□ 低 班□ 高 班□</td>
    <td>報名費收據號碼：<br/>
日期：＿＿＿＿＿＿ 經手人：＿＿＿＿＿</td>
  </tr>
  <tr>
    <td rowspan="2">曾在</td>
    <td rowspan="4">本校<br/>
就讀</td>
    <td>兄姊姓名</td>
    <td></td>
    <td rowspan="2">面試日期：＿＿＿＿年＿_＿_月______日<br/>
上午／下午______時______分</td>
  </tr>
  <tr class="tg-even">
    <td>級  別</td>
    <td></td>
  </tr>
  <tr>
    <td rowspan="2">現在</td>
    <td>兄姊姓名</td>
    <td></td>
    <td rowspan="3">其他備註：（如入學日期）</td>
  </tr>
  <tr class="tg-even">
    <td>級  別</td>
    <td></td>
  </tr>
  <tr>
    <td colspan="4">是否需要乘搭校車？ 需要□ 不需要□<br/>
(*如需要，請填寫乘搭校車地點＿＿＿＿＿＿＿)</td>
  </tr>
  <tr class="tg-even">
    <td colspan="5">認識本校途徑：郵寄單張□ 報章□ 介紹□ 本會網站□ 其他網站□（請註明）_________<br/>
廣告□(請註明)＿＿＿＿＿＿＿＿＿＿＿ 其他＿＿＿＿＿＿＿＿＿＿＿＿</td>
  </tr>
</table>-->
<?}?>