<?php
//using Henry

/********************
 * 
 * Log :
 * Date		2013-12-31 [Henry]
 * 			File Created
 * 
 ********************/

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

//for the export
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

# Temp Assign memory of this page
ini_set("memory_limit", "250M"); 

$lexport = new libexporttext();

$ExportArr = array();
$exportColumn = array();

if($_SESSION['UserType']==USERTYPE_STAFF){
	# Define Column Title
	//$exportColumn = $lac->getExportHeader();
	
	$exportColumn[0][] = $kis_lang['form'];
	$exportColumn[0][] = $kis_lang['interviewdate'];
    $exportColumn[0][] = $kis_lang['timeslot'].'('.$kis_lang['from'].')';
    $exportColumn[0][] = $kis_lang['timeslot'].'('.$kis_lang['to'].')';
    if($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'){
    	$exportColumn[0][] = $kis_lang['interviewroom'];
    }else{
    	$exportColumn[0][] = $kis_lang['sessiongroup'];
    }
    $exportColumn[0][] = $kis_lang['qouta'];
	$exportColumn[0][] = $kis_lang['applicationno'];
	if($sys_custom['KIS_Admission']['KTLMSKG']['Settings']){
		$exportColumn[0][] = $kis_lang['importinterviewfield'][2];
		$exportColumn[0][] = $kis_lang['importinterviewfield'][1];
	}
	else if($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
		$exportColumn[0][] = $Lang['Admission']['chinesename'];
		$exportColumn[0][] = $Lang['Admission']['englishname'];
	}
	else{
		$exportColumn[0][] = $kis_lang['studentname'];
	}
	$exportColumn[0][] = $kis_lang['parentorguardian'];
	$exportColumn[0][] = $kis_lang['phoneno'];
	$exportColumn[0][] = $kis_lang['status'];
	
	if($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
		$exportColumn[0][] = $Lang['Admission']['HKUGAPS']['birthcertno'];
		$exportColumn[0][] = $Lang['Admission']['HKUGAPS']['twinsID'];
		$exportColumn[0][] = $Lang['Admission']['HKUGAPS']['spokenLanguageForInterview'];
		$exportColumn[0][] = $Lang['Admission']['HKUGAPS']['timeslotName'];
	}
//	$exportColumn[0][] = "";
//	$exportColumn[0][] = "";
//	
//	//student info header
//	$exportColumn[0][] = $Lang['Admission']['studentInfo'];
//	for($i=0; $i < count($exportHearder['studentInfo'])-2; $i++){
//		$exportColumn[0][] = "";
//	}
//	
//	//parent info header
//	$exportColumn[0][] = $Lang['Admission']['PGInfo']."(".$Lang['Admission']['PG_Type']['F'].")";
//	for($i=0; $i < count($exportHearder['parentInfoF'])-1; $i++){
//		$exportColumn[0][] = "";
//	}
//	$exportColumn[0][] = $Lang['Admission']['PGInfo']."(".$Lang['Admission']['PG_Type']['M'].")";
//	for($i=0; $i < count($exportHearder['parentInfoM'])-1; $i++){
//		$exportColumn[0][] = "";
//	}
//	$exportColumn[0][] = $Lang['Admission']['PGInfo']."(".$Lang['Admission']['PG_Type']['G'].")";
//	for($i=0; $i < count($exportHearder['parentInfoG'])-1; $i++){
//		$exportColumn[0][] = "";
//	}
//	
//	//other info header
//	$exportColumn[0][] = $Lang['Admission']['otherInfo'];
//	for($i=0; $i < count($exportHearder['otherInfo'])-1; $i++){
//		$exportColumn[0][] = "";
//	}
//	
//	//official use header
//	$exportColumn[0][] = $kis_lang['remarks'];
//	for($i=0; $i < count($exportHearder['officialUse'])-1; $i++){
//		$exportColumn[0][] = "";
//	}
//	
//	//sub header
//	$exportColumn[1] = array_merge(array($exportHearder[0]), $exportHearder['studentInfo'], $exportHearder['parentInfoF'], $exportHearder['parentInfoM'], $exportHearder['parentInfoG'], $exportHearder['otherInfo'], $exportHearder['officialUse']);
//	
//	if(!$interviewAry){
//		$applicationAry = array();
		$result = $lac->getInterviewListAry($interviewAry,  $date='', $startTime='', $endTime='', $keyword='', '', '', $round);
//		foreach($result as $aResult){
//			$applicationAry[] = $aResult['RecordID'];
//		}
//	}
	//all data
	$classLevel = $lac->getClassLevel();
	
	for($i=0;$i<count($result);$i++){
		$_applicationId = $result[$i]['application_id'];	
		
		$applicationDetails[$_applicationId]['ClassLevelID'] = $classLevel[$result[$i]['ClassLevelID']];
		$applicationDetails[$_applicationId]['Date'] = $result[$i]['Date'];
		$applicationDetails[$_applicationId]['StartTime'] = substr($result[$i]['StartTime'], 0, -3);
		$applicationDetails[$_applicationId]['EndTime'] = substr($result[$i]['EndTime'], 0, -3);
		$applicationDetails[$_applicationId]['Quota'] = $result[$i]['Quota'];
		$applicationDetails[$_applicationId]['GroupName'] = $result[$i]['GroupName'];
		
		if($sys_custom['KIS_Admission']['KTLMSKG']['Settings'] || $sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
			$applicationDetails[$_applicationId]['student_name_b5'] = str_replace(',','',$result[$i]['student_name_b5']);
			$applicationDetails[$_applicationId]['student_name_en'] = $result[$i]['student_name_en'];
		}else{
			$applicationDetails[$_applicationId]['student_name'] = $result[$i]['student_name'];
		}
		if($sys_custom['KIS_Admission']['ICMS']['Settings']){ 
			$applicationDetails[$_applicationId]['ApplyDayType1'] = $result[$i]['ApplyDayType1'];
			$applicationDetails[$_applicationId]['ApplyDayType2'] = $result[$i]['ApplyDayType2'];
			$applicationDetails[$_applicationId]['ApplyDayType3'] = $result[$i]['ApplyDayType3'];
		}
		$applicationDetails[$_applicationId]['parent_name'][] = $result[$i]['parent_name']; 
		$applicationDetails[$_applicationId]['parent_phone'][] = $result[$i]['parent_phone']; 
		$applicationDetails[$_applicationId]['application_status'] = $result[$i]['application_status']; 
		$applicationDetails[$_applicationId]['record_id'] = $result[$i]['record_id'];
		if($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
			$applicationDetails[$_applicationId]['BirthCertNo'] = $result[$i]['BirthCertNo'];
			$applicationDetails[$_applicationId]['TwinsApplicationID'] = $result[$i]['TwinsApplicationID'];
			$applicationDetails[$_applicationId]['LangSpokenAtHome'] = $result[$i]['LangSpokenAtHome'];
		}														
	}
	$idx = 0;
	if($applicationDetails){
		foreach($applicationDetails as $_applicationId => $_applicationDetailsAry){					
			$_recordId = $_applicationDetailsAry['record_id'];
			$_status = $_applicationDetailsAry['application_status'];
			if($sys_custom['KIS_Admission']['KTLMSKG']['Settings'] || $sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
				$_studentNameB5 = $_applicationDetailsAry['student_name_b5'];
				$_studentNameEn = $_applicationDetailsAry['student_name_en'];
			}
			else{
				$_studentName = $_applicationDetailsAry['student_name'];
			}
					
			$_parentName = implode(', ',array_filter($_applicationDetailsAry['parent_name']));	
			$_parentPhone = implode(', ',array_filter($_applicationDetailsAry['parent_phone']));
	
			switch($_status){
				case 'pending':$tr_css = ' class="absent"';break;
			//	case 'paymentsettled':$tr_css = '';break;
				case 'waitingforinterview':$tr_css = ' class="waiting"';break;
				case 'confirmed':$tr_css = ' class="done"';break;
				case 'cancelled':$tr_css = ' class="draft"';break;								
			}
			
			$dataArray[$idx][] = $_applicationDetailsAry['ClassLevelID'];
			$dataArray[$idx][] = $_applicationDetailsAry['Date'];
			$dataArray[$idx][] = $_applicationDetailsAry['StartTime'];
			$dataArray[$idx][] = $_applicationDetailsAry['EndTime'];
			$dataArray[$idx][] = $_applicationDetailsAry['GroupName'];
			$dataArray[$idx][] = $_applicationDetailsAry['Quota'];
			$dataArray[$idx][] = $_applicationId;
			if($sys_custom['KIS_Admission']['KTLMSKG']['Settings'] || $sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
				$dataArray[$idx][] = $_studentNameB5;
				$dataArray[$idx][] = $_studentNameEn;
			}
			else{
				$dataArray[$idx][] = $sys_custom['KIS_Admission']['ICMS']['Settings'] || $sys_custom['KIS_Admission']['CSM']['Settings']?str_replace(",", " ", $_studentName):$_studentName;
			}
			$dataArray[$idx][] = $_parentName?($sys_custom['KIS_Admission']['ICMS']['Settings']?str_replace(",", " ", $_parentName):$_parentName):'--';
			$dataArray[$idx][] =  $_parentPhone?$_parentPhone:'--';
			$dataArray[$idx][] = $kis_lang['Admission']['Status'][$_status];
			
			if($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
				$dataArray[$idx][] = $_applicationDetailsAry['BirthCertNo'];
				$dataArray[$idx][] = $_applicationDetailsAry['TwinsApplicationID'];
				$dataArray[$idx][] = $Lang['Admission']['Languages'][$_applicationDetailsAry['LangSpokenAtHome']];
				$dataArray[$idx][] = $lac->getInterviewTimeslotName($_recordId, $round).'-'.$_applicationDetailsAry['GroupName'];
			}
			
			$idx++;
		}
	}

	if(method_exists($lac,'applyFilter')){
        $dataArray = $lac->applyFilter($lac::FILTER_EXPORT_INTERVIEW_LIST,$dataArray);
    }

//	$interviewAryCnt = count($result);
//	$dataArray = array();
//	for($i=0;$i<$interviewAryCnt;$i++){
////		$result = $lac->getExportData($schoolYearID,'','',$applicationAry[$i]);
////		$ExportArr[] = $result;
//		$dataArray[$i][] = $result[$i]['Date'];
//		$dataArray[$i][] = substr($result[$i]['StartTime'], 0, -3)
//		$dataArray[$i][] = substr($result[$i]['EndTime'], 0, -3);
//		$dataArray[$i][] = $result[$i]['Quota'];
//		$dataArray[$i][] = $result[$i]['record_id'];
//		$dataArray[$i][] = $result[$i]['student_name'];
//		$dataArray[$i][] = implode('<br/>',array_filter($result[$i]['parent_name']));	
//		$dataArray[$i][] = implode('<br/>',array_filter($result[$i]['parent_phone']));
//		$dataArray[$i][] = $result[$i]['application_status'];
//	}
	//if($sys_custom['KIS_Admission']['ICMS']['Settings'])
		$filename = "interview_list.csv";
	//else	
		//$filename = "admission_form(".$result[2].").csv";
	
	//generate csv content
	$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($dataArray, $exportColumn, "\t", "\r\n", "\t", 0, "9");
	
	//Output The File To User Browser
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>