<?php
// modifying by: 

/**
 * ******************
 * Log :
 * Date 2017-09-27 [Pun]
 * added checking for birth cert number if the client not using intergrated server
 * Date 2016-06-08 [Henry]
 * added checking for birth cert number if the client not using intergrated server
 * Date 2016-03-14 [Henry]
 * send auto email for TBCPK
 * Date 2015-11-25 [Henry]
 * support attachment settings for tbcpk
 * Date 2015-11-16 [Pun]
 * don't send payment email if the application is admin apply
 * Date 2015-11-04 [Henry]
 * send payment email for $sys_custom['KIS_Admission']['UCCKE']['Settings']
 * Date 2015-10-02 [Henry]
 * send payment email for $sys_custom['KIS_Admission']['MUNSANG']['Settings']
 * Date 2015-09-15 [Henry]
 * added checking !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || $_GET['token']
 * Date 2015-07-17 [Henry]
 * add $sys_custom['KIS_Admission']['KTLMSKG']['Settings'], $sys_custom['KIS_Admission']['RMKG']['Settings'], $sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings'] to not need check HKID with central server
 * Date 2015-08-05 [Pun]
 * add $sys_custom['KIS_Admission']['KTLMSKG']['Settings'] to email
 * Date 2015-07-23 [Omas]
 * add $sys_custom['KIS_Admission']['RMKG']['Settings'] to email
 * Date 2015-07-22 [Pun]
 * add $sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings'] to email
 * Date 2015-07-17 [Henry]
 * add $sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings'] to not need check HKID with central server
 * Date 2015-07-16 [Omas]
 * add $sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings'] to email
 *
 * Date 2014-07-25 [Henry]
 * Auto send email to applicant
 *
 * Date 2014-01-15 [Carlos]
 * Modified other attachments section to follow attachment settings
 *
 * Date 2013-10-09 [Henry]
 * File Created
 *
 * ******************
 */
$PATH_WRT_ROOT = "../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis_ui.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis_utility.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis_apps.php");
include_once ($PATH_WRT_ROOT . "lang/kis/lang_common_" . $intranet_session_language . ".php");
include_once ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_" . $intranet_session_language . ".php");
include_once ($PATH_WRT_ROOT . "lang/admission_lang." . $intranet_session_language . ".php");
include_once ("../config.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");

// for the customization
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/config.php");
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_cust.php");
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_ui_cust.php");

intranet_opendb();

$libkis = new kis('');
$lac = new admission_cust();
$lauc = new admission_ui_cust();
$lfs = new libfilesystem();
// debug_pr($_POST);
// die();
if (! $plugin['eAdmission']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT("", "../");
    exit();
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool();

// $libkis_admission = $libkis->loadApp('admission');

// $fileData = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['hidden_file'])));
// $formData = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['hidden_data'])));
// $otherFileData = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['hidden_other_file'])));
// $tempFolderPath = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['tempFolderPath'])));

// For Debugging only
// $lfs->folder_remove_recursive($admission_cfg['FilePath']."/intranetdata");
// $lfs->lfs_remove($folderPath);
// $lfs->folder_new($folderPath."/t001");

// $lfs->folder_new($tempFolderPath.'/other_files');

// --------------------------------------
// debug_pr("Update is ".$lac->insertApplicationStudentInfo($formData,''));

// # for preview function
$allowToPreview = $lac->IsPreviewPeriod();

if (($_SESSION["platform"] != "KIS" || ! $_SESSION["UserID"] || ! $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || $_GET['token']) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $allowToPreview) {
    header("Location: finish.php");
    exit();
}

// -------------------------------------------------------
// # developing token
// $sys_custom['KIS_Admission']['IntegratedCentralServer'] = true;
$validForAdmission = true;
if ($sys_custom['KIS_Admission']['IntegratedCentralServer'] && ! $sys_custom['KIS_Admission']['MUNSANG']['Settings']) {
    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_HEADER, 0);
    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    
    // set URL and other appropriate options
    
    // curl_setopt($ch, CURLOPT_URL, "http://192.168.0.146:31002/test/queue/reserved.php?token=f590bac6181c28fda4ec6812e7f2ae4f92&FromKIS=1");
    
    // # the host name should be dynamic
    if ($sys_custom['KIS_Admission']['MUNSANG']['Settings']) {
        $formCode = 'QUOTA_FOR_ALL';
    } else {
        $formCode = $_REQUEST['sus_status'];
    }
    
    $fromReceived = count($lac->getApplicationOthersInfo($lac->schoolYearID));
    
    curl_setopt($ch, CURLOPT_URL, $admission_cfg['IntegratedCentralServer'] . "reserved.php?token=" . $_REQUEST['token'] . "&FromKIS=1&FormCode=" . $formCode . "&FilledBy=" . $fromReceived);
    
    // curl_setopt($ch, CURLOPT_URL, $admission_cfg['IntegratedCentralServer']."reserved.php?token=".$_REQUEST['token']."&FromKIS=1");
    
    // grab URL and pass it to the browser
    
    $data_from_checking = curl_exec($ch);
    
    $data_collected = unserialize($data_from_checking);
    // debug_pr($data_collected);
    // $validForAdmission = true;
    // if($data_collected['InDate'] == date('Y-m-d') && $data_collected['timeFrom'] >= date('H:i') && $data_collected['timeTo'] <= date('H:i')){
    // $validForAdmission = true;
    // }
    // debug_r($validForAdmission);
    // debug_pr(date('H:i'));
    // if((!$data_collected['AllowApplyNow'] || $data_collected['ErrorCode'] != '') && ($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"])){
    // //$validForAdmission = false
    // header("location: ".$PATH_WRT_ROOT."/kis/admission_form/access_deny.php?err=".$data_collected['ErrorCode']);
    // }
    $validForAdmission = true;
    
    // if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && ($_REQUEST['sus_status'] != $data_collected['ApplyFor'] || $_REQUEST['StudentBirthCertNo'] != $data_collected['IDNumber'] || !$_REQUEST['token'] || $lac->hasToken($_REQUEST['token']) > 0)){
    if (($_SESSION["platform"] != "KIS" || ! $_SESSION["UserID"] || ! $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || $_GET['token']) && ((! $sys_custom['KIS_Admission']['MGF']['Settings'] && ! $sys_custom['KIS_Admission']['HKUGAPS']['Settings'] && ! $sys_custom['KIS_Admission']['SFAEPS']['Settings'] && ! $sys_custom['KIS_Admission']['KTLMSKG']['Settings'] && ! $sys_custom['KIS_Admission']['RMKG']['Settings'] && ! $sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings'] && ! $sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings'] && ! $sys_custom['KIS_Admission']['YLSYK']['Settings'] && ! $sys_custom['KIS_Admission']['SHCK']['Settings'] && ! $sys_custom['KIS_Admission']['SSGC']['Settings'] && ! $sys_custom['KIS_Admission']['AOGKG']['Settings'] && ! $sys_custom['KIS_Admission']['TSS']['Settings'] && $_REQUEST['StudentBirthCertNo'] != $data_collected['IDNumber']) || ! $_REQUEST['token'] || $lac->hasToken($_REQUEST['token']) > 0 || $data_collected['QuotaReached'] || ! $_REQUEST['sus_status']) || ($_REQUEST['StudentBirthCertNo'] && $lac->hasBirthCertNumber($_REQUEST['StudentBirthCertNo'], $_REQUEST['sus_status']))) {
        $validForAdmission = false;
        header("location: " . $PATH_WRT_ROOT . "/kis/admission_form/index.php?token={$_REQUEST['token']}&err=1");
    }
} else 
    if (($_SESSION["platform"] != "KIS" || ! $_SESSION["UserID"] || ! $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || $_GET['token']) && ! $_REQUEST['sus_status'] || ($_REQUEST['StudentBirthCertNo'] && $lac->hasBirthCertNumber($_REQUEST['StudentBirthCertNo'], $_REQUEST['sus_status']))) {
        $validForAdmission = false;
        header("location: " . $PATH_WRT_ROOT . "/kis/admission_form/index.php?err=2");
        exit();
    }

// for $sys_custom['KIS_Admission']['MUNSANG']['Settings']
if (($_SESSION["platform"] != "KIS" || ! $_SESSION["UserID"] || ! $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || $_GET['token']) && $sys_custom['KIS_Admission']['MUNSANG']['Settings'] && (! $_REQUEST['token'] || $lac->hasToken($_REQUEST['token']) > 0 || ! $_REQUEST['sus_status'])) {
    $validForAdmission = false;
    header("location: " . $PATH_WRT_ROOT . "/kis/admission_form/index.php?token={$_REQUEST['token']}&err=3");
}
// if(false){
if (! $sys_custom['KIS_Admission']['IntegratedCentralServer'] || $validForAdmission) {
    // --------------------------------------------------------
    
    // ------------The final things---------------------
    // should imprement the generation of the $applicationID
    // $applicationID = uniqid();
    
    // Henry added [20140808]
    if ($sys_custom['KIS_Admission']['ApplyQuotaSettings']) {
        $numOFQuotaLeft = $lac->NumOfQuotaLeft($_REQUEST['sus_status'], $_REQUEST['SchoolYearID']);
        if ($numOFQuotaLeft <= 0 /*&& ($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"])*/){
            header("Location: quota_full.php");
            exit();
        }
    }
    
    if ($sys_custom['KIS_Admission']['InterviewSettings']) {
        $numOfInterviewQuotaLeft = $lac->NumOfInterviewQuotaLeft($_REQUEST['InterviewSettingID']);
        if ($numOfInterviewQuotaLeft <= 0) {
            $_REQUEST['InterviewSettingID'] = 0;
            // header("Location: interview_quota_full.php");
            // exit;
        }
    }
    
    $success = true;
    
    if ($sys_custom['KIS_Admission']['CSM']['Settings'] && $_SESSION["platform"] == "KIS" && $_SESSION["UserID"] && trim($_REQUEST['CustApplicationID'])) {
        $applicationID = $lac->newApplicationNumber2($_REQUEST['SchoolYearID'], $_REQUEST['CustApplicationID']);
    } else 
    	if($sys_custom['KIS_Admission']['MUNSANG']['Settings']){
    		$applicationID = $lac->newApplicationNumber2($_REQUEST['SchoolYearID'], $_REQUEST['sus_status'], $_REQUEST['twinsapplicationid']);
    	}
        else if ($sys_custom['KIS_Admission']['ICMS']['Settings'] || $sys_custom['KIS_Admission']['MGF']['Settings'] || $sys_custom['KIS_Admission']['HKUGAPS']['Settings'])
            $applicationID = $lac->newApplicationNumber2($_REQUEST['SchoolYearID'], $_REQUEST['sus_status']);
        else
            $applicationID = $lac->newApplicationNumber2($_REQUEST['SchoolYearID']);
    $recordID = mysql_insert_id();
    
    if (! $applicationID)
        $success = false;
    
    if (! $lac->insertApplicationAllInfo("", $_REQUEST, $applicationID))
        $success = false;
    
    if (! $recordID)
        $recordID = mysql_insert_id();
        
        // $folderPath =$admission_cfg['FilePath']."/".$recordID;
    $folderPath = $file_path . "/file/admission/" . $recordID;
    
    list ($filename, $ext) = explode('.', $_FILES['StudentPersonalPhoto']['name']);
    $timestamp = date("YmdHis");
    $filename = base64_encode('p' . $filename . '_' . $timestamp);
    $filename = str_replace('/', '+', $filename);
    // Henry Modifying 20131028
    // if(!rename($admission_cfg['FilePath']."/temp/".$_REQUEST['tempFolderName'].'/personal_photo/personal_photo'.$ext, $folderPath.'/personal_photo/'.$filename.'.'.$ext))
    if (! $sys_custom['KIS_Admission']['CSM']['Settings'] && ! $sys_custom['KIS_Admission']['YLSYK']['Settings']) {
    	if($_FILES['StudentPersonalPhoto'] && $_FILES['StudentPersonalPhoto']['name'] !=''){
	        if (! $lac->uploadAttachment("personal_photo", $_FILES['StudentPersonalPhoto'], $folderPath . '/personal_photo', $filename))
	            $success = false;
	        else {
	            $AttachmentType = "personal_photo";
	            $AttachmentName = $filename . '.' . strtolower(getFileExtention($_FILES['StudentPersonalPhoto']['name']));
	            // $lac->insertAttachmentRecord($AttachmentType,$AttachmentName, $applicationID);
	            $data = array();
	            $data['recordID'] = $recordID;
	            $data['attachment_type'] = $AttachmentType;
	            $data['attachment_name'] = $AttachmentName;
	            $lac->saveApplicationAttachment($data);
	        }
    	}
    }
    // if(!$sys_custom['KIS_Admission']['TBCPK']['Settings']){
    $attachment_settings = $lac->getAttachmentSettings();
    /*$file_index = 0;
    while (isset($_FILES['OtherFile' . $file_index]) && isset($attachment_settings[$file_index])) {
        if ($_FILES['OtherFile' . $file_index]['name'] != '') {
            list ($filename, $ext) = explode('.', $_FILES['OtherFile' . $file_index]['name']);
            $timestamp = date("YmdHis");
            $filename = base64_encode('o' . $file_index . $filename . '_' . $timestamp);
            $filename = str_replace('/', '+', $filename);
            
            $file = $_FILES['OtherFile' . $file_index];
            $file['image_data'] = $_POST['CropOtherFile' . $file_index];
            
            if (! $lac->uploadAttachment($attachment_settings[$file_index]['AttachmentName'], $file, $folderPath . '/other_files', $filename))
                $success = false;
            else {
                $AttachmentType = $attachment_settings[$file_index]['AttachmentName'];
                $AttachmentName = $filename . '.' . strtolower(getFileExtention($_FILES['OtherFile' . $file_index]['name']));
                
                $data = array();
                $data['recordID'] = $recordID;
                $data['attachment_type'] = $AttachmentType;
                $data['attachment_name'] = $AttachmentName;
                $lac->saveApplicationAttachment($data);
            }
        }
        $file_index += 1;
    }*/
    foreach($_FILES as $fileName => $fileData){
        if(strpos($fileName, 'OtherFile') === 0 && $fileData['name'] !=''){
            $file_index = substr($fileName, strlen('OtherFile'));

            list ($filename, $ext) = explode('.', $_FILES['OtherFile' . $file_index]['name']);
            $timestamp = date("YmdHis");
            $filename = base64_encode('o' . $file_index . $filename . '_' . $timestamp);
            $filename = str_replace('/', '+', $filename);
            
            $file = $_FILES['OtherFile' . $file_index];
            $file['image_data'] = $_POST['CropOtherFile' . $file_index];
            
            if (! $lac->uploadAttachment($attachment_settings[$file_index]['AttachmentName'], $file, $folderPath . '/other_files', $filename))
                $success = false;
            else {
                $AttachmentType = $attachment_settings[$file_index]['AttachmentName'];
                $AttachmentName = $filename . '.' . strtolower(getFileExtention($_FILES['OtherFile' . $file_index]['name']));
            
                $data = array();
                $data['recordID'] = $recordID;
                $data['attachment_type'] = $AttachmentType;
                $data['attachment_name'] = $AttachmentName;
                $lac->saveApplicationAttachment($data);
            }
        }
    }
    
    // auto send the email to applicant
    if ($sys_custom['KIS_Admission']['TBCPK']['Settings'] || $sys_custom['KIS_Admission']['CSM']['Settings'] || $sys_custom['KIS_Admission']['MGF']['Settings'] || $sys_custom['KIS_Admission']['MUNSANG']['Settings'] || $sys_custom['KIS_Admission']['ICMS']['Settings'] || ($sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings'] && ! $sys_custom['KIS_Admission']['StaticApplicationPage']) || $sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings'] || $sys_custom['KIS_Admission']['RMKG']['Settings'] || $sys_custom['KIS_Admission']['KTLMSKG']['Settings'] || $sys_custom['KIS_Admission']['KTLSKS']['Settings'] || ($sys_custom['KIS_Admission']['UCCKE']['Settings'] && ! $lac->isInternalUse($_REQUEST['token'])) || $sys_custom['KIS_Admission']['YLSYK']['Settings'] || ($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] && ! $lac->isInternalUse($_REQUEST['token'])) || ($sys_custom['KIS_Admission']['MINGWAI']['Settings'] && ! $lac->isInternalUse($_REQUEST['token'])) || $sys_custom['KIS_Admission']['SHCK']['Settings'] || $sys_custom['KIS_Admission']['SSGC']['Settings'] || $sys_custom['KIS_Admission']['AOGKG']['Settings'] || $lac->AdmissionFormSendEmail) {
        $applicationSetting = $lac->getApplicationSetting($_REQUEST['SchoolYearID']);
        
        // #### Get last content START #### //
        $lastContent = (trim($applicationSetting[$_REQUEST['sus_status']]['EmailContent']) && trim($applicationSetting[$_REQUEST['sus_status']]['EmailContent']) != '<br />') ? $applicationSetting[$_REQUEST['sus_status']]['EmailContent'] : $applicationSetting[$_REQUEST['sus_status']]['LastPageContent'];
        if ($admission_cfg['MultipleLang']) {
            foreach ($admission_cfg['Lang'] as $index => $lang) {
                if ($lang == $intranet_session_language) {
                    $isEmailContentExists = (trim($applicationSetting[$_REQUEST['sus_status']]["EmailContent{$index}"]) && trim($applicationSetting[$_REQUEST['sus_status']]["EmailContent{$index}"]) != '<br />');
                    
                    if ($isEmailContentExists) {
                        $lastContent = $applicationSetting[$_REQUEST['sus_status']]["EmailContent{$index}"];
                    } else {
                        $lastContent = $applicationSetting[$_REQUEST['sus_status']]["LastPageContent{$index}"];
                    }
                    break;
                }
            }
        }
        // #### Get last content END #### //
        
        if ($sys_custom['KIS_Admission']['MGF']['Settings']) {
            $mail_content = $lauc->getFinishPageEmailContent($applicationID, $lastContent, $_REQUEST['SchoolYearID'], $_REQUEST['sus_status']);
        } elseif ($sys_custom['KIS_Admission']['MUNSANG']['Settings'] || $sys_custom['KIS_Admission']['UCCKE']['Settings']) {
            $mail_content = $lauc->getFinishPageEmailContent($applicationID, $lastContent, $_REQUEST['SchoolYearID'], 1);
        } else {
            $mail_content = $lauc->getFinishPageEmailContent($applicationID, $lastContent, $_REQUEST['SchoolYearID']);
        }
        
        $lac->sendMailToNewApplicant($applicationID, '', $mail_content);
    }
    if ($success) {
        
        // add call back to the central server [start]
        // # preview form function
        if ($sys_custom['KIS_Admission']['IntegratedCentralServer']) {
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_HEADER, 0);
            
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            // set URL and other appropriate options
            
            // curl_setopt($ch, CURLOPT_URL, "http://192.168.0.146:31002/test/queue/reserved.php?token=f590bac6181c28fda4ec6812e7f2ae4f92&FromKIS=1");
            // # the host name should be dynamic
            if ($sys_custom['KIS_Admission']['MUNSANG']['Settings']) {
                $formCode = 'QUOTA_FOR_ALL';
            } else {
                $formCode = $_REQUEST['sus_status'];
            }
            
            $fromReceived = count($lac->getApplicationOthersInfo($lac->schoolYearID));
            
            curl_setopt($ch, CURLOPT_URL, $admission_cfg['IntegratedCentralServer'] . "reserved.php?token=" . $_REQUEST['token'] . "&FromKIS=1&FormCode=" . $formCode . "&FilledBy=" . $fromReceived);
            
            // grab URL and pass it to the browser
            
            $data_from_checking = curl_exec($ch);
            
            // $data_collected = unserialize($data_from_checking);
            // //debug_pr($data_collected);
            //
            // // if($data_collected['InDate'] == date('Y-m-d') && $data_collected['timeFrom'] >= date('H:i') && $data_collected['timeTo'] <= date('H:i')){
            // // $validForAdmission = true;
            // // }
            // // debug_r($validForAdmission);
            // // debug_pr(date('H:i'));
            //
            // if((!$data_collected['AllowApplyNow'] || $data_collected['ErrorCode'] != '' || $lac->hasToken($_GET['token']) > 0) && ($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"])){
            // $validForAdmission = false;
            // //header("location: ".$PATH_WRT_ROOT."/kis/admission_form/access_deny.php?err=".$data_collected['ErrorCode']);
            // if($_GET['token'] && $lac->hasToken($_GET['token']) > 0){
            // $data_collected['ErrorMsg'] = "閣下已遞交網上申請表，申請通知電郵已發送，請檢查閣下在申請表填寫的電郵。";
            // $data_collected['ErrorMsg'] .= "<br/>You have applied the admission! Please check your Email to get the admission Notification!";
            // }
            // $main_content = '<div class="admission_board">';
            // $main_content .= '<div class="admission_complete_msg"><h1>'.$data_collected['ErrorMsg'].'</h1></div>';
            // $main_content .= '</div>';
            // }
        }
        // add call back to the central server [end]
        
        $id = urlencode(getEncryptedText("ApplicationID=" . $applicationID . "&sus_status=" . $_REQUEST['sus_status'] . "&SchoolYearID=" . $_REQUEST['SchoolYearID'], $admission_cfg['FilePathKey']));
        // header("Location: finish.php?ApplicationID=".$applicationID."&sus_status=".$_REQUEST['sus_status']);
        header("Location: finish.php?id=" . $id);
    } else {
        $id = urlencode(getEncryptedText("sus_status=" . $_REQUEST['sus_status'], $admission_cfg['FilePathKey']));
        // header("Location: finish.php?sus_status=".$_REQUEST['sus_status']);
        header("Location: finish.php?id=" . $id);
    }
    // $lac->uploadAttachment($tempFolderPath, $folderPath);
    // --------------------------------------
    
    // --------------------------------------
    // //should copy the file from temp
    // debug_pr("Success to move personal_photo to server?\n".$lfs->lfs_copy($tempFolderPath."/personal_photo", $folderPath."/t001"));
    // debug_pr("Success to move other_files to server?\n".$lfs->lfs_copy($tempFolderPath."/other_files", $folderPath."/t001"));
    //
    // ///////////////$lac->uploadAttachment($tempFolderPath, $folderPath);
    // debug_pr("Success to remove temp folder?\n".$lfs->folder_remove_recursive($tempFolderPath));
    //
    // debug_pr("The selected class id is ".$_REQUEST['hidden_class']);
    //
    // debug_pr("The temp folder path is ".$tempFolderPath);
    //
    // debug_pr("Infomation in hidden variable");
    // debug_pr($formData);
    // debug_pr($fileData);
    // debug_pr($otherFileData);
    // --------------------------------------
    
    // debug_pr($_REQUEST['hidden_file']);
    // debug_pr($otherFileData['OtherFile']['tmp_name']);
    
    // debug_pr("Success to upload photo file to temp?\n".move_uploaded_file($otherFileData['OtherFile']['tmp_name'], $PATH_WRT_ROOT."file/kis/admission/".$otherFileData['OtherFile']['name']));
}