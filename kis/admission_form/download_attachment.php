<?php 
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");

include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");


intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

if(!$_SESSION["UserID"]){
	$time = $_SERVER['REQUEST_TIME'];
	/**
	 * for a 30 minute timeout, specified in seconds
	 */
	$timeout_duration = 3600;
	
	/**
	 * Here we look for the user・s LAST_ACTIVITY timestamp. If
	 * it・s set and indicates our $timeout_duration has passed,
	 * blow away any previous $_SESSION data and start a new one.
	 */
	if (isset($_SESSION['LAST_ACTIVITY']) && ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
		unset($_SESSION['KIS_ApplicationID']);
		unset($_SESSION['KIS_StudentDateOfBirth']);
		unset($_SESSION['KIS_StudentBirthCertNo']);
	}

	/**
	 * Finally, update LAST_ACTIVITY so that our timeout
	 * is based on it and not the user・s login time.
	 */
	$_SESSION['LAST_ACTIVITY'] = $time;

}

$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
	//debug_pr($formData);
	//debug_pr($application_details);
	if(count($application_details) > 0){
		$applicationAttachmentInfo = $lac->getApplicationAttachmentRecord($application_details['ApplyYear'],array('applicationID'=>$application_details['ApplicationID']));
		//debug_pr($applicationAttachmentInfo);
}

if(isset($_SESSION['KIS_ApplicationID']) && $_SESSION['KIS_ApplicationID'] != ''){
	$file = $intranet_root."/file/admission/".$applicationAttachmentInfo[$application_details['ApplicationID']][$_GET['type']]['attachment_link'][0];
	if (file_exists($file)) {
        $filename = basename($file);
	    if($sys_custom['KIS_Admission']['HKUGAC']['Settings']){
            $attachment_settings = $lac->getAttachmentSettings();
            $isExtra = false;
            foreach($attachment_settings as $attachment_setting){
                if($attachment_setting['AttachmentName'] == $_GET['type']){
                    $isExtra = $attachment_setting['IsExtra'];
                    break;
                }
            }

	        $originalFileName = $applicationAttachmentInfo[$application_details['ApplicationID']][$_GET['type']]['original_attachment_name'][0];
            $filename = ($isExtra && $originalFileName)?$originalFileName:$filename;
        }

	    header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename="'.$filename.'"');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($file));
	    readfile($file);
	    exit;
	}
}
else{
	echo '更新已逾時，請重新登入&nbsp;&nbsp;&nbsp;System timeout, please login again.';
}
?>