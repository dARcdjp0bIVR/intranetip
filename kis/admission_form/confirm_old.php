<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2013-10-09 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/settings.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/libadmission_ui_cust.php");

intranet_opendb();

$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$lfs		=new libfilesystem();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool(); 

$libkis_admission = $libkis->loadApp('admission');



$fileData = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['hidden_file'])));
$formData = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['hidden_data'])));
$tempFolderPath = unserialize(stripslashes( htmlspecialchars_decode($_REQUEST['tempFolderPath'])));
//$folderPath =$PATH_WRT_ROOT."file/kis/admission/testing/t001";

$religion_selected = $lac->returnPresetCodeName("RELIGION", $formData['StudentReligion']);

debug_pr("The selected class id is ".$_REQUEST['hidden_class']);

debug_pr("This path of the temp folder\n".$tempFolderPath);

debug_pr("Infomation in hidden variable");
debug_pr($formData);
debug_pr($fileData);
debug_pr($_FILES['OtherFile']);
//debug_pr($fileData['StudentPersonalPhoto']['tmp_name']);
//debug_pr($fileData['StudentPersonalPhoto']['name']);
//debug_pr($formData['StudentEngName']);
//$folderPath = $admission_cfg['FilePath']."/t001/other_files";

//$lfs->folder_remove_recursive($folderPath);
//$lfs->folder_new($folderPath);
//debug_pr($_FILES['OtherFile']);
//debug_pr($_FILES['OtherFile']['tmp_name']);          
//debug_pr(move_uploaded_file($_FILES['OtherFile']['tmp_name'], $folderPath."/".$_FILES['OtherFile']['name']));

//Manage the temp folder and upload the other file
/*$lfs->folder_new($tempFolderPath.'/other_files/');
debug_pr("Success to upload other file to temp?\n".move_uploaded_file($_FILES['OtherFile']['tmp_name'], $tempFolderPath.'/other_files/'.$_FILES['OtherFile']['name']));
*/
//////////////debug_r($lac->uploadTempAttachment("birth_cert",$_FILES['OtherFile'], $tempFolderPath.'/other_files'));
//debug_pr(move_uploaded_file($fileData['StudentPersonalPhoto']['tmp_name'], $folderPath.'/'.$fileData['StudentPersonalPhoto']['name']));
//debug_pr(move_uploaded_file($_FILES['StudentPersonalPhoto']['tmp_name'], $folderPath.'/'.$_FILES['StudentPersonalPhoto']['name']));
$main_content = $lauc->getConfirmPageContent();

include_once("common_tmpl.php");
?>
<script type="text/javascript">
function checkForm(form1) {
	return true;
}
function submit2(){
	$('#setItem').val(1);
	var obj = document.form1;
	if(!checkForm(obj)){
		return false;
	}
	obj.submit();
}
</script>
