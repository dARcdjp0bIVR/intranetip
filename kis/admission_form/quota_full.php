<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2013-12-04 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");

include_once("../config.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

intranet_opendb();

$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$lfs		=new libfilesystem();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool(); 

$libkis_admission = $libkis->loadApp('admission');

$applicationSetting = $lac->getApplicationSetting($lac->schoolYearID);

$lastContent = $applicationSetting[$_REQUEST['sus_status']]['LastPageContent'];

$main_content = $lauc->getQuotaFullPageContent('Admission','');

include_once("common_tmpl.php");
?>

