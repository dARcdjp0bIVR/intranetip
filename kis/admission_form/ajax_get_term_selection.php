<?php
# modifying by: Omas
 
/********************
 * Log :
 * Date		2015-07-15 [Omas]
 * 			added  $sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings'] get a cust drop down list
 * 
 * Date		2013-10-21 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");




intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$libkis_admission = $libkis->loadApp('admission');

$fileData = $_REQUEST;
$formData = $_REQUEST;

$applicationSetting = $libkis_admission->getApplicationSetting($_REQUEST['SchoolYearID']);

$termType = $applicationSetting[$_REQUEST['sus_status']]['TermType'];
$termTypeArr = explode(',',$termType);

$termTypeOption = '';
if($termTypeArr[0]){
	foreach($termTypeArr as $aTermType){
		$termTypeOption .=$li->Get_Radio_Button('OthersApplyTerm'.$aTermType, 'OthersApplyTerm', $aTermType, '0','',$Lang['Admission']['Term'][$aTermType]).' ';
	}
}

echo $termTypeOption;
?>