<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2013-10-21 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");


intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$libkis_admission = $libkis->loadApp('admission');
	
//Get the intruction content
//$applicationSetting = $libkis_admission->getApplicationSetting($libkis_admission->schoolYearID);
$result = $lac->getInterviewResult($_REQUEST['StudentDateOfBirth'], $_REQUEST['StudentBirthCertNo'], $libkis_admission->schoolYearID,1,$_REQUEST['ApplicationNo']);
$result2 = $lac->getInterviewResult($_REQUEST['StudentDateOfBirth'], $_REQUEST['StudentBirthCertNo'], $libkis_admission->schoolYearID,2,$_REQUEST['ApplicationNo']);
$result3 = $lac->getInterviewResult($_REQUEST['StudentDateOfBirth'], $_REQUEST['StudentBirthCertNo'], $libkis_admission->schoolYearID,3,$_REQUEST['ApplicationNo']);


$x .= '<table class="form_table" style="font-size: 13px">';
$x .= '<tr>';
$x .= '<td class="field_title">申請編號 Application #</td>';
$x .= '<td>'.$result['ApplicationID'].'</td>';
$x .= '</tr>';
$x .= '<td class="field_title">申請者姓名 Name of Applicant</td>';
$x .= '<td>'.str_replace(',',' ',$result['ChineseName']).' ('.str_replace(',',' ',$result['EnglishName']).')</td>';
$x .= '</tr>';

if($sys_custom['KIS_Admission']['InterviewSettings']){
	if(method_exists($lauc,'getInterviewResultContent')){
	    $x .= $lauc->getInterviewResultContent($result, $result2, $result3);
	}else{
		$applicationStatus = current($lac->getApplicationStatus($libkis_admission->schoolYearID,'',$result['ApplicationID']));
		$basicSettings = $libkis_admission->getBasicSettings($libkis_admission->schoolYearID,array('firstInterviewAnnouncement', 'secondInterviewAnnouncement', 'thirdInterviewAnnouncement', 'applicationStatusAnnouncement', 'applicationStatusAnnouncementStartDate'));
		if($sys_custom['KIS_Admission']['KTLMSKG']['Settings'] && ($basicSettings['applicationStatusAnnouncement'] || $basicSettings['firstInterviewAnnouncement'] && !$result['Date'] || $basicSettings['secondInterviewAnnouncement'] && !$result2['Date'] || $basicSettings['thirdInterviewAnnouncement'] && !$result3['Date']) && $applicationStatus['statusCode'] >= 8 && $applicationStatus['statusCode'] <= 10){
			$tempLang = $Lang;
			include_once($PATH_WRT_ROOT."lang/admission_lang.en.php");
			$LangEn = $Lang;
			$Lang = $tempLang;
			$x .= '<tr>';
			$x .= '<td class="field_title">報名狀態 Admission Status</td>';
			$x .= '<td>';
			$x .= $Lang['Admission']['Status'][$applicationStatus['status']].' '.$LangEn['Admission']['Status'][$applicationStatus['status']];
			if($applicationStatus['statusCode'] == 9){ //reserve
				$x .= '<br/><br/>';
				$x .= 'Thank you for your child’s attending the interview held earlier for admission to our Primary Section. We would like to inform you that your child’s application for a place of study in our Primary One classes has been placed on the waiting list. We regret to tell that we are currently unable to offer your child a place due to the vast number of applications received, though your child has out-performed many others in the selection process. Subject to adjustments possibly made on the availability of places, applicants on the waiting list may be further notified, according to priority ranked, for the enrolment and the registration arrangements.
				<br/>We would also like to keep you informed that there will be the second phase of accepting the re-submission of applications for the arrangement of an extended interview by those not yet offered a place in this first phase of the admission process in May next year. 	Parents are welcome to resubmit the admission application through school website.
				<br/>We are well aware of the different pace of growth and development of children. The best result obtained by an applicant from the interviews conducted in the two different phases will be extracted for reference. It must however be noted that the waiting list concluded in the first phase and that in the second phase will be merged as one, with the priority given to applicants re-arranged according to their best performance in the interviews.
				<br/>Thank you for your interest in our school once again and we wish your child every success in the future.
				<br/>
				<br/>感謝 閣下對敝校辦學理念的認同與支持， 貴子弟雖於芸芸申請中已屬表現優良，惟抱歉因現時學額有限，故 貴子弟暫只能安排於備取之列。若有學額空缺，將按備取名單的順序通知辦理註冊手續。
				<br/>鑑於兒童個人成長進展因人而異，本校亦接受備取生於第二階段時重新遞交申請，爭取入學機會。 貴家長可考慮於明年五月透過學校網頁重新遞交表格，本校將以兩次申請結果中表現較佳者為準。不擬參加六月備取入學面見的備取生家長則須了解，第一階段之備取名單將與六月之備取名單合併為一，按表現重新排列優次，敬請垂注。
				<br/>再次感謝家長對本校的支持，並謹祝　貴子弟學業進步。
				';
			}
			else if($applicationStatus['statusCode'] == 10){ //notadmitted
				$x .= '<br/><br/>';
				$x .= 'Thank you for your interest in applying for a place of study for your child in our school. We regret to inform you that we are unable to offer your child a place due to the overwhelming number of applications received.
				<br/>We would also like to keep you informed that there will be "Phase II Admission" in May next year. Parents are welcome to resubmit the admission application through school website.
				<br/>Thank you once again for your interest in our school and we wish your child every success in the future.
				<br/>				
				<br/>感謝 閣下對敝校辦學理念的認同與支持，由於是次申請人數眾多，　貴子弟未獲取錄。
				<br/>本校亦接受未獲取錄者於第二階段時重新遞交申請，爭取入學機會。 貴家長可考慮於明年五月份透過學校網頁重新遞交入學申請表格，敬請垂注。
				<br/>再次感謝家長對本校的支持，並謹祝　貴子弟學業進步。
				';
			}
			$x .= '</td>';
			$x .= '</tr>';
			if($applicationStatus['statusCode'] == 8){ //admitted
				$x .= '<tr><td class="field_title">取錄注意事項 Points to note about confirmed</td>';
				$x .= '<td><a href="https://drive.google.com/file/d/1hQ9nr2qNFjFUZlKW_-U5shPcFdy8zH7H/view?usp=sharing" target="_blank">請按此 Please Click Here</a></td></tr>';
			}
		}
		else{ 
			$x .= '<tr>';
			$x .= '<td class="field_title">面試時段 Interview Timeslot'.($result2['Date'] || $result3['Date']?' ('.sprintf($kis_lang['round'],1).' Round 1)':'').'</td>';
			if($basicSettings['firstInterviewAnnouncement'] && $result['Date'])
				$x .= '<td>日期 Date: '.$result['Date'].' <br/>時段 Timeslot: '.substr($result['StartTime'], 0, -3).' ~ '.substr($result['EndTime'], 0, -3).($result['GroupName']?' <br/>'.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?'面試房間 Interview Room':'組別 Group').': '.$result['GroupName']:'').'</td>';
			else
				$x .= '<td>面試時段尚未編排 No interview timeslot assigned</td>';
			$x .= '</tr>';
			if($basicSettings['secondInterviewAnnouncement'] && $result2['Date']){
				$x .= '<td class="field_title">面試時段 Interview Timeslot ('.sprintf($kis_lang['round'],2).' Round 2)</td>';
				$x .= '<td>日期 Date: '.$result2['Date'].' <br/>時段 Timeslot: '.substr($result2['StartTime'], 0, -3).' ~ '.substr($result2['EndTime'], 0, -3).($result2['GroupName']?' <br/>'.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?'面試房間 Interview Room':'組別 Group').': '.$result2['GroupName']:'').'</td>';
				$x .= '</tr>';
			}
			if($basicSettings['thirdInterviewAnnouncement'] && $result3['Date']){
				$x .= '<td class="field_title">面試時段 Interview Timeslot ('.sprintf($kis_lang['round'],3).' Round 3)</td>';
				$x .= '<td>日期 Date: '.$result3['Date'].' <br/>時段 Timeslot: '.substr($result3['StartTime'], 0, -3).' ~ '.substr($result3['EndTime'], 0, -3).($result3['GroupName']?' <br/>'.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?'面試房間 Interview Room':'組別 Group').': '.$result3['GroupName']:'').'</td>';
				$x .= '</tr>';
			}
			if($sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings']){
				if($result['Date'] || $result2['Date'] || $result3['Date']){
					$x .= '</tr>';
					$x .= '<td class="field_title">幼兒面試證  Interview Slip</td>';
					$x .= '<td><a href="'.$lac->getPrintLink2('', $result['ApplicationID'], 'interview_form').'" target="_blank">列印 Print</a></td>';
					$x .= '</tr>';	
				}
				$tempLang = $Lang;
				include_once($PATH_WRT_ROOT."lang/admission_lang.en.php");
				$LangEn = $Lang;
				$Lang = $tempLang;
				$x .= '<tr>';
				$x .= '<td class="field_title">報名狀態 Admission Status</td>';
				$x .= '<td>'.$Lang['Admission']['Status'][$applicationStatus['status']].' '.$LangEn['Admission']['Status'][$applicationStatus['status']].'</td>';
				$x .= '</tr>';
			}
			if($sys_custom['KIS_Admission']['KTLMSKG']['Settings'] && ($basicSettings['firstInterviewAnnouncement'] && $result['Date'] || $basicSettings['secondInterviewAnnouncement'] && $result2['Date'] || $basicSettings['thirdInterviewAnnouncement'] && $result3['Date'])){
				$x .= '<tr>';
				$x .= '<td class="field_title">地點 Venue</td>';
				if($result['Date'] == '2019-10-26' || $result['Date'] == '2020-06-13' || $result2['Date'] == '2020-06-13' || $result3['Date'] == '2020-06-13'){
					$x .= '<td>Kowloon True Light Middle School (Primary Section)<br/>115 Waterloo Road, Kowloon Tong.<br/>九龍真光中學(小學部)九龍窩打老道115號 (<a href="https://goo.gl/maps/gxWiHbUTqTZ19Jzv6" target="_blank">map 地圖</a>)</td>';
				}
				else{
					$x .= '<td>Kowloon True Light School (SECONDARY SECTION)<br/>No. 1 True Light Lane, Suffolk Road, Kowloon Tong, Kowloon<br/>(opposite to the Festival Walk)<br/>九龍真光中學(本校中學部)九龍塘沙福道真光里1號<br/>(九龍塘又一城對面) (<a href="https://goo.gl/maps/GbQfD3WPwym" target="_blank">map 地圖</a>)</td>';
				}
				$x .= '</tr>';
				$x .= '<tr>';
				$x .= '<td class="field_title">面見注意事項 Points to note about the interview</td>';
				if($result3['Date']){
					$x .= '<td><a href="https://drive.google.com/open?id=1S0EiFwBuL7B6gza-qspxSSoqpW_mcx0V" target="_blank">請按此 Please Click Here</a></td>';				
				}
				else if($result2['Date']){
					$x .= '<td><a href="https://drive.google.com/file/d/1Y9izVkllDL_4JQo9gBAMUkt0OkCfvUXk/view?usp=sharing" target="_blank">請按此 Please Click Here</a></td>';				
				}
				else if($result['Date']){
					$x .= '<td><a href="https://drive.google.com/open?id=1GQCKou14kALVAr-H_ksmJUUNHdN4lUKU" target="_blank">請按此 Please Click Here</a></td>';
				}
				$x .= '</tr>';
			}
			
		}
	}
}
else{
	$basicSettings = $libkis_admission->getBasicSettings($libkis_admission->schoolYearID,array('firstInterviewAnnouncement', 'secondInterviewAnnouncement', 'thirdInterviewAnnouncement', 'applicationStatusAnnouncement', 'applicationStatusAnnouncementStartDate'));
	if((!$sys_custom['KIS_Admission']['STCC']['Settings'] && !$sys_custom['KIS_Admission']['UCCKE']['Settings']) || $basicSettings['firstInterviewAnnouncement']){
		$x .= '<tr>';
		$x .= '<td class="field_title">面試資料 Interview Information</td>';
		if($result['InterviewDate']){
			$interviewDateTime = explode(" ", $result['InterviewDate']);
			$x .= '<td>面試日期 Interview Date: '.$interviewDateTime[0].' <br/>面試時間 Interview Time: '.substr($interviewDateTime[1], 0, -3).' <br/>面試地點 Interview Location: '.$result['InterviewLocation'].'</td>';
		}
		else
			$x .= '<td>面試時段尚未編排 No interview timeslot assigned</td>';
		$x .= '</tr>';
	}
	if($sys_custom['KIS_Admission']['UCCKE']['Settings'] && $basicSettings['firstInterviewAnnouncement'] && $result['InterviewDate']){
		$x .= '<tr>';
		$x .= '<td class="field_title">中一面試家長須知 S.1 Admission Interview Parent’s Notice</td>';
		$x .= '<td><a href="https://drive.google.com/open?id=1Bp1SRnGequJKawS2EQyWHVK44EtbsFJN" target="_blank">請按此 Please Click Here</a></td>';				
		$x .= '</tr>';
		$x .= '<tr>';
		$x .= '<td class="field_title">中一面試家長問卷 S.1 Admission Interview Parent’s Questionnaire</td>';
		$x .= '<td><a href="https://drive.google.com/open?id=1I0cwExSq6rLbT2SyBSxobIvjMpO6dJR1" target="_blank">請按此 Please Click Here</a></td>';				
		$x .= '</tr>';
	} 
	$applicationStatus = current($lac->getApplicationStatus($libkis_admission->schoolYearID,'',$result['ApplicationID']));
	if(($sys_custom['KIS_Admission']['STCC']['Settings'] || $sys_custom['KIS_Admission']['UCCKE']['Settings']) && $basicSettings['applicationStatusAnnouncement']){
		$x .= '<tr>';
		$x .= '<td class="field_title">申請結果 Application results</td>';
		$x .= '<td style="font-weight: bold;">';
		
		$tempLang = $Lang;
		include_once($PATH_WRT_ROOT."lang/admission_lang.en.php");
		$LangEn = $Lang;
		$Lang = $tempLang;
		if($basicSettings['applicationStatusAnnouncementStartDate'] > date('Y-m-d H:i:s')){
			if($admission_cfg['Status'][$applicationStatus['status']] == '4' || $admission_cfg['Status'][$applicationStatus['status']] == '8' || $admission_cfg['Status'][$applicationStatus['status']] == '9' || $admission_cfg['Status'][$applicationStatus['status']] == '10'){
				$x .= $Lang['Admission']['Status'][$applicationStatus['status']].' '.$LangEn['Admission']['Status'][$applicationStatus['status']];
			}
			else{
				$x .= '評核中 Pending';
			}
		}
		else if($admission_cfg['Status'][$applicationStatus['status']] == '4'){
			$x .= $Lang['Admission']['Status'][$applicationStatus['status']].' '.$LangEn['Admission']['Status'][$applicationStatus['status']];
		}
		else{
			$x .= '不獲取錄 Unsuccessful';
		}
		$x .= '</td>';
		$x .= '</tr>';
	}
}

$x .= '</table>';

if($sys_custom['KIS_Admission']['MUNSANG']['Settings']){
	$applicationSetting = $lac->getApplicationSetting($output['SchoolYearID']?$output['SchoolYearID']:$lac->schoolYearID);
	$x .= $applicationSetting[$result['ApplyLevel']]['LastPageContent'];
}
echo $result?$x:$result;
//echo debug_pr($_REQUEST).debug_pr($_FILES);
?>