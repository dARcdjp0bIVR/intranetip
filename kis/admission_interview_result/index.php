<?php
# modifying by: 
 
/********************
 * Log :
 * Date		2015-07-22 [Henry]
 * 			File Created
 * 
 ********************/
 
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");

include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");


intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

if(!$sys_custom['KIS_Admission']['STCC']['Settings'] && !$sys_custom['KIS_Admission']['UCCKE']['Settings'] && $sys_custom['KIS_Admission']['AdmissionFormVersion']){
    header("Location: ../admission_interview_result{$sys_custom['KIS_Admission']['AdmissionFormVersion']}");
    exit;
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool();

if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){
    $sql = "SELECT SettingValue FROM ADMISSION_SETTING WHERE SettingName = 'adminModeActive'";
    $isAdminMode = $libkis->returnVector($sql);
    
    $isLogined = $_SESSION["UserID"] == ""? 0:1;
}

//The docs upload page
$main_content .= "<div id='step_index'>";
//$main_content .= $lauc->getConfirmPageContent();
//$main_content .=$li->GET_ACTION_BTN('Back', "button", "goto('step_confirm','step_docs_upload')", "SubmitBtn", "", 0, "formbutton");
//$main_content .=$li->GET_ACTION_BTN('Next', "button", "goto('step_docs_upload','step_confirm')", "SubmitBtn", "", 0, "formbutton");
//$main_content .= '<table cellpadding="10" cellspacing="0">
//	<thead>
//		<tr>
//	    	<th colspan="2">eAdmission 面試編排結果</th>
//		</tr>
//	</thead>
//<tbody>
//<tr><td align="right" nowrap="nowarp">輸入出世紙號碼： </td><td nowrap="nowarp"><input type="text" name="BCN" id="BCN" /><br />不需要輸入括號，例如：1234567(8)，請輸入 "12345678"。</td></tr>
//<tr><td align="right" nowrap="nowarp">輸入生日日期： </td><td nowrap="nowarp"><input type="text" name="BCN2" id="BCN2" /><br /><font size="-1">Please check the information carefully as it cannot edit after submitted. <br />請仔細閲讀提交的資料，提交後將無法作任何更改。</font></td></tr>
//
//<tr>
//  		<td colspan="2" align="center"  class="padtop25 padbottom25">
//			<div><input type="submit" name="Submit" value=" Submit  提交  " style="font-size:18px;FONT-FAMILY: \'微軟正黑體\', Verdana, Arial, Helvetica, Mingliu, Sans-Serif;" /></div>
//		</td>
//		</tr>
//	</tbody>
//</table>';

$star = '<font style="color:red;">*</font>';

$x = '';
$x .= '<div class="admission_board" style="min-height: 150px">';
if($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
	$x .= '<h1>港大同學會小學 面試編排結果<br/>HKUGA Primary School Interview Arrangement Result</h1>';
	
}elseif($sys_custom['KIS_Admission']['SFAEPS']['Settings']){
	$x .= '<h1 style="margin-left:0">聖方濟各英文小學 面試編排結果<br/>St. Francis of Assisi\'s English Primary School Interview Arrangement Result</h1>';

}
else if($sys_custom['KIS_Admission']['STCC']['Settings'] || $sys_custom['KIS_Admission']['UCCKE']['Settings']){
	$libkis_admission = $libkis->loadApp('admission');
	$basicSettings = $libkis_admission->getBasicSettings($libkis_admission->schoolYearID,array('firstInterviewAnnouncement', 'secondInterviewAnnouncement', 'thirdInterviewAnnouncement', 'applicationStatusAnnouncement', 'applicationStatusAnnouncementStartDate'));
	if($basicSettings['applicationStatusAnnouncement']){
		$x .= '<h1>eAdmission 報名結果 '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'-'.date('Y',getEndOfAcademicYear('',$lac->schoolYearID)).'<br/>eAdmission Application Result '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'-'.date('Y',getEndOfAcademicYear('',$lac->schoolYearID)).'</h1>';
	}
	else{
		$x .= '<h1>eAdmission 面試編排結果 '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'-'.date('Y',getEndOfAcademicYear('',$lac->schoolYearID)).'<br/>eAdmission Interview Arrangement Result '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'-'.date('Y',getEndOfAcademicYear('',$lac->schoolYearID)).'</h1>';	
	}
}
else{
	$x .= '<h1>eAdmission 面試編排結果 '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'-'.date('Y',getEndOfAcademicYear('',$lac->schoolYearID)).'<br/>eAdmission Interview Arrangement Result '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'-'.date('Y',getEndOfAcademicYear('',$lac->schoolYearID)).'</h1>';
}

$x .='<form id="form1" name="form1">';
$x .= '<table class="form_table" style="font-size: 13px">';
if($isAdminMode[0] && !$isLogined){
    $sql = "SELECT SettingValue FROM ADMISSION_SETTING WHERE SettingName = 'adminMessage'";
    $adminMessage = $libkis->returnVector($sql);
    
    $x .= "<td>".$adminMessage[0]."</td>";
}else{
if($sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings']){
	$x .= '<tr>';
	$x .= '<td class="field_title">'.$star.'輸入申請編號<br/>Application Number</td>';
	$x .= '<td><input style="width:200px" name="ApplicationNo" type="text" id="ApplicationNo" class="textboxtext" maxlength="64" size="8" autocomplete="off"/></td>';
	$x .= '</tr>';
}
$x .= '<tr>';
if($sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings']){
	$x .= '<td class="field_title">'.$star.'輸入報名時填寫的身份証明文件號碼<br/>HK Birth Certification No./ HKID No./ Others</td>';
}
else if($sys_custom['KIS_Admission']['KTLMSKG']['Settings']){
	$x .= '<td class="field_title">'.$star."輸入申請人出世紙號碼<br/>Applicant's Birth Certificate Number</td>";
}
else if($sys_custom['KIS_Admission']['STCC']['Settings'] || $sys_custom['KIS_Admission']['UCCKE']['Settings']){
	$x .= '<td class="field_title">'.$star."輸入香港身份証號碼<br/>HKID Card No.</td>";
}
else{
	$x .= '<td class="field_title">'.$star.'輸入出世紙號碼<br/>Birth Certificate Number</td>';
}
$x .= '<td><input style="width:200px" name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="64" size="8" autocomplete="off"/><br />不需要輸入括號，例如：A123456(7)，請輸入 "A1234567"。<br/>No need to enter the bracket, e.g. A123456(7). Please enter "A1234567".</td>';
$x .= '</tr>';
$x .= '<tr>';
if($sys_custom['KIS_Admission']['KTLMSKG']['Settings']){
	$x .= '<td class="field_title">'.$star."輸入申請人出生日期 <br/>Applicant's Date of Birth</td>";
}
else{
	$x .= '<td class="field_title">'.$star.'輸入出生日期 Date of Birth</td>';
}
$x .= '<td><input style="width:200px" name="StudentDateOfBirth" type="text" id="StudentDateOfBirth" class="textboxtext" maxlength="10" size="15" autocomplete="off"/>(YYYY-MM-DD)</td>';
$x .= '</tr>';
}
$x .= '</table>';
$x .='</form>';


$x .= '</div>';

if(!$isAdminMode[0] || $isLogined){
$x .= '<div class="edit_bottom">
		'.$lauc->GET_ACTION_BTN($Lang['Btn']['Submit'].' Submit', "button", "goto('step_index','step_result')", "SubmitBtn", "", 0, "formbutton").'
	</div>';
}
$x .= '<p class="spacer"></p>';
	
$x .= '</div>';



$main_content .= $x;

$main_content .= "</div>";

$main_content .= "<div id='step_result' style='display:none'>";

$x = '';
$x .= '<div class="admission_board" style="min-height: 150px">';
if($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
	$x .= '<h1>港大同學會小學 面試編排結果<br/>HKUGA Primary School Interview Arrangement Result</h1>';
	
}elseif($sys_custom['KIS_Admission']['SFAEPS']['Settings']){
    $x .= '<h1>聖方濟各英文小學 面試編排結果<br/>St. Francis of Assisi\'s English Primary School Interview Arrangement Result</h1>';

}
else if($sys_custom['KIS_Admission']['STCC']['Settings'] || $sys_custom['KIS_Admission']['UCCKE']['Settings']){
	$libkis_admission = $libkis->loadApp('admission');
	$basicSettings = $libkis_admission->getBasicSettings($libkis_admission->schoolYearID,array('firstInterviewAnnouncement', 'secondInterviewAnnouncement', 'thirdInterviewAnnouncement', 'applicationStatusAnnouncement', 'applicationStatusAnnouncementStartDate'));
	if($basicSettings['applicationStatusAnnouncement']){
		$x .= '<h1>eAdmission 報名結果 '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'-'.date('Y',getEndOfAcademicYear('',$lac->schoolYearID)).'<br/>eAdmission Application Result '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'-'.date('Y',getEndOfAcademicYear('',$lac->schoolYearID)).'</h1>';
	}
	else{
		$x .= '<h1>eAdmission 面試編排結果 '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'-'.date('Y',getEndOfAcademicYear('',$lac->schoolYearID)).'<br/>eAdmission Interview Arrangement Result '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'-'.date('Y',getEndOfAcademicYear('',$lac->schoolYearID)).'</h1>';	
	}
}
else{
	$x .= '<h1>eAdmission 面試編排結果 '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'<br/>eAdmission Interview Arrangement Result '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'</h1>';
}
//$x .= '<table class="form_table" style="font-size: 13px">';
//$x .= '<tr>';
//$x .= '<td class="field_title">申請編號 Application #</td>';
//$x .= '<td><div id="divInterviewResult"></div></td>';
//$x .= '</tr>';
//$x .= '<td class="field_title">申請者姓名 Name of Applicant</td>';
//$x .= '<td><div id="divInterviewResult"></div></td>';
//$x .= '</tr>';
//$x .= '<tr>';
//$x .= '<td class="field_title">面試時段 Interview Timeslot</td>';
//$x .= '<td><div id="divInterviewResult"></div></td>';
//$x .= '</tr>';
//$x .= '</table>';
$x .='<div id="divInterviewResult"></div>';

$x .= '</div>
	<div class="edit_bottom">
		'.$lauc->GET_ACTION_BTN($Lang['Admission']['finish'].' Finish', "button", "location.href='index.php'", "SubmitBtn", "", 0, "formbutton").'
	</div>
	<p class="spacer"></p>';

$main_content .= $x;

$main_content .= "</div>";


include_once("common_tmpl.php");
//include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/commonJs.php");
?>
<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.datepicker-zh-HK.js"></script>
<script type="text/javascript" src="/templates/kis/js/config.js"></script>
<script type="text/javascript" src="/templates/kis/js/kis.js"></script>
<style>
.ui-autocomplete {max-height: 200px;max-width: 200px;overflow-y: auto;overflow-x: hidden;font-size: 12px;font-family: Verdana, "微軟正黑體";}
.ui-autocomplete-category{font-style: italic;}
.ui-datepicker{font-size: 12px;width: 210px;font-family: Verdana, "微軟正黑體";}
.ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {width:auto;}
.ui-selectable tr.ui-selecting td, .ui-selectable tr.ui-selected td{background-color: #fff7a3}

@media print {
.ui-datepicker {
    display:none;
  }
}

</style>
<script type="text/javascript">

kis.datepicker('#StudentDateOfBirth');

function goto(current,page){

	if(current == 'step_index' && page == 'step_result'){
		<?if($sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings']){?>
		if(form1.ApplicationNo.value==''){
			alert("請輸入申請編號。\nPlease enter Application Number.");	
			form1.ApplicationNo.focus();
			return false;
		}
		else <?}?>if(form1.StudentBirthCertNo.value==''){
			<?if($sys_custom['KIS_Admission']['STCC']['Settings'] || $sys_custom['KIS_Admission']['UCCKE']['Settings']){?>
			alert("<?=$Lang['Admission']['UCCKE']['msg']['enterHKID']?>\nPlease enter HKID Card No.");	
			<?} else {?>
			alert("<?=$Lang['Admission']['munsang']['msg']['enterbirthcertno']?>\nPlease enter Birth Certificate Number.");	
			<?}?>
			form1.StudentBirthCertNo.focus();
			return false;
		}
//		else if(!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test(form1.StudentBirthCertNo.value)){
//			alert("<?=$Lang['Admission']['munsang']['msg']['invalidbirthcertificatenumber']?>\nInvalid Birth Certificate Number.");	
//			form1.StudentBirthCertNo.focus();
//			return false;
//		}
		else if(!form1.StudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
			if(form1.StudentDateOfBirth.value!=''){
				alert("<?=$Lang['Admission']['msg']['invaliddateformat']?>\nInvalid Date Format");
			}
			else{
				alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>\nPlease enter Date of Birth.");	
			}
			
			form1.StudentDateOfBirth.focus();
			return false;
		}
		
		/* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_interview_result.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           if(data==0){
		           		alert("Incorrect <?if($sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings']){echo 'Application Number, ';}?><?=($sys_custom['KIS_Admission']['STCC']['Settings'] || $sys_custom['KIS_Admission']['UCCKE']['Settings']?'HKID Card No.':'Birth Certificate Number')?> or Date of Birth.");
						form1.StudentBirthCertNo.focus();
						return false;
		           }
		           $("#divInterviewResult").html(data);
		           document.getElementById(current).style.display = "none";
				   document.getElementById(page).style.display = "";
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
	}
	
}

</script>