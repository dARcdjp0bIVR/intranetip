<?php
// Editing by: 
/**************************************************************** Change Log ***************************************************************
 * 2019-03-01 (Henry): added eclasslearning.broadlearning.com (192.168.0.33) for testing KIS
 * 2015-11-30 (Henry): flag $sys_custom['KIS_Background_Cust'] for cust background
 * 2015-07-31 (Yuen): Added favicon
 * 2015-07-16 (Carlos): Added secure token to prevent Cross Site Request Forgery attack.
 * 2014-10-24 (Henry): Fixed: Missing the documentation icon
 * 2014-10-23 (Henry): add checking whether the site is kis or not
 * 2014-10-20 (Henry): add Reprint Card System link
 * 2014-08-08 (YatWoon): hide "Documentations" for start alone eAdmission
 * 2014-06-30 (Pun): Add autoLogin at the end of the page(For CMS).
 * 2014-03-27 (Carlos): Temporary hide [Menu] and [Logs], <div class="student_name_btn"> has set inline style padding-right to 0px
 */
 
include("../includes/global.php");
include("../includes/SecureToken.php");
if($plugin["platform"] != "KIS" && $server_ip != '192.168.0.146' && $server_ip != '192.168.0.149' && $server_ip != '192.168.0.171' && $server_ip != '192.168.0.172' && $server_ip != '192.168.0.33' && $server_ip != '192.168.0.166'){
    header("Location: /home/");
}
intranet_opendb();
$LibSecureToken = new SecureToken();

$platformVersion = Get_IP_Version();
$platformVersionAry = explode('.', $platformVersion);
$copyRightYear = 2009 + $platformVersionAry[3];
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>eClass KIS</title>

<link id="page_favicon" href="/images/kis/kis_favicon.png" rel="icon" type="image/x-icon" />


<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/jquery.fancybox.css">
<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/jquery.fancybox-thumbs.css">
<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<link type="text/css" rel="stylesheet" media="screen" href="/templates/kis/css/common.css">
<?if($sys_custom['KIS_Background_Cust']){?> 
<link type="text/css" rel="stylesheet" media="screen" href="/templates/kis/css/cust/<?=key($sys_custom['KIS_Background_Cust'])?>/bg.css">
<?}?>
<script type="text/javascript" src="/lang/script.en.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.address-1.5.min.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.masonry.min.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.datepicker-zh-HK.js"></script>
<script type="text/javascript" src="/templates/jquery/plupload/plupload.full.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
<script type="text/javascript" src="/templates/html_editor/fckeditor.js"></script>
<script type="text/javascript" src="/templates/script.js"></script>
<script type="text/javascript" src="/templates/kis/js/config.js"></script>
<script type="text/javascript" src="/templates/kis/js/kis.js"></script>
<script type="text/javascript" src="/templates/kis/js/kis.myaccount.js"></script>
<style>
.ui-autocomplete {max-height: 200px;max-width: 200px;overflow-y: auto;overflow-x: hidden;font-size: 12px;font-family: Verdana, "微軟正黑體";}
.ui-autocomplete-category{font-style: italic;}
.ui-datepicker{font-size: 12px;width: 210px;font-family: Verdana, "微軟正黑體";}
.ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {width:auto;}
.ui-selectable tr.ui-selecting td, .ui-selectable tr.ui-selected td{background-color: #fff7a3}
</style>
</head>

<body>
    <img src="/images/kis/logo_kis.png" style="position:fixed;top:-100px"/>
    <div id="container" style="display:none">
    <script>
		
	$(function(){
	    
	    kis.init();
	    
	    $(".fancybox-thumb").fancybox({
		prevEffect : 'fade',
		nextEffect : 'fade',
		padding: 0,
		autoSize: true,
		closeBtn: false,
		helpers: {
		    title: {type: 'outside'}
		   
		}
	    });
	    
	    $(".fancybox-ajax").fancybox({
		prevEffect : 'fade',
		nextEffect : 'fade',
		padding: 0,
		type: 'ajax'
	    });
	    
	    $('.btn_logs').click(function(e){
		alert('Coming soon!');
		return false;
	    });
	    
	});
	function checkForgetForm(){
		<?if($_SERVER['SERVER_NAME'] == 'hke-icms.eclass.hk' || $_SERVER['SERVER_NAME'] == 'icms.eclass.hk'){?>
			if($('#Campus').val()+".eclass.hk" != window.location.host){
				window.location.replace("http://"+$('#Campus').val()+".eclass.hk/kis/?reset_pw=1");
				return;
			}
		<?}?>	
     	obj = document.forgot_password_form;
     	tmp = prompt(ForgotPasswordMsg, "");
     	if(tmp!=null && $.trim(tmp)!=""){
          	obj.UserLogin.value = tmp;
          	obj.submit();
     	}
	}
	
	<? if($_SERVER['SERVER_NAME'] == 'hke-icms.eclass.hk' || $_SERVER['SERVER_NAME'] == 'icms.eclass.hk'){?>
    	window.onload=function(){
    		<?if($_GET['reset_pw']){?>
    			setTimeout(function(){
    				obj = document.forgot_password_form;
			     	tmp = prompt(ForgotPasswordMsg, "");
			     	if(tmp!=null && $.trim(tmp)!=""){
			          	obj.UserLogin.value = tmp;
			          	obj.submit();
			     	}
    			},1000);
    			
			<?}?>
		    $('#login_button').click(function(e){
		    	if($('#Campus').val()+".eclass.hk" != window.location.host && $('#login_form input[name="UserLogin"]').val() && $('#login_form input[name="UserPassword"]').val()){
			    	$('#login_form .loading_msg').show();
	    			$('#login_form .error_msg').hide();
			    	$('#hidden_login_form input[name="hiddenUserLogin"]').val($('#login_form input[name="UserLogin"]').val());
			    	$('#hidden_login_form input[name="hiddenUserPassword"]').val($('#login_form input[name="UserPassword"]').val());
			    	$('#hidden_login_form').attr("action","http://"+$('#Campus').val()+".eclass.hk/kis/");
			    	$('#hidden_login_form').submit();
			    	$('#hidden_login_form input[name="hiddenUserLogin"]').removeAttr('value').attr('value','');
					$('#hidden_login_form input[name="hiddenUserPassword"]').removeAttr('value').attr('value','');
					return false;
		    	}
		    });
		    if($('#login_form input[name="UserLogin"]').val() !="" && $('#login_form input[name="UserPassword"]').val() !=""){
				$('#login_form').submit();
//				$('#hidden_login_form input[name="hiddenUserLogin"]').removeAttr('value').attr('value','');
//				$('#login_form input[name="UserLogin"]').removeAttr('value').attr('value','');
//				$('#login_form input[name="UserPassword"]').removeAttr('value').attr('value','');
		    }
    	}
    <?}?>
    </script>
    <div id="top_header">

	<a title="eClass KIS" class="logo" href="./" ></a>

	<div class="school_name"></div>
	<div class="user_btn">
	    
	    <a title="Log Out" class="btn_logout en" href="/logout.php">
		<span style="display:none" class="message">Are you sure to log out?</span>
	    </a>
	    <a title="登出" class="btn_logout b5" href="/logout.php">
		<span style="display:none" class="message">確認要登出嗎?</span>
	    </a>
	    
	    <a title="Change to English Language" class="btn_lang_eng b5" href="/lang.php?lang=en"></a>
	    <a title="切換至中文" class="btn_lang_chn en" href="/lang.php?lang=b5"></a>
	    <? if(!$stand_alone['eAdmission']) {?>
	    <a title="About" class="btn_lang_about" href="javascript:void(0);" onmouseover="MM_showHideLayers('about_layer','','show')"></a>
	    <div id="about_layer" class="layer_admin_group"  onmouseover="MM_showHideLayers('about_layer','','show')"  onmouseout="MM_showHideLayers('about_layer','','hide')" style="left:auto; right:60px; top:35px">
	     	<a href="http://support.broadlearning.com/doc/help/portal/KIS/doc.html" target="_blank" class="support_doc en" style="margin:0; width:auto; height:auto">Documentations</a>
	     	<a href="http://support.broadlearning.com/doc/help/portal/KIS/doc.html" target="_blank" class="support_doc b5" style="margin:0; width:auto; height:auto; float:left">文件下載</a>
	            <p class="spacer"></p>
	     </div>   
	     <? } ?>
	    <span class="sep">|</span>
	    <a title="My Account" class="btn_user_acc en" href="#/myaccount/"></a>
	    <a title="我的帳戶" class="btn_user_acc b5" href="#/myaccount/"></a>
	    <span><em class="user_type"></em>, <em class="user_name"></em></span>            
	</div>

	<div class="parent_btn" style="display:none;">
	    <!--<a class="btn_menu btn_menu_on" href="#"><span class="en">Menu</span><span class="b5">選單</span><em style="display:none"></em></a>-->
	    <div class="student_name_btn" style="padding-right:0px;"><div class="student_name_btn_inside">
		<span class="student_name"><em class=""></em>
		    <span></span>
		    <select class="student_choose" style="display:none">
		
		    </select>
		</span>
		<span class="student_text en" href="#">Student</span>
		<span class="student_text b5" href="#">學生</span>
		<a class="btn_change_student" href="#"></a>
		<!--
		<a class="btn_logs en" href="#/logs/"><span>Logs</span></a>
		<a class="btn_logs b5" href="#/logs/"><span>日誌</span></a>
		-->
		
	    </div></div>
	
	    
	</div>


    </div>
    <div class="container_slider">
	<div id="update_page" class="board" style="opacity:0">
	    <div class="board_top"><div class="board_top_right"><div class="board_top_bg"><div class="board_top_content">
		<div class="update_photo">
		    <img src=""/>
		</div>
		
		<div class="update_info">
			<h1></h1>
			<span class="en">Class</span>
			<span class="b5">班別</span>
			<a class="student_class"></a><br/>
			<span class="en">Class Teacher</span>
			<span class="b5">班主任</span>
			<a class="student_class_teacher"></a>
		</div>
		    
	    </div></div></div></div>
	    <div class="board_main"><div class="board_main_right"><div class="board_main_bg"><div class="board_main_content">
		    
    
	    </div></div></div></div>
	    
	    <div class="board_bottom"><div class="board_bottom_right"><div class="board_bottom_bg"></div></div></div>
	</div>
	
	
	<div id="main_portal" class="board">
		<div class="board_top"><div class="board_top_right"><div class="board_top_bg"><div class="board_top_content">
		
		
		</div></div></div></div>
		<div class="board_main"><div class="board_main_right"><div class="board_main_bg"><div class="board_main_content">
		<!-- setting icons start-->
            <div class="module_admin_group">
            	<ul>
					<li><a href="/home/eClassStore/redirect.php" target="_blank" class="btn_admin_order en" style="background:url(/images/kis/module_icon/icon_setting.png) no-repeat left -400px;">Reprint Card System</a>
						<a href="/home/eClassStore/redirect.php" target="_blank" class="btn_admin_order b5" style="background:url(/images/kis/module_icon/icon_setting.png) no-repeat left -400px;">智能卡訂購系統</a></ul>
                </ul>
            </div>
            <p class="spacer"></p>
            <!-- setting icons end-->
		    <!---->
		    <ul class="module_list">
			
		    </ul>    	
		    <!---->
			     <p class="spacer"></p>
			     
		</div></div></div></div>
		<div class="board_bottom"><div class="board_bottom_right"><div class="board_bottom_bg"></div></div></div>
		
	</div>
        
	<div id="module_page" class="board">
    
	    <div class="board_top"><div class="board_top_right"><div class="board_top_bg"><div class="board_top_content">
		<div class="navigation_bar">
		    <a class="btn_home en" href="#">Home</a>
		    <a class="btn_home b5" href="#">主頁</a>
		    <a class="module_title"></a>
		</div>
		
		<div class="module_admin" style="display:none">
		    <a class="en" title="Admin">Admin</a>
		    <a class="b5" title="管理員">管理員</a>
		</div>
	    </div></div></div></div>
	    
	    <div class="board_main"><div class="board_main_right"><div class="board_main_bg"><div class="board_main_content">
	    </div></div></div></div>
	    
	<div class="board_bottom"><div class="board_bottom_right"><div class="board_bottom_bg"></div></div></div>
		
    </div>
	
    </div>
      <p class="spacer"></p>
    
    </div>
    <div class="board_loading_overlay">
	<div class="b5">載入中...</div>
	<div class="en">Loading...</div>
    </div>
    <div class="attach_file_overlay">
	<div class="b5">將檔案拖放到這裡</div>
	<div class="en">Drop Files Here</div>
    </div>

   <div id="welcome_message" class="bg_message">
    Loading...Please wait
    <p class="logo_kis"></p>
    </div>
 	<?if($_SERVER['SERVER_NAME'] == 'icms.eclass.hk' || $_SERVER['SERVER_NAME'] == 'hke-icms.eclass.hk'){?>
 	<form id="hidden_login_form" method="post" action="" style="display:none">
 		<input type="hidden" name="hiddenUserLogin" value="<?=urldecode($_POST['hiddenUserLogin'])?>"/>
 		<input type="hidden" name="hiddenUserPassword" value=""/>
 	</form>	
 	<?}?>
    
    <form id="login_form" class="login_board" style="display:none">
       	<h1></h1><!-- class="title_chn"-->
        <?if($_SERVER['SERVER_NAME'] == 'icms.eclass.hk' || $_SERVER['SERVER_NAME'] == 'hke-icms.eclass.hk'){?>
        <div class="login_box" style="top:135px">
		Campus / 分校:
		<select name="Campus" id="Campus" style="font-size:0.8em"> 
		  <option value="icms" <?=$_SERVER['SERVER_NAME'] == 'icms.eclass.hk'?'selected':''?>>Tin Hau Campus & Toddler Campus</option>
		  <option value="hke-icms" <?=$_SERVER['SERVER_NAME'] == 'hke-icms.eclass.hk'?'selected':''?>>HK East Campus & TOM HKE</option>
		</select>
        <p class="spacer"></p>
        <?}else{?>
        <div class="login_box">
        <?}?>
	    <span>Login ID / 登入號碼 :</span><input type="text" required autofocus name="UserLogin" value="<?=urldecode($_POST['hiddenUserLogin'])?>"/>
	    <p class="spacer"></p>
	    <span>Password / 密碼 :</span><input type="password" required name="UserPassword" value="<?=urldecode($_POST['hiddenUserPassword'])?>" />
	    <p class="spacer"></p>
	    <!--a href="javascript:checkForgetForm();">Forgot Password?</a-->
	    <a href="javascript:checkForgetForm();">Reset Password / 重設密碼</a>
	    	<?php if($err==1) { ?><div class="error_msg" style="padding-left:20px;">Incorrect LoginID/Password.</div><?php } ?>
			<?php if($msg==1) { ?><div class="done_msg" style="padding-left:20px;">Password resetting request has been sent.</div><?php } ?>
			<?php if($err==2) { ?><div class="error_msg" style="padding-left:20px;">Invalid LoginID.</div><?php } ?>
        </div>
        <div class="btn_login"><a title="Sign In" href="#"></a><input id="login_button" type="image" style="opacity:0"/></div>
        <div class="system_msg">
	  <span class="error_msg" style="display:none;" id="stop_login">Invalid Login ID or Password!</span>
	  <span class="error_msg" style="display:none;" id="stop_">Please Log in again</span>
	  <span class="loading_msg" style="display:none;">Logging In...</span>
	</div>
        <!--div class="text_copyright">&copy;<?=date('Y')?> BroadLearning Education (Asia) Limited. All rights reserved.</div-->
        <div class="text_copyright">&copy;<?=$copyRightYear?> BroadLearning Education (Asia) Limited. All rights reserved.</div>          
        <div class="text_browser">
	    <span>Recommended browsers:</span>
	    <a title="Google Chrome" target="_blank" href="http://www.google.com/chrome" class="browser_chrome">12+</a>
	    <a title="Mozilla Firefox" target="_blank" href="http://www.mozilla.org" class="browser_firefox">4+</a>
	    <!--<a title="Internet Explorer" target="_blank" href="http://www.microsoft.com/ie" class="browser_ie">9+</a>-->
	</div>
        <input type="hidden" name="target_url" value="/kis/ajax.php?a=login"/>
        <?=$LibSecureToken->WriteFormToken()?>
    </form>
    <form id="forgot_password_form" name="forgot_password_form" action="../forget.php" method="post">
    	<input type="hidden" name="UserLogin">
    	<input type="hidden" name="url_success" value="/kis/index.php?msg=1">
		<input type="hidden" name="url_fail" value="/kis/index.php?err=2">
		<?=$LibSecureToken->WriteFormToken()?>
    </form>
    <!-- system message start-->
    <div id="system_message"><span></span></div>
    <!--- system message end---->
    <div class="footer"><a title="Powered by eClass" href="http://eclass.com.hk"></a><span></span></div>
    <?php 
    	/*
    	 * For autoLogin, post these data{
    	 * 		autoLogin: 1,
    	 * 		hiddenUserLogin: (Login name),
    	 * 		hiddenUserPassword: (Login password)
    	 * }
    	 */
    	if($autoLogin == 1) {
    ?>
	    <script type="text/javascript">
	    $( document ).ready(function() {
		    $('#login_form').attr('method','POST').submit();
	    });
	    </script>
    <?php } ?>
</body></html>
<?php
intranet_closedb();
?>