<?php

# modifying by: Henry

/********************
 * 
 * This file is the general template of the addmission form, 
 * the varable $javascript and $main_content are used to store the dynamic javascript and html content in different pages
 * 
 * Log :
 * Date		2013-10-08 [Henry]
 * 			File Created
 * 
 ********************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<title>:: eClass KIS ::</title>
	
	<link href="/templates/kis/css/common.css" rel="stylesheet" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="/templates/jquery/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="/templates/jquery/jquery.fancybox.js?v=2.1.0"></script>
	<link rel="stylesheet" type="text/css" href="/templates/jquery/jquery.fancybox.css?v=2.1.0" media="screen" />
	<script language="JavaScript" src="/templates/script.js"></script>
	
	<script type="text/javascript">
		function MM_goToURL() { //v3.0
			var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
		}
		<?=$javascript?>
	</script>
	
</head>
<body class="parent">
	<div id="container">
        <div class="top_header">
            <a href="./" class="logo" title="eClass KIS" style="background-image: url('<?=$school['logo']?>')"></a>
            <div class="school_name"><?=GET_SCHOOL_NAME()?></div>
            <!--<div class="user_btn">
            	<?if($intranet_session_language == 'b5'){?>
            	<a title="Change to English Language" class="btn_lang_eng b5" href="/lang.php?lang=en"></a>
            	<?}else{?>
	    		<a title="切換至中文" class="btn_lang_chn en" href="/lang.php?lang=b5"></a>
	    		<?}?>
                <span class="sep">|</span>
                <span><em><?=$kis_lang['parent']?>,</em> <?=$Lang['Admission']['guest']?></span>            
            </div>-->
        </div>
        <div class="board" id="module_wood_page" style="width:100%">
        	<div class="board_top"><div class="board_top_right"><div class="board_top_bg"><div class="board_top_content">
            	<!--<div class="navigation_bar">
                	<a href="/index.php" class="btn_home"><?=$kis_lang['home']?></a><span class="module_title"><?=$kis_lang['app_admission']?></span>
                </div>-->
        	</div></div></div></div>
			<div class="board_main" style="padding-left: 30px;"><div class="board_main_right" style="padding-right: 30px;"><div class="board_main_bg"><div class="board_main_content">
				<div class="main_content">	
					<?=$main_content?>                                 
				</div>
			</div></div></div></div>
			<div class="board_bottom"><div class="board_bottom_right"><div class="board_bottom_bg"></div></div></div>
		</div>
        <div class="footer"><a href="http://eclass.com.hk" title="eClass"></a><span>Powered by</span></div>
	</div>
<!--</body>
</html>-->