<?php
# modifying by: Henry

/********************
 * Log :
 * Date        2013-10-21 [Henry]
 *            File Created
 *
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT . "includes/libdb.php");
include_once($PATH_WRT_ROOT . "includes/libinterface.php");
include_once($PATH_WRT_ROOT . "includes/kis/libkis.php");
include_once($PATH_WRT_ROOT . "includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT . "includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT . "includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_cust.php");
include_once($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT . "lang/lang." . $intranet_session_language . ".php");
include_once($PATH_WRT_ROOT . "lang/kis/lang_common_" . $intranet_session_language . ".php");
include_once($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_" . $intranet_session_language . ".php");
include_once($PATH_WRT_ROOT . "lang/admission_lang." . $intranet_session_language . ".php");


intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis = new kis('');
$lac = new admission_cust();
$lauc = new admission_ui_cust();
$li = new interface_html();

if (!$plugin['eAdmission']) {
    include_once($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT("", "../");
    exit;
}

$libkis_admission = $libkis->loadApp('admission');

//Get the intruction content
//$applicationSetting = $libkis_admission->getApplicationSetting($libkis_admission->schoolYearID);
$StudentDateOfBirth = $_REQUEST['StudentDateOfBirthYear'] . '-' . $_REQUEST['StudentDateOfBirthMonth'] . '-' . $_REQUEST['StudentDateOfBirthDay'];
$result = $lac->getInterviewResult($StudentDateOfBirth, $_REQUEST['StudentBirthCertNo'], $libkis_admission->schoolYearID, 1, $_REQUEST['ApplicationNo']);
$result2 = $lac->getInterviewResult($StudentDateOfBirth, $_REQUEST['StudentBirthCertNo'], $libkis_admission->schoolYearID, 2, $_REQUEST['ApplicationNo']);
$result3 = $lac->getInterviewResult($StudentDateOfBirth, $_REQUEST['StudentBirthCertNo'], $libkis_admission->schoolYearID, 3, $_REQUEST['ApplicationNo']);

$x .= '<section class="form">
		<div class="sheet">
			<div class="item">
				<div class="itemLabel">
					申請編號 Application #
				</div>
				<div class="itemData">
					<div class="dataValue">' . $result['ApplicationID'] . '</div>
				</div>
			</div>
			<div class="item">
				<div class="itemLabel">
					申請者姓名 Name of Applicant
				</div>
				<div class="itemData">
					<div class="dataValue">' . str_replace(',', ' ', $result['ChineseName']) . ' (' . str_replace(',', ' ', $result['EnglishName']) . ')</div>
				</div>
			</div>';

$moduleSettings = $libkis_admission->getBasicSettings(99999, array (
	'enableinterviewform'
));

if ($sys_custom['KIS_Admission']['InterviewSettings'] || $moduleSettings['enableinterviewform']) {
    if (method_exists($lauc, 'getInterviewResultContent')) {
        $x .= $lauc->getInterviewResultContent($result, $result2, $result3);
    } else {
		$applicationStatus = current($lac->getApplicationStatus($libkis_admission->schoolYearID,'',$result['ApplicationID']));
		$basicSettings = $libkis_admission->getBasicSettings($libkis_admission->schoolYearID,array('firstInterviewAnnouncement', 'secondInterviewAnnouncement', 'thirdInterviewAnnouncement', 'applicationStatusAnnouncement', 'applicationStatusAnnouncementStartDate'));
		
        $x .= '<div class="item">';
        $x .= '<div class="itemLabel">面試時段 Interview Timeslot' . ($result2['Date'] || $result3['Date'] ? ' (' . sprintf($kis_lang['round'], 1) . ' Round 1)' : '') . '</div>';
        $x .= '<div class="itemData">';
        if ($basicSettings['firstInterviewAnnouncement'] && $result['Date'])
            $x .= '<div class="dataValue">日期 Date: ' . $result['Date'] . ' <br/>時段 Timeslot: ' . substr($result['StartTime'], 0, -3) . ' ~ ' . substr($result['EndTime'], 0, -3) . ($result['GroupName'] ? ' <br/>' . ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? '面試房間 Interview Room' : '組別 Group') . ': ' . $result['GroupName'] : '') . '</div>';
        else
            $x .= '<td>面試時段尚未編排 No interview timeslot assigned</td>';
        $x .= '</div></div>';

        if ($basicSettings['secondInterviewAnnouncement'] && $result2['Date']) {
            $x .= '<div class="item">';
            $x .= '<div class="itemLabel">面試時段 Interview Timeslot (' . sprintf($kis_lang['round'], 2) . ' Round 2)</div>';
            $x .= '<div class="itemData">';
            $x .= '<div class="dataValue">日期 Date: ' . $result2['Date'] . ' <br/>時段 Timeslot: ' . substr($result2['StartTime'], 0, -3) . ' ~ ' . substr($result2['EndTime'], 0, -3) . ($result2['GroupName'] ? ' <br/>' . ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? '面試房間 Interview Room' : '組別 Group') . ': ' . $result2['GroupName'] : '') . '</div>';
            $x .= '</div></div>';
        }
        if ($basicSettings['thirdInterviewAnnouncement'] && $result3['Date']) {
            $x .= '<div class="item">';
            $x .= '<div class="itemLabel">面試時段 Interview Timeslot (' . sprintf($kis_lang['round'], 3) . ' Round 3)</div>';
            $x .= '<div class="itemData">';
            $x .= '<div class="dataValue">日期 Date: ' . $result3['Date'] . ' <br/>時段 Timeslot: ' . substr($result3['StartTime'], 0, -3) . ' ~ ' . substr($result3['EndTime'], 0, -3) . ($result3['GroupName'] ? ' <br/>' . ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? '面試房間 Interview Room' : '組別 Group') . ': ' . $result3['GroupName'] : '') . '</div>';
            $x .= '</div></div>';
        }
        
        $applicationAdmitted = ($admission_cfg['Status'][$applicationStatus['status']] == $admission_cfg['Status']['admitted']);
		$applicationNotAdmitted = ($admission_cfg['Status'][$applicationStatus['status']] == $admission_cfg['Status']['notadmitted']);
			
        if($basicSettings['applicationStatusAnnouncement'] && ($applicationAdmitted || $applicationNotAdmitted)){
        	$tempLang = $Lang;
			include_once($PATH_WRT_ROOT."lang/admission_lang.en.php");
			$LangEn = $Lang;
			$Lang = $tempLang;
        	$x .= '<div class="item">';
            $x .= '<div class="itemLabel">取錄結果 Application Result</div>';
            $x .= '<div class="itemData">';
            $x .= '<div class="dataValue">' . $Lang['Admission']['Status'][$applicationStatus['status']].' '.$LangEn['Admission']['Status'][$applicationStatus['status']] . '</div>';
            $x .= '</div></div>';
        }
    }
} else {
    $x .= '<div class="item">';
    $x .= '<div class="itemLabel">面試資料 Interview Information</div>';
    $x .= '<div class="itemData">';

    if ($result['InterviewDate']) {
        $interviewDateTime = explode(" ", $result['InterviewDate']);
        $x .= '<div class="dataValue">面試日期 Interview Date: ' . $interviewDateTime[0] . ' <br/>面試時間 Interview Time: ' . substr($interviewDateTime[1], 0, -3) . ' <br/>面試地點 Interview Location: ' . $result['InterviewLocation'] . '</div>';
    } else
        $x .= '<div class="dataValue">面試時段尚未編排 No interview timeslot assigned</div>';

    $x .= '</div></div>';
}

$x .= '</div></section>';

echo $result ? $x : $result;
//echo debug_pr($_REQUEST).debug_pr($_FILES);
?>