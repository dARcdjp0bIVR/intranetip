<script>
$(function(){
	'use strict';

	/**** Basic START ****/
	$(window).scroll(function() {
		var scroll = $(window).scrollTop();

		if (scroll >= 200) {
			$("#blkTopBar").addClass("show");
		}else{
			$("#blkTopBar").removeClass("show");
		}
	});

	$("#blkTopBar #btnMenu").click(function () {
		$(this).toggleClass("showTopMenu");
		$(this).siblings("a").toggleClass("showTopMenu");
		$("#blkTopBar > img").toggleClass("showTopMenu");
	});


	var $tbxFloat = $('.textbox-floatlabel input[type="text"], .textbox-floatlabel input[type="password"], .textbox-floatlabel textarea');
	$tbxFloat.blur(function(){
		var tmpval = $(this).val();
		if(tmpval == '') {
			$(this).addClass('empty');
			$(this).closest(".itemInput").addClass('empty');
			$(this).removeClass('notEmpty');
			$(this).closest(".itemInput").removeClass('notEmpty');
		} else {
			$(this).addClass('notEmpty');
			$(this).closest(".itemInput").addClass('notEmpty');
			$(this).removeClass('empty');
			$(this).closest(".itemInput").removeClass('empty');
		}
	});


	var $Choice = $('input[type="radio"],input[type="checkbox"]')
	$Choice.click(function() {
		 if($(this).is(':checked')) {
			 $(this).closest(".itemInput").removeClass('empty').addClass('notEmpty');
		 }
	});

	var $itemInputChoice = $(".itemInput-choice");
	$itemInputChoice.click(function(){
		var $cbxUnchecked = $(this).find('input[type="checkbox"]:not(:checked)');
		var $cbxGroup = $(this).find('input[type="checkbox"]');
		
		if($cbxUnchecked.length == $cbxGroup.length) {
			$cbxUnchecked.closest(".itemInput").removeClass('notEmpty').addClass('empty');
		} else {
			$cbxUnchecked.closest(".itemInput").removeClass('empty').addClass('notEmpty');
		}
	});

	$(".selector select").focus(function(){
		$(this).parent().addClass("focus");
	}).blur(function(){
		$(this).parent().removeClass("focus");
	});

	$('.itemInput-selector .selector select').change(function() {
		var $options = $(this).find("option:hidden ~ option");
		if($options.is(':selected')){
			$(this).closest(".itemInput").removeClass('empty').addClass('notEmpty');
		}
	});

	$(".closeDialog-button").click(function(){
		$(".dialog-container").hide();
	});
	/**** Basic END ****/

	/**** Dialog START ****/
	$("#btnCancel").click(function() {
		$("#cancel-dialog").removeClass("hide").show();
	});
	$("#btnConfirmCancel").click(function() {
		window.location.href = 'index.php';
	});
	$("#btnConfirmCancelEdit").click(function() {
		window.location.href = 'index_edit.php';
	});
	$("#btnSubmit").click(function() {
		$("#submit-dialog").removeClass("hide").show();
	});
	$("#btnConfirmSubmit").click(function() {
		$("#submit-dialog").addClass("hide").hide();
		$("#form1").submit();
	});
	/**** Dialog END ****/
});
</script>

</body>

</html>