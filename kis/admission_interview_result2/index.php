<?php
# modifying by: 
 
/********************
 * Log :
 * Date		2018-10-09 [Henry]
 * 			File Created
 * 
 ********************/
 
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");

include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

intranet_opendb();

#### Lang START ####
if($intranet_session_language == 'en'){
	if($admission_cfg['IsBilingual']){
	    $intranet_session_language = 'b5';
	    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
	    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
	    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
	    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
	    $LangB5 = $Lang;
	    $kis_lang_b5 = $kis_lang;
	    unset($Lang);
	    unset($kis_lang);
	}
	
    $intranet_session_language = 'en';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
}else{
	if($admission_cfg['IsBilingual']){
	    $intranet_session_language = 'en';
	    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
	    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
	    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
	    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
	    $LangEn = $Lang;
	    $kis_lang_en = $kis_lang;
	    unset($Lang);
	    unset($kis_lang);
	}
	
    $intranet_session_language = 'b5';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
}
#### Lang END ####

$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool();

$class_level = $lac->getClassLevel();
$application_setting = $lac->getApplicationSetting();


#### Get next school year name START ####
$academicYearB5 = getAcademicYearByAcademicYearID($lac->schoolYearID, 'b5');
$academicYearEn = getAcademicYearByAcademicYearID($lac->schoolYearID, 'en');
#### Get next school year name END ####
$isIndex = true;
include(__DIR__.'/header.php');
?>

<div id="blkWelcomeUpper">
	<div>
		<img id="imgSchLogo" src="<?=$school['logo']?>">
		<span>
			<?php 
			if($admission_cfg['SchoolName']):
    			if($admission_cfg['SchoolName']['b5']):
			?>
        			<div>
        				<span id="lblSchName-chi"><?=$admission_cfg['SchoolName']['b5']?></span>&nbsp;<span>網上收生系統</span>
        			</div>
			<?php
    			endif;
    			if($admission_cfg['SchoolName']['en']):
			?>
        			<div>
        				<span id="lblSchName-eng"><?=$admission_cfg['SchoolName']['en'] ?></span>&nbsp;<span>eAdmission System</span>
        			</div>
			<?php
    			endif;
			else:
			?>
    			<div>
    				<span id="lblSchName-chi"><?=GET_SCHOOL_NAME()?></span>&nbsp;<span>網上收生系統</span>
    			</div>
			<?php
			endif;
			?>
		</span>
	</div>
</div>
<div id='step_index'>
<form id='form1' autocomplete="off">
	<input autocomplete="false" name="hidden" type="text" style="display:none;">
	<input type="hidden" name="token" value="<?=$_GET['token'] ?>"/>
	<input type="hidden" name="BirthCertNo" value="<?=$data_collected['IDNumber'] ?>"/>
    <article id="blkWelcome">
    	<div id="lblWelcomeTitle">
    		eAdmission 面試編排結果 <?=date('Y',getStartOfAcademicYear('',$lac->schoolYearID))?>-<?=date('Y',getEndOfAcademicYear('',$lac->schoolYearID))?><br/>eAdmission Interview Arrangement Result <?=date('Y',getStartOfAcademicYear('',$lac->schoolYearID))?>-<?=date('Y',getEndOfAcademicYear('',$lac->schoolYearID))?>
    	</div>
        	<section class="form sheet">
        		<div class="item">
					<span class="itemInput empty">
						<div class="textbox-floatlabel">
							<input type="text" name="ApplicationNo" id="ApplicationNo" maxlength="16" size="8" class="empty" required>
							<div class="textboxLabel requiredLabel">輸入申請編號 Application Number</div>
							<div class="remark remark-warn hide">必須填寫 Required</div>
							<div class="remark remark-warn err hide"><?=$sys_custom['KIS_Admission']['STANDARD']['Settings']?'申請編號，香港香港出生證明書 / 其他出生證明號碼或出生日期不正確。<br/>Incorrect Application Number, Birth certificate No. or Date of Birth.':'申請編號，出生證明書號碼或出生日期不正確。<br/>Incorrect Application Number, Birth Certificate Number or Date of Birth.'?></div>
						</div>
					</span>
				</div>
				<div class="item">
					<span class="itemInput empty">
						<div class="textbox-floatlabel">
							<input type="text" name="StudentBirthCertNo" id="StudentBirthCertNo" maxlength="64" size="8" class="empty" required>
							<div class="textboxLabel requiredLabel"><?=$sys_custom['KIS_Admission']['STANDARD']['Settings']?'輸入香港出生證明書 / 其他出生證明號碼 Birth Certificate Number':'輸入出生證明書號碼 Birth Certificate Number'?></div>
							<div class="remark remark-warn hide">必須填寫 Required</div>
							<div class="remark remark-warn err hide"><?=$sys_custom['KIS_Admission']['STANDARD']['Settings']?'申請編號，香港香港出生證明書 / 其他出生證明號碼或出生日期不正確。<br/>Incorrect Application Number, Birth certificate No. or Date of Birth.':'申請編號，出生證明書號碼或出生日期不正確。<br/>Incorrect Application Number, Birth Certificate Number or Date of Birth.'?></div>
							<div class="remark">(例如 e.g.:"A123456(7)"，請輸入 please enter "A1234567")</div>
						</div>
					</span>
				</div>
				<div class="item">
					<div class="itemLabel requiredLabel">輸入出生日期 Date of Birth</div>
					<span class="itemInput itemInput-selector">
						<span class="selector">
							<select id="StudentDateOfBirthYear" name="StudentDateOfBirthYear" class="dob dateYear" required>
								<option value="" hidden><?=$lauc->getLangStr(array(
		                            'b5' => $LangB5['Admission']['year2'],
		                            'en' => $LangEn['Admission']['year2'],
	                            ))?></option>
								<?php 
	    						$startYear = date('Y') - 15;
	    						$endYear = date('Y');
	    						 
								for($i=$endYear;$i>=$startYear;$i--): 
								?>
									<option value="<?=$i ?>" <?=$dobYear == $i ? 'selected' : ''?>><?= $i ?></option>
								<?php 
								endfor; 
								?>
							</select>
						</span><span class="selector">
							<select id="StudentDateOfBirthMonth" name="StudentDateOfBirthMonth" class="dob dateMonth" required>
								<option value="" hidden><?=$lauc->getLangStr(array(
		                            'b5' => $LangB5['Admission']['month2'],
		                            'en' => $LangEn['Admission']['month2'],
	                            ))?></option>
								<?php for($i=1;$i<=12;$i++): ?>
									<option value="<?=str_pad($i, 2, "0", STR_PAD_LEFT) ?>" <?=$dobMonth == $i ? 'selected' : ''?>><?= str_pad($i, 2, "0", STR_PAD_LEFT) ?></option>
								<?php endfor; ?>
							</select>
						</span><span class="selector">
							<select id="StudentDateOfBirthDay" name="StudentDateOfBirthDay" class="dob dateDay" required>
								<option value="" hidden><?=$lauc->getLangStr(array(
		                            'b5' => $LangB5['Admission']['day2'],
		                            'en' => $LangEn['Admission']['day2'],
	                            ))?></option>
								<?php for($i=1;$i<=31;$i++): ?>
									<option value="<?=str_pad($i, 2, "0", STR_PAD_LEFT) ?>" <?=$dobDay == $i ? 'selected' : ''?>><?= str_pad($i, 2, "0", STR_PAD_LEFT) ?></option>
								<?php endfor; ?>
							</select>
						</span>
						<span>
							<div class="remark remark-warn hide" id="errDobRequired"><?=$LangB5['Admission']['pleaseSelect'] ?> <?=$LangEn['Admission']['pleaseSelect'] ?></div>
							<div class="remark remark-warn err hide"><?=$sys_custom['KIS_Admission']['STANDARD']['Settings']?'申請編號，香港香港出生證明書 / 其他出生證明號碼或出生日期不正確。<br/>Incorrect Application Number, Birth certificate No. or Date of Birth.':'申請編號，出生證明書號碼或出生日期不正確。<br/>Incorrect Application Number, Birth Certificate Number or Date of Birth.'?></div>
						</span>
					</span>
				</div>
        		<?php
        		if(!$admission_cfg['IsBilingual'] && $admission_cfg['Lang']):
        		?>
            		<div class="item">
            			<div class="itemLabel requiredLabel">
            				<?=$LangB5['Admission']['Language'] ?>
            				<?=$LangEn['Admission']['Language'] ?>
            			</div>
            			<span class="itemInput itemInput-choice">
            				<?php 
            				foreach($admission_cfg['Lang'] as $val => $code): 
            				    $checked = ($code == $admission_cfg['DefaultLang'])? 'checked' : '';
            				?>
                				<span>
                					<input type="radio" id="lang_<?=$val ?>" name="lang" value="<?=$val ?>" <?=$checked ?> />
                					<label for="lang_<?=$val ?>"><?=$Lang['Admission']['LanguagesTransDisplay'][$code] ?></label>
                				</span>
            				<?php 
            				endforeach;
            				?>
        				</span>
            		</div>
        		<?php
        		else:
        		?>
        			<input type="hidden" name="lang" value="<?=$admission_cfg['DefaultLang'] ?>" />
        		<?php
        		endif;
        		?>
        	</section>
    </article>
    
    <div id="blkButtons" class="layout-m">
    	<button type="submit" class="button floatR" onClick="goto('step_index','step_result'); return false">
    		<?=$LangB5['Admission']['continue'] ?>
    		<?=$LangEn['Admission']['continue'] ?>
		</button>
    </div>
</form>
</div>

<div id='step_result' style='display:none'>
	<article id="blkWelcome">
		<div id="lblWelcomeTitle">
    		eAdmission 面試編排結果 <?=date('Y',getStartOfAcademicYear('',$lac->schoolYearID))?>-<?=date('Y',getEndOfAcademicYear('',$lac->schoolYearID))?><br/>eAdmission Interview Arrangement Result <?=date('Y',getStartOfAcademicYear('',$lac->schoolYearID))?>-<?=date('Y',getEndOfAcademicYear('',$lac->schoolYearID))?>
    	</div>
		<div id="divInterviewResult"></div>
		<div id="blkButtons" class="layout-m">
			<div class="button floatR" id="btnFinish" onClick="location.reload();"><?=$LangB5['Admission']['finish'] ?> <?=$LangEn['Admission']['finish'] ?></div>
		</div>
	</article>
</div>

<div id="blkFooter" class="layout-m">
	<span id="lbleClass"><span>Powered by</span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img src="/images/kis/eadmission/eClassLogo.png"></a></span>
</div>

<?php
include(__DIR__.'/footer.php');
?>

<script type="text/javascript">

function goto(current,page){

	if(current == 'step_index' && page == 'step_result'){
		
		$('.remark.remark-warn.err').addClass('hide');
		
		/* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_interview_result.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           if(data==0){
		           		$('.remark.remark-warn.err').removeClass('hide');
//		           		alert("Incorrect <?if($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){echo 'Application Number, ';}?>Birth Certificate Number or Date of Birth.");	
//						form1.StudentBirthCertNo.focus();
						return false;
		           }
		           $("#divInterviewResult").html(data);
		           document.getElementById(current).style.display = "none";
				   document.getElementById(page).style.display = "";
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
	}
	
}

</script>