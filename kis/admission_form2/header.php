<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>eClass 網上收生系統 | eClass eAdmission System</title>
	<link href="/templates/kis/css/jquery-ui.css" rel="stylesheet" type="text/css">
	<link href="/templates/kis/css/main.css" rel="stylesheet" type="text/css">
	<link href="/templates/kis/css/form.css" rel="stylesheet" type="text/css">
	<?php if($isIndex): ?>
		<link href="/templates/kis/css/index.css" rel="stylesheet" type="text/css">
	<?php endif; ?>
	<link href="/templates/kis/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" media="all" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/earlyaccess/notosanstc.css" media="all" rel="stylesheet" type="text/css">
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
	<script src="/templates/jquery/jquery-3.3.1.min.js"></script>
	<!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
	<script src="/templates/jquery/jquery-ui-1.12.1.js"></script>
	<script src="/templates/javascript-state-machine/state-machine.min.js"></script>
	<script src="/templates/lodash/lodash.js"></script>
	<link rel="shortcut icon" href="/templates/kis/images/favicon.png">
	<link rel="icon" type="image/png" href="/templates/kis/images/favicon.png">
	<meta name="viewport" content="width=device-width">
    <style>
        ol{
            padding-left: 40px;
            list-style-type: decimal;
        }
        ul{
            padding-left: 40px;
            list-style-type: disc;
        }
    </style>
</head>
<body class="<?=$admission_cfg['themeStyle'] ?> lang_<?=$lang ?>">
