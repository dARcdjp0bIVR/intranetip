<?php
# modifying by: 
 
/********************
 * Log :
 * Date		2018-09-15 [Henry]
 * 			File Created
 * 
 ********************/
 
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");

include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

intranet_opendb();

#### Lang START ####
if($intranet_session_language == 'en'){
	if($admission_cfg['IsBilingual']){
	    $intranet_session_language = 'b5';
	    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
	    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
	    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
	    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
	    $LangB5 = $Lang;
	    $kis_lang_b5 = $kis_lang;
	    unset($Lang);
	    unset($kis_lang);
	}
	
    $intranet_session_language = 'en';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
}else{
	if($admission_cfg['IsBilingual']){
	    $intranet_session_language = 'en';
	    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
	    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
	    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
	    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
	    $LangEn = $Lang;
	    $kis_lang_en = $kis_lang;
	    unset($Lang);
	    unset($kis_lang);
	}
	
    $intranet_session_language = 'b5';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
}
#### Lang END ####

$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool();
$next_school_year_basic_settings = $lac->getBasicSettings('',array('generalInstruction'));
$basic_settings = $lac->getBasicSettings('99999');

#### Get next school year name START ####
$academicYearB5 = getAcademicYearByAcademicYearID($lac->schoolYearID, 'b5');
$academicYearEn = getAcademicYearByAcademicYearID($lac->schoolYearID, 'en');
#### Get next school year name END ####


$class_level = $lac->getClassLevel();
$application_setting = $lac->getApplicationSetting();

unset($_SESSION['KIS_ApplicationID']);
unset($_SESSION['KIS_StudentDateOfBirth']);
unset($_SESSION['KIS_StudentBirthCertNo']);

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool(); 

//$libkis_admission = $libkis->loadApp('admission');
//Get the index content

$allowToView = (
                $sys_custom['KIS_Admission']['UCCKE']['Settings'] 
            && 
                $lac->IsAfterUpdatePeriod()
            );

$allowToViewOnly = 
    (
        $sys_custom['KIS_Admission']['ApplicantUpdateForm'] && 
        (
            !$lac->IsUpdatePeriod() 
        && 
            (
                $sys_custom['KIS_Admission']['UCCKE']['Settings'] 
            && 
                $lac->IsAfterUpdatePeriod()
            )
        )
    );
    
## Update form function
$allowToUpdate = 
    (
        $sys_custom['KIS_Admission']['ApplicantUpdateForm'] && 
        (
            $lac->IsUpdatePeriod()
        ||
            $lac->IsUpdateExtraAttachmentPeriod()
        ||
            (
                $sys_custom['KIS_Admission']['UCCKE']['Settings'] 
            && 
                $lac->IsAfterUpdatePeriod()
            )
        )
    );

###################################### UI START ######################################
$isIndex = true;
include(__DIR__.'/header.php');
?>

<div id="blkWelcomeUpper">
	<div>
		<img id="imgSchLogo" src="<?=is_file('/file/kis/admission/images/logo.png') ? '/file/kis/admission/images/logo.png' : $school['logo']?>">
		<span>
			<?php 
			if ($sys_custom['KIS_Admission']['JOYFUL']['Settings']):
                if ($basic_settings['schoolnamechi']?$basic_settings['schoolnamechi']:$admission_cfg['SchoolName']['b5']):
                    ?>
                    <div>
        				<span id="lblSchName-chi"><?= $basic_settings['schoolnamechi']?$basic_settings['schoolnamechi']:$admission_cfg['SchoolName']['b5'] ?></span>
        			</div>
                <?php
                endif;
                if ($basic_settings['schoolnameeng']?$basic_settings['schoolnameeng']:$admission_cfg['SchoolName']['en']):
                    ?>
                    <div>
        				<span id="lblSchName-eng"><?= $basic_settings['schoolnameeng']?$basic_settings['schoolnameeng']:$admission_cfg['SchoolName']['en'] ?></span>
        			</div>
                <?php
                endif;
            elseif($admission_cfg['SchoolName']):
    			if($basic_settings['schoolnamechi']?$basic_settings['schoolnamechi']:$admission_cfg['SchoolName']['b5']):
			?>
        			<div>
        				<span id="lblSchName-chi"><?=$basic_settings['schoolnamechi']?$basic_settings['schoolnamechi']:$admission_cfg['SchoolName']['b5']?></span>&nbsp;<span><?=$admission_cfg['AdmissionSystemTitle']['b5']?$admission_cfg['AdmissionSystemTitle']['b5']:'網上收生系統'?></span>
        			</div>
			<?php
    			endif;
    			if($basic_settings['schoolnameeng']?$basic_settings['schoolnameeng']:$admission_cfg['SchoolName']['en']):
			?>
        			<div>
        				<span id="lblSchName-eng"><?=$basic_settings['schoolnameeng']?$basic_settings['schoolnameeng']:$admission_cfg['SchoolName']['en'] ?></span>&nbsp;<span><?=$admission_cfg['AdmissionSystemTitle']['en']?$admission_cfg['AdmissionSystemTitle']['en']:'eAdmission System'?></span>
        			</div>
			<?php
    			endif;
			else:
			?>
    			<div>
    				<span id="lblSchName-chi"><?=GET_SCHOOL_NAME()?></span>&nbsp;<span>網上收生系統</span>
    			</div>
			<?php
			endif;
			?>
		</span>
	</div>
	<?php
    if ($sys_custom['KIS_Admission']['JOYFUL']['Settings']):
    ?>
    	<div>
        <span><?=$admission_cfg['AdmissionSystemTitle']['b5']?$admission_cfg['AdmissionSystemTitle']['b5']:'網上收生系統'?></span>
        &nbsp;<span><?=$admission_cfg['AdmissionSystemTitle']['en']?$admission_cfg['AdmissionSystemTitle']['en']:'eAdmission System'?></span>
		</div>
	<?php
    endif;
    ?>
</div>

<form autocomplete="off" action="<?=$allowToViewOnly?'finish_edit.php':'form_edit.php'?>" method="POST">
	<input autocomplete="false" name="hidden" type="text" style="display:none;">
	<input type="hidden" name="token" value="<?=$_GET['token'] ?>"/>
	<input type="hidden" name="BirthCertNo" value="<?=$data_collected['IDNumber'] ?>"/>
    <article id="blkWelcome">
    	<div id="lblWelcomeTitle">
    		<?=$lauc->getLangStr(array(
    			'b5' => '<span>'.str_replace('<!--SchoolYear-->', $academicYearB5, $allowToViewOnly?$LangB5['Admission']['applicationviewtitle2']:$LangB5['Admission']['applicationupatetitle2']) . '</span>',
	            'en' => '<span>'.str_replace('<!--SchoolYear-->', $academicYearEn, $allowToViewOnly?$LangEn['Admission']['applicationviewtitle2']:$LangEn['Admission']['applicationupatetitle2']) . '</span>',
            ))?>
    	</div>
    
    	<section class="form sheet" id="blkWelcomeMsg">
    		<?php
    		if ($lac->schoolYearID){
    		    echo $next_school_year_basic_settings['generalInstruction'];
    		}else{
    		?>
        		<fieldset class="warning_box">
        			<legend><?=$Lang['Admission']['warning'] ?></legend>
        			<ul>
        				<li><?=$Lang['Admission']['msg']['noclasslevelapply'] ?></li>
        			</ul>
        		</fieldset>
    		<?php
    		}
    		?>
    	</section>
    
        <?php if($isSubmitted && $sys_custom['KIS_Admission']['IntegratedCentralServer']): ?>
        	<section class="form sheet">
        		<div class="item">
        			<span>
        			閣下已遞交網上申請表，申請通知電郵已發送，請檢查閣下在申請表填寫的電郵。
        			<br />
        			You have applied the admission! Please check your Email to get the admission Notification!
        			</span>
        		</div>
    		</section>
        <?php else: ?>
        	<section class="form sheet">
        	<?php
				if($allowToUpdate):
			?>
        		<div class="item">
					<span class="itemInput empty">
						<div class="textbox-floatlabel">
							<input type="text" name="InputApplicationID" id="InputApplicationID" maxlength="16" size="8" class="empty" required>
							<div class="textboxLabel requiredLabel">
							<?=$lauc->getLangStr(array(
                                'b5' => '輸入申請編號',
                                'en' => 'Application Number',
                            ), ' ')?>
							</div>
							<div class="remark remark-warn hide">
							<?=$lauc->getLangStr(array(
                                'b5' => '必須填寫',
                                'en' => 'Required',
                            ), ' ')?>
							</div>
							<div class="remark remark-warn">
							<?=$lauc->getLangStr(array(
                                'b5' => $_GET['err']? ($sys_custom['KIS_Admission']['STANDARD']['Settings']?'申請編號，香港香港出生證明書 / 其他出生證明號碼或出生日期不正確。':($sys_custom['KIS_Admission']['JOYFUL']['Settings']?'申請編號，香港身份證號碼/護照號碼或出生日期不正確。':'申請編號，香港身份證號碼或出生日期不正確。')):'',
                                'en' => $_GET['err']? ($sys_custom['KIS_Admission']['STANDARD']['Settings']?'Incorrect Application Number, Birth certificate No. or Date of Birth.':($sys_custom['KIS_Admission']['JOYFUL']['Settings']?'Incorrect Application Number, HKID Card No. / Passport No. or Date of Birth.':'Incorrect Application Number, HKID Card No. or Date of Birth.')):'',
                            ), '<br/>')?>
							</div>
						</div>
					</span>
				</div>
				<div class="item">
					<span class="itemInput empty">
						<div class="textbox-floatlabel">
							<input type="text" name="InputStudentBirthCertNo" id="InputStudentBirthCertNo" maxlength="64" size="8" class="empty" required>
							<div class="textboxLabel requiredLabel">
							<?=$lauc->getLangStr(array(
                                'b5' => $sys_custom['KIS_Admission']['STANDARD']['Settings']?'輸入香港出生證明書 / 其他出生證明號碼':($sys_custom['KIS_Admission']['JOYFUL']['Settings']?'輸入香港身份證號碼 / 護照號碼':'輸入香港身份證號碼'),
                                'en' => $sys_custom['KIS_Admission']['STANDARD']['Settings']?'Birth certificate No.':($sys_custom['KIS_Admission']['JOYFUL']['Settings']?'HKID Card No. / Passport No.':'HKID Card No.'),
                            ), ' ')?>
							</div>
							<div class="remark remark-warn hide">
							<?=$lauc->getLangStr(array(
                                'b5' => '必須填寫',
                                'en' => 'Required',
                            ), ' ')?>
							</div>
							<div class="remark remark-warn">
							<?=$lauc->getLangStr(array(
                                'b5' => $_GET['err']? ($sys_custom['KIS_Admission']['STANDARD']['Settings']?'申請編號，香港香港出生證明書 / 其他出生證明號碼或出生日期不正確。':($sys_custom['KIS_Admission']['JOYFUL']['Settings']?'申請編號，香港身份證號碼/護照號碼或出生日期不正確。':'申請編號，香港身份證號碼或出生日期不正確。')):'',
                                'en' => $_GET['err']? ($sys_custom['KIS_Admission']['STANDARD']['Settings']?'Incorrect Application Number, Birth certificate No. or Date of Birth.':($sys_custom['KIS_Admission']['JOYFUL']['Settings']?'Incorrect Application Number, HKID Card No. / Passport No. or Date of Birth.':'Incorrect Application Number, HKID Card No. or Date of Birth.')):'',
                            ), '<br/>')?>
							</div>
							<div class="remark">
							<?=$lauc->getLangStr(array(
                                'b5' => '(例如 "A123456(7)"，請輸入 "A1234567")',
                                'en' => '(e.g. "A123456(7)", please enter "A1234567")'
                            ), ' ')?>
							
							</div>
						</div>
					</span>
				</div>
				<div class="item">
					<div class="itemLabel requiredLabel">
					<?=$lauc->getLangStr(array(
                                'b5' => '輸入出生日期',
                                'en' => 'Date of Birth'
                            ), ' ')?>
					</div>
					<span class="itemInput itemInput-selector">
						<span class="selector">
							<select id="StudentDateOfBirthYear" name="StudentDateOfBirthYear" class="dob dateYear" required>
								<option value="" hidden><?=$lauc->getLangStr(array(
		                            'b5' => $LangB5['Admission']['year2'],
		                            'en' => $LangEn['Admission']['year2'],
	                            ))?></option>
								<?php 
	    						$startYear = date('Y') - 15;
	    						$endYear = date('Y');
	    						 
								for($i=$endYear;$i>=$startYear;$i--): 
								?>
									<option value="<?=$i ?>" <?=$dobYear == $i ? 'selected' : ''?>><?= $i ?></option>
								<?php 
								endfor; 
								?>
							</select>
						</span><span class="selector">
							<select id="StudentDateOfBirthMonth" name="StudentDateOfBirthMonth" class="dob dateMonth" required>
								<option value="" hidden><?=$lauc->getLangStr(array(
		                            'b5' => $LangB5['Admission']['month2'],
		                            'en' => $LangEn['Admission']['month2'],
	                            ))?></option>
								<?php for($i=1;$i<=12;$i++): ?>
									<option value="<?=str_pad($i, 2, "0", STR_PAD_LEFT) ?>" <?=$dobMonth == $i ? 'selected' : ''?>><?= str_pad($i, 2, "0", STR_PAD_LEFT) ?></option>
								<?php endfor; ?>
							</select>
						</span><span class="selector">
							<select id="StudentDateOfBirthDay" name="StudentDateOfBirthDay" class="dob dateDay" required>
								<option value="" hidden><?=$lauc->getLangStr(array(
		                            'b5' => $LangB5['Admission']['day2'],
		                            'en' => $LangEn['Admission']['day2'],
	                            ))?></option>
								<?php for($i=1;$i<=31;$i++): ?>
									<option value="<?=str_pad($i, 2, "0", STR_PAD_LEFT) ?>" <?=$dobDay == $i ? 'selected' : ''?>><?= str_pad($i, 2, "0", STR_PAD_LEFT) ?></option>
								<?php endfor; ?>
							</select>
						</span>
						<span>
							<div class="remark remark-warn hide" id="errDobRequired">
							<?=$lauc->getLangStr(array(
                                'b5' => $LangB5['Admission']['pleaseSelect'] ,
                                'en' => $LangEn['Admission']['pleaseSelect']
                            ), ' ')?>
							</div>
							<div class="remark remark-warn">
							<?=$lauc->getLangStr(array(
                                'b5' => $_GET['err']? ($sys_custom['KIS_Admission']['STANDARD']['Settings']?'申請編號，香港香港出生證明書 / 其他出生證明號碼或出生日期不正確。':($sys_custom['KIS_Admission']['JOYFUL']['Settings']?'申請編號，香港身份證號碼/護照號碼或出生日期不正確。':'申請編號，香港身份證號碼或出生日期不正確。')):'',
                                'en' => $_GET['err']? ($sys_custom['KIS_Admission']['STANDARD']['Settings']?'Incorrect Application Number, Birth certificate No. or Date of Birth.':($sys_custom['KIS_Admission']['JOYFUL']['Settings']?'Incorrect Application Number, HKID Card No. / Passport No. or Date of Birth.':'Incorrect Application Number, HKID Card No. or Date of Birth.')):'',
                            ), '<br/>')?>
							</div>
						</span>
					</span>
				</div>
<!--				<div class="item">
					<span class="itemInput empty">
						<div class="textbox-floatlabel">
							<input type="text" name="InputStudentDateOfBirth" id="InputStudentDateOfBirth" maxlength="10" size="15" class="empty" required>
							<div class="textboxLabel requiredLabel">輸入出生日期 Date of Birth</div>
							<div class="remark remark-warn hide">必須填寫 Required</div>
							<div class="remark remark-warn"><?=$_GET['err']? '申請編號，香港身份證號碼或出生日期不正確。<br/>Incorrect Application Number, HKID Card No. or Date of Birth.':''?></div>
						</div>
					</span>
				</div>-->
				<?php
				else:
				?>
				<div class="item">
					<span class="itemInput itemInput-choice">
						<span style="color:red;">
							<?=$lauc->getLangStr(array(
                                'b5' => $LangB5['Admission']['msg']['noclasslevelapply'] ,
                                'en' => $LangEn['Admission']['msg']['noclasslevelapply']
                            ), '<br/>')?>
						</span>
					</span>
				</div>
				<?php
				endif;
				?>
        		<?php
        		if($admission_cfg['Lang'] && !$admission_cfg['HideSelectLang']):
        		?>
            		<div class="item">
            			<div class="itemLabel requiredLabel">
            				<?=$lauc->getLangStr(array(
				                'b5' => $LangB5['Admission']['Language'],
				                'en' => $LangEn['Admission']['Language']
				            ), ' ')?>
            			</div>
            			<span class="itemInput itemInput-choice">
            				<?php 
            				foreach($admission_cfg['Lang'] as $val => $code): 
            				    $checked = ($code == $admission_cfg['DefaultLang'])? 'checked' : '';
            				?>
                				<span>
                					<input type="radio" id="lang_<?=$val ?>" name="lang" value="<?=$val ?>" <?=$checked ?> />
                					<label for="lang_<?=$val ?>"><?=$Lang['Admission']['LanguagesTransDisplay'][$code] ?></label>
                				</span>
            				<?php 
            				endforeach;
            				?>
        				</span>
            		</div>
        		<?php
        		else:
        		?>
        			<input type="hidden" name="lang" value="<?=$admission_cfg['DefaultLang'] ?>" />
        		<?php
        		endif;
        		?>
        	</section>
        <?php endif; ?>
    </article>
    
    <div id="blkButtons" class="layout-m">
    	<button type="submit" class="button floatR">
    		<?=$lauc->getLangStr(array(
                'b5' => $LangB5['Admission']['continue'],
                'en' => $LangEn['Admission']['continue']
            ), ' ')?>
		</button>
    </div>
</form>

<div id="blkFooter" class="layout-m">
	<span id="lbleClass"><span>Powered by</span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img src="/images/kis/eadmission/eClassLogo.png"></a></span>
</div>

<?php
include(__DIR__.'/footer.php');


