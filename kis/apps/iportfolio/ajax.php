<?
//Using:
/**
 * Change Log:
 * 2020-09-16 (Philips)
 * 		- added syncToCEES actions
 * 2020-08-20 (Philips)
 * 		- added 'saveSchoolAward'
 * 2015-11-13 (Pun)
 * 		- added 'checkImportPortfolioDataInfo' and 'importPortfolioDataInfo'
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT.'kis/init.php');

$libkis_iportfolio = $libkis->loadApp('iportfolio');

$lpf = new libpf_sbs();
$lo  = new libportfolio_group($lpf->course_db);
$lgs = new growth_scheme();

if ($kis_user['type']==kis::$user_types['teacher']){
    
    $liblp2 = new libpf_lp2($kis_user['id'], 0, $kis_user['id'], 'publish');
    switch ($action){
		case 'getAssessmentList':
			$libkis_iportfolio->getAssessmentList($assessmentId,$classId,$status,$keyword,$sortby,$order,$amount,$page);
			break;
		case 'getAssessmentById':
			$AssessmentArr = current($libkis_iportfolio->getAssessmentList($assessmentId));
			$kis_data['id'] = $AssessmentArr['id'];
			$kis_data['classId'] = $AssessmentArr['classId'];
			$kis_data['title'] = $AssessmentArr['title'];  
			$kis_data['release_date'] = $AssessmentArr['release_date'];  
			echo $libjson->encode($kis_data); 
			break;			
		case 'saveAssessment':
			$libkis_iportfolio->saveAssessment($title,$release_date,$classId);
			break;
		case 'copyAssessment':
			$AssessmentArr = current($libkis_iportfolio->getAssessmentList($assessmentId));
			$title = $AssessmentArr['title']."(".$kis_lang['copy'].")";  
			$release_date = $AssessmentArr['release_date'];  
			$classId = $AssessmentArr['classId'];
			$libkis_iportfolio->saveAssessment($title,$release_date,$classId);
			break;
		case 'updateAssessment':
			$libkis_iportfolio->updateAssessment($assessmentId,$title,$release_date);
			break;	
		case 'removeAssessment':
			$libkis_iportfolio->removeAssessment($assessmentId);
			break;	
		case 'addStudentAssessment':
			$folder = 'assessment/'.$assessmentId.'/'.$studentId;
			$file_url = kis_iportfolio::$attachment_url.$folder;
			$file_name = kis_utility::getSaveFileName($_FILES['file']['name']);
			$path = $file_path.$file_url;
			$libkis_iportfolio->saveStudentAssessment($assessmentId,$classId,$studentId,$file_name);
			if (!file_exists($path)) mkdir($path, 0755, true);
			move_uploaded_file($_FILES['file']['tmp_name'], $path.'/'.$file_name);
			
			$AssessmentArr = current($libkis_iportfolio->getStudentAssessmentList($assessmentId,$classId,array($studentId)));
			$kis_data['folder'] = $folder;
			$kis_data['file_url'] = $file_url.'/'.$file_name;
			$kis_data['file_name'] = $file_name;  
			$kis_data['modified_date'] = $AssessmentArr['assessment']['modified_date'];  
			$kis_data['modified_user'] = $AssessmentArr['assessment']['modified_user'];  
			
			echo $libjson->encode($kis_data); 
			break;
		case 'removeStudentAssessment':
				$libkis_iportfolio->removeStudentAssessment($assessmentId,$classId,$studentId);
			break;
		case 'getStudentAssessmentFile':
			$AssessmentArr = current($libkis_iportfolio->getStudentAssessmentList($assessmentId,'',array($studentId), $status='all', $keyword='', $schoolYear='all'));
			$folder = 'assessment/'.$assessmentId.'/'.$studentId;
			$file_name = $AssessmentArr['assessment']['title'];
			$attachment_path = $file_path.kis_iportfolio::$attachment_url.$folder.'/'.$file_name;
			if (file_exists($attachment_path)){
				kis_utility::downloadFile($attachment_path, $file_name);
			} 
			break;
		case 'getStudentInfo':
			$lib_student_kis = new kis($studentId);
			$studentInfo = $lib_student_kis->user;
			echo $libkis_iportfolio->getStudentInfoHTML($studentInfo,$infoType);
			break;
		case 'updateStudentInfo':
			$libkis_iportfolio->updateStudentInfo($_POST);
			break;		
		case 'getTermSelection':
			include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
			$libenroll = new libclubsenrol($libkis_iportfolio->schoolyear);
			$term_onchange = ($isOnChange)?"$('form.filter_form').submit();":"";
			$withWholeYear = ($hideWholeYear)?0:1;
			echo $libenroll->Get_Term_Selection('school_year_term_id', $AcademicYearID, '', $term_onchange, $NoFirst=1, $NoPastTerm=0, $withWholeYear);
			break;
		case 'saveSchoolRecord':
				list($action,$type) = explode('_',$_POST['recordType']);
				if($action=='new'||empty($_POST['recordId'])){
					if($type=='awards')
						$libkis_iportfolio->insertAwardRecord($_POST);
					else if($type=='activities')
						$libkis_iportfolio->insertActivityRecord($_POST);
					else if($type=='schoolaward')
						$libkis_iportfolio->insertSchoolAwardRecord($_POST);
				}else{
					if($type=='awards')
						$libkis_iportfolio->updateAwardRecord($_POST);
					else if($type=='activities')
						$libkis_iportfolio->updateActivityRecord($_POST);
					else if($type=='schoolaward')
						$libkis_iportfolio->updateSchoolAwardRecord($_POST);
				}
			break;		
		case 'removeSchoolRecord':
			if($retrieve_type=='awards')
				$libkis_iportfolio->removeAwardRecord($recordId);
			else if($retrieve_type=='activities')
				$libkis_iportfolio->removeActivityRecord($recordId);
			else if($retrieve_type=='schoolaward')
				$libkis_iportfolio->removeSchoolAwardRecord($recordId);
			break;		
		case 'copySchoolRecord':
			$para['recordId'] = $recordId; 
			if($retrieve_type=='awards'){
				list($total,$data) = $libkis_iportfolio->getAwardRecordList($para);
				if($total>0){
					$data = current($data);
					$data['award_name'] .= "(".$kis_lang['copy'].")"; 
					$data['cur_student_id'] = $data['user_id'];
					$libkis_iportfolio->insertAwardRecord($data);
				}
			}else if($retrieve_type=='activities'){
				list($total,$data) = $libkis_iportfolio->getActivityRecordList($para);
				if($total>0){
					$data = current($data);
					$data['activity_name'] .= "(".$kis_lang['copy'].")";
					$data['cur_student_id'] = $data['user_id'];
					$libkis_iportfolio->insertActivityRecord($data);
				}
			}	
			break;	
		case 'searchusers':
			$classId = (!empty($classId)&&$classId!='all')?$classId:'';
			$excludes = explode(',',$exclude_list);
			$user = kis_utility::getUsers(array('class_id'=>$classId,'keyword'=>trim($keyword),'excludes'=>$excludes));
	
			if (sizeof($user)>500){
				$kis_data['ui'] = $kis_lang['toomanyresults'].'!';
				
			}else if (!$user){
				$kis_data['ui'] = $kis_lang['norecord'].'!';
				
			}else{
				ob_start();
			
				kis_ui::loadTemplate('schoolrecords/select_users',array('users'=>$user, 'form_name'=>'target_user'));
				
				$kis_data['ui'] = ob_get_clean();
				
				ob_end_clean;
			}
			 
			$kis_data['count'] = sizeof($user);
			echo $libjson->encode($kis_data);
			break;
		case 'saveAwardRecord':
			$data['school_year_id'] = $school_year_id;
			$data['school_year_term_id'] = $school_year_term_id;
			$data['award_name'] = $award_name;
			$data['award_date'] = $award_date;
			$data['organization'] = $organization;
			$data['subject_area'] = $subject_area;
			$data['remarks'] = $remarks;
			$data['SDAS_AllowSync'] = $SDAS_AllowSync;
			$data['participatedGroup'] = $participatedGroup;
			if($school_record_action=='new'){ 
				for($i=0;$i<count($target_user);$i++){
					$data['cur_student_id'] = $target_user[$i];
					$libkis_iportfolio->insertAwardRecord($data);
				}
			}else{
				$data['recordId'] = $recordId;
				$libkis_iportfolio->updateAwardRecord($data);
			}
			break;
		case 'saveSchoolAwardRecord':
			$data['school_year_id'] = $school_year_id;
			$data['school_year_term_id'] = $school_year_term_id;
			$data['award_name'] = $award_name;
			$data['award_date'] = $award_date;
			$data['organization'] = $organization;
			$data['subject_area'] = $subject_area;
			$data['remarks'] = $remarks;
			$data['SDAS_AllowSync'] = $SDAS_AllowSync;
			$data['participatedGroup'] = $participatedGroup;
			if($school_record_action=='new'){
				$libkis_iportfolio->insertSchoolAwardRecord($data);
			}else{
				$data['recordId'] = $recordId;
				$libkis_iportfolio->updateSchoolAwardRecord($data);
			}
			break;
		case 'saveActivityRecord':
			$data['school_year_id'] = $school_year_id;
			$data['school_year_term_id'] = $school_year_term_id;
			$data['activity_name'] = $activity_name;
			$data['role'] = $_POST['role'];
			$data['organization'] = $organization;
			$data['performance'] = $performance;
			if($school_record_action=='new'){ 
				for($i=0;$i<count($target_user);$i++){
					$data['cur_student_id'] = $target_user[$i];
					$libkis_iportfolio->insertActivityRecord($data);
				}
			}else{
				$data['recordId'] = $recordId;
				$libkis_iportfolio->updateActivityRecord($data);
			}
			break;
		############ Sync to CEES START ############
		case 'initSyncToCEESTable':
			include_once($intranet_root."/lang/lang.".$intranet_session_language.".php");
			include_once($intranet_root."/includes/cust/student_data_analysis_system_kis/libSDAS.php");
			include_once($intranet_root."/lang/kis/apps/lang_cees_".$intranet_session_language.".php");
			include_once($intranet_root."/includes/json.php");
			$libSDAS = new libSDAS();
			$JSONobj = new JSON_obj();
			$tableItem = array();
			$ButtonClass = 'class="btnSyncECA"';
			if($month== ''){
				$month = date('m');
			}
			$ECAReport = array(
					BuildMultiKeyAssoc($libSDAS->getInterSchoolECA($academicYearId),'ReportID'),
					BuildMultiKeyAssoc($libSDAS->getIntraSchoolECA($academicYearId),'ReportID'),
					$libSDAS->getMonthlyECA($academicYearId, $month)
			);
			$tableItem[0][0] = $Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'] . '(' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterSchool'] . ')';
			$tableItem[1][0] = $Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'] . '(' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['IntraSchool'] . ')';
			$tableItem[2][0] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Title'];
			$tableHtml = '';
			for($i=1; $i<3; $i++){
				if($i!=2 && count($ECAReport[$i])<=0){
					$tableItem[$i][1] = $Lang['General']['EmptySymbol'];
					$tableItem[$i][2] = $Lang['General']['EmptySymbol'];
					$tableItem[$i][3] = $Lang['General']['EmptySymbol'];
				} else {
					if($i!=2){
						if($ReportID[$i]){
							$rID = $ReportID[$i];
						} else {
							$firstReport = current($ECAReport[$i]);
							$rID = $firstReport['ReportID'];
						}
					}
					$ECAReportListData = array();
					if($i!=2){
						foreach($ECAReport[$i] as $id => $ecar){
							$ECAReportListData[] = array($id, $ecar['StartDate'] . ' ' . $Lang['General']['To'] . ' ' . $ecar['EndDate']);
						}
					} else {
						$yearName = getAcademicYearByAcademicYearID($academicYearId);
						$yearName = substr($yearName,0,4);
						$nextYr = strval($yearName) + 1;
						for($m=1;$m<=12;$m++){
							$selected = ($m==$selectedMonth) ? 'selected' : '';
							$str = ($m>=9? $yearName:$nextYr).'/'.($m<10?'0':'').$m;
							$ECAReportListData[] = array($m, $str);
							if($m==$month) $rID = $m;
						}
					}
					$onChange = ($i!=2) ? '' : '';
					$ECAReportSelect = getSelectByArray($ECAReportListData, 'name="ReportID['.$i.']" id="ReportID_'.$i.'" onChange="'.$onChange.'"', $rID, 0, $noFirst=1, $FirstTitle="");
					
					$isSubmit = $ECAReport[$i][$rID]['SubmitStatus'] == '1';
					$isDraft = $ECAReport[$i][$rID]['DraftStatus']=='1';
					if($i==2 && empty($ECAReport[$i]) ){
						$_updateBtn = $Lang['General']['EmptySymbol'];
					}else if(!$isSubmit && !$isDraft){
						$_updateBtn = "<input name='button' type='button' value='".$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Sync']." 'id='btnSyncECA_{$i}_{$rID}' $ButtonClass>";
					} else {
						$_updateBtn = $Lang['CEES']['Management']['SchoolActivityReport']['Hint']['AlreadySubmitted'];
					}
					$tableItem[$i][1] = $ECAReportSelect;
					$tableItem[$i][2] = $_updateBtn;
					$tableItem[$i][3] = $ECAReport[$i][$rID]['ModifiedDate'] ? $ECAReport[$i][$rID]['ModifiedDate']: $Lang['General']['EmptySymbol'];
				}
				$tableHtml .= '<tr valign="middle" class="tablerow'. ($i%2 +1).'">';
				foreach($tableItem[$i] as $k => $item){
					$tableHtml .= '<td nowrap="nowrap" align="center" id="contentRow'.$i.'_'.$k.'">' . $item . '</td>';
				}
				$tableHtml .= '</tr>';
			}
			
			?>
		<table class='common_table_list edit_table_list'>
        	<tr class="tabletop">
        		<th nowrap="nowrap" align="center" class="tabletopnolink"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['TargetReport']?></th>
        		<th nowrap="nowrap" align="center" class="tabletopnolink"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SelectReportPeriod']?></th>
        		<th nowrap="nowrap" align="center" class="tabletopnolink"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Sync']?></th>
        		<th nowrap="nowrap" align="center" class="tabletopnolink"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['LastModificatedDate']?></th>
        	</tr>
		    <?=$tableHtml?>
        </table>
		<?php
		break;
		case 'SyncToCEESReport':
			include_once($intranet_root."/includes/cust/student_data_analysis_system_kis/libSDAS.php");
			include_once($intranet_root."/lang/kis/apps/lang_cees_".$intranet_session_language.".php");
			include_once($intranet_root."/includes/json.php");
			$libSDAS = new libSDAS();
			$JSONobj = new JSON_obj();
			if($type!=2){
				$rID = $ReportID;
				$report = $libSDAS->getIntraSchoolECA($academicYearId,$rID);
				$isSubmit = $report['SubmitStatus'] == '1';
				$startDate = $report['StartDate'];
				$endDate = $report['EndDate'];
			} else {
				$rID = $ReportID;
				$report = $libSDAS->getMonthlyECA($academicYearId, $rID);
				if(empty($report)){
					$yearName = getAcademicYearByAcademicYearID($academicYearId);
					$yearName = substr($yearName, 0, 4);
					$month = $rID;
					if($month > 9)
						$yearName = strval($yearName) + 1;
					$yearName = $nextYr;
					$reportID = $libSDAS->initMonthlyECA($academicYearId, $month, date($yearName.'-'.($month<9?$month:'0'.$month).'-01'));
					$report = $libSDAS->getMonthlyECA($academicYearId, $month);
				}
				$rID = $report['ReportID'];
				$year = $report['Year'];
				$month = ($report['Month']<9?$report['Month']:'0'.$report['Month']);
				$startDate = date($year.'-'.$month.'-01');
				$endDate = date($year.'-'.$month.'-t');
			}
			// 1. Erase Existed Record
			$result = array();
			switch($type){
				case '0':
					$result['Delete'] = $libSDAS->clearInterSchoolECA($rID);
					break;
				case '1':
					$result['Delete'] = $libSDAS->clearIntraSchoolECA($rID,$exceptAward = false, $exceptActivity = true);
					$AllowSync = 'I';
					break;
				case '2':
					$result['Delete'] = $libSDAS->clearMonthlyECA($rID,$exceptAward = false, $exceptActivity = true);
					$AllowSync = 'M';
					break;
			}
			// 2. Insert Sync Record
			$insertDataAry = array();
			
			$table = $eclass_db . '.AWARD_SCHOOL';
			$col = "'4_1' AS SDAS_Category, AwardDate, AwardName, AwardFile, Details, Organization, SubjectArea, Remark, ParticipatedGroup";
			$conds = "AcademicYearID = '$academicYearId' AND AwardDate >= '$startDate' AND AwardDate <= '$endDate' AND SDAS_AllowSync LIKE '%{$AllowSync}%' ";
			$sql = "SELECT $col FROM $table WHERE $conds";
			
			$recordAry= $libSDAS->returnArray($sql);
			
			$colMap = array(
					"SDAS_Category" => "Section",
					"AwardDate" => "RecordDate",
					"SubjectArea" => "Event",
					"Organization" => "Organization",
					"ParticipatedGroup" => "ParticipatedGroup",
					"AwardName" => "Award",
					"Remark" => "Remark"
			);
			foreach($recordAry as $rc){
				$row = array();
				foreach($colMap as $key => $col){
					if($rc[$key] != '')	$row[$col] = $rc[$key];
				}
				$row['ReportID'] = $rID;
				$insertDataAry[] = $row;
			}
			
			$table = $eclass_db . '.AWARD_STUDENT';
			$col = "'4_3' AS SDAS_Category,a.AwardDate, a.AwardName, a.AwardFile, a.Details, a.Organization, a.SubjectArea, a.Remark, a.ParticipatedGroup,
				GROUP_CONCAT(iu.EnglishName) AS EnglishName, GROUP_CONCAT(iu.ChineseName) AS ChineseName,
				GROUP_CONCAT(DISTINCT yc.ClassTitleB5) AS ClassTitleB5, GROUP_CONCAT(DISTINCT yc.ClassTitleEN) AS ClassTitleEN, COUNT(*) AS Participant
				";
			$conds = "a.AcademicYearID = '$academicYearId' AND a.AwardDate >= '$startDate' AND a.AwardDate <= '$endDate' AND a.SDAS_AllowSync LIKE '%M%' ";
			$sql = "SELECT $col FROM $table a
					INNER JOIN INTRANET_USER iu
					ON iu.UserID = a.UserID
					INNER JOIN YEAR_CLASS_USER ycu
					ON iu.UserID = ycu.UserID
					INNER JOIN YEAR_CLASS yc
					ON yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = a.AcademicYearID
					WHERE $conds
					GROUP BY a.AwardDate, AwardName, a.Organization, a.ParticipatedGroup";
			$recordAry= $libSDAS->returnArray($sql);
			$_ceesReportNameField = Get_Lang_Selection('ChineseName','EnglishName');
			$colMap = array(
					"SDAS_Category" => "Section",
					"AwardDate" => "RecordDate",
					"SubjectArea" => "Event",
					"Organization" => "Organization",
					"ParticipatedGroup" => "ParticipatedGroup",
					"AwardName" => "Award",
					"Remark" => "Remark",
					"ClassTitleEN" => "Class",
					$_ceesReportNameField => "Name",
			);
			$colMap["Participant"] = "Participant";
			
			foreach($recordAry as $rc){
				$row = array();
				foreach($colMap as $key => $col){
					if($rc[$key] != '')	$row[$col] = $rc[$key];
				}
				$row['ReportID'] = $rID;
				$insertDataAry[] = $row;
			}
			switch($type){
				case '0':
					$result['Insert'] = $libSDAS->insertInterSchoolECARecord($rID, $insertDataAry);
					$result['Update'] = $libSDAS->updateInterSchoolECA($rID, array());
					break;
				case '1':
					$result['Insert'] = $libSDAS->insertIntraSchoolECARecord($rID, $insertDataAry);
					$result['Update'] = $libSDAS->updateIntraSchoolECA($rID, array());
					break;
				case '2':
					$result['Insert'] = $libSDAS->insertMonthlyECARecord($rID, $insertDataAry);
					$result['Update'] = $libSDAS->updateMonthlyECA($rID, array());
					break;
			}
		case 'RefreshSyncToCEESReport':
			include_once($intranet_root."/includes/cust/student_data_analysis_system_kis/libSDAS.php");
			include_once($intranet_root."/lang/kis/apps/lang_cees_".$intranet_session_language.".php");
			include_once($intranet_root."/includes/json.php");
			$libSDAS = new libSDAS();
			$JSONobj = new JSON_obj();
			$ButtonClass = 'class="btnSyncECA"';
			if($type!=2){
				$rID = $ReportID;
				$report = ($type==0 ? $libSDAS->getInterSchoolECA($academicYearId,$rID) : $libSDAS->getIntraSchoolECA($academicYearId,$rID));
			} else {
				$rID = $month;
				$report = $libSDAS->getMonthlyECA($academicYearId, $month);
			}
			$isSubmit = $report['SubmitStatus'] == '1';
			$isDraft= $report['DraftStatus'] == '1';
			if(!$isSubmit && !$isDraft){
				$_updateBtn = "<input name='button' type='button' value='".$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Sync']."' id='btnSyncECA_{$type}_{$rID}' $ButtonClass>";
			} else {
				$_updateBtn = $Lang['CEES']['Management']['SchoolActivityReport']['Hint']['AlreadySubmitted'];
			}
			$ModifiedDate = $report['ModifiedDate'] ? $report['ModifiedDate']: $Lang['General']['EmptySymbol'];
			echo $JSONobj->encode(array($_updateBtn,$ModifiedDate));
			break;
		############# Sync to CEES END #############
		######## Portfolio Data Imort START ########
		case 'checkImportPortfolioDataInfo':
			include_once($intranet_root."/includes/libfilesystem.php");
			include_once($intranet_root."/includes/libftp.php");
			include_once($intranet_root."/includes/libimport.php");
			include_once($intranet_root."/includes/libimporttext.php");
			
			$limport = new libimporttext();
			$lo = new libfilesystem();
			$li = new libimport();
			
			### CSV Checking
			$name = $_FILES['userfile']['name'];
			$ext = strtoupper($lo->file_ext($name));
			
			if(!($ext == ".CSV" || $ext == ".TXT"))
			{
				intranet_closedb();
				header("location: import.php?xmsg=import_failed&TabID=$TabID");
				exit();
			}
			$data = $limport->GET_IMPORT_TXT($userfile);
			array_shift($data);
			
			$csv_header = array_shift($data);
			
			$file_format = array('Class Name',
				'Class Number',
				'Start age (Year)',
				'Start age (Month)',
				'Start weight',
				'Start Height',
				'End age (Year)',
				'End age (Month)',
				'End weight',
				'End Height',
				'School Days',
				'Present Days',
				'Sick Leave Days',
				'Other Leave Days',
				'Late Days',
				'Early Leave Days',
				'Award',
				'Remarks'
			);
			
			# check csv header
			$format_wrong = false;
			
			for($i=0; $i<sizeof($file_format); $i++)
			{
				if ($csv_header[$i]!=$file_format[$i])
				{
			
					$format_wrong = true;
					break;
				}
			}
			
			if($format_wrong)
			{
				echo "<font color=red>".$kis_lang['invalidHeader']."!</font>";
				exit();
			}
			if(empty($data))
			{
				echo "<font color=red>".$kis_lang['CSVFileNoData']."!</font>";
				exit();
					
			}

			$schoolYearID = $_REQUEST['school_year_id'];
			echo $libkis_iportfolio->checkImportDataForImportPortfolioData($schoolYearID, $data);
			break;
		case 'importPortfolioDataInfo':
			include_once($intranet_root."/includes/libfilesystem.php");
			include_once($intranet_root."/includes/libftp.php");
			include_once($intranet_root."/includes/libimport.php");
			include_once($intranet_root."/includes/libimporttext.php");
			
			$limport = new libimporttext();
			$lo = new libfilesystem();
			$li = new libimport();
	
			### CSV Checking
			$name = $_FILES['userfile']['name'];
			$ext = strtoupper($lo->file_ext($name));
			
			if(!($ext == ".CSV" || $ext == ".TXT"))
			{
				intranet_closedb();
				header("location: import.php?xmsg=import_failed&TabID=$TabID");
				exit();
			}
			$data = $limport->GET_IMPORT_TXT($userfile);
			$schoolYearID = $_REQUEST['school_year_id'];
			$school_year_term_id = $_REQUEST['school_year_term_id'];
			
			$result = $libkis_iportfolio->importDataForImportPortfolioData($schoolYearID, $school_year_term_id, $data);

			if(!in_array(false,$result))
				echo $kis_lang['ImportSuccessful'];
			else
				echo "Error!";
			break;
		######## Portfolio Data Imort END ########
		
		case 'createGroup':
			include_once($intranet_root."/includes/portfolio25/libpf-mgmt-group.php");
			$has_right = 0;
			$lpf_mgmt_group = new libpf_mgmt_group();
			$lpf_mgmt_group->setGroupName($_REQUEST['group_title']);
			$lpf_mgmt_group->setGroupDesc($_REQUEST['group_desc']);
			$lpf_mgmt_group->setGroupStatus(1);
			$lpf_mgmt_group->setGroupHasRight($has_right);
			
			$lpf_mgmt_group->ADD_GROUP();
				
			break;
			
		case 'editGroup' :
			include_once ($intranet_root . "/includes/portfolio25/libpf-mgmt-group.php");
			$has_right = 0;
			$lpf_mgmt_group = new libpf_mgmt_group ();
			$lpf_mgmt_group->setGroupName ( $_REQUEST ['group_title'] );
			$lpf_mgmt_group->setGroupDesc ( $_REQUEST ['group_desc'] );
			$lpf_mgmt_group->setGroupStatus ( 1 );
			$lpf_mgmt_group->setGroupHasRight ( $has_right );
			
			$lpf_mgmt_group->setGroupID($_REQUEST ['groupId']);
			
			$lpf_mgmt_group->UPDATE_GROUP();
			
			break;
			
		case 'getGroup':
			include_once($intranet_root."/includes/portfolio25/libpf-mgmt-group.php");
			
			$lpf_mgmt_group = new libpf_mgmt_group();
			$lpf_mgmt_group->setGroupID($_REQUEST['group_id']);
			$lpf_mgmt_group->setGroupProperty();
			
			$returnAry = array();
			$GroupName = $lpf_mgmt_group->getGroupName();
			$returnAry['GroupName'] = htmlentities($GroupName, ENT_COMPAT, "UTF-8");
			$GroupDesc = $lpf_mgmt_group->getGroupDesc();
			$returnAry['GroupDesc'] = htmlentities($GroupDesc, ENT_COMPAT, "UTF-8");
			
			echo $libjson->encode($returnAry);
			break;
		
		case 'deleteGroup':
			include_once($intranet_root."/includes/portfolio25/libpf-mgmt-group.php");
			$lpf_mgmt_group = new libpf_mgmt_group();
			$lpf_mgmt_group->DELETE_GROUP($_REQUEST['group_id']);
			break;
			
		case 'updateStudentList':
			include_once($intranet_root."/includes/portfolio25/libpf-mgmt-group.php");
			$lpf_mgmt_group = new libpf_mgmt_group();
			
			$lpf_mgmt_group->setGroupID($groupId);
			$lpf_mgmt_group->setGroupMember($uID);
			
			$lpf_mgmt_group->ADD_GROUP_MEMBER();
			break;
		
		case 'deleteStudentList':
			include_once($intranet_root."/includes/portfolio25/libpf-mgmt-group.php");
			$lpf_mgmt_group = new libpf_mgmt_group();
			
			$lpf_mgmt_group->setGroupID($groupId);
			$lpf_mgmt_group->DELETE_GROUP_MEMBER($student_id);
			break;
			
		case 'updateTeacherList':
			#### Get Old User START ####
			$sql = "SELECT
			UserID
			FROM
			{$intranet_db}.ASSESSMENT_ACCESS_RIGHT
			WHERE
			AccessType = '0'
			AND
			SubjectID IS NULL";
				
			$oldUser = $lo->returnArray($sql);
			#### Get Old User END ####
			
			foreach($uID as $teacherID){
				if(!in_array($teacherID, $oldUser)){
					$sql = "INSERT INTO {$intranet_db}.ASSESSMENT_ACCESS_RIGHT (
					UserID,
					AccessType,
					InputDate,
					CreatedBy
					) VALUES (
					'{$teacherID}',
					'0',
					CURRENT_TIMESTAMP,
					'{$UserID}'
					)";
					$lo->db_db_query($sql);
				}
			}
					
			break;
		
		case 'deleteTeacherList':
			
			$recordIdList = implode(",", $teacher_id);
			$recordIdList = IntegerSafe($recordIdList);
				
			$sql = "DELETE FROM {$intranet_db}.ASSESSMENT_ACCESS_RIGHT WHERE RecordID IN ($recordIdList)";
			$lo->db_db_query($sql);
			
			
		case 'addSubjectPanelList':
			
			$yearID = (array)IntegerSafe($_REQUEST['yearId']);
			#### Get Old User START ####
			$sql = "SELECT
			*
			FROM
			{$intranet_db}.ASSESSMENT_ACCESS_RIGHT
			WHERE
			AccessType = '1'
			AND
			SubjectID IS NULL";
			$rs = $lo->returnResultSet($sql);
			
			$oldUser = array();
			foreach($rs as $r){
				$oldUser[ $r['UserID'] ][ $r['YearID'] ] = 1;
			}
			#### Get Old User END ####
			
			
			#### Add User START ####
				foreach($yearID as $year){
					if(!$oldUser[$teacherID][$year]){
						$sql = "INSERT INTO {$intranet_db}.ASSESSMENT_ACCESS_RIGHT (
						UserID,
						AccessType,
						SubjectID,
						YearID,
						InputDate,
						CreatedBy
						) VALUES (
						'{$teacherID}',
						'1',
						NULL,
						'{$year}',
						CURRENT_TIMESTAMP,
						'{$UserID}'
						)";
						$lo->db_db_query($sql);
					}
				}
			
			#### Add User END ####
			break;
			
		case 'deleteSubjectPanelList':
			$recordIdList = (array)$_REQUEST['subjectbox_id'];
			$recordIdList = implode(",", $recordIdList);
			$recordIdList = IntegerSafe($recordIdList);
			
			$sql = "DELETE FROM {$intranet_db}.ASSESSMENT_ACCESS_RIGHT WHERE RecordID IN ($recordIdList)";
			$lo->db_db_query($sql);
			
			break;
	default:
	break;
	
    }
	
}else if ($kis_user['type']==kis::$user_types['parent']){
    
    $liblp2 = new libpf_lp2($kis_user['current_child'], 0, $kis_user['current_child'], 'publish');
    switch ($action){
		case 'editSBSContent':
			$content = $libkis_iportfolio->getSBSContent($parentId,$assignmentId);
			echo $libjson->encode($content); 
			break;
		case 'saveSBSContent':
			$libkis_iportfolio->saveSBSContent($p_id,$a_id,$h_id,$ans_str);
			break;
		case 'getStudentAssessmentFile':
			$studentId = $kis_user['current_child'];
			$AssessmentArr = current($libkis_iportfolio->getStudentAssessmentList($assessmentId,'',array($studentId), $status='all', $keyword='', $schoolYear='all'));
			if(!$AssessmentArr['Status']){
				$libkis_iportfolio->updateStudentAssessment($assessmentId,$studentId,1);
			}
			$folder = 'assessment/'.$assessmentId.'/'.$studentId;
			$file_name = $AssessmentArr['assessment']['title'];
			$attachment_path = $file_path.kis_iportfolio::$attachment_url.$folder.'/'.$file_name;
			if (file_exists($attachment_path)){
				kis_utility::downloadFile($attachment_path, $file_name);
			}
			break;
	default:
	
	    
	break;
    
	
    }
}

?>