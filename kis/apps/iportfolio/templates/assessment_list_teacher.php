<script>
$(function(){
    kis.iportfolio.assessment_list_teacher_init();
});
</script>
<style>
.fancybox-skin	{background-color:#fcf1d5;}
</style>
<p class="spacer"></p>
<form name="assessmentForm" class="filter_form" method="post">
    <div class="table_board">
        <div class="Content_tool"><a href="#create_new_box" class="new fancybox fancybox_smallnew  fancybox fancybox_small"><?=$kis_lang['new']?></a></div>
            <p class="spacer"></p>
            <div id="table_filter">
				<?=$kis_data['ClassList']?>
				<?=$kis_data['StatusSelection']?>
            </div>
            <div class="search"><!--<a href="#">Advanced</a>-->
				<input type="text" name="keyword" class="auto_submit" id="keyword" placeholder="<?=$kis_lang['Assessment']['Search']?>" value="<?=$kis_data['keyword']?>"/>
			</div>
			<p class="spacer"></p>&nbsp;
            <p class="spacer"></p>
			<div id="div_assessment_list"></div>
				<table class="common_table_list">
                        <tr class="sub_table_top">
                          <th><? kis_ui::loadSortButton('title','assessment_title', $sortby, $order)?></th>
                          <th><? kis_ui::loadSortButton('release_date','release_date', $sortby, $order)?></th>
                          <th><? kis_ui::loadSortButton('classname','classname', $sortby, $order)?></th>
                          <th><?=$kis_lang['Assessment']['NoOfUploads']?></th>
                          <th>&nbsp;</th>
                        </tr>
				<?php for($i=0;$i<$kis_data['total'];$i++){ ?>
                        <tr>
                          <td><a href="teacher_iportfolio_assessment_class.htm"><?=$kis_data['assessment_list'][$i]['title']?></a></td>
                          <td><?=$kis_data['assessment_list'][$i]['release_date']?><span class="date_time"><em><?=$kis_data['assessment_list'][$i]['created_user']?></em></span></td>
                          <td><?=$kis_data['assessment_list'][$i]['classname']?></td>
                          <td><span class="common_table_list edit_table_list">15/18</span></td>
                          <td><div class="table_row_tool row_content_tool"><a href="#" class="edit_dim" title="Delete"></a><a href="#" class="copy_dim" title="Copy"></a><a href="#" class="delete_dim" title="Delete"></a></div>
                              <div class="table_row_tool row_content_tool"></div></td>
                        </tr>
				<?php } ?>
                      </table>
                    	  <p class="spacer"></p>
                      <? kis_ui::loadPageBar($kis_data['page'], $kis_data['amount'], $kis_data['total'], $kis_data['sortby'], $kis_data['order']) ?>
                      	
                      	    	
							<p class="spacer"></p><br />
        </div>
		
		<!--FancyBox-->
		<div id='create_new_box' style="padding:5px;display:none;" class="pop_edit">
			<div class="pop_title">
				<span><?=$kis_lang['Assessment']['NewAssessment']?></span>
			</div>
			<div class="table_board">
    
				<table class="form_table">
					<tr>
						<td class="field_title"><?=$kis_lang['Assessment']['Title']?></td>
						<td ><input name="title" type="text" id="title" class="textboxtext" /></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['Assessment']['ReleaseDate']?></td>
						<td ><input name="release_date" type="text" id="release_date" size="15" /></td>
					</tr>
          <tr>
            <td class="field_title"><?=$kis_lang['Assessment']['Target']?></td>
            <td><?=$kis_data['ClassList']?></td>
          </tr>
          <col class="field_title" />
          <col  class="field_c" />
        </table>
      <p class="spacer"></p>
      </div>
        <div class="edit_bottom">         
           <input id="submitBtn" name="submitBtn" type="button" class="formbutton" value="Submit" />
           <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();" value="<?=$kis_lang['cancel']?>" />
      </div>
		</div>
</form>