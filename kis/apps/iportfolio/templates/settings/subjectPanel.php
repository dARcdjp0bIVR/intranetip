<script>
$(function(){
    kis.iportfolio.settings_subjectpanellist_init({
	message: '<?=$kis_lang['areyousureto']?>',
	remove: '<?=strtolower($kis_lang['remove'])?> ',
	please_select: '<?=$kis_lang['please_select']?> ',
	user: '<?=strtolower($kis_lang['user'])?> '
	});
	
});


function checkform(formObj){
	$("#SelectedUser option").attr("selected", true);
	return true;
}

function resizeSelect(){
	$("#User2Select").width($("#User2Select").parent().width());
	$("#SelectedUser").width($("#SelectedUser").parent().width());
}

$(document).ready(function(){
	$("#AddAll").click(function(){
		$("#User2Select option").remove().appendTo("#SelectedUser");
		resizeSelect();
	});
	
	$("#Add").click(function(){
		$("#User2Select option:selected").remove().appendTo("#SelectedUser");
		resizeSelect();
	});
	
	$("#Remove").click(function(){
		$("#SelectedUser option:selected").remove().appendTo("#User2Select");
		resizeSelect();
	});
	
	$("#RemoveAll").click(function(){
		$("#SelectedUser option").remove().appendTo("#User2Select");
		resizeSelect();
	});
});

</script>
<? kis_ui::loadModuleTab(array('studentgroup','teachergroup','subjectpanel'), 'subjectpanel', '#apps/iportfolio/settings/') ?>
<p class="spacer"></p>

<div class="table_board">			
		<!--Filter Form-->
			<form class="filter_form" method="post">   
				<p class="spacer"></p>
				<div class="search"><!--<a href="#">Advanced</a>-->
					<input type="text" name="keyword" class="auto_submit" id="keyword" placeholder="<?=$kis_lang['Assessment']['Search']?>" value="<?=$kis_data['keyword']?>"/>
				</div>
			</form>
			<?=$kis_data['NavigationBar'] ?>
			<p class="spacer"></p>&nbsp;
            <p class="spacer"></p>
            <div class="Content_tool"><a href="#" class="new"><?=$kis_lang['new']?></a></div>
			<div class="common_table_tool common_table_tool_table">
					<a href="#" class="tool_delete"><?=$kis_lang['delete'] ?></a>
			</div>
			<form>
				<table class="common_table_list">
                        <tr class="sub_table_top">
                          <th>#</th>
                          <th><? kis_ui::loadSortButton('DisplayName','member_name', $sortby, $order)?></th>
                          <th><? kis_ui::loadSortButton('YearName','form', $sortby, $order)?></th>
                          <th><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,'subjectbox_id[]'):setChecked(0,this.form,'subjectbox_id[]')"></th>
                        </tr>
               <?php if(count($kis_data['subject_list']) == 0){
               	echo "<td colspan='4'>No Record</td>";
               }?>
               <?php for($i=0;$i<count($kis_data['subject_list']);$i++){ ?>
               			<form>
                        <tr>
                          <td><?=$i+1 ?></td>
                          <td><?=$kis_data['subject_list'][$i]['DisplayName'] ?></td>
                          <td><?=$kis_data['subject_list'][$i]['YearName']?></td>
                          <td><?=$kis_data['subject_list'][$i]['checkbox']?></td>
                        </tr>
				<?php } ?>
                      </table>
             </form>
                    	  <p class="spacer"></p>
                      <?php list($page,$amount,$total,$sortby,$order) = $kis_data['PageBar'];  
                      ?>
						<? kis_ui::loadPageBar($page, $amount, $total, $sortby, $order) ?>     
                      	    	
							<p class="spacer"></p><br />	
						
        </div>
        
        		<!--FancyBox-->
		
		<div id='create_new_box' style="padding:5px;display:none;" class="pop_edit">
			<div class="pop_title">
				<span><?=$kis_lang['Settings']['NewTeacher']?></span>
			</div>
			<div class="table_board" style="height:330px">

			<form id="subjectlistForm" method="post">
				<table class="form_table">
					<tr>
						<td><?=$kis_lang['member_name'] ?>:</td>
						<td><?=$kis_data['new_table']['teacherSelect']?></td>
					</tr>
					
					<tr>
						<td><?=$kis_lang['form'] ?>:</td>
						<td><?=$kis_data['new_table']['formCheckbox']?></td>
					</tr>
					
				  <col class="field_title" />
				  <col  class="field_c" />
				</table>
			<input type="hidden" name="group_type" value="0">
			<input type="hidden" name="type" value="teacher">
			<input type="hidden" name="groupId" value="<?=$kis_data['group_id'] ?>">
		</form>	
      <p class="spacer"></p>
      </div>
        <div class="edit_bottom">         
           <input id="submitBtn" name="submitBtn" type="button" class="formbutton" value="<?=$kis_lang['submit']?>" />
           <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();" value="<?=$kis_lang['cancel']?>" />
      </div>
		</div>