<script>
$(function(){
    kis.iportfolio.settings_memberlist_init({
	message: '<?=$kis_lang['areyousureto']?>',
	remove: '<?=strtolower($kis_lang['remove'])?> ',
	please_select: '<?=$kis_lang['please_select']?> ',
	user: '<?=strtolower($kis_lang['user'])?> ',
	type: 'student'
	});
	
});


function checkform(formObj){
	$("#SelectedUser option").attr("selected", true);
	return true;
}

function resizeSelect(){
	$("#User2Select").width($("#User2Select").parent().width());
	$("#SelectedUser").width($("#SelectedUser").parent().width());
}

$(document).ready(function(){
	$("#AddAll").click(function(){
		$("#User2Select option").remove().appendTo("#SelectedUser");
		resizeSelect();
	});
	
	$("#Add").click(function(){
		$("#User2Select option:selected").remove().appendTo("#SelectedUser");
		resizeSelect();
	});
	
	$("#Remove").click(function(){
		$("#SelectedUser option:selected").remove().appendTo("#User2Select");
		resizeSelect();
	});
	
	$("#RemoveAll").click(function(){
		$("#SelectedUser option").remove().appendTo("#User2Select");
		resizeSelect();
	});
});

</script>
<? kis_ui::loadModuleTab(array('studentgroup','teachergroup','subjectpanel'), 'studentgroup', '#apps/iportfolio/settings/') ?>
<p class="spacer"></p>

<div class="table_board">			
		<!--Filter Form-->
			<form class="filter_form" method="post">   
				<p class="spacer"></p>
				<div class="search"><!--<a href="#">Advanced</a>-->
					<input type="text" name="keyword" class="auto_submit" id="keyword" placeholder="<?=$kis_lang['Assessment']['Search']?>" value="<?=$kis_data['keyword']?>"/>
				</div>
			</form>
			<?=$kis_data['NavigationBar'] ?>
			<p class="spacer"></p>&nbsp;
            <p class="spacer"></p>
            <div class="Content_tool"><a href="#" class="new"><?=$kis_lang['new']?></a></div>
			<div class="common_table_tool common_table_tool_table">
					<a href="#" class="tool_delete"><?=$kis_lang['delete'] ?></a>
			</div>
			<form>
				<table class="common_table_list">
                        <tr class="sub_table_top">
                          <th>#</th>
                          <th><? kis_ui::loadSortButton('DisplayName','member_name', $sortby, $order)?></th>
                          <th><? kis_ui::loadSortButton('ClassName','class', $sortby, $order)?></th>
                          <th><? kis_ui::loadSortButton('ClassNumber','class_number', $sortby, $order)?></th>
                          <th><? kis_ui::loadSortButton('identity','identity', $sortby, $order)?></th>
                          <th><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,'student_id[]'):setChecked(0,this.form,'student_id[]')"></th>
                        </tr>
               <?php if(count($kis_data['member_list']) == 0){
               	echo "<td colspan='6'>No Record</td>";
               }?>
               <?php for($i=0;$i<count($kis_data['member_list']);$i++){ ?>
               			<form>
                        <tr>
                          <td><?=$i+1 ?></td>
                          <td><?=$kis_data['member_list'][$i]['DisplayName'] ?></td>
                          <td><?=$kis_data['member_list'][$i]['ClassName']?></td>
                          <td><?=$kis_data['member_list'][$i]['ClassNumber'] ?></td>
                          <td><?=$kis_data['member_list'][$i]['identity']?></td>
                          <td><?=$kis_data['member_list'][$i]['checkbox']?></td>
                        </tr>
				<?php } ?>
                      </table>
             </form>
                    	  <p class="spacer"></p>
                      <?php list($page,$amount,$total,$sortby,$order) = $kis_data['PageBar'];  
                      ?>
						<? kis_ui::loadPageBar($page, $amount, $total, $sortby, $order) ?>     
                      	    	
							<p class="spacer"></p><br />	
						
        </div>
        
        		<!--FancyBox-->
		
		<div id='create_new_box' style="padding:5px;display:none;" class="pop_edit">
			<div class="pop_title">
				<span><?=$kis_lang['Settings']['NewStudent']?></span>
			</div>
			<div class="table_board" style="height:330px">

			<form id="memberlistForm" method="post">
				<table class="form_table">
					<tr>
					<td>
						<table width="100%" border="0" cellpadding="5" cellspacing="0">
							<col style="width:50%">
							<col style="width:40px">
							<col style="width:50%">
							<tr>
								<td bgcolor="#eeeeee">&nbsp</td>
								<td></td>
								<td class="steptitletext" bgcolor="#effee2"><?=$kis_lang['Settings']['SelectedUser']?> </td>
							</tr>
							<tr>
								<td align="center" bgcolor="#eeeeee">
									<?=$kis_data['select_list']?>	
									<span style="color:grey"><?=$kis_lang['Settings']['CtrlMultiSelectMessage']?> </span>
								</td>
								<td>
									<input id="AddAll" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;&gt;" style="width: 40px;" title="Add All" type="button"><br>
									<input id="Add" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;" style="width: 40px;" title="Add Selected" type="button"><br><br>
									<input id="Remove" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;" style="width: 40px;" title="Remove Selected" type="button"><br>
									<input id="RemoveAll" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;&lt;" style="width: 40px;" title="Remove All" type="button">
								</td>
								<td id="SelectedUserCell" bgcolor="#effee2"><?=$kis_data['selected_list']?>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				  <col class="field_title" />
				  <col  class="field_c" />
				</table>
			<input type="hidden" name="group_type" value="0">
			<input type="hidden" name="type" value="student">
			<input type="hidden" name="groupId" value="<?=$kis_data['group_id'] ?>">
		</form>	
      <p class="spacer"></p>
      </div>
        <div class="edit_bottom">         
           <input id="submitBtn" name="submitBtn" type="button" class="formbutton" value="<?=$kis_lang['submit']?>" />
           <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();" value="<?=$kis_lang['cancel']?>" />
      </div>
		</div>