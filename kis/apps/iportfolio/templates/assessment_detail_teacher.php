

                         <div class="navigation_bar">
                	<a href="#/apps/iportfolio/assessments"><?=$kis_lang['assessmentlist']?></a><span>第一階段中文學習評估(2012-09-01 to 2012-12-30) </span>                </div>
<form id="filter_form">                 
    <div id="table_filter">
	<select name="class">
	     <option value=""><?=$kis_lang['all'] ?></option>
	     <option <?=$class=='apple'?'selected':''?> value="apple">Class Apple</option>
	     <option <?=$class=='banana'?'selected':''?> value="banana">Class Banana</option>
	     
	</select>
	
    </div>

</form>

                       <div class="table_tool"><a href="#/apps/eattendance/dailyrecord/1" class="tool_edit"><?=$kis_lang['edit']?></a></div>
                      <p class="spacer"></p>
                    	  <div class="table_board">
          <table class="common_table_list edit_table_list">
							<colgroup><col nowrap="nowrap">
							</colgroup><thead>
								<tr>
								  <th width="20">#</th>
								  <th width="">Student</th>
								  <th>From</th>
								  <th>Last Updated</th>
							  </tr>
							</thead>
							<tbody>
								<tr>
								  <td>1</td>
								  <td>Chan Ka Wing</td>
								  <td><div class="table_tool" style="float:left"><a href="#/apps/iportfolio/assessments/1?student=2&edit=1" class="tool_edit">Edit</a></div></td>
								  <td>--</td>
							  </tr>
								<tr>
								  <td>2</td>
								  <td>Chan Siu Ming</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_view">View</a></div></td>
								  <td>2012-12-20 by Miss chan</td>
							    </tr>
								<tr>
								  <td>3</td>
								  <td class="absent">Cheung Tai Ming</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_edit">Edit</a></div></td>
								  <td>--</td>
								</tr>
								<tr>
								  <td>4</td>
								  <td>Chung Shan Shan</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_view">View</a></div></td>
								  <td>2012-12-20 by Miss chan</td>
								</tr>
								<tr>
								  <td>5</td>
								  <td>Hui Wai Kit</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_edit">Edit</a></div></td>
								  <td>--</td>
							  </tr>
								<tr>
                                  <td>1</td>
								  <td>Kwok Ka Hei</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_view">View</a></div></td>
								  <td>2012-12-20 by Miss chan</td>
							  </tr>
								<tr>
                                  <td>2</td>
								  <td class="past">Lau Yuk Yi</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_edit">Edit</a></div></td>
								  <td>--</td>
							  </tr>
								<tr>
                                  <td>3</td>
								  <td class="past">Lee Tze Wing</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_view">View</a></div></td>
								  <td>2012-12-20 by Miss chan</td>
							  </tr>
								<tr>
                                  <td>4</td>
								  <td>Leung Siu Ming</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_view">View</a></div></td>
								  <td>2012-12-20 by Miss chan</td>
							  </tr>
								<tr>
                                  <td>5</td>
								  <td>Li Ming Ming</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_view">View</a></div></td>
								  <td>2012-12-20 by Miss chan</td>
							  </tr>
								<tr>
                                  <td>6</td>
								  <td>Ng Chi Kit</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_view">View</a></div></td>
								  <td>2012-12-20 by Miss chan</td>
							  </tr>
								<tr>
                                  <td>1</td>
								  <td class="past">Wong Man Hin</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_view">View</a></div></td>
								  <td>2012-12-20 by Miss chan</td>
							  </tr>
								<tr>
                                  <td>2</td>
								  <td>Wong Pui Man&lt;</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_view">View</a></div></td>
								  <td>2012-12-20 by Miss chan</td>
							  </tr>
								<tr>
                                  <td>3</td>
								  <td>Wong Shek Leung</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_edit">Edit</a></div></td>
								  <td>--</td>
							  </tr>
								<tr>
                                  <td>4</td>
								  <td>Wong Siu Man</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_edit">Edit</a></div></td>
								  <td>--</td>
							  </tr>
								<tr>
                                  <td>5</td>
								  <td>Wong Wai Man</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_edit">Edit</a></div></td>
								  <td>--</td>
							  </tr>
								<tr>
                                  <td>6</td>
								  <td>Yeung Ka Yan</td>
								  <td><div class="table_tool" style="float:left"><a href="#" class="tool_view">View</a></div></td>
								  <td>2012-12-20 by Miss chan</td>
							  </tr>
								<tr>
								  <td>6</td>
                                  <td class="past">Yip Wai Ming</td>
                                  <td><div class="table_tool" style="float:left"><a href="#" class="tool_view">View</a></div></td>
                                  <td>2012-11-18 by Miss chan</td>
							  </tr>
							</tbody>
						</table>
          <p class="spacer"></p><p class="spacer"></p><br>
        </div>
                    
                    
                    