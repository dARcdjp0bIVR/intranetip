<?php

$tabArray = array('awards','activities');

if($sys_custom['iPf']['chiuchunkg']['SBS']){ // CCKG Cust
	$tabArray[] = 'portfoliodata';
}
global $plugin;
if($plugin['SDAS_module']['KISMode']){
	$tabArray[] = 'schoolaward';
	$tabArray[] = 'synctocees';
}
// debug_r($kis_data['portfolio_record']);
?>
<script>
$(function(){
    kis.iportfolio.teacher_schoolrecord_list_init({
		are_you_sure_to_delete: '<?=$kis_lang['msg']['are_you_sure_to_delete']?> ',
	});
});
</script>
<?php list($page,$amount,$total,$sortby,$order) = $kis_data['PageBar'];  ?>
<div class="main_content">
 <? kis_ui::loadModuleTab($tabArray, 'portfoliodata', '#apps/iportfolio/schoolrecords/') ?>
 <?=$kis_data['NavigationBar']?>
    <div class="table_board">
		<p class="spacer"></p>
		<form class="filter_form">
			<div id="table_filter">
				<?=$kis_data['select_academicYear']?>
				<span id="span_term"><?=$kis_data['select_academicYearTerm']?></span>
				<input type="hidden" name="retrieve_type" id="retrieve_type" value="portfoliodata"> 
				<input type="hidden" name="classId" id="classId" value="<?=$kis_data['classId']?>">
			</div>
			<div class="page_no student_no" ><span><?=$kis_lang['student']?> :</span><?=$kis_data['StudentList']?></div>
		</form>
        <p class="spacer"></p>&nbsp;
<!-- -------- Display Data START -------- -->
		
			<table class="form_table">
				<colgroup>
					<col width="30%">
				</colgroup>
				<tbody>
					<tr>
						<td class="field_title"><?=$kis_lang['PortfolioData']['Birthday'] ?></td>
						<td><?=kis_ui::displayTableField($kis_data['portfolio_record']['birthday']) ?></td>
					</tr>
					<tr>
						<td colspan="2">
							<table class="form_table">
								<tbody>
									<tr>
										<td width="30%">&nbsp;</td>
										<td class="form_guardian_head" width="35%">
											<?=$kis_lang['PortfolioData']['StartSchool'] ?>
										</td>
										<td class="form_guardian_head" width="35%">
											<?=$kis_lang['PortfolioData']['EndSchool'] ?>
										</td>
									</tr>
									<tr>
										<td class="field_title"><?=$kis_lang['PortfolioData']['Ages'] ?></td>
										<td class="form_guardian_field">
											<?=kis_ui::displayTableField($kis_data['portfolio_record']['StartAgeYear']) ?>
											<?=$kis_lang['PortfolioData']['Age']['Year'] ?>
											<?=kis_ui::displayTableField($kis_data['portfolio_record']['StartAgeMonth']) ?>
											<?=$kis_lang['PortfolioData']['Age']['Month'] ?>
										</td>
										<td class="form_guardian_field">
											<?=kis_ui::displayTableField($kis_data['portfolio_record']['EndAgeYear']) ?>
											<?=$kis_lang['PortfolioData']['Age']['Year'] ?>
											<?=kis_ui::displayTableField($kis_data['portfolio_record']['EndAgeMonth']) ?>
											<?=$kis_lang['PortfolioData']['Age']['Month'] ?>
										</td>
									</tr>
									<tr>
										<td class="field_title"><?=$kis_lang['PortfolioData']['Weight'] ?></td>
										<td class="form_guardian_field">
											<?=kis_ui::displayTableField($kis_data['portfolio_record']['StartWeight']) ?>
											<?=$kis_lang['PortfolioData']['WeightKg'] ?>
										</td>
										<td class="form_guardian_field">
											<?=kis_ui::displayTableField($kis_data['portfolio_record']['EndWeight']) ?>
											<?=$kis_lang['PortfolioData']['WeightKg'] ?>
										</td>
									</tr>
									<tr>
										<td class="field_title"><?=$kis_lang['PortfolioData']['Height'] ?></td>
										<td class="form_guardian_field">
											<?=kis_ui::displayTableField($kis_data['portfolio_record']['StartHeight']) ?>
											<?=$kis_lang['PortfolioData']['HeightCm'] ?>
										</td>
										<td class="form_guardian_field">
											<?=kis_ui::displayTableField($kis_data['portfolio_record']['EndHeight']) ?>
											<?=$kis_lang['PortfolioData']['HeightCm'] ?>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['PortfolioData']['TermDayCount'] ?></td>
						<td><?=kis_ui::displayTableField($kis_data['portfolio_record']['TotalSchoolDay']) ?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['PortfolioData']['PresentDayCount'] ?></td>
						<td><?=kis_ui::displayTableField($kis_data['portfolio_record']['PresentDay']) ?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['PortfolioData']['SickLeaveDayCount'] ?></td>
						<td><?=kis_ui::displayTableField($kis_data['portfolio_record']['SickLeaveDay']) ?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['PortfolioData']['OthersLeaveDayCount'] ?></td>
						<td><?=kis_ui::displayTableField($kis_data['portfolio_record']['OtherLeaveDay']) ?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['PortfolioData']['LateDayCount'] ?></td>
						<td><?=kis_ui::displayTableField($kis_data['portfolio_record']['LateDay']) ?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['PortfolioData']['EarlyLeaveDayCount'] ?></td>
						<td><?=kis_ui::displayTableField($kis_data['portfolio_record']['EarlyLeaveDay']) ?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['PortfolioData']['Award'] ?></td>
						<td><?=kis_ui::displayTableField($kis_data['portfolio_record']['Award']) ?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['PortfolioData']['Remarks'] ?></td>
						<td><?=kis_ui::displayTableField($kis_data['portfolio_record']['Remark']) ?></td>
					</tr>
				</tbody>
			</table>
		
<!-- -------- Display Data END -------- --> 
		<p class="spacer"></p><br />
    </div>
</div>