<?php

global $plugin;
include_once($PATH_WRT_ROOT.'includes/cust/student_data_analysis_system_kis/libSDAS.php');
include_once($PATH_WRT_ROOT.'lang/kis/apps/lang_cees_'.$intranet_session_language.'.php');
$tabArray = array('awards','activities');

if($sys_custom['iPf']['chiuchunkg']['SBS']){ // CCKG Cust
	$tabArray[] = 'portfoliodata';
}
$tabArray[] = 'schoolaward';
$tabArray[] = 'synctocees';
?>
<script>
$(function(){
	kis.iportfolio.teacher_schoolaward_edit_init({
		please_fill_in:"<?=$kis_lang['please_fill_in']?>",
		award_title: '<?=strtolower($kis_lang['award_title'])?> ',
		award_date: '<?=strtolower($kis_lang['award_date'])?> '
	});
});
</script>
<div class="main_content">
<form id='award_form'>
    <div class="main_content_detail">
        <? kis_ui::loadModuleTab($tabArray, 'schoolaward', '#apps/iportfolio/schoolrecords/') ?>
        <p class="spacer"></p>
        <?=$kis_data['NavigationBar']?>
        <p class="spacer"></p>
         <div class="table_board">
			<table class="form_table">
			    <tr>
					<td class="field_title"><?=$kis_lang['schoolyear']?></td>
					<td><?=$kis_data['select_academicYear']?></td>
				</tr>		
			    <tr>
					<td class="field_title"><?=$kis_lang['term']?></td>
					<td><span id="span_term"><?=$kis_data['select_academicYearTerm']?></span></td>
				</tr>					
			    <tr>
					<td class="field_title"><span class="tabletextrequire">*</span><?=$kis_lang['award_title']?></td>
					<td ><?=kis_iportfolio::getFormField("text","award_name",$kis_data['RetrieveList']['award_name'],'edit',' class="textboxtext"')?></td>
				</tr>
			    <tr>
					<td class="field_title"><span class="tabletextrequire">*</span><?=$kis_lang['award_date']?></td>
					<td ><?=kis_iportfolio::getFormField("text","award_date",$kis_data['RetrieveList']['award_date'],'edit')?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SDAS_AllowSync']?></td>
					<td>
						<input type='checkbox' name='SDAS_AllowSync[]' id='SDAS_AllowSync_I' value='I' <?=( in_array('I',$kis_data['RetrieveList']['SDAS_AllowSync'])?'checked':'')?> />
						<label for='SDAS_AllowSync_I'><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'].'('.$Lang['CEES']['Management']['SchoolActivityReport']['Category']['IntraSchool'].')'?></label>
						<input type='checkbox' name='SDAS_AllowSync[]' id='SDAS_AllowSync_M' value='M' <?=( in_array('M',$kis_data['RetrieveList']['SDAS_AllowSync'])?'checked':'')?>/>
						<label for='SDAS_AllowSync_M'><?=$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Title']?></label>
					</td>
				</tr>			
			    <tr>
			    	<td class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ParticipatedGroup']?></td>
					<td ><?=kis_iportfolio::getFormField("text","participatedGroup",$kis_data['RetrieveList']['participatedGroup'],'edit',' class="textboxtext"')?></td>
			    </tr>
			    <tr>
					<td class="field_title"><?=$kis_lang['award_organization']?></td>
					<td ><?=kis_iportfolio::getFormField("text","organization",$kis_data['RetrieveList']['organization'],'edit',' class="textboxtext"')?></td>
				</tr>
			    <tr>
					<td class="field_title"><?=$kis_lang['subject_area']?></td>
					<td ><?=kis_iportfolio::getFormField("text","subject_area",$kis_data['RetrieveList']['subject_area'],'edit',' class="textboxtext"')?></td>
				</tr>
			    <tr>
					<td class="field_title"><?=$kis_lang['remarks']?></td>
					<td><?=kis_iportfolio::getFormField("textarea","remarks",$kis_data['RetrieveList']['remarks'],'edit',' rows="4" wrap="virtual" class="textboxtext"')?></td>
				</tr>
				<col class="field_title" />
				<col  class="field_c" />
			</table>
			<p class="spacer"></p>
			<p class="spacer"></p><br />
        </div>
		<div class="edit_bottom">
		<?php if($kis_data['school_record_action']=='edit'){ ?>
			<input type="hidden" name="recordId" id="recordId" value="<?=$kis_data['RetrieveList']['award_id']?>">
		<?php } ?>
			
			<input type="hidden" name="school_record_action" id="school_record_action" value="<?=$kis_data['school_record_action']?>">
			<input type="submit" id="submitBtn" name="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
			<input type="button" id="cancelBtn" name="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
		</div>
    </div>
</form>
</div>			