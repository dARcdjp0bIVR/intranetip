<script type="text/javascript">
$(document).ready( function() {
	$('select#newsTypeSel').change( function() {
		goRefresh();
	});
});

function goRefresh() {
	$('form.filter_form').submit();
}
</script>
<div class="main_content">
    <p class="spacer"></p>
	<form class="filter_form">
		<div id="table_filter">
		   <?=$kis_data['news_type_selection']?>
		</div>
		<div class="search"><!--<a href="#">Advanced</a>-->
		    <input placeholder="<?=$kis_lang['search'] ?>" name="search" value="<?=$search?>" type="text"/>
		</div>
    </form>
   	<div class="table_board">
  	    <table class="common_table_list edit_table_list">
			<col  />
			<thead>
				<tr>
				  <th width="20">#</th>
				  <th width="20%"><? kis_ui::loadSortButton('AnnouncementDate', 'start_date', $kis_data['sortby'], $kis_data['order'])?></th>
				  <th><? kis_ui::loadSortButton('Title', 'title', $kis_data['sortby'], $kis_data['order'])?></th>
				  <th width="20%"><? kis_ui::loadSortButton('PostedBy', 'posted_by', $kis_data['sortby'], $kis_data['order'])?></th>
			  </tr>
			</thead>
			<tbody>
				<?php if ($kis_data['num_of_data'] == 0) { ?>
					<tr><td colspan="6" style="text-align:center;"><?=$kis_lang['norecord']?></td></tr>			
				<?php } else { ?>
					<?php for($i=0; $i<$kis_data['num_of_data']; $i++){ ?>
						<tr>
						  <td><?=($i+1)?></td>
						  <td><?=$kis_data['data_array'][$i]['AnnouncementDate']?></td>
						  <td><?=$kis_data['data_array'][$i]['NewsLink']?></td>
						  <td><?=$kis_data['data_array'][$i]['PostedBy']?></td>
						</tr>
					<?php } ?>
				<?php } ?>
			</tbody>
		</table>
        <p class="spacer"></p>
		<?php list($page,$amount,$total,$kis_data['sortby'],$kis_data['order']) = $kis_data['PageBar'];  ?>
		<? kis_ui::loadPageBar($page, $amount, $total, $kis_data['sortby'], $kis_data['order']) ?>
		<p class="spacer"></p><br />
	</div>
    
    <p class="spacer"></p>
</div>                  