<script>
$(function(){
    kis.album.album_init();
    <?if($sys_custom['KIS_Album']['DisableRightClick']){?>
    //disable the right click on the photo
    $('div.photo_thumb_list a, div.fancybox-overlay').live('contextmenu', function(e) {
	    return false;
	});
	
	//disable open in new tab of the image
	$('div.photo_thumb_list a').on('mousedown', function (e) {
        if (e.which == 1 || e.which == 2) {
            e.preventDefault();
            $(this).fancybox({
                // API options
            }).click();
        }
        return false;
    });
    
    //disable drag and drop of the image
    $('div.fancybox-inner img').live('mousedown', function (e) {
           return false;
    });
    <?}?>
});
</script>
<div class="main_content">
    
    <span class="album_title" style="display:none;"><?=$album['title']?$album['title']:'('.$kis_lang['untitledalbum'].')'?></span>
    
    <div>
    <?=$album['description']?>
    <?=(($shared_all || $shared_groups || $shared_users) && $album['until'])?'<br/>'.$kis_lang['enddateofalbum'].': '.date('Y-m-d',$album['until']):''?>  
    <? if ($album['editable']): ?>
      <div class="common_table_tool">
	  <a class="edit" href="#/apps/album/edit/?id=<?=$album['id']?>"><?=$kis_lang['edit']?></a>
      </div>
   	  <p class="spacer"></p>
    <? endif; ?>
    </div>
    
    <div class="navigation_bar">
	<span><?=$kis_lang['total']?> <?=sizeof($photos)?> <?=$kis_lang['photos']?> <em> (<?=date('Y-m-d', $album['date'])?>)</em></span>
    </div>
    <p class="spacer"></p>
  
	  
    <div class="photo_thumb_list">
	<? if ($photos): ?>
	<ul>
	    <? foreach ($photos as $photo): ?>
	    <li>
		<a title="<?=$photo['title']?>" href="<?=kis_album::getPhotoFileName('photo', $album, $photo)?>" rel="fancybox-thumb" class="fancybox-thumb"
		    style="background-image: url(<?=kis_album::getPhotoFileName('thumbnail', $album, $photo)?>)">
		</a>
		<div class="photo_thumb_detail"> 
		    <div class="description"><?=$photo['description']?$photo['description']:$photo['title']?></div>    
		</div>
		<p class="spacer"></p>
	    </li>
	    <? endforeach; ?>
	</ul>
	<? else: ?>
	<div class="no_record"><?=$kis_lang['norecord']?></div>
	<? endif; ?>

	<p class="spacer"></p>
    </div>
</div>