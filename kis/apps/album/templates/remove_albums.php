<script type="text/javascript">
kis.album.remove_albums_init({
	msg_confirm_delete_album : '<?=$kis_lang['msg']['areyousuretoremovealbum']?>',
	msg_selectatleastonerecord: '<?=$kis_lang['msg']['selectatleastonealbum']?>'	      
    });

function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

</script>
<div class="main_content">
    <form class="filter_form">
	<div id="table_filter">
	    <? if ($kis_user['type']==kis::$user_types['teacher']): ?>
	    <?=$kis_lang['groups']?>: 
	    <select name="group_id" class="auto_submit">
		
		<option value=""><?=$kis_lang['all']?></option>
		<? foreach ($groups as $group): ?>
		<option <?=$group_id==$group['group_id']?'selected':''?> value="<?=$group['group_id']?>"><?=$group['group_name_'.$lang]?></option>
		<? endforeach; ?>
	    </select>
	    <? endif; ?>
	    <? if ($can_create_album): ?>
		<input type="checkbox" id="my_album_only" name="my_album_only" <?=$my_album_only?'checked':''?> value="1"/><label for="my_album_only"><?=$kis_lang['onlyalbumscreatedbyme']?></label>
	    <? endif; ?>
	    <span class="date_time" style="padding-left:20px;"><?=$kis_lang['total']?> <?=sizeof($albums)?> <?=$kis_lang['albums']?></span>
	</div>
	<div class="search"><!--<a href="#">Advanced</a>-->
	    
	    <input placeholder="<?=$kis_lang['search'] ?>" name="search" value="<?=$search?>" type="text"/>
	</div>
    </form>
    
    <p class="spacer"></p>
    
    
	
    <? if ($albums): ?>
	<ul class="album_list">
	    <? foreach ($albums as $album): ?>
	    <li><input type="checkbox" name="album_ids[]" id="album_id_<?=$album['id']?>" value="<?=$album['id']?>"></input><a href="javascript:void(0)" onclick="$('#album_id_<?=$album['id']?>').click()">
		<span><?=$album['title']?$album['title']:'('.$kis_lang['untitledalbum'].')'?> <i>(<?=$album['photo_count']?>)</i>
		 <p class="spacer"></p>
         <span class="photo_date"><?=date('Y-m-d', $album['date'])?></span> 
         <? if (count($album['groups'])>0): ?>
         <div class="btn_access_ppl" onmouseover="MM_showHideLayers('access_ppl_list_<?=$album['id']?>','','show')" onmouseout="MM_showHideLayers('access_ppl_list_<?=$album['id']?>','','hide')">
            <span></span>
         	<div class="access_ppl_layer" id="access_ppl_list_<?=$album['id']?>" onmousedown="if (event.preventDefault) event.preventDefault()">
                <h1><?=$kis_lang['targetgroup']?></h1>
                <div class="access_ppl_list">
                <?php foreach($album['groups'] as $_group):?>
                <p><?=$_group['group_name_'.$lang]?></p>
                <? endforeach; ?>
                </div>
            </div>
         
        </div>
        <? endif; ?>
        <? if (count($album['users'])>0): ?>
         <div class="btn_access_ppl" onmouseover="MM_showHideLayers('access_ppl_list_<?=$album['id']?>','','show')" onmouseout="MM_showHideLayers('access_ppl_list_<?=$album['id']?>','','hide')">
            <?if ($kis_user['type']==kis::$user_types['teacher']){?>
            <span></span>
         	<div class="access_ppl_layer" id="access_ppl_list_<?=$album['id']?>" onmousedown="if (event.preventDefault) event.preventDefault()">
                <h1><?=$kis_lang['specificusers']?></h1>
                <div class="access_ppl_list">
                <?php foreach($album['users'] as $_user):?>
                <p><?=($_user['user_name_'.$lang]?$_user['user_name_'.$lang]:$_user['user_name_en']).' ('.($_user['user_class_name']?$_user['user_class_name'].' ':'').$kis_lang['userrecordtype_'.$_user['user_type']].')'?></p>
                <? endforeach;?>
                </div>
            </div>
        	<?}else{?>
        	<span title="<?=$kis_lang['specificusers']?>"></span>
        	<?}?> 
        </div>
        <? endif; ?>
        <? if (!$album['shared_all']&&!$album['shared_groups']&&!$album['shared_users']): ?>
		<div class="btn_access_self">
    		<span title="<?=$kis_lang['onlymyself']?>"></span>
		</div>
        <? endif; ?>
        <? if ($album['shared_all']): ?>
		<div class="btn_access_all">
    		<span title="<?=$kis_lang['allusers']?>"></span>
		</div>
        <? endif; ?>
		 </span>
		<? if ($album['first_photo_id']): ?>
		<div class="thumbnail" style="background-image: url(<?=kis_album::getPhotoFileName('thumbnail', $album, $album['cover_photo'])?>)"></div>
		<? endif; ?>
	    
	    </a></li>
	
	<? endforeach; ?>
	</ul>
    <? else: ?>
	<div class="no_record"><?=$kis_lang['norecord']?></div>
    <? endif; ?>
    
    <p class="spacer"></p>
    <div class="edit_bottom">
	<input type="button" value="<?=$kis_lang['remove']?>" class="formbutton remove">
	<input type="button" value="<?=$kis_lang['back']?>" class="formsubbutton back">
    </div>
</div>                  