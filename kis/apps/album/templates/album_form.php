<?
// Editing by Henry

/*
 *	
 */
?>
<script>
$(function(){
    kis.album.album_form_init({
	areyousureto : '<?=$kis_lang['areyousureto']?>',
	allphotoswillberemoved: '<?=$kis_lang['allphotoswillberemoved']?>!',
	stopuploadingphotos: '<?=$kis_lang['stopuploadingphotos']?>',
	removeall: '<?=$kis_lang['removeall']?>',
	photomaxsize: '<?=$sys_custom['KIS_Album']['PhotoMaxSize']?>'		      
    },{photo_count: <?=sizeof($photos)?>},{all:'<?=$kis_lang['allusers']?>',groups:'<?=$kis_lang['specificgroups']?>',myself:'<?=$kis_lang['onlymyself']?>',users:'<?=$kis_lang['specificusers']?>'});
});
</script>
<div class="main_content">
    
    <? if ($album): ?>
    <span class="album_title" style="display:none;"><?=$album['title']?$album['title']:'('.$kis_lang['untitledalbum'].')'?></span>
    <? endif; ?>
    <form class="album_form">
	
	<div class="album_form_title" ><?=$kis_lang['albumtitle']?> :</div> 
	
	<input type="text" name="title" maxlength="60" class="album_form_input" value="<?=$album['title']?>" placeholder="<?=$kis_lang['albumtitle']?>..." size="30">
	<div class="album_form_date_access" style="padding-right:5px;width:195px;"><?=$kis_lang['eventdate']?> :<input class="date" name="date" type="text" size="12" value="<?=$album['date']? date('Y-m-d',$album['date']): date('Y-m-d')?>"></div>
	<p class="spacer"></p>
	<div class="album_form_title"><?=$kis_lang['description']?> :</div> 
	
	<input type="text" name="description" value="<?=$album['description']?>" class="album_form_input" placeholder="<?=$kis_lang['description']?>..." size="50"> 
	<input type="hidden" name="album_id" value="<?=$album['id']?>"> 
	<div class="album_form_date_access">
		<span id="span_current_target"><?php
	    	if($shared_all){
	    		echo $kis_lang['allusers'];
	    	}else if($shared_groups){
	    		echo $kis_lang['specificgroups'];
	    	}else if($shared_users){
	    		echo $kis_lang['specificusers'];
	    	}else{
	    		echo $kis_lang['onlymyself'];
	    	}
	    ?></span>
	    <a class="btn_select_ppl class_select_ppl" href="#"></a>
	    <div class="album_form_date_access_select" style="<?=$sys_custom['KIS_Album']['AlbumUserSelection']?'width:300px;':''?>display:none;">
		<input <?=!$shared_all&&!$shared_groups?'checked':''?> type="radio" name="share_to" id="share_to_myself" value=""/><label for="share_to_myself"><?=$kis_lang['onlymyself']?></label><br/>
		<input <?=$shared_all?'checked':''?> type="radio" name="share_to" id="share_to_all" value="all"/><label for="share_to_all"><?=$kis_lang['allusers']?></label><br/>
		<input <?=$shared_groups?'checked':''?> type="radio" name="share_to" id="share_to_groups" value="groups"/><label for="share_to_groups"><?=$kis_lang['specificgroups']?></label><br/>
		<?if($sys_custom['KIS_Album']['AlbumUserSelection']){?>
		<input <?=$shared_users?'checked':''?> type="radio" name="share_to" id="share_to_users" value="users"/><label for="share_to_users"><?=$kis_lang['specificusers']?></label><br/>
		<?}?>
		<select multiple="multiple" size="8" name="select_groups[]"  class="select_groups" <?=$shared_groups?'style="width:100%;"':'style="width:100%;display:none"'?>>
		    <? foreach ($groups as $group): ?>
		    
		    <option <?=in_array($group['group_id'], $shared_groups)?'selected':''?> value="<?=$group['group_id']?>"><?=$group['group_name_'.$lang]?></option>
		    <? endforeach; ?>
		</select>
		
	    <?if($sys_custom['KIS_Album']['AlbumUserSelection']){?>
	    <div class="select_user_div" <?=$shared_users?'':'style="display:none"'?>><div style="width:100%;height: 250px;" id='form_div' class='mail_to_list'>
	    
	    <?
	    ob_start();
	
	    kis_ui::loadTemplate('select_handler',array('users'=>$users, 'form_name'=>'select_users[]'));
	    
	    echo ob_get_clean();
	    
	    ob_end_clean;
	    
	    ?>
	    </div>
	    <p class="spacer"></p> 
	    <div class="mail_to_btn" style="float:left;"><div class="mail_icon_form">
		<a href="#" class="btn_select_ppl"><?=$kis_lang['select']?>
		    <span class="target" style="display:none">select_users</span>
		</a><a href="#" class="btn_remove" style="left: 25px;"><?=$kis_lang['removeall']?></a>
	    </div></div></div>
	    
	    <p class="spacer"></p> 
	    <?}?>
		<div class="select_date" <?=$shared_users||$shared_groups||$shared_all?'':'style="display:none"'?>><?=$kis_lang['since']?> <input name="since" type="text" class="date" value="<?=$album['since']?date('Y-m-d',$album['since']):''?>"/></div>
		<p class="spacer"></p> 
		<div class="select_date" <?=$shared_users||$shared_groups||$shared_all?'':'style="display:none"'?>><?=$kis_lang['until']?> <input name="until" type="text" class="date" value="<?=$album['until']?date('Y-m-d',$album['until']):''?>"/></div>
	    </div>
	</div>
    </form>
   
                        
    <p class="spacer"></p> 
		      
    <div class="Content_tool">
	<a class="new" id="add_photos" href="#"><?=$kis_lang['addphotos']?></a>
    </div>
    <div class="filter_ordering">
		       
	<a href="#title"><?=$kis_lang['photoname']?></a><a class="int" href="#date_uploaded"><?=$kis_lang['uploaddate']?></a><a class="int" href="#date_taken"><?=$kis_lang['datetaken']?></a><span><?=$kis_lang['reorderphotos']?> :</span>
    </div>
    <p class="spacer"></p>
    <div class="edit_photo_list" id="attach_file_area">
	<ul>
	    <? foreach ((array)$photos as $photo): ?>
	    <li class="uploaded">
		<span title="<?=$photo['title']?>" style="background-image:url(<?=kis_album::getPhotoFileName('thumbnail', $album, $photo)?>)"/></span>
		<p class="spacer"></p>
	    
		<textarea maxlength="255" placeholder="<?=$kis_lang['descriptionhere']?>..." name="descriptions[]" class="input_desc" wrap="virtual" rows="2"><?=$photo['description']?></textarea>
		<input type="hidden" name="photo_ids[]" class="photo_ids" value="<?=$photo['id']?>"/>
		<input type="hidden" class="date_taken" value="<?=$photo['date_taken']?>"/>
		<input type="hidden" class="date_uploaded" value="<?=$photo['date_uploaded']?>"/>
		<input type="hidden" class="title" value="<?=$photo['title']?>"/>
		<p class="spacer"></p>
		<div class="table_row_tool">

		    <a href="#" class="copy_dim" title="<?=$kis_lang['setascoverphoto']?>"></a> 
		    <a href="#" class="delete_dim" title="<?=$kis_lang['removephoto']?>"></a>
		</div>
	    </li>
	    <? endforeach; ?>
	   
	</ul>
	
	<p class="spacer"></p>
    </div>				
    		  
    <div class="edit_bottom">
	<input type="button" value="<?=$kis_lang['finish']?>" class="formbutton">
	<a class="album_form_remove" <?=$album?'':'style="display:none"'?> href="#"><?=$kis_lang['removealbum']?></a>
    </div>
    
    <p class="spacer"></p>
                      	
</div>
<ul id="edit_photo_list_template" style="display:none">
    <li style="display:none">
	
	<span></span>
	<p class="spacer"></p>
	<textarea disabled maxlength="255" placeholder="<?=$kis_lang['descriptionhere']?>..." name="descriptions[]" class="input_desc" wrap="virtual" rows="2"></textarea>
	<input type="hidden" name="photo_ids[]" class="photo_ids" value=""/>
	<input type="hidden" class="date_taken" value=""/>
	<input type="hidden" class="date_uploaded" value=""/>
	<input type="hidden" class="title" value=""/>
	<p class="spacer"></p>
	<div class="table_row_tool" style="display:none">
	    <a href="#" class="copy_dim" title="<?=$kis_lang['setascoverphoto']?>"></a><a href="#" class="delete_dim" title="<?=$kis_lang['removephoto']?>"></a>
	</div>
    </li>
</ul>

<?if($sys_custom['KIS_Album']['AlbumUserSelection']){?>
<form class='mail_select_user' style='z-index:7071'>
    <h2><?=$kis_lang['findusers']?></h2>
    <?=$kis_lang['keyword']?>: <input type="text" name="keyword" style="float:right" value=""/>
    <p class="spacer"></p>
    <?=$kis_lang['group']?>: <select name="user_group" style="max-width: 21em;float:right" >
	<option value=""><?=$kis_lang['all']?> <?=$kis_lang['groups']?></option>
	<? foreach ($groups as $group): ?>
	<option value="<?=$group['group_id']?>"><?=$group['group_name_'.$lang]?></option>
	<? endforeach; ?>
    </select>
    <p class="spacer"></p>
    <?=$kis_lang['identity']?>:
    <label for="user_type_4" style="padding-right:0px;"><?=$kis_lang['parent']?></label><input type="radio" id="user_type_4" name="user_type" value='4' />
    <label for="user_type_3" style="padding-right:0px;"><?=$kis_lang['staff']?></label><input type="radio" id="user_type_3" name="user_type" value='3' />
    <label for="user_type_2" style="padding-right:0px;"><?=$kis_lang['student']?></label><input type="radio" id="user_type_2" name="user_type" value='2' />
    <label for="user_type_1" style="padding-right:0px;"><?=$kis_lang['teacher']?></label><input type="radio" id="user_type_1" name="user_type" checked value='1' />
    <input type="hidden" name="target" value=""/>
    <input type="hidden" name="exclude_list" value=""/>
    <p class="spacer"></p>
    <div class="button">  
	<input class="formbutton" value="<?=$kis_lang['search']?>" type="submit"/>
	<input class="formsubbutton" value="<?=$kis_lang['close']?>" type="submit"/>
    </div>
    
    <a class="mail_select_all" href="#"><?=$kis_lang['addall']?></a>
    <p class="spacer"></p>
    <div class="search_results">
    </div>
</form>
<?}?>