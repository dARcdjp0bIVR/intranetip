<?php
// Editing by 
/*
 * 2020-03-05 (Bill): hide balance display if enabled payment system    [2020-0227-0929-45235]
 *
 * 2014-03-20 (Henry): only show paymentrecords for TBCPK and display $0 if balance is 0
 * 
 * 2013-10-30 (Carlos): Hide Balance info and add value records
 */

if ($sys_custom['ePayment']['KIS_OnlyShowPaymentRecords']){
	$moduleTabAry = array('paymentrecords');
}
else if($sys_custom['ePayment']['KIS_NoBalance']){
 	$moduleTabAry = array('transactionrecords', 'paymentrecords');
}
else{
	$moduleTabAry = array('transactionrecords', 'paymentrecords', 'addvaluerecords');
}
?>

<script>
$(function(){
    <? if (!$unpaid_count): ?>
    $('#module_page .module_tab span.tab_paymentrecords').append('<em class="tabletextrequire"> (<?=$unpaid_count?>)</em>');
    <? endif; ?>
});
</script>

<?php if(!$sys_custom['ePayment']['KIS_NoBalance'] && !($sys_custom['ePayment']['Alipay'] || $sys_custom['ePayment']['TNG'] || $sys_custom['ePayment']['TAPANDGO'] || $sys_custom['ePayment']['FPS'])){ ?>
<div class="epayment_balance">
    <em>$ <?=$account_stat['balance']?$account_stat['balance']:'0'?></em>
    <div class="balance_title"><h1> <?=$kis_lang['accountbalance']?> : </h1><span class="date_time">(<?=$kis_lang['lastupdated']?>: <?=$account_stat['updated']?$account_stat['updated']:' -- '?>)</span></div>	
    <p class="spacer"></p>
</div>
<?php } ?>
<div class="main_content">

<? kis_ui::loadModuleTab($moduleTabAry, $q[0], "#/apps/epayment/");?>
<? kis_ui::loadTemplate($kis_data['main_template'], $kis_data); ?>
</div>