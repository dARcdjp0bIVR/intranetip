<?php

//debug_r($applicationInfo);

?>
<table class="form_table">

<tr>
	<td>&nbsp;</td>
	<td class="form_guardian_head">
		<center><?=$kis_lang['Admission']['PG_Type']['F']?></center>
	</td>
	<td class="form_guardian_head">
		<center><?=$kis_lang['Admission']['PG_Type']['M']?></center>
	</td>
</tr>
<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['englishname']?>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['F']['parent_name_en'])?></center>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['M']['parent_name_en'])?></center>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['chinesename']?>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['F']['parent_name_b5'])?></center>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['M']['parent_name_b5'])?></center>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['munsang']['levelofeducation']?>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['F']['levelofeducation'])?></center>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['M']['levelofeducation'])?></center>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['occupation']?>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['F']['occupation'])?></center>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['M']['occupation'])?></center>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['RMKG']['company']?>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['F']['companyaddress'])?></center>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['M']['companyaddress'])?></center>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['CHIUCHUNKG']['contactnumber']?>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['F']['mobile'])?></center>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['M']['mobile'])?></center>
	</td>
</tr>

</table>



<table class="form_table">

<tr>
	<td>&nbsp;</td>
	<td class="form_guardian_head">
		<center><?=kis_ui::displayTableField($kis_lang['Admission']['PG_Type']['G'])?></center>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['name']?>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['G']['parent_name_b5'])?></center>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['CHIUCHUNKG']['contactnumber']?>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['G']['mobile'])?></center>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['CHIUCHUNKG']['relationship']?>
	</td>
	<td class="form_guardian_field">
		<center><?=kis_ui::displayTableField($applicationInfo['G']['relationship'])?></center>
	</td>
</tr>

</table>
