<?php

?>
<table class="form_table">
	<tbody>
		<tr>
			<td width="30%" class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['applicationstatus']?></td>
			<td width="70%"><?=$statusSelection?></td>
		</tr>
		<?if($sys_custom['KIS_Admission']['TBCPK']['Settings'] || $sys_custom['KIS_Admission']['CSM']['Settings'] || $sys_custom['KIS_Admission']['MGF']['Settings'] || $sys_custom['KIS_Admission']['MUNSANG']['Settings'] || $sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings'] ||  $sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings'] || $sys_custom['KIS_Admission']['RMKG']['Settings']||$sys_custom['KIS_Admission']['KTLMSKG']['Settings']){?>
			<tr>
				<td class="field_title"><?=$kis_lang['Admission']['applicationfee']?></td>
				<td>
					<table style="width:auto;">
						<?if($paypalPaymentInfo[0]['payment_status']){?>
						<tr>
							<td colspan="4"><font color="green"><?=$kis_lang['Admission']['KTLMSKG']['paidByPayPal']?></font><br/>
							<?foreach($paypalPaymentInfo as $apaypalPaymentInfo){?>
							(<?=$kis_lang['Admission']['KTLMSKG']['PaymentItem']?>) <?=$apaypalPaymentInfo['item_name']?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PaymentTotal']?>) <?=$apaypalPaymentInfo['mc_currency']?> <?=$apaypalPaymentInfo['mc_gross']?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PaymentDate']?>) <?=date('Y-m-d H:i:s', strtotime($apaypalPaymentInfo['payment_date']))?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PayerEmail']?>) <?=$apaypalPaymentInfo['payer_email']?><br/><br/>
							<?}?>
							</td>
						</tr>
						<tr>
							<td colspan="4">&nbsp;</td>
						</tr>
						<?}?>
						<tr>
							<td>(<?=$kis_lang['Admission']['KTLMSKG']['bankName']?>)</td>
							<td><?=$libinterface->GET_TEXTBOX('FeeBankName', 'FeeBankName', $applicationInfo['FeeBankName'], $OtherClass='', $OtherPar=array())?></td>
							<td>(<?=$kis_lang['Admission']['KTLMSKG']['chequeNum']?>)</td>
							<td><?=$libinterface->GET_TEXTBOX('FeeChequeNo', 'FeeChequeNo', $applicationInfo['FeeChequeNo'], $OtherClass='', $OtherPar=array())?></td>
						</tr>
						<tr>
							<td>(<?=$kis_lang['Admission']['receiptcode']?>)</td>
							<td colspan="3"><?=$libinterface->GET_TEXTBOX('receiptID', 'receiptID', $applicationInfo['receiptID'], $OtherClass='', $OtherPar=array())?></td>
						</tr>
						<tr class="receipt_info" <?=empty($applicationInfo['receiptID'])?' style="display:none;"':''?>>
							<td>(<?=$kis_lang['date']?>)</td>
							<td><input type="text" name="receiptdate" id="receiptdate" value="<?=$applicationInfo['receiptdate']?>">&nbsp;<span class="error_msg" id="warning_receiptdate"></span></td>
						</tr>
						<tr class="receipt_info" <?=empty($applicationInfo['receiptID'])?' style="display:none;"':''?>>
							<td>(<?=$kis_lang['Admission']['handler']?>)</td>
							<td>
								<span class="mail_to_list" id="span_handler" style="height:auto;width:auto;border:0;">
									<span class="mail_user">
										<?if(!empty($applicationInfo['handler'])):?>
											<div class="mail_user_name">
												<?=$applicationInfo['handler']?>
											</div>
											<div class="mail_icon_form">
												<a href="#" class="btn_remove" id="remove_<?=$applicationInfo['handler_id']?>"></a>
												<input type="hidden" name="handler" id="handler" value="<?=$applicationInfo['handler_id']?>">
											</div>
										<?else:?>
											--
											<input type="hidden" name="handler" id="handler">
										<?endif;?>
									</span>
								</span>
								<div class="mail_icon_form">
									<a href="#" class="btn_select_ppl">
										<?=$kis_lang['select']?>
									</a>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<?
					$_filePath = $attachmentList['receipt_copy']['link'];
					if($_filePath){
						$_spanDisplay = '';
						$_buttonDisplay = ' style="display:none;"';
					}else{
						$_filePath = '';
						$_spanDisplay = ' style="display:none;"';
						$_buttonDisplay = '';
					}
					
					$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
					$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';	
				?>                         
				<td class="field_title"><?=$kis_lang['receiptcopy']?></td>
				<td id="receipt_copy">
					<span class="view_attachment" <?=$_spanDisplay?>><?=$_attachment?></span>
					<input type="button" class="attachment_upload_btn formsmallbutton" value="<?=$kis_lang['Upload']?>"  id="uploader-receipt_copy" <?=$_buttonDisplay?>/>
				</td>
			</tr>
			<? list($interviewdate,$interviewtime) = explode(" ",$applicationInfo['interviewdate']); ?>
			<?if(!$sys_custom['KIS_Admission']['InterviewSettings']){?>
				<tr>
					<td class="field_title"><?=$kis_lang['Admission']['interviewdate']?></td>
					<td><input type="text" name="interviewdate" id="interviewdate" value="<?=(is_date_empty($interviewdate)?'':$interviewdate)?>">&nbsp;<span class="error_msg" id="warning_interviewdate"></span><?=$libadmission->Get_Time_Selection($libinterface,'interview',$interviewtime)?></td>
				</tr>
				<tr>
					<td class="field_title"><?=$kis_lang['Admission']['interviewlocation']?></td>
					<td><input type="text" name="interviewlocation" id="interviewlocation" value="<?=$applicationInfo['interviewlocation']?>" /></td>
				</tr>
			<?}?>
			<?if($sys_custom['KIS_Admission']['InterviewSettings']){?>
				<tr>
					<td class="field_title"><?=$kis_lang['Admission']['interviewdate']?></td>
					<td>
						(1) <?=$interviewSettingSelection?><br/>
						(2) <?=$interviewSettingSelection2?><br/>
						(3) <?=$interviewSettingSelection3?>
					</td>
				</tr>
			<?}?>  
			<tr>
				<td class="field_title"><?=$kis_lang['Admission']['isnotified']?></td>
				<td><?=$libinterface->Get_Checkbox('isnotified', 'isnotified', 1, ($applicationInfo['isnotified']=='yes'), $Class='', $Display='', $Onclick='', $Disabled='')?></td>
			</tr>
		<?}?>
		<?if($sys_custom['KIS_Admission']['ICMS']['Settings']){
			list($interviewdate,$interviewtime) = explode(" ",$applicationInfo['interviewdate']);
		?>
			<tr>
				<td class="field_title"><?=$kis_lang['Admission']['interviewdate']?></td>
				<td><input type="text" name="interviewdate" id="interviewdate" value="<?=(is_date_empty($interviewdate)?'':$interviewdate)?>">&nbsp;<span class="error_msg" id="warning_interviewdate"></span><?=$libadmission->Get_Time_Selection($libinterface,'interview',$interviewtime)?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$kis_lang['Admission']['interviewlocation']?></td>
				<td><input type="text" name="interviewlocation" id="interviewlocation" value="<?=$applicationInfo['interviewlocation']?>" /></td>
			</tr>
		<?}?>    
		<tr>
			<td class="field_title"><?=$kis_lang['Admission']['otherremarks']?></td>
			<td><textarea name="remark" id="remark" cols="75" rows="5"><?=$applicationInfo['remark']?></textarea></td>
		</tr>
	</tbody>
</table>
</table>