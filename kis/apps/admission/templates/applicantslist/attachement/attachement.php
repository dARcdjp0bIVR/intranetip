<?php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT.'kis/init.php');
include_once($PATH_WRT_ROOT."kis/config.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");

$libkis_admission = $libkis->loadApp('admission');

$sql = "Select r.ApplicationID, r.AttachmentType, CONCAT(o.RecordID,'/',IF(r.AttachmentType = 'personal_photo',r.AttachmentType,'other_files'),'/',r.AttachmentName), o.ApplyYear schoolYearID, o.RecordID RecordID
			From ADMISSION_ATTACHMENT_RECORD r
			INNER JOIN
				ADMISSION_OTHERS_INFO o
			ON r.ApplicationID = o.ApplicationID
			where o.RecordID = '{$_GET['recordID']}' and r.AttachmentType = '{$_GET['attachmentType']}'
			ORDER BY r.DateInput desc
			";

$attachmentAry = current($libkis->returnArray($sql));


$image_exts = array("gif","jpg","jpe","jpeg","png","bmp");
if(isset($attachmentAry[2])){
	$filetype = explode('.',$attachmentAry[2]);
	$filetype = $filetype[1];

	$isImage = in_array(strtolower($filetype), $image_exts);
	$attachmentPath = $admission_cfg['FilePath'].$attachmentAry[2];
	
	if(isset($filetype) && $filetype !='' && !$isImage){
		header('Location: '.$attachmentPath);
	}else if(isset($filetype) && $filetype !='' && $isImage){
		echo '<a href="'.$attachmentPath.'" target="_blank"><img src="'.$attachmentPath.'" style="max-width: 100%;" /></a>';
	}else{
		echo $kis_lang['noattachement'];
	}
}else{
	echo $kis_lang['noattachement'];
}

?>