<?php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT.'kis/init.php');
include_once($PATH_WRT_ROOT."kis/config.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");

$libinterface = new interface_html();
$libkis_admission = $libkis->loadApp('admission');

$FilePath = $admission_cfg['FilePath'];

$attachmentSettingAry = $libkis_admission->getAttachmentSettings();

$attachmentSelectAry[] = array('', $kis_lang['allphotoattachments']);
$attachmentSelectAry[] = array('personal_photo', $kis_lang['Admission']['personalPhoto']);
foreach($attachmentSettingAry as $aAttachmentSettiing){
	$attachmentSelectAry[] = array($aAttachmentSettiing['AttachmentName'], $aAttachmentSettiing['AttachmentName']);
}

$data = array(
		'classLevelID'=>$classLevelId,
		'status'=>$status
);
list($total,$tempApplicationDetails) = $libkis_admission->getApplicationDetails($schoolYearID,$data);

$count = count($tempApplicationDetails);
$applicationDetails = array();
for($i=0;$i<$count;$i++){
	$_applicationId = (is_numeric($tempApplicationDetails[$i]['application_id'])?$tempApplicationDetails[$i]['application_id'].' ':$tempApplicationDetails[$i]['application_id']);
		
	$applicationDetails[$_applicationId]['student_name'] = $tempApplicationDetails[$i]['student_name'];
	$applicationDetails[$_applicationId]['parent_name'][] = $tempApplicationDetails[$i]['parent_name'];
	$applicationDetails[$_applicationId]['parent_phone'][] = $tempApplicationDetails[$i]['parent_phone'];
	$applicationDetails[$_applicationId]['application_status'] = $tempApplicationDetails[$i]['application_status'];
	$applicationDetails[$_applicationId]['record_id'] = $tempApplicationDetails[$i]['record_id'];
}
foreach($applicationDetails as $key=>$applicant){
	$applicantSelectAry[] = array($applicant['record_id'], $key.' ('.$applicant['student_name'].')');
}

?>
<script type="text/javascript" src="/templates/jquery/jquery-1.8.3.min.js"></script>
<script language="Javascript">
function changeFile(obj) {
	var attachmentType = obj.options[obj.selectedIndex].value;

	if(attachmentType){
		parent.display_pad.location = 'attachement.php?recordID='+$('#recordID').val()+'&attachmentType='+attachmentType;
	}else{
		parent.display_pad.location = 'all_image_attachement.php?recordID='+$('#recordID').val();
	}
}

function changeStudent() {
	var attachmentType = $('#attachement').val();

	parent.function_pad.location = 'studentInfo.php?schoolYearID=<?=$schoolYearID?>&recordID='+$('#recordID').val();

	if(attachmentType){
		parent.display_pad.location = 'attachement.php?recordID='+$('#recordID').val()+'&attachmentType='+attachmentType;
	}else{
		parent.display_pad.location = 'all_image_attachement.php?recordID='+$('#recordID').val();
	}
}

$( document ).ready(function() {
	changeStudent();

	$('#prevStudent').click(function(){
		if ( $("#recordID > option:selected").is( ":first-child" ) ){
			return;
		}
		$("#recordID > option:selected")
            .prop("selected", false)
            .prev()
            .prop("selected", true);
		changeStudent();
	});

	$('#nextStudent').click(function(){
		if ( $("#recordID > option:selected").is( ":last-child" ) ){
			return;
		}
		$("#recordID > option:selected")
            .prop("selected", false)
            .next()
            .prop("selected", true);
		changeStudent();
	});
});
</script>
<link type="text/css" rel="stylesheet" media="screen" href="/templates/kis/css/common.css">
<body style="background: none !important; min-width:0;">
<div>
	<div style="float:left;padding: 5;"><?=$kis_lang['attachments'] ?>: <?=$libinterface->GET_SELECTION_BOX($attachmentSelectAry, "name='type' id='attachement' class='' onchange='changeFile(this)'",'')?></div>
	<div style="float:right;padding: 5;">
    	<span class="page_no" style="margin-top:0;">
    		<a href="#" id="prevStudent" class="prev">&nbsp;</a>
        	<?=$kis_lang['student'] ?>: 
    		<?=$libinterface->GET_SELECTION_BOX($applicantSelectAry, "name='student' id='recordID' class='' onchange='changeStudent()'",'')?>
    		<a href="#" id="nextStudent" class="next" style="float: right;">&nbsp;</a>
		</span>
	</div>
</div>
</body>
