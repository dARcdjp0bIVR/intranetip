<?php
// Editing by 
/*
 * 2020-06-30 (Tommy): modified $sys_custom['KIS_Admission']['MINGWAI']['Settings'] briefingsession selection
 * 2017-08-31 (Pun): Modified cust for mosgraceful
 * 2016-08-25 (Ronald): Added $sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] customization
 * 2016-08-25 (Ronald): Added $sys_custom['KIS_Admission']['MINGWAI']['Settings'] customization
 * 2015-11-25 (Henry):  support attachment settings for tbcpk
 * 2015-09-22 (Henry):  modified cust for munsang
 * 2015-07-28 (Pun):	moved js application_details_edit_init() parameter to a global variable
 * 2015-07-27 (Pun):	added dynamic template, see ./ktlmskg.eclass.hk/ folder
 * 2015-07-20 (Henry):	added cust for tsuenwanbckg.eclass.hk
 * 2015-07-16 (Omas):	modified cust for $sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']
 * 2015-07-03 (Henry):	added cust for chiuchunkg.edu.hk
 * 2014-10-22 (Henry):	added cust for eclassk.munsang.edu.hk
 * 2014-01-15 (Carlos): Modified [studentinfo] other attachments html
 */

global $setting_path_ip_rel;
?>
<script type="text/javascript">
$(function(){
	lang = {
		invaliddateformat:'<?=$kis_lang['Admission']['msg']['invaliddateformat']?>',
		enterenglishname:'<?=$kis_lang['Admission']['msg']['enterenglishname']?>',
		enterchinesename:'<?=$kis_lang['Admission']['msg']['enterchinesename']?>',
		selectgender:'<?=$kis_lang['Admission']['msg']['selectgender']?>',
		enterdateofbirth:'<?=$kis_lang['Admission']['msg']['enterdateofbirth']?>',
		enterbirthcertificatenumber:'<?=$kis_lang['Admission']['msg']['enterbirthcertificatenumber']?>',
		enterplaceofbirth:'<?=$kis_lang['Admission']['msg']['enterplaceofbirth']?>',
		enternativeplace:'<?=$kis_lang['Admission']['msg']['enternativeplace']?>',
		enteroccupation:'<?=$kis_lang['Admission']['msg']['enteroccupation']?>',
		enteratleastonephoneno:'<?=$kis_lang['Admission']['msg']['enteratleastonephoneno']?>',
		invalidofficephoneformat:'<?=$kis_lang['Admission']['msg']['invalidofficephoneformat']?>',
		invalidmobilephoneformat:'<?=$kis_lang['Admission']['msg']['invalidmobilephoneformat']?>',
		enteratleastoneparent:'<?=$kis_lang['Admission']['msg']['enteratleastoneparent']?>',
		parenttype:'<?=implode(",",$pgType)?>',
		timeslot:'<?=implode(",",$kis_lang['Admission']['TimeSlot'])?>',
		timeslotselected:'<?=$kis_lang['Admission']['msg']['timeslotselected']?>',
		enterneedschoolbus: '<?=$kis_lang['Admission']['msg']['enterneedschoolbus']?>',
		enterplacefortakingschoolbus: '<?=$kis_lang['Admission']['msg']['enterplacefortakingschoolbus']?>',
		selectknowusby: '<?=$kis_lang['Admission']['msg']['selectknowusby']?>',
		enterknowusby: '<?=$kis_lang['Admission']['msg']['enterknowusby']?>',
		deletecurrentpersonalphoto: '<?=$kis_lang['Admission']['msg']['deletecurrentpersonalphoto']?>',
		invalidhomephoneformat: '<?=$kis_lang['Admission']['msg']['invalidhomephoneformat']?>',
		enterapplyTerm: '<?=$kis_lang['Admission']['msg']['enterapplyTerm']?>',
		enterreceiptdate: '<?=$kis_lang['Admission']['msg']['enterreceiptdate']?>',
		enterreceipthandler: '<?=$kis_lang['Admission']['msg']['enterreceipthandler']?>',
		are_you_sure_to_remove_birth_cert: '<?=$kis_lang['Admission']['msg']['are_you_sure_to_remove_birth_cert']?>',
		are_you_sure_to_remove_immunisation_record: '<?=$kis_lang['Admission']['msg']['are_you_sure_to_remove_immunisation_record']?>',
		are_you_sure_to_remove_attachment: '<?=$kis_lang['msg']['are_you_sure_to_remove_attachment']?>',
		upload: '<?=$kis_lang['Upload']?>',
		enterstudenthomephoneno: '<?=$kis_lang['Admission']['msg']['enterstudenthomephoneno']?>',
		enterstudentcontactperson: '<?=$kis_lang['Admission']['msg']['enterstudentcontactperson']?>',
		enterstudentrelationship: '<?=$kis_lang['Admission']['msg']['enterstudentrelationship']?>',
		enterhomeaddress: '<?=$kis_lang['Admission']['msg']['enterhomeaddress']?>',
		enterstudentemail: '<?=$kis_lang['Admission']['msg']['enterstudentemail']?>',
		enterschoolcurrentlyattending_icms: '<?=$kis_lang['Admission']['icms']['msg']['enterschoolcurrentlyattending']?>',
		acknowledgement_icms: '<?=$kis_lang['Admission']['icms']['msg']['acknowledgement']?>',
		entermobileno_icms: '<?=$kis_lang['Admission']['icms']['msg']['entermobileno']?>',
		entercompanyno_icms: '<?=$kis_lang['Admission']['icms']['msg']['entercompanyno']?>',
		entercompanyaddress_icms: '<?=$kis_lang['Admission']['icms']['msg']['entercompanyaddress']?>',
		enternativelanguage_icms: '<?=$kis_lang['Admission']['icms']['msg']['enternativelanguage']?>',
		enterhometelephonenumber_icms: '<?=$kis_lang['Admission']['icms']['msg']['enterhometelephonenumber']?>',
		entermailaddress: '<?=$kis_lang['Admission']['icms']['msg']['entermailaddress']?>',
		CurBSName_icms: '<?=$kis_lang['Admission']['icms']['msg']['CurBSName']?>',
		enterlangspokenathome_icms: '<?=$kis_lang['Admission']['icms']['msg']['enterlangspokenathome']?>',
		enternationality_icms: '<?=$kis_lang['Admission']['icms']['msg']['enternationality']?>',
		applyDayTypeHints_icms: '<?=$kis_lang['Admission']['icms']['msg']['applyDayTypeHints']?>',
		childhealth_icms: "<?=$kis_lang['Admission']['icms']['childhealth']?>",
		studentssurname_b5_csm: "<?=$kis_lang['Admission']['csm']['msg']['studentssurname_b5']?>",
		studentsfirstname_b5_csm: "<?=$kis_lang['Admission']['csm']['msg']['studentsfirstname_b5']?>",
		studentssurname_en_csm: "<?=$kis_lang['Admission']['csm']['msg']['studentssurname_en']?>",
		studentsfirstname_en_csm: "<?=$kis_lang['Admission']['csm']['msg']['studentsfirstname_en']?>",
		StudentLastSchool_csm: "<?=$kis_lang['Admission']['csm']['msg']['StudentLastSchool']?>",
		entercurbsname_csm: "<?=$kis_lang['Admission']['csm']['msg']['CurBSName']?>",
		enterbirthcertificatenumber_csm: "<?=$kis_lang['Admission']['csm']['msg']['enterbirthcertificatenumber']?>",
		duplicatebirthcertificatenumber_csm: "<?=$kis_lang['Admission']['csm']['msg']['duplicatebirthcertificatenumber']?>",
		enterstudenthomephoneno_csm: "<?=$kis_lang['Admission']['csm']['msg']['enterstudenthomephoneno']?>",
		enterhomeaddress_csm: "<?=$kis_lang['Admission']['csm']['msg']['enterhomeaddress']?>",
		MaritalStatus_csm: "<?=$kis_lang['Admission']['csm']['msg']['MaritalStatus']?>",
		StudentRelationship_csm: "<?=$kis_lang['Admission']['csm']['msg']['StudentRelationship']?>",
		G1EnglishName_csm: "<?=$kis_lang['Admission']['csm']['msg']['G1EnglishName']?>",
		G1ChineseName_csm: "<?=$kis_lang['Admission']['csm']['msg']['G1ChineseName']?>",
		G1HKID_csm: "<?=$kis_lang['Admission']['csm']['msg']['G1HKID']?>",
		G1Email_csm: "<?=$kis_lang['Admission']['csm']['msg']['G1Email']?>",
		G1Mobile_csm: "<?=$kis_lang['Admission']['csm']['msg']['G1Mobile']?>",
		parentlivewithchild_csm: "<?=$kis_lang['Admission']['csm']['msg']['parentlivewithchild']?>",
		singleparent_csm: "<?=$kis_lang['Admission']['csm']['msg']['singleparent']?>",
		isFullTime_csm: "<?=$kis_lang['Admission']['csm']['msg']['isFullTime']?>",
		enterbirthcertificatenumber_MUNSANG: "<?=$kis_lang['Admission']['munsang']['msg']['enterbirthcertno']?>",
		enterstudenthomephoneno_MUNSANG: "<?=$kis_lang['Admission']['munsang']['msg']['enterphone']?>",
		enterhomeaddress_MUNSANG: "<?=$kis_lang['Admission']['munsang']['msg']['enteraddress']?>"
		<?=$sys_custom['KIS_Admission']['CSM']['Settings']?',CSM_Settings:"1"':''?>
		<?=$sys_custom['KIS_Admission']['MGF']['Settings']?',MGF_Settings:"1"':''?>
		<?=$sys_custom['KIS_Admission']['MUNSANG']['Settings']?',MUNSANG_Settings:"1"':''?>
		<?=$sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']?',CHIUCHUNKG_Settings:"1"':''?>
		<?=$sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings']?',TSUENWANBCKG_Settings:"1"':''?>
		<?=$sys_custom['KIS_Admission']['RMKG']['Settings']?',RMKG_Settings:"1"':''?>
		<?=$sys_custom['KIS_Admission']['KTLMSKG']['Settings']?',KTLMSKG_Settings:"1"':''?>
		<?=$sys_custom['KIS_Admission']['YLSYK']['Settings']?',YLSYK_Settings:"1"':''?>
		<?=$sys_custom['KIS_Admission']['MINGWAIPE']['Settings']?',MINGWAIPE_Settings:"1"':''?>
		<?=$sys_custom['KIS_Admission']['MINGWAI']['Settings']?',MINGWAI_Settings:"1"':''?>
	};
	kis.admission.application_details_edit_init(lang);
});

<?if($sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']){?>
	function showOtherTextField(id,value){
		switch(id){
			case 'placeofbirth':
				if(value == 4){
					$('span#POB_Other').show();
					$('input#FormStudentInfo_POB_Other').removeAttr('disabled');
				}
				else{
					$('span#POB_Other').hide();
					$('input#FormStudentInfo_POB_Other').attr('disabled',true);
				}
			break;
			case 'religion':
				if(value == 3){
					$('span#Religion_Other').show();
					$('input#FormStudentInfo_Religion_Other').removeAttr('disabled');
				}
				else{
					$('span#Religion_Other').hide();
					$('input#FormStudentInfo_Religion_Other').attr('disabled',true);
				}
			break;
			case 'FormStudentInfo_StuIDType':
			if(value == 3){
					$('span#StuID_Other').show();
					$('input#FormStudentInfo_StuIDType_Other').removeAttr('disabled');
				}
				else{
					$('span#StuID_Other').hide();
					$('input#FormStudentInfo_StuIDType_Other').attr('disabled',true);
				}
			break;
		}
	}
<?}?>
</script>
<div class="main_content_detail">
<form id="applicant_form" enctype="multipart/form-data">
    <? if($isValidRecord): ?>
    	<?=$kis_data['NavigationBar']?>
		<p class="spacer"></p><br>
		<!--tab-->
	<?
	if($sys_custom['KIS_Admission']['CSM']['Settings']){
		kis_ui::loadModuleTab(array('studentinfo','parentinfo','remarks'), $display, '#/apps/admission/applicantslist/details/'.$schoolYearID.'/'.$recordID.'/',2 );
	}else if($sys_custom['KIS_Admission']['ICMS']['Settings']){
		kis_ui::loadModuleTab(array('icmssectiona','icmssectionb','icmssectionc','icmssectiondef','icmsattachment','remarks'), $display, '#/apps/admission/applicantslist/details/'.$schoolYearID.'/'.$recordID.'/',5 );
	}else if(method_exists($lauc, 'getModuleTab')){
	    $moduleTab = $lauc->getModuleTab();
	    $defaultTabIndex = $lauc->getModuleTabDefaultIndex();
	    kis_ui::loadModuleTab($moduleTab, $display, '#/apps/admission/applicantslist/details/'.$schoolYearID.'/'.$recordID.'/', $defaultTabIndex );
	}else{
		kis_ui::loadModuleTab(array('studentinfo','parentinfo','otherinfo','remarks'), $display, '#/apps/admission/applicantslist/details/'.$schoolYearID.'/'.$recordID.'/',3 );
	}
	?>
    <!--tab end-->
    <p class="spacer"></p>
                     <!---->
                     <p class="spacer"></p>
                      	  <div class="table_board">
<? if(file_exists("{$PATH_WRT_ROOT}/includes/admission/{$setting_path_ip_rel}/template/applicantsList/{$display}_edit.php")){
        include("{$PATH_WRT_ROOT}/includes/admission/{$setting_path_ip_rel}/template/applicantsList/{$display}_edit.php");
    }elseif($sys_custom['KIS_Admission']['DynamicAdmissionForm']){
        $lauc->getDetailsPage($display, true);
    }else if($display=='icmssectiona'):?>
                      	    <table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['schoolYearOption']?></td>
                               <td width="70%"><?=kis_ui::displayTableField(getAcademicYearByAcademicYearID($applicationInfo['schoolYearId']))?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['selectcampus']?></td>
                               <td width="70%"><?=kis_ui::displayTableField($kis_lang['Admission']['icms']['tinhaucampus'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['selectprogram']?></td>
                               <td><?=kis_ui::displayTableField($classLevelAry[$applicationInfo['classLevelID']])?></td>
                             </tr>
                              <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['icms']['applyDayType']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                              		<?foreach($applyTimeSlotAry as $_key => $_value):?>
                              				<?$_key++;?>
                       						<td>(<?=$kis_lang['Admission']['Option']?> <?=$_key?>)</td>
                       						<td><?=$libinterface->GET_SELECTION_BOX($applyTimeSlotAry, "name='ApplyDayType[]' id='ApplyDayType".$_key."' class='timeslotselection'",$kis_lang['Admission']['Nil'], $applicationInfo['ApplyDayType'.$_key])?></td>
                       				<?endforeach;?>
                       					</tr>
                               		</table>
								</td>
                             </tr>
                           </tbody></table>
<? elseif($display=='icmssectionb'):?>
                      	    <table class="form_table">
                             <tbody>
                             <tr>
                               <td width="20%" class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['icms']['studentssurname']?></td>
                               <td width="30%"><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo $libinterface->GET_TEXTBOX('student_surname', 'student_surname', $tempStuEngName[0], $OtherClass='', $OtherPar=array())?></td>
                               <td width="50%" rowspan="14" width="145">
                               <div id="studentphoto" class="student_info" style="margin:0px;">
                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								    <div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
									<a id = "btn_remove" href="#" class="btn_remove"></a>
								    </div>
								    <div class="text_remark" style="text-align:center;"><?=$kis_lang['Admission']['msg']['clicktouploadphoto']?></div>
								</div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title" width="30%"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['icms']['firstname']?></td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo $libinterface->GET_TEXTBOX('student_firstname', 'student_firstname', $tempStuEngName[1], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['nameinchinese']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('student_name_b5', 'student_name_b5', $applicationInfo['student_name_b5'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['gender']?></td>
                               <td>
                               		<?=$libinterface->Get_Radio_Button('gender_M', 'gender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
                                	<?=$libinterface->Get_Radio_Button('gender_F', 'gender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><input type="text" name="dateofbirth" id="dateofbirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('placeofbirth', 'placeofbirth', $applicationInfo['placeofbirth'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['icms']['nationality']?></td>
                               <td><input type="text" name="province" id="province" value="<?=$applicationInfo['province']?>"></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['homeaddress']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('homeaddress', 'homeaddress', $applicationInfo['homeaddress'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['icms']['homenumber'] ?></td>
                               <td><input type="text" name="homephoneno" id="homephoneno" value="<?=$applicationInfo['homephoneno']?>"></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['icms']['emailaddress']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('email', 'email', $applicationInfo['email'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['icms']['CurBSName']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('OthersCurBSName', 'OthersCurBSName',  $otherApplicationInfo['CurBSName'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['icms']['langspokenathome']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('LangSpokenAtHome', 'LangSpokenAtHome', $applicationInfo['langspokenathome'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                           </tbody></table>
<? elseif($display=='icmssectionc'):?>
                      	    <table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <? foreach($kis_lang['Admission']['icms']['PG_Type'] as $_pgKey => $_pgLang): ?>
                               <td class="form_guardian_head" width="35%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                               		$tempEnglishName = explode(',',$applicationInfo[$_pgKey]['parent_name_en']);

                                	$parentAry['studentssurname'][] = '<td class="form_guardian_field">';
                               		$parentAry['studentssurname'][] = $libinterface->GET_TEXTBOX('parent_surname', 'parent_surname[]', $tempEnglishName[0], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                               		$parentAry['studentssurname'][] =  $libinterface->GET_HIDDEN_INPUT('parent_id', 'parent_id[]',  $applicationInfo[$_pgKey]['RecordID']?$applicationInfo[$_pgKey]['RecordID']:'new');
                               		$parentAry['studentssurname'][] = '</td>';

                               		$parentAry['firstname'][] = '<td class="form_guardian_field">';
                               		$parentAry['firstname'][] = $libinterface->GET_TEXTBOX('parent_firstname', 'parent_firstname[]', $tempEnglishName[1], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                               		$parentAry['firstname'][] = '</td>';

                               		$parentAry['mobilenumber'][] = '<td class="form_guardian_field">';
                                	$parentAry['mobilenumber'][] = '<input type="text" class="parent_'.$_pgKey.'" id="mobile" name="mobile[]" value="'.$applicationInfo[$_pgKey]['mobile'].'">';
                                	$parentAry['mobilenumber'][] = '</td>';

                               		$parentAry['nativelanguage'][] = '<td class="form_guardian_field">';
                                	$parentAry['nativelanguage'][] = $libinterface->GET_TEXTBOX('NativeLanguage', 'NativeLanguage[]', $applicationInfo[$_pgKey]['nativelanguage'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['nativelanguage'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = $libinterface->GET_TEXTBOX('occupation', 'occupation[]', $applicationInfo[$_pgKey]['occupation'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['worknumber'][] = '<td class="form_guardian_field">';
                                	$parentAry['worknumber'][] = '<input type="text" class="parent_'.$_pgKey.'" id="office" name="office[]" value="'.$applicationInfo[$_pgKey]['companyphone'].'">';
                                	$parentAry['worknumber'][] = '</td>';

                                	$parentAry['workaddress'][] = '<td class="form_guardian_field">';
                                	$parentAry['workaddress'][] = $libinterface->GET_TEXTBOX('companyaddress', 'companyaddress[]', $applicationInfo[$_pgKey]['companyaddress'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['workaddress'][] = '</td>';

                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['icms'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
<? elseif($display=='icmssectiondef'):?>
                      	    <table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['icms']['listprevschool']?></td>
                               <td width="70%"><?=$libinterface->GET_TEXTBOX('LastSchool', 'LastSchool', $studentApplicationInfo['lastschool'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['childdescription']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('ChildDescription', 'ChildDescription', $applicationInfo['childdescription'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['icms']['childhealth']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('ChildHealth', 'ChildHealth', $applicationInfo['childhealth'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                               <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['yourwish']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('YourWish', 'YourWish', $applicationInfo['yourwish'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                           </tbody></table>
<? elseif($display=='icmsattachment'):?>
							<table class="form_table">
                             <tbody>
                       		<?for($i=0;$i<sizeof($attachmentSettings);$i++) {

	                             	$attachment_name = $attachmentSettings[$i]['AttachmentName'];

//	                             	$_filePath = $attachmentList[$attachment_name]['link'];
	                             	$_filePath = '/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($attachment_name);
	                             	if($_filePath){
										$_spanDisplay = '';
										$_buttonDisplay = ' style="display:none;"';
									}else{
										$_filePath = '';
										$_spanDisplay = ' style="display:none;"';
										$_buttonDisplay = '';
									}

									$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
									$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';

	                             echo '<tr>
		                               	<td class="field_title">'.$attachment_name.'</td>
		                               	<td id="'.$attachment_name.'">
		                               		<span class="view_attachment" '.$_spanDisplay.'>'.$_attachment.'</span>
											<input type="button" class="attachment_upload_btn formsmallbutton" value="'.$kis_lang['Upload'].'"  id="uploader-'.$attachment_name.'" '.$_buttonDisplay.'/>
										</td>
		                             	</tr>';
	                             }?>
                           </tbody></table>
<? elseif($display=='studentinfo'):?>
	<?if($sys_custom['KIS_Admission']['YLSYK']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?></td>
                               <td width="40%"><?echo $libinterface->GET_TEXTBOX('student_name_b5', 'student_name_b5', $applicationInfo['student_name_b5'], $OtherClass='', $OtherPar=array())?></td>
                               <td width="30%" rowspan="7" width="145">
	                             <div id="studentphoto" class="student_info" style="margin:0px;">
                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								    <div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
										<a id = "btn_remove" href="#" class="btn_remove"></a>
								    </div>
								    <div class="text_remark" style="text-align:center;"><?=$kis_lang['Admission']['msg']['clicktouploadphoto']?></div>
								</div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?></td>
                               <td><?echo $libinterface->GET_TEXTBOX('student_name_en', 'student_name_en', $applicationInfo['student_name_en'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['gender']?></td>
                               <td>
                               		<?=$libinterface->Get_Radio_Button('gender_M', 'gender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
                                	<?=$libinterface->Get_Radio_Button('gender_F', 'gender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><input type="text" name="dateofbirth" id="dateofbirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('placeofbirth', 'placeofbirth', $applicationInfo['placeofbirth'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['YLSYK']['birthcertno']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('birthcertno', 'birthcertno', $applicationInfo['birthcertno'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['TSUENWANBCKG']['nationality']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('nationality', 'nationality', $applicationInfo['county'], $OtherClass='', $OtherPar=array())?></td>
							 </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['religion']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('religion', 'religion', $applicationInfo['religionOther'], $OtherClass='', $OtherPar=array())?><!--<?=$libadmission->displayPresetCodeSelection("RELIGION", "religion", $applicationInfo['religion'])?>--></td>
							 </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['homephoneno']?></td>
                               <td><input type="text" name="homephoneno" id="homephoneno" value="<?=$applicationInfo['homephoneno']?>"></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['TSUENWANBCKG']['homeaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Chi'].')'?></td>
                               <td><?=$libinterface->GET_TEXTBOX('homeaddresschi', 'homeaddresschi', $applicationInfo['homeaddresschi'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['TSUENWANBCKG']['homeaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Eng'].')'?></td>
                               <td><?=$libinterface->GET_TEXTBOX('homeaddress', 'homeaddress', $applicationInfo['homeaddress'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                              <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['csm']['email']?></td>
                              <td><input type="text" name="email" id="email" class="textboxtext" value="<?=$applicationInfo['email']?>"></td>
                            </tr>
                             <?php
	                             for($i=0;$i<sizeof($attachmentSettings);$i++) {

	                             	$attachment_name = $attachmentSettings[$i]['AttachmentName'];

	                             	$_filePath = $attachmentList[$attachment_name]['link'];
	                             	if($_filePath){
										$_spanDisplay = '';
										$_buttonDisplay = ' style="display:none;"';
									}else{
										$_filePath = '';
										$_spanDisplay = ' style="display:none;"';
										$_buttonDisplay = '';
									}

									$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
									$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';

	                             echo '<tr>
		                               	<td class="field_title">'.$attachment_name.'</td>
		                               	<td id="'.$attachment_name.'">
		                               		<span class="view_attachment" '.$_spanDisplay.'>'.$_attachment.'</span>
											<input type="button" class="attachment_upload_btn formsmallbutton" value="'.$kis_lang['Upload'].'"  id="uploader-'.$attachment_name.'" '.$_buttonDisplay.'/>
										</td>
		                             	</tr>';
	                             }
                             ?>
                           </tbody></table>
	<?}else if($sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td width="40%"><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo $libinterface->GET_TEXTBOX('student_surname_b5', 'student_surname_b5', $tempStuEngName[0], $OtherClass='', $OtherPar=array())?></td>
                               <td width="30%" rowspan="7" width="145">
	                             <div id="studentphoto" class="student_info" style="margin:0px;">
                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								    <div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
										<a id = "btn_remove" href="#" class="btn_remove"></a>
								    </div>
								    <div class="text_remark" style="text-align:center;"><?=$kis_lang['Admission']['msg']['clicktouploadphoto']?></div>
								</div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo $libinterface->GET_TEXTBOX('student_firstname_b5', 'student_firstname_b5', $tempStuEngName[1], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo $libinterface->GET_TEXTBOX('student_surname_en', 'student_surname_en', $tempStuEngName[0], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo $libinterface->GET_TEXTBOX('student_firstname_en', 'student_firstname_en', $tempStuEngName[1], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><input type="text" name="dateofbirth" id="dateofbirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['gender']?></td>
                               <td>
                               		<?=$libinterface->Get_Radio_Button('gender_M', 'gender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
                                	<?=$libinterface->Get_Radio_Button('gender_F', 'gender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('placeofbirth', 'placeofbirth', $applicationInfo['placeofbirth'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['langspokenathome']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('homelang', 'homelang', $applicationInfo['homelang'], $OtherClass='', $OtherPar=array())?></td>
							 </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['religion']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('religion', 'religion', $applicationInfo['religionOther'], $OtherClass='', $OtherPar=array())?><!--<?=$libadmission->displayPresetCodeSelection("RELIGION", "religion", $applicationInfo['religion'])?>--></td>
							 </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['church']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('church', 'church', $applicationInfo['church'], $OtherClass='', $OtherPar=array())?></td>
							 </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['munsang']['birthcertno']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('birthcertno', 'birthcertno', $applicationInfo['birthcertno'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['munsang']['phone']?></td>
                               <td><input type="text" name="homephoneno" id="homephoneno" value="<?=$applicationInfo['homephoneno']?>"></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['munsang']['mobile']?></td>
                               <td><input type="text" name="mobileno" id="mobileno" value="<?=$applicationInfo['mobileno']?>"></td>
                             </tr>
                             <tr>
                              <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['contactEmail']?></td>
                              <td><input type="text" name="email" id="email" class="textboxtext" value="<?=$applicationInfo['email']?>"></td>
                            </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['homeaddress']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('homeaddress', 'homeaddress', $applicationInfo['homeaddress'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['TSUENWANBCKG']['contactaddress']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('contactaddress', 'contactaddress', $applicationInfo['contactaddress'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <?php
	                             for($i=0;$i<sizeof($attachmentSettings);$i++) {

	                             	$attachment_name = $attachmentSettings[$i]['AttachmentName'];

	                             	$_filePath = $attachmentList[$attachment_name]['link'];
	                             	if($_filePath){
										$_spanDisplay = '';
										$_buttonDisplay = ' style="display:none;"';
									}else{
										$_filePath = '';
										$_spanDisplay = ' style="display:none;"';
										$_buttonDisplay = '';
									}

									$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
									$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';

	                             echo '<tr>
		                               	<td class="field_title">'.$attachment_name.'</td>
		                               	<td id="'.$attachment_name.'">
		                               		<span class="view_attachment" '.$_spanDisplay.'>'.$_attachment.'</span>
											<input type="button" class="attachment_upload_btn formsmallbutton" value="'.$kis_lang['Upload'].'"  id="uploader-'.$attachment_name.'" '.$_buttonDisplay.'/>
										</td>
		                             	</tr>';
	                             }
                             ?>
                           </tbody></table>
	<?}else if($sys_custom['KIS_Admission']['RMKG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td width="40%"><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo $libinterface->GET_TEXTBOX('student_surname_b5', 'student_surname_b5', $tempStuEngName[0], $OtherClass='', $OtherPar=array())?></td>
                               <td width="30%" rowspan="7" width="145">
	                             <div id="studentphoto" class="student_info" style="margin:0px;">
                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								    <div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
										<a id = "btn_remove" href="#" class="btn_remove"></a>
								    </div>
								    <div class="text_remark" style="text-align:center;"><?=$kis_lang['Admission']['msg']['clicktouploadphoto']?></div>
								</div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo $libinterface->GET_TEXTBOX('student_firstname_b5', 'student_firstname_b5', $tempStuEngName[1], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo $libinterface->GET_TEXTBOX('student_surname_en', 'student_surname_en', $tempStuEngName[0], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo $libinterface->GET_TEXTBOX('student_firstname_en', 'student_firstname_en', $tempStuEngName[1], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><input type="text" name="dateofbirth" id="dateofbirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['gender']?></td>
                               <td>
                               		<?=$libinterface->Get_Radio_Button('gender_M', 'gender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
                                	<?=$libinterface->Get_Radio_Button('gender_F', 'gender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('placeofbirth','placeofbirth',$applicationInfo['placeofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['RMKG']['nationality']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('Nationality','Nationality',$applicationInfo['Nationality'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['birthcertno']?></td>
                               <td>
                               		<?=$libinterface->GET_TEXTBOX('birthcertno', 'birthcertno', $applicationInfo['birthcertno'], $OtherClass='', $OtherPar=array())?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['CHIUCHUNKG']['homenumber']?></td>
                               <td><input type="text" name="homenumber" id="homenumber" value="<?=$applicationInfo['homephoneno']?>" maxlength="8"></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['CHIUCHUNKG']['homeaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Chi'].')'?></td>
                               <td>
                               		<table>
										<tr>
											<td width="120px"><?=$kis_lang['Admission']['RMKG']['room']?></td>
											<td><?=$libinterface->GET_TEXTBOX('HomeAddrRoom','HomeAddrRoom',$applicationInfo['AddressRoom'])?></td>
										</tr>
										<tr>
											<td width="120px"><?=$kis_lang['Admission']['RMKG']['floor']?></td>
											<td><?=$libinterface->GET_TEXTBOX('HomeAddrFloor','HomeAddrFloor',$applicationInfo['AddressFloor'])?></td>
										</tr>
										<tr>
											<td><?=$kis_lang['Admission']['RMKG']['block']?></td>
											<td><?=$libinterface->GET_TEXTBOX('HomeAddrBlock','HomeAddrBlock',$applicationInfo['AddressBlock'])?></td>
										</tr>
										<tr>
											<td><?=$kis_lang['Admission']['RMKG']['bldg']?></td>
											<td><?=$libinterface->GET_TEXTBOX('HomeAddrBlding','HomeAddrBlding',$applicationInfo['AddressBldg'])?></td>
										</tr>
										<tr>
											<td><?=$kis_lang['Admission']['RMKG']['street']?></td>
											<td><?=$libinterface->GET_TEXTBOX('HomeAddrStreet','HomeAddrStreet',$applicationInfo['AddressStreet'])?></td>
										</tr>
										<tr>
											<td><?=$kis_lang['Admission']['RMKG']['district']?></td>
											<td><?=$lauc->getSelectByConfig($admission_cfg['addressdistrict'],'HomeAddrDistrict',$applicationInfo['AddressDistrict'])?></td>
										</tr>
									</table>
                               	</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['RMKG']['1stemail']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('Email','Email',$applicationInfo['Email'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['RMKG']['2ndemail']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('Email2','Email2',$applicationInfo['Email2'])?></td>
                             </tr>
                             <?php
	                             for($i=0;$i<sizeof($attachmentSettings);$i++) {

	                             	$attachment_name = $attachmentSettings[$i]['AttachmentName'];

	                             	$_filePath = $attachmentList[$attachment_name]['link'];
	                             	if($_filePath){
										$_spanDisplay = '';
										$_buttonDisplay = ' style="display:none;"';
									}else{
										$_filePath = '';
										$_spanDisplay = ' style="display:none;"';
										$_buttonDisplay = '';
									}

									$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
									$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';

	                             echo '<tr>
		                               	<td class="field_title">'.$attachment_name.'</td>
		                               	<td id="'.$attachment_name.'">
		                               		<span class="view_attachment" '.$_spanDisplay.'>'.$_attachment.'</span>
											<input type="button" class="attachment_upload_btn formsmallbutton" value="'.$kis_lang['Upload'].'"  id="uploader-'.$attachment_name.'" '.$_buttonDisplay.'/>
										</td>
		                             	</tr>';
	                             }
                             ?>
                           </tbody></table>
	<?}else if($sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td width="40%"><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo $libinterface->GET_TEXTBOX('student_surname_b5', 'student_surname_b5', $tempStuEngName[0], $OtherClass='', $OtherPar=array())?></td>
                               <td width="30%" rowspan="7" width="145">
	                             <div id="studentphoto" class="student_info" style="margin:0px;">
                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								    <div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
										<a id = "btn_remove" href="#" class="btn_remove"></a>
								    </div>
								    <div class="text_remark" style="text-align:center;"><?=$kis_lang['Admission']['msg']['clicktouploadphoto']?></div>
								</div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo $libinterface->GET_TEXTBOX('student_firstname_b5', 'student_firstname_b5', $tempStuEngName[1], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo $libinterface->GET_TEXTBOX('student_surname_en', 'student_surname_en', $tempStuEngName[0], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo $libinterface->GET_TEXTBOX('student_firstname_en', 'student_firstname_en', $tempStuEngName[1], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><input type="text" name="dateofbirth" id="dateofbirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['gender']?></td>
                               <td>
                               		<?=$libinterface->Get_Radio_Button('gender_M', 'gender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
                                	<?=$libinterface->Get_Radio_Button('gender_F', 'gender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td>
                               		<?=$lauc->getSelectByConfig($admission_cfg['placeofbirth'],'placeofbirth',$applicationInfo['placeofbirth'])?>
                               		<span id="POB_Other" style="display:<?=($applicationInfo['placeofbirth']==$admission_cfg['placeofbirth']['other']?'inline;' :'none;' )?>"><?=$kis_lang['Admission']['CHIUCHUNKG']['PleaseSpecify']?><input name="FormStudentInfo_POB_Other" type="text" id="FormStudentInfo_POB_Other" class="textboxtext" style="width:50px" value="<?=$applicationInfo['PlaceOfBirthOther']?>"></span>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['nativeplace']?></td>
								<td>
	                               <input type="text" name="province" id="province" value="<?=$applicationInfo['province']?>">
	                               <?=$kis_lang['Admission']['province']?>
	                               <input type="text" name="county" id="county" value="<?=$applicationInfo['county']?>">
	                               <?=$kis_lang['Admission']['county']?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['religion']?></td>
                               <td>
                               		<?=$lauc->getSelectByConfig($admission_cfg['religion'],'religion',$applicationInfo['religion1'])?>
                               		<span id="Religion_Other" style="display:<?=($applicationInfo['religion1']==$admission_cfg['religion']['other']?'inline;' :'none;' )?>"><?=$kis_lang['Admission']['CHIUCHUNKG']['PleaseSpecify']?><input name="FormStudentInfo_Religion_Other" type="text" id="FormStudentInfo_Religion_Other" class="textboxtext" style="width:50px" value="<?=$applicationInfo['ReligionOther']?>"></span>
                               </td>
							 </tr>
							 <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['church']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('church', 'church', $applicationInfo['Church'], $OtherClass='', $OtherPar=array())?></td>
							 </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['CHIUCHUNKG']['IDtype']?></td>
                               <td>
                               		<?=$lauc->getSelectByConfig($admission_cfg['BirthCertType'],'FormStudentInfo_StuIDType',$applicationInfo['birthcerttype'])?>
                               		<span id="StuID_Other" style="display:<?=($applicationInfo['birthcerttype']==$admission_cfg['BirthCertType']['other']?'inline;' :'none;' )?>"><?=$kis_lang['Admission']['CHIUCHUNKG']['PleaseSpecify']?><input name="FormStudentInfo_StuIDType_Other" type="text" id="FormStudentInfo_StuIDType_Other" class="textboxtext" style="width:100px" value="<?=$applicationInfo['birthcerttypeother']?>"></span>
                               		<?=$libinterface->GET_TEXTBOX('birthcertno', 'birthcertno', $applicationInfo['birthcertno'], $OtherClass='', $OtherPar=array())?>
                               </td>
                             </tr>
                             <? $StuPhoneNoAry = explode('|||',$applicationInfo['homephoneno']);?>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['CHIUCHUNKG']['homenumber']?></td>
                               <td><input type="text" name="homenumber" id="homenumber" value="<?=$StuPhoneNoAry[0]?>"></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['contactnumber']?></td>
                               <td><input type="text" name="contactnumber" id="contactnumber" value="<?=$StuPhoneNoAry[1]?>"></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['CHIUCHUNKG']['homeaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Chi'].')'?></td>
                               <td>
                               		<?$AddressDistrict = explode('|||',$applicationInfo['AddressDistrict'])?>
                               		<?=$lauc->getSelectByConfig($admission_cfg['addressdistrict'],'HomeAddressDistrict',$AddressDistrict[0]) ?>
                               		<?=$libinterface->GET_TEXTBOX('homeaddresschi', 'homeaddresschi', $applicationInfo['homeaddresschi'], $OtherClass='', $OtherPar=array())?>
                               	</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['CHIUCHUNKG']['homeaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Eng'].')'?></td>
                               <td><?=$libinterface->GET_TEXTBOX('homeaddress', 'homeaddress', $applicationInfo['homeaddress'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['contactaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Chi'].')'?></td>
                               <td>
                               		<?=$lauc->getSelectByConfig($admission_cfg['addressdistrict'],'ContactAddressDistrict',$AddressDistrict[1])?>
                               		<?=$libinterface->GET_TEXTBOX('ContactAddressChi', 'ContactAddressChi', $applicationInfo['ContactAddressChi'], $OtherClass='', $OtherPar=array())?>
                               	</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['contactaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Eng'].')'?></td>
                               <td><?=$libinterface->GET_TEXTBOX('ContactAddress', 'ContactAddress', $applicationInfo['ContactAddress'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <?php
	                             for($i=0;$i<sizeof($attachmentSettings);$i++) {

	                             	$attachment_name = $attachmentSettings[$i]['AttachmentName'];

	                             	$_filePath = $attachmentList[$attachment_name]['link'];
	                             	if($_filePath){
										$_spanDisplay = '';
										$_buttonDisplay = ' style="display:none;"';
									}else{
										$_filePath = '';
										$_spanDisplay = ' style="display:none;"';
										$_buttonDisplay = '';
									}

									$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
									$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';

	                             echo '<tr>
		                               	<td class="field_title">'.$attachment_name.'</td>
		                               	<td id="'.$attachment_name.'">
		                               		<span class="view_attachment" '.$_spanDisplay.'>'.$_attachment.'</span>
											<input type="button" class="attachment_upload_btn formsmallbutton" value="'.$kis_lang['Upload'].'"  id="uploader-'.$attachment_name.'" '.$_buttonDisplay.'/>
										</td>
		                             	</tr>';
	                             }
                             ?>
                           </tbody></table>
	<?}else if($sys_custom['KIS_Admission']['MUNSANG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td width="40%"><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo $libinterface->GET_TEXTBOX('student_surname_b5', 'student_surname_b5', $tempStuEngName[0], $OtherClass='', $OtherPar=array())?></td>
                               <td width="30%" rowspan="7" width="145">
	                             <div id="studentphoto" class="student_info" style="margin:0px;">
                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								    <div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
										<a id = "btn_remove" href="#" class="btn_remove"></a>
								    </div>
								    <div class="text_remark" style="text-align:center;"><?=$kis_lang['Admission']['msg']['clicktouploadphoto']?></div>
								</div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo $libinterface->GET_TEXTBOX('student_firstname_b5', 'student_firstname_b5', $tempStuEngName[1], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo $libinterface->GET_TEXTBOX('student_surname_en', 'student_surname_en', $tempStuEngName[0], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo $libinterface->GET_TEXTBOX('student_firstname_en', 'student_firstname_en', $tempStuEngName[1], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><input type="text" name="dateofbirth" id="dateofbirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['gender']?></td>
                               <td>
                               		<?=$libinterface->Get_Radio_Button('gender_M', 'gender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
                                	<?=$libinterface->Get_Radio_Button('gender_F', 'gender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('placeofbirth', 'placeofbirth', $applicationInfo['placeofbirth'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['religion']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('religion', 'religion', $applicationInfo['religion'], $OtherClass='', $OtherPar=array())?><!--<?=$libadmission->displayPresetCodeSelection("RELIGION", "religion", $applicationInfo['religion'])?>--></td>
							 </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['munsang']['birthcertno']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('birthcertno', 'birthcertno', $applicationInfo['birthcertno'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['munsang']['phone']?></td>
                               <td><input type="text" name="homephoneno" id="homephoneno" value="<?=$applicationInfo['homephoneno']?>"></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['homeaddress']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('homeaddress', 'homeaddress', $applicationInfo['homeaddress'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['contactEmail']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('email', 'email', $applicationInfo['email'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
								<td class="field_title">
									<?=$kis_lang['Admission']['KTLMSKG']['twins']?>
								</td>
								<td>
									<?=$libinterface->Get_Radio_Button('twinsY', 'twins', 'Y', ($applicationInfo['istwinsapplied']=='Y'), '', $kis_lang['Admission']['yes'])?>
									<?=$libinterface->Get_Radio_Button('twinsN', 'twins', 'N', ($applicationInfo['istwinsapplied']=='N'), '', $kis_lang['Admission']['no'])?>
								</td>
							</tr>
							<tr>
								<td class="field_title">
									<?=$kis_lang['Admission']['munsang']['twinsID']?>
								</td>
								<td>
									<input type="text" class="textboxtext"
										name="twinsapplicationid" id="twinsapplicationid"
										value="<?=$applicationInfo['twinsapplicationid']?>"
										onkeypress="$('#twinsY').attr('checked', 'checked')"
									/>
								</td>
							</tr>
                             <?php
	                             for($i=0;$i<sizeof($attachmentSettings);$i++) {

	                             	$attachment_name = $attachmentSettings[$i]['AttachmentName'];

	                             	$_filePath = $attachmentList[$attachment_name]['link'];
	                             	if($_filePath){
										$_spanDisplay = '';
										$_buttonDisplay = ' style="display:none;"';
									}else{
										$_filePath = '';
										$_spanDisplay = ' style="display:none;"';
										$_buttonDisplay = '';
									}

									$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
									$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';

	                             echo '<tr>
		                               	<td class="field_title">'.$attachment_name.'</td>
		                               	<td id="'.$attachment_name.'">
		                               		<span class="view_attachment" '.$_spanDisplay.'>'.$_attachment.'</span>
											<input type="button" class="attachment_upload_btn formsmallbutton" value="'.$kis_lang['Upload'].'"  id="uploader-'.$attachment_name.'" '.$_buttonDisplay.'/>
										</td>
		                             	</tr>';
	                             }
                             ?>
                           </tbody></table>
	<?}else if($sys_custom['KIS_Admission']['CSM']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td class="field_title" width="20%"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['csm']['englishName']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td width="50%"><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo $libinterface->GET_TEXTBOX('student_surname_en', 'student_surname_en', $tempStuEngName[0], $OtherClass='', $OtherPar=array())?></td>
                               <td width="30%" rowspan="14" width="145">
                               <div id="studentphoto" class="student_info" style="margin:0px;">
                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								    <div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
									<a id = "btn_remove" href="#" class="btn_remove"></a>
								    </div>
								    <div class="text_remark" style="text-align:center;"><?=$kis_lang['Admission']['msg']['clicktouploadphoto']?></div>
								</div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title" width="30%"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['csm']['englishName']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo $libinterface->GET_TEXTBOX('student_firstname_en', 'student_firstname_en', $tempStuEngName[1], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo $libinterface->GET_TEXTBOX('student_surname_b5', 'student_surname_b5', $tempStuEngName[0], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo $libinterface->GET_TEXTBOX('student_firstname_b5', 'student_firstname_b5', $tempStuEngName[1], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['gender']?></td>
                               <td>
                               		<?=$libinterface->Get_Radio_Button('gender_M', 'gender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
                                	<?=$libinterface->Get_Radio_Button('gender_F', 'gender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><input type="text" name="dateofbirth" id="dateofbirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('placeofbirth', 'placeofbirth', $applicationInfo['placeofbirth'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['csm']['birthcertno']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('birthcertno', 'birthcertno', $applicationInfo['birthcertno'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['csm']['homephoneno']?></td>
                               <td><input type="text" name="homephoneno" id="homephoneno" value="<?=$applicationInfo['homephoneno']?>"></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['csm']['homeaddress']?></td>
                               <td><?
                               for($i=1; $i<=count($kis_lang['Admission']['csm']['AddressLocation']); $i++){
									if($i == count($kis_lang['Admission']['csm']['AddressLocation'])){
										echo '<table><tr><td width="55px" style="padding:0px">'.$libinterface->Get_Radio_Button('homeaddress'.$i, 'homeaddress', $i, !is_numeric($applicationInfo['homeaddress']),'',$kis_lang['Admission']['csm']['AddressLocation'][$i]).'</td><td><input name="homeaddressothers" type="text" id="homeaddressothers" class="textboxtext" value="'.(!is_numeric($applicationInfo['homeaddress'])?$applicationInfo['homeaddress']:'').'" /></td></tr></table>';
									}else{
										echo $libinterface->Get_Radio_Button('homeaddress'.$i, 'homeaddress', $i, $applicationInfo['homeaddress']==$i,'',$kis_lang['Admission']['csm']['AddressLocation'][$i]).'<br/>';
									}
								}
                               	?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['csm']['lastschool']?></td>
                               <td><?=$libinterface->Get_Radio_Button('lastschool1', 'lastschool', 'yes', $applicationInfo['lastschool']=='yes','',$kis_lang['Admission']['yes']).'&nbsp;&nbsp;'
										.$libinterface->Get_Radio_Button('lastschool2', 'lastschool', 'no', $applicationInfo['lastschool']=='no','',$kis_lang['Admission']['no'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['csm']['isTwins']?></td>
                               <td><?=$libinterface->Get_Radio_Button('IsTwins1', 'IsTwins', 'Y', $applicationInfo['IsTwins']=='Y','',$kis_lang['Admission']['yes']).'&nbsp;&nbsp;'
										.$libinterface->Get_Radio_Button('IsTwins2', 'IsTwins', 'N', $applicationInfo['IsTwins']=='N','',$kis_lang['Admission']['no'])
										.' ( '.$kis_lang['Admission']['csm']['isTwinsApplied'].' '.$libinterface->Get_Radio_Button('IsTwinsApplied1', 'IsTwinsApplied', 'Y', $applicationInfo['IsTwinsApplied']=='Y','',$kis_lang['Admission']['yes2']).' '.$libinterface->Get_Radio_Button('IsTwinsApplied2', 'IsTwinsApplied', 'N', $applicationInfo['IsTwinsApplied']=='N','',$kis_lang['Admission']['no2']).')<br/>'.$kis_lang['Admission']['csm']['TwinsApplicationID'].': <input type="text" name="TwinsApplicationID" id="TwinsApplicationID" value="'.$applicationInfo['TwinsApplicationID'].'">'
										?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['csm']['CurBSName']?></td>
                               <td><?=$libinterface->Get_Radio_Button('CurBSName1', 'CurBSName', 'yes', $applicationInfo['CurBSName']=='yes','',$kis_lang['Admission']['yes']).'&nbsp;&nbsp;'
										.$libinterface->Get_Radio_Button('CurBSName2', 'CurBSName', 'no', $applicationInfo['CurBSName']=='no','',$kis_lang['Admission']['no'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['csm']['applyDayType']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                              		<?foreach($applyTimeSlotAry as $_key => $_value):?>
                              				<?$_key++;?>
                       						<td>(<?=$kis_lang['Admission']['Option']?> <?=$_key?>)</td>
                       						<td><?=$libinterface->GET_SELECTION_BOX($applyTimeSlotAry, "name='ApplyDayType[]' id='ApplyDayType".$_key."' class='timeslotselection'",$kis_lang['Admission']['Nil'], $otherApplicationInfo['ApplyDayType'.$_key])?></td>
                       				<?endforeach;?>
                       					</tr>
                               		</table>
								</td>
                             </tr>
                             <?php
                             if($sys_custom['KIS_Admission']['TBCPK']['Settings']){

                             	$_filePath = $attachmentList['birth_cert']['link'];
                             	if($_filePath){
									$_spanDisplay = '';
									$_buttonDisplay = ' style="display:none;"';
								}else{
									$_filePath = '';
									$_spanDisplay = ' style="display:none;"';
									$_buttonDisplay = '';
								}

								$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
								$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';

                             echo '<tr>
                               <td class="field_title">'.$kis_lang['Admission']['birth_cert'].'</td>
                               <td id="birth_cert">
                               		<span class="view_attachment" '.$_spanDisplay.'>'.$_attachment.'</span>
									<input type="button" class="attachment_upload_btn formsmallbutton" value="'.$kis_lang['Upload'].'"  id="uploader-birth_cert" '.$_buttonDisplay.'/>
								</td>
                             </tr>';

                             	$_filePath = $attachmentList['immunisation_record']['link'];
                             	if($_filePath){
									$_spanDisplay = '';
									$_buttonDisplay = ' style="display:none;"';
								}else{
									$_filePath = '';
									$_spanDisplay = ' style="display:none;"';
									$_buttonDisplay = '';
								}

								$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
								$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';

                             echo '<tr>
                               <td class="field_title">'.$kis_lang['Admission']['immunisation_record'].'</td>
                               <td id="immunisation_record">
                               		<span class="view_attachment" '.$_spanDisplay.'>'.$_attachment.'</span>
									<input type="button" class="attachment_upload_btn formsmallbutton" value="'.$kis_lang['Upload'].'"  id="uploader-immunisation_record" '.$_buttonDisplay.'/>
								</td>
                             </tr>';
                             }
                             else{
	                             for($i=0;$i<sizeof($attachmentSettings);$i++) {

	                             	$attachment_name = $attachmentSettings[$i]['AttachmentName'];

	                             	$_filePath = $attachmentList[$attachment_name]['link'];
	                             	if($_filePath){
										$_spanDisplay = '';
										$_buttonDisplay = ' style="display:none;"';
									}else{
										$_filePath = '';
										$_spanDisplay = ' style="display:none;"';
										$_buttonDisplay = '';
									}

									$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
									$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';

	                             echo '<tr>
		                               	<td class="field_title">'.$attachment_name.'</td>
		                               	<td id="'.$attachment_name.'">
		                               		<span class="view_attachment" '.$_spanDisplay.'>'.$_attachment.'</span>
											<input type="button" class="attachment_upload_btn formsmallbutton" value="'.$kis_lang['Upload'].'"  id="uploader-'.$attachment_name.'" '.$_buttonDisplay.'/>
										</td>
		                             	</tr>';
	                             }
                             }
                             ?>

                           </tbody></table>
    <?}else if($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings']){?>
                      	    <table class="form_table">
                             <tbody>
                             <tr>
                               <td class="field_title" width="20%"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?></td>
                               <td width="30%"><?=$libinterface->GET_TEXTBOX('student_name_b5', 'student_name_b5', $applicationInfo['student_name_b5'], $OtherClass='', $OtherPar=array())?></td>
                               <td width="50%" rowspan="14" width="145">
                               <div id="studentphoto" class="student_info" style="margin:0px;">
                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								    <div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
									<a id = "btn_remove" href="#" class="btn_remove"></a>
								    </div>
								    <div class="text_remark" style="text-align:center;"><?=$kis_lang['Admission']['msg']['clicktouploadphoto']?></div>
								</div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title" width="30%"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('student_name_en', 'student_name_en', $applicationInfo['student_name_en'], $OtherClass='', $OtherPar=array())?></td>
                              </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><input type="text" name="dateofbirth" id="dateofbirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['gender']?></td>
                               <td>
                               		<?=$libinterface->Get_Radio_Button('gender_M', 'gender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
                                	<?=$libinterface->Get_Radio_Button('gender_F', 'gender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['birthcertno']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('birthcertno', 'birthcertno', $applicationInfo['birthcertno'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('placeofbirth', 'placeofbirth', $applicationInfo['placeofbirth'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['RMKG']['nationality']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('nationality', 'nationality', $applicationInfo['Nationality'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['homephoneno']?></td>
                               <td>(852) <input type="text" name="homephoneno" id="homephoneno" value="<?=$applicationInfo['homephoneno']?>" style="width: calc(99% - 50px);"></td>
                             </tr>
                             <!--tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['UCCKE']['fax']?></td>
                               <td><input type="text" name="fax" id="fax" value="<?=$applicationInfo['fax']?>"></td>
                             </tr-->
                              <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['homeaddress']?></td>
                               <td colspan="2">
                				    <table>
                				        <colgroup>
                				            <col style="width: 120px;">
                				            <col style="width: 100px;">
                				            <col style="width: 100px;">
                				            <col style="width: 100px;">
                				            <col style="width: 100px;">
                				            <col style="width: 150px;">
                				            <col>
                				        </colgroup>
                				        <tr>
                				            <td><?=$kis_lang['Admission']['MINGWAI']['room'] ?></td>
                				            <td>
                				            	<input type="text" name="HomeAddrRoom" id="HomeAddrRoom" value="<?=$applicationInfo['AddressRoom']?>">
                				            </td>
                				            <td style="text-align: right;"><?=$kis_lang['Admission']['MINGWAI']['floor'] ?></td>
                				            <td>
                				            	<input type="text" name="HomeAddrFloor" id="HomeAddrFloor" value="<?=$applicationInfo['AddressFloor']?>">
                				            </td>
                				            <td style="text-align: right;"><?=$kis_lang['Admission']['MINGWAI']['block'] ?></td>
                				            <td>
                				            	<input type="text" name="HomeAddrBlock" id="HomeAddrBlock" value="<?=$applicationInfo['AddressBlock']?>">
                				            </td>
                				            <td>&nbsp;</td>
                				        </tr>
                				        <tr>
                				            <td><?=$kis_lang['Admission']['MINGWAI']['building'] ?></td>
                				            <td colspan="5">
                				            	<input type="text" name="HomeAddrBlding" id="HomeAddrBlding" value="<?=$applicationInfo['AddressBldg']?>" class="textboxtext"/>
                				            </td>
                				        </tr>
                				        <tr>
                				            <td><?=$kis_lang['Admission']['MINGWAI']['street'] ?></td>
                				            <td colspan="5">
                				            	<input type="text" name="HomeAddrStreet" id="HomeAddrStreet" value="<?=$applicationInfo['AddressStreet']?>" class="textboxtext"/>
                				            </td>
                				        </tr>
                				        <tr>
                				            <td><?=$kis_lang['Admission']['MINGWAI']['district'] ?></td>
                				            <td colspan="5">
                				            	<input type="text" name="HomeAddrDistrict" id="HomeAddrDistrict" value="<?=$applicationInfo['AddressDistrict']?>" class="textboxtext"/>
                				            </td>
                				        </tr>
                				        <tr>
                				            <td><?=$kis_lang['Admission']['MINGWAI']['region'] ?></td>
                				            <td colspan="5">
                				            	<select name="HomeAddrRegion" id="HomeAddrRegion">
                                                    <?php
            										foreach($admission_cfg['addressdistrict'] as $key => $langVal):
            										    $selected = ($applicationInfo['AddressEstate'] == $langVal)? 'selected' : '';
            										?>
            											<option value="<?=$langVal ?>" <?=$selected ?> ><?=$kis_lang['Admission']['CHIUCHUNKG'][$key] ?></option>
            										<?php
            										endforeach;
            										?>
                                                </select>
                				            </td>
                				        </tr>
                				    </table>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['MINGWAI']['email']?></td>
                               <td>
                               		<?php
                               		$role = $kis_data['libadmission']->getApplicationCustInfo($applicationInfo['applicationID'], 'PrimaryEmailRole', 1);
                               		$role = $role['Value'];
                               		?>
                        		    <select id="PrimaryEmailRole" name="PrimaryEmailRole">
                        		        <option value="F" <?= ($role == 'F')? 'selected' : '' ?> ><?=$kis_lang['Admission']['PG_Type']['F'] ?></option>
                        		        <option value="M" <?= ($role == 'M')? 'selected' : '' ?> ><?=$kis_lang['Admission']['PG_Type']['M'] ?></option>
                        		        <option value="G" <?= ($role == 'G')? 'selected' : '' ?> ><?=$kis_lang['Admission']['PG_Type']['G'] ?></option>
                        		    </select>
                               		<?=$libinterface->GET_TEXTBOX('email', 'email', $applicationInfo['Email'], $OtherClass='', $OtherPar=array( 'style'=>'width: calc(99% - 100px);' ))?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['MINGWAI']['email2']?></td>
                               <td>
                               		<?php
                               		$role = $kis_data['libadmission']->getApplicationCustInfo($applicationInfo['applicationID'], 'SecondaryEmailRole', 1);
                               		$role = $role['Value'];
                               		?>
                        		    <select id="SecondaryEmailRole" name="SecondaryEmailRole">
                        		        <option value="F" <?= ($role == 'F')? 'selected' : '' ?> ><?=$kis_lang['Admission']['PG_Type']['F'] ?></option>
                        		        <option value="M" <?= ($role == 'M')? 'selected' : '' ?> ><?=$kis_lang['Admission']['PG_Type']['M'] ?></option>
                        		        <option value="G" <?= ($role == 'G')? 'selected' : '' ?> ><?=$kis_lang['Admission']['PG_Type']['G'] ?></option>
                        		    </select>
                               		<?=$libinterface->GET_TEXTBOX('email2', 'email2', $applicationInfo['Email2'], $OtherClass='', $OtherPar=array( 'style'=>'width: calc(99% - 100px);' ))?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['HKUGAPS']['SpokenLanguage']?></td>
                               <td>
                        		    <select id="SpokenLanguage" name="SpokenLanguage">
										<?php
										$SpokenLanguageOther = '';
										foreach($admission_cfg['lang'] as $key => $langVal):
										    $selected = ($applicationInfo['LangSpokenAtHome'] == $langVal)? 'selected' : '';
										    if($langVal == $admission_cfg['lang']['others'] && !in_array($applicationInfo['LangSpokenAtHome'], $admission_cfg['lang'])){
										        $selected = 'selected';
										        $SpokenLanguageOther = $applicationInfo['LangSpokenAtHome'];
										    }
										?>
											<option value="<?=$langVal ?>" <?=$selected ?> ><?=$kis_lang['Admission']['MINGWAI'][$key] ?></option>
										<?php
										endforeach;
										?>
                        		    </select>
                    			    <span id="SpokenLanguageOtherDiv" style="<?= ($SpokenLanguageOther)? '' : 'display:none;' ?>">
                        			    (
                            			    <?= $kis_lang['Admission']['PleaseSpecify'] ?>:
                            			    <?=$libinterface->GET_TEXTBOX('SpokenLanguageOther', 'SpokenLanguageOther', $SpokenLanguageOther, $OtherClass='', $OtherPar=array( 'style'=>'width: 150px;' ))?>
                        			    )
                    			    </span>
                    			    <script>
                        			    $(function(){
                        			    	$('#SpokenLanguage').change(function(){
                        			    		if($(this).val() == '<?=$admission_cfg['lang']['others'] ?>'){
                        			    			$('#SpokenLanguageOtherDiv').show();
                        			    		}else{
                        			    			$('#SpokenLanguageOtherDiv').hide();
                        			    		}
                        			    	});
                        			    });
                    			    </script>
								</td>
                              </tr>
                             <tr>
								<td class="field_title">
									<?=$kis_lang['Admission']['KTLMSKG']['twins']?>
								</td>
								<td>
									<?=$libinterface->Get_Radio_Button('twinsY', 'twins', '1', ($applicationInfo['IsTwinsApplied']=='1'), '', $kis_lang['Admission']['yes'])?>
									<?=$libinterface->Get_Radio_Button('twinsN', 'twins', '0', ($applicationInfo['IsTwinsApplied']=='0'), '', $kis_lang['Admission']['no'])?>
								</td>
							</tr>
							<tr>
								<td class="field_title">
									<?=$kis_lang['Admission']['munsang']['twinsID']?>
								</td>
								<td>
									<input type="text" class="textboxtext"
										name="twinsapplicationid" id="twinsapplicationid"
										value="<?=$applicationInfo['TwinsApplicationID']?>"
										onkeypress="$('#twinsY').attr('checked', 'checked')"
									/>
								</td>
							</tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['lastschool']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('lastschool', 'lastschool', $applicationInfo['LastSchool'], $OtherClass='', $OtherPar=array())?></td>
                              </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['lastschoollevel']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('lastschoollevel', 'lastschoollevel', $applicationInfo['LastSchoolLevel'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <?php
	                             for($i=0;$i<sizeof($attachmentSettings);$i++) {

	                             	$attachment_name = $attachmentSettings[$i]['AttachmentName'];

	                             	$_filePath = $attachmentList[$attachment_name]['link'];
	                             	if($_filePath){
										$_spanDisplay = '';
										$_buttonDisplay = ' style="display:none;"';
									}else{
										$_filePath = '';
										$_spanDisplay = ' style="display:none;"';
										$_buttonDisplay = '';
									}

									$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
									$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';

	                             echo '<tr>
		                               	<td class="field_title">'.$attachment_name.'</td>
		                               	<td id="'.$attachment_name.'">
		                               		<span class="view_attachment" '.$_spanDisplay.'>'.$_attachment.'</span>
											<input type="button" class="attachment_upload_btn formsmallbutton" value="'.$kis_lang['Upload'].'"  id="uploader-'.$attachment_name.'" '.$_buttonDisplay.'/>
										</td>
		                             	</tr>';
	                             }
//                             }
                             ?>

                           </tbody></table>
    <?}else if(file_exists(dirname(__FILE__) . "/{$setting_path_ip_rel}/studentinfo_edit.php")){
		include(dirname(__FILE__) . "/{$setting_path_ip_rel}/studentinfo_edit.php");
	}else{?>
                      	    <table class="form_table">
                             <tbody>
                             <tr>
                               <td class="field_title" width="20%"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?></td>
                               <td width="30%"><?=$libinterface->GET_TEXTBOX('student_name_b5', 'student_name_b5', $applicationInfo['student_name_b5'], $OtherClass='', $OtherPar=array())?></td>
                               <td width="50%" rowspan="14" width="145">
                               <div id="studentphoto" class="student_info" style="margin:0px;">
                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								    <div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
									<a id = "btn_remove" href="#" class="btn_remove"></a>
								    </div>
								    <div class="text_remark" style="text-align:center;"><?=$kis_lang['Admission']['msg']['clicktouploadphoto']?></div>
								</div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title" width="30%"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('student_name_en', 'student_name_en', $applicationInfo['student_name_en'], $OtherClass='', $OtherPar=array())?></td>
                              </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><input type="text" name="dateofbirth" id="dateofbirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['gender']?></td>
                               <td>
                               		<?=$libinterface->Get_Radio_Button('gender_M', 'gender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
                                	<?=$libinterface->Get_Radio_Button('gender_F', 'gender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['birthcertno']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('birthcertno', 'birthcertno', $applicationInfo['birthcertno'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('placeofbirth', 'placeofbirth', $applicationInfo['placeofbirth'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <!--
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['nativeplace']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               				<td>(<?=$kis_lang['Admission']['province']?>)</td>
                               				<td><input type="text" name="province" id="province" value="<?=$applicationInfo['province']?>"></td>
                               				<td>(<?=$kis_lang['Admission']['county']?>)</td>
                               				<td><input type="text" name="county" id="county" value="<?=$applicationInfo['county']?>"></td>
                               			</tr>
                               		</table>
                               </td>
                            </tr>
                            -->
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['langspokenathome']?></td>
                               <td><input type="text" name="homeLang" id="homeLang" value="<?=$applicationInfo['homeLang']?>"></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['homephoneno']?></td>
                               <td><input type="text" name="homephoneno" id="homephoneno" value="<?=$applicationInfo['homephoneno']?>"></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['contactperson']?></td>
                                <td><input type="text" name="contactperson" id="contactperson" value="<?=$applicationInfo['contactperson']?>"></td>
                             </tr>
                              <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['contactpersonrelationship']?></td>
                                <td><input type="text" name="contactpersonrelationship" id="contactpersonrelationship" value="<?=$applicationInfo['contactpersonrelationship']?>"></td>
                             </tr>
                              <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['homeaddress']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('homeaddress', 'homeaddress', $applicationInfo['homeaddress'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['email']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('email', 'email', $applicationInfo['email'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['religion']?></td>
                               <td><?=$libadmission->displayPresetCodeSelection("RELIGION", "religion", $applicationInfo['religion'])?></td>
							 </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['church']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('church', 'church', $applicationInfo['church'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['lastschool']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('lastschool', 'lastschool', $applicationInfo['lastschool'], $OtherClass='', $OtherPar=array())?></td>
                              </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['lastschoollevel']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('lastschoollevel', 'lastschoollevel', $applicationInfo['lastschoollevel'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <?php
//                             if($sys_custom['KIS_Admission']['TBCPK']['Settings']){
//
//                             	$_filePath = $attachmentList['birth_cert']['link'];
//                             	if($_filePath){
//									$_spanDisplay = '';
//									$_buttonDisplay = ' style="display:none;"';
//								}else{
//									$_filePath = '';
//									$_spanDisplay = ' style="display:none;"';
//									$_buttonDisplay = '';
//								}
//
//								$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
//								$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';
//
//                             echo '<tr>
//                               <td class="field_title">'.$kis_lang['Admission']['birth_cert'].'</td>
//                               <td id="birth_cert">
//                               		<span class="view_attachment" '.$_spanDisplay.'>'.$_attachment.'</span>
//									<input type="button" class="attachment_upload_btn formsmallbutton" value="'.$kis_lang['Upload'].'"  id="uploader-birth_cert" '.$_buttonDisplay.'/>
//								</td>
//                             </tr>';
//
//                             	$_filePath = $attachmentList['immunisation_record']['link'];
//                             	if($_filePath){
//									$_spanDisplay = '';
//									$_buttonDisplay = ' style="display:none;"';
//								}else{
//									$_filePath = '';
//									$_spanDisplay = ' style="display:none;"';
//									$_buttonDisplay = '';
//								}
//
//								$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
//								$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';
//
//                             echo '<tr>
//                               <td class="field_title">'.$kis_lang['Admission']['immunisation_record'].'</td>
//                               <td id="immunisation_record">
//                               		<span class="view_attachment" '.$_spanDisplay.'>'.$_attachment.'</span>
//									<input type="button" class="attachment_upload_btn formsmallbutton" value="'.$kis_lang['Upload'].'"  id="uploader-immunisation_record" '.$_buttonDisplay.'/>
//								</td>
//                             </tr>';
//                             }
//                             else{
	                             for($i=0;$i<sizeof($attachmentSettings);$i++) {

	                             	$attachment_name = $attachmentSettings[$i]['AttachmentName'];

	                             	$_filePath = $attachmentList[$attachment_name]['link'];
	                             	if($_filePath){
										$_spanDisplay = '';
										$_buttonDisplay = ' style="display:none;"';
									}else{
										$_filePath = '';
										$_spanDisplay = ' style="display:none;"';
										$_buttonDisplay = '';
									}

									$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
									$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';

	                             echo '<tr>
		                               	<td class="field_title">'.$attachment_name.'</td>
		                               	<td id="'.$attachment_name.'">
		                               		<span class="view_attachment" '.$_spanDisplay.'>'.$_attachment.'</span>
											<input type="button" class="attachment_upload_btn formsmallbutton" value="'.$kis_lang['Upload'].'"  id="uploader-'.$attachment_name.'" '.$_buttonDisplay.'/>
										</td>
		                             	</tr>';
	                             }
//                             }
                             ?>

                           </tbody></table>
	<?}?>
<? elseif($display=='parentinfo'):?>
	<?if($sys_custom['KIS_Admission']['YLSYK']['Settings']){?>
  							<table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <?
                              //$mustfillAry = array('chinesename','englishname','occupation','mobile');
                              $mustfillAry = array();
                              ?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang):
                              ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                               		$parentAry['chinesename'][] = '<td class="form_guardian_field">';
                                	$parentAry['chinesename'][] = $libinterface->GET_TEXTBOX('parent_name_b5', 'parent_name_b5[]',  $applicationInfo[$_pgKey]['parent_name_b5'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['chinesename'][] = $libinterface->GET_HIDDEN_INPUT('parent_id', 'parent_id[]',  $applicationInfo[$_pgKey]['RecordID']?$applicationInfo[$_pgKey]['RecordID']:'new');
                                	$parentAry['chinesename'][] = '</td>';

                                	$parentAry['englishname'][] = '<td class="form_guardian_field">';
                                	$parentAry['englishname'][] = $libinterface->GET_TEXTBOX('parent_name_en', 'parent_name_en[]',  $applicationInfo[$_pgKey]['parent_name_EN'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['englishname'][] = $libinterface->GET_HIDDEN_INPUT('parent_id', 'parent_id[]',  $applicationInfo[$_pgKey]['RecordID']?$applicationInfo[$_pgKey]['RecordID']:'new');
                                	$parentAry['englishname'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = $libinterface->GET_TEXTBOX('occupation', 'occupation[]', $applicationInfo[$_pgKey]['occupation'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['mobile'][] = '<td class="form_guardian_field">';
                                	$parentAry['mobile'][] = $libinterface->GET_TEXTBOX('mobile', 'mobile[]',  $applicationInfo[$_pgKey]['mobile'], $OtherClass='parent_'.$_pgKey, $OtherPar=array('style' => 'width:100%'));
                                	$parentAry['mobile'][] = '</td>';

                                	$parentAry['worknumber'][] = '<td class="form_guardian_field">';
                                	$parentAry['worknumber'][] = $libinterface->GET_TEXTBOX('office', 'office[]',  $applicationInfo[$_pgKey]['office_tel_no'], $OtherClass='parent_'.$_pgKey, $OtherPar=array('style' => 'width:100%'));
                                	$parentAry['worknumber'][] = '</td>';
                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=in_array($_key,$mustfillAry)?$mustfillinsymbol:''?><?=$kis_lang['Admission']['YLSYK'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings']){?>
  							<table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <?
                              $mustfillAry = array('name','mobile');
                              //$mustfillAry = array();
                              ?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang):
                              ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                                	$parentAry['name'][] = '<td class="form_guardian_field">';
                                	$parentAry['name'][] = $libinterface->GET_TEXTBOX('parent_name_b5', 'parent_name_b5[]',  $applicationInfo[$_pgKey]['parent_name_b5'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['name'][] = $libinterface->GET_HIDDEN_INPUT('parent_id', 'parent_id[]',  $applicationInfo[$_pgKey]['RecordID']?$applicationInfo[$_pgKey]['RecordID']:'new');
                                	$parentAry['name'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = $libinterface->GET_TEXTBOX('occupation', 'occupation[]', $applicationInfo[$_pgKey]['occupation'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['mobile'][] = '<td class="form_guardian_field">';
                                	$parentAry['mobile'][] = $libinterface->GET_TEXTBOX('mobile', 'mobile[]',  $applicationInfo[$_pgKey]['mobile'], $OtherClass='parent_'.$_pgKey, $OtherPar=array('style' => 'width:100%'));
                                	$parentAry['mobile'][] = '</td>';

                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=in_array($_key,$mustfillAry)?$mustfillinsymbol:''?><?=$kis_lang['Admission']['munsang'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['RMKG']['Settings']){?>
  							<table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <?
                              //$mustfillAry = array('name','mobile','gender','relationship','company','occupation','email');
                              $mustfillAry = array();
                              ?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang): ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                                	$parentAry['name'][] = '<td class="form_guardian_field">';
                                	$parentAry['name'][] = $libinterface->GET_TEXTBOX('parent_name_b5', 'parent_name_b5[]',  $applicationInfo[$_pgKey]['ChineseName'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['name'][] = $libinterface->GET_HIDDEN_INPUT('parent_id', 'parent_id[]',  $applicationInfo[$_pgKey]['RecordID']?$applicationInfo[$_pgKey]['RecordID']:'new');
                                	$parentAry['name'][] = '</td>';

                              		$parentAry['nationality'][] = '<td class="form_guardian_field">';
                                	$parentAry['nationality'][] = $libinterface->GET_TEXTBOX('nationality', 'nationality[]', $applicationInfo[$_pgKey]['Nationality'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['nationality'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = $libinterface->GET_TEXTBOX('occupation', 'occupation[]', $applicationInfo[$_pgKey]['JobTitle'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['company'][] = '<td class="form_guardian_field">';
                                	$parentAry['company'][] = $libinterface->GET_TEXTBOX('companyname', 'companyname[]', $applicationInfo[$_pgKey]['Company'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['company'][] = '</td>';

                                	$parentAry['workno'][] = '<td class="form_guardian_field">';
                               		$parentAry['workno'][] = $libinterface->GET_TEXTBOX('office', 'office[]', $applicationInfo[$_pgKey]['OfficeTelNo'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                               		$parentAry['workno'][] = '</td>';

                                	$parentAry['mobile'][] = '<td class="form_guardian_field">';
                                	$parentAry['mobile'][] = $libinterface->GET_TEXTBOX('mobile', 'mobile[]',  $applicationInfo[$_pgKey]['Mobile'], $OtherClass='parent_'.$_pgKey, $OtherPar=array('style' => 'width:100%'));
                                	$parentAry['mobile'][] = '</td>';

                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=in_array($_key,$mustfillAry)?$mustfillinsymbol:''?><?=$kis_lang['Admission']['RMKG'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']){?>
  							<table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <?
                              $mustfillAry = array('name','mobile','gender','relationship','company','occupation','email');
                              //$mustfillAry = array();
                              ?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang): ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                                	$parentAry['name'][] = '<td class="form_guardian_field">';
                                	$parentAry['name'][] = $libinterface->GET_TEXTBOX('parent_name_b5', 'parent_name_b5[]',  $applicationInfo[$_pgKey]['parent_name_b5'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['name'][] = $libinterface->GET_HIDDEN_INPUT('parent_id', 'parent_id[]',  $applicationInfo[$_pgKey]['RecordID']?$applicationInfo[$_pgKey]['RecordID']:'new');
                                	$parentAry['name'][] = '</td>';

                                	$parentAry['gender'][] = '<td class="form_guardian_field">';
                                	if($_pgKey!='G'){
                                		$parentAry['gender'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['Gender']);
                                	}
                                	else{
                                		$parentAry['gender'][] = $libinterface->Get_Radio_Button('G3Gender1', 'G3Gender', 'M', ($applicationInfo['G']['Gender']=='M'?'checked':''),'',$kis_lang['Admission']['genderType']['M'].' M').'&nbsp;&nbsp;'
																.$libinterface->Get_Radio_Button('G3Gender2', 'G3Gender', 'F', ($applicationInfo['G']['Gender']=='F'?'checked':''),'',$kis_lang['Admission']['genderType']['F'].' F');
                                	}
                                	$parentAry['gender'][] = '</td>';

                                	$parentAry['relationship'][] = '<td class="form_guardian_field">';
                                	if($_pgKey!='G'){
                                		$parentAry['relationship'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['Relationship']);
                                	}
                                	else{
                                		$parentAry['relationship'][] = '<input name="G3Relationship" type="text" id="G3Relationship" class="textboxtext" value="'.$applicationInfo['G']['Relationship'].'" />' ;
                                	}
                                	$parentAry['relationship'][] = '</td>';

                                	$parentAry['company'][] = '<td class="form_guardian_field">';
                               		$parentAry['company'][] = $libinterface->GET_TEXTBOX('office', 'office[]', $applicationInfo[$_pgKey]['OfficeTelNo'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                               		$parentAry['company'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = $libinterface->GET_TEXTBOX('occupation', 'occupation[]', $applicationInfo[$_pgKey]['occupation'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['mobile'][] = '<td class="form_guardian_field">';
                                	$parentAry['mobile'][] = $libinterface->GET_TEXTBOX('mobile', 'mobile[]',  $applicationInfo[$_pgKey]['mobile'], $OtherClass='parent_'.$_pgKey, $OtherPar=array('style' => 'width:100%'));
                                	$parentAry['mobile'][] = '</td>';

                                	$parentAry['email'][] = '<td class="form_guardian_field">';
                               		$parentAry['email'][] = $libinterface->GET_TEXTBOX('email', 'email[]',  $applicationInfo[$_pgKey]['email'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                               		$parentAry['email'][] = '</td>';

                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=in_array($_key,$mustfillAry)?$mustfillinsymbol:''?><?=$kis_lang['Admission']['CHIUCHUNKG'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['MUNSANG']['Settings']){?>
  							<table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <?
                              $mustfillAry = array('chinesename','englishname','mobile');
                              //$mustfillAry = array();
                              ?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang):
                              	if($_pgKey == 'G') continue;
                              ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                                	$parentAry['chinesename'][] = '<td class="form_guardian_field">';
                                	$parentAry['chinesename'][] = $libinterface->GET_TEXTBOX('parent_name_b5', 'parent_name_b5[]',  $applicationInfo[$_pgKey]['parent_name_b5'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['chinesename'][] = $libinterface->GET_HIDDEN_INPUT('parent_id', 'parent_id[]',  $applicationInfo[$_pgKey]['RecordID']?$applicationInfo[$_pgKey]['RecordID']:'new');
                                	$parentAry['chinesename'][] = '</td>';

									$parentAry['englishname'][] = '<td class="form_guardian_field">';
                               		$parentAry['englishname'][] = $libinterface->GET_TEXTBOX('parent_name_en', 'parent_name_en[]',  $applicationInfo[$_pgKey]['parent_name_en'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                               		$parentAry['englishname'][] = '</td>';
									
                                	$parentAry['company'][] = '<td class="form_guardian_field">';
                               		$parentAry['company'][] = $libinterface->GET_TEXTBOX('companyname', 'companyname[]', $applicationInfo[$_pgKey]['companyname'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                               		$parentAry['company'][] = '</td>';

                                	$parentAry['address'][] = '<td class="form_guardian_field">';
                               		$parentAry['address'][] = $libinterface->GET_TEXTBOX('companyaddress', 'companyaddress[]', $applicationInfo[$_pgKey]['companyaddress'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                               		$parentAry['address'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = $libinterface->GET_TEXTBOX('occupation', 'occupation[]', $applicationInfo[$_pgKey]['occupation'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['mobile'][] = '<td class="form_guardian_field">';
                                	$parentAry['mobile'][] = $libinterface->GET_TEXTBOX('mobile', 'mobile[]',  $applicationInfo[$_pgKey]['mobile'], $OtherClass='parent_'.$_pgKey, $OtherPar=array('style' => 'width:100%'));
                                	$parentAry['mobile'][] = '</td>';

                                	$parentAry['email'][] = '<td class="form_guardian_field">';
                               		$parentAry['email'][] = $libinterface->GET_TEXTBOX('email', 'email[]',  $applicationInfo[$_pgKey]['email'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                               		$parentAry['email'][] = '</td>';

                               		$parentAry['levelofeducation'][] = '<td class="form_guardian_field">';
                               		$parentAry['levelofeducation'][] = $libinterface->GET_TEXTBOX('levelofeducation', 'levelofeducation[]',  $applicationInfo[$_pgKey]['levelofeducation'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                               		$parentAry['levelofeducation'][] = '</td>';

                               		$parentAry['nameofschool'][] = '<td class="form_guardian_field">';
                               		$parentAry['nameofschool'][] = $libinterface->GET_TEXTBOX('lastschool', 'lastschool[]',  $applicationInfo[$_pgKey]['lastschool'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                               		$parentAry['nameofschool'][] = '</td>';
                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=in_array($_key,$mustfillAry)?$mustfillinsymbol:''?><?=$kis_lang['Admission']['munsang'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
  	<?} else if($sys_custom['KIS_Admission']['MGF']['Settings']){?>
		  					<table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <?
                              //$mustfillAry = array('englishname','chinesename','occupation','phoneno');
                              $mustfillAry = array();
                              ?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang):
                              		if($_pgKey == 'G') continue;
                              ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                               		$parentAry['chinesename'][] = '<td class="form_guardian_field">';
                                	$parentAry['chinesename'][] = $libinterface->GET_TEXTBOX('parent_name_b5', 'parent_name_b5[]',  $applicationInfo[$_pgKey]['parent_name_b5'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['chinesename'][] = '</td>';

                                	$parentAry['hkid'][] = '<td class="form_guardian_field">';
                               		$parentAry['hkid'][] = $libinterface->GET_TEXTBOX('hkid', 'hkid[]',  $applicationInfo[$_pgKey]['hkid'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                               		$parentAry['hkid'][] = $libinterface->GET_HIDDEN_INPUT('parent_id', 'parent_id[]',  $applicationInfo[$_pgKey]['RecordID']?$applicationInfo[$_pgKey]['RecordID']:'new');
                               		$parentAry['hkid'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = $libinterface->GET_TEXTBOX('occupation', 'occupation[]', $applicationInfo[$_pgKey]['occupation'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['mobile'][] = '<td class="form_guardian_field">';
                                	$parentAry['mobile'][] =  '<input type="text" class="parent_'.$_pgKey.'" id="mobile" name="mobile[]" value="'.$applicationInfo[$_pgKey]['mobile'].'">';
                                	$parentAry['mobile'][] = '</td>';
                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=in_array($_key,$mustfillAry)?$mustfillinsymbol:''?><?=$kis_lang['Admission']['mgf'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['CSM']['Settings']){?>
							<table class="form_table">
                             <tr>
                               <td><?=$mustfillinsymbol?><?=$kis_lang['Admission']['csm']['relationshipBetweenChild']?></td>
                              <? $parentAry = array();?>
                              <?
                              $mustfillAry = array('parantenglishname','parantchinesename','parentlivewithchild','singleparent','IsFamilySpecialCase','email','mobile');
                              //$mustfillAry = array();
                              ?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang): ?>
                               <td class="form_guardian_head" width="28%"><?=$libinterface->Get_Radio_Button('StudentRelationship'.$_pgKey, 'StudentRelationship', $_pgKey, $applicationInfo['F']['relationship'] == $_pgKey,'',$_pgLang)?><?=($_pgKey == 'G'?' <input name="G3Relationship" type="text" id="G3Relationship" class="textboxtext" style="width:100px;" value="'.$applicationInfo[$_pgKey]['relationship'].'" />':'')?></td>
                               <? //Prepare TD field
                                	$parentAry['parantenglishname'][] = '<td class="form_guardian_field">';
                               		$parentAry['parantenglishname'][] = $libinterface->GET_TEXTBOX('parent_name_en', 'parent_name_en[]',  $applicationInfo[$_pgKey]['parent_name_en'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                               		$parentAry['parantenglishname'][] = $libinterface->GET_HIDDEN_INPUT('parent_id', 'parent_id[]',  $applicationInfo[$_pgKey]['RecordID']?$applicationInfo[$_pgKey]['RecordID']:'new');
                               		$parentAry['parantenglishname'][] = '</td>';

                               		$parentAry['parantchinesename'][] = '<td class="form_guardian_field">';
                                	$parentAry['parantchinesename'][] = $libinterface->GET_TEXTBOX('parent_name_b5', 'parent_name_b5[]',  $applicationInfo[$_pgKey]['parent_name_b5'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['parantchinesename'][] = '</td>';

                               		$parentAry['parentlivewithchild'][] = '<td class="form_guardian_field">';
                                	if($_pgKey != 'G')
                                		$parentAry['parentlivewithchild'][] = $libinterface->Get_Checkbox('LiveWithChild'.$_pgKey.'1', 'LiveWithChild'.$_pgKey, 'Y', $applicationInfo[$_pgKey]['IsLiveWithChild']=='Y','',$kis_lang['Admission']['yes'], "document.getElementById('LiveWithChild".$_pgKey."2').checked = false;").' '.$libinterface->Get_Checkbox('LiveWithChild'.$_pgKey.'2', 'LiveWithChild'.$_pgKey, 'N', $applicationInfo[$_pgKey]['IsLiveWithChild']=='N','',$kis_lang['Admission']['no'], "document.getElementById('LiveWithChild".$_pgKey."1').checked = false;");
                                	else
                                		$parentAry['parentlivewithchild'][] = '--';
                                	$parentAry['parentlivewithchild'][] = '</td>';

                                	$parentAry['mobile'][] = '<td class="form_guardian_field">'.$libinterface->Get_Radio_Button('StudentContactPerson'.$_pgKey, 'StudentContactPerson', $_pgKey, $studentApplicationInfo['ContactPersonRelationship'] == $_pgKey).' ';
                                	$parentAry['mobile'][] = $libinterface->GET_TEXTBOX('mobile', 'mobile[]',  $applicationInfo[$_pgKey]['mobile'], $OtherClass='parent_'.$_pgKey, $OtherPar=array('style' => 'width:80%'));
                                	$parentAry['mobile'][] = '</td>';

                               		if($_pgKey == 'F'){
                               			$parentAry['email'][] = '<td class="form_guardian_field" colspan="3">';
	                                	$parentAry['email'][] = $libinterface->GET_TEXTBOX('email', 'email[]',  $applicationInfo[$_pgKey]['email'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
	                                	$parentAry['email'][] = '</td>';

	                                	$parentAry['singleparent'][] = '<td class="form_guardian_field" colspan="3">';
	                                	$parentAry['singleparent'][] = $libinterface->Get_Radio_Button('IsSingleParent1', 'IsSingleParent[]', 'Y', $applicationInfo[$_pgKey]['lsSingleParents']=='Y','',$kis_lang['Admission']['yes']).' '.$libinterface->Get_Radio_Button('IsSingleParent2', 'IsSingleParent[]', 'N', $applicationInfo[$_pgKey]['lsSingleParents']=='N','',$kis_lang['Admission']['no']).' '.$libinterface->Get_Radio_Button('IsSingleParent3', 'IsSingleParent[]', '', ($applicationInfo[$_pgKey]['lsSingleParents']!='Y' && $applicationInfo[$_pgKey]['lsSingleParents']!='N'),'',$kis_lang['Admission']['na']);
										$parentAry['singleparent'][] ='<br/> ( '.$kis_lang['Admission']['csm']['IsApplyFullDayCare'].' '.$libinterface->Get_Radio_Button('IsApplyFullDayCare1', 'IsApplyFullDayCare', 'Y', $applicationInfo[$_pgKey]['IsApplyFullDayCare']=='Y','',$kis_lang['Admission']['agree']).' '.$libinterface->Get_Radio_Button('IsApplyFullDayCare3', 'IsApplyFullDayCare', 'N', $applicationInfo[$_pgKey]['IsApplyFullDayCare']=='N','',$kis_lang['Admission']['disagree']).')';
	                                	$parentAry['singleparent'][] = '</td>';

//	                                	$parentAry['IsFamilySpecialCase'][] = '<td class="form_guardian_field" colspan="3">';
//	                                	$parentAry['IsFamilySpecialCase'][] = $libinterface->Get_Radio_Button('IsFamilySpecialCase1', 'IsFamilySpecialCase', 'Y', $applicationInfo[$_pgKey]['IsFamilySpecialCase']=='Y','',$kis_lang['Admission']['yes']).' '.$libinterface->Get_Radio_Button('IsFamilySpecialCase2', 'IsFamilySpecialCase', 'N', $applicationInfo[$_pgKey]['IsFamilySpecialCase']=='N','',$kis_lang['Admission']['no']);
//										$parentAry['IsFamilySpecialCase'][] =' ( '.$kis_lang['Admission']['csm']['IsApplyFullDayCare'].' '.$libinterface->Get_Radio_Button('IsApplyFullDayCare1', 'IsApplyFullDayCare', 'Y', $applicationInfo[$_pgKey]['IsApplyFullDayCare']=='Y','',$kis_lang['Admission']['yes'],'$(\'#IsApplyFullDayCare1_temp\').prop(\'checked\',true)').' '.$libinterface->Get_Radio_Button('IsApplyFullDayCare2', 'IsApplyFullDayCare', 'N', $applicationInfo[$_pgKey]['IsApplyFullDayCare']=='N','',$kis_lang['Admission']['no'],'$(\'#IsApplyFullDayCare2_temp\').prop(\'checked\',true)').')';
//	                                	$parentAry['IsFamilySpecialCase'][] = '</td>';
                                	}
                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=in_array($_key,$mustfillAry)?$mustfillinsymbol:''?><?=$kis_lang['Admission']['csm'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
    <?}else if($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings']){?>
                           <table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <?
                              //$mustfillAry = array('englishname','chinesename','occupation','phoneno');
                              $mustfillAry = array();
                              ?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang): ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                               		$parentAry['chinesename'][] = '<td class="form_guardian_field">';
                                	$parentAry['chinesename'][] = $libinterface->GET_TEXTBOX('parent_name_b5', 'parent_name_b5[]',  $applicationInfo[$_pgKey]['ChineseName'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['chinesename'][] = $libinterface->GET_HIDDEN_INPUT('parent_id', 'parent_id[]',  $applicationInfo[$_pgKey]['RecordID']?$applicationInfo[$_pgKey]['RecordID']:'new');
                                	$parentAry['chinesename'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = $libinterface->GET_TEXTBOX('occupation', 'occupation[]', $applicationInfo[$_pgKey]['JobTitle'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['companyname'][] = '<td class="form_guardian_field">';
                                	$parentAry['companyname'][] = $libinterface->GET_TEXTBOX('companyname', 'companyname[]', $applicationInfo[$_pgKey]['Company'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['companyname'][] = '</td>';

                                	$parentAry['phoneno'][] = '<td class="form_guardian_field">';
                                	$parentAry['phoneno'][] = '(852) ' . $libinterface->GET_TEXTBOX('mobile', 'mobile[]', $applicationInfo[$_pgKey]['Mobile'], $OtherClass='parent_'.$_pgKey, $OtherPar=array('style' => 'width: calc(99% - 50px);'));
                                	$parentAry['phoneno'][] = '</td>';
                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=in_array($_key,$mustfillAry)?$mustfillinsymbol:''?><?=$kis_lang['Admission'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
    <?}else if(file_exists(dirname(__FILE__) . "/{$setting_path_ip_rel}/parentinfo_edit.php")){
		include(dirname(__FILE__) . "/{$setting_path_ip_rel}/parentinfo_edit.php");
	}else{?>
                           <table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <?
                              //$mustfillAry = array('englishname','chinesename','occupation','phoneno');
                              $mustfillAry = array();
                              ?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang): ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                               		$parentAry['chinesename'][] = '<td class="form_guardian_field">';
                                	$parentAry['chinesename'][] = $libinterface->GET_TEXTBOX('parent_name_b5', 'parent_name_b5[]',  $applicationInfo[$_pgKey]['parent_name_b5'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['chinesename'][] = '</td>';

                                	$parentAry['englishname'][] = '<td class="form_guardian_field">';
                               		$parentAry['englishname'][] = $libinterface->GET_TEXTBOX('parent_name_en', 'parent_name_en[]',  $applicationInfo[$_pgKey]['parent_name_en'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                               		$parentAry['englishname'][] = $libinterface->GET_HIDDEN_INPUT('parent_id', 'parent_id[]',  $applicationInfo[$_pgKey]['RecordID']?$applicationInfo[$_pgKey]['RecordID']:'new');
                               		$parentAry['englishname'][] = '</td>';

                               		$parentAry['relationship'][] = '<td class="form_guardian_field">';
                                	$parentAry['relationship'][] = ($_pgKey=='G')?$libinterface->GET_TEXTBOX('relationship', 'relationship', $applicationInfo[$_pgKey]['relationship'], $OtherClass='parent_'.$_pgKey, $OtherPar=array()):'';
                                	$parentAry['relationship'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = $libinterface->GET_TEXTBOX('occupation', 'occupation[]', $applicationInfo[$_pgKey]['occupation'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['companyname'][] = '<td class="form_guardian_field">';
                                	$parentAry['companyname'][] = $libinterface->GET_TEXTBOX('companyname', 'companyname[]', $applicationInfo[$_pgKey]['companyname'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['companyname'][] = '</td>';

                                	$parentAry['jobposition'][] = '<td class="form_guardian_field">';
                                	$parentAry['jobposition'][] = $libinterface->GET_TEXTBOX('jobposition', 'jobposition[]', $applicationInfo[$_pgKey]['jobposition'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['jobposition'][] = '</td>';

                                	$parentAry['companyaddress'][] = '<td class="form_guardian_field">';
                                	$parentAry['companyaddress'][] = $libinterface->GET_TEXTBOX('companyaddress', 'companyaddress[]', $applicationInfo[$_pgKey]['companyaddress'], $OtherClass='parent_'.$_pgKey, $OtherPar=array());
                                	$parentAry['companyaddress'][] = '</td>';

                                	$parentAry['phoneno'][] = '<td class="form_guardian_field">';
                                	$_phone = '<table style="width:auto;">';
                                	$_phone .= '<tr><td>'.$kis_lang['Admission']['office'].' : </td><td><input type="text" class="parent_'.$_pgKey.'" id="office" name="office[]" value="'.$applicationInfo[$_pgKey]['companyphone'].'"></td></tr>';
                                	$_phone .= '<tr><td>'.$kis_lang['Admission']['mobile'].' : </td><td><input type="text" class="parent_'.$_pgKey.'" id="mobile" name="mobile[]" value="'.$applicationInfo[$_pgKey]['mobile'].'"></td></tr>';
                                	$_phone .= '</table>';
                                	$parentAry['phoneno'][] =  $_phone;
                                	$parentAry['phoneno'][] = '</td>';
                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=in_array($_key,$mustfillAry)?$mustfillinsymbol:''?><?=$kis_lang['Admission'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
	<?}?>
<? elseif($display=='otherinfo'):?>
	<?if($sys_custom['KIS_Admission']['YLSYK']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=$classLevel?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['csm']['applyDayType']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                              		<?foreach($applyTimeSlotAry as $_key => $_value):?>
                              				<?$_key++;?>
                       						<td>(<?=$kis_lang['Admission']['Option']?> <?=$_key?>)</td>
                       						<td><?=$libinterface->GET_SELECTION_BOX($applyTimeSlotAry, "name='ApplyDayType[]' id='ApplyDayType".$_key."' class='timeslotselection'",$kis_lang['Admission']['Nil'], $applicationInfo['ApplyDayType'.$_key])?></td>
                       				<?endforeach;?>
                       					</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['YLSYK']['BriefingApplyInfo']?></td>
                               <td width="70%"><?=$libinterface->GET_TEXTBOX('OthersBriefingApplicationNumber', 'OthersBriefingApplicationNumber', $applicationInfo['BriefingApplicationNo'])?></td>
                             </tr>
                           </tbody></table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['YLSYK']['RefBroSisInfo']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['name']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant']?></td>
                             </tr>
                             <? for($i=0; $i< 1;$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRefRelativeStudiedName', 'OthersRefRelativeStudiedName[]',  $studentApplicationRelativesInfoCustRef[$i]['OthersRelativeStudiedName'])?>
                             	  <?=$libinterface->GET_HIDDEN_INPUT('student_ref_cust_id', 'student_ref_cust_id[]',  $studentApplicationRelativesInfoCustRef[$i]['RecordID']?$studentApplicationRelativesInfoCustRef[$i]['RecordID']:'new');?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRefRelativeRelationship', 'OthersRefRelativeRelationship[]',  $studentApplicationRelativesInfoCustRef[$i]['OthersRelativeRelationship'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['YLSYK']['ExBroSisInfo']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['name']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['TSUENWANBCKG']['graduateYear']?></td>
                             </tr>
                             <? for($i=0; $i< 2;$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersExRelativeStudiedName', 'OthersExRelativeStudiedName[]',  $studentApplicationRelativesInfoCustEx[$i]['OthersRelativeStudiedName'])?>
                             	  <?=$libinterface->GET_HIDDEN_INPUT('student_ex_cust_id', 'student_ex_cust_id[]',  $studentApplicationRelativesInfoCustEx[$i]['RecordID']?$studentApplicationRelativesInfoCustEx[$i]['RecordID']:'new');?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersExRelativeRelationship', 'OthersExRelativeRelationship[]',  $studentApplicationRelativesInfoCustEx[$i]['OthersRelativeRelationship'])?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersExRelativeStudiedYear', 'OthersExRelativeStudiedYear[]',  $studentApplicationRelativesInfoCustEx[$i]['OthersRelativeStudiedYear'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['YLSYK']['CurBroSisInfo']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['name']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['YLSYK']['currentclass']?></td>
                             </tr>
                             <? for($i=0; $i< 2;$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersCurRelativeStudiedName', 'OthersCurRelativeStudiedName[]',  $studentApplicationRelativesInfoCustCur[$i]['OthersRelativeStudiedName'])?>
                             	  <?=$libinterface->GET_HIDDEN_INPUT('student_cur_cust_id', 'student_cur_cust_id[]',  $studentApplicationRelativesInfoCustCur[$i]['RecordID']?$studentApplicationRelativesInfoCustCur[$i]['RecordID']:'new');?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersCurRelativeRelationship', 'OthersCurRelativeRelationship[]',  $studentApplicationRelativesInfoCustCur[$i]['OthersRelativeRelationship'])?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersCurRelativeStudiedYear', 'OthersCurRelativeStudiedYear[]',  $studentApplicationRelativesInfoCustCur[$i]['OthersRelativeStudiedYear'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['YLSYK']['BroSisApplyInfo']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['name']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant']?></td>
                               <td class="form_guardian_head" width="10%"><?=$kis_lang['Admission']['YLSYK']['ApplyClass']?></td>
                               <td class="form_guardian_head" width="10%"><?=$kis_lang['Admission']['YLSYK']['birthcertno']?></td>
                             </tr>
                             <? for($i=0; $i< 2;$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRelativeApplyName', 'OthersRelativeApplyName[]',  $studentApplicationRelativesInfoCustApply[$i]['OthersRelativeStudiedName'])?>
                             	  <?=$libinterface->GET_HIDDEN_INPUT('student_apply_cust_id', 'student_apply_cust_id[]',  $studentApplicationRelativesInfoCustApply[$i]['RecordID']?$studentApplicationRelativesInfoCustApply[$i]['RecordID']:'new');?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRelativeApplyRelationship', 'OthersRelativeApplyRelationship[]',  $studentApplicationRelativesInfoCustApply[$i]['OthersRelativeRelationship'])?></td>
                             	  <td class="form_guardian_field"><?=$libkis_admission->getClassLevelSelection($studentApplicationRelativesInfoCustApply[$i]['OthersRelativeClassPosition'],'OthersRelativeApplyClass[]',true)?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRelativeApplyBirthCertNo', 'OthersRelativeApplyBirthCertNo[]',  $studentApplicationRelativesInfoCustApply[$i]['OthersRelativeBirthCertNo'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=$classLevel?></td>
                             </tr>
                           </tbody></table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['TSUENWANBCKG']['currentBroSisInfo']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['name']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['TSUENWANBCKG']['currentClass']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['TSUENWANBCKG']['graduateYear']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['relationship']?></td>
                             </tr>
                             <? for($i=0; $i< 2;$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRelativeStudiedName', 'OthersRelativeStudiedName[]',  $studentApplicationRelativesInfo[$i]['OthersRelativeStudiedName'])?>
                             	  <?=$libinterface->GET_HIDDEN_INPUT('student_cust_id', 'student_cust_id[]',  $studentApplicationRelativesInfo[$i]['RecordID']?$studentApplicationRelativesInfo[$i]['RecordID']:'new');?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRelativeClassPosition', 'OthersRelativeClassPosition[]',  $studentApplicationRelativesInfo[$i]['OthersRelativeClassPosition'])?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRelativeStudiedYear', 'OthersRelativeStudiedYear[]',  $studentApplicationRelativesInfo[$i]['OthersRelativeStudiedYear'])?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRelativeRelationship', 'OthersRelativeRelationship[]',  $studentApplicationRelativesInfo[$i]['OthersRelativeRelationship'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['RMKG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=$classLevel?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['applyDayType']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                              		<?foreach($applyTimeSlotAry as $_key => $_value):?>
                              				<?$_key++;?>
                       						<td>(<?=$kis_lang['Admission']['Option']?> <?=$_key?>)</td>
                       						<td><?=$libinterface->GET_SELECTION_BOX($applyTimeSlotAry, "name='ApplyDayType[]' id='ApplyDayType".$_key."' class='timeslotselection'",$kis_lang['Admission']['Nil'], $applicationInfo['ApplyDayType'.$_key])?></td>
                       				<?endforeach;?>
                       					</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['munsang']['IsConsiderAlternative']?></td>
                               <td><?=$libinterface->Get_Radio_Button('OthersIsConsiderAlternative_y', 'OthersIsConsiderAlternative', 'Y', ($applicationInfo['OthersIsConsiderAlternative']=='Y'), '', $kis_lang['yes'])?>
                               <?=$libinterface->Get_Radio_Button('OthersIsConsiderAlternative_n', 'OthersIsConsiderAlternative', 'N', ($applicationInfo['OthersIsConsiderAlternative']=='N'), '', $kis_lang['no'])?></td>
                             </tr>
                           </tbody></table>
                           <table class="form_table">
							<tr>
									<td></td>
									<td class="form_guardian_head"><center><?=$kis_lang['Admission']['RMKG']['1stlang']?></center></td>
									<td class="form_guardian_head"><center><?=$kis_lang['Admission']['RMKG']['2ndlang']?></center></td>
									<td class="form_guardian_head"><center><?=$kis_lang['Admission']['RMKG']['3rdlang']?></center></td>
								</tr>
								<tr>
									<td class="field_title"><?=$kis_lang['Admission']['RMKG']['SpokenLang']?></td>
									<? $StuLangArr = explode('|||',$applicationInfo['NativeLanguage']); ?>
									<td class="form_guardian_field"><center><?=$lauc->getSelectByConfig($admission_cfg['lang'],'StuMotherTongue',$StuLangArr[0])?></center></td>
									<td class="form_guardian_field"><center><?=$lauc->getSelectByConfig($admission_cfg['lang'],'StuSecondTongue',$StuLangArr[1])?></center></td>
									<td class="form_guardian_field"><center><?=$lauc->getSelectByConfig($admission_cfg['lang'],'StuThirdTongue',$StuLangArr[2])?></center></td>
								</tr>
							<tr>
									<td class="field_title"><?=$kis_lang['Admission']['RMKG']['KinderU']?></td>
									<td><center>
										<?=$lauc->Get_Radio_Button('IsKinderU1', 'IsKinderU', '1', ($applicationInfo['CustQ1']==1?'checked':''),'',$kis_lang['Admission']['yes']).'&nbsp;&nbsp;'
							  			.$lauc->Get_Radio_Button('IsKinderU2', 'IsKinderU', '0', ($applicationInfo['CustQ1']==0?'checked':''),'',$kis_lang['Admission']['no'])?>
							  		</center></td>
								</tr>
							</table>

                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['munsang']['RelativeStudiedWorkedAtCollege']?></td>
                               <td class="form_guardian_head" width="15%"><?=$kis_lang['Admission']['RMKG']['name']?></td>
                               <td class="form_guardian_head" width="15%"><?=$kis_lang['Admission']['RMKG']['age']?></td>
                               <td class="form_guardian_head" width="15%"><?=$kis_lang['Admission']['RMKG']['sex']?></td>
                               <td class="form_guardian_head" width="15%"><?=$kis_lang['Admission']['RMKG']['class']?></td>
                               <td class="form_guardian_head" width="15%"><?=$kis_lang['Admission']['RMKG']['schoolyear']?></td>
                             </tr>
                             <? for($i=0; $i< 4/*count($studentApplicationRelativesInfo)*/;$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <?=$libinterface->GET_HIDDEN_INPUT('relatives_info_id', 'relatives_info_id['.$i.']',  $studentApplicationRelativesInfo[$i]['RecordID']?$studentApplicationRelativesInfo[$i]['RecordID']:'new');?></td>
                             	  <td class="form_guardian_field"><center><input name="OthersRelativeStudiedName[<?=$i?>]" type="text" id="OthersRelativeStudiedName" class="textboxtext" value="<?=$studentApplicationRelativesInfo[$i]['Name']?>" /></center></td>
                             	  <td class="form_guardian_field"><center><input name="OthersRelativeAge[<?=$i?>]" type="text" id="OthersRelativeAge" class="textboxtext" value="<?=$studentApplicationRelativesInfo[$i]['Age']?>"/></center></td>
                             	  <td class="form_guardian_field"><center>
                             	  	<?=$lauc->Get_Radio_Button('OthersRelativeGender'.$i.'_M', 'OthersRelativeGender['.$i.']', 'M', ($studentApplicationRelativesInfo[$i]['Gender']=='M'?'checked' :''),'',$kis_lang['Admission']['genderType']['M']).'&nbsp;&nbsp;'
									.$lauc->Get_Radio_Button('OthersRelativeGender'.$i.'_F', 'OthersRelativeGender['.$i.']', 'F', ($studentApplicationRelativesInfo[$i]['Gender']=='F'?'checked' :''),'',$kis_lang['Admission']['genderType']['F'])?>
								  </center></td>
                             	  <td class="form_guardian_field"><center><?=$lauc->getPrevClassSelection('OthersRelativeClass['.$i.']',$studentApplicationRelativesInfo[$i]['ClassPosition'])?></center></td>
                             	  <td class="form_guardian_field"><center><?=$lauc->getSchoolYearSelect('OthersRelativeStudiedYear['.$i.']',$studentApplicationRelativesInfo[$i]['Year'])?></center></td>
                             </tr>
                             <? endfor;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=$classLevel?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['applyDayType']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                              		<?foreach($applyTimeSlotAry as $_key => $_value):?>
                              				<?$_key++;?>
                       						<td>(<?=$kis_lang['Admission']['Option']?> <?=$_key?>)</td>
                       						<td><?=$libinterface->GET_SELECTION_BOX($applyTimeSlotAry, "name='ApplyDayType[]' id='ApplyDayType".$_key."' class='timeslotselection'",$kis_lang['Admission']['Nil'], $applicationInfo['ApplyDayType'.$_key])?></td>
                       				<?endforeach;?>
                       					</tr>
                               		</table>
								</td>
                             </tr>
                           </tbody></table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['CHIUCHUNKG']['CurrentSchoolInfo']?></td>
                               <td class="form_guardian_head" width="28%"><?=$kis_lang['Admission']['munsang']['NameOfSchool']?></td>
                               <td class="form_guardian_head" width="26%"><?=$kis_lang['Admission']['munsang']['Class']?></td>
                               <td class="form_guardian_head" width="26%"><?=$kis_lang['Admission']['munsang']['address']?></td>
                             </tr>
                             <? for($i=0; $i< 1/*count($studentApplicationInfoCust)*/;$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersPrevSchName', 'OthersPrevSchName[]',  $studentApplicationInfoCust[$i]['OthersPrevSchName'])?></td>
                             	  <td class="form_guardian_field">
                             	  	<input name="OthersPrevSchClass[]" type="text" id="OthersPrevSchClass" value="<?=$studentApplicationInfoCust[$i]['OthersPrevSchClass']?>">
                             	  	<?=$lauc->getSelectByConfig($admission_cfg['classtype2'],'ClassType',$studentApplicationInfoCust[$i]['ClassType']) ?>
                             	  </td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('SchoolAddress', 'SchoolAddress[]',  $studentApplicationInfoCust[$i]['SchoolAddress'])?>
                             	  <?=$libinterface->GET_HIDDEN_INPUT('student_cust_id', 'student_cust_id[]',  $studentApplicationInfoCust[$i]['RecordID']?$studentApplicationInfoCust[$i]['RecordID']:'new');?></td>
                             </tr>
                             <? endfor;?>
                           </table>
                           <table class="form_table">
                             <tr>
                               <td colspan="2"><?=$kis_lang['Admission']['CHIUCHUNKG']['Family']?></td>
                             </tr>
                             <tr>
	                             <td width="30%" class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['B']?></td>
	                             <td><input name="NumElderBro" type="text" id="NumElderBro" class="textboxtext" maxlength="1" style="width:20px" value="<?=$applicationInfo['EBrotherNo']?>"></td>
                             </tr>
                             <tr>
	                             <td width="30%" class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['S']?></td>
	                             <td><input name="NumElderSis" type="text" id="NumElderSis" class="textboxtext" maxlength="1" style="width:20px" value="<?=$applicationInfo['ESisterNo']?>"></td>
                             </tr>
                             <tr>
	                             <td width="30%" class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['YB']?></td>
	                             <td><input name="NumYoungBro" type="text" id="NumYoungBro" class="textboxtext" maxlength="1" style="width:20px" value="<?=$applicationInfo['YBrotherNo']?>"></td>
	                         </tr>
                             <tr>
	                             <td width="30%" class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['YS']?></td>
	                             <td><input name="NumYoungSis" type="text" id="NumYoungSis" class="textboxtext" maxlength="1" style="width:20px" value="<?=$applicationInfo['YSisterNo']?>"></td>
	                         </tr>
                             <!--tr>
	                             <td width="30%" class="field_title"><//?=$kis_lang['Admission']['CHIUCHUNKG']['RankInFamily']?></td>
	                             <td><//?=($applicationInfo['EBrotherNo']+$applicationInfo['ESisterNo'])?></td>
	                         </tr-->
                             <tr>
	                             <td width="30%" class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['Twins']?></td>
	                             <td><input type="checkbox" id="IsTwins" name="IsTwins" value="1" <?=($applicationStuInfo['IsTwins']==1?'checked=1':'')?>"></td>
	                         </tr>
	                         <tr>
                             	<td width="30%" class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['TwinsID']?></td>
                             	<td><input type ="text" id="TwinsIDNo" name="TwinsIDNo"' value="<?=$applicationStuInfo['TwinsIDNo']?>"/></td>
                             </tr>
                           </table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['munsang']['RelativeStudiedWorkedAtCollege']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['munsang']['name']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['munsang']['RelationshipWithApplicant']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['munsang']['Year']?></td>
                             </tr>
                             <? for($i=0; $i< 4/*count($studentApplicationRelativesInfo)*/;$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <?=$libinterface->GET_HIDDEN_INPUT('relatives_info_id', 'relatives_info_id[]',  $studentApplicationRelativesInfo[$i]['RecordID']?$studentApplicationRelativesInfo[$i]['RecordID']:'new');?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRelativeStudiedName', 'OthersRelativeStudiedName[]',  $studentApplicationRelativesInfo[$i]['OthersRelativeStudiedName'])?></td>
                             	  <td class="form_guardian_field"><?=$lauc->getSelectByConfig($admission_cfg['relation'],'OthersRelativeRelationship[]',$studentApplicationRelativesInfo[$i]['OthersRelativeRelationship'])?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRelativeStudiedYear', 'OthersRelativeStudiedYear[]',  $studentApplicationRelativesInfo[$i]['OthersRelativeStudiedYear'])?>
                             </tr>
                             <? endfor;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['MUNSANG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=$classLevel?></td>
                             </tr>
                              <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['munsang']['applyDayType']?></td>
                               <td width="70%">
                               <?php
                               $applyDayType[] = array('1',$kis_lang['Admission']['TimeSlot'][1]);
                               $applyDayType[] = array('2',$kis_lang['Admission']['TimeSlot'][2]);
                               ?>
                               <?=$libinterface->GET_SELECTION_BOX($applyDayType, 'name = \'DayTypeOption\'','',$applicationInfo['ApplyDayType1']) ?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['munsang']['IsConsiderAlternative']?></td>
                               <td><?=$libinterface->Get_Radio_Button('OthersIsConsiderAlternative_y', 'OthersIsConsiderAlternative', 'Y', ($applicationInfo['OthersIsConsiderAlternative']=='Y'), '', $kis_lang['yes'])?>
                               <?=$libinterface->Get_Radio_Button('OthersIsConsiderAlternative_n', 'OthersIsConsiderAlternative', 'N', ($applicationInfo['OthersIsConsiderAlternative']=='N'), '', $kis_lang['no'])?></td>
                             </tr>
                           </tbody></table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['munsang']['PrevSchRecord']?></td>
                               <td class="form_guardian_head" width="26%"><?=$kis_lang['Admission']['munsang']['Year']?></td>
                               <td class="form_guardian_head" width="26%"><?=$kis_lang['Admission']['munsang']['Class']?></td>
                               <td class="form_guardian_head" width="28%"><?=$kis_lang['Admission']['munsang']['NameOfSchool']?></td>
                             </tr>
                             <? for($i=0; $i< 5/*count($studentApplicationInfoCust)*/;$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersPrevSchYear', 'OthersPrevSchYear[]',  $studentApplicationInfoCust[$i]['OthersPrevSchYear'])?>
                             	  <?=$libinterface->GET_HIDDEN_INPUT('student_cust_id', 'student_cust_id[]',  $studentApplicationInfoCust[$i]['RecordID']?$studentApplicationInfoCust[$i]['RecordID']:'new');?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersPrevSchClass', 'OthersPrevSchClass[]',  $studentApplicationInfoCust[$i]['OthersPrevSchClass'])?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersPrevSchName', 'OthersPrevSchName[]',  $studentApplicationInfoCust[$i]['OthersPrevSchName'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['munsang']['RelativeStudiedWorkedAtCollege']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['munsang']['Year']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['munsang']['name']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['munsang']['ClassPosition']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['munsang']['RelationshipWithApplicant']?></td>
                             </tr>
                             <? for($i=0; $i< 5/*count($studentApplicationRelativesInfo)*/;$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRelativeStudiedYear', 'OthersRelativeStudiedYear[]',  $studentApplicationRelativesInfo[$i]['OthersRelativeStudiedYear'])?>
                             	  <?=$libinterface->GET_HIDDEN_INPUT('relatives_info_id', 'relatives_info_id[]',  $studentApplicationRelativesInfo[$i]['RecordID']?$studentApplicationRelativesInfo[$i]['RecordID']:'new');?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRelativeStudiedName', 'OthersRelativeStudiedName[]',  $studentApplicationRelativesInfo[$i]['OthersRelativeStudiedName'])?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRelativeClassPosition', 'OthersRelativeClassPosition[]',  $studentApplicationRelativesInfo[$i]['OthersRelativeClassPosition'])?></td>
                             	  <td class="form_guardian_field"><?=$libinterface->GET_TEXTBOX('OthersRelativeRelationship', 'OthersRelativeRelationship[]',  $studentApplicationRelativesInfo[$i]['OthersRelativeRelationship'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
	<?} else if($sys_custom['KIS_Admission']['MGF']['Settings']){?>
						<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=$classLevel?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['applyDayType']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                              		<?foreach($applyTimeSlotAry as $_key => $_value):?>
                              				<?$_key++;?>
                       						<td>(<?=$kis_lang['Admission']['Option']?> <?=$_key?>)</td>
                       						<td><?=$libinterface->GET_SELECTION_BOX($applyTimeSlotAry, "name='ApplyDayType[]' id='ApplyDayType".$_key."' class='timeslotselection'",$kis_lang['Admission']['Nil'], $applicationInfo['ApplyDayType'.$_key])?></td>
                       				<?endforeach;?>
                       					</tr>
                               		</table>
								</td>
                             </tr>
                             <?
                           	 $OtherPar = array();
                           	 $OtherPar['style'] = 'width:200px';
                           	 ?>
                           	 <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['curbsname']?></td>
                               <td>(<?=$kis_lang['Admission']['name']?>) <?=$libinterface->GET_TEXTBOX('OthersCurBSName', 'OthersCurBSName',  $applicationInfo['CurBSName'], $OtherClass='', $OtherPar)?> (<?=$kis_lang['Admission']['class']?>) <?=$libinterface->GET_TEXTBOX('OthersCurBSLevel', 'OthersCurBSLevel',  $applicationInfo['CurBSLevel'], $OtherClass='', $OtherPar)?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['hassiblingapplied']?></td>
                               <td>
                               	<?=$libinterface->Get_Radio_Button('OthersSiblingApplied1', 'OthersSiblingApplied', 'yes', ($studentApplicationInfo['IsTwinsApplied'])?'1':'0','',$kis_lang['Admission']['yes'],'$(\'#div_OthersSiblingAppliedName\').css(\'display\', \'\');') ?>
                               	<?=$libinterface->Get_Radio_Button('OthersSiblingApplied2', 'OthersSiblingApplied', 'no', ($studentApplicationInfo['IsTwinsApplied'])?'0':'1','',$kis_lang['Admission']['no'],'$(\'#div_OthersSiblingAppliedName\').css(\'display\', \'none\');') ?>
                               	<div id="div_OthersSiblingAppliedName" style="<?=($studentApplicationInfo['IsTwinsApplied'])?'':'display:none' ?>">
                                   	<?="({$kis_lang['Admission']['name']})" ?>
                                   	<?=$libinterface->GET_TEXTBOX('OthersSiblingAppliedName', 'OthersSiblingAppliedName',  $applicationInfo['SiblingAppliedName'], $OtherClass='', $OtherPar) ?>
                                   	<?="({$kis_lang['Admission']['HKUGAPS']['twinsID']})" ?>
                                   	<?=$libinterface->GET_TEXTBOX('twinsapplicationid', 'twinsapplicationid',  $studentApplicationInfo['SiblingAppliedID'], $OtherClass='', $OtherPar) ?>
                               	</div>
                               </td>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['issiblinggradhere']?></td>
                               <td>
                               	<?='('.$kis_lang['Admission']['name'].') '.$libinterface->GET_TEXTBOX('OthersExBSName', 'OthersExBSName',  $applicationInfo['ExBSName'], $OtherClass='', $OtherPar).' ('.$kis_lang['Admission']['GradYear'].'：'.$libinterface->GET_TEXTBOX('OthersExBSGradYear', 'OthersExBSGradYear',  $applicationInfo['ExBSGradYear'], $OtherClass='', $OtherPar).')<br/>'?>
                               	<?='('.$kis_lang['Admission']['name'].') '.$libinterface->GET_TEXTBOX('OthersExBSName2', 'OthersExBSName2',  $applicationInfo['ExBSName2'], $OtherClass='', $OtherPar).' ('.$kis_lang['Admission']['GradYear'].'：'.$libinterface->GET_TEXTBOX('OthersExBSGradYear2', 'OthersExBSGradYear2',  $applicationInfo['ExBSGradYear2'], $OtherClass='', $OtherPar).')<br/>'?>
                               	<?='('.$kis_lang['Admission']['name'].') '.$libinterface->GET_TEXTBOX('OthersExBSName3', 'OthersExBSName3',  $applicationInfo['ExBSName3'], $OtherClass='', $OtherPar).' ('.$kis_lang['Admission']['GradYear'].'：'.$libinterface->GET_TEXTBOX('OthersExBSGradYear3', 'OthersExBSGradYear3',  $applicationInfo['ExBSGradYear3'], $OtherClass='', $OtherPar).')'?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['teacherOrManagerChildren']?></td>
                               <td>

								<?=$libinterface->Get_Radio_Button('teacherOrManagerChildren1', 'teacherOrManagerChildren', 'yes', ($applicationInfo['TeacherManagerChildren'] == 'yes'),'',$kis_lang['Admission']['yes'])?>
								<?=$libinterface->Get_Radio_Button('teacherOrManagerChildren2', 'teacherOrManagerChildren', 'no', ($applicationInfo['TeacherManagerChildren'] != 'yes'),'',$kis_lang['Admission']['no'])?>
							   </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['lastschool']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('lastschool', 'lastschool',  $studentApplicationInfo['lastschool'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['remarks']?></td>
                               <td><?=$libinterface->GET_TEXTBOX('OthersRemarks', 'OthersRemarks',  $applicationInfo['Remarks'], $OtherClass='', $OtherPar=array())?></td>
                             </tr>
                           </tbody></table>
   	<?}else if($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings']){?>
                           <table class="form_table">
                             <tbody>
                             <tr >
                               <td  width="30%" class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td width="70%" ><input type="text" name="ApplyStartDay" id="ApplyStartDay" value="<?=$applicationInfo['ApplyStartDay']?>">&nbsp;<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span></td>
                             </tr>
                             <?php
                             if($sys_custom['KIS_Admission']['MINGWAI']['Settings']){
                                 $program_selection1 = '<select name="StudentProgram1" id="StudentProgram1">';
                                 $program_selection1 .= '<option value="0"'.($applicationInfo['ApplyProgram1']== 0?'selected':'').'>Not Applicable 不適用</option>';
                                 $program_selection1 .= '<option value="1"'.($applicationInfo['ApplyProgram1']== 1?'selected':'').'>International Stream PN-K3 國際課程 PN-K3</option>';
                                 $program_selection1 .= '<option value="2"'.($applicationInfo['ApplyProgram1']== 2?'selected':'').'>Local Stream PN / Cambridge Stream K1 - K3 本地課程 PN / 劍橋英語課程 K1 - K3</option>';
                                 $program_selection1 .= '</select>';

                                 $program_selection2 = '<select name="StudentProgram2" id="StudentProgram2">';
                                 $program_selection2 .= '<option value="0"'.($applicationInfo['ApplyProgram2']== 0?'selected':'').'>Not Applicable 不適用</option>';
                                 $program_selection2 .= '<option value="1"'.($applicationInfo['ApplyProgram2']== 1?'selected':'').'>International Stream PN-K3 國際課程 PN-K3</option>';
                                 $program_selection2 .= '<option value="2"'.($applicationInfo['ApplyProgram2']== 2?'selected':'').'>Local Stream PN / Cambridge Stream K1 - K3 本地課程 PN / 劍橋英語課程 K1 - K3</option>';
                                 $program_selection2 .= '</select>';
                             ?>
                                 <tr>
                                   	<td width="30%" class="field_title"><?=$mustfillinsymbol?>Program 課程</td>
                                   	<td width="70%">
                                   		<table style="width:auto;">
                                   			<tr>
                                   				<td>(<?=$kis_lang['Admission']['Option']?> 1) <?=$program_selection1?></td>
                                   				<td>(<?=$kis_lang['Admission']['Option']?> 2) <?=$program_selection2?></td>
                                   			</tr>
                                   		</table>
                                   	</td>
                                 </tr>
                             <?php
                             }
                             ?>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['applyDayType']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                              		<?foreach($applyTimeSlotAry as $_key => $_value):?>
                              				<?$_key++;?>
                       						<td>(<?=$kis_lang['Admission']['Option']?> <?=$_key?>)</td>
                       						<td><?=$libinterface->GET_SELECTION_BOX($applyTimeSlotAry, "name='ApplyDayType[]' id='ApplyDayType".$_key."' class='timeslotselection'",$kis_lang['Admission']['Nil'], $applicationInfo['ApplyDayType'.$_key])?></td>
                       				<?endforeach;?>
                       					</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=$classLevel?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['ExBSName']?></td>
                              <td>
                              	<?=$libinterface->GET_HIDDEN_INPUT('relatives_info_ex_id', 'relatives_info_ex_id',  $studentApplicationRelativesInfoCustEx[0]['RecordID']?$studentApplicationRelativesInfoCustEx[0]['RecordID']:'new');?>

                           		<table style="width:auto;">
                           			<tr>
                           				<td style="width: 140px;"><?=$kis_lang['Admission']['name'] ?>:</td>
                           				<td><?=$libinterface->GET_TEXTBOX('OthersExName', 'OthersExName', $studentApplicationRelativesInfoCustEx[0]['Name'], $OtherClass='', $OtherPar=array())?></td>
                           			</tr>
                           			<tr>
                           				<td><?=$kis_lang['Admission']['yearOfGraduation'] ?>:</td>
                           				<td><?=$libinterface->GET_TEXTBOX('OthersExYear', 'OthersExYear', $studentApplicationRelativesInfoCustEx[0]['Year'], $OtherClass='', $OtherPar=array())?></td>
                           			</tr>
                           		</table>
                              </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CurBSName']?></td>
                            <td>
								<?=$libinterface->GET_HIDDEN_INPUT('relatives_info_cur_id', 'relatives_info_cur_id',  $studentApplicationRelativesInfoCustCur[0]['RecordID']?$studentApplicationRelativesInfoCustCur[0]['RecordID']:'new');?>

                           		<table style="width:auto;">
                           			<tr>
                           				<td style="width: 140px;"><?=$kis_lang['Admission']['name'] ?>:</td>
                           				<td><?=$libinterface->GET_TEXTBOX('OthersCurName', 'OthersCurName', $studentApplicationRelativesInfoCustCur[0]['Name'], $OtherClass='', $OtherPar=array()) ?></td>
                           			</tr>
                           			<tr>
                           				<td><?=$kis_lang['Admission']['yearOfGraduation'] ?>:</td>
                           				<td><?=$libinterface->GET_TEXTBOX('OthersCurClass', 'OthersCurClass', $studentApplicationRelativesInfoCustCur[0]['ClassPosition'], $OtherClass='', $OtherPar=array()) ?></td>
                           			</tr>
                           		</table>
                          	</td>
                             </tr>
                           </tbody></table>
	<?}else if(file_exists(dirname(__FILE__) . "/{$setting_path_ip_rel}/otherinfo_edit.php")){
		include(dirname(__FILE__) . "/{$setting_path_ip_rel}/otherinfo_edit.php");
	}else{?>
                           <table class="form_table">
                             <tbody>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['dateOfEntry']?></td>
                               <td>
									<table style="width:auto;">
                               			<tr>
                               				<td>(<?=$kis_lang['year']?>)</td>
                               				<td><?=$schoolYear?></td>
                               				<td>(<?=$kis_lang['month']?>)</td>
                               				<td><?=$libinterface->Get_Number_Selection('OthersApplyMonth', 1, 12, $applicationInfo['month'], $Onchange='', $noFirst=1, $isAll=0, $FirstTitle='', $Disabled=0)?></td>
                               			</tr>
                               		</table>
								</td>
                             </tr>

                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['applyTerm']?></td>
                               <td>
                              	 <?foreach($applyTermAry as $_key => $_value):?>
                               		<?=$libinterface->Get_Radio_Button('OthersApplyTerm_'.$_key, 'OthersApplyTerm', $_key, ($applicationInfo['term']==$_key), '', $_value)?>
                               	<?endforeach;?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['applyDayType']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                              		<?foreach($applyTimeSlotAry as $_key => $_value):?>
                              				<?$_key++;?>
                       						<td>(<?=$kis_lang['Admission']['Option']?> <?=$_key?>)</td>
                       						<td><?=$libinterface->GET_SELECTION_BOX($applyTimeSlotAry, "name='ApplyDayType[]' id='ApplyDayType".$_key."' class='timeslotselection'",$kis_lang['Admission']['Nil'], $applicationInfo['ApplyDayType'.$_key])?></td>
                       				<?endforeach;?>
                       					</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=$classLevel?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['familyStatus']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               				<td>(<?=$kis_lang['Admission']['elderBrother']?>)</td>
                               				<td><?=$libinterface->Get_Number_Selection('OthersFamilyStatus_EB', 0, 5, $applicationInfo['elder_brother'], $Onchange='', $noFirst=1, $isAll=0, $FirstTitle='', $Disabled=0)?></td>
                               				<td>(<?=$kis_lang['Admission']['elderSister']?>)</td>
                               				<td><?=$libinterface->Get_Number_Selection('OthersFamilyStatus_ES', 0, 5, $applicationInfo['elder_sister'], $Onchange='', $noFirst=1, $isAll=0, $FirstTitle='', $Disabled=0)?></td>
                               				<td>(<?=$kis_lang['Admission']['youngerBrother']?>)</td>
                               				<td><?=$libinterface->Get_Number_Selection('OthersFamilyStatus_YB', 0, 5, $applicationInfo['younger_brother'], $Onchange='', $noFirst=1, $isAll=0, $FirstTitle='', $Disabled=0)?></td>
                               				<td>(<?=$kis_lang['Admission']['youngerSister']?>)</td>
                               				<td><?=$libinterface->Get_Number_Selection('OthersFamilyStatus_YS', 0, 5, $applicationInfo['younger_sister'], $Onchange='', $noFirst=1, $isAll=0, $FirstTitle='', $Disabled=0)?></td>
                               			</tr>
                               		</table>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['ExBSName']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               				<td>(<?=$kis_lang['Admission']['name']?>)</td>
                               				<td><?=$libinterface->GET_TEXTBOX('OthersExBSName', 'OthersExBSName',  $applicationInfo['ExBSName'], $OtherClass='', $OtherPar=array())?></td>
                               				<td>(<?=$kis_lang['form']?>)</td>
                               				<td><?=$libinterface->GET_TEXTBOX('OthersExBSLevel', 'OthersExBSLevel',  $applicationInfo['ExBSLevel'], $OtherClass='', $OtherPar=array())?></td>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CurBSName']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               				<td>(<?=$kis_lang['Admission']['name']?>)</td>
                               				<td><?=$libinterface->GET_TEXTBOX('OthersCurBSName', 'OthersCurBSName',  $applicationInfo['CurBSName'], $OtherClass='', $OtherPar=array())?></td>
                               				<td>(<?=$kis_lang['form']?>)</td>
                               				<td><?=$libinterface->GET_TEXTBOX('OthersCurBSLevel', 'OthersCurBSLevel',  $applicationInfo['CurBSLevel'], $OtherClass='', $OtherPar=array())?></td>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                              <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['needSchoolBus']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               				 <td>
                               					<?=$libinterface->Get_Radio_Button('needschoolbus_y', 'OthersNeedSchoolBus', '1', ($applicationInfo['needschoolbus']=='yes'), '', $kis_lang['yes'])?>
                               				</td>
                               				<td id="td_needschoolbus" <?=($applicationInfo['needschoolbus']=='yes')?'':'style="display:none;"'?>>
                               				(<?=$kis_lang['Admission']['placeForTakingSchoolBus']?>) <input type="text" name="OthersSchoolBusPlace" id="OthersSchoolBusPlace" value="<?=$applicationInfo['SchoolBusPlace']?>">
                               				</td>
                               				<td>
                                				<?=$libinterface->Get_Radio_Button('needschoolbus_n', 'OthersNeedSchoolBus', '0', ($applicationInfo['needschoolbus']=='no'), '', $kis_lang['no'])?>
											</td>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                              <tr>
                               <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['knowUsBy']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               			<?$knowusbyhtml = '';?>
                               			<?foreach($KnowUsByAry as $_key => $_knowUsAry):?>
                       						<td>
                       							<?=$libinterface->Get_Radio_Button('knowusby_'.$_key, 'OthersKnowUsBy', $_knowUsAry['index'], ($applicationInfo['knowusby']==$_key), (($_knowUsAry['desc'])?'hasknowusbyother':''), $kis_lang['Admission'][$_key])?>
                       							<?$knowusbyhtml .= ($_knowUsAry['desc'])?'<span id="span_knowusby_'.$_key.'" class="knowusbyother"'.(($applicationInfo['knowusby']==$_key)?'':' style="display:none;"').'><input type="text" name="txtOthersKnowUsBy'.$_knowUsAry['index'].'" id="txtOthersKnowUsBy'.$_knowUsAry['index'].'" value="'.($applicationInfo['knowusby']==$_key?$applicationInfo['KnowUsByOther']:'').'"></span>':''?>
                       						</td>
                       					<?endforeach;?>
                               			</tr>
                               			<tr><td colspan="<?=count($KnowUsByAry)?>"><?=$knowusbyhtml?></td></tr>
                               		</table>
								</td>
                             </tr>
                           </tbody></table>
	<?}?>
<? elseif($display=='remarks'):?>
	<?if(file_exists(dirname(__FILE__) . "/{$setting_path_ip_rel}/remarks_edit.php")){
		include(dirname(__FILE__) . "/{$setting_path_ip_rel}/remarks_edit.php");
	}else{?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['applicationstatus']?></td>
                               <td width="70%"><?=$statusSelection?></td>
                             </tr>
                             <?php if($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings']){?>
                                 <tr>
                                   <td width="30%" class="field_title"><?=$kis_lang['Admission']['MINGWAI']['admissionMethod']?></td>
                                   <td width="70%">
                                   	<?php
                                   		$admissionMethod = $kis_data['libadmission']->getApplicationCustInfo($applicationInfo['applicationID'], 'AdmissionMethod', 1);
                                   		$admissionMethod = $admissionMethod['Value'];
                                   		if($admissionMethod == $admission_cfg['admissionMethod']['Online']){
                           		    ?>
                           		    		<input type="hidden" id="AdmissionMethod" name="AdmissionMethod" value="<?=$admissionMethod ?>">
                           		    		<?=$kis_lang['Admission']['MINGWAI']['admissionMethodType'][$admissionMethod] ?>
                           		    <?php
                                   		}else{
                           		    ?>
                                   		<select id="AdmissionMethod" name="AdmissionMethod">
                                       		<?php
                                           		foreach($admission_cfg['admissionMethod'] as $method){
                                           		    if($method == $admission_cfg['admissionMethod']['Online']){
                                           		        continue;
                                           		    }
                                           		    $selected = ($method == $admissionMethod)? 'selected' : '';
                                       		?>
                                       			<option value="<?=$method ?>" <?=$selected ?> ><?=$kis_lang['Admission']['MINGWAI']['admissionMethodType'][$method] ?></option>
                                       		<?php
                                           		}
                                       		?>
                                   		</select>

                                   		<?php
                                       		$mailCode = $kis_data['libadmission']->getApplicationCustInfo($applicationInfo['applicationID'], 'MailCode', 1);
                                       		$mailCode = $mailCode['Value'];
                                   		?>
                                   		(
                                       		<?=$kis_lang['Admission']['MINGWAI']['MailCode'] ?>:
                                       		<input type="text" name="MailCode" id="MailCode" value="<?=$mailCode?>">
                                   		)
                               		<?php
                               		}
                               		?>
                                   </td>
                                 </tr>
                             <?php } ?>
                             <?if($sys_custom['KIS_Admission']['TBCPK']['Settings'] || $sys_custom['KIS_Admission']['CSM']['Settings'] || $sys_custom['KIS_Admission']['MGF']['Settings'] || $sys_custom['KIS_Admission']['MUNSANG']['Settings'] || $sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings'] ||  $sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings'] || $sys_custom['KIS_Admission']['RMKG']['Settings']||$sys_custom['KIS_Admission']['KTLMSKG']['Settings']||$sys_custom['KIS_Admission']['YLSYK']['Settings']||$sys_custom['KIS_Admission']['MINGWAI']['Settings']||$sys_custom['KIS_Admission']['MINGWAIPE']['Settings']){?>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['applicationfee']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<?if($paypalPaymentInfo[0]['payment_status']){?>
										<tr>
											<td colspan="4"><font color="green"><?=$kis_lang['Admission']['KTLMSKG']['paidByPayPal']?></font><br/>
											<?foreach($paypalPaymentInfo as $apaypalPaymentInfo){?>
											(<?=$kis_lang['Admission']['KTLMSKG']['PaymentItem']?>) <?=$apaypalPaymentInfo['item_name']?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PaymentTotal']?>) <?=$apaypalPaymentInfo['mc_currency']?> <?=$apaypalPaymentInfo['mc_gross']?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PaymentDate']?>) <?=date('Y-m-d H:i:s', strtotime($apaypalPaymentInfo['payment_date']))?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PayerEmail']?>) <?=$apaypalPaymentInfo['payer_email']?><br/><br/>
											<?}?>
											</td>
										</tr>
										<tr>
											<td colspan="4">&nbsp;</td>
										</tr>
										<?}?>
										<?if($alipayPaymentInfo[0]['RecordStatus']){?>
										<tr>
											<td colspan="4"><font color="green"><?=$kis_lang['Admission']['paidByAlipayHK']?></font><br/>
											<?foreach($alipayPaymentInfo as $aAlipayPaymentInfo){
												if($aAlipayPaymentInfo['RecordStatus']==1)	{
											?>
											(<?=$kis_lang['Admission']['PaymentsStatus']?>) <?=$aAlipayPaymentInfo['RecordStatus']==1?'Success':''?> <br/>(<?=$kis_lang['Admission']['TradeNo']?>) <?=$aAlipayPaymentInfo['TradeNo']?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PaymentTotal']?>) <?=$aAlipayPaymentInfo['Amount']?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PaymentDate']?>) <?=date('Y-m-d H:i:s', strtotime($aAlipayPaymentInfo['DateModified']))?><br/><br/>
											<?	}
											}?>
											</td>
										</tr>
										<tr>
											<td colspan="4">&nbsp;</td>
										</tr>
										<?}?>
                               			<tr>
                               				<td>(<?=$kis_lang['Admission']['receiptcode']?>)</td>
                               				<td colspan="3"><?=$libinterface->GET_TEXTBOX('receiptID', 'receiptID', $applicationInfo['receiptID'], $OtherClass='', $OtherPar=array())?></td>
                               			</tr>
                               			<tr class="receipt_info" <?=empty($applicationInfo['receiptID'])?' style="display:none;"':''?>>
                               				<td>(<?=$kis_lang['date']?>)</td>
                               				<td><input type="text" name="receiptdate" id="receiptdate" value="<?=$applicationInfo['receiptdate']?>">&nbsp;<span class="error_msg" id="warning_receiptdate"></span></td>
                               			</tr>
                               			<tr class="receipt_info" <?=empty($applicationInfo['receiptID'])?' style="display:none;"':''?>>
                               				<td>(<?=$kis_lang['Admission']['handler']?>)</td>
                               				<td>
                               				<span class="mail_to_list" id="span_handler" style="height:auto;width:auto;border:0;">
                               						<span class="mail_user">
                               						<?if(!empty($applicationInfo['handler'])):?>
                               							<div class="mail_user_name">
                               								<?=$applicationInfo['handler']?>
                               							</div>
                               							<div class="mail_icon_form">
                               									<a href="#" class="btn_remove" id="remove_<?=$applicationInfo['handler_id']?>"></a>
                               									<input type="hidden" name="handler" id="handler" value="<?=$applicationInfo['handler_id']?>">
                               							</div>
                               						<?else:?>
                               							--
                               							<input type="hidden" name="handler" id="handler">
                               						<?endif;?>
                               						</span>
                               					</span>
                               					<div class="mail_icon_form">
													<a href="#" class="btn_select_ppl">
													    <?=$kis_lang['select']?>
													</a>
												    </div>
                               				</td>
                               			</tr>
                               		</table>
                               	</td>
                             </tr>
                             <tr>
                               <?
                               	$_filePath = $attachmentList['receipt_copy']['link'];
                             	if($_filePath){
									$_spanDisplay = '';
									$_buttonDisplay = ' style="display:none;"';
								}else{
									$_filePath = '';
									$_spanDisplay = ' style="display:none;"';
									$_buttonDisplay = '';
								}

								$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
								$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';
                               ?>
                            	<td class="field_title"><?=$kis_lang['receiptcopy']?></td>
                            	<td id="receipt_copy">
                               		<span class="view_attachment" <?=$_spanDisplay?>><?=$_attachment?></span>
									<input type="button" class="attachment_upload_btn formsmallbutton" value="<?=$kis_lang['Upload']?>"  id="uploader-receipt_copy" <?=$_buttonDisplay?>/>
								</td>

                   			</tr>

                             <?
                             	list($interviewdate,$interviewtime) = explode(" ",$applicationInfo['interviewdate']);
                             ?>
                             <?if(!$sys_custom['KIS_Admission']['InterviewSettings']){?>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['interviewdate']?></td>
                               <td><input type="text" name="interviewdate" id="interviewdate" value="<?=(is_date_empty($interviewdate)?'':$interviewdate)?>">&nbsp;<span class="error_msg" id="warning_interviewdate"></span><?=$libadmission->Get_Time_Selection($libinterface,'interview',$interviewtime)?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['interviewlocation']?></td>
                               <td><input type="text" name="interviewlocation" id="interviewlocation" value="<?=$applicationInfo['interviewlocation']?>" /></td>
                             </tr>
                             <?}?>
                             <?if($sys_custom['KIS_Admission']['InterviewSettings']){?>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['interviewdate']?></td>
                               <td>
                               	(1) <?=$interviewSettingSelection?><br/>
                               	(2) <?=$interviewSettingSelection2?><br/>
                               	(3) <?=$interviewSettingSelection3?>
                               </td>
                             </tr>
                             <?}?>
                           	 <?
                             	list($briefingdate,$briefingtime) = explode(" ",$applicationInfo['briefingdate']);
                             ?>
                             <?if($sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings']){ ?>
                            	<?php
                            	    $libkis_admission_briefing = new admission_briefing_base();
                            	    $briefing = $libkis_admission_briefing->getBriefingApplicantByApplicantID($applicationInfo['applicationID']);
                        	        $rs = $libkis_admission_briefing->getAllBriefingSession($schoolYearID);
                        	        
                        	        $briefingSessions = array();
                        	        foreach ($rs as $r){
                        	            $briefingDateTime = $r['BriefingStartDate'] . " ~ " . substr($r['BriefingEndDate'], 11, strlen($r['BriefingEndDate']));
                        	            
                        	            $briefingSessions[] = array($r['BriefingID'], $r['Title']."(".$briefingDateTime.")");
                        	        }
                        	        
                        	        $briefingSessionSeats = array();
                        	        for($i=0;$i<11;$i++){
                        	            $briefingSessionSeats[] = array($i,$i);
                        	        }
                            	?>
                              <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['briefing']?></td>
                               <td>
                               		<?=$libinterface->GET_SELECTION_BOX($briefingSessions, ' id="briefingsession" name="briefingsession"', $kis_lang['Admission']['PleaseSelect'], $briefing['BriefingID']); ?>
                               		(
                               			<?=$kis_lang['Admission']['requestSeat'] ?>
                               			<?=$libinterface->GET_SELECTION_BOX($briefingSessionSeats, ' id="BreafingReserveSeats" name="BreafingReserveSeats"', '', $briefing['SeatRequest']); ?>
                           			)
								</td>
                             </tr>
                             <!--tr>
                               <td class="field_title"><?=$kis_lang['Admission']['briefingdate']?></td>
                               <td><input type="text" name="briefingdate" id="briefingdate" value="<?=(is_date_empty($briefingdate)?'':$briefingdate)?>">&nbsp;<span class="error_msg" id="warning_briefingdate"></span><?=$libadmission->Get_Time_Selection($libinterface,'briefing',$briefingtime)?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['briefinginformation']?></td>
                               <td><input type="text" name="briefinginformation" id="briefinginformation" value="<?=$applicationInfo['briefinginfo']?>" /></td>
                             </tr-->

                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['MINGWAI']['fee']?></td>
                               <td>
                               		<?php
                                   		$fee = $kis_data['libadmission']->getApplicationCustInfo($applicationInfo['applicationID'], 'Fee', 1);
                                   		$fees = explode(';', $fee['Value']);

                                   		foreach($admission_cfg['fee'] as $feeType){
                                   		    $checked = (in_array($feeType, $fees))? 'checked' : '';
                           		    ?>
                           		    	<input type="checkbox" name="Fee[]" id="Fee_<?=$feeType ?>" value="<?=$feeType ?>" <?=$checked ?> />
                           		    	<label for="Fee_<?=$feeType ?>"><?=$kis_lang['Admission']['MINGWAI']['feeType'][$feeType] ?></label>
                           		    <?php
                                   		}
                               		?>
							   </td>
                             </tr>

                             <?} ?>
                              <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['isnotified']?></td>
                               <td><?=$libinterface->Get_Checkbox('isnotified', 'isnotified', 1, ($applicationInfo['isnotified']=='yes'), $Class='', $Display='', $Onclick='', $Disabled='')?></td>
                             </tr>
                             <?}?>
                             <?if($sys_custom['KIS_Admission']['ICMS']['Settings']){
                             	list($interviewdate,$interviewtime) = explode(" ",$applicationInfo['interviewdate']);
                             ?>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['interviewdate']?></td>
                               <td><input type="text" name="interviewdate" id="interviewdate" value="<?=(is_date_empty($interviewdate)?'':$interviewdate)?>">&nbsp;<span class="error_msg" id="warning_interviewdate"></span><?=$libadmission->Get_Time_Selection($libinterface,'interview',$interviewtime)?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['interviewlocation']?></td>
                               <td><input type="text" name="interviewlocation" id="interviewlocation" value="<?=$applicationInfo['interviewlocation']?>" /></td>
                             </tr>
                             <?}?>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['otherremarks']?></td>
                               <td><textarea name="remark" id="remark" cols="75" rows="5"><?=$applicationInfo['remark']?></textarea></td>
                             </tr>
                           </tbody></table>
   <?php } ?>
<? endif; ?>
			<div class="text_remark"><?=$kis_lang['requiredfield']?></div>
			<div class="edit_bottom">
				<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />
	       		<input type="hidden" name="schoolYearId" id="schoolYearId" value="<?=$schoolYearID?>">
	       		<input type="hidden" name="recordID" id="recordID" value="<?=$recordID?>">
	       		<input type="hidden" name="display" id="display" value="<?=$display?>">
	        	<input type="submit" id="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
	            <input type="button" id="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
	        </div>
          	<p class="spacer"></p><br>
        </div>
    <? else: ?>
   	 <? kis_ui::loadNoRecord() ?>
    <? endif; ?>
</form>
</div>
<form class='mail_select_user'>
    <h2><?=$kis_lang['findusers']?></h2>

    <?=$kis_lang['keyword']?>: <input type="text" name="keyword" style="float:right" value=""/>
    <p class="spacer"></p>
    <?=$kis_lang['group']?>: <select name="user_group" style="float:right" >
	<option value=""><?=$kis_lang['all']?> <?=$kis_lang['groups']?></option>
	<?if($groups){ foreach ($groups as $group): ?>
	<option value="<?=$group['group_id']?>"><?=$group['group_name_'.$lang]?></option>
	<? endforeach; }?>
    </select>
    <p class="spacer"></p>
    <?=$kis_lang['identity']?>:
    <label for="user_type_3" style="padding-right:0px;"><?=$kis_lang['staff']?></label><input type="radio" id="user_type_3" name="user_type" value='3' />
    <label for="user_type_1" style="padding-right:0px;"><?=$kis_lang['teacher']?></label><input type="radio" id="user_type_1" name="user_type" checked value='1' />
    <p class="spacer"></p>
    <div class="button">
	<input class="formbutton" value="<?=$kis_lang['search']?>" type="submit"/>
	<input class="formsubbutton" value="<?=$kis_lang['close']?>" type="submit"/>
    </div>
    <p class="spacer"></p>
    <div class="search_results">
    </div>


</form>
