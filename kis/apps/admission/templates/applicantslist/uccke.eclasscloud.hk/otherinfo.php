<?php

global $libkis_admission;

#### Get Student Cust Info START ####
$applicationInfo['studentCustInfo'] = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
#### Get Student Cust Info START ####


#### Get Relatives Info START ####
$applicationInfo['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
#### Get Relatives Info END ####


?>
<table class="form_table">
	<colgroup>
		<col width="30%">
		<col width="20%">
		<col width="30%">
		<col width="20%">
	</colgroup>
	<tbody>                              
		<!--tr> 
			<td width="30%" class="field_title"><?=$kis_lang['form']?></td>
			<td width="70%"><?=kis_ui::displayTableField($classLevelAry[$applicationInfo['classLevelID']])?></td>
		</tr-->
		
<!-------------- Student Other Info START -------------->
<!--tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['KTLMSKG']['CurrentStudySchool']?> (<?=$kis_lang['Admission']['class']?>)
	</td>
	<td>
		<span><?=kis_ui::displayTableField($currentSchoolInfo['OthersPrevSchName'])?> (<?=kis_ui::displayTableField($currentSchoolInfo['OthersPrevSchClass'])?>)</span>
	</td>
</tr-->
<!-------------- Student Other Info END -------------->


<!-------------- Siblings START -------------->
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['RelativeAttending'] ?>
			</td>
			<td>
				<?=kis_ui::displayTableField($applicationInfo['studentApplicationRelativesInfo'][0]['OthersRelativeStudiedName'])?>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['Relationship'] ?>
			</td>
			<td>
				<?=kis_ui::displayTableField($applicationInfo['studentApplicationRelativesInfo'][0]['OthersRelativeRelationship'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['ClassAttending'] ?>
			</td>
			<td>
				<?=kis_ui::displayTableField($applicationInfo['studentApplicationRelativesInfo'][0]['OthersRelativeClassPosition'])?>
			</td>
		</tr>
<!-------------- Siblings END -------------->


<!-------------- Referee START -------------->
<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['KTLMSKG']['introducedName']?>
	</td>
	<td colspan="3">
		<?=kis_ui::displayTableField($applicationInfo['studentCustInfo']['Referee_Name'][0]['Value'])?>
	</td>
</tr>
<!-------------- Referee END -------------->

</tbody>
</table>
