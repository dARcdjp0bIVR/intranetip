<?php

global $libkis_admission;

######## Get Parent Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

#### Religion START ####
$regligion = $applicationCustInfo['Parent_Religion'][0]['Value'];
#### Religion END ####

#### Fax START ####
/*$fax = $applicationCustInfo['Parent_Fax'][0]['Value'];
#### Fax END ####

#### Lang Spoken START ####
$langStr = '';
$Lang_Spoken = (array)$applicationCustInfo['Parent_Lang_Spoken'];
foreach($Lang_Spoken as $lang){
	if($lang['Value'] == 'Cantonese'){
		$LangSpokenCantonese = 'checked="checked"';
	}else if($lang['Value'] == 'Putonghua'){
		$LangSpokenPutonghua = 'checked="checked"';
	}else if($lang['Value'] == 'English'){
		$LangSpokenEnglish = 'checked="checked"';
	}else if($lang['Value'] == 'Others'){
		$LangSpokenOthersChk = 'checked="checked"';
		$LangSpokenOthers = $applicationCustInfo['Parent_Lang_Spoken_Other'][0]['Value'];
	}
	$langStr .= '<br />';
}
// $langStr = substr($langStr, 0, strlen($langStr)-6); // Trim br
$langSpoken = $langStr;
#### Lang Spoken END ####

#### Level of education START ####
if($applicationInfo['G']['levelofeducation'] == 'Tertiary'){
	$levelofeducationTertiary = 'checked="checked"';
}else if($applicationInfo['G']['levelofeducation'] == 'Secondary'){
	$levelofeducationSecondary = 'checked="checked"';
}else{
	$levelofeducationOthersChk = 'checked="checked"';
	$levelofeducation = $applicationInfo['G']['levelofeducation'];
}*/
#### Level of education END ####
######## Get Parent Cust Info END ########
?>
<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />
<table class="form_table">
	<colgroup>
		<col width="30%">
		<col width="40%">
		<col width="30%">
	</colgroup>
	<tbody>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['UCCKE']['NameOfParentGuardian']?>
			</td>
			<td>
				<?=$libinterface->GET_TEXTBOX('ParentName', 'ParentName', $applicationInfo['G']['parent_name'], $OtherClass='', $OtherPar=array())?>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['relationship']?>
			</td>
			<td>
				<?=$libinterface->GET_TEXTBOX('ParentRelationship', 'ParentRelationship', $applicationInfo['G']['relationship'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['religion']?> (<?=$kis_lang['Admission']['ifAny'] ?>)
			</td>
			<td>
				<?=$libinterface->GET_TEXTBOX('ParentReligion', 'ParentReligion', $regligion, $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['email']?>
			</td>
			<td>
				<?=$libinterface->GET_TEXTBOX('ParentEmail', 'ParentEmail', $applicationInfo['G']['email'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['UCCKE']['EngAddress']?>
			</td>
			<td>
				<?=$libinterface->GET_TEXTBOX('ParentAddress', 'ParentAddress', $applicationInfo['G']['companyaddress'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['UCCKE']['phome']?>
			</td>
			<td>
				<?=$libinterface->GET_TEXTBOX('ParentHomeTelNo', 'ParentHomeTelNo', $applicationInfo['G']['homephone'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['UCCKE']['mobile']?>
			</td>
			<td>
				<?=$libinterface->GET_TEXTBOX('ParentMobileNo', 'ParentMobileNo', $applicationInfo['G']['mobile'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['UCCKE']['occupation']?>
			</td>
			<td>
				<?=$libinterface->GET_TEXTBOX('ParentOccupation', 'ParentOccupation', $applicationInfo['G']['occupation'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
	</tbody>
</table>


<script>
$('#applicant_form').unbind('submit').submit(function(e){
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkValidForm(){
	
	return true;
}
</script>