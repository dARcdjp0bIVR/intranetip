<?php

global $libkis_admission;

######## Get Parent Cust Info START ########
$parentType = (isset($applicationInfo['F']))? 'F' : 'G';
//$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
######## Get Parent Cust Info END ########
?>
<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />

<table class="form_table">
	<colgroup>
        <col style="width:30%">
        <col style="width:35%">
        <col style="width:35%">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center>
    		<select name="parentType">
    			<option value="F" <?= ($parentType == 'F')? 'selected' : '' ?>><?=$kis_lang['Admission']['HKUGAPS']['father'] ?></option>
    			<option value="G" <?= ($parentType == 'G')? 'selected' : '' ?>><?=$kis_lang['Admission']['SSGC']['guardian'] ?></option>
    		</select>
		</center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['mother'] ?></center></td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename'] ?>
		</td>
		<td class="form_guardian_field">
			<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" value="<?=$applicationInfo[$parentType]['ChineseName'] ?>"/>
    	</td>
		<td class="form_guardian_field">
			<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" value="<?=$applicationInfo['M']['ChineseName'] ?>"/>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname'] ?>
		</td>
		<td class="form_guardian_field">
			<input name="G1EnglishName" type="text" id="G1EnglishName" class="textboxtext" value="<?=$applicationInfo[$parentType]['EnglishName'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2EnglishName" type="text" id="G2EnglishName" class="textboxtext" value="<?=$applicationInfo['M']['EnglishName'] ?>"/>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$mustfillinsymbol?><?=$kis_lang['Admission']['occupation']?>
		</td>
		<td class="form_guardian_field">
			<input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext" value="<?=$applicationInfo[$parentType]['JobTitle'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext" value="<?=$applicationInfo['M']['JobTitle'] ?>"/>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$mustfillinsymbol?><?=$kis_lang['Admission']['SSGC']['organization']?>
		</td>
		<td class="form_guardian_field">
			<input name="G1Company" type="text" id="G1Company" class="textboxtext" value="<?=$applicationInfo[$parentType]['Company'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2Company" type="text" id="G2Company" class="textboxtext" value="<?=$applicationInfo['M']['Company'] ?>"/>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$mustfillinsymbol?><?=$kis_lang['Admission']['SHCK']['ContactNo']?>
		</td>
		<td class="form_guardian_field">
			<input name="G1MobileNo" type="text" id="G1MobileNo" class="textboxtext" value="<?=$applicationInfo[$parentType]['Mobile'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2MobileNo" type="text" id="G2MobileNo" class="textboxtext" value="<?=$applicationInfo['M']['Mobile'] ?>"/>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['email']?>
		</td>
		<td class="form_guardian_field">
			<input name="G1Email" type="text" id="G1Email" class="textboxtext" value="<?=$applicationInfo[$parentType]['Email'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2Email" type="text" id="G2Email" class="textboxtext" value="<?=$applicationInfo['M']['Email'] ?>"/>
    	</td>
	</tr>
	
</table>
<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function checkValidForm(){
	var re = /\S+@\S+\.\S+/;
	var form1 = applicant_form;

	/******** Parent Info START ********/
	/**** Name START ****/
	if($.trim(form1.G1ChineseName.value)==''){
		alert(" <?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>\n Please Enter Name of Parent.");
		form1.G1ChineseName.focus();
		return false;
	}
	if(
		!checkNaNull(form1.G1ChineseName.value) &&
		!checkIsChineseCharacter(form1.G1ChineseName.value)
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>\n Please enter Chinese character.");	
		form1.G1ChineseName.focus();
		return false;
	}
	
	if($.trim(form1.G2ChineseName.value)==''){
		alert(" <?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>\n Please Enter Name of Parent.");
		form1.G2ChineseName.focus();
		return false;
	}
	if(
		!checkNaNull(form1.G2ChineseName.value) &&
		!checkIsChineseCharacter(form1.G2ChineseName.value)
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>\n Please enter Chinese character.");	
		form1.G2ChineseName.focus();
		return false;
	}

	if($.trim(form1.G1EnglishName.value)==''){
		alert(" <?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>\n Please Enter Name of Parent.");
		form1.G1EnglishName.focus();
		return false;
	}
	if(
		!checkNaNull(form1.G1EnglishName.value) &&
		!checkIsEnglishCharacter(form1.G1EnglishName.value)
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>\n Please enter English character.");	
		form1.G1EnglishName.focus();
		return false;
	}

	if($.trim(form1.G2EnglishName.value)==''){
		alert(" <?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>\n Please Enter Name of Parent.");
		form1.G2EnglishName.focus();
		return false;
	}
	if(
		!checkNaNull(form1.G2EnglishName.value) &&
		!checkIsEnglishCharacter(form1.G2EnglishName.value)
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>\n Please enter English character.");	
		form1.G2EnglishName.focus();
		return false;
	}
	/**** Name END ****/
	
	/**** Occupation START ****/
	if($.trim(form1.G1Occupation.value)==''){
		alert(" <?=$kis_lang['Admission']['msg']['enteroccupation']?>\n Please enter Occupation.");
		form1.G1Occupation.focus();
		return false;
	}
	if($.trim(form1.G2Occupation.value)==''){
		alert(" <?=$kis_lang['Admission']['msg']['enteroccupation']?>\n Please enter Occupation.");
		form1.G2Occupation.focus();
		return false;
	}
	/**** Occupation START ****/
	
	/**** Occupation START ****/
	if($.trim(form1.G1Company.value)==''){
		alert(" <?=$kis_lang['Admission']['SSGC']['msg']['enterOrganization']?>\n Please enter Organization.");
		form1.G1Company.focus();
		return false;
	}
	if($.trim(form1.G2Company.value)==''){
		alert(" <?=$kis_lang['Admission']['SSGC']['msg']['enterOrganization']?>\n Please enter Organization.");
		form1.G2Company.focus();
		return false;
	}
	/**** Occupation START ****/
	
	/**** Contact Number START ****/
	if($.trim(form1.G1MobileNo.value)==''){
		alert(" <?=$kis_lang['Admission']['HKUGAPS']['msg']['enterContactNo']?>\n Please Enter Contact Number.");
		form1.G1MobileNo.focus();
		return false;
	}
	if(
		!checkNaNull(form1.G1MobileNo.value) &&
		!/^[0-9]*$/.test(form1.G1MobileNo.value)
	){
		alert(" <?=$kis_lang['Admission']['HKUGAPS']['msg']['invalidContactNoFormat']?>\n Invalid Contact Number Format.");
		form1.G1MobileNo.focus();
		return false;
	}
	
	if($.trim(form1.G2MobileNo.value)==''){
		alert(" <?=$kis_lang['Admission']['HKUGAPS']['msg']['enterContactNo']?>\n Please Enter Contact Number.");
		form1.G2MobileNo.focus();
		return false;
	}
	if(
		!checkNaNull(form1.G2MobileNo.value) &&
		!/^[0-9]*$/.test(form1.G2MobileNo.value)
	){
		alert(" <?=$kis_lang['Admission']['HKUGAPS']['msg']['invalidContactNoFormat']?>\n Invalid Contact Number Format.");
		form1.G2MobileNo.focus();
		return false;
	}
	/**** Contact Number END ****/

	/**** Email START ****/
	if($.trim(form1.G1Email.value)!='' && !re.test(form1.G1Email.value)){
		alert(" <?=$kis_lang['Admission']['icms']['msg']['invalidmailaddress']?>\n Invalid E-mail Format");
		form1.G1Email.focus();
		return false;
	}
	if($.trim(form1.G2Email.value)!='' && !re.test(form1.G2Email.value)){
		alert(" <?=$kis_lang['Admission']['icms']['msg']['invalidmailaddress']?>\n Invalid E-mail Format");
		form1.G2Email.focus();
		return false;
	}
	/**** Email END ****/
	/******** Parent Info END ********/
	return true;
}
</script>