<?php
/**
 * Change Log:
 * 2018-08-13 Pun
 *  - File created
 */
global $kis_data, $libkis_admission;

if($applicationIds){
    $applicationIds = explode(',', $applicationIds);
}else{
    $applicationIds = array();
}

#### NavigationBar START ####
$selectedSchoolYear = current(kis_utility :: getAcademicYears(array (
    'AcademicYearID' => $kis_data['schoolYearID']
)));
$schoolYear = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
$classLevel = $libkis_admission->classLevelAry[$classLevelID];

$NavArr[] = array (
    'applicantslist/',
    $schoolYear
);
$NavArr[] = array (
    "applicantslist/listbyform/{$classLevelID}/",
    $classLevel
);
$NavArr[] = array (
	'',
	$kis_lang['export']
);
$navigationBar = $libkis_admission->getNavigationBar($NavArr);
#### NavigationBar END ####

#### UI START ####
?>
<style>
.sub-info {
    padding-left: 20px;
}
</style>
<div class="main_content_detail">
    <form id="exportForm" name="exportForm" action='/kis/admission_form/export_form.php' method="post" enctype="multipart/form-data">
    	<?=$navigationBar?>
    
    	<div class="table_board">
    		<table id="ComposeMailTable" class="form_table">
            	<tbody>
                	<tr> 
                    	<td class="field_title" style="width:30%;">
                    		<?=$kis_data['mustfillinsymbol']?> <?=$kis_lang['exportField'] ?>
    					</td>
                    	<td style="width:70%;">
                    		<div>
                    			<input type="checkbox" name="customFields[]" id="field_student_info" value="student_info" checked />
                    			<label for="field_student_info"><?=$kis_lang['Admission']['studentInfo'] ?></label>
                    		</div>
                    		<?php
                    		$stuInfos = array(
                                'chinesename' => $kis_lang['Admission']['chinesename'],
                                'englishname' => $kis_lang['Admission']['englishname'],
                                'gender' => $kis_lang['Admission']['gender'],
                                'birthCertNo' => $kis_lang['Admission']['SHCK']['birthCertNo'],
                                'dateofbirth' => $kis_lang['Admission']['dateofbirth'],
                                'nationality' => $kis_lang['Admission']['nationality'],
                                'placeofbirth' => $kis_lang['Admission']['placeofbirth'],
                                'religion' => $kis_lang['Admission']['SSGC']['religion'],
                                'districtCode' => $kis_lang['Admission']['SSGC']['districtCode'],
                                'district' => $kis_lang['Admission']['district'],
                                'addressChinese' => $kis_lang['Admission']['SSGC']['addressChinese'],
                                'addressEnglish' => $kis_lang['Admission']['SSGC']['addressEnglish'],
                                'presentSchool' => $kis_lang['Admission']['SSGC']['presentSchool'],
                                'homeTel' => $kis_lang['Admission']['SSGC']['homeTel'],
                                'email' => $kis_lang['Admission']['email'],
                                'desiredSession1' => $kis_lang['Admission']['SSGC']['desiredSession'] . ' (1)',
                                'desiredSession2' => $kis_lang['Admission']['SSGC']['desiredSession'] . ' (2)',
                                'desiredSession3' => $kis_lang['Admission']['SSGC']['desiredSession'] . ' (3)',
                                'langSpoken' => $kis_lang['Admission']['SSGC']['langSpoken'],
                                'appliedBefore' => $kis_lang['Admission']['SSGC']['appliedBefore'],
                    		);
                    		foreach($stuInfos as $key => $lang){
                    		?>
                        		<div class="student_infos sub-info">
                        			<input type="checkbox" name="customFields[]" id="field_student_info_<?=$key ?>" value="student_info_<?=$key ?>" checked />
                        			<label for="field_student_info_<?=$key ?>"><?=$lang ?></label>
                        		</div>
                    		<?php 
                    		}
                    		?>
                    		
                    		<div>
                    			<input type="checkbox" name="customFields[]" id="field_parent_info" value="parent_info" checked />
                    			<label for="field_parent_info"><?=$kis_lang['Admission']['PGInfo'] ?></label>
                    		</div>
                    		
                    		<div>
                    			<input type="checkbox" name="customFields[]" id="field_sibiling_info" value="sibiling_info" checked />
                    			<label for="field_sibiling_info"><?=$kis_lang['Admission']['SSGC']['sibilingInfo'] ?></label>
                    		</div>
                    		
                    		<div>
                    			<input type="checkbox" name="customFields[]" id="field_other_info" value="other_info" checked />
                    			<label for="field_other_info"><?=$kis_lang['Admission']['otherInfo'] ?></label>
                    		</div>
                    		
                    		<div>
                    			<input type="checkbox" name="customFields[]" id="field_remarks_info" value="remarks_info" checked />
                    			<label for="field_remarks_info"><?=$kis_lang['Admission']['remarks'] ?></label>
                    		</div>
                    	</td>
                    </tr>
                </tbody>
            </table>
            <div class="text_remark step1"><?=str_replace('*','<span style="color:red">*</span>',$kis_lang['requiredfield'])?></div>
    		<div class="edit_bottom">
                <input type="submit" name="doneBtn" id="doneBtn" class="formbutton" value="<?=$kis_lang['export']?>" />
            	<input type="button" name="backBtn" id="backBtn" class="formsubbutton" value="<?=$kis_lang['back']?>" />
    	    	<input type="hidden" name="classLevelID" id="classLevelID" value="<?=$classLevelID?>" />
    	    	<input type="hidden" name="selectStatus" id="selectStatus" value="<?=$selectStatus?>" />
    	    	<input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$kis_data['schoolYearID']?>" />
    	    	<?php foreach($applicationIds as $applicationId): ?>
    	    		<input type="hidden" name="applicationAry[]" id="applicationAry[]" value="<?=$applicationId?>" />
    	    	<?php endforeach; ?>
    	    </div>
            <p class="spacer"></p><br>  
    	</div>
    </form>
</div>

<script>
$(function(){
	'use strict';
	
	$('#field_student_info').click(function(){
		$('.student_infos input').prop('checked', $(this).prop('checked'));
	});
	$('.student_infos input').click(function(){
		if($(this).prop('checked')){
			$('#field_student_info').prop('checked', true);
		}
	});
	
	$('#exportForm').submit(function(){
		if($(this).find('input[type="checkbox"]:checked').length == 0){
			alert(globalAlertMsg2);
			return false;
		}
		return true;
	});
	$('#backBtn').click(function(){
		$.address.value('/apps/admission/applicantslist/listbyform/<?=$classLevelID?>/');
	});
});
</script>