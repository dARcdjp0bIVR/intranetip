<?php

global $libkis_admission;
$allClassLevel = $libkis_admission->getClassLevel();

######## Get Student Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
$applicationInfo['studentApplicationInfoCust'] = $libkis_admission->getApplicationOthersInfo($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
$applicationInfo['studentApplicationInfoCust'] = $applicationInfo['studentApplicationInfoCust'][0];


#### Get Desired Session Data START ####
$applicationSetting = $libkis_admission->getApplicationSetting($kis_data['schoolYearID']);
$dayType = $applicationSetting[ $kis_data['classLevelID'] ]['DayType'];
$dayTypeArr = explode(',',$dayType);

$applyDayTypeArr = array();
$applyTimeSlotAry = array();
for($i=0;$i<count($dayTypeArr);$i++){
	$applyTimeSlotAry[] = array($dayTypeArr[$i],$kis_lang['Admission']['TimeSlot'][$dayTypeArr[$i]]);
}
$applyDayTypeArr[] = $applicationInfo['studentApplicationInfoCust']['ApplyDayType1'];
$applyDayTypeArr[] = $applicationInfo['studentApplicationInfoCust']['ApplyDayType2'];
$applyDayTypeArr[] = $applicationInfo['studentApplicationInfoCust']['ApplyDayType3'];
#### Get Desired Session Data END ####

$prevApplyClassLevel = array();
$rs = $libkis_admission->getApplicationCustInfo($applicationInfo['applicationID'], 'AppliedBeforeClass');
$prevApplyClassLevel = Get_Array_By_Key($rs, 'Value');
######## Get Student Cust Info END ########

?>
<style>
select:disabled{
    color: #ccc;
}
textarea{
    height: 100px;
    resize: vertical;
}

.sessionChoice{
    display: inline-block;
    margin-right: 30px;
}
</style>

<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />
<table class="form_table">
	<colgroup>
		<col style="width: 30%;"/>
		<col style="width: 40%;"/>
		<col style="width: 30%;"/>
	</colgroup>
	<tbody>
		<tr> 
			<td width="30%" class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['HKUGAPS']['chinesename'] ?>
			</td>
			<td width="40%">
				<?=$libinterface->GET_TEXTBOX('studentsname_b5', 'studentsname_b5', $applicationInfo['student_name_b5'], $OtherClass='', $OtherPar=array())?>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin:0px;">
					<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
					<div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
						<a id = "btn_remove" href="#" class="btn_remove"></a>
					</div>
					<div class="text_remark" style="text-align:center;">
						<?=$kis_lang['Admission']['msg']['clicktouploadphoto']?>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['HKUGAPS']['englishname'] ?>
			</td>
			<td>
			<?=$libinterface->GET_TEXTBOX('studentsname_en', 'studentsname_en', $applicationInfo['student_name_en'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['dateofbirth'] ?>
			</td>
			<td>
			<input name="StudentDateOfBirth" id="StudentDateOfBirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;
			<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['gender'] ?>
			</td>
    		<td>
    			<?=$libinterface->Get_Radio_Button('StudentGender1', 'StudentGender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
    			<?=$libinterface->Get_Radio_Button('StudentGender2', 'StudentGender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>                              
    		</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['SHCK']['birthCertNo'] ?>
			</td>
			<td nowrap="" colspan="2">
    			<input name="StudentBirthCertNo" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" value="<?=$applicationInfo['birthcertno'] ?>"/>
    			<br/>
    			<?=$kis_lang['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['nationality'] ?>
			</td>
			<td nowrap="">
    			<input id="Nationality" name="Nationality" class="textboxtext" value="<?=$applicationInfo['Nationality'] ?>">
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['placeofbirth'] ?>
			</td>
			<td nowrap="">
    			<input id="StudentPlaceOfBirth" name="StudentPlaceOfBirth" class="textboxtext" value="<?=$applicationInfo['PlaceOfBirth'] ?>">
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['religion'] ?>
			</td>
			<td nowrap="">
    			<input id="ReligionOther" name="ReligionOther" class="textboxtext" value="<?=$applicationInfo['ReligionOther'] ?>" style="width: 120px;" />
    			<?=$kis_lang['Admission']['SSGC']['religionShort'] ?>
    			&nbsp;&nbsp;&nbsp;
    			<input id="Church" name="Church" class="textboxtext" value="<?=$applicationInfo['Church'] ?>" style="width: 120px;" />
    			<?=$kis_lang['Admission']['SSGC']['church'] ?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['district'] ?>
			</td>
			<td nowrap="">
    			<select id="AddressDistrict" name="AddressDistrict">
    				<?php
    				foreach($admission_cfg['DistrictArea'] as $index => $address):
    				    $selected = ($applicationInfo['AddressDistrict'] == $index)? 'selected' : '';
    				?>
    					<option value="<?=$index ?>" <?=$selected ?> ><?=$address ?></option>
    				<?php
    				endforeach;
    				?>
    			</select>
    			
    			<?php foreach($admission_cfg['DistrictArea'] as $areaIndex => $address): ?>
        			<select id="AddressEstate<?=$areaIndex ?>" name="AddressEstate">
        				<?php
        				foreach($admission_cfg['DistrictAddress'][$areaIndex] as $addressIndex => $address):
        				    if(
        				        $applicationInfo['AddressDistrict'] == $areaIndex && 
        				        $applicationInfo['AddressEstate'] == $addressIndex
        				    ){
        				        $selected = 'selected';
        				    }else{
        				        $selected = '';
        				    }
        				?>
        					<option value="<?=$addressIndex ?>" <?=$selected ?> ><?=$address ?></option>
        				<?php
        				endforeach;
        				?>
        			</select>
    			<?php endforeach; ?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['SSGC']['addressChinese'] ?>
			</td>
			<td nowrap="">
    			<textarea id="AddressChi" name="AddressChi" class="textboxtext"><?=$applicationInfo['AddressChi'] ?></textarea>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['SSGC']['addressEnglish'] ?>
			</td>
			<td nowrap="">
    			<textarea id="Address" name="Address" class="textboxtext"><?=$applicationInfo['Address'] ?></textarea>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['SSGC']['presentSchool'] ?>
			</td>
			<td nowrap="">
    			<input id="LastSchool" name="LastSchool" class="textboxtext" value="<?=$applicationInfo['LastSchool'] ?>">
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['SSGC']['homeTel'] ?>
			</td>
			<td nowrap="">
    			<input id="HomeTelNo" name="HomeTelNo" class="textboxtext" value="<?=$applicationInfo['HomeTelNo'] ?>">
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['contactEmail'] ?>
			</td>
			<td nowrap="">
    			<input id="Email" name="Email" class="textboxtext" value="<?=$applicationInfo['Email'] ?>">
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['SSGC']['desiredSession'] ?>
			</td>
			<td nowrap="">
				<?php 
				for($i=1;$i<=count($dayTypeArr);$i++): 
				?>
					<label for="OthersApplyDayType<?=$i ?>">
						<?= "{$kis_lang['Admission']['Option']} {$i}: " ?>
					</label>
					<?= $kis_data['libinterface']->GET_SELECTION_BOX($applyTimeSlotAry, "name='OthersApplyDayType{$i}' id='OthersApplyDayType{$i}' class='timeslotselection' style='margin-bottom: 5px;'",( (count($dayTypeArr)==1)? '' : $kis_lang['Admission']['Nil'] ), $applyDayTypeArr[$i-1] );?>
					<br />
				<?php 
				endfor; 
				?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['SSGC']['langSpoken'] ?>
			</td>
			<td nowrap="">
    			<input type="radio" value="1" id="LangSpokenAtHomeY" name="LangSpokenAtHome" <?=($applicationInfo['LangSpokenAtHome'] == 1)?'checked':'' ?> />
    			<label for="LangSpokenAtHomeY"><?=$kis_lang['Admission']['yes3']?></label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="LangSpokenAtHomeN" name="LangSpokenAtHome" <?=($applicationInfo['LangSpokenAtHome'] == 0)?'checked':'' ?> />
    			<label for="LangSpokenAtHomeN"><?=$kis_lang['Admission']['no3']?></label>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['SSGC']['appliedBefore'] ?>
			</td>
			<td nowrap="">
        		<div>
        			<input type="radio" value="1" id="AppliedBeforeY" name="AppliedBefore" <?=($applicationCustInfo['AppliedBefore'][0]['Value'] == 1)?'checked':'' ?> />
        			<label for="AppliedBeforeY"><?=$kis_lang['Admission']['yes']?></label>&nbsp;&nbsp;
        			<input type="radio" value="0" id="AppliedBeforeN" name="AppliedBefore" <?=($applicationCustInfo['AppliedBefore'][0]['Value'] == 0)?'checked':'' ?> />
        			<label for="AppliedBeforeN"><?=$kis_lang['Admission']['no']?></label>
    			</div>
    			<div id="AppliedBeforeClassDiv" style="margin-top: 10px">
    				<?php
    				$index = 1;
    				foreach($allClassLevel as $classLevelId => $classLevel):
    				    $checked = (in_array($classLevelId, $prevApplyClassLevel))? 'checked' : '';
    				?>
            			<input type="checkbox" value="<?=$classLevelId ?>" id="AppliedBeforeClass<?=$classLevelId ?>" name="AppliedBeforeClass[]" <?=$checked ?> />
            			<label for="AppliedBeforeClass<?=$classLevelId ?>" style="margin-right: 10px;"><?=$classLevel ?></label>
    				<?php
    				    echo ($index++ % 5 == 0)? '<br />' : '';
    				endforeach;
    				?>
    			</div>
			</td>
		</tr>
		
		
	<?php
	for($i=0;$i<sizeof($attachmentSettings);$i++) {
		$attachment_name = $attachmentSettings[$i]['AttachmentName'];
		
		$_filePath = $attachmentList[$attachment_name]['link'];
		if($_filePath){
			$_spanDisplay = '';
			$_buttonDisplay = ' style="display:none;"';
		}else{
			$_filePath = '';
			$_spanDisplay = ' style="display:none;"';
			$_buttonDisplay = '';
		}
		
		$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
		$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';
	?>
		<tr>
			<td class="field_title">
				<?=$attachment_name?>
			</td>
			<td id="<?=$attachment_name?>">
				<span class="view_attachment" <?=$_spanDisplay?>><?=$_attachment?></span>
				<input type="button" class="attachment_upload_btn formsmallbutton" value="<?=$kis_lang['Upload']?>"  id="uploader-<?=$attachment_name?>" <?=$_buttonDisplay?>/>
			</td>
		</tr>	
	<?php
	}
	?>
	</tbody>
</table>

<script>
//// UI Releated START ////
function updateUI(){
	var addressDistrict = $('#AddressDistrict').val();
	$('[name="AddressEstate"]').hide().prop('disabled', true);
	$('#AddressEstate' + addressDistrict).show().prop('disabled', false);

	if($('[name="AppliedBefore"]:checked').val() == '1'){
		$('#AppliedBeforeClassDiv').show().find('input').prop('disabled', false);
	}else{
		$('#AppliedBeforeClassDiv').hide().find('input').prop('disabled', true);
	}
}

$('[name="AppliedBefore"]').change(updateUI);
$('#AddressDistrict').change(updateUI);
updateUI();
////UI Releated END ////

$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	
	return false;
});

function check_hkid(hkid) {
	var re = /^([A-Z]{1,2})((\d){6})\({0,1}([A0-9]{1})\){0,1}$/g;
	var ra = re.exec(hkid);

	if (ra != null) {
		var p1 = ra[1];
		var p2 = ra[2];
		var p3 = ra[4];
		var check_sum = 0;
		if (p1.length == 2) {
			check_sum = (p1.charCodeAt(0)-55) * 9 + (p1.charCodeAt(1)-55) * 8;
		}
		else if (p1.length == 1){
			check_sum = 324 + (p1.charCodeAt(0)-55) * 8;
		}

		check_sum += parseInt(p2.charAt(0)) * 7 + parseInt(p2.charAt(1)) * 6 + parseInt(p2.charAt(2)) * 5 + parseInt(p2.charAt(3)) * 4 + parseInt(p2.charAt(4)) * 3 + parseInt(p2.charAt(5)) * 2;
		var check_digit = 11 - (check_sum % 11);
		if (check_digit == '11') {
			check_digit = 0;
		}
		else if (check_digit == '10') {
			check_digit = 'A';
		}
		if (check_digit == p3 ) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}
	
function checkValidForm(){
	var re = /\S+@\S+\.\S+/;
	var dOBRange = new Array();
	   $.ajax({
	       url: "admission_form/ajax_get_bday_range.php",
	       type: "post",
	       data: $("#applicant_form").serialize(),
	       async: false,
	       success: function(data){
	           //alert("debugging: The classlevel is updated!");
	           dOBRange = data.split(",");
	       },
	       error:function(){
	           //alert("failure");
	           $("#result").html('There is error while submit');
	       }
	   });


	var form1 = applicant_form;
	
	/******** Student Info START ********/
	/**** Name START ****/
	if($.trim(form1.studentsname_b5.value)==''){
		alert(" <?=$kis_lang['Admission']['msg']['enterchinesename']?>\n Please enter Name in Chinese.");	
		form1.studentsname_b5.focus();
		return false;
	}
	if(
		!checkNaNull(form1.studentsname_b5.value) && 
		!checkIsChineseCharacter(form1.studentsname_b5.value)
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>\n Please enter Chinese character.");	
		form1.studentsname_b5.focus();
		return false;
	}
	
	if($.trim(form1.studentsname_en.value)==''){
		alert(" <?=$kis_lang['Admission']['msg']['enterenglishname']?>\n Please enter Name in English.");	
		form1.studentsname_en.focus();
		return false;
	}
	if(
		!checkNaNull(form1.studentsname_en.value) &&
		!checkIsEnglishCharacter(form1.studentsname_en.value)
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>\n Please enter English character.");	
		form1.studentsname_en.focus();
		return false;
	}
	/**** Name END ****/
	
	/**** DOB START ****/
	if(!form1.StudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
		if(form1.StudentDateOfBirth.value!=''){
			alert(" <?=$kis_lang['Admission']['msg']['invaliddateformat']?>\n Invalid Date Format");
		}
		else{
			alert(" <?=$kis_lang['Admission']['msg']['enterdateofbirth']?>\n Please enter Date of Birth.");	
		}
		
		form1.StudentDateOfBirth.focus();
		return false;
	} 
	if(dOBRange[0] !='' && form1.StudentDateOfBirth.value < dOBRange[0] || dOBRange[1] !='' && form1.StudentDateOfBirth.value > dOBRange[1]){
		alert(" <?=$kis_lang['Admission']['msg']['invalidbdaydateformat']?>\n Invalid Birthday Range of Student");
		form1.StudentDateOfBirth.focus();
		return false;
	}
	/**** DOB END ****/

	/**** Gender START ****/
	if($.trim($('input:radio[name=StudentGender]:checked').val())==''){
		alert(" <?=$kis_lang['Admission']['msg']['selectgender']?>\n Please select Gender.");	
		form1.StudentGender[0].focus();
		return false;
	}
	/**** Gender END ****/
	
	/**** Personal Identification START ****/
	if($.trim(form1.StudentBirthCertNo.value)==''){
		alert(" <?=$kis_lang['Admission']['SHCK']['msg']['invalidBirthCertNo']?>\n Invalid Birth Cert No.");
		form1.StudentBirthCertNo.focus();
		return false;
	}
	
	/*if(checkBirthCertNo() > 0){
		alert(" <?=$kis_lang['Admission']['SHCK']['msg']['duplicateBirthCertNo']?>\n The Birth Cert No. is used for admission! Please enter another Birth Cert No.");	
		form1.StudentBirthCertNo.focus();
		return false;
	}*/
	/**** Personal Identification END ****/

	/**** Nationality START ****/
	if($.trim($('#Nationality').val())==''){
		alert(" <?=$kis_lang['Admission']['msg']['enternationality']?>\n Please enter Nationality.");	
		form1.Nationality.focus();
		return false;
	}
	/**** Nationality END ****/

	/**** Place Of Birth START ****/
	if($.trim($('#StudentPlaceOfBirth').val())==''){
		alert(" <?=$kis_lang['Admission']['msg']['enterplaceofbirth']?>\n Please enter Place of Birth.");	
		form1.StudentPlaceOfBirth.focus();
		return false;
	}
	/**** Place Of Birth END ****/

	/**** Religion START **** /
	if($.trim($('#ReligionOther').val())==''){
		alert(" <?=$kis_lang['Admission']['SSGC']['msg']['enterReligion']?>\n Please enter Religion.");	
		form1.ReligionOther.focus();
		return false;
	}
	/**** Religion END ****/
	
	/**** Address START ****/
	if($.trim($('#AddressChi').val())==''){
		alert(" <?=$kis_lang['Admission']['munsang']['msg']['enteraddress']?>\n Please enter Address.");	
		form1.AddressChi.focus();
		return false;
	}
	if($.trim($('#Address').val())==''){
		alert(" <?=$kis_lang['Admission']['munsang']['msg']['enteraddress']?>\n Please enter Address.");	
		form1.Address.focus();
		return false;
	}
	/**** Religion END ****/

	/**** Present School START **** /
	if($.trim($('#LastSchool').val())==''){
		alert(" <?=$kis_lang['Admission']['SSGC']['presentSchool']?>\n Please enter Present School.");	
		form1.LastSchool.focus();
		return false;
	}
	/**** Present School END ****/
	
	/**** Tel START ****/
	/*if(form1.HomeTelNo.value==''){
		alert(" <?=$kis_lang['Admission']['SSGC']['msg']['enterHomeTelNo']?>\n Please enter Home Tel.");	
		form1.HomeTelNo.focus();
		return false;
	}*/
	if(form1.HomeTelNo.value!='' && !/^[0-9]*$/.test(form1.HomeTelNo.value)){
		alert(" <?=$kis_lang['Admission']['SSGC']['msg']['invalidHomeTelNoFormat']?>\n Invalid Home Tel.");
		form1.HomeTelNo.focus();
		return false;
	}
	/**** Tel END ****/
	
	/**** Email START ****/
	if($.trim(form1.Email.value)==''){
		alert(" <?=$kis_lang['Admission']['icms']['msg']['entermailaddress']?>\n Please enter E-mail.");
		form1.Email.focus();
		return false;
	}
	if($.trim(form1.Email.value)!='' && !re.test(form1.Email.value)){
		alert(" <?=$kis_lang['Admission']['icms']['msg']['invalidmailaddress']?>\n Invalid E-mail Format");
		form1.Email.focus();
		return false;
	}
	if(typeof(form1.EmailConfirm) != 'undefined' && $.trim(form1.Email.value)!=$.trim(form1.EmailConfirm.value)){
		alert(" <?=$kis_lang['Admission']['SHCK']['msg']['emailMismatch']?>\n E-mail mismatch.");
		form1.EmailConfirm.focus();
		return false;
	}
	/**** Email END ****/
	
	/**** Desired Session START ****/
	if($('select[name^="OthersApplyDayType"]').length > 0){
    	if(
    		$('#OthersApplyDayType1').val() == '' &&
    		$('#OthersApplyDayType2').val() == '' &&
    		$('#OthersApplyDayType3').val() == ''
    	){
    		alert(" <?=$kis_lang['Admission']['SSGC']['msg']['selectDesiredSession']?>\n Please select Desired Session.");	
    		form1.LangForInterview[0].focus();
    		return false;
    	}
	}
	/**** Desired Session END ****/
	
	/**** Applied Before START ****/
	if($('[name="AppliedBefore"]:checked').val() == '1'){
    	if($('#AppliedBeforeClassDiv input:checked').length == 0){
    		alert(" <?=$kis_lang['Admission']['msg']['selectclass']?>\n Please select Class.");	
    		form1.AppliedBefore[0].focus();
    		return false;
    	}
	}
	/**** Applied Before END ****/
	/******** Student Info END ********/
	
	return true;
}
</script>