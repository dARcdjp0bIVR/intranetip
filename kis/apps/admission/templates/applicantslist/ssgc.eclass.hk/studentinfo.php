<?php

global $libkis_admission;

$allClassLevel = $libkis_admission->getClassLevel();

######## Get Student Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
$applicationInfo['studentApplicationInfoCust'] = $libkis_admission->getApplicationStudentInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
######## Get Student Cust Info END ########

?>
<table class="form_table">
	<tbody>
		<tr> 
			<td width="30%" class="field_title">
				<?= $kis_lang['Admission']['chinesename'] ?>
			</td>
			<td width="40%">
				<?=$applicationInfo['ChineseName']?>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin:0px;">
					<img src="<?= $attachmentList['personal_photo']['link'] ? $attachmentList['personal_photo']['link'] : $blankphoto ?>?_=<?= time() ?>"/>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['englishname'] ?>
			</td>
			<td>
				<?=$applicationInfo['EnglishName']?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['dateofbirth'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationInfo['dateofbirth']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['gender'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['Gender']]) ?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['SHCK']['birthCertNo'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationInfo['BirthCertNo']) ?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['nationality'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($applicationInfo['Nationality']);
				?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['placeofbirth'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($applicationInfo['PlaceOfBirth']);
				?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['SSGC']['religion'] ?>
			</td>
			<td>
				<?php
				    if($applicationInfo['ReligionOther'] == '' && $applicationInfo['Church'] == ''){
				        echo kis_ui::displayTableField('');
				    }else{
    			        echo kis_ui::displayTableField($applicationInfo['ReligionOther']);
    				    echo '&nbsp;' . $kis_lang['Admission']['SSGC']['religionShort'];
    				    
    				    echo '&nbsp;&nbsp;';
    				    
    			        echo kis_ui::displayTableField($applicationInfo['Church']);
    				    echo '&nbsp;' . $kis_lang['Admission']['SSGC']['church'];
				    }
				?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['district'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField("{$admission_cfg['DistrictArea'][ $applicationInfo['AddressDistrict'] ]} ({$admission_cfg['DistrictAddress'][ $applicationInfo['AddressDistrict'] ][ $applicationInfo['AddressEstate'] ]})");
				?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['SSGC']['addressChinese'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField(nl2br($applicationInfo['AddressChi']));
				?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['SSGC']['addressEnglish'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField(nl2br($applicationInfo['Address']));
				?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['SSGC']['presentSchool'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($applicationInfo['LastSchool']);
				?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['SSGC']['homeTel'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($applicationInfo['HomeTelNo']);
				?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['contactEmail'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($applicationInfo['Email']);
				?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['SSGC']['desiredSession'] ?>
			</td>
			<td>
				<?php
        		    $options = array();
        		    for($i=1;$i<=3;$i++){
        		        if($applicationInfo['ApplyDayType'.$i]){
        		            $options[] = "{$kis_lang['Admission']['Option']} {$i}: {$kis_lang['Admission']['TimeSlot'][ $applicationInfo['ApplyDayType'.$i] ]}";
        		        }
        		    }
			        echo kis_ui::displayTableField(implode('<br />', $options));
				?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['SSGC']['langSpoken'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField(($applicationInfo['LangSpokenAtHome'])?$kis_lang['Admission']['yes3']:$kis_lang['Admission']['no3']);
				?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['SSGC']['appliedBefore'] ?>
			</td>
			<td>
				<?php
				    if($applicationCustInfo['AppliedBefore'][0]['Value']){
				        $appliedBeforeClass = array();
				        foreach($applicationCustInfo['AppliedBeforeClass'] as $data){
				            $appliedBeforeClass[] = $allClassLevel[$data['Value']];
				        }
				        $appliedBeforeClassHTML = implode(', ', $appliedBeforeClass);
				        echo "{$kis_lang['Admission']['yes']} ({$appliedBeforeClassHTML})";
				    }else{
				        echo $kis_lang['Admission']['no'];
				    }
				?>
			</td>
		</tr>
		
		
		
		
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['document'] ?>
			</td>
			<td colspan="2">
				<?php
					$attachmentAry = array();
					foreach ($attachmentList as $_type => $_attachmentAry) {
						if ($_type != 'personal_photo') {
							if ($_attachmentAry['link']) {
								//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
								$attachmentAry[] = '<a href="' . $_attachmentAry['link'] . '" target="_blank">' . $_type . '</a>';
							}
						}
					}
				?>
				<?= count($attachmentAry) == 0 ? '--' : implode('<br/>', $attachmentAry); ?>
			</td>
		</tr>                                                                                 
	</tbody>
</table>