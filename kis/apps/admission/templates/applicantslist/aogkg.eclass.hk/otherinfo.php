<?php

global $libkis_admission;


#### Get Student Cust Info START ####
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
#### Get Student Cust Info START ####


?>

<table id="dataTable1" class="form_table" >
<colgroup>
    <col style="width:30%">
    <col style="">
</colgroup>

<tbody>
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$kis_lang['Admission']['AOGKG']['currentStudySibiling']?>
    		<br />
    		
    	</td>
    	<td>
    		<div class="otherDiv">
    			<label for="CurrentStudySibiling_Name">
    				<?=$kis_lang['Admission']['name'] ?>
    				: 
    			</label>
    			<?=$allCustInfo['CurrentStudySibiling_Name'][0]['Value'] ?>
    		</div>
    		<div class="otherDiv">
    			<label for="CurrentStudySibiling_Class">
    				<?=$kis_lang['Admission']['class'] ?>
    				: 
    			</label>
    			<?=$allCustInfo['CurrentStudySibiling_Class'][0]['Value'] ?>
    		</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$kis_lang['Admission']['AOGKG']['alunmusSibiling']?>
    		<br />
    		
    	</td>
    	<td>
    		<div class="otherDiv">
    			<label for="AlunmusSibiling_Name">
    				<?=$kis_lang['Admission']['name'] ?>
    				: 
    			</label>
    			<?=$allCustInfo['AlunmusSibiling_Name'][0]['Value'] ?>
    		</div>
    		<div class="otherDiv">
    			<label for="CurrentStudySibiling_GraduationYear">
    				<?=$kis_lang['Admission']['GradYear'] ?>
    				: 
    			</label>
    			<?=$allCustInfo['CurrentStudySibiling_GraduationYear'][0]['Value'] ?>
    		</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$kis_lang['Admission']['AOGKG']['memberOfFanling']?>
    		<br />
    		
    	</td>
    	<td>
    		<div class="otherDiv">
    			<label for="MemberOfFanling_Name">
    				<?=$kis_lang['Admission']['name'] ?>
    				: 
    			</label>
    			<?=$allCustInfo['MemberOfFanling_Name'][0]['Value'] ?>
    			&nbsp;
    			&nbsp;
    			(
    			<label for="MemberOfFanling_MembershipNumber">
    				<?=$kis_lang['Admission']['AOGKG']['membershipNumber'] ?>
    				: 
    			</label>
    			<?=$allCustInfo['MemberOfFanling_MembershipNumber'][0]['Value'] ?>
    			)
    		</div>
    		<div class="otherDiv">
    			<label for="MemberOfFanling_Relationship">
    				<?=$kis_lang['Admission']['AOGKG']['membershipReleationship'] ?>
    				: 
    			</label>
    			<?=($allCustInfo['MemberOfFanling_Relationship'][0]['Value'])?$kis_lang['Admission']['PG_Type'][$allCustInfo['MemberOfFanling_Relationship'][0]['Value']]:'--' ?>
    		</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$kis_lang['Admission']['AOGKG']['twinApply']?>
    		
    	</td>
    	<td>
			<label for="TwinApply_Name">
				<?=$kis_lang['Admission']['name'] ?>
				: 
			</label>
			<?=$allCustInfo['TwinApply_Name'][0]['Value'] ?>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$kis_lang['Admission']['remarks']?>
    		
    	</td>
    	<td>
			<?=$allCustInfo['Remarks'][0]['Value'] ?>
    	</td>
    </tr>
    
</tbody>


</table>
