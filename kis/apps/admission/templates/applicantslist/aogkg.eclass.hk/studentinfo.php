<?php

global $libkis_admission;

$allClassLevel = $libkis_admission->getClassLevel();

######## Get Student Cust Info START ########
$applicationStudentInfo = $applicationInfo;
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationStudentInfo['applicationID']);
$applicationStudentInfo['studentApplicationInfoCust'] = $libkis_admission->getApplicationStudentInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
######## Get Student Cust Info END ########

?>
<table class="form_table">
	<tbody>
	
		<!-- ######## Basic Information START ######## -->
		<tr> 
			<td width="30%" class="field_title">
				<?= $kis_lang['Admission']['chinesename'] ?>
			</td>
			<td width="40%">
				<?=$applicationStudentInfo['ChineseName']?>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin:0px;">
					<img src="<?= $attachmentList['personal_photo']['link'] ? $attachmentList['personal_photo']['link'] : $blankphoto ?>?_=<?= time() ?>"/>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['englishname'] ?>
			</td>
			<td>
				<?=$applicationStudentInfo['EnglishName']?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['dateofbirth'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationStudentInfo['dateofbirth']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['gender'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationStudentInfo['Gender']]) ?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['SHCK']['birthCertNo'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationStudentInfo['BirthCertNo']) ?>
			</td>
		</tr>
		<!-- ######## Basic Information END ######## -->
		
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['langspokenathome'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($applicationStudentInfo['LangSpokenAtHome']);
				?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['placeofbirth'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($applicationStudentInfo['PlaceOfBirth']);
				?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['Age'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($applicationStudentInfo['Age']);
				?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['AOGKG']['homeTel'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($applicationStudentInfo['HomeTelNo']);
				?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['mobilephoneno'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($applicationStudentInfo['MobileNo']);
				?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['email'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($applicationStudentInfo['Email']);
				?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['district'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($admission_cfg['District'][$applicationStudentInfo['AddressDistrict']]);
				?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['AOGKG']['address'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($applicationStudentInfo['Address']);
				?>
			</td>
		</tr>
		
		
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['AOGKG']['sessionPreference'] ?>
			</td>
			<td colspan="2">
				<?php
        		    
				    if($applicationStudentInfo['ApplyDayType1'] == '1'){
				        echo $kis_lang['Admission']['AOGKG']['sessionPreferenceType'][1] . '<br />';
				        
				        if($applicationStudentInfo['ApplyDayType2'] == '2'){
				            echo $kis_lang['Admission']['AOGKG']['sessionPmHint'];
				            echo $kis_lang['Admission']['AOGKG']['sessionPmCanSelect'];
				        }else{
				            echo $kis_lang['Admission']['AOGKG']['sessionPmHint'];
				            echo $kis_lang['Admission']['AOGKG']['sessionPmCannotSelect'];
				        }
				    }else{
				        echo $kis_lang['Admission']['AOGKG']['sessionPreferenceType'][2];
				    }
				?>
			</td>
		</tr>
		
		
		
		<!-- ######## Document START ######## -->
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['document'] ?>
			</td>
			<td colspan="2">
				<?php
					$attachmentAry = array();
					foreach ($attachmentList as $_type => $_attachmentAry) {
						if ($_type != 'personal_photo') {
							if ($_attachmentAry['link']) {
								//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
								$attachmentAry[] = '<a href="' . $_attachmentAry['link'] . '" target="_blank">' . $_type . '</a>';
							}
						}
					}
				?>
				<?= count($attachmentAry) == 0 ? '--' : implode('<br/>', $attachmentAry); ?>
			</td>
		</tr>    
		<!-- ######## Document END ######## -->                                                                             
	</tbody>
</table>