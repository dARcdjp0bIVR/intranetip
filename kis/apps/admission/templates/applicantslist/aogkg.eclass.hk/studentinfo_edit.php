<?php

global $libkis_admission;
$allClassLevel = $libkis_admission->getClassLevel();
$star = $mustfillinsymbol;
$applicationStudentInfo = $applicationInfo;

######## Get Student Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationStudentInfo['applicationID']);
$applicationStudentInfo['studentApplicationInfoCust'] = $libkis_admission->getApplicationOthersInfo($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
$applicationStudentInfo['studentApplicationInfoCust'] = $applicationStudentInfo['studentApplicationInfoCust'][0];
$applyDayType1 = $applicationStudentInfo['studentApplicationInfoCust']['ApplyDayType1'];
$applyDayType2 = $applicationStudentInfo['studentApplicationInfoCust']['ApplyDayType2'];
$applyDayType3 = $applicationStudentInfo['studentApplicationInfoCust']['ApplyDayType3'];
######## Get Student Cust Info END ########
?>
<style>
select:disabled{
    color: #ccc;
}
</style>

<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />
<table class="form_table">
	<colgroup>
		<col style="width: 30%;"/>
		<col style="width: 40%;"/>
		<col style="width: 30%;"/>
	</colgroup>
	<tbody>
	
	
		<!-- ######## Basic Information START ######## -->
		<tr> 
			<td width="30%" class="field_title">
				<?=$star?><?= $kis_lang['Admission']['chinesename'] ?>
			</td>
			<td width="40%">
				<?=$libinterface->GET_TEXTBOX('studentsname_b5', 'studentsname_b5', $applicationStudentInfo['student_name_b5'], $OtherClass='', $OtherPar=array())?>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin:0px;">
					<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
					<div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
						<a id = "btn_remove" href="#" class="btn_remove"></a>
					</div>
					<div class="text_remark" style="text-align:center;">
						<?=$kis_lang['Admission']['msg']['clicktouploadphoto']?>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?><?= $kis_lang['Admission']['englishname'] ?>
			</td>
			<td>
			<?=$libinterface->GET_TEXTBOX('studentsname_en', 'studentsname_en', $applicationStudentInfo['student_name_en'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?=$star?><?= $kis_lang['Admission']['dateofbirth'] ?>
			</td>
			<td>
			<input name="StudentDateOfBirth" id="StudentDateOfBirth" value="<?=$applicationStudentInfo['dateofbirth']?>">&nbsp;
			<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?><?= $kis_lang['Admission']['gender'] ?>
			</td>
    		<td>
    			<?=$libinterface->Get_Radio_Button('StudentGender1', 'StudentGender', 'M', ($applicationStudentInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
    			<?=$libinterface->Get_Radio_Button('StudentGender2', 'StudentGender', 'F', ($applicationStudentInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>                              
    		</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$star?><?=$kis_lang['Admission']['SHCK']['birthCertNo'] ?>
			</td>
			<td nowrap="" colspan="2">
    			<input name="StudentBirthCertNo" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" value="<?=$applicationStudentInfo['birthcertno'] ?>"/>
    			<br/>
    			<?=$kis_lang['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
			</td>
		</tr>
		<!-- ######## Basic Information END ######## -->
		
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['langspokenathome'] ?>
			</td>
			<td>
    			<input id="LangSpokenAtHome" name="LangSpokenAtHome" class="textboxtext" value="<?=$applicationStudentInfo['LangSpokenAtHome'] ?>">
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['placeofbirth'] ?>
			</td>
			<td>
    			<input id="StudentPlaceOfBirth" name="StudentPlaceOfBirth" class="textboxtext" value="<?=$applicationStudentInfo['PlaceOfBirth'] ?>">
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['Age'] ?>
			</td>
			<td>
    			<input id="Age" name="Age" class="textboxtext" value="<?=$applicationStudentInfo['Age'] ?>">
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['AOGKG']['homeTel'] ?>
			</td>
			<td>
    			<input id="HomeTelNo" name="HomeTelNo" class="textboxtext" value="<?=$applicationStudentInfo['HomeTelNo'] ?>">
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['mobilephoneno'] ?>
			</td>
			<td>
    			<input id="MobileNo" name="MobileNo" class="textboxtext" value="<?=$applicationStudentInfo['MobileNo'] ?>">
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['email'] ?>
			</td>
			<td>
    			<input id="Email" name="Email" class="textboxtext" value="<?=$applicationStudentInfo['Email'] ?>">
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['district'] ?>
			</td>
			<td>
    			<?php

    			foreach($admission_cfg['District'] as $id => $val){
    			    $checked = ($applicationStudentInfo['AddressDistrict'] == $id)? 'checked' : '';
			    ?>
					<input type="radio" id="AddressDistrict<?=$id ?>" name="AddressDistrict" value="<?=$id ?>" <?=$checked ?>/>
					<label for="AddressDistrict<?=$id ?>"><?=$val ?></label>
					<br />
				<?php
				    }
    			?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['AOGKG']['address'] ?>
			</td>
			<td>
    			<input id="Address" name="Address" class="textboxtext" value="<?=$applicationStudentInfo['Address'] ?>">
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['AOGKG']['sessionPreference'] ?>
			</td>
			<td>
				<?php
    				$dayType1AmChecked = ($applyDayType1 === '1')? 'checked' : '';
    				$dayType1PmChecked = ($applyDayType1 === '2')? 'checked' : '';
    				$dayType2PmCanSelectChecked = ($applyDayType2)? 'checked' : '';
    				$dayType2PmCannotSelectChecked = (!$applyDayType2)? 'checked' : '';
				?>
				
    			<div>
    				<input type="radio" id="OthersApplyDayType1Am" name="OthersApplyDayType1" value="1" <?=$dayType1AmChecked ?> />
    				<label for="OthersApplyDayType1Am">
    					<?=$kis_lang['Admission']['AOGKG']['sessionPreferenceType'][1] ?>
    				</label>
    			</div>
    			<div id="dayType2" style="margin-left: 27px;display: none;">
    				<table>
    					<tr>
    						<td rowspan="2" style="width: 160px;">
        						<?=$star ?>
            					<?=$kis_lang['Admission']['AOGKG']['sessionPmHint'] ?>
    						</td>
    						<td>
    							<input type="radio" id="OthersApplyDayType2CanSelect" name="OthersApplyDayType2" value="2" <?=$dayType2PmCanSelectChecked ?> />
                				<label for="OthersApplyDayType2CanSelect">
                					<?=$kis_lang['Admission']['AOGKG']['sessionPmCanSelect'] ?>
                				</label>
    						</td>
    					</tr>
    					<tr>
    						<td>
    							<input type="radio" id="OthersApplyDayType2CannotSelect" name="OthersApplyDayType2" value="" <?=$dayType2PmCannotSelectChecked ?> />
                				<label for="OthersApplyDayType2CannotSelect">
                					<?=$kis_lang['Admission']['AOGKG']['sessionPmCannotSelect'] ?>
                				</label>
    						</td>
    					</tr>
    				</table>
    			</div>
    			
    			<div>
    				<input type="radio" id="OthersApplyDayType1Pm" name="OthersApplyDayType1" value="2" <?=$dayType1PmChecked ?> />
    				<label for="OthersApplyDayType1Pm">
    					<?=$kis_lang['Admission']['AOGKG']['sessionPreferenceType'][2] ?>
    				</label>
    			</div>
			</td>
		</tr>
		
		
		
		
		
		<!-- ######## Document START ######## --> 
    	<?php
    	for($i=0;$i<sizeof($attachmentSettings);$i++) {
    		$attachment_name = $attachmentSettings[$i]['AttachmentName'];
    		
    		$_filePath = $attachmentList[$attachment_name]['link'];
    		if($_filePath){
    			$_spanDisplay = '';
    			$_buttonDisplay = ' style="display:none;"';
    		}else{
    			$_filePath = '';
    			$_spanDisplay = ' style="display:none;"';
    			$_buttonDisplay = '';
    		}
    		
    		$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
    		$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';
    	?>
    		<tr>
    			<td class="field_title">
    				<?=$attachment_name?>
    			</td>
    			<td id="<?=$attachment_name?>">
    				<span class="view_attachment" <?=$_spanDisplay?>><?=$_attachment?></span>
    				<input type="button" class="attachment_upload_btn formsmallbutton" value="<?=$kis_lang['Upload']?>"  id="uploader-<?=$attachment_name?>" <?=$_buttonDisplay?>/>
    			</td>
    		</tr>	
    	<?php
    	}
    	?>
		<!-- ######## Document END ######## --> 
	</tbody>
</table>

<script>
//// UI Releated START ////
function updateUI(){
	if($('input[name="OthersApplyDayType1"]:checked').val() == '1'){
		$('#dayType2').slideDown();
	}else{
		$('#dayType2').slideUp();
	}
}

$('input[name="OthersApplyDayType1"]').click(updateUI);
updateUI();
////UI Releated END ////

$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	
	return false;
});

function check_hkid(hkid) {
	var re = /^([A-Z]{1})((\d){6})\({0,1}([A0-9]{1})\){0,1}$/g;
	var ra = re.exec(hkid);

	if (ra != null) {
		var p1 = ra[1];
		var p2 = ra[2];
		var p3 = ra[4];
		var check_sum = 0;
		if (p1.length == 2) {
			check_sum = (p1.charCodeAt(0)-55) * 9 + (p1.charCodeAt(1)-55) * 8;
		}
		else if (p1.length == 1){
			check_sum = 324 + (p1.charCodeAt(0)-55) * 8;
		}

		check_sum += parseInt(p2.charAt(0)) * 7 + parseInt(p2.charAt(1)) * 6 + parseInt(p2.charAt(2)) * 5 + parseInt(p2.charAt(3)) * 4 + parseInt(p2.charAt(4)) * 3 + parseInt(p2.charAt(5)) * 2;
		var check_digit = 11 - (check_sum % 11);
		if (check_digit == '11') {
			check_digit = 0;
		}
		else if (check_digit == '10') {
			check_digit = 'A';
		}
		if (check_digit == p3 ) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}
	
function checkValidForm(){
	var re = /\S+@\S+\.\S+/;
	var dOBRange = new Array();
	   $.ajax({
	       url: "admission_form/ajax_get_bday_range.php",
	       type: "post",
	       data: $("#applicant_form").serialize(),
	       async: false,
	       success: function(data){
	           //alert("debugging: The classlevel is updated!");
	           dOBRange = data.split(",");
	       },
	       error:function(){
	           //alert("failure");
	           $("#result").html('There is error while submit');
	       }
	   });


	var form1 = applicant_form;

	/******** Student Info START ********/
	/**** Name START ****/
	if($.trim(form1.studentsname_b5.value)==''){
		alert(" <?=$kis_lang['Admission']['msg']['enterchinesename']?>\n Please enter Name in Chinese.");	
		form1.studentsname_b5.focus();
		return false;
	}
	if(
		!checkNaNull(form1.studentsname_b5.value) && 
		!checkIsChineseCharacter(form1.studentsname_b5.value)
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>\n Please enter Chinese character.");	
		form1.studentsname_b5.focus();
		return false;
	}
	
	if($.trim(form1.studentsname_en.value)==''){
		alert(" <?=$kis_lang['Admission']['msg']['enterenglishname']?>\n Please enter Name in English.");	
		form1.studentsname_en.focus();
		return false;
	}
	if(
		!checkNaNull(form1.studentsname_en.value) &&
		!checkIsEnglishCharacter(form1.studentsname_en.value)
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>\n Please enter English character.");	
		form1.studentsname_en.focus();
		return false;
	}
	/**** Name END ****/
	
	/**** DOB START ****/
	if(!form1.StudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
		if(form1.StudentDateOfBirth.value!=''){
			alert(" <?=$kis_lang['Admission']['msg']['invaliddateformat']?>\n Invalid Date Format");
		}
		else{
			alert(" <?=$kis_lang['Admission']['msg']['enterdateofbirth']?>\n Please enter Date of Birth.");	
		}
		
		form1.StudentDateOfBirth.focus();
		return false;
	} 
	if(dOBRange[0] !='' && form1.StudentDateOfBirth.value < dOBRange[0] || dOBRange[1] !='' && form1.StudentDateOfBirth.value > dOBRange[1]){
		alert(" <?=$kis_lang['Admission']['msg']['invalidbdaydateformat']?>\n Invalid Birthday Range of Student");
		form1.StudentDateOfBirth.focus();
		return false;
	} 
	/**** DOB END ****/

	/**** Gender START ****/
	if($.trim($('input:radio[name=StudentGender]:checked').val())==''){
		alert(" <?=$kis_lang['Admission']['msg']['selectgender']?>\n Please select Gender.");	
		form1.StudentGender[0].focus();
		return false;
	}
	/**** Gender END ****/
	
	/**** Personal Identification START ****/
	if(!check_hkid(form1.StudentBirthCertNo.value)){
		alert(" <?=$kis_lang['Admission']['SHCK']['msg']['invalidBirthCertNo']?>\n Invalid Birth Cert No.");
		form1.StudentBirthCertNo.focus();
		return false;
	}
	
	/*if(checkBirthCertNo() > 0){
		alert(" <?=$kis_lang['Admission']['SHCK']['msg']['duplicateBirthCertNo']?>\n The Birth Cert No. is used for admission! Please enter another Birth Cert No.");	
		form1.StudentBirthCertNo.focus();
		return false;
	}*/
	/**** Personal Identification END ****/


	
	
	/**** Lang spoken at home START ****/
	if($.trim($('#LangSpokenAtHome').val())==''){
		alert(" <?=$kis_lang['Admission']['msg']['enterlangspokenathome']?>");	
		form1.LangSpokenAtHome.focus();
		return false;
	}
	/**** Lang spoken at home END ****/
	
	/**** Place Of Birth START ****/
	if($.trim($('#StudentPlaceOfBirth').val())==''){
		alert(" <?=$kis_lang['Admission']['msg']['enterplaceofbirth']?>");	
		form1.StudentPlaceOfBirth.focus();
		return false;
	}
	/**** Place Of Birth END ****/
	
	/**** Age START ****/
	if($.trim($('#Age').val())==''){
		alert(" <?=$kis_lang['Admission']['msg']['enterage']?>");	
		form1.Age.focus();
		return false;
	}
	if(!/^[0-9]*$/.test(form1.Age.value)){
		alert(" <?=$kis_lang['Admission']['msg']['invalidage']?>");
		form1.Age.focus();
		return false;
	}
	/**** Age END ****/

	/**** Tel START ****/
	if(form1.HomeTelNo.value==''){
		alert(" <?=$kis_lang['Admission']['AOGKG']['msg']['homeTel']?>");	
		form1.HomeTelNo.focus();
		return false;
	}
	if(!/^[0-9]*$/.test(form1.HomeTelNo.value)){
		alert(" <?=$kis_lang['Admission']['AOGKG']['msg']['invalidHomeTelNoFormat']?>");
		form1.HomeTelNo.focus();
		return false;
	}
	/**** Tel END ****/

	/**** Mobile START ****/
	if(form1.MobileNo.value==''){
		alert(" <?=$kis_lang['Admission']['msg']['entermobilephoneno']?>");	
		form1.MobileNo.focus();
		return false;
	}
	if(!/^[0-9]*$/.test(form1.MobileNo.value)){
		alert(" <?=$kis_lang['Admission']['msg']['invalidmobilephoneno']?>");
		form1.MobileNo.focus();
		return false;
	}
	/**** Mobile END ****/

	/**** Email START ****/
	if($.trim(form1.Email.value)==''){
		alert(" <?=$kis_lang['Admission']['icms']['msg']['entermailaddress']?>");
		form1.Email.focus();
		return false;
	}
	if($.trim(form1.Email.value)!='' && !re.test(form1.Email.value)){
		alert(" <?=$kis_lang['Admission']['icms']['msg']['invalidmailaddress']?>");
		form1.Email.focus();
		return false;
	}
	/**** Email END ****/

	/**** District START ****/
	if($('input[name="AddressDistrict"]:checked').length == 0){
		alert(" <?=$kis_lang['Admission']['HKUGAPS']['msg']['selectDistrict']?>");	
		form1.AddressDistrict[0].focus();
		return false;
	}
	/**** District END ****/

	/**** Address START ****/
	if($.trim($('#Address').val())==''){
		alert(" <?=$kis_lang['Admission']['AOGKG']['msg']['enterAddress']?>.");	
		form1.Address.focus();
		return false;
	}
	/**** Address END ****/
	
	/**** Session Preference START ****/
	if($('input[name="OthersApplyDayType1"]:checked').length == 0){
		alert(" <?=$kis_lang['Admission']['AOGKG']['msg']['selectSessionPreference']?>");	
		form1.OthersApplyDayType1Am.focus();
		return false;
	}
	if(
		$('input[name="OthersApplyDayType1"]:checked').val() == '1' &&
		$('input[name="OthersApplyDayType2"]:checked').length == 0
	){
		alert(" <?=$kis_lang['Admission']['AOGKG']['msg']['selectSessionPreference']?>");	
		form1.OthersApplyDayType2CanSelect.focus();
		return false;
	}
	/**** Session Preference END ****/
	/******** Student Info END ********/
	
	return true;
}
</script>