<?php

global $libkis_admission;

$parentInfoArr = $applicationInfo;

######## Get Parent Cust Info START ########
//$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($parentInfoArr['applicationID']);
######## Get Parent Cust Info END ########
?>
<table class="form_table">
	<colgroup>
        <col style="width:30%">
        <col style="width:23%">
        <col style="width:23%">
        <col style="width:24%">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['PG_Type']['F'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['PG_Type']['M'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['PG_Type']['G'] ?></center></td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['chinesename'] ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['F']['ChineseName']) ?>
    	</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['M']['ChineseName']) ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['G']['ChineseName']) ?>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['englishname'] ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['F']['EnglishName']) ?>	
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['M']['EnglishName']) ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['G']['EnglishName']) ?>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['phoneno'] ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['F']['Mobile']) ?>	
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['M']['Mobile']) ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['G']['Mobile']) ?>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['AOGKG']['nameOfOffice'] ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['F']['Company']) ?>	
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['M']['Company']) ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['G']['Company']) ?>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['AOGKG']['position'] ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['F']['JobPosition']) ?>	
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['M']['JobPosition']) ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($parentInfoArr['G']['JobPosition']) ?>
		</td>
	</tr>
	
</table>