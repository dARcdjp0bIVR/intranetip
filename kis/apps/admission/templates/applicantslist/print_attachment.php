<?php
/// Editing by: 
/*
 * 2014-11-27 (Henry) [ip2.5.5.12.1]: file created
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");

//include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."kis/config.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

intranet_auth();
intranet_opendb();

$ldb = new libdb();
$lfs = new libfilesystem();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

if(sizeof($ApplicationID)==0 && $AppID!="") {
	$ApplicationID = explode(',', $AppID);
	$cond = " AND r.ApplicationID IN ('".implode("','",$ApplicationID)."')";
}

if($FromNo!="" && $ToNo!="") {
	$cond  = " AND (r.ApplicationID >= '".$FromNo."' AND r.ApplicationID <= '".$ToNo."')";
//	debug_pr(similar_text($FromNo, $ToNo));
//	debug_pr($ToNo - $FromNo);
}
//exit();

$FolderPath = $file_path."/file/admission/";
$FilePath = $admission_cfg['FilePath'];

if($type){
	$cond .=  " AND  r.AttachmentType = '".$type."'";
}
?>
<style type="text/css">
.table_img {
    border-collapse: collapse;
}

.table_img, .table_img th, .table_img td {
    border: 1px solid black;
}
.td_img {
    height: 200px;
    width: 150px;
    vertical-align: center;
    	text-align: center;
}

.td_img img{
    height: auto;
    width: auto;
	max-width: 150px;
}

table { page-break-inside:auto }
   tr    { page-break-inside:avoid; page-break-after:auto }
@media print
{    
    .print_hide, .print_hide *
    {
        display: none !important;
    }
}
</style>
<?
if($_SESSION['UserType']==USERTYPE_STAFF){
	
	$sql = "Select count(*)
			from ADMISSION_OTHERS_INFO r
			where (r.ApplicationID >= '".$FromNo."' AND r.ApplicationID <= '".$ToNo."')";;
			
	$countAttachmentAry = $lac->returnArray($sql);
	if($countAttachmentAry[0][0] > 100){
		echo $kis_lang['msg']['rangeOfApplcationNoLimit'] ;
		exit();
	}
	
	$sql = "Select r.ApplicationID, r.AttachmentType, CONCAT(o.RecordID,'/',IF(r.AttachmentType = 'personal_photo',r.AttachmentType,'other_files'),'/',r.AttachmentName), o.ApplyYear schoolYearID, o.RecordID RecordID   
			From ADMISSION_ATTACHMENT_RECORD r
			INNER JOIN
				ADMISSION_OTHERS_INFO o 
			ON r.ApplicationID = o.ApplicationID
			where 1 ".$cond;
			
	$attachmentAry = $lac->returnArray($sql);

//	debug_pr($attachmentAry);
//	exit();
	
	$printButton = '<div id="printOption" style="float: right"><table width="90%" align="center" class="print_hide" border="0">
			<tr>
				<td align="right">'.$lauc->GET_ACTION_BTN($kis_lang['print'], "button", "javascript:window.print();").'</td>
			</tr>
		</table></div>';

	//echo $printButton;
	
	$x .="<h1>".($type=='personal_photo'?$kis_lang['Admission']['personalPhoto']:$type)."</h1>";
	$x .= "<table class='".($type=='personal_photo'||true?'table_img':'')."'>";
	
	$cnt = 0;
	for($i=0; $i<sizeof($attachmentAry); $i++) { 
			list($ApplicationID, $AttachmentType, $FileFullPath, $schoolYearID, $RecordID) = $attachmentAry[$i];
			if(!file_exists($FolderPath.$FileFullPath)){
				continue;
			}
			
			$editPath = "/kis/#/apps/admission/applicantslist/edit/".$schoolYearID."/".$RecordID."/studentinfo/";

			if($cnt % 10 == 0 || $type !='personal_photo' && false)
				$x .= "<tr>";
				
			$x .= "<td class='td_img'>";
			
			$a = getimagesize($FolderPath.$FileFullPath);
    		$image_type = $a[2];
		    if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
		    {
		        $x.="[<a target='_blank' href='".$editPath."'>".$kis_lang['edit']."</a>]<br/><img src='".$FilePath.$FileFullPath."'></img><a target='_blank' href='".$FilePath.$FileFullPath."'><br/>".$ApplicationID." <br/>(".$AttachmentType.")"."</a>";
		    }
		    else{
		    	$x.="[<a target='_blank' href='".$editPath."'>".$kis_lang['edit']."</a>]<br/><p nowrap>".$kis_lang['msg']['notAnImageFile']."</p><br/><a target='_blank' href='".$FilePath.$FileFullPath."'><br/>".$ApplicationID." <br/>(".$AttachmentType.")"."</a>";
		    }
			$x.="</td>";
			
			if($cnt % 10 == 9 && $i > 0 || $type !='personal_photo' && false)
				$x .= "</tr>";
			
			$cnt++;
		}
	
	$x .= "</table>";	
	echo $x;	
	//echo $printButton;
	
}

?>