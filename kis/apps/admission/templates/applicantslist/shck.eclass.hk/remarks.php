<?php

?>

<table class="form_table">
	<tbody>
		<tr>
			<td width="30%" class="field_title"><?=$kis_lang['Admission']['applicationstatus']?></td>
			<td width="70%"><?=kis_ui::displayTableField($kis_lang['Admission']['Status'][$applicationInfo['status']])?></td>
		</tr>
		<?if(!$sys_custom['KIS_Admission']['ICMS']['Settings']){?>                              
			<tr>
				<td class="field_title"><?=$kis_lang['Admission']['applicationfee']?></td>
				<td>
					<table style="">
						<?if($paypalPaymentInfo[0]['payment_status']){?>
						<tr>
							<td colspan="4"><font color="green"><?=$kis_lang['Admission']['KTLMSKG']['paidByPayPal']?></font><br/>
							<?foreach($paypalPaymentInfo as $apaypalPaymentInfo){?>
							(<?=$kis_lang['Admission']['PaymentsStatus']?>) <?=$apaypalPaymentInfo['payment_status']?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PaymentItem']?>) <?=$apaypalPaymentInfo['item_name']?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PaymentTotal']?>) <?=$apaypalPaymentInfo['mc_currency']?> <?=$apaypalPaymentInfo['mc_gross']?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PaymentDate']?>) <?=date('Y-m-d H:i:s', strtotime($apaypalPaymentInfo['payment_date']))?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PayerEmail']?>) <?=$apaypalPaymentInfo['payer_email']?><br/><br/>
							<?}?>
							</td>
						</tr>
						<tr>
							<td colspan="4">&nbsp;</td>
						</tr>
						<?}?>
						<tr>
							<td>(<?=$kis_lang['Admission']['KTLMSKG']['bankName']?>)</td>
							<td><?=kis_ui::displayTableField($applicationInfo['FeeBankName'])?></td>
							<td>(<?=$kis_lang['Admission']['KTLMSKG']['chequeNum']?>)</td>
							<td><?=kis_ui::displayTableField($applicationInfo['FeeChequeNo'])?></td>
						</tr>
						<tr>
							<td>(<?=$kis_lang['Admission']['receiptcode']?>)</td>
							<td colspan="3"><?=kis_ui::displayTableField($applicationInfo['receiptID'])?></td>
						</tr>
						<tr>
							<td width="20%">(<?=$kis_lang['date']?>)</td>
							<td width="30%"><?=kis_ui::displayTableField($applicationInfo['receiptdate'])?></td>                               			
							<td width="20%">(<?=$kis_lang['Admission']['handler']?>)</td>
							<td width="30%"><?=kis_ui::displayTableField($applicationInfo['handler'])?></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?=$kis_lang['receiptcopy']?></td>
				<td>
					<?
					$attachmentAry = array();
					foreach($attachmentList as $_type => $_attachmentAry){
						if($_type == 'receipt_copy'){
							if($_attachmentAry['link']){
								//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
								$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['view'].'</a>';
							}
						}
					}
					?>
					<?=count($attachmentAry)==0?'--':implode('<br/>',$attachmentAry);?>
				</td>
			</tr>
			<?if(!$sys_custom['KIS_Admission']['InterviewSettings']){?>
				<tr>
					<td class="field_title"><?=$kis_lang['Admission']['interviewdate']?></td>
					<td><?=kis_ui::displayTableField(substr($applicationInfo['interviewdate'], 0, -3))?></td>
				</tr>
				<tr>
					<td class="field_title"><?=$kis_lang['Admission']['interviewlocation']?></td>
					<td><?=kis_ui::displayTableField($applicationInfo['interviewlocation'])?></td>
				</tr>
			<?}?>
			<?if($sys_custom['KIS_Admission']['InterviewSettings']){?>
				<tr>
					<td class="field_title"><?=$kis_lang['Admission']['interviewdate']?></td>
					<td>
						(1) <?=kis_ui::displayTableField($interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'--')?><br/>
						(2) <?=kis_ui::displayTableField($interviewInfo2?$interviewInfo2['Date'].' ('.substr($interviewInfo2['StartTime'], 0, -3).' ~ '.substr($interviewInfo2['EndTime'], 0, -3).')'.($interviewInfo2['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo2['GroupName']:''):'--')?><br/>
						(3) <?=kis_ui::displayTableField($interviewInfo3?$interviewInfo3['Date'].' ('.substr($interviewInfo3['StartTime'], 0, -3).' ~ '.substr($interviewInfo3['EndTime'], 0, -3).')'.($interviewInfo3['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo3['GroupName']:''):'--')?>
					</td>
				</tr>
			<?}?>
			<tr>
				<td class="field_title"><?=$kis_lang['Admission']['isnotified']?></td>
				<td><?=$kis_lang[$applicationInfo['isnotified']]?></td>
			</tr>
		<?}?>
		<?if($sys_custom['KIS_Admission']['ICMS']['Settings']){?>
			<tr>
				<td class="field_title"><?=$kis_lang['Admission']['interviewdate']?></td>
				<td><?=kis_ui::displayTableField(substr($applicationInfo['interviewdate'], 0, -3))?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$kis_lang['Admission']['interviewlocation']?></td>
				<td><?=kis_ui::displayTableField($applicationInfo['interviewlocation'])?></td>
			</tr>
		<?}?>	
		<tr>
			<td class="field_title"><?=$kis_lang['Admission']['otherremarks']?></td>
			<td><?=kis_ui::displayTableField($applicationInfo['remark'])?></td>
		</tr>                                                                              
	</tbody>
</table>  