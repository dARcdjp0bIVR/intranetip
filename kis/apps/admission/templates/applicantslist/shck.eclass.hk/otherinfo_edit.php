<?php

global $libkis_admission;

#### Get Student Cust Info START ####
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
#### Get Student Cust Info START ####
?>
<style>
select:disabled{
    color: #ccc;
}
</style>

<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />



<table id="dataTable1" class="form_table otherInformation" >
<colgroup>
    <col style="width:31%">
    <col style="width:1%">
    <col style="width:17%">
    <col style="width:17%">
    <col style="width:17%">
    <col style="width:17%">
</colgroup>

<tbody>
    <tr class="otherQuestionRow">
    	<td class="field_title" rowspan="5" >
    		<?=$kis_lang['Admission']['SHCK']['SibilingsStudyingInThisSchool']?>
    	</td>
    	<td colspan="3">
		    <?=$libinterface->Get_Radio_Button(
		        'SibilingsStudyingInThisSchoolY', 
		        'SibilingsStudyingInThisSchool', 
		        '1', 
		        ($applicationCustInfo['SibilingsStudyingInThisSchool'][0]['Value']=='1'), 
		        '', 
		        $kis_lang['Admission']['yes']
	        )?>&nbsp;&nbsp;&nbsp;
		    <?=$libinterface->Get_Radio_Button(
		        'SibilingsStudyingInThisSchoolN', 
		        'SibilingsStudyingInThisSchool', 
		        '0', 
		        ($applicationCustInfo['SibilingsStudyingInThisSchool'][0]['Value']=='0'), 
		        '', 
		        $kis_lang['Admission']['no']
	        )?>
    	</td>
    </tr>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['name'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['Relationship'] ?></center></td>
		<td class="form_guardian_head" colspan="2"><center><?=$kis_lang['Admission']['class']?></center></td>
	</tr>
	<?php for($i=0;$i<3;$i++){ ?>
    	<tr>
    		<td class="field_title" style="text-align:right;width:30px;">(<?=$i+1 ?>)</td>
    		<td class="form_guardian_field">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="SibilingsStudyingInThisSchool_Name<?=$i ?>" 
        			name="SibilingsStudyingInThisSchool_Name[]" 
        			value="<?=$applicationCustInfo['SibilingsStudyingInThisSchool_Name'][$i]['Value']?>"
    			/>
    		</td>
    		<td class="form_guardian_field">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="SibilingsStudyingInThisSchool_Relationship<?=$i ?>" 
        			name="SibilingsStudyingInThisSchool_Relationship[]" 
        			value="<?=$applicationCustInfo['SibilingsStudyingInThisSchool_Relationship'][$i]['Value']?>"
    			/>
    		</td>
    		<td class="form_guardian_field" colspan="2">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="SibilingsStudyingInThisSchool_Class<?=$i ?>" 
        			name="SibilingsStudyingInThisSchool_Class[]" 
        			value="<?=$applicationCustInfo['SibilingsStudyingInThisSchool_Class'][$i]['Value']?>"
    			/>
    		</td>
    	</tr>
	<?php } ?>
</tbody>

<tbody>
    <tr class="otherQuestionRow">
    	<td class="field_title" rowspan="5" >
    		<?=$kis_lang['Admission']['SHCK']['FormerStudentInThisSchool']?>
    	</td>
    	<td colspan="3">
		    <?=$libinterface->Get_Radio_Button(
		        'FormerStudentInThisSchoolY', 
		        'FormerStudentInThisSchool', 
		        '1', 
		        ($applicationCustInfo['FormerStudentInThisSchool'][0]['Value']=='1'), 
		        '', 
		        $kis_lang['Admission']['yes']
	        )?>&nbsp;&nbsp;&nbsp;
		    <?=$libinterface->Get_Radio_Button(
		        'FormerStudentInThisSchoolN', 
		        'FormerStudentInThisSchool', 
		        '0', 
		        ($applicationCustInfo['FormerStudentInThisSchool'][0]['Value']=='0'), 
		        '', 
		        $kis_lang['Admission']['no']
	        )?>
    	</td>
    </tr>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['name'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['Relationship'] ?></center></td>
		<td class="form_guardian_head" colspan="2"><center><?=$kis_lang['Admission']['SHCK']['YearOfGraduation']?></center></td>
	</tr>
	<?php for($i=0;$i<3;$i++){ ?>
    	<tr>
    		<td class="field_title" style="text-align:right;width:30px;">(<?=$i+1 ?>)</td>
    		<td class="form_guardian_field">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="FormerStudentInThisSchool_Name<?=$i ?>" 
        			name="FormerStudentInThisSchool_Name[]" 
        			value="<?=$applicationCustInfo['FormerStudentInThisSchool_Name'][$i]['Value']?>"
    			/>
    		</td>
    		<td class="form_guardian_field">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="FormerStudentInThisSchool_Relationship<?=$i ?>" 
        			name="FormerStudentInThisSchool_Relationship[]" 
        			value="<?=$applicationCustInfo['FormerStudentInThisSchool_Relationship'][$i]['Value']?>"
    			/>
    		</td>
    		<td class="form_guardian_field" colspan="2">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="FormerStudentInThisSchool_YearOfGraduation<?=$i ?>" 
        			name="FormerStudentInThisSchool_YearOfGraduation[]" 
        			value="<?=$applicationCustInfo['FormerStudentInThisSchool_YearOfGraduation'][$i]['Value']?>"
    			/>
    		</td>
    	</tr>
	<?php } ?>
</tbody>

<tbody>
    <tr class="otherQuestionRow">
    	<td class="field_title" rowspan="5" >
    		<?=$kis_lang['Admission']['SHCK']['SibilingsStudyingInOtherCanossianSchool']?>
    	</td>
    	<td colspan="3">
		    <?=$libinterface->Get_Radio_Button(
		        'SibilingsStudyingInOtherCanossianSchoolY', 
		        'SibilingsStudyingInOtherCanossianSchool', 
		        '1', 
		        ($applicationCustInfo['SibilingsStudyingInOtherCanossianSchool'][0]['Value']=='1'), 
		        '', 
		        $kis_lang['Admission']['yes']
	        )?>&nbsp;&nbsp;&nbsp;
		    <?=$libinterface->Get_Radio_Button(
		        'SibilingsStudyingInOtherCanossianSchoolN', 
		        'SibilingsStudyingInOtherCanossianSchool', 
		        '0', 
		        ($applicationCustInfo['SibilingsStudyingInOtherCanossianSchool'][0]['Value']=='0'), 
		        '', 
		        $kis_lang['Admission']['no']
	        )?>
    	</td>
    </tr>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['name'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['Relationship'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['class']?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['NameOfSchool']?></center></td>
	</tr>
	<?php for($i=0;$i<3;$i++){ ?>
    	<tr>
    		<td class="field_title" style="text-align:right;width:30px;">(<?=$i+1 ?>)</td>
    		<td class="form_guardian_field">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="SibilingsStudyingInOtherCanossianSchool_Name<?=$i ?>" 
        			name="SibilingsStudyingInOtherCanossianSchool_Name[]" 
        			value="<?=$applicationCustInfo['SibilingsStudyingInOtherCanossianSchool_Name'][$i]['Value']?>"
    			/>
    		</td>
    		<td class="form_guardian_field">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="SibilingsStudyingInOtherCanossianSchool_Relationship<?=$i ?>" 
        			name="SibilingsStudyingInOtherCanossianSchool_Relationship[]" 
        			value="<?=$applicationCustInfo['SibilingsStudyingInOtherCanossianSchool_Relationship'][$i]['Value']?>"
    			/>
    		</td>
    		<td class="form_guardian_field">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="SibilingsStudyingInOtherCanossianSchool_Class<?=$i ?>" 
        			name="SibilingsStudyingInOtherCanossianSchool_Class[]" 
        			value="<?=$applicationCustInfo['SibilingsStudyingInOtherCanossianSchool_Class'][$i]['Value']?>"
    			/>
    		</td>
    		<td class="form_guardian_field">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="SibilingsStudyingInOtherCanossianSchool_NameOfSchool<?=$i ?>" 
        			name="SibilingsStudyingInOtherCanossianSchool_NameOfSchool[]" 
        			value="<?=$applicationCustInfo['SibilingsStudyingInOtherCanossianSchool_NameOfSchool'][$i]['Value']?>"
    			/>
    		</td>
    	</tr>
	<?php } ?>
</tbody>

<tbody>
    <tr class="otherQuestionRow">
    	<td class="field_title" rowspan="5" >
    		<?=$kis_lang['Admission']['SHCK']['FormerStudentInOtherCanossianSchool']?>
    	</td>
    	<td colspan="3">
		    <?=$libinterface->Get_Radio_Button(
		        'FormerStudentInOtherCanossianSchoolY', 
		        'FormerStudentInOtherCanossianSchool', 
		        '1', 
		        ($applicationCustInfo['FormerStudentInOtherCanossianSchool'][0]['Value']=='1'), 
		        '', 
		        $kis_lang['Admission']['yes']
	        )?>&nbsp;&nbsp;&nbsp;
		    <?=$libinterface->Get_Radio_Button(
		        'FormerStudentInOtherCanossianSchoolN', 
		        'FormerStudentInOtherCanossianSchool', 
		        '0', 
		        ($applicationCustInfo['FormerStudentInOtherCanossianSchool'][0]['Value']=='0'), 
		        '', 
		        $kis_lang['Admission']['no']
	        )?>
    	</td>
    </tr>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['name'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['Relationship'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['YearOfGraduation']?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['NameOfSchool']?></center></td>
	</tr>
	<?php for($i=0;$i<3;$i++){ ?>
    	<tr>
    		<td class="field_title" style="text-align:right;width:30px;">(<?=$i+1 ?>)</td>
    		<td class="form_guardian_field">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="FormerStudentInOtherCanossianSchool_Name<?=$i ?>" 
        			name="FormerStudentInOtherCanossianSchool_Name[]" 
        			value="<?=$applicationCustInfo['FormerStudentInOtherCanossianSchool_Name'][$i]['Value']?>"
    			/>
    		</td>
    		<td class="form_guardian_field">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="FormerStudentInOtherCanossianSchool_Relationship<?=$i ?>" 
        			name="FormerStudentInOtherCanossianSchool_Relationship[]" 
        			value="<?=$applicationCustInfo['FormerStudentInOtherCanossianSchool_Relationship'][$i]['Value']?>"
    			/>
    		</td>
    		<td class="form_guardian_field">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="FormerStudentInOtherCanossianSchool_YearOfGraduation<?=$i ?>" 
        			name="FormerStudentInOtherCanossianSchool_YearOfGraduation[]" 
        			value="<?=$applicationCustInfo['FormerStudentInOtherCanossianSchool_YearOfGraduation'][$i]['Value']?>"
    			/>
    		</td>
    		<td class="form_guardian_field">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="FormerStudentInOtherCanossianSchool_NameOfSchool<?=$i ?>" 
        			name="FormerStudentInOtherCanossianSchool_NameOfSchool[]" 
        			value="<?=$applicationCustInfo['FormerStudentInOtherCanossianSchool_NameOfSchool'][$i]['Value']?>"
    			/>
    		</td>
    	</tr>
	<?php } ?>
</tbody>

<tbody>
    <tr class="otherQuestionRow">
    	<td class="field_title" rowspan="5" >
    		<?=$kis_lang['Admission']['SHCK']['EmployeeOfCanossianInstitutions']?>
    	</td>
    	<td colspan="3">
		    <?=$libinterface->Get_Radio_Button(
		        'EmployeeOfCanossianInstitutionsY', 
		        'EmployeeOfCanossianInstitutions', 
		        '1', 
		        ($applicationCustInfo['EmployeeOfCanossianInstitutions'][0]['Value']=='1'), 
		        '', 
		        $kis_lang['Admission']['yes']
	        )?>&nbsp;&nbsp;&nbsp;
		    <?=$libinterface->Get_Radio_Button(
		        'EmployeeOfCanossianInstitutionsN', 
		        'EmployeeOfCanossianInstitutions', 
		        '0', 
		        ($applicationCustInfo['EmployeeOfCanossianInstitutions'][0]['Value']=='0'), 
		        '', 
		        $kis_lang['Admission']['no']
	        )?>
    	</td>
    </tr>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['name'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['Relationship'] ?></center></td>
		<td class="form_guardian_head" colspan="2"><center><?=$kis_lang['Admission']['SHCK']['NameOfOurInstitution']?></center></td>
	</tr>
	<?php for($i=0;$i<3;$i++){ ?>
    	<tr>
    		<td class="field_title" style="text-align:right;width:30px;">(<?=$i+1 ?>)</td>
    		<td class="form_guardian_field">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="EmployeeOfCanossianInstitutions_Name<?=$i ?>" 
        			name="EmployeeOfCanossianInstitutions_Name[]" 
        			value="<?=$applicationCustInfo['EmployeeOfCanossianInstitutions_Name'][$i]['Value']?>"
    			/>
    		</td>
    		<td class="form_guardian_field">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="EmployeeOfCanossianInstitutions_Relationship<?=$i ?>" 
        			name="EmployeeOfCanossianInstitutions_Relationship[]" 
        			value="<?=$applicationCustInfo['EmployeeOfCanossianInstitutions_Relationship'][$i]['Value']?>"
    			/>
    		</td>
    		<td class="form_guardian_field" colspan="2">
    			<input 
        			type="text" 
        			class="textboxtext" 
        			id="EmployeeOfCanossianInstitutions_NameOfOurInstitution<?=$i ?>" 
        			name="EmployeeOfCanossianInstitutions_NameOfOurInstitution[]" 
        			value="<?=$applicationCustInfo['EmployeeOfCanossianInstitutions_NameOfOurInstitution'][$i]['Value']?>"
    			/>
    		</td>
    	</tr>
	<?php } ?>
</tbody>

</table>

<script>
////UI Releated START ////
$(function(){
	$('.otherInformation .otherQuestionRow input[type="radio"]').click(function(){
		var $tbody = $(this).closest('tbody');
		if($(this).val() == '0'){
			$tbody.find('tr:not(.otherQuestionRow)').hide().find('input').prop('disabled', true);
		}else{
			$tbody.find('tr:not(.otherQuestionRow)').show().find('input').prop('disabled', false);
		}
	});
	$('.otherInformation .otherQuestionRow input[type="radio"]:checked').click();
});
//// UI Releated END////

$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function checkValidForm(){

	/******** Other Info START ********/
	/**** Sibilings Studying In This School START ****/
	if($('#SibilingsStudyingInThisSchoolY:checked').length == 1){
		var name = '';
		for(var i=0;i<3;i++){
			name += $('#SibilingsStudyingInThisSchool_Name' + i).val().trim();
		}
		if(name == ''){
			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterName']?>");
			$('#SibilingsStudyingInThisSchool_Name0').focus();
			return false;
		}

		for(var i=0;i<3;i++){
			if(
				$('#SibilingsStudyingInThisSchool_Name' + i).val().trim() != '' &&
				$('#SibilingsStudyingInThisSchool_Relationship' + i).val().trim() == ''
			){
    			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterRelationship']?>");
    			$('#SibilingsStudyingInThisSchool_Relationship' + i).focus();
    			return false;
			}

			if(
				$('#SibilingsStudyingInThisSchool_Name' + i).val().trim() != '' &&
				$('#SibilingsStudyingInThisSchool_Class' + i).val().trim() == ''
			){
    			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterClass']?>");
    			$('#SibilingsStudyingInThisSchool_Class' + i).focus();
    			return false;
			}
		}
	}
	/**** Sibilings Studying In This School END ****/
	
	/**** Former Student In This School START ****/
	if($('#FormerStudentInThisSchoolY:checked').length == 1){
		var name = '';
		for(var i=0;i<3;i++){
			name += $('#FormerStudentInThisSchool_Name' + i).val().trim();
		}
		if(name == ''){
			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterName']?>");
			$('#FormerStudentInThisSchool_Name0').focus();
			return false;
		}

		for(var i=0;i<3;i++){
			if(
				$('#FormerStudentInThisSchool_Name' + i).val().trim() != '' &&
				$('#FormerStudentInThisSchool_Relationship' + i).val().trim() == ''
			){
    			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterRelationship']?>");
    			$('#FormerStudentInThisSchool_Relationship' + i).focus();
    			return false;
			}

			if(
				$('#FormerStudentInThisSchool_Name' + i).val().trim() != '' &&
				$('#FormerStudentInThisSchool_YearOfGraduation' + i).val().trim() == ''
			){
    			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterYearOfGraduation']?>");
    			$('#FormerStudentInThisSchool_YearOfGraduation' + i).focus();
    			return false;
			}
		}
	}
	/**** Former Student In This School END ****/
	
	/**** Sibilings Studying In Other Canossian School START ****/
	if($('#SibilingsStudyingInOtherCanossianSchoolY:checked').length == 1){
		var name = '';
		for(var i=0;i<3;i++){
			name += $('#SibilingsStudyingInOtherCanossianSchool_Name' + i).val().trim();
		}
		if(name == ''){
			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterName']?>");
			$('#SibilingsStudyingInOtherCanossianSchool_Name0').focus();
			return false;
		}

		for(var i=0;i<3;i++){
			if(
				$('#SibilingsStudyingInOtherCanossianSchool_Name' + i).val().trim() != '' &&
				$('#SibilingsStudyingInOtherCanossianSchool_Relationship' + i).val().trim() == ''
			){
    			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterRelationship']?>");
    			$('#SibilingsStudyingInOtherCanossianSchool_Relationship' + i).focus();
    			return false;
			}

			if(
				$('#SibilingsStudyingInOtherCanossianSchool_Name' + i).val().trim() != '' &&
				$('#SibilingsStudyingInOtherCanossianSchool_Class' + i).val().trim() == ''
			){
    			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterClass']?>");
    			$('#SibilingsStudyingInOtherCanossianSchool_Class' + i).focus();
    			return false;
			}

			if(
				$('#SibilingsStudyingInOtherCanossianSchool_Name' + i).val().trim() != '' &&
				$('#SibilingsStudyingInOtherCanossianSchool_NameOfSchool' + i).val().trim() == ''
			){
    			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterNameOfSchool']?>");
    			$('#SibilingsStudyingInOtherCanossianSchool_NameOfSchool' + i).focus();
    			return false;
			}
		}
	}
	/**** Sibilings Studying In Other Canossian School END ****/
	
	/**** Former Student In Other Canossian School START ****/
	if($('#FormerStudentInOtherCanossianSchoolY:checked').length == 1){
		var name = '';
		for(var i=0;i<3;i++){
			name += $('#FormerStudentInOtherCanossianSchool_Name' + i).val().trim();
		}
		if(name == ''){
			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterName']?>");
			$('#FormerStudentInOtherCanossianSchool_Name0').focus();
			return false;
		}

		for(var i=0;i<3;i++){
			if(
				$('#FormerStudentInOtherCanossianSchool_Name' + i).val().trim() != '' &&
				$('#FormerStudentInOtherCanossianSchool_Relationship' + i).val().trim() == ''
			){
    			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterRelationship']?>");
    			$('#FormerStudentInOtherCanossianSchool_Relationship' + i).focus();
    			return false;
			}

			if(
				$('#FormerStudentInOtherCanossianSchool_Name' + i).val().trim() != '' &&
				$('#FormerStudentInOtherCanossianSchool_YearOfGraduation' + i).val().trim() == ''
			){
    			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterYearOfGraduation']?>");
    			$('#FormerStudentInOtherCanossianSchool_YearOfGraduation' + i).focus();
    			return false;
			}

			if(
				$('#FormerStudentInOtherCanossianSchool_Name' + i).val().trim() != '' &&
				$('#FormerStudentInOtherCanossianSchool_NameOfSchool' + i).val().trim() == ''
			){
    			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterNameOfSchool']?>");
    			$('#FormerStudentInOtherCanossianSchool_NameOfSchool' + i).focus();
    			return false;
			}
		}
	}
	/**** Former Student In Other Canossian School END ****/
	
	/**** Employee Of Canossian Institutions START ****/
	if($('#EmployeeOfCanossianInstitutionsY:checked').length == 1){
		var name = '';
		for(var i=0;i<3;i++){
			name += $('#EmployeeOfCanossianInstitutions_Name' + i).val().trim();
		}
		if(name == ''){
			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterName']?>");
			$('#EmployeeOfCanossianInstitutions_Name0').focus();
			return false;
		}

		for(var i=0;i<3;i++){
			if(
				$('#EmployeeOfCanossianInstitutions_Name' + i).val().trim() != '' &&
				$('#EmployeeOfCanossianInstitutions_Relationship' + i).val().trim() == ''
			){
    			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterRelationship']?>");
    			$('#EmployeeOfCanossianInstitutions_Relationship' + i).focus();
    			return false;
			}

			if(
				$('#EmployeeOfCanossianInstitutions_Name' + i).val().trim() != '' &&
				$('#EmployeeOfCanossianInstitutions_NameOfOurInstitution' + i).val().trim() == ''
			){
    			alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterNameOfOurInstitution']?>");
    			$('#EmployeeOfCanossianInstitutions_NameOfOurInstitution' + i).focus();
    			return false;
			}
		}
	}
	/**** Employee Of Canossian Institutions END ****/
	/******** Other Info END ********/
	return true;
}
</script>