<?php

global $libkis_admission;

######## Get Student Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
$applicationInfo['studentApplicationInfoCust'] = $libkis_admission->getApplicationStudentInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
######## Get Student Cust Info END ########

?>
<style>
select:disabled{
    color: #ccc;
}
</style>

<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />
<table class="form_table">
	<tbody>
		<tr> 
			<td width="30%" class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['HKUGAPS']['chinesename'] ?>
			</td>
			<td width="40%">
				<?=$libinterface->GET_TEXTBOX('studentsname_b5', 'studentsname_b5', $applicationInfo['student_name_b5'], $OtherClass='', $OtherPar=array())?>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin:0px;">
					<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
					<div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
						<a id = "btn_remove" href="#" class="btn_remove"></a>
					</div>
					<div class="text_remark" style="text-align:center;">
						<?=$kis_lang['Admission']['msg']['clicktouploadphoto']?>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['HKUGAPS']['englishname'] ?>
			</td>
			<td>
			<?=$libinterface->GET_TEXTBOX('studentsname_en', 'studentsname_en', $applicationInfo['student_name_en'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['dateofbirth'] ?>
			</td>
			<td>
			<input type="text" name="StudentDateOfBirth" id="StudentDateOfBirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;
			<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['gender'] ?>
			</td>
		<td>
			<?=$libinterface->Get_Radio_Button('StudentGender1', 'StudentGender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
			<?=$libinterface->Get_Radio_Button('StudentGender2', 'StudentGender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>                              
		</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['SHCK']['birthCertNo'] ?>
			</td>
			<td nowrap="" colspan="2">
    			<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" value="<?=$applicationInfo['birthcertno'] ?>"/>
    			<br/>
    			<?=$kis_lang['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['SHCK']['ethnicity'] ?>
			</td>
			<td nowrap="" colspan="2">
        		<?php
        		    $ethnicitySelectOption = array();
    				foreach($admission_cfg['Ethnicity'] as $index=>$ethnicity){ 
    				    $ethnicitySelectOption[] = array($index, $ethnicity);
    				}
            		echo $libinterface->GET_SELECTION_BOX(
					    $ethnicitySelectOption,
					    'name="Nationality" id="Nationality"',
					    '',
					    $selected = $applicationInfo['Nationality']); 
        		?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['placeofbirth'] ?>
			</td>
			<td>
        		<?php
        		    $placeSelectOption = array();
    				foreach($admission_cfg['PlaceOfBirth'] as $index=>$place){ 
    				    $placeSelectOption[] = array($index, $place);
    				}

            		echo $libinterface->GET_SELECTION_BOX(
					    $placeSelectOption,
					    'name="StudentPlaceOfBirth" id="StudentPlaceOfBirth"',
					    '',
					    $selected = $applicationInfo['PlaceOfBirth']); 
        		?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['SHCK']['langspokenathome'] ?>
			</td>
			<td colspan="2">
        		<?php
        		    $langSelectOption = array();
    				foreach($admission_cfg['Language'] as $index=>$lang){
                		$langSelectOption[] = array($index, $lang);
    				}  

            		echo $libinterface->GET_SELECTION_BOX(
					    $langSelectOption,
					    'name="LangSpokenAtHome" id="LangSpokenAtHome"',
					    '',
					    $selected = $applicationInfo['LangSpokenAtHome']); 
				?>
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['address'] ?>
			</td>
			<td>
				<textarea name="Address" type="text" id="Address" class="textboxtext" style="height: 100px; resize: vertical;"><?=$applicationInfo['Address'] ?></textarea>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['SHCK']['district'] ?>
			</td>
			<td colspan="2">
        		<?php
        		    $districtSelectOption = array();
    				foreach($admission_cfg['District'] as $index=>$district){
                		$districtSelectOption[] = array($index, $district);
    				}  

            		echo $libinterface->GET_SELECTION_BOX(
					    $districtSelectOption,
					    'name="District" id="District"',
					    '',
					    $selected = $applicationInfo['AddressDistrict']); 
				?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['SHCK']['ContactNo'] ?>
			</td>
			<td nowrap="">
    			<input name="HomeTelNo" type="text" id="HomeTelNo" class="textboxtext" style="width:150px" value="<?=$applicationInfo['HomeTelNo'] ?>"/>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['email'] ?>
			</td>
			<td nowrap="">
    			<input name="Email" type="text" id="Email" class="textboxtext" value="<?=$applicationInfo['Email'] ?>"/>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['SHCK']['FormerSchool'] ?>
			</td>
			<td nowrap="">
    			<input name="LastSchool" type="text" id="LastSchool" class="textboxtext" value="<?=$applicationInfo['LastSchool'] ?>"/>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['SHCK']['ClassLastAttended'] ?>
			</td>
			<td nowrap="">
    			<input name="LastSchoolLevel" type="text" id="LastSchoolLevel" class="textboxtext" value="<?=$applicationInfo['LastSchoolLevel'] ?>"/>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['religion'] ?>
			</td>
			<td nowrap="">
    			<input name="ReligionOther" type="text" id="ReligionOther" class="textboxtext" value="<?=$applicationInfo['ReligionOther'] ?>"/>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['SHCK']['BaptismCertNo'] ?>
			</td>
			<td nowrap="">
    			<input name="BaptismCertNo" type="text" id="BaptismCertNo" class="textboxtext" value="<?=$applicationCustInfo['BaptismCertNo'][0]['Value'] ?>"/>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['SHCK']['Church'] ?>
			</td>
			<td nowrap="">
    			<input name="Church" type="text" id="Church" class="textboxtext" value="<?=$applicationInfo['Church'] ?>"/>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['SHCK']['Siblings'] ?>
			</td>
			<td nowrap="">

            	<div style="margin-bottom: 5px;">
            		<label for="ElderBrother" style="width: 130px;display: inline-block;">
            			<?=$kis_lang['Admission']['SHCK']['ElderBrother'] ?>
            		</label>
        			<select id="ElderBrother" name="ElderBrother">
        				<?php
        				    for($i=0;$i<=10;$i++){
        				        $selected = ($applicationCustInfo['ElderBrother'][0]['Value'] == $i)? 'selected' : '';
                                $text = ($i == 0)? 'N/A' : $i;
        				?>
        					<option value="<?=$i ?>" <?=$selected ?>><?=$text ?></option>
        				<?php
        				    }
        				?>
        			</select>
        		</div>

            	<div style="margin-bottom: 5px;">
            		<label for="ElderSister" style="width: 130px;display: inline-block;">
            			<?=$kis_lang['Admission']['SHCK']['ElderSister'] ?>
            		</label>
        			<select id="ElderSister" name="ElderSister">
        				<?php
        				    for($i=0;$i<=10;$i++){
        				        $selected = ($applicationCustInfo['ElderSister'][0]['Value'] == $i)? 'selected' : '';
                                $text = ($i == 0)? 'N/A' : $i;
        				?>
        					<option value="<?=$i ?>" <?=$selected ?>><?=$text ?></option>
        				<?php
        				    }
        				?>
        			</select>
        		</div>
        		
            	<div style="margin-bottom: 5px;">
            		<label for="YoungerBrother" style="width: 130px;display: inline-block;">
            			<?=$kis_lang['Admission']['SHCK']['YoungerBrother'] ?>
            		</label>
        			<select id="YoungerBrother" name="YoungerBrother">
        				<?php
        				    for($i=0;$i<=10;$i++){
        				        $selected = ($applicationCustInfo['YoungerBrother'][0]['Value'] == $i)? 'selected' : '';
                                $text = ($i == 0)? 'N/A' : $i;
        				?>
        					<option value="<?=$i ?>" <?=$selected ?>><?=$text ?></option>
        				<?php
        				    }
        				?>
        			</select>
        		</div>

            	<div style="margin-bottom: 5px;">
            		<label for="YoungerSister" style="width: 130px;display: inline-block;">
            			<?=$kis_lang['Admission']['SHCK']['YoungerSister'] ?>
            		</label>
        			<select id="YoungerSister" name="YoungerSister">
        				<?php
        				    for($i=0;$i<=10;$i++){
        				        $selected = ($applicationCustInfo['YoungerSister'][0]['Value'] == $i)? 'selected' : '';
                                $text = ($i == 0)? 'N/A' : $i;
        				?>
        					<option value="<?=$i ?>" <?=$selected ?>><?=$text ?></option>
        				<?php
        				    }
        				?>
        			</select>
        		</div>
        		
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['SHCK']['LangForInterview'] ?>
			</td>
			<td nowrap="">
        		<?php
        		    $langSelectOption = array();
    				foreach($admission_cfg['LanguageForInterview'] as $index=>$lang){
                		echo $libinterface->Get_Radio_Button(
                		    "LangForInterview{$index}", 
                		    'LangForInterview', 
                		    $index, 
                		    ($applicationCustInfo['LangForInterview'][0]['Value']==$index), 
                		    '', 
                		    $lang
            		    );
    				}  
				?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['SHCK']['SessionChoice'] ?>
			</td>
			<td nowrap="">
        		<?php
        		    $langSelectOption = array();
    				foreach($admission_cfg['SessionChoice'] as $index=>$lang){
                		echo $libinterface->Get_Radio_Button(
                		    "SessionChoice{$index}", 
                		    'SessionChoice', 
                		    $index, 
                		    ($applicationCustInfo['SessionChoice'][0]['Value']==$index), 
                		    '', 
                		    $lang
            		    );
    				}
				?>
			</td>
		</tr>
		
		<tr>
	       <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['KTLMSKG']['twins']?></td>
	       <td><?=$libinterface->Get_Radio_Button('IsTwinsApplied1', 'IsTwinsApplied', 'Y', $applicationInfo['IsTwinsApplied']=='Y','',$kis_lang['Admission']['yes']).' '.$libinterface->Get_Radio_Button('IsTwinsApplied2', 'IsTwinsApplied', 'N', $applicationInfo['IsTwinsApplied']=='N','',$kis_lang['Admission']['no']).'<br/>'.$kis_lang['Admission']['KTLMSKG']['twinsID'].': <input type="text" name="TwinsBirthCertNo" id="TwinsBirthCertNo" value="'.$applicationInfo['TwinsApplicationID'].'">'
			?></td>
	    </tr>
                             
	<?php
	for($i=0;$i<sizeof($attachmentSettings);$i++) {
		$attachment_name = $attachmentSettings[$i]['AttachmentName'];
		
		$_filePath = $attachmentList[$attachment_name]['link'];
		if($_filePath){
			$_spanDisplay = '';
			$_buttonDisplay = ' style="display:none;"';
		}else{
			$_filePath = '';
			$_spanDisplay = ' style="display:none;"';
			$_buttonDisplay = '';
		}
		
		$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
		$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';
	?>
		<tr>
			<td class="field_title">
				<?=$attachment_name?>
			</td>
			<td id="<?=$attachment_name?>">
				<span class="view_attachment" <?=$_spanDisplay?>><?=$_attachment?></span>
				<input type="button" class="attachment_upload_btn formsmallbutton" value="<?=$kis_lang['Upload']?>"  id="uploader-<?=$attachment_name?>" <?=$_buttonDisplay?>/>
			</td>
		</tr>	
	<?php
	}
	?>                                                                           
	</tbody>
</table>

<script>
//// UI Releated START ////
////UI Releated END ////

$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	
	return false;
});

function check_hkid(hkid) {
	// hkid = $.trim(hkid);
	// hkid = hkid.replace(/\s/g, '');
	// hkid = hkid.toUpperCase();
	// $(":input[name='id_no']").val(hkid);

	var re = /^([A-Z]{1,2})((\d){6})\({0,1}([A0-9]{1})\){0,1}$/g;
	var ra = re.exec(hkid);

	if (ra != null) {
		var p1 = ra[1];
		var p2 = ra[2];
		var p3 = ra[4];
		var check_sum = 0;
		if (p1.length == 2) {
			check_sum = (p1.charCodeAt(0)-55) * 9 + (p1.charCodeAt(1)-55) * 8;
		}
		else if (p1.length == 1){
			check_sum = 324 + (p1.charCodeAt(0)-55) * 8;
		}

		check_sum += parseInt(p2.charAt(0)) * 7 + parseInt(p2.charAt(1)) * 6 + parseInt(p2.charAt(2)) * 5 + parseInt(p2.charAt(3)) * 4 + parseInt(p2.charAt(4)) * 3 + parseInt(p2.charAt(5)) * 2;
		var check_digit = 11 - (check_sum % 11);
		if (check_digit == '11') {
			check_digit = 0;
		}
		else if (check_digit == '10') {
			check_digit = 'A';
		}
		if (check_digit == p3 ) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}
	
function checkValidForm(){
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var dOBRange = new Array();
	   $.ajax({
	       url: "admission_form/ajax_get_bday_range.php",
	       type: "post",
	       data: $("#applicant_form").serialize(),
	       async: false,
	       success: function(data){
	           //alert("debugging: The classlevel is updated!");
	           dOBRange = data.split(",");
	       },
	       error:function(){
	           //alert("failure");
	           $("#result").html('There is error while submit');
	       }
	   });

	var form1 = applicant_form;
    /******** Student Info START ********/
    /**** Name START ****/
    if($.trim(form1.studentsname_b5.value)==''){
    	alert("<?=$kis_lang['Admission']['msg']['enterchinesename']?>");	
    	form1.studentsname_b5.focus();
    	return false;
    }
    if(
    	!checkNaNull(form1.studentsname_b5.value) && 
    	!checkIsChineseCharacter(form1.studentsname_b5.value)
    ){
    	alert("<?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
    	form1.studentsname_b5.focus();
    	return false;
    }
    
    if($.trim(form1.studentsname_en.value)==''){
    	alert("<?=$kis_lang['Admission']['msg']['enterenglishname']?>");	
    	form1.studentsname_en.focus();
    	return false;
    }
    if(
    	!checkNaNull(form1.studentsname_en.value) &&
    	!checkIsEnglishCharacter(form1.studentsname_en.value)
    ){
    	alert("<?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
    	form1.studentsname_en.focus();
    	return false;
    }
    /**** Name END ****/
    
    /**** DOB START ****/
    if(!form1.StudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
    	if(form1.StudentDateOfBirth.value!=''){
    		alert("<?=$kis_lang['Admission']['msg']['invaliddateformat']?>");
    	}
    	else{
    		alert("<?=$kis_lang['Admission']['msg']['enterdateofbirth']?>");	
    	}
    	
    	form1.StudentDateOfBirth.focus();
    	return false;
    } 
    if(dOBRange[0] !='' && form1.StudentDateOfBirth.value < dOBRange[0] || dOBRange[1] !='' && form1.StudentDateOfBirth.value > dOBRange[1]){
    	alert("<?=$kis_lang['Admission']['msg']['invalidbdaydateformat']?>");
    	form1.StudentDateOfBirth.focus();
    	return false;
    } 
    /**** DOB END ****/
    
    /**** Gender START ****/
    if($.trim($('input:radio[name=StudentGender]:checked').val())==''){
    	alert("<?=$kis_lang['Admission']['msg']['selectgender']?>");	
    	form1.StudentGender[0].focus();
    	return false;
    }
    /**** Gender END ****/
    
    /**** Personal Identification START ****/
    if(form1.StudentBirthCertNo.value==''){
    	alert("<?=$kis_lang['Admission']['SHCK']['msg']['invalidBirthCertNo']?>");
    	form1.StudentBirthCertNo.focus();
    	return false;
    }
    /**** Personal Identification END ****/
    
    /**** Address START ****/
    if($.trim(form1.Address.value)==''){
    	alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterAddress']?>");	
    	form1.Address.focus();
    	return false;
    } 
    /**** Address END ****/
    
    /**** Tel START ****/
    if(form1.HomeTelNo.value==''){
    	alert("<?=$kis_lang['Admission']['SHCK']['msg']['enterContactNo']?>");	
    	form1.HomeTelNo.focus();
    	return false;
    }
    if(!/^[0-9]*$/.test(form1.HomeTelNo.value)){
    	alert("<?=$kis_lang['Admission']['SHCK']['msg']['invalidContactNoFormat']?>");
    	form1.HomeTelNo.focus();
    	return false;
    }
    /**** Tel END ****/
    
    /**** Email START ****/
    if($.trim(form1.Email.value)==''){
    	alert("<?=$kis_lang['Admission']['icms']['msg']['entermailaddress']?>");
    	form1.Email.focus();
    	return false;
    }
    if($.trim(form1.Email.value)!='' && !re.test(form1.Email.value)){
    	alert("<?=$kis_lang['Admission']['icms']['msg']['invalidmailaddress']?>");
    	form1.Email.focus();
    	return false;
    }
    /**** Email END ****/
    
    
    /**** Language For Interview START ****/
    if($.trim($('input:radio[name=LangForInterview]:checked').val())==''){
    	alert("<?=$kis_lang['Admission']['SHCK']['msg']['selectLanguageForInterview']?>");	
    	form1.LangForInterview[0].focus();
    	return false;
    }
    /**** Language For Interview END ****/
    
    
    /**** Session Choice START ****/
    if($.trim($('input:radio[name=SessionChoice]:checked').val())==''){
    	alert("<?=$kis_lang['Admission']['SHCK']['msg']['selectSessionChoice']?>");	
    	form1.LangForInterview[0].focus();
    	return false;
    }
    /**** Session Choice END ****/
    /******** Student Info END ********/
    
    return true;
}
</script>