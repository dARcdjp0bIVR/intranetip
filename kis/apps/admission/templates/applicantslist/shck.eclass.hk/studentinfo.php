<?php

global $libkis_admission;

######## Get Student Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
$applicationInfo['studentApplicationInfoCust'] = $libkis_admission->getApplicationStudentInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
######## Get Student Cust Info END ########

?>
<table class="form_table">
	<tbody>
		<tr> 
			<td width="30%" class="field_title">
				<?= $kis_lang['Admission']['chinesename'] ?>
			</td>
			<td width="40%">
				<?=$applicationInfo['ChineseName']?>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin:0px;">
					<img src="<?= $attachmentList['personal_photo']['link'] ? $attachmentList['personal_photo']['link'] : $blankphoto ?>?_=<?= time() ?>"/>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['englishname'] ?>
			</td>
			<td>
				<?=$applicationInfo['EnglishName']?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['dateofbirth'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationInfo['dateofbirth']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['gender'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['Gender']]) ?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['SHCK']['birthCertNo'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationInfo['BirthCertNo']) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['SHCK']['ethnicity'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($admission_cfg['Ethnicity'][ $applicationInfo['Nationality'] ]);
				?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['placeofbirth'] ?>
			</td>
			<td>
				<?php
			        echo kis_ui::displayTableField($admission_cfg['PlaceOfBirth'][ $applicationInfo['PlaceOfBirth'] ]);
				?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['SHCK']['langspokenathome'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($admission_cfg['Language'][ $applicationInfo['LangSpokenAtHome'] ]) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['address'] ?>
			</td>
			<td colspan="2">
    			<?=kis_ui::displayTableField(nl2br($applicationInfo['Address']))?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['SHCK']['district'] ?>
			</td>
			<td colspan="2">
    			<?=kis_ui::displayTableField($admission_cfg['District'][ $applicationInfo['AddressDistrict'] ])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['SHCK']['ContactNo'] ?>
			</td>
			<td colspan="2">
    			<?=kis_ui::displayTableField($applicationInfo['HomeTelNo'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['email'] ?>
			</td>
			<td colspan="2">
    			<?=kis_ui::displayTableField($applicationInfo['Email'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['SHCK']['FormerSchool'] ?>
			</td>
			<td colspan="2">
    			<?=kis_ui::displayTableField($applicationInfo['LastSchool'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['SHCK']['ClassLastAttended'] ?>
			</td>
			<td colspan="2">
    			<?=kis_ui::displayTableField($applicationInfo['LastSchoolLevel'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['religion'] ?>
			</td>
			<td colspan="2">
    			<?=kis_ui::displayTableField($applicationInfo['ReligionOther'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['SHCK']['BaptismCertNo'] ?>
			</td>
			<td colspan="2">
    			<?=kis_ui::displayTableField($applicationCustInfo['BaptismCertNo'][0]['Value'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['SHCK']['Church'] ?>
			</td>
			<td colspan="2">
    			<?=kis_ui::displayTableField($applicationInfo['Church'])?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['SHCK']['Siblings'] ?>
			</td>
			<td colspan="2">
    			<div style="margin-bottom: 5px;">
        			<?=$kis_lang['Admission']['SHCK']['ElderBrother'] ?>:
        			<?=kis_ui::displayTableField(
        			    ($applicationCustInfo['ElderBrother'][0]['Value'])?$applicationCustInfo['ElderBrother'][0]['Value']:'N/A'
    			    )?>
        		</div>
        	
            	<div style="margin-bottom: 5px;">
        			<?=$kis_lang['Admission']['SHCK']['ElderSister'] ?>:
        			<?=kis_ui::displayTableField(
        			    ($applicationCustInfo['ElderSister'][0]['Value'])?$applicationCustInfo['ElderSister'][0]['Value']:'N/A'
    			    )?>
        		</div>
        	
            	<div style="margin-bottom: 5px;">
        			<?=$kis_lang['Admission']['SHCK']['YoungerBrother'] ?>:
        			<?=kis_ui::displayTableField(
        			    ($applicationCustInfo['YoungerBrother'][0]['Value'])?$applicationCustInfo['YoungerBrother'][0]['Value']:'N/A'
    			    )?>
        		</div>
        	
            	<div style="">
        			<?=$kis_lang['Admission']['SHCK']['YoungerSister'] ?>:
        			<?=kis_ui::displayTableField(
        			    ($applicationCustInfo['YoungerSister'][0]['Value'])?$applicationCustInfo['YoungerSister'][0]['Value']:'N/A'
    			    )?>
        		</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['SHCK']['LangForInterview'] ?>
			</td>
			<td colspan="2">
    			<?=kis_ui::displayTableField($admission_cfg['LanguageForInterview'][ $applicationCustInfo['LangForInterview'][0]['Value'] ])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['SHCK']['SessionChoice'] ?>
			</td>
			<td colspan="2">
    			<?=kis_ui::displayTableField($admission_cfg['SessionChoice'][ $applicationCustInfo['SessionChoice'][0]['Value'] ])?>
			</td>
		</tr>
		<tr>
	       <td class="field_title"><?=$kis_lang['Admission']['KTLMSKG']['twins']?></td>
	       <td colspan="2"><?=kis_ui::displayTableField(($applicationInfo['IsTwinsApplied'] =='Y'?$kis_lang['Admission']['yes']:$kis_lang['Admission']['no']).'<br/>'.$kis_lang['Admission']['KTLMSKG']['twinsID'].': '.($applicationInfo['TwinsApplicationID']?$applicationInfo['TwinsApplicationID']:'--'))?></td>
	    </tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['document'] ?>
			</td>
			<td colspan="2">
				<?php
					$attachmentAry = array();
					foreach ($attachmentList as $_type => $_attachmentAry) {
						if ($_type != 'personal_photo') {
							if ($_attachmentAry['link']) {
								//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
								$attachmentAry[] = '<a href="' . $_attachmentAry['link'] . '" target="_blank">' . $_type . '</a>';
							}
						}
					}
				?>
				<?= count($attachmentAry) == 0 ? '--' : implode('<br/>', $attachmentAry); ?>
			</td>
		</tr>                                                                                 
	</tbody>
</table>