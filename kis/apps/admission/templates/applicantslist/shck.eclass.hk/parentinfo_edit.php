<?php

global $libkis_admission;

######## Get Parent Cust Info START ########
//$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
######## Get Parent Cust Info END ########
?>
<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />
<table class="form_table">
	<colgroup>
        <col style="width:30%">
        <col style="width:35%">
        <col style="width:35%">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['father'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['mother'] ?></center></td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename'] ?>
		</td>
		<td class="form_guardian_field">
			<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" value="<?=$applicationInfo['F']['ChineseName'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" value="<?=$applicationInfo['M']['ChineseName'] ?>"/>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname'] ?>
		</td>
		<td class="form_guardian_field">
			<input name="G1EnglishName" type="text" id="G1EnglishName" class="textboxtext" value="<?=$applicationInfo['F']['EnglishName'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2EnglishName" type="text" id="G2EnglishName" class="textboxtext" value="<?=$applicationInfo['M']['EnglishName'] ?>"/>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['religion']?>
		</td>
		<td class="form_guardian_field">
			<input name="G1Religion" type="text" id="G1Religion" class="textboxtext" value="<?=$applicationInfo['F']['Company'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2Religion" type="text" id="G2Religion" class="textboxtext" value="<?=$applicationInfo['M']['Company'] ?>"/>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['occupation']?>
		</td>
		<td class="form_guardian_field">
			<input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext" value="<?=$applicationInfo['F']['JobTitle'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext" value="<?=$applicationInfo['M']['JobTitle'] ?>"/>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['SHCK']['OfficeAddress']?>
		</td>
		<td class="form_guardian_field">
			<textarea name="G1Address" type="text" id="G1Address" class="textboxtext" style="height: 100px; resize: vertical;"><?=$applicationInfo['F']['OfficeAddress'] ?></textarea>
		</td>
		<td class="form_guardian_field">
			<textarea name="G2Address" type="text" id="G2Address" class="textboxtext" style="height: 100px; resize: vertical;"><?=$applicationInfo['M']['OfficeAddress'] ?></textarea>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['SHCK']['OfficeTel']?>
		</td>
		<td class="form_guardian_field">
			<input name="G1OfficeTel" type="text" id="G1OfficeTel" class="textboxtext" value="<?=$applicationInfo['F']['OfficeTelNo'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2OfficeTel" type="text" id="G2OfficeTel" class="textboxtext" value="<?=$applicationInfo['M']['OfficeTelNo'] ?>"/>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$mustfillinsymbol?><?=$kis_lang['Admission']['SHCK']['MobileTel']?>
		</td>
		<td class="form_guardian_field">
			<input name="G1MobileNo" type="text" id="G1MobileNo" class="textboxtext" value="<?=$applicationInfo['F']['Mobile'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2MobileNo" type="text" id="G2MobileNo" class="textboxtext" value="<?=$applicationInfo['M']['Mobile'] ?>"/>
		</td>
	</tr>

</tbody>
</table>
<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function checkValidForm(){
	var form1 = applicant_form;
	/******** Parent Info START ********/
	/**** Name START ****/
	if($.trim(form1.G1ChineseName.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>");
		form1.G1ChineseName.focus();
		return false;
	}
	if(
		!checkNaNull(form1.G1ChineseName.value) &&
		!checkIsChineseCharacter(form1.G1ChineseName.value)
	){
		alert("<?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
		form1.G1ChineseName.focus();
		return false;
	}
	
	if($.trim(form1.G2ChineseName.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>");
		form1.G2ChineseName.focus();
		return false;
	}
	if(
		!checkNaNull(form1.G2ChineseName.value) &&
		!checkIsChineseCharacter(form1.G2ChineseName.value)
	){
		alert("<?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
		form1.G2ChineseName.focus();
		return false;
	}

	if($.trim(form1.G1EnglishName.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>");
		form1.G1EnglishName.focus();
		return false;
	}
	if(
		!checkNaNull(form1.G1EnglishName.value) &&
		!checkIsEnglishCharacter(form1.G1EnglishName.value)
	){
		alert("<?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
		form1.G1EnglishName.focus();
		return false;
	}

	if($.trim(form1.G2EnglishName.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>");
		form1.G2EnglishName.focus();
		return false;
	}
	if(
		!checkNaNull(form1.G2EnglishName.value) &&
		!checkIsEnglishCharacter(form1.G2EnglishName.value)
	){
		alert("<?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
		form1.G2EnglishName.focus();
		return false;
	}
	/**** Name END ****/
	
	/**** Contact Number START ****/
	if($.trim(form1.G1MobileNo.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterContactNo']?>");
		form1.G1MobileNo.focus();
		return false;
	}
	if(
		!checkNaNull(form1.G1MobileNo.value) &&
		!/^[0-9]*$/.test(form1.G1MobileNo.value)
	){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['invalidContactNoFormat']?>");
		form1.G1MobileNo.focus();
		return false;
	}
	
	if($.trim(form1.G2MobileNo.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterContactNo']?>");
		form1.G2MobileNo.focus();
		return false;
	}
	if(
		!checkNaNull(form1.G2MobileNo.value) &&
		!/^[0-9]*$/.test(form1.G2MobileNo.value)
	){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['invalidContactNoFormat']?>");
		form1.G2MobileNo.focus();
		return false;
	}
	/**** Contact Number END ****/
	/******** Parent Info END ********/
	return true;
}
</script>