<?php

$hasImage = !!($attachmentList['personal_photo']['link']);
if($hasImage){
    $imageLink = $attachmentList['personal_photo']['link'] . '?_=' . time();
}else{
    $imageLink = $blankphoto;
}

?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/thickbox.css">
<script type="text/javascript" src="/templates/jquery/thickbox-compressed.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.cropit.js"></script>

<table class="form_table">
 <tbody>
 <tr>
	 <td class="field_title" width="20%"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?></td>
	 <td width="30%"><?=$libinterface->GET_TEXTBOX('student_name_b5', 'student_name_b5', $applicationInfo['student_name_b5'], $OtherClass='', $OtherPar=array())?></td>
	 <td width="50%" rowspan="14" width="145">
		 <div id="studentphoto" class="student_info" style="margin:0px;">
	 		<img src="<?=$imageLink?>"/>
			<div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
			<a id = "btn_remove" href="#" class="btn_remove"></a>
			</div>
			<div class="text_remark" style="text-align:center;"><?=$kis_lang['Admission']['msg']['clicktouploadphoto']?></div>
			<input 
				id="openCropPhotoDiv"
				type="button"
				style="margin-top: 5px; width: 136px; border-radius: 8px; border: 1px solid #e26613;"
				
				title="<?=$kis_lang['Admission']['mgf']['cropPhoto'] ?>" 
				class="formbutton"
				
				value="<?=$kis_lang['Admission']['mgf']['cropPhoto'] ?>"
			>
		</div>
	 </td>
 </tr>
 <tr>
	 <td class="field_title" width="30%"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?></td>
	 <td><?=$libinterface->GET_TEXTBOX('student_name_en', 'student_name_en', $applicationInfo['student_name_en'], $OtherClass='', $OtherPar=array())?></td>															 
 </tr>
 <tr>
	 <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['gender']?></td>
	 <td>
	 		<?=$libinterface->Get_Radio_Button('gender_M', 'gender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
			<?=$libinterface->Get_Radio_Button('gender_F', 'gender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>															
	 </td>
 </tr>
 <tr>	
	 <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['dateofbirth']?></td>
	 <td><input type="text" name="dateofbirth" id="dateofbirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span></td>
 </tr>
 <tr>	 
	 <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['Age']?></td>
	 <td><?=$libinterface->GET_TEXTBOX('age', 'age', $applicationInfo['age'], $OtherClass='', $OtherPar=array())?></td>
 </tr>
 <tr>
	 <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['mgf']['birthcertno']?></td>
	 <td nowrap><?=$birth_cert_type_selection?> <?=$libinterface->GET_TEXTBOX('birthcertno', 'birthcertno', $applicationInfo['birthcertno'], $OtherClass='', $OtherPar=array('style'=>'width:150px'))?></td>
 </tr>
 <tr>
	 <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['placeofbirth']?></td>
	 <td><?=$libinterface->GET_TEXTBOX('placeofbirth', 'placeofbirth', $applicationInfo['placeofbirth'], $OtherClass='', $OtherPar=array())?></td>
 </tr>														
 <tr>
	 <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['nativeplace']?></td>
	 <td><input type="text" name="province" id="province" value="<?=$applicationInfo['province']?>">
	 		<!--<table style="width:auto;">
	 			<tr>
	 				<td>(<?=$kis_lang['Admission']['province']?>)</td>
	 				<td><input type="text" name="province" id="province" value="<?=$applicationInfo['province']?>"></td>
	 				<td>(<?=$kis_lang['Admission']['county']?>)</td>
	 				<td><input type="text" name="county" id="county" value="<?=$applicationInfo['county']?>"></td>
	 			</tr>
	 		</table>-->
	 </td>
</tr>
 <tr>		
	 <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['mgf']['phoneno']?></td>
	 <td><input type="text" name="homephoneno" id="homephoneno" value="<?=$applicationInfo['homephoneno']?>"></td>
 </tr>
	<tr>
	 <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['mgf']['homeaddress']?></td>
	 <td><?=$libinterface->GET_TEXTBOX('homeaddress', 'homeaddress', $applicationInfo['homeaddress'], $OtherClass='', $OtherPar=array())?></td>
 </tr>														
 <tr>
	 <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['mgf']['email']?></td>
	 <td><?=$libinterface->GET_TEXTBOX('email', 'email', $applicationInfo['email'], $OtherClass='', $OtherPar=array())?></td>
 </tr>
 <tr>
	 <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['mgf']['religion']?></td>
	 <td><?=$libadmission->displayPresetCodeSelection("RELIGION", "religion", $applicationInfo['religion'])?></td>
 </tr>
 <tr>													 
	 <td class="field_title"><?=$kis_lang['Admission']['church']?></td>
	 <td><?=$libinterface->GET_TEXTBOX('church', 'church', $applicationInfo['church'], $OtherClass='', $OtherPar=array())?></td>
 </tr>	
 <?php
		 for($i=0;$i<sizeof($attachmentSettings);$i++) {
		 	
		 	$attachment_name = $attachmentSettings[$i]['AttachmentName'];
		 	
		 	$_filePath = $attachmentList[$attachment_name]['link'];
		 	if($_filePath){
			$_spanDisplay = '';
			$_buttonDisplay = ' style="display:none;"';
		}else{
			$_filePath = '';
			$_spanDisplay = ' style="display:none;"';
			$_buttonDisplay = '';
		}
		
		$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
		$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';
		 	
		 echo '<tr>
					 	<td class="field_title">'.$attachment_name.'</td>
					 	<td id="'.$attachment_name.'">
					 		<span class="view_attachment" '.$_spanDisplay.'>'.$_attachment.'</span>
				<input type="button" class="attachment_upload_btn formsmallbutton" value="'.$kis_lang['Upload'].'"	id="uploader-'.$attachment_name.'" '.$_buttonDisplay.'/>
			</td>
				 	</tr>';	
		 }
 ?>
																																																						
</tbody></table>


<style>
/**** Image Cropper START ****/
.cropit-preview {
    width: 200px;
    height: 260px;
    margin: 25px auto;
}
.cropit-preview-container {
    border: 1px solid lightgrey;
}
.cropit-preview-image-container{
    cursor: move;
    border: 1px solid red;
}
input.cropit-image-input {
    display: none;
}
input.cropit-image-zoom-input {
    position: relative;
}
input.cropit-image-zoom-input[disabled] {
    cursor: not-allowed;
}
#image-cropper {
    overflow: hidden;
}
.image-control{
    text-align: center;
    padding-bottom: 5px;
}
.cropit-preview-background {
    opacity: .2;
}
.rotateBtn {
    font-size: 2em;
    color: #737373;
    cursor: pointer;
}
.select-image-btn{
    margin-top: 5px;
}
/**** Image Cropper END ****/

/**** Save START ****/
#saveControlBtn{
    margin-top: 10px;
    padding: 10px;
    text-align: center;
}
/**** Save END ****/
</style>
<div id="cropPhotoDiv" style="display:none;">
	<!-- This wraps the whole cropper -->
	<div id="image-cropper">
		<div class="cropit-preview-container">
    		<!-- This is where the preview image is displayed -->
			<div class="cropit-preview"></div>
		</div>
		
		<div class="image-control">
    		<!-- This range input controls zoom -->
    		<!-- You can add additional elements here, e.g. the image icons -->
    		<input type="range" class="cropit-image-zoom-input" />
    		
    		<!-- This is where user selects new image -->
    		<span class="rotateBtn rotate-ccw-btn"><i class="fa fa-rotate-left" aria-hidden="true"></i></span>
    		<span class="rotateBtn rotate-cw-btn"><i class="fa fa-rotate-right" aria-hidden="true"></i></span>
    		<input type="file" class="cropit-image-input" />
    		<!--button 
    			type="button"
    			class="formsubbutton select-image-btn"
    		>
    			<?=$kis_lang['Admission']['mgf']['uploadNewImage'] ?>
    		</button-->
		</div>
	</div>
	
	<div id="saveControlBtn" class="edit_bottom">
    	<button type="button" class="formbutton" id="saveCropImage">
    		<?=$kis_lang['submit']?>
    	</button>
    	&nbsp;
    	<button type="button" class="formsubbutton" onclick="tb_remove()">
    		<?=$kis_lang['cancel']?>
    	</button>
	</div>
</div>




<script>
(function() {
	'use strict';

	function getOriginalImageLink(link){
		var match = /(.*)\/(.*)/.exec(link);
		var folder = match[1];
		var filename = match[2];
		var originalFilename = folder + '/original_' + filename;
		return originalFilename;
	}

	var $imageCropper = $('#image-cropper');
	$imageCropper.cropit({
		imageBackground: true,
		imageBackgroundBorderWidth: 25,
		smallImage: 'stretch',
		maxZoom: 2
	});
	/*$('.select-image-btn').click(function() {
		$('.cropit-image-input').click();
	});*/
    $('.rotate-cw-btn').click(function() {
    	$('#image-cropper').cropit('rotateCW');
    });
    $('.rotate-ccw-btn').click(function() {
    	$('#image-cropper').cropit('rotateCCW');
    });


	$('#openCropPhotoDiv').click(function(){
		$.get('apps/admission/ajax.php?action=getpersonalphoto&id='+$('#recordID').val(), function(res){
            res = $.parseJSON(res);
            var defaultImage = getOriginalImageLink(res.personal_photo);
    		$imageCropper.cropit('imageSrc', defaultImage);

    		tb_show('','#TB_inline?height=420&width=260&inlineId=cropPhotoDiv&modal=true',null);
		});
		return false; // Prevent open default upload behaviour
	});
	$('#saveCropImage').click(function(){
		if(!confirm('<?=$kis_lang['Admission']['mgf']['confirmReplaceCurrentPhoto'] ?>')){
			return false;
		}

		var newPhotoString = $imageCropper.cropit('export', {
            type: 'image/jpeg',
            quality: 1,
            originalSize: true
        });
        //$('#studentphoto > img').attr('src', newPhotoString);
        
        $.post('apps/admission/ajax.php?action=updatecroppedphoto&id='+$('#recordID').val(), {
        	'data': newPhotoString
        }, function(res){
            res = $.parseJSON(res);
            var defaultImage = getOriginalImageLink(res.personal_photo);
        	$('#studentphoto > img').attr('src', res.personal_photo);
            tb_remove();
        });
	});
})();

</script>