<?php

//debug_r($applicationInfo);

?>
<table class="form_table">

<tr>
	<td>&nbsp;</td>
	<td class="form_guardian_head">
		<center><?=$kis_lang['Admission']['PG_Type']['F']?></center>
	</td>
	<td class="form_guardian_head">
		<center><?=$kis_lang['Admission']['PG_Type']['M']?></center>
	</td>
</tr>
<tr>
	<td class="field_title">
		<?=$mustfillinsymbol.$kis_lang['Admission']['englishname']?>
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['F']['parent_name_en']?>" name="parent_name_en[]" type="text" id="G1EnglishName" class="textboxtext" />
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['M']['parent_name_en']?>" name="parent_name_en[]" type="text" id="G2EnglishName" class="textboxtext" />
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$mustfillinsymbol.$kis_lang['Admission']['chinesename']?>
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['F']['parent_name_b5']?>" name="parent_name_b5[]" type="text" id="G1ChineseName" class="textboxtext" />
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['M']['parent_name_b5']?>" name="parent_name_b5[]" type="text" id="G2ChineseName" class="textboxtext" />
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$mustfillinsymbol.$kis_lang['Admission']['munsang']['levelofeducation']?>
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['F']['levelofeducation']?>" name="levelofeducation[]" type="text" id="G1EducationLevel" class="textboxtext" />
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['M']['levelofeducation']?>" name="levelofeducation[]" type="text" id="G2EducationLevel" class="textboxtext" />
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$mustfillinsymbol.$kis_lang['Admission']['occupation']?>
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['F']['occupation']?>" name="occupation[]" type="text" id="G1Occupation" class="textboxtext" />
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['M']['occupation']?>" name="occupation[]" type="text" id="G2Occupation" class="textboxtext" />
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$mustfillinsymbol.$kis_lang['Admission']['icms']['workaddress']?>
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['F']['companyaddress']?>" name="companyaddress[]" type="text" id="G1CompanyAddress" class="textboxtext" />
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['M']['companyaddress']?>" name="companyaddress[]" type="text" id="G2CompanyAddress" class="textboxtext" />
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$mustfillinsymbol.$kis_lang['Admission']['CHIUCHUNKG']['contactnumber']?>
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['F']['mobile']?>" name="mobile[]" type="text" id="G1ContactNumber" class="textboxtext" />
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['M']['mobile']?>" name="mobile[]" type="text" id="G2ContactNumber" class="textboxtext" />
	</td>
</tr>

</table>



<table class="form_table">

<tr>
	<td>&nbsp;</td>
	<td class="form_guardian_head">
		<center><?=$kis_lang['Admission']['PG_Type']['G']?></center>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['name']?>
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['G']['parent_name_b5']?>" name="parent_name_b5[]" type="text" id="G3ChineseName" class="textboxtext" />
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['CHIUCHUNKG']['contactnumber']?>
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['G']['mobile']?>" name="mobile[]" type="text" id="G3ContactNumber" class="textboxtext" />
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['CHIUCHUNKG']['relationship']?>
	</td>
	<td class="form_guardian_field">
		<input value="<?=$applicationInfo['G']['relationship']?>" name="relationship" type="text" id="G3Relationship" class="textboxtext" />
	</td>
</tr>

</table>

<input type="hidden" name="parent_id[]" value="<?=($applicationInfo['F']['RecordID'])?$applicationInfo['F']['RecordID']:'new'?>" />
<input type="hidden" name="parent_id[]" value="<?=($applicationInfo['M']['RecordID'])?$applicationInfo['M']['RecordID']:'new'?>" />
<input type="hidden" name="parent_id[]" value="<?=($applicationInfo['G']['RecordID'])?$applicationInfo['G']['RecordID']:'new'?>" />


<script>
$('#applicant_form').unbind('submit').submit(function(e){
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkValidForm(){
	
	return true;
}
</script>