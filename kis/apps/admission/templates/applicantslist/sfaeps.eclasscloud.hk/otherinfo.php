<?php

global $libkis_admission;

#### Get Student Cust Info START ####
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
#### Get Student Cust Info START ####


#### Get Relatives Info START ####
$siblingsInfo = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
$siblingsInfoArr = array();
foreach ($siblingsInfo as $siblings) {
    foreach ($siblings as $para => $info) {
        $siblingsInfoArr[$siblings['type']][$para] = $info;
    }
}#### Get Relatives Info END ####

?>


<table id="dataTable1" class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="">
        <col style="width:23%">
        <col style="width:23%">
        <col style="width:23%">
    </colgroup>
    <tr>
        <td rowspan="4" class="field_title">
            <?= $kis_lang['Admission']['SFAEPS']['currentSiblingInformation'] ?>
        </td>
        <td>&nbsp;</td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['HKUGAPS']['nameChi'] ?> </center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['HKUGAPS']['nameEng'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['SFAEPS']['studentNo'] ?></center>
        </td>
    </tr>
    <?php for ($i = 1; $i <= 3; $i++): ?>
        <tr>
            <td class="field_title" style="text-align:right;width:50px;">(<?= $i ?>)</td>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($siblingsInfoArr[$i]['name']) ?>
            </td>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($siblingsInfoArr[$i]['englishName']) ?>
            </td>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($siblingsInfoArr[$i]['studentId']) ?>
            </td>
        </tr>
    <?php endfor; ?>
</table>

<table id="dataTable1" class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="">
        <col style="width:17.5%">
        <col style="width:17.5%">
        <col style="width:17.5%">
        <col style="width:17.5%">
    </colgroup>
    <tr>
        <td rowspan="4" class="field_title">
            <?= $kis_lang['Admission']['SFAEPS']['graduatedSiblingInformation'] ?>
        </td>
        <td>&nbsp;</td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['relationship'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['HKUGAPS']['nameChi'] ?> </center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['HKUGAPS']['nameEng'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['HKUGAPS']['graduationYear'] ?> </center>
        </td>
    </tr>
    <?php for ($i = 1; $i <= 3; $i++): ?>
        <tr>
            <td class="field_title" style="text-align:right;width:50px;">(<?= $i ?>)</td>
            <td class="form_guardian_field">
                <?php
                $relstionship = $siblingsInfoArr[$i + 3]['relationship'];
                switch($relstionship){
                    case 'S':
                        echo $kis_lang['Admission']['Sibling'];
                        break;
                    case 'F':
                    case 'M':
                        echo $kis_lang['Admission']['PG_Type'][$relstionship];
                        break;
                    default:
                        echo kis_ui::displayTableField('');
                }
                ?>
            </td>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($siblingsInfoArr[$i + 3]['name']) ?>
            </td>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($siblingsInfoArr[$i + 3]['englishName']) ?>
            </td>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($siblingsInfoArr[$i + 3]['year']) ?>
            </td>
        </tr>
    <?php endfor; ?>
</table>

<table id="dataTable1" class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="">
        <col style="width:23%">
        <col style="width:23%">
        <col style="width:23%">
    </colgroup>
    <tr>
        <td rowspan="4" class="field_title">
            <?= $kis_lang['Admission']['SFAEPS']['awards'] ?>
        </td>
        <td>&nbsp;</td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['SFAEPS']['item'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['SFAEPS']['award'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['SFAEPS']['year'] ?></center>
        </td>
    </tr>
    <?php for ($i = 0; $i < 3; $i++): ?>
        <tr>
            <td class="field_title" style="text-align:right;width:50px;">(<?= $i + 1 ?>)</td>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($applicationCustInfo['AwardItem'][$i]['Value']) ?>
            </td>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($applicationCustInfo['AwardAward'][$i]['Value']) ?>
            </td>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($applicationCustInfo['AwardYear'][$i]['Value']) ?>
            </td>
        </tr>
    <?php endfor; ?>
</table>
