<?php

global $libkis_admission;

######## Get Student Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
$applicationInfo['studentApplicationInfoCust'] = $libkis_admission->getApplicationStudentInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);

#### contact person START ####
$persons = array();
foreach((array)$applicationCustInfo['Contact_Person'] as $contactPerson){
	$persons[] = $contactPerson['Value'];
}
#### contact persion END ####

#### previous School Start ####
foreach($applicationInfo['studentApplicationInfoCust'] as $key=>$school){
	$applicationInfo['studentApplicationInfoCust'][$key]['Class'] = explode(' - ', $school['Class']);
	$applicationInfo['studentApplicationInfoCust'][$key]['Year'] = explode(' - ', $school['Year']);
}
#### previous School End ####

?>
<style>
select:disabled{
    color: #ccc;
}
</style>

<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />
<table class="form_table">
	<tbody>
		<tr> 
			<td width="30%" class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['HKUGAPS']['chinesename'] ?>
			</td>
			<td width="40%">
				<?=$libinterface->GET_TEXTBOX('studentsname_b5', 'studentsname_b5', $applicationInfo['student_name_b5'], $OtherClass='', $OtherPar=array())?>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin:0px;">
					<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
					<div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
						<a id = "btn_remove" href="#" class="btn_remove"></a>
					</div>
					<div class="text_remark" style="text-align:center;">
						<?=$kis_lang['Admission']['msg']['clicktouploadphoto']?>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['HKUGAPS']['englishname'] ?>
			</td>
			<td>
			<?=$libinterface->GET_TEXTBOX('studentsname_en', 'studentsname_en', $applicationInfo['student_name_en'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['dateofbirth'] ?>
			</td>
			<td>
			<input type="text" name="StudentDateOfBirth" id="StudentDateOfBirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;
			<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['gender'] ?>
			</td>
		<td>
			<?=$libinterface->Get_Radio_Button('StudentGender1', 'StudentGender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
			<?=$libinterface->Get_Radio_Button('StudentGender2', 'StudentGender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>                              
		</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['placeofbirth'] ?>
			</td>
			<td>
        		<?php
        		    $placeSelectOption = array();
    				foreach($admission_cfg['PlaceOfBirth'] as $index=>$place){ 
    				    $placeSelectOption[] = array($index, $place);
    				}

    				if(
    				    is_numeric($applicationInfo['placeofbirth']) ||
    				    $applicationInfo['placeofbirth'] == ''
    				){
    				    $selected = $applicationInfo['placeofbirth'];
    				    $otherValue = '';
    				}else{
    				    $selected = '0';
    				    $otherValue = $applicationInfo['placeofbirth'];
    				}
    				
            		echo $libinterface->GET_SELECTION_BOX(
					    $placeSelectOption,
					    'name="StudentPlaceOfBirth" id="StudentPlaceOfBirth"',
					    '',
					    $selected); 
        		?>
				<input name="StudentPlaceOfBirthOther" type="text" id="StudentPlaceOfBirthOther" class="textboxtext" style="width:100px;" value="<?=$otherValue?>" />
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['religion']?>
			</td>
			<td>
			<?php 
			$religiousSelection = array();
			foreach ($admission_cfg['Religious'] as $index => $religious) {
			    $religiousSelection[] = array($index, $religious);
			}
			
			?>
			<?=$libinterface->GET_SELECTION_BOX(
			            $religiousSelection,
					    'name="religion" id="religion"',
					    '',
			            $applicationInfo['religionOther']);?>
			</td>
		</tr>
		<!--<tr>
			<td class="field_title">
        		<?php if($libkis_admission->isFirstYear($applicationInfo['classLevelID'])): ?>
            		<?=$kis_lang['Admission']['HKUGAPS']['spokenLanguageForInterview']?>
        		<?php else: ?>
            		<?=$kis_lang['Admission']['HKUGAPS']['SpokenLanguage']?>
        		<?php endif; ?>
			</td>
			<td colspan="2">
			<?=$libinterface->Get_Radio_Button('SpokenLanguageForInterview1', 'SpokenLanguageForInterview', 'Cantonese', ($applicationInfo['langspokenathome']=='Cantonese'), '', $kis_lang['Admission']['Languages']['Cantonese'])?>
			<?=$libinterface->Get_Radio_Button('SpokenLanguageForInterview2', 'SpokenLanguageForInterview', 'English', ($applicationInfo['langspokenathome']=='English'), '', $kis_lang['Admission']['Languages']['English'])?>  
			<?=$libinterface->Get_Radio_Button('SpokenLanguageForInterview3', 'SpokenLanguageForInterview', 'Putonghua', ($applicationInfo['langspokenathome']=='Putonghua'), '', $kis_lang['Admission']['Languages']['Putonghua'])?>  
			</td>
		</tr>-->
		<tr>
			<td class="field_title">
				<?=$mustfillinsymbol?><?=$kis_lang['Admission']['HKUGAPS']['personalIdentification'] ?>
			</td>
			<td nowrap="" colspan="2">
				<?=$libinterface->GET_SELECTION_BOX(array(array("1",$kis_lang['Admission']['BirthCertType']['hk2']),array("2",$kis_lang['Admission']['HKUGAPS']['birthcertno']),array("3",$kis_lang['Admission']['BirthCertType']['others'])),'name="BirthCertType" id="BirthCertType" onchange="showOtherTypeTextField(this.value)"',$kis_lang['Admission']['PleaseSelect'],$applicationInfo['birthcerttype']); ?>
				
			<input name="BirthCertTypeOther" type="text" id="BirthCertTypeOther" class="textboxtext" style="width:100px" value="<?=$applicationInfo['birthcerttypeother']?>" <?=$applicationInfo['birthcerttype'] == "3"?"":"hidden" ?>/>
        			<label for="BirthCertTypeOther"><?=$kis_lang['Admission']['HKUGAPS']['no'] ?></label>
        			<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" value="<?=$applicationInfo['birthcertno'] ?>"/>
        			<br/>
        			<?=$kis_lang['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
			</td>
		</tr> 
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['address'] ?>
			</td>
			<td colspan="2">
            	<table>
            		<tr>
            			<td >
							<?=$libinterface->GET_TEXTBOX('HomeAddress', 'HomeAddress',$applicationInfo['Address'], $OtherClass='', $OtherPar=array())?>
						</td>
					</tr>
            	</table>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['hometel'] ?>
			</td>
			<td >
				<?=$libinterface->GET_TEXTBOX('TelHome', 'HomeTelNo',$applicationInfo['homephoneno'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
				<tr>   
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['contactperson'] ?>
			</td>
			<td >
				<?=$libinterface->Get_Checkbox('ContactPerson1','ContactPerson[]','Father',(in_array('Father',$persons)),'',$kis_lang['Admission']['PG_Type']['F']);?>
				<?=$libinterface->Get_Checkbox('ContactPerson2','ContactPerson[]','Mother',(in_array('Mother',$persons)),'',$kis_lang['Admission']['PG_Type']['M']);?>
				<?=$libinterface->Get_Checkbox('ContactPerson3','ContactPerson[]','Guardian',(in_array('Guardian',$persons)),'',$kis_lang['Admission']['PG_Type']['G']);?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?=$mustfillinsymbol?><?= $kis_lang['Admission']['HKUGAPS']['contactEmail'] ?>
			</td>
			<td>
				<table>
					<tr>
						<td style="width: 20px">1)</td>
						<td>
							<?=$libinterface->GET_TEXTBOX('ContactEmail', 'ContactEmail',$applicationInfo['email'], $OtherClass='', $OtherPar=array())?>
						</td>
					</tr>
					<tr>
						<td>2)</td>
						<td>
							<?=$libinterface->GET_TEXTBOX('ContactEmail2', 'ContactEmail2',$applicationInfo['email2'], $OtherClass='', $OtherPar=array())?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['noBrotherSister'] ?>
			</td>
			<td>
    			<select name="NoBrotherSister" id="NoBrotherSister">
    				<option value="" ><?=$kis_lang['Admission']['PleaseSelect'] ?></option>
    				<?php
    				for($i=0;$i<11;$i++){
    				    if(
    				        $applicationCustInfo['No_Brother_Sister'][0]['Value'] == $i &&
    				        $applicationCustInfo['No_Brother_Sister'][0]['Value'] !== '' && 
    				        $applicationCustInfo['No_Brother_Sister'][0]['Value'] !== null
    			        ){
    				        $selected = 'selected';
    				    }else{
    				        $selected = '';
    				    }
    			    ?>
    			    	<option value="<?=$i ?>" <?=$selected ?>><?=$i ?></option>
    			    <?php
    				}
    				?>
    			</select>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['rank'] ?>
			</td>
			<td>
    			<select name="BrotherSisterRank" id="BrotherSisterRank">
    				<option value="" ><?=$kis_lang['Admission']['PleaseSelect'] ?></option>
    				<?php
    				for($i=1;$i<11;$i++){
    				    if(
    				        $applicationCustInfo['Brother_Sister_Rank'][0]['Value'] == $i &&
    				        $applicationCustInfo['Brother_Sister_Rank'][0]['Value'] !== '' && 
    				        $applicationCustInfo['Brother_Sister_Rank'][0]['Value'] !== null
    			        ){
    				        $selected = 'selected';
    				    }else{
    				        $selected = '';
    				    }
    			    ?>
    			    	<option value="<?=$i ?>" <?=$selected ?>><?=$i ?></option>
    			    <?php
    				}
    				?>
    			</select>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?=$mustfillinsymbol?>
        		<?php if($libkis_admission->isFirstYear($applicationInfo['classLevelID'])): ?>
            		<?=$kis_lang['Admission']['HKUGAPS']['currentAttend_Y1']?>
        		<?php else: ?>
            		<?=$kis_lang['Admission']['HKUGAPS']['currentAttend']?>
        		<?php endif; ?>
			</td>
			<td colspan="2">
				<table id="dataTable1" class="form_table" style="font-size: 13px">
				<tr>
					<td>&nbsp;</td>
					<td class="form_guardian_head" style="width: 300px;"><center><?=$kis_lang['Admission']['HKUGAPS']['name'] ?></center></td>
					<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['grades'] ?></center></td>
					<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['year'] ?></center></td>
				</tr>
				<tr>
					<td class="field_title" style="text-align:right;width:50px;">(1)</td>
					<td class="form_guardian_field">
						<input name="CurrentAttend1" type="text" id="CurrentAttend1" class="textboxtext" value="<?=$applicationInfo['studentApplicationInfoCust'][0]['NameOfSchool']?>"/>
					</td>
					<td class="form_guardian_field">
						<input name="Grades1Start" type="text" id="Grades1Start" value="<?=$applicationInfo['studentApplicationInfoCust'][0]['Class'][0]?>" style="width: 40%"/>
						-
						<input name="Grades1End" type="text" id="Grades1End" value="<?=$applicationInfo['studentApplicationInfoCust'][0]['Class'][1]?>" style="width: 40%"/>
					</td>
					<td class="form_guardian_field">
						<input name="Year1Start" type="text" id="Year1Start" maxlength="4" value="<?=$applicationInfo['studentApplicationInfoCust'][0]['Year'][0]?>" style="width: 40%"/>
						-
						<input name="Year1End" type="text" id="Year1End" maxlength="4" value="<?=$applicationInfo['studentApplicationInfoCust'][0]['Year'][1]?>" style="width: 40%"/>
					</td>
				</tr>
				<tr>
					<td class="field_title" style="text-align:right;width:50px;">(2)</td>
					<td class="form_guardian_field">
						<input name="CurrentAttend2" type="text" id="CurrentAttend2" class="textboxtext" value="<?=$applicationInfo['studentApplicationInfoCust'][1]['NameOfSchool']?>"/>
					</td>
					<td class="form_guardian_field">
						<input name="Grades2Start" type="text" id="Grades2Start" value="<?=$applicationInfo['studentApplicationInfoCust'][1]['Class'][0]?>" style="width: 40%"/>
						-
						<input name="Grades2End" type="text" id="Grades2End" value="<?=$applicationInfo['studentApplicationInfoCust'][1]['Class'][1]?>" style="width: 40%"/>
					</td>
					<td class="form_guardian_field">
						<input name="Year2Start" type="text" id="Year2Start" maxlength="4" value="<?=$applicationInfo['studentApplicationInfoCust'][1]['Year'][0]?>" style="width: 40%"/>
						-
						<input name="Year2End" type="text" id="Year2End" maxlength="4" value="<?=$applicationInfo['studentApplicationInfoCust'][1]['Year'][1]?>" style="width: 40%"/>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		
		<?php if(!$libkis_admission->isFirstYear($applicationInfo['classLevelID'])){ ?>
    		<tr>   
    			<td class="field_title">
    				<?= $kis_lang['Admission']['HKUGAPS']['specialEducationalNeeds'] ?>
    			</td>
    			<td colspan="2">
        			<input type="radio" value="1" id="specialEducationalNeedsY" name="specialEducationalNeeds" <?=$applicationCustInfo['specialEducationalNeeds'][0]['Value']=='1'?'checked':'' ?>>
        			<label for="specialEducationalNeedsY"> <?=$kis_lang['Admission']['yes']?></label>
        			(
        				<label for="specialEducationalNeedsY">
        					<?=$kis_lang['Admission']['PleaseSpecify'] ?>
        				</label>
        				<input name="specialEducationalNeeds_Details" type="text" id="specialEducationalNeeds_Details" class="textboxtext" style="width: 300px;" value="<?=$applicationCustInfo['specialEducationalNeeds_Details'][0]['Value']?>"/>
        			)
    				<br />
        			<input type="radio" value="0" id="specialEducationalNeedsN" name="specialEducationalNeeds" <?=$applicationCustInfo['specialEducationalNeeds'][0]['Value']=='1'?'':'checked' ?>>
        			<label for="specialEducationalNeedsN"> <?=$kis_lang['Admission']['no']?></label>
    			</td>
    		</tr>
    		<tr>   
    			<td class="field_title">
    				<?= $kis_lang['Admission']['HKUGAPS']['appliedBefore'] ?>
    			</td>
    			<td colspan="2">
        			<input type="radio" value="1" id="appliedBeforeY" name="appliedBefore" <?=$applicationCustInfo['appliedBefore'][0]['Value']=='1'?'checked':'' ?>>
        			<label for="appliedBeforeY"> <?=$kis_lang['Admission']['yes']?></label>
        			(
        				<label for="appliedBeforeY">
        					<?=$kis_lang['Admission']['HKUGAPS']['PleaseSpecifyYear'] ?>
        				</label>
        				<input name="appliedBefore_Details" type="text" id="appliedBefore_Details" class="textboxtext" style="width: 300px;" value="<?=$applicationCustInfo['appliedBefore_Details'][0]['Value']?>"/>
        			)
    				<br />
        			<input type="radio" value="0" id="appliedBeforeN" name="appliedBefore" <?=$applicationCustInfo['appliedBefore'][0]['Value']=='1'?'':'checked' ?>>
        			<label for="appliedBeforeN"> <?=$kis_lang['Admission']['no']?></label>
    			</td>
    		</tr>
		<?php } ?>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['twinsApp'] ?>
			</td>
			<td>
				<?=$libinterface->Get_Radio_Button('twinsY', 'twins', '1', ($applicationInfo['istwinsapplied']=='1'), '', $kis_lang['Admission']['yes'])?>
				<?=$libinterface->Get_Radio_Button('twinsN', 'twins', '0', ($applicationInfo['istwinsapplied']=='0'), '', $kis_lang['Admission']['no'])?>                              
			</td>
		</tr>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['twinsID'] ?>
			</td>
			<td>
				<?=$libinterface->GET_TEXTBOX('twinsapplicationid', 'twinsapplicationid',$applicationInfo['twinsapplicationid'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
	<?php
	for($i=0;$i<sizeof($attachmentSettings);$i++) {
		$attachment_name = $attachmentSettings[$i]['AttachmentName'];

		
//		$_filePath = $attachmentList[$attachment_name]['link'];
		$_filePath = '/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($attachment_name);

		if($attachmentList[$attachment_name]['link']){
			$_spanDisplay = '';
			$_buttonDisplay = ' style="display:none;"';
		}else{
			$_filePath = '';
			$_spanDisplay = ' style="display:none;"';
			$_buttonDisplay = '';
		}
		
		$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
		$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';
	?>
		<tr>
			<td class="field_title">
				<?=$attachment_name?>
			</td>
			<td id="<?=$attachment_name?>">
				<span class="view_attachment" <?=$_spanDisplay?>><?=$_attachment?></span>
				<input type="button" class="attachment_upload_btn formsmallbutton" value="<?=$kis_lang['Upload']?>"  id="uploader-<?=$attachment_name?>" <?=$_buttonDisplay?>/>
			</td>
		</tr>	
	<?php
	}
	?>                                                                           
	</tbody>
</table>

<script>
//// UI Releated START ////
$('#StudentPlaceOfBirth').change(function(){
	if($(this).val() == '0'){
		$('#StudentPlaceOfBirthOther').show();
	}else{
		$('#StudentPlaceOfBirthOther').hide();
	}
}).change();
$('#HomeAddrArea').change(function(){
	if($(this).val() == '3'){
		$('#HomeAddrAreaOther').show();
	}else{
		$('#HomeAddrAreaOther').hide();
	}
}).change();
$('#NoBrotherSister').change(function(){
	if($(this).val() > '0'){
		$('#BrotherSisterRank').removeAttr('disabled');
	}else{
		$('#BrotherSisterRank').attr('disabled', 'disabled');
	}
}).change();
$('input[name="twins"]').change(function(){
	if($(this).attr('checked')){
    	if($(this).val() == '1'){
    		$('#twinsapplicationid').removeAttr('disabled');
    	}else{
    		$('#twinsapplicationid').attr('disabled', 'disabled');
    	}
	}
}).change();

function showOtherTypeTextField(value){

	if(value == "3"){
		$('#BirthCertTypeOther').show();
	}else{
		$('#BirthCertTypeOther').hide();
	}
	
}
////UI Releated END ////

$('#applicant_form').unbind('submit').submit(function(e){
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	
	return false;
});

function check_hkid(hkid) {
	// hkid = $.trim(hkid);
	// hkid = hkid.replace(/\s/g, '');
	// hkid = hkid.toUpperCase();
	// $(":input[name='id_no']").val(hkid);

	var re = /^([A-Z]{1,2})((\d){6})\({0,1}([A0-9]{1})\){0,1}$/g;
	var ra = re.exec(hkid);

	if (ra != null) {
		var p1 = ra[1];
		var p2 = ra[2];
		var p3 = ra[4];
		var check_sum = 0;
		if (p1.length == 2) {
			check_sum = (p1.charCodeAt(0)-55) * 9 + (p1.charCodeAt(1)-55) * 8;
		}
		else if (p1.length == 1){
			check_sum = 324 + (p1.charCodeAt(0)-55) * 8;
		}

		check_sum += parseInt(p2.charAt(0)) * 7 + parseInt(p2.charAt(1)) * 6 + parseInt(p2.charAt(2)) * 5 + parseInt(p2.charAt(3)) * 4 + parseInt(p2.charAt(4)) * 3 + parseInt(p2.charAt(5)) * 2;
		var check_digit = 11 - (check_sum % 11);
		if (check_digit == '11') {
			check_digit = 0;
		}
		else if (check_digit == '10') {
			check_digit = 'A';
		}
		if (check_digit == p3 ) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}
	
function checkValidForm(){
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var dOBRange = new Array();
	   $.ajax({
	       url: "admission_form/ajax_get_bday_range.php",
	       type: "post",
	       data: $("#applicant_form").serialize(),
	       async: false,
	       success: function(data){
	           //alert("debugging: The classlevel is updated!");
	           dOBRange = data.split(",");
	       },
	       error:function(){
	           //alert("failure");
	           $("#result").html('There is error while submit');
	       }
	   });
	  
	/******** Student Info START ********/
	/**** Name START ****/
	if($.trim(applicant_form.studentsname_b5.value)==''){
		alert("<?=$kis_lang['Admission']['msg']['enterchinesename']?>");	
		applicant_form.studentsname_b5.focus();
		return false;
	}
//	if(
//		!checkNaNull(applicant_form.studentsname_b5.value) && 
//		!checkIsChineseCharacter(applicant_form.studentsname_b5.value)
//	){
//		alert("<?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
//		applicant_form.studentsname_b5.focus();
//		return false;
//	}
	
	if($.trim(applicant_form.studentsname_en.value)==''){
		alert("<?=$kis_lang['Admission']['msg']['enterenglishname']?>");	
		applicant_form.studentsname_en.focus();
		return false;
	}
	if(
		!checkNaNull(applicant_form.studentsname_en.value) &&
		!checkIsEnglishCharacter(applicant_form.studentsname_en.value)
	){
		alert("<?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
		applicant_form.studentsname_en.focus();
		return false;
	}
	/**** Name END ****/
	
	/**** DOB START ****/
	if(!applicant_form.StudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
		if(applicant_form.StudentDateOfBirth.value!=''){
			alert("<?=$kis_lang['Admission']['msg']['invaliddateformat']?>");
		}
		else{
			alert("<?=$kis_lang['Admission']['msg']['enterdateofbirth']?>");	
		}
		
		applicant_form.StudentDateOfBirth.focus();
		return false;
	} 
	if(dOBRange[0] !='' && applicant_form.StudentDateOfBirth.value < dOBRange[0] || dOBRange[1] !='' && applicant_form.StudentDateOfBirth.value > dOBRange[1]){
		alert("<?=$kis_lang['Admission']['msg']['invalidbdaydateformat']?>");
		applicant_form.StudentDateOfBirth.focus();
		return false;
	} 
	/**** DOB END ****/

	/**** Gender START ****/
	if($.trim($('input:radio[name=StudentGender]:checked').val())==''){
		alert("<?=$kis_lang['Admission']['msg']['selectgender']?>");	
		applicant_form.StudentGender[0].focus();
		return false;
	}
	/**** Gender END ****/
	
	/**** Place of Birth START ****/
	if($.trim(applicant_form.StudentPlaceOfBirth.value)==''){
		alert("<?=$kis_lang['Admission']['msg']['enterplaceofbirth']?>");	
		applicant_form.StudentPlaceOfBirth.focus();
		return false;
	} 
	if($.trim(applicant_form.StudentPlaceOfBirth.value)=='0' && $.trim(applicant_form.StudentPlaceOfBirthOther.value)==''){
		alert("<?=$kis_lang['Admission']['msg']['enterplaceofbirth']?>");	
		applicant_form.StudentPlaceOfBirthOther.focus();
		return false;
	} 
	/**** Place of Birth END ****/
	
	/**** Personal Identification START ****/
	if($.trim(applicant_form.BirthCertType.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['birthcertType']?>");	
		applicant_form.BirthCertType.focus();
		return false;
	}

	if($.trim(applicant_form.BirthCertType.value)=='1' && $.trim(applicant_form.StudentBirthCertNo.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterBirthCert']?>");	
		applicant_form.StudentBirthCertNo.focus();
		return false;
	}

	if($.trim(applicant_form.BirthCertType.value)=='2' && $.trim(applicant_form.StudentBirthCertNo.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterHKID']?>");
		applicant_form.StudentBirthCertNo.focus();
		return false;
	}

	if(
		(
			$.trim(applicant_form.BirthCertType.value)=='2' && 
			!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test(applicant_form.StudentBirthCertNo.value)
		)||(
			$.trim(applicant_form.BirthCertType.value)=='2' && 
			!check_hkid(applicant_form.StudentBirthCertNo.value)
		)
	){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['invalidHKID']?>");	
		applicant_form.StudentBirthCertNo.focus();
		return false;
	}

	if($.trim(applicant_form.BirthCertType.value)=='3' && $.trim(applicant_form.BirthCertTypeOther.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterIdentification']?>");	
		applicant_form.BirthCertTypeOther.focus();
		return false;
	} 
	if($.trim(applicant_form.BirthCertType.value)=='3' && $.trim(applicant_form.StudentBirthCertNo.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterIdentificationNo']?>");	
		applicant_form.StudentBirthCertNo.focus();
		return false;
	}
	
	/**** Personal Identification END ****/

	/**** Address START ****/
	/*if(applicant_form.StudentHomeAddressChi.value==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enteraddress']?>");	
		applicant_form.StudentHomeAddressChi.focus();
		return false;
	}*/
	/**** Address END ****/

	/**** Tel START ****/
	/*if(applicant_form.TelHome.value==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterTel']?>");	
		applicant_form.TelHome.focus();
		return false;
	}*/
	if(!/^[0-9]*$/.test(applicant_form.TelHome.value)){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['invalidContactNoFormat']?>");
		applicant_form.TelHome.focus();
		return false;
	}
	/**** Tel END ****/
	
	/**** Contact Person START ****/
	if($('[name="ContactPerson\[\]"]:checked').length == 0){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['SelectContactPerson']?>");	
		applicant_form.ContactPerson1.focus();
		return false;
	} 
	/**** Contact Person END ****/
	
	/**** Email START ****/
    if($.trim(applicant_form.ContactEmail.value)==''){
		alert("<?=$kis_lang['Admission']['icms']['msg']['entermailaddress']?>");
		applicant_form.ContactEmail.focus();
		return false;
	}
	if(!re.test(applicant_form.ContactEmail.value)){
		alert("<?=$kis_lang['Admission']['icms']['msg']['invalidmailaddress']?>");
		applicant_form.ContactEmail.focus();
		return false;
	}
//    if($.trim(applicant_form.ContactEmail2.value)==''){
//		alert("<?=$kis_lang['Admission']['icms']['msg']['entermailaddress']?>");
//		applicant_form.ContactEmail2.focus();
//		return false;
//	}
	if($.trim(applicant_form.ContactEmail2.value) && !re.test(applicant_form.ContactEmail2.value)){
		alert("<?=$kis_lang['Admission']['icms']['msg']['invalidmailaddress']?>");
		applicant_form.ContactEmail2.focus();
		return false;
	}
	/**** Email END ****/
	
	/**** School Attending START ****/
	if(applicant_form.CurrentAttend1.value=='' && applicant_form.CurrentAttend2.value==''){
		alert("<?=$kis_lang['Admission']['UCCKE']['msg']['enterLastSchool']?>");	
		applicant_form.CurrentAttend1.focus();
		return false;
	}
	if(applicant_form.CurrentAttend1.value!=''){
    	if(applicant_form.Grades1Start.value==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterGrade']?>");	
    		applicant_form.Grades1Start.focus();
    		return false;
    	} 
    	if(applicant_form.Grades1End.value==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterGrade']?>");	
    		applicant_form.Grades1End.focus();
    		return false;
    	} 
    	if(applicant_form.Year1Start.value==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterYear']?>");	
    		applicant_form.Year1Start.focus();
    		return false;
    	}
    	if(applicant_form.Year1Start.value!='' && !/^[0-9]{4}$/.test(applicant_form.Year1Start.value)){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['invalidYearFormat']?>");
    		applicant_form.Year1Start.focus();
    		return false;
    	}
    	if(applicant_form.Year1End.value==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterYear']?>");	
    		applicant_form.Year1End.focus();
    		return false;
    	} 
    	if(applicant_form.Year1End.value!='' && !/^[0-9]{4}$/.test(applicant_form.Year1End.value)){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['invalidYearFormat']?>");
    		applicant_form.Year1End.focus();
    		return false;
    	}
	}
	
	if(applicant_form.CurrentAttend2.value!=''){
    	if(applicant_form.Grades2Start.value==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterGrade']?>");	
    		applicant_form.Grades2Start.focus();
    		return false;
    	} 
    	if(applicant_form.Grades2End.value==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterGrade']?>");	
    		applicant_form.Grades2End.focus();
    		return false;
    	} 
    	if(applicant_form.Year2Start.value==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterYear']?>");	
    		applicant_form.Year2Start.focus();
    		return false;
    	} 
    	if(applicant_form.Year2Start.value!='' && !/^[0-9]{4}$/.test(applicant_form.Year2Start.value)){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['invalidYearFormat']?>");
    		applicant_form.Year2Start.focus();
    		return false;
    	}
    	if(applicant_form.Year2End.value==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterYear']?>");	
    		applicant_form.Year2End.focus();
    		return false;
    	} 
    	if(applicant_form.Year2End.value!='' && !/^[0-9]{4}$/.test(applicant_form.Year2End.value)){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['invalidYearFormat']?>");
    		applicant_form.Year2End.focus();
    		return false;
    	}
	}
	/**** School Attending END ****/
	
	/**** Twins Chekcing START ****/
	if($('#twinsY').attr('checked') && $.trim(applicant_form.twinsapplicationid.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterTwinsID']?>");	
		applicant_form.twinsapplicationid.focus();
		return false;
	}
	/**** Twins Chekcing END ****/
	
	
	/******** Student Info END ********/
	
	return true;
}
</script>