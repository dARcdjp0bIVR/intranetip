<?php

global $libkis_admission;

######## Get Student Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
$applicationInfo['studentApplicationInfoCust'] = $libkis_admission->getApplicationStudentInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);

#### Lang for interview START ####
if($applicationInfo['langspokenathome'] == 'Cantonese'){
	$langSpoken = $kis_lang['Admission']['Languages']['Cantonese'];
}else if($applicationInfo['langspokenathome'] == 'Putonghua'){
	$langSpoken = $kis_lang['Admission']['Languages']['Putonghua'];
}else if($applicationInfo['langspokenathome'] == 'English'){
	$langSpoken = $kis_lang['Admission']['Languages']['English'];
}
#### Lang for interview END ####

#### Personal Identification START ####
$persionalId = "";
if($applicationInfo['birthcerttype'] == "1"){
	$persionalId = "{$kis_lang['Admission']['BirthCertType']['hk2']}: {$applicationInfo['birthcertno']}";
}else if ($applicationInfo['birthcerttype'] == "2"){
	$persionalId = "{$kis_lang['Admission']['HKUGAPS']['birthcertno']}: {$applicationInfo['birthcertno']}";
}else if ($applicationInfo['birthcerttype'] == "3"){
	$persionalId = "{$applicationInfo['birthcerttypeother']}: {$applicationInfo['birthcertno']}";
}
#### Personal Identification END ####

#### contact person START ####
$persons = array();
foreach((array)$applicationCustInfo['Contact_Person'] as $contactPerson){
	if($contactPerson['Value'] == "Father"){
		$persons[] = $kis_lang['Admission']['PG_Type']['F'];
	}else if($contactPerson['Value'] == "Mother"){
		$persons[] = $kis_lang['Admission']['PG_Type']['M'];
	}else if($contactPerson['Value'] == "Guardian"){
		$persons[] = $kis_lang['Admission']['PG_Type']['G'];
	}
}
$persons = implode(', ', $persons);
#### contact persion END ####

#### previous School Start ####
$perviousSchool = array();
foreach($applicationInfo['studentApplicationInfoCust'] as $school){
	$perviousSchool[] = "{$school['NameOfSchool']} {$kis_lang['Admission']['HKUGAPS']['grades']}({$school['Class']}) {$kis_lang['Admission']['HKUGAPS']['year']}({$school['Year']})";
}
$perviousSchool = implode('<br/> ', $perviousSchool);
#### previous School End ####

#### twins start Start ####
if($applicationInfo["istwinsapplied"] == '1'){
	$twins = "{$kis_lang['Admission']['yes']} ({$kis_lang['Admission']['HKUGAPS']['twinsID']}: {$applicationInfo["twinsapplicationid"]})";
}else{
	$twins = $kis_lang['Admission']['no'];
}
#### twins start Start ####
?>
<table class="form_table">
	<tbody>
		<tr> 
			<td width="30%" class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['chinesename'] ?>
			</td>
			<td width="40%">
				<?=$applicationInfo['student_name_b5']?>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin:0px;">
					<img src="<?= $attachmentList['personal_photo']['link'] ? $attachmentList['personal_photo']['link'] : $blankphoto ?>?_=<?= time() ?>"/>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['englishname'] ?>
			</td>
			<td>
				<?=$applicationInfo['student_name_en']?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['dateofbirth'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationInfo['dateofbirth']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['gender'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['gender']]) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['placeofbirth'] ?>
			</td>
			<td>
				<?php
				    if($admission_cfg['PlaceOfBirth'][ $applicationInfo['placeofbirth'] ]){
				        echo kis_ui::displayTableField($admission_cfg['PlaceOfBirth'][ $applicationInfo['placeofbirth'] ]);
				    }else{
				        echo kis_ui::displayTableField($applicationInfo['placeofbirth']);
				    }
				?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['religion'] ?>
			</td>
			<td>
				<?if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){
				    if($applicationInfo['religionOther'] == 0){
				        $religious = $admission_cfg['Religious'][0];
				    }elseif($applicationInfo['religionOther'] == 1){
				        $religious = $admission_cfg['Religious'][1];
				    }else{
				        $religious = $admission_cfg['Religious'][2];
				    }
				    
				    echo kis_ui::displayTableField($religious);
				}else{?>
				<?= kis_ui::displayTableField($applicationInfo['religionOther']) ?>
				<?}?>

			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['HKUGAPS']['personalIdentification'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($persionalId) ?>
			</td>
		</tr>           
		<!--<tr>
			<td class="field_title">
        		<?php if($libkis_admission->isFirstYear($applicationInfo['classLevelID'])): ?>
            		<?=$kis_lang['Admission']['HKUGAPS']['spokenLanguageForInterview']?>
        		<?php else: ?>
            		<?=$kis_lang['Admission']['HKUGAPS']['SpokenLanguage']?>
        		<?php endif; ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($langSpoken) ?>
			</td>
		</tr>-->
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['address'] ?>
			</td>
			<td colspan="2">
            	<table>
            		<tr>
            			<?= kis_ui::displayTableField($applicationInfo['Address']) ?>
            		</tr>
            	</table>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['hometel'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($applicationInfo['homephoneno']) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['contactEmail'] ?>
			</td>
			<td colspan="2">
				1) <?= kis_ui::displayTableField($applicationInfo['email']) ?>
				<br />
				2) <?= kis_ui::displayTableField($applicationInfo['email2']) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['noBrotherSister'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($applicationCustInfo['No_Brother_Sister'][0]['Value']) ?>
				<?= ($applicationCustInfo['No_Brother_Sister'][0]['Value'] > '0')?  " ({$kis_lang['Admission']['HKUGAPS']['rank']}: ".kis_ui::displayTableField($applicationCustInfo['Brother_Sister_Rank'][0]['Value']).")" : '' ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['contactperson'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($persons) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
        		<?php if($libkis_admission->isFirstYear($applicationInfo['classLevelID'])): ?>
            		<?=$kis_lang['Admission']['HKUGAPS']['currentAttend_Y1']?>
        		<?php else: ?>
            		<?=$kis_lang['Admission']['HKUGAPS']['currentAttend']?>
        		<?php endif; ?>
			</td>
			<td colspan="2">
				<table id="dataTable1" class="form_table" style="font-size: 13px">
				<tr>
					<td>&nbsp;</td>
					<td class="form_guardian_head" style="width: 300px;"><center><?=$kis_lang['Admission']['HKUGAPS']['name'] ?></center></td>
					<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['grades'] ?></center></td>
					<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['year'] ?></center></td>
				</tr>
				<tr>
					<td class="field_title" style="text-align:right;width:50px;">(1)</td>
					<td class="form_guardian_field">
						<?=kis_ui::displayTableField($applicationInfo['studentApplicationInfoCust'][0]['NameOfSchool'])?>
					</td>
					<td class="form_guardian_field">
						<?=kis_ui::displayTableField($applicationInfo['studentApplicationInfoCust'][0]['Class'])?>
					</td>
					<td class="form_guardian_field">
						<?=kis_ui::displayTableField($applicationInfo['studentApplicationInfoCust'][0]['Year'])?>
					</td>
				</tr>
				<tr>
					<td class="field_title" style="text-align:right;width:50px;">(2)</td>
					<td class="form_guardian_field">
						<?=kis_ui::displayTableField($applicationInfo['studentApplicationInfoCust'][1]['NameOfSchool'])?>
					</td>
					<td class="form_guardian_field">
						<?=kis_ui::displayTableField($applicationInfo['studentApplicationInfoCust'][1]['Class'])?>
					</td>
					<td class="form_guardian_field">
						<?=kis_ui::displayTableField($applicationInfo['studentApplicationInfoCust'][1]['Year'])?>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		
		<?php if(!$libkis_admission->isFirstYear($applicationInfo['classLevelID'])){ ?>
    		<tr>   
    			<td class="field_title">
    				<?= $kis_lang['Admission']['HKUGAPS']['specialEducationalNeeds'] ?>
    			</td>
    			<td colspan="2">
        			<?php 
            		    if($applicationCustInfo['specialEducationalNeeds'][0]['Value']){
            		        echo $kis_lang['Admission']['yes'];
            		        if($applicationCustInfo['specialEducationalNeeds_Details'][0]['Value']){
            		            echo " ({$applicationCustInfo['specialEducationalNeeds_Details'][0]['Value']})";
            		        }
            		    }else{
            		        echo $kis_lang['Admission']['no'];
            		    }
        			?>
    			</td>
    		</tr>
    		<tr>   
    			<td class="field_title">
    				<?= $kis_lang['Admission']['HKUGAPS']['appliedBefore'] ?>
    			</td>
    			<td colspan="2">
        			<?php 
            		    if($applicationCustInfo['appliedBefore'][0]['Value']){
            		        echo $kis_lang['Admission']['yes'];
            		        if($applicationCustInfo['appliedBefore_Details'][0]['Value']){
            		            echo " ({$applicationCustInfo['appliedBefore_Details'][0]['Value']})";
            		        }
            		    }else{
            		        echo $kis_lang['Admission']['no'];
            		    }
        			?>
    			</td>
    		</tr>
		<?php } ?>
		
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['twinsApp'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($twins) ?>
			</td>
		</tr>
	
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['document'] ?>
			</td>
			<td colspan="2">
				<?php
					$attachmentAry = array();
					foreach ($attachmentList as $_type => $_attachmentAry) {
						if ($_type != 'personal_photo') {
							if ($_attachmentAry['link']) {
								//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
								$attachmentAry[] = '<a href="/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($_type) . '" target="_blank">' . $_type . '</a>';
							}
						}
					}
				?>
				<?= count($attachmentAry) == 0 ? '--' : implode('<br/>', $attachmentAry); ?>
			</td>
		</tr>                                                                                 
	</tbody>
</table>