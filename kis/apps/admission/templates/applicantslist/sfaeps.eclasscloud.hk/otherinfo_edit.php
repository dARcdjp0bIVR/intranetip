<?php

global $libkis_admission;

#### Get Admission Year START ####
$admission_year = getAcademicYearByAcademicYearID($libkis_admission->getNextSchoolYearID());
$admission_year_start = substr($admission_year, 0, 4);
#### Get Admission Year END ####

#### Get Student Cust Info START ####
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
#### Get Student Cust Info START ####

#### Get Relatives Info START ####
$siblingsInfo = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
$siblingsInfoArr = array();
foreach ($siblingsInfo as $siblings) {
    foreach ($siblings as $para => $info) {
        $siblingsInfoArr[$siblings['type']][$para] = $info;
    }
}

#### Get Relatives Info END ####
?>
<style>
    select:disabled {
        color: #ccc;
    }
</style>

<input type="hidden" name="ApplicationID" value="<?= $applicationInfo['applicationID'] ?>"/>


<table id="dataTable1" class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="">
        <col style="width:23%">
        <col style="width:23%">
        <col style="width:23%">
    </colgroup>
    <tr>
        <td rowspan="4" class="field_title">
            <?= $kis_lang['Admission']['SFAEPS']['currentSiblingInformation'] ?>
        </td>
        <td>&nbsp;</td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['HKUGAPS']['nameChi'] ?> </center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['HKUGAPS']['nameEng'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['SFAEPS']['studentNo'] ?></center>
        </td>
    </tr>
    <?php for ($i = 1; $i <= 3; $i++): ?>
        <tr>
            <td class="field_title" style="text-align:right;width:50px;">(<?= $i ?>)</td>
            <td class="form_guardian_field">
                <input name="OthersRelativeStudiedNameChi<?= $i ?>" type="text"
                       id="OthersRelativeStudiedNameChi<?= $i ?>"
                       class="textboxtext" value="<?= $siblingsInfoArr[$i]['name'] ?>"/>
            </td>
            <td class="form_guardian_field">
                <input name="OthersRelativeStudiedNameEng<?= $i ?>" type="text"
                       id="OthersRelativeStudiedNameEng<?= $i ?>"
                       class="textboxtext" value="<?= $siblingsInfoArr[$i]['englishName'] ?>"/>
            </td>
            <td class="form_guardian_field">
                <input name="OthersRelativeStudiedStudentId<?= $i ?>" type="text"
                       id="OthersRelativeStudiedStudentId<?= $i ?>"
                       class="textboxtext" value="<?= $siblingsInfoArr[$i]['studentId'] ?>"/>
            </td>
        </tr>
    <?php endfor; ?>
</table>

<table id="dataTable1" class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="">
        <col style="width:17.5%">
        <col style="width:17.5%">
        <col style="width:17.5%">
        <col style="width:17.5%">
    </colgroup>
    <tr>
        <td rowspan="4" class="field_title">
            <?= $kis_lang['Admission']['SFAEPS']['graduatedSiblingInformation'] ?>
        </td>
        <td>&nbsp;</td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['relationship'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['HKUGAPS']['nameChi'] ?> </center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['HKUGAPS']['nameEng'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['HKUGAPS']['graduationYear'] ?> </center>
        </td>
    </tr>
    <?php for ($i = 1; $i <= 3; $i++): ?>
        <tr>
            <td class="field_title" style="text-align:right;width:50px;">(<?= $i ?>)</td>
            <td class="form_guardian_field">
                <?php $relstionship = $siblingsInfoArr[$i + 3]['relationship']; ?>
                <select name="OthersRelativeGraduationRelationship<?= $i ?>"
                        id="OthersRelativeGraduationRelationship<?= $i ?>"
                        class="textboxtext"
                >
                    <option value="" <?= ($relstionship == '') ? 'selected' : '' ?>><?= $kis_lang['Admission']['pleaseSelect'] ?></option>
                    <option value="F" <?= ($relstionship == 'F') ? 'selected' : '' ?>><?= $kis_lang['Admission']['PG_Type']['F'] ?></option>
                    <option value="M" <?= ($relstionship == 'M') ? 'selected' : '' ?>><?= $kis_lang['Admission']['PG_Type']['M'] ?></option>
                    <option value="S" <?= ($relstionship == 'S') ? 'selected' : '' ?>><?= $kis_lang['Admission']['Sibling'] ?></option>
                </select>
            </td>
            <td class="form_guardian_field">
                <input name="OthersRelativeGraduationNameChi<?= $i ?>" type="text"
                       id="OthersRelativeGraduationNameChi<?= $i ?>"
                       class="textboxtext" value="<?= $siblingsInfoArr[$i + 3]['name'] ?>"/>
            </td>
            <td class="form_guardian_field">
                <input name="OthersRelativeGraduationNameEng<?= $i ?>" type="text"
                       id="OthersRelativeGraduationNameEng<?= $i ?>"
                       class="textboxtext" value="<?= $siblingsInfoArr[$i + 3]['englishName'] ?>"/>
            </td>
            <td class="form_guardian_field">
                <input
                        name="OthersRelativeGraduationYear<?= $i ?>"
                        type="text"
                        id="OthersRelativeGraduationYear<?= $i ?>"
                        class="textboxtext" value="<?= $siblingsInfoArr[$i + 3]['year'] ?>"
                        maxlength="4"
                />
            </td>
        </tr>
    <?php endfor; ?>
</table>

<table id="dataTable1" class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="">
        <col style="width:23%">
        <col style="width:23%">
        <col style="width:23%">
    </colgroup>
    <tr>
        <td rowspan="4" class="field_title">
            <?= $kis_lang['Admission']['SFAEPS']['awards'] ?>
        </td>
        <td>&nbsp;</td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['SFAEPS']['item'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['SFAEPS']['award'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['SFAEPS']['year'] ?></center>
        </td>
    </tr>
    <?php for ($i = 1; $i <= 3; $i++): ?>
        <tr>
            <td class="field_title" style="text-align:right;width:50px;">(<?= $i ?>)</td>
            <td class="form_guardian_field">
                <input name="awardItem<?= $i ?>" type="text"
                       id="awardItem<?= $i ?>"
                       class="textboxtext" value="<?= $applicationCustInfo['AwardItem'][$i - 1]['Value'] ?>"/>
            </td>
            <td class="form_guardian_field">
                <input name="awardAward<?= $i ?>" type="text"
                       id="awardAward<?= $i ?>"
                       class="textboxtext" value="<?= $applicationCustInfo['AwardAward'][$i - 1]['Value'] ?>"/>
            </td>
            <td class="form_guardian_field">
                <input name="awardYear<?= $i ?>" type="text"
                       id="awardYear<?= $i ?>"
                       class="textboxtext" value="<?= $applicationCustInfo['AwardYear'][$i - 1]['Value'] ?>"
                       maxlength="4"
                />
            </td>
        </tr>
    <?php endfor; ?>
</table>


<script>

    $('#applicant_form').unbind('submit').submit(function (e) {
        var schoolYearId = $('#schoolYearId').val();
        var recordID = $('#recordID').val();
        var display = $('#display').val();
        var timeSlot = lang.timeslot.split(',');
        if (checkValidForm()) {
            $.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function (success) {
                $.address.value('/apps/admission/applicantslist/details/' + schoolYearId + '/' + recordID + '/' + display + '&sysMsg=' + success);
            });
        }
        return false;
    });

    function checkIsChineseCharacter(str) {
        return str.match(/^[\u3400-\u9FBF]*$/);
    }

    function checkIsEnglishCharacter(str) {
        return str.match(/^[A-Za-z ]*$/);
    }

    function checkNaNull(str) {
        return (
            ($.trim(str).toLowerCase() == '沒有') ||
            ($.trim(str).toLowerCase() == 'nil') ||
            ($.trim(str).toLowerCase() == 'n.a.')
        );
    }

    function checkValidForm() {
        var form1 = $('#applicant_form')[0];
        var isTeacherInput = true;

        /******** Other Info START ********/
        <?php for($i = 1;$i <= 3;$i++):?>
        /**** Sibling Studying START ****/
        if (form1.OthersRelativeStudiedNameChi<?=$i?>.value != '') {
            /*if (
                !checkNaNull(form1.OthersRelativeStudiedNameChi<?=$i?>.value) &&
                !checkIsChineseCharacter(form1.OthersRelativeStudiedNameChi<?=$i?>.value)
            ) {
                alert("<?=$Lang['Admission']['msg']['enterchinesecharacter']?>\nPlease enter Chinese character.");
                form1.OthersRelativeStudiedNameChi<?=$i?>.focus();
                return false;
            }*/
            if (
                !checkNaNull(form1.OthersRelativeStudiedNameEng<?=$i?>.value) &&
                !checkIsEnglishCharacter(form1.OthersRelativeStudiedNameEng<?=$i?>.value)
            ) {
                alert("<?=$Lang['Admission']['msg']['enterenglishcharacter']?>\nPlease enter English character.");
                form1.OthersRelativeStudiedNameEng<?=$i?>.focus();
                return false;
            }

            if (!isTeacherInput) {
                if ($.trim(form1.OthersRelativeStudiedNameEng<?=$i?>.value) == '') {
                    alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>\nPlease Enter Sibling Information.");
                    form1.OthersRelativeStudiedNameEng<?=$i?>.focus();
                    return false;
                }
                if ($.trim(form1.OthersRelativeStudiedStudentId<?=$i?>.value) == '') {
                    alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>\nPlease Enter Sibling Information.");
                    form1.OthersRelativeStudiedStudentId<?=$i?>.focus();
                    return false;
                }
            }
        }
        /**** Sibling Studying END ****/

        /**** Sibling Graduated START ****/
        /**** Relationship START ****/
        if (form1.OthersRelativeGraduationRelationship<?=$i?>.value == '' && $.trim(form1.OthersRelativeGraduationNameChi<?=$i?>.value) != '') {
            alert("<?=$Lang['Admission']['SFAEPS']['msg']['selectRelationship']?>\n<?=$LangEn['Admission']['SFAEPS']['msg']['selectRelationship']?>");
            form1.OthersRelativeGraduationRelationship<?=$i?>.focus();
            return false;
        }
        /**** Relationship END ****/

        if (form1.OthersRelativeGraduationNameChi<?=$i?>.value != '') {
            /*if (
                !checkNaNull(form1.OthersRelativeGraduationNameChi<?=$i?>.value) &&
                !checkIsChineseCharacter(form1.OthersRelativeGraduationNameChi<?=$i?>.value)
            ) {
                alert("<?=$Lang['Admission']['msg']['enterchinesecharacter']?>\nPlease enter Chinese character.");
                form1.OthersRelativeGraduationNameChi<?=$i?>.focus();
                return false;
            }*/
            if (
                !checkNaNull(form1.OthersRelativeGraduationNameEng<?=$i?>.value) &&
                !checkIsEnglishCharacter(form1.OthersRelativeGraduationNameEng<?=$i?>.value)
            ) {
                alert("<?=$Lang['Admission']['msg']['enterenglishcharacter']?>\nPlease enter English character.");
                form1.OthersRelativeGraduationNameEng<?=$i?>.focus();
                return false;
            }

            if (!isTeacherInput) {
                if ($.trim(form1.OthersRelativeGraduationNameEng<?=$i?>.value) == '') {
                    alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>\nPlease Enter Sibling Information.");
                    form1.OthersRelativeGraduationNameEng<?=$i?>.focus();
                    return false;
                }
                if ($.trim(form1.OthersRelativeGraduationYear<?=$i?>.value) == '') {
                    alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>\nPlease Enter Sibling Information.");
                    form1.OthersRelativeGraduationYear<?=$i?>.focus();
                    return false;
                }
            }
            if (form1.OthersRelativeGraduationYear<?=$i?>.value != '' && !/^[0-9]{4}$/.test(form1.OthersRelativeGraduationYear<?=$i?>.value)) {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['invalidYearFormat']?>\nInvalid Year format.");
                form1.OthersRelativeGraduationYear<?=$i?>.focus();
                return false;
            }
        }
        /**** Sibling Graduated END ****/
        <?php endfor; ?>
        /******** Other Info END ********/

        /******** Awards START ********/
        <?php for($i = 1;$i <= 3;$i++):?>
        if (form1.awardItem<?=$i?>.value != '') {
            if (form1.awardYear<?=$i?>.value != '' && !/^[0-9]{4}$/.test(form1.awardYear<?=$i?>.value)) {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['invalidYearFormat']?>\nInvalid Year format.");
                form1.awardYear<?=$i?>.focus();
                return false;
            }
        }
        <?php endfor; ?>
        /******** Awards END ********/

        return true;
    }
</script>