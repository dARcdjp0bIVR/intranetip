<?php

?>
<table class="form_table">
	<tbody>
	<tr> 
		<td width="30%" class="field_title">
			<?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)
		</td>
		<td width="40%">
		<?php
			$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);
			echo $libinterface->GET_TEXTBOX('student_surname_b5', 'student_surname_b5', $tempStuEngName[0], $OtherClass='', $OtherPar=array())
		?>
		</td>
		<td width="30%" rowspan="7" width="145">
			<div id="studentphoto" class="student_info" style="margin:0px;">
				<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
				<div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
					<a id = "btn_remove" href="#" class="btn_remove"></a>
				</div>
				<div class="text_remark" style="text-align:center;">
					<?=$kis_lang['Admission']['msg']['clicktouploadphoto']?>
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td class="field_title">
		<?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)
		</td>
		<td>
		<?php
			$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);
			echo $libinterface->GET_TEXTBOX('student_firstname_b5', 'student_firstname_b5', $tempStuEngName[1], $OtherClass='', $OtherPar=array())
		?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)
		</td>
		<td>
		<?php
			$tempStuEngName = explode(',',$applicationInfo['student_name_en']);
			echo $libinterface->GET_TEXTBOX('student_surname_en', 'student_surname_en', $tempStuEngName[0], $OtherClass='', $OtherPar=array())
		?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)
		</td>
		<td>
		<?php
			$tempStuEngName = explode(',',$applicationInfo['student_name_en']);
			echo $libinterface->GET_TEXTBOX('student_firstname_en', 'student_firstname_en', $tempStuEngName[1], $OtherClass='', $OtherPar=array())
		?>
		</td>
	</tr>
	<tr>   
		<td class="field_title">
			<?=$mustfillinsymbol?><?=$kis_lang['Admission']['dateofbirth']?>
		</td>
		<td>
			<input type="text" name="dateofbirth" id="dateofbirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;
			<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['gender']?>
		</td>
		<td>
			<?=$libinterface->Get_Radio_Button('gender_M', 'gender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
			<?=$libinterface->Get_Radio_Button('gender_F', 'gender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>                              
		</td>
	</tr>
	<tr>   
		<td class="field_title">
			<?=$kis_lang['Admission']['placeofbirth']?>
		</td>
		<td>
			<?=$libinterface->GET_TEXTBOX('placeofbirth', 'placeofbirth', $applicationInfo['placeofbirth'], $OtherClass='', $OtherPar=array())?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['TSUENWANBCKG']['nationality']?>
		</td>
		<td>
			<?=$libinterface->GET_TEXTBOX('nationality', 'nationality', $applicationInfo['county'], $OtherClass='', $OtherPar=array())?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['KTLMSKG']['IdType']?>
		</td>
		<td>
			<?=$libinterface->Get_Radio_Button('BirthCertType1', 'BirthCertType', $admission_cfg['BirthCertType']['hk'], ($applicationInfo['birthcerttype']==$admission_cfg['BirthCertType']['hk']), '', $kis_lang['Admission']['BirthCertType']['hk2'])?>
			<?=$libinterface->Get_Radio_Button('BirthCertType2', 'BirthCertType', $admission_cfg['BirthCertType']['others'], ($applicationInfo['birthcerttype']==$admission_cfg['BirthCertType']['others']), '', $kis_lang['Admission']['BirthCertType']['others'].': ')?>
			<?=$libinterface->GET_TEXTBOX('BirthCertTypeOther', 'BirthCertTypeOther', $applicationInfo['birthcerttypeother'], $OtherClass='', $OtherPar=array('style'=>'width:30%;', 'onkeypress' => "\$('#BirthCertType2').attr('checked', 'checked')"))?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['KTLMSKG']['IdNum']?>
		</td>
		<td>
			<?=$libinterface->GET_TEXTBOX('BirthCertNo', 'BirthCertNo', $applicationInfo['birthcertno'], $OtherClass='', $OtherPar=array())?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['KTLMSKG']['address']?>
		</td>
		<td>
			<?=$libinterface->GET_TEXTBOX('homeaddress', 'homeaddress', $applicationInfo['homeaddress'], $OtherClass='', $OtherPar=array())?>
		</td>
	</tr>                   
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['KTLMSKG']['correspondenceAddress']?>
		</td>
		<td>
			<?=$libinterface->GET_TEXTBOX('contactaddress', 'contactaddress', $applicationInfo['contactaddress'], $OtherClass='', $OtherPar=array())?>
		</td>
	</tr>
	<tr>   
		<td class="field_title">
			<?=$kis_lang['Admission']['munsang']['phone']?>
		</td>
		<td>
			<input type="text" name="homephoneno" id="homephoneno" value="<?=$applicationInfo['homephoneno']?>">
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['religion']?>
		</td>
		<td>
			<?=$libinterface->GET_TEXTBOX('religion', 'religion', $applicationInfo['religionOther'], $OtherClass='', $OtherPar=array())?>
		</td>
	</tr>
	<tr>   
		<td class="field_title">
			<?=$mustfillinsymbol?><?=$kis_lang['Admission']['contactEmail']?>
		</td>
		<td>
			<input type="text" name="email" id="email" class="textboxtext" value="<?=$applicationInfo['email']?>">
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['KTLMSKG']['twins']?>
		</td>
		<td>
			<?=$libinterface->Get_Radio_Button('twinsY', 'twins', '1', ($applicationInfo['istwinsapplied']=='1'), '', $kis_lang['Admission']['yes'])?>
			<?=$libinterface->Get_Radio_Button('twinsN', 'twins', '0', ($applicationInfo['istwinsapplied']=='0'), '', $kis_lang['Admission']['no'])?>                              
		</td>
	</tr>
	<tr>   
		<td class="field_title">
			<?=$kis_lang['Admission']['KTLMSKG']['twinsID']?>
		</td>
		<td>
			<input type="text" class="textboxtext"
				name="twinsapplicationid" id="twinsapplicationid" 
				value="<?=($applicationInfo['istwinsapplied']=='1')?$applicationInfo['twinsapplicationid']:''?>"
				onkeypress="$('#twinsY').attr('checked', 'checked')"
			/>
		</td>
	</tr>
	
	
	<?php
	for($i=0;$i<sizeof($attachmentSettings);$i++) {
		$attachment_name = $attachmentSettings[$i]['AttachmentName'];
		
		$_filePath = $attachmentList[$attachment_name]['link'];
		if($_filePath){
			$_spanDisplay = '';
			$_buttonDisplay = ' style="display:none;"';
		}else{
			$_filePath = '';
			$_spanDisplay = ' style="display:none;"';
			$_buttonDisplay = '';
		}
		
		$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
		$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';
	?>
		<tr>
			<td class="field_title">
				<?=$attachment_name?>
			</td>
			<td id="<?=$attachment_name?>">
				<span class="view_attachment" <?=$_spanDisplay?>><?=$_attachment?></span>
				<input type="button" class="attachment_upload_btn formsmallbutton" value="<?=$kis_lang['Upload']?>"  id="uploader-<?=$attachment_name?>" <?=$_buttonDisplay?>/>
			</td>
		</tr>	
	<?php
	}
	?>                                                                           
	</tbody>
</table>

<script>
$('#applicant_form').unbind('submit').submit(function(e){
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkValidForm(){
		
	if($('#student_surname_b5').val() == ''){
		alert(lang.studentssurname_b5_csm);
		$('#student_surname_b5').focus();
		return false;
	}
	
	if($('#student_firstname_b5').val() == ''){
		alert(lang.studentsfirstname_b5_csm);
		$('#student_firstname_b5').focus();
		return false;
	}
	
	if($('#student_surname_en').val() == ''){
		alert(lang.studentssurname_en_csm);
		$('#student_surname_en').focus();
		return false;
	}
	
	if($('#student_firstname_en').val() == ''){
		alert(lang.studentsfirstname_en_csm);
		$('#student_firstname_en').focus();
		return false;
	}
	
	if(!check_date_without_return_msg($('#dateofbirth')[0])){
		alert(lang.invaliddateformat);
		$('#dateofbirth').focus();
		return false;
	}
	/*
	if(!$("input[name=gender]:checked").val()){
		alert(lang.selectgender);
		$('#gender_M').focus();
		return false;
	}
	
	if($('#placeofbirth').val()==''){
		alert(lang.enterplaceofbirth);
		$('#placeofbirth').focus();
		return false;
	}
	
	if($('#nationality').val()==''){
		alert(lang.enternationality_icms);
		$('#nationality').focus();
		return false;
	}*/
	
	if(
		$('#BirthCertType2').attr('checked') == 'checked' &&
		$('#BirthCertTypeOther').val() == ''
	){
		alert('<?=$kis_lang['Admission']['KTLMSKG']['msg']['enterBirthCertTypeOther']?>');
		$('#BirthCertTypeOther').focus();
		return false;
	}
	/*
	if($('#BirthCertNo').val() == ''){
		alert('<?=$kis_lang['Admission']['KTLMSKG']['msg']['enterIdNum']?>');
		$('#BirthCertNo').focus();
		return false;
	}
	
	if($('#homeaddress').val() == ''){
		alert(lang.enterhomeaddress);
		$('#homeaddress').focus();
		return false;
	}
	
	/*if($('#homephoneno').val() == ''){
		alert(lang.enterstudenthomephoneno);
		$('#homephoneno').focus();
		return false;
	}*/
	
	if($('#email').val() == ''){
		alert(lang.enterstudentemail);
		$('#email').focus();
		return false;
	}
	
	if( $('#twinsY').attr('checked') == 'checked' && $('#twinsapplicationid').val() == ''){
		alert('<?=$kis_lang['Admission']['KTLMSKG']['msg']['enterTwinsIdNum']?>');
		$('#twinsapplicationid').focus();
		return false;
	}
	
	return true;
}
</script>