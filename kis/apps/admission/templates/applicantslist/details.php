<?php
// Editing by 
/**
 * 2020-06-30 (Tommy): modified $sys_custom['KIS_Admission']['MINGWAI']['Settings'] briefingsession title
 * 2019-05-07 (Pun): Added prev/next page
 * 2017-08-31 (Pun): Modified cust for mosgraceful
 * 2016-08-25 (Ronald): Added $sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] customization
 * 2016-08-25 (Ronald): Added $sys_custom['KIS_Admission']['MINGWAI']['Settings'] customization
 * 2015-10-12 (Henry):	added checking for $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission-interview-marker"]
 * 2015-09-22 (Henry):  modified cust for munsang
 * 2015-07-27 (Pun):	added dynamic template, see ./ktlmskg.eclass.hk/ folder
 * 2015-07-20 (Henry):	added cust for tsuenwanbckg.eclass.hk
 * 2015-07-16 (Omas):	modified cust for $sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']
 * 2015-07-03 (Henry):	added cust for chiuchunkg.edu.hk
 * 2014-10-22 (Henry):	added cust for eclassk.munsang.edu.hk
 * 2014-01-15 (Carlos): Modified the display of other attachments at [studentinfo] section
 */

global $setting_path_ip_rel;

#### Get prev/next record url START ####
$prevRecordId = $kis_data['libadmission']->getPrevApplicationRecordId($recordID);
$nextRecordId = $kis_data['libadmission']->getNextApplicationRecordId($recordID);

$url = explode('/', $_SERVER['REQUEST_URI']);
$currentPage = end($url);
$currentPage = is_numeric($currentPage)?'':$currentPage;
#### Get prev/next record url END ####

?>
<script type="text/javascript">
$(function(){
	kis.admission.application_details_init({invaliddateformat:'<?=$kis_lang['Admission']['msg']['invaliddateformat']?>'},
		{
			returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
			close:'<?=$kis_lang['close']?>'
		}

	);
});
</script>
<style>
.prevNext{
    text-align: right;
    padding-right: 15px;
    margin-top: -32px;
}
.prevNext > a > i{
    background: url(/images/kis/btn_next_prev.png) no-repeat;
    height: 25px;
    display: inline-block;
    width: 25px;
    position: relative;
    top: 10px;
}
.prevNext .next{
    background-position-y: -50px;
}
</style>
<div class="main_content_detail">
    <? if($isValidRecord): ?>
    	<?=$kis_data['NavigationBar']?>

		<div class="prevNext">
    		<?php if($prevRecordId): ?>
        		<a href="#/apps/admission/applicantslist/details/<?=$schoolYearID ?>/<?=$prevRecordId ?>/<?=$currentPage?>"><i class="prev"></i> <span><?=$kis_lang['Admission']['PreviousRecord']?></span></a>
    		<?php endif; ?>
    		<?php if($nextRecordId): ?>
    			&nbsp;
	            <a href="#/apps/admission/applicantslist/details/<?=$schoolYearID ?>/<?=$nextRecordId ?>/<?=$currentPage?>"><span><?=$kis_lang['Admission']['NextRecord']?></span> <i class="next"></i></a>
    		<?php endif; ?>
        </div>

    	<!--tab-->
    	<?
    	if($sys_custom['KIS_Admission']['CSM']['Settings']){
    		kis_ui::loadModuleTab(array('studentinfo','parentinfo','remarks'), $display, '#/apps/admission/applicantslist/details/'.$schoolYearID.'/'.$recordID.'/',2 );
    	}else if($sys_custom['KIS_Admission']['ICMS']['Settings']){
    		kis_ui::loadModuleTab(array('icmssectiona','icmssectionb','icmssectionc','icmssectiondef','icmsattachment','remarks'), $display, '#/apps/admission/applicantslist/details/'.$schoolYearID.'/'.$recordID.'/',5 );
    	}else if(method_exists($lauc, 'getModuleTab')){
    	    $moduleTab = $lauc->getModuleTab();
    	    $defaultTabIndex = $lauc->getModuleTabDefaultIndex();
    	    kis_ui::loadModuleTab($moduleTab, $display, '#/apps/admission/applicantslist/details/'.$schoolYearID.'/'.$recordID.'/', $defaultTabIndex );
    	}else{
    		kis_ui::loadModuleTab(array('studentinfo','parentinfo','otherinfo','remarks'), $display, '#/apps/admission/applicantslist/details/'.$schoolYearID.'/'.$recordID.'/',3 );
    	}
    	?>
        <!--tab end-->
        <p class="spacer"></p>
                     <!---->
                     <p class="spacer"></p>
                     <form id="application_form" name="application_form" method="post">
                     	 <input type="hidden" name="applicationAry[]" id="application_<?=$recordID?>" value="<?=$recordID?>">
                    	 <input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$schoolYearID?>">
					</form>
                      	  <div class="table_board">
							<div class="Content_tool">
								<?if(!$sys_custom['KIS_Admission']['EditUploadDocumentMode']){?>
								<a href="#" class="print" target="_blank"><?=$kis_lang['print']?></a>
								<?}?>
								<?if($sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings']){?>
								<a href="#" class="print print_interview_form" target="_blank"><?=$kis_lang['Admission']['MINGWAI']['printinterviewform']?></a>
								<?}?>
							</div>
                      	   	<?if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission-interview-marker"]){?>
                      	   	<div class="common_table_tool">
                            	<a class="tool_edit" href="#/apps/admission/applicantslist/edit/<?=$schoolYearID?>/<?=$recordID?>/<?=$display?>/"><?=$kis_lang['edit']?><input type="hidden" id="display" name="display" value="<?=$display?>"></a>
                          	</div>
                          	<?}?>
<?
    if(file_exists("{$PATH_WRT_ROOT}/includes/admission/{$setting_path_ip_rel}/template/applicantsList/{$display}.php")){
        include("{$PATH_WRT_ROOT}/includes/admission/{$setting_path_ip_rel}/template/applicantsList/{$display}.php");
    }elseif($sys_custom['KIS_Admission']['DynamicAdmissionForm']){
        $lauc->getDetailsPage($display, false);
    }else if($display=='icmssectiona'):?>
                      	    <table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['schoolYearOption']?></td>
                               <td width="70%"><?=kis_ui::displayTableField(getAcademicYearByAcademicYearID($applicationInfo['schoolYearId']))?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['selectcampus']?></td>
                               <td width="70%"><?=kis_ui::displayTableField($kis_lang['Admission']['icms']['tinhaucampus'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['selectprogram']?></td>
                               <td><?=kis_ui::displayTableField($classLevelAry[$applicationInfo['classLevelID']])?></td>
                             </tr>
                              <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['applyDayType']?></td>
                               <td>
                              	 	<table style="width:auto;">
                               			<tr>
                               				<?
                               					$idx = 1;
                               					for($i=1;$i<=count($kis_lang['Admission']['icms'][$classLevelAry[$applicationInfo['classLevelID']]]['TimeSlot']);$i++):
                               						$_applyDayType = $applicationInfo['ApplyDayType'.$i];
                               						if(!empty($_applyDayType)):
                               				?>
	                               					<td>(<?=$kis_lang['Admission']['Option']?> <?=$idx?>) <?=kis_ui::displayTableField($kis_lang['Admission']['icms'][$classLevelAry[$applicationInfo['classLevelID']]]['TimeSlot'][$_applyDayType])?></td>
	                               					<?$idx++;?>
                               					<?endif;?>
                               				<?endfor;?>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                           </tbody></table>
<? elseif($display=='icmssectionb'):?>
                      	    <table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['icms']['studentssurname']?></td>
                               <td width="40%"><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo kis_ui::displayTableField($tempStuEngName[0])?></td>
                               <td width="30%" rowspan="7" width="145">
	                               <div id="studentphoto" class="student_info" style="margin:0px;">
	                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								   </div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['firstname']?></td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo kis_ui::displayTableField($tempStuEngName[1])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['nameinchinese']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['student_name_b5'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['gender']?></td>
                               <td><?=kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['gender']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['dateofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['placeofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['nationality']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['province'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['homeaddress']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homeaddress'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['homenumber'] ?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homephoneno'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['emailaddress']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['email'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['CurBSName']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($otherApplicationInfo['CurBSName'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['langspokenathome']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['langspokenathome'])?></td>
                             </tr>
                           </tbody></table>
<? elseif($display=='icmssectionc'):?>
                      	    <table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <? foreach($kis_lang['Admission']['icms']['PG_Type'] as $_pgKey => $_pgLang): ?>
                               <td class="form_guardian_head" width="35%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                               		$tempEnglishName = explode(',',$applicationInfo[$_pgKey]['parent_name_en']);

                                	$parentAry['studentssurname'][] = '<td class="form_guardian_field">';
                               		$parentAry['studentssurname'][] = kis_ui::displayTableField($tempEnglishName[0]);
                               		$parentAry['studentssurname'][] = '</td>';

                               		$parentAry['firstname'][] = '<td class="form_guardian_field">';
                               		$parentAry['firstname'][] = kis_ui::displayTableField($tempEnglishName[1]);
                               		$parentAry['firstname'][] = '</td>';

                               		$parentAry['mobilenumber'][] = '<td class="form_guardian_field">';
                                	$parentAry['mobilenumber'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['mobile']);
                                	$parentAry['mobilenumber'][] = '</td>';

                               		$parentAry['nativelanguage'][] = '<td class="form_guardian_field">';
                                	$parentAry['nativelanguage'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['nativelanguage']);
                                	$parentAry['nativelanguage'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['occupation']);
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['worknumber'][] = '<td class="form_guardian_field">';
                                	$parentAry['worknumber'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['companyphone']);
                                	$parentAry['worknumber'][] = '</td>';

                                	$parentAry['workaddress'][] = '<td class="form_guardian_field">';
                                	$parentAry['workaddress'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['companyaddress']);
                                	$parentAry['workaddress'][] = '</td>';

                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=$kis_lang['Admission']['icms'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
<? elseif($display=='icmssectiondef'):?>
                      	    <table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['icms']['listprevschool']?></td>
                               <td width="70%"><?=kis_ui::displayTableField($studentApplicationInfo['lastschool'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['childdescription']?></td>
                               <td width="70%"><?=kis_ui::displayTableField($applicationInfo['childdescription'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['childhealth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['childhealth'])?></td>
                             </tr>
                               <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['icms']['yourwish']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['yourwish'])?></td>
                             </tr>
                           </tbody></table>
<? elseif($display=='icmsattachment'):?>
							<table class="form_table">
                             <tbody>
                       			<tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['document']?></td>
                               <td width="70%">
                               <?
                               		$attachmentAry = array();
	                               	foreach($attachmentList as $_type => $_attachmentAry){
	                               		if($_type != 'personal_photo'){
	                               			if($_attachmentAry['link']){
	                               				//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
	                               				$attachmentAry[] = '<a href="/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($_type).'" target="_blank">'.$_type.'</a>';
	                               			}
	                               		}
	                               	}
                               ?>
                               <?=count($attachmentAry)==0?'--':implode('<br/>',$attachmentAry);?>
                               </td>
                             </tr>
                           </tbody></table>
<? elseif($display=='studentinfo'):?>
	<?if($sys_custom['KIS_Admission']['YLSYK']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['chinesename']?></td>
                               <td width="40%"><?echo kis_ui::displayTableField($applicationInfo['student_name_b5'])?></td>
                               <td width="30%" rowspan="7" width="145">
	                               <div id="studentphoto" class="student_info" style="margin:0px;">
	                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								   </div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['englishname']?></td>
                               <td><?echo kis_ui::displayTableField($applicationInfo['student_name_en'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['gender']?></td>
                               <td><?=kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['gender']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['dateofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['placeofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['YLSYK']['birthcertno']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['birthcertno'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['TSUENWANBCKG']['nationality']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['county'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['religion']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['religionOther'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['homephoneno']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homephoneno'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['TSUENWANBCKG']['homeaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Chi'].')'?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homeaddresschi'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['TSUENWANBCKG']['homeaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Eng'].')'?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homeaddress'])?></td>
                             </tr>
                            <tr>
                              <td class="field_title"><?=$kis_lang['Admission']['csm']['email']?></td>
                              <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['email'])?></td>
                            </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['document']?></td>
                               <td colspan="2">
                               <?
                               		$attachmentAry = array();
	                               	foreach($attachmentList as $_type => $_attachmentAry){
	                               		if($_type != 'personal_photo'){
	                               			if($_attachmentAry['link']){
	                               				//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
	                               				$attachmentAry[] = '<a href="/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($_type).'" target="_blank">'.$_type.'</a>';
	                               			}
	                               		}
	                               	}
                               ?>
                               <?=count($attachmentAry)==0?'--':implode('<br/>',$attachmentAry);?>
                               </td>
                             </tr>
                           </tbody></table>
    <?}else if($sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td width="40%"><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo kis_ui::displayTableField($tempStuEngName[0])?></td>
                               <td width="30%" rowspan="7" width="145">
	                               <div id="studentphoto" class="student_info" style="margin:0px;">
	                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								   </div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo kis_ui::displayTableField($tempStuEngName[1])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo kis_ui::displayTableField($tempStuEngName[0])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo kis_ui::displayTableField($tempStuEngName[1])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['dateofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['gender']?></td>
                               <td><?=kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['gender']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['placeofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['langspokenathome']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['homelang'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['religion']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['religionOther'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['church']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['church'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['munsang']['birthcertno']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['birthcertno'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['munsang']['phone']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homephoneno'])?></td>
                             </tr>
                               <td class="field_title"><?=$kis_lang['Admission']['munsang']['mobile']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['mobileno'])?></td>
                             </tr>
                            <tr>
                              <td class="field_title"><?=$kis_lang['Admission']['contactEmail']?></td>
                              <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['email'])?></td>
                            </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['TSUENWANBCKG']['homeaddress']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homeaddress'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['TSUENWANBCKG']['contactaddress']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['contactaddress'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['document']?></td>
                               <td colspan="2">
                               <?
                               		$attachmentAry = array();
	                               	foreach($attachmentList as $_type => $_attachmentAry){
	                               		if($_type != 'personal_photo'){
	                               			if($_attachmentAry['link']){
	                               				//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
	                               				$attachmentAry[] = '<a href="/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($_type).'" target="_blank">'.$_type.'</a>';
	                               			}
	                               		}
	                               	}
                               ?>
                               <?=count($attachmentAry)==0?'--':implode('<br/>',$attachmentAry);?>
                               </td>
                             </tr>
                           </tbody></table>
    <?}else if($sys_custom['KIS_Admission']['RMKG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td width="40%"><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo kis_ui::displayTableField($tempStuEngName[0])?></td>
                               <td width="30%" rowspan="7" width="145">
	                               <div id="studentphoto" class="student_info" style="margin:0px;">
	                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								   </div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo kis_ui::displayTableField($tempStuEngName[1])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo kis_ui::displayTableField($tempStuEngName[0])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo kis_ui::displayTableField($tempStuEngName[1])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['dateofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['gender']?></td>
                               <td><?=kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['gender']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td>
                               		<?=kis_ui::displayTableField($applicationInfo['placeofbirth'])?>
                               	</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['RMKG']['nationality']?></td>
                               <td>
                               		<?=kis_ui::displayTableField($applicationInfo['Nationality'])?>
                               	</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['birthcertno']?></td>
                               <td>
	                               <?=kis_ui::displayTableField($applicationInfo['birthcertno'])?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['homenumber']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homephoneno'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['homeaddress']?></td>
                               <td colspan="2">
                               		 <table>
										<tr>
											<td width="120px"><?=$kis_lang['Admission']['RMKG']['room']?></td>
											<td><?=$applicationInfo['AddressRoom']?></td>
										</tr>
										<tr>
											<td width="120px"><?=$kis_lang['Admission']['RMKG']['floor']?></td>
											<td><?=$applicationInfo['AddressFloor']?></td>
										</tr>
										<tr>
											<td><?=$kis_lang['Admission']['RMKG']['block']?></td>
											<td><?=$applicationInfo['AddressBlock']?></td>
										</tr>
										<tr>
											<td><?=$kis_lang['Admission']['RMKG']['bldg']?></td>
											<td><?=$applicationInfo['AddressBldg']?></td>
										</tr>
										<tr>
											<td><?=$kis_lang['Admission']['RMKG']['street']?></td>
											<td><?=$applicationInfo['AddressStreet']?></td>
										</tr>
										<tr>
											<td><?=$kis_lang['Admission']['RMKG']['district']?></td>
											<td><?=$lauc->getSelectedValueLang($applicationInfo['AddressDistrict'],$admission_cfg['addressdistrict'])?></td>
										</tr>
									</table>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['RMKG']['1stemail']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['Email'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['RMKG']['2ndemail']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['Email2'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['document']?></td>
                               <td colspan="2">
                               <?
                               		$attachmentAry = array();
	                               	foreach($attachmentList as $_type => $_attachmentAry){
	                               		if($_type != 'personal_photo'){
	                               			if($_attachmentAry['link']){
	                               				//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
	                               				$attachmentAry[] = '<a href="/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($_type).'" target="_blank">'.$_type.'</a>';
	                               			}
	                               		}
	                               	}
                               ?>
                               <?=count($attachmentAry)==0?'--':implode('<br/>',$attachmentAry);?>
                               </td>
                             </tr>
                           </tbody></table>
	<?}else if($sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td width="40%"><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo kis_ui::displayTableField($tempStuEngName[0])?></td>
                               <td width="30%" rowspan="7" width="145">
	                               <div id="studentphoto" class="student_info" style="margin:0px;">
	                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								   </div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo kis_ui::displayTableField($tempStuEngName[1])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo kis_ui::displayTableField($tempStuEngName[0])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo kis_ui::displayTableField($tempStuEngName[1])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['dateofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['gender']?></td>
                               <td><?=kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['gender']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td>
                               		<?=kis_ui::displayTableField($lauc->getSelectedValueLang($applicationInfo['placeofbirth'],$admission_cfg['placeofbirth']))?>
                               		<?=($applicationInfo['placeofbirth']==$admission_cfg['placeofbirth']['other'] ? '('.$applicationInfo['PlaceOfBirthOther'].')' : '')?>
                               	</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['nativeplace']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['province'].' '.$applicationInfo['county'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['religion']?></td>
                               <td colspan="2">
	                               <?=kis_ui::displayTableField($lauc->getSelectedValueLang($applicationInfo['religion1'],$admission_cfg['religion']))?>
	                               <?=($applicationInfo['religion1']==$admission_cfg['religion']['other'] ? '('.$applicationInfo['ReligionOther'].')' : '')?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['church']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['Church'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['IDtype']?></td>
                               <td>
	                               <?=kis_ui::displayTableField($lauc->getSelectedValueLang($applicationInfo['birthcerttype'],$admission_cfg['BirthCertType']))?>
	                               <?=($applicationInfo['birthcerttype']==$admission_cfg['BirthCertType']['other'] ? '('.$applicationInfo['birthcerttypeother'].')' : '')?><br />
	                               <?=kis_ui::displayTableField($applicationInfo['birthcertno'])?>
                               </td>
                             </tr>
                             <? $StuPhoneNoAry = explode('|||',$applicationInfo['homephoneno']);?>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['homenumber']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($StuPhoneNoAry[0])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['contactnumber']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($StuPhoneNoAry[1])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['homeaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Chi'].')'?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homeaddresschi'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['homeaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Eng'].')'?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homeaddress'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['contactaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Chi'].')'?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['ContactAddressChi'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['contactaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Eng'].')'?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['ContactAddress'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['document']?></td>
                               <td colspan="2">
                               <?
                               		$attachmentAry = array();
	                               	foreach($attachmentList as $_type => $_attachmentAry){
	                               		if($_type != 'personal_photo'){
	                               			if($_attachmentAry['link']){
	                               				//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
	                               				$attachmentAry[] = '<a href="/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($_type).'" target="_blank">'.$_type.'</a>';
	                               			}
	                               		}
	                               	}
                               ?>
                               <?=count($attachmentAry)==0?'--':implode('<br/>',$attachmentAry);?>
                               </td>
                             </tr>
                           </tbody></table>
	<?}else if($sys_custom['KIS_Admission']['MUNSANG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td width="40%"><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo kis_ui::displayTableField($tempStuEngName[0])?></td>
                               <td width="30%" rowspan="7" width="145">
	                               <div id="studentphoto" class="student_info" style="margin:0px;">
	                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								   </div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo kis_ui::displayTableField($tempStuEngName[1])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo kis_ui::displayTableField($tempStuEngName[0])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['englishname']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo kis_ui::displayTableField($tempStuEngName[1])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['dateofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['gender']?></td>
                               <td><?=kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['gender']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['placeofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['religion']?></td>
                               <? $religion = $libadmission->returnPresetCodeName("RELIGION", $applicationInfo['religion'])?>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['religion'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['munsang']['birthcertno']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['birthcertno'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['munsang']['phone']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homephoneno'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['homeaddress']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homeaddress'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['contactEmail']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['email'])?></td>
                             </tr>
                             <tr>
								<td class="field_title">
									<?= $kis_lang['Admission']['KTLMSKG']['twins'] ?>
								</td>
								<td>
									<?= ($applicationInfo['istwinsapplied']=='Y')? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'] ?>
								</td>
							</tr>
							<?php if($applicationInfo['istwinsapplied']){ ?>
							<tr>
								<td class="field_title">
									<?= $kis_lang['Admission']['munsang']['twinsID'] ?>
								</td>
								<td>
									<?= kis_ui::displayTableField($applicationInfo['twinsapplicationid']) ?>
								</td>
							</tr>
							<?php } ?>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['document']?></td>
                               <td colspan="2">
                               <?
                               		$attachmentAry = array();
	                               	foreach($attachmentList as $_type => $_attachmentAry){
	                               		if($_type != 'personal_photo'){
	                               			if($_attachmentAry['link']){
	                               				//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
	                               				$attachmentAry[] = '<a href="/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($_type).'" target="_blank">'.$_type.'</a>';
	                               			}
	                               		}
	                               	}
                               ?>
                               <?=count($attachmentAry)==0?'--':implode('<br/>',$attachmentAry);?>
                               </td>
                             </tr>
                           </tbody></table>
	<?}else if($sys_custom['KIS_Admission']['MGF']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['chinesename']?></td>
                               <td width="40%"><?=kis_ui::displayTableField($applicationInfo['student_name_b5'])?></td>
                               <td width="30%" rowspan="7" width="145">
	                               <div id="studentphoto" class="student_info" style="margin:0px;">
	                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								   </div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['englishname']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['student_name_en'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['gender']?></td>
                               <td><?=kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['gender']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['dateofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['Age']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['age'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['birthcertno']?></td>
                               <td> (<?=kis_ui::displayTableField($birth_cert_type_selection)?>) <?=kis_ui::displayTableField($applicationInfo['birthcertno'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['placeofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['nativeplace']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['province'])?>
                               		<!--<table style="width:auto;">
                               			<tr>
                               				<td>(<?=$kis_lang['Admission']['province']?>)</td>
                               				<td><?=kis_ui::displayTableField($applicationInfo['province'])?></td>
                               				<td>(<?=$kis_lang['Admission']['county']?>)</td>
                               				<td><?=kis_ui::displayTableField($applicationInfo['county'])?></td>
                               			</tr>
                               		</table>-->
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['phoneno']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homephoneno'])?></td>
                             </tr>
                              <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['homeaddress']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homeaddress'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['email']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['email'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['religion']?></td>
                               <? $religion = $libadmission->returnPresetCodeName("RELIGION", $applicationInfo['religion'])?>
                               <td colspan="2"><?=kis_ui::displayTableField($religion)?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['church']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['church'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['document']?></td>
                               <td colspan="2">
                               <?
                               		$attachmentAry = array();
	                               	foreach($attachmentList as $_type => $_attachmentAry){
	                               		if($_type != 'personal_photo'){
	                               			if($_attachmentAry['link']){
	                               				//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
	                               				$attachmentAry[] = '<a href="/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($_type).'" target="_blank">'.$_type.'</a>';
	                               			}
	                               		}
	                               	}
                               ?>
                               <?=count($attachmentAry)==0?'--':implode('<br/>',$attachmentAry);?>
                               </td>
                             </tr>
                           </tbody></table>
	<?}else if($sys_custom['KIS_Admission']['CSM']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['csm']['englishName']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td width="40%"><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo kis_ui::displayTableField($tempStuEngName[0])?></td>
                               <td width="30%" rowspan="7" width="145">
	                               <div id="studentphoto" class="student_info" style="margin:0px;">
	                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								   </div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['csm']['englishName']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_en']);echo kis_ui::displayTableField($tempStuEngName[1])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['surname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo kis_ui::displayTableField($tempStuEngName[0])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['chinesename']?> (<?=$kis_lang['Admission']['csm']['firstname_b5']?>)</td>
                               <td><?$tempStuEngName = explode(',',$applicationInfo['student_name_b5']);echo kis_ui::displayTableField($tempStuEngName[1])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['gender']?></td>
                               <td><?=kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['gender']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['dateofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['placeofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['csm']['birthcertno']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['birthcertno'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['csm']['homephoneno']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homephoneno'])?></td>
                             </tr>

                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['csm']['homeaddress']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField(is_numeric($applicationInfo['homeaddress'])?$kis_lang['Admission']['csm']['AddressLocation'][$applicationInfo['homeaddress']]:$applicationInfo['homeaddress'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['csm']['lastschool']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($kis_lang['Admission'][$applicationInfo['lastschool']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['csm']['isTwins']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField(($applicationInfo['IsTwins'] =='Y'?$kis_lang['Admission']['yes'].' ( '.$kis_lang['Admission']['csm']['isTwinsApplied'].' '.($applicationInfo['IsTwinsApplied'] =='Y'?$kis_lang['Admission']['yes2']:$kis_lang['Admission']['no2']).' )<br/>'.$kis_lang['Admission']['csm']['TwinsApplicationID'].': '.($applicationInfo['TwinsApplicationID']?$applicationInfo['TwinsApplicationID']:'--'):$kis_lang['Admission']['no']))?></td>
                             </tr>
                             <?if($applicationInfo['IsTwinsApplied'] =='Y'){?>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['csm']['TwinsApplication']?></td>
                               <td colspan="2"><?foreach($TwinListArray as $aTwins){
                               	echo str_replace(',',' ',Get_Lang_Selection($aTwins['ChineseName'],$aTwins['EnglishName'])).' ('.$aTwins['ApplicationID'].')<br/>';
                               }?></td>
                             </tr>
                             <?}?>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['csm']['CurBSName']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($kis_lang['Admission'][$applicationInfo['CurBSName']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['applyDayType']?></td>
                               <td>
                              	 	<table style="width:auto;">
                               			<tr>
                               				<?
                               					$idx = 1;
                               					for($i=1;$i<=count($kis_lang['Admission']['TimeSlot']);$i++):
                               						$_applyDayType = $otherApplicationInfo['ApplyDayType'.$i];
                               						if(!empty($_applyDayType)):
                               				?>
	                               					<td>(<?=$kis_lang['Admission']['Option']?> <?=$idx?>) <?=kis_ui::displayTableField($kis_lang['Admission']['csm']['TimeSlot'][$_applyDayType])?></td>
	                               					<?$idx++;?>
                               					<?endif;?>
                               				<?endfor;?>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['document']?></td>
                               <td colspan="2">
                               <?
                               		$attachmentAry = array();
	                               	foreach($attachmentList as $_type => $_attachmentAry){
	                               		if($_type != 'personal_photo'){
	                               			if($_attachmentAry['link']){
	                               				//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
	                               				$attachmentAry[] = '<a href="/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($_type).'" target="_blank">'.$_type.'</a>';
	                               			}
	                               		}
	                               	}
                               ?>
                               <?=count($attachmentAry)==0?'--':implode('<br/>',$attachmentAry);?>
                               </td>
                             </tr>
                           </tbody></table>
	<?}else if($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings']){?>
                      	    <table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['chinesename']?></td>
                               <td width="40%"><?=kis_ui::displayTableField($applicationInfo['student_name_b5'])?></td>
                               <td width="30%" rowspan="7" width="145">
	                               <div id="studentphoto" class="student_info" style="margin:0px;">
	                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								   </div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['englishname']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['student_name_en'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['dateofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['gender']?></td>
                               <td><?=kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['gender']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['birthcertno']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['birthcertno'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['placeofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['RMKG']['nationality']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['Nationality'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['homephoneno']?></td>
                               <td colspan="2">(852) <?=kis_ui::displayTableField($applicationInfo['homephoneno'])?></td>
                             </tr>
                             <!--tr>
                               <td class="field_title"><?=$kis_lang['Admission']['UCCKE']['fax']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['fax'])?></td>
                             </tr-->
                              <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['homeaddress']?></td>
                               <td colspan="3">
                				    <table>
                				    	<colgroup>
                				            <col style="width: 120px;">
                				            <col style="width: 100px;">
                				            <col style="width: 100px;">
                				            <col style="width: 100px;">
                				            <col style="width: 100px;">
                				            <col style="width: 150px;">
                				            <col>
                				        </colgroup>
                				        <tr>
                				            <td><?=$kis_lang['Admission']['MINGWAI']['room'] ?></td>
                				            <td><?=kis_ui::displayTableField($applicationInfo['AddressRoom'])?></td>
                				            <td style="text-align: right;"><?=$kis_lang['Admission']['MINGWAI']['floor'] ?></td>
                				            <td><?=kis_ui::displayTableField($applicationInfo['AddressFloor'])?></td>
                				            <td style="text-align: right;"><?=$kis_lang['Admission']['MINGWAI']['block'] ?></td>
                				            <td><?=kis_ui::displayTableField($applicationInfo['AddressBlock'])?></td>
                				            <td>&nbsp;</td>
                				        </tr>
                				        <tr>
                				            <td><?=$kis_lang['Admission']['MINGWAI']['building'] ?></td>
                				            <td colspan="6"><?=kis_ui::displayTableField($applicationInfo['AddressBldg'])?></td>
                				        </tr>
                				        <tr>
                				            <td><?=$kis_lang['Admission']['MINGWAI']['street'] ?></td>
                				            <td colspan="6"><?=kis_ui::displayTableField($applicationInfo['AddressStreet'])?></td>
                				        </tr>
                				        <tr>
                				            <td><?=$kis_lang['Admission']['MINGWAI']['district'] ?></td>
                				            <td colspan="6"><?=kis_ui::displayTableField($applicationInfo['AddressDistrict'])?></td>
            				            </tr>
            				            <tr>
                				            <td><?=$kis_lang['Admission']['MINGWAI']['region'] ?></td>
                				            <td colspan="6">
                    				            <?php
                        				            foreach($admission_cfg['addressdistrict'] as $key => $langVal){
                        				                if($langVal == $applicationInfo['AddressEstate']){
                        				                    break;
                        				                }
                        				            }
                        				            echo kis_ui::displayTableField($kis_lang['Admission']['CHIUCHUNKG'][ $key ]);
                    				            ?>
                				            </td>
                				        </tr>
                				    </table>
            				    </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['MINGWAI']['email']?></td>
                               <td colspan="2">
                               		<?php
                                   		$role = $kis_data['libadmission']->getApplicationCustInfo($applicationInfo['applicationID'], 'PrimaryEmailRole', 1);
                                   		$role = $role['Value'];
                                   		echo "({$kis_lang['Admission']['PG_Type'][$role]}) ";
                               		?>
                               		<?=kis_ui::displayTableField($applicationInfo['Email'])?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['MINGWAI']['email2']?></td>
                               <td colspan="2">
                               		<?php
                                   		$role = $kis_data['libadmission']->getApplicationCustInfo($applicationInfo['applicationID'], 'SecondaryEmailRole', 1);
                                   		$role = $role['Value'];
                                   		echo "({$kis_lang['Admission']['PG_Type'][$role]}) ";
                               		?>
                               		<?=kis_ui::displayTableField($applicationInfo['Email2'])?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['HKUGAPS']['SpokenLanguage']?></td>
                               <td colspan="2">
                               		<?php
                               		if(in_array($applicationInfo['LangSpokenAtHome'], $admission_cfg['lang'])){
                               		    foreach($admission_cfg['lang'] as $key => $langVal){
                               		        if($langVal == $applicationInfo['LangSpokenAtHome']){
                               		            break;
                               		        }
                               		    }
                               		    echo $kis_lang['Admission']['MINGWAI'][ $key ];
                               		}else{
                               		    echo kis_ui::displayTableField($applicationInfo['LangSpokenAtHome']);
                               		}
                               		?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['lastschool']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['LastSchool'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['KTLMSKG']['twins']?></td>
                               <td colspan="2"><?=($applicationInfo['IsTwinsApplied'] ? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['KTLMSKG']['twinsID']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['TwinsApplicationID'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['lastschoollevel']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['LastSchoolLevel'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['document']?></td>
                               <td colspan="2">
                               <?
                               		$attachmentAry = array();
	                               	foreach($attachmentList as $_type => $_attachmentAry){
	                               		if($_type != 'personal_photo'){
	                               			if($_attachmentAry['link']){
	                               				//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
	                               				$attachmentAry[] = '<a href="/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($_type).'" target="_blank">'.$_type.'</a>';
	                               			}
	                               		}
	                               	}
                               ?>
                               <?=count($attachmentAry)==0?'--':implode('<br/>',$attachmentAry);?>
                               </td>
                             </tr>
                           </tbody></table>
	<?}else if(file_exists(dirname(__FILE__) . "/{$setting_path_ip_rel}/studentinfo.php")){
		include(dirname(__FILE__) . "/{$setting_path_ip_rel}/studentinfo.php");
	}else{
	?>
                      	    <table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['chinesename']?></td>
                               <td width="40%"><?=kis_ui::displayTableField($applicationInfo['student_name_b5'])?></td>
                               <td width="30%" rowspan="7" width="145">
	                               <div id="studentphoto" class="student_info" style="margin:0px;">
	                               		<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
								   </div>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['englishname']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['student_name_en'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['dateofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['dateofbirth'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['gender']?></td>
                               <td><?=kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['gender']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['birthcertno']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['birthcertno'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['placeofbirth']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['placeofbirth'])?></td>
                             </tr>
							<!--
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['nativeplace']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               				<td>(<?=$kis_lang['Admission']['province']?>)</td>
                               				<td><?=kis_ui::displayTableField($applicationInfo['province'])?></td>
                               				<td>(<?=$kis_lang['Admission']['county']?>)</td>
                               				<td><?=kis_ui::displayTableField($applicationInfo['county'])?></td>
                               			</tr>
                               		</table>
                               </td>
                             </tr>
                             -->
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['langspokenathome']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homeLang'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['homephoneno']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homephoneno'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['contactperson']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['contactperson'])?></td>
                             </tr>
                              <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['contactpersonrelationship']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['contactpersonrelationship'])?></td>
                             </tr>
                              <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['homeaddress']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['homeaddress'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['email']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['email'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['religion']?></td>
                               <? $religion = $libadmission->returnPresetCodeName("RELIGION", $applicationInfo['religion'])?>
                               <td colspan="2"><?=kis_ui::displayTableField($religion)?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['church']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['church'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['lastschool']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['lastschool'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['lastschoollevel']?></td>
                               <td colspan="2"><?=kis_ui::displayTableField($applicationInfo['lastschoollevel'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['document']?></td>
                               <td colspan="2">
                               <?
                               		$attachmentAry = array();
	                               	foreach($attachmentList as $_type => $_attachmentAry){
	                               		if($_type != 'personal_photo'){
	                               			if($_attachmentAry['link']){
	                               				//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
	                               				$attachmentAry[] = '<a href="/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($_type).'" target="_blank">'.$_type.'</a>';
	                               			}
	                               		}
	                               	}
                               ?>
                               <?=count($attachmentAry)==0?'--':implode('<br/>',$attachmentAry);?>
                               </td>
                             </tr>
                           </tbody></table>
	<?}?>
<? elseif($display=='parentinfo'):?>
	<?if($sys_custom['KIS_Admission']['YLSYK']['Settings']){?>
  							<table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang):
                              ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                                	$parentAry['chinesename'][] = '<td class="form_guardian_field">';
                                	$parentAry['chinesename'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['parent_name_b5']);
                                	$parentAry['chinesename'][] = '</td>';

                                	$parentAry['englishname'][] = '<td class="form_guardian_field">';
                                	$parentAry['englishname'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['parent_name_EN']);
                                	$parentAry['englishname'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['occupation']);
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['mobile'][] = '<td class="form_guardian_field">';
                                	$parentAry['mobile'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['mobile']);
                                	$parentAry['mobile'][] = '</td>';

                                	$parentAry['worknumber'][] = '<td class="form_guardian_field">';
                                	$parentAry['worknumber'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['office_tel_no']);
                                	$parentAry['worknumber'][] = '</td>';

                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=$kis_lang['Admission']['YLSYK'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings']){?>
  							<table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang):
                              ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                                	$parentAry['name'][] = '<td class="form_guardian_field">';
                                	$parentAry['name'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['parent_name_b5']);
                                	$parentAry['name'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['occupation']);
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['mobile'][] = '<td class="form_guardian_field">';
                                	$parentAry['mobile'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['mobile']);
                                	$parentAry['mobile'][] = '</td>';

                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=$kis_lang['Admission']['munsang'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['RMKG']['Settings']){?>
  							<table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang): ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                               		$parentAry['name'][] = '<td class="form_guardian_field">';
                                	$parentAry['name'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['ChineseName']);
                                	$parentAry['name'][] = '</td>';

                                	$parentAry['nationality'][] = '<td class="form_guardian_field">';
                                	$parentAry['nationality'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['Nationality']);
                                	$parentAry['nationality'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['JobTitle']);
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['company'][] = '<td class="form_guardian_field">';
                               		$parentAry['company'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['Company']);
                               		$parentAry['company'][] = '</td>';

                               		$parentAry['workno'][] = '<td class="form_guardian_field">';
                               		$parentAry['workno'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['OfficeTelNo']);
                               		$parentAry['workno'][] = '</td>';

                                	$parentAry['mobile'][] = '<td class="form_guardian_field">';
                                	$parentAry['mobile'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['Mobile']);
                                	$parentAry['mobile'][] = '</td>';

                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=($kis_lang['Admission']['RMKG'][$_key]?$kis_lang['Admission']['RMKG'][$_key]:$kis_lang['Admission'][$_key])?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']){?>
  							<table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang): ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field

                                	$parentAry['name'][] = '<td class="form_guardian_field">';
                                	$parentAry['name'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['parent_name_b5']);
                                	$parentAry['name'][] = '</td>';

                                	$parentAry['gender'][] = '<td class="form_guardian_field">';
                                	$parentAry['gender'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['Gender']);
                                	$parentAry['gender'][] = '</td>';

                                	$parentAry['relationship'][] = '<td class="form_guardian_field">';
                                	$parentAry['relationship'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['Relationship']);
                                	$parentAry['relationship'][] = '</td>';

                                	$parentAry['company'][] = '<td class="form_guardian_field">';
                               		$parentAry['company'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['OfficeTelNo']);
                               		$parentAry['company'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['occupation']);
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['mobile'][] = '<td class="form_guardian_field">';
                                	$parentAry['mobile'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['mobile']);
                                	$parentAry['mobile'][] = '</td>';

                                	$parentAry['email'][] = '<td class="form_guardian_field">';
                               		$parentAry['email'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['email']);
                               		$parentAry['email'][] = '</td>';

                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=($kis_lang['Admission']['CHIUCHUNKG'][$_key]?$kis_lang['Admission']['CHIUCHUNKG'][$_key]:$kis_lang['Admission'][$_key])?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['MUNSANG']['Settings']){?>
  							<table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang):
                              	if($_pgKey == 'G') continue;
                              ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                                	$parentAry['chinesename'][] = '<td class="form_guardian_field">';
                                	$parentAry['chinesename'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['parent_name_b5']);
                                	$parentAry['chinesename'][] = '</td>';

									$parentAry['englishname'][] = '<td class="form_guardian_field">';
                               		$parentAry['englishname'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['parent_name_en']);
                               		$parentAry['englishname'][] = '</td>';
									
                                	$parentAry['company'][] = '<td class="form_guardian_field">';
                               		$parentAry['company'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['companyname']);
                               		$parentAry['company'][] = '</td>';

                                	$parentAry['address'][] = '<td class="form_guardian_field">';
                               		$parentAry['address'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['companyaddress']);
                               		$parentAry['address'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['occupation']);
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['mobile'][] = '<td class="form_guardian_field">';
                                	$parentAry['mobile'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['mobile']);
                                	$parentAry['mobile'][] = '</td>';

                                	$parentAry['email'][] = '<td class="form_guardian_field">';
                               		$parentAry['email'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['email']);
                               		$parentAry['email'][] = '</td>';

                               		$parentAry['levelofeducation'][] = '<td class="form_guardian_field">';
                               		$parentAry['levelofeducation'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['levelofeducation']);
                               		$parentAry['levelofeducation'][] = '</td>';

                               		$parentAry['nameofschool'][] = '<td class="form_guardian_field">';
                               		$parentAry['nameofschool'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['lastschool']);
                               		$parentAry['nameofschool'][] = '</td>';
                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=$kis_lang['Admission']['munsang'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
  	<?} else if($sys_custom['KIS_Admission']['MGF']['Settings']){?>
  							<table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang):
                              	if($_pgKey == 'G') continue;
                              ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                                	$parentAry['chinesename'][] = '<td class="form_guardian_field">';
                                	$parentAry['chinesename'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['parent_name_b5']);
                                	$parentAry['chinesename'][] = '</td>';

                                	$parentAry['hkid'][] = '<td class="form_guardian_field">';
                               		$parentAry['hkid'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['hkid']);
                               		$parentAry['hkid'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['occupation']);
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['mobile'][] = '<td class="form_guardian_field">';
                                	$parentAry['mobile'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['mobile']);
                                	$parentAry['mobile'][] = '</td>';

                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=$kis_lang['Admission']['mgf'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
	<?} else if($sys_custom['KIS_Admission']['CSM']['Settings']){?>
							<table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['csm']['relationshipBetweenChild']?></td>
                              <? $parentAry = array();?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang): ?>
                               <td class="form_guardian_head" width="28%"><input type="checkbox" onclick="return false" <?=($applicationInfo[$_pgKey]['relationship'] == $_pgKey?'checked':($_pgKey == 'G' && $applicationInfo['F']['relationship'] == 'G'?'checked':''))?> /><?=$_pgLang?><?=($_pgKey == 'G'?' ('.$applicationInfo[$_pgKey]['relationship'].')':'')?></td>
                               <? //Prepare TD field
                               		$parentAry['parantenglishname'][] = '<td class="form_guardian_field">';
                               		$parentAry['parantenglishname'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['parent_name_en']);
                               		$parentAry['parantenglishname'][] = '</td>';

                                	$parentAry['parantchinesename'][] = '<td class="form_guardian_field">';
                                	$parentAry['parantchinesename'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['parent_name_b5']);
                                	$parentAry['parantchinesename'][] = '</td>';

                               		$parentAry['parentlivewithchild'][] = '<td class="form_guardian_field">';
                                	if($_pgKey != 'G'){
                                		$parentAry['parentlivewithchild'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['IsLiveWithChild']=="Y"?$kis_lang['Admission']['yes']:($applicationInfo[$_pgKey]['IsLiveWithChild']?$kis_lang['Admission']['no']:''));
                                	}
                                	else
                                		$parentAry['parentlivewithchild'][] = '--';
                                	$parentAry['parentlivewithchild'][] = '</td>';



                                	$parentAry['mobile'][] = '<td class="form_guardian_field"><input type="checkbox" onclick="return false" '.($studentApplicationInfo['ContactPersonRelationship'] == $_pgKey?'checked':'').' /> ';
                                	$parentAry['mobile'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['mobile']);
                                	$parentAry['mobile'][] = '</td>';

                                	if($_pgKey == 'F'){
                                		$parentAry['email'][] = '<td class="form_guardian_field" colspan="3">';
	                                	$parentAry['email'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['email']);
	                                	$parentAry['email'][] = '</td>';

	                                	$parentAry['singleparent'][] = '<td class="form_guardian_field" colspan="3">';
	                                	$parentAry['singleparent'][] = $applicationInfo[$_pgKey]['lsSingleParents']=='Y'?$kis_lang['Admission']['yes']:($applicationInfo[$_pgKey]['lsSingleParents']=='N'?$kis_lang['Admission']['no']:$kis_lang['Admission']['na']);
										if($applicationInfo[$_pgKey]['lsSingleParents']=='Y')
											$parentAry['singleparent'][] ='<br/> ( '.$kis_lang['Admission']['csm']['IsApplyFullDayCare'].' '.($applicationInfo[$_pgKey]['IsApplyFullDayCare']=='Y'?$kis_lang['Admission']['agree']:$kis_lang['Admission']['disagree']).')';
	                                	$parentAry['singleparent'][] = '</td>';

//	                                	$parentAry['IsFamilySpecialCase'][] = '<td class="form_guardian_field" colspan="3">';
//	                                	$parentAry['IsFamilySpecialCase'][] = $applicationInfo[$_pgKey]['IsFamilySpecialCase']=='Y'?$kis_lang['Admission']['yes']:$kis_lang['Admission']['no'];
//										if($applicationInfo[$_pgKey]['IsFamilySpecialCase']=='Y')
//											$parentAry['IsFamilySpecialCase'][] =' ( '.$kis_lang['Admission']['csm']['IsApplyFullDayCare'].' '.($applicationInfo[$_pgKey]['IsApplyFullDayCare']=='Y'?$kis_lang['Admission']['yes']:$kis_lang['Admission']['no']).')';
//	                                	$parentAry['IsFamilySpecialCase'][] = '</td>';
                                	}
                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=$kis_lang['Admission']['csm'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
   	<?}else if($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings']){?>
                           <table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang): ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                                	$parentAry['chinesename'][] = '<td class="form_guardian_field">';
                                	$parentAry['chinesename'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['ChineseName']);
                                	$parentAry['chinesename'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['JobTitle']);
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['companyname'][] = '<td class="form_guardian_field">';
                                	$parentAry['companyname'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['Company']);
                                	$parentAry['companyname'][] = '</td>';

                                	$parentAry['phoneno'][] = '<td class="form_guardian_field">';
                                	$parentAry['phoneno'][] = '(852) ' . kis_ui::displayTableField($applicationInfo[$_pgKey]['Mobile']);
                                	$parentAry['phoneno'][] = '</td>';
                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=$kis_lang['Admission'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
   	<?}else if(file_exists(dirname(__FILE__) . "/{$setting_path_ip_rel}/parentinfo.php")){
		include(dirname(__FILE__) . "/{$setting_path_ip_rel}/parentinfo.php");
	}else{
	?>
                           <table class="form_table">
                             <tr>
                               <td>&nbsp;</td>
                              <? $parentAry = array();?>
                              <? foreach($kis_lang['Admission']['PG_Type'] as $_pgKey => $_pgLang): ?>
                               <td class="form_guardian_head" width="28%"><?=$_pgLang?></td>
                               <? //Prepare TD field
                                	$parentAry['chinesename'][] = '<td class="form_guardian_field">';
                                	$parentAry['chinesename'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['parent_name_b5']);
                                	$parentAry['chinesename'][] = '</td>';

                                	$parentAry['englishname'][] = '<td class="form_guardian_field">';
                               		$parentAry['englishname'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['parent_name_en']);
                               		$parentAry['englishname'][] = '</td>';

                               		$parentAry['relationship'][] = '<td class="form_guardian_field">';
                                	$parentAry['relationship'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['relationship']);
                                	$parentAry['relationship'][] = '</td>';

                                	$parentAry['occupation'][] = '<td class="form_guardian_field">';
                                	$parentAry['occupation'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['occupation']);
                                	$parentAry['occupation'][] = '</td>';

                                	$parentAry['companyname'][] = '<td class="form_guardian_field">';
                                	$parentAry['companyname'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['companyname']);
                                	$parentAry['companyname'][] = '</td>';

                                	$parentAry['jobposition'][] = '<td class="form_guardian_field">';
                                	$parentAry['jobposition'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['jobposition']);
                                	$parentAry['jobposition'][] = '</td>';

                                	$parentAry['companyaddress'][] = '<td class="form_guardian_field">';
                                	$parentAry['companyaddress'][] = kis_ui::displayTableField($applicationInfo[$_pgKey]['companyaddress']);
                                	$parentAry['companyaddress'][] = '</td>';

                                	$parentAry['phoneno'][] = '<td class="form_guardian_field">';
                                	$_phone = '<table style="width:auto;">';
                                	$_phone .= '<tr><td>'.$kis_lang['Admission']['office'].' : </td><td>'.kis_ui::displayTableField($applicationInfo[$_pgKey]['companyphone']).'</td></tr>';
                                	$_phone .= '<tr><td>'.$kis_lang['Admission']['mobile'].' : </td><td>'.kis_ui::displayTableField($applicationInfo[$_pgKey]['mobile']).'</td></tr>';
                                	$_phone .= '</table>';
                                	$parentAry['phoneno'][] =  $_phone;
                                	$parentAry['phoneno'][] = '</td>';
                               ?>
                              <? endforeach;?>
                             </tr>
                             <? foreach($parentAry as $_key => $_value): ?>
                             <tr>
                           		  <td class="field_title"><?=$kis_lang['Admission'][$_key]?></td>
                             	  <?=implode('',$_value);?>
                             </tr>
                             <? endforeach;?>
                           </table>
	<?}?>
<? elseif($display=='otherinfo'):?>
	<?if($sys_custom['KIS_Admission']['YLSYK']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=kis_ui::displayTableField($classLevelAry[$applicationInfo['classLevelID']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['applyDayType']?></td>
                               <td>
                              	 	<table style="width:auto;">
                               			<tr>
                               				<?
                               					$idx = 1;
                               					for($i=1;$i<=count($kis_lang['Admission']['TimeSlot']);$i++):
                               						$_applyDayType = $applicationInfo['ApplyDayType'.$i];
                               						if(!empty($_applyDayType)):
                               				?>
	                               					<td>(<?=$kis_lang['Admission']['Option']?> <?=$idx?>) <?=kis_ui::displayTableField($kis_lang['Admission']['csm']['TimeSlot'][$_applyDayType])?></td>
	                               					<?$idx++;?>
                               					<?endif;?>
                               				<?endfor;?>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['YLSYK']['BriefingApplyInfo']?></td>
                               <td width="70%"><?=kis_ui::displayTableField($applicationInfo['BriefingApplicationNo'])?></td>
                             </tr>
                           </tbody></table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['YLSYK']['RefBroSisInfo']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['name']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant']?></td>
                             </tr>
                             <?if(count($studentApplicationRelativesInfoCustRef) < 1){?>
                             <tr>
                             	<td class="field_title" colspan="4"><center><?=$kis_lang['norecord']?></center></td>
                             </tr>
                             <?}?>
                             <? for($i=0; $i< count($studentApplicationRelativesInfoCustRef);$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustRef[$i]['OthersRelativeStudiedName'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustRef[$i]['OthersRelativeRelationship'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['YLSYK']['ExBroSisInfo']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['name']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['TSUENWANBCKG']['graduateYear']?></td>
                             </tr>
                             <?if(count($studentApplicationRelativesInfoCustEx) < 1){?>
                             <tr>
                             	<td class="field_title" colspan="5"><center><?=$kis_lang['norecord']?></center></td>
                             </tr>
                             <?}?>
                             <? for($i=0; $i< count($studentApplicationRelativesInfoCustEx);$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustEx[$i]['OthersRelativeStudiedName'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustEx[$i]['OthersRelativeRelationship'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustEx[$i]['OthersRelativeStudiedYear'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['YLSYK']['CurBroSisInfo']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['name']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['YLSYK']['currentclass']?></td>
                             </tr>
                             <?if(count($studentApplicationRelativesInfoCustCur) < 1){?>
                             <tr>
                             	<td class="field_title" colspan="5"><center><?=$kis_lang['norecord']?></center></td>
                             </tr>
                             <?}?>
                             <? for($i=0; $i< count($studentApplicationRelativesInfoCustCur);$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustCur[$i]['OthersRelativeStudiedName'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustCur[$i]['OthersRelativeRelationship'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustCur[$i]['OthersRelativeStudiedYear'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['YLSYK']['BroSisApplyInfo']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['name']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant']?></td>
                               <td class="form_guardian_head" width="10%"><?=$kis_lang['Admission']['YLSYK']['ApplyClass']?></td>
                               <td class="form_guardian_head" width="10%"><?=$kis_lang['Admission']['YLSYK']['birthcertno']?></td>
                             </tr>
                             <?if(count($studentApplicationRelativesInfoCustApply) < 1){?>
                             <tr>
                             	<td class="field_title" colspan="5"><center><?=$kis_lang['norecord']?></center></td>
                             </tr>
                             <?}?>
                             <? for($i=0; $i< count($studentApplicationRelativesInfoCustApply);$i++):
                             	$OthersRelativeApplyClass = $libkis_admission->getClassLevel($studentApplicationRelativesInfoCustApply[$i]['OthersRelativeClassPosition']);
							 	$OthersRelativeApplyClass = $OthersRelativeApplyClass[$studentApplicationRelativesInfoCustApply[$i]['OthersRelativeClassPosition']];
                             ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustApply[$i]['OthersRelativeStudiedName'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustApply[$i]['OthersRelativeRelationship'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($OthersRelativeApplyClass)?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustApply[$i]['OthersRelativeBirthCertNo'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=kis_ui::displayTableField($classLevelAry[$applicationInfo['classLevelID']])?></td>
                             </tr>
                           </tbody></table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['TSUENWANBCKG']['currentBroSisInfo']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['name']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['TSUENWANBCKG']['currentClass']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['TSUENWANBCKG']['graduateYear']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['relationship']?></td>
                             </tr>
                             <?if(count($studentApplicationRelativesInfo) < 1){?>
                             <tr>
                             	<td class="field_title" colspan="5"><center><?=$kis_lang['norecord']?></center></td>
                             </tr>
                             <?}?>
                             <? for($i=0; $i< count($studentApplicationRelativesInfo);$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfo[$i]['OthersRelativeStudiedName'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfo[$i]['OthersRelativeClassPosition'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfo[$i]['OthersRelativeStudiedYear'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfo[$i]['OthersRelativeRelationship'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['RMKG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=kis_ui::displayTableField($classLevelAry[$applicationInfo['classLevelID']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['Wish']?></td>
                               <td>
                              	 	<table style="width:auto;">
                               			<tr>
                               				<?
                               					$idx = 1;
                               					for($i=1;$i<=count($kis_lang['Admission']['TimeSlot']);$i++):
                               						$_applyDayType = $applicationInfo['ApplyDayType'.$i];
                               						if(!empty($_applyDayType)):
                               				?>
	                               					<td>(<?=$kis_lang['Admission']['Option']?> <?=$idx?>) <?=kis_ui::displayTableField($kis_lang['Admission']['TimeSlot'][$_applyDayType])?></td>
	                               					<?$idx++;?>
                               					<?endif;?>
                               				<?endfor;?>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['munsang']['IsConsiderAlternative']?></td>
                               <td><?=kis_ui::displayTableField(($applicationInfo['OthersIsConsiderAlternative'] == 'Y'?$kis_lang['Admission']['yes']:$kis_lang['Admission']['no']))?></td>
                             </tr>
                           </tbody></table>

				            <table class="form_table">
							<tr>
									<td></td>
									<td class="form_guardian_head"><center><?=$kis_lang['Admission']['RMKG']['1stlang']?></center></td>
									<td class="form_guardian_head"><center><?=$kis_lang['Admission']['RMKG']['2ndlang']?></center></td>
									<td class="form_guardian_head"><center><?=$kis_lang['Admission']['RMKG']['3rdlang']?></center></td>
								</tr>
								<tr>
									<td class="field_title"><?=$kis_lang['Admission']['RMKG']['SpokenLang']?></td>
									<? $StuLangArr = explode('|||',$applicationInfo['NativeLanguage']); ?>
									<td class="form_guardian_field"><center><?=kis_ui::displayTableField($lauc->getSelectedValueLang($StuLangArr[0],$admission_cfg['lang']))?></center></td>
									<td class="form_guardian_field"><center><?=kis_ui::displayTableField($lauc->getSelectedValueLang($StuLangArr[1],$admission_cfg['lang']))?></center></td>
									<td class="form_guardian_field"><center><?=kis_ui::displayTableField($lauc->getSelectedValueLang($StuLangArr[2],$admission_cfg['lang']))?></center></td>
								</tr>
								<tr>
									<td class="field_title"><?=$kis_lang['Admission']['RMKG']['KinderU']?></td>
									<td><center><?=kis_ui::displayTableField(($applicationInfo['CustQ1'] == '1'?$kis_lang['Admission']['yes']:$kis_lang['Admission']['no']))?></center></td>
								</tr>
							</table>

                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['munsang']['RelativeStudiedWorkedAtCollege']?></td>
                               <td class="form_guardian_head" width="15%"><?=$kis_lang['Admission']['RMKG']['name']?></td>
                               <td class="form_guardian_head" width="15%"><?=$kis_lang['Admission']['RMKG']['age']?></td>
                               <td class="form_guardian_head" width="15%"><?=$kis_lang['Admission']['RMKG']['sex']?></td>
                               <td class="form_guardian_head" width="15%"><?=$kis_lang['Admission']['RMKG']['class']?></td>
                               <td class="form_guardian_head" width="15%"><?=$kis_lang['Admission']['RMKG']['schoolyear']?></td>
                             </tr>
                             <?if(count($studentApplicationRelativesInfo) < 1){ ?>
                             <tr>
                             	<td class="field_title" colspan="5"><center><?=$kis_lang['norecord']?></center></td>
                             </tr>
                             <?}?>

                             <? for($i=0; $i< count($studentApplicationRelativesInfo);$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfo[$i]['Name'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfo[$i]['Age'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfo[$i]['Gender'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($lauc->getPrevClassValueLang($studentApplicationRelativesInfo[$i]['ClassPosition']))?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfo[$i]['Year'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=kis_ui::displayTableField($classLevelAry[$applicationInfo['classLevelID']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['Wish']?></td>
                               <td>
                              	 	<table style="width:auto;">
                               			<tr>
                               				<?
                               					$idx = 1;
                               					for($i=1;$i<=count($kis_lang['Admission']['TimeSlot']);$i++):
                               						$_applyDayType = $applicationInfo['ApplyDayType'.$i];
                               						if(!empty($_applyDayType)):
                               				?>
	                               					<td>(<?=$kis_lang['Admission']['Option']?> <?=$idx?>) <?=kis_ui::displayTableField($kis_lang['Admission']['TimeSlot'][$_applyDayType])?></td>
	                               					<?$idx++;?>
                               					<?endif;?>
                               				<?endfor;?>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                           </tbody></table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['CHIUCHUNKG']['CurrentSchoolInfo']?></td>
                               <td class="form_guardian_head" width="28%"><?=$kis_lang['Admission']['munsang']['NameOfSchool']?></td>
                               <td class="form_guardian_head" width="26%"><?=$kis_lang['Admission']['munsang']['Class']?></td>
                               <td class="form_guardian_head" width="26%"><?=$kis_lang['Admission']['munsang']['address']?></td>
                             </tr>
                             <?if(count($studentApplicationInfoCust) < 1){?>
                             <tr>
                             	<td class="field_title" colspan="4"><center><?=$kis_lang['norecord']?></center></td>
                             </tr>
                             <?}?>
                             <? for($i=0; $i< count($studentApplicationInfoCust);$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationInfoCust[$i]['OthersPrevSchName'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationInfoCust[$i]['OthersPrevSchClass'].' - '.$lauc->getSelectedValueLang($studentApplicationInfoCust[$i]['ClassType'],$admission_cfg['classtype2']))?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationInfoCust[$i]['SchoolAddress'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
                           <table class="form_table">
                             <tr>
                               <td colspan="2"><?=$kis_lang['Admission']['CHIUCHUNKG']['Family']?></td>
                             </tr>
                             <tr><td width="30%" class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['B']?></td><td><?=$applicationInfo['EBrotherNo']?></td></tr>
                             <tr><td width="30%" class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['S']?></td><td><?=$applicationInfo['ESisterNo']?></td></tr>
                             <tr><td width="30%" class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['YB']?></td><td><?=$applicationInfo['YBrotherNo']?></td></tr>
                             <tr><td width="30%" class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['YS']?></td><td><?=$applicationInfo['YSisterNo']?></td></tr>
                             <tr><td width="30%" class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['RankInFamily']?></td><td><?=($applicationInfo['EBrotherNo']+$applicationInfo['ESisterNo']+1)?></td></tr>
                             <tr>
                             	<td width="30%" class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['Twins']?></td>
                             	<td>
                             		<?=($applicationStuInfo['IsTwins']==1?$kis_lang['yes']:$kis_lang['no'])?>
                             	</td>
                             </tr>
                             <tr>
                             	<td width="30%" class="field_title"><?=$kis_lang['Admission']['CHIUCHUNKG']['TwinsID']?></td>
                             	<td><?=$applicationStuInfo['TwinsIDNo']?></td>
                             </tr>
                           </table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['munsang']['RelativeStudiedWorkedAtCollege']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['munsang']['name']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['munsang']['RelationshipWithApplicant']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['munsang']['Year']?></td>
                             </tr>
                             <?if(count($studentApplicationRelativesInfo) < 1){?>
                             <tr>
                             	<td class="field_title" colspan="5"><center><?=$kis_lang['norecord']?></center></td>
                             </tr>
                             <?}?>

                             <? for($i=0; $i< count($studentApplicationRelativesInfo);$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfo[$i]['OthersRelativeStudiedName'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($lauc->getSelectedValueLang($studentApplicationRelativesInfo[$i]['OthersRelativeRelationship'],$admission_cfg['relation']))?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfo[$i]['OthersRelativeStudiedYear'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
	<?}else if($sys_custom['KIS_Admission']['MUNSANG']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=kis_ui::displayTableField($classLevelAry[$applicationInfo['classLevelID']])?></td>
                             </tr>
                             <!--<tr>
                               <td class="field_title"><?=$kis_lang['Admission']['applyDayType']?></td>
                               <td>
                              	 	<table style="width:auto;">
                               			<tr>
                               				<?
                               					$idx = 1;
                               					for($i=1;$i<=count($kis_lang['Admission']['TimeSlot']);$i++):
                               						$_applyDayType = $applicationInfo['ApplyDayType'.$i];
                               						if(!empty($_applyDayType)):
                               				?>
	                               					<td>(<?=$kis_lang['Admission']['Option']?> <?=$idx?>) <?=kis_ui::displayTableField($kis_lang['Admission']['TimeSlot'][$_applyDayType])?></td>
	                               					<?$idx++;?>
                               					<?endif;?>
                               				<?endfor;?>
                               			</tr>
                               		</table>
								</td>
                             </tr>-->
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['munsang']['applyDayType']?></td>
                               <td><?=kis_ui::displayTableField(($applicationInfo['ApplyDayType1'] == '1'?$kis_lang['Admission']['TimeSlot'][1]:$kis_lang['Admission']['TimeSlot'][2]))?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['munsang']['IsConsiderAlternative']?></td>
                               <td><?=kis_ui::displayTableField(($applicationInfo['OthersIsConsiderAlternative'] == 'Y'?$kis_lang['Admission']['yes']:$kis_lang['Admission']['no']))?></td>
                             </tr>
                           </tbody></table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['munsang']['PrevSchRecord']?></td>
                               <td class="form_guardian_head" width="26%"><?=$kis_lang['Admission']['munsang']['Year']?></td>
                               <td class="form_guardian_head" width="26%"><?=$kis_lang['Admission']['munsang']['Class']?></td>
                               <td class="form_guardian_head" width="28%"><?=$kis_lang['Admission']['munsang']['NameOfSchool']?></td>
                             </tr>
                             <?if(count($studentApplicationInfoCust) < 1){?>
                             <tr>
                             	<td class="field_title" colspan="4"><center><?=$kis_lang['norecord']?></center></td>
                             </tr>
                             <?}?>
                             <? for($i=0; $i< count($studentApplicationInfoCust);$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationInfoCust[$i]['OthersPrevSchYear'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationInfoCust[$i]['OthersPrevSchClass'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationInfoCust[$i]['OthersPrevSchName'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
                           <table class="form_table">
                             <tr>
                               <td><?=$kis_lang['Admission']['munsang']['RelativeStudiedWorkedAtCollege']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['munsang']['Year']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['munsang']['name']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['munsang']['ClassPosition']?></td>
                               <td class="form_guardian_head" width="20%"><?=$kis_lang['Admission']['munsang']['RelationshipWithApplicant']?></td>
                             </tr>
                             <?if(count($studentApplicationRelativesInfo) < 1){?>
                             <tr>
                             	<td class="field_title" colspan="5"><center><?=$kis_lang['norecord']?></center></td>
                             </tr>
                             <?}?>
                             <? for($i=0; $i< count($studentApplicationRelativesInfo);$i++): ?>
                             <tr>
                           		  <td class="field_title" style="text-align:right">(<?=($i+1)?>)</td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfo[$i]['OthersRelativeStudiedYear'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfo[$i]['OthersRelativeStudiedName'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfo[$i]['OthersRelativeClassPosition'])?></td>
                             	  <td class="form_guardian_field"><?=kis_ui::displayTableField($studentApplicationRelativesInfo[$i]['OthersRelativeRelationship'])?></td>
                             </tr>
                             <? endfor;?>
                           </table>
	<?} else if($sys_custom['KIS_Admission']['MGF']['Settings']){?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=kis_ui::displayTableField($classLevelAry[$applicationInfo['classLevelID']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['applyDayType']?></td>
                               <td>
                              	 	<table style="width:auto;">
                               			<tr>
                               				<?
                               					$idx = 1;
                               					for($i=1;$i<=count($kis_lang['Admission']['TimeSlot']);$i++):
                               						$_applyDayType = $applicationInfo['ApplyDayType'.$i];
                               						if(!empty($_applyDayType)):
                               				?>
	                               					<td>(<?=$kis_lang['Admission']['Option']?> <?=$idx?>) <?=kis_ui::displayTableField($kis_lang['Admission']['TimeSlot'][$_applyDayType])?></td>
	                               					<?$idx++;?>
                               					<?endif;?>
                               				<?endfor;?>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['curbsname']?></td>
                               <td>(<?=$kis_lang['Admission']['name']?>) <?=kis_ui::displayTableField($applicationInfo['CurBSName'])?> (<?=$kis_lang['Admission']['class']?>) <?=kis_ui::displayTableField($applicationInfo['CurBSLevel'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['hassiblingapplied']?></td>
                               <td>
                               	<?php
                               	$siblingHTML = '';
                               	if($studentApplicationInfo['IsTwinsApplied']){
                               	    $siblingHTML .= $kis_lang['Admission']['yes'];
                               	    $siblingHTML .= '&nbsp;&nbsp;&nbsp;(';
                               	    $siblingHTML .= "{$kis_lang['Admission']['name']}: {$applicationInfo['SiblingAppliedName']}, ";
                               	    $siblingHTML .= "{$kis_lang['Admission']['HKUGAPS']['twinsID']}: {$studentApplicationInfo['SiblingAppliedID']}";
                               	    $siblingHTML .= ')';
                               	}else{
                               	    $siblingHTML = $kis_lang['Admission']['no'];
                               	}
                           	    echo kis_ui::displayTableField($siblingHTML);
                               	?>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['issiblinggradhere']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               				<td><?=kis_ui::displayTableField($applicationInfo['ExBSName']?$kis_lang['Admission']['yes']:$kis_lang['Admission']['no'])?></td>
                               				<td><?=$applicationInfo['ExBSName3']?'3 '.$kis_lang['Admission']['NumOfPerson']:($applicationInfo['ExBSName2']?'2 '.$kis_lang['Admission']['NumOfPerson']:($applicationInfo['ExBSName']?'1 '.$kis_lang['Admission']['NumOfPerson']:''))?></td>
                               			</tr>
                               			<tr>
                               				<td colspan="2">
                               					<?=$applicationInfo['ExBSName']?$kis_lang['Admission']['name'].': '.$applicationInfo['ExBSName'].' ('.$kis_lang['Admission']['GradYear'].'：'.$applicationInfo['ExBSGradYear'].')<br/>':''?>
                               					<?=$applicationInfo['ExBSName2']?$kis_lang['Admission']['name'].': '.$applicationInfo['ExBSName2'].' ('.$kis_lang['Admission']['GradYear'].'：'.$applicationInfo['ExBSGradYear2'].')<br/>':''?>
                               					<?=$applicationInfo['ExBSName3']?$kis_lang['Admission']['name'].': '.$applicationInfo['ExBSName3'].' ('.$kis_lang['Admission']['GradYear'].'：'.$applicationInfo['ExBSGradYear3'].')':''?>
                               				</td>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['teacherOrManagerChildren']?></td>
                               <td><?=kis_ui::displayTableField( ($applicationInfo['TeacherManagerChildren'] == 'yes')? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'] )?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['mgf']['lastschool']?></td>
                               <td><?=kis_ui::displayTableField($studentApplicationInfo['lastschool'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['remarks']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['Remarks'])?></td>
                             </tr>
                           </tbody></table>
    <?}else if($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings']){?>
                           <table class="form_table">
                             <tbody>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['MINGWAI']['dateOfEntry']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['ApplyStartDay'])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['applyTerm']?></td>
                           		<?php if($sys_custom['KIS_Admission']['MINGWAI']['Settings']): ?>
                                   <td>
                                   		<table style="width:auto;">
                                   			<tr>
                                   				<td>(<?=$kis_lang['Admission']['Option']?> 1) <?=kis_ui::displayTableField($applicationInfo['ApplyProgram1']== 1?'International Stream PN-K3 國際課程 PN-K3':($applicationInfo['ApplyProgram1']== 2?'Local Stream PN / Cambridge Stream K1 - K3 本地課程 PN / 劍橋英語課程 K1 - K3':''))?></td>
                                   				<td>(<?=$kis_lang['Admission']['Option']?> 2) <?=kis_ui::displayTableField($applicationInfo['ApplyProgram2']== 0?'Not Applicable 不適用':($applicationInfo['ApplyProgram2']== 1?'International Stream PN-K3 國際課程 PN-K3':($applicationInfo['ApplyProgram2']== 2?'Local Stream PN / Cambridge Stream K1 - K3 本地課程 PN / 劍橋英語課程 K1 - K3':'')))?></td>
                                   			</tr>
                                   		</table>
                                   	</td>
                           		<?php else: ?>
                           			<td><?=kis_ui::displayTableField('International Stream 國際課程')?></td>
                           		<?php endif; ?>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['applyDayType']?></td>
                               <td>
                              	 	<table style="width:auto;">
                               			<tr>
                               				<?
                               					$idx = 1;
                               					for($i=1;$i<=count($kis_lang['Admission']['TimeSlot']);$i++):
                               						$_applyDayType = $applicationInfo['ApplyDayType'.$i];
                               						if(!empty($_applyDayType)):
                               				?>
	                               					<td>(<?=$kis_lang['Admission']['Option']?> <?=$idx?>) <?=kis_ui::displayTableField($kis_lang['Admission']['TimeSlot'][$_applyDayType])?></td>
	                               					<?$idx++;?>
                               					<?endif;?>
                               				<?endfor;?>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=kis_ui::displayTableField($classLevelAry[$applicationInfo['classLevelID']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['ExBSName']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               				<td style="width: 140px;"><?=$kis_lang['Admission']['name'] ?>:</td>
                               				<td><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustEx[0]['Name'])?></td>
                               			</tr>
                               			<tr>
                               				<td><?=$kis_lang['Admission']['yearOfGraduation'] ?>:</td>
                               				<td><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustEx[0]['Year'])?></td>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CurBSName']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               				<td style="width: 140px;"><?=$kis_lang['Admission']['name'] ?>:</td>
                               				<td><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustCur[0]['Name'])?></td>
                               			</tr>
                               			<tr>
                               				<td><?=$kis_lang['Admission']['class'] ?>:</td>
                               				<td><?=kis_ui::displayTableField($studentApplicationRelativesInfoCustCur[0]['ClassPosition'])?></td>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                           </tbody></table>
	<?
	}else if(file_exists(dirname(__FILE__) . "/{$setting_path_ip_rel}/otherinfo.php")){
		include(dirname(__FILE__) . "/{$setting_path_ip_rel}/otherinfo.php");
	}else{
	?>
                           <table class="form_table">
                             <tbody>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['dateOfEntry']?></td>
                               <td>
									<table style="width:auto;">
                               			<tr>
                               				<td>(<?=$kis_lang['year']?>)</td>
                               				<td><?=kis_ui::displayTableField($schoolYear)?></td>
                               				<td>(<?=$kis_lang['month']?>)</td>
                               				<td><?=kis_ui::displayTableField($applicationInfo['month'])?></td>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['applyTerm']?></td>
                               <td><?=kis_ui::displayTableField($kis_lang['Admission']['Term'][$applicationInfo['term']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['applyDayType']?></td>
                               <td>
                              	 	<table style="width:auto;">
                               			<tr>
                               				<?
                               					$idx = 1;
                               					for($i=1;$i<=count($kis_lang['Admission']['TimeSlot']);$i++):
                               						$_applyDayType = $applicationInfo['ApplyDayType'.$i];
                               						if(!empty($_applyDayType)):
                               				?>
	                               					<td>(<?=$kis_lang['Admission']['Option']?> <?=$idx?>) <?=kis_ui::displayTableField($kis_lang['Admission']['TimeSlot'][$_applyDayType])?></td>
	                               					<?$idx++;?>
                               					<?endif;?>
                               				<?endfor;?>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['form']?></td>
                               <td width="70%"><?=kis_ui::displayTableField($classLevelAry[$applicationInfo['classLevelID']])?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['familyStatus']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               				<td>(<?=$kis_lang['Admission']['elderBrother']?>)</td>
                               				<td><?=kis_ui::displayTableField($applicationInfo['elder_brother'])?></td>
                               				<td>(<?=$kis_lang['Admission']['elderSister']?>)</td>
                               				<td><?=kis_ui::displayTableField($applicationInfo['elder_sister'])?></td>
                               				<td>(<?=$kis_lang['Admission']['youngerBrother']?>)</td>
                               				<td><?=kis_ui::displayTableField($applicationInfo['younger_brother'])?></td>
                               				<td>(<?=$kis_lang['Admission']['youngerSister']?>)</td>
                               				<td><?=kis_ui::displayTableField($applicationInfo['younger_sister'])?></td>
                               			</tr>
                               		</table>
                               </td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['ExBSName']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               				<td>(<?=$kis_lang['Admission']['name']?>)</td>
                               				<td><?=kis_ui::displayTableField($applicationInfo['ExBSName'])?></td>
                               				<td>(<?=$kis_lang['form']?>)</td>
                               				<td><?=kis_ui::displayTableField($applicationInfo['ExBSLevel'])?></td>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['CurBSName']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               				<td>(<?=$kis_lang['Admission']['name']?>)</td>
                               				<td><?=kis_ui::displayTableField($applicationInfo['CurBSName'])?></td>
                               				<td>(<?=$kis_lang['form']?>)</td>
                               				<td><?=kis_ui::displayTableField($applicationInfo['CurBSLevel'])?></td>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                              <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['needSchoolBus']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               				<td><?=kis_ui::displayTableField($kis_lang[$applicationInfo['needschoolbus']])?></td>
                               			<?if($applicationInfo['needschoolbus']=='yes'):?>
                               				<td>(<?=$kis_lang['Admission']['placeForTakingSchoolBus']?>) <?=kis_ui::displayTableField($applicationInfo['SchoolBusPlace'])?></td>
                               			<?endif;?>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                              <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['knowUsBy']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<tr>
                               				<td><?=kis_ui::displayTableField($kis_lang['Admission'][$applicationInfo['knowusby']])?></td>
                               			<?if(!empty($applicationInfo['knowusby'])):?>
                               				<td><?=$applicationInfo['KnowUsByOther']?'('.$applicationInfo['KnowUsByOther'].')':''?></td>
                               			<?endif;?>
                               			</tr>
                               		</table>
								</td>
                             </tr>
                           </tbody></table>
	<?}?>
<? elseif($display=='remarks'):?>
	<?php
	if(file_exists(dirname(__FILE__) . "/{$setting_path_ip_rel}/remarks.php")){
		include(dirname(__FILE__) . "/{$setting_path_ip_rel}/remarks.php");
	}else{?>
							<table class="form_table">
                             <tbody>
                             <tr>
                               <td width="30%" class="field_title"><?=$kis_lang['Admission']['applicationstatus']?></td>
                               <td width="70%"><?=kis_ui::displayTableField($kis_lang['Admission']['Status'][$applicationInfo['status']])?></td>
                             </tr>
                             <?php if($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings']){?>
                                 <tr>
                                   <td width="30%" class="field_title"><?=$kis_lang['Admission']['MINGWAI']['admissionMethod']?></td>
                                   <td width="70%">
                                   		<?php
                                   		$admissionMethod = $kis_data['libadmission']->getApplicationCustInfo($applicationInfo['applicationID'], 'AdmissionMethod', 1);
                                   		$admissionMethod = $admissionMethod['Value'];
                                   		?>
                                   		<?=kis_ui::displayTableField($kis_lang['Admission']['MINGWAI']['admissionMethodType'][$admissionMethod])?>

                                   		<?php
                                   		$mailCode = $kis_data['libadmission']->getApplicationCustInfo($applicationInfo['applicationID'], 'MailCode', 1);
                                   		$mailCode = $mailCode['Value'];
                                   		if($mailCode):
                                   		?>
                                       		(
                                           		<?=$kis_lang['Admission']['MINGWAI']['MailCode'] ?>:
                                           		<?=kis_ui::displayTableField($mailCode)?>
                                       		)
                                   		<?php
                                   		endif;
                                   		?>
                                   </td>
                                 </tr>
                             <?php } ?>
                             <?if(!$sys_custom['KIS_Admission']['ICMS']['Settings']){?>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['applicationfee']?></td>
                               <td>
                               		<table style="width:auto;">
                               			<?if($paypalPaymentInfo[0]['payment_status']){?>
										<tr>
											<td colspan="4"><font color="green"><?=$kis_lang['Admission']['KTLMSKG']['paidByPayPal']?></font><br/>
											<?foreach($paypalPaymentInfo as $apaypalPaymentInfo){?>
											(<?=$kis_lang['Admission']['PaymentsStatus']?>) <?=$apaypalPaymentInfo['payment_status']?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PaymentItem']?>) <?=$apaypalPaymentInfo['item_name']?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PaymentTotal']?>) <?=$apaypalPaymentInfo['mc_currency']?> <?=$apaypalPaymentInfo['mc_gross']?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PaymentDate']?>) <?=date('Y-m-d H:i:s', strtotime($apaypalPaymentInfo['payment_date']))?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PayerEmail']?>) <?=$apaypalPaymentInfo['payer_email']?><br/><br/>
											<?}?>
											</td>
										</tr>
										<tr>
											<td colspan="4">&nbsp;</td>
										</tr>
										<?}?>
										<?if($alipayPaymentInfo[0]['RecordStatus']){?>
										<tr>
											<td colspan="4"><font color="green"><?=$kis_lang['Admission']['paidByAlipayHK']?></font><br/>
											<?foreach($alipayPaymentInfo as $aAlipayPaymentInfo){
												if($aAlipayPaymentInfo['RecordStatus']==1)	{
											?>
											(<?=$kis_lang['Admission']['PaymentsStatus']?>) <?=$aAlipayPaymentInfo['RecordStatus']==1?'Success':''?> <br/>(<?=$kis_lang['Admission']['TradeNo']?>) <?=$aAlipayPaymentInfo['TradeNo']?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PaymentTotal']?>) <?=$aAlipayPaymentInfo['Amount']?> <br/>(<?=$kis_lang['Admission']['KTLMSKG']['PaymentDate']?>) <?=date('Y-m-d H:i:s', strtotime($aAlipayPaymentInfo['DateModified']))?><br/><br/>
											<?	}
											}?>
											</td>
										</tr>
										<tr>
											<td colspan="4">&nbsp;</td>
										</tr>
										<?}?>
                               			<tr>
                               				<td>(<?=$kis_lang['Admission']['receiptcode']?>)</td>
                               				<td colspan="3"><?=kis_ui::displayTableField($applicationInfo['receiptID'])?></td>
                               			</tr>
                               			<tr>
                               				<td width="20%">(<?=$kis_lang['date']?>)</td>
                               				<td width="30%"><?=kis_ui::displayTableField($applicationInfo['receiptdate'])?></td>
                               				<td width="20%">(<?=$kis_lang['Admission']['handler']?>)</td>
                               				<td width="30%"><?=kis_ui::displayTableField($applicationInfo['handler'])?></td>
                               			</tr>
                               		</table>
                               	</td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['receiptcopy']?></td>
                               <td>
                               <?
                               		$attachmentAry = array();
	                               	foreach($attachmentList as $_type => $_attachmentAry){
	                               		if($_type == 'receipt_copy'){
	                               			if($_attachmentAry['link']){
	                               				//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
	                               				$attachmentAry[] = '<a href="/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($_type).'" target="_blank">'.$kis_lang['view'].'</a>';
	                               			}
	                               		}
	                               	}
                               ?>
                               <?=count($attachmentAry)==0?'--':implode('<br/>',$attachmentAry);?>
                               </td>
                             </tr>
                             <?if(!$sys_custom['KIS_Admission']['InterviewSettings']){?>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['interviewdate']?></td>
                               <td><?=kis_ui::displayTableField(substr($applicationInfo['interviewdate'], 0, -3))?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['interviewlocation']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['interviewlocation'])?></td>
                             </tr>
                             <?}?>
                             <?if($sys_custom['KIS_Admission']['InterviewSettings']){?>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['interviewdate']?></td>
                               <td>
                               	(1) <?=kis_ui::displayTableField($interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'--')?><br/>
                               	(2) <?=kis_ui::displayTableField($interviewInfo2?$interviewInfo2['Date'].' ('.substr($interviewInfo2['StartTime'], 0, -3).' ~ '.substr($interviewInfo2['EndTime'], 0, -3).')'.($interviewInfo2['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo2['GroupName']:''):'--')?><br/>
                               	(3) <?=kis_ui::displayTableField($interviewInfo3?$interviewInfo3['Date'].' ('.substr($interviewInfo3['StartTime'], 0, -3).' ~ '.substr($interviewInfo3['EndTime'], 0, -3).')'.($interviewInfo3['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo3['GroupName']:''):'--')?>
                               </td>
                             </tr>
                             <?}?>
                            <?if($sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings']){ ?>
                            	<?php
                            	    $libkis_admission_briefing = new admission_briefing_base();
                            	    $briefing = $libkis_admission_briefing->getBriefingApplicantByApplicantID($applicationInfo['applicationID']);
                        	        $briefingSession = $libkis_admission_briefing->getBriefingSession($briefing['BriefingID']);
                        	        
                        	        if($briefingSession['BriefingStartDate'] == "" && $briefingSession['BriefingEndDate'] == "")
                        	            $briefingDateTime = "--";
                        	        else
                        	           $briefingDateTime = "(".$briefingSession['BriefingStartDate'] . " ~ " . substr($briefingSession['BriefingEndDate'], 11, strlen($briefingSession['BriefingEndDate'])).") ";
                            	?>
                              <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['briefing']?></td>
                               <td>
                               		<?=kis_ui::displayTableField($briefingSession['Title'].$briefingDateTime)?>
                               		<?php
                               		if($briefingSession['Title']){
                               		    echo "({$kis_lang['Admission']['requestSeat']}: {$briefing['SeatRequest']})";
                               		}
                               		?>
                               </td>
                             </tr>
                              <!--tr>
                               <td class="field_title"><?=$kis_lang['Admission']['briefingdate']?></td>
                               <td><?=kis_ui::displayTableField(substr($applicationInfo['briefingdate'], 0, -3))?></td>
                             </tr>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['briefinginformation']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['briefinginfo'])?></td>
                             </tr-->

                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['MINGWAI']['fee']?></td>
                               <td>
                               		<?php
                                   		$fee = $kis_data['libadmission']->getApplicationCustInfo($applicationInfo['applicationID'], 'Fee', 1);
                                   		$fees = explode(';', $fee['Value']);
                                   		$feeHTML = '';
                                   		foreach($admission_cfg['fee'] as $feeType){
                                   		    if(in_array($feeType, $fees)){
                                   		        $feeHTML .= "{$kis_lang['Admission']['MINGWAI']['feeType'][$feeType]}, ";
                                   		    }
                                   		}
                                   		$feeHTML = trim($feeHTML, ', ');
                                   		echo kis_ui::displayTableField($feeHTML);
                               		?>
							   </td>
                             </tr>

                             <tr>
                               <td class="field_title"><?=$kis_lang['emailchecked']?></td>
                               <td>
                               		<?php
                               		$checkedDate = $lauc->getStudentEmailChecked($applicationInfo['applicationID']);
                               		echo kis_ui::displayTableField($checkedDate);
                               		?>
							   </td>
                             </tr>

                             <?} ?>
                              <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['isnotified']?></td>
                               <td><?=$kis_lang[$applicationInfo['isnotified']]?></td>
                             </tr>
                             <?}?>
                             <?if($sys_custom['KIS_Admission']['ICMS']['Settings']){?>
                             	  <tr>
	                               <td class="field_title"><?=$kis_lang['Admission']['interviewdate']?></td>
	                               <td><?=kis_ui::displayTableField(substr($applicationInfo['interviewdate'], 0, -3))?></td>
	                             </tr>
	                             <tr>
	                               <td class="field_title"><?=$kis_lang['Admission']['interviewlocation']?></td>
	                               <td><?=kis_ui::displayTableField($applicationInfo['interviewlocation'])?></td>
	                             </tr>
                             <?}?>
                             <tr>
                               <td class="field_title"><?=$kis_lang['Admission']['otherremarks']?></td>
                               <td><?=kis_ui::displayTableField($applicationInfo['remark'])?></td>
                             </tr>
                           </tbody></table>
   <?php } ?>
<? endif; ?>
          <p class="spacer"></p><br>
        </div>
    <? else: ?>
   	 <? kis_ui::loadNoRecord() ?>
    <? endif; ?>
</div>