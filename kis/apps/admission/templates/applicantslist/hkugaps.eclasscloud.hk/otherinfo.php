<?php

global $libkis_admission;

#### Get Student Cust Info START ####
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
#### Get Student Cust Info START ####


#### Get Relatives Info START ####
$siblingsInfo = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
$siblingsInfoArr = array();
foreach($siblingsInfo as $siblings){
	foreach($siblings as $para=>$info){
		$siblingsInfoArr[$siblings['type']][$para] = $info;
	}
}#### Get Relatives Info END ####

?>
<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>
<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['HKUGAPS']['memberOfHKUGA']?>
	</td>
	<td colspan="3">
		<?php
		    if($applicationCustInfo['HKUGA_Member'][0]['Value'] == "1"){
		        echo "{$kis_lang['Admission']['yes']}<br/>{$kis_lang['Admission']['HKUGAPS']['memberName']} Member's Name: {$applicationCustInfo['HKUGA_Member_Name'][0]['Value']}<br/>{$kis_lang['Admission']['HKUGAPS']['memberNo']} Membership No.: {$applicationCustInfo['HKUGA_Member_No'][0]['Value']}";
		    }else{
		        echo "{$kis_lang['Admission']['no']}";
		    }?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['HKUGAPS']['memberOfHKUGAFoundation']?>
	</td>
	<td colspan="3">
		<?php
		    if($applicationCustInfo['HKUGA_Foundation_Member'][0]['Value']){
		        echo "{$kis_lang['Admission']['yes']}<br/>{$kis_lang['Admission']['HKUGAPS']['memberName']} Member's Name: {$applicationCustInfo['HKUGA_Foundation_Member_Name'][0]['Value']}";
		    }else{
		        echo "{$kis_lang['Admission']['no']}";
		    }
		 ?>

	</td>
</tr>
</table>

<table id="dataTable1" class="form_table" >
	<tr>
		<td rowspan="3" class="field_title">
			<?=$kis_lang['Admission']['HKUGAPS']['siblingInformation'] ?>
		</td>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['nameChi'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['nameEng'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['currentClass']?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['graduationYear'] ?></center></td>
	</tr>
	<tr>
		<td class="field_title" style="text-align:right;width:30px;">(1)</td>
		<td class="form_guardian_field">
			<?=kis_ui::displayTableField($siblingsInfoArr[1]['name']) ?>
		</td>
		<td class="form_guardian_field">
			<?=kis_ui::displayTableField($siblingsInfoArr[1]['englishName']) ?>
		</td>
		<td class="form_guardian_field">
			<?=kis_ui::displayTableField($siblingsInfoArr[1]['classposition']) ?>
		</td>
		<td class="form_guardian_field">
			<?=kis_ui::displayTableField($siblingsInfoArr[1]['year']) ?>
		</td>
	</tr>
	<tr>
		<td class="field_title" style="text-align:right;width:30px;">(2)</td>
		<td class="form_guardian_field">
			<?=kis_ui::displayTableField($siblingsInfoArr[2]['name']) ?>
		</td>
		<td class="form_guardian_field">
			<?=kis_ui::displayTableField($siblingsInfoArr[2]['englishName']) ?>
		</td>
		<td class="form_guardian_field">
			<?=kis_ui::displayTableField($siblingsInfoArr[2]['classposition']) ?>
		</td>
		<td class="form_guardian_field">
			<?=kis_ui::displayTableField($siblingsInfoArr[2]['year']) ?>
		</td>
	</tr>
</table>

<table id="dataTable1" class="form_table" style="font-size: 13px">
	<colgroup>
    	<col width="30%">
    	<col width="auto">
    	<col width="13%">
    	<col width="13%">
    	<col width="10%">
    	<col width="9%">
    	<col width="20%">
    	<col width="5%">
	</colgroup>
	<tr>
		<td rowspan="3" class="field_title">
			<?=$kis_lang['Admission']['HKUGAPS']['siblingOther'] ?>
		</td>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['nameChi'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['nameEng'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['dateofbirth']?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['twins'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['school'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['grades'] ?></center></td>
	</tr>
	<tr>
		<td class="field_title" style="text-align:right;width:50px;">(1)</td>
		<td class="form_guardian_field">
			<?=kis_ui::displayTableField($siblingsInfoArr[3]['name']) ?>
		</td>
		<td class="form_guardian_field">
			<?=kis_ui::displayTableField($siblingsInfoArr[3]['englishName']) ?>
		</td>
		<td class="form_guardian_field">
    		<?=kis_ui::displayTableField($siblingsInfoArr[3]['dob']) ?>
		</td>
		<td class="form_guardian_field">
    		<?=kis_ui::displayTableField($siblingsInfoArr[3]['istwins']=="1"? $kis_lang['Admission']['yes']:($siblingsInfoArr[3]['istwins']=="0"? $kis_lang['Admission']['no']:"")) ?>
		</td>
		<td class="form_guardian_field">
    		<?=kis_ui::displayTableField($siblingsInfoArr[3]['schoolName']) ?>
		</td>
		<td class="form_guardian_field">
    		<?=kis_ui::displayTableField($siblingsInfoArr[3]['classposition']) ?>
		</td>
	</tr>
	<tr>
		<td class="field_title" style="text-align:right;width:50px;">(2)</td>
<td class="form_guardian_field">
			<?=kis_ui::displayTableField($siblingsInfoArr[4]['name']) ?>
		</td>
		<td class="form_guardian_field">
			<?=kis_ui::displayTableField($siblingsInfoArr[4]['englishName']) ?>
		</td>
		<td class="form_guardian_field">
    		<?=kis_ui::displayTableField($siblingsInfoArr[4]['dob']) ?>
		</td>
		<td class="form_guardian_field">
    		<?=kis_ui::displayTableField($siblingsInfoArr[4]['istwins']=="1"? $kis_lang['Admission']['yes']:($siblingsInfoArr[4]['istwins']=="0"? $kis_lang['Admission']['no']:"")) ?>
		</td>
		<td class="form_guardian_field">
    		<?=kis_ui::displayTableField($siblingsInfoArr[4]['schoolName']) ?>
		</td>
		<td class="form_guardian_field">
    		<?=kis_ui::displayTableField($siblingsInfoArr[4]['classposition']) ?>
		</td>
	</tr>
</table>
