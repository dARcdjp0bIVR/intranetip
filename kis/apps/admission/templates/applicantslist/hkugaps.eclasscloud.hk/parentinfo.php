<?php

global $libkis_admission;

######## Get Parent Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
######## Get Parent Cust Info END ########
?>
<table class="form_table">
	<colgroup>
        <col style="width:30%">
        <col style="width:35%">
        <col style="width:35%">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['father'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['mother'] ?></center></td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAPS']['nameChi'] ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['F']['chinesename']) ?>
    	</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['M']['chinesename']) ?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAPS']['nameEng'] ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['F']['englishname']) ?>	
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['M']['englishname']) ?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAPS']['contactNum']?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['F']['mobile']) ?>
		</td>
		<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($applicationInfo['M']['mobile']) ?>
    	</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAPS']['email']?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['F']['email']) ?>
    	</td>
		<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($applicationInfo['M']['email']) ?>
    	</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['occupation']?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['F']['occupation']) ?>
    	</td>
		<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($applicationInfo['M']['occupation']) ?>
    	</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAPS']['nameInstitution']?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['F']['companyname']) ?>
    	</td>
		<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($applicationInfo['M']['companyname']) ?>
    	</td>
	</tr>
</table>


<table class="form_table" >
<tr>
	<td class="field_title"  style="width:30%">
		<?=$kis_lang['Admission']['HKUGAPS']['guardianName']?>
	</td>
	<td>
		<?= kis_ui::displayTableField($applicationInfo['G']['chinesename']) ?>
	</td>
</tr>
<tr>
	<td class="field_title"  style="width:30%">
		<?=$kis_lang['Admission']['HKUGAPS']['guardianReleation']?>
	</td>
	<td>
		<?= kis_ui::displayTableField($applicationInfo['G']['relationship']) ?>
	</td>
</tr>
</table>