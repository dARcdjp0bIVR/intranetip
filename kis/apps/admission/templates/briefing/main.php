<?php
// Using: Pun

######## Init START ########
if(!$selectSchoolYearID){
	$selectSchoolYearID = $schoolYearID;
}

if(class_exists('admission_briefing')){
	$abb = new admission_briefing();
}else{
	$abb = new admission_briefing_base();
}
######## Init END ########

######## Get Data START ########
/** /
for($i=6;$i<50;$i++){
$rs = $abb->insertBriefingApplicant(array(
	'BriefingID'=>'2',
	'Email'=>'email'.$i,
	'ApplicantID'=>'ApplicantID2_'.$i,
	'BirthCertNo'=>'BirthCertNo'.$i,
	'ParentName'=>'ParentName'.$i,
	'PhoneNo'=>'PhoneNo'.$i,
	'SeatRequest'=>1,
));
}
/**/

$allSession = $abb->getAllBriefingSession($selectSchoolYearID);
$allUsedQuota = $abb->getAllBriefingSessionUsedQuota($selectSchoolYearID);
$applicantCount = $abb->getAllBriefingSessionApplicantCount($selectSchoolYearID);
######## Get Data END ########

?>
<script>
kis.admission.briefing_init({
	returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
	msg_confirm_delete:'<?=$kis_lang['Admission']['msg']['are_you_sure_to_remove_item']?>',
	msg_selectatleastonerecord:'<?=$kis_lang['Admission']['msg']['selectatleastonerecord']?>',
	selectatleastonerecord:'<?=$kis_lang['Admission']['msg']['selectatleastonerecord']?>',
	selectonlyonerecord:'<?=$kis_lang['Admission']['msg']['selectonlyonerecord']?>',
	exportallrecordsornot:'<?=$kis_lang['Admission']['briefingExportAllRecordsOrNot']?>',
	printallrecordsornot:'<?=$kis_lang['Admission']['msg']['printallrecordsornot']?>',
	sendemailallrecordsornot:'<?=$kis_lang['Admission']['msg']['sendemailallrecordsornot']?>',
	importinterviewallrecordsornot:'<?=$kis_lang['Admission']['msg']['importinterviewallrecordsornot']?>',
	returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
	close:'<?=$kis_lang['close']?>',
	importinterviewinfobyarrangementornot:'<?=$kis_lang['msg']['importinterviewinfobyarrangement']?>'
});
</script>
<div class="main_content_detail">
	<?php
    	include 'moduleTab.php';
    ?>


 	<p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	
	<div class="Content_tool">
		<a href="#" class="tool_new new"><?=$kis_lang['new']?></a>
		
		<a href="#" class="tool_export export"> <?=$kis_lang['export']?> </a>
		
		<?php if($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){ ?>
			<a href="javascript: void(0);" id="briefingApplicantLottery" class="refresh"> <?=$kis_lang['Admission']['lottery']?> </a>
		<?php } ?>
	</div>
	
    <form class="filter_form"> 
		<div id="table_filter">
	        <?=$schoolYearSelection?>
	    </div>
	    <div class="search">
		    <input placeholder="<?=$kis_lang['search'] ?>" name="keyword" value="<?=$keyword?>" type="text"/>
		</div>
    </form>
    
	<div class="table_board">
		<div class="common_table_tool common_table_tool_table">
            <a href="#" class="tool_edit"><?=$kis_lang['edit']?></a>
			<a href="#" class="tool_delete"><?=$kis_lang['delete']?></a>
        </div>
        <p class="spacer"></p>
        <form id="form1" method="post">
			<table class="common_table_list edit_table_list">
				<colgroup>
					<col nowrap="nowrap">
				</colgroup>
        
				<thead>
<!-- Table Header START -->
					<tr>
						<th width="20">&nbsp;</th>
						<th><?=$kis_lang['briefingdate']?></th>
						<th><?=$kis_lang['timeslot']?></th>
						<th><?=$kis_lang['sessionTitle']?></th>
						<th><?=$kis_lang['quotaRemains']?> </th>
						<th><?=$kis_lang['applicantsCount']?></th>
						<th><?=$kis_lang['enrolmentPeriod']?></th>
						<th><input type="checkbox" id="checkmaster"></th>
					</tr>
<!-- Table Header END -->
				</thead>
				
				<tbody>
<!-- Table Content START -->
					<?php 
					if(count($allSession)){
						foreach($allSession as $index=>$session){
							$BriefingID = $session['BriefingID'];
							$remainsQuota = $session['TotalQuota'] - $allUsedQuota[$BriefingID];
							if($remainsQuota<1){
								$remainsQuota = "<font color=\"red\">{$remainsQuota}</font>";
							}
					?>
					<tr>
						<td>
							<?=($index+1)?>
						</td>
						<td>
							<?=substr($session['BriefingStartDate'], 0, 10)?>
						</td>
						<td>
							<?=substr($session['BriefingStartDate'], 11, 5)?>
							~
							<?=substr($session['BriefingEndDate'], 11, 5)?>
						</td>
						<td>
							<?=$session['Title']?>
						</td>
						<td>
							<?=$session['TotalQuota']?> (<?=$remainsQuota?>)
						</td>
						<td>
							<?php if($applicantCount[$BriefingID]){ ?>
								<a href="#/apps/admission/briefing/applicantList?recordID=<?=$BriefingID?>">
									<?=$applicantCount[$BriefingID]?>
								</a>
							<?php }else{ ?>
								<span>0</span>
							<?php } ?>
						</td>
						<td>
							<?=substr($session['EnrolmentStartDate'], 0, 16)?>
							~
							<?=substr($session['EnrolmentEndDate'], 0, 16)?>
						</td>
						<td>
							<input type="checkbox" name="briefingId[]" value="<?=$BriefingID?>" />
						</td>
					</tr>
					<?php 
						}
					} else {
					?>
					<tr>
						<td colspan="7" style="text-align:center;"><?=$kis_lang['norecord']?></td>
					</tr>
					<?php
					}
					?>
<!-- Table Content END -->
				</tbody>
				
			</table>
			<input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$schoolYearID?>">
		</form>
		<p class="spacer"></p>
		<p class="spacer"></p>
		<br>
		<?=$kis_lang['Admission']['briefingformlink']?>: <a target="_blank" href="<?=(checkHttpsWebProtocol())? 'https':'http'?>://<?=$_SERVER['HTTP_HOST']?>/kis/admission_briefing/"><?=(checkHttpsWebProtocol())? 'https':'http'?>://<?=$_SERVER['HTTP_HOST']?>/kis/admission_briefing/</a>
	</div><!-- <div class="table_board"> -->
</div><!-- <div class="main_content_detail"> -->

<form id="exportForm" action="/kis/admission_briefing/export_briefing_result.php" method="POST" style="display:none">
</form>

<script>
$('#checkmaster').click(function(){
	if($(this).attr('checked') == 'checked'){
		$('input[name="briefingId\[\]"]').attr('checked', 'checked' );
	}else{
		$('input[name="briefingId\[\]"]').removeAttr('checked');
	}
});

$('#briefingApplicantLottery').click(function(){
	if(!confirm('<?=$kis_lang['Admission']['msg']['lotteryRemarks'] ?>')){
		return false;
	}
	kis.showLoading(); 
	$.post('apps/admission/ajax.php?action=briefingApplicantLottery', {
			'schoolYearId': '<?=$schoolYearID ?>'
		}, function(data){	
		console.log(data);
		
		//parent.$.fancybox.close();
		kis.hideLoading().loadBoard();
		if(data < 1){
			alert("Cannot process lottery. Please try again!");
		}else{
			$.address.value('/apps/admission/briefing/?sysMsg=' + data);
		}
	});
	return false;
});
</script>