<?php
// Editing by 
include_once($intranet_root."/lang/lang.".$intranet_session_language.".php");


############## Init START ##############
$linterface = $kis_data['libinterface'];
if(class_exists('admission_briefing')){
	$admission_briefing = new admission_briefing();
}else{
	$admission_briefing = new admission_briefing_base();
}
$BriefingID = $_REQUEST['recordID'];

$amount = ($amount == '')?20:$amount;
$page = ($page == '')?1:$page;
$filterDeleted = ($filterDeleted == '')?0:$filterDeleted;
$selectStatus = (isset($selectStatus))?$selectStatus:'';
############## Init END ##############


############## Get Data START ##############
$filter = array(
	'page' => $page,
	'amount' => $amount,
	'keyword' => $keyword,
	'sortby' => $sortby,
	'order' => $order,
	'filterDeleted' => $filterDeleted,
	'schoolYearID' => $schoolYearID
);

if($sys_custom['KIS_Admission']['BriefingStatus']){
    unset($filter['filterDeleted']);
    $filter['status'] = $selectStatus;
}

$total = $countAllApplicant = $admission_briefing->getBriefingSessionApplicantCount($BriefingID, $keyword, $schoolYearID, $filter);

$allApplicant = $admission_briefing->getFilterBriefingApplicant($BriefingID, $filter);
foreach($allApplicant as $index=>$applicant){
	$allApplicant[$index]['Display_Email'] = ($applicant['Email'])?$applicant['Email']:$applicant['Email_BAK'];
	$allApplicant[$index]['Display_StudentGender'] = $kis_lang['Admission']['genderType'][ $applicant['StudentGender'] ];
	
	if($sys_custom['KIS_Admission']['BriefingStatus']){
	    foreach($admission_cfg['BriefingStatus'] as $status => $value){
	        if($applicant['Status'] == $value){
	            $allApplicant[$index]['Status'] = $status;
	        }
	    }
	}
}
############## Get Data END ##############

############## UI Init START ##############
if($sys_custom['KIS_Admission']['BriefingStatus']){
    $filterDeletedHTML = $admission_briefing->getStatusSelection($selectStatus);
    $statusSelection = $admission_briefing->getStatusSelection($selectStatus, 'status', false, false);
}else{
    $filterDeletedHTML = '<select id="filterDeleted" name="filterDeleted" class="auto_submit">';
    $filterDeletedHTML .= '<option value="0" ' . (($filterDeleted)?'':'selected') . '>' . $kis_lang['Admission']['briefingStatus']['active'] . '</option>';
    $filterDeletedHTML .= '<option value="1" ' . (($filterDeleted)?'selected':'') . '>' . $kis_lang['Admission']['briefingStatus']['deleted'] . '</option>';
    $filterDeletedHTML .= '</select>';
}


$rs = $admission_briefing->getAllBriefingSession($schoolYearID);
$allBriefingSession = array();
foreach($rs as $r){
    $date = substr($r['BriefingStartDate'], 0, 10);
    $allBriefingSession[] = array(
        $r['BriefingID'], 
        "[{$date}] {$r['Title']}"
    );
}
$briefingSelection = $libinterface->GET_SELECTION_BOX($allBriefingSession, ' id="BriefingID"', '', $BriefingID);
############## UI Init END ##############


############## Display Field START ##############
$displayField = array();

if($BriefingID <= 0){
//	$displayField[] = array(
//		'title' => $kis_lang['briefingdate'],
//		'objField' => 'BriefingDate',
//	);
//	$displayField[] = array(
//		'title' => $kis_lang['timeslot'],
//		'objField' => 'BriefingTimeSlot',
//	);
	$displayField[] = array(
		'title' => $kis_lang['sessionTitle'],
		'objField' => 'Title',
	);
}

if($sys_custom['KIS_Admission']['KTLMSKG']['Settings']){
	$displayField[] = array(
		'title' => $kis_lang['applicationno'],
		'objField' => 'ApplicantID',
	);
	$displayField[] = array(
		'title' => $kis_lang['studentname'],
		'objField' => 'StudentName',
	);
	$displayField[] = array(
		'title' => $kis_lang['Admission']['gender'],
		'objField' => 'Display_StudentGender',
	);
	$displayField[] = array(
		'title' => $kis_lang['parentname'],
		'objField' => 'ParentName',
	);
	$displayField[] = array(
		'title' => $kis_lang['emailaddress'],
		'objField' => 'Display_Email',
	);
	$displayField[] = array(
		'title' => $kis_lang['Admission']['phoneno'],
		'objField' => 'PhoneNo',
	);
	$displayField[] = array(
		'title' => $kis_lang['requestSeat'],
		'objField' => 'SeatRequest',
	);
	$displayField[] = array(
		'title' => $kis_lang['Admission']['KTLMSKG']['KindergartenAttending'],
		'objField' => 'Kindergarten',
	);
}elseif($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
	$displayField[] = array(
		'title' => $kis_lang['applicationno'],
		'objField' => 'ApplicantID',
	);
	$displayField[] = array(
		'title' => $kis_lang['studentname'],
		'objField' => 'StudentName',
	);
	$displayField[] = array(
		'title' => $kis_lang['parentname'],
		'objField' => 'ParentName',
	);
	$displayField[] = array(
		'title' => $kis_lang['emailaddress'],
		'objField' => 'Display_Email',
	);
	$displayField[] = array(
		'title' => $kis_lang['Admission']['phoneno'],
		'objField' => 'PhoneNo',
	);
	$displayField[] = array(
		'title' => $kis_lang['requestSeat'],
		'objField' => 'SeatRequest',
	);
	$displayField[] = array(
		'title' => $kis_lang['Admission']['HKUGAPS']['studentyearofbirth'],
		'objField' => 'DOB',
	);
	
	function valueFilter($field, $applicant){
	    $value = $applicant[$field['objField']];
	    if($field['objField'] == 'DOB'){
	        $value = substr($value, 0, 4);
	    }
	    return $value;
	}
}elseif($sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings']){
    $displayField[] = array(
        'title' => $kis_lang['briefingdate'],
        'objField' => 'BriefingDate',
    );
    $displayField[] = array(
        'title' => $kis_lang['briefing'].$kis_lang['timeslot'],
        'objField' => 'BriefingTimeSlot',
    );
    $displayField[] = array(
        'title' => $kis_lang['applicationno'],
        'objField' => 'ApplicantID',
    );
    $displayField[] = array(
        'title' => $kis_lang['studentname'],
        'objField' => 'StudentName',
    );
    $displayField[] = array(
        'title' => $kis_lang['parentname'],
        'objField' => 'ParentName',
    );
    $displayField[] = array(
        'title' => $kis_lang['emailaddress'],
        'objField' => 'Display_Email',
    );
    $displayField[] = array(
        'title' => $kis_lang['Admission']['phoneno'],
        'objField' => 'PhoneNo',
    );
    $displayField[] = array(
        'title' => $kis_lang['requestSeat'],
        'objField' => 'SeatRequest',
    );
    $displayField[] = array(
        'title' => $kis_lang['Admission']['birthcertno'],
        'objField' => 'BirthCertNo',
    );
}else{
	$displayField[] = array(
		'title' => $kis_lang['applicationno'],
		'objField' => 'ApplicantID',
	);
	$displayField[] = array(
		'title' => $kis_lang['studentname'],
		'objField' => 'StudentName',
	);
	$displayField[] = array(
		'title' => $kis_lang['parentname'],
		'objField' => 'ParentName',
	);
	$displayField[] = array(
		'title' => $kis_lang['emailaddress'],
		'objField' => 'Display_Email',
	);
	$displayField[] = array(
		'title' => $kis_lang['Admission']['phoneno'],
		'objField' => 'PhoneNo',
	);
	$displayField[] = array(
		'title' => $kis_lang['requestSeat'],
		'objField' => 'SeatRequest',
	);
	$displayField[] = array(
		'title' => $kis_lang['Admission']['birthcertno'],
		'objField' => 'BirthCertNo',
	);
}
############## Display Field END ##############


############## UI START ##############
?>
<div class="main_content_detail">
	<?php
    	include 'moduleTab.php';
    ?>

	<?=$kis_data['NavigationBar']?>

 	<p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	
	<? if(!$filterDeleted){ ?>
	<div class="Content_tool">
		<a href="#" class="tool_export export"> <?=$kis_lang['export']?> </a>
	</div>
	<? } ?>
	
    <form class="filter_form"> 
		<div class="search">
		    <input placeholder="<?=$kis_lang['search'] ?>" name="keyword" value="<?=$keyword?>" type="text"/>
		</div>
    </form>
    
	<div class="table_board">
	
        <form class="filter_form"> 
    		<div id="table_filter">
    			<?=$filterDeletedHTML?>
    	    </div>

			<div class="common_table_tool common_table_tool_table">
	    		<?php if($sys_custom['KIS_Admission']['BriefingStatus']){ ?>
    				<a href="#" class="tool_set openBox" data-target="#edit_status_box"><?=$kis_lang['changestatus']?></a>
                <?php } ?>
				<!--a href="#" class="openBox" data-target="#edit_session_box"><?=$kis_lang['changebriefingsession']?></a-->
            </div>
        </form>
        
        <p class="spacer"></p>
        <form id="form1" method="post">
			<table class="common_table_list edit_table_list">
				<colgroup>
					<col nowrap="nowrap">
				</colgroup>
        
				<thead>
<!-- Table Header START -->
					<tr>
						<th width="20">&nbsp;</th>
						<!--th><?kis_ui::loadSortButton('ApplicantID','applicationno', $sortby, $order)?></th-->
						
						<?php foreach($displayField as $field){ ?>
							<th><?=$field['title']?></th>
						<?php } ?>

						<?php if($sys_custom['KIS_Admission']['BriefingStatus']){ ?>
							<th><?=$kis_lang['status'] ?></th>
				  			<th><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,'Briefing_ApplicantID[]'):setChecked(0,this.form,'Briefing_ApplicantID[]')"></th>
				  		<?php } ?>
					</tr>
<!-- Table Header END -->
				</thead>
				
				<tbody>
<!-- Table Content START -->
					<?php 
					if(count($allApplicant)){
						$start = ($page - 1) * $amount;
						foreach($allApplicant as $index=>$applicant){
						    $rowClass = '';
						    if($applicant['Status'] == 'pending'){
    						    $rowClass = 'absent';
						    }elseif($applicant['Status'] == 'cancelled' || $applicant['DateDeleted']){
    						    $rowClass = 'draft';
						    }
					?>
    					<tr class="<?=$rowClass ?>">
    						<td>
    							<?=($index + 1 + $start)?>
    						</td>
    						
    						<?php foreach($displayField as $field){ ?>
    							<td>
        							<?php
        							if(function_exists('valueFilter')){
        							    echo valueFilter($field, $applicant);
        							}else{
        							    echo $applicant[ $field['objField'] ];
        							}
        							?>
    							</td>
    						<?php } ?>
    						
    						<?php if($sys_custom['KIS_Admission']['BriefingStatus']){ ?>
    							<td><?=$kis_lang['Admission']['Status'][ $applicant['Status'] ] ?></td>
    				  			<td><input type="checkbox" name="Briefing_ApplicantID[]" value="<?=$applicant['Briefing_ApplicantID'] ?>"></td>
    				  		<?php } ?>
    					</tr>
					<?php 
						}
					} else {
					?>
    					<tr>
    						<td colspan="99" style="text-align:center;"><?=$kis_lang['norecord']?></td>
    					</tr>
					<?php
					}
					?>
<!-- Table Content END -->
				</tbody>
				
			</table>
		</form>
		<p class="spacer"></p>
		
<!-- Paging START -->
		<div id="table_filter">
			<form class="filter_form">
				<input type="hidden" name="selectSchoolYearID" value="<?=$schoolYearID?>"> 
				<input type="hidden" name="recordID" id="recordID" value="<?=$BriefingID?>"> 
			</form>  
		</div>
		<?php kis_ui::loadPageBar($page, $amount, $total, $sortby, $order) ?>
<!-- Paging END -->
		<p class="spacer"></p>
		<br>
	</div><!-- <div class="table_board"> -->
</div><!-- <div class="main_content_detail"> -->


<form id="exportForm" action="/kis/admission_briefing/export_briefing_result.php" method="POST" style="display:none">
    <input name="briefingId[]" value="<?=$BriefingID?>" />
    <input name="keyword" value="<?=$keyword?>" />
    <input type="hidden" name="selectSchoolYearID" value="<?=$schoolYearID?>">
</form>


<!-- Edit status START -->
<div id='edit_status_box' style="padding:5px;display:none;" class="pop_edit">
	<form id="briefing_status_form" method="post">
		<div class="pop_title">
			<span><?=$kis_lang['changestatus']?></span>
		</div>
		<div class="table_board" style="height:330px">
			
			<table class="form_table">
				<tr>
                   <td width="30%" class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['applicationstatus']?></td>
                   <td width="70%"><?=$statusSelection?></td>
                 </tr> 
			</table>
		  <p class="spacer"></p>
	   </div>
	    <div class="edit_bottom">         
	       <input id="submitBtn" name="submitBtn" type="submit" class="formbutton" value="<?=$kis_lang['submit']?>" />
	       <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();" value="<?=$kis_lang['cancel']?>" />
	  	</div>
  		
	</form> 
</div>
<!-- Edit status END -->

<!-- Edit session START -->
<div id='edit_session_box' style="padding:5px;display:none;" class="pop_edit">
	<form id="briefing_session_form" method="post">
		<div class="pop_title">
			<span><?=$kis_lang['changebriefingsession']?></span>
		</div>
		<div class="table_board" style="height:330px">
			
			<table class="form_table">
				<tr>
                   <td width="30%" class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['briefing']?></td>
                   <td width="70%"><?=$briefingSelection?></td>
                 </tr> 
			</table>
		  <p class="spacer"></p>
	   </div>
	    <div class="edit_bottom">         
	       <input id="submitBtn" name="submitBtn" type="submit" class="formbutton" value="<?=$kis_lang['submit']?>" />
	       <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();" value="<?=$kis_lang['cancel']?>" />
	  	</div>
  		
	</form> 
</div>
<!-- Edit session END -->

<script>
	$('.auto_submit').change(function(){
		$(this).closest('form').submit();
	});

	$('.Content_tool .export').click(function(e){
		e.preventDefault();
		$('#exportForm').submit();
	});



	

	var open_fancy_box = function(page){
		$.fancybox( {
			'fitToView'    	 : false,	
			'autoDimensions'    : false,
			'autoScale'         : false,
			'autoSize'          : false,
			'width'             : 600,
			'height'            : 450,
			'padding'			 : 20,

			'transitionIn'      : 'elastic',
			'transitionOut'     : 'elastic',
			'href' : page				 
		});	 
	}
	$('div.common_table_tool a.openBox').click(function(){
		if(!check_checkbox($('#form1')[0], 'Briefing_ApplicantID[]')){
			alert('<?=$kis_lang['Admission']['msg']['selectatleastonerecord']?>');
			return false;
		}

		var page = $(this).data('target');
		open_fancy_box(page);
		return false;
	});
	$('form#briefing_status_form').submit(function(){
		var status = $('#status').val();
		var briefingApplicationIds = Get_Check_Box_Value('Briefing_ApplicantID[]');
		briefingApplicationIds = briefingApplicationIds.join(',');

		kis.showLoading();
		$.post('apps/admission/ajax.php?action=updateBriefingApplicationStatusByIds', {briefingApplicationIds:briefingApplicationIds,status:status}, function(data){	
			parent.$.fancybox.close();
			kis.hideLoading().loadBoard();
		});
		return false;
	});
	$('form#briefing_session_form').submit(function(){
		var briefingID = $('#BriefingID').val();
		var briefingApplicationIds = Get_Check_Box_Value('Briefing_ApplicantID[]');
		briefingApplicationIds = briefingApplicationIds.join(',');

		kis.showLoading();
		$.post('apps/admission/ajax.php?action=updateBriefingApplicationSessionByIds', {briefingApplicationIds:briefingApplicationIds,briefingID:briefingID}, function(data){	
			parent.$.fancybox.close();
			kis.hideLoading().loadBoard();
		});
		return false;
	});
</script>