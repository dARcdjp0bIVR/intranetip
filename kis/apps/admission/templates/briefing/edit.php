<?php
// Editing by Pun
include_once($intranet_root."/lang/lang.".$intranet_session_language.".php");


############## Init START ##############
$linterface = $kis_data['libinterface'];
$admission_briefing_base = new admission_briefing_base();
############## Init END ##############


############## Get Data START ##############
$kis_data['BriefingID'] = $briefingId;
if($kis_data['BriefingID']!=''){
	$session = $admission_briefing_base->getBriefingSession($kis_data['BriefingID']);
	$briefing_date = substr($session['BriefingStartDate'], 0, 10);
	$briefing_start_time = substr($session['BriefingStartDate'], 11);
	$briefing_end_time = substr($session['BriefingEndDate'], 11);
	$briefing_title = $session['Title'];
	$quota = $session['TotalQuota'];
	$applicantsSeatMax = $session['QuotaPerApplicant'];
	$enrolment_start_period_date = substr($session['EnrolmentStartDate'],0,10);
	$enrolment_start_time = substr($session['EnrolmentStartDate'], 11);
	$enrolment_end_period_date = substr($session['EnrolmentEndDate'],0,10);
	$enrolment_end_time = substr($session['EnrolmentEndDate'], 11);
	$instruction = $session['Instruction'];
}
############## Get Data END ##############

############## UI Init START ##############
$seatMaxSelection = '<select id="applicantsSeatMax" name="applicantsSeatMax">';
for($i=1;$i<=10;$i++){
	$selected = '';
	if($i == $applicantsSeatMax){
		$selected = ' selected';
	}
	$seatMaxSelection .= "<option value=\"{$i}\" {$selected}>{$i}</option>";
}
$seatMaxSelection .= '</select>';
############## UI Init END ##############
?>
<script type="text/javascript">
$(function(){
	kis.admission.briefing_edit_init({
		invaliddateformat:'<?=$kis_lang['Admission']['msg']['invaliddateformat']?>',
		enddateearlier: '<?=$kis_lang['Admission']['msg']['enddateearlier']?>',
		entertitle: '<?=$kis_lang['msg']['enterTitle']?>',
		enterQuota: '<?=$kis_lang['msg']['enterQuota']?>',
		briefingenddateearlier: '<?=$kis_lang['Admission']['msg']['briefingEndDateEarlier']?>',
		
		emptyapplicationdatewarning: '<?=$kis_lang['Admission']['msg']['emptyapplicationdatewarning']?>',
		selectatleastonetimeslot: '<?=$kis_lang['Admission']['msg']['selectatleastonetimeslot']?>',
		enterpositivenumber: '<?=$kis_lang['Admission']['msg']['enternumber']?>',
		quotashouldnotlessthanapplied: '<?=$kis_lang['Admission']['msg']['quotashouldnotlessthanapplied']?>',
		applied: '<?=$applied?>'
	});
});
</script>
<div class="main_content">
	<form id='briefing_edit_form'>
	    <div class="main_content_detail">
        	<?php
            	include 'moduleTab.php';
            ?>
        	    
			<?=$kis_data['NavigationBar']?>
			<p class="spacer"></p>
			<div class="table_board">
				<table class="form_table">

<!---------------- Form START ---------------->
				    <tr>
						<td class="field_title" width="25%"><?=$mustfillinsymbol?><?=$kis_lang['briefingdate']?></td>
						<td width="75%"><input type="text" name="briefing_date" id="briefing_date" value="<?=$briefing_date?>">&nbsp;<span class="error_msg" id="warning_briefing_date"></span></td>
					</tr>				
				    <tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['briefing'].$kis_lang['timeslot']?></td>
						<td>
							<?=$libadmission->Get_Time_Selection($libinterface,'briefing_start_time',$briefing_start_time,'',false)?>
							~
							<?=$libadmission->Get_Time_Selection($libinterface,'briefing_end_time',$briefing_end_time,'',false)?>&nbsp;
							<span class="error_msg" id="warning_briefing_time"></span>
						</td>
					</tr>
				    <tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['sessionTitle']?></td>
						<td><input type="text" name="briefing_title" id="briefing_title" value="<?=$briefing_title?>">&nbsp;<span class="error_msg" id="warning_briefing_title"></span></td>
					</tr>
					<tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['qouta']?></td>
						<td><input type="text" name="quota" id="quota" value="<?=$quota?>" />&nbsp;<span class="error_msg" id="warning_quota"></span></td>
					</tr>
					<tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['applicantsSeatMax']?></td>
						<td><?=$seatMaxSelection?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['enrolmentPeriodStart']?></td>
						<td>
							<input type="text" name="enrolment_start_period_date" id="enrolment_start_period_date" value="<?=$enrolment_start_period_date?>">
							<?=$libadmission->Get_Time_Selection($libinterface,'enrolment_start_time',$enrolment_start_time,'',false)?>
							<span class="error_msg" id="warning_enrolment_start_period"></span>
						</td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['enrolmentPeriodEnd']?></td>
						<td>
							<input type="text" name="enrolment_end_period_date" id="enrolment_end_period_date" value="<?=$enrolment_end_period_date?>">
							<?=$libadmission->Get_Time_Selection($libinterface,'enrolment_end_time',($enrolment_end_time)?$enrolment_end_time:'23:59:59','',false)?>
							<span class="error_msg" id="warning_enrolment_end_period"></span>
						</td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['instructiontoapplicants']?></td>
						<td>
							<textarea id="instruction" name="instruction"><?=$instruction?></textarea>
						</td>
					</tr>
<!---------------- Form END ---------------->

			    </table>
				<p class="spacer"></p>
				<p class="spacer"></p><br />
	        </div>
	        <div class="text_remark"><?=$kis_lang['requiredfield']?></div>
			<div class="edit_bottom">
				<input type="hidden" name="BriefingID" id="BriefingID" value="<?=$kis_data['BriefingID']?>">
				<input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$kis_data['schoolYearID']?>">
				<input type="submit" id="submitBtn" name="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
				<input type="button" id="cancelBtn" name="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
			</div>
		</div>
	</form>
</div>