<?php
// Editing by 
?>
<script>
kis.admission.update_status_init({
	msg_selectstatus:'<?=$kis_lang['msg']['selectstatus']?>',
	msg_worngapplicationno:'<?=$kis_lang['msg']['worngapplicationno']?>',
	msg_updateunsuccess:'<?=$kis_lang['msg']['updateunsuccess']?>',
	msg_enterapplicationno:'<?=$kis_lang['msg']['enterapplicationno']?>'
});
</script>
<div class="main_content_detail">
 	 <p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
   
    	<div id="input_field" style="float:right;margin-right:5px">
    		<?=$kis_lang['changestatus']?> <?=$applicationStatus?>&nbsp;&nbsp; <?=$kis_lang['enterscanapplicationno']?> <input style="border:1px solid #CCC;border-radius:3px;padding-top:3px;padding-bottom:3px;" name="applicationno" value="" type="text" />
        </div>
                      <p class="spacer"></p>
                      	  <div class="table_board">
                      <table class="common_table_list edit_table_list">
                        <colgroup><col nowrap="nowrap">
                        </colgroup><thead>
                          <tr> 
                            <th><?=$kis_lang['applicationno']?></th>
                            <th><?=$kis_lang['form']?></th>
				  			<th><?=$kis_lang['studentname']?></th>
                            <th><?=$kis_lang['parentorguardian']?></th>
				  			<th><?=$kis_lang['phoneno']?></th>
				  			<th><?=$kis_lang['status']?></th>
				  			<th><?=$kis_lang['lastupdated']?></th>
                          </tr>
                        </thead>
                        <tbody>
                  	<?php 
					if($applicationDetails){
						$idx=0;
						foreach($applicationDetails as $_applicationId => $_applicationDetailsAry){
							$idx++;
							if($idx > 5)
								break;
							$_lastUpdated = $_applicationDetailsAry['modified_by'].' ('.$_applicationDetailsAry['date_modified'].')';
							$_recordId = $_applicationDetailsAry['record_id'];
							$_status = $_applicationDetailsAry['application_status'];
							$_classLevelName = $_applicationDetailsAry['ClassLevelName'];
							$_studentName = $_applicationDetailsAry['student_name'];		
							$_parentName = implode('<br/>',array_filter($_applicationDetailsAry['parent_name']));	
							$_parentPhone = implode('<br/>',array_filter($_applicationDetailsAry['parent_phone']));
							if($sys_custom['KIS_Admission']['ICMS']['Settings']){
								$classlevel = $applicationlistAry[$kis_data['classLevelID']]['ClassLevelName'];
								$_applyDayType = '<td>';
								for($i=1;$i<=3;$i++){
									if($_applicationDetailsAry['ApplyDayType'.$i])
										$_applyDayType .= '('.$i.') '.$kis_lang['Admission']['icms'][$classlevel]['TimeSlot'][$_applicationDetailsAry['ApplyDayType'.$i]].'<br/>';																				
								}
								$_applyDayType .= '</td>';
							}
							$tr_css = '';
							switch($_status){
								case 'pending':$tr_css = ' class="absent"';break;
							//	case 'paymentsettled':$tr_css = '';break;
								case 'waitingforinterview':$tr_css = ' class="waiting"';break;
								case 'confirmed':$tr_css = ' class="done"';break;
								case 'cancelled':$tr_css = ' class="draft"';break;								
							}
				?>			
							<tr<?=$tr_css?>>
							  <td><a href="#/apps/admission/applicantslist/details/<?=$schoolYearID?>/<?=$_recordId?>/"><?=$_applicationId?></a><br></td>
							  <td><?=$_classLevelName?></td>
							  <td><?=$sys_custom['KIS_Admission']['ICMS']['Settings']?str_replace(",", " ", $_studentName):$_studentName?></td>
							  <td><?=$_parentName?($sys_custom['KIS_Admission']['ICMS']['Settings']?str_replace(",", " ", $_parentName):$_parentName):'--'?></td>
							  <td nowrap="nowrap"><?=$_parentPhone?$_parentPhone:'--'?></td>
							  <!--<?=$sys_custom['KIS_Admission']['ICMS']['Settings']?$_applyDayType:''?>-->
							  <td><?=$kis_lang['Admission']['Status'][$_status]?></td>
							  <td><?=$_lastUpdated?></td>
							 </tr>
				<?php } 
					}else{ //no record	
				?>		
							<tr>
							<?if($sys_custom['KIS_Admission']['ICMS']['Settings']){?>
							  <td colspan="8" style="text-align:center;"><?=$kis_lang['norecord']?></td>
							<?}else{?>
							  <td colspan="7" style="text-align:center;"><?=$kis_lang['norecord']?></td>
							<?}?>
							</tr>				
				<?php } ?>	  
                        </tbody>
                      </table>
                      <p class="spacer"></p>
                      <br><div class="text_remark"><?=$kis_lang['lastfiverecords']?></div>
                    </div></div>