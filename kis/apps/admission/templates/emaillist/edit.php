<?php
// Editing by 
/*
 * 2014-11-04 (Carlos): Added progress bar layer
 * 2014-08-11 (Carlos): Added attachment and email template
 */
include_once($intranet_root."/lang/lang.".$intranet_session_language.".php");

$linterface = $kis_data['libinterface'];

if(!isset($kis_data['recordID'])){
	exit;
}

if($kis_data['emailRecordDetail']['RecordID'] == ''){
	exit;
}

if($kis_data['recordID'] != '' && isset($kis_data['emailRecordDetail'])){
	$numOfReceiver = $kis_data['emailRecordDetail']['NumberOfReceiver'];
}else{
	$numOfReceiver = count(is_array($kis_data['applicationIds'])? $kis_data['applicationIds'] : explode(",",$kis_data['applicationIds']));
}

$attachment_ary = array();
if($kis_data['recordID']!=''){
	$goto_step = 2;
//	$preview_subject = $kis_data['libadmission']->replaceEmailVariables($kis_data['emailRecordDetail']['Subject'], $kis_data['emailReceivers'][0]['ChineseName'],$kis_data['emailReceivers'][0]['EnglishName'], $kis_data['emailReceivers'][0]['ApplicationNo'], $kis_data['emailReceivers'][0]['Status'],$kis_data['emailReceivers'][0]['InterviewDate']);
//	$preview_message = $kis_data['libadmission']->replaceEmailVariables($kis_data['emailRecordDetail']['Message'], $kis_data['emailReceivers'][0]['ChineseName'],$kis_data['emailReceivers'][0]['EnglishName'], $kis_data['emailReceivers'][0]['ApplicationNo'], $kis_data['emailReceivers'][0]['Status'],$kis_data['emailReceivers'][0]['InterviewDate']);
//	$hidden_preview_subject = $kis_data['emailRecordDetail']['Subject'];
//	$hidden_preview_message = $kis_data['emailRecordDetail']['Message'];
	$preview_subject = $kis_data['emailRecordDetail']['Subject'];
	$preview_message = $kis_data['emailRecordDetail']['Message'];
	
	$attachment_ary = $kis_data['libadmission']->getEmailAttachmentRecords($kis_data['libadmission']->getEmailAttachmentType('Email'), $kis_data['recordID']);
}else{
	$goto_step = 1;
}

$failCount = 0;
$notAckCount = 0;
//email preview
for($i=0;$i<count($kis_data['emailReceivers']);$i++){
	$PreviewChoice[$i][] = $kis_data['emailReceivers'][$i]['UserID'];
	$PreviewChoice[$i][] = ($i+1).'. '.$kis_data['emailReceivers'][$i]['EnglishName'].' ('.$kis_data['emailReceivers'][$i]['ApplicationNo'].')';
	
	if($kis_data['emailReceivers'][$i]['SentStatus']=='0'){
		$failCount++;
	}	
	if($kis_data['emailReceivers'][$i]['AcknowledgeDate']==''){
		$notAckCount++;
	}
}
if (sizeof($PreviewChoice)>0)
{
	$button_previous_shown = $linterface->GET_BTN(" &lt;&lt;", "button", "", "ViewPrevious", " title='".$Lang['MassMailing']['PreviousRecord']."' onMouseOver=\"this.className='formsubbutton'\"");

	if (sizeof($PreviewChoice)>1)
	{
		$button_next_shown = $linterface->GET_BTN("&gt;&gt; ", "button", "", "ViewNext", "title='".$Lang['MassMailing']['NextRecord']."' onMouseOver=\"this.className='formsubbutton'\"");
	} else
	{
		$button_next_shown = $linterface->GET_BTN("&gt;&gt; ", "button", "", "ViewNext", " title='".$Lang['MassMailing']['NextRecord']."' onMouseOver=\"this.className='formsubbutton'\"");
	}
	$PreviewSelections = $button_previous_shown ." " .$linterface->GET_SELECTION_BOX($PreviewChoice, " name='PreviewOptions' id='PreviewOptions' ", "") . " " . $button_next_shown;
	$PreviewSelections .= " <span id='PreviewStatus'></span>";
}

$email_template_type = $kis_data['libadmission']->getEmailTemplateType('Active');
$email_templates = $kis_data['libadmission']->getEmailTemplateRecords($email_template_type);
$email_template_selection = getSelectByArray($email_templates, ' id="TemplateID" name="TemplateID" ', "", 0, 0, "-- ".$kis_lang['select_from_email_templates']." --");

?>
<style type="text/css">
/******************* step div [start] ****************************/
.stepboard {margin:0 auto; width:85%; display:block; padding:5px; border: dashed 1px #32C617; margin-bottom:10px; clear:both}
.stepboard  h1 {display:block; background:#EDFFE0; color:#188107; font-size:1em; padding:5px; margin:0}
.stepboard ul {list-style-type: none; margin:0; padding:10px; clear:both; display:block}
.stepboard ul li{ display:inline;float:left; padding:0px; margin:0px; line-height:18px;}
.stepboard ul li div{ padding-left:25px; display:block;}
.stepboard ul li em {font-style:normal; color:#FFFFFF;  background:url(/images/2009a/stepnum_off.gif) no-repeat 3px 1px; margin-left:-25px; float:left; display:block; width:25px; height:25px; text-align:center;}
.stepboard ul li span {color:#C4C4C4; display:block; padding-right:10px;}
.stepboard ul li.stepon em {font-style:bold;  background:url(/images/2009a/stepnum_on.gif) no-repeat 1px top; line-height:20px}
.stepboard ul li.stepon span {color:#188107; font-weight:bold;}

.stepboard_2s ul li{ width:50%}                   
.stepboard_3s ul li{ width:33%}                   
.stepboard_4s ul li{ width:25%}                   
.stepboard_5s ul li{ width:20%}
/******************* step div [end] ****************************/

/******************* section title span [start] ****************************/
.sectiontitle_v30 {	font-size: 1.2em;	font-weight: bold;	color: #006600;	padding-right: 3px;	padding-left: 23px;	 display:block; float:left; line-height:25px; background: url(../../../images/2009a/icon_section.gif) no-repeat 0px 3px;}
/******************* section title span [end] ****************************/
</style>
<script type="text/javascript" language="JavaScript">
$(function(){
	kis.admission.sendmail_compose_init({
		close: '<?=$kis_lang['close']?>',
		returnmsg: '',
		confirm_delete_email_attachment: '<?=$kis_lang['msg']['confirm_delete_email_attachment']?>',
		confirm_change_email_template : '<?=$kis_lang['msg']['confirm_change_email_template']?>',
		confirm_to_terminate: '<?=$Lang['MassMailing']['ConfirmToTerminate']?>',
		sending_msg1: '<?=$Lang['MassMailing']['SendingTo1']?>',
		sending_msg2: '<?=$Lang['MassMailing']['SendingTo2']?>',
		selectatleastoneapplicant: '<?=$kis_lang['msg']['selectatleastoneapplicant']?>'   
	});
})
</script>
<div class="main_content_detail">
<form id="MailForm" name="MailForm" method="POST" action="" enctype="multipart/form-data">
	<?=$kis_data['NavigationBar']?>
	<p class="spacer"></p><br>
	
	<div class="table_board">
		<table id="ComposeMailTable" class="form_table">
        	<tbody>
            	<tr> 
                	<td class="field_title" style="width:30%;"><?=$kis_lang['selectedstudents']?></td>
                	<td style="width:70%;"><a href="javascript:void(0);" id="ViewStudentBtn"><?=$numOfReceiver?></a></td>
                </tr>
                <tr class="step1" <?=$goto_step==2?'style="display:none;"':''?>> 
                	<td class="field_title" style="width:30%;"><?=$kis_lang['subject']?><?=$mustfillinsymbol?></td>
                	<td style="width:70%;">
                		<?=$linterface->GET_TEXTBOX("Subject", "Subject", $kis_data['emailRecordDetail']['Subject'], $OtherClass='', $OtherPar=array()).'<br />'?>
                		<?=$linterface->Get_Form_Warning_Msg("WarnSubject", $kis_lang['msg']['requestinputmailsubject'], $Class='', $display=false)?>
                	</td>
                </tr>
                <tr class="step2" <?=$goto_step==1?'style="display:none;"':''?>>
                	<td class="field_title" style="width:30%;"><?=$kis_lang['subject'].'<!-- ('.$kis_lang['preview'].')-->'?></td>
                	<td style="width:70%;"><div id="PreviewSubject"><?=$preview_subject?></div></td>
                </tr>
                <tr class="step1" <?=$goto_step==2?'style="display:none;"':''?>> 
                	<td class="field_title" style="width:30%;"><?=$kis_lang['mailcontents']?><?=$mustfillinsymbol?></td>
                	<td style="width:70%;">
                		<?=$email_template_selection?><br />
                		<textarea id="Message" name="Message"><?=$kis_data['emailRecordDetail']['Message']?></textarea><br />
                		<div class="text_remark"><?=($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings'] ? $kis_lang['emailvariableremarkwithbriefinginfo'] : ($sys_custom['KIS_Admission']['HKUGAPS']['Settings']?$kis_lang['emailvariableremarkForHKUGAPS']:($sys_custom['KIS_Admission']['HKUGAC']['Settings']?$kis_lang['emailvariableremarkForHKUGAC']:$kis_lang['emailvariableremark'])))?></div>
                		<?=$linterface->Get_Form_Warning_Msg("WarnMessage", $kis_lang['msg']['requestinputmailmessage'], $Class='', $display=false)?>
                	</td>
                </tr>
                <tr <?=$goto_step==1?'style="display:none;"':'style="display:none;"'?>> 
                	<td class="field_title" style="width:30%;"><?=$kis_lang['mailcontents'].' ('.$kis_lang['preview'].')'?></td>
                	<td style="width:70%;border:1px solid black;"><div id="PreviewMessage"><?=$preview_message?></div></td>
                </tr>
                
                <tr>
                	<td class="field_title" style="width:30%;"><?=$kis_lang['email_template_attachment']?></td>
                	<td style="width:70%;">
                		<div class="mail_icon_form attachment_list">
                			<div id="TemplateAttachmentDiv"></div>
                			<div id="AttachmentDiv">
                		<?php
                			$x = '';
                			for($i=0;$i<count($attachment_ary);$i++){
                				$display_size = '&nbsp;('.$kis_data['libadmission']->getAttachmentDisplaySize($attachment_ary[$i]['SizeInBytes']).')';
                				$x .= '<div>';
								$x .= '<a class="btn_attachment" href="/kis/apps/admission/ajax.php?action=viewEmailAttachment&AttachmentID='.$attachment_ary[$i]['AttachmentID'].'" id="AttachmentFile_'.$attachment_ary[$i]['AttachmentID'].'" name="AttachmentFile[]">'.$attachment_ary[$i]['FileName'].$display_size.'</a>';
								$x .= '<a class="btn_remove step1" href="javascript:void(0);" id="AttachmentRemoveBtn_'.$attachment_ary[$i]['AttachmentID'].'" name="AttachmentRemoveBtn[]" alt="'.$kis_lang['remove'].'" style="display:none;"></a>';
								$x .= '<input type="hidden" name="AttachmentID[]" value="'.$attachment_ary[$i]['AttachmentID'].'" />';
								$x .= '</div>';
                			}
                			if(count($attachment_ary)==0){
                				$x = '-';
                			}
                			echo $x;
                		?>
                			</div>
	                		<div id="FileUploadDiv" class="step1" <?=$goto_step==2?'style="display:none;"':''?>>
	                			<div><input type="file" name="FileUpload_0" /><a href="javascript:void(0);" class="btn_remove" name="DeleteFileUpload[]" ></a></div>
	                		</div>
	                		<br style="clear:both" class="step1" <?=$goto_step==2?'style="display:none;"':''?> />
	                		<a href="javascript:void(0);" id="add_more_attachment" class="btn_add step1" <?=$goto_step==2?'style="display:none;"':''?>><?=$kis_lang['add']?></a>
	                		<input type="hidden" id="FileUploadTmpFolder" name="FileUploadTmpFolder" value="" />
	                		<input type="hidden" id="FileUploadCount" name="FileUploadCount" value="0" />
                		</div>
                	</td>
                </tr>
                
                <tr class="step2" <?=$goto_step==1?'style="display:none;"':''?>>
                	<td class="field_title" style="width:30%;"><?=$kis_lang['lastsenton']?></td>
                	<td style="width:70%;"><?=$kis_data['emailRecordDetail']['LastSentDate']?></td>
                </tr>
                <tr class="step1" <?=$goto_step==2?'style="display:none;"':''?>> 
                	<td class="field_title" style="width:30%;"><?=$kis_lang['requirerecipienttoacknowledge']?></td>
                	<td style="width:70%;">
                		<?=$linterface->Get_Radio_Button("IsAcknowledgeYes", "IsAcknowledge", "1", $kis_data['emailRecordDetail']['IsAcknowledge'] == '' || $kis_data['emailRecordDetail']['IsAcknowledge']=='1', $Class='', $Display=$kis_lang['yes'], $Onclick='', $Disabled='').'&nbsp;'?>
                		<?=$linterface->Get_Radio_Button("IsAcknowledgeNo", "IsAcknowledge", "0", $kis_data['emailRecordDetail']['IsAcknowledge']=='0', $Class='', $Display=$kis_lang['no'], $Onclick='', $Disabled='')?>
                	</td>
                </tr>
                <tr id="PreviewAcknowledgeRow" class="step2" <?=$goto_step==1?'style="display:none;"':''?>> 
                	<td class="field_title" style="width:30%;"><?=$kis_lang['requirerecipienttoacknowledge']?></td>
                	<td style="width:70%;">
                		<span id="PreviewAcknowledgeYes" <?=$kis_data['emailRecordDetail']['IsAcknowledge']=='1'?'':'style="display:none;"'?>><?=$kis_lang['yes']?></span>
                		<span id="PreviewAcknowledgeNo" <?=$kis_data['emailRecordDetail']['IsAcknowledge']!='1'?'':'style="display:none;"'?>><?=$kis_lang['no']?></span>
                	</td>
                </tr>
                <tr id="AcknowledgeRow" class="step1" <?=$kis_data['emailRecordDetail']['IsAcknowledge']=='0'?'style="display:none"':''?> <?=$goto_step==2?'style="display:none;"':''?>> 
                	<td style="width:30%;">&nbsp;</td>
                	<td style="width:70%;">
                		<?=$linterface->GET_TEXTBOX("AcknowledgeMessage", "AcknowledgeMessage", isset($kis_data['emailRecordDetail']['AcknowledgeMessage'])?$kis_data['emailRecordDetail']['AcknowledgeMessage']:$kis_lang['acknowledgementlinktext'], $OtherClass='', $OtherPar=array('size'=>200))?>
                		<br /><div class="text_remark"><?=$kis_lang['acknowledgemessageremark']?></div>
                		<?=$linterface->Get_Form_Warning_Msg("WarnAcknowledgeMessage", $kis_lang['msg']['requestinputacknowledgemessage'], $Class='', $display=false)?>
                	</td>
                </tr>
            </tbody>
        </table>
        <div class="text_remark step1" <?=$goto_step==2?'style="display:none;"':''?>><?=str_replace('*','<span style="color:red">*</span>',$kis_lang['requiredfield'])?></div>
		<div class="edit_bottom">
			<input type="hidden" name="recordID" id="recordID" value="<?=$kis_data['recordID']?>">
       		<input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$kis_data['schoolYearID']?>">
       		<input type="hidden" name="classLevelID" id="classLevelID" value="<?=$kis_data['classLevelID']?>">
       		<input type="hidden" name="applicationIds" id="applicationIds" value="<?=is_array($kis_data['applicationIds'])?implode(",",$kis_data['applicationIds']):$kis_data['applicationIds']?>" />
        	<input type="button" name="editSubmitBtn" id="editSubmitBtn" class="formbutton step1" value="<?=$kis_lang['submit']?>" <?=$goto_step==2?'style="display:none;"':''?> />
        	<input type="button" name="editBackBtn" id="editBackBtn" class="formbutton step1" value="<?=$kis_lang['back']?>" <?=$goto_step==2?'style="display:none;"':''?> />
            <?php if(!$sys_custom['KIS_Admission']['CREATIVE']['Settings']): ?>
        	<input type="button" name="backBtn" id="backBtn" class="formbutton step2" value="<?=$goto_step==2? $kis_lang['edit']:$kis_lang['back']?>" <?=$goto_step==1?'style="display:none;"':''?> />
            <?php endif; ?>
        	<input type="button" name="resendBtn" id="resendBtn" class="formbutton step2" value="<?=$kis_lang['resend']?>" <?=$goto_step==1?'style="display:none;"':''?> />
            <input type="button" name="printBtn" id="printBtn" class="formbutton step2" value="<?=$kis_lang['print']?>" <?=$goto_step==1?'style="display:none;"':''?> />
            <input type="button" name="editCancelBtn" id="editCancelBtn" class="formsubbutton step2" value="<?=$kis_lang['cancel']?>" />
            <input type="button" name="editDoneBtn" id="editDoneBtn" class="formsubbutton step3" value="<?=$kis_lang['done']?>" style="display:none;" />
	    </div>
        <p class="spacer"></p><br>
        <table class="form_table step2" width="90%" border="0" cellpadding="5" cellspacing="0" align="center" <?=$goto_step==1?'style="display:none;"':''?>>
			<tr><td colspan="3"><div class="table_board"><span class="sectiontitle_v30"><?=$kis_lang['mailcontents'].' ('.$kis_lang['preview'].')' ." ". $PreviewSelections ?></span></div></td></tr>
			<tr>
				<td nowrap="nowrap">&nbsp; &nbsp;</td>
				<td width="95%" valign="top" style="border:1px solid black;">
				  <div id="PreviewMerged"></div>
				  <!--<input type="hidden" name="hidden_preview_message" id="hidden_preview_message" value="<?=htmlentities($hidden_preview_message)?>" />
				  <input type="hidden" name="hidden_preview_subject" id="hidden_preview_subject" value="<?=htmlentities($hidden_preview_subject)?>" />-->
				</td>
				<td nowrap="nowrap">&nbsp; &nbsp;</td>			
			</tr>
		</table>  
	</div>
</form>
<iframe style="display:none;" name="FileUploadIframe" id="FileUploadIframe"></iframe>
</div>
<div id='ViewReceiverLayer' style="padding:5px;display:none;">
	<div class="table_board">
		<table class="common_table_list edit_table_list">
			<colgroup><col nowrap="nowrap">
			</colgroup>
			<thead>
				<tr>
				  <th width="20">#</th>
				  <th><?=$kis_lang['applicationno']?></th>
				  <th><?=$kis_lang['chinesename']?></th>
				  <th><?=$kis_lang['englishname']?></th>
				  <th><?=$kis_lang['emailaddress']?></th>
				  <!--<th><?=$kis_lang['interviewdate']?></th>-->
				  <th><?=$kis_lang['status']?></th>
				  <?php if($kis_data['recordID']!=''){ ?>
				 	<th><?=$kis_lang['sentstatus']?></th>
					<th><?=$kis_lang['lastsenton']?></th>
				  <?php } ?>
				  <?php if($kis_data['recordID']!='' && $kis_data['emailRecordDetail']['IsAcknowledge']==1){ ?>
				  <th><?=$kis_lang['acknowledgementtime']?></th>
				  <?php } ?>
			  	</tr>
			</thead>
			<tbody>
				<?php
					$x = '';
					for($i=0;$i<count($kis_data['emailReceivers']);$i++){
						$x .= '<tr>';
							$x .= '<td>'.($i+1).'</td>';
							$x .= '<td>'.kis_ui::displayTableField($kis_data['emailReceivers'][$i]['ApplicationNo']).'</td>';
							$x .= '<td>'.kis_ui::displayTableField($kis_data['emailReceivers'][$i]['ChineseName']).'</td>';
							$x .= '<td>'.kis_ui::displayTableField($kis_data['emailReceivers'][$i]['EnglishName']).'</td>';
							$x .= '<td>'.kis_ui::displayTableField($kis_data['emailReceivers'][$i]['Email']).'</td>';
							//$x .= '<td>'.kis_ui::displayTableField($kis_data['emailReceivers'][$i]['InterviewDateTime']).'</td>';
							$x .= '<td>'.kis_ui::displayTableField($kis_data['emailReceivers'][$i]['Status']).'</td>';
							if($kis_data['recordID']!=''){
								$x .= '<td>'.kis_ui::displayTableField($kis_data['emailReceivers'][$i]['SentStatus']=='1'?$kis_lang['success']:$kis_lang['failed']).'</td>';
								$x .= '<td>'.kis_ui::displayTableField($kis_data['emailReceivers'][$i]['DateModified']).'</td>';
							}
							if($kis_data['recordID']!='' && $kis_data['emailRecordDetail']['IsAcknowledge']==1){
								$x .= '<td>'.kis_ui::displayTableField($kis_data['emailReceivers'][$i]['AcknowledgeDate']).'</td>';
							}
						$x .='</tr>';
					}
					echo $x;
				?>
			</tbody>
		</table>
	  	<p class="spacer"></p>
    </div>
    <p class="spacer"></p>
    <div class="edit_bottom">         
       <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();" value="<?=$kis_lang['close']?>" />
  	</div>
</div>
<div id="ResendOptionLayer" style="padding:5px;display:none;" class="pop_edit">
	<div class="table_board">
		<table class="form_table">
			<tr>
               <td width="30%" class="field_title"><?=$kis_lang['sendto']?></td>
               <td width="70%">
               		<select id="SendTarget" name="SendTarget">
               			<option value="1" seelcted="selected"><?=$kis_lang['allapplicants']?>(<?=count($kis_data['emailReceivers'])?>)</option>
               			<!--<option value="2"><?=$kis_lang['applicantssuccesslasttime']?></option>-->
               			<option value="3"><?=$kis_lang['applicantsfailedlasttime']?>(<?=$failCount?>)</option>
               		<?php 
               			if($kis_data['recordID']!='' && $kis_data['emailRecordDetail']['IsAcknowledge']==1){
               				echo '<option value="4">'.$kis_lang['applicantshavenotacknowledged'].'('.$notAckCount.')</option>';	
               			}
               		?>
               			<option value="5"><?=$kis_lang['selectapplicants']?>(0)</option>
               		</select>
               </td>
             </tr> 
		</table>
		<div id="DivSelectApplicants" style="display:none;height: 240px;overflow: scroll;overflow-x: hidden;">
		<table class="common_table_list">
		<?php
			foreach($PreviewChoice as $aPreviewChoice){
				echo "<tr onclick='selectRow(this)' ><td>".$aPreviewChoice[1]."</td><td><input type='checkbox' onclick='event.stopPropagation();updateSelectedNum();' name='applicant_id[]' value='".$aPreviewChoice[0]."'></input></td></tr>";
			}
		?>
		</table>
		</div>
	  <p class="spacer"></p>
   </div>
	<p class="spacer"></p>
	<div class="edit_bottom">
    	<input id="confirmResendBtn" name="confirmResendBtn" type="button" class="formsubbutton" value="<?=$kis_lang['submit']?>" />         
       	<input name="cancelResendBtn" type="button" class="formsubbutton" onclick="unselectAllApplicants();parent.$.fancybox.close();" value="<?=$kis_lang['close']?>" />
  	</div>
</div>

<div id="PrintOptionLayer" style="padding:5px;display:none;" class="pop_edit">
	<div class="table_board">
		<table class="form_table">
			<tr>
               <td width="30%" class="field_title"><?=$kis_lang['applicant']?></td>
               <td width="70%">
               		<select id="SendTargetForPrint" name="SendTargetForPrint">
               			<option value="1" seelcted="selected"><?=$kis_lang['allapplicants']?>(<?=count($kis_data['emailReceivers'])?>)</option>
               			<!--<option value="2"><?=$kis_lang['applicantssuccesslasttime']?></option>-->
               			<option value="3"><?=$kis_lang['applicantsfailedlasttime']?>(<?=$failCount?>)</option>
               		<?php 
               			if($kis_data['recordID']!='' && $kis_data['emailRecordDetail']['IsAcknowledge']==1){
               				echo '<option value="4">'.$kis_lang['applicantshavenotacknowledged'].'('.$notAckCount.')</option>';	
               			}
               		?>
               			<option value="5"><?=$kis_lang['selectapplicants']?>(0)</option>
               		</select>
               </td>
             </tr> 
		</table>
		<div id="DivSelectApplicantsForPrint" style="display:none;height: 240px;overflow: scroll;overflow-x: hidden;">
		<table class="common_table_list">
		<?php
			foreach($PreviewChoice as $aPreviewChoice){
				echo "<tr onclick='selectRow(this)' ><td>".$aPreviewChoice[1]."</td><td><input type='checkbox' onclick='event.stopPropagation();updateSelectedNum();' name='applicant_id[]' value='".$aPreviewChoice[0]."'></input></td></tr>";
			}
		?>
		</table>
		</div>
	  <p class="spacer"></p>
   </div>
	<p class="spacer"></p>
	<div class="edit_bottom">
    	<input id="confirmPrintBtn" name="confirmPrintBtn" type="button" class="formsubbutton" value="<?=$kis_lang['print']?>" />         
       	<input name="cancelResendBtn" type="button" class="formsubbutton" onclick="unselectAllApplicants();parent.$.fancybox.close();" value="<?=$kis_lang['close']?>" />
  	</div>
</div>

<style type="text/css">
#ProgressContainer { 
	position: fixed;
	left: 50%;
	top: 50%;
	width: 480px;
	height: 120px; 
	margin: auto;
	margin-left: -240px;
	margin-bottom: -60px;
	padding: 30px 50px 10px 50px;
	background-color:#FFF;
	text-align: center;
	vertical-align: middle;
	z-index: 1000000;
	border: 1px solid black;
	display: none;
}
</style>
<script language="JavaScript" src="/templates/jquery/jquery.progressbar.min.js"></script>
<script type="text/javascript" language="JavaScript">
$(document).ready(function() {
	$("#progressbarA").progressBar({barImage: '/images/progress_bar_green.gif'} );
});
function updateSelectedNum(){
	var checkboxes = document.getElementsByName('applicant_id[]');
	var vals = 0;
	for (var i=0, n=checkboxes.length;i<n;i++){
		if(checkboxes[i].checked){
			vals++;
		}
	}
	$('select option:contains("<?=$kis_lang['selectapplicants']?>")').text('<?=$kis_lang['selectapplicants']?>('+vals+')');
}
function unselectAllApplicants(){
	$("input[name='applicant_id[]']").each(function(){
		this.checked = false;
	});
	updateSelectedNum();
}
function selectRow(row)
{
    var firstInput = row.getElementsByTagName('input')[0];
    firstInput.checked = !firstInput.checked;
    updateSelectedNum();
}
</script>
<div id="ProgressContainer">
	<div id="ProgressMessage"><?=$Lang['MassMailing']['Start2Send']?></div>
	<table align="center">
		<tr><td width="240" align="center"><span class="progressBar" id="progressbarA">0%</span></td></tr>
	</table>
	<div id="ProgressCancel"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "", "ProgressCancelBtn")?></div>
	<div id="ProgressClose" style="display:none"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "", "ProgressCloseBtn")?></div>
</div>