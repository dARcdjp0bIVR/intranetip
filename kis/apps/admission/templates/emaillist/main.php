<?php
// Editing by 
?>
<script>
kis.admission.email_list_init({
	msg_confirm_delete_email_list:'<?=$kis_lang['Admission']['msg']['confirm_delete_email_list']?>',
	msg_selectatleastonerecord:'<?=$kis_lang['Admission']['msg']['selectatleastonerecord']?>',
	exportallrecordsornot:'<?=$kis_lang['Admission']['msg']['exportallrecordsornot']?>'
});
</script>
<div class="main_content_detail">
 	 <p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	<div class="Content_tool">
	    <div class="btn_option"  id="ExportDiv">
			<a id="btn_import" class="tool_export export" href="javascript:void(0);"><?=$kis_lang['exportsendingrecord']?></a>
		</div>  
	</div>
	<form class="filter_form"> 
    	<div id="table_filter">
        	<?=$schoolYearSelection?>
        	<?=$classLevelSelection?>
   		</div>	
		<div class="search">
		    <input placeholder="<?=$kis_lang['search'] ?>" name="keyword" value="<?=$keyword?>" type="text"/>
		</div>
    </form>
      
                     <!---->
                    	
                      <p class="spacer"></p>
                      	  <div class="table_board">
                      	  <div class="common_table_tool common_table_tool_table">
				<a href="#" class="tool_delete"><?=$kis_lang['delete']?></a>
            </div>
            <p class="spacer"></p>
             <form id="email_list" name="email_list" method="post">
                      <table class="common_table_list edit_table_list">
                        <colgroup><col nowrap="nowrap">
                        </colgroup><thead>
                          <tr>
                            <th width="20">&nbsp;</th>
                            <th><?=$kis_lang['form']?></th>
                            <th><?=$kis_lang['subject']?> </th>
                            <th><?=$kis_lang['noofrecipients']?></th>
                            <th><?=$kis_lang['confirmedcount']?></th>
                            <th><?=$kis_lang['sentby']?></th>
                            <th><?=$kis_lang['sentdate']?></th>
                            <th><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,'emailAry[]'):setChecked(0,this.form,'emailAry[]')"></th>
                          </tr>
                        </thead>
                        <tbody>
                  	<? 
                  		$idx = 0;
			      		foreach($emailListAry as $_classLevelId => $_classLevelRecord): 
			      			//foreach($_classLevelAry as $_classLevelRecord){
			      				$idx++;	      			
			      	?>
							 <tr>
	                            <td><?=$idx?></td>
	                            <td><?=$_classLevelRecord['ClassLevelName']?></td>
	                            <td><a href="#/apps/admission/emaillist/edit/?recordID=<?=$_classLevelRecord['RecordID']?>"><?=kis_ui::displayTableField($_classLevelRecord['Subject'])?></a></td>
	                            <td><a href="javascript:void(0);" id="emaillist_ViewStudentBtn_<?=$_classLevelRecord['RecordID']?>" name="emaillist_ViewStudentBtn"><?=kis_ui::displayTableField($_classLevelRecord['NoOfSuccessSent'] . " / " . $_classLevelRecord['NoOfRecipient'])?></a></td>
	                            <td><?=$_classLevelRecord['IsAcknowledge']?$_classLevelRecord['NoOfAcknowledged']:'--'?></td>
	                            <td><?=kis_ui::displayTableField($_classLevelRecord['LastSentBy'])?></td>
	                            <td><?=kis_ui::displayTableField($_classLevelRecord['LastSentDate'])?></td>
	                            <td><?=$libinterface->Get_Checkbox('email_'.$_classLevelRecord['RecordID'], 'emailAry[]', $_classLevelRecord['RecordID'], '', $Class='', $Display='', $Onclick="unset_checkall(this, document.getElementById('email_list'));", $Disabled='')?></td>
	                          </tr>
					<? 		//}
						endforeach; 
					if($idx == 0){?>
                  		<tr>
                  			<td colspan="7" style="text-align:center;"><?=$kis_lang['norecord']?></td>
                  		</tr>  
					<?}?>	  
                        </tbody>
                      </table>
                      <input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$schoolYearID?>">
					</form>
                      <p class="spacer"></p>
                      <p class="spacer"></p>
                      <br>
                    </div></div>