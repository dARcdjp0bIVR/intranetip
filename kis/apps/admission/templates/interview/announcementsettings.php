<?php
// Editing by 
/****************************************
 *	2017-09-13 (Henry):	created thi page
 ****************************************/
?>
<style type="text/css">
table.common_table_list tr.move_selected td {
	background-color:#fbf786; border-top: 2px dashed #d3981a; border-bottom: 2px dashed #d3981a
}
</style>
<script type="text/javascript">
kis.admission.interview_arrangement_init = function(msg){
	var msg_confirm_delete_email_template = msg.confirm_delete_email_template;
	
	var sysmsg = msg.returnmsg;
	if(sysmsg!=''){
		var returnmsg = sysmsg.split('|=|')[1];
		returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+msg.close+']</a>';
		var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
		$('div#system_message').removeClass();
		$('div#system_message').addClass(msgclass).show();
		$('div#system_message span').html(returnmsg);
		setTimeout(function(){
			$('#system_message').hide();
		}, 3000);	
	}
	
	$('a.delete_dim').click(function(){
		var thisObj = $(this);
		var deleteRecordId = thisObj.find('input[name="TemplateID[]"]').val();
		
		if(confirm(msg_confirm_delete_email_template)){
			$.post('apps/admission/ajax.php?action=deleteInterviewArrangementRecords', 
			{
				'TemplateID[]': [deleteRecordId]
			}, 
			function(success){
				$.address.value('/apps/admission/interview/timeslotsettings/?sysMsg='+success + '&r=' + (new Date()).getTime());
			});
		}
	});
	
	$('select#RecordType').change(function(){
		$('#filter_form').submit();
	});
	
	$('select#selectInterviewRound').change(function(){
		$.address.value('/apps/admission/interview/timeslotsettings/'+(this.value?this.value:1)+'/');
	});
	
	var tableObj = $(".common_table_list");
	if(tableObj.length > 0 && tableObj.tableDnD){
		tableObj.tableDnD({
			onDrop: function(table, DroppedRow) {
				var recordIdObj = document.getElementsByName('TemplateID[]');
				var recordIdAry = [];
				for(var i=0;i<recordIdObj.length;i++){
					recordIdAry.push(recordIdObj[i].value);
				}
				var id_list = recordIdAry.join();
				if(this.IdList == id_list) return;
				
				$.post(
					'apps/admission/ajax.php?action=reorderInterviewArrangementRecords',
					{
						'TemplateID[]': recordIdAry
					},
					function(success){
						$.address.value('/apps/admission/interview/timeslotsettings/?sysMsg='+success + '&r=' + (new Date()).getTime());
					}
				);
			},
			onDragStart: function(table, DraggedCell) {
				var recordIdObj = document.getElementsByName('TemplateID[]');
				var recordIdAry = [];
				for(var i=0;i<recordIdObj.length;i++){
					recordIdAry.push(recordIdObj[i].value);
				}
				this.IdList = recordIdAry.join();
			},
			dragHandle: "Dragable", 
			onDragClass: "move_selected"
		});
	}	
};

$(function(){
	kis.admission.interview_arrangement_init({
		'close':'<?=$kis_lang['close']?>',
		'delete':'<?=$kis_lang['delete']?>',
		'confirm_delete_email_template':'<?=$kis_lang['msg']['deleteinterviewarrangement']?>',
		'returnmsg':'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>'
	});
});
</script>
<div class="main_content_detail">
	<?php
    if (method_exists($lauc, 'getAdminModuleTab')) {
        kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), 'announcementsettings', '#/apps/admission/interview/');
    } elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), 'announcementsettings', '#/apps/admission/interview/');
    } else if ($sys_custom['KIS_Admission']['InterviewArrangement'] || $moduleSettings['enableinterviewarrangement']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'timeslotsettings', 'announcementsettings'), 'announcementsettings', '#/apps/admission/interview/');
    }
    ?>
    <p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	<div class="common_table_tool">
    	<a class="tool_edit" href="#/apps/admission/interview/announcementsettings_edit/"><?=$kis_lang['edit']?></a>
  	</div>
	<table class="form_table">
	     <tbody>
	     <?if($moduleSettings['enableinterviewform']){?>
	     <tr> 
	       <td width="30%" class="field_title"><?=$kis_lang['enableAnnouncement']?></td>
	       <td width="40%"><?=$basicSettings['enableInterviewAnnouncement']>0?$kis_lang['yes']:$kis_lang['no']?></td>
	     </tr>
	     <?}?>
	     <tr> 
	       <td width="30%" class="field_title"><?=($sys_custom['KIS_Admission']['InterviewSettings'] || $moduleSettings['enableinterviewform']?sprintf($sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings']?$kis_lang['HKUGAPS']['announceroundinterviewresult']:$kis_lang['announceroundinterviewTimeslot'], '1'):$kis_lang['announceinterviewtimeslot'])?></td>
	       <?if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){
	           if(str_replace(' ', '', $basicSettings['firstInterviewAnnouncementStart']) == "00:00" && str_replace(' ', '', $basicSettings['firstInterviewAnnouncementEnd']) == "00:00"){
	               $firstInterviewAnnouncement = " -- ";
	           }else{
	               $firstInterviewAnnouncement = $basicSettings['firstInterviewAnnouncementStart'] . " ~ " . $basicSettings['firstInterviewAnnouncementEnd'];
	           }
	       ?>
	       <td width="40%"><?=$firstInterviewAnnouncement?></td>
	       <?}else{?>
	       <td width="40%"><?=$basicSettings['firstInterviewAnnouncement']>0?$kis_lang['yes']:$kis_lang['no']?></td>
	       <?}?>
	     </tr>
	     <?if($sys_custom['KIS_Admission']['InterviewSettings'] || $moduleSettings['enableinterviewform']){?>
	     <tr>
	       <td class="field_title"><?=sprintf($sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings']?$kis_lang['HKUGAPS']['announceroundinterviewresult']:$kis_lang['announceroundinterviewTimeslot'], '2')?></td>
	       <?if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){
	           if(str_replace(' ', '', $basicSettings['secondInterviewAnnouncementStart']) == "00:00" && str_replace(' ', '', $basicSettings['secondInterviewAnnouncementEnd']) == "00:00"){
	               $secondInterviewAnnouncement = " -- ";
	           }else{
	               $secondInterviewAnnouncement = $basicSettings['secondInterviewAnnouncementStart'] . " ~ " . $basicSettings['secondInterviewAnnouncementEnd'];
	           }
	       ?>
	       <td width="40%"><?=$secondInterviewAnnouncement?></td>
	       <?}else{?>
	       <td><?=$basicSettings['secondInterviewAnnouncement']>0?$kis_lang['yes']:$kis_lang['no']?></td>
	       <?}?>
	     </tr>
	     <tr>
	       <td class="field_title"><?=sprintf($sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings']?$kis_lang['HKUGAPS']['announceroundinterviewresult']:$kis_lang['announceroundinterviewTimeslot'], '3')?></td>
	       <?if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){
	           if(str_replace(' ', '', $basicSettings['thirdInterviewAnnouncementStart']) == "00:00" && str_replace(' ', '', $basicSettings['thirdInterviewAnnouncementEnd']) == "00:00"){
	               $thirdInterviewAnnouncement = " -- ";
	           }else{
	               $thirdInterviewAnnouncement = $basicSettings['thirdInterviewAnnouncementStart'] . " ~ " . $basicSettings['thirdInterviewAnnouncementEnd'];
	           }
	       ?>
	       <td width="40%"><?=$thirdInterviewAnnouncement?></td>
	       <?}else{?>
	       <td><?=$basicSettings['thirdInterviewAnnouncement']>0?$kis_lang['yes']:$kis_lang['no']?></td>
	       <?}?>
	     </tr>
	     <?}?>
	     <?if($sys_custom['KIS_Admission']['STCC']['Settings'] || $sys_custom['KIS_Admission']['UCCKE']['Settings'] || $sys_custom['KIS_Admission']['KTLMSKG']['Settings'] || $sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['HKUGAC']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings'] || $sys_custom['KIS_Admission']['STANDARD']['Settings']){?>
	     <tr>
	       <td class="field_title"><?=$sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['HKUGAC']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings'] || $sys_custom['KIS_Admission']['STANDARD']['Settings']?$kis_lang['HKUGAPS']['announceroundapplicationresult']:$kis_lang['announceroundapplicationresult']?></td>
	       <?if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){
	           if(str_replace(' ', '', $basicSettings['applicationStatusAnnouncementStart']) == "00:00" && str_replace(' ', '', $basicSettings['applicationStatusAnnouncementEnd']) == "00:00"){
	               $applicationStatusAnnouncement = " -- ";
	           }else{
	               $applicationStatusAnnouncement = $basicSettings['applicationStatusAnnouncementStart'] . " ~ " . $basicSettings['applicationStatusAnnouncementEnd'];
	           }
	       ?>
	       <td><?=$applicationStatusAnnouncement?></td>
	       <?}else{?>
	       <td><?=$basicSettings['applicationStatusAnnouncement']>0?$kis_lang['yes']:$kis_lang['no']?></td>
	       <?}?>
	     </tr>
	     <?}?>
	     <?if($sys_custom['KIS_Admission']['STCC']['Settings'] || $sys_custom['KIS_Admission']['UCCKE']['Settings']){?>
	     <tr>
	       <td class="field_title"><?=$kis_lang['announceroundapplicationresultstartdate']?></td>
	       <td><?=$basicSettings['applicationStatusAnnouncementStartDate']!=""?substr($basicSettings['applicationStatusAnnouncementStartDate'], 0, -3):'--'?></td>
	     </tr>
	     <?}?>
	     <?if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){?>
		 <tr>
		 	<td class="field_title"><?=$kis_lang['adminMode']?></td>
		 	<td><?=$basicSettings['adminModeActive'] == 1?$kis_lang['yes']:$kis_lang['no'] ?></td>
		 </tr>
		 <?}?>
	   </tbody></table>
</div>
