<?php
// Editing by 
/****************************************
 *	2015-07-20 (Henry):	added Interview Arrangement tab
 ****************************************/
include_once($intranet_root."/lang/lang.".$intranet_session_language.".php");

?>
<script type="text/javascript">
kis.admission.interview_arrangement_init = function(){
	kis.datepicker('#applicationStatusAnnouncementStartDate_date');

	kis.datepicker('#firstInterviewAnnouncementStart, #firstInterviewAnnouncementEnd');
	kis.datepicker('#secondInterviewAnnouncementStart, #secondInterviewAnnouncementEnd');
	kis.datepicker('#thirdInterviewAnnouncementStart, #thirdInterviewAnnouncementEnd');
	kis.datepicker('#applicationStatusAnnouncementStart, #applicationStatusAnnouncementEnd');

	if($('#adminMessage').length){
		var editor = kis.editor('adminMessage');
	}
	
	//checking for update period
	$('#applicationStatusAnnouncementStartDate_date').keyup(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = '<?=$kis_lang['Admission']['msg']['invaliddateformat']?>';
		}
		$('span#warning_applicationStatusAnnouncementStartDate_date').html(warning);
	});
	$('#applicationStatusAnnouncementStartDate_date').change(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = '<?=$kis_lang['Admission']['msg']['invaliddateformat']?>';
		}
		
		$('span#warning_applicationStatusAnnouncementStartDate_date_date').html(warning);
	});

	$('#submitBtn').click(function(){
		checkDate();
	});
		
	$('#application_settings').submit(function(){
		if(editor){
			editor.finish();
		}
		
				$.post('apps/admission/ajax.php?action=updateInterviewAnnouncementSettings', $(this).serialize(), function(success){
					$.address.value('/apps/admission/interview/announcementsettings/?sysMsg='+success);
				});
		    return false;
		}); 	
	$('input#cancelBtn').click(function(){
		$.address.value('/apps/admission/interview/announcementsettings/');
	});

};
$(function(){
	kis.admission.interview_arrangement_init();
});

function checkDate(){
    var invaliddateformat ='<?=$kis_lang['Admission']['msg']['invaliddateformat']?>';
    var enddateearlier = '<?=$kis_lang['Admission']['msg']['enddateearlier']?>';
    var emptyapplicationdatewarning = '<?=$kis_lang['Admission']['msg']['emptyapplicationdatewarning']?>';
    var selectatleastonetimeslot = '<?=$kis_lang['Admission']['msg']['selectatleastonetimeslot']?>';

    $('#firstInterviewAnnouncementStart').keyup(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('firstInterviewAnnouncementStart', 'firstStartTime_hour :selected', 'firstStartTime_min :selected', '', 'firstInterviewAnnouncementEnd', 'firstEndTime_hour :selected', 'firstEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_date_1').html(warning);
	});
	$('#firstInterviewAnnouncementStart').change(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('firstInterviewAnnouncementStart', 'firstStartTime_hour :selected', 'firstStartTime_min :selected', '', 'firstInterviewAnnouncementEnd', 'firstEndTime_hour :selected', 'firstEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_date_1').html(warning);
	});
	
	$('#firstInterviewAnnouncementEnd').keyup(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('firstInterviewAnnouncementStart', 'firstStartTime_hour :selected', 'firstStartTime_min :selected', '', 'firstInterviewAnnouncementEnd', 'firstEndTime_hour :selected', 'firstEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_date_1').html(warning);
	});
	$('#firstInterviewAnnouncementEnd').change(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('firstInterviewAnnouncementStart', 'firstStartTime_hour :selected', 'firstStartTime_min :selected', '', 'firstInterviewAnnouncementEnd', 'firstEndTime_hour :selected', 'firstEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_date_1').html(warning);
	});

	$('#secondInterviewAnnouncementStart').keyup(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('secondInterviewAnnouncementStart','secondStartTime_hour :selected','secondStartTime_min :selected', '', 'secondInterviewAnnouncementEnd','secondEndTime_hour :selected', 'secondEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_date_2').html(warning);
	});
	$('#secondInterviewAnnouncementStart').change(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('secondInterviewAnnouncementStart','secondStartTime_hour :selected','secondStartTime_min :selected', '', 'secondInterviewAnnouncementEnd','secondEndTime_hour :selected', 'secondEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_date_2').html(warning);
	});
	
	$('#secondInterviewAnnouncementEnd').keyup(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('secondInterviewAnnouncementStart','secondStartTime_hour :selected','secondStartTime_min :selected', '', 'secondInterviewAnnouncementEnd','secondEndTime_hour :selected', 'secondEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_date_2').html(warning);
	});
	$('#secondInterviewAnnouncementEnd').change(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('secondInterviewAnnouncementStart','secondStartTime_hour :selected','secondStartTime_min :selected', '', 'secondInterviewAnnouncementEnd','secondEndTime_hour :selected', 'secondEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_date_2').html(warning);
	});

	$('#thirdInterviewAnnouncementStart').keyup(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('thirdInterviewAnnouncementStart','thirdStartTime_hour :selected','thirdStartTime_min :selected', '', 'thirdInterviewAnnouncementEnd','thirdEndTime_hour :selected', 'thirdEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_date_3').html(warning);
	});
	$('#thirdInterviewAnnouncementStart').change(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('thirdInterviewAnnouncementStart','thirdStartTime_hour :selected','thirdStartTime_min :selected', '', 'thirdInterviewAnnouncementEnd','thirdEndTime_hour :selected', 'thirdEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_date_3').html(warning);
	});
	
	$('#thirdInterviewAnnouncementEnd').keyup(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('thirdInterviewAnnouncementStart','thirdStartTime_hour :selected','thirdStartTime_min :selected', '', 'thirdInterviewAnnouncementEnd','thirdEndTime_hour :selected', 'thirdEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_date_3').html(warning);
	});
	$('#thirdInterviewAnnouncementEnd').change(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('thirdInterviewAnnouncementStart','thirdStartTime_hour :selected','thirdStartTime_min :selected', '', 'thirdInterviewAnnouncementEnd','thirdEndTime_hour :selected', 'thirdEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_date_3').html(warning);
	});

	$('#applicationStatusAnnouncementStart').keyup(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('applicationStatusAnnouncementStart','appStatusStartTime_hour :selected','appStatusStartTime_min :selected', '', 'applicationStatusAnnouncementEnd','appStatusEndTime_hour :selected', 'appStatusEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_appStatus').html(warning);
	});
	$('#applicationStatusAnnouncementStart').change(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('applicationStatusAnnouncementStart','appStatusStartTime_hour :selected','appStatusStartTime_min :selected', '', 'applicationStatusAnnouncementEnd','appStatusEndTime_hour :selected', 'appStatusEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_appStatus').html(warning);
	});
	
	$('#applicationStatusAnnouncementEnd').keyup(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('applicationStatusAnnouncementStart','appStatusStartTime_hour :selected','appStatusStartTime_min :selected', '', 'applicationStatusAnnouncementEnd','appStatusEndTime_hour :selected', 'appStatusEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_appStatus').html(warning);
	});
	$('#applicationStatusAnnouncementEnd').change(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = invaliddateformat;
		}else if(this.value!=''&&compareTimeByObjId('applicationStatusAnnouncementStart','appStatusStartTime_hour :selected','appStatusStartTime_min :selected', '', 'applicationStatusAnnouncementEnd','appStatusEndTime_hour :selected', 'appStatusEndTime_min :selected', '')==false){
			warning = enddateearlier;
		}
		
		$('span#warning_appStatus').html(warning);
	});

	
		var start_date_1 = $('#firstInterviewAnnouncementStart').val();
		var end_date_1 = $('#firstInterviewAnnouncementEnd').val();

		var start_date_2 = $('#secondInterviewAnnouncementStart').val();
		var end_date_2 = $('#secondInterviewAnnouncementEnd').val();

		var start_date_3 = $('#thirdInterviewAnnouncementStart').val();
		var end_date_3 = $('#thirdInterviewAnnouncementEnd').val();

		var appStatusStart = $('#applicationStatusAnnouncementStart').val();
		var appStatusEnd = $('#applicationStatusAnnouncementEnd').val();
		
    	if(start_date_1!=''&&end_date_1!=''){
    		if(!check_date_without_return_msg(document.getElementById('firstInterviewAnnouncementStart'))){
    			$('span#warning_start_date_1').html(invaliddateformat);
    			$('#firstInterviewAnnouncementStart').focus();
    			return false;
    		}else if(!check_date_without_return_msg(document.getElementById('firstInterviewAnnouncementEnd'))){
    			$('span#warning_end_date_1').html(invaliddateformat);
    			$('#firstInterviewAnnouncementEnd').focus();
    			return false;
    		}else if(compareTimeByObjId('firstInterviewAnnouncementStart', 'firstStartTime_hour :selected', 'firstStartTime_min :selected', '', 'firstInterviewAnnouncementEnd', 'firstEndTime_hour :selected', 'firstEndTime_min :selected', '')==false){
    			$('span#warning_start_date_1').html(enddateearlier);
    			$('#firstInterviewAnnouncementStart').focus();
    			return false;
    		}
    	}else{
    		// miss either one of them
    		if(!confirm(emptyapplicationdatewarning)){
    				return false;
    		}
    	}

    	if(start_date_2!=''&&end_date_2!=''){
    		if(!check_date_without_return_msg(document.getElementById('secondInterviewAnnouncementStart'))){
    			$('span#warning_date_2').html(invaliddateformat);
    			$('#secondInterviewAnnouncementStart').focus();
    			return false;
    		}else if(!check_date_without_return_msg(document.getElementById('secondInterviewAnnouncementEnd'))){
    			$('span#warning_date_2').html(invaliddateformat);
    			$('#secondInterviewAnnouncementEnd').focus();
    			return false;
    		}else if(compareTimeByObjId('secondInterviewAnnouncementStart','secondStartTime_hour :selected','secondStartTime_min :selected', '', 'secondInterviewAnnouncementEnd','secondEndTime_hour :selected', 'secondEndTime_min :selected', '')==false){
    			$('span#warning_date_2').html(enddateearlier);
    			$('#secondInterviewAnnouncementStart').focus();
    			return false;
    		}
    	}else{
    		// miss either one of them
//     		if(!confirm(emptyapplicationdatewarning)){
    				
//     		}
    	}

    	if(start_date_3!=''&&end_date_3!=''){
    		if(!check_date_without_return_msg(document.getElementById('thirdInterviewAnnouncementStart'))){
    			$('span#warning_date_3').html(invaliddateformat);
    			$('#thirdInterviewAnnouncementStart').focus();
    			return false;
    		}else if(!check_date_without_return_msg(document.getElementById('thirdInterviewAnnouncementEnd'))){
    			$('span#warning_date_3').html(invaliddateformat);
    			$('#thirdInterviewAnnouncementEnd').focus();
    			return false;
    		}else if(compareTimeByObjId('thirdInterviewAnnouncementStart','thirdStartTime_hour :selected','thirdStartTime_min :selected', '', 'thirdInterviewAnnouncementEnd','thirdEndTime_hour :selected', 'thirdEndTime_min :selected', '')==false){
    			$('span#warning_date_3').html(enddateearlier);
    			$('#thirdInterviewAnnouncementStart').focus();
    			return false;
    		}
    	}else{
    		// miss either one of them
//     		if(!confirm(emptyapplicationdatewarning)){
    				
//     		}
    	}

    	if(appStatusStart!=''&&appStatusEnd!=''){
    		if(!check_date_without_return_msg(document.getElementById('applicationStatusAnnouncementStart'))){
    			$('span#warning_appStatus').html(invaliddateformat);
    			$('#applicationStatusAnnouncementStart').focus();
    			return false;
    		}else if(!check_date_without_return_msg(document.getElementById('applicationStatusAnnouncementEnd'))){
    			$('span#warning_appStatus').html(invaliddateformat);
    			$('#applicationStatusAnnouncementEnd').focus();
    			return false;
    		}else if(compareTimeByObjId('applicationStatusAnnouncementStart','appStatusStartTime_hour :selected','appStatusStartTime_min :selected', '', 'applicationStatusAnnouncementEnd','appStatusEndTime_hour :selected', 'appStatusEndTime_min :selected', '')==false){
    			$('span#warning_appStatus').html(enddateearlier);
    			$('#applicationStatusAnnouncementStart').focus();
    			return false;
    		}
    	}else{
    		// miss either one of them
    		if(!confirm(emptyapplicationdatewarning)){
    				return false;
    		}
    	}
	
	$('#application_settings').submit();
}

</script>
<div class="main_content_detail">
    <?php
    if (method_exists($lauc, 'getAdminModuleTab')) {
        kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), 'announcementsettings', '#/apps/admission/interview/');
    } elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), 'announcementsettings', '#/apps/admission/interview/');
    } else if ($sys_custom['KIS_Admission']['InterviewArrangement'] || $moduleSettings['enableinterviewarrangement']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'timeslotsettings', 'announcementsettings'), 'announcementsettings', '#/apps/admission/interview/');
    }
    ?>
<p class="spacer"></p>
<form id="application_settings">
	<?=$kis_data['NavigationBar']?>
	<p class="spacer"></p>
		<table id="FormTable" class="form_table">
        	<tbody>
	        	<?if($moduleSettings['enableinterviewform']){?>
			    <tr> 
			       <td width="30%" class="field_title"><?=$kis_lang['enableAnnouncement']?></td>
			       <td width="40%"><input type="checkbox" id="enableInterviewAnnouncement" name="enableInterviewAnnouncement" value="1" <?=($basicSettings['enableInterviewAnnouncement']==1?'checked=1':'')?>"></td>
			    </tr>
			    <?}?>
                <tr> 
			       <td width="30%" class="field_title"><?=($sys_custom['KIS_Admission']['InterviewSettings'] || $moduleSettings['enableinterviewform']?sprintf($sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings']?$kis_lang['HKUGAPS']['announceroundinterviewresult']:$kis_lang['announceroundinterviewTimeslot'], '1'):$kis_lang['announceinterviewtimeslot'])?></td>
			     <?if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){?>
			    <?php
    			    list ($firstStartDate, $firstStartTime) = explode(" ", $basicSettings['firstInterviewAnnouncementStart']);
    			    list ($firstEndDate, $firstEndTime) = explode(" ", $basicSettings['firstInterviewAnnouncementEnd']);
                ?>
			       <td width="40%"><input type="text" id="firstInterviewAnnouncementStart" name="firstInterviewAnnouncementStart" value="<?=$firstStartDate?>"><?=$libadmission->Get_Time_Selection($libinterface,'firstStartTime',$firstStartTime,'',false)?> ~ 
			       <input type="text" id="firstInterviewAnnouncementEnd" name="firstInterviewAnnouncementEnd" value="<?=$firstEndDate?>"><?=$libadmission->Get_Time_Selection($libinterface,'firstEndTime',$firstEndTime,'',false)?>
			       &nbsp;<span class="error_msg" id="warning_date_1"></span></td>
			     <?}else{?>
			       <td width="40%"><input type="checkbox" id="firstInterviewAnnouncement" name="firstInterviewAnnouncement" value="1" <?=($basicSettings['firstInterviewAnnouncement']==1?'checked=1':'')?>"></td>
			     <?}?>
			     </tr>
			     <?if($sys_custom['KIS_Admission']['InterviewSettings'] || $moduleSettings['enableinterviewform']){?>
			     <tr>
			       <td class="field_title"><?=sprintf($sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings']?$kis_lang['HKUGAPS']['announceroundinterviewresult']:$kis_lang['announceroundinterviewTimeslot'], '2')?></td>
			     <?if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){?>
			    <?php
			        list ($secondStartDate, $secondStartTime) = explode(" ", $basicSettings['secondInterviewAnnouncementStart']);
    			    list ($secondEndDate, $secondEndTime) = explode(" ", $basicSettings['secondInterviewAnnouncementEnd']);
                ?>
			       <td width="40%"><input type="text" id="secondInterviewAnnouncementStart" name="secondInterviewAnnouncementStart" value="<?=$secondStartDate?>"><?=$libadmission->Get_Time_Selection($libinterface,'secondStartTime',$secondStartTime,'',false)?> ~
			       <input type="text" id="secondInterviewAnnouncementEnd" name="secondInterviewAnnouncementEnd" value="<?=$secondEndDate?>"><?=$libadmission->Get_Time_Selection($libinterface,'secondEndTime',$secondEndTime,'',false)?>
			       &nbsp;<span class="error_msg" id="warning_date_2"></span></td>
			     <?}else{?>
			       <td><input type="checkbox" id="secondInterviewAnnouncement" name="secondInterviewAnnouncement" value="1" <?=($basicSettings['secondInterviewAnnouncement']==1?'checked=1':'')?>"></td>
			     <?}?>
			     </tr>
			     <tr>
			       <td class="field_title"><?=sprintf($sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings']?$kis_lang['HKUGAPS']['announceroundinterviewresult']:$kis_lang['announceroundinterviewTimeslot'], '3')?></td>
			     <?if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){?>
			    <?php
    			    list ($thirdStartDate, $thirdStartTime) = explode(" ", $basicSettings['thirdInterviewAnnouncementStart']);
    			    list ($thirdEndDate, $thirdEndTime) = explode(" ", $basicSettings['thirdInterviewAnnouncementEnd']);
                ?>
			       <td width="40%"><input type="text" id="thirdInterviewAnnouncementStart" name="thirdInterviewAnnouncementStart" value="<?=$thirdStartDate?>"><?=$libadmission->Get_Time_Selection($libinterface,'thirdStartTime',$thirdStartTime,'',false)?> ~
			       <input type="text" id="thirdInterviewAnnouncementEnd" name="thirdInterviewAnnouncementEnd" value="<?=$thirdStartDate?>"><?=$libadmission->Get_Time_Selection($libinterface,'thirdEndTime',$thirdEndTime,'',false)?>
			       &nbsp;<span class="error_msg" id="warning_date_3"></span></td>
			     <?}else{?>
			       <td><input type="checkbox" id="thirdInterviewAnnouncement" name="thirdInterviewAnnouncement" value="1" <?=($basicSettings['thirdInterviewAnnouncement']==1?'checked=1':'')?>"></td>
			     <?}?>
			     </tr>
			     <?}?>
			     <?if($sys_custom['KIS_Admission']['STCC']['Settings'] || $sys_custom['KIS_Admission']['UCCKE']['Settings'] || $sys_custom['KIS_Admission']['KTLMSKG']['Settings']||$sys_custom['KIS_Admission']['HKUGAPS']['Settings'] ||$sys_custom['KIS_Admission']['HKUGAC']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings'] || $sys_custom['KIS_Admission']['STANDARD']['Settings']){?>
			     <tr>
			       <td class="field_title"><?=$sys_custom['KIS_Admission']['HKUGAPS']['Settings'] ||$sys_custom['KIS_Admission']['HKUGAC']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings'] || $sys_custom['KIS_Admission']['STANDARD']['Settings']?$kis_lang['HKUGAPS']['announceroundapplicationresult']:$kis_lang['announceroundapplicationresult']?></td>
			     <?if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){?>
			    <?php
    			    list ($appStatusStartDate, $appStatusStartTime) = explode(" ", $basicSettings['applicationStatusAnnouncementStart']);
    			    list ($appStatusEndDate, $appStatusEndTime) = explode(" ", $basicSettings['applicationStatusAnnouncementEnd']);
                ?>
			       <td width="40%"><input type="text" id="applicationStatusAnnouncementStart" name="applicationStatusAnnouncementStart" value="<?=$appStatusStartDate?>"><?=$libadmission->Get_Time_Selection($libinterface,'appStatusStartTime',$appStatusStartTime,'',false)?> ~
			       <input type="text" id="applicationStatusAnnouncementEnd" name="applicationStatusAnnouncementEnd" value="<?=$appStatusEndDate?>"><?=$libadmission->Get_Time_Selection($libinterface,'appStatusEndTime',$appStatusEndTime,'',false)?>
			       &nbsp;<span class="error_msg" id="warning_appStatus"></span></td>    
			     <?}else{?>
			       <td><input type="checkbox" id="applicationStatusAnnouncement" name="applicationStatusAnnouncement" value="1" <?=($basicSettings['applicationStatusAnnouncement']==1?'checked=1':'')?>"></td>
			      <?}?>
			     </tr>
			     <?}?>
			     <?if($sys_custom['KIS_Admission']['STCC']['Settings'] || $sys_custom['KIS_Admission']['UCCKE']['Settings']){?>
			     <tr>
			       <td class="field_title"><?=$kis_lang['announceroundapplicationresultstartdate']?></td>
			       <td><input type="text" name="applicationStatusAnnouncementStartDate_date"
							id="applicationStatusAnnouncementStartDate_date" value="<?=substr($basicSettings['applicationStatusAnnouncementStartDate'],0,-9)?>">&nbsp;<span
							class="error_msg" id="warning_applicationStatusAnnouncementStartDate_time"></span><?=$libadmission->Get_Time_Selection($libinterface,'applicationStatusAnnouncementStartDate_time',substr($basicSettings['applicationStatusAnnouncementStartDate'],-8),'',false)?></td>
			     </tr>
			     <?}?>
			     <?if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){?>
			     <tr>
			     	<td><label><input type="checkbox" id="adminModeActive" name="adminModeActive" value="1" <?=$basicSettings['adminModeActive']==1?"checked":""?>><?=$kis_lang['adminMode']?></label></td>
			     </tr>
			     <tr>
			     	<td colspan="2"><textarea rows="20px" cols="100px" style="resize:none;" id="adminMessage" name="adminMessage"><?=$basicSettings['adminMessage']?></textarea></td>
			     </tr>
			     <?}?>
            </tbody>
        </table>
        <div class="edit_bottom">
        	<input type="hidden" name="schoolYearId" id="schoolYearId" value="<?=$schoolYearID?>">
        	<?if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){?>
        	<input type="button" name="submitBtn" id="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
        	<?}else{?>
			<input type="submit" name="submitBtn" id="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
			<?}?>
            <input type="button" name="cancelBtn" id="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
	    </div>
</form>
</div>