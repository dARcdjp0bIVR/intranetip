<?php
// Editing by Henry

/**
 *	2015-08-04 (Henry): Created file
 **/
?>
<?php
list($page,$amount,$total,$sortby,$order,$keyword) = $kis_data['PageBar']; 
?>
<script type="text/javascript" language="JavaScript">
$(function(){
	kis.admission.interview_details_add_init({
		msg_selectatleastonerecord:'<?=$kis_lang['msg']['selectonlyonerecord']?>',
		msg_confirm_submit:'<?=$kis_lang['Admission']['msg']['suresubmit']?>'
	});
})
</script>
<div class="main_content_detail">
	<?php
    if (method_exists($lauc, 'getAdminModuleTab')) {
        kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), '', '#/apps/admission/interview/');
    } elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), '', '#/apps/admission/interview/');
    } else if ($sys_custom['KIS_Admission']['InterviewArrangement'] || $moduleSettings['enableinterviewarrangement']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'timeslotsettings', 'announcementsettings'), '', '#/apps/admission/interview/');
    }
    ?>
	<?=$kis_data['NavigationBar']?>
	<p class="spacer"></p>
	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
		<form class="filter_form"> 	
			<div class="search">
				<input placeholder="<?=$kis_lang['search'] ?>" name="keyword" value="<?=$keyword?>" type="text">
			</div>  
		</form>	
				<div id="table_filter">
					<form class="filter_form">
					<?=$applicationStatus?><?=$interviewStatusSelection?>
					<input type="hidden" name="selectSchoolYearID" id="selectSchoolYearID" value="<?=$schoolYearID?>"> 
					</form>  
				</div>
				<p class="spacer"></p>
		<form id="importForm" name="importForm" action="./apps/admission/ajax.php?action=importInterviewInfoByArrangement" method="post">
	  	    <table class="common_table_list edit_table_list">
			<colgroup><col nowrap="nowrap">
			</colgroup><thead>
				<tr>
				  <th width="20">#</th>
				  <th><?kis_ui::loadSortButton('application_id','applicationno', $sortby, $order)?></th>
				  <th><?kis_ui::loadSortButton('student_name','studentname', $sortby, $order)?></th>
				  <th><?=$kis_lang['parentorguardian']?></th>
				  <th><?=$kis_lang['phoneno']?></th>
				  <?=$sys_custom['KIS_Admission']['ICMS']['Settings']?'<th>'.$kis_lang['Admission']['icms']['applyDayType'].'</th>':''?>
				  <th><?kis_ui::loadSortButton('application_status','status', $sortby, $order)?></th>
				  <th><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,'applicationAry[]'):setChecked(0,this.form,'applicationAry[]')"></th>
			  </tr>
			</thead>
			<tbody>
				<?php 
					if($total>0){
						$idx=$amount*($page-1);
						foreach($applicationDetails as $_applicationId => $_applicationDetailsAry){
							$idx++;
							$_recordId = $_applicationDetailsAry['record_id'];
							$_status = $_applicationDetailsAry['application_status'];
							$_studentName = $_applicationDetailsAry['student_name'];		
							$_parentName = implode('<br/>',array_filter($_applicationDetailsAry['parent_name']));	
							$_parentPhone = implode('<br/>',array_filter($_applicationDetailsAry['parent_phone']));
							if($sys_custom['KIS_Admission']['ICMS']['Settings']){
								$classlevel = $applicationlistAry[$kis_data['classLevelID']]['ClassLevelName'];
								$_applyDayType = '<td>';
								for($i=1;$i<=3;$i++){
									if($_applicationDetailsAry['ApplyDayType'.$i])
										$_applyDayType .= '('.$i.') '.$kis_lang['Admission']['icms'][$classlevel]['TimeSlot'][$_applicationDetailsAry['ApplyDayType'.$i]].'<br/>';																				
								}
								$_applyDayType .= '</td>';
							}
							$tr_css = '';
							switch($_status){
								
								case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'waitingforinterview':$tr_css = ' class="absent"';break;
								case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'interviewed':$tr_css = ' class="waiting"';break;
								case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'admitted':$tr_css = ' class="done"';break;
								case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'notadmitted':$tr_css = ' class="draft"';break;
								
								case 'pending':$tr_css = ' class="absent"';break;
							//	case 'paymentsettled':$tr_css = '';break;
								case 'waitingforinterview':$tr_css = ' class="waiting"';break;
								case 'confirmed':$tr_css = ' class="done"';break;
								case 'cancelled':$tr_css = ' class="draft"';break;								
							}
				?>			
							<tr<?=$tr_css?>>
							  <td><?=$idx?></td>
							  <td><a href="#/apps/admission/applicantslist/details/<?=$schoolYearID?>/<?=$_recordId?>/"><?=$_applicationId?></a><br></td>
							  <td><?=$sys_custom['KIS_Admission']['ICMS']['Settings'] || $sys_custom['KIS_Admission']['CSM']['Settings'] || $sys_custom['KIS_Admission']['MUNSANG']['Settings']?str_replace(",", " ", $_studentName):$_studentName?></td>
							  <td><?=$_parentName?($sys_custom['KIS_Admission']['ICMS']['Settings']?str_replace(",", " ", $_parentName):$_parentName):'--'?></td>
							  <td nowrap="nowrap"><?=$_parentPhone?$_parentPhone:'--'?></td>
							  <?=$sys_custom['KIS_Admission']['ICMS']['Settings']?$_applyDayType:''?>
							  <td><?=$kis_lang['Admission']['Status'][$_status]?></td>
							  <td><?=$libinterface->Get_Checkbox('application_'.$_recordId, 'applicationAry[]', $_recordId, '', $Class='', $Display='', $Onclick="unset_checkall(this, document.getElementById('application_form'));", $Disabled='')?></td>
							 </tr>
				<?php } 
					}else{ //no record	
				?>		
							<tr>
							<?if($sys_custom['KIS_Admission']['ICMS']['Settings']){?>
							  <td colspan="8" style="text-align:center;"><?=$kis_lang['norecord']?></td>
							<?}else{?>
							  <td colspan="7" style="text-align:center;"><?=$kis_lang['norecord']?></td>
							<?}?>
							</tr>				
				<?php } ?>		
							</tbody>
						</table>
						<input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$schoolYearID?>">
						<input type="hidden" name="classLevelID" id="classLevelID" value="<?=$classLevelID?>">
					</form>
        <p class="spacer"></p>
		<? kis_ui::loadPageBar($page, $amount, $total, array(10, 20, 50, 100, 500, 1000, 1500)) ?>      
		<p class="spacer"></p><br>

		<div class="edit_bottom">
			<input type="hidden" name="round" id="round" value="<?=$selectInterviewRound?>" />
			<input type="hidden" name="recordID" id="recordID" value="<?=$recordID?>" />
        	<input type="button" name="submitBtn" id="submitBtn" class="formbutton" value="<?=$kis_lang['new']?>" />
        	<input type="button" name="cancelBtn" id="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
	    </div>
	    <!--<input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$schoolYearID?>" />-->
	    <input type="hidden" name="selectSchoolYearID" id="selectSchoolYearID" value="<?=$schoolYearID?>" />
        <p class="spacer"></p><br>  
	
</form>
</div>
