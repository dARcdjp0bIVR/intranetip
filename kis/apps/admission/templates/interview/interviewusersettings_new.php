<?php
// Editing by Henry
/****************************************
 *	2015-10-09 (Henry):	file created
 ****************************************/

if($kis_data['recordID']!=''){
	//$schoolYearID  = $kis_data['interviewSettingAry'][0]['SchoolYearID'];
	$classLevelID = $kis_data['interviewSettingAry'][0]['ClassLevelID'];
	$date = $kis_data['interviewSettingAry'][0]['Date'];
	$startTime = $kis_data['interviewSettingAry'][0]['StartTime'];
	$endTime = $kis_data['interviewSettingAry'][0]['EndTime'];
	$groupName = $kis_data['interviewSettingAry'][0]['GroupName'];
	$quota = $kis_data['interviewSettingAry'][0]['Quota'];
	$applied = $kis_data['interviewSettingAry'][0]['Applied'];
	$round = $kis_data['interviewSettingAry'][0]['Round'];
}

?>
<style type="text/css">
/******************* content div [start] ****************************/

#form_div ul, #form_tools_div ul { list-style-type: none; margin: 0; padding: 0; margin-bottom: 10px; min-height: 200px; }

#form_tools_div li { margin: 5px; padding: 5px; width: 200px;}

#form_div li { margin: 5px; padding: 5px; width: 250px;}

#form_div {
	float: left;
    border: 1px solid grey;
    padding: 0px; 
    width: 60%;
    min-height: 200px;    
}

#form_tools_div {
	float: left;
    /*border: 1px solid grey;*/
    padding: 0px; 
    width: 20%;
    /*height: 150px;*/    
}

.control-group-edit{
	display:none;
}

/******************* content div [end] ****************************/
</style>
<script type="text/javascript">

$(function(){
	kis.admission.interview_user_settings_new_init({
		invaliddateformat:'<?=$kis_lang['Admission']['msg']['invaliddateformat']?>',
		enddateearlier: '<?=$kis_lang['Admission']['msg']['enddateearlier']?>',
		emptyapplicationdatewarning: '<?=$kis_lang['Admission']['msg']['emptyinterviewformdatewarning']?>',
		selectatleastonetimeslot: '<?=$kis_lang['Admission']['msg']['selectatleastonetimeslot']?>',
		enterpositivenumber: '<?=$kis_lang['Admission']['msg']['enternumber']?>',
		quotashouldnotlessthanapplied: '<?=$kis_lang['Admission']['msg']['quotashouldnotlessthanapplied']?>',
		applied: '<?=$applied?>',
		entertitle: '<?=$kis_lang['msg']['enterTitle']?>',
		enterinputinterviewquestion: '<?=$kis_lang['msg']['enterinputinterviewquestion']?>',
		selectatleastoneuser: '<?=$kis_lang['msg']['selectatleastoneuser']?>'
	});
	
});

</script>
<div class="main_content">
	    <div class="main_content_detail">
	    	<?php
            if (method_exists($lauc, 'getAdminModuleTab')) {
                kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), 'interviewusersettings', '#/apps/admission/interview/');
            } elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
                kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), 'interviewusersettings', '#/apps/admission/interview/');
            }
            ?>
			<?=$kis_data['NavigationBar']?>
			<p class="spacer"></p>
			<form id='interview_user_settings_form' name='interview_user_settings_form'>
			<div class="table_board">
				<table class="form_table">
					<tr>
						<td class="field_title" style="width:30%"><?=$mustfillinsymbol?><?=$kis_lang['marker']?>
						</td>
						<td style="width:40%">
							<div style="width:100%" id='form_div' class='mail_to_list'>
							
							</div>
							<div class="mail_to_btn"><div class="mail_icon_form">
							<a href="#" class="btn_select_ppl"><?=$kis_lang['select']?>
							    <span class="target" style="display:none">recipients</span>
							</a>
						    </div></div>
						</td>
						<td style="width:30%">
						</td>
					</tr>     
			    </table>
				<p class="spacer"></p>
				<p class="spacer"></p><br />
	        </div>
	        </form>
	        <div class="text_remark"><?=$kis_lang['requiredfield']?></div>
			<div class="edit_bottom">
				<?if(!$hasFilled){?>
					<input type="button" id="submitBtn" name="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
				<?}?>
				<input type="button" id="cancelBtn" name="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
			</div>
		</div>
</div>

<form class='mail_select_user'>
    <h2><?=$kis_lang['findusers']?></h2>
    
    <?=$kis_lang['keyword']?>: <input type="text" name="keyword" style="float:right" value=""/>
    <p class="spacer"></p>
    <?=$kis_lang['group']?>: <select name="user_group" style="float:right" >
	<option value=""><?=$kis_lang['all']?> <?=$kis_lang['groups']?></option>
	<? foreach ($groups as $group): ?>
	<option value="<?=$group['group_id']?>"><?=$group['group_name_'.$lang]?></option>
	<? endforeach; ?>
    </select>
    <p class="spacer"></p>
    <?=$kis_lang['identity']?>:
    <!--<label for="user_type_4" style="padding-right:0px;"><?=$kis_lang['parent']?></label><input type="radio" id="user_type_4" name="user_type" value='4' />-->
    <label for="user_type_3" style="padding-right:0px;"><?=$kis_lang['staff']?></label><input type="radio" id="user_type_3" name="user_type" value='3' />
    <!--<label for="user_type_2" style="padding-right:0px;"><?=$kis_lang['student']?></label><input type="radio" id="user_type_2" name="user_type" value='2' />-->
    <label for="user_type_1" style="padding-right:0px;"><?=$kis_lang['teacher']?></label><input type="radio" id="user_type_1" name="user_type" checked value='1' />
    <input type="hidden" name="target" value=""/>
    <input type="hidden" name="exclude_list" value=""/>
    <p class="spacer"></p>
    <div class="button">  
	<input class="formbutton" value="<?=$kis_lang['search']?>" type="submit"/>
	<input class="formsubbutton" value="<?=$kis_lang['close']?>" type="submit"/>
    </div>
    
    <a class="mail_select_all" href="#"><?=$kis_lang['addall']?></a>
    <p class="spacer"></p>
    <div class="search_results">
    </div>
    
    
</form>