<?php
// Editing by Henry
/****************************************
 *	2015-07-20 (Henry):	added Interview Arrangement tab
 ****************************************/
include_once($intranet_root."/lang/lang.".$intranet_session_language.".php");

global $setting_path_ip_rel;
$linterface = $kis_data['libinterface'];

//$template_type_active = $kis_data['libadmission']->getEmailTemplateType('Active');
//$template_type_draft = $kis_data['libadmission']->getEmailTemplateType('Draft');

if(count($kis_data['interviewArrangementRecords'])>0){
	$is_edit = true;
}else{
	$is_edit = false;
}


?>
<style>
select:disabled, #groupType_customField.disabled label{color: #ccc;}
</style>
<script type="text/javascript">
kis.admission.interview_arrangement_init = function(msg){
	kis.datepicker('#Date');

	$('#Date').keyup(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = lang.invaliddateformat;
		}
		$('span#warning_interview_date').html(warning);
	});

	var msg_request_input_email_template_title = msg.request_input_email_template_title;
	var msg_request_input_email_template_content = msg.request_input_email_template_content;
	var msg_confirm_delete_email_template_attachment = msg.confirm_delete_email_template_attachment;

	var sysmsg = msg.returnmsg;
	if(sysmsg!=''){
		var returnmsg = sysmsg.split('|=|')[1];
		returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+msg.close+']</a>';
		var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
		$('div#system_message').removeClass();
		$('div#system_message').addClass(msgclass).show();
		$('div#system_message span').html(returnmsg);
		setTimeout(function(){
			$('#system_message').hide();
		}, 3000);
	}

	var checkEmailTemplateRecord = function(){
		var Date = $('#Date').val();
		var Quota = $('#Quota').val();
		var NumOfGroup = $('#NumOfGroup').val();

		//checking the date

		if(!check_date_without_return_msg(document.getElementById('Date'))){
			$('span#warning_Date').html(msg.invaliddateformat);
			$('#Date').focus();
			return false;
		}else if(compareTimeByObjId('Date','StartTime_hour :selected','StartTime_min :selected','StartTime_sec :selected','Date','EndTime_hour :selected', 'EndTime_min :selected','EndTime_sec :selected')==false){
			$('span#warning_StartTime').html(lang.enddateearlier);
			$('#StartTime').focus();
			return false;
		}else if(!is_positive_int(Quota)){
			alert(msg.enterpositivenumber);
			$('#Quota').focus();
			return false;
		}else if($('#groupTypeSetting_num').prop('checked') && !is_positive_int(NumOfGroup)){
			alert(msg.enterpositivenumber);
			$('#NumOfGroup').focus();
			return false;
		}else if($('#groupTypeSetting_custom').prop('checked') && $('input[name="customGroup\[\]"]:checked').length == 0){
			alert(<?=($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room')?'msg.selectatleastoneroom':'msg.selectatleastonegroup'?>);
			$('#groupTypeSetting_custom').focus();
			return false;
		}
		return true;
	};

	$('#AddTimeslot').click(function(){
		var x ='<tr><td style="width:230px">\
					<?=preg_replace('/\s+/', ' ', str_replace("'", "\'", $libadmission->Get_Time_Selection($linterface,'StartTime_{{{index}}}','','',false)))?>&nbsp;<span class="error_msg" id="warning_StartTime_{{{index}}}"></span> ~ \
	                <?=preg_replace('/\s+/', ' ', str_replace("'", "\'", $libadmission->Get_Time_Selection($linterface,'EndTime_{{{index}}}','','',false)))?>&nbsp;<span class="error_msg" id="warning_EndTime_{{{index}}}"></span>\
	                <?=preg_replace('/\s+/', ' ', str_replace("'", "\'", $linterface->Get_Form_Warning_Msg("WarnContent", $kis_lang['msg']['request_input_email_template_content'], $Class='', $display=false)))?>\
	            </td><td><div class="table_row_tool"><a title="<?=$kis_lang['Delete']?>" id="DeleteTimeslot" onClick="$(this).parent().parent().parent().remove();" class="delete_dim" href="javascript:void(0);"></a></td></tr>';
	    x = x.replace(/{{{index}}}/g, $('#NumOfTimeslot').val());
	    $(x).insertBefore($('#timeslot_table_last'));
	    $('#NumOfTimeslot').val(parseInt($('#NumOfTimeslot').val())+1);
	});

	$('input#submitBtn').click(function(){
		var formObj = $('form#MailForm');
		if(checkEmailTemplateRecord()){
			formObj.submit();
		}
	});

	$('input#cancelBtn').click(function(){
		$.address.value('/apps/admission/interview/timeslotsettings/<?=$selectInterviewRound?>/');
	});
};

$(function(){
	kis.admission.interview_arrangement_init({
		'close':'<?=$kis_lang['close']?>',
		'delete':'<?=$kis_lang['delete']?>',
		'request_input_email_template_title':'<?=$kis_lang['msg']['requestinputemailtemplatetitle']?>',
		'request_input_email_template_content':'<?=$kis_lang['msg']['request_input_email_template_content']?>',
		'confirm_delete_email_template_attachment':'<?=$kis_lang['msg']['confirm_delete_email_template_attachment']?>',
		'returnmsg':'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
		invaliddateformat:'<?=$kis_lang['Admission']['msg']['invaliddateformat']?>',
		enddateearlier: '<?=$kis_lang['Admission']['msg']['enddateearlier']?>',
		emptyapplicationdatewarning: '<?=$kis_lang['Admission']['msg']['emptyapplicationdatewarning']?>',
		selectatleastonetimeslot: '<?=$kis_lang['Admission']['msg']['selectatleastonetimeslot']?>',
		selectatleastoneroom: '<?=$kis_lang['Admission']['msg']['selectatleastoneroom']?>',
		selectatleastonegroup: '<?=$kis_lang['Admission']['msg']['selectatleastonegroup']?>',
		enterpositivenumber: '<?=$kis_lang['Admission']['msg']['enternumber']?>'
	});

	$('input[name="groupType"]').change(function(){
		if($('input[name="groupType"]:checked').val()=='custom'){
			$('#NumOfGroup').prop('disabled', true);
			$('#groupType_customField input').prop('disabled', false);
			$('#groupType_customField').removeClass('disabled');
		}else{
			$('#NumOfGroup').prop('disabled', false);
			$('#groupType_customField input').prop('disabled', true);
			$('#groupType_customField').addClass('disabled');
		}
	}).change();
	$('#customGroup_all').change(function(){
		$('.customGroup').prop('checked', $(this).prop('checked'));
	});
	$('.customGroup').change(function(){
		if(!$(this).prop('checked')){
			$('#customGroup_all').prop('checked', false);
		}
	});
});
</script>
<div class="main_content_detail">
	<?php
    if (method_exists($lauc, 'getAdminModuleTab')) {
        kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), 'timeslotsettings', '#/apps/admission/interview/');
    } elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), 'timeslotsettings', '#/apps/admission/interview/');
    } else if ($sys_custom['KIS_Admission']['InterviewArrangement'] || $moduleSettings['enableinterviewarrangement']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'timeslotsettings', 'announcementsettings'), 'timeslotsettings', '#/apps/admission/interview/');
    }
    ?>
<p class="spacer"></p>
<form id="MailForm" name="MailForm" method="POST" action="apps/admission/templates/interview/timeslotsettings_edit_update.php" enctype="multipart/form-data">
	<?=$kis_data['NavigationBar']?>
	<p class="spacer"></p><br>

	<div class="table_board">
		<?php
		if(file_exists("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/template/interview/timeSlotSettingsEdit.php")):
			include("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/template/interview/timeSlotSettingsEdit.php");
		else:
		?>
		<table id="FormTable" class="form_table">
        	<tbody>
                <tr>
                	<td class="field_title" style="width:30%;"><?=$kis_lang['date']?><?=$mustfillinsymbol?></td>
                	<td style="width:70%;">
                		<input type="text" name="Date" id="Date" value="<?=$kis_data['interviewArrangementRecords'][$kis_data['recordID']]['Date']?>">&nbsp;<span class="error_msg" id="warning_Date"></span>
                	</td>
                </tr>
                <tr>
                	<td class="field_title" style="width:30%;"><?=$kis_lang['timeslot']?><?=$mustfillinsymbol?></td>
                	<td style="width:70%;">
                		<table class="form_table" id="timeslot_table">

                			<?for($i=0; $i<sizeof($kis_data['interviewArrangementRecords'][$kis_data['recordID']]['StartTime']); $i++){?>
	                			<tr>
	                			<td style="width:230px"><?=$libadmission->Get_Time_Selection($linterface,'StartTime_'.$i,$kis_data['interviewArrangementRecords'][$kis_data['recordID']]['StartTime'][$i],'',false)?>&nbsp;<span class="error_msg" id="warning_StartTime_<?=$i?>"></span> ~
	                				<?=$libadmission->Get_Time_Selection($linterface,'EndTime_'.$i,$kis_data['interviewArrangementRecords'][$kis_data['recordID']]['EndTime'][$i],'',false)?>&nbsp;<span class="error_msg" id="warning_EndTime_<?=$i?>"></span>
	                				<?=$linterface->Get_Form_Warning_Msg("WarnContent", $kis_lang['msg']['request_input_email_template_content'], $Class='', $display=false)?>
	                			</td>
	                			<td>
	                			<?if($i>0){?>
	                				<div class="table_row_tool"><a title="<?=$kis_lang['Delete']?>" id="DeleteTimeslot" onClick="$(this).parent().parent().parent().remove();" class="delete_dim" href="javascript:void(0);"></a>
	                			<?}?>
	                			</td>
	                			</tr>
                			<?}?>
                			<?if(sizeof($kis_data['interviewArrangementRecords'][$kis_data['recordID']]['StartTime']) <=0){?>
                				<tr>
	                			<td style="width:230px"><?=$libadmission->Get_Time_Selection($linterface,'StartTime_0','','',false)?>&nbsp;<span class="error_msg" id="warning_StartTime_0"></span> ~
	                				<?=$libadmission->Get_Time_Selection($linterface,'EndTime_0','','',false)?>&nbsp;<span class="error_msg" id="warning_EndTime_0"></span>
	                				<?=$linterface->Get_Form_Warning_Msg("WarnContent", $kis_lang['msg']['request_input_email_template_content'], $Class='', $display=false)?>
	                			</td>
	                			<td>
	                			</td>
	                			</tr>
                			<?}?>
                		<tr id="timeslot_table_last">
	                		<td colspan="2">
	                			<div class="table_row_tool"><a title="<?=$kis_lang['add']?>" id="AddTimeslot" class="add_dim" href="javascript:void(0);"></a></div>
	                		</td>
                		</tr>
                		</table>
                	</td>
                </tr>
                <tr>
                	<td class="field_title" style="width:30%;"><?=$admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['roomsetting']:$kis_lang['groupsetting']?><?=$mustfillinsymbol?></td>
                	<td style="width:70%;">
                    	<?php
                    	if($kis_data['interviewArrangementRecords'][$kis_data['recordID']]['Group']){
                    	    $numberChecked = '';
                    	    $customChecked = 'checked';
                    	    $groupCheckedArr = explode(',', ($kis_data['interviewArrangementRecords'][$kis_data['recordID']]['Group']));
                    	}else{
                    	    $numberChecked = 'checked';
                    	    $customChecked = '';
                    	    $groupCheckedArr = array();
                    	}

                    	if($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'){
                    	    $langNumOf = $kis_lang['numofroom'];
                    	    $langCustom = $kis_lang['customroom'];
                    	}else{
                    	    $langNumOf = $kis_lang['numofgroup'];
                    	    $langCustom = $kis_lang['customgroup'];
                    	}

                    	if($admission_cfg['interview_arrangment']['interview_group_name']){
                    	    $groups = $admission_cfg['interview_arrangment']['interview_group_name'];
                    	}else{
                    	    $groups = range('A', 'Z');
                    	}
                    	?>

                		<div style="margin-bottom: 5px;">
                			<input type="radio" id="groupTypeSetting_num" name="groupType" value="number" <?=$numberChecked ?> />
                			<label for="groupTypeSetting_num"><?=$langNumOf?>: </label>
                			<?=getSelectByValue(range(1, count($groups)), ' id="NumOfGroup" name="NumOfGroup"', $kis_data['interviewArrangementRecords'][$kis_data['recordID']]['NumOfGroup'], 1, 0, $kis_lang['Admission']['pleaseSelect']) ?>
                		</div>

                		<div style="margin-bottom: 5px;">
                			<input type="radio" id="groupTypeSetting_custom" name="groupType" value="custom" <?=$customChecked ?> />
                			<label for="groupTypeSetting_custom"><?=$langCustom?>: </label>
                			<div id="groupType_customField" style="margin-left: 20px;margin-top: 5px;">
                				<label>
                					<input id="customGroup_all" type="checkbox" value="all"/>
                					<?=$Lang['Btn']['SelectAll']?>
                				</label>
                				<br />

                				<?php
                				foreach($groups as $index => $group){
                				    $checked = (in_array($group, $groupCheckedArr))? 'checked':'';
                				?>
                					<label style="min-width: 80px;display:inline-block;">
                    					<input type="checkbox" class="customGroup" name="customGroup[]" value="<?=$group ?>" <?=$checked ?>/>
                    					<?=$group ?>
                					</label>

                					<?php if($index % 5 == 4){ ?>
	                					<br />
                    				<?php } ?>
                				<?php
                				}
                				?>
                			</div>
                		</div>
                	</td>
                </tr>
                <tr>
                	<td class="field_title" style="width:30%;"><?=$kis_lang['qouta']?><?=$mustfillinsymbol?></td>
                	<td style="width:70%;">
            			<?=$linterface->GET_TEXTBOX("Quota", "Quota", $kis_data['interviewArrangementRecords'][$kis_data['recordID']]['Quota'], $OtherClass='', $OtherPar=array()).'<br />'?>
            		</td>
            	</tr>
            </tbody>
        </table>
        <?php
        endif;
        ?>
        <div class="text_remark step1" <?=$goto_step==2?'style="display:none;"':''?>><?=str_replace('*','<span style="color:red">*</span>',$kis_lang['requiredfield'])?></div>
		<div class="edit_bottom">
			<input type="hidden" name="RecordID" id="RecordID" value="<?=$kis_data['recordID']?>">
			<input type="hidden" name="NumOfTimeslot" id="NumOfTimeslot" value="<?=(sizeof($kis_data['interviewArrangementRecords'][$kis_data['recordID']]['StartTime'])?sizeof($kis_data['interviewArrangementRecords'][$kis_data['recordID']]['StartTime']):1)?>">
        	<input type="hidden" name="Round" id="Round" value="<?=$selectInterviewRound?>">
        	<input type="button" name="submitBtn" id="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
            <input type="button" name="cancelBtn" id="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
	    </div>
        <p class="spacer"></p><br>
	</div>
</form>
</div>
