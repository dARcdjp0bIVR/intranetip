<?php
// Editing by Henry

//include_once($intranet_root."/lang/lang.".$intranet_session_language.".php");

//$linterface = $kis_data['libinterface'];

//if(!isset($kis_data['recordID'])){
//	exit;
//}

if($kis_data['recordID']!=''){
	//$schoolYearID  = $kis_data['interviewSettingAry'][0]['SchoolYearID'];
	$classLevelID = $kis_data['interviewSettingAry'][0]['ClassLevelID'];
	$date = $kis_data['interviewSettingAry'][0]['Date'];
	$startTime = $kis_data['interviewSettingAry'][0]['StartTime'];
	$endTime = $kis_data['interviewSettingAry'][0]['EndTime'];
	$groupName = $kis_data['interviewSettingAry'][0]['GroupName'];
	$quota = $kis_data['interviewSettingAry'][0]['Quota'];
	$applied = $kis_data['interviewSettingAry'][0]['Applied'];
	$round = $kis_data['interviewSettingAry'][0]['Round'];
}

?>
<style type="text/css">
/******************* content div [start] ****************************/

#form_div ul, #form_tools_div ul { list-style-type: none; margin: 0; padding: 0; margin-bottom: 10px; min-height: 200px; }

#form_tools_div li { margin: 5px; padding: 5px; width: 200px;}

#form_div li { margin: 5px; padding: 5px; width: 250px;}

#form_div {
	float: left;
    border: 1px solid grey;
    padding: 0px; 
    width: 60%;
    min-height: 200px;    
}

#form_tools_div {
	float: left;
    /*border: 1px solid grey;*/
    padding: 0px; 
    width: 20%;
    /*height: 150px;*/    
}

.control-group-edit{
	display:none;
}

/******************* content div [end] ****************************/
</style>
<script type="text/javascript">

$(function(){
	kis.admission.interview_form_builter_edit_init({
		invaliddateformat:'<?=$kis_lang['Admission']['msg']['invaliddateformat']?>',
		enddateearlier: '<?=$kis_lang['Admission']['msg']['enddateearlier']?>',
		emptyapplicationdatewarning: '<?=$kis_lang['Admission']['msg']['emptyinterviewformdatewarning']?>',
		selectatleastonetimeslot: '<?=$kis_lang['Admission']['msg']['selectatleastonetimeslot']?>',
		enterpositivenumber: '<?=$kis_lang['Admission']['msg']['enternumber']?>',
		quotashouldnotlessthanapplied: '<?=$kis_lang['Admission']['msg']['quotashouldnotlessthanapplied']?>',
		applied: '<?=$applied?>',
		entertitle: '<?=$kis_lang['msg']['enterTitle']?>',
		enterinputinterviewquestion: '<?=$kis_lang['msg']['enterinputinterviewquestion']?>',
		selectatleastoneclass: '<?=$kis_lang['msg']['selectatleastoneclass']?>'
	});
	
	/******************* content div [start] ****************************/
	var removeIntent = false;
	
	$( "#sortable" ).sortable({
      //revert: true,
      over: function () {
                removeIntent = false;
            },
	    out: function () {
	        removeIntent = true;
	    },
	    beforeStop: function (event, ui) {
	        if(removeIntent == true){
	            ui.item.remove();   
	        }
	    }
    });
    
    $( ".draggable" ).draggable({
      connectToSortable: "#sortable",
      helper: "clone",
      revert: "invalid"
    });
    
    $( "#sortable" ).droppable({
      accept: ".draggable",
      //activeClass: "ui-state-default",
      drop: function( event, ui ) {
      	//alert('dropped');
      	//$( "#textinput" ).val('hello world');
      	ui.draggable.toggleClass('draggable dragged');
      	ui.draggable.toggleClass('ui-state-highlight ui-state-default');
      }
    });
    
    //$( "ul, li" ).disableSelection();
    $( ".control-group" ).disableSelection();
    /******************* content div [end] ****************************/
    
    $('#form_div').on( "click",function() {
    	//get the question array
    	var question_type_arr = $("#form_div input[name='questions_type[]']");
    	
    	for(var i=0; i<question_type_arr.length; i++){
    		$('#form_div .lbl_text_title').eq(i).text($("#form_div input[name='text_title[]']").eq(i).val());
    		$('#form_div .lbl_text_remarks').eq(i).text($("#form_div input[name='text_remarks[]']").eq(i).val());
    		
    		if($("#form_div input[name='chk_required']").eq(i).prop( "checked" )){
    			$("#form_div input[name='text_required[]']").eq(i).val(1);
    			$('#form_div .lbl_text_required').eq(i).html('<?=$mustfillinsymbol?>');
    		}
    		else{
    			$("#form_div input[name='text_required[]']").eq(i).val(0);
    			$('#form_div .lbl_text_required').eq(i).html('');
    		}
    		
    		if($("#form_div input[name='chk_mark']").eq(i).prop( "checked" )){
    			$("#form_div input[name='text_mark[]']").eq(i).val(1);
    		}
    		else{
    			$("#form_div input[name='text_mark[]']").eq(i).val(0);
    		}
    		
    		if(question_type_arr.eq(i).val() == 'text'){
    		
    			$('#form_div .lbl_text_input').eq(i).attr("placeholder", $("#form_div [name='text_input[]']").eq(i).val());
    			
    		}
    		else if(question_type_arr.eq(i).val() == 'radio'){
    		
    			var str = $("#form_div [name='text_input[]']").eq(i).val();
    			var res = str.split("\n");
    			
    			var output = '';
    			for(var j=0; j < res.length; j++){
    				if(res[j])
    					output += '<label class="radio inline" for="radios-'+j+'"><input type="radio" name="radios" id="radios-'+j+'" value="'+res[j]+'">'+res[j]+'</label> ';
    			}
    			
    			$('#form_div .lbl_text_input').eq(i).html(output);
    			
    		}
    		else if(question_type_arr.eq(i).val() == 'select'){
    			
    			var str = $("#form_div [name='text_input[]']").eq(i).val();
    			var res = str.split("\n");
    			
    			var output = '<select id="selectbasic" name="selectbasic" class="input-xlarge">';
    			for(var j=0; j < res.length; j++){
    				output += '<option>'+res[j]+'</option>';
    			}
    			output += '</select>';
    			
    			$('#form_div .lbl_text_input').eq(i).html(output);
    			
    		}
    	}
    	
		$(".control-group-edit").hide();
		$(".control-group" ).show();
	});
	
    $('#form_div').on( "click",".dragged", function(event){
	    event.stopPropagation();
	});
	
	$('#form_div').on( "click","select[name=selectbasic]", function(event){
	    event.stopPropagation();
	});
	
    $( "#form_div" ).on( "click",".dragged", function() {
    	$('#needUpdateQuestion').val(1);
		$(this).find(".control-group-edit" ).show();
		$(this).find(".control-group" ).hide();
	});
	
});

</script>
<div class="main_content">
	    <div class="main_content_detail">
	    	<?php
            if (method_exists($lauc, 'getAdminModuleTab')) {
                kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), 'interviewformbuilter', '#/apps/admission/interview/');
            } elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
                kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), 'interviewformbuilter', '#/apps/admission/interview/');
            }
            ?>
			<?=$kis_data['NavigationBar']?>
			<p class="spacer"></p>
			<form id='interview_form' name='interview_form'>
			<div class="table_board">
				<table class="form_table">
					 <tr>
						<td class="field_title" width="25%"><?=$mustfillinsymbol?><?=$kis_lang['form']?></td>
						<td width="75%">
							<?=$classLevelSelection?>
							<span class=\"tabletextremark\">
								<?=$kis_lang['CtrlMultiSelectMessage']?>
							</span>
						</td>
					</tr>
				    <tr>
						<td class="field_title"><?=$kis_lang['from']?></td>
						<td><input type="text" name="start_date" id="start_date" value="<?=substr($interviewFormSettingAry['StartDate'], 0, 10)?>">&nbsp;<span class="error_msg" id="warning_start_date"></span><?=$libadmission->Get_Time_Selection($libinterface,'start_time',substr($interviewFormSettingAry['StartDate'], -8),'',false)?></td>
					</tr>		
				    <tr>
						<td class="field_title"><?=$kis_lang['to']?></td>
						<td><input type="text" name="end_date" id="end_date" value="<?=substr($interviewFormSettingAry['EndDate'], 0, 10)?>">&nbsp;<span class="error_msg" id="warning_end_date"></span><?=$libadmission->Get_Time_Selection($libinterface,'end_time',($interviewFormSettingAry['EndDate']?substr($interviewFormSettingAry['EndDate'], -8):'23:59:59'),'',false)?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['email_template_title']?></td>
						<td><input type="text" name="title" id="title" value="<?=$interviewFormSettingAry['Title']?>" /></td>
					</tr>
					<tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['email_template_content']?>
						<?=($interviewFormSettingAry['ResultCount']>0?'<br/><font color="red">('.$kis_lang['msg']['cannotdeleteinterviewquestion'].')</font>':'')?>
						</td>
					<!--</tr>
					<tr>-->
						<td>
							<div id='form_div' <?=($interviewFormSettingAry['ResultCount']>0?'style="pointer-events: none;"':'')?>>
							
								<!-- Form Content [start] --> 
								<ul id="sortable">
								  <!--<li class="ui-state-default">Drop here...</li>-->
								  <?
								  for($i=0; $i < sizeof($interviewQuestionAry); $i++){
								  	if($interviewQuestionAry[$i]['Type'] == 'text'){
								  		echo '<li class="dragged ui-state-default">
											  	<div class="control-group">
												  <label class="lbl_text_required"></label>
												  <label class="control-label lbl_text_title" for="textinput">'.($interviewQuestionAry[$i]['Required']?$mustfillinsymbol:'').$interviewQuestionAry[$i]['Title'].'</label>
												  <div class="controls">
												    <input type="text" placeholder="'.$interviewQuestionAry[$i]['Name'].'" class="lbl_text_input input-xlarge">
												  	<br/><label class="lbl_text_remarks">'.$interviewQuestionAry[$i]['Remarks'].'</label>
												  </div>
												</div>
												<div class="control-group-edit">
												  <label class="control-label" for="textinput"><input type="text" name="text_title[]" placeholder="'.$kis_lang['interviewtextinput'].'" value="'.$interviewQuestionAry[$i]['Title'].'"></input></label>
												  <div class="controls">
												    <input id="textinput" name="text_input[]" type="text" placeholder="'.$kis_lang['interviewplaceholder'].'" value="'.$interviewQuestionAry[$i]['Name'].'" class="input-xlarge">
												  	<br/><input type="text" name="text_remarks[]" placeholder="'.$kis_lang['interviewtextinputremarks'].'" value="'.$interviewQuestionAry[$i]['Remarks'].'"></input>
												  </div>
												  <label class="control-label" for="checkboxes">'.$kis_lang['interviewrequired'].'</label><input type="checkbox" name="chk_required" value="" '.($interviewQuestionAry[$i]['Required']?'checked':'').'></input>
												  <br/><input type="checkbox" name="chk_mark" style="display:none"/>
												  <input type="hidden" name="text_required[]" value="'.$interviewQuestionAry[$i]['Required'].'">
												  <input type="hidden" name="text_mark[]" value="" />
												  <input type="hidden" name="questions_type[]" value="text" />
												</div>
											  </li>';
								  	}
								  	else if($interviewQuestionAry[$i]['Type'] == 'radio'){
								  		echo '<li class="dragged ui-state-default">
											  	<div class="component"><!-- Multiple Radios (inline) -->
													<div class="control-group">
														<label class="lbl_text_required"></label>
													  <label class="control-label lbl_text_title" for="radios">'.($interviewQuestionAry[$i]['Required']?$mustfillinsymbol:'').$interviewQuestionAry[$i]['Title'].'</label>
													  <div class="lbl_text_input controls">';
													  	$option_array = explode(',',$interviewQuestionAry[$i]['Name']);
													  	
													  	for($j=0; $j<sizeof($option_array); $j++){
														    echo '<label class="radio inline" for="radios-'.$j.'">
														      <input type="radio" name="radios" id="radios-'.$j.'" value="'.$option_array[$j].'">'.$option_array[$j].'</label>';
													  	}
													  	
												echo '</div><label class="lbl_text_remarks">'.$interviewQuestionAry[$i]['Remarks'].'</label>
													</div>
													<div class="control-group-edit">
													  <label class="control-label" for="radios"><input type="text" name="text_title[]" value="'.$interviewQuestionAry[$i]['Title'].'"></label>
													  <div class="controls">
													    <textarea name="text_input[]" rows="8">'.str_replace(',',"\n",$interviewQuestionAry[$i]['Name']).'</textarea>
													    <br/><input type="text" name="text_remarks[]" placeholder="'.$kis_lang['interviewinlineradiosremarks'].'" value="'.$interviewQuestionAry[$i]['Remarks'].'"></input>
													  </div>
													  <label class="control-label" for="checkboxes">'.$kis_lang['interviewrequired'].'</label><input type="checkbox" name="chk_required" value="" '.($interviewQuestionAry[$i]['Required']?'checked':'').'></input>
													  <br/><label class="control-label" for="checkboxes">'.$kis_lang['interviewformarking'].'</label><input type="checkbox" name="chk_mark" value="" '.($interviewQuestionAry[$i]['Marked']?'checked':'').'/>
													  <input type="hidden" name="text_required[]" value="'.$interviewQuestionAry[$i]['Name'].'">
													  <input type="hidden" name="text_mark[]" value="'.$interviewQuestionAry[$i]['Marked'].'" />
													  <input type="hidden" name="questions_type[]" value="radio" />
													</div>
												</div>
											  </li>';
								  	}
								  	else if($interviewQuestionAry[$i]['Type'] == 'select'){
								  		echo '<li class="dragged ui-state-default">
											  	<div class="component"><!-- Multiple Radios (inline) -->
													<div class="control-group">
														<label class="lbl_text_required"></label>
													  <label class="control-label lbl_text_title" for="radios">'.($interviewQuestionAry[$i]['Required']?$mustfillinsymbol:'').$interviewQuestionAry[$i]['Title'].'</label>
													  <div class="lbl_text_input controls">
														<select id="selectbasic" name="selectbasic" class="input-xlarge">';

													  	$option_array = explode(',',$interviewQuestionAry[$i]['Name']);
													  	
													  	for($j=0; $j<sizeof($option_array); $j++){
														    echo '<option>'.$option_array[$j].'</option>';
													  	}
													  	
												echo '</select></div><label class="lbl_text_remarks">'.$interviewQuestionAry[$i]['Remarks'].'</label>
													</div>
													<div class="control-group-edit">
													  <label class="control-label" for="radios"><input type="text" name="text_title[]" value="'.$interviewQuestionAry[$i]['Title'].'"></label>
													  <div class="controls">
													    <textarea name="text_input[]" rows="8">'.str_replace(',',"\n",$interviewQuestionAry[$i]['Name']).'</textarea>
													    <br/><input type="text" name="text_remarks[]" placeholder="'.$kis_lang['interviewselectbasicremarks'].'" value="'.$interviewQuestionAry[$i]['Remarks'].'"></input>
													  </div>
													  <label class="control-label" for="checkboxes">'.$kis_lang['interviewrequired'].'</label><input type="checkbox" name="chk_required" value="" '.($interviewQuestionAry[$i]['Required']?'checked':'').'></input>
													  <br/><label class="control-label" for="checkboxes">'.$kis_lang['interviewformarking'].'</label><input type="checkbox" name="chk_mark" value="" '.($interviewQuestionAry[$i]['Marked']?'checked':'').'/>
													  <input type="hidden" name="text_required[]" value="'.$interviewQuestionAry[$i]['Name'].'">
													  <input type="hidden" name="text_mark[]" value="'.$interviewQuestionAry[$i]['Marked'].'" />
													  <input type="hidden" name="questions_type[]" value="select" />
													</div>
												</div>
											  </li>';
								  	}
								  	
								  }
								  ?>
								</ul>
								<!-- Form Content [end] -->
								<?if($interviewFormSettingAry['ResultCount']<=0){?>
								<p align="center"><?=$kis_lang['dropitemabove']?> ...</p>
								<?}?>
							</div>
							<input type="hidden" name="recordID" id="recordID" value="<?=$kis_data['recordID']?>">
							<input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$kis_data['schoolYearID']?>">
							<input type="hidden" name="needUpdateQuestion" id="needUpdateQuestion" value="0" />
							
							<?if($interviewFormSettingAry['ResultCount']<=0){?>
							<div id='form_tools_div' <?=($interviewFormSettingAry['ResultCount']>0?'style="pointer-events: none;"':'')?> >
								<!-- Form Tools [start] -->
								<ul>
								  <li class="draggable ui-state-highlight">
								  	<div class="control-group">
									  <label class="lbl_text_required"></label>
									  <label class="control-label lbl_text_title" for="textinput"><?=$kis_lang['interviewtextinput']?></label>
									  <div class="controls">
									    <input type="text" placeholder="<?=$kis_lang['interviewplaceholder']?>" class="lbl_text_input input-xlarge">
									  	<br/><label class="lbl_text_remarks"><?=$kis_lang['interviewtextinputremarks']?></label>
									  </div>
									</div>
									<div class="control-group-edit">
									  <label class="control-label" for="textinput"><input type='text' name='text_title[]' placeholder="<?=$kis_lang['interviewtextinput']?>" value="<?=$kis_lang['interviewtextinput']?>"></input></label>
									  <div class="controls">
									    <input id="textinput" name="text_input[]" type="text" placeholder="<?=$kis_lang['interviewplaceholder']?>" value="<?=$kis_lang['interviewplaceholder']?>" class="input-xlarge">
									  	<br/><input type='text' name="text_remarks[]" placeholder="<?=$kis_lang['interviewtextinputremarks']?>" value="<?=$kis_lang['interviewtextinputremarks']?>"></input>
									  </div>
									  <label class="control-label" for="checkboxes"><?=$kis_lang['interviewrequired']?></label><input type='checkbox' name="chk_required" value=""></input>
									  <br/><input type="checkbox" name="chk_mark" style="display:none"/>
									  <input type='hidden' name="text_required[]" value="">
									  <input type="hidden" name="text_mark[]" value="" />
									  <input type="hidden" name="questions_type[]" value="text" />
									</div>
								  </li>
								  
								  <li class="draggable ui-state-highlight">
								  	<div class="component"><!-- Multiple Radios (inline) -->
										<div class="control-group">
											<label class="lbl_text_required"></label>
										  <label class="control-label lbl_text_title" for="radios"><?=$kis_lang['interviewinlineradios']?></label>
										  <div class="lbl_text_input controls">
										    <label class="radio inline" for="radios-0">
										      <input type="radio" name="radios" id="radios-0" value="1" checked="checked">1</label>
										    <label class="radio inline" for="radios-1">
										      <input type="radio" name="radios" id="radios-1" value="2">2</label>
										    <label class="radio inline" for="radios-2">
										      <input type="radio" name="radios" id="radios-2" value="3">3</label>
										    <label class="radio inline" for="radios-3">
										      <input type="radio" name="radios" id="radios-3" value="4">4</label>
										   
										  </div><label class="lbl_text_remarks"><?=$kis_lang['interviewinlineradiosremarks']?></label>
										</div>
										<div class="control-group-edit">
										  <label class="control-label" for="radios"><input type="text" name="text_title[]" value="<?=$kis_lang['interviewinlineradios']?>"></label>
										  <div class="controls">
										    <textarea name="text_input[]" rows="8">1&#13;&#10;2&#13;&#10;3&#13;&#10;4</textarea>
										    <br/><input type='text' name="text_remarks[]" placeholder="<?=$kis_lang['interviewinlineradiosremarks']?>" value="<?=$kis_lang['interviewinlineradiosremarks']?>"></input>
										  </div>
										  <label class="control-label" for="checkboxes"><?=$kis_lang['interviewrequired']?></label><input type='checkbox' name="chk_required"></input>
										  <br/><label class="control-label" for="checkboxes"><?=$kis_lang['interviewformarking']?></label><input type="checkbox" name="chk_mark" />
										  <input type='hidden' name="text_required[]" value="">
										  <input type="hidden" name="text_mark[]" value="" />
										  <input type="hidden" name="questions_type[]" value="radio" />
										</div>
									</div>
								  </li>
								  
								  <li class="draggable ui-state-highlight">
								  	<div class="component"><!-- Select Basic -->
										<div class="control-group">
										  <label class="lbl_text_required"></label>
										  <label class="control-label lbl_text_title" for="selectbasic"><?=$kis_lang['interviewselectbasic']?></label>
										  <div class="lbl_text_input controls">
										    <select id="selectbasic" name="selectbasic" class="input-xlarge">
										      <option><?=$kis_lang['interviewoptionone']?></option>
										      <option><?=$kis_lang['interviewoptiontwo']?></option>
										    </select>
										  </div><label class="lbl_text_remarks"><?=$kis_lang['interviewselectbasicremarks']?></label>
										</div>
										<div class="control-group-edit">
										  <label class="control-label" for="selectbasic"><input type="text" name="text_title[]" value="<?=$kis_lang['interviewselectbasic']?>"></label>
										  <div class="controls">
										    <textarea name="text_input[]" rows="8"><?=$kis_lang['interviewoptionone']?>&#13;&#10;<?=$kis_lang['interviewoptiontwo']?></textarea> 
										  </div>
										  <input type='text' name="text_remarks[]" placeholder="<?=$kis_lang['interviewselectbasicremarks']?>" value="<?=$kis_lang['interviewselectbasicremarks']?>"></input>
										  <br/><label class="control-label" for="checkboxes"><?=$kis_lang['interviewrequired']?></label><input type='checkbox' name="chk_required" value=""></input>
										  <br/><label class="control-label" for="checkboxes"><?=$kis_lang['interviewformarking']?></label><input type="checkbox" name="chk_mark" />
										  <input type='hidden' name="text_required[]" value="">
										  <input type="hidden" name="text_mark[]" value="" />
										  <input type="hidden" name="questions_type[]" value="select" />
										</div>
									</div>
								  </li>
								</ul>
								<!-- Form Tools [end] -->
							</div>
							<?}?>
						</td>
					</tr>     
			    </table>
				<p class="spacer"></p>
				<p class="spacer"></p><br />
	        </div>
	        </form>
	        <div class="text_remark"><?=$kis_lang['requiredfield']?></div>
			<div class="edit_bottom">
				<?if(!$hasFilled){?>
					<input type="button" id="submitBtn" name="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
				<?}?>
				<input type="button" id="cancelBtn" name="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
			</div>
		</div>
</div>