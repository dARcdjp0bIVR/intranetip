<?php
// Editing by 

/**
 *	2014-08-29 (Henry): Created file
 **/
 
include_once($intranet_root."/lang/lang.".$intranet_session_language.".php");

$linterface = $kis_data['libinterface'];

$goto_step = 1;
?>
<style type="text/css">
/******************* step div [start] ****************************/
.stepboard {margin:0 auto; width:85%; display:block; padding:5px; border: dashed 1px #32C617; margin-bottom:10px; clear:both}
.stepboard  h1 {display:block; background:#EDFFE0; color:#188107; font-size:1em; padding:5px; margin:0}
.stepboard ul {list-style-type: none; margin:0; padding:10px; clear:both; display:block}
.stepboard ul li{ display:inline;float:left; padding:0px; margin:0px; line-height:18px;}
.stepboard ul li div{ padding-left:25px; display:block;}
.stepboard ul li em {font-style:normal; color:#FFFFFF;  background:url(/images/2009a/stepnum_off.gif) no-repeat 3px 1px; margin-left:-25px; float:left; display:block; width:25px; height:25px; text-align:center;}
.stepboard ul li span {color:#C4C4C4; display:block; padding-right:10px;}
.stepboard ul li.stepon em {font-style:bold;  background:url(/images/2009a/stepnum_on.gif) no-repeat 1px top; line-height:20px}
.stepboard ul li.stepon span {color:#188107; font-weight:bold;}

.stepboard_2s ul li{ width:50%}                   
.stepboard_3s ul li{ width:33%}                   
.stepboard_4s ul li{ width:25%}                   
.stepboard_5s ul li{ width:20%}
/******************* step div [end] ****************************/
</style>
<script type="text/javascript" src="/templates/jquery/jquery.form.js"></script>
<script type="text/javascript" language="JavaScript">
jQuery.extend({
    handleError: function( s, xhr, status, e ) {
        // If a local callback was specified, fire it
        if ( s.error )
            s.error( xhr, status, e );
        // If we have some XML response text (e.g. from an AJAX call) then log it in the console
        else if(xhr.responseText)
            console.log(xhr.responseText);
    }
});
$(function(){
	kis.admission.import_interview_settings_init({
		pleaseselectcsvortxt:'<?=$kis_lang['Admission']['msg']['PleaseSelectCSVorTXT']?>'
	});
})
</script>
<div class="main_content_detail">
<form id="importForm" name="importForm" action='./apps/admission/ajax.php?action=checkImportInterviewSettingsInfo' method="post" enctype="multipart/form-data">
	<?php
    if (method_exists($lauc, 'getAdminModuleTab')) {
        kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), '', '#/apps/admission/interview/');
    } elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), '', '#/apps/admission/interview/');
    } else if ($sys_custom['KIS_Admission']['InterviewArrangement'] || $moduleSettings['enableinterviewarrangement']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'timeslotsettings', 'announcementsettings'), '', '#/apps/admission/interview/');
    }
    ?>
	<?=$kis_data['NavigationBar']?>
	<p class="spacer"></p><br>
	
	<div class="table_board">
		<div class="step1">
		<?php
			$STEPS_OBJ = array();
			$STEPS_OBJ[] = array($kis_lang['SelectCSVFile'], 1);
			$STEPS_OBJ[] = array($kis_lang['CSVConfirmation'], 0);
			$STEPS_OBJ[] = array($kis_lang['ImportResult'], 0);
			echo $linterface->GET_STEPS_IP25($STEPS_OBJ);
		?>
		</div>
		<div class="step2" style="display:none;">
		<?php
			$STEPS_OBJ = array();
			$STEPS_OBJ[] = array($kis_lang['SelectCSVFile'], 0);
			$STEPS_OBJ[] = array($kis_lang['CSVConfirmation'], 1);
			$STEPS_OBJ[] = array($kis_lang['ImportResult'], 0);
			echo $linterface->GET_STEPS_IP25($STEPS_OBJ);
		?>
		</div>
		<div class="step3" style="display:none;">
		<?php
			$STEPS_OBJ = array();
			$STEPS_OBJ[] = array($kis_lang['SelectCSVFile'], 0);
			$STEPS_OBJ[] = array($kis_lang['CSVConfirmation'], 0);
			$STEPS_OBJ[] = array($kis_lang['ImportResult'], 1);
			echo $linterface->GET_STEPS_IP25($STEPS_OBJ);
		?>
		</div>
		<table id="ComposeMailTable" class="form_table">
        	<tbody>
            	<tr class="step1"> 
                	<td class="field_title" style="width:30%;"><?=$kis_lang['SelectCSVFile']?> <?=$kis_lang['CSVFileFormat']?></td>
                	<td style="width:70%;">
                	<input class="file" type="file"  id = "userfile" name="userfile">
                	</td>
                </tr>
                <tr class="step1"> 
                	<td class="field_title" style="width:30%;"><?=$kis_lang['Remark']?></td>
                	<td style="width:70%;">
                	<?
                	$ImportRemarkArr = $kis_lang['importinterviewtimeslotremarks'];
					$ImportRemark = '<table cellpadding=0 cellspacing=0 border=0>';	
					for($i=0; $i < count($ImportRemarkArr); $i++)
						$ImportRemark .= '<tr><td valign=top width=20px class="tabletext">'.($i+1).'. </td><td>'.$ImportRemarkArr[$i].'</td></tr>';	 
					$ImportRemark .= '</table>';
					echo $ImportRemark;
					?>
                	</td>
                </tr>
                <tr class="step1"> 
                	<td class="field_title" style="width:30%;"><?=$kis_lang['CSVSample']?></td>
                	<td style="width:70%;"><?=$kis_data['csvFile']?></td>
                </tr>
                <tr class="step1"> 
                	<td class="field_title" style="width:30%;"><?=$kis_lang['DataColumn']?></td>
                	<td style="width:70%;">
                	<?
                	$ImportFieldArr = $kis_lang['importinterviewtimeslotfield'];
					$ImportField = '<table cellpadding=0 cellspacing=0 border=0>';	
					for($i=0; $i < count($ImportFieldArr); $i++)
						$ImportField .= '<tr><td>'.$kis_lang['Column'].' '.($i+1).': '.$ImportFieldArr[$i].'</td></tr>';	 
					$ImportField .= '<tr><td>'.$kis_lang['Column'].' '.(count($ImportFieldArr)+1).': '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).'</td></tr>';
					$ImportField .= '</table>';
					echo $ImportField;
                	?>
                	</td>
                </tr>
                
                <tr class="step2" style="display:none">
					<td colspan="2">
					<table style="width:30%" id="totalRecordResult">
						<tr>
							<td class='field_title'><?=$Lang['General']['SuccessfulRecord']?></td>
							<td class='tabletext'><div id='validTotal'></div></td>
						</tr>
						<tr>
							<td class='field_title'><?=$Lang['General']['FailureRecord']?></td>
							<td class='tabletext <?=count($error_msg)?"red":""?>'><div id='invalidTotal'></div></td>
						</tr>
					</table>
					<div id="message"></div>
					</td>
				</tr>
				<tr class="step2" style="display:none;"><td colspan="2">&nbsp;</td></tr>
				<tr class="step3" style="display:none;">
					<td colspan="2">
					<table style="width:90%">
					<tr>
						<td class='tabletext'><div id="importResult"></div></td>
					</tr>
					</table>
					</td>
				</tr>
            </tbody>
        </table>
        <div class="text_remark step1"><?=str_replace('*','<span style="color:red">*</span>',$kis_lang['requiredfield'])?></div>
		<div class="edit_bottom">
			<input type="hidden" name="round" id="round" value="<?=$selectInterviewRound?>" />
        	<input type="button" name="nextBtn" id="nextBtn" class="formbutton step1" value="<?=$kis_lang['next']?>" <?=$goto_step==2?'style="display:none;"':''?> />
        	<input type="button" name="backBtn" id="backBtn" class="formbutton step2" value="<?=$kis_lang['back']?>" <?=$goto_step==1?'style="display:none;"':''?> />
        	<?php if($kis_data['recordID'] != '' && $kis_data['recordID'] > 0){ ?>
        	<input type="button" name="resendBtn" id="resendBtn" class="formbutton step2" value="<?=$kis_lang['resend']?>" <?=$goto_step==1?'style="display:none;"':''?> />
        	<?php }else{ ?>
        	<input type="button" name="submitBtn" id="submitBtn" class="formbutton step2" value="<?=$kis_lang['confirm']?>" <?=$goto_step==1?'style="display:none;"':''?> />
        	<?php } ?>
        	<input type="button" name="cancelBtn" id="cancelBtn" class="formsubbutton step1" value="<?=$kis_lang['cancel']?>" />
            <input style="display:none;" type="button" name="cancelBtn" id="cancelBtn" class="formsubbutton step2" value="<?=$kis_lang['cancel']?>" />
            <input type="button" name="doneBtn" id="doneBtn" class="formsubbutton step3" value="<?=$kis_lang['done']?>" style="display:none;" />
	    </div>
	    <input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$schoolYearID?>" />
        <p class="spacer"></p><br>  
	</div>
</form>
</div>
