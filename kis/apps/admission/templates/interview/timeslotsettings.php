<?php
// Editing by Henry
/****************************************
 *    2015-07-20 (Henry):    added Interview Arrangement tab
 ****************************************/

global $setting_path_ip_rel;
?>
<style type="text/css">
    table.common_table_list tr.move_selected td {
        background-color: #fbf786;
        border-top: 2px dashed #d3981a;
        border-bottom: 2px dashed #d3981a
    }
</style>
<script type="text/javascript">
    kis.admission.interview_arrangement_init = function (msg) {
        var msg_confirm_delete_email_template = msg.confirm_delete_email_template;

        var sysmsg = msg.returnmsg;
        if (sysmsg != '') {
            var returnmsg = sysmsg.split('|=|')[1];
            returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >[' + msg.close + ']</a>';
            var msgclass = sysmsg.split('|=|')[0] == 1 ? 'system_success' : 'system_alert';
            $('div#system_message').removeClass();
            $('div#system_message').addClass(msgclass).show();
            $('div#system_message span').html(returnmsg);
            setTimeout(function () {
                $('#system_message').hide();
            }, 3000);
        }

        $('a.tool_import_by_system').click(function () {
            var thisObj = $(this);
            var selectInterviewRound = $('#selectInterviewRound').val();

            if (confirm('<?="{$kis_lang['interviewarrangementremarks'][0]}\\n{$kis_lang['msg']['importinterviewsessionbysystem']}"?>')) {
                $.post('apps/admission/ajax.php?action=importInterviewSessionBySettings',
                    {
                        round: selectInterviewRound
                    },
                    function (success) {
                        $.address.value('/apps/admission/interview/timeslotsettings/?sysMsg=' + success + '&r=' + (new Date()).getTime());
                    });
            }
        });

        $('a.delete_dim').click(function () {
            var thisObj = $(this);
            var deleteRecordId = thisObj.find('input[name="TemplateID[]"]').val();

            if (confirm(msg_confirm_delete_email_template)) {
                $.post('apps/admission/ajax.php?action=deleteInterviewArrangementRecords',
                    {
                        'TemplateID[]': [deleteRecordId]
                    },
                    function (success) {
                        $.address.value('/apps/admission/interview/timeslotsettings/?sysMsg=' + success + '&r=' + (new Date()).getTime());
                    });
            }
        });

        $('select#RecordType').change(function () {
            $('#filter_form').submit();
        });

        $('select#selectInterviewRound').change(function () {
            $.address.value('/apps/admission/interview/timeslotsettings/' + (this.value ? this.value : 1) + '/');
        });

        var tableObj = $(".common_table_list");
        if (tableObj.length > 0 && tableObj.tableDnD) {
            tableObj.tableDnD({
                onDrop: function (table, DroppedRow) {
                    var recordIdObj = document.getElementsByName('TemplateID[]');
                    var recordIdAry = [];
                    for (var i = 0; i < recordIdObj.length; i++) {
                        recordIdAry.push(recordIdObj[i].value);
                    }
                    var id_list = recordIdAry.join();
                    if (this.IdList == id_list) return;

                    $.post(
                        'apps/admission/ajax.php?action=reorderInterviewArrangementRecords',
                        {
                            'TemplateID[]': recordIdAry
                        },
                        function (success) {
                            $.address.value('/apps/admission/interview/timeslotsettings/?sysMsg=' + success + '&r=' + (new Date()).getTime());
                        }
                    );
                },
                onDragStart: function (table, DraggedCell) {
                    var recordIdObj = document.getElementsByName('TemplateID[]');
                    var recordIdAry = [];
                    for (var i = 0; i < recordIdObj.length; i++) {
                        recordIdAry.push(recordIdObj[i].value);
                    }
                    this.IdList = recordIdAry.join();
                },
                dragHandle: "Dragable",
                onDragClass: "move_selected"
            });
        }
    };

    $(function () {
        kis.admission.interview_arrangement_init({
            'close': '<?=$kis_lang['close']?>',
            'delete': '<?=$kis_lang['delete']?>',
            'confirm_delete_email_template': '<?=$kis_lang['msg']['deleteinterviewarrangement']?>',
            'returnmsg': '<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>'
        });
    });
</script>
<div class="main_content_detail">
    <?php
    if (method_exists($lauc, 'getAdminModuleTab')) {
        kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), 'timeslotsettings', '#/apps/admission/interview/');
    } elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), 'timeslotsettings', '#/apps/admission/interview/');
    } else if ($sys_custom['KIS_Admission']['InterviewArrangement'] || $moduleSettings['enableinterviewarrangement']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'timeslotsettings'), 'timeslotsettings', '#/apps/admission/interview/');
    }
    ?>
    <p class="spacer"></p>
    <div class="Content_tool">
        <a class="new"
           href="#/apps/admission/interview/timeslotsettings/<?= $selectInterviewRound ?>/edit"><?= $kis_lang['new'] ?></a>

        <?php if($sys_custom['KIS_Admission']['CREATIVE']['Settings']){ ?>
        <a class="tool_import_by_system import"
           href="javascript:void(0);"><?= $kis_lang['generateInterviewTimeslot'] ?></a>
        <?php } ?>
    </div>
    <p class="spacer"></p>
    <? if (!empty($warning_msg)): ?>
        <?= $warning_msg ?>
    <? endif; ?>

    <div id="table_filter">
        <form class="filter_form">
            <?= $libadmission->Get_Number_Selection('selectInterviewRound', '1', '3', $selectInterviewRound, '', 1, 0, '', 0, $kis_lang['round']) ?>
        </form>
    </div>

    <!--<div id="table_filter">
    <form id="filter_form" class="filter_form" method="POST" action="apps/admission/templates/interview/timeslotsettings.php">
       <?php
    $template_types = array();
    $template_types[] = array($kis_data['libadmission']->getEmailTemplateType('Active'), $kis_lang['email_template_active']);
    $template_types[] = array($kis_data['libadmission']->getEmailTemplateType('Draft'), $kis_lang['email_template_draft']);

    $template_type_selection = getSelectByArray($template_types, ' id="RecordType" name="RecordType" ', $RecordType, 0, 0, $kis_lang['all']);
    echo $template_type_selection;
    ?>
    </form>
    </div>

      <p class="spacer"></p>-->
    <!--
	  <div class="common_table_tool">
	  	<a class="tool_new" href="#/apps/admission/interview/timeslotsettings/edit"><?= $kis_lang['new'] ?></a>
	  </div>
	  -->
    <div class="table_board">
        <?php
        if(file_exists("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/template/interview/timeSlotSettings.php")):
            include("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/template/interview/timeSlotSettings.php");
        else:
        ?>
            <table class="common_table_list edit_table_list">
                <colgroup>
                    <col nowrap="nowrap">
                </colgroup>
                <thead>
                <tr>
                    <th width="10">&nbsp;</th>
                    <th width="20%"><?= $kis_lang['date'] ?></th>
                    <th width="40%"><?= $kis_lang['timeslot'] ?></th>
                    <th width="5%"><?= $admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? $kis_lang['numofroom'] : $kis_lang['numofgroup'] ?></th>
                    <th width="5%"><?= $kis_lang['qouta'] ?></th>
                    <th width="20%">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $x = '';
                if (sizeof($interviewArrangementRecords) > 0) {
                    $index = 0;
                    foreach ($interviewArrangementRecords as $key => $record) {
                        $index++;
                        $x .= '<tr id="row_' . $record['RecordID'] . '">';
                        $x .= '<td class="Dragable">' . $index . '</td>';
                        $x .= '<td class="Dragable"><a class="td_form_name" href="#/apps/admission/interview/timeslotsettings/' . $selectInterviewRound . '/edit/' . $record['RecordID'] . '">' . $record['Date'] . '</a></td>';

                        $timeslotText = '';
                        for ($i = 0; $i < sizeof($record['StartTime']); $i++) {
                            $timeslotText .= substr($record['StartTime'][$i], 0, -3) . ' ~ ' . substr($record['EndTime'][$i], 0, -3) . '<br/>';
                        }

                        $x .= '<td class="Dragable">' . $timeslotText . '</td>';
                        $x .= '<td class="Dragable">' . $record['NumOfGroup'] . '</td>';
                        $x .= '<td class="Dragable">' . $record['Quota'] . '</td>';
                        $x .= '<td class="Dragable">';
                        $x .= '<div class="table_row_tool">';
                        $x .= '<a title="' . $kis_lang['edit'] . '" class="edit_dim" href="#/apps/admission/interview/timeslotsettings/' . $selectInterviewRound . '/edit/' . $record['RecordID'] . '" ></a>';
                        $x .= '<a title="' . $kis_lang['delete'] . '" class="delete_dim" href="javascript:void(0);"><input type="hidden" name="TemplateID[]" value="' . $record['RecordID'] . '" /></a>';
                        $x .= '</div>';
                        $x .= '</td>';
                        $x .= '</tr>';
                    }
                } else {
                    $x .= '<tr><td colspan="6" style="text-align:center;">' . $kis_lang['norecord'] . '</td></tr>';
                }
                echo $x;
                ?>
                </tbody>
            </table>
        <?php
        endif;
        ?>
        <p class="spacer"></p>
        <p class="spacer"></p><br>
    </div>
</div>
