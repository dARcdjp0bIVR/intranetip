<?php
// Editing by Henry

/**
 *	2015-07-29 (Henry): Created file
 **/
?>
<script>
kis.admission.interview_form_input_init({
	returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
	msg_confirm_delete_email_list:'<?=$kis_lang['Admission']['msg']['confirm_delete_email_list']?>',
	msg_selectatleastonerecord:'<?=$kis_lang['Admission']['msg']['selectatleastonerecord']?>',
	selectatleastonerecord:'<?=$kis_lang['Admission']['msg']['selectatleastonerecord']?>',
	exportallrecordsornot:'<?=$kis_lang['Admission']['msg']['exportallrecordsornot']?>',
	printallrecordsornot:'<?=$kis_lang['Admission']['msg']['printallrecordsornot']?>',
	sendemailallrecordsornot:'<?=$kis_lang['Admission']['msg']['sendemailallrecordsornot']?>',
	importinterviewallrecordsornot:'<?=$kis_lang['Admission']['msg']['importinterviewallrecordsornot']?>',
	returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
	close:'<?=$kis_lang['close']?>',
	importinterviewinfobyarrangementornot:'<?=$kis_lang['msg']['importinterviewinfobyarrangement']?>'
});
</script>
<div class="main_content_detail">
    <?php
    if (method_exists($lauc, 'getAdminModuleTab')) {
        kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), 'interviewforminput', '#/apps/admission/interview/');
    } elseif (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission-interview-marker"]) {
        kis_ui::loadModuleTab(array('interviewforminput'), 'interviewforminput', '#/apps/admission/interview/');
    } else if ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), 'interviewforminput', '#/apps/admission/interview/');
    }
    ?>
 	 <p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	<!--<div class="Content_tool">
		<a href="#" class="tool_new new"><?=$kis_lang['new']?></a>
	    <div class="btn_option"  id="ExportDiv">
	    	<a id="btn_import" class="tool_import import" href="javascript:void(0);"><?=$kis_lang['importinterviewtimeslotinfo']?></a>
			<a id="btn_import" class="tool_export export" href="javascript:void(0);"><?=$kis_lang['exportAppliedDetails']?></a>
			<?if($sys_custom['KIS_Admission']['InterviewArrangement']){?>
			<a id="btn_import_by_arragement" class="tool_import_by_arragement import" href="javascript:void(0);"><?=$kis_lang['importinterviewinfobyarrangement']?></a>
			<?}?>
		</div>  
	</div>-->
	<div id="table_filter">
    <form class="filter_form"> 
        <?=$schoolYearSelection?>
        <?=$libadmission->Get_Number_Selection('selectInterviewRound', '1', '3', $selectInterviewRound, '', 1, 0, '', 0, $kis_lang['round'])?>
    	<?=$interviewDateSelection?>
    </form>
    </div>
	 <!--<form class="filter_form"> 
    	<div id="table_filter">
        	<?=$schoolYearSelection?>
        	<?=$classLevelSelection?>
   		</div>	
		<div class="search">
		    <input placeholder="<?=$kis_lang['search'] ?>" name="keyword" value="<?=$keyword?>" type="text"/>
		</div>
    </form>-->
    
                     <!---->
                    	
                      <!--<p class="spacer"></p>-->
                      	  <div class="table_board">
                      	 <!-- <div class="common_table_tool common_table_tool_table">
                <a href="#" class="tool_edit"><?=$kis_lang['edit']?></a>
				<a href="#" class="tool_delete"><?=$kis_lang['delete']?></a>
            </div>
            <p class="spacer"></p>-->
             <form id="interview_list" name="interview_list" method="post">
                      <table class="common_table_list edit_table_list">
                        <colgroup><col nowrap="nowrap">
                        </colgroup><thead>
                          <tr>
                            <th width="20">&nbsp;</th>
                            <th><?=$kis_lang['form']?></th>
                            <th><?=$kis_lang['interviewdate']?></th>
                            <th><?=$kis_lang['timeslot']?> </th>
                            <?if($sys_custom['KIS_Admission']['InterviewArrangement']){?>
                            <th><?=$kis_lang['sessiongroup']?></th>
                            <?}?>
                            <th><?=$kis_lang['qouta']?></th>
                            <th><?=$kis_lang['applied']?></th>
                            <th><?=$kis_lang['interviewmarkingprogress']?></th>
                            <!--<th><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,'interviewAry[]'):setChecked(0,this.form,'interviewAry[]')"></th>-->
                          </tr>
                        </thead>
                        <tbody>
                  	<? 
                  		$idx = 0;
			      		foreach($interviewSettingAry as $_classLevelId => $_classLevelRecord): 
			      			//foreach($_classLevelAry as $_classLevelRecord){
			      				$idx++;
			      				$SchoolYearArr = current(kis_utility::getAcademicYears(array("AcademicYearID"=>$_classLevelRecord['ClassLevelID'])));
			      				$formName = $SchoolYearArr['academic_year_name_'.$intranet_session_language];			
			      	?>
							 <tr>
	                            <td><?=$idx?></td>
	                            <td><?=$classLevelAry[$_classLevelRecord['ClassLevelID']]?></td>
	                            <td><?=$_classLevelRecord['Date']?></td>
	                            <td><?=substr($_classLevelRecord['StartTime'], 0, -3)?> ~ <?=substr($_classLevelRecord['EndTime'], 0, -3)?></td>
	                            <?if($sys_custom['KIS_Admission']['InterviewArrangement']){?>
	                            <td><?=($_classLevelRecord['GroupName']?$_classLevelRecord['GroupName']:'--')?></td>
	                            <?}?>
	                            <td><?=$_classLevelRecord['Quota']?></td>
	                            <td><?=($_classLevelRecord['Applied'] > 0)?'<a href="#/apps/admission/interview/interviewforminput_details/'.($selectInterviewRound?$selectInterviewRound:1).'/'.($selectInterviewDate?$selectInterviewDate:0).'/'.$_classLevelRecord['RecordID'].'/">'.$_classLevelRecord['Applied'].'</a>':$_classLevelRecord['Applied']?>
	                            <input type="hidden" name="applied_<?=$_classLevelRecord['RecordID']?>" id="applied_<?=$_classLevelRecord['RecordID']?>" value="<?=$_classLevelRecord['Applied']?>">
	                            </td>
	                            <td><?if($_classLevelRecord['Applied']>0){?><font style="color:<?=($interviewFormResultAryById[$_classLevelRecord['RecordID']]>=$_classLevelRecord['Applied']?'green':'red')?>"><?=$interviewFormResultAryById[$_classLevelRecord['RecordID']]?>/<?=$_classLevelRecord['Applied']?><?}else{?>--<?}?>
	                            </td>
	                            <!--<td><?=$libinterface->Get_Checkbox('interview_'.$_classLevelRecord['RecordID'], 'interviewAry[]', $_classLevelRecord['RecordID'], '', $Class='', $Display='', $Onclick="unset_checkall(this, document.getElementById('interview_list'));", $Disabled='')?></td>-->
	                          </tr>
					<? 		//}
						endforeach; 
					if($idx == 0){?>
                  		<tr>
                  			<td colspan="7" style="text-align:center;"><?=$kis_lang['norecord']?></td>
                  		</tr>  
					<?}?>	  
                        </tbody>
                      </table>
                      <input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$schoolYearID?>">
					</form>
                      <p class="spacer"></p>
                      <p class="spacer"></p>
                      <br>
                    </div></div>