<?php
// Editing by 
?>
<script>
    kis.admission.interview_details_init({
        selectatleastonerecord: '<?=$kis_lang['Admission']['msg']['selectatleastonerecord']?>',
        returnmsg: '<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
        msg_confirm_delete_email_list: '<?=$kis_lang['msg']['deleteinterviewrecord']?>',
        msg_selectatleastonerecord: '<?=$kis_lang['Admission']['msg']['selectatleastonerecord']?>',
        printallrecordsornot: '<?=$kis_lang['Admission']['msg']['printallrecordsornot']?>'
    });
</script>
<div class="main_content_detail">
    <?php
    if (method_exists($lauc, 'getAdminModuleTab')) {
        kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), '', '#/apps/admission/interview/');
    } elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), '', '#/apps/admission/interview/');
    } else if ($sys_custom['KIS_Admission']['InterviewArrangement'] || $moduleSettings['enableinterviewarrangement']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'timeslotsettings', 'announcementsettings'), '', '#/apps/admission/interview/');
    }
    ?>
    <?= $kis_data['NavigationBar'] ?>
    <p class="spacer"></p>
    <? if (!empty($warning_msg)): ?>
        <?= $warning_msg ?>
    <? endif; ?>
    <? if ($enableNew) { ?>
        <div class="Content_tool">
            <a href="#" class="tool_new new"><?= $kis_lang['new'] ?></a>
        </div>
    <? } ?>
    <!--<form class="filter_form">
    	<div id="table_filter">
        	<?= $schoolYearSelection ?>
        	<?= $classLevelSelection ?>
   		</div>	
		<div class="search">
		    <input placeholder="<?= $kis_lang['search'] ?>" name="keyword" value="<?= $keyword ?>" type="text"/>
		</div>
    </form>-->

    <!---->

    <!--<p class="spacer"></p>-->
    <div class="table_board">
        <div class="common_table_tool common_table_tool_table">
            <!--<a href="#" class="tool_new"><?= $kis_lang['new'] ?></a>
                <a href="#" class="tool_edit"><?= $kis_lang['edit'] ?></a>-->
            <a href="#" class="tool_email tool_send_email"><?= $kis_lang['sendemail'] ?></a>
            <?php if ($sys_custom['KIS_Admission']['CREATIVE']['Settings']): ?>
                <a
                        class="tool_export_label"
                        href="javascript:void(0);"
                        style="background-position: 0px -580px;"
                >
                    <?= $kis_lang['Admission']['CREATIVE']['exportApplicantLabel'] ?>
                </a>
                <script>
                    $('a.tool_export_label').click(function (e) {
                        var open_fancy_box2 = function () {
                            $.fancybox({
                                'fitToView': false,
                                'autoDimensions': false,
                                'autoScale': false,
                                'autoSize': false,
                                'width': 300,
                                'height': 250,

                                'transitionIn': 'elastic',
                                'transitionOut': 'elastic',
                                'href': '#print_label_box'
                            });
                        };

                        var hasChecked = false;
                        $("input[name='applicationAry[]']").each(function () {
                            if (this.checked) {
                                hasChecked = true;
                                return;
                            }
                        });
                        $('#selectedApplicant').empty();
                        if (hasChecked) {
                            $("input[name='applicationAry[]']:checked").each(function () {
                                $('#selectedApplicant')
                                    .append('<input name="filterApplicantAry[]" value="' + $(this).val() + '" />');
                            });
                        } else {
                            if (!confirm("<?=$kis_lang['Admission']['msg']['exportallrecordsornot']?>")) {
                                return false;
                            }
                        }
                        open_fancy_box2();

                        return false;
                    });
                </script>
            <?php endif; ?>
            <a href="#" class="tool_print tool_print_form"><?= $kis_lang['print'] ?></a>
            <?if($sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings']){?>
   			<a href="#" class="tool_print tool_print_interview_form"><?=$kis_lang['Admission']['MINGWAI']['printinterviewform']?></a>
    		<?}?>
            <a href="#" class="tool_delete"><?= $kis_lang['delete'] ?></a>
        </div>
        <p class="spacer"></p>

        <form id="application_form" name="application_form" method="post">
            <table class="common_table_list edit_table_list">
                <colgroup>
                    <col nowrap="nowrap">
                </colgroup>
                <thead>
                <tr>
                    <th width="20">#</th>
                    <th><? kis_ui::loadSortButton('application_id', 'applicationno', $sortby, $order) ?></th>
                    <th><? kis_ui::loadSortButton('student_name', 'studentname', $sortby, $order) ?></th>
                    <th><?= $kis_lang['parentorguardian'] ?></th>
                    <th><?= $kis_lang['phoneno'] ?></th>
                    <?= $sys_custom['KIS_Admission']['ICMS']['Settings'] ? '<th>' . $kis_lang['Admission']['icms']['applyDayType'] . '</th>' : '' ?>
                    <th><? kis_ui::loadSortButton('application_status', 'status', $sortby, $order) ?></th>
                    <?= $sys_custom['KIS_Admission']['HKUGAPS']['Settings'] ? '<th>' : '' ?>
                    <? if ($sys_custom['KIS_Admission']['HKUGAPS']['Settings']) {
                        kis_ui::loadSortButton('langspokenathome', 'spokenLanguageForInterview', $sortby, $order);
                    } ?>
                    <?= $sys_custom['KIS_Admission']['HKUGAPS']['Settings'] ? '</th>' : '' ?>
                    <th><input type="checkbox" name="checkmaster"
                               onclick="(this.checked)?setChecked(1,this.form,'applicationAry[]'):setChecked(0,this.form,'applicationAry[]')">
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (count($applicationDetails) > 0) {
                    $idx = $amount * ($page - 1);
                    foreach ($applicationDetails as $_applicationId => $_applicationDetailsAry) {
                        $idx++;
                        $_recordId = $_applicationDetailsAry['other_record_id'];
                        $_status = $_applicationDetailsAry['application_status'];
                        $_studentName = $_applicationDetailsAry['student_name'];
                        $_parentName = implode('<br/>', array_filter($_applicationDetailsAry['parent_name']));
                        $_parentPhone = implode('<br/>', array_filter($_applicationDetailsAry['parent_phone']));
                        if ($sys_custom['KIS_Admission']['ICMS']['Settings']) {
                            $classlevel = $applicationlistAry[$kis_data['classLevelID']]['ClassLevelName'];
                            $_applyDayType = '<td>';
                            for ($i = 1; $i <= 3; $i++) {
                                if ($_applicationDetailsAry['ApplyDayType' . $i])
                                    $_applyDayType .= '(' . $i . ') ' . $kis_lang['Admission']['icms'][$classlevel]['TimeSlot'][$_applicationDetailsAry['ApplyDayType' . $i]] . '<br/>';
                            }
                            $_applyDayType .= '</td>';
                        }
                        if ($sys_custom['KIS_Admission']['HKUGAPS']['Settings']) {
                            $langSpoken = '';
                            if ($_applicationDetailsAry['langspokenathome'] == 'Cantonese') {
                                $langSpoken = $kis_lang['Admission']['Languages']['Cantonese'];
                            } else if ($_applicationDetailsAry['langspokenathome'] == 'Putonghua') {
                                $langSpoken = $kis_lang['Admission']['Languages']['Putonghua'];
                            } else if ($_applicationDetailsAry['langspokenathome'] == 'English') {
                                $langSpoken = $kis_lang['Admission']['Languages']['English'];
                            }
                            $_spokenLanguageForInterview = '<td>' . ($langSpoken ? $langSpoken : '--') . '</td>';
                        }
                        $tr_css = '';
                        switch ($_status) {
                            case 'pending':
                                $tr_css = ' class="absent"';
                                break;
                            //	case 'paymentsettled':$tr_css = '';break;
                            case 'waitingforinterview':
                                $tr_css = ' class="waiting"';
                                break;
                            case 'confirmed':
                                $tr_css = ' class="done"';
                                break;
                            case 'cancelled':
                                $tr_css = ' class="draft"';
                                break;
                        }
                        ?>
                        <tr<?= $tr_css ?>>
                            <td><?= $idx ?></td>
                            <td>
                                <a href="#/apps/admission/applicantslist/details/<?= $schoolYearID ?>/<?= $_recordId ?>/"><?= $_applicationId ?></a><br>
                            </td>
                            <td><?= $sys_custom['KIS_Admission']['ICMS']['Settings'] || $sys_custom['KIS_Admission']['CSM']['Settings'] ? str_replace(",", " ", $_studentName) : $_studentName ?></td>
                            <td><?= $_parentName ? ($sys_custom['KIS_Admission']['ICMS']['Settings'] ? str_replace(",", " ", $_parentName) : $_parentName) : '--' ?></td>
                            <td nowrap="nowrap"><?= $_parentPhone ? $_parentPhone : '--' ?></td>
                            <?= $sys_custom['KIS_Admission']['ICMS']['Settings'] ? $_applyDayType : '' ?>
                            <td><?= $kis_lang['Admission']['Status'][$_status] ?></td>
                            <?= $sys_custom['KIS_Admission']['HKUGAPS']['Settings'] ? $_spokenLanguageForInterview : '' ?>
                            <td><?= $libinterface->Get_Checkbox('application_' . $_recordId, 'applicationAry[]', $_recordId, '', $Class = '', $Display = '', $Onclick = "unset_checkall(this, document.getElementById('application_form'));", $Disabled = '') ?></td>
                        </tr>
                    <?php }
                } else { //no record
                    ?>
                    <tr>
                        <? if ($sys_custom['KIS_Admission']['ICMS']['Settings']) {
                            ?>
                            <td colspan="8" style="text-align:center;"><?= $kis_lang['norecord'] ?></td>
                        <? } else {
                            ?>
                            <td colspan="7" style="text-align:center;"><?= $kis_lang['norecord'] ?></td>
                        <? } ?>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <input type="hidden" name="recordID" id="recordID" value="<?= $recordID ?>">
            <input type="hidden" name="round" id="round" value="<?= $selectInterviewRound ?>">
            <input type="hidden" name="schoolYearID" id="schoolYearID" value="<?= $schoolYearID ?>">
            <input type="hidden" name="classLevelID" id="classLevelID" value="<?= $classLevelID ?>">
        </form>


        <p class="spacer"></p>
        <p class="spacer"></p>
        <br>
    </div>
</div>


<?php if ($sys_custom['KIS_Admission']['CREATIVE']['Settings']): ?>
    <div id='print_label_box' style="padding:5px;display:none;" class="pop_edit">
        <form method="post" action="/kis/admission_form/export_interview_label.php" target="_blank">
            <div id="selectedApplicant" style="display:none;"></div>
            <input type="hidden" name="interviewAry[]" value="<?= $recordID ?>" />
            <input type="hidden" name="schoolYearID" id="schoolYearID" value="<?= $schoolYearID ?>">
            <input type="hidden" name="round" id="round" value="<?= $selectInterviewRound ?>">

            <div class="pop_title">
                <span><?= $kis_lang['Admission']['CREATIVE']['exportApplicantLabel'] ?></span>
            </div>
            <div class="table_board" style="height:100px">

                <table class="form_table">
                    <tr>
                        <td class="field_title" style="width:50%">
                            <label for="showTime"><?= $kis_lang['Admission']['CREATIVE']['showTime'] ?></label>
                        </td>
                        <td style="width:50%">
                            <input type="checkbox" id="showTime" name="showTime" value="1" checked/>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%" class="field_title">
                            <label for="showRoom"><?= $kis_lang['Admission']['CREATIVE']['showRoom'] ?></label>
                        </td>
                        <td style="width:50%">
                            <input type="checkbox" id="showRoom" name="showRoom" value="1" checked/>
                        </td>
                    </tr>
                </table>
                <p class="spacer"></p>
            </div>
            <div class="edit_bottom">
                <input id="submitBtn" name="submitBtn" type="submit" class="formbutton"
                       value="<?= $kis_lang['submit'] ?>"/>
                <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();"
                       value="<?= $kis_lang['cancel'] ?>"/>
            </div>

        </form>
    </div>
<?php endif; ?>