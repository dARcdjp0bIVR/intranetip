<?php
// Editing by Henry

/**
 *	2016-08-09 (Henry): Created file
 **/
 
include_once($intranet_root."/lang/lang.".$intranet_session_language.".php");

$linterface = $kis_data['libinterface'];

?>
<script type="text/javascript" language="JavaScript">
$(function(){
	kis.admission.reports_payment_init({
		invaliddateformat:'<?=$kis_lang['Admission']['msg']['invaliddateformat']?>',
		enddateearlier: '<?=$kis_lang['Admission']['msg']['enddateearlier']?>'
	});
})
</script>
<div class="main_content_detail">
<?
	kis_ui::loadModuleTab(array('paymentreport'), '', '#/apps/admission/reports/');
?>
	<table class="form_table">
    	<tbody>
        	<tr> 
            	<td class="field_title" style="width:30%;"><span style="color:red">*</span><?=$kis_lang['from']?></td>
            	<td style="width:70%;">
            	<input type="text" name="start_date" id="start_date" value="<?=($start_date?$start_date:date('Y-m-01'))?>">&nbsp;<span class="error_msg" id="warning_start_date"></span>
            	</td>
            </tr>
            <tr> 
            	<td class="field_title" style="width:30%;"><span style="color:red">*</span><?=$kis_lang['to']?></td>
            	<td style="width:70%;">
            	<input type="text" name="end_date" id="end_date" value="<?=($end_date?$end_date:date('Y-m-t'))?>">&nbsp;<span class="error_msg" id="warning_end_date"></span>
            	</td>
            </tr>
            <?if($sys_custom['KIS_Admission']['PayPal'] && $admission_cfg['PaymentStatus']){?>
			<!--<tr class="step1">
            	<td class="field_title" style="width:30%;"><?=$kis_lang['status']?></td>
            	<td style="width:70%;">
            	<?=$paymentStatus?>
            	</td>
            </tr>-->
            <?}?>
        </tbody>
    </table>
	<div class="edit_bottom">
    	<input type="button" name="submitBtn" id="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
		<!--<input type="button" name="cancelBtn" id="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />-->
    </div>
    <p class="spacer"></p><br>  
</div>
