<?php
// using:

/**
 * **************************************
 * 2019-11-29 (Henry): file created
 * **************************************
 */
?>
<script type="text/javascript">
kis.admission.module_settings_edit_init = function(msg){
	var schoolYearId = $('#schoolYearId').val();
	var msg_confirm_delete_email_template_attachment = msg.confirm_delete_email_template_attachment;
		
	var sysmsg = msg.returnmsg;
    if (sysmsg != '') {
        var returnmsg = sysmsg.split('|=|')[1];
        returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >[' + msg.close + ']</a>';
        var msgclass = sysmsg.split('|=|')[0] == 1 ? 'system_success' : 'system_alert';
        $('div#system_message').removeClass();
        $('div#system_message').addClass(msgclass).show();
        $('div#system_message span').html(returnmsg);
        setTimeout(function () {
            $('#system_message').hide();
        }, 3000);
    }
    
   var deleteUploadFile = function (obj) {
        $(obj).parent().remove();
    };

    var deleteAttachment = function (obj) {
        if (confirm(msg_confirm_delete_email_template_attachment)) {
            $(obj).parent().remove();
            $('#FileUploadDiv').show();
        }
    };
    
    $('a[name="DeleteFileUpload\\[\\]"]:last').click(function () {
        deleteUploadFile(this);
    });
    
    $('a[name="AttachmentRemoveBtn\\[\\]"]').click(function () {
        deleteAttachment(this);
    });
    
    $('#enableinterviewarrangement').change(function(){
		if(this.checked==false){
			$('#enableinterviewform').prop('checked', false);
			$('#enableinterviewform').prop('disabled', true);
		}
		else{
			$('#enableinterviewform').prop('disabled', false);
		}
	});
        
	$('#module_settings_form').submit(function(){
			$.post('apps/admission/ajax.php?action=updateModuleSettings', $(this).serialize(), function(success){
				$.address.value('/apps/admission/settings/modulesettings/?sysMsg='+success);
			});
		    return false;
		}); 	
	$('input#cancelBtn').click(function(){
		$.address.value('/apps/admission/settings/modulesettings/');
	});
};
$(function(){
    kis.admission.module_settings_edit_init({
        invaliddateformat:'<?=$kis_lang['Admission']['msg']['invaliddateformat']?>',
        enddateearlier: '<?=$kis_lang['Admission']['msg']['enddateearlier']?>',
        emptyapplicationdatewarning: '<?=$kis_lang['Admission']['msg']['emptyapplicationdatewarning']?>',
        selectatleastonetimeslot: '<?=$kis_lang['Admission']['msg']['selectatleastonetimeslot']?>',
        confirm_delete_email_template_attachment: '<?=$kis_lang['msg']['confirm_delete_email_template_attachment']?>','returnmsg': '<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>'
    });
});
</script>
<div class="main_content">
	<form id='module_settings_form' name="module_settings_form">
		<div class="main_content_detail">
    <?php
    kis_ui::loadModuleTab(array(
        'applicationsettings',
        'instructiontoapplicants',
        'attachmentsettings',
        'email_template',
		'printformheader',
		'onlinepayment',
		'modulesettings'
    ), 'modulesettings', '#/apps/admission/settings/');
    ?>
        <p class="spacer"></p>
        <?=$kis_data['NavigationBar']?>
        <p class="spacer"></p>
			<div class="table_board">
				<table id="FormTable" class="form_table">
		        	<tbody>
		        	<tr> 
	       <td width="30%" class="field_title"><?=$kis_lang['interviewarrangement']?></td>
	       <td width="40%"><input type="checkbox" id="enableinterviewarrangement" name="enableinterviewarrangement" value="1" <?=($basicSettings['enableinterviewarrangement']==1?'checked=1':'')?>"></td>
	     </tr>
	     <tr> 
	       <td width="30%" class="field_title"><?=$kis_lang['interviewforminput']?></td>
	       <td width="40%"><input type="checkbox" id="enableinterviewform" name="enableinterviewform" value="1" <?=($basicSettings['enableinterviewform']==1?'checked=1':'')?>" <?=$basicSettings['enableinterviewarrangement']==1?'':'disabled'?>></td>
	     </tr>
	     <tr> 
	       <td width="30%" class="field_title"><?=$kis_lang['briefing']?></td>
	       <td width="40%"><input type="checkbox" id="enablebriefingsession" name="enablebriefingsession" value="1" <?=($basicSettings['enablebriefingsession']==1?'checked=1':'')?>"></td>
	     </tr>
		        		
		            </tbody>
		        </table>
				<p class="spacer"></p>
				<p class="spacer"></p>
				<br />
			</div>
			<div class="edit_bottom">
				<input type="hidden" name="schoolYearId" id="schoolYearId"
					value="<?=$schoolYearID?>"> <input type="submit" id="submitBtn"
					name="submitBtn" class="formbutton"
					value="<?=$kis_lang['submit']?>" /> <input type="button"
					id="cancelBtn" name="cancelBtn" class="formsubbutton"
					value="<?=$kis_lang['cancel']?>" />
			</div>
		</div>
	</form>
</div>

<script>
$(function(){
	$('.multiLang .main_menu a').click(function(e){
		e.preventDefault();

		var $td = $(this).closest('td');
		var newLang = $(this).attr('href');
		newLang = newLang.substring(0, newLang.length - 1); // Remove last '/'

		$td.find('.main_menu li').removeClass('selected');
		$(this).closest('li').addClass('selected');

		$td.find('.multiLangContainer').hide();
		$td.find('.multiLangContainer.' + newLang).show();
	});
	$('.multiLang .main_menu').find('a:first').click();
});
</script>