<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT.'kis/init.php');
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$libkis_admission = $libkis->loadApp('admission');

$Date = $_REQUEST['Date'];
$NumOfGroup = $_REQUEST['NumOfGroup'];
$Quota = $_REQUEST['Quota'];
$RecordID = $_REQUEST['RecordID'];
$NumOfTimeslot = $_REQUEST['NumOfTimeslot'];


$StartTime_hour = array();
$StartTime_min = array();
$EndTime_hour = array();
$EndTime_min = array();

for($i=0; $i<$NumOfTimeslot; $i++){
	if($_REQUEST['StartTime_'.$i.'_hour']){
		$StartTime_hour[] = $_REQUEST['StartTime_'.$i.'_hour'];
		$StartTime_min[] =  $_REQUEST['StartTime_'.$i.'_min'];
		$EndTime_hour[] = $_REQUEST['EndTime_'.$i.'_hour'];
		$EndTime_min[] =  $_REQUEST['EndTime_'.$i.'_min'];
	}
}

$sessionAry = array();
for($i=0; $i<sizeof($StartTime_hour); $i++){
	$sessionAry[$i]['StartTime'] = $StartTime_hour[$i].':'.$StartTime_min[$i].':00';
	$sessionAry[$i]['EndTime'] =  $EndTime_hour[$i].':'.$EndTime_min[$i].':59';
}

if($RecordID==''){
	$is_edit = false;
}else{
	$is_edit = true;
}

$updated_template_id = $libkis_admission->updateInterviewArrangementRecord($Date,$NumOfGroup,$Quota,$sessionAry,$RecordID);

$resultAry = array();

if($updated_template_id != '' && $updated_template_id > 0)
{
	$resultAry['UpdateRecord'] = true;
}else{
	$resultAry['UpdateRecord'] = false;
}

$sysMsg = !in_array(false,$resultAry)? "UpdateSuccess" : "UpdateUnsuccess";
header("Location: /kis/#apps/admission/settings/interviewarrangement/?sysMsg=".$sysMsg);
?>