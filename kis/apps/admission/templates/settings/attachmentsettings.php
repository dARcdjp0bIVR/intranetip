<?php
// Editing by Henry
/**
 * **************************************
 * 2015-07-20 (Henry): added Interview Arrangement tab
 * **************************************
 */
?>
<style type="text/css">
table.common_table_list tr.move_selected td {
	background-color: #fbf786;
	border-top: 2px dashed #d3981a;
	border-bottom: 2px dashed #d3981a
}
</style>
<script type="text/javascript">
$(function(){
	kis.admission.attachment_settings_init(
		{
			returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
			close:'<?=$kis_lang['close']?>',
			request_input_attachment_name: '<?=$kis_lang['msg']['requestinputattachmentname']?>',
			attachment_name_cannot_use: '<?=$kis_lang['msg']['attachmentnamecannotuse']?>',
			confirm_delete_attachment_setting: '<?=$kis_lang['msg']['confirmdeleteattachmentsetting']?>',
			reserved_attachment_names: [<?="'".implode("','",(array)$kis_config['eAdmission']['ReservedAttachmentNames'])."'"?>]
		}
	);
});
</script>
<div class="main_content_detail">
    <?
    if ($sys_custom['KIS_Admission']['ICMS']['Settings']) {
        kis_ui::loadModuleTab(array(
            'applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template'
        ), 'attachmentsettings', '#/apps/admission/settings/');
    }  else if($sys_custom['KIS_Admission']['STANDARD']['Settings']){
    	kis_ui::loadModuleTab(array('applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template','printformheader','onlinepayment', 'modulesettings'), 'attachmentsettings', '#/apps/admission/settings/');
    }
      // else if($sys_custom['KIS_Admission']['InterviewArrangement']){
      // kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template', 'interviewarrangement'), 'attachmentsettings', '#/apps/admission/settings/');
      // }
    else {
        kis_ui::loadModuleTab(array(
            'applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template'
        ), 'attachmentsettings', '#/apps/admission/settings/');
    }
    ?>
    <p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
    <div id="table_filter">
		<form class="filter_form"></form>
	</div>

	<p class="spacer"></p>
	<div class="common_table_tool">
		<a class="tool_new"
			href="#/apps/admission/settings/attachmentsettings/edit"><?=$kis_lang['new']?></a>
	</div>
	<div class="text_remark">
		<span>&nbsp;&nbsp;(<?=($sys_custom['KIS_Admission']['STANDARD']['Settings']?$kis_lang['Admission']['STANDARD']['msg']['birthCertFormat']:$kis_lang['Admission']['msg']['birthCertFormat']).($maxUploadSize?$maxUploadSize:'1')?> MB) </span>
	</div>
	<div class="table_board">
		<table class="common_table_list edit_table_list">
			<colgroup>
				<col nowrap="nowrap">
			</colgroup>
			<thead>
				<tr>
					<th width="20">&nbsp;</th>
					<th width=""><?=$kis_lang['attachmentname'] ?></th>
				  <?php if($sys_custom['KIS_Admission']['DocUploadManualMandatory']){ ?>
				  	<th width="16%"><?=$kis_lang['mandatoryoptional']?></th>
				  <?php } ?>
				  <?php if($sys_custom['KIS_Admission']['DocUploadExtraDoc']){ ?>
				  	<th width="16%"><?=$kis_lang['Admission']['additionalAttachment']?></th>
				  <?php } ?>
				  <?php if($sys_custom['KIS_Admission']['DocUploadCustomClassLevel']){ ?>
				  	<th width="16%"><?=$kis_lang['form']?></th>
				  <?php } ?>
				  <th width="16%"><?=$kis_lang['lastupdated']?></th>
					<th width="16%">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
		  	<?php
    $x = '';
    if (sizeof($attachmentSettings) > 0) {
        $index = 0;
        foreach ($attachmentSettings as $key => $record) {
            $index ++;

            $attachmentName = $record['AttachmentName'];
            if ($admission_cfg['MultipleLang'] && !$sys_custom['KIS_Admission']['CREATIVE']['Settings'] && !$sys_custom['KIS_Admission']['STANDARD']['Settings']) {
                foreach($admission_cfg['Lang'] as $_index => $_lang){
                    if($_lang == $intranet_session_language){
                        $attachmentName = $record["AttachmentName{$_index}"];
                        break;
                    }
                }
            }
            
            $x .= '<tr id="row_' . $record['RecordID'] . '">';
            $x .= '<td class="Dragable">' . $index . '</td>';
            $x .= '<td class="Dragable"><a class="td_form_name" href="#/apps/admission/settings/attachmentsettings/edit/' . $record['RecordID'] . '">' . $attachmentName . '</a></td>';
            
            if ($sys_custom['KIS_Admission']['DocUploadManualMandatory']) {
                $x .= '<td class="Dragable">' . (($record['IsOptional']) ? $kis_lang['optional'] : $kis_lang['mandatory']) . '</td>';
            }
            if ($sys_custom['KIS_Admission']['DocUploadExtraDoc']) {
                $x .= '<td class="Dragable">' . (($record['IsExtra']) ? $kis_lang['yes'] : $kis_lang['no']) . '</td>';
            }
            
            if ($sys_custom['KIS_Admission']['DocUploadCustomClassLevel']) {
                if ($record['ClassLevelStr']) {
                    $classLevelIdArr = explode(',', $record['ClassLevelStr']);
                    $classLevelStr = '';
                    foreach ($classLevelIdArr as $classLevelId) {
                        $classLevelStr .= $kis_data['libadmission']->classLevelAry[$classLevelId] . ',';
                    }
                    $classLevelStr = trim($classLevelStr, ',');
                } else {
                    $classLevelStr = $kis_lang['allforms'];
                }
                // debug_r($kis_data['libadmission']->classLevelAry);
                $x .= '<td class="Dragable">' . $classLevelStr . '</td>';
            }
            
            $x .= '<td class="Dragable">' . $record['DateModified'] . '</td>';
            $x .= '<td class="Dragable">';
            $x .= '<div class="table_row_tool">';
            $x .= '<a title="' . $kis_lang['edit'] . '" class="edit_dim" href="#/apps/admission/settings/attachmentsettings/edit/' . $record['RecordID'] . '" ></a>';
            $x .= '<a title="' . $kis_lang['delete'] . '" class="delete_dim" href="javascript:void(0);"><input type="hidden" name="RecordID[]" value="' . $record['RecordID'] . '" /></a>';
            $x .= '</div>';
            $x .= '</td>';
            $x .= '</tr>';
        }
    } else {
        $x .= '<tr><td colspan="4" style="text-align:center;">' . $kis_lang['norecord'] . '</td></tr>';
    }
    echo $x;
    ?>	
			</tbody>
		</table>
		<p class="spacer"></p>
		<p class="spacer"></p>
		<br>
	</div>
</div>