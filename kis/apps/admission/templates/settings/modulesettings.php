<?php
// Editing by Henry
/****************************************
 *	2019-12-03 (Henry):	created this page
 ****************************************/
?>
<script type="text/javascript">
$(function(){
	kis.admission.module_settings_init({
		'close':'<?=$kis_lang['close']?>',
		'delete':'<?=$kis_lang['delete']?>',
		'confirm_delete_email_template':'<?=$kis_lang['msg']['deleteinterviewarrangement']?>',
		'returnmsg':'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>'
	});
});
</script>
<div class="main_content_detail">
	<? 
    if($sys_custom['KIS_Admission']['ICMS']['Settings']){
    	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template','printformheader','onlinepayment', 'modulesettings'),'modulesettings', '#/apps/admission/settings/');
    }
//    else if($sys_custom['KIS_Admission']['TBCPK']['Settings']){
//    	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants'), 'instructiontoapplicants', '#/apps/admission/settings/');
//    }
//    else if($sys_custom['KIS_Admission']['InterviewArrangement']){
//	    kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template', 'interviewarrangement'), 'instructiontoapplicants', '#/apps/admission/settings/');
//	}
    else{
    	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template','printformheader','onlinepayment', 'modulesettings'),'modulesettings', '#/apps/admission/settings/');
    }
    ?><p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	<div class="common_table_tool">
    	<a class="tool_edit" href="#/apps/admission/settings/modulesettings/edit/"><?=$kis_lang['edit']?></a>
  	</div>
	<table class="form_table">
	     <tbody>
	     <tr> 
	       <td width="30%" class="field_title"><?=$kis_lang['interviewarrangement']?></td>
	       <td width="40%"><?=$basicSettings['enableinterviewarrangement']>0?$kis_lang['enable']:$kis_lang['disable']?></td>
	     </tr>
	     <tr> 
	       <td width="30%" class="field_title"><?=$kis_lang['interviewforminput']?></td>
	       <td width="40%"><?=$basicSettings['enableinterviewform']>0?$kis_lang['enable']:$kis_lang['disable']?></td>
	     </tr>
	     <tr> 
	       <td width="30%" class="field_title"><?=$kis_lang['briefing']?></td>
	       <td width="40%"><?=$basicSettings['enablebriefingsession']>0?$kis_lang['enable']:$kis_lang['disable']?></td>
	     </tr>
	   </tbody></table>
</div>
