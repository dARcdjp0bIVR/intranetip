<?php
// Editing by 
/****************************************
 *	2015-07-20 (Henry):	added Interview Arrangement tab
 ****************************************/
?>
<script type="text/javascript">
$(function(){
	kis.admission.application_settings_init(
		{
			returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
			close:'<?=$kis_lang['close']?>'
		}
		
	);
});
</script>
<div class="main_content_detail">
    <? 
    if($sys_custom['KIS_Admission']['ICMS']['Settings']){
    	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template'), 'instructiontoapplicants', '#/apps/admission/settings/');
    }
else if($sys_custom['KIS_Admission']['STANDARD']['Settings']){
    	kis_ui::loadModuleTab(array('applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template','printformheader','onlinepayment', 'modulesettings'), 'instructiontoapplicants', '#/apps/admission/settings/');
    }
//    else if($sys_custom['KIS_Admission']['InterviewArrangement']){
//	    kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template', 'interviewarrangement'), 'instructiontoapplicants', '#/apps/admission/settings/');
//	}
    else{
    	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template'), 'instructiontoapplicants', '#/apps/admission/settings/');
    }
    ?><p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>    
    <div id="table_filter">
    <form class="filter_form"> 
        <?=$schoolYearSelection?>
    </form>
    </div>
    <p class="spacer"></p>
    <div class="common_table_tool">
    	<a class="tool_edit" href="#/apps/admission/settings/instructiontoapplicants/edit/"><?=$kis_lang['edit']?></a>
    </div>
    <div class="table_board">
    <table class="form_table">
    	<tr>
    		<td>
    			<div class="admission_board">
					<p class="spacer"></p>
				    <div class="admission_complete_msg"><?=$basicSettings['generalInstruction']?></div>
				</div>
	        </td>
        </tr>       
        </table>
	</div>
</div>
