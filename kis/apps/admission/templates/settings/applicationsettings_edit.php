<?php
// using:

/**
 * **************************************
 * 2016-02-03 (Henry): support termType for tbcpk
 * 2015-09-25 (Henry): developing
 * 2015-07-20 (Henry): added Interview Arrangement tab
 * **************************************
 */
?>
<script>
$(function(){
    kis.admission.application_period_edit_init({
        invaliddateformat:'<?=$kis_lang['Admission']['msg']['invaliddateformat']?>',
        enddateearlier: '<?=$kis_lang['Admission']['msg']['enddateearlier']?>',
        emptyapplicationdatewarning: '<?=$kis_lang['Admission']['msg']['emptyapplicationdatewarning']?>',
        selectatleastonetimeslot: '<?=$kis_lang['Admission']['msg']['selectatleastonetimeslot']?>'
    });
});
</script>
<div class="main_content">
	<form id='application_period_form'>
		<div class="main_content_detail">
    <?php
    if ($sys_custom['KIS_Admission']['ICMS']['Settings']) {
        kis_ui::loadModuleTab(array(
            'applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template'
        ), '', '#/apps/admission/settings/');
    }  else if($sys_custom['KIS_Admission']['STANDARD']['Settings']){
    	kis_ui::loadModuleTab(array('applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template','printformheader','onlinepayment', 'modulesettings'), '', '#/apps/admission/settings/');
    }
      // else if($sys_custom['KIS_Admission']['InterviewArrangement']){
      // kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template', 'interviewarrangement'), '', '#/apps/admission/settings/');
      // }
    else {
        kis_ui::loadModuleTab(array(
            'applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template'
        ), '', '#/apps/admission/settings/');
    }
    ?>
        <p class="spacer"></p>
        <?=$kis_data['NavigationBar']?>
        <p class="spacer"></p>
			<div class="table_board">
				<table class="form_table">
					<tr>
						<td class="field_title" width="25%"><?=$kis_lang['form']?></td>
						<td width="75%"><?=$classLevelSelection?></td>
					</tr>                
                <?php if($sys_custom['KIS_Admission']['PreviewFormMode']){?>
                    <?php
                    list ($previewStartDate, $previewStartTime) = explode(" ", $applicationSetting['PreviewStartDate']);
                    list ($previewEndDate, $previewEndTime) = explode(" ", $applicationSetting['PreviewEndDate']);
                    ?>
                    <tr>
						<td class="field_title"><?=$kis_lang['preview']?> (<?=$kis_lang['from']?>)</td>
						<td><input type="text" name="preview_start_date"
							id="preview_start_date" value="<?=$previewStartDate?>">&nbsp;<span
							class="error_msg" id="warning_preview_start_date"></span><?=$libadmission->Get_Time_Selection($libinterface,'preview_start_time',$previewStartTime,'',false)?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['preview']?> (<?=$kis_lang['to']?>)</td>
						<td><input type="text" name="preview_end_date"
							id="preview_end_date" value="<?=$previewEndDate?>">&nbsp;<span
							class="error_msg" id="warning_preview_end_date"></span><?=$libadmission->Get_Time_Selection($libinterface,'preview_end_time',($previewEndDate?$previewEndTime:'23:59:59'),'',false)?></td>
					</tr>
                <?php } ?>
                <?php
                list ($startDate, $startTime) = explode(" ", $applicationSetting['StartDate']);
                list ($endDate, $endTime) = explode(" ", $applicationSetting['EndDate']);
                ?>
                <tr>
						<td class="field_title"><?=$kis_lang['from']?></td>
						<td><input type="text" name="start_date" id="start_date"
							value="<?=$startDate?>">&nbsp;<span class="error_msg"
							id="warning_start_date"></span><?=$libadmission->Get_Time_Selection($libinterface,'start_time',$startTime,'',false)?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['to']?></td>
						<td><input type="text" name="end_date" id="end_date"
							value="<?=$endDate?>">&nbsp;<span class="error_msg"
							id="warning_end_date"></span><?=$libadmission->Get_Time_Selection($libinterface,'end_time',($endDate?$endTime:'23:59:59'),'',false)?></td>
					</tr>
                <?php if($sys_custom['KIS_Admission']['ApplicantUpdateForm']){?>
                    <?php
                    list ($updateStartDate, $updateStartTime) = explode(" ", $applicationSetting['UpdateStartDate']);
                    list ($updateEndDate, $updateEndTime) = explode(" ", $applicationSetting['UpdateEndDate']);
                    ?>
                    <tr>
						<td class="field_title"><?=($sys_custom['KIS_Admission']['MGF']['Settings'])?$kis_lang['Admission']['mgf']['viewInterview'] : $kis_lang['modification']?> (<?=$kis_lang['from']?>)</td>
						<td><input type="text" name="update_start_date"
							id="update_start_date" value="<?=$updateStartDate?>">&nbsp;<span
							class="error_msg" id="warning_update_start_date"></span><?=$libadmission->Get_Time_Selection($libinterface,'update_start_time',$updateStartTime,'',false)?></td>
					</tr>
					<tr>
						<td class="field_title"><?=($sys_custom['KIS_Admission']['MGF']['Settings'])?$kis_lang['Admission']['mgf']['viewInterview'] : $kis_lang['modification']?> (<?=$kis_lang['to']?>)</td>
						<td><input type="text" name="update_end_date" id="update_end_date"
							value="<?=$updateEndDate?>">&nbsp;<span class="error_msg"
							id="warning_update_end_date"></span><?=$libadmission->Get_Time_Selection($libinterface,'update_end_time',($updateEndDate?$updateEndTime:'23:59:59'),'',false)?></td>
					</tr>
                <?php } ?>
                <?php if($sys_custom['KIS_Admission']['DocUploadExtraDoc']){?>
                    <?php
                    list ($updateExtraAttachmentStartDate, $updateExtraAttachmentStartTime) = explode(" ", $applicationSetting['UpdateExtraAttachmentStartDate']);
                    list ($updateExtraAttachmentEndDate, $updateExtraAttachmentEndTime) = explode(" ", $applicationSetting['UpdateExtraAttachmentEndDate']);
                    ?>
                    <tr>
						<td class="field_title"><?=$kis_lang['modification_extra_doc']?> (<?=$kis_lang['from']?>)</td>
						<td><input type="text" name="update_extra_attachment_start_date"
							id="update_extra_attachment_start_date" value="<?=$updateExtraAttachmentStartDate?>">&nbsp;<span
							class="error_msg" id="warning_update_extra_attachment_start_date"></span><?=$libadmission->Get_Time_Selection($libinterface,'update_extra_attachment_start_time',$updateExtraAttachmentStartTime,'',false)?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['modification_extra_doc']?> (<?=$kis_lang['to']?>)</td>
						<td><input type="text" name="update_extra_attachment_end_date" id="update_extra_attachment_end_date"
							value="<?=$updateExtraAttachmentEndDate?>">&nbsp;<span class="error_msg"
							id="warning_update_extra_attachment_end_date"></span><?=$libadmission->Get_Time_Selection($libinterface,'update_extra_attachment_end_time',($updateExtraAttachmentEndDate?$updateExtraAttachmentEndTime:'23:59:59'),'',false)?></td>
					</tr>
                <?php } ?>
                <?php if($sys_custom['KIS_Admission']['ApplyQuotaSettings']){?>
                    <tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['qouta']?></td>
						<td><input type="text" name="quota" id="quota"
							value="<?=$applicationSetting['Quota']?>" /></td>
					</tr>
                <?php } ?>
                <?php if(!$sys_custom['KIS_Admission']['ICMS']['Settings'] && !$sys_custom['KIS_Admission']['MUNSANG']['Settings']){?>
                <tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['availabletimeslot']?></td>
						<td>
                        <?php
                    $_dayType = ! empty($applicationSetting['DayType']) ? explode(',', $applicationSetting['DayType']) : array();
                    foreach ($kis_lang['Admission']['TimeSlot'] as $_key => $_timeSlot) :
                        $_hasCheck = (count($_dayType) > 0) ? (in_array($_key, $_dayType)) ? true : false : false;
                        ?>
                              <input type="checkbox"
							class="timeSlotSettings" name="timeSlotSettings[]"
							id="timeslot_<?=$_key?>" value="<?=$_key?>"
							<?=$_hasCheck?' checked="checked""':''?> /><label
							for="timeslot_<?=$_key?>"><?=$_timeSlot?></label> 
                        <?php endforeach; ?>    
                    </td>
					</tr>
                <?php } ?>
                <?php if($sys_custom['KIS_Admission']['TBCPK']['Settings']){?>
                    <tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['applyTerm']?></td>
						<td>
                            <?php
                    $_termType = ! empty($applicationSetting['TermType']) ? explode(',', $applicationSetting['TermType']) : array();
                    foreach ($kis_lang['Admission']['Term'] as $_key => $_term) :
                        $_hasCheck = (count($_termType) > 0) ? (in_array($_key, $_termType)) ? true : false : false;
                        ?>
                                  <input type="checkbox"
							class="termSettings" name="termSettings[]" id="term_<?=$_key?>"
							value="<?=$_key?>" <?=$_hasCheck?' checked="checked""':''?> /><label
							for="term_<?=$_key?>"><?=$_term?></label> 
                            <?php endforeach; ?>    
                        </td>
					</tr>
                <?php } ?>
                
                <?php if($sys_custom['KIS_Admission']['PFK']['Settings']): ?>
                    <tr>
						<td class="field_title"><?=$kis_lang['Admission']['PFK']['school']?></td>
						<td>
                            <?php
                    $selectSchoolArr = explode(',', $applicationSetting['CustSetting']);
                    foreach ($admission_cfg['SchoolType'] as $typeId => $type) :
                        $checked = (in_array($typeId, $selectSchoolArr)) ? 'checked' : '';
                        ?>
                        	<input type="checkbox"
							class="schoolTypeSettings" name="schoolTypeSettings[]"
							id="schoolType_<?=$typeId?>" value="<?=$typeId?>" <?=$checked ?> />
							<label for="schoolType_<?=$typeId?>">
                                    <?=$kis_lang['Admission']['PFK']['schoolType'][$type]?>
                                </label> 
                            <?php
                    endforeach
                    ;
                    ?>
                        </td>
					</tr>
                <?php endif; ?>
                
                <tr>
						<td class="field_title"><?=$kis_lang['Admission']['studentbirthdayrange']?></td>
						<td><input type="text" name="bday_start_date" id="bday_start_date"
							value="<?=$applicationSetting['DOBStart']?>">&nbsp;<span
							class="error_msg" id="warning_bday_start_date"></span><?=$kis_lang['to']?> <input
							type="text" name="bday_end_date" id="bday_end_date"
							value="<?=$applicationSetting['DOBEnd']?>">&nbsp;<span
							class="error_msg" id="warning_bday_end_date"></span></td>
					</tr>
				<?php if($sys_custom['KIS_Admission']['CREATIVE']['Settings'] || $sys_custom['KIS_Admission']['STANDARD']['Settings']): ?>

                    <tr
                        <?=$sys_custom['KIS_Admission']['ICMS']['CasaNProCasa']?'style="display:none"':''?>>
                        <td class="field_title"><?=$sys_custom['KIS_Admission']['ICMS']['Settings']?$kis_lang['Admission']['icms']['regandpayinfo']:$kis_lang['Admission']['contentonfirstpage'].'<br/>'.$kis_lang['Admission']['contentonfirstpageremark']?></td>
                        <td><textarea id="firstPageContent" name="firstPageContent"><?=$applicationSetting['FirstPageContent']?></textarea></td>
                    </tr>
                    <tr>
                        <td class="field_title"><?=$kis_lang['Admission']['contentonlastpage'].'<br/>'.$kis_lang['Admission']['contentonlastpageremark']?></td>
                        <td><textarea id="lastPageContent" name="lastPageContent"><?=$applicationSetting['LastPageContent']?></textarea></td>
                    </tr>

                    <tr>
                        <td class="field_title">
                            <?=$kis_lang['Admission']['contentonemail']?>
                            <br />
                            <?=$kis_lang['Admission']['contentonemailremark']?>
                        </td>
                        <?php if($sys_custom['KIS_Admission']['STANDARD']['Settings']): ?>
                        <td><textarea id="emailContent" name="emailContent"><?=$applicationSetting['EmailContent']?></textarea></td>
                        <?php else: ?>
                        <td class="multiLang">
                            <?php kis_ui::loadModuleTab($admission_cfg['Lang'],'','',-1)?>

                            <?php
                            foreach ($admission_cfg['Lang'] as $key => $lang) :
                                $display = ($lang == $admission_cfg['DefaultLang']) ? '' : 'display:none;';
                                ?>
                                <div class="<?=$lang ?> multiLangContainer" style="<?=$display ?>">
								<textarea id="emailContent<?=$key ?>"
                                          name="emailContent<?=$key ?>"><?=$applicationSetting["EmailContent{$key}"]?></textarea>
                                </div>
                            <?php
                            endforeach
                            ;
                            ?>
                        </td>
                        <?php endif; ?>
                    </tr>

                <?php elseif($admission_cfg['MultipleLang']): ?>

					<tr>
						<td class="field_title">
							<?=$kis_lang['Admission']['contentonfirstpage']?>
							<br />
							<?=$kis_lang['Admission']['contentonfirstpageremark']?>
						</td>
						<td class="multiLang">
    						<?php kis_ui::loadModuleTab($admission_cfg['Lang'],'','',-1)?>
							
							<?php
        foreach ($admission_cfg['Lang'] as $key => $lang) :
            $display = ($lang == $admission_cfg['DefaultLang']) ? '' : 'display:none;';
            ?>
    							<div class="<?=$lang ?> multiLangContainer" style="<?=$display ?>">
								<textarea id="firstPageContent<?=$key ?>"
									name="firstPageContent<?=$key ?>"><?=$applicationSetting["FirstPageContent{$key}"]?></textarea>
							</div>
							<?php
        endforeach
        ;
        ?>
						</td>
					</tr>
					<tr>
						<td class="field_title">
							<?=$kis_lang['Admission']['contentonlastpage']?>
							<br />
							<?=$kis_lang['Admission']['contentonlastpageremark']?>
						</td>
						<td class="multiLang">
    						<?php kis_ui::loadModuleTab($admission_cfg['Lang'],'','',-1)?>
						
							<?php
        foreach ($admission_cfg['Lang'] as $key => $lang) :
            $display = ($lang == $admission_cfg['DefaultLang']) ? '' : 'display:none;';
            ?>
    							<div class="<?=$lang ?> multiLangContainer" style="<?=$display ?>">
								<textarea id="lastPageContent<?=$key ?>"
									name="lastPageContent<?=$key ?>"><?=$applicationSetting["LastPageContent{$key}"]?></textarea>
							</div>
							<?php
        endforeach
        ;
        ?>
						</td>
					</tr>
					<tr>
						<td class="field_title">
							<?=$kis_lang['Admission']['contentonemail']?>
							<br />
							<?=$kis_lang['Admission']['contentonemailremark']?>
						</td>
						<td class="multiLang">
    						<?php kis_ui::loadModuleTab($admission_cfg['Lang'],'','',-1)?>
						
							<?php
        foreach ($admission_cfg['Lang'] as $key => $lang) :
            $display = ($lang == $admission_cfg['DefaultLang']) ? '' : 'display:none;';
            ?>
    							<div class="<?=$lang ?> multiLangContainer" style="<?=$display ?>">
								<textarea id="emailContent<?=$key ?>"
									name="emailContent<?=$key ?>"><?=$applicationSetting["EmailContent{$key}"]?></textarea>
							</div>
							<?php
        endforeach
        ;
        ?>
						</td>
					</tr>
					
				<?php else: ?>
					<tr
						<?=$sys_custom['KIS_Admission']['ICMS']['CasaNProCasa']?'style="display:none"':''?>>
						<td class="field_title"><?=$sys_custom['KIS_Admission']['ICMS']['Settings']?$kis_lang['Admission']['icms']['regandpayinfo']:$kis_lang['Admission']['contentonfirstpage'].'<br/>'.$kis_lang['Admission']['contentonfirstpageremark']?></td>
						<td><textarea id="firstPageContent" name="firstPageContent"><?=$applicationSetting['FirstPageContent']?></textarea></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['Admission']['contentonlastpage'].'<br/>'.$kis_lang['Admission']['contentonlastpageremark']?></td>
						<td><textarea id="lastPageContent" name="lastPageContent"><?=$applicationSetting['LastPageContent']?></textarea></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['Admission']['contentonemail'].'<br/>'.$kis_lang['Admission']['contentonemailremark']?></td>
						<td><textarea id="emailContent" name="emailContent"><?=$applicationSetting['EmailContent']?></textarea></td>
					</tr>
				<?php endif; ?>
				
                <?php if($sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['HKUGAC']['Settings'] || $sys_custom['KIS_Admission']['STANDARD']['Settings']){?>
                <tr>
						<td class="field_title"><?=$kis_lang['Admission']['openForInternalUse']?></td>
						<td><input type="checkbox" name="allowInternalUse"
							id="allowInternalUse" value="1"
							<?=$applicationSetting['AllowInternalUse']?' checked="checked""':''?> /></td>
					</tr>
                <?php } ?> 
                  
            </table>
				<p class="spacer"></p>
				<p class="spacer"></p>
				<br />
			</div>
			<div class="text_remark"><?=$kis_lang['requiredfield']?></div>
			<div class="edit_bottom">
				<input type="hidden" name="schoolYearId" id="schoolYearId"
					value="<?=$schoolYearID?>"> <input type="submit" id="submitBtn"
					name="submitBtn" class="formbutton"
					value="<?=$kis_lang['submit']?>" /> <input type="button"
					id="cancelBtn" name="cancelBtn" class="formsubbutton"
					value="<?=$kis_lang['cancel']?>" />
			</div>
		</div>
	</form>
</div>

<script>
$(function(){
	$('.multiLang .main_menu a').click(function(e){
		e.preventDefault();

		var $td = $(this).closest('td');
		var newLang = $(this).attr('href');
		newLang = newLang.substring(0, newLang.length - 1); // Remove last '/'

		$td.find('.main_menu li').removeClass('selected');
		$(this).closest('li').addClass('selected');

		$td.find('.multiLangContainer').hide();
		$td.find('.multiLangContainer.' + newLang).show();
	});
	$('.multiLang .main_menu').find('a:first').click();
});
</script>