<?php
// using:
/**
 * **************************************
 * 2015-07-20 (Henry): added Interview Arrangement tab
 * **************************************
 */
?>
<script>
$(function(){
	kis.admission.attachment_settings_init(
		{
			returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
			close:'<?=$kis_lang['close']?>',
			request_input_attachment_name: '<?=$kis_lang['msg']['requestinputattachmentname']?>',
			attachment_name_cannot_use: '<?=$kis_lang['msg']['attachmentnamecannotuse']?>',
			confirm_delete_attachment_setting: '<?=$kis_lang['msg']['confirmdeleteattachmentsetting']?>',
			reserved_attachment_names: [<?="'".implode("','",(array)$kis_config['eAdmission']['ReservedAttachmentNames'])."'"?>]
		}
	);
});
</script>
<div class="main_content">
	<form id='attachment_settings_form' method="post"
		onsubmit="return false;">
		<div class="main_content_detail">
    <?
    if ($sys_custom['KIS_Admission']['ICMS']['Settings']) {
        kis_ui::loadModuleTab(array(
            'applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template'
        ), 'attachmentsettings', '#/apps/admission/settings/');
    }  else if($sys_custom['KIS_Admission']['STANDARD']['Settings']){
    	kis_ui::loadModuleTab(array('applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template','printformheader','onlinepayment', 'modulesettings'), 'attachmentsettings', '#/apps/admission/settings/');
    }
      // else if($sys_custom['KIS_Admission']['InterviewArrangement']){
      // kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template', 'interviewarrangement'), 'attachmentsettings', '#/apps/admission/settings/');
      // }
    else {
        kis_ui::loadModuleTab(array(
            'applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template'
        ), 'attachmentsettings', '#/apps/admission/settings/');
    }
    ?>
        <p class="spacer"></p>
        <?=$kis_data['NavigationBar']?>
        <p class="spacer"></p>
			<div class="table_board">
				<table class="form_table">
					<colgroup>
						<col style="width: 30%" />
						<col />
					</colgroup>
				<?php if($admission_cfg['MultipleLang'] && !$sys_custom['KIS_Admission']['CREATIVE']['Settings'] && !$sys_custom['KIS_Admission']['STANDARD']['Settings']): ?>
				
    				<?php foreach($admission_cfg['Lang'] as $index => $lang): ?>
    				<tr>
						<td class="field_title">
    						<?=$mustfillinsymbol?>
    						<?=$kis_lang['attachmentname']?>
    						( <?=$kis_lang[$lang] ?> )
						</td>
						<td><input type="text" name="AttachmentName<?=$index ?>"
							id="AttachmentName<?=$index ?>"
							value="<?=$attachmentSettings[0]["AttachmentName{$index}"]?>"
							maxlength="255">&nbsp; <span class="error_msg warning_attachment_name"
							id="warning_attachment_name<?=$index ?>"></span></td>
					</tr>
    				<?php endforeach; ?>
					
				<?php else: ?>
    				<tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['attachmentname']?></td>
						<td><input type="text" name="AttachmentName" id="AttachmentName"
							value="<?=$attachmentSettings[0]['AttachmentName']?>"
							maxlength="255">&nbsp; <span class="error_msg warning_attachment_name"
							id="warning_attachment_name"></span></td>
					</tr>
				<?php endif; ?>
						
				<?php if($sys_custom['KIS_Admission']['DocUploadManualMandatory']){ ?>								  
    				<tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['mandatoryoptional']?></td>
						<td><input type="radio" name="AttachmentIsOptional"
							id="AttachmentIsOptional_Mandatory" value="0"
							<?=($attachmentSettings[0]['IsOptional'])? '' : 'checked' ?> /> <label
							for="AttachmentIsOptional_Mandatory"><?=$kis_lang['mandatory'] ?></label>

							&nbsp;&nbsp; <input type="radio" name="AttachmentIsOptional"
							id="AttachmentIsOptional_Optional" value="1"
							<?=($attachmentSettings[0]['IsOptional'])? 'checked' : '' ?> /> <label
							for="AttachmentIsOptional_Optional"><?=$kis_lang['optional'] ?></label>
						</td>
					</tr>										  
				<?php }else{ ?>
					<input type="hidden" name="AttachmentIsOptional" value="1" />
				<?php } ?>
				
				<?php if($sys_custom['KIS_Admission']['DocUploadExtraDoc']){ ?>
                    <tr>
                        <td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['additionalAttachment']?></td>
                        <td>
                            <input type="radio" name="AttachmentIsExtra" id="AttachmentIsExtra_Y" value="1" <?=($attachmentSettings[0]['IsExtra'])? 'checked' : '' ?> />
                            <label for="AttachmentIsExtra_Y"><?=$kis_lang['Admission']['yes'] ?></label>

                            &nbsp;&nbsp; <input type="radio" name="AttachmentIsExtra" id="AttachmentIsExtra_N" value="0" <?=($attachmentSettings[0]['IsExtra'])? '' : 'checked' ?> />
                            <label for="AttachmentIsExtra_N"><?=$kis_lang['Admission']['no'] ?></label>
                        </td>
                    </tr>
                <?php }else{ ?>
                    <input type="hidden" name="IsExtra" value="0" />
                <?php } ?>

				<?php if($sys_custom['KIS_Admission']['DocUploadCustomClassLevel']){ ?>
    				<tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['form']?></td>
						<td>
    						<?php
        $classLevelIdArr = explode(',', $attachmentSettings[0]['ClassLevelStr']);
        foreach ($kis_data['libadmission']->classLevelAry as $classLevelId => $classLevel) {
            $checked = (in_array($classLevelId, $classLevelIdArr)) ? 'checked' : '';
            if (empty($attachmentSettings[0]['ClassLevelStr'])) {
                $checked = 'checked';
            }
            ?>
    						    <input type="checkbox" name="AttachmentYearClassID[]"
							id="AttachmentYearClassID_<?=$classLevelId ?>"
							value="<?=$classLevelId ?>" <?=$checked ?> /> <label
							for="AttachmentYearClassID_<?=$classLevelId ?>"><?=$classLevel ?></label>
							<br />
						    <?php
        }
        ?>
    					</td>
					</tr>										  
				<?php } ?>
		    </table>
				<p class="spacer"></p>
				<p class="spacer"></p>
				<br />
			</div>
			<div class="text_remark"><?=$kis_lang['requiredfield']?></div>
			<div class="text_remark"><?=$kis_lang['msg']['attachment_settings_remark']?></div>
			<div class="edit_bottom">
				<input type="hidden" name="RecordID" id="RecordID"
					value="<?=$attachmentSettings[0]['RecordID']?>"> <input
					type="button" id="submitBtn" name="submitBtn" class="formbutton"
					value="<?=$kis_lang['submit']?>" /> <input type="button"
					id="cancelBtn" name="cancelBtn" class="formsubbutton"
					value="<?=$kis_lang['cancel']?>" />
			</div>
		</div>
	</form>
</div>

