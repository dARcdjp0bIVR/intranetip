<?php
// using:

/**
 * **************************************
 * 2019-11-29 (Henry): file created
 * **************************************
 */
?>
<script type="text/javascript">
kis.admission.online_payment_edit_init = function(msg){
	var schoolYearId = $('#schoolYearId').val();
	var msg_confirm_delete_email_template_attachment = msg.confirm_delete_email_template_attachment;
		
	var sysmsg = msg.returnmsg;
    if (sysmsg != '') {
        var returnmsg = sysmsg.split('|=|')[1];
        returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >[' + msg.close + ']</a>';
        var msgclass = sysmsg.split('|=|')[0] == 1 ? 'system_success' : 'system_alert';
        $('div#system_message').removeClass();
        $('div#system_message').addClass(msgclass).show();
        $('div#system_message span').html(returnmsg);
        setTimeout(function () {
            $('#system_message').hide();
        }, 3000);
    }
    
   var deleteUploadFile = function (obj) {
        $(obj).parent().remove();
    };

    var deleteAttachment = function (obj) {
        if (confirm(msg_confirm_delete_email_template_attachment)) {
            $(obj).parent().remove();
            $('#FileUploadDiv').show();
        }
    };
    
    $('a[name="DeleteFileUpload\\[\\]"]:last').click(function () {
        deleteUploadFile(this);
    });
    
    $('a[name="AttachmentRemoveBtn\\[\\]"]').click(function () {
        deleteAttachment(this);
    });
        
	$('#online_payment_form').submit(function(){
			$.post('apps/admission/ajax.php?action=updateOnlinePaymentSettings', $(this).serialize(), function(success){
				$.address.value('/apps/admission/settings/onlinepayment/?sysMsg='+success);
			});
		    return false;
		}); 	
	$('input#cancelBtn').click(function(){
		$.address.value('/apps/admission/settings/onlinepayment/');
	});
	
	$('#enablepaypal').change(function(){
		if(this.checked==false){
			$('.paypal_tr').hide();
		}
		else{
			$('.paypal_tr').show();
		}
	});
};
$(function(){
    kis.admission.online_payment_edit_init({
        invaliddateformat:'<?=$kis_lang['Admission']['msg']['invaliddateformat']?>',
        enddateearlier: '<?=$kis_lang['Admission']['msg']['enddateearlier']?>',
        emptyapplicationdatewarning: '<?=$kis_lang['Admission']['msg']['emptyapplicationdatewarning']?>',
        selectatleastonetimeslot: '<?=$kis_lang['Admission']['msg']['selectatleastonetimeslot']?>',
        confirm_delete_email_template_attachment: '<?=$kis_lang['msg']['confirm_delete_email_template_attachment']?>','returnmsg': '<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>'
    });
});
</script>
<div class="main_content">
	<form id='online_payment_form' name="online_payment_form">
		<div class="main_content_detail">
    <?php
    kis_ui::loadModuleTab(array(
        'applicationsettings',
        'instructiontoapplicants',
        'attachmentsettings',
        'email_template',
		'printformheader',
		'onlinepayment',
		'modulesettings'
    ), 'onlinepayment', '#/apps/admission/settings/');
    ?>
        <p class="spacer"></p>
        <?=$kis_data['NavigationBar']?>
        <p class="spacer"></p>
			<div class="table_board">
				<table id="FormTable" class="form_table">
		        	<tbody>
		        		<tr> 
	       <td width="30%" class="field_title"><?=$kis_lang['inputamount']?></td>
	       <td width="40%">$ <input type="text" id="onlinepaymentamount" name="onlinepaymentamount" value="<?=$basicSettings['onlinepaymentamount']?>" /><br/>(<?=$kis_lang['paypalamountremarks']?>)</td>
	     </tr>
	     <tr> 
	       <td width="30%" class="field_title"><?=$kis_lang['paybypaypal']?> (<a href="/kis/apps/admission/templates/settings/PayPal_setting_guideline.pdf" target="_blank"><?=$kis_lang['presstoviewsettingguidelines']?></a>)</td>
	       <td width="40%">
	       <input type="checkbox" id="enablepaypal" name="enablepaypal" value="1" <?=($basicSettings['enablepaypal']==1?'checked=1':'')?>">
	       </td>
	     </tr>
	     <tr class='paypal_tr' <?=$basicSettings['enablepaypal']==1?'':'style="display:none;"'?>> 
	       <td width="30%" class="field_title">- PayPal Signature</td>
	       <td width="40%"><input type="text" id="paypalsignature" name="paypalsignature" value="<?=$basicSettings['paypalsignature']?>" style="width:500px" /></td>
	     </tr>
	     <tr class='paypal_tr' <?=$basicSettings['enablepaypal']==1?'':'style="display:none;"'?>> 
	       <td width="30%" class="field_title">- PayPal Hosted Button ID</td>
	       <td width="40%"><input type="text" id="paypalhostedbuttonid" name="paypalhostedbuttonid" value="<?=$basicSettings['paypalhostedbuttonid']?>" style="width:500px" /></td>
	     </tr>
	     <tr class='paypal_tr' <?=$basicSettings['enablepaypal']==1?'':'style="display:none;"'?>> 
	       <td width="30%" class="field_title">- PayPal Company Name</td>
	       <td width="40%"><input type="text" id="paypalname" name="paypalname" value="<?=$basicSettings['paypalname']?>" style="width:500px" /></td>
	     </tr>
	     <?if($sys_custom['ePayment']['Alipay']){?>
	     <tr> 
	       <td width="30%" class="field_title"><?=$kis_lang['paybyalipayhk']?></td>
	       <td width="40%"><input type="checkbox" id="enablealipayhk" name="enablealipayhk" value="1" <?=($basicSettings['enablealipayhk']==1?'checked=1':'')?>"></td>
	     </tr>
	     <?}?>
		            </tbody>
		        </table>
				<p class="spacer"></p>
				<p class="spacer"></p>
				<br />
			</div>
			<div class="text_remark"><?=$kis_lang['requiredfield']?></div>
			<div class="edit_bottom">
				<input type="hidden" name="schoolYearId" id="schoolYearId"
					value="<?=$schoolYearID?>"> <input type="submit" id="submitBtn"
					name="submitBtn" class="formbutton"
					value="<?=$kis_lang['submit']?>" /> <input type="button"
					id="cancelBtn" name="cancelBtn" class="formsubbutton"
					value="<?=$kis_lang['cancel']?>" />
			</div>
		</div>
	</form>
</div>

<script>
$(function(){
	$('.multiLang .main_menu a').click(function(e){
		e.preventDefault();

		var $td = $(this).closest('td');
		var newLang = $(this).attr('href');
		newLang = newLang.substring(0, newLang.length - 1); // Remove last '/'

		$td.find('.main_menu li').removeClass('selected');
		$(this).closest('li').addClass('selected');

		$td.find('.multiLangContainer').hide();
		$td.find('.multiLangContainer.' + newLang).show();
	});
	$('.multiLang .main_menu').find('a:first').click();
});
</script>