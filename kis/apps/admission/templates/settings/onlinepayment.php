<?php
// Editing by Henry
/****************************************
 *	2019-12-03 (Henry):	created this page
 ****************************************/
?>
<script type="text/javascript">
$(function(){
	kis.admission.online_payment_init({
		'close':'<?=$kis_lang['close']?>',
		'delete':'<?=$kis_lang['delete']?>',
		'confirm_delete_email_template':'<?=$kis_lang['msg']['deleteinterviewarrangement']?>',
		'returnmsg':'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>'
	});
});
</script>
<div class="main_content_detail">
	<? 
    if($sys_custom['KIS_Admission']['ICMS']['Settings']){
    	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template','printformheader','onlinepayment', 'modulesettings'), 'onlinepayment', '#/apps/admission/settings/');
    }
//    else if($sys_custom['KIS_Admission']['TBCPK']['Settings']){
//    	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants'), 'instructiontoapplicants', '#/apps/admission/settings/');
//    }
//    else if($sys_custom['KIS_Admission']['InterviewArrangement']){
//	    kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template', 'interviewarrangement'), 'instructiontoapplicants', '#/apps/admission/settings/');
//	}
    else{
    	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template','printformheader','onlinepayment', 'modulesettings'),'onlinepayment', '#/apps/admission/settings/');
    }
    ?><p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	<div class="common_table_tool">
    	<a class="tool_edit" href="#/apps/admission/settings/onlinepayment/edit/"><?=$kis_lang['edit']?></a>
  	</div>
	<table class="form_table">
	     <tbody>
	     <tr> 
	       <td width="30%" class="field_title"><?=$kis_lang['inputamount']?></td>
	       <td width="40%"><?=$basicSettings['onlinepaymentamount']>0?'$'.$basicSettings['onlinepaymentamount']:'--'?></td>
	     </tr>
	     <tr> 
	       <td width="30%" class="field_title"><?=$kis_lang['paybypaypal']?> (<a href="/kis/apps/admission/templates/settings/PayPal_setting_guideline.pdf" target="_blank"><?=$kis_lang['presstoviewsettingguidelines']?></a>)</td>
	       <td width="40%"><?=$basicSettings['enablepaypal']>0?$kis_lang['enable'].$kis_data['payPalButton']:$kis_lang['disable']?></td>
	     </tr>
	     <?if($basicSettings['enablepaypal']>0){?>
	     <tr> 
	       <td width="30%" class="field_title">- PayPal Signature</td>
	       <td width="40%"><?=$basicSettings['paypalsignature']?></td>
	     </tr>
	     <tr> 
	       <td width="30%" class="field_title">- PayPal Hosted Button ID</td>
	       <td width="40%"><?=$basicSettings['paypalhostedbuttonid']?></td>
	     </tr>
	     <tr> 
	       <td width="30%" class="field_title">- PayPal Company Name</td>
	       <td width="40%"><?=$basicSettings['paypalname']?></td>
	     </tr>
	     <?}?>
	     <?if($sys_custom['ePayment']['Alipay']){?>
	     <tr> 
	       <td width="30%" class="field_title"><?=$kis_lang['paybyalipayhk']?></td>
	       <td width="40%"><?=$basicSettings['enablealipayhk']>0?$kis_lang['enable']:$kis_lang['disable']?></td>
	     </tr>
	     <?}?>
	   </tbody></table>
</div>
