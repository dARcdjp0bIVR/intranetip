<?
// Editing by 
/**
 * 2018-01-30 (Pun): modified updateApplicationInfo, added updateApplicationInfo()
 * 2017-10-09 (Pun): modified checkImportInterviewSettingsInfo,
 * added checkImportInterviewApplicant, importInterviewApplicant
 * 2017-09-13 (Pun): modified updateApplicationInfo
 * 2017-08-31 (Pun): Added getpersonalphoto, updatecroppedphoto
 * Modified updatephoto
 * Added support crop personal photo
 * 2016-02-03 (Henry): modified "updateApplicationPeriod" to support termType for tbcpk
 * 2015-11-04 (Pun): modified "updateApplicationInfo"
 * 2015-10-12 (Henry): added action "updateInterviewUserSettingsByIds"
 * 2015-09-25 (Henry): modified action "updateApplicationPeriod"
 * 2015-09-23 (Henry): added action "resendNotificationEmailByIds"
 * 2015-08-13 (Henry): modified "getReplacedMailText"
 * 2015-08-04 (Henry): added action "removeApplicationInterviewRecord"
 * 2015-07-31 (Pun): added action "removeBriefingEditByIds"
 * 2015-07-31 (Henry): added action "importInterviewInfoByArrangement"
 * 2015-07-20 (Henry): added deleteInterviewArrangementRecords(), reorderInterviewArrangementRecords()
 * 2015-07-16 (Omas): added cust $sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']
 * 2015-01-20 (Henry): added acton "updateApplicationClassLevelByIds()"
 * 2014-11-04 (Carlos): added action "prepareSendMails", "sendEmailToReceivers", "getSendMailResultTable"
 * 2014-08-20 (Henry): added checkImportAdmissionInfo() and importAdmissionInfo()
 * 2014-08-11 (Carlos): modified [sendMails] added attachment parameters.
 * added [viewEmailAttachment], [reorderEmailTemplateRecords], [uploadEmailAttachments], [resendUploadEmailAttachments] and [getEmailTemplateJsonRecord]
 * 2014-08-07 (Henry): modified updateApplicationPeriod
 * 2014-02-04 (Henry): Added actions for ICMS cust
 * 2014-01-15 (Carlos): Added actions for Attachment Settings
 */
$PATH_WRT_ROOT = "../../../";
include_once ($PATH_WRT_ROOT . 'kis/init.php');
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/config.php");
include_once ($PATH_WRT_ROOT . "includes/admission/libadmission_briefing_base.php");
$libkis_admission = $libkis->loadApp('admission');
if(class_exists('admission_briefing')){
	$abb = new admission_briefing();
}else{
	$abb = new admission_briefing_base();
}

if ($kis_user['type'] == kis::$user_types['teacher']) {
    switch ($action) {
        case 'updateApplicationInfo':
            // ####### All Info START ########
            if (method_exists($libkis_admission, 'updateApplicationInfo')) {
                $result = $libkis_admission->updateApplicationInfo($display, $_POST, $_POST['ApplicationID'], $_POST['schoolYearId']);
                // ####### All Info END ########
            } elseif ($display == 'icmsattachment') {
                $result = true;

                // ####### Student Info START ########
            } elseif ($display == 'studentinfo' || $display == 'icmssectionb') {
                $result = $libkis_admission->updateApplicationStudentInfo($_POST);
                if ($sys_custom['KIS_Admission']['CSM']['Settings'])
                    $result = $libkis_admission->updateApplicationOtherInfo($_POST);
                    if ($sys_custom['KIS_Admission']['UCCKE']['Settings'] || $sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings'])
                    $result = $libkis_admission->updateApplicationStudentInfoCust($_POST);
                // ####### Student Info END ########

                // ####### Parent Info START ########
            } elseif ($display == 'parentinfo' || $display == 'icmssectionc') {
                if ($sys_custom['KIS_Admission']['SSGC']['Settings'] || $sys_custom['KIS_Admission']['AOGKG']['Settings']) {
                    $result = $libkis_admission->saveApplicationParentInfo($_POST, true);
                } else {
                    $result = $libkis_admission->saveApplicationParentInfo($_POST);
                }
                if ($sys_custom['KIS_Admission']['CSM']['Settings'])
                    $result = $libkis_admission->updateApplicationStudentInfo($_POST, true);
                // ####### Parent Info END ########

                // ####### Other Info START ########
            } elseif ($display == 'otherinfo' || $display == 'icmssectiona' || $display == 'icmssectiondef') {
                $result = $libkis_admission->updateApplicationOtherInfo($_POST);
                if ($sys_custom['KIS_Admission']['MGF']['Settings']) {
                    $result = $libkis_admission->updateApplicationStudentInfo($_POST, true);
                } elseif ($sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']) {
                    $result = $libkis_admission->updateFamilyInfo($_POST);
                } elseif ($sys_custom['KIS_Admission']['STCC']['Settings']) {
                    $result = $libkis_admission->updateApplicationStudentInfoCust($_POST, true);
                }
                // ####### Other Info END ########

                // ####### Remarks START ########
            } elseif ($display == 'remarks') {
                $result = $libkis_admission->updateApplicationStatus($_POST);
                // if($sys_custom['KIS_Admission']['InterviewSettings']){
                // $result = $libkis_admission->updateApplicationOtherInfo($_POST);
                // }
                // ####### Remarks END ########
            }
            echo $result;
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'updateBasicSettings':
            $SettingNameValueAry = array(
                'generalInstruction' => $generalInstruction
            );
            $result = $libkis_admission->saveBasicSettings($schoolYearId, $SettingNameValueAry);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'getApplicationPeriod':
            $result = $libkis_admission->getApplicationSetting($schoolYearId);
            echo $libjson->encode($result[$classLevelId]);
            break;

        case 'updateApplicationPeriod':
            if ($timeSlotSettings) {
                $dayType = implode(',', $timeSlotSettings);
            }
            if ($termSettings) {
                $termType = implode(',', $termSettings);
            }

            if ($sys_custom['KIS_Admission']['PFK']['Settings']) {
                $custSettings = implode(',', (array) $schoolTypeSettings);
            }

            $data = array(
                'schoolYearID' => $schoolYearId,
                'classLevelID' => $selectClassLevelID,
                'startDate' => $start_date . ' ' . $start_time_hour . ':' . $start_time_min . ':' . $start_time_sec,
                'endDate' => $end_date . ' ' . $end_time_hour . ':' . $end_time_min . ':' . $end_time_sec,
                'previewStartDate' => $preview_start_date . ' ' . $preview_start_time_hour . ':' . $preview_start_time_min . ':' . $preview_start_time_sec,
                'previewEndDate' => $preview_end_date . ' ' . $preview_end_time_hour . ':' . $preview_end_time_min . ':' . $preview_end_time_sec,
                'updateStartDate' => $update_start_date . ' ' . $update_start_time_hour . ':' . $update_start_time_min . ':' . $update_start_time_sec,
                'updateEndDate' => $update_end_date . ' ' . $update_end_time_hour . ':' . $update_end_time_min . ':' . $update_end_time_sec,
                'updateExtraAttachmentStartDate' => $update_extra_attachment_start_date . ' ' . $update_extra_attachment_start_time_hour . ':' . $update_extra_attachment_start_time_min . ':' . $update_extra_attachment_start_time_sec,
                'updateExtraAttachmentEndDate' => $update_extra_attachment_end_date . ' ' . $update_extra_attachment_end_time_hour . ':' . $update_extra_attachment_end_time_min . ':' . $update_extra_attachment_end_time_sec,
                'dOBStart' => $bday_start_date,
                'dOBEnd' => $bday_end_date,
                'dayType' => $dayType,
                'termType' => $termType,
                'firstPageContent' => $firstPageContent,
                'firstPageContent1' => $firstPageContent1,
                'firstPageContent2' => $firstPageContent2,
                'firstPageContent3' => $firstPageContent3,
                'lastPageContent' => $lastPageContent,
                'lastPageContent1' => $lastPageContent1,
                'lastPageContent2' => $lastPageContent2,
                'lastPageContent3' => $lastPageContent3,
                'emailContent' => $emailContent,
                'emailContent1' => $emailContent1,
                'emailContent2' => $emailContent2,
                'emailContent3' => $emailContent3,
                'quota' => $quota,
                'allowInternalUse' => $allowInternalUse,
                'custSettings' => $custSettings
            );

            $result = $libkis_admission->updateApplicationSetting($data);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'updateInterviewEdit':
            $startTime = $start_time_hour . ':' . $start_time_min . ':' . $start_time_sec;
            $endTime = $end_time_hour . ':' . $end_time_min . ':' . $end_time_sec;
            $result = $libkis_admission->updateInterviewSettingAry($recordID, $interview_date, $startTime, $endTime, $quota, $schoolYearID, $selectClassLevelID, $group_name, $round);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'removeInterviewEditByIds':
            // $recordIds = (array)$recordIds;
            $result = $libkis_admission->removeInterviewSettingByIds($recordIds);
            if ($result) {
                echo 'DeleteSuccess';
            } else {
                echo 'DeleteUnsuccess';
            }
            break;

        case 'updateApplicationStatusByIds':
                $result = $libkis_admission->updateApplicationStatusByIds($applicationIds, $status);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'updateApplicationStatusByApplicationNos':
            $result = $libkis_admission->updateApplicationStatusByApplicationNos($applicationIds, $status);
            if ($result) {
                if (mysql_affected_rows() > 0)
                    echo 'UpdateSuccess';
                else
                    echo 'WrongApplicationNo';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'searchusers':

            $user = $libkis_admission->getSelectHandler(array(
                'user_type' => $user_type,
                'user_group' => $user_group,
                'keyword' => trim($keyword)
            ), $exclude_list);
            if (sizeof($user) > 500) {
                $kis_data['ui'] = $kis_lang['toomanyresults'] . '!';
            } else
                if (! $user) {
                    $kis_data['ui'] = $kis_lang['norecord'] . '!';
                } else {
                    ob_start();

                    kis_ui::loadTemplate('applicantslist/select_handler', array(
                        'users' => $user,
                        'form_name' => 'handler'
                    ));

                    $kis_data['ui'] = ob_get_clean();

                    ob_end_clean;
                }

            $kis_data['count'] = sizeof($user);
            echo $libjson->encode($kis_data);

            break;

        case 'getpersonalphoto':
            $attachments = $libkis_admission->getAttachmentsByOthersInfoRecordID($id);
            $currentPersonalPhoto = '';
            foreach ($attachments as $attachment) {
                if ($attachment['AttachmentType'] == 'personal_photo') {
                    $currentPersonalPhoto = $attachment['AttachmentName'];
                }
            }
            $image_url = $libkis_admission->filepath . $id . '/personal_photo';
            $image_path = $file_path . $image_url;
            $kis_data['personal_photo'] = $image_url . '/' . $currentPersonalPhoto . '?_=' . time();
            echo $libjson->encode($kis_data);
            break;

        case 'updatecroppedphoto':
            list ($type, $data) = explode(';', $data);
            list (, $data) = explode(',', $data);
            $data = base64_decode($data);

            $tmpfname = tempnam("/tmp", "student_photo_");
            $handle = fopen($tmpfname, "w");
            fwrite($handle, $data);
            fclose($handle);

            $_FILES['file']['tmp_name'] = $tmpfname;
            $_FILES['file']['name'] = "student_photo_{$id}";
        // NO BREAK STATMENT
        case 'updatephoto':
            if ($img_path = $_FILES['file']['tmp_name']) {
                list ($width, $height, $img_type) = getimagesize($img_path);

                // Maximum size is 1MB for now
                if (filesize($img_path) < 10485760) {
                    switch ($img_type) {
                        case IMAGETYPE_GIF:
                            $image = imagecreatefromgif($img_path);
                            break;
                        case IMAGETYPE_JPEG:
                            $image = @imagecreatefromjpeg($img_path);
                            if (!$image){
								$image = imagecreatefromstring(file_get_contents($img_path));
							}
                            break;
                        case IMAGETYPE_PNG:
                            $image = imagecreatefrompng($img_path);
                            break;
                    }
                }
            }

            if ($image) {
            	$exif = @exif_read_data($img_path);

			    //orientation handling [Start]
			    if (!empty($exif['Orientation'])) {
			    	ini_set('memory_limit','200M');
			        switch ($exif['Orientation']) {
			            case 3:
			                $image = imagerotate($image, 180, 0);
			                break;

			            case 6:
			                $image = imagerotate($image, -90, 0);
			                $temp_width = $width;
			                $width = $height;
			                $height = $temp_width;
			                break;

			            case 8:
			                $image = imagerotate($image, 90, 0);
			                $temp_width = $width;
			                $width = $height;
			                $height = $temp_width;
			                break;
			        }
			    }
			    //orientation handling [End]

                $filename = kis_utility::getSaveFileName($_FILES['file']['name']);
                $filename = $libkis_admission->encrypt_attachment($filename);
                $image_url = $libkis_admission->filepath . $id . '/personal_photo';
                $image_path = $file_path . $image_url;
                $sr = $width / $height;
                $rw = kis::$personal_photo_width;
                $rh = kis::$personal_photo_height;

                if ($sr > 1 / 1.3) {
                    $rw = floor($rh * $sr);
                } else {
                    $rh = floor($rw / $sr);
                }

                $rimage = imagecreatetruecolor(kis::$personal_photo_width, kis::$personal_photo_height);
                imagecopyresampled($rimage, $image, floor(($rw - kis::$personal_photo_width) / - 2), floor(($rh - kis::$personal_photo_height) / - 2), 0, 0, $rw, $rh, $width, $height);

                if (! file_exists($image_path)) {
                    mkdir($image_path, 0755, true);
                }

                if ($sys_custom['KIS_Admission']['SaveOriginalPersonalPhoto']) {
                    $attachments = $libkis_admission->getAttachmentsByOthersInfoRecordID($id);
                    $currentPersonalPhoto = '';
                    foreach ($attachments as $attachment) {
                        if ($attachment['AttachmentType'] == 'personal_photo') {
                            $currentPersonalPhoto = $attachment['AttachmentName'];
                        }
                    }
                    if ($currentPersonalPhoto) {
                        if ($action == 'updatecroppedphoto') {
                            $originalFilePath = $image_path . '/original_' . $currentPersonalPhoto;
                        } else {
                            $originalFilePath = $_FILES['file']['tmp_name'];
                        }
                        @chmod($originalFilePath, 0755);
                        @rename($originalFilePath, $image_path . '/original_' . $filename);
                        @unlink($image_path . '/original_' . $currentPersonalPhoto);
                    }
                }

                $image_path .= '/' . $filename;
                imagejpeg($rimage, $image_path, 100);

                $data = array(
                    "recordID" => $id,
                    "attachment_name" => $filename,
                    "attachment_type" => "personal_photo"
                );
                $libkis_admission->saveApplicationAttachment($data);
                $kis_data['personal_photo'] = $image_url . '/' . $filename . '?_=' . time();
            } else {
                if (filesize($img_path) < 10485760 || $img_type != IMAGETYPE_GIF && $img_type != IMAGETYPE_JPEG && $img_type != IMAGETYPE_PNG)
                    $kis_data['error'] = $kis_lang['Admission']['msg']['personalPhotoFormat'] . ($admission_cfg['maxUploadSize'] ? $admission_cfg['maxUploadSize'] : 1) . ' MB';
                else
                    $kis_data['error'] = $kis_lang['exceedlimit'] . ' (' . ($admission_cfg['maxUploadSize'] ? $admission_cfg['maxUploadSize'] : 1) . ' MB)';
            }
            echo $libjson->encode($kis_data);
            if ($action == 'updatecroppedphoto') {
                @unlink($_FILES['file']['tmp_name']);
            }
            break;

        case 'removeAttachment':
            $data = array(
                "recordID" => $id,
                "attachment_type" => $type
            );
            $libkis_admission->removeApplicationAttachment($data);
            break;

        case 'saveCroppedAttachment':
            $libkis_admission->removeApplicationAttachment(array(
                "recordID" => $id,
                "attachment_type" => $type
            ));

            list ($_type, $data) = explode(';', $data);
            list (, $data) = explode(',', $data);
            $data = base64_decode($data);

            $tmpfname = tempnam("/tmp", "admission_file_");
            $handle = fopen($tmpfname, "w");
            fwrite($handle, $data);
            fclose($handle);

            $_FILES['file']['tmp_name'] = $tmpfname;
            $_FILES['file']['name'] = "admission_file_{$id}.jpg";
        // NO BREAK STATMENT
        case 'saveAttachment':
            $path = $file_path . $libkis_admission->filepath . $id . '/other_files';
            $filename = $original_attachment_name = kis_utility::getSaveFileName($_FILES['file']['name']);
            $filename = $libkis_admission->encrypt_attachment($filename);

            $data = array(
                "recordID" => $id,
                "attachment_name" => $filename,
                "attachment_type" => $type,
                "original_attachment_name" => $original_attachment_name
            );
            $libkis_admission->saveApplicationAttachment($data);
            if (! file_exists($path))
                mkdir($path, 0755, true);

            if ($action == 'saveCroppedAttachment') {
                copy($_FILES['file']['tmp_name'], $path . '/' . $filename);
            } else {
                move_uploaded_file($_FILES['file']['tmp_name'], $path . '/' . $filename);
            }
            break;

        case 'getAttachmentFile':
            $data = array(
                "recordID" => $id,
                "attachment_type" => $type
            );
            $attachmentAry = $libkis_admission->getApplicationAttachmentRecord($schoolYearId, $data);
            foreach ($attachmentAry as $_applicationId => $_attachmentAry) {
                $file_name = $_attachmentAry[$type]['attachment_name'][0];
                $attachment_path = $file_path . $libkis_admission->filepath . $_attachmentAry[$type]['attachment_link'][0];
                if (file_exists($attachment_path)) {
                    if ($viewImage) {
                        header("Content-Type: image/jpeg");
                        header("Content-Description: File Transfer");

                        readfile($attachment_path);
                    } else {
                        kis_utility::downloadFile($attachment_path, $file_name);
                    }
                }
            }
            break;

        case 'updateAttachmentSetting':
            $Attachments = array(
                trim($AttachmentName),
                trim($AttachmentName1),
                trim($AttachmentName2),
                trim($AttachmentName3)
            );
            $result = $libkis_admission->updateAttachmentSetting($RecordID, $Attachments, $AttachmentIsOptional, (array) $AttachmentYearClassID, $AttachmentIsExtra);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'checkAttachmentName':
            $AttachmentName = trim(rawurldecode($AttachmentName));
            $AttachmentName1 = trim(rawurldecode($AttachmentName1));
            $AttachmentName2 = trim(rawurldecode($AttachmentName2));
            $AttachmentName3 = trim(rawurldecode($AttachmentName3));
            $ExcludeIdAry = array();
            if (! empty($ExcludeRecordID) && $ExcludeRecordID > 0) {
                $ExcludeIdAry[] = $ExcludeRecordID;
            }
            $records = $libkis_admission->getAttachmentSettings(array(), array(
                $AttachmentName,
                $AttachmentName1,
                $AttachmentName2,
                $AttachmentName3
            ), $ExcludeIdAry);
            echo sizeof($records);
            break;

        case 'removeAttachmentSetting':
            $RecordID = (array) $RecordID;
            $result = $libkis_admission->removeAttachmentSetting($RecordID);
            if ($result) {
                echo 'DeleteSuccess';
            } else {
                echo 'DeleteUnsuccess';
            }
            break;

        case 'reorderAttachmentSettings':
            $recordIdAry = (array) $RecordID;
            $result = $libkis_admission->reorderAttachmentSettings($recordIdAry);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'removeEmailListByIds':
            // $recordIds = (array)$recordIds;
            $result = $libkis_admission->removeEmailListByIds($recordIds);
            if ($result) {
                echo 'DeleteSuccess';
            } else {
                echo 'DeleteUnsuccess';
            }
            break;

        case 'getReplacedMailText':
            include_once ($intranet_root . "/lang/kis/apps/lang_admission_" . $intranet_session_language . ".php");
            $text = stripslashes(rawurldecode($text));
            $receiverAry = $libkis_admission->getEmailReceivers($applicationIds, $recordID);
            $replaced_text = $libkis_admission->replaceEmailVariables($text, $receiverAry[0]['ChineseName'], $receiverAry[0]['EnglishName'], $receiverAry[0]['ApplicationNo'], $receiverAry[0]['Status'], $receiverAry[0]['InterviewDate'], $receiverAry[0]['InterviewLocation'], $receiverAry[0]['InterviewSettingID'], $receiverAry[0]['InterviewSettingID2'], $receiverAry[0]['InterviewSettingID3'], $receiverAry[0]['BriefingDate'], $receiverAry[0]['BriefingInfo'], $receiverAry[0]['LangSpokenAtHome'], $libkis_admission->getPrintLink($recordID, $receiverAry[0]['UserID']));
            echo $replaced_text;
            break;

        case 'checkImportInterviewInfo':
            include_once ($intranet_root . "/includes/libfilesystem.php");
            include_once ($intranet_root . "/includes/libftp.php");
            include_once ($intranet_root . "/includes/libimport.php");
            include_once ($intranet_root . "/includes/libimporttext.php");

            $limport = new libimporttext();
            $lo = new libfilesystem();
            $li = new libimport();

            // ## CSV Checking
            $name = $_FILES['userfile']['name'];
            $ext = strtoupper($lo->file_ext($name));

            if (! ($ext == ".CSV" || $ext == ".TXT")) {
                intranet_closedb();
                header("location: import.php?xmsg=import_failed&TabID=$TabID");
                exit();
            }
            $data = $limport->GET_IMPORT_TXT($userfile);

            $csv_header = array_shift($data);

            if ($sys_custom['KIS_Admission']['ICMS']['Settings'])
                $file_format = array(
                    "Application#",
                    "StudentEnglishName",
                    "InterviewDate",
                    "InterviewTime",
                    "InterviewLocation"
                );
            else
                if ($sys_custom['KIS_Admission']['MUNSANG']['Settings'] || $sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['UCCKE']['Settings'])
                    $file_format = array(
                        "Application#",
                        "StudentEnglishName",
                        "StudrntChineseName",
                        ($sys_custom['KIS_Admission']['UCCKE']['Settings']?"HKIDNo":"BirthCertNo"),
                        "InterviewDate",
                        "InterviewTime",
                        "InterviewLocation"
                    );
                else
                    $file_format = array(
                        "Application#",
                        "StudentEnglishName",
                        "StudrntChineseName",
                        "BirthCertNo",
                        "InterviewDate",
                        "InterviewTime"
                    );

                // check csv header
            $format_wrong = false;

            for ($i = 0; $i < sizeof($file_format); $i ++) {
                if ($csv_header[$i] != $file_format[$i]) {

                    $format_wrong = true;
                    break;
                }
            }

            if ($format_wrong) {
                echo "<font color=red>" . $kis_lang['invalidHeader'] . "!</font>";
                exit();
            }
            if (empty($data)) {
                echo "<font color=red>" . $kis_lang['CSVFileNoData'] . "!</font>";
                exit();
            }
            $result = $libkis_admission->checkImportDataForImportInterview($data);
            // $result = implode(',',$result);
            $errCount = 0;

            $x .= '<table class="common_table_list"><tbody><tr class="step2">
					<th class="tablebluetop tabletopnolink">' . $kis_lang['Row'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['applicationno'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['importRemarks'] . '</th>
				</tr>';
            $i = 2;
            foreach ($result as $aResult) {
                if ($aResult['validData'] != false) {
                    $errCount ++;
                    $x .= '<tr class="step2">
					<td>' . $i . '</td>
					<td>' . $aResult['validData'] . '</td>
					<td><font color="red">';
                    if (! $aResult['validDate']) {
                        $x .= $kis_lang['invalidinterviewdateformat'];
                    } else
                        if (! $aResult['validTime']) {
                            $x .= $kis_lang['invalidinterviewtimeformat'];
                        } else
                            $x .= ($sys_custom['KIS_Admission']['UCCKE']['Settings']?$kis_lang['invalidapplicationHKIDno']:$kis_lang['invalidapplicationbirthno']);
                    $x .= '</font></td>
				</tr>';
                }
                $i ++;
            }
            $x .= '</tbody></table>';
            echo htmlspecialchars((count($data) - $errCount) . "," . $errCount . "," . $x);
            break;

        case 'importInterviewInfo':

            include_once ($intranet_root . "/includes/libfilesystem.php");
            include_once ($intranet_root . "/includes/libftp.php");
            include_once ($intranet_root . "/includes/libimport.php");
            include_once ($intranet_root . "/includes/libimporttext.php");

            $limport = new libimporttext();
            $lo = new libfilesystem();
            $li = new libimport();

            // ## CSV Checking
            $name = $_FILES['userfile']['name'];
            $ext = strtoupper($lo->file_ext($name));

            if (! ($ext == ".CSV" || $ext == ".TXT")) {
                intranet_closedb();
                header("location: import.php?xmsg=import_failed&TabID=$TabID");
                exit();
            }
            $data = $limport->GET_IMPORT_TXT($userfile);

            $result = $libkis_admission->importDataForImportInterview($data);
            // include_once($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
            // $text = stripslashes(rawurldecode($text));
            // $receiverAry = $libkis_admission->getEmailReceivers($applicationIds,$recordID);
            // $replaced_text = $libkis_admission->replaceEmailVariables($text, $receiverAry[0]['ChineseName'], $receiverAry[0]['EnglishName'], $receiverAry[0]['ApplicationNo'], $receiverAry[0]['Status'], $receiverAry[0]['InterviewDate']);

            if (! in_array(false, $result))
                echo $kis_lang['msg']['InterviewImportedSuccessfully'];
            else
                echo "Error!";
            break;

        case 'checkImportBriefingInfo':
            include_once ($intranet_root . "/includes/libfilesystem.php");
            include_once ($intranet_root . "/includes/libftp.php");
            include_once ($intranet_root . "/includes/libimport.php");
            include_once ($intranet_root . "/includes/libimporttext.php");

            $limport = new libimporttext();
            $lo = new libfilesystem();
            $li = new libimport();

            // ## CSV Checking
            $name = $_FILES['userfile']['name'];
            $ext = strtoupper($lo->file_ext($name));

            if (! ($ext == ".CSV" || $ext == ".TXT")) {
                intranet_closedb();
                header("location: import.php?xmsg=import_failed&TabID=$TabID");
                exit();
            }
            $data = $limport->GET_IMPORT_TXT($userfile);

            $csv_header = array_shift($data);

            $file_format = array(
                "Application#",
                "StudentEnglishName",
                "StudrntChineseName",
                "BirthCertNo",
                "BriefingDate",
                "BriefingTime",
                "BriefingInfo"
            );

            // check csv header
            $format_wrong = false;

            for ($i = 0; $i < sizeof($file_format); $i ++) {
                if ($csv_header[$i] != $file_format[$i]) {

                    $format_wrong = true;

                    break;
                }
            }

            if ($format_wrong) {
                echo "<font color=red>" . $kis_lang['invalidHeader'] . "!</font>";
                exit();
            }
            if (empty($data)) {
                echo "<font color=red>" . $kis_lang['CSVFileNoData'] . "!</font>";
                exit();
            }
            $result = $libkis_admission->checkImportDataForImportInterview($data);
            // $result = implode(',',$result);
            $errCount = 0;

            $x .= '<table class="common_table_list"><tbody><tr class="step2">
					<th class="tablebluetop tabletopnolink">' . $kis_lang['Row'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['applicationno'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['importRemarks'] . '</th>
				</tr>';
            $i = 2;
            foreach ($result as $aResult) {
                if ($aResult['validData'] != false) {
                    $errCount ++;
                    $x .= '<tr class="step2">
					<td>' . $i . '</td>
					<td>' . $aResult['validData'] . '</td>
					<td><font color="red">';
                    if (! $aResult['validDate']) {
                        $x .= $kis_lang['invalidinterviewdateformat'];
                    } else
                        if (! $aResult['validTime']) {
                            $x .= $kis_lang['invalidinterviewtimeformat'];
                        } else
                            $x .= $kis_lang['invalidapplicationbirthno'];
                    $x .= '</font></td>
				</tr>';
                }
                $i ++;
            }
            $x .= '</tbody></table>';
            echo htmlspecialchars((count($data) - $errCount) . "," . $errCount . "," . $x);
            break;

        case 'importBriefingInfo':

            include_once ($intranet_root . "/includes/libfilesystem.php");
            include_once ($intranet_root . "/includes/libftp.php");
            include_once ($intranet_root . "/includes/libimport.php");
            include_once ($intranet_root . "/includes/libimporttext.php");

            $limport = new libimporttext();
            $lo = new libfilesystem();
            $li = new libimport();

            // ## CSV Checking
            $name = $_FILES['userfile']['name'];
            $ext = strtoupper($lo->file_ext($name));

            if (! ($ext == ".CSV" || $ext == ".TXT")) {
                intranet_closedb();
                header("location: import.php?xmsg=import_failed&TabID=$TabID");
                exit();
            }
            $data = $limport->GET_IMPORT_TXT($userfile);

            $result = $libkis_admission->importDataForImportBriefing($data);
            // include_once($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
            // $text = stripslashes(rawurldecode($text));
            // $receiverAry = $libkis_admission->getEmailReceivers($applicationIds,$recordID);
            // $replaced_text = $libkis_admission->replaceEmailVariables($text, $receiverAry[0]['ChineseName'], $receiverAry[0]['EnglishName'], $receiverAry[0]['ApplicationNo'], $receiverAry[0]['Status'], $receiverAry[0]['InterviewDate']);

            if (! in_array(false, $result))
                echo $kis_lang['msg']['InterviewImportedSuccessfully'];
            else
                echo "Error!";
            break;

        case 'checkImportAdmissionInfo':
            include_once ($intranet_root . "/includes/libfilesystem.php");
            include_once ($intranet_root . "/includes/libftp.php");
            include_once ($intranet_root . "/includes/libimport.php");
            include_once ($intranet_root . "/includes/libimporttext.php");

            $limport = new libimporttext();
            $lo = new libfilesystem();
            $li = new libimport();

            // ## CSV Checking
            $name = $_FILES['userfile']['name'];
            $ext = strtoupper($lo->file_ext($name));

            if (! ($ext == ".CSV" || $ext == ".TXT")) {
                intranet_closedb();
                header("location: import.php?xmsg=import_failed&TabID=$TabID");
                exit();
            }
            $data = $raw_data = $limport->GET_IMPORT_TXT($userfile);

            $csv_header = array_shift($data);
            if(!$sys_custom['KIS_Admission']['UCCKE']['Settings'] && !$sys_custom['KIS_Admission']['HKUGAC']['Settings'] && $type != 'status'){
                $csv_header = array_shift($data);
            }
            // developing...
            if(method_exists($libkis_admission, 'checkImportDataForImportAdmissionHeaderCust')) {
                $format_wrong = $libkis_admission->checkImportDataForImportAdmissionHeaderCust((!$sys_custom['KIS_Admission']['HKUGAC']['Settings'] && $type != 'status'?$csv_header:$raw_data), 'b5', $type);
            }else {
                $format_wrong = $libkis_admission->checkImportDataForImportAdmissionHeader($csv_header, 'b5');
            }

            $file_format = $libkis_admission->getExportHeader();
            $file_format = $file_format[1];
            // debug_pr($csv_header);
            // debug_pr($file_format);
            // $file_format = array("Application#","StudentEnglishName","StudrntChineseName","BirthCertNo","InterviewDate","InterviewTime");
            //
            // # check csv header
            // $format_wrong = false;
            //
            // for($i=0; $i<sizeof($file_format); $i++)
            // {
            // if ($csv_header[$i]!=$file_format[$i])
            // {
            //
            // $format_wrong = true;
            // break;
            // }
            // }

            if ($format_wrong) {
                echo "<font color=red>" . $kis_lang['invalidHeader'] . "!</font>";
                exit();
            }
            if (empty($data)) {
                echo "<font color=red>" . $kis_lang['CSVFileNoData'] . "!</font>";
                exit();
            }
            if(method_exists($libkis_admission, 'checkImportDataForImportAdmissionCust')) {
                echo $libkis_admission->checkImportDataForImportAdmissionCust(!$sys_custom['KIS_Admission']['HKUGAC']['Settings'] && $type != 'status'?$data:$raw_data, $type);
            }else{
                echo $libkis_admission->checkImportDataForImportAdmission($data);
            }
            // $result = implode(',',$result);
            // $errCount = 0;
            //
            // $x .= '<table class="common_table_list"><tbody><tr class="step2">
            // <th class="tablebluetop tabletopnolink">'.$kis_lang['Row'].'</th>
            // <th class="tablebluetop tabletopnolink">'.$kis_lang['applicationno'].'</th>
            // <th class="tablebluetop tabletopnolink">'.$kis_lang['importRemarks'].'</th>
            // </tr>';
            // $i = 1;
            // foreach($result as $aResult){
            // if($aResult['validData']!=false){
            // $errCount++;
            // $x .= '<tr class="step2">
            // <td>'.$i.'</td>
            // <td>'.$aResult['validData'].'</td>
            // <td><font color="red">';
            // if(!$aResult['validDate']){
            // $x .= $kis_lang['invalidinterviewdateformat'];
            // }
            // else if(!$aResult['validTime']){
            // $x .= $kis_lang['invalidinterviewtimeformat'];
            // }
            // else
            // $x .= $kis_lang['invalidapplicationbirthno'];
            // $x .= '</font></td>
            // </tr>';
            // }
            // $i++;
            // }
            // $x .= '</tbody></table>';
            // echo htmlspecialchars((count($data)-$errCount).",".$errCount.",".$x);
            break;

        case 'importAdmissionInfo':

            include_once ($intranet_root . "/includes/libfilesystem.php");
            include_once ($intranet_root . "/includes/libftp.php");
            include_once ($intranet_root . "/includes/libimport.php");
            include_once ($intranet_root . "/includes/libimporttext.php");

            $limport = new libimporttext();
            $lo = new libfilesystem();
            $li = new libimport();

            // ## CSV Checking
            $name = $_FILES['userfile']['name'];
            $ext = strtoupper($lo->file_ext($name));

            if (! ($ext == ".CSV" || $ext == ".TXT")) {
                intranet_closedb();
                header("location: import.php?xmsg=import_failed&TabID=$TabID");
                exit();
            }
            $data = $limport->GET_IMPORT_TXT($userfile);

            $result = $libkis_admission->importDataForImportAdmission($data, $_REQUEST['type']);
            // include_once($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
            // $text = stripslashes(rawurldecode($text));
            // $receiverAry = $libkis_admission->getEmailReceivers($applicationIds,$recordID);
            // $replaced_text = $libkis_admission->replaceEmailVariables($text, $receiverAry[0]['ChineseName'], $receiverAry[0]['EnglishName'], $receiverAry[0]['ApplicationNo'], $receiverAry[0]['Status'], $receiverAry[0]['InterviewDate']);

            if (! in_array(false, $result))
                echo $kis_lang['msg']['ImportedSuccessfully'];
            else
                echo "Error!";
            break;

        case 'checkImportInterviewSettingsInfo':
            include_once ($intranet_root . "/includes/libfilesystem.php");
            include_once ($intranet_root . "/includes/libftp.php");
            include_once ($intranet_root . "/includes/libimport.php");
            include_once ($intranet_root . "/includes/libimporttext.php");

            $limport = new libimporttext();
            $lo = new libfilesystem();
            $li = new libimport();

            // ## CSV Checking
            $name = $_FILES['userfile']['name'];
            $ext = strtoupper($lo->file_ext($name));

            if (! ($ext == ".CSV" || $ext == ".TXT")) {
                intranet_closedb();
                header("location: import.php?xmsg=import_failed&TabID=$TabID");
                exit();
            }
            $data = $limport->GET_IMPORT_TXT($userfile);

            $csv_header = array_shift($data);

            $file_format = array(
                "Form",
                "InterviewDate",
                "Timeslot(Start)",
                "Timeslot(End)",
                "Quota",
                $admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? "Interview Room" : "Session"
            );

            // check csv header
            $format_wrong = false;

            for ($i = 0; $i < sizeof($file_format); $i ++) {
                if ($csv_header[$i] != $file_format[$i]) {

                    $format_wrong = true;
                    break;
                }
            }

            if ($format_wrong) {
                echo "<font color=red>" . $kis_lang['invalidHeader'] . "!</font>";
                exit();
            }
            if (empty($data)) {
                echo "<font color=red>" . $kis_lang['CSVFileNoData'] . "!</font>";
                exit();
            }
            echo $libkis_admission->checkImportDataForImportInterviewSettings($data);

            // $result = implode(',',$result);
            // $errCount = 0;
            //
            // $x .= '<table class="common_table_list"><tbody><tr class="step2">
            // <th class="tablebluetop tabletopnolink">'.$kis_lang['Row'].'</th>
            // <th class="tablebluetop tabletopnolink">'.$kis_lang['form'].'</th>
            // <th class="tablebluetop tabletopnolink">'.$kis_lang['interviewdate'].'</th>
            // <th class="tablebluetop tabletopnolink">'.$kis_lang['importRemarks'].'</th>
            // </tr>';
            // $i = 1;
            // foreach($result as $aResult){
            // if($aResult['validData']!=false){
            // $errCount++;
            // $x .= '<tr class="step2">
            // <td>'.$i.'</td>
            // <td>'.$aResult['validData'].'</td>
            // <td><font color="red">';
            // if(!$aResult['validDate']){
            // $x .= $kis_lang['invalidinterviewdateformat'];
            // }
            // else if(!$aResult['validTime']){
            // $x .= $kis_lang['invalidinterviewtimeformat'];
            // }
            // else
            // $x .= $kis_lang['invalidapplicationbirthno'];
            // $x .= '</font></td>
            // </tr>';
            // }
            // $i++;
            // }
            // $x .= '</tbody></table>';
            // echo htmlspecialchars((count($data)-$errCount).",".$errCount.",".$x);
            break;

        case 'importInterviewSettingsInfo':

            include_once ($intranet_root . "/includes/libfilesystem.php");
            include_once ($intranet_root . "/includes/libftp.php");
            include_once ($intranet_root . "/includes/libimport.php");
            include_once ($intranet_root . "/includes/libimporttext.php");

            $limport = new libimporttext();
            $lo = new libfilesystem();
            $li = new libimport();

            // ## CSV Checking
            $name = $_FILES['userfile']['name'];
            $ext = strtoupper($lo->file_ext($name));

            if (! ($ext == ".CSV" || $ext == ".TXT")) {
                intranet_closedb();
                header("location: import.php?xmsg=import_failed&TabID=$TabID");
                exit();
            }
            $data = $limport->GET_IMPORT_TXT($userfile);
            $schoolYearID = $_REQUEST['schoolYearID'];

            $result = $libkis_admission->importDataForImportInterviewSettings($schoolYearID, $data, $round);
            // include_once($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
            // $text = stripslashes(rawurldecode($text));
            // $receiverAry = $libkis_admission->getEmailReceivers($applicationIds,$recordID);
            // $replaced_text = $libkis_admission->replaceEmailVariables($text, $receiverAry[0]['ChineseName'], $receiverAry[0]['EnglishName'], $receiverAry[0]['ApplicationNo'], $receiverAry[0]['Status'], $receiverAry[0]['InterviewDate']);

            if (! in_array(false, $result))
                echo $kis_lang['msg']['InterviewImportedSuccessfully'];
            else
                echo "Error!";
            break;

        case 'checkImportInterviewApplicant':
            include_once ($intranet_root . "/includes/libfilesystem.php");
            include_once ($intranet_root . "/includes/libftp.php");
            include_once ($intranet_root . "/includes/libimport.php");
            include_once ($intranet_root . "/includes/libimporttext.php");

            $limport = new libimporttext();
            $lo = new libfilesystem();
            $li = new libimport();

            // ## CSV Checking
            $name = $_FILES['userfile']['name'];
            $ext = strtoupper($lo->file_ext($name));

            if (! ($ext == ".CSV" || $ext == ".TXT")) {
                intranet_closedb();
                header("location: import.php?xmsg=import_failed&TabID=$TabID");
                exit();
            }
            $data = $raw_data = $limport->GET_IMPORT_TXT($userfile);

            if(method_exists($libkis_admission, 'checkImportDataForImportInterviewApplicantHeader')){
                $format_wrong = !($libkis_admission->checkImportDataForImportInterviewApplicantHeader($data, $schoolYearID, $round));
            }else{
                $csv_header = array_shift($data);

                $file_format = array();
                $file_format[] = $kis_lang['form'];
                $file_format[] = $kis_lang['interviewdate'];
                $file_format[] = $kis_lang['timeslot'] . '(' . $kis_lang['from'] . ')';
                $file_format[] = $kis_lang['timeslot'] . '(' . $kis_lang['to'] . ')';
                if ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room') {
                    $file_format[] = $kis_lang['interviewroom'];
                } else {
                    $file_format[] = $kis_lang['sessiongroup'];
                }
                $file_format[] = $kis_lang['qouta'];
                $file_format[] = $kis_lang['applicationno'];
                $file_format[] = $kis_lang['studentname'];
                $file_format[] = $kis_lang['parentorguardian'];
                $file_format[] = $kis_lang['phoneno'];
                $file_format[] = $kis_lang['status'];

                // check csv header
                $format_wrong = false;

                for ($i = 0; $i < sizeof($file_format); $i ++) {
                    if ($csv_header[$i] != $file_format[$i]) {
                        $format_wrong = true;
                        break;
                    }
                }
            }

            if ($format_wrong) {
                echo "<font color=red>" . $kis_lang['invalidHeader'] . "!</font>";
                exit();
            }
            if (empty($data)) {
                echo "<font color=red>" . $kis_lang['CSVFileNoData'] . "!</font>";
                exit();
            }
            echo $libkis_admission->checkImportDataForImportInterviewApplicant($data, $schoolYearID, $round);
            break;

        case 'importInterviewApplicant':

            include_once ($intranet_root . "/includes/libfilesystem.php");
            include_once ($intranet_root . "/includes/libftp.php");
            include_once ($intranet_root . "/includes/libimport.php");
            include_once ($intranet_root . "/includes/libimporttext.php");

            $limport = new libimporttext();
            $lo = new libfilesystem();
            $li = new libimport();

            // ## CSV Checking
            $name = $_FILES['userfile']['name'];
            $ext = strtoupper($lo->file_ext($name));

            if (! ($ext == ".CSV" || $ext == ".TXT")) {
                intranet_closedb();
                header("location: import.php?xmsg=import_failed&TabID=$TabID");
                exit();
            }
            $data = $limport->GET_IMPORT_TXT($userfile);
            $schoolYearID = $_REQUEST['schoolYearID'];

            $result = $libkis_admission->importDataForImportInterviewApplicant($schoolYearID, $data, $round);

            if (! in_array(false, $result))
                echo $kis_lang['msg']['InterviewImportedSuccessfully'];
            else
                echo "Error!";
            break;

        case 'sendMails':

            $Subject = stripslashes($Subject);
            $Message = stripslashes($Message);
            $AttachmentID = (array) $AttachmentID;
            $result = $libkis_admission->sendMails($applicationIds, $Subject, $Message, $IsAcknowledge, $AcknowledgeMessage, $recordID, $schoolYearID, $classLevelID, $SendTarget, $FileUploadTmpFolder, $AttachmentID);
            // echo $result['result'] ? "1":"0";

            $x = $libkis_admission->getSendMailResultTable($result['recordId']);
            echo $x;
            break;

        case 'getEmailReceiversLayer':
		/*
		 * @param number $recordID : ADMISSION_EMAIL_RECORD.RecordID
		 * @param string $applicationIds in csv format : ADMISSION_OTHERS_INFO.RecordID
		 */

		$emailRecordDetail = $libkis_admission->getEmailRecordDetail($recordID);
            $applicationIdAry = $emailRecordDetail['applicationIds'];
            $emailReceivers = $libkis_admission->getEmailReceivers($applicationIdAry, $recordID);

            $x = '<div id="ViewReceiverLayer" style="padding:5px;">
				<div class="table_board">
					<table class="common_table_list edit_table_list">
						<colgroup><col nowrap="nowrap">
						</colgroup>
						<thead>
							<tr>
							  <th width="20">#</th>
							  <th>' . $kis_lang['applicationno'] . '</th>
							  <th>' . $kis_lang['chinesename'] . '</th>
							  <th>' . $kis_lang['englishname'] . '</th>
							  <th>' . $kis_lang['emailaddress'] . '</th>';
            // $x.= '<th>'.$kis_lang['interviewdate'].'</th>';
            $x .= '<th>' . $kis_lang['status'] . '</th>';
            $x .= '<th>' . $kis_lang['sentstatus'] . '</th>';
            $x .= '<th>' . $kis_lang['lastsenton'] . '</th>';
            if ($recordID != '' && $emailRecordDetail['IsAcknowledge'] == 1) {
                $x .= '<th>' . $kis_lang['acknowledgementtime'] . '</th>';
            }
            $x .= '</tr>
						</thead>
						<tbody>';

            for ($i = 0; $i < count($emailReceivers); $i ++) {
                $x .= '<tr>';
                $x .= '<td>' . ($i + 1) . '</td>';
                $x .= '<td>' . kis_ui::displayTableField($emailReceivers[$i]['ApplicationNo']) . '</td>';
                $x .= '<td>' . kis_ui::displayTableField($emailReceivers[$i]['ChineseName']) . '</td>';
                $x .= '<td>' . kis_ui::displayTableField($emailReceivers[$i]['EnglishName']) . '</td>';
                $x .= '<td>' . kis_ui::displayTableField($emailReceivers[$i]['Email']) . '</td>';
                // $x .= '<td>'.kis_ui::displayTableField($emailReceivers[$i]['InterviewDateTime']).'</td>';
                $x .= '<td>' . kis_ui::displayTableField($emailReceivers[$i]['Status']) . '</td>';
                $x .= '<td>' . kis_ui::displayTableField($emailReceivers[$i]['SentStatus'] == '1' ? $kis_lang['success'] : $kis_lang['failed']) . '</td>';
                $x .= '<td>' . kis_ui::displayTableField($emailReceivers[$i]['DateModified']) . '</td>';
                if ($recordID != '' && $emailRecordDetail['IsAcknowledge'] == 1) {
                    $x .= '<td>' . kis_ui::displayTableField($emailReceivers[$i]['AcknowledgeDate']) . '</td>';
                }
                $x .= '</tr>';
            }

            $x .= '</tbody>
				</table>
			  	<p class="spacer"></p>
		    </div>
		    <p class="spacer"></p>
		    <div class="edit_bottom">         
		       <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();" value="' . $kis_lang['close'] . '" />
		  	</div>
		</div>';
            echo $x;
            break;

        case "deleteEmailTemplateRecords":
            $result = $libkis_admission->deleteEmailTemplateRecord($TemplateID);
            if ($result) {
                echo 'DeleteSuccess';
            } else {
                echo 'DeleteUnsuccess';
            }
            break;

        case "viewEmailAttachment":
            if (empty($AttachmentID)) {
                echo "File does not exist.";
                exit();
            }
            include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
            $records = $libkis_admission->getEmailAttachmentRecords('', '', array(
                $AttachmentID
            ));
            if (count($records) > 0) {
                $lfs = new libfilesystem();
                $fileName = $records[0]['FileName'];

                $full_path = $file_path . $records[0]['FilePath'];
                if (file_exists($full_path)) {
                    $content = $lfs->file_read($full_path);
                    output2browser($content, $fileName);
                    exit();
                }
            }
            echo "File does not exist.";
            exit();
            break;

        case "reorderEmailTemplateRecords":
            $result = $libkis_admission->reorderEmailTemplateRecords($TemplateID);
            echo $result ? "UpdateSuccess" : "UpdateUnsuccess";
            break;

        case "uploadEmailAttachments":
            $tmp_folder = $libkis_admission->uploadEmailAttachmentsToTempFolder($_FILES);

            $x = '<script type="text/javascript">' . "\n";
            $x .= 'window.parent.kis.admission.sendmail_compose_send_email("' . $tmp_folder . '");';
            $x .= '</script>' . "\n";
            echo $x;
            break;

        case "resendUploadEmailAttachments":
            $tmp_folder = $libkis_admission->uploadEmailAttachmentsToTempFolder($_FILES);

            $x = '<script type="text/javascript">' . "\n";
            $x .= 'window.parent.kis.admission.sendmail_compose_resend_email("' . $tmp_folder . '");';
            $x .= '</script>' . "\n";
            echo $x;
            break;

        case "getEmailTemplateJsonRecord":
            $attachment_type = $libkis_admission->getEmailAttachmentType('EmailTemplate');
            $record = $libkis_admission->getEmailTemplateRecords('', (array) $TemplateID);
            $attachments = $libkis_admission->getEmailAttachmentRecords($attachment_type, $TemplateID);
            $attachment_count = count($attachments);
            $title = $record[0]['Title'];
            $content = $record[0]['Content'];
            $x = '';
            for ($i = 0; $i < count($attachments); $i ++) {
                $display_size = '&nbsp;(' . $libkis_admission->getAttachmentDisplaySize($attachments[$i]['SizeInBytes']) . ')';
                $x .= '<div>';
                $x .= '<a class="btn_attachment" href="/kis/apps/admission/ajax.php?action=viewEmailAttachment&AttachmentID=' . $attachments[$i]['AttachmentID'] . '" id="AttachmentFile_' . $attachments[$i]['AttachmentID'] . '" name="AttachmentFile[]">' . $attachments[$i]['FileName'] . $display_size . '</a>';
                $x .= '<a class="btn_remove step1" href="javascript:void(0);" id="AttachmentRemoveBtn_' . $attachments[$i]['AttachmentID'] . '" name="AttachmentRemoveBtn[]" alt="' . $kis_lang['remove'] . '"></a>';
                $x .= '<input type="hidden" name="AttachmentID[]" value="' . $attachments[$i]['AttachmentID'] . '" />';
                $x .= '</div>';
            }

            $json = '{';
            $json .= '"Title":"' . rawurlencode($title) . '",';
            $json .= '"Content":"' . rawurlencode($content) . '",';
            $json .= '"AttachmentHtml":"' . rawurlencode($x) . '"';
            $json .= '}';

            echo $json;
            break;

        case "prepareSendMails":
            $Subject = stripslashes($Subject);
            $Message = stripslashes($Message);
            $AttachmentID = (array) $AttachmentID;

            if ($SendTarget == 5) {
                $applicationIds = $applicant_id;
            }
            $result = $libkis_admission->prepareSendMails($applicationIds, $Subject, $Message, $IsAcknowledge, $AcknowledgeMessage, $recordID, $schoolYearID, $classLevelID, $SendTarget, $FileUploadTmpFolder, $AttachmentID);
            // debug_pr($result);
            $json = '{';
            $json .= '"RecordID":"' . $result['recordId'] . '",';
            $json .= '"AttachmentFolder":"' . $result['attachmentFolder'] . '",';
            $json .= '"ReceiverID":"' . implode(",", $result['receiverId']) . '"';
            $json .= '}';

            echo $json;
            break;

        case "sendEmailToReceivers":
            $recordId = $_REQUEST['RecordID'];
            $receiverIdAry = $_REQUEST['ReceiverID'];
            $attachmentFolderPath = $_REQUEST['AttachmentFolder'];
            $sendTarget = $_REQUEST['SendTarget'];
            $deleteTmpAttachmentFolder = $_REQUEST['IsLastBatch'];
            $libkis_admission->sendEmailToReceivers($recordId, $receiverIdAry, $attachmentFolderPath, $sendTarget, $deleteTmpAttachmentFolder);

            if($sys_custom['KIS_Admission']['CREATIVE']['Settings'] && $_REQUEST['changeStatus']){
                $tmpReceiverAry = $libkis_admission->getEmailReceivers('', $recordId, $receiverIdAry);
                $applicationIdAry = Get_Array_By_Key($tmpReceiverAry,'ApplicationNo');

                $updateArr = array();
                foreach($applicationIdAry as $applicationId){
                    $updateArr[] = array(
                        'filterApplicationId' => $applicationId,
                        'filterCode' => $_REQUEST['changeStatus'],
                        'Value' => '1',
                    );
                    $updateArr[] = array(
                        'filterApplicationId' => $applicationId,
                        'filterCode' => "{$_REQUEST['changeStatus']}Date",
                        'Value' => date('Y-m-d'),
                    );
                }
                foreach ($updateArr as $data) {
                    $result[] = $libkis_admission->updateApplicationCustInfo($data);
                }
            }

            if ($_REQUEST['IsLastBatch']) {
                $x = $libkis_admission->getSendMailResultTable($recordId);
                echo $x;
            }
            break;

        case "getSendMailResultTable":
            $x = $libkis_admission->getSendMailResultTable($recordId);
            // if cancel the send mail while in process, clean temp attachment folder if have
            $attachmentFolderPath = $_REQUEST['AttachmentFolder'];
            if ($attachmentFolderPath != '') {
                include_once ($intranet_root . "/includes/libfilesystem.php");
                $lfs = new libfilesystem();
                $base_dir = $libkis_admission->getEmailAttachmentBasePath();
                $attachment_folder_path = $base_dir . '/' . $attachmentFolderPath;
                $lfs->deleteDirectory($attachment_folder_path);
            }
            echo $x;
            break;

        case 'updateApplicationClassLevelByIds':
            $result = $libkis_admission->updateApplicationClassLevelByIds($applicationIds, $classlevel);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'removeBriefingEditByIds':
            $libkis_admission_briefing = new admission_briefing_base();
            $result = $libkis_admission_briefing->deleteBriefingSession($recordIds);
            if ($result) {
                echo 'DeleteSuccess';
            } else {
                echo 'DeleteUnsuccess';
            }
            break;

        case 'updateBriefingEdit':
            $libkis_admission_briefing = new admission_briefing_base();
            $result = $libkis_admission_briefing->updateBriefingSession($BriefingID, $Data);
            echo $result ? "UpdateSuccess" : "UpdateUnsuccess";
            break;

        case "importInterviewSessionBySettings":
            $result = $libkis_admission->importInterviewSessionBySettings($round);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case "deleteInterviewArrangementRecords":
            $result = $libkis_admission->deleteInterviewArrangementRecord($TemplateID);
            if ($result) {
                echo 'DeleteSuccess';
            } else {
                echo 'DeleteUnsuccess';
            }
            break;

        case "reorderInterviewArrangementRecords":
            $result = $libkis_admission->reorderInterviewArrangementRecords($TemplateID);
            echo $result ? "UpdateSuccess" : "UpdateUnsuccess";
            break;

        case "importInterviewInfoByArrangement":
            $result = $libkis_admission->importInterviewInfoByArrangement($selectSchoolYearID, $selectStatus, $round, $classLevelIds);
            echo $result;
            break;

        case 'removeApplicationInterviewRecord':
            // $recordIds = (array)$recordIds;
            $result = $libkis_admission->removeApplicationInterviewRecord($recordIds, $round);
            if ($result) {
                echo 'DeleteSuccess';
            } else {
                echo 'DeleteUnsuccess';
            }
            break;

        case 'assignApplicantToInterviewSetting':
            $result = $libkis_admission->assignApplicantToInterviewSetting($recordIds, $round, $interviewSettingID);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'updateInterviewFormBuilterEditByIds':
            $data = array(
                'recordID' => $recordID,
                'schoolYearID' => $schoolYearID,
                'selectClassLevelID' => $selectClassLevelID,
                'startDate' => $start_date . ' ' . $start_time_hour . ':' . $start_time_min . ':' . $start_time_sec,
                'endDate' => $end_date . ' ' . $end_time_hour . ':' . $end_time_min . ':' . $end_time_sec,
                'title' => $title,
                'text_title' => $text_title,
                'text_input' => $text_input,
                'text_remarks' => $text_remarks,
                'text_required' => $text_required,
                'text_mark' => $text_mark,
                'questions_type' => $questions_type,
                'needUpdateQuestion' => $needUpdateQuestion
            );
            $result = $libkis_admission->updateInterviewFormBuilterEditByIds($data);
            if ($result) {
                // debug_pr($data);
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'removeInterviewFormBuilterEditByIds':
            $result = $libkis_admission->removeInterviewFormBuilterEditByIds($recordIds);
            if ($result) {
                echo 'DeleteSuccess';
            } else {
                echo 'DeleteUnsuccess';
            }
            break;

        case 'updateInterviewFormResultByIds':
            $data = array(
                'interviewSettingID' => $recordID,
                'formID' => $formID,
                'questionID' => $question_id,
                'applicationID' => $applicationID,
                'answer' => $text_answer,
                'answerID' => $answerID
            );
            $result = $libkis_admission->updateInterviewFormResultByIds($data);
            if ($result) {
                // debug_pr($data);
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'resendNotificationEmailByIds':
            // $recordIds = (array)$recordIds;
            include ($PATH_WRT_ROOT . "lang/admission_lang.b5.php");
            include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_ui_cust.php");
            $lac = new admission_cust();
            $lauc = new admission_ui_cust();
            $result = $libkis_admission->resendNotificationEmailByIds($recordIds, $lauc);
            if ($result) {
                echo 'ResendSuccess';
            } else {
                echo 'ResendUnsuccess';
            }
            break;

        case 'updateInterviewUserSettingsByIds':
            $result = $libkis_admission->updateInterviewUserSettings($userIds);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'removeInterviewUserSettingsByIds':
            $result = $libkis_admission->deleteInterviewUserSettings($userIds);
            if ($result) {
                echo 'DeleteSuccess';
            } else {
                echo 'DeleteUnsuccess';
            }
            break;

        case 'searchinterviewusers':
            $user = $libkis_admission->getSelectHandler(array(
                'user_type' => $user_type,
                'user_group' => $user_group,
                'keyword' => trim($keyword)
            ), $exclude_list);
            if (sizeof($user) > 500) {
                $kis_data['ui'] = $kis_lang['toomanyresults'] . '!';
            } else
                if (! $user) {
                    $kis_data['ui'] = $kis_lang['norecord'] . '!';
                } else {
                    ob_start();

                    kis_ui::loadTemplate('applicantslist/select_handler', array(
                        'users' => $user,
                        'form_name' => 'recipients[]'
                    ));

                    $kis_data['ui'] = ob_get_clean();

                    ob_end_clean;
                }

            $kis_data['count'] = sizeof($user);
            echo $libjson->encode($kis_data);
            break;

        case 'updateInterviewAnnouncementSettings':
            $SettingNameValueAry = array();
            if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){
                $SettingNameValueAry['firstInterviewAnnouncementStart'] = $firstInterviewAnnouncementStart . " " . ($firstStartTime_hour > 9? $firstStartTime_hour : "0".$firstStartTime_hour). ":". ($firstStartTime_min > 9? $firstStartTime_min : "0".$firstStartTime_min);
                $SettingNameValueAry['firstInterviewAnnouncementEnd'] = $firstInterviewAnnouncementEnd . " " . ($firstEndTime_hour > 9? $firstEndTime_hour : "0".$firstEndTime_hour). ":". ($firstEndTime_min > 9? $firstEndTime_min : "0".$firstEndTime_min);
                $SettingNameValueAry['secondInterviewAnnouncementStart'] = $secondInterviewAnnouncementStart . " " . ($secondStartTime_hour > 9? $secondStartTime_hour : "0".$secondStartTime_hour). ":". ($secondStartTime_min > 9? $secondStartTime_min : "0".$secondStartTime_min);
                $SettingNameValueAry['secondInterviewAnnouncementEnd'] = $secondInterviewAnnouncementEnd . " " . ($secondEndTime_hour > 9? $secondEndTime_hour : "0".$secondEndTime_hour). ":". ($secondEndTime_min > 9? $secondEndTime_min : "0".$secondEndTime_min);
                $SettingNameValueAry['thirdInterviewAnnouncementStart'] = $thirdInterviewAnnouncementStart . " " . ($thirdStartTime_hour > 9? $thirdStartTime_hour : "0".$thirdStartTime_hour). ":". ($thirdStartTime_min > 9? $thirdStartTime_min : "0".$thirdStartTime_min);
                $SettingNameValueAry['thirdInterviewAnnouncementEnd'] = $thirdInterviewAnnouncementEnd . " " . ($thirdEndTime_hour > 9? $thirdEndTime_hour : "0".$thirdEndTime_hour). ":". ($thirdEndTime_min > 9? $thirdEndTime_min : "0".$thirdEndTime_min);
                $SettingNameValueAry['applicationStatusAnnouncementStart'] = $applicationStatusAnnouncementStart . " " . ($appStatusStartTime_hour > 9? $appStatusStartTime_hour : "0".$appStatusStartTime_hour). ":". ($appStatusStartTime_min > 9? $appStatusStartTime_min : "0".$appStatusStartTime_min);
                $SettingNameValueAry['applicationStatusAnnouncementEnd'] = $applicationStatusAnnouncementEnd . " " . ($appStatusEndTime_hour > 9? $appStatusEndTime_hour : "0".$appStatusEndTime_hour). ":". ($appStatusEndTime_min > 9? $appStatusEndTime_min : "0".$appStatusEndTime_min);
                $SettingNameValueAry['adminModeActive'] = $adminModeActive;
                $SettingNameValueAry['adminMessage'] = $adminMessage;
            }
            $SettingNameValueAry['firstInterviewAnnouncement'] = $firstInterviewAnnouncement;
            $SettingNameValueAry['secondInterviewAnnouncement'] = $secondInterviewAnnouncement;
            $SettingNameValueAry['thirdInterviewAnnouncement'] = $thirdInterviewAnnouncement;
            $SettingNameValueAry['applicationStatusAnnouncement'] = $applicationStatusAnnouncement;
            $SettingNameValueAry['enableInterviewAnnouncement'] = $enableInterviewAnnouncement;
            $time = strtotime($applicationStatusAnnouncementStartDate_date . ' ' . $applicationStatusAnnouncementStartDate_time_hour . ':' . $applicationStatusAnnouncementStartDate_time_min . ':' . $applicationStatusAnnouncementStartDate_time_sec);
            $SettingNameValueAry['applicationStatusAnnouncementStartDate'] = ($applicationStatusAnnouncementStartDate_date?date('Y-m-d H:i:s',$time):'');
            $result = $libkis_admission->saveBasicSettings($schoolYearId, $SettingNameValueAry);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;


        case 'updateBriefingAnnouncementSettings':
            $SettingNameValueAry = array();
            $SettingNameValueAry['briefingAnnouncement'] = $briefingAnnouncement;
            $result = $libkis_admission->saveBasicSettings($schoolYearId, $SettingNameValueAry);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'updateBriefingApplicationStatusByIds':
            $result = $abb->updateBriefingApplicationStatusByIds($briefingApplicationIds, $status);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case 'updateBriefingApplicationSessionByIds':
            $result = $abb->updateBriefingApplicationSessionByIds($briefingApplicationIds, $briefingID);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;

        case "briefingApplicantLottery":
            $result = $abb->briefingApplicantLottery(IntegerSafe($_POST['schoolYearId']));
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;
            
        case 'updateOnlinePaymentSettings':
        	$SettingNameValueAry = array();
            $SettingNameValueAry['onlinepaymentamount'] = $onlinepaymentamount;
            $SettingNameValueAry['enablepaypal'] = $enablepaypal;
            $SettingNameValueAry['paypalsignature'] = $paypalsignature;
            $SettingNameValueAry['paypalhostedbuttonid'] = $paypalhostedbuttonid;
            $SettingNameValueAry['paypalname'] = $paypalname;
            $SettingNameValueAry['enablealipayhk'] = $enablealipayhk;
            
            $result = $libkis_admission->logBasicSettings(99999, array_keys($SettingNameValueAry));
            $result = $libkis_admission->saveBasicSettings(99999, $SettingNameValueAry);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;
        case 'updateModuleSettings':
        	$SettingNameValueAry = array();
            $SettingNameValueAry['enableinterviewarrangement'] = $enableinterviewarrangement;
            $SettingNameValueAry['enableinterviewform'] = $enableinterviewform;
            $SettingNameValueAry['enablebriefingsession'] = $enablebriefingsession;
            
            $result = $libkis_admission->logBasicSettings(99999, array_keys($SettingNameValueAry));
            $result = $libkis_admission->saveBasicSettings(99999, $SettingNameValueAry);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
        	break;
        case 'updateOnlinePaymentSettings':
        	$SettingNameValueAry = array();
            $SettingNameValueAry['enableinterviewarrangement'] = $enableinterviewarrangement;
            $SettingNameValueAry['enableinterviewform'] = $enableinterviewform;
            $SettingNameValueAry['enablebriefingsession'] = $enablebriefingsession;
            
            $result = $libkis_admission->logBasicSettings(99999, array_keys($SettingNameValueAry));
            $result = $libkis_admission->saveBasicSettings(99999, $SettingNameValueAry);
            if ($result) {
                echo 'UpdateSuccess';
            } else {
                echo 'UpdateUnsuccess';
            }
            break;
    } // end of switch
}

?>
