<?

// Editing by 
/*
 * 2019-10-18 (Tommy) added STCC cust field
 * 2018-08-07 (Henry) apply briefing search in briefing applicantlist
 * 2017-10-09 (Pun): Modified interview/import, added importapplicant
 * 2016-08-25 (Ronald): Added $sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] customization
 * 2016-08-25 (Ronald): Added $sys_custom['KIS_Admission']['MINGWAI']['Settings'] customization
 * 2016-04-13 (Henry): added CustSelection filter getKtlmsCustSelection
 * 2016-02-16 (Henry): added paymentStatus filter for attachment_download
 * 2016-01-25 (Omas): added paymentStatus filter for $sys_custom['KIS_Admission']['PayPal']
 * 2015-10-09 (Henry) add case for interviewusersettings and interviewusersettings_new
 * 2015-09-29 (Henry) added TSUENWANBCKG_waitingforinterview
 * 2015-07-31 (Henry) added case for interviewarrangement
 * 2015-07-30 (Pun): Added Briefing Module
 * 2015-07-20 (Pun): Added $sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings'] customization
 * 2015-07-20 (Henry): Added Interview Arrangement Page
 * 2015-07-16 (Omas) : Added $sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings'] customization
 * 2015-07-09 (Henry): fixed: cannot display application no. if the no. does not contain any alphabet
 * 2015-01-20 (Henry): added classChangeLevelSelection at listbyform
 * 2014-09-19 (Henry): Added $kis_data['maxUploadSize']
 * 2014-08-20 (Henry): Added case "import"
 * 2014-08-12 (Henry): Modified /applicationlist/compose/
 * 2014-08-07 (Carlos): Added settings > email_template
 * 2014-02-04 (Henry): Added icms customization
 * 2014-01-15 (Carlos): Added [settings > attachmentsettings]
 */
$PATH_WRT_ROOT = "../../../";
include_once ($PATH_WRT_ROOT . 'kis/init.php');
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/config.php");
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_cust.php");
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_ui_cust.php");
include_once ($PATH_WRT_ROOT . "includes/admission/libadmission_briefing_base.php");
include_once ($PATH_WRT_ROOT . "includes/json.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
$json = new JSON_obj();
$arrCookies = array ();
$arrCookies[] = array (
	"admission_school_year_id",
	"selectSchoolYearID"
);
if (isset ($clearCoo) && $clearCoo == 1) {
	$arrCookies[] = array (
		"admission_page",
		"page"
	);
	$arrCookies[] = array (
		"admission_amount",
		"amount"
	);
	$arrCookies[] = array (
		"admission_total",
		"total"
	);
	$arrCookies[] = array (
		"admission_sortby",
		"sortby"
	);
	$arrCookies[] = array (
		"admission_order",
		"order"
	);
	$arrCookies[] = array (
		"admission_current_class_level",
		"currentClassLevel"
	);
	clearCookies($arrCookies);
} else {
	updateGetCookies($arrCookies);
}
if ($kis_user['type'] == kis :: $user_types['teacher']) {
	$libkis_admission = $libkis->loadApp('admission');
	// init
	$kis_data['lauc'] = new admission_ui_cust();
	$kis_data['admission_cfg'] = $admission_cfg;
	$kis_data['libadmission'] = $libkis_admission;
	$kis_data['libinterface'] = new interface_html();
	$kis_data['file_path'] = $file_path;
	$kis_data['blankphoto'] = $kis_config['blankphoto'];
	$kis_data['mustfillinsymbol'] = $kis_data['libinterface']->RequiredSymbol();
	$kis_data['ReturnMsgKey'] = $sysMsg;
	if ($q[0] == 'applicantslist' && $q[1] == 'details') { // #/apps/admission/applicantslist/details/
		$selectSchoolYearID = $q[2];
	}

	$kis_data['schoolYearID'] = $selectSchoolYearID ? $selectSchoolYearID : $libkis_admission->schoolYearID;
	if (!$kis_data['schoolYearID']) {
		$kis_data['schoolYearID'] = $_SESSION['CurrentSchoolYearID'];
		$kis_data['warning_msg'] = $libkis_admission->displayWarningMsg('notyetsetnextschoolyear');
	}
	elseif ($libkis_admission->hasApplicationSetting($kis_data['schoolYearID']) == 0) {
		$kis_data['warning_msg'] = $libkis_admission->displayWarningMsg('notyetsetapplicationsetting');
	}
	$setting = $libkis_admission->getApplicationSetting($kis_data['schoolYearID']);

	$selectedSchoolYear = current(kis_utility :: getAcademicYears(array (
		'AcademicYearID' => $kis_data['schoolYearID']
	)));
	$kis_data['schoolYearSelection'] = $libkis_admission->getAcademicYearSelection($kis_data['schoolYearID']);
	$classLevelIdAry = array_keys($libkis_admission->classLevelAry);
	
	$kis_data['moduleSettings'] = $libkis_admission->getBasicSettings(99999, array (
						'enableinterviewarrangement','enableinterviewform','enablebriefingsession','schoolnamechi','schoolnameeng'
					));
					
	switch ($q[0]) {

		case 'updatestatus' :
			switch ($q[1]) {
				default : // updatestatus/main/
					// $classLevelID = isset($_REQUEST['selectClassLevelID'])?$_REQUEST['selectClassLevelID']:$classLevelID;
					// $kis_data['classLevelSelection'] = $libkis_admission->getClassLevelSelection($classLevelID,'selectClassLevelID',true,$kis_lang['all']);
					// $kis_data['emailListAry'] = $libkis_admission->getEmailListAry($kis_data['schoolYearID'], $keyword,$classLevelID);
					// new [start]
					// $kis_data['updatestatusAry'] = $libkis_admission->getUpdateStatusAry($kis_data['schoolYearID']);
					$applicationDetails = $libkis_admission->getUpdateStatusAry($kis_data['schoolYearID']);
					$count = count($applicationDetails);
					$kis_data['applicationDetails'] = array ();
					for ($i = 0; $i < $count; $i++) {
						$_applicationId = $applicationDetails[$i]['application_id'];

						$kis_data['applicationDetails'][$_applicationId]['ClassLevelName'] = $applicationDetails[$i]['ClassLevelName'];
						$kis_data['applicationDetails'][$_applicationId]['student_name'] = $applicationDetails[$i]['student_name'];
						if ($sys_custom['KIS_Admission']['ICMS']['Settings']) {
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType1'] = $applicationDetails[$i]['ApplyDayType1'];
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType2'] = $applicationDetails[$i]['ApplyDayType2'];
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType3'] = $applicationDetails[$i]['ApplyDayType3'];
						}
						$kis_data['applicationDetails'][$_applicationId]['parent_name'][] = $applicationDetails[$i]['parent_name'];
						$kis_data['applicationDetails'][$_applicationId]['parent_phone'][] = $applicationDetails[$i]['parent_phone'];
						$kis_data['applicationDetails'][$_applicationId]['application_status'] = $applicationDetails[$i]['application_status'];
						$kis_data['applicationDetails'][$_applicationId]['record_id'] = $applicationDetails[$i]['record_id'];
						$kis_data['applicationDetails'][$_applicationId]['modified_by'] = $applicationDetails[$i]['modified_by'];
						$kis_data['applicationDetails'][$_applicationId]['date_modified'] = $applicationDetails[$i]['date_modified'];
					}
					$kis_data['statusSelection'] = $libkis_admission->getStatusSelection($selectStatus, 'status', $auto_submit = false, $isAll = false);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
					$kis_data['applicationSetting'] = $setting;
					if ($sys_custom['KIS_Admission']['ICMS']['Settings'])
						$kis_data['applicationlistAry'] = $libkis_admission->getApplicationListAry($kis_data['schoolYearID']);
					// new [end]
					$status = in_array($selectStatus, $admission_cfg['Status']) ? $selectStatus : '';
					$kis_data['applicationStatus'] = $libkis_admission->getStatusSelection($status, $name = "selectStatus", false, false);
					$main_template = 'updatestatus/main';
			}
			break;

		case 'emaillist' :
			switch ($q[1]) {
				case 'compose' : // /emaillist/compose/
				case 'edit' : // /emaillist/edit/
					if (isset ($_REQUEST['classLevelID'])) {
						$kis_data['classLevelID'] = $_REQUEST['classLevelID'];
					}
					if (isset ($_REQUEST['applicationIds'])) {
						$kis_data['applicationIds'] = $_REQUEST['applicationIds'];
					}
					if (isset ($_REQUEST['selectStatus'])) {
						$data = array (
							'classLevelID' => $kis_data['classLevelID'],
							'status' => $_REQUEST['selectStatus']
						);
						list ($total, $tempApplicationDetails) = $libkis_admission->getApplicationDetails($kis_data['schoolYearID'], $data);
						$applicationDetailsArr = array ();
						foreach ($tempApplicationDetails as $anApplicationDetails) {
							if (in_array($anApplicationDetails['record_id'], $applicationDetailsArr))
								continue;
							$kis_data['applicationIds'] .= $anApplicationDetails['record_id'] . ',';
							$applicationDetailsArr[] = $anApplicationDetails['record_id'];
						}
						$kis_data['applicationIds'] = substr($kis_data['applicationIds'], 0, -1);
					}
					if (isset ($_REQUEST['recordID'])) {
						$kis_data['recordID'] = $_REQUEST['recordID'];
						$kis_data['emailRecordDetail'] = $libkis_admission->getEmailRecordDetail($kis_data['recordID']);
						$kis_data['applicationIds'] = implode(",", $kis_data['emailRecordDetail']['applicationIds']);
						$kis_data['emailReceivers'] = $libkis_admission->getEmailReceivers($kis_data['applicationIds'], $kis_data['recordID']);
						$kis_data['classLevelID'] = $kis_data['emailReceivers'][0]['classLevelID'];
					} else {
						$kis_data['emailReceivers'] = $libkis_admission->getEmailReceivers($kis_data['applicationIds'], $kis_data['recordID']);
					}

					// $kis_data['applicationInfo'] = current($libkis_admission->getApplicationStatus($kis_data['schoolYearID'],$kis_data['classLevelID'],$applicationID='',''));
					// $kis_data['applicationID'] = $kis_data['applicationInfo']['applicationID'];
					// $kis_data['classLevelID'] = $kis_data['applicationInfo']['classLevelID'];

					$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
					$NavArr[] = array (
						'emaillist/',
						$kis_data['schoolYear'] . ' (' . $libkis_admission->classLevelAry[$kis_data['classLevelID']] . ')'
					);
					// $NavArr[] = array('emaillist/',$libkis_admission->classLevelAry[$kis_data['classLevelID']]);
					$NavArr[] = array (
						'',
						$kis_data['recordID'] == '' ? $kis_lang['sendemail'] : $kis_lang['editemail']
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					// if($kis_data['recordID']!=''){
					// $kis_data['emailRecordDetail'] = $libkis_admission->getEmailRecordDetail($kis_data['recordID']);
					// $kis_data['applicationIds'] = implode(",",$kis_data['emailRecordDetail']['applicationIds']);
					// }else{
					// $kis_data['applicationIds'] = $_REQUEST['applicationIds'];
					// }

					$main_template = $kis_data['recordID'] == '' ? 'emaillist/compose' : 'emaillist/edit';
					break;
				default : // emaillist/main/
					// $kis_data['classLevelAry'] = $libkis_admission->classLevelAry;
					// $kis_data['applicationSetting'] = $setting;
					$classLevelID = isset ($_REQUEST['selectClassLevelID']) ? $_REQUEST['selectClassLevelID'] : $classLevelID;
					$kis_data['classLevelSelection'] = $libkis_admission->getClassLevelSelection($classLevelID, 'selectClassLevelID', true, $kis_lang['all']);
					$kis_data['emailListAry'] = $libkis_admission->getEmailListAry($kis_data['schoolYearID'], $keyword, $classLevelID);
					$main_template = 'emaillist/main';
			}
			break;

		case 'briefing' :
			$admission_briefing_base = new admission_briefing_base();
			switch ($q[1]) {
				case 'announcementsettings' : // briefing/announcementsettings/
					$kis_data['basicSettings'] = $libkis_admission->getBasicSettings($kis_data['schoolYearID'], array (
						'briefingAnnouncement',
					));

					$main_template = 'briefing/announcementsettings';
					break;
				case 'announcementsettings_edit' : // briefing/announcementsettings_edit/
					$kis_data['basicSettings'] = $libkis_admission->getBasicSettings($kis_data['schoolYearID'], array (
						'briefingAnnouncement'
					));
					$NavArr[] = array (
						'',
						$kis_lang['edit']
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
					$main_template = 'briefing/announcementsettings_edit';
					break;
				case 'applicantlist' : // /briefing/applicantList/
					$sessionDetails = $admission_briefing_base->getBriefingSession($_REQUEST['recordID']);

					$NavArr[] = array (
						'briefing/',
						$kis_lang['briefing']
					);
					$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
					$NavArr[] = array (
						'briefing/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						$kis_data['schoolYear']
					);

					if ($q[2] == 'search') {
						$NavArr[] = array (
							'',
							$kis_lang['search_result']
						);
					} else {
						$NavArr[] = array (
							'',
							substr($sessionDetails['BriefingStartDate'], 0, 10) . ' (' . substr($sessionDetails['BriefingStartDate'], 11) . ' ~ ' . substr($sessionDetails['BriefingEndDate'], 11) . ') ' . $sessionDetails['Title']
						);
					}

					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					$main_template = 'briefing/applicantList';
					break;
				case 'edit' : // /briefing/edit/
					if (isset ($_REQUEST['recordID'])) {
						$kis_data['recordID'] = $_REQUEST['recordID'];
					}

					$NavArr[] = array (
						'briefing/',
						$kis_lang['briefing']
					);
					$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
					$NavArr[] = array (
						'briefing/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						$kis_data['schoolYear']
					);
					$NavArr[] = array (
						'',
						$kis_data['recordID'] == '' ? $kis_lang['new'] : $kis_lang['edit']
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					$main_template = 'briefing/edit';
					break;
				default : // briefing/main/
					$kis_data['classLevelAry'] = $libkis_admission->classLevelAry;
					$kis_data['interviewSettingAry'] = $libkis_admission->getInterviewSettingAry('', '', '', '', $keyword, $kis_data['schoolYearID']);
					$main_template = 'briefing/main';
					break;
			}
			break;
		case 'interview' :
			// for interview marker
			if (!$q[1] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission-interview-marker"]) {
				$q[1] = 'interviewforminput';
			}
			switch ($q[1]) {
				case 'details' : // /interview/details/
					if (isset ($_REQUEST['selectInterviewRound'])) {
						$kis_data['selectInterviewRound'] = ($_REQUEST['selectInterviewRound'] ? $_REQUEST['selectInterviewRound'] : '');
					} else {
						$kis_data['selectInterviewRound'] = ($q[2] ? $q[2] : '');
					}
					if (isset ($_REQUEST['recordID'])) {
						$kis_data['recordID'] = $_REQUEST['recordID'];
					} else {
						$kis_data['recordID'] = $q[3];
					}
					$kis_data['interviewListAry'] = $libkis_admission->getInterviewListAry($kis_data['recordID'], '', '', '', $keyword, $order, $sortby, $kis_data['selectInterviewRound']);
					$applicationDetails = $kis_data['interviewListAry'];
					$kis_data['applicationDetails'] = array ();
					for ($i = 0; $i < count($applicationDetails); $i++) {
						$_applicationId = $applicationDetails[$i]['application_id'];

						$kis_data['applicationDetails'][$_applicationId]['student_name'] = $applicationDetails[$i]['student_name'];
						if ($sys_custom['KIS_Admission']['ICMS']['Settings']) {
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType1'] = $applicationDetails[$i]['ApplyDayType1'];
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType2'] = $applicationDetails[$i]['ApplyDayType2'];
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType3'] = $applicationDetails[$i]['ApplyDayType3'];
						}
						if ($sys_custom['KIS_Admission']['HKUGAPS']['Settings']) {
							$kis_data['applicationDetails'][$_applicationId]['langspokenathome'] = $applicationDetails[$i]['LangSpokenAtHome'];
						}
						$kis_data['applicationDetails'][$_applicationId]['parent_name'][] = $applicationDetails[$i]['parent_name'];
						$kis_data['applicationDetails'][$_applicationId]['parent_phone'][] = $applicationDetails[$i]['parent_phone'];
						$kis_data['applicationDetails'][$_applicationId]['application_status'] = $applicationDetails[$i]['application_status'];
						$kis_data['applicationDetails'][$_applicationId]['record_id'] = $applicationDetails[$i]['record_id'];
						$kis_data['applicationDetails'][$_applicationId]['other_record_id'] = $applicationDetails[$i]['other_record_id'];
					}

					$kis_data['interviewSettingAry'] = $libkis_admission->getInterviewSettingAry($kis_data['recordID']);

					$kis_data['enableNew'] = $kis_data['interviewSettingAry'][0]['Quota'] > $kis_data['interviewSettingAry'][0]['Applied'];

					$NavArr[] = array (
						'interview/',
						$kis_lang['interview']
					);
					$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
					$NavArr[] = array (
						'interview/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						$kis_data['schoolYear']
					);
					if ($kis_data['selectInterviewRound']) {
						$NavArr[] = array (
							'interview/' . $kis_data['selectInterviewRound'] . '/?selectSchoolYearID=' . $kis_data['schoolYearID'],
							sprintf($kis_lang['round'], $kis_data['selectInterviewRound'])
						);
					}
					$NavArr[] = array (
						'',
						$kis_data['interviewSettingAry'][0]['Date'] . ' (' . substr($kis_data['interviewSettingAry'][0]['StartTime'], 0, -3) . ' ~ ' . substr($kis_data['interviewSettingAry'][0]['EndTime'], 0, -3) . ') ' . ($kis_data['interviewSettingAry'][0]['GroupName'] ? $kis_lang['sessiongroup'] . ' ' . $kis_data['interviewSettingAry'][0]['GroupName'] : '')
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					$main_template = 'interview/details';
					break;
				case 'details_add' :
					if (isset ($_REQUEST['selectInterviewRound'])) {
						$kis_data['selectInterviewRound'] = ($_REQUEST['selectInterviewRound'] ? $_REQUEST['selectInterviewRound'] : '');
					} else {
						$kis_data['selectInterviewRound'] = ($q[2] ? $q[2] : '');
					}
					if (isset ($_REQUEST['recordID'])) {
						$kis_data['recordID'] = $_REQUEST['recordID'];
					} else {
						$kis_data['recordID'] = $q[3];
					}

					// generate navigation bar
					$kis_data['interviewSettingAry'] = $libkis_admission->getInterviewSettingAry($kis_data['recordID']);
					$NavArr[] = array (
						'interview/',
						$kis_lang['interview']
					);
					$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
					$NavArr[] = array (
						'interview/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						$kis_data['schoolYear']
					);
					if ($kis_data['selectInterviewRound']) {
						$NavArr[] = array (
							'interview/' . $kis_data['selectInterviewRound'] . '/?selectSchoolYearID=' . $kis_data['schoolYearID'],
							sprintf($kis_lang['round'], $kis_data['selectInterviewRound'])
						);
					}
					$NavArr[] = array (
						'interview/details/' . $kis_data['selectInterviewRound'] . '/' . $kis_data['recordID'] . '/',
						$kis_data['interviewSettingAry'][0]['Date'] . ' (' . substr($kis_data['interviewSettingAry'][0]['StartTime'], 0, -3) . ' ~ ' . substr($kis_data['interviewSettingAry'][0]['EndTime'], 0, -3) . ') ' . ($kis_data['interviewSettingAry'][0]['GroupName'] ? $kis_lang['sessiongroup'] . ' ' . $kis_data['interviewSettingAry'][0]['GroupName'] : '')
					);
					$NavArr[] = array (
						'',
						$kis_lang['new']
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					// ------------- generate unassigned applicant list [start]
					$page = $page ? $page : 1;
					$amount = $amount ? $amount : 10;
					$order = $order ? $order : '';
					$sortby = $sortby ? $sortby : '';
					$keyword = $keyword ? $keyword : '';

					if ($q[2] == 'search') {
						$NavArr[] = array (
							'applicantslist/',
							$selectedSchoolYear['academic_year_name_' . $intranet_session_language]
						);
						$NavArr[] = array (
							'',
							$kis_lang['search_result']
						);
					} else {
						$kis_data['classLevelID'] = $classLevelId = (in_array($q[2], $classLevelIdAry)) ? $q[2] : $classLevelIdAry[0];
						$status = in_array($selectStatus, $admission_cfg['Status']) ? $selectStatus : '';
						$kis_data['interviewStatusSelection'] = ($status == $admission_cfg['Status']['waitingforinterview']) ? $libkis_admission->getInterviewStatusSelection('selectInterviewStatus', $selectInterviewStatus) : '';
						$kis_data['classLevelSelection'] = $libkis_admission->getClassLevelSelection($classLevelId);
						$kis_data['classChangeLevelSelection'] = $libkis_admission->getClassLevelSelection($classLevelId, 'selectChangeClassLevelID');
						$kis_data['applicationStatus'] = $libkis_admission->getStatusSelection($status);
						$selectedClassLevel = $libkis_admission->classLevelAry[$kis_data['classLevelID']];
						$NavArr[] = array (
							'applicantslist/',
							$selectedSchoolYear['academic_year_name_' . $intranet_session_language]
						);
						$NavArr[] = array (
							'',
							$selectedClassLevel
						);
					}

					$data = array (
						'classLevelID' => $classLevelId,
						'applicationID' => $applicationID,
						'status' => $status,
						'interviewStatus' => $selectInterviewStatus,
						'keyword' => $keyword,
						'page' => $page,
						'amount' => $amount,
						'order' => $order,
						'sortby' => $sortby
					);
					list ($total, $applicationDetails) = $libkis_admission->getUnassignedInterviewApplicantReocrd($kis_data['recordID'], $data);
					$count = count($applicationDetails);
					$kis_data['applicationDetails'] = array ();
					for ($i = 0; $i < $count; $i++) {
						$_applicationId = (is_numeric($applicationDetails[$i]['application_id']) ? $applicationDetails[$i]['application_id'] . ' ' : $applicationDetails[$i]['application_id']);

						$kis_data['applicationDetails'][$_applicationId]['student_name'] = $applicationDetails[$i]['student_name'];
						if ($sys_custom['KIS_Admission']['ICMS']['Settings']) {
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType1'] = $applicationDetails[$i]['ApplyDayType1'];
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType2'] = $applicationDetails[$i]['ApplyDayType2'];
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType3'] = $applicationDetails[$i]['ApplyDayType3'];
						}
						$kis_data['applicationDetails'][$_applicationId]['parent_name'][] = $applicationDetails[$i]['parent_name'];
						$kis_data['applicationDetails'][$_applicationId]['parent_phone'][] = $applicationDetails[$i]['parent_phone'];
						$kis_data['applicationDetails'][$_applicationId]['application_status'] = $applicationDetails[$i]['application_status'];
						$kis_data['applicationDetails'][$_applicationId]['record_id'] = $applicationDetails[$i]['record_id'];
					}

					$kis_data['applicationDetails'] = array_slice($kis_data['applicationDetails'], (($page -1) * $amount));
					$kis_data['applicationDetails'] = array_slice($kis_data['applicationDetails'], 0, $amount);
					// $kis_data['unassignedApplicantAry'] = $libkis_admission->getUnassignedInterviewApplicantReocrd($kis_data['recordID']);
					// ------------- generate unassigned applicant list [end]

					if (!$kis_data['classLevelID'])
						$kis_data['warning_msg'] = $libkis_admission->displayWarningMsg('notyetsetclasslevelatinterview');

					$main_template = 'interview/details_add';
					break;
				case 'edit' : // /interview/edit/
					if (isset ($_REQUEST['recordID'])) {
						$kis_data['recordID'] = $_REQUEST['recordID'];
					}
					if (isset ($_REQUEST['selectInterviewRound'])) {
						$kis_data['selectInterviewRound'] = ($_REQUEST['selectInterviewRound'] ? $_REQUEST['selectInterviewRound'] : '');
					} else {
						$kis_data['selectInterviewRound'] = ($q[2] ? $q[2] : '');
					}
					$kis_data['interviewSettingAry'] = $libkis_admission->getInterviewSettingAry($kis_data['recordID'], '', '', '', $keyword);
					// $kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_'.$intranet_session_language];
					$NavArr[] = array (
						'interview/',
						$kis_lang['interview']
					);
					$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
					$NavArr[] = array (
						'interview/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						$kis_data['schoolYear']
					);
					$NavArr[] = array (
						'interview/' . ($kis_data['recordID'] == '' ? $kis_data['selectInterviewRound'] : $kis_data['interviewSettingAry'][0]['Round']) . '/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						sprintf($kis_lang['round'], ($kis_data['recordID'] == '' ? $kis_data['selectInterviewRound'] : $kis_data['interviewSettingAry'][0]['Round']))
					);
					$NavArr[] = array (
						'',
						$kis_data['recordID'] == '' ? $kis_lang['new'] : $kis_lang['edit']
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					// if($kis_data['recordID']!=''){
					// $kis_data['emailRecordDetail'] = $libkis_admission->getEmailRecordDetail($kis_data['recordID']);
					// $kis_data['applicationIds'] = implode(",",$kis_data['emailRecordDetail']['applicationIds']);
					// }else{
					// $kis_data['applicationIds'] = $_REQUEST['applicationIds'];
					// }
					$kis_data['classLevelSelection'] = $libkis_admission->getClassLevelSelection($kis_data['interviewSettingAry'][0]['ClassLevelID']);
					$main_template = 'interview/edit';
					break;
				case 'import' : // interview/import/
					// developing...
					$kis_data['csvFile'] = "<a class='tablelink' href='" . $PATH_WRT_ROOT . "kis/admission_form/get_sample_file.php?file=interview_settings" . ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? "2" : "3") . ".csv' target=\"_self\">" . $kis_lang['ClickHereToDownloadSample'] . "</a>";
					//
					if (isset ($_REQUEST['selectInterviewRound'])) {
						$kis_data['selectInterviewRound'] = ($_REQUEST['selectInterviewRound'] ? $_REQUEST['selectInterviewRound'] : '');
					} else {
						$kis_data['selectInterviewRound'] = ($q[2] ? $q[2] : '');
					}
					if (isset ($_REQUEST['selectSchoolYearID'])) {
						$kis_data['selectSchoolYearID'] = $_REQUEST['selectSchoolYearID'];
					}

					$NavArr[] = array (
						'interview/',
						$kis_lang['interview']
					);
					$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
					$NavArr[] = array (
						'interview/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						$kis_data['schoolYear']
					);
					$NavArr[] = array (
						'interview/' . $kis_data['selectInterviewRound'] . '/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						sprintf($kis_lang['round'], $kis_data['selectInterviewRound'])
					);
					$NavArr[] = array (
						'',
						$kis_lang['importinterviewtimeslotinfo']
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					$main_template = 'interview/import';
					break;
				case 'importapplicant' : // interview/importapplicant/
					if (isset ($_REQUEST['selectInterviewRound'])) {
						$kis_data['selectInterviewRound'] = ($_REQUEST['selectInterviewRound'] ? $_REQUEST['selectInterviewRound'] : '');
					} else {
						$kis_data['selectInterviewRound'] = ($q[2] ? $q[2] : '');
					}
					if (isset ($_REQUEST['selectSchoolYearID'])) {
						$kis_data['selectSchoolYearID'] = $_REQUEST['selectSchoolYearID'];
					}

					$kis_data['interviewSettingAry'] = $libkis_admission->getInterviewSettingAry('', '', '', '', $keyword, $kis_data['schoolYearID'], '', $kis_data['selectInterviewRound']);

					$NavArr[] = array (
						'interview/',
						$kis_lang['interview']
					);
					$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
					$NavArr[] = array (
						'interview/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						$kis_data['schoolYear']
					);
					if(!$sys_custom['KIS_Admission']['HKUGAC']['Settings']) {
                        $NavArr[] = array(
                            'interview/' . $kis_data['selectInterviewRound'] . '/?selectSchoolYearID=' . $kis_data['schoolYearID'],
                            sprintf($kis_lang['round'], $kis_data['selectInterviewRound'])
                        );
                    }
					$NavArr[] = array (
						'',
						$kis_lang['importinterviewapplicant']
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					$main_template = 'interview/importapplicant';
					break;
				case 'interviewarrangement' : // interview/interviewarrangement/
					// $kis_data['csvFile'] = "<a class='tablelink' href='". $PATH_WRT_ROOT ."kis/admission_form/get_sample_file.php?file=interview_settings.csv' target=\"_self\">". $kis_lang['ClickHereToDownloadSample'] ."</a>";
					//
					if (isset ($_REQUEST['selectInterviewRound'])) {
						$kis_data['selectInterviewRound'] = ($_REQUEST['selectInterviewRound'] ? $_REQUEST['selectInterviewRound'] : '');
					} else {
						$kis_data['selectInterviewRound'] = ($q[2] ? $q[2] : '');
					}
					if (isset ($_REQUEST['selectSchoolYearID'])) {
						$kis_data['selectSchoolYearID'] = $_REQUEST['selectSchoolYearID'];
					}
					$kis_data['applicationStatus'] = $libkis_admission->getStatusSelection($status, "selectStatus[]", false, false, true);
					$NavArr[] = array (
						'interview/',
						$kis_lang['interview']
					);
					$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
					$NavArr[] = array (
						'interview/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						$kis_data['schoolYear']
					);
					$NavArr[] = array (
						'interview/' . $kis_data['selectInterviewRound'] . '/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						sprintf($kis_lang['round'], $kis_data['selectInterviewRound'])
					);
					$NavArr[] = array (
						'',
						$kis_lang['importinterviewinfobyarrangement']
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					$main_template = 'interview/interviewarrangement';
					break;
				case 'interviewformresult' : // interview/interviewformresult
					if (isset ($_REQUEST['selectInterviewRound'])) {
						$kis_data['selectInterviewRound'] = ($_REQUEST['selectInterviewRound'] ? $_REQUEST['selectInterviewRound'] : 1);
					} else {
						$kis_data['selectInterviewRound'] = ($q[3] ? $q[3] : 1);
					}

					/* Parameter */
					$page = $page ? $page : 1;
					$amount = $amount ? $amount : 10;
					$order = $order ? $order : '';
					$sortby = $sortby ? $sortby : '';
					$keyword = $keyword ? $keyword : '';

					if ($q[2] == 'search') {
						$NavArr[] = array (
							'applicantslist/',
							$selectedSchoolYear['academic_year_name_' . $intranet_session_language]
						);
						$NavArr[] = array (
							'',
							$kis_lang['search_result']
						);
					} else {
						$kis_data['classLevelID'] = $classLevelId = (in_array($q[2], $classLevelIdAry)) ? $q[2] : $classLevelIdAry[0];
						$status = in_array($selectStatus, $admission_cfg['Status']) ? $selectStatus : '';
						$kis_data['interviewStatusSelection'] = ($status == $admission_cfg['Status']['waitingforinterview']) ? $libkis_admission->getInterviewStatusSelection('selectInterviewStatus', $selectInterviewStatus) : '';
						$kis_data['classLevelSelection'] = $libkis_admission->getClassLevelSelection($classLevelId);
						$kis_data['classChangeLevelSelection'] = $libkis_admission->getClassLevelSelection($classLevelId, 'selectChangeClassLevelID');
						$kis_data['applicationStatus'] = $libkis_admission->getStatusSelection($status);
						$selectedClassLevel = $libkis_admission->classLevelAry[$kis_data['classLevelID']];
					}
					$data = array (
						'classLevelID' => $classLevelId,
						'applicationID' => $applicationID,
						'status' => $status,
						'interviewStatus' => $selectInterviewStatus,
						'keyword' => $keyword,
						'page' => $page,
						'amount' => $amount,
						'order' => $order,
						'sortby' => $sortby,
						'round' => $kis_data['selectInterviewRound'],
						'formQuestionID' => $selectFormQuestionID,
						'formAnswer' => $selectFormAnswerID,
						'interviewDate' => $selectInterviewDate,
					);

					list ($total, $applicationDetails) = $libkis_admission->getInterviewFormReport($kis_data['schoolYearID'], $data);
					$count = count($applicationDetails);
					// debug_pr($applicationDetails);
					$kis_data['interviewQuestionAnswer'] = $libkis_admission->getInterviewFormQuestionAnswerAry();
					// debug_pr($kis_data['interviewQuestionAnswer']);
					$kis_data['applicationDetails'] = array ();
					$unsetapplication = array ();
					for ($i = 0; $i < $count; $i++) {

						$_applicationId = (is_numeric($applicationDetails[$i]['application_id']) ? $applicationDetails[$i]['application_id'] . ' ' : $applicationDetails[$i]['application_id']);

						$kis_data['applicationDetails'][$_applicationId]['InterviewDate'] = $applicationDetails[$i]['InterviewDate'];
						$kis_data['applicationDetails'][$_applicationId]['student_name'] = $applicationDetails[$i]['student_name'];
						if ($sys_custom['KIS_Admission']['ICMS']['Settings']) {
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType1'] = $applicationDetails[$i]['ApplyDayType1'];
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType2'] = $applicationDetails[$i]['ApplyDayType2'];
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType3'] = $applicationDetails[$i]['ApplyDayType3'];
						}
						$kis_data['applicationDetails'][$_applicationId]['parent_name'][] = $applicationDetails[$i]['parent_name'];
						$kis_data['applicationDetails'][$_applicationId]['parent_phone'][] = $applicationDetails[$i]['parent_phone'];
						$kis_data['applicationDetails'][$_applicationId]['application_status'] = $applicationDetails[$i]['application_status'];
						$kis_data['applicationDetails'][$_applicationId]['record_id'] = $applicationDetails[$i]['record_id'];
						$kis_data['applicationDetails'][$_applicationId]['Marks'] = $applicationDetails[$i]['Marks'];
						$kis_data['applicationDetails'][$_applicationId]['QuestionAnswers'] = $kis_data['interviewQuestionAnswer'][$applicationDetails[$i]['record_id']][$applicationDetails[$i]['InterviewSettingID']];
						if ($applicationDetails[$i]['FormID'] && !$formIdForSelectionQuestion) {
							$formIdForSelectionQuestion = $applicationDetails[$i]['FormID'];
						}
						// debug_pr($kis_data['applicationDetails'][$_applicationId]['QuestionAnswers']);
						if ($selectFormQuestionID != '' && $selectFormAnswerID != '') {
							$hasQuestion = false;
							if (!$kis_data['applicationDetails'][$_applicationId]['QuestionAnswers']) {
								unset ($kis_data['applicationDetails'][$_applicationId]);
								if (!in_array($_applicationId, $unsetapplication)) {
									$total--;
								}
								$unsetapplication[] = $_applicationId;
							} else {
								foreach ($kis_data['applicationDetails'][$_applicationId]['QuestionAnswers'] as $aQestion) {
									$j = 0;
									foreach ($aQestion['QuestionsID'] as $_aQestion) {
										if ($_aQestion == $selectFormQuestionID && $aQestion['Answers'][$j] == $selectFormAnswerID) {
											$hasQuestion = true;
										}
										$j++;
									}
								}
								if (!$hasQuestion) {
									unset ($kis_data['applicationDetails'][$_applicationId]);
									if (!in_array($_applicationId, $unsetapplication)) {
										$total--;
									}
									$unsetapplication[] = $_applicationId;
								}
							}
						}
					}

					$kis_data['interviewFormQuestionSelection'] = $libkis_admission->getInterviewFormQuestionSelection($formIdForSelectionQuestion, $selectFormQuestionID, $name = 'selectFormQuestionID', $firstTitle = true, $kis_lang['interviewquestion'], $isMultiSelect = false, $otherTag = 'class="auto_submit"');
					$kis_data['interviewFormAnswerSelection'] = $libkis_admission->getInterviewFormAnswerSelection($selectFormQuestionID, $selectFormAnswerID, $name = 'selectFormAnswerID', $firstTitle = true, $kis_lang['interviewanswer'], $isMultiSelect = false, $otherTag = 'class="auto_submit"');
					if($sys_custom['KIS_Admission']['CREATIVE']['Settings'])
					    $kis_data['interviewDateSelection'] = $libkis_admission->getInterviewDateSelection($selectInterviewDate,'selectInterviewDate',true,$kis_lang['interviewdate'], $kis_data['schoolYearID'], $classLevelId, $kis_data['selectInterviewRound'], 'class="auto_submit"');

					$kis_data['applicationDetails'] = array_slice($kis_data['applicationDetails'], (($page -1) * $amount));
					$kis_data['applicationDetails'] = array_slice($kis_data['applicationDetails'], 0, $amount);

					$kis_data['statusSelection'] = $libkis_admission->getStatusSelection($selectStatus, 'status', $auto_submit = false, $isAll = false);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
					$kis_data['applicationSetting'] = $setting;
					if ($sys_custom['KIS_Admission']['ICMS']['Settings'])
						$kis_data['applicationlistAry'] = $libkis_admission->getApplicationListAry($kis_data['schoolYearID']);

					$main_template = 'interview/interviewformresult';
					break;
				case 'interviewformbuilter' : // interview/interviewformbuilter
					$kis_data['classLevelAry'] = $libkis_admission->classLevelAry;
					$kis_data['interviewSettingAry'] = $libkis_admission->getInterviewFormSetting('', '', '', $keyword, $kis_data['schoolYearID'], '');
					$main_template = 'interview/interviewformbuilter';
					break;
				case 'interviewformbuilter_edit' : // interview/interviewformbuilter_edit
					if (isset ($_REQUEST['recordID'])) {
						$kis_data['recordID'] = $_REQUEST['recordID'];
					} else {
						$kis_data['recordID'] = ($q[2] ? $q[2] : '');
					}
					// $kis_data['interviewSettingAry'] = $libkis_admission->getInterviewFormBuilterAry('', '', '', '', $keyword, $kis_data['schoolYearID'], '');
					if ($kis_data['recordID']) {
						$kis_data['interviewFormSettingAry'] = current($libkis_admission->getInterviewFormSetting($kis_data['recordID'], '', '', $keyword, $kis_data['schoolYearID'], ''));
						$kis_data['interviewQuestionAry'] = $libkis_admission->getInterviewFormBuilterAry($kis_data['recordID']);
					} // $NavArr[] = array('interview/interviewformbuilter/', $kis_lang['interview']);
					$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
					$NavArr[] = array (
						'interview/interviewformbuilter/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						$kis_data['schoolYear']
					);
					$NavArr[] = array (
						'',
						$kis_data['recordID'] == '' ? $kis_lang['new'] : $kis_lang['edit']
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					// if($kis_data['recordID']!=''){
					// $kis_data['emailRecordDetail'] = $libkis_admission->getEmailRecordDetail($kis_data['recordID']);
					// $kis_data['applicationIds'] = implode(",",$kis_data['emailRecordDetail']['applicationIds']);
					// }else{
					// $kis_data['applicationIds'] = $_REQUEST['applicationIds'];
					// }
					$kis_data['classLevelSelection'] = $libkis_admission->getClassLevelSelection(explode(',', $kis_data['interviewFormSettingAry']['ClassLevelID']), 'selectClassLevelID[]', false, '', true, 'size=5');

					$main_template = 'interview/interviewformbuilter_edit';
					break;
				case 'interviewforminput' : // interview/interviewforminput
					if (isset ($_REQUEST['selectInterviewDate'])) {
						$kis_data['selectInterviewDate'] = ($_REQUEST['selectInterviewDate'] ? $_REQUEST['selectInterviewDate'] : '');
					} else {
						$kis_data['selectInterviewDate'] = ($q[3] ? $q[3] : '');
					}
					if (isset ($_REQUEST['selectInterviewRound'])) {
						$kis_data['selectInterviewRound'] = ($_REQUEST['selectInterviewRound'] ? $_REQUEST['selectInterviewRound'] : '1');
					} else {
						$kis_data['selectInterviewRound'] = ($q[2] ? $q[2] : '1');
					}

					// ---
					$kis_data['classLevelAry'] = $libkis_admission->classLevelAry;
					$kis_data['interviewSettingAry'] = $libkis_admission->getInterviewSettingAry('', $kis_data['selectInterviewDate'], '', '', $keyword, $kis_data['schoolYearID'], '', $kis_data['selectInterviewRound']);
					$kis_data['interviewDateSelection'] = $libkis_admission->getInterviewDateSelection($kis_data['selectInterviewDate'], 'selectInterviewDate', true, $kis_lang['interviewdate'], $kis_data['schoolYearID']);
					// debug_pr($kis_data['interviewSettingAry']);
					for ($i = 0; $i < count($kis_data['interviewSettingAry']); $i++) {
						// $kis_data['interviewFormSettingAry'] = current($libkis_admission->getInterviewFormSetting('', date("Y-m-d H:i:s"), date("Y-m-d H:i:s"), $keyword, $kis_data['schoolYearID'], $kis_data['interviewSettingAry'][$i]['ClassLevelID']));
						// $kis_data['interviewFormResultAry'] = $libkis_admission->getInterviewFormResultAry($kis_data['interviewFormSettingAry']['RecordID'], $kis_data['applicationID'], '', $kis_data['interviewSettingAry'][$i]['RecordID']);
						// $kis_data['interviewFormResultAryById'][$kis_data['interviewSettingAry'][$i]['RecordID']] = $kis_data['interviewFormSettingAry']['ResultCount'];
						// debug_pr($kis_data['interviewFormSettingAry']);
						// debug_pr($kis_data['interviewFormResultAry']);
						// debug_pr($kis_data['interviewFormResultAryById'][$kis_data['interviewSettingAry'][$i]['RecordID']]);

						$kis_data['interviewFormSettingAry'] = current($libkis_admission->getInterviewFormSetting('', date("Y-m-d H:i:s"), date("Y-m-d H:i:s"), $keyword, $kis_data['schoolYearID'], $kis_data['interviewSettingAry'][$i]['ClassLevelID']));
						// debug_pr($kis_data['interviewFormSettingAry']);
						$kis_data['interviewListAry'] = $libkis_admission->getInterviewFromInputListAry($kis_data['interviewSettingAry'][$i]['RecordID'], '', '', '', '', '', '', $kis_data['selectInterviewRound'], 1);
						// debug_pr($kis_data['interviewListAry']);
						$applicationDetails = $kis_data['interviewListAry'];
						$kis_data['applicationDetails'] = array ();
						$kis_data['interviewFormResultAryById'][$kis_data['interviewSettingAry'][$i]['RecordID']] = 0;
						for ($j = 0; $j < count($applicationDetails); $j++) {
							$_applicationId = $applicationDetails[$j]['other_record_id'];
							// debug_pr($applicationDetails[$j]);
							$kis_data['interviewFormResultAryById'][$kis_data['interviewSettingAry'][$i]['RecordID']] += count($libkis_admission->getInterviewFormResultAry($kis_data['interviewFormSettingAry']['RecordID'], $_applicationId, '', $kis_data['interviewSettingAry'][$i]['RecordID']));
							// debug_pr('TEST:'.$kis_data['interviewSettingAry'][$i]['RecordID']);
							// debug_pr($libkis_admission->getInterviewFormResultAry($kis_data['interviewFormSettingAry']['RecordID'], $_applicationId));
						}
						// debug_pr($kis_data['interviewFormResultAryById'][$kis_data['interviewSettingAry'][$i]['RecordID']]);
					}
					// ---

					$main_template = 'interview/interviewforminput';
					break;
				case 'interviewforminput_details' : // /interview/interviewforminput_details/
					if (isset ($_REQUEST['selectInterviewRound'])) {
						$kis_data['selectInterviewRound'] = ($_REQUEST['selectInterviewRound'] ? $_REQUEST['selectInterviewRound'] : '');
					} else {
						$kis_data['selectInterviewRound'] = ($q[2] ? $q[2] : '');
					}
					if (isset ($_REQUEST['selectInterviewDate'])) {
						$kis_data['selectInterviewDate'] = $_REQUEST['selectInterviewDate'];
					} else {
						$kis_data['selectInterviewDate'] = ($q[3] ? $q[3] : '');
					}
					if (isset ($_REQUEST['recordID'])) {
						$kis_data['recordID'] = $_REQUEST['recordID'];
					} else {
						$kis_data['recordID'] = $q[4];
					}

					$order = $order ? $order : '';
					$sortby = $sortby ? $sortby : '';

					$kis_data['interviewListAry'] = $libkis_admission->getInterviewFromInputListAry($kis_data['recordID'], '', '', '', $keyword, $order, '', $kis_data['selectInterviewRound']);
					$applicationDetails = $kis_data['interviewListAry'];
					$kis_data['applicationDetails'] = array ();
					for ($i = 0; $i < count($applicationDetails); $i++) {
						$_applicationId = $applicationDetails[$i]['application_id'];
						$_classLevelId = $applicationDetails[$i]['ClassLevelID'];
						$kis_data['attachmentList'] = $libkis_admission->getAttachmentByApplicationID($kis_data['schoolYearID'], $applicationDetails[$i]['ApplicationID']);

						$kis_data['applicationDetails'][$_applicationId]['student_name'] = $applicationDetails[$i]['student_name'];
						if ($sys_custom['KIS_Admission']['ICMS']['Settings']) {
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType1'] = $applicationDetails[$i]['ApplyDayType1'];
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType2'] = $applicationDetails[$i]['ApplyDayType2'];
							$kis_data['applicationDetails'][$_applicationId]['ApplyDayType3'] = $applicationDetails[$i]['ApplyDayType3'];
						}
						$kis_data['applicationDetails'][$_applicationId]['parent_name'][] = $applicationDetails[$i]['parent_name'];
						$kis_data['applicationDetails'][$_applicationId]['parent_phone'][] = $applicationDetails[$i]['parent_phone'];
						$kis_data['applicationDetails'][$_applicationId]['application_status'] = $applicationDetails[$i]['application_status'];
						$kis_data['applicationDetails'][$_applicationId]['record_id'] = $applicationDetails[$i]['record_id'];
						$kis_data['applicationDetails'][$_applicationId]['other_record_id'] = $applicationDetails[$i]['other_record_id'];
						$kis_data['applicationDetails'][$_applicationId]['InputBy'] = $applicationDetails[$i]['InputBy'];
						$kis_data['applicationDetails'][$_applicationId]['DateModified'] = $applicationDetails[$i]['DateModified'];
						$kis_data['applicationDetails'][$_applicationId]['PhotoLink'] = $kis_data['attachmentList']['personal_photo']['link'];
					}

					$kis_data['interviewFormSettingAry'] = current($libkis_admission->getInterviewFormSetting('', date("Y-m-d H:i:s"), date("Y-m-d H:i:s"), $keyword, $kis_data['schoolYearID'], $_classLevelId));
					$kis_data['interviewFormResultAry'] = $libkis_admission->getInterviewFormResultAry($kis_data['interviewFormSettingAry']['RecordID'], $kis_data['applicationID'], '', $kis_data['recordID']);
					// debug_r($kis_data['interviewFormResultAry'] );

					$NavArr[] = array (
						'interview/interviewforminput/',
						$kis_lang['interviewforminput']
					);
					$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
					$NavArr[] = array (
						'interview/interviewforminput/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						$kis_data['schoolYear']
					);
					if ($kis_data['selectInterviewRound']) {
						$NavArr[] = array (
							'interview/interviewforminput/' . $kis_data['selectInterviewRound'] . '/?selectSchoolYearID=' . $kis_data['schoolYearID'],
							sprintf($kis_lang['round'], $kis_data['selectInterviewRound'])
						);
					}
					if ($kis_data['selectInterviewDate']) {
						$NavArr[] = array (
							'interview/interviewforminput/' . $kis_data['selectInterviewRound'] . '/' . $kis_data['selectInterviewDate'] . '/?selectSchoolYearID=' . $kis_data['schoolYearID'],
							$kis_data['selectInterviewDate']
						);
					}
					$NavArr[] = array (
						'',
						$kis_data['interviewListAry'][0]['Date'] . ' (' . substr($kis_data['interviewListAry'][0]['StartTime'], 0, -3) . ' ~ ' . substr($kis_data['interviewListAry'][0]['EndTime'], 0, -3) . ') ' . ($kis_data['interviewListAry'][0]['GroupName'] ? $kis_lang['sessiongroup'] . ' ' . $kis_data['interviewListAry'][0]['GroupName'] : '')
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					$main_template = 'interview/interviewforminput_details';
					break;
				case 'interviewforminput_details_view' : // /interview/interviewforminput_details_view/
					if (isset ($_REQUEST['selectInterviewRound'])) {
						$kis_data['selectInterviewRound'] = ($_REQUEST['selectInterviewRound'] ? $_REQUEST['selectInterviewRound'] : '');
					} else {
						$kis_data['selectInterviewRound'] = ($q[2] ? $q[2] : '');
					}
					if (isset ($_REQUEST['selectInterviewDate'])) {
						$kis_data['selectInterviewDate'] = $_REQUEST['selectInterviewDate'];
					} else {
						$kis_data['selectInterviewDate'] = ($q[3] ? $q[3] : '');
					}
					if (isset ($_REQUEST['recordID'])) {
						$kis_data['recordID'] = $_REQUEST['recordID'];
					} else {
						$kis_data['recordID'] = $q[4];
					}
					if (isset ($_REQUEST['applicationID'])) {
						$kis_data['applicationID'] = $_REQUEST['applicationID'];
					} else {
						$kis_data['applicationID'] = $q[5];
					}
					if (isset ($_REQUEST['inputBy'])) {
						$kis_data['inputBy'] = $_REQUEST['inputBy'];
					} else {
						$kis_data['inputBy'] = $q[6];
					}

					$order = $order ? $order : '';
					$sortby = $sortby ? $sortby : '';

					$kis_data['interviewListAry'] = $libkis_admission->getInterviewFromInputListAry($kis_data['recordID'], '', '', '', $keyword, $order, $sortby, $kis_data['selectInterviewRound']);
					$applicationDetails = $kis_data['interviewListAry'];

					// -- add interview form here ...
					// debug_pr($kis_data['interviewListAry']);
					$kis_data['studentInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], '', '', '', $kis_data['applicationID']));
					// debug_pr($kis_data['studentInfo']);
					$kis_data['classLevelAry'] = $libkis_admission->classLevelAry;

					$kis_data['interviewFormSettingAry'] = current($libkis_admission->getInterviewFormSetting('', date("Y-m-d H:i:s"), date("Y-m-d H:i:s"), $keyword, $kis_data['schoolYearID'], $kis_data['studentInfo']['classLevelID']));

					$kis_data['interviewFormResultAry'] = current($libkis_admission->getInterviewFormResultAry($kis_data['interviewFormSettingAry']['RecordID'], $kis_data['applicationID'], $kis_data['inputBy'], $kis_data['recordID']));
					// debug_pr($kis_data['interviewFormResultAry']);
					$kis_data['interviewQuestionAry'] = $libkis_admission->getInterviewFormBuilterAry($kis_data['interviewFormResultAry']['formid']);
					// debug_pr($kis_data['interviewQuestionAry']);
					$kis_data['interviewFormAnswerAry'] = $libkis_admission->getInterviewFormAnswerAry($kis_data['interviewFormResultAry']['formid'], $kis_data['applicationID'], $kis_data['inputBy']);
					// debug_pr($kis_data['interviewFormResultAry']);
					$NavArr[] = array (
						'interview/interviewforminput/',
						$kis_lang['interviewforminput']
					);
					$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
					$NavArr[] = array (
						'interview/interviewforminput/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						$kis_data['schoolYear']
					);
					if ($kis_data['selectInterviewRound']) {
						$NavArr[] = array (
							'interview/interviewforminput/' . $kis_data['selectInterviewRound'] . '/?selectSchoolYearID=' . $kis_data['schoolYearID'],
							sprintf($kis_lang['round'], $kis_data['selectInterviewRound'])
						);
					}
					if ($kis_data['selectInterviewDate']) {
						$NavArr[] = array (
							'interview/interviewforminput/' . $kis_data['selectInterviewRound'] . '/' . $kis_data['selectInterviewDate'] . '/?selectSchoolYearID=' . $kis_data['schoolYearID'],
							$kis_data['selectInterviewDate']
						);
					}
					$NavArr[] = array (
						'interview/interviewforminput_details/' . $kis_data['selectInterviewRound'] . '/' . ($kis_data['selectInterviewDate'] ? $kis_data['selectInterviewDate'] : 0) . '/' . $kis_data['recordID'] . '/',
						$kis_data['interviewListAry'][0]['Date'] . ' (' . substr($kis_data['interviewListAry'][0]['StartTime'], 0, -3) . ' ~ ' . substr($kis_data['interviewListAry'][0]['EndTime'], 0, -3) . ') ' . ($kis_data['interviewListAry'][0]['GroupName'] ? $kis_lang['sessiongroup'] . ' ' . $kis_data['interviewListAry'][0]['GroupName'] : '')
					);
					$NavArr[] = array (
						'',
						$kis_lang['view'] /*$kis_data['interviewFormResultAry']['name'].' ('.$kis_data['interviewFormResultAry']['datemodified'].')'*/

					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					$main_template = 'interview/interviewforminput_details_view';
					break;
				case 'interviewforminput_details_add' : // /interview/interviewforminput_details_add/
					if (isset ($_REQUEST['selectInterviewRound'])) {
						$kis_data['selectInterviewRound'] = ($_REQUEST['selectInterviewRound'] ? $_REQUEST['selectInterviewRound'] : '');
					} else {
						$kis_data['selectInterviewRound'] = ($q[2] ? $q[2] : '');
					}
					if (isset ($_REQUEST['selectInterviewDate'])) {
						$kis_data['selectInterviewDate'] = $_REQUEST['selectInterviewDate'];
					} else {
						$kis_data['selectInterviewDate'] = ($q[3] ? $q[3] : '');
					}
					if (isset ($_REQUEST['recordID'])) {
						$kis_data['recordID'] = $_REQUEST['recordID'];
					} else {
						$kis_data['recordID'] = $q[4];
					}
					if (isset ($_REQUEST['applicationID'])) {
						$kis_data['applicationID'] = $_REQUEST['applicationID'];
					} else {
						$kis_data['applicationID'] = $q[5];
					}

					$order = $order ? $order : '';
					$sortby = $sortby ? $sortby : '';

					$kis_data['interviewListAry'] = $libkis_admission->getInterviewFromInputListAry($kis_data['recordID'], '', '', '', $keyword, $order, $sortby, $kis_data['selectInterviewRound']);
					$applicationDetails = $kis_data['interviewListAry'];

					// -- add interview form here ...
					// debug_pr($kis_data['interviewListAry']);
					$kis_data['studentInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], '', '', '', $kis_data['applicationID']));
					// debug_pr($kis_data['studentInfo']);
					$kis_data['classLevelAry'] = $libkis_admission->classLevelAry;

					$kis_data['interviewFormSettingAry'] = current($libkis_admission->getInterviewFormSetting('', date("Y-m-d H:i:s"), date("Y-m-d H:i:s"), $keyword, $kis_data['schoolYearID'], $kis_data['studentInfo']['classLevelID']));
					$kis_data['interviewQuestionAry'] = $libkis_admission->getInterviewFormBuilterAry($kis_data['interviewFormSettingAry']['RecordID']);
					// debug_pr($kis_data['interviewQuestionAry']);
					$kis_data['interviewFormResultAry'] = $libkis_admission->getInterviewFormAnswerAry($kis_data['interviewFormSettingAry']['RecordID'], $kis_data['applicationID'], $UserID);
					// debug_pr($kis_data['interviewFormResultAry']);
					$NavArr[] = array (
						'interview/interviewforminput/',
						$kis_lang['interviewforminput']
					);
					$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
					$NavArr[] = array (
						'interview/interviewforminput/?selectSchoolYearID=' . $kis_data['schoolYearID'],
						$kis_data['schoolYear']
					);
					if ($kis_data['selectInterviewRound']) {
						$NavArr[] = array (
							'interview/interviewforminput/' . $kis_data['selectInterviewRound'] . '/?selectSchoolYearID=' . $kis_data['schoolYearID'],
							sprintf($kis_lang['round'], $kis_data['selectInterviewRound'])
						);
					}
					if ($kis_data['selectInterviewDate']) {
						$NavArr[] = array (
							'interview/interviewforminput/' . $kis_data['selectInterviewRound'] . '/' . $kis_data['selectInterviewDate'] . '/?selectSchoolYearID=' . $kis_data['schoolYearID'],
							$kis_data['selectInterviewDate']
						);
					}
					$NavArr[] = array (
						'interview/interviewforminput_details/' . $kis_data['selectInterviewRound'] . '/' . ($kis_data['selectInterviewDate'] ? $kis_data['selectInterviewDate'] : 0) . '/' . $kis_data['recordID'] . '/',
						$kis_data['interviewListAry'][0]['Date'] . ' (' . substr($kis_data['interviewListAry'][0]['StartTime'], 0, -3) . ' ~ ' . substr($kis_data['interviewListAry'][0]['EndTime'], 0, -3) . ') ' . ($kis_data['interviewListAry'][0]['GroupName'] ? $kis_lang['sessiongroup'] . ' ' . $kis_data['interviewListAry'][0]['GroupName'] : '')
					);
					$NavArr[] = array (
						'',
						$kis_lang['new']
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					$main_template = 'interview/interviewforminput_details_add';
					break;
				case 'timeslotsettings' : // interview/timeslotsettings/
					if (isset ($_REQUEST['selectInterviewRound'])) {
						$kis_data['selectInterviewRound'] = ($_REQUEST['selectInterviewRound'] ? $_REQUEST['selectInterviewRound'] : 1);
					} else {
						$kis_data['selectInterviewRound'] = ($q[2] ? $q[2] : 1);
					}
					if ($q[3] == 'edit') {
						if (is_numeric($q[4]) && $q[4] > 0) {
							$kis_data['recordID'] = $q[4];
							$kis_data['interviewArrangementRecords'] = $libkis_admission->getInterviewArrangementRecords(array (
								$q[4]
							), $kis_data['selectInterviewRound']);
						} else {
							$kis_data['interviewArrangementRecords'] = array ();
						}
						$NavArr[] = array (
							'interview/timeslotsettings/' . $kis_data['selectInterviewRound'] . '/',
							sprintf($kis_lang['round'], $kis_data['selectInterviewRound'])
						);
						$NavArr[] = array (
							'',
							$q[3] ? $kis_lang['edit'] . '&nbsp;' . $kis_data['interviewArrangementRecords'][0]['Title'] : $kis_lang['new'] . "&nbsp;" . $kis_lang['interviewarrangement']
						);

						$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
						$main_template = 'interview/timeslotsettings_edit';
					} else {
						$kis_data['interviewArrangementRecords'] = $libkis_admission->getInterviewArrangementRecords('', $kis_data['selectInterviewRound']);
						$main_template = 'interview/timeslotsettings';
					}
					break;
				case 'interviewusersettings' : // interview/interviewusersettings/
					$kis_data['interviewUserSettings'] = $libkis_admission->getInterviewUserSettings();
					$main_template = 'interview/interviewusersettings';
					break;
				case 'interviewusersettings_new' : // interview/interviewusersettings_new/
					$kis_data['groups'] = kis_utility :: getAcademicYearGroups();
					$kis_data['interviewUserSettings'] = $libkis_admission->getInterviewUserSettings();
					$NavArr[] = array (
						'',
						$kis_lang['new']
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
					$main_template = 'interview/interviewusersettings_new';
					break;
				case 'announcementsettings' : // interview/announcementsettings/
				    if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){
				        $kis_data['basicSettings'] = $libkis_admission->getBasicSettings($kis_data['schoolYearID'], array (
				            'firstInterviewAnnouncementStart',
				            'firstInterviewAnnouncementEnd',
				            'secondInterviewAnnouncementStart',
				            'secondInterviewAnnouncementEnd',
				            'thirdInterviewAnnouncementStart',
				            'thirdInterviewAnnouncementEnd',
				            'applicationStatusAnnouncementStart',
				            'applicationStatusAnnouncementEnd',
				            'adminModeActive'
				        ));
				    }else{
    					$kis_data['basicSettings'] = $libkis_admission->getBasicSettings($kis_data['schoolYearID'], array (
    						'firstInterviewAnnouncement',
    						'secondInterviewAnnouncement',
    						'thirdInterviewAnnouncement',
    						'applicationStatusAnnouncement',
    						'applicationStatusAnnouncementStartDate',
    						'enableInterviewAnnouncement'
    					));
				    }
					$kis_data['interviewArrangementRecords'] = $libkis_admission->getInterviewArrangementRecords('', $kis_data['selectInterviewRound']);
					$main_template = 'interview/announcementsettings';
					break;
				case 'announcementsettings_edit' : // interview/announcementsettings_edit/
				    if($sys_custom['KIS_Admission']['SFAEPS']['Settings']){
				        $kis_data['basicSettings'] = $libkis_admission->getBasicSettings($kis_data['schoolYearID'], array (
				            'firstInterviewAnnouncementStart',
				            'firstInterviewAnnouncementEnd',
				            'secondInterviewAnnouncementStart',
				            'secondInterviewAnnouncementEnd',
				            'thirdInterviewAnnouncementStart',
				            'thirdInterviewAnnouncementEnd',
				            'applicationStatusAnnouncementStart',
				            'applicationStatusAnnouncementEnd',
				            'adminModeActive',
				            'adminMessage'
				        ));
				    }else{
    					$kis_data['basicSettings'] = $libkis_admission->getBasicSettings($kis_data['schoolYearID'], array (
    						'firstInterviewAnnouncement',
    						'secondInterviewAnnouncement',
    						'thirdInterviewAnnouncement',
    						'applicationStatusAnnouncement',
    						'applicationStatusAnnouncementStartDate',
    						'enableInterviewAnnouncement'
    					));
				    }
					$NavArr[] = array (
						'',
						$kis_lang['edit']
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
					$main_template = 'interview/announcementsettings_edit';
					break;
				default : // interview/main/
					if (isset ($_REQUEST['selectInterviewRound'])) {
						$kis_data['selectInterviewRound'] = ($_REQUEST['selectInterviewRound'] ? $_REQUEST['selectInterviewRound'] : '1');
					} else {
						$kis_data['selectInterviewRound'] = ($q[1] ? $q[1] : '1');
					}
					$kis_data['classLevelAry'] = $libkis_admission->classLevelAry;
					// $kis_data['applicationSetting'] = $setting;
					// $classLevelID = isset($_REQUEST['selectClassLevelID'])?$_REQUEST['selectClassLevelID']:$classLevelID;
					// $kis_data['classLevelSelection'] = $libkis_admission->getClassLevelSelection($classLevelID,'selectClassLevelID',true,$kis_lang['all']);
					$kis_data['interviewSettingAry'] = $libkis_admission->getInterviewSettingAry('', '', '', '', $keyword, $kis_data['schoolYearID'], '', $kis_data['selectInterviewRound']);
					$kis_data['numberOfApplicant'] = $libkis_admission->getNumberOfApplicant($kis_data['schoolYearID'], $kis_data['selectInterviewRound']);
					$kis_data['numberOfApplicantAssignedTointerview'] = $libkis_admission->getNumberOfApplicantAssignedTointerview($kis_data['schoolYearID'], $kis_data['selectInterviewRound']);

					$settingIds = Get_Array_By_Key($kis_data['interviewSettingAry'], 'RecordID');
					$interviewListAry = array();
				    $rs = $libkis_admission->getInterviewListAry($settingIds, '', '', '', '', '', '', $kis_data['selectInterviewRound']);
				    foreach($rs as $r){
				        if(!in_array($r['other_record_id'], (array)$interviewListAry[$r['record_id']])){
				            $interviewListAry[$r['record_id']][] = $r['other_record_id'];
				        }
				    }
				    $kis_data['interviewListAry'] = $interviewListAry;

					$main_template = 'interview/main';
			}
			break;

		case 'reports' :
			switch ($q[1]) {
				case 'paymentreport' : // reports/paymentreport/
					$kis_data['start_date'] = $start_date;
					$kis_data['end_date'] = $end_date;
					$NavArr[] = array (
						'',
						$kis_lang['from'] . ' ' . $start_date . ' ' . $kis_lang['to'] . ' ' . $end_date
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					// generate the payment report
					$page = $page ? $page : 1;
					$amount = $amount ? $amount : 10;
					$order = $order ? $order : '';
					$sortby = $sortby ? $sortby : '';
					$keyword = $keyword ? $keyword : '';

					$data = array (
						'startDate' => $start_date,
						'endDate' => $end_date,
						'keyword' => $keyword,
						'page' => $page,
						'amount' => $amount,
						'order' => $order,
						'sortby' => $sortby
					);
					list ($total, $paymentReportDetails) = $libkis_admission->getPaymentReportDetails($data);
					$count = count($paymentReportDetails);
					$kis_data['paymentReportDetails'] = array ();
					$totalAmount = 0;
					$totalLoanAmount = 0;
					for ($i = 0; $i < $count; $i++) {
						$_applicationId = (is_numeric($paymentReportDetails[$i]['application_id']) ? $paymentReportDetails[$i]['application_id'] . ' ' : $paymentReportDetails[$i]['application_id']);

						$kis_data['paymentReportDetails'][$_applicationId]['date_input'] = $paymentReportDetails[$i]['date_input'];
						$kis_data['paymentReportDetails'][$_applicationId]['student_name'] = $paymentReportDetails[$i]['student_name'];
						$kis_data['paymentReportDetails'][$_applicationId]['year_name'] = $paymentReportDetails[$i]['year_name'];
						$kis_data['paymentReportDetails'][$_applicationId]['payment_amount'] = $paymentReportDetails[$i]['payment_amount'];
						$kis_data['paymentReportDetails'][$_applicationId]['txn_id'] = $paymentReportDetails[$i]['txn_id'];
						$kis_data['paymentReportDetails'][$_applicationId]['remarks'] = $paymentReportDetails[$i]['remarks'];
						$kis_data['paymentReportDetails'][$_applicationId]['parent_name'][] = $paymentReportDetails[$i]['parent_name'];
						$kis_data['paymentReportDetails'][$_applicationId]['parent_phone'][] = $paymentReportDetails[$i]['parent_phone'];
						$kis_data['paymentReportDetails'][$_applicationId]['apply_year'] = $paymentReportDetails[$i]['apply_year'];
						$kis_data['paymentReportDetails'][$_applicationId]['application_status'] = $paymentReportDetails[$i]['application_status'];
						$kis_data['paymentReportDetails'][$_applicationId]['payment_status'] = $paymentReportDetails[$i]['payment_status'];
					}
					foreach ($kis_data['paymentReportDetails'] as $key => $apaymentReportDetails) {
						if ($admission_cfg['Status'][$kis_data['paymentReportDetails'][$key]['application_status']] >= $admission_cfg['Status']['paymentsettled'])
							$totalAmount += $kis_data['paymentReportDetails'][$key]['payment_amount'];
						else {
							$kis_data['paymentReportDetails'][$key]['payment_amount'] = 0 - $kis_data['paymentReportDetails'][$key]['payment_amount'];
							$totalLoanAmount += $kis_data['paymentReportDetails'][$key]['payment_amount'];
						}
					}
					$kis_data['paymentReportDetails'] = array_slice($kis_data['paymentReportDetails'], (($page -1) * $amount));
					$kis_data['paymentReportDetails'] = array_slice($kis_data['paymentReportDetails'], 0, $amount);
					$kis_data['totalAmount'] = $totalAmount;
					$kis_data['totalLoanAmount'] = $totalLoanAmount;

					$main_template = 'reports/payment_report';
					break;
				default : // payment
					// if($sys_custom['KIS_Admission']['PayPal'] && $admission_cfg['PaymentStatus']){
					// $kis_data['paymentStatus'] = $libkis_admission->getPaymentStatusSelection();
					// }
					$kis_data['start_date'] = $start_date;
					$kis_data['end_date'] = $end_date;
					$main_template = 'reports/payment';
			}
			break;

		case 'settings' :
			switch ($q[1]) {
				case 'edit' : // settings/edit/
					$classLevelId = (in_array($q[2], $classLevelIdAry)) ? $q[2] : $classLevelIdAry[0];
					$kis_data['applicationSetting'] = $setting[$classLevelId];
					$kis_data['classLevelSelection'] = $libkis_admission->getClassLevelSelection($classLevelId);
					// NavigationBar
					$selectedClassLevel = $libkis_admission->classLevelAry[$classLevelId];
					$NavArr[] = array (
						'settings/',
						$selectedSchoolYear['academic_year_name_' . $intranet_session_language]
					);
					$NavArr[] = array (
						'',
						$kis_lang['edit'] . ' ' . $selectedClassLevel
					);
					$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

					$main_template = 'settings/applicationsettings_edit';
					break;
				case 'instructiontoapplicants' :
					$kis_data['basicSettings'] = $libkis_admission->getBasicSettings($kis_data['schoolYearID'], array (
						'generalInstruction'
					));
					if ($q[2] == 'edit') { // instructiontoapplicants/edit/
						$NavArr[] = array (
							'settings/instructiontoapplicants',
							$selectedSchoolYear['academic_year_name_' . $intranet_session_language]
						);
						$NavArr[] = array (
							'',
							$kis_lang['edit']
						);
						$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
						$main_template = 'settings/instruction_edit';
					} else {
						$main_template = 'settings/instructiontoapplicants';
					}

					break;
				case 'attachmentsettings' :
					if ($q[2] == 'edit') { // /attachmentsettings/edit/[RecordID]
						if (is_numeric($q[3]) && $q[3] > 0) {
							$kis_data['attachmentSettings'] = $libkis_admission->getAttachmentSettings($q[3]);
						} else {
							$kis_data['attachmentSettings'] = array ();
						}
						$NavArr[] = array (
							'',
							$q[3] ? $kis_lang['edit'] . ' ' . $kis_data['attachmentSettings'][0]['AttachmentName'] : $kis_lang['new']
						);

						$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
						$main_template = 'settings/attachmentsettings_edit';
					} else { // /attachmentsettings/
						$kis_data['maxUploadSize'] = $admission_cfg['maxUploadSize'];
						$kis_data['attachmentSettings'] = $libkis_admission->getAttachmentSettings();
						$main_template = 'settings/attachmentsettings';
					}
					break;
				case 'email_template' :
					if ($q[2] == 'edit') {
						if (is_numeric($q[3]) && $q[3] > 0) {
							$kis_data['emailTemplateRecords'] = $libkis_admission->getEmailTemplateRecords('', array (
								$q[3]
							));
						} else {
							$kis_data['emailTemplateRecords'] = array ();
						}
						$NavArr[] = array (
							'',
							$q[3] ? $kis_lang['edit'] . '&nbsp;' . $kis_data['emailTemplateRecords'][0]['Title'] : $kis_lang['new'] . "&nbsp;" . $kis_lang['email_template']
						);

						$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
						$main_template = 'settings/email_template_edit';
					} else {
						$kis_data['emailTemplateRecords'] = $libkis_admission->getEmailTemplateRecords($_REQUEST['RecordType']);
						$main_template = 'settings/email_template';
					}
					break;
				case 'interviewarrangement' :
					if ($q[2] == 'edit') {
						if (is_numeric($q[3]) && $q[3] > 0) {
							$kis_data['recordID'] = $q[3];
							$kis_data['interviewArrangementRecords'] = $libkis_admission->getInterviewArrangementRecords(array (
								$q[3]
							));
						} else {
							$kis_data['interviewArrangementRecords'] = array ();
						}
						$NavArr[] = array (
							'',
							$q[3] ? $kis_lang['edit'] . '&nbsp;' . $kis_data['interviewArrangementRecords'][0]['Title'] : $kis_lang['new'] . "&nbsp;" . $kis_lang['interviewarrangement']
						);

						$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
						$main_template = 'settings/interviewarrangement_edit';
					} else {
						$kis_data['interviewArrangementRecords'] = $libkis_admission->getInterviewArrangementRecords();
						$main_template = 'settings/interviewarrangement';
					}
					break;
					case 'printformheader' :
					$kis_data['basicSettings'] = $libkis_admission->getBasicSettings(99999, array (
						'schoolnamechi',
						'schoolnameeng',
						'schooladditionalinfo',
						'printformlabeltype',
						'applicationnoformatstartfrom',
						'remarksnamechi',
						'remarksnameeng'
					));
					$kis_data['yearStart'] = substr(date('Y', getStartOfAcademicYear('', $libkis_admission->schoolYearID)), -2);
					
					if ($q[2] == 'edit') {

						$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
						$main_template = 'settings/printformheader_edit';
					} else {
						$main_template = 'settings/printformheader';
					}
					break;
				case 'onlinepayment' :
				
					$protocol = (checkHttpsWebProtocol())? 'https':'http';
					$kis_data['basicSettings'] = $libkis_admission->getBasicSettings(99999, array (
						'onlinepaymentamount',
						'enablepaypal',
						'paypalsignature',
						'paypalhostedbuttonid',
						'paypalname',
						'enablealipayhk'
					));
					$kis_data['payPalButton'] = '<form action="'.$admission_cfg['paypal_url'].'" method="post" target="_blank">
						<input type="hidden" name="cmd" value="_s-xclick">
						<input type="hidden" name="hosted_button_id" value="'.$kis_data['basicSettings']['paypalhostedbuttonid'].'">
						<input type="hidden" name="return" value="'.$protocol.'://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_finish2.php" /> 
						    <input type="hidden" name="cancel_return" value="'.$protocol.'://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_finish2.php?cm=test" />
						    <input type="hidden" name="custom" value="0" />
							<input type="hidden" name="notify_url" value="'.$protocol.'://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_ipn.php" /> 
						<input type="image" src="https://www.sandbox.paypal.com/zh_HK/HK/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal － 更安全、更簡單的網上付款方式！">
						<img alt="" border="0" src="https://www.sandbox.paypal.com/zh_HK/i/scr/pixel.gif" width="1" height="1">
						</form>';
					
					if ($q[2] == 'edit') {

						$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
						$main_template = 'settings/onlinepayment_edit';
					} else {
						$main_template = 'settings/onlinepayment';
					}
					break;
				case 'modulesettings' :
				
					$protocol = (checkHttpsWebProtocol())? 'https':'http';
					$kis_data['basicSettings'] = $libkis_admission->getBasicSettings(99999, array (
						'enableinterviewarrangement',
						'enableinterviewform',
						'enablebriefingsession'
					));

					if ($q[2] == 'edit') {

						$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
						$main_template = 'settings/modulesettings_edit';
					} else {
						$main_template = 'settings/modulesettings';
					}
					break;
				default : // applicationsettings
					$kis_data['classLevelAry'] = $libkis_admission->classLevelAry;
					$kis_data['applicationSetting'] = $setting;
					$main_template = 'settings/applicationsettings';
			}
			break;

		default : // applicantslist
			if (!empty ($q[1])) {
				switch ($q[1]) {
					case 'attachment_download' : // applicantslist/attachment_download/
						if (isset ($_REQUEST['classLevelID'])) {
							$kis_data['classLevelID'] = $_REQUEST['classLevelID'];
						}
						$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
						$NavArr[] = array (
							'applicantslist/',
							$kis_data['schoolYear']
						);
						$NavArr[] = array (
							'applicantslist/listbyform/' . $kis_data['classLevelID'] . '/',
							$libkis_admission->classLevelAry[$kis_data['classLevelID']]
						);
						$NavArr[] = array (
							'',
							$kis_lang['downloadapplicationattachment']
						);
						$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

						$attachmentSettingAry = $libkis_admission->getAttachmentSettings();
						$kis_data['AttachmentTypeArray'][] = array (
							'personal_photo',
							$kis_lang['Admission']['personalPhoto']
						);
						foreach ($attachmentSettingAry as $aAttachmentSettiing) {
							$kis_data['AttachmentTypeArray'][] = array (
								$aAttachmentSettiing['AttachmentName'],
								$aAttachmentSettiing['AttachmentName']
							);
						}
						if ($sys_custom['KIS_Admission']['PayPal'] && $admission_cfg['PaymentStatus']) {
							$kis_data['paymentStatus'] = $libkis_admission->getPaymentStatusSelection();
						}
						$main_template = 'applicantslist/attachment_download';
						break;
					case 'import' : // applicantslist/import/

						// developing...
						$kis_data['csvFile'] = "<a class='tablelink' href='" . $PATH_WRT_ROOT . "kis/admission_form/export_form.php?selectStatus=" . $_REQUEST['selectStatus'] . "&classLevelID=" . $_REQUEST['classLevelID'] . "&schoolYearID=" . $_REQUEST['schoolYearID'] . "'>" . $kis_lang['ClickHereToDownloadSample'] . "</a>";
						if($sys_custom['KIS_Admission']['UCCKE']['Settings']){
    						$kis_data['csvFile'] = "<a class='tablelink' href='" . $PATH_WRT_ROOT . "kis/apps/admission/templates/applicantslist/uccke.eclasscloud.hk/admission_form_edb_sample.csv'>" . $kis_lang['ClickHereToDownloadSample'] . "</a>";
						}
						if($sys_custom['KIS_Admission']['HKUGAC']['Settings'] || $_REQUEST['type'] == 'status'){
                            $kis_data['csvFile'] = "<a class='tablelink' href='" . $PATH_WRT_ROOT . "kis/admission_form/export_form.php?type=status&classLevelID=" . $_REQUEST['classLevelID'] . "&schoolYearID=" . $_REQUEST['schoolYearID'] . "'>" . $kis_lang['ClickHereToDownloadSample'] . "</a>";
						}
						//
						if (isset ($_REQUEST['classLevelID'])) {
							$kis_data['classLevelID'] = $_REQUEST['classLevelID'];
						}
						if (isset ($_REQUEST['selectStatus'])) {
							$kis_data['selectStatus'] = $_REQUEST['selectStatus'];
						}
						if (isset ($_REQUEST['selectSchoolYearID'])) {
							$kis_data['selectSchoolYearID'] = $_REQUEST['selectSchoolYearID'];
						}
						if (isset ($_REQUEST['type'])) {
							$kis_data['type'] = $_REQUEST['type'];
						}
						// if(isset($_REQUEST['recordID']))
						// {
						// $kis_data['recordID'] = $_REQUEST['recordID'];
						// $kis_data['emailRecordDetail'] = $libkis_admission->getEmailRecordDetail($kis_data['recordID']);
						// $kis_data['applicationIds'] = implode(",",$kis_data['emailRecordDetail']['applicationIds']);
						// $kis_data['emailReceivers'] = $libkis_admission->getEmailReceivers($kis_data['applicationIds'],$kis_data['recordID']);
						// $kis_data['classLevelID'] = $kis_data['emailReceivers'][0]['classLevelID'];
						// }else{
						// $kis_data['emailReceivers'] = $libkis_admission->getEmailReceivers($kis_data['applicationIds'],$kis_data['recordID']);
						// }

						// [Start] something should modifying...
						// $kis_data['applicationInfo'] = current($libkis_admission->getApplicationStatus($kis_data['schoolYearID'],$kis_data['classLevelID'],$applicationID='',''));
						// $kis_data['applicationID'] = $kis_data['applicationInfo']['applicationID'];
						// $kis_data['classLevelID'] = $kis_data['applicationInfo']['classLevelID'];
						// [End] something should modifying...

						$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
						$NavArr[] = array (
							'applicantslist/',
							$kis_data['schoolYear']
						);
						$NavArr[] = array (
							'applicantslist/listbyform/' . $kis_data['classLevelID'] . '/',
							$libkis_admission->classLevelAry[$kis_data['classLevelID']]
						);

						if($sys_custom['KIS_Admission']['HKUGAC']['Settings'] || $kis_data['type'] == 'status') {
                            $NavArr[] = array(
                                '',
                                $kis_lang['Admission']['HKUGAC']['ImportStatus']
                            );
                        }else {
                            $NavArr[] = array(
                                '',
                                $kis_lang['importupdateadmissionform']
                            );
                        }

						$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

						// if($kis_data['recordID']!=''){
						// $kis_data['emailRecordDetail'] = $libkis_admission->getEmailRecordDetail($kis_data['recordID']);
						// $kis_data['applicationIds'] = implode(",",$kis_data['emailRecordDetail']['applicationIds']);
						// }else{
						// $kis_data['applicationIds'] = $_REQUEST['applicationIds'];
						// }

						if($sys_custom['KIS_Admission']['STANDARD']['Settings']){
                	    $kis_data['ImportFieldArr'] = $libkis_admission->getImportRemarks();
						}
						$main_template = 'applicantslist/import';
						break;
					case 'importinterview' : // importinterview/import/

						// developing...
						$kis_data['csvFile'] = "<a class='tablelink' href='" . $PATH_WRT_ROOT . "kis/admission_form/export_for_import_interview.php?selectStatus=" . $_REQUEST['selectStatus'] . "&classLevelID=" . $_REQUEST['classLevelID'] . "&schoolYearID=" . $_REQUEST['schoolYearID'] . "'>" . $kis_lang['ClickHereToDownloadSample'] . "</a>";

						if (isset ($_REQUEST['classLevelID'])) {
							$kis_data['classLevelID'] = $_REQUEST['classLevelID'];
						}
						if (isset ($_REQUEST['selectStatus'])) {
							$kis_data['selectStatus'] = $_REQUEST['selectStatus'];
						}
						if (isset ($_REQUEST['selectSchoolYearID'])) {
							$kis_data['selectSchoolYearID'] = $_REQUEST['selectSchoolYearID'];
						}

						// if(isset($_REQUEST['recordID']))
						// {
						// $kis_data['recordID'] = $_REQUEST['recordID'];
						// $kis_data['emailRecordDetail'] = $libkis_admission->getEmailRecordDetail($kis_data['recordID']);
						// $kis_data['applicationIds'] = implode(",",$kis_data['emailRecordDetail']['applicationIds']);
						// $kis_data['emailReceivers'] = $libkis_admission->getEmailReceivers($kis_data['applicationIds'],$kis_data['recordID']);
						// $kis_data['classLevelID'] = $kis_data['emailReceivers'][0]['classLevelID'];
						// }else{
						// $kis_data['emailReceivers'] = $libkis_admission->getEmailReceivers($kis_data['applicationIds'],$kis_data['recordID']);
						// }

						// [Start] something should modifying...
						// $kis_data['applicationInfo'] = current($libkis_admission->getApplicationStatus($kis_data['schoolYearID'],$kis_data['classLevelID'],$applicationID='',''));
						// $kis_data['applicationID'] = $kis_data['applicationInfo']['applicationID'];
						// $kis_data['classLevelID'] = $kis_data['applicationInfo']['classLevelID'];
						// [End] something should modifying...

						$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
						$NavArr[] = array (
							'applicantslist/',
							$kis_data['schoolYear']
						);
						$NavArr[] = array (
							'applicantslist/listbyform/' . $kis_data['classLevelID'] . '/',
							$libkis_admission->classLevelAry[$kis_data['classLevelID']]
						);
						$NavArr[] = array (
							'',
							$kis_lang['importinterviewinfo']
						);
						$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

						// if($kis_data['recordID']!=''){
						// $kis_data['emailRecordDetail'] = $libkis_admission->getEmailRecordDetail($kis_data['recordID']);
						// $kis_data['applicationIds'] = implode(",",$kis_data['emailRecordDetail']['applicationIds']);
						// }else{
						// $kis_data['applicationIds'] = $_REQUEST['applicationIds'];
						// }

						$main_template = 'importinterview/import';
						break;
					case 'importbriefing' : // importinterview/import/

						// developing...
						$kis_data['csvFile'] = "<a class='tablelink' href='" . $PATH_WRT_ROOT . "kis/admission_form/export_for_import_briefing.php?selectStatus=" . $_REQUEST['selectStatus'] . "&classLevelID=" . $_REQUEST['classLevelID'] . "&schoolYearID=" . $_REQUEST['schoolYearID'] . "'>" . $kis_lang['ClickHereToDownloadSample'] . "</a>";

						if (isset ($_REQUEST['classLevelID'])) {
							$kis_data['classLevelID'] = $_REQUEST['classLevelID'];
						}
						if (isset ($_REQUEST['selectStatus'])) {
							$kis_data['selectStatus'] = $_REQUEST['selectStatus'];
						}
						if (isset ($_REQUEST['selectSchoolYearID'])) {
							$kis_data['selectSchoolYearID'] = $_REQUEST['selectSchoolYearID'];
						}

						$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
						$NavArr[] = array (
							'applicantslist/',
							$kis_data['schoolYear']
						);
						$NavArr[] = array (
							'applicantslist/listbyform/' . $kis_data['classLevelID'] . '/',
							$libkis_admission->classLevelAry[$kis_data['classLevelID']]
						);
						$NavArr[] = array (
							'',
							$kis_lang['importibriefing']
						);
						$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

						$main_template = 'importbriefing/import';
						break;

					case 'compose' : // applicantslist/compose/
						if (isset ($_REQUEST['fromModule'])) {
							$kis_data['fromModule'] = $_REQUEST['fromModule'];
						}
						if (isset ($_REQUEST['classLevelID'])) {
							$kis_data['classLevelID'] = $_REQUEST['classLevelID'];
						}
						if (isset ($_REQUEST['applicationIds'])) {
							$kis_data['applicationIds'] = $_REQUEST['applicationIds'];
						}
						if (isset ($_REQUEST['selectStatus'])) {
							$data = array (
								'classLevelID' => $kis_data['classLevelID'],
								'status' => $_REQUEST['selectStatus']
							);
							list ($total, $tempApplicationDetails) = $libkis_admission->getApplicationDetails($kis_data['schoolYearID'], $data);
							$applicationDetailsArr = array ();
							foreach ($tempApplicationDetails as $anApplicationDetails) {
								if (in_array($anApplicationDetails['record_id'], $applicationDetailsArr))
									continue;
								$kis_data['applicationIds'] .= $anApplicationDetails['record_id'] . ',';
								$applicationDetailsArr[] = $anApplicationDetails['record_id'];
							}
							$kis_data['applicationIds'] = substr($kis_data['applicationIds'], 0, -1);
						}
						if (isset ($_REQUEST['recordID'])) {
							$kis_data['recordID'] = $_REQUEST['recordID'];
							$kis_data['emailRecordDetail'] = $libkis_admission->getEmailRecordDetail($kis_data['recordID']);
							$kis_data['applicationIds'] = implode(",", $kis_data['emailRecordDetail']['applicationIds']);
							$kis_data['emailReceivers'] = $libkis_admission->getEmailReceivers($kis_data['applicationIds'], $kis_data['recordID']);
							$kis_data['classLevelID'] = $kis_data['emailReceivers'][0]['classLevelID'];
						} else {
							$kis_data['emailReceivers'] = $libkis_admission->getEmailReceivers($kis_data['applicationIds'], $kis_data['recordID']);
						}

						// $kis_data['applicationInfo'] = current($libkis_admission->getApplicationStatus($kis_data['schoolYearID'],$kis_data['classLevelID'],$applicationID='',''));
						// $kis_data['applicationID'] = $kis_data['applicationInfo']['applicationID'];
						// $kis_data['classLevelID'] = $kis_data['applicationInfo']['classLevelID'];

						$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
						$NavArr[] = array (
							'applicantslist/',
							$kis_data['schoolYear']
						);
						if($kis_data['classLevelID']){
    						$NavArr[] = array (
    							'applicantslist/listbyform/' . $kis_data['classLevelID'] . '/',
    							$libkis_admission->classLevelAry[$kis_data['classLevelID']]
    						);
						}
						$NavArr[] = array (
							'',
							$kis_lang['sendemail']
						);
						$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);

						// if($kis_data['recordID']!=''){
						// $kis_data['emailRecordDetail'] = $libkis_admission->getEmailRecordDetail($kis_data['recordID']);
						// $kis_data['applicationIds'] = implode(",",$kis_data['emailRecordDetail']['applicationIds']);
						// }else{
						// $kis_data['applicationIds'] = $_REQUEST['applicationIds'];
						// }

						$main_template = 'emaillist/compose';
						break;
					case 'details' : // $q[2] => schoolYearID ,$q[3] => recordID, $q[4] => studentinfo/parentinfo/payment status
						$kis_data['recordID'] = $q[3];
						$kis_data['display'] = $q[4] ? $q[4] : 'remarks';
						switch ($kis_data['display']) {
							case 'parentinfo' :
							case 'icmssectionc' : // icms cust
								$recordAry = $libkis_admission->getApplicationParentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
								$recordCount = count($recordAry);
								$kis_data['applicationInfo'] = array ();
								for ($i = 0; $i < $recordCount; $i++) {
									$_type = $recordAry[$i]['type'];
									$kis_data['applicationInfo']['applicationID'] = $recordAry[$i]['applicationID'];
									$kis_data['applicationInfo']['classLevelID'] = $kis_data['applicationInfo']['classLevelID'] ? $kis_data['applicationInfo']['classLevelID'] : $recordAry[$i]['classLevelID'];
									$kis_data['applicationInfo'][$_type] = $recordAry[$i];
								}
								if ($sys_custom['KIS_Admission']['CSM']['Settings'])
									$kis_data['studentApplicationInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $status = '', $kis_data['recordID']));
								break;
							case 'otherinfo' :
							case 'icmssectiona' : // icms cust
								$kis_data['classLevelAry'] = $libkis_admission->classLevelAry;
								$kis_data['applicationInfo'] = current($libkis_admission->getApplicationOthersInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']));
								if ($sys_custom['KIS_Admission']['MGF']['Settings'])
									$kis_data['studentApplicationInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $status = '', $kis_data['recordID']));
								if ($sys_custom['KIS_Admission']['MUNSANG']['Settings']) {
									$kis_data['studentApplicationInfoCust'] = $libkis_admission->getApplicationStudentInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
									$kis_data['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
								}
								if ($sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']) {
									$kis_data['studentApplicationInfoCust'] = $libkis_admission->getApplicationStudentInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
									$kis_data['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
									$kis_data['applicationStuInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $status = '', $kis_data['recordID']));
								}
								if ($sys_custom['KIS_Admission']['RMKG']['Settings']) {
									$kis_data['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
								}
								if ($sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings']) {
									$kis_data['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
								}
								if ($sys_custom['KIS_Admission']['YLSYK']['Settings']) {
									// $kis_data['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
									$kis_data['studentApplicationRelativesInfoCustRef'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'REF');
									$kis_data['studentApplicationRelativesInfoCustEx'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'EX');
									$kis_data['studentApplicationRelativesInfoCustCur'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'CUR');
									$kis_data['studentApplicationRelativesInfoCustApply'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'APPLY');
									$kis_data['libkis_admission'] = $libkis_admission;
								}
								if ($sys_custom['KIS_Admission']['MINGWAIPE']['Settings']) {
									// $kis_data['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
									$kis_data['studentApplicationRelativesInfoCustEx'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'EX');
									$kis_data['studentApplicationRelativesInfoCustCur'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'CUR');
								}
								if ($sys_custom['KIS_Admission']['MINGWAI']['Settings']) {
									// $kis_data['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
									$kis_data['studentApplicationRelativesInfoCustEx'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'EX');
									$kis_data['studentApplicationRelativesInfoCustCur'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'CUR');
								}
								break;
							case 'studentinfo' :
							case 'icmsattachment' : // icms cust
								$kis_data['applicationInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $status = '', $kis_data['recordID']));
								$kis_data['attachmentSettings'] = $libkis_admission->getAttachmentSettings();
								$kis_data['attachmentList'] = $libkis_admission->getAttachmentByApplicationID($kis_data['schoolYearID'], $kis_data['applicationInfo']['applicationID']);
								if ($sys_custom['KIS_Admission']['CSM']['Settings']) {
									$kis_data['otherApplicationInfo'] = current($libkis_admission->getApplicationOthersInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']));
									$kis_data['TwinListArray'] = $libkis_admission->getTwinListArray($kis_data['applicationInfo']['applicationID']);
								}
								if ($sys_custom['KIS_Admission']['MGF']['Settings']) {
									foreach ($admission_cfg['BirthCertType'] as $_key => $_type) {
										if ($kis_data['applicationInfo']['birthcerttype'] == $_type) {
											$kis_data['birth_cert_type_selection'] = $kis_lang['Admission']['BirthCertType'][$_key];
											break;
										}
									}
								}
								break;
							case 'icmssectionb' : // icms cust
								$kis_data['applicationInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $status = '', $kis_data['recordID']));
								$kis_data['otherApplicationInfo'] = current($libkis_admission->getApplicationOthersInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']));
								$kis_data['attachmentList'] = $libkis_admission->getAttachmentByApplicationID($kis_data['schoolYearID'], $kis_data['applicationInfo']['applicationID']);
								break;
							case 'icmssectiondef' : // icms cust
								$kis_data['classLevelAry'] = $libkis_admission->classLevelAry;
								$kis_data['applicationInfo'] = current($libkis_admission->getApplicationOthersInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']));
								$kis_data['studentApplicationInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $status = '', $kis_data['recordID']));
								break;
							default :
								$kis_data['applicationInfo'] = current($libkis_admission->getApplicationStatus($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']));
								$kis_data['timeSelection'] = $kis_data['libinterface']->Get_Time_Selection('interview');
								$kis_data['attachmentList'] = $libkis_admission->getAttachmentByApplicationID($kis_data['schoolYearID'], $kis_data['applicationInfo']['applicationID']);
								if ($sys_custom['KIS_Admission']['InterviewSettings'] || $kis_data['moduleSettings']['enableinterviewarrangement']) {
									$kis_data['otherApplicationInfo'] = current($libkis_admission->getApplicationOthersInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']));
									$kis_data['interviewInfo'] = current($libkis_admission->getInterviewListAry($kis_data['otherApplicationInfo']['InterviewSettingID'], $date = '', $startTime = '', $endTime = '', $keyword = '', $order, $sortby, 1));
									$kis_data['interviewInfo2'] = current($libkis_admission->getInterviewListAry($kis_data['otherApplicationInfo']['InterviewSettingID2'], $date = '', $startTime = '', $endTime = '', $keyword = '', $order, $sortby, 2));
									$kis_data['interviewInfo3'] = current($libkis_admission->getInterviewListAry($kis_data['otherApplicationInfo']['InterviewSettingID3'], $date = '', $startTime = '', $endTime = '', $keyword = '', $order, $sortby, 3));

									// $kis_data['interviewSettingSelection'] = $libkis_admission->getInterviewSettingSelection();
								}
								if ($sys_custom['KIS_Admission']['PayPal']) {
									$kis_data['paypalPaymentInfo'] = $libkis_admission->getPaymentResult('', '', '', $kis_data['schoolYearID'], $kis_data['applicationInfo']['applicationID']);
								}
								if ($sys_custom['KIS_Admission']['STANDARD']['Settings']) {
									$kis_data['applicationCustInfo'] = $libkis_admission->getAllApplicationCustInfo($kis_data['applicationInfo']['applicationID']);
									if($sys_custom['ePayment']['Alipay']){
										$kis_data['alipayPaymentInfo'] = $libkis_admission->getAlipayPaymentResult('', '', '', $kis_data['schoolYearID'], $kis_data['applicationInfo']['applicationID']);
									}
								}
								else if ($sys_custom['KIS_Admission']['Alipay']) {
									$kis_data['alipayPaymentInfo'] = $libkis_admission->getAlipayPaymentResult('', '', '', $kis_data['schoolYearID'], $kis_data['applicationInfo']['applicationID']);
								}
						}

						if ($kis_data['applicationInfo']) {
							$kis_data['applicationID'] = $kis_data['applicationInfo']['applicationID'];
							$kis_data['classLevelID'] = $kis_data['applicationInfo']['classLevelID'];
							$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
							$NavArr[] = array (
								'applicantslist/',
								$kis_data['schoolYear']
							);
							$NavArr[] = array (
								'applicantslist/listbyform/' . $kis_data['classLevelID'] . '/',
								$libkis_admission->classLevelAry[$kis_data['classLevelID']]
							);

							if($sys_custom['KIS_Admission']['CREATIVE']['Settings'] || $sys_custom['KIS_Admission']['STANDARD']['Settings']){
                                $studentInfo = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $status = '', $kis_data['recordID']));
							    $NavArr[] = array(
                                    '',
                                    " {$kis_data['applicationID']} ({$studentInfo['student_name']})"
                                );
                            }else {
                                $NavArr[] = array(
                                    '',
                                    ' ' . $kis_data['applicationID']
                                );
                            }
							$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
							$kis_data['isValidRecord'] = true;
						} else {
							$kis_data['isValidRecord'] = false;
						}
						$main_template = 'applicantslist/details';
						break;
					case 'edit' : // $q[2] => schoolYearID ,$q[3] => recordID, $q[4] => studentinfo/parentinfo/payment status
						$kis_data['recordID'] = $q[3];
						$kis_data['display'] = $q[4] ? $q[4] : 'remarks';
						$kis_data['pgType'] = $libkis_admission->pg_type;
						switch ($q[4]) {
							case 'icmssectiona' : // icms cust
								$kis_data['classLevelAry'] = $libkis_admission->classLevelAry;
								$kis_data['applicationInfo'] = current($libkis_admission->getApplicationOthersInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']));
								$timeSlotAry = explode(',', $setting[$kis_data['applicationInfo']['classLevelID']]['DayType']);
								foreach ($timeSlotAry as $_key) {
									$kis_data['applyTimeSlotAry'][] = array (
										$_key,
										$kis_lang['Admission']['icms'][$kis_data['classLevelAry'][$kis_data['applicationInfo']['classLevelID']]]['TimeSlot'][$_key]
									);
								}
								break;
							case 'icmssectionb' : // icms cust
								$kis_data['applicationInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $status = '', $kis_data['recordID']));
								$kis_data['otherApplicationInfo'] = current($libkis_admission->getApplicationOthersInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']));
								$kis_data['attachmentList'] = $libkis_admission->getAttachmentByApplicationID($kis_data['schoolYearID'], $kis_data['applicationInfo']['applicationID']);
								$kis_data['attachmentSettings'] = $libkis_admission->getAttachmentSettings();
								break;
							case 'parentinfo' :
							case 'icmssectionc' : // icms cust
								$recordAry = $libkis_admission->getApplicationParentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
								$recordCount = count($recordAry);
								$kis_data['applicationInfo'] = array ();
								for ($i = 0; $i < $recordCount; $i++) {
									$_type = $recordAry[$i]['type'];
									$kis_data['applicationInfo']['applicationID'] = $recordAry[$i]['applicationID'];
									$kis_data['applicationInfo']['classLevelID'] = $kis_data['applicationInfo']['classLevelID'] ? $kis_data['applicationInfo']['classLevelID'] : $recordAry[$i]['classLevelID'];
									$kis_data['applicationInfo'][$_type] = $recordAry[$i];
								}
								if ($sys_custom['KIS_Admission']['CSM']['Settings'])
									$kis_data['studentApplicationInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $status = '', $kis_data['recordID']));
								break;
							case 'icmssectiondef' : // icms cust
								$kis_data['applicationInfo'] = current($libkis_admission->getApplicationOthersInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']));
								$kis_data['studentApplicationInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $status = '', $kis_data['recordID']));

								break;
							case 'otherinfo' :
								$kis_data['applyyearselection'] = $libkis_admission->getAcademicYearSelection($kis_data['schoolYearID'], 'OthersApplyYear');
								$kis_data['KnowUsByAry'] = $admission_cfg['KnowUsBy'];
								$kis_data['applicationInfo'] = current($libkis_admission->getApplicationOthersInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']));
								if ($sys_custom['KIS_Admission']['MGF']['Settings'])
									$kis_data['studentApplicationInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $status = '', $kis_data['recordID']));
								if ($sys_custom['KIS_Admission']['MUNSANG']['Settings']) {
									$kis_data['studentApplicationInfoCust'] = $libkis_admission->getApplicationStudentInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
									$kis_data['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
								}
								if ($sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']) {
									$kis_data['studentApplicationInfoCust'] = $libkis_admission->getApplicationStudentInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
									$kis_data['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
									$kis_data['applicationStuInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $status = '', $kis_data['recordID']));
								}
								if ($sys_custom['KIS_Admission']['RMKG']['Settings']) {
									$kis_data['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
								}
								if ($sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings']) {
									$kis_data['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
								}
								if ($sys_custom['KIS_Admission']['YLSYK']['Settings']) {
									// $kis_data['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
									$kis_data['studentApplicationRelativesInfoCustRef'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'REF');
									$kis_data['studentApplicationRelativesInfoCustEx'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'EX');
									$kis_data['studentApplicationRelativesInfoCustCur'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'CUR');
									$kis_data['studentApplicationRelativesInfoCustApply'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'APPLY');
									$kis_data['libkis_admission'] = $libkis_admission;
								}
								if ($sys_custom['KIS_Admission']['MINGWAIPE']['Settings']) {
									// $kis_data['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
									$kis_data['studentApplicationRelativesInfoCustEx'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'EX');
									$kis_data['studentApplicationRelativesInfoCustCur'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'CUR');
								}
								if ($sys_custom['KIS_Admission']['MINGWAI']['Settings']) {
									// $kis_data['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
									$kis_data['studentApplicationRelativesInfoCustEx'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'EX');
									$kis_data['studentApplicationRelativesInfoCustCur'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID'], 'CUR');
								}
								$timeSlotAry = explode(',', $setting[$kis_data['applicationInfo']['classLevelID']]['DayType']);
								foreach ($timeSlotAry as $_key) {
									$kis_data['applyTimeSlotAry'][] = array (
										$_key,
										$kis_lang['Admission']['TimeSlot'][$_key]
									);
								}
								if ($sys_custom['KIS_Admission']['TBCPK']['Settings']) {
									$termAry = explode(',', $setting[$kis_data['applicationInfo']['classLevelID']]['TermType']);
									foreach ($termAry as $_key) {
										$kis_data['applyTermAry'][$_key] = $kis_lang['Admission']['Term'][$_key];
									}
								}
								break;
							case 'remarks' :
								$kis_data['groups'] = kis_utility :: getAcademicYearGroups(array (
									'hide_basic_groups' => true
								));
								$kis_data['applicationInfo'] = current($libkis_admission->getApplicationStatus($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']));
								$kis_data['statusSelection'] = $libkis_admission->getStatusSelection($admission_cfg['Status'][$kis_data['applicationInfo']['status']], 'status', $auto_submit = false, $isAll = false);
								$kis_data['attachmentList'] = $libkis_admission->getAttachmentByApplicationID($kis_data['schoolYearID'], $kis_data['applicationInfo']['applicationID']);
								if ($sys_custom['KIS_Admission']['InterviewSettings'] || $kis_data['moduleSettings']['enableinterviewarrangement']) {
									$kis_data['otherApplicationInfo'] = current($libkis_admission->getApplicationOthersInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']));
									// $kis_data['interviewInfo'] = current($libkis_admission->getInterviewListAry($kis_data['otherApplicationInfo']['InterviewSettingID'], $date='', $startTime='', $endTime='', $keyword=''));
									$kis_data['interviewSettingSelection'] = $libkis_admission->getInterviewSettingSelection($kis_data['otherApplicationInfo']['InterviewSettingID'], 'InterviewSettingID', true, '', $kis_data['schoolYearID'], $kis_data['applicationInfo']['classLevelID'], 1);
									$kis_data['interviewSettingSelection2'] = $libkis_admission->getInterviewSettingSelection($kis_data['otherApplicationInfo']['InterviewSettingID2'], 'InterviewSettingID2', true, '', $kis_data['schoolYearID'], $kis_data['applicationInfo']['classLevelID'], 2);
									$kis_data['interviewSettingSelection3'] = $libkis_admission->getInterviewSettingSelection($kis_data['otherApplicationInfo']['InterviewSettingID3'], 'InterviewSettingID3', true, '', $kis_data['schoolYearID'], $kis_data['applicationInfo']['classLevelID'], 3);
								}
								if ($sys_custom['KIS_Admission']['PayPal']) {
									$kis_data['paypalPaymentInfo'] = $libkis_admission->getPaymentResult('', '', '', $kis_data['schoolYearID'], $kis_data['applicationInfo']['applicationID']);
								}
								if ($sys_custom['KIS_Admission']['STANDARD']['Settings']) {
									$kis_data['applicationCustInfo'] = $libkis_admission->getAllApplicationCustInfo($kis_data['applicationInfo']['applicationID']);
									if($sys_custom['ePayment']['Alipay']){
										$kis_data['alipayPaymentInfo'] = $libkis_admission->getAlipayPaymentResult('', '', '', $kis_data['schoolYearID'], $kis_data['applicationInfo']['applicationID']);
									}
								}
								else if ($sys_custom['KIS_Admission']['Alipay']) {
									$kis_data['alipayPaymentInfo'] = $libkis_admission->getAlipayPaymentResult('', '', '', $kis_data['schoolYearID'], $kis_data['applicationInfo']['applicationID']);
								}
								break;
							default :
								$kis_data['applicationInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $status = '', $kis_data['recordID']));
								$kis_data['attachmentList'] = $libkis_admission->getAttachmentByApplicationID($kis_data['schoolYearID'], $kis_data['applicationInfo']['applicationID']);
								$kis_data['attachmentSettings'] = $libkis_admission->getAttachmentSettings();
								if ($sys_custom['KIS_Admission']['CSM']['Settings']) {
									$kis_data['otherApplicationInfo'] = current($libkis_admission->getApplicationOthersInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']));
									$timeSlotAry = explode(',', $setting[$kis_data['otherApplicationInfo']['classLevelID']]['DayType']);
									foreach ($timeSlotAry as $_key) {
										$kis_data['applyTimeSlotAry'][] = array (
											$_key,
											$kis_lang['Admission']['csm']['TimeSlot'][$_key]
										);
									}
								}
								if ($sys_custom['KIS_Admission']['MGF']['Settings']) {
									$kis_data['birth_cert_type_selection'] = '<select name="birthcerttype" id="birthcerttype">';
									foreach ($admission_cfg['BirthCertType'] as $_key => $_type) {
										$kis_data['birth_cert_type_selection'] .= '<option value="' . $_type . '"' . ($kis_data['applicationInfo']['birthcerttype'] == $_type ? ' selected="selected"' : '') . '>';
										$kis_data['birth_cert_type_selection'] .= $kis_lang['Admission']['BirthCertType'][$_key];
										$kis_data['birth_cert_type_selection'] .= '</option>';
									}
									$kis_data['birth_cert_type_selection'] .= '</select>';
								}
						}
						if ($kis_data['applicationInfo']) {
							$kis_data['applicationID'] = $kis_data['applicationInfo']['applicationID'];
							$kis_data['classLevelID'] = $kis_data['applicationInfo']['classLevelID'];
							$kis_data['classLevel'] = $libkis_admission->classLevelAry[$kis_data['classLevelID']];
							$kis_data['schoolYear'] = $selectedSchoolYear['academic_year_name_' . $intranet_session_language];
							$NavArr[] = array (
								'applicantslist/',
								$kis_data['schoolYear']
							);
							$NavArr[] = array (
								'applicantslist/listbyform/' . $kis_data['classLevelID'] . '/',
								$kis_data['classLevel']
							);
							$NavArr[] = array (
								'applicantslist/details/' . $kis_data['schoolYearID'] . '/' . $kis_data['recordID'] . '/' . $kis_data['display'],
								$kis_data['applicationID']
							);
							$NavArr[] = array (
								'',
								$kis_lang['edit'] . " " . $kis_lang[$kis_data['display']]
							);
							$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
							$kis_data['isValidRecord'] = true;
						} else {
							$kis_data['isValidRecord'] = false;
						}
						$main_template = 'applicantslist/details_edit';
						break;
					case 'cust':
						$main_template = "applicantslist/{$kis_admission_school}/{$q[2]}";
						break;
					default : // listbyform $q[2] = classLevelID / search, applicantslist/listbyform
						/* Parameter */

						$arrCookiesTemp[] = array (
							"admission_current_class_level",
							"currentClassLevel"
						);
						updateGetCookies($arrCookiesTemp);

						$shouldClear = false;
						if ($currentClassLevel && $currentClassLevel != $q[2] || $q[2] == 'search') {
							$shouldClear = true;
						}

						$currentClassLevel = $q[2];
						$arrCookies2[] = array (
							"admission_page",
							"page"
						);
						$arrCookies2[] = array (
							"admission_amount",
							"amount"
						);
						$arrCookies2[] = array (
							"admission_total",
							"total"
						);
						$arrCookies2[] = array (
							"admission_sortby",
							"sortby"
						);
						$arrCookies2[] = array (
							"admission_order",
							"order"
						);
						$arrCookies2[] = array (
							"admission_current_class_level",
							"currentClassLevel"
						);

						if ($shouldClear) {
							clearCookies($arrCookies2);
						} else {
							updateGetCookies($arrCookies2);
						}

						$page = $page ? $page : 1;
						$amount = $amount ? $amount : 10;
						$order = $order ? $order : '';
						$sortby = $sortby ? $sortby : '';
						$keyword = $keyword ? $keyword : '';
						if ($q[2] == 'search') {
							$NavArr[] = array (
								'applicantslist/',
								$selectedSchoolYear['academic_year_name_' . $intranet_session_language]
							);
							$NavArr[] = array (
								'',
								$kis_lang['search_result']
							);
							$kis_data['classChangeLevelSelection'] = $libkis_admission->getClassLevelSelection('', 'selectChangeClassLevelID');
						} else {
							$kis_data['classLevelID'] = $classLevelId = (in_array($q[2], $classLevelIdAry)) ? $q[2] : $classLevelIdAry[0];
							$status = in_array($selectStatus, $admission_cfg['Status']) ? $selectStatus : '';
							$kis_data['interviewStatusSelection'] = ($status != '' && ($status == $admission_cfg['Status']['waitingforinterview'] || $status == $admission_cfg['Status']['TSUENWANBCKG_waitingforinterview'] || $status == $admission_cfg['Status']['CHIUCHUNKG_waitingforinterview'])) ? $libkis_admission->getInterviewStatusSelection('selectInterviewStatus', $selectInterviewStatus) : '';
							$kis_data['classLevelSelection'] = $libkis_admission->getClassLevelSelection($classLevelId);
							$kis_data['classChangeLevelSelection'] = $libkis_admission->getClassLevelSelection($classLevelId, 'selectChangeClassLevelID');
							$kis_data['applicationStatus'] = $libkis_admission->getStatusSelection($status);
							if ($sys_custom['KIS_Admission']['PayPal'] && $admission_cfg['PaymentStatus']) {
								$kis_data['paymentStatus'] = $libkis_admission->getPaymentStatusSelection($paymentStatus);
							}
							if ($sys_custom['KIS_Admission']['KTLMSKG']['Settings'] || $sys_custom['KIS_Admission']['KTLSKS']['Settings']) {
								$kis_data['custSelection'] = $libkis_admission->getKtlmsCustSelection($custSelection);
							}
							if ($sys_custom['KIS_Admission']['YLSYK']['Settings'] || $sys_custom['KIS_Admission']['SSGC']['Settings'] || $sys_custom['KIS_Admission']['JOYFUL']['Settings'] || $sys_custom['KIS_Admission']['STANDARD']['Settings']) {
								$kis_data['custSelection'] = $libkis_admission->getCustSelection($custSelection);
							}

							$selectedClassLevel = $libkis_admission->classLevelAry[$kis_data['classLevelID']];
							$NavArr[] = array (
								'applicantslist/',
								$selectedSchoolYear['academic_year_name_' . $intranet_session_language]
							);
							$NavArr[] = array (
								'',
								$selectedClassLevel
							);
						}
						$data = array (
							'classLevelID' => $classLevelId,
							'applicationID' => $applicationID,
							'status' => $status,
							'interviewStatus' => $selectInterviewStatus,
							'paymentStatus' => $paymentStatus,
							'custSelection' => $custSelection,
							'applicantInterviewStatus' => $applicantInterviewStatus,
							'admitStatus' => $admitStatus,
							'keyword' => $keyword,
							'page' => $page,
							'amount' => $amount,
							'order' => $order,
							'sortby' => $sortby
						);
						$kis_data['classLevelId'] = $classLevelId;
						$kis_data['status'] = $status;
						list ($total, $applicationDetails) = $libkis_admission->getApplicationDetails($kis_data['schoolYearID'], $data);
						$count = count($applicationDetails);
						$kis_data['applicationDetails'] = array ();
						for ($i = 0; $i < $count; $i++) {
							$_applicationId = (is_numeric($applicationDetails[$i]['application_id']) ? $applicationDetails[$i]['application_id'] . ' ' : $applicationDetails[$i]['application_id']);

							$kis_data['applicationDetails'][$_applicationId]['student_name'] = $applicationDetails[$i]['student_name'];
							if ($sys_custom['KIS_Admission']['ICMS']['Settings']) {
								$kis_data['applicationDetails'][$_applicationId]['ApplyDayType1'] = $applicationDetails[$i]['ApplyDayType1'];
								$kis_data['applicationDetails'][$_applicationId]['ApplyDayType2'] = $applicationDetails[$i]['ApplyDayType2'];
								$kis_data['applicationDetails'][$_applicationId]['ApplyDayType3'] = $applicationDetails[$i]['ApplyDayType3'];
							}
							if ($sys_custom['KIS_Admission']['UCCKE']['Settings'] || $sys_custom['KIS_Admission']['STCC']['Settings']) {
								$kis_data['applicationDetails'][$_applicationId]['student_hkid'] = $applicationDetails[$i]['student_hkid'];
								$custInfo = $libkis_admission->getAllApplicationCustInfo($_applicationId);
								$kis_data['applicationDetails'][$_applicationId]['student_strn'] = $custInfo['BD_Ref_Num'][0]['Value'];
							}
							if ($sys_custom['KIS_Admission']['HKUGAPS']['Settings']) {
								$kis_data['applicationDetails'][$_applicationId]['langspokenathome'] = $applicationDetails[$i]['langspokenathome'];
							}
							if ($sys_custom['KIS_Admission']['CREATIVE']['Settings']) {
								$kis_data['applicationDetails'][$_applicationId]['DateInput'] = $applicationDetails[$i]['DateInput'];
								$kis_data['applicationDetails'][$_applicationId]['Cust_RatingClass'] = $applicationDetails[$i]['Cust_RatingClass'];
								$kis_data['applicationDetails'][$_applicationId]['Cust_InterviewStatus'] = $applicationDetails[$i]['Cust_InterviewStatus'];
								$kis_data['applicationDetails'][$_applicationId]['Cust_AdmitStatus'] = $applicationDetails[$i]['Cust_AdmitStatus'];
							}
							$kis_data['applicationDetails'][$_applicationId]['parent_name'][] = $applicationDetails[$i]['parent_name'];
							$kis_data['applicationDetails'][$_applicationId]['parent_phone'][] = $applicationDetails[$i]['parent_phone'];
							$kis_data['applicationDetails'][$_applicationId]['application_status'] = $applicationDetails[$i]['application_status'];
							$kis_data['applicationDetails'][$_applicationId]['record_id'] = $applicationDetails[$i]['record_id'];

							if ($sys_custom['KIS_Admission']['SSGC']['Settings']) {
								$kis_data['applicationCustDetails'][$_applicationId] = $libkis_admission->getAllApplicationCustInfo($_applicationId);
							}
						}

						$kis_data['applicationDetails'] = array_slice($kis_data['applicationDetails'], (($page -1) * $amount));
						$kis_data['applicationDetails'] = array_slice($kis_data['applicationDetails'], 0, $amount);

						$kis_data['statusSelection'] = $libkis_admission->getStatusSelection($selectStatus, 'status', $auto_submit = false, $isAll = false);
						$kis_data['NavigationBar'] = $libkis_admission->getNavigationBar($NavArr);
						$kis_data['applicationSetting'] = $setting;
						if ($sys_custom['KIS_Admission']['ICMS']['Settings'])
							$kis_data['applicationlistAry'] = $libkis_admission->getApplicationListAry($kis_data['schoolYearID']);

                        $main_template = 'applicantslist/formlist';
				}
			} else {
				$keyword = $keyword ? $keyword : '';
				$kis_data['StatusAry'] = $admission_cfg['Status'];
				$kis_data['applicationlistAry'] = $libkis_admission->getApplicationListAry($kis_data['schoolYearID']);
				$main_template = 'applicantslist/main';
			}

			break;
	}
} else
	if ($kis_user['type'] == kis :: $user_types['parent']) {
	}
$kis_data['PageBar'] = array (
	$page,
	$amount,
	$total,
	$sortby,
	$order,
	$keyword
);
?>

<?if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission-interview-marker"]) :?>

<div class="content_board_menu">

    <?

$menuArr = array ();
$menuArr[] = 'applicantslist';
$menuArr[] = '';
if ($sys_custom['KIS_Admission']['InterviewSettings'] || $kis_data['moduleSettings']['enableinterviewarrangement']) {
	$menuArr[] = 'interview';
}
kis_ui :: loadLeftMenu($menuArr, $q[0], '#/apps/admission/');
?>

    <div class="main_content">
    <? kis_ui::loadTemplate($main_template, $kis_data);?>
    </div>
	<p class="spacer"></p>

</div>

<?elseif ($kis_user['type']==kis::$user_types['teacher']) : ?>

<div class="content_board_menu">

    <?

$menuArr = array ();
$menuArr[] = 'applicantslist';
$menuArr[] = '';
$menuArr[] = 'updatestatus';
$menuArr[] = '';
$menuArr[] = 'emaillist';
$menuArr[] = '';
if ($sys_custom['KIS_Admission']['BriefingModule'] || $kis_data['moduleSettings']['enablebriefingsession']) {
	$menuArr[] = 'briefing';
	$menuArr[] = '';
}
if ($sys_custom['KIS_Admission']['InterviewSettings'] || $kis_data['moduleSettings']['enableinterviewarrangement']) {
	$menuArr[] = 'interview';
	$menuArr[] = '';
}
if ($sys_custom['KIS_Admission']['PayPal'] && $sys_custom['KIS_Admission']['RMKG']['Settings']) {
	$menuArr[] = 'reports';
	$menuArr[] = '';
}
$menuArr[] = 'settings';

if ($sys_custom['KIS_Admission']['InterviewSettings'] || $kis_data['moduleSettings']['enableinterviewarrangement']) {
	kis_ui :: loadLeftMenu($menuArr, $q[0], '#/apps/admission/');
} else {
	kis_ui :: loadLeftMenu($menuArr, $q[0], '#/apps/admission/');
}
?>

    <div class="main_content">
    <? kis_ui::loadTemplate($main_template, $kis_data);?>
    </div>
	<p class="spacer"></p>

</div>

<? elseif ($kis_user['type']==kis::$user_types['parent']): ?>

<div class="main_content"></div>
<p class="spacer"></p>
<? endif; ?>
