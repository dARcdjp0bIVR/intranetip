<?
// Editing by 
/*
 * 2020-11-02 (Ray): Added body temperature
 * 2019-09-23 (Ray): Added present absent reason at student take attendance page
 * 2019-08-22 (Ray): Added reason, remark select box
 * 2019-02-20 (Carlos): Added flag $sys_custom['KIS_ClassGroupAttendanceType_IgnoreAtTakeAttendance'] to disable the checking of $sys_custom['KIS_ClassGroupAttendanceType'] at KIS take attendance page. [2019-0220-1139-38170]
 * 2017-08-22 (Carlos): Assign some settings into $kis_data for default query action.
 * 2016-01-04 (Carlos): $sys_custom['KIS_ClassGroupAttendanceType'] - display classes for AM/PM session according to class group settings. 
 * 2014-02-27 (Carlos): Use getClassConfirmRecord() to get the last confirm time and person at student take attendance page
 * 2014-02-14 (Carlos): Restrict teachers can only take own classes if corresponding setting is on and non-admin
 * 2013-10-16 (Carlos): Modified parent/student view, templates [monthlyrecord_parent] and [summary]
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT.'kis/init.php');

if(isset($intranet_session_language)){
	include_once($intranet_root."/lang/lang.$intranet_session_language.php");
}

if ($kis_user['type']==kis::$user_types['teacher']){
    
    switch ($q[0]){

	case 'monthlyrecord':

	    $libkis_attendance = $libkis->loadApp('eattendance');

	    //$kis_data['classes'] = kis_utility::getAcademicYearClasses();
		$kis_data['classes'] =  $libkis_attendance->getAllClassesDayAttendanceSummary();
	    $kis_data['years'] = $libkis_attendance->getAttendanceRecordedYears();
	    $kis_data['current_academic_year_id'] = $_SESSION['CurrentSchoolYearID'];
	    
	    $kis_data['main_template'] = 'monthlyrecord_form';
	break;
	  
	case 'reminderrecord':
	    $libkis_attendance = $libkis->loadApp('eattendance');
	    
	    if ($q[1]=='create'){
		
		$kis_data['today_date'] = date('Y-m-d');
		$kis_data['classes'] = kis_utility::getAcademicYearClasses();
		$kis_data['teachers'] = kis_utility::getUsers(array('user_type'=>kis::$user_types['teacher']));
		
		$kis_data['main_template'] = 'reminderrecord_create';
	    
	    }else if ($q[1]=='edit' && $kis_data['reminder']=$libkis_attendance->getReminderRecord($reminder_id)){
		
		$kis_data['teachers'] = kis_utility::getUsers(array('user_type'=>kis::$user_types['teacher']));
		
		$kis_data['main_template'] = 'reminderrecord_edit';
		
	    }else{
		
		$kis_data['page']   = $page?   $page: 1;
		$kis_data['amount'] = $amount? $amount: 20;
		$kis_data['offset'] = $amount*($page-1);
		    
		list($kis_data['total'], $kis_data['reminders']) = $libkis_attendance->getReminderRecords(
		    array('status'=>$status,'keyword'=>$search), $sortby, $order, $kis_data['amount'], $kis_data['page']
		);
	      
		$kis_data['main_template'] = 'reminderrecord';
	    
	    }

	break;

	default:
		
	    $target_ts = $date? strtotime($date): time();
	    $target_date = getdate($target_ts);
	    $kis_data['date'] = date('Y-m-d', $target_ts);
	    
	    $params = array('year'=>$target_date['year'], 'month'=>$target_date['mon'], 'day'=>$target_date['mday'], 'class_id'=>$class_id);
	
	    $libkis_attendance = $libkis->loadApp('eattendance', $params);
	    $lcardstudentattend = $libkis_attendance->getLibcardstudentattendance();
	    $kis_data['CannotTakeFutureDateRecord'] = $lcardstudentattend->CannotTakeFutureDateRecord? 1:0;
	    $kis_data['DisableISmartCardPastDate'] = $lcardstudentattend->DisableISmartCardPastDate ? 1:0;
	    $kis_data['EditDaysBeforeRecord'] = $lcardstudentattend->EditDaysBeforeRecord? 1:0;
	    $kis_data['EditDaysBeforeRecordSelect'] = intval($lcardstudentattend->EditDaysBeforeRecordSelect);

	    $attendance_mode = $libkis_attendance->getAttendanceMode();
	    $kis_data['attendance_mode'] = $attendance_mode;
	    $kis_data['attendance_mode_data'] = array();
	    if($attendance_mode != '1'){
	    	$kis_data['attendance_mode_data'][] = array('am', $Lang['StudentAttendance']['AM']);
	    }
	    if($attendance_mode != '0'){
	    	$kis_data['attendance_mode_data'][] = array('pm', $Lang['StudentAttendance']['PM']);
	    }
	    
	    $kis_data['apm'] = $time? $time: ($attendance_mode!='1'?'am':'pm');
	    
	    if ($class_id){
				
			$kis_data['students'] = $libkis_attendance->getClassDayAttendanceRecords($search, $sortby, $order);
			$class_confirm_records = $libkis_attendance->getClassConfirmRecord($class_id, $target_date['year'], $target_date['mon'], $target_date['mday'], $kis_data['apm']=='am'?'2':'3');
			$kis_data['class_confirm_record'] = $class_confirm_records[$class_id];
			$kis_data['classes'] = kis_utility::getAcademicYearClasses();
			if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]){
				$classes_can_take = $libkis_attendance->getAvailableClassesByDate($kis_data['date']);
				$filter_classes = array();
				for($i=0;$i<count($kis_data['classes']);$i++){
					if(in_array($kis_data['classes'][$i]['class_id'], $classes_can_take)){
						$filter_classes[] = $kis_data['classes'][$i];
					}
				}
				$kis_data['classes'] = $filter_classes;
			}
			
			// control the am/pm selection
			if(!$sys_custom['KIS_ClassGroupAttendanceType_IgnoreAtTakeAttendance'] && $sys_custom['KIS_ClassGroupAttendanceType']){
				$classIdToClassGroupAry = $libkis_attendance->getClassGroupSettings();
				$attendance_type = $classIdToClassGroupAry[$class_id]['AttendanceType'];
				if($attendance_type == 1){ // AM only
					$kis_data['attendance_mode_data'] = array(array('am', $Lang['StudentAttendance']['AM']));
				}else if($attendance_type == 2){ // PM only
					$kis_data['attendance_mode_data'] = array(array('pm', $Lang['StudentAttendance']['PM']));
				}
			}

			include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
			include_once($PATH_WRT_ROOT."includes/libinterface.php");
			include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
			include_once($intranet_root.'/includes/json.php');
			include_once($intranet_root.'/lang/lang.'.$intranet_session_language.'.ip20.php');
			$jsonObj = new JSON_obj();
			$linterface = new interface_html();
			$lc = new libcardstudentattend2();
			$lword = new libwordtemplates();
			$words = $lword->getWordListAttendance();
			$hasWord = sizeof($words)!=0;
			$reason_js = '';
			$teacher_remark_words_temp = array();
			if($hasWord) {
				$words_absence = $lword->getWordListAttendance(1);
				$words_late = $lword->getWordListAttendance(2);
				$words_early = $lword->getWordListAttendance(3);
				$words_outing = $lc->Get_Outing_Reason();
				foreach($words_absence as $key=>$word) {
					$words_absence[$key] = htmlspecialchars($word);
				}
				foreach($words_late as $key=>$word) {
					$words_late[$key] = htmlspecialchars($word);
				}
				foreach($words_early as $key=>$word) {
					$words_early[$key] = htmlspecialchars($word);
				}
				foreach($words_outing as $key=>$word) {
					$words_outing[$key] = htmlspecialchars($word);
				}
				$words_lateearly = array_unique(array_merge($words_late, $words_early));
				$reason_js .= $linterface->CONVERT_TO_JS_ARRAY($words_absence, "jArrayWordsAbsence", 1);
				$reason_js .= $linterface->CONVERT_TO_JS_ARRAY($words_late, "jArrayWordsLate", 1);
				$reason_js .= $linterface->CONVERT_TO_JS_ARRAY($words_early, "jArrayWordsEarly", 1);
				$reason_js .= $linterface->CONVERT_TO_JS_ARRAY($words_lateearly, "jArrayWordsLateEarly", 1);
				$reason_js .= $linterface->CONVERT_TO_JS_ARRAY($words_outing, "jArrayWordsOuting", 1);
			}

			$teacher_remark_words = $lc->GetTeacherRemarkPresetRecords(array());
			$teacher_remark_words = Get_Array_By_Key($teacher_remark_words, 'ReasonText');
			foreach($teacher_remark_words as $key=>$word) {
				$teacher_remark_words_temp[$key] = htmlspecialchars($word);
			}

			$kis_data['reason_js'] = $reason_js;
			$kis_data['teacher_remark_words'] = $teacher_remark_words_temp;

			if($kis_data['apm'] == 'am') {
				$DayType = PROFILE_DAY_TYPE_AM;
				$display_period = $i_DayTypeAM;
			} else {
				$DayType = PROFILE_DAY_TYPE_PM;
				$display_period = $i_DayTypePM;
			}
			$kis_data['display_period'] = $display_period;

			$PresetAbsentStudentAssoAry = array();
			$PresetAbsentStudentAry = array();

			if(count($kis_data['students']) > 0) {
				$AbsentStudentAry = Get_Array_By_Key($kis_data['students'], 'user_id');
				$sql = "SELECT 
						StudentID,
						DayPeriod,
						Reason,
						DocumentStatus,
						Remark
					FROM 
						CARD_STUDENT_PRESET_LEAVE 
					WHERE 
						StudentID IN (" . implode(',', IntegerSafe($AbsentStudentAry)) . ") AND
						DayPeriod = '" . $lc->Get_Safe_Sql_Query($DayType) . "' AND
						( '" . $lc->Get_Safe_Sql_Query($kis_data['date']) . "' = RecordDate )";
				$PresetAbsentStudentRecordAry = $lc->returnResultSet($sql);
				$PresetAbsentStudentAssoAry = BuildMultiKeyAssoc($PresetAbsentStudentRecordAry, 'StudentID');
				$PresetAbsentStudentAry = Get_Array_By_Key($PresetAbsentStudentRecordAry, 'StudentID');

				if($sys_custom['StudentAttendance']['RecordBodyTemperature']){
					$bodyTemperatureRecords = $lc->getBodyTemperatureRecords(array('RecordDate'=>$kis_data['date'],'StudentID'=>$AbsentStudentAry,'StudentIDToRecord'=>1));
					$kis_data['bodyTemperatureRecords'] = $bodyTemperatureRecords;
				}
			}

			$kis_data['PresetAbsentStudentAssoAry'] = $PresetAbsentStudentAssoAry;
			$kis_data['PresetAbsentStudentAry'] = $PresetAbsentStudentAry;

			$kis_data['main_template'] = 'takeattendance_students';
	    }else{
		
			$kis_data['classes'] = $libkis_attendance->getAllClassesDayAttendanceSummary($search, $kis_data['apm']);
			// control the displayed classes for am/pm
			if(!$sys_custom['KIS_ClassGroupAttendanceType_IgnoreAtTakeAttendance'] && $sys_custom['KIS_ClassGroupAttendanceType']){
				$time = !isset($time) ? ($attendance_mode!='1'?'am':'pm') : $time;
				$daytype_map = array('am'=>1,'pm'=>2,'wd'=>3);
				$classIdToClassGroupAry = $libkis_attendance->getClassGroupSettings();
				//debug_pr($class_groups);
				if(count($kis_data['classes'])>0){
					$tmp_classses = array();
					for($i=0;$i<count($kis_data['classes']);$i++){
						if(isset($classIdToClassGroupAry[$kis_data['classes'][$i]['class_id']])){
							$attendance_type = $classIdToClassGroupAry[$kis_data['classes'][$i]['class_id']]['AttendanceType'];
							if($attendance_type == '' || $attendance_type==3 || ($daytype_map[$time]==$attendance_type))
							{
								$tmp_classses[] = $kis_data['classes'][$i];
							}
						}else{
							$tmp_classses[] = $kis_data['classes'][$i];
						}
					}
					$kis_data['classes'] = $tmp_classses;
				}
			}
			
			$kis_data['classes_confirm_record'] = $libkis_attendance->getClassConfirmRecord(Get_Array_BY_Key($kis_data['classes'],'class_id'), $target_date['year'], $target_date['mon'],  $target_date['mday'],  $kis_data['apm']=='am'?'2':'3') ;
			$kis_data['main_template'] = 'takeattendance_classes';
	    }
	    
	break;
	
    }
    
}else{
    
    $kis_data['academic_year_id'] = isset($_REQUEST['academic_year_id']) && $_REQUEST['academic_year_id']!=''? $_REQUEST['academic_year_id'] : Get_Current_Academic_Year_ID();
    $kis_data['current_date']  = getdate();
    $kis_data['year']  = $q[0]? $q[0]: $kis_data['current_date']['year'];
    $kis_data['month'] = $q[1]? $q[1]: $kis_data['current_date']['mon'];
    
    $params = array('year'=>$kis_data['year'], 'month'=>$kis_data['month'], 'academic_year_id'=>$kis_data['academic_year_id']);
    $libkis_attendance = $libkis->loadApp('eattendance', $params);
   
    
    if ($q[1]){
	
	$kis_data['attend_days'] = $libkis_attendance->getStudentMonthAttendances();
	$kis_data['main_template'] = 'monthlyrecord_parent';
    }else{
	
	//$kis_data['summary_months'] = $libkis_attendance->getStudentYearAttendanceSummary();
	$kis_data['summary_months'] = $libkis_attendance->getStudentAcademicYearAttendanceSummary();
	$kis_data['main_template'] = 'summary';
    }
    
	
}

kis_ui::loadTemplate('main', $kis_data, $format);
?>