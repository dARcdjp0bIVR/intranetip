<?php
// Editing by 
/*
 * 2013-10-16 (Carlos): Modified year selection to academic year selection
 */
 
?>
<script type="text/javascript" language="JavaScript">
function academicYearSelectionChanged(selectedAcademicYearId)
{
	//$('#module_page .board_main_content').append($('.board_loading_overlay').clone().show());
	window.location.href="#/apps/eattendance/?academic_year_id="+selectedAcademicYearId;
}
</script>
<div id="table_filter">
	<?php
		$academic_year_id = isset($_REQUEST['academic_year_id']) && $_REQUEST['academic_year_id']!=''? $_REQUEST['academic_year_id'] : Get_Current_Academic_Year_ID();
		
		$ts_year_start = getStartOfAcademicYear('',$academic_year_id);
		$ts_year_end = getEndOfAcademicYear('',$academic_year_id);
		
		$academic_year_selection = getSelectAcademicYear("academic_year_id", 'onchange="academicYearSelectionChanged(this.value);"', 1, 0, $academic_year_id, 0, 1);
		echo $academic_year_selection;
	?>
</div>
<?php
if($sys_custom['KIS_eAttendance']['ParentHideDetail']){
?>
<p class="spacer"></p>
<div class="table_board">
  <table class="common_table_list edit_table_list">
	<colgroup><col nowrap="nowrap">
	</colgroup><thead>
		<tr>
		  <th><?=$kis_lang['month']?></th>
		  <th><?=$kis_lang['present_new']?></th>
		  <th><?=$kis_lang['absent']?></th>
	  </tr>
	</thead>
	<tbody>
	    <? for ($ts_cur_date=$ts_year_start,$i=date("n",$ts_cur_date),$year=date("Y",$ts_cur_date);
	    		$i=date("n",$ts_cur_date),$year=date("Y",$ts_cur_date),$ts_cur_date < $ts_year_end;
	    		$ts_cur_date=strtotime("+1 month",$ts_cur_date)): ?>
	    <tr>
                <td><a href="#/apps/eattendance/<?=$year?>/<?=$i?>/?academic_year_id=<?=$academic_year_id?>">
		     <?=$year?> <?=$kis_lang['month_'.$i]?>
		</a></td>
		<td class="row_present"><div align="center"><?=($summary_months[$year][$i]['present']+$summary_months[$year][$i]['late'])?></div></td>
		<td class="row_absent"><div align="center"><?=$summary_months[$year][$i]['absent']?></div></td>
	    </tr>
	    <?
		$total['present'] += $summary_months[$year][$i]['present'];
		$total['late'] += $summary_months[$year][$i]['late'];
		$total['present_new'] = $total['present']+$total['late'];
		$total['absent'] += $summary_months[$year][$i]['absent'];
	    ?>
	    <? endfor; ?>
	    <tr>
		<td style="background:#cccccc">&nbsp;</td>
		<td><div align="center"><strong><?=$total['present_new']?></strong></div></td>
		<td><div align="center"><strong style="color:red;"><?=$total['absent']?></strong></div></td>
	</tr>
	</tbody>
    </table>
    <p class="spacer"></p>
    <p class="spacer"></p><br>

</div>
<?php } 
else{ ?>                   
<p class="spacer"></p>
<div class="table_board">
  <table class="common_table_list edit_table_list">
	<colgroup><col nowrap="nowrap">
	</colgroup><thead>
		<tr>
		  <th><?=$kis_lang['month']?></th>
		  <th><?=$kis_lang['present']?></th>
		  <th><?=$kis_lang['late']?></th>
		  <th><?=$kis_lang['earlyleave']?></th>
		  <th><?=$kis_lang['absent']?></th>
		  <th><?=$kis_lang['outing']?></th>
	  </tr>
	</thead>
	<tbody>
	    <? for ($ts_cur_date=$ts_year_start,$i=date("n",$ts_cur_date),$year=date("Y",$ts_cur_date);
	    		$i=date("n",$ts_cur_date),$year=date("Y",$ts_cur_date),$ts_cur_date < $ts_year_end;
	    		$ts_cur_date=strtotime("+1 month",$ts_cur_date)): ?>
	    <tr>
                <td><a href="#/apps/eattendance/<?=$year?>/<?=$i?>/?academic_year_id=<?=$academic_year_id?>">
		     <?=$year?> <?=$kis_lang['month_'.$i]?>
		</a></td>
		<td class="row_present"><div align="center"><?=$summary_months[$year][$i]['present']?></div></td>
		<td class="row_present_sub"><div align="center"><?=$summary_months[$year][$i]['late']?></div></td>
		<td class="row_present_earlyleave"><div align="center"><?=$summary_months[$year][$i]['early_leave']?></div></td>
		<td class="row_absent"><div align="center"><?=$summary_months[$year][$i]['absent']?></div></td>
		<td class="row_outing"><div align="center"><?=$summary_months[$year][$i]['outing']?></div></td>
	    </tr>
	    <?
		$total['present'] += $summary_months[$year][$i]['present'];
		$total['late'] += $summary_months[$year][$i]['late'];
		$total['early_leave'] += $summary_months[$year][$i]['early_leave'];
		$total['absent'] += $summary_months[$year][$i]['absent'];
		$total['outing'] += $summary_months[$year][$i]['outing'];
	    ?>
	    <? endfor; ?>
	    <tr>
		<td style="background:#cccccc">&nbsp;</td>
		<td><div align="center"><strong><?=$total['present']?></strong></div></td>
		<td><div align="center"><strong><?=$total['late']?></strong></div></td>
		<td><div align="center"><strong><?=$total['early_leave']?></strong></div></td>
		<td><div align="center"><strong style="color:red;"><?=$total['absent']?></strong></div></td>
		<td><div align="center"><strong><?=$total['outing']?></strong></div></td>
	</tr>
	</tbody>
    </table>
    <p class="spacer"></p>
    <p class="spacer"></p><br>

</div>
                    
 <?php }?>                   