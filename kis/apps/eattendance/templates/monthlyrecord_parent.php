<?php
// Editing by 
/*
 * 2014-10-06 (Carlos): display reason 
 * 2014-02-07 (Carlos): $sys_custom['KIS_eAttendance']['ParentHideDetail'] - Modified logic to diplay In/Out status
 * 2013-10-16 (Carlos): Modified year selection and month selection to academic year selection and year-month selection
 */
?>
<script>
kis.eattendance.monthlyrecord_parent_init({monthdetails: '<?=$kis_lang['monthdetails']?>'});

function academicYearSelectionChanged(selectedAcademicYearId)
{
	window.location.href="#/apps/eattendance/?academic_year_id="+selectedAcademicYearId;
}
</script>
<div id="table_filter">

	<?php
		
		$academic_year_id = isset($kis_data['academic_year_id']) && $kis_data['academic_year_id']!=''? $kis_data['academic_year_id'] : Get_Current_Academic_Year_ID();
		
		$ts_year_start = getStartOfAcademicYear('',$academic_year_id);
		$ts_year_end = getEndOfAcademicYear('',$academic_year_id);
		
		$academic_year_selection = getSelectAcademicYear("academic_year_id", 'onchange="academicYearSelectionChanged(this.value);"', 1, 0, $academic_year_id, 0, 1);
		echo $academic_year_selection;
		
		$year_month_selection = '<select class="month">';
		for($ts_cur_date=$ts_year_start,$y=date("Y",$ts_cur_date),$m=date("n",$ts_cur_date);
			$y=date("Y",$ts_cur_date),$m=date("n",$ts_cur_date),$ts_cur_date < $ts_year_end;
			$ts_cur_date=strtotime("+1 month",$ts_cur_date))
		{
			$year_month_selection .= '<option value="/apps/eattendance/'.$y.'/'.$m.'/" '.($y==$kis_data['year'] && $m==$kis_data['month']?'selected':'').'>'.$y.'&nbsp;'.$kis_lang['month_'.$m].'</option>';
		}
		$year_month_selection .= '</select>';
		
		echo $year_month_selection;
	?>
    
</div>
<?php
if($sys_custom['KIS_eAttendance']['ParentHideDetail']){
?>
<p class="spacer"></p>

<div class="table_board">
    <table class="common_table_list edit_table_list">
						
	<tr>
	    <th><?=$kis_lang['date']?></th>
	    <th><?=$kis_lang['in']?> / <?=$kis_lang['out']?></th>
	    <!-- <th><?=$kis_lang['carriedby']?></th>-->
	    <th><?=$kis_lang['reason']?></th>
	    <th><?=$kis_lang['remark']?></th>
	    <th><?=$kis_lang['recordedby']?></th>
	</tr>
	
	<? if ($attend_days):?>
	    <? foreach ($attend_days as $attend_day): ?>
	       
		<tr>
		    <td><?=$year?>-<?=$month?>-<?=$attend_day['day']?> (AM)</td>
		    <td>		   
            <?php
             if ($attend_day['am_status'] == 'absent'){		  
			  	echo '<span class="att_absent">'.$kis_lang['absent'].'<br/>';
			 }else{
				if($attend_day['am_here'] || $attend_day['am_status'] == 'earlyleave' || $attend_day['am_status']=='lateandearlyleave'){
					echo $kis_lang['in'].'<br/>';
					if($attend_day['am_status'] == 'earlyleave' || $attend_day['am_status']=='lateandearlyleave'){
						echo $kis_lang['out'].'<br/>';
					}
				}else if (($attend_day['leave_school_hour']>=12 && $attend_day['in_school_hour']>=12)||!$attend_day['am_here']) {	
					echo '--';	
	            }else{
					if ($attend_day['am_here'] || $attend_day['in_school_hour']<12){
					    echo $kis_lang['in'].'<br/>';
					}
					if ($attend_day['am_status']=='earlyleave' || $attend_day['am_status']=='lateandearlyleave' || $attend_day['leave_school_hour']<12){
					   echo $kis_lang['out'].'<br/>';
					}
			    }
             }
            ?>		    
		    </td>
		    <!--<td>--</td>-->
		    <td><?=$attend_day['am_reason']?$attend_day['am_reason']:'--'?></td>
		    <td><?=$attend_day['am_remark']?$attend_day['am_remark']:'--'?></td>
		    <td><span class="date_time"><?=$attend_day['am_modified']==''?'--':date("Y-m-d",strtotime($attend_day['am_modified']))?><em><?=$attend_day['am_modified_by_'.$lang]?></em></span></td>
		</tr>
	      
		<tr>
		    <td><?=$year?>-<?=$month?>-<?=$attend_day['day']?> (PM)</td>
		    <td>
		    <? 
		    if ($attend_day['pm_status'] == 'absent') {		  
				echo '<span class="att_absent">'.$kis_lang['absent'].'<br/>';
		    }else{
		    	if ($attend_day['in_school_hour'] >= 12){
		    		echo $kis_lang['in'].'<br/>';
		    	}
		    	if ($attend_day['pm_status'] == 'earlyleave' || $attend_day['pm_status'] == 'lateandearlyleave' || $attend_day['leave_school_hour']>=12){
		    		echo $kis_lang['out'].'<br/>';
		    	}else if (($attend_day['leave_school_hour']<12 && $attend_day['in_school_hour']<12)||!$attend_day['pm_here']){		
					echo '--';
		    	}
		    }
		    ?>
		    </td>
		    <!--<td>--</td>-->
		    <td><?=$attend_day['pm_reason']?$attend_day['pm_reason']:'--'?></td>
		    <td><?=$attend_day['pm_remark']?$attend_day['pm_remark']:'--'?></td>
		    <td><span class="date_time"><?=$attend_day['pm_modified']==''?'--':date("Y-m-d",strtotime($attend_day['pm_modified']))?><em><?=$attend_day['pm_modified_by_'.$lang]?></em></span></td>
		</tr> 
		
	    <? endforeach; ?>
	
	<? else: ?>
	</tbody></table>
	    <div class="no_record">
	       <?=$kis_lang['norecord']?>!
	    </div>
	<? endif; ?>
    </table>
    <p class="spacer"></p>
    <p class="spacer"></p><br>
</div>

<?php } 
else{ ?>         
<p class="spacer"></p>

<div class="table_board">
    <table class="common_table_list edit_table_list">
						
	<tr>
	    <th><?=$kis_lang['date']?></th>
	    <th><?=$kis_lang['status']?></th>
	    <th><?=$kis_lang['in']?> / <?=$kis_lang['out']?></th>
	    <th><?=$kis_lang['time']?></th>
	    <!--<th><?=$kis_lang['carriedby']?></th>-->
	    <th><?=$kis_lang['remark']?></th>
		<?php if($sys_custom['KIS_eAttendance']['ParentHideRecordTimeAndUpdateUser']) {?>
		<th><?=$kis_lang['recorddate']?></th>
		<?php } else { ?>
	    <th><?=$kis_lang['recordedby']?></th>
		<?php } ?>
	</tr>
	
	<? if ($attend_days):?>
	    <? foreach ($attend_days as $attend_day): ?>
	       
		<tr>
		    <td><?=$year?>-<?=$month?>-<?=$attend_day['day']?> (AM)</td>
		    <td><span class="att_<?=$attend_day['am_status']?>"><?=$kis_lang[$attend_day['am_status']]?></span></td>
		    <td>
		    <? if ($attend_day['leave_school_hour']>=12 && $attend_day['in_school_hour']>=12 || !$attend_day['am_here'] ): ?>
			--
		    <? else: ?>
			<? if ($attend_day['in_school_hour']<12): ?>
			    <?=$kis_lang['in']?><br/>
			<? endif; ?>
			<? if ($attend_day['leave_school_hour']<12): ?>
			    <?=$kis_lang['out']?><br/>
			<? endif; ?>
		    <? endif; ?>
		    </td>
		    <td>
		    <? if (($attend_day['leave_school_hour']>=12 && $attend_day['in_school_hour']>=12) || !$attend_day['am_here']): ?>
			--
		    <? else: ?>
			<? if ($attend_day['in_school_hour']<12): ?>
			    <?=$attend_day['in_school_time']?><br/>
			<? endif; ?>
			<? if ($attend_day['leave_school_hour']<12): ?>
			    <?=$attend_day['leave_school_time']?><br/>
			<? endif; ?>
		    <? endif; ?>
		    </td>
		    <!--<td>--</td>-->
		    <td><?=$attend_day['am_remark']?$attend_day['am_remark']:'--'?></td>
            <?php if($sys_custom['KIS_eAttendance']['ParentHideRecordTimeAndUpdateUser']) { ?>
            <td><span class="date_time"><?=($attend_day['am_modified'] == '') ? '--' : date("Y-m-d", strtotime($attend_day['am_modified']))?></span></td>
            <?php } else { ?>
		    <td><span class="date_time"><?=$attend_day['am_modified']?><em><?=$attend_day['am_modified_by_'.$lang]?></em></span></td>
            <?php } ?>
		</tr>
	      
		<tr>
		    <td><?=$year?>-<?=$month?>-<?=$attend_day['day']?> (PM)</td>
		    <td><span class="att_<?=$attend_day['pm_status']?>"><?=$kis_lang[$attend_day['pm_status']]?></span></td>
		    <td>
		    <? if (($attend_day['leave_school_hour']<12 && $attend_day['in_school_hour']<12) || !$attend_day['pm_here']): ?>
			--
		    <? else: ?>
			<? if ($attend_day['in_school_hour']>=12): ?>
			    <?=$kis_lang['in']?><br/>
			<? endif; ?>
			<? if ($attend_day['leave_school_hour']>=12): ?>
			    <?=$kis_lang['out']?><br/>
			<? endif; ?>
		    <? endif; ?>
		    </td>
		    <td>
		    
		    <? if (($attend_day['leave_school_hour']<12 && $attend_day['in_school_hour']<12) || !$attend_day['pm_here']): ?>
			--
		    <? else: ?>
			<? if ($attend_day['in_school_hour']>=12): ?>
			    <?=$attend_day['in_school_time']?><br/>
			<? endif; ?>
			<? if ($attend_day['leave_school_hour']>=12): ?>
			    <?=$attend_day['leave_school_time']?><br/>
			<? endif; ?>
		    <? endif; ?>
		    </td>
		    <!--<td>--</td>-->
		    <td><?=$attend_day['pm_remark']?$attend_day['pm_remark']:'--'?></td>
			<?php if($sys_custom['KIS_eAttendance']['ParentHideRecordTimeAndUpdateUser']) { ?>
            <td><span class="date_time"><?=($attend_day['pm_modified'] == '') ? '--' : date("Y-m-d", strtotime($attend_day['pm_modified']))?></span></td>
			<?php } else { ?>
		    <td><span class="date_time"><?=$attend_day['pm_modified']?><em><?=$attend_day['pm_modified_by_'.$lang]?></em></span></td>
            <?php } ?>
		</tr> 
		
	    <? endforeach; ?>
	
	<? else: ?>
	</tbody></table>
	    <div class="no_record">
	       <?=$kis_lang['norecord']?>!
	    </div>
	<? endif; ?>
    </table>
    <p class="spacer"></p>
    <p class="spacer"></p><br>
</div>
 <?php }?>                   
                    
                    