<?php
/*
 *  Using: 
 *
 *  Purpose: build submit page for Alipay
 *
 *  2020-02-03 Henry
 *      - create this file
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/libalipay.php");
include_once($PATH_WRT_ROOT."kis/admission_alipayhk/eadmission_config.php");


include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");
intranet_opendb();
$li = new libdb();
$libkis 	= new kis('');
$lac= new admission_cust();
//if (empty($intranet_root)) {
//    return;
//}

//$error = array();
//$displayError = '';
//$transactionDetailAry = array();
//$totalAmount = 0;
//if (!$_POST['applicationID']) {
//    $error[]['ItemName'] = 'MissingUser';
//    $displayError .= $Lang['ePOS']['eClassApp']['error']['UserNotFound'];
//}
//else {
//    $pendingCart = $libpos->getPendingCart($_POST['applicationID']);
//
//    for ($i = 0, $iMax = count($pendingCart); $i < $iMax; $i++) {
//        $_pendingCart = $pendingCart[$i];
//        $_itemID = $_pendingCart['ItemID'];
//        $_itemName = $_pendingCart['ItemName'];
//        $_unitPrice = $_pendingCart['UnitPrice'];
//        $_purchaseQty = $_pendingCart['PurchaseQty'];
//        $_remainQty = $_pendingCart['RemainQty'];
//        $_sequence = $_pendingCart['Sequence'];
//        if ($_purchaseQty > $_remainQty) {
//            $error[$_itemID] = array('ItemName' => $_itemName, 'PurchaseQty' => $_purchaseQty, 'RemainQty' => $_remainQty, 'Error' => -4);      // -4: OutOfStock
//        }
//        $transactionDetailAry[$i]['ItemID'] = $_itemID;
//        $transactionDetailAry[$i]['Quantity'] = $_purchaseQty;
//        $transactionDetailAry[$i]['UnitPrice'] = $_unitPrice;
//        $transactionDetailAry[$i]['Sequence'] = $_sequence;
//        $totalAmount += $_unitPrice * $_purchaseQty;
//    }
//    
//    if (count($error)) {
//        $displayError .= $Lang['ePOS']['eClassApp']['OutOfStockItem']."<br>";
//        $i = 0;
//        foreach((array) $error as $_error) {
//            if ($i>0) {
//                $displayError .= "<br>";
//            }
//            $displayError .= $_error['ItemName'];
//            $i++;
//        }
//    }
//}
$applicationID = $lac->decodeMD5ApplicationID($_REQUEST['token']);
$totalAmount = $_REQUEST['totalAmount'];

if ($applicationID) {
	// Cancel Previous Alipay Transaction
	$result = true;
    $sql = "UPDATE ADMISSION_ALIPAY_TRANSACTION SET RecordStatus=2 WHERE ApplicationID='" . $applicationID . "' AND RecordStatus=0";
    $result = $li->db_db_query($sql);//debug_pr($sql);

	// Add Alipay Transaction
	if($result){
		$sql = "INSERT INTO ADMISSION_ALIPAY_TRANSACTION (
                        ApplicationID,
                        Amount,
                        DateModified) 
                    VALUES (
                        '" . $applicationID . "',
                        '" . $totalAmount . "',
                        NOW())";
            $res = $li->db_db_query($sql);

            if ($res) {
                $transactionID = $li->db_insert_id();
                $hash = sha1($transactionID . '::' . $applicationID . date("Y-m-d H:i:s"));
        		$outTradeNo = 'AF-' . $transactionID . '-' . $applicationID . '-' . substr($hash, 0, 10);

                $sql = "UPDATE ADMISSION_ALIPAY_TRANSACTION SET OutTradeNo='".$outTradeNo."' WHERE TransactionID='".$transactionID."'";
                $result = $li->db_db_query($sql);
            }
	}
	
    

    if ($outTradeNo) {
        $alipay_config = ($sys_custom['KIS_Admission']['env'] == 'dev') ? $alipay_dev_config : $alipay_prod_config;
        
        $libalipay = new libalipay($alipay_config);
        
        $parameter = array(
            "service" => $alipay_config['service'],
            "partner" => $alipay_config['partner'],
            "notify_url" => $alipay_config['notify_url'],
            "return_url" => $alipay_config['return_url'],

            "payment_inst" => "ALIPAYHK",
            "out_trade_no" => $outTradeNo,
            "subject" => $outTradeNo,
            "total_fee" => $totalAmount,
            "body" => "",
            "split_fund_info" => "",
            "currency" => $alipay_config['currency'],
            "product_code" => "NEW_WAP_OVERSEAS_SELLER",
            "_input_charset" => trim(strtolower($alipay_config['input_charset']))
        );
    }
    else {
        $displayError .= $Lang['ePOS']['eClassApp']['error']['NoOutTradeNo'];
    }
}

?>
<script type="text/javascript" src="/templates/jquery/jquery-1.8.0.min.js"></script>
<script type="text/javascript">
    function show_ajax_error()
    {
        alert('<?php echo $Lang['General']['AjaxError'];?>');
    }

    jQuery(document).ready(function(){
        $('#applForm').submit();
//        $(window.frames[0]).remove();
    });

</script>

</head>

<body>
<div class="mainBody">

    <div id="payBeforeLayout">
    <?php
        if ($outTradeNo) {
//            echo $libalipay->buildRequestForm($parameter, "applForm", "applForm", "post", "payFrame");
            echo $libalipay->buildRequestForm($parameter, "applForm", "applForm", "post", "");
        }
        else {
            echo 'Fails';
        }
    ?>
    </div>

<?php if ($outTradeNo): ?>
    <div id="div_processing">
        <table align="Center">
            <tr>
                <td align="center">
                    <h1 class="warning">付款進行中，請勿關閉本視窗。</h1>
                    <h1 class="warning">Please don't close this page while processing payment.</h1>
                    <br/>
                    <img id="spinner" src="<?php echo $PATH_WRT_ROOT;?>/images/<?php echo $LAYOUT_SKIN;?>/loadingAnimation.gif" width="208px" height="13px"/>
                </td>
            </tr>
        </table>
    </div>
<?php endif; ?>


</div>

<!-- 
<iframe id="payFrame" name="payFrame" src="" width="100%" height="100%" style="border:none;min-width:800px;min-height:600px;"></iframe>
 -->

</body>
</html>
