<?php
# modifying by: Pun
 
/********************
 * Log :
 * Date		2015-09-07 [Pun]
 * 			Add cust field
 * 
 * Date		2015-08-04 [Pun]
 * 			File Created
 * 
 ********************/

############ UI START ############ 
?>
<style>
.admission_board{
	min-height: 0;
}
</style>

<div class="admission_board">
	<p class="spacer"></p>
	<br />
	
<form id="form1" name="form1" method="POST" action="confirm_update.php">
	<input type="hidden" name="BriefingID" value="<?=$BriefingID?>" />

	
<!-- -------- Form START -------- -->		
	<h1><?=$Lang['Admission']['applyInfo']?> Applicant Information</h1>
	<table class="form_table" style="font-size: 13px">
	
		<tr>
			<td class="field_title"><?=$star.$Lang['Admission']['student'].$Lang['Admission']['name']?> Student Name</td>
			<td>
				<input type="text" name="StudentName" id="StudentName" class="textboxtext">
			</td>
			
			
			<td class="field_title"><?=$star.$Lang['Admission']['student'].$Lang['Admission']['gender']?> Gender</td>
			<td>
				<input type="radio" name="StudentGender" id="StudentGenderM" value="M"/>
				<label for="StudentGenderM"><?=$Lang['Admission']['genderType']['M']?></label>&nbsp;&nbsp;
				<input type="radio" name="StudentGender" id="StudentGenderF" value="F"/>
				<label for="StudentGenderF"><?=$Lang['Admission']['genderType']['F']?></label>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?=$Lang['Admission']['KTLMSKG']['KindergartenAttending']?> Kindergarten Attending</td>
			<td>
				<input type="text" name="Kindergarten" id="Kindergarten" class="textboxtext">
			</td>
			
			
			<td class="field_title"><?=$star.$Lang['Admission']['parent'].$Lang['Admission']['name']?> Parent Name</td>
			<td>
				<input type="text" name="ParentName" id="ParentName" class="textboxtext">
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?=$star.$Lang['Admission']['contactEmail']?> Contact Email</td>
			<td>
				<input type="text" name="Email" id="Email" class="textboxtext">
			</td>
			
			
			<td class="field_title"><?=$star.$Lang['Admission']['contactEmailConfirm']?> Confirm Contact Email</td>
			<td>
				<input type="text" id="EmailConfirm" class="textboxtext">
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?=$star.$Lang['Admission']['phoneno']?> Phone No.</td>
			<td>
				<input type="text" name="PhoneNo" id="PhoneNo" class="textboxtext" maxlength="8">
			</td>
			
			<td class="field_title"><?=$star.$Lang['Admission']['requestSeat']?> Seat requested</td>
			<td colspan="3">
				<?=$requestSeatHTML?>
			</td>
		</tr>
		
	</table>
<!-- -------- Form END -------- -->	

<span><?=$Lang['Admission']['munsang']['mandatoryfield']?></span>
<br/>
「<span class="tabletextrequire">*</span>」are mandatory but if not applicable please fill in N.A.

	<div class="edit_bottom">
		<input type="button" class="formsubbutton" onclick="MM_goToURL('parent','index.php');return document.MM_returnValue" value="取消 Cancel">
		<input type="button" id="submitbtn" class="formbutton" value="提交 Submit">
	</div>
</form>

	<p class="spacer"></p>
</div>

<script>
$('#submitbtn').click(function(e){
	
	//for email validation
	var re = /\S+@\S+\.\S+/;
	
	if($('#StudentName').val() == ''){
		alert('<?=$Lang['Admission']['msg']['inputStudentName']?> \nPlease enter Student Name.');
		$('#StudentName').focus();
		return false;
	}
	if($('input[name="StudentGender"]:checked').length == 0){
		alert('<?=$Lang['Admission']['msg']['selectgender']?> \nPlease select Gender.');
		$('input[name="StudentGender"]:first').focus();
		return false;
	}
	if($('#ParentName').val() == ''){
		alert('<?=$Lang['Admission']['msg']['inputParentName']?> \nPlease enter Parent Name.');
		$('#ParentName').focus();
		return false;
	}
	if($('#Email').val() == ''){
		alert("<?=$Lang['Admission']['msg']['enterstudentemail']?>\nPlease enter Contact Email");
		$('#Email').focus();
		return false;
	}
	if($('#EmailConfirm').val() != $('#Email').val()){
		alert("<?=$Lang['Admission']['msg']['contactEmailMismatch']?>\nConfirm Contact Email Mismatch");
		$('#EmailConfirm').focus();
		return false;
	}
	if(!re.test($('#Email').val())){
		alert("<?=$Lang['Admission']['mgf']['msg']['importInvalidFormatOfEmailAddress']?>\nInvalid Contact Email");	
		$('#Email').focus();
		return false;
	} 
	if($('#PhoneNo').val() == ''){
		alert("<?=$Lang['Admission']['munsang']['msg']['enterphone']?>\nPlease enter Telephone.");
		$('#PhoneNo').focus();
		return false;
	}
	if(!/^[0-9]{8}$/.test($('#PhoneNo').val())){
		alert("<?=$Lang['Admission']['munsang']['msg']['enternumber']?>\nPlease enter number with 8 digits.");
		$('#PhoneNo').focus();
		return false;
	}
	
	
	$.get('ajaxGetRemainsQuota.php', {
		'BriefingID': '<?=$BriefingID?>'
	}, function(res){
		var SeatRemains = parseInt(res);
		var SeatRequest = $('#SeatRequest').val();
		
		if(SeatRemains < 1){
			alert('<?=$Lang['Admission']['msg']['briefingFull']?> \nBriefing session is full.');
			MM_goToURL('parent','index.php');
			return false;
		}
		
		if( SeatRemains < SeatRequest ){
			var notEnoughSeatStr = '<?=$Lang['Admission']['msg']['notEnoughSeat']?> \nRemains quota is not enough for <!-- Request --> people (Remains: <!-- Remains -->). Change the request seat?';
			notEnoughSeatStr = notEnoughSeatStr.replace(/<!-- Request -->/g, SeatRequest);
			notEnoughSeatStr = notEnoughSeatStr.replace(/<!-- Remains -->/g, res);
			
			if(confirm(notEnoughSeatStr)){
				$('#SeatRequest').val(SeatRemains);
			}else{
				return false;
			}
		}
		
		//alert('submit');return false;
		$('#form1').submit();
	});
});
</script>