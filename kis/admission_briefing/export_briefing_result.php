<?php
//using Henry

/********************
 * 
 * Log :
 * Date		2015-09-14 [Pun]
 * 			File Created
 * 
 ********************/

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/libadmission_briefing_base.php");


######## Init START ########
intranet_opendb();
$abb		= new admission_briefing_base();
$lexport = new libexporttext();

ini_set("memory_limit", "150M"); 

$briefingIdArr = IntegerSafe((array)$briefingId);
$schoolYearID = IntegerSafe($selectSchoolYearID);
######## Init END ########

######## Access Right START ########
if($_SESSION['UserType']!=USERTYPE_STAFF){
	echo 'Access Denied.';
	exit;
}
######## Access Right END ########


$data = array();
######## Get Data START ########
#### Get Briefing Session Name START ####
if(count($briefingIdArr) > 0 && $briefingIdArr[0] > 0){
	$sessionArr = $abb->getBriefingSessionArr($briefingIdArr);
}else{
	$sessionArr = $abb->getAllBriefingSession($schoolYearID);
}
$sessionNameArr = BuildMultiKeyAssoc($sessionArr, array('BriefingID') , array('Title'), $SingleValue=1);
$briefingIdArr = array_keys($sessionNameArr);
#### Get Briefing Session Name END ####

#### Get Briefing Applicant START ####
// $applicantArr = $abb->getAllBriefingApplicant($briefingIdArr);
$applicantArr = $abb->getFilterBriefingApplicant($briefingIdArr, array(
	'keyword' => $keyword
));
#### Get Briefing Applicant END ####

#### Pack Data START ####
foreach($applicantArr as $applicant){
	$applicant['SessionName'] = $sessionNameArr[ $applicant['BriefingID'] ];
	$applicant['Display_Email'] = ($applicant['Email'])?$applicant['Email']:$applicant['Email_BAK'];
	$applicant['Display_StudentGender'] = $kis_lang['Admission']['genderType'][ $applicant['StudentGender'] ];
	$data[] = $applicant;
}
#### Pack Data END ####
######## Get Data END ########


$dataArray = array();
$exportColumn = array();
######## Pack Data START ########
if($sys_custom['KIS_Admission']['KTLMSKG']['Settings']){		
	$exportColumn[] = $kis_lang['applicationno'];
	$exportColumn[] = $kis_lang['Admission']['briefing'];
	$exportColumn[] = $kis_lang['studentname'];
	$exportColumn[] = $kis_lang['Admission']['gender'];
	$exportColumn[] = $kis_lang['parentname'];
	$exportColumn[] = $kis_lang['emailaddress'];
	$exportColumn[] = $kis_lang['Admission']['phoneno'];
	$exportColumn[] = $kis_lang['requestSeat'];
	$exportColumn[] = $kis_lang['Admission']['KTLMSKG']['KindergartenAttending'];

	foreach($data as $i=>$d){
		$dataArray[$i][] = $d['ApplicantID'];
		$dataArray[$i][] = $d['SessionName'];
		$dataArray[$i][] = $d['StudentName'];
		$dataArray[$i][] = $d['Display_StudentGender'];
		$dataArray[$i][] = $d['ParentName'];
		$dataArray[$i][] = $d['Display_Email'];
		$dataArray[$i][] = $d['PhoneNo'];
		$dataArray[$i][] = $d['SeatRequest'];
		$dataArray[$i][] = $d['Kindergarten'];
	}
}elseif($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
	$exportColumn[] = $kis_lang['applicationno'];
	$exportColumn[] = $kis_lang['Admission']['briefing'];
	$exportColumn[] = $kis_lang['studentname'];
	$exportColumn[] = $kis_lang['parentname'];
	$exportColumn[] = $kis_lang['emailaddress'];
	$exportColumn[] = $kis_lang['Admission']['phoneno'];
	$exportColumn[] = $kis_lang['requestSeat'];
	$exportColumn[] = $kis_lang['Admission']['yearofbirth'];
	$exportColumn[] = 'IP';

	foreach($data as $i=>$d){
		$dataArray[$i][] = $d['ApplicantID'];
		$dataArray[$i][] = $d['SessionName'];
		$dataArray[$i][] = $d['StudentName'];
		$dataArray[$i][] = $d['ParentName'];
		$dataArray[$i][] = $d['Display_Email'];
		$dataArray[$i][] = $d['PhoneNo'];
		$dataArray[$i][] = $d['SeatRequest'];
		$dataArray[$i][] = substr($d['DOB'], 0, 4);
		$dataArray[$i][] = $d['REMOTE_ADDR'];
	}
}else{	
	$exportColumn[] = $kis_lang['applicationno'];
	$exportColumn[] = $kis_lang['Admission']['briefing'];
	$exportColumn[] = $kis_lang['studentname'];
	$exportColumn[] = $kis_lang['parentname'];
	$exportColumn[] = $kis_lang['emailaddress'];
	$exportColumn[] = $kis_lang['Admission']['phoneno'];
	$exportColumn[] = $kis_lang['requestSeat'];
	$exportColumn[] = $kis_lang['Admission']['birthcertno'];

	foreach($data as $i=>$d){
		$dataArray[$i][] = $d['ApplicantID'];
		$dataArray[$i][] = $d['SessionName'];
		$dataArray[$i][] = $d['StudentName'];
		$dataArray[$i][] = $d['ParentName'];
		$dataArray[$i][] = $d['Display_Email'];
		$dataArray[$i][] = $d['PhoneNo'];
		$dataArray[$i][] = $d['SeatRequest'];
		$dataArray[$i][] = $d['BirthCertNo'];
	}
}
######## Pack Data END ########


######## Export START ########		
$filename = "briefing_list.csv";
$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($dataArray, $exportColumn, "\t", "\r\n", "\t", 0, "9");
$lexport->EXPORT_FILE($filename, $export_content);
?>