<?php
//Using: 
/**
 * Change log:
 * 2019-02-13 Pun
 *  - File created
 */
?>
<style>
.admission_board{
	min-height: 0;
}
</style>

<div class="admission_board">
	<p class="spacer"></p>
	<br />
	
<form id="form1" name="form1" method="POST" action="confirm_update.php">
	<input type="hidden" name="BriefingID" value="<?=$BriefingID?>" />

	
<!-- -------- Form START -------- -->		
	<h1><?=$Lang['Admission']['applyInfo']?> Applicant Information</h1>
	<table class="form_table" style="font-size: 13px">
	
		<tr>
			<td class="field_title"><?="{$star}{$LangB5['Admission']['student']}{$LangB5['Admission']['name']} {$LangEn['Admission']['student']} {$LangEn['Admission']['name']}"?></td>
			<td>
				<input type="text" name="StudentName" id="StudentName" class="textboxtext">
			</td>
			
			<td class="field_title"><?="{$star}{$LangB5['Admission']['HKUGAPS']['studentyearofbirth']} {$LangEn['Admission']['HKUGAPS']['studentyearofbirth']}"?></td>
			<td>
				<input type="text" name="StudentYearOfBirth" id="StudentYearOfBirth" class="textboxtext">
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?="{$star}{$LangB5['Admission']['parent']}{$LangB5['Admission']['name']} {$LangEn['Admission']['parent']}{$LangEn['Admission']['name']}"?></td>
			<td>
				<input type="text" name="ParentName" id="ParentName" class="textboxtext">
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?="{$star}{$LangB5['Admission']['contactEmail']} {$LangEn['Admission']['contactEmail']}"?></td>
			<td>
				<input type="text" name="Email" id="Email" class="textboxtext">
			</td>
			
			
			<td class="field_title"><?="{$star}{$LangB5['Admission']['contactEmailConfirm']} {$LangEn['Admission']['contactEmailConfirm']}"?></td>
			<td>
				<input type="text" id="EmailConfirm" class="textboxtext">
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?="{$star}{$LangB5['Admission']['phoneno']} {$LangEn['Admission']['phoneno']}"?></td>
			<td>
				<input type="text" name="PhoneNo" id="PhoneNo" class="textboxtext" maxlength="8">
			</td>
			
			<td class="field_title"><?="{$star}{$LangB5['Admission']['requestSeat']} {$LangEn['Admission']['requestSeat']}"?></td>
			<td colspan="3">
				<?=$requestSeatHTML?>
			</td>
		</tr>
		
	</table>
<!-- -------- Form END -------- -->	

<span><?=$LangB5['Admission']['munsang']['mandatoryfield']?></span>
<br/>
<span><?=$LangEn['Admission']['munsang']['mandatoryfield']?></span>

	<div class="edit_bottom">
		<input type="button" class="formsubbutton" onclick="MM_goToURL('parent','index.php');return document.MM_returnValue" value="取消 Cancel">
		<input type="button" id="submitbtn" class="formbutton" value="提交 Submit">
	</div>
</form>

	<p class="spacer"></p>
</div>

<script>
$('#submitbtn').click(function(e){
	
	//for email validation
	var re = /\S+@\S+\.\S+/;
	
	if($('#StudentName').val() == ''){
		alert('<?=$Lang['Admission']['msg']['inputStudentName']?> \nPlease enter Student Name.');
		$('#StudentName').focus();
		return false;
	}
	
	if(!/^[0-9]{4}$/.test($('#StudentYearOfBirth').val())){
		alert("<?=$LangB5['Admission']['HKUGAPS']['msg']['studentyearofbirth']?>\n<?=$LangEn['Admission']['HKUGAPS']['msg']['studentyearofbirth']?>");	
		$('#StudentYearOfBirth').focus();
		return false;
	}
	/*if($('#BirthCertNo').val() == ''){
		alert('<?=$Lang['Admission']['munsang']['msg']['enterbirthcertno']?> \nPlease enter Birth Certificate Number.');
		$('#BirthCertNo').focus();
		return false;
	}
	/* No format test for BirthCertNo * /
	if(!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test($('#BirthCertNo').val())){
		alert("<?=$Lang['Admission']['munsang']['msg']['invalidbirthcertificatenumber']?>\nInvalid Birth Certificate Number.");	
		$('#BirthCertNo').focus();
		return false;
	}
	/**/
	
	if($('#ParentName').val() == ''){
		alert('<?=$Lang['Admission']['msg']['inputParentName']?> \nPlease enter Parent Name.');
		$('#ParentName').focus();
		return false;
	}
	
	if($('#Email').val() == ''){
		alert("<?=$Lang['Admission']['msg']['enterstudentemail']?>\nPlease enter Contact Email");
		$('#Email').focus();
		return false;
	}
	if($('#EmailConfirm').val() != $('#Email').val()){
		alert("<?=$Lang['Admission']['msg']['contactEmailMismatch']?>\nConfirm Contact Email Mismatch");
		$('#EmailConfirm').focus();
		return false;
	}
	if(!re.test($('#Email').val())){
		alert("<?=$Lang['Admission']['mgf']['msg']['importInvalidFormatOfEmailAddress']?>\nInvalid Contact Email");	
		$('#Email').focus();
		return false;
	} 
	if($('#PhoneNo').val() == ''){
		alert("<?=$Lang['Admission']['munsang']['msg']['enterphone']?>\nPlease enter Telephone.");
		$('#PhoneNo').focus();
		return false;
	}
	if(!/^[0-9]{8}$/.test($('#PhoneNo').val())){
		alert("<?=$Lang['Admission']['munsang']['msg']['enternumber']?>\nPlease enter number with 8 digits.");
		$('#PhoneNo').focus();
		return false;
	}
	
	$('#form1').submit();
});
</script>