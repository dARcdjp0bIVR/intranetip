<?php
# modifying by: 
 
/********************
 * Log :
 * Date		2019-07-11 [Henry]
 * 			Ignore time checking for getDecryptedText
 * 
 * Date		2015-08-04 [Pun]
 * 			File Created
 * 
 ********************/
 
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");

include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/libadmission_briefing_base.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");


intranet_opendb();

$libkis 	= new kis('');
$lac		= new admission_cust();
$moduleSettings = $lac->getBasicSettings(99999, array (
						'enablebriefingsession'
					));
######## Access Right START ######## 
if (!$plugin['eAdmission'] || !$sys_custom['KIS_Admission']['BriefingModule'] && !$moduleSettings['enablebriefingsession'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}
######## Access Right END ######## 


######## Init START ######## 
$abb		= new admission_briefing_base();
$li			= new interface_html();

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool(); 
$schoolYearID = $lac->getNextSchoolYearID();

######## Init END ######## 


############ Get Data START ############
if($_REQUEST['id']){
	parse_str(getDecryptedText(urldecode($_REQUEST['id']),$admission_cfg['FilePathKey'], 1000000), $output);
	$Briefing_ApplicantID = $output['Briefing_ApplicantID'];
	$DeleteRecordPassKey = $output['DeleteRecordPassKey'];
	$application = $abb->getBriefingApplicant($Briefing_ApplicantID);
	if($application['DeleteRecordPassKey'] == $DeleteRecordPassKey){
		$abb->deleteBriefingApplicant($Briefing_ApplicantID);
	}
}else{
	$application = $abb->getBriefingApplicantByDeleteRecordPassKey(urldecode($_REQUEST['key']));
	$abb->deleteBriefingApplicant($application['Briefing_ApplicantID']);
}
############ Get Data END ############
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<title>:: eClass KIS ::</title>
	
	<link href="/templates/kis/css/common.css" rel="stylesheet" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="/templates/jquery/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="/templates/jquery/jquery.fancybox.js?v=2.1.0"></script>
	<link rel="stylesheet" type="text/css" href="/templates/jquery/jquery.fancybox.css?v=2.1.0" media="screen" />
	<script language="JavaScript" src="/templates/script.js"></script>
	
	<script type="text/javascript">
		function MM_goToURL() { //v3.0
			var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
		}
		<?=$javascript?>
	</script>
	
</head>
<body class="parent">
	<div id="container">
        <div class="top_header">
            <a href="./" class="logo" title="eClass KIS" style="background-image: url('<?=$school['logo']?>')"></a>
            <div class="school_name"><?=GET_SCHOOL_NAME()?></div>
        </div>
        <div class="board" id="module_wood_page" style="width:100%">
        	<div class="board_top"><div class="board_top_right"><div class="board_top_bg"><div class="board_top_content">
        	</div></div></div></div>
			<div class="board_main" style="padding-left: 30px;"><div class="board_main_right" style="padding-right: 30px;"><div class="board_main_bg"><div class="board_main_content">
				<div class="main_content">
<!-------------- Main Content START -------------->

					<div class="admission_board">
					
						<div class="admission_complete_msg">
							<h1>
								<?=$Lang['Admission']['RecordDeleted']?>
							</h1>
							<br />
							
							<h1>
								Record has been deleted.
							</h1>
						</div>
						
						<div class="edit_bottom">
							<input type="button" class="formsubbutton" onclick="location.href='index.php'" value="<?=$Lang['Btn']['Confirm']?> Confirm" />
						</div>
						<p class="spacer"></p>
					</div>
						
<!-------------- Main Content END -------------->                    
				</div>
			</div></div></div></div>
			<div class="board_bottom"><div class="board_bottom_right"><div class="board_bottom_bg"></div></div></div>
		</div>
        <div class="footer"><a href="http://eclass.com.hk" title="eClass"></a><span>Powered by</span></div>
	</div>
</body>
</html>