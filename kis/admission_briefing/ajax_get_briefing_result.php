<?php


$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");

include_once("../config.php");

include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/libadmission_briefing_base.php");

#### Lang START ####
if($intranet_session_language == 'en'){
    $intranet_session_language = 'b5';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
    unset($Lang);
    unset($kis_lang);

    $intranet_session_language = 'en';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
}else{
    $intranet_session_language = 'en';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
    unset($Lang);
    unset($kis_lang);

    $intranet_session_language = 'b5';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
}
#### Lang END ####

############ Header END ############

intranet_opendb();

############ Init START ############
if(class_exists('admission_briefing')){
	$abb = new admission_briefing();
}else{
	$abb = new admission_briefing_base();
}
$lac		= new admission_cust();
############ Init END ############


if(file_exists($setting_path_ip_rel."/ajax_get_briefing_result.php")){
    include($setting_path_ip_rel."/ajax_get_briefing_result.php");
    exit;
}


############ Get briefing applicant START ############ 
$briefingApplicant = $abb->getBriefingApplicantByApplicantID($ApplicationNo);
if(trim($ContactEmail) != trim($briefingApplicant['Email'])){
?>
	<script>alert('申請編號或聯絡電郵錯誤\nIncorrect Application Number or Contact Email.');</script>
<?php
    exit;
}
############ Get briefing applicant END ############ 

############ Get briefing session START ############ 
$briefingSession = $abb->getBriefingSession($briefingApplicant['BriefingID']);
############ Get briefing session END ############ 

$id = urlencode(getEncryptedText("Briefing_ApplicantID={$briefingApplicant['Briefing_ApplicantID']}", $admission_cfg['FilePathKey']));


$resultIsOpen = $lac->getBasicSettings($kis_data['schoolYearID'], array (
    'briefingAnnouncement',
));
$resultIsOpen = $resultIsOpen['briefingAnnouncement'];
?>


<!-------------- Main Content START -------------->
<form id="form1" action="print_briefing_result.php" method="POST" target="_blank">
    <input type="hidden" name="id" value="<?=$id?>" />
    <div class="admission_board">
    	<br />
    	
    	<div>
    		<h1>
    			<?=$Lang['Admission']['briefingApplicantInfo']?> Application Information
    		</h1>
    		<table class="form_table" style="font-size: 13px">
    		<colgroup>
    			<col style="width: 25%" />
    			<col style="width: 25%" />
    			<col style="width: 25%" />
    			<col style="width: 25%" />
    		</colgroup>
    		
    		
    		<tr>
    			<td class="field_title">
    				<?=$LangB5['Admission']['briefing']?>
    				<?=$LangEn['Admission']['briefing']?>
    			</td>
    			<td>
    				<?=$briefingSession['Title'] ?>
    			</td>
    			
    			<td class="field_title">
    				<?=$LangB5['Admission']['requestSeat']?>
    				<?=$LangEn['Admission']['requestSeat']?>
    			</td>
    			<td>
    				<?=$briefingApplicant['SeatRequest']?>
    			</td>
    		</tr>
    		
    		<tr>
    			<td class="field_title">
    				<?=$LangB5['Admission']['StartTime']?>
    				<?=$LangEn['Admission']['StartTime']?>
    			</td>
    			<td>
    				<?=substr($briefingSession['BriefingStartDate'], 11,5) ?>
    			</td>
    			
    			<td class="field_title">
    				<?=$LangB5['Admission']['EndTime']?>
    				<?=$LangEn['Admission']['EndTime']?>
    			</td>
    			<td>
    				<?=substr($briefingSession['BriefingEndDate'], 11,5) ?>
    			</td>
    		</tr>
    		
    		<tr>
    			<td>&nbsp;</td>
    		</tr>
    		
    		<tr>
    			<td class="field_title">
    				<?=$LangB5['Admission']['applicationno']?>
    				<?=$LangEn['Admission']['applicationno']?>
    			</td>
    			<td colspan="3">
    				<?=$briefingApplicant['ApplicantID']?>
    			</td>
    		</tr>
    		
    		<tr>
    			<td class="field_title">
    				<?=$LangB5['Admission']['student'].$LangB5['Admission']['name']?>
    				<?=$LangEn['Admission']['student'].$LangEn['Admission']['name']?>
    			</td>
    			<td>
    				<?=$briefingApplicant['StudentName']?>
    			</td>
    			<td class="field_title">
    				<?=$LangB5['Admission']['parent'].$LangB5['Admission']['name']?>
    				<?=$LangEn['Admission']['parent'].$LangEn['Admission']['name']?>
    			</td>
    			<td>
    				<?=$briefingApplicant['ParentName']?>
    			</td>
    		</tr>
    			
    		<tr>
    			<td class="field_title">
    				<?=$LangB5['Admission']['contactEmail']?>
    				<?=$LangEn['Admission']['contactEmail']?>
    			</td>
    			<td>
    				<?=$briefingApplicant['Email']?>
    			</td>
    
    			<td class="field_title">
    				<?=$LangB5['Admission']['phoneno']?>
					<?=$LangEn['Admission']['phoneno']?>
    			</td>
    			<td>
    				<?=$briefingApplicant['PhoneNo']?>
    			</td>
    		</tr>
    		
    		</table>
    	</div>
    	
		<h3 style="text-align: center;">
    		申請結果 Application result：
    		<?php
    		if($resultIsOpen){
        		if($briefingApplicant['Status'] == $admission_cfg['BriefingStatus']['confirmed']){
        		    echo "<font color='green'>已確認 Confirmed</font>";
        		}else{
        		    echo "<font color='red'>{$LangB5['Admission']['IsFull']} {$LangEn['Admission']['IsFull']}</font>";
        		}
    		}else{
    		    echo '<font color="black">(結果尚未公報)</font>';
    		}
    		?>
 		</h3>
    	
    	<?php if($resultIsOpen && $briefingApplicant['Status'] == $admission_cfg['BriefingStatus']['confirmed']){ ?>
        	<div class="edit_bottom">
        		<input type="submit" class="formbutton" value="<?=$LangB5['Btn']['Print']?> <?=$LangEn['Btn']['Print'] ?>" />
        	</div>
    	<?php } ?>
    	<p class="spacer"></p>
    </div>
</form>

<script>
$('#briefingForm').hide();
</script>