<?php
# modifying by: Pun
 
/********************
 * Log :
 * Date		2015-09-07 [Pun]
 * 			Added dynamic loading the cust template
 * 
 * Date		2015-08-04 [Pun]
 * 			File Created
 * 
 ********************/

############ Header START ############
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");

include_once("../config.php");

include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/libadmission_briefing_base.php");

#### Lang START ####
if($intranet_session_language == 'en'){
    $intranet_session_language = 'b5';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
    unset($Lang);
    unset($kis_lang);

    $intranet_session_language = 'en';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
}else{
    $intranet_session_language = 'en';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
    unset($Lang);
    unset($kis_lang);

    $intranet_session_language = 'b5';
    include($PATH_WRT_ROOT."lang/lang.{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/lang_common_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include($PATH_WRT_ROOT."lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
}
#### Lang END ####

############ Header END ############

intranet_opendb();

############ Init START ############
$abb		= new admission_briefing_base();
$li			= new interface_html();

$BriefingID = $_REQUEST['BriefingID'];
############ Init END ############


############ Get Data START ############ 
$session = $abb->getBriefingSession($BriefingID);
############ Get Data END ############ 

############ UI Init START ############
$requestSeatHTML = '<select id="SeatRequest" name="SeatRequest">';
for($i=1;$i<=$session['QuotaPerApplicant'];$i++){
	$requestSeatHTML .= "<option value=\"{$i}\">{$i}</option>";
}
$requestSeatHTML .= '</select>';
############ UI Init END ############


############ UI START ############ 
$star = '<font style="color:red;">*</font>';

if(file_exists($setting_path_ip_rel."/ajaxFormPage.php")){
	include($setting_path_ip_rel."/ajaxFormPage.php");
	exit;
}
?>
<style>
.admission_board{
	min-height: 0;
}
</style>

<div class="admission_board">
	<p class="spacer"></p>
	<br />
	
<form id="form1" name="form1" method="POST" action="confirm_update.php">
	<input type="hidden" name="BriefingID" value="<?=$BriefingID?>" />

	
<!-- -------- Form START -------- -->		
	<h1><?=$Lang['Admission']['applyInfo']?> Applicant Information</h1>
	<table class="form_table" style="font-size: 13px">
	
		<tr>
			<td class="field_title"><?="{$star}{$LangB5['Admission']['student']}{$LangB5['Admission']['name']} {$LangEn['Admission']['student']} {$LangEn['Admission']['name']}"?></td>
			<td>
				<input type="text" name="StudentName" id="StudentName" class="textboxtext">
			</td>
			
			
			<td class="field_title"><?="{$star}{$LangB5['Admission']['birthcertno']} {$LangEn['Admission']['birthcertno']}"?></td>
			<td>
				<input type="text" name="BirthCertNo" id="BirthCertNo" class="textboxtext">
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?="{$star}{$LangB5['Admission']['parent']}{$LangB5['Admission']['name']} {$LangEn['Admission']['parent']}{$LangEn['Admission']['name']}"?></td>
			<td>
				<input type="text" name="ParentName" id="ParentName" class="textboxtext">
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?="{$star}{$LangB5['Admission']['contactEmail']} {$LangEn['Admission']['contactEmail']}"?></td>
			<td>
				<input type="text" name="Email" id="Email" class="textboxtext">
			</td>
			
			
			<td class="field_title"><?="{$star}{$LangB5['Admission']['contactEmailConfirm']} {$LangEn['Admission']['contactEmailConfirm']}"?></td>
			<td>
				<input type="text" id="EmailConfirm" class="textboxtext">
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?="{$star}{$LangB5['Admission']['phoneno']} {$LangEn['Admission']['phoneno']}"?></td>
			<td>
				<input type="text" name="PhoneNo" id="PhoneNo" class="textboxtext" maxlength="8">
			</td>
			
			<td class="field_title"><?="{$star}{$LangB5['Admission']['requestSeat']} {$LangEn['Admission']['requestSeat']}"?></td>
			<td colspan="3">
				<?=$requestSeatHTML?>
			</td>
		</tr>
		
	</table>
<!-- -------- Form END -------- -->	

<span><?=$LangB5['Admission']['munsang']['mandatoryfield']?></span>
<br/>
<span><?=$LangEn['Admission']['munsang']['mandatoryfield']?></span>

	<div class="edit_bottom">
		<input type="button" class="formsubbutton" onclick="MM_goToURL('parent','index.php');return document.MM_returnValue" value="取消 Cancel">
		<input type="button" id="submitbtn" class="formbutton" value="提交 Submit">
	</div>
</form>

	<p class="spacer"></p>
</div>

<script>
$('#submitbtn').click(function(e){
	
	//for email validation
	var re = /\S+@\S+\.\S+/;
	
	if($('#StudentName').val() == ''){
		alert('<?=$Lang['Admission']['msg']['inputStudentName']?> \nPlease enter Student Name.');
		$('#StudentName').focus();
		return false;
	}
	if($('#ParentName').val() == ''){
		alert('<?=$Lang['Admission']['msg']['inputParentName']?> \nPlease enter Parent Name.');
		$('#ParentName').focus();
		return false;
	}
	if($('#BirthCertNo').val() == ''){
		alert('<?=$Lang['Admission']['munsang']['msg']['enterbirthcertno']?> \nPlease enter Birth Certificate Number.');
		$('#BirthCertNo').focus();
		return false;
	}
	/* No format test for BirthCertNo * /
	if(!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test($('#BirthCertNo').val())){
		alert("<?=$Lang['Admission']['munsang']['msg']['invalidbirthcertificatenumber']?>\nInvalid Birth Certificate Number.");	
		$('#BirthCertNo').focus();
		return false;
	}
	/**/
	if($('#Email').val() == ''){
		alert("<?=$Lang['Admission']['msg']['enterstudentemail']?>\nPlease enter Contact Email");
		$('#Email').focus();
		return false;
	}
	if($('#EmailConfirm').val() != $('#Email').val()){
		alert("<?=$Lang['Admission']['msg']['contactEmailMismatch']?>\nConfirm Contact Email Mismatch");
		$('#EmailConfirm').focus();
		return false;
	}
	if(!re.test($('#Email').val())){
		alert("<?=$Lang['Admission']['mgf']['msg']['importInvalidFormatOfEmailAddress']?>\nInvalid Contact Email");	
		$('#Email').focus();
		return false;
	} 
	if($('#PhoneNo').val() == ''){
		alert("<?=$Lang['Admission']['munsang']['msg']['enterphone']?>\nPlease enter Telephone.");
		$('#PhoneNo').focus();
		return false;
	}
	if(!/^[0-9]{8}$/.test($('#PhoneNo').val())){
		alert("<?=$Lang['Admission']['munsang']['msg']['enternumber']?>\nPlease enter number with 8 digits.");
		$('#PhoneNo').focus();
		return false;
	}
	
	
	$.get('ajaxGetRemainsQuota.php', {
		'BriefingID': '<?=$BriefingID?>'
	}, function(res){
		var SeatRemains = parseInt(res);
		var SeatRequest = $('#SeatRequest').val();
		
		if(SeatRemains < 1){
			alert('<?=$Lang['Admission']['msg']['briefingFull']?> \nBriefing session is full.');
			MM_goToURL('parent','index.php');
			return false;
		}
		
		if( SeatRemains < SeatRequest ){
			var notEnoughSeatStr = '<?=$Lang['Admission']['msg']['notEnoughSeat']?> \nRemains quota is not enough for <!-- Request --> people (Remains: <!-- Remains -->). Change the request seat?';
			notEnoughSeatStr = notEnoughSeatStr.replace(/<!-- Request -->/g, SeatRequest);
			notEnoughSeatStr = notEnoughSeatStr.replace(/<!-- Remains -->/g, res);
			
			if(confirm(notEnoughSeatStr)){
				$('#SeatRequest').val(SeatRemains);
			}else{
				return false;
			}
		}
		
		//alert('submit');return false;
		$('#form1').submit();
	});
});
</script>