<?php
//Using: 
/**
 * Change Log:
 * 2019-02-15 Pun
 *  - File created
 */
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");

include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");


intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<title>:: eClass KIS ::</title>
	
	<link href="/templates/kis/css/common.css" rel="stylesheet" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="/templates/jquery/jquery-1.8.0.min.js"></script>
	<script language="JavaScript" src="/templates/script.js"></script>
	
    <link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
    <script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
    <script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.datepicker-zh-HK.js"></script>
    <script type="text/javascript" src="/templates/kis/js/config.js"></script>
    <script type="text/javascript" src="/templates/kis/js/kis.js"></script>
    <style>
    .ui-autocomplete {max-height: 200px;max-width: 200px;overflow-y: auto;overflow-x: hidden;font-size: 12px;font-family: Verdana, "微軟正黑體";}
    .ui-autocomplete-category{font-style: italic;}
    .ui-datepicker{font-size: 12px;width: 210px;font-family: Verdana, "微軟正黑體";}
    .ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {width:auto;}
    .ui-selectable tr.ui-selecting td, .ui-selectable tr.ui-selected td{background-color: #fff7a3}
    
    @media print {
    .ui-datepicker {
        display:none;
      }
    }
    </style>
    	
	<script type="text/javascript">
		$(function(){
			kis.datepicker('#StudentDateOfBirth');
		});
	</script>
	
</head>
<body class="parent">
	<div id="container">
        <div class="top_header">
            <a href="./" class="logo" title="eClass KIS" style="background-image: url('<?=$school['logo']?>')"></a>
            <div class="school_name"><?=GET_SCHOOL_NAME()?></div>
        </div>
        <div class="board" id="module_wood_page" style="width:100%">
        	<div class="board_top"><div class="board_top_right"><div class="board_top_bg"><div class="board_top_content"></div></div></div></div>
			<div class="board_main" style="padding-left: 30px;"><div class="board_main_right" style="padding-right: 30px;"><div class="board_main_bg"><div class="board_main_content">
				<div class="main_content">	
					
					<form id="briefingForm" name="briefingForm">
    					<?php 
                        if(file_exists(dirname(__FILE__)."/{$setting_path_ip_rel}/briefing_result.php")){
                        	include_once(dirname(__FILE__)."/{$setting_path_ip_rel}/briefing_result.php");
                        }else{
    					?>
                        	<div class="admission_board" style="min-height: 150px">
                        		<h1>
                            		eAdmission 面試編排結果 <?=date('Y',getStartOfAcademicYear('',$lac->schoolYearID)) ?><br/>
                            		eAdmission Interview Arrangement Result <?=date('Y',getStartOfAcademicYear('',$lac->schoolYearID)) ?>
                        		</h1>
                    			<table class="form_table" style="font-size: 13px">
                    				<tr>
                    					<td class="field_title"><font style="color:red;">*</font>輸入申請編號<br/>Application Number</td>
                    					<td><input style="width:200px" name="ApplicationNo" type="text" id="ApplicationNo" class="textboxtext" maxlength="64" size="8"/></td>
                    				</tr>
                    				<tr>
                    					<td class="field_title"><font style="color:red;">*</font>輸入報名時填寫的身份証明文件號碼<br/>HK Birth Certification No./ HKID No./ Others</td>
                    					<td>
                        					<input style="width:200px" name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="64" size="8"/><br />
                        					不需要輸入括號，例如：A123456(7)，請輸入 "A1234567"。<br/>
                        					No need to enter the bracket, e.g. A123456(7). Please enter "A1234567".
                    					</td>
                    				</tr>
                    				<tr>
                    					<td class="field_title"><font style="color:red;">*</font>輸入出生日期 Date of Birth</td>
                    					<td><input style="width:200px" name="StudentDateOfBirth" type="text" id="StudentDateOfBirth" class="textboxtext" maxlength="10" size="15"/>(YYYY-MM-DD)</td>
                    				</tr>
                    			</table>
                            	<div class="edit_bottom">
                            		<input type="submit" name="SubmitBtn" id="SubmitBtn" class="formbutton" value="呈送 Submit" />
                            	</div>
                        	</div>
                        	<p class="spacer"></p>
    					<?php 
    					}
    					?>
                    </form>
				</div>
				<div id="step_result"></div>
			</div></div></div></div>
			<div class="board_bottom"><div class="board_bottom_right"><div class="board_bottom_bg"></div></div></div>
		</div>
        <div class="footer"><a href="http://eclass.com.hk" title="eClass"></a><span>Powered by</span></div>
	</div>
	
                        
<script>
$(function(){

	if(typeof(window.checkForm) == 'undefined'){
		window.checkForm = function(){
			if(briefingForm.ApplicationNo.value==''){
        		alert("請輸入申請編號。\nPlease enter Application Number.");	
        		briefingForm.ApplicationNo.focus();
        		return false;
        	}else if(briefingForm.StudentBirthCertNo.value==''){
        					alert("請輸入出生證明書號碼。\nPlease enter Birth Certificate Number.");	
        					briefingForm.StudentBirthCertNo.focus();
        		return false;
        	}else if(!briefingForm.StudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
        		if(briefingForm.StudentDateOfBirth.value!=''){
        			alert("日期格式不符\nInvalid Date Format");
        		}
        		else{
        			alert("請輸入出生日期。\nPlease enter Date of Birth.");	
        		}
        		
        		briefingForm.StudentDateOfBirth.focus();
        		return false;
        	}
			return true;
		}
	}
	
	$('#briefingForm').submit(function(e){
		e.preventDefault();
		
		if(window.checkForm()){
    		$.post('ajax_get_briefing_result.php', $(this).serialize(), function(res){
    			$('#step_result').html(res);
    			//$('#briefingForm').hide();
    		});
		}
	});
});
</script>
	
</body>
</html>