<?php
/*
 * Editing by Jason
 * 
 * Modificaiton Log:
 * 2018-08-10 (Ivan)
 *      - added HKPF SSO logic
 * 2015-06-18 (Jason)
 * 		- support php 5.4+ 
 */
include("includes/global.php");
if ($upgrade==1)
{
    //session_register ("iServerMaintenance");
    //$iServerMaintenance = true;
    session_register_intranet("iServerMaintenance", true);
}
else if ($upgrade==2)
{
     //$iServerMaintenance = false;
     //session_unregister("iServerMaintenance");
     session_unregister_intranet("iServerMaintenance");
}

if (is_file("$intranet_root/servermaintenance.php") && !$iServerMaintenance)
{
    header("Location: servermaintenance.php");
    exit();
}


if(!session_is_registered_intranet("UserID") || $UserID=="")
{
    ################## HKPF SSO [start] ##################
    if ($sys_custom['project']['HKPF']) {
        $logoutReferer = curPageUrl($withQueryString=false, $withPageSuffix=false).'/home/';
        $fromLogout = false;
        if ($_SERVER['HTTP_REFERER'] == $logoutReferer) {
            $fromLogout = true;
        }
        
        if ($fromLogout) {
            // do nth and show login page normally
        }
        else {
            // if have DPToken => do SSO
            
            include_once("includes/json.php");
            include_once("includes/libdb.php");
            
            intranet_opendb();
            $ldb = new libdb();
            
            ### config start
            $DPToken = trim($_GET['DPToken']);
            $AppID = 'PTTS';
            $clientIP = $_SERVER['REMOTE_ADDR'];
            $apiUrl = 'https://acamp02.hpf.gov.hk/asm/GetSession?AppID='.urlencode($AppID).'&DPToken='.urlencode($DPToken).'&ClientIP='.urlencode($clientIP);
            ### config end
            
            ### for dev ONLY [start]
//             $DPToken = 'xxx';
//             $responseJsonString = '{"Username":"ivanko_t","PolNo":"C47910","IP":"10.50.243.100"}';
            ### for dev ONLY [end]
            
            if ($DPToken != '') {
                ### for production [start]
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $apiUrl);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $responseJsonString = curl_exec($ch);
                curl_close($ch);
                ### for production [end]
                
                $jsonObj = new JSON_obj();
                $responseAry = $jsonObj->decode($responseJsonString);
                $userName = $responseAry['Username'];
                
                $sql = "SELECT HashedPass FROM INTRANET_USER Where UserLogin = '".$userName."'";
                $dataAry = $ldb->returnResultSet($sql);
                $hashPass = $dataAry[0]['HashedPass'];
                
                if ($userName != '' && $hashPass != '') {
                    $_SESSION['tmp_UserLogin'] = $userName;
                    $_SESSION['tmp_UserPassword'] = $hashPass;
                    
                    intranet_closedb();
                    header('location: ../login.php?OnlyModule=portal');
                    die();
                }
                else {
                    $_SESSION['tmp_UserLogin'] = '';
                    $_SESSION['tmp_UserPassword'] = '';
                }
            }
        }
    }
    ################## HKPF SSO [end] ##################
    
	$pos = strpos($_SERVER['HTTP_USER_AGENT'],"Windows CE");
	if ($pos === false) 
	{
	}
	else
	{
		if($plugin['attendancestudent'] || $plugin['ppc_eclass'])
	    		header("Location: /ppc/");
	}	
	
    if (is_file("$intranet_root/templates/login.customized.php"))
    {
        header("Location: templates/login.customized.php");
    }
    else
    {
        header("Location: templates/");
    }
}
else if($_SESSION["platform"] == "KIS"){
    header("Location: kis/");
}else{
    header("Location: home/");
}

?>