<?php
/*
 * 2015-04-24 (Carlos): Added password validation form and logic for flag $sys_custom['DocRouting']['RequirePasswordToSign'].
 * 2014-11-10 (Carlos): Created for redirect to Doucment Routing sign page
 */
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liburlparahandler.php");
include_once($PATH_WRT_ROOT."lang/dr_lang.$intranet_session_language.php");

intranet_opendb();

include_once($PATH_WRT_ROOT."includes/DocRouting/docRoutingConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/DocRouting/libDocRouting.php");
include_once($PATH_WRT_ROOT."includes/DocRouting/libDocRouting_ui.php");

$indexVar['libDocRouting'] = new libDocRouting();

$is_valid_token = false;
if(isset($_REQUEST['tok']) && $_REQUEST['tok']!=''){
	$sign_record = $indexVar['libDocRouting']->getSignEmailDocumentByToken($_REQUEST['tok']);
	if(count($sign_record)>0){ // it is a valid token	
		$is_valid_token = true;
		
		if($sys_custom['DocRouting']['RequirePasswordToSign'])
		{
			include_once($PATH_WRT_ROOT."includes/libauth.php");
			
			$li = new libauth();
			$lu = new libuser($sign_record[0]['UserID']);
			$UserName = ($intranet_default_lang=='en'? $lu->EnglishName : $lu->ChineseName);
			
			if(isset($_POST['LoginPassword']))
			{
				$UserLogin = $lu->UserLogin;
				$UserPassword = trim($_REQUEST['LoginPassword']);
				
				// validate user
				if ((isset($UseLDAP) && $UseLDAP) || $intranet_authentication_method == 'LDAP')
				{
					include_once($PATH_WRT_ROOT."includes/libldap.php");
				    # Different users using different LDAP dn string
				    if ($ldap_user_type_mode)
				    {
				        # Get User Type
				        $sql = "SELECT UserID, RecordType FROM INTRANET_USER WHERE UserLogin = '".$UserLogin."'";
				        $temp = $lu->returnArray($sql,2);
				
				        list($t_id, $t_user_type) = $temp[0];
				
				        if (!is_array($temp) || sizeof($temp)==0 || $t_user_type == "" || $t_user_type < 1 || $t_user_type > 4)
				        {
				             $url = ($url=="") ? "/" : $url;
				             header("Location: $url");
				             exit();
				        }
						
				        switch ($t_user_type)
				        {
				                case 1: $special_ldap_dn = $ldap_teacher_base_dn; break;
				                case 2: $special_ldap_dn = $ldap_student_base_dn; break;
				                case 3: $special_ldap_dn = $ldap_parent_base_dn; break;
				                default: $special_ldap_dn = $ldap_teacher_base_dn;
				        }
				    }
				
				    $lldap = new libldap();
				    $lldap->connect();
				
				    if (!$lldap->validate_try_all_ou($UserLogin,$UserPassword) && !$lldap->validate($UserLogin,$UserPassword))
				    {
				        //$ID = 0;
				        if(isset($_SESSION['DR_TMP_USERID']))
				        	session_destroy($_SESSION['DR_TMP_USERID']);
				        if(isset($_SESSION['DR_TMP_USERTYPE']))
				        	session_destroy($_SESSION['DR_TMP_USERTYPE']);
				        $error_msg = $Lang['eClassAPI']['WarningMsg']['InvalidPassword'];
				    }else{
				    	$_SESSION['DR_TMP_USERID'] = $lu->UserID;
				    	$_SESSION['DR_TMP_USERTYPE'] = $lu->RecordType;
				    }
				}else{
					$ID = $li->validate($UserLogin, $UserPassword);
					if($ID > 0){
						$_SESSION['DR_TMP_USERID'] = $lu->UserID;
				    	$_SESSION['DR_TMP_USERTYPE'] = $lu->RecordType;
					}else{
						if(isset($_SESSION['DR_TMP_USERID']))
				        	session_destroy($_SESSION['DR_TMP_USERID']);
				        if(isset($_SESSION['DR_TMP_USERTYPE']))
				        	session_destroy($_SESSION['DR_TMP_USERTYPE']);
				        $error_msg = $Lang['eClassAPI']['WarningMsg']['InvalidPassword'];
					}
				}
			}
		}
		
		if($sys_custom['DocRouting']['RequirePasswordToSign'] && (!isset($_SESSION['DR_TMP_USERID']) || !isset($_SESSION['DR_TMP_USERTYPE'])))
		{
			// login form
			/*
			$x = '<html>
					<head>
						<META http-equiv="Content-Type"  content="text/html" Charset="'.returnCharset().'"  />
						<style type="text/css">
							body {font-family: Verdana, "微軟正黑體"; font-size:12px;}
							div {padding:4px;}
							div#center_div {position:relative;left:50%;top:50%;width:500px;height:200px;margin-left:-250px;margin-top:-100px;}
							.error_msg {color:#FF0000;}
							.login_btn {
							  display: block;
							  border: 0;
							  background-image: url(/images/btn_bg.gif);
							  height: 26px;
							  color: #666666;
							  line-height: 26px;
							  font-weight: bold;
							  text-align: center;
							  width: 61px;
							  left: 310px;
							  top: 107px;
							}
							.login_btn:hover {background-image:url(/images/btn_bg_on.gif);color:#FF0000;}
						</style>
					</head>
					<body>
						<div style="width:100%;height:100%;">
						<div id="center_div">
							<form id="LoginForm" name="LoginForm" method="post">
								<div><h3>'.$Lang['DocRouting']['WarningMsg']['RequestEnterPasswordToSign'].'</h3></div>
								<div>'.$Lang['DocRouting']['UserName'].': '.$UserName.'</div>
								<div>'.$Lang['DocRouting']['Password'].': <input type="password" name="LoginPassword" value="" /></div>
								<div class="error_msg" '.($error_msg!=''?'':'style="display:none;"').'>'.$error_msg.'</div>
								<div><input type="submit" name="submitBtn" value="'.$Lang['LoginPage']['Login'].'" class="login_btn" /></div>
								<input type="hidden" name="tok" value="'.$_REQUEST['tok'].'" />
							</form>
						</div>
						</div>
					</body>
				</html>';
			*/
			
			$MODULE_OBJ['title'] = $Lang['Header']['Menu']['DocRouting'];
			$linterface = new interface_html("popup.html");
			$linterface->LAYOUT_START();

				$x .= '<form id="LoginForm" name="LoginForm" method="post">
						<table width="640px" border="0" cellspacing="0" cellpadding="5" align="center">
							<tr valign="top">
								<td width="100%" valign="top" nowrap="nowrap" colspan="2"><h3>'.$Lang['DocRouting']['WarningMsg']['RequestEnterPasswordToSign'].'</h3></td>
							</tr>
							<tr>
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">'.$Lang['DocRouting']['UserName'].': </td>
								<td class="tabletext">'.$UserName.'</td>
							</tr>
							<tr>
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">'.$Lang['DocRouting']['Password'].': </td>
								<td class="tabletext"><input type="password" id="LoginPassword" name="LoginPassword" value="" />
									<span style="color:#FF0000;" '.($error_msg!=''?'':'style="display:none;"').'>'.$error_msg.'</span>
								</td>
							</tr>
							<tr>
								<td class="dotline" colspan="2"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td>
							</tr>
							<tr>
								<td colspan="2" align="center">
									<input type="submit" name="submitBtn" value="'.$Lang['LoginPage']['Login'].'" class="formbutton_v30" />
									<input type="hidden" name="tok" value="'.$_REQUEST['tok'].'" />
								</td>
							</tr>
						</table>
						</form>';
				
				$x .= '<script type="text/javascript" language="JavaScript">
						$(document).ready(function(){$("#LoginPassword").focus();});
						</script>';
				
				echo $x;
				
			$linterface->LAYOUT_STOP();
		}else{
			$pe_link = $indexVar['libDocRouting']->getEncryptedParameter('management','sign_document',array('token'=>$_REQUEST['tok']));
			$link = "http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"")."/home/eAdmin/ResourcesMgmt/DocRouting/index.php?pe=".$pe_link."&tok=".$_REQUEST['tok'];
			intranet_closedb();
			header("Location: $link");
			exit;
		}
	}
}

if(!$is_valid_token){
	echo $Lang['DocRouting']['WarningMsg']['InvalidToken'];
}
intranet_closedb();
?>