<?php
// Editing by 
/*
 * 2019-05-24 Carlos: Ban certain file types that may have security risk, e.g. .shtml, .shtm, .stm
 */
ini_set("memory_limit","300M");
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

echo "<META http-equiv=Content-Type content='text/html; charset=utf-8'>";

$lu = new libuser($UserID);
if ($SessionKeyStr != $lu->sessionKey)
{
    header("Location: $intranet_httppath/home/school/close.php");
    exit();
}

$MODULE_OBJ['title'] = $i_frontpage_campusmail_attachment;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$no_file = 5;
$li = new libfilesystem();
$personal_path = "$file_path/file/mail/u$UserID";
if (!is_dir($personal_path))
{
    $li->folder_new($personal_path);
}

$forbidden_file_types = array('.shtml','.shtm','.stm');

$path = "$file_path/file/mail/$folder";

#$path = "$file_path/file/mail/$folder"."tmp";

$li->folder_new($path);

# remove user deleted files
$lremove = new libfiletable("", $path, 0, 0, "");
$files = $lremove->files;

$attachStr=stripslashes($attachStr);
while (list($key, $value) = each($files)) {
	$encoded_name = urlencode($files[$key][0]);
	//$encoded_name = $files[$key][0];
     if(!strstr($attachStr,$encoded_name)){
          $li->file_remove($path."/".$files[$key][0]);
     }
}

$lc = new libcampusquota($UserID);
$totalQuota = $lc->returnQuota() * 1024;
$usedQuota = $lc->returnUsedQuota();

$leftQuota = $totalQuota - $usedQuota;
$allSucceeded = true;
for($i=0;$i<$no_file;$i++){
     $loc = ${"userfile".$i};
     #$file = ${"userfile".$i."_name"};
     $file = stripslashes(${"hidden_userfile_name$i"});
     $lc_file_ext_with_dot = strtolower(substr($file, strrpos($file,".")));
     if(in_array($lc_file_ext_with_dot,$forbidden_file_types)){
     	continue;
     }
     
     $des = "$path/".$file;
     if (!is_file($loc))
     {
          continue;
     }
     $currSize = filesize($loc) /1024;
     if ($totalQuota != 0 && $currSize > $leftQuota)
     {
         $allSucceeded = false;
         break;
     }
     if($loc=="none"){
     } else {
          if(strpos($file, ".")==0){
          }else{
               $li->lfs_copy($loc, $des);
               $leftQuota -= $currSize;
          }
     }
}
$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;

$msg = ($allSucceeded? $i_Campusquota_uploadSuccess:$i_Campusquota_noQuota);
/*
if ($allSucceeded)
{
	$msg = $linterface->GET_SYS_MSG("", "<font color='green'>".$i_Campusquota_uploadSuccess."</font>");
} else {
	$msg = $linterface->GET_SYS_MSG("", $i_Campusquota_noQuota);
}
*/

?>


<span id="hidden_filename" style='visibility:hidden'></span>
<script language="JavaScript1.2">
function format_filename(filename){
	return filename.replace(/&amp;/g,'&');
}
par = opener.window;
obj = opener.window.document.form1.elements["Attachment[]"];
obj_realname = opener.window.document.form1.elements["real_name[]"];

function fileExists(obj,name){
	for(i=0;i<obj.options.length;i++){
		v = obj.options[i].value;
		if(v==name){
			return true;
		}
	}
	return false;
}
//par.checkOptionClear(obj);
if(obj.options.length>=1)
	obj.options[obj.options.length-1]=null;
<?php 
	while (list($key, $value) = each($files)) 
	{
		$file_name = $files[$key][0];
		//$encoded_name = $files[$key][0];
		$encoded_name = urlencode($files[$key][0]);
		echo "objFile= document.getElementById('hidden_filename');";
		echo "objFile.innerHTML='".str_replace("'","&#039;",$file_name)."';";
		echo "file_name = format_filename(objFile.innerHTML);";
		//echo "par.checkOptionAdd(obj, \"".addslashes($file_name)." (".ceil($files[$key][1]/1000)."Kb".")\", \"".$files[$key][0]."\");\n"; 
		echo "if(!fileExists(obj,'$encoded_name')){";
		echo " par.checkOptionAdd(obj, file_name+\" (".ceil($files[$key][1]/1000)."Kb".")\", \"".$encoded_name."\");\n"; 
		echo "}";
		echo "if(!fileExists(obj_realname,'$encoded_name')){";
		echo " par.checkOptionAdd(obj_realname, file_name, \"".$encoded_name."\");\n"; 
		echo "}";

		echo "objFile.innerHTML='';";

	 }	 
	 if (count($files) >0)
	 {
		 echo " par.jCHECK_ATTACHMENT();\n"; 
	 }
	 
?>
par.checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");

</script>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">	
	<tr>
		<td align="center" class="tabletext" >
		<?=$msg?>
		</td>
	</tr>
	</table>
	</td>
</tr>

<tr>
	<td>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<?php
	$linterface->LAYOUT_STOP();
?>