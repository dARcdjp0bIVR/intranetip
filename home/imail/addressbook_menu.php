<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccess.php");
include_once("../../includes/libwebmail.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
 $anc = $special_option['no_anchor'] ?"":"#anc";
$lwebmail = new libwebmail();
if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
{
    include_once("../../includes/libsystemaccess.php");
    $lsysaccess = new libsystemaccess($UserID);
    if ($lsysaccess->hasMailRight())
    {
        $noWebmail = false;
    }
    else
    {
        $noWebmail = true;
    }
}
else
{
    $noWebmail = true;
}
?>

<?php
$img_on_right ="<img src='$image_path/frontpage/imail/tab_menu_on_right.gif'>";
$img_off_right="<img src='$image_path/frontpage/imail/tab_menu_off_right.gif'>";

$tab_internal_group = ($TabID==1)?"tabon":"taboff";
$img_internal_group = ($TabID==1)?$img_on_right:$img_off_right;
$link_internal_group= ($TabID==1)?"$i_CampusMail_Internal_Recipient_Group":"<a href='groupalias.php?aliastype=0&TabID=1$anc'>$i_CampusMail_Internal_Recipient_Group</a>";

$tab_external_recipient = ($TabID==2)?"tabon":"taboff";
$img_external_recipient = ($TabID==2)?$img_on_right:$img_off_right;
$link_external_recipient= ($TabID==2)?"$i_CampusMail_External_Recipient":"<a href='addressbook.php?TabID=2$anc'>$i_CampusMail_External_Recipient</a>";

$tab_external_group = ($TabID==3)?"tabon":"taboff";
$img_external_group = ($TabID==3)?$img_on_right:$img_off_right;
$link_external_group = ($TabID==3)?"$i_CampusMail_External_Recipient_Group":"<a href='groupalias.php?aliastype=1&TabID=3$anc'>$i_CampusMail_External_Recipient_Group</a>";
?>
<style type="text/css">
<!--
.tabon {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 13px;
	font-weight: bold;
	text-decoration: underline;
	vertical-align: middle;
	background:url(/images/frontpage/imail/tab_menu_on_bg.gif);
}
.taboff {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #663300;
	text-decoration: none;
	vertical-align: middle;
	background:url(/images/frontpage/imail/tab_menu_off_bg.gif);
}
.taboff:hover {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #663300;
	text-decoration: underline;
	vertical-align: middle;
	background:url(/images/frontpage/imail/tab_menu_off_bg.gif);
}

-->
</style>
<a name=anc></a>
<TABLE cellSpacing=0 cellPadding=0 width=685 border=0>
  <TBODY>
    <TR> 
      <TD> <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td><IMG height=10 src="<?=$image_path?>/frontpage/imail/papertop2.gif" 
          width=685></td>
          </tr>
          <tr> 
            <td><table background="<?=$image_path?>/frontpage/imail/tab_menu_bg.gif" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="8"><img src="<?=$image_path?>/frontpage/imail/tab_menu_left.gif" width="8" height="32"></td>
                  <td><table border="0" cellpadding="0" cellspacing="0">
                      <tr> 
                        <!-- Internal -->
                        <td width="8" class="<?=$tab_internal_group?>"><img src="<?=$image_path?>/frontpage/imail/spacer.gif" width="8" height="32"></td>
                        <td class="<?=$tab_internal_group?>"><?=$link_internal_group?></td>
						<?php if(!$noWebmail){?>
	                        <td width="26" class="<?=$tab_external_group?>"><?=$img_internal_group?></td>
	                        <!-- External Group -->
	                        <td class="<?=$tab_external_group?>"><img src="<?=$image_path?>/frontpage/imail/spacer.gif" width="8" height="32"></td>
	                        <td class="<?=$tab_external_group?>"><?=$link_external_group?></td>
	                        <td width="26" class="<?=$tab_external_recipient?>"><?=$img_external_group?></td>
	                        
	                        <!-- External Recipient-->
	                        <td width="8" class="<?=$tab_external_recipient?>"><img src="<?=$image_path?>/frontpage/imail/spacer.gif" width="8" height="32"></td>
	                        <td class="<?=$tab_external_recipient?>"><?=$link_external_recipient?></td>
	                        <td width="32"><?=$img_external_recipient?></td>
                    	<?php }
                    		else{                 	?>
	                        	<td width="32"><?=$img_internal_group?></td>
                    		<?}?>
                      </tr>
                    </table>
                    </td>
                  <td width="8"><img src="<?=$image_path?>/frontpage/imail/spacer.gif" width="8" height="32"></td>
                </tr>
              </table></td>
          </tr>
        </table></TD>
    </TR>
  </TBODY>
</TABLE>

<!--<img src=/images/campusmail/papertop.gif width=685 height=10>-->
