<?php
// editing by 
/********************************************************* Change Log ************************
 * 2011-03-02 [Carlos]: add alphabetical sorting order to group title and user englishname.
 *********************************************************************************************/
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $i_CampusMail_New_SelectFromAlias;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$li = new libdb();


# List all alias
$MethodTypeArr = array();
$MethodTypeArr[] = array(1,$i_admintitle_us_user);
$MethodTypeArr[] = array(2,$i_admintitle_us_group);
$x2 = getSelectByArray($MethodTypeArr," name='MethodType' onchange='this.form.submit()' ", $MethodType);

$sql = "	SELECT 
				AliasID, AliasName 
			FROM 
				INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL 
			WHERE 
				OwnerID = '".$li->Get_Safe_Sql_Query($UserID)."' 
			ORDER BY 
				AliasName
		";
$alias_array2 = $li->returnArray($sql,2);
if (count($alias_array2) > 0)
{
	$HasAlias = true; 
}

// For new interface
$x1  = "<select name='AliasID[]' size='6' multiple >\n";
for ($i=0; $i<count($alias_array2); $i++)
{
	list ($id, $name) = $alias_array2[$i];
	if (is_array($AliasID) && count($AliasID)>0)
	{
		if (in_array($id,$AliasID))
		{
			$sel_str = " selected='selected' ";	
		} else {
			$sel_str = " ";	
		}
	}	else {
		$sel_str = " ";	
	}
	$x1 .= "<option value='$id' $sel_str>$name</option>\n";
}
$x1 .= "</select>\n";


if (is_array($AliasID) && count($AliasID)>0)
{
	$AliasImplode = implode(",",IntegerSafe($AliasID));
	
    $sql = "	SELECT 
    				DISTINCT  CONCAT('G',a.TargetID),b.Title 
    			FROM 
    				INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as a
				LEFT OUTER JOIN 
					INTRANET_GROUP as b 
				ON 
					a.TargetID = b.GroupID
				WHERE 
					a.AliasID IN ($AliasImplode) AND a.RecordType = 'G' 
				ORDER BY 
					b.Title 
			";
    $groups2 = $li->returnArray($sql,2);    	        
	
    $namefield = getNameFieldWithClassNumberByLang("b.");
    $sql = "	SELECT 
    				DISTINCT CONCAT('U',a.TargetID),$namefield 
    			FROM 
    				INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as a
				LEFT OUTER JOIN 
					INTRANET_USER as b 
				ON 
					a.TargetID = b.UserID
				WHERE 
					a.AliasID IN ($AliasImplode) AND a.RecordType = 'U'
				ORDER BY 
					b.EnglishName ";
    $users2 = $li->returnArray($sql,2);	
    
    $x3_text = $i_admintitle_us_group." / " .$i_admintitle_us_user;
    
	if (($curAction == "add") && ($addAction == "alias"))
	{
		$TotalArr = array();
	} 
	else 
	{    
	    $TotalArr = $groups2;
	    for ($i=0;$i<count($users2);$i++)
	    {
		    $TotalArr[] = $users2[$i];
	    }
    }
    $x3 = getSelectByArray($TotalArr," name='AliasUserGroupID[]' multiple size='10' ", "",0,1);
	$x3_button = $linterface->GET_BTN($button_add, "button", "jADD_USER_GROUP()","add1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ");
	$x3_button2 = $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['AliasUserGroupID[]'])","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ");
}

if ($curAction == "add")
{
	if ($addAction == "alias")
	{
		if (is_array($AliasID) && count($AliasID)>0)
		{		    
		    $AliasImplode = implode(",",IntegerSafe($AliasID));
		    
		    $namefield = getNameFieldWithClassNumberByLang("b.");
		    $sql = "SELECT DISTINCT a.TargetID,$namefield FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as a
		                   LEFT OUTER JOIN INTRANET_USER as b ON a.TargetID = b.UserID
		                   WHERE a.AliasID IN ($AliasImplode) AND a.RecordType = 'U' 
						   ORDER BY b.EnglishName ";
		    $users = $li->returnArray($sql,2);
		
		
		    $sql = "SELECT DISTINCT a.TargetID,b.Title FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as a
		                   LEFT OUTER JOIN INTRANET_GROUP as b ON a.TargetID = b.GroupID
		                   WHERE a.AliasID IN ($AliasImplode) AND a.RecordType = 'G' 
						   ORDER BY b.Title ";
		    $groups = $li->returnArray($sql,2);
		}		
		
	}	     
	else if ($addAction == "user")
	{
		if (is_array($AliasUserID) && count($AliasUserID)>0)
		{		    
			$AliasUserImplode = implode(",",IntegerSafe($AliasUserID));
			
			$namefield = getNameFieldWithClassNumberByLang("");
			$sql = "
						SELECT
							UserID, $namefield
						FROM 
							INTRANET_USER
						WHERE 
							UserID IN ($AliasUserImplode) 
						ORDER BY 
							EnglishName 
					";
	
			$users = $li->returnArray($sql,2);					
		}		
	}
	else if ($addAction == "group")
	{
		if (is_array($AliasGroupID) && count($AliasGroupID)>0)
		{		    
			$AliasGroupImplode = implode(",",IntegerSafe($AliasGroupID));
			
		    $sql = "	SELECT 
		    				GroupID, Title
		    			FROM
		    				INTRANET_GROUP
		    			WHERE	
		    				GroupID IN ($AliasGroupImplode)
		                ORDER BY 
							Title ";
		    $groups = $li->returnArray($sql,2);					
		}		
	}	
	else if ($addAction == "user_group")
	{
		$UserCnt = 0;
		$UserImplode = "";
		$GroupCnt = 0;
		$GroupImplode = "";
		
		for ($i=0;$i<count($AliasUserGroupID);$i++)
		{
			$TmpType = substr($AliasUserGroupID[$i],0,1);
			if ($TmpType == "U")
			{
				if ($UserCnt==0)
				{
					$UserImplode .= IntegerSafe(substr($AliasUserGroupID[$i],1));
				} else {
					$UserImplode .= ",".IntegerSafe(substr($AliasUserGroupID[$i],1));
				}
				$UserCnt++;
			} 
			else if ($TmpType == "G")
			{
				if ($GroupCnt==0)
				{
					$GroupImplode .= IntegerSafe(substr($AliasUserGroupID[$i],1));
				} else {
					$GroupImplode .= ",".IntegerSafe(substr($AliasUserGroupID[$i],1));
				}
				$GroupCnt++;
			} 			
		}
		
		if ($GroupImplode != "")
		{
		    $sql = "	SELECT 
		    				GroupID, Title
		    			FROM
		    				INTRANET_GROUP
		    			WHERE	
		    				GroupID IN ($GroupImplode)
		                ORDER BY
							Title ";
		    $groups = $li->returnArray($sql,2);		
	    }

	    if ($UserImplode != "")
	    {
		    $namefield = getNameFieldWithClassNumberByLang("");
			$sql = "
						SELECT
							UserID, $namefield
						FROM 
							INTRANET_USER
						WHERE 
							UserID IN ($UserImplode)
						ORDER BY 
							EnglishName ";
			$users = $li->returnArray($sql,2);		
		}	    	    
	}	
	
	$curAction = "";		
	$addAction = "";
}

if ((sizeof($users) >0) || (sizeof($groups)>0))
{
	if ($fieldname == "Recipient[]")
	{
		$ExtraJS = " par.displayTable('internalToTextDiv','block'); \n";
		$ExtraJS .= " par.displayTable('internalToRemoveBtnDiv','block'); \n";	
	}
	else if ($fieldname == "InternalCC[]")
	{
		$ExtraJS = " par.displayTable('internalCCTextDiv','block'); \n";
		$ExtraJS .= " par.displayTable('internalCCRemoveBtnDiv','block'); \n";	
	}
	else if ($fieldname == "InternalBCC[]")
	{
		$ExtraJS = " par.displayTable('internalBCCTextDiv','block'); \n";
		$ExtraJS .= " par.displayTable('internalBCCRemoveBtnDiv','block'); \n";	
	}
	
}

?>
<script language="javascript">
	par = opener.window;
	parObj = opener.window.document.form1.elements["<?php echo escape_double_quotes($fieldname); ?>"];
	par.checkOption(parObj);

	<?php
	for ($i=0; $i<sizeof($users); $i++)
	{
		list($id, $name) = $users[$i];
	?>
		par.checkOptionAdd(parObj, '<?=$name?>', 'U<?=$id?>');
	<?php
	}

	for ($i=0; $i<sizeof($groups); $i++)
	{
		list($id, $name) = $groups[$i];
	?>
          par.checkOptionAdd(parObj, '<?=$name?>', 'G<?=$id?>');
	<?php
     }
     ?>

     <?=$ExtraJS?>
//     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");

	function SelectAll(obj)
	{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
	}

     function jADD_USER()
     {	     
	     document.form1.curAction.value = "add";
	     document.form1.addAction.value = "user";	     
	     document.form1.submit();	     
     }
     
     function jADD_GROUP()
     {
	     document.form1.curAction.value = "add";
	     document.form1.addAction.value = "group";	     
	     document.form1.submit();	     
     }

     function jADD_USER_GROUP()
     {	     
	     document.form1.curAction.value = "add";
	     document.form1.addAction.value = "user_group";	     
	     document.form1.submit();	     
     }
          
     function jEXPAND_ALIAS()
     {
	     document.form1.curAction.value = "expand";
	     document.form1.expandAction.value = "alias";	     
	     document.form1.submit();
     }

     function jADD_ALIAS()
     {
	     document.form1.curAction.value = "add";
	     document.form1.addAction.value = "alias";	     
	     document.form1.submit();
     }
          
     if(parObj){
     	SelectAll(parObj);
     }
</script>

<form name="form1" action="alias_in.php" method="post" class="tabletext">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">	
	<tr>
		<td>

		<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tabletext" >
		<tr>
 			<tr>
			<td valign="top" nowrap="nowrap" width="30%" >
			<span class="tabletext"><?=$i_CampusMail_New_AliasGroup?>:</span>
			</td>
			<td >					
			<table cellpadding="0" cellspacing="0" >
			<tr>
				<td><?=$x1?></td>
				<td style="vertical-align:bottom">
				
				<table cellpadding="0" cellspacing="6" >				
				<tr>
					<td>					
					<?= $linterface->GET_BTN($button_add, "button", "jADD_ALIAS()","add1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
					</td>
				</tr>	
				<tr>
					<td>					
					<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "button", "jEXPAND_ALIAS()","expand1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
					</td>
				</tr>					
				<tr>
					<td>
					<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['AliasID[]'])","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ") ?>
					</td>
				</tr>									
				</table>				
				</td>
			</tr>
			</table>			
			</td>
		</tr>	

					
		<?php 
		if ((is_array($AliasID) && count($AliasID)>0) && !(($curAction == "add") && ($addAction == "alias")))
		{ 
		?>		
		<tr> 
			<td height="1" colspan="2" class="dotline" valign="middle" >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
			</td>
		</tr>
		<tr> 
			<td height="5" colspan="2"  >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
			</td>
		</tr>
		
		<tr> 			
			<td valign="top" nowrap="nowrap" width="30%" >
			<span class="tabletext"><?=$x3_text?>:</span>
			</td>
			<td >					
			<table cellpadding="0" cellspacing="0" >
			<tr>
				<td><?=$x3?></td>
				<td style="vertical-align:bottom">
				
				<table cellpadding="0" cellspacing="6" >				
				<tr>
					<td>
					<?=$x3_button?>
					</td>
				</tr>	
				<tr>
					<td>
					<?=$x3_button2?>
					</td>
				</tr>					
				</table>
				
				</td>
			</tr>
			</table>			
			</td>
		</tr>				
		<?php 
		} 
		?>
		</table>
		
		</td>
	</tr>		
	</table>   	      
	</td>
</tr>
<tr>
	<td>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
		
<br />

<input type="hidden" name="fieldname" value="<?php echo escape_double_quotes($fieldname); ?>">
<input type="hidden" name="curAction" value="" >
<input type="hidden" name="expandAction" value="<?=escape_double_quotes($expandAction)?>">
<input type="hidden" name="addAction" value="" >

</form>

<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>