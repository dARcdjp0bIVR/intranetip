<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
include_once("../../../../templates/fileheader.php");

$li = new libdb();
# get blocked groups and blocked usertypes
$sql ="SELECT a.RecordType,a.Teaching,c.ClassLevelID FROM INTRANET_USER AS a 
	LEFT OUTER JOIN INTRANET_CLASS AS b ON(a.ClassName=b.ClassName) 
	LEFT OUTER JOIN INTRANET_CLASSLEVEL AS c ON (b.ClassLevelID=c.ClassLevelID) 
	WHERE a.UserID='$UserID'";
	
$temp = $li->returnArray($sql,3);
list($usertype,$teaching,$class_level_id) = $temp[0];

$sql_blocked = "SELECT BlockedGroupCat,BlockedUserType FROM INTRANET_IMAIL_RECIPIENT_RESTRICTION WHERE TargetType='$usertype'";
if($usertype==1)
	$sql_blocked.=" AND Teaching='$teaching'";
else if($usertype==2)
	$sql_blocked.=" AND ClassLevel='$class_level_id'";
$result_blocked = $li->returnArray($sql_blocked,2);

list($blocked_groups,$blocked_usertypes) = $result_blocked[0];

$blocked_usertypes= explode(",",$blocked_usertypes);

# end get blocked groups / usertypes

$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
$content = explode("\n",$file_content);
# Get row 1,2,4 only. Compatible with previous version
if ($content[0][0]==1) $permitted[] = 1;
if ($content[1][0]==1 && !in_array(2,$blocked_usertypes)) $permitted[] = 2;
if ($content[3][0]==1 && !in_array(3,$blocked_usertypes)) $permitted[] = 3;
$permitted_list = implode(",",$permitted);

$teacherStaffAllowed = ($content[0][0]==1 && !in_array(1,$blocked_usertypes))?true:false;
$nonTeacherStaffAllowed = ($content[0][0]==1 && !in_array(5,$blocked_usertypes))?true:false;



?>
<form name=form1 action="search.php" method=post>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupgreen_t_l.gif" width="27" height="58"></td>
    <td class="imail_popup_green_cell_t"><img src="<?=$image_path?>/frontpage/imail/popup_choose_recipients_<?=$intranet_session_language?>.gif"></td>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupgreen_t_r.gif" width="27" height="58"></td>
  </tr>
</table>


<?


if ($flag == 1)     # Searching
{
	$teaching_conds="";
	if($teacherStaffAllowed === $nonTeacherStaffAllowed){
		$teaching_conds = "";
	}else if($teacherStaffAllowed){
		$teaching_conds = " AND Teaching=1 ";
	}else if($nonTeacherStaffAllowed){
		$teaching_conds = " AND (Teaching!=1 OR Teaching IS NULL) ";
	}
    $namefield = getNameFieldWithClassNumberByLang("");
    $keyword = trim($keyword);
    $sql = "SELECT DISTINCT UserID, $namefield FROM INTRANET_USER WHERE RecordType IN ($permitted_list) $teaching_conds
                   AND (EnglishName LIKE '%$keyword%' OR
                        ChineseName LIKE '%$keyword%' OR
                        UserLogin LIKE '%$keyword%' OR
                        ClassName LIKE '%$keyword%' OR
                        ClassNumber LIKE '%$keyword%')";
    $users = $li->returnArray($sql,2);
    $next_flag = 2;

?>
<SCRIPT language=Javascript>
function addSearchedParent()
{
         document.form1.targetToParent.value = 1;
         return checkRestore(document.form1,'target_user[]','search.php');
}
function addSearchedUser()
{
         document.form1.targetToParent.value = 0;
         return checkRestore(document.form1,'target_user[]','search.php');
}
</SCRIPT>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="15" class="imail_popup_green_cell_l"><img src="<?=$image_path?>/frontpage/imail/popupgreen_cell_l.gif" width="15" height="10"></td>
      <td bgcolor="#DDEF5D">
        <a class=functionlink_new href=search.php?fieldname=<?=$fieldname?>><?=$i_general_search_again?></a>
      </td>
      <td class=td_right_middle bgcolor="#DDEF5D">
        <a href=javascript:addSearchedParent()>
          <image src="<?=$image_path?>/btn_select_parent_<?=$intranet_session_language?>.gif" alt='<?=$i_general_select_parent?>' border=0>
        </a>
        <a href=javascript:addSearchedUser()>
          <image src="<?=$image_path?>/btn_add_<?=$intranet_session_language?>.gif" border=0>
        </a>
      </td>
      <td width="15" class="imail_popup_green_cell_r"><img src="<?=$image_path?>/frontpage/imail/popupgreen_cell_r.gif" width="15" height="10"></td>
    </tr>
</table>
<table width=100% border=0 cellspacing=0 cellpadding=0>
    <tr>
      <td width="15" class="imail_popup_green_cell_l"><img src="<?=$image_path?>/frontpage/imail/popupgreen_cell_l.gif" width="15" height="10"></td>
      <td class=td_right_middle bgcolor="#DDEF5D"><table width=100% border=1 cellpadding="0" cellspacing="0" bordercolorlight="#FFFFFF" bordercolordark="#DDEF5D" bgcolor="#F2F8CA" class="body">
<tr><td><?=$i_adminmenu_user?></td><td><input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'target_user[]'):setChecked(0,this.form,'target_user[]')></td></tr>
<?
for ($i=0; $i<sizeof($users); $i++)
{
     list($id,$name) = $users[$i];
     echo "<tr><td>$name</td><td><input type=checkbox name=target_user[] value='$id'></td></tr>\n";
}
?>
</table></td>
      <td width="15" class="imail_popup_green_cell_r"><img src="<?=$image_path?>/frontpage/imail/popupgreen_cell_r.gif" width="15" height="10"></td>
    </tr>
</table>
<?

}
else if ($flag == 2)   # Add to compose window
{
     if (is_array($target_user) && sizeof($target_user)>0)
     {
         $target_list = implode(",", $target_user);
         $namefield = getNameFieldWithClassNumberByLang("");
         $sql = "SELECT DISTINCT UserID, $namefield FROM INTRANET_USER WHERE RecordType IN ($permitted_list)
                        AND UserID IN ($target_list)";
         $users = $li->returnArray($sql,2);
         $suf_parent = $i_general_targetParent;
     ?>
<script language="javascript">
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];
     par.checkOption(parObj);

     <?
     if ($targetToParent == 1)
     {
         for ($i=0; $i<sizeof($target_user); $i++)
         {
              list($id, $name) = $users[$i];
              ?>
              par.checkOptionAdd(parObj, '<?=$name.$suf_parent?>', 'P<?=$id?>');
              <?
         }
     }
     else
     {
         for ($i=0; $i<sizeof($target_user); $i++)
         {
              list($id, $name) = $users[$i];
              ?>
              par.checkOptionAdd(parObj, '<?=$name?>', 'U<?=$id?>');
              <?
         }
     }
     ?>
              par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");

     <?
     }
     $next_flag = 0;
     ?>
     location.href = "search.php?fieldname=<?=$fieldname?>";
</script>

     <?
}
else     # Input components
{
     $next_flag = 1;
     ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="15" class="imail_popup_green_cell_l"><img src="<?=$image_path?>/frontpage/imail/popupgreen_cell_l.gif" width="15" height="10"></td>
      <td  bgcolor="#DDEF5D">
        <input type=text name=keyword size=40 maxlength=255> <input type=image src="<?=$image_path?>/btn_search_<?=$intranet_session_language?>.gif" align=absmiddle>
      </td>
      <td width="15" class="imail_popup_green_cell_r"><img src="<?=$image_path?>/frontpage/imail/popupgreen_cell_r.gif" width="15" height="10"></td>


    </tr>
</table>

<?
}

?>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupgreen_b_l.gif" width="27" height="30"></td>
    <td class="imail_popup_green_cell_b">&nbsp;</td>
    <td width="27"><img src="<?=$image_path?>/frontpage/imail/popupgreen_b_r.gif" width="27" height="30"></td>
  </tr>
</table>


<input type=hidden name=fieldname value="<?php echo $fieldname; ?>">
<input type=hidden name=flag value="<?=$next_flag?>">
<input type=hidden name=targetToParent value=0>
</form>

<?php
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>