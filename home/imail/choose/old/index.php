<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $button_select.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_ListAlias;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");

if ($file_content == "")
{
    $permitted = array(1,2,3);
}
else{
	$content = explode("\n",$file_content);
	$content2[] = explode(":",$content[0]);
    $content2[] = explode(":",$content[1]);
    $content2[] = explode(":",$content[3]);
	# Get row 1,2,4 only. Compatible with previous version
	if ($content2[0][0]==1) $permitted[] = 1;
	if ($content2[1][0]==1) $permitted[] = 2;
	if ($content2[2][0]==1) $permitted[] = 3;
}


/*
$count = 0;
for ($i=0; $i<sizeof ($content); $i++)
{
     $settings = split(":",$content[$i]);
     $access_right[$i] = $settings[0];
     if ($settings[0]==1)
     {
         $permitted[$count] = $i+1;
         $count++;
     }
}
*/


$li = new libgrouping();

if($CatID < 0){
     unset($ChooseGroupID);
     $ChooseGroupID[0] = 0-$CatID;
     if($CatID==-5)
     	$chooseGroupID[0]=1;
}

# get blocked groups and blocked usertypes
$sql ="SELECT a.RecordType,a.Teaching,c.ClassLevelID FROM INTRANET_USER AS a
	LEFT OUTER JOIN INTRANET_CLASS AS b ON(a.ClassName=b.ClassName)
	LEFT OUTER JOIN INTRANET_CLASSLEVEL AS c ON (b.ClassLevelID=c.ClassLevelID)
	WHERE a.UserID='$UserID'";

$temp = $li->returnArray($sql,3);
list($usertype,$teaching,$class_level_id) = $temp[0];

$sql_blocked = "SELECT BlockedGroupCat,BlockedUserType FROM INTRANET_IMAIL_RECIPIENT_RESTRICTION WHERE TargetType='$usertype' AND Restricted!=1";
if($usertype==1)
	$sql_blocked.=" AND Teaching='$teaching'";
else if($usertype==2)
	$sql_blocked.=" AND ClassLevel='$class_level_id'";
$result_blocked = $li->returnArray($sql_blocked,2);

list($blocked_groups,$blocked_usertypes) = $result_blocked[0];

$blocked_groups = explode(",",$blocked_groups);
$blocked_usertypes= explode(",",$blocked_usertypes);
# end get blocked groups / usertypes


$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();


$x1  = ($CatID!=0 && $CatID > 0) ? "<select name=CatID onChange=checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()>\n" : "<select name=CatID onChange=this.form.submit()>\n";
$x1 .= "<option value=0></option>\n";
for ($i=0; $i<sizeof($cats); $i++)
{
     list($id,$name) = $cats[$i];
     if ($id!=0 && !in_array($id,$blocked_groups))
     {
         $x1 .= "<option value=$id ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
     }
}
/*
$x1 .= "<option value=3 ".(($CatID==3)?"SELECTED":"").">".$i_GroupRole[3]."</option>\n";
$x1 .= "<option value=1 ".(($CatID==1)?"SELECTED":"").">".$i_GroupRole[1]."</option>\n";
$x1 .= "<option value=2 ".(($CatID==2)?"SELECTED":"").">".$i_GroupRole[2]."</option>\n";
$x1 .= "<option value=4 ".(($CatID==4)?"SELECTED":"").">".$i_GroupRole[4]."</option>\n";
$x1 .= "<option value=5 ".(($CatID==5)?"SELECTED":"").">".$i_GroupRole[5]."</option>\n";
$x1 .= "<option value=6 ".(($CatID==6)?"SELECTED":"").">".$i_GroupRole[6]."</option>\n";
*/
$x1 .= "<option value=0>";
for($i = 0; $i < 20; $i++)
$x1 .= "_";
$x1 .= "</option>\n";
$x1 .= "<option value=0></option>\n";


if (in_array(1,$permitted) && !in_array(1,$blocked_usertypes)){
    $x1 .= "<option value=-1 ".(($CatID==-1)?"SELECTED":"").">$i_teachingStaff</option>\n";
}
if (in_array(1,$permitted) && !in_array(5,$blocked_usertypes)){
    $x1 .= "<option value=-5 ".(($CatID==-5)?"SELECTED":"").">$i_nonteachingStaff</option>\n";
}
if (in_array(2,$permitted) && !in_array(2,$blocked_usertypes) )
    $x1 .= "<option value=-2 ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";
if (in_array(3,$permitted) && !in_array(3,$blocked_usertypes))
    $x1 .= "<option value=-3 ".(($CatID==-3)?"SELECTED":"").">$i_identity_parent</option>\n";


/*
$row = $li->returnCategoryGroups(0);
for($i=0; $i<sizeof($row); $i++){
     $IdentityCatID = $row[$i][0] + 10;
     $IdentityCatName = $row[$i][1];
     if ($access_right[$row[$i][0]-1]==1 || $file_content == "")
         $x1 .= "<option value=$IdentityCatID ".(($IdentityCatID==$CatID)?"SELECTED":"").">$IdentityCatName</option>\n";
}
*/
$x1 .= "<option value=0></option>\n";
$x1 .= "</select>";


if($CatID!=0 && $CatID > 0) {
     $row = $li->returnCategoryGroups($CatID);
     $x2  = "<select name=ChooseGroupID[] size=8 multiple>\n";
     for($i=0; $i<sizeof($row); $i++){
          $GroupCatID = $row[$i][0];
          $GroupCatName = $row[$i][1];
          $x2 .= "<option value=$GroupCatID";
          for($j=0; $j<sizeof($ChooseGroupID); $j++){
          $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
          }
          $x2 .= ">$GroupCatName</option>\n";
     }
     $x2 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x2 .= "&nbsp;";
     $x2 .= "</option>\n";
     $x2 .= "</select>\n";
}

if($CatID < 0){
   # Return users with identity chosen
   $selectedUserType = 0-$CatID;
   $c_teaching="";
   if($selectedUserType==5){
	   $c_user_type = 1;
	   $c_teaching = 0;
   }else if($selectedUserType==1){
	   $c_user_type = $selectedUserType;
	   $c_teaching =1;
   }else $c_user_type = $selectedUserType;

   //$row = $li->returnUserForType($selectedUserType);
   $lu = new libuser();
   $row = $lu->returnUsersByIdentity($c_user_type,$c_teaching);

     $x3  = "<select name=ChooseUserID[] size=10 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}
else if(isset($ChooseGroupID)) {
     $row = $li->returnGroupUsersInIdentity($ChooseGroupID, $permitted);
     $x3  = "<select name=ChooseUserID[] size=10 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}

$step1 = getStepIcons(3,1,$i_SelectMemberSteps);
$step2 = getStepIcons(3,2,$i_SelectMemberSteps);
$step3 = getStepIcons(3,3,$i_SelectMemberSteps);
$suf_parent = $i_general_targetParent;

if ($fieldname == "Recipient[]")
{
	$ExtraJS = " par.displayTable('internalToTextDiv','block'); \n";
	$ExtraJS .= " par.displayTable('internalToRemoveBtnDiv','block'); \n";	
}
else if ($fieldname == "InternalCC[]")
{
	$ExtraJS = " par.displayTable('internalCCTextDiv','block'); \n";
	$ExtraJS .= " par.displayTable('internalCCRemoveBtnDiv','block'); \n";	
}
else if ($fieldname == "InternalBCC[]")
{
	$ExtraJS = " par.displayTable('internalBCCTextDiv','block'); \n";
	$ExtraJS .= " par.displayTable('internalBCCRemoveBtnDiv','block'); \n";	
}	

?>

<script language="javascript">
function AddOptions(obj, type){
	
     par = opener.window;
     
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];
     
     //alert("0");
     
     if (type==0)    // Normal
         x = (obj.name == "ChooseGroupID[]") ? "G" : "U";
     else
         x = (obj.name == "ChooseGroupID[]") ? "Q" : "P";

     //alert("1");
         
     checkOption(obj);
     par.checkOption(parObj);
     
     //alert("2");
     
     i = obj.selectedIndex;
     while(i!=-1){
           if (type==0)
               addtext = obj.options[i].text;
           else
               addtext = obj.options[i].text + "<?=$suf_parent?>";

          par.checkOptionAdd(parObj, addtext, x + obj.options[i].value);
            
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     
     <?=$ExtraJS?>
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
</script>

<form name="form1" action="index.php" method="post">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<br />
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td>
		<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tabletext" >
<!--
		<tr >
			<td><a class=functionlink_new href='search.php?fieldname=<?=$fieldname?>'><?=$i_CampusMail_New_SearchRecipient?></a></td>
		</tr>
-->
		<tr>
			<td valign="top" nowrap="nowrap" width="30%" >
			<span class="tabletext"><?=$i_GroupCategorySettings?>:</span>
			</td>
			<td >
			<?=$x1?>
			</td>
		</tr>

		<?php
		if($CatID!=0 && $CatID > 0)
		{
		?>
		<tr>
			<td height="1" colspan="2" class="dotline">
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
			</td>
		</tr>

		<tr>
			<td height="5" colspan="2"  >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
			</td>
		</tr>

		<tr >
			<td valign="top" nowrap="nowrap" width="30%" >
			<span class="tabletext"><?=$i_adminmenu_group?>:</span>
			</td>
			<td >
			<table border="0" cellpadding="0" cellspacing="0" align="left">
			<tr >
				<td >
				<?=$x2?>
				</td>
				<td valign="bottom" >
				<table cellpadding="0" cellspacing="6" >
				<tr >
					<td >
					<?= $linterface->GET_BTN($button_add, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'],0)","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
				</tr >
				<tr >
					<td >
					<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
					</td >
				</tr >
				<tr >
					<td >
					<?= $linterface->GET_BTN($i_general_select_parent, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'],1);","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
					</td >
				</tr >
				<?php
				if(!$sys_custom['Mail_NoSelectAllButton'])
				{
				?>
				<tr >
					<td >
					<?= $linterface->GET_BTN($button_select_all, "submit", "SelectAll(this.form.elements['ChooseGroupID[]']); return false;","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
					</td >
				</tr >
				<?php
				}
				?>
				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>
		<?php
		}
		?>

		<?php
		if(isset($ChooseGroupID))
		{
		?>
		<tr>
			<td height="1" colspan="2" class="dotline">
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
			</td>
		</tr>

		<tr>
			<td height="5" colspan="2"  >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
			</td>
		</tr>

		<tr >
			<td valign="top" nowrap="nowrap" width="30%" >
			<span class="tabletext"><?=$i_CampusMail_New_AddressBook_ByUser?>:</span>
			</td>
			<td >
			<table border="0" cellpadding="0" cellspacing="0" align="left">
			<tr >
				<td >
				<?=$x3?>
				</td>
				<td valign="bottom" >
				<table cellpadding="0" cellspacing="6" >


				<tr >
					<td >
					<?= $linterface->GET_BTN($button_add, "submit", "checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'],0)","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
				</tr >

				<?php
				if($CatID!=-1 && $CatID!=-3 && $CatID!=-5)
				{
				?>
				<tr >
					<td >
					<?= $linterface->GET_BTN($i_general_select_parent, "submit", "checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'],1);","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
					</td >
				</tr >
				<?php
				}

				if(!$sys_custom['Mail_NoSelectAllButton'])
				{
				?>
				<tr >
					<td >
					<?= $linterface->GET_BTN($button_select_all, "submit", "SelectAll(this.form.elements['ChooseUserID[]']); return false;","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
					</td >
				</tr >
				<?php
				}
				?>

				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>											
		<?php 
		} 
		?>
						
		</table>
		<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />

		</td>
	</tr>

	<tr>
		<td>
		<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
			</td>
		</tr>
		</table>
		</td>
	</tr>

	</table>

	</td>
</tr>
</table>

</form>

<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>