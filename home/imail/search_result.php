<?php
// Editing by 
/*
 * 2014-05-09 (Carlos): Modified if Deleted=1, display folder name should be [Trash], and make the mail unviewable.					
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$curDateFormat = date("Y-m-d");

$lwebmail = new libwebmail();

if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) && $lwebmail->type == 3)
{
    $noWebmail = false;
}
else
{
    $noWebmail = true;
}


# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$keyword = trim($keyword);
$default_field = ($noWebmail) ? 4:5;
if($field=="") $field = $default_field;
if($order=="") $order = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->IsColOff = "imail";
$ldb = new libdb();

# Build Table and SQL
$username_field = getNameFieldWithClassNumberByLang("b."); //($chi? "ChineseName": "EnglishName");

# Inbox field : important, notification, attachment, subject, size, sender, is read
if (!$noWebmail)
{
     $li->field_array[] = "a.MailType";
     $db_webmail_field = "IF(a.MailType=2,'$i_CampusMail_New_Icon_ExtMail','$i_CampusMail_New_Icon_IntMail'),";
}
else
{
    $db_webmail_field = "";
}
$li->field_array[] = "a.IsImportant";
$li->field_array[] = "a.IsNotification";
$li->field_array[] = "UserName";
$li->field_array[] = "a.Subject";
$li->field_array[] = "a.DateModified";
$li->field_array[] = "FolderTitleDisplay";

// for search
$search_message1 = addslashes(trim($search_message));
$search_subject1 = addslashes(trim($search_subject));
$search_sender1 =  addslashes(trim($search_sender));
$search_recipient1 = addslashes(trim($search_recipient));


// for hidden values
$search_message = intranet_htmlspecialchars(stripslashes(trim($search_message)));
$search_subject = intranet_htmlspecialchars(stripslashes(trim($search_subject)));
$search_sender =  intranet_htmlspecialchars(stripslashes(trim($search_sender)));
$search_recipient = intranet_htmlspecialchars(stripslashes(trim($search_recipient)));


function getSearchField($fieldname){
	
	$fieldname =" REPLACE($fieldname,'&amp;','&') ";
	$fieldname =" REPLACE($fieldname,'&quot;','\"') ";
	$fieldname =" REPLACE($fieldname,'&#039;','\'') ";
	$fieldname =" REPLACE($fieldname,'&lt;','<') ";
	$fieldname =" REPLACE($fieldname,'&gt;','>') ";
	
	return $fieldname;
	
}

$search_conds = "";
if ($searchFolder != "")
{
    if ($searchFolder == "-1")
    {
        $search_conds .= " AND a.Deleted = 1 ";
    }
    else $search_conds .= " AND a.UserFolderID = '".$li->Get_Safe_Sql_Query($searchFolder)."' ";
}
if ($search_recipient != "")
{
    $recipient_sql = "	SELECT 
    							CONCAT('U',UserID) 
    						FROM 
    							INTRANET_USER 
    						WHERE
                             	".getSearchField("UserLogin")." LIKE '%".$li->Get_Safe_Sql_Like_Query($search_recipient1)."%'
                             	OR ".getSearchField("EnglishName")." LIKE '%".$li->Get_Safe_Sql_Like_Query($search_recipient1)."%'
                             	OR ".getSearchField("ChineseName")." LIKE '%".$li->Get_Safe_Sql_Like_Query($search_recipient1)."%'
                             ";
    $temp1 = $ldb->returnVector($recipient_sql);
    $recipient_g_sql = "SELECT CONCAT('G',GroupID) FROM INTRANET_GROUP WHERE
                               ".getSearchField("Title")." LIKE '%".$li->Get_Safe_Sql_Like_Query($search_recipient1)."%'";
    $temp2 = $ldb->returnVector($recipient_g_sql);

    $temp_re = array_merge($temp1, $temp2);
    $temp_re = array_values($temp_re);
    $re_conds = "";
    $re_delim = "";
    for ($i=0; $i<sizeof($temp_re); $i++)
    {
         $target = $temp_re[$i];
         $length = strlen($target);
         $re_conds .= "$re_delim (
                                  LOCATE('$target,',a.RecipientID) != 0
                                  OR LOCATE('$target,',a.InternalCC) != 0
                                  OR LOCATE('$target,',a.InternalBCC) != 0
                                  OR RIGHT(a.RecipientID, $length) = '$target'
                                  OR RIGHT(a.InternalCC, $length) = '$target'
                                  OR RIGHT(a.InternalBCC, $length) = '$target'
                                  )";
         $re_delim = " OR ";
    }

    $search_conds .= " AND (".getSearchField("a.ExternalTo")." LIKE '%".$li->Get_Safe_Sql_Like_Query($search_recipient1)."%'
                       OR ".getSearchField("a.ExternalCC")." LIKE '%".$li->Get_Safe_Sql_Like_Query($search_recipient1)."%'
                       OR ".getSearchField("a.ExternalBCC")." LIKE '%".$li->Get_Safe_Sql_Like_Query($search_recipient1)."%'
                       ".($re_conds==""?"":"OR ($re_conds)")." )";
}

if ($search_sender != "")
{
    $sender_sql = "SELECT UserID FROM INTRANET_USER WHERE
                             ".getSearchField("UserLogin")." LIKE '%".$li->Get_Safe_Sql_Like_Query($search_sender1)."%'
                             OR ".getSearchField("EnglishName")." LIKE '%".$li->Get_Safe_Sql_Like_Query($search_sender1)."%'
                             OR ".getSearchField("ChineseName")." LIKE '%".$li->Get_Safe_Sql_Like_Query($search_sender1)."%'
                             ";
    $temp = $ldb->returnVector($sender_sql);
    $user_list = implode(",",$temp);
    $search_conds .= " AND (".getSearchField("a.SenderEmail")." LIKE '%".$li->Get_Safe_Sql_Like_Query($search_sender1)."%' ".
                       ($user_list==""?"":" OR a.SenderID IN ($user_list)").")";
}
if ($search_subject != "")
{
    $search_conds .= " AND ".getSearchField("a.Subject")." LIKE '%".$li->Get_Safe_Sql_Like_Query($search_subject1)."%' ";
}

if ($search_message != "")
{
    $search_conds .= " AND a.Message LIKE '%".$li->Get_Safe_Sql_Like_Query($search_message1)."%' ";
}
if ($isAttachment)
{
    $search_conds .= " AND a.IsAttachment = 1 ";
}

if ($date_from != "")
{
    $search_conds .= " AND a.DateModified >= '".$li->Get_Safe_Sql_Query($date_from)."' ";
}
if ($date_to != "")
{
    $search_conds .= " AND a.DateModified <= '".$li->Get_Safe_Sql_Query($date_to)." 23:59:59' ";
}
if ($MailType=="" || $MailType == 0)
{
    $conds_mailtype = "";
}
else
{
    $conds_mailtype = " AND a.MailType = '".$li->Get_Safe_Sql_Query($MailType)."' ";
}



$returnPar  = "";
$returnPar .= "search_message=".urlencode(stripslashes($search_message));
$returnPar .= "&search_subject=".urlencode(stripslashes($search_subject));
$returnPar .= "&search_sender=".urlencode(stripslashes($search_sender));
$returnPar .= "&search_recipient=".urlencode(stripslashes($search_recipient));
$returnPar .= "&isAttachment=".urlencode($isAttachment);
$returnPar .= "&date_from=".urlencode($date_from);
$returnPar .= "&date_to=".urlencode($date_to);
$returnPar .= "&MailType=".urlencode($MailType);
$returnPar .= "&searchFolder=".urlencode($searchFolder);
$returnPar = str_replace("\"","%22",$returnPar);

/*
$sql  = "SELECT
               $db_webmail_field
               IF(a.IsImportant=1,'".addslashes($i_frontpage_campusmail_icon_important2_2007)."','<br>'),
               IF(a.IsNotification=1,'".addslashes($i_frontpage_campusmail_icon_notification2_2007)."','<br>'),               
               IF(a.MailType=1,
                  IF(a.SenderID = 0, '$i_general_sysadmin', $username_field),
                  a.SenderEmail
               ) as UserName,
               IF(a.RecordStatus='1' OR a.RecordStatus='2',
               IF(a.IsAttachment=1,
               CONCAT('<a href=\'viewmail.php?viewtype=search&CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubject\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', a.Subject, '</a>'),
               CONCAT('<a href=\'viewmail.php?viewtype=search&CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubject\' >', a.Subject, '</a>')),
               IF(a.IsAttachment=1,
               CONCAT('<a href=\'viewmail.php?viewtype=search&CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubjectunread\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', a.Subject, '</a>'),
               CONCAT('<a href=\'viewmail.php?viewtype=search&CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubjectunread\' >', a.Subject, '</a>'))),	               
                                             
           		IF ((DATE_FORMAT(a.DateModified, '%Y-%m-%d') = '{$curDateFormat}'),
				CONCAT('<span title=\"',DATE_FORMAT(a.DateModified, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateModified, '%H:%i'),'</span>'),
				CONCAT('<span title=\"',DATE_FORMAT(a.DateModified, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateModified, '%Y-%m-%d'),'</span>')),               		
               
               IF (a.Deleted != 1,c.FolderName,'$i_admintitle_im_campusmail_trash') AS FolderTitleDisplay,
               CONCAT('<input type=checkbox name=CampusMailID[] value=', a.CampusMailID ,'>')
               ,a.RecordStatus,''
          	FROM 
          		INTRANET_CAMPUSMAIL AS a
               	LEFT OUTER JOIN INTRANET_USER AS b ON a.SenderID = b.UserID
				LEFT OUTER JOIN INTRANET_CAMPUSMAIL_FOLDER as c ON a.UserFolderID = c.FolderID
          	WHERE
               a.UserID = $UserID
               $conds_mailtype $search_conds
          ";
*/
$sql  = "SELECT
               $db_webmail_field
               IF(a.IsImportant=1,'".addslashes($i_frontpage_campusmail_icon_important2_2007)."','<br>'),
               IF(a.IsNotification=1,'".addslashes($i_frontpage_campusmail_icon_notification2_2007)."','<br>'),               
               IF(a.MailType=1,
                  IF(a.SenderID = 0, '$i_general_sysadmin', $username_field),
                  a.SenderEmail
               ) as UserName,
				/*
               IF(a.RecordStatus='1' OR a.RecordStatus='2',
               	  IF(a.IsAttachment=1,
               		CONCAT('<a href=\'viewmail.php?viewtype=search&CampusMailID=', a.CampusMailID, '&preFolder=', a.UserFolderID, '\' class=\'iMailsubject\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', a.Subject, '</a>'),
               		CONCAT('<a href=\'viewmail.php?viewtype=search&CampusMailID=', a.CampusMailID, '&preFolder=', a.UserFolderID, '\' class=\'iMailsubject\' >', a.Subject, '</a>')
				  ),
              	  IF(a.IsAttachment=1,
               		CONCAT('<a href=\'viewmail.php?viewtype=search&CampusMailID=', a.CampusMailID, '&preFolder=', a.UserFolderID, '\' class=\'iMailsubjectunread\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', a.Subject, '</a>'),
               		CONCAT('<a href=\'viewmail.php?viewtype=search&CampusMailID=', a.CampusMailID, '&preFolder=', a.UserFolderID, '\' class=\'iMailsubjectunread\' >', a.Subject, '</a>')
				  )
				),	               
                */
				IF(a.Deleted='1',
					CONCAT('<span class=\'',IF(a.RecordStatus='1' OR a.RecordStatus='2','iMailsubject','iMailsubjectunread'),'\'>',IF(a.IsAttachment='1','".addslashes($i_frontpage_campusmail_icon_attachment_2007)."',''),a.Subject,'</span>'),
					CONCAT('<a href=\'viewmail.php?viewtype=search&CampusMailID=', a.CampusMailID, '&preFolder=', a.UserFolderID, '\' class=\'',IF(a.RecordStatus='1' OR a.RecordStatus='2','iMailsubject','iMailsubjectunread'),'\'>',IF(a.IsAttachment='1','".addslashes($i_frontpage_campusmail_icon_attachment_2007)."',''),a.Subject,'</a>')
				),
                             
           		IF ((DATE_FORMAT(a.DateInput, '%Y-%m-%d') = '{$curDateFormat}'),
				CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%H:%i'),'</span>'),
				CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%Y-%m-%d'),'</span>')),               		
               IF(a.Deleted='1', CONCAT('".$i_CampusMail_New_Trash."'),
               	 IF(a.UserFolderID = 2, CONCAT('".$i_CampusMail_New_Inbox."'), 
               		IF(a.UserFolderID = 1, CONCAT('".$i_frontpage_campusmail_draft."'), 
               			IF(a.UserFolderID = 0, CONCAT('".$i_CampusMail_New_Outbox."'), 
               				IF(a.UserFolderID = -1, CONCAT('".$i_CampusMail_New_Trash."'),
								IF(a.UserFolderID = -2, CONCAT('".$i_spam['FolderSpam']."'), c.FolderName )))))) AS FolderTitleDisplay,
               CONCAT('<input type=checkbox name=CampusMailID[] value=', a.CampusMailID ,'>')
               ,a.RecordStatus,'',CONCAT('SEARCH')	  
          	FROM 
          		INTRANET_CAMPUSMAIL AS a
               	LEFT OUTER JOIN INTRANET_USER AS b ON a.SenderID = b.UserID
				LEFT OUTER JOIN INTRANET_CAMPUSMAIL_FOLDER as c ON a.UserFolderID = c.FolderID
          	WHERE
               a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."'
               $conds_mailtype $search_conds
          ";
          //hdebug($sql);
// TABLE COLUMN
$pos = 0;
if (!$noWebmail)
{
     //$li->column_list .= "<td width='23' class='tabletop' >".$li->column_image($pos++,$i_CampusMail_New_MailSource, $i_CampusMail_New_Icon_MailSource)."</td>\n";
     //$pos++;
     //$li->column_list .= "<td width='23' class='tabletop' >".$i_CampusMail_New_Icon_MailSource."</td>\n";
     $li->column_list .= "<td width='23' class='tabletop' >".$li->column_image($pos++,$i_CampusMail_New_MailSource, $i_CampusMail_New_Icon_MailSource)."</td>\n";
     $width_subject = 177-23+43;
}
else
{
    $width_subject = 177+43;
}
$li->column_list .= "<td width='9' class='tabletop' >".$li->column_image($pos, $i_frontpage_campusmail_important, $i_frontpage_campusmail_icon_important_2007)."</td>\n";	
$li->column_list .= "<td width='20' class='tabletop' >".$li->column_image($pos+1, $i_frontpage_campusmail_notification, $i_frontpage_campusmail_icon_notification_2007)."</td>\n";
$li->column_list .= "<td width='113' class='tabletop' >".$li->column($pos+2, $i_frontpage_campusmail_sender)."</td>\n";
$li->column_list .= "<td class='tabletop' >".$li->column($pos+3, $i_frontpage_campusmail_subject)."</td>\n";
$li->column_list .= "<td width='99' class='tabletop' >".$li->column($pos+4, $i_frontpage_campusmail_date)."</td>\n";
$li->column_list .= "<td width='103' class='tabletop' >".$li->column($pos+5, $i_CampusMail_New_MailFolder)."</td>\n";
$li->column_list .= "<td width='40' class='tabletop' >".$li->check("CampusMailID[]")."</td>\n";
if (!$noWebmail)
{
    $li->column_array = array(0,0,0,5,5,0,0);
    $li->wrap_array = array(0,0,0,14,22,0,0);
}
else
{
    $li->column_array = array(0,0,0,5,0,0);
    $li->wrap_array = array(0,0,14,22,0,0);
}

$li->no_col = sizeof($li->field_array)+3;

# TABLE INFO
$li->sql = $sql;
$li->title = "";

$folder_sql = "	SELECT 
						FolderID, FolderName 
					FROM 
						INTRANET_CAMPUSMAIL_FOLDER 
					WHERE 
						OwnerID = '".$li->Get_Safe_Sql_Query($UserID)."' OR FolderID = 2 ORDER BY RecordType, FolderName
					";
$folders = $ldb->returnArray($folder_sql,2);

# Find unread mails number
/*
$sql  = "
			SELECT
				count(a.CampusMailID)
          	FROM 
          		INTRANET_CAMPUSMAIL AS a LEFT OUTER JOIN INTRANET_USER AS b
          	ON 
          		a.SenderID = b.UserID
			WHERE
               a.UserID = $UserID AND
               a.UserFolderID = 2 AND
               a.Deleted != 1 AND	               	               
               (a.RecordStatus = '' OR a.RecordStatus IS NULL)
          ";
$row = $ldb->returnVector($sql);	          
$unreadInboxNo = $row[0];
*/


$CurrentPage = "PageSearch";
$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_CampusMail_New_Mail_Search." ".$i_frontpage_bulletin_result, "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

$deleteBtn  = "<a href=\"javascript:checkRemove(document.form1,'CampusMailID[]','remove.php')\" class=\"tabletool\">";
$deleteBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
$deleteBtn .= $button_remove."</a>";

$backBtn  = "<a href=\"search.php?{$returnPar}\" class='tablebottomlink' onMouseOver=\"MM_swapImage('page_previous','','{$image_path}/{$LAYOUT_SKIN}/icon_prev_on.gif',1)\" onMouseOut='MM_swapImgRestore()' >";
$backBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_prev_off.gif\" name='page_previous' id='page_previous' width='11' height='10' border='0' hspace='3' align='absmiddle' >";
$backBtn .= $button_back."</a>";

$folder_sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID = '".$li->Get_Safe_Sql_Query($UserID)."' OR FolderID = 2 ORDER BY RecordType, FolderName";
$folders = $li->returnArray($folder_sql,2);
$select_toChangefolder = getSelectByArray($folders,"name='targetFolderID' onchange=\"if (this.selectedIndex != 0) {checkAlert(document.form1,'CampusMailID[]','changefolder.php','$i_CampusMail_New_alert_moveto'); this.selectedIndex=0}\" ","",0,0,$button_moveto);    

?>
<form name="form1" method="get" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<br />
	<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">	
	<?=$xmsg?>	
	<tr>
		<td >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">		
		<tr>
			<td>
			<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td><p><?=$backBtn?></p></td>
			</tr>
			</table>
			</td>
        </tr>
        <tr class="table-action-bar">
			<td align="right" style="vertical-align:bottom" >
			<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td>
				<?=$select_toChangefolder?>
				</td>
				<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
				<td><p><?=$deleteBtn?></p></td>
			</tr>
			</table>
			</td>
		</tr>
		</table>		
		</td>
	</tr>	
	<tr>
		<td >
		<?=$li->display($functionbar,$functionbar2)?>
		</td>
	</tr>
	</table>
	<br />
	</td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" /> 
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="MailType" value="<?=escape_double_quotes($MailType)?>" />
<input type="hidden" name="fromSearch" value="1" />
<input type="hidden" name="search_message" value="<?=escape_double_quotes($search_message)?>" />
<input type="hidden" name="search_subject" value="<?=escape_double_quotes($search_subject)?>" />
<input type="hidden" name="search_sender" value="<?=escape_double_quotes($search_sender)?>" />
<input type="hidden" name="search_recipient" value="<?=escape_double_quotes($search_recipient)?>" />
<input type="hidden" name="searchFolder" value="<?=escape_double_quotes($searchFolder)?>" />
<input type="hidden" name="isAttachment" value="<?=escape_double_quotes($isAttachment)?>" />
<input type="hidden" name="date_from" value="<?=escape_double_quotes($date_from)?>" />
<input type="hidden" name="date_to" value="<?=escape_double_quotes($date_to)?>" />

</form>

<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();	
?>