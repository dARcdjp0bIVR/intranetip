<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPageArr['iMail'] = 1;
$CurrentPage 	= "PageAddressBook_ExternalReceipientGroup";
$lwebmail	= new libwebmail();
$linterface = new interface_html();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$ldb = new libdb();
$sql = "SELECT AliasName,Remark,OwnerID FROM INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL WHERE AliasID = '$AliasID'";
$temp = $ldb->returnArray($sql,3);
list($aliasName,$aliasRemark,$OwnerID) = $temp[0];
if ($OwnerID != $UserID)
{
    header("Location: groupalias.php");
    exit();
}

if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$keyword = trim($keyword);

$keyword = trim($keyword);
$aliastype = 1;
$username_field = getNameFieldWithClassNumberByLang("b."); //($chi? "ChineseName": "EnglishName");

$sql  = "SELECT
		a.DateModified,
               b.TargetName,
               b.TargetAddress,
               a.DateModified,
               CONCAT('<input type=checkbox name=EntryID[] value=', a.EntryID ,'>')
          FROM INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY AS a
                 LEFT OUTER JOIN  INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL AS b ON a.TargetID = b.AddressID
          WHERE
               a.AliasID = '$AliasID' AND b.TargetName LIKE '%$keyword%'
          ";
# TABLE INFO
$li->field_array = array("b.TargetName","b.TargetAddress","a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->IsColOff = "imail_addressbook_list";
$li->title = "";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class='tabletoplink' width=1>#</td>\n";
$li->column_list .= "<td class='tabletoplink' width='20'>&nbsp;</td>\n";
$li->column_list .= "<td class='tabletoplink' width=45%>".$li->column($pos++, "$i_CampusMail_New_AddressBook_Name")."</td>\n";
$li->column_list .= "<td class='tabletoplink' width=34%>".$li->column($pos++, "$i_CampusMail_New_AddressBook_EmailAddress")."</td>\n";
$li->column_list .= "<td class='tabletoplink' width=20%>".$li->column($pos++, $i_LastModified)."</td>\n";
$li->column_list .= "<td class='tabletoplink' width=1%>".$li->check("EntryID[]")."</td>\n";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);

### Button / Tag
$AddBtn 	= "<a href=\"javascript:newWindow('groupalias_ex/?AliasID=$AliasID',1)\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_select . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'EntryID[]','groupalias_ex_remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$searchTag 	.= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
$searchTag 	.= "</tr></table>";

$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_address_exgroup.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='imailpagetitle'>{$i_CampusMail_External_Recipient_Group}</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleImage1.$TitleTitle1."</td></tr></table>";
$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($TitleTitle, "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

?>

<br />
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td valign="top" width="30%" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_CampusMail_New_AliasGroup?> </span></td>
					<td valign="top" class="iMailrecipientgroup"><?=$aliasName?></td>
				</tr>
				<tr>
					<td valign="top" width="30%" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext"><?=$i_CampusMail_New_AliasGroupRemark?> </span></td>
					<td valign="top" class="tabletext"><?=nl2br($aliasRemark)?> </td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		</table>
        </td>
</tr>
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
                                        	<table border="0" cellspacing="0" cellpadding="2">
                                            	<tr>
                                              		<td><p><?=$AddBtn?></p></td>
                                            	</tr>
                                          	</table>
					</td>
                                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
				</tr>
                                <tr>
					<td><?=$searchTag?></td>
					<td align="right" valign="bottom">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
        						<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
                                                        	<table border="0" cellspacing="0" cellpadding="2">
        							<tr>
        								<td nowrap><?=$delBtn?></td>
        							</tr>
        							</table>
        						</td>
        						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
						</tr>
						</table>
					</td>
				</tr>
                                <tr>
					<td colspan="2">
                                                <?php
							echo $li->display();
						?>
					</td>
				</tr>
				</table>
                        </td>
		</tr>
                </table>
	</td>
</tr>
</table>
<br />

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="AliasID" value="<?=$AliasID?>" />
<input type="hidden" name="TabID" value="<?=$TabID?>" />
</form>





<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>