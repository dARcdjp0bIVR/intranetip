<?php
// Editing by 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
#include_once("../../includes/libcampusquota.php");
include_once("../../includes/libaccess.php");
include_once("../../includes/libwebmail.php");
include_once("../../lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
auth_campusmail();


$lwebmail = new libwebmail();
/*
if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
{
    include_once("../../includes/libsystemaccess.php");
    $lsysaccess = new libsystemaccess($UserID);
    if ($lsysaccess->hasMailRight())
    {
        $noWebmail = false;
    }
    else
    {
        $noWebmail = true;
    }
}
else
{
    $noWebmail = true;
}
*/
# Block No webmail no preference
if ($UserID=="")
{
    header("Location: index.php");
    exit();
}

$li = new libuser($UserID);


$display_name_field="";
$reply_email_field="";
$days_in_spam_field="";
$days_in_trash_field="";
$signature_field="";
$forwarding_emails_field="";
$keep_copy_field="";


# submit from general prference page
if($TabID==1){
	# display name
	$display_name_field = " DisplayName = '".$li->Get_Safe_Sql_Query($display_name)."', ";
	
	# reply email
	$reply_email_field = " ReplyEmail = '".$li->Get_Safe_Sql_Query($reply_email)."', ";
	
	# days in spam
	$days_in_spam_field = " DaysInSpam = '".$li->Get_Safe_Sql_Query($days_in_spam)."', ";
	
	# days in trash
	$days_in_trash_field = " DaysInTrash ='".$li->Get_Safe_Sql_Query($days_in_trash)."', ";
}
else if($TabID==2){ # submit from signature page
	# signature
	$signature_field = " Signature = '".$li->Get_Safe_Sql_Query($signature)."', ";
}
else if($TabID==3){ # submit from email forwarding page
		
	# forwarding email
	$forwarding_emails_field =" ForwardedEmail='', ";
	$login = $li->UserLogin;
	if($disable_email_forwarding==""&&$forwarding_emails!=""){
		$temp = explode("\n",$forwarding_emails);
		$delimiter = "";
		$delim_db="";
		$targetEmail="";
		for($i=0;$i<sizeof($temp);$i++){
			$t_mail = trim($temp[$i]);
			if(intranet_validateEmail($t_mail)){
				$targetEmail .=$delimiter.$t_mail;
				$delimiter = ",";
			}
		}
		if($targetEmail!=""){
			$lwebmail->setMailForward($login, $targetEmail, $keep_copy);
			$forwarding_emails_field =" ForwardedEmail='".$li->Get_Safe_Sql_Query($targetEmail)."', ";

		}
	}
	else{
		//echo "login=$login";
		$lwebmail->removeMailForward($login);
	}

	
	# keep copy
	$keep_copy_field = $keep_copy=="1"?"ForwardKeepCopy=1,":"ForwardKeepCopy=0,";
}
else if ($TabID == 4) {
	
     # update table
     $sql = "SELECT UserID FROM INTRANET_IMAIL_PREFERENCE WHERE UserID = '".$li->Get_Safe_Sql_Query($UserID)."'";
     $temp = $li->returnVector($sql);

     if ($temp[0]==$UserID)    # Update
     {
         $sql = "UPDATE INTRANET_IMAIL_PREFERENCE SET SkipCheckEmail = '".$li->Get_Safe_Sql_Query($_REQUEST['disable_check_email'])."', DateModified=NOW() WHERE UserID = '".$li->Get_Safe_Sql_Query($UserID)."'";
     }
     else          # Insert
     {
         $sql = "INSERT INTRANET_IMAIL_PREFERENCE (UserID, SkipCheckEmail, DateInput, DateModified)
                 VALUES ('".$li->Get_Safe_Sql_Query($UserID)."','".$li->Get_Safe_Sql_Query($_REQUEST['disable_check_email'])."', NOW(), NOW() )";
     }
     $li->db_db_query($sql);
}
else if ($TabID == 5) {
	
     if ($enable_auto_reply)
     {
         if ($reply_content)
         {
          	 $reply_content = stripslashes($reply_content);
             $lwebmail->addAutoReply($li->UserLogin, $reply_content);
         }
     }
     else
     {
         $lwebmail->removeAutoReply($li->UserLogin);
     }
}


if ($TabID == 1 || $TabID == 2 || $TabID == 3)
{
# update table
$update_fields ="";
$update_fields =$display_name_field.$reply_email_field.$days_in_spam_field.$days_in_trash_field.$signature_field.$forwarding_emails_field.$keep_copy_field;
$update_fields = trim($update_fields);
if($update_fields!=""){
        $sql="INSERT INTO INTRANET_IMAIL_PREFERENCE (UserID,DateInput,DateModified) VALUES('".$li->Get_Safe_Sql_Query($UserID)."',NOW(),NOW())";
        $li->db_db_query($sql);
        $update_fields .=" DateModified=NOW()";
        $sql = "UPDATE INTRANET_IMAIL_PREFERENCE SET $update_fields WHERE UserID='".$li->Get_Safe_Sql_Query($UserID)."'";
        $li->db_db_query($sql);
}

}

intranet_closedb();

$return_url = $_SERVER['HTTP_REFERER'];
	
$return_url = explode("?",$return_url);

$anc = $special_option['no_anchor'] ?"":"#anc";
header("Location: ".$return_url[0]."?TabID=".urlencode($TabID)."&msg=2".$anc);
?>