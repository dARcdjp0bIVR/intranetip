<?php
// Editing by 
/***************************************
 *  modification log
 *  20190606 Carlos: Added CSRF token for folder_remove.php   
 *  20100621 Marcus:
 * 			check $SYS_CONFIG['Mail']['hide_imail'] , if true , open interface_html with "imail_archive.php"
 * 
 * **************************************/
 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

$lc = new libcampusmail();
$lc2 = new libcampusquota2007($UserID);
$lwebmail = new libwebmail();

$sql = "	SELECT 
				FolderID, FolderName 
			FROM 
				INTRANET_CAMPUSMAIL_FOLDER
        	WHERE 
        		OwnerID = '$UserID'
        	ORDER BY 
        		FolderName
        ";
$folders = $lc->returnArray($sql,2);

$list = "";
$delim = "";
for ($i=0; $i<sizeof($folders); $i++)
{
     $list .= "$delim ".$folders[$i][0];
     $delim = ",";
}

if ($list != "")
{
    $sql = "SELECT UserFolderID, COUNT(CampusMailID) FROM INTRANET_CAMPUSMAIL
            WHERE UserFolderID IN ($list) AND UserID = '$UserID'
            GROUP BY UserFolderID";
    $stat_array = $lc->returnArray($sql,2);
}

$display = "<table width='100%' border='0' cellpadding='5' cellspacing='0' align='center' >\n";
$display .= "<tr class='tabletop tabletoplink' >";
$display .= "<td>$i_CampusMail_New_FolderName</td>";
$display .= "<td>$i_CampusMail_New_NumOfMail</td>";
$display .= "<td >$button_remove</td>";
$display .= "</tr>\n";


if ($list == "")
{	
    $display .= "<tr><td colspan='2' align='center' class='tablegreenrow1 tabletext' >$i_CampusMail_New_NoFolder</td></tr>\n";
}
else
{
    $stat = build_assoc_array($stat_array);
    for ($i=0; $i<sizeof($folders); $i++)
    {
         list ($fid,$name) = $folders[$i];
         $count = $stat[$fid]+0;
         $css = ($i%2? "":"2");
         //$edit_link = "<a href=folder_edit.php?fid=$fid><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" border=0></a>";
         $edit_link = "<a href='folder_edit.php?fid=$fid' class='iMailsubject' >$name</a>";
         $remove_link = "<a href=\"javascript:alertRemove($fid)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" border=0></a>";
         $folder_link = "<a href='viewfolder.php?FolderID=$fid' class='iMailsubject' >$count</a>";
                  
		if ($i % 2 == 0)
		{
			$display .= "<tr class='tablegreenrow1 tabletext' ><td>$edit_link</td><td>$folder_link</td><td >$remove_link</td></tr>\n";
		} else {
         	$display .= "<tr class='tablegreenrow2 tabletext' ><td>$edit_link</td><td>$folder_link</td><td >$remove_link</td></tr>\n";
     	}
    }
}
$display .= "</table>\n";

### For new Interface ###
$folder_sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID = '$UserID' OR RecordType = 0 ORDER BY RecordType, FolderName";
$folders = $lc->returnArray($folder_sql,2);
$folders[] = array(-1,$i_admintitle_im_campusmail_trash);

/*
# Find unread mails number
$sql  = "
			SELECT
				count(a.CampusMailID)
          	FROM
          		INTRANET_CAMPUSMAIL AS a LEFT OUTER JOIN INTRANET_USER AS b
          	ON
          		a.SenderID = b.UserID
			WHERE
               a.UserID = $UserID AND
               a.UserFolderID = 2 AND
               a.Deleted != 1 AND
               (a.RecordStatus = '' OR a.RecordStatus IS NULL)
          ";
$row = $ldb->returnVector($sql);
$unreadInboxNo = $row[0];
*/

$CurrentPage = "PageCheckMail_FolderManager";

$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();

# tag information
$iMailImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_folder_open.gif' width='20' height='20' align='absmiddle' >";
$iMailTitle1 = "<span class='imailpagetitle'>{$i_CampusMail_New_FolderManager}</span>";
$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$iMailImage1.$iMailTitle1.$unreadLink."</td><td align='right' style=\"vertical-align: bottom;\" >&nbsp;</td></tr></table>";
$TAGS_OBJ[] = array($iMailTitle, "", 0);

if($SYS_CONFIG['Mail']['hide_imail']===true)
	$linterface = new interface_html("imail_archive.html");
else
	$linterface = new interface_html();
$linterface->LAYOUT_START();

$toolbar = $linterface->GET_LNK_NEW("folder_new.php");

if ($msg == "2")
{
	$xmsg = $linterface->GET_SYS_MSG("update");
} else {
	$xmsg = "&nbsp;";
}
##################

?>
<script language="javascript">
function alertRemove(id)
{
         if (confirm('<?=$i_CampusMail_New_Alert_RemoveFolder?>'))
         {
             location = 'folder_remove.php?fid='+id+'&csrf_token='+document.form1.csrf_token.value+'&csrf_token_key='+document.form1.csrf_token_key.value;
             return true;
         }
         //else return false;
}
</script>
<form name="form1" action="" method="GET">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align='right' ><?=$xmsg?></td>
	</tr>
	<tr>
		<td >
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0"  >
		<tr>
			<td><?=$toolbar?></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td><?=$display?></td>
		</tr>      
		</table>
				
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
<?php 
cleanCsrfTokenSessions();
echo csrfTokenHtml(generateCsrfToken());
?>
</form>
<?php
	//echo $lc->msg;
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>