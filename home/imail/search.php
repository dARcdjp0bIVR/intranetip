<?php


$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

if (!$imail_feature_allowed['mail_search'])
{
     header("Location: index.php");
     exit();
}

$lwebmail = new libwebmail();
if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) && $lwebmail->type == 3)
{
    include_once("../../includes/libsystemaccess.php");
    $lsysaccess = new libsystemaccess($UserID);
    if ($lsysaccess->hasMailRight())
    {
        $noWebmail = false;
    }
    else
    {
        $noWebmail = true;
    }
}
else
{
    $noWebmail = true;
}


$ldb = new libdb();
$folder_sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID = '$UserID' OR RecordType = 0 ORDER BY RecordType, FolderName";
$folders = $ldb->returnArray($folder_sql,2);
$folders[] = array(-1,$i_admintitle_im_campusmail_trash);
$select_folder = getSelectByArray($folders," name='searchFolder' ",$searchFolder,1);


# Find unread mails number
/*
$sql  = "
			SELECT
				count(a.CampusMailID)
          	FROM 
          		INTRANET_CAMPUSMAIL AS a LEFT OUTER JOIN INTRANET_USER AS b
          	ON 
          		a.SenderID = b.UserID
			WHERE
               a.UserID = $UserID AND
               a.UserFolderID = 2 AND
               a.Deleted != 1 AND	               	               
               (a.RecordStatus = '' OR a.RecordStatus IS NULL)
          ";
$row = $ldb->returnVector($sql);	          
$unreadInboxNo = $row[0];
*/


$CurrentPage = "PageSearch";
$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_CampusMail_New_Mail_Search, "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

$MailTypeSelect0 = ($MailType==0 || $MailType=="") ? " selected='selected' " : "";
$MailTypeSelect1 = ($MailType==1) ? " selected='selected' " : "";
$MailTypeSelect2 = ($MailType==2) ? " selected='selected' " : "";

$isAttachmentCheck= ($isAttachment==1) ? " checked='checked' " : ""; 

$search_subject = stripslashes($search_subject);
$search_subject = str_replace("\"","&quot;",$search_subject);
$search_message = stripslashes($search_message);
$search_message = str_replace("\"","&quot;",$search_message);
$search_recipient = stripslashes($search_recipient);
$search_recipient = str_replace("\"","&quot;",$search_recipient);
$search_sender = stripslashes($search_sender);
$search_sender = str_replace("\"","&quot;",$search_sender);


?>


     <script type="text/javascript">
     <!--

          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                   if (String(month).length == 1) {

                       month = '0' + month;
                   }

                   if (String(date).length == 1) {
                       date = '0' + date;
                   }
                   dateValue =year + '-' + month + '-' + date;
                   document.forms['form1'].date_from.value = dateValue;
          }
          function calendarCallback2(date, month, year)
          {
                   if (String(month).length == 1) {
                       month = '0' + month;
                   }

                   if (String(date).length == 1) {
                       date = '0' + date;
                   }

                   dateValue =year + '-' + month + '-' + date;
                   document.forms['form1'].date_to.value = dateValue;
          }
     // -->
     </script>

<form name="form1" method="get" action="search_result.php">
<?=$xmsg?>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_CampusMail_New_MailSearch_SearchFolder?></td>
		<td valign="top" width="70%"><?=$select_folder?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_frontpage_campusmail_recipients?></td>
		<td valign="top">
		<input type="text" name="search_recipient" size="50" class="textboxtext" value="<?=$search_recipient?>" />
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_frontpage_campusmail_sender?></td>
		<td valign="top">
		<input type="text" name="search_sender" size="50" class="textboxtext" value="<?=$search_sender?>" />
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_frontpage_campusmail_subject?></td>
		<td valign="top">
		<input type="text" name="search_subject" size="50" class="textboxtext" value="<?=$search_subject?>" />
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_frontpage_campusmail_message?></td>
		<td valign="top">
		<input type="text" name="search_message" size="50" class="textboxtext" value="<?=$search_message?>" />
		</td>
	</tr>
	<?php 
	if (!$noWebmail) 
	{			 
	?>
		<tr>
          	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_CampusMail_New_MailSource?></td>
          	<td width="70%">
            <select name="MailType">
          	<option value="0" <?=$MailTypeSelect0?> > -- <?=$i_status_all?> -- </option>
          	<option value="1" <?=$MailTypeSelect1?> ><?=$i_CampusMail_New_MailSource_Internal?></option>
          	<option value="2" <?=$MailTypeSelect2?> ><?=$i_CampusMail_New_MailSource_External?></option>
            </select>
          	</td>
		</tr>
	<?php 
	} 
	?>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_record_date?></td>
			<td valign="top" class="tabletext">
			<input type="text" name="date_from" size="10" class="textboxnum" value="<?=$date_from?>" /><?=$linterface->GET_CALENDAR("form1", "date_from")?>
			~
			<input type="text" name="date_to" size="10" class="textboxnum" value="<?=$date_to?>" /><?=$linterface->GET_CALENDAR("form1", "date_to")?>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
			<td valign="top" class="tabletext">
			<input type="checkbox" name="isAttachment" value="1" id="isAttachment1" <?=$isAttachmentCheck?> />
			<label for="isAttachment1"><?=$i_CampusMail_New_MailSearch_hasAttachment?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?= $linterface->GET_ACTION_BTN($button_quicksearch, "submit", "") ?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>




<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="numToAdd" value="" />
</form>

<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>