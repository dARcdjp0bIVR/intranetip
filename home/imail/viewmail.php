<?php 
#using : 
/******************* modification log ********************
 * 2020-01-03 Sam: Hide the reply box if $sys_custom['iMail']['HideNotificationReply'] set to true 
 * 2017-03-06 Carlos: Update cached counter.
 * 2011-03-24 Carlos: fix auto reply set read flag
 * 2010-06-21 Marcus: check $SYS_CONFIG['Mail']['hide_imail'] , if true , open interface_html with "imail_archive.php"
 *********************************************************/

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

$CampusMailID = IntegerSafe($CampusMailID);

$li = new libcampusmail2007($CampusMailID);
$lwebmail = new libwebmail();
$lc = new libcampusquota2007($UserID);
$outType = ($li->SenderID == $UserID && $li->CampusMailFromID == "");
$mailFolderID = $li->UserFolderID;

if ($preFolder >= 2)
{
	if (($li->RecordStatus==1) || ($li->RecordStatus==2))
	{
	}
	else
	{
		$iNewCampusMail -= 1;
	}
}

###################################################
### Added by Ronald on 20090311
###
###	Details:
###		Mark the selected mail as "Readed", so that the 'unread mail counter' will be updated
### 	before the left-hand side menu show.
###	
### Edited by Michael Cheung on 20091218
### 
### Details:
### 	Update the DateModified at INTRANET_CAMPUSMAIL_REPLY when the original mail is not read 
###################################################
if ($li->isAutoReply()) {
    $originalReadStatusSql = "SELECT IsRead FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID = '".$li->Get_Safe_Sql_Query($li->CampusMailFromID)."' AND UserID = '".$li->Get_Safe_Sql_Query($UserID)."'";
    $orgReadStatusReturn = $li->returnVector($originalReadStatusSql);
    $orgReadStatus = $orgReadStatusReturn[0];
    if ($orgReadStatus != 1) {
        $UpdateDateModifiedSql = "UPDATE INTRANET_CAMPUSMAIL_REPLY SET IsRead = 1, DateModified = now() WHERE CampusMailID = '".$li->Get_Safe_Sql_Query($li->CampusMailFromID)."' AND UserID = '".$li->Get_Safe_Sql_Query($UserID)."'";
        //echo '.'.'<div style="display:none;">'.$UpdateDateModifiedSql.'</div>';
        $li->db_db_query($UpdateDateModifiedSql);
    }
}
if(isset($li->RecordStatus)) {
	if(($li->RecordStatus != 1) || ($li->RecordStatus != 2)) {
		// continuous
	} else {
		$sql = "UPDATE INTRANET_CAMPUSMAIL SET RecordStatus = 1 WHERE CampusMailID = '".$li->Get_Safe_Sql_Query($CampusMailID)."'";
		$li->db_db_query($sql);
		
		$li->UpdateCampusMailNewMailCount($_SESSION['UserID'], $preFolder, -1);
	}
} else {
	$sql = "UPDATE INTRANET_CAMPUSMAIL SET RecordStatus = 1 WHERE CampusMailID = '".$li->Get_Safe_Sql_Query($CampusMailID)."'";
	$li->db_db_query($sql);
	
	$li->UpdateCampusMailNewMailCount($_SESSION['UserID'], $preFolder, -1);
}

if ($viewtype == "search")
{

}
else
{
    $nextID = $li->returniMailNextID($mailFolderID);
    $prevID = $li->returniMailPrevID($mailFolderID);
}

# Find unread mails number
/*
$sql  = "
			SELECT
				count(a.CampusMailID)
          	FROM
          		INTRANET_CAMPUSMAIL AS a LEFT OUTER JOIN INTRANET_USER AS b
          	ON
          		a.SenderID = b.UserID
			WHERE
               a.UserID = $UserID AND
               a.UserFolderID = 2 AND
               a.Deleted != 1 AND
               (a.RecordStatus = '' OR a.RecordStatus IS NULL) AND
               a.CampusMailID != '{$CampusMailID}'
          ";
$row = $li->returnVector($sql);
$unreadInboxNo = $row[0];
*/
$header_default_style = "/templates/style_mail.css";
$navigation = $i_frontpage_separator.$i_CampusMail_New_iMail;

### For new Interface ###
$folder_sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID = '".$li->Get_Safe_Sql_Query($UserID)."' OR RecordType = 0 ORDER BY RecordType, FolderName";
$folders = $lc->returnArray($folder_sql,2);
$folders[] = array(-1,$i_admintitle_im_campusmail_trash);
$FolderID = $preFolder;

# tag information
$iMailImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_inbox.gif' height='18' width='18' align='absmiddle' >";
$unreadInboxNo = $iNewCampusMail;
if ($preFolder == 2)
{
	$iMailTitle1 = "<span class='imailpagetitle'>{$i_frontpage_campusmail_inbox}</span>";

	if ($unreadInboxNo > 0)
	{
		$unreadLink = "<a href='viewfolder.php?FolderID=2&unreadOnly=1' class='imailpagetitleunread' > ({$unreadInboxNo} {$i_frontpage_campusmail_unread}) </a>";
	}

	//$unreadLink = "<a href='#' class='imailpagetitleunread' > ({$unreadInboxNo} Unread) </a>";
	$CurrentPage = "PageCheckMail_Inbox";
}
else if ($preFolder == 0)
{
	$iMailTitle1 = "<span class='imailpagetitle'>{$i_frontpage_campusmail_outbox}</span>";
	$CurrentPage = "PageCheckMail_Outbox";
}
else if ($preFolder == 1)
{
	$iMailTitle1 = "<span class='imailpagetitle'>{$i_frontpage_campusmail_draft}</span>";
	$CurrentPage = "PageCheckMail_Draft";
}
else if ($preFolder == -1)
{
	$iMailTitle1 = "<span class='imailpagetitle'>{$i_frontpage_campusmail_trash}</span>";
	$CurrentPage = "PageCheckMail_Trash";
}
else if ($preFolder == -2)
{
	$iMailTitle1 = "<span class='imailpagetitle'>{$i_spam['FolderSpam']}</span>";
	$CurrentPage = "PageCheckMail_Spam";
}
else
{
	for($i=0;$i<count($folders);$i++)
	{
		if ($folders[$i][0] == $preFolder)
		{
			$iMailTitle1 = "<span class='imailpagetitle'>".intranet_htmlspecialchars($folders[$i][1])."</span>";
		}
	}
	$CurrentPage = "PageCheckMail_FolderManager";
}

$FolderID = $preFolder;
$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();

$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$iMailImage1.$iMailTitle1.$unreadLink."</td><td align='right' style=\"vertical-align: bottom;\" >".$lc->displayiMailFullBar()."</td></tr></table>";
$TAGS_OBJ[] = array($iMailTitle, "", 0);

if($SYS_CONFIG['Mail']['hide_imail']===true)
	$linterface = new interface_html("imail_archive.html");
else
	$linterface = new interface_html("imail_default.html");
$linterface->LAYOUT_START();

if ($msg == "2")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("update")."</td></tr>";
}
else if ($msg == "3")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("delete")."</td></tr>";
}
else if ($exmsg == "1")
{
	if ($signal == "2")
	{		
		$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_gen_msg_email_save."</font>")."</td></tr>";
	} 
	else 
	{
		$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_msg_email."</font>" )."</td></tr>";
	}
} 
else if ($exmsg == "4")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_gen_msg_email_restore."</font>")."</td></tr>";
}
else if ($exmsg == "0")			### Only For IP2.0
{
	if($signal == "1")
	{
		$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_msg_email."</font>" )."</td></tr>";
	}
}								### End at here
else 
{
	$xmsg = "";
}

$pagebtnTable  = "<table border='0' cellpadding='2' cellspacing='0'>";
$pagebtnTable .= "<tr>";
if ($prevID!="")
{
	$prevBtn  = "<a href=\"viewmail.php?CampusMailID=".urlencode($prevID)."&preFolder=".urlencode($preFolder)."\" class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('prevp','','{$image_path}/{$LAYOUT_SKIN}/icon_prev_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">";
	$prevBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_prev_off.gif\" name=\"prevp\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"prevp\"> {$i_campusmail_prev}</a>";

	$pagebtnTable .= "<td align='center' valign='middle'>{$prevBtn}</td>";
	$pagebtnTable .= "<td align='center' valign='middle'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td>";
	if ($nextID!="") $pagebtnTable .= "<td align='center' valign='middle'>|</td>";
}

if ($nextID!="")
{
	$nextBtn  = "<a href=\"viewmail.php?CampusMailID=".urlencode($nextID)."&preFolder=".urlencode($preFolder)."\" class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('nextp','','{$image_path}/{$LAYOUT_SKIN}/icon_next_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">";
	$nextBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_next_off.gif\" name=\"nextp\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"nextp\"> {$i_campusmail_next}</a>";

	$pagebtnTable .= "<td align='center' valign='middle'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td>";
	$pagebtnTable .= "<td align='center' valign='middle'>{$nextBtn}</td>";
}
$pagebtnTable .= "</tr>";
$pagebtnTable .= "</table>";

if ($viewtype=="search")
{

	$backBtn  = "<br /><a href=\"javascript:window.history.go(-1)\" class='tablebottomlink' onMouseOver=\"MM_swapImage('page_previous','','{$image_path}/{$LAYOUT_SKIN}/icon_prev_on.gif',1)\" onMouseOut='MM_swapImgRestore()' >";
	$backBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_prev_off.gif\" name='page_previous' id='page_previous' width='11' height='10' border='0' hspace='3' align='absmiddle' >";
	$backBtn .= $button_back."</a><br /><br />";
}
##################

$DateInfo = strip_tags($li->GET_DATE_FIELD());

?>

<script language="javascript" >
function warning_msg()
{
	var email_warning ="<?=$i_CampusMail_Encoding_Warning?>";
	return confirm(email_warning);
}
function removeMail()
{
         if (confirm('<?=$i_CampusMail_New_Confirm_remove_mail?>'))
         {
             location.href = 'remove.php?CampusMailID=<?=urlencode($CampusMailID)?>&page_from=viewmail.php&nextID=<?=urlencode($nextID)?>&prevID=<?=urlencode($prevID)?>&preFolder=<?=urlencode($preFolder)?>';
         }
}
function viewMessageSource()
{
         newWindow('message_popup.php?CampusMailID=<?=urlencode($CampusMailID)?>&removeHTML=0',4);
}

function setLabels()
{

	var reply = '<?=$i_CampusMail_New_Reply?>';
	var replyall='<?=$i_CampusMail_New_ReplyAll?>';
	var forward ='<?=$i_CampusMail_New_Forward?>';
	var deletestr = '<?=$button_remove?>';
	var viewsource ='<?=$i_CampusMail_New_ViewFormat_MessageSource?>';
	var strdate = '<?=$i_frontpage_campusmail_date?>';
	var sender = '<?=$i_frontpage_campusmail_sender?>';
	var int_to = '<?=$i_CampusMail_New_To?>';
	var int_bcc ='<?=$i_CampusMail_New_BCC?>';
	var int_cc ='<?=$i_CampusMail_New_CC?>';
	var int_recipient ='<?=$i_CampusMail_New_InternalRecipients?>';
	var ext_to = int_to;
	var ext_bcc = int_bcc;
	var ext_cc = int_cc;
	var ext_recipient ='<?=$i_CampusMail_New_ExternalRecipients?>';
	var subject = '<?=$i_frontpage_campusmail_subject?>';
	var message='<?=$i_frontpage_campusmail_message?>';
	var attachment='<?=$i_frontpage_campusmail_attachment?>';
	var read ='<?=$i_frontpage_campusmail_read?>';
	var notification_reply='<?=$i_frontpage_campusmail_reply?>';
	var notification_instruction ='<?=$i_frontpage_campusmail_notification_instruction?>';
	var notification_instruction2='<?=$i_frontpage_campusmail_notification_instruction_message_only?>';
	var date_info='<?=$DateInfo?>';
	var rec_all='<?=$i_CampusMail_New_Show_All_Recipient?>';
	var rec_all_hide='<?=$i_CampusMail_New_Hide_All_Recipient?>';


	// Reply
	setAlt('reply_alt',reply);
	setText('reply',reply);

	// Reply All
	setAlt('replyall_alt',replyall);
	setText('replyall',replyall);

	// Forward
	setAlt('forward_alt',forward);
	setText('forward',forward);

	// delete
	setAlt('delete_alt',deletestr);
	setText('delete',deletestr);

	setText('view_source',viewsource);
	setText('date',strdate);
	setText('sender',sender);
	setText('int_new_to',int_to);
	setText('int_new_cc',int_cc);
	setText('int_new_bcc',int_bcc);
	setText('int_recipient',int_recipient);
	setText('ext_new_to',ext_to);
	setText('ext_new_cc',ext_cc);
	setText('ext_new_bcc',ext_bcc);
	setText('ext_recipient',ext_recipient);

	setText('subject',subject);
	//setText('message',message);
	setText('attachment',attachment);
	//setText('campusmail_read',read);
	//setText('campusmail_reply',notification_reply);
	//setText('instruction1',notification_instruction);
	//setText('instruction2',notification_instruction2);
	setText('PastDateDiv',date_info);

	setText('RecipientToDiv',rec_all);
	setText('RecipientCcDiv',rec_all);
	setText('RecipientBccDiv',rec_all);
	setText('RecipientExToDiv',rec_all);
	setText('RecipientExCcDiv',rec_all);
	setText('RecipientExBccDiv',rec_all);

	setText('HideRecipientToDiv',rec_all_hide);
	setText('HideRecipientCcDiv',rec_all_hide);
	setText('HideRecipientBccDiv',rec_all_hide);
	setText('HideRecipientExToDiv',rec_all_hide);
	setText('HideRecipientExCcDiv',rec_all_hide);
	setText('HideRecipientExBccDiv',rec_all_hide);

}
function setText(objName,val){
	obj = imail_frame.document.getElementById(objName);
	if(obj!=null)
		obj.innerHTML=val;

}
function setAlt(objName,val){
	obj = eval('imail_frame.document.'+objName);
	if(obj!=null)
		obj.alt=val;
}
function confirmNotification(){
	return confirm('<?=$i_frontpage_campusmail_alert_not_reply?>');
}
function showReplyBox(){
	obj = document.getElementById('reply_box');
	if(obj==null) return;
	if(<?=$sys_custom['iMail']['HideNotificationReply']?'true':'false'?>) return;
	obj.style.visibility='visible';
}
</script>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" >
<tr>
	<td>
	<br />
	<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<?=$xmsg?>
	<tr>
		<td >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
			<?=$backBtn?>
			<?=$pagebtnTable?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" class='iMailReadtable' >
		<tr>
			<td width="100%" align="left" valign="top" >
			<iframe frameborder="0" name="imail_frame" id="imail_frame" scrolling="no" width="100%" height="200" src="viewmail_content.php?CampusMailID=<?=urlencode($CampusMailID)?>" marginwidth="0" marginheight="0"></iframe>
			</td>
		</tr>
<!-- show notification -->

		<?php
		if (!$outType)
		{
			echo "<tr><td>";
			echo "<div id='reply_box' style='visibility:hidden'>";

	     	echo $li->displayiMailNotification2();
	     	echo "</div>";
	     	echo "</td></tr>";
	 	}

	 	?>
<!-- end show notification -->
		<tr>
			<td width="100%" align="left" valign="top" >
			<?php
			if ($outType)
			{
				echo $li->displayCampusMailReplyiMail();
			}
			?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	</table>
	<br />
	</td>
</tr>
</table>
<?
if($SYS_CONFIG['Mail']['hide_imail']===false)
{
	if($plugin['webmail'])
	{
?>
		<table width='96%' border='0' cellpadding='3' cellspacing='0' align='center'>
			<tr>
				<td>
					<p>
						<b><?=$Lang['iMail']['FieldTitle']['Reminder']['CannotReadMailContent_Title'];?></b>
						<br>
						<?=$Lang['iMail']['FieldTitle']['Reminder']['CannotReadMailContent_Body'];?>
					</p>
				</td>
			</tr>
		</table>
<?
	}
}
?>


<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>
