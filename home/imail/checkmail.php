<?php
// Using by : 
/*
 * 2017-09-27 (Carlos): If no access to webmail, delete new mail counter for inbox and spam box to recount.
 * 2014-09-03 (Carlos): If change password fail, then check if webmail account does not exist, auto create the web
 * 2014-07-03 (Carlos): Auto sync iMail account password if connection fail
 */
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libcampusmail.php");
include_once("../../includes/libwebmail.php");
intranet_auth();
intranet_opendb();

//skip check mail process by marcus
if($UserID == 5 && $special_feature['forCharles'])
{
	header("location: viewfolder.php?mailchk=1");
	exit;
}

$lwebmail = new libwebmail();

if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) && $lwebmail->type == 3)
{
	# Check preference of check email
    $li = new libdb();
    $sql ="SELECT SkipCheckEmail FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='$UserID'";
    $result = $li->returnArray($sql,1);
    $optionSkipCheckEmail = $result[0][0];
    if ($optionSkipCheckEmail)
    {
        $noWebmail = true;
    }
    else
    {
        $noWebmail = false;
    }

    ####################
}
else
{
    $noWebmail = true;
    
    $li = new libdb();
	$sql = "DELETE FROM INTRANET_CAMPUSMAIL_NEW_MAIL_COUNT WHERE UserID='".$_SESSION['UserID']."' AND FolderID IN (2,-2)";
	$li->db_db_query($sql);
}

if (!$noWebmail )
{
	include_once("../../includes/libuser.php");
	include_once("../../lang/lang.$intranet_session_language.php");
	$lu = new libuser($UserID);
	$inbox = $lwebmail->openInbox($lu->UserLogin,$lu->UserPassword);
	if($inbox === FALSE){
		$lwebmail->change_password($lu->UserLogin, $_SESSION['eclass_session_password'], "iMail");
		$inbox = $lwebmail->openInbox($lu->UserLogin,$lu->UserPassword);
		if($inbox === FALSE){
			$account_exists = $lwebmail->isAccountExist($lu->UserLogin,"iMail");
			if(!$account_exists){
				$lwebmail->open_account($lu->UserLogin, $_SESSION['eclass_session_password']);
				$lwebmail->setTotalQuota($lu->UserLogin,10,"iMail");
			}
		}
		$inbox = $lwebmail->openInbox($lu->UserLogin,$lu->UserPassword);
	}
	
	//session_register("imail_extotal");
	//session_register("imail_received");
	//$imail_received = 0;
	//$imail_extotal = $lwebmail->checkMailCount();
	
	session_register_intranet("imail_extotal", 0);
	session_register_intranet("imail_received", $lwebmail->checkMailCount());

?>
<META http-equiv=Content-Type content='text/html; charset=utf-8'>
<html>
<head><title><?=$i_CampusMail_New_PlsWait?></title></head>
<FRAMESET cols="0,*">
  <FRAME name="process" id="process" src="">
  <FRAME name=display src="checkmail_display.php">
</FRAMESET>
</html>
<?

}
else
{
    header("Location: viewfolder.php?mailchk=1");
}
intranet_closedb();

?>