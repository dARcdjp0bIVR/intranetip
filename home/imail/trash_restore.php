<?php
// Editing by 
/*
 * 2017-03-06 (Carlos): Update trash box and original folder new mail counter.
 */
$PATH_WRT_ROOT = "../../"; 
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

$li = new libcampusmail();

$sql = "SELECT UserFolderID, COUNT(*) as NewMailCount FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND CampusMailID IN (".implode(",", $CampusMailID).") AND (RecordStatus IS NULL OR RecordStatus='') GROUP BY UserFolderID";
$count_records = $li->returnResultSet($sql);

for($i=0;$i<count($count_records);$i++)
{
	$li->UpdateCampusMailNewMailCount($_SESSION['UserID'], $count_records[$i]['UserFolderID'], $count_records[$i]['NewMailCount']);
	$li->UpdateCampusMailNewMailCount($_SESSION['UserID'], -1, -$count_records[$i]['NewMailCount']);
}

//$sql = "UPDATE INTRANET_CAMPUSMAIL SET Deleted = 0 WHERE UserID = $UserID AND CampusMailID IN (".implode(",", $CampusMailID).")";
//$sql = "UPDATE INTRANET_CAMPUSMAIL SET Deleted = 0, UserFolderID = 2 WHERE UserID = $UserID AND CampusMailID IN (".implode(",", $CampusMailID).")";

$sql = "UPDATE INTRANET_CAMPUSMAIL SET Deleted = 0, DateInFolder = NOW() WHERE UserID = '$UserID' AND CampusMailID IN ('".implode("','", $CampusMailID)."')";
$li->db_db_query($sql);

intranet_closedb();
header("Location: trash.php?msg=4");
?>