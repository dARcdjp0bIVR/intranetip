<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

$lwebmail = new libwebmail();
$li = new libdb();

if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
{
    include_once($PATH_WRT_ROOT."includes/libsystemaccess.php");
    $lsysaccess = new libsystemaccess($UserID);
    if ($lsysaccess->hasMailRight())
    {
        $noWebmail = false;
    }
    else
    {
        $noWebmail = true;
    }
}
else
{
    $noWebmail = true;
}
//$noWebmail = false;


if ($UserID=="")
{
    header("Location: index.php");
    exit();
}

$CurrentPage = "PageSettings_EmailRules";
$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_CampusMail_New_Settings_EmailRules, "", 0);
$PAGE_NAVIGATION[] = array($button_new.($intranet_session_language=="en"?" ":""). ($TabID==1? $i_CampusMail_New_Settings_EmailRules : $i_CampusMail_New_Settings_EmailRules), "");

$linterface = new interface_html();
$linterface->LAYOUT_START();

if ($msg == "2")
{
	$xmsg = $linterface->GET_SYS_MSG("update");
} else {
	$xmsg = "&nbsp;";
}

### Generate the target folder list ###
$folder_sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE (OwnerID = '$UserID') AND (FolderID != '0,1,2') ORDER BY RecordType, FolderName";
$folders = $li->returnArray($folder_sql,2);

if($webmail_info['bl_spam']){
	$tmp_arr[0] = -2;
	$tmp_arr[FolderID] = -2;
	$tmp_arr[1] = $i_spam['FolderSpam'];
	$tmp_arr[FolderName] = $i_spam['FolderSpam'];
	array_push($folders,$tmp_arr);
}
$folders_selection = getSelectByArray($folders, "name='targetFolder'", $targetFolder);
?>
<script language='JavaScript'>

function validateExternalEmailAddress()
{
	objForwardTo= document.form1.act_forward;
	if(objForwardTo == null) return false;
	v_forward_to= objForwardTo.value;
	
	var msg = '<?=$i_invalid_email?>';
	
	invalid_forward_td= getInvalidEmailAddresses(v_forward_to);
	
	if(invalid_forward_td.length>0){
		str = invalid_forward_td.join("\n");
		alert(msg+":\n\n"+str);
		objForwardTo.focus();
		return false;
	}
	
	return true;
}
function getInvalidEmailAddresses(rec)
{		
	var mails = new Array();
	mailCnt = 0;
	mails2 = rec.split(";");	
	for(var i=0;i<mails2.length;i++)
	{		
		var tmpMails = mails2[i].split(",");			
		
		for(var j=0;j<tmpMails.length;j++)
		{			
			mails[mailCnt] = tmpMails[j];
			mailCnt++;
		}
	}
	
	invalid = new Array();
	for(i=0;i<mails.length;i++)
	{		
			mail = Trim(mails[i]);
			if(mail=="") continue;
			last_pos = mail.lastIndexOf("<");
			last_pos2= mail.lastIndexOf(">");
			if(last_pos != -1 && last_pos2 != -1 && last_pos2 > last_pos && last_pos2==mail.length-1){
				mail = mail.substring(last_pos+1,last_pos2);
			}
			
			if(!validateEmailAddress(mail))
				invalid.push(mails[i]);
	}
	return invalid;
}
function validateEmailAddress(email){
	
        //var re = /^.+@.+\..{2,3}$/;
        var re = /^([0-9A-Za-z_\.\-])+\@(([0-9A-Za-z\-])+\.)+([0-9A-Za-z]{2,3})+$/
        
        if (re.test(email)) {
                return true;
        }else{
                return false;
        }
}

function checkRuleCriteria(){
	
	var obj = document.form1;
	var cnt_fromEmail = 0;
	var cnt_toEmail = 0;
	var cnt_subject = 0;
	var cnt_message = 0;
	var cnt_hasAttachment = 0;

	if(obj.from_email.value == "")
	{
		cnt_fromEmail = 1;
	}
	if(obj.to_email.value == "")
	{
		cnt_toEmail = 1;
	}
	if(obj.subject.value == "")
	{
		cnt_subject == 1;
	}
	if(obj.message.value == "")
	{
		cnt_message == 1;
	}
	if(obj.has_attachment.checked == false){
		cnt_hasAttachment == 1;
	}
	 
	if(cnt_fromEmail == 1 && cnt_toEmail == 1 && cnt_subject == 1 && cnt_message == 1 && cnt_hasAttachment == 1){
		return false;
	}else{
		return true;
	}
}

function checkAction()
{
	var obj = document.form1;
	var cnt_act_MarkAsRead = 0;
	var cnt_act_Delete = 0;
	var cnt_act_Move = 0;
	var cnt_act_Forward = 0;
	
	if(obj.act_mark_as_read.checked == false){
		cnt_act_MarkAsRead = 1;
	}
	if(obj.act_delete_mail.checked == false){
		cnt_act_Delete = 1;
	}
	if(obj.targetFolder.selectedIndex == 0){
		cnt_act_Move = 1;
	}
	if(obj.act_forward.value == ""){
		cnt_act_Forward = 1;
	}
	
	if(cnt_act_MarkAsRead == 1 && cnt_act_Delete == 1 && cnt_act_Move == 1 && cnt_act_Forward == 1){
		return false;
	}else{
		return true;
	}
}

function checkForm()
{
	var obj = document.form1;
	
	if(checkRuleCriteria()){
		if(checkAction()){
			if((obj.act_delete_mail.checked == true) && (obj.act_mark_as_read.checked == true || obj.targetFolder.selectedIndex != 0)) {
				alert('<?=$i_CampusMail_MailRule_DeleteActionJSWaring;?>');
				obj.act_mark_as_read.checked = false;
				obj.targetFolder.selectedIndex = 0;
				return false;
			}
			return validateExternalEmailAddress();
		}else{
			alert('<?=$i_CampusMail_MailRule_ActionEmptyJSWaring;?>');
			return false;
		}
	}
	return false;
}

</script>
<br>
<form name="form1" action="pref_email_rules_new_update.php" method="POST" onSubmit="return checkForm();">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$xmsg?></td>
</tr>
</table>
<br>
<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
<!--<tr><td class='formfieldtitle tabletext'>Name</td><td><input type='input' name='rule_name'></td></tr>-->
<tr class='tabletop'><td class='tablesubtitle2' valign='top' nowrap='nowrap'><?=$i_CampusMail_Condition?></td></tr>	
<tr><td align='right'><?=$i_CampusMail_Condition_From;?></td><td><?=$linterface->GET_TEXTAREA("from_email",$from_email, 30, 5)?></td></tr>
<tr><td align='right'><?=$i_CampusMail_Condition_To;?></td><td><?=$linterface->GET_TEXTAREA("to_email",$to_email, 30, 5)?></td></tr>
<tr><td align='right'><?=$i_CampusMail_Condition_Subject;?></td><td><?=$linterface->GET_TEXTAREA("subject",$subject, 30, 5)?></td></tr>
<tr><td align='right'><?=$i_CampusMail_Condition_Message;?></td><td><?=$linterface->GET_TEXTAREA("message",$message, 30, 5)?></td></tr>
<?	if($has_attachment){
		$has_attachment_checked = "CHECKED";
	}
?>
<tr><td align='right'><?=$i_CampusMail_Condition_HasAttachment;?></td><td><input type='checkbox' name='has_attachment' value=1 <?=$has_attachment_checked;?> ></td></tr>

<tr class='tabletop'><td class='tablesubtitle2' valign='top' nowrap='nowrap'><?=$i_CampusMail_Action;?></td></tr>
<?	if($act_mark_as_read == 1){
		$mark_as_read_checked = "CHECKED";
	}else{
		$mark_as_read_checked = "";
	}
?>
<tr><td align='right'><?=$i_CampusMail_Action_MarkAsRead;?></td><td><input type='checkbox' name='act_mark_as_read' value=1 <?=$mark_as_read_checked;?>></td></tr>
<?	if($act_delete_mail == 1){
		$act_delete_mail = "CHECKED";
	}else{
		$act_delete_mail = "";
	}
?>
<tr><td align='right'><?=$i_CampusMail_Action_DeleteIt;?></td><td><input type='checkbox' name='act_delete_mail' value=1 <?=$act_delete_mail;?>></td></tr>
<tr><td align='right'><?=$i_CampusMail_Action_MoveToFolder;?></td><td><?=$folders_selection;?></td></tr>
<tr><td align='right'><?=$i_CampusMail_Action_MoveToFolder;?></td><td><?=$linterface->GET_TEXTAREA("act_forward",$act_forward, 30, 5)?></td></tr>
<tr><td class="dotline" colspan=2><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr>
	<td align="center" colspan=2>
	<?=$linterface->GET_ACTION_BTN($button_submit, "submit");?>
	<?=$linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");?>
	</td>
</tr>
</table>
<input type='hidden' name='UserID' value='<?=$UserID?>'>
</form>

<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>