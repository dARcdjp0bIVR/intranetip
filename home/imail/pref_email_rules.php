<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

$lwebmail = new libwebmail();
$li = new libdb();

if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
{
    include_once($PATH_WRT_ROOT."includes/libsystemaccess.php");
    $lsysaccess = new libsystemaccess($UserID);
    if ($lsysaccess->hasMailRight())
    {
        $noWebmail = false;
    }
    else
    {
        $noWebmail = true;
    }
}
else
{
    $noWebmail = true;
}
//$noWebmail = false;


if ($UserID=="")
{
    header("Location: index.php");
    exit();
}

$CurrentPage = "PageSettings_EmailRules";
$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_CampusMail_New_Settings_EmailRules, "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

# Generate System Message #
if ($xmsg != "") {
	$SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
	if ($msg == 15) $SysMsg = $linterface->GET_SYS_MSG("","$i_InventorySystem_Warning_SetBulkItemAsResourceItem");
}
# End #

$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('pref_email_rules_new.php')")."&nbsp;";

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'ruleID[]','pref_email_rules_remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'ruleID[]','pref_email_rules_edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";

### Generate the target folder list ###
$folder_sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE (OwnerID = '$UserID') AND (FolderID != '0,1,2') ORDER BY RecordType, FolderName";
$folders = $li->returnArray($folder_sql,2);
if($webmail_info['bl_spam']){
	$tmp_folder_arr[0] = -2;
	$tmp_folder_arr[FolderID] = -2;
	$tmp_folder_arr[1] = $i_spam['FolderSpam'];
	$tmp_folder_arr[FolderName] = $i_spam['FolderSpam'];
	array_push($folders,$tmp_folder_arr);
}

$table_content .= "<tr class='tabletop' valign='bottom'>";
//$table_content .= "<td width='25%' class='tabletopnolink'>Name</td>";
$table_content .= "<td width='50%' class='tabletopnolink'>$i_CampusMail_Condition</td>";
$table_content .= "<td width='50%' class='tabletopnolink'>$i_CampusMail_Action</td>";
$table_content .= "<td width='1%' class='tabletopnolink'><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'ruleID[]'):setChecked(0,this.form,'ruleID[]')\"></td>";
$table_content .= "</tr>";

$sql = "SELECT
				RuleID, Priority, DisplayName, FromEmail, ToEmail, Subject, Message, HasAttachment, 
				ActionMarkAsRead, ActionDelete, ActionMoveToFolder, ActionForwardTo
		FROM 
				INTRANET_IMAIL_MAIL_RULES 
		WHERE 
				UserID = '$UserID'
		ORDER BY
				Priority ASC";
$arr_result = $li->returnArray($sql,12);

if(sizeof($arr_result)>0){
	for($i=0; $i<sizeof($arr_result); $i++){
		list($rule_id, $rule_priority, $rule_name, $criteria_from, $criteria_to, $criteria_subject, $criteria_message, $criteria_has_attachment, $action_MarkAsRead, $action_Delete, $action_MoveToFolder, $action_ForwardTo, $forward_email) = $arr_result[$i];
		
		$arr_rule[] = $rule_id;
		$tmp_arr[$rule_id]['name'] = $rule_name;
		if($criteria_from != ""){
			$tmp_arr[$rule_id]['criteria'][] = "<b>".$i_CampusMail_Condition_From.":</b> ".$criteria_from;
		}
		if($criteria_to != ""){
			$tmp_arr[$rule_id]['criteria'][] = "<b>".$i_CampusMail_Condition_To.":</b> ".$criteria_to;
		}
		if($criteria_subject != ""){
			$tmp_arr[$rule_id]['criteria'][] = "<b>".$i_CampusMail_Condition_Subject.":</b> ".$criteria_subject;
		}
		if($criteria_message != ""){
			$tmp_arr[$rule_id]['criteria'][] = "<b>".$i_CampusMail_Condition_Message.":</b> ".$criteria_message;
		}
		if($criteria_has_attachment != 0){
			$tmp_arr[$rule_id]['criteria'][] = "<b>".$i_CampusMail_Condition_HasAttachment.":</b>";
		}
		
		if($action_MarkAsRead == 1){
			$tmp_arr[$rule_id]['action'][] = $i_CampusMail_Action_MarkAsRead;
		}
		if($action_Delete == 1){
			$tmp_arr[$rule_id]['action'][] = $i_CampusMail_Action_DeleteIt;
		}
		if($action_MoveToFolder != 0){
			if(sizeof($folders)>0){
				for($j=0; $j<sizeof($folders); $j++){
					list($folder_id, $folder_name) = $folders[$j];
					if($folder_id == $action_MoveToFolder){
						$x = $i_CampusMail_Action_MoveToFolder.": ".$folder_name;
						$tmp_arr[$rule_id]['action'][] = $x;
					}
				}
			}
			
		}
		if($action_ForwardTo != ""){
			$x = $i_CampusMail_Action_ForwardTo.": ".$action_ForwardTo;
			$tmp_arr[$rule_id]['action'][] = $x;
		}
	}
}

if(sizeof($arr_rule)>0){
	foreach($arr_rule as $rule_id){
		$cnt++;
		$css = $cnt%2==1?"tablerow1 tabletext":"tablerow2 tabletext";
		
		//$table_content .= "<tr class='$css'>\n<td class='tabletext' valign='top'>".$tmp_arr[$rule_id]['name']."</td>\n";
		$table_content .= "<tr class='$css'><td class='tabletext' valign='top'>".implode("<br>\n",$tmp_arr[$rule_id]['criteria'])."</td>\n";
		$table_content .= "<td class='tabletext' valign='top'>".implode("<br>\n",$tmp_arr[$rule_id]['action'])."</td>\n";
		$table_content .= "<td class='tabletext' valign='top'><input type='checkbox' name='ruleID[]' value=$rule_id></td>\n</tr>\n";
	}
}else{
	$table_content .= "<tr class=\"tablerow2 tabletext\">\n";
	$table_content .= "<td class=\"tabletext\" colspan=\"3\" align=\"center\">$i_no_record_exists_msg</td>\n";
	$table_content .= "</tr>\n";
}
$table_content .= "<tr height=\"25px\"><td class=\"tablebottom\" colspan=\"3\"></td></tr>\n";
?>

<br>
<form name="form1" action="" method="POST">

<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$table_content?>
</table>
</form>

<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>