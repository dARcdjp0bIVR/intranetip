<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$anc = $special_option['no_anchor'] ?"":"#anc";

if ($aliastype != 1) $aliastype = 0;
$tablename = ($aliastype==1?"INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL":"INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL" );

# Retrieve Info
$AliasID = (is_array($AliasID)? $AliasID[0]: $AliasID);
$li = new libdb();
$sql = "SELECT AliasName, Remark FROM $tablename WHERE AliasID = '$AliasID'";
$temp = $li->returnArray($sql,2);
list($name, $remark) = $temp[0];

$CurrentPageArr['iMail'] = 1;
$CurrentPage = $TabID==1 ? "PageAddressBook_InternalReceipientGroup" : "PageAddressBook_ExternalReceipientGroup";

$lwebmail	= new libwebmail();

### Title ###
$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_address_". ($TabID==1 ? "in" : "ex") ."group.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='imailpagetitle'>". ($TabID==1? $i_CampusMail_Internal_Recipient_Group : $i_CampusMail_External_Recipient_Group ) ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleImage1.$TitleTitle1."</td></tr></table>";
$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($TitleTitle, "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":""). ($TabID==1? $i_CampusMail_Internal_Recipient_Group : $i_CampusMail_External_Recipient_Group), "");

?>
<script language="javascript">
<!--
function checkform(obj)
{
     if(!check_text(obj.AliasName, "<?php echo $i_alert_pleasefillin.$i_CampusMail_New_AliasGroup; ?>.")) return false;
}    
//-->
</script>

<br />
<form name="form1" method="post" action="groupalias_edit_update.php" onSubmit="return checkform(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$xmsg?></td>
</tr>
<tr>
	<td colspan="2" align="center">
        <table width="90%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><br />
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_CampusMail_New_AliasGroup?> <span class='tabletextrequire'>*</span></span></td>
				<td><input name="AliasName" type="text" value="<?=$name?>" class="textboxtext" maxlength="255" /></td>
			</tr>
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_CampusMail_New_AliasGroupRemark?> </span></td>
				<td><?=$linterface->GET_TEXTAREA('Remark', $remark, 50, 5)?></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
        </td>
</tr>
<tr>
	<td colspan="2">
        	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","submit3"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='groupalias.php?aliastype=". $aliastype ."&TabID=". $TabID .$anc."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table> 
        </td>
</tr>
</table>
        

         
                                
<input type="hidden" name="pageNo" value="<?php echo $pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $order; ?>" />
<input type="hidden" name="field" value="<?php echo $field; ?>" />
<input type="hidden" name="aliastype" value="<?=$aliastype?>" />
<input type="hidden" name="AliasID" value="<?=$AliasID?>" />
<input type="hidden" name="TabID" value="<?=$TabID?>" />
</form>
            
<?
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.AliasName");
$linterface->LAYOUT_STOP();
?>
