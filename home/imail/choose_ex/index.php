<?php
// using : Ronald

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $i_CampusMail_New_SelectFromAlias;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if($field=="") $field = 0;
$li = new libdbtable($field, $order, $pageNo);

/*
$addressSql = "
					SELECT 
						AddressID, CONCAT(TargetName,' &lt;',TargetAddress,'&gt;') 
					FROM 
						INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
					WHERE 
						OwnerID = $UserID
					";
*/
$addressSql = "
					SELECT 
						AddressID, CONCAT(TargetName,' <',TargetAddress,'>') 
					FROM 
						INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
					WHERE 
						OwnerID = '$UserID' AND
						(TargetName like '%$keyword%' OR
                		TargetAddress like '%$keyword%')
                	ORDER BY
                		TargetAddress ASC";
$TotalArr = $li->returnArray($addressSql,2);    	        					

$x3_text = $i_admintitle_us_user;
$x3 = getSelectByArray($TotalArr," name='AliasEmailID[]' multiple size='20' ", "",0,1);
$x3_button = $linterface->GET_BTN($button_add, "button", "jADD_USER_EMAIL()","add1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ");
$x3_button2 = $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['AliasEmailID[]'])","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ");

if ($curAction == "add")
{
	if ($addAction == "user_email")
	{
		if (is_array($AliasEmailID) && count($AliasEmailID)>0)
		{		    
		    $AliasImplode = implode(",",$AliasEmailID);
		    
		    $addressSql = "
		    					SELECT 
		    						TargetName,TargetAddress 
		    					FROM 		    						
		    						INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
		    					WHERE 
		    						AddressID IN ($AliasImplode) 
		    					";
		     $targets = $li->returnArray($addressSql,2);
     
		     for ($i=0; $i<sizeof($targets); $i++)
		    {
		         list($name, $address) = $targets[$i];
		         if ($name=="" || $name==$address)
		         {
		             $target_string = $address;
		         }
		         else
		         {
		             $target_string = "$name <$address>";
		         }
		
		         $text_recipients .= "$target_string; ";
		    }
		    if ($text_recipients != "")
		    {
		    ?>
		        <script language="javascript" >
		        obj = opener.window.document.form1.<?=$fieldname?>;
		        s1 = Trim(obj.value);
		        strlength = s1.length;
		        if (strlength != 0 && s1.charAt(strlength-1)!=";")
		        {
		            obj.value = obj.value + "; ";
		        }
		        
		        var SelectedEmailArray = s1.split(";");
		        var temp_str = ("<?=addslashes(intranet_undo_htmlspecialchars($text_recipients))?>");
		        temp_str = Trim(temp_str);
		        var NewEmailArray = temp_str.split(";");
		        var new_str = "";
		        for(a=0; a<NewEmailArray.length; a++)
				{
					flag = false;
					if(SelectedEmailArray.length > 0)
					{
						for(b=0; b<SelectedEmailArray.length; b++)
						{
							if(NewEmailArray[a] == SelectedEmailArray[b])
							{
								flag = true;
								break;
							}
						}
					}
					if(flag == false)
					{
						if(NewEmailArray[a].replace(/(^\s*)|(\s*$)/g, "").length > 0)
						{
							new_str+=NewEmailArray[a]+";";
						}
					}
				}
				obj.value += new_str;
		        
		        </script>
		    <?
		    } 
		}		
	}			
}
	

?>
<script language="javascript">
function addAddress(obj)
{
         obj.selected.value = 1;
         checkRestore(obj,'AddressID[]','index.php');
         if (countChecked(obj,'AddressID[]')==0)
         {
             obj.selected.value = 0;
         }
}
</script>
<script language="javascript">
	function SelectAll(obj)
	{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
	}

     function jADD_USER()
     {	     
	     document.form1.curAction.value = "add";
	     document.form1.addAction.value = "user";	     
	     document.form1.submit();	     
     }
     
     function jADD_GROUP()
     {
	     document.form1.curAction.value = "add";
	     document.form1.addAction.value = "group";	     
	     document.form1.submit();	     
     }

     function jADD_USER_EMAIL()
     {	     
	     document.form1.curAction.value = "add";
	     document.form1.addAction.value = "user_email";	     
	     document.form1.submit();	     
     }
          
     function jEXPAND_ALIAS()
     {
	     document.form1.curAction.value = "expand";
	     document.form1.expandAction.value = "alias";	     
	     document.form1.submit();
     }

     function jADD_ALIAS()
     {
	     document.form1.curAction.value = "add";
	     document.form1.addAction.value = "alias";	     
	     document.form1.submit();
     }
               
</script>

<form name="form1" action="index.php" method="post" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr><td><?=$button_search;?> : <input type='text' name='keyword' class='formtextbox'> <?= $linterface->GET_ACTION_BTN($button_search, "submit") ?></td></tr>
<tr>
	<td>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">	
	<tr>
		<td>

		<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tabletext" >
							
		<tr> 			
			<td valign="top" nowrap="nowrap" width="30%" >
			<span class="tabletext"><?=$x3_text?>:</span>
			</td>
			<td >					
			<table cellpadding="0" cellspacing="0" >
			<tr>
				<td><?=$x3?></td>
				<td style="vertical-align:bottom">
				
				<table cellpadding="0" cellspacing="6" >				
				<tr>
					<td>
					<?=$x3_button?>
					</td>
				</tr>	
				<tr>
					<td>
					<?=$x3_button2?>
					</td>
				</tr>					
				</table>
				
				</td>
			</tr>
			</table>			
			</td>
		</tr>				

		</table>
		
		</td>
	</tr>		
	</table>   	      
	</td>
</tr>
<tr>
	<td>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>		
<br />

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="selected" value="0">
<input type="hidden" name="fieldname" value="<?=$fieldname?>">

<input type="hidden" name="curAction" value="">
<input type="hidden" name="expandAction" value="<?=$expandAction?>">
<input type="hidden" name="addAction" value="">

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>