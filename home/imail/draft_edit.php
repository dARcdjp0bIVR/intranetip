<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libcampusmail.php");
include("../../includes/libfilesystem.php");
include("../../includes/libfiletable.php");
include("../../includes/libaccess.php");
include("../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
auth_campusmail();
if (!auth_sendmail())
{
    header("Location: index.php");
    exit();
}

$li = new libcampusmail($CampusMailID);
$row = $li->returnRecipientOption($li->RecipientID);
if(!isset($_SESSION["composeFolder"])){
     $_SESSION["composeFolder"] = $li->Attachment;
}
$composeFolder = $li->Attachment;
$personal_path = "$file_path/file/mail/u$UserID";
if (!is_dir($personal_path))
{
    $li->folder_new($personal_path);
}

$path = "$file_path/file/mail/".$li->Attachment;

$lu = new libfilesystem();
$lu->folder_new($path);
if (!is_dir($path))
{
     header("Location: draft.php");
     exit();
}
$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;
if ($cmsg == 1)
{
    $xcmsg = $i_campusmail_novaliduser;
}

$navigation = $i_frontpage_separator.$i_frontpage_schoolinfo.$i_frontpage_separator.$i_frontpage_campusmail_campusmail;
include("../../templates/homeheader.php");
?>

<script language="javascript">
function checkform(obj){
     if(obj.elements["Recipient[]"].length==0){ alert("<?php echo $i_frontpage_campusmail_choose; ?>"); return false; }
     if(!check_text(obj.Subject, "<?php echo $i_alert_pleasefillin.$i_frontpage_campusmail_subject; ?>.")) return false;
     if(!check_text(obj.Message, "<?php echo $i_alert_pleasefillin.$i_frontpage_campusmail_message; ?>.")) return false;
     checkOptionAll(obj.elements["Recipient[]"]);
     checkOptionAll(obj.elements["Attachment[]"]);
}
</script>
<form name="form1" method="post" action="draft_update2.php" onSubmit="return checkform(this);">
      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td><?=$i_frontpage_campusmail_title_compose?></td>
        </tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td></td>
        </tr>
      </table>
      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="474" valign="top">
          <?php include("menu.php"); ?>
          </td>
          <td width="320" valign="top" align="center"><div id=msg><?=$xcmsg?></div>
          </td>
        </tr>
      </table>
      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="66" valign="top">
            <p><br>
              <?=$i_campusmail_lettertext?><br>
              <br>
              <br>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p><br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <img src="/images/campusmail/letterbottom.gif"> </p>
          </td>
          <td width="728" align="left" valign="top">
              <table width="685" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><img src="/images/campusmail/papertop.gif" width="685" height="10"></td>
              </tr>
            </table>
                 <table width="685" border="0" cellspacing="0" cellpadding="2" background="/images/campusmail/paperbg2.gif" class="body">
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td width="230" align="left" valign="top">&nbsp;</td>
                  <td width="335" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120"><?=$i_frontpage_campusmail_recipients?> :</td>
                  <td rowspan="2" align="left" valign="top">
<select name=Recipient[] size=4 multiple><option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select></td>
                  <td width="335" align="left" valign="top"><a href=javascript:newWindow("choose/index.php?fieldname=Recipient[]",9)><img src="<?=$image_chooserecipient?>" border=0></a></td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td width="335" align="left" valign="top"><a href=javascript:checkOptionRemove(document.form1.elements["Recipient[]"])><img src="<?=$image_removeselected?>" border=0></a></td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td width="230" align="left" valign="top">&nbsp;</td>
                  <td width="335" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120"><?=$i_frontpage_campusmail_subject?> :</td>
                  <td colspan="2" align="left" valign="top">
                  <input class=text type=text name=Subject size=80 maxlength=255 value="<?php echo $li->Subject; ?>"></td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td width="230" align="left" valign="top">&nbsp;</td>
                  <td width="335" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120"><?=$i_frontpage_campusmail_message?>:</td>
                  <td colspan="2" align="left" valign="top">
                      <textarea name=Message cols=80 rows=10><?php echo $li->Message; ?></textarea>
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td width="230" align="left" valign="top">&nbsp;</td>
                  <td width="335" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120"><?=$i_frontpage_campusmail_attachment?> :</td>
                  <td rowspan="2" align="left" valign="top">
                   <select name=Attachment[] size=4 multiple><option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select>
                  </td>
                  <td width="335" align="left" valign="top"><a href=javascript:newWindow("attach.php?isdraftedit=1&folder=<?php echo $composeFolder; ?>",2)><img border=0 src="<?=$image_attachfiles?>"> <?=$i_frontpage_campusmail_icon_attachment?></a></td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td width="335" align="left" valign="top"><a href=javascript:checkOptionRemove(document.form1.elements["Attachment[]"])><img border=0 src="<?=$image_removeselected?>"></a></td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td width="230" align="left" valign="top">&nbsp;</td>
                  <td width="335" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120"><?="$i_frontpage_campusmail_icon_important $i_frontpage_campusmail_important"?> :</td>
                  <td width="230" align="left" valign="top">
                    <input type=checkbox name=IsImportant value=1 <?php if($li->IsImportant) { echo "CHECKED"; } ?>>
                  </td>
                  <td width="335" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td width="230" align="left" valign="top">&nbsp;</td>
                  <td width="335" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120"><?="$i_frontpage_campusmail_icon_notification $i_frontpage_campusmail_notification"?> :</td>
                  <td width="230" align="left" valign="top">
                    <input type=checkbox name=IsNotification value=1 <?php if($li->IsNotification) { echo "CHECKED"; } ?>>
                  </td>
                  <td width="335" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td width="230" align="left" valign="top">&nbsp;</td>
                  <td width="335" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td width="230" align="left" valign="top">&nbsp;</td>
                  <td width="335" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td colspan="2" align="left" valign="top">
<input type=image src=<?=$image_send?> onClick="checkOption(this.form.elements['Attachment[]']); checkOption(this.form.elements['Recipient[]']);this.form.SubmitType.value=0;">
<input type=image src=<?=$image_save?> onClick="checkOption(this.form.elements['Attachment[]']); checkOption(this.form.elements['Recipient[]']);this.form.SubmitType.value=1;">
<input type=image src=<?=$image_reset?> onClick="this.form.reset(); return false;">
                </tr>
              </table>
              <table width="685" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><img src="/images/campusmail/paperbottom.gif" width="685" height="70"></td>
                </tr>
              </table>
          </td>
        </tr>
      </table>
<input type=hidden name=SubmitType value=0>
<input type=hidden name=CampusMailID value=<?=$CampusMailID?>>
</form>

<script language="JavaScript1.2">
obj = document.form1.elements["Attachment[]"];
checkOptionClear(obj);
<?php while (list($key, $value) = each($files)) echo "checkOptionAdd(obj, \"".$files[$key][0]." (".ceil($files[$key][1]/1000)."Kb".")\", \"".$files[$key][0]."\");\n";  ?>
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
obj = document.form1.elements["Recipient[]"];
checkOptionClear(obj);
<?php for($i=0; $i<sizeof($row); $i++){ echo "checkOptionAdd(obj, \"".$row[$i][1]."\", \"".$row[$i][0]."\");\n"; } ?>
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
</script>

<?php
include("../../templates/homefooter.php");
intranet_closedb();
?>