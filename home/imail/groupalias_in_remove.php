<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccess.php");
include_once("../../includes/libimailext.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

$tablename = "INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY";
$list = implode(",",$EntryID);
$li = new libimailext();
$sql = "DELETE FROM $tablename WHERE AliasID = '$AliasID' AND EntryID IN ($list)";
$li->db_db_query($sql);

$li->synAliasCount($AliasID,$aliastype);

$anc = $special_option['no_anchor'] ?"":"#anc";
header("Location: groupalias_view_in.php?aliastype=$aliastype&AliasID=$AliasID&pageNo=$pageNo&order=$order&field=$field&TabID=$TabID&xmsg=delete".$anc);
intranet_closedb();
?>