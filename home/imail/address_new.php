<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;
$CurrentPage = "PageAddressBook_ExternalReceipient";

$lwebmail = new libwebmail();
$linterface = new interface_html();

if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) && $lwebmail->type == 3)
{
    include_once($PATH_WRT_ROOT."includes/libsystemaccess.php");
    $lsysaccess = new libsystemaccess($UserID);
    if ($lsysaccess->hasMailRight())
    {
        $noWebmail = false;
    }
    else
    {
        $noWebmail = true;
    }
}
else
{
    $noWebmail = true;
}

$noWebmail = false;
if ($noWebmail || !is_numeric ($numToAdd) || $numToAdd < 1)
{
    header("Location: addressbook.php");
}

$li = new libdb();

### Title ###
$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_address_ex.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='imailpagetitle'>". $i_CampusMail_External_Recipient ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleImage1.$TitleTitle1."</td></tr></table>";
$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($TitleTitle, "", 0);

$linterface->LAYOUT_START();
$PAGE_NAVIGATION[] = array($button_new .($intranet_session_language=="en"?" ":""). $i_Circular_Recipient, "");
?>

<br />
<script language='javascript'>
function checkform(obj){
	if(obj==null) return false;
		if(!validateEmailAddresses(<?=$numToAdd?>)) return false;
	return checknames(<?=$numToAdd?>);
}
function checknames(n){
	for(i=0;i<n;i++){
		objs = document.getElementsByName('RecordName'+i);
		if(objs.length>0){
			obj = objs[0];
			if(obj.value.indexOf(";")>-1){
				alert("<?=$i_CampusMail_Warning_External_Recipient_No_Semicolon?>");
				obj.focus();
				return false;
			}
			if(obj.value.indexOf("<")>-1){
				alert("<?=$Lang['iMail']['JSWarning']['ExternalRecipientNoTriangleBrackets1'];?>");
				obj.focus();
				return false;
			}
			if(obj.value.indexOf(">")>-1){
				alert("<?=$Lang['iMail']['JSWarning']['ExternalRecipientNoTriangleBrackets2'];?>");
				obj.focus();
				return false;
			}
		}else continue;
	}
	return true;
}
function validateEmailAddresses(n){
	for(i=0;i<n;i++){
		objs = document.getElementsByName('RecordEmail'+i);
		objs2 = document.getElementsByName('RecordName'+i);
		if(objs.length>0){
			objMail = objs[0];
			objName = objs2[0];
			if(objMail.value=="" && objName.value=="") continue;
			if(!validateEmailAddress(objMail.value)){
				alert("<?=$i_invalid_email?>");
				objMail.focus();
				return false;
			}
		}else continue;
	}
	return true;
	
}
function validateEmailAddress(email){

        //var re = /^.+@.+\..{2,3}$/;
        var re = /^([0-9A-Za-z_\.\-])+\@(([0-9A-Za-z\-])+\.)+([0-9A-Za-z]{2,3})+$/
        
        if (re.test(email)) {
                return true;
        }else{
                return false;
        }
}
</script>
<form name="form1" method="post" ACTION="address_new_update.php" onsubmit='return checkform(this)'>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$xmsg?></td>
</tr>
<tr>
	<td colspan="2" align="center"><br />
        	<table width="90%" border="0" cellpadding="0" cellspacing="0">
		<tr>
	             	<td>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <tr class="tabletop">
                                	<td width="15" class="tabletoplink">#</td>
                                        <td width="50%" class="tabletoplink"><?=$i_CampusMail_New_AddressBook_Name?></td>
					<td width="50%" class="tabletoplink"><?=$i_CampusMail_New_AddressBook_EmailAddress?></td>
				</tr>

				<?  for ($i=0; $i<$numToAdd; $i++) { ?>
				<tr valign="top">
                                	<td valign="top" nowrap="nowrap"><span class="tabletext"><?=($i+1)?>. </span></td>
                                        <td width="50%"><input name="RecordName<?=$i?>" type="text" class="textboxtext" MAXLENGTH="255"></td>
                                        <td width="50%"><input name="RecordEmail<?=$i?>" type="text" class="textboxtext" MAXLENGTH="255"></td>
                                </tr>
                                <? } ?>
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>        
<tr>
	<td colspan="2" align="center">
        	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back();","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table> 
        </td>
</tr>        
</table>
                              
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="keyword" value="<?php echo $keyword; ?>">
<input type="hidden" name="numToAdd" value="<?=$numToAdd?>">
<input type="hidden" name="TabID" value="<?=$TabID?>">
</form>

<?
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.RecordName0");
$linterface->LAYOUT_STOP();
?>