<?php
// Editing by 
/*
 * 2019-08-30 Ray   : Added CKEditor
 * 2015-08-14 Carlos: Change form method from GET to POST, as tags in $_GET would be stripped at lib.php.
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
#include_once("../../includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

if ( $UserID=="")
{
    header("Location: index.php");
    exit();
}

if ($special_feature['imail_richtext'] && !($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod"))
{
    $use_html_editor = true;
}
if($TabID=="") $TabID=2;
$li = new libdb();
$sql ="SELECT Signature FROM INTRANET_IMAIL_PREFERENCE WHERE USerID='$UserID'";
$result = $li->returnVector($sql);

$signature = $result[0];

if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
{
	if ($signature!=strip_tags($signature))
	{
		$signature = strip_tags(convert_line_breaks($signature));
	}
} else
{
	if ($signature==strip_tags($signature))
	{
		$signature = nl2br($signature);
	}
}


if(!$use_html_editor)
{
	$signature = str_replace("\n","\n>",$signature);
	$signature = intranet_htmlspecialchars($signature);
	$signature = str_replace("&amp;","&",$signature);
}
$navigation = $i_frontpage_separator.$i_frontpage_schoolinfo.$i_frontpage_separator.$i_frontpage_campusmail_campusmail;

$lwebmail = new libwebmail();

$CurrentPage = "PageSettings_Signature";
$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($i_CampusMail_New_Settings_Signature, "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

if ($msg == "2")
{
	$xmsg = $linterface->GET_SYS_MSG("update");
} else {
	$xmsg = "&nbsp;";
}

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autogrowtextarea.js"></script>
<SCRIPT LANGUAGE='JAVASCRIPT'>
function resetForm(obj)
{
	location.href='pref_signature.php?TabID=<?=$TabID?>';
}

<?php
if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
{
?>
$(document).ready(function (){ 
	$('#signature').autoGrow();
});
<?php
}
?>
</SCRIPT>
<form name="form1" method="post" action="pref_update.php" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">	
	<tr>
		<td colspan="2" align="right" ><?=$xmsg?></td>
	</tr>       	
	<tr>
		<td align="left" colspan="2" class="tabletext" ><?=$i_CampusMail_New_Settings_PleaseFillInYourSignature?></td>
	</tr>
	<tr>
		<td align="left" colspan="2" >
		<?php 
		if ($use_html_editor) 
		{
			# Components size
			$msg_box_width = "100%";
			$msg_box_height = 250;
			$obj_name = "signature";
			$editor_width = $msg_box_width;
			$editor_height = $msg_box_height;
			$init_html_content = $signature;
			//include($PATH_WRT_ROOT."includes/html_editor_embed2.php");
            if($sys_custom['iMail']['CKEditor']) {
				echo getCkEditor("signature", $signature, "default",$editor_height, $editor_width);
            } else {
                include_once($PATH_WRT_ROOT . 'templates/html_editor/fckeditor.php');
                $html_editor = new FCKeditor('signature');
                $html_editor->BasePath = $PATH_WRT_ROOT . 'templates/html_editor/';
                $html_editor->Config['SkinPath'] = 'skins/silver/';
                $html_editor->ToolbarSet = 'Basic2';
                $html_editor->Value = $signature;
                $html_editor->Create();
            }
		} else { 
		?>
			<textarea rows="10" cols="80%" name="signature" id="signature" ><?=$signature?></textarea>
		<?php
		}
		?>	
	</table>   	      
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "button", "resetForm(document.form1)") ?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="TabID" value="<?=$TabID?>" />
</form>
<?php 
	if (!$use_html_editor) 
	{
		echo $linterface->FOCUS_ON_LOAD("form1.signature");
	}	
	
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>