<?php
// Editing by 
/***************************************
 *  modification log
 *  2015-02-25 (Carlos): Move [Empty Trash] button to LHS.
 * 						$sys_custom['iMailHideEmptyTrashButton'] can hide the button. (avoid delete all emails by accident)	
 * 
 * 	20100621 Marcus:
 * 			check $SYS_CONFIG['Mail']['hide_imail'] , if true , open interface_html with "imail_archive.php"
 * 
 * **************************************/
 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

if(isset($sys_custom['custom_imail_page_size'])){
	$page_size = $sys_custom['custom_imail_page_size'];
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$curDateFormat = date("Y-m-d");

$lwebmail = new libwebmail();

if($field=="") $field = 4;
$li = new libdbtable2007($field, $order, $pageNo);

/*
$sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE Deleted = 1 AND UserID = '$UserID' AND UserFolderID != -1";
$arr_result = $li->returnVector($sql);
if($arr_result[0] > 0)
{
	$sql = "UPDATE INTRANET_CAMPUSMAIL SET UserFolderID = -1 WHERE UserID = '$UserID' AND Deleted = 1";
	$li->db_db_query($sql);
}
*/
# Find unread mails number
/*
$sql  = "
			SELECT
				count(a.CampusMailID)
          	FROM 
          		INTRANET_CAMPUSMAIL AS a LEFT OUTER JOIN INTRANET_USER AS b
          	ON 
          		a.SenderID = b.UserID
			WHERE
               a.UserID = $UserID AND
               a.UserFolderID = 2 AND
               a.Deleted != 1 AND	               	               
               (a.RecordStatus = '' OR a.RecordStatus IS NULL)
          ";
$row = $li->returnVector($sql);	          
$unreadInboxNo = $row[0];
*/

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
$RecordType = array(3,4,5);
$li->field_array = array(
                         "a.IsImportant",
                         "a.IsNotification",
                         "UserName",                         
                         "a.Subject",
                         "a.DateInput",
                         "a.AttachmentSize",
                         "a.UserFolderID"
                         );
                         /*
$chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
$username_field = ($chi? "ChineseName": "EnglishName");
*/
$username_field = getNameFieldWithClassNumberByLang("b.");
$FolderID = -1;
if($sys_custom['iMail_iFrameSubject'] == '' || $sys_custom['iMail_iFrameSubject'] == false)
{
	$sql  = "SELECT
					IF(a.IsImportant=1,'".addslashes($i_frontpage_campusmail_icon_important2_2007)."','<br>'),
					IF(a.IsNotification=1,'".addslashes($i_frontpage_campusmail_icon_notification2_2007)."','<br>'),
					
					IF(a.MailType=1,
						IF(a.RecordStatus='1' OR a.RecordStatus='2',
						IF(a.SenderID = 0, '$i_general_sysadmin', CONCAT($username_field)),
						IF(a.SenderID = 0, '$i_general_sysadmin', CONCAT($username_field))),
						IF(a.RecordStatus='1' OR a.RecordStatus='2',CONCAT(a.SenderEmail),CONCAT(a.SenderEmail)) 
					) as UserNameWeb,
					' ',               
					
					IF ((a.Subject = '') OR (a.Subject IS NULL),'&nbsp;',
					IF(a.RecordStatus='1' OR a.RecordStatus='2',
					IF(a.IsAttachment=1,
					CONCAT('<span class=\'iMailsubject\' >".addslashes($i_frontpage_campusmail_icon_attachment_2007)."' , a.Subject, '</span>'),
					CONCAT('<span class=\'iMailsubject\' >', a.Subject, '</span>')),
					IF(a.IsAttachment=1,
					CONCAT('<span class=\'iMailsubjectunread\' >".addslashes($i_frontpage_campusmail_icon_attachment_2007)."' , a.Subject, '</span>'),
					CONCAT('<span class=\'iMailsubjectunread\' >', a.Subject, '</span>')))),
				   
					' ',
					IF ((DATE_FORMAT(a.DateInput, '%Y-%m-%d') = '{$curDateFormat}'),
					CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%H:%i'),'</span>'),
					CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%Y-%m-%d'),'</span>')),
					' ',
					IF(a.IsAttachment=1,
					IF (a.AttachmentSize IS NULL,'0', CONCAT(a.AttachmentSize,'')),'-'),
					' ',
					c.FolderName,
					CONCAT('<input type=\'checkbox\' name=\'CampusMailID[]\' value=', a.CampusMailID ,' />'),
					IF (a.RecordStatus IS NULL, 0, a.RecordStatus),
					
					IF(a.MailType=1,
						IF(a.SenderID = 0, '$i_general_sysadmin', $username_field),
						a.SenderEmail
					) as UserName,
					CONCAT('TRASH')

				FROM INTRANET_CAMPUSMAIL AS a LEFT OUTER JOIN INTRANET_USER AS b ON a.SenderID = b.UserID
					LEFT OUTER JOIN INTRANET_CAMPUSMAIL_FOLDER as c ON a.UserFolderID = c.FolderID
				WHERE
					a.UserID = '$UserID' AND
					a.Deleted = 1

			  ";
}
else
{
	$sql  = "SELECT
					IF(a.IsImportant=1,'".addslashes($i_frontpage_campusmail_icon_important2_2007)."','<br>'),
					IF(a.IsNotification=1,'".addslashes($i_frontpage_campusmail_icon_notification2_2007)."','<br>'),
					
					IF(a.MailType=1,
						IF(a.RecordStatus='1' OR a.RecordStatus='2',
						IF(a.SenderID = 0, '$i_general_sysadmin', CONCAT($username_field)),
						IF(a.SenderID = 0, '$i_general_sysadmin', CONCAT($username_field))),
						IF(a.RecordStatus='1' OR a.RecordStatus='2',CONCAT(a.SenderEmail),CONCAT(a.SenderEmail)) 
					) as UserNameWeb,
					' ',               
					
					IF ((a.Subject = '') OR (a.Subject IS NULL),'&nbsp;',
					IF(a.RecordStatus='1' OR a.RecordStatus='2',				
					IF(a.IsAttachment=1,
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=20 scrolling=no src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=0></iframe>'),
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubject\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', a.Subject, '</a>\n')
							),
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=20 scrolling=no src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=0></iframe>'),
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubject\' >', a.Subject, '</a>\n')
							)),
					   IF(a.IsAttachment=1,
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=20 scrolling=no src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=1></iframe>'),
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubjectunread\' > ".addslashes($i_frontpage_campusmail_icon_attachment_2007)."', a.Subject, '</a>\n')
							),
							IF(a.MessageEncoding != 'big5',
								CONCAT('<iframe frameborder=0 id=imail_subject_frame width=350 height=20 scrolling=no src=viewmail_subject.php?CampusMailID=',a.CampusMailID,'&FolderID={$FolderID}&unread=1></iframe>'),
								CONCAT('<a href=\'viewmail.php?CampusMailID=', a.CampusMailID, '&preFolder={$FolderID}\' class=\'iMailsubjectunread\' >', a.Subject, '</a>\n')
							))
					)),
				   
					' ',
					IF ((DATE_FORMAT(a.DateInput, '%Y-%m-%d') = '{$curDateFormat}'),
					CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%H:%i'),'</span>'),
					CONCAT('<span title=\"',DATE_FORMAT(a.DateInput, '%Y-%m-%d %H:%i'),'\">',DATE_FORMAT(a.DateInput, '%Y-%m-%d'),'</span>')),
					' ',
					IF(a.IsAttachment=1,
					IF (a.AttachmentSize IS NULL,'0', CONCAT(a.AttachmentSize,'')),'-'),
					' ',
					c.FolderName,
					CONCAT('<input type=\'checkbox\' name=\'CampusMailID[]\' value=', a.CampusMailID ,' />'),
					IF (a.RecordStatus IS NULL, 0, a.RecordStatus),
					
					IF(a.MailType=1,
						IF(a.SenderID = 0, '$i_general_sysadmin', $username_field),
						a.SenderEmail
					) as UserName,
					CONCAT('TRASH')

				FROM INTRANET_CAMPUSMAIL AS a LEFT OUTER JOIN INTRANET_USER AS b ON a.SenderID = b.UserID
					LEFT OUTER JOIN INTRANET_CAMPUSMAIL_FOLDER as c ON a.UserFolderID = c.FolderID
				WHERE
					a.UserID = '$UserID' AND
					a.Deleted = 1

			  ";
}
# TABLE INFO
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+7;
#$li->IsColOff = 1;
$li->IsColOff = "imail";
$li->title = "";
// TABLE COLUMN
$li->column_list .= "<td width='23' class='tabletop' >".$li->column_image(0, $i_frontpage_campusmail_important, $i_frontpage_campusmail_icon_important_2007)."</td>\n";
$li->column_list .= "<td width='27' class='tabletop' >".$li->column_image(1, $i_frontpage_campusmail_notification, $i_frontpage_campusmail_icon_notification_2007)."</td>\n";
$li->column_list .= "<td class='tabletop' >".$li->column(2, $i_frontpage_campusmail_sender)."</td>\n";
$li->column_list .= "<td width='2' class='tabletop' >&nbsp;&nbsp;</td>\n";
$li->column_list .= "<td class='tabletop' >".$li->column(3, $i_frontpage_campusmail_subject)."</td>\n";
$li->column_list .= "<td width='2' class='tabletop' >&nbsp;&nbsp;</td>\n";
$li->column_list .= "<td class='tabletop' >".$li->column(4, $i_frontpage_campusmail_date)."</td>\n";
$li->column_list .= "<td width='2' class='tabletop' >&nbsp;&nbsp;</td>\n";
$li->column_list .= "<td class='tabletop' >".$li->column(5, $i_frontpage_campusmail_size)."</td>\n";
$li->column_list .= "<td width='2' class='tabletop' >&nbsp;&nbsp;</td>\n";
$li->column_list .= "<td width='73' class='tabletop' >".$li->column(6, $i_frontpage_campusmail_folder)."</td>\n";
$li->column_list .= "<td width='40' class='tabletop' >".$li->check("CampusMailID[]")."</td>\n";
$li->column_array = array(0,0,5,0,5,0,11,0,0,0,0,0);
$li->wrap_array = array(0,0,14,0,40,0,0,0,0,0,0,0);

$lc = new libcampusquota2007($UserID);

$folder_sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID = $UserID OR RecordType = 0 ORDER BY RecordType, FolderName";
$folders = $lc->returnArray($folder_sql,2);
$folders[] = array(-1,$i_admintitle_im_campusmail_trash);

### For new Interface ###
$CurrentPage = "PageCheckMail_Trash";

$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();

# tag information
$iMailImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_trash.gif' width='20' height='20' align='absmiddle' />";
$iMailTitle1 = "<span class='imailpagetitle'>{$i_frontpage_campusmail_trash}</span>";
$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$iMailImage1.$iMailTitle1.$unreadLink."</td><td align='right' style=\"vertical-align: bottom;\" >".$lc->displayiMailFullBar()."</td></tr></table>";
$TAGS_OBJ[] = array($iMailTitle, "", 0);

if($SYS_CONFIG['Mail']['hide_imail']===true)
	$linterface = new interface_html("imail_archive.html");
else
	$linterface = new interface_html("imail_default.html");
    $linterface->LAYOUT_START();

hdebug($i_con_msg_email_restore);
if ($msg == "2")
{
	$xmsg = $linterface->GET_SYS_MSG("update");
} 
else if ($msg == "3")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("delete")."</td></tr>";
} 
else if ($msg == "4")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_gen_msg_email_restore."</font>")."</td></tr>";
} 
else 
{
	$xmsg = "&nbsp;";
}

$exportBtn = "<a href='javascript:openExport()' class='contenttool' >".exportIcon3()." ".$button_export."</a>";

$restoreBtn   = "<a class=\"tabletool\" href=\"javascript:checkRestore(document.form1,'CampusMailID[]','trash_restore.php')\" >";
$restoreBtn  .= "{$i_frontpage_campusmail_icon_restore_2007} {$button_restore}</a>";

$deleteBtn  = "<a href=\"javascript:checkRemove(document.form1,'CampusMailID[]','trash_remove.php')\" class=\"tabletool\">";
$deleteBtn .= "{$i_frontpage_campusmail_icon_trash_2007} {$button_remove}</a>";

$emptyBtn  = "<a href=\"javascript:AlertPost(document.form1,'empty_trash.php','$i_campusmail_alert_emptytrash')\" class=\"tabletool\" >";
$emptyBtn .= "{$i_frontpage_campusmail_icon_empty_trash_2007} {$button_emptytrash}</a>";

##################


?>

<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>		
	<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">	
	<?=$xmsg?>
	<tr>
		<td >		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr class="table-action-bar">
			<td>
			<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td><p>&nbsp;</p></td>
			</tr>
			</table>
			</td>		
			<td align="right" style="vertical-align:bottom">
			<table border="0" cellspacing="0" cellpadding="2">
			<tr>
			<?php if(!$sys_custom['iMailHideEmptyTrashButton']){ ?>
				<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></td>	
				<td><?=$emptyBtn?></td>	
			<?php } ?>
				<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></td>
				<td><?=$restoreBtn?></td>
				<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></td>
				<td><p><?=$deleteBtn?></p></td>	
			</tr>
			</table>
			</td>
		</tr>
		</table>				
		</td>
	</tr>
	<tr>
		<td width="100%" >
		<?=$li->display()?>
		</td>
	</tr>
	</table>
	<br />
	</td>
</tr>
</table>
<input type='hidden' name='pageNo' value="<?php echo $li->pageNo; ?>" />
<input type='hidden' name='order' value="<?php echo $li->order; ?>" />
<input type='hidden' name='field' value="<?php echo $li->field; ?>" />
<input type='hidden' name='page_size_change' value="" />
<input type='hidden' name='numPerPage' value="<?=$li->page_size?>" />
</form>
<?php	
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>