<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libcampusmail.php");
include_once("../../includes/libcampusquota.php");
include_once("../../includes/libwebmail.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lwebmail = new libwebmail();
if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID))
{
    $noWebmail = false;
}
else
{
    $noWebmail = true;
}

if (!$noWebmail )
{
     # Grab Internet E-Mail
     $lc_temp = new libcampusquota($UserID);
     $total_quota = $lc_temp->returnQuota();
     $used_quota = $lc_temp->returnUsedQuota();
     $left = $total_quota*1024 - $used_quota;
     if ($left > 0)
     {
         include_once("../../includes/libuser.php");
         include_once("../../includes/libfiletable.php");
         $lu = new libuser($UserID);
         $inbox = $lwebmail->openInbox($lu->UserLogin,$lu->UserPassword);
         $mail_entry = $lwebmail->checkOneMail($left);


         # Put mails into INTRANET_CAMPUSMAIL
         $mail_id = $mail_entry[0];
         if ($mail_id != "")
         {
              list($mail_id,$subject,$message,$isHTML,$curr_attach_size,$fromaddress,$toaddress,$ccaddress,$dateReceived,$attachment_path) = $mail_entry;
              #print_r($mail_entry);
              #exit();
              $lc = new libcampusmail();
              $message = $lc->convert_image_embeded_message($message, $lwebmail->embed_part_ids);

              $subject = $lc->convertBadWords($subject);
              $message = $lc->convertBadWords($message);
              $subject = addslashes($subject);
              $message = addslashes($message);
              $fromaddress = addslashes($fromaddress);
              $toaddress = addslashes($toaddress);
              $ccaddress = addslashes($ccaddress);
              if ($curr_attach_size > 0)
              {
                  $is_attach = 1;
                  $curr_attach_size = ceil($curr_attach_size);
              }
              $sql = "INSERT INTO INTRANET_CAMPUSMAIL (
                      UserID,UserFolderID,SenderEmail,RecipientID,InternalCC,ExternalTo,ExternalCC,
                      Subject,Message,Attachment,IsAttachment,AttachmentSize,IsImportant,
                      IsNotification,
                      MailType,RecordType,RecordStatus,DateInput,DateModified
                      )
                      VALUES
                      ($UserID,2,'$fromaddress','','','$toaddress','$ccaddress',
                      '$subject','$message','$attachment_path','$is_attach','$curr_attach_size',0,0,
                      2,2,NULL,'$dateReceived','$dateReceived')";

              $lc_temp->db_db_query($sql);

              # Change Attachment Scheme
              if ($is_attach == 1)
              {
                  $temp_campusmail_id = $lc_temp->db_insert_id();
                  $lftable = new libfiletable("","$intranet_root/file/mail/$attachment_path", 0,0,"");
                  $files = $lftable->files;    # Array (filename, filesize, filetime)
                  $temp_sql_values = "";
                  $delim = "";
                  while (list($key, $value) = each($files))
                  {
                         $temp_filename = $files[$key][0];
                         $is_embed = 0;
                         for ($i=0; $i<sizeof($lwebmail->embed_part_ids); $i++)
                         {
                              list($c_id, $c_filename) = $lwebmail->embed_part_ids[$i];
                              if ($c_filename == $temp_filename)
                              {
                                  $is_embed = 1;
                                  break;
                              }
                         }
                         $temp_filename = addslashes($temp_filename);
                         $temp_filesize = ceil($files[$key][1]/1000);
                         $temp_sql_values .= "$delim($temp_campusmail_id,'$attachment_path','$temp_filename','$temp_filesize','$is_embed')";
                         $delim = ",";
                  }
                  $temp_attach_sql = "INSERT IGNORE INTO INTRANET_IMAIL_ATTACHMENT_PART
                                     (CampusMailID,AttachmentPath,FileName,FileSize,isEmbed)
                                     VALUES $temp_sql_values";
                  $lc_temp->db_db_query($temp_attach_sql);
              }
              $imail_received++;
              $lwebmail->removeMail($mail_id);
              #exit();
              $checkmail_status = 1;                      # Successful got, submit again
         }
         else
         {
             # Can't get mail
             $exmail_left = $lwebmail->checkMailCount();
             if ($exmail_left > 0)
             {
                 $checkmail_status = 2;                 # No mails
             }
             else
             {
                 $checkmail_status = 0;                 # Not enough quota
             }
         }

         $lwebmail->close();
     }

     # Response
     if ($checkmail_status == 1)
     {
         $body_tags = "onLoad=process(this)";
         if ($imail_extotal == 0)
         {
             $pused = "50";
             echo "re: $imail_received";
             echo "/total: $imail_extotal";
         }
         else $pused = floor($imail_received/$imail_extotal*100);
         $pbar = "<TABLE width=80% align=left><tr><td style=\"font-size:10px;\"></td>";
         $pbar .= "<td width=100% class=td_left_middle>
<table cellpadding=0 border=0 cellspacing=0 style=\"border: #104a7b
1px solid; padding:1px; padding-right: 0px; padding-left: 0px;\" width=100%>
<tr>
<td width=100% class=td_left_middle bgcolor=white>
<div style=\"height:6px; width:$pused%; font-size:3px;
background-color:#CECFFF\"></div>
</td>
</tr>
</table>
</td>";
         $pbar .= "<td style=\"font-size:10px;\"></td></tr>";
         $pbar .= "</table>";
         $html_text = "$pbar <br>";
         $html_text .= "$i_CampusMail_New_Receiving1 $imail_received $i_CampusMail_New_Receiving2 $imail_extotal $i_CampusMail_New_Receiving3";
     }
     else if ($checkmail_status == 2)
     {
          $body_tags = "";
         if ($imail_extotal == 0)
         {
             $pused = "50";
             echo "re: $imail_received";
             echo "/total: $imail_extotal";
         }
         else $pused = floor($imail_received/$imail_extotal*100);
          $pbar = "<TABLE width=80% align=left><tr><td style=\"font-size:10px;\"></td>";
          $pbar .= "<td width=100% class=td_left_middle>
<table cellpadding=0 border=0 cellspacing=0 style=\"border: #104a7b
1px solid; padding:1px; padding-right: 0px; padding-left: 0px;\" width=100%>
<tr>
<td width=100% class=td_left_middle bgcolor=white>
<div style=\"height:6px; width:$pused%; font-size:3px;
background-color:#CECFFF\"></div>
</td>
</tr>
</table>
</td>";
          $pbar .= "<td style=\"font-size:10px;\"></td></tr>";
          $pbar .= "</table>";
          $html_text = "$pbar <br>";
          $html_text .= "$i_CampusMail_New_Receiving1 $imail_received $i_CampusMail_New_Receiving2 $imail_extotal $i_CampusMail_New_Receiving3";
          $html_text .= "<br><a href=viewfolder.php?FolderID=2&mailchk=1>$i_CampusMail_New_BackMailbox</a>";
     }
     else
     {
         $body_tags = "onLoad=goback()";
     }
}
else
{
    header("Location: viewfolder.php?FolderID=2&mailchk=1");
}
intranet_closedb();
$html_text = str_replace("\n","",$html_text);
$html_text = str_replace("\r","",$html_text);
$html_text = addslashes($html_text);
?>
<html>
<script LANGUAGE=Javascript>
function process(obj)
{
msg = top.display.document.getElementById('msg');
msg.innerHTML = "<?=$html_text?>";
obj.form1.submit();
}
function goback()
{
         top.location.href = "viewfolder.php?FolderID=2&mailchk=1";
}
</script>
<body <?=$body_tags?>>
<form name=form1 method=get action="">
<input type=submit>
</form>
</body>
</html>