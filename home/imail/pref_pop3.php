<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

$lwebmail = new libwebmail();
if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
{
    include_once("../../includes/libsystemaccess.php");
    $lsysaccess = new libsystemaccess($UserID);
    if ($lsysaccess->hasMailRight())
    {
        $noWebmail = false;
    }
    else
    {
        $noWebmail = true;
    }
}
else
{
    $noWebmail = true;
}
$TabID=4;

#$noWebmail = false;

# Block No webmail no preference
if ($noWebmail || $UserID=="")
{
    header("Location: index.php");
    exit();
}

$li = new libdb();
$sql ="SELECT SkipCheckEmail FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='$UserID'";
$result = $li->returnArray($sql,1);

$optionSkipCheckEmail = $result[0][0];
if ($optionSkipCheckEmail)
{
    $strOptionSkipCheckEmail = "CHECKED";
}
else
{
    $strOptionSkipCheckEmail = "";
}

$CurrentPage = "PageSettings_POP3";
$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_CampusMail_New_Settings_POP3, "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

if ($msg == "2")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("update")."</td></tr>";
} else {
	$xmsg = "";
}
?>

<form name=form1 method="get" action="pref_update.php">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">	
	<?=$xmsg?>
	<tr>
		<td align="right" >
		<table border="0" cellpadding="5" cellspacing="1" align="center" width="90%" class="tabletext" >
		<tr>
			<td colspan=3 height=50>&nbsp;</td>
		</tr>
		<tr>
			<td></td>
			<td colspan="2">
			<input type="checkbox" name="disable_check_email" value="1" <?=$strOptionSkipCheckEmail?>><?=$i_CampusMail_New_Settings_DisableCheckEmail?>
			</td>
		</tr>
		<tr>
			<td width=100></td>
			<td colspan=2>
			<a href="javascript:newWindow('pop3_guide.php',2)"><?=$i_CampusMail_New_Settings_POP3_guideline?></a>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	</table>
	</td>
</tr> 
<tr>
	<td>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_save, "submit", "submitForm(document.form1)") ?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "button", "resetForm(document.form1)") ?>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="TabID" value=<?=$TabID?> />
</form>

<?
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>