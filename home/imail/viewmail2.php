<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libcampusmail.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libfiletable.php");
include_once("../../includes/libcampusquota.php");
include_once("../../includes/libaccess.php");
include_once("../../includes/libwebmail.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

$li = new libcampusmail($CampusMailID);
$outType = ($li->SenderID == $UserID && $li->CampusMailFromID == "");
$mailFolderID = $li->UserFolderID;

if ($viewtype == "search")
{

}
else
{
    $nextID = $li->returniMailNextID($mailFolderID);
    $prevID = $li->returniMailPrevID($mailFolderID);
}


$header_default_style = "/templates/style_mail.css";
$navigation = $i_frontpage_separator.$i_CampusMail_New_iMail;
include_once("../../templates/homeheader.php");
?>

<SCRIPT LANGUAGE=Javascript>
function removeMail()
{
         if (confirm('<?=$i_CampusMail_New_Confirm_remove_mail?>'))
         {
             location.href = 'remove.php?CampusMailID=<?=$CampusMailID?>&page_from=viewmail.php&nextID=<?=$nextID?>&prevID=<?=$prevID?>';
         }
}
function viewMessageSource()
{
         newWindow('message_popup.php?CampusMailID=<?=$CampusMailID?>&removeHTML=0',4);
}
</SCRIPT>
<table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="25">&nbsp;</td>
          <td><img src="<?=$image_path?>/frontpage/imail/title_imail_<?=$intranet_session_language?>.gif"></td>
        </tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/frontpage/imail/toolbar_bg.gif">
        <tr>
          <td width="70%">
              <?php include_once("menu.php"); ?>

          </td>
          <td width="30%" valign="bottom">
            <table width=100% border=0 cellpadding=0 cellspacing=0>
              <tr>
                <td><!-- Quota Display --></td>
                <td align=right><img src="<?=$image_path?>/frontpage/imail/graphic_mailbox2.gif" width="60" height="62"></td>
              </tr>
            </table>

          </td>
        </tr>
      </table>
      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="210" valign="top"><img src="<?=$image_path?>/frontpage/imail/subhead_inbox_<?=$intranet_session_language?>.gif" width="210" height="44"></td>
          <td width="584" class=td_center_middle>
<table width=100% border=0 cellspacing=0 cellpadding=0>
<tr>
<td width=20%>&nbsp;</td>
<? if ($viewtype == "search") { ?>
<td class=td_center_middle width=60%><a href=javascript:history.back()><img src="<?=$image_path?>/btn_pre.gif" border=0 align=absmiddle> <?=$i_CampusMail_New_MailSearch_BackToResult?></a></td>
<? } else { ?>
<td align=left width=30%><?=($prevID!=""? "<a href=?CampusMailID=$prevID><img src=\"$image_path/btn_pre.gif\" border=0>$i_campusmail_prev</a>":"")?></td>
<td align=right width=30%><?=($nextID!=""? "<a href=?CampusMailID=$nextID>$i_campusmail_next<img src=\"$image_path/btn_next.gif\" border=0></a>":"")?></td>
<? } ?>
<td width=20%>&nbsp;</td>
</tr>
</table>

            </td>
        </tr>
      </table>

          </td>
        </tr>
      </table>
      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="66" valign="top">
            <p><br>
              <br>
              <br>
              <br>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p><br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
               </p>
          </td>
          <td width="728" align="left" valign="top">
          <iframe frameborder=0 id=imail_frame width=640 height=400 src=viewmail_content.php?CampusMailID=<?=$CampusMailID?>></iframe>
  <!--        <?php echo $li->displayiMail(); ?>
<?php
if (!$outType)
     echo $li->displayiMailNotification();
?>
<?=$li->viewPaperBottom()?>
<?php
if ($outType)
     echo $li->displayCampusMailReplyiMail();
?>-->
</td>
</tr>
</table>


<?php
include_once("../../templates/homefooter.php");
intranet_closedb();
?>