<?php
// Editing by 
/*
 * 2014-10-15 (Carlos)[ip2.5.5.10.1]: Because IE cannot print email content that inside an iframe, created this popup window for printing email.
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$linterface = new interface_html("imail_default.html");
$li = new libcampusmail2007($CampusMailID);

?>
<head>
<META http-equiv=Content-Type content='text/html; charset=utf-8'>
<link rel="stylesheet" href="/templates/style_mail.css" />

<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/leftmenu.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/footer.css" rel="stylesheet" type="text/css" />

<style type="text/css" >
html, body
{
	background-color:#FFFFFF;
	scrollbar-face-color:#F7F7F7;
	scrollbar-base-color:#F7F7F7;
	scrollbar-arrow-color:888888;
	scrollbar-track-color:#E2DFDF;
	scrollbar-shadow-color:#F7F7F7;
	scrollbar-highlight-color:#F7F7F7;
	scrollbar-3dlight-color:#C6C6C6;
	scrollbar-darkshadow-Color:#C6C6C6;
	background-image: url("");
	background-repeat: no-repeat;
	background-position: left top;
}

table td {
	padding: 4px;
}

.formsubbutton {
    cursor: pointer;
    border: 1px outset #CCC;
    color: #000;
    height: 28px;
    padding-top: 3px;
    padding-bottom: 4px;
    background-image: url('<?=$LAYOUT_SKIN?>images/2009a/btn_bg.gif');
    background-position: 0px -60px;
}

a, a:link, a:hover, a:focus, a:visited {
	color: #000;
	text-decoration: none;
	cursor: default;
}

@media print 
{
	.print_hide {
		display: none;
	}
}
</style>
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff">

<table id="contentDiv" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" >
<tr>
	<td width="100%" align="left" valign="top" >
	<?php 
		echo $li->displayMailPrintVersion();
	?>	
	</td>
</tr>
</table>
<script type="text/javascript">
$(document).ready(function(){
	var anchors = $('a');
	for(var i=0;i<anchors.length;i++){
		var anchor_obj = $(anchors[i]);
		var anchor_text = anchor_obj.html();
		anchor_obj.after(anchor_text);
		anchor_obj.remove();
	}
});
</script>
</body>
<?php
intranet_closedb();	
?>