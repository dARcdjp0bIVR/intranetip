<?php
@SET_TIME_LIMIT(600);
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libeclass.php");
include_once("../../includes/libftp.php");
include_once("../../includes/libimport.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libfilesams.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libldap.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libimporttext.php");
intranet_opendb();

$anc = $special_option['no_anchor'] ?"":"#anc";

$li = new libimport();
$limport = new libimporttext();
$lo = new libfilesams();
$libdb = new libdb();
$lfile = new libfilesystem();

# CSV
if($file_format==1)
{
	//check file extension
        if(!$limport->CHECK_FILE_EXT($filename))
        {
        	header("Location: address_import.php?TabID=$TabID&xmsg=import_failed".$anc);
                exit;
        }
	        
        
  # check format of first line of import file
  $format = array('UserName','Email Address');
  $fp = fopen($userfile,"r");
  # GET FIRST LINE
  $data = fgetcsv($fp,filesize($userfile));
  
  fclose($fp);
  $correctFormat = true;

  $new_data = $limport->GET_IMPORT_TXT($userfile);  
  array_shift($new_data);
  /*
  for($i=0;$i<sizeof($format);$i++){
  	if($format[$i]!=$new_data[$i]){
  		$correctFormat=false;
	}
 }	
 */
 /*
 hdebug_r($data);
 hdebug_r($new_data);
 die();
 */
 if($correctFormat){
	 // echo "<p>format is correct</p>";
	 // echo "<p>userfile=$userfile</p>";
	 // echo "<p>filename=$filename</p>";
	 
     # create array for import contents 		
	 //$row = $lo->get_file_array(1, $userfile, $filename);
	 $row = $new_data;
 }else{
	 header("Location: address_import.php?TabID=$TabID&xmsg=import_failed".$anc);
 }

}
# vCARD
else if($file_format==2)
{
	//check file extension
        if($lfile->file_ext($filename)<>".vcf")
        {
        	header("Location: address_import.php?TabID=$TabID&xmsg=import_failed".$anc);
                exit;
        }

	# parse the vCARD
	$name_prefix = "FN";
	$email_prefix= "EMAIL";
	$default_email="PREF";
	$encode ="QUOTED-PRINTABLE";
	
	$fp = fopen($userfile,"r");
	if($fp){
	    while ($line = fread($fp, filesize($userfile))) {
	        $data .= $line;
	    }
	}
   fclose($fp);
   if($data==""){
	 header("Location: address_import.php?TabID=$TabID&xmsg=import_failed2".$anc);
   }
   
   $data = str_replace("\r\n","\n",$data);
   $data = str_replace("\r","\n",$data);
   $data = str_replace("x00","",$data);
   $lines = explode("\n",$data);
   $k=0;
   for($i=0;$i<sizeof($lines);$i++){
	   if($lines[$i]=="") continue;
	   
	   $d = explode(":",$lines[$i]);
	   $left = $d[0];
	   $right =$d[1];
	   
	   # Quoted-Printable Decode
	   if(strpos(strtoupper($left),$encode)!==false){
		   $right = quoted_printable_decode($right);
	   }
	   $right = addslashes($right);
	   
   	   # if it is username
	   if(strpos(strtoupper($left),$name_prefix)!==false){
		   $username = $right;
		   $row[$k][0] = $username;
	   }
	   # if Email
	   else if(strpos(strtoupper($left),$email_prefix)!==false){
		   # if more than one email address, use the default (with Parameter 'PREF')
		   if(strpos(strtoupper($left),$default_email)!==false || $row[$k][1]==""){
		   		$email_address = $right;
		   		$row[$k][1] = $email_address;
		   }
	   }
	   //$k++;
   }

}


# insert/update INTRANET_CAMPUSMAIL_ADDRES_EXTERNAL
if(sizeof($row)>0){
	$sql = "SELECT AddressID,TargetName,TargetAddress FROM INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL WHERE OwnerID='$UserID'";
	$existing_addresses = $libdb->returnArray($sql,3);
	for($i=0;$i<sizeof($existing_addresses);$i++){
		list($aid,$tname,$taddress) = $existing_addresses[$i];
		$address_book[$taddress]['AddressID']= $aid;
		$address_book[$taddress]['TargetName']= $tname;
	}
	$sqlInsert = "INSERT INTO INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL(OwnerID,TargetName,TargetAddress,DateInput,DateModified) VALUES";
	$need_insert = false;
	for($i=0;$i<sizeof($row);$i++){
		 $targetName = $row[$i][0];
		 $targetEmail = $row[$i][1];
		 
		 if(strpos($targetName,";") > 0)
		 {
		 	$error++;
		 	continue;
		 }
		 if(strpos($targetName,"<") > 0)
		 {
		 	$error++;
		 	continue;
		 }
		 if(strpos($targetName,">") > 0)
		 {
		 	$error++;
		 	continue;
		 }
		 
		 if($targetEmail=="")
		 {
		 	$error++;
		 	continue;
		 }
		 # address not exists, insert
		 if($address_book[$targetEmail]['AddressID']==""){
		 	 $sqlInsert .= "('$UserID','".addslashes($targetName)."','$targetEmail',NOW(),NOW()),";
		 	 $need_insert = true;
		 }
		 # address exists, update if targetname is different from the name in db
		 else if($targetName!=$address_book[$targetEmail]['TargetName']){
		 	 $sqlUpdate = "UPDATE INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL SET ";
			 $sqlUpdate.=" TargetName='".addslashes($targetName)."' WHERE AddressID = ".$address_book[$targetEmail]['AddressID'];
			 $libdb->db_db_query($sqlUpdate);
		 }else{
			 # address exists, and the targetname is the same as the one in db
		 }
	}
	if($need_insert){
		$sqlInsert = substr($sqlInsert,0,strlen($sqlInsert)-1); 
		# insert into INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
		$libdb->db_db_query($sqlInsert);
	}
	if($error == sizeof($row)){
		header("Location: addressbook.php?TabID=$TabID&xmsg=import_failed2".$anc);
	}else{
		header("Location: addressbook.php?TabID=$TabID&xmsg=import_success".$anc);
	}

}
else {
	header("Location: addressbook.php?TabID=$TabID&xmsg=import_failed2".$anc);
}
intranet_closedb();

?>