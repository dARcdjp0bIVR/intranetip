<?
#Using 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libcampusmail.php");
include_once("../../includes/libwebmail.php");
intranet_auth();
intranet_opendb();

$lc = new libcampusmail($CampusMailID);
$lwebmail = new libwebmail();
if ($UserID != $lc->UserID)
{
    $title = "Error";
    $msg = "Error. Pls re-login system.";
}
else
{
	$UseRawMessage = ($lc->has_raw_message) ? true : false;
	
	if ($UseRawMessage) {
		$Charset = $lc->messageCharset;
		$header = $lwebmail->ReadOrignialMail($lc->headerPath).
		$message = $lwebmail->ReadOrignialMail($lc->messagePath);
		$msg = stripslashes($header)."\n".$msg;
	} else {
		$Charset = 'utf-8';
		$msg = $lc->Message;
	}
	// $msg = ($lc->has_raw_message) ? $lc->raw_message : $lc->Message;
    $title = $lc->Subject;

    if ($removeHTML == 1)
    {
        $msg = $lc->removeHTMLtags($msg);
    }

    if (!$lc->isHTMLMessage($msg))
    {
         $msg = "<pre>".$msg."</pre>";
    }
}
// debug_r($lc->has_raw_message);

if ($title == "") $title = "Unknown";
?>
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; Charset=<?=$Charset?>">
</HEAD>
<body topmargin=0 leftmargin=0 marginheight=0 marginwidth=0>
<?=$msg?>
</body>
</HTML>