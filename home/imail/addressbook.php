<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$anc = $special_option['no_anchor'] ?"":"#anc";

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPageArr['iMail'] = 1;
$CurrentPage = "PageAddressBook_ExternalReceipient";

$lwebmail = new libwebmail();
$linterface = new interface_html();

if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
{
    include_once($PATH_WRT_ROOT."includes/libsystemaccess.php");
    $lsysaccess = new libsystemaccess($UserID);
    if ($lsysaccess->hasMailRight())
    {
        $noWebmail = false;
    }
    else
    {
        $noWebmail = true;
    }
}
else
{
    $noWebmail = true;
}

# Block No webmail no address book
if ($noWebmail)
{
    header("Location: groupalias.php?TabID=1".$anc);
    exit();
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);

# External Address
$sql  = "SELECT
		DateModified,
               CONCAT('<a class=\"tablelink\" href=\"address_edit.php?AddressID=',AddressID,'&TabID=$TabID$anc\">',TargetName,'</a>'),
               CONCAT('<a class=\"tablelink\" href=\"address_edit.php?AddressID=',AddressID,'&TabID=$TabID$anc\">',TargetAddress,'</a>'),
               DateModified,
               CONCAT('<input type=\"checkbox\" name=\"AddressID[]\" value=\"', AddressID ,'\">')
          FROM INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
          WHERE
               OwnerID = $UserID AND
               (TargetName like '%$keyword%' OR
                TargetAddress like '%$keyword%'
                )
          ";

$TabID = 2;
# TABLE INFO
$li->field_array = array("TargetName","TargetAddress","DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->IsColOff = "imail_addressbook_externalRecipient";
$li->title = "";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class=\"tabletoplink\" width=\"1\">#</td>\n";
$li->column_list .= "<td class=\"tabletoplink\" width=\"20\">&nbsp;</td>\n";
$li->column_list .= "<td class=\"tabletoplink\" width=\"40%\">".$li->column($pos++, "$i_CampusMail_New_AddressBook_Name")."</td>\n";
$li->column_list .= "<td class=\"tabletoplink\" width=\"40%\">".$li->column($pos++, "$i_CampusMail_New_AddressBook_EmailAddress")."</td>\n";
$li->column_list .= "<td class=\"tabletoplink\" width=\"19%\">".$li->column($pos++, $i_LastModified)."</td>\n";
$li->column_list .= "<td class=\"tabletoplink\" width=\"1%\">".$li->check("AddressID[]")."</td>\n";
$li->column_array = array(0,5,0,0);
$li->wrap_array = array(0,50,0,0);

### Button / Tag
$AddBtn 	= "<a href=\"javascript:newExternal(document.form1)\" class=\"contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
$ImportBtn 	= "<a href=\"address_import.php?TabID=$TabID$anc\" class=\"contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_import . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'AddressID[]','address_edit.php?TabID=$TabID$anc')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'AddressID[]','address_remove.php?TabID=$TabID$anc')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$searchTag 	.= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
$searchTag 	.= "</tr></table>";

### Title ###
$iMailImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_address_ex.gif' align='absmiddle' />";
$iMailTitle1 = "<span class='imailpagetitle'>". $i_CampusMail_External_Recipient ."</span>";
$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$iMailImage1.$iMailTitle1."</td></tr></table>";
$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($iMailTitle, "", 0);

$linterface->LAYOUT_START();

?>
<SCRIPT LANGUAGE=Javascript>
function newExternal(obj)
{
         num = prompt('<?=$i_CampusMail_New_AddressBook_Prompt_Num?>',3);
         if (num != null && num != "")
         {
             value = parseInt(num);
             if (isNaN(value) || value <= 0)
             {
                 alert('<?=$i_CampusMail_New_AddressBook_Alert_NeedInteger?>');
             }
             else
             {
                 obj.numToAdd.value = value;
                 obj.action = 'address_new.php?TabID=$TabID$anc';
                 obj.submit();
             }
         }
         else
         {

         }
}
</SCRIPT>

<br />
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
        		<td colspan="2">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                		<tr>
                			<td><?=$AddBtn?> <?=$ImportBtn?></td>
                			<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
                		</tr>
                		</table>
			</td>
		</tr>
                <tr>
			<td><?=$searchTag?></td>
			<td align="right" valign="bottom">
				<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
					<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
					<table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td nowrap><?=$delBtn?></td>
						<td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5" /></td>
						<td nowrap><?=$editBtn?></td>
					</tr>
					</table>
					</td>
					<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
				</tr>
				</table>
			</td>
		</tr>
                <tr>
			<td colspan="2"><?=$li->display()?></td>
		</tr>
                </table>
        </td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="numToAdd" value="">
<input type="hidden" name="TabID" value="<?=$TabID?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>