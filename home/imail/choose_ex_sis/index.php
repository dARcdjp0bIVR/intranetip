<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libgroupcategory.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$db = new libdb();

# check and redirect to old / new interface
$sql ="SELECT a.RecordType,a.Teaching,c.ClassLevelID FROM INTRANET_USER AS a 
	LEFT OUTER JOIN INTRANET_CLASS AS b ON(a.ClassName=b.ClassName) 
	LEFT OUTER JOIN INTRANET_CLASSLEVEL AS c ON (b.ClassLevelID=c.ClassLevelID) 
	WHERE a.UserID='$UserID'";
	
$temp = $db->returnArray($sql,3);
list($usertype,$teaching,$class_level_id) = $temp[0];

$sql = "SELECT Restricted FROM INTRANET_IMAIL_RECIPIENT_RESTRICTION WHERE TargetType='$usertype'";
$conds = "";

# Teacher /Staff
if($usertype==1){
	 $conds =" AND Teaching='$teaching'";
}
# Student
else if($usertype==2){
	$conds = " AND ClassLevel = '$class_level_id'";
}
# Parent
else if($usertype==3){
	
}

$sql.=$conds;
$temp  = $db->returnVector($sql);

$restricted = $temp[0];

if($restricted==1){
	/*if($usertype==1)
		header("Location: new/staff.php?fieldname=$fieldname");
	if($usertype==2)
		header("Location: new/student.php?fieldname=$fieldname");
	if($usertype==3)
		header("Location: new/parent.php?fieldname=$fieldname");
	*/
	header("Location: new/index.php?fieldname=$fieldname");
	
}
else header("Location: old/index.php?fieldname=$fieldname");

intranet_closedb();
?>