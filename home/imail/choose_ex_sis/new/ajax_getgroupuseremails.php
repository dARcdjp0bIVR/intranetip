<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$li = new libuser();


$fields = "a.UserEmail ";
$tables = "INTRANET_USER AS a";
$conds ="a.RecordStatus=1";
# show student list
if($CatID==2 || $CatID==999)
{
	$tables.=",INTRANET_CLASS AS b";
	$conds.=" AND a.RecordType=2 AND b.GroupID IN($GroupID) AND a.ClassName=b.ClassName";
	$orderfield=" ORDER BY a.ClassName,a.ClassNumber ";
}
# show parent list
if($CatID==3)
{
	$tables.=",INTRANET_USER AS b,INTRANET_PARENTRELATION AS c, INTRANET_CLASS AS d";
	$conds.=" AND a.RecordType=3 AND d.GroupID IN($GroupID) AND b.ClassName=d.ClassName AND b.UserID=c.StudentID AND a.UserID=c.ParentID";
	$orderfield=" ORDER BY a.EnglishName ";
}

$sql ="SELECT $fields FROM $tables WHERE $conds $orderfield";

$result = $li->returnArray($sql);

for($a=0;$a<sizeof($result);$a++)
{
	$EmailArr[] = $result[$a]['UserEmail'];
}
if(is_array($EmailArr))
{
	$Email = implode(";",$EmailArr).";";
}

echo $Email;

intranet_closedb();

?>