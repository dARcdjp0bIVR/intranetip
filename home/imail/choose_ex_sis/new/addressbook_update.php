<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$li = new libdb();

$caller = $AliasID==""?"compose":"addressbook";


$parent_list=array();
$group_parent_list=array();
$user_list=array();
$group_list=array();

if($InternalRecipientID!=""){
	
	$ids = explode(",",$InternalRecipientID);
	
	for($i=0;$i<sizeof($ids);$i++){
		if(substr($ids[$i],0,1)=="U"){
			$user_list[] = substr($ids[$i],1);
		}
		if(substr($ids[$i],0,1)=="P"){
			$parent_list[] = substr($ids[$i],1);
		}
		if(substr($ids[$i],0,1)=="Q"){
			$group_parent[] = substr($ids[$i],1);
		}
		if(substr($ids[$i],0,1)=="G"){
			$group_list[] = substr($ids[$i],1);
		}
	}
	if(sizeof($parent_list)>0){
		$str = implode(",",$parent_list);
		$sql="SELECT DISTINCT ParentID FROM INTRANET_PARENTRELATION WHERE StudentID IN($str) ";
		$temp = $li->returnVector($sql);
		if(sizeof($temp)>0)
			$user_list = array_merge($user_list,$temp);
	}
	if(sizeof($group_list)>0){
		$str = implode(",",$group_list);
		$sql ="SELECT DISTINCT UserID FROM INTRANET_USERGROUP WHERE GroupID IN($str)";
		$temp = $li->returnVector($sql);
		if(sizeof($temp)>0)
			$user_list = array_merge($user_list,$temp);
	}
	if(sizeof($group_parent_list)>0){
		$str = implode(",",$group_parent_list);
		$sql ="SELECT DISTINCT UserID FROM INTRANET_USERGROUP WHERE GroupID IN($str)";
		$temp = $li->returnVector($sql);
		if(sizeof($temp)>0){
			$str = implode(",",$temp);
			$sql="SELECT DISTINCT ParentID FROM INTRANET_PARENTRELATION WHERE StudentID IN($str) ";
			$temp = $li->returnVector($sql);
			if(sizeof($temp)>0)
				$user_list = array_merge($user_list,$temp);
				
		}
	}
	if(sizeof($user_list)>0){
		$user_list = array_unique($user_list);
		$delim="";
		for($i=0;$i<sizeof($user_list);$i++){
			$values .= $delim."($AliasID, ".$user_list[$i].",'U',now(),now())";
			$delim=",";
		}
		$sql = "INSERT IGNORE INTO INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY
        (AliasID, TargetID, RecordType, DateInput, DateModified)
        VALUES $values ";	
        $li->db_db_query($sql);
   }
	
	
}


intranet_closedb();
//header("Location: index.php?AliasID=".$AliasID);
?>
<script language='javascript'>
opener.location.reload();
location.href='index.php?AliasID=<?=$AliasID?>';
</script>