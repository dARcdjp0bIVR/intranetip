<?php
// editing by 
/*************** Change Log ***************
 * Created on 2011-04-07
 * 2019-06-06 Carlos: Added CSRF token.
 ******************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

$lwebmail = new libwebmail();
$lc = new libcampusquota2007($UserID);

$folder_sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID = '$UserID' OR RecordType = 0 ORDER BY RecordType, FolderName";
$folders = $lc->returnArray($folder_sql,2);
$folders[] = array(-1,$i_admintitle_im_campusmail_trash);

$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();

$iMailTitle1 = "<span class='imailpagetitle'>".$Lang['iMail']['FieldTitle']['Archive']."&nbsp;".$i_CampusMail_New_Inbox."</span>";
$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$iMailImage1.$iMailTitle1."</td><td align='right' style=\"vertical-align: bottom;\" >".$lc->displayiMailFullBar()."</td></tr></table>";
$TAGS_OBJ[] = array($iMailTitle, "", 0);

if($SYS_CONFIG['Mail']['hide_imail']===true)
	$linterface = new interface_html("imail_archive.html");
else
	$linterface = new interface_html("imail_default.html");
$linterface->LAYOUT_START();

# Count mails by year
$sql = "SELECT DATE_FORMAT(DateInput,'%Y') as FolderYear, COUNT(CampusMailID) as NumOfMails 
		FROM INTRANET_CAMPUSMAIL 
		WHERE UserID = '".$_SESSION['UserID']."' AND UserFolderID = 2 
			AND DateInput IS NOT NULL AND DateInput <> '' AND Deleted != 1
		GROUP BY FolderYear ";
$data_list = $lc->returnArray($sql);

$data_rows = "";
$totalOfMails = 0;
for($i=0;$i<sizeof($data_list);$i++)
{
	list($folderYear,$numOfMails) = $data_list[$i];
	$css = ($i%2) ? "" : "2";
	$totalOfMails += $numOfMails;
	$data_rows .= "<tr>\n
					<td class=\"tableContent$css\" align=\"center\">$folderYear</td>\n
					<td class=\"tableContent$css\" align=\"center\">$numOfMails</td>\n
					<td class=\"tableContent$css\" align=\"center\">".$linterface->Get_Checkbox("archive$i","archive[]",$folderYear,0)."</td>\n
				  </tr>\n";
}
$data_rows .= "<tr class=\"total_row\">\n
				<th style=\"text-align:right\">".$Lang['General']['Total']."</th>
				<td style=\"text-align:center\">$totalOfMails</td>
				<td>&nbsp;</td>
			   </tr>\n";
# End

?>
<script>
function jsCheckSubmit()
{
	var numOfChecked = $('input[name=archive\\[\\]]:checked');
	if(numOfChecked.length==0){
		alert('<?=$Lang['iMail']['JSWarning']['RequestAtLeastOneYearToArchive']?>');
		return false;
	}
	document.form1.submit();
	$('input#submit_btn').attr('disabled',true);
}
</script>
<br />
<form name="form1" id="form1" method="POST" action="archive_mail_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">	
	<tr>
		<td width="100%" >
			<br />
			<table class="common_table_list_v30">
				<thead class="tabletop">
					<tr>
						<th style="text-align:center" width="50%"><?=$Lang['iMail']['FieldTitle']['Year']?></th>
						<th style="text-align:center" width="25%"><?=$Lang['iMail']['FieldTitle']['NumberOfEmails']?></th>
						<th style="text-align:center" width="25%"><?=$Lang['iMail']['FieldTitle']['Archive']?></th>
					</tr>
				</thead>
				<tbody>
					<?=$data_rows?>
				</tbody>
			</table>
			<div class="edit_bottom_v30">
                <p class="spacer"></p>
                <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","jsCheckSubmit();","submit_btn")?>
                <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location='./index.php';","cancel_btn")?>
                <p class="spacer"></p>
            </div>
		</td>
	</tr>
	</table>
	<br />
	</td>
</tr>
</table>
<?php echo csrfTokenHtml(generateCsrfToken());?>
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>