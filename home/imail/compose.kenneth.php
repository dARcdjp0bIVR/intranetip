<?php
// Modifing by 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libusertype.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

if (!auth_sendmail())
{
    header("Location: index.php");
    exit();
}

$lcampusmail = new libcampusmail($CampusMailID);
$li = new libfilesystem();
$LibUserType = new libusertype();
$CurUserType = $LibUserType->returnIdentity($UserID);

# get toStaff, toStudent, toParent options
$sql ="
		SELECT 
			a.RecordType,a.Teaching,c.ClassLevelID, a.UserEmail 
		FROM 
				INTRANET_USER AS a 
			LEFT OUTER JOIN 
				INTRANET_CLASS AS b 
			ON
				(a.ClassName=b.ClassName) 
			LEFT OUTER JOIN 
				INTRANET_CLASSLEVEL AS c 
			ON 
				(b.ClassLevelID=c.ClassLevelID) 
		WHERE 
			a.UserID='$UserID'
		";
$temp = $LibUserType->returnArray($sql,4);
list($usertype,$teaching,$class_level_id,$UserEmail) = $temp[0];

$sql_to_options = "	SELECT 
								ToStaffOption,ToStudentOption,ToParentOption 
							FROM 
								INTRANET_IMAIL_RECIPIENT_RESTRICTION 
							WHERE 
								TargetType='$usertype' AND Restricted=1
						";
if($usertype==1)
	$sql_to_options.=" AND Teaching='$teaching'";
else if($usertype==2)
	$sql_to_options.=" AND ClassLevel='$class_level_id'";
$result_to_options = $LibUserType->returnArray($sql_to_options,3);
//echo "<p>$sql_to_options</p>";
list($toStaff,$toStudent,$toParent) = $result_to_options[0];
# end get toStaff, toStudent, toParent options


$personal_path = "$file_path/file/mail/u$UserID";
if (!is_dir($personal_path))
{
    $li->folder_new($personal_path);
}

if ($special_feature['imail_richtext'])
{
    $use_html_editor = true;
}

$lwebmail = new libwebmail();
if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) && $lwebmail->type == 3)
{
    $noWebmail = false;
}
else
{
    $noWebmail = true;
}

# Check sending access right
$hide_internal = $lcampusmail->usage_internal_disabled;
$hide_external = ($noWebmail || $lcampusmail->usage_external_disabled);

if ($action=="D")   # Draft
{
    if(!session_is_registered("composeFolder"))
    {
        session_register("composeFolder");
    }
    $composeFolder = $lcampusmail->Attachment;
    if ($composeFolder == "")
    {
        header("Location: index.php");
        exit();
    }
    # Remove unsaved attachment files
    $sql = "SELECT FileName FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE CampusMailID = $CampusMailID";
    $file_array = $lcampusmail->returnVector($sql);

    # compute size of attachment
    $path = "$intranet_root/file/mail/".$lcampusmail->Attachment;
    $lsize = new libfiletable("",$path, 0,0,"");
    $files = $lsize->files;
    while (list($key, $value) = each($files))
    {
           list($filename, $filesize) = $files[$key];
           if (!in_array($filename,$file_array))
           {
                $li->file_remove("$path/$filename");
           }
    }
}
else
{
    if(!session_is_registered("composeFolder") || $composeFolder=="")
    {
        session_register("composeFolder");
    }

    if (substr($composeFolder,-3)== "tmp")
    {
        $mail_attachment_path = "$file_path/file/mail/$composeFolder";
        # Remove all attachments when clicked to this page (except for draft)
        if (is_dir($mail_attachment_path) && $composeFolder != "")
        {
            $li->lfs_remove($mail_attachment_path);
        }
    }
    # re-create new directory
    $composeFolder = session_id().".".time();
    $composeFolder = "u$UserID/$composeFolder"."tmp";
    #$path = "$personal_path/$composeFolder"."tmp";
}

$path = "$file_path/file/mail/$composeFolder";

#$path = "$file_path/file/mail/$composeFolder"."tmp";
$li->folder_new($path);

if ($cmsg == 1)
{
    $xcmsg = $i_campusmail_novaliduser;
}

# Check Identity type can receive mails
$laccess = new libaccess();
$mailtarget_teacherAllowed = $laccess->retrieveAccessCampusmailForType(1);
$mailtarget_studentAllowed = $laccess->retrieveAccessCampusmailForType(2);
$mailtarget_parentAllowed = $laccess->retrieveAccessCampusmailForType(3);

$navigation = $i_frontpage_separator.$i_CampusMail_New_iMail;
$preset_notify  = true;
$preset_important = false;

$isTotallyNew = false;

# Handle reply/reply all/forward/Draft action
if ($CampusMailID != "" && $lcampusmail->UserID == $UserID)
{
    $lcampusquota = new libcampusquota($UserID);
    $totalQuota = $lcampusquota->returnQuota() * 1024;
    $usedQuota = $lcampusquota->returnUsedQuota();
    $leftQuota = $totalQuota - $usedQuota;
/*
	$this->UserID = $this->CampusMail[0][2];
	$this->SenderID = $this->CampusMail[0][4];
	$this->RecipientID = $this->CampusMail[0][5];
	$this->Attachment = $this->CampusMail[0][8];
	$this->IsAttachment = $this->CampusMail[0][9];
	$this->RecordType = $this->CampusMail[0][12];
	$this->RecordStatus = $this->CampusMail[0][13];
	list($this->isDeleted, $this->mailType,$this->SenderEmail
	,$this->InternalCC,$this->InternalBCC, $this->ExternalTo
	,$this->ExternalCC,$this->ExternalBCC)
*/
    # Grab Sender information
    if ($lcampusmail->mailType==2)         # External
    {
        $mail_senderemail = intranet_htmlspecialchars($lcampusmail->SenderEmail);
        $preset_message_whowrite = $mail_senderemail;
    }
    else
    {
        $namefield = getNameFieldWithClassNumberByLang();
        $sql = "SELECT UserID,$namefield FROM INTRANET_USER WHERE UserID = '".$lcampusmail->SenderID."'";
        $temp = $lcampusmail->returnArray($sql,2);
        list($mail_senderid,$mail_sendername) = $temp[0];
        $preset_message_whowrite = $mail_sendername;
    }
    if ($action != "D")
    {
        if ($lcampusmail->isHTMLMessage($lcampusmail->Message))
        {
            if (!$use_html_editor)
            {
                 $preset_message = $lcampusmail->removeHTMLtags($lcampusmail->Message);
            }
            else
            {
                 $preset_message = $lcampusmail->Message;
            }
        }
        else
        {
            if (!$use_html_editor)
            {
                 $preset_message = $lcampusmail->Message;
            }
            else
            {
                $preset_message = nl2br($lcampusmail->Message);
            }
        }
        if ($use_html_editor)
        {
            $preset_message = $preset_message_whowrite."$i_CampusMail_New_Wrote\n".
                              "<BLOCKQUOTE dir=ltr "."style=\"PADDING-RIGHT: 0px; PADDING-LEFT: 5px; MARGIN-LEFT: 5px; BORDER-LEFT: #000000 2px solid; MARGIN-RIGHT: 0px\"". " >".
            $preset_message. "</BLOCKQUOTE>
                                <br />
                                ";
        }
        else
        {
            $preset_message = ">".str_replace("\n","\n>",$preset_message);
            $preset_message = intranet_htmlspecialchars($preset_message);
            $preset_message = str_replace("&amp;","&",$preset_message);
            $preset_message = $preset_message_whowrite."$i_CampusMail_New_Wrote\n".$preset_message;
        }
    }
    else
    {
        $preset_message = $lcampusmail->Message;
    }

    # Handle Attachment (Duplicated for Forward message and embedded images)
    if (($action == "F" || $action=="R" || $action=="RA") && $lcampusmail->IsAttachment)
    {
		if ($action != "F")
		{
			$sql = "	SELECT
							FileName
						FROM
							INTRANET_IMAIL_ATTACHMENT_PART
						WHERE
							CampusMailID = '$CampusMailID' AND isEmbed = 1
					";
			$array_embed = $lcampusmail->returnVector($sql);
		}
        $source = "$file_path/file/mail/".$lcampusmail->Attachment;
        #$li->lfs_copy($source,$path);
        $lfiletable_source = new libfiletable("", $source, 0, 0, "");
        $preset_files = $lfiletable_source->files;
        $preset_quota_notenough = false;
        while (list($key, $value) = each($preset_files))
        {
               $filesize = $preset_files[$key][1]/1024;
               if ($action != "F")
               {
                   $t_filename = $preset_files[$key][0];
                   if (!in_array($t_filename,$array_embed))
                   {
                        continue;
                   }
               }

               if ($leftQuota > $filesize)
               {
                       /*
                       # copy to temp folder
                       $source_enc = strtolower($lcampusmail->MessageEncoding);
                       $target_enc = "";
                       if(strpos($source_enc,"iso")!==false)
                                            $source_enc="";
                               if($intranet_session_language=="b5")
                                          $target_enc = "big5";
                       else if($intranet_session_language=="gb")
                                       $target_enc = "gb2312";
                                else if($intranet_session_language=="en"){
                                                if(is_array($intranet_default_lang_set) && in_array("b5",$intranet_default_lang_set))
                                               $target_enc = "big5";
                                                       if(is_array($intranet_default_lang_set) && in_array("gb",$intranet_default_lang_set))
                                               $target_enc = "gb2312";
                       }
                       //if($source_enc!="" && strtolower($source_enc)!="utf-8")
                       if($target_enc!="" && $source_enc!=""&& $source_enc !=$target_enc && !$function_undefined['iconv']){
                                       $new_file_name = iconv($source_enc,$target_enc,stripslashes($preset_files[$key][0]));
                       }
                       else
                       */
					$new_file_name = $preset_files[$key][0];
					$temp_folder=$composeFolder;
                   	$li->file_copy("$source/".$preset_files[$key][0],$path."/$new_file_name");
                   	$leftQuota -= $filesize;
               	}
               	else
               	{
                   	$preset_quota_notenough = true;
               	}
			}
    }
    if ($action == "R")  # Reply
    {
	    //$preset_subject = "Re: ".$lcampusmail->Subject;
	    if($lcampusmail->MessageEncoding != 'big-5'){
        	$preset_subject = "Re: ".iconv($lcampusmail->MessageEncoding,"BIG5//TRANSLIT",$lcampusmail->Subject);
    	}else{
	    	$preset_subject = "Re: ".$lcampusmail->Subject;
    	}
    	
        if ($lcampusmail->mailType==2)         # External
        {
            $preset_senderemail = $mail_senderemail;
        }
        else
        {
            $preset_senderid = $mail_senderid;
            $preset_sendername = $mail_sendername;
        }
        $preset_notify  = true;
        $preset_important = false;

    }
    else if ($action == "RA")
    {
        //$preset_subject = "Re: ".$lcampusmail->Subject;
        if($lcampusmail->MessageEncoding != 'big-5'){
        	$preset_subject = "Re: ".iconv($lcampusmail->MessageEncoding,"BIG5//TRANSLIT",$lcampusmail->Subject);
    	}else{
	    	$preset_subject = "Re: ".$lcampusmail->Subject;
    	}        
        
        if ($lcampusmail->mailType==2)         # External
        {
            $preset_senderemail = $mail_senderemail;
        }
        else
        {
            $preset_senderid = $mail_senderid;
            $preset_sendername = $mail_sendername;            
            # Handle internal recipients
            if ($lcampusmail->RecipientID != "")
            {
                $mail_recipients2 = $lcampusmail->getRecipientNames($lcampusmail->RecipientID);
                for ($i=0;$i<count($mail_recipients2);$i++)
                {	                
	                if ($mail_recipients2[$i][0] != "U".$UserID)
	                {
	                	$mail_recipients[] = $mail_recipients2[$i];
                	}
                }
                
            }
            if ($lcampusmail->InternalCC != "")
            {
	           $activate_InternalCC = true;
                $mail_cc_recipients = $lcampusmail->getRecipientNames($lcampusmail->InternalCC);
            }
        }
                
        if ($lcampusmail->ExternalTo != "")
        {   
            //$preset_to_address = $lcampusmail->convertEmailAddressComma($li->ExternalTo);
            #$preset_to_address = str_replace(",",";",$lcampusmail->ExternalTo);
            $preset_to_address = $lcampusmail->convertExternalEmailAddressComma($lcampusmail->ExternalTo);
                        
            $preset_to_address_arr = explode (";",$preset_to_address);
            for ($i=0;$i<count($preset_to_address_arr);$i++)
            {
	            $TmpEmail = trim($preset_to_address_arr[$i]);
	            $Pos1 = strpos($TmpEmail,"<");
	            if ($Pos1===false)
	            {
		            $TmpEmail2 = $TmpEmail;
	            } else {
		            $TmpEmail2 = substr($TmpEmail,$Pos1+1,strlen($TmpEmail)-$Pos1-2);
	            }
	            
	            if (trim($TmpEmail2) != trim($UserEmail))
	            {
	            	$preset_to_address_arr2[] = $TmpEmail;
            	}
            }
            if (is_array($preset_to_address_arr2) && count($preset_to_address_arr2)>0)
            {
	            $preset_senderemail = implode(";",$preset_to_address_arr2);
            }
            
            if ($preset_senderemail != "")
            {
                $preset_senderemail .= "; $preset_to_address";
            }
            else
            {
                $preset_senderemail = "$preset_to_address";
            }
        }
        if ($lcampusmail->ExternalCC != "")
        {
	        $activate_ExternalCC = true;
            $preset_cc_address = $lcampusmail->convertEmailAddressComma($lcampusmail->ExternalCC);
            #$preset_cc_address = str_replace(",",";",$lcampusmail->ExternalCC);
        }
        $preset_notify  = true;
        $preset_important = false;
    }
    else if ($action == "F")
    {
        $preset_subject = "Fw: ".$lcampusmail->Subject;
        $preset_notify  = true;
        $preset_important = false;
    }
    else if ($action == "D")
    {
         $preset_subject = $lcampusmail->Subject;
         $preset_senderemail = str_replace(",",";",$lcampusmail->ExternalTo);
         $preset_cc_address = str_replace(",",";",$lcampusmail->ExternalCC);
         $preset_bcc_address = str_replace(",",";",$lcampusmail->ExternalBCC);
         if ($lcampusmail->RecipientID != "")
         {
             $mail_recipients = $lcampusmail->getRecipientNames($lcampusmail->RecipientID);
         }
         if ($lcampusmail->InternalCC != "")
         {

             $mail_cc_recipients = $lcampusmail->getRecipientNames($lcampusmail->InternalCC);
         }
         if ($lcampusmail->InternalBCC != "")
         {
             $mail_bcc_recipients = $lcampusmail->getRecipientNames($lcampusmail->InternalBCC);
         }
         if ($lcampusmail->IsNotification)
             $preset_notify  = true;
         else $preset_notify = false;
         if ($lcampusmail->IsImportant)
             $preset_important = true;
         else $preset_important = false;
    }
    else
    {
    }
}
else if ($CampusMailReplyID!="")  # Get info from reply notification
{
     # Get info from DB
     $namefield = getNameFieldWithClassNumberByLang("c.");
     $sql = "	SELECT
     				a.UserID, a.CampusMailID, a.Message, b.UserID, $namefield, b.Subject
				FROM
					INTRANET_CAMPUSMAIL_REPLY as a
					LEFT OUTER JOIN INTRANET_CAMPUSMAIL as b ON a.CampusMailID = b.CampusMailID
					LEFT OUTER JOIN INTRANET_USER as c ON a.UserID = c.UserID
				WHERE
					a.CampusMailReplyID = $CampusMailReplyID
				";
     $temp = $lcampusmail->returnArray($sql,6);
     list($receiverID, $cMailID, $replyMessage, $senderID, $receiverName, $mail_subj) = $temp[0];
     if ($cMailID != "" && $receiverID != "" && $senderID == $UserID)
     {
         $mail_recipients[] = array("U".$receiverID, $receiverName);
         $preset_subject = "Re: ". $mail_subj;
         $preset_message = $replyMessage;
         if ($use_html_editor)
         {
             $preset_message = $receiverName."$i_CampusMail_New_Wrote\n".
                               "<BLOCKQUOTE dir=ltr ".$css_display_style["imail_included_message"]. " >".
                               $preset_message. "</BLOCKQUOTE>
                               <br>
                               ";
         }
         else
         {
             $preset_message = ">".str_replace("\n","\n>",$preset_message);
             $preset_message = intranet_htmlspecialchars($preset_message);
             $preset_message = str_replace("&amp;","&",$preset_message);
             $preset_message = $receiverName."$i_CampusMail_New_Wrote\n".$preset_message;
         }
     }

}
else
{
    if (isset($targetemail))
    {
        if ($targetname != "")
        {
            $targetname = stripslashes($targetname);
            $targetname = intranet_htmlspecialchars(str_replace(";",",",intranet_undo_htmlspecialchars($targetname)));
            $preset_senderemail = "$targetname <$targetemail>";
        }
        else
        {
            $preset_senderemail = "$targetemail";
        }
    }
    
    $isTotallyNew = true;
}

if ($isTotallyNew)
{
	$InternalToStyle = " style='display:none' ";
	$InternalCCStyle = " style='display:none' ";
	$InternalBCCStyle = " style='display:none' ";
} else {
	$InternalToStyle = "";
	$InternalCCStyle = "";
	$InternalBCCStyle = "";
}

# signature
$signature_sql = "SELECT Signature FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='$UserID'";
$temp_result = $lcampusmail->returnVector($signature_sql);
$signature = $temp_result[0];


$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;

$ldb = new libdb();
$folder_sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID = $UserID OR RecordType = 0 ORDER BY RecordType, FolderName";
$folders = $ldb->returnArray($folder_sql,2);
$folders[] = array(-1,$i_admintitle_im_campusmail_trash);
/*
# Find unread mails number
$sql  = "
			SELECT
				count(a.CampusMailID)
          	FROM
          		INTRANET_CAMPUSMAIL AS a LEFT OUTER JOIN INTRANET_USER AS b
          	ON
          		a.SenderID = b.UserID
			WHERE
               a.UserID = $UserID AND
               a.UserFolderID = 2 AND
               a.Deleted != 1 AND
               (a.RecordStatus = '' OR a.RecordStatus IS NULL)
          ";
$row = $ldb->returnVector($sql);
$unreadInboxNo = $row[0];
*/

$CurrentPage = "PageComposeMail";
$MODULE_OBJ = $lwebmail->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_CampusMail_New_ComposeMail, "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

if ($cmsg==1)
{
    $xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("",$i_CampusMail_New_alert_norecipients)."</td></tr>";
}

?>

<link type="text/css" rel="stylesheet" href="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.css">

<style type="text/css">
    #statesmod {position:relative;}
    #statesautocomplete {position:relative;width:22em;margin-bottom:1em;}/* set width of widget here*/
    #statesautocomplete {z-index:9000} /* for IE z-index of absolute divs inside relative divs issue */
    #statesinput {_position:absolute;width:100%;height:1.4em;z-index:0;} /* abs for ie quirks */
    #statescontainer, #statescontainerCC, #statescontainerBCC {position:absolute;top:0.3em;width:100%}
    #statescontainer .yui-ac-content, #statescontainerCC .yui-ac-content, #statescontainerBCC .yui-ac-content {position:absolute;width:100%;border:1px solid #404040;background:#eeeeee;overflow:hidden;z-index:9050;}
    #statescontainer .yui-ac-shadow, #statescontainerCC .yui-ac-shadow, #statescontainerBCC .yui-ac-shadow {position:absolute;margin:.3em;width:100%;background:#a0a0a0;z-index:9049;}
    #statescontainer ul, #statescontainerCC ul, #statescontainerBCC ul {padding:5px 0;width:100%;}
    #statescontainer li, #statescontainerCC li, #statescontainerBCC li {padding:0 5px;cursor:default;white-space:nowrap;}
    #statescontainer li.yui-ac-highlight, #statescontainerCC li.yui-ac-highlight, #statescontainerBCC li.yui-ac-highlight {background:#bbbbbb;}
    #statescontainer li.yui-ac-prehighlight, #statescontainerCC li.yui-ac-prehighlight, #statescontainerBCC li.yui-ac-prehighlight {background:#FFFFFF;}
    
    
	#statesmod div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote{margin:0;padding:0;}
	#statesmod table{border-collapse:collapse;border-spacing:0;}
	#statesmod fieldset,img{border:0;}
	#statesmod address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal;}
	#statesmod ol,ul {list-style:none;}
	#statesmod caption,th {text-align:left;}
	#statesmod h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}
	#statesmod q:before,q:after{content:'';}
	#statesmod abbr,acronym {border:0;}
	#statesmod {font:13px arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}
</style>

<script language="javascript">

function disableReturnKey(evt) {
  var evt = (evt) ? evt : ((event) ? event : null);
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
}

document.onkeypress = disableReturnKey;

function checkform(obj)
{
	var jIsDraft = (obj.SubmitType.value==1);
	
	//if(obj.elements["Recipient[]"].length==0){ alert("<?php echo $i_frontpage_campusmail_choose; ?>"); return false; }
	
	if (jIsDraft)
	{
        if(Trim(obj.Subject.value)=="")
        {
	        obj.Subject.value = "<?=$i_CampusMail_New_No_Subject?>";
        }
		
	}
	else 
	{
		if(!check_text(obj.Subject, "<?php echo $i_alert_pleasefillin.$i_frontpage_campusmail_subject; ?>.")) return false;
	} 
     
     <? if ($use_html_editor) { ?>
     if (obj.Message.value == '')
     {
         alert('<?=$i_alert_pleasefillin.$i_frontpage_campusmail_message?>');
         return false;
     }
     <? } else { ?>
     if(!check_text(obj.Message, "<?php echo $i_alert_pleasefillin.$i_frontpage_campusmail_message; ?>.")) return false;
     <? } ?>
     
     // Check any recipients input
     if (!jIsDraft)
     {
	     if (
	         <?php 
	         if (!$hide_internal && $mailtarget_teacherAllowed && $imail_feature_allowed['withcheckbox'] && $toStaff == 0)
	         { 
			?>
	         obj.AllStaffTo.checked ||
	         obj.AllStaffCC.checked ||
	         obj.AllStaffBCC.checked ||
	         <?php 
	         }
	         
	         if (!$hide_internal && $mailtarget_studentAllowed && $imail_feature_allowed['withcheckbox'] && $toStudent == 0) 
	         {
	         ?>
	         obj.AllStudentTo.checked ||
	         obj.AllStudentCC.checked ||
	         obj.AllStudentBCC.checked ||
	         <?php 
	         }
	         if (!$hide_internal && $mailtarget_parentAllowed && $imail_feature_allowed['withcheckbox'] && $toParent == 0)
	         {
	         ?>
	         obj.AllParentTo.checked ||
	         obj.AllParentCC.checked ||
	         obj.AllParentBCC.checked ||
	         <?php
	         }
	         ?>
	         false
	         )
	     {
	         // Either one of all identity box checked
	     }
	     <? if (!$hide_internal) { ?>
	     else if (obj.elements["Recipient[]"].length!=0 || obj.elements["InternalCC[]"].length!=0 ||obj.elements["InternalBCC[]"].length!=0)
	     {
	          // has internal
	     }
	     <? } ?>
	     <? if (!$hide_external) { ?>
    
	     else if (obj.ExternalTo.value != "" || obj.ExternalCC.value != "" || obj.ExternalBCC.value != "")
	     {
	     }
	     
	     <? } ?>
	     else
	     {
	         alert('<?=$i_CampusMail_New_alert_norecipients?>');
	         return false;
	     }
     }
     <? if (!$hide_internal) { ?>
     checkOptionAll(obj.elements["Recipient[]"]);
     checkOptionAll(obj.elements["InternalCC[]"]);
     checkOptionAll(obj.elements["InternalBCC[]"]);
     <? } ?>
     checkOptionAll(obj.elements["Attachment[]"]);
     
	
	<? if(!$hide_external) { ?>
     		return validateExternalEmailAddress();
	<?php
		} 
		else 
		{
	?>
			return true;
	<?php		
		}
	?>
}

function validateExternalEmailAddress()
{
	objExtTo = document.form1.ExternalTo;
	objExtCc = document.form1.ExternalCC;
	objExtBcc= document.form1.ExternalBCC;
	
	if(objExtTo == null || objExtCc == null || objExtBcc == null) return false;
	v_ext_to = objExtTo.value;
	v_ext_cc = objExtCc.value;
	v_ext_bcc= objExtBcc.value;
	
	var msg = '<?=$i_invalid_email?>';
	
	invalid_to = getInvalidEmailAddresses(v_ext_to);
	invalid_cc = getInvalidEmailAddresses(v_ext_cc);
	invalid_bcc= getInvalidEmailAddresses(v_ext_bcc);
	
	//invalid_to = invalid_to.concat(invalid_cc,invalid_bcc);
	if(invalid_to.length>0){
		str = invalid_to.join("\n");
		alert(msg+":\n\n"+str);
		objExtTo.focus();
		return false;
	}
	if(invalid_cc.length>0){
		str = invalid_cc.join("\n");
		alert(msg+":\n\n"+str);
		objExtCc.focus();
		return false;
	}
	if(invalid_bcc.length>0){
		str = invalid_bcc.join("\n");
		alert(msg+":\n\n"+str);
		objExtBcc.focus();
		return false;
	}
	
	return true;
}
function getInvalidEmailAddresses(rec)
{		
	var mails = new Array();
	mailCnt = 0;
	mails2 = rec.split(";");	
	for(var i=0;i<mails2.length;i++)
	{		
		var tmpMails = mails2[i].split(",");			
		
		for(var j=0;j<tmpMails.length;j++)
		{			
			mails[mailCnt] = tmpMails[j];
			mailCnt++;
		}
	}
	
	invalid = new Array();
	for(i=0;i<mails.length;i++)
	{		
			mail = Trim(mails[i]);
			if(mail=="") continue;
			last_pos = mail.lastIndexOf("<");
			last_pos2= mail.lastIndexOf(">");
			if(last_pos != -1 && last_pos2 != -1 && last_pos2 > last_pos && last_pos2==mail.length-1){
				mail = mail.substring(last_pos+1,last_pos2);
			}
			
			if(!validateEmailAddress(mail))
				invalid.push(mails[i]);
	}
	return invalid;
}
function validateEmailAddress(email){
	
        //var re = /^.+@.+\..{2,3}$/;
        var re = /^([0-9A-Za-z_\.\-])+\@(([0-9A-Za-z\-])+\.)+([0-9A-Za-z]{2,3})+$/
        
        if (re.test(email)) {
                return true;
        }else{
                return false;
        }
}
</script>

<script language='javascript'>
y='';
function setContent()
{
         x = "<?=(($action==""&&$CampusMailReplyID=="")?"":"$i_CampusMail_New_Wrote")?>";
         x = contentframe.document.content_form.who_wrote.value+x;
<?php
 /*
         if($lcampusmail->mailType==2){
                 echo "x = contentframe.document.content_form.who_wrote.value+x;";
         }else{
                  echo "x = '$preset_message_whowrite'+x;";
         }
*/
?>
<?php
         if($action!="D"){
                 echo "x = x+contentframe.document.content_form.prev_msg.value;";
         }else{
                 echo "x = contentframe.document.content_form.prev_msg.value;";
         }

?>
        signatureObj = document.getElementById('signature');
        signature_text='';
        if(signatureObj!=null)
        {
			signature_text = <?php if($action!="D") echo "signatureObj.value;"; else echo "'';";?>
        }
        if (signature_text.length == 0)
        {
         	//document.form1.Message.value=x;
         	y = x;
        } else {
         	//document.form1.Message.value=x+signature_text;
         	y = x+signature_text;
		}
}
function resetForm(obj){
        //obj.reset();
        //document.form1.Message.value=y;
        location.href='<?=$HTTP_SERVER_VARS['PHP_SELF']."?".$HTTP_SERVER_VARS['QUERY_STRING']?>';
}

var  withInternalCC = false;
var  withInternalBCC = false;
function jADD_INTERNAL_CC()
{
	displayTable('internalCCBtnDiv',"none");	
	displayTable('internalStrokeDiv',"none");	
	displayTable('internalCCDiv',"block");		
	withInternalCC = true;
}

function jADD_INTERNAL_BCC()
{
	displayTable('internalBCCBtnDiv',"none");	
	displayTable('internalStrokeDiv',"none");	
	displayTable('internalBCCDiv',"block");
	withInternalBCC = true;
}

function jDELETE_INTERNAL_CC()
{
	displayTable('internalCCBtnDiv',"block");	
	if (!withInternalBCC)
	{
		displayTable('internalStrokeDiv',"block");	
	}	
	displayTable('internalCCDiv',"none");		
	checkOptionClear(document.form1.elements['InternalCC[]']);
	withInternalCC = false;
	
	jCHECK_INTERNAL();
}

function jDELETE_INTERNAL_BCC()
{
	displayTable('internalBCCBtnDiv',"block");	
	if (!withInternalCC)
	{	
		displayTable('internalStrokeDiv',"block");	
	}
	displayTable('internalBCCDiv',"none");
	checkOptionClear(document.form1.elements['InternalBCC[]']);	
	withInternalBCC = false;
	
	jCHECK_INTERNAL();
}

var  withExternalCC = false;
var  withExternalBCC = false;
function jADD_EXTERNAL_CC()
{
	displayTable('externalCCBtnDiv',"none");	
	displayTable('externalStrokeDiv',"none");	
	displayTable('externalCCDiv',"block");		
	withExternalCC = true;
}

function jADD_EXTERNAL_BCC()
{
	displayTable('externalBCCBtnDiv',"none");	
	displayTable('externalStrokeDiv',"none");	
	displayTable('externalBCCDiv',"block");
	withExternalBCC = true;
}

function jDELETE_EXTERNAL_CC()
{
	displayTable('externalCCBtnDiv',"block");	
	if (!withExternalBCC)
	{
		displayTable('externalStrokeDiv',"block");	
	}	
	displayTable('externalCCDiv',"none");		
	document.form1.ExternalCC.value="";
	withExternalCC = false;
}

function jDELETE_EXTERNAL_BCC()
{
	displayTable('externalBCCBtnDiv',"block");	
	if (!withExternalCC)
	{	
		displayTable('externalStrokeDiv',"block");	
	}
	displayTable('externalBCCDiv',"none");
	document.form1.ExternalBCC.value="";
	withExternalBCC = false;
}

function jCHECK_INTERNAL()
{
	if (document.form1.elements['Recipient[]'].length == 0)
	{
		displayTable('internalToTextDiv','none');
		displayTable('internalToRemoveBtnDiv','none');		
	}	
	if (document.form1.elements['InternalCC[]'].length == 0)
	{
		displayTable('internalCCTextDiv','none');
		displayTable('internalCCRemoveBtnDiv','none');		
	}	
	if (document.form1.elements['InternalBCC[]'].length == 0)
	{
		displayTable('internalBCCTextDiv','none');
		displayTable('internalBCCRemoveBtnDiv','none');		
	}	
	
}

function jCHECK_ATTACHMENT()
{
	if (document.form1.elements['Attachment[]'].length == 0)
	{	
		displayTable('AttachmentDiv','none');
		displayTable('AttachmentBtnDiv','none');
	}
	else 
	{
		
		displayTable('AttachmentDiv','block');
		displayTable('AttachmentBtnDiv','block');
	}
}

function jSubmitForm(num)
{
	if (num == "1")
	{
		//checkOption(document.form1.elements['Attachment[]']);
		//getRealName(); 
		//checkOption(document.form1.elements['Recipient[]']);
		//checkOption(document.form1.elements['InternalCC[]']);
		//checkOption(document.form1.elements['InternalBCC[]']);
		document.form1.SubmitType.value=0;
	}
	else if (num == "2")
	{		
		//checkOption(document.form1.elements['Attachment[]']); 
		//checkOption(document.form1.elements['Recipient[]']);
		//checkOption(document.form1.elements['InternalCC[]']);
		//checkOption(document.form1.elements['InternalBCC[]']);
		document.form1.SubmitType.value=1;
	}
	if (checkform(document.form1))
	{
		document.form1.submit();
	}
}

<? if ($hide_internal && $hide_external) { ?>
alert("The system has been configured to restrict all email sending.");
location.href="index.php";
<? } ?>

</script>

	<!-- AutoComplete begins -->
    <div id="statesmod">
        <form onsubmit="return YAHOO.example.ACJSArray.validateForm();">

        </form>
    </div>
    <!-- AutoComplete ends -->

<form name="form1" method="post" action="compose_update2.php" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<?=$xmsg?>
<tr>
	<td>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td>
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		<? if (!$hide_internal) { ?>
		<?//= Internal To ?>
		<tr >
			<td style="vertical-align:top" nowrap="nowrap" class="formfieldtitle">
			<span class="tabletext"><?=$i_CampusMail_New_To?></span><br />
			<span class="tabletextremark" ><?=$i_CampusMail_New_InternalRecipients?></span>
			</td>
			<td width="80%" style="vertical-align:top">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="tabletext">
				<table border="0" cellpadding="2" cellspacing="0" class="tabletext">
				<tr>
					<?php
					if ($mailtarget_teacherAllowed && $imail_feature_allowed['withcheckbox'] && $toStaff == 0)
					{
					?>
						<td style="vertical-align:middle">
						<input type="checkbox" name="AllStaffTo" value="1" id="checkStaff1" />
						</td>
						<td style="vertical-align:middle" >
						<label for="checkStaff1" >
						<?=$i_campusmail_all_teacher_staff?>
						</label>
						</td>
						<td style="vertical-align:middle" ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></td>
					<?php
					}

					if ($mailtarget_studentAllowed && $imail_feature_allowed['withcheckbox'] && $toStudent == 0)
					{
					?>

						<td style="vertical-align:middle" ><input type="checkbox" name="AllStudentTo" value="1" id="checkStudent1" /></td>
						<td style="vertical-align:middle" >
						<label for="checkStudent1" >
						<?=$i_campusmail_all_students?>
						</label>
						</td>
						<td style="vertical-align:middle" ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></td>
					<?php
					}

					if ($mailtarget_parentAllowed && $imail_feature_allowed['withcheckbox'] && $toParent == 0)
					{
					?>
						<td style="vertical-align:middle" ><input type="checkbox" name="AllParentTo" value="1" id="checkParent1" /></td>
						<td style="vertical-align:middle" >
						<label for="checkParent1" >
						<?=$i_campusmail_all_parents?>
						</label>
						</td>

					<?php
					}
					?>
				</tr>
				</table>
				</td>
			</tr>
			<tr <?=$InternalToStyle?> id ="internalToTextDiv" >
				<td class="tabletext">
				<select name="Recipient[]" size="5" multiple style="width:100%" style="background: #FFFFFF; border-left: 1px #FFFFFF solid;"  >
				<?php
				if ($preset_senderid != "" && $preset_sendername)
				   echo "<OPTION value='U$preset_senderid' >$preset_sendername</OPTION>\n";
				for ($i=0; $i<sizeof($mail_recipients); $i++)
				{
				    list($id, $name) = $mail_recipients[$i];
				    echo "<OPTION value='$id' >$name</OPTION>\n";
				}
				?>
				</select>
				</td>
			</tr>
			<tr>
				<td class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
					<table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td>
						<a href="javascript:newWindow('choose/alias_in.php?fieldname=Recipient[]',9)" class='iMailsubject' >
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_alias_group.gif" alt="<?=$i_CampusMail_New_SelectFromAlias?>" width="20" height="20" border="0" align="absmiddle" />
						<?=$i_CampusMail_New_SelectFromAlias?>
						</a>
						</td>
						<td>
						<a href="javascript:newWindow('choose/index.php?fieldname=Recipient[]',9)" class='iMailsubject' >
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_address_book.gif" alt="<?=$i_frontpage_campusmail_choose?>" width="20" height="20" border="0" align="absmiddle" />
						<?=$i_frontpage_campusmail_choose?>
						</a>
						</td>
						<td <?=$InternalToStyle?> id ="internalToRemoveBtnDiv"  >
						<a href="javascript:checkOptionRemove(document.form1.elements['Recipient[]']);jCHECK_INTERNAL()" class='iMailsubject' >
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_delete_selected.gif" alt="<?=$i_frontpage_campusmail_remove?>" width="20" height="20" border="0" align="absmiddle" />
						<?=$i_frontpage_campusmail_remove?>
						</a>
						</td>
					</tr>
					</table>
					</td>
					<td align="right">
					<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td nowrap="nowrap" >					
						<span id ="internalCCBtnDiv" ><a href="javascript:jADD_INTERNAL_CC()" class="tabletool">
						<?=$i_frontpage_campusmail_add.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_CC?>
						</a></span>						
						</td >
						<td >
						<span id ="internalStrokeDiv" >&nbsp;|&nbsp;</span>
						</td >
						<td nowrap="nowrap" >
						<span id ="internalBCCBtnDiv" ><a href="javascript:jADD_INTERNAL_BCC()" class="tabletool">
						<?=$i_frontpage_campusmail_add.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_BCC?>
						</a></span>
						</td >
					</tr>
					</table>
					</td>
				</tr>
				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>

		<?//= //Internal CC ?>
		<tr id ="internalCCDiv" style="display:none" >
			<td style="vertical-align:top" nowrap="nowrap" class="formfieldtitle">
			<span class="tabletext"><?=$i_CampusMail_New_CC?></span><br />
			<span class="tabletextremark" ><?=$i_CampusMail_New_InternalRecipients?></span>
			</td>
			<td width="80%" style="vertical-align:top">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="tabletext">
				<table border="0" cellpadding="2" cellspacing="0" class="tabletext">
				<tr>
					<?php
					if ($mailtarget_teacherAllowed && $imail_feature_allowed['withcheckbox'] && $toStaff == 0)
					{
					?>
						<td style="vertical-align:middle">
						<input type="checkbox" name="AllStaffCC" value="1" id="checkStaff2" />
						</td>
						<td style="vertical-align:middle" >
						<label for="checkStaff2" >
						<?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_teachstaff?>
						</label>
						</td>
						<td style="vertical-align:middle" ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></td>
					<?php
					}

					if ($mailtarget_studentAllowed && $imail_feature_allowed['withcheckbox'] && $toStudent == 0)
					{
					?>

						<td style="vertical-align:middle" ><input type="checkbox" name="AllStudentCC" value="1" id="checkStudent2" /></td>
						<td style="vertical-align:middle" >
						<label for="checkStudent2" >
						<?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_student?>
						</label>
						</td>
						<td style="vertical-align:middle" ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></td>
					<?php
					}

					if ($mailtarget_parentAllowed && $imail_feature_allowed['withcheckbox'] && $toParent == 0)
					{
					?>
						<td style="vertical-align:middle" ><input type="checkbox" name="AllParentCC" value="1" id="checkParent2" /></td>
						<td style="vertical-align:middle" >
						<label for="checkParent2" >
						<?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_parent?>
						</label>
						</td>

					<?php
					}
					?>
				</tr>
				</table>
				</td>
			</tr>
			<tr <?=$InternalCCStyle?> id ="internalCCTextDiv" >
				<td class="tabletext">
				<select name="InternalCC[]" size="3" multiple  style="width:100%" >
				<?php
                   for ($i=0; $i<sizeof($mail_cc_recipients); $i++)
                   {
                        list($id, $name) = $mail_cc_recipients[$i];
                        echo "<OPTION value='$id' >$name</OPTION>\n";
                   }
				?>
				</td>
			</tr>
			<tr>
				<td class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td >
					<table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td >
						<a href="javascript:newWindow('choose/alias_in.php?fieldname=InternalCC[]',9)" class='iMailsubject' >
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_alias_group.gif" alt="<?=$i_CampusMail_New_SelectFromAlias?>" width="20" height="20" border="0" align="absmiddle" />
						<?=$i_CampusMail_New_SelectFromAlias?>
						</a>
						</td>
						<td>
						<a href="javascript:newWindow('choose/index.php?fieldname=InternalCC[]',9)" class='iMailsubject'>
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_address_book.gif" alt="<?=$i_frontpage_campusmail_choose?>" width="20" height="20" border="0" align="absmiddle" />
						<?=$i_frontpage_campusmail_choose?>
						</a>
						</td>
						<td <?=$InternalCCStyle?> id ="internalCCRemoveBtnDiv"  >
						<a href="javascript:checkOptionRemove(document.form1.elements['InternalCC[]']);jCHECK_INTERNAL()" class='iMailsubject'>
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_delete_selected.gif" alt="<?=$i_frontpage_campusmail_remove?>" width="20" height="20" border="0" align="absmiddle" />
						<?=$i_frontpage_campusmail_remove?>
						</a>
						</td>
						<td>&nbsp;</td>
					</tr>
					</table>
					</td>
					<td align="right">
					<a href="javascript:jDELETE_INTERNAL_CC()" class="tabletool">
					<?=$button_cancel?>
					</a>						
					</td>					
				</tr>
				</table>

				</td>
			</tr>
			</table>
			</td>
		</tr>
		<?php if($activate_InternalCC){
				echo "<script language='javascript'>\n";
				echo "jADD_INTERNAL_CC();\n";
				echo "</script>\n";
			 }
		?>
		<?//= //Internal BCC ?>
		<tr id ="internalBCCDiv" style="display:none" >
			<td style="vertical-align:top" nowrap="nowrap" class="formfieldtitle">
			<span class="tabletext"><?=$i_CampusMail_New_BCC?></span><br />
			<span class="tabletextremark" ><?=$i_CampusMail_New_InternalRecipients?></span>
			</td>
			<td width="80%" style="vertical-align:top">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="tabletext">
				<table border="0" cellpadding="2" cellspacing="0" class="tabletext">
				<tr>
					<?php
					if ($mailtarget_teacherAllowed && $imail_feature_allowed['withcheckbox'] && $toStaff == 0)
					{
					?>
						<td style="vertical-align:middle">
						<input type="checkbox" name="AllStaffBCC" value="1" id="checkStaff3" />
						</td>
						<td style="vertical-align:middle" >
						<label for="checkStaff3" >
						<?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_teachstaff?>
						</label>
						</td>
						<td style="vertical-align:middle" ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></td>
					<?php
					}

					if ($mailtarget_studentAllowed && $imail_feature_allowed['withcheckbox'] && $toStudent == 0)
					{
					?>

						<td style="vertical-align:middle" ><input type="checkbox" name="AllStudentBCC" value="1" id="checkStudent3" /></td>
						<td style="vertical-align:middle" >
						<label for="checkStudent3" >
						<?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_student?>
						</label>
						</td>
						<td style="vertical-align:middle" ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></td>
					<?php
					}

					if ($mailtarget_parentAllowed && $imail_feature_allowed['withcheckbox'] && $toParent == 0)
					{
					?>
						<td style="vertical-align:middle" ><input type="checkbox" name="AllParentBCC" value="1" id="checkParent3" /></td>
						<td style="vertical-align:middle" >
						<label for="checkParent3" >
						<?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_parent?>
						</label>
						</td>

					<?php
					}
					?>
				</tr>
				</table>
				</td>
			</tr>
			<tr <?=$InternalBCCStyle?> id ="internalBCCTextDiv" >
				<td class="tabletext">
               	<select name="InternalBCC[]" size="3" multiple  style="width:100%" >
               <?php
               for ($i=0; $i<sizeof($mail_bcc_recipients); $i++)
               {
                    list($id, $name) = $mail_bcc_recipients[$i];
                    echo "<OPTION value=$id>$name</OPTION>\n";
               }
               ?>
               </select>
				</td>
			</tr>
			<tr>
				<td class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td >
					<table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td>
						<a href="javascript:newWindow('choose/alias_in.php?fieldname=InternalBCC[]',9)" class='iMailsubject' >
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_alias_group.gif" alt="<?=$i_CampusMail_New_SelectFromAlias?>" width="20" height="20" border="0" align="absmiddle" />
						<?=$i_CampusMail_New_SelectFromAlias?>
						</a>
						</td>
						<td>
						<a href="javascript:newWindow('choose/index.php?fieldname=InternalBCC[]',9)" class='iMailsubject'  >
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_address_book.gif" alt="<?=$i_frontpage_campusmail_choose?>" width="20" height="20" border="0" align="absmiddle" />
						<?=$i_frontpage_campusmail_choose?>
						</a>
						</td>
						<td <?=$InternalBCCStyle?> id ="internalBCCRemoveBtnDiv"  >
						<a href="javascript:checkOptionRemove(document.form1.elements['InternalBCC[]']);jCHECK_INTERNAL()" class='iMailsubject' >
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_delete_selected.gif" alt="<?=$i_frontpage_campusmail_remove?>" width="20" height="20" border="0" align="absmiddle" />
						<?=$i_frontpage_campusmail_remove?>
						</a>
						</td>
						<td>&nbsp;</td>
					</tr>
					</table>
					</td>					
					<td align="right">
					<a href="javascript:jDELETE_INTERNAL_BCC()" class="tabletool">
					<?=$button_cancel?>
					</a>						
					</td>										
				</tr>
				</table>

				</td>				
			</tr>
			</table>
			</td>
		</tr>
		<? } ?>

		
		<?php
		if (!$noWebmail)
		{
		?>
		<? if (!$hide_external) { ?>
		<?//= //External To ?>
		<?
		$addressSql = "
						SELECT 
							AddressID, CONCAT(TargetName,' &lt;',TargetAddress,'&gt;') 
						FROM 
							INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
						WHERE 
							OwnerID = $UserID
						";
		$TotalArr = $lcampusmail->returnArray($addressSql,2); 
		for ($countli = 0; $countli < sizeof($TotalArr); $countli++) {
			($countli == 0) ? $liList = "<li class=\"\" style=\"display: none;\">".$TotalArr[$countli][1]."</li>\n" : $liList = "<li style=\"display: none;\">".$TotalArr[$countli][1]."</li>\n";
			$liArr .= "\"".str_replace("&gt;", ">", str_replace("&lt;", "<", $TotalArr[$countli][1]))."\"";
			($countli == (sizeof($TotalArr)-1)) ? $liArr .= "" : $liArr .= ",\n";
		}		
		?>
		<tr>
			<td style="vertical-align:top" nowrap="nowrap" class="formfieldtitle">
			<span class="tabletext"><?=$i_CampusMail_New_To?></span><br />
			<span class="tabletextremark"><?=$i_CampusMail_New_ExternalRecipients?></span>			
			</td>
			<td style="vertical-align:top">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="tabletext">
				<span class="tabletextremark">(<?=$i_CampusMail_New_EmailAddressSeparationNote?>)</span>
				</td>
			</tr>
			<tr>
				<td class="tabletext">							
				<div id="statesautocomplete">
				<?=$linterface->GET_TEXTAREA("ExternalTo", $preset_senderemail, 50, 5)?>
				<div id="statescontainer"><div style="display: none; width: 199px; height: 0px;" class="yui-ac-content"><div style="display: none;" class="yui-ac-hd"></div><div class="yui-ac-bd">
					<ul>
						<?= $liList?>
					</ul>
					</div><div style="display: none;" class="yui-ac-ft"></div></div><div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div></div>
				</div>
				</td>
			</tr>
			<tr>
				<td class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
					<table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td>
						<a href="javascript:newWindow('choose_ex/alias_ex.php?fieldname=ExternalTo',9)" class='iMailsubject' >
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_alias_group.gif" alt="<?=$i_CampusMail_New_SelectFromAlias?>" width="20" height="20" border="0" />
						<?=$i_CampusMail_New_SelectFromAlias?>
						</a>
						</td>
						<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></td>
						<td>
						<a href="javascript:newWindow('choose_ex/index.php?fieldname=ExternalTo',9)" class='iMailsubject' >
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_address_book.gif" alt="<?=$i_frontpage_campusmail_choose?>" width="20" height="20" border="0" />
						<?=$i_frontpage_campusmail_choose?>
						</a>
						</td>
					</tr>
					</table>
					</td>
					<td align="right">
					<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td >					
						<span id ="externalCCBtnDiv" ><a href="javascript:jADD_EXTERNAL_CC()" class="tabletool">
						<?=$i_frontpage_campusmail_add.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_CC?>
						</a></span>						
						</td >
						<td >
						<span id ="externalStrokeDiv" >&nbsp;|&nbsp;</span>
						</td >
						<td >
						<span id ="externalBCCBtnDiv" ><a href="javascript:jADD_EXTERNAL_BCC()" class="tabletool">
						<?=$i_frontpage_campusmail_add.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_BCC?>
						</a></span>
						</td >
					</tr>
					</table>					
					</td>
				</tr>
				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>

		<?//= //External CC ?>
		<tr id="externalCCDiv" style="display:none" >
			<td style="vertical-align:top" nowrap="nowrap" class="formfieldtitle">
			<span class="tabletext"><?=$i_CampusMail_New_CC?></span><br />
			<span class="tabletextremark"><?=$i_CampusMail_New_ExternalRecipients?></span>
			</td>
			<td style="vertical-align:top">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="tabletext">
				<span class="tabletextremark">(<?=$i_CampusMail_New_EmailAddressSeparationNote?>)</span>
				</td>
			</tr>
			<tr>
				<td class="tabletext">
				<div id="statesautocomplete">
				<?=$linterface->GET_TEXTAREA("ExternalCC", $preset_cc_address, 50, 3)?>
				<div id="statescontainerCC"><div style="display: none; width: 199px; height: 0px;" class="yui-ac-content"><div style="display: none;" class="yui-ac-hd"></div><div class="yui-ac-bd">
					<ul>
						<?= $liList?>
					</ul>
					</div><div style="display: none;" class="yui-ac-ft"></div></div><div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div></div>
				</div>
				</td>
			</tr>
			<tr>
				<td class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td >
					<table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td>
						<a href="javascript:newWindow('choose_ex/alias_ex.php?fieldname=ExternalCC',9)" class='iMailsubject' >
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_alias_group.gif" alt="<?=$i_CampusMail_New_SelectFromAlias?>" width="20" height="20" border="0" />
						<?=$i_CampusMail_New_SelectFromAlias?>
						</a>
						</td>
						<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></td>
						<td>
						<a href="javascript:newWindow('choose_ex/index.php?fieldname=ExternalCC',9)" class='iMailsubject' >
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_address_book.gif" alt="<?=$i_frontpage_campusmail_choose?>" width="20" height="20" border="0" />
						<?=$i_frontpage_campusmail_choose?>
						</a>
						</td>
						<td>&nbsp;</td>
					</tr>
					</table>
					</td>					
					<td align="right">
					<a href="javascript:jDELETE_EXTERNAL_CC()" class="tabletool">
					<?=$button_cancel?>
					</a>						
					</td>					
				</tr>
				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>
		<?php if($activate_ExternalCC){
				echo "<script language='javascript'>\n";
				echo "jADD_EXTERNAL_CC();\n";
				echo "</script>\n";
			 }
		?>
		<?//= //External BCC ?>
		<tr id ="externalBCCDiv" style="display:none" >
			<td style="vertical-align:top" nowrap="nowrap" class="formfieldtitle">
			<span class="tabletext"><?=$i_CampusMail_New_BCC?></span><br />
			<span class="tabletextremark"><?=$i_CampusMail_New_ExternalRecipients?></span>
			</td>
			<td style="vertical-align:top">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="tabletext">
				<span class="tabletextremark">(<?=$i_CampusMail_New_EmailAddressSeparationNote?>)</span>
				</td>
			</tr>
			<tr>
				<td class="tabletext">
				<div id="statesautocomplete">
				<?=$linterface->GET_TEXTAREA("ExternalBCC", $preset_bcc_address, 50, 3)?>
				<div id="statescontainerBCC"><div style="display: none; width: 199px; height: 0px;" class="yui-ac-content"><div style="display: none;" class="yui-ac-hd"></div><div class="yui-ac-bd">
					<ul>
						<?= $liList?>
					</ul>
					</div><div style="display: none;" class="yui-ac-ft"></div></div><div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div></div>
				</div>
				</td>
			</tr>
			<tr>
				<td class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
					<table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td>
						<a href="javascript:newWindow('choose_ex/alias_ex.php?fieldname=ExternalBCC',9)" class='iMailsubject' >
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_alias_group.gif" alt="<?=$i_CampusMail_New_SelectFromAlias?>" width="20" height="20" border="0" />
						<?=$i_CampusMail_New_SelectFromAlias?>
						</a>
						</td>
						<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></td>
						<td>
						<a href="javascript:newWindow('choose_ex/index.php?fieldname=ExternalBCC',9)" class='iMailsubject' >
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/btn_address_book.gif" alt="<?=$i_frontpage_campusmail_choose?>" width="20" height="20" border="0" />
						<?=$i_frontpage_campusmail_choose?>
						</a>
						</td>
						<td>&nbsp;</td>
					</tr>
					</table>
					</td>					
					<td align="right">
					<a href="javascript:jDELETE_EXTERNAL_BCC()" class="tabletool">
					<?=$button_cancel?>
					</a>						
					</td>					
				</tr>
				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>
		<? } ?>
		<?php
	 	}
		?>

		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle">
			<span class="tabletext"><?=$i_frontpage_campusmail_subject?></span>
			</td>
			<td>
			<input class="textboxtext" type="text" name="Subject" size="55" maxlength="255" value="<?=$preset_subject?>" />
			</td>
		</tr>

		<tr valign="top">
			<td valign="top" nowrap="nowrap" class="formfieldtitle">
			<span class="tabletext">
			<?=$i_frontpage_campusmail_icon_attachment_2007?> <?=$i_frontpage_campusmail_attachment?></span>
			</td>
			<td>
			<table border="0" cellpadding="2" cellspacing="0"  width='120'>
			<tr>
				<td id="AttachmentDiv" style="display: none;"  >
				<select name="Attachment[]" size="4" multiple="multiple" style="width:250" >
				<!--
				<option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option>
				-->
				</select>
					<?php
						if ($preset_quota_notenough)
						{
							echo "<font color=red>$i_CampusMail_New_QuotaNotEnough</font>";
						}
					?>					
				</td>
				<td valign="bottom">
				<?php
						if ($preset_quota_notenough)
						{
							echo "<div id=\"AttachmentBtnDiv\" style=\"display: none;\" ></div>"; 
						} 
						else 
						{				
							echo $linterface->GET_BTN("   {$button_add_attachment}  ", "button", "newWindow('attach.php?folder=".urlencode($composeFolder)."',2)","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."<br /><br />"; 
							echo "<div id=\"AttachmentBtnDiv\" style=\"display: none;\"  >".$linterface->GET_BTN($button_remove_selected, "button", "checkOptionRemove(document.form1.elements['Attachment[]']);removeRealName();jCHECK_ATTACHMENT()","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</div>"; 
						}
				?>
				</td>
			</tr>
			</table>
			</td>
		</tr>

		<tr valign="top">
			<td valign="top" nowrap="nowrap" class="formfieldtitle">
			<span class="tabletext"><?=$i_frontpage_campusmail_message?></span>
			</td>
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
          		<?php
          		if ($use_html_editor)
          		{
					# Components size
					$msg_box_width = "100%";
					$msg_box_height = 320;
					$obj_name = "Message";
					$editor_width = $msg_box_width;
					$editor_height = $msg_box_height;
					#$init_html_content = $preset_message;
					//include($PATH_WRT_ROOT."includes/html_editor_embed2.php");
					
					include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
					$oFCKeditor = new FCKeditor('Message') ;
					$oFCKeditor->BasePath = $PATH_WRT_ROOT.'templates/html_editor/';
					//$oFCKeditor->Config['SkinPath'] = 'skins/silver/' ;
					$oFCKeditor->ToolbarSet = 'Basic2';
					//$oFCKeditor->Value = '<p>This is some <strong>sample text</strong>. You are using <a href="http://www.fckeditor.net/">FCKeditor</a>.</p>' ;
					$oFCKeditor->Value = "hohohohohoo";
					$oFCKeditor->Create() ;
					
                } else {
	                //echo $linterface->GET_TEXTAREA("Message", $preset_message, 80, 10);
				}
				?>
				</td>
			</tr>
			<tr>
				<td>
				<table border="0" cellpadding="2" cellspacing="0" class="tabletext">
				<tr>
					<td valign="middle">
					<input type="checkbox" name="IsImportant" id="CheckImportant" value="1" <?=($preset_important? "CHECKED":"")?> />
					</td>
					<td valign="middle">
					<label for="CheckImportant" >
					<?=$i_frontpage_campusmail_icon_important2_2007?> <?=$i_frontpage_campusmail_important?>
					</label>
					</td>
					<td valign="middle"><label><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" /></label></td>
					<td valign="middle">
					<input type="checkbox" name="IsNotification" value="1" <?=($preset_notify? "CHECKED":"")?> id="CheckNotification" />
					</td>
					<td valign="middle">
					<label for="CheckNotification" >
					<?=$i_frontpage_campusmail_icon_notification2_2007?> <?=$i_frontpage_campusmail_notification?>
					<span class="tabletextremark"> ( <?=$i_CampusMail_New_NotificationInternalOnly?>  )</span>
					</td>
				</tr>
				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>

		<tr>
			<td valign="top" nowrap="nowrap" >
			<span class="tabletextremark" >
			<?=$i_general_required_field2?>
			</span>
			</td>
		</tr>

		</table>
		</td>
	</tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1" /></td></tr>
	<tr>
		<td align="right">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_send, "button", "jSubmitForm(1)") ?>
			<?= $linterface->GET_ACTION_BTN($button_save_as_draft, "button", "jSubmitForm(2)") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "button", "resetForm(document.form1)") ?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	</table>
	<br />
	</td>
</tr>
</table>


<?php
	if($use_html_editor)
	{
		echo "<textarea id='signature' style='visibility:hidden' rows='0' cols='0' >".$signature."</textarea>";
	}
	else
	{
		echo "<textarea id='signature' style='visibility:hidden' rows='0' cols='0' >".$signature."</textarea>";
	}
?>
<input type="hidden" name="SubmitType" value="0" />
<input type="hidden" name="action" value="<?=$action?>" />
<input type="hidden" name="OriginalCampusMailID" value="<?=$CampusMailID?>" />
<input type="hidden" name="isHTML" value="<?=($use_html_editor?1:-1)?>" />
<iframe name="contentframe" id="contentframe" width="0" height="0" src="compose_content2.php?tmpFolder=<?=urlencode($temp_folder)?>&CampusMailID=<?=$CampusMailID?>&action=<?=$action?>&CampusMailReplyID=<?=$CampusMailReplyID?>" >
</iframe>


<select name="real_name[]" style='visibility:hidden'>
</select>
<script language='javascript'>
function removeRealName()
{
    target_obj = document.form1.elements["Attachment[]"];
    src_obj = document.form1.elements["real_name[]"];
    if(src_obj==null || target_obj==null) return;
    for(i=0;i<src_obj.options.length;i++)
    {
        v = src_obj.options[i].value;
        exist = false;
        for(j=0;j<target_obj.options.length;j++)
        {
            t_v = target_obj.options[j].value;
            if(v == t_v)
            {
                exist = true;
                break;
            }
        }
        if(!exist)
			src_obj.options[i] = null;
    }
}
function getRealName(){
        obj = document.form1.elements["real_name[]"];
        if(obj==null) return;
        s='';
        for(i=0;i<obj.options.length;i++){
                v = obj.options[i].value;
                t = obj.options[i].text;
                s+=v+':'+t+'|||';
        }
        s = s.substring(0,s.length-3);
        document.form1.elements["file_names"].value=s;
}
</script>
<input type="hidden" name="file_names" value='' />
</form>
    
    
<!-- Libary begins -->
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/yahoo.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dom.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/event-debug.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/animation.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/autocomplete-debug.js"></script>
<!-- Library ends -->

<!-- In-memory JS array begins-->
<script type="text/javascript">
var statesArray = [
    <?= $liArr?>
];
var delimArray = [
    ";"
];
</script>
<!-- In-memory JS array ends-->


<script type="text/javascript">
/*
YAHOO.example.ACJSArray = function() {
    var oACDS, oACDSCC, oACDSBCC;
    var oAutoComp, oAutoCompCC, oAutoCompBCC;
    return {
        init: function() {

            // Instantiate first JS Array DataSource
            oACDS = new YAHOO.widget.DS_JSArray(statesArray);
            oACDSCC = new YAHOO.widget.DS_JSArray(statesArray);
            oACDSBCC = new YAHOO.widget.DS_JSArray(statesArray);

            // Instantiate first AutoComplete            
            oAutoComp = new YAHOO.widget.AutoComplete('ExternalTo','statescontainer', oACDS);
            oAutoComp.queryDelay = 0;
            oAutoComp.prehighlightClassName = "yui-ac-prehighlight";
            //oAutoComp.typeAhead = true;
            oAutoComp.useShadow = true;
            oAutoComp.minQueryLength = 0;
            oAutoComp.delimChar = delimArray;
            
            // Instantiate CC AutoComplete            
            oAutoCompCC = new YAHOO.widget.AutoComplete('ExternalCC','statescontainerCC', oACDSCC);
            oAutoCompCC.queryDelay = 0;
            oAutoCompCC.prehighlightClassName = "yui-ac-prehighlight";
            //oAutoCompCC.typeAhead = true;
            oAutoCompCC.useShadow = true;
            oAutoCompCC.minQueryLength = 0;
            oAutoCompCC.delimChar = delimArray;            
            
            // Instantiate BCC AutoComplete            
            oAutoCompBCC = new YAHOO.widget.AutoComplete('ExternalBCC','statescontainerBCC', oACDSBCC);
            oAutoCompBCC.queryDelay = 0;
            oAutoCompBCC.prehighlightClassName = "yui-ac-prehighlight";
            //oAutoCompBCC.typeAhead = true;
            oAutoCompBCC.useShadow = true;
            oAutoCompBCC.minQueryLength = 0;
            oAutoCompBCC.delimChar = delimArray;            
        },

        validateForm: function() {
            // Validate form inputs here
            return false;
        }
    };
}();

YAHOO.util.Event.addListener(this,'load',YAHOO.example.ACJSArray.init);
*/
</script>

<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.js"></script>
<script type="text/javascript">
/* dp.SyntaxHighlighter.HighlightAll('code'); */
</script>
    
<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>
