<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libfiletable.php");
include_once("../../includes/libwebmail.php");
include_once("../../includes/libaccess.php");
include_once("../../includes/libcampusmail.php");
include_once("../../includes/libcampusquota.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
auth_campusmail();
if (!auth_sendmail())
{
    header("Location: index.php");
    exit();
}

$lcampusmail = new libcampusmail($CampusMailID);

$li = new libfilesystem();
$personal_path = "$file_path/file/mail/u$UserID";
if (!is_dir($personal_path))
{
    $li->folder_new($personal_path);
}

if ($special_feature['imail_richtext'])
{
    $use_html_editor = true;
}

$lwebmail = new libwebmail();

if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) && $lwebmail->type == 3)
{
    $noWebmail = false;
}
else
{
    $noWebmail = true;
}

if ($action=="D")   # Draft
{
    if(!session_is_registered("composeFolder")){
        session_register("composeFolder");
    }
    $composeFolder = $lcampusmail->Attachment;
    if ($composeFolder == "")
    {
        header("Location: index.php");
        exit();
    }
    # Remove unsaved attachment files
    $sql = "SELECT FileName FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE CampusMailID = $CampusMailID";
    $file_array = $lcampusmail->returnVector($sql);

    # compute size of attachment
    $path = "$intranet_root/file/mail/".$lcampusmail->Attachment;
    $lsize = new libfiletable("",$path, 0,0,"");
    $files = $lsize->files;
    while (list($key, $value) = each($files)) {
           list($filename, $filesize) = $files[$key];
           if (!in_array($filename,$file_array))
           {
                $li->file_remove("$path/$filename");
           }
    }

}
else
{
    if(!session_is_registered("composeFolder") || $composeFolder==""){
        session_register("composeFolder");
    }

    if (substr($composeFolder,-3)== "tmp")
    {
        $mail_attachment_path = "$file_path/file/mail/$composeFolder";
        # Remove all attachments when clicked to this page (except for draft)
        if (is_dir($mail_attachment_path) && $composeFolder != "")
        {
            $li->lfs_remove($mail_attachment_path);
        }
    }
    # re-create new directory
    $composeFolder = session_id().".".time();
    $composeFolder = "u$UserID/$composeFolder"."tmp";
    #$path = "$personal_path/$composeFolder"."tmp";
}

$path = "$file_path/file/mail/$composeFolder";

#$path = "$file_path/file/mail/$composeFolder"."tmp";
$li->folder_new($path);


if ($cmsg == 1)
{
    $xcmsg = $i_campusmail_novaliduser;
}

# Check Identity type can receive mails
$laccess = new libaccess();
$mailtarget_teacherAllowed = $laccess->retrieveAccessCampusmailForType(1);
$mailtarget_studentAllowed = $laccess->retrieveAccessCampusmailForType(2);
$mailtarget_parentAllowed = $laccess->retrieveAccessCampusmailForType(3);

$navigation = $i_frontpage_separator.$i_CampusMail_New_iMail;
if ($cmsg==1)
{
    $xmsg = "$i_CampusMail_New_alert_norecipients";
}
$preset_notify  = true;
$preset_important = false;

# Handle reply/reply all/forward/Draft action
if ($CampusMailID != "" && $lcampusmail->UserID == $UserID)
{
    $lcampusquota = new libcampusquota($UserID);
    $totalQuota = $lcampusquota->returnQuota() * 1024;
    $usedQuota = $lcampusquota->returnUsedQuota();
    $leftQuota = $totalQuota - $usedQuota;
/*
               $this->UserID = $this->CampusMail[0][2];
               $this->SenderID = $this->CampusMail[0][4];
               $this->RecipientID = $this->CampusMail[0][5];
               $this->Attachment = $this->CampusMail[0][8];
               $this->IsAttachment = $this->CampusMail[0][9];
               $this->RecordType = $this->CampusMail[0][12];
               $this->RecordStatus = $this->CampusMail[0][13];
                   list($this->isDeleted, $this->mailType,$this->SenderEmail
                        ,$this->InternalCC,$this->InternalBCC, $this->ExternalTo
                        ,$this->ExternalCC,$this->ExternalBCC)
                        */
    # Grab Sender information
    if ($lcampusmail->mailType==2)         # External
    {
        $mail_senderemail = intranet_htmlspecialchars($lcampusmail->SenderEmail);
        $preset_message_whowrite = $mail_senderemail;
    }
    else
    {
        $namefield = getNameFieldWithClassNumberByLang();
        $sql = "SELECT UserID,$namefield FROM INTRANET_USER WHERE UserID = '".$lcampusmail->SenderID."'";
        $temp = $lcampusmail->returnArray($sql,2);
        list($mail_senderid,$mail_sendername) = $temp[0];
        $preset_message_whowrite = $mail_sendername;
    }
    if ($action != "D")
    {
        if ($lcampusmail->isHTMLMessage($lcampusmail->Message))
        {
            if (!$use_html_editor)
            {
                 $preset_message = $lcampusmail->removeHTMLtags($lcampusmail->Message);
            }
            else
            {
                 $preset_message = $lcampusmail->Message;
            }
        }
        else
        {
            if (!$use_html_editor)
            {
                 $preset_message = $lcampusmail->Message;
            }
            else
            {
                $preset_message = nl2br($lcampusmail->Message);
            }
        }
        if ($use_html_editor)
        {
            $preset_message = $preset_message_whowrite."$i_CampusMail_New_Wrote\n".
                              "<BLOCKQUOTE dir=ltr "."style=\"PADDING-RIGHT: 0px; PADDING-LEFT: 5px; MARGIN-LEFT: 5px; BORDER-LEFT: #000000 2px solid; MARGIN-RIGHT: 0px\"". " >".
            $preset_message. "</BLOCKQUOTE>
                                <br>
                                ";
        }
        else
        {
                $preset_message = ">".str_replace("\n","\n>",$preset_message);
                $preset_message = intranet_htmlspecialchars($preset_message);
                $preset_message = str_replace("&amp;","&",$preset_message);
                $preset_message = $preset_message_whowrite."$i_CampusMail_New_Wrote\n".$preset_message;
        }
    }
    else
    {
        $preset_message = $lcampusmail->Message;
    }
    # Handle Attachment (Duplicated for Forward message and embedded images)
    if (($action == "F" || $action=="R" || $action=="RA") && $lcampusmail->IsAttachment)
    {
         if ($action != "F")
         {
             $sql = "SELECT FileName FROM INTRANET_IMAIL_ATTACHMENT_PART
                            WHERE CampusMailID = '$CampusMailID' AND isEmbed = 1";
             $array_embed = $lcampusmail->returnVector($sql);
         }
        $source = "$file_path/file/mail/".$lcampusmail->Attachment;
        #$li->lfs_copy($source,$path);
        $lfiletable_source = new libfiletable("", $source, 0, 0, "");
        $preset_files = $lfiletable_source->files;
        $preset_quota_notenough = false;
        while (list($key, $value) = each($preset_files))
        {
               $filesize = $preset_files[$key][1]/1024;
               if ($action != "F")
               {
                   $t_filename = $preset_files[$key][0];
                   if (!in_array($t_filename,$array_embed))
                   {
                        continue;
                   }
               }

               if ($leftQuota > $filesize)
               {
                   $li->file_copy("$source/".$preset_files[$key][0],$path);
                   $leftQuota -= $filesize;
               }
               else
               {
                   $preset_quota_notenough = true;
               }
          }
    }
    if ($action == "R")  # Reply
    {
        $preset_subject = "Re: ".$lcampusmail->Subject;
        if ($lcampusmail->mailType==2)         # External
        {
            $preset_senderemail = $mail_senderemail;
        }
        else
        {
            $preset_senderid = $mail_senderid;
            $preset_sendername = $mail_sendername;
        }
        $preset_notify  = true;
        $preset_important = false;

    }
    else if ($action == "RA")
    {
        $preset_subject = "Re: ".$lcampusmail->Subject;
        if ($lcampusmail->mailType==2)         # External
        {
            $preset_senderemail = $mail_senderemail;
        }
        else
        {
            $preset_senderid = $mail_senderid;
            $preset_sendername = $mail_sendername;
            # Handle internal recipients
            if ($lcampusmail->RecipientID != "")
            {
                $mail_recipients = $lcampusmail->getRecipientNames($lcampusmail->RecipientID);
            }
            if ($lcampusmail->InternalCC != "")
            {
                $mail_cc_recipients = $lcampusmail->getRecipientNames($lcampusmail->InternalCC);
            }
        }
        if ($lcampusmail->ExternalTo != "")
        {
            $preset_to_address = str_replace(",",";",$lcampusmail->ExternalTo);
            if ($preset_senderemail != "")
            {
                $preset_senderemail .= "; $preset_to_address";
            }
            else
            {
                $preset_senderemail = "$preset_to_address";
            }
        }
        if ($lcampusmail->ExternalCC != "")
        {
            $preset_cc_address = str_replace(",",";",$lcampusmail->ExternalCC);
        }
        $preset_notify  = true;
        $preset_important = false;
    }
    else if ($action == "F")
    {
        $preset_subject = "Fw: ".$lcampusmail->Subject;
        $preset_notify  = true;
        $preset_important = false;
    }
    else if ($action == "D")
    {
         $preset_subject = $lcampusmail->Subject;
         $preset_senderemail = str_replace(",",";",$lcampusmail->ExternalTo);
         $preset_cc_address = str_replace(",",";",$lcampusmail->ExternalCC);
         $preset_bcc_address = str_replace(",",";",$lcampusmail->ExternalBCC);
         if ($lcampusmail->RecipientID != "")
         {
             $mail_recipients = $lcampusmail->getRecipientNames($lcampusmail->RecipientID);
         }
         if ($lcampusmail->InternalCC != "")
         {
             $mail_cc_recipients = $lcampusmail->getRecipientNames($lcampusmail->InternalCC);
         }
         if ($lcampusmail->InternalBCC != "")
         {
             $mail_bcc_recipients = $lcampusmail->getRecipientNames($lcampusmail->InternalBCC);
         }
         if ($lcampusmail->IsNotification)
             $preset_notify  = true;
         else $preset_notify = false;
         if ($lcampusmail->IsImportant)
             $preset_important = true;
         else $preset_important = false;
    }
    else
    {
    }
}
else if ($CampusMailReplyID!="")  # Get info from reply notification
{
     # Get info from DB
     $namefield = getNameFieldWithClassNumberByLang("c.");
     $sql = "SELECT a.UserID, a.CampusMailID, a.Message, b.UserID, $namefield, b.Subject
                    FROM INTRANET_CAMPUSMAIL_REPLY as a
                         LEFT OUTER JOIN INTRANET_CAMPUSMAIL as b ON a.CampusMailID = b.CampusMailID
                         LEFT OUTER JOIN INTRANET_USER as c ON a.UserID = c.UserID
                    WHERE a.CampusMailReplyID = $CampusMailReplyID";
     $temp = $lcampusmail->returnArray($sql,6);
     list($receiverID, $cMailID, $replyMessage, $senderID, $receiverName, $mail_subj) = $temp[0];
     if ($cMailID != "" && $receiverID != "" && $senderID == $UserID)
     {
         $mail_recipients[] = array("U".$receiverID, $receiverName);
         $preset_subject = "Re: ". $mail_subj;
         $preset_message = $replyMessage;
         if ($use_html_editor)
         {
             $preset_message = $receiverName."$i_CampusMail_New_Wrote\n".
                               "<BLOCKQUOTE dir=ltr ".$css_display_style["imail_included_message"]. " >".
                               $preset_message. "</BLOCKQUOTE>
                               <br>
                               ";
         }
         else
         {
             $preset_message = ">".str_replace("\n","\n>",$preset_message);
             $preset_message = intranet_htmlspecialchars($preset_message);
             $preset_message = str_replace("&amp;","&",$preset_message);
             $preset_message = $receiverName."$i_CampusMail_New_Wrote\n".$preset_message;
         }
     }

}
else
{
    if (isset($targetemail))
    {
        if ($targetname != "")
        {
            $preset_senderemail = "$targetname <$targetemail>";
        }
        else
        {
            $preset_senderemail = "$targetemail";
        }
    }
}
$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;

include("../../templates/homeheader.php");
?>


<script language="javascript">
function checkform(obj){
//     if(obj.elements["Recipient[]"].length==0){ alert("<?php echo $i_frontpage_campusmail_choose; ?>"); return false; }
     if(!check_text(obj.Subject, "<?php echo $i_alert_pleasefillin.$i_frontpage_campusmail_subject; ?>.")) return false;
     <? if ($use_html_editor) { ?>
     if (obj.Message.value == '')
     {
         alert('<?=$i_alert_pleasefillin.$i_frontpage_campusmail_message?>');
         return false;
     }
     <? } else { ?>
     if(!check_text(obj.Message, "<?php echo $i_alert_pleasefillin.$i_frontpage_campusmail_message; ?>.")) return false;
     <? } ?>
     // Check any recipients input
     if (
         <? if ($mailtarget_teacherAllowed && $imail_feature_allowed['withcheckbox']) { ?>
         obj.AllStaffTo.checked ||
         obj.AllStaffCC.checked ||
         obj.AllStaffBCC.checked ||
         <? }
         if ($mailtarget_studentAllowed && $imail_feature_allowed['withcheckbox']) {
         ?>
         obj.AllStudentTo.checked ||
         obj.AllStudentCC.checked ||
         obj.AllStudentBCC.checked ||
         <? }
         if ($mailtarget_parentAllowed && $imail_feature_allowed['withcheckbox']) {
         ?>
         obj.AllParentTo.checked ||
         obj.AllParentCC.checked ||
         obj.AllParentBCC.checked ||
         <? }
         ?>
         false
         )
     {
         // Either one of all identity box checked
     }
     else if (obj.elements["Recipient[]"].length!=0 || obj.elements["InternalCC[]"].length!=0 ||obj.elements["InternalBCC[]"].length!=0)
     {
          // has internal
     }
     <? if (!$noWebmail) { ?>
     else if (obj.ExternalTo.value != "" || obj.ExternalCC.value != "" || obj.ExternalBCC.value != "")
     {
     }
     <? } ?>
     else
     {
         alert('<?=$i_CampusMail_New_alert_norecipients?>');
         return false;
     }
     checkOptionAll(obj.elements["Recipient[]"]);
     checkOptionAll(obj.elements["InternalCC[]"]);
     checkOptionAll(obj.elements["InternalBCC[]"]);
     checkOptionAll(obj.elements["Attachment[]"]);
}
</script>
<form name="form1" method="post" action="compose_update2.php" onSubmit="return checkform(this);">
<table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="25">&nbsp;</td>
          <td><img src="<?=$image_path?>/frontpage/imail/title_imail_<?=$intranet_session_language?>.gif"></td>
        </tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/frontpage/imail/toolbar_bg.gif">
              <tr>
                <td width="70%">
                <?php include_once("menu.php"); ?>

                </td>
                <td width="30%" valign="bottom">
                  <table width=100% border=0 cellpadding=0 cellspacing=0>
                    <tr>
                      <td><!-- Quota Display --></td>
                      <td align=right><img src="<?=$image_path?>/frontpage/imail/graphic_mailbox2.gif" width="60" height="62"></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="794" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="210" valign="top"><img src="<?=$image_path?>/frontpage/imail/subhead_draft_<?=$intranet_session_language?>.gif" width="210" height="44"></td>
                <td width="584" align="center" valign="middle">
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>

      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="66" valign="top">
            <p><br>
              <br>
              <br>
              <br>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p><br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              </p>
          </td>
          <td width="728" align="left" valign="top">
              <table width="685" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><img src="/images/campusmail/papertop.gif" width="685" height="10"></td>
              </tr>
            </table>
                 <table width="685" border="0" cellspacing="0" cellpadding="2" background="/images/campusmail/paperbg2.gif" class="body">
                   <tr>
                     <td>&nbsp;<font color=#5284B5><b><?=$i_CampusMail_New_InternalRecipients?>:</b></font></td>
                   </tr>
                   <tr>
                     <td>
                       <table width=90% border=0 cellpadding=0 cellspacing=0 align=center>
                         <tr>
                           <td><I><?=$i_CampusMail_New_To?>:</I></td>
                           <td>
                             <table width=100% border=0 cellspacing=0 cellpadding=0>
                               <tr>
                                 <td>
                                   <select name=Recipient[] size=4 multiple><?
                                   if ($preset_senderid != "" && $preset_sendername)
                                       echo "<OPTION value=U$preset_senderid>$preset_sendername</OPTION>\n";
                                   for ($i=0; $i<sizeof($mail_recipients); $i++)
                                   {
                                        list($id, $name) = $mail_recipients[$i];
                                        echo "<OPTION value=$id>$name</OPTION>\n";
                                   }
                                    ?><option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select><br>
                                   <? if ($mailtarget_teacherAllowed && $imail_feature_allowed['withcheckbox']) { ?>
                                   <input type=checkbox name=AllStaffTo value="1"> <?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_teachstaff?>
                                   <? } ?>
                                   <? if ($mailtarget_studentAllowed && $imail_feature_allowed['withcheckbox']) { ?>
                                   <input type=checkbox name=AllStudentTo value="1"> <?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_student?>
                                   <? } ?>
                                   <? if ($mailtarget_parentAllowed && $imail_feature_allowed['withcheckbox']) { ?>
                                   <?=($intranet_session_language=="en"?"<br>":"")?><input type=checkbox name=AllParentTo value="1"> <?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_parent?>
                                   <? }
                                    ?>
                                 </td>
                                 <td>
                                   <table width=100% border=0 cellspacing=0 cellpadding=0 align=right>
                                     <tr>
                                       <td align=right>
                                         <a href=javascript:newWindow("choose/alias_in.php?fieldname=Recipient[]",9)><img src="<?=$image_path?>/btn_select_alias_<?=$intranet_session_language?>.gif" alt='<?=$i_CampusMail_New_SelectFromAlias?>' border=0></a>
                                       </td>
                                       <td align=right>
                                         <a href=javascript:newWindow("choose/index.php?fieldname=Recipient[]",9)><img src="<?=$image_chooserecipient?>" border=0></a>
                                       </td>
                                     </tr>
                                     <tr>
                                       <td align=right>
                                         <a href=javascript:checkOptionRemove(document.form1.elements["Recipient[]"])><img src="<?=$image_removeselected?>" border=0></a>
                                       </td>
                                       <td></td>
                                     </tr>
                                   </table>
                                 </td>
                               </tr>
                             </table>
                           </td>
                         </tr>
                         <tr>
                           <td><I><?=$i_CampusMail_New_CC?>:</I></td>
                           <td>
                             <table width=100% border=0 cellspacing=0 cellpadding=0>
                               <tr>
                                 <td>
                                   <select name=InternalCC[] size=4 multiple>
                                   <?
                                   for ($i=0; $i<sizeof($mail_cc_recipients); $i++)
                                   {
                                        list($id, $name) = $mail_cc_recipients[$i];
                                        echo "<OPTION value=$id>$name</OPTION>\n";
                                   }
                                   ?>
                                   <option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select><br>
                                   <? if ($mailtarget_teacherAllowed && $imail_feature_allowed['withcheckbox']) { ?>
                                   <input type=checkbox name=AllStaffCC value="1"> <?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_teachstaff?>
                                   <? } ?>
                                   <? if ($mailtarget_studentAllowed && $imail_feature_allowed['withcheckbox']) { ?>
                                   <input type=checkbox name=AllStudentCC value="1"> <?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_student?>
                                   <? } ?>
                                   <? if ($mailtarget_parentAllowed && $imail_feature_allowed['withcheckbox']) { ?>
                                   <?=($intranet_session_language=="en"?"<br>":"")?><input type=checkbox name=AllParentCC value="1"> <?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_parent?>
                                   <? } ?>
                                 </td>
                                 <td>
                                   <table width=100% border=0 cellspacing=0 cellpadding=0 align=right>
                                     <tr>
                                       <td align=right>
                                         <a href=javascript:newWindow("choose/alias_in.php?fieldname=InternalCC[]",9)><img src="<?=$image_path?>/btn_select_alias_<?=$intranet_session_language?>.gif" alt='<?=$i_CampusMail_New_SelectFromAlias?>' border=0></a>
                                       </td>
                                       <td align=right>
                                         <a href=javascript:newWindow("choose/index.php?fieldname=InternalCC[]",9)><img src="<?=$image_chooserecipient?>" border=0></a>
                                       </td>
                                     </tr>
                                     <tr>
                                       <td align=right>
                                         <a href=javascript:checkOptionRemove(document.form1.elements["InternalCC[]"])><img src="<?=$image_removeselected?>" border=0></a>
                                       </td>
                                       <td></td>
                                     </tr>
                                   </table>

                                 </td>
                               </tr>
                             </table>
                           </td>
                         </tr>
                         <tr>
                           <td><I><?=$i_CampusMail_New_BCC?>:</I></td>
                           <td>
                             <table width=100% border=0 cellspacing=0 cellpadding=0>
                               <tr>
                                 <td>
                                   <select name=InternalBCC[] size=4 multiple>
                                   <?
                                   for ($i=0; $i<sizeof($mail_bcc_recipients); $i++)
                                   {
                                        list($id, $name) = $mail_bcc_recipients[$i];
                                        echo "<OPTION value=$id>$name</OPTION>\n";
                                   }
                                   ?><option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select><br>
                                   <? if ($mailtarget_teacherAllowed && $imail_feature_allowed['withcheckbox']) { ?>
                                   <input type=checkbox name=AllStaffBCC value="1"> <?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_teachstaff?>
                                   <? } ?>
                                   <? if ($mailtarget_studentAllowed && $imail_feature_allowed['withcheckbox']) { ?>
                                   <input type=checkbox name=AllStudentBCC value="1"> <?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_student?>
                                   <? } ?>
                                   <? if ($mailtarget_parentAllowed && $imail_feature_allowed['withcheckbox']) { ?>
                                   <?=($intranet_session_language=="en"?"<br>":"")?><input type=checkbox name=AllParentBCC value="1"> <?=$i_status_all.($intranet_session_language=="en"?" ":"").$i_identity_parent?>
                                   <? } ?>
                                 </td>
                                 <td>
                                   <table width=100% border=0 cellspacing=0 cellpadding=0 align=right>
                                     <tr>
                                       <td align=right>
                                         <a href=javascript:newWindow("choose/alias_in.php?fieldname=InternalBCC[]",9)><img src="<?=$image_path?>/btn_select_alias_<?=$intranet_session_language?>.gif" alt='<?=$i_CampusMail_New_SelectFromAlias?>' border=0></a>
                                       </td>
                                       <td align=right>
                                         <a href=javascript:newWindow("choose/index.php?fieldname=InternalBCC[]",9)><img src="<?=$image_chooserecipient?>" border=0></a><br>
                                       </td>
                                     </tr>
                                     <tr>
                                       <td align=right>
                                         <a href=javascript:checkOptionRemove(document.form1.elements["InternalBCC[]"])><img src="<?=$image_removeselected?>" border=0></a>
                                       </td>
                                       <td></td>
                                     </tr>
                                   </table>

                                 </td>
                               </tr>
                             </table>
                           </td>
                         </tr>
                       </table>
                     </td>
                   </tr>
                 </table>
                   <? if (!$noWebmail) { ?>
                 <table width="685" border="0" cellspacing="0" cellpadding="2" background="/images/campusmail/paperbg2.gif" class="body">
                   <tr>
                     <td>&nbsp;<font color=#5284B5><b><?=$i_CampusMail_New_ExternalRecipients?>:</b><b><font color=red>(<?=$i_CampusMail_New_EmailAddressSeparationNote?>)</font></b></font></td>
                   </tr>
                   <tr>
                     <td>
                       <table width=90% border=0 cellpadding=0 cellspacing=0 align=center>
                         <tr>
                           <td><I><?=$i_CampusMail_New_To?>:</I></td>
                           <td>
                             <table width=100% border=0 cellspacing=0 cellpadding=0>
                               <tr>
                                 <td>
                                   <textarea name=ExternalTo rows=2 cols=50><?=$preset_senderemail?></textarea>
                                 </td>
                                 <td align=center>
                                   <a href=javascript:newWindow("choose_ex/index.php?fieldname=ExternalTo",9)><img src="<?=$image_chooserecipient?>" border=0></a>
                                   <a href=javascript:newWindow("choose_ex/alias_ex.php?fieldname=ExternalTo",9)><img src="<?=$image_path?>/btn_select_alias_<?=$intranet_session_language?>.gif" alt='<?=$i_CampusMail_New_SelectFromAlias?>' border=0></a>
                                 </td>
                               </tr>
                             </table>
                           </td>
                         </tr>
                         <tr>
                           <td><I><?=$i_CampusMail_New_CC?>:</I></td>
                           <td>
                             <table width=100% border=0 cellspacing=0 cellpadding=0>
                               <tr>
                                 <td>
                                   <textarea name=ExternalCC rows=2 cols=50><?=$preset_cc_address?></textarea>
                                 </td>
                                 <td align=center>
                                   <a href=javascript:newWindow("choose_ex/index.php?fieldname=ExternalCC",9)><img src="<?=$image_chooserecipient?>" border=0></a>
                                   <a href=javascript:newWindow("choose_ex/alias_ex.php?fieldname=ExternalCC",9)><img src="<?=$image_path?>/btn_select_alias_<?=$intranet_session_language?>.gif" alt='<?=$i_CampusMail_New_SelectFromAlias?>' border=0></a>
                                 </td>
                               </tr>
                             </table>
                           </td>
                         </tr>
                         <tr>
                           <td><I><?=$i_CampusMail_New_BCC?>:</I></td>
                           <td>
                             <table width=100% border=0 cellspacing=0 cellpadding=0>
                               <tr>
                                 <td>
                                   <textarea name=ExternalBCC rows=2 cols=50><?=$preset_bcc_address?></textarea>
                                 </td>
                                 <td align=center>
                                   <a href=javascript:newWindow("choose_ex/index.php?fieldname=ExternalBCC",9)><img src="<?=$image_chooserecipient?>" border=0></a>
                                   <a href=javascript:newWindow("choose_ex/alias_ex.php?fieldname=ExternalBCC",9)><img src="<?=$image_path?>/btn_select_alias_<?=$intranet_session_language?>.gif" alt='<?=$i_CampusMail_New_SelectFromAlias?>' border=0></a>
                                 </td>
                               </tr>
                             </table>
                           </td>
                         </tr>
                       </table>
                     </td>
                   </tr>
                 </table>
                   <? } ?>
                 <table width="685" border="0" cellspacing="0" cellpadding="2" background="/images/campusmail/paperbg2.gif" class="body">
                   <tr>
                     <td>
                       <table width=95% bgcolor=#FFE6BD border=0 cellspacing=0 cellpadding=0 align=center>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120"><?=$i_frontpage_campusmail_subject?> :</td>
                  <td align="left" valign="top">
                  <input class=text type=text name=Subject size=55 maxlength=255 value="<?=$preset_subject?>"></td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120"><?=$i_frontpage_campusmail_message?>:</td>
                  <td align="left" valign="top">
                      <? if ($use_html_editor) {
                   # Components size
                   $msg_box_width = "100%";
                   $msg_box_height = 320;
                       $obj_name = "Message";
                       $editor_width = $msg_box_width;
                       $editor_height = $msg_box_height;
                       $init_html_content = $preset_message;
                       include("../../includes/html_editor_embed.php");
                    } else { ?>
                      <textarea name=Message cols=80 rows=10><?=$preset_message?></textarea>
                   <? } ?>
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120"><?=$i_frontpage_campusmail_attachment?> :</td>
                  <td align="left" valign="top">
                    <table width=100% border=0 cellpadding=0 cellspacing=0>
                      <tr>
                        <td>
                          <table width=100% border=0 cellpadding=0 cellspacing=0>
                            <tr>
                              <td>
                                <select name=Attachment[] size=4 multiple><option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select>
                                <br><?
                                if ($preset_quota_notenough)
                                  echo "<font color=red>$i_CampusMail_New_QuotaNotEnough</font>";
                                ?>
                              </td>
                              <td align=left>
                                <a href=javascript:newWindow("attach.php?folder=<?php echo urlencode("$composeFolder"); ?>",2)><img border=0 src="<?=$image_attachfiles?>"> <?=$i_frontpage_campusmail_icon_attachment?></a><br>
                                <a href=javascript:checkOptionRemove(document.form1.elements["Attachment[]"])><img border=0 src="<?=$image_removeselected?>"></a>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120"><?="$i_frontpage_campusmail_icon_important $i_frontpage_campusmail_important"?> :</td>
                  <td align="left" valign="top">
                    <input type=checkbox name=IsImportant value=1 <?=($preset_important? "CHECKED":"")?>>
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120"><?="$i_frontpage_campusmail_icon_notification $i_frontpage_campusmail_notification"?> :</td>
                  <td align="left" valign="top">
                    <input type=checkbox name=IsNotification value=1 <?=($preset_notify? "CHECKED":"")?>> <I><?=$i_CampusMail_New_NotificationInternalOnly?></I>
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td  align="left" valign="top">&nbsp;</td>
                </tr>
                       </table>
                     </td>
                   </tr>
                 </table>
                 <table width="685" border="0" cellspacing="0" cellpadding="2" background="/images/campusmail/paperbg2.gif" class="body">
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" valign="top" width="120">&nbsp;</td>
                  <td align="left" valign="top">
<input type=image src=<?=$image_send?> onClick="checkOption(this.form.elements['Attachment[]']); checkOption(this.form.elements['Recipient[]']);checkOption(this.form.elements['InternalCC[]']);checkOption(this.form.elements['InternalBCC[]']);this.form.SubmitType.value=0;">
<input type=image src=<?=$image_save?> onClick="checkOption(this.form.elements['Attachment[]']); checkOption(this.form.elements['Recipient[]']);checkOption(this.form.elements['InternalCC[]']);checkOption(this.form.elements['InternalBCC[]']);this.form.SubmitType.value=1;">
<input type=image src=<?=$image_reset?> onClick="this.form.reset(); return false;">
                </tr>

              </table>
              <table width="685" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><img src="/images/campusmail/paperbottom.gif" width="685" height="70"></td>
                </tr>
              </table>
          </td>
        </tr>
      </table>
<input type=hidden name=SubmitType value=0>
<input type=hidden name=action value="<?=$action?>">
<input type=hidden name=OriginalCampusMailID value="<?=$CampusMailID?>">
</form>

<script language="JavaScript1.2">
obj = document.form1.elements["Attachment[]"];
checkOptionClear(obj);
<?php while (list($key, $value) = each($files)) echo "checkOptionAdd(obj, \"".$files[$key][0]." (".ceil($files[$key][1]/1000)."Kb".")\", \"".$files[$key][0]."\");\n";  ?>
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
/*
obj = document.form1.elements["Recipient[]"];
checkOptionClear(obj);
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
obj = document.form1.elements["InternalCC[]"];
checkOptionClear(obj);
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
obj = document.form1.elements["InternalBCC[]"];
checkOptionClear(obj);
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
*/
</script>

<?php
include("../../templates/homefooter.php");
intranet_closedb();
?>