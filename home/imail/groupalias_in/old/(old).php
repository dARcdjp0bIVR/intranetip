<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libgroupcategory.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
include_once("../../../templates/fileheader.php");
$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
$content = explode("\n",$file_content);
# Get row 1,2,4 only. Compatible with previous version
if ($content[0][0]==1) $permitted[] = 1;
if ($content[1][0]==1) $permitted[] = 2;
if ($content[3][0]==1) $permitted[] = 3;

/*
$count = 0;
for ($i=0; $i<sizeof ($content); $i++)
{
     $settings = split(":",$content[$i]);
     $access_right[$i] = $settings[0];
     if ($settings[0]==1)
     {
         $permitted[$count] = $i+1;
         $count++;
     }
}
*/
if ($file_content == "")
{
    $permitted = array(1,2,3);
}

$li = new libgrouping();

if($CatID < 0){
     unset($ChooseGroupID);
     $ChooseGroupID[0] = 0-$CatID;
}
$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();

$x1  = ($CatID!=0 && $CatID > 0) ? "<select name=CatID onChange=checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()>\n" : "<select name=CatID onChange=this.form.submit()>\n";
$x1 .= "<option value=0></option>\n";
for ($i=0; $i<sizeof($cats); $i++)
{
     list($id,$name) = $cats[$i];
     if ($id!=0)
     {
         $x1 .= "<option value=$id ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
     }
}
/*
$x1 .= "<option value=3 ".(($CatID==3)?"SELECTED":"").">".$i_GroupRole[3]."</option>\n";
$x1 .= "<option value=1 ".(($CatID==1)?"SELECTED":"").">".$i_GroupRole[1]."</option>\n";
$x1 .= "<option value=2 ".(($CatID==2)?"SELECTED":"").">".$i_GroupRole[2]."</option>\n";
$x1 .= "<option value=4 ".(($CatID==4)?"SELECTED":"").">".$i_GroupRole[4]."</option>\n";
$x1 .= "<option value=5 ".(($CatID==5)?"SELECTED":"").">".$i_GroupRole[5]."</option>\n";
$x1 .= "<option value=6 ".(($CatID==6)?"SELECTED":"").">".$i_GroupRole[6]."</option>\n";
*/
$x1 .= "<option value=0>";
for($i = 0; $i < 20; $i++)
$x1 .= "_";
$x1 .= "</option>\n";
$x1 .= "<option value=0></option>\n";

if (in_array(1,$permitted))
    $x1 .= "<option value=-1 ".(($CatID==-1)?"SELECTED":"").">$i_identity_teachstaff</option>\n";
if (in_array(2,$permitted))
    $x1 .= "<option value=-2 ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";
if (in_array(3,$permitted))
    $x1 .= "<option value=-3 ".(($CatID==-3)?"SELECTED":"").">$i_identity_parent</option>\n";
/*
$row = $li->returnCategoryGroups(0);
for($i=0; $i<sizeof($row); $i++){
     $IdentityCatID = $row[$i][0] + 10;
     $IdentityCatName = $row[$i][1];
     if ($access_right[$row[$i][0]-1]==1 || $file_content == "")
         $x1 .= "<option value=$IdentityCatID ".(($IdentityCatID==$CatID)?"SELECTED":"").">$IdentityCatName</option>\n";
}
*/
$x1 .= "<option value=0></option>\n";
$x1 .= "</select>";


if($CatID!=0 && $CatID > 0) {
     $row = $li->returnCategoryGroups($CatID);
     $x2  = "<select name=ChooseGroupID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++){
          $GroupCatID = $row[$i][0];
          $GroupCatName = $row[$i][1];
          $x2 .= "<option value=$GroupCatID";
          for($j=0; $j<sizeof($ChooseGroupID); $j++){
          $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
          }
          $x2 .= ">$GroupCatName</option>\n";
     }
     $x2 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x2 .= "&nbsp;";
     $x2 .= "</option>\n";
     $x2 .= "</select>\n";
}

if($CatID < 0){
   # Return users with identity chosen
   $selectedUserType = 0-$CatID;
   $row = $li->returnUserForType($selectedUserType);

     $x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}
else if(isset($ChooseGroupID)) {
     $row = $li->returnGroupUsersInIdentity($ChooseGroupID, $permitted);
     $x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}

$step1 = getStepIcons(3,1,$i_SelectMemberSteps);
$step2 = getStepIcons(3,2,$i_SelectMemberSteps);
$step3 = getStepIcons(3,3,$i_SelectMemberSteps);
?>

<script language="javascript">
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];
     x = (obj.name == "ChooseGroupID[]") ? "G" : "U";
     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;
     while(i!=-1){
          par.checkOptionAdd(parObj, obj.options[i].text, x + obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
</script>

<form name=form1 action="index.php" method=post>
<table border=0 cellpadding=0 cellspacing=0>
<tr><td><?php echo $i_frontpage_campusmail_namelist_t; ?></td></tr>
<tr>
<td class=campusmail_namelist_m align=center>
<br>
<table width=400 border=0 cellpadding=0 cellspacing=1><tr><td bgcolor=#FFFFFF>
<table width=100% border=0 cellpadding=5 cellspacing=1>
<tr>
<td width=100% bgcolor=#F2F8CA><?=$i_SelectMemberInstruction?><br>
<hr width=90%>
<?=$step1?><br>
<?php echo $i_frontpage_campusmail_select_category; ?><br><img src=../../../images/space.gif border=0 width=1 height=3><br>
<?php echo $x1; ?>
</td>
</tr>
<?php if($CatID!=0 && $CatID > 0) { ?>
<tr>
<td bgcolor=#EAF3B0>
<?=$step2?><br>
<?php echo $i_frontpage_campusmail_select_group; ?><br><img src=../../../images/space.gif border=0 width=1 height=3><br>
<table width=100% cellpadding=0 cellspacing=0>
<tr><td>
<?php echo $x2; ?></td>
<td style="vertical-align:bottom">
<input type=image src=<?=$image_add?> alt='<?=$button_add?>' onClick=checkOption(this.form.elements["ChooseGroupID[]"]);AddOptions(this.form.elements["ChooseGroupID[]"])><input type=image src=<?=$image_expand?> alt="<?php echo $i_frontpage_campusmail_expand; ?>" onClick=checkOption(this.form.elements["ChooseGroupID[]"]);>
<input type=image src=<?=$image_selectall?> onClick="SelectAll(this.form.elements['ChooseGroupID[]']); return false;" alt="<?=$button_select_all?>"></td></tr>
</table>
</td>
</tr>
<?php } ?>
<?php if(isset($ChooseGroupID)) { ?>
<tr>
<td bgcolor=#F2F8CA>
<?=$step3?><br>
<?php echo $i_frontpage_campusmail_select_user; ?><br><img src=../../../images/space.gif border=0 width=1 height=3><br>
<?php echo $x3; ?>
<input type=image src=<?=$image_add?> alt='<?=$button_add?>' onClick=checkOption(this.form.elements["ChooseUserID[]"]);AddOptions(this.form.elements["ChooseUserID[]"])><input type=image src=<?=$image_selectall?> onClick="SelectAll(this.form.elements['ChooseUserID[]']); return false;" alt="<?=$button_select_all?>">
</td>
</tr>
<?php } ?>
</table>
</td></tr></table>
<br>
<a href=javascript:self.close()><img src=<?=$image_closewindow?> alt="<?php echo $button_close; ?>" border=0></a>
</td>
</tr>
<tr><td><?php echo $i_frontpage_campusmail_namelist_b; ?></td></tr>
</table>
<input type=hidden name=fieldname value="<?php echo $fieldname; ?>">
</form>

<?php
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>