<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if ($ChooseUserType == "")
{
    header("Location: index.php?AliasID=$AliasID");
    exit();
}

$li = new libdb();
# get blocked groups and blocked usertypes
$sql ="SELECT a.RecordType,a.Teaching,c.ClassLevelID FROM INTRANET_USER AS a 
	LEFT OUTER JOIN INTRANET_CLASS AS b ON(a.ClassName=b.ClassName) 
	LEFT OUTER JOIN INTRANET_CLASSLEVEL AS c ON (b.ClassLevelID=c.ClassLevelID) 
	WHERE a.UserID='$UserID'";
$temp = $li->returnArray($sql,3);
list($usertype,$teaching,$class_level_id) = $temp[0];

$sql_blocked = "SELECT BlockedGroupCat,BlockedUserType FROM INTRANET_IMAIL_RECIPIENT_RESTRICTION WHERE TargetType='$usertype' AND Restricted!=1";
if($usertype==1)
	$sql_blocked.=" AND Teaching='$teaching'";
else if($usertype==2)
	$sql_blocked.=" AND ClassLevel='$class_level_id'";
$result_blocked = $li->returnArray($sql_blocked,2);

list($blocked_groups,$blocked_usertypes) = $result_blocked[0];

$blocked_groups = explode(",",$blocked_groups);
$blocked_usertypes= explode(",",$blocked_usertypes);
# end get blocked groups / usertypes

$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
$content = explode("\n",$file_content);
# Get row 1,2,4 only. Compatible with previous version
if ($content[0][0]==1) $permitted[] = 1;
if ($content[1][0]==1) $permitted[] = 2;
if ($content[3][0]==1) $permitted[] = 3;

# non-teaching staff
if ($content[0][0]==1) $permitted[] = 5;

if ($file_content == "")
{
    $permitted = array(1,2,3,5);
}
if (!in_array($ChooseUserType,$permitted)|| in_array($ChooseUserType,$blocked_usertypes))
{
    header("Location: index.php?AliasID=$AliasID");
    exit();
}

$luser = new libuser();

if ($ChooseUserType==1 || $ChooseUserType==3||$ChooseUserType==5)
{
    if($ChooseUserType==1)
        $users = $luser->returnUsersByIdentity($ChooseUserType,1);
    if($ChooseUserType==5)
    	$users = $luser->returnUsersByIdentity(1);
    if($ChooseUserType==3)
    	$users = $luser->returnUsersByIdentity($ChooseUserType);

}
else         # Students
{
    if ($Class == "")
    {
        header("Location: class.php?AliasID=$AliasID");
        exit();
    }
    $lclass = new libclass();
    $users = $lclass->getStudentNameListByClassName($Class);

}

# get users already added to INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY
	$sqlUsers = "SELECT TargetID FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY WHERE AliasID='$AliasID' AND RecordType='U'";
	$user_in_table = $luser->returnVector($sqlUsers);
# end get users

# filter
for($i=0;$i<sizeof($users);$i++){
	if(!in_array($users[$i][0],$user_in_table))
		$targetUsers[]=$users[$i];
}

$x2  = "<select name=\"TargetID[]\" size=\"8\" multiple>\n";
for($i=0; $i<sizeof($targetUsers); $i++)
{
          $id 		= $targetUsers[$i][0];
          $name 	= $targetUsers[$i][1];
          $class_number = $targetUsers[$i][2];
          
          $x2 .= "<option value=\"$id\"";
          for($j=0; $j<sizeof($user_in_table); $j++)
          {
          	$x2 .= ($id == $user_in_table[$j]) ? " SELECTED" : "";
          }
          $x2 .= ">$name</option>\n";
     }
$x2 .= "</select>\n";

$display_usertype=$ChooseUserType==5?$i_nonteachingStaff:$i_identity_array[$ChooseUserType];

$MODULE_OBJ['title'] = $button_select.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_ListAlias;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--
function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
//-->
</script>

<form name="form1" action="list_update.php" method="POST" >

<table id="html_body_frame" width="96%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<br />
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tabletext">
        <tr><td ><b><?="$i_CampusMail_New_AddressBook_ByUser &gt; ".$display_usertype.($Class==""?"":" &gt; $Class")?></b></td><td></td></tr>
        <tr><td height="5" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10" /></td></tr>
        <tr><td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1" /></td></tr>
        <tr><td height="5" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10" /></td></tr>
        </table>
        
        <table width="100%" border="0" cellpadding="3" cellspacing="0" class="tabletext">
        
        <?  	if(sizeof($targetUsers)<=0)
		{
			echo "<tr><td align=center colspan=2>$i_no_record_exists_msg</td></tr>\n";
		}        
		else
		{ 
			?>	
	        <tr >
			<td valign="top" nowrap="nowrap" width="20%" >
			<span class="tabletext"><?=$i_CampusMail_New_AddressBook_ByUser?>:</span>
			</td>
			<td >					
			<table border="0" cellpadding="0" cellspacing="0" align="left">
                        
                        
			<tr >
				<td >				
				<?=$x2?>
				</td>
				<td valign="bottom" >
				<table cellpadding="0" cellspacing="6" >
				<tr >
					<td ><?= $linterface->GET_BTN($button_add, "submit") ?></td>										
				</tr >	

				<?php 
				if(!$sys_custom['Mail_NoSelectAllButton']) 
				{ 
				?>
				<tr >
					<td >																		
					<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['TargetID[]']); return false;","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
					</td >
				</tr >	
				<?php 
				} 
				?>				
				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>
	<? } ?>                	
        </table>

        </td>
</tr>

<tr>
		<td>
		<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr>
			<td align="center">
                        <?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php?AliasID=". $AliasID ."'") ?>
			<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
</table>        

<input type="hidden" name="AliasID" value="<?=$AliasID?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>