<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");

intranet_auth();
intranet_opendb();

$li = new libdb();

# get blocked groups and blocked usertypes
$sql ="SELECT a.RecordType,a.Teaching,c.ClassLevelID FROM INTRANET_USER AS a 
	LEFT OUTER JOIN INTRANET_CLASS AS b ON(a.ClassName=b.ClassName) 
	LEFT OUTER JOIN INTRANET_CLASSLEVEL AS c ON (b.ClassLevelID=c.ClassLevelID) 
	WHERE a.UserID='$UserID'";
	
$temp = $li->returnArray($sql,3);
list($usertype,$teaching,$class_level_id) = $temp[0];

$sql_blocked = "SELECT BlockedGroupCat,BlockedUserType FROM INTRANET_IMAIL_RECIPIENT_RESTRICTION WHERE TargetType='$usertype' AND Restricted!=1";
if($usertype==1)
	$sql_blocked.=" AND Teaching='$teaching'";
else if($usertype==2)
	$sql_blocked.=" AND ClassLevel='$class_level_id'";
$result_blocked = $li->returnArray($sql_blocked,2);

list($blocked_groups,$blocked_usertypes) = $result_blocked[0];

$blocked_groups = explode(",",$blocked_groups);
$blocked_usertypes= explode(",",$blocked_usertypes);
# end get blocked groups / usertypes

# get GROUPS already added to INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY
	$sqlGroups = "SELECT TargetID FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY WHERE AliasID='$AliasID' AND RecordType='G'";
	$group_in_table = $li->returnVector($sqlGroups);
# end get GROUPS

$lgroupcat = new libgroupcategory();
$temp = $lgroupcat->returnAllCat();
$cats = array();
for($i=0;$i<sizeof($temp);$i++){
	
	if($temp[$i][0]!=0 &&!in_array($temp[$i][0],$blocked_groups)){
		$cats[]= $temp[$i];
	}
}

$select_cat = getSelectByArray($cats,"name=\"GroupCategory\" onChange=\"changeGroup(this.form)\"","$GroupCategory",0,1);


$libgroup = new libgroup();
$temp = $libgroup->returnGroupsByCategory($GroupCategory);
for($i=0;$i<sizeof($temp);$i++)
	if(!in_array($temp[$i][0],$group_in_table))
		$groups[] = $temp[$i];

### Group List ###
$x2  = "<select name=\"TargetID[]\" size=\"8\" multiple>\n";
for($i=0; $i<sizeof($groups); $i++)
{
          $id 		= $groups[$i][0];
          $name 	= $groups[$i][1];

          $x2 .= "<option value=\"$id\">$name</option>\n";
}
$x2 .= "</select>\n";

### Group List - End - ###
     
$MODULE_OBJ['title'] = $button_select.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_ListAlias;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();                

?>
<script language="javascript">
function changeGroup(formObj){
	formObj.action='';
	formObj.submit();
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
//-->
</script>

<form name="form1" action="group_update.php" method="post">

<table id="html_body_frame" width="96%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<br />        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabletext">
        <tr>
			<td valign="top" nowrap="nowrap" width="20%" >
			<span class="tabletext"><?=$i_GroupCategorySettings?>:</span>
			</td>
			<td ><?=$select_cat?>
			</td>
		</tr>
		<tr>
			<td colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="5" /></td>
		</tr>  
		<tr>
			<td colspan="2" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="5" /></td>
		</tr>  
        
        <? if(sizeof($groups)<=0) { 
        	echo "<tr><td align=\"center\" colspan=\"2\">$i_no_record_exists_msg</td></tr>\n";
        } else {?>
        	<tr >
			<td valign="top" nowrap="nowrap" width="20%" >
			<span class="tabletext"><?=$i_CampusMail_New_AddressBook_ByGroup?>:</span>
			</td>
			<td >					
			<table border="0" cellpadding="0" cellspacing="0" align="left">
			<tr >
				<td >				
				<?=$x2?>
				</td>
				<td valign="bottom" >
				<table cellpadding="0" cellspacing="6" >
				<tr >
					<td ><?= $linterface->GET_BTN($button_add, "submit") ?></td>										
				</tr >	

				<?php 
				if(!$sys_custom['Mail_NoSelectAllButton']) 
				{ 
				?>
				<tr >
					<td >																		
					<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['TargetID[]']); return false;","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
					</td >
				</tr >	
				<?php 
				} 
				?>				
				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>
        <? } ?>
        </table>

        </td>
</tr>

<tr>
		<td>
		<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr>
			<td align="center">
                        <?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php?AliasID=". $AliasID ."'") ?>
			<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
</table>        

<input type="hidden" name="AliasID" value="<?=$AliasID?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
