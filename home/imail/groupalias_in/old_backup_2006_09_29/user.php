<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
#include_once("../../../includes/libgrouping.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
$content = explode("\n",$file_content);
# Get row 1,2,4 only. Compatible with previous version
if ($content[0][0]==1) $permitted[] = 1;
if ($content[1][0]==1) $permitted[] = 2;
if ($content[3][0]==1) $permitted[] = 3;

if ($file_content == "")
{
    $permitted = array(1,2,3);
}

if (!in_array($UserType,$permitted))
{
    header("Location: index.php");
    exit();
}
if ($UserType == 1)   # Staff: Just show the list directly
{
    header("Location: list.php?UserType=$UserType&AliasID=$AliasID");
    exit();
}
if ($UserType == 2)
{
    header("Location: class.php?UserType=$UserType&AliasID=$AliasID");
    exit();
}
else if ($UserType == 3)
{
    header("Location: list.php?UserType=$UserType&AliasID=$AliasID");
    exit();

#include_once("../../../templates/fileheader.php");
#include_once("../../../templates/filefooter.php");

}
intranet_closedb();
?>