<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if ($AliasID == "")
{
    header("Location: ../../../school/close.php");
    exit();
}

$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
$content = explode("\n",$file_content);
# Get row 1,2,4 only. Compatible with previous version
if ($content[0][0]==1) $permitted[] = 1;
if ($content[1][0]==1) $permitted[] = 2;
if ($content[3][0]==1) $permitted[] = 3;

if ($file_content == "")
{
    $permitted = array(1,2,3);
}


$li = new libuser($UserID);

# get toStaff, toStudent, toParent options
$sql ="SELECT a.RecordType,a.Teaching,c.ClassLevelID FROM INTRANET_USER AS a 
	LEFT OUTER JOIN INTRANET_CLASS AS b ON(a.ClassName=b.ClassName) 
	LEFT OUTER JOIN INTRANET_CLASSLEVEL AS c ON (b.ClassLevelID=c.ClassLevelID) 
	WHERE a.UserID='$UserID'";
$temp = $li->returnArray($sql,3);
list($usertype,$teaching,$class_level_id) = $temp[0];

$sql_to_options = "SELECT ToStaffOption,ToStudentOption,ToParentOption FROM INTRANET_IMAIL_RECIPIENT_RESTRICTION WHERE TargetType='$usertype' AND Restricted=1";
if($usertype==1)
	$sql_to_options.=" AND Teaching='$teaching'";
else if($usertype==2)
	$sql_to_options.=" AND ClassLevel='$class_level_id'";
$result_to_options = $li->returnArray($sql_to_options,3);
list($toStaff,$toStudent,$toParent) = $result_to_options[0];
# end get toStaff, toStudent, toParent options

#####
$x1="";

# Create Cat list
if (in_array(1,$permitted) && $toStaff!=-1){
    $x1 .= "<option value=1 >$i_identity_teachstaff</option>\n";
}
if (in_array(2,$permitted) && $toStudent!=-1){
    $x1 .= "<option value=2 >$i_identity_student</option>\n";
}
if (in_array(3,$permitted) && $toParent!=-1){
    $x1 .= "<option value=3 >$i_identity_parent</option>\n";
}
if($x1!=""){
	$x1 = "<select name=UserType>\n".$x1."</select>\n";
}

######


$MODULE_OBJ['title'] = $button_select.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_ListAlias;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>

<form name="form1" action="user.php" method="get">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<br />
     	   
        <table width="80%" border="0" cellspacing="1" cellpadding="2" class="tabletext">
        <?php if($x1!=""){?>
        <tr><td colspan="2" align="center"><?=$i_CampusMail_New_AddressBook_ByUser?>: <?=$x1?></td></tr>
        <?php } ?>
        <?php if($x1==""){?>
        <tr><td align="center" colspan="2"><?=$i_campusmail_no_users_available?></td></tr>
        <tr><td align="center" colspan="2"><a href="javascript:window.close()"><img src="<?=$image_path?>/btn_closewindow_<?=$intranet_session_language?>.gif" border="0" /></a></td></tr>
        
        <?php }else{ ?>
        <tr><td align="center" colspan="2"><?= $linterface->GET_BTN($button_next, "submit") ?></td></tr>
        <?php } ?>
        
        
        </table>

        </td>
</tr>

<tr>
		<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
</table>        

<input type="hidden" name="AliasID" value="<?=$AliasID?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>