<?php
// editing 
/*
 * 2013-04-16 Carlos: 
 * 	- added flag $sys_custom['iMail']['ArchiveMailNumber'] to control how many mails to trigger archive (default 3000)
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$Action = trim($_REQUEST['Action']);

switch($Action)
{
	case "GetMailCount":
		$FolderID = $_REQUEST['FolderID'];
		$li = new libdb();
		$sql = "SELECT COUNT(CampusMailID) FROM INTRANET_CAMPUSMAIL 
				WHERE UserID = '".$_SESSION['UserID']."' AND UserFolderID = '$FolderID' AND Deleted != 1 ";
		$mail_count = $li->returnVector($sql);
		$_SESSION['intranet_iMail_archived'] = 1; // only count one time to avoid performance issue as innodb select count from large dataset is slow
		echo $mail_count[0];
	break;
	case "GetArchiveMailMessageLayer":
		$lwebmail = new libwebmail();
		$id = 'sub_layer_imail_archive_mail_v30';
		$archive_number = isset($sys_custom['iMail']['ArchiveMailNumber'])? $sys_custom['iMail']['ArchiveMailNumber'] : 3000;
		$message = str_replace("<!--NUMBER-->",$archive_number, $Lang['iMail']['FieldTitle']['ArchiveInboxMsg']);
		//$message = str_replace("{#LINK_OPEN_TAG#}","<a href=\"archive_mail.php\">",$message);
		//$message = str_replace("{#LINK_CLOSE_TAG#}","</a>",$message);
		echo $lwebmail->Get_Archive_Mail_Message_Layer($id,$message,'500px','80px');
	break;
	case "SetRemindLater":
		$_SESSION['intranet_iMail_archived'] = 1;
		echo $_SESSION['intranet_iMail_archived'];
	break;
}

intranet_closedb();
?>