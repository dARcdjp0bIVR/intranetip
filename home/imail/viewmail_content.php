<?php
// using : 
/*
 * 2014-10-15 (Carlos)[ip2.5.5.10.1]: added css for pritn email button
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CampusMailID = IntegerSafe($CampusMailID);

$linterface = new interface_html("imail_default.html");
$li = new libcampusmail2007($CampusMailID);

$header_default_style = "/templates/style_mail.css";

$outType = ($li->SenderID == $UserID && $li->CampusMailFromID == "");
$mailFolderID = $li->UserFolderID;
if($mailFolderID>0){
	$outType = false;
}

if ($viewtype == "search")
{

}
else
{
    $nextID = $li->returniMailNextID($mailFolderID);
    $prevID = $li->returniMailPrevID($mailFolderID);
}

/*
# Convert Message
if ($li->isHTML == 1)
{
    $html_flag = true;
}
else if ($li->isHTML == -1)
{
     $html_flag = false;
}
else
{
    $html_flag = $li->isHTMLMessage($li->Message);
}

if ($html_flag)
{
    $display_message = $li->Message;
}
else
{
    #$text = $li->removeHTMLtags($li->Message);
    $text = $li->Message;
    $text = nl2br($text); #nl2br(intranet_htmlspecialchars($text));
    $text = $li->convertURLS2($text);
    $text = $li->convertEmailToiMailCompose($text);
    $display_message = $text;
}
*/

/*
if($li->MessageEncoding==""){
	if(isset($intranet_default_lang_set) && is_array($intranet_default_lang_set)){
		  if (in_array("b5",$intranet_default_lang_set))
		  {
		      $thisEncoding = "big5";
		  }
		  else
		  {
		      $thisEncoding = "GB2312";
		  }
	}else{
		$thisEncoding="big5";
	}	
}else{
	$thisEncoding = $li->MessageEncoding;
}

if(strtolower($thisEncoding)=="us-ascii"){
	if(isset($intranet_default_lang_set) && is_array($intranet_default_lang_set)){	
	  if (in_array("b5",$intranet_default_lang_set))
	  {
	      $thisEncoding = "big5";
	  }
	  else
	  {
	      $thisEncoding = "GB2312";
	  }	
	}
}
*/
?>
<head>
<META http-equiv=Content-Type content='text/html; charset=utf-8'>
<link rel="stylesheet" href="/templates/style_mail.css" />

<script language="javascript" >
var OriginalHeight = "";
var OriginalWidth = "";

function format_filename(filename){	
	return filename.replace(/&amp;/g,'&');
}
function removeMail()
{
         if(parent!=null)
         	parent.removeMail();
}
function viewMessageSource()
{
	if(parent!=null)
		parent.viewMessageSource();
}
function resize_iframe()
{
	scroll_size = 10;
	iframeObj = this.parent.document.getElementById("imail_frame");
	if(iframeObj==null) return;
	ch= this.document.body.clientHeight;
	cw = this.document.body.clientWidth;
			
	this.scroll(scroll_size,scroll_size);	
	while(this.document.body.scrollTop > 0 || this.document.body.scrollLeft > 0)
	{
		if(this.document.body.scrollTop > 0)
			iframeObj.style.height = ch+"px";
		if(this.document.body.scrollLeft > 0)
			iframeObj.style.width = cw+"px";
		ch+=scroll_size;
		cw+=scroll_size;
		this.scroll(scroll_size,scroll_size);
	}
;
<?php
/* 
	if (!$outType)
	{
	// Additional height for textarea
?>		
		additionH = parseInt(iframeObj.style.height)+20;
		iframeObj.style.height = additionH;	
<?php 
	}	
*/
?>			
	
}

function resize_iframe_new()
{
	scroll_size = 0;
	iframeObj = this.parent.document.getElementById("imail_frame");
	if(iframeObj==null) return;
	ch = this.document.body.clientHeight;
	cw = this.document.body.clientWidth;
			
	this.scroll(scroll_size,scroll_size);	
	
	var tmp_height = document.getElementById('contentDiv').clientHeight
	var tmp_width = document.getElementById('contentDiv').clientWidth
	
	iframeObj.style.height = tmp_height+"px";
	iframeObj.style.width = tmp_width+"px";
}

function showInternalRep(ShowType)
{
	if(ShowType == "cc")
	{
		displayTable("InterCcShortDiv","none");
		displayTable("InterCcLongDiv","");
	}
	else if(ShowType == "bcc")
	{
		displayTable("InterBccShortDiv","none");
		displayTable("InterBccLongDiv","");			
	}	
	else
	{
		displayTable("InterToShortDiv","none");
		displayTable("InterToLongDiv","");			
	}
	resize_iframe();
}

function showExternalRep(ShowType)
{
	if(ShowType == "cc")
	{
		displayTable("ExterCcShortDiv","none");
		displayTable("ExterCcLongDiv","");
	}
	else if(ShowType == "bcc")
	{
		displayTable("ExterBccShortDiv","none");
		displayTable("ExterBccLongDiv","");			
	}	
	else
	{
		displayTable("ExterToShortDiv","none");
		displayTable("ExterToLongDiv","");			
	}
	resize_iframe();
}

function hideInternalRep(ShowType)
{
	if(ShowType == "cc")
	{
		displayTable("InterCcShortDiv","");
		displayTable("InterCcLongDiv","none");
	}
	else if(ShowType == "bcc")
	{
		displayTable("InterBccShortDiv","");
		displayTable("InterBccLongDiv","none");			
	}	
	else
	{
		displayTable("InterToShortDiv","");
		displayTable("InterToLongDiv","none");			
	}
	resize_iframe();
}

function hideExternalRep(ShowType)
{
	if(ShowType == "cc")
	{
		displayTable("ExterCcShortDiv","");
		displayTable("ExterCcLongDiv","none");
	}
	else if(ShowType == "bcc")
	{
		displayTable("ExterBccShortDiv","");
		displayTable("ExterBccLongDiv","none");			
	}	
	else
	{
		displayTable("ExterToShortDiv","");
		displayTable("ExterToLongDiv","none");			
	}
	resize_iframe();
}

function jUPDATE_EMAIL()
{
	parent.setLabels();
	resize_iframe_new();
	parent.showReplyBox();	
}
</script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/2007script.js"></script>
<!--
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>lang/script.<?= $intranet_session_language?>.js"></script>
-->
<!--
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/content25.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/topbar.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/barchar.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/misc.css" rel="stylesheet" type="text/css" />
-->
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/leftmenu.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/footer.css" rel="stylesheet" type="text/css" />

<style type="text/css" >
html, body
{
	background-color:#FFFFFF;
	scrollbar-face-color:#F7F7F7;
	scrollbar-base-color:#F7F7F7;
	scrollbar-arrow-color:888888;
	scrollbar-track-color:#E2DFDF;
	scrollbar-shadow-color:#F7F7F7;
	scrollbar-highlight-color:#F7F7F7;
	scrollbar-3dlight-color:#C6C6C6;
	scrollbar-darkshadow-Color:#C6C6C6;
	background-image: url("");
	background-repeat: no-repeat;
	background-position: left top;
}

.Conntent_tool{ float:left; display:block;}
.Conntent_tool a, .Conntent_tool a:link, .Conntent_tool a:hover {text-decoration:none;}
.Conntent_tool a.parent_btn {background-color:#e9eef2; border:1px solid #CCCCCC; border-bottom:none;z-index:999999}
.Conntent_tool .btn_option { display:block; float:left}
.Conntent_tool a, .Conntent_tool i{ padding-left:20px; display:block; float:left; height:20px; line-height:20px; 	color: #9966CC;	background:url(../../../images/2009a/content_icon.gif) 0px 0px no-repeat;  padding-right:3px; margin-right:3px}
.Conntent_tool a:hover{	color: #FF0000;}
.Conntent_tool a.print, .Conntent_tool i.print{	background-position:0px -60px;}

</style>
<base target="_parent" >
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff" onload="jUPDATE_EMAIL()" >

<table id="contentDiv" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" >
<tr>
	<td width="100%" align="left" valign="top" >
	<?php 
		echo $li->displayiMail2();
		/*
		if (!$outType)
		{

	     	echo $li->displayiMailNotification2();
	 	}
	 	*/
	?>	
	</td>
</tr>
</table>
</body>
<?php

	intranet_closedb();	
?>