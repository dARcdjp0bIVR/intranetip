<?php
// editing by 
/******************************** Modification Log *****************************************************************
 * 2012-09-04 [Carlos]: Add $Action has_attachment_size_exceed_limit
 * 2011-03-14 [Carlos]: Add action clear tmp folders
 * 2011-01-21 [Carlos]: Add this file for checking quota left after attachment(s) are removed from attachment list 
 *******************************************************************************************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$Action = $_REQUEST['Action'];

switch($Action)
{
	case "clear_tmp_folders":
		$li = new libfilesystem();
		# Clear tmp directories 
	    $composeTime = time();
	    $composeBufferTime = 24*60*60; // 1 day long compose buffer, tmp dir will hold for 1 day
	    $personal_path = "$file_path/file/mail/u$UserID";
	    $tmpComposeFolders = $li->return_folderlist($personal_path);
	    $tmpFoldersToRemove = array();
	    clearstatcache();
	    for($i=0;$i<sizeof($tmpComposeFolders);$i++){
	    	$file_modified_time = filectime($tmpComposeFolders[$i]);
	    	// ready to be removed tmp dir should fulfil these conditions
	    	// 1). it is a directory; 2). end with tmp; 3). dir has been there longer than buffer time 
	    	if(is_dir($tmpComposeFolders[$i]) && file_exists($tmpComposeFolders[$i]) && substr($tmpComposeFolders[$i],-3)=="tmp" && ($composeTime-$file_modified_time)>$composeBufferTime){
	    		if($li->deleteDirectory($tmpComposeFolders[$i]))
	    			$tmpFoldersToRemove[] = $tmpComposeFolders[$i];
	    	}
	    }
	    echo "tmp folders cleared";
	break;
	case "check_attachment_left_quota":
		$folder = trim(urldecode($_REQUEST['folder']));
		$Attachment = sizeof($_REQUEST['Attachment'])>0?$_REQUEST['Attachment']:array();
		
		$li = new libfilesystem();
		$personal_path = "$file_path/file/mail/u$UserID";
		if (!is_dir($personal_path))
		{
		    $li->folder_new($personal_path);
		}
		
		$path = "$file_path/file/mail/$folder";
		$lft = new libfiletable("", $path, 0, 0, "");
		$files = $lft->files;
		
		$totalSize = 0;// count in Kb
		while (list($key, $value) = each($files)) {
			$encoded_name = urlencode($files[$key][0]);
		     if(in_array($encoded_name,$Attachment)){
		          $totalSize += $files[$key][1]/1024;
		     }
		}
		
		$lc = new libcampusquota($UserID);
		$totalQuota = $lc->returnQuota() * 1024;
		$usedQuota = $lc->returnUsedQuota();
		
		$leftQuota = $totalQuota - $usedQuota;
		$leftQuota -= $totalSize;
		echo $leftQuota;
	break;
	
	case "has_attachment_size_exceed_limit":
		$folder = trim(urldecode($_REQUEST['folder']));
		$Attachment = sizeof($_REQUEST['Attachment'])>0?$_REQUEST['Attachment']:array();
		$MaxAttachmentSize = isset($webmail_info['max_attachment_size'])?$webmail_info['max_attachment_size']:10240; // in KB
		
		$li = new libfilesystem();
		$personal_path = "$file_path/file/mail/u$UserID";
		if (!is_dir($personal_path))
		{
		    $li->folder_new($personal_path);
		}
		
		$path = "$file_path/file/mail/$folder";
		$lft = new libfiletable("", $path, 0, 0, "");
		$files = $lft->files;
		
		$totalSize = 0;// count in Kb
		while (list($key, $value) = each($files)) {
			$encoded_name = urlencode($files[$key][0]);
		     if(in_array($encoded_name,$Attachment)){
		          $totalSize += $files[$key][1]/1024;
		     }
		}
		
		if($totalSize > $MaxAttachmentSize){
			echo "1"; // exceeds max limit
		}
	break;
}

intranet_closedb();
?>