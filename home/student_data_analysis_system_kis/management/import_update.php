<?php
## Using By :
##############################################
##	Modification Log:
##	2020-06-22 (Philips)
##	- create this file
##
##############################################
include_once($PATH_WRT_ROOT . 'includes/json.php');
include_once($intranet_root.'/includes/libimporttext.php');
######## Init START ########
$libSDAS = new libSDAS();
$limport = new libimporttext();
$libJSON = new JSON_obj();
######## Init END ########
$Action = $_POST['Action'];
$ReportType = $_POST['Import_Report']; // 0 - Monthly, 1 - Inter School, 2 - Intra School
$ReportSection = $_POST['Import_Section'];
$ReportStartDate = $_POST['ReportStartDate'];
$ReportEndDate = $_POST['ReportEndDate'];
$CurrentPageName =  $Lang['General']['import'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = array();
$MODULE_OBJ["title"] = $CurrentPageName;

$sectionAry = explode('_',$ReportSection);

$csvFile = $_FILES['csvFile'];
$filepath = $csvFile['tmp_name'];
$fileData = $limport->GET_IMPORT_TXT($filepath);
$errorMsg = array();

if($ReportType == 1){
	$headerAry = array('RecordDate','Event','Venue','Guest','ParticipatedSchool','Target','TotalParticipant');
	$colAry = $headerAry;
} else if($ReportType == 2) {
	$headerAry = array(
			array('RecordDate','Event','Venue','Guest','Participant'),
			array('RecordDate','Event','Venue','Guest','Target','Participant')
	);
	$section4HeaderAry = array(
			array('RecordDate','Event','Organization','ParticipatedGroup','Award'),
			array('RecordDate','Event','Organization','ParticipatedGroup','Name','Position','Award','Participant'),
			array('RecordDate','Event','Organization','ParticipatedGroup','Class','Name','Award','Participant')
	);
	if($sectionAry[0]=='1'){
		$colAry = $headerAry[0];
	} else if($sectionAry[0]=='4'){
		$colAry = $section4HeaderAry[$sectionAry[1]-1];
	} else {
		$colAry = $headerAry[1];
	}
} else if($ReportType == 0) {
	$headerAry = array(
			array('RecordDate','Event','Venue','Guest','Participant','Remark'),
			array('RecordDate','Event','Venue','Guest','Target','Participant','Remark')
	);
	$section4HeaderAry = array(
			array('RecordDate','Event','Organization','ParticipatedGroup','Award','Remark'),
			array('RecordDate','Event','Organization','ParticipatedGroup','Name','Position','Award','Participant','Remark'),
			array('RecordDate','Event','Organization','ParticipatedGroup','Class','Name','Award','Participant','Remark')
	);
	if($sectionAry[0]=='1'){
		$colAry = $headerAry[0];
	} else if($sectionAry[0]=='4'){
		$colAry = $section4HeaderAry[$sectionAry[1]-1];
	} else {
		$colAry = $headerAry[1];
	}
}
if(sizeof($fileData) > 1){
	for($i=1;$i<sizeof($fileData);$i++){
		$dataRow = &$fileData[$i];
		for($j=0;$j<sizeof($colAry);$j++){
			$col = $colAry[$j];
			$ckResult = true;
			switch($col){
				case 'RecordDate':
				case 'RecordDate':
					$ckResult = checkDateField($dataRow[$j]);
					break;
				case 'Participant':
				case 'TotalParticipant':
					$ckResult = checkPositiveInt((int)$dataRow[$j]);
					break;
				default:
					$ckResult = true;
					$dataRow[$j] = htmlspecialchars($dataRow[$j]);
					break;
			}
			if($ckResult !== true){
				$errorMsg[] = $Lang['General']['ImportArr']['Row'] . $i . $Lang['General']['ImportArr']['Column'] . $col . ': ' . $ckResult;
			}
		}
	}
	
	if(sizeof($errorMsg) > 0){
		// has error
		$resultTable = '';
		$resultTable .= "<table width='300px' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>";
			$resultTable .= "<tr class=tableTitle bgcolor='#CFE6FE'><td nowarp width='10%'>#</td><td width='90%' class='tabletext'>".$ec_guide['import_error_detail']."</td></tr>\n";
			for($i=0;$i<sizeof($errorMsg);$i++){
				$resultTable .= "<tr>";
					$resultTable .= "<td class='tabletext'>" . ($i + 1) . "</td>";
					$resultTable .= "<td class='tabletext'>" . $errorMsg[$i] . "</td>";
				$resultTable .= "</tr>";
			}
		$resultTable .= '</table>';
	} else {
		// no error
		$resultTable = "<table border='0' cellpadding='5' cellspacing='0' width='300px'>";
			$resultTable .= "<tr>";
				$resultTable .= "<td class='tabletext' width='40%'>" . $Lang['General']['ImportArr']['ImportStepArr']['ImportResult'] . "</td>";
				$resultTable .= "<td class='tabletext' width='60%'>" . $Lang['CEES']['Management']['SchoolActivityReport']['Msg']['Success']. "</td>";
			$resultTable .="</tr>";
			$resultTable .= "<tr>";
				$resultTable .= "<td class='tabletext' width='40%'>" .  $Lang['CEES']['Management']['SchoolActivityReport']['Msg']['ImportCount'] . "</td>";
				$resultTable .= "<td class='tabletext' width='60%'>" . (sizeof($fileData)-1) . "</td>";
			$resultTable .="</tr>";
		$resultTable .= "</table>";
		
		$btnSubmit = $linterface->GET_ACTION_BTN($Lang['CEES']['Management']['SchoolActivityReport']['Hint']['ConfirmImport'], 'button', 'js_ImportFile()');
	
		$AddRowScript = '';
		for($i=1;$i<sizeof($fileData);$i++){
			$dataRow = $fileData[$i];
			$dataObj = array();
			foreach($colAry as $k => $col){
				$dataObj[$col] = $dataRow[$k];
			}
			$rowJson = $libJSON->encode($dataObj);
			$AddRowScript .= "opener.window.js_AddRow('$ReportSection',".$rowJson.");\n";
		}
	}
} else {
	$resultTable = '';
	$resultTable .= "<table width='300px' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>";
		$resultTable .= "<tr>";
		$resultTable .= "<td class='tabletext' align='center'>" . $Lang['CEES']['Management']['SchoolActivityReport']['Msg']['ImportEmptyError']. "</td>";
		$resultTable .= "</tr>";
	$resultTable .= '</table>';
}

$btnCancel = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'window.history.back()');

$linterface->LAYOUT_START();
?>
<script type='text/javascript'>
function js_ImportFile(){
	<?=$AddRowScript?>
	window.close();
}
</script>
<style>
table{width: 300px;}
.main_content{height: 150px;}
</style>
<?=$resultTable?>
<div class="edit_bottom_v30">
	<?=$btnSubmit?>
	&nbsp;
	<?=$btnCancel?>
</div>

<?php
$linterface->LAYOUT_STOP();
function checkDateField(&$field){
	global $Lang;
	//Date format YYYY-MM-DD
	$check_format1 = preg_match('/^[\d]{4}[-]([\d]{2}||[\d]{1})[-]([\d]{2}||[\d]{1})$/i', $field);
	$check_format2 = preg_match('/^([\d]{2}||[\d]{1})[\/]([\d]{2}||[\d]{1})[\/][\d]{4}$/i', $field);
	if($check_format1 || $check_format2) {
		if ($check_format1) {
			$date = explode('-', $field);
			return checkdate($date[1], $date[2], $date[0]) ? true : 0;
		} else {
			$date = explode('/', $field);
			$field = $date[2] . '-' . $date[1] . '-' . $date[0];
			return checkdate($date[1], $date[0], $date[2]) ? true : 0;
		}
	}
	return 'invalid';
}

function checkPositiveInt($field, $step = 1){
	return ($field >= 0 && is_numeric($field) && fmod($field, $step) == 0) ? true : 'invalid';
}

?>