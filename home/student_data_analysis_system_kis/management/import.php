<?php
## Using By :
##############################################
##	Modification Log:
##	2020-06-19 (Philips)
##	- create this file
##
##############################################
######## Init START ########
$libSDAS = new libSDAS();
######## Init END ########
$Action = $_POST['Action'];
$ReportType = $_REQUEST['ReportType']; // 0 - Monthly, 1 - Inter School, 2 - Intra School
$ReportSection = $_REQUEST['ReportSection'];
$ReportStartDate = $_POST['ReportStartDate'];
$ReportEndDate = $_POST['ReportEndDate'];
$csvTemplateLink = "index.php?t=management.csv_template&reportType={$ReportType}&section={$ReportSection}";
$csvTemplateBtn = $linterface->GET_LNK_EXPORT_IP25($csvTemplateLink, $Lang['General']['CSVSample']);
$CurrentPageName =  $Lang['General']['import'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = array();
$MODULE_OBJ["title"] = $CurrentPageName;
$sectionAry = explode('_',$ReportSection);
if($ReportType==0){
	// Monthly
	$navs = array(
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section1'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['ManagementAndOrganization'],
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section2'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['LearnAndTeach'],
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section3'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentSupport'],
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section4'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentArcheivement']
	);
	$subNav = array(
			array(
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ProfessionalDevelopmentAndTrainning'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAdministration'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ProfessionalDevelopmentAndTrainning'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterflowAndSharing']
			),
			array(
					'',
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InsideSchoolActivity'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['OutsideSchoolActivity']
			),
			array(
					'',
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParentSchoolActivity'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParentEducation'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['OtherSupportActivity'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Others']
			),
			array(
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAward'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StaffAward'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentAward']
			)
	);
	$ReportTitle = $Lang['CEES']['Management']['SchoolActivityReport']['Category']['MonthlyActivityReport'];
	$sectionTitle = $subNav[$sectionAry[0]-1][$sectionAry[1]-1];
} else if($ReportType==1){
	// Inter School
	$navs = array(
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ProfessionalDevelopmentAndTrainning'],
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['LearnAndTeach'],
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentSupport']
	);
	$ReportTitle = $Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'] . '(' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterSchool'] . ')';
	$sectionTitle = $navs[$sectionAry[0]-1];
} else if($ReportType==2){
	// Intra School
	$navs = array(
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section1'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['ManagementAndOrganization'],
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section2'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['LearnAndTeach'],
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section3'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentSupport'],
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section4'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAndStudentPerformance']
	);
	$subNav = array(
			array(
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAdministration'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ProfessionalDevelopmentAndTrainning'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterflowAndSharing']
			),
			array(
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InsideSchoolActivity'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['OutsideSchoolActivity']
			),
			array(
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParentSchoolActivity'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParentEducation'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['OtherSupportActivity'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Others']
			),
			array(
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAward'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StaffAward'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentAward']
			)
	);
	$ReportTitle = $Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'] . '(' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['IntraSchool'] . ')';
	$sectionTitle = $subNav[$sectionAry[0]-1][$sectionAry[1]-1];
}

$linterface->LAYOUT_START();
?>
<style>
table{width: 500px;}
</style>
<FORM name="form1" id="form1" style='width:500px;' enctype="multipart/form-data" action="index.php?t=management.import_update" method="POST">
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td height="10" colspan="2">&nbsp;</td>
    </tr>
  	<tr>
		<td align="right" valign="top" class="tabletext"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['TargetReport']?></td>
		<td valign="top"><?=$ReportTitle?></td>
	</tr>
	<tr>
		<td align="right" valign="top" class="tabletext"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['TargetSection']?></td>
		<td valign="top"><?=$sectionTitle?></td>
	</tr>
    <tr>
      <td align="right" class="tabletext"><?=$Lang['General']['File']?></td>
      <td class="tabletext"><input name="csvFile" class="file" type="file" size="20" /></td>
    </tr>
  </table>
  
  <br>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
    </tr>
  </table>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
    	<td><?=$csvTemplateBtn?></td>
      <td align="right">
        <input id="btn_submit" class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_submit?>" >
        <input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" onClick="self.close()">
      </td>
    </tr>
  </table>
	<input type="hidden" name="Import_Section" value="<?=$ReportSection?>" >
	<input type="hidden" name="Import_Report" value="<?=$ReportType?>" >
	<input type="hidden" name="ReportStartDate" value="<?=$ReportStartDate?>" />
	<input type="hidden" name="ReportEndDate" value="<?=$ReportEndDate?>" />
</FORM>
<?php 
$linterface->LAYOUT_STOP();
?>