<?php
## Using By :
##############################################
##	Modification Log:
##	2020-06-02 (Philips)
##	- create this file
##
##############################################
include_once($PATH_WRT_ROOT . 'includes/json.php');
######## Init START ########
$libSDAS = new libSDAS();
$libJSON = new JSON_obj();
######## Init END ########
$Action = $_POST['Action'];
$ReportMonth = $_POST['ReportMonth'];
$ReportStartDate = date('Y-'.$ReportMonth.'-01');
$ReportEndDate = date('Y-m-t',strtotime($ReportStartDate));
// $ReportEndDate = date('Y-'.$ReportMonth.'-t');
$AcademicYearID= $_POST['AcademicYearID'];
$ReportSection = $_POST['ReportSection'];
$CategoryAry= array(
			'O1' => array('ProfessionalDevelopmentAndTrainning'),
			'O2' => array('LearnAndTeach'),
			'O3' => array('StudentSupport'),
			'I1' => array('SchoolAdministration','ProfessionalDevelopmentAndTrainning','InterflowAndSharing'),
			'I2' => array('InsideSchoolActivity', 'OutsideSchoolActivity'),
			'I3' => array('ParentSchoolActivity','ParentEducation','OtherSupportActivity','Others')
);
switch($Action){
	case 'syncEnrolment':
		$sectionAry = explode('_', $ReportSection);
		$ActivityPrefix = ($sectionAry[1]=='1') ? 'O' : 'I';
// 		$sectionAry[0] = $sectionAry[0]=='2'?'3': $sectionAry[0];
		if($ActivityPrefix == 'O'){
			$targetCategory = $ActivityPrefix . $sectionAry[0]. '_'.$CategoryAry[$ActivityPrefix.$sectionAry[0]][0];
		} else {
			$targetCategory = $ActivityPrefix . $sectionAry[0]. '_'.$CategoryAry[$ActivityPrefix.$sectionAry[0]][$sectionAry[1]-2];
		}
		$table = 'INTRANET_ENROL_EVENTINFO';
		$joinTable = 'INTRANET_ENROL_EVENT_DATE';
		$joinTable2 = 'INTRANET_ENROL_EVENTSTUDENT';
		
		// 2020-07-06 (Philips) - Section 1 no need to check member
		if($sectionAry[0]!='1'){
			//$join2 = "INNER JOIN $joinTable2 c
			$join2 = "LEFT JOIN $joinTable2 c
			ON a.EnrolEventID = c.EnrolEventID";
		} else {
			$join2 = "LEFT OUTER JOIN $joinTable2 c
			ON a.EnrolEventID = c.EnrolEventID";
		}
		
		//GROUP_CONCAT(DATE_FORMAT(b.ActivityDateStart, "%Y-%m-%d"))
		$cols = 'a.EventTitle, a.SDAS_Category, a.SDAS_AllowSync, GROUP_CONCAT(DISTINCT DATE_FORMAT(b.ActivityDateStart, "%Y-%m-%d")) AS EventDate, a.Location, IF(a.TotalParticipant NOT IN (0, ""), a.TotalParticipant, COUNT(*)) AS Total,
				 a.ActivityTarget, a.Guest, a.ParticipatedSchool';
		$conds = " a.SDAS_AllowSync LIKE '%M%' AND a.SDAS_Category = '$targetCategory' ";
		$conds .= " AND DATE_FORMAT(b.ActivityDateStart, '%Y-%m-%d') >= '$ReportStartDate' AND DATE_FORMAT(b.ActivityDateStart, '%Y-%m-%d') <= '$ReportEndDate' ";
		$sql = "SELECT $cols FROM $table a
		INNER JOIN $joinTable b
		ON a.EnrolEventID = b.EnrolEventID
		$join2
		WHERE $conds
		Group By a.EnrolEventID
		";
// 						debug_pr($sql);
		$recordAry = $libSDAS->returnArray($sql);
		$result['record'] = $recordAry;
		echo $libJSON->encode($result);
		break;
	case 'syncSchoolAward':
		$table = $eclass_db . '.AWARD_SCHOOL';
		$col = "AwardDate, AwardName, AwardFile, Details, Organization, SubjectArea, Remark, ParticipatedGroup";
		$conds = "AcademicYearID = '$AcademicYearID' AND AwardDate >= '$ReportStartDate' AND AwardDate <= '$ReportEndDate' AND SDAS_AllowSync LIKE '%M%' ";
		$sql = "SELECT $col FROM $table WHERE $conds";
// 		debug_pr($sql);die();
		$recordAry = $libSDAS->returnArray($sql);
		$result['record'] = $recordAry;
		echo $libJSON->encode($result);
		break;
	case 'syncStudentAward':
		$table = $eclass_db . '.AWARD_STUDENT';
		$col = "a.AwardDate, a.AwardName, a.AwardFile, a.Details, a.Organization, a.SubjectArea, a.Remark, a.ParticipatedGroup,
				GROUP_CONCAT(iu.EnglishName) AS EnglishName, GROUP_CONCAT(iu.ChineseName) AS ChineseName,
				GROUP_CONCAT(DISTINCT yc.ClassTitleB5) AS ClassTitleB5, GROUP_CONCAT(DISTINCT yc.ClassTitleEN) AS ClassTitleEN, COUNT(*) AS TotalParticipant
				";
		$conds = "a.AcademicYearID = '$AcademicYearID' AND a.AwardDate >= '$ReportStartDate' AND a.AwardDate <= '$ReportEndDate' AND a.SDAS_AllowSync LIKE '%M%' ";
		$sql = "SELECT $col FROM $table a
		INNER JOIN INTRANET_USER iu
		ON iu.UserID = a.UserID
		INNER JOIN YEAR_CLASS_USER ycu
		ON iu.UserID = ycu.UserID
		INNER JOIN YEAR_CLASS yc
		ON yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = a.AcademicYearID
		WHERE $conds
		GROUP BY a.AwardDate, AwardName, a.Organization, a.ParticipatedGroup";
		$recordAry = $libSDAS->returnArray($sql);
		$result['record'] = $recordAry;
		echo $libJSON->encode($result);
		break;
	default:
		break;
}
?>