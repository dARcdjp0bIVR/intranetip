<?php
## using: anna
################# Change Log [Start] #####
#				Page Created
#
################## Change Log [End] ######

######## Init START ########
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/cees/libcees.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");
include_once($PATH_WRT_ROOT."/includes/subject_class_mapping.php");


$jsonObj = new JSON_obj();
$libpf_exam = new libpf_exam();
$libSDAS = new libSDAS();
$lcees = new libcees();
$libpf_academic = new libpf_academic();
$subject_class  = new subject();
######## Init END ########

// $academicYearID = 20;
// $exam = 3;
define("success", 1);
define("failed", 0);

if($academicYearID > 0 && $reportType>0){
	if($reportType==1){
		// InterSchool
		$ReportData = $libSDAS->getInterSchoolECA($academicYearID,$reportID);
		$ReportInfo = array(
				'Year' => $ReportData['Year'],
				'StartDate' => $ReportData['StartDate'],
				'EndDate' => $ReportData['EndDate'],
				'ReportKey' => $ReportData['ReportKey']
		);
		$ReportRecordAry = $libSDAS->getInterSchoolECARecord($ReportData['ReportID']);
	} else if($reportType==2){
		// IntraSchool
		$ReportData = $libSDAS->getIntraSchoolECA($academicYearID,$reportID);
		$ReportRecordAry = $libSDAS->getIntraSchoolECARecord($ReportData['ReportID']);
		$ReportInfo = array(
				'Year' => $ReportData['Year'],
				'StartDate' => $ReportData['StartDate'],
				'EndDate' => $ReportData['EndDate'],
				'ReportKey' => $ReportData['ReportKey']
		);
	} else if($reportType==3){
		$ReportData = $libSDAS->getMonthlyECA($academicYearID, $month);
		$ReportInfo = array(
				'Year' => $ReportData['Year'],
				'ReportDate' => $ReportData['ReportDate'],
				'Month' => $ReportData['Month']
		);
		$ReportRecordAry = $libSDAS->getMonthlyECARecord($ReportData['ReportID']);
	}
	if(count($ReportRecordAry)>0){
		// prepare data
		$colAry =array(
				"",
				"Section,RecordDate,Event,Venue,Guest,ParticipatedSchool,Target,TotalParticipant",
				"Section,RecordDate,Event,Venue,Guest,Target,ParticipatedSchool,Participant,Organization,ParticipatedGroup,Award,Name,Position,Class",
				"Section,RecordDate,Event,Venue,Guest,Target,ParticipatedSchool,Participant,Organization,ParticipatedGroup,Award,Name,Position,Class,Remark"
		);
		$_thisCol = $colAry[$reportType];
		$_thisFieldAry = explode(',', $_thisCol);
		foreach((array)$ReportRecordAry as $_dataArr){
			// initialize
			$dataTemp = array();
			foreach($_thisFieldAry as $_thisField){
				$dataTemp[$_thisField] = $_dataArr[$_thisField];
			}
			$dataArrTemp[] = $dataTemp;
		}
	}else{
		$dataArrTemp = array('confirmWithNoData');
	}
	// common sendData to Central
	include_once($PATH_WRT_ROOT."includes/cees/libcees_aes.php");
	$lceesAes = new libcees_aes();
	$dataArr = $dataArrTemp;
	$success = $lcees->syncReportToCentral($academicYearID, $reportType, $ReportInfo,$dataArr);
	
	# Use report save sync record time
	$tableAry = array(
			"",
			"CEES_KIS_MONTHLY_REPORT_ECA_INTERSCHOOL",
			"CEES_KIS_MONTHLY_REPORT_ECA_INTRASCHOOL",
			"CEES_KIS_MONTHLY_REPORT_ECA_MONTHLY_REPORT"
	);
	$table = $tableAry[$reportType];
	$conds = "ReportID = '$ReportData[ReportID]'";
	$vals = "SubmitStatus = '1', SubmittedToCeesBy = '$_SESSION[UserID]', SubmittedToCeesDate = NOW()";
	$sql = "UPDATE $table SET $vals WHERE $conds";
	$libSDAS->db_db_query($sql);
// 	debug_pr($sql);
    if($success === 1){
    	$status = success;
    }else{
    	$status = failed;
    }
    echo $status;
}
?>