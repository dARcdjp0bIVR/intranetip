<?php
## Using By : 
##############################################
##	Modification Log:
##	2020-05-28 (Philips)
##	- create this file
##
##############################################

include_once($PATH_WRT_ROOT . 'includes/json.php');

######## Init START ########
$libSDAS = new libSDAS();
$libJSON = new JSON_obj();
######## Init END ########

######## Page Setting START ########
$CurrentPage = "management.intra_school_activity_report";
$CurrentPageName =  $Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'] . '(' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['IntraSchool'] . ')';
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
$Page_NavArr = array();
######## Page Setting END ########

$ViewMode = 'Mark';
$AcademicYearID = $_REQUEST['AcademicYearID'];
if($AcademicYearID == ''){
	$AcademicYearID = Get_Current_Academic_Year_ID();
}
$ECAReportList = $libSDAS->getIntraSchoolECA($AcademicYearID);
$ECAReportListData = array();
foreach($ECAReportList as $ecar){
	$ECAReportListData[] = array($ecar['ReportID'], $ecar['StartDate'] . ' ' . $Lang['General']['To'] . ' ' . $ecar['EndDate']);
}
$ECAReportSelect = getSelectByArray($ECAReportListData, 'name="ReportID" id="ReportID" onChange="js_Changed_ReportID_Selection(this.value)"', $ReportID, 0, $noFirst=0, $FirstTitle="");

$xmsg = $_REQUEST['xmsg'];

$libfcm_ui = new form_class_manage_ui();
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
foreach((array)$academic_year_arr as $_key => $_infoArr){
	$academic_year_arr[$_key]['1'] = iconv('Big5-HKSCS','UTF-8',$_infoArr['1']);
	$academic_year_arr[$_key]['AcademicYearName'] = iconv('Big5-HKSCS','UTF-8',$_infoArr['AcademicYearName']);
}
$AcademicYearSelection = getSelectByArray($academic_year_arr, "name='AcademicYearID' id='AcademicYearID' onChange='js_Changed_AcademicYear_Selection(this.value);'", $AcademicYearID, 0, 1);
if($ReportID!=''){
	# Navigation Init
	$navs = array(
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section1'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['ManagementAndOrganization'],
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section2'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['LearnAndTeach'],
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section3'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentSupport'],
			$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section4'] . ': ' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAndStudentPerformance']
	);
	$subNav = array(
			array(
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAdministration'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ProfessionalDevelopmentAndTrainning'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterflowAndSharing']
			),
			array(
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InsideSchoolActivity'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['OutsideSchoolActivity']
			),
			array(
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParentSchoolActivity'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParentEducation'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['OtherSupportActivity'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Others']
			),
			array(
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAward'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StaffAward'],
					$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentAward']
			)
	);
	
	
	$ECAReport = $libSDAS->getIntraSchoolECA($AcademicYearID, $ReportID);
	$isSubmit = $ECAReport['SubmitStatus'];
	$isDraft = $ECAReport['DraftStatus'];
	if($ECAReport['ReportID']){
		$recordAry = $libSDAS->getIntraSchoolECARecord($ECAReport['ReportID']);
		// 	$SectionDataAry = BuildMultiKeyAssoc($recordAry, 'Section', array('ReportID', 'Section', 'RecordDate', 'Event', 'Venue', 'Guest', 'ParticipatedSchool', 'Target', 'TotalParticipant'));
		$SectionDataAry = array();
		foreach($recordAry as $record){
			$sNum = $record['Section'];
			if(!isset($SectionDataAry[$sNum])) $SectionDataAry[$sNum] = array();
			$row = array();
			foreach($record as $k => $v){
				if(!is_int($k)){
					$row[$k] = $v;
				}
			}
			$SectionDataAry[$sNum][] = $row;
			unset($row);
		}
		$SectionCountAry = array(array());
		foreach($subNav as $k => $sNav){
			foreach($sNav as $i => $item){
				$SectionCountAry[$k+1][$i+1] = count($SectionDataAry[($k+1).'_'.($i+1)]);
			}
		}
	} else {
		$recordAry = array();
		$SectionCountAry = array(array());
		foreach($subNav as $k => $sNav){
			foreach($sNav as $i => $item){
				$SectionCountAry[$k+1][$i+1] = 0;
			}
		}
	}
	$sectionCountJson = $libJSON->encode($SectionCountAry);
	$headerAry = array(
			array(
					'order' => array('#','1'),
					'Date' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Date'], '20%'),
					'Event' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Event'], '25%'),
					'Venue' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Venue'], '20%'),
					'Guest' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Guest'], '25%'),
					'Participant' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Participant'], '10%'),
					'delete' => array('', '1')
			),
			array(
					'order' => array('#','1'),
					'Date' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Date'], '20%'),
					'Event' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Event'], '20%'),
					'Venue' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Venue'], '15%'),
					'Guest' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Guest'], '20%'),
					'Target' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Target'], '15%'),
					'Participant' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Participant'], '10%'),
					'delete' => array('', '1')
			)
	);
	$section4HeaderAry = array(
			array(
				'order' => array('#','1'),
				'Date' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['AwardDate'], '20%'),
				'Event' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['EventName'], '25%'),
				'Organization' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Organization'], '20%'),
				'ParticipatedGroup' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ParticipatedGroup'], '25%'),
				'Award' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Award'], '10%'),
				'delete' => array('', '1')
			),
			array(
				'order' => array('#','1'),
				'Date' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['AwardDate'], '15%'),
				'Event' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['EventName'], '15%'),
				'Organization' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Organization'], '15%'),
				'ParticipatedGroup' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ParticipatedGroup'], '15%'),
				'Name' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Name'], '10%'),
				'Position' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Position'], '10%'),
				'Award' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Award'], '10%'),
				'Participant' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['TotalParticipant'], '10%'),
				'delete' => array('', '1')
			),
			array(
				'order' => array('#','1'),
				'Date' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['AwardDate'], '15%'),
				'Event' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['EventName'], '15%'),
				'Organization' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Organization'], '15%'),
				'ParticipatedGroup' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ParticipatedGroup'], '15%'),
				'Class' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Class'], '10%'),
				'Name' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Name'], '10%'),
				'Award' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Award'], '10%'),
				'Participant' => array($Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['TotalParticipant'], '10%'),
				'delete' => array('', '1')
			)
	);
	$widthAry = array();
	foreach($headerAry as $header){
		$widthAry[0][] = $header[1];
	}
	
	foreach($section4HeaderAry as $x => $ary){
		foreach($ary as $header){
			$widthAry[$x+1][] = $header[1];
		}
	}
	foreach($navs as $nav){
		$Page_NavArr[] = $nav;
	}
	
	if($AcademicYearID){
		$ecaAry = array();
	}
	
	// Warning Msg Box
	
	// Submit Button
	$SubmitBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Submit_Report();", $id="Btn_Submit");
	// Submit Button
	$DraftBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "js_Draft_Report();", $id="Btn_Draft");
	// Cancel Submit Button
	$CancelSubmitBtn = $linterface->GET_ACTION_BTN($Lang['CEES']['Management']['SchoolActivityReport']['Category']['CancelSubmit'], "button", "js_CancelSubmit_Report();", $id="Btn_CancelSubmit");
	
	if(!empty($SectionDataAry)){
		$AddRowScript = "";
		foreach($SectionDataAry as $section => $SectionData){
			foreach($SectionData as $data){
				$json = $libJSON->encode($data);
				$AddRowScript .= "js_AddRow('$section', $json);\n";
			}
		}
	}

}
// debug_pr($AddRowScript);die();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
// echo $linterface->Include_Cookies_JS_CSS();
echo $linterface->Include_CopyPaste_JS_CSS('2016');
echo $linterface->Include_Excel_JS_CSS();
echo $linterface->Include_Thickbox_JS_CSS();
?>
<script>
var jsCurAcademicYearID = '<?php echo $AcademicYearID?>';
<?php if($ReportID!=''){ ?>
var SectionRowCount = <?=$sectionCountJson?>;
var widthSetting = ['<?=implode("','", $widthAry[0])?>'];
var widthSetting1 = ['<?=implode("','", $widthAry[1])?>'];
var widthSetting2 = ['<?=implode("','", $widthAry[2])?>'];
<?php } ?>
$(document).ready( function() {
	<?=$AddRowScript?>
	<?php if($isDraft || $isSubmit){?>
	$('input').attr('disabled',true);
	$('input[type=button]').removeAttr('disabled');
	$('textarea').attr('disabled',true);
<?php }?>
});

function js_Draft_Report(){
	if(checkForm()){
		document.form1.action.value = 'draft';
		document.form1.setAttribute('action','?t=management.intra_school_activity_report.update');
		document.form1.submit();
	}
}
function js_Submit_Report(){
	if(checkForm()){
		document.form1.action.value = 'submit';
		document.form1.setAttribute('action','?t=management.intra_school_activity_report.update');
		document.form1.submit();
	}
}
function js_CancelSubmit_Report(){
	if(checkForm()){
		document.form1.action.value = 'release';
		document.form1.setAttribute('action','?t=management.intra_school_activity_report.update');
		document.form1.submit();
	}
}
function js_Changed_AcademicYear_Selection(id){
	window.location.href = '?t=management.intra_school_activity_report.index&AcademicYearID=' + id;
}
function js_Changed_ReportID_Selection(id){
	window.location.href = '?t=management.intra_school_activity_report.index&AcademicYearID=' + $('#AcademicYearID').val() + '&ReportID=' + id;
}
function js_AddRow(targetTable, data){
	let division = targetTable.split('_');
// 	console.log(division);
	let id = SectionRowCount[division[0]][division[1]]++;
// 	console.log(id);
	tempData = {
		date: data['RecordDate'] ? data['RecordDate'] : '',
		evt: data['Event'] ? data['Event'] : '',
		venue: data['Venue'] ? data['Venue'] : '',
		guest: data['Guest'] ? data['Guest'] : '',
		participant: data['Participant'] ? data['Participant'] : '',
		target: data['Target'] ? data['Target'] : '',
		organization: data['Organization'] ? data['Organization'] : '',
		participatedGroup: data['ParticipatedGroup'] ? data['ParticipatedGroup'] : '',
		award: data['Award'] ? data['Award'] : '',
		name: data['Name'] ? data['Name'] : '',
		position: data['Position'] ? data['Position'] : '',
		class: data['Class'] ? data['Class'] : ''
	}
	tableEle = $('#Section'+targetTable+'Div').find('table')[0];
	tableBody = $(tableEle).find('tbody')[0];
	tableBody = $(tableBody);
	lastRow = $(tableBody).find('tr').last();

	let newRow = $('<tr>').attr('id', 'Section'+targetTable+'_Row'+id);
	let emptyTD = $('<td>');
	newRow.append(emptyTD);
	if(targetTable!='4_1' && targetTable!='4_2' && targetTable!='4_3'){
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Date['+id+']" id="Section'+targetTable+'Date['+id+']" class="input_date" />').val(tempData.date));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Event['+id+']" id="Section'+targetTable+'Event['+id+']" class="input_text" />').val(tempData.evt));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Venue['+id+']" id="Section'+targetTable+'Venue['+id+']" class="input_text" />').val(tempData.venue));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Guest['+id+']" id="Section'+targetTable+'Guest['+id+']" class="input_text" />').val(tempData.guest));
		newRow.append(emptyTD);
		if(targetTable.charAt(0) == '2' || targetTable.charAt(0)=='3'){
			emptyTD = $('<td>');
			emptyTD.append( $('<input type="text" name="Section'+targetTable+'Target['+id+']" id="Section'+targetTable+'Target['+id+']" class="input_text" />').val(tempData.target));
			newRow.append(emptyTD);
		}
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Participant['+id+']" id="Section'+targetTable+'Participant['+id+']" class="input_number" />').val(tempData.participant));
		newRow.append(emptyTD);
	} else if(targetTable=='4_1'){
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Date['+id+']" id="Section'+targetTable+'Date['+id+']" class="input_date" />').val(tempData.date));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Event['+id+']" id="Section'+targetTable+'Event['+id+']" class="input_text" />').val(tempData.evt));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Organization['+id+']" id="Section'+targetTable+'Organization['+id+']" class="input_text" />').val(tempData.organization));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'ParticipatedGroup['+id+']" id="Section'+targetTable+'ParticipatedGroup['+id+']" class="input_text" />').val(tempData.participatedGroup));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Award['+id+']" id="Section'+targetTable+'Award['+id+']" class="input_text" />').val(tempData.award));
		newRow.append(emptyTD);
	} else if(targetTable=='4_2'){
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Date['+id+']" id="Section'+targetTable+'Date['+id+']" class="input_date" />').val(tempData.date));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Event['+id+']" id="Section'+targetTable+'Event['+id+']" class="input_text" />').val(tempData.evt));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Organization['+id+']" id="Section'+targetTable+'Organization['+id+']" class="input_text" />').val(tempData.organization));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'ParticipatedGroup['+id+']" id="Section'+targetTable+'ParticipatedGroup['+id+']" class="input_text" />').val(tempData.participatedGroup));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Name['+id+']" id="Section'+targetTable+'Name['+id+']" class="input_text" />').val(tempData.name));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Position['+id+']" id="Section'+targetTable+'Position['+id+']" class="input_txet" />').val(tempData.position));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Award['+id+']" id="Section'+targetTable+'Award['+id+']" class="input_text" />').val(tempData.award));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Participant['+id+']" id="Section'+targetTable+'Participant['+id+']" class="input_number" />').val(tempData.participant));
		newRow.append(emptyTD);
	} else {
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Date['+id+']" id="Section'+targetTable+'Date['+id+']" class="input_date" />').val(tempData.date));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Event['+id+']" id="Section'+targetTable+'Event['+id+']" class="input_text" />').val(tempData.evt));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Organization['+id+']" id="Section'+targetTable+'Organization['+id+']" class="input_text" />').val(tempData.organization));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'ParticipatedGroup['+id+']" id="Section'+targetTable+'ParticipatedGroup['+id+']" class="input_text" />').val(tempData.participatedGroup));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Class['+id+']" id="Section'+targetTable+'Class['+id+']" class="input_txet" />').val(tempData.class));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Name['+id+']" id="Section'+targetTable+'Name['+id+']" class="input_text" />').val(tempData.name));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Award['+id+']" id="Section'+targetTable+'Award['+id+']" class="input_text" />').val(tempData.award));
		newRow.append(emptyTD);
		emptyTD = $('<td>');
		emptyTD.append( $('<input type="text" name="Section'+targetTable+'Participant['+id+']" id="Section'+targetTable+'Participant['+id+']" class="input_number" />').val(tempData.participant));
		newRow.append(emptyTD);
	}
	<?php if(!$isDraft && !$isSubmit){ ?>
	emptyTD = $('<td>');
	let linkDel = $('<?=$linterface->GET_LNK_DELETE('#')?>');
	linkDel.find('a').attr('href', 'javascript:js_RemoveRow("Section'+targetTable+'_Row'+id+'")');
	emptyTD.append(linkDel);
	newRow.append(emptyTD);
	<?php }?>
	newRow.insertBefore(lastRow);

	js_InitDatePicker('Section'+targetTable+'_Date['+id+']');

	js_RefreshRowNum();
	js_RefreshRowWidth();
}
function js_RefreshRowWidth(){
	$('.record_table tr:not(:last)').each(function(k, ele){
		$(ele).find('td').each(function(index, val){
			$(val).css('width', widthSetting[index]);
			$(val).find('input').css('width','90%');
			$(val).find('textarea').css('width','90%');
		})
	});
	$('#Section4_1Table tr:not(:last)').each(function(k, ele){
		$(ele).find('td').each(function(index, val){
			$(val).css('width', widthSetting1[index]);
			$(val).find('input').css('width','90%');
			$(val).find('textarea').css('width','90%');
		})
	});
	$('#Section4_2Table tr:not(:last)').each(function(k, ele){
		$(ele).find('td').each(function(index, val){
			$(val).css('width', widthSetting2[index]);
			$(val).find('input').css('width','90%');
			$(val).find('textarea').css('width','90%');
		})
	});
	$('#Section4_3Table tr:not(:last)').each(function(k, ele){
		$(ele).find('td').each(function(index, val){
			$(val).css('width', widthSetting2[index]);
			$(val).find('input').css('width','90%');
			$(val).find('textarea').css('width','90%');
		})
	});
}
function js_RefreshRowNum(){
	$('.record_table').each(function(k, table){
			$(table).find('tr:not(:last)').each(function(index, val) {
			$(val).find('td').first().text(index);
		});
	});
}
function js_RemoveRow(id){
	$('#'+id).remove();
	js_RefreshRowNum();
}
function checkForm(){
	$('input.input_date').each(function(i, ele){ if(!check_date_30(ele, '<?=$Lang['General']['InvalidDateFormat']?>')) return false;});
	return true;
}
function js_AddDatePicker(id, val){
	let dom = '<input type="text" name="'+id+'" id="'+id+'" value="'+val+'" size="10" maxlength="10" class="textboxnum hasDatepick" onkeyup="Date_Picker_Check_Date_Format(this,\'DPWL-'+id+'\',\'\');">';
	return dom;
}
function js_AddDatePickerSpan(id){
	let dom = '<span style="color:red;" id="DPWL-'+id+'"></span>';
	return dom;
}
function js_InitDatePicker(id){
	$('input#' + id).ready(function(){
		$('input#' + id).datepick({
			
			dateFormat: 'yy-mm-dd',
			dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			changeFirstDay: false,
			firstDay: 0,
			onSelect: function(dateText, inst) {
				Date_Picker_Check_Date_Format(document.getElementById(id),'DPWL-' + id,'');
			}
		});
	});
}
function js_RetrieveData(section, target){
	let href = '?t=management.intra_school_activity_report.ajax';
	$.ajax({
		url: href,
		data: {
			AcademicYearID: $('#AcademicYearID').val(),
			ReportStartDate: $('#ReportStartDate').val(),
			ReportEndDate: $('#ReportEndDate').val(),
			Action: 'sync'+section,
			ReportSection: target
		},
		dataType: 'JSON',
		method: 'POST',
		success: function(data){
// 			console.log(data);return;
			let confirm = window.confirm('<?=$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['RetrieveDataRow'] . ': '?>' + data.record.length + '<?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Row']?>');
			if(confirm){
				for(let i=0;i<data.record.length; i++){
					let recordRow = {};
					if(section=='SchoolAward'||section=='StudentAward'){
						recordRow = {
							RecordDate: data.record[i].AwardDate,
							Event: data.record[i].SubjectArea,
							Organization: data.record[i].Organization,
							Award: data.record[i].AwardName,
							ParticipatedGroup: data.record[i].ParticipatedGroup,
						};
						if(section=='StudentAward'){
							recordRow.Class = data.record[i].ClassTitleEN;
							<?php if($intranet_session_language == 'en'){?>
							recordRow.Name = data.record[i].EnglishName;
							<?php }else{?>
							recordRow.Name = data.record[i].ChineseName;
							<?php }?>
							recordRow.Participant = data.record[i].TotalParticipant;
						}
					} else {
						recordRow = {
							RecordDate: data.record[i].EventDate,
							Event: data.record[i].EventTitle,
							Venue: data.record[i].Location,
							Participant: data.record[i].Total,
							Guest: data.record[i].Guest,
							ParticipatedSchool: data.record[i].ParticipatedSchool,
							Target: data.record[i].ActivityTarget
						};
					}
					js_AddRow(target, recordRow);
				}
			}
		}
	});
}
function js_AddReport(){
	let AcademicYearID = $('#AcademicYearID').val();
	$.ajax({
		url: '?t=management.intra_school_activity_report.ajax',
		method: 'POST',
		data: {'AcademicYearID': AcademicYearID, 'Action': 'addReport'},
		success: function(data){
			if(data!=''){
				reportID = data;
				js_Changed_ReportID_Selection(reportID);
			} else {
			}
		}
	});
}

function js_RemoveReport(){
	let AcademicYearID = $('#AcademicYearID').val();
	let ReportID = $('#ReportID').val();
	let confirm = window.confirm('<?=$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['ConfirmRemoveReport']?>');
	if(!confirm) return;
	$.ajax({
		url: '?t=management.intra_school_activity_report.ajax',
		method: 'POST',
		data: {'AcademicYearID': AcademicYearID,'ReportID': ReportID, 'Action': 'removeReport'},
		success: function(data){
			if(data!=''){
				js_Changed_AcademicYear_Selection(AcademicYearID);
			} else {
			}
		}
	});
}
function js_ImportData(target){
	href = "?t=management.import&ReportType=2&ReportSection=" + target;
	newWindow(href, 16);
}
</script>
<form id="form1" name="form1" action="" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0" width='1000px'>
		<tr>
			<td style="padding-left:7px;">
				<br style="clear:both;" />
				<div class="table_filter">
					<?=$AcademicYearSelection?>
					<?=$ECAReportSelect?>
				</div>
				<div class='table_row_tool row_content_tool' style='float:left'>
					<?php if($ReportID!=''&&!$isDraft&&!$isSubmit){
						echo $linterface->GET_LNK_DELETE('javascript:js_RemoveReport()','','');
					}?>
				</div>
				<br style="clear:both;" />
				<div class='Conntent_tool'>
				<?php if($ReportID == ''){?>
					<?=$linterface->GET_LNK_NEW("javascript:js_AddReport()", $Lang['CEES']['Management']['SchoolActivityReport']['Category']['NewReport'], $ParOnClick="")?>
				<?php }?>
				</div>
				<br style="clear:both;" />
				<?php if($ReportID!=''){?>
					<?php if($AcademicYearID!=''){?>
						<table class='form_table_v30' width='50%'>
							<tbody>
								<tr>
									<td class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ReportStartDate']?></td>
									<td class="field_content_short">
										<?=$linterface->GET_DATE_PICKER('ReportStartDate', $ECAReport['StartDate'])?>
									</td>
								</tr>
								<tr>
									<td class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ReportEndDate']?></td>
									<td class="field_content_short">
										<?=$linterface->GET_DATE_PICKER('ReportEndDate', $ECAReport['EndDate'])?>
									</td>
								</tr>
							</tbody>
						</table>
						<?php $i = 0;
						foreach($Page_NavArr as $Page_Nav){
							$i++; 
							$y=0;
							?>
							<hr/>
							<?=$linterface->GET_NAVIGATION2($Page_Nav);
								foreach($subNav[$i-1] as $sNav){
									$y++;
									if($i=='1') $tempHeaderAry = $headerAry[0];
									if($i!='1') $tempHeaderAry = $headerAry[1];
									if($i=='4') $tempHeaderAry = $section4HeaderAry[$y-1];
							?>
								<br style="clear:both;" />
								<h3><?=chr(64+$y).'. '.$sNav?></h3>
								<div id="Section<?=$i.'_'.$y?>Div">
									<div class="Conntent_tool">
									<?php
									if(!$isDraft && !$isSubmit) {
										if($i!=4){
											echo $linterface->GET_LNK_IMPORT("javascript:js_RetrieveData('Enrolment', '$i".'_'."$y')", $Lang['CEES']['Management']['SchoolActivityReport']['Category']['RetrieveDataFromeEnrolment'], $ParOnClick="");
											if($i==1){
												echo $linterface->GET_LNK_IMPORT("javascript:js_ImportData('$i".'_'."$y')", $Lang['General']['Import'], $ParOnClick="");
											}
										}else if($i==4&&$y==1){
											echo $linterface->GET_LNK_IMPORT("javascript:js_RetrieveData('SchoolAward', '$i".'_'."$y')", $Lang['CEES']['Management']['SchoolActivityReport']['Category']['RetrieveDataFromiPortoflio'], $ParOnClick="");
										} else if($i==4&&$y==3){
											echo $linterface->GET_LNK_IMPORT("javascript:js_RetrieveData('StudentAward', '$i".'_'."$y')", $Lang['CEES']['Management']['SchoolActivityReport']['Category']['RetrieveDataFromiPortoflio'], $ParOnClick="");
										} else if($i==4&&$y==2){
											echo $linterface->GET_LNK_IMPORT("javascript:js_ImportData('$i".'_'."$y')", $Lang['General']['Import'], $ParOnClick="");
										}
									}
										?>
									</div>
									<table id="Section<?=$i.'_'.$y?>Table" class="common_table_list_v30 edit_table_list_v30 record_table" width='100%'>
										<thead>
											<?php foreach($tempHeaderAry as $key => $header){?>
												<th width='<?=$header[1]?>'><?=$header[0]?></th>
											<?php }?>
										</thead>
										<tbody>
											<tr><td colspan='<?=sizeof($tempHeaderAry)?>'>
											<?php if(!$isDraft && !$isSubmit){ ?>
												<div class="table_row_tool row_content_tool"><a class="add_dim" href="javascript:js_AddRow('<?=$i.'_'.$y?>', {})"></a></div>
											<?php }?>
											</td></tr>
										</tbody>
									</table>
								</div>
								<br style="clear:both;" />
							<?php }?>
						<?php }?>
						<input type='hidden' name='action' id='action' value="" />
						<div class="edit_bottom_v30">
							<?php if(!$isDraft && !$isSubmit){
								echo $DraftBtn;
								if($ECAReport['ReportID']){
									echo '&nbsp;&nbsp;';
									echo $SubmitBtn;
								}
							} else if($isDraft && !$isSubmit){
								echo $CancelSubmitBtn;
							}?>
						</div>
					<?}?>
				<?}?>
			</td>
		</tr>
	</table>
	
</form>

<?php
$linterface->LAYOUT_STOP();
?>