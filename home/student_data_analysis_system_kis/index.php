<?php
// using: 
/**
 * Change Log:
 * 2020-04-16 Philips - create this file
 */

#### Basic Include START ####
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_cees_$intranet_session_language.php");
#### Basic Include END ####


#### Module Include START #### 
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system_kis/libSDAS.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system/libPopupInterface.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/cust/analysis_system_lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."includes/json.php");
#### Module Include END #### 

#### Basic Access Right START ####
if(!$plugin['StudentDataAnalysisSystem'] || $plugin['StudentDataAnalysisSystem_Style'] != "tungwah" || !$plugin['SDAS_module']['KISMode']){
	No_Access_Right_Pop_Up('','/');
	exit();
}
#### Basic Access Right END ####

#### Init START ####
intranet_auth();
intranet_opendb();

// This cust module don't allow IE7 to open
$home_header_no_EmulateIE7 = true;
$objSDAS = new libSDAS();
if($t == ''){
	$accessRight = $objSDAS->getAssessmentStatReportAccessRight();
	/*
	if($sys_custom['SDAS']['CEES']['ExcludeSDASfunctions'] ){
		// for only monthly report
		if($accessRight['admin']){
			if( $objSDAS->isSuperAdmin()){
				$t = 'settings.assessmentStatReport.accessRightConfigAdmin';
			}else{
				// principal redirect to monthly report
				header('Location: /home/cees/monthlyreport');
				exit;
			}
		}else{
			// add new permission for monthly report here
			exit;
		}
	}else{
    */
		if($accessRight['admin']){
			$t = 'settings.assessmentStatReport.accessRightConfigAdmin';
		}else if($objSDAS->isAccessGroupMember()){
		    $rights = $objSDAS->getAccessGroupMemberRights();
		    $targetPage = $MODULE_OBJ['menu']['academic']['Child'][$rights[0]['AcademicStatisticName']][1];
		    $start = strrpos($targetPage, 't=') + 2;
		    $t = substr($targetPage, $start);
		}else{
			No_Access_Right_Pop_Up('','/');
			exit;
		}
	//}
}

$MODULE_OBJ = $objSDAS->GET_MODULE_OBJ_ARR();
//$t="management.to_cees.index";
$t = trim($t);
$user = new libuser($_SESSION['UserID']);

#### Init END ####

#### AccessRight START ####
$pageArr = explode('.', $t);

if(!$objSDAS->checkAccessRight($pageArr[0], $pageArr[1], $pageArr[2])){
// 	echo 'noAccessRight';
// 	No_Access_Right_Pop_Up('','/');
// 	exit;
}
#### AccessRight END ####


#### Routing START ####
// Prevent searching parent directories, like 'task=../../index'.
$t= str_replace("../", "", $t);
$t = str_replace('.','/',$t);
$mod_script = $t.'.php';
#### Routing END ####


#### Custom CSS START ####
$customCSS = '';
if($plugin['StudentDataAnalysisSystem_Style_User'][$user->UserLogin]){
	$customCSS .= "StudentAnalysisDataSystem/{$plugin['StudentDataAnalysisSystem_Style_User'][$user->UserLogin]}/cust_style.css";
}else if($plugin['StudentDataAnalysisSystem_Style']){
	$customCSS .= "StudentAnalysisDataSystem/{$plugin['StudentDataAnalysisSystem_Style']}/cust_style.css";
}
#### Custom CSS END ####


#### Custom jQuery START ####
// $customJQuery = 'jquery-1.8.0.min.js';
$module_custom['jQuery']['version'] = '1.12.4';
#### Custom jQuery END ####


#### Content START ####
$linterface = new libPopupInterface('noHeader_template.html');
$Msg = urldecode($Msg);

if(file_exists($mod_script)){
	include($mod_script);	
}else
{
	echo 'file not found<br/>';
}


intranet_closedb();
exit();
?>