<?php
/*
 * Editing by Paul
 * 
 * 2016-05-20 (Paul)
 * 		- add new logout mechanism for PowerFlip lite
 * 2016-03-02 (Paul)
 * 		- add new button "Manage" for powerflip to handle lesson plan
 * 2014-10-09 (Jason)
 * 		- add FromAndroidApp for temp use called from Andriod App but in web view access 
 */

$PATH_WRT_ROOT = "../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

# This page can be access by Android Only
if(!$sys_custom['power_lesson_app_web_view_all_platform'] && !$plugin['PowerFlip'] && !$plugin['PowerFlip_lite'] && (!$plugin['power_lesson'] || (isset($userBrowser) && $userBrowser->platform!='Andriod'))){
	header('location:/');
	die();
}

$fromAndroidApp = isset($fromAndroidApp) && $fromAndroidApp != '' ? $fromAndroidApp : '';


$UserCourseID = isset($UserCourseID) && $UserCourseID!=''? $UserCourseID:0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta content="no-cache" http-equiv="Cache-Control">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
		<?php if($plugin['PowerFlip'] || $plugin['PowerFlip_lite']){ ?>
		<title>eClass PowerFlip</title>
		<?php }else{ ?>
		<title>eClass PowerLesson</title>
		<?php } ?>	
		<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/<?=$LAYOUT_SKIN?>/css/powerlessonwebview.css"/>
		<script src="<?=$intranet_httppath?>/templates/script.js" language="JavaScript"></script>
		<script src="<?=$intranet_httppath?>/templates/jquery/jquery-1.8.3.min.js" language="JavaScript"></script>
		
		<?php if($plugin['PowerFlip'] || $plugin['PowerFlip_lite']){ ?>
			<script src="<?=$intranet_httppath?>/templates/jquery/jquery.fancybox.pack.js" language="JavaScript"></script>
			<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/jquery/jquery.fancybox.css" media="screen"/>
			<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/<?=$LAYOUT_SKIN?>/css/powerflipwebview.css"/>
		<?php } ?>
		
		<script>
			$(document).ready(function(){
				calculateHeight();
				getClassroomList(<?=$UserCourseID?>);
				$('#addLessonPlanBtn').fancybox({'padding' : 0});
			});
			
			$(window).resize(function(){calculateHeight();});
			
			function calculateHeight(){
				var ClassroomListHeight = $(window).height() - 50;
				$('#ClassroomList, #ClassroomListMsg').css('height', ClassroomListHeight);
				
				var LessonListHeight = $(window).height() - 35;
				$('#LessonList, #LessonListMsg').css('height', LessonListHeight);
			}
			
			function getClassroomList(DefaultClassroom){
				DefaultClassroom = DefaultClassroom || 0;
				
				displayClassroomListMsg('<?=$Lang['PowerLessonAppWeb']['Loading']?>');
				displayLessonListMsg('<?=$Lang['PowerLessonAppWeb']['Loading']?>');
				
				$.ajax({
					url		: 'getClassroomList.php',
					type	: 'POST',
					dataType: 'json',
					success : function(responseObj){
						var NoOfClassroom = responseObj.NoOfClassroom;
						
						$('#ClassroomList li').remove();
							
						if(NoOfClassroom>0){
							var ClassroomAry  = responseObj.Classrooms.Classroom;
							var htmlStr = '';

							<?php if($plugin['PowerFlip'] || $plugin['PowerFlip_lite']){ ?>
								var addLessonPlanClassroomHtmlStr = '';
							<?php } ?>
							
							for(var i=0;i<NoOfClassroom;i++){
								htmlStr += "<li id='UserCourseID_"+ClassroomAry[i].UserCourseID+"'>";
								htmlStr += "	<a href='javascript:void(0)' onclick='getLessonList("+i+", "+ClassroomAry[i].UserCourseID+", \""+ClassroomAry[i].Language+"\")'>";
								htmlStr += "		<div style='display:block; overflow:hidden; height:40px;'>" + ClassroomAry[i].ClassroomTitle + "</div>";
								htmlStr += "	</a>";
								htmlStr += "</li>";
								
								<?php if($plugin['PowerFlip'] || $plugin['PowerFlip_lite']){ ?>
									if(ClassroomAry[i].MemberType=='T'){
										addLessonPlanClassroomHtmlStr += "<option value='" + ClassroomAry[i].UserCourseID + "'>" + ClassroomAry[i].ClassroomTitle + "</option>";
									}
								<?php } ?>
							}
							
							$('#ClassroomList').html(htmlStr);

							<?php if($plugin['PowerFlip'] || $plugin['PowerFlip_lite']){ ?>
								if(addLessonPlanClassroomHtmlStr){
									$('#selectClassroomList').html(addLessonPlanClassroomHtmlStr);
									$('#manageLessonPlanBtnView').show();
									$('#addLessonPlanBtnView').show();									
								}
							<?php } ?>
							
							if(DefaultClassroom)
								$('#UserCourseID_'+DefaultClassroom+' a').click();
							else
								$('#ClassroomList li:first a').click();
							
							hideClassroomListMsg();
						}
						else{
							displayClassroomListMsg('<?=$Lang['PowerLessonAppWeb']['NoClassroom']?>');
							displayLessonListMsg('<?=$Lang['PowerLessonAppWeb']['NoLesson']?>');
						}
					}
				});
			}
			
			function getLessonList(btnIndex, UserCourseID, DisplayLang){
				$('#ClassroomList li').removeClass('selected');
				$('#ClassroomList li:nth-child('+(btnIndex+1)+')').addClass('selected');
				
				displayLessonListMsg('<?=$Lang['PowerLessonAppWeb']['Loading']?>');

				<?php if($plugin['PowerFlip'] || $plugin['PowerFlip_lite']){ ?>
					$('#selectClassroomList').val(UserCourseID);
				<?php } ?>
				
				$.ajax({
					url		: 'getLessonList.php',
					type	: 'POST',
					data	: {'UserCourseID' : UserCourseID, 'DisplayLang' : DisplayLang},
					dataType: 'json',
					success : function(responseObj){
						var NoOfLesson = responseObj.NoOfLesson;
						
						$('#LessonList li').remove();
						
						if(NoOfLesson>0){
							var LessonAry  = responseObj.Lessons.Lesson;
							var htmlStr = '';
							
							for(var i=0;i<NoOfLesson;i++){
								var LessonStatusClass = '';
								
								switch(LessonAry[i].LessonStatus){
									case '1': LessonStatusClass = 'status_on'; break;
									case '2': LessonStatusClass = 'status_pass'; break;
									default: LessonStatusClass = '';
								}
								
								var InProgress = LessonAry[i].LessonStatus==1? 1:0;
								var LessonID   = InProgress==1? LessonAry[i].LessonSession : LessonAry[i].LessonID;
								
								htmlStr += "<li class='" + LessonStatusClass + "'>";
								htmlStr += "	<a href='javascript:void(0)' onclick='goLessonPlan(" + UserCourseID + ", \"" + LessonID + "\", " + InProgress + ")'>";
                    			htmlStr += "		<h1>";
                    			htmlStr += "			<span>" + LessonAry[i].LessonTitle + "</span>";
                    			htmlStr += "			<em>" + LessonAry[i].LessonDesc + "</em>";
                    			htmlStr += "		</h1>";
                    			htmlStr += "		<ul>";
                    			htmlStr += "			<li>" + LessonAry[i].LessonTeacher + "</li>";
                    			htmlStr += "		</ul>";
                    			htmlStr += "	</a>";
                    			htmlStr += "</li>";
							}
							
							$('#LessonList').html(htmlStr);
							
							hideLessonListMsg();
						}
						else{
							displayLessonListMsg('<?=$Lang['PowerLessonAppWeb']['NoLesson']?>');
						}
					}
				});
			}
			
			function displayClassroomListMsg(message){
				$('#ClassroomList').hide();
				$('#ClassroomListMsg').show();
				$('#ClassroomListMsgContent').html(message);
			}
			
			function hideClassroomListMsg(){
				$('#ClassroomList').show();
				$('#ClassroomListMsg').hide();
			}
			
			function displayLessonListMsg(message){
				$('#LessonList').hide();
				$('#LessonListMsg').show();
				$('#LessonListMsgContent').html(message);
			}
			
			function hideLessonListMsg(){
				$('#LessonList').show();
				$('#LessonListMsg').hide();
			}
			
			function reloadAll(){
				getClassroomList(0);
			}
			
			function logout(){
				<?php if($plugin['PowerFlip_lite'] || $plugin['PowerFlip']){ 
					if(!$plugin['PowerFlip_lite']){?>
						window.close();
					<?	
					}else{
					?>
						var obj = document.form1;
						
						obj.action = '/logout.php';
						
						obj.target_url.value 	= '/pf/index.php';
						obj.targetAction.value	= '';
						obj.UserCourseID.value 	= '';
						obj.LessonID.value 		= '';
						obj.InProgress.value 	= '';
						
						obj.submit();
					<?
					}
					?>
				<?php } else { ?>
					var obj = document.form1;
					
					obj.action = '/logout.php';
					
					obj.target_url.value 	= '/pl/index.php';
					obj.targetAction.value	= '';
					obj.UserCourseID.value 	= '';
					obj.LessonID.value 		= '';
					obj.InProgress.value 	= '';
					
					obj.submit();
				<?php } ?>
			}
			
			function goLessonPlan(UserCourseID, LessonID, InProgress){
				var obj = document.form1;
				
				obj.action = 'login.php';
				
				obj.target_url.value 	= '';
				obj.targetAction.value	= 'goLessonPlan';
				obj.UserCourseID.value 	= UserCourseID;
				obj.LessonID.value 		= LessonID;
				obj.InProgress.value 	= InProgress;
				
				obj.submit();
			}

			<?php if($plugin['PowerFlip'] || $plugin['PowerFlip_lite']){ ?>
				function addLessonPlan(UserCourseID){
					var obj = document.form1;
					
					obj.action = 'login.php';
					
					obj.target_url.value 	= '';
					obj.targetAction.value	= 'addLessonPlan';
					obj.UserCourseID.value 	= UserCourseID;
					obj.LessonID.value 		= '';
					obj.InProgress.value 	= '';
					
					obj.submit();
				}
				function manageLessonPlan(){
					var UserCourseID = $('#selectClassroomList').val();
					var obj = document.form1;
					
					obj.action = 'login.php';
					
					obj.target_url.value 	= '';
					obj.targetAction.value	= 'manageLessonPlan';
					obj.UserCourseID.value 	= UserCourseID;
					obj.LessonID.value 		= '';
					obj.InProgress.value 	= '';
					
					obj.submit();
				}
			<?php } ?>
		</script>
	</head>
	<body class="pl_list">
		<div id="outerbox">
     		<div id="innerbox">
     			<div class="list_board">
		    		<div class="left_sub_list">
		            	<ul id="ClassroomList" style="height:446px; overflow:auto; display:none"></ul>
		            	<table id="ClassroomListMsg" style="height:446px; width:100%">
		            		<tr>
		            			<td id="ClassroomListMsgContent" align="center" style="color:#ab300b;"><?=$Lang['PowerLessonAppWeb']['Loading']?></td>
		            		</tr>
		            	</table>
		            </div>
            		<div class="list_item">
						<ul id="LessonList" style="height:461px; overflow:auto; display:none"></ul>
		            	<table id="LessonListMsg" style="height:461px; width:100%">
		            		<tr>
		            			<td id="LessonListMsgContent" align="center" style="color:#626262;"><?=$Lang['PowerLessonAppWeb']['Loading']?></td>
		            		</tr>
		            	</table>
					</div>
					<div class="list_header">
						<a href="javascript:void(0)" onclick="logout()" class="btn_logout" id="logout_btn"></a>
						<a href="javascript:void(0)" onclick="reloadAll()" class="btn_reload"></a>
						<?php if($plugin['PowerFlip'] || $plugin['PowerFlip_lite']){ ?>
							<div id="addLessonPlanBtnView" style="display:none">
								<span></span>
								<a id="addLessonPlanBtn" href="#selectClassroom" class="btn_add_lessonplan"><?=$Lang['PowerLessonAppWeb']['AddLessonPlan']?></a>
							</div>
							<div id="manageLessonPlanBtnView" style="display:none">
								<span></span>
								<a id="manageLessonPlanBtn" href="#" class="btn_manage" onclick="javascript:manageLessonPlan()"><?=$Lang['PowerLessonAppWeb']['ManageLessonPlan']?></a>
							</div>
						<?php } else { ?>
							<?php if($fromAndroidApp!=-1){ ?>
								<span></span>
								<a class="btn_intranet" href="/home/index.php"><?=$Lang['PowerLessonAppWeb']['GoToIntranet']?></a>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php if($plugin['PowerFlip'] || $plugin['PowerFlip_lite']){ ?>
			<div style="display:none">
				<div id="selectClassroom">
					<div class="pop_board" style="margin:0; min-width:500px">
						<div class="board_top">
	        				<div class="board_title"><?=$Lang['PowerLessonAppWeb']['AddLessonPlan']?></div>
	    				</div>
		  				<div>
			   				<span class="board_main_center">
			   					<span class="note_simple"><?=$Lang['PowerLessonAppWeb']['PleaseSelectClassroom']?></span>
			    				<select id="selectClassroomList" style="margin:20px"></select>
			    			</span>
			    			<br><br>
							<div class="board_bottom_button">
								<p class="spacer"></p>
								<input name="submit" type="button" class="formbutton btn_common" onclick="addLessonPlan($('#selectClassroomList').val())" value="<?=$Lang['Btn']['Select']?>">
								<p class="spacer"></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		<form id="form1" name="form1" method="POST">
			<input type="hidden" id="target_url" name="target_url" value="">
			<input type="hidden" id="targetAction" name="targetAction" value="">
			<input type="hidden" id="UserCourseID" name="UserCourseID" value="">
			<input type="hidden" id="LessonID" name="LessonID" value="">
			<input type="hidden" id="InProgress" name="InProgress" value="">
		</form>
	</body>
</html>
