<?php
# using :

$PATH_WRT_ROOT = "../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libmethod.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

# This page can be access by Android Only
if(!$sys_custom['power_lesson_app_web_view_all_platform'] && !$plugin['PowerFlip'] && !$plugin['PowerFlip_lite'] && (!$plugin['power_lesson'] || (isset($userBrowser) && $userBrowser->platform!='Andriod'))){
	die();
}

# initialize object
$method = new LibMethod();
$json   = new JSON_obj();

$ParArr['eClassRequest']['Request']['UserCourseID'] = $UserCourseID;
$ParArr['eClassRequest']['Request']['DisplayLang']  = $DisplayLang;

$ClassroomResult = $method->callMethod("AppGetLesson", $ParArr);

# Transform the $ClassroomResult to json string
$json_str = $json->encode($ClassroomResult);

echo $json_str;

intranet_closedb();
?>
