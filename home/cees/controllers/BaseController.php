<?php
// Editing by 

abstract class BaseController
{
	const FLASH_ATTRIBUTES = 'FLASH_ATTRIBUTES';
	
	/*
	 * Usage of request mapping: 
	 * $this->RequestMapping['uri_regex_pattern'] = array('function'=>'controller method name','auth'=>'controller authentication method name','pathVars'=>array());
	 * uri_regex_pattern : absolute uri path that supports regular expression, where the first uri section is used to map to controller
	 * e.g. '/module1/section1/function1/record/(\d+)' module1 would be the expected controller, if that controller not exist, all requests would be delegated to IndexController. 
	 * the (\d+) would match the record id, and would pass as arguments to the controller method and temporary stored in 'pathVars'(no need to set it, router would auto map it).
	 * 'auth' is optional, if not set, would use default authenticate() method.  
	 */
	protected $RequestMapping = array(); // child classes need inherit this attribute
	/*
	 * ObjectRepository is used store class instances that avoid same classe created repeatedly. 
	 */
	protected $ObjectRepository = array(); 
	
	public function __construct()
	{
		global $_PAGE;
		if(isset($_SESSION[self::FLASH_ATTRIBUTES]) && count($_SESSION[self::FLASH_ATTRIBUTES])>0){
			foreach($_SESSION[self::FLASH_ATTRIBUTES] as $key => $val){
				$_PAGE[$key] = $val;
			}
		}
		unset($_SESSION[self::FLASH_ATTRIBUTES]);
	}
	
	public function __destruct()
	{
		global $_PAGE;
		unset($_PAGE);
	}
	
	/*
	 * Usage of flash attributes
	 * Flash attributes are used to pass temporary data across pages, e.g. pass error code from update page to submit page to indicate result.
	 * Flash attributes are copied from session to page scope variable $_PAGE automatically when controller is created.  
	 */
	public function setFlashAttribute($key, $value)
	{
		if(!isset($_SESSION[self::FLASH_ATTRIBUTES])){
			$_SESSION[self::FLASH_ATTRIBUTES] = array();
		}
		$_SESSION[self::FLASH_ATTRIBUTES][$key] = $value;
		//debug_pr($_SESSION);
	}
	
	public function getFlashAttribute($key)
	{
		global $_PAGE;
		if(isset($_PAGE[self::FLASH_ATTRIBUTES]) && isset($_PAGE[self::FLASH_ATTRIBUTES][$key])){
			return $_PAGE[self::FLASH_ATTRIBUTES][$key];
		}
		return '';
	}
	
	public function findRequestMapping($uri)
	{
		if(isset($this->RequestMapping[$uri])){
			$mapping = $this->RequestMapping[$uri];
			$mapping['uri'] = $uri; 
			return $mapping;
		}else if(count($this->RequestMapping)>0){
			
			foreach($this->RequestMapping as $key => $val)
			{
				if(preg_match('{^'.$key.'$}', $uri, $matches)){
					$mapping = $this->RequestMapping[$key];
					$mapping['uri'] = $key; 
					if(count($matches)>1){
						array_shift($matches);
						$mapping['pathVars'] = $matches;
					}
					
					return $mapping;
				}
			}
		}
		
		return array();
	}
	
	public function getRequestMapping()
	{
		return $this->RequestMapping;
	}
	
	public function getViewBaseDir()
	{
		return '/home/cees/views';
	}
	
	// include a php page that inside the base view dir and output as is
	public function includeView($view_path)
	{
		global $intranet_root,$Lang,$_PAGE; 
		
		if(file_exists($intranet_root.$this->getViewBaseDir().$view_path)){
			include($intranet_root.$this->getViewBaseDir().$view_path);
		}
	}
	
	// get the content from a php view page but do not output
	public function getViewContent($view_path, $replaceMap=array())
	{
		global $Lang,$_PAGE;
		
		ob_start();
		$this->includeView($view_path);
		$content = ob_get_clean();
		
		if(count($replaceMap)>0){
			foreach($replaceMap as $placeholder => $replace_content)
			{
				$content = str_replace($placeholder, $replace_content, $content);
			}
		}
		
		return $content;
	}
	
	// this is the overall layout with sidebar view optionally provided
	public function getLayout($mainViewContent, $sidebarViewContent='')
	{
		$replaceViews = array();
		$header = $this->getViewContent('/header.php', array());
		$footer = $this->getViewContent('/footer.php', array());
		//$sidebar = $this->getViewContent('/sidebar.php', array());
		$sidebar = $sidebarViewContent;
		$main = $this->getViewContent('/main.php', array('<!--MAIN-->'=>$mainViewContent));
		
		$replaceViews['<!--HEADER-->'] = $header;
		$replaceViews['<!--MAIN-->'] = $main;
		$replaceViews['<!--FOOTER-->'] = $footer;
		$replaceViews['<!--SIDEBAR-->'] = $sidebar;
		
		$view = $this->getViewContent('/template.php', $replaceViews);
		return $view;
	}
	
	// redirect to a page
	public function sendRedirect($url)
	{
		header("Location: ".$url);
		exit;
	}
	
	public function requestObject($className, $includePath, $params=array())
	{
		global $intranet_root, $PATH_WRT_ROOT;
		
		$object_key = $className;
		if(count($params)>0){
			$object_key = $className.md5(serialize($params));
		}
		
		if(isset($this->ObjectRepository[$object_key])){
			return $this->ObjectRepository[$object_key];
		}else {
			$includePath = ltrim($includePath,'/');
			include_once($intranet_root.'/'.$includePath);
			
			if(count($params)>0){
				$clazz = new ReflectionClass($className);
				$this->ObjectRepository[$object_key] = $clazz->newInstanceArgs($params);
			}else{
				$this->ObjectRepository[$object_key] = new $className();
			}
			return $this->ObjectRepository[$object_key];
		}
	}
	
	public function index()
	{
		debug_pr('base->index');
		die();
	}
	
	public function authenticate()
	{
		return true;
	}
}