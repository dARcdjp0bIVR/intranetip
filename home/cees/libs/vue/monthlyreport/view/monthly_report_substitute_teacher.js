Vue.component('substitute-teacher', {
    template: '\
    <table class="table report-style" id="tbl_other" width="100%">\
    <colgroup>\
        <col style="width: 3%">\
        <col style="width: 28%">\
        <col style="width: 28%">\
        <col style="width: 12%">\
        <col style="width: 23%">\
    </colgroup>\
    <thead>\
    <tr>\
	    <th style="text-align: left;">#</th>\
	    <th style="text-align: left;">'+jsLang.SubstituteTeacher+'</th>\
	    <th style="text-align: left;">'+jsLang.SubstitutionDate +'</th>\
	    <th style="text-align: left;">'+ jsLang.FacilitateVacancy+'</th>\
	    <th style="text-align: left;">'+ jsLang.SubstitutedName+'</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td style="text-align: left;">{{ key + 1 }}</td>\
            <td style="text-align: left;">\
    			{{content.teachername}}\
            </td>\
		    <td style="text-align: left;">\
    	        {{content.date}}\
		    </td>\
            <td class="text-center" style="text-align: left;"><i v-if="content.termination==true" class="fa fa-check"></i></td>\
    	 	<td style="text-align: left;">\
    			{{content.teacherId | getTeacherName(teachers)}}\
        	</td>\
        </tr>\
    </tbody>\
    </table>\
    ',
    data: function () {
        return {
            contents: formData.SubstituteTeacher.slice(),
            teachers: this.$root.teachers
        }
    },
    filters: {
        getTeacherName: function (teacherid, teachers) {
            return teachers[teacherid];
        }
    }
});