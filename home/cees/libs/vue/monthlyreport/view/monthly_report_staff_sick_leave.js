Vue.component('teacher-sick-leave', {
    // language=HTML
    template: '\
    <table class="table report-style" id="tbl_other" width="100%">\
    <colgroup>\
	    <col style="width: 3%">\
	    <col style="width: 32%">\
	    <col style="width: 18%">\
	    <col style="width: 23%">\
	    <col style="width: 20%">\
    </colgroup>\
    <thead>\
    <tr>\
	    <th style="text-align: left;">#</th>\
	    <th style="text-align: left;">'+jsLang.StaffName +'</th>\
        <th style="text-align: left;">'+jsLang.Absent +'</th>\
        <th style="text-align: left;">'+jsLang.Certificate +'</th>\
        <th style="text-align: left;">'+jsLang.AbsentSeptember +'</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td style="text-align: left;">{{ key + 1 }}</td>\
            <td style="text-align: left;">\
    			 {{content.teacherName}}\
            </td>\
            <td style="text-align: left;">\
                {{content.thisMonthDay}}\
            </td>\
            <td style="text-align: left;">\
                {{content.WithCer}}\
            </td>\
            <td style="text-align: left;">\
                <div v-if="content.lastMonthDay == \'--\' ">\
                    {{content.thisMonthDay}}\
                </div>\
                <div v-else>\
                   {{content.lastMonthDay +content.thisMonthDay}}\
                </div>\
            </td>\
        </tr>\
    </tbody>\
    </table>\
    ',
    data: function () {
        return {
            contents: formData.TeacherSickLeave.slice(),
            teachers: this.$root.teachers,
            sickleaveday: this.$root.sickleaveday
        }
    },
    filters: {
        // getTeacherName: function (teacherid, teachers) {
        //     return teachers[teacherid];
        // }
    }
});