Vue.component('school-awards', {
    template: '\
    <table class="table report-style" id="tbl_other" width="100%">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 93%">\
    </colgroup>\
    <thead>\
    <tr>\
    <th style="text-align: left;">#</th>\
    <th style="text-align: left;">'+jsLang.Award+'</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td style="text-align: left;">{{ key + 1 }}</td>\
            <td style="text-align: left;">\
                {{content.text}}\
            </td>\
        </tr>\
    </tbody>\
    </table>\
    ',
    data: function () {
        return {
            contents: formData.SchoolAwards.slice()
        }
    }
});