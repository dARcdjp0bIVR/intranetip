Vue.component('contentPopup',{
    template:'\
    <div class="popup-window panel panel-default" style="position: absolute">\
        <button class="popup-close">X</button>\
        <table class="popup-table">\
         <thead>\
            <th class="popup-header">' + jsLang.CodeIndex + '</th>\
            <th class="popup-header">{{title}}</th>\
         </thead>\
         <tbody>\
            <tr v-for="(item, index) in content" class="popup-row">\
                <td class="popup-index">{{index}}</td>\
                <td class="popup-data">{{item}}</td>\
            </tr>\
         </tbody>\
        </table>\
    </div>\
    ',
    props: ['title','content'],
    data: function(){
        return{
        };
     },
    mounted: function(){
        $('.popup-window').hide();
        $('.popup-show').click(function(){
            var popup = $(this).next();
            var position = $(this).position();
            popup.css('top', position.top + "px");
            popup.css("left",position.left + "px");
            $('.popup-window').hide();
            $(this).next().show();
        })
        $('.popup-close').click(function(){
            $(this).parent().hide();
        });

    }
});
Vue.component('importStructure', {
    template: '\
               <div>\
                   <span class="importInstruction">' + " " + '</span>\
                   <ul>\
                        <li v-for="(item, index) in this.columns">\
                            {{Column}} {{colIndex(index)}} : {{item}} \
                            <div v-if="pContent != null" class="popup-div">\
                                <button v-if="pContent[index] != null" class="popup-show btn-info btn-blue">?</button>\
                                <contentPopup v-if="pContent[index] != null" :title="pContent[index].title" :content="pContent[index].content"></contentPopup>\
                            </div>\
                            <span v-if="reminder[index] != null" style="color: #999999">\
                                ({{reminder[index]}})\
                            </span>\
                        </li>\
                   </ul>\
               </div>\
    ',
    props: ['columns','pContent', 'reminder'],
    methods : {
    },
    data: function(){
        return {
            Column : jsLang.Column,
            colIndex : function(num){
                var A = 65;
                if(num <=25 ){
                    return String.fromCharCode(A + num);
                } else {
                    var front = Math.floor(num / 26);
                    var rear = num % 26;
                    return String.fromCharCode(65+front) + String.fromCharCode(65+rear);
                }
            },
        }
    },
});
Vue.component('section-item', {
    template: '\
    <div>\
        <div class="section-divider"></div>\
        <div class="component">\
          <div class="section-title">\
            <ol type="I" :start="id">\
             <li><span>{{ title }}</span></li>\
            </ol>\
           </div>\
          <div class="section-body">\
          <div :is="type" :ref="type"></div>\
          </div>\
        </div>\
    </div>\
  ',
    props: ['id', 'title', 'type']
});
//    <div id="id"  class="modal fade move-folder" tabindex="-1" role="dialog" aria-labelledby="divId" style="display: none;" aria-hidden="true">
//  <div id="id"  role="dialog" aria-labelledby="divId">
Vue.component('import-thickbox',{
        template:'\
     <div :id="this.id" class="modal fade move-folder" tabindex="-1" role="dialog" :aria-labelledby="divId" style="display: none;" aria-hidden="true">\
            <div class="modal-dialog" role="document">\
                <div class="modal-content">\
                    <div class="modal-header">\
                      <div class="modal-title" :id="divId">{{title}}</div>\
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                        <span aria-hidden="true">×</span>\
                      </button>\
                    </div>\
                    <div :is="id"></div>\
                    <div \
                        <div class="submission-area">\
                          <button class="btn btn-blue" data-dismiss="modal" aria-label="Close" @click="confirm">'+jsLang.Confirm+'</button>\
                          <button class="btn btn-secondary btn-cancel" data-dismiss="modal" aria-label="Close">'+jsLang.Cancel+'</button>\
                        </div>\
                    </div>\
            </div>\
       </div>\
        ',
    mixins: [myMixin],
        props: ['id', 'title','method'],
        methods: {
            confirm: function(e) {
                eventHub.$emit('confirm-info-'+this.id);
            }
        },

        computed: {
            divId: function () {
                return this.id + 'ModalLabel';
            }
        },
        mounted: function () {

         }
    }
);

// thickbox2
Vue.component('import-thickbox2',{
        template:'\
     <div :id="this.id" class="modal fade move-folder" tabindex="-1" role="dialog" :aria-labelledby="divId" style="display: none;" aria-hidden="true">\
            <div class="modal-dialog" role="document">\
                <div class="modal-content">\
                    <div class="modal-header">\
                      <div class="modal-title" :id="divId">{{title}}</div>\
                      <button type="button" class="close" @click="close" data-dismiss="modal" aria-label="Close">\
                        <span aria-hidden="true">×</span>\
                      </button>\
                    </div>\
                        <div :is="bodyId"></div>\
                        <div class="submission-area">\
                          <button class="btn btn-blue btn-confirm" aria-label="Close" @click="confirm">'+jsLang.Confirm+'</button>\
                          <button class="btn btn-blue btn-prev" aria-label="Close" @click="close">'+ jsLang.PrevPage +'</button>\
                          <button class="btn btn-secondary btn-cancel" @click="close" data-dismiss="modal" aria-label="Close">'+jsLang.Cancel+'</button>\
                        </div>\
                    </div>\
            </div>\
       </div>\
        ',
        mixins: [myMixin],
        props: ['id', 'title','method'],
        data: function(){
            return {
                bodyId : this.$props.id
            }
        },
        methods: {
            confirm: function(e) {
                var self = this;
                if(self.bodyId == self.$props.id) {
                    eventHub.$emit('confirm-info-' + this.id);
                    eventHub.$on('confirm-table-' + this.id, function () {
                        self.bodyId = self.$props.id.replace('Modal', '') + "ConfirmTable";
                    });
                } else {
                    eventHub.$emit('confirm-data-' + this.id);
                }
            },
            close: function(e){
                var self = this;
                this.bodyId = this.id;
                $('button.btn-prev').hide();
                $('button.btn-confirm').show();
                this.$nextTick(function(){
                    $('button.btn-confirm').removeAttr('data-dismiss');
                });
            }
        },

        computed: {
            divId: function () {
                return this.id + 'ModalLabel';
            }
        },
        mounted: function () {
            $('button.btn-prev').hide();
            eventHub.$on('close-'+this.id, this.close);
        }
    }
);
// thickbox2

new Vue({
    el: '#edit-data',
    data: {
        teachers: [],
        categories:[],
        // sections: this.showsections,
        retrieves:[
            {
                id: 'sickLeaveModal',
                name: jsLang.RetrieveFromStaffAttendance,
                method: 'GetStaffSickLeaveMonthlyData'
            },
            {
                id: 'stdEnrolmentModal',
                name: jsLang.RetrieveFromStudentAttendance
            },
            {
                id: 'extraActivitiesInternalModal',
                name: jsLang.RetrieveFromEnrolment
            },
            {
                id: 'extraActivitiesExternalModal',
                name: jsLang.RetrieveFromEnrolment
            },
        ],
        imports: [
            {
                id: 'studentECAModal',
                name: jsLang.RetrieveFromCSV,
                method: ""
            },
            {
                id: 'schoolAwardModal',
                name: jsLang.RetrieveFromCSV,
                method: ""
            },
            {
                id: 'advancedNoticeModal',
                name: jsLang.RetrieveFromCSV,
                method: ""
            },
            {
                id: 'othersReportModal',
                name: jsLang.RetrieveFromCSV,
                method: ""
            },
            {
                id: 'studentEnrollModal',
                name: jsLang.RetrieveFromCSV,
                method: ""
            },
            {
                id: 'visitInspectionModal',
                name: jsLang.RetrieveFromCSV,
                method: ""
            },
            {
                id: 'schoolAdministrationModal',
                name: jsLang.RetrieveFromCSV,
                method: ""
            },
            {
                id: 'teacherCourseModal',
                name: jsLang.RetrieveFromCSV,
                method: ""
            },
            {
                id: 'sickLeaveCSVModal',
                name: jsLang.RetrieveFromCSV,
                method: ''
            },
            {
                id: 'appointmentModal',
                name: jsLang.RetrieveFromCSV,
                method: ''
            },
            {
                id: 'terminationModal',
                name: jsLang.RetrieveFromCSV,
                method: ''
            },
            {
                id: 'substituteTeacherModal',
                name: jsLang.RetrieveFromCSV,
                method: ''
            },

        ]
    },
    computed:{
        sections : function() {
            // var showSections = [
            //     {
            //         title: jsLang.Appoiontment,
            //         type: 'appointment-termination',
            //     },
            // {
            //     title: jsLang.Substitute,
            //     type: 'substitute-teacher',
            // },
            // {
            //     title: jsLang.SickLeave,
            //     type: 'teacher-sick-leave',
            // },
            // {
            //     title: jsLang.StudentEnrol,
            //     type: 'student_enrolment',
            // },
            // {
            //     title: jsLang.Schoolvisit,
            //     type: 'school-visit',
            // },
            // {
            //     title: jsLang.Schooladministration,
            //     type: 'school-administration',
            // },
            // {
            //     title: jsLang.StaffCourse,
            //     type: 'teacher-course',
            // },
            // {
            //     title: jsLang.Studenteca,
            //     type: 'student-eca',
            // },
            // {
            //     title: jsLang.SchoolAwards,
            //     type: 'school-awards',
            // },
            // {
            //     title: jsLang.AdvancedNotice,
            //     type: 'advanced-notice',
            // },
            //     {
            //         title: jsLang.Others,
            //         type: 'others',
            //     }
            // ];

            var reportID = document.getElementById("reportID").value;
            var yearid = document.getElementById("reportYear").value;
            var monthid = document.getElementById("reportMonth").value;

            var showsections = $.ajax({
                type: 'POST',
                url: '/home/cees/monthlyreport/ajaxGetDisplaySection',
                data: {
                    reportId: reportID,
                    year: yearid,
                    month: monthid
                },
                async: false,
                success: function (data) {

                    return data;
                }
            });

            var SectionsList = $.parseJSON(showsections.responseText);
            var SectionsAry = [];
            var i = 0;
            SectionsList.forEach(function (element) {
                var nametest = String(element.title);
                SectionsAry.push({
                    title: jsLang[nametest],
                    type: element.type
                });

            });
            return SectionsAry;
        }
    },
    methods: {
        submit: function () {
            eventHub.$emit('collect-info');
            var Supervisor = document.getElementById("inputSupervisorName").value;
            var reportID = document.getElementById("reportID").value;
            var yearid = document.getElementById("reportYear").value;
            var monthid = document.getElementById("reportMonth").value;
            $.ajax({
                type: 'POST',
                url: '/home/cees/monthlyreport/update',
                data: {
                    reportId: reportID,
                    year: yearid,
                    month: monthid,
                    reportData: submitForm,
                    supervisorName: Supervisor,
                    DraftStatus: '0'
                },
                success: function (data) {
                   window.location.href = "/home/cees/monthlyreport/";
                }
            });
        },
        draft: function () {
            eventHub.$emit('collect-info');
            var Supervisor = document.getElementById("inputSupervisorName").value;
            var reportID = document.getElementById("reportID").value;
            var yearid = document.getElementById("reportYear").value;
            var monthid = document.getElementById("reportMonth").value;
            $.ajax({
                type: 'POST',
                url: '/home/cees/monthlyreport/update',
                data: {
                    reportId: reportID,
                    year: yearid,
                    month: monthid,
                    reportData: submitForm,
                    supervisorName: Supervisor,
                    DraftStatus: '1'
                },
                //  dataType: 'json',
                success: function (data) {
                    window.location.href = "/home/cees/monthlyreport/";
                }
            });
        },
        cancelsubmit: function () {
            window.location.href = "/home/cees/monthlyreport/";
        }
    },
    created: function () {
        var TeacherList = $.parseJSON(TeacherListJson);
        this.teachers = TeacherList;

        this.positions = {};
        for(n = 1; n<=161; n++ ){
            this.positions[n] = jsLang.PositionName[n];
        }

        var OLECategoryList = $.parseJSON(OLECategoryJson);

        this.categories = OLECategoryList;

        this.contracttypes = {1:jsLang.Permanent, 2:jsLang.Establishment, 3:jsLang.Funding, 4:jsLang.Contract};
        this.terminationreasons = {1:jsLang.Expired, 2:jsLang.Resign, 3:jsLang.Retire, 4:jsLang.Termination};
        this.errorLang = jsLang.ErrorLang;
        // Teacher Array Here
        this.teacherArray = {};
        for(var i in this.teachers){
            if(this.teachers[i] !== undefined && this.teachers[i] != null){
                for(var x in this.teachers[i]){
                    if(this.teachers[i][x] !== undefined){
                        this.teacherArray[this.teachers[i][x].id] = this.teachers[i][x].name;
                    }
                }
            }
        }
        // Categories Array Here
        this.categoriesArray = {};
        for(var x in this.categories){
            this.categoriesArray[this.categories[x].id] = this.categories[x].name;
        }
       // this.ecacategories = {1:jsLang.Expired, 2:jsLang.Resign, 3:jsLang.Retire};
        this.ecaToggle = true;
        var self = this;
        eventHub.$on('toggle-ECAModal-internal', function(){
            self.ecaToggle = true;
        });
        eventHub.$on('toggle-ECAModal-external', function(){
            self.ecaToggle = false;
        });

        this.confirmTableTemplate = '\
                                       <div class="modal-body">\
                                           <div class="browse-folder-container">\
                                                <table class="table">\
                                                    <thead>\
                                                        <th class=" rowIndex" style="padding: 0;">#</th>\
                                                            <th class="" v-for="item in result.columns">\
                                                            {{item}}\
                                                            </th>\
                                                        <th class=" rowRemark">'+jsLang.Error+'</th>\
                                                    </thead>\
                                                    <tbody>\
                                                        <tr class="" v-for="(row, index) in result.data">\
                                                            <td class=" rowIndex">{{index+1}}</td>\
                                                            <td class="" v-for="(field,colNum) in row">\
                                                                <span v-if="result.checkResult[index][colNum]==1">\
                                                                {{field}}\
                                                                </span>\
                                                                <span v-else style="color: red" class="errorField">\
                                                                {{field}}\
                                                                </span>\
                                                            </td>\
                                                            <td class=" rowRemark">\
                                                                <ul v-if="result.rowResult[index] != 1" class="errorList">\
                                                                    <li v-for="(err, colNum) in result.checkResult[index]">\
                                                                        <span v-if="err instanceof Object">\
                                                                            {{result.columns[colNum]}} &gt;&gt;\
                                                                            <b v-for="(remark, key) in err" v-if="remark!=1">\
                                                                                {{key}} : {{errorLang[remark]}}\
                                                                            </b>\
                                                                        </span>\
                                                                        <span v-else-if="err != 1">\
                                                                            {{result.columns[colNum]}} &gt;&gt; {{errorLang[err]}}\
                                                                        </span>\
                                                                        <span v-else>\
                                                                        </span>\
                                                                    </li>\
                                                                </ul>\
                                                            </td>\
                                                        </tr>\
                                                    </tbody>\
                                                </table>\
                                                <div v-if="error > 0" class="errorTotal">\
                                                    {{TotalError}} : {{error}}\
                                                </div>\
                                              </div>\
                                          </div>\
                                          ';
    },
    mounted: function () {

    }
});