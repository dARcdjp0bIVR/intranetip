Vue.component('substitute-teacher', {
    template: '\
    <table class="table report-style" id="tbl_other">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 28%">\
    <col style="width: 28%">\
    <col style="width: 12%">\
    <col style="width: 23%">\
    <col style="width: 4%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="6">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#substituteTeacherModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
	    <th>#</th>\
	     <th>'+jsLang.SubstituteTeacher+'</th>\
	     <th>'+jsLang.SubstitutionDate +'</th>\
	     <th>'+jsLang.FacilitateVacancy +'</th>\
         <th>'+ jsLang.SubstitutedName+'</th>\
	    <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td>{{ key + 1 }}</td>\
            <td>\
    			<input type="text" class="form-control" id="\'inputTeachername\'+ key" v-model="content.teachername" v-bind:type="\'substitute_teacher\'">\
            </td>\
		    <td>\
    			<div is="multiple-date-select" v-bind:id="key" v-bind:type="\'substitute_teacher\'" v-bind:sourceKey="\'date\'">\
    			</div>\
		    </td>\
		    <td>\
                <div class="form-check form-check-inline text-center">\
                    <label class="form-check-label">\
                        <input class="form-check-input" type="checkbox" id="\'inlineCheckbox2\' + key" v-model="content.termination"><span>&nbsp;</span>\
                     </label>\
                </div>\
            </td>\
    	 	<td>\
    			<div is="teacher-select" v-bind:id="key" v-bind:type="\'substitute_teacher\'">\
        		</div>\
        	</td>\
            <td>\
            <div is="del-btn" v-bind:remove="remove" v-bind:target="key" v-bind:location="contents">\
            </div>\
            </td>\
        </tr>\
        <tr is="add-row" v-bind:add="add"></tr>\
    </tbody>\
    </table>\
    ',
    mixins: [myMixin],
    data: function () {
        return {
            contents: formData.SubstituteTeacher.slice(),
            row: {
                teachername: '',
                date: '',
                termination:'',
                teacherId: ''
            },
            teachers: this.$root.teachers,
            teacherArray: this.$root.teacherArray
        }
    },
    methods: {
        add: function (e) {
            // this.addNewRows(e.target.value, this.row, this.contents);
        },
        remove: function (key) {
            this.removeRow(key, this.contents)
        },
        assign: function () {
            //  console.log('teacher-course is assigning');
            submitForm.SubstituteTeacher = this.contents;
        },
        confirmData: function(){
            var self = this;
            this.confirmDataNow(this, 'substituteTeacher', {
                checkResult: function(result){
                    var arrayCheck = null;
                    for(var z in result[3]){
                        if(result[3][z] != 1) {
                            if(arrayCheck==null) arrayCheck = {};
                            arrayCheck[z.replace('u', '')] = 0;
                        }
                    }
                    if(arrayCheck==null)
                        result[3] = 1;
                    return result;
                },
                before: function(data){
                    data[3] = self.teacherArray[data[3]];
                    data[0] = (data[0] != "") ? data[0] : "Error"
                    data[1] = (data[1] != "") ? data[1] : "Error"
                    data[2] = (data[2] != "") ? data[2] : "Error"
                    if(self.teacherArray[data[3]] != null){
                       data[3] = self.teacherArray[data[3]];
                    }
                    return data;
                },
                addRow: function(data){
                    return {
                        teachername: data[0],
                        date: data[1],
                        termination: data[2] == "Y" ? "checked" : "",
                        teacherId: data[3]
                    };
                }
            });
        }
    },
    mounted: function () {
        //teacher array
        eventHub.$on('collect-info', this.assign);
        eventHub.$on('confirm-info-substituteTeacherModal', this.confirmData);
        // eventHub.$on('assign-startdate-substitute_teacher_from', this.autoDateTo);
    }
});

Vue.component('substituteTeacherModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}} : <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.SubstituteTeacher,
                jsLang.SubstitutionDate,
                jsLang.FacilitateVacancy,
                jsLang.SubstitutedUserLogin
            ],
            popup:[
                null,
                null,
                null,
                null
            ],
            reminder:[
                null,
                "YYYY-MM-DD or DD/MM/YYYY",
                jsLang.YesOrNo,
                null
            ],
            filetemp: "/home/cees/monthlyreport/template/substituteTeacher",
            fileFieldId: "substituteTeacherFile"
        }
    },
    mounted: function(){
    }
});