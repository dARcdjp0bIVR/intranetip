Vue.component('teacher-course', {
    template: '\
    <table class="table report-style" id="tbl_other">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 28%">\
    <col style="width: 24%">\
    <col style="width: 24%">\
    <col style="width: 17%">\
    <col style="width: 4%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="5">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#teacherCourseModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
    <th>#</th>\
       <th>'+jsLang.Date+'</th>\
        <th>'+jsLang.StaffName+'</th>\
        <th>'+jsLang.CourseName+'</th>\
        <th>'+jsLang.OrganizedBy+'</th>\
    <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td>{{ key + 1 }}</td>\
            <td>\
            <div is="multiple-date-select" v-bind:id="key" v-bind:type="\'teacher_course_from\'" v-bind:sourceKey="\'date\'"> \
            </div>\
            </td>\
            <td>\
            <div is="teacher-multi-select" v-bind:id="key" v-bind:type="\'teacher_course\'">\
            </div>\
            </td>\
            <td>\
            <input type="text" class="form-control" id="\'inputCourseName\'+ key" v-model="content.course">\
            </td>\
            <td>\
            <input type="text" class="form-control" id="\'inputOrganizer\' + key   " v-model="content.organization">\
            </td>\
            <td>\
            <div is="del-btn" v-bind:remove="remove" v-bind:target="key" v-bind:location="contents">\
            </div>\
            </td>\
        </tr>\
        <tr is="add-row" v-bind:add="add"></tr>\
    </tbody>\
    </table>\
    ',
    mixins: [myMixin],
    data: function () {
        return {
            contents: formData.TeacherCourse.slice(),
            row: {
                date: '',
                teacherIds: [],
                course: '',
                organization: ''
            },
            teachers: this.$root.teachers,
            teacherArray: this.$root.teacherArray
        }
    },
    methods: {
        add: function (e) {
            // this.addNewRows(e.target.value, this.row, this.contents);
        },
        remove: function (key) {
            this.removeRow(key, this.contents)
        },
        assign: function () {
            //  console.log('teacher-course is assigning');
            submitForm.TeacherCourse = this.contents;
        },
        confirmData: function() {
            var self = this;
            var self = this;
            this.confirmDataNow(this, 'teacherCourse', {
                arrayColumn: [1],
                checkResult: function (result) {
                    var arrayCheck = null;
                    for (var z in result[1]) {
                        if (result[1][z] != 1) {
                            if (arrayCheck == null) arrayCheck = {};
                            arrayCheck[z.replace('u', '')] = 0;
                        }
                    }
                    if (arrayCheck == null)
                        result[1] = 1;
                    return result;
                },
                before: function (data) {
                    data[0] = (data[0] != "") ? data[0] : "Error";
                    for (var z in data[1]) {
                        if (self.teacherArray[data[1][z]] != null) {
                            data[1][z] = self.teacherArray[data[1][z]];
                        }
                    }
                    data[2] = (data[2] != "") ? data[2] : "Error";
                    data[3] = (data[3] != "") ? data[3] : "Error";
                    return data;
                },
                addRow: function (data) {
                    return {
                        date: data[0],
                        teacherIds: data[1],
                        course: data[2],
                        organization: data[3]
                    };
                }
            });
        }
    },
    mounted: function () {
        eventHub.$on('collect-info', this.assign);
        eventHub.$on('confirm-info-teacherCourseModal', this.confirmData);
        // eventHub.$on('assign-startdate-teacher_course_from', this.autoDateTo);
    }
});

Vue.component('teacherCourseModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}} : <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.Date,
                jsLang.StaffUserLogin,
                jsLang.CourseName,
                jsLang.OrganizedBy
            ],
            popup:[
                null,
                null,
                null,
                null
            ],
            reminder:[
                "YYYY-MM-DD or DD/MM/YYYY",
                jsLang.DivideSymbol,
                null,
                null
            ],
            filetemp: "/home/cees/monthlyreport/template/teacherCourse",
            fileFieldId: "teacherCourseFile"
        }
    },
    mounted: function(){
    }
});