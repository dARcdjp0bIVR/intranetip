Vue.component('school-visit', {
    template: '\
    <table class="table report-style" id="tbl_other">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 25%">\
    <col style="width: 68%">\
    <col style="width: 4%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="3">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#visitInspectionModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
    <th>#</th>\
    <th>'+jsLang.Date+'</th>\
    <th>'+jsLang.Purpose+'</th>\
    <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td>{{ key + 1 }}</td>\
            <td>\
                <div is="multiple-date-select" v-bind:id="key" v-bind:type="\'school_visit\'" v-bind:sourceKey="\'date\'">\
                </div>\
            </td>\
            <td>\
                <input type="text" class="form-control " id="\'inputPurpose\'+ key" v-model="content.purpose" \>\
            </td>\
            <td>\
                <div is="del-btn" v-bind:remove="remove" v-bind:target="key" v-bind:location="contents">\
                </div>\
            </td>\
            </tr>\
            <tr is="add-row" v-bind:add="add"></tr>\
        </tbody>\
        </table>\
        ',
    mixins: [myMixin],
    data: function () {
        return {
            contents: formData.SchoolVisit.slice(),
            row: {
                date: '',
                purpose: ''
            }
        }
    },
    methods: {
        add: function (e) {
            // this.addNewRows(e.target.value, this.row, this.contents);
        },
        remove: function (key) {
            this.removeRow(key, this.contents)
        },
        assign: function () {
            submitForm.SchoolVisit = this.contents;
        },
        confirmData: function(){
            var self = this;
            this.confirmDataNow(this, 'visitInspection', {
                before: function(data){
                    data[0] = (data[0] != "") ? data[0] : "Error";
                    data[1] = (data[1] != "") ? data[1] : "Error";
                    return data;
                },
                addRow: function(data){
                    return {
                        date: data[0],
                        purpose: data[1]
                    };
                }
            });
            /*var uploadfile = $('#visitInspectionFile')[0].files[0];
            //File reading
            var fr = new FileReader();
            var resultAry = [];
            fr.onload = () => {
                var allResults = fr.result.split("\n");
                for( var x in allResults) {
                    if(allResults[x]!="")
                        resultAry.push(allResults[x].split(','));
                }
                //File reading
                $.ajax({
                    url: "/home/cees/monthlyreport/ajaxImport/visitInspection",
                    method: "POST",
                    type: "POST",
                    data: {"content" : resultAry},
                    success: function(data){
                        var result = JSON.parse(data);
                        if(!window.prop){
                            window.prop = {};
                            window.prop.tempData = [];
                        }
                        window.prop.tempData['visitInspectionTempData'] = result;
                        Vue.component('visitInspectionConfirmTable', {
                            template: self.$root.confirmTableTemplate,
                            data: function(){
                                return{
                                    TotalError : jsLang.TotalError,
                                    Remark : jsLang.Remark,
                                    result : {},
                                    error : 0,
                                    errorLang: self.$root.errorLang
                                };
                            },
                            mounted: function(){
                                var data = JSON.parse(JSON.stringify(window.prop.tempData['visitInspectionTempData'].result));
                                var here = this;
                                here.error = window.prop.tempData['visitInspectionTempData'].error;
                                here.result = data;
                                if(here.error != 0){
                                    $('button.btn-prev').show();
                                    $('button.btn-confirm').hide();
                                }
                                for(var x in data.data){
                                    data.data[x][0] = (data.data[x][0] != "") ? data.data[x][0] : "Error";
                                    data.data[x][1] = (data.data[x][1] != "") ? data.data[x][1] : "Error";
                                }
                                eventHub.$once('confirm-data-visitInspectionModal', function(){
                                    if(here.error == 0){
                                        //eventHub.$emit('add-data-appointmentModal');
                                        var newArr = [];
                                        var data = window.prop.tempData['visitInspectionTempData'].result;
                                        for(var x in data.data){
                                            newArr.push({
                                                date: data.data[x][0],
                                                purpose: data.data[x][1]
                                            });
                                        }
                                        self.addRows(newArr, self.contents);
                                        eventHub.$emit('close-visitInspectionModal');
                                        window.prop.tempData['visitInspectionTempData'] = null;
                                    }
                                });
                            },
                        });
                        eventHub.$emit('confirm-table-visitInspectionModal');
                    }
                });
            }
            fr.readAsText(uploadfile);*/
        }
    },
    mounted: function () {
        eventHub.$on('collect-info', this.assign);
        eventHub.$on('confirm-info-visitInspectionModal', this.confirmData);
    }
});

Vue.component('visitInspectionModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}} : <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.Date,
                jsLang.Purpose,
            ],
            popup:[
                null,
                null
            ],
            reminder:[
                "YYYY-MM-DD or DD/MM/YYYY",
                null
            ],
            filetemp: "/home/cees/monthlyreport/template/visitInspection",
            fileFieldId: "visitInspectionFile"
        }
    },
    mounted: function(){
    }
});