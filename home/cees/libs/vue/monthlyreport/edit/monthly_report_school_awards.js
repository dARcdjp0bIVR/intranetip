Vue.component('school-awards', {
    template: '\
    <table class="table report-style" id="tbl_other">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 93%">\
    <col style="width: 4%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="2">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#schoolAwardModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
    <th>#</th>\
    <th>'+jsLang.Award+'</th>\
    <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td>{{ key + 1 }}</td>\
            <td>\
            <input type="text" class="form-control" id="\'inputAwardDes\' + key" v-model="content.text">\
            </td>\
            <td>\
            <div is="del-btn" v-bind:remove="remove" v-bind:target="key">\
            </div>\
            </td>\
        </tr>\
        <tr is="add-row" v-bind:add="add"></tr>\
    </tbody>\
    </table>\
    ',
    mixins: [myMixin],
    data: function () {
        return {
            contents: formData.SchoolAwards.slice(),
            row: {
                text: ''
            }
        }
    },
    methods: {
        add: function (e) {
            // this.addNewRows(e.target.value, this.row, this.contents);
        },
        remove: function (key) {
            this.removeRow(key, this.contents)
        },
        assign: function () {
            submitForm.SchoolAwards = this.contents;
        },
        confirmData: function(){
            this.confirmDataNow(this, 'schoolAward', {
                before: function(data){
                    data[0] = (data[0] != "") ? data[0] : "Error";
                    return data;
                },
                addRow: function(data){
                    return {
                        text: data[0]
                    };
                }
            });
            /*var self = this;
            var uploadfile = $('#schoolAwardFile')[0].files[0];
            //File reading
            var fr = new FileReader();
            var resultAry = [];
            fr.onload = () => {
                var allResults = fr.result.split("\n");
                for( var x in allResults) {
                    if(allResults[x]!="")
                        resultAry.push(allResults[x].split(','));
                }
                //File reading
                $.ajax({
                    url: "/home/cees/monthlyreport/ajaxImport/schoolAward",
                    method: "POST",
                    type: "POST",
                    data: {"content" : resultAry},
                    success: function(data){
                        var result = JSON.parse(data);
                        if(!window.prop){
                            window.prop = {};
                            window.prop.tempData = [];
                        }
                        window.prop.tempData['schoolAwardTempData'] = result;
                        Vue.component('schoolAwardConfirmTable', {
                            template: self.$root.confirmTableTemplate,
                            data: function(){
                                return{
                                    TotalError : jsLang.TotalError,
                                    Remark : jsLang.Remark,
                                    result : {},
                                    error : 0,
                                    errorLang: self.$root.errorLang
                                };
                            },
                            mounted: function(){
                                var data = JSON.parse(JSON.stringify(window.prop.tempData['schoolAwardTempData'].result));
                                var here = this;
                                here.error = window.prop.tempData['schoolAwardTempData'].error;
                                here.result = data;
                                if(here.error != 0){
                                    $('button.btn-prev').show();
                                    $('button.btn-confirm').hide();
                                }
                                for(var x in data.data){
                                    data.data[x][0] = (data.data[x][0] != "") ? data.data[x][0] : "Error";
                                }
                                eventHub.$once('confirm-data-schoolAwardModal', function(){
                                    if(here.error == 0){
                                        //eventHub.$emit('add-data-appointmentModal');
                                        var newArr = [];
                                        var data = window.prop.tempData['schoolAwardTempData'].result;
                                        for(var x in data.data){
                                            newArr.push({
                                                text: data.data[x][0]
                                            });
                                        }
                                        self.addRows(newArr, self.contents);
                                        eventHub.$emit('close-schoolAwardModal');
                                        window.prop.tempData['schoolAwardTempData'] = null;
                                    }
                                });
                            },
                        });
                        eventHub.$emit('confirm-table-schoolAwardModal');
                    }
                });
            }
            fr.readAsText(uploadfile);*/
        }
    },
    mounted: function () {
        eventHub.$on('collect-info', this.assign);

        eventHub.$on('confirm-info-schoolAwardModal', this.confirmData);
    }
});

Vue.component('schoolAwardModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}} : <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.Award,
            ],
            popup:[
                null
            ],
            reminder:[
                null
            ],
            filetemp: "/home/cees/monthlyreport/template/schoolAward",
            fileFieldId: "schoolAwardFile"
        }
    },
    mounted: function(){
    }
});

