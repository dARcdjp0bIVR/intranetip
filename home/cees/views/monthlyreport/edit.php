<?php
global $config_school_code, $sys_custom, $Lang, $plugin, $module_version;

$reportInfo = $_PAGE['View']['Data']['reportInfo'];
$displaySchName = isset($Lang['CEES']['School']['Name'][$config_school_code])? $Lang['CEES']['School']['Name'][$config_school_code] : $Lang['CEES']['School']['Name']['Default'];
$displaySchType = isEJ()? $Lang['CEES']['School']['Type']['Primary'] : $Lang['CEES']['School']['Type']['Secondary'];
if($_PAGE['View']['Data']['schoolType'] == 'B') {
    $displaySchType .= ' ('.$Lang['CEES']['MonthlyReport']['JSLang']['BoardingSchool'].')';
}

if($sys_custom['SDAS']['CEES']['MonthlyReport']['VueEnv'] == 'dev'){
    $vue = 'vue.js';
}else{
    $vue = 'vue.min.js';
}

$eEnrollment = ($plugin['eEnrollment'])?'1':'0';
$SickLeave = ($plugin['attendancestaff'] && $module_version['StaffAttendance'] >= 3.0)? '1':'0';
$StudentEnrol = ($plugin['attendancestudent'])?'1':'0';
//debug_pr(addslashes($_PAGE['View']['Data']['TeacherListJson']));
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-SCALE=1">
    <title>TWGHS Monthly Report</title>
    <script type="text/javascript" src="/home/cees/libs/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/home/cees/libs/js/moment.js"></script>
    <script type="text/javascript" src="/home/cees/libs/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript"
            src="/home/cees/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="/home/cees/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>


<!--    <link rel="stylesheet/less" type="text/css" href="/home/cees/libs/bootstrap-datetimepicker/build/build_standalone.less">-->
<!--    <script src="less.js" type="text/javascript"></script>-->
    <script type="text/javascript" src="/home/cees/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.standalone.css"/>
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css"/>
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker3.standalone.css"/>

    <script type="text/javascript" src="/home/cees/libs/bootstrap-select/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/bootstrap-select/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="/home/cees/libs/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/css/form-TWGHS.css"/>
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/css/monthly_report.css"/><!-- Philips added -->
    <meta name="theme-color" content="#ffffff">
</head>
<body class="monthlyReport">
<script src="/home/cees/libs/js/<?echo $vue?>"></script>
<script src="/home/cees/libs/js/lodash.min.js"></script>
<div id="edit-data">
    <div id="wrapper">
    <div id="main-body">
        <div class="content-wrapper">
            <div class="content-header">
                <span class="title"><?=$Lang['CEES']['School']['MonthlyReport']?> > <?=$Lang['CEES']['School']['FillIn']?></span>
            </div>
            <hr class="titleDivider"/>
            <div class="content-body">

                <div id="form-header">
                    <div class="form-group row formInfo">
                        <label class="col-sm-4 col-md-3 col-form-label"><?=$Lang['CEES']['SchoolName']?></label>
                        <div class="gen-txt col-sm-4"><? echo $displaySchName ?></div>
                    </div>

                        <div class="form-group row formInfo">
                            <label class="col-sm-4 col-md-3 col-form-label"><?=$Lang['CEES']['SchoolType']?></label>
                            <div class="gen-txt col-sm-4"><? echo $displaySchType ?></div>
                        </div>

                        <div class="form-group row formInfo">
                            <label class="col-sm-4 col-md-3 col-form-label"><?=$Lang['CEES']['MonthYear']?></label>
                            <div class="gen-txt col-sm-4"><? echo str_pad($reportInfo['Month'], 2, '0', STR_PAD_LEFT) . '/' . $reportInfo['Year'] ?></div>
                        </div>

                        <div class="form-group row formInfo">
                            <label for="inputSupervisorName" class="col-sm-4 col-md-3 col-form-label"><?=$Lang['CEES']['SupervisorName']?></label>
                            <div class="col-sm-8 col-md-9">
                                <input type="text" class="form-control" id="inputSupervisorName" placeholder=""
                                       value="<? echo $reportInfo['SupervisorName'] ?>">
                            </div>
                        </div>
                    </div>
                    <div id="form-body">
                        <section-item
                                v-for="(section, index) in sections"
                                key="index"
                                class="section"
                                v-bind:id="index + 1"
                                v-bind:title="section.title"
                                v-bind:type="section.type"
                        ></section-item>
                        <!-- Submission Buttons -->
                        <div class="submission-area">
                            <button id="submitBtn" class="btn btn-blue" @click="submit"><?=$Lang['CEES']['MonthlyReport']['Submit']?></button>
                            <button id="draftBtn" class="btn btn-blue" @click="draft"><?=$Lang['CEES']['MonthlyReport']['Draft']?></button>
                            <button id="cancelBtn" class="btn btn-secondary btn-cancel" @click="cancelsubmit"><?=$Lang['CEES']['MonthlyReport']['Cancel']?>
                            </button>
                        </div>
                        <input type="hidden" id="reportID" value="<?= $reportInfo['ReportID'] ?>">
                        <input type="hidden" id="reportYear" value="<?= $reportInfo['Year'] ?>">
                        <input type="hidden" id="reportMonth" value="<?= $reportInfo['Month'] ?>">
                        <input type="hidden" id="reportAcademicYearID" value="<?= $reportInfo['AcademicYearID'] ?>">
                        <input type="hidden" id="reportSchoolType" value="<?= $reportInfo['SchoolType'] ?>">
                    </div>
                </div>
            </div>
        </div>
    </div







    >
        <import-thickbox
                v-for="(retrieve, index) in retrieves"
                v-bind:id="retrieve.id"
                v-bind:title="retrieve.name"
                v-bind:method="retrieve.method"
                ref="abc"
        ></import-thickbox>
        <import-thickbox2
                v-for="(retrieve, index) in imports"
                v-bind:id="retrieve.id"
                v-bind:title="retrieve.name"
                v-bind:method="retrieve.method"
                ref="abc"
        ></import-thickbox2>
</div>
</body>
</html>
<script>
    var formDataJson = "<?php echo addslashes($_PAGE['View']['Data']['reportDataJson'])?>";
    var TeacherListJson = "<?php echo addslashes($_PAGE['View']['Data']['TeacherListJson'])?>";
    var OLECategoryJson = "<?php echo addslashes($_PAGE['View']['Data']['OLECategoryJson'])?>";
    var jsLangJson = "<?php echo addslashes($_PAGE['View']['Data']['LangJson'])?>";
    var SickLeave = "<?php echo $SickLeave?>";
    var enrollment = "<?php echo $eEnrollment?>";
    var StudentEnrol = "<?php echo $StudentEnrol?>";
</script>

<script src="/home/cees/libs/vue/monthlyreport/edit/monthly_report_common.js"></script>
<script src="/home/cees/libs/vue/monthlyreport/edit/monthly_report_appointment_termination.js"></script>
<script src="/home/cees/libs/vue/monthlyreport/edit/monthly_report_substitute_teacher.js"></script>
<script src="/home/cees/libs/vue/monthlyreport/edit/monthly_report_staff_sick_leave.js"></script>
<script src="/home/cees/libs/vue/monthlyreport/edit/monthly_report_student_enroll.js"></script>
<script src="/home/cees/libs/vue/monthlyreport/edit/monthly_report_visit_inspection.js"></script>
<script src="/home/cees/libs/vue/monthlyreport/edit/monthly_report_school_administration.js"></script>
<script src="/home/cees/libs/vue/monthlyreport/edit/monthly_report_teacher_course.js"></script>
<script src="/home/cees/libs/vue/monthlyreport/edit/monthly_report_student_eca.js"></script>
<script src="/home/cees/libs/vue/monthlyreport/edit/monthly_report_school_awards.js"></script>
<script src="/home/cees/libs/vue/monthlyreport/edit/monthly_report_advanced_notice.js"></script>
<script src="/home/cees/libs/vue/monthlyreport/edit/monthly_report_others.js"></script>
<script src="/home/cees/libs/vue/monthlyreport/edit/monthly_report_base.js"></script>