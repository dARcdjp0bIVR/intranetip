<?php

class Section
{
    var $db;
    var $table;
    var $reportId;
    var $contents;

    var $dbFieldArr;
    var $jsonToDbMapping;

    public function __construct()
    {

    }

    public function set($contents)
    {

        $this->contents = $contents;
    }

    public function save()
    {
        if (isset($this->contents)) {
            $contents = $this->contents;
            if (!empty($contents)) {
                $dataArr = array();
                foreach ($contents as $_key => $_row) {
                    foreach ($this->jsonToDbMapping as $__jsonName => $__dbName) {
                        $dataArr[$_key][] = $_row[$__jsonName];
                    }
                    $dataArr[$_key][] = $this->reportId;
                }
                $this->insertData($this->table, $this->getSaveFieldArr(), $dataArr);
            }
        }
    }

    public function getSaveFieldArr()
    {
        $temp = $this->dbFieldArr;
        $temp[] = 'ReportID';
        return $temp;
    }

    /********************************
     * From ePCM insert Data (Start)*
     ********************************/

    private function getStandardInsertValue($stringArr)
    {
        $result = '';
        $delimeter = ', ';
        $numString = count($stringArr);
        if ($numString > 0) {
            $result = '( ';
            for ($i = 0; $i < $numString; $i++) {
                $_string = $this->db->Get_Safe_Sql_Query($stringArr[$i]);
                $result .= "'$_string'";
                if ($i != ($numString - 1)) {
                    $result .= $delimeter;
                }
            }
            $result .= ' )';
        }
        return $result;
    }

    private function getSafeInsertValueList($multiRowValueArr)
    {
        $valueListArr = array();
        foreach ((array)$multiRowValueArr as $key => $row) {
            $valueListArr[] = $this->getStandardInsertValue($row);
        }
        return $valueListArr;
    }

    protected function insertData($tableName, $fieldNameArr, $multiRowValueArr)
    {
        $sql = '';
        if (empty($fieldNameArr) || empty($multiRowValueArr) || $tableName == '') {
// 			$sql = "/*Cannot empty any param when calling libPCM_db->getInsertSQL */";
        } else {
            $valueListArr = $this->getSafeInsertValueList($multiRowValueArr);
            if (!empty($valueListArr)) {
                $sql = "INSERT INTO $tableName (" . implode(', ', $fieldNameArr) . ") Values " . implode(',', $valueListArr) . ";";
            }
        }
        if ($sql != '') {
            debug_pr($sql);
            return $this->db->db_db_query($sql);
        }
    }

    /********************************
     * From ePCM insert Data (End)*
     ********************************/

    public function fetch()
    {
        $jsonToDbMapping = $this->jsonToDbMapping;
        $selectFieldArr = array();
        foreach ($jsonToDbMapping as $_jsonName => $_dbName) {
            $selectFieldArr[] = $_dbName . ' as ' . $_jsonName;
        }
        $sql = "SELECT " . implode(',', $selectFieldArr) . " FROM {$this->table} WHERE ReportID = '{$this->reportId}'";
        $this->contents = $this->db->returnResultSet($sql);
    }

    public function read()
    {
        $this->fetch();
        return $this->contents;
    }

    public function delete()
    {
        $sql = "DELETE FROM " . $this->table . " WHERE ReportID = '" . $this->reportId . "'";
        return $this->db->db_db_query($sql);
    }

    public function date_sort($a, $b) {
        return strtotime($a['date']) - strtotime($b['date']);
    }

    public function dateList_sort($a, $b) {
        return strtotime($a) - strtotime($b);
    }

    public function dateSub_sort($a, $b) {
        return strtotime($a['date'][0]) - strtotime($b['date'][0]);
    }

}