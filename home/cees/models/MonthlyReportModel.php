<?php

Class MonthlyReportModel
{
    var $db;
    private $reportId;
    private $reportInfo;
    private $reportData;
    private $dataModel = array();

    public function __construct($db, $year, $month, $reportId = '', $schoolType='')
    {
        $this->db = $db;

        if ($reportId != '') {
            //$reportKey_conds = " AND ReportKey = UNHEX('$reportKey') ";
            $reportId_conds = " AND ReportID = '$reportId' ";
        }
        if ($schoolType != '') {
            $schoolType = $schoolType=='N' ? '' : $schoolType;
            $schoolType_conds = " AND SchoolType = '$schoolType' ";
        }

        $sql = "SELECT * FROM CEES_SCHOOL_MONTHLY_REPORT WHERE Year = '$year' AND Month = '$month' $reportId_conds $schoolType_conds ";
        $temp = $this->db->returnResultSet($sql);

        if (empty($temp)) {
            if ($reportId != '') {
                throw new Exception('Report Do Not Exist');
            } else {
                // new report
                $this->createNewReport($year, $month, $schoolType);
                $sql = "SELECT * FROM CEES_SCHOOL_MONTHLY_REPORT WHERE Year = '$year' AND Month = '$month' $reportKey_conds $schoolType_conds ";
                $temp = $this->db->returnResultSet($sql);
            }
        }
        $this->reportInfo = $temp[0];
        $this->reportId = $temp[0]['ReportID'];
        $this->prepareDataModel();
    }

    private function createNewReport($year, $month, $schoolType='')
    {
        $lastSupervisor = $this->getLastSupervisor($year, $month, $schoolType);
        $randomMD5 = md5(time());
        $academicYearIDAry = getAcademicYearAndYearTermByDate($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01');
        $academicYearID = $academicYearIDAry['AcademicYearID'];

        $sql = "INSERT INTO CEES_SCHOOL_MONTHLY_REPORT
                (ReportKey,Year, Month, SupervisorName, AcademicYearID, SchoolType,
                 SubmitStatus, DraftStatus, 
                 InuptBy, InputDate, ModifiedBy, ModifiedDate )
                VALUES
                ( UNHEX('$randomMD5') ,'$year', '$month', '$lastSupervisor', '$academicYearID', '$schoolType',
                0,1, '{$_SESSION['UserID']}', now(),'{$_SESSION['UserID']}',now() )";
        $success = $this->db->db_db_query($sql);
        $reportId = $this->db->db_insert_id();

        $result = ($success) ? $reportId : -1;
        return $result;
    }

    private function prepareDataModel()
    {
        $sections = $this->getAllSections();
        $dataModel = array();
        foreach ((array)$sections as $_sectionName) {
            $modelName = 'Section_' . $_sectionName;
            include_once $modelName . '.php';
            $dataModel[$_sectionName] = new $modelName($this->db, $this->reportId);
        }
        $this->dataModel = $dataModel;
    }

    private function getDataModel($modelName)
    {
        return $this->dataModel[$modelName];
    }

    public function getReportSectionData($modelName)
    {
        if (isset($this->dataModel[$modelName])) {
            $dataModel = $this->getDataModel($modelName);
            return $dataModel->read();
        } else {
            return array();
        }
    }

    public function getReportData()
    {
        $data = array();
        foreach ($this->getAllSections() as $_sectionName) {
            $data[$_sectionName] = $this->getReportSectionData($_sectionName);
        }
        return $data;
    }

    public function getReportInfo()
    {
        return $this->reportInfo;
    }

    public function setReportData($dataAssocArr)
    {
        $this->reportData = $dataAssocArr;
    }

    public function saveReport()
    {
        if (isset($this->reportData)) {
            foreach ($this->getAllSections() as $_sectionName) {
                $_sectionData = $this->reportData[$_sectionName];
                $_dataModel = $this->getDataModel($_sectionName);
                $_dataModel->set($_sectionData);
                $_dataModel->delete();
                $_dataModel->save();
            }
        }
    }

    public function changeDraftStatus($DraftStatus)
    {

        $sql = "UPDATE 
                  CEES_SCHOOL_MONTHLY_REPORT 
                Set
                 DraftStatus = '$DraftStatus',
                 ModifiedBy='{$_SESSION['UserID']}', 
                 ModifiedDate  = now()
                Where ReportID = '$this->reportId' ";

        $success = $this->db->db_db_query($sql);
        return $success;

    }

    public function changeSubmitStatus($SubmitStatus)
    {
        $sql = "UPDATE 
                  CEES_SCHOOL_MONTHLY_REPORT 
                Set
                 SubmitStatus = '$SubmitStatus',
                 SubmittedToCeesBy='{$_SESSION['UserID']}', 
                 SubmittedToCeesDate  = now()
                Where ReportID = '$this->reportId' ";

        $success = $this->db->db_db_query($sql);
        return $success;

    }

    public function updateSupervisorName($Supervisor)
    {
        $sql = "UPDATE 
                  CEES_SCHOOL_MONTHLY_REPORT 
                Set
                 SupervisorName = '$Supervisor',
                 ModifiedBy='{$_SESSION['UserID']}', 
                 ModifiedDate  = now()
                Where ReportID = '$this->reportId' ";

        $success = $this->db->db_db_query($sql);
        return $success;

    }

    public function getTeacherName()
    {
        global $intranet_session_language;
        $tablename = isEJ()?'INTRANET_USER_UTF8':'INTRANET_USER';

        if($intranet_session_language == 'b5'){
            $name  = "IF(iu.ChineseName is null or iu.ChineseName ='', iu.EnglishName, " . Get_Lang_Selection('iu.ChineseName', 'iu.EnglishName') . " )";
        }else{
            $name  = "IF(iu.EnglishName is null or iu.EnglishName ='', iu.ChineseName, " . Get_Lang_Selection('iu.ChineseName', 'iu.EnglishName') . " )";
        }
//        $name = isEJ()?'iu.EnglishName' : $name;
        // EXCEPT ARCHIVE USER (EJ WILL NOT DELETE USER FROM _TRF8 TABLE)
        $exceptArchiveUser =isEJ()? "INNER JOIN INTRANET_USER as b ON (b.UserID = iu.UserID )": "";

        $groupName = "IF(ig.Title is null or ig.Title ='', ig.TitleChinese, " . Get_Lang_Selection('ig.TitleChinese', 'ig.Title') . " )";
//
//        $sql = "SELECT
//                    iu.UserID as id,
//                    $name as name,
//                    $groupName as groupName
//                FROM
//                    ".$tablename." as iu
//                LEFT JOIN INTRANET_USERGROUP  as iug ON (iu.UserID = iug.UserID)
//                LEFT JOIN INTRANET_GROUP AS ig ON (ig.GroupID = iug.GroupID)
//                WHERE
//                    iu.RecordType='1' AND
//                    iu.RecordStatus='1'
//                ORDER BY
//                    iu.EnglishName
//                        ";

        $sql = "SELECT 
                    iu.Teaching, 
                    iu.UserID as id, 
                    $name as name
                   
                FROM
                    ".$tablename." as iu
                   $exceptArchiveUser       
                WHERE 
                    iu.RecordType='1' AND
                    iu.RecordStatus='1'
                ORDER BY 
                    iu.Teaching, iu.EnglishName
                        ";
        $InUsedTeacherInfo = $this->db->returnResultSet($sql);

        $sql = "SELECT 
                    '2' as Teaching, 
                    iu.UserID as id, 
                    $name as name
                   
                FROM
                   INTRANET_ARCHIVE_USER as iu               
                WHERE 
                    iu.RecordType='1' AND
                    iu.DateArchive >(NOW() - INTERVAL 90 DAY)
                ORDER BY 
                    iu.EnglishName
                        ";


        $ArchiveTeacherInfo = $this->db->returnResultSet($sql);
        $TeacherInfo  =  array_merge($InUsedTeacherInfo,$ArchiveTeacherInfo);

        return $TeacherInfo;
    }

    public function getOLECategory(){
        global $eclass_db;


        $name  = Get_Lang_Selection('ChiTitle', 'EngTitle');

        $sql = "SELECT 
                    DefaultID as id, 
                    $name as name 
                FROM
                    {$eclass_db}.OLE_ELE  
                WHERE
                    DefaultID IS NOT NULL        
                ORDER BY 
                    RecordID
                        ";
        $CategoryInfo = $this->db->returnResultSet($sql);

        return $CategoryInfo;


    }

    public function getAllSections()
    {

        $ShowSection = $this->getUserAccessSections();
//        $ShowSection =  monthlyreportmodel::getUserAccessSections();
        $ShowSectionAssAry = BuildMultiKeyAssoc($ShowSection,'DisplaySection');
        foreach($ShowSectionAssAry as $SectionName => $SectionAry){
            $section[] = $SectionName;
        }
//debug_pr($section);
//        $section = array(
//            'AppointmentTermination',
//            'SubstituteTeacher',
//            'TeacherSickLeave',
//            'StudentEnrolment',
//            'SchoolVisit',
//            'SchoolAdministration',
//            'TeacherCourse',
//            'StudentEca',
//            'SchoolAwards',
//            'AdvancedNotice',
//            'Others'
//        );
        return $section;
    }

    private function getLastSupervisor($year, $month, $schoolType='')
    {
        $dateString = $year . '-' . $month . '-01';
        $dateString = strtotime('-1 month', $dateString);

        $sql_year = date('Y', $dateString);
        $sql_month = date('m', $dateString);

        $sql = "SELECT SupervisorName FROM CEES_SCHOOL_MONTHLY_REPORT WHERE Year = '$sql_year'  AND Month = '$sql_month' AND SchoolType = '$schoolType' ";
        $result = $this->db->returnResultSet($sql);
        return $result[0];
    }

    public function getUserAccessSections(){

//        $sql = "select SettingValue from GENERAL_SETTING where SettingName = 'MonthlyReportPIC'";
//        $PICList =  $this->db->returnVector($sql);
//        $PICList =  MonthlyReportModel::db->returnVector($sql);

//        $PICAry = explode(",",  $PICList[0]);

        $UserAccessSectionAry = array();
//        if(in_array($_SESSION['UserID'],$PICAry)){
        if($_SESSION['CEES']['MonthlyReportPIC'] || $_SESSION['CEES']['SuperAdmin'] || $_SESSION['CEES']['Principal']){
            $SectionDisplayAry = array('AppointmentTermination','SubstituteTeacher','TeacherSickLeave',
                'StudentEnrolment','SchoolVisit','SchoolAdministration',
                'TeacherCourse','StudentEca','SchoolAwards','AdvancedNotice','Others');

            $SectionNameAry = array('Appoiontment','Substitute','SickLeave',
                'StudentEnrol','Schoolvisit','Schooladministration',
                'StaffCourse','Studenteca','SchoolAwards','AdvancedNotice','Others');

            $SectionTemplateAry = array('appointment-termination','substitute-teacher','teacher-sick-leave',
                'student_enrolment','school-visit','school-administration',
                'teacher-course','student-eca','school-awards','advanced-notice','others');

            for($i=0;$i<count($SectionDisplayAry);$i++){
                $UserAccessSectionAry[$i]['0'] = $SectionDisplayAry[$i];
                $UserAccessSectionAry[$i]['DisplaySection'] = $SectionDisplayAry[$i];
                $UserAccessSectionAry[$i]['1'] = $SectionNameAry[$i];
                $UserAccessSectionAry[$i]['SectionName'] = $SectionNameAry[$i];
                $UserAccessSectionAry[$i]['2'] = $SectionTemplateAry[$i];
                $UserAccessSectionAry[$i]['TemplateName'] = $SectionTemplateAry[$i];
            }

            return $UserAccessSectionAry;
        }else{
            $sql = "SELECT 
                  SectionName as DisplaySection,
                  CASE  
                      WHEN SectionName = 'AppointmentTermination' THEN 'Appoiontment'
                      WHEN SectionName = 'SubstituteTeacher' THEN 'Substitute'
                      WHEN SectionName = 'TeacherSickLeave' THEN 'SickLeave'
                      WHEN SectionName = 'StudentEnrolment' THEN 'StudentEnrol'
                      WHEN SectionName = 'SchoolVisit' THEN 'Schoolvisit'
                      WHEN SectionName = 'SchoolAdministration' THEN 'Schooladministration'
                      WHEN SectionName = 'TeacherCourse' THEN 'StaffCourse'
                      WHEN SectionName = 'StudentEca' THEN 'Studenteca'
                      WHEN SectionName = 'SchoolAwards' THEN 'SchoolAwards'
                      WHEN SectionName = 'AdvancedNotice' THEN 'AdvancedNotice'
                     ELSE 'Others'
                   END as SectionName,
                   CASE  
                      WHEN SectionName = 'AppointmentTermination' THEN 'appointment-termination'
                      WHEN SectionName = 'SubstituteTeacher' THEN 'substitute-teacher'
                      WHEN SectionName = 'TeacherSickLeave' THEN 'teacher-sick-leave'
                      WHEN SectionName = 'StudentEnrolment' THEN 'student_enrolment'
                      WHEN SectionName = 'SchoolVisit' THEN 'school-visit'
                      WHEN SectionName = 'SchoolAdministration' THEN 'school-administration'
                      WHEN SectionName = 'TeacherCourse' THEN 'teacher-course'
                      WHEN SectionName = 'StudentEca' THEN 'student-eca'
                      WHEN SectionName = 'SchoolAwards' THEN 'school-awards'
                      WHEN SectionName = 'AdvancedNotice' THEN 'advanced-notice'
                     ELSE 'others'
                   END as TemplateName,
                   CASE  
                      WHEN SectionName = 'AppointmentTermination' THEN '0'
                      WHEN SectionName = 'SubstituteTeacher' THEN '1'
                      WHEN SectionName = 'TeacherSickLeave' THEN '2'
                      WHEN SectionName = 'StudentEnrolment' THEN '3'
                      WHEN SectionName = 'SchoolVisit' THEN '4'
                      WHEN SectionName = 'SchoolAdministration' THEN '5'
                      WHEN SectionName = 'TeacherCourse' THEN '6'
                      WHEN SectionName = 'StudentEca' THEN '7'
                      WHEN SectionName = 'SchoolAwards' THEN '8'
                      WHEN SectionName = 'AdvancedNotice' THEN '9'
                     ELSE '10'
                   END as orderNumber
                FROM
                    CEES_SCHOOL_MONTHLY_REPORT_USER_ACCESS_RIGHT  
                WHERE
                    UserID = '{$_SESSION['UserID']}'   
                ORDER BY orderNumber    
                        ";

            $UserAccessSectionAry = $this->db->returnArray($sql);

            return $UserAccessSectionAry;
        }

    }

}