<?php
include_once 'Section.php';

class Section_AdvancedNotice extends Section
{
    var $db;
    var $table = 'CEES_SCHOOL_MONTHLY_REPORT_ADVANCED_NOTICE';
    var $reportId;
    var $contents = array();

    var $dbFieldArr = array('Description');
    var $jsonToDbMapping = array(
        'text' => 'Description'
    );

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;
    }
}