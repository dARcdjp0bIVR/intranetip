<?php
include_once 'Section.php';

class Section_StudentEca extends Section
{
    var $db;
    var $table = 'CEES_SCHOOL_MONTHLY_REPORT_STUDENT_ECA';
    var $reportId;
    var $contents = array();

    var $dbFieldArr = array('ECADate', 'RecordType','Category', 'ActivityName', 'ParticipantNo');
    var $jsonToDbMapping = array(
        'date' => 'ECADate',
        'type' => 'RecordType',
        'categoryID'=>'Category',
        'activityName' => 'ActivityName',
        'participantsNo' => 'ParticipantNo'
    );

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;
    }

    public function fetch()
    {
        $jsonToDbMapping = $this->jsonToDbMapping;
        $selectFieldArr = array();

        foreach ($jsonToDbMapping as $_jsonName => $_dbName) {
            $selectFieldArr[] = $_dbName . ' as ' . $_jsonName;
        }
        $sqlI = "SELECT " . implode(',', $selectFieldArr) . " FROM {$this->table} WHERE ReportID = '{$this->reportId}' AND RecordType='I' ";
        $this->contents['internal'] = $this->db->returnResultSet($sqlI);

        $sqlE = "SELECT " . implode(',', $selectFieldArr) . " FROM {$this->table} WHERE ReportID = '{$this->reportId}' AND RecordType='E' ";
        $this->contents['external'] = $this->db->returnResultSet($sqlE);
    }

    public function save()
    {
        if (isset($this->contents)) {
            $internalContents = $this->contents['internal'];

            for($i=0;$i<count($internalContents);$i++){
                $dateList = $internalContents[$i]['date'];
                $dateListAry = explode(',',$dateList);
                usort($dateListAry, array($this,"dateList_sort"));
                $internalContents[$i]['date'] = $dateListAry;
            }

            usort($internalContents, array( $this, 'dateSub_sort'));
            foreach($internalContents as $key=>$InfoAry){
                $internalContents[$key]['date'] = implode(',',$InfoAry['date']);
            }

            if (!empty($internalContents)) {
                $dataArr = array();
                foreach ($internalContents  as $_key => $_row) {
                    foreach ($this->jsonToDbMapping as $__jsonName => $__dbName) {
                        if ($__jsonName == 'categoryID') {
                            $dataArr[$_key][] = implode(',', $_row[$__jsonName]);
                        } else {
                            $dataArr[$_key][] = $_row[$__jsonName];
                        }
                    }
                    $dataArr[$_key][] = $this->reportId;
                }
                $this->insertData($this->table, $this->getSaveFieldArr(), $dataArr);
            }

            $externalContents = $this->contents['external'];

            for($i=0;$i<count($externalContents);$i++){
                $dateList = $externalContents[$i]['date'];
                $dateListAry = explode(',',$dateList);
                usort($dateListAry, array($this,"dateList_sort"));
                $externalContents[$i]['date'] = $dateListAry;
            }

            usort($externalContents, array( $this, 'dateSub_sort'));
            foreach($externalContents as $key=>$InfoAry){
                $externalContents[$key]['date'] = implode(',',$InfoAry['date']);
            }

            if (!empty($externalContents)) {
                $dataArr = array();
                foreach ($externalContents as $_key => $_row) {
                    foreach ($this->jsonToDbMapping as $__jsonName => $__dbName) {
                        if ($__jsonName == 'categoryID') {
                            $dataArr[$_key][] = implode(',', $_row[$__jsonName]);
                        } else {
                            $dataArr[$_key][] = $_row[$__jsonName];
                        }
                    }
                    $dataArr[$_key][] = $this->reportId;
                }
                $this->insertData($this->table, $this->getSaveFieldArr(), $dataArr);

            }

        }
    }

    public function read()
    {
        $this->fetch();
        $contents = $this->contents;
        $internalContents = $this->contents['internal'];
        foreach ($internalContents as &$_row) {

            $_row['categoryID'] = explode(',', $_row['categoryID']);

        }
        $contents['internal'] = $internalContents;

        $externalContents = $this->contents['external'];
        foreach ($externalContents as &$_row) {

            $_row['categoryID'] = explode(',', $_row['categoryID']);
        }
        $contents['external'] = $externalContents;
        return $contents;
    }
}