<?php
include_once 'Section.php';

class Section_StudentEnrolment extends Section
{
    var $db;
    var $table = 'CEES_SCHOOL_MONTHLY_REPORT_STUDENT_ENROL_ATTENDACE';
    var $reportId;
    var $contents = array();

    var $dbFieldArr = array('Level', 'ApprovedNo', 'OperationNo', 'ApprovedPlaceNo', 'EnrolledNo', 'AttendanceRate');
    var $jsonToDbMapping = array(
        'level' => 'Level',
        'approvedNo' => 'ApprovedNo',
        'operationNo' => 'OperationNo',
        'placeApprovedNo' => 'ApprovedPlaceNo',
        'thisMonthEnrolled' => 'EnrolledNo',
        'averageAttendance' => 'AttendanceRate'
    );

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;


    }

    public function fetch()
    {
        $jsonToDbMapping = $this->jsonToDbMapping;
        $selectFieldArr = array();
        foreach ($jsonToDbMapping as $_jsonName => $_dbName) {
            $selectFieldArr[] = $_dbName . ' as ' . $_jsonName;
        }
        $sql = "SELECT " . implode(',', $selectFieldArr) . " FROM {$this->table} WHERE ReportID = '{$this->reportId}'";
        $EnrollResult = $this->db->returnResultSet($sql);
        $i = 0;
        foreach ($EnrollResult as $EnrollArr) {
            $Result[$i]['level'] = $EnrollArr['level'];
            $Result[$i]['approvedNo'] = $EnrollArr['approvedNo'];
            $Result[$i]['operationNo'] = $EnrollArr['operationNo'];
            $Result[$i]['placeApprovedNo'] = $EnrollArr['placeApprovedNo'];
            $Result[$i]['lastMonthEnrolled'] = $this->getSeptemberEnrolledNumber($Result[$i]['level']);
            $Result[$i]['thisMonthEnrolled'] = $EnrollArr['thisMonthEnrolled'];
            $Result[$i]['averageAttendance'] = floatval($EnrollArr['averageAttendance']);
            $i++;
        }

        $this->contents = $Result;
        //   $this->contents = $this->db->returnResultSet($sql);
    }

    public function read()
    {

        $this->fetch();
        if (empty($this->contents)) {
            $StudentEnrol = array();
            if (isEJ()) {
                $StudentEnrol[] = array('level' => 'P1', 'approvedNo' => '', 'operationNo' => '', 'placeApprovedNo' => '', 'lastMonthEnrolled' => $this->getSeptemberEnrolledNumber('P1'), 'thisMonthEnrolled' => '', 'averageAttendance' => '');
                $StudentEnrol[] = array('level' => 'P2', 'approvedNo' => '', 'operationNo' => '', 'placeApprovedNo' => '', 'lastMonthEnrolled' => $this->getSeptemberEnrolledNumber('P2'), 'thisMonthEnrolled' => '', 'averageAttendance' => '');
                $StudentEnrol[] = array('level' => 'P3', 'approvedNo' => '', 'operationNo' => '', 'placeApprovedNo' => '', 'lastMonthEnrolled' => $this->getSeptemberEnrolledNumber('P3'), 'thisMonthEnrolled' => '', 'averageAttendance' => '');
                $StudentEnrol[] = array('level' => 'P4', 'approvedNo' => '', 'operationNo' => '', 'placeApprovedNo' => '', 'lastMonthEnrolled' => $this->getSeptemberEnrolledNumber('P4'), 'thisMonthEnrolled' => '', 'averageAttendance' => '');
                $StudentEnrol[] = array('level' => 'P5', 'approvedNo' => '', 'operationNo' => '', 'placeApprovedNo' => '', 'lastMonthEnrolled' => $this->getSeptemberEnrolledNumber('P5'), 'thisMonthEnrolled' => '', 'averageAttendance' => '');
                $StudentEnrol[] = array('level' => 'P6', 'approvedNo' => '', 'operationNo' => '', 'placeApprovedNo' => '', 'lastMonthEnrolled' => $this->getSeptemberEnrolledNumber('P6'), 'thisMonthEnrolled' => '', 'averageAttendance' => '');
            } else {
                $StudentEnrol[] = array('level' => 'S1', 'approvedNo' => '', 'operationNo' => '', 'placeApprovedNo' => '', 'lastMonthEnrolled' => $this->getSeptemberEnrolledNumber('S1'), 'thisMonthEnrolled' => '', 'averageAttendance' => '');
                $StudentEnrol[] = array('level' => 'S2', 'approvedNo' => '', 'operationNo' => '', 'placeApprovedNo' => '', 'lastMonthEnrolled' => $this->getSeptemberEnrolledNumber('S2'), 'thisMonthEnrolled' => '', 'averageAttendance' => '');
                $StudentEnrol[] = array('level' => 'S3', 'approvedNo' => '', 'operationNo' => '', 'placeApprovedNo' => '', 'lastMonthEnrolled' => $this->getSeptemberEnrolledNumber('S3'), 'thisMonthEnrolled' => '', 'averageAttendance' => '');
                $StudentEnrol[] = array('level' => 'S4', 'approvedNo' => '', 'operationNo' => '', 'placeApprovedNo' => '', 'lastMonthEnrolled' => $this->getSeptemberEnrolledNumber('S4'), 'thisMonthEnrolled' => '', 'averageAttendance' => '');
                $StudentEnrol[] = array('level' => 'S5', 'approvedNo' => '', 'operationNo' => '', 'placeApprovedNo' => '', 'lastMonthEnrolled' => $this->getSeptemberEnrolledNumber('S5'), 'thisMonthEnrolled' => '', 'averageAttendance' => '');
                $StudentEnrol[] = array('level' => 'S6', 'approvedNo' => '', 'operationNo' => '', 'placeApprovedNo' => '', 'lastMonthEnrolled' => $this->getSeptemberEnrolledNumber('S6'), 'thisMonthEnrolled' => '', 'averageAttendance' => '');

            }
            $this->contents = $StudentEnrol;
        }
        return $this->contents;

    }

    function getSeptemberEnrolledNumber($LevelName)
    {
        $sql = "SELECT Year,Month FROM CEES_SCHOOL_MONTHLY_REPORT WHERE ReportID  = '$this->reportId'";
        $ReportInfo = $this->db->returnResultSet($sql);
        $Month = $ReportInfo[0]['Month'];
        $Year = $ReportInfo[0]['Year'];
        if($Month >= '9'){
            $conds = "AND Year='$Year' and Month='9'";
        }
        else{
            $lastYear = intval($Year)-1;
            $conds = "AND Year = '$lastYear' and Month='9'";
        }

        $sql = "SELECT
                  sta.EnrolledNo
                FROM
                  CEES_SCHOOL_MONTHLY_REPORT_STUDENT_ENROL_ATTENDACE AS sta
                INNER JOIN
                  CEES_SCHOOL_MONTHLY_REPORT AS mr ON (mr.ReportID = sta.ReportID)
                 WHERE mr.ReportID = (
                      SELECT ReportID FROM CEES_SCHOOL_MONTHLY_REPORT WHERE 1 $conds
                 )
                  AND sta.Level = '$LevelName'
              ";

        $SeptemberNoAry = $this->db->returnVector($sql);
        $SeptemberNo = $SeptemberNoAry[0] == '0' || $SeptemberNoAry[0] == '' ? '---' : $SeptemberNoAry[0];

        return $SeptemberNo;
    }
}