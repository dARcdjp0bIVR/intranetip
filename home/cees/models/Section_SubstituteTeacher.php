<?php
include_once 'Section.php';

class Section_SubstituteTeacher extends Section
{
    var $db;
    var $table = 'CEES_SCHOOL_MONTHLY_REPORT_SUBSTITUTE_INFO';
    var $reportId;
    var $contents = array();

    var $dbFieldArr = array('SubstituteName', 'StartDate', 'SubstitutedStaffID','Terminationed');
    var $jsonToDbMapping = array(
        'teachername' => 'SubstituteName',
        'date' => 'StartDate',
        'teacherId' => 'SubstitutedStaffID',
        'termination' => 'Terminationed'
    );

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;
    }

    public function fetch()
    {
        $jsonToDbMapping = $this->jsonToDbMapping;
        $selectFieldArr = array();
        foreach ($jsonToDbMapping as $_jsonName => $_dbName) {
            $selectFieldArr[] = $_dbName . ' as ' . $_jsonName;
        }
        $sql = "SELECT " . implode(',', $selectFieldArr) . " FROM {$this->table} WHERE ReportID = '{$this->reportId}'";
        $SubstituteResult = $this->db->returnResultSet($sql);
        $Result = array();
        $i = 0;
        foreach ($SubstituteResult as $SubstituteArr) {
            $Result[$i]['teachername'] = $SubstituteArr['teachername'];
            $Result[$i]['date'] = $SubstituteArr['date'];
            $Result[$i]['termination'] = $SubstituteArr['termination'] == '1' ? true : false;
            $Result[$i]['teacherId'] =$SubstituteArr['teacherId'];
            $i++;
        }
        $this->contents = $Result;
    }

    public function save()
    {
        if (isset($this->contents)) {
            $contents = $this->contents;
            for($i=0;$i<count($contents);$i++){
                $dateList = $contents[$i]['date'];
                $dateListAry = explode(',',$dateList);
                usort($dateListAry, array($this,"dateList_sort"));
                $contents[$i]['date'] = $dateListAry;
            }

            usort($contents, array( $this, 'dateSub_sort'));
            foreach($contents as $key=>$InfoAry){
                $contents[$key]['date'] = implode(',',$InfoAry['date']);
            }

            if (!empty($contents)) {
                $dataArr = array();
                foreach ($contents as $_key => $_row) {

                    foreach ($this->jsonToDbMapping as $__jsonName => $__dbName) {
                        if ($__jsonName == 'termination') {
                            $Terminationed = $_row[$__jsonName] == 'true' ? '1' : '0';
                            $dataArr[$_key][] = $Terminationed;
                        } else {
                            $dataArr[$_key][] = $_row[$__jsonName];
                        }
                    }
                    $dataArr[$_key][] = $this->reportId;
                }

                $this->insertData($this->table, $this->getSaveFieldArr(), $dataArr);
            }
        }
    }
}