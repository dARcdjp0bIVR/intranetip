<?
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampustv.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$lcampustv = new libcampustv();
//echo $channel_id;
$x = "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";

# Live Menu
if($lcampustv->live_show)
{
	$this_css = $channel_id==0 ? "indextvlink":"indextvbtn";
	$x .= "
			<tr>
				<td valign=\"middle\">
					<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
					<tr>
						<td width=\"15\"><img src=\"/images/2009a/index/campusTV/tv_btn_01.gif\" name=\"tv1\" id=\"tv1\" width=\"15\" height=\"23\"></td>
						<td align=\"left\" background=\"/images/2009a/index/campusTV/tv_btn_02.gif\" nowrap=\"nowrap\"><a href=\"javascript:clickTVcategory(0);\" class=\"". $this_css."\">". $Lang['Portal']['CampusTV']['Live'] ."</a></td>
						<td width=\"9\"><img src=\"/images/2009a/index/campusTV/tv_btn_03.gif\" width=\"9\" height=\"23\"></td>
					</tr>
					</table>
				</td>
			</tr>
			";
}

# Other channels
$tv_channels = $lcampustv->returnChannels();
if(sizeof($tv_channels))
{
	foreach($tv_channels as $key=>$data)
	{
		$this_css = $channel_id==$data['ChannelID'] ? "indextvlink":"indextvbtn";
		
		$x .= "
				<tr>
					<td valign=\"middle\">
						<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
						<tr>
							<td width=\"15\"><img src=\"/images/2009a/index/campusTV/tv_btn_01.gif\" name=\"tv1\" id=\"tv1\" width=\"15\" height=\"23\"></td>
							<td align=\"left\" background=\"/images/2009a/index/campusTV/tv_btn_02.gif\" nowrap=\"nowrap\"><a href=\"javascript:clickTVcategory(". $data['ChannelID'] .");\" class=\"". $this_css ."\">". $data['Title'] ."</a></td>
							<td width=\"9\"><img src=\"/images/2009a/index/campusTV/tv_btn_03.gif\" width=\"9\" height=\"23\"></td>
						</tr>
						</table>
					</td>
				</tr>
				";	
	}
}

$x .= "</table>";
intranet_closedb();

echo $x;
?>




