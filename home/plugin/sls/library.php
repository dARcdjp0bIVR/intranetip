<?php
/********************** Change Log ***********************/
#	Date:		2014-07-08 [Yuen]
#				cater SLS encoding (UCS-2LE) with a new config $sls_mapping_encoding which can be set in plugins/sls_conf.php

/********************** Change Log ***********************/

include ("../../../includes/global.php");
include ("../../../includes/libdb.php");
include ("../../../includes/libuser.php");
intranet_auth();
intranet_opendb();

if (isset($plugin['library']) && $plugin['library'])
{
    $lu = new libuser($UserID);
    
    $content = get_file_content("$mapping_file_path");
    if ($sls_mapping_encoding=="UCS-2LE")
    {
   		 $content = iconv("UCS-2LE", "UTF-8", $content);
    }
    $records = explode("\n",$content);
    $login = $lu->UserLogin;
    for ($i=0; $i<sizeof($records); $i++)
    {
         $data = explode("::", $records[$i]);
         $data[0] = trim($data[0]);
         $data[1] = trim($data[1]);
         if (strtolower($login) == strtolower($data[0]))
         {
             if ($intranet_session_language == "en")
             {
                 $lang_str = "1";
             }
             else
             {
                 $lang_str = "0";
             }
             #$path = "$library_http_path?oid=".$data[1].$lang_str;
             ?>
  <html>
  <title></title>
  <body onLoad=document.autoform.submit()>
  <form name=autoform ACTION="<?=$library_http_path?>" method=POST>
  <input type=hidden name="oid" value="<?=$data[1]?>">
  <input type=hidden name="lang" value="<?=$lang_str?>">
  </body>
  </html>
             <?
//           header ("Location: $library_http_path?oid=".$data[1]);
             break;
         }
    }
}
else
{
    header("Location: /");
}
intranet_closedb();
?>