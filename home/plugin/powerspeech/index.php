<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."templates/fileheader.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();


include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/popup_header.php");

/*
if(check_powerspeech_is_expired() == 1){
	?>
	<script>
		alert("<?=$Lang['TextToSpeech']['Expired']?>");
		window.close();
	</script>
	<?php
}*/

$portal_image = "/images/junior_$intranet_session_language"."/portal";

?>
<script language="JavaScript" type="text/javascript" src="../../../templates/swf_object/swfobject.js"></script>
<script language="Javascript" type="text/javascript" src="../../../templates/text_to_speech/text_to_speech.js"></script>
<script>
var defaultImagePath = "../../";

var IMAGE_PREFIX	= defaultImagePath;




TTS_Tool.play_content = function(target){
	
	resetReadableContent(target);
	
	//Play content
	TTS_Play('psTool', 0);
	
	//Override Commit function - so that the fulltext is up to date
	if(TTS_Players[target]){
		TTS_Players[target].CommitChange = function(){
			this.setSettings = true;
			this.voice = this.selectedVoice;
			this.speed = this.selectedSpeed;
			
			resetReadableContent(this.target);
			//2010-03-05 : Re-generate Player after applied settings
			TTS_Play(this.target, 0);
		}
	}
	
}
TTS_Tool.ApplyUpdate = function(target, commit){
	if(commit){
		resetReadableContent(target);
		TTS_Players[target].CommitChange();
	}
	var settingLayerID = "speech_setting_layer_" + target;
	MM_showHideLayers(settingLayerID,'','hide');	
}

function resetReadableContent(target){
	//Assign textarea content into hidden DIV
	if(TTS_Players[target]){
	//Format the text so that new lines are recognized (no need to parse HTML) - read exactly what the user inputed.
	fullText = Trim($("#enterText").val());
	//fullText = fullText.replace(/\r|\n|\s>/gi, " . ");
	//fullText = fullText.replace(/\s/g,'');
	//$("#psTool").text(fullText);
	//var fullText  = $("#psTool").html();
	
	//Assign new fulltext to the player
	TTS_Players[target].fullText = fullText;
	TTS_Players[target].playText = fullText;
	}
}

function hidePSTool(){
	
	if($("#divPlayer_psTool").css("display") != "none"){
			
		$("#divPlayer_psTool").attr("id", "old_divPlayer_psTool");		
		$("<div id='divPlayer_psTool'></div>").insertAfter("#old_divPlayer_psTool");
		$("#old_divPlayer_psTool").remove();
	}	
}

$(document).ready(function(){
		
	if($("#divPlayer_psTool").length > 0){		
		$("#divPlayer_psTool").remove();		
	}
	$("#show_player").attr("id", "divPlayer_psTool");
	
	//Init text to speech to display the icons
	init_textToSpeach(true);
});

</script>
<BR />
<div class="pop_header_title">
	<h1 class="title_powerspeech"><?=$Lang['Header']['Menu']['PowerSpeech']?></h1></div>
<div class="pop_content">
  <span class="pop_intrustion"><?=$Lang['TextToSpeech']['Instruction']?></span>
  <div class="pop_powerspeech_text"><textarea name="enterText" id="enterText" wrap="virtual" onKeyUp="hidePSTool();"></textarea></div>
    
  <div class="pop_powerspeech_control_bar">
	<div  class="pop_powerspeech_control">
		<div class="textToSpeech">
        
             <div class="speech_button_layer" >
             <?php echo generate_texttospeech_ui_IP("psTool", 0, 1,0,1);?>
             </div>
             <br style="clear:both" />
          </div>

	</div> 
		<div class="pop_powerspeech_play"><div id="show_player"></div>
		</div>
  </div>
</div>

</div>



<div id="psTool" style="display:none;"></div>

<?php
include("../../../templates/filefooter.php");
?>