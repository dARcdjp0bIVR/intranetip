<?php
/*
 * 2015-12-10 (Carlos): Applied IntegerSafe() to integer type parameters to prevent SQL injection.
 */
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libcampustv.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$UserID = IntegerSafe($UserID);

$lu = new libuser($UserID);
$system_upload_restricted = get_file_content("$intranet_root/file/campustvupload.txt");
if ($system_upload_restricted != 1 && $lu->isTeacherStaff())
{
    $allow_upload = true;
}
else
{
    $allow_upload = false;
}

if (!$allow_upload)
{
     header("/home/school/close.php");
     exit();
}

$lcampustv = new libcampustv();
$channels = $lcampustv->returnChannels();
$channel_select = getSelectByArray($channels,"name=ChannelID","",0,1);

if ($success==1)
{
    $signal_msg = "$i_CampusTV_UploadSuccessful";
    $display_msg = "<table width=100% border=0 cellpadding=5 cellspacing=0 bgcolor=#0099FF class=h-bold>
          <tr>
            <td align=center><font color=#FFFFFF>$signal_msg</font></td>
          </tr>
        </table><br>\n";
}



$body_tags = "background=\"$image_path/campus_tv/bg2.gif\" leftmargin=0 topmargin=0 marginwidth=0 marginheight=0";
include_once("../../../templates/fileheader.php");
?>
<script language="javascript">
function checkform(obj){
        if(!check_text(obj.ClipName, "<?php echo $i_alert_pleasefillin.$i_CampusTV_ClipName; ?>.")) return false;
}
</script>
    <form action="upload_update.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this);">
<table width="780" border="0" cellpadding="0" cellspacing="0" class="h1">
  <tr>
    <td width="92"><img src="<?=$image_path?>/campus_tv/other_graphic1.jpg" width="92" height="150"></td>
    <td width="600"><img src="<?=$image_path?>/campus_tv/other_graphic2_upload_<?=$intranet_session_language?>.jpg" width="600" height="150"></td>
    <td width="88"><img src="<?=$image_path?>/campus_tv/other_graphic3.jpg" width="88" height="150"></td>
  </tr>
  <tr>
    <td class=intranet_tv_other_cell_left>&nbsp;</td>
    <td valign="middle" bgcolor="#446EAF">
    <?=$display_msg?>
        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="bodytext">
          <tr>
            <td width="150" align="right"><font color="#FFFFFF"><?php echo $i_CampusTV_ClipName; ?>:</font></td>
            <td><input class=text type=text name=ClipName size=41 maxlength=200></td>
          </tr>
          <tr>
            <td align="right" valign="top"><font color="#FFFFFF"><?php echo $i_general_description; ?>:</font></td>
            <td><TEXTAREA name=Description rows=5 cols=40></TEXTAREA></textarea></td>
          </tr>
          <tr>
            <td align="right"><font color="#FFFFFF"><?php echo $i_CampusTV_ChannelName; ?>:</font></td>
            <td><?=$channel_select?></td>
          </tr>
          <tr>
            <td align="right"><font color="#FFFFFF"><?php echo $i_CampusTV_SelectMovieFile; ?>:</font><br></td>
            <td><input type=file name=ClipFile size=30></td>
          </tr>
          <tr>
            <td align="right"></td>
            <td align="right"><a href=javascript:history.back()><img src="<?=$image_path?>/btn_back_<?=$intranet_session_language?>.gif" border=0></a>
            <input type=image src="<?=$image_path?>/btn_upload_<?=$intranet_session_language?>.gif" border=0>
            </td>
          </tr>
        </table>
    </td>
    <td class=intranet_tv_other_cell_right><img src="<?=$image_path?>/spacer.gif" width="10" height="248"></td>
  </tr>
  <tr>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic4.jpg" width="92" height="152"></td>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic5.jpg" width="600" height="152"></td>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic6.jpg" width="88" height="152"></td>
  </tr>
</table>


</form>
<?
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>