<?php
//using: 
/*
 * 2015-12-10 (Carlos): Applied IntegerSafe() to integer type parameters to prevent SQL injection.
 */
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libcampustv.php");
include_once("../../../lang/lang.$intranet_session_language.php");

$ClipID = IntegerSafe($ClipID);
$archive = IntegerSafe($archive);
$channelID = IntegerSafe($channelID);

$lcampustv = new libcampustv();

if (!$lcampustv->publicAllowed)
{
     intranet_auth();
}
intranet_opendb();

if ($ClipID != "")
{
    $clip_info = $lcampustv->retrieveClipInfo($ClipID);
    $channelID = $clip_info[6];
    $archive = 1;
}

$alt_open = true;
$removePlayer=false;

if (!isset($archive) || $archive == 0)
{
    # Programme List
    $list = $lcampustv->programmeList;
    $table_list = "<tr>
      <td><img src=\"$image_path/campus_tv/head_tvlist_$intranet_session_language.gif\" width=144 height=44></td>
    </tr>\n";
    for ($i=0; $i<sizeof($list); $i++)
    {
         list($range,$title) = $list[$i];
         $table_list .= "<tr>
      <td><img src=\"$image_path/campus_tv/tvlist_top.gif\" width=144 height=8></td>
    </tr>\n";
         $table_list .= "<tr>
      <td align=center bgcolor=#8B7027><span class=s><font color=#FFFFFF>$range <br>$title</font></span></td>
    </tr>\n";
         $table_list .= "
    <tr>
      <td><img src=\"$image_path/campus_tv/tvlist_bottom.gif\" width=144 height=10></td>
    </tr>\n";
    }
    $movie_title = "$i_CampusTV_LiveBroadcast";
    $movie_url = $lcampustv->LiveURL;
    
    // added by PeterHO 19/07/2006
    if($movie_url==""){
    		header("Location: /home/plugin/campustv/?archive=1");
    }
}
else if ($channelID!="")
{
     # Clip List
     $channel_info = $lcampustv->retrieveChannelInfo($channelID);
    $table_list = "<tr>
      <td class=tv_list_title height=25>".$channel_info[0]." <br></td>
    </tr>\n";

    $clips = $lcampustv->returnClips($channelID);
    for ($i=0; $i<sizeof($clips); $i++)
    {
         list ($id,$name,$description,$recommended) = $clips[$i];
         $table_list .= "<tr>
      <td><img src=\"$image_path/campus_tv/channel_top.gif\" width=144 height=8></td>
    </tr>
    <tr>
      <td align=center bgcolor=#342685><a class=tv_link_new href=?archive=1&ClipID=$id>$name</a></td>
    </tr>
    <tr>
      <td><img src=\"$image_path/campus_tv/channel_bottom.gif\" width=144 height=10></td>
    </tr>\n";
    }
    // added by PeterHO 19/07/2006
    if(sizeof($clips)==0){
    	$table_list.="<tr><td align=center><font color=white><b>$i_CampusTV_Alert_No_Movies_Available</b></font></td></tr>";
    }

    $movie_title = "$i_CampusTV_SelectClip";
}
else
{
    # Channel List
    $table_list = "<tr>
      <td><img src=\"$image_path/campus_tv/head_channel_$intranet_session_language.gif\"></td>
    </tr>\n";

    $channels = $lcampustv->returnChannels();
    for ($i=0; $i<sizeof($channels); $i++)
    {
         list ($id,$name) = $channels[$i];
         $table_list .= "<tr>
      <td><img src=\"$image_path/campus_tv/channel_top.gif\" width=144 height=8></td>
    </tr>
    <tr>
      <td align=center bgcolor=#342685><a class=tv_link_new href=?archive=1&channelID=$id>$name</a></td>
    </tr>
    <tr>
      <td><img src=\"$image_path/campus_tv/channel_bottom.gif\" width=144 height=10></td>
    </tr>\n";
    }
    // added by PeterHO 19/07/2006
    if(sizeof($channels)==0){
    	$table_list.="<tr><td align=center><font color=white><b>$i_CampusTV_Alert_No_Channels_Available</b></font></td></tr>";
    }
    $movie_title = "$i_CampusTV_SelectChannel";
    $alt_open = false;
}

if ($archive != 0 && $clip_info[0]!="")
{
    $movie_title = $clip_info[0];
    $movie_url = $clip_info[2];
}else if($archive!=0){
		$removePlayer=true;
}

$imgfile = get_file_content($intranet_root."/file/schoolbadge.txt");
$logo_img = ($imgfile == "" ?"$image_path/intranet_campustv/sch_logo.gif" : "/file/$imgfile");

$body_tags = "background=\"$image_path/campus_tv/bg2.gif\" leftmargin=0 topmargin=0 marginwidth=0 marginheight=0";
include_once("../../../templates/fileheader.php");
?>
<div id="Layer3" style="position:absolute; left:0px; top:0px; width:165px; height:96px; z-index:2">
  <table width="195" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td colspan="3"><img src="<?=$image_path?>/campus_tv/badge_top.gif" width="195" height="18"></td>
    </tr>
    <tr>
      <td width="22"><img src="<?=$image_path?>/campus_tv/badge_left.gif" width="22" height="68"></td>
      <td width="151" align="center" valign="middle" bgcolor="#F3ECD4"><img src="<?=$logo_img?>" width="120" height="60"></td>
      <td width="22"><img src="<?=$image_path?>/campus_tv/badge_right.gif" width="22" height="68"></td>
    </tr>
    <tr>
      <td colspan="3"><img src="<?=$image_path?>/campus_tv/badge_bottom_<?=$intranet_session_language?>.gif" width="195" height="27"></td>
    </tr>
  </table>
</div>
<div id="Layer2" style="position:absolute; left:92px; top:167px; width:161px; height:340px; z-index:1; overflow: auto">
  <table width="144" border="0" cellpadding="0" cellspacing="0" bgcolor="#363636" class="bodytext">
    <?=$table_list?>
  </table>
</div>
<table width="780" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2"><table width="780" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="3"><img src="<?=$image_path?>/campus_tv/tv_graphic1.jpg" width="780" height="32"></td>
        </tr>
        <tr>
          <td width="196"><img src="<?=$image_path?>/campus_tv/tv_graphic2.jpg" width="196" height="36"></td>
          <td width="432" class=intranet_tv_movie_title><B><?=$movie_title?></B></td>
          <td width="152"><img src="<?=$image_path?>/campus_tv/tv_graphic4.jpg" width="152" height="36"></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td width="275"><table width="275" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2"><img src="<?=$image_path?>/campus_tv/tv_graphic5.jpg" width="275" height="58"></td>
        </tr>
        <tr>
          <td width="60"><img src="<?=$image_path?>/campus_tv/tv_graphic6.jpg" width="60" height="34"></td>
          <td width="215" align="center" valign="middle" bgcolor="#DAD3BA"><a href="?live=1"><img border=0 src="<?=$image_path?>/campus_tv/btn_tvlist.gif" width="30" height="27" align="absmiddle">
            <span class="body"><?=$i_CampusTV_LiveProgrammeList?></span></a><a href="?archive=1"><img border=0 src="<?=$image_path?>/campus_tv/btn_channel.gif" width="42" height="27" align="absmiddle"><span class="body"><?=$i_CampusTV_ChannelList?></span></a></td>
        </tr>
        <tr>
          <td><img src="<?=$image_path?>/campus_tv/tv_graphic7.jpg" width="60" height="343"></td>
          <td><img src="<?=$image_path?>/campus_tv/tv_graphic8.gif" width="215" height="343"></td>
        </tr>
      </table></td>
    <td width="505"><table width="505" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="58"><img src="<?=$image_path?>/campus_tv/tv_tv1.jpg" width="58" height="33"></td>
          <td width="360"><img src="<?=$image_path?>/campus_tv/tv_tv2.jpg" width="360" height="33"></td>
          <td width="87"><img src="<?=$image_path?>/campus_tv/tv_tv3.jpg" width="87" height="33"></td>
        </tr>

        
        <tr>
          <td><img src="<?=$image_path?>/campus_tv/tv_tv4.jpg" width="58" height="360"></td>
          <td class=intranet_tv_movie><? if(!$removePlayer){?>
          <? if ($tv_playertype == "QuickTime" && $tv_quicktime_special_embed)
          {
                # For UCCKE
                $faked_filename = "schoolsong.mov";
                 $pos = strpos($movie_url,"//");
                if ($pos === false)
                {
                        $url_nop = $movie_url;
                }
                else
                {
                        $url_nop = substr($pos+2,$movie_url);
                }

          ?>
<PARAM name="SRC" VALUE="<?=$movie_url?>">
<PARAM name="AUTOPLAY" VALUE="true">
<PARAM name="CONTROLLER" VALUE="true">
<EMBED SRC="<?=$faked_filename?>" QTSRC="<?=$movie_url?>" WIDTH="352" HEIGHT="357" AUTOPLAY="true" CONTROLLER="true" PLUGINSPAGE="http://www.apple.com/quicktime/download/"
kioskmode="true">
</EMBED>
                    <?
          }
          else if ($tv_playertype == "QuickTime")
          {
                 $pos = strpos($movie_url,"//");
                if ($pos === false)
                {
                        $url_nop = $movie_url;
                }
                else
                {
                        $url_nop = substr($pos+2,$movie_url);
                }

          ?>

<OBJECT CLASSID="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" WIDTH="352" HEIGHT="357"
CODEBASE="http://www.apple.com/qtactivex/qtplugin.cab">
<PARAM name="SRC" VALUE="<?=$movie_url?>">
<PARAM name="QTSRC" VALUES="<?=$url_nop?>">
<PARAM name="AUTOPLAY" VALUE="true">
<PARAM name="CONTROLLER" VALUE="true">
<EMBED SRC="<?=$movie_url?>" QTSRC="<?=$url_nop?>" WIDTH="352" HEIGHT="357" AUTOPLAY="true" CONTROLLER="true" PLUGINSPAGE="http://www.apple.com/quicktime/download/"
kioskmode="true">
</EMBED>
</OBJECT>
                    <?
          }
          else
          {

              if ($tv_secure_ftp_user != "" && $UserID != "" && $clip_info[8]==1)
              {
                  $secret_word = $tv_secure_secret;
                  $current_time = time();
                  $cipher = md5($current_time . $secret_word);
                  $cipherStr = "?cipher=$current_time+$cipher";
              }
              else
              {
                  $cipherStr = "";
              }

          ?>
        <OBJECT ID="MediaPlayer1" width=352 height=357
             classid="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95"
             codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715"
                standby="Loading MicrosoftR WindowsR Media Player components..."
                type="application/x-oleobject">
          <PARAM NAME="FileName" VALUE="<?="$movie_url$cipherStr"?>">
          <PARAM NAME="AutoStart" VALUE="true">
          <param name="AllowChangeDisplaySize" value="-1">
          <param name="ShowControls" value="true">
          <param name="Mute" value="0">
          <PARAM NAME="ShowStatusBar" VALUE="True">
          <EMBED type="application/x-mplayer2"
             pluginspage = "http://www.microsoft.com/Windows/Downloads/Contents/Products/MediaPlayer/"
             SRC="<?="$movie_url$cipherStr"?>"
             name="MediaPlayer1"
             width=352
             height=357
             AutoStart=true>
          </EMBED>
        </OBJECT>
      <? } }
      		else{  # when the player is removed
      			
      						if($channelID==""&&$ClipID==""){
      								echo "<center><font color=white size=14><b>$i_CampusTV_Guide_Please_Select_Channel</b></font></center>";
      						}
      						else if($ClipID==""){
		      						echo "<center><font color=white size=14><b>$i_CampusTV_Guide_Please_Select_Movie</b></font></center>";
      						}
      				}
        ?>
          </td>
          <td><img src="<?=$image_path?>/campus_tv/tv_tv6.jpg" width="87" height="360"></td>
        </tr>
        <tr>
          <td><img src="<?=$image_path?>/campus_tv/tv_tv7.jpg" width="58" height="42"></td>
          <td><img src="<?=$image_path?>/campus_tv/tv_tv8.jpg" width="360" height="42"></td>
          <td><img src="<?=$image_path?>/campus_tv/tv_tv9.jpg" width="87" height="42"></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td colspan="2"><table width="780" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="727" class=intranet_tv_bottom><font size="2"><?
          if ($alt_open)
          {
              $text = "$i_CampusTV_Failed <a href=\"$movie_url$cipherStr\" class=list_link>$i_CampusTV_alt</a>";
              echo $text;
          }
          ?>
          </font></td>
          <td width="53"><img src="<?=$image_path?>/campus_tv/tv_graphic10.jpg" width="53" height="28"></td>
        </tr>
        <tr>
          <td colspan="2"><img src="<?=$image_path?>/campus_tv/tv_graphic11.jpg" width="780" height="19"></td>
        </tr>
      </table></td>
  </tr>
</table>
<?
include_once("../../../templates/filefooter.php");
?>