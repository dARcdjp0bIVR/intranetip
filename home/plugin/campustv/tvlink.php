<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libcampustv.php");

$lcampustv = new libcampustv();

$data = $lcampustv->getMovieStat();
print_r ($data);

echo "Order by Times <br>\n";
$byTimes = sortByColumn($data,1,0);
echo printTable($byTimes);

echo "Original <br>\n";
echo printTable($data);

echo "Order by Duration <br>\n";
$byDuration = sortByColumn($data,2,0);
echo printTable($byDuration);


function printTable($array)
{
         $x = "<table width=50% border=1>\n";

         while (list($key, $value) = each ($array))
         {
              $x .= "<tr>";
              for ($j=0; $j<sizeof($array[$key]); $j++)
              {
                   $x .= "<td>".$array[$key][$j]."</td>";
              }
              $x .= "</tr>\n";
         }
         $x .="</table>\n";
         return $x;
}
?>
