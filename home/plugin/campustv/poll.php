<?php
/*
 * 2015-12-10 (Carlos): Applied IntegerSafe() to integer type parameters to prevent SQL injection.
 */
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libcampustv.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$UserID = IntegerSafe($UserID);
$PollingID = IntegerSafe($PollingID);

$lu = new libuser($UserID);

$lcampustv = new libcampustv();
$polling = $lcampustv->returnPolling($PollingID,$UserID);
list($id,$question,$start,$end,$ref,$answer,$options) = $polling;
if ($answer!="")
{
    header("Location: result.php?PollingID=$PollingID");
    exit();
}

$chk = "CHECKED";
$x = "";
$x .= "<p><font color=#85E9FB size=4><strong>$question</strong></font></p>";
for ($i=0; $i<sizeof($options); $i++)
{
     list($opOrder,$clipID,$ClipURL,$Title) = $options[$i];
     $link = "<a class=tv_poll_link href=javascript:open_tv($clipID)>$Title</a>";
#     $link = "<a target=_blank href=\"$ClipURL\"><img alt='$i_CampusTV_Play' border=0 src=$image_path/btn_next.gif></a>";
     $x .= "<p><font color=#85E9FB><input $chk type=radio name=vote value=$opOrder>$link</font></p>\n";
     $chk = "";
}

$body_tags = "background=\"$image_path/campus_tv/bg2.gif\" leftmargin=0 topmargin=0 marginwidth=0 marginheight=0";
include_once("../../../templates/fileheader.php");
?>
<SCRIPT LANGUAGE=Javascript>
function open_tv(id)
{
         tv_window = window.open('/home/plugin/campustv/?archive=1&ClipID='+id,'tv_window','scrollbars=0,toolbar=0,menubar=0,resizable=1,dependent=0,status=0,width=780,height=550,left=25,top=25')
}
</SCRIPT>

<form name="form1" method="post" action="poll_update.php">
<table width="780" border="0" cellpadding="0" cellspacing="0" class="h1">
  <tr>
    <td width="92"><img src="<?=$image_path?>/campus_tv/other_graphic1.jpg" width="92" height="150"></td>
    <td width="600"><table width="600" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2"><img src="<?=$image_path?>/campus_tv/other_graphic2_polling_<?=$intranet_session_language?>.jpg" width="600" height="117"></td>
        </tr>
        <tr>
          <td width="400"><img src="<?=$image_path?>/campus_tv/other_graphic2_pollingcurrent_<?=$intranet_session_language?>.jpg" width="400" height="33"></td>
          <td width="200" align="right" valign="middle" bgcolor="#446EAF" class="body">
          <a href=pastpoll.php>
          <font color="#FFFFFF" size="2"><img border=0 src="<?=$image_path?>/campus_tv/btn_pastpolling.gif" width="23" height="23" align="absmiddle">
            <?=$i_CampusTV_PastPoll?></font></a>
          </td>
        </tr>
      </table></td>
    <td width="88"><img src="<?=$image_path?>/campus_tv/other_graphic3.jpg" width="88" height="150"></td>
  </tr>
  <tr>
    <td class=intranet_tv_other_cell_left>&nbsp;</td>
    <td valign="middle" bgcolor="#446EAF">
      <table width="100%" border="0" cellpadding="5" cellspacing="0" class="body">
        <tr>
          <td>
              <?=$x?>
          </td>
        </tr>
        <tr>
          <td align="right"><a href=javascript:history.back()><img src="<?=$image_path?>/btn_back_<?=$intranet_session_language?>.gif" border=0 ></a>
            <input type=image src="<?=$image_path?>/btn_poll_<?=$intranet_session_language?>.gif" border=0> </td>
        </tr>
      </table>
    </td>
    <td class=intranet_tv_other_cell_right><img src="<?=$image_path?>/spacer.gif" width="10" height="248"></td>
  </tr>
  <tr>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic4.jpg" width="92" height="152"></td>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic5.jpg" width="600" height="152"></td>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic6.jpg" width="88" height="152"></td>
  </tr>
</table>
<input type=hidden name=PollingID value="<?=$PollingID?>">
            </form>
<?
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>