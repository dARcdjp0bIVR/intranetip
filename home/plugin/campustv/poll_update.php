<?
# using: 

############################################
# 	2015-12-10 (Carlos): Applied IntegerSafe() to integer type parameters to prevent SQL injection.
#	Date:	2014-05-21	YatWoon
#			Improved: add duplicate record checking [Case#H61673]
############################################
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libcampustv.php");
intranet_auth();
intranet_opendb();

$PollingID = IntegerSafe($PollingID);
$UserID = IntegerSafe($UserID);

$lcampustv = new libcampustv();

# check duplicate or not
$sql = "select count(*) from INTRANET_TV_POLLING_RESULT where PollingID='$PollingID' and UserID='". $UserID ."'";
$result = $lcampustv->returnVector($sql);
if(!$result[0])
{
	$fields = "PollingID,AnswerOptionOrder,UserID,DateInput,DateModified";
	$values = "'$PollingID','$vote','$UserID',now(),now()";
	$sql = "INSERT INTO INTRANET_TV_POLLING_RESULT ($fields) VALUES ($values)";
	$lcampustv->db_db_query($sql);
}


intranet_closedb();
header("Location: result.php?PollingID=$PollingID");
?>
