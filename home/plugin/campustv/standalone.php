<?php
/*
 * 2015-12-10 (Carlos): Applied IntegerSafe() to integer type parameters to prevent SQL injection.
 */
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libcampustv.php");
include_once("../../../lang/lang.$intranet_session_language.php");

intranet_opendb();

$UserID = IntegerSafe($UserID);

$lcampustv = new libcampustv();

if (!$lcampustv->publicAllowed)
{
     intranet_auth();
}
intranet_opendb();

if ($UserID != "")
{
    $lu = new libuser($UserID);
    $system_upload_restricted = get_file_content("$intranet_root/file/campustvupload.txt");
    if ($system_upload_restricted != 1 && $lu->isTeacherStaff())
    {
        $allow_upload = true;
    }
    else
    {
        $allow_upload = false;
    }

    $allow_bulletin = !$lcampustv->bulletinDisabled;
    $allow_polling = !$lcampustv->pollingDisabled;
}
else
{
    $allow_upload = false;
    $allow_bulletin = false;
    $allow_polling = false;
}
$body_tags = "background=\"$image_path/campus_tv/bg2.gif\" leftmargin=0 topmargin=0 marginwidth=0 marginheight=0";
$imgfile = get_file_content($intranet_root."/file/schoolbadge.txt");
$logo_img = ($imgfile == "" ?"/images/header/logo_eclassintranet.gif" : "/file/$imgfile");

include_once("../../../templates/fileheader.php");
?>
<table width="780" border="0" cellspacing="0" cellpadding="0" background="/images/header/bg_gradient3.gif">
  <tr>
    <td width="70"><img src="/images/header/school.gif"></td>
    <td width="120" STYLE="background-image: url(images/cloud.gif)"><a href=/home/><img src="<?=$logo_img?>" width=120 height=60 border=0></a></td>
    <td width="75"><img src="/images/header/mr_wong.gif"></td>
    <td width="515" align="right" valign="bottom">
    </td>
  </tr>
</table>
<table width="780" border="0" cellspacing="0" cellpadding="0" background="images/header_bar.gif">
<?


if (is_file("$intranet_root/images/school_name_$intranet_session_language.gif"))
{
    $school_name = "<tr>
                      <td colspan=3 align=right STYLE='background-image: url(/images/school_name_$intranet_session_language.gif)' height=18>$alert_icon_list &nbsp;".toolBarSpacer().toolBarSpacer().toolBarSpacer()."</td>
                    </tr>\n";
}
else
{
    $school_name = "<tr>
                      <td colspan=3 align=right height=18>$alert_icon_list &nbsp;".toolBarSpacer().toolBarSpacer().toolBarSpacer()."</td>
                    </tr>\n";
}
?>
  <?=$school_name?>
  <tr>
    <td width="10">&nbsp;</td>
    <td align=right width="100%">
    <?
        if (!isset($intranet_default_lang_set)) $intranet_default_lang_set = array("b5");
        $button_b5 = (in_array("b5",$intranet_default_lang_set)? "<a href=/lang.php?lang=b5><img src=\"/images/header/icon_big5.gif\" border=0></a>":"");
        $button_en = "<a href=\"/lang.php?lang=en\"><img src=\"/images/header/icon_eng.gif\" border=0></a>"; # "<a href=/lang.php?lang=en><img src=\"/images/header/icon_eng.gif\" border=0></a>";
        $button_gb = (in_array("gb",$intranet_default_lang_set)? "<a href=/lang.php?lang=gb><img src=\"/images/header/icon_gb.gif\" border=0></a>":"");
    ?>
    <?php if($intranet_session_language=="en") { echo "$button_b5$button_gb"; } ?>
    <?php if($intranet_session_language=="b5") { echo "$button_en$button_gb"; } ?>
    <?php if($intranet_session_language=="gb") { echo "$button_en$button_b5"; } ?>
    <a href=/logout.php onClick="if(confirm(globalAlertMsg16)){return true;}else{return false;}"><?php echo $i_frontpage_menu_exit; ?></a></td>
    <td width="10">&nbsp;</td>
  </tr>
</table>

<SCRIPT LANGUAGE=Javascript>
function open_tv()
{
         tv_window = window.open('/home/plugin/campustv/','tv_window','scrollbars=0,toolbar=0,menubar=0,resizable=1,dependent=0,status=0,width=780,height=550,left=25,top=25')
}
</SCRIPT>
<table width="780" border="0" cellpadding="0" cellspacing="0" class="h1">
  <tr>
    <td width="110"><img src="<?=$image_path?>/campus_tv/main_graphic1.jpg" width="110" height="150"></td>
    <td width="560"><img src="<?=$image_path?>/campus_tv/main_graphic2_<?=$intranet_session_language?>.jpg" width="560" height="150"></td>
    <td width="110"><img src="images/main_graphic3.jpg" width="110" height="150"></td>
  </tr>
  <tr>
    <td><img src="<?=$image_path?>/campus_tv/main_graphic4.jpg" width="110" height="248"></td>
    <td class=intranet_tv_menu>
<img src="<?=$image_path?>/campus_tv/bullet.gif" width="34" height="20" align="absmiddle"><font color="#FFFFFF" size="4"><a class=tv_menu_link href=javascript:open_tv()><?=$i_CampusTV_ViewProgrammer?></a></font><br><br>
<img src="<?=$image_path?>/campus_tv/bullet.gif" width="34" height="20" align="absmiddle"><font color="#FFFFFF" size="4"><a class=tv_menu_link href=javascript:newWindowNotClose('recommend.php',10)><?=$i_CampusTV_RecommendedList?></a></font><br><br>
<? if ($tv_log_user!="") { ?>
<img src="<?=$image_path?>/campus_tv/bullet.gif" width="34" height="20" align="absmiddle"><font color="#FFFFFF" size="4"><a class=tv_menu_link href=javascript:newWindowNotClose('mostchannel.php',10)><?=$i_CampusTV_MostChannel?></a></font><br><br>
<img src="<?=$image_path?>/campus_tv/bullet.gif" width="34" height="20" align="absmiddle"><font color="#FFFFFF" size="4"><a class=tv_menu_link href=javascript:newWindowNotClose('mosthit.php',10)><?=$i_CampusTV_MostClip?></a></font><br><br>
<? } ?>
<? if ($allow_upload) { ?>
<img src="<?=$image_path?>/campus_tv/bullet.gif" width="34" height="20" align="absmiddle"><font color="#FFFFFF" size="4"><a class=tv_menu_link href=javascript:newWindowNotClose('upload.php',10)><?=$i_CampusTV_Upload?></a></font><br><br>
<? } ?>
<? if ($allow_bulletin) { ?>
<img src="<?=$image_path?>/campus_tv/bullet.gif" width="34" height="20" align="absmiddle"><font color="#FFFFFF" size="4"><a class=tv_menu_link href=javascript:newWindowNotClose('bulletin.php',10)><?=$i_CampusTV_Bulletin?></a></font><br><br>
<? } ?>
<? if ($allow_polling) { ?>
<img src="<?=$image_path?>/campus_tv/bullet.gif" width="34" height="20" align="absmiddle"><font color="#FFFFFF" size="4"><a class=tv_menu_link href=javascript:newWindowNotClose('viewpoll.php',10)><?=$i_CampusTV_Polling?></a></font><br><br>
<? } ?>
</td>
    <td><img src="images/main_graphic6.jpg" width="110" height="248"></td>
  </tr>
  <tr>
    <td><img src="images/main_graphic7.jpg" width="110" height="152"></td>
    <td><img src="images/main_graphic8.jpg" width="560" height="152"></td>
    <td><img src="images/main_graphic9.jpg" width="110" height="152"></td>
  </tr>
</table>
<?
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>