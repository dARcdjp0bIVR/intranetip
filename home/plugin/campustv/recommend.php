<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libcampustv.php");
include_once("../../../lang/lang.$intranet_session_language.php");

$lcampustv = new libcampustv();

if (!$lcampustv->publicAllowed)
{
     intranet_auth();
}
intranet_opendb();

$clips = $lcampustv->returnRecommendList();

$x .= "<table width=100% border=0 cellpadding=3 cellspacing=0 class=bodytext>\n";
for ($i=0; $i<sizeof($clips); $i++)
{
     list($ClipID,$ClipName,$ClipDescription,$Channel,$ClipURL) = $clips[$i];
     $link = "<a class=tv_recommend_link href=javascript:open_tv($ClipID)>$ClipName</a>";
     $link1 = "<a class=tv_recommend_link href=javascript:open_tv($ClipID)>".($i+1).".</a>";
     $x .= "<tr class=tv_recommend_link align=center valign=top>\n";
     $x .= "<td width=40 align=right>$link1</td>\n";
     $x .= "<td align=left valign=top>$link</td>\n</tr>\n";
}
$x .= "</table>\n";

$imgfile = get_file_content($intranet_root."/file/schoolbadge.txt");
$logo_img = ($imgfile == "" ?"$image_path/intranet_campustv/sch_logo.gif" : "/file/$imgfile");

$body_tags = "background=\"$image_path/campus_tv/bg2.gif\" leftmargin=0 topmargin=0 marginwidth=0 marginheight=0";

include_once("../../../templates/fileheader.php");
?>
<SCRIPT LANGUAGE=Javascript>
function open_tv(id)
{
         tv_window = window.open('/home/plugin/campustv/?archive=1&ClipID='+id,'tv_window','scrollbars=0,toolbar=0,menubar=0,resizable=1,dependent=0,status=0,width=780,height=550,left=25,top=25')
}
</SCRIPT>
<table width="780" border="0" cellpadding="0" cellspacing="0" class="h1">
  <tr>
    <td width="92"><img src="<?=$image_path?>/campus_tv/other_graphic1.jpg" width="92" height="150"></td>
    <td width="600"><img src="<?=$image_path?>/campus_tv/other_graphic2_recommend_<?=$intranet_session_language?>.jpg" width="600" height="150"></td>
    <td width="88"><img src="<?=$image_path?>/campus_tv/other_graphic3.jpg" width="88" height="150"></td>
  </tr>
  <tr>
    <td class=intranet_tv_other_cell_left>&nbsp;</td>
    <td valign="middle" bgcolor="#446EAF">
    <?=$x?>
    </td>
    <td class=intranet_tv_other_cell_right><img src="<?=$image_path?>/spacer.gif" width="10" height="248"></td>
  </tr>
  <tr>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic4.jpg" width="92" height="152"></td>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic5.jpg" width="600" height="152"></td>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic6.jpg" width="88" height="152"></td>
  </tr>
</table>
<?
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>