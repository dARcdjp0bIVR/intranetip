<?php
// Editing by 
/*
 * 2015-12-10 (Carlos): Applied IntegerSafe() to integer type parameters to prevent SQL injection.
 */
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libcampustv.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$UserID = IntegerSafe($UserID);
$PollingID = IntegerSafe($PollingID);

$lu = new libuser($UserID);

$lcampustv = new libcampustv();

$polling = $lcampustv->returnPolling($PollingID,$UserID);

$result = $lcampustv->returnPollingResult($PollingID);

if ($polling[0]=="")
{
    header("Location: /home/school/close.php");
    exit();
}
list($id,$que,$start,$end,$ref,$answer,$options) = $polling;

$x = "";
$max = 0;
$total = 0;
for ($i=0; $i<sizeof($result); $i++)
{
     list($opOrder,$count,$ClipURL,$ClipName,$ClipID) = $result[$i];
     if ($max < $count) $max = $count;
     $total += $count;
}
if ($total == 0)
{
    $x .= "<tr><td><font color=#85E9FB>$i_CampusTV_NoVote</font></td></tr>\n";
}
else
{

for ($i=0; $i<sizeof($result); $i++)
{
     list($opOrder,$count,$ClipURL,$ClipName,$ClipID) = $result[$i];
     $percentage[$i] = round(100 * $count / $total,1);
     $link = "<a class=tv_poll_link href=javascript:open_tv($ClipID)>$ClipName</a>";
     $x .= "<tr>
                  <td class=td_left_middle><font color=#85E9FB>$link </font></td>
                  <td><table width=200 height=35 border=2 cellpadding=0 cellspacing=0 bordercolor=#85E9FB>
                      <tr>
                        <td bgcolor=#444444><table width=\"".$percentage[$i]."%\" height=100% border=0 cellpadding=0 cellspacing=0>
                            <tr>
                              <td class=intranet_tv_poll_result_bg><img src=\"$image_path/spacer.gif\" width=1 height=35></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                  <td><font color=#85E9FB>".$percentage[$i]."% ($count)</font></td>
                </tr>\n";
}
     $x .= "<tr>
                  <td class=td_left_middle></td>
                  <td align=right><font color=#85E9FB>$list_total: </font></td>
                  <td><font color=#85E9FB>$total</font></td>
                </tr>\n";
}


$body_tags = "background=\"$image_path/campus_tv/bg2.gif\" leftmargin=0 topmargin=0 marginwidth=0 marginheight=0";
include_once("../../../templates/fileheader.php");
?>
<SCRIPT LANGUAGE=Javascript>
function open_tv(id)
{
         tv_window = window.open('/home/plugin/campustv/?archive=1&ClipID='+id,'tv_window','scrollbars=0,toolbar=0,menubar=0,resizable=1,dependent=0,status=0,width=780,height=550,left=25,top=25')
}
</SCRIPT>
<table width="780" border="0" cellpadding="0" cellspacing="0" class="h1">
  <tr>
    <td width="92"><img src="<?=$image_path?>/campus_tv/other_graphic1.jpg" width="92" height="150"></td>
    <td width="600"><table width="600" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2"><img src="<?=$image_path?>/campus_tv/other_graphic2_polling_<?=$intranet_session_language?>.jpg" width="600" height="117"></td>
        </tr>
        <tr>
          <td width="400"><img src="<?=$image_path?>/campus_tv/other_graphic2_pollingresult_<?=$intranet_session_language?>.jpg" width="400" height="33"></td>
          <td width="200" align="right" valign="middle" bgcolor="#446EAF" class="body">
          <a href=pastpoll.php>
          <font color="#FFFFFF" size="2"><img border=0 src="<?=$image_path?>/campus_tv/btn_pastpolling.gif" width="23" height="23" align="absmiddle">
            <?=$i_CampusTV_PastPoll?></font></a>
          </td>
        </tr>
      </table></td>
    <td width="88"><img src="<?=$image_path?>/campus_tv/other_graphic3.jpg" width="88" height="150"></td>
  </tr>
  <tr>
    <td class=intranet_tv_other_cell_left>&nbsp;</td>
    <td valign="middle" bgcolor="#446EAF">
      <table width="100%" border="0" cellpadding="5" cellspacing="0" class="body">
        <tr>
          <td>
              <?=$x?>
          </td>
        </tr>
      </table>
    </td>
    <td class=intranet_tv_other_cell_right><img src="<?=$image_path?>/spacer.gif" width="10" height="248"></td>
  </tr>
  <tr>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic4.jpg" width="92" height="152"></td>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic5.jpg" width="600" height="152"></td>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic6.jpg" width="88" height="152"></td>
  </tr>
</table>
<?
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>