<?php
/*
 * 2015-12-10 (Carlos): Applied IntegerSafe() to integer type parameters to prevent SQL injection.
 */
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libcampustv.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$UserID = IntegerSafe($UserID);

$lu = new libuser($UserID);

$lcampustv = new libcampustv();

$polls = $lcampustv->getCurrentPolls($UserID);
$x = "";
if (sizeof($polls)==0)
{
    $x .= "<font color=#85E9FB>";
    $x .= $i_CampusTV_NoPolling;
    $x .= "</font>\n";
}
else
{
    for ($i=0; $i<sizeof($polls); $i++)
    {
         list($id,$question,$start,$end,$ref) = $polls[$i];
         $x .= "<p>";
         $x .= "<font color=#85E9FB>";
         $x .= ($i+1);
         $x .= ". <a class=tv_poll_name href=poll.php?PollingID=$id>$question</a>";
         $x .= "</font></p>\n";
    }
}


$body_tags = "background=\"$image_path/campus_tv/bg2.gif\" leftmargin=0 topmargin=0 marginwidth=0 marginheight=0";
#$body_tags = "background=\"$image_path/intranet_campustv/poll_bg.gif\" leftmargin=0 topmargin=0 marginwidth=0 marginheight=0 onLoad=\"MM_preloadImages('$image_path/intranet_campustv/poll_btn_past_o_$intranet_session_language.gif','$image_path/intranet_campustv/btn_close_o_$intranet_session_language.gif','$image_path/intranet_campustv/poll_btn_current_o_$intranet_session_language.gif')\"";
include_once("../../../templates/fileheader.php");
?>
<table width="780" border="0" cellpadding="0" cellspacing="0" class="h1">
  <tr>
    <td width="92"><img src="<?=$image_path?>/campus_tv/other_graphic1.jpg" width="92" height="150"></td>
    <td width="600"><table width="600" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2"><img src="<?=$image_path?>/campus_tv/other_graphic2_polling_<?=$intranet_session_language?>.jpg" width="600" height="117"></td>
        </tr>
        <tr>
          <td width="400"><img src="<?=$image_path?>/campus_tv/other_graphic2_pollingcurrent_<?=$intranet_session_language?>.jpg" width="400" height="33" ></td>
          <td width="200" align="right" valign="middle" bgcolor="#446EAF" class="body">
          <a href=pastpoll.php>
          <font color="#FFFFFF" size="2"><img border=0 src="<?=$image_path?>/campus_tv/btn_pastpolling.gif" width="23" height="23" align="absmiddle">
            <?=$i_CampusTV_PastPoll?></font></a>
          </td>
        </tr>
      </table></td>
    <td width="88"><img src="<?=$image_path?>/campus_tv/other_graphic3.jpg" width="88" height="150"></td>
  </tr>
  <tr>
    <td class=intranet_tv_other_cell_left>&nbsp;</td>
    <td valign="middle" bgcolor="#446EAF">
      <table width="100%" border="0" cellpadding="5" cellspacing="0" class="body">
        <tr>
          <td>
              <?=$x?>
          </td>
        </tr>
      </table>
    </td>
    <td class=intranet_tv_other_cell_right><img src="<?=$image_path?>/spacer.gif" width="10" height="248"></td>
  </tr>
  <tr>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic4.jpg" width="92" height="152"></td>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic5.jpg" width="600" height="152"></td>
    <td><img src="<?=$image_path?>/campus_tv/other_graphic6.jpg" width="88" height="152"></td>
  </tr>
</table>

<?
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>