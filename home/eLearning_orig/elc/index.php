<?php


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libelc.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageMyeClass";

$li 	= new libuser($UserID);
$lo 	= new libeclass();
$leclass = new libeclass2007();
$libelc = new libelc($access2readingrm, $access2elprm, $access2ssrm);

# Special Rooms
$header_leclass = new libeclass();
$readingRmID = 1;
$sql = "SELECT a.user_course_id, b.RoomType,
          if(b.StartDate IS NULL OR UNIX_TIMESTAMP(b.StartDate)=0 OR UNIX_TIMESTAMP(now())>UNIX_TIMESTAMP(b.StartDate), 1, 0),
          if(b.EndDate IS NULL OR UNIX_TIMESTAMP(b.EndDate)=0 OR UNIX_TIMESTAMP(now())<UNIX_TIMESTAMP(b.EndDate), 1, 0)
        FROM $eclass_db.user_course as a
        LEFT OUTER JOIN $eclass_db.course as b ON a.course_id = b.course_id
        WHERE a.status IS NULL AND (b.RoomType>0) AND a.user_email = '".$li->UserEmail."'";
$temp = $libelc->returnArray($sql, 4);

for ($i=0; $i<sizeof($temp); $i++)
{
  if ($temp[$i][1]==1) {
    $access2readingrm = true;
    $readingRmID = $temp[$i][0];
  } elseif ($temp[$i][1]==2 && $header_leclass->license_elp!=0) {
    $access2elprm = true;
  } elseif ($temp[$i][1]==3 && $temp[$i][2] && $temp[$i][3]) {
    $access2ssrm = true;
  }
}
$access2specialrm = ($access2readingrm || $access2elprm || $access2ssrm);

### Title ###
$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$ip20TopMenu['eLC'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ['title'] = $ip20TopMenu['eLC'];
$CurrentPageArr['eLC'] = 1;
//$MODULE_OBJ = $libelc->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

if ($access2readingrm) {
  $SpecialRmArr[] = array("javascript:newWindow('/home/eLearning/elc/readingrm.php',8);", $i_SpecialRoom_ReadingRoom);
}
if ($access2elprm) {
  $SpecialRmArr[] = array("javascript:newWindow('/home/eLearning/elc/index_elp.php',8)", $i_SpecialRoom_ELP);
}
if ($access2ssrm) {
  $SpecialRmArr[] = array("javascript:newWindow('/home/eLearning/elc/index_ssr.php',8)", $i_eClass_ssr);
}

# for scrabble
if($_SESSION['UserType']==1)
  $user_type = 'Teacher';
else if($_SESSION['UserType']==2)
  $user_type = 'Student';

# Eric Yip (20090821): Check scrabble image size
if($plugin['ScrabbleFC'] && function_exists("getimagesize"))
{
  $scrabble_image_info = getimagesize($intranet_root.$image_path."/sfc/elcad_sfc_".$intranet_session_language."_bg.jpg");
  $scrabble_image_height = $scrabble_image_info[1];
  $scrabble_cell_height = ceil($scrabble_image_height/10)*10;
  $scrabble_cell_height_str = "height=\"".$scrabble_cell_height."\"";
}
?>
<script language="JavaScript" type="text/JavaScript">
function toScrabble()
{
  document.form1.method="post"
  document.form1.action="http://sfc.broadlearner.com/main.php?schoolName=<?=$BroadlearningClientName?>&schoolType=<?=$config_school_type?>"
  document.form1.target="_blank"
  document.form1.submit();
}

</script>
<form name="form1">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">

<!-- ################################################################################# -->
<!-- hardcoding for demo of Scrabble Fun Corner -->
<?php if($plugin['ScrabbleFC']){ ?>
  <tr>
    <td align="center">
      <table width="96%" border="0" cellspacing="0" cellpadding="8">
        <tr>
          <td>
            <table width="50%" border="0" cellspacing="0" cellpadding="8" align="center">
              <tr>
                <td align='center' width='50%' <?=$scrabble_cell_height_str?> valign='top'>
                  <!--<a href='http://sfc.broadlearner.com/main.php?schoolName=<?=$BroadlearningClientName?>&schoolType=<?=$config_school_type?>' target='_blank'>-->
                  <a href="javascript:toScrabble();"><img src='<?=$image_path?>/sfc/elcad_sfc_<?=$intranet_session_language?>_bg.jpg' border='0' />
                  </a>
                  <div style='height:1px; overflow:visible; margin-right:-350px; margin-top:-80px;'>
                    <div style='display:block; width:240px; height:50px; text-align:justify;'>
                      <?=$word_sfc['description']?>
                    </div>
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
<?php } ?>


<!-- /hardcoding for demo of Scrabble Fun Corner -->
<!-- ################################################################################# -->

  <tr>
    <td align="center">
      <table width="96%" border="0" cellspacing="0" cellpadding="8">
        <tr>
          <td>
            <?=$libelc->displayUserEClass($SpecialRmArr)?>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br />
<input type="hidden" id="user_type" name="user_type" value="<?=$user_type?>">
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
