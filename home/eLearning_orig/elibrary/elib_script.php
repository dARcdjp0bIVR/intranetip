﻿<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");

// catalogue Menu
function Get_Catalogue_Menu($Lang="chi")
{
global $image_path, $LAYOUT_SKIN, $eLib;
$x = "";
$x .="<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$x .="<tr>";
$x .="<td valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$x .="<tr>";
$x .="<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$x .="<tr>";
$x .="<td width=\"24\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_title_01.gif\" width=\"24\" height=\"24\"></td>";
$x .="<td background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_title_02.gif\"><span class=\"eLibrary_title_heading\">".$eLib["html"]["catalogue"]."</span></td>";
$x .="</tr>";
$x .="</table></td>";
$x .="</tr>";
$x .="<tr>";
$x .="<td height=\"47\"><table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
$x .="<tr>";
$x .="<td align=\"right\"><a href=\"elib_catalogue_list.php\" class=\"eLibrary_list_all\">".$eLib["html"]["list_all"]."</a></td>";
$x .="</tr>";
$x .="</table>";

$x .="<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" name=\"CatTabList\" id=\"CatTabList\">";
$x .="<tr>";
if($Lang == "chi")
{
$x .="<td width=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" alt=\"\" width=\"5\" height=\"22\"></td>";
$x .="<td width=\"4\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_tab_off_01.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
$x .="<td align=\"center\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_tab_off_02.gif\"><span id=\"engTag\" name=\"engTag\" class=\"eLibrary_cat_tab_off\" onClick=\"changeTagLanguage('eng');\" onMouseOver=\"this.style.cursor='hand'\">".$eLib["html"]["english"]."</span></td>";
$x .="<td width=\"4\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_tab_off_03.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
$x .="<td width=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" alt=\"\" width=\"5\" height=\"22\"></td>";
$x .="<td width=\"4\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_tab_on_01.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
$x .="<td align=\"center\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_tab_on_02.gif\"><span id=\"chiTag\" name=\"chiTag\" class=\"eLibrary_title_heading\" onClick=\"changeTagLanguage('chi');\" onMouseOver=\"this.style.cursor='hand'\">".$eLib["html"]["chinese"]."</span></td>";
$x .="<td width=\"4\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_tab_on_03.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
$x .="<td width=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" alt=\"\" width=\"5\" height=\"22\"></td>";
}
else if($Lang == "eng")
{
$x .="<td width=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" alt=\"\" width=\"5\" height=\"22\"></td>";
$x .="<td width=\"4\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_tab_on_01.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
$x .="<td align=\"center\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_tab_on_02.gif\"><span id=\"engTag\" name=\"engTag\" class=\"eLibrary_title_heading\" onClick=\"changeTagLanguage('eng');\" onMouseOver=\"this.style.cursor='hand'\">".$eLib["html"]["english"]."</span></td>";
$x .="<td width=\"4\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_tab_on_03.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
$x .="<td width=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" alt=\"\" width=\"5\" height=\"22\"></td>";
$x .="<td width=\"4\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_tab_off_01.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
$x .="<td align=\"center\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_tab_ff_02.gif\"><span id=\"chiTag\" name=\"chiTag\" class=\"eLibrary_cat_tab_off\" onClick=\"changeTagLanguage('chi');\" onMouseOver=\"this.style.cursor='hand'\">".$eLib["html"]["chinese"]."</span></td>";
$x .="<td width=\"4\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_tab_off_03.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
$x .="<td width=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" alt=\"\" width=\"5\" height=\"22\"></td>";	
}
$x .="</tr>";
$x .="<tr>";
if($Lang == "chi")
{
$x .="<td bgcolor=\"#b1bfdb\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td bgcolor=\"#b1bfdb\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td bgcolor=\"#b1bfdb\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td bgcolor=\"#b1bfdb\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td bgcolor=\"#b1bfdb\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td height=\"1\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td height=\"1\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td height=\"1\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td height=\"1\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
}
else if($Lang == "eng")
{
$x .="<td height=\"1\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td height=\"1\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td height=\"1\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td height=\"1\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";	
$x .="<td bgcolor=\"#b1bfdb\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td bgcolor=\"#b1bfdb\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td bgcolor=\"#b1bfdb\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td bgcolor=\"#b1bfdb\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
$x .="<td bgcolor=\"#b1bfdb\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"1\"></td>";
}
$x .="</tr>";
$x .="</table>";

$x .="<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" name=\"CatList\" id=\"CatList\">";
$x .="</table>";
$x .="</td>";
$x .="</tr>";
$x .="</table>";
$x .="</td>";
$x .="<td width=\"12\" height=\"300\" valign=\"top\" style=\"background-image:url(".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_st_bg.gif); background-repeat:repeat-y; background-position:bottom\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/catalogue_st_top.gif\" width=\"12\" height=\"128\"></td>";
$x .="</tr>";
$x .="</table>";

return $x;
} // end function get catalogue Menu

if($intranet_session_language == "en"){
	$eLib_Catalogue = Get_Catalogue_Menu("eng");
}else if($intranet_session_language == "b5"){
	$eLib_Catalogue = Get_Catalogue_Menu("chi");
}

///////////////////////////////////////////////////////////////////////////
// preset catalogue list ////
$catListArr["eng"][0]["name"] = "Readers";
$catListArr["eng"][0]["subname"][0] = "Adventure";
$catListArr["eng"][0]["subname"][1] = "Comedy";
$catListArr["eng"][0]["subname"][2] = "Future Thriller";
$catListArr["eng"][0]["subname"][3] = "Human Interest";
$catListArr["eng"][0]["subname"][4] = "Horror";
$catListArr["eng"][0]["subname"][5] = "Ghost Story";
$catListArr["eng"][0]["subname"][6] = "Murder Mystery";
$catListArr["eng"][0]["subname"][7] = "Romance";
$catListArr["eng"][0]["subname"][8] = "Science Fiction";
$catListArr["eng"][0]["subname"][9] = "Short Stories";
$catListArr["eng"][0]["subname"][10] = "Thriller";

$catListArr["chi"][0]["name"] = "文學";
$catListArr["chi"][0]["subname"][0] = "中國現代文學名著";
$catListArr["chi"][0]["subname"][1] = "中國古典詩詞";
$catListArr["chi"][0]["subname"][2] = "中華歷史文庫";

$catListArr["chi"][1]["name"] = "傳記";
$catListArr["chi"][1]["subname"][0] = "中外名人傳記";

$catListArr["chi"][2]["name"] = "生活";
$catListArr["chi"][2]["subname"][0] = "科普藝術教育";
$catListArr["chi"][2]["subname"][1] = "家庭生活百科";


$catListArr["eng"][0]["total_count"] = 0;
$catListArr["eng"][0]["count"][0] = 0;
$catListArr["eng"][0]["count"][1] = 0;
$catListArr["eng"][0]["count"][2] = 0;
$catListArr["eng"][0]["count"][3] = 0;
$catListArr["eng"][0]["count"][4] = 0;
$catListArr["eng"][0]["count"][5] = 0;
$catListArr["eng"][0]["count"][6] = 0;
$catListArr["eng"][0]["count"][7] = 0;
$catListArr["eng"][0]["count"][8] = 0;
$catListArr["eng"][0]["count"][9] = 0;
$catListArr["eng"][0]["count"][10] = 0;

$catListArr["chi"][0]["total_count"] = 0;
$catListArr["chi"][0]["count"][0] = 0;
$catListArr["chi"][0]["count"][1] = 0;
$catListArr["chi"][0]["count"][2] = 0;

$catListArr["chi"][1]["total_count"] = 0;
$catListArr["chi"][1]["count"][0] = 0;

$catListArr["chi"][2]["total_count"] = 0;
$catListArr["chi"][2]["count"][0] = 0;
$catListArr["chi"][2]["count"][1] = 0;

?>