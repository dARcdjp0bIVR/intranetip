/**
* access to SharedFontsManager instance:
* <pre>
* var sfm_instance:SharedFontsManager = SharedFontsManager(_level0.font_mng);
* 
* </pre>
* <B>Methods:</B>
* <pre>
* static init(Void):SharedFontsManager;
* registerFont(source:TextField):Boolean;
* isRegistered(text_format:TextFormat):Boolean;
* getFontMap(source:TextField):Array
* addListener(listener:Object):Boolean;
* removeListener(listener:Object):Boolean;
* 
* </pre>
* <B>Events:</B>
* <pre>
* onRegister(registered_fmt:TextFormat);
* onRegisterError(error_fmt:TextFormat);
* </pre>
*/
class fonts.SharedFontsManager {
	
	// ---------------------------------------- STATIC  ---------------------------------------- //
	/**
	 * @param Void;
	 * @return SharedFontsManager instance;
	 */
	public static function init (Void):SharedFontsManager {
		if (!SharedFontsManager(_level0["font_mng"])) {
			new SharedFontsManager();
		}
		return SharedFontsManager(_level0["font_mng"]);
	}

	// ---------------------------------------- INSTANCE  ---------------------------------------- //
	
	// variables
	private var setFlags:Function;
	private var _listeners:Array;
	private var holder:Object;
	
	// constructor
	private function SharedFontsManager(Void) {
		_level0["font_mng"] = this;
		AsBroadcaster.initialize(this);
		this.holder = {};
		this.setFlags = _global["ASSetPropFlags"];
		
		this.lock(SharedFontsManager, "instance");
		this.lock(_level0, "font_mng");
		this.lock(this,  "_listeners", "holder", "setFlags");
	}
	
	// public methods
	
	/**
	 * @param source:TextField contained embeded font.
	 * @return Boolean
	 */
	public function registerFont (source:TextField):Boolean {
		var success:Boolean = source.embedFonts;
		var source_fmt:TextFormat = source.getNewTextFormat();
		var empty_fmt:TextFormat = new TextFormat(source_fmt.font, null, null, source_fmt.bold, source_fmt.italic);
		var font:String = source_fmt.font;
		var bold:Boolean = Boolean(source_fmt.bold);
		var italic:Boolean = Boolean(source_fmt.italic);
		var name:String = font;
		if (success) {
			// save info about registration for isRegistered and getRegisteredFonts methods
			if (bold) name+=chr(1);
			if (italic) name+=chr(2);
			this.holder[name] = empty_fmt;
			//
			this.broadcastMessage("onRegister", new TextFormat(source_fmt.font, null, null, source_fmt.bold, source_fmt.italic));
			return true;
		} 
		this.broadcastMessage("onRegisterError", new TextFormat(source_fmt.font, null, null, source_fmt.bold, source_fmt.italic));
		return false;
	}

	/**
	 * @param font:String - name of checked font;
	 * @param bold:Boolean - checked font is bold;
	 * @param italic:Boolean - checked font is italic;
	 * @return Boolean;
	 */
	public function isRegistered (text_format:TextFormat):Boolean {
		var font:String = text_format.font;
		if (text_format.bold) font+=chr(1);
		if (text_format.italic) font+=chr(2);
		return Boolean(this.holder[font]);
	}
	
	/**
	 * @param Void
	 * @return Array - an array of TextFormat objects;
	 */
	public function getRegisteredFonts (Void):Array {
		var registered_fonts:Array = [];
		var holder_fmt:TextFormat;
		for (var i : String in this.holder) {
			holder_fmt = TextFormat(this.holder[i]);
			if (holder_fmt) {
				registered_fonts.push(new TextFormat(holder_fmt.font, null, null, holder_fmt.bold,holder_fmt.italic));
			} 
		}
		return registered_fonts;
	}
	
	/**
	 * @param listener - An object that listens for a callback notification from the instance of the SharedFontsManager events
	 */
	public function addListener (listener:Object):Boolean {
		return false;
	}
	
	/**
	 * @param listener - An object added to an object's callback list with addListener().
	 */
	public function removeListener (listener:Object):Boolean {
		return false;
	}
	
	/**
	 * detection fonts used in source textfield;
	* @param source:TextField;
	* @return Array - an array contained TextFormat objects; this textFormat objects contain font, bold, italic properties only.
	* all another properties have default values.
	*/	
	public function getFontMap(source:TextField):Array {
		var state:Boolean = source.html;
		source.html=true;
		var source_xhtml:XML = new XML(source.htmlText);
		source.html = state;
		
		var font_map:Array = new Array(); 
		var unicalizer:Object =new Object();
		var current_node:XMLNode;
		var parent_node:XMLNode;
		var font:String;

		var current_fmt:TextFormat;
		
		var text_nodes:Array = this.getTextNodes(source_xhtml);
		var length:Number = text_nodes.length;
		
		while (length--) {
			current_node=text_nodes[length];
			parent_node=current_node.parentNode;
			font = this.getFace(parent_node);
			var bold:Boolean=false;
			var italic:Boolean= false;			
			if (parent_node.nodeName == "I") {
				italic = true;
				if (parent_node.parentNode.nodeName == "B") {
					bold = true;
				}
			} else if (parent_node.nodeName == "B") {
				bold = true;
			}
			if (bold) font+=chr(1);
			if (italic) font+=chr(2);
			current_fmt = 	new TextFormat(font, null, null, bold, italic);
			unicalizer[font] = current_fmt;
		}
		
		for (var i:String in unicalizer) {
			var unique_fmt:TextFormat = TextFormat(unicalizer[i]); 
			font_map.push(unique_fmt);
		}
		return font_map;
	}
	
	// events (for help files generation only)
	/**
	 * @param registered_fmt:TextFormat;
	 * @return Void;
	 */
	public function onRegister (registered_fmt:TextFormat):Void {
	}
	/**
	 * @param error_fmt:TextFormat;
	 * @return Void;
	 */
	public function onRegisterError (error_fmt:TextFormat):Void {
	}
	
	// private methods
	
	// nextNode
	private function nextNode(current_node:XMLNode):XMLNode {
		if (current_node.firstChild != null) {
			return current_node.firstChild;
		}
		var n:XMLNode = current_node;
		while (n.nextSibling == null) {
			if (n.parentNode) {
				n = n.parentNode;
			} else {
				return null;
			}
		}
		return n.nextSibling;
	}

	// getTextNodes
	private function getTextNodes(current_node:XMLNode):Array {
		var out_array:Array = new Array();
		while (current_node) {
			if (current_node.nodeType == 3) {
				out_array.push(current_node);
			}
			current_node = nextNode(current_node);
		}
		return out_array;
	}

	// getFace
	private function getFace(current_node:XMLNode):String {
		var face:String = current_node.attributes["FACE"];
		if (face != undefined) {
			return face;
		}
		return arguments.callee(current_node.parentNode);
	}
	
	// broadcastMessage
	private function broadcastMessage (message:String):Void {
	}
	
	// lock
	private function lock (obj:Object):Void {
		arguments.shift();
		setFlags(obj, arguments, parseInt("111", 2));
	}
	
	// hide
	private function hide (obj:Object):Void {
		arguments.shift();
		setFlags(obj, arguments, parseInt("011", 2), parseInt("100", 2));
	}
	
	// unLock
	private function unLock (obj:Object):Void {
		arguments.shift();
		setFlags(obj, arguments, parseInt("000", 2), parseInt("111", 2));
	}
	
	// getNextNode
	private function getNextNode(current_node:XMLNode):XMLNode {
		if (current_node.firstChild == undefined) {
			while (current_node.nextSibling == undefined) {
				if (current_node.parentNode) {
					current_node = current_node.parentNode;
				} else {
					return undefined;
				}
			}
			return current_node.nextSibling;
		} else {
			return current_node.firstChild;
		}
	}
	
	
}


















