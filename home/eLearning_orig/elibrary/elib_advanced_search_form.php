<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

// additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();

$li = new libcalevent();
$ts = time();
if ($startdate == "")
{
    $tstart = $li->getStartOfAcademicYear($ts);
    $startdate = date('Y-m-d',$tstart);
}
else
{
    $tstart = strtotime($startdate);
}
if ($enddate == "")
{
    $tend = $li->getEndOfAcademicYear($ts);
    $enddate = date('Y-m-d',$tend);
}
else
{
    $tend = strtotime($enddate);
}

$linterface 	= new interface_html("default3.html");
$CurrentPage	= "PageMyeClass";

$lo 	= new libeclass();

$lelib = new elibrary();
### Title ###
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;
$title = $lelib->printSearchInput($ParArr, $eLib);
	
$TAGS_OBJ[] = array($title,"");

$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'];
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

$settingArr = $lelib->GET_USER_BOOK_SETTING();

$NumReviewer = $settingArr[0]["DisplayReviewer"];
$NumReview = $settingArr[0]["DisplayReview"];
$NumRecommend = $settingArr[0]["DisplayRecommendBook"];
$NumWeeklyHit = $settingArr[0]["DisplayWeeklyHitBook"];
$NumHit = $settingArr[0]["DisplayHitBook"];

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();
?>

<script language="JavaScript" type="text/JavaScript">
<!--
function checkValidField(obj)
{
	return true;	
} // end function check valid field
//-->
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="320" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="175" valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="20">

<!-- Setting logo -->
<!--
<table width="95%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_01.gif" width="4" height="4"></td>
<td height="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_03.gif" width="4" height="4"></td>
</tr>
<tr>
<td width="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
<td bgcolor="#d7e9f4"><a href="#" class="menuon" onClick="MM_openBrWindow('elib_setting.php','','scrollbars=yes')"> <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_manage.gif" width="20" height="20" border="0" align="absmiddle"><?=$eLib["html"]["settings"]?></a></td>
<td width="4" bgcolor="#d7e9f4">&nbsp;</td>
</tr>
<tr>
<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_07.gif" width="4" height="4"></td>
<td height="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
<td width="4" height="4" ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_09.gif" width="4" height="4"></td>
</tr>
</table>
-->
<!-- End Setting logo -->
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
<!-- Record List Menu -->
<?=$lelib->displayRecordListMenu($ParArr,$eLib);?>
<!-- End Record List Menu -->
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
<!-- catalogue Menu -->
<?=$eLib_Catalogue?>
<!-- end catalogue Menu -->
<br></td>
<td align="center" valign="top">
<!-- Start of Content -->
<form name="form1" id="form1" action="elib_advanced_search_result.php" method="post" onSubmit="return checkValidField(this);">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td class="eLibrary_navigation"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="index.php"><?=$eLib["html"]["home"]?></a> <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle"><?=$eLib["html"]["advance_search"]?></td>
</tr>
</table>
										
<table width="88%" border="0" cellpadding="5" cellspacing="0">
<tr>
<td>
<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
<tr>
<td align="left" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eLib["html"]["title"]?></span></td>
<td width="80%" align="left" valign="top">
<input name="BookTitle" type="text" class="textboxtext" />
</td>
</tr>
<tr>
<td align="left" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eLib["html"]["author"]?></span></td>
<td align="left" valign="top">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td>
<input name="Author" id="Author" type="text" class="textboxtext" />
</td>
<td width="25" align="center">
<?php echo $lelib->displayPresetListAuthorInput("Author"); ?>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="left" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eLib["html"]["source"]?></span></td>
<td align="left"><table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input name="Source" id="Source" type="text" class="textboxtext" /></td>
<td width="25" align="center">
<?php echo $lelib->displayPresetListSourceInput("Source"); ?>
</td>
</tr>
</table></td>
</tr>
<tr valign="top">
<td align="left" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eLib["html"]["category"]?></span></td>
<td align="left">

<?php echo $lelib->displayCategorySelect("selCategory", $eLib); ?>

<br>
</td>
</tr>
<tr>
<td align="left" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eLib["html"]["level"]?></span></td>
<td align="left">

<?php echo $lelib->displayLevelSelect("selLevel", $eLib); ?>

</td>
</tr>
<tr>
<td align="left" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eLib["html"]["book_input_date"]?></span> </td>
<td align="left">
<?php echo $li->displayDateRangeInput($tstart,$tend); ?>
</td>
</tr>
<!--
<tr valign="top">
<td align="left" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eLib["html"]["with_worksheets"]?></span></td>
<td align="left"><label>
<input type="checkbox" name="checkWorksheets" id="checkWorksheets">
</label></td>
</tr>
-->
<tr valign="top">
<td align="left" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eLib["html"]["sort_by"]?></span></td>
<td align="left">
<select name="sortField">
<option value="Title"><?=$eLib["html"]["book_title"]?></option>
<option value="Author"><?=$eLib["html"]["author"]?></option>
<option value="Source"><?=$eLib["html"]["source"]?></option>
</select>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
</tr>
<tr>
<td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="tabletextremark">&nbsp;</td>
<td align="center">
<input name="submit" type="submit" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$eLib["html"]["search"]?>">
&nbsp;
<input name="reset" type="reset" class="formbutton" value="<?=$eLib["html"]["reset"]?>"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'">
</td>
</tr>
</table>
</td>
</tr>
</table>
</form>
<!-- End of Content -->
</td></tr></table>
</td></tr></table>
</td></tr></table>

<script language="JavaScript" type="text/JavaScript">
showChineseBookList();if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
