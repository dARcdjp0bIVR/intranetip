<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

// additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();

$lelib = new elibrary();

if($lelib->IS_ADMIN_USER($_SESSION["UserID"]))
{
	// continue
}
else
{
	header("Location: book_detail.php?BookID=$BookID");	
}

$linterface 	= new interface_html("popup.html");
$CurrentPage	= "PageMyeClass";

### Title ###
$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$ip20TopMenu['eLibrary'];
$TAGS_OBJ[] = array($title,"");

$lelib = new elibrary();
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'];
$MODULE_OBJ["logo"] = "";
$customLeftMenu = ' ';
//$CurrentPageArr['eLib'] = 1;

$ParArr["BookID"] = $BookID;
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$RecArr = $lelib->getBookRecommend($ParArr);

if(count($RecArr) <= 0)
$Mode = "add";
else
{
	$Mode = "edit";
	$DefaultDescription = $RecArr[0]["Description"];
	$DefaultGroup = $RecArr[0]["RecommendGroup"];
	
	$GroupArr = explode("|", $DefaultGroup);
}

$returnArr = $lelib->getBookInformation($ParArr);

if($returnArr != "")
{
	/*
	$Title = iconv("utf-8","big-5",$returnArr[0]["Title"]);
	$Author = iconv("utf-8","big-5",$returnArr[0]["Author"]);
	$Source = iconv("utf-8","big-5",$returnArr[0]["Source"]);
	$Category = iconv("utf-8","big-5",$returnArr[0]["Category"]);
	$SubCategory = iconv("utf-8","big-5",$returnArr[0]["SubCategory"]);
	$Publisher = iconv("utf-8","big-5",$returnArr[0]["Publisher"]);
	$Level = $returnArr [0]["Level"];
	$DateModified = $returnArr[0]["DateModified"];
	$Description = iconv("utf-8","big-5",$returnArr[0]["Preface"]);
	*/
	$Title = $returnArr[0]["Title"];
	$Author = $returnArr[0]["Author"];
	$Source = $returnArr[0]["Source"];
	$Category = $returnArr[0]["Category"];
	$SubCategory = $returnArr[0]["SubCategory"];
	$Publisher = $returnArr[0]["Publisher"];
	$Level = $returnArr[0]["Level"];
	$DateModified = $returnArr[0]["DateModified"];
	$Description = $returnArr[0]["Preface"];

	if($Description == "")
	$Description = "--";
	
	if($Level == "")
	$Level = "--";
	
	$Source = $lelib->getSourceFullName($Source);
	
	$reviewArr = $lelib->getReview($ParArr);
	
	$reviewNum = count($reviewArr);
	
	
	$sumRating = 0;
	for($c = 0; $c < $reviewNum ; $c++)
	{
		$sumRating += $reviewArr[$c]["Rating"];
	}
	
	if($reviewNum > 0)
	$rating = $sumRating / $reviewNum;
	else
	$rating = 0;

//// draw star ////////////////////////////////////////////////////////////////////////////////////////

$x = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
$x .= "<td class=\"tabletextremark\"><a href=\"book_detail.php?BookID=$BookID\" class=\"contenttool\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_comment.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">".$eLib["html"]["rating"]."</a></td>";
$x .= "<td>";
for ($star = 1; $star <= 5; $star++)
{
	if($star <= $rating)
	{
	$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_on.gif\" width=\"15\" height=\"20\">";
	}
	else
	{
		if($star <= $rating + 0.5)
		$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_half.gif\" width=\"15\" height=\"20\">";
		else
		$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_off.gif\" width=\"15\" height=\"20\">";
	}
}
$x .= "</td></tr></table>";
$starImg = $x;

////////////////////////////////////////////////////////////////////////////////////////////////////////
} //end if get book detail

////////////////////////////////////////////////////////////
	$sql = "SELECT 
		LevelName, ClassLevelID
        FROM INTRANET_CLASSLEVEL
        WHERE RecordStatus = 1
        ";

    $classLevelArr = $lelib->returnArray($sql,2);

    $checkFormList = "&nbsp;&nbsp;&nbsp;&nbsp;";

	for($i = 0; $i < count($classLevelArr); $i++)
	{
		if($i == 7)
		$checkFormList .= "<br />&nbsp;&nbsp;&nbsp;&nbsp;";
		
		// check the class group is selected
		if($Mode == "edit")
		{
			$checked = "";
			for($k = 0; $k < count($GroupArr); $k++)
			{
				if(($classLevelArr[$i]["ClassLevelID"] == $GroupArr[$k] && $GroupArr[$k] != "") || $GroupArr[$k] == -1)
				{
					$checked = "checked";
					break;
				}
			}
		}
		else
			$checked = "checked";
			
		$ClassCheckBox = "<input type=checkbox name=ClassLevelID[] value=".$classLevelArr[$i]["ClassLevelID"]." ".$checked.">";
		
		$checkFormList .= $ClassCheckBox.$classLevelArr[$i]["LevelName"]."&nbsp;&nbsp;&nbsp;&nbsp;";
	}

$linterface->LAYOUT_START();

?>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function submitForm()
{
	document.form1.submit();
} // end confirm form

//-->
</script>

<form name="form1" action="elib_recommend_update.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5"><tr><td align="center">
<table width="96%" border="0" cellspacing="0" cellpadding="8"><tr><td>	
<table width="100%" border="0" cellspacing="0" cellpadding="5"><tr><td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="left">&nbsp;</td></tr>
<tr><td>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
<tr>
	<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td>
</tr>
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr>
<td align="left" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr>
<td width="125" align="center" valign="top">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><div id="book_border"><a href="#" onClick="MM_openBrWindowFull('tool/index.php?BookID=<?=$BookID?>','booktool','scrollbars=yes')" ><span><img src="/file/elibrary/content/<?=$BookID?>/image/cover.jpg" width="120" height="183" border="0"><br><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/icon_openbook2.gif" width="21" height="20" border="0" align="absmiddle">&nbsp;<?=$eLib["html"]["click_to_read"]?>&nbsp;</span></a></div></td>
</tr>
</table>
</td>
<td align="left" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="60%">
<table width="100%" border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="80" align="left" class="tabletext"><?=$eLib["html"]["title"]?></td>
<td width="10" align="left" class="tabletext">�G</td>
<td align="left" nowrap class="tabletext"><span class="eLibrary_commonlink"><strong><?=$Title?></strong></span></td>
</tr>
<tr>
<td width="50" align="left" class="tabletext"><?=$eLib["html"]["author"]?></td>
<td align="left" class="tabletext">�G</td>
<td align="left" nowrap class="tabletext"><span class="eLibrary_commonlink"><?=$Author?></span></td>
</tr>
<tr>
<td width="50" align="left" class="tabletext"><?=$eLib["html"]["category"]?></td>
<td align="left" class="tabletext">�G</td>
<td align="left" nowrap class="tabletext"><span class="eLibrary_commonlink">
<?=$Category?> -> <?=$SubCategory?>
</span></td>
</tr>
<tr>
<td align="left" class="tabletext"><?=$eLib["html"]["level"]?></td>
<td align="left" class="tabletext">�G</td>
<td align="left" nowrap class="tabletext"><span class="eLibrary_commonlink"><?=$Level?></span></td>
</tr>
<tr>
<td width="50" align="left" class="tabletext"><?=$eLib["html"]["publisher"]?></td>
<td align="left" class="tabletext">�G</td>
<td align="left" nowrap class="tabletext"><span class="eLibrary_commonlink"><?=$Publisher?></span></td>
</tr>
<tr>
<td width="50" align="left" class="tabletext"><?=$eLib["html"]["source"]?></td>
<td align="left" class="tabletext">�G</td>
<td align="left" nowrap class="tabletext"><span class="eLibrary_commonlink"><?=$Source?></span></td>
</tr>
<tr>
<td width="50" align="left" class="tabletext"><?=$eLib["html"]["date"]?></td>
<td align="left" class="tabletext">�G</td>
<td align="left" nowrap class="tabletext"><?=$DateModified?></td>
</tr>
<tr>
<td width="80" align="left" valign="top" class="tabletext"><?=$eLib["html"]["description"]?></td>
<td width="10" align="left" valign="top" class="tabletext">�G</td>
<td align="left" class="tabletext"></td>
</tr>
<tr>
<td align="left" class="tabletext" colspan="3"><?=$Description?></td>
</tr>
</table>
</td></tr></table>
</td></tr></table>
</td></tr></table>

<table border="0" cellspacing="0" cellpadding="2"><tr>
<td><?=$starImg?></td>
<td><a href="book_detail.php?BookID=<?=$BookID?>" class="contenttool">(<?=$reviewNum?> <?=$eLib["html"]["reviewsUnit"]?><?=$eLib["html"]["reviews"]?>)</a></td>
</tr>
</table>

</td></tr>
<tr><td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5">

<!-------------- Content Form ------------------>
<tr>
<td height="30" class="tablebottom"><span class="eLibrary_title_heading"><?=$eLib["html"]["recommend_this_book"]?></span></td>
</tr>

<tr><td align="left">&nbsp;</td></tr>

<tr>
<td align="left">
<table width="100%" border="0" cellspacing="0" cellpadding="2"><tr>
<td width="80%" align="left" valign="top">
<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0"><tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eLib["html"]["recommend_to"]?></span></td>
<td width="80%" valign="top">
<label>
<input type="checkbox" name="checkboxW" onClick=(this.checked)?setChecked(1,this.form,'ClassLevelID[]'):setChecked(0,this.form,'ClassLevelID[]')>
<span class="tabletext"><?=$eLib["html"]["whole_school"]?></span><br>
<span class="tabletext">
<?=$checkFormList?>
</span>
</label></td>
</tr>

<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext"><?=$eLib["html"]["reason_to_recommend"]?></span></td>
<td valign="top">
<?=$linterface->GET_TEXTAREA("Description", $DefaultDescription, 100, 5)?>
</td>
</tr>
</table></td></tr>
</table></td></tr>

<tr><td align="center">
<input name="Submit" type="button" class="formbutton" onClick="submitForm();"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$eLib["html"]["submit"]?>">
&nbsp;
<input name="Reset" type="reset" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$eLib["html"]["reset"]?>">
&nbsp;
<input name="Cancel" type="button" class="formbutton" onClick="window.location='book_detail.php?BookID=<?=$BookID?>'"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$eLib["html"]["cancel"]?>">
</td></tr>
<!-------------- End Content Form -------------->

<tr><td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1">
</td></tr>
</table>
</td></tr></table>
</td></tr></table>
</td></tr></table>		
</td></tr></table>
</td></tr></table>

<input type="hidden" name="BookID" value="<?=$BookID?>" >
<input type="hidden" name="Mode" value="<?=$Mode?>" >
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>