<?php

//using by: 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

// additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html("default3.html");

$CurrentPage	= "PageMyeClass";

$lo 	= new libeclass();

$lelib = new elibrary();
### Title ###
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;
$title = $lelib->printSearchInput($ParArr, $eLib);
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'];
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

$DisplayMode = 2; // list mode (no image cover)

if($CurrentPageNum == "" || $CurrentPageNum == null)
$CurrentPageNum = 1;

if($DisplayNumPage == "" || $DisplayNumPage == null)
$DisplayNumPage = 10;

if($sortFieldOrder == "")
$sortFieldOrder = "ASC";

$inputArr["DisplayMode"] = $DisplayMode;
$inputArr["CurrentPageNum"] = $CurrentPageNum;
$inputArr["DisplayNumPage"] = $DisplayNumPage;

$inputArr["image_path"] = $image_path;
$inputArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$lang = $_SESSION["intranet_session_language"];

$tmpArr[0]["value"] = "UserName";
$tmpArr[1]["value"] = "ClassName";
$tmpArr[2]["value"] = "ClassNumber";
$tmpArr[3]["value"] = "NumBookRead";
$tmpArr[4]["value"] = "NumBookReview";

$tmpArr[0]["text"] = $eLib["html"]["student_name"];
$tmpArr[1]["text"] = $eLib["html"]["class"];
$tmpArr[2]["text"] = $eLib["html"]["class_number"];
$tmpArr[3]["text"] = $eLib["html"]["num_of_book_read"];
$tmpArr[4]["text"] = $eLib["html"]["num_of_review"];

$tmpArr[0]["width"] = "30%";
$tmpArr[1]["width"] = "15%";
$tmpArr[2]["width"] = "15%";
$tmpArr[3]["width"] = "20%";
$tmpArr[4]["width"] = "20%";

if($sortField == "")
$sortField = $tmpArr[0]["value"];

$inputArr["sortField"] = $sortField;
$inputArr["sortFieldOrder"] = $sortFieldOrder;

$inputArr["sortFieldArray"] = $tmpArr;

$inputArr["selectNumField"] = 5;
$UserID = $_SESSION["UserID"];

$con = $sortField." ".$sortFieldOrder;

$dsql1 = "drop table IF EXISTS INTRANET_ELIB_TEMP_NUM_REVIEW_{$UserID}";
$lelib->db_db_query($dsql1);

$dsql2 = "drop table IF EXISTS INTRANET_ELIB_TEMP_NUM_READ_{$UserID}";
$lelib->db_db_query($dsql2);

$tsql1 = "
		CREATE TABLE INTRANET_ELIB_TEMP_NUM_REVIEW_{$UserID}
		SELECT d.UserID, Count(d.ReviewID) as NumBookReview 
		FROM INTRANET_ELIB_BOOK a, INTRANET_USER c, INTRANET_ELIB_BOOK_REVIEW d 
		WHERE a.BookID = d.BookID AND c.UserID = d.UserID AND a.Publish = 1
		GROUP BY d.UserID
		";

$tsql2 = "
		CREATE TABLE INTRANET_ELIB_TEMP_NUM_READ_{$UserID}
		SELECT b.UserID, Count(DISTINCT b.BookID) as NumBookRead
		FROM INTRANET_ELIB_BOOK a, INTRANET_ELIB_BOOK_HISTORY b, INTRANET_USER c
		WHERE a.BookID = b.BookID AND b.UserID = c.UserID AND a.Publish = 1
		GROUP BY b.UserID
		";	
		
$lelib->db_db_query($tsql1);
$lelib->db_db_query($tsql2);

$selName1 = "if((a.ChineseName IS NOT NULL AND a.ChineseName != ''), a.ChineseName, a.EnglishName) as UserName";
$selName2 = "if((a.EnglishName IS NOT NULL AND a.EnglishName != ''), a.EnglishName, a.ChineseName) as UserName";

if($lang == "b5")
$selName = $selName1;
else
$selName = $selName2;

if($ClassName == -1 || $ClassName == "")
$selClass = "";
else
$selClass = "AND a.ClassName='".$ClassName."'";

$sql = "
		SELECT
		a.UserID,
		a.ClassName,
		a.ClassNumber,
		$selName,
		if((b.NumBookRead IS NOT NULL),CONCAT('<a href=\"elib_public_students_summary_read.php?ClassName=',a.ClassName,'&StudentID=',a.UserID,'\" class=\"tablelink\">',b.NumBookRead,'</a>'), 0) as NumBookRead,
		if((c.NumBookReview IS NOT NULL),CONCAT('<a href=\"elib_public_students_summary_review.php?ClassName=',a.ClassName,'&StudentID=',a.UserID,'\" class=\"tablelink\">',c.NumBookReview,'</a>'), 0) as NumBookReview
		FROM
		INTRANET_ELIB_TEMP_NUM_REVIEW_{$UserID} c
		RIGHT JOIN
		INTRANET_USER a
		ON
		a.UserID = c.UserID
		LEFT JOIN
		INTRANET_ELIB_TEMP_NUM_READ_{$UserID} b
		ON
		a.UserID = b.UserID
	 	WHERE 1
	 	$selClass
	 	AND (a.ClassName IS NOT NULL)
	 	AND !(a.ClassName = '')
		ORDER BY
		$con
		";
		
		//debug_r($sql);

$resultArr = $lelib->displayBookDetail($inputArr, $eLib, $sql);
$contentHtml = $resultArr["contentHtml"];
$totalRecord = $resultArr["totalRecord"];
$DisplayNumPage = $resultArr["DisplayNumPage"];

// drop the temp table create before
$lelib->db_db_query($dsql1);
$lelib->db_db_query($dsql2);

if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}

$totalPage = ceil($totalRecord / $DisplayNumPage);

///////////////////// Selection Box ////////////////////////////////////////////////

$returnClassNameArr = $lelib->GET_CLASSNAME();

	$ClassNameArr = array(
				array("-1", $eLib["html"]["all_class"])
				);
			
	//debug_r($returnClassNameArr);
	for($i = 0; $i < count($returnClassNameArr); $i++)
	{
		
		if(trim($returnClassNameArr[$i]["ClassTitleEN"]) != "")
		$tmpClassArr = "";
		$tmpClassArr = array($returnClassNameArr[$i]["ClassTitleEN"], $returnClassNameArr[$i]["ClassTitleEN"]);
		
		$ClassNameArr[$i+1] = $tmpClassArr;
	}
	//debug_r($ClassNameArr);
	

	if($ClassName == "")
	$ClassName = $ClassNameArr[0][0];
	
	$ClassSelection = $linterface->GET_SELECTION_BOX($ClassNameArr, "name='ClassName' id='ClassName' onChange='changeClassName(this);'", "", $ClassName);
	
	$classStr = $eLib["html"]["class"]." : ";
	
	$SelectionTable = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
	$SelectionTable .= "<tr><td><span class=\"tabletext\">".$classStr." ".$ClassSelection."</span></td></tr>";
	$SelectionTable .= "</table>";
//////////////////////////////////////////////////////////////////////////////////

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();
?>


<script language="JavaScript" type="text/JavaScript">
<!--

function changeClassName(obj)
{
	document.form1.CurrentPageNum.value = 1;
	document.form1.submit();
} // end function change class name

function checkValidField(obj)
{
	obj.submit();
} // end function check valid field

//-->
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="320" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="175" valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="20">

<!-- Setting logo -->

<!-- End Setting logo -->

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
<!-- Record List Menu -->									
<?=$lelib->displayRecordListMenu($ParArr,$eLib);?>
<!-- End Record List Menu -->
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
<!-- catalogue Menu -->
<?=$eLib_Catalogue?>
<!-- end catalogue Menu -->
<br></td>
<td align="center" valign="top">
<!-- Start of Content -->
<form name="form1" action="elib_public_students_summary.php" method="post">

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>

<!-- head menu -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td class="eLibrary_navigation">
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="index.php"><?=$eLib["html"]["home"]?></a> 
<!--
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<?=$eLib["html"]["public"]?>
-->
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle"><?=$eLib["html"]["student_summary"]?></td>
</tr>
</table>
<!-- end head menu -->


<table width="98%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif" width="15" height="18"></td>
<td align="left" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif" width="19" height="18"></td>
<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif" width="25" height="18"></td>
</tr>

<tr>
<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif" width="15" height="21"></td>

<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_05.gif">
<!-- show content -->
<?=$SelectionTable; ?>
<?=$contentHtml; ?>
<!-- end show content -->
</td>


<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif" width="25" height="30"></td>
</tr>
													
<tr>
<td width="15" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_07.gif" width="15" height="26"></td>
<td height="26" style="background-image:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_08.gif); background-repeat:repeat-x; background-position:right"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="8" height="20"></td>
<td width="25" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_09.gif" width="25" height="26"></td>
</tr>
</table>

<input type="hidden" name="DisplayMode" value="<?=$DisplayMode?>" >
<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >
<input type="hidden" name="sortField" value="<?=$sortField?>" >
<input type="hidden" name="sortFieldOrder" value="<?=$sortFieldOrder?>" >

</form>
<!-- End of Content -->
</td></tr></table>
</td></tr></table>
</td></tr></table>

<script language="JavaScript" type="text/JavaScript">
if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
