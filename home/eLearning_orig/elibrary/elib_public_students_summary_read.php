<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

// additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html("default3.html");

$CurrentPage	= "PageMyeClass";

$lo 	= new libeclass();

$lelib = new elibrary();
### Title ###
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;
$title = $lelib->printSearchInput($ParArr, $eLib);
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'];
$MODULE_OBJ["logo"] = "";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;
$lang = $_SESSION["intranet_session_language"];

///////////////////// Selection Box ////////////////////////////////////////////////

$returnClassNameArr = $lelib->GET_CLASSNAME();
			
	for($i = 0; $i < count($returnClassNameArr); $i++)
	{
		$tmpClassArr = "";
		$tmpClassArr = array($returnClassNameArr[$i]["ClassName"], $returnClassNameArr[$i]["ClassName"]);
		$ClassNameArr[$i] = $tmpClassArr;
	}

	if($ClassName == "")
	$ClassName = $ClassNameArr[0][0];
	
	$ClassSelection = $linterface->GET_SELECTION_BOX($ClassNameArr, "name='ClassName' id='ClassName' onChange='changeClassName(this);'", "", $ClassName);
	
	$classStr = $eLib["html"]["class"]." : ";

	///////////////////// student name /////////////////////////////////////////////
		
	$ParArr["ClassName"] = $ClassName;
	$returnStudentArr = $lelib->GET_STUDENT_NAME_BY_CLASS($ParArr);
	
	if($lang == "b5")
	{
		$prefix1 = "ChineseName";
		$prefix2 = "EnglishName";
	}
	else
	{
		$prefix1 = "EnglishName";
		$prefix2 = "ChineseName";	
	}
	
	for($i = 0; $i < count($returnStudentArr); $i++)
	{
		
		if($returnStudentArr[$i][$prefix1] == "")
		$sName = $returnStudentArr[$i][$prefix2];
		else
		$sName = $returnStudentArr[$i][$prefix1];
		
		$tmpStudentArr = "";
		$tmpStudentArr = array($returnStudentArr[$i]["UserID"], $sName);
		$StudentNameArr[$i] = $tmpStudentArr;
	}

	if($StudentID == "")
	$StudentID = $StudentNameArr[0]["UserID"];
	
	$studentSelection = $linterface->GET_SELECTION_BOX($StudentNameArr, "name='StudentID' id='StudentID' onChange='changeStudentName(this);'", "", $StudentID);
	
	$studentStr = $eLib["html"]["student"]." : ";
	
	$SelectionTable = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
	$SelectionTable .= "<tr><td>";
	$SelectionTable .= "<span class=\"tabletext\">".$classStr." ".$ClassSelection." ".$studentStr." ".$studentSelection."</span>";
	$SelectionTable .= "</td></tr>";
	$SelectionTable .= "</table>";
//////////////////////////////////////////////////////////////////////////////////

$ParArr["StudentID"] = $StudentID;
$curStudentArr = $lelib->GET_STUDENT_NAME_BY_ID($ParArr);

if($curStudentArr[0][$prefix1] == "")
$curStudentName = $curStudentArr[0][$prefix2];
else
$curStudentName = $curStudentArr[0][$prefix1];

$ClassNumber = $curStudentArr[0]["ClassNumber"];

$studentInfo = $curStudentName." ".$ClassName."-".$ClassNumber;


$DisplayMode = 2; // list mode (no image cover)

if($CurrentPageNum == "" || $CurrentPageNum == null)
$CurrentPageNum = 1;

if($DisplayNumPage == "" || $DisplayNumPage == null)
$DisplayNumPage = 10;

if($sortFieldOrder == "")
$sortFieldOrder = "ASC";

$inputArr["DisplayMode"] = $DisplayMode;
$inputArr["CurrentPageNum"] = $CurrentPageNum;
$inputArr["DisplayNumPage"] = $DisplayNumPage;

$inputArr["sortField"] = $sortField;
$inputArr["sortFieldOrder"] = $sortFieldOrder;

$inputArr["image_path"] = $image_path;
$inputArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$tmpArr[0]["value"] = "Title";
$tmpArr[1]["value"] = "Author";
$tmpArr[2]["value"] = "Date";

$tmpArr[0]["text"] = $eLib["html"]["book_title"];
$tmpArr[1]["text"] = $eLib["html"]["author"];
$tmpArr[2]["text"] = $eLib["html"]["last_read"];

$tmpArr[0]["width"] = "50%";
$tmpArr[1]["width"] = "30%";
$tmpArr[2]["width"] = "20%";

if($sortField == "")
$sortField = $tmpArr[0]["value"];

$inputArr["sortFieldArray"] = $tmpArr;

$inputArr["selectNumField"] = 4;
$UserID = $_SESSION["UserID"];

$con = $sortField;
$con = $con." ".$sortFieldOrder;

$selClass = "AND a.ClassName='".$ClassName."'";
$selStudentID = "AND a.UserID='".$StudentID."'";

$sql = "
		SELECT
		b.BookID,
		c.Title,
		c.Author,
		max(DATE_FORMAT(b.DateModified, '%y/%m/%d')) as Date
		FROM
		INTRANET_USER a, INTRANET_ELIB_BOOK_HISTORY b, INTRANET_ELIB_BOOK c
	 	WHERE 1
	 	AND a.UserID = b.UserID
	 	AND b.BookID = c.BookID
	 	AND c.Publish = 1
	 	$selClass
	 	$selStudentID
	 	GROUP BY
	 	b.UserID
		ORDER BY
		$con
		";
		
		//debug_r($sql);

$resultArr = $lelib->displayBookDetail($inputArr, $eLib, $sql);
$contentHtml = $resultArr["contentHtml"];
$totalRecord = $resultArr["totalRecord"];
$DisplayNumPage = $resultArr["DisplayNumPage"];

if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}

$totalPage = ceil($totalRecord / $DisplayNumPage);

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();

?>


<script language="JavaScript" type="text/JavaScript">
<!--

function changeStudentName(obj)
{
	document.form1.CurrentPageNum.value = 1;
	document.form1.submit();
} // end function change class name

function changeClassName(obj)
{
	document.form1.CurrentPageNum.value = 1;
	document.form1.submit();
} // end function change class name

function checkValidField(obj)
{
	obj.submit();
} // end function check valid field

//-->
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="320" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="175" valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="20">

<!-- Setting logo -->

<!-- End Setting logo -->

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
<!-- Record List Menu -->									
<?=$lelib->displayRecordListMenu($ParArr,$eLib);?>
<!-- End Record List Menu -->
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
<!-- catalogue Menu -->
<?=$eLib_Catalogue?>
<!-- end catalogue Menu -->
<br></td>
<td align="center" valign="top">
<!-- Start of Content -->
<form name="form1" action="elib_public_students_summary_read.php" method="post">

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>

<!-- head menu -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td class="eLibrary_navigation">
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="index.php"><?=$eLib["html"]["home"]?></a> 
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<?=$eLib["html"]["public"]?>
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="elib_public_students_summary.php"><?=$eLib["html"]["student_summary"]?></a>
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle"><?=$eLib["html"]["book_read"]?> (<?=$studentInfo?>)</td>
</tr>
</table>
<!-- end head menu -->


<table width="98%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif" width="15" height="18"></td>
<td align="left" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif" width="19" height="18"></td>
<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif" width="25" height="18"></td>
</tr>

<tr>
<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif" width="15" height="21"></td>

<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_05.gif">
<!-- show content -->
<?=$SelectionTable; ?>
<?=$contentHtml; ?>
<!-- end show content -->
</td>


<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif" width="25" height="30"></td>
</tr>
													
<tr>
<td width="15" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_07.gif" width="15" height="26"></td>
<td height="26" style="background-image:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_08.gif); background-repeat:repeat-x; background-position:right"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="8" height="20"></td>
<td width="25" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_09.gif" width="25" height="26"></td>
</tr>
</table>

<input type="hidden" name="DisplayMode" value="<?=$DisplayMode?>" >
<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >
<input type="hidden" name="sortField" value="<?=$sortField?>" >
<input type="hidden" name="sortFieldOrder" value="<?=$sortFieldOrder?>" >

</form>
<!-- End of Content -->
</td></tr></table>
</td></tr></table>
</td></tr></table>

<script language="JavaScript" type="text/JavaScript">
if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
