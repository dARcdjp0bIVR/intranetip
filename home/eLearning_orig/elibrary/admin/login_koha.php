<?php
// Modifing by key

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libkoha.php");

intranet_auth();
intranet_opendb();

if (!$plugin["koha"])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$usr = new libuser($UserID);
$koha = new libkoha();

$UserLogin = $usr->UserLogin;
$UserPassword = $usr->UserPassword;

$surname = $usr->EnglishName;
$zipcode = '111';
$userid = $usr->UserLogin;
$password = $koha->Gen_Password($usr->UserPassword);
$dateenrolled = '2008-11-20';
$dateexpiry = '2020-11-20';
$cardnumber = $UserID; // will be modified
$branchcode = 'CPL';

if($intranet_elibrary_usertype == "TEACHER")
$categorycode = 'S'; // staff
else
$categorycode = 'ST'; // student

$address = 'NULL';
$city = 'NULL';

//debug_r($_SESSION);
intranet_closedb();

$koha->db_connect();

// check same cardnumber
$sql = "select * from borrowers where cardnumber='$cardnumber'";
$row = $koha->returnArray($sql);

if(count($row) >= 1)
{
	// user exists
	$result = true;
}
else
{
	// insert user account
	$sql = "insert into borrowers 
	set 
	surname='$surname', 
	zipcode='$zipcode', 
	userid='$userid', 
	password='$password', 
	dateenrolled='$dateenrolled', 
	dateexpiry='$dateexpiry', 
	cardnumber='$cardnumber', 
	branchcode='$branchcode', 
	categorycode='$categorycode', 
	address='$address', 
	city='$city';";
	
	//debug_r($sql);
	$id = $koha->db_db_query($sql);
	
	if($id)
		$result = true;
	else
		$result = false;
}

// clear the previous sessions by checking the ip
$sql = "SELECT id, a_session FROM sessions";
$row = $koha->returnArray($sql);

for($i = 0; $i < count($row); $i++)
{
	$id = $row[$i]["id"];
	$str = $row[$i]["a_session"];
	$x = strstr($str, "_SESSION_REMOTE_ADDR:");
	$ip_str = "";
	for($t = 0; $t < strlen($x); $t++)
	{
		if($x[$t] != "\n")	
		$ip_str .= $x[$t];
		else
		break;
	}
	
	if($ip_str != "")
	{
		$tmpArr = explode(":", $ip_str);
		$my_ip = trim($tmpArr[1]);
		
		$current_ip = $REMOTE_ADDR;
		
		if($my_ip != "" && $my_ip == $current_ip)
		{
			// get the session id
			$my_id = $id;
			
			// remove the session with same ip
			$sql = "DELETE FROM sessions WHERE id = '$my_id'";
			$result = $koha->db_db_query($sql);
			
			//debug_r($result);
		}
	}
}


// login to koha
if($categorycode == 'S')
{
	$js = "document.form1.action='http://192.168.0.211:8080/';\n";
	
	$UserLogin = "kohaadmin";
	$UserPassword = "katikoan";
}
else
{
	$js = "document.form1.action='http://192.168.0.211:81/';\n";
}
$js .= "document.form1.submit();\n";

$koha->db_close();
?>

<html>
<head>
</head>
<body>
<form name="form1" method="post" action"">
<!-- koha login -->
<input type="hidden" name="userid" value="<?=$UserLogin?>" />
<input type="hidden" name="password" value="<?=$UserPassword?>" />
</form>

<script>
<?=$js?>
</script>
</body>
