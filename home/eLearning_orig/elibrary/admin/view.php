<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");


include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");


intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$linterface 	= new interface_html();
$CurrentPage	= "PageContentView";

$lu = new libuser($UserID);
$LibeLib = new elibrary();

if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
if (!$plugin['eLib_ADMIN'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
/////////////////////////////////////////////////////////////////////////////////
// Filtering 
/////////////////////////////////////////////////////////////////////////////////
$LangArr = array();
$LangArr[] = array("chi",$i_QB_LangSelectChinese);
$LangArr[] = array("eng",$i_QB_LangSelectEnglish);
if ($intranet_session_language=="en")
{	
	$LangSelect = $linterface->GET_SELECTION_BOX($LangArr," name='Language' onchange='document.form1.submit()' ",$i_status_all."&nbsp;".$i_QB_LangSelect, $Language);
}
else
{
	$LangSelect = $linterface->GET_SELECTION_BOX($LangArr," name='Language' onchange='document.form1.submit()' ",$i_status_all."".$i_QB_LangSelect, $Language);
}
$TmpArr["Language"] = $Language;


$AuthorArr = $LibeLib->GET_ALL_AUTHOR_ARRAY();
if ($intranet_session_language=="en")
{	
	$AuthorSelect = $linterface->GET_SELECTION_BOX($AuthorArr," name='Author' onchange='document.form1.submit()' ",$i_status_all."&nbsp;".$eLib['Book']["Author"] , $Author);
}
else
{
	$AuthorSelect = $linterface->GET_SELECTION_BOX($AuthorArr," name='Author' onchange='document.form1.submit()' ",$i_status_all."".$eLib['Book']["Author"] , $Author);
}
$TmpArr["AuthorID"] = $Author;


$SourceArr = $LibeLib->GET_ALL_SOURCE_ARRAY();
if ($intranet_session_language=="en")
{	
	$SourceSelect = $linterface->GET_SELECTION_BOX($SourceArr," name='Source' onchange='document.form1.submit()' ",$i_status_all."&nbsp;".$eLib["SourceFrom"] , $Source);
}
else
{
	$SourceSelect = $linterface->GET_SELECTION_BOX($SourceArr," name='Source' onchange='document.form1.submit()' ",$i_status_all."".$eLib["SourceFrom"] , $Source);
}
$TmpArr["Source"] = $Source;
$TmpArr["Link1"] = "view_content.php";
/////////////////////////////////////////////////////////////////////////////////


# TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("DateModified");


$li->sql = $LibeLib->GET_BOOK_OVERVIEW_SQL($TmpArr);
$li->IsColOff = "2";
$li->no_col = 9;
$li->column_array = array(12,12,12,12,12,0,0,0);

// TABLE COLUMN
$li->column_list .= "<td class='tabletop tabletopnolink'>"."#"."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$eLib['Book']["Title"]."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$eLib['Book']["Author"]."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$eLib["html"]["publish"]."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$eLib['Book']["Publisher"]."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$eLib["SourceFrom"]."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$i_QB_LangSelect."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$eLib['Book']["DateModified"]."</td>\n";
$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("BookID[]")."</td>\n";

                          
### Button
$delBtn 	= "<a href=\"javascript:checkRemoveThis(document.form1,'BookID[]','remove_update.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'BookID[]','edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
$edit_link	= "edit.php";

### Title ###

$TAGS_OBJ[] = array($eLib['ManageBook']["ContentView"],"");
$MODULE_OBJ = $LibeLib->GET_MODULE_OBJ_ARR();


$linterface->LAYOUT_START();

?>
<SCRIPT LANGUAGE=Javascript>
function viewNotice(id)
{
         newWindow('./sign.php?NoticeID='+id,10);
}
function viewResult(id)
{
         newWindow('./result.php?NoticeID='+id,10);
}
function sign(id,studentID)
{
         newWindow('./sign.php?NoticeID='+id+'&StudentID='+studentID,10);
}
function viewReply(id,studentID)
{
         newWindow('./view.php?NoticeID='+id+'&StudentID='+studentID,10);
}
function viewNoticeClass(id)
{
         newWindow('./tableview.php?type=1&NoticeID='+id,10);
}
function checkRemoveThis(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm("<?=$i_Notice_RemovalWarning?>")){	            
                obj.action=page;                
                obj.method="POST";
                obj.submit();				             
                }
        }
}
function editNotice(nid)
{
         with(document.form1)
         {
                NoticeID.value = nid;
                action = "edit.php";
                submit();
         }
}

function newNotice()
{
         with(document.form1)
         {
                action = "new.php";
                submit();
         }
}

function checkKohaLogin()
{
		document.form1.action = "http://192.168.0.211:8080/";
		//document.form1.action = "http://192.168.0.211:81/";
		document.form1.method = "post";
		document.form1.submit();
}

</SCRIPT>

<br />
<form name="form1" method="get" action="view.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table width="96%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" class="tabletext">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
			<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><p><?=$koha_btn?></p><br /></td>
			</tr>
			</table>
			</td>
			<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
		</tr>

		<tr>
			<td align="left" valign="bottom">
			<?=$LangSelect?><?=$AuthorSelect?><?=$SourceSelect?>
			<br /><br />
			</td>
			<td align="right" valign="bottom" height="28">
			<?php
			if($lu->isTeacherStaff()) 
			{
			?>
				<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
					<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
					<table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td nowrap><?=$delBtn?></td>
					</tr>
					</table>
					</td>
					<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
				</tr>
				</table>
			<?php 
			} 
			?>
			</td>
		</tr>
				
		<tr>
			<td colspan="2">
			<?=$li->display();?>
			</td>
		</tr>
                                                                
		</table>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
<br />

<input type="hidden" name="NoticeID" value="" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>


<?php

intranet_closedb();
$linterface->LAYOUT_STOP();

?>