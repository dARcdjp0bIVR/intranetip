<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageConvertBook";

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if($errMsg == "1")
{
	$WarningMessage = "<p><font color='#ff0000'>Missing File(s) in zip file.</font></p>"; 
}

    
### Title ###
$TAGS_OBJ[] = array($eLib['ManageBook']["ConvertBook"],"");
$MODULE_OBJ = $LibeLib->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$SourceArr = array();
if (is_array($eLib['Source']) && count($eLib['Source'])>0)
{
foreach($eLib['Source'] as $Key => $Value)	
{
	$SourceArr[] = array($Key,$Value);
}
}
$SourceSelect = $linterface->GET_SELECTION_BOX($SourceArr," name='SourceFrom' ","", $SourceFrom);

$LangArr = array();
$LangArr[] = array("chi",$i_QB_LangSelectChinese);
$LangArr[] = array("eng",$i_QB_LangSelectEnglish);
$LangSelect = $linterface->GET_SELECTION_BOX($LangArr," name='Language' ","", $Language);


?>

<br />   
<form name="form1" method="post" enctype="multipart/form-data" action="convert_book_update.php" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
		<td><br />
		<table align="center" width="80%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$WarningMessage?></span></td>
			<td></td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Language"]?></span></td>
			<td>
			<?=$LangSelect?>
			</td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$eLib['ManageBook']["ImportZIP"]?></span></td>
			<td>	
			<input type="file" name="InputFile" />	
			</td>
		</tr>	
		
		<tr>
			<td colspan="2">
			<span class='tabletext'>
			<?=$eLib['ManageBook']["ImportZIPDescribeHeading"]?>
			<br />
			<br />
			<?=$eLib['ManageBook']["ImportZIPDescribeChinese"]?>
			<br />
			<?=$eLib['ManageBook']["ImportZIPDescribeEnglish"]?>
			</span>
			</td>
		</tr>	
		</table>
		
		</td>
	</tr>
	</table>
	</td>
</tr>

<tr>
	<td colspan="2">        
	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
    <tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
    <tr>
		<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_upload, "submit") ?>
		</td>
	</tr>
	</table>
	</td>
</tr>

</table>
<br />

</form>


<?php
intranet_closedb();

echo $linterface->FOCUS_ON_LOAD("form1.InputFile");
$linterface->LAYOUT_STOP();

?>