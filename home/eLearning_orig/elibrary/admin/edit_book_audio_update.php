<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$startCueArr = Array();
$endCueArr = Array();

foreach($_POST as $key => $data)
{
	//debug_r($key." ".$data);
	if($key != "BookID")
	{
		list($type, $BookPageID) = explode("_", $key);
		
		if($type == "start")
		$startCueArr[] = array("BookPageID" => $BookPageID, "soundStartCue" => $data);
		else if($type == "end")
		$endCueArr[] = array("BookPageID" => $BookPageID, "soundEndCue" => $data);
	}
}

for($i = 0; $i < count($startCueArr); $i++)
{
	$BookPageID = $startCueArr[$i]["BookPageID"];
	$SoundStartCue = $startCueArr[$i]["soundStartCue"];
	$SoundEndCue = $endCueArr[$i]["soundEndCue"];
	//echo "<p>BookPageID is ";
	//var_dump($BookPageID);
	//echo "</p>";
	//echo "<p>SoundStartCue is ";
	//var_dump($SoundStartCue);
	//echo "</p>";
	//echo "<p>SoundEndCue is ";
	//var_dump($SoundEndCue);	
	//echo "</p>";
	
	
	//debug_r($BookPageID." ".$soundStartCue." ".$soundEndCue);
	$sql = "UPDATE INTRANET_ELIB_BOOK_PAGE SET SoundStartCue='$SoundStartCue', SoundEndCue='$SoundEndCue' WHERE BookPageID='$BookPageID'";
	$LibeLib->db_db_query($sql);
}

//debug_r($startCueArr);
//debug_r($endCueArr);

intranet_closedb();
header("Location: index.php");
?>