<?php
header('Content-Type: text/html; charset=utf-8');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$InputArray["BookID"] = $BookID;
$InputArray["Publish"] = 1;
$LibeLib->UPDATE_BOOK_PUBLISH($InputArray);

intranet_closedb();
header("Location: index.php");


?>