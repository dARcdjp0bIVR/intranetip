<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageContentView";

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
$BookArr = $LibeLib->GET_BOOK_DETAILS($BookID);
$BookPageArr = $LibeLib->GET_BOOK_PAGE_TYPE_ARRAY($BookID);
$PrePageNum = ($BookPageArr["pre"] != "") ? $BookPageArr["pre"] : 0; 
$PageNum = ($BookPageArr["page"] != "") ? $BookPageArr["page"] : 0; 

if ($pageid == "")
{
	$pageid = 0;
}

if($pageid > 0)
{
  $sql = "SELECT IsChapterStart, ContentXML
          FROM
            INTRANET_ELIB_BOOK_PAGE
          WHERE
            BookID = '".$BookID."' AND
            PageID = '".$pageid."'
          ";
  $BookContent = $LibeLib->returnArray($sql);
  
	$Chapter_Indexes = $LibeLib->GET_CHAPTER_INDEXES($BookID);
}
else
{
	$sql = "	SELECT 
					BookChapterID, Title, OrigPageID, IsChapter
				FROM
					INTRANET_ELIB_BOOK_CHAPTER
				WHERE
					BookID = '".$BookID."'
				ORDER BY
				  OrigPageID, BookChapterID
	";
	$ContentPage = $LibeLib->returnArray($sql);
}
/*
$sql = "SELECT MAX(PageID)
        FROM
          INTRANET_ELIB_BOOK_PAGE
        WHERE
          BookID = '".$book_id."'
        ";
        
$temp = $LibeLib->returnVector($sql);
$MaxPage = $temp[0];
*/
$MinPage = 0;

$MaxPage = $PrePageNum + $PageNum;

$add_btn = $linterface->GET_ACTION_BTN("+", "button", "jADD_ROW(this)", "add_btn[]");
$add_btn = str_replace('"', '\"', $add_btn);

$add_image_btn = $linterface->GET_ACTION_BTN("+i", "button", "jADD_IMAGE_ROW(this)", "add_image_btn[]");
$add_image_btn = str_replace('"', '\"', $add_image_btn);

$del_btn = $linterface->GET_ACTION_BTN("-", "button", "jDELETE_ROW(this)", "delete_btn[]");
$del_btn = str_replace('"', '\"', $del_btn);
?>
<META http-equiv=Content-Type content='text/html; charset=UTF-8'>
<link href="/templates/style_mail_utf8.css" rel="stylesheet" type="text/css"  />
<link href="/templates/2009a/css/content_utf8.css" rel="stylesheet" type="text/css" />

<style type="text/css" >
html, body
{
	background-color:#FFFFFF;
	scrollbar-face-color:#F7F7F7;
	scrollbar-base-color:#F7F7F7;
	scrollbar-arrow-color:888888;
	scrollbar-track-color:#E2DFDF;
	scrollbar-shadow-color:#F7F7F7;
	scrollbar-highlight-color:#F7F7F7;
	scrollbar-3dlight-color:#C6C6C6;
	scrollbar-darkshadow-Color:#C6C6C6;
	background-image: url("");
	background-repeat: no-repeat;
	background-position: left top;
}
</style>

<base target="_parent" >
<script language="JavaScript">
jLinesArray = new Array();

function jCHANGE_PAGE(jParPageOffset)
{
  if(jParPageOffset != 0)
    form1.pageid.value = <?=$pageid?> + jParPageOffset;
  if(form1.pageid.value < <?=$MinPage?>)
  {
    form1.pageid.value = <?=$MinPage?>;
  }
  else if(form1.pageid.value > <?=$MaxPage?>)
  {
    form1.pageid.value = <?=$MaxPage?>;
  }
  
  form1.action = "view_content.php";
  form1.submit();
//  self.location = "edit_content_fcontent.php?BookID=" + form1.BookID.value + "&pageid=" + form1.pageid.value; 
}  


function resize_iframe()
{
	scroll_size = 10;
	iframeObj = this.parent.document.getElementById("content_frame");
	if(iframeObj==null) return;
	ch= this.document.body.clientHeight;
	cw = this.document.body.clientWidth;
			
	this.scroll(scroll_size,scroll_size);	
	while(this.document.body.scrollTop>0 || this.document.body.scrollLeft > 0)
	{
		if(this.document.body.scrollTop>0)
			iframeObj.style.height = ch;
		if(this.document.body.scrollLeft > 0)
			iframeObj.style.width = cw;
		ch+=scroll_size;
		cw+=scroll_size;
		this.scroll(scroll_size,scroll_size);
	}					
}

function jVIEW_PAGE()
{
	document.form1.CurrentMode.value=0;
	document.form1.submit();
}

function getIndexOf( ref, group ) {
  if(isNaN(group.length))
    return 0;
 
  for(var i = 0, n = group.length; i < n; i++ ) {
    if( group[ i ] == ref ) {
        return i;
    }
  }
}

function jADD_ROW(ParButton)
{
  index = getIndexOf(ParButton, form1.elements['add_btn[]']);
  if(form1.pageid.value == 0)
  {
    new_row = document.all.content_table.insertRow(index+1);
  
    cell_one = new_row.insertCell(0);
    cell_one.innerHTML = "<input type=\"text\" name=\"Title[]\" size=\"40\" />";
    cell_one.className = "tabletext";
      
    cell_two = new_row.insertCell(1);
    cell_two.innerHTML = "<?=str_pad("", 30, ".")?>";
    cell_two.className = "tabletext";
    cell_two.align = "center";
    
    cell_three = new_row.insertCell(2);
    cell_three.innerHTML = "<input type=\"text\" name=\"OrigPageID[]\" size=\"4\" />";
    cell_three.className = "tabletext";
    
    
    cell_four = new_row.insertCell(3);
    cell_four.innerHTML = "<SELECT NAME=\"IsChapter[]\"><OPTION value=\"1\" SELECTED>Chapter</OPTION><OPTION value=\"0\">Sub-Chapter</OPTION></SELECT>";
    cell_four.className = "tabletext";
    
    
    cell_five = new_row.insertCell(4);
    cell_five.innerHTML = "<?=$add_btn?>";
    cell_five.className = "tabletext";
    
    cell_six = new_row.insertCell(5);
    cell_six.innerHTML = "<?=$del_btn?>";
    cell_six.className = "tabletext";
  }
  else
  {
    new_row = document.all.content_table.insertRow(index+3);
  
    cell_one = new_row.insertCell(0);
    cell_one.innerHTML = "<input type=\"text\" name=\"Content[]\" size=\"40\" />";
    cell_one.className = "tabletext";
      
    cell_two = new_row.insertCell(1);
    cell_two.innerHTML = "<input type=\"text\" name=\"Class[]\" size=\"40\" /><input type=\"hidden\" name=\"p_type[]\" value=\"text\" /><input type=\"hidden\" name=\"OldContent[]\" value=\"\" />";
    cell_two.className = "tabletext";

    cell_three = new_row.insertCell(2);
    cell_three.innerHTML = "<?=$add_btn?>";
    cell_three.className = "tabletext";
    
    cell_four = new_row.insertCell(3);
    cell_four.innerHTML = "<?=$del_btn?>";
    cell_four.className = "tabletext";
  }
  
  resize_iframe();
}

function jADD_IMAGE_ROW(ParButton)
{
  index = getIndexOf(ParButton, form1.elements['add_image_btn[]']);

  new_row = document.all.content_table.insertRow(index+3);

  cell_one = new_row.insertCell(0);
  cell_one.innerHTML = "<input type=\"file\" name=\"ContentF[]\" size=\"20\" />";
  cell_one.className = "tabletext";
    
  cell_two = new_row.insertCell(1);
  cell_two.innerHTML = "<input type=\"text\" name=\"Class[]\" size=\"40\" value=\"image1\" /><input type=\"hidden\" name=\"p_type[]\" value=\"image\" /><input type=\"hidden\" name=\"OldContent[]\" value=\"\" />";
  cell_two.className = "tabletext";

  cell_three = new_row.insertCell(2);
  cell_three.innerHTML = "<?=$add_btn?>";
  cell_three.className = "tabletext";

  cell_four = new_row.insertCell(3);
  cell_four.innerHTML = "<?=$add_image_btn?>";
  cell_four.className = "tabletext";
  
  cell_five = new_row.insertCell(4);
  cell_five.innerHTML = "<?=$del_btn?>";
  cell_five.className = "tabletext";
  
  resize_iframe();
}

function jDELETE_ROW(ParButton)
{
  index = getIndexOf(ParButton, form1.elements['delete_btn[]']);
  if(form1.pageid.value == 0)
  {
    document.all.content_table.deleteRow(index+1);
  }
  else
  {
    document.all.content_table.deleteRow(index+3);
  }
  
  resize_iframe();
}

</script>

<br />   
<form name="form1" method="post" enctype="multipart/form-data" action="edit_content_fcontent_update.php" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2" >
	<table  border="0" cellspacing="0" cellpadding="10" align="center" width="90%" >	
	<tr>
		<td align="right">
		<?php  
			echo $linterface->GET_ACTION_BTN($button_view, "button", "jVIEW_PAGE()");  
		?>
		</td>
	</tr>				
	</table>
	</td>
</tr>

<tr>
  <td colspan="2">
  <table  border="1" cellspacing="0" cellpadding="10" align="center" bordercolor="#" >
  <tr>
    <td>
<?php
  $i = 0;

	if($pageid > 0)
	{
//		$display_content = preg_replace("/(\<img src=\")/","\\1/file/elibrary/content/".$BookID."/",$BookContent[0]['Content']);
//		echo nl2br($display_content);

    $temp = preg_replace("/(\<img src=\")(.*)(\" alt=\"Image\"\>\<\/img\>)/","\\2",$BookContent[0]['ContentXML']); 
    $temp = "<CONTENT>".$temp."</CONTENT>";
		
    $xml_parser = xml_parser_create("UTF-8");
    xml_parse_into_struct($xml_parser, $temp, $temp_arr_vals);
    xml_parser_free($xml_parser);

    foreach($temp_arr_vals as $key=>$value)
    {
      if($value['type'] == "complete")
      {
        $page_content[$i]['class'] = $value['attributes']['CLASS'];
        $page_content[$i]['value'] = $value['value'];
        
        $i++;
      }
    }    

    $IsChapterSelect = "<SELECT NAME=\"IsChapterStart\">";
    
    if($BookContent[0]['IsChapterStart'])
      $IsChapterSelect .= "<OPTION VALUE=\"1\" SELECTED>".$i_general_yes."</OPTION><OPTION VALUE=\"0\">".$i_general_no."</OPTION>";
    else
      $IsChapterSelect .= "<OPTION VALUE=\"1\">".$i_general_yes."</OPTION><OPTION VALUE=\"0\" SELECTED>".$i_general_no."</OPTION>";
    
    $IsChapterSelect .= "
                        </SELECT>
                        ";
                        
    $ChapterIndexSelect = "<SELECT NAME=\"ChapterID\">";

    $ChapterIndexSelect .= "<OPTION value=\"\">---</OPTION>";    
    foreach($Chapter_Indexes as $ChapterIndex)
    {
      $Selected = $BookContent[0]['ChapterID'] == $ChapterIndex[0]?"SELECTED":"";
      $ChapterIndexSelect .= "<OPTION value=\"".$ChapterIndex[0]."\" $Selected>".$ChapterIndex[1]."</OPTION>";
    }
    
    $ChapterIndexSelect .= "
                        </SELECT>
                        ";
		
		echo "<table id=\"content_table\" width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">\n";
		echo "<tr>\n";
		echo "<td class=\"tabletext\">".$eLib['EditBook']["IsChapterStart"]."</td>\n";
		echo "<td class=\"tabletext\">".$IsChapterSelect."</td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<td class=\"tabletext\">".$eLib['EditBook']["ChapterID"]."</td>\n";
		echo "<td class=\"tabletext\">".$ChapterIndexSelect."</td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<td class=\"tabletext\">Value</td>\n";
		echo "<td class=\"tabletext\">Class</td>\n";
		echo "</tr>\n";
		foreach($page_content as $key => $content)
		{	
			echo "<tr>\n";
			//if($content['class'] != "image1")
			if(substr($content['class'],0,5) != "image")
			{
  			echo "<td class=\"tabletext\" width=\"35%\"><input type=\"text\" name=\"Content[]\" value=\"".$content['value']."\" size=\"40\" /></td>\n";
  			echo "<td class=\"tabletext\" width=\"35%\"><input type=\"text\" name=\"Class[]\" value=\"".$content['class']."\" size=\"40\" /></td>\n";
  			echo "<input type=\"hidden\" name=\"OldContent[]\" value=\"\" />";
        echo "<input type=\"hidden\" name=\"p_type[]\" value=\"text\" />\n";        
			}
			else
			{
//			 $content['value'] = preg_replace("/\[img\](.*)\[\/img\]/", "<img src=\"/file/elibrary/content/".$BookID."/\\1\" alt=\"Image\" height=\"100\"></img>", $content['value']); 
        echo "<td class=\"tabletext\" width=\"35%\"><img src=\"/file/elibrary/content/".$BookID."/".$content['value']."\" alt=\"Image\"></img>\n";
        echo "<input type=\"file\" name=\"ContentF[]\" size=\"20\" /></td>\n";
        echo "<td class=\"tabletext\" width=\"35%\"><input type=\"text\" name=\"Class[]\" value=\"".$content['class']."\" size=\"40\" /></td>";
        echo "<input type=\"hidden\" name=\"OldContent[]\" value=\"".basename($content['value'])."\" />";
        echo "<input type=\"hidden\" name=\"p_type[]\" value=\"image\" />\n";
			 
        $ExistImage[] = basename($content['value']);
			}
			
			echo "<td class=\"tabletext\" width=\"10%\">".$linterface->GET_ACTION_BTN("+", "button", "jADD_ROW(this)", "add_btn[]")."</td>\n";
			echo "<td class=\"tabletext\" width=\"10%\">".$linterface->GET_ACTION_BTN("+i", "button", "jADD_IMAGE_ROW(this)", "add_image_btn[]")."</td>\n";
			echo "<td class=\"tabletext\" width=\"10%\">".$linterface->GET_ACTION_BTN("-", "button", "jDELETE_ROW(this)", "delete_btn[]")."</td>\n";
//			echo "<td class=\"tabletext\" width=\"10%\"><input type=\"checkbox\" name=\"IsDelete[]\" value=\"".$key."\"></td>\n";
			echo "</tr>\n";

//			$i++;
		}
		echo "<tr>\n";
		echo "<td class=\"tabletext\" width=\"35%\">&nbsp;</td>\n";
		echo "<td class=\"tabletext\" width=\"35%\">&nbsp;</td>\n";
		echo "<td class=\"tabletext\" width=\"10%\">".$linterface->GET_ACTION_BTN("+", "button", "jADD_ROW(this)", "add_btn[]")."</td>\n";
		echo "<td class=\"tabletext\" width=\"10%\">".$linterface->GET_ACTION_BTN("+i", "button", "jADD_IMAGE_ROW(this)", "add_image_btn[]")."</td>\n";
		echo "<td class=\"tabletext\" width=\"10%\">&nbsp;</td>\n";
    echo "</tr>\n";
		
		echo "</table>\n";
	}
	else
	{
		echo "<table id=\"content_table\" width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">\n";
		echo "<tr>\n";
		echo "<td colspan=\"5\" align=\"center\" class=\"tabletext\" >".$eLib['Book']["TableOfContent"]."</td>\n";
		echo "</tr>\n";
		foreach($ContentPage as $key => $content)
		{	
			echo "<tr>\n";
			if ($content['IsChapter'])
			{
			  $Options = "<OPTION value=\"1\" SELECTED>Chapter</OPTION>\n<OPTION value=\"0\">Sub-Chapter</OPTION>";
				echo "<td class=\"tabletext\" ><input type=\"text\" name=\"Title[]\" value=\"".$content['Title']."\" size=\"40\" /></td>\n";
			}
			else
			{
			  $Options = "<OPTION value=\"1\">Chapter</OPTION>\n<OPTION value=\"0\" SELECTED>Sub-Chapter</OPTION>";
				echo "<td class=\"tabletext\" ><input type=\"text\" name=\"Title[]\" value=\"".$content['Title']."\" size=\"40\" /></td>\n";
			}
			echo "<td class=\"tabletext\" align=\"center\">".str_pad("", 30, ".")."</td>\n";
			echo "<td class=\"tabletext\" width=\"10%\"><input type=\"text\" name=\"OrigPageID[]\" value=\"".$content['OrigPageID']."\" size=\"4\" /></td>\n";
			echo "<td class=\"tabletext\" width=\"10%\"><SELECT NAME=\"IsChapter[]\">".$Options."</SELECT></td>\n";
			echo "<td class=\"tabletext\" width=\"10%\">".$linterface->GET_ACTION_BTN("+", "button", "jADD_ROW(this)", "add_btn[]")."</td>\n";
			echo "<td class=\"tabletext\" width=\"10%\">".$linterface->GET_ACTION_BTN("-", "button", "jDELETE_ROW(this)", "delete_btn[]")."</td>\n";
//			echo "<td class=\"tabletext\" width=\"10%\"><input type=\"checkbox\" name=\"IsDelete[]\" value=\"".$key."\"></td>\n";
			echo "</tr>\n";
			echo "<input type=\"hidden\" name=\"BookChapterID[]\" value=\"".$content['BookChapterID']."\" />\n";
			
//			$i++;
		}
		
		echo "<tr>\n";
		echo "<td class=\"tabletext\" >&nbsp;</td>\n";
		echo "<td class=\"tabletext\" align=\"center\">&nbsp;</td>\n";
		echo "<td class=\"tabletext\" width=\"10%\">&nbsp;</td>\n";
		echo "<td class=\"tabletext\" width=\"10%\">&nbsp;</td>\n";
		echo "<td class=\"tabletext\" width=\"10%\">".$linterface->GET_ACTION_BTN("+", "button", "jADD_ROW(this)", "add_btn[]")."</td>\n";
		echo "<td class=\"tabletext\" width=\"10%\">&nbsp;</td>\n";
    echo "</tr>\n";
		
		echo "</table>\n";
	}	
?>
    </td>
	</tr>
	<tr>
		<td align="right">
		<?php  
			echo $linterface->GET_ACTION_BTN($button_save, "submit");  
		?>
		</td>
	</tr>	
	</table>
	</td>
</tr>


<tr>
	<td colspan="2">        
	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
    <tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
    <tr>
		<td align="center">
      	<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
    		<tr>
    			<td align='left' width='33%'><?= $pageid>$MinPage?$linterface->GET_ACTION_BTN($button_previous_page, "button", "jCHANGE_PAGE(-1)"):"&nbsp;" ?></td>
    			<td align="center" width='33%'><input type="text" name="pageid" value="<?=$pageid?>" /><?= $linterface->GET_ACTION_BTN($button_view, "button", "jCHANGE_PAGE(0)") ?></td>
    			<td align='right' width='33%'><?= $pageid<$MaxPage?$linterface->GET_ACTION_BTN($button_next_page, "button", "jCHANGE_PAGE(1)"):"&nbsp;" ?></td>
    		</tr>			
		  </table>
		</td>
	</tr>

    <tr>
		<td align="right">
		<?=$pageid?> / <?=$MaxPage?>
		</td>
	</tr>
		
	
	</table>                                
	</td>
</tr>

</table>                        
<br />

<input type="hidden" name="BookID" value="<?=$BookID?>" />
<input type="hidden" name="OrigPage" value="<?=$pageid?>" />
<input type="hidden" name="CurrentMode" value="1" />

<?php
  if(is_array($ExistImage))
    echo "<input type=\"hidden\" name=\"ExistImage\" value=\"".implode(",",$ExistImage)."\" />";
?>

</form>

<script language="javascript" >
<!--
	resize_iframe();
-->
</script>

<?php
intranet_closedb();

echo $linterface->FOCUS_ON_LOAD("form1.InputFile");

?>
