<?php
header('Content-Type: text/html; charset=utf-8');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
$LibFS = new libfilesystem();
$Location1 = $intranet_root."/file/elibrary";
if (!is_dir($Location1))
{
	$LibFS->folder_new($Location1);
}
$Location2 = $intranet_root."/file/elibrary/tmp";
if (!is_dir($Location2))
{
	$LibFS->folder_new($Location2);
}
$File = $Location2."/".mktime().".csv";
if (move_uploaded_file($HTTP_POST_FILES['InputFile']['tmp_name'],$File))
{
	$TmpArr["Source"]  = $SourceFrom;
	$TmpArr["Language"]  = $Language;
	$LibeLib->IMPORT_BOOK_INFO($File,$TmpArr);
	
	unlink($File);
}


intranet_closedb();
//header("Location: index.php?xmsg=add&pageNo=$pageNo&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&noticeType=$noticeType&status=$status&year=$year&month=$month");
header("Location: import_book.php");

?>