<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
intranet_auth();
intranet_opendb();

// will move to lang file
$eLib['ManageBook']["AudioManage"] = "Manage Audio";


$linterface 	= new interface_html();
$CurrentPage	= "PageContentManage";

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
    
### Title ###
if (is_array($BookID))
{
	$BookID = $BookID[0];
}
if ($BookID=="")
{
	$BookID=1;
}

$BookArr = $LibeLib->GET_BOOK_DETAILS($BookID);

if ($BookArr["Title"] != "")
{
	//$BookTitle = " (".iconv("UTF-8", "big5"."//IGNORE", $BookArr["Title"]).")";
	$BookTitle = " (".$BookArr["Title"].")";
}

// Get all book pages
$sql = "SELECT BookPageID, PageID, ChapterID, SoundStartCue, SoundEndCue from INTRANET_ELIB_BOOK_PAGE WHERE BookID = '$BookID' ORDER BY PageID ASC";
$pageArr = $LibeLib->returnArray($sql);

//debug_r($pageArr);

$x = "";
for($i = 0; $i < count($pageArr); $i++)
{
	$BookPageID = $pageArr[$i]["BookPageID"];
	$x .= "<tr>";
	$x .= "<td nowrap align=\"left\">Chapter ".$pageArr[$i]["ChapterID"]."</td>";
	$x .= "<td nowrap align=\"left\">Page ".$pageArr[$i]["PageID"]."</td>";
	$x .= "<td nowrap><input type=\"text\" name=\"start_$BookPageID\" value=\"".$pageArr[$i]["SoundStartCue"]."\" /></td>";
	$x .= "<td nowrap><input type=\"text\" name=\"end_$BookPageID\" value=\"".$pageArr[$i]["SoundEndCue"]."\" /></td>";
	$x .= "</tr>";
}

$AudioList = $x;

$TAGS_OBJ[] = array($eLib['ManageBook']["AudioManage"].$BookTitle,"");
$MODULE_OBJ = $LibeLib->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>

<form name="form1" method="post" action="edit_book_audio_update.php">
<table width="50%" border="0" cellspacing="3" cellpadding="3" align="center">
<tr>
<td align="left" nowrap>ChapterID</td>
<td align="left" nowrap>PageID</td>
<td align="left" nowrap>Sound Start Time (hh:mm:ss)</td>
<td align="left" nowrap>Sound End Time (hh:mm:ss)</td>
</tr>
<?=$AudioList?>
</table>

<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
<td align="center">
<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
&nbsp;&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button","parent.location='index.php'") ?>
</td>
</tr>
</table>

<input type="hidden" name="BookID" value="<?=$BookID?>" />
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>