<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->HAS_RIGHT())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$ParArr["BookID"] = $BookID;
$ParArr["Rating"] = $MyRating;
$ParArr["Content"] = $review_content;

$LibeLib->addBookReview($ParArr);

intranet_closedb();

header("Location: book_detail.php?BookID=$BookID");
?>