<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

// additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html("default3.html");
$CurrentPage	= "PageMyeClass";

$lo 	= new libeclass();

$lelib = new elibrary();
### Title ###
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;
$title = $lelib->printSearchInput($ParArr, $eLib);
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'];
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

$DisplayMode = 2; // list mode (no image cover)

if($CurrentPageNum == "" || $CurrentPageNum == null)
$CurrentPageNum = 1;

if($DisplayNumPage == "" || $DisplayNumPage == null)
$DisplayNumPage = 10;

if($sortField == "")
$sortField = "Title";

if($sortFieldOrder == "")
$sortFieldOrder = "DESC";

$inputArr["DisplayMode"] = $DisplayMode;
$inputArr["CurrentPageNum"] = $CurrentPageNum;
$inputArr["DisplayNumPage"] = $DisplayNumPage;

$inputArr["sortField"] = $sortField;
$inputArr["sortFieldOrder"] = $sortFieldOrder;

$inputArr["image_path"] = $image_path;
$inputArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$tmpArr[0]["value"] = "Title";
$tmpArr[1]["value"] = "Author";
$tmpArr[2]["value"] = "NumReview";
$tmpArr[3]["value"] = "Date";

$tmpArr[0]["text"] = $eLib["html"]["book_title"];
$tmpArr[1]["text"] = $eLib["html"]["author"];
$tmpArr[2]["text"] = $eLib["html"]["num_of_review"];
$tmpArr[3]["text"] = $eLib["html"]["last_submitted"];

$tmpArr[0]["width"] = "45%";
$tmpArr[1]["width"] = "20%";
$tmpArr[2]["width"] = "15%";
$tmpArr[3]["width"] = "20%";

$inputArr["sortFieldArray"] = $tmpArr;

$inputArr["selectNumField"] = 4;
$UserID = $_SESSION["UserID"];

if($sortField == "Title" || $sortField == "Author")
$con = "a.".$sortField;
else if($sortField == "Date" || $sortField == "NumReview")
$con = $sortField;

$con = $con." ".$sortFieldOrder;

$sql = "
		SELECT 
		a.BookID, 
		a.Title, 
		a.Author, 
		MAX(DATE_FORMAT(b.DateModified,'%Y/%m/%d')) as Date,
		Count(b.BookID) as NumReview
		FROM
		INTRANET_ELIB_BOOK a, INTRANET_ELIB_BOOK_REVIEW b
		WHERE
		a.BookID = b.BookID
		AND a.Publish = 1
		GROUP BY
		b.BookID
		ORDER BY
		Date desc, $con 
		";
		
		//b.UserID = {$UserID}
		//AND a.BookID = b.BookID		
		//debug_r($sql);

$resultArr = $lelib->displayBookDetail($inputArr, $eLib, $sql);
$contentHtml = $resultArr["contentHtml"];
$totalRecord = $resultArr["totalRecord"];
$DisplayNumPage = $resultArr["DisplayNumPage"];

if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}

$totalPage = ceil($totalRecord / $DisplayNumPage);

$linterface->LAYOUT_START();

// additional javascript for eLibrary only
include_once("elib_script_function.php");
?>


<script language="JavaScript" type="text/JavaScript">
<!--

function checkValidField(obj)
{
	obj.submit();
} // end function check valid field

//-->
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="320" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="175" valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="20">

<!-- Setting logo -->

<!-- End Setting logo -->

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
<!-- Record List Menu -->									
<?=$lelib->displayRecordListMenu($ParArr,$eLib);?>
<!-- End Record List Menu -->
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
<!-- catalogue Menu -->
<?=$eLib_Catalogue?>
<!-- end catalogue Menu -->
<br></td>
<td align="center" valign="top">
<!-- Start of Content -->
<form name="form1" action="elib_public_all_reviews.php" method="post">

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>

<!-- head menu -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td class="eLibrary_navigation">
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="index.php"><?=$eLib["html"]["home"]?></a> 
<!--
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<?=$eLib["html"]["public"]?>
-->
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle"><?=$eLib["html"]["all_reviews"]?></td>
</tr>
</table>
<!-- end head menu -->


<table width="98%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif" width="15" height="18"></td>
<td align="left" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif" width="19" height="18"></td>
<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif" width="25" height="18"></td>
</tr>

<tr>
<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif" width="15" height="21"></td>

<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_05.gif">
<!-- show content -->
<?=$contentHtml; ?>
<!-- end show content -->
</td>


<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif" width="25" height="30"></td>
</tr>
													
<tr>
<td width="15" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_07.gif" width="15" height="26"></td>
<td height="26" style="background-image:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_08.gif); background-repeat:repeat-x; background-position:right"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="8" height="20"></td>
<td width="25" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_09.gif" width="25" height="26"></td>
</tr>
</table>

<input type="hidden" name="DisplayMode" value="<?=$DisplayMode?>" >
<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >
<input type="hidden" name="sortField" value="<?=$sortField?>" >
<input type="hidden" name="sortFieldOrder" value="<?=$sortFieldOrder?>" >

</form>
<!-- End of Content -->
</td></tr></table>
</td></tr></table>
</td></tr></table>

<script language="JavaScript" type="text/JavaScript">
if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
