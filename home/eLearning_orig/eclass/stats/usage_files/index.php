<?php
@SET_TIME_LIMIT(600);
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once("$eclass40_filepath/src/includes/php/lib-quota.php");
include_once("$eclass40_filepath/system/settings/lang/".$lang);

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageStatistics";

$lo = new libeclass();
$leclass = new libeclass2007();

# Left menu 
$title = $i_eClass_Admin_Stats;
$TAGS_OBJ[] = array($i_eClass_Admin_stats_files,"index.php",1);
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$qo = new quota($eclass_db, 0);
$qo->calAllCourseFileUsage($eclass_db);


$searchTag 	= "<table border=\"0\" cellspacing=\"5\" cellpadding=\"5\"><tr>";
$searchTag  	.= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3")."</td>";
$searchTag 	.= "</tr></table>";

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field == "") $field = 3;

/*$sql = "SELECT
		   CONCAT('<a class=\"tablelink\" href=\"usage_course.php?course_id=', course_id, '\">', course_name, '</a>'),
		   ifnull(max_storage, '-'),
		   file_no,
		   file_size,
		   (max_storage-file_size) AS storage_left
	  FROM
			{$eclass_db}.course_file_usage
	  WHERE
           (course_name like '%$keyword%')
	  ";*/


$sql = "SELECT
		   CONCAT('<a class=\"tablelink\" href=\"usage_course.php?course_id=', course_id, '\">', course_name, '</a>'),
		   ifnull(max_storage, '-'),
		   file_no,
		   file_size,
		   (max_storage-file_size) AS storage_left
	  FROM
			{$eclass_db}.course_file_usage
	  WHERE
            (course_name like '%".str_replace(Array("%","_"),Array("\\%","\\_"),htmlspecialchars(addslashes($keyword),ENT_QUOTES))."%')	  ";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("course_name", "max_storage", "file_no", "file_size", "storage_left");
$li->db = $eclass_db;
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $i_admintitle_eclass;
$li->column_array = array(0,0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class='tabletop' style='vertical-align:middle'>#</td>\n";
$li->column_list .= "<td width=40% class='tabletop' style='vertical-align:middle'>".$li->column(0, $i_eClassCourseName)."</td>\n";
$li->column_list .= "<td width=15% class='tabletop' style='vertical-align:middle'>".$li->column(1, $i_eClassMaxStorage)."</td>\n";
$li->column_list .= "<td width=15% class='tabletop' style='vertical-align:middle'>".$li->column(2, $i_eClassFileNo)."</td>\n";
$li->column_list .= "<td width=15% class='tabletop' style='vertical-align:middle'>".$li->column(3, $i_eClassFileSize)."</td>\n";
$li->column_list .= "<td width=15% class='tabletop' style='vertical-align:middle'>".$li->column(4, $i_eClassFileSpace)."</td>\n";
?>


<form name="form1" method="get">

<!--table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table-->
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td align="center">
        <table width="96%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" class="tabletext">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="left" valign="bottom"><?=$searchTag?></td>
                  <td align="right" valign="bottom" height="28">
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <?=$li->display();?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
<!--table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table-->

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<p></p>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
