<?php
//editing: Kit
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
intranet_auth();
eclass_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageManagement";

# eClass
$lo = new libeclass($course_id);
$lelcass = new libeclass2007($course_id);

$lu = new libuser($UserID);
# block illegal access
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])
{
  if ($lu->RecordType!=1 || !$la->isAccessEClassMgt())
  {
    header("Location: /");
    exit;
  }
}

$uids = (is_array($user_id)) ? implode(",", $user_id) : $user_id;
$name_field = getNameFieldForRecord2eClassOnly();
//$sql = "SELECT user_id, firstname, lastname, memberType, class_number FROM user_course WHERE user_id IN (".$uids.") AND course_id = ".$course_id;
$sql = "
  SELECT 
    ".$name_field." as user_name, 
    class_number,
    memberType, 
    CONCAT('<input type=\"hidden\" name=\"user_id[]\" value=\"', user_id ,'\">',if(memberType = 'T',CONCAT('<input type=\"hidden\" name=\"teacher_removed[]\" value=\"', user_id ,'\">'),'')) as input,
    if(class_number is null or class_number = '','',concat(left(class_number,LOCATE(' - ',class_number)), ' - ' , LPAD(TRIM(SUBSTRING(class_number, LOCATE(' - ',class_number) + 3)), 3, '0'))) as class_order,
    user_id 
  FROM user_course WHERE user_id IN (".$uids.") AND course_id = ".$course_id;


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = false;
$viewTypeChangeEnabled = false;
# TABLE INFO
$field = 1;
$order = 1;
$lia = new libdbtable2007($field, $order, $pageNo);
$lia->field_array = array("user_name", "class_order", "memberType", "input");
$lia->db = $eclass_db;
$lia->sql = $sql;
$lia->no_col = 5;
$lia->IsColOff = 2;
$lia->with_navigation = false;
// TABLE COLUMN
$lia->column_list .= "<td width='1' class='tabletop tabletopnolink'>&nbsp;#&nbsp;</td>\n";
//$lia->column_list .= "<td width='75%' class='tabletop'>".$lia->column(0, $i_UserName)."</td>\n";
$lia->column_list .= "<td width='74%' class='tabletop'>".$i_UserName."</td>\n";
$lia->column_list .= "<td width='16%' class='tabletop'>".$i_UserClassName." (".$i_UserClassNumber.")</td>\n";
$lia->column_list .= "<td width='10%' class='tabletop'>".$i_eClass_Identity."</td>\n";
$lia->column_list .= "<td width='1' class='tabletop tabletopnolink'>&nbsp;</td>\n";

$removeUserList = $lia->display();

# get if there are Teacher users to be removed
$sql = "SELECT user_id FROM user_course WHERE user_id IN (".$uids.") AND memberType = 'T' AND course_id = ".$course_id; 
$row = $lo->returnVector($sql);

if(sizeof($row) > 0)
{
	$teacher_delete = implode(",",$row);
  ######################### GET EXISTING TEACHER LIST #########################
	$ec_db_name = $lo->db_prefix."c$course_id";
	$sql = "SELECT user_id, ".$name_field." as t_name, user_email ";
	$sql .= "FROM ".$ec_db_name.".usermaster ";
	$sql .= "WHERE memberType='T' AND (status IS NULL OR status='') AND user_id NOT IN ($teacher_delete)";
	$sql .= "ORDER BY t_name ";
	$row_e = $lo->returnArray($sql);

	$sql = "SELECT UserID, EnglishName, ChineseName, UserEmail ";
	$sql .= "FROM INTRANET_USER ";
	$sql .= "WHERE RecordType = 1 ";
	$sql .= "ORDER BY EnglishName ";
	$li = new libdb();
	$row_i = $li->returnArray($sql);

	$sql = "SELECT user_email ";
	$sql .= "FROM ".$ec_db_name.".usermaster ";
	$sql .= "WHERE user_id IN ($teacher_delete)";
	$row_d = $lo->returnArray($sql);
	$exclus_email = array();
	for ($i=0; $i<sizeof($row_d); $i++)
	{
		$exclus_email[] = trim($row_d[$i][0]);
	}

	$teacher_exist_list = "<select name='teacher_benefit'>\n";
	$teacher_exist_list .= "<option value='0'>".$i_ec_file_no_transfer."</option>\n";
	$teacher_exist_list .= "<option value=''>____________________</option>";
	$teacher_exist_list .= "<option value=''>&nbsp;-- ".$i_ec_file_exist_teacher." --&nbsp;</option>\n";
	$cours_teacher_size = sizeof($row_e);
	for ($i=0; $i<$cours_teacher_size; $i++)
	{
		$teacher_exist_list .= "<option value='".$row_e[$i][0]."'>".$row_e[$i][1]."</option>\n";
		$exclus_email[] = trim($row_e[$i][2]);
	}

	$teacher_exist_list .= "<option value=''>____________________</option>";
	$teacher_exist_list .= "<option value=''>&nbsp;-- ".$i_ec_file_not_exist_teacher." --&nbsp;</option>\n";
	for ($i=0; $i<sizeof($row_i); $i++)
	{
		if (!in_array($row_i[$i][3], $exclus_email))
		{
			$chiname = ($row_i[$i][2]==""? "": "(".$row_i[$i][2].")" );
			$teacher_exist_list .= "<option value='".$row_i[$i][0]."'>".$row_i[$i][1]." $chiname</option>\n";
		}
	}

	$teacher_exist_list .= "</select>\n";
	$y = "<br><br><br>";
	$y .= $i_ec_file_msg_transfer . "<br><br>" . $teacher_exist_list ."</br>";
}

$anotherTeacherList = $y;

### Title ###
$title_table = "<table width='100%' cellpadding='0' cellspacing='0' border='0'>";
$title_table .= "<tr>";
$title_table .= "<td class='contenttitle' style='vertical-align: bottom;'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_management.gif' align='absmiddle' /> $i_frontpage_menu_eclass_mgt</td>";
$title_table .= "<td class='tabletext' style='vertical-align: bottom;' align='right'>$size_msg</td>";
$title_table .= "</tr>";
$title_table .= "</table>";
$TAGS_OBJ[] = array($title_table,"");
$MODULE_OBJ = $lelcass->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION = $lo->course_code . " " . $lo->course_name;
$linterface->LAYOUT_START();
?>

<script language="javascript">
function checkform(obj){
	if (typeof(obj.teacher_benefit)!="undefined")
	{
		if (obj.teacher_benefit.options[obj.teacher_benefit.selectedIndex].value=="")
		{
			alert("<?=$i_ec_file_warning2 ?>");
			return false;
		}
		if (confirm("<?=$i_ec_file_user_delete_confirm?>"))
		{			
			if (obj.teacher_benefit.selectedIndex><?=$cours_teacher_size?>+4)
			{
				obj.is_user_import.value = 1;
				if (confirm("<?=$i_ec_file_confirm2?>"))
				{
					return true;
				} else
				{
					return false;
				}
			} else
			{
				return true;
			}
		} else
		{
			return false;
		}
	} else
	{
		return true;
	}
}
</script>

<form name="form1" method="post" action="delete_update.php" onSubmit="return checkform(this)">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION) ?></td>
    </tr>    
    <tr>
      <td>        
        <table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
          <tr>
            <td align="left" class="tabletext" align="center">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="left">
                    <?=$i_ec_file_user_delete?> <br>
                    <?=$removeUserList?>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    <?=$anotherTeacherList?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="center">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" colspan="2">
                <?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submitbtn") ?>
                <?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:self.location ='index.php?course_id=$course_id';","cancelbtn") ?>
                </td>
              </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
</table>

<input type=hidden name=course_id value=<?php echo $lo->course_id; ?>>
<input type='hidden' name='is_user_import' value='0'>
</form>


<?php
$linterface->LAYOUT_STOP();
eclass_closedb();
?>