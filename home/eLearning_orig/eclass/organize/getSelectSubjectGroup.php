<?php
//editing: Stanley
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb(); 
if ($subjectID!=""){
	$libdb = new libdb();
	$classTitle = "c.ClassTitle".strtoupper($intranet_session_language);
	$sql = "select $classTitle as ClassTitle, c.SubjectGroupID, 
			case when 
			(c.course_id is not null and c.course_id = '$selectedID' and c.course_id <> 0)
			then 'selected'
			else ''
			end as selected
			from {$intranet_db}.SUBJECT_TERM as t inner join 
			{$intranet_db}.SUBJECT_TERM_CLASS as c on
				t.SubjectGroupID = c.SubjectGroupID 
			where
				t.SubjectID = '$subjectID' and 
				((c.course_id is null OR c.course_id = 0)
				and t.YearTermID in (
					select
						ayt.YearTermID
					from
						{$intranet_db}.ACADEMIC_YEAR as ay
						inner join
						{$intranet_db}.ACADEMIC_YEAR_TERM as ayt
						on ay.AcademicYearID = ayt.AcademicYearID
					where NOW() between ayt.TermStart and ayt.TermEnd
				) 
				OR (c.course_id = '$selectedID' and c.course_id <> 0 
				and c.course_id is not null))
			";
	$result = $libdb->returnArray($sql); 

	$selectGroup = "<select id='subjectGroupID' name='subjectGroupID'>";
	$selectGroup .= "<option value=''>--{$button_select}--</option>";
	foreach($result as $r){
		$selected = $r['selected'];
		$selectGroup .= '<option value="'.$r["SubjectGroupID"].'" '.$selected.'  >'.$r["ClassTitle"].'</option>';
	}
	$selectGroup .= "</select>";
	echo $selectGroup;
}

echo "";
intranet_closedb();
?>