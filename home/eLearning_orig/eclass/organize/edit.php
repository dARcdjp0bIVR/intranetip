<?php
#editing: Stanley
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");

intranet_auth();
eclass_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageManagement";

# eClass
$lo 		= new libeclass($course_id[0]);
$eclass_quota 	= $lo->status();
$lelcass	= new libeclass2007();

#### subject and subject group select box #####
$libdb = new libdb();
$sql = "SELECT s.SubjectGroupID, s.SubjectID, c.IsFromSubjectGroup
		FROM {$eclass_db}.course as c
		inner join SUBJECT_TERM as s on
		c.SubjectGroupID = s.SubjectGroupID
		where c.course_id='".$course_id[0]."' and
			c.SubjectGroupID is not null";
/*$sql = "SELECT s.SubjectGroupID, s.SubjectID, 
		case when c.IsFromSubjectGroup =1 OR c.IsFromClass = 1
		then 1 ELse 0 End As IsEditable
		FROM {$eclass_db}.course as c
		Left Join SUBJECT_TERM as s on
		c.SubjectGroupID = s.SubjectGroupID
		and c.SubjectGroupID is not null and c.SubjectGroupID <> 0
		where c.course_id='".$course_id[0]."' and
			(c.SubjectGroupID is not null and c.SubjectGroupID <> 0
				OR c.YearClassID is not null and c.YearClassID <> 0
			)			
			"; *///echo $sql;
$sid = $libdb->returnArray($sql);

/* Edit : 20090917 */
//$IsEditable = ($sid[0]["IsEditable"]==1?"true":"false"); // String
//$IsEditable = ($sid[0]["IsEditable"]==1?true:false);		// Boolean
$IsEditable = ($sid[0]["IsFromSubjectGroup"]==1?"1":"0");		// Boolean

$selectGroup = '';
if (count($sid)>0){
	$subjectID = trim($sid[0]["SubjectID"]);
	$subjectGroupID = trim($sid[0]["SubjectGroupID"]);
	$classTitle = "c.ClassTitle".strtoupper($intranet_session_language);
	$sql = "select $classTitle as ClassTitle, c.SubjectGroupID, 
			case when 
			(c.course_id is not null and c.course_id = '$selectedID' and c.course_id <> 0)
			then 'selected'
			else ''
			end as selected
			from {$intranet_db}.SUBJECT_TERM as t inner join 
			{$intranet_db}.SUBJECT_TERM_CLASS as c on
				t.SubjectGroupID = c.SubjectGroupID 
			where
				t.SubjectID = '$subjectID' and 
				((c.course_id is null OR c.course_id = 0)
				and t.YearTermID in (
					select
						ayt.YearTermID
					from
						{$intranet_db}.ACADEMIC_YEAR as ay
						inner join
						{$intranet_db}.ACADEMIC_YEAR_TERM as ayt
						on ay.AcademicYearID = ayt.AcademicYearID
					where NOW() between ayt.TermStart and ayt.TermEnd
				) 
				OR (c.course_id = ".$course_id[0]." and c.course_id <> 0 
				and c.course_id is not null))
			";
	$result = $libdb->returnArray($sql);

	$selectGroup = "<select id='subjectGroupID' name='subjectGroupID'>";
	$selectGroup .= "<option value=''>--{$button_select}--</option>";
	foreach($result as $r){
		$selected = ($subjectGroupID==trim($r["SubjectGroupID"])?" selected ":"");
		$selectGroup .= '<option value="'.$r["SubjectGroupID"].'" '.$selected.'>'.$r["ClassTitle"].'</option>';
	}
	$selectGroup .= "</select>";
	$selectGroup .= $IsEditable?"<input type='hidden' value='$subjectGroupID' name='subjectGroupID'>":"";
}

$fieldName = $intranet_session_language=="en"?"EN_DES":"CH_DES";
$sql = "select RecordID, $fieldName as SubjectName from ASSESSMENT_SUBJECT where RecordStatus = 1 AND
(CMP_CODEID IS NULL OR CMP_CODEID = '')";
$result = $libdb->returnArray($sql);
$subjectBox = '<select id="subjectID" name="subjectID" onchange="getSelectGroup()">';
$subjectBox .= "<option value=''>--{$button_select}--</option>";
foreach ($result as $r){
	$selected = ($subjectID==trim($r["RecordID"])?"selected ":"");
	$subjectBox .= '<option value="'.$r["RecordID"].'" '.$selected.'>'.$r["SubjectName"].'</option>';
}
$subjectBox .= '</select>';
$subjectBox .= '&nbsp;&nbsp;<div id="subjectGroupSelectBox" style="display:inline">'.$selectGroup.'</div>';
###############################################

$lu = new libuser($UserID);
# block illegal access
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])
{
  if ($lu->teaching&&!$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& !$la->isAccessEClassNTMgtCourse() || $lu->RecordType!=1 || !$la->isAccessEClassMgt() )
  {
    header("Location: /");
  }
}
# verify user right
// get right of creator of the course / admin
$sql = "SELECT course_id FROM course where course_id='".$course_id[0]."' ";
$sql .= ($_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])? " LIMIT 1 ":" AND creator_id='$UserID'  LIMIT 1"; // Admin can manage any course
$row = $lo->returnArray($sql, 1);
$isAccessGained = ($row[0][0]!="");
if (!$isAccessGained)
{
  // get right of teacher identity in the course
  $sql = "SELECT course_id FROM user_course where user_email='".$lu->UserEmail."' AND memberType='T' AND course_id='".$course_id[0]."' LIMIT 1";
  $row = $lo->returnArray($sql, 1);
  $isAccessGained = ($row[0][0]!="");
}

$tool_select = "";

# Retrieve Equation Editor Rights
/*$content = trim(get_file_content($lo->filepath."/files/equation.txt"));
$equation_class = ($content=="")? array(): explode(",",$content);
$left = $lo->license_equation - sizeof($equation_class);
if (in_array($course_id[0],$equation_class))
{
  $tool_select .= "$i_eClass_Tool_EquationEditorUsing";
}*/
# Retrieve Equation Editor Rights
$content = trim(get_file_content($lo->filepath."/files/equation.txt"));
$equation_class = ($content=="")? array(): explode(",",$content);
$left = $lo->license_equation - sizeof($equation_class);
if ($left > 0 && !in_array($course_id[0],$equation_class))
{
    $tool_select .= "<input type=checkbox name=hasEquation value=1> $i_eClass_Tool_EquationEditor ($i_eClass_Tool_LicenseLeft: $left)";
}
else
{
    if (in_array($course_id[0],$equation_class))
    {
        $tool_select .= "$i_eClass_Tool_EquationEditorUsing";
    }
}

### Title ###
$TAGS_OBJ[] = array($i_frontpage_menu_eclass_mgt,"");
$MODULE_OBJ = $lelcass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":""). $i_adminmenu_eclass, "");
?>

<script language="javascript">
function checkform(obj){
  if(!check_text(obj.course_code, "<?php echo $i_alert_pleasefillin.$i_eClassCourseCode; ?>.")) return false;
  if(!check_text(obj.course_name, "<?php echo $i_alert_pleasefillin.$i_eClassCourseName; ?>.")) return false;
  if (document.getElementById('subjectID').selectedIndex > 0 && document.getElementById('subjectGroupID').selectedIndex == 0){
	alert('<?=$Lang['eclass']['warning']['noSubjectGroup']?>');
  }
}

function getSelectGroup(){
	selectedValue = document.getElementById('subjectID').options[document.getElementById('subjectID').selectedIndex].value;
	document.body.style.cursor='wait';
	$('div#subjectGroupSelectBox').load(
		'getSelectSubjectGroup.php',
		{subjectID: selectedValue, selectedID: '<?=$course_id[0]?>'},
		function(){
			document.body.style.cursor='default';
		}
	);
}
</script>


<br />
<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">

  <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
      <td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
    </tr>

    <tr>
      <td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td><br />
              <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
<? if ($isAccessGained) {?>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassCourseCode?> <span class='tabletextrequire'>*</span></span></td>
                  <td class='mayHidden'>
				  <input name="course_code" type="text" class="textboxnum" maxlength="10" value="<?php echo $lo->course_code; ?>" />
				  <?php if($IsEditable) echo '<input type="hidden" name="course_code" value="'.$lo->course_code.'">'?>
				  </td>
                </tr>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassCourseName?> <span class='tabletextrequire'>*</span></span></td>
                  <td class='mayHidden'>
				  <input name="course_name" type="text" class="textboxtext" maxlength="100" value="<?php echo $lo->course_name; ?>" />
				  <?php if($IsEditable) echo '<input type="hidden" name="course_name" value="'.$lo->course_name.'">'?>
				  </td>
                </tr>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassCourseDesc?> </span></td>
                  <td class='mayHidden'>
				  <?=$linterface->GET_TEXTAREA("course_desc", $lo->course_desc);?>
				  <?php if($IsEditable) echo '<input type="hidden" name="course_desc" value="'.$lo->course_desc.'">'?>
				  </td>
                </tr>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassNumUsers?> </span></td>
                  <td class='mayHidden'><input name="max_user" type="text" class="textboxnum" maxlength="5" value="<?php echo $lo->max_user; ?>" /></td>
                </tr>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassMaxStorage?> </span></td>
                  <td class="tabletext"><input name="max_storage" type="text" class="textboxnum" maxlength="5" value="<?php echo $lo->max_storage; ?>" /> MB</td>
                </tr>

				<!-- subject group -->
				<tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?></span></td>
                  <td class="tabletext mayHidden"><?=$subjectBox?></td>
                </tr>

<? if ($tool_select != "") { ?>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClass_Tools?> </span></td>
                  <td class="tabletext"><?=$tool_select?></td>
                </tr>
<? } ?>
<!--
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">&nbsp;</td>
                  <td class="tabletext mayHidden"><?php //echo $lo->eClassCategory(); ?></td>
                </tr>
-->
<? } else {?>
                <tr valign="top">
                  <td class="tabletext" colspan="2" align="center"><br><br><blockquote><blockquote><b>Sorry, you don't have the access right to add user to this course!</b></blockquote></blockquote></td>
                </tr>
<? } ?>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>

    <tr>
      <td colspan="2">
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
<? if ($isAccessGained) {?>
          <tr>
            <td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
          </tr>
          <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="center">
              <?= $linterface->GET_ACTION_BTN($button_save, "submit", "", "submit2") ?>
              <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
              <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancelbtn") ?>
            </td>
          </tr>
<? } else {?>
          <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="center">
              <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancelbtn") ?>
            </td>
          </tr>
<? } ?>
        </table>
      </td>
    </tr>
  </table>
  <br />

  <input type="hidden" name="course_id" value="<?php echo $lo->course_id; ?>" />
</form>
<script language="javascript">

if (<?=$IsEditable?>){
$('.mayHidden').find('input:text,input:checkbox,textarea,select').attr('disabled','true');
}
</script>
<?php
if ($IsEditable)
	$focus = "max_storage";
else
	$focus = "course_code";
print $linterface->FOCUS_ON_LOAD("form1.".$focus);
$linterface->LAYOUT_STOP();
eclass_closedb();
?>