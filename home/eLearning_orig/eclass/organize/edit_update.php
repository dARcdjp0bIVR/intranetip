<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");

intranet_auth();
eclass_opendb();
$course_code = intranet_htmlspecialchars($course_code);
$course_name = intranet_htmlspecialchars($course_name);
$course_desc = intranet_htmlspecialchars($course_desc);
$max_user = intranet_htmlspecialchars(0 + $max_user);
$max_storage = intranet_htmlspecialchars(0 + $max_storage);
if($max_user == 0) $max_user = "NULL";
if($max_storage == 0) $max_storage = "NULL";

$lo = new libeclass();
$iCal = new icalendar();
$lu = new libuser($UserID);
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])
{
  if ($lu->teaching&&!$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& !$la->isAccessEClassNTMgtCourse() || $lu->RecordType!=1 || !$la->isAccessEClassMgt() )
  {
    header("Location: /");
    die();
  }
}
#update calendar
$sql = "select CalID from course where course_id = '$course_id'";
$calID = $lo->returnVector($sql);
$sql = "update CALENDAR_CALENDAR set 
		Name = '$course_name', Description = '$course_desc'
		where CalID = '".$calID[0]."'
		";
$iCal->db_db_query($sql);

#update subject Group
$sql = "update SUBJECT_TERM_CLASS set course_id = NULL
		where
		course_id = $course_id
	";
$iCal->db_db_query($sql);
if (isset($subjectGroupID) && $subjectGroupID != ''){
	$sql = "update SUBJECT_TERM_CLASS set course_id = $course_id
			where
			SubjectGroupID = $subjectGroupID
		";
	$iCal->db_db_query($sql);
}

$lo->eClassUpdate($course_id, $course_code, $course_name, $course_desc, $max_user, $max_storage, $subjectGroupID);
$lo->eClassSubjectUpdate($subj_id, $course_id);


# Set Equation Editor Rights
if ($hasEquation == 1)
{
    $content = trim(get_file_content($lo->filepath."/files/equation.txt"));
    $equation_class = ($content=="")? array(): explode(",",$content);
    $equation_class[] = $course_id;
    write_file_content(implode(",",$equation_class),$lo->filepath."/files/equation.txt");
}

eclass_closedb();
header("Location: index.php?xmsg=update");
?>