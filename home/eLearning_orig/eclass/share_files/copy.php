<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");
include_once("$eclass40_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass40_filepath/system/settings/lang/".$lang);

if ($courseID=="" || $categoryID=="")
{
	header("Location: index.php");
	die();
}

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
# block illegal access
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])
{
  if (!$lu->teaching || !$la->isAccessEClassMgtCourse())
  {
    header("Location: /");
  }
}

$ori_memberType = $ck_memberType;
$ck_memberType = "Z";

$fm = new fileManager($courseID, $categoryID, $folderID);
$vPath = stripslashes($fm->getVirtualPath());

$file_ids = (is_array($file_id)) ? implode(",", $file_id) : $file_id;
if ($file_ids!="") {
	$sql  = "SELECT Title, IsDir, Size, FileID FROM eclass_file WHERE FileID IN ($file_ids) ORDER BY IsDir desc, Title asc ";
	$row = $fm->returnArray($sql, 4);
	for ($i=0; $i<sizeof($row); $i++) {
		if ($fm->checkAccessRight($ck_user_id, $row[$i][3])=="0") {
			$fileNFolder0 .= ($row[$i][1]==1) ?
				"<img src='$eclass_url_root/images/icon/resources/files/folder.gif' align='absmiddle' hspace='3' border='0'><span class=hFile>".$row[$i][0]." - ".number_format($row[$i][2])." $file_kb</span><br>" :
				"<img src='$eclass_url_root/images/icon/resources/files/file.gif' align='absmiddle' hspace='3' border='0'><span class=hFile>".$row[$i][0]." - ".number_format($row[$i][2])." $file_kb</span><br>";
		} else {
			$hidden .= "<input type=hidden name='file_id[]' value='".$row[$i][3]."'>\n";
			$fileNFolder1 .= ($row[$i][1]==1) ?
				"<img src='$eclass_url_root/images/icon/resources/files/folder.gif' align='absmiddle' hspace='3' border='0'>".$row[$i][0]." - ".number_format($row[$i][2])." $file_kb<br>" :
				"<img src='$eclass_url_root/images/icon/resources/files/file.gif' align='absmiddle' hspace='3' border='0'>".$row[$i][0]." - ".number_format($row[$i][2])." $file_kb<br>";
		}
	}
}
$break = ($fileNFolder1!="") ? "<br>" : "";
$fileNFolder = ($fileNFolder0!="") ?
				$fileNFolder1 ."$break<span class=title>".$file_no_copy_right."</span><br>".$fileNFolder0 :
				$fileNFolder1;

$linterface = new interface_html();
$CurrentPage = "PageShareArea";

$li = new libuser($UserID);
$lo = new libeclass();
$leclass = new libeclass2007();
$lp = new libportal();

### Title ###
$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_directory.gif' align='absmiddle' /> $i_eClass_Admin_Shared_Files";
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
$PAGE_NAVIGATION[] = array($i_eClass_Admin_Shared_Files, "index.php");	
$PAGE_NAVIGATION[] = array($button_copy, "");					
?>

<script language=javascript>
function checkform(obj) {
	var msg = "<?=$classfiles_alertMsgFile8?>";
	if(confirm(msg)){
		return true;
	} else
	{
		return false;
	}
}
</script>


<form name=form1 method="post" action="copy_update.php" onSubmit="return checkform(this);">
<br>
<table width=90% border=0 cellpadding=5 cellspacing=0 align="center">
  <tr>
      <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
  </tr>
  <tr>
  <td colspan="2">
        <table width="70%" border="0" cellspacing="0" cellpadding="5" align="center">  
        <tr>
      	<td colspan=2 class=bodycolor3 align=center><u><?=$profiles_from?></u></td>
        </tr>
        <tr>
      	<td class="formfieldtitle" nowrap valign="top"><span class=tabletext><?=$file_location?>:</span>&nbsp;</td>
      	<td >
      	<?= stripslashes($fm->getVirtualPath()) ?>
      	</td>
        </tr>
        <tr>
      	<td class="formfieldtitle" nowrap valign="top"><span class=tabletext><?php echo $file_file."/".$file_folder; ?>:</span>&nbsp;</td>
      	<td >
      	  <?= $fileNFolder ?><br>
      	</td>
        </tr>
      <?php
      if ($fileNFolder1!="") {
      ?>
        <tr>
      	<td colspan=2 class=bodycolor3 align=center><u><?=$profiles_to?></u></td>
        </tr>
        <tr>
      	<td class="formfieldtitle" nowrap valign="top"><span class=tabletext><?=$file_location?>:</span>&nbsp;</td>
      	<td valign="top">
      	  <select name="dest_folder">
      		<?php echo $fm->getFolderOptionAdmin($folderID, $categoryID, $courseID, 1); ?>
      	  </select>
      	</td>
        </tr>
      
      <?php
      }
      ?>
      </table>
   </td>
   </tr>
</table>

<table width="70%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
  <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
  <td align="center">
    <?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit2") ?>
    <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
    <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancelbtn") ?>
  </td>
</tr>
</table>
<input name="courseID" value="<?=$courseID?>" type=hidden>
<input name="categoryID" value="<?=$categoryID?>" type=hidden>
<input name="folderID" value="<?=$folderID?>" type=hidden>
<input name="course_id" value="<?=$courseID?>" type=hidden>
<?=$hidden?>
<p></p>
</form>

<?php
$ck_memberType = $ori_memberType;
$linterface->LAYOUT_STOP();
intranet_closedb();
?>