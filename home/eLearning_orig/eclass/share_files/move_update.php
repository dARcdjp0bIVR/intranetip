<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once("$eclass40_filepath/src/includes/php/lib-filesystem.php");
include_once("$eclass40_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass40_filepath/system/settings/lang/".$lang);

intranet_opendb();

if ($courseID=="") $courseID=$ck_course_id;

$li = new libdb();
$li->db = classNamingDB($courseID);

$params = "courseID=$courseID&categoryID=$categoryID&folderID=$folderID";

$fs = new phpduoFileSystem();
$fm = new fileManager($courseID, $categoryID, $folderID);

$user_name = $i_admintitle_sa;
$fm->user_name = $user_name;

if (sizeof($file_id)>0 && $course_id!="" && $dest_folder!="") {
	$tmp_id = split("\_", $dest_folder);
	if (sizeof($tmp_id)==2) {
		$fm->dest_categoryID = $tmp_id[0];
		$fm->dest_courseID = $eclass_db;
		$fm->dest_folderID = $tmp_id[1];

		for($i=0;$i<sizeof($file_id);$i++){
			// check disk space
			$sql = "SELECT Size FROM eclass_file ";
			$sql .= " WHERE FileID='".$file_id[$i]."'";
			$row = $fm->returnArray($sql, 1);
			$spaceNeed = $row[0][0];

			if ($folderID!="") {
				$fm->db = classNamingDB($fm->dest_courseID);
				$folderQuota = $fm->checkFolderQuota($fm->dest_folderID, $spaceNeed);
				$fm->db = classNamingDB($courseID);
				if ($folderQuota!="0")
					$folderQuotaMsg = "<br><br>".$file_msg16.":<br>".$folderQuota;
			} else
				$folderQuota="0";

			$fm->moveFile($file_id[$i], $fm->dest_folderID);
		}

		// update folder size
		$fm->updateFolderSize($folderID);
		$fm->db = classNamingDB($fm->dest_courseID);
		$fm->updateFolderSize($fm->dest_folderID);
	}
}

intranet_closedb();

$url = "index.php?xmsg=update&$params&reload=1";

header("Location: $url");
?>