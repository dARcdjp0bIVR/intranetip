<?php

#modified by adam : 2009-02-05

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libhomework.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../includes/libeclass_extra.php");
include_once("../../../includes/libworkhandle.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass_filepath/system/settings/lang/$eclass_lang");
intranet_auth();
intranet_opendb();

$assignment_id = (is_array($assignment_id)) ? $assignment_id[0] : $assignment_id;

if ($isAppendView)
{
	$l_wh = new handleWork($eclass_db, $subject_id, 2, 1);
	$AppendCourseID = $l_wh->returnAppendCourseID();
}

$lh = new libhomework();
$body_tags = " leftmargin='0' topmargin='0' marginwidth='0' marginheight='0' bgcolor='#CAF6FF' ";
include("../../../templates/fileheader.php");

for ($i=0; $i<sizeof($ck_subject_all); $i++)
{
	if ($ck_subject_all[$i][0]==$subject_id)
	{
		$homework_subject = $ck_subject_all[$i][1];
		break;
	}
}

$sql = "SELECT
			assignment_id,
			title,
			instruction,
			html_opt,
			attachment,
			".format_datetime_sql('deadline').",
			worktype,
			modified,
			Loading,
			".format_datetime_sql('StartDate').",
			SubjectID,
			answer,
			answersheet,
			modelanswer,
			unix_timestamp(now())-unix_timestamp(deadline),
			unix_timestamp(deadline),
			sheettype
		FROM
			assignment
		WHERE
			assignment_id = '$assignment_id'
		ORDER BY
			deadline desc, StartDate desc
		LIMIT
			1
		";

$course_id_used = ($AppendCourseID) ? $AppendCourseID : $ck_course_id;
$li = new libdb();
$li->db = classNamingDB($course_id_used);

$row = $li->returnArray($sql, 17);


$hw_title = $row[0][1];
//$hw_instruction = ($row[0][3]==1) ? nl2br(intranet_undo_htmlspecialchars($row[0][2])) : intranet_convertAllLinks(nl2br($row[0][2]));
$hw_instruction = nl2br(intranet_undo_htmlspecialchars($row[0][2]));
if ($hw_instruction=="") $hw_instruction = "--";
$fm = new fileManager($course_id_used, 3, "");
$fm->nameOnly = true;
$hw_attachfile = $row[0][4];

$hw_attachment = $fm->manipulateFile($row[0][4], 3, $course_id_used);
$hw_deadline = $row[0][5];
$worktype = $row[0][6];
$hw_modified = $row[0][7];
$hw_Loading = $row[0][8];
$hw_StartDate = $row[0][9];
$subject_id = $row[0][10];
$right_answer = $row[0][11];
$hw_answer = ($ck_memberType=="T" || $ck_memberType=="X" || $ck_memberType=="A" || $isAppendView) ? $fm->manipulateFile($right_answer,3, $course_id_used) : "--";
$hw_answersheet = trim($row[0][12]);
$hw_modelanswer = trim($row[0][13]);
$is_deadline_past = ($row[0][14]>=0);
$ts_deadline = $row[0][15];
$sheettype = $row[0][16];
//$hw_answersheet = "===PVoice===";
/*
if ($hw_answersheet=="" && $sheettype==1)
{
	$hw_answersheet = "answersheet";
}
*/

if ($hw_answersheet=="PC")
{
	$is_powerconcept = true;
	//$hw_deal_with = "javascript:viewPC()";
} 
else
{
	$is_answerSheet = (($hw_answersheet!="" || ($hw_answersheet=="" && $sheettype==1)) && ($hw_modelanswer!="" || $hw_answersheet=="===writing==="));
	//$is_answerSheet = ($hw_answersheet!="" || $hw_answersheet=="===writing===");
	$hw_deal_with = ($is_answerSheet) ? "javascript:viewAS()" : "javascript:uploadFile($assignment_id)";
}

if ($ck_memberType=="T" || $ck_memberType=="X" || $ck_memberType=="A" || $ck_memberType=="Z" || $isAppendView)
{
	switch ($hw_answersheet)
	{
		case "===writing===":
						$handin_method_html = $ec_words['online_writing'];
						break;
		case "PC":
						$handin_method_html = "<img src='http://$eclass_httppath/images/icon/powerconcept.gif' border='0' align='absmiddle' hspace='5'>".$jr_concept_map;
						break;
		case "===PVoice===":		
						$handin_method_html = $ec_words['voices']['sound_recording'];
						break;
		case "===PPractice===":		
						$handin_method_html = $ec_words['voices']['pronun_practice'];
						break;				
						
		default:
						$handin_method_html = ($hw_answersheet!="") ? "<a href='javascript:viewAS()'><img src='$jr_image_path/btn_watch.gif' border='0' align='absmiddle'>".$answer_sheet : $project_file;
						break;
	}
} 
else
{
	switch ($hw_answersheet)
	{
		case "===writing===":
						$handin_method_html = $ec_words['online_writing'];
						break;
		case "PC":
						$handin_method_html = "<img src='http://$eclass_httppath/images/icon/powerconcept.gif' border='0' align='absmiddle' hspace='5'>".$jr_concept_map;
						break;
		case "===PVoice===":
						$handin_method_html = $ec_words['voices']['sound_recording'];
						break;
		case "===PPractice===":		
						$handin_method_html = $ec_words['voices']['pronun_practice'];
						break;				
		default:
						$handin_method_html = ($hw_answersheet!="") ? $answer_sheet : $project_file;
						break;
	}
}

switch ($isView)
{
	case 2:		$url_cancel = "javascript:self.close()"; break;
	case 3:		$url_cancel = "hw_subject_std.php?subject_id={$subject_id}&is_finish_page=$is_finish_page"; break;
	case 4:		$url_cancel = "javascript:if (history.length==0){self.close()} else {history.back();};"; break;
	case 5:		$url_cancel = "hw_online_pend.php"; break;
	default:		$url_cancel = "hw_subject.php?subject_id={$subject_id}"; break;
}
$url_edit = "edit.php?subject_id={$subject_id}&assignment_id[]=$assignment_id&isView=$isView";

switch ($worktype)
{
	case 11: $type_image = "icon_onlinehw.gif"; break;
	case 12: $type_image = "icon_otherhw.gif"; break;
	case 13: $type_image = "icon_bkreport.gif"; break;
	case 14: $type_image = "icon_notice.gif"; break;
}


// check if handin exists and graded
if (($ck_memberType=="S" || $ck_memberType=="P") && $worktype==11)
{
	$mark_save = "%SAVE%";
	$hw_answer = ($right_answer!="") ? "" : "--";

	$fieldname = "filename, filename_mark, grade, comment, ".format_datetime_sql("inputdate").", handin_id, UNIX_TIMESTAMP(inputdate), is_correction, filepath ";
	$sql = "SELECT $fieldname FROM handin WHERE assignment_id ='$assignment_id' and user_id='$ck_user_id'";

	$row_h = $li->returnArray($sql, 9);
	if (sizeof($row_h)>0)
	{
		$filename = $row_h[0][0];

		$filename_mark = (trim($row_h[0][1])!="") ? $fm->manipulateFile($row_h[0][1],4) : "";
		$grade = $row_h[0][2];
		$comment = $row_h[0][3];
		$inputdate = $row_h[0][4];
		$handin_id = $row_h[0][5];
		$is_late = ($ts_deadline - $row_h[0][6] < 0);
		$is_correction = $row_h[0][7];
		$folder_path = $row_h[0][8];

		if ($hw_answersheet=="PC")
		{
			$hw_deal_with = "javascript:viewPC()";
		}

		
		//$file_uploaded = (trim($filename)!="") ? "<tr><td nowrap>".$homework_status['file_uploaded'].":</td><td >".$fm->manipulateFile($filename,4)."</td></tr>" : "";
		if (($plugin["power_voice"]) && (substr($attachment,-4) == ".mp3"))
		{	
			$file_uploaded = (trim($filename)!="") ? "<tr><td nowrap>".$homework_status['file_uploaded'].":</td><td >".$fm->manipulateFile($filename,4,1)."</td></tr>" : "";
		}
		else
		{
			$file_uploaded = (trim($filename)!="") ? "<tr><td nowrap>".$homework_status['file_uploaded'].":</td><td >".$fm->manipulateFile($filename,4)."</td></tr>" : "";
		}
		//echo "Debug 1: ".$file_uploaded;

		if ($comment==$mark_save)
		{
			// just SAVED, not hand in yet!
			$myStatus = "<tr><td nowrap>".$homework_status['status'].":</td><td nowrap><font color='blue'><b>".$homework_status['not_finish']."</b></font></td></tr>";
			if ($hw_answersheet=="PC")
			{
				$button_used = "<a href=\"javascript:doPC($assignment_id, 0)\"><img src='/images/btn_conceptmap_$intranet_session_language.gif' border='0' align=absmiddle></a>";
			} 
			else
			{
				$button_used = "<a href='$hw_deal_with'><img src='$jr_image_path/btn_do_hw.gif' border='0'></a>";
			}
			$url_answersheet = ($hw_answersheet=="===writing===") ? "url = \"http://$eclass_httppath/src/student/work_student/online_writing.php?assignment_id=$assignment_id\";" : "url = \"http://$eclass_httppath/src/student/work/assignment/answer/index.php?assignment_id=$assignment_id\";";
			$msg = ($inputdate) ? "<tr><td nowrap>".$homework_status['last_do'].":</td><td nowrap>$inputdate</td></tr>" : "";
		} 
		else
		{
			// show time of submission and corresponding message
			
			if (trim($grade)=="")
			{
				// if not graded, provide button to EDIT handin
				$myStatus = "<tr><td nowrap>".$homework_status['status'].":</td><td nowrap><font color='orange'>".$homework_status['submitted']."</font></td></tr>";
				if ($hw_answersheet=="PC")
				{
					$button_used = "<a href=\"javascript:doPC($assignment_id, 0)\"><img src='/images/btn_conceptmap_$intranet_session_language.gif' border='0' align=absmiddle></a>";
				} else
				{
					$button_used = "<a href='$hw_deal_with'><img src='$jr_image_path/btn_amend_hw.gif' border='0'></a>";
				}
				$url_answersheet = ($hw_answersheet=="===writing===") ? "url = \"http://$eclass_httppath/src/student/work_student/online_writing.php?assignment_id=$assignment_id\";" : "url = \"http://$eclass_httppath/src/student/work/assignment/answer/index.php?assignment_id=$assignment_id\";";
			} else
			{
				// provide button to show GRADED handin
				$myStatus = "<tr><td nowrap>".$homework_status['status'].":</td><td nowrap><font color='green'>".$homework_status['graded']."</font></td></tr>";
				$myStatus .= "<tr><td nowrap>".$homework_status['grade'].":</td><td nowrap><font color='red'><b>".$grade."</b></font></td></tr>";
				$myStatus .= ($comment!="") ? "<tr><td valign='top' nowrap>".$homework_status['comment'].":</td><td>".nl2br($comment)."</td></tr>" : "";
				
				$myStatus .= ($filename_mark!="") ? "<tr><td nowrap>".$homework_status['file_return'].":</td><td>".$filename_mark."</td></tr>" : "";
				if ($hw_answersheet=="PC")
				{
					$button_used = "<a href=\"javascript:viewPC($handin_id)\"><img src='/images/btn_conceptmap_$intranet_session_language.gif' border='0' align=absmiddle></a>";
				} else
				{
					$button_used = ($is_answerSheet) ? "<a href='$hw_deal_with'><img src='$jr_image_path/btn_see_result.gif' border='0'></a>" : "";
				}
				$url_answersheet = ($hw_answersheet=="===writing===") ? "url = \"http://$eclass_httppath/src/student/work_student/online_writing.php?assignment_id=$assignment_id\";" : "url = \"http://$eclass_httppath/src/student/work/assignment/answer/handinView.php?assignment_id=$assignment_id&handin_id=$handin_id\";";
				$hw_answer = ($is_deadline_past) ? $fm->manipulateFile($right_answer,3) : $hw_answer;
			}
			if ($is_correction && $handin_id!="")
			{
				$correction_action = ($is_correction==1) ? "<img src='$jr_image_path/btn_DoCorrect.gif' border='0'>" : "<img src='$jr_image_path/btn_CheckCorrect.gif' border='0'>";
				$url_answersheet = "url = \"http://$eclass_httppath/src/student/work_student/online_writing_correction.php?handin_id=$handin_id&assignment_id=$assignment_id\"";
				$button_used = "<a href=\"javascript:viewAS()\">".$correction_action."</a>";
			}
			$late_msg = ($is_late) ? "<font color=red>[".$assignments_late."]</font>" : "";
			$msg = ($inputdate) ? "<tr><td nowrap>".$assignments_datesubmit.":</td><td nowrap>$inputdate $late_msg</td></tr>" : "";
		}
	} 
	else
	{
		// provide button TO DO the homework
		$myStatus = "<tr><td nowrap>".$homework_status['status'].":</td><td nowrap><font color='red'><b>".$homework_status['not_start']."</b></font></td></tr>";
		if ($hw_answersheet=="PC")
		{
			$button_used = "<a href=\"javascript:doPC($assignment_id, 0)\"><img src='/images/btn_conceptmap_$intranet_session_language.gif' border='0' align=absmiddle></a>";
		} 
		/*
		elseif ($hw_answersheet=="===PVoice===")
		{
			$button_used = "<a href=\"javascript:doPVoice($assignment_id, 0)\"><img src='/images/btn_conceptmap_$intranet_session_language.gif' border='0' align=absmiddle></a>";				
		} 
		*/					
		else
		{
			$button_used = "<a href='$hw_deal_with'><img src='$jr_image_path/btn_do_hw.gif' border='0'></a>";
			$url_answersheet = ($hw_answersheet=="===writing===") ? "url = \"http://$eclass_httppath/src/student/work_student/online_writing.php?assignment_id=$assignment_id\";" : "url = \"http://$eclass_httppath/src/student/work/assignment/answer/index.php?assignment_id=$assignment_id\";";
		}
	}

	if ($ck_memberType=="P")
	{
		$button_used = ($is_answerSheet) ? "<a href='$hw_deal_with'><img src='$jr_image_path/btn_see_result.gif' border='0'></a>" : "";
	}
} 
else
{
	if ($ck_memberType=="T" || $ck_memberType=="X" || $ck_memberType=="A" || $ck_memberType=="Z" || $isAppendView )
	{
		// provide edit button
		if ($ck_memberType!="Z")
		{
			$button_used = ($isView==4) ? "" : "<a href='$url_edit'><img src='$jr_image_path/btn_edit.gif' border='0'></a>";
		}
		$url_answersheet = "url = \"http://$eclass_httppath/src/student/work/project/answersheet/index.php?edit=0&field=1&file=".urlencode($hw_attachfile)."&ecid=$AppendCourseID\";";
	}
}

$view_status = ($ck_memberType=="T" || $ck_memberType=="X" || $ck_memberType=="A") ? "$homework_index &gt; " : "";
$view_status .= ($isView==4) ? "<font color='#985E02'>$homework_subject</font>" : "<a href='$url_cancel'><font color='#985E02'>$homework_subject</font></a>";
$view_status .= " &gt; $button_view(".$homework_type[$worktype].")</font>";
?>

<script language="JavaScript" src="../../../templates/tooltip.js" type="text/javascript" ></script>
<style type="text/css">
#ToolTip{position:absolute; top: 0px; left: 0px; z-index:1; visibility:hidden;}
#ToolMenu{position:absolute; top: 0px; left: 0px; z-index:2; visibility:hidden;}
</style>

<script language="JavaScript">
isToolTip = true;
isMenu = true;

function tipsNow(text)
{
	if (document.readyState=="complete")	
	{
		tt= "<table border='0' cellspacing='0' cellpadding='1' width='<?=$descr_width?>'>\n";
			
		tt += "<tr><td class='tipborder'><table width='100%' border='0' cellspacing='0' cellpadding='5'>\n";
			
		tt += "<tr><td valign='top' nowrap class='tipbg'><?=$links_des?>:</td><td class='tipbg' width='90%'>" + text + "</td></tr></table>\n";
			
		showTip('ToolTip', tt);
		}

	}

function viewAS(){
	<?= $url_answersheet ?>

	intWidth = screen.width - 10; //Adjust for the end of screen (don't know why?)
	intHeight = screen.height - 80; //Adjust for the Icon Bar at the bottom of the window.
	strWinProp = " toolbar=no"         //Back, Forward, etc...
			   + ",location=no"      //URL field
			   + ",directories=no"   //"What's New", etc...
			   + ",status=yes"       //Status Bar at bottom of window.
			   + ",menubar=no"       //Menubar at top of window.
			   + ",resizable"    //Allow resizing by dragging. (Yes - Does not work with Netscape or IE)
			   + ",scrollbars=yes"   //Displays scrollbars is document is larger than window.
			   + ",titlebar=yes"     //Enable/Disable titlebar resize capability.
			   + ",width="+intWidth    //Standard 640,800/788, 800/788
			   + ",height="+intHeight  //Standard 480,600/541, 600/566
			   + ",top=0"              //Offset of windows top edge from screen.
			   + ",left=0"             //Offset of windows left edge from screen.
			   + "";
	window.open(url,'',strWinProp);
}

function uploadFile(id)
{
	var url = "upload.php?assignment_id="+id;
	newWindow(url, 1);
}

function doPC(aid, gid){
	newWindow("http://<?=$eclass_httppath?>/src/tool/powerconcept/index.php?assignment_id="+aid+"&group_id="+gid, 90);
}

function viewPC(hid){
	newWindow("http://<?=$eclass_httppath?>/src/tool/powerconcept/view.php?handin_id="+hid, 90);
}

function checkform(obj){
	<?php
	if ($singleFileVersion)
	{
	?>
		globalAlertMsg9 = '<?=$file_msg23?>';
		var tmp_path = obj.userfile.value.split('\\');
		myfile = tmp_path[tmp_path.length-1];
		if (!validateFilename(myfile))
		{
			return false;
		}
		Big5FileUploadHandler();
	<?php
	}
	?>

	return true;
}

function doPVoice(aid, gid)
{	
	newWindow("http://<?=$eclass_httppath?>/src/tool/powervoice/index.php?assignment_id="+aid+"&group_id="+gid+"&isFileNew=1&fileName=<?=$pc_filename?>&fileID=<?=$fid?>&courseID=<?=$ck_course_id?>&categoryID=4&mode=s&autoSubmit=2&worktype=<?=$workType?>", 90,580,310);
}

function viewPVoice(hid){
	newWindow("http://<?=$eclass_httppath?>/src/tool/powervoice/view.php?handin_id="+hid+"&mode=s", 90,500,200);
}

function editPVoiceSingle(fileID,aid, gid)
{
	newWindow("http://<?=$eclass_httppath?>/src/tool/powervoice/index.php?assignment_id="+aid+"&group_id="+gid+"&fileID="+fileID+"&courseID=<?=$ck_course_id?>&categoryID=4&mode=s&fromPageType=assignment&autoSubmit=3&worktype=<?=$workType?>", 12,580,310);
}

</script>

<?php
$singleFileVersion = ($sheettype!="MF");

if ($singleFileVersion)
{
?>
<form name="form1" action="http://<?=$eclass_httppath?>/src/student/work_student/upload.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this)">
<?php
} 
else
{
?>
<form name="form1" action="http://<?=$eclass_httppath?>/src/student/work_student/upload.php" method="get" enctype="multipart/form-data" onSubmit="return checkform(this)">
<?php
}
?>
<div id="ToolTip"></div>
<div id="ToolMenu"></div>
<table width="660" border="0" cellspacing="0" cellpadding="0">
<tr>
<td> 
	<table width="660" border="0" cellpadding="0" cellspacing="0" class="bodytext-bold">
  	<tr>
    	<td width="245" background="<?=$jr_image_path?>/class_hwlist/popup_titlebar.gif"><img src="<?=$jr_image_path?>/class_hwlist/popup_title.gif"></td>
    	<td valign="middle" background="<?=$jr_image_path?>/class_hwlist/popup_titlebar.gif"><font color="#985E02"><?=$view_status?></td>
  	</tr>
	</table>
	</td>
</tr>
<tr>
	<td align="center"><img src="<?=$jr_image_path?>/class_hwlist/popup_cloud.gif" width="660" height="64"></td>
</tr>
<tr>
<td align="center" background="<?=$jr_image_path?>/class_hwlist/popup_bg.gif"><img src="<?=$jr_image_path?>/class_hwlist/popup_gradient_down.gif" width="660" height="12">

<table width="612" border="1" cellpadding="5"cellspacing="0" bordercolorlight="#FC8473" bordercolordark="#FBFDE2" class="bodytext">

<?php

if ($worktype==11)
{
?>
	<tr>
	<td colspan=2 class="bodytext-bold"><img src="<?=$jr_image_path?>/class_hwlist/<?=$type_image?>" border='0' align='absmiddle' hspace='5'><font color="#EA6303"><?=$hw_title?></font></td>
	</tr>

	<tr>
	<td align=right valign="top" width='100'><span class="title"><?php echo $assignments_instruction; ?>:</span>&nbsp;</td>
	<td width='512'><?=$hw_instruction?></td>
	</tr>

	<tr>
	<td align=right><span class="title"><?php echo $assignments_attachment; ?>:</span>&nbsp;</td>
	<td nowrap><?=$hw_attachment?></td>
	</tr>

	<tr>
	<td align=right nowrap><span class="title"><?= $ec_words['method'] ?>:</span>&nbsp;</td>
	<td nowrap><?=$handin_method_html?></td>
	</tr>

	<tr>
	<td align=right nowrap valign=top><span class="title"><?php echo $assignments_modelanswer; ?>:</span>&nbsp;</td>
	<td>
	<?php
	#adam: can play model answer once deadline was past
	if($right_answer && $is_deadline_past){
		echo $fm->manipulateFile($right_answer,3);
	}else{
		echo $hw_answer;
	}	
	if ($hw_answer!="") {
	?>
		<br>
	<?php
	}
	?>
	<?php
	if ($hw_answer!="--") {//Work_model_ans_for_pronun_practice
	if($hw_answersheet=='===PPractice===')
	?>
		<span class=guide><?=$Work_model_ans_for_pronun_practice?></span>
		
	<?php
	}
	else
	{
	?>
		<span class=guide><?=$Work_model_ans?></span>
	<?}?>
	</td>
	</tr>

	<tr>
	<td align="right"><?=$i_Homework_loading?>:&nbsp;</td>
	<td ><?=$lh->getSelectedLoadingText($hw_Loading)?> <?=$i_Homework_hours?></td>
	</tr>

	<tr>
	<td align=right nowrap><span class="title"><?=$quizbank_sd?>:</span>&nbsp;</td>
	<td><?=$hw_StartDate ?></td>
	</tr>
	<tr>
	<td align=right><span class="title"><?php echo $assignments_deadline; ?>:</span>&nbsp;</td>
	<td><?=$hw_deadline ?></td>
	</tr>

<?php
}
elseif ($worktype==12)
{
?>
	<tr>
	<td colspan=2 class="bodytext-bold"><img src="<?=$jr_image_path?>/class_hwlist/<?=$type_image?>" border='0' align='absmiddle' hspace='5'><font color="#EA6303"><?=$hw_title?></font></td>
	</tr>

	<tr>
	<td align=right valign="top" width='100'><span class="title"><?php echo $assignments_instruction; ?>:</span>&nbsp;</td>
	<td width='512'><?=$hw_instruction?></td>
	</tr>

	<tr>
	<td align=right><span class="title"><?php echo $assignments_attachment; ?>:</span>&nbsp;</td>
	<td nowrap><?=$hw_attachment?></td>
	</tr>

	<tr>
	<td align="right"><?=$i_Homework_loading?>:&nbsp;</td>
	<td ><?=$lh->getSelectedLoadingText($hw_Loading)?> <?=$i_Homework_hours?></td>
	</tr>

	<tr>
	<td align=right nowrap><span class="title"><?=$quizbank_sd?>:</span>&nbsp;</td>
	<td><?=$hw_StartDate ?></td>
	</tr>
	<tr>
	<td align=right><span class="title"><?php echo $assignments_deadline; ?>:</span>&nbsp;</td>
	<td><?=$hw_deadline ?></td>
	</tr>

<?php
}
elseif ($worktype==13)
{ // reading scheme
?>
	<tr>
	<td colspan=2 class="bodytext-bold"><img src="<?=$jr_image_path?>/class_hwlist/<?=$type_image?>" border='0' align='absmiddle' hspace='5'><font color="#EA6303"><?=$hw_title?></font></td>
	</tr>
	<tr>
	<td align=right valign="top" width='100'><span class="title"><?php echo $assignments_instruction; ?>:</span>&nbsp;</td>
	<td width='512'><?=$hw_instruction?></td>
	</tr>

	<tr>
	<td align=right><span class="title"><?php echo $assignments_attachment; ?>:</span>&nbsp;</td>
	<td nowrap><?=$hw_attachment?></td>
	</tr>

	<tr>
	<td align="right"><?=$i_Homework_loading?>:&nbsp;</td>
	<td ><?=$lh->getSelectedLoadingText($hw_Loading)?> <?=$i_Homework_hours?></td>
	</tr>

	<tr>
	<td align=right nowrap><span class="title"><?=$quizbank_sd?>:</span>&nbsp;</td>
	<td><?=$hw_StartDate ?></td>
	</tr>
	<tr>
	<td align=right><span class="title"><?php echo $assignments_deadline; ?>:</span>&nbsp;</td>
	<td><?=$hw_deadline ?></td>
	</tr>
<?php
}
elseif ($worktype==14)
{
?>
	<tr>
	<td colspan=2 class="bodytext-bold"><img src="<?=$jr_image_path?>/class_hwlist/<?=$type_image?>" border='0' align='absmiddle' hspace='5'><font color="#EA6303"><?=$hw_title?></font></td>
	</tr>

	<tr>
	<td align=right valign="top" width='100'><span class="title"><?php echo $assignments_instruction; ?>:</span>&nbsp;</td>
	<td width='512'><?=$hw_instruction?></td>
	</tr>

	<tr>
	<td align=right><span class="title"><?php echo $assignments_attachment; ?>:</span>&nbsp;</td>
	<td nowrap><?=$hw_attachment?></td>
	</tr>

	<tr>
	<td align=right nowrap><span class="title"><?=$quizbank_sd?>:</span>&nbsp;</td>
	<td><?=$hw_StartDate ?></td>
	</tr>

<?php
}
?>

</table>
<?php

if($worktype==11)
{
	if (($ck_memberType=="S" || $ck_memberType=="P") && $hw_answersheet!=""  && $hw_answersheet!="===PVoice===" && $hw_answersheet!="===PPractice===") 
	{
				
	?>
		<br><table style="border:dotted #FF77FF" cellpadding=4>
		<?=$myStatus.$file_uploaded.$msg?>
		</table>
	<?php
	}
	else if($ck_memberType!="T" && $ck_memberType!="X")
	{
		if($grade=="")
		{
			
			if ($hw_answersheet=="===PVoice===" || $hw_answersheet=="===PPractice===")
			{
				echo "<br />";
				if (false && $singleFileVersion)
				{
				}
				else
				{
					# new version (multiple files supported)
					
					if($right_answer && $is_deadline_past){
						$file_submitted = $fm->manipulateFileMulti($filename, 4, $folder_path, 0, 1, $assignment_id, 2);
					}else{	
						$file_submitted = $fm->manipulateFileMulti($filename, 4, $folder_path);
					}	
					$folderID = $fm->ParentDirID;
			?>
					<script language="JavaScript">
					function submitSave(obj, myChoice)
					{
						if (obj.handin_id.value=="")
						{
							alert("<?=$ec_warning['assignment_file_missed']?>");
						}
						else
						{
							var conmsg = (myChoice==0) ? "<?=$ec_warning['submit_assignment']?>" : "<?=$ec_warning['assignment_handin_save']?>";
							if (!confirm(conmsg))
							{
							}
							else
							{
								obj.method = "post";
								obj.is_save.value = myChoice;
								obj.action = "handin_file_update.php";
								obj.submit();
							}
						}
					}
					function removeFile(fileID, filePos)
					{
						if (confirm("<?=$ec_warning['assignment_file_remove']?>"))
						{
							document.form1.method = "post";
							document.form1.filePos.value = filePos;
							document.form1.fileID.value = fileID;
							document.form1.action = "handin_file_remove_update.php";
							document.form1.submit();
							document.form1.method = "get";
						}
					}
	
					function editFile(fileID)
					{
						<?php
						if ($hw_answersheet == "===PVoice===")
						{
						?>
							newWindow("http://<?=$eclass_httppath?>/src/tool/powervoice/index.php?assignment_id=<?=$assignment_id?>&group_id=0&fileName=<?=$pc_filename?>&fileID="+fileID+"&courseID=<?=$ck_course_id?>&categoryID=4&mode=s&autoSubmit=2&worktype=<?=$workType?>", 12,580,310);
						<?php
						}
						else 
						{
						?>					
							document.form1.fileID.value = fileID;
							document.form1.method = "post";
							postInstantForm(document.form1, 'handin_file_edit.php', '', 15, 'intranet_popup15');
							document.form1.method = "get";
						<?php
						}
						?>																				
					}
	
					function addFile()
					{
						document.form1.method = "post";
						postInstantForm(document.form1, 'attach.php', '', 14, 'intranet_popup14');
						document.form1.method = "get";
					}
	
					function reloadOpener()
					{
						return;
					}
					
					<?php
					if ($plugin['power_voice'])
					{ 
						if($hw_answersheet == "===PPractice===")
						$HasPractice = 1;
						else
						$HasPractice = 0;
					?>
						function addPVoice(fileType)
						{
							newWindow("http://<?=$eclass_httppath?>/src/tool/powervoice/index.php?HasPractice=<?=$HasPractice?>&assignment_id=<?=$assignment_id?>&group_id=0&isFileNew=1&fileName=<?=$pc_filename?>&fileID=<?=$fid?>&courseID=<?=$ck_course_id?>&categoryID=4&mode=s&fromFile="+fileType+"&autoSubmit=2&worktype=<?=$workType?>", 12,610,290);
						}
					
						function editPVoice(fileType,fileID)
						{
							newWindow("http://<?=$eclass_httppath?>/src/tool/powervoice/index.php?HasPractice=<?=$HasPractice?>&assignment_id=<?=$assignment_id?>&group_id=0&fileName=<?=$pc_filename?>&fileID="+fileID+"&courseID=<?=$ck_course_id?>&categoryID=4&mode=s&fromFile="+fileType+"&autoSubmit=2&worktype=<?=$workType?>", 12,580,270);
						}
						function file_exists()
						{
							alert("<?=$classfiles_alertMsgFile12?>");
						}					
					<?php
					} 
					?>
					
					</script>
	
					<span class="guide"><?=$ec_words['assignment_powervoice_guide']?></span>
					<table align="center" border="1" bordercolor="#BBAABB" cellpadding="5" cellspacing="0" width="90%">
					<tr><td width="5%" valign="top" class="title">&nbsp;</td><td width="45%" valign="top" class="title"><?=$wording['filename']?></td><td valign="top" class="title" nowrap><?=$file_size?></td><td width="35%" valign="top" class="title"><?=$file_lastmodi?></td><td width="5%" valign="top">&nbsp;</td></tr>
					<!--#adam: can play model answer once deadline was past-->
					<?=$file_submitted?>
					</table>
						
					<?php
					if ($singleFileVersion) 
					{
						if (trim($filename) == "")
						{
					?>						
							<table align="center" border="0" cellpadding="5" cellspacing="0" width="90%">
							<tr><td>&nbsp;&nbsp; <a href='javascript:addPVoice(1)'><img src='<?=$jr_image_path?>/btn_add.gif' border='0'></a></td></tr>
							</table>
				<?php
						}					
					} 
					else 
					{
					?>			
							<table align="center" border="0" cellpadding="5" cellspacing="0" width="90%">
							<tr><td>&nbsp;&nbsp; <a href='javascript:addPVoice(1)'><img src='<?=$jr_image_path?>/btn_add.gif' border='0'></a></td></tr>
							</table>
					<?php
					}
					?>			
					
				<?php
					if ($ck_memberType=="T")
					{
						$buttons = "<a href='javascript:window.close()'><img src='$jr_image_path/btn_upload.gif' border='0'></a>";
					} 
					else
					{
						$buttons = "<a href='javascript:submitSave(document.form1, 0)'><img src='$jr_image_path/btn_submit.gif' border='0'></a>&nbsp;<a href='javascript:submitSave(document.form1, 1)'><img src='$jr_image_path/btn_temporary_save.gif' border='0'></a>";
					}
				}
			}
			else
			{
			echo "<br />";
			if ($singleFileVersion)
			{
				# old version
				if($right_answer && $is_deadline_past){
					$file_submitted = $fm->manipulateFile($filename,4,"",1,1,"",$assignment_id,2);
				}else{	
					$file_submitted = $fm->manipulateFile($filename,4);
				}
				?>
				<script language="JavaScript">
				function submitSave(obj, myChoice)
				{
					var conmsg = (myChoice==0) ? "<?=$ec_warning['submit_assignment']?>" : "<?=$ec_warning['assignment_handin_save']?>";
					if (!confirm(conmsg))
					{
					}
					else
					{
						obj.method = "post";
						obj.is_save.value = myChoice;
						obj.action = "http://<?=$eclass_httppath?>/src/student/work_student/upload.php";
						obj.submit();
					}
				}
				</script>
				<?php
				if ($file_submitted!="" && $file_submitted!="--")
				{
					#adam: can play model answer once deadline was past
					echo "<table border=1 cellspacing=0 cellpadding=5><tr><td><table border='0'><tr><td valign='top'>".$Assignments_submitted.": </td><td>".$file_submitted."<br>".$inputdate."</td></tr></table></td></tr></table><br>";
				} 
				else
				{
					echo $work_upload;
				}

				echo "
					<table><tr><td><span class='guide'>".$upload_rules."</span></td></tr></table>
					<br>
					<input class=file type=file name=userfile size=35>
					<input type='hidden' name='userfile_hidden' >
					" . generateFileUploadNameHandler("form1","userfile","userfile_hidden");

				if ($ck_memberType=="T")
				{
					$buttons = "<a href='javascript:window.close()'><img src='$jr_image_path/btn_upload.gif' border='0'></a>";
				} 
				else
				{
					$buttons = "<a href='javascript:submitSave(document.form1, 0)'><img src='$jr_image_path/btn_submit.gif' border='0'></a>&nbsp;<a href='javascript:submitSave(document.form1, 1)'><img src='$jr_image_path/btn_temporary_save.gif' border='0'></a>";
				}
			} 
			else
			{
				# new version (multiple files supported)
				
				if($right_answer && $is_deadline_past){
					$file_submitted = $fm->manipulateFileMulti($filename, 4, $folder_path, 0, 1, $assignment_id, 2);
				}else{	
					$file_submitted = $fm->manipulateFileMulti($filename, 4, $folder_path);
				}		
				$folderID = $fm->ParentDirID;
		?>
				<script language="JavaScript">
				function submitSave(obj, myChoice)
				{
					if (obj.handin_id.value=="")
					{
						alert("<?=$ec_warning['assignment_file_missed']?>");
					}
					else
					{
						var conmsg = (myChoice==0) ? "<?=$ec_warning['submit_assignment']?>" : "<?=$ec_warning['assignment_handin_save']?>";
						if (!confirm(conmsg))
						{
						}
						else
						{
							obj.method = "post";
							obj.is_save.value = myChoice;
							obj.action = "handin_file_update.php";
							obj.submit();
						}
					}
				}
				function removeFile(fileID, filePos)
				{
					if (confirm("<?=$ec_warning['assignment_file_remove']?>"))
					{
						document.form1.method = "post";
						document.form1.filePos.value = filePos;
						document.form1.fileID.value = fileID;
						document.form1.action = "handin_file_remove_update.php";
						document.form1.submit();
						document.form1.method = "get";
					}
				}

				function editFile(fileID)
				{
					<?php
					if ($hw_answersheet == "===PVoice===" || $hw_answersheet == "===PPractice===")
					{
						if($hw_answersheet == "===PPractice===")
						$HasPractice = 1;
						else
						$HasPractice = 0;
					?>
						newWindow("http://<?=$eclass_httppath?>/src/tool/powervoice/index.php?HasPractice=<?=$HasPractice?>&assignment_id=<?=$assignment_id?>&group_id=0&fileName=<?=$pc_filename?>&fileID="+fileID+"&courseID=<?=$ck_course_id?>&categoryID=4&mode=s&autoSubmit=2&worktype=<?=$workType?>", 12,580,310);
					<?php
					}
					else 
					{
					?>					
						document.form1.fileID.value = fileID;
						document.form1.method = "post";
						postInstantForm(document.form1, 'handin_file_edit.php', '', 15, 'intranet_popup15');
						document.form1.method = "get";
					<?php
					}
					?>																									
				}

				function addFile()
				{
					document.form1.method = "post";
					postInstantForm(document.form1, 'attach.php', '', 14, 'intranet_popup14');
					document.form1.method = "get";
				}

				function reloadOpener()
				{
					return;
				}
				
				</script>

				<span class="guide"><?=$ec_words['assignment_upload_guide']?></span>
				<table align="center" border="1" bordercolor="#BBAABB" cellpadding="5" cellspacing="0" width="90%">
				<tr><td width="5%" valign="top" class="title">&nbsp;</td><td width="45%" valign="top" class="title"><?=$wording['filename']?></td><td valign="top" class="title" nowrap><?=$file_size?></td><td width="35%" valign="top" class="title"><?=$file_lastmodi?></td><td width="5%" valign="top">&nbsp;</td></tr>
				<!--#adam: can play model answer once deadline was past-->
				<?=$file_submitted?>
				</table>

				<table align="center" border="0" cellpadding="5" cellspacing="0" width="90%">
				<!--<tr><td>&nbsp; &nbsp; &nbsp; <input type="button" class="button" value="&nbsp;<?=$button_add?>&nbsp;" onClick="addFile()"></td></tr>-->
				<tr><td>&nbsp;&nbsp; <a href='javascript:addFile()'><img src='<?=$jr_image_path?>/btn_add.gif' border='0'></a></td></tr>
				</table>
								
			<?php
				if ($ck_memberType=="T")
				{
					$buttons = "<a href='javascript:window.close()'><img src='$jr_image_path/btn_upload.gif' border='0'></a>";
				} 
				else
				{
					$buttons = "<a href='javascript:submitSave(document.form1, 0)'><img src='$jr_image_path/btn_submit.gif' border='0'></a>&nbsp;<a href='javascript:submitSave(document.form1, 1)'><img src='$jr_image_path/btn_temporary_save.gif' border='0'></a>";
				}
			}
			}
		}
		else
		{
			// KELLY add a checking, if the grade_public value not equal to 1, do not show the grade
			$grade = ($public_view==1 && $grade_public!=1) ? "" : $grade;

			echo "<br><table style=\"border:dotted #FF77FF\" cellpadding=4>".$myStatus."<tr><td> ".$msg."</td></tr></table>";
			//<br><span class='numF'>".$grade."</span>".$msg;

			if ($singleFileVersion)
			{
				# Detect if filename includes filepath
				if(strpos($filename, "/") === false){
					if($hw_answer!="" && $hw_answer!="--"){
						echo "<br><table border=1 cellspacing=0 cellpadding=5><tr><td>".$Assignments_submitted.": ".$fm->manipulateFile($folder_path.$filename,4,"",1,1,"",$assignment_id,2)."</td></tr></table><br>";
					}else{
						echo "<br><table border=1 cellspacing=0 cellpadding=5><tr><td>".$Assignments_submitted.": ".$fm->manipulateFile($folder_path.$filename,4)."</td></tr></table><br>";
					}		
				}else{
					if($hw_answer!="" && $hw_answer!="--"){
						echo "<br><table border=1 cellspacing=0 cellpadding=5><tr><td>".$Assignments_submitted.": ".$fm->manipulateFile($filename,4,"",1,1,"",$assignment_id,2)."</td></tr></table><br>";
					}else{
						echo "<br><table border=1 cellspacing=0 cellpadding=5><tr><td>".$Assignments_submitted.": ".$fm->manipulateFile($filename,4)."</td></tr></table><br>";
					}	
				}	
			} 
			else
			{
				if($hw_answer!="" && $hw_answer!="--"){
					$file_submitted = $fm->manipulateFileMulti($filename, 4, $folder_path, 0, 1, $assignment_id, 2);
				}else{	
					$file_submitted = $fm->manipulateFileMulti($filename, 4, $folder_path, 1);
				}	
					
	?>		
				<br />
				<span class="guide"><?=$Assignments_submitted?>:</span>
				<table align="center" border="1" bordercolor="#BBAABB" cellpadding="5" cellspacing="0" width="90%">
				<tr class="bodycolor3"><td width="5%" valign="top">&nbsp;</td><td width="45%" valign="top" class="title"><?=$wording['filename']?></td><td valign="top" class="title" nowrap><?=$file_size?></td><td width="35%" valign="top" class="title"><?=$file_lastmodi?></td></tr>
				<?=$file_submitted?>
				</table>
				<br />
	<?php
			}
		}
	}
}
?>

<input type="hidden" name="handin_id" value="<?= $handin_id ?>">
<input type="hidden" name="is_save">
<input type="hidden" name="filePos">
<input type="hidden" name="fileID">
<input type="hidden" name="categoryID" value="4">
<input type="hidden" name="folderID" value="<?= $folderID ?>">
<input type="hidden" name="from_view" value=1>

<input type="hidden" name="worktype" value="<?=$worktype?>" >
<input type="hidden" name="subject_id" value="<?=$subject_id?>" >
<input type=hidden name=assignment_id value="<?=$assignment_id?>">
<input type=hidden name=ASQ1 value="<?=$hw_answersheet?>">
<input type=hidden name=ASA1 value="<?=$hw_modelanswer?>">
<input type=hidden name=attachment>
<input type=hidden name=fieldname>

<img src="<?=$jr_image_path?>/class_hwlist/popup_gradient_up.gif" width="660" height="13"></td>
    </tr>
    <tr>
      <td align="center">
		<table width="638" border="0" cellpadding="0" cellspacing="0" class="control">
          <tr>
            <td width="13"><img src="<?=$jr_image_path?>/class_hwlist/fuc_topleft.gif" width="13" height="9"></td>
            <td width="603" background="<?=$jr_image_path?>/class_hwlist/fuc_cell_t.gif"><img src="<?= $jr_image_path ?>/spacer.gif" width="10" height="9"></td>
            <td width="22"><p><img src="<?=$jr_image_path?>/class_hwlist/fuc_topright.gif" width="22" height="9"></p></td>
          </tr>
          <tr>
            <td background="<?=$jr_image_path?>/class_directory/fuc_cell_l.gif">&nbsp;</td>
            <td align="right" bgcolor="#F7F7F7"><?=($buttons==""?$button_used:$buttons)?>
			  <a href='<?=$url_cancel?>'><img src="<?=$jr_image_path?>/btn_back.gif" border='0'></a>
			  </td>
            <td align="right" valign="bottom" background="<?=$jr_image_path?>/class_hwlist/fuc_cell_r.gif"><img src="<?=$jr_image_path?>/class_hwlist/fuc_corner.gif" width="22" height="9"></td>
          </tr>
          <tr>
            <td><img src="<?=$jr_image_path?>/class_hwlist/fuc_btleft.gif" width="13" height="4"></td>
            <td background="<?=$jr_image_path?>/class_hwlist/fuc_cell_b.gif"><img src="<?= $jr_image_path ?>/spacer.gif" width="10" height="4"></td>
            <td><img src="<?=$jr_image_path?>/class_directory/fuc_btright.gif" width="22" height="4"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</form>


<?php
include("../../../templates/filefooter.php");
intranet_closedb();
?>
