<?php
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");

intranet_auth();
intranet_opendb();
########################################################################################################

## Get Data
$ajax_action = (isset($ajax_action) && $ajax_action != '') ? $ajax_action : '';

## Use Library
$db =new libdb();
$lu = new libuser($UserID);
$lportal = new libportal();


## Init
$content = '';


## Main
if($ajax_action == 'get_active_lesson_list'){
	$allUserCourse = $lportal->returnEClassUserIDCourseIDStandard($lu->UserEmail);
	$cntLesson = 0;
	if (!empty($allUserCourse)){
		foreach($allUserCourse as $course){
			$lsession = $lportal->get_lesson_session($course[0]);
			$cntLesson += count($lsession);
			if (!empty($lsession)){
				foreach($lsession as $lesson){
					$content .= '
						<a class="indextabclasslist" href="javascript:Lesson.login_lesson(\''.$course['user_course_id'].'\',\''.$lesson[0].'\')">
							<div class="btnEnter_top">
								'.$course['course_code'].' - '.$course['course_name'].'<br>
								<span>('.$lesson['username'].')</span>
							</div>
							<div class="btnEnter_bottom"></div>
						</a>';
				}
			}
		}
	}
	if($cntLesson == 0){
		$content .= 'No records at this moment.';
	}
}

########################################################################################################
intranet_closedb();

echo $content;
?>