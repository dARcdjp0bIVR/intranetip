<?php
// page modifing by: Michael Cheung
$PathRelative = "../../../";
$CurSubFunction = "Communication";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}
AuthPage(array("Communication-Mail-GeneralUsage"));
if (isset($_REQUEST["SchoolCode"]))
{
	include($PathRelative."config/" . $_REQUEST["SchoolCode"] . "/system_settings.php");
}
include_once($PathRelative."src/include/template/popup_header.php");
include_once($PathRelative."src/include/class/imap_ui.php");
include_once($PathRelative."src/include/class/imap_address_ui.php");
include_once($PathRelative."src/include/class/database.php");

// create UI instance
$lui = new imap_ui();
$LibAddress = new imap_address_ui();

// db instance
$LibDB = new database();

// name of selection menu
$InputName = 'UserIDs[]';
?>
<script language="javascript">
var UserArr = new Array();

function jRollBackStep(val)
{
	document.forma.RollBackStep.value = val;
	document.forma.submit();
}

function checkOption(obj){
    for(i=0; i<obj.length; i++){
        if(obj.options[i].value== ''){
            obj.options[i] = null;
        }
    }
}

function AddRecords(TmpStr)
{
	var FormObj = window.opener.document.EditUserForm;
	FormObj.action = "imail_preference_mail_delegation.php";

	TmpStr += ',' + FormObj.HiddenUserID.value;
	FormObj.HiddenUserID.value = TmpStr;
	
	FormObj.submit();
}

function jAdd_ACTION(jParAct, jParSrc)
{
	document.forma.specialAction.value = jParAct;
	document.forma.actionLevel.value = jParSrc;
	document.forma.submit();	
}

</script>


<?php

$LoadJavascript = "";

////////////////////////////////////////////////////
//	Step1: Fixed
///////////////////////////////////////////////////
$Step1Arr = array();
$Step1Arr[] = array($Lang['email']['TeachingStaff'],0);
$Step1Arr[] = array($Lang['email']['NonTeachingStaff'],1);
//$Step1Arr[] = array($Lang['email']['Subjects'],2);
//$Step1Arr[] = array($Lang['email']['Sections'],3);
//$Step1Arr[] = array($Lang['email']['Houses'],4);
//$Step1Arr[] = array($Lang['email']['SchoolBasedGroup'],5);

$Step1Select = $lui->Get_Input_Select("TeachType","TeachType",$Step1Arr,$TeachType,"size=\"10\" style=\"width: 250px\"  ");

$ActionStep1 = "onclick=\"this.form.action='mail_delegation_select_member.php#step2';".$GroupSubAction."jAdd_ACTION('add','TeachType')\"";
///////////////////////////////////////////////////

$IsEnding = false;
$IsEnding2 = false;
$IsEnding3 = false;
$IsEnding4 = false;
$IsEnding5 = false;

if ($TeachType == "1")
{
// Non-teaching staff	

	$Sql =  	"
					exec Search_StaffEmail 
					@Staff_Type='N,', 
					@Roll_Group=',',					
					@School_Year =',',
					@House=',' ,
					@SubjectName=',' , 
					@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
				";
	$ReturnArr = $LibDB->returnArray($Sql);
	
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{
		if (count($UserIDs) > 0)
		{
			$LoadJavascript .= "<script language='javascript' > ";
		}
	}	
	$OutputArr = array();	
	$FirstFlag = true;
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);		

		if (is_array($UserIDs) && in_array($ReturnArr[$i]["UserID"], $UserIDs))
		{
			if ($FirstFlag) {
				$TmpStr = $ReturnArr[$i]["UserID"];
				$FirstFlag = false;
			}
			else 
				$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
		}
	}	
	
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{		
		$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
	}
	
	$ActionStep2 = "onclick=\"this.form.action='mail_delegation_select_member.php#step2';".$UserSubAction."jAdd_ACTION('add','step1_2')\"";
	
	$Step2Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
	$IsEnding2 = true;
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{
	if (count($UserIDs) > 0)
	{
		$LoadJavascript .= "</script> ";
	}
	}	
}
else if ($TeachType == "0")
{
// Teaching staff
	///////////////////////////////////////////////////
	//	Step2: Fixed
	///////////////////////////////////////////////////
	$Step2Arr = array();
	$Step2Arr[] = array($Lang['email']['Individual'],0);
	$Step2Arr[] = array($Lang['email']['Subjects'],1);
	$Step2Arr[] = array($Lang['email']['Years'],2);
	//$Step2Arr[] = array($Lang['email']['Sections'],3);
	//$Step2Arr[] = array($Lang['email']['Houses'],4);
	$Step2Select = $lui->Get_Input_Select("TeachType2","TeachType2",$Step2Arr,$TeachType2,"size=\"10\" style=\"width: 250px\"");	
	
	$ActionStep2 = "onclick=\"this.form.action='mail_delegation_select_member.php#step3';".$GroupSubAction."jAdd_ACTION('add','TeachType')\"";
	///////////////////////////////////////////////////

	////////////////////////////////////////////////////
	//	Step3: From DB
	///////////////////////////////////////////////////
	if ($TeachType2 == "0")
	{
		// individual
			$Sql =  	"
					exec Search_StaffEmail 
					@Staff_Type='T,', 
					@Roll_Group=',',					
					@School_Year =',',
					@House=',' ,
					@SubjectName=',' , 
					@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
				";
	$ReturnArr = $LibDB->returnArray($Sql);
	
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{
	if (count($UserIDs) > 0)
	{
		$LoadJavascript .= "<script language='javascript' > ";
	}
	}	
	$OutputArr = array();	
	$FirstFlag = true;
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);		

		if (is_array($UserIDs) && in_array($ReturnArr[$i]["UserID"], $UserIDs))
		{
			if ($FirstFlag) {
				$TmpStr = $ReturnArr[$i]["UserID"];
				$FirstFlag = false;
			}
			else 
				$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
		}
	}	
	
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{		
		$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
	}
	$ActionStep3 = "onclick=\"this.form.action='mail_delegation_select_member.php#step3';".$UserSubAction."jAdd_ACTION('add','step1_2')\"";

	$Step3Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 200px\" ");
	$IsEnding3 = true;
		if (($specialAction == "add") && ($actionLevel == "step1_2"))
		{
			if (count($UserIDs) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
		}
	} // end if individual
	else if ($TeachType2 == "1")
	{
	// Subjects		

		//$Sql =  "exec Get_SubjectList_ByTTPeriod @TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."' ";
		$Sql =  "exec Get_SubjectNameList_ByTTPeriod @TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."' ";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$OutputArr = array();	
		$FirstFlag = true;
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["FullName"], $ReturnArr[$i]["Subject_Code"]);
		}			
		$Step3Select = $lui->Get_Input_Select("TeachSubject","TeachSubject",$OutputArr,$TeachSubject,"size=\"10\" style=\"width: 250px\" ");
		
		$ActionStep3 = "onclick=\"this.form.action='mail_delegation_select_member.php#step4';".$GroupSubAction."jAdd_ACTION('add','TeachSubject')\"";
		
		////////////////////////////////////////////////////
		//	Step4: From DB
		///////////////////////////////////////////////////		
		
		if (($specialAction == "add") && ($actionLevel == "step2_4"))
		{
		if (count($UserIDs) > 0)
		{
			$LoadJavascript .= "<script language='javascript' > ";
		}
		}	
		
		if ($TeachSubject != "")
		{
			$Sql =  	"
							exec Search_StaffEmail 
							@Staff_Type=',', 
							@Roll_Group=',',					
							@School_Year =',',
							@House=',' ,
							@SubjectName='".$TeachSubject.",' , 
							@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
						";
			$ReturnArr = $LibDB->returnArray($Sql);
			$OutputArr = array();	
			$FirstFlag = true;
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);		

				if (is_array($UserIDs) && in_array($ReturnArr[$i]["UserID"], $UserIDs))
				{
					if ($FirstFlag) {
						$TmpStr = $ReturnArr[$i]["UserID"];
						$FirstFlag = false;
					}
					else 
						$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
				}
			}
			
			if (($specialAction == "add") && ($actionLevel == "step2_4"))
			{
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
			}
			$Step4Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
			$IsEnding4 = true;
		}
		$ActionStep4 = "onclick=\"this.form.action='mail_delegation_select_member.php#step5';".$UserSubAction."jAdd_ACTION('add','step2_4')\"";		
		
		if (($specialAction == "add") && ($actionLevel == "step2_4"))
		{
		if (count($UserIDs) > 0)
		{
			$LoadJavascript .= "</script> ";
		}
		}	
						
		///////////////////////////////////////////////////
	}
	else if ($TeachType2 == "2")
	{
		// Years
		
		$Sql =  "exec Get_SchoolYearList ";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$OutputArr = array();	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);		
		}
		
		$Step3Select = $lui->Get_Input_Select("SchoolYear","SchoolYear",$OutputArr,$SchoolYear,"size=\"10\" style=\"width: 250px\" ");
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['SchoolYear'], 1);" : "";
		$ActionStep3 = "onclick=\"this.form.action='mail_delegation_select_member.php#step4';".$GroupSubAction."jAdd_ACTION('add','SchoolYear')\"";
	
		////////////////////////////////////////////////////
		//	Step4: From DB
		///////////////////////////////////////////////////	
	
		if($SchoolYear != "")
		{
		$Step4Arr = array();
		$Step4Arr[] = array($Lang['email']['All_Teacher'],0);
		$Step4Arr[] = array($Lang['email']['Roll_Group_Teacher'],1);
		
		$Step4Select = $lui->Get_Input_Select("Year_Teacher_Type","Year_Teacher_Type",$Step4Arr,$Year_Teacher_Type,"size=\"10\" style=\"width: 250px\"");	
		$ActionStep4 = "onclick=\"this.form.action='mail_delegation_select_member.php#step5';".$GroupSubAction."jAdd_ACTION('add','Year_Teacher_Type')\"";
		}
		
		////////////////////////////////////////////////////
		//	Step5: From DB
		///////////////////////////////////////////////////	
		if($Year_Teacher_Type == "0")
		{
			//echo $SchoolYear;
		if ($SchoolYear != "")
		{
							$Sql = "
								exec Search_StaffEmail 
								@Staff_Type=',', 
								@Roll_Group=',',					
								@School_Year ='".$SchoolYear.",',
								@House=',' ,
								@SubjectName=',' , 
								@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
							";
				$ReturnArr = $LibDB->returnArray($Sql);
				
			if (($specialAction == "add") && ($actionLevel == "step3_5"))
			{
			if (count($UserIDs) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
			}	
				
				$OutputArr = array();					
				$FirstFlag = true;
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);		

					if (is_array($UserIDs) && in_array($ReturnArr[$i]["UserID"], $UserIDs))
					{
						if ($FirstFlag) {
							$TmpStr = $ReturnArr[$i]["UserID"];
							$FirstFlag = false;
						}
						else 
							$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
					}
				}	// end for
				
				if (($specialAction == "add") && ($actionLevel == "step3_5"))
				{
					$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";
				}
				$Step5Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$IsEnding5 = true;
				
				$ActionStep5 = "onclick=\"this.form.action='mail_delegation_select_member.php#step5';".$UserSubAction."jAdd_ACTION('add','step3_5')\"";	
			
			if (($specialAction == "add") && ($actionLevel == "step3_5"))
			{
			if (count($UserIDs) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
			}	
			} // end if check schoolList
		} // end if $Year_Teacher_Type == 0
		else if($Year_Teacher_Type == 1)
		{
			//echo $SchoolYear;
		if ($SchoolYear != "")
		{
							$Sql = "
								exec Search_StaffEmail 
								@Staff_Type=',', 
								@Roll_Group=',',
								@School_Year =',',
								@House=',' ,
								@SubjectName=',' , 
								@Teach_Roll_Group=',' , 
								@Roll_Group_Year= '".$SchoolYear.",',
								@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
							";
				$ReturnArr = $LibDB->returnArray($Sql);
				
			if (($specialAction == "add") && ($actionLevel == "step3_5"))
			{
			if (count($UserIDs) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
			}	
				
				$OutputArr = array();					
				$FirstFlag = true;
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);		

					if (is_array($UserIDs) && in_array($ReturnArr[$i]["UserID"], $UserIDs))
					{
						if ($FirstFlag) {
							$TmpStr = $ReturnArr[$i]["UserID"];
							$FirstFlag = false;
						}
						else 
							$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
					}
				}	
				
				if (($specialAction == "add") && ($actionLevel == "step3_5"))
				{
					$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
				}
				$Step5Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$IsEnding5 = true;
				
				$ActionStep5 = "onclick=\"this.form.action='mail_delegation_select_member.php#step5';".$UserSubAction."jAdd_ACTION('add','step3_5')\"";	
			
			if (($specialAction == "add") && ($actionLevel == "step3_5"))
			{
			if (count($UserIDs) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
			}	
			} // end if check schoolList
		} // end if $Year_Teacher_Type == 1
		///////////////////////////////////////////////////
	}
	else if ($TeachType2 == "3")
	{
	// Sections	
	}
	else if ($TeachType2 == "4")
	{
	// House
		$Sql =  "exec Get_HouseList";
		$ReturnArr = $LibDB->returnArray($Sql);
				
		$Step3Select = $lui->Get_Input_Select("House","House",$ReturnArr,$House,"size=\"10\" style=\"width: 250px\" ");		
		$ActionStep3 = "onclick=\"this.form.action='mail_delegation_select_member.php#step4';jAdd_ACTION('add','House')\"";
		
		////////////////////////////////////////////////////
		//	Step4: From DB
		///////////////////////////////////////////////////		
		if ($House != "")
		{			
			////////////////////////////////////////////////////
			//	Step5: From DB
			///////////////////////////////////////////////////		
			if (($specialAction == "add") && ($actionLevel == "step4_4"))
			{
			if (count($UserIDs) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
			}	
			
			//if ($School_Year_House != "")
			//{
				$Sql =  	"
								exec Search_StaffEmail 
								@Staff_Type=',', 
								@Roll_Group=',',					
								@School_Year =',',
								@House='".$House.",' ,
								@SubjectName=',' , 
								@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
							";
//echo $Sql;
				$ReturnArr = $LibDB->returnArray($Sql);
				$OutputArr = array();	
				$FirstFlag = true;
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);		
			
					if (is_array($UserIDs) && in_array($ReturnArr[$i]["UserID"], $UserIDs))
					{
						if ($FirstFlag) {
							$TmpStr = $ReturnArr[$i]["UserID"];
							$FirstFlag = false;
						}
						else 
							$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
					}
				}	
				
				if (($specialAction == "add") && ($actionLevel == "step4_4"))
				{					
					$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
				}
				$Step4Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$IsEnding4 = true;
				
				$ActionStep4 = "onclick=\"this.form.action='mail_delegation_select_member.php#step5';".$UserSubAction."jAdd_ACTION('add','step4_4')\"";
				
			//}		
			
			if (($specialAction == "add") && ($actionLevel == "step4_4"))
			{
			if (count($UserIDs) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
			}				
			///////////////////////////////////////////////////
			
		}		
		///////////////////////////////////////////////////

		
	}
	///////////////////////////////////////////////////		
		
}
else if ($TeachType == "2")
{
	// Subjects		
		$Sql =  "exec Get_SubjectNameList_ByTTPeriod @TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."' ";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$OutputArr = array();	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["FullName"], $ReturnArr[$i]["Subject_Code"]);
		}			
		$Step2Select = $lui->Get_Input_Select("TeachSubject","TeachSubject",$OutputArr,$TeachSubject,"size=\"10\" style=\"width: 250px\" ");
		
		$GroupSubAction = ($isiCal == 1) ? "AddRecords(this.form.elements['TeachSubject'], 1);" : "";
		$ActionStep2 = "onclick=\"this.form.action='mail_delegation_select_member.php#step3';".$GroupSubAction."jAdd_ACTION('add','TeachSubject')\"";
		
		////////////////////////////////////////////////////
		//	Step3: From DB
		///////////////////////////////////////////////////		
		
		if (($specialAction == "add") && ($actionLevel == "step2_2_4"))
		{
		if (count($UserIDs) > 0)
		{
			$LoadJavascript .= "<script language='javascript' > ";
		}
		}	
		
		if ($TeachSubject != "")
		{
			$Sql =  	"
							exec Search_StaffEmail 
							@Staff_Type=',', 
							@Roll_Group=',',					
							@School_Year =',',
							@House=',' ,
							@SubjectName='".$TeachSubject.",' , 
							@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
						";
						
			$ReturnArr = $LibDB->returnArray($Sql);
			$OutputArr = array();	
			$FirstFlag = true;
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);		

				if (is_array($UserIDs) && in_array($ReturnArr[$i]["UserID"], $UserIDs))
				{
					if ($FirstFlag) {
						$TmpStr = $ReturnArr[$i]["UserID"];
						$FirstFlag = false;
					}
					else 
						$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
				}
			}	
			
			if (($specialAction == "add") && ($actionLevel == "step2_2_4"))
			{
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
			}
			$Step3Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
			$IsEnding3 = true;
		}
		$ActionStep3 = "onclick=\"this.form.action='mail_delegation_select_member.php#step5';".$UserSubAction."jAdd_ACTION('add','step2_2_4')\"";		
		
		if (($specialAction == "add") && ($actionLevel == "step2_2_4"))
		{
		if (count($UserIDs) > 0)
		{
			$LoadJavascript .= "</script> ";
		}
		}	
						
		///////////////////////////////////////////////////	
}
else if ($TeachType == "3")
{
	// Sections	
		$Sql =  "exec Get_SchoolYearList ";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$OutputArr = array();	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);		
		}	
		
		$Step2Select = $lui->Get_Input_Select("SchoolYear","SchoolYear",$OutputArr,$SchoolYear,"size=\"10\" style=\"width: 250px\" ");
		
		$ActionStep2 = "onclick=\"this.form.action='mail_delegation_select_member.php#step3';".$GroupSubAction."jAdd_ACTION('add','SchoolYear')\"";
	
		////////////////////////////////////////////////////
		//	Step3: From DB
		///////////////////////////////////////////////////		
		if ($SchoolYear != "")
		{
			$Sql =  "exec Get_RollGroupList_BySchoolYear @School_Year = '".$SchoolYear."' ";
			$ReturnArr = $LibDB->returnArray($Sql);
						
			$OutputArr = array();	
			$IsFound = false;
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				$OutputArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);		
				if ($Roll_Group == $ReturnArr[$i]["Roll_Group"])
				{
					$IsFound = true;
				}
			}	
			if (!$IsFound)
			{
				$Roll_Group  = "";
			}			
						
			$Step3Select = $lui->Get_Input_Select("Roll_Group","Roll_Group",$OutputArr,$Roll_Group,"size=\"10\" style=\"width: 250px\" ");

			$ActionStep3 = "onclick=\"this.form.action='mail_delegation_select_member.php#step4';".$GroupSubAction."jAdd_ACTION('add','Roll_Group')\"";
			
			////////////////////////////////////////////////////
			//	Step5: From DB
			///////////////////////////////////////////////////		
			if (($specialAction == "add") && ($actionLevel == "step3_3_5"))
			{
			if (count($UserIDs) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
			}	
			
			if ($Roll_Group != "")
			{
				$Sql =  	"
								exec Search_StaffEmail 
								@Staff_Type=',', 
								@Roll_Group='".$Roll_Group.",',					
								@School_Year =',',
								@House=',' ,
								@SubjectName=',' , 
								@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
							";
				$ReturnArr = $LibDB->returnArray($Sql);
				$OutputArr = array();					
				$FirstFlag = true;
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);		
					
					if (is_array($UserIDs) && in_array($ReturnArr[$i]["UserID"], $UserIDs))
					{
						if ($FirstFlag) {
							$TmpStr = $ReturnArr[$i]["UserID"];
							$FirstFlag = false;
						}
						else 
							$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
					}
				}	
				
				if (($specialAction == "add") && ($actionLevel == "step3_3_5"))
				{
					$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
				}
				$Step4Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$IsEnding4 = true;
				
				$ActionStep4 = "onclick=\"this.form.action='mail_delegation_select_member.php#step4';".$UserSubAction."jAdd_ACTION('add','step3_3_5')\"";
			}		
			
			if (($specialAction == "add") && ($actionLevel == "step3_3_5"))
			{
			if (count($UserIDs) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
			}	
						
			///////////////////////////////////////////////////
			
		}		
		///////////////////////////////////////////////////
	
}
else if ($TeachType == "4")
{
	// Sections	
		$Sql =  "exec Get_HouseList";
		$ReturnArr = $LibDB->returnArray($Sql);
				
		$Step2Select = $lui->Get_Input_Select("House","House",$ReturnArr,$House,"size=\"10\" style=\"width: 250px\" ");	
		
		$ActionStep2 = "onclick=\"this.form.action='mail_delegation_select_member.php#step2';".$GroupSubAction."jAdd_ACTION('add','House')\"";
		
		////////////////////////////////////////////////////
		//	Step3: From DB
		///////////////////////////////////////////////////		
		if ($House != "")
		{
			$Sql =  "exec Get_SchoolYearList_ByHouse @House = '".$House."' ";
			$ReturnArr = $LibDB->returnArray($Sql);
						
			$OutputArr = array();	
			$IsFound = false;
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				$OutputArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);		
				if ($School_Year_House == $ReturnArr[$i]["School_Year"])
				{
					$IsFound = true;
				}
			}	
			if (!$IsFound)
			{
				$School_Year_House  = "";
			}			
						
			$Step3Select = $lui->Get_Input_Select("School_Year_House","School_Year_House",$OutputArr,$School_Year_House,"size=\"10\" style=\"width: 250px\" ");
			
			$ActionStep3 = "onclick=\"this.form.action='mail_delegation_select_member.php#step3';".$GroupSubAction."jAdd_ACTION('add','School_Year_House')\"";
			
			////////////////////////////////////////////////////
			//	Step5: From DB
			///////////////////////////////////////////////////		
			if (($specialAction == "add") && ($actionLevel == "step4_4_4"))
			{
			if (count($UserIDs) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
			}	
			
			if ($School_Year_House != "")
			{
				$Sql =  	"
								exec Search_StaffEmail 
								@Staff_Type='T,', 
								@Roll_Group=',',					
								@School_Year ='".$School_Year_House.",',
								@House=',' ,
								@SubjectName=',' , 
								@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
							";
				$ReturnArr = $LibDB->returnArray($Sql);
				$OutputArr = array();	
				$FirstFlag = true;
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);		

					if (is_array($UserIDs) && in_array($ReturnArr[$i]["UserID"], $UserIDs))
					{
						if ($FirstFlag) {
							$TmpStr = $ReturnArr[$i]["UserID"];
							$FirstFlag = false;
						}
						else 
							$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
					}
				}	
				
				if (($specialAction == "add") && ($actionLevel == "step4_4_4"))
				{					
					$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
				}
				$Step4Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
				$IsEnding4 = true;
				
				$ActionStep4 = "onclick=\"this.form.action='mail_delegation_select_member.php#step3';".$UserSubAction."jAdd_ACTION('add','step4_4_4')\"";
				
			}		
			
			if (($specialAction == "add") && ($actionLevel == "step4_4_4"))
			{
			if (count($UserIDs) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
			}				
			///////////////////////////////////////////////////
			
		}		
		///////////////////////////////////////////////////	
	
}
else if ($TeachType == "5")
{
//School groups

	// Subjects		

		//$Sql =  "exec Get_SubjectList_ByTTPeriod @TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."' ";
		$Sql =  " SELECT GroupName, MailGroupID FROM MAIL_ADDRESS_GROUP WHERE GroupType='S' ";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$OutputArr = array();	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["GroupName"], $ReturnArr[$i]["MailGroupID"]);		
		}
		$Step2Select = $lui->Get_Input_Select("SchoolGroup","SchoolGroup",$OutputArr,$SchoolGroup,"size=\"10\" style=\"width: 250px\" ");
		
		$ActionStep2 = "onclick=\"this.form.action='mail_delegation_select_member.php#step3';".$GroupSubAction."jAdd_ACTION('add','SchoolGroup')\"";
		
		////////////////////////////////////////////////////
		//	Step3: From DB
		///////////////////////////////////////////////////		
		
		if (($specialAction == "add") && ($actionLevel == "step5_2_4"))
		{
		if (count($UserIDs) > 0)
		{
			$LoadJavascript .= "<script language='javascript' > ";
		}
		}	
		
		if ($SchoolGroup != "")
		{
			$Sql =  	"
							SELECT
								b.UserName, b.UserEmail, b.MailUserID AS UserID 
							FROM
								MAIL_ADDRESS_MAPPING a,
								MAIL_ADDRESS_USER b
							WHERE
								a.MailUserID = b.MailUserID
								AND a.MailGroupID = '".$SchoolGroup."'	 
						";
			$ReturnArr = $LibDB->returnArray($Sql);
			$OutputArr = array();	
			$FirstFlag = true;
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);		

				if (is_array($UserIDs) && in_array($ReturnArr[$i]["UserID"], $UserIDs))
				{
					if ($FirstFlag) {
						$TmpStr = $ReturnArr[$i]["UserID"];
						$FirstFlag = false;
					}
					else 
						$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
				}
			}	
			
			if (($specialAction == "add") && ($actionLevel == "step5_2_4"))
			{
				$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
			}
			$Step3Select = $lui->Get_Input_Select_multi($InputName,$InputName,$OutputArr,"","size=\"10\" style=\"width: 250px\" ");
			$IsEnding3 = true;
		}
		$ActionStep3 = "onclick=\"this.form.action='mail_delegation_select_member.php#step5';".$UserSubAction."jAdd_ACTION('add','step5_2_4')\"";		
		
		if (($specialAction == "add") && ($actionLevel == "step5_2_4"))
		{
		if (count($UserIDs) > 0)
		{
			$LoadJavascript .= "</script> ";
		}
		}	
						
		///////////////////////////////////////////////////	
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Adding groups
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
if ($specialAction == "add")
{
	if ($actionLevel == "TeachType")
	{		
		if($TeachType == 5)
		{
			$Sql = "SELECT
						UserName AS OfficialFullName, UserEmail AS Email, 
						MailUserID AS UserID 
					FROM
						MAIL_ADDRESS_USER b
					WHERE
						UserType = 'S'";
		} 
		else 
		{
			if ($TeachType == "1")
			{
				$Staff_Type = "N";
			}
			else
			{
				$Staff_Type = "T";
			}
			$Sql =  	"
							exec Search_StaffEmail 
							@Staff_Type='".$Staff_Type.",', 
							@Roll_Group=',',					
							@School_Year =',',
							@House=',' ,
							@SubjectName=',' , 
							@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
						";
		}
		$ReturnArr = $LibDB->returnArray($Sql);
		$LoadJavascript .= "<script language='javascript' > ";	
		$FirstFlag = true;	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			if ($FirstFlag) {
				$TmpStr = $ReturnArr[$i]["UserID"];
				$FirstFlag = false;
			}
			else 
				$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
		}
		
		$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "TeachSubject")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group=',',					
						@School_Year =',',
						@House=',' ,
						@SubjectName='".$TeachSubject.",' , 
						@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";		
		$FirstFlag = true;	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			if ($FirstFlag) {
				$TmpStr = $ReturnArr[$i]["UserID"];
				$FirstFlag = false;
			}
			else 
				$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
		}
		
		$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "SchoolYear")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group=',',					
						@School_Year ='".$SchoolYear.",',
						@House=',' ,
						@SubjectName=',' , 
						@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";		
		$FirstFlag = true;	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			if ($FirstFlag) {
				$TmpStr = $ReturnArr[$i]["UserID"];
				$FirstFlag = false;
			}
			else 
				$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
		}
		
		$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "Roll_Group")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group='".$Roll_Group.",',					
						@School_Year =',',
						@House=',' ,
						@SubjectName=',' , 
						@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";		
		$FirstFlag = true;	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			if ($FirstFlag) {
				$TmpStr = $ReturnArr[$i]["UserID"];
				$FirstFlag = false;
			}
			else 
				$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
		}
		
		$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "House")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group=',',					
						@School_Year =',',
						@House='".$House.",' ,
						@SubjectName=',' , 
						@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";		
		$FirstFlag = true;	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			if ($FirstFlag) {
				$TmpStr = $ReturnArr[$i]["UserID"];
				$FirstFlag = false;
			}
			else 
				$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
		}
		
		$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "School_Year_House")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group=',',					
						@School_Year ='".$School_Year_House.",',
						@House=',' ,
						@SubjectName=',' , 
						@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";		
		$FirstFlag = true;	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			if ($FirstFlag) {
				$TmpStr = $ReturnArr[$i]["UserID"];
				$FirstFlag = false;
			}
			else 
				$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
		}
		
		$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		$LoadJavascript .= "</script> ";
	}	
	else if($actionLevel == "Year_Teacher_Type" && $SchoolYear != "")
	{
		if($Year_Teacher_Type == "0")
		{
			$Sql = "
				exec Search_StaffEmail 
				@Staff_Type=',', 
				@Roll_Group=',',					
				@School_Year ='".$SchoolYear.",',
				@House=',' ,
				@SubjectName=',' , 
				@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
				";
		}
		else if($Year_Teacher_Type == "1")
		{
			$Sql = "
				exec Search_StaffEmail 
				@Staff_Type=',', 
				@Roll_Group=',',
				@School_Year =',',
				@House=',' ,
				@SubjectName=',' , 
				@Teach_Roll_Group=',' , 
				@Roll_Group_Year= '".$SchoolYear.",',
				@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'
				";
		}
				
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";
		$FirstFlag = true;	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			if ($FirstFlag) {
				$TmpStr = $ReturnArr[$i]["UserID"];
				$FirstFlag = false;
			}
			else 
				$TmpStr .= ','.$ReturnArr[$i]["UserID"];	
		}
		
		$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		$LoadJavascript .= "</script> ";
	} // end if Year_Teacher_Type
} // end if action add

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo $LoadJavascript;
echo $lui->Get_Div_Open("module_bulletin"," class=\"module_content\" " );
echo $lui->Get_Div_Open(""," style=\"width:99%\" " );

echo $lui->Get_Div_Open("module_content_header");				
echo $lui->Get_Div_Open("module_content_header_title");				
echo $lui->Get_HyperLink_Open("#");
echo $Lang['SysMgr']['HeaderTitle'];
echo $lui->Get_HyperLink_Close();
echo $lui->Get_Div_Close();
echo "<br />";
echo $lui->Get_Div_Close();

echo "<br style=\"clear:none\">";


////////////// DEBUG ///////////////////
if(Auth(array("MySchool-TeachingStaff"), "TARGET") || Auth(array("MySchool-NonTeachingStaff"), "TARGET"))
{
//////////// END DEBUG ///////////////////

//echo $lui->Get_Div_Open("commontabs");
echo $lui->Get_Div_Open();
//$SchoolLink .=  $lui->Get_HyperLink_Open("imail_address_book.php?ChooseType=1");
$SchoolLink .= $lui->Get_HyperLink_Open("#");
$SchoolLink .= $lui->Get_Span_Open();
$SchoolLink .= $Lang['email']['SelectRecipient'];
$SchoolLink .= $lui->Get_Span_Close();
$SchoolLink .= $lui->Get_HyperLink_Close();
/*
$PersonalLink .= $lui->Get_HyperLink_Open("imail_address_book.php?ChooseType=2");
$PersonalLink .= $lui->Get_Span_Open();
$PersonalLink .= $Lang['email']['Personal'];
$PersonalLink .= $lui->Get_Span_Close();
$PersonalLink .= $lui->Get_HyperLink_Close();
*/

echo $lui->Get_Div_Close();

//echo "<pre>";
//var_dump($_POST);
//echo "</pre>";
/////////////////////////////////////////
////   Gen the menu list ////////////////
if($RollBackStep == "")
$RollBackStep = 5; // MAX STEP SHOW

$NatArr = array();
$NatArr[]= array($Lang['email']['SelectRecipient'], 0);

if($TeachType != "" && $RollBackStep >= 1)
{
	if($TeachType == 0)
	$NatArr[] = array($Lang['email']['TeachingStaff'], 0);
	else if($TeachType == 1)
	$NatArr[] = array($Lang['email']['NonTeachingStaff'], 1);
	else if($TeachType == 5)
	$NatArr[] = array($Lang['email']['SchoolBasedGroup'], 5);
}

if($TeachType2 != "" && $RollBackStep >= 2)
{
	if($TeachType2 == 0)
	$NatArr[] = array($Lang['email']['Individual'], 0);
	else if($TeachType2 == 1)
	$NatArr[] = array($Lang['email']['Subjects'], 1);
	else if($TeachType2 == 2)
	$NatArr[] = array($Lang['email']['Years'], 2);
	
	//else if($TeachType2 == 3)
	//$NatArr[] = array($Lang['email']['Sections'], 3);
}

if($SchoolGroup != "" && $RollBackStep >= 2)
{
	$Sql =  " SELECT GroupName, MailGroupID FROM MAIL_ADDRESS_GROUP WHERE GroupType='S' ";
	$ReturnArr = $LibDB->returnArray($Sql);

	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($SchoolGroup == $ReturnArr[$i]["MailGroupID"])
		{
			$NatArr[] = array($ReturnArr[$i]["GroupName"], $ReturnArr[$i]["MailGroupID"]);
			break;
		}
	}
} // end school group

if($TeachSubject != "" && $RollBackStep >= 3)
{
	// Subject
	$Sql =  "exec Get_SubjectNameList_ByTTPeriod @TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."' ";
	$ReturnArr = $LibDB->returnArray($Sql);
		
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($TeachSubject == $ReturnArr[$i]["Subject_Code"])
		{
			$NatArr[] = array($ReturnArr[$i]["FullName"], $ReturnArr[$i]["Subject_Code"]);
			break;
		}
	}
} // end if Teach Subject

if($House != "" && $RollBackStep >= 3)
{
	$Sql =  "exec Get_HouseList";
	$ReturnArr = $LibDB->returnArray($Sql);
	
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($House == $ReturnArr[$i]["HouseCode"])
		{
			$NatArr[] = array($ReturnArr[$i]["HouseName"], $ReturnArr[$i]["HouseCode"]);
			break;
		}
	}
} // end if Hosue

if($SchoolYear != "" && $RollBackStep >= 3)
{
	$Sql =  "exec Get_SchoolYearList ";
	$ReturnArr = $LibDB->returnArray($Sql);
			
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($SchoolYear == $ReturnArr[$i]["School_Year"])
		{
			$NatArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);	
			break;
		}	
	}
} // end if school year

if($SchoolYear != "" && $Roll_Group != "" && $RollBackStep >= 4)
{
	$Sql =  "exec Get_RollGroupList_BySchoolYear @School_Year = '".$SchoolYear."' ";
	$ReturnArr = $LibDB->returnArray($Sql);
						
	$IsFound = false;
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($Roll_Group == $ReturnArr[$i]["Roll_Group"])
		{
			$NatArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);		
			if ($Roll_Group == $ReturnArr[$i]["Roll_Group"])
			{
				$IsFound = true;
			}
			break;
		}
	}	
	if (!$IsFound)
	{
		$Roll_Group  = "";
	}
} // end roll group

if($House != "" && $School_Year_House != "" && $RollBackStep >= 4)
{
	$Sql =  "exec Get_SchoolYearList_ByHouse @House = '".$House."' ";
	$ReturnArr = $LibDB->returnArray($Sql);
						
	$IsFound = false;
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($School_Year_House == $ReturnArr[$i]["School_Year"])
		{
			$NatArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);		
			if ($School_Year_House == $ReturnArr[$i]["School_Year"])
			{
				$IsFound = true;
			}
			break;
		}
	}	
	if (!$IsFound)
	{
		$School_Year_House  = "";
	}
} // end if House->School Year

if($Year_Teacher_Type != "" && $RollBackStep >= 4)
{
	if($Year_Teacher_Type == 0)
	$NatArr[] = array($Lang['email']['All_Teacher'], 0);
	else if($Year_Teacher_Type == 1)
	$NatArr[] = array($Lang['email']['Roll_Group_Teacher'], 1);
}

echo $lui->Get_Div_Open("", 'class="imail_link_menu"');
echo $lui->Get_Span_Open("", 'class="imail_entry_read_link"');

for($i = 0; $i < count($NatArr); $i++)
{
	if($i%3 == 0 && $i != 0)
	$MenuList .= $lui->Get_Br();
	
	$MenuList .= $lui->Get_HyperLink_Open("#", 'onClick="jRollBackStep('.$i.')"');
	if($i == 0)
	$MenuList .= $NatArr[$i][0];
	else
	$MenuList .= " > ".$NatArr[$i][0];
	$MenuList .= $lui->Get_HyperLink_Close();
}

if (isset($_REQUEST["SchoolName"]))
{
	echo $lui->Get_HyperLink_Open("select_school.php", "") . $_REQUEST["SchoolName"] . $lui->Get_HyperLink_Close();
	echo " > ";
}
echo $MenuList;

echo $lui->Get_Span_Close();
echo $lui->Get_Div_Close();
echo $lui->Get_Br();

/////////////////////////////////////////////////////////////////////
if($RollBackStep < 1)
{
$Year_Teacher_Type = "";
$TeachType = "";
$TeachType2 = "";
$TeachSubject = "";
$SchoolYear = "";
$Roll_Group = "";
$House = "";
$School_Year_House = "";
$SchoolGroup = "";

$Step2Select = "";
$Step3Select = "";
$Step4Select = "";
$Step5Select = "";
}
else if($RollBackStep < 2)
{
$Year_Teacher_Type = "";
$TeachType2 = "";
$TeachSubject = "";
$SchoolYear = "";
$Roll_Group = "";
$House = "";
$School_Year_House = "";
$SchoolGroup = "";

$Step3Select = "";
$Step4Select = "";
$Step5Select = "";
}
else if($RollBackStep < 3)
{
$Year_Teacher_Type = "";
$TeachSubject = "";
$SchoolYear = "";
$Roll_Group = "";
$House = "";
$School_Year_House = "";

$Step4Select = "";
$Step5Select = "";
}
else if($RollBackStep < 4)
{
$Year_Teacher_Type = "";
$Roll_Group = "";
$School_Year_House = "";
$Step5Select = "";
}

if($Step5Select != "")
{
	$StepSelect = $Step5Select;
	$ActionStep = $ActionStep5;
	$ShowStep = "step5";
	$IsEndingMode = $IsEnding5;
}
else if($Step4Select != "")
{
	$StepSelect = $Step4Select;
	$ActionStep = $ActionStep4;
	$ShowStep = "step5";
	$IsEndingMode = $IsEnding4;
}
else if($Step3Select != "")
{
	$StepSelect = $Step3Select;
	$ActionStep = $ActionStep3;
	$ShowStep = "step4";
	$IsEndingMode = $IsEnding3;
}
else if($Step2Select != "")
{
	$StepSelect = $Step2Select;
	$ActionStep = $ActionStep2;
	$ShowStep = "step3";
	$IsEndingMode = $IsEnding2;
}
else
{
	$StepSelect = $Step1Select;
	$ActionStep = $ActionStep1;
	$ShowStep = "step2";
	$IsEndingMode = $IsEnding;
}

$StepBtn .=  $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep.' ');

if (!$IsEndingMode)
{	
	$StepBtn .=  "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'mail_delegation_select_member.php#'.$ShowStep.'\';this.form.submit()" ');
}

$StepBtn = $lui->Get_Div_Open("form_btn").$StepBtn.$lui->Get_Div_Close();

$A_Link =  "<a name=\"".$ShowStep."\" ></a>";

//$StepSelect = "";
$Step1Select = "";
$Step2Select = "";
$Step3Select = "";
$Step4Select = "";
$Step5Select = "";

////////////////////////////////////////

echo $lui->Get_Div_Open("commontabs_board", "class=\"imail_mail_content\"");											
echo $lui->Get_Form_Open("forma","POST","mail_delegation_select_member.php",'');

if (isset($_REQUEST["SchoolCode"]))
{
	echo $lui->Get_Input_Hidden("SchoolCode", "SchoolCode", $_REQUEST["SchoolCode"]);
	echo $lui->Get_Input_Hidden("SchoolName", "SchoolName", $_REQUEST["SchoolName"]);
}

if ($StepSelect != "")
{	
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $StepSelect;
	echo $StepBtn;
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();		
}

if($TeachType != ""){
	echo $lui->Get_Input_Hidden("TeachType", "TeachType", $TeachType)."\n";
}
if($TeachType2 != ""){
	echo $lui->Get_Input_Hidden("TeachType2", "TeachType2", $TeachType2)."\n";
}
if($TeachSubject != ""){
	echo $lui->Get_Input_Hidden("TeachSubject", "TeachSubject", $TeachSubject)."\n";
}
if($SchoolYear != ""){
	echo $lui->Get_Input_Hidden("SchoolYear", "SchoolYear", $SchoolYear)."\n";
}
if($Roll_Group != ""){
	echo $lui->Get_Input_Hidden("Roll_Group", "Roll_Group", $Roll_Group)."\n";
}
if($House != ""){
	echo $lui->Get_Input_Hidden("House", "House", $House)."\n";
}
if($School_Year_House != ""){
	echo $lui->Get_Input_Hidden("School_Year_House", "School_Year_House", $School_Year_House)."\n";
}
if($SchoolGroup != ""){
	echo $lui->Get_Input_Hidden("SchoolGroup", "SchoolGroup", $SchoolGroup)."\n";	
}
if($Year_Teacher_Type != ""){
	echo $lui->Get_Input_Hidden("Year_Teacher_Type", "Year_Teacher_Type", $Year_Teacher_Type)."\n";
}

////////////////////    Show Step 1    ////////////////////////////
if ($Step1Select != "")
{
echo $lui->Get_Paragraph_Open("align=\"center\"");
echo $Step1Select;

echo $lui->Get_Div_Open("form_btn");
echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep1.' ');
if (!$IsEnding)
{	
	echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'mail_delegation_select_member.php#step2\';this.form.submit()" ');
}
echo $lui->Get_Div_Close();

echo $lui->Get_Paragraph_Close();

echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
echo $lui->Get_Paragraph_Close();
}
////////////////////////////////////////////////////////////////

if ($Step2Select != "")
{	
	echo "<a name=\"step2\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step2Select;	
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep2.' ');
	if (!$IsEnding2)
	{	
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'mail_delegation_select_member.php#step3\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();		
} // end if show step2

if ($Step3Select != "")
{
	echo "<a name=\"step3\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step3Select;	
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep3.' ');
	if (!$IsEnding3)
	{	
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'mail_delegation_select_member.php#step4\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
} // end if show step3

if ($Step4Select != "")
{
	echo "<a name=\"step4\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step4Select;	
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep4.' ');
	if (!$IsEnding4)
	{	
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'mail_delegation_select_member.php#step5\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
} // end if show step4

if ($Step5Select != "")
{
	echo "<a name=\"step5\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step5Select;	
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep5.' ');
	if (!$IsEnding5)
	{	
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'mail_delegation_select_member.php#step5\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
}

} //end check auth right

else
{
echo $lui->Get_Div_Open("commontabs");
echo $lui->Get_HyperLink_Open("#");
echo $lui->Get_Span_Open();
echo "You have no premission to select recipient";
echo $lui->Get_Span_Close();
echo $lui->Get_HyperLink_Close();
}


echo $lui->Get_Div_Open("form_btn");
echo $lui->Get_Input_Button("btn_close1","button",$Lang['btn_close_window'],'class="button_main_act" onclick="window.close()" ');
echo $lui->Get_Div_Close();

echo $lui->Get_Input_Hidden("actionLevel","actionLevel");
echo $lui->Get_Input_Hidden("specialAction","specialAction");
echo $lui->Get_Input_Hidden("fromSrc","fromSrc",$fromSrc);
echo $lui->Get_Input_Hidden("srcForm","srcForm",$srcForm);
echo $lui->Get_Input_Hidden("Mode","Mode",$_REQUEST['Mode']);
echo $lui->Get_Input_Hidden("RollBackStep","RollBackStep"); // to roll back the last step
echo $lui->Get_Form_Close();

echo $lui->Get_Div_Close();
echo $lui->Get_Div_Close();
echo $lui->Get_Div_Close();

include_once($PathRelative."src/include/template/popup_footer.php");
?>
