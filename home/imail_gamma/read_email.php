<?php
// Page Modifying by: Henry Yuen
$PathRelative = "../../../../";
$CurSubFunction = "Communication";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}
AuthPage(array("Communication-Mail-GeneralUsage"));
include_once($PathRelative."src/include/template/general_header.php");
include_once($PathRelative."src/include/class/imap_gamma.php");
include_once($PathRelative."src/include/class/imap_gamma_ui.php");

#########################################################################################

## Use Library
$lui = new imap_gamma_ui();
$IMap = new imap_gamma();

## Get Data 
$MessageID   = (isset($uid) && $uid != "") ? $uid : '';	
$mailbox 	 = (isset($mailbox) && $mailbox != "") ? $mailbox : "Inbox";
$displayMode = (isset($displayMode) && $displayMode != "") ? $displayMode : "folder";		
$currentPage = (isset($currentPage) && $currentPage != "") ? $currentPage : 0;
$PartNumber = (isset($PartNumber) && $PartNumber != "") ? $PartNumber : "";
$NoButton = $_GET['NoButton'];

$Msg 	    = (isset($Msg) && $Msg != "") ? $Msg : '';
$Keyword    = (isset($Keyword) && $Keyword != "") ? stripslashes(trim($Keyword)) : '';
$Folder     = (isset($Folder) && $Folder != "") ? stripslashes(urldecode($Folder)) : $SYS_CONFIG['Mail']['FolderPrefix'];
$FromSearch = (isset($FromSearch) && $FromSearch != "") ? $FromSearch : 0;
$sort 		= (isset($sort) && $sort != "") ? $sort : (($FromSearch == 1) ? "SortDate" : "SORTARRIVAL");	
$pageNo     = (isset($pageNo) && $pageNo != "") ? $pageNo : 1;				
$reverse    = (isset($reverse) && $reverse != "") ? $reverse : 1;			

if($FromSearch == 1){
	$IsAdvanceSearch = (isset($IsAdvanceSearch) && $IsAdvanceSearch != "") ? $IsAdvanceSearch : 1;
} else {
	$IsAdvanceSearch = '';
}

$CurMenu    = '1';
$CurTag     = $Folder;

# Get Data From Simple Serach 
$keyword 		= (isset($keyword) && $keyword != "") ? stripslashes(trim($keyword)) : '';

# Get Data From Advance Search
$KeywordFrom 	= (isset($KeywordFrom) && $KeywordFrom != "") ? $KeywordFrom : '';
$KeywordTo   	= (isset($KeywordTo) && $KeywordTo != "") ? $KeywordTo : '';
$KeywordCc   	= (isset($KeywordCc) && $KeywordCc != "") ? $KeywordCc : '';
$KeywordSubject = (isset($KeywordSubject) && $KeywordSubject != "") ? $KeywordSubject : '';
$FromDate  		= (isset($FromDate) && $FromDate != "") ? $FromDate : '';
$ToDate  		= (isset($ToDate) && $ToDate != "") ? $ToDate : '';
$SearchFolder   = (isset($SearchFolder) && is_array($SearchFolder) && count($SearchFolder) > 0) ? $SearchFolder : $IMap->getMailFolders();

if (trim($PartNumber) == "") {
	## Preparation
	$QuerySearchFolder = "";
	for ($i=0;$i< sizeof($SearchFolder); $i++) {
		$QuerySearchFolder .= $lui->Get_Input_Hidden("SearchFolder[]","SearchFolder[]", stripslashes($SearchFolder[$i]))."\n";
	}
	
	# Get Prev/Next Message UID
	$PrevNextUID = $IMap->Get_Prev_Next_UID($Folder, $sort, $reverse, $MessageID);
	
	# Get Folder Structure for select box
	$FolderArray = $IMap->Get_Folder_Structure();
}

# Get the Email for display
$EmailContent = $IMap->getMailRecord($Folder, $MessageID, $PartNumber);

##############################################################################
# Imap use only
//$imap 		   = imap_open("{" . $server . ":" . $port . "}" . $mailbox, $username, $password);
imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$Folder);		# Go to the Corresponding Folder Currently selected
$imap_obj 	   = imap_check($IMap->inbox);
$unseen_status = imap_status($IMap->inbox, "{".$IMap->host."}".$Folder, SA_UNSEEN);
$statusInbox   = imap_status($IMap->inbox, "{".$IMap->host."}Inbox", SA_UNSEEN);

$imapInfo['imap_obj'] 	   = $imap_obj;
$imapInfo['unseen_status'] = $unseen_status;
$imapInfo['statusInbox']   = $statusInbox;
$imapInfo['displayMode']   = $displayMode;
$imapInfo['sort'] 		   = $sort;
$imapInfo['reverse'] 	   = $reverse;
//$imapInfo['currentPage']   = $currentPage;	//	old pagination with gamma approach
$imapInfo['currentPage']   = ($pageNo - 1);	//	new pagination with gamma approach

# Can be ignored
$imapInfo['mailbox']   	   = $Folder;			// orig: mailbox

##############################################################################

## create Tool bar structure
# Left Menu Tool Bar 
$LeftMenu = array();
if (trim($PartNumber) == "") {
	if ($Folder == $IMap->RootPrefix."Trash"){
		$LeftMenu[] = $lui->Get_Input_Button("Go","button",$Lang['email']['Delete'],'class="button" onclick="Confirm_Submit_Page(\''.$Lang['Warning']['MailDeleteWarn'].'\',\'form1\',\'delete_email.php\');"')."&nbsp;|&nbsp;";
	} else {
		$LeftMenu[] = $lui->Get_Input_Button("Go","button",$Lang['email']['Delete'],'class="button" onclick="Submit_Page(\'form1\',\'delete_email.php\');"')."&nbsp;|&nbsp;";
		$LeftMenu[] .= $lui->Get_Input_Button("UnreadButton", "UnreadButton", $Lang['email']['MarkAsUnread'], ' class="button" onclick="Submit_Page(\'form1\',\'setunreadflag.php\');"').'&nbsp;|&nbsp;<br/>';
	}
}
$LeftMenu[] = $lui->Get_Input_Button("Go","button",$Lang['email']['Reply'],'class="button" onclick="document.form1.MailAction.value = \'Reply\'; Submit_Page(\'form1\',\'compose_email.php\');"')."&nbsp;";
$LeftMenu[] = $lui->Get_Input_Button("Go","button",$Lang['email']['ReplyAll'],'class="button"  onclick="document.form1.MailAction.value = \'ReplyAll\'; Submit_Page(\'form1\',\'compose_email.php\');"')."&nbsp;";
$LeftMenu[] = $lui->Get_Input_Button("Go","button",$Lang['email']['Forward'],'class="button"  onclick="document.form1.MailAction.value = \'Forward\'; Submit_Page(\'form1\',\'compose_email.php\');"')."&nbsp;";
if (trim($PartNumber) == "") {
	$LeftMenu[] = $lui->Get_Input_Select_With_Label("","",$FolderArray,"",'onchange=" if (this.value != \'\' && this.value != \'Default\' && this.value != \'Personal\') { document.form1.DestinationFolder.value = encodeURIComponent(this.value); Confirm_Submit_Page(\''.$Lang['Warning']['MailMoveWarn'].'\',\'form1\',\'move_email.php\'); }"');
	
	$redirect_url = ($FromSearch == 1) ? (($IsAdvanceSearch == 1) ? "advance_search_result.php" : "search_result.php") : "imail_gamma.php";
	if ($Keyword != ""){
		$LeftMenu[] = "&nbsp;|&nbsp;".$lui->Get_Input_Button("Go","button",$Lang['email']['SearchBack'],'class="button"  onclick="Submit_Page(\'form1\',\''.$redirect_url.'\');"')."&nbsp;";
	} else {
		$LeftMenu[] = "&nbsp;|&nbsp;".$lui->Get_Input_Button("Go","button",$Lang['email']['SearchBack'],'class="button"  onclick="Parent_To_Top(); Submit_Page(\'form1\',\''.$redirect_url.'\');"')."&nbsp;";
	}
}
# Right Menu Tool Bar
$RightMenu = array();

$RightMenu[] = $lui->Get_HyperLink_Open("#", 'onclick="window.open(\''.$PathRelative.'/src/module/email/cache_print_email.php?Folder='.urlencode($Folder).'&uid='.$MessageID.'&PartNumber='.$PartNumber.'\',\'\',\'scrollbars=1, left=0, location=0, menubar=0, status=0, titlebar=0, toolbar=0, top=0\');return false"').$lui->Get_Image($SYS_CONFIG['Image']['Abs']."icon_print.gif", 18, 18, 'ALIGN="absmiddle" BORDER="0"')." ".$Lang['email']['Print'].$lui->Get_HyperLink_Close();
//functions are not ready, hide them first
if (trim($PartNumber) == "") {
	$RightMenu[] = "|";
	if ($PrevNextUID['PrevUID'] !== false){
		//$RightMenu[] = $lui->Get_HyperLink_Open("read_email.php?uid=".$PrevNextUID['PrevUID']."&Folder=".$Folder."&CurMenu=1&CurTag=".$CurTag."&order=".$order."&field=".$field)."&lt; ".$Lang['email']['PreviousMail'].$lui->Get_HyperLink_Close();
		$RightMenu[] = $lui->Get_HyperLink_Open("javascript:document.form1.uid.value=".$PrevNextUID['PrevUID'].";Submit_Page('form1', 'read_email.php')")."&lt; ".$Lang['email']['PreviousMail'].$lui->Get_HyperLink_Close();
	} else {
		$RightMenu[] = " -- ";
	}
	if ($PrevNextUID['NextUID'] !== false){
		//$RightMenu[] = $lui->Get_HyperLink_Open("read_email.php?uid=".$PrevNextUID['NextUID']."&Folder=".$Folder."&CurMenu=1&CurTag=".$CurTag."&order=".$order."&field=".$field).$Lang['email']['NextMail']."&nbsp;>".$lui->Get_HyperLink_Close();
		$RightMenu[] = $lui->Get_HyperLink_Open("javascript:document.form1.uid.value=".$PrevNextUID['NextUID'].";Submit_Page('form1', 'read_email.php')").$Lang['email']['NextMail']."&nbsp;>".$lui->Get_HyperLink_Close();
	} else {
		$RightMenu[] = " -- ";
	}
}

## Main 
echo $lui->Get_Div_Open("module_imail", 'class="module_content"');

echo $lui->Get_Sub_Function_Header($Lang['email_gamma']['HeaderTitle'], $Msg);
echo $lui->Get_Switch_MailBox();

# Left Menu
echo $lui->Get_Left_Sub_Mail_Menu($CurMenu, $CurTag, $IMap, $Folder, $imapInfo);

# Right Menu
echo $lui->Get_Div_Open("imail_right");
if ($NoButton != 1) {
	# generate Upper Toolbar
	
	echo $lui->Get_Tool_Bar($LeftMenu,$RightMenu);
	
}

# Display Email Content
/* -------------------------------------------------------------------------------------------------------*/
echo $lui->Get_Read_Mail($EmailContent);
/* -------------------------------------------------------------------------------------------------------*/

if ($NoButton != 1) {
	# generate Lower Toolbar
	echo $lui->Get_Tool_Bar($LeftMenu, $RightMenu);
}

echo $lui->Get_Form_Open("form1", "post");
# Hidden Values
echo $lui->Get_Input_Hidden("uid", "uid", $MessageID)."\n";			// for swtich mail previous & next use only 
echo $lui->Get_Input_Hidden("PartNumber","PartNumber",$PartNumber)."\n";
echo $lui->Get_Input_Hidden("CurTag", "CurTag", $CurTag)."\n";
echo $lui->Get_Input_Hidden("CurMenu", "CurMenu", $CurMenu)."\n";
echo $lui->Get_Input_Hidden("Folder", "Folder", urlencode($Folder))."\n";
echo $lui->Get_Input_Hidden("pageNo", "pageNo", $pageNo)."\n";
echo $lui->Get_Input_Hidden("reverse", "reverse", $reverse)."\n";
echo $lui->Get_Input_Hidden("sort", "sort", $sort)."\n";
echo $lui->Get_Input_Hidden("DestinationFolder", "DestinationFolder")."\n";

echo $lui->Get_Input_Hidden("UID", "UID", $MessageID)."\n";			// for delete & move mail use only
echo $lui->Get_Input_Hidden("MailAction", "MailAction")."\n";
//echo $lui->Get_Input_Hidden("Keyword", "Keyword", $Keyword)."\n";
echo $lui->Get_Input_Hidden("FromSearch", "FromSearch", $FromSearch)."\n";
echo $lui->Get_Input_Hidden("IsAdvanceSearch", "IsAdvanceSearch", $IsAdvanceSearch)."\n";

# Hidden Values - Simple Search
echo $lui->Get_Input_Hidden("keyword", "keyword", $keyword)."\n";

# Hidden Values - Advance Search 
echo $lui->Get_Input_Hidden("KeywordFrom", "KeywordFrom", $KeywordFrom)."\n";
echo $lui->Get_Input_Hidden("KeywordTo", "KeywordTo", $KeywordTo)."\n";
echo $lui->Get_Input_Hidden("KeywordCc", "KeywordCc", $KeywordCc)."\n";
echo $lui->Get_Input_Hidden("KeywordSubject", "KeywordSubject", $KeywordSubject)."\n";
echo $lui->Get_Input_Hidden("FromDate", "FromDate", $FromDate)."\n";
echo $lui->Get_Input_Hidden("ToDate", "ToDate", $ToDate)."\n";
echo $QuerySearchFolder;

echo $lui->Get_Form_Close();	# End form1

echo $lui->Get_Div_Close();
echo $lui->Get_Div_Close();

#########################################################################################
include_once($PathRelative."src/include/template/general_footer.php");
?>
