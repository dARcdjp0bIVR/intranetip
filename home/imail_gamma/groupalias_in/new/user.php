<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
#include_once("../../../../includes/libgrouping.php");
include_once("../../../../includes/libuser.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
$content = explode("\n",$file_content);
# Get row 1,2,4 only. Compatible with previous version
if ($content[0][0]==1) $permitted[] = 1;
if ($content[1][0]==1) $permitted[] = 2;
if ($content[3][0]==1) $permitted[] = 3;

if ($file_content == "")
{
    $permitted = array(1,2,3);
}

$li = new libuser($UserID);

# get toStaff, toStudent, toParent options
$sql ="SELECT a.RecordType,a.Teaching,c.ClassLevelID FROM INTRANET_USER AS a 
	LEFT OUTER JOIN INTRANET_CLASS AS b ON(a.ClassName=b.ClassName) 
	LEFT OUTER JOIN INTRANET_CLASSLEVEL AS c ON (b.ClassLevelID=c.ClassLevelID) 
	WHERE a.UserID='$UserID'";
$temp = $li->returnArray($sql,3);
list($usertype,$teaching,$class_level_id) = $temp[0];

$sql_to_options = "SELECT ToStaffOption,ToStudentOption,ToParentOption FROM INTRANET_IMAIL_RECIPIENT_RESTRICTION WHERE TargetType='$usertype' AND Restricted=1";
if($usertype==1)
	$sql_to_options.=" AND Teaching='$teaching'";
else if($usertype==2)
	$sql_to_options.=" AND ClassLevel='$class_level_id'";
$result_to_options = $li->returnArray($sql_to_options,3);
list($toStaff,$toStudent,$toParent) = $result_to_options[0];

# end get toStaff, toStudent, toParent options


if (!in_array($UserType,$permitted) || ($toStaff==-1 && $toStudent==-1 && $toParent==-1))
{
    header("Location: index.php");
    exit();
}
if ($UserType == 1 && $toStaff!=-1)   # Staff: Just show the list directly
{
    header("Location: list.php?UserType=$UserType&AliasID=$AliasID");
    exit();
}
if ($UserType == 2 && $toStudent!=-1)
{

   	header("Location: list.php?UserType=$UserType&AliasID=$AliasID");
    exit();
}
else if ($UserType == 3 && $toParent!=-1)
{
   	header("Location: list.php?UserType=$UserType&AliasID=$AliasID");
    exit();

#include_once("../../../templates/fileheader.php");
#include_once("../../../templates/filefooter.php");

}
intranet_closedb();
?>