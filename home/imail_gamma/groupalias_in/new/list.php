<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/includes/libclass.php");

intranet_auth();
intranet_opendb();

if ($UserType == "")
{
    header("Location: index.php?AliasID=$AliasID");
    exit();
}

$li = new libuser($UserID);

# get toStaff, toStudent, toParent options
$sql ="SELECT a.RecordType,a.Teaching,c.ClassLevelID FROM INTRANET_USER AS a 
	LEFT OUTER JOIN INTRANET_CLASS AS b ON(a.ClassName=b.ClassName) 
	LEFT OUTER JOIN INTRANET_CLASSLEVEL AS c ON (b.ClassLevelID=c.ClassLevelID) 
	WHERE a.UserID='$UserID'";
$temp = $li->returnArray($sql,3);
list($usertype,$teaching,$class_level_id) = $temp[0];

$sql_to_options = "SELECT ToStaffOption,ToStudentOption,ToParentOption FROM INTRANET_IMAIL_RECIPIENT_RESTRICTION WHERE TargetType='$usertype' AND Restricted=1";
if($usertype==1)
	$sql_to_options.=" AND Teaching='$teaching'";
else if($usertype==2)
	$sql_to_options.=" AND ClassLevel='$class_level_id'";
$result_to_options = $li->returnArray($sql_to_options,3);
list($toStaff,$toStudent,$toParent) = $result_to_options[0];

# end get toStaff, toStudent, toParent options


$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
$content = explode("\n",$file_content);
# Get row 1,2,4 only. Compatible with previous version
if ($content[0][0]==1) $permitted[] = 1;
if ($content[1][0]==1) $permitted[] = 2;
if ($content[3][0]==1) $permitted[] = 3;

if ($file_content == "")
{
    $permitted = array(1,2,3);
}

if (!in_array($UserType,$permitted) || ($toStaff==-1 && $toStudent==-1 && $toParent==-1)){
    header("Location: index.php?AliasID=$AliasID");
    exit();
}

# get users already added to INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY
	$sqlUsers = "SELECT TargetID FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY WHERE AliasID='$AliasID' AND RecordType='U'";
	$user_in_table = $li->returnVector($sqlUsers);
	$user_in_table = implode(",",$user_in_table);
# end get users

########################
$name_field = getNameFieldByLang("a.");
$fields = "DISTINCT a.UserID,$name_field";
$tables = "INTRANET_USER AS a";
$conds  = "a.RecordStatus=1";
if($user_in_table!="")$conds.=" AND a.UserID NOT IN($user_in_table)";

if($li->isParent()){
		$child_list = $li->getChildren();
		$child_list = implode(",",$child_list);
}else if($li->isStudent()){
		$child_list = $UserID;
}



# Staff selected
if($UserType==1){
	 $conds .=" AND a.RecordType=1 ";
	 if($toStaff==1){ 
		switch($li->RecordType){
				# sender is student, get teachers whos teaching the student
				case 2 : 	
							//$tables .=",INTRANET_USERGROUP AS b, INTRANET_USERGROUP AS c,INTRANET_CLASS AS d ";
							//$conds .=" AND a.UserID=c.UserID AND b.GroupID=c.GroupID AND b.UserID='$UserID' AND b.GroupID=d.GroupID";
							$t_sql = "SELECT b.ClassID FROM INTRANET_CLASS AS b, INTRANET_USER AS a WHERE a.UserID='$UserID' AND a.ClassName=b.ClassName ";
							$t_result = $li->returnVector($t_sql);
							$t_class_id = $t_result[0];
							$t_sql =" SELECT DISTINCT UserID FROM INTRANET_CLASSTEACHER WHERE ClassID='$t_class_id'";
							$t_sql2 =" SELECT DISTINCT UserID FROM INTRANET_SUBJECT_TEACHER WHERE ClassID='$t_class_id'";
							$t_result = $li->returnVector($t_sql);
							$t_result2 = $li->returnVector($t_sql2);
							for($x=0;$x<sizeof($t_result2);$x++){
								if(!in_array($t_result2[$x],$t_result))
									$t_result[] = $t_result2[$x];
							}
							$t_teacher_list = implode(",",$t_result);
							//$tables .=" LEFT OUTER JOIN INTRANET_CLASSTEACHER AS b ON (b.ClassID=d.ClassID) LEFT OUTER JOIN INTRANET_SUBJECT_TEACHER AS c ON (c.ClassID=d.ClassID) ,INTRANET_CLASS AS d,INTRANET_USER AS e ";
							//$conds .= " AND a.UserID IN (e.UserID='$UserID' AND e.ClassName = d.ClassName AND b.ClassID=d.ClassID AND c.ClassID=d.ClassID ";
							if($t_teacher_list=="") $t_teacher_list = "''";
							$conds .= " AND a.UserID IN ($t_teacher_list) ";
							break;
				
				# sender is parent, get teachers teaching the parent's child
				case 3 : 	//$tables .=",INTRANET_CLASS AS b,INTRANET_USERGROUP AS c,INTRANET_USER AS d ";
							//$conds .=" AND d.UserID IN($child_list) AND d.ClassName=b.ClassName and b.GroupID=c.GroupID AND a.UserID=c.UserID";
							if($child_list=="") break;
							$t_sql = "SELECT DISTINCT b.ClassID FROM INTRANET_CLASS AS b, INTRANET_USER AS a WHERE a.UserID IN ($child_list) AND a.ClassName=b.ClassName ";
							$t_result = $li->returnVector($t_sql);
							$t_class_id = implode(",",$t_result);
							$t_sql =" SELECT DISTINCT UserID FROM INTRANET_CLASSTEACHER WHERE ClassID IN ($t_class_id) ";
							$t_sql2 =" SELECT DISTINCT UserID FROM INTRANET_SUBJECT_TEACHER WHERE ClassID IN ($t_class_id)";
							$t_result = $li->returnVector($t_sql);
							$t_result2 = $li->returnVector($t_sql2);
							for($x=0;$x<sizeof($t_result2);$x++){
								if(!in_array($t_result2[$x],$t_result))
									$t_result[] = $t_result2[$x];
							}
							$t_teacher_list = implode(",",$t_result);
							if($t_teacher_list!="")
								$conds .= " AND a.UserID IN ($t_teacher_list) ";
							break;
				default: break;
		}
	 }
	$sql="SELECT $fields FROM $tables WHERE $conds ";
	$targetUsers = $li->returnArray($sql,2);
}
# student selected
else if($UserType==2){
	$conds .=" AND a.RecordType=2 ";
	
    if($toStudent==4){ # own children
	    $tables .=",INTRANET_PARENTRELATION AS b";
	    $conds .=" AND a.UserID=b.StudentID AND b.ParentID='$UserID'";
	    $sql="SELECT $fields FROM $tables WHERE $conds ";
		$targetUsers = $li->returnArray($sql,2);
	}else{
		# create class list
		$tmpFields = "DISTINCT a.GroupID, a.ClassName";
		$tmpTables = "INTRANET_CLASS AS a";
		$tmpConds  = "a.RecordStatus=1";
		switch($toStudent){
			# students in taught class ( only when sender is teacher)
			case  1 :	
						$t_sql =" SELECT DISTINCT ClassID FROM INTRANET_CLASSTEACHER WHERE UserID='$UserID' ";
						$t_sql2 =" SELECT DISTINCT ClassID FROM INTRANET_SUBJECT_TEACHER WHERE UserID='$UserID' ";
						$t_result = $li->returnVector($t_sql);
						$t_result2 = $li->returnVector($t_sql2);
						for($x=0;$x<sizeof($t_result2);$x++){
							if(!in_array($t_result2[$x],$t_result))
								$t_result[] = $t_result2[$x];
						}
						$t_class_list = implode(",",$t_result);  // the class list taught by the teacher
						if($t_class_list=="") $t_class_list = "''";
						$tmpConds.= " AND a.ClassID IN ($t_class_list) ";
						break;
			# students in same class level ( only when the sender is parent/student)
			case  2 : 	
						$tmpTables.=", INTRANET_CLASS AS b,INTRANET_USER AS c "; 
						$tmpConds.=" AND c.UserID IN($child_list) AND c.ClassName=b.ClassName AND b.ClassLevelID=a.ClassLevelID ";
						break;
			# students in same class ( only when the sender is parent/student)
			case  3 : 	
						$tmpTables.=", INTRANET_USER AS b "; 
						$tmpConds.=" AND b.UserID IN($child_list) AND b.ClassName = a.ClassName ";
						break;
			# all students
			default : break;
		}
		$tmpSql="SELECT $tmpFields FROM $tmpTables WHERE $tmpConds ";
		
		$class_list = $li->returnArray($tmpSql,2);
		$valid_class=false;
		$select_class_list="";
		if($classGroupID=="" && sizeof($class_list)>0)
			$classGroupID=$class_list[0][0];
		for($i=0;$i<sizeof($class_list);$i++){
			if($classGroupID ==$class_list[$i][0])
				$valid_class = true;
			$select_class_list .= "<OPTION value=\"".$class_list[$i][0]."\"".($classGroupID ==$class_list[$i][0]?"SELECTED":"").">".$class_list[$i][1]."</OPTION>";
		}
		if($select_class_list!="")
			$select_class_list ="$i_Teaching_ClassList : <SELECT name=classGroupID onChange='changeClass(this.form)'>".$select_class_list."</SELECT>";
		if($valid_class){
			//$tables.=",INTRANET_CLASS AS b, INTRANET_USERGROUP AS c";
			//$conds.=" AND c.UserID=a.UserID AND c.GroupID=b.GroupID AND b.GroupID='$classGroupID'";
			$tables.=",INTRANET_CLASS AS b";
			$conds.=" AND a.ClassName = b.ClassName AND b.GroupID='$classGroupID'";

			$sql="SELECT $fields FROM $tables WHERE $conds ";
			$targetUsers = $li->returnArray($sql,2);
		}
		
	}
}
# parent selected
else if($UserType==3){
	$conds .=" AND a.RecordType=3 ";
	
	if($toParent==4){ # own parent ( only when the sender is student)
	   	$tables .=",INTRANET_PARENTRELATION AS b";
	   	$conds.=" AND a.UserID=b.ParentID AND b.StudentID='$UserID'";
	    $sql="SELECT $fields FROM $tables WHERE $conds ";
		$targetUsers = $li->returnArray($sql,2);
	}
    else{
	    # create class list
		$tmpFields = "DISTINCT a.GroupID, a.ClassName";
		$tmpTables = "INTRANET_CLASS AS a";
		$tmpConds  = "a.RecordStatus=1";
	    switch($toParent){
			# parents in taught class
			case  1 :	//$tables.=",INTRANET_USERGROUP AS b";
						//$conds.=" AND b.UserID='$UserID' AND b.GroupID=a.GroupID";
						$t_sql =" SELECT DISTINCT ClassID FROM INTRANET_CLASSTEACHER WHERE UserID='$UserID' ";
						$t_sql2 =" SELECT DISTINCT ClassID FROM INTRANET_SUBJECT_TEACHER WHERE UserID='$UserID' ";
						$t_result = $li->returnVector($t_sql);
						$t_result2 = $li->returnVector($t_sql2);
						for($x=0;$x<sizeof($t_result2);$x++){
							if(!in_array($t_result2[$x],$t_result))
								$t_result[] = $t_result2[$x];
						}
						$t_class_list = implode(",",$t_result);  // the class list taught by the teacher
						if($t_class_list=="") $t_class_list = "''";
						$tmpConds.= " AND a.ClassID IN ($t_class_list) ";
						break;
			# parent in same class level ( parent/stuent sender only )
			case  2 : 	$tmpTables.=", INTRANET_CLASS AS b,INTRANET_USER AS c "; 
						$tmpConds.=" AND c.UserID IN ($child_list) AND c.ClassName=b.ClassName AND b.ClassLevelID=a.ClassLevelID ";
						break;
			# parent in same class ( parent/stuent sender only )
			case  3 : 	$tmpTables.=", INTRANET_USER AS b "; 
						$tmpConds.=" AND b.UserID IN($child_list) AND b.ClassName=a.ClassName ";
						break;
			# all parent
			default : break;
		}
		$tmpSql="SELECT $tmpFields FROM $tmpTables WHERE $tmpConds ";
		
		$class_list = $li->returnArray($tmpSql,2);
		$valid_class=false;
		$select_class_list="";
		if($classGroupID=="" && sizeof($class_list)>0)
			$classGroupID=$class_list[0][0];
		for($i=0;$i<sizeof($class_list);$i++){
			if($classGroupID ==$class_list[$i][0])
				$valid_class = true;
			$select_class_list .= "<OPTION value=\"".$class_list[$i][0]."\"".($classGroupID ==$class_list[$i][0]?"SELECTED":"").">".$class_list[$i][1]."</OPTION>";
		}
		if($select_class_list!="")
			$select_class_list ="$i_Teaching_ClassList : <SELECT name=classGroupID onChange='changeClass(this.form)'>".$select_class_list."</SELECT>";
		if($valid_class){
			$tables.=",INTRANET_USER AS b, INTRANET_CLASS AS d,INTRANET_PARENTRELATION AS c";
			$conds.=" AND d.GroupID='$classGroupID' AND b.ClassName=d.ClassName AND b.UserID=c.StudentID AND a.UserID=c.ParentID";

			$sql="SELECT $fields FROM $tables WHERE $conds ";
			$targetUsers = $li->returnArray($sql,2);
		}
	}
}

$display_usertype=$i_identity_array[$UserType];

$x2  = "<select name=\"TargetID[]\" size=\"8\" multiple>\n";
for($i=0; $i<sizeof($targetUsers); $i++)
{
          $id 		= $targetUsers[$i][0];
          $name 	= $targetUsers[$i][1];
          $class_number = $targetUsers[$i][2];
          
          $x2 .= "<option value=\"$id\">$name</option>\n";
     }
$x2 .= "</select>\n";

$MODULE_OBJ['title'] = $button_select.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_ListAlias;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function changeClass(formObj){
	formObj.action='list.php';
	formObj.submit();
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
//-->
</script>

<form name="form1" action="list_update.php" method="POST" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<br />
        
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tabletext">
        <tr><td ><b><?="$i_CampusMail_New_AddressBook_ByUser &gt; ".$display_usertype.($Class==""?"":" &gt; $Class")?></b></td><td></td></tr>
        <tr><td height="5" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10" /></td></tr>
        <tr><td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1" /></td></tr>
        <tr><td height="5" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10" /></td></tr>
        </table>
        
        
        <table width="100%" border="0" cellpadding="3" cellspacing="0" class="tabletext">
        
        <?  	if(sizeof($targetUsers)<=0)
		{
			echo "<tr><td align=\"center\" colspan=\"2\">$i_no_record_exists_msg</td></tr>\n";
		}        
		else
		{ 
			?>	
	        <tr >
			<td valign="top" nowrap="nowrap" width="20%" >
			<span class="tabletext"><?=$i_CampusMail_New_AddressBook_ByUser?>:</span>
			</td>
			<td >					
			<table border="0" cellpadding="0" cellspacing="0" align="left">
                        
                        
			<tr >
				<td >				
				<?=$x2?>
				</td>
				<td valign="bottom" >
				<table cellpadding="0" cellspacing="6" >
				<tr >
					<td ><?= $linterface->GET_BTN($button_add, "submit") ?></td>										
				</tr >	

				<?php 
				if(!$sys_custom['Mail_NoSelectAllButton']) 
				{ 
				?>
				<tr >
					<td >																		
					<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['TargetID[]']); return false;","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
					</td >
				</tr >	
				<?php 
				} 
				?>				
				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>
	<? } ?>                	
        </table>
        
        
        </td>
</tr>

<tr>
		<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center">
                        <?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php?AliasID=". $AliasID ."'") ?>
			<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
</table>        


<input type="hidden" name="UserType" value="<?=$UserType?>">
<input type="hidden" name="AliasID" value="<?=$AliasID?>">
</form>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>