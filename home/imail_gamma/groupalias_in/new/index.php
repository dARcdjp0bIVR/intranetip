<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimailext.php");

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if ($selected == 1 && isset($AddressID) && is_array($AddressID))
{
    # Add to INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY
    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY (AliasID,TargetID,DateInput,DateModified) VALUES ";
    for($i=0;$i<sizeof($AddressID);$i++){
	    $entry = "('$AliasID', '".$AddressID[$i]."',NOW(),NOW()),";
	    $sql .= $entry;
	}
	$sql=substr($sql,0,strlen($sql)-1);
	$lm = new libimailext();
	$lm->db_db_query($sql);
	$lm->synAliasCount($AliasID,1);
	echo "<script language=javascript>
			window.opener.location.reload();
			//window.close();
		</script>";
}
?>
<?
# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html("popup.html");

if($field=="") $field = 0;
if($order=="") $order = 1;
$li = new libdbtable2007($field, $order, $pageNo);
$keyword = trim($keyword);

$lib = new libdb();
$tmp = $lib->returnVector("SELECT TargetID FROM INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY WHERE AliasID='$AliasID'",1);

$sql  = "SELECT
		DateModified,
               TargetName,
               TargetAddress,
               DateModified,
               CONCAT('<input type=\"checkbox\" name=\"AddressID[]\" value=\"', AddressID ,'\">')
          FROM INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
          WHERE
               OwnerID = '$UserID' AND
               (TargetName like '%".$li->Get_Safe_Sql_Like_Query(htmlspecialchars(stripslashes($keyword),ENT_QUOTES))."%' OR
                TargetAddress like '%".$li->Get_Safe_Sql_Like_Query(htmlspecialchars(stripslashes($keyword),ENT_QUOTES))."%'
                )
          ";
if(sizeof($tmp)>0){
	$sql.=" AND AddressID NOT IN (".implode(",",$tmp).")";
}

# TABLE INFO
$li->field_array = array("TargetName","TargetAddress","DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->IsColOff = "imail_addressbook_list_externalRecipient";
$li->title = "";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class=\"tabletoplink\" width=\"1\">#</td>\n";
$li->column_list .= "<td class=\"tabletoplink\" width=\"40%\">".$li->column($pos++, "$i_CampusMail_New_AddressBook_Name")."</td>\n";
$li->column_list .= "<td class=\"tabletoplink\" width=\"40%\">".$li->column($pos++, "$i_CampusMail_New_AddressBook_EmailAddress")."</td>\n";
$li->column_list .= "<td class=\"tabletoplink\" width=\"19%\">".$li->column($pos++, $i_LastModified)."</td>\n";
$li->column_list .= "<td class=\"tabletoplink\" width=\"1%\">".$li->check("AddressID[]")."</td>\n";
$li->column_list .= "<td class=\"tabletoplink\" width=\"1%\"></td>\n";
$li->column_array = array(0,5,0,0);
$li->wrap_array = array(0,50,0,0);

### Button / Tag
$Addbtn		= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><td>&nbsp;</td><td>" . $linterface->GET_BTN($button_add, "button", "addAddress(document.form1)","addbtn"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ") . "</td></tr></table>";
$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$searchTag 	.= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".htmlspecialchars(stripslashes($keyword),ENT_QUOTES)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
$searchTag 	.= "</tr></table>";

$MODULE_OBJ['title'] = $button_select.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_ListAlias;

$linterface->LAYOUT_START();

?>
<SCRIPT LANGUAGE=Javascript>
function addAddress(obj)
{
         obj.selected.value = 1;
         checkRestore(obj,'AddressID[]','index.php');
         if (countChecked(obj,'AddressID[]')==0)
         {
             obj.selected.value = 0;
         }
}
</SCRIPT>

<form name="form1" method="get" action= "" >

<table width="96%" border="0" cellspacing="0" cellpadding="5">
<tr> 
	<td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		
		<tr>
		<td>
    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><?=$searchTag?></td>
				<td align="right" valign="bottom"><?=$Addbtn?></td>
			</tr>
            </table>
            </td>
			</tr>
			
			<tr>
				<td><?=$li->display()?></td>
			</tr>
			
			</table>
			</td>
		</tr>
		</table>
		</td>
	</tr>
        <tr>
		<td align="right"><?=$Addbtn?></td>
	</tr>
        <tr>
		<td>
		<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
        
	</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="selected" value="0">
<input type="hidden" name="fieldname" value="<?=$fieldname?>">
<input type="hidden" name="AliasID" value="<?=$AliasID?>">
</form>                  

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>