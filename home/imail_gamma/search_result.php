<?php
// editing by 
############################### Change Log #################################
# 2020-08-24 by Cameron: fix: should reset pageNo to 1 for each search click, otherwise, it'd lead to wrong navigation record shown from starting
# 2015-05-20 by Carlos : Improved to make the toolbar panel floats on top when window scrolls down and toolbar is not visible to user.
# 2014-10-27 by Carlos : Added base64 encoded folder name Folder_b and SearchFolder_b[]
# 2014-09-02 by Carlos : Modified js checkAll(obj), initSelectAllBlock(), initHighlightRow() to adapt the new search approach
# 2014-06-16 by Carlos : Modified to search mails by time spans
# 2014-06-06 by Carlos : Added missing js MoveMailTo()
# 2012-07-19 by Carlos : Added cache search fields to restore from viewmail page Back action
# 2012-05-28 by Carlos : Changed sender/receiver column width from 150 to 15% 
# 2011-11-18 by Carlos : added js initSelectAllBlock(), selectAllUid(), deselectAllUid() for select all feature
# 2011-06-15 by Carlos : added search from cached mails from DB
# 2010-10-07 by Carlos : added data column Size
############################################################################
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$IMap = new imap_gamma();
$linterface = new interface_html("imail_default.html");
//$IMap->Start_Timer();

## Get search condition fields from file cache
if($FromSearchCache==1){
	$search_result = $IMap->Get_Cached_Search_Result();
	$search_fields = $search_result[2];
	if(count($search_fields)>0){
		//debug_pr($search_fields);
		$SearchFolder = array();
		for($i=0;$i<count($search_fields);$i++){
			if(count($search_fields[$i])>0){
				foreach($search_fields[$i] as $k => $v){
					if($k == 'SearchFolder[]'){
						$SearchFolder[] = $v;
					}else{
						$$k = $v;
					}
				}
			}
		}
	}
}

## Get Data
$isAdvanceSearch = isset($isAdvanceSearch)? $isAdvanceSearch : (isset($KeywordFrom)?1:'');
$KeywordFrom 	= (isset($KeywordFrom) && $KeywordFrom != "") ? stripslashes(trim($KeywordFrom)) : "";
$KeywordTo		= (isset($KeywordTo) && $KeywordTo != "") ? stripslashes(trim($KeywordTo)) : "";
$KeywordCc 		= (isset($KeywordCc) && $KeywordCc != "") ? stripslashes(trim($KeywordCc)) : "";
$KeywordBcc 	= (isset($KeywordBcc) && $KeywordBcc != "") ? stripslashes(trim($KeywordBcc)) : "";
$KeywordSubject = (isset($KeywordSubject) && $KeywordSubject != "") ? stripslashes(trim($KeywordSubject)) : "";
$KeywordBody 	= (isset($KeywordBody) && $KeywordBody != "") ? stripslashes(trim($KeywordBody)) : "";
$KeywordAttachment = (isset($KeywordAttachment) && $KeywordAttachment != "") ? stripslashes(trim($KeywordAttachment)) : "";
$ToDate 		= (isset($ToDate) && $ToDate != "") ? trim($ToDate) : date("Y-m-d");
$FromDate 		= (isset($FromDate) && $FromDate != "") ? trim($FromDate) : date("Y-m-d",strtotime("-1 month"));
if(isset($Folder_b) && $Folder_b != ''){
	$Folder = base64_decode($Folder_b);
}else{
	$Folder 		= (isset($Folder))? stripslashes($Folder) : $SYS_CONFIG['Mail']['FolderPrefix'];
}

# get Folder to search
if(isset($SearchFolder_b) && count($SearchFolder_b)>0){
	$SearchFolder = array();
	for($i=0;$i<count($SearchFolder_b);$i++){
		$SearchFolder[] = base64_decode($SearchFolder_b[$i]);
	}
	$SearchFolder = array_unique($SearchFolder);
}else{
	$SearchFolder 	= (isset($SearchFolder) && count($SearchFolder) > 0) ? $SearchFolder : $IMap->getMailFolders();
}
$SearchFolderHidden = '';
for ($i=0; $i <sizeof($SearchFolder) ; $i++) {
	$SearchFolderHidden .= '<input type="hidden" id="SearchFolder[]" name="SearchFolder[]" value="'.stripslashes($SearchFolder[$i]).'">';
	$SearchFolderHidden .= '<input type="hidden" name="SearchFolder_b[]" value="'.base64_encode($SearchFolder[$i]).'">';
}

# Page Info Data
//$sort 		 = (isset($sort) && $sort != "") ? $sort : "DateSort";		// array sort fields: DateSort, From, Subject, Size, To, Folder
$reverse 	 = (isset($order) && $order != "") ? $order : 1;		// $order
$pageNo 	 = (isset($pageNo) && $pageNo != "") ? $pageNo : 1;
$numPerPage  = (isset($numPerPage) && $numPerPage != "") ? $numPerPage : 20;

# Update the NumPerPage of User Preference
//if($numPerPageOld != "" && $numPerPageOld > 0){
//	if($numPerPage != $numPerPageOld){
//		if($numPerPage != "" && $numPerPage > 0){
//			$UPref->Set_User_Mail_Per_Page($numPerPage);
//		} else {
//			$numPerPage = $numPerPageOld;
//		}
//	}
//} else {
//	$numPerPage = 50;
//}


## Initialization
$CurTag 	= $Folder;
$CurMenu    = 1;
$SearchInfo = array();
$imapInfo   = array();

## Preparation
$FolderArray = $IMap->Get_Folder_Structure(true);		// get Folder Structure for select box

$SearchInfo['keyword'] 	    =  stripslashes(trim($keyword));
//$SearchInfo['Folder'][] 	= $Folder;
$SearchInfo['From'] 	= $KeywordFrom;
$SearchInfo['To'] 		= $KeywordTo;
$SearchInfo['Cc'] 		= $KeywordCc;
$SearchInfo['Bcc'] 		= $KeywordBcc;
$SearchInfo['Subject']  = $KeywordSubject;
$SearchInfo['FromDate'] = $FromDate;
$SearchInfo['ToDate'] 	= $ToDate;
$SearchInfo['Body'] = $KeywordBody;
$SearchInfo['Attachment'] = $KeywordAttachment;
$SearchInfo['Folder'] 	= $SearchFolder;
//$SearchInfo['Sort'] 	= $sort;
//$SearchInfo['Reverse'] 	= $reverse;

##############################################################################
# Imap use only
//$imap_obj 	   = imap_check($IMap->inbox);
//$unseen_status = imap_status($IMap->inbox, "{".$IMap->host."}".$Folder, SA_UNSEEN);
//$statusInbox   = imap_status($IMap->inbox, "{".$IMap->host."}Inbox", SA_UNSEEN);

//$imapInfo['imap_obj'] 	   = $imap_obj;
//$imapInfo['unseen_status'] = $unseen_status;
//$imapInfo['statusInbox']   = $statusInbox;
##############################################################################

 	

################## dbtable settings  ####################
$default_field = 2; // date 
if($field=="") $field = $default_field;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
    $pageNo = 1;
}

$default_order = $IMap->CacheMailToDB == true? 0:1;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$numPerPage = ($numPerPage != "") ? $numPerPage :$ck_page_size;
$pageSize = ($numPerPage != "") ? $numPerPage : 20;		# default 20
$order 	 = (isset($order) && $order != "") ? $order : $default_order;
$pageNo 	 = (isset($pageNo) && $pageNo != "") ? $pageNo : 1;
$numPerPage  = (isset($numPerPage) && $numPerPage != "") ? $numPerPage : $pageSize;
$currentPage = (isset($currentPage) && $currentPage != "") ? $currentPage : 0;

$li = new libdbtable2007($field, $order, $pageNo);
$li->page_size = $numPerPage;

$li->IsColOff = "imail_gamma_search";
$li->field_array= array("'From'","Subject","DateSort","Folder","'Size'");

$isDraftInbox = $Folder == $IMap->DraftFolder ;
$isOutbox = $Folder == $IMap->SentFolder;
//$senderreceiver = ($isDraftInbox||$isOutbox)?$i_frontpage_campusmail_recipients:$i_frontpage_campusmail_sender;
if($isAdvanceSearch == 1){
	$senderreceiver = $i_frontpage_campusmail_sender.' / '.$i_frontpage_campusmail_recipients;
}else{
	$senderreceiver = ($isDraftInbox||$isOutbox)?$i_frontpage_campusmail_recipients:$i_frontpage_campusmail_sender;
}

	$pos=0;
	$li->column_list .= "<td width='1%' class='tabletop' >". $i_frontpage_campusmail_icon_status_2007."</td>\n";
	$li->column_list .= "<td width='1%' class='tabletop' ><img align='absmiddle' src='$image_path/$LAYOUT_SKIN/icon_star_off.gif' border='0' /></td>\n";
	$li->column_list .= "<td width='1%' class='tabletop' ><img align='absmiddle' src='$image_path/$LAYOUT_SKIN/iMail/icon_important_title.gif' border='0' /></td>\n";
	$li->column_list .= "<td width='1%' class='tabletop' >". $i_frontpage_campusmail_icon_notification_2007."</td>\n";
	$li->column_list .= "<td width='15%' class='tabletop' >".$li->column($pos++, $senderreceiver)."</td>\n";
	$li->column_list .= "<td width='60%' class='tabletop' >".$li->column($pos++, $Lang['Gamma']['Subject'])."</td>\n";
	$li->column_list .= "<td width='100' class='tabletop' >".$li->column($pos++, $i_frontpage_campusmail_date)."</td>\n";
	$li->column_list .= "<td width='60' class='tabletop' >".$li->column($pos++, $i_frontpage_campusmail_folder)."</td>\n";
	$li->column_list .= "<td width='60' class='tabletop' >".$li->column($pos++, $i_frontpage_campusmail_size)."</td>\n";
	$li->column_list .= "<td width='25' class='tabletop' ><input type='checkbox' onclick='checkAll(this)' id='CheckAllBox'></td>\n";

$li->no_col = 11;
################## dbtable settings end ####################


### Get Search Mail List Data 
$pageStart = $numPerPage * ($pageNo - 1) + 1; 
$pageEnd = $numPerPage * $pageNo;

if($IMap->CacheMailToDB == true)
{
	$sql_fields = "IF(SenderName <> '',SenderName,SenderEmail) as 'From',
					Subject,
					MailTimestamp as DateSort,
					Folder,
					MailSize as 'Size',
					UID,
					ToRecipient as 'To',
					MailDateString as DateReceived,
					Recent as recent,
					Flagged as flagged,
					Answered as answered,
					Deleted as deleted,
					Seen as seen,
					Draft as draft,
					IsImportant as Priority,
					Attachment as HaveAttachment ";
	
	$SkipFolders = array();
	$SkipFolders[] = $IMap->reportSpamFolder;
	$SkipFolders[] = $IMap->reportNonSpamFolder;
	$SkipFolders[] = $IMap->HiddenFolder;
	
	$sql_cond = "WHERE UserEmail = '".$IMap->CurUserAddress."' AND Folder NOT IN ('".implode("','",$SkipFolders)."') ";
	if(sizeof($SearchInfo['Folder'])>0){
		$sql_cond .= " AND Folder IN ('".implode("','",$SearchInfo['Folder'])."') ";
	}
	// advance search
	if($SearchInfo['FromDate']!=''){
		$sql_cond .= " AND MailDate >= '".$SearchInfo['FromDate']." 00:00:00' ";
	}
	if($SearchInfo['ToDate']!=''){
		$sql_cond .= " AND MailDate <= '".$SearchInfo['ToDate']." 23:59:59' ";
	}
	if($SearchInfo['From']!=''){
		$QueryFrom = $li->Get_Safe_Sql_Like_Query($SearchInfo['From']);
		$sql_cond .= " AND (SenderName LIKE '%".$QueryFrom."%' 
						OR SenderEmail LIKE '%".$QueryFrom."%') ";
	}
	if($SearchInfo['Subject']!=''){
		$QuerySubject = $li->Get_Safe_Sql_Like_Query($SearchInfo['Subject']);
		$sql_cond .= " AND Subject LIKE '%".$QuerySubject."%' ";
	}
	if($SearchInfo['To']!=''){
		$QueryTo = $li->Get_Safe_Sql_Like_Query($SearchInfo['To']);
		$sql_cond .= " AND ToRecipient LIKE '%".$QueryTo."%' ";
	}
	if($SearchInfo['Cc']!=''){
		$QueryCc = $li->Get_Safe_Sql_Like_Query($SearchInfo['Cc']);
		$sql_cond .= " AND CcRecipient LIKE '%".$QueryCc."%' ";
	}
	if($SearchInfo['Bcc']!=''){
		$QueryBcc = $li->Get_Safe_Sql_Like_Query($SearchInfo['Bcc']);
		$sql_cond .= " AND BccRecipient LIKE '%".$QueryBcc."%' ";
	}
	if($MailStatus=="UNSEEN"){
		$sql_cond .= " AND Seen <> 1 ";
	}elseif($MailStatus=="SEEN"){
		$sql_cond .= " AND Seen = 1 ";
	}elseif($MailStatus=="FLAGGED"){
		$sql_cond .= " AND Flagged = 1 ";
	}
	if($SearchInfo['Attachment']!=''){
		$QueryAttachment = $li->Get_Safe_Sql_Like_Query($SearchInfo['Attachment']);
		$sql_cond .= " AND Attachment LIKE '%".$QueryAttachment."%' ";
	}
	if($SearchInfo['Body']!=''){
		$QueryBody = $li->Get_Safe_Sql_Like_Query($SearchInfo['Body']);
		$sql_cond .= " AND Message LIKE '%".$QueryBody."%' ";
	}
	
	// search from folder
	if($SearchInfo['keyword']!=''){
		$QueryKeyword = $li->Get_Safe_Sql_Like_Query($SearchInfo['keyword']);
		$sql_cond .= " AND (MailDate LIKE '%".$QueryKeyword."%' 
							OR SenderName LIKE '%".$QueryKeyword."%' 
							OR SenderEmail LIKE '%".$QueryKeyword."%' 
							OR Subject LIKE '%".$QueryKeyword."%' 
							OR ToRecipient LIKE '%".$QueryKeyword."%' 
							OR CcRecipient LIKE '%".$QueryKeyword."%' 
							OR BccRecipient LIKE '%".$QueryKeyword."%' 
							OR Attachment LIKE '%".$QueryKeyword."%' 
							OR Message LIKE '%".$QueryKeyword."%' 
						)";
	}
	
	$sql = "SELECT ";
	$sql .= $sql_fields;
	$sql .= " FROM MAIL_CACHED_MAILS ";
	$sql .= $sql_cond;
	$li->sql = $sql;
	$built_sql = $li->built_sql();
	
	$MailList[0] = $li->returnArray($built_sql);
	$MailList[1] = $li->total_row;
	
	$pageStart = $li->n_start;
	$pageEnd = min($li->n_start + $li->page_size, $MailList[1]);
}else 
{
	//$MailList = $IMap->Get_Search_Mail_List($pageStart, $pageEnd, $li->field_array[$field], $order, $SearchField, $SearchInfo, $FromSearch,$MailStatus);
}
##################### gen tool bar start ###################
#
# back btn

# Search bar
$spacer = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" />';
if(!$FromSearch)
{
	
$date_range_ary = array();
$date_range_ary[] = array('1week',$Lang['Gamma']['OneWeek']);
$date_range_ary[] = array('1month',$Lang['Gamma']['OneMonth']);
$date_range_ary[] = array('3month',$Lang['Gamma']['ThreeMonths']);
$date_range_ary[] = array('6month',$Lang['Gamma']['SixMonths']);
$date_range_ary[] = array('1year',$Lang['Gamma']['OneYear']);
$date_range_ary[] = array('custom',$Lang['Gamma']['CustomDateRange']);

$DateRangeMethod = isset($DateRangeMethod) && in_array($DateRangeMethod,array('1week','1month','3month','6month','1year','custom'))? $DateRangeMethod : '1month'; 
$date_range_selection = $linterface->GET_SELECTION_BOX($date_range_ary,' id="SimpleDateRangeMethod" name="SimpleDateRangeMethod" onchange="onDateRangeMethodChanged(this);" ','',$DateRangeMethod);

$ts_today = time();
$ToDate = isset($ToDate) && $ToDate!="" ? $ToDate : date("Y-m-d",$ts_today);
$FromDate = isset($FromDate) && $FromDate != "" ? $FromDate : date("Y-m-d",strtotime("-1 month",$ts_today));
$FromDateInput = $linterface->GET_DATE_PICKER("SimpleStartDate",$FromDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $DateRangeMethod != 'custom', $cssClass="textboxnum");
$ToDateInput = $linterface->GET_DATE_PICKER("SimpleEndDate",$ToDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $DateRangeMethod != 'custom', $cssClass="textboxnum");
	
	
//$searchbar = '<form id="SearchForm" name="SearchForm" method="POST" action="search_result.php" onsubmit="return false;">';
	//$searchbar .= $linterface->Get_Search_Box_Div("keyword", ($keyword?stripslashes(trim($keyword)):"Search on Subject"),' onfocus="if (this.value == \'Search on Subject\') this.value=\'\';"');
	
	$searchbar .= '<div class="selectbox_group selectbox_group_filter">';
		$searchbar .= '<a href="javascript:void(0);" onclick="if(document.getElementById(\'search_option\').style.visibility==\'hidden\'){MM_showHideLayers(\'search_option\',\'\',\'show\');}else{MM_showHideLayers(\'search_option\',\'\',\'hide\');}"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_search.gif" width="20" height="20" border="0" align="absmiddle" />'.$Lang['Btn']['Search'].'</a>';
	$searchbar .= '</div>';
	$searchbar .= '<p class="spacer"></p>';
	$searchbar .= '<div id="search_option" class="selectbox_layer selectbox_group_layer" style="width:640px;visibility:hidden;">';
	$searchbar .= '<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>
						<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$i_frontpage_campusmail_recipients.'</td>
							<td valign="top">
							<input type="text" name="SimpleKeywordTo" size="50" class="textboxtext" value="'.$KeywordTo.'" />
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$i_frontpage_campusmail_sender.'</td>
							<td valign="top">
							<input type="text" name="SimpleKeywordFrom" size="50" class="textboxtext" value="'.$KeywordFrom.'" />
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['Gamma']['cc'].'</td>
							<td valign="top">
							<input type="text" name="SimpleKeywordCc" size="50" class="textboxtext" value="'.$KeywordCc.'" />
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['Gamma']['bcc'].'</td>
							<td valign="top">
							<input type="text" name="SimpleKeywordBcc" size="50" class="textboxtext" value="'.$KeywordBcc.'" />
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['Gamma']['Subject'].'</td>
							<td valign="top">
							<input type="text" name="SimpleKeywordSubject" size="50" class="textboxtext" value="'.$KeywordSubject.'" />
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$i_frontpage_campusmail_message.'</td>
							<td valign="top">
							<input type="text" name="SimpleKeywordBody" size="50" class="textboxtext" value="'.$KeywordBody.'" />
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$i_general_record_date.'</td>
							<td valign="top" class="tabletext">
							'.$date_range_selection.$Lang['General']['From'].'&nbsp;'.$FromDateInput.$Lang['General']['To'].'&nbsp;'.$ToDateInput.'
							</td>
						</tr>
						</table>
						</td>
					</tr>
					</table>';
	
	
		$searchbar .= '<p class="spacer"></p>';
		$searchbar .= '<div class="edit_bottom">';
		$searchbar .= '<input type="button" onmouseout="this.className=\'formsmallbutton\'" onmouseover="this.className=\'formsmallbuttonon\'" value="'.$Lang['Btn']['Search'].'" onclick="searchSubmit(document.getElementById(\'form1\'));MM_showHideLayers(\'search_option\',\'\',\'hide\');" class="formsmallbutton ">&nbsp;';
		$searchbar .= '<input type="button" onmouseout="this.className=\'formsmallbutton\'" onmouseover="this.className=\'formsmallbuttonon\'" value="'.$Lang['Btn']['Cancel'].'" onclick="MM_showHideLayers(\'search_option\',\'\',\'hide\');" class="formsmallbutton ">';
		$searchbar .= '</div>';
	$searchbar .= '</div>';
	
	$searchbar .= '<input type="hidden" name="displayMode" id="displayMode" value="search">';
	$searchbar .= '<input type="hidden" name="Folder" id="Folder" value="'.urlencode($Folder).'">';
	$searchbar .= '<input type="hidden" name="SearchFolder[]" value="'.$Folder.'" />';
	//$searchbar .= '<input type="hidden" name="Folder" value="'.$Folder.'" />';
	$searchbar .= '<input type="hidden" name="SearchFolder_b[]" value="'.base64_encode($Folder).'" />';
	$searchbar .= '<input type="hidden" name="Folder_b" value="'.base64_encode($Folder).'" />';
	$searchbar .= '<input type="hidden" name="page_from" value="viewfolder.php" />';
	$searchbar .= '<input type="hidden" name="mailbox" value="'.urlencode($Folder).'" />';
//$searchbar .= '</form>';
}
else
{
	$http_var = $_SERVER["QUERY_STRING"];
	if($FromExtAddressGroup){
		$url = "groupalias.php?aliastype=1&TabID=3";
	}else if($FromIntAddressGroup){
		$url = "groupalias.php?aliastype=0&TabID=1";
	}else if($FromExtAddressBook){
		$url = "addressbook.php";
	}else if($FromIntAddressBook){
		$url = "addressbook_internal.php";
	}else{
		$url = $FromSearch?"advance_search.php?$http_var":"viewfolder.php?Folder=urlencode($Folder)";
	}
	$backBtn  = "<a href=\"$url\" class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('prevp','','{$image_path}/{$LAYOUT_SKIN}/icon_prev_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">";
	$backBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_prev_off.gif\" name=\"prevp\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"prevp\"> {$button_back}</a>";
}

# Move to Selection
$MoveTo = $IMap->getMoveToSelection("targetFolder","");

# deleteBtn
$deleteBtn  = "<a href=\"javascript:checkRemove(document.form1,'Uid[]','remove.php')\" class=\"tabletool\">";
$deleteBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\" />";
$deleteBtn .= $button_remove."</a>";

# reportSpam/ report non spam btn
//20100812 
$confirmMsg = $Folder==$IMap->SpamFolder?$Lang['Gamma']['ConfirmMsg']['reportNonSpam']:$Lang['Gamma']['ConfirmMsg']['reportSpam'];
$BtnName = $Folder==$IMap->SpamFolder?$Lang['Gamma']['reportNonSpam']:$Lang['Gamma']['reportSpam'];
if($webmail_info['bl_spam']==true){
	$ReportSpamBtn  = "<a href=\"javascript:checkAlert(document.form1,'Uid[]','report_spam.php','$confirmMsg')\" class=\"tabletool\">";
	$ReportSpamBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_spam.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\" />";
	$ReportSpamBtn .= $BtnName."</a>"; 
}else{
	$ReportSpamBtn = "";
}
# $unseenfilter
foreach($Lang['Gamma']['UnseenStatus'] as $key => $value)
	$StatusAry[] = array($key,$value);
$unseenfilter = getSelectByArray($StatusAry,"name='MailStatus' onchange='this.form.submit()'",$MailStatus,0,1);

# mark mail as read / unread
array_shift($StatusAry);
$MarkAsSelection = getSelectByArray($StatusAry,"name='MarkAs' onchange='this.form.action=\"mark_read_unread.php\"; this.form.submit()'",'',0,0,$Lang['Gamma']['MarkAs']);
$MarkAsSelection = getSelectByArray($StatusAry,"name='MarkAs' onchange='MarkMailAs(this)'",'',0,0,$Lang['Gamma']['MarkAs']);

#	
##################### gen tool bar end ###################

######################### Cache Search Fields ###################################
$SearchFields = array();
$SearchFields[]['FromSearch'] = $FromSearch;
$SearchFields[]['order'] = $order;
$SearchFields[]['field'] = $field;
$SearchFields[]['pageNo'] = $pageNo;
$SearchFields[]['keyword'] = stripslashes(trim($keyword));
$SearchFields[]['KeywordFrom'] = $KeywordFrom;
$SearchFields[]['KeywordTo'] = $KeywordTo;
$SearchFields[]['KeywordCc'] = $KeywordCc;
$SearchFields[]['KeywordBcc'] = $KeywordBcc;
$SearchFields[]['KeywordSubject'] = $KeywordSubject;
$SearchFields[]['KeywordBody'] = $KeywordBody;
$SearchFields[]['KeywordAttachment'] = $KeywordAttachment;
$SearchFields[]['FromDate'] = $FromDate;
$SearchFields[]['ToDate'] = $ToDate;
$SearchFields[]['Folder'] = $Folder;
for ($i=0; $i <sizeof($SearchFolder) ; $i++) {
	$SearchFields[]['SearchFolder[]'] = stripslashes($SearchFolder[$i]);
	$SearchFields[]['SearchFolder_b[]'] = base64_encode($SearchFolder[$i]);
}
##################################################################################

##################### Module Obj ####################
$iMailTitle1 = "<span class='imailpagetitle'>{$i_CampusMail_New_MailSearch_SearchResult}</span>";

if($FromSearch) $Folder='';
//20100812
switch(trim($Folder))
{
	case $IMap->SentFolder	:	
		$CurrentPage = "PageCheckMail_Outbox";		break;
	
	case $IMap->DraftFolder	:	
		$CurrentPage = "PageCheckMail_Draft";		break;
	
	case $IMap->TrashFolder	:	
		$CurrentPage = "PageCheckMail_Trash";		break;
	
	case $IMap->SpamFolder	:	
		$CurrentPage = "PageCheckMail_Spam";		break;
	
	case $IMap->InboxFolder		:	
		$CurrentPage = "PageCheckMail_Inbox";		break;
	default:
}
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();

$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$iMailTitle1."</td><td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>";
$TAGS_OBJ[] = array($iMailTitle, "", 0);

$linterface->LAYOUT_START();

?>

<form id="form1" name="form1" method="get" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">	
	<?=$xmsg?>
	<tr>
		<td >
		<br />
		<table id="ToolbarTable" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr class="table-action-bar">
			<td>
				<table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td><?=$backBtn?><?=$searchbar?></td>
					</tr>
				</table>
			</td>		
			<td align="right" >
			
			<table id="Toolbar" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td><?=$unseenfilter?></td>
				<td><?=$MoveTo?></td>
				<td><?=$MarkAsSelection?></td>
				<td><?=$deleteBtn?></td>
				<td><?=$ReportSpamBtn?></td>
			</tr>
			</table>
			</td>
		</tr>
		</table>		
		</td>
	</tr>
	
	<tr>
		
		<td width="100%" >
		
		<?php
		if($IMap->CacheMailToDB == true){
			echo $li->display();
		}else
		{
			$loading_image = $linterface->Get_Ajax_Loading_Image();
			
			$x = "<table width='100%' border='0' cellspacing='0' cellpadding='4' id='MailList'  onselectstart='return false;' style='-moz-user-select:-moz-none;'>";
				$x .= "<thead>";
				$x .= $li->displayColumn();
				$x .= "</thead>";
				$x .= "<tbody>";
				
				
				//$x .= "<tr id='MailListFooter'><td colspan='".($li->no_col)."' class='tablebottom'>".$li->navigation()."</td></tr>";
				$x .= "</tbody>";
			$x .= "</table>\n";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
				$x .= "<tbody>";
				$x .= "<tr id='noRecordRow' style='display:none;'>
                          <td class='tabletext' align='center' colspan='".($li->no_col-1)."' ><br />".$li->no_msg."<br /><br /></td>
						</tr>\n";
				$x .= "<tr id='loadingRow'>
		                   <td class='tabletext' align='center' colspan='".($li->no_col-1)."' >".$loading_image."</td>
						</tr>\n";
				$x .= "<tr id='tableNavigation' style='display:none;'><td colspan='".($li->no_col-1)."' class='tablebottom'></td></tr>";

				$x .= "</tbody>";
			$x .= "</table>";
			
			echo $x;
		}
		?>
		<input type="hidden" id="pageNo" name="pageNo" value="<?php echo $li->pageNo; ?>" />
		<input type="hidden" id="order" name="order" value="<?php echo $li->order; ?>" />
		<input type="hidden" id="field" name="field" value="<?php echo $li->field; ?>" />
		<input type="hidden" id="page_size_change" name="page_size_change" value="" />
		<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>" />
		<!--<input type="hidden" name="FolderID" value="<?=$FolderID?>" />-->
		<input type="hidden" name="Folder" value="<?=urlencode($Folder)?>" />
		<input type="hidden" name="Folder_b" value="<?=base64_encode($Folder)?>" />
		<input type="hidden" name="page_from" value="viewfolder.php" />
		<input type="hidden" name="mailbox" value="<?=urlencode($Folder)?>" />
		<!--<input type="hidden" name="sort" value="<?=$sort?>" />-->
		
		<input type="hidden" name="keyword" value="<?=$keyword?>" />
		<input type="hidden" name="FromSearch" value="<?=$FromSearch?>" />
		<input type="hidden" name="KeywordFrom" value="<?=$KeywordFrom?>" />
		<input type="hidden" name="KeywordTo" value="<?=$KeywordTo?>" />
		<input type="hidden" name="KeywordCc" value="<?=$KeywordCc?>" />
		<input type="hidden" name="KeywordBcc" value="<?=$KeywordBcc?>" />
		<input type="hidden" name="KeywordSubject" value="<?=$KeywordSubject?>" />
		<input type="hidden" name="KeywordBody" value="<?=$KeywordBody?>" />
	<?php if(isset($KeywordAttachment) && trim($KeywordAttachment)!=''){ ?>
		<input type="hidden" name="KeywordAttachment" value="<?=$KeywordAttachment?>" />
	<?php } ?>
		<input type="hidden" id="FromDate" name="FromDate" value="<?=$FromDate?>" />
		<input type="hidden" id="ToDate" name="ToDate" value="<?=$ToDate?>" />
		<input type="hidden" name="isAdvanceSearch" value="<?=$isAdvanceSearch?>" />
		<input type="hidden" name="DateRangeMethod" value="<?=$DateRangeMethod?>" />
		<input type="hidden" name="FromExtAddressBook" value="<?=$FromExtAddressBook?>" />
		<input type="hidden" name="FromIntAddressBook" value="<?=$FromIntAddressBook?>" />
		<input type="hidden" name="FromExtAddressGroup" value="<?=$FromExtAddressGroup?>" />
		<input type="hidden" name="FromIntAddressGroup" value="<?=$FromIntAddressGroup?>" />
		<?=$SearchFolderHidden?>
		
		
		</td>
	</tr>
	</table>
	<br />
	</td>
</tr>
</table>
</form>

<style type="text/css">
.highlight_block_normal {
	text-align: center;
	line-height: 100%; 
	min-height: 1.375em; 
	width: 99%; 
	display: inline-block; 
	background: none repeat scroll 0pt 0pt #FFFFCC; 
	border-color: #FFFFCC; 
	padding: 3px 0pt 3px; 
	margin: 3px;
}
.highlight_block_active {
	text-align: center;
	line-height: 100%; 
	min-height: 1.375em; 
	width: 99%; 
	display: inline-block; 
	background: none repeat scroll 0pt 0pt #FFFF80; 
	border-color: #FFFF80; 
	padding: 3px 0pt 3px; 
	margin: 3px;
}
</style>

<link href="/templates/jquery/jquery.alerts.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.alerts.js"></script>

<script type="text/javascript" language="JavaScript" src="libdbtable.js"></script>
<script language="javascript">
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length;
	
    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

function MarkMailAs(obj)
{
	var confirmMsg;
	switch(obj.value)
	{
		case "SEEN":
			confirmMsg = "<?=$Lang['Gamma']['ConfirmMsg']['MarkAsSeen']?>";
			break;
		case "FLAGGED":
			confirmMsg = "<?=$Lang['Gamma']['ConfirmMsg']['MarkAsStarred']?>";
			break;
		default:
			confirmMsg = "<?=$Lang['Gamma']['ConfirmMsg']['MarkAsUnseen']?>";
			break;
	}
	
	if(obj.selectedIndex)
	{
		if(!checkAlert(document.form1,'Uid[]','mark_read_unread.php',confirmMsg))
		{
			obj.selectedIndex=0;    
		}	
	}
     
}

function checkAll(obj)
{
	if(obj.checked)
	{
		$("input[name='Uid[]']").not('.classUid').attr("checked","checked");
	<?php if($IMap->CacheMailToDB == true){ ?>
		$("table#MailList>tbody>tr:not(:first):not(:last)").find("td").removeClass("tabletext iMailrow").addClass("tabletext iMailrowSelect");
		if(!$("table#MailList").prev().is('div')){
			initSelectAllBlock();
		}
	<?php }else{ ?>
		$("table#MailList>tbody>tr").find("td").removeClass("tabletext iMailrow").addClass("tabletext iMailrowSelect");
		if($("table#MailList").prev().is('div')){
			$("table#MailList").prev().remove();
		}
		initSelectAllBlock();
	<?php } ?>
	}
	else
	{
		$("input[name='Uid[]']").not('.classUid').attr("checked","");
	<?php if($IMap->CacheMailToDB == true){ ?>	
		$("table#MailList>tbody>tr:not(:first):not(:last)").find("td").removeClass("tabletext iMailrowSelect").addClass("tabletext iMailrow");
	<?php }else{ ?>
		$("table#MailList>tbody>tr").find("td").removeClass("tabletext iMailrowSelect").addClass("tabletext iMailrow");
		if($("table#MailList").prev().is('div')){
			$("table#MailList").prev().remove();
		}
	<?php } ?>
	}
}

function setFlag(folder,uid,obj)
{
	var imgsrc = obj.src.substr(obj.src.lastIndexOf("/")+1)
	var setflag = imgsrc=="icon_star_on.gif"?0:1; //set off if on
	if(setflag==1)
		obj.src=obj.src.replace("icon_star_off.gif","icon_star_on.gif")
	else
		obj.src=obj.src.replace("icon_star_on.gif","icon_star_off.gif")

	$.post("ajax_task.php",
		{
			"task"		: "SetMailFlag",
			"Folder"	: folder,
			"Uid"		: uid,
			"setflag"	: setflag
		}
	);

}

function jCheckAlert(obj,element,page,msg,cancel_caller,cancel_callback)
{
	if(countChecked(obj,element)==0){
	    jAlert(globalAlertMsg2);
	    if(cancel_caller && cancel_callback){
    		cancel_callback.apply(cancel_caller);
    	}
	}else{
		jConfirm(msg, 'Confirmation Dialog', function(r) {
		    if(r){
		    	// OK
		    	obj.action=page;
	        	obj.method="POST";
	        	obj.submit();
		    }else{
		    	// Cancel
		    	if(cancel_caller && cancel_callback){
		    		cancel_callback.apply(cancel_caller);
		    	}
		    }
		});
	}
}

function MoveMailTo(obj)
{
	if (obj.selectedIndex != 0)
	{
		var cancel_callback = function()
		{
			obj.selectedIndex = 0;
		}
		jCheckAlert(document.form1,'Uid[]','move_email.php','<?=$i_CampusMail_New_alert_moveto?>',this,cancel_callback);
	}
}

//$().ready(function(){
//	initHighlightRow();
//	$("input[name='Uid[]']:checked").parent().parent().find("td").removeClass("iMailrow").addClass("iMailrowSelect");
//})

var mousedown;
var action = '';
var selectingoption = false; //use to avoid deselecting a mail when click on a selection box option
var disablehighlight = false; 
var startrow,endrow;
function initHighlightRow()
{
	mousedown = false;
	// highlight ticked row on load	
	$("input[name='Uid[]']").click(function(){
		// the tick checkbox action will be handled on mousedown event
		return false;
	})
	
	$(document).mousedown(function(){mousedown=true; }).mouseup(function(){mousedown=false; action='';})

	// disable select row on certain action
	$("select").click(function(){selectingoption=true;}); 
	$("a").mouseover(function(){disablehighlight=true; }).mouseout(function(){disablehighlight=false; });
	$("img.mailflag").mouseover(function(){disablehighlight=true; }).mouseout(function(){disablehighlight=false;}); 
	
	<?php if($IMap->CacheMailToDB == true){ ?>
	$("table#MailList>tbody>tr:not(:first:last)").mouseover(function(){
	<?php }else{ ?>
	$("table#MailList>tbody>tr").mouseover(function(){
	<?php } ?>
		if(!selectingoption)
		{
			if(mousedown==true)
			{
				endrow= $("table#MailList>tbody>tr").index(this);
				if(endrow>startrow)
				{
					var lower = startrow;
					var upper = endrow+1;
				}
				else
				{
					var lower = endrow-1;
					var upper = startrow;
				}
				
				if(action=='select')
				{
					$("table#MailList>tbody>tr:lt("+upper+"):gt("+lower+")").children().removeClass("iMailrow").addClass("iMailrowSelect");
					$("table#MailList>tbody>tr:lt("+upper+"):gt("+lower+")").find("[name='Uid[]']:not(:checked)").attr("checked","checked");
				}
				else
				{
					$("table#MailList>tbody>tr:lt("+upper+"):gt("+lower+")").children().removeClass("iMailrowSelect").addClass("iMailrow");
					$("table#MailList>tbody>tr:lt("+upper+"):gt("+lower+")").find("[name='Uid[]']:checked").attr("checked","");
				}
				
				if($("input[name='Uid[]']:not(:checked)").length==0)
					$("#CheckAllBox").attr("checked","checked");
				else
					$("#CheckAllBox").attr("checked","");
			}
		}
		selectingoption=false;
	}).mousedown(function(evt){
		startrow=$("table#MailList>tbody>tr").index(this);
		if(!disablehighlight)
		{
			var boxchecked = !$(this).find("[name='Uid[]']").attr("checked")
			$(this).find("[name='Uid[]']").attr("checked",boxchecked);
			if(boxchecked)
			{
				$(this).find("td").removeClass("iMailrow").addClass("iMailrowSelect");
				action = 'select';
			}
			else
			{
				$(this).find("td").removeClass("iMailrowSelect").addClass("iMailrow");
				action = 'deselect';
				
			}
		<?php if(!$IMap->CacheMailToDB){ ?>	
			if($("table#MailList").prev().is('div')){
				$("table#MailList").prev().remove();
				initSelectAllBlock();
			}
		<? } ?>
			if($("input[name='Uid[]']:not(:checked)").length==0)
				$("#CheckAllBox").attr("checked","checked");
			else
				$("#CheckAllBox").attr("checked","");
		}
	});
	
	
	
}

function initSelectAllBlock()
{
<?php if($IMap->CacheMailToDB == true){ ?>		
	$.get(
		"ajax_task.php",
		{
			"task": "GetAllSearchUIDFolder"
		},
		function(data){
			if(data == "") return;
			var uidArr = data.split('/');
			var total = uidArr.length;
			var visibleUid = [];
			$('input[name=Uid[]]').each(function(i){
				visibleUid.push($(this).val());
			});
			if(total == 0 || total <= visibleUid.length) return;
			var selectedCnt = $('input[name=Uid[]]:checked').length;
			var msg1 = '<?=$Lang['Gamma']['CommonMsg']['CurrentPageSelectedMails']?>';
			var msg2 = '<a href="javascript:selectAllUid();">'+'<?=$Lang['Gamma']['CommonMsg']['SelectMatchedMails']?>'+'</a>';
			msg1 = msg1.replace('<!--NUMBER-->',selectedCnt);
			var divBlock = '<div class="highlight_block_normal">' + msg1 + ' ' + msg2 + '</div>';
			$("table#MailList").before(divBlock);
			var frm = $('form[name=form1]');
			for(var i=0;i<uidArr.length;i++){
				if(visibleUid.indexOf(uidArr[i])==-1){
					frm.append('<input type="checkbox" name="Uid[]" value="'+uidArr[i]+'" class="classUid" style="display:none;"/>');
				}
			}
		}
	);
<? }else{ ?>
	var visibleUidCount = $('input[name="Uid\\[\\]"]:visible').length;
	var allUidCount = $('input[name="Uid\\[\\]"]').length;
	var selectedCnt = $('input[name="Uid\\[\\]"]:checked').length;
	if(allUidCount == 0 || selectedCnt == 0) return;
	if(allUidCount > visibleUidCount){
		var msg1 = '<?=$Lang['Gamma']['CommonMsg']['CurrentPageSelectedMails']?>';
		msg1 = msg1.replace('<!--NUMBER-->',selectedCnt);
		var divBlock = '<div class="highlight_block_normal">' + msg1 + '</div>';
		$("table#MailList").before(divBlock);
	}
<? } ?>
}

function selectAllUid()
{
	$("input[name='Uid[]']").attr("checked","checked");
	<?php if($IMap->CacheMailToDB == true){ ?>
		$("table#MailList>tbody>tr:not(:first):not(:last)").find("td").removeClass("tabletext iMailrow").addClass("tabletext iMailrowSelect");
	<?php }else{ ?>
	$("table#MailList>tbody>tr").find("td").removeClass("tabletext iMailrow").addClass("tabletext iMailrowSelect");
	<?php } ?>
	$('table#MailList').prev().remove();
	var selectedCnt = $("input[name='Uid[]']:checked").length;
	var msg1 = '<?=$Lang['Gamma']['CommonMsg']['SelectedAllMatchedMails']?>';
	var msg2 = '<a href="javascript:deselectAllUid();">'+'<?=$Lang['Gamma']['CommonMsg']['ClearSelection']?>'+'</a>';
	msg1 = msg1.replace('<!--NUMBER-->',selectedCnt);
	var divBlock = '<div class="highlight_block_active">' + msg1 + ' ' + msg2 + '</div>';
	$("table#MailList").before(divBlock);
}
 
function deselectAllUid()
{
	$('table#MailList').prev().remove();
	$("input.classUid").remove();
	$("input#CheckAllBox").attr("checked","");
	$("input[name='Uid[]']").attr("checked","");
	<?php if($IMap->CacheMailToDB == true){ ?>
	$("table#MailList>tbody>tr:not(:first):not(:last)").find("td").removeClass("tabletext iMailrowSelect").addClass("tabletext iMailrow");
	<?php }else{ ?>
	$("table#MailList>tbody>tr").find("td").removeClass("tabletext iMailrowSelect").addClass("tabletext iMailrow");
	<?php } ?>
}

var date_ary = [];
function loadMails()
{
	if(date_ary.length > 0){
		var date_obj = date_ary.shift();
		var data = $('#form1').serialize();
		data += "&StartDate=" + date_obj['StartDate'] + "&EndDate=" + date_obj['EndDate'];
		$.post(
			'ajax_search.php',
			data,
			function(returnHtml){
				$('table#MailList tbody').append(returnHtml);
				//refreshTableRecords();
				getTableNavigation();
				//initHighlightRow();
				setTimeout(function(){
					loadMails();
				},10);
			}
		);
	}else{
		// loading completed
		$('tr#loadingRow').hide();
		if($('table#MailList > tbody tr').length == 0){
			$('tr#noRecordRow').show();
			$('tr#tableNavigation').hide();
		}
		initHighlightRow();
		getTableNavigation();
		setSearchResult();
	}
}

function getFormatDate(dateObj)
{
	var date_year = dateObj.getFullYear();
	var date_month = dateObj.getMonth()+1;
	var date_day = dateObj.getDate();
	date_month = date_month < 10? '0'+date_month : ''+date_month; 
	date_day = date_day < 10 ? '0'+date_day : ''+date_day; 
	var date_string = date_year + '-' + date_month + '-' + date_day;
	
	return date_string;
}


function prepareDateRange()
{
	var to_date = $('#ToDate').val();
	var from_date = $('#FromDate').val();
	var to_date_parts = to_date.split("-");
	var from_date_parts = from_date.split("-");
	//var ts_from = (new Date(from_date)).getTime(); // IE not work
	//var ts_to = (new Date(to_date)).getTime(); // IE not work
	var ts_from = (new Date(from_date_parts[0],from_date_parts[1]-1,from_date_parts[2],0,0,0,0)).getTime();
	var ts_to = (new Date(to_date_parts[0],to_date_parts[1]-1,to_date_parts[2],0,0,0,0)).getTime();
	var ms_per_day = 24 * 60 * 60 * 1000;
	var ms_per_search = 10 * ms_per_day; // milliseconds for 10 days
	for(var i=ts_to;i>=ts_from;i-=ms_per_search){
		var end_ts = i;
		var start_ts = Math.max(ts_from, i - ms_per_search + ms_per_day);
		var end_date = getFormatDate(new Date(end_ts));
		var start_date = getFormatDate(new Date(start_ts));
		date_ary.push({'StartDate':start_date,'EndDate':end_date});
	}
}

var global_sort_order = 0; // 0=ascending, 1=descending
function sortPage(order,field_index,obj)
{
<?php 
// Chrome's javascript array sort of objects is slow and buggy, use alternative to do server site sorting instead
if(/*$userBrowser->browsertype == "Chrome" && */ false /* sort with server side php */){	
?>
	Block_Element('html_body_frame');
	var row_html = $('table#MailList > tbody').html();
	var cell_index = field_index + 4;
	var order = document.getElementById('order').value;
	global_sort_order = order == 1 ? 0 : 1;
	document.getElementById('field').value = field_index;
	document.getElementById('order').value = global_sort_order;
	var table = document.getElementById('MailList');
	var tbody = table.getElementsByTagName('tbody')[0];
	var rows = tbody.getElementsByTagName('tr');
	var rowArray = new Array();
	var colArray = new Array();
	for (var i=0;i<rows.length;i++){
		var cell = rows[i].getElementsByTagName('td')[cell_index].innerHTML.replace(/(<([^>]+)>)/ig,"");
		colArray.push(cell);
		rowArray.push('<tr>'+encodeURIComponent(rows[i].innerHTML)+'</tr>');
	}
	$.post(
		'ajax_task.php',
		{
			'task': 'SortEmails',
			'rows[]': rowArray,
			'cols[]': colArray,
			'field': cell_index,
			'order': order 
		},
		function(returnHtml){
			$('table#MailList > tbody').html(returnHtml);
			
			getTableNavigation();
			initHighlightRow();
			setSearchResult();
			
			UnBlock_Element('html_body_frame');
		}
	);
<?php }else{ /* sort with client side javascript */ ?>	
	Block_Element('html_body_frame');
	
	var cell_index = field_index + 4;
	document.getElementById('field').value = field_index;
	var order = document.getElementById('order').value;
	global_sort_order = order == 1 ? 0 : 1;
	document.getElementById('order').value = global_sort_order;
	var table = document.getElementById('MailList');
	var tbody = table.getElementsByTagName('tbody')[0];
	var rows = tbody.getElementsByTagName('tr');
	var rowArray = new Array();
	for (var i=0;i<rows.length;i++) {
		var rowObj = {};
		rowObj.oldIndex = i;
		rowObj.dataType = (field_index == 4? 'number' : 'string');
		rowObj.value = rows[i].getElementsByTagName('td')[cell_index].innerHTML.replace(/(<([^>]+)>)/ig,"");
		rowArray.push(rowObj);
	}
	rowArray.sort(RowCompare);
	
	var newTbody = document.createElement('tbody');
	for (var i=0; i<rowArray.length; i++) {
		newTbody.appendChild(rows[rowArray[i].oldIndex].cloneNode(true));
	}
	table.replaceChild(newTbody, tbody);
	
	getTableNavigation();
	initHighlightRow();
	setSearchResult();
	
	UnBlock_Element('html_body_frame');
<?php }	?>
}

function RowCompare(a, b)
{
	var aVal = a.dataType == 'number'? parseInt(a.value) : a.value;
	var bVal = b.dataType == 'number'? parseInt(b.value) : b.value;
	if(aVal == bVal) return 0;
	return global_sort_order == 0? (aVal > bVal ? 1 : -1) : (aVal > bVal ? -1 : 1);
}

function refreshTableRecords()
{
	var total_row = $('table#MailList > tbody tr').length;
	var pageNo = $('#pageNo').val();
	var page_size = $('#numPerPage').val();
	var field = $('#field').val();
	var order = $('#order').val();
	
	var row_start = (pageNo-1) * page_size; // start from 0
	var row_end = Math.max(0, Math.min(pageNo * page_size, total_row)-1); // end at size-1
	var rows = $('table#MailList > tbody tr');
	for(var i=0;i<rows.length;i++){
		if(i<row_start || i > row_end){
			rows.get(i).style.display = 'none';
		}else{
			rows.get(i).style.display = '';
		}
	}
}

function getTableNavigation()
{
	var total_row = $('table#MailList > tbody tr').length;
	var pageNo = $('#pageNo').val();
	var page_size = $('#numPerPage').val();
	var field = $('#field').val();
	var order = $('#order').val();
	
	var row_start = (pageNo-1) * page_size; // start from 0
	var row_end = Math.max(0, Math.min(pageNo * page_size, total_row)-1); // end at size-1
	var rows = $('table#MailList > tbody tr');
	for(var i=0;i<rows.length;i++){
		if(i<row_start || i > row_end){
			rows.get(i).style.display = 'none';
		}else{
			rows.get(i).style.display = '';
		}
	}
	
	if(total_row == 0){
		// $('tr#tableNavigation').hide();
        $('tr#tableNavigation').css('display','');
		return;
	}
	
	var dbtable = new libdbtable();
	dbtable.column_list = '';
	dbtable.IsColOff = 'imail_gamma_search';
	dbtable.no_col = 11;
	dbtable.total_row = total_row;
	dbtable.pageNo = pageNo;
	dbtable.page_size = page_size;
	dbtable.n_start = (dbtable.pageNo-1) * dbtable.page_size;
	dbtable.title = '<?=$Lang['General']['Record']?>';
	dbtable.field = field;
	dbtable.order = order;
	dbtable.sortby = '<?=$list_sortby?>';
	dbtable.total = '<?=$list_total?>';
	dbtable.pagePrev = '<?=$list_prev?>';
	dbtable.pageNext = '<?=$list_next?>';
	dbtable.onChangeSubmit = 'getTableNavigation()';
	dbtable.form_name = 'form1';
	dbtable.pagename = 'Page';
	dbtable.pageNo_name = 'pageNo';
	dbtable.image_path = '<?=$image_path?>';
	dbtable.LAYOUT_SKIN = '<?=$LAYOUT_SKIN?>';
	dbtable.pageSizeChangeEnabled = true;
	dbtable.i_general_EachDisplay = '<?=$i_general_EachDisplay?>';
	dbtable.i_general_PerPage = '<?=$i_general_PerPage?>';
	dbtable.i_general_record = '<?=$Lang['General']['Record']?>';
	dbtable.i_email_message = '<?=$i_email_message?>';
	dbtable.i_adminmenu_group = '<?=$i_adminmenu_group?>';
	
	var pos=0;
	dbtable.column_list += '<td width="1%" class="tabletop" ><?=str_replace("'",'"',$i_frontpage_campusmail_icon_status_2007)?></td>';
	dbtable.column_list += '<td width="1%" class="tabletop" ><img align="absmiddle" src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_star_off.gif" border="0" /></td>';
	dbtable.column_list += '<td width="1%" class="tabletop" ><img align="absmiddle" src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iMail/icon_important_title.gif" border="0" /></td>';
	dbtable.column_list += '<td width="1%" class="tabletop" ><?=str_replace("'",'"',$i_frontpage_campusmail_icon_notification_2007)?></td>';
	dbtable.column_list += '<td width="15%" class="tabletop" >' + dbtable.column(pos++, '<?=$senderreceiver?>')+'</td>';
	dbtable.column_list += '<td width="60%" class="tabletop" >' + dbtable.column(pos++, '<?=$Lang['Gamma']['Subject']?>')+'</td>';
	dbtable.column_list += '<td width="100" class="tabletop" >' + dbtable.column(pos++, '<?=$i_frontpage_campusmail_date?>')+'</td>';
	dbtable.column_list += '<td width="60" class="tabletop" >' + dbtable.column(pos++, '<?=$i_frontpage_campusmail_folder?>')+'</td>';
	dbtable.column_list += '<td width="60" class="tabletop" >'+ dbtable.column(pos++, '<?=$i_frontpage_campusmail_size?>')+'</td>';
	dbtable.column_list += '<td width="25" class="tabletop" ><input type="checkbox" onclick="checkAll(this)" id="CheckAllBox"></td>';
	
	$('table#MailList > thead').html(dbtable.displayColumn());
	$('tr#tableNavigation > td').html(dbtable.navigation());
	$('tr#tableNavigation').show();
}

function setSearchResult()
{
	var postData = $('#form1').serialize();
	var uid_objs = document.getElementsByName('Uid[]');
	for(var i=0;i<uid_objs.length;i++){
		postData += '&Uid[]=' + uid_objs[i].value;
	}
	
	$.post(
		'ajax_task.php?task=SetCachedSearchResult',
		postData,
		function(returnData){
			
		}
	);
}

function onDateRangeMethodChanged(obj)
{
 	var selected_value = $(obj).val();
 	if(selected_value == 'custom'){
 		$('#SimpleStartDate').attr('disabled',false);
 		$('#SimpleStartDate').next('img').show();
 		$('#SimpleEndDate').attr('disabled',false);
 		$('#SimpleEndDate').next('img').show();
 	}else{
 		$('#SimpleStartDate').attr('disabled',true);
 		$('#SimpleStartDate').next('img').hide();
 		$('#SimpleEndDate').attr('disabled',true);
 		$('#SimpleEndDate').next('img').hide();
 		
 		var dateObj = new Date();
 		var end_date = getFormatDate(dateObj);
 		var start_date = end_date;
 		
 		if(selected_value == '1week'){
 			dateObj.setDate(dateObj.getDate()-7);
 		}else if(selected_value == '1month'){
 			dateObj.setMonth(dateObj.getMonth()-1);
 		}else if(selected_value == '3month'){
 			dateObj.setMonth(dateObj.getMonth()-3);
 		}else if(selected_value == '6month'){
 			dateObj.setMonth(dateObj.getMonth()-6);
 		}else if(selected_value == '1year'){
 			dateObj.setFullYear(dateObj.getFullYear()-1);
 		}
 		start_date = getFormatDate(dateObj);
 		
 		$('#SimpleStartDate').val(start_date);
 		$('#SimpleEndDate').val(end_date);
 	}
}

function getFormatDate(dateObj)
{
	var date_year = dateObj.getFullYear();
	var date_month = dateObj.getMonth()+1;
	var date_day = dateObj.getDate();
	date_month = date_month < 10? '0'+date_month : ''+date_month; 
	date_day = date_day < 10 ? '0'+date_day : ''+date_day; 
	var date_string = date_year + '-' + date_month + '-' + date_day;
	
	return date_string;
}

function searchSubmit(formObj)
{
	var from_date_obj = document.getElementById('SimpleStartDate');
	var to_date_obj = document.getElementById('SimpleEndDate');
	var is_valid = true;
	
	if(!check_date_without_return_msg(from_date_obj)){
		is_valid = false;
	}
	if(!check_date_without_return_msg(to_date_obj)){
		is_valid = false;
	}
	
	if(is_valid){
		if(from_date_obj.value > to_date_obj.value){
			is_valid = false;
			alert('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
			return;
		}
		
	}
	
	if(is_valid){
		$('#FromDate').val(from_date_obj.value);
		$('#ToDate').val(to_date_obj.value);
		$('input[name="DateRangeMethod"]').val($('[name="SimpleDateRangeMethod"]').val());
		$('input[name="KeywordFrom"]').val($('input[name="SimpleKeywordFrom"]').val());
		$('input[name="KeywordTo"]').val($('input[name="SimpleKeywordTo"]').val());
		$('input[name="KeywordCc"]').val($('input[name="SimpleKeywordCc"]').val());
		$('input[name="KeywordBcc"]').val($('input[name="SimpleKeywordBcc"]').val());
		$('input[name="KeywordSubject"]').val($('input[name="SimpleKeywordSubject"]').val());
		$('input[name="KeywordBody"]').val($('input[name="SimpleKeywordBody"]').val());
        $('input[name="pageNo"]').val(1);       // reset page number when search
		formObj.submit();
	}
}

$(document).ready(function(){
	function isElementInView(elem)
	{
	    var $elem = $(elem);
	    var $window = $(window);
	
	    var docViewTop = $window.scrollTop();
	    var docViewBottom = docViewTop + $window.height();
	
	    var elemTop = $elem.offset().top;
	    var elemBottom = elemTop + $elem.height();
	
	    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	}
	
	var GlobalToolbar = {
		"top":$('select[name="MailStatus"]').offset().top,
		"left":$('select[name="MailStatus"]').offset().left,
		"position":"static",
		"zIndex":$('#ToolbarTable').css('zIndex')
	};
	
	$(window).scroll(function(){
		var toolbar_table = document.getElementById('ToolbarTable');
		var $toolbar = $('#Toolbar');
		if(isElementInView(toolbar_table)){
			$toolbar.css({'position':'static', 'top':GlobalToolbar["top"]+'px', 'left':GlobalToolbar["left"]+'px', 'background-color':'#FFFFFF','border':'0px none #FFFFFF', 'zIndex':GlobalToolbar["zIndex"]});
			GlobalToolbar["position"] = "static";
		}else{
			$toolbar.css({'position':'fixed', 'top': '0px', 'left': ($(window).width() - $toolbar.width() - 50) + 'px', 'background-color':'#FFFFFF', 'border':'1px solid #CCCCCC', 'zIndex':1000});
			$toolbar.css({'top': '0px', 'left': ($(window).width() - $toolbar.width() - 50) + 'px'});
			GlobalToolbar["position"] = "fixed";
		}
	}).resize(function(){
		if(GlobalToolbar["position"]=="fixed"){
			var $toolbar = $('#Toolbar');
			$toolbar.css('left',($(window).width() - $toolbar.width() - 50) + 'px');
		}
	});
	
	setTimeout(function(){
		var date_range_method_obj = $('#SimpleDateRangeMethod').length>0? document.getElementById('SimpleDateRangeMethod') : document.getElementById('DateRangeMethod');
		onDateRangeMethodChanged(date_range_method_obj);
	},100);
	prepareDateRange();
	loadMails();
});
</script>
<?
//debug_pr($IMap->Stop_Timer());

imap_close($IMap->inbox);
###########################################################################################
$linterface->LAYOUT_STOP();
intranet_closedb();
?>