<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccess.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

$li = new libdb();

$values = "";
$delimiter = "";
for ($i=0; $i<$numToAdd; $i++)
{
     $name = trim(${"RecordName$i"});
     $email = trim(${"RecordEmail$i"});
     if ( $email == "" || !intranet_validateEmail($email))
     {
        continue;
     }
     $name = intranet_htmlspecialchars($name);
     $email = intranet_htmlspecialchars($email);
     $values .= "$delimiter ('$UserID', '$name','$email',now(), now())";
     $delimiter = ",";
}

if ($values != "")
{
    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL (OwnerID, TargetName, TargetAddress, DateInput, DateModified) VALUES $values";
    $li->db_db_query($sql);
}

intranet_closedb();
$anc = $special_option['no_anchor'] ?"":"#anc";
header("Location: addressbook.php?field=$field&order=$order&pageNo=$pageNo&keyword=$keyword&msg=1&TabID=$TabID".$anc);
?>