<?php
// page modifing by :
/*
 * 2020-05-27 (Henry): addslashes after Base64 decode EncodedMailBody
 * 2020-05-18 (Sam): Encode MailBody to Base64 to avoid WAF blocking 
 * 2016-03-01 (Carlos): Do stripslashes() for To, Cc, Bcc recipient input to remove extra backward slashes. 
 * 2015-05-29 (Carlos): replace comma separator to semi-colon separator with Replace_Display_Comma() for the To, Cc and Bcc values.
 * 2015-04-20 (Carlos): Split recipients with imap_gamma->splitEmailStringIntoArray()
 * 2013-07-26 (Carlos): Do not stripslashes() Subject and Body, use str_replace to replace \' and \"
 * 2013-02-20 (Carlos): Fix inline image path
 */
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();
intranet_opendb();

##############################################################################
	
## Use Library
$lfs = new libfilesystem();
$ldb = new libdb();
$IMap = new imap_gamma();

//$use_magic_quote = get_magic_quotes_gpc();

## Get Data 
//$DraftUID 	= (isset($uid) && $uid != "")? $uid : "";													// Original UID
$UID = (trim($_POST['MailAction']) != "")? $_POST['ActionUID']:$_POST['DraftUID'];
$PartNumber = $_POST['PartNumber']; // Mail to be replied/ forwarded
$Folder 	= (isset($Folder) && $Folder != "") ? stripslashes(urldecode($Folder)) : $SYS_CONFIG['Mail']['FolderPrefix'];// Get Current Folder
$FileID 	= (isset($FileID)) ? implode(",", $FileID) : "";								// File to be sent
$AttachPartNumber = $_POST['AttachPartNumber'];
$TNEFFile = $_POST['TNEFFile'];
$FromSearch = (isset($FromSearch) && $FromSearch != "") ? $FromSearch : 0;

$ComposeTime = $_POST['ComposeTime'];
$ComposeDatetime = date("Y-m-d H:i:s",$ComposeTime);

# Page Info Data
$pageNo  	= (isset($pageNo) && $pageNo != "") ? $pageNo : 1;
$reverse  	= (isset($reverse) && $reverse != "") ? $reverse : 1;
$sort 		= (isset($sort) && $sort != "") ? $sort : (($FromSearch == 1) ? "SortDate" : "SORTARRIVAL");	

if($FromSearch == 1){
	$IsAdvanceSearch = (isset($IsAdvanceSearch) && $IsAdvanceSearch != "") ? $IsAdvanceSearch : 1;
} else {
	$IsAdvanceSearch = '';
}

# Get Data From Simple Search
$keyword 	    = (isset($keyword) && $keyword != "") ? stripslashes(trim($keyword)) : "";

# Other Post Data
$KeywordFrom 	= (isset($KeywordFrom) && $KeywordFrom != "") ? stripslashes(trim($KeywordFrom)) : '';
$KeywordTo   	= (isset($KeywordTo) && $KeywordTo != "") ? stripslashes(trim($KeywordTo)) : '';
$KeywordCc   	= (isset($KeywordCc) && $KeywordCc != "") ? stripslashes(trim($KeywordCc)) : '';
$KeywordSubject = (isset($KeywordSubject) && $KeywordSubject != "") ? stripslashes(trim($KeywordSubject)) : '';
$FromDate  		= (isset($FromDate) && $FromDate != "") ? trim($FromDate) : '';
$ToDate  		= (isset($ToDate) && $ToDate != "") ? trim($ToDate) : '';
$SearchFolder   = (isset($SearchFolder) && is_array($SearchFolder) && count($SearchFolder) > 0) ? $SearchFolder : array();



## Initialization
$AttachList = array();


## Preparation
//$personal_path = $SYS_CONFIG['sys_user_file']."/file/mail/u".$_SESSION['SSV_USERID'];
$personal_path = "$file_path/file/gamma_mail/u$UserID";
if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}

# Get the File Attachement List - the files, which are uploaded before, are related to this email
if ($FileID != "") {
	$sql = 'Select 
				FileID, 
				OriginalFileName,
				EncodeFileName
			from 
				MAIL_ATTACH_FILE_MAP 
			where 
				FileID in ('.$FileID.')';
	$AttachList = $ldb->returnArray($sql,3);
	
	for ($i=0; $i< sizeof($AttachList); $i++) {
		$AttachList[$i][2] = $personal_path."/".$AttachList[$i][2];
		$AttachList[$i][1] = $AttachList[$i][1];
	}
}

# Get related inline image list
$personal_path = $PATH_WRT_ROOT."file/gamma_mail/u$UserID";
if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}
$personal_related_path = $personal_path."/related";
if (!is_dir($personal_related_path))
{
    $lfs->folder_new($personal_related_path);
}
$RelatedList=array();
$sql = "SELECT 
			FileID,
			OriginalImageFileName as ImageName,
			ContentID  
		FROM MAIL_INLINE_IMAGE_MAP 
		WHERE UserID = '$UserID' AND DATE_FORMAT(ComposeTime,'%Y-%m-%d %H:%i:%s') = '$ComposeDatetime'";
$RelatedFileList = $ldb->returnArray($sql,2);
for($i=0;$i<sizeof($RelatedFileList);$i++)
{
	$RelatedList[$i]['FilePath'] = $personal_related_path."/".$RelatedFileList[$i]['ContentID']."/".$RelatedFileList[$i]['ImageName'];
	$RelatedList[$i]['FileContentID'] = $RelatedFileList[$i]['ContentID'];
}

/* ----- User ----- */
if ($user->Email == $IMap->CurUserAddress) {
	$OrgAlias = $user->Get_Default_Alias();
	$From = ($OrgAlias !== false) ? $OrgAlias : $IMap->CurUserAddress;
} else {
	$From = $IMap->CurUserAddress;
}

/* ----- Mail Server ----- */
//$Perference = $IMap->Get_Preference();
//$RawSubject = stripslashes( $_POST['Subject']);
$RawSubject = str_replace(array("\'",'\"'),array("'",'"'),$_POST['Subject']);
//$Subject 	= $IMap->Get_Mail_Format($RawSubject, "UTF-8");
$Subject 	= $RawSubject;
//$To 		= Replace_Display_Comma($_POST['ToAddress']);
//$ToArray 	= explode(";", $To);
$To = $_POST['ToAddress'];
//if($use_magic_quote){
	$To = stripslashes($To);
//}
$To = Replace_Display_Comma($To);
$TmpTo = $IMap->splitEmailStringIntoArray($To);
$ToArray = $TmpTo[0];

$Cc = $_POST['CcAddress'];
//if($use_magic_quote){
	$Cc = stripslashes($Cc);
//}
$Cc = Replace_Display_Comma($Cc);
$TmpCc = $IMap->splitEmailStringIntoArray($Cc);
$CcArray = $TmpCc[0];
//$Cc 		= Replace_Display_Comma($_POST['CcAddress']);
//$CcArray 	= explode(";", $Cc);

$Bcc = $_POST['BccAddress'];
//if($use_magic_quote){
	$Bcc = stripslashes($Bcc);
//}
$Bcc = Replace_Display_Comma($Bcc);
$TmpBcc = $IMap->splitEmailStringIntoArray($Bcc);
$BccArray = $TmpBcc[0];

//$Bcc 		= Replace_Display_Comma($_POST['BccAddress']);
//$BccArray 	= explode(";", $Bcc);

//$Body = stripslashes( $_POST['MailBody']);
// $Body = str_replace(array("\'",'\"'),array("'",'"'), $_POST['MailBody']);
                                                     // WAF workaround
$Body = str_replace(array("\'",'\"'),array("'",'"'), addslashes(base64_decode($_POST['EncodedMailBody'])));
$ImportantFlag = $_POST['ImportantFlag'];

//$MimeMessage = $IMap->buildMimeText($Subject, $Body, $IMap->CurUserAddress, $ToArray, $CcArray, $BccArray, $AttachList, $ImportantFlag, true, $Perference);	// old function
$MimeMessage = $IMap->Build_Mime_Text($Subject, $Body, $From, $ToArray, $CcArray, $BccArray, $AttachList, $ImportantFlag, true, $Perference, $RelatedList);

## Main
# Delete and Check the old Draft Record
if ($DraftUID != "") {
	//20100812
	$Result['DeleteOldDraft'] = $IMap->deleteMail($IMap->DraftFolder, $DraftUID);
}

# save a copy in Draft folder
$Result['SaveDraft'] = $IMap->saveDraftCopy($MimeMessage['FullMimeMsg']);
//20100812
$NewUID = $IMap->Get_Last_Msg_UID($IMap->DraftFolder);

if (!in_array(false, $Result)) {

	# select the file list to be delete
	$sql = 'SELECT 
				FileID, 
				EncodeFileName
			FROM MAIL_ATTACH_FILE_MAP 
			WHERE 
			(
				(
					OrgDraftMailUID = \''.$DraftUID.'\' 
					AND
					DraftMailUID = -1 
					AND 
					UserID = '.$UserID.' 
				) 
				OR  
				( ';
	$sql .= ($FileID != "") ? 'FileID not in ('.$FileID.') AND ' : '';
	$sql .=	'		UserID = '.$UserID.' AND 
					(DraftMailUID IS NULL OR DraftMailUID = \''.$DraftUID.'\') 
				)
			)  AND DATE_FORMAT(ComposeTime,\'%Y-%m-%d %H:%i:%s\') = \''.$ComposeDatetime.'\' ';						
	$DeleteList = $ldb->returnArray($sql,2);
	
	# delete the file in file system
	for($i=0; $i<sizeof($DeleteList) ; $i++){
		list($FileID,$FileName) = $DeleteList[$i];
		$loc = $personal_path."/".$FileName;
		
		$lfs->file_remove($loc);
		
		// delete the file record in database
		$sql = 'DELETE FROM MAIL_ATTACH_FILE_MAP 
				WHERE FileID = \''.$DeleteList[$i][0].'\' ';
		$ldb->db_db_query($sql);
	}

	# Update the Old Draft UID to a New UID
	/*$sql = 'Update MAIL_ATTACH_FILE_MAP 
			Set
				DraftMailUID = '.$NewUID.'
			where 
				(
				DraftMailUID IS NULL 
				OR 
				DraftMailUID = \''.$DraftUID.'\'
				) 
				and 
				UserID = '.$_SESSION['SSV_USERID'];
	$ldb->db_db_query($sql);*/

	//$Msg = $Lang['ReturnMsg']['MailSavedSuccess'];
	$exmsg = 1;
	$signal = 2;
}
else {
	$Msg = $Lang['ReturnMsg']['MailSavedUnsuccess'];
}

##############################################################################
intranet_closedb();

//if($FromSearch == 1){
//	$redirect_url = ($IsAdvanceSearch == 0) ? "search_result.php" : "advance_search_result.php";
//	
//	# Page from Advance Search
//	echo '<html><body>';
//	echo $lui->Get_Form_Open("form1", "post", $redirect_url);
//	echo $lui->Get_Input_Hidden("CurTag", "CurTag", urlencode($Folder))."\n";
//	echo $lui->Get_Input_Hidden("Folder", "Folder", urlencode($Folder))."\n";
//	echo $lui->Get_Input_Hidden("reverse", "reverse", $reverse)."\n";
//	echo $lui->Get_Input_Hidden("sort", "sort", $sort)."\n";
//	echo $lui->Get_Input_Hidden("pageNo", "pageNo", $pageNo)."\n";
//	echo $lui->Get_Input_Hidden("FromSearch", "FromSearch", $FromSearch)."\n";
//	echo $lui->Get_Input_Hidden("IsAdvanceSearch", "IsAdvanceSearch", $IsAdvanceSearch)."\n";
//
//	# Hidden Values - Simple Search
//	echo $lui->Get_Input_Hidden("keyword", "keyword", $keyword)."\n";
//	
//	# Hidden Values - Advance Search Field
//	echo $lui->Get_Input_Hidden("KeywordFrom", "KeywordFrom", $KeywordFrom)."\n";
//	echo $lui->Get_Input_Hidden("KeywordTo", "KeywordTo", $KeywordTo)."\n";
//	echo $lui->Get_Input_Hidden("KeywordCc", "KeywordCc", $KeywordCc)."\n";
//	echo $lui->Get_Input_Hidden("KeywordSubject", "KeywordSubject", $KeywordSubject)."\n";
//	echo $lui->Get_Input_Hidden("FromDate", "FromDate", $FromDate)."\n";
//	echo $lui->Get_Input_Hidden("ToDate", "ToDate", $ToDate)."\n";
//	if(count($SearchFolder) > 0){
//		for ($i=0; $i < sizeof($SearchFolder); $i++) {
//			echo $lui->Get_Input_Hidden("SearchFolder[]", "SearchFolder[]", stripslashes($SearchFolder[$i]))."\n";
//		}
//	}
//	echo $lui->Get_Form_Close();
//	echo '<script type="text/javascript">';
//	echo "function submit_form(){ document.form1.submit(); }";
//	echo "submit_form();";
//	echo "</script>";
//	echo '</body></html>';
//} else {
	# Page not from Search
	//20100812
	$Folder = $IMap->DraftFolder;
	$params  = "exmsg=".$exmsg."&signal=".$signal."&CurTag=".urlencode($Folder)."&Folder=".urlencode($Folder);
	$params .= "&pageNo=$pageNo&sort=$sort&order=$reverse";
	header("Location: viewfolder.php?".$params);
	exit;
//}
?>