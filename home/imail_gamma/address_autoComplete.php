<?php
// page modifing by : Michael Cheung
$PathRelative = "../../../../";
$CurSubFunction = "Communication";

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}
AuthPage(array("Communication-Mail-GeneralUsage"));

include_once($PathRelative."src/include/class/database.php");
include_once($PathRelative."src/include/class/imap_gamma_ui.php");
include_once($PathRelative."src/include/class/imap_address.php");

# Get the Key to Search
$Key = trim($_POST['Key']);
$CompletePlace = $_POST['ReturnType'];

# Preparation
if($CompletePlace == "") {
	$InputName = 'ToAddress';
	$HideDivID = 'to_list';
	$AddressListID = 'ToEmailList';
}
else if($CompletePlace == "CC") {
	$InputName = 'CcAddress';
	$HideDivID = 'cc_list';
	$AddressListID = 'CcEmailList';
}
else if($CompletePlace == "BCC") {
	$InputName = 'BccAddress';
	$HideDivID = 'bcc_list';
	$AddressListID = 'BccEmailList';
}

# Use Library
$ldb = new database();

// create esfc foundation database ; 2009-08-04
//$esfdb= new database(false, false, true);
$ESFDBName = $SYS_CONFIG['ESF']['DB']['DBName'];
$lui = new imap_gamma_ui();
$ia = new imap_address();
// Get Autocomplete list Array
/* used an SP to replace the code
$sql = 'SELECT 
					UserName, 
					\' &lt;\'+ UserEmail + \'&gt;\' 
				FROM 
					MAIL_ADDRESS_USER 
				WHERE 
					UserID = '.$_SESSION['SSV_USERID'];
$PersonalEmailArray = $ldb->returnArray($sql,2);

$sql = 'EXEC Get_AllStaffEmail';
$StaffEmailArray = $ldb->returnArray($sql,2);

$sql = 'EXEC Get_AllStudentEmail';
$StudentEmailArray = $ldb->returnArray($sql,2);

$FinalEmailArray = array_merge($PersonalEmailArray,$StaffEmailArray,$StudentEmailArray);

for ($i = 0; $i < sizeof($FinalEmailArray); $i++) {
	list($UserName, $Email)  = $FinalEmailArray[$i];
	if (stristr($UserName,$Key) != false || stristr($Email,$Key) != false) {
		$DisplayEmailArray[] = $FinalEmailArray[$i];
	}
}
*/

$sp = "exec Get_StudentStaffEmail_ByNameEmail ";
$sp .= "@UserID=" . $_SESSION['SSV_USERID'] . ", ";
$sp .= "@Name='" . $ldb->Get_Safe_Sql_Query($Key) . "' ";
if ($SYS_CONFIG['SchoolCode'] == "ESFC") {
	$sp .= ", @HideStudent=1";
}
if ($UserType != "") {
	$sp .= ", @UserType='" . $UserType . "'";
}
if(Auth(array("MySchool-TeachingStaff"), "TARGET") OR Auth(array("MySchool-TeachingStaffAll"), "TARGET")) {
	$sp .= ", @TeachStaff = 1";	
}
if(Auth(array("MySchool-NonTeachingStaff"), "TARGET")) {
	$sp .= ", @NonTeachStaff = 1";	
}
if(Auth(array("MySchool-Student"), "TARGET")) {
	$sp .= ", @Student = 1";	
}
if(Auth(array("MySchool-SchoolBasedGroup"), "TARGET")) {
	$sp .= ", @SchoolBasedGroup = 1";	
}

// echo 'SP : '.$sp.'<br>';

$DisplayEmailArray = array();
$DisplayEmailArray = $ldb->returnArray($sp, 4);

if(Auth(array("Foundation-FoundationBasedGroup"), "TARGET"))
{
// Select foundation groups -- 2009-08-04
$esfdb = new database(false, false, true);
$spesf = "exec $ESFDBName.dbo.Get_FoundationStudentStaffEmail_ByNameEmail ";
$spesf .= "@UserID=" . $_SESSION['SSV_USERID'] . ", ";
$spesf .= "@Name='" . $esfdb->Get_Safe_Sql_Query($Key) . "' ";
$spesf .= ", @UserType='F'";
//if(Auth(array("Foundation-FoundationBasedGroup"), "TARGET")) {
	$spesf .= ", @FoundationBasedGroup = 1";	
//}

$DisplayFoundationEmailArray= $esfdb->returnArray($spesf, 4);
$totalFoundationRecords=count($DisplayFoundationEmailArray);
if( $totalFoundationRecords > 0)
{
	for($k=0;$k<$totalFoundationRecords;$k++)
	{
		array_push($DisplayEmailArray, $DisplayFoundationEmailArray[$k]);
	}
}
// End select foundation groups
}

if (sizeof($DisplayEmailArray) == 0)
{
	echo "";
	return;
}


$GroupSelection = "	
							SELECT 
								a.MailGroupID, b.UserName, b.MailUserID, 
								CASE 
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') = '' THEN RTRIM(LTRIM(b.UserEmail)) collate Latin1_General_CI_AS 
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') != '' THEN RTRIM(LTRIM(i.Email)) collate Latin1_General_CI_AS 
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') != '' THEN RTRIM(LTRIM(i.EmailAlias1)) collate Latin1_General_CI_AS 
								END AS 'UserEmail'							
							FROM
								MAIL_ADDRESS_MAPPING a
							INNER JOIN
								MAIL_ADDRESS_USER b ON a.MailUserID = b.MailUserID
							LEFT OUTER JOIN intranet_user i ON b.IntranetUserID = i.UserID							
							WHERE 							
								(b.UserType = 'S' OR (b.UserType = 'U' AND b.UserID = '".$_SESSION['SSV_USERID'] ."'))
						";
$ReturnGroupArr = $ldb->returnArray($GroupSelection, 4);

$GroupArr = array();
for ($i=0;$i<count($ReturnGroupArr);$i++)
{
	$TmpGroupID = $ReturnGroupArr[$i][0];
	$TmpName = $ReturnGroupArr[$i][1];
	$TmpEmail = $ReturnGroupArr[$i][3];
	$GroupArr[$TmpGroupID][] = array($TmpName,$TmpEmail);
}

if(Auth(array("Foundation-FoundationBasedGroup"), "TARGET"))
{
// select names and mails from foundation groups -- 2009-08-04
/*$FoundationGroupSelection = "SELECT MailGroupID, max(UserName), max(MailUserID), UserEmail 
					FROM (
							SELECT 
								a.MailGroupID, b.UserName, b.MailUserID, 
								CASE 
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') = '' THEN RTRIM(LTRIM(b.UserEmail)) collate Latin1_General_CI_AS 
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') != '' THEN RTRIM(LTRIM(i.Email)) collate Latin1_General_CI_AS 
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') != '' THEN RTRIM(LTRIM(i.EmailAlias1)) collate Latin1_General_CI_AS 
								END AS 'UserEmail'							
							FROM
								$ESFDBName.dbo.MAIL_ADDRESS_Group m
							INNER JOIN 
								$ESFDBName.dbo.MAIL_ADDRESS_MAPPING a ON m.MailGroupID = a.MailGroupID
							INNER JOIN
								$ESFDBName.dbo.MAIL_ADDRESS_USER b ON a.MailUserID = b.MailUserID
							LEFT OUTER JOIN $ESFDBName.dbo.intranet_user i ON b.IntranetUserID = i.UserID							
							WHERE 							
								m.GroupType = 'F' OR (b.UserType = 'F' OR (b.UserType = 'F' AND b.UserID = '".$_SESSION['SSV_USERID'] ."'))
						) As p
					GROUP BY MailGroupID, UserEmail ";*/
					
$Sql = "SELECT b.IntranetUserID AS IntranetUserID, 
		b.IntranetUserSchoolCode AS IntranetUserSchoolCode,
		b.MailUserID AS MailUserID
		FROM $ESFDBName.dbo.MAIL_ADDRESS_MAPPING AS a
		INNER JOIN $ESFDBName.dbo.MAIL_ADDRESS_USER AS b ON a.MailUserID = b.MailUserID
		WHERE b.UserType = 'F'";
		
$LibESF = new database(false,false,true);
$Result = $LibESF->returnArray($Sql);

for ($i=0;$i<count($Result);$i++){
	if($Result[$i]['IntranetUserID']==NULL){
		$AllFoundationSql1 .= "SELECT c.MailGroupID, b.UserName, b.MailUserID,
								b.UserEmail AS 'Email'
								FROM $ESFDBName.dbo.MAIL_ADDRESS_USER AS b 
								INNER JOIN $ESFDBName.dbo.MAIL_ADDRESS_MAPPING AS c ON c.MailUserID = b.MailUserID 
								WHERE b.MailUserID = ".$Result[$i]['MailUserID']." 
								UNION ";
		
	}
	else{
		$dbCode = $ia->Get_DB_Code($Result[$i]['IntranetUserSchoolCode']);
		
		$AllFoundationSql2 .= "SELECT c.MailGroupID, i.OfficialFullName AS UserName, b.MailUserID, 
								CASE
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') = '' 
									THEN RTRIM(LTRIM(b.UserEmail)) collate Latin1_General_CI_AS
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') = '' AND isnull(RTRIM(LTRIM(i.Email)), '') != '' 
									THEN RTRIM(LTRIM(i.Email)) collate Latin1_General_CI_AS 
									WHEN isnull(RTRIM(LTRIM(i.EmailAlias1)), '') != '' 
									THEN RTRIM(LTRIM(i.EmailAlias1)) collate Latin1_General_CI_AS 
								END AS 'Email',
								b.MailUserID AS UserID
								FROM $ESFDBName.dbo.MAIL_ADDRESS_USER AS b
								INNER JOIN $ESFDBName.dbo.MAIL_ADDRESS_MAPPING AS c ON c.MailUserID = b.MailUserID
								INNER JOIN ".$dbCode.".dbo.intranet_user AS i ON b.IntranetUserID = i.UserID
								WHERE b.MailUserID = ".$Result[$i]['MailUserID']." 
								UNION ";
	}
}
$FoundationGroupSelection1 = substr($AllFoundationSql1,0,strlen($AllFoundationSql1)-6);
$FoundationGroupSelection2 = substr($AllFoundationSql2,0,strlen($AllFoundationSql2)-6);
			
$esfdb= new database(false, false, true);
$ReturnFoundationGroupArr1 = $esfdb->returnArray($FoundationGroupSelection1, 4);
$ReturnFoundationGroupArr2 = $esfdb->returnArray($FoundationGroupSelection2, 4);

$ReturnFoundationGroupArr = array_merge($ReturnFoundationGroupArr1, $ReturnFoundationGroupArr2);

for ($m=0;$m<count($ReturnFoundationGroupArr);$m++)
{
	$TmpGroupID = $ReturnFoundationGroupArr[$m][0];
	$TmpName = $ReturnFoundationGroupArr[$m][1];
	$TmpEmail = $ReturnFoundationGroupArr[$m][3];
	$TmpPair = array($TmpName,$TmpEmail);
	#if(is_array($GroupArr[$TmpGroupID]))
	#{
		#if(!in_array($TmpPair, $GroupArr[$TmpGroupID]) )
		#{
			$GroupArr[$TmpGroupID][] = $TmpPair;
		#}
	#}
}
// End select names and mails from foundation groups
}

echo $lui->Get_List_Open();


$RealNum = 0;
for ($i = 0; $i < sizeof($DisplayEmailArray); $i++) 
{
	$TmpEmailType = $DisplayEmailArray[$i][3];
	
	if ($TmpEmailType == "U")
	{
		if (trim($DisplayEmailArray[$i][0]) == "")
		{
			$value = $DisplayEmailArray[$i][1];
		} 
		else
		{
			$value = '"';
			$value .= $DisplayEmailArray[$i][0];
			$value .= '" &lt;';
			$value .= $DisplayEmailArray[$i][1];
			$value .= '&gt;';
		}
		$x = $value;
		
		//echo $lui->Get_List_Member($x, 'id="'.$AddressListID. $i . '" onmouseover="HighlightAddress(\''.$AddressListID.'\', SelectedEmailID, '.$i.')" onmouseout="ResetAddressHighlight(\''.$AddressListID.'\')" onmousedown="EmailListFocus=false;AddEmailAddress(\''.$InputName.'\', \''.$HideDivID.'\', \''.$AddressListID.'\', '.$i.');return false"');
		echo $lui->Get_List_Member($x, 'id="'.$AddressListID. $RealNum . '" onmouseover="HighlightAddress(\''.$AddressListID.'\', SelectedEmailID, '.$RealNum.')" onmouseout="ResetAddressHighlight(\''.$AddressListID.'\')" onmousedown="EmailListFocus=false;AddEmailAddress(\''.$InputName.'\', \''.$HideDivID.'\', \''.$AddressListID.'\', '.$RealNum.');return false"');
		$RealNum++;
	}
	else 
	{
		// if ( count($GroupArr[$DisplayEmailArray[$i][1]]) > 0)
		// {
			$TmpOutputValue = "";
			if ( count($GroupArr[$DisplayEmailArray[$i][1]]) > 0)
			{
				for ($j=0;$j<count($GroupArr[$DisplayEmailArray[$i][1]]);$j++)
				{
					if (trim($GroupArr[$DisplayEmailArray[$i][1]][$j][0]) == "")
					{
						$value = $GroupArr[$DisplayEmailArray[$i][1]][$j][1];
					}
					else
					{
						$value = '&quot;';
						$value .= $GroupArr[$DisplayEmailArray[$i][1]][$j][0];
						$value .= '&quot; &lt;';
						$value .= $GroupArr[$DisplayEmailArray[$i][1]][$j][1];
						$value .= '&gt;';
					}
					
					$value = str_replace("'","&#39;",$value);
					if ($TmpOutputValue == "")
					{
						$TmpOutputValue .= $value;
					}
					else
					{
						$TmpOutputValue .= ";".$value;
					}					
				}
			}
			
			$x = $DisplayEmailArray[$i][0];				
			$x2 = $DisplayEmailArray[$i][0]."[".$TmpOutputValue."]";
			
			//echo $lui->Get_List_Member($x, 'realContent="'.$x2.'" id="'.$AddressListID. $i . '" onmouseover="HighlightAddress(\''.$AddressListID.'\', SelectedEmailID, '.$i.')" onmouseout="ResetAddressHighlight(\''.$AddressListID.'\')" onmousedown="EmailListFocus=false;AddGroupAddress(\''.$InputName.'\', \''.$HideDivID.'\', \''.$AddressListID.'\', '.$i.');return false"');		
			echo $lui->Get_List_Member($x, 'realContent="'.$x2.'" isGroup="1" id="'.$AddressListID.$RealNum. '" onmouseover="HighlightAddress(\''.$AddressListID.'\', SelectedEmailID, '.$RealNum.')" onmouseout="ResetAddressHighlight(\''.$AddressListID.'\')" onmousedown="EmailListFocus=false;AddGroupAddress(\''.$InputName.'\', \''.$HideDivID.'\', \''.$AddressListID.'\', '.$RealNum.');return false"');		
			$RealNum++;
		// }
	}
	
	//$x = $lui->Get_HyperLink_Open("#", "onclick='AddAddress(\'".$InputName."\',\'".$value."\')'");
	//$x = $lui->Get_HyperLink_Open("#", 'onclick="AddAddress(\''.$InputName.'\', \''.$value.'\');return false"');
	
	//$x = $lui->Get_HyperLink_Open("#", 'onmousedown="AddEmailAddress(\''.$InputName.'\', \''.$DisplayEmailArray[$i][0].'\', \''.$DisplayEmailArray[$i][1].'\', \'' . $HideDivID . '\', \''.$Key.'\');return false"');
	
	//$x .= $lui->Get_HyperLink_Close();
	
	
}
echo $lui->Get_List_Close();

intranet_closedb();
?>