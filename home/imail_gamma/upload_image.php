<?php
// page modifing by: 
/*********************************** Change log ******************************************
 * 2012-05-07 (Carlos): modified inline image path - add contentID as subfolder
 */
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

set_time_limit(0);
intranet_auth();
intranet_opendb();
auth_campusmail();

################################################################################################

## Use Library
$ldb = new libdb();
$lfs = new libfilesystem();
$lui = new interface_html();

## Initization
$MessageUID = '';

## Get Data
$MailAction = (isset($_POST['MailAction']) && $_POST['MailAction'] != "") ? $_POST['MailAction'] : "";
$DraftUID   = (isset($_POST['DraftUID']) && $_POST['DraftUID'] != "") ? $_POST['DraftUID'] : "";
$ActionUID  = (isset($_POST['ActionUID']) && $_POST['ActionUID'] != "") ? $_POST['ActionUID'] : "";
$MessageUID = ($MailAction == "Forward") ? $ActionUID : $DraftUID;

$ComposeTime = $_POST['ComposeTime'];
$ComposeDatetime = date("Y-m-d H:i:s",$ComposeTime);

## Preparation
$ImgID = md5("cid".$ImageInputNumber."_".time());

$FileNumber=0;
$personal_path = $PATH_WRT_ROOT."file/gamma_mail/u$UserID";
if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}
$personal_path .= "/related";
if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}
$personal_path .= "/".$ImgID;
if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}

$loc = $_FILES['userfile_image'.$ImageInputNumber];

$FileName = stripslashes($loc['name']);
$TempName = $loc['tmp_name'];
$FullFilePath = "$personal_path/".$FileName;

## Main
if(is_file($TempName) && @exif_imagetype($TempName))
{
	$lfs->lfs_copy($TempName, $FullFilePath);
	$FileSize = (round(filesize($FullFilePath)/1024,2));
	//$ImgID = "cid".$ImageInputNumber."_".time();
//	$ImgID = md5("cid".$ImageInputNumber."_".time());
	$sql = 'Insert into MAIL_INLINE_IMAGE_MAP 
				(UserID, UploadTime, OriginalImageFileName, ContentID, ComposeTime) 
			Values 
				(\''.$UserID.'\',NOW(),\''.$ldb->Get_Safe_Sql_Query($FileName).'\',\''.$ImgID.'\',\''.$ComposeDatetime.'\') ';
	
	$ldb->db_db_query($sql);
	$ImageFileID = $ldb->db_insert_id();
}

if(file_exists($FullFilePath))
{
?>	
<script language="javascript">
par = window.parent;
var oEditor = par.FCKeditorAPI.GetInstance("MailBody");
oEditor.InsertHtml('<img src="<?="/".$FullFilePath?>" title="<?=$FileName?>" />');
var image_span = par.document.getElementById("ImageFile"+<?=$ImageInputNumber?>);
image_span.innerHTML = '';
image_span.style.display = "none";
var mail_image_list = par.document.getElementById("MailImageList");
var image_data = '<input type="hidden" name="ImageFileSize[]" value="<?=$FileSize?>" />';
image_data += '<input type="hidden" name="ImageFileID[]" value="<?="/".$FullFilePath?>" />';
mail_image_list.innerHTML += image_data;
par.Check_Total_Filesize(0);
</script>
<?
}else
{
?>
<script language="javascript">
par = window.parent;
var image_span = par.document.getElementById("ImageFile"+<?=$ImageInputNumber?>);
image_span.innerHTML = '<p><?=htmlspecialchars($FileName." ".$Lang['Gamma']['UploadFail'],ENT_QUOTES)?></p>';
</script>
<?
}
################################################################################################
intranet_closedb();
?>