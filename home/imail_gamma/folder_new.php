<?php
// using by 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
//include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

$IMap = new imap_gamma();

//$lc = new libcampusmail();
//$lwebmail = new libwebmail();
//
//
//### For new Interface ###
//$folder_sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID = $UserID OR RecordType = 0 ORDER BY RecordType, FolderName";
//$folders = $lc->returnArray($folder_sql,2);
//$folders[] = array(-1,$i_admintitle_im_campusmail_trash);

$CurrentPage = "PageCheckMail_FolderManager";

$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();

# tag information
$iMailImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_folder_open.gif' width='20' height='20' align='absmiddle' >";
$iMailTitle1 = "<span class='imailpagetitle'>{$i_CampusMail_New_FolderManager}</span>";
$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$iMailImage1.$iMailTitle1.$unreadLink."</td><td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>";
$TAGS_OBJ[] = array($iMailTitle, "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

if ($err == 1)
{
    $err_msg = "<font color='red' >$i_CampusMail_New_Prompt_FolderExist</font>";
    $xmsg = $linterface->GET_SYS_MSG("",$i_CampusMail_New_Prompt_FolderExist);    
}

if ($intranet_session_language == "en")
{
	$PAGE_NAVIGATION[] = array($button_new." ".$i_frontpage_campusmail_folder, "");
} else {
	$PAGE_NAVIGATION[] = array($button_new.$i_frontpage_campusmail_folder, "");
}

##################

?>
<script language="javascript">
function checkform(obj)
{
	//alert(obj.FolderName.value);
	if (!check_text(obj.FolderName,'<?="$i_alert_pleasefillin $i_CampusMail_New_FolderName"?>')) return false;
	if (obj.FolderName.value.lastIndexOf(".")>0 || obj.FolderName.value.lastIndexOf("/")>0
	   || obj.FolderName.value.lastIndexOf("+")>0 ) 
	{
		alert('<?=$Lang['Gamma']['FolderNameContainDots']?>');
		return false;
	}
}
</script>
<br>
<form name="form1" action="folder_new_update.php" onSubmit="return checkform(this);" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$xmsg?></td>
</tr>
<tr>
	<td colspan="2" >
	<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">	
	<tr>
		<td >
		<br />
		<table width="100%" align="center' border="0">
		<tr>
			<td class="formfieldtitle tabletext" width="30%" ><?=$i_CampusMail_New_FolderName?></td>
			<td  >
			<input type="text" name="FolderName" class="textboxtext" maxlength="255" />
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align="left" class="tabletextremark"><span class="tabletextrequire">*</span> <?=$Lang['Gamma']['FolderNameContainDots']?></td>
	</tr>
	</table>
	</td>
</tr>
<tr >
	<td colspan="2">
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="dotline">
		<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1" />
		</td>
	</tr>
	<tr>
		<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='folder.php'") ?>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
</form>

<?php
	echo $linterface->FOCUS_ON_LOAD("form1.FolderName");
	
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>