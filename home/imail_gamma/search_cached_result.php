<?php
// editing by Carlos
############################### Change Log #################################
# 2010-10-07 by Carlos : added data column Size
############################################################################
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$IMap = new imap_gamma();
$linterface = new interface_html("imail_default.html");
//$IMap->Start_Timer();
## Get Data
$KeywordFrom 	= (isset($KeywordFrom) && $KeywordFrom != "") ? stripslashes(trim($KeywordFrom)) : "";
$KeywordTo		= (isset($KeywordTo) && $KeywordTo != "") ? stripslashes(trim($KeywordTo)) : "";
$KeywordCc 		= (isset($KeywordCc) && $KeywordCc != "") ? stripslashes(trim($KeywordCc)) : "";
$KeywordBcc 	= (isset($KeywordBcc) && $KeywordBcc != "") ? stripslashes(trim($KeywordBcc)) : "";
$KeywordSubject = (isset($KeywordSubject) && $KeywordSubject != "") ? stripslashes(trim($KeywordSubject)) : "";
$KeywordBody 	= (isset($KeywordBody) && $KeywordBody != "") ? stripslashes(trim($KeywordBody)) : "";
$KeywordAttachment = (isset($KeywordAttachment) && $KeywordAttachment != "") ? stripslashes(trim($KeywordAttachment)) : "";
$FromDate 		= (isset($FromDate) && $FromDate != "") ? trim($FromDate) : "";
$ToDate 		= (isset($ToDate) && $ToDate != "") ? trim($ToDate) : "";
$Folder 		= (isset($Folder))? stripslashes($Folder) : $SYS_CONFIG['Mail']['FolderPrefix'];

# get Folder to search
$SearchFolder 	= (isset($SearchFolder) && count($SearchFolder) > 0) ? $SearchFolder : $IMap->getMailFolders();
$SearchFolderHidden = '';
for ($i=0; $i <sizeof($SearchFolder) ; $i++) {
	$SearchFolderHidden .= '<input type="hidden" id="SearchFolder[]" name="SearchFolder[]" value="'.stripslashes($SearchFolder[$i]).'">';
}

# Page Info Data
//$sort 		 = (isset($sort) && $sort != "") ? $sort : "DateSort";		// array sort fields: DateSort, From, Subject, Size, To, Folder
$reverse 	 = (isset($order) && $order != "") ? $order : 1;		// $order
$pageNo 	 = (isset($pageNo) && $pageNo != "") ? $pageNo : 1;
$numPerPage  = (isset($numPerPage) && $numPerPage != "") ? $numPerPage : 0;

# Update the NumPerPage of User Preference
//if($numPerPageOld != "" && $numPerPageOld > 0){
//	if($numPerPage != $numPerPageOld){
//		if($numPerPage != "" && $numPerPage > 0){
//			$UPref->Set_User_Mail_Per_Page($numPerPage);
//		} else {
//			$numPerPage = $numPerPageOld;
//		}
//	}
//} else {
//	$numPerPage = 50;
//}


## Initialization
$CurTag 	= $Folder;
$CurMenu    = 1;
$SearchInfo = array();
$imapInfo   = array();

## Preparation
$FolderArray = $IMap->Get_Folder_Structure(true);		// get Folder Structure for select box

$SearchInfo['keyword'] 	    =  stripslashes(trim($keyword));
//$SearchInfo['Folder'][] 	= $Folder;
$SearchInfo['From'] 	= $KeywordFrom;
$SearchInfo['To'] 		= $KeywordTo;
$SearchInfo['Cc'] 		= $KeywordCc;
$SearchInfo['Bcc'] 		= $KeywordBcc;
$SearchInfo['Subject']  = $KeywordSubject;
$SearchInfo['FromDate'] = $FromDate;
$SearchInfo['ToDate'] 	= $ToDate;
$SearchInfo['Body'] = $KeywordBody;
$SearchInfo['Attachment'] = $KeywordAttachment;
$SearchInfo['Folder'] 	= $SearchFolder;
//$SearchInfo['Sort'] 	= $sort;
//$SearchInfo['Reverse'] 	= $reverse;

##############################################################################
# Imap use only
//$imap_obj 	   = imap_check($IMap->inbox);
//$unseen_status = imap_status($IMap->inbox, "{".$IMap->host."}".$Folder, SA_UNSEEN);
//$statusInbox   = imap_status($IMap->inbox, "{".$IMap->host."}Inbox", SA_UNSEEN);

//$imapInfo['imap_obj'] 	   = $imap_obj;
//$imapInfo['unseen_status'] = $unseen_status;
//$imapInfo['statusInbox']   = $statusInbox;
##############################################################################

 	

################## dbtable settings  ####################
$default_field = 2; // date 
if($field=="") $field = $default_field;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
    $pageNo = 1;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$numPerPage = ($numPerPage != "") ? $numPerPage :$ck_page_size;
$pageSize = ($numPerPage != "") ? $numPerPage : 20;		# default 20
$order 	 = (isset($order) && $order != "") ? $order : 1;
$pageNo 	 = (isset($pageNo) && $pageNo != "") ? $pageNo : 1;
$numPerPage  = (isset($numPerPage) && $numPerPage != "") ? $numPerPage : $pageSize;
$currentPage = (isset($currentPage) && $currentPage != "") ? $currentPage : 0;

$li = new libdbtable2007($field, $order, $pageNo);



//$li->IsColOff = "imail_gamma_search";
$li->field_array= array("SenderEmail","Subject","MailDate","Folder","Size");

$senderreceiver = ($isDraftInbox||$isOutbox)?$i_frontpage_campusmail_recipients:$i_frontpage_campusmail_sender;

	$pos=0;
	$li->column_list .= "<td width='1%' class='tabletop' >". $i_frontpage_campusmail_icon_status_2007."</td>\n";
	$li->column_list .= "<td width='1%' class='tabletop' ><img align='absmiddle' src='$image_path/$LAYOUT_SKIN/icon_star_off.gif' border='0' /></td>\n";
	$li->column_list .= "<td width='1%' class='tabletop' ><img align='absmiddle' src='$image_path/$LAYOUT_SKIN/iMail/icon_important_title.gif' border='0' /></td>\n";
	$li->column_list .= "<td width='1%' class='tabletop' >". $i_frontpage_campusmail_icon_notification_2007."</td>\n";
	$li->column_list .= "<td width='150' class='tabletop' >".$li->column($pos++, $senderreceiver)."</td>\n";
	$li->column_list .= "<td width='60%' class='tabletop' >".$li->column($pos++, $Lang['Gamma']['Subject'])."</td>\n";
	$li->column_list .= "<td width='100' class='tabletop' >".$li->column($pos++, $i_frontpage_campusmail_date)."</td>\n";
	$li->column_list .= "<td width='60' class='tabletop' >".$li->column($pos++, $i_frontpage_campusmail_folder)."</td>\n";
	$li->column_list .= "<td width='60' class='tabletop' >".$li->column($pos++, $i_frontpage_campusmail_size)."</td>\n";
	$li->column_list .= "<td width='25' class='tabletop' ><input type='checkbox' onclick='checkAll(this)' id='CheckAllBox'></td>\n";

$li->no_col = 11;

$sql = "SELECT 
			
			SenderEmail,
			Subject,
			MailDate,
			Folder,
			MailSize,
			CONCAT('<input type=\"checkbox\" name=\"Uid[]\" value=\"',UID,',',Folder,'\" />') as Checkbox 
		MAIL_CACHED_MAILS 
		WHERE 
			UserEmail = '".$IMap->CurUserAddress."' 
			AND ";

$li->sql = $sql;
################## dbtable settings end ####################



##################### gen tool bar start ###################
#
# back btn

# Search bar
$spacer = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" />';
if(!$FromSearch)
{
$searchbar = '<form id="SearchForm" name="SearchForm" method="GET" action="search_result.php">';
	$searchbar .= $linterface->Get_Search_Box_Div("keyword", ($keyword?stripslashes(trim($keyword)):"Search on Subject"),' onfocus="if (this.value == \'Search on Subject\') this.value=\'\';"');
	$searchbar .= '<input type="hidden" name="displayMode" id="displayMode" value="search">';
	$searchbar .= '<input type="hidden" name="Folder" id="Folder" value="'.urlencode($Folder).'">';
	$searchbar .= '<input type="hidden" name="SearchFolder[]" value="'.$Folder.'" />';
	$searchbar .= '<input type="hidden" name="Folder" value="'.$Folder.'" />';
	$searchbar .= '<input type="hidden" name="page_from" value="viewfolder.php" />';
	$searchbar .= '<input type="hidden" name="mailbox" value="'.$Folder.'" />';
$searchbar .= '</form>';
}
else
{
	$http_var = $_SERVER["QUERY_STRING"];
	$url = $FromSearch?"advance_search.php?$http_var":"viewfolder.php?Folder=urlencode($Folder)";
	$backBtn  = "<a href=\"$url\" class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('prevp','','{$image_path}/{$LAYOUT_SKIN}/icon_prev_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">";
	$backBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_prev_off.gif\" name=\"prevp\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"prevp\"> {$button_back}</a>";
}

# Move to Selection
$MoveTo = $IMap->getMoveToSelection("targetFolder","");

# deleteBtn
$deleteBtn  = "<a href=\"javascript:checkRemove(document.form1,'Uid[]','remove.php')\" class=\"tabletool\">";
$deleteBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\" />";
$deleteBtn .= $button_remove."</a>";

# reportSpam/ report non spam btn
//20100812 
$confirmMsg = $Folder==$IMap->SpamFolder?$Lang['Gamma']['ConfirmMsg']['reportNonSpam']:$Lang['Gamma']['ConfirmMsg']['reportSpam'];
$BtnName = $Folder==$IMap->SpamFolder?$Lang['Gamma']['reportNonSpam']:$Lang['Gamma']['reportSpam'];
if($webmail_info['bl_spam']==true){
	$ReportSpamBtn  = "<a href=\"javascript:checkAlert(document.form1,'Uid[]','report_spam.php','$confirmMsg')\" class=\"tabletool\">";
	$ReportSpamBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_spam.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\" />";
	$ReportSpamBtn .= $BtnName."</a>"; 
}else{
	$ReportSpamBtn = "";
}
# $unseenfilter
foreach($Lang['Gamma']['UnseenStatus'] as $key => $value)
	$StatusAry[] = array($key,$value);
$unseenfilter = getSelectByArray($StatusAry,"name='MailStatus' onchange='this.form.submit()'",$MailStatus,0,1);

# mark mail as read / unread
array_shift($StatusAry);
$MarkAsSelection = getSelectByArray($StatusAry,"name='MarkAs' onchange='this.form.action=\"mark_read_unread.php\"; this.form.submit()'",'',0,0,$Lang['Gamma']['MarkAs']);
$MarkAsSelection = getSelectByArray($StatusAry,"name='MarkAs' onchange='MarkMailAs(this)'",'',0,0,$Lang['Gamma']['MarkAs']);

#	
##################### gen tool bar end ###################

##################### Module Obj ####################
$iMailTitle1 = "<span class='imailpagetitle'>{$i_CampusMail_New_MailSearch_SearchResult}</span>";

if($FromSearch) $Folder='';
//20100812
switch(trim($Folder))
{
	case $IMap->SentFolder	:	
		$CurrentPage = "PageCheckMail_Outbox";		break;
	
	case $IMap->DraftFolder	:	
		$CurrentPage = "PageCheckMail_Draft";		break;
	
	case $IMap->TrashFolder	:	
		$CurrentPage = "PageCheckMail_Trash";		break;
	
	case $IMap->SpamFolder	:	
		$CurrentPage = "PageCheckMail_Spam";		break;
	
	case $IMap->InboxFolder		:	
		$CurrentPage = "PageCheckMail_Inbox";		break;
	default:
}
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();

$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$iMailTitle1."</td><td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>";
$TAGS_OBJ[] = array($iMailTitle, "", 0);

$linterface->LAYOUT_START();


?>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">	
	<?=$xmsg?>
	<tr>
		<td >
		<br />
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td><?=$backBtn?><?=$searchbar?></td>
					</tr>
				</table>
			</td>		
			<td align="right" >
			<form name="form1" method="get" >
			<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td><?=$unseenfilter?></td>
				<td><?=$MoveTo?></td>
				<td><?=$MarkAsSelection?></td>
				<td><?=$deleteBtn?></td>
				<td><?=$ReportSpamBtn?></td>
			</tr>
			</table>
			</td>
		</tr>
		</table>		
		</td>
	</tr>
	
	<tr>
		
		<td width="100%" >
		<?=$li->display()?>
		<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
		<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
		<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
		<input type="hidden" name="page_size_change" value="" />
		<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
		<!--<input type="hidden" name="FolderID" value="<?=$FolderID?>" />-->
		<input type="hidden" name="Folder" value="<?=$Folder?>" />
		<input type="hidden" name="page_from" value="viewfolder.php" />
		<input type="hidden" name="mailbox" value="<?=$Folder?>" />
		<!--<input type="hidden" name="sort" value="<?=$sort?>" />-->
		
		<input type="hidden" name="keyword" value="<?=$keyword?>" />
		<input type="hidden" name="FromSearch" value="<?=$FromSearch?>" />
		<input type="hidden" name="KeywordFrom" value="<?=$KeywordFrom?>" />
		<input type="hidden" name="KeywordTo" value="<?=$KeywordTo?>" />
		<input type="hidden" name="KeywordCc" value="<?=$KeywordCc?>" />
		<input type="hidden" name="KeywordBcc" value="<?=$KeywordBcc?>" />
		<input type="hidden" name="KeywordSubject" value="<?=$KeywordSubject?>" />
		<input type="hidden" name="KeywordBody" value="<?=$KeywordBody?>" />
		<input type="hidden" name="FromDate" value="<?=$FromDate?>" />
		<input type="hidden" name="ToDate" value="<?=$ToDate?>" />
		<?=$SearchFolderHidden?>
		
		</form>
		</td>
	</tr>
	</table>
	<br />
	</td>
</tr>
</table>

<script language="javascript">
function MarkMailAs(obj)
{
	var confirmMsg;
	switch(obj.value)
	{
		case "SEEN":
			confirmMsg = "<?=$Lang['Gamma']['ConfirmMsg']['MarkAsSeen']?>";
			break;
		case "FLAGGED":
			confirmMsg = "<?=$Lang['Gamma']['ConfirmMsg']['MarkAsStarred']?>";
			break;
		default:
			confirmMsg = "<?=$Lang['Gamma']['ConfirmMsg']['MarkAsUnseen']?>";
			break;
	}
	
	if(obj.selectedIndex)
	{
		if(!checkAlert(document.form1,'Uid[]','mark_read_unread.php',confirmMsg))
		{
			obj.selectedIndex=0;    
		}	
	}
     
}

function checkAll(obj)
{
	if(obj.checked)
	{
		$("input[name='Uid[]']").attr("checked","checked");
		$("table#MailList>tbody>tr:not(:first:last)").find("td").removeClass("tabletext iMailrow").addClass("tabletext iMailrowSelect");
	}
	else
	{
		$("input[name='Uid[]']").attr("checked","");
		$("table#MailList>tbody>tr:not(:first:last)").find("td").removeClass("tabletext iMailrowSelect").addClass("tabletext iMailrow");
	}
}

function setFlag(folder,uid,obj)
{
	var imgsrc = obj.src.substr(obj.src.lastIndexOf("/")+1)
	var setflag = imgsrc=="icon_star_on.gif"?0:1; //set off if on
	if(setflag==1)
		obj.src=obj.src.replace("icon_star_off.gif","icon_star_on.gif")
	else
		obj.src=obj.src.replace("icon_star_on.gif","icon_star_off.gif")

	$.post("ajax_task.php",
		{
			"task"		: "SetMailFlag",
			"Folder"	: folder,
			"Uid"		: uid,
			"setflag"	: setflag
		}
	);

}

$().ready(function(){
	initHighlightRow();
	$("input[name='Uid[]']:checked").parent().parent().find("td").removeClass("iMailrow").addClass("iMailrowSelect")
})

var mousedown;
var action = '';
var selectingoption = false; //use to avoid deselecting a mail when click on a selection box option
var disablehighlight = false; 
var startrow,endrow;
function initHighlightRow()
{
	mousedown = false;
	// highlight ticked row on load	
	$("input[name='Uid[]']").click(function(){
		// the tick checkbox action will be handled on mousedown event
		return false;
	})
	
	$(document).mousedown(function(){mousedown=true; }).mouseup(function(){mousedown=false; action='';})

	// disable select row on certain action
	$("select").click(function(){selectingoption=true;}); 
	$("a").mouseover(function(){disablehighlight=true; }).mouseout(function(){disablehighlight=false; });
	$("img.mailflag").mouseover(function(){disablehighlight=true; }).mouseout(function(){disablehighlight=false;}); 
	
	$("table#MailList>tbody>tr:not(:first:last)").mouseover(function(){
		if(!selectingoption)
		{
			if(mousedown==true)
			{
				endrow= $("table#MailList>tbody>tr").index(this);
				if(endrow>startrow)
				{
					var lower = startrow;
					var upper = endrow+1;
				}
				else
				{
					var lower = endrow-1;
					var upper = startrow;
				}
				
				if(action=='select')
				{
					$("table#MailList>tbody>tr:lt("+upper+"):gt("+lower+")").children().removeClass("iMailrow").addClass("iMailrowSelect");
					$("table#MailList>tbody>tr:lt("+upper+"):gt("+lower+")").find("[name='Uid[]']:not(:checked)").attr("checked","checked");
				}
				else
				{
					$("table#MailList>tbody>tr:lt("+upper+"):gt("+lower+")").children().removeClass("iMailrowSelect").addClass("iMailrow");
					$("table#MailList>tbody>tr:lt("+upper+"):gt("+lower+")").find("[name='Uid[]']:checked").attr("checked","");
				}
				
				if($("input[name='Uid[]']:not(:checked)").length==0)
					$("#CheckAllBox").attr("checked","checked");
				else
					$("#CheckAllBox").attr("checked","");
			}
		}
		selectingoption=false;
	}).mousedown(function(evt){
		startrow=$("table#MailList>tbody>tr").index(this);
		if(!disablehighlight)
		{
			var boxchecked = !$(this).find("[name='Uid[]']").attr("checked")
			$(this).find("[name='Uid[]']").attr("checked",boxchecked);
			if(boxchecked)
			{
				$(this).find("td").removeClass("iMailrow").addClass("iMailrowSelect");
				action = 'select';
			}
			else
			{
				$(this).find("td").removeClass("iMailrowSelect").addClass("iMailrow");
				action = 'deselect';
			}
	
			if($("input[name='Uid[]']:not(:checked)").length==0)
				$("#CheckAllBox").attr("checked","checked");
			else
				$("#CheckAllBox").attr("checked","");
		}
	});
	
	
	
}
</script>
<?
//debug_pr($IMap->Stop_Timer());

imap_close($IMap->inbox);
###########################################################################################
$linterface->LAYOUT_STOP();

?>