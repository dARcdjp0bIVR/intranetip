<?php
// using 
/*
 * 2014-09-17 (Carlos): rawurldecode() twice of Folder name because link has urlencode() one time and js has encodeURIComponent() one time
 */
ini_set("memory_limit","300M");
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");

intranet_auth();
intranet_opendb();

$ldamu = new libdigitalarchive_moduleupload();
//$lda = $ldamu->Get_libdigitalarchive();
//if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && sizeof($lda->UserInGroupList())==0){
//	exit;
//}
if(!$ldamu->User_Can_Archive_File()){
	exit;
}

$task = $_REQUEST['task'];

if($task=='GetUploadFileToDA_TBForm'){
	########### Create a tmp folder that store all module files here ############# 
	
	$IMap = new imap_gamma();
	$IMapCache = $IMap->getImapCacheAgent();
	$lfile = new libfilesystem();
	
	$Folder = rawurldecode(rawurldecode($_REQUEST['Folder']));
	$uid = $_REQUEST['uid'];
	$MailContent = $IMap->getMailRecord($Folder, $uid);
	
	$Attachment = $MailContent[0]["attach_parts"];
	
	if (!(is_null($Attachment) || $Attachment == "" || empty($Attachment))) {
		// clean tmp attachments and all_attachments.zip 
		$personal_path = $intranet_root."/file/gamma_mail/u".$_SESSION['UserID'];
		$dir_path = $personal_path."/attachments";
	
		if(file_exists($dir_path)){
			$lfile->folder_remove_recursive($dir_path);
		}
		
		$lfile->folder_new($dir_path);
		$lfile->chmod_R($dir_path, 0777);
		
		// short list	
		for ($i=0; $i< sizeof($Attachment); $i++) 
		{	
			$FileName = $Attachment[$i]["FileName"];
			$filepath = $dir_path."/".$FileName;
			
			while(file_exists($filepath)){
				//$FileName = rename_file($FileName);
				$FileName = $ldamu->Rename_File($FileName);
				$filepath = $dir_path."/".$FileName;
			}
			
			if(!file_exists($filepath))
			{
				$thisAttachment= $IMapCache->Get_Attachment($Attachment[$i]['PartID'],$uid,$Folder);
				$target_filepath = $personal_path."/attachments/".$FileName;
				$lfile->file_write($thisAttachment["Content"],$target_filepath);
			}
		}
		################## Return the Thickbox form ####################
		echo $ldamu->Get_Module_Files_Group_Folder_List_TB_Form('iMail plus',$dir_path);
	}
}

intranet_closedb();
?>