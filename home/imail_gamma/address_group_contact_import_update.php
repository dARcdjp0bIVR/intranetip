<?php
// editing by 
/*
 * 2014-12-18 (Carlos): Fixed one email to multiple groups logic
 */
@SET_TIME_LIMIT(600);
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libeclass.php");
include_once("../../includes/libftp.php");
include_once("../../includes/libimport.php");
include_once("../../includes/libfilesystem.php");
//include_once("../../includes/libfilesams.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../includes/libaccount.php");
//include_once("../../includes/libldap.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libimporttext.php");
intranet_opendb();

$anc = $special_option['no_anchor'] ?"":"#anc";

$li = new libimport();
$limport = new libimporttext();
//$lo = new libfilesams();
$libdb = new libdb();
$lfile = new libfilesystem();

# CSV
//check file extension
if(!$limport->CHECK_FILE_EXT($_FILES["userfile"]["name"]))
{
	header("Location: address_group_contact_import.php?aliastype=1&TabID=$TabID&xmsg=import_failed".$anc);
    exit;
}

# check format of first line of import file
$format = array('Group Name','User Name','User Email Address');
//$fp = fopen($userfile,"r");
# GET FIRST LINE
//$data = fgetcsv($fp,filesize($userfile));
  
//fclose($fp);
$correctFormat = true;

$new_data = $limport->GET_IMPORT_TXT($_FILES["userfile"]["tmp_name"] );  
array_shift($new_data);

if($correctFormat){
 	# create array for import contents 		
 	$row = $new_data;
}else{
	header("Location: address_group_contact_import.php?aliastype=1&TabID=$TabID&xmsg=import_failed".$anc);
}

# insert/update INTRANET_CAMPUSMAIL_ADDRES_EXTERNAL
if(sizeof($row)>0){
	
	$GroupNameToGroupID = array();
	$UserEmailToInfo = array();
	// find existing groups and group members
	$sql = "SELECT 
				AliasID as GroupID,
				AliasName as GroupName 
			FROM INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL 
			WHERE OwnerID='".$_SESSION['UserID']."'";
	$groups = $libdb->returnResultSet($sql);
	for($i=0;$i<count($groups);$i++){
		$GroupNameToGroupID[$groups[$i]['GroupName']] = $groups[$i]['GroupID'];
	}
	
	$sql = "SELECT 
				g.AliasID as GroupID,
				g.AliasName as GroupName,
				a.AddressID,
				a.TargetName as UserName,
				a.TargetAddress as UserEmailAddress 
			FROM INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL as g 
			LEFT JOIN INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY as e ON e.AliasID = g.AliasID 
			LEFT JOIN INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL as a ON a.AddressID = e.TargetID 
			WHERE g.OwnerID = '".$_SESSION['UserID']."' AND a.OwnerID = '".$_SESSION['UserID']."' 
			ORDER BY GroupID, UserEmailAddress";
	
	$groups_users = $libdb->returnArray($sql);
	
	for($i=0;$i<count($groups_users);$i++){
		$GroupNameToGroupID[$groups_users[$i]['GroupName']] = $groups_users[$i]['GroupID'];
		
		$address_id = trim($groups_users[$i]['AddressID']);
		$user_email = trim($groups_users[$i]['UserEmailAddress']);
		if($user_email != '' && !isset($UserEmailToInfo[$user_email])){
			$UserEmailToInfo[$user_email] = array();
		}
		if($address_id != '' && $user_email != ''){
			$UserEmailToInfo[$user_email][$groups_users[$i]['GroupID']] = array('GroupID'=>$groups_users[$i]['GroupID'], 'AddressID'=>$groups_users[$i]['AddressID']);
		}
	}
	
	// find existing external user emails
	$sql = "SELECT AddressID,TargetAddress as UserEmailAddress 
			FROM INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL 
			WHERE OwnerID = '".$_SESSION['UserID']."'";
	$ExternalUserEmails = $libdb->returnArray($sql);
	$ExternalEmailToAddressID = array();
	for($i=0;$i<count($ExternalUserEmails);$i++){
		$ExternalEmailToAddressID[$ExternalUserEmails[$i]['UserEmailAddress']] = $ExternalUserEmails[$i]['AddressID'];
	}
	
	for($i=0;$i<sizeof($row);$i++){
		 $groupName = trim($row[$i][0]);
		 $userName = trim($row[$i][1]);
		 $userEmail = trim($row[$i][2]);
		 
		 if($groupName == '' || $userName == '' || $userEmail == '' || !intranet_validateEmail($userEmail)){
		 	$error++;
		 	continue;
		 }
		 
		 // if group name not exist, add it
		 if(!isset($GroupNameToGroupID[$groupName]) || $GroupNameToGroupID[$groupName]==''){
		 	$safe_groupName = $libdb->Get_Safe_Sql_Query($groupName);
		 	$sql = "INSERT INTO INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL (OwnerID, AliasName, NumberOfEntry, DateInput, DateModified) 
					VALUES ('".$_SESSION['UserID']."','".$safe_groupName."',0,NOW(),NOW())";
			$insert_group_success = $libdb->db_db_query($sql);
			if($insert_group_success){
				$new_groupID = $libdb->db_insert_id();
				$GroupNameToGroupID[$groupName] = $new_groupID;
			}
		 }
		 
		 // if external user email not exist, add it
		 if(!isset($ExternalEmailToAddressID[$userEmail])){
		 	$safe_userName = $libdb->Get_Safe_Sql_Query($userName);
		 	$sql = "INSERT INTO INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL (OwnerID,TargetName,TargetAddress,DateInput,DateModified) 
					VALUES ('".$_SESSION['UserID']."','".$safe_userName."','".$userEmail."',NOW(),NOW())";
			$insert_useremail_success = $libdb->db_db_query($sql);
			if($insert_useremail_success){
				$new_addressID = $libdb->db_insert_id();
				$ExternalEmailToAddressID[$userEmail] = $new_addressID;	
			}
		 }
		 
		 // if external user email not added to group yet, add it to group
		 if(!isset($UserEmailToInfo[$userEmail][$GroupNameToGroupID[$groupName]])){
			$sql = "INSERT INTO INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY (AliasID,TargetID,DateInput,DateModified) 
					VALUES('".$GroupNameToGroupID[$groupName]."','".$ExternalEmailToAddressID[$userEmail]."',NOW(),NOW())";
			$insert_entry_success = $libdb->db_db_query($sql);
			if($insert_entry_success){
				if(!isset($UserEmailToInfo[$userEmail])){
					$UserEmailToInfo[$userEmail] = array();
				}
				$UserEmailToInfo[$userEmail][$GroupNameToGroupID[$groupName]] = array('GroupID'=>$GroupNameToGroupID[$groupName],'AddressID'=>$ExternalEmailToAddressID[$userEmail]);
			}
		 }
	}
	
	// update total number of entries for each group
	$sql = "SELECT g.AliasID, COUNT(e.EntryID) as NumberOfEntry FROM INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL as g 
			INNER JOIN INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY as e ON e.AliasID = g.AliasID 
			WHERE g.OwnerID = '".$_SESSION['UserID']."' 
			GROUP BY g.AliasID";
	$group_entries = $libdb->returnArray($sql);
	for($i=0;$i<count($group_entries);$i++){
		$sql = "UPDATE INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL SET NumberOfEntry = '".$group_entries[$i]['NumberOfEntry']."' 
				WHERE AliasID = '".$group_entries[$i]['AliasID']."' AND OwnerID = '".$_SESSION['UserID']."'";
		$libdb->db_db_query($sql);
	}
	
	if($error == sizeof($row)){
		header("Location: address_group_contact_import.php?aliastype=1&TabID=$TabID&xmsg=import_failed2".$anc);
	}else{
		header("Location: groupalias.php?aliastype=1&TabID=$TabID&xmsg=import_success".$anc);
	}

}
else {
	header("Location: address_group_contact_import.php?aliastype=1&TabID=$TabID&xmsg=import_failed2".$anc);
}
intranet_closedb();

?>