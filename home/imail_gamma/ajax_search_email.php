<?php
// Editing by 
/******************************************************** Changes ******************************************************
 * 2015-05-29 (Carlos): replace comma separator to semi-colon separator with Replace_Display_Comma() for the input value.
 * 2015-04-20 (Carlos): Escape double quotes with slash in display names.
 * 2015-04-14 (Carlos): Added flag $sys_custom['iMailPlus']['EmailNameNoNickName'] to show official name, do not use nick name for MunSang College.
 * 2014-12-04 (Carlos): [ip2.5.5.12.1] added local functions replace_chars_in_value($s) and replace_chars_in_display_text($s) to handle auto completion output value
 * 2013-07-12 (Carlos): Fix internal group selection query
 * 2013-03-27 (Carlos): added ClassName to search criteria
 * 2013-02-08 (Carlos): added $sys_custom['iMailPlusEmailNameWithoutClassInfo'] to force do not display class name and class number
 * 2013-01-29 (Carlos): changed variable name $UserID to $user_id as it may overwrite session var
 * 2012-06-14 (Carlos): if user is staff type, he/she can search all intranet users
 * 2012-03-20 (Carlos): change getNameFieldByLang2() to getNameFieldByLang() which with title
 * 2012-03-06 (Carlos): fields use TRIM(NULL) = '' is still NULL, fail to select recipients, added checking IS NOT NULL
 ***********************************************************************************************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_opendb();

function replace_chars_in_value($s)
{
	return str_replace(array('&#039;','&quot;','&amp;'),array("'",'\"',"&"),$s);
}

function replace_chars_in_display_text($s)
{
	return str_replace(array('<','>','&#039;','&quot;','&amp;'),array('&lt;','&gt;',"'",'\"','&'),$s);
}

$libdb = new libdb();

$user_id = $_REQUEST['user_id'];
$InputValue = stripslashes(urldecode($_REQUEST['q']));

$InputValue = Replace_Display_Comma($InputValue);
//$InputValue = str_replace(',',';',$InputValue);
//$SearchValue = strstr($InputValue,";")?trim(substr($InputValue,strrpos($InputValue,";")+1)):trim($InputValue);  
$SearchValue = strrpos($InputValue,";")!==false?trim(substr($InputValue,strrpos($InputValue,";")+1)):trim($InputValue);

if($SearchValue=='') die;

			$AddressBookSql = "
							SELECT 
								CONCAT('\"',TargetName,'\" <',TargetAddress,'>') 
							FROM 
								INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
							WHERE 
								OwnerID = $user_id
								AND 
								(
									TargetName LIKE '%$SearchValue%'
									OR TargetAddress LIKE '%$SearchValue%'
								)
							";
			$AddressBookArr = $libdb->returnArray($AddressBookSql,3); 
			
			### If the user is a staff/teacher, they can also search the email address by typing the login name of the target user
			$InternalArr = array();
			if($UserType == 1)
			{
				if($sys_custom['iMailPlus']['EmailNameNoNickName'])
				{
					$NameField = getNameFieldByLang();
				}else{
					$NameField = " IF(NickName IS NULL OR TRIM(NickName)='',".getNameFieldByLang().",NickName) ";
				}
				if($sys_custom['iMailPlusEmailNameWithoutClassInfo']) {
					$ClassInfo = "''";
				}else{
					$ClassInfo = "IF(RecordType = 1, '', IF(ClassName = '' OR ClassName IS NULL, '', CONCAT(' (',ClassName,'-',ClassNumber,')')))";
				}
				$InternalSql = "SELECT 
									CONCAT('\"',$NameField, $ClassInfo ,'\" <',ImapUserEmail,'>') as email 
								FROM 
									INTRANET_USER 
								WHERE
									RecordStatus = 1
									AND 
									(
										EnglishName LIKE '%$SearchValue%'
										OR ChineseName LIKE '%$SearchValue%' 
										OR ImapUserEmail LIKE '%$SearchValue%'
										OR UserLogin LIKE '%$SearchValue%' 
										OR ClassName LIKE '%$SearchValue%' 
									)
									AND (ImapUserEmail IS NOT NULL AND ImapUserEmail <> '') 
								ORDER BY RecordType, IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName";
				$InternalArr = $libdb->returnArray($InternalSql,3);
			}
			
			$Result = array_merge($AddressBookArr,$InternalArr);

			# search cookie
			foreach((array)$_COOKIE["Gamma"]["AutoFill"] as $email => $emailwithname)
			{
				if(stristr($email,$SearchValue) || stristr($emailwithname,$SearchValue))
					$Result[]= array(0=>stripslashes($emailwithname));
			}
			
			$Result = Get_Array_By_Key($Result,0); 
			$Result = array_values(array_unique($Result));

			# search internal group name
			if($sys_custom['iMailPlus']['EmailNameNoNickName'])
			{
				$NameField = getNameFieldByLang("c.");
			}else{
				$NameField = " IF(c.NickName IS NULL OR TRIM(c.NickName)='',".getNameFieldByLang("c.").",c.NickName) ";
			}
			if($sys_custom['iMailPlusEmailNameWithoutClassInfo']) {
				$ClassInfo = "''";
			}else{
				$ClassInfo = "IF(c.RecordType = 1, '', IF(c.ClassName = '' OR c.ClassName IS NULL, '', CONCAT(' (',c.ClassName,'-',c.ClassNumber,')')))";
			}
			# get current academic year id
			$current_academic_year_id = Get_Current_Academic_Year_ID();
			$sql = "
				SELECT 
					a.Title as Title, 
					CONCAT('\"',$NameField, IF(c.RecordType = 1, '', $ClassInfo),'\" <',c.ImapUserEmail,'>') as email
				FROM 
					INTRANET_GROUP a 
					INNER JOIN INTRANET_USERGROUP b on a.GroupID = b.GroupID 
					INNER JOIN INTRANET_USER c ON c.UserID = b.UserID 
				WHERE 
					c.RecordStatus = 1 
					AND (a.AcademicYearID = '$current_academic_year_id' OR a.AcademicYearID IS NULL)
					AND (c.IMapUserEmail <> '' AND c.IMapUserEmail Is NOT NULL)
					AND (a.Title LIKE '%$SearchValue%' OR a.TitleChinese LIKE '%$SearchValue%')
				ORDER BY 
					a.Title
			";
			
			$ResultArr = $libdb->returnArray($sql);
			foreach((array)$ResultArr as $EmailArr)
			{
				$GroupEmailArr[$EmailArr["Title"]][] = $EmailArr["email"];
			}
			
			# Search External Group Name
			$sql = "
				SELECT
					c.AliasName,
					CONCAT('\"',b.TargetName,'\" <',b.TargetAddress,'>') as email
	          	FROM 
					INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY AS a
	                INNER JOIN  INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL AS b ON a.TargetID = b.AddressID
					INNER JOIN INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL c ON a.AliasID = c.AliasID
				WHERE
					c.OwnerID = '$user_id'
					AND c.AliasName LIKE '%$SearchValue%'
			";
			$ResultArr = $libdb->returnArray($sql);
			foreach((array)$ResultArr as $EmailArr)
			{
				$ExGroupEmailArr[$EmailArr["AliasName"]][] = $EmailArr["email"];
			}

for ($i=0; $i< sizeof($Result); $i++) {
	//echo $Result[$i]."|".htmlspecialchars($Result[$i])."\n";
	echo replace_chars_in_value($Result[$i])."|".replace_chars_in_display_text($Result[$i])."\n";
}
foreach((array)$GroupEmailArr as $Title => $IMapEmailArr )
{
	//echo implode("; ",$IMapEmailArr)."|".$Lang['Gamma']['InternalGroupLabel'].htmlspecialchars($Title)."\n";
	echo replace_chars_in_value(implode("; ",$IMapEmailArr))."|".$Lang['Gamma']['InternalGroupLabel'].replace_chars_in_display_text($Title)."\n";
}
foreach((array)$ExGroupEmailArr as $Title => $IMapEmailArr )
{
	//echo implode("; ",$IMapEmailArr)."|".$Lang['Gamma']['ExternalGroupLabel'].htmlspecialchars($Title)."\n";
	echo replace_chars_in_value(implode("; ",$IMapEmailArr))."|".$Lang['Gamma']['ExternalGroupLabel'].replace_chars_in_display_text($Title)."\n";
}

intranet_closedb();
die;
?>