<?php
// editing by 
/************************ Modification Log **************************************
 * 2014-06-18 (Carlos): Bind contacts links to do keyword search
 * 2011-10-20 (Carlos): Added Identity type Alumni
 * 2010-12-31 (Carlos): Add Option [Others] > [Shared MailBox]
 ********************************************************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/role_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$anc = $special_option['no_anchor'] ?"":"#anc";

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPageArr['iMail'] = 1;
$CurrentPage = "PageAddressBook_ExternalReceipient";

$IMap = new imap_gamma();
$lwebmail = new libwebmail();
$linterface = new interface_html();
$li = new libuser($UserID);
$lrole = new role_manage();
$fcm = new form_class_manage();
$lgrouping = new libgrouping();

# Block No webmail no address book
if ($noWebmail)
{
    header("Location: groupalias.php?TabID=1".$anc);
    exit();
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
$escaped_keyword = $li->Get_Safe_Sql_Like_Query(stripslashes($keyword));
if($field=="") $field = 0;
if($order=="") $order = 1;
$litable = new libdbtable2007($field, $order, $pageNo);

$mail_domain = $lwebmail->mailaddr_domain;

$name_field = getNameFieldWithClassNumberByLang("a.");
$identity = $lrole->Get_Identity_Type($UserID);
$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

## get toStaff, toStudent, toParent options - IP25 only ##
$result_to_options['ToTeachingStaffOption'] = 0;
$result_to_options['ToNonTeachingStaffOption'] = 0;
$result_to_options['ToStudentOption'] = 0;
$result_to_options['ToParentOption'] = 0;
$result_to_options['ToAlumniOption'] = 0;

## Newly added by Ronald (20091118) - for �^�خѰ| [CRM Ref No.: 2009-1116-0915]
## Control by flag - $sys_custom['iMail_RecipientCategory_StaffAndTeacher']
$result_to_options['ToTeacherAndStaff'] = 0;
if($sys_custom['iMail_RecipientCategory_StaffAndTeacher'])
{
	if(($identity == "Teaching") || ($identity == "NonTeaching"))
	{
		$result_to_options['ToTeacherAndStaff'] = 1;
	}
}

### Pre-Gen all related group email ###
$js_all_teacher = addslashes($IMap->GetAllTeacherEmailByUserIdentity($UserID));
$js_all_form_teacher = addslashes($IMap->GetAllSameFormTeacherEmailByUserIdentity($UserID));
$js_all_class_teacher = addslashes($IMap->GetAllSameClassTeacherEmailByUserIdentity($UserID));
$js_all_subject_teacher = addslashes($IMap->GetAllSameSubjectTeacherEmailByUserIdentity($UserID));
$js_all_subject_group_teacher = addslashes($IMap->GetAllSameSubjectGroupTeacherEmailByUserIdentity($UserID));

$js_all_student = addslashes($IMap->GetAllStudentEmailByUserIdentity($UserID));
$js_all_form_student = addslashes($IMap->GetAllSameFormStudentEmailByUserIdentity($UserID));
$js_all_class_student = addslashes($IMap->GetAllSameClassStudentEmailByUserIdentity($UserID));
$js_all_subject_student = addslashes($IMap->GetAllSameSubjectStudentEmailByUserIdentity($UserID));
$js_all_subject_group_student = addslashes($IMap->GetAllSameSubjectGroupStudentEmailByUserIdentity($UserID));

$js_all_parent = addslashes($IMap->GetAllParentEmailByUserIdentity($UserID));
$js_all_form_parent = addslashes($IMap->GetAllSameFormParentEmailByUserIdentity($UserID));
$js_all_class_parent = addslashes($IMap->GetAllSameClassParentEmailByUserIdentity($UserID));
$js_all_subject_parent = addslashes($IMap->GetAllSameSubjectParentEmailByUserIdentity($UserID));
$js_all_subject_group_parent = addslashes($IMap->GetAllSameSubjectGroupParentEmailByUserIdentity($UserID));

if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Staff-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup']))
{
	if($sys_custom['iMail_RemoveTeacherCat']) {
		$result_to_options['ToTeachingStaffOption'] = 0;
	} else {
		$result_to_options['ToTeachingStaffOption'] = 1;
	}
}
if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']))
{
	if($sys_custom['iMail_RemoveNonTeachingCat']) {
		$result_to_options['ToNonTeachingStaffOption'] = 0;
	} else {
		$result_to_options['ToNonTeachingStaffOption'] = 1;
	}
}
if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Student-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Student-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup']))
{
	$result_to_options['ToStudentOption'] = 1;
}
if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Parent-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup']))
{
	$result_to_options['ToParentOption'] = 1;
}
if($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['All-Yes'] || $_SESSION['SSV_USER_TARGET']['Alumni-All'])){
	$result_to_options['ToAlumniOption'] = 1;
}

if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup']) || ($special_feature['alumni'] && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup']))
{
	$result_to_options['ToGroupOption'] = 1;
	
	$result_to_group_options['ToTeacher'] = 0;
	$result_to_group_options['ToStaff'] = 0;
	$result_to_group_options['ToStudent'] = 0;
	$result_to_group_options['ToParent'] = 0;
	$result_to_group_options['ToAlumni'] = 0;
	
	if($_SESSION['SSV_USER_TARGET']['All-Yes']){
		$result_to_group_options['ToTeacher'] = 1;
		$result_to_group_options['ToStaff'] = 1;
		$result_to_group_options['ToStudent'] = 1;
		$result_to_group_options['ToParent'] = 1;
		if($special_feature['alumni']) $result_to_group_options['ToAlumni'] = 1;
	}else{
		if(($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])){
			$result_to_group_options['ToTeacher'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])){
			$result_to_group_options['ToStaff'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup'])){
			$result_to_group_options['ToStudent'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])){
			$result_to_group_options['ToParent'] = 1;
		}
		if($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['Alumni-All'] || $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'])){
			$result_to_group_options['ToAlumni'] = 1;
		}
	}
}

$AlumniNoTargetCond = true;
if($special_feature['alumni']) $AlumniNoTargetCond = $_SESSION['SSV_USER_TARGET']['Alumni-All'] == '' && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'] == '';
### If no mail targeting is set in the front-end, than will assign some default targeting to user ###
if( ($_SESSION['SSV_USER_TARGET']['All-Yes'] == '') && 
($_SESSION['SSV_USER_TARGET']['All-No'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MyForm'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MyClass'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MySubject'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-All'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MyForm'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MyClass'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MySubject'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MyGroup'] == '') &&
($_SESSION['SSV_USER_TARGET']['Parent-All'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MyForm'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MyClass'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MySubject'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'] == '') && 
$AlumniNoTargetCond 
)
{
	if($result_to_options['ToTeachingStaffOption'] == 0 && $result_to_options['ToNonTeachingStaffOption'] == 0 && $result_to_options['ToStudentOption'] == 0 && $result_to_options['ToParentOption'] == 0 && $result_to_options['ToAlumniOption'] == 0)
	{
		if(($identity == "Teaching") || ($identity == "NonTeaching"))
		{
			### If user is Teacher, then will have the follow targeting :
			###  - to All Teaching Staff
			###  - to All NonTeaching Staff
			###  - to All Student 
			###  - to All Parent 
			$result_to_options['ToTeachingStaffOption'] = 1;
			$result_to_options['ToNonTeachingStaffOption'] = 1;
			$result_to_options['ToStudentOption'] = 1;
			$result_to_options['ToParentOption'] = 1;
			if($special_feature['alumni']) $result_to_options['ToAlumniOption'] = 1;
			$_SESSION['SSV_USER_TARGET']['All-Yes'] = true;
		}
		if($identity == "Student")
		{
			### If user is Student, then will have the follow targeting :
			###  - to Student Own Class Student
			###  - to Student Own Subject Group Student
			$result_to_options['ToStudentOption'] = 1;
			$_SESSION['SSV_USER_TARGET']['Student-MyClass'] = true;	
			$_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'] = true;
		}
		if($identity == "Parent")
		{
			### If user is Parent, then will have the follow targeting :
			###  - to Their Child's Own Class Teacher
			###  - to Their Child's Own Subject Group Teacher
			$result_to_options['ToTeachingStaffOption'] = 1;
			$_SESSION['SSV_USER_TARGET']['Staff-MyClass'] = true;	
			$_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'] = true;
		}
	}
	$result_to_options['ToGroupOption'] = 1;
	
	$result_to_group_options['ToTeacher'] = 0;
	$result_to_group_options['ToStaff'] = 0;
	$result_to_group_options['ToStudent'] = 0;
	$result_to_group_options['ToParent'] = 0;
	$result_to_group_options['ToAlumni'] = 0;
	
	if($_SESSION['SSV_USER_TARGET']['All-Yes']){
		$result_to_group_options['ToTeacher'] = 1;
		$result_to_group_options['ToStaff'] = 1;
		$result_to_group_options['ToStudent'] = 1;
		$result_to_group_options['ToParent'] = 1;
		if($special_feature['alumni']) $result_to_group_options['ToAlumni'] = 1;
	}else{
		if(($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])){
			$result_to_group_options['ToTeacher'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])){
			$result_to_group_options['ToStaff'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup'])){
			$result_to_group_options['ToStudent'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])){
			$result_to_group_options['ToParent'] = 1;
		}
		if($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['Alumni-All'] || $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'])){
			$result_to_group_options['ToAlumni'] = 1;
		}
	}
}

if($identity != "Student")
{
	$sql = "SELECT COUNT(*) FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
	$result = $li->returnVector($sql);
	if($result[0]>0)
		$result_to_options['ToMyChildrenOption'] = 1;
}
if($identity == "Student")
{
	$sql = "SELECT COUNT(*) FROM INTRANET_PARENTRELATION WHERE StudentID = '$UserID'";
	$result = $li->returnVector($sql);
	if($result[0]>0)
		$result_to_options['ToMyParentOption'] = 1;
}

//$arrOption[] = array(1,$Lang['iMail']['FieldTitle']['ByIdentity']);
//$arrOption[] = array(2,$Lang['iMail']['FieldTitle']['ByGroup']);
//$OptionSelection = getSelectByArray($arrOption, " name='GroupOpt' onChange='this.form.submit(); resetGroupOption(this.value);'", $GroupOpt);

$x1  = ($OptValue!="" && $OptValue > 0) ? "<select name='OptValue' onChange='checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()' >\n" : "<select name='OptValue' onChange='checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]); this.form.submit()' >\n";
$x1 .= "<option value='0' >--{$button_select}--</option>\n";
$x1 .= "<optgroup label='".htmlspecialchars($Lang['iMail']['FieldTitle']['ByIdentity'],ENT_QUOTES)."'>";

# Create Cat list (Teacher, Staff, Student, Parent, Group) - IP25 Only #
## Newly added by Ronald (20091118) - for �^�خѰ| [CRM Ref No.: 2009-1116-0915]
## Control by flag - $sys_custom['iMail_RecipientCategory_StaffAndTeacher']
if ($result_to_options['ToTeacherAndStaff'])
{
	$x1 .= "<option value=-3 ".(($OptValue==-3)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['TeacherAndStaff']."</option>\n";
}
if ($result_to_options['ToTeachingStaffOption'])
{
    $x1 .= "<option value=-1 ".(($OptValue==-1)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Teacher']."</option>\n";
}
if ($result_to_options['ToNonTeachingStaffOption'])
{
    $x1 .= "<option value=-2 ".(($OptValue==-2)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['NonTeachingStaff']."</option>\n";
}
if ($result_to_options['ToStudentOption'])
{
    $x1 .= "<option value=2 ".(($OptValue==2)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Student']."</option>\n";
}
if ($result_to_options['ToParentOption'])
{
    $x1 .= "<option value=3 ".(($OptValue==3)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Parent']."</option>\n";
}
if ($special_feature['alumni'] && $result_to_options['ToAlumniOption'])
{
    $x1 .= "<option value=-4 ".(($OptValue==-4)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Alumni']."</option>\n";
}
if ($result_to_options['ToMyChildrenOption'])
{
	$x1 .= "<option value=5 ".(($OptValue==5)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['MyChildren']."</option>\n";
}
if ($result_to_options['ToMyParentOption'])
{
	$x1 .= "<option value=6 ".(($OptValue==6)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['MyParent']."</option>\n";
}
$x1 .= "</optgroup>";
	
//$lclubsenrol = new libclubsenrol();

$arrExcludeGroupCatID[] = 0;
//if(($plugin['eEnrollment'] == true) && ($lclubsenrol->isUsingYearTermBased == 1)){
//	$arrExcludeGroupCatID[] = 5;
//}
$targetGroupCatIDs = implode(",",$arrExcludeGroupCatID);

if($_SESSION['SSV_USER_TARGET']['All-Yes'])
{
	$sql = "SELECT DISTINCT GroupCategoryID, CategoryName FROM INTRANET_GROUP_CATEGORY WHERE GroupCategoryID NOT IN ($targetGroupCatIDs) ORDER BY CategoryName ASC";
}
else
{
	if($result_to_group_options['ToTeacher'] || $result_to_group_options['ToStaff'] || $result_to_group_options['ToStudent'] || $result_to_group_options['ToParent'] || ($special_feature['alumni'] && $result_to_group_options['ToAlumni']))
	{
		$sql = "SELECT DISTINCT a.GroupCategoryID, a.CategoryName FROM INTRANET_GROUP_CATEGORY AS a INNER JOIN INTRANET_GROUP AS b ON (a.GroupCategoryID = b.RecordType) INNER JOIN INTRANET_USERGROUP AS c ON (b.GroupID = c.GroupID) WHERE c.UserID = '$UserID' AND b.RecordType != 0 AND a.GroupCategoryID NOT IN ($targetGroupCatIDs) ORDER BY a.CategoryName ASC";
	}
}
$result = $li->returnArray($sql);

if(sizeof($result) > 0){
	for($i=0; $i<sizeof($result); $i++)
	{
		if($i == 0){
			$x1 .= "<optgroup label='".htmlspecialchars($Lang['iMail']['FieldTitle']['ByGroup'],ENT_QUOTES)."'>";
		}
		list($GroupCatID, $GroupCatName) = $result[$i];
		$GroupCatID = "GROUP_".$GroupCatID;
		
		$x1 .= "<option value='$GroupCatID' ".(($GroupCatID == $OptValue)?"SELECTED":"").">".$GroupCatName."</option>";
						
		if($i== sizeof($result)-1) {
			$x1 .= "</optgroup>";
		}
	}
}

## Others > Shared MailBox
if($_SESSION['SSV_USER_TARGET']['All-Yes']){
	
	$sql = "SELECT s.MailBoxID as UserID, IF(p.DisplayName IS NULL OR p.DisplayName = '',s.MailBoxName,p.DisplayName) as UserName, s.MailBoxName as UserEmail FROM MAIL_SHARED_MAILBOX as s LEFT JOIN MAIL_PREFERENCE as p ON p.MailBoxName = s.MailBoxName ";
	$SharedMailBoxes = $li->returnArray($sql,3);
	if(sizeof($SharedMailBoxes)>0){
		$x1 .= "<optgroup label='".$Lang['General']['Others']."'>";
		$x1 .= "<option value='SHARED_MAILBOX' ".($OptValue == "SHARED_MAILBOX"?"SELECTED":"").">".$Lang['SharedMailBox']['SharedMailBox']."</option>";
		$x1 .= "</optgroup>";
	}
}

$x1 .= "</select>";

if($OptValue != "")
{
	if($OptValue=="SHARED_MAILBOX")
	{
		## Others
		$GroupOpt = 3;
		$CatID = $OptValue;
		$x1 .= "<input type='hidden' name='CatID' value='$CatID'>";
	}
	else if(strpos($OptValue,"GROUP_") !== false)
	{
		## GROUP 
		$GroupOpt = 2;
		$CatID = 4;
		$ChooseGroupCatID = substr($OptValue,6,strlen($OptValue));
		$x1 .= "<input type='hidden' name='CatID' value=$CatID>";
	}
	else
	{	
		## Identity
		$GroupOpt = 1;
		$CatID = $OptValue;
		$x1 .= "<input type='hidden' name='CatID' value=$CatID>";
	}
}
else
{
	$GroupOpt = "";
	$CatID = "";
	$ChooseGroupCatID = "";
	$x1 .= "<input type='hidden' name='CatID' value=''>";
}

# 2nd Level Cat List - IP25 only #
if(($CatID != "") || ($CatID != 0) || ($CatID != 4)){
	if($CatID == -1){
		//$x2 = "<select name='Cat2ID' id='Cat2ID' multiple size='10'>";
		$x2 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10'>";
		## All Teaching Staff ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])){
			
			$num_of_all_teacher = $lwebmail->returnNumOfAllTeachingStaff($identity);
			if($num_of_all_teacher > 0){
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='1' ".((in_array(1,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff']."</option>";
				else
					$x2 .= "<option value='1' >".$Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff']."</option>";
			}
		}
		## Form Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MyForm'])){
			
			$num_of_form_teacher = $lwebmail->returnNumOfFormTeacher($identity);
			if($num_of_form_teacher > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='2' ".((in_array(2,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToFormTeachingStaff']."</option>";
				else
					$x2 .= "<option value='2' >".$Lang['iMail']['FieldTitle']['ToFormTeachingStaff']."</option>";
			}
		}
		## Class Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MyClass'])){
			
			$num_of_class_teacher = $lwebmail->returnNumOfClassTeacher($identity);
			if($num_of_class_teacher > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='3' ".((in_array(3,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToClassTeachingStaff']."</option>";
				else
					$x2 .= "<option value='3' >".$Lang['iMail']['FieldTitle']['ToClassTeachingStaff']."</option>";
			}
		}
		## Subject Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MySubject'])){
			
			$num_of_subject_teacher = $lwebmail->returnNumOfSubjectTeacher($identity);
			if($num_of_subject_teacher > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='4' ".((in_array(4,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff']."</option>";
				else
					$x2 .= "<option value='4' >".$Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff']."</option>";
			}
					
		}
		## Subject Group Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'])){
			
			$num_of_subject_group_teacher = $lwebmail->returnNumOfSubjectGroupTeacher($identity);
			if($num_of_subject_group_teacher > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='5' ".((in_array(5,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff']."</option>";
				else
					$x2 .= "<option value='5' >".$Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff']."</option>";
			}
		}
		$x2 .= "</select>";
	}
	if($CatID == -2){
		## Non-teaching Staff ##
		//$result = $fcm->Get_Non_Teaching_Staff_List();
		$NameField = getNameFieldByLang('a.');
		$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
		$sql = "Select a.UserID, ".$NameField." as UserName, a.IMapUserEmail as UserEmail From INTRANET_USER as a where a.RecordStatus = 1 and a.RecordType = 1 and (a.Teaching = 0 OR a.Teaching IS NULL) AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY EnglishName";
		//$result = $li->returnArray($sql,3);
		/*$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				//$x3 .= "<option value='$u_email'>".$u_name."</option>";
				$final_email = '"'.$u_name.'"'." <".$u_email."> ";
				$x3 .= "<option value='$final_email'>$u_name</option>";
			}
		}else{
			$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		$x3 .= "</select>";*/
	}
	## Newly added by Ronald (20091118) - for �^�خѰ| [CRM Ref No.: 2009-1116-0915]
	## Control by flag - $sys_custom['iMail_RecipientCategory_StaffAndTeacher']
	if($CatID == -3){
		## Teachers / Staff ##
		$NameField = getNameFieldByLang('a.');
		$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
		$sql = "Select a.UserID as UserID, ".$NameField." as UserName, a.IMapUserEmail as UserEmail From INTRANET_USER as a WHERE a.RecordStatus = '1' AND a.RecordType = '1' AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY EnglishName";
		//$result = $fcm->returnArray($sql,3);
		
		/*$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				$final_email = '"'.$u_name.'"'." <".$u_email."> ";
				$x3 .= "<option value='$final_email'>".$u_name."</option>";
			}
		}else{
			$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		$x3 .= "</select>";*/
	}
	if($CatID == 2){
		$x2 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10'>";
		## All Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])){
			
			$num_of_all_student = $lwebmail->returnNumOfAllStudent($identity);
			if($num_of_all_student > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='1' ".((in_array(1,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToIndividualsStudent']."</option>";
				else
					$x2 .= "<option value='1' >".$Lang['iMail']['FieldTitle']['ToIndividualsStudent']."</option>";
			}
		}
		## Form Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MyForm'])){
			
			$num_of_form_subject = $lwebmail->returnNumOfFormStudent($identity);
			if($num_of_form_subject > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='2' ".((in_array(2,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToFormStudent']."</option>";
				else
					$x2 .= "<option value='2' >".$Lang['iMail']['FieldTitle']['ToFormStudent']."</option>";
			}
		}
		## Class Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MyClass'])){
			
			$num_of_class_student = $lwebmail->returnNumOfClassStudent($identity);
			if($num_of_class_student > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='3' ".((in_array(3,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToClassStudent']."</option>";
				else
					$x2 .= "<option value='3' >".$Lang['iMail']['FieldTitle']['ToClassStudent']."</option>";
			}
		}
		## Subject Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MySubject'])){
			
			$num_of_subject_student = $lwebmail->returnNumOfSubjectStudent($identity);
			if($num_of_subject_student > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='4' ".((in_array(4,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectStudent']."</option>";
				else
					$x2 .= "<option value='4' >".$Lang['iMail']['FieldTitle']['ToSubjectStudent']."</option>";
			}
		}
		## Subject Group Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'])){
			
			$num_of_subject_group_student = $lwebmail->returnNumOfSubjectGroupStudent($identity);
			if($num_of_subject_group_student > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='5' ".((in_array(5,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectGroupStudent']."</option>";
				else
					$x2 .= "<option value='5' >".$Lang['iMail']['FieldTitle']['ToSubjectGroupStudent']."</option>";
			}
		}
		$x2 .= "</select>";
	}
	if($CatID == 3){
		$x2 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10'>";
		## All Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])){
			
			$num_of_all_parent = $lwebmail->returnNumOfAllParent($identity);
			if($num_of_all_parent > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='1' ".((in_array(1,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToIndividualsParents']."</option>";
				else
					$x2 .= "<option value='1' >".$Lang['iMail']['FieldTitle']['ToIndividualsParents']."</option>";
			}
		}
		## Form Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MyForm'])){
			
			$num_of_form_parent = $lwebmail->returnNumOfFormParent($identity);
			if($num_of_form_parent > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='2' ".((in_array(2,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToFormParents']."</option>";
				else
					$x2 .= "<option value='2' >".$Lang['iMail']['FieldTitle']['ToFormParents']."</option>";
			}
		}
		## Class Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MyClass'])){
			
			$num_of_class_parent = $lwebmail->returnNumOfClassParent($identity);
			if($num_of_class_parent > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='3' ".((in_array(3,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToClassParents']."</option>";
				else
					$x2 .= "<option value='3' >".$Lang['iMail']['FieldTitle']['ToClassParents']."</option>";
			}
		}
		## Subject Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MySubject'])){
			
			$num_of_subject_parent = $lwebmail->returnNumOfSubjectParent($identity);
			if($num_of_subject_parent > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='4' ".((in_array(4,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectParents']."</option>";
				else
					$x2 .= "<option value='4' >".$Lang['iMail']['FieldTitle']['ToSubjectParents']."</option>";
			}
		}
		## Subject Group Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup'])){
			
			$num_of_subject_group_parent = $lwebmail->returnNumOfSubjectGroupParent($identity);
			if($num_of_subject_group_parent > 0)
			{
				if(sizeof($ChooseGroupID)>0)
					$x2 .= "<option value='5' ".((in_array(5,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectGroupParents']."</option>";
				else
					$x2 .= "<option value='5' >".$Lang['iMail']['FieldTitle']['ToSubjectGroupParents']."</option>";
			}
		}
		$x2 .= "</select>";
	}
	if($special_feature['alumni'] && $CatID == -4){
		## All Alumni ##
		$NameField = getNameFieldByLang('a.');
		$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
		$sql = "Select a.UserID, ".$NameField." as UserName, a.IMapUserEmail as UserEmail From INTRANET_USER as a where a.RecordStatus = 1 and a.RecordType = 4 and (a.Teaching = 0 OR a.Teaching IS NULL) AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY EnglishName";
	}
	if($CatID == 5){
		$NameField = getNameFieldByLang2("b.");
		$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
		$sql = "SELECT b.UserID as UserID, $NameField as UserName, b.IMapUserEmail as UserEmail FROM INTRANET_PARENTRELATION AS a INNER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE a.ParentID = '$UserID' AND b.RecordStatus = 1 AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') $keyword_cond ";
		//$result = $li->returnArray($sql,3);
		
		/*$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				$x3 .= "<option value='$u_email'>".$u_name."</option>";
			}
		}else{
			$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		$x3 .= "</select>";*/
	}
	if($CatID == 6){
		$NameField = getNameFieldByLang2("b.");
		$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
		$sql = "SELECT b.UserID as UserID, $NameField as UserName, b.IMapUserEmail as UserEmail FROM INTRANET_PARENTRELATION AS a INNER JOIN INTRANET_USER AS b ON (a.ParentID = b.UserID) WHERE a.StudentID = '$UserID' AND b.RecordStatus = 1  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') $keyword_cond ";
		//$result = $li->returnArray($sql,3);
		
		/*$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				$x3 .= "<option value='$u_email'>".$u_name."</option>";
			}
		}else{
			$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		$x3 .= "</select>";*/
	}
	
	if($CatID == "SHARED_MAILBOX"){
		//$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
		$keyword_cond = " WHERE (p.DisplayName LIKE '%".$escaped_keyword."%' OR s.MailBoxName LIKE '%".$escaped_keyword."%') ";
		$sql = "SELECT s.MailBoxID as UserID, IF(p.DisplayName IS NULL OR p.DisplayName = '',s.MailBoxName,p.DisplayName) as UserName, s.MailBoxName as UserEmail FROM MAIL_SHARED_MAILBOX as s LEFT JOIN MAIL_PREFERENCE as p ON p.MailBoxName = s.MailBoxName ".$keyword_cond;
		/*$SharedMailBoxes = $li->returnArray($sql,3);
		if(sizeof($SharedMailBoxes)>0){
			for($i=0;$i<sizeof($SharedMailBoxes);$i++){
				list($mail_box_id,$display_name,$mail_box_name) = $SharedMailBoxes[$i];
				$final_email = htmlspecialchars('"'.$display_name.'"'." <".$mail_box_name."> ",ENT_QUOTES);
				$x3 .= "<option value='".$final_email."'>".htmlspecialchars($display_name,ENT_QUOTES)."</option>";
			}
		}else{
			$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
		}
		$x3 .= "</select>";*/
	}
}

if($GroupOpt == 2 && $CatID == 4 && $ChooseGroupCatID!="")
{
	if($_SESSION['SSV_USER_TARGET']['All-Yes'])
	{
		$sql = "SELECT GroupID, Title FROM INTRANET_GROUP WHERE RecordType = '".$ChooseGroupCatID."' AND RecordType != 0 AND AcademicYearID = '$CurrentAcademicYearID' ORDER BY Title";
	}else{
		
		if($result_to_group_options['ToTeacher'] || $result_to_group_options['ToStaff'] || $result_to_group_options['ToStudent'] || $result_to_group_options['ToParent'] || ($special_feature['alumni'] && $result_to_group_options['ToAlumni']))
		{
			$sql = "SELECT a.GroupID, a.Title FROM INTRANET_GROUP AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.GroupID = b.GroupID) WHERE a.RecordType = '".$ChooseGroupCatID."' AND a.RecordType != 0 AND a.AcademicYearID = '$CurrentAcademicYearID' AND b.UserID = '$UserID' ORDER BY Title";
		}
	}
	$GroupArray = $li->returnArray($sql,2);
	$x2_5 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10'>";
	if(sizeof($GroupArray) > 0){
		$sql = "SELECT MAX(GroupID) FROM INTRANET_GROUP";
		$MaxGroupID = $li->returnVector($sql);
		// init js array to store group alias email
		$js_all_group_email .= "var js_group_email = new Array(".$MaxGroupID[0].");\n";
		
		for($i=0; $i<sizeof($GroupArray); $i++)
		{
			list($GroupID, $GroupName) = $GroupArray[$i];
			if(sizeof($ChooseGroupID)>0)
				$x2_5 .= "<option value='$GroupID' ".((in_array($GroupID,$ChooseGroupID))?"SELECTED":"").">".$GroupName."</option>";
			else
				$x2_5 .= "<option value='$GroupID' >".$GroupName."</option>";
				
			## Get All Related Email in selected Group (for group alias use) ##
			$targetEmail = $IMap->GetAllGroupEmail($GroupID,$result_to_group_options['ToTeacher'],$result_to_group_options['ToStaff'],$result_to_group_options['ToStudent'],$result_to_group_options['ToParent'],$result_to_group_options['ToAlumni']);
			$js_all_group_email .= "js_group_email[".$GroupID."] = '".str_replace("'","\'",$targetEmail)."';\n";
		}
	}else{
		$x2_5 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
	}
	$x2_5 .= "</select>";
	$ShowSubGroupSelection = true;
}
else
{
	$ShowSubGroupSelection = false;
}

### Please Follow the follow logic ###
if($CatID != "" && sizeof($ChooseGroupID)>0)
{
	$sql = "";
	if($CatID == -1) // teaching staff
	{
		for($i=0; $i<sizeof($ChooseGroupID); $i++)
		{			
			if($ChooseGroupID[$i] == 1)
			{
				## All Teaching Staff ##
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
				{
					
					$NameField = getNameFieldWithClassNumberByLang("all_user.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$all_sql = " SELECT all_user.UserID as UserID, $NameField as UserName, all_user.IMapUserEmail as UserEmail FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1  AND (all_user.IMapUserEmail IS NOT NULL AND all_user.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
					$sql = $all_sql;
				}
			}
			if($ChooseGroupID[$i] == 2)
			{
				## Form Teacher ##
				if($identity == "Teaching")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("a.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$form_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID')  AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to form teacher
				}
				if($identity == "Student")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("a.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$form_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND  b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID')  AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("a.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$form_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$UserID AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID')  AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if($ChooseGroupID[$i] == 3)
			{		
				## Class Teacher ##
				if($identity == "Teaching")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("a.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$class_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID') AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to class teacher
				}
				if($identity == "Student")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("a.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$class_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID')  AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("a.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$class_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID')  AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
			}
			if($ChooseGroupID[$i] == 4)
			{
				## Subject Teacher ##
				if($identity == "Teaching")
				{					
					
					$NameField = getNameFieldWithClassNumberByLang("a.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$subject_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')  AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to subject teacher
				}
				if($identity == "Student")
				{					
					
					$NameField = getNameFieldWithClassNumberByLang("a.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$subject_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')  AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{					
					
					$NameField = getNameFieldWithClassNumberByLang("a.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$subject_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')  AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if($ChooseGroupID[$i] == 5)		/* start from here */
			{
				## Subject Group Teacher ##
				if($identity == "Teaching")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("a.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$subject_group_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID')  AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to subject group teacher
				}
				if($identity == "Student")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("a.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$subject_group_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID')  AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("a.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$subject_group_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID')  AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}
		}
		$result = $li->returnArray($sql);
	}
	if($CatID == 2) // student
	{
		for($i=0; $i<sizeof($ChooseGroupID); $i++)
		{
			if($ChooseGroupID[$i] == 1)
			{
				## All Student ##
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
				{
					
					$NameField = getNameFieldWithClassNumberByLang('a.');
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$all_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail FROM INTRANET_USER as a WHERE a.RecordType = 2 AND a.RecordStatus = 1  AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//" ORDER BY IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName ";
					$sql = $all_sql;
				}
			}
			if($ChooseGroupID[$i] == 2)
			{
				## My Form Student ##
				if($identity == "Teaching")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("b.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$form_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, b.IMapUserEmail as UserEmail FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to form student
				}
				if($identity == "Student")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("b.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$form_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, b.IMapUserEmail as UserEmail FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = $UserID AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') $keyword_cond  ";//"ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("b.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$form_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, b.IMapUserEmail as UserEmail FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if($ChooseGroupID[$i] == 3)
			{
				## My Class Student ##
				if($identity == "Teaching")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("b.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$class_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, b.IMapUserEmail as UserEmail FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to class student
				}
				if($identity == "Student")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("b.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$class_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, b.IMapUserEmail as UserEmail FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("b.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$class_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, b.IMapUserEmail as UserEmail FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
			}
			if($ChooseGroupID[$i] == 4)
			{
				## My Subject Student ##
				if($identity == "Teaching")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("b.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$subject_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, b.IMapUserEmail as UserEmail FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to Subject student
				}
				if($identity == "Student")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("b.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$subject_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, b.IMapUserEmail as UserEmail FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{
					
					$NameField = getNameFieldWithClassNumberByLang("b.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$subject_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, b.IMapUserEmail as UserEmail FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = $UserID AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if($ChooseGroupID[$i] == 5)
			{		
				## My Subject Group Student ##		
				if($identity == "Teaching")
				{	
					
					$NameField = getNameFieldWithClassNumberByLang("b.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$subject_group_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, b.IMapUserEmail as UserEmail FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to subject group student
				}
				if($identity == "Student")
				{	
					
					$NameField = getNameFieldWithClassNumberByLang("b.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$subject_group_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, b.IMapUserEmail as UserEmail FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{	
					
					$NameField = getNameFieldWithClassNumberByLang("b.");
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$subject_group_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, b.IMapUserEmail as UserEmail FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}
		}
		$result = $li->returnArray($sql);
	}
	if($CatID == 3) // parent
	{
		for($i=0; $i<sizeof($ChooseGroupID); $i++)
		{
			if($ChooseGroupID[$i] == 1)
			{
				## All Parents ##
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
				{					
					
					$NameField = "IF(c.EnglishName != '' AND c.EnglishName IS NOT NULL,
									  IF(c.ClassName != '' AND c.ClassName IS NOT NULL and c.ClassNumber != '' AND c.ClassNumber IS NOT NULL,
											CONCAT('(',c.ClassName,'-',c.ClassNumber,') ', ".getNameFieldByLang2('c.').",'".$Lang['iMail']['FieldTitle']['TargetParent']."', IF(a.EnglishName !='' AND a.EnglishName IS NOT NULL,CONCAT(' (',".getNameFieldByLang2('a.').",')'),'')),
											".getNameFieldByLang2('a.')."),
									  IF(TRIM(a.EnglishName)='' AND TRIM(a.ChineseName)='',a.UserLogin,".getNameFieldByLang2('a.').")
									)";
					$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
					$all_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail  FROM INTRANET_USER AS a LEFT OUTER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) LEFT OUTER JOIN INTRANET_USER AS c ON (b.StudentID = c.UserID) WHERE a.RecordType = 3 AND a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(c.ClassName,''), IFNULL(c.ClassNumber,0), c.EnglishName)";
					$sql = $all_sql;
				}
			}
			if($ChooseGroupID[$i] == 2)
			{
				## My Form Parents ##
				if($identity == "Teaching")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					
					$keyword_cond = " AND $name_field LIKE '%".$escaped_keyword."%' ";
					$form_sql = " SELECT DISTINCT c.UserID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to form parent
				}
				if($identity == "Student")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					
					$keyword_cond = " AND $name_field LIKE '%".$escaped_keyword."%' ";
					$form_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					
					$keyword_cond = " AND $name_field LIKE '%".$escaped_keyword."%' ";
					$form_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if($ChooseGroupID[$i] == 3)
			{
				## My Class Parents ##
				if($identity == "Teaching")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					
					$keyword_cond = " AND $name_field LIKE '%".$escaped_keyword."%' ";
					$class_sql = " SELECT DISTINCT c.UserID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID')) AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to class parent
				}
				if($identity == "Student")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					
					$keyword_cond = " AND $name_field LIKE '%".$escaped_keyword."%' ";
					$class_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID')) AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					
					$keyword_cond = " AND $name_field LIKE '%".$escaped_keyword."%' ";
					$class_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
			}
			if($ChooseGroupID[$i] == 4)
			{
				## My Subject Parents ##
				if($identity == "Teaching")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					
					$keyword_cond = " AND $name_field LIKE '%".$escaped_keyword."%' ";
					$subject_sql = " SELECT DISTINCT c.UserID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to subject parent
				}
				if($identity == "Student")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					
					$keyword_cond = " AND $name_field LIKE '%".$escaped_keyword."%' ";
					$subject_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					
					$keyword_cond = " AND $name_field LIKE '%".$escaped_keyword."%' ";
					$subject_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if($ChooseGroupID[$i] == 5)
			{
				## My Subject Group Parents ##
				if($identity == "Teaching")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					
					$keyword_cond = " AND $name_field LIKE '%".$escaped_keyword."%' ";
					$subject_group_sql = " SELECT DISTINCT c.UserID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to subject group parent
				}
				if($identity == "Student")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					
					$keyword_cond = " AND $name_field LIKE '%".$escaped_keyword."%' ";
					$subject_group_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID')) AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{
					
					$name_field = getParentNameWithStudentInfo("a.","c.");
					
					$keyword_cond = " AND $name_field LIKE '%".$escaped_keyword."%' ";
					$subject_group_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, c.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID'))  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') $keyword_cond ";//"ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}			
		}
		$result = $li->returnArray($sql);
	}
	if($CatID == 4) // Group
	{
		if(sizeof($ChooseGroupID)>0)
		{			
			$TargetGroupID = implode(",",$ChooseGroupID);
			
			$cond = "";
			if($result_to_group_options['ToTeacher'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 1 AND (a.Teaching = 1)) ";
			}
			if($result_to_group_options['ToStaff'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 1 AND (a.TEACHING = 0 OR a.Teaching IS NULL)) ";
			}
			if($result_to_group_options['ToStudent'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 2) ";
			}
			if($result_to_group_options['ToParent'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 3) ";
			}
			if($special_feature['alumni'] && $result_to_group_options['ToAlumni'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 4) ";
			}
			
			if($cond != "")
				$final_cond = " AND ( $cond ) ";
			$NameField = getNameFieldWithClassNumberByLang("a.");
			$keyword_cond = " AND $NameField LIKE '%".$escaped_keyword."%' ";
			$sql = "SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.IMapUserEmail as UserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.GroupID IN ($TargetGroupID)  AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') $final_cond $keyword_cond ";//"ORDER BY a.RecordType, IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName";
			$result = $li->returnArray($sql,2);
		}
	}
	/*
	$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10 >";
	if(sizeof($result)>0){
		for($i=0; $i<sizeof($result); $i++){
			list($u_id, $u_name, $u_email) = $result[$i];
			$final_email = '"'.$u_name.'"'." <".$u_email."> ";
			//$x3 .= "<option value='$u_email'>$u_name</option>";
			$x3 .= "<option value='$final_email'>$u_name</option>";
		}
	}else{
		$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
	}
	$x3 .= "</select>";*/
}

$direct_to_step3 = false;
if($CatID != "")
{
	if($GroupOpt == 3 && ($CatID == "SHARED_MAILBOX")){
		$direct_to_step3 = true;
	}else if(($GroupOpt == 1) && (($CatID == -3)||($CatID == -2)||($CatID == 5)||($CatID == 6)) || ($CatID == -4)){
		$direct_to_step3 = true;
	}else if(($GroupOpt == 2) && ($CatID == 4) && (sizeof($ChooseGroupID)>0) && (sizeof($ChooseGroupCatID)>0)) {
		$direct_to_step3 = true;
	} else {
		if(($GroupOpt == 1) && ($CatID != 4) && (sizeof($ChooseGroupID)>0)){
			$direct_to_step3 = true;
		}
	}
}

$TabID = 1;
# TABLE INFO
$litable->field_array = array("UserName","UserEmail");
$litable->sql = $sql;
$litable->no_col = sizeof($litable->field_array)+3;
$litable->IsColOff = "imail_addressbook_list";
$litable->title = "";
$litable->count_mode="1";

# TABLE COLUMN
$pos = 0;
$litable->column_list .= "<td class=\"tabletoplink\" width=\"1\">#</td>\n";
$litable->column_list .= "<td class=\"tabletoplink\" width=\"20\">&nbsp;</td>\n";
$litable->column_list .= "<td class=\"tabletoplink\" width=\"50%\">".$litable->column($pos++, "$i_CampusMail_New_AddressBook_Name")."</td>\n";
$litable->column_list .= "<td class=\"tabletoplink\" width=\"50%\">".$litable->column($pos++, "$i_CampusMail_New_AddressBook_EmailAddress")."</td>\n";
$litable->column_list .= "<td class=\"tabletoplink\" width=\"0\"></td>\n";
//$litable->column_array = array(0,0);
//$litable->wrap_array = array(0,0);

# SEARCH BUTTON
$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\" align=\"right\"><tr>";
//$searchTag 	.= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag  .= "<td>".$linterface->Get_Search_Box_Div("keyword", stripslashes($keyword),"")."</td>";
//$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
$searchTag 	.= "</tr></table>";

### Title ###
//$iMailImageEx = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_address_ex.gif' align='absmiddle' />";
//$iMailTitleEx = "<span class='imailpagetitle'>". $i_CampusMail_External_Recipient ."</span>";
//$iMailImageIn = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_address_ingroup.gif' align='absmiddle' style=''/>";
//$iMailTitleIn = "<span class='imailpagetitle'>".$Lang['Gamma']['InternalRecipient']."</span>";
//$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$iMailImageIn.$iMailTitleIn."</td><td align='right' style=\"vertical-align: bottom;\"  nowrap>".$IMap->TitleToolBar()."</td></tr></table>";
//$TAGS_OBJ[] = array($iMailTitle, "", 0);
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($i_CampusMail_External_Recipient,$PATH_WRT_ROOT."home/imail_gamma/addressbook.php",0);
$TAGS_OBJ[] = array($Lang['Gamma']['InternalRecipient'],$PATH_WRT_ROOT."home/imail_gamma/addressbook_internal.php", 1);

$linterface->LAYOUT_START();

?>
<script language="javascript">
var AllTeacherEmail = '<?=$js_all_teacher;?>';
var AllFormTeacherEmail = '<?=$js_all_form_teacher;?>';
var AllClassTeacherEmail = '<?=$js_all_class_teacher;?>';
var AllSubjectTeacherEmail = '<?=$js_all_subject_teacher;?>';
var AllSubjectGroupTeacherEmail = '<?=$js_all_subject_group_teacher;?>';

var AllStudentEmail = '<?=$js_all_student;?>';
var AllFormStudentEmail = '<?=$js_all_form_student;?>';
var AllClassStudentEmail = '<?=$js_all_class_student;?>';
var AllSubjectStudentEmail = '<?=$js_all_subject_student;?>';
var AllSubjectGroupStudentEmail = '<?=$js_all_subject_group_student;?>';

var AllParentEmail = '<?=$js_all_parent;?>';
var AllFormParentEmail = '<?=$js_all_form_parent;?>';
var AllClassParentEmail = '<?=$js_all_class_parent;?>';
var AllSubjectParentEmail = '<?=$js_all_subject_parent;?>';
var AllSubjectGroupParentEmail = '<?=$js_all_subject_group_parent;?>';

<?=$js_all_group_email;?>

function checkOptionNone(obj){
	if(obj==null)return;
       for(i=0; i<obj.length; i++){
                obj.options[i].selected = false;
        }
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

<?php if($direct_to_step3){ ?>
function getFormatDate(dateObj)
{
	var date_year = dateObj.getFullYear();
	var date_month = dateObj.getMonth()+1;
	var date_day = dateObj.getDate();
	date_month = date_month < 10? '0'+date_month : ''+date_month; 
	date_day = date_day < 10 ? '0'+date_day : ''+date_day; 
	var date_string = date_year + '-' + date_month + '-' + date_day;
	
	return date_string;
}

function viewRelatedMails(email)
{
	var dateObj = new Date();
	var end_date = getFormatDate(dateObj);
	dateObj.setFullYear(dateObj.getFullYear()-1);
	var start_date = getFormatDate(dateObj);
	
	var tmp_form_html = '<form id="tmp_form" name="tmp_form" method="get" action="search_result.php">';
	tmp_form_html += '<input type="hidden" name="keyword" value="'+email+'" />';
	tmp_form_html += '<input type="hidden" name="isAdvanceSearch" value="0" />';
	tmp_form_html += '<input type="hidden" name="FromSearch" value="1" />';
	tmp_form_html += '<input type="hidden" name="FromIntAddressBook" value="1" />';
	tmp_form_html += '<input type="hidden" name="DateRangeMethod" value="1year" />';
	tmp_form_html += '<input type="hidden" name="FromDate" value="'+start_date+'" />';
	tmp_form_html += '<input type="hidden" name="ToDate" value="'+end_date+'" />';
	tmp_form_html += '</form>';
	
	if($('#tmp_form').length > 0)
	{
		$('#tmp_form').remove();
	}
	$('form[name="form1"]').after(tmp_form_html);
	$('#tmp_form').submit();
	if($('#tmp_form').length > 0)
	{
		$('#tmp_form').remove();
	}
}

function initAllLinks()
{
	var row1 = $('tr.tablerow1');
	var row2 = $('tr.tablerow2');
	
	for(var i=0;i<row1.length;i++){
		var tr = $(row1.get(i));
		var td2 = tr.find('td:eq(2)');
		var td3 = tr.find('td:eq(3)');
		var td2_text = $.trim(td2.text());
		var td3_text = $.trim(td3.text());
		td2.html('<a href="javascript:void(0);" onclick="viewRelatedMails(\''+td2_text+'\')" class="tablelink" title="<?=$Lang['Gamma']['ViewRelatedEmails']?>">'+td2_text+'</a>');
		td3.html('<a href="javascript:void(0);" onclick="viewRelatedMails(\''+td3_text+'\')" class="tablelink" title="<?=$Lang['Gamma']['ViewRelatedEmails']?>">'+td3_text+'</a>');
	}
	
	for(var i=0;i<row2.length;i++){
		var tr = $(row2.get(i));
		var td2 = tr.find('td:eq(2)');
		var td3 = tr.find('td:eq(3)');
		var td2_text = $.trim(td2.text());
		var td3_text = $.trim(td3.text());
		td2.html('<a href="javascript:void(0);" onclick="viewRelatedMails(\''+td2_text+'\')" class="tablelink" title="<?=$Lang['Gamma']['ViewRelatedEmails']?>">'+td2_text+'</a>');
		td3.html('<a href="javascript:void(0);" onclick="viewRelatedMails(\''+td3_text+'\')" class="tablelink" title="<?=$Lang['Gamma']['ViewRelatedEmails']?>">'+td3_text+'</a>');
	}
}

$(document).ready(function(){
	initAllLinks();
});
<?php } ?>
</script>
<br />
<form name="form1" method="post" action="<?= $_SERVER['PHP_SELF']?>">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
   <td align="center">
      <table width="96%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
			<td valign="top" nowrap="nowrap" width="30%" >
				<span class="tabletext"><?=$Lang['iMail']['FieldTitle']['Choose'];?>:</span>
			</td>
			<td >
				<?=$x1;?>
			</td>
		</tr>
		
		<?php 
		if($CatID != "")
		{
			if($CatID != 0 && $CatID!=-2 && $CatID!=5 && $CatID!=6 && $CatID!=-3 && $CatID!=-4)
			{
		?>
		
		<? if(($GroupOpt == 1) && ($CatID != 4)) { ?>
		<tr> 
			<td height="5" colspan="2"  >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
			</td>
		</tr>
		<tr> 
			<td height="1" colspan="2" class="dotline" valign="middle" >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
			</td>
		</tr>
		<tr> 
			<td height="5" colspan="2"  >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
			</td>
		</tr>
		
		<tr>
			<td valign="top" nowrap="nowrap" width="30%" >
			<span class="tabletext"><?=$Lang['iMail']['FieldTitle']['SubCategory'];?>:</span>
			</td>
			<td >					
			<table cellpadding="0" cellspacing="0" >
			<tr>
				<td><?=$x2?></td>
				<td style="vertical-align:bottom">
				<table cellpadding="0" cellspacing="6" >				
				<?php 
				if($CatID!=4 && $CatID!=999)
				{
				?>	
				<?php 
				}
				?>
				<?php 
					if($CatID==999)
					{
				?>
				<? 
					}
					if($CatID != 4)
					{ 
				?>
					<tr>
						<td>
						<!-- Button for add recipient (ONLY Teaching, Student & Parent) -->
						<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
						</td>
					</tr>	
				<?
					}
				?>
				
				<?php 
				if(!$sys_custom['Mail_NoSelectAllButton']) 
				{ 
				?>
					<tr >
						<td >
						<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['ChooseGroupID[]']); return false;","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
						</td >
					</tr >
				<?php 
				} 
				?>
				</table>	
				</td>
			</tr>
			</table>			
			</td>
		</tr>
		<? } ?>
		<?php 
			}
		}
		?>
		<? 
		### Show Group Multi Selection Box
		if($ShowSubGroupSelection)
		{
		?>
		
			<tr> 
				<td height="5" colspan="2"  >
				<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
				</td>
			</tr>
			<tr> 
				<td height="1" colspan="2" class="dotline">
				<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
				</td>
			</tr>
			<tr> 
				<td height="5" colspan="2"  >
				<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
				</td>
			</tr>
		
			<tr>
				<td valign="top" nowrap="nowrap" width="30%" >
					<span class="tabletext"><?=$Lang['iMail']['FieldTitle']['Group'];?>:</span>
				</td>
				<td >
				  <table border="0" cellpadding="0" cellspacing="0" align="left">		
					<tr >
						<td ><?=$x2_5?></td>
						<td valign="bottom" >
							<table cellpadding="0" cellspacing="6" >
							<tr>
							<td >
								
							</td >
							</tr>
							<tr>
								<td>
								<!-- Button for add recipient (ONLY Teaching, Student & Parent) -->
								<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit", "checkOption(this.form.elements['ChooseGroupID[]']);","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
								</td>
							</tr>
							<?php 
								if(!$sys_custom['Mail_NoSelectAllButton']) 
								{	 
							?>
							<tr >
								<td >
								<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['ChooseGroupID[]']); return false;","submit2"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
								</td >
							</tr >
							<?php 
								} 
							?>
						    </table>
					   </td>
				    </tr>
				  </table>
			  </td>
		   </tr>
		<?
		}
		?>
		
		<?
		if($direct_to_step3){
		?>
		
		<tr> 
			<td height="5" colspan="2"  >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
			</td>
		</tr>
		<tr> 
			<td height="1" colspan="2" class="dotline">
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
			</td>
		</tr>
		<tr> 
			<td height="5" colspan="2"  >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
			</td>
		</tr>
		
		<tr>
			<td colspan="2"><?=$searchTag?></td>
		</tr>
		<tr>
			<td colspan="2"><?=$litable->display()?></td>
		</tr>
		
		<?php 
		} 
		?>
	
        </table>
    </td>
</tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $litable->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $litable->order; ?>">
<input type="hidden" name="field" value="<?php echo $litable->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$litable->page_size?>">
<input type="hidden" name="TabID" value="<?=escape_double_quotes($TabID)?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>