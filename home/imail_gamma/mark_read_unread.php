<?php
// editing by 
/*
 * 2013-01-17 (Carlos): Log down mark email as unread action for tracing case 2013-0103-1343-08073 St Margaret's Girls' College
 */
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libaccess.php");
include("../../includes/libwebmail.php");
include("../../includes/imap_gamma.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$IMap = new imap_gamma();

## Get Data
$Uid = (isset($Uid)) ? $Uid : "";		// needs to be array format
if(is_array($Uid)){
	$UIDList = $Uid;
} else {
	if($Uid != ""){ $UIDList[0] = $Uid; }
}

$Folder = stripslashes($Folder);
if($Folder == '') $Folder = $IMap->InboxFolder;
if(count($UIDList) > 0){
	for ($i=0; $i<sizeof($UIDList) ; $i++) {
		if (stristr($UIDList[$i], ",") !== false) {
			List($UID, $Folder) = explode(",", $UIDList[$i]);
		}
		else {
			$UID = $UIDList[$i];
			//$Folder = $_GET['Folder'];
		}
		
		if($MarkAs=="UNSEEN") {
			$result = $IMap->unreadMail($Folder,$UID);
			
			$log_content = "Action:Unread,\n<br>UserID:".$_SESSION['UserID'].",\n<br>Email:".$_SESSION['SSV_LOGIN_EMAIL'].",\n<br>Folder:$Folder,\n<br>UID:$UID";
			$IMap->Log_Delete_Action($log_content);
		}else if($MarkAs=="FLAGGED")
			$result = $IMap->Set_Flagged($Folder,$UID);
		else
			$result = $IMap->readMail($Folder,$UID);
		
//		$Result = $IMap->Move_Mail($Folder, $UID, $targetFolder);
	}
}

$url = $_SERVER["HTTP_REFERER"];

intranet_closedb();
header("Location: $url");
?>