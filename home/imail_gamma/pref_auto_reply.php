<?php

## Using By : 
################ Change Log [Start] #####################
#	2015-04-28 (Carlos): $sys_custom['iMailPlus']['DisableStudentAutoReply'] - disable auto reply for student type
#	2014-09-22 (Carlos): set form onsubmit event to return false
#
#	Date	:	2010-11-18 [Yuen]
#	Details :	removed unnecessary rows above the settings
#
################ Change Log [End] #####################

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

if($_SESSION['UserType']==USERTYPE_STUDENT && $sys_custom['iMailPlus']['DisableStudentAutoReply']){
	intranet_closedb();
	header("Location: index.php");
	exit;
}

$lwebmail = new libwebmail();
$IMap = new imap_gamma();

//if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
//{
//    include_once("../../includes/libsystemaccess.php");
//    $lsysaccess = new libsystemaccess($UserID);
//    if ($lsysaccess->hasMailRight())
//    {
//        $noWebmail = false;
//    }
//    else
//    {
//        $noWebmail = true;
//    }
//}
//else
//{
//    $noWebmail = true;
//}
$TabID = 5;

# Block No webmail no preference
//if ($noWebmail)
//{
//    header("Location: index.php");
//    exit();
//}

## Retrieve auto reply message
$lu = new libuser($UserID);

//$auto_reply_content = $IMap->readAutoReply($IMap->CurUserAddress);
$pref = $IMap->Get_Preference();
$auto_reply_content = $pref["AutoReplyTxt"];
$auto_reply_content = stripslashes($auto_reply_content);
$auto_reply_enabled = ($auto_reply_content != "");

$CurrentPage = "PageSettings_AutoReply";
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array("<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$i_CampusMail_New_Settings_AutoReply."<td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>", "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();


if ($msg == "2")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("update")."</td></tr>";
} else {
	$xmsg = "";
}

?>
<script language='javascript'>
function setAutoReply(){
        objAutoReplyContent = document.form1.reply_content;
        if(objAutoReplyContent!=null)
                objAutoReplyContent.disabled = !objAutoReplyContent.disabled;
}
function resetForm(obj){
        if(obj==null) return;
        objEnable = obj.enable_auto_reply;
        objContent = obj.reply_content;

        if(objEnable==null || objContent==null ) return;

        obj.reset();

        objContent.disabled = !objEnable.checked;
}
function checkform(obj){
        if(obj==null) return false;
        if (obj.enable_auto_reply.checked)
        {
            if (!check_text(obj.reply_content,"Please fill in content.")) return false;
        }

        return true;
}
function submitForm(obj){
        if(checkform(obj))
                obj.submit();
}
</script>
<form name=form1 method="POST" action="pref_update.php" onsubmit="return false;">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">	
	<?=$xmsg?>
	<tr>
		<td>
		<table border="0" cellpadding="5" cellspacing="1" align="center" width="90%" class="tabletext" >

			<tr>
				<td></td>
				<td colspan=2>
				<input type=checkbox name=enable_auto_reply value="1" onClick='setAutoReply()' <?php if($auto_reply_enabled) echo "CHECKED";?>><?=$i_CampusMail_New_Settings_EnableAutoReply?>
				</td>
			</tr>
			<tr>
				<td width=100></td>
				<td colspan=2><?=$Lang['Gamma']['AutoReplyInstruction']?>
				</td>
			</tr>
			<tr>
				<td></td>
				<td ALIGN=left colspan=2>
				<textarea rows=10 cols=70 name="reply_content" <?=($auto_reply_enabled?"":"disabled")?>><?=$auto_reply_content?></textarea>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	</table>
</tr>
<tr>
	<td>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_save, "submit", "submitForm(document.form1)") ?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "button", "resetForm(document.form1)") ?>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="TabID" value="<?=$TabID?>" />
</form>

<?
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>