<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccess.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

$li = new libdb();

$list = "'".implode("','",$AddressID)."'";
$sql = "DELETE FROM INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL WHERE OwnerID = '$UserID' AND AddressID IN ($list)";
$li->db_db_query($sql);

## Delete the record in the External Recipient Group ##
$sql = "SELECT COUNT(EntryID) FROM INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY WHERE TargetID IN ($list)";
$tmp_result = $li->returnVector($sql);
if($tmp_result[0] > 0){
	$sql = "DELETE FROM INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY WHERE TargetID IN ($list)";
	$li->db_db_query($sql);
}

intranet_closedb();

$anc = $special_option['no_anchor'] ?"":"#anc";

header("Location: addressbook.php?field=$field&order=$order&pageNo=$pageNo&keyword=$keyword&msg=3&TabID=$TabID&xmsg=delete".$anc);
?>