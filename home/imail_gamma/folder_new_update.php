<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccess.php");
include_once("../../includes/imap_gamma.php");
//include_once("../../includes/libcampusmail.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

$IMap = new imap_gamma();
//$lc = new libcampusmail();


$CurFolder = $IMap->FolderPrefix;
$FolderName = stripslashes(trim($FolderName));

$FolderName = IMap_Encode_Folder_Name($FolderName);

$Result = $IMap->createMailFolder($CurFolder, $FolderName);

intranet_closedb();
if(!$Result)
	header("Location: folder_new.php?err=1");
else
	header("Location: folder.php?msg=1");
?>