<?php
//editing by 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;
$CurrentPage = "PageAddressBook_ExternalReceipient";

$IMap = new imap_gamma();
$lwebmail = new libwebmail();
$linterface = new interface_html();

if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
{
    include_once($PATH_WRT_ROOT."includes/libsystemaccess.php");
    $lsysaccess = new libsystemaccess($UserID);
    if ($lsysaccess->hasMailRight())
    {
        $noWebmail = false;
    }
    else
    {
        $noWebmail = true;
    }
}
else
{
    $noWebmail = true;
}

$noWebmail = false;
if ($noWebmail)
{
    header("Location: addressbook.php?TabID=2");
    exit();
}

$AddressID = (is_array($AddressID)? $AddressID[0]: $AddressID);
$li = new libdb();

$sql = "SELECT AddressID, OwnerID, TargetName, TargetAddress FROM INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL WHERE AddressID = '$AddressID'";
$result = $li->returnArray($sql,5);
list($AddressID, $OwnerID, $TargetName, $TargetAddress) = $result[0];
if ($OwnerID != $UserID)
{
    header("Location: addressbook.php");
    exit();
}

### Title ###
/*
$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_address_ex.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='imailpagetitle'>". $i_CampusMail_External_Recipient ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleImage1.$TitleTitle1."</td><td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>";
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($TitleTitle, "", 0);
*/
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($i_CampusMail_External_Recipient,$PATH_WRT_ROOT."home/imail_gamma/addressbook.php",1);
$TAGS_OBJ[] = array($Lang['Gamma']['InternalRecipient'],$PATH_WRT_ROOT."home/imail_gamma/addressbook_internal.php",0);

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit .($intranet_session_language=="en"?" ":""). $i_Circular_Recipient, "");

?>
<script language="javascript">
function checkform(obj){
	if(obj==null) return false;
	objName = obj.TargetName;
	objAddress = obj.TargetAddress;
	if(objName ==null || objAddress == null)
		return false;
		
	if(!validateEmailAddress(objAddress.value)){
		alert("<?=$i_invalid_email?>");
		objAddress.focus();
		return false;
	}
			
	if(objName.value.indexOf(";")>-1){
		alert("<?=$i_CampusMail_Warning_External_Recipient_No_Semicolon?>");
		objName.focus();
		return false;
	}
	if(objName.value.indexOf("<")>-1){
		alert("<?=$Lang['iMail']['JSWarning']['ExternalRecipientNoTriangleBrackets1'];?>");
		obj.focus();
		return false;
	}
	if(objName.value.indexOf(">")>-1){
		alert("<?=$Lang['iMail']['JSWarning']['ExternalRecipientNoTriangleBrackets2'];?>");
		obj.focus();
		return false;
	}	
}
function validateEmailAddress(email){

        //var re = /^.+@.+\..{2,3}$/;
        var re = /^([0-9A-Za-z_\.\-])+\@(([0-9A-Za-z\-])+\.)+([0-9A-Za-z]{2,3})+$/
        
        if (re.test(email)) {
                return true;
        }else{
                return false;
        }
}
<!--
/*
function checkform(obj)
{
     if(!check_text(obj.TargetName, "<?php echo $i_alert_pleasefillin.$i_CampusMail_New_AddressBook_Name; ?>.")) return false;
     if(!check_text(obj.TargetAddress, "<?php echo $i_alert_pleasefillin.$i_CampusMail_New_AddressBook_EmailAddress; ?>.")) return false;
}    
*/
//-->

</script>

<br />
<form name="form1" method="post" ACTION="address_edit_update.php" onSubmit="return checkform(this);">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$xmsg?></td>
</tr>
<tr>
	<td colspan="2" align="center">
        	<table width="90%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_CampusMail_New_AddressBook_Name?> <span class='tabletextrequire'>*</span></span></td>
                                        <td><input name="TargetName" type="text" value='<?=$TargetName?>' class="textboxtext" maxlength="255" /></td>
				</tr>
                                <tr valign="top">
                                	<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_CampusMail_New_AddressBook_EmailAddress?> <span class='tabletextrequire'>*</span></span></td>
                                        <td><input name="TargetAddress" type="text" value='<?=$TargetAddress?>' class="textboxtext" maxlength="255" /></td>
				</tr>
                                </table>
			</td>
		</tr>
                </table>
        </td>
</tr>        
<tr>
	<td colspan="2" align="center">
        	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back();","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table> 
        </td>
</tr>        
</table>

<input type="hidden" name="pageNo" value="<?php echo $pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $order; ?>">
<input type="hidden" name="field" value="<?php echo $field; ?>">
<input type="hidden" name="keyword" value="<?php echo $keyword; ?>">
<input type="hidden" name="AddressID" value="<?=$AddressID?>">
<input type="hidden" name="TabID" value="<?=$TabID?>">
</form>

<?
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.TargetName");
$linterface->LAYOUT_STOP();
?>