<?php
// page modifing by: 
$PathRelative = "../../../../";
$CurSubFunction = "Communication";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}
AuthPage(array("Communication-Mail-GeneralUsage"));
include_once($PathRelative."src/include/template/general_header.php");
//include_once($PathRelative."src/include/class/mailtable.php");
include_once($PathRelative."src/include/class/imap_gamma.php");
include_once($PathRelative."src/include/class/imap_gamma_ui.php");
include_once($PathRelative."src/include/class/imap_preference_ui.php");
include_once($PathRelative."src/include/class/gmail_api_agent.php");
######################################################################################################

## Use Library
$lui  = new imap_gamma_ui();
$pui  = new imap_preference_ui();
$IMap = new imap_gamma();
if ($_SESSION['SSV_USER_TYPE'] == "S") {
	// Gmail API setup
	$GmailApi = new gmail_api_agent();
}
else {
	// IMAP setup
	$IMap = new imap_gamma();
}

## Get Data
$CurMenu = '3';
$CurTag = (isset($CurTag) && $CurTag != "") ? stripslashes(urldecode($CurTag)) : '1';
$Msg 	= (isset($msg) && $msg != "") ? $msg : "";
$Folder = (isset($Folder) && $Folder != "") ? stripslashes(urldecode($Folder)) : $SYS_CONFIG['Mail']['FolderPrefix'];


## Initialization
$UpdateFail = false;
$UpdateSuccess = false;

if (isset($_POST["btnSubmit"]))
{
	$forwardAddress = trim(Get_Request($_POST["txtForwardAddress"]));
	if (IsValidEmail($forwardAddress) || ($_POST["EmailForward"] != "1"))
	{
		$user = new user($_SESSION['SSV_USERID']);
		$userEmail = $user->Email;
		$userAlias = $user->Get_User_Alias_List();
		$emailList = array();
		$emailList[] = $_SESSION['SSV_LOGIN_EMAIL'];
		
		//old logic, 20091210 Joe
		/*
		($userEmail != $forwardAddress) ? $passport = true : $passport = false;
		
		if (($_SESSION['SSV_LOGIN_EMAIL'] == $userEmail) && ($passport) && $_SESSION['SSV_USER_TYPE'] != "S")
		{	
			for ($i = 0; $i < count($userAlias); ++$i)
			{
				(($userEmail != $userAlias[$i]["EmailAlias"]) && ($forwardAddress != $userAlias[$i]["EmailAlias"]) && ($passport)) ? $passport = true : $passport = false;
				
				if ($passport) {
					for ($j = 0; $j < count($emailList); ++$j)
					{
						(($userAlias[$i]["EmailAlias"] != $emailList[$j]) && ($passport)) ? $passport = true : $passport = false;
					}
					if ($passport)
						$emailList[] = $userAlias[$i]["EmailAlias"];
				}
			}
		}
		*/
		
		$passport = ($userEmail != $forwardAddress);
		if (is_array($userAlias))
		{
			for ($i = 0; $i < count($userAlias) && $passport; ++$i)
            {
            	$passport = $passport && ($forwardAddress != $userAlias[$i]["EmailAlias"]);
            }
		}
		
		if ($passport) //not forwarding to itself 
		{
			if (is_array($userAlias))
			{
				for ($i = 0; $i < count($userAlias); ++$i)
                {
                	$emailList[] = $userAlias[$i]["EmailAlias"];
                }
			}
			$emailList = array_unique($emailList);
			
			if ($_SESSION['SSV_USER_TYPE'] != "S") {
				if ($_POST["EmailForward"] == "1") 
				{
					$UpdateSuccess = $IMap->setMailForward($forwardAddress, (($_POST["KeepLocalCopy"] == "1") ? true : false), $emailList);
				}
				else
				{
					$UpdateSuccess = $IMap->removeMailForward($emailList);
				}
			}
			else {
				$UpdateSuccess = $GmailApi->Set_Auto_Forward(($_POST["EmailForward"] == "1"),$forwardAddress,($_POST["KeepLocalCopy"] == "1"));
			}
		}
		$UpdateFail = !$UpdateSuccess;
	}
	else
	{
		$UpdateFail = true;
	}
}


## Preparation
##############################################################################
# Imap use only
//$imap = imap_open("{" . $server . ":" . $port . "}" . $mailbox, $username, $password);
imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$Folder);		# Go to the Corresponding Folder Currently selected

$imap_obj 	   = imap_check($IMap->inbox);
$unseen_status = imap_status($IMap->inbox, "{".$IMap->host."}".$Folder, SA_UNSEEN);
$statusInbox   = imap_status($IMap->inbox, "{".$IMap->host."}Inbox", SA_UNSEEN);

$imapInfo['imap_obj'] 	   = $imap_obj;
$imapInfo['unseen_status'] = $unseen_status;
$imapInfo['statusInbox']   = $statusInbox;
##############################################################################

# get Folder Structure for select box
$FolderArray = $IMap->Get_Folder_Structure();


if ($UpdateSuccess)
{
	$ReturnMessage = $Lang['ReturnMsg']['UpdatePrefSuccess']; 
}
else if ($UpdateFail)
{
	$ReturnMessage = $Lang['ReturnMsg']['UpdatePrefFail'];
}
else
{
	$ReturnMessage = "";
}

## Main
echo $lui->Get_Div_Open("module_imail", 'class="module_content"');

echo $lui->Get_Sub_Function_Header($Lang['email_gamma']['HeaderTitle'], $ReturnMessage);
echo $lui->Get_Switch_MailBox();

# Left Menu
echo $lui->Get_Left_Sub_Mail_Menu($CurMenu, $CurTag, $IMap, $Folder, $imapInfo);

# Right Menu Render the preference panel
echo $pui->Get_Preference_Forward(false, true);

######################################################################################################
include_once($PathRelative."src/include/template/general_footer.php");
?>