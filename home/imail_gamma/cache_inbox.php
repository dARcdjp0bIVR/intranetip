<?php
// page modifing by: Michael Cheung
$OverAllTime = microtime(true);
$PathRelative = "../../../";
$CurSubFunction = "Communication";
$CurPage = $_SERVER["REQUEST_URI"];
$SkipAutoRedirect = true;

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	if ($SessionStatus == 1) {
		$Msg = $SYS_CONFIG['SystemMsg']['SessionExpired'];
	}
	else {
		$Msg = $SYS_CONFIG['SystemMsg']['SessionKicked'];
	}
?>
		<script>
		window.top.location = '<?=($PathRelative."login.php?Msg=".$Msg."&CurPage=".$PathRelative."src/module/email/cache_index.php")?>';
		</script>
<?
//	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}

AuthPage(array("Communication-Mail-GeneralUsage"));

include_once($PathRelative."src/include/template/iframe_header.php");
include_once($PathRelative."src/include/class/mailtable_cache.php");
/*
//include_once($PathRelative."src/include/class/imap.php");
include_once($PathRelative."src/include/class/imap_cache_agent.php");*/
include_once($PathRelative."src/include/class/imap_cache_ui.php");

intranet_opendb(true);

// create UI instance
$lui = new imap_cache_ui();

// get return message
$Msg = $_GET['Msg'];

// IMAP setup
//$IMap = new imap();
//$IMapCache = new imap_cache_agent();


//Get Folder Select
$Folder = (trim($_GET['Folder'])!="")? stripslashes(urldecode($_GET['Folder'])):$SYS_CONFIG['Mail']['SystemMailBox']['inbox'];
$AlwaysSyn = (trim($_GET['AlwaysSyn'])!="")? $_GET['AlwaysSyn']:0;
$SwitchPage = (trim($_GET['SwitchPage'])!="")? $_GET['SwitchPage']:0;

// get Folder Structure for select box
/*$FolderArray = $IMapCache->Get_Folder_Structure();
$DefaultFolderList = $SYS_CONFIG['Mail']['SystemMailBox'];*/
// mail table setup
$keyword = $_GET['keyword'];
$order = (trim($_GET['order']) != "")? $_GET['order']:1;
$field = (trim($_GET['field']) != "")? $_GET['field']:3;
$pageNo = (trim($_GET['pageNo']) != "")? $_GET['pageNo']:1;
//$numPerPage = $SYS_CONFIG['Interface']['DefaultPageSize'];
$numPerPage = (trim($_GET['numPerPage']) != "")? $_GET['numPerPage']:0;

$li = new mailtable($field, $order, $pageNo, $isSearch, $numPerPage, $Folder, 'form1', $IMapCache);//$numPerPage, $MailFolder);

# Listing Control - function bar
$buttons = array();
if ($Folder == $IMapCache->DefaultFolderList['TrashFolder'] || $Folder == $IMapCache->DefaultFolderList['SpamFolder']) {
	$Warning = ($Folder == $IMapCache->DefaultFolderList['TrashFolder'])? $Lang['Warning']['ClearTrashWarn']:$Lang['Warning']['ClearSpamWarn'];
	$buttons[] = array($lui->Get_Input_Button("ClearButton","ClearButton",$Lang['email']['Clear'],'class="button" onclick="Parent_To_Top(); Confirm_Submit_Page(\''.$Warning.'\',\'form1\',\'cache_clear_folder.php\');"'));
}
if ($Folder == $IMapCache->DefaultFolderList['TrashFolder'])
	$buttons[] = array($lui->Get_Input_Button("DeleteButton","DeleteButton",$Lang['email']['Delete'],'disabled="disabled" class="button" onclick="Parent_To_Top(); Confirm_Submit_Page(\''.$Lang['Warning']['MailDeleteWarn'].'\',\'form1\',\'cache_delete_email.php\');"'),"DeleteButton",true);
else 
	$buttons[] = array($lui->Get_Input_Button("DeleteButton","DeleteButton",$Lang['email']['Delete'],'disabled="disabled" class="button" onclick="Parent_To_Top(); Submit_Page(\'form1\',\'cache_delete_email.php\');"'),"DeleteButton",true);
$buttons[] = array($lui->Get_Input_Select_With_Label("FolderSelect","FolderSelect",$FolderArray,"",'disabled="disabled" onchange="Parent_To_Top();  if (this.value != \'\' && this.value != \'Default\' && this.value != \'Personal\') { document.form1.DestinationFolder.value = encodeURIComponent(this.value); if (!Confirm_Submit_Page(\''.$Lang['Warning']['MailMoveWarn'].'\',\'form1\',\'cache_move_email.php\')) { this.options[0].selected = true;} }"'),"FolderSelect",true);

for($i=0; $i<count($buttons);$i++){
    $li->appendFunctionControl($buttons[$i][0],$buttons[$i][1],$buttons[$i][2]);
}
echo $li->Get_All_JS_Script();
echo $lui->Show_Parent_ReturnMessage($Msg);

echo $lui->Get_Div_Open("imail_iframe_right");
echo $lui->Get_Div_Open('IMapStatus','class="imail_entry_read_link" style="width:100%; "');
echo $lui->Get_Div_Close();
echo $lui->Get_Div_Close();
include_once($PathRelative."src/include/template/iframe_footer.php");
//echo 'Over All used Time: '. (time() - $OverAllTime);
?>
<script>
function GetXmlHttpObject()
{
  var xmlHttp=null;
  try
  {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
  }
  catch (e)
  {
    // Internet Explorer
    try
    {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e)
    {
      xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  }
  
  return xmlHttp;
} 		
	
function Syn_Mail_Ajax(Mode) {
	if (!Check_Box_Checked('UID[]')) {
		wordXmlHttp = GetXmlHttpObject();
	   
	  if (wordXmlHttp == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'get_mail_process.php';
		var postContent = "MailAddress=<?=$_SESSION['SSV_LOGIN_EMAIL']?>";
		postContent += "&MailPassword=<?=$_SESSION['SSV_EMAIL_PASSWORD']?>";
		postContent += "&Site=<?=$SYS_CONFIG['SchoolCode']?>";
		postContent += "&Folder=<?=urlencode($Folder)?>";
		postContent += "&UserType=<?=$_SESSION['SSV_USER_TYPE']?>";
		if (Mode == 0) 
	  	wordXmlHttp.onreadystatechange = Syn_Mail_State_Changed;
	  else
	  	wordXmlHttp.onreadystatechange = Syn_Mail_State_Changed_Silent;
	  wordXmlHttp.open("POST", url, true);
		wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		wordXmlHttp.send(postContent);
	}
	else {
		setTimeout("Syn_Mail_Ajax(1);",1000*60*<?=$SYS_CONFIG['Mail']['AutoSynTimer']?>);
		//setTimeout("Syn_Mail_Ajax(1);",1000*5);
	}
}	

function Syn_Mail_State_Changed() {
	if (wordXmlHttp.readyState==1) {
		//document.getElementById('IMapStatus').innerHTML = '<img src="/theme/image_en/ajax-loader.gif"  width="16"  height="16"  />';
		document.getElementById('IMapStatus').innerHTML = ' Getting mail, please wait...';
	}
	else if (wordXmlHttp.readyState==2) {
		//document.getElementById('IMapStatus').innerHTML = '<img src="/theme/image_en/ajax-loader.gif"  width="16"  height="16"  />';
		document.getElementById('IMapStatus').innerHTML = ' Sending Request...';
	}
	else if (wordXmlHttp.readyState==3 && navigator.appName != "Microsoft Internet Explorer") {
		var responseValue = wordXmlHttp.responseText;
		var ToOutput = responseValue.substring(responseValue.lastIndexOf('|')+1,responseValue.length);
		
		//document.getElementById('IMapStatus').innerHTML = '<img src="/theme/image_en/ajax-loader.gif"  width="16"  height="16"  />';
		document.getElementById('IMapStatus').innerHTML = ' ' + ToOutput;
	}
	else if (wordXmlHttp.readyState==4) {
		var responseValue = wordXmlHttp.responseText;
		var ToOutput = responseValue.substring(responseValue.lastIndexOf('|')+1,responseValue.length);
		var RightPanelUrl = window.parent.document.getElementById('RightPanel').contentWindow.location.href;
		
		//document.getElementById('IMapStatus').innerHTML = '<img src="/theme/image_en/ajax-loader.gif"  width="16"  height="16"  />';
		document.getElementById('IMapStatus').innerHTML = ' ' + ToOutput;
  	
  	Right_Panel_Ajax();
	}
}
	
function Syn_Mail_State_Changed_Silent() {
	if (wordXmlHttp.readyState==4) {
  		Right_Panel_Ajax(); 
  }
}

var order = "<?=$order?>";
var field = "<?=$field?>";
var pageNo = "<?=$pageNo?>";
var numPerPage = "<?=$numPerPage?>";
var SwitchPage = "<?=$SwitchPage?>";

function Right_Panel_Ajax() {
	wordXmlHttp = GetXmlHttpObject();
    
  if (wordXmlHttp == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_cache_inbox.php';
	var postContent = "order="+order;
	postContent += "&field="+field;
	postContent += "&Folder=<?=urlencode($Folder)?>";
	postContent += "&pageNo="+pageNo;
	postContent += "&numPerPage="+numPerPage;
	postContent += "&AlwaysSyn=<?=$AlwaysSyn?>";
	postContent += "&SwitchPage="+SwitchPage;
  wordXmlHttp.onreadystatechange = Right_Panel_Ajax_Change;
  wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

function Right_Panel_Ajax_Change() {
	var ResponseText = "";
	
	if (wordXmlHttp.readyState==1) {
		//document.getElementById('IMapStatus').innerHTML = '<img src="/theme/image_en/ajax-loader.gif"  width="16"  height="16"  />';
		//document.getElementById('IMapStatus').innerHTML = ' Loading email list...';
	}
	else if (wordXmlHttp.readyState==2 && navigator.appName != "Microsoft Internet Explorer") {
		ResponseText = wordXmlHttp.responseText.Trim();
  	if (ResponseText.substr(0,4) == "0|=|") {
  		window.top.location = '<?=($PathRelative."login.php?CurPage=".$PathRelative."src/module/email/cache_index.php")?>&Msg=' + ResponseText.substring(4);
  	}
  	else {
	  	document.getElementById("imail_iframe_right").innerHTML = wordXmlHttp.responseText;
	  }
	}
	else if (wordXmlHttp.readyState==3 && navigator.appName != "Microsoft Internet Explorer") {
		ResponseText = wordXmlHttp.responseText.Trim();
  	if (ResponseText.substr(0,4) == "0|=|") {
  		window.top.location = '<?=($PathRelative."login.php?CurPage=".$PathRelative."src/module/email/cache_index.php")?>&Msg=' + ResponseText.substring(4);
  	}
  	else {
	  	document.getElementById("imail_iframe_right").innerHTML = wordXmlHttp.responseText;
	  }
	}
	else if (wordXmlHttp.readyState==4)
  { 	
  	ResponseText = wordXmlHttp.responseText.Trim();
  	if (ResponseText.substr(0,4) == "0|=|") {
  		window.top.location = '<?=($PathRelative."login.php?CurPage=".$PathRelative."src/module/email/cache_index.php")?>&Msg=' + ResponseText.substring(4);
  	}
  	else {
	  	document.getElementById("imail_iframe_right").innerHTML = wordXmlHttp.responseText;
		
<? 
			if ($_REQUEST['Refresh'] == 1 || ($AlwaysSyn && !$SwitchPage)) {
?>
				parent.document.getElementById("LeftMenu").contentWindow.Left_Menu_Ajax('<?=str_replace("'", "\\'", $Folder)?>');
<?
			}
?>
	  	Resize_Iframe("RightPanel");
	  	//setTimeout("Syn_Mail_Ajax(1);",1000*60*<?=$SYS_CONFIG['Mail']['AutoSynTimer']?>);
	  	//setTimeout("Syn_Mail_Ajax(1);",1000*5);
	  }
  }
	
}

<?
// tempory condition for production user request on 27/10/2008
/*if ($SYS_CONFIG['Mail']['FolderPrefix'] == '') {
	$SentFolder = $SYS_CONFIG['Mail']['SystemMailBox']['SentFolder'];
	$DraftFolder = $SYS_CONFIG['Mail']['SystemMailBox']['DraftFolder'];
	$SpamFolder = $SYS_CONFIG['Mail']['SystemMailBox']['SpamFolder'];
}
else {
	$SentFolder = $SYS_CONFIG['Mail']['FolderPrefix'].$SYS_CONFIG['Mail']['FolderDelimiter'].$SYS_CONFIG['Mail']['SystemMailBox']['SentFolder'];
	$DraftFolder = $SYS_CONFIG['Mail']['FolderPrefix'].$SYS_CONFIG['Mail']['FolderDelimiter'].$SYS_CONFIG['Mail']['SystemMailBox']['DraftFolder'];
	$SpamFolder = $SYS_CONFIG['Mail']['FolderPrefix'].$SYS_CONFIG['Mail']['FolderDelimiter'].$SYS_CONFIG['Mail']['SystemMailBox']['SpamFolder'];
}

if ($Folder == $SentFolder || 
		$Folder == $DraftFolder || 
		$Folder == $SpamFolder) {*/
	if (trim($_GET['AlwaysSyn']) == "") {
	?>
		Syn_Mail_Ajax(0);
	<?
	}
	else {
		if ($AlwaysSyn && !$SwitchPage || $_REQUEST['HardRefresh'] == 1) {
	?>
			Syn_Mail_Ajax(0);
	<?
		}
		else {
	?>
			Right_Panel_Ajax(); 
	<?
		}
	}
/*}
else {
?>
	Right_Panel_Ajax(); 
<?
}*/
?>
function findPosition( oElement ) {
  if( typeof( oElement.offsetParent ) != 'undefined' ) {
    for( var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent ) {
      posX += oElement.offsetLeft;
      posY += oElement.offsetTop;
    }
    return [ posX, posY ];
  } else {
    return [ oElement.x, oElement.y ];
  }
}

function Email_Preview(Obj,SeenFlag){
	var divPreview = document.getElementById('divPreview');
	// alert(Obj.href);
	
	var ObjPosition = findPosition(Obj);
	
	wordXmlHttp = GetXmlHttpObject();
    
	if (wordXmlHttp == null)
	{
		alert (errAjax);
		return;
	} 
	
	var ObjHref = Obj.href.split('?');
	var url = '<?=$PathRelative?>src/module/email/ajax_cache_preview_email.php';
	var postContent = ObjHref[1];
		postContent += '&SeenFlag='+SeenFlag;
	divPreview.innerHTML = '<img src="/theme/image_en/ajax_loading_indicator.gif"  width="16"  height="16" style="position: relative; top: 50%; left:100px" />';
	divPreview.style.left = ObjPosition[0];
	divPreview.style.top = (ObjPosition[1] + 15);
	divPreview.style.display = '';
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.onreadystatechange = function() {
		var ResponseText = "";
		if (ResponseText.substr(0,4) == "0|=|") {
			window.top.location = '<?=($PathRelative."login.php?CurPage=".$PathRelative."src/module/email/cache_index.php")?>&Msg=' + ResponseText.substring(4);
		}
		else {
			if (wordXmlHttp.readyState == 4) {
				divPreview.innerHTML = wordXmlHttp.responseText;
				// setTimeout("Close_Email_Preview()",5000);
			}
		}
	};
	wordXmlHttp.send(postContent);
	
	
	

}

function Close_Email_Preview() {
	var divPreview = document.getElementById('divPreview');
		divPreview.style.display = 'none';
}

Resize_Iframe("RightPanel");

</script>
<?
Write_Message_Log("Log",'Page Total: '.(microtime(true) - $OverAllTime));
?>