<?php
// Editing by 
/*
 * 2015-04-20 (Carlos): Escape double quotes with slash in display names.
 * 2014-06-19 (Carlos): Created to merge both external recipient groups and individual recipients
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $Lang['Gamma']['ExternalContacts'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$li = new libdb();

$contact_types_ary = array();
$contact_types_ary[] = array('group',$Lang['Gamma']['ExternalRecipientGroup']);
$contact_types_ary[] = array('person',$Lang['Gamma']['ExternalRecipient']);
$contact_type_selection = $linterface->GET_SELECTION_BOX($contact_types_ary, 'id="ContactType" name="ContactType" onchange="document.getElementById(\'NewMailForm\').submit();"', $Lang['General']['EmptySymbol'].$Lang['General']['PleaseSelect'].$Lang['General']['EmptySymbol'], $ContactType);

if($ContactType == 'group')
{
	# List all alias
	$MethodTypeArr = array();
	$MethodTypeArr[] = array(1,$i_admintitle_us_user);
	$MethodTypeArr[] = array(2,$i_admintitle_us_group);
	$x2 = getSelectByArray($MethodTypeArr," name='MethodType' onchange='this.form.submit()' ", $MethodType);
	
	$sql = "SELECT AliasID, AliasName, NumberOfEntry FROM INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL WHERE OwnerID = '$UserID' ORDER BY AliasName";
	$alias_array2 = $li->returnArray($sql,2);
	if (count($alias_array2) > 0)
	{
		$HasAlias = true; 
	}
	
	$x1  = "<select name='AliasID[]' size='6' multiple >\n";
	for ($i=0; $i<count($alias_array2); $i++)
	{
		list ($id, $name, $id_num) = $alias_array2[$i];
		if (is_array($AliasID) && count($AliasID)>0)
		{
			if (in_array($id,$AliasID))
			{
				$sel_str = " selected='selected' ";	
			} else {
				$sel_str = " ";	
			}
		}	else {
			$sel_str = " ";	
		}
		$x1 .= "<option value='$id' $sel_str>$name</option>\n";
	}
	$x1 .= "</select>\n";
	
	if (is_array($AliasID) && count($AliasID)>0)
	{
		$AliasImplode = implode(",",$AliasID);
		
	  	$addressSql = "
	    					SELECT 
	    						B.AddressID, CONCAT(B.TargetName,' &lt;',B.TargetAddress,'&gt;') 
	    					FROM 
	    						INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY AS a,
	    						INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL AS B
	    					WHERE 
	    						a.AliasID IN ($AliasImplode) AND a.TargetID = B.AddressID
	    					ORDER BY 
								B.TargetName ";
		if (($curAction == "add") && ($addAction == "alias"))
		{
			$TotalArr = array();
		} 
		else 
		{        					
			$TotalArr = $li->returnArray($addressSql,2);    	        					
		}
		
		$x3_text = $i_admintitle_us_user;
	    $x3 = getSelectByArray($TotalArr," name='AliasEmailID[]' multiple ", "",0,1);
		$x3_button = $linterface->GET_BTN($button_add, "button", "jADD_USER_EMAIL()","add1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ");
		$x3_button2 = $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['AliasEmailID[]'])","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ");
	}
	
	
	if ($curAction == "add")
	{
		if ($addAction == "alias")
		{
			if (is_array($AliasID) && count($AliasID)>0)
			{		    
			    $AliasImplode = implode(",",$AliasID);
			    
			    $addressSql = "
			    					SELECT 
			    						B.TargetName,B.TargetAddress FROM INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY AS a,
			    						INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL AS B
			    					WHERE 
			    						a.AliasID IN ($AliasImplode) AND a.TargetID = B.AddressID
			    					ORDER BY 
										B.TargetName ";
			     $targets = $li->returnArray($addressSql,2);
	     
			     for ($i=0; $i<sizeof($targets); $i++)
			    {
			         list($name, $address) = $targets[$i];
			         if ($name=="" || $name==$address)
			         {
			             $target_string = $address;
			         }
			         else
			         {
			             $target_string = "\"".str_replace(array('\"','&quot;'),'\"',$name)."\" <$address>";
			         }
			
			         $text_recipients .= "$target_string; ";
			    }
			    if ($text_recipients != "")
			    {
			    ?>
			        <script language="javascript" >
			        obj = opener.window.document.NewMailForm.<?=$fieldname?>;
			        s1 = Trim(obj.value);
			        strlength = s1.length;
			        if (strlength != 0 && s1.charAt(strlength-1)!=";")
			        {
			            obj.value = obj.value + "; ";
			        }
			        obj.value += "<?=addslashes(intranet_undo_htmlspecialchars($text_recipients))?>";
			        </script>
			    <?
			    } 
			}		
		}	     
		else if ($addAction == "user_email")
		{
			if (is_array($AliasEmailID) && count($AliasEmailID)>0)
			{		    
			    $AliasImplode = implode(",",$AliasEmailID);
			    
			    $addressSql = "
			    					SELECT 
			    						TargetName,TargetAddress 
			    					FROM 		    						
			    						INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
			    					WHERE 
			    						AddressID IN ($AliasImplode) 
			    					ORDER BY 
										TargetName ";
			     $targets = $li->returnArray($addressSql,2);
	     
			     for ($i=0; $i<sizeof($targets); $i++)
			    {
			         list($name, $address) = $targets[$i];
			         if ($name=="" || $name==$address)
			         {
			             $target_string = $address;
			         }
			         else
			         {
			             $target_string = "\"".str_replace(array('\"','&quot;'),'\\"',$name)."\" <$address>";
			         }
			
			         $text_recipients .= "$target_string; ";
			    }
			    
			    if ($text_recipients != "")
			    {
			    ?>
			        <script language="javascript" >
			        obj = opener.window.document.NewMailForm.<?=$fieldname?>;
			        s1 = Trim(obj.value);
			        strlength = s1.length;
			        if (strlength != 0 && s1.charAt(strlength-1)!=";")
			        {
			            obj.value = obj.value + "; ";
			        }
			        obj.value += "<?=addslashes(intranet_undo_htmlspecialchars($text_recipients))?>";
			        </script>
			    <?
			    } 
			}		
		}			
	}
} // $ContactType == 'group'

if($ContactType == 'person')
{
	$keyword = trim($keyword);
	
	$addressSql = "SELECT 
						AddressID, CONCAT(TargetName,' <',TargetAddress,'>') 
					FROM 
						INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
					WHERE 
						OwnerID = '$UserID' AND
						(TargetName like '%".$li->Get_Safe_Sql_Like_Query(htmlspecialchars(stripslashes($keyword),ENT_QUOTES))."%' OR
                		TargetAddress like '%".$li->Get_Safe_Sql_Like_Query(htmlspecialchars(stripslashes($keyword),ENT_QUOTES))."%')
                	ORDER BY
                		TargetAddress ASC";
	$TotalArr = $li->returnArray($addressSql,2);    	        					
	
	$x3_text = $i_admintitle_us_user;
	$x3 = getSelectByArray($TotalArr," name='AliasEmailID[]' multiple size='20' ", "",0,1);
	$x3_button = $linterface->GET_BTN($button_add, "button", "jADD_USER_EMAIL()","add1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ");
	$x3_button2 = $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['AliasEmailID[]'])","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ");
	
	if ($curAction == "add")
	{
		if ($addAction == "user_email")
		{
			if (is_array($AliasEmailID) && count($AliasEmailID)>0)
			{		    
			    $AliasImplode = implode(",",$AliasEmailID);
			    
			    $addressSql = "
			    					SELECT 
			    						TargetName,TargetAddress 
			    					FROM 		    						
			    						INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
			    					WHERE 
			    						AddressID IN ($AliasImplode) 
			    					";
			     $targets = $li->returnArray($addressSql,2);
	     
			     for ($i=0; $i<sizeof($targets); $i++)
			    {
			         list($name, $address) = $targets[$i];
			         if ($name=="" || $name==$address)
			         {
			             $target_string = $address;
			         }
			         else
			         {
			             $target_string = "\"".str_replace(array('\"','&quot;'),'\\"',$name)."\" <$address>";
			         }
			
			         $text_recipients .= "$target_string; ";
			    }
			    if ($text_recipients != "")
			    {
			    ?>
			        <script language="javascript" >
			        obj = opener.window.document.NewMailForm.<?=$fieldname?>;
			        s1 = Trim(obj.value);
			        strlength = s1.length;
			        if (strlength != 0 && s1.charAt(strlength-1)!=";")
			        {
			            obj.value = obj.value + "; ";
			        }
			        
			        var SelectedEmailArray = s1.split(";");
			        var temp_str = ("<?=addslashes(intranet_undo_htmlspecialchars($text_recipients))?>");
			        temp_str = Trim(temp_str);
			        var NewEmailArray = temp_str.split(";");
			        var new_str = "";
			        for(a=0; a<NewEmailArray.length; a++)
					{
						flag = false;
						if(SelectedEmailArray.length > 0)
						{
							for(b=0; b<SelectedEmailArray.length; b++)
							{
								if(NewEmailArray[a] == SelectedEmailArray[b])
								{
									flag = true;
									break;
								}
							}
						}
						if(flag == false)
						{
							if(NewEmailArray[a].replace(/(^\s*)|(\s*$)/g, "").length > 0)
							{
								new_str+=NewEmailArray[a]+";";
							}
						}
					}
					obj.value += new_str;
			        
			        </script>
			    <?
			    } 
			}		
		}			
	}
} // $ContactType == 'person'

?>
<script type="text/javascript" language="JavaScript">
function addAddress(obj)
{
         obj.selected.value = 1;
         checkRestore(obj,'AddressID[]','index.php');
         if (countChecked(obj,'AddressID[]')==0)
         {
             obj.selected.value = 0;
         }
}

function SelectAll(obj)
{
     for (i=0; i<obj.length; i++)
     {
          obj.options[i].selected = true;
     }
}

 function jADD_USER()
 {	     
     document.NewMailForm.curAction.value = "add";
     document.NewMailForm.addAction.value = "user";	     
     document.NewMailForm.submit();	     
 }
 
 function jADD_GROUP()
 {
     document.NewMailForm.curAction.value = "add";
     document.NewMailForm.addAction.value = "group";	     
     document.NewMailForm.submit();	     
 }

 function jADD_USER_EMAIL()
 {	     
     document.NewMailForm.curAction.value = "add";
     document.NewMailForm.addAction.value = "user_email";	     
     document.NewMailForm.submit();	     
 }
      
 function jEXPAND_ALIAS()
 {
     document.NewMailForm.curAction.value = "expand";
     document.NewMailForm.expandAction.value = "alias";	     
     document.NewMailForm.submit();
 }

 function jADD_ALIAS()
 {
     document.NewMailForm.curAction.value = "add";
     document.NewMailForm.addAction.value = "alias";	     
     document.NewMailForm.submit();
 }
</script>
<br />
<form id="NewMailForm" name="NewMailForm" action="external_contacts.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="left">	
			<tr>
				<td>
					<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tabletext" >
						<tr>
							<td valign="top" nowrap="nowrap" width="30%" >
								<span class="tabletext"><?=$Lang['Gamma']['RecipientType']?>:</span>
							</td>
							<td>					
								<?=$contact_type_selection?>			
							</td>
						</tr>	
					</table>
				</td>
			</tr>		
		</table>   	      
	</td>
</tr>
<?php if($ContactType == 'group'){ ?>
<tr>
	<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="left">	
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tabletext" >
					<tr>
						<td valign="top" nowrap="nowrap" width="30%" >
							<span class="tabletext"><?=$i_CampusMail_New_AliasGroup?>:</span>
						</td>
						<td>					
							<table cellpadding="0" cellspacing="0" >
								<tr>
									<td><?=$x1?></td>
									<td style="vertical-align:bottom">
										<table cellpadding="0" cellspacing="6" >				
											<tr>
												<td>					
												<?= $linterface->GET_BTN($button_add, "button", "jADD_ALIAS()","add1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
												</td>
											</tr>	
											<tr>
												<td>					
												<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "button", "jEXPAND_ALIAS()","expand1"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."" ?>
												</td>
											</tr>					
											<tr>
												<td>
												<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['AliasID[]'])","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ") ?>
												</td>
											</tr>									
										</table>				
									</td>
								</tr>
							</table>			
						</td>
					</tr>	
					<?php 
					if (is_array($AliasID) && count($AliasID)>0 && !(($curAction == "add") && ($addAction == "alias")))
					{ 
					?>		
					<tr> 
						<td height="1" colspan="2" class="dotline" valign="middle" >
							<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
						</td>
					</tr>
					<tr> 
						<td height="5" colspan="2"  >
							<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />			
						</td>
					</tr>
					<tr> 			
						<td valign="top" nowrap="nowrap" width="30%" >
							<span class="tabletext"><?=$x3_text?>:</span>
						</td>
						<td >					
							<table cellpadding="0" cellspacing="0" >
								<tr>
									<td><?=$x3?></td>
									<td style="vertical-align:bottom">
										<table cellpadding="0" cellspacing="6" >				
											<tr>
												<td>
												<?=$x3_button?>
												</td>
											</tr>	
											<tr>
												<td>
												<?=$x3_button2?>
												</td>
											</tr>					
										</table>
									</td>
								</tr>
							</table>			
						</td>
					</tr>				
					<?php 
					} 
					?>
				</table>
			</td>
		</tr>		
		</table>
		<input type="hidden" name="curAction" value="">
		<input type="hidden" name="expandAction" value="<?=$expandAction?>">
		<input type="hidden" name="addAction" value=""> 	      
	</td>
</tr>
<?php } // $ContactType==group ?>
<?php if($ContactType == 'person'){ ?>
<tr>
	<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="left">	
			<tr>
				<td>
					<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tabletext" >				
						<tr> 			
							<td valign="top" nowrap="nowrap" width="30%" >
								<span class="tabletext"><?=$button_search?> : </span>
							</td>
							<td>
								<input type='text' name='keyword' class='formtextbox' value="<?=htmlspecialchars(stripslashes($keyword),ENT_QUOTES)?>"> <?= $linterface->GET_ACTION_BTN($button_search, "submit") ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="left">	
			<tr>
				<td>
					<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tabletext" >				
						<tr> 			
							<td valign="top" nowrap="nowrap" width="30%" >
								<span class="tabletext"><?=$x3_text?>:</span>
							</td>
							<td >					
								<table cellpadding="0" cellspacing="0" >
									<tr>
										<td><?=$x3?></td>
										<td style="vertical-align:bottom">
										
											<table cellpadding="0" cellspacing="6" >				
												<tr>
													<td>
													<?=$x3_button?>
													</td>
												</tr>	
												<tr>
													<td>
													<?=$x3_button2?>
													</td>
												</tr>					
											</table>
											
										</td>
									</tr>
								</table>			
							</td>
						</tr>				
					</table>
				</td>
			</tr>		
		</table>
		<input type="hidden" name="selected" value="0">
		<input type="hidden" name="curAction" value="">
		<input type="hidden" name="expandAction" value="<?=$expandAction?>">
		<input type="hidden" name="addAction" value=""> 	      
	</td>
</tr>
<?php } ?>
<tr>
	<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
			</tr>
			<tr>
				<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>		
<br />
<input type="hidden" name="fieldname" value="<?=$fieldname?>" />
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>