<?php
// using by 
/*
 * 2014-06-16 (Carlos): add task SetCachedSearchResult
 * 2013-02-14 (Carlos): modify task GetFolderAllUID filter with MailStatus
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$skip_connection = false;
if($_REQUEST['skip_login']){
	$skip_connection = true;
}
$IMap = new imap_gamma($skip_connection);

if($IMap->ConnectionStatus != true)
{ 
	intranet_closedb();
	exit;
}

switch ($task)
{
	case "UpdatePref":
		$Result = $IMap->Update_Preference(array("GmailMode"=>$GmailMode))?"Success":"Failed";
		echo $Lang['Gamma']['ReturnMsg']['UserPref']['Save'][$Result];
	break;	
	
	case "LoadGmailModeEmailTopBar":
		$EmailTopBarRows = $IMap->Get_Gmail_Mode_Mail_Top_Bar($Folder, $MessageID, $PartNumber,1);
		echo $EmailTopBarRows;
	break;
	
	case "LoadGmailModeEmailContent":
		$EmailContentRow = $IMap->Get_Mail_Content_UI($Folder, $Uid);
		echo $EmailContentRow;
	break;
	
	case "LoadAllGmailModeEmailContent":
		$EmailContentRows = $IMap->Get_Gmail_Mode_Mails_Content($FolderArray, $UidArray);
		echo $EmailContentRows;
	break;
	
	case "SetMailFlag":
		if($setflag==1)
			$flagged = $IMap->Set_Flagged($Folder,$Uid)?1:0;
		else
			$flagged = $IMap->Clear_Flagged($Folder,$Uid)?0:1;
		
		echo $flagged;
	break;
	case "DeleteCachedAttachment":
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$lfile = new libfilesystem();
		$result = $lfile->folder_remove_recursive($path);
	break;
	
	case "ProceedBatchRemoval":
		$Result['ExpungeMails'] = $IMap->Batch_Removal_Expunge_Mails();
		$Result['RestoreMails'] = $IMap->Batch_Removal_Restore_Mails();
		$Result['CollectMails'] = $IMap->Collect_Batch_Removal_Mails();
		echo in_array(true,$Result)?"1":"0"; // either one is successful, return 1
	break;
	
	// Get all UID from a folder and return in csv form
	case "GetFolderAllUID":
		$return = '';
		/*
		if($IMap->Go_To_Folder($Folder)){
			$maillist = $IMap->getMailList();
			if(count($maillist)>0){
				foreach($maillist as $entry){
					$return .= $entry->uid . ',';
				}
				$return = substr($return,0,strlen($return)-1);
			}
		}
		*/
		/*
		$imap_socket = new imap_socket();
		if($imap_socket->Connect()){
			if($Folder == ''){
				$Folder = "INBOX";
			}
			if($imap_socket->Go_To_Folder($Folder)){
				 $uids = $imap_socket->Get_UID_ARRAY("1:*");
				 if($uids){
				 	$return = implode(",",$uids);
				 }
			}
			$imap_socket->Close_Connect();
		}
		echo $return;
		*/
		if($IMap->Go_To_Folder($Folder)){
			$uid_array = $IMap->Get_Sort_Index($IMap, $imapInfo=array('displayMode'=>'folder'), $search, $MailStatus, 1);
			if(sizeof($uid_array)>0) {
				$return = implode(",",$uid_array);
			}
		}
		echo $return;
	break;
	
	// Get all UID and Folder pairs from cached search result, return in "UID1,Folder1/UID2,Folder2/..."
	case "GetAllSearchUIDFolder":
		$return = '';
		$UIDFolderArr = $IMap->Get_Cached_Search_Result();
		$UIDArr = $UIDFolderArr[0];
		$FolderArr = $UIDFolderArr[1];
		for($i=0;$i<count($UIDArr);$i++){
			$return .= $UIDArr[$i].",".$FolderArr[$i]."/";
		}
		$return = substr($return,0,strlen($return)-1);
		echo $return;
	break;
	
	case "SetCachedSearchResult": 
		## Get Data
		$isAdvanceSearch = isset($isAdvanceSearch)? $isAdvanceSearch : (isset($KeywordFrom)?1:'');
		$KeywordFrom 	= (isset($KeywordFrom) && $KeywordFrom != "") ? stripslashes(trim($KeywordFrom)) : "";
		$KeywordTo		= (isset($KeywordTo) && $KeywordTo != "") ? stripslashes(trim($KeywordTo)) : "";
		$KeywordCc 		= (isset($KeywordCc) && $KeywordCc != "") ? stripslashes(trim($KeywordCc)) : "";
		$KeywordBcc 	= (isset($KeywordBcc) && $KeywordBcc != "") ? stripslashes(trim($KeywordBcc)) : "";
		$KeywordSubject = (isset($KeywordSubject) && $KeywordSubject != "") ? stripslashes(trim($KeywordSubject)) : "";
		$KeywordBody 	= (isset($KeywordBody) && $KeywordBody != "") ? stripslashes(trim($KeywordBody)) : "";
		$KeywordAttachment = (isset($KeywordAttachment) && $KeywordAttachment != "") ? stripslashes(trim($KeywordAttachment)) : "";
		$FromDate 		= (isset($FromDate) && $FromDate != "") ? trim($FromDate) : "";
		$ToDate 		= (isset($ToDate) && $ToDate != "") ? trim($ToDate) : "";
		$Folder 		= (isset($Folder))? stripslashes($Folder) : $SYS_CONFIG['Mail']['FolderPrefix'];
		
		######################### Cache Search Fields ###################################
		$SearchFields = array();
		$SearchFields[]['isAdvanceSearch'] = $isAdvanceSearch; // added 2014-06-18
		$SearchFields[]['FromExtAddressBook'] = $FromExtAddressBook; // added 2014-06-18
		$SearchFields[]['FromIntAddressBook'] = $FromIntAddressBook; // added 2014-06-18
		$SearchFields[]['FromExtAddressGroup'] = $FromExtAddressGroup; // added 2014-06-18
		$SearchFields[]['FromIntAddressGroup'] = $FromIntAddressGroup; // added 2014-06-18
		$SearchFields[]['FromSearch'] = $FromSearch;
		$SearchFields[]['order'] = $order;
		$SearchFields[]['field'] = $field;
		$SearchFields[]['pageNo'] = $pageNo;
		$SearchFields[]['keyword'] = stripslashes(trim($keyword));
		$SearchFields[]['KeywordFrom'] = $KeywordFrom;
		$SearchFields[]['KeywordTo'] = $KeywordTo;
		$SearchFields[]['KeywordCc'] = $KeywordCc;
		$SearchFields[]['KeywordBcc'] = $KeywordBcc;
		$SearchFields[]['KeywordSubject'] = $KeywordSubject;
		$SearchFields[]['KeywordBody'] = $KeywordBody;
		$SearchFields[]['KeywordAttachment'] = $KeywordAttachment;
		$SearchFields[]['FromDate'] = $FromDate;
		$SearchFields[]['ToDate'] = $ToDate;
		$SearchFields[]['Folder'] = $Folder;
		for ($i=0; $i <sizeof($SearchFolder) ; $i++) {
			$SearchFields[]['SearchFolder[]'] = stripslashes($SearchFolder[$i]);
		}
		##################################################################################
		
		$UidAry = $_REQUEST['Uid']; // array
		$UidAry_count = count($UidAry);
		$CachedUID = array();
		$CachedFolder = array();
		for($i=0;$i<$UidAry_count;$i++) {
			$parts = explode(",",$UidAry[$i]);
			$CachedUID[] = $parts[0];
			$CachedFolder[] = $parts[1];
		}
		$IMap->Set_Cached_Search_Result($CachedUID,$CachedFolder,$SearchFields);
	break;
	
	case "SortEmails":
		$order = $_REQUEST['order'];
		
		function SortCompare($a,$b)
		{
			global $order;
			if($a[0] == $b[0]) return 0;
			
			return $order == 1? ($a[0] > $b[0] ? 1 : -1) : ($a[0] > $b[0] ? -1:1);
		}
		
		$rows = $_REQUEST['rows']; // array of table rows <tr>...</tr>
		$cols = $_REQUEST['cols']; // array of the order field text content
		$row_count = count($rows);
		$sort_ary = array();
		
		for($i=0;$i<$row_count;$i++){
			$sort_ary[] = array($cols[$i],rawurldecode($rows[$i]));
		}
		
		usort($sort_ary,"SortCompare");
		$x = '';
		for($i=0;$i<$row_count;$i++){
			$x .= $sort_ary[$i][1];
		}
		echo $x;
	break;
}

intranet_closedb();
die;
?>