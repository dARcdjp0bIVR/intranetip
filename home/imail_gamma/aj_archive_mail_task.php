<?php
// editing by 
/*
 * 2015-06-08 (Carlos): Do not use imap_cahce_agent.php Get_Check_Mail_Count_In_Folder() to count number of emails 
 * 						as it use a while loop to parse the result that may cause server unresponsive at the moment.
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
//include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$Action = trim($_REQUEST['Action']);

switch($Action)
{
	case "GetMailCount":
		//$imap_cache = new imap_cache_agent();
		$Folder = urldecode($_REQUEST['Folder']);
		//$mail_count = $imap_cache->Get_Check_Mail_Count_In_Folder($Folder);
		
		$mail_count = 0;
		$IMap = new imap_gamma();
		$change_folder_success = $IMap->Go_To_Folder($Folder);
		if($change_folder_success)
			$mail_count = imap_num_msg($IMap->inbox);
		
		$_SESSION['intranet_iMail_archived'] = 1;
		echo $mail_count;
	break;
	case "GetArchiveMailMessageLayer":
		$lwebmail = new libwebmail();
		$id = 'sub_layer_imail_archive_mail_v30';
		$archive_number = isset($sys_custom['iMail']['ArchiveMailNumber'])?$sys_custom['iMail']['ArchiveMailNumber']:3000;
		$message = str_replace("<!--NUMBER-->",$archive_number,$Lang['iMail']['FieldTitle']['ArchiveInboxMsg']);
		echo $lwebmail->Get_Archive_Mail_Message_Layer($id,$message,'500px','100px');
	break;
	case "SetRemindLater":
		$_SESSION['intranet_iMail_archived'] = 1;
		echo $_SESSION['intranet_iMail_archived'];
	break;
}

intranet_closedb();
?>