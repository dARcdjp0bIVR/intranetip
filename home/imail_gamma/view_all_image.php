<?php
// using 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/$LAYOUT_SKIN/layout/print_header.php");

intranet_opendb();

$IMap = new imap_gamma();

$lfile = new libfilesystem();
$MailContent = $IMap->getMailRecord($Folder, $uid);

$Attachment = $MailContent[0]["attach_parts"];
if($IMap->IMapCache===false) $IMapCache = new imap_cache_agent();
else $IMapCache = $IMap->IMapCache;
if (!(is_null($Attachment) || $Attachment == "" || empty($Attachment))) {
	
	// short list	
	for ($i=0; $i< sizeof($Attachment); $i++) 
	{	
		if (!$Attachment[$i]['IsTNEF'] && $Attachment[$i]['AttachType'] != "File") 
			continue;
		
		$dir_path = $IMap->user_file_path."/attachments";
		$lfile->folder_new($dir_path);
		
		$dir_path = $dir_path."/".$uid;
		$lfile->folder_new($dir_path);
		if(!empty($Attachment[$i]['PartID'])){
			$dir_path .= "/".$Attachment[$i]['PartID'];
			$lfile->folder_new($dir_path);
		}
		$filepath = $dir_path."/".$Attachment[$i]["FileName"];
		if(!file_exists($filepath))
		{
			$thisAttachment= $IMapCache->Get_Attachment($Attachment[$i]['PartID'],$uid,$Folder);
			$lfile->file_write($thisAttachment["Content"],$filepath);
		}
		if(@exif_imagetype($filepath)) //if attachment is image
		{
			//$x .= "<div class='tableheader' style='padding:10px'>".$Attachment[$i]["FileName"]."</div>";
			$x .= "<div class='image_wrapper' style='padding:10px'>";
				$x .= $Attachment[$i]["FileName"]."<br>";
				$x .= "<img src='$filepath'>";
			$x .= "</div><hr>";
		}			
	}
	echo $x;
	
	
}
?>
<script>
$().ready(function(){
	$("div.image_wrapper>img").each(function(){
		if($(this).width()>$(window).width()||$(this).height()>$(window).height())
		{
			$(this).click(function(){toggleSize(this)})
			$(this).css("width","auto");
			$(this).css("height","auto");
			toggleSize(this)
		}
	});
	
	DeleteAttachment()
});

$(window).unload(function(){ajaxObj.abort()});

var ajaxObj; 
function DeleteAttachment()
{
	ajaxObj = $.post(
		"ajax_task.php",
		{
			task:"DeleteCachedAttachment",
			path:"<?=$IMap->user_file_path."/attachments"?>"
		}
	)
}

function toggleSize(imgObj)
{
	if($(imgObj).height()>$(window).height() && $(imgObj).width()>$(window).width())
	{
		if($(imgObj).css("height")=="auto")
		{
			var hratio = $(window).height()/$(imgObj).height();
			var wratio = $(window).width()/$(imgObj).width();
			var resize_ratio = (hratio>wratio?wratio:hratio);
			var new_width = ($(imgObj).width()*resize_ratio)+'px';
			var new_height = ($(imgObj).height()*resize_ratio)+'px';
			$(imgObj).css("height",new_height)
			$(imgObj).css("width",new_width)
			$(imgObj).css("cursor", "-moz-zoom-in")
		}
		else
		{
			$(imgObj).css("height","auto")
			$(imgObj).css("width","auto")
			$(imgObj).css("cursor", "-moz-zoom-out")
		}
		
	}
	else 
	if($(imgObj).height()>$(window).height())
	{
		$(imgObj).css("width","auto");
		if($(imgObj).css("height")=="auto")
		{
			$(imgObj).css("height",(parseInt($(window).height())-20)+'px')
			$(imgObj).css("cursor", "-moz-zoom-in")
		}
		else
		{
			$(imgObj).css("height","auto")
			$(imgObj).css("cursor", "-moz-zoom-out")
		}
		
	}
	else
	{
		$(imgObj).css("height","auto")		
		if($(imgObj).css("width")=="auto")
		{
			$(imgObj).css("width",(parseInt($(window).width())-20)+'px')
			$(imgObj).css("cursor", "-moz-zoom-in")
		}
		else
		{
			$(imgObj).css("width","auto")
			$(imgObj).css("cursor", "-moz-zoom-out")
		}
	}
}




</script>
<style>
.image_wrapper{	padding:10px;}
</style>
<?
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/$LAYOUT_SKIN/layout/print_footer.php");
die;
?>