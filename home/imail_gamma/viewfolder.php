<?php
## Using By : 
################ Change Log [Start] #####################
#	Date	:	2018-08-14 (Carlos)
#				$sys_custom['iMailPlus']['EDSReportSpam'] - added Report Spam button to move emails to EDS_ReportSpam folder for analysis.
#
#	Date	:	2015-05-20 (Carlos)
#				Improved to make the toolbar panel floats on top when window scrolls down and toolbar is not visible to user.
#
#	Date	:	2015-02-25 (Carlos)
#				Swap [Empty Trash/Junk] button with [Remove] button.
#				$sys_custom['iMailHideEmptyTrashButton'] can hide the [Empty Trash/Junk] button.
#
#	Date	:	2014-10-27 (Carlos)
#				Added hidden field Folder_b which is base64 encoded folder name. Useful for remove.php and report_spam.php
#
#	Date	:	2014-06-17 (Carlos)
#				Changed keyword search to search options
#
#	Date	:	2013-12-03 (Carlos)
#				Included jquery.alerts, [Move To] and [Marks As] changed onchange event to use jAlert to avoid iPad dialog hang problem
#
#	Date	:	2013-09-26 [Carlos]
#				modified js initHighlightRow(), enable highlighting row by clicking checkbox 
#
#	Date	:	2013-08-01 [Carlos]
#				added return $msg=9	for fail to save sent copy			
#
#	Date	:	2013-02-14 [Carlos]
#				modified js initSelectAllBlock() add ajax parameter MailStatus
#
#	Date	:	2013-02-06 [Carlos]
#				Connect Junk box twice to let mail server auto correct read/unread status
#
#	Date	:	2012-08-28 [Carlos]
#				Check and fix current page number if current page number exceeds actual page count
#
#	Date	:	2012-06-12 [Carlos]
#				Temporary hide auto clean trash/spam folder msg until server script is ready
#
#	Date	:	2012-05-28 [Carlos]
#				Changed sender/receiver column width from 150 to 15%
#
#	Date	:	2011-11-17 [Carlos]
#				Added select all mails at once in current folder feature
#				added js initSelectAllBlock(), selectAllUid(), deselectAllUid()
#
#	Date	:	2011-06-15 [Carlos]
#				Added js CacheMails() to sync new mails to cache db
#
#	Date	:	2011-04-19 [Carlos]
#				Added js functions to check if number of mails > 3000 show archive mail message 
#
#	Date	:	2011-04-14 [Carlos]
#	Details	:	Add readed counting column for Sent Folder
#
#	Date	:	2011-03-25 [Carlos]
#	Details :	Add js ProceedBatchRemoval() to perform batch removal
#
#	Date	:	2010-11-18 [Yuen]
#	Details :	Get the latest emails in INBOX by refreshing (duration = 10mins), controlled by setting $special_feature['imail_refresh_inbox']
#
#	Date	:	2010-11-05 [Yuen]
#	Details :	Display reminder in Junk/Trash folder so that user know when the emails will be removed automatically
#
#	Date	:	2010-10-07 [Carlos]
#	Details :	Add data column Size(Kb)
#
#   Date    :   2010-07-08 [Carlos]
#   Details :   Add Empty Trash button / Empty Junk button
#
#	Date 	:	2010-06-25 [Yuen]
#	Details	:	Adopt $sys_custom['custom_imail_page_size'] for default page size if set in settings.php
#
################ Change Log [End] #####################

//$NoLangOld20 = true;
//ini_set("memory_limit","300M");
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
//include_once($PATH_WRT_ROOT."includes/imap_gamma_ui.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$IMap = new imap_gamma();
//$IMapCache = new imap_cache_agent();
//
//$IMapCache->Get_Unseen_Mail_Subject($Folder);

# swtich to folder other than INBOX
//if($Folder!="INBOX")
//	@imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}INBOX.".$IMap->encodeFolderName($Folder));		# Go to the Corresponding Folder Currently selected

$CurrentPageArr['iMail'] = 1;

$page_size_default = (isset($sys_custom['custom_imail_page_size'])) ? $sys_custom['custom_imail_page_size'] : 20;		# default 20

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$pageSizeChangeEnabled = true;

$numPerPage = ($numPerPage != "") ? $numPerPage :$ck_page_size;
$page_size = $pageSize = ($numPerPage != "") ? $numPerPage : $page_size_default;
$display = $pageSize ;
$pageCount = 0;

# convert $field to $sort (imap format)
switch($field)
{
	//20100812
	case "0"	:	$sort = in_array($Folder,array($IMap->SentFolder,$IMap->DraftFolder))?"SORTTO":"SORTFROM";	break;
	case "1"	:	$sort = "SORTSUBJECT";		break;
	case "2"	:	$sort = "SORTARRIVAL";		break;
	case "3"	:	$sort = "SORTSIZE";			break;
	default	:	$sort = "SORTARRIVAL";	$field=2;
}  

# Mail Data
//$mailbox 	 = (isset($mailbox) && $mailbox != "") ? stripslashes($mailbox) : "Inbox";
$displayMode = (isset($displayMode) && $displayMode != "") ? $displayMode : "folder";
if(isset($Folder_b) && $Folder_b!=''){
	$Folder = base64_decode($Folder_b);
}else{
	$Folder 	 = (isset($Folder) && $Folder != "") ? stripslashes($Folder) : $IMap->InboxFolder;
}

# Page Info Data
//$sort 		 = (isset($sort) && $sort != "") ? $sort : "SORTARRIVAL";
$order 	 = (isset($order) && $order != "") ? $order : 1;
$pageNo 	 = (isset($pageNo) && $pageNo != "") ? $pageNo : 1;
$numPerPage  = (isset($numPerPage) && $numPerPage != "") ? $numPerPage : 0;
$currentPage = (isset($currentPage) && $currentPage != "") ? $currentPage : 0;

$CacheNewMail = false; // flag to control whether to sync new mails to db 

if (!$IMap->ExtMailFlag) {
	if($IMap->ConnectionStatus == true){
//		$lui->logTime['Imap_Reopen_Mailbox'] = Get_Timer();
		
		$imap_reopen_success = imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));		# Go to the Corresponding Folder Currently selected
		if($Folder == $IMap->SpamFolder) {
			// Connect Junk box twice to let mail server auto correct read/unread status
			$imap_reopen_success = imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));
		}
		if(!$imap_reopen_success){
			// Delay 1 second and connect again
			sleep(1);
			imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));
		}
		//Write_Message_Log('Gamma', "IMAP_Reopen_Mailbox: ".(Get_Timer() - $lui->logTime['Imap_Reopen_Mailbox']));
	
//		$IMap_obj 	   = imap_check($IMap->inbox);
//		$unseen_status = imap_status($IMap->inbox, "{".$IMap->host."}".$Folder, SA_UNSEEN);
//		$statusInbox   = imap_status($IMap->inbox, "{".$IMap->host."}Inbox", SA_UNSEEN);
		if($IMap->CacheMailToDB == true){
			$CacheMailMaxUIDs = $IMap->Check_New_Mails_For_Cached_Mails($Folder);
			$server_max_uid = $CacheMailMaxUIDs[$Folder]['ServerMaxUID'];
			$client_max_uid = !isset($CacheMailMaxUIDs[$Folder]['ClientMaxUID'])?0:$CacheMailMaxUIDs[$Folder]['ClientMaxUID'];
			$max_uid_diff = $server_max_uid - $client_max_uid;
			//if($client_max_uid == 0 && $max_uid_diff >= 20)
			//	$CacheNewMail = false; // maybe need full sync first
			//else
			if( $max_uid_diff > 0 )
				$CacheNewMail = true;
		}
		
		$imapInfo['pageInfo']['pageNo'] 	= $pageNo;
		$imapInfo['pageInfo']['numPerPage'] = $numPerPage;
		$imapInfo['imap_stream']   = $IMap->inbox;
		$imapInfo['server']    	   = $IMap->host;
		$imapInfo['password'] 	   = $IMap->CurUserPassword;
		$imapInfo['port']  	   	   = $IMap->mail_server_port;
		$imapInfo['username']  	   = $IMap->CurUserAddress;
//		$imapInfo['imap_obj'] 	   = $IMap_obj;
//		$imapInfo['unseen_status'] = $unseen_status;
//		$imapInfo['statusInbox']   = $statusInbox;
		$imapInfo['displayMode']   = $displayMode;
		$imapInfo['sort'] 		   = $sort;
		$imapInfo['reverse'] 	   = $order;
		//$imapInfo['currentPage']   = $currentPage;	//	old pagination with gamma approach
		$imapInfo['currentPage']   = ($pageNo - 1);	//	new pagination with gamma approach
		$imapInfo['messageSeq']    = $messageSeq;
		$imapInfo['messageNo']     = $messageNo;
	
		# Can be ignored
		$imapInfo['mailbox']   	   = $Folder;			// orig: mailbox
		
//		$status   	 = $imapInfo['unseen_status'];
//		$IMap_obj 	 = $imapInfo['imap_obj'];
//		$statusInbox = $imapInfo['statusInbox'];
		
		//20100812
		$isDraftInbox = ($Folder == $IMap->DraftFolder) ? 1 : 0;
		$isOutbox = ($Folder == $IMap->SentFolder) ? 1 : 0;
		
		# Get Sorting Index
		$array = $IMap->Get_Sort_Index($IMap, $imapInfo, $search, $MailStatus);
		
		# Page Settings
		if (isset($array)) {
			// DECIDE ON NUMBER OF PAGES TO SHOW
			$pageCount = ceil(count($array) / $pageSize) ;
			// Check and fix current page number if current page number exceeds actual page count
			$pageNo = min($pageNo,$pageCount);
			$imapInfo['pageInfo']['pageNo'] = $pageNo;
			if(($pageCount-1) >= 0){
				$imapInfo['currentPage'] = min($imapInfo['currentPage'],$pageCount-1);
			}
			// GET NUMBER OF MESSAGES TO SHOW ON THIS PAGE
			if (count($array) < ($pageSize + ($pageSize * $imapInfo['currentPage']))) {
				$display = count($array) - ($pageSize * $imapInfo['currentPage']) ;
			}
		}		
		
		# Assign Page Info for Pagination
		$imapInfo['pageInfo']['pageSize']   = $pageSize;
		$imapInfo['pageInfo']['totalCount'] = (is_array($array)) ? count($array) : 0;
		$imapInfo['pageInfo']['pageStart']  = ($pageSize * ($imapInfo['pageInfo']['pageNo'] - 1) + 1);
		
		# build $MailHeaderList, pass to libdbtable2007a.php to handle
		for ($i=(0 + ($pageSize * $imapInfo['currentPage'])) ; $i<($display + ($pageSize * $imapInfo['currentPage'])) ; $i++) {
			$MailHeaderList[] = imap_headerinfo($IMap->inbox, $array[$i], 80, 80);
			//$Uid[] 	= @imap_uid($IMap->inbox, $array[$i]);
			$msgno[] = $array[$i];
		}		
		
		$MailFlags = $IMap->GetMailFlags($msgno,$Folder);
		$MailPriority = $IMap->GetMailPriority($msgno,$Folder);
		if($Folder == $IMap->SentFolder){
			// $MailReadedCount[MsgNo] => [MailID,NumOfReaded,Total] to be used in libdbtable2007a.php
			$MailXeClassMailID = array();
			$MailMsgNo2XeClassMailID = array();
			if(sizeof($IMap->message_cache)>0){
				foreach($IMap->message_cache as $k => $message_cache_item){
					$MailXeClassMailID[$message_cache_item['MsgNo']] = "'".$message_cache_item['x-eclass-mailid']."'";
					$MailMsgNo2XeClassMailID[$message_cache_item['MsgNo']] = $message_cache_item['x-eclass-mailid'];
				}
				$MailReadedCountAssoc = $IMap->Get_Mail_Receipt_Readed_Counting_Info($MailXeClassMailID);
				$MailReadedCount = array();
				foreach($MailMsgNo2XeClassMailID as $k => $xeclassmailid){
					if(!isset($MailReadedCountAssoc[$xeclassmailid])) continue;
					$MailReadedCount[$k] = $MailReadedCountAssoc[$xeclassmailid];
				}
			}
		}
	}
}
$default_field = 2;
//$default_field = ($noWebmail || ($FolderID == 0 || $FolderID == 1) ? 3:4);
if($field=="") $field = $default_field;

$li = new libdbtable2007($field, $order, $pageNo);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

//$keyword = trim($keyword);

//$RecordType = $FolderID;
#$li->IsColOff = 8; #"imail";
$li->IsColOff = "imail_gamma";
$pos=0;



$senderreceiver = ($isDraftInbox||$isOutbox)?$i_frontpage_campusmail_recipients:$i_frontpage_campusmail_sender;
$li->column_list .= "<td width='1%' class='tabletop' >". $i_frontpage_campusmail_icon_status_2007."</td>\n";
$li->column_list .= "<td width='1%' class='tabletop' ><img align='absmiddle' src='$image_path/$LAYOUT_SKIN/icon_star_off.gif' border='0' /></td>\n";
$li->column_list .= "<td width='1%' class='tabletop' ><img align='absmiddle' src='$image_path/$LAYOUT_SKIN/iMail/icon_important_title.gif' border='0' /></td>\n";
$li->column_list .= "<td width='1%' class='tabletop' >". $i_frontpage_campusmail_icon_notification_2007."</td>\n";
$li->column_list .= "<td width='1%' class='tabletop' >&nbsp;</td>\n";
$li->column_list .= "<td width='15%' class='tabletop' >".$li->column($pos++, $senderreceiver)."</td>\n";
$li->column_list .= "<td width='60%' class='tabletop' >".$li->column($pos++, $Lang['Gamma']['Subject'])."</td>\n";
$li->column_list .= "<td width='150' class='tabletop' >".$li->column($pos++, $i_frontpage_campusmail_date)."</td>\n";
$li->column_list .= "<td width='60' class='tabletop' >".$li->column($pos++, $i_frontpage_campusmail_size)."</td>\n";
if($Folder==$IMap->SentFolder) $li->column_list .= "<td width='60' class='tabletop' >$i_frontpage_campusmail_read / $i_frontpage_campusmail_total</td>\n";
$li->column_list .= "<td width='25' class='tabletop' ><input id='CheckAllBox' type='checkbox' onclick='checkAll(this)'></td>\n";

$li->no_col = $pos+6;
if($Folder==$IMap->SentFolder) $li->no_col+=1;

# tag information
# highlight left menu

$foldertitle = $IMap->getMailDisplayFolder(IMap_Decode_Folder_Name($Folder));
//20100812
switch(trim($Folder))
{
	case $IMap->SentFolder	:	
		$CurrentPage = "PageCheckMail_Outbox";	
		$foldericon = "icon_outbox.gif";
	break;
	
	case $IMap->DraftFolder	:	
		$CurrentPage = "PageCheckMail_Draft";	
		$foldericon = "icon_draft.gif";
	break;
	
	case $IMap->TrashFolder	:	
		$CurrentPage = "PageCheckMail_Trash";	
		$foldericon = "icon_trash.gif";
	break;
	
	case $IMap->SpamFolder	:	
		$CurrentPage = "PageCheckMail_Spam";	
		$foldericon = "icon_spam.gif";
	break;
	
	case ""				:
	case $IMap->InboxFolder		:	
		$CurrentPage = "PageCheckMail_Inbox";	
		$foldericon = "icon_inbox.gif";
		$refresh_inbox_list = ($special_feature['imail_refresh_inbox']) ? "<meta http-equiv='refresh' content='600' />" : "";
	break;
		
	default:
		$foldericon = "icon_folder_close.gif";
}
if($foldericon)
	$iMailImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/$foldericon' align='absmiddle' border='0' />";
$iMailTitle1 = "{$foldertitle}";

$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();

$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0'><tr><td style=\"vertical-align: bottom;\" ><span class='imailpagetitle'>".$iMailImage1.$iMailTitle1."</span>".$unreadLink."</td><td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>";
$TAGS_OBJ[] = array($iMailTitle, "", 0);

$linterface = new interface_html("imail_default.html");
$linterface->LAYOUT_START();

//Get msg
if($msg == "9"){
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=red>".$Lang['Gamma']['ReturnMsg']['FailToSaveSentCopy']."</font>")."</td></tr>";
}else if ($msg == "3")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_gen_msg_email_remove."</font>")."</td></tr>";
}else if($msg == "-1")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=red>".$Lang['iMail']['ErrorMsg']['MailCannotSendOutSuccessfully']."</font>")."</td></tr>";
} 
else if ($exmsg == "1")
{
	if ($signal == "2")
	{		
		$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_gen_msg_email_save."</font>")."</td></tr>";
	} 
	else 
	{
		$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_msg_email."</font>" )."</td></tr>";
	}
} 
else if ($exmsg == "4")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_gen_msg_email_restore."</font>")."</td></tr>";
}
else if ($exmsg == "0")			### Only For IP2.0
{
	if($signal == "1")
	{
		$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$i_con_msg_email."</font>" )."</td></tr>";
	}
}								### End at here
else if($msg == "5")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$Lang['Gamma']['ReturnMsg']['EmptyTrashSuccess']."</font>")."</td></tr>";
}else if($msg == "6")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=red>".$Lang['Gamma']['ReturnMsg']['EmptyTrashUnsuccess']."</font>")."</td></tr>";
}else if($msg == "7")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=green>".$Lang['Gamma']['ReturnMsg']['EmptyJunkSuccess']."</font>")."</td></tr>";
}else if($msg == "8")
{
	$xmsg = "<tr><td align='right' >".$linterface->GET_SYS_MSG("","<font color=red>".$Lang['Gamma']['ReturnMsg']['EmptyJunkUnsuccess']."</font>")."</td></tr>";
}
else 
{
	$xmsg = "";
}

#############################  gen tool bar ########################### 
#

$date_range_ary = array();
$date_range_ary[] = array('1week',$Lang['Gamma']['OneWeek']);
$date_range_ary[] = array('1month',$Lang['Gamma']['OneMonth']);
$date_range_ary[] = array('3month',$Lang['Gamma']['ThreeMonths']);
$date_range_ary[] = array('6month',$Lang['Gamma']['SixMonths']);
$date_range_ary[] = array('1year',$Lang['Gamma']['OneYear']);
$date_range_ary[] = array('custom',$Lang['Gamma']['CustomDateRange']);

$DateRangeMethod = isset($DateRangeMethod) && in_array($DateRangeMethod,array('1week','1month','3month','6month','1year','custom'))? $DateRangeMethod : '3month'; 
$date_range_selection = $linterface->GET_SELECTION_BOX($date_range_ary,' id="DateRangeMethod" name="DateRangeMethod" onchange="onDateRangeMethodChanged(this);" ','',$DateRangeMethod);

$ts_today = time();
$ToDate = isset($ToDate) && $ToDate!="" ? $ToDate : date("Y-m-d",$ts_today);
$FromDate = isset($FromDate) && $FromDate != "" ? $FromDate : date("Y-m-d",strtotime("-1 month",$ts_today));
$FromDateInput = $linterface->GET_DATE_PICKER("StartDate",$FromDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $DateRangeMethod != 'custom', $cssClass="textboxnum");
$ToDateInput = $linterface->GET_DATE_PICKER("EndDate",$ToDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $DateRangeMethod != 'custom', $cssClass="textboxnum");


# Search bar
$spacer = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" />';
$searchbar = '<form id="SearchForm" name="SearchForm" method="get" action="search_result.php" onsubmit="return false;">';
	//$searchbar .= $linterface->Get_Search_Box_Div("keyword", ($Keyword?$Keyword:"Search on Subject"),' onfocus="if (this.value == \'Search on Subject\') this.value=\'\';"');
	
	
	$searchbar .= '<div class="selectbox_group selectbox_group_filter">';
		$searchbar .= '<a href="javascript:void(0);" onclick="if(document.getElementById(\'search_option\').style.visibility==\'hidden\'){MM_showHideLayers(\'search_option\',\'\',\'show\');}else{MM_showHideLayers(\'search_option\',\'\',\'hide\');}"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_search.gif" width="20" height="20" border="0" align="absmiddle" />'.$Lang['Btn']['Search'].'</a>';
	$searchbar .= '</div>';
	$searchbar .= '<p class="spacer"></p>';
	$searchbar .= '<div id="search_option" class="selectbox_layer selectbox_group_layer" style="width:640px;visibility:hidden;">';
	$searchbar .= '<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>
						<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$i_frontpage_campusmail_recipients.'</td>
							<td valign="top">
							<input type="text" name="KeywordTo" size="50" class="textboxtext" value="'.$KeywordTo.'" />
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$i_frontpage_campusmail_sender.'</td>
							<td valign="top">
							<input type="text" name="KeywordFrom" size="50" class="textboxtext" value="'.$KeywordFrom.'" />
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['Gamma']['cc'].'</td>
							<td valign="top">
							<input type="text" name="KeywordCc" size="50" class="textboxtext" value="'.$KeywordCc.'" />
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['Gamma']['bcc'].'</td>
							<td valign="top">
							<input type="text" name="KeywordBcc" size="50" class="textboxtext" value="'.$KeywordBcc.'" />
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['Gamma']['Subject'].'</td>
							<td valign="top">
							<input type="text" name="KeywordSubject" size="50" class="textboxtext" value="'.$KeywordSubject.'" />
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$i_frontpage_campusmail_message.'</td>
							<td valign="top">
							<input type="text" name="KeywordBody" size="50" class="textboxtext" value="'.$KeywordBody.'" />
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$i_general_record_date.'</td>
							<td valign="top" class="tabletext">
							'.$date_range_selection.$Lang['General']['From'].'&nbsp;'.$FromDateInput.$Lang['General']['To'].'&nbsp;'.$ToDateInput.'
							</td>
						</tr>
						</table>
						</td>
					</tr>
					</table>';
	
	
		$searchbar .= '<p class="spacer"></p>';
		$searchbar .= '<div class="edit_bottom">';
		$searchbar .= '<input type="button" onmouseout="this.className=\'formsmallbutton\'" onmouseover="this.className=\'formsmallbuttonon\'" value="'.$Lang['Btn']['Search'].'" onclick="searchSubmit(document.getElementById(\'SearchForm\'));MM_showHideLayers(\'search_option\',\'\',\'hide\');" class="formsmallbutton ">&nbsp;';
		$searchbar .= '<input type="button" onmouseout="this.className=\'formsmallbutton\'" onmouseover="this.className=\'formsmallbuttonon\'" value="'.$Lang['Btn']['Cancel'].'" onclick="MM_showHideLayers(\'search_option\',\'\',\'hide\');" class="formsmallbutton ">';
		$searchbar .= '</div>';
	$searchbar .= '</div>';
	
	$searchbar .= '<input type="hidden" name="displayMode" id="displayMode" value="search">';
	//$searchbar .= '<input type="hidden" name="Folder" id="Folder" value="'.urlencode($Folder).'">';
	$searchbar .= '<input type="hidden" name="SearchFolder_b[]" value="'.base64_encode($Folder).'" />';
	$searchbar .= '<input type="hidden" name="Folder_b" value="'.base64_encode($Folder).'" />';
	$searchbar .= '<input type="hidden" name="SearchFolder[]" value="'.$Folder.'" />';
	$searchbar .= '<input type="hidden" name="Folder" value="'.$Folder.'" />';
	$searchbar .= '<input type="hidden" name="page_from" value="viewfolder.php" />';
	$searchbar .= '<input type="hidden" name="mailbox" value="'.$Folder.'" />';
	$searchbar .= '<input type="hidden" id="FromDate" name="FromDate" value="" />';
	$searchbar .= '<input type="hidden" id="ToDate" name="ToDate" value="" />';
	$searchbar .= '<input type="hidden" id="isAdvanceSearch" name="isAdvanceSearch" value="1" />';
	
$searchbar .= '</form>';

# Move to Selection
$MoveTo = $IMap->getMoveToSelection("targetFolder",$Folder);

# deleteBtn
$deleteBtn  = "<a href=\"javascript:checkRemove(document.form1,'Uid[]','remove.php')\" class=\"tabletool\">";
$deleteBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\" />";
$deleteBtn .= $button_remove."</a>"; 
  
# reportSpam/ report non spam btn 
//20100812
$confirmMsg = $Folder==$IMap->SpamFolder?$Lang['Gamma']['ConfirmMsg']['reportNonSpam']:$Lang['Gamma']['ConfirmMsg']['reportSpam'];
$BtnName = $Folder==$IMap->SpamFolder?$Lang['Gamma']['reportNonSpam']:$Lang['Gamma']['reportSpam'];
if($webmail_info['bl_spam']==true){
	$ReportSpamBtn  = "<a href=\"javascript:checkAlert(document.form1,'Uid[]','report_spam.php','$confirmMsg')\" class=\"tabletool\">";
	$ReportSpamBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_spam.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\" />";
	$ReportSpamBtn .= $BtnName."</a>"; 
}else{
	$ReportSpamBtn = "";
}

if($sys_custom['iMailPlus']['EDSReportSpam'] && $Folder != $IMap->EDSReportSpamFolder){
	$ReportSpamBtn .= "<a href=\"javascript:checkAlert(document.form1,'Uid[]','eds_report_spam.php','".str_replace('<!--FOLDER-->','EDS_ReportSpam',$Lang['Gamma']['ConfirmMsg']['ReportSpamToFolder'])."')\" class=\"tabletool\">";
	$ReportSpamBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_spam.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\" />";
	$ReportSpamBtn .= $Lang['Gamma']['reportSpam']."</a>"; 
}

//20100812
if(!$sys_custom['iMailHideEmptyTrashButton'] && ($Folder == $IMap->TrashFolder || $Folder == $IMap->SpamFolder))
{
	# empty trash box / empty junk box btn 
	$EmptyMailBoxBtn  = "<td nowrap><a href=\"javascript:AlertPost(document.form1,'clear_folder.php','";
	$EmptyMailBoxBtn .= $Folder == $IMap->TrashFolder?$Lang['Gamma']['ConfirmMsg']['ViewMail']['emptyTrash']:$Lang['Gamma']['ConfirmMsg']['ViewMail']['emptyJunk'];
	$EmptyMailBoxBtn .= "')\" class=\"tabletool\">";
	$EmptyMailBoxBtn .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\" />";
	$EmptyMailBoxBtn .= $Folder == $IMap->TrashFolder?$Lang['Gamma']['EmptyTrash']:$Lang['Gamma']['EmptyJunk'];
	$EmptyMailBoxBtn .= "</a></td>";
}else $EmptyMailBoxBtn = "";

# $unseenfilter
foreach($Lang['Gamma']['UnseenStatus'] as $key => $value)
	$StatusAry[] = array($key,$value);
$unseenfilter = getSelectByArray($StatusAry,"name='MailStatus' onchange='this.form.submit()'",$MailStatus,0,1);

# mark mail as read / unread
array_shift($StatusAry);
$MarkAsSelection = getSelectByArray($StatusAry,"name='MarkAs' onchange='this.form.action=\"mark_read_unread.php\"; this.form.submit()'",'',0,0,$Lang['Gamma']['MarkAs']);
$MarkAsSelection = getSelectByArray($StatusAry,"name='MarkAs' onchange='MarkMailAs(this)'",'',0,0,$Lang['Gamma']['MarkAs']);

//*** TRACE ERROR ***
//if($IMap->inbox === FALSE || $IMap->inbox === NULL){
//	$last_error = imap_last_error();
//	if(trim($last_error)!='') debug_r($last_error);
//}

#
############################# gen tool bar end #######################
if ($Folder=="INBOX.Trash" && $IMap->days_in_trash>0)
{
	$message_for_junk_and_bin = str_replace("<!--DaysNum-->", $IMap->days_in_trash, $Lang['Gamma']['WarningHouseKeepingTrash']);
} elseif ($Folder=="INBOX.Junk" && $IMap->days_in_spam>0)
{
	$message_for_junk_and_bin = str_replace("<!--DaysNum-->", $IMap->days_in_spam, $Lang['Gamma']['WarningHouseKeepingJunk']);
}

?>


<?= $refresh_inbox_list ?>

<style type="text/css">
.highlight_block_normal {
	text-align: center;
	line-height: 100%; 
	min-height: 1.375em; 
	width: 99%; 
	display: inline-block; 
	background: none repeat scroll 0pt 0pt #FFFFCC; 
	border-color: #FFFFCC; 
	padding: 3px 0pt 3px; 
	margin: 3px;
}
.highlight_block_active {
	text-align: center;
	line-height: 100%; 
	min-height: 1.375em; 
	width: 99%; 
	display: inline-block; 
	background: none repeat scroll 0pt 0pt #FFFF80; 
	border-color: #FFFF80; 
	padding: 3px 0pt 3px; 
	margin: 3px;
}
</style>

<link href="/templates/jquery/jquery.alerts.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.alerts.js"></script>

<script language="javascript">
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length;
	
    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

function jCheckAlert(obj,element,page,msg,cancel_caller,cancel_callback)
{
	if(countChecked(obj,element)==0){
	    jAlert(globalAlertMsg2);
	    if(cancel_caller && cancel_callback){
    		cancel_callback.apply(cancel_caller);
    	}
	}else{
		jConfirm(msg, 'Confirmation Dialog', function(r) {
		    if(r){
		    	// OK
		    	obj.action=page;
	        	obj.method="POST";
	        	obj.submit();
		    }else{
		    	// Cancel
		    	if(cancel_caller && cancel_callback){
		    		cancel_callback.apply(cancel_caller);
		    	}
		    }
		});
	}
}

function openExport()
{
     if(confirm('<?=$i_campusmail_alert_export?>'))
     {
         newWindow('export.php?UserFolderID=<?=$FolderID?>',4);
     }
}


function MoveMailTo(obj)
{
	if (obj.selectedIndex != 0)
	{
		var cancel_callback = function()
		{
			obj.selectedIndex = 0;
		}
		jCheckAlert(document.form1,'Uid[]','move_email.php','<?=$i_CampusMail_New_alert_moveto?>',this,cancel_callback);
	}
}

function MarkMailAs(obj)
{
	var confirmMsg;
	switch(obj.value)
	{
		case "SEEN":
			confirmMsg = "<?=$Lang['Gamma']['ConfirmMsg']['MarkAsSeen']?>";
			break;
		case "FLAGGED":
			confirmMsg = "<?=$Lang['Gamma']['ConfirmMsg']['MarkAsStarred']?>";
			break;
		default:
			confirmMsg = "<?=$Lang['Gamma']['ConfirmMsg']['MarkAsUnseen']?>";
			break;
	}
	
	if(obj.selectedIndex)
	{
		var cancel_callback = function(){
			obj.selectedIndex=0; 
		}
		jCheckAlert(document.form1,'Uid[]','mark_read_unread.php',confirmMsg,this,cancel_callback);
		//if(!checkAlert(document.form1,'Uid[]','mark_read_unread.php',confirmMsg))
		//{
		//	obj.selectedIndex=0;    
		//}	
	}
}

function checkAll(obj)
{
	if(obj.checked)
	{
		$("input[name='Uid[]']").not('.classUid').attr("checked","checked");
		$("table#MailList>tbody>tr:not(:first):not(:last)").find("td").removeClass("tabletext iMailrow").addClass("tabletext iMailrowSelect");
		
		if(!$("table#MailList").prev().is('div')){
			initSelectAllBlock();
		}
	}
	else
	{
		$("input[name='Uid[]']").not('.classUid').attr("checked","");
		$("table#MailList>tbody>tr:not(:first):not(:last)").find("td").removeClass("tabletext iMailrowSelect").addClass("tabletext iMailrow");
	}
}

function setFlag(folder,uid,obj)
{
	var imgsrc = obj.src.substr(obj.src.lastIndexOf("/")+1)
	var setflag = imgsrc=="icon_star_on.gif"?0:1; //set off if on
	if(setflag==1)
		obj.src=obj.src.replace("icon_star_off.gif","icon_star_on.gif")
	else
		obj.src=obj.src.replace("icon_star_on.gif","icon_star_off.gif")

	$.post("ajax_task.php",
		{
			"task"		: "SetMailFlag",
			"Folder"	: folder,
			"Uid"		: uid,
			"setflag"	: setflag
		}
	);

}

$().ready(function(){
	initHighlightRow();
	$("input[name='Uid[]']:checked").parent().parent().find("td").removeClass("iMailrow").addClass("iMailrowSelect")
})

var mousedown;
var action = '';
var selectingoption = false; //use to avoid deselecting a mail when click on a selection box option
var disablehighlight = false; 
var startrow,endrow;
function initHighlightRow()
{
	mousedown = false;
	// highlight ticked row on load	
	$("input[name='Uid[]']").click(function(evt){
		// the tick checkbox action will be handled on mousedown event
		//return false;
		if(!disablehighlight)
		{
			var thisCheckbox = $(this);
			var boxchecked = thisCheckbox.is(":checked");
			//thisCheckbox.attr("checked",boxchecked);
			if(boxchecked)
			{
				thisCheckbox.closest('tr').find("td").removeClass("iMailrow").addClass("iMailrowSelect");
				action = 'select';
			}
			else
			{
				thisCheckbox.closest('tr').find("td").removeClass("iMailrowSelect").addClass("iMailrow");
				action = 'deselect';
			}
	
			if($("input[name='Uid[]']:not(:checked)").length==0)
				$("#CheckAllBox").attr("checked","checked");
			else
				$("#CheckAllBox").attr("checked","");
		}
	});
	$("input[name='Uid[]']").mousedown(function(evt){
		evt.stopPropagation();
	});
	
	$(document).mousedown(function(){mousedown=true; }).mouseup(function(){mousedown=false; action='';})
	
	// disable select row on certain action
	$("select").click(function(){selectingoption=true;}); 
	$("a").mouseover(function(){disablehighlight=true; }).mouseout(function(){disablehighlight=false; });
	$("img.mailflag").mouseover(function(){disablehighlight=true; }).mouseout(function(){disablehighlight=false;}); 
	
	$("table#MailList>tbody>tr:not(:first:last)").mouseover(function(){
		if(!selectingoption)
		{
			if(mousedown==true)
			{
				endrow= $("table#MailList>tbody>tr").index(this);
				if(endrow>startrow)
				{
					var lower = startrow;
					var upper = endrow+1;
				}
				else
				{
					var lower = endrow-1;
					var upper = startrow;
				}
				
				if(action=='select')
				{
					$("table#MailList>tbody>tr:lt("+upper+"):gt("+lower+")").children().removeClass("iMailrow").addClass("iMailrowSelect");
					$("table#MailList>tbody>tr:lt("+upper+"):gt("+lower+")").find("[name='Uid[]']:not(:checked)").attr("checked","checked");
				}
				else
				{
					$("table#MailList>tbody>tr:lt("+upper+"):gt("+lower+")").children().removeClass("iMailrowSelect").addClass("iMailrow");
					$("table#MailList>tbody>tr:lt("+upper+"):gt("+lower+")").find("[name='Uid[]']:checked").attr("checked","");
				}
				
				if($("input[name='Uid[]']:not(:checked)").length==0)
					$("#CheckAllBox").attr("checked","checked");
				else
					$("#CheckAllBox").attr("checked","");
			}
		}
		selectingoption=false;
	}).mousedown(function(evt){
		startrow=$("table#MailList>tbody>tr").index(this);
		if(!disablehighlight)
		{
			var boxchecked = !$(this).find("[name='Uid[]']").attr("checked")
			$(this).find("[name='Uid[]']").attr("checked",boxchecked);
			if(boxchecked)
			{
				$(this).find("td").removeClass("iMailrow").addClass("iMailrowSelect");
				action = 'select';
			}
			else
			{
				$(this).find("td").removeClass("iMailrowSelect").addClass("iMailrow");
				action = 'deselect';
			}
	
			if($("input[name='Uid[]']:not(:checked)").length==0)
				$("#CheckAllBox").attr("checked","checked");
			else
				$("#CheckAllBox").attr("checked","");
		}
	});
}

<?php
if($Folder == $IMap->InboxFolder && $_SESSION['intranet_iMail_archived']!=1){ // Inbox
?>
function getMailCount()
{
	var limit = <?=(isset($sys_custom['iMail']['ArchiveMailNumber'])?$sys_custom['iMail']['ArchiveMailNumber']:"3000")?>;
	$.get(
		"aj_archive_mail_task.php",
		{
			"Action":"GetMailCount",
			"Folder":"<?=urlencode($Folder)?>"
		},
		function(response){
			if(response>limit){
				getArchiveMailMessageLayer();
			}
		}
	);
}

function getArchiveMailMessageLayer()
{
	$.get(
		"aj_archive_mail_task.php",
		{
			"Action":"GetArchiveMailMessageLayer"
		},
		function(ReturnData){
			$('body').prepend(ReturnData);
			var pos = $('#html_body_frame').position();
			$('div#sub_layer_imail_archive_mail_v30').css(
				{
					'position':'absolute',
					'left': pos.left+20,
					'top': pos.top+20
				}
			);
		}
	);
}

function goProceed()
{
	window.location = "./archive_mail.php?Folder=<?=urlencode($Folder)?>";
}

function setRemindLater()
{
	$.get(
		"aj_archive_mail_task.php",
		{
			"Action":"SetRemindLater"
		},
		function(ReturnData){
			$('div#sub_layer_imail_archive_mail_v30').remove();
		}
	);
}
<?php
}
?>

function ProceedBatchRemoval()
{
	$.get(
		"ajax_task.php",
		{
			"task":"ProceedBatchRemoval"
		},
		function(response){
			if(response=='1'){
				location.reload();
			}
		}
	);
}
<?php if($CacheNewMail == true){?>
function CacheMails()
{
	$.post(
		"ajax_cache_mail.php",
		{
			"Action":'SyncNewMails',
			"Folder":'<?=urlencode($Folder)?>',
			"ServerMaxUID":'<?=$server_max_uid?>',
			"ClientMaxUID":'<?=$client_max_uid?>' 
		},
		function(response){
			
		}
	);
}
<?php } ?>

$(document).ready(function(){
	setTimeout(function(){
		onDateRangeMethodChanged(document.getElementById('DateRangeMethod'));
	},100);
	<?php
	if($Folder == $IMap->InboxFolder && $_SESSION['intranet_iMail_archived']!=1){ // Inbox
	?>
		getMailCount();
	<?php
	}
	?>
	
	<?php
	if($CacheNewMail == true){
	?>
		CacheMails();
	<?php
	}
	?>
	
	<?php
	if($_SESSION['SSV_GAMMA_BATCH_REMOVAL_STATE']!=1){
	?>
		ProceedBatchRemoval();
	<?php
	}
	?>
	
	function isElementInView(elem)
	{
	    var $elem = $(elem);
	    var $window = $(window);
	
	    var docViewTop = $window.scrollTop();
	    var docViewBottom = docViewTop + $window.height();
	
	    var elemTop = $elem.offset().top;
	    var elemBottom = elemTop + $elem.height();
	
	    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	}
	
	var GlobalToolbar = {
		"top":$('select[name="MailStatus"]').offset().top,
		"left":$('select[name="MailStatus"]').offset().left,
		"position":"static",
		"zIndex":$('#ToolbarTable').css('zIndex')
	};
	
	$(window).scroll(function(){
		var toolbar_table = document.getElementById('ToolbarTable');
		var $toolbar = $('#Toolbar');
		if(isElementInView(toolbar_table)){
			$toolbar.css({'position':'static', 'top':GlobalToolbar["top"]+'px', 'left':GlobalToolbar["left"]+'px', 'background-color':'#FFFFFF','border':'0px none #FFFFFF', 'zIndex':GlobalToolbar["zIndex"]});
			GlobalToolbar["position"] = "static";
		}else{
			$toolbar.css({'position':'fixed', 'top': '0px', 'left': ($(window).width() - $toolbar.width() - 50) + 'px', 'background-color':'#FFFFFF', 'border':'1px solid #CCCCCC', 'zIndex':1000});
			$toolbar.css({'top': '0px', 'left': ($(window).width() - $toolbar.width() - 50) + 'px'});
			GlobalToolbar["position"] = "fixed";
		}
	}).resize(function(){
		if(GlobalToolbar["position"]=="fixed"){
			var $toolbar = $('#Toolbar');
			$toolbar.css('left',($(window).width() - $toolbar.width() - 50) + 'px');
		}
	});
});

function initSelectAllBlock()
{
	var curFolder = $('input[name=Folder]').val();
	var mailStatus = $('[name="MailStatus"]').val();
	$.get(
		"ajax_task.php",
		{
			"task": "GetFolderAllUID",
			"Folder": encodeURIComponent(curFolder),
			"MailStatus": mailStatus  
		},
		function(data){
			if(data == "") return;
			var uidArr = data.split(',');
			var total = uidArr.length;
			var visibleUid = [];
			$('input[name=Uid[]]').each(function(i){
				visibleUid.push($(this).val());
			});
			if(total == 0 || total <= visibleUid.length) return;
			var folderName = $('input[name=FolderName]').val();
			var selectedCnt = $('input[name=Uid[]]:checked').length;
			var msg1 = '<?=$Lang['Gamma']['CommonMsg']['CurrentPageSelectedMails']?>';
			var msg2 = '<?=$Lang['Gamma']['CommonMsg']['SelectAllMailsInFolder']?>';
			msg1 = msg1.replace('<!--NUMBER-->',selectedCnt);
			msg2 = '<a href="javascript:selectAllUid();">'+msg2.replace('<!--FOLDER-->',folderName+'</a>');
			msg2 = msg2.replace('<!--NUMBER-->',total);
			var divBlock = '<div class="highlight_block_normal">' + msg1 + ' ' + msg2 + '</div>';
			$("table#MailList").before(divBlock);
			
			var frm = $('form[name=form1]');
			for(var i=0;i<uidArr.length;i++){
				if(visibleUid.indexOf(uidArr[i])==-1){
					frm.append('<input type="checkbox" name="Uid[]" value="'+uidArr[i]+'" class="classUid" style="display:none;"/>');
				}
			}
		}
	);
}

function selectAllUid()
{
	$("input[name='Uid[]']").attr("checked","checked");
	$("table#MailList>tbody>tr:not(:first):not(:last)").find("td").removeClass("tabletext iMailrow").addClass("tabletext iMailrowSelect");
	$('table#MailList').prev().remove();
	var folderName = $('input[name=FolderName]').val();
	var selectedCnt = $("input[name='Uid[]']:checked").length;
	var msg1 = '<?=$Lang['Gamma']['CommonMsg']['SelectedAllMailsInFolder']?>';
	var msg2 = '<a href="javascript:deselectAllUid();">'+'<?=$Lang['Gamma']['CommonMsg']['ClearSelection']?>'+'</a>';
	msg1 = msg1.replace('<!--FOLDER-->',folderName);
	msg1 = msg1.replace('<!--NUMBER-->',selectedCnt);
	var divBlock = '<div class="highlight_block_active">' + msg1 + ' ' + msg2 + '</div>';
	$("table#MailList").before(divBlock);
}

function deselectAllUid()
{
	$('table#MailList').prev().remove();
	$("input.classUid").remove();
	$("input#CheckAllBox").attr("checked","");
	$("input[name='Uid[]']").attr("checked","");
	$("table#MailList>tbody>tr:not(:first):not(:last)").find("td").removeClass("tabletext iMailrowSelect").addClass("tabletext iMailrow");
}

function onDateRangeMethodChanged(obj)
{
 	var selected_value = $(obj).val();
 	if(selected_value == 'custom'){
 		$('#StartDate').attr('disabled',false);
 		$('#StartDate').next('img').show();
 		$('#EndDate').attr('disabled',false);
 		$('#EndDate').next('img').show();
 	}else{
 		$('#StartDate').attr('disabled',true);
 		$('#StartDate').next('img').hide();
 		$('#EndDate').attr('disabled',true);
 		$('#EndDate').next('img').hide();
 		
 		var dateObj = new Date();
 		var end_date = getFormatDate(dateObj);
 		var start_date = end_date;
 		
 		if(selected_value == '1week'){
 			dateObj.setDate(dateObj.getDate()-7);
 		}else if(selected_value == '1month'){
 			dateObj.setMonth(dateObj.getMonth()-1);
 		}else if(selected_value == '3month'){
 			dateObj.setMonth(dateObj.getMonth()-3);
 		}else if(selected_value == '6month'){
 			dateObj.setMonth(dateObj.getMonth()-6);
 		}else if(selected_value == '1year'){
 			dateObj.setFullYear(dateObj.getFullYear()-1);
 		}
 		start_date = getFormatDate(dateObj);
 		
 		$('#StartDate').val(start_date);
 		$('#EndDate').val(end_date);
 	}
}

function getFormatDate(dateObj)
{
	var date_year = dateObj.getFullYear();
	var date_month = dateObj.getMonth()+1;
	var date_day = dateObj.getDate();
	date_month = date_month < 10? '0'+date_month : ''+date_month; 
	date_day = date_day < 10 ? '0'+date_day : ''+date_day; 
	var date_string = date_year + '-' + date_month + '-' + date_day;
	
	return date_string;
}

function searchSubmit(formObj)
{
	var from_date_obj = document.getElementById('StartDate');
	var to_date_obj = document.getElementById('EndDate');
	var is_valid = true;
	
	if(!check_date_without_return_msg(from_date_obj)){
		is_valid = false;
	}
	if(!check_date_without_return_msg(to_date_obj)){
		is_valid = false;
	}
	
	if(is_valid){
		if(from_date_obj.value > to_date_obj.value){
			is_valid = false;
			alert('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
			return;
		}
		
	}
	
	if(is_valid){
		$('#FromDate').val(from_date_obj.value);
		$('#ToDate').val(to_date_obj.value);
		formObj.submit();
	}
}
</script>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">	
	<?=$xmsg?>
	<tr>
		<td >
		<br />
		<table id="ToolbarTable" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr class="table-action-bar">
			<td>
			<font color="#880000"><?=$message_for_junk_and_bin?></font>
			<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td>
					<?=$searchbar?>
				</td>
			</tr>
			</table>
			</td>		
			<td align="right" >
			<form name="form1" method="get" >
			<table id="Toolbar" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td><?=$unseenfilter?></td>
				<td><?=$MoveTo?></td>
				<td><?=$MarkAsSelection?></td>
				<?=$EmptyMailBoxBtn?>
				<td nowrap><?=$deleteBtn?></td>
				<td nowrap><?=$ReportSpamBtn?></td>
			</tr>
			</table>
			</td>
		</tr>
		</table>		
		</td>
	</tr>
	
	<tr>
	
		<td width="100%" >
		<?=$li->display()?>
		</td>
	</tr>
	</table>
	<br />
	</td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<!--<input type="hidden" name="FolderID" value="<?=$FolderID?>" />-->
<input type="hidden" name="Folder" value="<?=htmlspecialchars($Folder)?>" />
<input type="hidden" name="page_from" value="viewfolder.php" />
<input type="hidden" name="mailbox" value="<?=$Folder?>" />
<input type="hidden" name="sort" value="<?=$sort?>" />
<input type="hidden" name="FolderName" value="<?=htmlspecialchars($foldertitle, ENT_QUOTES)?>" />
<input type="hidden" name="Folder_b" value="<?=base64_encode($Folder)?>" />
</form>
<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>