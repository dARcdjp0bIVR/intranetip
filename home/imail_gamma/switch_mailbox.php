<?php
// page modifing by: 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();
intranet_opendb();

$IMap = new imap_gamma();

if($SwitchTo)
{
	$_SESSION['SSV_EMAIL_LOGIN']    = $SwitchTo;
	$_SESSION['SSV_EMAIL_PASSWORD'] = $IMap->Get_MailBox_Password($SwitchTo);
	$_SESSION['SSV_LOGIN_EMAIL']    = $SwitchTo;
	
	unset($_SESSION['SSV_GAMMA_INIT_FOLDER']);
	unset($_SESSION['SSV_GAMMA_BATCH_REMOVAL_STATE']);
	unset($_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_send_right"]);
	unset($_SESSION['SSV_GAMMA_USE_CACHE']);
}



//##########################################################################################
//
//## Use Library
//$SharedMailBox = new shared_mailbox();
//
//## Get Data
//$MailBoxName = (isset($MailBoxName) && $MailBoxName != "") ? $MailBoxName : "";
//$MailBoxArr = explode('|=|',$MailBoxName);
//
//if ($MailBoxArr[0] != 'Delegated') {
//## Main
////if($MailBoxName != ""){
//	$_SESSION['SSV_EMAIL_LOGIN']    = $MailBoxName;
//	$_SESSION['SSV_EMAIL_PASSWORD'] = $SharedMailBox->Get_MailBox_Password($MailBoxName);
//	$_SESSION['SSV_LOGIN_EMAIL']    = $MailBoxName;
//} else {
//	$MailDelegation = new mail_delegation_class();
//	
//	$_SESSION['SSV_EMAIL_LOGIN'] = $MailBoxArr[1];
//	$_SESSION['SSV_EMAIL_PASSWORD'] = $MailDelegation->Get_MailBox_Password($MailBoxArr[1]);
//	$_SESSION['SSV_LOGIN_EMAIL'] = $MailBoxArr[1];
//}
//	$ReturnMsg = $Lang['ReturnMsg']['MailBoxSwitchSuccess'];
///*} else {
//	$ReturnMsg = $Lang['ReturnMsg']['MailBoxSwitchUnSuccess'];
//}*/
//
//##########################################################################################
intranet_closedb();

header("Location: ".$PATH_WRT_ROOT."home/imail_gamma/index.php");
exit;
?>
