<?php
// Editing by:
/*********************************** Change log ******************************************
 * 2020-05-18 (Sam) : Remove file path value stored in ImageFileID to avoid WAF blocking
 */
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

set_time_limit(0);
intranet_auth();
intranet_opendb();
auth_campusmail();

################################################################################################

## Use Library
$ldb = new libdb();
$lfs = new libfilesystem();
$lui = new interface_html();

## Initization
$MessageUID = '';

## Get Data
$MailAction = (isset($_POST['MailAction']) && $_POST['MailAction'] != "") ? $_POST['MailAction'] : "";
$DraftUID   = (isset($_POST['DraftUID']) && $_POST['DraftUID'] != "") ? $_POST['DraftUID'] : "";
$ActionUID  = (isset($_POST['ActionUID']) && $_POST['ActionUID'] != "") ? $_POST['ActionUID'] : "";
$MessageUID = ($MailAction == "Forward") ? $ActionUID : $DraftUID;

$ComposeTime = $_POST['ComposeTime'];
$ComposeDatetime = date("Y-m-d H:i:s",$ComposeTime);

$ImageInputNumber = mt_rand();

## Preparation
$ImgID = md5("cid".$ImageInputNumber."_".time());

//$x .= '/* '.$ImgID." */\n";

$FileNumber=0;
$personal_path = $PATH_WRT_ROOT."file/gamma_mail/u$UserID";
$plupload_tmp_path = $personal_path."/".$ComposeTime;

if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}
$personal_path .= "/related";
if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}
$personal_path .= "/".$ImgID;
if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}

if (!is_dir($plupload_tmp_path))
{
    $lfs->folder_new($plupload_tmp_path);
}

//$loc = $_FILES['userfile_image'.$ImageInputNumber];

//$FileName = stripslashes($loc['name']);
//$TempName = $loc['tmp_name'];
$FileName = rawurldecode(stripslashes($_REQUEST['upload_file_name']));
$TempName = $plupload_tmp_path."/".$FileName;

// remove some special characters
$FinalFileName = str_replace(array("#","%","&"),array("_","_","_"),$FileName);

$FullFilePath = "$personal_path/".$FinalFileName;

//$x .= '/* '.$TempName.' */'."\n";
//$x .= '/* '.$FullFilePath.' */'."\n";

## Main
if(is_file($TempName) && @exif_imagetype($TempName))
{
	$lfs->lfs_copy($TempName, $FullFilePath);
	$FileSize = (round(filesize($FullFilePath)/1024,2));
	//$ImgID = "cid".$ImageInputNumber."_".time();
//	$ImgID = md5("cid".$ImageInputNumber."_".time());
	$sql = 'Insert into MAIL_INLINE_IMAGE_MAP 
				(UserID, UploadTime, OriginalImageFileName, ContentID, ComposeTime) 
			Values 
				('.$UserID.',NOW(),\''.$ldb->Get_Safe_Sql_Query($FinalFileName).'\',\''.$ImgID.'\',\''.$ComposeDatetime.'\') ';
	
	$ldb->db_db_query($sql);
	$ImageFileID = $ldb->db_insert_id();
	$lfs->file_remove($TempName);
}

//$x = '';
if(file_exists($FullFilePath))
{
	$x .= 'var oEditor = FCKeditorAPI? FCKeditorAPI.GetInstance("MailBody") : CKEDITOR.instances["MailBody"];'."\n";
	$x .= 'oEditor.InsertHtml(\'<img src="/'.str_replace("'","\'",$FullFilePath).'" title="'.str_replace("'","\'",$FinalFileName).'" />\');'."\n";
	//$x .= 'var image_span = document.getElementById("ImageFile"+'.$ImageInputNumber.');'."\n";
	//$x .= 'image_span.innerHTML = \'\';'."\n";
	//$x .= 'image_span.style.display = "none";'."\n";
	$x .= 'var mail_image_list = document.getElementById("MailImageList");'."\n";
	$x .= 'var image_data = \'<input type="hidden" name="ImageFileSize[]" value="'.$FileSize.'" />\';'."\n";
// 	$x .= 'image_data += \'<input type="hidden" name="ImageFileID[]" value="/'.str_replace("'","\'",$FullFilePath).'" />\';'."\n";
// 	WAF workaround
	$x .= 'image_data += \'<input type="hidden" name="ImageFileID[]" value="'.$ImageFileID.'" />\';'."\n";
	$x .= 'mail_image_list.innerHTML += image_data;'."\n";
	$x .= 'Check_Total_Filesize(0);'."\n";

}
else
{
	//$x .= 'var image_span = document.getElementById("ImageFile"+'.$ImageInputNumber.');'."\n";
	//$x .= 'image_span.innerHTML = \'<p>'.htmlspecialchars($FileName." ".$Lang['Gamma']['UploadFail'],ENT_QUOTES).'</p>\';'."\n";
	$x .= 'alert(\''.htmlspecialchars($FileName." ".$Lang['Gamma']['UploadFail'],ENT_QUOTES).'\');'."\n";
}

echo $x;
################################################################################################
intranet_closedb();
?>