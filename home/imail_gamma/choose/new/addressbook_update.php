<?php
// editing by 
/*
 * 2014-03-12 (Carlos): Added checking on $AliasID - must be own internal alias group
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/role_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lrole = new role_manage();
$fcm = new form_class_manage();

$AliasID = IntegerSafe($AliasID);

if($AliasID != "") {
	$sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL WHERE AliasID='$AliasID' AND OwnerID='".$_SESSION['UserID']."'";
	$temp = $li->returnVector($sql);
	if($temp[0] == 0){
		No_Access_Right_Pop_Up();
	}
}

$caller = $AliasID==""?"compose":"addressbook";

$parent_list=array();
$group_parent_list=array();
$user_list=array();
$group_list=array();

$identity = $lrole->Get_Identity_Type($UserID);
$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

if($InternalRecipientID!=""){
	
	$ids = explode(",",$InternalRecipientID);
	
	for($i=0;$i<sizeof($ids);$i++){
		
		if(substr($ids[$i],0,1)=="U"){
			$user_list[] = substr($ids[$i],1);
		}
		if(substr($ids[$i],0,1)=="P"){
			$parent_list[] = substr($ids[$i],1);
		}
		if(substr($ids[$i],0,1)=="Q"){
			$group_parent[] = substr($ids[$i],1);
		}
		if(substr($ids[$i],0,1)=="G"){
			$group_list[] = substr($ids[$i],1);
		}
		if(substr($ids[$i],0,1)=="T"){		// teacher group
			$group_list[] = substr($ids[$i],1);
		}
		if(substr($ids[$i],0,1)=="S"){		// student group
			$group_list[] = substr($ids[$i],1);
		}
		if(substr($ids[$i],0,1)=="R"){		// parent group
			$group_list[] = substr($ids[$i],1);
		}
		if(substr($ids[$i],0,1)=="O"){		// intranet group
			$intranet_group_list[] = substr($ids[$i],1);
		}
	}
	
	if(sizeof($parent_list)>0){
		$str = implode(",",$parent_list);
		$sql="SELECT DISTINCT ParentID FROM INTRANET_PARENTRELATION WHERE StudentID IN($str) ";
		$temp = $li->returnVector($sql);
		if(sizeof($temp)>0)
			$user_list = array_merge($user_list,$temp);
	}
	
	if(sizeof($group_list)>0){
		if($HiddenCatID == -1)
		{
			if(in_array(1,$group_list))
			{
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent") || $identity == "Alumni")
				{
					$all_sql = "(SELECT DISTINCT all_user.UserID FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1 AND (all_user.IMapUserEmail IS NOT NULL AND all_user.IMapUserEmail<> '') ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
					$sql = $all_sql;
				}
			}
			if(in_array(2,$group_list))
			{
				if($identity == "Teaching")
				{
					$form_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') AND b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID AND c.AcademicYearID = $CurrentAcademicYearID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to form teacher
				}
				if($identity == "Student")
				{
					$form_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') AND b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID AND c.AcademicYearID = $CurrentAcademicYearID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					$form_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') AND b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID AND c.AcademicYearID = $CurrentAcademicYearID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if(in_array(3,$group_list))
			{
				## Class Teacher ##
				if($identity == "Teaching")
				{
					$class_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to class teacher
				}
				if($identity == "Student")
				{
					$class_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					$class_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE relation.ParentID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
			}
			if(in_array(4,$group_list))
			{
				## Subject Teacher ##
				if($identity == "Teaching")
				{					
					$subject_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to subject teacher
				}
				if($identity == "Student")
				{					
					$subject_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{					
					$subject_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if(in_array(5,$group_list))
			{
				## Subject Group Teacher ##
				if($identity == "Teaching")
				{
					$subject_group_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to subject group teacher
				}
				if($identity == "Student")
				{
					$subject_group_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{
					$subject_group_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = $UserID AND b.YearTermID = $CurrentTermID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}

			$temp = $li->returnVector($sql);
			if(sizeof($temp)>0)
				$user_list = array_merge($user_list,$temp);
		}
		
		if($HiddenCatID == 2)
		{
			if(in_array(1,$group_list))
			{
				## All Student ##
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
				{
					$all_sql = "(SELECT DISTINCT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName)";
					$sql = $all_sql;
				}
			}
			if(in_array(2,$group_list))
			{
				## My Form Student ##
				if($identity == "Teaching")
				{
					$form_sql = "(SELECT DISTINCT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID AND c.AcademicYearID = $CurrentAcademicYearID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to form student
				}
				if($identity == "Student")
				{
					$form_sql = "(SELECT DISTINCT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID AND c.AcademicYearID = $CurrentAcademicYearID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					$form_sql = "(SELECT DISTINCT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID AND c.AcademicYearID = $CurrentAcademicYearID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if(in_array(3,$group_list))
			{
				## My Class Student ##
				if($identity == "Teaching")
				{
					$class_sql = "(SELECT DISTINCT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to class student
				}
				if($identity == "Student")
				{
					$class_sql = "(SELECT DISTINCT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					$class_sql = "(SELECT DISTINCT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
			}
			if(in_array(4,$group_list))
			{
				## My Subject Student ##
				if($identity == "Teaching")
				{
					$subject_sql = "(SELECT DISTINCT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to Subject student
				}
				if($identity == "Student")
				{
					$subject_sql = "(SELECT DISTINCT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{
					$subject_sql = "(SELECT DISTINCT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if(in_array(5,$group_list))
			{
				## My Subject Group Student ##		
				if($identity == "Teaching")
				{	
					$subject_group_sql = "(SELECT DISTINCT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to subject group student
				}
				if($identity == "Student")
				{	
					$subject_group_sql = "(SELECT DISTINCT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{	
					$subject_group_sql = "(SELECT DISTINCT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '') AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = $UserID AND b.YearTermID = $CurrentTermID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}
			
			$temp = $li->returnVector($sql);
			if(sizeof($temp)>0)
				$user_list = array_merge($user_list,$temp);
		}
		if($HiddenCatID == 3)
		{
			if(in_array(1,$group_list))
			{
				## All Parents ##
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
				{					
					$all_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a LEFT OUTER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) LEFT OUTER JOIN INTRANET_USER AS c ON (b.StudentID = c.UserID) WHERE a.RecordType = 3 AND a.RecordStatus = 1 AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') ORDER BY IFNULL(c.ClassName,''), IFNULL(c.ClassNumber,0), c.EnglishName)";
					$sql = $all_sql;
				}
			}
			if(in_array(2,$group_list))
			{
				## My Form Parents ##
				if($identity == "Teaching")
				{
					$form_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND a.AcademicYearID = $CurrentAcademicYearID)) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to form parent
				}
				if($identity == "Student")
				{
					$form_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND a.AcademicYearID = $CurrentAcademicYearID)) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					$form_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND a.AcademicYearID = $CurrentAcademicYearID)) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if(in_array(3,$group_list))
			{
				## My Class Parents ##
				if($identity == "Teaching")
				{
					$class_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID)) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to class parent
				}
				if($identity == "Student")
				{
					$class_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID)) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					$class_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID)) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
			}
			if(in_array(4,$group_list))
			{
				## My Subject Parents ##
				if($identity == "Teaching")
				{
					$subject_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID)) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to subject parent
				}
				if($identity == "Student")
				{
					$subject_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID)) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{
					$subject_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID)) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if(in_array(5,$group_list))
			{
				## My Subject Group Parents ##
				if($identity == "Teaching")
				{
					$subject_group_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID)) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to subject group parent
				}
				if($identity == "Student")
				{
					$subject_group_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID)) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{
					$subject_group_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = $UserID AND b.YearTermID = $CurrentTermID)) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}
			$temp = $li->returnVector($sql);
			if(sizeof($temp)>0)
				$user_list = array_merge($user_list,$temp);
		}
	}
	
	if(sizeof($group_parent_list)>0){
		$str = implode(",",$group_parent_list);
		$sql ="SELECT DISTINCT UserID FROM INTRANET_USERGROUP WHERE GroupID IN($str)";
		$temp = $li->returnVector($sql);
		if(sizeof($temp)>0){
			$str = implode(",",$temp);
			$sql="SELECT DISTINCT ParentID FROM INTRANET_PARENTRELATION WHERE StudentID IN($str) ";
			echo $sql;die;
			$temp = $li->returnVector($sql);
			if(sizeof($temp)>0)
				$user_list = array_merge($user_list,$temp);
				
		}
	}
	
	if(sizeof($user_list)>0){
		$user_list = array_values(array_unique($user_list));
		$delim="";
		for($i=0;$i<sizeof($user_list);$i++){
			$values .= $delim."($AliasID, ".$user_list[$i].",'U',now(),now())";
			$delim=",";
		}
		$sql = "INSERT IGNORE INTO INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY
        (AliasID, TargetID, RecordType, DateInput, DateModified)
        VALUES $values ";	
        $li->db_db_query($sql);
   }
   
	if(sizeof($intranet_group_list)>0){
		$intranet_group_list = array_values(array_unique($intranet_group_list));
		$delim="";
		for($i=0;$i<sizeof($intranet_group_list);$i++){
			$values .= $delim."($AliasID, ".$intranet_group_list[$i].",'G',now(),now())";
			$delim=",";
		}
		$sql = "INSERT IGNORE INTO INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY
        (AliasID, TargetID, RecordType, DateInput, DateModified)
        VALUES $values ";	
        $li->db_db_query($sql);
	}
		
}


intranet_closedb();
//header("Location: index.php?AliasID=".$AliasID);
?>
<script language='javascript'>
opener.location.reload();
location.href='index.php?AliasID=<?=$AliasID?>';
</script>