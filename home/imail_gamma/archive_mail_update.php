<?php
// editing by 
/*************** Change Log ***************
 * Created on 2011-04-18
 * 2015-04-15 (Carlos): Modified to move mails by UID csv, that way is much faster than move one by one.
 * 2013-02-20 (Carlos): Modified to use UID to move mails, and sleep some time to avoid use up CPU power
 * 2011-11-18 (Carlos): Fix time out problem by reconnect mailbox after certain interval
 * 						modified to expunge mails after moved certain amount of mails
 ******************************************/
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
//include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$IMap = new imap_gamma();

//$IMap->Start_Timer();

$Folder = trim(stripslashes(urldecode($_REQUEST['Folder'])));
$Folder = (isset($Folder) && $Folder != "") ? $Folder : $IMap->InboxFolder;

$numOfYears = sizeof($archive);
if($numOfYears==0) header("./index.php");

$userFolders = array();
for($i=0;$i<$numOfYears;$i++){
	// 0=> Year as DisplayFolderName, 1=>ImapFolderName
	$userFolders[$archive[$i]] = array($archive[$i],"");
}

$imap_folders = $IMap->Get_Folder_Structure(true,false);
// array[i][0=>Display Folder Name, 1=>IMAP Folder Name]
for($i=0;$i<sizeof($imap_folders);$i++){
	list($display_folder_name,$imap_folder_name) = $imap_folders[$i];
	if(isset($userFolders[$display_folder_name])) $userFolders[$display_folder_name][1] = $imap_folder_name;
}

$FolderPrefix = $IMap->FolderPrefix;

$Result = array();
if(sizeof($userFolders)>0){
	// Find all the mails in Folder
	$imap_reopen_success = imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));
	if(!$imap_reopen_success){
		// Delay 1 second and connect again
		sleep(1);
		imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));
	}
	$IMap_obj = imap_check($IMap->inbox);
	$mail_headers = imap_fetch_overview($IMap->inbox,"1:{$IMap_obj->Nmsgs}",0);
	$mail_headers_size = sizeof($mail_headers);
	/* sample header overview: 
	 * [0] => stdClass Object
        (
            [subject] => =?UTF-8?B?dGVzdGluZyBmcm9tIGNhcmxvc190Mg==?=
            [from] => carlos_t2@broadlearning.com
            [to] => =?UTF-8?B?Ik1yLiBjYXJsb3NfdDFfZW5nIg==?= 
            [date] => Thu, 08 Jul 2010 09:41:07 +0800
            [message_id] => <201007080141.o681f7vu022503@dev3.broadlearning.com>
            [size] => 2425
            [uid] => 1
            [msgno] => 1
            [recent] => 0
            [flagged] => 0
            [answered] => 0
            [deleted] => 0
            [seen] => 1
            [draft] => 0
        )
	 */
	$timeout_period = 300; // 300 second = 5 minute
	$last_time = time();
	
	$mails_info = array();
	
	session_write_close(); // prevent session locking due to long processing time here
	//$IMap->Start_Timer();
	if($mail_headers_size>0)
	{
		foreach($mail_headers as $key=>$overview){
			$date_year = date("Y",strtotime($IMap->formatDatetimeString($overview->date)));
			//$mails_info[$date_year][] = $overview->msgno;
			$mails_info[$date_year][] = $overview->uid;
		}
	}
	//$t_end1 = $IMap->Stop_Timer();
	//debug_r("Go through emails elapsed ".$t_end1." seconds.");
	
	//$IMap->Start_Timer();
	
	foreach($userFolders as $key => $arr){
		list($DisplayFolderName, $ImapFolderName) = $arr;
		if($ImapFolderName=="")
		{ // Create new folder
			$create_folder_success = $IMap->createMailFolder($FolderPrefix, $DisplayFolderName);
			if($create_folder_success)
			{
				$userFolders[$key][1] = $FolderPrefix.$IMap->folderDelimiter.$DisplayFolderName;
				$ImapFolderName = $userFolders[$key][1];
			}
		}
		$NumOfMailsThisYear = sizeof($mails_info[$DisplayFolderName]);
		if($ImapFolderName!="" && $NumOfMailsThisYear>0){
			// Move mails 
			/*
			$movedCount = 0;
			for($j=0;$j<$NumOfMailsThisYear;$j++){
				// reconnect after expire period
				$current_time = time();
				if(($current_time - $last_time) > $timeout_period){
					$IMap->openInbox($IMap->CurUserAddress, $IMap->CurUserPassword,$Folder);
					$IMap->Go_To_Folder($Folder);
					$last_time = $current_time;
				}
				
				$uid = $mails_info[$DisplayFolderName][$j];
				$Result['Move'.$uid] = imap_mail_move($IMap->inbox,$uid,$IMap->encodeFolderName($ImapFolderName),CP_UID);
				$movedCount++;
				
				if($movedCount >= 50){
					imap_expunge($IMap->inbox);
					$movedCount = 0;
					usleep(200000);
				}
			}
			*/
			
			$uid_csv = implode(",",$mails_info[$DisplayFolderName]);
			$Result['Move_'.$DisplayFolderName] = imap_mail_move($IMap->inbox,$uid_csv,$IMap->encodeFolderName($ImapFolderName),CP_UID);

			
		}else{
			$Result[$DisplayFolderName] = false;
		}
		$Result['Expunge_'.$key] = imap_expunge($IMap->inbox);
		
		$current_time = time();
		if(($current_time - $last_time) > $timeout_period){
			$IMap->openInbox($IMap->CurUserAddress, $IMap->CurUserPassword,$Folder);
			$IMap->Go_To_Folder($Folder);
			$last_time = $current_time;
		}
		
	}
	$Result['Expunge'] = imap_expunge($IMap->inbox);
	
	//$t_end2  = $IMap->Stop_Timer();
	//debug_r("Move emails elapsed ".$t_end2." seconds.");
}else{
	$Result[] = false;
}

// 10392 mails, takes 544.13274 seconds ( 9.067 minutes ) to move
//$time_required = $IMap->Stop_Timer();
//debug_pr($time_required);
//intranet_closedb();
//exit;

if(!in_array(false,$Result)){
	$_SESSION['intranet_iMail_archived'] = 1;
	$msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}else
	$msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];

intranet_closedb();
header("Location: viewfolder.php?Folder=".urlencode($IMap->InboxFolder)."&msg=".rawurlencode($msg));
?>