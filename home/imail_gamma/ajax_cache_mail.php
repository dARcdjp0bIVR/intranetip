<?php
// using by 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$IMap = new imap_gamma();

if($IMap->ConnectionStatus != true)
{ 
	intranet_closedb();
	exit;
}

$Action = $_REQUEST['Action'];
//$Folder = $_REQUEST['Folder'];
$Folder = array();
if(is_array($_REQUEST['Folder']) && count($_REQUEST['Folder'])>0){
	foreach($_REQUEST['Folder'] as $key=>$val){
		$Folder[] = trim(urldecode($val));
	}
}else{
	$Folder[] = trim(urldecode($_REQUEST['Folder']));
}

switch ($Action)
{
	case "FullSyncMails":
		include_once($PATH_WRT_ROOT."includes/libdb.php");
		$li = new libdb();
		
		$FolderList = $IMap->getAllFolderList();
		//$SkipFolders = array();
		//$SkipFolders[] = $IMap->reportSpamFolder;
		//$SkipFolders[] = $IMap->reportNonSpamFolder;
		//$SkipFolders[] = $IMap->HiddenFolder;
		$Folder = $FolderList;
		//for($i=0;$i<sizeof($FolderList);$i++){
		//	if(in_array($FolderList[$i],$SkipFolders)) continue;
		//	$Folder[] = $FolderList[$i];
		//}
		
		$sql = "DELETE FROM MAIL_CACHED_MAILS WHERE UserEmail = '".$IMap->CurUserAddress."' ";
		$li->db_db_query($sql);
		// continue CheckNewMails will do full sync 
	case "CheckNewMails":
		// quickly checks the largest mail UID of a folder/all folders against that in cached db
		$IMap->Start_Timer();
		//$Folder = $_REQUEST['Folder'];
		$Result = $IMap->Check_New_Mails_For_Cached_Mails($Folder);
		//debug_pr($Folder);
		$MailsToAdd = array();
		$SyncResult = true;
		if(sizeof($Result)>0){
			foreach($Result as $folder => $uid_arr){
				$server_max_uid = $uid_arr['ServerMaxUID'];
				$client_max_uid = !isset($uid_arr['ClientMaxUID'])?0:$uid_arr['ClientMaxUID'];
				if($server_max_uid > $client_max_uid){
					// sync new mails to db
					$MailsToAdd[$folder] = array('StartUID'=>$client_max_uid,'EndUID'=>$server_max_uid);
				}
			}
		}
		//debug_pr($MailsToAdd);
		// this process may takes long time ...
		if(sizeof($MailsToAdd)>0){
			$SyncResult = $IMap->Add_Cached_Mails($MailsToAdd);
		}
		
		if($SyncResult)
			echo "1";
		else
			echo "0";
		echo "<br>";
		echo $IMap->Stop_Timer();
	break;
	
	case "SyncNewMails":
		$IMap->Start_Timer();
		$server_max_uid = $_REQUEST['ServerMaxUID'];
		$client_max_uid = $_REQUEST['ClientMaxUID'];
		
		$MailsToAdd = array();
		$MailsToAdd[$Folder[0]] = array('StartUID'=>$client_max_uid,'EndUID'=>$server_max_uid);
		$SyncResult = $IMap->Add_Cached_Mails($MailsToAdd);
		if($SyncResult)
			echo "1";
		else
			echo "0";
		echo "<br>";
		echo $IMap->Stop_Timer();
	break;
	case "RemoveOldMails":
		$IMap->Start_Timer();
		//$Folder = $_REQUEST['Folder'];
		$SyncResult = $IMap->Remove_Not_Exist_Cached_Mails($Folder);
		
		if($SyncResult)
			echo "1";
		else
			echo "0";
		echo "<br>";
		echo $IMap->Stop_Timer();
	break;
}

intranet_closedb();
die;
?>