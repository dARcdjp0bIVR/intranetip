<?php
# page modifing by : 
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

## Use Library
$IMap = new imap_gamma();


## Get Date 
$Folder = (isset($Folder) && $Folder != "") ? stripslashes(urldecode($Folder)) : "";


## Initialization
$ClearFolder = '';


## Preparation
if($Folder == $IMap->DefaultFolderList['TrashFolder']){
	# Trash Folder
	$ClearFolder = $IMap->DefaultFolderList['TrashFolder'];
	
} else if($Folder == $IMap->DefaultFolderList['SpamFolder']){
	# Spam Folder
	$ClearFolder = $IMap->DefaultFolderList['SpamFolder'];
}

if ($Folder != "") {
	$ReturnTag = $IMap->getMailDisplayFolder($Folder);
	$ReturnFolder = $Folder;
}
else {
	$ReturnTag = $IMap->getMailDisplayFolder($SYS_CONFIG['Mail']['SystemMailBox'][0]);
	$ReturnFolder = $SYS_CONFIG['Mail']['SystemMailBox'][0];
}

## Main
if($ClearFolder != "" ){
	$Result = $IMap->Clear_Folder($ClearFolder);
}


if ($Result) {
	$Msg = ($ClearFolder == $IMap->DefaultFolderList['TrashFolder'])?"5":"7";
}
else {
	$Msg = ($ClearFolder == $IMap->DefaultFolderList['TrashFolder'])?"6":"8";
}

intranet_closedb();
header("Location: viewfolder.php?Folder=".urlencode($ReturnFolder)."&msg=".$Msg);
exit;
?>