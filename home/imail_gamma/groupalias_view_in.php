<?php
// editing by 
/*
 * 2014-06-18 (Carlos): Bind contacts links to do keyword search
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPageArr['iMail'] = 1;
$CurrentPage 	= "PageAddressBook_InternalReceipientGroup";
$lwebmail	= new libwebmail();
$IMap	= new imap_gamma();
$linterface = new interface_html();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$ldb = new libdb();

$sql = "SELECT AliasName,Remark,OwnerID FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL WHERE AliasID = '$AliasID'";
$temp = $ldb->returnArray($sql,3);
list($aliasName,$aliasRemark,$OwnerID) = $temp[0];
if ($OwnerID != $UserID)
{
    header("Location: groupalias.php");
    exit();
}

if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$keyword = trim($keyword);

$aliastype = 0;
$username_field = getNameFieldWithClassNumberByLang("b."); //($chi? "ChineseName": "EnglishName");

##########################################################################################
## INSERT INTO TEMP TABLE
## Since deleted class is still keep record in INTRANET_GROUP
##########################################################################################
$create_temp_table2 = "CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_GROUPALIAS_VIEW_IN
							(
								EntryID int(11),
								AliasID int(11),
								TargetID int(11),
								RecordType char(2),
								DateModified datetime
							)
						";
	$temp_sql = "
		SELECT
			a.EntryID,
			a.AliasID,
			a.TargetID,
			a.DateModified,
			a.RecordType,
			c.RecordType,
			d.ClassID,
			b.IMapUserEmail 
		FROM 
			INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY AS a
			LEFT JOIN INTRANET_USER AS b ON (a.TargetID = b.UserID) 
			LEFT JOIN INTRANET_GROUP AS c ON (a.TargetID = c.GroupID)
			left join INTRANET_CLASS as d on (a.TargetID = d.GroupID)
		WHERE
			a.AliasID = '$AliasID' 
	";
	
	$ldb->db_db_query($create_temp_table2);
	$ldb->db_db_query($temp_sql);
	$temp = $ldb->returnArray($temp_sql,8);
	//echo "<PRE>";print_r($temp);echo "</PRE>";
	for($i=0; $i<sizeof($temp); $i++)
	{
		list($temp_entryid, $temp_aliasid, $temp_targetid, $temp_datemodified, $temp_group_recordtype, $temp_recordtype, $temp_classid, $temp_imap_email) = $temp[$i];
		//if(($temp_group_recordtype=='U' && trim($temp_imap_email)!='') or ($temp_group_recordtype=='G' and $temp_recordtype==1) or ($temp_group_recordtype=='G' and $temp_recordtype==3 and $temp_classid) )
		if(($temp_group_recordtype=='U' && trim($temp_imap_email)!='') or ($temp_group_recordtype=='G') )
		{
			$insert_sql = "
				insert into TEMP_GROUPALIAS_VIEW_IN 
				(EntryID, AliasID, TargetID, RecordType, DateModified)
				values 
				('$temp_entryid', '$temp_aliasid', '$temp_targetid', '$temp_group_recordtype', '$temp_datemodified')
			";
			//echo "<PRE>".$insert_sql." ;$temp_imap_email</PRE>";
			$ldb->db_db_query($insert_sql);
		}
	}
##########################################################################################
##########################################################################################

	

if(!$sys_custom['iMail_ShowPhoto'])		 # no need to show photo
{
	$li = new libdbtable2007($field, $order, $pageNo);
	/*
	$sql  = "SELECT
					1,
	               IF(a.RecordType='G',c.Title,$username_field) as EntryName,
	               IF(a.RecordType='G','Group','User'),
                       a.DateModified,
			CONCAT('<input type=\'checkbox\' name=\'EntryID[]\' value=\'', a.EntryID ,'\'>')
	          FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY AS a
	                 LEFT OUTER JOIN INTRANET_USER AS b ON a.TargetID = b.UserID
	                 LEFT OUTER JOIN INTRANET_GROUP AS c ON a.TargetID = c.GroupID
	          WHERE
	               a.AliasID = $AliasID AND
	               (
	                 (a.RecordType = 'G' AND c.Title like '%$keyword%')
	                 OR (a.RecordType = 'U' AND $username_field like '%$keyword%')
	               )
	          ";
	*/
	$escape_keyword = $li->Get_Safe_Sql_Like_Query(htmlspecialchars(stripslashes($keyword),ENT_QUOTES));
	$sql  = "SELECT
					1,
	               IF(a.RecordType='G',c.Title,$username_field) as EntryName,
	               IF(a.RecordType='G','Group','User'),
                       a.DateModified,
			CONCAT('<input type=\'checkbox\' name=\'EntryID[]\' value=\'', a.EntryID ,'\'>')
	          FROM TEMP_GROUPALIAS_VIEW_IN  AS a
	                 LEFT OUTER JOIN INTRANET_USER AS b ON a.TargetID = b.UserID
	                 LEFT OUTER JOIN INTRANET_GROUP AS c ON a.TargetID = c.GroupID
	          WHERE
	               a.AliasID = '$AliasID' AND
	               (
	                 (a.RecordType = 'G' AND c.Title like '%".$escape_keyword."%')
	                 OR (a.RecordType = 'U' AND (b.EnglishName like '%".$escape_keyword."%' OR b.ChineseName like '%".$escape_keyword."%'))
	               )
	          ";
}
else{ # need to show photo

	$sql = "SELECT b.UserID,b.WebSAMSRegNo,b.PhotoLink  FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY AS a ,
			INTRANET_USER AS b WHERE a.TargetID=b.UserID AND a.RecordType='U' AND a.AliasID='$AliasID'
			";
	$temp = $ldb->returnArray($sql,3);

	$link_ary=array();
	for($i=0;$i<sizeof($temp);$i++){
		list($sid,$regno,$p_link)=$temp[$i];
		$photo_url="";
		if($regno!=""){ # use official photo
			$tmp_regno = trim(str_replace("#", "", $regno));
			$photolink = "/file/official_photo/".$tmp_regno.".jpg";
			if(file_exists($intranet_root.$photolink))
					$photo_url = $photolink;
		}
		if($photo_url=="" && $p_link!=""){ # use photo uploaded by student
			$photo_url = $p_link;
		}
		if($photo_url!="" && file_exists($intranet_root.$photo_url))
			$link_ary[$sid]=$photo_url;
		else $link_ary[$sid]="";

	}

	$sql_insert_temp = "INSERT INTO TEMP_STUDENT_PHOTO_LINK (StudentID,Link) VALUES";
	if(sizeof($link_ary)>0){
		foreach($link_ary AS $student_id => $link){
			$sql_insert_temp.="($student_id,'$link'),";
		}
		$sql_insert_temp = substr($sql_insert_temp,0,strlen($sql_insert_temp)-1);
	}

	$create_temp_table = "CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_STUDENT_PHOTO_LINK(
							StudentID INT(8),Link VARCHAR(255) )
						";

	//<table border=\"0\" cellpadding=\"5\" cellspacing=\"0\" class=\"attendancestudentphoto\"><tr><td>
        //</td></tr></table>
	
	
	/*
	$create_temp_table2 = "CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_GROUPALIAS_VIEW_IN
							(
								EntryID int(11),
								AliasID int(11),
								TargetID int(11),
								RecordType char(2),
								DateModified datetime
							)
						";
	$temp_sql = "
		SELECT
			a.EntryID,
			a.AliasID,
			a.TargetID,
			a.DateModified,
			a.RecordType,
			c.RecordType,
			d.ClassID
		FROM 
			INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY AS a
			LEFT JOIN INTRANET_USER AS b ON (a.TargetID = b.UserID)
			LEFT JOIN INTRANET_GROUP AS c ON (a.TargetID = c.GroupID)
			left join INTRANET_CLASS as d on (a.TargetID = d.GroupID)
		WHERE
			a.AliasID = $AliasID
	";
	
	$ldb->db_db_query($create_temp_table2);
	$ldb->db_db_query($temp_sql);
	$temp = $ldb->returnArray($temp_sql,7);
	
	for($i=0; $i<sizeof($temp); $i++)
	{
		list($temp_entryid, $temp_aliasid, $temp_targetid, $temp_datemodified, $temp_group_recordtype, $temp_recordtype, $temp_classid) = $temp[$i];
		if($temp_group_recordtype=='U' or ($temp_group_recordtype=='G' and $temp_recordtype==1) or ($temp_group_recordtype=='G' and $temp_recordtype==3 and $temp_classid) )
		{
			$insert_sql = "
				insert into TEMP_GROUPALIAS_VIEW_IN 
				(EntryID, AliasID, TargetID, RecordType, DateModified)
				values 
				($temp_entryid, $temp_aliasid, $temp_targetid, '$temp_group_recordtype', '$temp_datemodified')
			";
			
			$ldb->db_db_query($insert_sql);
		}
	}
	*/
	$escape_keyword = $li->Get_Safe_Sql_Like_Query(htmlspecialchars(stripslashes($keyword),ENT_QUOTES));
	$sql = "
		SELECT
			a.DateModified,
			IF(a.RecordType='G',c.Title,IF((d.Link IS NULL OR d.Link=''),$username_field, CONCAT('<div onMouseOut=\'hideTip()\' onMouseOver=\"showTip(\'ToolTip\',makeTip(\'<table border=0 cellpadding=5 cellspacing=0 class=attendancestudentphoto><tr><td><img src=',d.Link,'></td></tr></table>\'))\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_student_photo.gif\" width=\"20\" height=\"20\" align=\"absmiddle\">',$username_field,'</div>')) ) as EntryName,
			IF(a.RecordType='G','". $i_CampusMail_New_AddressBook_ByGroup ."','". $i_CampusMail_New_AddressBook_ByUser ."'),
			a.DateModified,
			CONCAT('<input type=\'checkbox\' name=\'EntryID[]\' value=\'', a.EntryID ,'\'>')
		FROM 
			TEMP_GROUPALIAS_VIEW_IN AS a
			LEFT OUTER JOIN INTRANET_USER AS b ON a.TargetID = b.UserID 
			LEFT OUTER JOIN INTRANET_GROUP AS c ON a.TargetID = c.GroupID
			LEFT OUTER JOIN TEMP_STUDENT_PHOTO_LINK AS d ON a.TargetID=d.StudentID
		WHERE
			a.AliasID = '$AliasID' AND
			(
			(a.RecordType = 'G' AND c.Title like '%".$escape_keyword."%')
			OR (a.RecordType = 'U' AND (b.EnglishName like '%".$escape_keyword."%' OR b.ChineseName like '%".$escape_keyword."%'))
			) 
	";
}

# TABLE INFO
$li->field_array = array("EntryName","a.RecordType","a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->IsColOff = "imail_addressbook_list";
$li->title = "";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class='tabletoplink' width='1'>#</td>\n";
$li->column_list .= "<td class='tabletoplink' width='20'>&nbsp;</td>\n";
$li->column_list .= "<td class='tabletoplink' width='45%'>".$li->column($pos++, "$i_CampusMail_New_AddressBook_Name")."</td>\n";
$li->column_list .= "<td class='tabletoplink' width='34%'>".$li->column($pos++, "$i_CampusMail_New_AddressBook_Type")."</td>\n";
$li->column_list .= "<td class='tabletoplink' width='20%'>".$li->column($pos++, $i_LastModified)."</td>\n";
$li->column_list .= "<td class='tabletoplink' width='1%'>".$li->check("EntryID[]")."</td>\n";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);

### Button / Tag
$AddBtn 	= "<a href=\"javascript:newWindow('groupalias_in/?AliasID=$AliasID',1)\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_select . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'EntryID[]','groupalias_in_remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$searchTag 	.= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".htmlspecialchars(stripslashes($keyword),ENT_QUOTES)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
$searchTag 	.= "</tr></table>";
/*
$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_address_ingroup.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='imailpagetitle'>{$i_CampusMail_Internal_Recipient_Group}</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleImage1.$TitleTitle1."</td><td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>";
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($TitleTitle, "", 0);
*/
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($i_CampusMail_External_Recipient_Group, $PATH_WRT_ROOT."home/imail_gamma/groupalias.php?aliastype=1&TabID=3", 0);
$TAGS_OBJ[] = array($i_CampusMail_Internal_Recipient_Group, $PATH_WRT_ROOT."home/imail_gamma/groupalias.php?aliastype=0&TabID=1", 1);

//$body_tags = "onMouseMove=\"overhere();\"";
$linterface = new interface_html();
$linterface->LAYOUT_START();

echo "<script language=\"Javascript\" src='/templates/tooltip.js'></script>";


?>
<style type="text/css">
     #ToolTip{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>

<script language="javascript1.2">
     isToolTip = true;
</script>

<script type="text/javascript" language="JavaScript">
function getFormatDate(dateObj)
{
	var date_year = dateObj.getFullYear();
	var date_month = dateObj.getMonth()+1;
	var date_day = dateObj.getDate();
	date_month = date_month < 10? '0'+date_month : ''+date_month; 
	date_day = date_day < 10 ? '0'+date_day : ''+date_day; 
	var date_string = date_year + '-' + date_month + '-' + date_day;
	
	return date_string;
}

function viewRelatedMails(email)
{
	var dateObj = new Date();
	var end_date = getFormatDate(dateObj);
	dateObj.setFullYear(dateObj.getFullYear()-1);
	var start_date = getFormatDate(dateObj);
	
	var tmp_form_html = '<form id="tmp_form" name="tmp_form" method="get" action="search_result.php">';
	tmp_form_html += '<input type="hidden" name="keyword" value="'+email+'" />';
	tmp_form_html += '<input type="hidden" name="isAdvanceSearch" value="0" />';
	tmp_form_html += '<input type="hidden" name="FromSearch" value="1" />';
	tmp_form_html += '<input type="hidden" name="FromIntAddressGroup" value="1" />';
	tmp_form_html += '<input type="hidden" name="DateRangeMethod" value="1year" />';
	tmp_form_html += '<input type="hidden" name="FromDate" value="'+start_date+'" />';
	tmp_form_html += '<input type="hidden" name="ToDate" value="'+end_date+'" />';
	tmp_form_html += '</form>';
	
	if($('#tmp_form').length > 0)
	{
		$('#tmp_form').remove();
	}
	$('form[name="form1"]').after(tmp_form_html);
	$('#tmp_form').submit();
	if($('#tmp_form').length > 0)
	{
		$('#tmp_form').remove();
	}
}

function initAllLinks()
{
	var row1 = $('tr.tablerow1');
	var row2 = $('tr.tablerow2');
	
	for(var i=0;i<row1.length;i++){
		var tr = $(row1.get(i));
		var td2 = tr.find('td:eq(2)');
		var td2_text = $.trim(td2.text());
		td2.html('<a href="javascript:void(0);" onclick="viewRelatedMails(\''+td2_text+'\')" class="tablelink" title="<?=$Lang['Gamma']['ViewRelatedEmails']?>">'+td2_text+'</a>');
	}
	
	for(var i=0;i<row2.length;i++){
		var tr = $(row2.get(i));
		var td2 = tr.find('td:eq(2)');
		var td2_text = $.trim(td2.text());
		td2.html('<a href="javascript:void(0);" onclick="viewRelatedMails(\''+td2_text+'\')" class="tablelink" title="<?=$Lang['Gamma']['ViewRelatedEmails']?>">'+td2_text+'</a>');
	}
}

$(document).ready(function(){
	initAllLinks();
});
</script>

<div id="ToolTip"></div>

<br />
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" onMouseMove='overhere();'>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$Lang['Gamma']['GroupName']?> </span></td>
					<td valign="top" class="iMailrecipientgroup"><?=$aliasName?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext"><?=$Lang['Gamma']['GroupRemark']?> </span></td>
					<td valign="top" class="tabletext"><?=nl2br($aliasRemark)?> </td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		</table>
        </td>
</tr>
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
                                        	<table border="0" cellspacing="0" cellpadding="2">
                                            	<tr>
                                              		<td><p><?=$AddBtn?></p></td>
                                            	</tr>
                                          	</table>
					</td>
                                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
				</tr>

				<tr class="table-action-bar">
					<td><?=$searchTag?></td>
					<td align="right" valign="bottom">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
							<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
								<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td nowrap><?=$delBtn?></td>
								</tr>
        							</table>
							</td>
        						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
						</tr>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td colspan="2">
						<?php
                                                	if($sys_custom['iMail_ShowPhoto'])
                                                        {
								$ldb->db_db_query($create_temp_table);
								$ldb->db_db_query($sql_insert_temp);
							}
							echo $li->display();
						?>
					</td>
				</tr>
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
</table>
<br />

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="AliasID" value="<?=$AliasID?>" />
<input type="hidden" name="TabID" value="<?=$TabID?>" />
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>