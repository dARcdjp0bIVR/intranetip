<?php
// using 
/*
 * 2014-05-15 (Carlos): Fix fail to write tnef attachments into zip problem.
 */
ini_set("memory_limit","300M");
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

function rename_file($filename){
	$new_filename = $filename;
	if( preg_match('/^.+\((\d+)\)\.[^\.]+$/', $filename, $matches) ){
		if(count($matches)>1){
			$num = intval($matches[count($matches)-1]);
			$new_filename = str_replace("(".$num.").","(".($num+1).").",$filename);
		}
	}else{
		$dot_pos = strrpos($filename,".",0);
		$suffix = substr($filename,$dot_pos);
		$new_filename = substr_replace($filename,"(1)".$suffix,$dot_pos);
	}
	return $new_filename;
}

$IMap = new imap_gamma();
//$IMapCache = new imap_cache_agent();
$IMapCache = $IMap->getImapCacheAgent();
$lfile = new libfilesystem();

$MailContent = $IMap->getMailRecord($Folder, $uid);

$Attachment = $MailContent[0]["attach_parts"];

if (!(is_null($Attachment) || $Attachment == "" || empty($Attachment))) {
	// clean tmp attachments and all_attachments.zip 
	$personal_path = $intranet_root."/file/gamma_mail/u".$_SESSION['UserID'];
	//$dir_path = $IMap->user_file_path."/attachments";
	$dir_path = $personal_path."/attachments";
	$zip_file = "all_attachment.zip";
	$zip_file_full_path = $personal_path.'/'.$zip_file;
	
	if(file_exists($dir_path)){
		$lfile->folder_remove_recursive($dir_path);
	}
	if(file_exists($zip_file_full_path)){
		$lfile->file_remove($zip_file_full_path);
	}
	$lfile->folder_new($dir_path);
	$lfile->chmod_R($dir_path, 0777);
	
	// short list	
	for ($i=0; $i< sizeof($Attachment); $i++) 
	{	
		$FileName = $Attachment[$i]["FileName"];
		// file system cannot write UTF-8 file name, need to first convert to BIG5 if is UTF-8
		$filename_charset = strtoupper(mb_detect_encoding($FileName, 'UTF-8, BIG5, GB2312, GBK, ASCII, US-ASCII'));
		if(in_array($filename_charset,array('UTF8','UTF-8'))) {
			$FileName = mb_convert_encoding($FileName, "BIG5",$filename_charset);
		}
		$filepath = $dir_path."/".$FileName;
		
		while(file_exists($filepath)){
			$FileName = rename_file($FileName);
			$filepath = $dir_path."/".$FileName;
		}
		
		if(!file_exists($filepath))
		{
			if($Attachment[$i]['IsTNEF']){
				$target_filepath = $personal_path."/attachments/".$FileName;
				$lfile->file_copy($Attachment[$i]['FilePath'],$target_filepath);
			}else{
				$thisAttachment= $IMapCache->Get_Attachment($Attachment[$i]['PartID'],$uid,$Folder);
				$target_filepath = $personal_path."/attachments/".$FileName;
				$lfile->file_write($thisAttachment["Content"],$target_filepath);
			}
			//$lfile->chmod_R($target_filepath, 0777);
		}
	}
	$lfile->file_zip("attachments",$zip_file,$personal_path);
	//chdir($personal_path);
	//$zip_result = exec("zip -rv $zip_file attachments", $output);
	if(file_exists($zip_file_full_path)){
		$content =  $lfile->file_read($zip_file_full_path);
  		output2browser($content, $zip_file);
	}else{
		echo "Error: failed to zip attachments.";
	}
}

intranet_closedb();
?>