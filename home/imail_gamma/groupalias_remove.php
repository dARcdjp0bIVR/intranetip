<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccess.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

$tablename = ($aliastype==1?"INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL":"INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL" );
$list = implode(",",$AliasID);
$li = new libdb();
$sql = "DELETE FROM $tablename WHERE AliasID IN ($list) AND OwnerID = '$UserID'";
$li->db_db_query($sql);

$anc = $special_option['no_anchor'] ?"":"#anc";

header("Location: groupalias.php?aliastype=$aliastype&countSyn=1&pageNo=$pageNo&order=$order&field=$field&TabID=$TabID&xmsg=delete".$anc);
intranet_closedb();
?>