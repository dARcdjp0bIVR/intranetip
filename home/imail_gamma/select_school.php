<?php
// page modifing by: 
$PathRelative = "../../../../";
$CurSubFunction = "Communication";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}
AuthPage(array("Communication-Mail-GeneralUsage"));
if (isset($_REQUEST["SchoolCode"]))
{
	include($PathRelative."config/" . $_REQUEST["SchoolCode"] . "/system_settings.php");
}
include_once($PathRelative."src/include/template/popup_header.php");
include_once($PathRelative."src/include/class/imap_gamma_ui.php");
include_once($PathRelative."src/include/class/imap_address_ui.php");

if (Auth(array("AllSchool"), "TARGET"))
{
	echo "<script>\n";
	echo "function SelectSchool()\n";
	echo "{\n";
	echo 'window.location = "select_recipient.php?SchoolCode="+document.getElementById("ddlSchool").value+"&SchoolName="+document.getElementById("ddlSchool").options[document.getElementById("ddlSchool").selectedIndex].text+"&fromSrc='.$_GET["fromSrc"].'&srcForm='.$_GET["srcFrom"].'";';
	echo "}\n";
	echo "</script>";

	// create UI instance
	$lui = new imap_gamma_ui();
	$LibAddress = new imap_address_ui();

	$SchoolList = array(); //build the school list from global_settings config, excluding the current school

	if (Auth(array("MySchool"), "TARGET"))
	{
		$SchoolList[0][0] = $Lang['email']['MySchool'];
		$SchoolList[0][1] = $_SESSION['SchoolCode'];
		$j = 1;
	}
	else
	{
		$j = 0;
	}

	for ($i = 0; $i < count($SYS_CONFIG['school']); ++$i)
	{
		if ($SYS_CONFIG['school'][$i][0] != $_SESSION['SchoolCode'])
		{
			$SchoolList[$j][0] = $SYS_CONFIG['school'][$i][1];
			$SchoolList[$j][1] = $SYS_CONFIG['school'][$i][0];
			++$j;
		}
	}

	echo $lui->Get_Div_Open("module_bulletin"," class=\"module_content\" " );
	echo $lui->Get_Div_Open(""," style=\"width:99%\" " );

	echo $lui->Get_Div_Open("module_content_header");				
	echo $lui->Get_Div_Open("module_content_header_title");				
	echo $lui->Get_HyperLink_Open("#");
	echo ($isiCal == 1) ? $Lang['calendar']['HeaderTitle'] : $Lang['email']['HeaderTitle'];
	echo $lui->Get_HyperLink_Close();
	echo $lui->Get_Div_Close();
	echo $lui->Get_Br();
	echo $lui->Get_Div_Close();
	echo $lui->Get_Br('style="clear:none"');

	echo $lui->Get_Div_Open();
	$SchoolLink .= $lui->Get_HyperLink_Open("#");
	$SchoolLink .= $lui->Get_Span_Open();
	$SchoolLink .= $Lang['email']['SelectRecipient'];
	$SchoolLink .= $lui->Get_Span_Close();
	$SchoolLink .= $lui->Get_HyperLink_Close();

	echo $lui->Get_Div_Close();

	echo $lui->Get_Div_Open("", 'class="imail_link_menu"');
	echo $lui->Get_Span_Open("", 'class="imail_entry_read_link"');

	echo $lui->Get_HyperLink_Open("select_school.php", "") . "Select School" . $lui->Get_HyperLink_Close();

	echo $lui->Get_Span_Close();
	echo $lui->Get_Div_Close();
	echo $lui->Get_Br();

	echo $lui->Get_Div_Open("commontabs_board", "class=\"imail_mail_content\"");
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $lui->Get_Input_Select("ddlSchool","ddlSchool",$SchoolList, $_SESSION['SchoolCode'], "size=\"10\" style=\"width: 250px\"  ");
	echo $lui->Get_Br();
	echo $lui->Get_Input_Button("btnSelect", "button", $Lang['btn_select'], 'class="button_main_act" onclick="SelectSchool()" ');
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Div_Close();

	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();


	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_close1","button",$Lang['btn_close_window'],'class="button_main_act" onclick="window.close()" ');
	echo $lui->Get_Div_Close();

	echo $lui->Get_Form_Close();

	echo $lui->Get_Div_Close();
	echo $lui->Get_Div_Close();
	echo $lui->Get_Div_Close();
}
elseif (Auth(array("MySchool"), "TARGET"))
{
	echo "<script>\n";
	echo 'window.location = "select_recipient.php?SchoolCode='.$_SESSION["SchoolCode"].'&SchoolName=My%20School&fromSrc='.$_GET["fromSrc"].'&srcForm='.$_GET["srcFrom"].'";';
	echo "</script>";
}
else
{
	echo $lui->Get_Div_Open("commontabs", "style='margin-top: 10px; margin-left: 10px'");
	echo $lui->Get_HyperLink_Open("#");
	echo $lui->Get_Span_Open();
	echo "You have no permission to select recipient";
	echo $lui->Get_Span_Close();
	echo $lui->Get_HyperLink_Close();
	echo $lui->Get_Br();
	
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_close1","button",$Lang['btn_close_window'],'class="button_main_act" onclick="window.close()" ');
	echo $lui->Get_Div_Close();
	echo $lui->Get_Div_Close();
}
include_once($PathRelative."src/include/template/popup_footer.php");
?>
