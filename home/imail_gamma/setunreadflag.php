<?php # Page Modifying By : Michael Cheung
$PathRelative = "../../../../";
$CurSubFunction = "Communication";

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}

AuthPage(array("Communication-Mail-GeneralUsage"));

include_once($PathRelative."src/include/class/imap_gamma.php");
include_once($PathRelative."src/include/class/imap_gamma_ui.php");
include_once($PathRelative."src/include/class/file_table.php");
include_once($PathRelative."src/include/class/file_system.php");
include_once($PathRelative."src/include/class/database.php");
include_once($PathRelative."src/include/class/user.php");

intranet_opendb();
####################################################################################

## Use Library
$IMap = new imap_gamma();
$lui = new imap_gamma_ui();
$ldb = new database();
$lfs = new file_system();


## Get Data 
//$UIDList 	= (isset($UID) && is_array($UID) && count($UID) > 0) ? $UID : array();
$UID  = (isset($UID)) ? $UID : "";		// needs to be array format
if(is_array($UID)){
	$UIDList = $UID;
} else {
	if($UID != ""){ $UIDList[0] = $UID; }
}

$CurTag  	= (isset($CurTag) && $CurTag != "") ? stripslashes(urldecode($CurTag)) : "Inbox";
$Folder  	= (isset($Folder) && $Folder != "") ? stripslashes(urldecode($Folder)) : '';
$FromSearch = (isset($FromSearch) && $FromSearch != "") ? $FromSearch : 0;

# Page Info Data 
$pageNo  	= (isset($pageNo) && $pageNo != "") ? $pageNo : 1;
$reverse  	= (isset($reverse) && $reverse != "") ? $reverse : 1;
$sort 		= (isset($sort) && $sort != "") ? $sort : (($FromSearch == 1) ? "SortDate" : "SORTARRIVAL");	

if($FromSearch == 1){
	$IsAdvanceSearch = (isset($IsAdvanceSearch) && $IsAdvanceSearch != "") ? $IsAdvanceSearch : 1;
} else {
	$IsAdvanceSearch = '';
}

# Get Data From Simple Search
$keyword 	    = (isset($keyword) && $keyword != "") ? stripslashes(trim($keyword)) : "";

# Other Post Data
$KeywordFrom 	= (isset($KeywordFrom) && $KeywordFrom != "") ? stripslashes(trim($KeywordFrom)) : '';
$KeywordTo   	= (isset($KeywordTo) && $KeywordTo != "") ? stripslashes(trim($KeywordTo)) : '';
$KeywordCc   	= (isset($KeywordCc) && $KeywordCc != "") ? stripslashes(trim($KeywordCc)) : '';
$KeywordSubject = (isset($KeywordSubject) && $KeywordSubject != "") ? stripslashes(trim($KeywordSubject)) : '';
$FromDate  		= (isset($FromDate) && $FromDate != "") ? trim($FromDate) : '';
$ToDate  		= (isset($ToDate) && $ToDate != "") ? trim($ToDate) : '';
$SearchFolder   = (isset($SearchFolder) && is_array($SearchFolder) && count($SearchFolder) > 0) ? $SearchFolder : array();

## Preparation
$personal_path = $SYS_CONFIG['sys_user_file']."/file/mail/u".$_SESSION['SSV_USERID'];
if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}

if ($Folder != "") {
	$ReturnTag = urlencode($Folder);
	$ReturnFolder = urlencode($Folder);
}
else {
	$ReturnTag = $SYS_CONFIG['Mail']['SystemMailBox'][0];
	$ReturnFolder = $SYS_CONFIG['Mail']['SystemMailBox'][0];
}


## Main 
for ($i=0; $i<sizeof($UIDList) ; $i++) {
	if (stristr($UIDList[$i],",") !== FALSE) {
		# Case for delete mails from Advance Search Result 
		List($UID, $Folder) = explode(",",$UIDList[$i]);
		
	}
	else {
		# Case for delete mails from List Mail Form
		$UID = $UIDList[$i];
		//$Folder = $_GET['Folder'];
		$Folder = (trim($Folder) == '') ? 'Inbox' : $Folder;
	}
	
	$Result = $IMap->unreadMail($Folder,$UID);
}

if ($Result) {
	// add log
	$user = new user($_SESSION['SSV_USERID']);
	$sql = 'Insert Into SYS_LOG
				(LogDate, LogUser, LogAction )
			Values 
				(
				GetDate(), 
				\''.$user->UserID.'\', 
				\''.$user->OfficialFullName.' unread '.sizeof($UIDList).' mail(s) from '.$Folder.'\'
				)
			 ';
	$ldb->db_db_query($sql);
	
	$Msg = $Lang['ReturnMsg']['MailMarkAsUnreadSuccess'];
}
else {
	$Msg = $Lang['ReturnMsg']['MailMarkAsUnreadUnsuccess'];
}

####################################################################################
intranet_closedb();

if($FromSearch == 1){
	$redirect_url = ($IsAdvanceSearch == 0) ? "search_result.php" : "advance_search_result.php";
	
	# Page from Advance Search
	echo '<html><body>';
	echo $lui->Get_Form_Open("form1", "POST", $redirect_url);
	echo $lui->Get_Input_Hidden("CurTag", "CurTag", urlencode($Folder))."\n";
	echo $lui->Get_Input_Hidden("Folder", "Folder", urlencode($Folder))."\n";
	echo $lui->Get_Input_Hidden("reverse", "reverse", $reverse)."\n";
	echo $lui->Get_Input_Hidden("sort", "sort", $sort)."\n";
	echo $lui->Get_Input_Hidden("pageNo", "pageNo", $pageNo)."\n";
	echo $lui->Get_Input_Hidden("FromSearch", "FromSearch", $FromSearch)."\n";
	echo $lui->Get_Input_Hidden("IsAdvanceSearch", "IsAdvanceSearch", $IsAdvanceSearch)."\n";

	# Hidden Values - Simple Search
	echo $lui->Get_Input_Hidden("keyword", "keyword", $keyword)."\n";
	
	# Hidden Values - Advance Search Field
	echo $lui->Get_Input_Hidden("KeywordFrom", "KeywordFrom", $KeywordFrom)."\n";
	echo $lui->Get_Input_Hidden("KeywordTo", "KeywordTo", $KeywordTo)."\n";
	echo $lui->Get_Input_Hidden("KeywordCc", "KeywordCc", $KeywordCc)."\n";
	echo $lui->Get_Input_Hidden("KeywordSubject", "KeywordSubject", $KeywordSubject)."\n";
	echo $lui->Get_Input_Hidden("FromDate", "FromDate", $FromDate)."\n";
	echo $lui->Get_Input_Hidden("ToDate", "ToDate", $ToDate)."\n";
	if(count($SearchFolder) > 0){
		for ($i=0; $i < sizeof($SearchFolder); $i++) {
			echo $lui->Get_Input_Hidden("SearchFolder[]", "SearchFolder[]", stripslashes($SearchFolder[$i]))."\n";
		}
	}
	echo $lui->Get_Form_Close();
	echo '<script type="text/javascript">';
	echo "function submit_form(){ document.form1.submit(); }";
	echo "submit_form();";
	echo "</script>";
	echo '</body></html>';
} else {
	# Page not from Search
	$params  = "?CurTag=$ReturnTag&Folder=$ReturnFolder&Msg=$Msg";
	$params .= "&pageNo=$pageNo&reverse=$reverse&sort=$sort";
	header("Location: imail_gamma.php".$params);
	exit;
}

?>