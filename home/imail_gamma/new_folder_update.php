<?php
// page modifing by : 
$PathRelative = "../../../../";
$CurSubFunction = "Communication";

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}
AuthPage(array("Communication-Mail-GeneralUsage"));

include_once($PathRelative."src/include/class/imap_gamma.php");

#########################################################################################################

## Use Library
$IMap = new imap_gamma();

## Get Data
$CurFolder  = (isset($CurFolder) && $CurFolder != "") ? stripslashes(urldecode($CurFolder)) : $IMap->FolderPrefix;
$CurTag 	= (isset($CurTag) && $CurTag != "") ? stripslashes(urldecode($CurTag)) : '';
$FolderName = (isset($FolderName) && $FolderName != "") ? stripslashes(trim($FolderName)) : "";
$ReturnPage = (isset($ReturnPage) && $ReturnPage != "") ? $ReturnPage : '';


## Main
if($FolderName != "" && $CurFolder != ""){
	$FolderName = IMap_Encode_Folder_Name($FolderName);
	$Result = $IMap->createMailFolder($CurFolder, $FolderName);
}

if ($Result) {
	$Msg = $Lang['ReturnMsg']['CreateSuccess'];
}
else {
	$Msg = $Lang['ReturnMsg']['CreateUnsuccess'];
}

#########################################################################################################
if($ReturnPage) {
	header("Location: ".$ReturnPage."?Msg=".$Msg);
} else {
	header("Location: imail_gamma.php?CurTag=".$CurTag."&Folder=".$CurFolder."&Msg=".$Msg);
}
exit;

?>