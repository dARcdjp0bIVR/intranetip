<?php
// page modifing by: 
/*********************************** Change Log *******************************
 * 2020-08-21 (Henry): bug fix for android chrome browser cannot send email [Case#R192408]
 * 2020-06-22 (Tommy): added permission checking 
 * 2020-05-18 (Sam): Encode MailBody to Base64 to avoid WAF blocking
 *                   Remove any unnecessary MailBody submission to avoid WAF blocking
 * 2017-06-09 (Carlos) : $sys_custom['iMailPlus']['CKEditor'] Use CKEditor.
 * 2016-03-11 (Carlos) : Use base64 encoded folder name $Folder_b if present, because some urlencoded plain text folder name unable to be decoded to the original value.
 * 2015-07-09 (Carlos) : disable send/save draft/reset buttons when press send or save draft to prevent fast repeated submits.
 * 2015-04-17 (Carlos) : modified js Check_Valid_Email_Address() to popup the invalid recipient alert.
 * 2014-03-26 (Carlos) : modified auto save interval to 2 minutes if no preference set
 * 2013-09-13 (Carlos) : if $sys_custom['iMailPlusUseStandardUploadMethod'], force use standard file upload method
 * 2013-06-26 (Carlos) : ask user whether to save draft when close the window 
 * 						(Note: Opera does not work, Chrome does not working properly as it ignore blocking functions like confirm() inside onbeforeunload event)
 * 2013-06-06 (Carlos) : modified js Check_Attach_Existence(), skip checking attachment existence, always return true
 * 2013-06-05 (Carlos) : IE7 or below use traditional file upload method
 * 2013-05-23 (Carlos) : modified js FCKeditor_OnComplete(), change div to textarea
 * 2013-04-09 (Carlos) : use plupload to upload attachment and embed image 
 * 2013-03-25 (Carlos) : disable Enter key to submit form
 * 2013-01-29 (Carlos) : changed js Update_Auto_Complete_Extra_Para() ExtraPara['UserID'] to ExtraPara['user_id'] as it may overwrite session
 * 2012-05-11 (Carlos) : added jquery extension function focusEnd() to set cursor position to the end of input text field 
 * 2012-05-07 (Carlos) : modified inline image path - add contentID as subfolder
 * 2011-11-22 (Carlos) : modified js Check_Submit() use getEditorValue() to get MailBody value instead of form.MailBody.value
 * 2011-10-28 (Carlos) : changed warnig msg at js Check_Attach_Existence()
 * 2011-08-19 (Yuen) : support iPad using plain text editor
 * 2011-04-29 (Carlos) : modified js Check_Total_Filesize() to auto remove newly uploaded attachment that exceed total size limit 
 * 2011-04-18 (Carlos) : fix js RemoveEmailInSelection()
 * 2011-04-15 (Carlos) : modified js checking and confirm msg for send mail by BCC in several batches if number of recipients > 350
 * 2011-02-07 (Carlos) : Added js function FCKeditor_OnComplete() to set content to preserve html & formatted text
 * 2011-01-17 (Carlos) : Fix Reset function
 ******************************************************************************/
$PATH_WRT_ROOT = "../../";
$CurSubFunction = "Communication";
$CurPage = $_SERVER["REQUEST_URI"];

//include_once($PATH_WRT_ROOT."src/include/class/startup.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail2007a.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/lang.marcus.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libusertype.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

if(!auth_sendmail()){
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

##########################################################################################

# Use Libraray
$ldb = new libdb();
$lfs = new libfilesystem();
$IMap = new imap_gamma();
$LibUserType = new libusertype();
$CurUserType = $LibUserType->returnIdentity($UserID);

//$lui = new imap_gamma_ui();
$is_below_IE8 = ($userBrowser->browsertype == "MSIE" && intval($userBrowser->version) < 8) || $sys_custom['iMailPlusUseStandardUploadMethod'];
$is_mobile_tablet_platform = ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod");
$is_chrome = $userBrowser->isChrome();

### Get Data 
//20100812
$CurTag 	  = (isset($CurTag) && $CurTag != "") ? stripslashes(urldecode($CurTag)) : $IMap->InboxFolder;
$CurMenu 	  = (isset($CurMenu) && $CurMenu != "") ? $CurMenu : 1;
$Msg 		  = (isset($msg) && $msg != "") ? $msg : "";
$FromSearch   = (isset($FromSearch) && $FromSearch != "") ? $FromSearch : 0;
$sort 		  = (isset($sort) && $sort != "") ? $sort : (($FromSearch == 1) ? "SortDate" : "SORTARRIVAL");	
$reverse 	  = (isset($reverse) && $reverse != "") ? $reverse : 1;
$pageNo 	  = (isset($pageNo) && $pageNo != "") ? $pageNo : 1;

if($FromSearch == 1){
	$IsAdvanceSearch = (isset($IsAdvanceSearch) && $IsAdvanceSearch != "") ? $IsAdvanceSearch : 1;
} else {
	$IsAdvanceSearch = '';
}

# Get Data From Simple Search
$keyword 	  = (isset($keyword) && $keyword != "") ? stripslashes(trim($keyword)) : "";

# Get Data From Advance Search
$KeywordFrom 	= (isset($KeywordFrom) && $KeywordFrom != "") ? $KeywordFrom : '';
$KeywordTo   	= (isset($KeywordTo) && $KeywordTo != "") ? $KeywordTo : '';
$KeywordCc   	= (isset($KeywordCc) && $KeywordCc != "") ? $KeywordCc : '';
$KeywordSubject = (isset($KeywordSubject) && $KeywordSubject != "") ? $KeywordSubject : '';
$FromDate  		= (isset($FromDate) && $FromDate != "") ? $FromDate : '';
$ToDate  		= (isset($ToDate) && $ToDate != "") ? $ToDate : '';
if(isset($IMap->AllFolderList) && sizeof($IMap->AllFolderList)>0){
	$FolderList = $IMap->AllFolderList;
}else{
	$FolderList = $IMap->getMailFolders();
	$IMap->AllFolderList = $FolderList;
}
$SearchFolder   = (isset($SearchFolder) && is_array($SearchFolder) && count($SearchFolder) > 0) ? $SearchFolder : $FolderList;

# Mail Data
$MailAction   = (isset($MailAction) && $MailAction != "") ? $MailAction : "";			// Action Type (Reply/ Forward/ ReplyAll)
$ActionUID    = (isset($UID) && $UID != "") ? $UID : "";								// UID to reply
$DraftUID 	  = (isset($uid) && $uid != "") ? $uid : "";								// Draft mail UID
//20100812
if(isset($Folder_b) && $Folder_b!=''){
	$Folder = base64_decode($Folder_b);
}else{
	$Folder = (isset($Folder) && $Folder != "") ? stripslashes(urldecode($Folder)) : $IMap->InboxFolder;
}
$MessageID	  = ($MailAction == 'Reply' || $MailAction == 'ReplyAll' || $MailAction == 'Forward') ? $ActionUID : $DraftUID;
$PartNumber = (isset($PartNumber) && $PartNumber != "") ? $PartNumber : "";

# Compose Time
$ComposeTime = time();
$ComposeDatetime = date("Y-m-d H:i:s",$ComposeTime);

# Max number of mail address allowed to send in each To, Cc, Bcc field
$MAX_NUM_MAIL_ADDRESS = !isset($SYS_CONFIG['Mail']['MaxNumMailsPerBatch'])?200:$SYS_CONFIG['Mail']['MaxNumMailsPerBatch'];

### Preparation
//$personal_path = $SYS_CONFIG['sys_user_file']."/file/mail/u".$_SESSION['SSV_USERID'];
$gamma_path = "$file_path/file/gamma_mail";
if (!is_dir($gamma_path))
{
    $lfs->folder_new($gamma_path);
}

$personal_path = "$file_path/file/gamma_mail/u$UserID";
if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}

$plupload_tmp_path = $personal_path."/".$ComposeTime;

if(!is_dir($plupload_tmp_path)) {
	$lfs->folder_new($plupload_tmp_path);
}

$sql = "INSERT INTO MAIL_TEMP_UPLOAD_FOLDER (UserID,ComposeTime) VALUES ('$UserID','$ComposeTime')";
$ldb->db_db_query($sql);

# select file list to delete
$sql = "SELECT 
			FileID, EncodeFileName
		FROM 
			MAIL_ATTACH_FILE_MAP 
		WHERE 
			UserID = '".$UserID."' AND  
			DraftMailUID IS NULL AND 
			OrgDraftMailUID IS NULL AND 
			TIMESTAMPDIFF(SECOND,ComposeTime,'$ComposeDatetime') > 86400 ";
$DeleteList = $ldb->returnArray($sql,2);

$emailAddr = Array();
# calendar action: submit and send e-mail
if (isset($eventID) && !empty($eventID)){
	$NameField = " IF(TRIM(a.NickName)='',".getNameFieldByLang2("a.").",a.NickName) ";
	$sql = "SELECT 
				CONCAT('\"',$NameField, IF(a.RecordType = 1, '', IF(a.ClassName = '' OR a.ClassName IS NULL, '', CONCAT(' (',a.ClassName,'-',a.ClassNumber,')'))) ,'\" <',a.ImapUserEmail,'>') as email,
				c.Title as EventName 
			FROM INTRANET_USER as a INNER JOIN CALENDAR_EVENT_USER as b
			ON a.UserID = b.UserID
			INNER JOIN CALENDAR_EVENT_ENTRY as c
			ON c.EventID = b.EventID
			WHERE c.EventID = '$eventID' AND a.UserID <> '".$_SESSION['UserID']."' ";
	$resultSet = $ldb->returnArray($sql);
	//$msg = $isOptional?$iCalendar_submitNsend_msg:$iCalendar_submitNsend_msgShort;
	//$preloadMsg = str_replace("%EventTitle%",$resultSet[0][2],$msg);
	for($i=0;$i<sizeof($resultSet);$i++)
	{
		$emailAddr[]['email'] = $resultSet[$i]['email'];
	}
}

# internal use
$internalUseArray = array();
if(isset($internalUse) && !empty($internalUse))
{
	$internalUseArray['internalUse'] = $internalUse;
	$internalUseArray['internalSubject'] = stripslashes(stripslashes($internalSubject));
	$internalUseArray['internalBody'] = $internalBody;
	$internalUseArray['internalSender'] = $internalSender;
	$internalUseArray['internalTo'] = $internalTo;
	$internalUseArray['internalCc'] = $internalCc;
	$internalUseArray['internalBcc'] = $internalBcc;
	$internalUseArray['internalEnc'] = isset($internalEnc);
}

# delete the file in file system
for($i=0;$i< sizeof($DeleteList);$i++){
	list($FileID,$FileName) = $DeleteList[$i];
	$loc = $personal_path."/".$FileName;
	
	$lfs->file_remove($loc);
}
	
$sql = "Delete from 
			MAIL_ATTACH_FILE_MAP 
		WHERE 
			UserID = '".$UserID."' AND 
			DraftMailUID IS NULL AND 
			OrgDraftMailUID IS NULL AND
			TIMESTAMPDIFF(SECOND,ComposeTime,'$ComposeDatetime') > 86400 ";
$ldb->db_db_query($sql);

if ($DraftUID != "") {
	$sql = "Update MAIL_ATTACH_FILE_MAP 
			SET 
				DraftMailUID = '$DraftUID' ,
				ComposeTime = '$ComposeDatetime' 
			WHERE 
				UserID = '".$UserID."' AND  
				OrgDraftMailUID = '$DraftUID' AND 
				DraftMailUID = -1";				// -1 ???
	$ldb->db_db_query($sql);
}

/*
# Delete temporary images attached to mail body when composing mail
$lfs->folder_remove_recursive($personal_path."/related");
$sql = "DELETE FROM MAIL_INLINE_IMAGE_MAP WHERE UserID = '$UserID' ";
$ldb->db_db_query($sql);
*/
$personal_related_path = $personal_path."/related";

# Select inline image list to delete 
$sql = "SELECT FileID,OriginalImageFileName,ContentID FROM MAIL_INLINE_IMAGE_MAP WHERE UserID = '$UserID' AND TIMESTAMPDIFF(SECOND,ComposeTime,'$ComposeDatetime') > 86400 ";
$DeleteRelatedInlineList = $ldb->returnArray($sql,3);
for($i=0;$i<sizeof($DeleteRelatedInlineList);$i++){
	list($RelatedFileID,$RelatedFileName,$RelatedContentID) = $DeleteRelatedInlineList[$i];
	//$loc = $personal_related_path."/".$RelatedContentID."/".$RelatedFileName;
	//$lfs->file_remove($loc);
	$lfs->deleteDirectory($personal_related_path."/".$RelatedContentID);
}
$sql = "DELETE FROM MAIL_INLINE_IMAGE_MAP WHERE UserID = '$UserID' AND TIMESTAMPDIFF(SECOND,ComposeTime,'$ComposeDatetime') > 86400 ";
$ldb->db_db_query($sql);

# select related files to be preserved, not to delete yet
$sql = "SELECT FileID,OriginalImageFileName,ContentID FROM MAIL_INLINE_IMAGE_MAP WHERE UserID = '$UserID' AND TIMESTAMPDIFF(SECOND,ComposeTime,'$ComposeDatetime') <= 86400 ";
$PreserveRelatedInlineList = $ldb->returnArray($sql,3);
$related_files_preserve = array();
for($i=0;$i<sizeof($PreserveRelatedInlineList);$i++){
	list($RelatedFileID,$RelatedFileName,$RelatedContentID) = $PreserveRelatedInlineList[$i];
	$related_files_preserve[$RelatedContentID."/".$RelatedFileName] = $RelatedContentID."/".$RelatedFileName;
}

# get all remaining related files
$related_files_fullpath = $lfs->return_folderlist($personal_related_path);
for($i=0;$i<sizeof($related_files_fullpath);$i++){
	if(is_file($related_files_fullpath[$i])){
		$last_slash = strrpos($related_files_fullpath[$i],"/");
		if($last_slash !== FALSE){
			$last_second_slash = strrpos($related_files_fullpath[$i],"/",-(strlen($related_files_fullpath[$i])-$last_slash+1));
			if($last_second_slash !== FALSE){
				$temp_filename = substr($related_files_fullpath[$i],$last_second_slash+1);
				if(!isset($related_files_preserve[$temp_filename])){
					$temp_folder = substr($related_files_fullpath[$i],0,$last_slash);
					//$lfs->file_remove($related_files_fullpath[$i]);
					$lfs->deleteDirectory($temp_folder);
				}
			}
		}
	}
	//$temp_filename = basename($related_files_fullpath[$i]);
	//if(!isset($related_files_preserve[$temp_filename])) $lfs->file_remove($related_files_fullpath[$i]);
}

# clean temp plupload folders
$temp_folders_to_delete = array();
$sql = "SELECT FolderID,ComposeTime FROM MAIL_TEMP_UPLOAD_FOLDER WHERE UserID='".$_SESSION['UserID']."' AND $ComposeTime > (ComposeTime+86400)";
$temp_folders = $ldb->returnResultSet($sql);
for($i=0;$i<sizeof($temp_folders);$i++) {
	$temp_folder = $personal_path."/".$temp_folders[$i]['ComposeTime'];
	$lfs->deleteDirectory($temp_folder);
	$temp_folders_to_delete[] = $temp_folders[$i]['FolderID'];
}
if(count($temp_folders_to_delete)>0) {
	$sql = "DELETE FROM MAIL_TEMP_UPLOAD_FOLDER WHERE UserID='".$_SESSION['UserID']."' AND FolderID IN (".implode(",",$temp_folders_to_delete).")";
	$ldb->db_db_query($sql);
}

# Hidden - Search Folder
//for ($i=0;$i< sizeof($SearchFolder); $i++) {
//	$QuerySearchFolder .= $lui->Get_Input_Hidden("SearchFolder[]","SearchFolder[]", stripslashes($SearchFolder[$i]))."\n";
//}

#get preference
$pref = $IMap->Get_Preference();

#auto save interval
$jsInterval = $pref["AutoSaveInterval"]!=''?$pref["AutoSaveInterval"]:2;
$jsEnableAutoSave = ($jsInterval>0&&$SYS_CONFIG['AutoSave']['Mail']['Draft']['Status'])?"true":"false";

# signature

//$signature_sql = "SELECT Signature FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='$UserID'";
//$temp_result = $ldb->returnVector($signature_sql);
//$signature = $temp_result[0];
$signature = $pref["Signature"];

## Special handling for CS department
if(isset($internalUseArray['internalSender']) && preg_match('/^(.)*support@broadlearning.com(.)*$/i',$internalUseArray['internalSender']))
{
	$sql = "SELECT Signature FROM MAIL_PREFERENCE WHERE MailBoxName = 'support@broadlearning.com' ";
	$internalUseSignature = $ldb->returnVector($sql);
	if(trim($internalUseSignature[0])!='')
	{
		$signature = $internalUseSignature[0];
		//$IMap->Update_Preference(array("Signature"=>$signature));
	}
}

# after auto save draft. current uid (passed by url param) will be expired.
# as new draft cannot be saved with current uid(only can append mail with new uid), need to keep refreshing new uid
# $_SESSION["UIDChange"][$DraftUID] was set as new uid every auto save in auto_save_draft.php
# $_SESSION["UIDChange"][$DraftUID] was reset in imap_gamma constructor except that called by current page
$OriDraftUID = $DraftUID;
if(isset($_SESSION["UIDChange"][$DraftUID]) && $DraftUID!='')
	$DraftUID = $_SESSION["UIDChange"][$DraftUID];

################# New to re-design the logic for READ Mail Content ################# 
# Get the Mail Content for display
if($CampusMailID=='')
{
	if ($DraftUID != "") {											# Draft Mail 
		# if $DraftUID exist, $Folder should always be INBOX.Drafts;
		//20100812
		$Folder = $IMap->DraftFolder;
		$EmailContent = $IMap->getMailRecord($Folder, $DraftUID, $PartNumber);
	} else if ($MailAction != "") {									# Not New Compose Email
		$EmailContent = $IMap->getMailRecord($Folder, $ActionUID, $PartNumber);
	}
	
	if (isset($_GET["to"]))
	{
		// may put back to user class
	//	$sql = "SELECT CASE
	//				WHEN U.OfficialFullName is NULL THEN U.FirstName + ' ' + U.Surname
	//				ELSE U.OfficialFullName
	//				END as 'Name'
	//			FROM INTRANET_USER U WITH (NOLOCK)
	//			WHERE U.Email = '".$_GET['to']."'";
	//	$EmailName = $ldb->returnVector($sql);
		$tempEmail = explode("@", urldecode($_GET["to"]));
		$EmailContent[0]['toDetail'][] = (object)array();
		$EmailContent[0]['toDetail'][count($EmailContent[0]['toDetail'])-1]->mailbox = $tempEmail[0];
		$EmailContent[0]['toDetail'][count($EmailContent[0]['toDetail'])-1]->host = $tempEmail[1];
		if($_GET["person"])
		$EmailContent[0]['toDetail'][count($EmailContent[0]['toDetail'])-1]->personal = $_GET["person"];
	}
}
## Handling the attached files which are included in "Forward" MailAction Type
/*if ($MailAction == "Forward") {
	# Remove the existing attached files in Forwarded Email Message 
	# in case any dummy record stored in db and server	
	# Remove the Unlink Attached Files which may created in before forwarded mail
	$IMap->Remove_Unlink_Attach_File($MessageID, $MailAction);
	
	# Re-insert the Forwared Attached File into Db and Server
	$AttachList = $EmailContent[0]["attach_parts"];	
	if(count($AttachList) > 0){
		for ($i=0; $i< sizeof($AttachList); $i++) {
			$file = $AttachList[$i]['filename'];
		     
			if ($file != "") {
				$sql = 'Insert into MAIL_ATTACH_FILE_MAP 
							(UserID, UploadTime, OriginalFileName, OrgDraftMailUID) 
						Values 
							('.$_SESSION['SSV_USERID'].',GETDATE(), 
							 '.UTF8_To_DB_Handler($file).', \''.$MessageID.'\') 
						 ';
				$ldb->db_db_query($sql);
				$EncodeName = $ldb->db_insert_id();
				$FileNumber++;
			}
			$des = "$personal_path/".$EncodeName;
			$lfs->file_write($AttachList[$i]['content'], $des);
		}
	}
}*/

################# ################# ################# 

##############################################################################
### Imap use only
//$mailbox 	   = (isset($_GET["mailbox"])) ? $_GET["mailbox"] : "Inbox";				//SET UP INBOX
# Go to the Corresponding Folder Currently selected
imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$Folder);		
$imap_obj 	   = imap_check($IMap->inbox);
$unseen_status = imap_status($IMap->inbox, "{".$IMap->host."}".$Folder, SA_UNSEEN);
$statusInbox   = imap_status($IMap->inbox, "{".$IMap->host."}Inbox", SA_UNSEEN);

$imapInfo = array();
$imapInfo['imap_obj'] 	   = $imap_obj;
$imapInfo['unseen_status'] = $unseen_status;
$imapInfo['statusInbox']   = $statusInbox;
$imapInfo['mailbox']   	   = $Folder;
##############################################################################
	
$CurrentPage = "PageComposeMail";
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();

# tag information
$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$i_CampusMail_New_ComposeMail."</td><td align='right' style='vertical-align: bottom;' >".$IMap->TitleToolBar()."</td></tr></table>";

$TAGS_OBJ[] = array($iMailTitle, "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();
	
?>
<?=$linterface->Include_Plupload_JS_CSS()?>
<script>
<!--

var AjaxActive = true;
var debugMode;
//debugMode=true;

function getEditorValue(instanceName) {
// Get the editor instance that we want to interact with.
var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;

// Get the editor contents as XHTML.
return oEditor.GetXHTML( true ) ; // "true" means you want it formatted.
}

var enableAutoSave = <?=$jsEnableAutoSave?>;
var interval = <?=$jsInterval?>;

//var interval = <?=$SYS_CONFIG['AutoSave']['Mail']['Draft']['Interval']?>;
interval = interval * 60 * 1000;
// interval = 15000;

var intervalID = 0; 

	if(enableAutoSave) {		
		intervalID = setInterval('autoSave()', interval);
	}

var first =1;

function autoSave(isSynchronousRequest) {	
	// updateRTEs();
	if(!AjaxActive)
		return false;
	
	EmailAjaxObj = GetXmlHttpObject();
	
	// var postObj = new Array();
		
	if (EmailAjaxObj == null){
		alert (errAjax);
		return;
	}
	var isAsynchronous = isSynchronousRequest ? false : true;
//	if(!Check_Number_Of_Email_Address("ToAddress")) { return false; }
//	if(!Check_Number_Of_Email_Address("CcAddress")) { return false; }
//	if(!Check_Number_Of_Email_Address("BccAddress")) { return false; }
	
	var url = "auto_save_draft.php";
	var excluded = "MailBody";
	var postContent = Get_Form_Values(document.getElementById('NewMailForm'),excluded);
	
	var To = document.NewMailForm.ToAddress.value;
	var Cc = document.NewMailForm.CcAddress.value;
	var Bcc = document.NewMailForm.BccAddress.value;
	                                  // WAF workaround
	var MailBody = encodeURIComponent(Base64.encode(getEditorValue("MailBody")));
		postContent += '&ToAddress='+To;
		postContent += '&CcAddress='+Cc;
		postContent += '&BccAddress='+Bcc;
		postContent += '&MailBody='+MailBody;
	
	if (!debugMode) {
		
		EmailAjaxObj.onreadystatechange = function () {
			if (EmailAjaxObj.readyState==4) {
				var responseValue = $.trim(EmailAjaxObj.responseText);
				var responseArray = responseValue.split("|==|");
				var MsgArr = responseArray[0].split('|=|');
				var MsgFlag = MsgArr[0];
				var Msg = MsgArr[1];
				var UID = responseArray[1];

				if (MsgFlag == 1) {
//					if (document.getElementById('MailAction').value != '') {
//						document.getElementById('ActionUID').value = UID;
//					} 
//					else {
//						document.getElementById('DraftUID').value = UID;
//					}
					document.getElementById('DraftUID').value = UID;
				}

				Get_Return_Message(responseArray[0]);
			}
		};
		EmailAjaxObj.open("POST", url, isAsynchronous);
		EmailAjaxObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		EmailAjaxObj.send(postContent);
	} else {
		alert(postContent);
	}
}

function Check_Submit(obj,mode) {
	// updateRTEs();
	// Check the total attachment size 
	if (!Check_Total_Filesize(0)) {
		disable_action_buttons(false);
		return false;
	}

	// Check the total number of input email address
//	if(!Check_Number_Of_Email_Address("ToAddress")) { return false; }
//	if(!Check_Number_Of_Email_Address("CcAddress")) { return false; }
//	if(!Check_Number_Of_Email_Address("BccAddress")) { return false; }
	if(mode==0 && (!Check_Number_Of_Email_Address("ToAddress") 
	  || !Check_Number_Of_Email_Address("CcAddress")
	  || !Check_Number_Of_Email_Address("BccAddress")))
	{
		if(!confirm('<?=str_replace("<!--UpperLimit-->",$MAX_NUM_MAIL_ADDRESS,$Lang['Gamma']['ConfirmMsg']['ConfirmSplitMailListAndSendInBatch'])?>')){
			disable_action_buttons(false);
			return false;
		}
	}
<?php
if ($userBrowser->platform!="iPad" && $userBrowser->platform!="Andriod" && !$is_below_IE8)
{
?>
	if (FileCount > 0) {
		alert('<?=$Lang['Gamma']['Warning']['AttachmentUploadInProgress']?>');
		disable_action_buttons(false);
		return false;
	}
<?php 
} else {
?>
	if (FileCount > 0) {
		for (var i=0; i< FileCount; i++) {
			Temp = document.getElementById("File"+i).innerHTML;
			if (Temp.indexOf("uploading...") != -1) {
				alert('<?=$Lang['Gamma']['Warning']['AttachmentUploadInProgress']?>');
				disable_action_buttons(false);
				return false;
			}
		}
	}
<?php 
} 
?>
	var To = obj.ToAddress.value;
	var Cc = obj.CcAddress.value;
	var Bcc = obj.BccAddress.value;
<?php
if ($sys_custom['iMailPlus']['CKEditor'] || ($userBrowser->platform!="iPad" && $userBrowser->platform!="Andriod"))
{
?>
	var Body = getEditorValue('MailBody');
<?php
}else{
?>
	var Body = obj.MailBody.value;
<?php
}
?>
	var Subject = obj.Subject.value;
	
	if (mode == 0) {	
		if (To.Trim() == "" && Cc.Trim() == "" && Bcc.Trim() == "") {
			alert("<?=$Lang['Warning']['EmptyToFieldWarning']?>");
			disable_action_buttons(false);
			return false;
		}
		if(Subject.Trim() == '')
		{
			LeaveTitleEmpty = confirm("<?=$Lang['Gamma']['Warning']['SendMessageWithoutSubject']?>");
			if(!LeaveTitleEmpty)
			{
				disable_action_buttons(false);
				return false;
			}
		}
		//if (Check_Mail_Format()) {
			obj.action = "compose_email_process.php?uid=<?=$DraftUID?>";
			obj.target = "_self";
			
			if (Check_Attach_Existence(Body)) {
				//document.getElementById("SendBtn").disabled = true;
				//document.getElementById("SaveBtn").disabled = true;
				//document.getElementById("CancelBtn").disabled = true;
				disable_action_buttons(true);
				AjaxActive = false;
				isCloseWindow = false;
				// WAF workaround
//				var editor = FCKeditorAPI.GetInstance("MailBody");
				var EncodedMailBody = Base64.encode(Body);
				$(obj).append('<input type="hidden" name="EncodedMailBody" value="'+EncodedMailBody+'" />');
// 				editor.SetData ? editor.SetData('') : editor.setData('');
// 				editor.Events.AttachEvent( 'OnAfterSetHTML', function() {obj.submit()});
				$("#MailBody").attr("disabled", "disabled");
				obj.submit()
				return true;
			}
			else {
				disable_action_buttons(false);
				return false;
			}
		//}
	}
	else { 
		//if (Check_Mail_Format()) {
			obj.action = "save_draft.php?uid=<?=$DraftUID?>";
			obj.target = "_self";
	
			if (Check_Attach_Existence(Body)) {
				//document.getElementById("SendBtn").disabled = true;
				//document.getElementById("SaveBtn").disabled = true;
				//document.getElementById("CancelBtn").disabled = true;
				disable_action_buttons(true);
				AjaxActive = false;
				isCloseWindow = false;
				// WAF workaround
//				var editor = FCKeditorAPI.GetInstance("MailBody");
				var EncodedMailBody = Base64.encode(Body);
				$(obj).append('<input type="hidden" name="EncodedMailBody" value="'+EncodedMailBody+'" />');
// 				editor.SetData ? editor.SetData('') : editor.setData('');
// 				editor.Events.AttachEvent( 'OnAfterSetHTML', function() {obj.submit()});
				$("#MailBody").attr("disabled", "disabled");
				obj.submit()
				return true;
			}
			else {
				disable_action_buttons(false);
				return false;
			}
		//}
	}
	
	return false;
}

function Check_Attach_Existence(CheckText) {
	return true;
/*	
	var re = new RegExp("(attachment|attach)","i");
	var mailAction = document.getElementById('MailAction').value;
	if (CheckText.match(re)) {
		var ActionMailAttachList = document.getElementsByName('AttachPartNumber[]');

		var ActionMailAttachedSelected = false;
		if (ActionMailAttachList != null) {			
			for (var i=0; i < ActionMailAttachList.length; i++) {
				if (ActionMailAttachList[i].checked == true) 
					ActionMailAttachedSelected = true;
			}
		}
		
		var TNEFList = document.getElementsByName('TNEFFile[]');

		var TNEFListSelected = false;
		if (TNEFList != null) {			
			for (var i=0; i < TNEFList.length; i++) {
				if (TNEFList[i].checked == true) 
					TNEFListSelected = true;
			}
		}
		
		if (document.getElementById('FileID[]') == null && !ActionMailAttachedSelected && !TNEFListSelected && !(mailAction == 'Reply' || mailAction == 'Forward' || mailAction == 'ReplyAll')) {
			return confirm("<?=$Lang['Gamma']['Warning']['SendWithoutAttachment']?>");
		}
		else {
			return true;
		}
	}
	else {
		return true;
	}
*/
}

function Check_Mail_Format() {
	var ToValue = document.getElementById('ToAddress').value;
	var CcValue = document.getElementById('CcAddress').value;
	var BccValue = document.getElementById('BccAddress').value;
	
	SplitValue = ';';
	var ToEmailList = ToValue.split(SplitValue);
	var CcEmailList = CcValue.split(SplitValue);
	var BccEmailList = BccValue.split(SplitValue);
	
	// check for To Address
	msg = "";
	for (var i=0; i< ToEmailList.length; i++) {
		//alert(ToEmailList[i]);
		if (ToEmailList[i].indexOf(',') == -1) {
			if (!Validate_Email(ToEmailList[i]) && ToEmailList[i].Trim() != "") 
				msg += ToEmailList[i] + ", \n";
		}
		else {
			TempFirstDoubleQuoteIndex=-1;
			TempSecondDoubleQuoteIndex=-1;
			StartIndex = 0;
			Actioned = false;
			FirstQuoteIndexSet = false;
			SecondQuoteIndexSet = false;
			SubSplitTo = ToEmailList[i].split("");
			for (var j=0; j< SubSplitTo.length; j++) {
				if (Actioned) {
					FirstQuoteIndexSet = false;
					SecondQuoteIndexSet = false;
					Actioned = false;
					StartIndex = j;
				}
					
				if (SubSplitTo[j] == '"') {
					if (!FirstQuoteIndexSet) {
						TempFirstDoubleQuoteIndex = j;
						FirstQuoteIndexSet = true;
					}
					else {
						TempSecondDoubleQuoteIndex = j;
						SecondQuoteIndexSet = true;
					}
				}
				
				if (SubSplitTo[j] == ',') {
					if (FirstQuoteIndexSet) {
						if (!SecondQuoteIndexSet) {
						}
						else {
							Actioned = true;
							ValToTest = ToEmailList[i].substring(StartIndex,j);
							//alert(ValToTest+"HAVEQUOTE");
							if (!Validate_Email(ValToTest)) 
								msg += ValToTest + ", \n";
						}
					}
					else {
						Actioned = true;
						ValToTest = ToEmailList[i].substring(StartIndex,j);
						//alert(ValToTest+"NOQUOTE");
						if (!Validate_Email(ValToTest)) 
							msg += ValToTest + ", \n";
					}
					continue;
				}
				
				if (j == (SubSplitTo.length-1) ) {
					ValToTest = ToEmailList[i].substring(StartIndex,(j+1));
					//alert(ValToTest+"END");
					if (!Validate_Email(ValToTest)) 
						msg += ValToTest + ", \n";
				}
			}
		}
	}
	if (msg != "") {
		msg += '<?=$Lang['Warning']['ToMailFormatWarning']?>';
		alert(msg);
		return false;
	}
	
	// check for Cc Address
	msg = "";
	for (var i=0; i< CcEmailList.length; i++) {
		//alert(CcEmailList[i]);
		if (CcEmailList[i].indexOf(',') == -1) {
			if (!Validate_Email(CcEmailList[i]) && CcEmailList[i].Trim() != "") 
							msg += CcEmailList[i] + ", \n";
		}
		else {
			TempFirstDoubleQuoteIndex=-1;
			TempSecondDoubleQuoteIndex=-1;
			StartIndex = 0;
			Actioned = false;
			FirstQuoteIndexSet = false;
			SecondQuoteIndexSet = false;
			SubSplitTo = CcEmailList[i].split("");
			for (var j=0; j< SubSplitTo.length; j++) {
				if (Actioned) {
					FirstQuoteIndexSet = false;
					SecondQuoteIndexSet = false;
					Actioned = false;
					StartIndex = j;
				}
					
				if (SubSplitTo[j] == '"') {
					if (!FirstQuoteIndexSet) {
						TempFirstDoubleQuoteIndex = j;
						FirstQuoteIndexSet = true;
					}
					else {
						TempSecondDoubleQuoteIndex = j;
						SecondQuoteIndexSet = true;
					}
				}
				
				if (SubSplitTo[j] == ',') {
					if (FirstQuoteIndexSet) {
						if (!SecondQuoteIndexSet) {
						}
						else {
							Actioned = true;
							ValToTest = CcEmailList[i].substring(StartIndex,j);
							//alert(ValToTest+"HAVEQUOTE");
							if (!Validate_Email(ValToTest)) 
								msg += ValToTest + ", \n";
						}
					}
					else {
						Actioned = true;
						ValToTest = CcEmailList[i].substring(StartIndex,j);
						//alert(ValToTest+"NOQUOTE");
						if (!Validate_Email(ValToTest)) 
							msg += ValToTest + ", \n";
					}
					continue;
				}
				
				if (j == (SubSplitTo.length-1) ) {
					ValToTest = CcEmailList[i].substring(StartIndex,(j+1));
					//alert(ValToTest+"END");
					if (!Validate_Email(ValToTest)) 
						msg += ValToTest + ", \n";
				}
			}
		}
	}
	if (msg != "") {
		msg += '<?=$Lang['Warning']['CcMailFormatWarning']?>';
		alert(msg);
		return false;
	}
	
	// check for Bcc Address
	msg = "";
	for (var i=0; i< BccEmailList.length; i++) {
		//alert(BccEmailList[i]);
		if (BccEmailList[i].indexOf(',') == -1) {
			if (!Validate_Email(BccEmailList[i]) && BccEmailList[i].Trim() != "") 
				msg += BccEmailList[i] + ", \n";
		}
		else {
			TempFirstDoubleQuoteIndex=-1;
			TempSecondDoubleQuoteIndex=-1;
			StartIndex = 0;
			Actioned = false;
			FirstQuoteIndexSet = false;
			SecondQuoteIndexSet = false;
			SubSplitTo = BccEmailList[i].split("");
			for (var j=0; j< SubSplitTo.length; j++) {
				if (Actioned) {
					FirstQuoteIndexSet = false;
					SecondQuoteIndexSet = false;
					Actioned = false;
					StartIndex = j;
				}
					
				if (SubSplitTo[j] == '"') {
					if (!FirstQuoteIndexSet) {
						TempFirstDoubleQuoteIndex = j;
						FirstQuoteIndexSet = true;
					}
					else {
						TempSecondDoubleQuoteIndex = j;
						SecondQuoteIndexSet = true;
					}
				}
				
				if (SubSplitTo[j] == ',') {
					if (FirstQuoteIndexSet) {
						if (!SecondQuoteIndexSet) {
						}
						else {
							Actioned = true;
							ValToTest = BccEmailList[i].substring(StartIndex,j);
							//alert(ValToTest+"HAVEQUOTE");
							if (!Validate_Email(ValToTest)) 
								msg += ValToTest + ", \n";
						}
					}
					else {
						Actioned = true;
						ValToTest = BccEmailList[i].substring(StartIndex,j);
						//alert(ValToTest+"NOQUOTE");
						if (!Validate_Email(ValToTest)) 
							msg += ValToTest + ", \n";
					}
					continue;
				}
				
				if (j == (SubSplitTo.length-1) ) {
					ValToTest = BccEmailList[i].substring(StartIndex,(j+1));
					//alert(ValToTest+"END");
					if (!Validate_Email(ValToTest)) 
						msg += ValToTest + ", \n";
				}
			}
		}
	}
	if (msg != "") {
		msg += '<?=$Lang['Warning']['BccMailFormatWarning']?>';
		alert(msg);
		return false;
	}
	
	return true;
}


/*------ new JS -----*/
function Check_Total_Filesize(CheckNewOnly){
	var max_size = <?=$SYS_CONFIG['Mail']['MaxAttachmentSize']?>;	// 8 * 1024 Kb = 8Mb
	var total_size = 0;
	var file_size = 0;
	
	// new attached file
	if(document.getElementsByName("FileID[]")){					// if object exists
		obj = document.getElementsByName("FileID[]");
		if(obj.length > 0){
			sizeObj = document.getElementsByName("FileSize[]");
			for(var i=0 ; i<obj.length ; i++){
				if(obj[i].checked == true){
					file_size = sizeObj[i].value;
					total_size = parseFloat(file_size) + parseFloat(total_size);
				}
			}
		}
	}
	
	// exist attached file
	if(CheckNewOnly == 0){
		if(document.getElementsByName("AttachPartNumber[]")){	// if object exists
			var attObj = document.getElementsByName("AttachPartNumber[]");
			if(attObj.length > 0){
				attSizeObj = document.getElementsByName("AttachPartSize[]");
				for(var i=0 ; i<attObj.length ; i++){
					if(attObj[i].checked == true){
						file_size = attSizeObj[i].value;
						total_size = parseFloat(file_size) + parseFloat(total_size);
					}
				}
			}
		}
	}
<?php
if ($userBrowser->platform!="iPad" && $userBrowser->platform!="Andriod")
{
?>	
	// check new inline images size
	var image_file_ids= document.getElementsByName("ImageFileID[]");
	if(image_file_ids){
		var mail_content = FCKeditorAPI.GetInstance("MailBody").GetData ? FCKeditorAPI.GetInstance("MailBody").GetData() : FCKeditorAPI.GetInstance("MailBody").getData();
		
		var image_file_sizes = document.getElementsByName("ImageFileSize[]");
		for(var i=0;i<image_file_ids.length;i++){
			if(mail_content.indexOf(image_file_ids[i].value.valueOf())>=0){// count those image that is not removed by user
				file_size = image_file_sizes[i].value;
				total_size = parseFloat(file_size) + parseFloat(total_size);
			}
		}
	}
<?php
}
?>	
	if(total_size > max_size){
		if(CheckNewOnly==1){
			lastAttachFileID = $('input[name="FileID[]"][@checked]').filter(':last').val();
			if(typeof lastAttachFileID !='undefined' && lastAttachFileID != ''){
				FileDeleteSubmit(lastAttachFileID);
			}
			alert("<?=str_replace("<!--MAX_SIZE-->",sprintf("%d",$SYS_CONFIG['Mail']['MaxAttachmentSize']/1024),$Lang['Gamma']['Warning']['MaxAttachmentSizeExceededAndRemoved'])?>");
		}else{
			alert("<?=str_replace("<!--MAX_SIZE-->",sprintf("%d",$SYS_CONFIG['Mail']['MaxAttachmentSize']/1024),$Lang['email']['WarningLimitAttachmentSize'])?>");
		}
		return false;
	}
	return true;
}

function Show_Attach_List_Link(state){
	var obj = document.getElementById("AttachLinkDiv");
	obj.style.display = state;
}

// passing the field id of textarea like ToAddress, CcAddress, BccAddress
// the Max. Number of Email Address in Address Fields is default set to 350
var max_email_add_num = <?=$MAX_NUM_MAIL_ADDRESS?>;
function Check_Number_Of_Email_Address(field){
	var obj = document.getElementById(field);
	var str = obj.value;
	var count = 0;
	
	if(str != ""){
		var tmp = str.split('');
		if(tmp.length > 0){
			for(var i=0 ; i<tmp.length ; i++){
				if(tmp[i] == '@'){
					count++;
				}
			}
		}
		if(count > max_email_add_num){
		//	alert("The number of Email Address in To, CC, BCC should be less than or equal to 350.");
			return false;
		}
	}
	return true;
}

var EmailAjaxObj = '';
var FormAction = '';	// 0 - Send  ; 1 - Save as Draft
function Ajax_Check_Valid_Email_Address(form_action){	
	FormAction = form_action;
	var toObj = document.getElementById("ToAddress");
	var ccObj = document.getElementById("CcAddress");
	var bccObj = document.getElementById("BccAddress");
	EmailAjaxObj = GetXmlHttpObject();
						    
	if (EmailAjaxObj == null){
		alert (errAjax);
		return;
	}
	
	disable_action_buttons(true);
	var url = "./ajax_check_email_address.php";
	var postContent = "ToAdd="+encodeURIComponent(toObj.value);
	postContent += "&CcAdd="+encodeURIComponent(ccObj.value);
	postContent += "&BccAdd="+encodeURIComponent(bccObj.value);

	EmailAjaxObj.onreadystatechange = Check_Valid_Email_Address;
	EmailAjaxObj.open("POST", url, true);
	EmailAjaxObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	EmailAjaxObj.send(postContent);
}

function Check_Valid_Email_Address(){
//alert(EmailAjaxObj.readyState)
	if (EmailAjaxObj.readyState==4) {
		var responseValue = EmailAjaxObj.responseText;
		var responseArray = responseValue.split("|=|");
		var ValidTo, ValidCc, ValidBcc, InvalidTo, InvalidCc, InvalidBcc, Result;
		var PopMsg = "";
		Result = responseArray[0];
		InvalidTo = responseArray[1];
		InvalidCc = responseArray[2];
		InvalidBcc = responseArray[3];
		ValidTo = responseArray[4];
		ValidCc = responseArray[5];
		ValidBcc = responseArray[6];

		/* if(Result == "1"){
			Check_Submit(document.NewMailForm, FormAction);
		   } else if(Result == "0"){
			PopMsg += "There are invalid e-mail addresses in the to/cc/bcc fields.\n";
			if (InvalidTo != "") {
				PopMsg += "To:\n";
				PopMsg += "\t"+InvalidTo+"\n\n";
			}
			if (InvalidCc != "") {
				PopMsg += "Cc:\n";
				PopMsg += "\t"+InvalidCc+"\n\n";
			}
			if (InvalidBcc != "") {
				PopMsg += "Bcc:\n";
				PopMsg += "\t"+InvalidBcc+"\n\n";
			}
			PopMsg += "Do you wish to remove these and send the mail?";
			if (confirm(PopMsg)) {
				document.getElementById("ToAddress").value = ValidTo;
				document.getElementById("CcAddress").value = ValidCc;
				document.getElementById("BccAddress").value = ValidBcc;
				Check_Submit(document.NewMailForm, FormAction);
			} 
			else
				return false; 
		} */
		
		if (Result==1) {
			Check_Submit(document.NewMailForm, FormAction);
		} else {
			PopMsg += "<?=$Lang['Warning']['MailFormatWarning']?>";
			if (InvalidTo != "") {
				PopMsg += "To:\n";
				PopMsg += "\t"+InvalidTo+"\n\n";
			}
			if (InvalidCc != "") {
				PopMsg += "Cc:\n";
				PopMsg += "\t"+InvalidCc+"\n\n";
			}
			if (InvalidBcc != "") {
				PopMsg += "Bcc:\n";
				PopMsg += "\t"+InvalidBcc+"\n\n";
			}
			alert(PopMsg);
			disable_action_buttons(false);
			return false;
		}
	}
}

function jSet_User_Interface_Style()
{
	if(document.all){		// for IE
		document.getElementById("ToAddress").setAttribute("rows", 4);
		document.getElementById("CcAddress").setAttribute("rows", 4);
		document.getElementById("BccAddress").setAttribute("rows", 4);
	} else {				// non IE
	}
}

function showCc(str)
{
	var row = "#"+str+"row";
	var addrow = "#add"+str+"row";
	$(row).show();
	$(addrow).hide();
	$("#delimiter").hide();
	if(str.toLowerCase()=='cc') $('#CcAddress').focus();
	if(str.toLowerCase()=='bcc') $('#BccAddress').focus();
}

function RemoveEmailInSelection(Textfield)
{
	var SelName = Textfield+"Select";
	var EmailStr = $("#"+Textfield).val();
	var EmailArr = EmailStr.split(";");
	$("#"+SelName).children(":selected").each(function(){
		for(i=0;i<EmailArr.length;i++){
			if(EmailArr[i].indexOf($(this).val())>=0)
				EmailArr.splice(i,1);
		}
		$(this).remove();
	});
	$("#"+Textfield).val(EmailArr.join(";"));
}

function resetForm(obj){
    var CurMenu = $('input#CurMenu').val();
    var CurTag = $('input#CurTag').val();
    var Folder = $('input#Folder').val();
    var UID = $('input#UID').val();
    var DraftUID = $('input#DraftUID').val();
    var OriDraftUID = $('input#OriDraftUID').val();
    var MailAction = $('input#MailAction').val();
    var ActionUID = $('input#ActionUID').val();
    var PartNumber = $('input#PartNumber').val();
    var q = '?CurMenu='+CurMenu+'&CurTag='+CurTag+'&Folder'+Folder+'&UID='+UID+'&DraftUID='+DraftUID+'&OriDraftUID='+OriDraftUID+'&MailAction='+MailAction+'&ActionUID='+ActionUID+'&PartNumber='+PartNumber;
    afterClickedLinksButtons();
    window.location.href='<?=$_SERVER['PHP_SELF']?>'+q;
}

function FCKeditor_OnComplete(EDITOR){
	$(document).ready(function(){
		//EDITOR.SetHTML($('div#MailBodyContent').html());
		//$('div#MailBodyContent').remove();
		
		EDITOR.SetHTML($('textarea#MailBodyContent').val());
		$('textarea#MailBodyContent').remove();
	});
}
//-->
</script>
<form id="NewMailForm" name="NewMailForm" method="POST" onsubmit="return false;">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<?=$xmsg?>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td>
				<?=$IMap->Get_Compose_Mail_Form($EmailContent,$MailAction,$Folder,$signature,$emailAddr,$eventID,$CampusMailID,$internalUseArray)?>
				</td>
			</tr>
			<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1" /></td></tr>
			<tr>
				<td align="right">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_send, "button", "Ajax_Check_Valid_Email_Address(0);","SendBtn"," tabindex='8' ") ?>
					<?= $linterface->GET_ACTION_BTN($button_save_as_draft, "button", "Ajax_Check_Valid_Email_Address(1);","SaveBtn"," tabindex='9' ") ?>
					<?= $linterface->GET_ACTION_BTN($button_reset, "button", "resetForm(document.NewMailForm)","CancelBtn"," tabindex='10' ") ?>
					</td>
				</tr>
				</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>

<input type=hidden id="CurMenu" name="CurMenu" value="<?=$CurMenu?>">
<input type=hidden id="CurTag" name="CurTag" value="<?=$CurTag?>">
<input type=hidden id="Folder" name="Folder" value="<?=$Folder?>">
<input type=hidden id="UID" name="UID" value="<?=$UID?>">
<input type=hidden id="DraftUID" name="DraftUID" value="<?=$DraftUID?>">
<input type=hidden id="OriDraftUID" name="OriDraftUID" value="<?=$OriDraftUID?>">
<input type=hidden id="MailAction" name="MailAction" value="<?=$MailAction?>">
<input type=hidden id="ActionUID" name="ActionUID" value="<?=$ActionUID?>">
<input type=hidden id="PartNumber" name="PartNumber" value="<?=$PartNumber?>">
<input type=hidden id="pageNo" name="pageNo" value="<?=$pageNo?>">
<input type=hidden id="reverse" name="reverse" value="<?=$reverse?>">
<input type=hidden id="sort" name="sort" value="<?=$sort?>">
<input type=hidden id="FromSearch" name="FromSearch" value="<?=$FromSearch?>">
<input type=hidden id="IsAdvanceSearch" name="IsAdvanceSearch" value="<?=$IsAdvanceSearch?>">
<input type=hidden id="FileInputNumber" name="FileInputNumber" value="0">


<input type=hidden id="keyword" name="keyword" value="<?=$keyword?>">

<input type=hidden id="KeywordFrom" name="KeywordFrom" value="<?=$KeywordFrom?>">
<input type=hidden id="KeywordTo" name="KeywordTo" value="<?=$KeywordTo?>">
<input type=hidden id="KeywordCc" name="KeywordCc" value="<?=$KeywordCc?>">
<input type=hidden id="KeywordSubject" name="KeywordSubject" value="<?=$KeywordSubject?>">
<input type=hidden id="FromDate" name="FromDate" value="<?=$FromDate?>">
<input type=hidden id="ToDate" name="ToDate" value="<?=$ToDate?>">

<input type=hidden id="ComposeTime" name="ComposeTime" value="<?=$ComposeTime?>">
</form>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autogrowtextarea.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/base64.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.css" type="text/css" />
<script>
// jquery extension for focus input text field to the end
$.fn.setCursorPosition = function(position){
    if(this.length == 0) return this;
    return $(this).setSelection(position, position);
}

$.fn.setSelection = function(selectionStart, selectionEnd) {
    if(this.length == 0) return this;
    input = this[0];

    if (input.createTextRange) {
        var range = input.createTextRange();
        range.collapse(true);
        range.moveEnd('character', selectionEnd);
        range.moveStart('character', selectionStart);
        range.select();
    } else if (input.setSelectionRange) {
        input.focus();
        input.setSelectionRange(selectionStart, selectionEnd);
    }

    return this;
}

$.fn.focusEnd = function(){
    this.setCursorPosition(this.val().length);
}
// end of jquery extension

// jquery autocomplete function 

var AutoCompleteObj;
function Init_JQuery_AutoComplete(InputID,UserID,UserType) {
	AutoCompleteObj = $("#"+InputID).autocomplete("ajax_search_email.php",
		{  			
			onItemSelect: function () {
				//$("#"+InputID).focus();
				$("#"+InputID).focusEnd();
			},
			formatItem: function(row) {
				return row[1] ;
			},
			maxItemsToShow: 50,
			minChars: 1,
			delay: 0,
			autoFill: false,
			forGamma: true,
			overflow: true,
			divheight: 150
		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Update_Auto_Complete_Extra_Para() {
	var ExtraPara = new Array();
	ExtraPara['user_id'] = <?=$UserID?>;
	ExtraPara['UserType'] = '<?=$CurUserType?>';
	AutoCompleteObj[0].autocompleter.setExtraParams(ExtraPara);
}

function selectItem(li) {

		if (!li) {
			li = document.createElement("li");
			li.extra = [];
			li.selectValue = "";
		}
		var v = $.trim(li.selectValue ? li.selectValue : li.innerHTML);
		input.lastSelected = v;
		prev = v;
		$results.html("");
		$input.val(v);
		hideResultsNow();
		if (options.onItemSelect) setTimeout(function() { options.onItemSelect(li) }, 1);
};

var file_uploader = {};
function initUploadAttachment(containerId,buttonId)
{
	var container = $('#' + containerId);
	var btn = $('#' + buttonId);
	
	if(container.length == 0) return;
	
	file_uploader = new plupload.Uploader({
	        runtimes : 'html5,flash,silverlight,html4,gears,browserplus',
	        browse_button : buttonId,
	        container : containerId,
			flash_swf_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.flash.swf',
			silverlight_xap_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.silverlight.xap',
			url : 'plupload_process.php?ComposeTime=<?=$ComposeTime?>',
			max_file_size : '<?=($SYS_CONFIG['Mail']['MaxAttachmentSize'] / 1024)?>mb',
			chunk_size : '1mb',
			unique_names : <?=($is_mobile_tablet_platform?"true":"false")?>
	    });
	    
	file_uploader.init();
	
	file_uploader.bind('BeforeUpload', function(up, file){
		FileCount += 1; // global var
	});
	
	file_uploader.bind('FilesAdded', function(up, files) {
		
        $.each(files, function(i, file) {
            $('#'+containerId).append(
                '<div id="' + file.id + '">' +
                file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
            '</div>');
        });
 		
        up.refresh(); // Reposition Flash/Silverlight
        up.start();
    });
    
    file_uploader.bind('UploadProgress', function(up, file) {
	        $('#' + file.id + " b").html(file.percent + "%");
	});
	/* 
    file_uploader.bind('Error', function(up, err) {
        $('#'+containerId).append("<div>Error: " + err.code +
            ", Message: " + err.message +
            (err.file ? ", File: " + err.file.name : "") +
            "</div>"
     	);
 
        up.refresh(); // Reposition Flash/Silverlight
    });
    */
    file_uploader.bind('UploadComplete', function(up, files) {
    	// WAF workaround
    	$("#MailBody").attr("disabled", "disabled");
		var postData = $('form#NewMailForm').serialize();
		$("#MailBody").removeAttr("disabled");
		var fileCount = files.length;
		files.reverse();
		ajax_update_attachment(fileCount, postData, files);
		//up.destroy();
	});
}

function ajax_update_attachment(numberLeft, postData, files)
{
	var index = numberLeft - 1;
	if(index >= 0) {
		var finalPostData = postData;
		<?php if($is_mobile_tablet_platform){ ?>
		finalPostData += '&upload_file_name=' + encodeURIComponent(files[index]['target_name']);
		<?php }else{ ?>
		finalPostData += '&upload_file_name=' + encodeURIComponent(files[index]['name']);
		<?php } ?>
		$.post(
			'plupload_attach_update.php',
			finalPostData,
			function(data){
				eval(data);
				if(file_uploader.removeFile) {
					$('#' + files[index].id).remove();
					file_uploader.removeFile(files[index]);
				}
				FileCount -= 1; // global var
				numberLeft -= 1;
				if(numberLeft > 0) {
					ajax_update_attachment(numberLeft, postData, files);
				}
			}
		);
	}
}

function displayUploadPanel(containerId,buttonId)
{
	var container = $('#' + containerId);
	var btn = $('#' + buttonId);
	
	if(container.length > 0){
	
		container.html('');
		container.pluploadQueue({

			runtimes : 'html5,flash,silverlight,html4,gears,browserplus',
			flash_swf_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.flash.swf',
			silverlight_xap_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.silverlight.xap',
			url : 'plupload_process.php?ComposeTime=<?=$ComposeTime?>',
			max_file_size : '<?=($SYS_CONFIG['Mail']['MaxAttachmentSize'] / 1024)?>mb',
			chunk_size : '1mb',
			unique_names : <?=($is_mobile_tablet_platform?"true":"false")?>,
			init : {
				UploadComplete: function(up, files) {
					// WAF workaround
					$("#MailBody").attr("disabled", "disabled");
					var postData = $('form#NewMailForm').serialize();
					$("#MailBody").removeAttr("disabled");
					var fileCount = files.length;
					files.reverse();
					ajax_update_attachment(fileCount, postData, files);
					container.hide();
					btn.val('<?=$Lang['Gamma']['ShowUploadAttachmentWidget']?>');
				},
				BeforeUpload : function(up, file) {
					FileCount += 1; // global var
				}
			}
		});
		
		if(container.is(':visible')) {
			container.hide();
			btn.val('<?=$Lang['Gamma']['ShowUploadAttachmentWidget']?>');
		}else{
			container.show();
			btn.val('<?=$Lang['Gamma']['HideUploadAttachmentWidget']?>');
		}
	}
}

var image_uploader = {};
function initUploadImage(containerId,buttonId)
{
	var container = $('#' + containerId);
	var btn = $('#' + buttonId);
	
	if(container.length == 0) return;
	
	image_uploader = new plupload.Uploader({
	        runtimes : 'html5,flash,silverlight,html4,gears,browserplus',
	        browse_button : buttonId,
	        container : containerId,
			flash_swf_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.flash.swf',
			silverlight_xap_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.silverlight.xap',
			url : 'plupload_process.php?ComposeTime=<?=$ComposeTime?>',
			max_file_size : '<?=($SYS_CONFIG['Mail']['MaxAttachmentSize'] / 1024)?>mb',
			chunk_size : '1mb',
			unique_names : <?=($is_mobile_tablet_platform?"true":"false")?>,
			filters : [{title:'Image files',extensions:'jpg,jpeg,gif,png,bmp,tiff'}] 
	    });
	    
	image_uploader.init();
	
	image_uploader.bind('BeforeUpload', function(up, file){
		FileCount += 1; // global var
	});
	
	image_uploader.bind('FilesAdded', function(up, files) {
		
        $.each(files, function(i, file) {
            $('#'+containerId).append(
                '<div id="' + file.id + '">' +
                file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
            '</div>');
        });
 		
        up.refresh(); // Reposition Flash/Silverlight
        up.start();
    });
    
    image_uploader.bind('UploadProgress', function(up, file) {
	        $('#' + file.id + " b").html(file.percent + "%");
	});
	/* 
    image_uploader.bind('Error', function(up, err) {
        $('#'+containerId).append("<div>Error: " + err.code +
            ", Message: " + err.message +
            (err.file ? ", File: " + err.file.name : "") +
            "</div>"
     	);
 
        up.refresh(); // Reposition Flash/Silverlight
    });
    */
    image_uploader.bind('UploadComplete', function(up, files) {
    	// WAF workaround
    	$("#MailBody").attr("disabled", "disabled");
		var postData = $('form#NewMailForm').serialize();
		$("#MailBody").removeAttr("disabled");
		var fileCount = files.length;
		files.reverse();
		ajax_update_embed_image(fileCount, postData, files);
		//up.destroy();
	});
}

function ajax_update_embed_image(numberLeft, postData, files)
{
	var index = numberLeft - 1;
	if(index >= 0) {
		var finalPostData = postData;
		<?php if($is_mobile_tablet_platform){ ?>
		finalPostData += '&upload_file_name=' + encodeURIComponent(files[index]['target_name']);
		<?php }else{ ?>
		finalPostData += '&upload_file_name=' + encodeURIComponent(files[index]['name']);
		<?php } ?>
		$.post(
			'plupload_upload_image.php',
			finalPostData,
			function(data){
				eval(data);
				if(image_uploader.removeFile) {
					$('#' + files[index].id).remove();
					image_uploader.removeFile(files[index]);
				}
				FileCount -= 1; // global var
				numberLeft -= 1;
				if(numberLeft > 0) {
					ajax_update_embed_image(numberLeft, postData, files);
				}
			}
		);
	}
}

function format_filename(filename){
	return filename.replace(/&amp;/g,'&');
}

function disable_action_buttons(disable)
{
	$('#SendBtn').attr('disabled',disable);
	$('#SaveBtn').attr('disabled',disable);
	$('#CancelBtn').attr('disabled',disable);
}
</script>

<script>
$().ready(function(){
	jSet_User_Interface_Style();
	Init_JQuery_AutoComplete("ToAddress",<?=$UserID?>,<?=$UserType?>);
	Init_JQuery_AutoComplete("CcAddress",<?=$UserID?>,<?=$UserType?>);
	Init_JQuery_AutoComplete("BccAddress",<?=$UserID?>,<?=$UserType?>);
	$("#ToAddress").focus();
<?php
if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
{
?>
	//$('#MailBody').autoGrow();
<?php
}else if(!$is_below_IE8){
?>
	initUploadImage('DivUploadImagePanel','BtnAddImage');
	initUploadAttachment('DivUploadPanel','BtnAddAttachment');
<?php } ?>
	
	bindClickEventToElements();
});

var isCloseWindow = true;
function afterClickedLinksButtons()
{
	isCloseWindow = false;
	setTimeout(function(){isCloseWindow=true;},1);
}
function bindClickEventToElements()
{
	$('a').bind('click',afterClickedLinksButtons);
}
window.onbeforeunload=function(){
	if(isCloseWindow <?php if(!$is_chrome){ ?> && confirm('<?=$Lang['Gamma']['ConfirmMsg']['ConfirmSaveDraftBeforeClosingWindow']?>') <?php } ?>){
		autoSave(true);
	}
	return;
};

</script>
<?php	
	intranet_closedb();
	//$linterface->FOCUS_ON_LOAD("MailBody");
	$linterface->LAYOUT_STOP();
?>
