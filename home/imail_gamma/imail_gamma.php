<?php # Page Modifying by:
$PageStartTime = date("H:i:s") . " " . microtime();
/*
*
* This is a page for testing imail without getting cache and Follow the UI of TG
* Gamma v0.0.09 
*
*/
$PathRelative = "../../";

include_once($PathRelative."includes/global.php");
include_once($PathRelative."includes/libdb.php");
include_once($PathRelative."includes/libdbtable.php");
include_once($PathRelative."includes/imap_gamma.php");
include_once($PathRelative."includes/imap_gamma_ui.php");

intranet_auth();
intranet_opendb();

//$CurSubFunction = "Communication";
//$CurPage = $_SERVER["REQUEST_URI"];


//include_once($PathRelative."src/include/class/startup.php");


//if (empty($_SESSION['SSV_USERID'])) {
//	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
//	exit;
//}

//AuthPage(array("Communication-Mail-GeneralUsage"));


//include_once($PathRelative."src/include/class/imap_gamma.php");
//include_once($PathRelative."src/include/class/imap_gamma_ui.php");
//include_once($PathRelative."src/include/class/user_preference.php");
//include_once($PathRelative."src/include/template/general_header.php");

# Logging Purpose
$PageStartTime = Get_Timer();
####################################################################################

# Libarary
$UPref = new user_preference($_SESSION['SSV_USERID']);
$lui  = new imap_gamma_ui();
$IMap = new imap_gamma();

$lui->logTime['List_Mail_Page'] = $PageStartTime;

# Get the User Preference of Page Info
$numPerPageOld  = $UPref->MailPerPage;


## Get Data 		
$CurMenu 	 = '1';	
$CurTag 	 = (isset($CurTag)) ? stripslashes(urldecode($CurTag)) : $SYS_CONFIG['Mail']['FolderPrefix'];
$keyword 	 = (isset($keyword) && $keyword != "") ? stripslashes(trim($keyword)) : "";
$Msg 		 = (isset($Msg) && $Msg != "") ? $Msg : "";
	
# Mail Data
//$mailbox 	 = (isset($mailbox) && $mailbox != "") ? stripslashes($mailbox) : "Inbox";
$displayMode = (isset($displayMode) && $displayMode != "") ? $displayMode : "folder";
$Folder 	 = (isset($Folder) && $Folder != "") ? stripslashes(urldecode($Folder)) : "";

# Page Info Data
$sort 		 = (isset($sort) && $sort != "") ? $sort : "SORTARRIVAL";
$reverse 	 = (isset($reverse) && $reverse != "") ? $reverse : 1;
$pageNo 	 = (isset($pageNo) && $pageNo != "") ? $pageNo : 1;
$numPerPage  = (isset($numPerPage) && $numPerPage != "") ? $numPerPage : 0;
$currentPage = (isset($currentPage) && $currentPage != "") ? $currentPage : 0;

# for read mail use only - old gamma version?
$messageSeq	 = (isset($messageSeq) && $messageSeq != "") ? $messageSeq : "";

# Update the NumPerPage of User Preference
if($numPerPageOld != "" && $numPerPageOld > 0){
	if($numPerPage != $numPerPageOld){
		if($numPerPage != "" && $numPerPage > 0){
			$UPref->Set_User_Mail_Per_Page($numPerPage);
		} else {
			$numPerPage = $numPerPageOld;
		}
	}
} else {
	$numPerPage = 50;
}


# Assign to PageInfo 
$imapInfo = array();
$imapInfo['pageInfo']['pageNo'] 	= $pageNo;
$imapInfo['pageInfo']['numPerPage'] = $numPerPage;

##############################################################################
# Imap use only
if (!$IMap->ExtMailFlag) {
	if($IMap->ConnectionStatus == true){
		$lui->logTime['Imap_Reopen_Mailbox'] = Get_Timer();
		imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));		# Go to the Corresponding Folder Currently selected
		Write_Message_Log('Gamma', "IMAP_Reopen_Mailbox: ".(Get_Timer() - $lui->logTime['Imap_Reopen_Mailbox']));
	
	
		$imap_obj 	   = imap_check($IMap->inbox);
		$unseen_status = imap_status($IMap->inbox, "{".$IMap->host."}".$Folder, SA_UNSEEN);
		$statusInbox   = imap_status($IMap->inbox, "{".$IMap->host."}Inbox", SA_UNSEEN);
	
		$imapInfo['imap_stream']   = $IMap->inbox;
		$imapInfo['server']    	   = $IMap->host;
		$imapInfo['password'] 	   = $IMap->CurUserPassword;
		$imapInfo['port']  	   	   = $IMap->mail_server_port;
		$imapInfo['username']  	   = $IMap->CurUserAddress;
		$imapInfo['imap_obj'] 	   = $imap_obj;
		$imapInfo['unseen_status'] = $unseen_status;
		$imapInfo['statusInbox']   = $statusInbox;
		$imapInfo['displayMode']   = $displayMode;
		$imapInfo['sort'] 		   = $sort;
		$imapInfo['reverse'] 	   = $reverse;
		//$imapInfo['currentPage']   = $currentPage;	//	old pagination with gamma approach
		$imapInfo['currentPage']   = ($pageNo - 1);	//	new pagination with gamma approach
		$imapInfo['messageSeq']    = $messageSeq;
		$imapInfo['messageNo']     = $messageNo;
	
		# Can be ignored
		$imapInfo['mailbox']   	   = $Folder;			// orig: mailbox
	
		##############################################################################
	
		### Main
		echo $lui->Get_Div_Open('module_imail',  'class="module_content"');
		# Top Bar
		echo $lui->Get_Sub_Function_Header($Lang['email_gamma']['HeaderTitle'], $Msg);
		echo $lui->Get_Switch_MailBox();
		# Left Menu
		echo $lui->Get_Left_Sub_Mail_Menu($CurMenu, $CurTag, $IMap, $Folder, $imapInfo, $keyword);
	
		# Right Menu
		echo $lui->Get_Div_Open("imail_right");
		echo $lui->Get_Form_Open("form1", "post");
		echo $lui->Get_Right_Mail_List($CurMenu, $CurTag, $IMap, $Folder, $imapInfo, $keyword);
	
		# Hidden Values
		echo $lui->Get_Input_Hidden("Folder", "Folder", urlencode($Folder))."\n";
		echo $lui->Get_Input_Hidden("CurTag", "CurTag", urlencode($CurTag))."\n";
		echo $lui->Get_Input_Hidden("numPerPage", "numPerPage", $numPerPage)."\n";
		echo $lui->Get_Input_Hidden("DestinationFolder", "DestinationFolder")."\n";
		echo $lui->Get_Input_Hidden("sort", "sort", $sort)."\n";
		echo $lui->Get_Input_Hidden("reverse", "reverse", $reverse)."\n";
		echo $lui->Get_Input_Hidden("pageNo", "pageNo", $pageNo)."\n";
	
		echo $lui->Get_Input_Hidden("displayMode", "displayMode", $displayMode)."\n";
		echo $lui->Get_Input_Hidden("refreshFlag", "refreshFlag", 1)."\n";
	
		echo $lui->Get_Form_Close();	# End form1
	
		echo $lui->Get_Div_Close();		# End imail_right
		echo $lui->Get_Div_Close();		# End module_imail
	
		imap_close($IMap->inbox);
	}
	else 
	{
		if ($_SESSION['SSV_USER_TYPE'] == 'S') {
			$x = $lui->Get_Div_Open("module_imail", 'class="module_content"');
			$x .= $lui->Get_Sub_Function_Header($Lang['email']['HeaderTitle'],$Msg);
			$x .= $lui->Get_Div_Open("commontabs_board", 'class="bulletin_board"');
			$x .= $lui->Get_Div_Open('','class="load_error" style="margin:10px; padding:10px;"');
			$x .= $Lang['email']['StudentCantConnect'].'<br>';
			$x .= $lui->Get_Table_Open("","100%","",0,0,2);
			$x .= $lui->Get_Table_Row_Open();
			$x .= $lui->Get_Table_Cell_Open('','width="50%" align="center"');
			$x .= $Lang['SysMgr']['MailManagement']['MailBoxPassword'].': ';
			$x .= $lui->Get_Input_Password("MailPassword","MailPassword","",'class="textbox" style="width:100px;"');
			$x .= $lui->Get_Input_Button("","",$Lang['btn_go'],'onclick="Go_Login_Gmail();"');
			$x .= $lui->Get_Table_Cell_Close();
			
			$x .= $lui->Get_Table_Cell_Open('','width="50%" align="center"');
			$x .= $lui->Get_Input_Button("","",$Lang['email']['ActivateGmail'],'onclick="Go_Activate_Gmail_Account();"');
			$x .= $lui->Get_Table_Cell_Close();
			$x .= $lui->Get_Table_Row_Close();
			$x .= $lui->Get_Table_Close();
			$x .= $lui->Get_Div_Close();
			$x .= $lui->Get_Br();
			$x .= $lui->Get_Div_Close();
			$x .= $lui->Get_Div_Close();
			
			echo $x;
		}
		else {
			echo $lui->Get_Div_Open("module_imail", 'class="module_content"');
			echo $lui->Get_Sub_Function_Header($Lang['email_gamma']['HeaderTitle'],$Msg);
			echo $lui->Get_Switch_MailBox();
			echo $lui->Get_Br('style="clear:both"');
			echo $lui->Get_Div_Open("commontabs_board", 'class="bulletin_board"');
			echo $lui->Get_Div_Open('','class="load_error"');
			echo $Lang['email']['CantConnect'];
			echo $lui->Get_Div_Close();
			echo $lui->Get_Br();
			echo $lui->Get_Div_Close();
			echo $lui->Get_Div_Close();
		}
	}
}
else {
	echo $lui->Get_Div_Open("module_imail", 'class="module_content"');
	echo $lui->Get_Sub_Function_Header($Lang['email_gamma']['HeaderTitle'],$Msg);
	echo $lui->Get_Switch_MailBox();
	echo $lui->Get_Br('style="clear:both"');
	echo $lui->Get_Div_Open("commontabs_board", 'class="bulletin_board"');
	echo $lui->Get_Div_Open('','class="load_error"');
	echo $Lang['email']['NotMailServerAddress'];
	echo $lui->Get_Div_Close();
	echo $lui->Get_Br();
	echo $lui->Get_Div_Close();
	echo $lui->Get_Div_Close();
}

####################################################################################
include_once($PathRelative."src/include/template/general_footer.php");
# Logging Purpose
Write_Message_Log('Gamma', "Page of List Mail Total: ".(Get_Timer() - $lui->logTime['List_Mail_Page']));

?>
<script>
function Go_Login_Gmail() {
	var MailPassword = document.getElementById('MailPassword').value;
	var OrgHTML = document.getElementById('commontabs_board').innerHTML;
  LoginGmailAjax = GetXmlHttpObject();
  
  if (LoginGmailAjax == null)
  {
    alert (errAjax);
    return;
  } 
  
  var url = "/src/module/email/ajax_gmail_login.php";
  var postContent = "GmailPassword=" + MailPassword;

  LoginGmailAjax.onreadystatechange = function() {
  	if (LoginGmailAjax.readyState==1) 
  	{
  		document.getElementById('commontabs_board').innerHTML = '<img width="16" height="16" border="0" align="absmiddle" src="<?=$SYS_CONFIG['Image']['Abs']?>ajax_loader_transparent.gif"/>';
  	}
  	else if (LoginGmailAjax.readyState==4)
	  {
	  	Response = LoginGmailAjax.responseText;
	  	if (Response.Trim() == "1") {
	  		window.location.href = 'imail_gamma.php';
	  	}
	  	else {
	  		document.getElementById('commontabs_board').innerHTML = OrgHTML;
	  	}
	  }
  };
  LoginGmailAjax.open("POST", url, true);
	LoginGmailAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	LoginGmailAjax.send(postContent);
}

function Go_Activate_Gmail_Account() {
	var GmailLoginSite = 'https://www.google.com/a/<?=$SYS_CONFIG['StudMail']['UserNameSubfix']?>/ServiceLogin?service=CPanel&continue=https%3A%2F%2Fwww.google.com%3A443%2Fa%2Fcpanel%2F<?=$SYS_CONFIG['StudMail']['UserNameSubfix']?>%2FDashboard&passive=true&Email=<?=substr($_SESSION['SSV_LOGIN_EMAIL'],0,strpos($_SESSION['SSV_LOGIN_EMAIL'],'@'))?>';
	
	window.open(GmailLoginSite,'');
}

function findPosition( oElement ) {
  if( typeof( oElement.offsetParent ) != 'undefined' ) {
    for( var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent ) {
      posX += oElement.offsetLeft;
      posY += oElement.offsetTop;
    }
    return [ posX, posY ];
  } else {
    return [ oElement.x, oElement.y ];
  }
}


function Email_Preview(Obj,SeenFlag){
	var divPreview = document.getElementById('divPreview');
	// alert(Obj.href);
	
	var ObjPosition = findPosition(Obj);
	
	wordXmlHttp = GetXmlHttpObject();
    
	if (wordXmlHttp == null)
	{
		alert (errAjax);
		return;
	} 
	
	var ObjHref = Obj.href.split('?');
	var url = '<?=$PathRelative?>src/module/email/ajax_cache_preview_email.php';
	var postContent = ObjHref[1];
		postContent += '&SeenFlag=' + SeenFlag;
	divPreview.innerHTML = '<img src="/theme/image_en/ajax_loading_indicator.gif"  width="16"  height="16" style="position: relative; top: 50%; left:100px" />';
	divPreview.style.left = ObjPosition[0];
	divPreview.style.top = (ObjPosition[1] + 15);
	divPreview.style.display = '';
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.onreadystatechange = function() {
		var ResponseText = "";
		if (ResponseText.substr(0,4) == "0|=|") {
			window.top.location = '<?=($PathRelative."login.php?CurPage=".$PathRelative."src/module/email/cache_index.php")?>&Msg=' + ResponseText.substring(4);
		}
		else {
			if (wordXmlHttp.readyState == 4) {
				divPreview.innerHTML = wordXmlHttp.responseText;
				// setTimeout("Close_Email_Preview()",5000);
			}
		}
	}
	wordXmlHttp.send(postContent);
	
	
	

}

function Close_Email_Preview() {
	var divPreview = document.getElementById('divPreview');
		divPreview.style.display = 'none';
}
</script>