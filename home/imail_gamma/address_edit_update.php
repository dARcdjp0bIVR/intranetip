<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccess.php");
intranet_auth();
intranet_opendb();
auth_campusmail();

$li = new libdb();

$TargetName = trim($TargetName);
$TargetAddress = trim($TargetAddress);

$is_email_valid = intranet_validateEmail($TargetAddress);

$TargetName = intranet_htmlspecialchars($TargetName);
$TargetAddress = intranet_htmlspecialchars($TargetAddress);

$success = $is_email_valid;
if($is_email_valid){
	$sql = "UPDATE INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL SET TargetName = '$TargetName',
	        TargetAddress = '$TargetAddress', DateModified = now()
	        WHERE AddressID='$AddressID' AND OwnerID ='$UserID'";
	
	$success = $li->db_db_query($sql);
}
intranet_closedb();
$anc = $special_option['no_anchor'] ?"":"#anc";
$xmsg = ($success? "update":"update_failed"); 
header("Location: addressbook.php?field=$field&order=$order&pageNo=$pageNo&keyword=$keyword&xmsg=$xmsg&TabID=$TabID".$anc);
?>