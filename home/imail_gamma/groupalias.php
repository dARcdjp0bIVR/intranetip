<?php
// editing by 
/*************************************** Changes *******************************************
 * 
 * 2016-04-07 (Carlos): Added [Export] for [Internal Recipient Group].
 * 2015-07-28 (Shan): Added export button   
 * 2012-04-05 (Carlos): Added Import button for External Group
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
 

intranet_auth();
intranet_opendb();
auth_campusmail();

$anc = $special_option['no_anchor'] ?"":"#anc";

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPageArr['iMail'] = 1;
if($TabID==1)
	$CurrentPage = "PageAddressBook_InternalReceipientGroup";
else
	$CurrentPage = "PageAddressBook_ExternalReceipientGroup";
        
# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;        

$lwebmail	= new libwebmail();
$IMap = new imap_gamma();
$linterface = new interface_html();

### Check WebMail ###
//if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
//{
//    include_once($PATH_WRT_ROOT."includes/libsystemaccess.php");
//    $lsysaccess = new libsystemaccess($UserID);
//    if ($lsysaccess->hasMailRight())
//    {
//        $noWebmail = false;
//    }
//    else
//    {
//        $noWebmail = true;
//    }
//}
//else
//{
//    $noWebmail = true;
//}

# Block No webmail no address book
if ($noWebmail and $TabID<>1)
{
    header("Location: groupalias.php?aliastype=0&TabID=1".$anc);
    exit();
}

if($field=="") $field = 0;
if($order=="") $order = 1;
$li = new libdbtable2007($field, $order, $pageNo);
$keyword = trim($keyword);
if ($aliastype != 1) $aliastype = 0;
if ($aliastype==1)
{
    $tablename = "INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL";
    $viewpage = "groupalias_view_ex.php";
}
else
{
    $tablename = "INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL";
    $viewpage = "groupalias_view_in.php";
}

if ($countSyn!=1)
{
    # Syn the count of alias
    $ldb = new libdb();
    $sql = "SELECT AliasID FROM $tablename WHERE OwnerID = '$UserID'";
    $groups = $ldb->returnVector($sql);
   
    if (sizeof($groups)!=0)
    {
        $list = implode(",",$groups);
        $entry_table = ($aliastype==1?"INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY":"INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY");
        if($aliastype==1){
	        $sql = "SELECT AliasID, COUNT(EntryID) FROM $entry_table WHERE AliasID IN ($list)
	                GROUP BY AliasID
	                ";
        }else{
        	//$sql = "SELECT AliasID,COUNT(EntryID) FROM $entry_table WHERE AliasID IN ($list) 
	        //        GROUP BY AliasID
	        //        ";
	        $sql = "SELECT AliasID, COUNT(EntryID) FROM $entry_table WHERE (EntryID IN
					(
					SELECT e.EntryID FROM $entry_table as e 
					INNER JOIN INTRANET_USER as u ON u.UserID = e.TargetID 
					WHERE AliasID IN ($list) AND e.RecordType = 'U' AND u.IMapUserEmail IS NOT NULL AND u.IMapUserEmail != '')
					OR RecordType = 'G' )
					AND AliasID IN ($list)
					GROUP BY AliasID ";
        }
        $temp = $ldb->returnArray($sql,2);
        $counts = build_assoc_array($temp);
        for ($i=0; $i<sizeof($groups); $i++)
        {
             $id = $groups[$i];
             $updatedCount = $counts[$id];
             if ($updatedCount!="")
             {
                 $sql = "UPDATE $tablename SET NumberOfEntry = '$updatedCount' WHERE AliasID = '$id'";
                 $ldb->db_db_query($sql);
             }
             else
             {
                 $sql = "UPDATE $tablename SET Count = 0 WHERE AliasID = '$id'";
                 $ldb->db_db_query($sql);
             }
        }
    }
}

if($aliastype==1)
{
	$sql  = "SELECT
               NumberOfEntry,
               CONCAT('<a class=\"tablelink\" href=\"$viewpage?TabID=$TabID&aliastype=$aliastype&AliasID=',AliasID,'$anc\">',AliasName,'</a>'),
               Remark,
               NumberOfEntry,
               DateModified,
               CONCAT('<input type=\"checkbox\" name=\"AliasID[]\" value=\"', AliasID ,'\">')
          FROM $tablename
          WHERE
               OwnerID = '$UserID' AND
               (AliasName like '%".$li->Get_Safe_Sql_Like_Query(htmlspecialchars(stripslashes($keyword),ENT_QUOTES))."%'
                )
          ";
} else {
	/*
	$sql  = "SELECT
               a.NumberOfEntry,
               CONCAT('<a class=\"tablelink\" href=\"$viewpage?TabID=$TabID&aliastype=$aliastype&AliasID=',a.AliasID,'$anc\">',a.AliasName,'</a>'),
               a.Remark,
               count(c.UserID) + count(d.GroupID) + count(e.GroupID) as NumberOfEntry, 
               a.DateModified,
               CONCAT('<input type=\"checkbox\" name=\"AliasID[]\" value=\"', a.AliasID ,'\">')
          FROM 
          		INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL as a
          		left join INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as b on (b.AliasID = a.AliasID)
				LEFT JOIN INTRANET_USER AS c ON (b.TargetID = c.UserID and b.RecordType='U')
				LEFT JOIN INTRANET_GROUP AS d ON (b.TargetID = d.GroupID and b.RecordType='G' and d.RecordType!=3)
				LEFT JOIN INTRANET_CLASS AS e ON (b.TargetID = e.GroupID and b.RecordType='G')
          WHERE
               a.OwnerID = $UserID AND
               (a.AliasName like '%$keyword%'
                )
			group by 
				a.AliasID
          ";
	*/
	/*// Commented on 2010-08-16
	$sql  = "SELECT
               a.NumberOfEntry,
               CONCAT('<a class=\"tablelink\" href=\"$viewpage?TabID=$TabID&aliastype=$aliastype&AliasID=',a.AliasID,'$anc\">',a.AliasName,'</a>'),
               a.Remark,
               count(c.UserID) + count(d.GroupID) as NumberOfEntry, 
               a.DateModified,
               CONCAT('<input type=\"checkbox\" name=\"AliasID[]\" value=\"', a.AliasID ,'\">')
          FROM 
          		INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL as a
          		left join INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as b on (b.AliasID = a.AliasID)
				LEFT JOIN INTRANET_USER AS c ON (b.TargetID = c.UserID and b.RecordType='U')
				LEFT JOIN INTRANET_GROUP AS d ON (b.TargetID = d.GroupID and b.RecordType='G' and d.RecordType!=3)
          WHERE
               a.OwnerID = $UserID AND
               (a.AliasName like '%$keyword%'
                )
			group by 
				a.AliasID
          ";
    */
	$sql  = "SELECT
               a.NumberOfEntry,
               CONCAT('<a class=\"tablelink\" href=\"$viewpage?TabID=$TabID&aliastype=$aliastype&AliasID=',a.AliasID,'$anc\">',a.AliasName,'</a>'),
               a.Remark,
               a.NumberOfEntry as NumberOfEntry, 
               a.DateModified,
               CONCAT('<input type=\"checkbox\" name=\"AliasID[]\" value=\"', a.AliasID ,'\">')
          FROM 
          		INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL as a
          		left join INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as b on (b.AliasID = a.AliasID)
				LEFT JOIN INTRANET_USER AS c ON (b.TargetID = c.UserID and b.RecordType='U')
				LEFT JOIN INTRANET_GROUP AS d ON (b.TargetID = d.GroupID and b.RecordType='G' and d.RecordType!=3)
          WHERE
               a.OwnerID = '$UserID' AND
               (a.AliasName like '%".$li->Get_Safe_Sql_Like_Query(htmlspecialchars(stripslashes($keyword),ENT_QUOTES))."%'
                )
			group by 
				a.AliasID
          ";
}

$li->field_array = array("AliasName","Remark","NumberOfEntry","DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->IsColOff = "imail_addressbook";
$li->title = "";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class='tabletoplink' width='15'>#</td>\n";
$li->column_list .= "<td class='tabletoplink' width='20'>&nbsp;</td>\n";
$li->column_list .= "<td class='tabletoplink' width='25%'>".$li->column($pos++, $Lang['Gamma']['GroupName'])."</td>\n";
$li->column_list .= "<td class='tabletoplink' width='25%'>".$li->column($pos++, $Lang['Gamma']['GroupRemark'])."</td>\n";
$li->column_list .= "<td class='tabletoplink' width='25%'>".$li->column($pos++, "$i_CampusMail_New_NumberOfEntry")."</td>\n";
$li->column_list .= "<td class='tabletoplink' width='25%'>".$li->column($pos++, $i_LastModified)."</td>\n";
$li->column_list .= "<td class='tabletoplink' width='25'>".$li->check("AliasID[]")."</td>\n";
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0);

### Title ###
/*
$iMailImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_address_". ($TabID==1 ? "in" : "ex") ."group.gif' align='absmiddle' />";
$iMailTitle1 = "<span class='imailpagetitle'>". ($TabID==1 ? $i_CampusMail_Internal_Recipient_Group : $i_CampusMail_External_Recipient_Group) ."</span>";
$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$iMailImage1.$iMailTitle1."</td><td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>";

$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($iMailTitle, "", 0);
*/
$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($i_CampusMail_External_Recipient_Group, $PATH_WRT_ROOT."home/imail_gamma/groupalias.php?aliastype=1&TabID=3", $TabID==1?0:1);
$TAGS_OBJ[] = array($i_CampusMail_Internal_Recipient_Group, $PATH_WRT_ROOT."home/imail_gamma/groupalias.php?aliastype=0&TabID=1", $TabID==1?1:0);

### Button / Tag
$AddBtn 	= "<a href='groupalias_new.php?aliastype=$aliastype&TabID=$TabID$anc' class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'AliasID[]','groupalias_remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'AliasID[]','groupalias_edit.php$anc')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$searchTag 	.= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".htmlspecialchars(stripslashes($keyword),ENT_QUOTES)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag .= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
$searchTag 	.= "</tr></table>";

if($aliastype==1){
	$ImportBtn 	= "<a href=\"address_group_contact_import.php?aliastype=$aliastype&TabID=$TabID$anc\" class=\"contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_import . "</a>";
	$ExportBtn 	= "<a href=\"address_group_contact_export.php?aliastype=$aliastype&TabID=$TabID$anc\" class=\"contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_export . "</a>";
}else{
	$ExportBtn 	= "<a href=\"address_group_contact_export.php?aliastype=$aliastype&TabID=$TabID$anc\" class=\"contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_export . "</a>";
}

$linterface->LAYOUT_START();


?>
<br />
<form name="form1" method="get" action= "" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr> 
					<td>
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
        						<td><?=$AddBtn?><?=($aliastype==1?' '.$ImportBtn:'')?> <?=' '.$ExportBtn?> </td>
        					</tr>
        					</table>
        				</td>
        				<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
        			</tr>
        			</table>
        		</td>
        	</tr>
                <tr class="table-action-bar">
        		<td><?=$searchTag?></td>
        		<td align="right" valign="bottom">
        			<table border="0" cellspacing="0" cellpadding="0">
        			<tr>
        				<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
        				<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
        					<table border="0" cellspacing="0" cellpadding="2">
        					<tr>
                                                        <td nowrap><?=$delBtn?></td>
							<td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5" /></td>
							<td nowrap><?=$editBtn?></td>
						</tr>
        					</table>
        				</td>
        				<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
        			</tr>
        			</table>
        		</td>
        	</tr>
                <tr>
				<td colspan="2"><?=$li->display()?></td>  
        	</tr>
                </table>
        </td>
</tr>
</table>        

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

<input type="hidden" name="numToAdd" value="" />
<input type="hidden" name="countSyn" value="1" />
<input type="hidden" name="aliastype" value="<?=$aliastype?>" />

<input type="hidden" name="TabID" value="<?=$TabID?>" />
</form>                        

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
