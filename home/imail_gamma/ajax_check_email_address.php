<?php
// page modifing by: 
/*
 * 2016-09-26 (Danny ): Fix a problem, when user type invalid email eg ".@abc.com" in BCC email, and valid CC / TO, no Alert Popup to remind for wrong email format
 * 2015-05-29 (Carlos): replace comma separator to semi-colon separator with Replace_Display_Comma() for the To, Cc, Bcc values.
 * 2015-04-17 (Carlos): Use new method to validate recipients.
 */
$PATH_WRT_ROOT = "../../";
//$CurSubFunction = "Communication";
//$CurPage = $_SERVER["REQUEST_URI"];
//$SkipAutoRedirect = true;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

################################################################################

$IMap = new imap_gamma(true);

# Get Data
$ToAdd  = (isset($ToAdd) && trim($ToAdd) != "") ? trim($ToAdd) : "";
$CcAdd  = (isset($CcAdd) && trim($CcAdd) != "") ? trim($CcAdd) : "";
$BccAdd = (isset($BccAdd) && trim($BccAdd) != "") ? trim($BccAdd) : "";

//$ToAdd = '\'"ABBISS, Jo-Ann"\' <joann@mail.island.edu.hk>;"AN, Kam Fung" <ken@mail.island.edu.hk>;"BALL, Angela" <angela.ball@mail.island.edu.hk>;"BERESFORD, Therese" <beresfordt1@is-tg.broadlearning.com>;"BOWD, Joanna" <joanna@mail.island.edu.hk>;"CHAN, Lai Har" <laihar@mail.island.edu.hk>;"CHAN, Man Kwong" <marco@mail.island.edu.hk>;"CHENG, Mei Shan" <gwenyth@mail.island.edu.hk>;"CHENG, Pui Shan" <alfreda@mail.island.edu.hk>;"CHO, Wing Sau" <winnie@mail.island.edu.hk>;"CHOW, Ngan Yan" <annette@mail.island.edu.hk>;"CORRIGAL, James" <james.corrigal@mail.island.edu.hk>;"FONG, Yeuk Heung" <winny@mail.island.edu.hk>;"HAU, Wai Hung Vivian" <vivian@mail.island.edu.hk>;"IRWIN, Priscilla" <priscilla@mail.island.edu.hk>;"KOMAR, Mark" <mark.komar@mail.island.edu.hk>;"KWONG, Wai Hong" <m.kwong@mail.island.edu.hk>;"LAI, Annie" <annie.lai@mail.island.edu.hk>;"LAM, Ting Kin" <lam@mail.island.edu.hk>;"LAU, Yee Nee" <megan@mail.island.edu.hk>;"LAW, Chi Hong" <chihong@mail.island.edu.hk>;"LEE, Sheung Hung" <shlee@mail.island.edu.hk>;"LEUNG, Ming Leung" <kenny@mail.island.edu.hk>;"LEUNG, winnie" <>;"LEUNG, Yau Hang" <andy@mail.island.edu.hk>;"LI, Jackie" <jackie.li@is-tg.broadlearning.com>;"LI, Michelle" <michelle.li@mail.island.edu.hk>;"LIANG, Wen Yang" <william@mail.island.edu.hk>;"MA, Sik Ching" <johanna@mail.island.edu.hk>;"MAN, Yu Keung" <michael@mail.island.edu.hk>;"MARTIN, Nathan" <nathan.martin@mail.island.edu.hk>;"MATSUDA, Michiko" <machiko@mail.island.edu.hk>;"NG, Lidia Joa" <lidia.ng@mail.island.edu.hk>;"O\'SULLIVAN, elizabeth" <elizabeth.o\'sullivan@mail.island.edu.hk>;"PRESHNER, Barry" <barry@mail.island.edu.hk>;"PTA, Administrator" <pta@mail.island.edu.hk>;"PTA, Book Shop" <pta.book@mail.island.edu.hk>;"QUINTANA LLUCH, Olga Maria" <olga@mail.island.edu.hk>;"RAJEEV, Deepti" <rajeevd2@mail.island.edu.hk>;"RAYMOND, Barry John" <barryr@mail.island.edu.hk>;"RYAN, Anna" <anna.ryan@mail.island.edu.hk>;"SACHDEV, Suman" <suman@mail.island.edu.hk>;"SAINT-PE, Emilie" <emilie@mail.island.edu.hk>;"SANDHU, Dilip" <dilip@mail.island.edu.hk>;"SCOTT, Tracy" <tracy.scott@mail.island.edu.hk>;"SEN, Daniel Marcus" <daniel@mail.island.edu.hk>;"SHEPPARD, Roberta" <roberta@mail.island.edu.hk>;"SRIVASTAVA, Shweta" <shweta.srivastava@mail.island.edu.hk>;"TANG, Fung Kwan" <teresa@mail.island.edu.hk>;"TANG, Wai Wan" <raymond@mail.island.edu.hk>;"testing1, testing" <testing1@is-tg.broadlearning.com>;"testing2, testing" <testing2@is-tg.broadlearning.com>;"testing3, testing" <testing3@is-tg.broadlearning.com>;"TIN, Kam Wah" <kamwah@mail.island.edu.hk>;"TIN, Sun Kong" <sunkong@mail.island.edu.hk>;"TONG, Hung Kit" <angus@mail.island.edu.hk>;"TSANG, Ka Chun Arion" <arion@mail.island.edu.hk>;"TSANG, King Ho" <king@mail.island.edu.hk>;"TSANG, Yik Chi" <gigi@mail.island.edu.hk>;"TSU, Betty" <betty@mail.island.edu.hk>;"URQUHART, Kim Keow" <kim@mail.island.edu.hk>;"WAI, Chi Cheong" <alex@mail.island.edu.hk>;"WAN, Chor Fong" <celia@mail.island.edu.hk>;"WONG, albert" <>;"WONG, Ho Kar Emily" <emily@mail.island.edu.hk>;"WONG, Rose" <rose.wong@mail.island.edu.hk>;"YAM, Kim" <kim.yam@mail.island.edu.hk>;"YEUNG, Mei Chui Kamy" <swkamy@mail.island.edu.hk>;"YIP, Judy" <judy@mail.island.edu.hk>;"YIP, Tsz Ching" <rebecca@mail.island.edu.hk>;"YIP, wai yee" <waiyee.yip@mail.island.edu.hk>;"YIP, Yin Ho" <jennifer@mail.island.edu.hk>;';
//echo $ToAdd.'<br>';
# Trim the slash added by POST format
$ToAdd = stripslashes($ToAdd);
$ToAdd = Replace_Display_Comma($ToAdd);

$CcAdd = stripslashes($CcAdd);
$CcAdd = Replace_Display_Comma($CcAdd);

$BccAdd = stripslashes($BccAdd);
$BccAdd = Replace_Display_Comma($BccAdd);

# Convert  Semi-colon to Comma 
//$ToAdd = str_replace(";", ",", $ToAdd);
//$CcAdd = str_replace(";", ",", $CcAdd);
//$BccAdd = str_replace(";", ",", $BccAdd);

# Initialization
$result = array();
$ToList = array();
$CcList = array();
$BccList = array();

# Invalid Mail Message
$ToInvalid = "";
$CcInvalid = "";
$BccInvalid = "";

# Valid Mail Message
$ToValid = "";
$CcValid = "";
$BccValid = "";

# function 
{
	function Check_Valid_Email_Address($list,$Field){		
		global $ToValid, $CcValid, $BccValid, $ToInvalid, $CcInvalid, $BccInvalid;
		
		$isValid = 1;
		$EmailReg = "/^([a-zA-Z0-9&\._-]+)@([^\(\);:,<>]+\.[a-zA-Z]{2,4})/";
	
		# Definition of ERROR which may be given by  imap_rfc822_parse_adrlist() - a php function
		$AddressErrorMsg = array('UNEXPECTED_DATA_AFTER_ADDRESS','.SYNTAX-ERROR.','INVALID_ADDRESS');
		
		if(count($list) > 0){
			for ($i=0; $i< sizeof($list); $i++) {
				$Temp = imap_rfc822_parse_adrlist($list[$i],"");
				if(in_array($Temp[0]->mailbox, $AddressErrorMsg) || $Temp[0]->mailbox == null || 
				   in_array($Temp[0]->host, $AddressErrorMsg) || $Temp[0]->host == null || 
				   in_array($Temp[0]->personal, $AddressErrorMsg) || !preg_match($EmailReg,$Temp[0]->mailbox.'@'.$Temp[0]->host)){	
					
					if ($Field == "To") {
						$ToInvalid .= $list[$i].", ";
					}
					else if ($Field == "Cc") {
						$CcInvalid .= $list[$i].", ";
					}
					else if ($Field == "Bcc") {
						$BccInvalid .= $list[$i].", ";
					}
					
					$isValid = 0;
			    /*echo "  mailbox : " . $Temp[0]->mailbox . "<br>";
			    echo "  host    : " . $Temp[0]->host . "<br>";
			    echo "  personal: " . $Temp[0]->personal . "<br>";
			    echo "  adl     : " . $Temp[0]->adl . "<br>";*/
				}
				else {
					if ($Field == "To") {
						$ToValid .= imap_rfc822_write_address($Temp[0]->mailbox,$Temp[0]->host,$Temp[0]->personal).", ";
					}
					else if ($Field == "Cc") {
						$CcValid .= imap_rfc822_write_address($Temp[0]->mailbox,$Temp[0]->host,$Temp[0]->personal).", ";
					}
					else if ($Field == "Bcc") {
						$BccValid .= imap_rfc822_write_address($Temp[0]->mailbox,$Temp[0]->host,$Temp[0]->personal).", ";
					}
				}
			}
			
			if ($Field == "To") {
				$ToValid = substr($ToValid,0,strlen($ToValid)-2);
				$ToInvalid = substr($ToInvalid,0,strlen($ToInvalid)-2);
			}
			else if ($Field == "Cc") {
				$CcValid = substr($CcValid,0,strlen($CcValid)-2);
				$CcInvalid = substr($CcInvalid,0,strlen($CcInvalid)-2);
			}
			else if ($Field == "Bcc") {
				$BccValid = substr($BccValid,0,strlen($BccValid)-2);
				$BccInvalid = substr($BccInvalid,0,strlen($BccInvalid)-2);
			}
		}
		return ($isValid == 1) ? true : false;
	}
	
	/*
	function Replace_Display_Comma($AddressList) {
		$QuotePairs = array();
		$FirstDoubleQuote = "";
		$FirstSingleQuote = "";

		for ($i=0; $i< strlen($AddressList); $i++) {
			if ($AddressList[$i] == '"') {
				if ($FirstDoubleQuote === "") {
					$FirstDoubleQuote = $i;
				}
				else {
					$QuotePairs[] = array($FirstDoubleQuote,$i+1);
					
					if ($FirstSingleQuote > $FirstDoubleQuote && $FirstSingleQuote < $i && $FirstSingleQuote != "") {
						$FirstSingleQuote = "";
					}
					$FirstDoubleQuote = "";
				}
			}
			else if ($AddressList[$i] == "'") {
				if ($FirstSingleQuote === "") {
					$FirstSingleQuote = $i;
				}
				else {
					$QuotePairs[] = array($FirstSingleQuote,$i+1);
					
					if ($FirstDoubleQuote > $FirstSingleQuote && $FirstDoubleQuote < $i && $FirstDoubleQuote != "") {
						$FirstDoubleQuote = "";
					}
					$FirstSingleQuote = "";
				}
			}
		}
		
		for ($i=0; $i< sizeof($QuotePairs); $i++) {
			for ($j=$QuotePairs[$i][0]; $j< $QuotePairs[$i][1]; $j++) {
				if ($AddressList[$j] == ",") $AddressList[$j] = "|";
			}
		}
		
		$Result = explode(",",$AddressList);
		
		for ($i=0; $i< sizeof($Result); $i++) {
			$Result[$i] = str_replace("|",",",$Result[$i]);				
		}
		
		return $Result;
	}
	*/
}

# Main
# ToAddress

if($ToAdd != ""){
//	if(strrpos($ToAdd, ",") == strlen($ToAdd)-1){
//		$ToAdd = substr($ToAdd, 0, strlen($ToAdd)-1);
//	}
//	$AddressArray = Replace_Display_Comma($ToAdd);
	
	$to_result = $IMap->splitEmailStringIntoArray($ToAdd, ';', false);
	$AddressArray = $to_result[0];
	$invalid_to_ary = $to_result[1];

	$result['ToAdd'] = Check_Valid_Email_Address($AddressArray,"To");
	for($i=0;$i<count($invalid_to_ary);$i++){
		if($ToInvalid != ''){
			$ToInvalid .= ", ".$invalid_to_ary[$i];
		}else{
			$ToInvalid .= $invalid_to_ary[$i];
		}
	}
	if(count($invalid_to_ary)>0){
		$result['ToAdd2'] = false;
	}
}

# CcAddress
if($CcAdd != ""){
//	if(strrpos($CcAdd, ",") == strlen($CcAdd)-1){
//		$CcAdd = substr($CcAdd, 0, strlen($CcAdd)-1);
//	}
//	$AddressArray = Replace_Display_Comma($CcAdd);

	$cc_result = $IMap->splitEmailStringIntoArray($CcAdd, ';', false);
	$AddressArray = $cc_result[0];
	$invalid_cc_ary = $cc_result[1];

	$result['CcAdd'] = Check_Valid_Email_Address($AddressArray,"Cc");
	for($i=0;$i<count($invalid_cc_ary);$i++){
		if($CcInvalid != ''){
			$CcInvalid .= ", ".$invalid_cc_ary[$i];
		}else{
			$CcInvalid .= $invalid_cc_ary[$i];
		}
	}
	if(count($invalid_cc_ary)>0){
		$result['CcAdd2'] = false;
	}
}

# BccAddress
if($BccAdd != ""){
//	if(strrpos($BccAdd, ",") == strlen($BccAdd)-1){
//		$BccAdd = substr($BccAdd, 0, strlen($BccAdd)-1);
//	}
//	$AddressArray = Replace_Display_Comma($BccAdd);

	$bcc_result = $IMap->splitEmailStringIntoArray($BccAdd, ';', false);
	$AddressArray = $bcc_result[0];
	$invalid_bcc_ary = $bcc_result[1];

	$result['BccAdd'] = Check_Valid_Email_Address($AddressArray,"Bcc");
	for($i=0;$i<count($invalid_bcc_ary);$i++){
		if($BccInvalid != ''){
			$BccInvalid .= ", ".$invalid_bcc_ary[$i];
		}else{
			$BccInvalid .= $invalid_bcc_ary[$i];
		}
	}
	if(count($invalid_bcc_ary)>0){
		$result['BccAdd2'] = false;
	}
}

# Output 
echo (!in_array(false, $result)) ? "1|=|": "0|=|";
echo $ToInvalid."|=|";
echo $CcInvalid."|=|";
echo $BccInvalid."|=|";
echo $ToValid."|=|";
echo $CcValid."|=|";
echo $BccValid."|=|";
################################################################################
?>