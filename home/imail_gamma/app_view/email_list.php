<?php
/********************
 * change log:
 * 2020-11-05  Ray
 *                        add $_SESSION['SSV_EMAIL_LOGIN'], $_SESSION['SSV_EMAIL_PASSWORD']
 *
 * 2020-09-29  Ray
 *                        add $_SESSION['emailAppType']
 *
 * 2020-09-21  Ray
 *                        add pageBackCheck
 *
 * 2020-08-04  Ray
 *                        add empty email text
 *
 * 2020-03-04  Sam        [Z181214] Draft receiver becomes system admin if not set
 *
 * 2020-02-24  Sam        [S180470] Fail to display "System Admin" as sender.
 *                        Show "System Admin" as sender name when $senderID == 0
 *
 * 2019-03-22  Tiffany    [P157879] Fail to load new mails.
 *                        Change the $(window).height() to window.innerHeight for $(window).height() is not correct for some device.
 *                        Add Math.floor for that test some phone that get the $(window).scrollTop() with Decimal point
 *******************/
$PATH_WRT_ROOT = '../../../';

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

include_once ($PATH_WRT_ROOT ."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libpwm.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp_imail.php');

include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

//$uid = IntegerSafe($_GET['uid']);
//
//if ($_SESSION['UserID']==''){
//	$UserID = $uid;
//	$_SESSION['UserID'] = $uid;
//}

intranet_auth();
intranet_opendb();

$libDb = new libdb();
$libeClassApp = new libeClassApp();
$libpwm= new libpwm();
$linterface = new interface_html();
$emailClassApp = new emailClassApp();
$folderAssoAry ="";
$defaultFolder = array();
$personalFolder = array();

$uid = $_SESSION['UserID'];


if(isset($AppType)) {
	$_SESSION['emailAppType'] = $AppType;
} else {
	$AppType = $_SESSION['emailAppType'];
}

//$token = $_GET['token'];
//$ul = $_GET['ul'];
$passwordAry = $libpwm->getData($uid);
foreach((array)$passwordAry as $_folderType => $_folderKey){
	if($_folderType==$uid)$password=$_folderKey;
}

//	$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
//	if (!$isTokenValid) {
//		die($Lang['Gamma']['App']['NoAccessRight']);
//	}
    $thisFolder = $_GET["TargetFolderName"];
	$_displayName = $Lang['Gamma']['SystemFolderName'][$thisFolder];
	if($_displayName==''){
		$_displayName = $thisFolder;
	}

	$myTrashIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_trash_white.png"."\"";
	$myComposeIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_edit.png"."\"";
	$myOutboxIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_folder_open_white_36dp.png"."\"";
	$myAttachmentIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_attachment_20px.png"."\"";
	$importantFlagUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/icon_important.gif"."\"";
	$notificationFlagUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/icon_notification.gif"."\"";
	$refreshIconUrl =$linterface->Get_Ajax_Loading_Image($noLang=true);

if ($plugin['imail_gamma']) {
    $isImailGamma = 1;
    $curUserId = $uid;
    $sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$curUserId."'";
	$dataAry = $libDb->returnResultSet($sql);

	if(isset($_SESSION['SSV_EMAIL_LOGIN']) && isset($_SESSION['SSV_EMAIL_PASSWORD'])) {
		$iMapEmail = $_SESSION['SSV_EMAIL_LOGIN'];
		$iMapPwd = $_SESSION['SSV_EMAIL_PASSWORD'];
	} else {
		$iMapEmail = $dataAry[0]['ImapUserEmail'];
		$iMapPwd = $password;
	}

    $IMap = new imap_gamma(false,$iMapEmail, $iMapPwd);
    /*url for selected email's content*/
    $FolderForLink =  urldecode($IMap->decodeFolderName($thisFolder));
    $FolderForLink = urlencode($IMap->encodeFolderName(stripslashes($FolderForLink)));

    if($thisFolder!="INBOX"){
    	$Folder = "INBOX.".$thisFolder;
    }else{
    	$Folder = $thisFolder;
    }

  $_displayName = IMap_Decode_Folder_Name($_displayName);
	
$sort = "SORTARRIVAL";
$displayMode = (isset($displayMode) && $displayMode != "") ? $displayMode : "folder";
$Folder = (isset($Folder) && $Folder != "") ? stripslashes($Folder) : $IMap->InboxFolder;
$order 	 = (isset($order) && $order != "") ? $order : 1;
$CacheNewMail = false;

if (!$IMap->ExtMailFlag) {
	if($IMap->ConnectionStatus == true){
		$imap_reopen_success = imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));	
		if($Folder == $IMap->SpamFolder) {
			// Connect Junk box twice to let mail server auto correct read/unread status
			$imap_reopen_success = imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));
		   }
		   
		if(!$imap_reopen_success){
			sleep(1);
			imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));
		}

		$imapInfo['imap_stream']   = $IMap->inbox;
		$imapInfo['server']    	   = $IMap->host;
		$imapInfo['password'] 	   = $IMap->CurUserPassword;
		$imapInfo['port']  	   	   = $IMap->mail_server_port;
		$imapInfo['username']  	   = $IMap->CurUserAddress;
		$imapInfo['displayMode']   = $displayMode;
		$imapInfo['sort'] 		   = $sort;
		$imapInfo['reverse'] 	   = $order;
		# Can be ignored
		$imapInfo['mailbox']   	   = $Folder;// orig: mailbox

		$isDraftInbox = ($Folder == $IMap->DraftFolder) ? 1 : 0;
		$isOutbox = ($Folder == $IMap->SentFolder) ? 1 : 0;
		$array = $IMap->Get_Sort_Index($IMap, $imapInfo, $search, $MailStatus);

		$imapInfo['pageInfo']['totalCount'] = (is_array($array)) ? count($array) : 0;
		$imapInfo['pageInfo']['pageStart']  = ($pageSize * ($imapInfo['pageInfo']['pageNo'] - 1) + 1);
		//This logic is the same as the emailGenerator.php, generate 20 emails once. 
		$startIndex = 0;
		$emailAryTotalCount = $imapInfo['pageInfo']['totalCount'];
		if($emailAryTotalCount >($startIndex+20)){
			$isEmailMoreThanOnePage = 1;
			$endIndex = ($startIndex+20);
		}else{
			$isEmailMoreThanOnePage = 0;
			$endIndex = $emailAryTotalCount;
		}	
		
		for ($i= $startIndex ; $i<$endIndex; $i++) {
			$MailHeaderList[] = imap_headerinfo($IMap->inbox, $array[$i], 80, 80);
			$msgno[] = $array[$i];
		}
		$emailListHtml = "";
        $MailHeaderListcounter = count($MailHeaderList);

  		$MailFlags = $IMap->GetMailPriority($msgno,$Folder);
        //Loop For First Page     
   		for($i= 0 ; $i<$MailHeaderListcounter; $i++){
			$contentAry = $MailHeaderList[$i]; 
	        //SUBJECT    
	        
	        $subject = $IMap->MIME_Decode($contentAry->subject); 
		 	if ($subject != "") {
				$subjectPrint = (substr($subject, 0, 10) == "=?UTF-8?Q?") ? substr($subject, 10, -2) : $subject ;
			} else {
				$subjectPrint = "[NO SUBJECT]";
			}
			$subjectPrint = $AttachmentIcon.htmlspecialchars($subjectPrint);
			$subjectPrint = $IMap->BadWordFilter($subjectPrint);
	       //DATE
	        if($IMap->IsToday(str_ireplace("UT","UTC",$contentAry->date)))
			$DateFormat = "H:i";
			else
			$DateFormat = "Y-m-d";
			$prettydate = date($DateFormat, strtotime(str_ireplace("UT","UTC",$contentAry->date)));
	        //SENDER
	        if($thisFolder=='Drafts'||$thisFolder=='Sent'){
	        $toArray = $contentAry-> to;
	        $_stdClassObject = $toArray[0];
	        }else{        	
	        $fromArray = $contentAry-> from;
	        $_stdClassObject = $fromArray[0];
	        }
	       $mailbox =  $IMap->MIME_Decode( $_stdClassObject->personal);
	        //if not internal user add the email address's name as the title
	       if($mailbox==''){
	       	 $mailbox = $_stdClassObject->mailbox;  
	       } 	
	        //if has no user
	       if($mailbox==''){
	       	$mailbox = "-";
	       }
	       //UID AND ATTACHMENT
	        $currentMessageNo = $contentAry-> Msgno;
	        $currentUID = $IMap->Get_UID ($currentMessageNo);
	        $iscurrentAttachment = $IMap->Check_Any_Mail_Attachment($currentMessageNo);                
	        $_hasAttachment = ($iscurrentAttachment==1)?true:false;
	        $isImportant = $MailFlags[intVal($currentMessageNo)]['ImportantFlag']=== 'true'?true:false;
	      	
	        //SEEN FOR BACKGROUND
	        $unSeen = $contentAry->Unseen;		
	        $_unSeen = ($unSeen=="U")?true:false;    
	        $emailListHtml .= $emailClassApp->getEmailListHtml($_unSeen,$currentUID,$_hasAttachment,$subjectPrint,$mailbox,$prettydate,$myAttachmentIconUrl,$isImportant,$importantFlagUrl);
		}
			}}
}//end gamma
else{
	$isImailGamma = 0;
    $lc = new libcampusquota2007($UserID);
    if($thisFolder =='INBOX'){
		$folderID[0] = 2;
		$senderIDTitle = "SenderID";
	}else if($thisFolder =='Drafts'){
		$folderID[0] = 1;
		$senderIDTitle = "RecipientID";
	}else if($thisFolder =='Sent'){
		$folderID[0] = 0;
		$senderIDTitle = "RecipientID";
	}else if($thisFolder == 'Trash'){
        $folderID[0] = -1;
        $senderIDTitle = "SenderID";
	}else{
	    $sql = "select FolderID from INTRANET_CAMPUSMAIL_FOLDER where OwnerID='$uid' and foldername = '".$libDb->Get_Safe_Sql_Query($thisFolder)."'";
	    $senderIDTitle = "SenderID";
	    $folderID = $libDb->returnVector($sql);
	}
	if( $folderID[0] == -1){
	$sql  = "select CampusMailID,".$senderIDTitle.",SenderEmail,ExternalTo,Subject,DateInput,RecordStatus,IsAttachment,IsImportant,IsNotification from INTRANET_CAMPUSMAIL where UserID ='$uid' and Deleted= '1' order by DateInput";		
	}else{
	$sql  = "select CampusMailID,".$senderIDTitle.",SenderEmail,ExternalTo,Subject,DateInput,RecordStatus,IsAttachment,IsImportant,IsNotification from INTRANET_CAMPUSMAIL where UserID ='$uid' AND (Deleted != '1' or Deleted is null) and UserFolderID= '".$folderID[0]."' order by DateInput";		
	}
	$MailHeaderList = $lc->returnArray($sql);	
	$emailAryTotalCount = count($MailHeaderList);
	$startIndex = $emailAryTotalCount-1;
   if($emailAryTotalCount>20){
   	$isEmailMoreThanOnePage = 1;
   	$endIndex = $emailAryTotalCount-20;
   }else{
   	$isEmailMoreThanOnePage = 0;
   	$endIndex = 0;
   }
   
   	for($i= $startIndex ; $i>=$endIndex; $i--){
	$emailReturnList[] = $MailHeaderList[$i];
	}
    $emailListHtml = "";
	for($i= 0 ; $i<count($emailReturnList); $i++){
		$contentAry = $emailReturnList[$i];
        $subject = $contentAry['Subject']; 
        $date = $contentAry['DateInput']; 
        $currentUID = $contentAry['CampusMailID']; 
        $senderID = str_replace('U', '', $contentAry[$senderIDTitle]);
        $senderEmail = $contentAry['SenderEmail']; 
        $externalTo = $contentAry['ExternalTo'];
        $recordStatus = $contentAry['RecordStatus']; 
        $_unSeen = ($recordStatus=="")?true:false;
        $iscurrentAttachment = $contentAry['IsAttachment'];
        $_hasAttachment = ($iscurrentAttachment ==1)?true:false;
        $isImportant = ($contentAry['IsImportant'] === '1')?true:false;
        $IsNotification = ($contentAry['IsNotification'] === '1')?true:false;

        if(($senderID!=null)&&($senderID!='')){
        	//for intranet receiver
        $sql  = "select ".getNameFieldByLang()."as Name from INTRANET_USER where UserID ='$senderID'";  
        $SenderName = $lc->returnArray($sql);	
        $mailbox = $SenderName[0]['Name'];
        }else{
			    if($thisFolder =='Sent'||$thisFolder =='Drafts'){
		        $pieces = explode(";", $externalTo);
		    	}else{
			    $pieces = explode(" ", $senderEmail);
		    	}
		    	$mailbox =  str_replace(";","",$pieces[0]);
         }
	 	if($mailbox==''){
	 	    if($senderID == '0'){//[Z181214]
				$mailbox = $Lang['AppNotifyMessage']['SystemAdmin'];
			}else{
				$mailbox = "-";
			}
	 	}
	 	if ($subject != "") {
			$subjectPrint = (substr($subject, 0, 10) == "=?UTF-8?Q?") ? substr($subject, 10, -2) : $subject ;
		} else {
			$subjectPrint = "[NO SUBJECT]";
		}

		$DateFormat = "Y-m-d";
        if(date("Y-m-d")==date($DateFormat, strtotime(str_ireplace("UT","UTC",$date)))){
        	$DateFormat = "H:i";
        }
		if($date!=''){
	    $prettydate = date($DateFormat, strtotime(str_ireplace("UT","UTC",$date)));
		}
        $emailListHtml .= $emailClassApp->getEmailListHtml($_unSeen,$currentUID,$_hasAttachment,$subjectPrint,$mailbox,$prettydate,$myAttachmentIconUrl,$isImportant,$importantFlagUrl,$IsNotification,$notificationFlagUrl);
		}
		$FolderForLink = $thisFolder;
       }
    //LINKS
	$composeEmailUrl = "email_reply_forward_page.php?MailAction=Compose&TargetFolderName=".$FolderForLink."&parLang=".$parLang;
	$emailMainUrl = "email_main.php?parLang=".$parLang;
	if ($thisFolder == 'Drafts') {
	$EmailListSelectFormAction = "email_reply_forward_page.php?MailAction=Drafts&TargetFolderName=".$thisFolder."&parLang=".$parLang;
	}
	else {
	$EmailListSelectFormAction = "email_content.php?TargetFolderName=".$FolderForLink ."&parLang=".$parLang;
	}
	$deleteCheckBoxUrl = "email_delete_action_redirection.php?TargetFolderName=".$FolderForLink."&password=".$password."&parLang=".$parLang."&IndividualEmail=0";
   
    $loadingX = $emailClassApp->getLoadingHtml();
    $bgColor = ($sys_custom['eClassTeacherApp']['iMailToolbarColorCode'])? $sys_custom['eClassTeacherApp']['iMailToolbarColorCode'] : '#429DEA';
    if($AppType=="S"){
        $bgColor = "#F8BD10";
    }
    if($AppType=="P"){
        $bgColor = "#16A30D";
    }
    $header = '<div data-role="header"  data-position="fixed" style ="background-color:'.$bgColor.';" >'.
			 $loadingX.
			 '<a  href="#" data-icon="myapp-myOutbox" data-position-to="window" data-role="button" class = "ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon ui-btn-left" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 10px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;" onclick ="getEmailMainPage();"></a>
			  <p  style="text-align:left; margin-left:40px;margin-top: 18px;" class = "ui-li-text-my-title-font">'."&nbsp&nbsp > &nbsp&nbsp".$_displayName.'</p>
			  <div class="ui-btn-right" data-role="controlgroup" data-type="horizontal">
			  <div id = "composeIconDiv" class = "show">
			  <!--<a href="#" id="search" data-role="button" class ="ui-nodisc-icon" data-icon="search" style="top: 15px;"></a>-->
			  <a href="#"  id = "ComposeNewEmailButton" data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline ui-btn-icon-left ui-btn-a ui-nodisc-icon" data-icon="myapp-compose" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;"></a>
			  </div>
			  <div id = "trashIconDiv" class = "hidden">
			  <a href="#"  id = "DeleteEmailsButton" data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline ui-btn-icon-left ui-btn-a ui-nodisc-icon" data-icon="myapp-trash"   style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;"></a>
			  </div></div>
			  </div>';
    $x = '<div data-role="content">
		  <form  name = "delete_email" id = "deleteEmailCheckBoxForm" method="post" action='.$deleteCheckBoxUrl.'>	
		  <fieldset>
		  <div id="myEmailListChooseItems"><ul id="email_list" data-role="listview"  data-inset="false" data-split-icon="gear" data-split-theme="a" data-inset="true">';
    $x .= $emailListHtml.'</ul></div></fieldset></form>';
    $x .= '<div id = "refreshIconDiv" class = "show">'.$refreshIconUrl.'</div>';
    $x .= '<input type="hidden" id ="emailAryTotalCount" name="emailAryTotalCount" value="'.$emailAryTotalCount.'">';
    if($InitLoad == "")
    $InitLoad = 1;
    echo $libeClassApp->getAppWebPageInitStart();
?>
<script>
window.JSON || 
document.write('<script src="//cdnjs.cloudflare.com/ajax/libs/json3/3.2.4/json3.min.js"><\/scr'+'ipt>');
</script>
<script language="javascript">
</script>
<style type="text/css">
    
	.li{
	    height: 20% !important;
	}
	 .ui-li-text-my-title-font{
	 	color:white;
	 	text-shadow: none!important;
	}
	.ui-li-text-my-h6{
		color:black;
		font-size: .75em !important;
	}
	 .ui-li-text-my-h5{
	 	color:black;
		font-size:.90em !important;
		overflow: hidden; !important;
		max-width: 550px; !important;
		white-space: nowrap; !important;
		text-overflow: ellipsis;
	}
	div.my_email_title{
		font-size:1em!important;
		overflow: hidden; !important;
		white-space: nowrap; !important;
		text-overflow: ellipsis;
	    max-width: -webkit-calc(100% - 80px);
	}		
	.ui-icon-myapp-trash {
	 background-color: transparent !important;
	 border: 0 !important;
	 -webkit-box-shadow:none !important;
	 background: url(<?=$myTrashIconUrl?>) no-repeat !important;
	 }
	 
	 .ui-icon-myapp-compose {
	 background-color: transparent !important;
	 border: 0 !important;
	 -webkit-box-shadow:none !important;
	 background: url(<?=$myComposeIconUrl?>) no-repeat !important;
	 }
	 
	 .ui-icon-myapp-myOutbox{			
	 border: 0 !important;
	 -webkit-box-shadow:none !important;
	 background: url(<?=$myOutboxIconUrl?>) no-repeat !important;
	}
	
	div.hidden{
		display:none ;
	}
	div.show{
		display:block ;
	}
	#refreshIconDiv{
	 position:absolute;
	 left:50%;
	 margin:0 0 0 0;
	 background:tranparent!important;
	}
	#refreshIconDiv2{
	     position:absolute;
	     left:50%;
	     margin:10px 0 0 0;
	     background:tranparent!important;
	}
	.unSeen {
    background: white !important;
    font-weight:bold !important;
   }

  .seen {
   background: #f6f6f6 !important; 
   font-weight:normal !important;
	}
   .unSeen_selected {
   background: #D9E6FC !important; 
   }
   .seen_selected {
   background: #D9E6FC !important; 
   }
		
   #loadingmsg {
      color: black;
      background:tranparent!important;
      position: fixed;
      top: 50%;
      left: 50%;
      z-index: 100;
      margin: -8px 0px 0px -8px;
      }
      #loadingover {
      background: white;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }
    .ui-normal-text{font-weight:normal;}    .ui-bold-text{font-weight: bold;}    .ui-content .ui-listview, .ui-panel-inner>.ui-listview { margin:0em}
    .ui-mobile .ui-page { min-height: 300px; overflow: hidden; }
    .container{position: relative;}
    .ui-btn-icon-left:after, .ui-btn-icon-right:after, .ui-btn-icon-top:after, .ui-btn-icon-bottom:after, .ui-btn-icon-notext:after{
    	  background-color: rgba(255, 252, 252, 0.3);
    }
    .ui-page-theme-a .ui-btn.ui-btn-active, html .ui-bar-a .ui-btn.ui-btn-active, html .ui-body-a .ui-btn.ui-btn-active, html body .ui-group-theme-a .ui-btn.ui-btn-active, html head+body .ui-btn.ui-btn-a.ui-btn-active, .ui-page-theme-a .ui-checkbox-on:after, html .ui-bar-a .ui-checkbox-on:after, html .ui-body-a .ui-checkbox-on:after, html body .ui-group-theme-a .ui-checkbox-on:after, .ui-btn.ui-checkbox-on.ui-btn-a:after, .ui-page-theme-a .ui-flipswitch-active, html .ui-bar-a .ui-flipswitch-active, html .ui-body-a .ui-flipswitch-active, html body .ui-group-theme-a .ui-flipswitch-active, html body .ui-flipswitch.ui-bar-a.ui-flipswitch-active, .ui-page-theme-a .ui-slider-track .ui-btn-active, html .ui-bar-a .ui-slider-track .ui-btn-active, html .ui-body-a .ui-slider-track .ui-btn-active, html body .ui-group-theme-a .ui-slider-track .ui-btn-active, html body div.ui-slider-track.ui-body-a .ui-btn-active{
    	background-color:transparent !important;
    }
    

</style>
		<script>
        function pageBackCheck() {
            if(performance.navigation.type == 2) {
                location.reload();
            }
        }

		var deleteCounter = 0;
		
		$(document).ready(function(){

            $("#loadingover").on("pageinit", function (e) {
                var page = $(this);
                pageBackCheck();
            });

            var myCustomEvent = (navigator.userAgent.match('iPhone') != null) ? 'popstate' : 'pageshow';
            $(window).on(myCustomEvent, function(e) {
                pageBackCheck();
            });

			$("#comfirmDeleteEmails").click(function(){
				$("#deleteEmailCheckBoxForm").submit();
				return false;
		  	});  
		  	
		  	$("#ComposeNewEmailButton").bind('mousedown', function(){
		  		 getEmailComposePage();
		  	});
		  	
		  	$("#DeleteEmailsButton").bind('mouseup',function(){
			  	 $("#popupDeleteComfirm").popup('open');
			  	 $("#DeleteEmailsButton").addClass('ui-icon-myapp-trash'); 
		  	});
		  	
		    var PageYOffsetFirstLoad = 0;
		  	$(window).scroll(function(){
		  		if(PageYOffsetFirstLoad == 0){
		  			PageYOffsetFirstLoad = 1;
		  			$(window).scrollTop(0);
		  		}
			  	var isEmailMoreThanOnePage = <?=$isEmailMoreThanOnePage?>;
		  		if(!isEmailMoreThanOnePage){
			  	   $("#refreshIconDiv").hide(); 
			  	}else{
			  	   $("#refreshIconDiv").show(); 
			  	}
			  	<!--detect scroll to bottom-->
                var innerHeight =  window.innerHeight; // the inner height for windows is the height for phone [P157879]
                if((Math.floor($(window).scrollTop()) == $(document).height() - innerHeight)&&($(window).scrollTop()!=0)){
                // if(($(window).scrollTop() == $(document).height() - $(window).height())&&($(window).scrollTop()!=0)){
					var emails = document.getElementsByName("emailListChoseEmailBox[]");
	            	var existEmailNumber = emails.length;
	            	var emailAryTotalCount = $("input#emailAryTotalCount").val();
	            	if(emailAryTotalCount>existEmailNumber){
                        $("#emptyEmailDivText").hide();
	             		getExtraEmails(existEmailNumber,function(obj){
	            			var moreList = '';
	            			var currentEmails = document.getElementsByName("emailListChoseEmailBox[]");
	            			var currentExistEmailNumber = currentEmails.length;
	            			var currentStartIndex = obj.startIndex;
	            			<!--determine whether get the emails more than once, if already attach this email list then do nothing with the current return-->
	           				if(!(currentExistEmailNumber>currentStartIndex)){       
	            			    moreList = GenerateEmailList(obj);   
								<!--append and refresh listview-->
								$('#email_list').append(moreList).listview('refresh');   
	            			}<!--end if get current list more than once-->
                            if(obj.emailArr.length == 0) {
                                $("#emptyEmailDivText").show();
                            }
	            		});<!--end get extra emails-->
	            		checkExternalMail();
	            	}<!--end whether need to retrieve more email-->
	            	else{
	            		$("#refreshIconDiv").hide();
	            	}
				}<!--end if bottom-->
				else{
					<!--detect is popupDeleteComfirm shown-->
					<!--in IOS UIWebview, popup delete Comfirm will triger scroll-->
					var isDeleteEmailsButtonShow = 0;
					if ( $(popupDeleteComfirm).parent().hasClass('ui-popup-active')) {
					   isDeleteEmailsButtonShow	 = 1;
					}
					<!--detect scroll to top-->
					
					var visible = $('#trashIconDiv').is(':visible');					
					if(($(window).scrollTop()==0)&&(isDeleteEmailsButtonShow != 1) &&(!visible)){
						var initLoading = $("input[name='InitLoad']").val();
						if(initLoading != 0){
							$("#refreshIconDiv2").show(); 
							$("#refreshIconDivText").show();
                            $("#emptyEmailDivText").hide();
							getExtraEmails(0,function(obj){
							var newList = GenerateEmailList(obj);	
							$('#email_list').empty();
							$('#email_list').append(newList).listview('refresh');   
							$("#refreshIconDiv2").hide(); 
							$("#refreshIconDivText").hide();
                            if(obj.emailArr.length == 0) {
                                $("#emptyEmailDivText").show();
                            }
							});  
							checkExternalMail();
						}else{
							$("input[name='InitLoad']").val('1');
						}
					}
				}
       		});<!--end scroll-->
            $('#email_list').on('click', 'li', function() {
             <!--check box method step 2-->
             composeAndTrashShow();
             });

       		<?php if($emailListHtml == "") { ?>
            $("#emptyEmailDivText").show();
            <?php } ?>
		});<!--end $(document).ready-->
		
        function AutoCalculateMandateOnChange(){
        	composeAndTrashShow();
        }
        
        function composeAndTrashShow(){
    		 deleteCounter = 0;
    		 <!--loop all checkboxs to check their checked status-->
			 $("input[name='emailListChoseEmailBox[]']").each(function(){
		 	    var checked_status = this.checked;
		 	    var str = '';
		 		var res = '';
			 	 if(checked_status){
			 			$(this).css("background-color", "#4B5FD5");
			 			if(($(this).closest( "li" )[0].className.indexOf("seen") > -1) && ($(this).closest( "li" )[0].className.indexOf("seen_")<=-1)){
			 			   str = $(this).closest( "li" )[0].className;
                           res = str.replace("seen", "seen_selected");
                           $(this).closest( "li" )[0].className = res;
                           
			 			}else if(($(this).closest( "li" )[0].className.indexOf("unSeen") > -1) && ($(this).closest( "li" )[0].className.indexOf("unSeen_")<=-1)){
			 			     str = $(this).closest( "li" )[0].className;
                             res = str.replace("unSeen", "unSeen_selected");
			 				$(this).closest( "li" )[0].className = res;	
			 			}	
			 					
				  		deleteCounter++;
			  	   }else{
			  	      	$(this).css("background-color", "white");
			  	      	if($(this).closest( "li" )[0].className.indexOf("seen_selected") > -1) {
			  	      	    str = $(this).closest( "li" )[0].className;
                            res = str.replace("seen_selected", "seen");
                           $(this).closest( "li" )[0].className = res;
			  	      	}else if($(this).closest( "li" )[0].className.indexOf("unSeen_selected") > -1) {
			  	      		str = $(this).closest( "li" )[0].className;
                            res = str.replace("unSeen_selected", "unSeen");
                           $(this).closest( "li" )[0].className = res;
			  	      	}
			  	   }
			  });
			
			<!--show the trash icon when at least one email is chosen to be deleted,hide when not any email is chosen-->
    		if(deleteCounter==0){
				<!--show send email counter-->
				  $("#composeIconDiv").show();
	              $("#trashIconDiv").hide();
			}else{
				  $("#composeIconDiv").hide();
	              $("#trashIconDiv").show();
			}
			
        }
        function getEmailDetails(inputId)
	  	{
	  	   <!--disable clicking email to content page when at least one email is chosen to be deleted-->
	  	   if(deleteCounter==0){
		  		showLoading();
			  	var emaillink = "<?=$EmailListSelectFormAction."&CurrentUID="?>";
			  	emaillink = emaillink + inputId;
			  	window.location.assign(emaillink);
	  	   }else{
		  	   	<!--check box method step 1-->
		  	   	var checkboxID = "checkbox_"+inputId;
		  	   	var checked_status = document.getElementById(checkboxID).checked;
		  	   	if(checked_status){
		  	   		document.getElementById(checkboxID).checked = false;
		  	   	}else{
		  	   		document.getElementById(checkboxID).checked = true;
		  	   	}
	  	   }
	  	}
	  	
       	function getExtraEmails(StartIndex,callback){     	
       		var uid = "<?=$uid?>";
       		var TargetFolderName = "<?=$thisFolder?>";
 	  		$.post("emailGenerator.php",{StartIndex:StartIndex,uid:uid,TargetFolderName:TargetFolderName },function(data) {
            })
	  		.done(function(data) {
				var obj = JSON.parse(data);	
				$("input#emailAryTotalCount").val(obj.emailAryTotalCount).change();
			 	callback(obj);
		  	});	
       	}  
       	
       	function checkExternalMail(){
       		<!--need to check out external mails manually in imail setting-->
       		var isGamma = <?=$isImailGamma?>;
       		var uid = "<?=$uid?>";
       		if(isGamma==0){
       		$.post("<?=$PATH_WRT_ROOT?>home/imail/checkmail_process2.php",{uid:uid,app:1 },function(data){
       		});       			
       		}
       	}
       
       	function getEmailMainPage(){
       		showLoading();
			window.location.assign("<?=$emailMainUrl?>");
		}
	    function getEmailComposePage(){
       		showLoading();
			window.location.assign("<?=$composeEmailUrl?>");
		}	
	function showLoading() {
		    document.getElementById('loadingmsg').style.display = 'block';
		    document.getElementById('loadingover').style.display = 'block';
		}
    
    function GenerateEmailList(obj){
    	<!--corresponding to method getEmailListHtml in libeClassApp_imail-->
    	var output = '';
		for(var i = 0; i<obj.emailArr.length;i++){
            var currentEmail = obj.emailArr[i];
            var currentUID =  currentEmail.UID;
            var Date = currentEmail.Date;
            var Subject = currentEmail.Subject;
            var Mailbox = currentEmail.Mailbox;
            var UnSeen = currentEmail.UnSeen;
            var IsCurrentHasAttachment = currentEmail.HasAttachment;
            var isCurrenteMailImportant = currentEmail.IsImportant;
            var isCurrentMailNeedNotification = currentEmail.IsNotification;

            var currentBGClass = "seen";
            var fontWeight = "normal";
               if(UnSeen){
               currentBGClass = "unSeen";
               fontWeight = "bold";
               }
            var attachmentLinkIcon = '';
            if(IsCurrentHasAttachment){
            	attachmentLinkIcon = '<input type="image" id ="markEmailAttachment"  src=<?=$myAttachmentIconUrl?> >';
            }
	        var importantIcon = '';
	        if(isCurrenteMailImportant){
	        	importantIcon = '<input type="image" id ="importantEmail"  src=<?=$importantFlagUrl?> >';
	        }
	        var notificationIcon = '';
	        if(isCurrentMailNeedNotification){
	        	notificationIcon =  '<input type="image" id ="notifiedEmail"  src=<?=$notificationFlagUrl?> >';
	        }
            var currentCheckboxDiv = '<div class = "ui-checkbox" style = "margin-top: 0px;margin-bottom: 0px;"><label style="padding: 20px 10px 20px 10px !important;margin: 0px 0px 0px 0px !important;border-width: 0px 1px 0px 0px !important;height:30px;float:left;width:30px;" data-corners="false"><fieldset data-role="controlgroup" style="height:0px !important"><input type="checkbox" data-enhanced="true" id="checkbox_'+currentUID+'" name="emailListChoseEmailBox[]"  style="margin-top: 30px;" value = "'+currentUID+'" onchange = "AutoCalculateMandateOnChange()"/></fieldset></label></div>';
            var herfDiv = '<a href="#" id="'+currentUID+'"  onclick="getEmailDetails(this.id)" style="padding-left: 0px;background: transparent !important;font-weight:'+fontWeight+'!important;"><div class="my_email_title">'+
            importantIcon+notificationIcon+attachmentLinkIcon+Mailbox+'</div><p class = "ui-li-text-my-h5">'+Subject+'</p><p class="ui-li-aside ui-li-text-my-h5" style="right:1em;">'+Date+'</p></a>';            
            output += '<li data-icon="false" class="'+currentBGClass+'" value="'+currentBGClass+'" >'+currentCheckboxDiv+herfDiv+'</li>';          
		}<!--end loop return array-->
		return output;
    }
    
    function scrollWindowTop(){
    	<!--invoked from Android side for dealing situation that list cannot scroll if already top-->
    	var visible = $('#trashIconDiv').is(':visible');
    	if(($(window).scrollTop()==0)&&(($(document).height()-$(window).height())==0)&&(!visible)){
			$("#refreshIconDiv2").show(); 
			$("#refreshIconDivText").show();
            $("#emptyEmailDivText").hide();
			getExtraEmails(0,function(obj){
			var newList = GenerateEmailList(obj);	
			$('#email_list').empty();
			$('#email_list').append(newList).listview('refresh');   
			$("#refreshIconDiv2").hide(); 
			$("#refreshIconDivText").hide();
			if(obj.emailArr.length == 0) {
                $("#emptyEmailDivText").show();
            }
			});  
    	}
    }
    
</script>
	</head>
	<body>
		<div data-role="page" >			
	    <?=$header?>
	    <div id = "refreshIconDiv2" class = "hidden" style="margin-top: 0px;margin-bottom: 0px;"><br><?=$refreshIconUrl?><br></div>
	    <div id="refreshIconDivText" class="hidden" style="text-align:center;height:40px;margin-top: 35px;margin-left: 15px;"><?=$Lang['Gamma']['App']['Updating']?></div>
        <div id="emptyEmailDivText" class="hidden" style="text-align:center;height:40px;margin-top: 35px;margin-left: 15px;"><?=$Lang['General']['NoRecordAtThisMoment']?></div>
        <?=$x?>
        <input type="hidden" name="InitLoad" id="InitLoad" value="<?=$InitLoad?>">
<?php
echo $emailClassApp->getDeleteComfirmPopupHtml();
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>
