<?php
// Editing by 
/*
 * 2020-11-05 (Ray): Add $_SESSION['SSV_EMAIL_LOGIN'], $_SESSION['SSV_EMAIL_PASSWORD']
 * 2017-03-23 (Carlos): Decrement new mail counter for iMail after deleted emails.
 */
$PATH_WRT_ROOT = '../../../';
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libpwm.php");
//$uid = IntegerSafe($_GET['uid']);
//$_SESSION['UserID'] = $uid;
intranet_auth();
intranet_opendb();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<title>email list</title>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
		<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
		<script language="javascript">
		</script>
		<script>
		$(document).ready(function(){
		});
		</script>
	</head>
	<body>
	<div data-role="page">	
<?php


$libDb = new libdb(); 
$libeClassApp = new libeClassApp();
$libpwm= new libpwm();

$uid = $_SESSION['UserID'];

//$token = $_GET['token'];
//$ul = $_GET['ul'];
$parLang = $_GET['parLang'];
$passwordAry = $libpwm->getData($uid);
foreach((array)$passwordAry as $_folderType => $_folderKey){
	if($_folderType==$uid)$password=$_folderKey;
}
//$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
//if (!$isTokenValid) {
//	die($Lang['Gamma']['App']['NoAccessRight']);
//}

$folderAssoAry ="";
$defaultFolder = array();
$personalFolder = array();
$curUserId = $uid;
if ($curUserId == '') {
	$UserID = $uid;
}

	$Uid = array();
	$IndividualEmail = $_GET['IndividualEmail'];
	if($IndividualEmail==0){
    $Uid = $emailListChoseEmailBox;
	}else{
    $Uid[0] = $_GET["CurrentUID"];
	}
    $uidAraycount = count($Uid);	
    $currentFolder= $_GET["TargetFolderName"];

	//imail gamma
	if ($plugin['imail_gamma']) {		
    $sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$curUserId."'";
	$dataAry = $libDb->returnResultSet($sql);

	if(isset($_SESSION['SSV_EMAIL_LOGIN']) && isset($_SESSION['SSV_EMAIL_PASSWORD'])) {
		$iMapEmail = $_SESSION['SSV_EMAIL_LOGIN'];
		$iMapPwd = $_SESSION['SSV_EMAIL_PASSWORD'];
	} else {
		$iMapEmail = $dataAry[0]['ImapUserEmail'];
		$iMapPwd = $password;    // hardcode for trial only
	}
    $IMap = new imap_gamma(false,$iMapEmail, $iMapPwd);
    
      if($currentFolder!="INBOX"){
    	$Folder = "INBOX.".$currentFolder;
       }else{
    	$Folder = $currentFolder;
       }
    
        $Folder = (isset($Folder) && $Folder != "") ? stripslashes($Folder) : $IMap->InboxFolder;

	 for($i=0; $i<$uidAraycount;$i++)	
		{
			if(strstr($Uid[$i],","))
				list($thisUid,$thisFolder) = explode(",",$Uid[$i]);
			else
			{
				$thisUid = $Uid[$i];
				$thisFolder = $Folder;
			}
			if($currentFolder =='Trash'){
			 $Result['DeleteMail'] = $IMap->deleteMail($IMap->TrashFolder, $thisUid);	
			}else{
			$Result[] = $IMap->Move_To_Trash($thisFolder,$thisUid);	
			}
		}	
//		 $currentFolder = urlencode($currentFolder);//encode thisFolder for url use handle chinese folder case		
    $currentFolder =  urldecode($IMap->decodeFolderName($currentFolder));
    $currentFolder = urlencode($IMap->encodeFolderName(stripslashes($currentFolder)));
		}//end gamma
	   else{
       if($currentFolder =='Trash'){
         	
         	$li = new libcampusmail();
			$lf = new libfilesystem();
			$lwebmail = new libwebmail();
			# Step 1: Extract valid mail IDs
			$sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND Deleted = 1 AND CampusMailID IN (".implode(",", $Uid).")";
			$targetID = $li->returnVector($sql);
			
			$iMail_insertDB_debug = true;
			if (sizeof($targetID)!=0)
			{
			    $CampusMailIDStr = implode(",",$targetID);
			
				if(method_exists($li,'UpdateCampusMailNewMailCount')){
					// decrement trash box new mail counter
				    $sql = "SELECT COUNT(*) as NewMailCount FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND CampusMailID IN (".$CampusMailIDStr.") AND (RecordStatus IS NULL OR RecordStatus='')";
					$count_record = $li->returnVector($sql);
					if($count_record[0] > 0){
					    $li->UpdateCampusMailNewMailCount($UserID, -1, -$count_record[0]);
					}
				}
			
			$sql = "SELECT HeaderFilePath, MessageFilePath FROM INTRANET_RAWCAMPUSMAIL WHERE CampusMailID IN (".$CampusMailIDStr.")";
			$FilePathArr = $li->returnArray($sql);
			
			## create a delete log
			if($iMail_insertDB_debug) {
				//$fs = new libfilesystem();
				
				$sql = "SELECT CampusMailID, Subject, Message FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND CampusMailID IN ($CampusMailIDStr)";
				$MailContent = $li->returnArray($sql,3);
				
				if(sizeof($MailContent)>0) {
					for($i=0; $i<sizeof($MailContent); $i++) {
						list($mail_id, $mail_subject, $mail_content) = $MailContent[$i];
						
						$log_content .= "--------------------------------------------------\n";
						$log_content .= "From Page : ".$_SERVER['HTTP_REFERER']."\n";
						$log_content .= "Action : Delete from Trash Box"."\n";
						$log_content .= "Date : ".date("Y-m-d H:i:s")."\n";
						$log_content .= "Target CampusmailID : ".$mail_id."\n";
						$log_content .= "Subject : ".$mail_subject."\n";
						$log_content .= "Message : ".$mail_content."\n\n";
					}
				}
				
				if (!is_dir("$intranet_root/file/mailDeleteLog")) {
					mkdir("$intranet_root/file/mailDeleteLog",0777);
				}
				
				if (!is_dir("$intranet_root/file/mailDeleteLog/u".$UserID)) {
					mkdir("$intranet_root/file/mailDeleteLog/u$UserID",0777);
				}
				$maillogPath = $intranet_root.'/file/mailDeleteLog/u'.$UserID;
				
				$logFilename = date('Y-m-d').'.log';
			
				$logFileHandle = (file_exists($maillogPath.'/'.$logFilename)) ? fopen($maillogPath.'/'.$logFilename,'a') : fopen($maillogPath.'/'.$logFilename,'w');
				
				fwrite($logFileHandle, $log_content);
				fclose($logFileHandle);
			}
			
			# Select internet mails CampusMailID for removing raw meesage log file
			$sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE MailType = 2 AND UserID = '$UserID' AND CampusMailID IN ($CampusMailIDStr)"; 
			$to_remove_rawlog_campusmailid = $li->returnVector($sql);
			for($i=0;$i<count($to_remove_rawlog_campusmailid);$i++) {
				$lwebmail->RemoveRawBodyLogFile($to_remove_rawlog_campusmailid[$i]);
			}
			
			# Step 2: Extract Attachment paths
			$sql = "SELECT Attachment FROM INTRANET_CAMPUSMAIL WHERE MailType = 1 AND IsAttachment = 1 AND Deleted = 1 AND Attachment != '' AND UserID = '$UserID' AND CampusMailID IN ($CampusMailIDStr)";
			$to_remove_attachmentpaths_int = $li->returnVector($sql);
			$sql = "SELECT Attachment FROM INTRANET_CAMPUSMAIL WHERE MailType = 2 AND IsAttachment = 1 AND Deleted = 1 AND Attachment != '' AND UserID = '$UserID' AND CampusMailID IN ($CampusMailIDStr)";
			$to_remove_attachmentpaths_ext = $li->returnVector($sql);
			
			if ($debug_mode)
			{
			    echo "Result in Step 2\n";
			    print_r($to_remove_attachmentpaths_int);
			    print_r($to_remove_attachmentpaths_ext);
			}
			$benchmark['step2 ends'] = time();
			
			# Newly added by Ronald on 20090818 
			# update the mailbox total quota used 
			$sql = "SELECT IFNULL(SUM(AttachmentSize),0) FROM INTRANET_CAMPUSMAIL WHERE MailType IN (1,2) AND IsAttachment = 1 AND Deleted = 1 AND Attachment != '' AND UserID = '$UserID' AND CampusMailID IN ($CampusMailIDStr)";
			$TotalAttachmentSize = $li->returnVector($sql);
			$sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET QuotaUsed = QuotaUsed - ".$TotalAttachmentSize[0]." WHERE UserID = '$UserID'";
			$li->db_db_query($sql);
			
			# Step 3: Remove reply records
			# Remove the replies for notification mail sent
			$sql = "DELETE FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID IN (".$CampusMailIDStr.")";
			$li->db_db_query($sql);
			
			# Step 4: Remove Mail DB records
			$sql = "DELETE FROM INTRANET_RAWCAMPUSMAIL WHERE UserID = '$UserID' AND CampusMailID IN ($CampusMailIDStr)";
			$li->db_db_query($sql);
			$sql = "DELETE FROM INTRANET_CAMPUSMAIL WHERE Deleted = 1 AND UserID = '$UserID' AND CampusMailID IN ($CampusMailIDStr)";
			$li->db_db_query($sql);
			
			# Step 5: Update Notification
			$sql = "UPDATE INTRANET_CAMPUSMAIL SET IsNotification = 0 WHERE CampusMailFromID IN ($CampusMailIDStr)";
			$li->db_db_query($sql);
			$benchmark['step5 ends'] = time();
			
			## Log down deleted attachment
			if($iMail_insertDB_debug){
				$logFileHandle = (file_exists($maillogPath.'/'.$logFilename)) ? fopen($maillogPath.'/'.$logFilename,'a') : fopen($maillogPath.'/'.$logFilename,'w');
			}
			
			# Step 6: Search for attachment path (Internal)
			//$lf = new libfilesystem();
			unset($list_path_string_to_remove);
			$delim = "";
			for ($i=0; $i<sizeof($to_remove_attachmentpaths_int); $i++)
			{
			     $target_path = $to_remove_attachmentpaths_int[$i];
			     if ($target_path != "")
			     {
			         $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE Attachment = '$target_path'";
			         $temp = $li->returnVector($sql);
			         if (is_array($temp) && sizeof($temp)==1 && $temp[0]==0)
			         {
			             # Remove Database records
			             $list_path_string_to_remove .= "$delim'$target_path'";
			             $delim = ",";
			
			             # Remove actual files
			             $full_path = "$file_path/file/mail/".$target_path;
			             if ($bug_tracing['imail_remove_attachment'])
			             {
			                 $command ="mv ".OsCommandSafe($full_path)." ".OsCommandSafe($full_path)."_bak";
			                 exec($command);
			             }
			             else
			             {
			                 $lf->lfs_remove($full_path);
			             }
			             
			             if($iMail_insertDB_debug && $logFileHandle){
			             	$log_content = "$sql\n";
			             	$log_content .= "COUNT ".$temp[0]."\n";
			             	$log_content .= "Deleted Internal Mail Path: $full_path\n";
			             	fwrite($logFileHandle, $log_content);
			             }
			         }
			
			     }
			}
			$benchmark['step6 ends'] = time();
			
			# Step 7: Remove Attachment path (External Mail)
			for ($i=0; $i<sizeof($to_remove_attachmentpaths_ext); $i++)
			{
			     $target_path = $to_remove_attachmentpaths_ext[$i];
			     if ($target_path != "")
			     {
			         # Remove Database records
			         $list_path_string_to_remove .= "$delim'$target_path'";
			         $delim = ",";
			
			         # Remove actual files
			         $full_path = "$file_path/file/mail/".$target_path;
			         if ($bug_tracing['imail_remove_attachment'])
			         {
			             $command ="mv ".OsCommandSafe($full_path)." ".OsCommandSafe($full_path)."_bak";
			             exec($command);
			         }
			         else
			         {
			             $lf->lfs_remove($full_path);
			         }
			         
			         if($iMail_insertDB_debug && $logFileHandle){
			             $log_content = "External Mail Attachment Path: $full_path\n";
			             fwrite($logFileHandle, $log_content);
			         }
			     }
			}
			
			# Remove Database Records
			if ($list_path_string_to_remove != "")
			{
			    $sql = "DELETE FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE AttachmentPath IN ($list_path_string_to_remove)";
			    $li->db_db_query($sql);
			}
			$benchmark['step7 ends'] = time();
			
			if($iMail_insertDB_debug && $logFileHandle){
				if($list_path_string_to_remove != ""){
					$log_content = "INTRANET_IMAIL_ATTACHMENT_PART AttachmentPath: $list_path_string_to_remove\n\n";
			        fwrite($logFileHandle, $log_content);
				}
				fclose($logFileHandle);
			}
			
			#Remove Original Email Source
			for($i=0;$i<count($FilePathArr);$i++) {
				$HeaderFilePath = $FilePathArr[$i]['HeaderFilePath'];
				$MessageFilePath = $FilePathArr[$i]['MessageFilePath'];
				if (trim($HeaderFilePath) != '' && trim($MessageFilePath) != '') {
					$HeaderFullPath = "$file_path/file/mail/".$HeaderFilePath;
					$MessageFullPath = "$file_path/file/mail/".$MessageFilePath;
					
					if (trim($HeaderFilePath) != '') {
						$lf->lfs_remove($HeaderFullPath);
					}
					if (trim($MessageFullPath) != '') {
							$lf->lfs_remove($MessageFullPath);
						}
					}
				}
			
			}
			}else{
				$li = new libcampusmail();
				if(method_exists($li,'UpdateCampusMailNewMailCount')){
					
					$sql = "SELECT UserFolderID, COUNT(*) as NewMailCount FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND CampusMailID IN (".implode(",", $Uid).") AND (RecordStatus IS NULL OR RecordStatus='') AND Deleted<>1 GROUP BY UserFolderID";
					$count_records = $li->returnResultSet($sql);
					for($i=0;$i<count($count_records);$i++)
					{
						$li->UpdateCampusMailNewMailCount($UserID, $count_records[$i]['UserFolderID'], -$count_records[$i]['NewMailCount']);
						$li->UpdateCampusMailNewMailCount($UserID, -1, $count_records[$i]['NewMailCount']);
					}
				}
				
				//set current email as trash and update the date also
				$sql = "UPDATE INTRANET_CAMPUSMAIL SET Deleted = 1, DateInFolder = NOW() WHERE UserID = '$UserID' AND CampusMailID IN (".implode(",", $Uid).")";
				$libDb->db_db_query($sql);
			
				
			}
		
//		if($currentFolder =='INBOX'){
//	 	$folderID = '2';
//	 		
//	 	}else if($currentFolder =='Drafts'){
//	 		$folderID = '1';
//	 	}else if($currentFolder =='Sent'){
//	 		$folderID = '0';
//	 	}		
//		
//	for($i=0; $i<$uidAraycount;$i++)	
//		{
//			if(strstr($Uid[$i],","))
//				list($currentUID,$thisFolder) = explode(",",$Uid[$i]);
//			else
//			{
//				$currentUID = $Uid[$i];
//			}
//	    if(($currentFolder =='INBOX')||($currentFolder =='Sent')){
//			//set current email as trash and update the date also
//			$sql = "UPDATE INTRANET_CAMPUSMAIL SET Deleted = 1, DateInFolder = NOW() WHERE UserID = '$uid' AND CampusMailID = '$currentUID'";
//			$libDb->db_db_query($sql);	
//			
//			}
//            else if($currentFolder =='Drafts'){
//            //delete email in draft folder and trash folder
//			$sql = "DELETE from INTRANET_CAMPUSMAIL where UserID='$uid' AND CampusMailID = '$currentUID' AND UserFolderID='$folderID'";	
//		    $libDb->db_db_query($sql);	
//		    //check need to delete attachment or not
//         }else if($currentFolder =='Trash'){
//         	$sql = "DELETE from INTRANET_CAMPUSMAIL where UserID='$uid' AND CampusMailID = '$currentUID' AND UserFolderID='$folderID'";	
//		    $libDb->db_db_query($sql);	
//         }	
//	    }		
	}
	
   $url="email_list.php?TargetFolderName=".$currentFolder."&parLang=".$parLang."&InitLoad=0";
?>
	<!-- /header -->   
	<META HTTP-EQUIV="REFRESH" CONTENT="0; URL=<?=$url?>"> 
  <!-- Home -->
</div>
<!-- /content -->
		<div data-role="footer">
		</div><!-- /footer -->
		</div><!-- /page -->
	</body>
</html>