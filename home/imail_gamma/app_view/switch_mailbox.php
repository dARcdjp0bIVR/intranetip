<?php
// page modifing by:
$PATH_WRT_ROOT = '../../../';

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpwm.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp_imail.php');


intranet_auth();
intranet_opendb();

$IMap = new imap_gamma();

if($SwitchTo)
{
	$_SESSION['SSV_EMAIL_LOGIN']    = $SwitchTo;
	$_SESSION['SSV_EMAIL_PASSWORD'] = $IMap->Get_MailBox_Password($SwitchTo);
	$_SESSION['SSV_LOGIN_EMAIL']    = $SwitchTo;

	unset($_SESSION['SSV_GAMMA_INIT_FOLDER']);
	unset($_SESSION['SSV_GAMMA_BATCH_REMOVAL_STATE']);
	unset($_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_send_right"]);
	unset($_SESSION['SSV_GAMMA_USE_CACHE']);
}


intranet_closedb();

header("Location: ".$PATH_WRT_ROOT."home/imail_gamma/app_view/email_main.php?parLang=".$parLang);
exit;
?>
