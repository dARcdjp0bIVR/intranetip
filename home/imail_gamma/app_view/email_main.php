<?php
$PATH_WRT_ROOT = '../../../';

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpwm.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp_imail.php');

//$token = $_GET['token'];
//$uid = IntegerSafe($_GET['uid']);
//$_SESSION['UserID'] = $uid;
//$ul = $_GET['ul'];

intranet_auth();
intranet_opendb();

$libDb = new libdb(); 
$libeClassApp = new libeClassApp();
$libpwm= new libpwm();
$linterface = new interface_html();
$emailClassApp = new emailClassApp();
$folderAssoAry ="";
$defaultFolder = array();
$personalFolder = array();

$uid = $_SESSION['UserID'];

$passwordAry = $libpwm->getData($uid);
foreach((array)$passwordAry as $_folderType => $_folderKey){
	if($_folderType==$uid)$password=$_folderKey;
}

//$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
//if (!$isTokenValid) {
//	die($Lang['Gamma']['App']['NoAccessRight']);
//}

$curUserId = $uid;

//if ($curUserId == '') {
//	$_SESSION['UserID'] = $curUserId;
//	$UserID = $curUserId;
//}

	$defaultFolderName =  $Lang['Gamma']['DefaulFolder'];
	$personalFolderName =  $Lang['Gamma']['PersonalFolder'];
	$_url =  "email_list.php?parLang=".$parLang."&InitLoad=0&TargetFolderName=";
	$refreshIconUrl =$linterface->Get_Ajax_Loading_Image($noLang=true);
	$show_main_table = "";
    $show_main_table_default_folder = "";
    $show_main_table_personal_folder = "";
    $switch_mail_box = "";

if ($plugin['imail_gamma']) {
	$sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$curUserId."'";
	$dataAry = $libDb->returnResultSet($sql);
	if(isset($_SESSION['SSV_EMAIL_LOGIN']) && isset($_SESSION['SSV_EMAIL_PASSWORD'])) {
		$iMapEmail = $_SESSION['SSV_EMAIL_LOGIN'];
		$iMapPwd = $_SESSION['SSV_EMAIL_PASSWORD'];
	} else {
		$iMapEmail = $dataAry[0]['ImapUserEmail'];
		$iMapPwd = $password;
	}
	$IMap = new imap_gamma($skipAutoLogin = false, $iMapEmail, $iMapPwd);
	$folderAry = $IMap->getMailFolders();
	$numOfFolder = count($folderAry);	
	$folderKeyAry = array();
		foreach ($folderAry as $key => $value) {
         $folderKeyAry[]=$key;
         }


	$selectedMailBox = $_SESSION['SSV_EMAIL_LOGIN'];
	$luser = new libuser($UserID);
	$SharedMail = $IMap->GetUserSharedMailBox($UserID);

	$MailBoxList[] = array($luser->ImapUserEmail,$luser->ImapUserEmail);
	for($i=0; $i<sizeof($SharedMail);$i++)
	{
		$MailBoxList[] = array($SharedMail[$i]["MailBoxName"],$SharedMail[$i]["MailBoxName"]);
	}

	if(count($MailBoxList) > 1) {
		$onchange = ' onchange="showLoading();this.form.submit()" ';
		$selection = getSelectByArray($MailBoxList, "name='SwitchTo' id='SwitchTo' $onchange", $selectedMailBox, 0, 1);
		$switch_mail_box = '<form name="SwitchMailBoxForm" method="POST" action="switch_mailbox.php?parLang=' . $parLang . '" style="margin:0; padding:0">';
		$switch_mail_box .= $selection;
		$switch_mail_box .= '</form>';
	}

	//Folders' name are with the prefix "INBOX." except folder "INBOX"
	for ($i=0; $i<$numOfFolder; $i++) {
		$_folderName = $folderAry[$folderKeyAry[$i]];		
		$_folderNamePieces = explode('.', $_folderName);
		$_numOfPieces = count($_folderNamePieces);
		if ($_numOfPieces == 1) {
			$_targetFolderName = 'INBOX';
		}
		else {
			$_targetFolderName = $_folderNamePieces[1];
		}
		
	    $_targetFolderName = IMap_Decode_Folder_Name($_targetFolderName);	
		$_imagePath = $image_path."/".$LAYOUT_SKIN."/iMail/app_view/";
	
		if($_targetFolderName == 'INBOX'){
		    $_imagePath .="icon_inbox.png";				
		}
		else if($_targetFolderName == 'Drafts'){
			$_imagePath .="icon_draft.png";
		}
		else if($_targetFolderName == 'Junk'){
			$_imagePath .="icon_spam.png";
		}
		else if($_targetFolderName == 'Sent'){
			$_imagePath .="icon_outbox.png";
		}
		else if($_targetFolderName == 'Trash'){
			$_imagePath .="icon_trash.png";
		}else{
			$_imagePath .="icon_folder.png";
		}	
		//get the folder's name according to the language setting
	     $_displayName = $Lang['Gamma']['SystemFolderName'][$_targetFolderName];
	   	if($_displayName!=''){
	   		//add to default folder list			
			$show_main_table_default_folder .= $emailClassApp->getImailFolderHtml(urlencode($IMap->encodeFolderName($_targetFolderName)),$_imagePath,$_displayName);
	   	}else{
	   		if (!(in_array($_targetFolderName, (array)$SYS_CONFIG['Mail']['SystemMailBox']))){
	   		//add to personal folder list
			$show_main_table_personal_folder .= $emailClassApp->getImailFolderHtml(urlencode($IMap->encodeFolderName($_targetFolderName)),$_imagePath,$_targetFolderName);
	   		}
	   	}
	}	
}// end gamma
else {
	
	//FolderID: 0 is Outbox, 1 is Draft, 2 is Inbox
	//other FolderID is user created folders
	$sql = "SELECT FolderID, FolderName FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID = '".$curUserId."' OR RecordType = 0 ORDER BY  FolderID DESC";
	$folderAry = BuildMultiKeyAssoc($libDb->returnResultSet($sql), 'FolderID', $IncludedDBField=array('FolderName'), $SingleValue=1);
	$folderAry['-1'] = 'Trash';
	$numOfFolder = count($folderAry);	
	//get folder's name without the prefix INBOX. 
	$folderKeyAry = array();
	foreach ($folderAry as $key => $value) {
     $folderKeyAry[]=$key;
     } 
         
	for ($i=0; $i<$numOfFolder; $i++) {
		
		$_targetFolderName = $folderAry[$folderKeyAry[$i]];
		$_imagePath = $image_path."/".$LAYOUT_SKIN."/iMail/app_view/";
		//Default folder icon is icon_folder.png
		$__imagePath = $_imagePath."icon_folder.png";
		if($_targetFolderName =='Inbox'){
			$_targetFolderName = "INBOX";
			$_displayName = $Lang['Gamma']['SystemFolderName'][$_targetFolderName];
			$__imagePath = $_imagePath."icon_inbox.png";
			//add to default folder list
			$show_main_table_default_folder .= $emailClassApp->getImailFolderHtml($_targetFolderName,$__imagePath,$_displayName);
		}
		else if($_targetFolderName =='Draft'){
			$_targetFolderName = "Drafts";
			$_displayName = $Lang['Gamma']['SystemFolderName'][$_targetFolderName];
			$__imagePath = $_imagePath."icon_draft.png";
			$show_main_table_default_folder .= $emailClassApp->getImailFolderHtml($_targetFolderName,$__imagePath,$_displayName);
		}
		else if($_targetFolderName =='Outbox'){
			$_targetFolderName = "Sent";
			$_displayName = $Lang['Gamma']['SystemFolderName'][$_targetFolderName];
			$__imagePath = $_imagePath."icon_outbox.png";
			$show_main_table_default_folder .= $emailClassApp->getImailFolderHtml($_targetFolderName,$__imagePath,$_displayName);
		}else if($_targetFolderName =='Trash'){
			$_targetFolderName = "Trash";
			$_displayName = $Lang['Gamma']['SystemFolderName'][$_targetFolderName];
			$__imagePath = $_imagePath."icon_trash.png";
		    $show_main_table_default_folder .= $emailClassApp->getImailFolderHtml($_targetFolderName,$__imagePath,$_displayName);
		}else{
			$show_main_table_personal_folder .= $emailClassApp->getImailFolderHtml($_targetFolderName,$__imagePath,$_targetFolderName);
		}
	}				
}

$show_main_table = "<ul data-role='listview' id='defalt_email_list' data-inset='false' data-split-icon='plus' style = 'margin-bottom: 0px;'>";
if($switch_mail_box != "") {
	$show_main_table .= '<li>';
	$show_main_table .= '<div class="ui-controlgroup-controls" style="font-size:0;!important">';
	$show_main_table .= $switch_mail_box;
	$show_main_table .= '</div>';
	$show_main_table .= '</li>';
}

$show_main_table .= "<li data-theme='' style ='background-color:#f1f1f1;'>
        <h1>".$defaultFolderName."</h1>
        </li>".$show_main_table_default_folder."</ul><ul data-role='listview' id='personal_email_list' data-inset='false' data-split-icon='plus' style = 'margin-top: 0px;'>
                    <li data-theme='' style ='background-color:#f1f1f1; '>
               <h1 >".$personalFolderName."</h1></li>".$show_main_table_personal_folder."</ul>";
               
$loadingX = $emailClassApp->getLoadingHtml();
echo $libeClassApp->getAppWebPageInitStart();
?>
		<script>
	  $( document ).on( "mobileinit" , function () {
	    $.mobile.toolbar.prototype.options.addBackBtn = true;
	  });

  	  function getFolderEmailList(inputId)
	  {
	  	showLoading();
	    var emaillink = "<?=$_url?>";
     emaillink = emaillink + inputId;
     window.location.assign(emaillink);
	  }
	 function showLoading() {
	    document.getElementById('loadingmsg').style.display = 'block';
	    document.getElementById('loadingover').style.display = 'block';
	 }	    
     </script>
	 <style type="text/css">	
     #loadingmsg {
      color: black;
      background:tranparent!important;
      position:fixed;
      top: 50%;
      left: 50%;
      z-index: 100;
      margin: -8px 0px 0px -8px;
      }
      #loadingover {
      background: white;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }
    </style>
	</head>
	<body>
    <div data-role="page" id="body">
    <?=$loadingX?>
    <div data-role="content">
    <?=$show_main_table?>
<?php
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>