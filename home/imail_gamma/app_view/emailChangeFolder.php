<?php
// Editing by 
/*
 * 2020-11-05 (Ray):  add $_SESSION['SSV_EMAIL_LOGIN'], $_SESSION['SSV_EMAIL_PASSWORD']
 * 2017-03-23 (Carlos): Update new mail counter for iMail.
 */
$PATH_WRT_ROOT = '../../../';
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libpwm.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

$targetFolder = $_POST['targetFolder'];
$thisFolder = $_POST['originalFolder'];
$originalUID = IntegerSafe($_POST['originalUID']);
$uid = IntegerSafe($_POST['UID']);
$currentUID = "";
$ResponseStatus = 0;

intranet_auth();
intranet_opendb();

$libpwm= new libpwm();
$libDb = new libcampusmail(); 

if ($plugin['imail_gamma']) {
//	$thisFolder = urldecode($thisFolder);
//	$targetFolder = urldecode($targetFolder);
	$passwordAry = $libpwm->getData($uid);
	foreach((array)$passwordAry as $_folderType => $_folderKey){
		if($_folderType==$uid)$password=$_folderKey;
	}
	$sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$uid."'";
	$dataAry = $libDb->returnResultSet($sql);
	if(isset($_SESSION['SSV_EMAIL_LOGIN']) && isset($_SESSION['SSV_EMAIL_PASSWORD'])) {
		$iMapEmail = $_SESSION['SSV_EMAIL_LOGIN'];
		$iMapPwd = $_SESSION['SSV_EMAIL_PASSWORD'];
	} else {
		$iMapEmail = $dataAry[0]['ImapUserEmail'];
		$iMapPwd = $password;
	}
    $IMap = new imap_gamma(false,$iMapEmail, $iMapPwd);
    $targetFolder = str_replace('\"','"',$targetFolder);
    $targetFolder = stripslashes($targetFolder);
    $thisFolder = str_replace('\"','"',$thisFolder);
    $thisFolder = stripslashes($thisFolder);
//    $currentFolder = urlencode($IMap->encodeFolderName($thisFolder));
    $currentFolder =  urldecode($IMap->decodeFolderName($thisFolder));
    $currentFolder = urlencode($IMap->encodeFolderName(($currentFolder)));
    if($targetFolder!="INBOX"){
	$targetFolder = "INBOX.".$targetFolder;
    }
	if($thisFolder!="INBOX"){
	$thisFolder = "INBOX.".$thisFolder;
	} 
	$sort = "SORTARRIVAL";
	$PrevNextUID = $IMap->Get_Prev_Next_UID($thisFolder, $sort, 0, $originalUID);
//	$PrevNextUID = $IMap->Get_Prev_Next_UID($IMap->encodeFolderName($thisFolder), $sort, 0, $originalUID);
	$prevID = $PrevNextUID["PrevUID"];
	$nextID = $PrevNextUID["NextUID"];
	$ResponseStatus = $IMap->Move_Mail($thisFolder, $originalUID,stripslashes($targetFolder));	
    //$currentUID = $IMap->Get_Last_Msg_UID($targetFolder);  
//   $ResponseStatus = $IMap->Move_Mail(stripslashes($IMap->encodeFolderName($thisFolder)), $originalUID, stripslashes($IMap->encodeFolderName($targetFolder)));
}// end gamma
else{
	$currentFolder = $thisFolder;
	if($thisFolder =='2'){
		$currentFolder = "INBOX";
	}else if($thisFolder =='1'){
		$currentFolder = "Drafts";
	}else if($thisFolder =='0'){
		$currentFolder = "Sent";
	}else if($thisFolder == '-1'){
        $currentFolder = "Trash";
	}else{
	$sql = "select foldername from INTRANET_CAMPUSMAIL_FOLDER where OwnerID='$uid' and FolderID= '$thisFolder'";
	$currentFolderDB = $libDb->returnVector($sql);
	$currentFolder = $currentFolderDB[0];
	}
	
	#Get prevous and nextID of the currentUID
	$sql = "Select CampusMailID from INTRANET_CAMPUSMAIL where UserID ='$uid' and UserFolderID = '$thisFolder' AND Deleted = '0' order by CampusMailID desc;";
	$originalFolderCampusMailIDS = $libDb->returnVector($sql);
	$prevID = '';
	$nextID = '';
    for($i=0;$i<count($originalFolderCampusMailIDS);$i++){
    	if($originalFolderCampusMailIDS[$i]==$originalUID){
    		if($i!=0){
    			$prevID = $originalFolderCampusMailIDS[$i-1];
    		}
    		if($i!=(count($originalFolderCampusMailIDS)-1)){
    			$nextID = $originalFolderCampusMailIDS[$i+1];
    		}
    		break;
    	}
    }
    
    if(method_exists($libDb,'UpdateCampusMailNewMailCount')){
	    // update new mail counter
		$sql = "SELECT IF(Deleted=1,-1,UserFolderID) as UserFolderID, COUNT(*) as NewMailCount,Deleted FROM INTRANET_CAMPUSMAIL WHERE UserID = '$uid' AND CampusMailID='$originalUID' AND (RecordStatus IS NULL OR RecordStatus='') GROUP BY UserFolderID,Deleted";
		$count_records = $libDb->returnResultSet($sql);
		for($i=0;$i<count($count_records);$i++)
		{
			$libDb->UpdateCampusMailNewMailCount($uid, $count_records[$i]['UserFolderID'], -$count_records[$i]['NewMailCount']);
			$libDb->UpdateCampusMailNewMailCount($uid, $targetFolder, $count_records[$i]['NewMailCount']);
		}
    }
    
	$sql = "UPDATE INTRANET_CAMPUSMAIL SET UserFolderID = '$targetFolder', DateInFolder = NOW() WHERE UserID = '$uid' AND CampusMailID = '$originalUID'";
	$ResponseStatus = $libDb->db_db_query($sql);
}
	if($prevID!=""){//has previous email, go to previous email
		$currentUID = $prevID;
	}else if($nextID!=""){//no previous email, go to next email
		$currentUID = $nextID;
	}
    #no email in current folder, go to the folder list#

$changeEmailFolderResult = "{\"currentUID\": \"".$currentUID."\",\"currentFolder\":\"".$currentFolder."\",\"ResponseStatus\":\"".$ResponseStatus."\"}";
echo $changeEmailFolderResult;
?>