<?php
//using:
/*
 * 2020-11-05 (Ray): Add $_SESSION['SSV_EMAIL_LOGIN'], $_SESSION['SSV_EMAIL_PASSWORD']
 * 2020-03-04 (Sam)  Draft receiver becomes system admin if not set [Z181214]
 * 2020-02-24 (Sam): Show "System Admin" as display name when $senderID == 0 [S180470]
 */
$PATH_WRT_ROOT = '../../../';
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libpwm.php");
include_once($PATH_WRT_ROOT."includes/json.php");

$uid = IntegerSafe($_POST['uid']);
$startIndex = $_REQUEST['StartIndex'];
$thisFolder = $_REQUEST['TargetFolderName'];

intranet_auth();
intranet_opendb();

$libDb = new libdb(); 
$libeClassApp = new libeClassApp();
$libpwm= new libpwm();
$folderAssoAry ="";
$defaultFolder = array();
$personalFolder = array();
$json = new JSON_obj();

if ($plugin['imail_gamma']) {
    $passwordAry = $libpwm->getData($uid);
foreach((array)$passwordAry as $_folderType => $_folderKey){
	if($_folderType==$uid)$password=$_folderKey;
}

	$curUserId  = $uid;

    $sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$curUserId."'";
	$dataAry = $libDb->returnResultSet($sql);
	if(isset($_SESSION['SSV_EMAIL_LOGIN']) && isset($_SESSION['SSV_EMAIL_PASSWORD'])) {
		$iMapEmail = $_SESSION['SSV_EMAIL_LOGIN'];
		$iMapPwd = $_SESSION['SSV_EMAIL_PASSWORD'];
	} else {
		$iMapEmail = $dataAry[0]['ImapUserEmail'];
		$iMapPwd = $password;
	}
    $IMap = new imap_gamma(false,$iMapEmail, $iMapPwd);
    $_displayName = $Lang['Gamma']['SystemFolderName'][$thisFolder];
    if($thisFolder!="INBOX"){
    	$Folder = "INBOX.".$thisFolder;
    }else{
    	$Folder = $thisFolder;
    }
	
$sort = "SORTARRIVAL";
$displayMode = (isset($displayMode) && $displayMode != "") ? $displayMode : "folder";
$Folder = (isset($Folder) && $Folder != "") ? stripslashes($Folder) : $IMap->InboxFolder;
$order 	 = (isset($order) && $order != "") ? $order : 1;
$CacheNewMail = false;

if (!$IMap->ExtMailFlag) {
	if($IMap->ConnectionStatus == true){
		$imap_reopen_success = imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));	
		if($Folder == $IMap->SpamFolder) {
			// Connect Junk box twice to let mail server auto correct read/unread status
			$imap_reopen_success = imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));
		   }
		   
		if(!$imap_reopen_success){
			sleep(1);
			imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));
		}
		if($IMap->CacheMailToDB == true){
		}
		$imapInfo['imap_stream']   = $IMap->inbox;
		$imapInfo['server']    	   = $IMap->host;
		$imapInfo['password'] 	   = $IMap->CurUserPassword;
		$imapInfo['port']  	   	   = $IMap->mail_server_port;
		$imapInfo['username']  	   = $IMap->CurUserAddress;
		$imapInfo['displayMode']   = $displayMode;
		$imapInfo['sort'] 		   = $sort;
		$imapInfo['reverse'] 	   = $order;
		$imapInfo['messageSeq']    = $messageSeq;//have no value
		$imapInfo['messageNo']     = $messageNo;//no value too
		# Can be ignored
		$imapInfo['mailbox']   	   = $Folder;// orig: mailbox
		$isDraftInbox = ($Folder == $IMap->DraftFolder) ? 1 : 0;
		$isOutbox = ($Folder == $IMap->SentFolder) ? 1 : 0;
		$array = $IMap->Get_Sort_Index($IMap, $imapInfo, $search, $MailStatus);
      
		$imapInfo['pageInfo']['totalCount'] = (is_array($array)) ? count($array) : 0;
		$imapInfo['pageInfo']['pageStart']  = ($pageSize * ($imapInfo['pageInfo']['pageNo'] - 1) + 1);
		$imapInfo['pageInfo']['totalCount'] = (is_array($array)) ? count($array) : 0;
		$emailAryTotalCount = $imapInfo['pageInfo']['totalCount'];
		if($emailAryTotalCount>($startIndex+20)){
			$endIndex = ($startIndex+20);
		}else{
			$endIndex = $emailAryTotalCount;
		}
		for ($i= $startIndex ; $i<$endIndex; $i++) {
		$MailHeaderList[] = imap_headerinfo($IMap->inbox, $array[$i], 80, 80);
		$msgno[] = $array[$i];
		}
        $emailAry = array();
        $MailHeaderListcounter = count($MailHeaderList); 
        
        $MailFlags = $IMap->GetMailPriority($msgno,$Folder);
         
   		for($i= 0 ; $i<$MailHeaderListcounter; $i++){
		$contentAry = $MailHeaderList[$i];
	    $unSeen = $contentAry->Unseen;
        $_unSeen = ($unSeen=="U")?true:false;
        $subject = $IMap->MIME_Decode($contentAry->subject); 
	 	if ($subject != "") {
			$subjectPrint = (substr($subject, 0, 10) == "=?UTF-8?Q?") ? substr($subject, 10, -2) : $subject ;
		} else {
			$subjectPrint = "[NO SUBJECT]";
		}
		$subjectPrint = $AttachmentIcon.htmlspecialchars($subjectPrint);
		$subjectPrint = $IMap->BadWordFilter($subjectPrint);
        $emailAry[$i]['Subject'] = $subjectPrint;
        //DATE
        if($IMap->IsToday(str_ireplace("UT","UTC",$contentAry->date)))
		$DateFormat = "H:i";
		else
		$DateFormat = "Y-m-d";
		$prettydate = date($DateFormat, strtotime(str_ireplace("UT","UTC",$contentAry->date)));
        $emailAry[$i]['Date'] = $prettydate;
        //SENDER
        if($thisFolder=='Drafts'||$thisFolder=='Sent'){
        $toArray = $contentAry-> to;
        $_stdClassObject = $toArray[0];
        }else{        	
        $fromArray = $contentAry-> from;
        $_stdClassObject = $fromArray[0];
        }

        $mailbox =  $IMap->MIME_Decode( $_stdClassObject->personal);
       //if not internal user add the email address's name as the title
       if($mailbox==''){
       	 $mailbox = $_stdClassObject->mailbox;  
       } 
       //if has no user
       if($mailbox==''){
       	$mailbox = "-";
       }
        $emailAry[$i]['Mailbox'] = $mailbox;
        
        $currentMessageNo = $contentAry-> Msgno;
        $iscurrentAttachment = $IMap->Check_Any_Mail_Attachment($currentMessageNo);
        $_hasAttachment = ($iscurrentAttachment==1)?true:false;
        $currentUID = $IMap->Get_UID ($currentMessageNo);
        $isImportant = $MailFlags[intVal($currentMessageNo)]['ImportantFlag']=== 'true'?true:false;
        
        $emailAry[$i]['UID'] = $currentUID;
        $emailAry[$i]['UnSeen'] = $_unSeen;
        $emailAry[$i]['HasAttachment'] = $_hasAttachment;
        $emailAry[$i]['IsImportant'] = $isImportant;
        $emailAry[$i]['IsNotification'] = false;
		}
}}}//end gamma
else{
		$lc = new libcampusquota2007($UserID);
	if($thisFolder =='INBOX'){
		$folderID[0] = 2;
		$senderIDTitle = "SenderID";
	}else if($thisFolder =='Drafts'){
		$folderID[0] = 1;
		$senderIDTitle = "RecipientID";
	}else if($thisFolder =='Sent'){
		$folderID[0] = 0;
		$senderIDTitle = "RecipientID";
	}else if($thisFolder == 'Trash'){
        $folderID[0] = -1;
        $senderIDTitle = "SenderID";
	}else{
	    $sql = "select FolderID from INTRANET_CAMPUSMAIL_FOLDER where OwnerID='$uid' and foldername = '".$libDb->Get_Safe_Sql_Query($thisFolder)."'";
	    $senderIDTitle = "SenderID";
	    $folderID = $libDb->returnVector($sql);
	}
	if( $folderID[0] == -1){
	$sql  = "select CampusMailID,".$senderIDTitle.",SenderEmail,ExternalTo,Subject,DateInput,RecordStatus,IsAttachment,IsImportant,IsNotification from INTRANET_CAMPUSMAIL where UserID ='$uid' and Deleted= '1' order by DateInput";		
	}else{
	$sql  = "select CampusMailID,".$senderIDTitle.",SenderEmail,ExternalTo,Subject,DateInput,RecordStatus,IsAttachment,IsImportant,IsNotification from INTRANET_CAMPUSMAIL where UserID ='$uid' AND (Deleted != '1' or Deleted is null) and UserFolderID= '".$folderID[0]."' order by DateInput";		
	}

	$MailHeaderList = $lc->returnArray($sql);	
	$emailAryTotalCount = count($MailHeaderList);
	$startRetriveIndex = $emailAryTotalCount -$startIndex-1;
	if($emailAryTotalCount>($startIndex+20)){
		$endIndex = $emailAryTotalCount -$startIndex-20;
	}else{
		$endIndex = 0;
	}
	$emailAry = array();
	for($i= $startRetriveIndex ; $i>=$endIndex; $i--){
	$emailReturnList[] = $MailHeaderList[$i];
	}
	for($i= 0 ; $i<count($emailReturnList); $i++){
		$contentAry = $emailReturnList[$i];
        $subject = $contentAry['Subject']; 
        $date = $contentAry['DateInput']; 
        $currentUID = $contentAry['CampusMailID']; 
        $senderID = str_replace('U', '', $contentAry[$senderIDTitle]);
        $senderEmail = $contentAry['SenderEmail'];
        $externalTo = $contentAry['ExternalTo']; 
        $recordStatus = $contentAry['RecordStatus']; 
        $_unSeen = ($recordStatus=="")?true:false;
        $iscurrentAttachment = $contentAry['IsAttachment'];
        $_hasAttachment = ($iscurrentAttachment ==1)?true:false;
        $isImportant = ($contentAry['IsImportant'] === '1')?true:false;
        $isNotification = ($contentAry['IsNotification'] === '1')?true:false;
        
        if(($senderID!=null)&&($senderID!='')){
        $nameLan = Get_Lang_Selection('ChineseName', 'EnglishName');
        $sql  = "select ".getNameFieldByLang()."as Name from INTRANET_USER where UserID ='$senderID'";  
        $SenderName = $lc->returnArray($sql);		
        $mailbox = $SenderName[0]['Name'];
        }else{
			   if($thisFolder =='Sent'||$thisFolder =='Drafts'){
		        $pieces = explode(" ", $externalTo);
		    	}else{
			    $pieces = explode(" ", $senderEmail);
		    	}
		    	$mailbox =  str_replace(";","",$pieces[0]);
          }
        if($mailbox==''){
            if($senderID == '0'){//[Z181214]
                $mailbox = $Lang['AppNotifyMessage']['SystemAdmin'];
            }else{
                $mailbox = "-";
            }
        }
	 	if ($subject != "") {
			$subjectPrint = (substr($subject, 0, 10) == "=?UTF-8?Q?") ? substr($subject, 10, -2) : $subject ;
		} else {
			$subjectPrint = "[NO SUBJECT]";
		}

        $emailAry[$i]['Subject'] = $subjectPrint;
        
        $DateFormat = "Y-m-d";
        if(date("Y-m-d")==date($DateFormat, strtotime(str_ireplace("UT","UTC",$date)))){
        	$DateFormat = "H:i";
        }
		$emailAry[$i]['Date']='';
		if($date!=''){
	    $prettydate = date($DateFormat, strtotime(str_ireplace("UT","UTC",$date)));
        $emailAry[$i]['Date'] = $prettydate;	
		}
        
        $emailAry[$i]['Mailbox'] = $mailbox;
        $emailAry[$i]['UID'] = $currentUID;
        $emailAry[$i]['UnSeen'] = $_unSeen;
        $emailAry[$i]['HasAttachment'] = $_hasAttachment;
        $emailAry[$i]['IsImportant'] = $isImportant;
        $emailAry[$i]['IsNotification'] = $isNotification;
		}
		
	}
	$emailArrReturnJson = "{\"emailAryTotalCount\": \"".$emailAryTotalCount."\",\"startIndex\": \"".$startIndex."\",\"emailArr\":".$json->encode($emailAry)."}";
		
	echo $emailArrReturnJson;
?>