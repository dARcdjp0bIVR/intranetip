<?php
// Editing by 
/*
 * 2016-09-29 (Carlos): Use a hidden date time string for sorting.
 * 2015-12-11 (Carlos): Perform usort() on the email result list to sort in order.
 * 2014-06-16 (Carlos): Created for searching emails in a certain time span
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");

intranet_auth();
intranet_opendb();
auth_campusmail();

$IMap = new imap_gamma();

## Get Data
//$isAdvanceSearch = isset($isAdvanceSearch)? $isAdvanceSearch : (isset($KeywordFrom)?1:'');
$isAdvanceSearch = isset($isAdvanceSearch)? $isAdvanceSearch : 0;
$KeywordFrom 	= (isset($KeywordFrom) && $KeywordFrom != "") ? stripslashes(trim($KeywordFrom)) : "";
$KeywordTo		= (isset($KeywordTo) && $KeywordTo != "") ? stripslashes(trim($KeywordTo)) : "";
$KeywordCc 		= (isset($KeywordCc) && $KeywordCc != "") ? stripslashes(trim($KeywordCc)) : "";
$KeywordBcc 	= (isset($KeywordBcc) && $KeywordBcc != "") ? stripslashes(trim($KeywordBcc)) : "";
$KeywordSubject = (isset($KeywordSubject) && $KeywordSubject != "") ? stripslashes(trim($KeywordSubject)) : "";
$KeywordBody 	= (isset($KeywordBody) && $KeywordBody != "") ? stripslashes(trim($KeywordBody)) : "";
$KeywordAttachment = (isset($KeywordAttachment) && $KeywordAttachment != "") ? stripslashes(trim($KeywordAttachment)) : "";
$FromDate 		= (isset($FromDate) && $FromDate != "") ? trim($FromDate) : "";
$ToDate 		= (isset($ToDate) && $ToDate != "") ? trim($ToDate) : "";
if($Folder_b != ""){
	$Folder = base64_decode($Folder_b);
}else{
	$Folder 		= (isset($Folder))? stripslashes($Folder) : $SYS_CONFIG['Mail']['FolderPrefix'];
}
$FromDate = $StartDate;
$ToDate = $EndDate;

# get Folder to search
if(isset($SearchFolder_b) && count($SearchFolder_b)>0){
	$SearchFolder = array();
	for($i=0;$i<count($SearchFolder_b);$i++){
		$SearchFolder[] = base64_decode($SearchFolder_b[$i]);
	}
	$SearchFolder = array_values(array_unique($SearchFolder));
}else{
	$SearchFolder 	= (isset($SearchFolder) && count($SearchFolder) > 0) ? array_unique($SearchFolder) : $IMap->getMailFolders();
}
$SearchFolderHidden = '';
for ($i=0; $i <sizeof($SearchFolder) ; $i++) {
	$SearchFolderHidden .= '<input type="hidden" id="SearchFolder[]" name="SearchFolder[]" value="'.stripslashes($SearchFolder[$i]).'">';
}

# Page Info Data
//$sort 		 = (isset($sort) && $sort != "") ? $sort : "DateSort";		// array sort fields: DateSort, From, Subject, Size, To, Folder
$reverse 	 = (isset($order) && $order != "") ? $order : 1;		// $order
$pageNo 	 = (isset($pageNo) && $pageNo != "") ? $pageNo : 1;
$numPerPage  = (isset($numPerPage) && $numPerPage != "") ? $numPerPage : 20;

## Initialization
$CurTag 	= $Folder;
$CurMenu    = 1;
$SearchInfo = array();
$imapInfo   = array();

## Preparation
//$FolderArray = $IMap->Get_Folder_Structure(true);		// get Folder Structure for select box

$SearchInfo['keyword'] 	    =  stripslashes(trim($keyword));
//$SearchInfo['Folder'][] 	= $Folder;
$SearchInfo['From'] 	= $KeywordFrom;
$SearchInfo['To'] 		= $KeywordTo;
$SearchInfo['Cc'] 		= $KeywordCc;
$SearchInfo['Bcc'] 		= $KeywordBcc;
$SearchInfo['Subject']  = $KeywordSubject;
$SearchInfo['FromDate'] = $FromDate;
$SearchInfo['ToDate'] 	= $ToDate;
$SearchInfo['Body'] = $KeywordBody;
$SearchInfo['Attachment'] = $KeywordAttachment;
$SearchInfo['Folder'] 	= $SearchFolder;


$default_field = 2; // date 
if($field=="") $field = $default_field;

if ($page_size_change == 1)
{
    //setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    //$ck_page_size = $numPerPage;
    $pageNo = 1;
}

$MailList = $IMap->Get_Search_Mail_List($pageStart, $pageEnd, $li->field_array[$field], $order, $SearchField, $SearchInfo, $isAdvanceSearch,$MailStatus);

$li = new libdbtable2007($field, $order, $pageNo);

$li->IsColOff = "imail_gamma_search";
$li->field_array= array("'From'","Subject","DateSort","Folder","'Size'");

$isDraftInbox = $Folder == $IMap->DraftFolder ;
$isOutbox = $Folder == $IMap->SentFolder;
//$senderreceiver = ($isDraftInbox||$isOutbox)?$i_frontpage_campusmail_recipients:$i_frontpage_campusmail_sender;
if($isAdvanceSearch == 1){
	$senderreceiver = $i_frontpage_campusmail_sender.' / '.$i_frontpage_campusmail_recipients;
}else{
	$senderreceiver = ($isDraftInbox||$isOutbox)?$i_frontpage_campusmail_recipients:$i_frontpage_campusmail_sender;
}

$pos=0;
$li->column_list .= "<td width='1%' class='tabletop' >". $i_frontpage_campusmail_icon_status_2007."</td>\n";
$li->column_list .= "<td width='1%' class='tabletop' ><img align='absmiddle' src='$image_path/$LAYOUT_SKIN/icon_star_off.gif' border='0' /></td>\n";
$li->column_list .= "<td width='1%' class='tabletop' ><img align='absmiddle' src='$image_path/$LAYOUT_SKIN/iMail/icon_important_title.gif' border='0' /></td>\n";
$li->column_list .= "<td width='1%' class='tabletop' >". $i_frontpage_campusmail_icon_notification_2007."</td>\n";
$li->column_list .= "<td width='15%' class='tabletop' >".$li->column($pos++, $senderreceiver)."</td>\n";
$li->column_list .= "<td width='60%' class='tabletop' >".$li->column($pos++, $Lang['Gamma']['Subject'])."</td>\n";
$li->column_list .= "<td width='100' class='tabletop' >".$li->column($pos++, $i_frontpage_campusmail_date)."</td>\n";
$li->column_list .= "<td width='60' class='tabletop' >".$li->column($pos++, $i_frontpage_campusmail_folder)."</td>\n";
$li->column_list .= "<td width='60' class='tabletop' >".$li->column($pos++, $i_frontpage_campusmail_size)."</td>\n";
$li->column_list .= "<td width='25' class='tabletop' ><input type='checkbox' onclick='checkAll(this)' id='CheckAllBox'></td>\n";

$li->no_col = 11;


######################### Cache Search Fields ###################################
$SearchFields = array();
$SearchFields[]['FromSearch'] = $FromSearch;
$SearchFields[]['order'] = $order;
$SearchFields[]['field'] = $field;
$SearchFields[]['pageNo'] = $pageNo;
$SearchFields[]['keyword'] = stripslashes(trim($keyword));
$SearchFields[]['KeywordFrom'] = $KeywordFrom;
$SearchFields[]['KeywordTo'] = $KeywordTo;
$SearchFields[]['KeywordCc'] = $KeywordCc;
$SearchFields[]['KeywordBcc'] = $KeywordBcc;
$SearchFields[]['KeywordSubject'] = $KeywordSubject;
$SearchFields[]['KeywordBody'] = $KeywordBody;
$SearchFields[]['KeywordAttachment'] = $KeywordAttachment;
$SearchFields[]['FromDate'] = $FromDate;
$SearchFields[]['ToDate'] = $ToDate;
$SearchFields[]['Folder'] = $Folder;
for ($i=0; $i <sizeof($SearchFolder) ; $i++) {
	$SearchFields[]['SearchFolder[]'] = stripslashes($SearchFolder[$i]);
}
##################################################################################

$CmpField = str_replace("'","", $li->field_array[$field]);

$MailAry = $MailList[0];
usort($MailAry, 'URSortCmp');

$UIDList = Get_Array_By_Key($MailAry,"UID");

if(isset($IMap->CacheMailToDB) && $IMap->CacheMailToDB == true){
	// flags from db
}else{
	//$MailPriority = $IMap->GetMailPriority($UIDList,$Folder,$isUID=1);
	$MailPriority = array();
	if(isset($SearchInfo['Folder']) && count($SearchInfo['Folder'])>0){
		foreach($SearchInfo['Folder'] as $FolderKey=>$FolderName){
			$TempMailPriority = $IMap->GetMailPriority($UIDList,$FolderName,$isUID=1);
			$MailPriority[$FolderName] = $TempMailPriority;
		}
	}
}
//$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4' id='MailList'  onselectstart='return false;' style='-moz-user-select:-moz-none;;'>";
//$x .= $this->displayColumn();

if (count($MailAry)==0)
{
	/*
	$x .= "<tr>
                <td class='iMailrow tabletext' align='center' colspan='".($this->no_col-1)."' ><br />".$this->no_msg."<br /><br></td>
			</tr>\n
			";
	*/
} 
else 
{
	/*
	$sortby = $this->field_array[$this->field];
	if(isset($IMap->CacheMailToDB) && $IMap->CacheMailToDB == true){
		// sort by sql
	}else{
		sortByColumn2($MailAry,$sortby,$this->order);
	}
	*/
	## Store sorted search result to user file
//	$CachedUID = Get_Array_By_Key($MailAry,"UID");
//	$CachedFolder = Get_Array_By_Key($MailAry,"Folder");
//	$IMap->Set_Cached_Search_Result($CachedUID,$CachedFolder,$SearchFields);
	/*
	if(isset($IMap->CacheMailToDB) && $IMap->CacheMailToDB == true){
		$start = 0;
		$end = count($MailAry);
	}else{
		$start = ($this->pageNo-1)*$this->page_size;
		$end = $this->page_size*$this->pageNo;
	}
	*/
	$start = 0;
	$end = count($MailAry);
	
	for($i=$start; $i<$end&&$i<count($MailAry); $i++)
	{
		
		$j=0;
		
		$From 	  = $MailAry[$i]["From"];
		$To 	  = $MailAry[$i]["To"];
		$Date 	  = str_ireplace("UT","UTC",$MailAry[$i]["DateReceived"]);
		$Priority = $MailAry[$i]["Priority"];
		$Subject  = $IMap->BadWordFilter($MailAry[$i]["Subject"]);
		$Uid 	  = $MailAry[$i]["UID"];
		$Size 	  = $MailAry[$i]["Size"];
		$recent   = $MailAry[$i]["recent"];
		$flagged  = $MailAry[$i]["flagged"];
		$answered = $MailAry[$i]["answered"];
		$deleted  = $MailAry[$i]["deleted"];
		$seen  	  = $MailAry[$i]["seen"];
		$draft 	  = $MailAry[$i]["draft"];
		$Folder   = stripslashes($MailAry[$i]["Folder"]);
		$HaveAttachment = $MailAry[$i]["HaveAttachment"];
		if(isset($IMap->CacheMailToDB) && $IMap->CacheMailToDB == true){
			$ImportantFlag = $MailAry[$i]["Priority"]==1;
		}else{
			$ImportantFlag = $MailPriority[$Folder][$Uid]["ImportantFlag"]=='true';
		}
		# Get Folder Name
		$FolderDisplayName = IMap_Decode_Folder_Name($IMap->getMailDisplayFolder($Folder));
		
		
		if ($seen==1) 
        {
	        $RowStyle = "iMailrow";
	        $LinkStyle = "iMailsubject";
	        $SenderStyle = "iMailsender";
	        $StatusIcon = '<img border="0" align="absmiddle" alt="read" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_open_mail.gif">';
        }
        else 
		{
			$RowStyle = "iMailrowunread ";
	        $LinkStyle = "iMailsubjectunread";
	        $SenderStyle = "iMailsenderunread";
	        $StatusIcon = '<img border="0" align="absmiddle" alt="Unread" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_unread_mail.gif">';
        }
        
        if ($answered==1) 
        {
        	$repliedIcon = '<img border="0" align="absmiddle" alt="Replied" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_notification.gif">';
        }
        else
     	{
     		$repliedIcon = "<img src='$image_path/space.gif' width='1' height='1' border='0' />";
     	}
     	
     	# flagged /star
		$starPath = trim($flagged)?"icon_star_on.gif":"icon_star_off.gif";
		$starIcon = "<img class='mailflag' align='absmiddle' src='$image_path/$LAYOUT_SKIN/$starPath' border='0' style='cursor:pointer' onclick='setFlag(\"$Folder\",\"$Uid\", this)'/>";
     	
     	# importantFlag
		$importantIcon = '';
		if ($ImportantFlag) 
        	$importantIcon = '<img border="0" align="absmiddle" alt="Important" title="Important" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_important.gif">';
        else
     		$importantIcon = "<img src='$image_path/space.gif' width='1' height='1' border='0' />";
     	
     	# From / Recipient
     	if($Folder == $IMap->SentFolder || $Folder == $IMap->DraftFolder){
     		$ToArray = explode(",",$To);
     		$fromToTitle = $ToArray[0];
     	}else{
     		$fromToTitle = $From;
     	}
     	
     	# Subject
		$url  = ($Folder != $IMap->DefaultFolderList['DraftFolder']) ? "viewmail.php" : "compose_email.php";
		$url .= "?uid=".$Uid."&Folder=".urlencode($Folder)."&CurMenu=Search&CurTag=".urlencode($Folder);
		$url .= "&pageNo=".$pagingInfo["pageNo"]."&sort=".$pagingInfo["sort"]."&reverse=".$pagingInfo["reverse"]."&keyword=".$SearchInfo['keyword'];
		$url .= "&KeywordFrom=".$SearchInfo['From']."&KeywordTo=".$SearchInfo['To']."&KeywordCc=".$SearchInfo['Cc'];
		$url .= "&KeywordSubject=".$SearchInfo['Subject']."&FromDate=".$SearchInfo['FromDate']."&ToDate=".$SearchInfo['ToDate'];
		$url .= "&FromSearch=1&IsAdvanceSearch=".$IsAdvanceSearch."&".$params;
		if ($deleted!=1)
			$subjectPrint='<span style="-moz-user-select:text">&nbsp;<a href="'.$url.'" class="'.$LinkStyle.'"> '.$Subject.'</a>&nbsp;</span>';
     	else
     		$subjectPrint = $Subject;
     	$AttachmentIcon = $HaveAttachment?'<img border="0" align="absmiddle" alt="Attachment" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_attachment.gif">':"";
     	$subjectPrint = $AttachmentIcon.$subjectPrint;
     		
     	# Check Box
        $checkbox = "<input type='checkbox' value='$Uid,$Folder' name='Uid[]'>";
        
        #Format Date
        $date_ts = strtotime($Date);
        if($IMap->IsToday($Date))
			$DateFormat = "H:i";
		else
			$DateFormat = "Y-m-d";
		$displayDate = date($DateFormat, $date_ts);
        $fullDate = date("Y-m-d H:i:s", $date_ts);
        
        $x .= "<tr>\n";
        //$x .= "<td class='$RowStyle' ><img src='$image_path/space.gif' width='1' height='1' border='0' /></td>\n";
        $x.= $li->displayCell($j++,$StatusIcon, "$RowStyle tabletext","nowrap");
        $x.= $li->displayCell($j++,$starIcon, "$RowStyle tabletext","nowrap");
        $x.= $li->displayCell($j++,$importantIcon, "$RowStyle tabletext","nowrap");
        $x.= $li->displayCell($j++,$repliedIcon, "$RowStyle tabletext","nowrap");
		$x.= $li->displayCell($j++,'<span style="-moz-user-select:text">'.$fromToTitle.'</span>', "$RowStyle tabletext $SenderStyle");	 
		$x.= $li->displayCell($j++,$subjectPrint, "$RowStyle tabletext");
		$x.= $li->displayCell($j++,'<span style="display:none;">'.$fullDate.'</span>'.$displayDate, "$RowStyle tabletext");
		$x.= $li->displayCell($j++,$FolderDisplayName, "$RowStyle tabletext");
		$x.= $li->displayCell($j++,round($Size,0),"$RowStyle tabletext");
		$x.= $li->displayCell($j++,$checkbox, "$RowStyle tabletext","nowrap");
		$x .= "</tr>\n";

	}

}
/*
if(isset($IMap->CacheMailToDB) && $IMap->CacheMailToDB == true){
	
}else{
	$this->n_start = $start;
	$this->total_row = $MailList[1];
}
if($this->total_row<>0) $x .= "<tr><td colspan='".($this->no_col)."' class='tablebottom'>".$this->navigation()."</td></tr>";
$x .= "</table>\n";
*/
//$this->db_free_result();

echo $x;

imap_close($IMap->inbox);

intranet_closedb();
?>