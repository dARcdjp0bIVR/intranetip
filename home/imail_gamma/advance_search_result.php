<?php
# page modifing by:  Michael Cheung 
$PathRelative = "../../../../";
$CurSubFunction = "Communication";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");


if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}

AuthPage(array("Communication-Mail-GeneralUsage"));

include_once($PathRelative."src/include/class/imap_gamma.php");
include_once($PathRelative."src/include/class/imap_gamma_ui.php");
include_once($PathRelative."src/include/class/user_preference.php");
include_once($PathRelative."src/include/template/general_header.php");

###########################################################################################

## Use Library
$UPref = new user_preference($_SESSION['SSV_USERID']);
$IMap  = new imap_gamma();
$lui   = new imap_gamma_ui();


# Get the User Preference of Page Info
$numPerPageOld  = $UPref->MailPerPage;


## Get Data
$KeywordFrom 	= (isset($KeywordFrom) && $KeywordFrom != "") ? stripslashes(trim($KeywordFrom)) : "";
$KeywordTo		= (isset($KeywordTo) && $KeywordTo != "") ? stripslashes(trim($KeywordTo)) : "";
$KeywordCc 		= (isset($KeywordCc) && $KeywordCc != "") ? stripslashes(trim($KeywordCc)) : "";
$KeywordBcc 	= (isset($KeywordBcc) && $KeywordBcc != "") ? stripslashes(trim($KeywordBcc)) : "";
$KeywordSubject = (isset($KeywordSubject) && $KeywordSubject != "") ? stripslashes(trim($KeywordSubject)) : "";
$KeywordBody = (isset($KeywordBody) && $KeywordBody != "") ? stripslashes(trim($KeywordBody)) : "";
$KeywordAttachment = (isset($KeywordAttachment) && $KeywordAttachment != "") ? stripslashes(trim($KeywordAttachment)) : "";
$FromDate 		= (isset($FromDate) && $FromDate != "") ? trim($FromDate) : "";
$ToDate 		= (isset($ToDate) && $ToDate != "") ? trim($ToDate) : "";
$Folder 		= (isset($Folder))? stripslashes(urldecode($Folder)) : $SYS_CONFIG['Mail']['FolderPrefix'];

# get Folder to search
$SearchFolder 	= (isset($SearchFolder) && count($SearchFolder) > 0) ? $SearchFolder : $IMap->Get_All_Folder_List();
$SearchFolderHidden = '';
for ($i=0; $i <sizeof($SearchFolder) ; $i++) {
	$SearchFolderHidden .= $lui->Get_Input_Hidden("SearchFolder[]", "SearchFolder[]", stripslashes($SearchFolder[$i]))."\n";
}


# Page Info Data
$sort 		 = (isset($sort) && $sort != "") ? $sort : "DateSort";		// array sort fields: DateSort, From, Subject, Size, To, Folder
$reverse 	 = (isset($reverse) && $reverse != "") ? $reverse : 1;		// $order
$pageNo 	 = (isset($pageNo) && $pageNo != "") ? $pageNo : 1;
$numPerPage  = (isset($numPerPage) && $numPerPage != "") ? $numPerPage : 0;

# Update the NumPerPage of User Preference
if($numPerPageOld != "" && $numPerPageOld > 0){
	if($numPerPage != $numPerPageOld){
		if($numPerPage != "" && $numPerPage > 0){
			$UPref->Set_User_Mail_Per_Page($numPerPage);
		} else {
			$numPerPage = $numPerPageOld;
		}
	}
} else {
	$numPerPage = 50;
}


## Initialization
$CurTag 	= $Folder;
$CurMenu    = 1;
$SearchInfo = array();
$imapInfo   = array();

## Preparation
$FolderArray = $IMap->Get_Folder_Structure(true);		// get Folder Structure for select box

$SearchInfo['From'] 	= $KeywordFrom;
$SearchInfo['To'] 		= $KeywordTo;
$SearchInfo['Cc'] 		= $KeywordCc;
$SearchInfo['Bcc'] 		= $KeywordBcc;
$SearchInfo['Subject']  = $KeywordSubject;
$SearchInfo['FromDate'] = $FromDate;
$SearchInfo['ToDate'] 	= $ToDate;
$SearchInfo['Body'] = $KeywordBody;
$SearchInfo['Attachment'] = $KeywordAttachment;
$SearchInfo['Folder'] 	= $SearchFolder;

##############################################################################
# Imap use only
$imap_obj 	   = imap_check($IMap->inbox);
$unseen_status = imap_status($IMap->inbox, "{".$IMap->host."}".$Folder, SA_UNSEEN);
$statusInbox   = imap_status($IMap->inbox, "{".$IMap->host."}Inbox", SA_UNSEEN);

$imapInfo['imap_obj'] 	   = $imap_obj;
$imapInfo['unseen_status'] = $unseen_status;
$imapInfo['statusInbox']   = $statusInbox;
##############################################################################
	
### Get Search Mail List Data 
$pageStart = $numPerPage * ($pageNo - 1) + 1; 
$MailList = $IMap->Get_Search_Mail_List($pageStart, $numPerPage, $sort, $reverse, $SearchField, $SearchInfo);


## Main
echo $lui->Get_Div_Open('module_imail',  'class="module_content"');

# Top Bar
echo $lui->Get_Sub_Function_Header($Lang['email_gamma']['HeaderTitle'], $Msg);
echo $lui->Get_Switch_MailBox();

# Left Menu
echo $lui->Get_Left_Sub_Mail_Menu($CurMenu, $CurTag, $IMap, $Folder, $imapInfo, $keyword);

# Right Menu
echo $lui->Get_Div_Open("imail_right");
echo $lui->Get_Form_Open("form1", "POST");

# Generate Search Mail List Table
echo $lui->Gen_Search_Mail_List($IMap, $MailList, $pageNo, $numPerPage, $sort, $reverse, $SearchInfo);

# Hidden Value
echo $lui->Get_Input_Hidden("reverse", "reverse", $reverse)."\n";
echo $lui->Get_Input_Hidden("sort", "sort", $sort)."\n";
echo $lui->Get_Input_Hidden("Folder", "Folder", urlencode($Folder))."\n";
echo $lui->Get_Input_Hidden("CurTag", "CurTag", $CurTag)."\n";
echo $lui->Get_Input_Hidden("pageNo", "pageNo", $pageNo)."\n";
echo $lui->Get_Input_Hidden("numPerPage", "numPerPage", $numPerPage)."\n";
echo $lui->Get_Input_Hidden("DestinationFolder", "DestinationFolder")."\n";

# Hidden Values - Advance Search Field
echo $lui->Get_Input_Hidden("FromSearch", "FromSearch", 1)."\n";
echo $lui->Get_Input_Hidden("KeywordFrom", "KeywordFrom", $KeywordFrom)."\n";
echo $lui->Get_Input_Hidden("KeywordTo", "KeywordTo", $KeywordTo)."\n";
echo $lui->Get_Input_Hidden("KeywordCc", "KeywordCc", $KeywordCc)."\n";
echo $lui->Get_Input_Hidden("KeywordBcc", "KeywordBcc", $KeywordBcc)."\n";
echo $lui->Get_Input_Hidden("KeywordSubject", "KeywordSubject", $KeywordSubject)."\n";
echo $lui->Get_Input_Hidden("FromDate", "FromDate", $FromDate)."\n";
echo $lui->Get_Input_Hidden("ToDate", "ToDate", $ToDate)."\n";
echo $SearchFolderHidden;

echo $lui->Get_Form_Close();
echo $lui->Get_Div_Close();

echo $lui->Get_Div_Close();


imap_close($IMap->inbox);
###########################################################################################
include_once($PathRelative."src/include/template/general_footer.php");

?>