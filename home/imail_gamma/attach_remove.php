<?php
// page modifing by: 
$PATH_WRT_ROOT = "../../";
//$CurSubFunction = "Communication";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

set_time_limit(0);
intranet_auth();
intranet_opendb();
auth_campusmail();
################################################################################################

## Use Library
$lui = new interface_html();
$lfs = new libfilesystem();
$ldb = new libdb();

## Get Data
$FileID = $_GET['FileID'];
$MailAction = (isset($_POST['MailAction']) && $_POST['MailAction'] != "") ? $_POST['MailAction'] : "";
$DraftUID   = (isset($_POST['DraftUID']) && $_POST['DraftUID'] != "") ? $_POST['DraftUID'] : "";
$ActionUID  = (isset($_POST['ActionUID']) && $_POST['ActionUID'] != "") ? $_POST['ActionUID'] : "";
$MessageUID = ($MailAction == "Forward") ? $ActionUID : $DraftUID;

$ComposeTime = $_POST['ComposeTime'];
$ComposeDatetime = date("Y-m-d H:i:s",$ComposeTime);

## Preparation
//$personal_path = $SYS_CONFIG['sys_user_file']."/file/mail/u".$_SESSION['SSV_USERID'];
$personal_path = "$file_path/file/gamma_mail/u$UserID";
if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}


## Main 
# Get File Info of FileID provided so as to check whether it belongs to draft mail or not
$sql = 'Select 
			FileID, DraftMailUID 
		from 
			MAIL_ATTACH_FILE_MAP
		where 
			FileID = \''.$FileID.'\'';
$result = $ldb->returnArray($sql,2);

if (sizeof($result) > 0) {
	if (is_null($result[0][1])) {
		// select file list to delete
		$sql = 'SELECT 
					FileID, EncodeFileName
				FROM 
					MAIL_ATTACH_FILE_MAP 
				where 
					FileID = \''.$FileID.'\'';
		$DeleteList = $ldb->returnArray($sql,2);
	
		// delete the file in file system
		for($i=0;$i< sizeof($DeleteList);$i++){
			list($FileID,$FileName) = $DeleteList[$i];
			$loc = $personal_path."/".$FileName;
			
			$lfs->file_remove($loc);
		}
	
		$sql = 'Delete From MAIL_ATTACH_FILE_MAP WHERE FileID = \''.$FileID.'\'';
	}
	else {	// what is the use ?
		$sql = 'Update 
					MAIL_ATTACH_FILE_MAP 
				SET
					DraftMailUID = -1, 
					OrgDraftMailUID = \''.$DraftUID.'\' 
				WHERE
					FileID = \''.$FileID.'\' AND 
					DraftMailUID = \''.$DraftUID.'\'';
	}
	$ldb->db_db_query($sql);
}

$sql = "SELECT 
			FileID, UserID, UploadTime, DraftMailUID,
			OriginalFileName,
			EncodeFileName 
		FROM 
			MAIL_ATTACH_FILE_MAP 
		WHERE 
			UserID = '".$UserID."' AND 
			(
				(DraftMailUID IS NULL AND OrgDraftMailUID IS NULL) OR 
				 DraftMailUID = '$MessageUID' OR 
				(DraftMailUID IS NULL AND OrgDraftMailUID = '$MessageUID') 
			)
			AND DATE_FORMAT(ComposeTime,'%Y-%m-%d %H:%i:%s') = '$ComposeDatetime' ";
					
$AttachList = $ldb->returnArray($sql,6);
?>
<span id="hidden_filename" style='visibility:hidden'></span>

<script language="JavaScript1.2">
<?

if (sizeof($AttachList) > 0) {
?>

function format_filename(filename){
	return filename.replace(/&amp;/g,'&');
}
par = window.parent;
ObjAttachedDisplay = par.document.getElementById("AttachmentList");

var AttachmentList = "";
AttachmentList = "<ul>";
<?
for ($i=0; $i< sizeof($AttachList); $i++) {
	$FileSize = (round(filesize($personal_path."/".$AttachList[$i][5])/1024,2));
		
	$AttachDisplay  = '<input type="checkbox" id="FileID[]" name="FileID[]" value="'.$AttachList[$i][0].'" checked style="visiblity:hidden; display:none;">';
	$AttachDisplay .= '<a class="tabletool" href="view_compose_attachment.php?FileID='.$AttachList[$i][0].'">';
	$AttachDisplay .= str_replace("'","\'",$AttachList[$i][4]).'('.$FileSize.'K)';
	$AttachDisplay .= '</a>';
	$AttachDisplay .= '[<a class="tabletool" href="javascript:void(0);" onclick="FileDeleteSubmit(\\\''.$AttachList[$i][0].'\\\')">'.$Lang['Gamma']['RemoveAttachment'].'</a>]';
	$AttachDisplay .= '<input type="hidden" id="FileSize[]" name="FileSize[]" value="'.$FileSize.'">';
	$AttachDisplay .= '<br>';
	?>
	AttachmentList += '<?=$AttachDisplay?>';
<?
}
?>
AttachmentList += "</ul>";
ObjAttachedDisplay.innerHTML = AttachmentList;
<?php 
}
else {
?>
	par = window.parent;
	ObjAttachedDisplay = par.document.getElementById("AttachmentList");
	
	ObjAttachedDisplay.innerHTML = "";
<?
}
####################################################################################################
intranet_closedb();
?>
	window.close();
</script>