<?
// page modifing by: 
$PathRelative = "../../../../";
$CurSubFunction = "Communication";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");
include_once($PathRelative."src/include/class/imap_gamma.php");
include_once($PathRelative."src/include/class/imap_gamma_ui.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}
AuthPage(array("Communication-Mail-GeneralUsage"));

// UI instance
$lui = new imap_gamma_ui();

// fetch info 
$MessageID = $_GET['MessageID'];

// Get Folder Select
$Folder = urldecode($_GET['Folder']);

$IMap = new imap_gamma();

$EmailContent = $IMap->getMailRecord($Folder,$MessageID);
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?
/*include_once($PathRelative."theme/css/common.css.php");
include_once($PathRelative."theme/css/layout_grey.css.php");
include_once($PathRelative."theme/css/font_size_normal.css.php");
include_once($PathRelative."theme/css/font_face_verdana.css.php");
include_once($PathRelative."theme/css/font_color_layout_grey.css.php");*/
?>
<base target="_blank"/>
</head>
<script>
function resize_iframe() {
	scroll_size = 10;
	iframeObj = this.parent.document.getElementById("MailBody");
	if(iframeObj==null) return;
	/*ch= this.document.body.clientHeight;
	cw = this.document.body.clientWidth;*/
	// Firefox 
	if( window.innerHeight && window.scrollMaxY ) {
		pageWidth = window.innerWidth + window.scrollMaxX;
		pageHeight = window.innerHeight + window.scrollMaxY;
	}
	// all but Explorer Mac
	else if( document.body.scrollHeight > document.body.offsetHeight ) {
		pageWidth = document.body.scrollWidth;
		pageHeight = document.body.scrollHeight;
	}
	// works in Explorer 6 Strict, Mozilla (not FF) and Safari
	else {
		pageWidth = document.body.offsetWidth + document.body.offsetLeft; 
		pageHeight = document.body.offsetHeight + document.body.offsetTop; 
	}
			
	/*this.scroll(scroll_size,scroll_size);	
	while(this.document.body.scrollTop>0 || this.document.body.scrollLeft > 0 || ch < 640)
	{
		ch+=scroll_size;
		cw+=scroll_size;
		this.scroll(scroll_size,scroll_size);
	}	*/	
	
	if (pageHeight > 640) {
		iframeObj.style.height = 640;	
		//iframeObj.style.width = pageWidth;
	}
	else {
		iframeObj.style.height = pageHeight;	
		//iframeObj.style.width = pageWidth;
	}
}
</script>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff" onload="resize_iframe();">
<?
$x .= $lui->Get_Div_Open("mail_form_content_board",'STYLE="border:0px;"');
if ($EmailContent[0]["is_html_message"] == (-1)) {
	$OutputContent = str_replace(chr(10),'<br>',$EmailContent[0]['text_message']);
	$x .= $OutputContent;
}
else {
	$x .= $EmailContent[0]['message'];
}
$x .= $lui->Get_Div_Close();

echo $x;
intranet_closedb();
?>
</body>
</html>