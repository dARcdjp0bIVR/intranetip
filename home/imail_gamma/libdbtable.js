function libdbtable()
{
	var self = this;
	
	self.field_array = [];
	self.column_list = '';
	self.IsColOff = '';
	self.no_col= 11;
	self.total_row = 0;
	self.pageNo = 1;
	self.page_size = 20;
	self.n_start = (self.pageNo-1) * self.page_size;
	self.title = '';
	self.field = 0;
	self.order = 0;
	self.sortby = '';
	self.total = '';
	self.pagePrev = '';
	self.pageNext = '';
	self.onChangeSubmit = '';
	self.form_name = 'form1';
	self.pagename = '';
	self.pageNo_name = '';
	self.image_path = '/images';
	self.LAYOUT_SKIN = '2009a';
	self.pageSizeChangeEnabled = 1;
	self.i_general_EachDisplay = '';
	self.i_general_PerPage = '';
	self.i_general_record = '';
	self.i_email_message = '';
	self.i_adminmenu_group = '';
	
	self.column = function(field_index,field_name){
		var x = '';
		var _image_path = self.image_path;
		if (self.IsColOff=="staff")
        {
        	_image_path="/images/staffattend";
        }

        if(self.field==field_index)
        {
        	//Change arrow display
           	if(self.order==1) x += '<a class="'+self.returnColumnClassID()+'" href="javascript:sortPage(0,'+field_index+',document.'+self.form_name+')" onMouseOver="window.status=\''+self.sortby+' '+field_name+'\';MM_swapImage(\'sort_icon\',\'\',\'' + _image_path+'/'+self.LAYOUT_SKIN+'/icon_sort_a_on.gif\',1);return true;" onMouseOut="window.status=\'\';MM_swapImgRestore();return true;" title="'+self.sortby+' '+field_name+'" >';
            if(self.order==0) x += '<a class="'+self.returnColumnClassID()+'" href="javascript:sortPage(1,'+field_index+',document.'+self.form_name+')" onMouseOver="window.status=\''+self.sortby+' '+field_name+'\';MM_swapImage(\'sort_icon\',\'\',\'' + _image_path+'/'+self.LAYOUT_SKIN+'/icon_sort_d_on.gif\',1);return true;" onMouseOut="window.status=\'\';MM_swapImgRestore();return true;" title="'+self.sortby+' '+field_name+'" >';

        } else
        {
            x += '<a class="'+self.returnColumnClassID()+'" href="javascript:sortPage('+self.order+','+field_index+',document.'+self.form_name+')" onMouseOver="window.status=\''+self.sortby+' '+field_name+'\';return true;" onMouseOut="window.status=\'\';return true;" title="'+self.sortby+' '+field_name+'" >';
        }
        x += field_name.replace(/_/gi," ");
        if (self.field==field_index)
        {
        	//Change arrow display
        	if(self.order==1) x += '<img name="sort_icon" id="sort_icon" src="'+_image_path+'/'+self.LAYOUT_SKIN+'/icon_sort_a_off.gif" align="absmiddle" border="0" />';
			if(self.order==0) x += '<img name="sort_icon" id="sort_icon" onMouseOver="MM_swapImage(\'sort_asc\',\'\',\''+_image_path+'/'+self.LAYOUT_SKIN+'/icon_sort_d_on.gif\',1)" onMouseOut="MM_swapImgRestore()" src="'+_image_path+'/'+self.LAYOUT_SKIN+'/icon_sort_d_off.gif" align="absmiddle" border="0" />';
        }
        x += '</a>';
        return x;
	};
	
	self.returnColumnClassID = function(){
		var x = '';
		if (self.IsColOff=="eCommForumList")
	    	x = "forumtabletoplink";
	    else
	        x = "tabletoplink";
	    return x;
	};
	
	self.displayColumn = function(param_css){
		var x = '';
		var css = param_css? param_css : 'tabletop';
		
		x += '<tr class="'+ css + '">';
		x += (self.IsColOff==1 || self.IsColOff==4 || self.IsColOff==8 ) ? '<td width="15">' : '';
		x += (self.IsColOff=="imail" ) ? '<td width="1">&nbsp;</td>':'';
		x += self.column_list;
		x += (self.IsColOff == 12) ? self.column_list : '';
		x += (self.IsColOff==1 || self.IsColOff==4 || self.IsColOff==8 ) ? '<td width="15">':'';
		x += (self.IsColOff=="imail") ? '<td width="1">&nbsp;</td>':'';
		x += '</tr>';
		
		return x;
	};
	
	self.header = function(){
		return self.displayColumn();
	};
	
	self.navigation = function(){
		var x = '', n_page='', n_total = '', n_size='';
		x += '<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tabletext">';
		x += '<tr>';
			x += '<td width="100%" >';
			x += self.record_range();
			x += '</td><td nowrap="nowrap">';

			//$x .= $li->prev_n();
			//$previous_icon = "<img src=$image_pre border=0 hspace=2 vspace=0 align=middle>";
			n_page = self.pageNo;
	    	n_total = Math.ceil(self.total_row/self.page_size);
	        n_size = self.page_size;
	        x += (n_page==1) ? '&nbsp;<img src="'+self.image_path+'/'+self.LAYOUT_SKIN+'/icon_prev_off.gif" name="page_previous" id="page_previous" width="11" height="10" border="0" hspace="3" align="absmiddle" />&nbsp;' :
	                             '<a href="javascript:document.getElementById(\'pageNo\').value=\''+(parseInt(n_page)-1)+'\';'+self.onChangeSubmit+'" class="tablebottomlink" onMouseOver="MM_swapImage(\'page_previous\',\'\',\''+self.image_path+'/'+self.LAYOUT_SKIN+'/icon_prev_on.gif\',1)" onMouseOut="MM_swapImgRestore()">&nbsp;<img src="'+self.image_path+'/'+self.LAYOUT_SKIN+'/icon_prev_off.gif" name="page_previous" id="page_previous" width="11" height="10" border="0" align="absmiddle" title="'+self.pagePrev+'" />&nbsp;</a>';
		
			// $x .= $li->go_page();
			n_page=self.pageNo;
	        n_total=Math.ceil(self.total_row/self.page_size);
	        x += self.pagename + ' <select class="formtextbox" onChange="document.form1.'+self.pageNo_name+'.value=this.options[this.selectedIndex].value;'+self.onChangeSubmit+'">';
		    for(var i=1;i<=n_total;i++){
		    	x += (n_page==i) ? '<option value="'+i+'" selected>'+i+'</option>' : '<option value="'+i+'">'+i+'</option>';
		    }
		    x += '</select>';
			
			//$x .= $li->next_n();
			//$next_icon = "<img src=$image_next border=0 hspace=2 vspace=0 align=middle>";
	        n_page = self.pageNo;
		    n_total = Math.ceil(self.total_row/self.page_size);
		    n_size = self.page_size;
	        x += (n_page==n_total) ? '&nbsp;<img src="'+self.image_path+'/'+self.LAYOUT_SKIN+'/icon_next_off.gif" name="page_next" id="page_next" width="11" height="10" border="0" hspace="3" align="absmiddle" />&nbsp;':
		                                        '<a href="javascript:document.getElementById(\'pageNo\').value=\''+(parseInt(n_page)+1)+'\';'+self.onChangeSubmit+'" class="tablebottomlink" onMouseOver="MM_swapImage(\'page_next\',\'\',\''+self.image_path+'/'+self.LAYOUT_SKIN+'/icon_next_on.gif\',1)" onMouseOut="MM_swapImgRestore()">&nbsp;<img src="'+self.image_path+'/'+self.LAYOUT_SKIN+'/icon_next_off.gif" name="page_next" id="page_next" width="11" height="10" border="0" align="absmiddle" title="'+self.pageNext+'" />&nbsp;</a>';
				
		if (self.pageSizeChangeEnabled)
		{
		    //$x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
		    //$x .= $li->page_size_input();
		     x += self.i_general_EachDisplay+' <select name="num_per_page" class="formtextbox" onChange="document.form1.pageNo.value=1;document.form1.page_size_change.value=1;document.form1.numPerPage.value=this.options[this.selectedIndex].value;'+self.onChangeSubmit+'">';
		     x += '<option value="10" '+(self.page_size==10? 'SELECTED':'')+'>10</option>';
		     x += '<option value="20" '+(self.page_size==20? 'SELECTED':'')+'>20</option>';
		     x += '<option value="30" '+(self.page_size==30? 'SELECTED':'')+'>30</option>';
		     x += '<option value="40" '+(self.page_size==40? 'SELECTED':'')+'>40</option>';
		     x += '<option value="50" '+(self.page_size==50? 'SELECTED':'')+'>50</option>';
		     x += '<option value="60" '+(self.page_size==60? 'SELECTED':'')+'>60</option>';
		     x += '<option value="70" '+(self.page_size==70? 'SELECTED':'')+'>70</option>';
		     x += '<option value="80" '+(self.page_size==80? 'SELECTED':'')+'>80</option>';
		     x += '<option value="90" '+(self.page_size==90? 'SELECTED':'')+'>90</option>';
		     x += '<option value="100" '+(self.page_size==100? 'SELECTED':'')+'>100</option>';
		     x += '</select>'+self.i_general_PerPage;
		}
		x += '&nbsp;</td>';
		x += '</tr>';
		x += '</table>';
		
		return x;
	};
	
	self.body = function(){
		
	};
	
	self.record_range = function(){
		var x = '';
		var n_title = (self.title == '')? self.i_general_record : self.title;
        var n_start = self.n_start+1;
        var n_end = Math.min(self.total_row,(self.pageNo*self.page_size));
        var n_total = self.total_row;
		
        switch (self.IsColOff)
        {
            case "imail":
            case "imail_gamma":
            case "imail_gamma_search":
                x = self.i_email_message + ' ' + n_start + ' - ' + n_end + ' / ' + self.total + ' '+n_total;
                break;
            case "imail_addressbook":
                x = self.i_adminmenu_group + ' ' + n_start + ' - ' + n_end + ' / ' + self.total + ' ' + n_total;
                break;
            default:
                x = n_title + ' ' + n_start + ' - ' + n_end+ ', ' + self.total + ' ' + n_total;
                break;
        }
        return x;
	};
};