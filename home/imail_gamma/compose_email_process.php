<?php
// page modifing by :
/************************************* Change Log *******************************************
 * 2020-05-27 (Henry): addslashes after Base64 decode EncodedMailBody
 * 2020-05-18 (Sam): Encode MailBody to Base64 to avoid WAF blocking
 * 2020-03-16 (Sam): Save a email copy to sent box even though the send result is failed [N181620]
 * 2016-03-01 (Carlos): Do stripslashes() for To, Cc, Bcc recipient input to remove extra backward slashes. 
 * 2015-05-29 (Carlos): replace comma separator to semi-colon separator with Replace_Display_Comma() for the To, Cc, Bcc values.
 * 2015-04-20 (Carlos): Split To/Cc/Bcc recipients with imap_gamma->splitEmailStringIntoArray()
 * 2015-03-05 (Carlos): When use bcc to send email, do not implicitly add Undisclosed-Recipient<noreply@domain> to [To], left it blank. 
 * 						As some servers would get fail result from mail() and save to draft. 
 * 2013-11-01 (Cameron): Change to combine To, Cc and Bcc into Bcc if $sys_custom['iMailSendToParent'] == true
 * 2013-08-01 (Carlos): added return $msg=9	when fail to save sent copy
 * 2013-04-09 (Carlos): clean plupload temp folder
 * 2012-11-20 (Carlos): changed go back to current mail's located folder, if compose new mail go to INBOX
 * 2012-05-10 (Carlos): truncate invlaid text after > if recipient email is in format <mailbox@host> to avoid bounce back mail
 * 2012-05-07 (Carlos): modified inline image path - add contentID as subfolder
 * 2012-03-20 (Carlos): added duplication checking for each To, Cc, Bcc recipients
 * 2011-10-03 (Carlos): save sent copy to switched mail account if sender is changed
 * 2011-08-10 (Carlos): fixed switch sender field failed to refer to replied/forwarded mail  
 * 2011-04-15 (Carlos): added logic to send mail by BCC in several batches if number of recipients > LIMIT
 * 2011-04-11 (Carlos): modified after sent mail, return to inbox
 ********************************************************************************************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

# Logging Purpose
$PageStartTime = Get_Timer();
##############################################################################

## User Library
$lfs  = new libfilesystem();
$ldb  = new libdb();
$IMap = new imap_gamma();
$user = new libuser($UserID);
$lui->logTime['Compose_Mail_Page'] = $PageStartTime;

//$use_magic_quote = get_magic_quotes_gpc();

## Get Data 
# Mail Data
$UID = (trim($_POST['MailAction']) != "")? $_POST['ActionUID']:$_POST['DraftUID'];
/*$DraftUID 	= (isset($uid) && $uid != "") ? $uid : "";							// Get Draft mail UID
$MailAction = (isset($MailAction) && $MailAction != "") ? $MailAction : "";		// Get Mail Action 
$ActionUID 	= (isset($ActionUID) && $ActionUID != "") ? $ActionUID : "";		// Mail to be replied/ forwarded*/
$FileID 	= (isset($FileID)) ? implode(",", $FileID) : ""; // User Attachment File to be sent
$AttachPartNumber = $_POST['AttachPartNumber']; // Forward Attachment File to be sent (on Forward)
$TNEFFile = $_POST['TNEFFile']; // winmail.dat extracted Attachment File to be sent (on Forward)
$Folder 	= (isset($Folder) && $Folder != "") ? stripslashes(urldecode($Folder)) : $SYS_CONFIG['Mail']['FolderPrefix'];	// Get Current Folder
$FromSearch = (isset($FromSearch) && $FromSearch != "") ? $FromSearch : 0;
$MessageUID = $DraftUID;
$PartNumber = $_POST['PartNumber'];

# Page Info Data
$sort 		= (isset($sort) && $sort != "") ? $sort : (($FromSearch == 1) ? "SortDate" : "SORTARRIVAL");
$pageNo     = (isset($pageNo) && $pageNo != "")? $pageNo : 1;
$reverse    = (isset($reverse) && $reverse != "")? $reverse : 1;

if($FromSearch == 1){
	$IsAdvanceSearch = (isset($IsAdvanceSearch) && $IsAdvanceSearch != "") ? $IsAdvanceSearch : 1;
} else {
	$IsAdvanceSearch = '';
}

# Get Data From Simple Search
$keyword 	    = (isset($keyword) && $keyword != "") ? stripslashes(trim($keyword)) : "";

# Get Data From Advance Search
$KeywordFrom 	= (isset($KeywordFrom) && $KeywordFrom != "") ? stripslashes(trim($KeywordFrom)) : '';
$KeywordTo   	= (isset($KeywordTo) && $KeywordTo != "") ? stripslashes(trim($KeywordTo)) : '';
$KeywordCc   	= (isset($KeywordCc) && $KeywordCc != "") ? stripslashes(trim($KeywordCc)) : '';
$KeywordSubject = (isset($KeywordSubject) && $KeywordSubject != "") ? stripslashes(trim($KeywordSubject)) : '';
$FromDate  		= (isset($FromDate) && $FromDate != "") ? trim($FromDate) : '';
$ToDate  		= (isset($ToDate) && $ToDate != "") ? trim($ToDate) : '';
$SearchFolder   = (isset($SearchFolder) && is_array($SearchFolder) && count($SearchFolder) > 0) ? $SearchFolder : array();

$ComposeTime = $_POST['ComposeTime'];
$ComposeDatetime = date("Y-m-d H:i:s",$ComposeTime);

//$ImapUserEmail = $user->ImapUserEmail;

## Preparation
//$personal_path = $SYS_CONFIG['sys_user_file']."/file/mail/u".$_SESSION['SSV_USERID'];
$personal_path = "$file_path/file/gamma_mail/u$UserID";

if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}


# Get the File Attachement List - the files, which are uploaded before, are related to this email
if ($FileID != "") {
	$sql = 'Select 
				FileID, 
				OriginalFileName,
				EncodeFileName 
			from 
				MAIL_ATTACH_FILE_MAP 
			where 
				FileID in ('.$FileID.')';
	$AttachList = $ldb->returnArray($sql,3);
	
	for ($i=0; $i< sizeof($AttachList); $i++) {
		$AttachList[$i][2] = $personal_path."/".$AttachList[$i][2];
		$AttachList[$i][1] = $AttachList[$i][1];
	}
}

# Get related inline image list
$personal_path = $PATH_WRT_ROOT."file/gamma_mail/u$UserID";
if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}
$personal_related_path = $personal_path."/related";
if (!is_dir($personal_path))
{
    $lfs->folder_new($personal_path);
}
$RelatedList=array();
$sql = "SELECT 
			FileID,
			OriginalImageFileName as ImageName,
			ContentID  
		FROM MAIL_INLINE_IMAGE_MAP 
		WHERE UserID = '$UserID' AND DATE_FORMAT(ComposeTime,'%Y-%m-%d %H:%i:%s') = '$ComposeDatetime' ";
$RelatedFileList = $ldb->returnArray($sql,2);
for($i=0;$i<sizeof($RelatedFileList);$i++)
{
	$RelatedList[$i]['FilePath'] = $personal_related_path."/".$RelatedFileList[$i]['ContentID']."/".$RelatedFileList[$i]['ImageName'];
	$RelatedList[$i]['FileContentID'] = $RelatedFileList[$i]['ContentID'];
}

# clean temp plupload folders
$temp_folder = $personal_path."/".$ComposeTime;
$lfs->deleteDirectory($temp_folder);
$sql = "DELETE FROM MAIL_TEMP_UPLOAD_FOLDER WHERE UserID='".$_SESSION['UserID']."' AND ComposeTime='$ComposeTime'";
$ldb->db_db_query($sql);


/* ----- User ----- */
//if ($user->Email == $IMap->CurUserAddress) {
//	$OrgAlias = $user->Get_Default_Alias();
//	$From = ($OrgAlias !== false) ? $OrgAlias : $IMap->CurUserAddress;
//} else {
//	$From = $IMap->CurUserAddress;
//}
if(isset($_POST['FromSharedMailBox']) && trim($_POST['FromSharedMailBox'])!='')
{
	$From = $_POST['FromSharedMailBox'];
//	$_SESSION['SSV_EMAIL_LOGIN']    = $From;
//	$_SESSION['SSV_EMAIL_PASSWORD'] = $IMap->Get_MailBox_Password($From);
//	$_SESSION['SSV_LOGIN_EMAIL']    = $From;
//	$IMap = new imap_gamma();
}else
{
	// Normal flow
	if($user->ImapUserEmail == $IMap->CurUserAddress){
		$From = $user->ImapUserEmail;
	}else{
		$From = $IMap->CurUserAddress;
	}
}

/* ----- Mail Server ----- */
// temp comment by marcus 20100226
//$Preference = $IMap->Get_Preference();
$RawSubject = str_replace("\'","'",$_POST['Subject']);
$RawSubject = str_replace('\"','"',$RawSubject);

// $To = (substr($_POST['ToAddress'],-1) == ';') ? substr($_POST['ToAddress'], 0, -1) : $_POST['ToAddress'];
$To = (substr($_POST['ToAddress'],-1) != ';') ? $_POST['ToAddress'].';' : $_POST['ToAddress'];
////$To = Replace_Display_Comma($To);
////$PreToArray = explode(";",$To);

//if($use_magic_quote){
	$To = stripslashes($To);
//}
$To = Replace_Display_Comma($To);
$TmpTo = $IMap->splitEmailStringIntoArray($To);
$PreToArray = $TmpTo[0];

// $Cc = (substr($_POST['CcAddress'],-1) == ';') ? substr($_POST['CcAddress'], 0, -1) : $_POST['CcAddress'];
$Cc = (substr($_POST['CcAddress'],-1) != ';') ? $_POST['CcAddress'].';' : $_POST['CcAddress'];
////$Cc = Replace_Display_Comma($Cc);
////$PreCcArray = explode(";",$Cc);
//if($use_magic_quote){
	$Cc = stripslashes($Cc);
//}
$Cc = Replace_Display_Comma($Cc);
$TmpCc = $IMap->splitEmailStringIntoArray($Cc);
$PreCcArray = $TmpCc[0];

// $Bcc = (substr($_POST['BccAddress'],-1) == ';') ? substr($_POST['BccAddress'], 0, -1) : $_POST['BccAddress'];
$Bcc = (substr($_POST['BccAddress'],-1) != ';') ? $_POST['BccAddress'].';' : $_POST['BccAddress'];
////$Bcc = Replace_Display_Comma($Bcc);
////$PreBccArray = explode(";",$Bcc);
//if($use_magic_quote){
	$Bcc = stripslashes($Bcc);
//}
$Bcc = Replace_Display_Comma($Bcc);
$TmpBcc = $IMap->splitEmailStringIntoArray($Bcc);
$PreBccArray = $TmpBcc[0];

// $RawBody 	= str_replace("\'","'",$_POST['MailBody']);
                                   // WAF workaround
$RawBody 	= str_replace("\'","'",addslashes(base64_decode($_POST['EncodedMailBody'])));
$Body 		= str_replace('\"','"',$RawBody);
$ImportantFlag = $_POST['ImportantFlag'];

# add email address to "To" field if to and cc field is null and bcc is not null
if($To == "" && $Cc == "" && $Bcc != "") {	
	$ToArray[0] = "Undisclosed-Recipient<noreply@".$SYS_CONFIG['Mail']['UserNameSubfix'].">";	
}

function RemoveComma($email)
{
	$firstQuote = strpos($email, '"'); 
	if ($firstQuote === false) return $email;
	
	$secondQuote = strpos($email, '"', $firstQuote+1); 
	if ($secondQuote === false) return $email;
}


### Bounce Back Email Logic - Added By Michael Cheung (2009-09-28) ###
$ToArrayEmails = array(); // use for checking duplication
$CcArrayEmails = array(); // use for checking duplication
$BccArrayEmails = array(); // use for checking duplication

$EmptyEmail = null;
$AllEmailList = array();
$ReceiptEmailList=array();// For adding receipt records to track sent mail's read status 
if (trim($_POST['ToAddress']) != '') {
	foreach ($PreToArray as $Key => $ToEmail) {
		// echo str_replace('\"','',substr_replace(trim($ToEmail), '', stripos(trim($ToEmail),'<'))).'<br>';
		$ToEmail = trim($ToEmail);
		if ($ToEmail != '') {
			if(strstr($ToEmail,"<")&&strstr($ToEmail,">")){
				$EmailAddr = substr($ToEmail,strrpos($ToEmail,"<")+1,strrpos($ToEmail,">")-strrpos($ToEmail,"<")-1);
				$ToEmail = substr($ToEmail,0,strrpos($ToEmail,">")+1);
			}else{
				$EmailAddr = $ToEmail;
			}
			if(!isset($ToArrayEmails[$EmailAddr])){ // set if not duplicated recipient
				$ToArrayEmails[$EmailAddr] = $ToEmail;
				
				if (!stristr($ToEmail,'@')) {
					$EmptyEmail[] = str_replace('\"','',substr_replace(trim($ToEmail), '', stripos(trim($ToEmail),'<')));
				} else {
					$ToArray[] = $ToEmail;
				}
				
				if(sizeof((array)$_COOKIE['Gamma']['AutoFill'])<19){	
					@setcookie("Gamma[AutoFill][".$EmailAddr."]",stripslashes($ToEmail),time()+60*60*24*30,"/home/imail_gamma/");
					$_COOKIE['Gamma']['AutoFill'][$EmailAddr] = stripslashes($ToEmail);
				}else if(isset($_COOKIE['Gamma']['AutoFill'][$EmailAddr])){
					@setcookie("Gamma[AutoFill][".$EmailAddr."]","",time()-60*60*24*30,"/home/imail_gamma/");
					unset($_COOKIE['Gamma']['AutoFill'][$EmailAddr]);
				}
				$ReceiptEmailList[] = $EmailAddr;
			
			}
		}
	}
}
if (trim($_POST['CcAddress']) != '') {
	foreach ($PreCcArray as $Key => $CcEmail) {
		// echo str_replace('\"','',substr_replace(trim($CcEmail), '', stripos(trim($CcEmail),'<'))).'<br>';
		$CcEmail = trim($CcEmail);
		if (trim($CcEmail) != '') {
			if(strstr($CcEmail,"<")&&strstr($CcEmail,">")){
				$EmailAddr = substr($CcEmail,strrpos($CcEmail,"<")+1,strrpos($CcEmail,">")-strrpos($CcEmail,"<")-1);
				$CcEmail = substr($CcEmail,0,strrpos($CcEmail,">")+1);
			}else{
				$EmailAddr = $CcEmail;
			}	
			if(!isset($CcArrayEmails[$EmailAddr])){ // set if not duplicated recipient
				$CcArrayEmails[$EmailAddr] = $CcEmail;
				
				if (!stristr($CcEmail,'@')) {
					$EmptyEmail[] = str_replace('\"','',substr_replace(trim($CcEmail), '', stripos(trim($CcEmail),'<')));
				} else {
					$CcArray[] = $CcEmail;
				}
				
				if(sizeof((array)$_COOKIE['Gamma']['AutoFill'])<19){
					@setcookie("Gamma[AutoFill][$EmailAddr]",stripslashes($CcEmail),time()+60*60*24*30,"/home/imail_gamma/");
					$_COOKIE['Gamma']['AutoFill'][$EmailAddr] = stripslashes($CcEmail);
				}else if(isset($_COOKIE['Gamma']['AutoFill'][$EmailAddr])){
					@setcookie("Gamma[AutoFill][".$EmailAddr."]","",time()-60*60*24*30,"/home/imail_gamma/");
					unset($_COOKIE['Gamma']['AutoFill'][$EmailAddr]);
				}
				$ReceiptEmailList[] = $EmailAddr;
			
			}
		}
		
	}
}
if (trim($_POST['BccAddress']) != '') {
	foreach ($PreBccArray as $Key => $BccEmail) {
		// echo str_replace('\"','',substr_replace(trim($BccEmail), '', stripos(trim($BccEmail),'<'))).'<br>';
		$BccEmail = trim($BccEmail);
		if (trim($BccEmail) != '') {
			if(strstr($BccEmail,"<")&&strstr($BccEmail,">")){
				$EmailAddr = substr($BccEmail,strrpos($BccEmail,"<")+1,strrpos($BccEmail,">")-strrpos($BccEmail,"<")-1);
				$BccEmail = substr($BccEmail,0,strrpos($BccEmail,">")+1);
			}else{
				$EmailAddr = $BccEmail;
			}
			if(!isset($BccArrayEmails[$EmailAddr])){ // set if not duplicated recipient
				$BccArrayEmails[$EmailAddr] = $BccEmail;
				
				if (!stristr($BccEmail,'@')) {
					$EmptyEmail[] = str_replace('\"','',substr_replace(trim($BccEmail), '', stripos(trim($BccEmail),'<')));
				} else {
					$BccArray[] = $BccEmail;
				}
				
				if(sizeof((array)$_COOKIE['Gamma']['AutoFill'])<19){
					@setcookie("Gamma[AutoFill][$EmailAddr]",stripslashes($BccEmail),time()+60*60*24*30,"/home/imail_gamma/");
					$_COOKIE['Gamma']['AutoFill'][$EmailAddr] = stripslashes($BccEmail);
				}else if(isset($_COOKIE['Gamma']['AutoFill'][$EmailAddr])){
					@setcookie("Gamma[AutoFill][".$EmailAddr."]","",time()-60*60*24*30,"/home/imail_gamma/");
					unset($_COOKIE['Gamma']['AutoFill'][$EmailAddr]);
				}
				$ReceiptEmailList[] = $EmailAddr;
			
			}
		}
	}
}
//$MimeMessage = $IMap->buildMimeText($RawSubject,$Body,$From,$ToArray,$CcArray,$BccArray,$AttachList,$ImportantFlag,false,$Preference);
//echo "ToAddress: ";debug_r($_POST['ToAddress']);echo "<br />PreToArray:";debug_r($PreToArray);echo "<br />ToArray:";debug_r($ToArray);
$Preference = $IMap->Get_Preference($From);

$XeClassMailID = "u$UserID-".time(); // global var used in Build_Mime_Text()

$MaxNumOfRecipientPerTime = !isset($SYS_CONFIG['Mail']['MaxNumMailsPerBatch'])?200:$SYS_CONFIG['Mail']['MaxNumMailsPerBatch'];
$SendMailBySplitInBatch = false;

//if(sizeof($ToArray)>$MaxNumOfRecipientPerTime || sizeof($CcArray)>$MaxNumOfRecipientPerTime || sizeof($BccArray)>$MaxNumOfRecipientPerTime)
if(sizeof($ToArray)>$MaxNumOfRecipientPerTime || sizeof($CcArray)>$MaxNumOfRecipientPerTime || sizeof($BccArray)>$MaxNumOfRecipientPerTime || $sys_custom['iMailSendToParent'] == true)
{
	## Append all To, Cc mail addresses to Bcc
	//$ToUndisclosed = array("Undisclosed-Recipient<noreply@".$SYS_CONFIG['Mail']['UserNameSubfix'].">");
	$ToUndisclosed = array();
	$BccArray = array_merge((array)$CcArray,(array)$BccArray);
	$BccArray = array_merge((array)$ToArray,(array)$BccArray);
	$SendMailBySplitInBatch = true;
}

if($SendMailBySplitInBatch)
{
	## Split Bcc into several chunks to send
	$SplitBccArray = array_chunk($BccArray,$MaxNumOfRecipientPerTime);
	for($i=0;$i<sizeof($SplitBccArray);$i++){
		## For each chunk of bcc addresses, rebuild the mail and send, slower but can ensure correctness
		$MimeMessage = $IMap->Build_Mime_Text($Subject, $Body, $From, $ToUndisclosed, "", $SplitBccArray[$i], $AttachList, $ImportantFlag, true, $Preference, $RelatedList);
		$Result['SendMail'] = mail("", $MimeMessage['subject'], $MimeMessage['body'], $MimeMessage['headers2'], "-f $From");
	}
	## Build a mail with all bcc addresses for saving to sent box or draft box
	$MimeMessage = $IMap->Build_Mime_Text($Subject, $Body, $From, $ToUndisclosed, "", $BccArray, $AttachList, $ImportantFlag, true, $Preference, $RelatedList);
	
}else
{
	$MimeMessage = $IMap->Build_Mime_Text($Subject, $Body, $From, $ToArray, $CcArray, $BccArray, $AttachList, $ImportantFlag, true, $Preference, $RelatedList);
//	 debug_r($MimeMessage);
	
	// debug_r($EmptyEmail);
	//if ($Preference[0]['ReceiveBounceBack'] == 1 && count($EmptyEmail) > 0) {
	//	$BounceBackMailFrom = $SYS_CONFIG['SystemGeneratedEmail'];
	//	$BounceBackMailTo = array($From);
	//	$BounceBackMailCc = array();
	//	$BounceBackMailBcc = array();
	//	$BounceBackMailAttachList = array();
	//	$BounceBackMailImportantFlag = 0;
	//	
	//	$BounceBackMailSubject = 
	//		'ESF - Returned Mail ('.$Subject.')';
	//	$BounceBackMailBody = 
	//		"<p>This is a computer generated message. DO NOT REPLY!</p>".
	//		"<p>Error in sending for the follow recepient(s) : ".
	//		"<br>";
	//	foreach ($EmptyEmail as $Key => $ToName) {
	//		$BounceBackMailBody .= $Key +1 ." : ".$ToName."<br/>";
	//	}
	//	$BounceBackMailBody .= "</p>";
	//	$BounceBackMailBody .= 
	//		"<p>Please check whether you have inserted the correct email address.</p>".
	//		"<p>Thank you</p>";
	//	
	//	$BounceBackMailMimeMessage = $IMap->Build_Mime_Text($BounceBackMailSubject, $BounceBackMailBody, $BounceBackMailFrom, $BounceBackMailTo, $BounceBackMailCc, $BounceBackMailBcc, $BounceBackMailAttachList, $BounceBackMailImportantFlag, true, $BounceBackMailPreference);
	//	
	//	// debug_r($BounceBackMailMimeMessage);
	//}
	
	// exit;
	/*echo '<textarea>';
	echo $RawBody;
	echo '</textarea>';*/
	
	/* echo '<pre>';
	var_dump($MimeMessage);
	echo '</pre>';
	die; */
	// send by local sendmail, imap_mail
	#$Result = imap_mail($MimeMessage['to'],$MimeMessage['subject'],$MimeMessage['body'],$MimeMessage['headers'],$MimeMessage['cc'],$MimeMessage['bcc'],$ReturnPath);//,$IMap->CurUserAddress);
	#$Result = imap_mail($MimeMessage['to'],$MimeMessage['subject'],$MimeMessage['body'],$MimeMessage['headers'],$MimeMessage['cc'],$MimeMessage['bcc'],$From);
	// Kenneth Wong (20080612) Changed to mail() instead of imap_mail() due to return path problem
	$Result['SendMail'] = mail($MimeMessage['to'], $MimeMessage['subject'], $MimeMessage['body'], $MimeMessage['headers2'], "-f $From");
	//$Result['SendMail'] = true;
	// send by phpmailer
	//$Result = $IMap->Send_Mail_By_PHPMailer($ToArray,$CcArray,$BccArray,$Subject,$Body,$AttachList);
	
	### Sending Bounce Back Mail ###
	//if ($Preference[0]['ReceiveBounceBack'] && count($EmptyEmail) > 0) {
	//	$Result['BounceBack'] = mail($BounceBackMailMimeMessage['to'], $BounceBackMailMimeMessage['subject'], $BounceBackMailMimeMessage['body'], $BounceBackMailMimeMessage['headers2'], "-f $BounceBackMailFrom");
	//}
}

if (!$Result['SendMail']) {
	//[N181620]
	//$Result['SaveDraft'] = $IMap->saveDraftCopy($MimeMessage['FullMimeMsg']);
	//save a copy in Sent folder
	$Result['SaveSendCopy'] = $IMap->saveSentCopy($MimeMessage['FullMimeMsg']);
}
else {
	// save a copy in Sent folder
	$Result['SaveSendCopy'] = $IMap->saveSentCopy($MimeMessage['FullMimeMsg']);
	
	if (($MailAction == "Reply" || $MailAction == "ReplyAll") && $Folder != "") {	
		$IMap->Set_Answered($Folder,$ActionUID);
	}
	else if ($MailAction == "Forward" && $Folder != "") {	
		$IMap->Set_Forwarded($Folder,$ActionUID);
	}
	
	// Add receipt records to track sent mail's read status
	if($_POST['IsNotification']=='1' && $Result['SaveSendCopy']){
		preg_match("/.*X-eClass-MailID:(.*)"."(".chr(10)."|".$IMap->MimeLineBreak.").*/",$MimeMessage['headers2'],$reg_found);
		$eClassMailID = trim($reg_found[1]);
		if($eClassMailID != '') $IMap->InsertMailReceiptByMailAddresses($eClassMailID,array_unique($ReceiptEmailList));
	}
}

# delete the draft mail if is draft
if ($DraftUID != "") {
	if(!empty($_SESSION["UIDChange"])){
		$key = array_search($DraftUID, $_SESSION["UIDChange"]);
		if($key!== false)
			unset($_SESSION["UIDChange"][$key]);
	}
	//20100812	
	$Result['DeleteMail'] = $IMap->deleteMail($IMap->DraftFolder, $DraftUID);
}


## delete corresponding files 
# Remove the files which are NEWLY uploaded in New Compose Mail, Compose Forward, Reply, ReplyALL and Draft Mail
$IMap->Remove_Unlink_Attach_File("","",$ComposeDatetime);

# Remove the files which belong to Draft Mail and are NOT newly added
if ($MailAction == "" && $DraftUID != "") {
	$IMap->Remove_Unlink_Attach_File($DraftUID,"",$ComposeDatetime);
}

# Remove the files which are followed by an Forward Mail 
if($MailAction == "Forward"){
	$IMap->Remove_Unlink_Attach_File($MessageUID, $MailAction, $ComposeDatetime);
}
/*
#  delete Temp file created in composing mail, select file list to delete
$sql = 'SELECT 
			FileID, 
			EncodeFileName
		FROM 
			MAIL_ATTACH_FILE_MAP 
		where 
		(
			DraftMailUID IS NULL OR DraftMailUID = \''.$DraftUID.'\' OR 
			(DraftMailUID = -1 AND OrgDraftMailUID = \''.$DraftUID.'\')
		) 
		and 
		UserID = '.$_SESSION['SSV_USERID'];
$DeleteList = $ldb->returnArray($sql,2);

# delete the file in file system
for($i=0;$i< sizeof($DeleteList);$i++){
	list($FileID,$FileName) = $DeleteList[$i];
	$loc = $personal_path."/".$FileName;
	
	$lfs->file_remove($loc);
}

# delete file record in database
$sql = 'DELETE FROM MAIL_ATTACH_FILE_MAP 
		WHERE 
			UserID = '.$_SESSION['SSV_USERID'] AND 
			(
				DraftMailUID IS NULL OR DraftMailUID = \''.$DraftUID.'\' OR 
				(DraftMailUID = -1 AND	OrgDraftMailUID = \''.$DraftUID.'\')
			) 
		;
$ldb->db_db_query($sql);
*/
	
	//20100812
if (!in_array(false,$Result)) {
	//$Msg = $Lang['ReturnMsg']['MailSendSuccess'];
		//$Folder = $IMap->SentFolder;
//	if($Folder != $IMap->SentFolder)
//		$Folder = $IMap->InboxFolder;
	$exmsg = "1";
}
else {
	//$Msg = $Lang['ReturnMsg']['MailSendUnsuccess'];
	if((isset($Result['SaveDraft']) && !$Result['SaveDraft']) || (isset($Result['SaveSendCopy']) && !$Result['SaveSendCopy'])) {
		$msg = "9";
	}else{
		$msg = "-1";
	}
	//[N181620]
	//$Folder = $IMap->DraftFolder;
	// debug_r($Result);
	// var_dump($Result['BounceBack']);
	// exit;
}

if(isset($_POST['FromSharedMailBox']) && trim($_POST['FromSharedMailBox'])!='' && $_SESSION['SSV_LOGIN_EMAIL'] != $_POST['FromSharedMailBox']){
	$_SESSION['SSV_EMAIL_LOGIN']    = $_POST['FromSharedMailBox'];
	$_SESSION['SSV_EMAIL_PASSWORD'] = $IMap->Get_MailBox_Password($_POST['FromSharedMailBox']);
	$_SESSION['SSV_LOGIN_EMAIL']    = $_POST['FromSharedMailBox'];
	unset($_SESSION['SSV_GAMMA_INIT_FOLDER']);
	unset($_SESSION['SSV_GAMMA_BATCH_REMOVAL_STATE']);
	unset($_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_send_right"]);
	unset($_SESSION['SSV_GAMMA_USE_CACHE']);
	
	// Login to another mail account and save sent copy
	$IMap = new imap_gamma();
	if (!$Result['SendMail']) {
		//[N181620]
		//$Result['SaveDraft2'] = $IMap->saveDraftCopy($MimeMessage['FullMimeMsg']);
		//if(!$Result['SaveDraft2']) {
		//	$msg = "9";
		//}
		//save a copy in Sent folder
		$Result['SaveSendCopy2'] = $IMap->saveSentCopy($MimeMessage['FullMimeMsg']);
		if(!$Result['SaveSendCopy2']){
			$msg = "9";
		}
	}
	else {
		// save a copy in Sent folder
		$Result['SaveSendCopy2'] = $IMap->saveSentCopy($MimeMessage['FullMimeMsg']);
		if(!$Result['SaveSendCopy2']){
			$msg = "9";
		}
	}
}

##############################################################################
//Write_Message_Log('Gamma', "Page of Compose Mail Total: ".(Get_Timer() - $lui->logTime['Compose_Mail_Page']));
intranet_closedb();

//if($FromSearch == 1){
//	$redirect_url = ($IsAdvanceSearch == 0) ? "search_result.php" : "advance_search_result.php";
//	
//	# Page from Advance Search
//	echo '<html><body>';
//	echo $lui->Get_Form_Open("form1", "post", $redirect_url);
//	echo $lui->Get_Input_Hidden("CurTag", "CurTag", urlencode($Folder))."\n";
//	echo $lui->Get_Input_Hidden("Folder", "Folder", urlencode($Folder))."\n";
//	echo $lui->Get_Input_Hidden("reverse", "reverse", $reverse)."\n";
//	echo $lui->Get_Input_Hidden("sort", "sort", $sort)."\n";
//	echo $lui->Get_Input_Hidden("pageNo", "pageNo", $pageNo)."\n";
//	echo $lui->Get_Input_Hidden("FromSearch", "FromSearch", $FromSearch)."\n";
//	echo $lui->Get_Input_Hidden("IsAdvanceSearch", "IsAdvanceSearch", $IsAdvanceSearch)."\n";
//
//	# Hidden Values - Simple Search
//	echo $lui->Get_Input_Hidden("keyword", "keyword", $keyword)."\n";
//
//	# Hidden Values - Advance Search Field
//	echo $lui->Get_Input_Hidden("KeywordFrom", "KeywordFrom", $KeywordFrom)."\n";
//	echo $lui->Get_Input_Hidden("KeywordTo", "KeywordTo", $KeywordTo)."\n";
//	echo $lui->Get_Input_Hidden("KeywordCc", "KeywordCc", $KeywordCc)."\n";
//	echo $lui->Get_Input_Hidden("KeywordSubject", "KeywordSubject", $KeywordSubject)."\n";
//	echo $lui->Get_Input_Hidden("FromDate", "FromDate", $FromDate)."\n";
//	echo $lui->Get_Input_Hidden("ToDate", "ToDate", $ToDate)."\n";
//	if(count($SearchFolder) > 0){
//		for ($i=0; $i < sizeof($SearchFolder); $i++) {
//			echo $lui->Get_Input_Hidden("SearchFolder[]", "SearchFolder[]", stripslashes($SearchFolder[$i]))."\n";
//		}
//	}
//	echo $lui->Get_Form_Close();
//	echo '<script type="text/javascript">';
//	echo "function submit_form(){ document.form1.submit(); }";
//	echo "submit_form();";
//	echo "</script>";
//	echo '</body></html>';
//} else {
	# Page not from Search
	$params  = "?msg=".$msg."&exmsg=".$exmsg."&Folder=".urlencode($Folder)."&CurTag=".urlencode($Folder);
	$params .= "&pageNo=$pageNo&sort=$sort&reverse=$reverse";
	
	header("Location: viewfolder.php".$params);
	exit;
//}
?>
