<?php
// editing by 
/*
 * 2013-10-04 (Carlos): base64_encode() folder name to pass into js alertRemove()
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
//include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
//include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
//include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");


intranet_auth();
intranet_opendb();
auth_campusmail();

$CurrentPageArr['iMail'] = 1;

$IMap = new imap_gamma();
$libdb = new libdb();

# get user create folder
$Folders = $IMap->Get_Folder_Structure(true,false);

# build table
$table = "<table width='100%' border='0' cellpadding='5' cellspacing='0' align='center' >\n";
$table .= "<tr class='tablegreentop tabletopnolink' >";
$table .= "<td>$i_CampusMail_New_FolderName</td>";
$table .= "<td>$i_CampusMail_New_NumOfMail</td>";
$table .= "<td >$button_remove</td>";
$table .= "</tr>\n";

foreach((array)$Folders as $thisFolder)
{
	list($FolderName, $FolderPath) = $thisFolder;
	
	// Skip reportSpam, reportHam folder (default added by solution team)
	if(in_array($FolderName,array("reportSpam","reportNonSpam")))
		continue;

	$rowcss = "tablegreenrow".(($rowctr++)%2+1); 
	
	$NumOfMail = $IMap->Check_Mail_Count($FolderPath);
	
	$remove_link = "<a href=\"javascript:alertRemove('".base64_encode($FolderPath)."',$NumOfMail)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" border=0></a>";
	$mailbox_link = "viewfolder.php?Folder=".urlencode($FolderPath);
	$edit_link = "folder_edit.php?Folder=".urlencode($FolderPath);
	
	$table .= "<tr class='$rowcss'>";
		$table .= "<td><a class='tablelink' href='$edit_link'>".IMap_Decode_Folder_Name($FolderName)."</a></td>";
		$table .= "<td><a class='tablelink' href='$mailbox_link'>$NumOfMail</a></td>";
		$table .= "<td>$remove_link</td>";
	$table .= "</tr>"; 
}

$table .= "</table>";

$CurrentPage = "PageCheckMail_FolderManager";

$MODULE_OBJ = $IMap->GET_MODULE_OBJ_ARR();

# tag information
$iMailImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_folder_open.gif' width='20' height='20' align='absmiddle' >";
$iMailTitle1 = "<span class='imailpagetitle'>{$i_CampusMail_New_FolderManager}</span>";
$iMailTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0'><tr><td style=\"vertical-align: bottom;\" >".$iMailImage1.$iMailTitle1.$unreadLink."</td><td align='right' style=\"vertical-align: bottom;\" >".$IMap->TitleToolBar()."</td></tr></table>";
$TAGS_OBJ[] = array($iMailTitle, "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

$toolbar = $linterface->GET_LNK_NEW("folder_new.php","","","","",0);

if ($msg == "2")
{
	$xmsg = $linterface->GET_SYS_MSG("update");
} else {
	$xmsg = "&nbsp;";
}
##################

?>
<script language="javascript">
function alertRemove(Folder, numofmail)
{
	     if (confirm('<?=$Lang['Gamma']['ConfirmMsg']['ConfirmRemoveFolder']?>'))
         {
         	 var url = 'folder_remove.php?Folder='+Folder;
             location = url;
             return true;
         }
         //else return false;
	        
}
</script>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align='right' ><?=$xmsg?></td>
	</tr>
	<tr>
		<td >
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0"  >
		<tr>
			<td><?=$toolbar?></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td><?=$table?></td>
		</tr>      
		</table>
				
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<?php
	//echo $lc->msg;
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>