<?php
$PATH_WRT_ROOT = '../../../';

$parLang = $_GET['parLang'];
switch (strtolower($parLang)) {
case 'en':
$intranet_hardcode_lang = 'en';
break;
default:
$intranet_hardcode_lang = 'b5';
}

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libpwm.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
$uid = $_GET['uid'];
if($_SESSION['UserID'] ==''){
$_SESSION['UserID'] = $uid;	
}
//intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$libDb = new libdb(); 
$libeClassApp = new libeClassApp(); 
$json = new JSON_obj();
$libpwm= new libpwm();

$token = $_GET['token'];
$ul = $_GET['ul'];
$AttachmentWorksOnCurrentVersion = $_GET['AttachmentWorksOnCurrentVersion'];
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
if (!$isTokenValid) {
	die("No access right to see this page.");
}

$curUserId = $uid;
$UserID = $uid;
$thisFolder= $_GET["TargetFolderName"];
$currentUID =$_GET["CurrentUID"];
$ContentMailAction = $_GET["MailAction"];

$folderAssoAry ="";
$defaultFolder = array();
$personalFolder = array();
//$IsImailGamma = 1;
if ($plugin['imail_gamma']) {
	
	$passwordAry = $libpwm->getData($uid);
	foreach((array)$passwordAry as $_folderType => $_folderKey){
		if($_folderType==$uid)$password=$_folderKey;
	}
    $sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$curUserId."'";
	$dataAry = $libDb->returnResultSet($sql);
	$iMapEmail = $dataAry[0]['ImapUserEmail'];
	$iMapPwd = $password;	// hardcode for trial only

    $IMap = new imap_gamma(false,$iMapEmail, $iMapPwd);
	$IMap->IMapCache = new imap_cache_agent($iMapEmail, $iMapPwd);

	if($thisFolder!="INBOX"){
    	$Folder = "INBOX.".$thisFolder;
    }
    else{
    	$Folder = $thisFolder;
    }
    
  $thisFolder = urlencode($thisFolder);//encode thisFolder for url use handle chinese folder case
	
$Folder = (isset($Folder) && $Folder != "") ? stripslashes($Folder) : $IMap->InboxFolder;
$CacheNewMail = false;
$currentMailRecord = $IMap->getMailRecord($Folder, $currentUID, "");

$currentMailRecordContent = $currentMailRecord[0];

	$__fromname = $currentMailRecordContent['fromname'];            	  	  
	$__subject = $currentMailRecordContent['subject'];
	$__dateReceive = $currentMailRecordContent['dateReceive'];
	$__message = $currentMailRecordContent['message'];
	$_currentAttachment = $currentMailRecordContent['attach_parts'];
	
	$_currentMailToArrays = $currentMailRecordContent['toDetail'];
	$_currentMailCcArrays = $currentMailRecordContent['ccDetail'];
	$_currentMailBccArrays = $currentMailRecordContent['bccDetail'];
            	  	  
	$__currentMailTos = '';
	for($i =0;$i<count($_currentMailToArrays);$i++){
		$stdClassObject = $_currentMailToArrays[$i];
		foreach((array)$stdClassObject as $_Key => $_Detail){
			if($_Key=='mailbox'){
				$__mailBox = $_Detail;
			}
			else if($_Key=='host'){
				$__host = $_Detail;
			}
		}
		$__currentMailTos .= $__mailBox.'@'.$__host;
	}
	
	$__currentMailCcs = '';
	for($i =0;$i<count($_currentMailCcArrays);$i++){
		$stdClassObject = $_currentMailCcArrays[$i];
		foreach((array)$stdClassObject as $_Key => $_Detail){
			if($_Key=='mailbox'){
				$__mailBox = $_Detail;
			}
			else if($_Key=='host'){
				$__host = $_Detail;
			}
		}
		$__currentMailCcs .= $__mailBox.'@'.$__host;
	}
	
	$__currentMailBccs = '';
	for($i =0;$i<count($_currentMailBccArrays);$i++){
		$stdClassObject = $_currentMailBccArrays[$i];
		foreach((array)$stdClassObject as $_Key => $_Detail){
			 if($_Key=='mailbox'){
				$__mailBox = $_Detail;
			}
			else if($_Key=='host'){
				$__host = $_Detail;
			}
		}
		$__currentMailBccs .= $__mailBox.'@'.$__host;
	}
	$MailHeader = $IMap->Get_Preference($iMapEmail);
	$_signature = $MailHeader['Signature'];
	
}//end gamma
else{
	
	 $lc = new libcampusquota2007($UserID);
 	if($thisFolder =='INBOX'){
	$folderID[0] = 2;
	}else if($thisFolder =='Drafts'){
		$folderID[0] = 1;
	}else if($thisFolder =='Sent'){
		$folderID[0] = 0;
	}else if($thisFolder == 'Trash'){
        $folderID[0] = -1;
	}else{
    $sql = "select FolderID from INTRANET_CAMPUSMAIL_FOLDER where OwnerID='$uid' and foldername = '".$libDb->Get_Safe_Sql_Query($thisFolder)."'";
	$folderID = $libDb->returnVector($sql);
	}//end initilize folderID   
	if($ContentMailAction !='Compose'){
		
		
	 if( $folderID[0] == -1){
	 $sql  = "select Message,SenderID,SenderEmail,Subject,DateInput,ExternalTo from INTRANET_CAMPUSMAIL where UserID ='$uid' and Deleted= '1' AND CampusMailID='$currentUID'";		
	}else{
	 $sql  = "select Message,SenderID,SenderEmail,Subject,DateInput,ExternalTo from INTRANET_CAMPUSMAIL where UserID ='$uid' and UserFolderID= '$folderID[0]' AND CampusMailID='$currentUID'";
	}	   
	    $MailHeader = $lc->returnArray($sql);

	    $content = $MailHeader[0];
	    $__subject = $content['Subject']; 
        $__dateReceive = $content['DateInput']; 
        $senderID = $content['SenderID'];
        $__fromname = $content['SenderEmail']; 
        $__message = $content['Message'];
        $__currentMailTos = $content['ExternalTo'];                       
        
        if(($senderID!=null)&&($senderID!='')){
        	if($parLang=='en'){
        		$nameLan = "EnglishName";
        	}else{
        		$nameLan = "ChineseName";
        	}
        $sql  = "select ".$nameLan.",UserEmail from INTRANET_USER where UserID ='$senderID'";  
        $SenderName = $lc->returnArray($sql);	
        
        $senderNameLan = $SenderName[0][$nameLan];
        $senderEmail = $SenderName[0]['UserEmail'];
        $__fromname = $senderNameLan ."<".$senderEmail.">";
        }
   
//	 	if ($subject != "") {
//			$subjectPrint = (substr($subject, 0, 10) == "=?UTF-8?Q?") ? substr($subject, 10, -2) : $subject ;
//		} else {
//			$subjectPrint = "No subject";
//		}
		$DateFormat = "Y-m-d";
		$prettydate='';
		if($__dateReceive!=''){
	    $__dateReceive = date($DateFormat, strtotime(str_ireplace("UT","UTC",$__dateReceive)));
		}	
	}//end not compose which has no currentUID
	$sql = "SELECT Signature FROM INTRANET_IMAIL_PREFERENCE WHERE UserID = '$uid'";
	$MailHeader = $lc->returnArray($sql);	
	$_signature = $MailHeader[0]['Signature'];	
}//end imail				


$currentMailShowedContent = array();	
$currentMailShowedContent['Sender']=$__fromname;

$currentMailShowedContent['Date']=$__dateReceive;
$currentMailShowedContent['Content']=$__message;
//GET FROM link parameter
$currentMailShowedContent['EmailReplyType'] = $ContentMailAction;
$currentMailShowedContent['Signature'] = $_signature;
if($ContentMailAction =='Compose'){
	$currentMailShowedContent['Receiver'] = '';
	$currentMailShowedContent['Subject']='';
	$currentMailShowedContent['CcReceiver'] ='';
	$currentMailShowedContent['BccReceiver'] ='';
//	$currentMailShowedContent['Content']= '';
    $currentMailShowedContent['ContentTitleMessage']='';
    $currentMailShowedContent['ContentSenderMessage'] ='';
    $currentMailShowedContent['ContentDateMessage'] = '';
    $currentMailShowedContent['Content'] = '';
}
else if($ContentMailAction =='Reply'){
	$currentMailShowedContent['Receiver'] = $__fromname;
	$currentMailShowedContent['CcReceiver'] ='';
	$currentMailShowedContent['BccReceiver'] ='';
	$currentMailShowedContent['Subject']="Re:".$__subject;
//	$currentMailShowedContent['Content']= "".$currentMailShowedContent['Date'].",".$currentMailShowedContent['Sender']."<br>wrote:".$__message;
    $currentMailShowedContent['ContentTitleMessage']='';
    $currentMailShowedContent['ContentSenderMessage'] ='';
    $currentMailShowedContent['ContentDateMessage'] = $currentMailShowedContent['Date'].",".$currentMailShowedContent['Sender'];
    $currentMailShowedContent['Content'] = "wrote:".$__message;
}else if($ContentMailAction =='ReplyAll'){
	
	$currentMailShowedContent['Receiver'] = $__fromname;
	$currentMailShowedContent['CcReceiver'] =$__currentMailCcs;
	$currentMailShowedContent['BccReceiver'] ='';
	$currentMailShowedContent['Subject']="Re:".$__subject;
//	$currentMailShowedContent['Content']="". $currentMailShowedContent['Date'].",".$currentMailShowedContent['Sender']."<br>wrote:".$__message;
    $currentMailShowedContent['ContentTitleMessage']='';
    $currentMailShowedContent['ContentSenderMessage'] ='';
    $currentMailShowedContent['ContentDateMessage'] = $currentMailShowedContent['Date'].",".$currentMailShowedContent['Sender'];
    $currentMailShowedContent['Content'] = "wrote:".$__message;
	
}else if($ContentMailAction =='Forward'){
	$currentMailShowedContent['Receiver'] = '';
	$currentMailShowedContent['CcReceiver'] ='';
	$currentMailShowedContent['BccReceiver'] ='';
	$currentMailShowedContent['Subject']="Fwd:".$__subject;
	//$currentMailShowedContent['Content']= "Forward message------------<br>From:".$currentMailShowedContent['Sender']."<br>Date:".$currentMailShowedContent['Date']."<br>".$__message;
    $currentMailShowedContent['ContentTitleMessage']="Forward message------------------------";
    $currentMailShowedContent['ContentSenderMessage'] = "From:".$currentMailShowedContent['Sender'];
    $currentMailShowedContent['ContentDateMessage'] = "Date:".$currentMailShowedContent['Date'];
    $currentMailShowedContent['Content'] = $__message;

}else if($ContentMailAction =='Drafts'){
	$currentMailShowedContent['Receiver'] = $__currentMailTos;
	$currentMailShowedContent['CcReceiver'] = $__currentMailCcs;
	$currentMailShowedContent['BccReceiver'] = $__currentMailBccs;
	$currentMailShowedContent['Subject'] = "".$__subject;
	$currentMailShowedContent['ContentTitleMessage'] = "";
    $currentMailShowedContent['ContentSenderMessage'] = "";
    $currentMailShowedContent['ContentDateMessage'] = "";
    $currentMailShowedContent['Content'] = $__message;
}
     $currentMailShowedContent['Content'] = htmlspecialchars_decode($currentMailShowedContent['Content']);
     
    $emil_reply_forward_operation_form_action="email_operation.php?MailAction=".$ContentMailAction."&token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang."&TargetFolderName=".$thisFolder."&CurrentUID=".$currentUID."&AttachmentWorksOnCurrentVersion=".$AttachmentWorksOnCurrentVersion;
	$emil_reply_forward_send_form_action = $emil_reply_forward_operation_form_action."&IMailOperation=Send";
	$emil_reply_forward_save_form_action =  $emil_reply_forward_operation_form_action."&IMailOperation=Save";
	
	//sending email loading icon
	$refreshIconUrl =$linterface->Get_Ajax_Loading_Image($noLang=true);

    //icon paths
	$mySaveAsDraftIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/teacher_icon_save_as_draft_36px.png"."\"";
	$mySendIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/teacher_icon_send_36px.png"."\"";
	$myCloseIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_close.png"."\"";
	$myAttachmentIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_attachment_36px.png"."\"";
	
   //language setting
   $_SELECT_ALL = $Lang['Gamma']['SelectAll'];
   $_SELECT_FROM_EXTERNALGROUP = $Lang['Gamma']['SelectFromExternalRecipientGroup'];
   $_SELECT_FROM_EXTERNALCONTACT = $Lang['Gamma']['SelectFromExternalRecipient'];
   $_SELECT_FROM_INTERNALGROUP = $Lang['Gamma']['SelectFromInternalRecipientGroup'];
   $_SELECT_FROM_INTERNALCONTACT = $Lang['Gamma']['SelectFromInternalRecipient'];
   
   $AddressTypeArr = array($_SELECT_FROM_EXTERNALGROUP,$_SELECT_FROM_EXTERNALCONTACT,$_SELECT_FROM_INTERNALGROUP,$_SELECT_FROM_INTERNALCONTACT);

   $ToTitleText = $Lang['Gamma']['To'];
   $CcTitleText = $Lang['Gamma']['cc'];
   $BccTitleText = $Lang['Gamma']['bcc'];
   $SubjectTitleText = $Lang['Gamma']['Subject'];
   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<title>email list</title>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
		<script>
		window.JSON || 
		document.write('<script src="//cdnjs.cloudflare.com/ajax/libs/json3/3.2.4/json3.min.js"><\/scr'+'ipt>');
		</script>
		
		<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
		<script language="javascript">
		</script>
	</head>
	<body>
	<div data-role="page">	
	<header data-role ="header" data-position="fixed" style ="background-color:#f6f6f6;border-bottom-width: 0px; z-index:999;">
	 <div id='loadingmsg' style='display: none;'><br><?=$refreshIconUrl?><br></div>
     <div id='loadingover' style='display: none;'></div>
	 <a  href="#" data-rel="back" data-icon="myapp-delete" data-role="button" class = "ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon ui-btn-left" style="top: 5px;padding-bottom: 16px;padding-top: 16px;padding-left: 12px;padding-right: 20px;"></a>
     <p  style="text-align:left; margin-left:30px;" ><?php echo"&nbsp&nbsp  ".$Lang['Gamma']['App']['ComposeMail']?></p>
   
     <div  class="ui-btn-right" data-role="controlgroup" data-type="horizontal">
     <a href="#"    id="saveEmailToTrash"  name="Save"  data-icon="myapp-saveAsDraft"     data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="top: 5px;padding-bottom: 16px;padding-top: 16px;padding-left: 15px;padding-right: 20px;" ></a>
     <a href="#"    id="sendBtn"  name="Send"  data-icon="myapp-send"  data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="top: 5px;padding-bottom: 16px;padding-top: 16px;padding-left: 15px;padding-right: 20px;"  ></a>
    </div>
    </header>
    <!-- /header -->            
    <!-- Home -->
    <div role="main" class="ui-content" sytle = " padding-left: 0px; padding-right: 0px;" style=" padding-left: 0px; padding-right: 0px; padding-top: 0px;" > 
    <form class="ui-filterable"  id = "email_compose_process" name = "email_compose_process" style = "background-color:#FFFFFF"  method="post" action="<?=$emil_reply_forward_operation_form_action?>"   enctype="multipart/form-data">
    <div id="demo-borders" style ="width:100%;background-color:#FFFFFF">
    <!-- collapsible To -->
    <fieldset data-role="collapsible" data-inset="false" data-iconpos="right" style = "background-color:#FFFFFF;width:100%;" >
    <legend><?=$ToTitleText?>: <input type="email" name="myReceiverFilter" value="<?=$currentMailShowedContent['Receiver']?>"  style="width: 85%; border-top-width: 0px;border-right-width: 0px;border-bottom-width: 0px;border-left-width: 0px;height: 35px;" id="myReceiverFilter" ></legend>
    <div id="mySearchPickPlace">
    <ul id="myReceiverFilterAddressList" data-role="listview" data-filter="false" data-input="#myReceiverFilter"  data-inset="false">
    <li data-name="0"><a href="#" ><?=$AddressTypeArr[0]?></a></li>
    <li data-name="1"><a href="#" ><?=$AddressTypeArr[1]?></a></li>
    <li data-name="2"><a href="#" ><?=$AddressTypeArr[2]?></a></li>
    <li data-name="3"><a href="#" ><?=$AddressTypeArr[3]?></a></li>
    </ul></div>
    </fieldset><!-- /collapsible To -->
    </div> 
    
    
    <!-- CC and Bcc button -->
    <?php
    if(	($currentMailShowedContent['CcReceiver'] =='')&&($currentMailShowedContent['BccReceiver'] =='')){
    	$isFoldCcBcc ="show";
    	$isExpandCcBcc = "hidden";
    }
    else{
    	$isFoldCcBcc ="hidden";
    	$isExpandCcBcc = "show";
    }
    ?>
   <div class = <?=$isFoldCcBcc?> id="foldCc">
   <table style="width: 100%;padding-left: 15px;background-color:#f6f6f6;">
   <tr>
   <td style ="width:50px;"><?=$Lang['Gamma']['App']['FolderCCNBCC']?>:</td>
   <td><a href="#" id = "showExpandedCc"><input type="email" name="" id="CcNBccTextClick" value=""></a></td>
   <td ></td>
   </tr></table></div>
    <!--/Subject -->
         
     <!-- /CC and Bcc button -->

    <!--collapsible Cc and Bcc expanded -->
    <div id="myExpandedCcNBcc" class = <?=$isExpandCcBcc?>>
    
    <fieldset data-role="collapsible" data-inset="false" data-iconpos="right" id="myExpandedCc">
    <legend><?=$CcTitleText?>: <input type="email" name="myCcFilter" value="<?=$currentMailShowedContent['CcReceiver']?>"  style="width: 85%;  border-top-width: 0px;border-right-width: 0px;border-bottom-width: 0px;border-left-width: 0px;height: 35px;" id="myCcFilter" ></legend>
    <div id="myCcSearchPickPlace">
    <ul  id="myCcFilterAddressList" data-role="listview" data-filter="true" data-input="#myCcFilter"  data-inset="false">
   <!-- <li style= "padding-top:20px;padding-bottom:25px;" >-->
    <li data-name="0"><a href="#" ><?=$AddressTypeArr[0]?></a></li>
    <li data-name="1"><a href="#" ><?=$AddressTypeArr[1]?></a></li>
    <li data-name="2"><a href="#" ><?=$AddressTypeArr[2]?></a></li>
    <li data-name="3"><a href="#" ><?=$AddressTypeArr[3]?></a></li>
    </ul></div>
    </fieldset><!-- /collapsible Cc -->
    
    <fieldset data-role="collapsible" data-inset="false" data-iconpos="right" id ="myExpandedBcc">
    <legend><?=$BccTitleText?>: <input type="email" name="myBccFilter" value=""  style="width: 80%; border-top-width: 0px;border-right-width: 0px;border-bottom-width: 0px;border-left-width: 0px;height: 35px;" id="myBCcFilter" ></legend>
    <div id="myBccSearchPickPlace">
    <ul id="myBccFilterAddressList" data-role="listview" data-filter="true" data-input="#myBccFilter"  data-inset="false">
    <li data-name="0"><a href="#" ><?=$AddressTypeArr[0]?></a></li>
    <li data-name="1"><a href="#" ><?=$AddressTypeArr[1]?></a></li>
    <li data-name="2"><a href="#" ><?=$AddressTypeArr[2]?></a></li>
    <li data-name="3"><a href="#" ><?=$AddressTypeArr[3]?></a></li>
    </ul></div>
    </fieldset>
    </div>
    <!--/collapsible Cc and Bcc expanded -->
   
    <!--Subject -->
   <table style="width: 100%;padding-left: 15px; background-color:#f6f6f6;">
   <tr>
   <td style ="width:50px;"><?=$SubjectTitleText?>:</td>
   <td><input type="text" name="Subject" id="Subject" value="<?=$currentMailShowedContent['Subject']?>"></td>
   <td style ="width:30px;">
   <label for="attachmentFile" id="upload-file-container" style =" height: 30px;width: 30px; margin-right: 10px; margin-top: 10px;margin-buttom: 10px;"></label>
   <input type="file"  id ="attachmentFile" name="attachmentFile" multiple style="display: inline; position: fixed; top: 0px; right: 0px; z-index:-1;"></td>
   <!--<td style ="width:36px;"><input type="image" id ="markEmail"  src="/images/2009a/iMail/app_view/icon_star_36px.png"  style="padding-bottom: 5px;padding-right: 0px;"></td>-->
   </tr></table>
    <!--/Subject -->
    
    <!--attachment-->
    <div id="selectedFiles"  style = "margin-top:10px;"></div>
    <!--attachment-->
    
    <!--Content-->
    <div class="editable" name="editableEmailContentBody" id="editableEmailContentBody" >
    <br><br><br>
    <?=$currentMailShowedContent['ContentTitleMessage']?><br>
    <?=$currentMailShowedContent['ContentSenderMessage']?><br>
    <?=$currentMailShowedContent['ContentDateMessage']?><br>
    <?=$currentMailShowedContent['Content']?><br>
    <?=$currentMailShowedContent['Signature']?>
    </div>
    <textarea id="emailMainBody" name = "emailMainBody" style="display:none"></textarea>
	<!--end Content-->
	  
      <input type="hidden" id ="chosenRecipientIDs" name="chosenRecipientIDs" >
      <input type="hidden" id ="chosenInternalCCIDs" name="chosenInternalCCIDs" >
      <input type="hidden" id ="chosenInternalBCCIDs" name="chosenInternalBCCIDs" >
      <input type="hidden" id ="chosenExternalRecipientAddresses" name="chosenExternalRecipientAddresses" >
      <input type="hidden" id ="chosenExternalCcAddresses" name="chosenExternalCcAddresses" >
      <input type="hidden" id ="chosenExternalBCCAddresses" name="chosenExternalBCCAddresses" >
      
      <input type="hidden" id ="chosenFromEmailAddressListGenerator" name="chosenFromEmailAddressListGenerator" >
      <input type="hidden" id ="chosenCCFromEmailAddressListGenerator" name="chosenCCFromEmailAddressListGenerator" >
      <input type="hidden" id ="chosenBCCFromEmailAddressListGenerator" name="chosenBCCFromEmailAddressListGenerator" >
      
      <input type="hidden" id ="chosenUploadedAttachmentLink" name="chosenUploadedAttachmentLink" >
   </form>
   </div><!-- /Home -->
	<div data-role="footer">
	</div><!-- /footer -->
	<style type="text/css">
	input
    {
    box-shadow:inset 0 0 0px 0px #f6f6f6;
    }
	div.hidden{
		display:none ;
	}
	
	div.show{
		display:block ;
	}
	a{background-color:#FFFFFF !improtant;}
	
	.custom {
	padding: 0;
	}
	.custom > div {
	    margin: 0;
	}
	.custom > div > label {
	    border-radius: 0;
	    border: 0;
	}
	div.editable {
	
	    border: 1px solid #ccc;
	    padding: 5px;
	}

	 .ui-icon-myapp-saveAsDraft{
     background-color: transparent !important;
   	 border: 0 !important;
   	 -webkit-box-shadow:none !important;  
	 background: url(<?=$mySaveAsDraftIconUrl?>) no-repeat !important;
    }
     .ui-icon-myapp-send{
      background-color: transparent !important;          
   	 border: 0 !important;
   	 -webkit-box-shadow:none !important;
	 background: url(<?= $mySendIconUrl ?>) no-repeat !important;
    }
    
    	 .ui-icon-myapp-delete{
     background-color: transparent !important;
   	 border: 0 !important;
   	 -webkit-box-shadow:none !important;
	 background: url(<?=$myCloseIconUrl?>) no-repeat !important;
    }
    
    
    .myReceiverFilterAddressList {
    overflow: auto;
    height:200px;
}

	#selectedFiles img {
		max-width: 20px;
		max-height: 20px;
		float: left;
		margin-bottom:10px;
	}
	#selectedFiles a {
		max-width: 20px;
		max-height: 20px;
		float: right;
		margin-bottom:10px;
	}
	
		.ui-li-text-my-chosenFileNameP{
    	color:black;
    	font-size: .75em !important;
    }
    
    #upload-file-container {
     background-color: transparent !important;
   	 border: 0 !important;
   	 -webkit-box-shadow:none !important;
	 background: url(<?=$myAttachmentIconUrl?>) no-repeat !important;
}

	#upload-file-container input {
	position: absolute; !important;
	right: 0;!important;
	 font-size: <many a>px; !important;
	 opacity: 0; !important;
	 margin: 0; !important;
	 padding: 0; !important;
	 border: none;!important;
	}	
	
	 #loadingmsg {
      color: black;
      background:tranparent!important;
      padding: 10px;
      position: fixed;
      top: 50%;
      left: 50%;
      z-index: 100;
      margin-right: -25%;
      margin-bottom: -25%;
      }
      #loadingover {
      background: white;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }

	</style>
	<script> 		
		
	$(document).ready(function(){
	
    document.getElementById('attachmentFile').addEventListener('change', handleFileSelect, false); 
             
        $("#attachmentFile").click(function(){
        	var ua = window.navigator.userAgent;
			if( ua.indexOf("Android") >= 0 )
            {
              var androidversion = ua.slice(ua.indexOf("Android")+8,ua.indexOf("Android")+13);
              if((androidversion=='4.4.1')||(androidversion=='4.4.2')||(androidversion=='4.4.3')){
              	alert("cuttent android version do no support attachment");
              }
             }
		});	     
		
		$("#showExpandedCc").click(function(){
		   $("#myExpandedCcNBcc").show();
		    $("#foldCc").hide();
		});
		
		  $("#sendBtn").click(function(){	
		  	
		  	var totalReceiverAddress =  $("input[name='myReceiverFilter']").val();
			var totalCcAddress =  $("input[name='myCcFilter']").val();
			var totalBccAddress =  $("input[name='myBccFilter']").val();
			var subject =  $("input[name='Subject']").val();
			if(totalReceiverAddress == ""&& totalCcAddress == "" && totalBccAddress == ""){
				alert("請輸入郵件收件人");
			}else{
				if(subject == ""){
					 var r = confirm("該郵件沒有主題。您確定要發送嗎?");
					if (r == true) {
						showLoading();
					    SendEmail();
					}
				}else{
					showLoading();
                    SendEmail();
				}		
			}
		});
		
		function SendEmail(){
		 var editableEmailContentDivBody= document.getElementById("editableEmailContentBody").innerHTML;
         document.getElementById("emailMainBody").value = editableEmailContentDivBody;
		 document.email_compose_process.action = "<?=$emil_reply_forward_send_form_action?>";
         document.email_compose_process.submit();
		}
		
		
		function showLoading() {
		    document.getElementById('loadingmsg').style.display = 'block';
		    document.getElementById('loadingover').style.display = 'block';
		}
		
		$("#saveEmailToTrash").click(function(){
			 var editableEmailContentDivBody= document.getElementById("editableEmailContentBody").innerHTML;
             document.getElementById("emailMainBody").value = editableEmailContentDivBody;
			 document.email_compose_process.action = "<?=$emil_reply_forward_save_form_action?>";
            $("#email_compose_process").submit();
		});
		initilizeNewButtons('To','layer1');
		initilizeNewButtons('Cc','layer1');
		initilizeNewButtons('Bcc','layer1');
		
		$('.editable').each(function(){
        this.contentEditable = true;
		});		
   });	
   
      function handleFileSelect(e) {
      	var selDiv = document.querySelector("#selectedFiles");
		var originalSelDivHTML = selDiv.innerHTML;
        if(!e.target.files || !window.FileReader) return;
		selDiv.innerHTML = originalSelDivHTML;
		var files = e.target.files;
		var filesArr = Array.prototype.slice.call(files);
		var attachImgSrc= <?=$myAttachmentIconUrl?>;
		var deleteImgSrc= <?=$myCloseIconUrl?>;
		for(var i=0; i < files.length; i++) {
		<!--var pullPath =  e.target.value;-->
			     var f = files[i];
				 var reader = new FileReader();
				 reader.onload = (function(theFile){
			    var name = theFile.name;
			    return function(e){
			    saveUploadData(e.target.result,name,function(obj) {
    	  	   	var imageInput = "<input type=\"hidden\" id =\"" + name +"\" name=\"chosenFiles[]\"  value=\"" + name +"\">";
                 var html = "<img src=\"" + attachImgSrc + "\">" + name + "<a href=\"#\"  id=\"deleteChosenFile\"  name=\"deleteChosenFile\"   style = \"width: 20px; margin-right: 10px;\" >"+ "<img src=\"" + deleteImgSrc + "\" id=\"deleteChosenFile_" + name +"\" onClick=\"delete_attachment_click(this.id)\">"+"</a>"+imageInput+"<br clear=\"left\"/>";
			    selDiv.innerHTML += html;
                 });<!--save the chosen image to-->
			    };
			})(f);  
			 reader.readAsDataURL(f); 
}
$('#attachmentFile').val('');
}

function saveUploadData(Data,ImageName,callback)
	{
		 var userID = <?=$uid?>;
		 var attachmentAction ="SAVE";
		 var currentEmailAttachmentLink = $("input[name='chosenUploadedAttachmentLink']").val();
		 if(currentEmailAttachmentLink==""){
		 	currentEmailAttachmentLink = "N";
		 }
		   $.post( "emailSaveAttachment.php",{Data:Data,ImageName:ImageName, userID:userID,currentEmailAttachmentLink:currentEmailAttachmentLink,attachmentAction:attachmentAction},function(data) {
             })
	  .done(function(data) {
	  	if(data==''){
	  		alert('the file is too large, upload failed');
	  	}else{
  			 if( $("input[name='chosenUploadedAttachmentLink']").val()==""){
  	  	    $("input[name='chosenUploadedAttachmentLink']").val(data);
  	          }
	  		callback();
	  	}
		  })
		  .fail(function() {
		  })
		  .always(function() {
		});
		  
		  
	}
	function delete_attachment_click(clicked_id){
		var selDiv = document.querySelector("#selectedFiles");
		var newHtml = '';
		var attachFiles=document.getElementsByName("chosenFiles[]");
        var attachFilesLength = attachFiles.length;
        var attachImgSrc= <?=$myAttachmentIconUrl?>;
		var deleteImgSrc= <?=$myCloseIconUrl?>;
        for(var i=0;i<attachFilesLength;i++){
        var currentElement = attachFiles[i];
		var currentID = currentElement.id;
		var currentDeleteImage = "deleteChosenFile_"+currentID;
		
		<!--generate new attachment-->
		if(clicked_id!=currentDeleteImage){
		   var name = currentID;
		   var imageInput = "<input type=\"hidden\" id =\"" + name +"\" name=\"chosenFiles[]\"  value=\"" + name +"\">";
		   var newHtml = newHtml+"<img src=\"" + attachImgSrc + "\">" + name + "<a href=\"#\"  id=\"deleteChosenFile\"  name=\"deleteChosenFile\"   style = \"width: 20px; margin-right: 10px;\" >"+ "<img src=\"" + deleteImgSrc + "\" id=\"deleteChosenFile_" + name +"\" onClick=\"delete_attachment_click(this.id)\">"+"</a>"+imageInput+"<br clear=\"left\"/>";
		}else{
		   delete_attachment_in_server(currentID); 
		}		
        }
        <!--refresh the attachment list-->
        selDiv.innerHTML = newHtml;
	}
    function delete_attachment_in_server(currentID){
     
         var userID = <?=$uid?>;
		 var attachmentAction ="DELETE";
		 var currentEmailAttachmentLink = $("input[name='chosenUploadedAttachmentLink']").val();
		 if(currentEmailAttachmentLink ==""){
		 currentEmailAttachmentLink = "N";
		 }
		   $.post( "emailSaveAttachment.php",{userID:userID,currentEmailAttachmentLink:currentEmailAttachmentLink,attachmentAction:attachmentAction,currentID:currentID},function(data) {
             })
	  .done(function(data) {
		  })
		  .fail(function() {
		  })
		  .always(function() {
		});
    }


<!--this function is for initilize each layer button or herf response function and there are three layers to get correspinding emailAddress List-->
<!--each click at each layer will trigger invoke this function-->
<!--the invoke part is trigger from its upper layer-->
function initilizeNewButtons(filterItem,listType) {	
    if(listType =='layer1'){	
    
    	if(filterItem =='To'){
    		var tOut_layer1To = null;
    		var clicked_layer1To = true;
    		 	<!--receive address-->
    $('#mySearchPickPlace ul').children('li').on('click', function (e) {
    e.preventDefault();
    }).on('mousedown', function () {
    	tOut_layer1To = setTimeout(function () {
        clicked_layer1To = false;
        }, 200);
    	}).on('mouseup', function () {
     <!--click case-->
    if (clicked_layer1To == true) {
       clearTimeout(tOut_layer1To);
      var selectedTypeDataName = $(this).attr('data-name');
      if(selectedTypeDataName == 0){
      	  loadXMLDoc(selectedTypeDataName,"ExternalGroup","","",<?php echo $uid?>,function(obj) {
          layer3UpdatelistView('To',obj);<!--initilze catid as ExternalGroup,go to layer3 directly-->
        }); 
      }
      else if(selectedTypeDataName == 1){
      	  loadXMLDoc(selectedTypeDataName,"ExternalContact","","",<?php echo $uid?>,function(obj) {
          updateCheckAddresslistView('To',obj);<!--initilze catid as ExternalContact,go to layer4 directly-->
        }); 
      }
      if(selectedTypeDataName == 2){
      	  loadXMLDoc(selectedTypeDataName,"INTERNAL_GROUP","","",<?php echo $uid?>,function(obj) {
          layer3UpdatelistView('To',obj);<!--initilze catid as INTERNAL_GROUP,go to layer3 directly-->
        }); 
      } else if(selectedTypeDataName ==3){
      	  loadXMLDoc(selectedTypeDataName,"","","",<?php echo $uid?>,function(obj) {
          updatelistView('To',obj);
        }); 
      }  
        } <!--end scroll or click--> 
        else {
    	<!--time out case just scroll-->
        clicked_layer1To = true;
        }
    });<!--end layer1 Filter TO select function-->
    	}else if(filterItem =='Cc'){
    		 var tOut_layer1Cc = null;
    		 var clicked_layer1Cc = true;
    		 <!--Cc -->
      $('#myCcSearchPickPlace ul').children('li').on('click', function (e) {
    e.preventDefault();
    }).on('mousedown', function () {
    	tOut_layer1Cc = setTimeout(function () {
        clicked_layer1Cc = false;
        }, 200);
    	}).on('mouseup', function () {
     <!--click case-->
    if (clicked_layer1Cc == true) {
       clearTimeout(tOut_layer1Cc);
        
      var selectedTypeDataName = $(this).attr('data-name');
      if(selectedTypeDataName == 0){
      	  loadXMLDoc(selectedTypeDataName,"ExternalGroup","","",<?php echo $uid?>,function(obj) {
          layer3UpdatelistView('Cc',obj);<!--initilze catid as INTERNAL_GROUP,go to layer3 directly-->
        }); 
      }
      else if(selectedTypeDataName == 1){
      	  loadXMLDoc(selectedTypeDataName,"ExternalContact","","",<?php echo $uid?>,function(obj) {
          updateCheckAddresslistView('Cc',obj);<!--initilze catid as INTERNAL_GROUP,go to layer3 directly-->
        }); 
      }
      if(selectedTypeDataName == 2){
      	  loadXMLDoc(selectedTypeDataName,"INTERNAL_GROUP","","",<?php echo $uid?>,function(obj) {
          layer3UpdatelistView('Cc',obj);<!--initilze catid as INTERNAL_GROUP,go to layer3 directly-->
        }); 
      } else if(selectedTypeDataName ==3){
      	  loadXMLDoc(selectedTypeDataName,"","","",<?php echo $uid?>,function(obj) {
          updatelistView('Cc',obj);
        }); 
      } 
          } else {
    	<!--time out case just scroll-->
        clicked_layer1Cc = true;
        }
       
    });<!--end layer1 Cc select function-->
    	}else if(filterItem =='Bcc'){
    		 var tOut_layer1Bcc = null;
	         var clicked_layer1Bcc = true;
    		<!--Bcc -->
       $('#myBccSearchPickPlace ul').children('li').on('click', function (e) {
    e.preventDefault();
    }).on('mousedown', function () {
    	tOut_layer1Bcc = setTimeout(function () {
        clicked_layer1Bcc = false;
        }, 200);
    	}).on('mouseup', function () {
     <!--click case-->
    if (clicked_layer1Bcc == true) {
       clearTimeout(tOut_layer1Bcc);
       
      var selectedTypeDataName = $(this).attr('data-name');
      if(selectedTypeDataName == 0){
      	  loadXMLDoc(selectedTypeDataName,"ExternalGroup","","",<?php echo $uid?>,function(obj) {
          layer3UpdatelistView('Bcc',obj);<!--initilze catid as INTERNAL_GROUP,go to layer3 directly-->
        }); 
      }
      else if(selectedTypeDataName == 1){
      	  loadXMLDoc(selectedTypeDataName,"ExternalContact","","",<?php echo $uid?>,function(obj) {
          updateCheckAddresslistView('Bcc',obj);<!--initilze catid as INTERNAL_GROUP,go to layer3 directly-->
        }); 
      }
      if(selectedTypeDataName == 2){
      	  loadXMLDoc(selectedTypeDataName,"INTERNAL_GROUP","","",<?php echo $uid?>,function(obj) {
          layer3UpdatelistView('Bcc',obj);<!--initilze catid as INTERNAL_GROUP,go to layer3 directly-->
        }); 
      } else if(selectedTypeDataName ==3){
      	  loadXMLDoc(selectedTypeDataName,"","","",<?php echo $uid?>,function(obj) {
          updatelistView('Bcc',obj);
        }); 
      }  <!--end selectedTypeDataName-->
         } else {
    	<!--time out case just scroll-->
        clicked_layer1Bcc = true;
        }<!--end scroll and click case-->
    });<!--end layer1 Bcc select function-->
    	}
    }
    <!--layer2-->
    else if(listType =='layer2'){
    	
    if(filterItem =='To'){
    var tOut_layer2To = null;
	var clicked_layer2To = true;
   $("#layer2backTo").click(function(){
      updateLayer1ListView('To');
    }); <!--end layer2back-->	
    	
    	
     $('#mySearchPickPlace ul').children('li').on('click', function (e) {
     	 e.preventDefault();
       }).on('mousedown', function () {
       	   tOut_layer2To = setTimeout(function () {
           clicked_layer2To = false;
       }, 200);
      }).on('mouseup', function () {
          if (clicked_layer2To == true) {
         clearTimeout(tOut_layer2To);    
          
         var selectedTypeDataName = $(this).attr('data-name');
        var selectedTypeDataLayer1Item = $(this).attr('id');
        if(selectedTypeDataName!='layer2backButton'){
      loadXMLDoc(selectedTypeDataLayer1Item,selectedTypeDataName,"","",<?php echo $uid?>,function(obj) {
      layer3UpdatelistView('To',obj);
      });}<!--end click case-->              
         	  } else {
        clicked_layer2To = true;
         }
      }); <!--end layer2 select function--> <!--end To-->     		 
    }
    
    else if(filterItem =='Cc'){	
    var tOut_layer2Cc = null;
	var clicked_layer2Cc = true;

    $("#layer2backCc").click(function(){
      updateLayer1ListView('Cc');
    }); <!--end layer2back-->	
    	
    $('#myCcSearchPickPlace ul').children('li').on('click', function (e) {
    e.preventDefault();
    }).on('mousedown', function () {
       tOut_layer2Cc = setTimeout(function () {
        clicked_layer2Cc = false;
    }, 200);

}).on('mouseup', function () {
	  if (clicked_layer2Cc == true) {
        clearTimeout(tOut_layer2Cc);
        
    var selectedTypeDataName = $(this).attr('data-name');
    var selectedTypeDataLayer1Item = $(this).attr('id');
    
     if(selectedTypeDataName!='layer2backButton'){
      loadXMLDoc(selectedTypeDataLayer1Item,selectedTypeDataName,"","",<?php echo $uid?>,function(obj) {
      layer3UpdatelistView('Cc',obj);
    });	}<!--end click item case-->
     } else {
        clicked_layer2Cc = true;}
    });<!--end layer2 select function--> <!--end Cc-->  
    
     }else if(filterItem =='Bcc'){
     	
    var tOut_layer2Bcc = null;
	var clicked_layer2Bcc = true;
	
     $("#layer2backBcc").click(function(){
      updateLayer1ListView('Bcc');
    }); <!--end layer2back-->	
    	
    $('#myBccSearchPickPlace ul').children('li').on('click', function (e) {
    e.preventDefault();
}).on('mousedown', function () {
    tOut_layer2Bcc = setTimeout(function () {
    clicked_layer2Bcc = false;
    }, 200);
}).on('mouseup', function () {
    if (clicked_layer2Bcc == true) {
    	clearTimeout(tOut_layer2Bcc);
    	
    var selectedTypeDataName = $(this).attr('data-name');
    var selectedTypeDataLayer1Item = $(this).attr('id');
        if(selectedTypeDataName!='layer2backButton'){
      loadXMLDoc(selectedTypeDataLayer1Item,selectedTypeDataName,"","",<?php echo $uid?>,function(obj) {
      layer3UpdatelistView('Bcc',obj);
    });	}<!--end click case-->
    }else {
        clicked_layer2Bcc = true;}
    });<!--end layer2 select function--><!--end Bcc--> 
    }
    }   
    
    <!--layer3-->
    else if(listType =='layer3'){
 	<!--receive address-->
 	<!--layer3 has three functions,1:back;2:select All;3:finish select buttons-->
 	<!--initilize back and finish button when user choose a email address type and refresh listview -->
	
 	if(filterItem =='To'){
    var tOut_layer3To = null;
	var clicked_layer3To = true;
	
 	 $("#layer3backTo").click(function(){
   	<!--load layer2 list and set buttons' function-->
   	<!--important-->
       	  var selectedTypeDataLayer1Item = $(this).attr('value');
       	   if (selectedTypeDataLayer1Item == 0||selectedTypeDataLayer1Item == 2){
       	   	<!--external group and internal group have no layer2, if the layer1 of current layer3 is 0 or2,then go back to layer1 directly-->
       	     updateLayer1ListView('To'); 
       	   }else{
       	   	<!--only applicable for internal contact-->
       	   loadXMLDoc(selectedTypeDataLayer1Item,"","","",<?php echo $uid?>,function(obj) {
           updatelistView(filterItem,obj);
            });   
       	   }<!--end if layer1 equals 2-->
		}); <!--end back-->
 		
 			<!--layer3 click function-->
   $('#mySearchPickPlace ul').children('li').on('click', function (e) {
    e.preventDefault();
}).on('mousedown', function () {
    tOut_layer3To = setTimeout(function () {
    clicked_layer3To = false;
    }, 200);
}).on('mouseup', function () {
    if (clicked_layer3To == true) {
        clearTimeout(tOut_layer3To);
        
    var currentItemGroupID = $(this).attr('data-name');
    var IDArray = $(this).attr("id");
    var res = $(this).attr("id").split(",");
    var selectedTypeDataLayer1Item =  res[0];  
    var currentItemCatID =  res[1]; 
    
    if(currentItemGroupID!='layer3backButton'){
      <!--important need catid  there-->
      loadXMLDoc(selectedTypeDataLayer1Item,currentItemCatID,currentItemGroupID,"",<?php echo $uid?>,function(obj) {
      updateCheckAddresslistView('To',obj);<!--layer4 is the final email list with checkbox-->
      });
      }<!--end if not back-->
      } else {
        clicked_layer3To = true;}
      
      }); <!--end  list click-->				 
    }
    
    else if(filterItem =='Cc'){	
   var tOut_layer3Cc = null;
	var clicked_layer3Cc = true;
    $("#layer3backCc").click(function(){
   	<!--load layer2 list and set buttons' function-->
   	<!--important-->
       	  var selectedTypeDataLayer1Item = $(this).attr('value');
       	   if (selectedTypeDataLayer1Item == 0||selectedTypeDataLayer1Item == 2){
       	   	<!--external group and internal group have no layer2, if the layer1 of current layer3 is 0 or2,then go back to layer1 directly-->
       	     updateLayer1ListView('Cc'); 
       	   }else{
       	   	<!--only applicable for internal contact-->
       	   loadXMLDoc(selectedTypeDataLayer1Item,"","","",<?php echo $uid?>,function(obj) {
           updatelistView('Cc',obj);
            });   
       	   }<!--end if layer1 equals 2-->
		}); <!--end back-->
 		
 	<!--layer3 click function-->
   $('#myCcSearchPickPlace ul').children('li').on('click', function (e) {
    e.preventDefault();
}).on('mousedown', function () {
    tOut_layer3Cc = setTimeout(function () {
    clicked_layer3Cc = false;
    }, 200);
}).on('mouseup', function () {
    if (clicked_layer3Cc == true) {
        clearTimeout(tOut_layer3Cc);
        
    var currentItemGroupID = $(this).attr('data-name');
    var IDArray = $(this).attr("id");
    var res = $(this).attr("id").split(",");
    var selectedTypeDataLayer1Item =  res[0];  
    var currentItemCatID =  res[1]; 
    
    if(currentItemGroupID!='layer3backButton'){
      <!--important need catid  there-->
      loadXMLDoc(selectedTypeDataLayer1Item,currentItemCatID,currentItemGroupID,"",<?php echo $uid?>,function(obj) {
      updateCheckAddresslistView('Cc',obj);<!--layer4 is the final email list with checkbox-->
      });
      }<!--end if not back-->
         } else {
        clicked_layer3Cc = true;}
      }); <!--end  list click--><!--end Cc-->  


     }else if(filterItem =='Bcc'){
     var tOut_layer3Bcc = null;
	var clicked_layer3Bcc = true;
     $("#layer3backBcc").click(function(){
   	<!--load layer2 list and set buttons' function-->
   	<!--important-->
       	  var selectedTypeDataLayer1Item = $(this).attr('value');
       	   if (selectedTypeDataLayer1Item == 0||selectedTypeDataLayer1Item == 2){
       	   	<!--external group and internal group have no layer2, if the layer1 of current layer3 is 0 or2,then go back to layer1 directly-->
       	     updateLayer1ListView('Bcc'); 
       	   }else{
       	   	<!--only applicable for internal contact-->
       	   loadXMLDoc(selectedTypeDataLayer1Item,"","","",<?php echo $uid?>,function(obj) {
           updatelistView('Bcc',obj);
            });   
       	   }<!--end if layer1 equals 2-->
		}); <!--end back-->
 		
    <!--layer3 click function-->
   $('#myBccSearchPickPlace ul').children('li').on('click', function (e) {
    e.preventDefault();
}).on('mousedown', function () {
    tOut_layer3Bcc = setTimeout(function () {
    clicked_layer3Bcc = false;
    }, 200);
}).on('mouseup', function () {
    if (clicked_layer3Bcc == true) {
        clearTimeout(tOut_layer3Bcc);
    var currentItemGroupID = $(this).attr('data-name');
    var IDArray = $(this).attr("id");
    var res = $(this).attr("id").split(",");
    var selectedTypeDataLayer1Item =  res[0];  
    var currentItemCatID =  res[1]; 
    
    if(currentItemGroupID!='layer3backButton'){
      <!--important need catid  there-->
      loadXMLDoc(selectedTypeDataLayer1Item,currentItemCatID,currentItemGroupID,"",<?php echo $uid?>,function(obj) {
      updateCheckAddresslistView('Bcc',obj);<!--layer4 is the final email list with checkbox-->
      });
      }<!--end if not back-->
        } else {
        clicked_layer3Bcc = true;}
      }); <!--end  list click--><!--end Bcc--> 
    }
    }  

     <!--layer4-->
  
  else if(listType =='layer4'){
  	   if(filterItem =='To'){ 	    		    	
     <!--initilize back button-->
  	$("#layer4BackTo").click(function(){
           var currentItemCatID =  $(this).attr("name"); 
           var selectedTypeDataLayer1Item = $(this).attr('value');
            if(selectedTypeDataLayer1Item == 1){
            <!--the external contact item only have two layer that is layer1 and layer4-->
            updateLayer1ListView('To'); 
            }
            else{
          loadXMLDoc(selectedTypeDataLayer1Item,currentItemCatID,"","",<?php echo $uid?>,function(obj) {
          layer3UpdatelistView('To',obj);
        });
            }   
		}); <!--end back-->
		
     <!--initilize select all-->
	$('input#selectAllEmailAddressInReceiveFilterTo').change(function() {
     var checked_status = this.checked;
     $("input[name='selectArrayInReceiveFilterTo']").each(function(){ 
      this.checked = checked_status;
      });
      $("input[name='selectArrayInReceiveFilterTo']").checkboxradio("refresh");
	});
 <!--initilize finish button--> 
   $("#receiveAddressChoseFinishTo").click(function(){
	  <!--generate the new array-->
	  var receiveAddress = '';
	  var receiveAddressIDs ='';
	  var receiveExternalAddress ='';
	  $("input[name='selectArrayInReceiveFilterTo']").each(function(){ 
	  	var checked_status = this.checked;
	  	if(checked_status){
	  		var checked_email_address =this.value;
	  		var checked_email_uid = this.id;
	  		receiveAddress += checked_email_address+";";
	  		if(checked_email_uid!='-1'){
	  			receiveAddressIDs += checked_email_uid+";"
	  		}else{
	  			receiveExternalAddress +=checked_email_address+";";
	  		}
	  		
	  	}	  	  		
	  });
	var totalAddress =  $("input[name='myReceiverFilter']").val()+receiveAddress;
	$("input[name='myReceiverFilter']").val(totalAddress);
	
	var totalChoosenAddress =  $("input[name='chosenFromEmailAddressListGenerator']").val()+receiveAddress;
	$("input[name='chosenFromEmailAddressListGenerator']").val(totalChoosenAddress);
	
	if(receiveAddressIDs!=''){
	var totalAddressIDs =  $("input[name='chosenRecipientIDs']").val()+receiveAddressIDs;
	$("input[name='chosenRecipientIDs']").val(totalAddressIDs);
	}
	
	if(receiveExternalAddress!=''){
	var totalExternalAddress =  $("input[name='chosenExternalRecipientAddresses']").val()+receiveExternalAddress;
	$("input[name='chosenExternalRecipientAddresses']").val(totalExternalAddress);
	}
	 
	updateLayer1ListView('To');
	 
});   <!--end finish button--><!--end To-->	 	    	
 
    }
    
    else if(filterItem =='Cc'){	
    	
    	$("#layer4BackCc").click(function(){

           var currentItemCatID =  $(this).attr("name"); 
           var selectedTypeDataLayer1Item = $(this).attr('value');
            if(selectedTypeDataLayer1Item == 1){
            <!--the external contact item only have two layer that is layer1 and layer4-->
            updateLayer1ListView('Cc'); 
            }
            else{
          loadXMLDoc(selectedTypeDataLayer1Item,currentItemCatID,"","",<?php echo $uid?>,function(obj) {
          layer3UpdatelistView('Cc',obj);
        });
            }   
		}); <!--end back-->
		
		  <!--initilize select all-->
	$('input#selectAllEmailAddressInReceiveFilterCc').change(function() {
     var checked_status = this.checked;
     $("input[name='selectArrayInReceiveFilterCc']").each(function(){ 
      this.checked = checked_status;
      });
      $("input[name='selectArrayInReceiveFilterCc']").checkboxradio("refresh");
	});
		
		
 <!--initilize finish button--> 
   $("#receiveAddressChoseFinishCc").click(function(){
	  <!--generate the new array-->
	  var receiveAddress = '';
	  var receiveAddressCcIDs ='';
	  var receiveExternalCcAddress ='';
	  $("input[name='selectArrayInReceiveFilterCc']").each(function(){ 
	  	var checked_status = this.checked;
	  	var checked_email_uid = this.id;
	  	if(checked_status){
	  		var checked_email_address =this.value;
	  		receiveAddress += checked_email_address+";";
	  		if(checked_email_uid!='-1'){
	  			receiveAddressCcIDs += checked_email_uid+";"
	  		}else{
	  			receiveExternalCcAddress +=checked_email_address+";";
	  		}
	  	}	  	  		
	  });
	 var totalAddress =  $("input[name='myCcFilter']").val()+receiveAddress;
	 $("input[name='myCcFilter']").val(totalAddress);
	 
	 var totalChoosenAddress =  $("input[name='chosenCCFromEmailAddressListGenerator']").val()+receiveAddress;
	$("input[name='chosenCCFromEmailAddressListGenerator']").val(totalChoosenAddress);
	
	 if(receiveAddressCcIDs!=''){
	var totalAddressIDs =  $("input[name='chosenInternalCCIDs']").val()+receiveAddressCcIDs;
	$("input[name='chosenInternalCCIDs']").val(totalAddressIDs);
	}
		if(receiveExternalCcAddress!=''){
	var totalExternalAddress =  $("input[name='chosenExternalCcAddresses']").val()+receiveExternalCcAddress;
	$("input[name='chosenExternalCcAddresses']").val(totalExternalAddress);
	}
	 
	updateLayer1ListView('Cc');
	 
});   <!--end finish button-->	 <!--end Cc-->	

     }else if(filterItem =='Bcc'){
     	
     	$("#layer4BackBcc").click(function(){

           var currentItemCatID =  $(this).attr("name"); 
           var selectedTypeDataLayer1Item = $(this).attr('value');
            if(selectedTypeDataLayer1Item == 1){
            <!--the external contact item only have two layer that is layer1 and layer4-->
            updateLayer1ListView('Bcc'); 
            }
            else{
          loadXMLDoc(selectedTypeDataLayer1Item,currentItemCatID,"","",<?php echo $uid?>,function(obj) {
          layer3UpdatelistView('Bcc',obj);
        });
            }   
		}); <!--end back-->

		  <!--initilize select all-->
	$('input#selectAllEmailAddressInReceiveFilterBcc').change(function() {
     var checked_status = this.checked;
     $("input[name='selectArrayInReceiveFilterBcc']").each(function(){ 
      this.checked = checked_status;
      });
      $("input[name='selectArrayInReceiveFilterBcc']").checkboxradio("refresh");
	});
    	
 <!--initilize finish button--> 
   $("#receiveAddressChoseFinishBcc").click(function(){
	  <!--generate the new array-->
	  var receiveAddress = '';
	  var receiveAddressBccIDs ='';
	  var receiveExternalBccAddress ='';
	  $("input[name='selectArrayInReceiveFilterBcc']").each(function(){ 
	  	var checked_status = this.checked;
	  	if(checked_status){
	  		var checked_email_address =this.value;
	  		var checked_email_uid = this.id;
	  		receiveAddress += checked_email_address+";";
	  		if(checked_email_uid!='-1'){
	  			receiveAddressBccIDs += checked_email_uid+";"
	  		}else{
	  			receiveExternalBccAddress +=checked_email_address+";";
	  		}
	  	}	  	  		
	  });
     var totalAddress =  $("input[name='myBccFilter']").val()+receiveAddress;
	 $("input[name='myBccFilter']").val(totalAddress);
	 
	 var totalChoosenAddress =  $("input[name='chosenBCCFromEmailAddressListGenerator']").val()+receiveAddress;
	$("input[name='chosenBCCFromEmailAddressListGenerator']").val(totalChoosenAddress);
	 
	if(receiveAddressBccIDs!=''){
	var totalAddressIDs =  $("input[name='chosenInternalBCCIDs']").val()+receiveAddressBccIDs;
	$("input[name='chosenInternalBCCIDs']").val(totalAddressIDs);
	}
	
	if(receiveExternalBccAddress!=''){
	var totalExternalAddress =  $("input[name='chosenExternalBCCAddresses']").val()+receiveExternalBccAddress;
	$("input[name='chosenExternalBCCAddresses']").val(totalExternalAddress);
	}
	 
	updateLayer1ListView('Bcc');
	 
});   <!--end finish button--><!--end Bcc--> 
    }
 
  }<!--end if else-->
    }<!--end function initilizeNewButtons-->   

    function loadXMLDoc(LayerID,CatID,ChooseGroupID,ChooseUserID,userID,callback)
	{			
	<!--alert(LayerID+"|||catid:"+CatID+"|||groupid:"+ChooseGroupID+"|||"+userID);-->
	 $.post( "emailAddressListGenerator.php",{LayerID:LayerID, CatID:CatID, ChooseGroupID:ChooseGroupID, ChooseUserID: ChooseUserID , userID:userID },function(data) {
             })
	  .done(function(data) {
		var obj = JSON.parse(data);	
		 callback(obj, CatID,ChooseGroupID,ChooseUserID,userID);
		  })
		  .fail(function() {
		  })
		  .always(function() {
		});
	}
	
	<!--update layer1 and init corresponding buttons-->
	function updateLayer1ListView(filterType){
	   var output = ''; 
       var jsArray = ["<?=$AddressTypeArr[0]?>", "<?=$AddressTypeArr[1]?>", "<?=$AddressTypeArr[2]?>","<?=$AddressTypeArr[3]?>"];
	   for (i = 0; i < jsArray.length; i++) { 
	   output +='<li data-name = "'+i+'"><a href='+"#"+'>'+jsArray[i]  +'</a></li>';        
	    }
        refreshListView(filterType,output);
		initilizeNewButtons(filterType,'layer1');	
	}
	
	<!--update layer2 and init corresponding buttons-->
	function updatelistView(filterType,obj){
	   var output = '';   
       var layer1Item = obj.layerID;
       var layer2backButtonID = "layer2back"+filterType;
       var layer2backButtonDataName = "layer2backButton"
       var backOperationString ='<a href='+ "#" + ' id="'+layer2backButtonID+'" class="ui-btn-left ui-btn ui-btn-inline ui-mini ui-corner-all ui-btn-icon-left ui-icon-carat-l" style ="background-color: #fff; padding-top: 20px;padding-left: 20px;padding-bottom: 20px;padding-right: 20px;left: 5px;border-left-width: 0px;margin-bottom: 0px;margin-top: 0px;margin-right: 0px;border-right-width: 0px;border-top-width: 0px;border-bottom-width: 0px;"></a>';      
	   output +='<li data-name = "'+layer2backButtonDataName+'" style="padding-top:20px;padding-bottom:25px;"><div>'+backOperationString+'</div></li>';  
	   for (i = 0; i < obj.addressArr.length; i++) { 
	   output +='<li data-name = "'+obj.addressArr[i].groupType+'" id ='+layer1Item+' ><a href='+"#"+'>'+ obj.addressArr[i].groupName +'</a></li>';        
	    }
        refreshListView(filterType,output);
		initilizeNewButtons(filterType,'layer2');	
	}
	
	<!--update layer3 and init corresponding buttons-->
	function layer3UpdatelistView(filterType,obj){
	   <!--generate data-->
	   var output = '';   
       var catID = obj.CatID;
       var layer1Item = obj.layerID;<!--to carry the layer1 value that the layer3 belong to-->
       var listItemID = layer1Item+","+catID;
       var layer3backButtonID = "layer3back"+filterType;
       var backOperationString ='<a href='+ "#" + ' id="'+layer3backButtonID+'" value='+layer1Item+' class="ui-btn-left ui-btn ui-btn-inline ui-mini ui-corner-all ui-btn-icon-left ui-icon-carat-l" style ="background-color: #fff; padding-top: 20px;padding-left: 20px;padding-bottom: 20px;padding-right: 20px;left: 5px;border-left-width: 0px;margin-bottom: 0px;margin-top: 0px;margin-right: 0px;border-right-width: 0px;border-top-width: 0px;border-bottom-width: 0px;"></a>';      
	   output +='<li data-name ="layer3backButton" style="padding-top:20px;padding-bottom:25px;"><div>'+backOperationString+'</div></li>';  
	   for (i = 0; i < obj.addressArr.length; i++) { 
	   output +='<li id ="'+listItemID+'" data-name = "'+obj.addressArr[i].groupType+'"><a href='+"#"+'>'+ obj.addressArr[i].groupName +'</a></li>';        
	    }
	    refreshListView(filterType,output);
		initilizeNewButtons(filterType,'layer3');
	}
	<!--update layer4 and init corresponding buttons-->
	function updateCheckAddresslistView(filterType,obj){
		<!--generate data-->
	   var output = '';
       var catID = obj.CatID;<!--determine current email adderess's layer2-->
       var layer1Item = obj.layerID;
       var ChooseGroupID = obj.ChooseGroupID;<!--determine current email adderess's layer3-->
       var layer4backButtonID = "layer4Back"+filterType;
       var selectAllButtonID = "selectAllEmailAddressInReceiveFilter"+filterType;
       var selectselectArray = "selectArrayInReceiveFilter"+filterType;
       var finishButtonID = "receiveAddressChoseFinish"+filterType;
       var backOperationString ='<a href='+ "#" + ' id="'+layer4backButtonID+'" value = '+layer1Item+' name = '+catID+'  class="ui-btn-left ui-btn ui-btn-inline ui-mini ui-corner-all ui-btn-icon-left ui-icon-carat-l" style ="background-color: #fff; padding-top: 20px;padding-left: 20px;padding-bottom: 20px;padding-right: 20px;left: 5px;border-left-width: 0px;margin-bottom: 0px;margin-top: 0px;margin-right: 0px;border-right-width: 0px;border-top-width: 0px;border-bottom-width: 0px;"></a>';
       var finishOperationString = '<a href='+"#"+'  id="'+finishButtonID+'" name="receiveAddressChoseFinish" class="ui-nodisc-icon ui-btn-right ui-btn ui-btn-b ui-btn-inline ui-mini ui-corner-all" style = "background-color:#fff;border-color:#FFFFFF;color:#000;"><?=$Lang['Gamma']['App']['Finish']?></a>';        
       output +='<li style="padding-top:20px;padding-bottom:25px;"><div>'+backOperationString+finishOperationString+'</div></li>';    
       output +='<li><input type = "checkbox" name = "selectTotalArrayInReceiveFilter[]"  id ="'+selectAllButtonID+'">'+"<?=$_SELECT_ALL?>"+'</li>';
      
       for (i = 0; i < obj.addressArr.length; i++) { 
       var currentID = '"'+selectAllButtonID+i+'"';
       var currentCheckBoxID = '';
       if((layer1Item=='2')||(layer1Item=='3'))
       {
       	currentCheckBoxID = obj.addressArr[i].groupName;
       }else{
       	currentCheckBoxID = '-1';
       }
       output +='<li><input type = "checkbox" name = "'+selectselectArray+'"  id = '+currentCheckBoxID+' class = "custom" value = "'+ obj.addressArr[i].groupType+'">'+ obj.addressArr[i].groupType +'</li>';        
        }
        refreshListView(filterType,output);
        initilizeNewButtons(filterType,'layer4');
	}
	
	function layer2CallBack(selectedTypeDataName,selectedTypeDataName,filterItem){
	  if(selectedTypeDataName!='layer2backButton'){
      loadXMLDoc(selectedTypeDataLayer1Item,selectedTypeDataName,"","",<?php echo $uid?>,function(obj) {
      layer3UpdatelistView(filterItem,obj);
    });	}
	}
	
	function refreshListView(filterType,output){
		if(filterType=='To'){
		$("#myReceiverFilterAddressList").empty();
		$('#myReceiverFilterAddressList').append(output).listview('refresh');   
		initilizeNewButtons('layer3');		
		}else if(filterType=='Cc'){
		$("#myCcFilterAddressList").empty();
		$('#myCcFilterAddressList').append(output).listview('refresh');   
		}else if(filterType=='Bcc'){
		$("#myBccFilterAddressList").empty();
		$('#myBccFilterAddressList').append(output).listview('refresh');   
		}
	}
	
	</script>
	</div><!-- /page -->
	</body>
</html>