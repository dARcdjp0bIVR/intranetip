<?php
//using: 
$PATH_WRT_ROOT = '../../../';

$parLang = $_GET['parLang'];

switch (strtolower($parLang)) {
case 'en':
$intranet_hardcode_lang = 'en';
break;
default:
$intranet_hardcode_lang = 'b5';
}

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libpwm.php");
$uid = $_GET['uid'];
$_SESSION['UserID'] = $uid;
//intranet_auth();
intranet_opendb();


$libDb = new libdb();
$libeClassApp = new libeClassApp(); 
$libpwm= new libpwm();
$linterface = new interface_html();
$folderAssoAry ="";
$defaultFolder = array();
$personalFolder = array();

$token = $_GET['token'];
$ul = $_GET['ul'];
$AttachmentWorksOnCurrentVersion = $_GET['AttachmentWorksOnCurrentVersion'];
$passwordAry = $libpwm->getData($uid);
foreach((array)$passwordAry as $_folderType => $_folderKey){
	if($_folderType==$uid)$password=$_folderKey;
}

$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if (!$isTokenValid) {
	die("No access right to see this page.");
}

//    $curUserId = $uid;
	$currentUID = $_GET["CurrentUID"];
	$thisFolder= $_GET["TargetFolderName"];

	$_displayName = $Lang['Gamma']['SystemFolderName'][$thisFolder];
    if($_displayName ==''){
    $_displayName = $thisFolder;
	}
	
if ($plugin['imail_gamma']) {
    $sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$uid."'";
	$dataAry = $libDb->returnResultSet($sql);
	$iMapEmail = $dataAry[0]['ImapUserEmail'];
	$iMapPwd = $password;
    $IMap = new imap_gamma(false,$iMapEmail, $iMapPwd);
	$IMap->IMapCache = new imap_cache_agent($iMapEmail, $iMapPwd);

	if($thisFolder!="INBOX"){
    	$Folder = "INBOX.".$thisFolder;
    }
    else{
    	$Folder = $thisFolder;
    }
    
   $thisFolder = urlencode($thisFolder);//encode thisFolder for url use handle chinese folder case
   $_displayName = IMap_Decode_Folder_Name($_displayName);
    
	
   $Folder = (isset($Folder) && $Folder != "") ? stripslashes($Folder) : $IMap->InboxFolder;

   $CacheNewMail = false;
   $currentMailRecord = $IMap->getMailRecord($Folder, $currentUID, "");
   $CurUserAddress = $IMap->CurUserAddress;
   $CurUserPassword = $IMap->CurUserPassword;
   $currentMailRecordContent = $currentMailRecord[0];
   $flag=$currentMailRecordContent["flagged"];
   $currentMailShowedContent = array();
   $currentAttachment = array();

   foreach((array)$currentMailRecordContent as $_emailKey => $_emailCurrentDetail){
            	  	  if($_emailKey=='fromname'){
            	  	  	 $__fromname = $_emailCurrentDetail;
            	  	  	 }else if($_emailKey=='subject'){
            	  	  	 	  $__subject = $_emailCurrentDetail;
            	  	  	 	   }else if($_emailKey=='dateReceive'){
            	  	  	 	   	   $__dateReceive = $_emailCurrentDetail;}
            	  	  	 	   	   else if($_emailKey=='message'){
            	  	  	 	   	   	$__message = $_emailCurrentDetail;
            	  	  	 	   	   }else if($_emailKey=='attach_parts'){
            	  	  	 	   	   	$currentAttachment = $_emailCurrentDetail;
            	  	  	 	   	   }
            	  	  }           	  	  
            	  	  
    $attachmentLinkPrefix = "downLoadAttachment.php?MailAddress=".$CurUserAddress."&MailPassword=".$CurUserPassword."&Folder=".$thisFolder."&MessageID=".$currentUID."&PartNumber=";

	$currentMailShowedContent['Sender']=$__fromname;
	$currentMailShowedContent['Subject']=$__subject;
	$currentMailShowedContent['Date']=$__dateReceive;
	$currentMailShowedContent['Content']=$__message;
}//end gamma
else{
	 $lc = new libcampusquota2007($uid);

	 	if($thisFolder =='INBOX'){
		$folderID[0] = 2;
		$senderIDTitle = "SenderID";
	}else if($thisFolder =='Drafts'){
		$folderID[0] = 1;
		$senderIDTitle = "SenderID";
	}else if($thisFolder =='Sent'){
		$folderID[0] = 0;
		$senderIDTitle = "RecipientID";
	}else if($thisFolder == 'Trash'){
        $folderID[0] = -1;
        $senderIDTitle = "SenderID";
	}else{
	$sql = "select FolderID from INTRANET_CAMPUSMAIL_FOLDER where OwnerID='$uid' and foldername = '".$libDb->Get_Safe_Sql_Query($thisFolder)."'";
	$folderID = $libDb->returnVector($sql);
	$senderIDTitle = "SenderID";
	}
    if( $folderID[0] == -1){
	 $sql  = "select Message,".$senderIDTitle.",SenderEmail,ExternalTo,Subject,DateInput,RecordStatus,Attachment,IsAttachment,CampusMailFromID from INTRANET_CAMPUSMAIL where UserID ='$uid' and Deleted= '1' AND CampusMailID='$currentUID'";		
	}else{
	 $sql  = "select Message,".$senderIDTitle.",SenderEmail,ExternalTo,Subject,DateInput,RecordStatus,Attachment,IsAttachment,CampusMailFromID from INTRANET_CAMPUSMAIL where UserID ='$uid' and UserFolderID= '$folderID[0]' AND CampusMailID='$currentUID'";
	}
	    $MailHeader = $lc->returnArray($sql);	
	    $content = $MailHeader[0];
	    $subject = $content['Subject']; 
        $date = $content['DateInput']; 
        $senderID = str_replace('U', '', $content[$senderIDTitle]);
        $senderEmail = $content['SenderEmail']; 
        $message = $content['Message'];
        $recordStatus = $content['RecordStatus']; 
        $externalTo = $content['ExternalTo'];
        $_unSeen = ($recordStatus=="")?true:false;
        $isAttachment = $content['IsAttachment'];
        
        $atta = $content['CampusMailFromID']!=''?$content['CampusMailFromID']:$currentUID; 

        $getAttachmentSql = "select FileName,PartID from INTRANET_IMAIL_ATTACHMENT_PART where CampusMailID ='$atta'";
        $currentAttachment =$lc->returnArray($getAttachmentSql);        
        $attachmentLinkPrefix = "downLoadAttachment.php?uid=".$uid."&MessageID=".$atta."&PartNumber="; 

        if($_unSeen){
        $sql  = "UPDATE INTRANET_CAMPUSMAIL set RecordStatus='1'  where UserID ='$uid' and CampusMailID = '$currentUID'";
        $MailHeader = $lc->returnArray($sql);	
        }
        
        if(($senderID!=null)&&($senderID!='')){
        	if($parLang=='en'){
        		$nameLan = "EnglishName";
        	}else{
        		$nameLan = "ChineseName";
        	}
        $sql  = "select ".$nameLan." from INTRANET_USER where UserID ='$senderID'";  
        $SenderName = $lc->returnArray($sql);	
        $mailbox = $SenderName[0][$nameLan];
        }else{
    	if($thisFolder =='Sent'){
            $pieces = explode(" ", $externalTo);
        	}else{
		    $pieces = explode(" ", $senderEmail);
        	}
        	$mailbox = str_replace(";","",$pieces[0]);	
        }
   
	 	if ($subject != "") {
			$subjectPrint = (substr($subject, 0, 10) == "=?UTF-8?Q?") ? substr($subject, 10, -2) : $subject ;
		} else {
			$subjectPrint = "No subject";
		}
		$DateFormat = "Y-m-d";
		$prettydate='';
		if($date!=''){
	    $prettydate = date($DateFormat, strtotime(str_ireplace("UT","UTC",$date)));
		}
	$currentMailShowedContent['Sender']=$mailbox;
	$currentMailShowedContent['Subject']=$subjectPrint;
	$currentMailShowedContent['Date']=$prettydate;
	$currentMailShowedContent['Content']=$message;
	$flag ="IMAIL_NO_FLAG";
}
    $currentMailShowedContent['Content'] = htmlspecialchars_decode($currentMailShowedContent['Content']);
 
 	$myReplyIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_reply_white_36px.png"."\"";
	$myReplyAllIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_replyall_white_36px.png"."\"";
	$myForwardIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_forward_white_36px.png"."\"";
	$myTrashIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_trash_white_36px.png"."\"";
	
	$myInboxIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_inbox_white_36px.png"."\"";
	$myOutboxIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_outbox_white_36px.png"."\"";
	$myStarIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_star_36px.png"."\"";
    $myFilledStarIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/icon_star_filled_36px.png"."\"";
 
 	//initilize the photo according to its status
	$flagIconUrl ="";
	if($flag =='F'){
		$flagIconUrl =$myFilledStarIconUrl;
	}
	else if($flag !='IMAIL_NO_FLAG'){
		$flagIconUrl =$myStarIconUrl;
	}
	$emailListUrl = "email_list.php?token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang."&TargetFolderName=".$thisFolder."&AttachmentWorksOnCurrentVersion=".$AttachmentWorksOnCurrentVersion;
	$urlSuffix = "&TargetFolderName=".$thisFolder."&CurrentUID=".$currentUID."&token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang."&AttachmentWorksOnCurrentVersion=".$AttachmentWorksOnCurrentVersion;
	$current_email_delete_action_redirection = "email_delete_action_redirection.php?TargetFolderName=".$thisFolder."&CurrentUID=".$currentUID."&token=".$token."&uid=".$uid."&ul=".$ul."&parLang=".$parLang."&IndividualEmail=1";
   	$refreshIconUrl =$linterface->Get_Ajax_Loading_Image($noLang=true);
		
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<title>email list</title>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
		<script src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
		<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
		<script language="javascript">
			$(document).ready(function($) {
				$('div#descriptionDiv').css('width', Get_Screen_Width() - 10);
			});
		</script>
	</head>
	<body>
	<div data-role="page">
	
	<header data-role ="header"  data-position="fixed"  style ='background-color:#429DEA;'>
	 <div id='loadingmsg' style='display: none;'><br><?=$refreshIconUrl?><br></div>
     <div id='loadingover' style='display: none;'></div>
	 <a  href="#" data-icon="myapp-myOutbox" data-role="button" class = "ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon ui-btn-left" style="top: 5px;padding-bottom: 16px;padding-top: 16px;padding-left: 12px;padding-right: 20px;" onclick ="getEmailListPage();"></a>
     <p  style="text-align:left; margin-left:30px;" class = "ui-li-text-my-title-font"><?php echo"&nbsp&nbsp".$_displayName?></p>
   
     <div  class="ui-btn-right" data-role="controlgroup" data-type="horizontal">
     <a href="#"    data-icon="myapp-reply"    id = "Reply" data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="top: 5px;padding-bottom: 16px;padding-top: 16px;padding-left: 15px;padding-right: 20px;" onclick = 'ComposeEmail(this.id);'></a>
     <a href="#" data-icon="myapp-replyAll" id = "ReplyAll" data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="top: 5px;padding-bottom: 16px;padding-top: 16px;padding-left: 15px;padding-right: 20px;"  onclick = 'ComposeEmail(this.id);'></a>
     <a href="#"  data-icon="myapp-forward"  id = "Forward" data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="top: 5px;padding-bottom: 16px;padding-top: 16px;padding-left: 15px;padding-right: 20px;" onclick = 'ComposeEmail(this.id);' ></a>
     <a href="#popupDeleteComfirm" data-icon="myapp-trash"    data-rel ="popup"  data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="top: 5px;padding-bottom: 16px;padding-top: 16px;padding-left: 15px;padding-right: 20px;"  data-transition="pop"></a>
    </div>
    </header><!-- /header -->            
  <!-- Home -->
   <div role="main" class="ui-content" style = "padding-left: 0px;padding-top:0px" >  
   <?php

   echo"<table style='width:100%;'><tr>";
   if($flagIconUrl!=""){
   	 echo"<td class = 'my-column' valign='top' style='padding-left: 5px;padding-top: 20px;'><input type='image' id ='markEmail'  src=".$flagIconUrl." style='padding-bottom: 5px;padding-right: 0px;'></td>";
   }else{
     echo"<td class = 'my-column'></td>";
   }
   echo"<th class = 'my-column' ><div class = 'my-subjectWarpContent'> <p class = 'ui-li-text-my-h1'  style='text-align:left;'>".$currentMailShowedContent['Subject']."</p></div></th> 
   <td class = 'my-dateColumn'></td>
   </tr>
   <tr>
   <td class = 'my-column'></td>
   <th class = 'my-column'> <p class = 'ui-li-text-my-h3'>".$currentMailShowedContent['Sender']."</p></th>
   <td class = 'my-dateColumn'><p class = 'ui-li-text-my-h6' style='text-align:right;'>".$currentMailShowedContent['Date']."</p></td>
   </tr>
   </table> <div id='descriptionDiv'  style ='width: 100%; overflow-x: scroll; overflow-y: hidden;' class = 'my-messageWarpContent'>".$currentMailShowedContent['Content']."</div>";  	  

    echo "<div style ='padding-left: 30px;padding-right:30px ' class = 'my-messageWarpContent'>";
    
      for($attachmentCount=0;$attachmentCount<count($currentAttachment);$attachmentCount++){
     	$currentAttachmentName = $currentAttachment[$attachmentCount]['FileName'];
        $currentAttachmentLink =$attachmentLinkPrefix. $currentAttachment[$attachmentCount]['PartID'];
    	
    	echo "<a href=\"javascript:void(0);\" onclick=\"goDownloadAttachment('".$currentAttachment[$attachmentCount]['PartID']."');\"><img hspace='2' vspace='2' border='0' align='absmiddle' src='/images/file.gif'>".$currentAttachmentName."</a>";
        //echo "<a href=".$currentAttachmentLink."><img hspace='2' vspace='2' border='0' align='absmiddle' src='/images/file.gif'>".$currentAttachmentName."</a>";
        }
     ?>
     </div>
</div>

<div data-role="popup" id="popupDeleteComfirm" data-overlay-theme="b" data-theme="b" data-dismissible="false" style="max-width:400px;">
    <div data-role="header" data-theme="a">
    <h1><?=$Lang['Gamma']['App']['DeleteEmailHeader']?></h1>
    </div>
    <div role="main" class="ui-content">
        <h3 class="ui-title"><?=$Lang['Gamma']['App']['DeleteEmailTitle'] ?></h3>
        <a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-b" data-rel="back"><?=$Lang['Gamma']['App']['DeleteEmailCancel']?></a>
        <a href="#" id="comfirmDeleteEmails"  class="ui-btn  ui-corner-all ui-shadow ui-btn-inline ui-btn-b" ><?=$Lang['Gamma']['App']['DeleteEmailComfirm']?></a>
    </div>
</div>
   
<!-- /content -->
		
	<div data-role="footer">
	</div><!-- /footer -->
			
	<style type="text/css">
	 .ui-icon-myapp-reply{
     background-color: transparent !important;
   	 border: 0 !important;
   	 -webkit-box-shadow:none !important;
	 background: url(<?=$myReplyIconUrl?>) no-repeat !important;
    }
     .ui-icon-myapp-replyAll{
      background-color: transparent !important;
   	 border: 0 !important;
   	 -webkit-box-shadow:none !important;
	 background: url(<?=$myReplyAllIconUrl?>) no-repeat !important;
    }
     .ui-icon-myapp-forward{
     background-color: transparent !important;
   	 border: 0 !important;
   	 -webkit-box-shadow:none !important;
	 background: url(<?=$myForwardIconUrl?>) no-repeat !important;
    }
     .ui-icon-myapp-trash{
      background-color: transparent !important;
   	  border: 0 !important;
   	 -webkit-box-shadow:none !important;
	 background: url(<?=$myTrashIconUrl?>) no-repeat !important;
    }
    
    	 .ui-icon-myapp-myInbox{
     background-color: transparent !important;
   	 border: 0 !important;
   	 -webkit-box-shadow:none !important;
	 background: url(<?=$myInboxIconUrl?>) no-repeat !important;
    }
    
     .ui-icon-myapp-myOutbox{
     background-color: transparent !important;
   	 border: 0 !important;
   	 -webkit-box-shadow:none !important;
	 background: url(<?=$myOutboxIconUrl?>) no-repeat !important;
    }
    
	 .ui-icon-myapp-myStar{
     background-color: transparent !important;
   	 border: 0 !important;
   	 -webkit-box-shadow:none !important;
	 background: url(<?=$myFilledStarIconUrl?>) no-repeat !important;
    }
    
    .ui-li-divider a {
    text-decoration:  none;
    color: black !important;
}

.ui-li-text-my-h6{
    	color:black;
    	font-size: .75em !important;
    }
 .ui-li-text-my-title-font{
     	color:white;
    }
  .ui-li-text-my-h3{
      	color:black; !important;
    	font-size:1em!important;
    	font-weight:bold;
    }
      .ui-li-text-my-h1{
      	color:black;
    	font-size:1.30em!important;
    	font-weight:bold;
    }
    
    table.my-table{
    	width: 100%;
    }
    td.my-column{
    	width: 10%;
    }
       th.my-column{
    	width: 65%;
    }
    td.my-dateColumn{
    	width:25%
    }
    
    div.my-subjectWarpContent{
   margin:0 0 0 0 !important;
   width:100%;
   height:auto;
   word-wrap: break-word;
}
    div.my-messageWarpContent{
   margin:0 10px 0 10px;
   width:90%;
   height:auto;
   word-wrap: break-word;
}

table th { text-align:left; padding:6px;} 

    #loadingmsg {
      color: black;
      background:tranparent!important;
      padding: 10px;
      position: fixed;
      top: 50%;
      left: 50%;
      z-index: 100;
      margin-right: -25%;
      margin-bottom: -25%;
      }
      #loadingover {
      background: white;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }
		</style>
		<script>
		$(document).ready(function(){
		  
		  $("#search").click(function(){
		  $("#edit").hide();
		  });  			
		  <!--delete email-->
		  $("#comfirmDeleteEmails").click(function(){
          window.location.assign("<?=$current_email_delete_action_redirection?>");  
		  return false;
		  });  
		  <!--mark email with star function-->
		  $("#markEmail").click(function(){
           $markEmailStatus = $("#markEmail").attr('src');
            var Folder = "<?=$Folder?>";
           	var currentUID = "<?=$currentUID?>";
           	var uid ="<?=$uid?>";
      
           if($markEmailStatus =='/images/2009a/iMail/app_view/icon_star_filled_36px.png'){
           	$markUrl = '/images/2009a/iMail/app_view/icon_star_36px.png' ;
          $.post( "flagEmail.php",{Folder:Folder, currentUID:currentUID, Flag:0,uid:uid},function(data) {
             })
	  .done(function(data) {

	  	console.log(data);	
		  })
		  .fail(function() {
		  	console.log("error in response emailAddressListGenerator"); 
		  })
		  .always(function() {
		});

           }else{
           	$markUrl = '/images/2009a/iMail/app_view/icon_star_filled_36px.png' ;
          $.post( "flagEmail.php",{Folder:Folder, currentUID:currentUID, Flag:1,uid:uid},function(data) {
             })
	  .done(function(data) {
	  	console.log(data);	
		  })
		  .fail(function() {
		  	console.log("error in response emailAddressListGenerator"); 
		  })
		  .always(function() {
		});
           }
         	$("#markEmail").attr('src',$markUrl);});
		});
		<!--end delete email-->
		function getEmailListPage(){
		showLoading();
		window.location.assign("<?=$emailListUrl?>");
		}
		
		function goDownloadAttachment(parPartId) {
			
			var link = "<?=$attachmentLinkPrefix?>"+parPartId;
			window.location.assign(link);
		}
		
		function ComposeEmail(composeID){
			showLoading();
			var link = "email_reply_forward_page.php?MailAction="+composeID+"<?=$urlSuffix?>";
			window.location.assign(link);
		}
		
		function showLoading() {
		    document.getElementById('loadingmsg').style.display = 'block';
		    document.getElementById('loadingover').style.display = 'block';
		}	
		</script>
		</div><!-- /page -->
		<form id="form1" name="form1" method="post">
			<input type="hidden" id="MailAddress" name="MailAddress" value="<?=urlencode($CurUserAddress)?>" />
			<input type="hidden" id="MailPassword" name="MailPassword" value="<?=$CurUserPassword?>" />
			<input type="hidden" id="Folder" name="Folder" value="<?=urlencode($thisFolder)?>" />
			<input type="hidden" id="MessageID" name="MessageID" value="<?=$currentUID?>" />
			<input type="hidden" id="PartNumber" name="PartNumber" value="" />
		</form>
	</body>
</html>