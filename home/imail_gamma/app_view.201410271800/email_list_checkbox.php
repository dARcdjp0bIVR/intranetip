<?php
$PATH_WRT_ROOT = '../../../';

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libpwm.php");
$uid = $_GET['uid'];
$_SESSION['UserID'] = $uid;
//intranet_auth();
intranet_opendb();

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" /> 
		<title>email list</title>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
		<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
		<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
		<script language="javascript">
		</script>
		<style type="text/css">
		</style>
	</head>
	<body>
	<div data-role="page" >		

<?php
$libDb = new libdb(); 
$libeClassApp = new libeClassApp();
$libpwm= new libpwm();
$token = $_GET['token'];

$ul = $_GET['ul'];
$passwordAry = $libpwm->getData($uid);
foreach((array)$passwordAry as $_folderType => $_folderKey){
	if($_folderType==$uid)$password=$_folderKey;
}
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if (!$isTokenValid) {
	die("No access right to see this page.");
}

$folderAssoAry ="";
$defaultFolder = array();
$personalFolder = array();

$curUserId = $uid;
if ($curUserId == '') {

	$UserID = $uid;
}
    $sql = "Select ImapUserEmail From INTRANET_USER Where UserID = '".$curUserId."'";
	$dataAry = $libDb->returnResultSet($sql);
	$iMapEmail = $dataAry[0]['ImapUserEmail'];
	$iMapPwd = 'abc';	// hardcode for trial only
    $IMap = new imap_gamma(false,$iMapEmail, $iMapPwd);
    $thisFolder= $_GET["TargetFolderName"];
    $_displayName = $Lang['Gamma']['SystemFolderName'][$thisFolder];
    if($thisFolder!="INBOX"){
    	$Folder = "INBOX.".$thisFolder;
    }else{
    	$Folder = $thisFolder;
    }

$sort = "SORTARRIVAL";
$displayMode = (isset($displayMode) && $displayMode != "") ? $displayMode : "folder";
$Folder = (isset($Folder) && $Folder != "") ? stripslashes($Folder) : $IMap->InboxFolder;
$order 	 = (isset($order) && $order != "") ? $order : 1;
$CacheNewMail = false;

if (!$IMap->ExtMailFlag) {
	if($IMap->ConnectionStatus == true){
		$imap_reopen_success = imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));	
		if($Folder == $IMap->SpamFolder) {
			// Connect Junk box twice to let mail server auto correct read/unread status
			$imap_reopen_success = imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));
		   }
		   
		if(!$imap_reopen_success){
			sleep(1);
			imap_reopen($IMap->inbox, "{".$IMap->host.":".$IMap->mail_server_port."}".$IMap->encodeFolderName($Folder));
		}
		if($IMap->CacheMailToDB == true){
		}
		$imapInfo['imap_stream']   = $IMap->inbox;
		$imapInfo['server']    	   = $IMap->host;
		$imapInfo['password'] 	   = $IMap->CurUserPassword;
		$imapInfo['port']  	   	   = $IMap->mail_server_port;
		$imapInfo['username']  	   = $IMap->CurUserAddress;
		$imapInfo['displayMode']   = $displayMode;
		$imapInfo['sort'] 		   = $sort;
		$imapInfo['reverse'] 	   = $order;
		$imapInfo['messageSeq']    = $messageSeq;
		$imapInfo['messageNo']     = $messageNo;
		# Can be ignored
		$imapInfo['mailbox']   	   = $Folder;
		
		$isDraftInbox = ($Folder == $IMap->DraftFolder) ? 1 : 0;
		$isOutbox = ($Folder == $IMap->SentFolder) ? 1 : 0;
		$array = $IMap->Get_Sort_Index($IMap, $imapInfo, $search, $MailStatus);

		$imapInfo['pageInfo']['totalCount'] = (is_array($array)) ? count($array) : 0;
		$imapInfo['pageInfo']['pageStart']  = ($pageSize * ($imapInfo['pageInfo']['pageNo'] - 1) + 1);
		for ($i= 0 ; $i<$imapInfo['pageInfo']['totalCount']; $i++) {
		$MailHeaderList[] = imap_headerinfo($IMap->inbox, $array[$i], 80, 80);
		$msgno[] = $array[$i];
		}
        $emailAry = array ();
        $MailHeaderListcounter = count($MailHeaderList);
        
        
   		for($i= 0 ; $i<$MailHeaderListcounter; $i++){
		$contentAry = $MailHeaderList[$i];
        $subject = $IMap->MIME_Decode($contentAry->subject); 
	 	if ($subject != "") {
			$subjectPrint = (substr($subject, 0, 10) == "=?UTF-8?Q?") ? substr($subject, 10, -2) : $subject ;
		} else {
			$subjectPrint = "No subject" ;
		}
		$subjectPrint = $AttachmentIcon.htmlspecialchars($subjectPrint);
		$subjectPrint = $IMap->BadWordFilter($subjectPrint);
        $emailAry[$i]['Subject'] = $subjectPrint;
        if($IMap->IsToday(str_ireplace("UT","UTC",$contentAry->date)))
		$DateFormat = "H:i";
		else
		$DateFormat = "Y-m-d";
		$prettydate = date($DateFormat, strtotime(str_ireplace("UT","UTC",$contentAry->date)));
        $emailAry[$i]['Date'] = $prettydate;
        
        $fromArray = $contentAry-> from;
        $_stdClassObject = $fromArray[0];
        $mailbox = $_stdClassObject->mailbox;
        $emailAry[$i]['Mailbox'] = $mailbox;
        $currentMessageNo = $contentAry-> Msgno;
        $currentUID = $IMap->Get_UID ($currentMessageNo);
        $emailAry[$i]['UID'] = $currentUID;
		}
		$numOfEmail = count($emailAry);
			}}	
			$myTrashIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_trash_white_36px.png"."\"";

?>	
	 <!-- /header --> 	
	 <div data-role="header" style ="background-color:#3a960e; ">
     <p data-inline="true" style="text-align:left; margin-left:10px;"><?php echo"&nbsp&nbsp&nbsp&nbsp".$_displayName?></p>
     <div class="ui-btn-right" data-role="controlgroup" data-type="horizontal">
     <a href="#popupDeleteComfirm"  data-rel ="popup"  data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline ui-btn-icon-left ui-btn-a ui-nodisc-icon" data-icon="myapp-trash" style="top: 5px;padding-bottom: 16px;padding-top: 16px;padding-left: 12.500;padding-left: 12.500;"  data-transition="pop"></a>
     </div></div><!-- /header -->  
   
        <div data-role="popup" id="popupDeleteComfirm" data-overlay-theme="b" data-theme="b" data-dismissible="false" style="max-width:400px;">
    <div data-role="header" data-theme="a">
    <h1>Delete Emails</h1>
    </div>
    <div role="main" class="ui-content">
        <h3 class="ui-title">Are you sure you want to delete chosen emails?</h3>
        <a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-b" data-rel="back">Cancel</a>
        <a href="#" id="comfirmDeleteEmails"  class="ui-btn  ui-corner-all ui-shadow ui-btn-inline ui-btn-b" >Comfirm</a>
    </div>
</div>
         <form  name = "delete_email" id = "deleteEmailCheckBoxForm" method="post" action=<?="email_delete_action_redirection.php?TargetFolderName=".$thisFolder."&token=".$token."&uid=".$uid."&password=".$password."&ul=".$ul?>>		
          <fieldset>
            <ul data-role="listview" data-split-icon="info" data-split-theme="a" data-inset="true">
            <?php
            for($i=0;$i<$numOfEmail;$i++){
            	 $_currentEmail = $emailAry[$i];
            	  foreach((array)$_currentEmail as $_emailKey => $_emailCurrentDetail){
            	  	  if($_emailKey=='Subject'){
            	  	  	 $__currentSubjext = $_emailCurrentDetail;
            	  	  	 }else if($_emailKey=='Date'){
            	  	  	 	  $__currentDate = $_emailCurrentDetail;
            	  	  	 	   }else if($_emailKey=='Mailbox'){
            	  	  	 	   	   $__currentSender = $_emailCurrentDetail;}
            	  	  	 	   	   else if($_emailKey=='UID'){
            	  	  	 	   	   	$__currentUID = $_emailCurrentDetail;
            	  	  	 	   	   }
            	  	  }
            	  	  	 	   	   
            	echo"<li><a  class='custom'><input type='checkbox' name='chosenEmails[]' value = '".$__currentUID."' id='".$__currentUID."'/><label for='".$__currentUID."'> ";
            	echo $__currentSender.
            		"<p class = 'ui-li-text-my-h5'>".$__currentSubjext.
            		"</p><p class='ui-li-aside ui-li-text-my-h6' style='right:1em;'>".$__currentDate;
            echo "</p></label></a></li>";
            }
            ?>
 
            </ul>            
    
        </fieldset>
    </form>
	<!-- /content -->
	<div data-role="footer">
	</div><!-- /footer -->

<style type="text/css">
p {color:white;}
.ui-li-text-my-h6{
    color:black;
    font-size: .75em !important;
    }
     .ui-li-text-my-h5{
     color:black;
    font-size:.90em !important;
    }
  .ui-li-text-my-h3{
      color:black;
    font-size:1em !important;
    font-weight:bold !important;
    }
    

.custom {
    padding: 0;
}
.custom > div {
    margin: 0;
}
.custom > div > label {
    border-radius: 0;
    border: 0;
}

.ui-icon-delete{
 background-color: transparent !important;
 border: 0 !important;
 -webkit-box-shadow:none !important;
    }
    
    .ui-icon-myapp-trash {
     background-color: transparent !important;
   	 border: 0 !important;
   	 -webkit-box-shadow:none !important;
	 background: url(<?=$myTrashIconUrl?>) no-repeat !important;

	}
    
		</style>
		<script>
		$(document).ready(function(){			
		  $("#comfirmDeleteEmails").click(function(){
          $("#deleteEmailCheckBoxForm").submit();
		  return false;
		  });  
		});
		</script>

</div><!-- /page -->
	</body>
</html>