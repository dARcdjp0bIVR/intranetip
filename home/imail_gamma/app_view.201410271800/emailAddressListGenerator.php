<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/role_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/user_right_target.php");
include_once($PATH_WRT_ROOT."includes/json.php");


$_SESSION['UserID'] = $_REQUEST['userID'];
//intranet_auth();
intranet_opendb();

$layerID = $_REQUEST['LayerID'];
$CatID = $_REQUEST['CatID'];
$ChooseGroupID = array();
if($_REQUEST['ChooseGroupID']!=''){
	$ChooseGroupID[0]= $_REQUEST['ChooseGroupID'];
}
$ChooseUserID = $_REQUEST['ChooseUserID'];
$CurrentUID = $_SESSION['UserID'];

$addressArr = array();
$addressArr2 = array();
$addressArr3 = array();

$splitter = str_replace("*","\*",$imail_forwarded_email_splitter);
$li = new libuser($UserID);
$lrole = new role_manage();
$fcm = new form_class_manage();
$lgrouping = new libgrouping();
$lwebmail = new libwebmail();
$IMap = new imap_gamma(true);
$json = new JSON_obj();

$mail_domain = $lwebmail->mailaddr_domain;

//get the last item
if($layerID == 3)
{
##1st layer
unset($_SESSION['SSV_USER_TARGET']);
if(!isset($_SESSION['SSV_USER_TARGET'])){
$UserRightTarget = new user_right_target();
//$_SESSION['SSV_USER_ACCESS'] = $UserRightTarget->Load_User_Right($UserID);
$_SESSION['SSV_USER_TARGET'] = $UserRightTarget->Load_User_Target($_SESSION['UserID']);
}

$name_field = getNameFieldWithClassNumberByLang("a.");
$identity = $lrole->Get_Identity_Type($UserID);
$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

## get toStaff, toStudent, toParent options - IP25 only ##
$result_to_options['ToTeachingStaffOption'] = 0;
$result_to_options['ToNonTeachingStaffOption'] = 0;
$result_to_options['ToStudentOption'] = 0;
$result_to_options['ToParentOption'] = 0;
$result_to_options['ToAlumniOption'] = 0;

## Newly added by Ronald (20091118) - for �^�خѰ| [CRM Ref No.: 2009-1116-0915]
## Control by flag - $sys_custom['iMail_RecipientCategory_StaffAndTeacher']
$result_to_options['ToTeacherAndStaff'] = 0;
if($sys_custom['iMail_RecipientCategory_StaffAndTeacher'])
{
	if(($identity == "Teaching") || ($identity == "NonTeaching"))
	{
		$result_to_options['ToTeacherAndStaff'] = 1;
	}
}

if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Staff-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup']))
{
	if($sys_custom['iMail_RemoveTeacherCat']) {
		$result_to_options['ToTeachingStaffOption'] = 0;
	} else {
		$result_to_options['ToTeachingStaffOption'] = 1;
	}
}
if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']))
{
	if($sys_custom['iMail_RemoveNonTeachingCat']) {
		$result_to_options['ToNonTeachingStaffOption'] = 0;
	} else {
		$result_to_options['ToNonTeachingStaffOption'] = 1;
	}
}
if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Student-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Student-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup']))
{
	$result_to_options['ToStudentOption'] = 1;
}
if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Parent-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup']))
{
	$result_to_options['ToParentOption'] = 1;
}
if($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['All-Yes'] || $_SESION['SSV_USER_TARGET']['Alumni-All']) ){
	$result_to_options['ToAlumniOption'] = 1;
}

if(	($_SESSION['SSV_USER_TARGET']['All-Yes']) || 
	($_SESSION['SSV_USER_TARGET']['Staff-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['Student-MyGroup']) || 
	($_SESSION['SSV_USER_TARGET']['Parent-MyGroup']) || 
	($special_feature['alumni'] && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup']) || 
	($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || 
	($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || 
	($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Student-All']) || 
	($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Parent-All'])
)
{
	$result_to_options['ToGroupOption'] = 1;
	
	$result_to_group_options['ToTeacher'] = 0;
	$result_to_group_options['ToStaff'] = 0;
	$result_to_group_options['ToStudent'] = 0;
	$result_to_group_options['ToParent'] = 0;
	$result_to_group_options['ToAlumni'] = 0;
	
	if($_SESSION['SSV_USER_TARGET']['All-Yes']){
		$result_to_group_options['ToTeacher'] = 1;
		$result_to_group_options['ToStaff'] = 1;
		$result_to_group_options['ToStudent'] = 1;
		$result_to_group_options['ToParent'] = 1;
		if($special_feature['alumni']) $result_to_group_options['ToAlumni'] = 1;
	}else{
		if( ($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])){
			$result_to_group_options['ToTeacher'] = 1;
		}
		if( ($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])){
			$result_to_group_options['ToStaff'] = 1;
		}
		if( ($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup'])){
			$result_to_group_options['ToStudent'] = 1;
		}
		if( ($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])){
			$result_to_group_options['ToParent'] = 1;
		}
		if($special_feature['alumni'] && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup']){
			$result_to_group_options['ToAlumni'] = 1;
		}
	}
}

$AlumniNoTargetCond = true;
if($special_feature['alumni']) $AlumniNoTargetCond = $_SESSION['SSV_USER_TARGET']['Alumni-All'] == '' && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'] == '';
### If no mail targeting is set in the front-end, than will assign some default targeting to user ###
if( ($_SESSION['SSV_USER_TARGET']['All-Yes'] == '') && 
($_SESSION['SSV_USER_TARGET']['All-No'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MyForm'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MyClass'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MySubject'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-All'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MyForm'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MyClass'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MySubject'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Student-MyGroup'] == '') &&
($_SESSION['SSV_USER_TARGET']['Parent-All'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MyForm'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MyClass'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MySubject'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup'] == '') && 
($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'] == '') && 
$AlumniNoTargetCond 
)
{
	if($result_to_options['ToTeachingStaffOption'] == 0 && $result_to_options['ToNonTeachingStaffOption'] == 0 && $result_to_options['ToStudentOption'] == 0 && $result_to_options['ToParentOption'] == 0 && $result_to_options['ToAlumniOption'] == 0)
	{
		if(($identity == "Teaching") || ($identity == "NonTeaching"))
		{
			### If user is Teacher, then will have the follow targeting :
			###  - to All Teaching Staff
			###  - to All NonTeaching Staff
			###  - to All Student 
			###  - to All Parent 
			$result_to_options['ToTeachingStaffOption'] = 1;
			$result_to_options['ToNonTeachingStaffOption'] = 1;
			$result_to_options['ToStudentOption'] = 1;
			$result_to_options['ToParentOption'] = 1;
			if($special_feature['alumni']) $result_to_options['ToAlumniOption'] = 1;
			$_SESSION['SSV_USER_TARGET']['All-Yes'] = true;
		}
		if($identity == "Student")
		{
			### If user is Student, then will have the follow targeting :
			###  - to Student Own Class Student
			###  - to Student Own Subject Group Student
			$result_to_options['ToStudentOption'] = 1;
			$_SESSION['SSV_USER_TARGET']['Student-MyClass'] = true;	
			$_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'] = true;
		}
		if($identity == "Parent")
		{
			### If user is Parent, then will have the follow targeting :
			###  - to Their Child's Own Class Teacher
			###  - to Their Child's Own Subject Group Teacher
			$result_to_options['ToTeachingStaffOption'] = 1;
			$_SESSION['SSV_USER_TARGET']['Staff-MyClass'] = true;	
			$_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'] = true;
		}
	}
	$result_to_options['ToGroupOption'] = 1;
	
	$result_to_group_options['ToTeacher'] = 0;
	$result_to_group_options['ToStaff'] = 0;
	$result_to_group_options['ToStudent'] = 0;
	$result_to_group_options['ToParent'] = 0;
	$result_to_group_options['ToAlumni'] = 0;
	
	if($_SESSION['SSV_USER_TARGET']['All-Yes']){
		$result_to_group_options['ToTeacher'] = 1;
		$result_to_group_options['ToStaff'] = 1;
		$result_to_group_options['ToStudent'] = 1;
		$result_to_group_options['ToParent'] = 1;
		if($special_feature['alumni']) $result_to_group_options['ToAlumni'] = 1;
	}else{
		if(($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])){
			$result_to_group_options['ToTeacher'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])){
			$result_to_group_options['ToStaff'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup'])){
			$result_to_group_options['ToStudent'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])){
			$result_to_group_options['ToParent'] = 1;
		}
		if($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['Alumni-All'] || $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'])){
			$result_to_group_options['ToAlumni'] = 1;
		}
	}
}

if($identity != "Student")
{
	$sql = "SELECT COUNT(*) FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
	$result = $li->returnVector($sql);
	if($result[0]>0)
		$result_to_options['ToMyChildrenOption'] = 1;
}
if($identity == "Student")
{
	$sql = "SELECT COUNT(*) FROM INTRANET_PARENTRELATION WHERE StudentID = '$UserID'";
	$result = $li->returnVector($sql);
	if($result[0]>0)
		$result_to_options['ToMyParentOption'] = 1;
}

if(isset($sys_custom['iMail']['SelectRecipientType']) && is_array($sys_custom['iMail']['SelectRecipientType'])){
	$identity_group_display_order = $sys_custom['iMail']['SelectRecipientType'];
}else{
	$identity_group_display_order = array('identity','group');
}

# Create Cat list (Teacher, Staff, Student, Parent, Group) - IP25 Only #
## Newly added by Ronald (20091118) - for �^�خѰ| [CRM Ref No.: 2009-1116-0915]
## Control by flag - $sys_custom['iMail_RecipientCategory_StaffAndTeacher']
if ($result_to_options['ToTeacherAndStaff'])
{
	$addressArr[] = array($Lang['iMail']['FieldTitle']['TeacherAndStaff'],-3);
}
if ($result_to_options['ToTeachingStaffOption'])
{
	$addressArr[] = array($Lang['iMail']['FieldTitle']['Teacher'],-1);
}
if ($result_to_options['ToNonTeachingStaffOption'])
{
	$addressArr[] = array($Lang['iMail']['FieldTitle']['NonTeachingStaff'],-2);
}
if ($result_to_options['ToStudentOption'])
{
	$addressArr[] = array($Lang['iMail']['FieldTitle']['Student'],2);
}
if ($result_to_options['ToParentOption'])
{
	$addressArr[] = array($Lang['iMail']['FieldTitle']['Parent'],3);
}
if ($special_feature['alumni'] && $result_to_options['ToAlumniOption'])
{
	$addressArr[] = array($Lang['iMail']['FieldTitle']['Alumni'],-4);
}
if ($result_to_options['ToMyChildrenOption'])
{
	$addressArr[] = array($Lang['iMail']['FieldTitle']['MyChildren'],5);
}
if ($result_to_options['ToMyParentOption'])
{
	
	$addressArr[] = array($Lang['iMail']['FieldTitle']['MyParent'],6);
}
	
$arrExcludeGroupCatID[] = 0;
$targetGroupCatIDs = implode(",",$arrExcludeGroupCatID);

if($_SESSION['SSV_USER_TARGET']['All-Yes'])
{
	$sql = "SELECT DISTINCT GroupCategoryID, CategoryName FROM INTRANET_GROUP_CATEGORY WHERE GroupCategoryID NOT IN ($targetGroupCatIDs) ORDER BY CategoryName ASC";
}
else
{
	if($result_to_group_options['ToTeacher'] || $result_to_group_options['ToStaff'] || $result_to_group_options['ToStudent'] || $result_to_group_options['ToParent'] || ($special_feature['alumni'] && $result_to_group_options['ToAlumni']))
	{
		$sql = "SELECT DISTINCT a.GroupCategoryID, a.CategoryName FROM INTRANET_GROUP_CATEGORY AS a INNER JOIN INTRANET_GROUP AS b ON (a.GroupCategoryID = b.RecordType) INNER JOIN INTRANET_USERGROUP AS c ON (b.GroupID = c.GroupID) WHERE c.UserID = '$UserID' AND b.RecordType != 0 AND a.GroupCategoryID NOT IN ($targetGroupCatIDs) ORDER BY a.CategoryName ASC";
	}
}
$result = $li->returnArray($sql);

if(sizeof($result) > 0){
	for($i=0; $i<sizeof($result); $i++)
	{
		list($GroupCatID, $GroupCatName) = $result[$i];
		$GroupCatID = "GROUP_".$GroupCatID;
		$addressArr[] = array($GroupCatName,$GroupCatID);	
	}
}

## Others > Shared MailBox
if($_SESSION['SSV_USER_TARGET']['All-Yes']){
	
	$sql = "SELECT s.MailBoxID, s.MailBoxName, IF(p.DisplayName IS NULL OR p.DisplayName = '',s.MailBoxName,p.DisplayName) as DisplayName FROM MAIL_SHARED_MAILBOX as s LEFT JOIN MAIL_PREFERENCE as p ON p.MailBoxName = s.MailBoxName ORDER BY DisplayName ";
	$SharedMailBoxes = $li->returnArray($sql);
	if(sizeof($SharedMailBoxes)>0){
		$addressArr[] = array($Lang['SharedMailBox']['SharedMailBox'],'SHARED_MAILBOX');
	}
}

if($CatID != "")
{
	if($CatID=="SHARED_MAILBOX" || $CatID == "INTERNAL_GROUP")
	{
		## Others
		$GroupOpt = 3;
	}
	else if(strpos($CatID,"GROUP_") !== false)
	{
		## GROUP 
		$GroupOpt = 2;
		$ChooseGroupCatID = substr($CatID,6,strlen($CatID));
	}
	else
	{	
		## Identity
		$GroupOpt = 1;
	}
}

# 2nd Level Cat List - IP25 only #
if(($CatID != "") || ($CatID != 0) || ($CatID != 4)){
	if($CatID == -1){
		## All Teaching Staff ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])){
			$num_of_all_teacher = $lwebmail->returnNumOfAllTeachingStaff($identity);
			if($num_of_all_teacher > 0){
				$addressArr2[] = array(1,$Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff']);
				}
		} ##end All Teaching Staff ##
		## Form Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MyForm'])){
			
			$num_of_form_teacher = $lwebmail->returnNumOfFormTeacher($identity);
			if($num_of_form_teacher > 0)
			{
			$addressArr2[] = array(2,$Lang['iMail']['FieldTitle']['ToFormTeachingStaff']);
			}}##end All Teacher ##
			## Class Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MyClass'])){			
			$num_of_class_teacher = $lwebmail->returnNumOfClassTeacher($identity);
			if($num_of_class_teacher > 0)
			{
				$addressArr2[] = array(3,$Lang['iMail']['FieldTitle']['ToClassTeachingStaff']);
			}
		}## end Class Teacher ##
		## Subject Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MySubject'])){			
			$num_of_subject_teacher = $lwebmail->returnNumOfSubjectTeacher($identity);
			if($num_of_subject_teacher > 0)
			{
				$addressArr2[] = array(4,$Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff']);
			}					
		}## end Subject Teacher ##
	     ## Subject Group Teacher ##
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'])){			
			$num_of_subject_group_teacher = $lwebmail->returnNumOfSubjectGroupTeacher($identity);
			if($num_of_subject_group_teacher > 0)
			{
				$addressArr2[] = array(5,$Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff']);
			}
		} ## end Subject Group Teacher ##	
	}//end catID ==-1
	else if($CatID == 2){
		## All Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])){
			$num_of_all_student = $lwebmail->returnNumOfAllStudent($identity);
			if($num_of_all_student > 0)
			{
				$addressArr2[] = array(1,$Lang['iMail']['FieldTitle']['ToIndividualsStudent']);
			}
		}
		## Form Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MyForm'])){			
			$num_of_form_subject = $lwebmail->returnNumOfFormStudent($identity);
			if($num_of_form_subject > 0)
			{
				$addressArr2[] = array(2,$Lang['iMail']['FieldTitle']['ToFormStudent']);
			}
		}
		## Class Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MyClass'])){
			
			$num_of_class_student = $lwebmail->returnNumOfClassStudent($identity);
			if($num_of_class_student > 0)
			{
				$addressArr2[] = array(3,$Lang['iMail']['FieldTitle']['ToClassStudent']);
			}
		}
		## Subject Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MySubject'])){
			
			$num_of_subject_student = $lwebmail->returnNumOfSubjectStudent($identity);
			if($num_of_subject_student > 0)
			{
				$addressArr2[] = array(4,$Lang['iMail']['FieldTitle']['ToSubjectStudent']);
			}
		}
		## Subject Group Student
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'])){
			
			$num_of_subject_group_student = $lwebmail->returnNumOfSubjectGroupStudent($identity);
			if($num_of_subject_group_student > 0)
			{
				$addressArr2[] = array(5,$Lang['iMail']['FieldTitle']['ToSubjectGroupStudent']);
			}
		}	
	}//end catID ==2
	else if($CatID == 3){
		
			## All Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])){
			
			$num_of_all_parent = $lwebmail->returnNumOfAllParent($identity);
			if($num_of_all_parent > 0)
			{
				$addressArr2[] = array(1,$Lang['iMail']['FieldTitle']['ToIndividualsParents']);
			}
		}
		## Form Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MyForm'])){
			
			$num_of_form_parent = $lwebmail->returnNumOfFormParent($identity);
			if($num_of_form_parent > 0)
			{
				$addressArr2[] = array(2,$Lang['iMail']['FieldTitle']['ToFormParents']);
			}
		}
		## Class Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MyClass'])){
			
			$num_of_class_parent = $lwebmail->returnNumOfClassParent($identity);
			if($num_of_class_parent > 0)
			{
				$addressArr2[] = array(3,$Lang['iMail']['FieldTitle']['ToClassParents']);
			}
		}
		## Subject Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MySubject'])){
			
			$num_of_subject_parent = $lwebmail->returnNumOfSubjectParent($identity);
			if($num_of_subject_parent > 0)
			{
				$addressArr2[] = array(4,$Lang['iMail']['FieldTitle']['ToSubjectParents']);
			}
		}
		## Subject Group Parent
		if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup'])){
			
			$num_of_subject_group_parent = $lwebmail->returnNumOfSubjectGroupParent($identity);
			if($num_of_subject_group_parent > 0)
			{
				$addressArr2[] = array(5,$Lang['iMail']['FieldTitle']['ToSubjectGroupParents']);
			}
		}		
	}//end catID ==3
}	

if($CatID !='' && $GroupOpt == 2 /*&& $CatID == 4*/ && $ChooseGroupCatID!="")
{
	if($_SESSION['SSV_USER_TARGET']['All-Yes'])
	{
		$title_field = $lgrouping->getGroupTitleByLang();
		$sql = "SELECT GroupID, $title_field as Title FROM INTRANET_GROUP WHERE RecordType = '".$ChooseGroupCatID."' AND RecordType != 0 AND AcademicYearID = '$CurrentAcademicYearID' ORDER BY $title_field";
	}else{
		//$sql = "SELECT GroupID, Title FROM INTRANET_GROUP WHERE RecordType = ".$ChooseGroupCatID." AND RecordType != 0 AND AcademicYearID = $CurrentAcademicYearID ORDER BY Title";
		$title_field = $lgrouping->getGroupTitleByLang("a.");
		if($result_to_group_options['ToTeacher'] || $result_to_group_options['ToStaff'] || $result_to_group_options['ToStudent'] || $result_to_group_options['ToParent'] || ($special_feature['alumni'] && $result_to_group_options['ToAlumni']))
		{
			$sql = "SELECT a.GroupID, $title_field as Title FROM INTRANET_GROUP AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.GroupID = b.GroupID) WHERE a.RecordType = '".$ChooseGroupCatID."' AND a.RecordType != 0 AND a.AcademicYearID = '$CurrentAcademicYearID' AND b.UserID = '$UserID' ORDER BY $title_field";
		}
	}	
	
	$GroupArray = $li->returnArray($sql,2);
	$x2_5 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10'>";
	if(sizeof($GroupArray) > 0){
		$sql = "SELECT MAX(GroupID) FROM INTRANET_GROUP";
		$MaxGroupID = $li->returnVector($sql);
		// init js array to store group alias email
		$js_all_group_email .= "var js_group_email = new Array(".$MaxGroupID[0].");\n";
		
		for($i=0; $i<sizeof($GroupArray); $i++)
		{
			list($GroupID, $GroupName) = $GroupArray[$i];
			if(sizeof($ChooseGroupID)>0)
				$x2_5 .= "<option value='$GroupID' ".((in_array($GroupID,$ChooseGroupID))?"SELECTED":"").">".$GroupName."</option>";
			else
				$x2_5 .= "<option value='$GroupID' >".$GroupName."</option>";
			$addressArr2[] = array($GroupID,$GroupName);
			## Get All Related Email in selected Group (for group alias use) ##
			$targetEmail = $IMap->GetAllGroupEmail($GroupID,$result_to_group_options['ToTeacher'],$result_to_group_options['ToStaff'],$result_to_group_options['ToStudent'],$result_to_group_options['ToParent'],$result_to_group_options['ToAlumni']);
			$js_all_group_email .= "js_group_email[".$GroupID."] = '".str_replace("'","\'",htmlspecialchars_decode($targetEmail,ENT_QUOTES))."';\n";
		}
	}else{
		$x2_5 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
	}
	$x2_5 .= "</select>";
	$ShowSubGroupSelection = true;	
	
}//end $GroupOpt == 2
## end 2nd level

## 3rd level

if($plugin['imail_gamma']){
$email_field = "IMapUserEmail";
$email_field_a = "ImapUserEmail";
$email_field_all_user = "IMapUserEmail";
$email_field_a_gamma = "a.ImapUserEmail";
$email_field_b="ImapUserEmail";
$email_field_c = "c.ImapUserEmail";
$gamma_filter = " AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ";
$gamma_filter_a = " AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail<> '') ";
$gamma_filter_c = "AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '')";
}else{
$email_field = " CONCAT(UserLogin,'@$mail_domain') ";
$email_field_a = " CONCAT(a.UserLogin,'@$mail_domain') ";
$email_field_all_user = " CONCAT(all_user.UserLogin,'@$mail_domain') ";
$email_field_b="CONCAT(b.UserLogin,'@$mail_domain')";
$email_field_c="CONCAT(c.UserLogin,'@$mail_domain')";
$email_field_a_gamma = "CONCAT(a.UserLogin,'@$mail_domain')";
$gamma_filter = "";
$gamma_filter_a = "";
$gamma_filter_c = "";
}

## 3rd level
if(($CatID != "") || ($CatID != 0) || ($CatID != 4)){

	if($CatID == -2){
		## Non-teaching Staff ##
		$NameField = getNameFieldByLang();
        $sql = "Select UserID, ".$NameField." as Name, $email_field From INTRANET_USER where RecordStatus = 1 and RecordType = 1 and (Teaching = 0 OR Teaching IS NULL) $gamma_filter ORDER BY EnglishName";
		$result = $li->returnArray($sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				
				list($u_id, $u_name, $u_email) = $result[$i];				
				if ($plugin['imail_gamma']) {
				if(count($SelectedRecipientsEmails)>0 && in_array($u_email,$SelectedRecipientsEmails)){
					continue;
				}	
				$final_email = $IMap->FormatRecipientOption('"'.$u_name.'"'." <".$u_email."> ");
				}else{
				$final_email = $u_name." <".$u_email."> ";
				}
				$addressArr3[] = array($u_id,$final_email);
				
			}
		}
	}## end catid==-2##
	## Newly added by Ronald (20091118) - for �^�خѰ| [CRM Ref No.: 2009-1116-0915]
	## Control by flag - $sys_custom['iMail_RecipientCategory_StaffAndTeacher']
	if($CatID == -3){
		## Teachers / Staff ##
		$NameField = getNameFieldByLang();
		$sql = "Select UserID, ".$NameField." as Name, $email_field From INTRANET_USER WHERE RecordStatus = '1' AND RecordType = '1' $gamma_filter ORDER BY EnglishName";
		$result = $fcm->returnArray($sql,3);
		
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				if ($plugin['imail_gamma']) {
				if(count($SelectedRecipientsEmails)>0 && in_array($u_email,$SelectedRecipientsEmails)){
					continue;
				}
				$final_email = $IMap->FormatRecipientOption('"'.$u_name.'"'." <".$u_email."> ");					
				}else{
				$final_email = $u_name." <".$u_email."> ";
				}
				$addressArr3[] = array($u_id,$final_email);
			}
		}
	}## end catid==-3##
	
	if($special_feature['alumni'] && $CatID == -4){
		## All Alumni ##
		$NameField = getNameFieldByLang();
		$sql = "Select UserID, ".$NameField." as Name, $email_field From INTRANET_USER where RecordStatus = 1 and RecordType = 4 $gamma_filter ORDER BY EnglishName";
		$result = $li->returnArray($sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				if ($plugin['imail_gamma']) {
				if(count($SelectedRecipientsEmails)>0 && in_array($u_email,$SelectedRecipientsEmails)){
					continue;
				}
				$final_email = $IMap->FormatRecipientOption('"'.$u_name.'"'." <".$u_email."> ");
				}else{
				$final_email = $u_name." <".$u_email."> ";
				}
				$addressArr3[] = array($u_id,$final_email);
			}
		}
	}## end All Alumni##
	
	if($CatID == 5){
		$sql = "SELECT b.UserID, ".getNameFieldByLang2("b.").", IMapUserEmail FROM INTRANET_PARENTRELATION AS a INNER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE a.ParentID = '$UserID' AND b.RecordStatus = 1 AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ";
		$result = $li->returnArray($sql,3);
		
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				if ($plugin['imail_gamma']) {
				if(count($SelectedRecipientsEmails)>0 && in_array($u_email,$SelectedRecipientsEmails)){
					continue;
				}
				$final_email = $IMap->FormatRecipientOption('"'.$u_name.'"'." <".$u_email."> ");
				}else{
				$final_email = $u_name." <".$u_email."> ";
				}
				$addressArr3[] = array($u_id,$final_email);
			}
		}
	}## end $CatID == 5##
	if($CatID == 6){
		$sql = "SELECT b.UserID, ".getNameFieldByLang2("b.").", IMapUserEmail FROM INTRANET_PARENTRELATION AS a INNER JOIN INTRANET_USER AS b ON (a.ParentID = b.UserID) WHERE a.StudentID = '$UserID' AND b.RecordStatus = 1  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ";
		$result = $li->returnArray($sql,3);
		
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				if ($plugin['imail_gamma']) {
				if(count($SelectedRecipientsEmails)>0 && in_array($u_email,$SelectedRecipientsEmails)){
					continue;
				}
				$final_email = $IMap->FormatRecipientOption('"'.$u_name.'"'." <".$u_email."> ");
				}else{
				$final_email = $u_name." <".$u_email."> ";
				}
				$addressArr3[] = array($u_id,$final_email);
			}
		}
	}
	
if ($plugin['imail_gamma']) {	
	//only imail gamma has the shared mailbox
	if($CatID == "SHARED_MAILBOX"){
		if(sizeof($SharedMailBoxes)>0){
			for($i=0;$i<sizeof($SharedMailBoxes);$i++){
				list($mail_box_id,$mail_box_name,$display_name) = $SharedMailBoxes[$i];
				if(count($SelectedRecipientsEmails)>0 && in_array($mail_box_name,$SelectedRecipientsEmails)){
					continue;
				}
				$final_email = $IMap->FormatRecipientOption('"'.$display_name.'"'." <".$mail_box_name."> ");
				$addressArr3[]  = array($mail_box_id,$final_email);
			}
		}
	}
}
}### end imail gamma's shared box layer 3 ###

### Please Follow the follow logic ###
if($CatID != "" && sizeof($ChooseGroupID)>0)
{
	$sql = "";
	if($CatID == -1) // teaching staff
	{
		for($i=0; $i<sizeof($ChooseGroupID); $i++)
		{			
			if($ChooseGroupID[$i] == 1)
			{
				## All Teaching Staff ##
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent") || ($special_feature['alumni'] && $identity == "Alumni"))
				{
					$all_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang("all_user.").", $email_field_all_user FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1  $gamma_filter  ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
					$sql = $all_sql;
				}
			}
			if($ChooseGroupID[$i] == 2)
			{
				## Form Teacher ##
				if($identity == "Teaching")
				{
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", $email_field_a FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID')  $gamma_filter ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to form teacher
				}
				if($identity == "Student")
				{
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", $email_field_a FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND  b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID')  $gamma_filter ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", $email_field_a FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID')  $gamma_filter ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if($ChooseGroupID[$i] == 3)
			{		
				## Class Teacher ##
				if($identity == "Teaching")
				{
					$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", $email_field_a FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID') $gamma_filter ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to class teacher
				}
				if($identity == "Student")
				{
					$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", $email_field_a FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID')  $gamma_filter ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", $email_field_a FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID')  $gamma_filter ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
			}
			if($ChooseGroupID[$i] == 4)
			{
				## Subject Teacher ##
				if($identity == "Teaching")
				{					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", $email_field_a FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')  $gamma_filter ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to subject teacher
				}
				if($identity == "Student")
				{					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", $email_field_a FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')  $gamma_filter ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{					
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", $email_field_a FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = $UserID AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')  $gamma_filter ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if($ChooseGroupID[$i] == 5)		/* start from here */
			{
				## Subject Group Teacher ##
				if($identity == "Teaching")
				{
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", $email_field_a FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID')  $gamma_filter ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## Non teaching suppose cannnot send to subject group teacher
				}
				if($identity == "Student")
				{
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", $email_field_a FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID')  $gamma_filter ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", $email_field_a FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID')  $gamma_filter ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}
		}
		$result = $li->returnArray($sql);
	}
	if($CatID == 2) // student
	{
		for($i=0; $i<sizeof($ChooseGroupID); $i++)
		{
			if($ChooseGroupID[$i] == 1)
			{
				## All Student ##
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent") || ($special_feature['alumni'] && $identity == "Alumni"))
				{
					$all_sql = "(SELECT DISTINCT UserID, ".getNameFieldWithClassNumberByLang().", $email_field  FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1  $gamma_filter ORDER BY IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName)";
					$sql = $all_sql;
				}
			}
			if($ChooseGroupID[$i] == 2)
			{
				## My Form Student ##
				if($identity == "Teaching")
				{
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", $email_field_b FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  $gamma_filter ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to form student
				}
				if($identity == "Student")
				{
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", $email_field_b FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) $gamma_filter  ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", $email_field_b FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) $gamma_filter  ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if($ChooseGroupID[$i] == 3)
			{
				## My Class Student ##
				if($identity == "Teaching")
				{
					$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.").", $email_field_b FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  $gamma_filter ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to class student
				}
				if($identity == "Student")
				{
					$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.").", $email_field_b FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  $gamma_filter ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.").", $email_field_b FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  $gamma_filter ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				
			}
			if($ChooseGroupID[$i] == 4)
			{
				## My Subject Student ##
				if($identity == "Teaching")
				{
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", $email_field_b FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  $gamma_filter ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to Subject student
				}
				if($identity == "Student")
				{
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", $email_field_b FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  $gamma_filter ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{
					$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", $email_field_b FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  $gamma_filter ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if($ChooseGroupID[$i] == 5)
			{		
				## My Subject Group Student ##		
				if($identity == "Teaching")
				{	
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", $email_field_b FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  $gamma_filter ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## support staff suppose cannot send to subject group student
				}
				if($identity == "Student")
				{	
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", $email_field_b FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  $gamma_filter ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{	
					$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", $email_field_b FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  $gamma_filter ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}
		}
		$result = $li->returnArray($sql);
	}
	if($CatID == 3) // parent
	{
		for($i=0; $i<sizeof($ChooseGroupID); $i++)
		{
			if($ChooseGroupID[$i] == 1)
			{
				## All Parents ##
				if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent") || ($special_feature['alumni'] && $identity == "Alumni"))
				{	
					$NameField = "IF(c.EnglishName != '' AND c.EnglishName IS NOT NULL,
									  IF(c.ClassName != '' AND c.ClassName IS NOT NULL and c.ClassNumber != '' AND c.ClassNumber IS NOT NULL,
											CONCAT('(',c.ClassName,'-',c.ClassNumber,') ', ".getNameFieldByLang2('c.').",'".$Lang['iMail']['FieldTitle']['TargetParent']."', IF(a.EnglishName !='' AND a.EnglishName IS NOT NULL,CONCAT(' (',".getNameFieldByLang2('a.').",')'),'')),
											".getNameFieldByLang2('a.')."),
									  IF(TRIM(a.EnglishName)='' AND TRIM(a.ChineseName)='',a.UserLogin,".getNameFieldByLang2('a.').")
									)";
					$SortField = getParentNameWithStudentInfo2("c.","a.","",0,"0",5);
					if ($sys_custom['iMailSendToParent'] == true)
					{
						$all_sql = $IMap->GetSQL_ParentEmail4All($NameField,$SortField);
					}
					else
					{					
						$all_sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, $email_field_a_gamma as UserEmail, $SortField as SortField  FROM INTRANET_USER AS a LEFT OUTER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) LEFT OUTER JOIN INTRANET_USER AS c ON (b.StudentID = c.UserID) WHERE a.RecordType = 3 AND a.RecordStatus = 1 $gamma_filter_a ";
					}
					$sql = $all_sql;
				}
			}
			if($ChooseGroupID[$i] == 2)
			{
				
				## My Form Parents ##
				if($identity == "Teaching")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					if (($sys_custom['iMailSendToParent'] == true)&&$plugin['imail_gamma'])
					{
						$form_sql = $IMap->GetSQL_ParentEmail4FormClass($name_field,$sort_field,$UserID,$CurrentAcademicYearID,$identity,$level="form");
					}
					else
					{					
						$form_sql = " SELECT DISTINCT c.UserID as UserID, $name_field as UserName, $email_field_c as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID'))  $gamma_filter_c ";
					}
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to form parent
				}
				if($identity == "Student")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					$form_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, $email_field_c as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID'))  $gamma_filter_c ";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
				if($identity == "Parent")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
						$form_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, $email_field_c as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID'))  $gamma_filter_c ";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$form_sql;
				}
			}
			if($ChooseGroupID[$i] == 3)
			{
				## My Class Parents ##
				if($identity == "Teaching")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					if (($sys_custom['iMailSendToParent'] == true)&&$plugin['imail_gamma'])
					{
						$class_sql = $IMap->GetSQL_ParentEmail4FormClass($name_field,$sort_field,$UserID,$CurrentAcademicYearID,$identity,$level="class");
					}
					else
					{					
						$class_sql = " SELECT DISTINCT c.UserID as UserID, $name_field as UserName, $email_field_c as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = '$CurrentAcademicYearID')) $gamma_filter_c ";
					}
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to class parent
				}
				if($identity == "Student")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
						$class_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, $email_field_c as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID')) $gamma_filter_c ";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
				if($identity == "Parent")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
						$class_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, $email_field_c as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'))  $gamma_filter_c ";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$class_sql;
				}
			}
			if($ChooseGroupID[$i] == 4)
			{
				## My Subject Parents ##
				if($identity == "Teaching")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);

					if ($sys_custom['iMailSendToParent'] == true)
					{
						$subject_sql = $IMap->GetSQL_ParentEmail4SubjectGroup($name_field,$sort_field,$UserID,$CurrentTermID,$level="subject");
					}
					else
					{					
						$subject_sql = " SELECT DISTINCT c.UserID as UserID, $name_field as UserName, $email_field_c as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'))  $gamma_filter_c ";
					}
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to subject parent
				}
				if($identity == "Student")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					$subject_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, $email_field_c as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'))  $gamma_filter_c ";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
				if($identity == "Parent")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					$subject_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, $email_field_c as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'))  $gamma_filter_c ";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_sql;
				}
			}
			if($ChooseGroupID[$i] == 5)
			{
				## My Subject Group Parents ##
				if($identity == "Teaching")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);

					if ($sys_custom['iMailSendToParent'] == true)
					{
						$subject_group_sql = $IMap->GetSQL_ParentEmail4SubjectGroup($name_field,$sort_field,$UserID,$CurrentTermID,$level="subject_group");
					}
					else
					{					
						$subject_group_sql = " SELECT DISTINCT c.UserID as UserID, $name_field as UserName, $email_field_c as UserEmail,$sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'))  $gamma_filter_c ";
					}
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "NonTeaching")
				{
					## Support Staff suppose cannot send to subject group parent
				}
				if($identity == "Student")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					$subject_group_sql = " SELECT DISTINCT b.ParentID as UserID, $name_field as UserName, $email_field_c as UserEmail, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID')) $gamma_filter_c ";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
				if($identity == "Parent")
				{
					$name_field = getParentNameWithStudentInfo("a.","c.");
					$sort_field = getParentNameWithStudentInfo2("a.","c.","",0,"0",5);
					$subject_group_sql = " SELECT DISTINCT b.ParentID, $name_field as UserName, $email_field_c, $sort_field as SortField FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID'))  $gamma_filter_c ";
					if($sql != "")
						$delimiter = " UNION ";
					
					$sql .= $delimiter.$subject_group_sql;
				}
			}			
		}
		if($sql != "") $sql.=" ORDER BY SortField ";
		$result = $li->returnArray($sql);
	}
	if(/*$CatID == 4*/ $GroupOpt == 2) // Group
	{
		if(sizeof($ChooseGroupID)>0)
		{			
			$TargetGroupID = implode(",",$ChooseGroupID);
			
			$cond = "";
			if($result_to_group_options['ToTeacher'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 1 AND (a.Teaching = 1)) ";
			}
			if($result_to_group_options['ToStaff'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 1 AND (TEACHING = 0 OR a.Teaching IS NULL)) ";
			}
			if($result_to_group_options['ToStudent'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 2) ";
			}
			if($result_to_group_options['ToParent'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 3) ";
			}
			if($special_feature['alumni'] && $result_to_group_options['ToAlumni'] == 1){
				if($cond != "")
					$cond .= " OR ";
				$cond .= " (a.RecordType = 4) ";
			}
			
			if($cond != "")
				$final_cond = " AND ( $cond ) ";
			
			$sql = "SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", $gamma_filter_a FROM INTRANET_USER AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.GroupID IN ($TargetGroupID)  $gamma_filter $final_cond ORDER BY a.RecordType, IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName";
			$result = $li->returnArray($sql,2);
		}
	}
	if($CatID !="INTERNAL_GROUP"){
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($u_id, $u_name, $u_email) = $result[$i];
				if ($plugin['imail_gamma']) {
				if(count($SelectedRecipientsEmails)>0 && in_array($u_email,$SelectedRecipientsEmails)){
					continue;
				}
				$final_email = $IMap->FormatRecipientOption('"'.$u_name.'"'." <".$u_email."> ");
				}else{
				$final_email = $u_name." <".$u_email."> ";
				}
				$addressArr3[] = array($u_id,$final_email);
			}
		}				
	}//end not INTERNAL_GROUP $addressArr3	
}	
}// end layerID ==3

else if($layerID == 2){
	//directly db retreive data
	if($CatID == "INTERNAL_GROUP"){
	//get layer2 data	
	 $sql_int_group = "SELECT 
								AliasID, AliasName 
							FROM 
								INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL 
							WHERE 
								OwnerID = '$UserID' 
							ORDER BY 
								AliasName ";
		$InternalRecipientGroups =  $li->returnArray($sql_int_group);
			   if(count($InternalRecipientGroups)!=0){
			for($i=0;$i<sizeof($InternalRecipientGroups);$i++){
				$addressArr2[] = array($InternalRecipientGroups[$i]['AliasID'],$InternalRecipientGroups[$i]['AliasName']);
			}
		}
		
		//layer3 data
		$namefield = getNameFieldWithClassNumberByLang("u.");
		$sql = "SELECT 
    				DISTINCT ga.AliasID,ge.TargetID,$namefield as UserName,u.IMapUserEmail 
    			FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL as ga 
				INNER JOIN INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as ge ON ga.AliasID=ge.AliasID AND ga.OwnerID='$UserID'
				INNER JOIN INTRANET_USERGROUP as ug ON ge.TargetID=ug.GroupID 
				INNER JOIN INTRANET_USER as u ON u.UserID=ug.UserID 
				WHERE ga.OwnerID='$UserID' AND ge.RecordType = 'G' AND (u.IMapUserEmail IS NOT NULL AND u.IMapUserEmail<> '') 
				ORDER BY u.EnglishName";
	    $internal_group_groups = $li->returnArray($sql);    	        
	   
    	$sql = "SELECT 
    				DISTINCT ga.AliasID,ge.TargetID,$namefield as UserName,u.IMapUserEmail 
    			FROM INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL as ga  
				INNER JOIN INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as ge ON ga.AliasID=ge.AliasID AND ga.OwnerID='$UserID'
				INNER JOIN INTRANET_USER as u ON ge.TargetID = u.UserID
				WHERE ga.OwnerID='$UserID' AND ge.RecordType = 'U' AND (u.IMapUserEmail IS NOT NULL AND u.IMapUserEmail<> '')
				ORDER BY u.EnglishName ";
	    $internal_group_users = $li->returnArray($sql);
		
//		$js_internal_groups  = 'var js_internal_group_alias={};'."\n"; // store group AliasID to its member users array 
//		$js_internal_groups .= 'var js_internal_group_target_group = {};'."\n"; // store group type TargetID to users array
//		$js_internal_groups .= 'var js_internal_group_target_user = {};'."\n"; // store user type TargetID to user		
		
//		for($i=0;$i<sizeof($internal_group_groups);$i++){
//			list($t_alias_id, $t_target_id, $t_name, $t_email) = $internal_group_groups[$i];
//			$js_internal_groups .= 'if(!js_internal_group_alias['.$t_alias_id.']){'."\n";
//			$js_internal_groups .= '  js_internal_group_alias['.$t_alias_id.']=[];'."\n";
//			$js_internal_groups .= '}'."\n";
//			$js_internal_groups .= 'js_internal_group_alias['.$t_alias_id.'].push(\''.'"'.str_replace("'","\'",$t_name).'"<'.$t_email.'>'.'\');'."\n";
//			
//			$js_internal_groups .= 'if(!js_internal_group_target_group['.$t_target_id.']){'."\n";
//			$js_internal_groups .= '  js_internal_group_target_group['.$t_target_id.']=[];'."\n";
//			$js_internal_groups .= '}'."\n";
//			$js_internal_groups .= 'js_internal_group_target_group['.$t_target_id.'].push(\''.'"'.str_replace("'","\'",$t_name).'"<'.$t_email.'>'.'\');'."\n";
//		}
		
//		for($i=0;$i<sizeof($internal_group_users);$i++){
//			list($t_alias_id, $t_target_id, $t_name, $t_email) = $internal_group_users[$i];
//			$js_internal_groups .= 'if(!js_internal_group_alias['.$t_alias_id.']){'."\n";
//			$js_internal_groups .= '  js_internal_group_alias['.$t_alias_id.']=[];'."\n";
//			$js_internal_groups .= '}'."\n";
//			$js_internal_groups .= 'js_internal_group_alias['.$t_alias_id.'].push(\''.'"'.str_replace("'","\'",$t_name).'"<'.$t_email.'>'.'\');'."\n";
//			
//			$js_internal_groups .= 'js_internal_group_target_user['.$t_target_id.']=\''.'"'.str_replace("'","\'",$t_name).'"<'.$t_email.'>'.'\';'."\n";
//		}
		
         //get layer3 data
		$AliasImplode = "'".implode("','",$ChooseGroupID)."'";		
		$sql = "SELECT 
    				DISTINCT  CONCAT('G',a.TargetID),b.Title,a.TargetID
    			FROM 
    				INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as a
				INNER JOIN 
					INTRANET_GROUP as b 
				ON 
					a.TargetID = b.GroupID
				WHERE 
					a.AliasID IN ($AliasImplode) AND a.RecordType = 'G'
				ORDER BY 
					b.Title ";
	    $internal_group_groups = $li->returnArray($sql,3);            
		
	    $namefield = getNameFieldWithClassNumberByLang("b.");
	    $sql = "	SELECT 
	    				DISTINCT $namefield , b.ImapUserEmail ,TargetID
	    			FROM 
	    				INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as a
					INNER JOIN 
						INTRANET_USER as b 
					ON 
						a.TargetID = b.UserID
					WHERE 
						a.AliasID IN ($AliasImplode) AND a.RecordType = 'U' AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail<> '')
					ORDER BY 
						b.EnglishName ";	
	    $internal_group_users = $li->returnArray($sql,3);
		
		for($i=0;$i<sizeof($internal_group_groups);$i++){
			if ($plugin['imail_gamma']) {
			$___finalEmail =$IMap->FormatRecipientOption('"'.$internal_group_users[$i][0].'" <'.$internal_group_users[$i][1].'> ');			
			}else{
			$___finalEmail = '"'.$internal_group_groups[$i][0].'"'." <".$internal_group_groups[$i][1]."> ";	
			}
			$addressArr3[] = array($internal_group_groups[$i][2],$___finalEmail);
		}
		for($i=0;$i<sizeof($internal_group_users);$i++){
		  	if ($plugin['imail_gamma']) {
			$___finalEmail =$IMap->FormatRecipientOption('"'.$internal_group_users[$i][0].'" <'.$internal_group_users[$i][1].'> ');			
			}else{
			$___finalEmail = '"'.$internal_group_groups[$i][0].'"'." <".$internal_group_groups[$i][1]."> ";	
			}
			$addressArr3[] = array($internal_group_users[$i][2],$___finalEmail);
			}		
	}
	
		
} // end layerID ==2
else{
	//cause get external info directly from db, no need to distinguish imail gamma and imail
	// layerID 0 and 1

	$li = new libuser();
	$json = new JSON_obj();
	if($layerID == 0 && $CatID == 'ExternalGroup'){
		
		$sql = "SELECT AliasID, AliasName, NumberOfEntry FROM INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL WHERE OwnerID = '$CurrentUID' ORDER BY AliasName";
		$alias_array2 = $li->returnArray($sql,2);
		
		for ($i=0; $i<count($alias_array2); $i++){
			list ($id, $name, $id_num) = $alias_array2[$i];
			$addressArr2[] = array($id, $name);
			
		}
		
		if($ChooseGroupID[0] != ''){
			$addressSql = "
	    					SELECT 
	    						B.TargetName, CONCAT(B.TargetName,' &lt;',B.TargetAddress,'&gt;') 
	    					FROM 
	    						INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY AS a,
	    						INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL AS B
	    					WHERE 
	    						a.AliasID='".$ChooseGroupID[0]."' AND a.TargetID = B.AddressID
	    					ORDER BY 
								B.TargetName ";
			$arr = $li->returnArray($addressSql,2);  
			for($i=0;$i<count($arr);$i++){
				$addressArr3[] = array($arr[$i][1],$arr[$i][1]);
			}			
		}
		
	}else if($layerID==1 && $CatID == 'ExternalContact'){
	
		$addressArr2 = array();
		$addressSql = "SELECT 
						TargetName, TargetAddress
					FROM 
						INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
					WHERE 
						OwnerID = '$CurrentUID' 
                	ORDER BY
                		TargetAddress ASC";
		$arr = $li->returnArray($addressSql,2);  

		for($i=0;$i<count($arr);$i++){
			$final_arr3_email_format = $arr[$i][0]."<".$arr[$i][1].">";
			$addressArr3[] = array($final_arr3_email_format,$final_arr3_email_format);
		}
	}
	
}

$addressArrReturn =  array();
// 2nd layer
if($CatID != '' && $ChooseGroupID[0] == ''){
	$finalArr = $addressArr2;
	if(($CatID == -4 ||$CatID == -2 || $CatID=='ExternalGroup' || $CatID=='ExternalContact') && count($addressArr3)>0){
		$finalArr = array_merge($addressArr2,$addressArr3);
	}
	for($i=0; $i<sizeof($finalArr); $i++){
	$groupData = array('groupType' => $finalArr[$i][0], 'groupName' => $finalArr[$i][1]);
	$addressArrReturn[$i]  = $groupData ;
		}
}else if($CatID != '' && $ChooseGroupID[0] != ''){
	// 3rd layer
	$finalArr = $addressArr3;
	for($i=0; $i<sizeof($finalArr); $i++){
	$groupData = array('groupName' => $finalArr[$i][0], 'groupType' => $finalArr[$i][1]);
	$addressArrReturn[$i]  = $groupData ;
}
}else{
	$finalArr = $addressArr;
	for($i=0; $i<sizeof($finalArr); $i++){
		$groupData = array('groupName' => $finalArr[$i][0], 'groupType' => $finalArr[$i][1]);
		$addressArrReturn[$i]  = $groupData ;
	}	
}

$addressArrReturnJson = "{\"layerID\": \"".$layerID."\",\"CatID\":\"".$CatID."\",\"ChooseGroupID\":\"".(is_array($ChooseGroupID)? $ChooseGroupID[0] : $ChooseGroupID )."\",\"ChooseUserID\":\"".$ChooseUserID."\",   \"addressArr\":".$json->encode($addressArrReturn)."}";
echo $addressArrReturnJson;
?>