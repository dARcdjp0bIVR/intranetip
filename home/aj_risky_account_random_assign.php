<?php
# using: 

#################################
#	Date:	2015-07-15	Bill	[2015-0714-1051-41207]
#			remove limit when system in debug mode
#
#	Date:	2013-12-09	Ivan
#			fixed: php error due to defined $result as integer and then array. Standardize $result to array now.
#
#	Date:	2013-06-11	Yuen
#			inform user(s) about password reset by email
#
#	Date:	2013-03-07	Yuen
#			first version
#################################


	@SET_TIME_LIMIT(600);

	$PATH_WRT_ROOT = "../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libwebmail.php");
	include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
	include_once($PATH_WRT_ROOT."includes/libftp.php");
	include_once($PATH_WRT_ROOT."includes/libemail.php");
	include_once($PATH_WRT_ROOT."includes/libsendmail.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");

	# iMail Plus enabled, assigned user only
	# to be changed
	if (!$_SESSION['SSV_PRIVILEGE']['schoolsettings']["isAdmin"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libauth.php");
	
	intranet_auth();
	intranet_opendb();
	
	# Create a new interface instance
	$linterface = new interface_html("popup_no_layout.html");
	$li = new libauth();
	
     $luser = new libuser();
     $lu = new libsendmail();
     
	# Webmail
    $lwebmail = new libwebmail();
	$IMap = new imap_gamma();
    $lftp = new libftp();
    
    $result = array();
         
	$rows = $li->getProtentialRiskByPassword();
	// [2015-0714-1051-41207] remove limit when system in debug mode
//	$data_size = ($DebugMode) ? 2 : sizeof($rows);
	$data_size = sizeof($rows);
	for ($i=0; $i<$data_size; $i++)
	{
		$rowObj = $rows[$i];
		
		$newPassword = intranet_random_passwd(8);
		$newPasswordHashed = $li->getHashedPassword($rowObj["UserLogin"], $newPassword);
		
		
		
		$sql = "UPDATE INTRANET_USER SET HashedPass='$newPasswordHashed' WHERE UserID = ".$rowObj["UserID"];
		//debug($rowObj["UserLogin"]. " :: ".$newPassword. " -- ".$sql);
		$li->db_db_query($sql);
		$li->UpdateEncryptedPassword($rowObj["UserID"], $newPassword);
		
		
        if($lwebmail->has_webmail && !$plugin['imail_gamma'])
        {
        	$result['webmail_changepassword'] = $lwebmail->change_password($rowObj["UserLogin"],$newPassword,"iMail");
        }
        # iMail Gamma 
        if($plugin['imail_gamma'])
		{
			$IMapEmail = trim($rowObj["UserLogin"])."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
			if($IMap->is_user_exist($IMapEmail))
			{
				$imap_result = $IMap->change_password($IMapEmail, $newPassword);
			} else
			{
				$imap_result = null;
			}
        }
        
        # FTP management
        if ($plugin['personalfile'])
        {
            if ($personalfile_type == 'FTP' || $personalfile_type == 'LOCAL_FTP' || $personalfile_type == 'REMOTE_FTP')
            {
            	$result['ftp_changepassword'] = @$lftp->changePassword($rowObj["UserLogin"],$newPassword,"iFolder");
            }
        }

		
		# notify the user by email
		$UserInfo = $li->forgetPassword2($rowObj["UserLogin"]);
		
	     $UserEmail = $UserInfo[3];
	     $EnglishName = $UserInfo[4];
	     $ChineseName = $UserInfo[5];
	     
	
		 list($mailSubject, $mailBody) = $luser->returnEmailNotificationData($UserEmail, $rowObj["UserLogin"], $newPassword, $EnglishName);
	     $result['send_email'] = $lu->send_email($UserEmail, $Lang['EmailNotification']['PasswordReset']['Subject'], $mailBody,"");
			
	}
	
$linterface->LAYOUT_START();
?>



<br /><br />
<p><font size="3"><font color='blue'><?=str_replace("[=total_number=]", $data_size, $Lang['login']['password_follow_option_a_result_text'])?></font></font></p>
<br />
<p><?= $linterface->GET_ACTION_BTN($Lang['General']['Close'], "button", "self.parent.tb_remove()")?></p>
<!--
<table width="96%" border="0" align="center">
<tr>
	<td>
	<?=$print_all?>
		<table align="center" class="common_table_list">
		<thead>
			<th width='1%'>#</th>
			<th width='10%'><?=$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['UserType']?></th>
			<th width='15%'><?=$Lang['RepairSystem']['ImportDataCol'][0]?></th>
			<th width='20%'><?=$Lang['General']['EnglishName']?></th>
			<th width='20%'><?=$Lang['General']['ChineseName']?></th>
			<th width='8%'><?=$Lang['SysMgr']['RoleManagement']['Class']?></th>
			<th width='7%'><?=$Lang['SysMgr']['RoleManagement']['ClassNumber']?></th>
			<th width='20%'><?=$Lang['login']['last_used']?></th>
		</thead>
		<tbody>
			<?=$sent_records?>
		</tbody>
		</table>
	</td>
</tr>
</table>
-->

<?
	
	intranet_closedb();
$linterface->LAYOUT_STOP();
?>