<?php
include_once('./db_config.php'); //for db param config


$search_type = $_GET["search_type"];        //normal_search; advanced_search; super_search
$user_id = $_GET["user_id"];
$is_admin = $_GET["is_admin"];

$search_contents = $_GET["search_contents"];

//advanced search field
$email_from=$_GET["email_from"];
$sent_to=$_GET["sent_to"];
$cc=$_GET["cc"];
$bcc=$_GET["bcc"];
$subject=$_GET["subject"];
$contents=$_GET["contents"];
$folder_name=$_GET["folder_name"];
$from_date=$_GET["from_date"];
$to_date=$_GET["to_date"];
$attachments=$_GET["attachments"];
$has_read=$_GET["has_read"];
$importance=$_GET["importance"];

//database connection
// $servername = "padmin.biz21.hk";
// $username = "c21biz30_imail2";
// $password = "dLeAE@5#Wk]$";
// $dbname = "c21biz30_imail";

$conn = new mysqli($servername, $username, $password, $dbname, $dbport);
 
// check database connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$fulltextsearch = $_REQUEST['fulltextsearch'];

$debug_count = $_REQUEST['debug_count']==1;

if($fulltextsearch){
	$email_table = "emails_20180112";
}else{
	$email_table = "emails";
}

//************* get specific record *************
if ($search_type == 'normal_search') {
    $search_contents = $conn->real_escape_string($search_contents);
    $sql = "SELECT user_id, email_time, folder_name, email_id, email_from, sent_to, cc, bcc, subject, contents, attachments, importance, flagged, has_read, has_replied, has_forward FROM $email_table 
        WHERE   user_id=$user_id AND 
              ( folder_name LIKE '%$search_contents%' OR
                subject LIKE '%$search_contents%' OR ";
      if($fulltextsearch){
      	$sql .= " MATCH(contents) AGAINST ('\"".$conn->real_escape_string($contents)."\"' IN BOOLEAN MODE) OR ";
      }else{
      	$sql .= " contents LIKE '%$search_contents%' OR ";
      } 
      $sql .= " sent_to LIKE '%$search_contents%' OR
                email_from LIKE '%$search_contents%' OR
                attachments LIKE '%$search_contents%' OR
                cc LIKE '%$search_contents%' OR
                bcc LIKE '%$search_contents%')      
        ORDER BY email_time DESC";
}

if ($search_type == 'advanced_search') {
    
    $sql = "SELECT user_id, email_time, folder_name, email_id, email_from, sent_to, cc, bcc, subject, contents, attachments, importance, flagged, has_read, has_replied, has_forward FROM $email_table WHERE user_id = $user_id";

    if (isset($email_from)&&$email_from!="") {
        $sql .= " AND email_from LIKE '%".$conn->real_escape_string($email_from)."%' ";
    }

    if (isset($sent_to)&&$sent_to!="") {
        $sql .= " AND sent_to LIKE '%".$conn->real_escape_string($sent_to)."%' ";
    }

    if (isset($cc)&&$cc!="") {
        $sql .= " AND cc LIKE '%".$conn->real_escape_string($cc)."%' ";
    }

    if (isset($bcc)&&$bcc!="") {
        $sql .= " AND bcc LIKE '%".$conn->real_escape_string($bcc)."%' ";
    }

    if (isset($subject)&&$subject!="") {
        $sql .= " AND subject LIKE '%".$conn->real_escape_string($subject)."%' ";
    }

    if (isset($contents)&&$contents!="") {
    	if($fulltextsearch){
        	$sql .= " AND MATCH(contents) AGAINST ('\"".$conn->real_escape_string($contents)."\"' IN BOOLEAN MODE) ";
        	//$sql .= " AND MATCH(contents) AGAINST ('\"".$conn->real_escape_string($contents)."\"' IN NATURAL LANGUAGE MODE) ";
    	}else{
        	$sql .= " AND contents LIKE '%".$conn->real_escape_string($contents)."%' ";
    	}
    }

    if (isset($folder_name)&&$folder_name!="") {
        $sql .= " AND folder_name LIKE '%".$conn->real_escape_string($folder_name)."%' ";
    }

    if (isset($from_date)&&isset($to_date)&&$from_date!=""&&$to_date!="") {
        $sql .= " AND email_time BETWEEN '".$conn->real_escape_string($from_date)."' AND '".$conn->real_escape_string($to_date)."' ";
    }

    if (isset($attachments)&&$attachments!="") {
        $sql .= " AND attachments LIKE '%".$conn->real_escape_string($attachments)."%' ";
    }

    if (isset($has_read)&&$has_read!="") {
        $sql .= " AND has_read='".$conn->real_escape_string($has_read)."' ";
    }

    if (isset($importance)&&$importance!="") {
        $sql .= " AND importance='".$conn->real_escape_string($importance)."' ";
    }

    $sql .= " ORDER BY email_time DESC";

}

if ($search_type == 'super_search' && $is_admin ==1) { //super search, without user id limit
    
    $sql = "SELECT user_id, email_time, folder_name, email_id, email_from, sent_to, cc, bcc, subject, contents, attachments, importance, flagged, has_read, has_replied, has_forward FROM $email_table WHERE user_id IS NOT NULL";

    if (isset($email_from)&&$email_from!="") {
        $sql .= " AND email_from LIKE '%".$conn->real_escape_string($email_from)."%' ";
    }

    if (isset($sent_to)&&$sent_to!="") {
        $sql .= " AND sent_to LIKE '%".$conn->real_escape_string($sent_to)."%' ";
    }

    if (isset($cc)&&$cc!="") {
        $sql .= " AND cc LIKE '%".$conn->real_escape_string($cc)."%' ";
    }

    if (isset($bcc)&&$bcc!="") {
        $sql .= " AND bcc LIKE '%".$conn->real_escape_string($bcc)."%' ";
    }

    if (isset($subject)&&$subject!="") {
        $sql .= " AND subject LIKE '%".$conn->real_escape_string($subject)."%' ";
    }

    if (isset($contents)&&$contents!="") {
        if($fulltextsearch){
        	$sql .= " AND MATCH(contents) AGAINST ('\"".$conn->real_escape_string($contents)."\"' IN BOOLEAN MODE) ";
    	}else{
        	$sql .= " AND contents LIKE '%".$conn->real_escape_string($contents)."%' ";
        }
    }

    if (isset($folder_name)&&$folder_name!="") {
        $sql .= " AND folder_name LIKE '%".$conn->real_escape_string($folder_name)."%' ";
    }

    if (isset($from_date)&&isset($to_date)&&$from_date!=""&&$to_date!="") {
        $sql .= " AND email_time BETWEEN '".$conn->real_escape_string($from_date)."' AND '".$conn->real_escape_string($to_date)."' ";
    }

    if (isset($attachments)&&$attachments!="") {
        $sql .= " AND attachments LIKE '%".$conn->real_escape_string($attachments)."%' ";
    }

    if (isset($has_read)&&$has_read!="") {
        $sql .= " AND has_read='".$conn->real_escape_string($has_read)."' ";
    }

    if (isset($importance)&&$importance!="") {
        $sql .= " AND importance='".$conn->real_escape_string($importance)."' ";
    }

    $sql .= " ORDER BY email_time DESC";


}

//(user_id, email_time, folder_name, email_id, email_from, sent_to, cc, bcc, subject, contents, attachments, importance, flagged, has_read, has_replied, has_forward, system_tags)

$result = $conn->query($sql);


if ($result->num_rows > 0) {
    
    while($row = $result->fetch_assoc()) {
    	
//        $row["email_from"] = unserialize($row["email_from"]);
//        $row["sent_to"] = unserialize($row["sent_to"]);
//        $row["cc"] = unserialize($row["cc"]);
//        $row["bcc"] = unserialize($row["bcc"]);
//        //$row["contents"] = str_replace("/\"", "'", $row["contents"]);
//        $row["attachments"] = unserialize($row["attachments"]);

        $return[]=$row;
        
    }

	if($debug_count){
		$record_count = count($return);
		$return = $record_count;
	}

} else {
    $return['No result']='No result found.';
}


/*
header("Content-Type: text/html;charset=utf-8");
echo '<PRE>';
echo $sql;echo "<br />\n";
//print_r($return);
echo count($return);
echo '</PRE>';
*/


header("Content-Type: application/json;charset=utf-8");
echo json_encode($return);


/*
$encoded_json = json_encode($return);
$decoded_json = json_decode($encoded_json);

echo '<PRE>';
print_r($decoded_json);
echo '</PRE>';
*/

$conn->close();

?>