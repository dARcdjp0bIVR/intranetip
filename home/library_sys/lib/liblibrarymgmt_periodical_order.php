<?php
/*
 * 
 */ 
if (!defined("LIBLIBRARYMGMT_PERIODICAL_ORDER_DEFINED"))         // Preprocessor directives
{
	define("LIBLIBRARYMGMT_PERIODICAL_ORDER_DEFINED", true);
	
	include_once($PATH_WRT_ROOT.'includes/libdbobject.php');
	class liblms_periodical_order extends libdbobject {
		var $liblms;
		
		var $orderId;
		var $periodicalBookId;
		var $orderStartDate;
		var $orderEndDate;
		var $price;
		var $frequencyUnit;
		var $frequencyNum;
		var $publishDateFirst;
		var $publishDateLast;
		var $firstIssue;
		var $lastIssue;
		var $distributor;
		var $remarks;
		var $alertDay;
		var $isGenerated;
		var $generateDate;
		var $generatedBy;
		var $recordStatus;
		var $dateInput;
		var $inputBy;
		var $dateModified;
		var $lastModifiedBy;
		
		public function __construct($objectId='') {
			$this->liblms = new liblms();
			
			parent::__construct($this->liblms->db.'.LIBMS_PERIODICAL_ORDER', 'OrderID', $this->returnFieldMappingAry(), $objectId);
		}
		
		function setOrderId($val) {
			$this->orderId = $val;
		}
		function getOrderId() {
			return $this->orderId;
		}
		
		function setPeriodicalBookId($val) {
			$this->periodicalPeriodicalBookId = $val;
		}
		function getPeriodicalBookId() {
			return $this->periodicalPeriodicalBookId;
		}
		
		function setOrderStartDate($val) {
			$this->orderStartDate = $val;
		}
		function getOrderStartDate() {
			return $this->orderStartDate;
		}
		
		function setOrderEndDate($val) {
			$this->orderEndDate = $val;
		}
		function getOrderEndDate() {
			return $this->orderEndDate;
		}
		
		function setPrice($val) {
			$this->price = $val;
		}
		function getPrice() {
			return $this->price;
		}
		
		function setFrequencyUnit($val) {
			$this->frequencyUnit = $val;
		}
		function getFrequencyUnit() {
			return $this->frequencyUnit;
		}
		
		function setFrequencyNum($val) {
			$this->frequencyNum = $val;
		}
		function getFrequencyNum() {
			return $this->frequencyNum;
		}
		
		function setPublishDateFirst($val) {
			$this->publishDateFirst = $val;
		}
		function getPublishDateFirst() {
			return $this->publishDateFirst;
		}
		
		function setPublishDateLast($val) {
			$this->publishDateLast = $val;
		}
		function getPublishDateLast() {
			return $this->publishDateLast;
		}
		
		function setFirstIssue($val) {
			$this->firstIssue = $val;
		}
		function getFirstIssue() {
			return $this->firstIssue;
		}
		
		function setLastIssue($val) {
			$this->lastIssue = $val;
		}
		function getLastIssue() {
			return $this->lastIssue;
		}
		
		function setDistributor($val) {
			$this->distributor = $val;
		}
		function getDistributor() {
			return $this->distributor;
		}
		
		function setRemarks($val) {
			$this->remarks = $val;
		}
		function getRemarks() {
			return $this->remarks;
		}
		
		function setAlertDay($val) {
			$this->alertDay = $val;
		}
		function getAlertDay() {
			return $this->alertDay;
		}
		
		function setIsGenerated($val) {
			$this->isGenerated = $val;
		}
		function getIsGenerated() {
			return $this->isGenerated;
		}
		
		function setGenerateDate($val) {
			$this->generateDate = $val;
		}
		function getGenerateDate() {
			return $this->generateDate;
		}
		
		function setGeneratedBy($val) {
			$this->generatedBy = $val;
		}
		function getGeneratedBy() {
			return $this->generatedBy;
		}
		
		function setRecordStatus($val) {
			$this->recordStatus = $val;
		}
		function getRecordStatus() {
			return $this->recordStatus;
		}
		
		function setDateInput($val) {
			$this->dateInput = $val;
		}
		function getDateInput() {
			return $this->dateInput;
		}
		
		function setInputBy($val) {
			$this->inputBy = $val;
		}
		function getInputBy() {
			return $this->inputBy;
		}
		
		function setDateModified($val) {
			$this->dateModified = $val;
		}
		function getDateModified() {
			return $this->dateModified;
		}
		
		function setLastModifiedBy($val) {
			$this->lastModifiedBy = $val;
		}
		function getLastModifiedBy() {
			return $this->lastModifiedBy;
		}
		
		function returnFieldMappingAry() {
			$fieldMappingAry = array();
			$fieldMappingAry[] = array('OrderID', 'int', 'setOrderId', 'getOrderId');
			$fieldMappingAry[] = array('PeriodicalBookID', 'int', 'setPeriodicalBookId', 'getPeriodicalBookId');
			$fieldMappingAry[] = array('OrderStartDate', 'date', 'setOrderStartDate', 'getOrderStartDate');
			$fieldMappingAry[] = array('OrderEndDate', 'date', 'setOrderEndDate', 'getOrderEndDate');
			$fieldMappingAry[] = array('Price', 'int', 'setPrice', 'getPrice');
			$fieldMappingAry[] = array('FrequencyUnit', 'str', 'setFrequencyUnit', 'getFrequencyUnit');
			$fieldMappingAry[] = array('FrequencyNum', 'int', 'setFrequencyNum', 'getFrequencyNum');
			$fieldMappingAry[] = array('PublishDateFirst', 'date', 'setPublishDateFirst', 'getPublishDateFirst');
			$fieldMappingAry[] = array('PublishDateLast', 'date', 'setPublishDateLast', 'getPublishDateLast');
			$fieldMappingAry[] = array('FirstIssue', 'int', 'setFirstIssue', 'getFirstIssue');
			$fieldMappingAry[] = array('LastIssue', 'int', 'setLastIssue', 'getLastIssue');
			$fieldMappingAry[] = array('Distributor', 'str', 'setDistributor', 'getDistributor');
			$fieldMappingAry[] = array('Remarks', 'str', 'setRemarks', 'getRemarks');
			$fieldMappingAry[] = array('AlertDay', 'int', 'setAlertDay', 'getAlertDay');
			$fieldMappingAry[] = array('IsGenerated', 'int', 'setIsGenerated', 'getIsGenerated');
			$fieldMappingAry[] = array('GenerateDate', 'date', 'setGenerateDate', 'getGenerateDate');
			$fieldMappingAry[] = array('GeneratedBy', 'int', 'setGeneratedBy', 'getGeneratedBy');
			$fieldMappingAry[] = array('RecordStatus', 'int', 'setRecordStatus', 'getRecordStatus');
			$fieldMappingAry[] = array('DateInput', 'date', 'setDateInput', 'getDateInput');
			$fieldMappingAry[] = array('InputBy', 'int', 'setInputBy', 'getInputBy');
			$fieldMappingAry[] = array('DateModified', 'date', 'setDateModified', 'getDateModified');
			$fieldMappingAry[] = array('LastModifiedBy', 'int', 'setLastModifiedBy', 'getLastModifiedBy');
			return $fieldMappingAry;
		}
				
		
		
		function newRecordBeforeHandling() {
			$this->setDateInput('now()');
			$this->setInputBy($_SESSION['UserID']);
			$this->setDateModified('now()');
			$this->setLastModifiedBy($_SESSION['UserID']);
			return true;
		}
		
		function updateRecordBeforeHandling() {
			$this->setDateModified('now()');
			$this->setLastModifiedBy($_SESSION['UserID']);
			return true;
		}
		
//		function deleteRecordBeforeHandling() {
//			$tmpAry = array();
//		    $tmpAry['BookID'] = $this->getPeriodicalBookId();
//		    $tmpAry['OrderStartDate'] = $this->getOrderStartDate();
//		    $tmpAry['OrderEndDate'] = $this->getOrderEndDate();
//		    $tmpAry['Price'] = $this->getPrice();
//		    $tmpAry['FrequencyUnit'] = $this->getFrequencyUnit();
//		    $tmpAry['FrequencyNum'] = $this->getFrequencyNum();
//		    $tmpAry['PublishDateFirst'] = $this->getPublishDateFirst();
//		    $tmpAry['PublishDateLast'] = $this->getPublishDateLast();
//		    $tmpAry['FirstIssue'] = $this->getFirstIssue();
//		    $tmpAry['LastIssue'] = $this->getLastIssue();
//		    $tmpAry['Distributor'] = $this->getDistributor();
//		    $tmpAry['Remarks'] = $this->getRemarks();
//		    $tmpAry['AlertDay'] = $this->getAlertDay();
//		    $tmpAry['IsGenerated'] = $this->getIsGenerated();
//		    $tmpAry['DateInput'] = $this->getDateInput();
//		    $tmpAry['InputBy'] = $this->getInputBy();
//		    $tmpAry['DateModified'] = $this->getDateModified();
//		    $tmpAry['LastModifiedBy'] = $this->getLastModifiedBy();
//		    
//		    $liblog = new liblog();
//		    return $liblog->INSERT_LOG('LMS', 'deletePeriodicalOrder', $liblog->BUILD_DETAIL($tmpAry), 'LIBMS_PERIODICAL_ORDER', $this->getOrderId());
//		}
	}
}
?>