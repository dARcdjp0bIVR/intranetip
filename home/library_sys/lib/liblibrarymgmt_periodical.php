<?php
// using: ivan
if (!defined("LIBLIBRARYMGMT_PERIODICAL_DEFINED"))                     // Preprocessor directive
{
	define("LIBLIBRARYMGMT_PERIODICAL_DEFINED", true);
	
	class liblms_periodical extends liblms{

		function __construct(){
			parent::__construct();
		}
		
		function GET_PERIODICAL_INFO($periodicalBookIdAry='', $periodicalCodeAry='', $excludePeriodicalBookIdAry='', $checkActiveOnly=true) {
			if ($periodicalBookIdAry !== '' && $periodicalBookIdAry !== null) {
				$condsPeroidicalBookId = " And PeriodicalBookID In ('".implode("','", (array)$periodicalBookIdAry)."') ";
			}
			
			if ($periodicalCodeAry !== '' && $periodicalCodeAry !== null) {
				$numOfCode = count($periodicalCodeAry);
				for ($i=0; $i<$numOfCode; $i++) {
					$periodicalCodeAry[$i] = $this->Get_Safe_Sql_Query($periodicalCodeAry[$i]);
				}
				$condsPeriodicalCode = " And PeriodicalCode In ('".implode("','", (array)$periodicalCodeAry)."') ";
			}
			
			if ($excludePeriodicalBookIdAry !== '' && $excludePeriodicalBookIdAry !== null) {
				$condsExcludePerioidicalBookId = " And PeriodcialBookID NOT In ('".implode("','", (array)$excludePeriodicalBookIdAry)."') ";
			}
			
			if ($checkActiveOnly) {
				$condsActivePeriodical = " And RecordStatus = 1 ";
			}
			
			$lmsDb = $this->db;
			$sql = "SELECT * FROM $lmsDb.LIBMS_PERIODICAL_BOOK WHERE 1 $condsActivePeriodical $condsPeroidicalBookId $condsKeyword $condsPeriodicalCode $condsExcludePerioidicalBookId";	
	    	return $this->returnArray($sql);
		}
		
		function GET_PERIODICAL_LIST_SQL($keyword='') {
			global $junior_mck;
			
			if ($keyword !== '' && $keyword !== null) {
				$condsKeyword = " And (
										lpb.PeriodicalCode Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or lpb.BookTitle Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or lpb.ISSN Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
									  ) 
								";
			}
			
			$PeriodicalBookID_For_CONCAT = (isset($junior_mck)) ? "CONVERT(lpb.PeriodicalBookID USING utf8)" : "lpb.PeriodicalBookID";
			
			// count by number of comma
			$orderCountField = "length(GROUP_CONCAT(DISTINCT(lpo.OrderID))) - length(replace(GROUP_CONCAT(DISTINCT(lpo.OrderID)), ',', '')) + 1";
			$itemCountField = "length(GROUP_CONCAT(DISTINCT(lpi.ItemID))) - length(replace(GROUP_CONCAT(DISTINCT(lpi.ItemID)), ',', '')) + 1";
			$lmsDb = $this->db;
			$sql = "Select
							lpb.PeriodicalCode,
							CONCAT('<a href=\"javascript:goEdit(\'', $PeriodicalBookID_For_CONCAT, '\');\">', lpb.BookTitle, '</a>'),
							lpb.ISSN,
							IF($orderCountField IS NULL, CONCAT('<a href=\"javascript:goOrderList(', lpb.PeriodicalBookID, ')\" class=\"tablelink\">0</a>'), CONCAT('<a href=\"javascript:goOrderList(', lpb.PeriodicalBookID, ')\" class=\"tablelink\">', $orderCountField,'</a>')) as OrderLink,
							IF($itemCountField IS NULL, CONCAT('<a href=\"javascript:goOrderList(', lpb.PeriodicalBookID, ')\" class=\"tablelink\">0</a>'), CONCAT('<a href=\"javascript:goRegistartionList(', lpb.PeriodicalBookID, ')\" class=\"tablelink\">', $itemCountField,'</a>')) as ItemLink,
							CONCAT('<input type=\"checkbox\" name=\"PeriodicalBookIdAry[]\" id=\"PeriodicalBookIdAry[]\" value=\"', lpb.PeriodicalBookID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />'),
							$orderCountField as OrderCount,
							$itemCountField as ItemCount
					From
							$lmsDb.LIBMS_PERIODICAL_BOOK as lpb
							Left Outer Join $lmsDb.LIBMS_PERIODICAL_ORDER as lpo On (lpb.PeriodicalBookID = lpo.PeriodicalBookID And lpo.RecordStatus = '1')
							Left Outer Join $lmsDb.LIBMS_PERIODICAL_ITEM as lpi On (lpb.PeriodicalBookID = lpi.PeriodicalBookID And lpi.RecordStatus = '1')
					Where
							lpb.RecordStatus = 1 
							$condsKeyword
					Group By
							lpb.PeriodicalBookID
					";
			return $sql;
		}
		
		function GET_PERIODICAL_ORDER_INFO($orderIdAry='') {
			if ($orderIdAry !== '' && $orderIdAry !== null) {
				$condsOrderId = " And OrderID In ('".implode("','", (array)$orderIdAry)."') ";
			}
			
			$lmsDb = $this->db;
			$sql = "SELECT * FROM $lmsDb.LIBMS_PERIODICAL_ORDER WHERE 1 $condsOrderId Order By OrderStartDate";
	    	return $this->returnArray($sql);
		}
		
		function GET_PERIODICAL_PUBLISH_FREQUENCY_UNIT_SELECTION($Id, $Name, $Selected='', $OnChange='') {
			global $Lang;
			
			$selectAry = array();
			$selectAry['D'] = $Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Day"];
			$selectAry['W'] = $Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Week"];
			$selectAry['M'] = $Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Month"];
			$selectAry['Y'] = $Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Year"];
			
			if ($OnChange !== '') {
				$onchangeAttr = ' onchange="'.$OnChange.'" ';
			}
			
			$selectAttr = ' id="'.$Id.'" name="'.$Name.'" '.$onchangeAttr;
			
			return getSelectByAssoArray($selectAry, $selectAttr, $Selected, $all=0, $noFirst=1);
		}
		
		function GET_PERIODICAL_GENERATED_SELECTION($Id, $Name, $Selected='', $OnChange='') {
			global $Lang;
			
			$selectAry = array();
			$selectAry['G'] = $Lang["libms"]["periodicalMgmt"]["Generated"];
			$selectAry['NG'] = $Lang["libms"]["periodicalMgmt"]["NotGeneratedYet"];
			
			if ($OnChange !== '') {
				$onchangeAttr = ' onchange="'.$OnChange.'" ';
			}
			
			$selectAttr = ' id="'.$Id.'" name="'.$Name.'" '.$onchangeAttr;
			
			return getSelectByAssoArray($selectAry, $selectAttr, $Selected, $all=1, $noFirst=0, Get_Selection_First_Title($Lang["libms"]["periodicalMgmt"]["GenerateStatus"]));
		}
		
		function GET_PERIODICAL_ITEM_REGISTERED_SELECTION($Id, $Name, $Selected='', $OnChange='') {
			global $Lang;
			
			$selectAry = array();
			$selectAry['R'] = $Lang["libms"]["periodicalMgmt"]["Registered"];
			$selectAry['NR'] = $Lang["libms"]["periodicalMgmt"]["NotRegistered"];
			
			if ($OnChange !== '') {
				$onchangeAttr = ' onchange="'.$OnChange.'" ';
			}
			
			$selectAttr = ' id="'.$Id.'" name="'.$Name.'" '.$onchangeAttr;
			
			return getSelectByAssoArray($selectAry, $selectAttr, $Selected, $all=1, $noFirst=0, Get_Selection_First_Title($Lang["libms"]["periodicalMgmt"]["RegistrationStatus"]));
		}
		
		function GET_PERIODICAL_ORDER_LIST_SQL($periodicalBookId, $keyword='', $generateStatus='') {
			global $junior_mck, $Lang;
			
			if ($keyword !== '' && $keyword !== null) {
				$condsKeyword = " And (
										OrderStartDate Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or OrderEndDate Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or FirstIssue Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or LastIssue Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or Remarks Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
									  ) 
								";
			}
			
			if ($generateStatus !== '' && $generateStatus !== null) {
				$generateStatusDb = ($generateStatus=='G')? 1 : 0;
				$condsGenerateStatus = " And IsGenerated = '".$generateStatusDb."' ";
			}
			
			$lmsDb = $this->db;
			$sql = "Select
							OrderStartDate,
							OrderEndDate,
							IF (FirstIssue = 0 Or FirstIssue is null, '".$Lang['General']['EmptySymbol']."', FirstIssue) as FirstIssue,
							IF (LastIssue = 0 Or LastIssue is null, '".$Lang['General']['EmptySymbol']."', LastIssue) as LastIssue,
							IF (Remarks is null Or Remarks = '', '".$Lang['General']['EmptySymbol']."', Remarks) as Remarks,
							IF(IsGenerated = 1, '".$Lang['General']['Yes']."', '".$Lang['General']['No']."') as IsGenerated,
							CONCAT('<input type=\"checkbox\" name=\"orderIdAry[]\" id=\"orderIdAry[]\" value=\"', OrderID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />'),
							IF (DATE(NOW()) > OrderEndDate, 'row_suspend', '') as trCustClass
					From
							$lmsDb.LIBMS_PERIODICAL_ORDER
					Where
							PeriodicalBookID = '".$periodicalBookId."'
							And RecordStatus = '1'
							$condsKeyword
							$condsGenerateStatus
					";
			return $sql;
		}
		
		function GET_PERIODICAL_ORDER_ITEM_LIST_SQL($periodicalBookId, $keyword='', $registerStatus='') {
			global $junior_mck, $Lang;
			
			if ($keyword !== '' && $keyword !== null) {
				$condsKeyword = " And (
										lpi.Details Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or lpi.Issue Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
									  ) 
								";
			}
			
			if ($registerStatus && $registerStatus !== null) {
				$registerStatusDb = ($registerStatus=='R')? ' IS NOT NULL ' : ' IS NULL ';
				$condsGenerateStatus = " And lpi.RegistrationDate $registerStatusDb ";
			}
			
			$lmsDb = $this->db;
			$sql = "Select
							lpi.PublishDate,
							lpi.Issue,
							IF (lpi.Details is null, '".$Lang['General']['EmptySymbol']."', lpi.Details) as Details,
							IF (lpi.RegistrationDate is null, '".$Lang['General']['No']."', '".$Lang['General']['Yes']."') as IsRegistered,
							IF (lpi.RegistrationDate is null, '".$Lang['General']['EmptySymbol']."', lpi.RegistrationDate) as RegistrationDate,
							CONCAT('<a href=\"javascript:goRegister(', lpi.ItemID, ');\" class=\"tablelink\">', Count(lbu.UniqueID), '</a>') as RegisteredItemLink,
							CONCAT('<input type=\"checkbox\" name=\"itemIdAry[]\" id=\"itemIdAry[]\" value=\"', ItemID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />'),
							Count(lbu.UniqueID) as RegisteredItemNum
					From
							$lmsDb.LIBMS_PERIODICAL_ITEM as lpi
							Left Outer Join $lmsDb.LIBMS_BOOK_UNIQUE as lbu On (lpi.ItemID = lbu.PeriodicalItemID And lpi.PeriodicalBookID = lbu.PeriodicalBookID And lbu.RecordStatus != 'DELETE')
							Left Outer Join $lmsDb.LIBMS_BOOK as lb On (lbu.BookID = lb.BookID And lpi.ItemID = lb.PeriodicalItemID And lpi.PeriodicalBookID = lb.PeriodicalBookID And lb.RecordStatus != 'DELETE')
					Where
							lpi.PeriodicalBookID = '".$periodicalBookId."'
							And lpi.RecordStatus = '1'
							$condsKeyword
							$condsGenerateStatus
					Group By
							lpi.ItemID
					";
			return $sql;
		}
		
		function GET_TEMP_PUBLISH_ITEM_INFO_BY_ORDERID($orderIdAry) {
			$orderInfoAry = $this->GET_PERIODICAL_ORDER_INFO($orderIdAry);
			$numOfOrder = count((array)$orderInfoAry);
			
			$itemInfoAry = array();
			$itemCount = 0;
			for ($i=0; $i<$numOfOrder; $i++) {
				$_orderId = $orderInfoAry[$i]['OrderID'];
				$_publishDateFirst = $orderInfoAry[$i]['PublishDateFirst'];
				$_publishDateLast = $orderInfoAry[$i]['PublishDateLast'];
				$_frequencyUnit = $orderInfoAry[$i]['FrequencyUnit'];
				$_frequencyNum = $orderInfoAry[$i]['FrequencyNum'];
				$_firstIssue = $orderInfoAry[$i]['FirstIssue'];
				
				$_publishDateAry = $this->GET_REPEATED_DATE_ARRAY($_publishDateFirst, $_publishDateLast, $_frequencyUnit, $_frequencyNum);
				$_numOfPublishDate = count($_publishDateAry);
				for ($j=0; $j<$_numOfPublishDate; $j++) {
					$__publishDate = $_publishDateAry[$j];
					$__issueNum = $_firstIssue + $j;
					
					$itemInfoAry[$itemCount]['PublishDate'] = $__publishDate;
					$itemInfoAry[$itemCount]['IssueNum'] = $__issueNum;
					$itemCount++;
				}
			}
			
			return $itemInfoAry;
		}
		
		// reference: /includes/icalender.php function get_all_repeated_day() 
		function GET_REPEATED_DATE_ARRAY($startDate, $endDate, $frequencyUnit, $frequencyNum) {
			$days = array();
			$start = strtotime($startDate);
			$end = strtotime($endDate);
			
//			$interval = empty($rule["interval"])?1:$rule["interval"];
			$interval = $frequencyNum;
			switch ($frequencyUnit){
				case "D":	// daily
					while ($start <= $end){
						$days[] = date("Y-m-d",$start);
						$start = strtotime("+".$interval." day",$start);
					}
				break;
				case "W":	// weekly
//					if(!empty($rule['byDay'])){
//						$weeks = explode(",",$rule["byDay"]);
//						$weeks = empty($weeks)? array($rule["byDay"]):$weeks;
//						$temp_weekday = $start;
//						$continue = true;
//						while (true){
//							if($start > $end) break;
//							for($i=1;$i<=7;$i++){
//								$weekday_prefix = strtoupper(substr(date('D',$temp_weekday),0,2));
//								if(in_array($weekday_prefix,$weeks)){
//									if ($temp_weekday <= $end)
//										$days[] = date("Y-m-d",$temp_weekday);
//									else{
//										$continue = false;
//										break;
//									}
//								}
//								$temp_weekday = strtotime("+1 day",$temp_weekday);
//							}
//							if (!$continue)
//								break;
//							$start = strtotime("+".$interval." week",$start);
//							$temp_weekday = $start;
//						}
//					}else{
						while($start <= $end){
							$days[] = date("Y-m-d",$start);
							$start = strtotime("+".$interval." week",$start);
						}
//					}
				break;
				case "M":	// monthly	
//					if (!empty($rule["byDay"])){
//						$days = array();
//						$weekDays = explode(",",$rule['byDay']);
//						$temp = $start;
//						$next_start = $start;
//						while($temp <= $end && count($weekDays)>0){
//							foreach($weekDays as $weekDay){
//								$weekDay = trim($weekDay);
//								$iweekday = intval($weekDay);
//								if(preg_match("/.*(\w\w).*/",$weekDay,$matches)){
//									$weekday = $matches[1];
//								}
//								$temp = $this->getNthWeekdayOfMonth($iweekday,$weekday,$next_start);
//								
//								if($temp >= $start && $temp <= $end){
//									$days[] = date("Y-m-d", $temp);
//								}
//							}
//							$next_start = strtotime("+".$interval." month",$next_start);
//						}
//					}
//					elseif (!empty($rule["byMonthDay"])){
						//day of week
						while ($start <= $end){
							$days[] = date("Y-m-d",$start);
							$start = strtotime("+".$interval." month",$start);
						}
//					}
					
				break;
				case "Y":	// yearly
					while ($start <= $end){
						$days[] = date("Y-m-d",$start);
						$start = strtotime("+".$interval." year",$start);
					}
				break;
			};
			return $days;
		}
		
		function GET_PERIODICAL_BASIC_INFO_DISPLAY($periodicalBookId) {
			global $Lang;
			
			$periodicalInfoAry = $this->GET_PERIODICAL_INFO($periodicalBookId);
			$periodicalCode = $periodicalInfoAry[0]['PeriodicalCode'];
			$bookTitle = $periodicalInfoAry[0]['BookTitle'];

			$x = '';
			$x .= '<table class="form_table_v30">'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td class="field_title">'.$Lang["libms"]["periodicalMgmt"]["PeriodicalCode"].'</td>'."\r\n";
					$x .= '<td>'.$periodicalCode.'</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td class="field_title">'.$Lang["libms"]["periodicalMgmt"]["PeriodicalTitle"].'</td>'."\r\n";
					$x .= '<td>'.$bookTitle.'</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</table>'."\r\n";
			
			return $x;
		}
		
		/*
		 * If change logic of the below function, please also update "/home/index.php" $showLibraryPeriodicalOrderAlert logic
		 */
		function GET_PERIODICAL_RENEW_ALERT_INFO() {
			$lmsDb = $this->db;
			$sql = "Select
							lpb.PeriodicalBookID, lpb.PeriodicalCode, lpb.BookTitle, lpo.OrderEndDate, lpo.PublishDateLast, lpo.Remarks
					From
							$lmsDb.LIBMS_PERIODICAL_BOOK as lpb
							Inner Join $lmsDb.LIBMS_PERIODICAL_ORDER as lpo On (lpb.PeriodicalBookID = lpo.PeriodicalBookID)
					Where
							lpb.RecordStatus = 1
							And lpo.RecordStatus = 1
							And lpo.AlertDay > 0
							And DATE_SUB(lpo.OrderEndDate, INTERVAL lpo.AlertDay DAY) <= CURDATE()
							And CURDATE() <= lpo.OrderEndDate
					Order By
							lpo.OrderEndDate
					";
			return $this->returnResultSet($sql);
		}
		
		function INACTIVATE_ORDER_ITEM($orderIdAry) {
			$lmsDb = $this->db;
			$sql = "Update $lmsDb.LIBMS_PERIODICAL_ITEM Set RecordStatus = '0' Where OrderID IN ('".implode("','", (array)$orderIdAry)."')";
			return $this->db_db_query($sql);
		}
		
		function GET_PERIODICAL_ITEM_INFO($periodicalBookIdAry='', $publishDateAry='', $excludeItemIdAry='', $includeItemIdAry='', $checkActiveOnly=true) {
			if ($periodicalBookIdAry !== '' && $periodicalBookIdAry !== null) {
				$condsPeriodicalBookId = " And lpi.PeriodicalBookID In ('".implode("','", (array)$periodicalBookIdAry)."') ";
			}
			
			if ($publishDateAry !== '' && $publishDateAry !== null) {
				$condsPublishDate = " And lpi.PublishDate In ('".implode("','", (array)$publishDateAry)."') ";
			}
			
			if ($excludeItemIdAry !== '' && $excludeItemIdAry !== null) {
				$condsExcludeItemId = " And lpi.ItemID NOT In ('".implode("','", (array)$excludeItemIdAry)."') ";
			}
			
			if ($includeItemIdAry !== '' && $includeItemIdAry !== null) {
				$condsIncludeItemId = " And lpi.ItemID In ('".implode("','", (array)$includeItemIdAry)."') ";
			}
			
			if ($checkActiveOnly) {
				$condsActive = " And lpi.RecordStatus = '1' And lpb.RecordStatus = '1' ";
			}
			
			$lmsDb = $this->db;
			$sql = "SELECT
							lpi.*,
							lpo.Distributor,
							lpb.PurchaseByDepartment
					FROM
							$lmsDb.LIBMS_PERIODICAL_ITEM as lpi
							Inner Join $lmsDb.LIBMS_PERIODICAL_BOOK as lpb On (lpi.PeriodicalBookID = lpb.PeriodicalBookID)
							Left Outer Join $lmsDb.LIBMS_PERIODICAL_ORDER as lpo On (lpi.OrderID = lpo.OrderID)
					WHERE
							1
							$condsActive
							$condsPeriodicalBookId
							$condsPublishDate
							$condsExcludeItemId
							$condsIncludeItemId
					";
	    	return $this->returnArray($sql);
		}
		
		function GET_LINKED_BOOK_INFO($periodicalBookIdAry, $periodicalItemIdAry) {
			$lmsDb = $this->db;
			$sql = "Select
							lb.BookID
					From
							$lmsDb.LIBMS_BOOK as lb
					Where
							lb.PeriodicalBookID IN ('".implode("','", (array)$periodicalBookIdAry)."')
							AND lb.PeriodicalItemID IN ('".implode("','", (array)$periodicalItemIdAry)."')
							AND (lb.RecordStatus is null Or lb.RecordStatus != 'DELETE')
					";
			return $this->returnResultSet($sql);
		}
	}	
}
?>