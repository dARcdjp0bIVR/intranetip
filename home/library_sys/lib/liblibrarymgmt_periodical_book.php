<?php
/*
 * 
 */ 
if (!defined("LIBLIBRARYMGMT_PERIODICAL_BOOK_DEFINED"))         // Preprocessor directives
{
	define("LIBLIBRARYMGMT_PERIODICAL_BOOK_DEFINED", true);
	
	include_once($PATH_WRT_ROOT.'includes/libdbobject.php');
	class liblms_periodical_book extends libdbobject {
		var $liblms;
		
		var $periodicalBookId;
		var $periodicalCode;
		var $resourcesTypeCode;
		var $circulationTypeCode;
		var $bookTitle;
		var $publisher;
		var $publishPlace;
		var $issn;
		var $alert;
		var $bookCategoryCode;
		var $purchaseByDepartment;
		var $remarks;
		var $openBorrow;
		var $openReservation;
		var $recordStatus;
		var $dateInput;
		var $inputBy;
		var $dateModified;
		var $lastModifiedBy;
		
		public function __construct($objectId='') {
			$this->liblms = new liblms();
			
			parent::__construct($this->liblms->db.'.LIBMS_PERIODICAL_BOOK', 'PeriodicalBookID', $this->returnFieldMappingAry(), $objectId);
		}
		
		function setPeriodicalBookId($val) {
			$this->periodicalBookId = $val;
		}
		function getPeriodicalBookId() {
			return $this->periodicalBookId;
		}
		
		function setPeriodicalCode($val) {
			$this->periodicalCode = $val;
		}
		function getPeriodicalCode() {
			return $this->periodicalCode;
		}
		
		function setResourcesTypeCode($val) {
			$this->resourcesTypeCode = $val;
		}
		function getResourcesTypeCode() {
			return $this->resourcesTypeCode;
		}
		
		function setCirculationTypeCode($val) {
			$this->circulationTypeCode = $val;
		}
		function getCirculationTypeCode() {
			return $this->circulationTypeCode;
		}
		
		function setBookTitle($val) {
			$this->bookTitle = $val;
		}
		function getBookTitle() {
			return $this->bookTitle;
		}
		
		function setPublisher($val) {
			$this->publisher = $val;
		}
		function getPublisher() {
			return $this->publisher;
		}
		
		function setPublishPlace($val) {
			$this->publishPlace = $val;
		}
		function getPublishPlace() {
			return $this->publishPlace;
		}
		
		function setIssn($val) {
			$this->issn = $val;
		}
		function getIssn() {
			return $this->issn;
		}
		
		function setAlert($val) {
			$this->alert = $val;
		}
		function getAlert() {
			return $this->alert;
		}
		
		function setBookCategoryCode($val) {
			$this->bookCategoryCode = $val;
		}
		function getBookCategoryCode() {
			return $this->bookCategoryCode;
		}
		
		function setPurchaseByDepartment($val) {
			$this->purchaseByDepartment = $val;
		}
		function getPurchaseByDepartment() {
			return $this->purchaseByDepartment;
		}
		
		function setRemarks($val) {
			$this->remarks = $val;
		}
		function getRemarks() {
			return $this->remarks;
		}
		
		function setOpenBorrow($val) {
			$this->openBorrow = $val;
		}
		function getOpenBorrow() {
			return $this->openBorrow;
		}
		
		function setOpenReservation($val) {
			$this->openReservation = $val;
		}
		function getOpenReservation() {
			return $this->openReservation;
		}
		
		function setRecordStatus($val) {
			$this->recordStatus = $val;
		}
		function getRecordStatus() {
			return $this->recordStatus;
		}
		
		function setDateInput($val) {
			$this->dateInput = $val;
		}
		function getDateInput() {
			return $this->dateInput;
		}
		
		function setInputBy($val) {
			$this->inputBy = $val;
		}
		function getInputBy() {
			return $this->inputBy;
		}
		
		function setDateModified($val) {
			$this->dateModified = $val;
		}
		function getDateModified() {
			return $this->dateModified;
		}
		
		function setLastModifiedBy($val) {
			$this->lastModifiedBy = $val;
		}
		function getLastModifiedBy() {
			return $this->lastModifiedBy;
		}
		
		function returnFieldMappingAry() {
			$fieldMappingAry = array();
			$fieldMappingAry[] = array('PeriodicalBookID', 'int', 'setPeriodicalBookId', 'getPeriodicalBookId');
			$fieldMappingAry[] = array('PeriodicalCode', 'str', 'setPeriodicalCode', 'getPeriodicalCode');
			$fieldMappingAry[] = array('ResourcesTypeCode', 'str', 'setResourcesTypeCode', 'getResourcesTypeCode');
			$fieldMappingAry[] = array('CirculationTypeCode', 'str', 'setCirculationTypeCode', 'getCirculationTypeCode');
			$fieldMappingAry[] = array('BookTitle', 'str', 'setBookTitle', 'getBookTitle');
			$fieldMappingAry[] = array('Publisher', 'str', 'setPublisher', 'getPublisher');
			$fieldMappingAry[] = array('PublishPlace', 'str', 'setPublishPlace', 'getPublishPlace');
			$fieldMappingAry[] = array('ISSN', 'str', 'setIssn', 'getIssn');
			$fieldMappingAry[] = array('Alert', 'str', 'setAlert', 'getAlert');
			$fieldMappingAry[] = array('BookCategoryCode', 'str', 'setBookCategoryCode', 'getBookCategoryCode');
			$fieldMappingAry[] = array('PurchaseByDepartment', 'str', 'setPurchaseByDepartment', 'getPurchaseByDepartment');
			$fieldMappingAry[] = array('Remarks', 'str', 'setRemarks', 'getRemarks');
			$fieldMappingAry[] = array('OpenBorrow', 'int', 'setOpenBorrow', 'getOpenBorrow');
			$fieldMappingAry[] = array('OpenReservation', 'int', 'setOpenReservation', 'getOpenReservation');
			$fieldMappingAry[] = array('RecordStatus', 'int', 'setRecordStatus', 'getRecordStatus');
			$fieldMappingAry[] = array('DateInput', 'date', 'setDateInput', 'getDateInput');
			$fieldMappingAry[] = array('InputBy', 'int', 'setInputBy', 'getInputBy');
			$fieldMappingAry[] = array('DateModified', 'date', 'setDateModified', 'getDateModified');
			$fieldMappingAry[] = array('LastModifiedBy', 'int', 'setLastModifiedBy', 'getLastModifiedBy');
			return $fieldMappingAry;
		}

				
		
		
		function newRecordBeforeHandling() {
			$this->setDateInput('now()');
			$this->setInputBy($_SESSION['UserID']);
			$this->setDateModified('now()');
			$this->setLastModifiedBy($_SESSION['UserID']);
			return true;
		}
		
		function updateRecordBeforeHandling() {
			$this->setDateModified('now()');
			$this->setLastModifiedBy($_SESSION['UserID']);
			return true;
		}
	}
}
?>