<?php
// using: 
/**
 * Log :
 * 
 * Date:    2019-07-30 [Tommy]
 *          created Get_Group_Member_For_Export() for exporting all group member at once
 *
 * Date:	2016-06-07 [Cameron]
 * 			Change sorting from Name to EnglishName in Get_Avaliable_User_List() because sorting Chinese Name is not easy
 * 			to understand [case #F95350]
 *
 * Date:	2016-01-05 [Henry]
 * 			Modified Add_Class_Mgt_Group_Member() to allow no user update
 * 
 * Date:	2015-06-08 [Cameron]
 * 			Add order by Name in Get_Avaliable_User_List()
 * 
 * Date:	2015-06-08 [Cameron]
 * 			Add IdentityType "Staff" to Get_Avaliable_User_List()
 * 
 * Date:	2015-06-05 [Henry]
 * 			added Add_Class_Mgt_Group_Member(), Change_Class_Mgt_Location() and Save_Class_Mgt_Group_Right()
 * 
 * Date:	2015-02-04 [Ryan]
 * 			Modified Get_Group_Member_List_With_Info() order by name field 
 *  
 * Date:	2014-12-05 [Ryan]
 * 			Added Get_Group_Member_List_With_Info()
 *  
 * Date:	2014-11-27 [Ryan]
 * 			Modified Get_Group_Period_Date(), Save_Group_Period(), add "SameReutenDay" option 
 * 
 * Date:	2013-09-11 [Siuwan]
 * 			Modified $this->MemberList, Get_Group_List() to display student which RecordStatus = 1 only
 *  
 * Date:	2013-08-29 [Ivan]
 * 			Added Check_Group_Code() and Rename_GroupCode()
 * 
 * Date:	2013-08-28 [Ivan]
 * 			Modified Get_Group_List() added return field GroupType
 * 
 * Date: 	2013-08-20 [Henry]
 * 			Modified the sql in Search_User()
 * 
 * Date: 	2013-04-18 [Cameron]
 * 			Retrieve BarCode in function liblibrarymgmt_group->MemberList
 */


if (!defined("LIBLIBRARYMGMT_GROUP_DEFINED"))                     // Preprocessor directive
{
define("LIBLIBRARYMGMT_GROUP_DEFINED", true);


if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    if (function_exists ("array_walk_recursive"))
    {
    	array_walk_recursive($gpc, "magicQuotes_awStripslashes");
    }
}
 

include_once("{$_SERVER['DOCUMENT_ROOT']}/includes/libdb.php");
include_once("{$_SERVER['DOCUMENT_ROOT']}/includes/libuser.php");
include_once("{$_SERVER['DOCUMENT_ROOT']}/includes/lib.php");
include_once("{$_SERVER['DOCUMENT_ROOT']}/includes/pdump/pdump.php");
include_once("{$_SERVER['DOCUMENT_ROOT']}/lang/libms.lang.$intranet_session_language.php");

class liblibrarymgmt_group extends libdb {
	
	var	$GroupID;
	var	$GroupTitle;
	var	$GroupCode;
	var	$IsDefault;
	var	$DateModified;
	var	$LastModifiedBy;
	
	var	$RightList;
	var	$RuleList;
	var	$MemberList;

	function liblibrarymgmt_group($GroupID="",$LoadRight=false,$LoadMember=false,$LoadRule=false) {
		global $Lang;
		global $eclass_prefix;

		# database for this module
		$this->db = $eclass_prefix . "eClass_LIBMS";
		//parent::libdb();
		if ($GroupID != "") {
			$sql = 'SELECT
					GroupID,
					GroupTitle,
					GroupCode,
					IsDefault,
					DateModified,
					LastModifiedBy
				FROM
					LIBMS_GROUP 
				WHERE
					GroupID = \''.$GroupID.'\'
				
				';
			$Result = $this->returnArray($sql);
			//$GLOBALS['DEBUG']['SQLs'][] = array( $sql, $Result, mysql_error());
			$this->GroupID = $Result[0]['GroupID'];
			$this->GroupTitle = $Result[0]['GroupTitle'];
			$this->GroupCode = $Result[0]['GroupCode'];
			$this->IsDefault = $Result[0]['IsDefault'];
			$this->DateModified = $Result[0]['DateModified'];
			$this->LastModifiedBy = $Result[0]['LastModifiedBy'];
			

			if ($LoadRight) {
				$sql = 'Select
						Section,
						Function,
						IsAllow
					From
						LIBMS_GROUP_RIGHT
					WHERE
						GroupID = \''.$this->GroupID.'\'
					';
		  	$this->RightList = $this->returnArray($sql);
			}
			//$GLOBALS['DEBUG']['SQLs'][] = array( $sql, $this->RightList,mysql_error());
			
			if ($LoadMember) {
				$NameField = $Lang['libms']['SQL']['UserNameFeild'];
				
				
				//$ParentNameField = getParentNameWithStudentInfo("r.","u.","",1);
				//$StudentNameField = getNameFieldWithClassNumberByLang("u.");
				
				$sql = 'Select
						u.UserID,						
						u.'.$NameField.' as MemberName,
						u.BarCode,
						u.UserType,
						u.ClassNumber,
						u.ClassName
						
					From
						LIBMS_GROUP_USER gu
					INNER JOIN
					    LIBMS_USER u
					ON
					    u.UserID = gu.UserID
					WHERE 
						GroupID = \''.$this->GroupID.'\'
					AND
						u.RecordStatus = 1	
					ORDER BY u.ClassLevel, u.ClassName, u.ClassNumber
					';
				//debug_r($sql);
				$this->MemberList = $this->returnArray($sql);
				//$GLOBALS['DEBUG']['SQLs'][] = array( $sql, $this->MemberList,mysql_error());
			}
			if ($LoadRule){
			    $CirDescription = $Lang['libms']['SQL']['CirDescription'];
			    $sql = 'Select
					    g.CirculationTypeCode,
					    c.'.$CirDescription.' as CirDescription,
					    g.LimitBorrow,
					    g.LimitRenew,
					    g.LimitReserve,
					    g.ReturnDuration,
					    g.OverDueCharge,
					    g.MaxFine
					    
				    From
					LIBMS_GROUP_CIRCULATION g
				    Left JOIN
					LIBMS_CIRCULATION_TYPE c
				    ON
					g.CirculationTypeCode = c.CirculationTypeCode
				    WHERE 
					    g.GroupID = \''.$this->GroupID.'\'
				    ';
			    
			    $this->RuleList = $this->returnArray($sql);

			    
			}
		}
	}
}//end of class liblibrarymgmt_group

class liblibrarymgmt_group_manage extends libdb {
    
    
	function liblibrarymgmt_group_manage(){
	    parent::libdb();
	    global $eclass_prefix;

	    # database for this module
	    $this->db = $eclass_prefix . "eClass_LIBMS";
	}
    
	function Get_Avaliable_User_List($AddUserID,$IdentityType,$YearClassSelect,$ParentStudentID,$GroupID) {
	    global $Lang;
	    if(!empty($YearClassSelect))
	    	$YearClassSelect_cond = " and u.`ClassName` = '{$YearClassSelect}'";
	   	
		$GroupMemberIDs = $this->Get_Group_Member_List($GroupID);
		if (sizeof($GroupMemberIDs) > 0) {
			$GroupMemberIDs = implode(',',$GroupMemberIDs);
			$GroupCondition = '-1,'.$GroupMemberIDs;
		}
		else;
			$GroupCondition = '-1';

		switch($IdentityType) {
			case "Teaching":
				$Condition = ' u.RecordStatus=\'1\' and u.UserType = \'T\'';
				break;
			case "NonTeaching":
				$Condition = ' u.RecordStatus=\'1\' and u.UserType = \'NT\'';
				break;
			case "Staff":	// including both Teaching and NonTeaching
				$Condition = ' u.RecordStatus=\'1\' and u.UserType IN (\'T\', \'NT\')';
				break;
			case "Student":
				$Condition = ' u.RecordStatus=\'1\' and u.UserType = \'S\' ';
				break;
			case "Parent":
				$Condition = ' u.RecordStatus=\'1\' and u.UserType = \'P\' ';
				break;
			default:
				break;
		}
		$NameField = 'u.'.$Lang['libms']['SQL']['UserNameFeild'];
		if (sizeof($AddUserID) > 0) {
			//var_dump($AddUserID); die;
			$UserQuery = 'and u.UserID not in ('.implode(',',$AddUserID).') ';
		}
		$sql = 'select
			  u.`UserID`,
			  '.$NameField.' as Name,
			  u.`ClassNumber`,
			  u.`ClassName`,
			  u.`ClassLevel`,
			  gu.`GroupID`
			From
			    `LIBMS_USER` as u
			LEFT JOIN 
				`LIBMS_GROUP_USER` AS gu 
				ON gu.`UserID` = u.`UserID`
			
			WHERE
			    '.$Condition.'
			    '.$UserQuery.'
			    and u.`UserID` not in ('.$GroupCondition.')
			    AND u.`RecordStatus` = \'1\'
			    '.$YearClassSelect_cond.'
			ORDER BY u.`ClassLevel`, u.`ClassName`, u.`ClassNumber`, u.`EnglishName` ';
			

		

		$result = $this->returnArray($sql);
		return $result;
	}

	function Get_Group_List() {
		$sql = 'Select
			    r.GroupID,
			    r.GroupTitle,
			    r.GroupCode,
			    r.IsDefault, 
				count(lu.UserID) AS GroupMembers,
				r.GroupType
			From
			    LIBMS_GROUP r 
				LEFT JOIN LIBMS_GROUP_USER gu ON gu.GroupID=r.GroupID
				LEFT JOIN LIBMS_USER lu ON (gu.UserID = lu.UserID And lu.RecordStatus = 1) 
			Group BY
				r.GroupID
			Order by
			    r.GroupTitle';
		
		return $this->returnArray($sql);
	}

	function Check_Group_Name($GroupTitle,$GroupID="") {
		$sql = 'select
				count(1)
			from
				LIBMS_GROUP
			where
				GroupTitle = \''.$this->Get_Safe_Sql_Query($GroupTitle).'\'
				and
				GroupID <> \''.$GroupID.'\'';
		$Result = $this->returnVector($sql);
		//echo $sql; die;
		return ($Result[0] == 0);
	}
	
	function Check_Group_Code($GroupCode,$GroupID="") {
		$sql = 'select
				count(1)
			from
				LIBMS_GROUP
			where
				GroupCode = \''.$this->Get_Safe_Sql_Query($GroupCode).'\'
				and
				GroupID <> \''.$GroupID.'\'';
		$Result = $this->returnVector($sql);
		//echo $sql; die;
		return ($Result[0] == 0);
	}

	function Create_Group($GroupTitle,$GroupCode) {
		$sql = 'insert into LIBMS_GROUP (
							GroupTitle,
							GroupCode,
							DateModified,
							LastModifiedBy
						)
						value (
							\''.$this->Get_Safe_Sql_Query($GroupTitle).'\',
							\''.$this->Get_Safe_Sql_Query($GroupCode).'\',
							now(),
							\''.$_SESSION['UserID'].'\'
						)';
		$Result = $this->db_db_query($sql);
		
		# Default pre-insert all group target [All-Yes] for new group
		/*
		if ($Result){
			$GroupID = $this->db_insert_id();
			$this->Save_Group_Right_Target($GroupID,"",array("All-Yes"),"Target");
		}else
			$GroupID = false;
		*/
		return $GroupID;
	}

	function Rename_Group($GroupID,$GroupTitle) {
		$sql = 'update LIBMS_GROUP set
							GroupTitle = \''.$this->Get_Safe_Sql_Query($GroupTitle).'\',
							LastModifiedBy = \''.$_SESSION['UserID'].'\'
						where
							GroupID = \''.$GroupID.'\'';
		//echo $sql; die;
		return $this->db_db_query($sql);
	}
	
	function Rename_GroupCode($GroupID,$GroupCode) {
		$sql = 'update LIBMS_GROUP set
							GroupCode = \''.$this->Get_Safe_Sql_Query($GroupCode).'\',
							LastModifiedBy = \''.$_SESSION['UserID'].'\'
						where
							GroupID = \''.$GroupID.'\'';
		//echo $sql; die;
		return $this->db_db_query($sql);
	}

	function Add_Group_Member($GroupID,$TargetUserIDs) {
		/*$sql = 'delete from ROLE_MEMBER where GroupID = \''.$GroupID.'\'';
		$Result['ClearOldMembers'] = $this->db_db_query($sql);*/
		//tolog($TargetUserIDs);
		if (sizeof($TargetUserIDs) > 0) {
			$TargetUserIDs = array_values(array_unique($TargetUserIDs));
			//tolog($TargetUserIDs);
			//remove all user group relationship in the user list
			$imploded_TargetUserIDs = "'".implode("','", $TargetUserIDs)."'";
			//tolog($imploded_TargetUserIDs);
			$sql = 'DELETE FROM `LIBMS_GROUP_USER`
					WHERE
					`UserID` in ('.$imploded_TargetUserIDs.')';
			
			//tolog($sql);
			$this->db_db_query($sql);
 			//dump($sql);
 			//$err = mysql_error();
 			//dex($err);
			for ($i=0; $i< sizeof($TargetUserIDs); $i++) {
				if ($TargetUserIDs != "") {
					$sql = 'INSERT INTO `LIBMS_GROUP_USER` (
										GroupID,
										UserID,
										DateModified,
										LastModifiedBy
									)
									value (
										\''.$GroupID.'\',
										\''.$TargetUserIDs[$i].'\',
										Now(),
										\''.$_SESSION['UserID'].'\'
									)';
					//debug_r($sql);
					$Result['InsertGroupMember:'.$TargetUserIDs[$i]] = $this->db_db_query($sql);
				}
			}

			//debug_r($Result);
			return (!in_array(false,$Result));
		}
		else {
			return false;
		}
	}

	function Remove_Group($GroupID) {
		$sql = 'delete from LIBMS_GROUP_RIGHT where GroupID = \''.$GroupID.'\'';
		$Result['DeleteRight'] = $this->db_db_query($sql);

		$sql = 'delete from LIBMS_GROUP_USER where GroupID = \''.$GroupID.'\'';
		$Result['DeleteMember'] = $this->db_db_query($sql);

		$sql = 'delete from LIBMS_GROUP where GroupID = \''.$GroupID.'\'';
		$Result['DeleteROLE'] = $this->db_db_query($sql);

		return (!in_array(false,$Result));
	}

	function Remove_Group_Member($GroupID,$TargetUserIDs) {
		if (sizeof($TargetUserIDs) > 0) {
			$UserQuery = implode(',',$TargetUserIDs);

			$sql = 'delete from LIBMS_GROUP_USER
				where
					GroupID = \''.$GroupID.'\'
					and
					UserID in ('.$UserQuery.')';

			return $this->db_db_query($sql);
		}
		else
			return false;
	}
	
	function Get_Group_Period_Date($GroupID)
	{
		$sql = 'Select
					   if(StartDate="0000-00-00", "", StartDate) As StartDate, if(EndDate="0000-00-00", "", EndDate) As EndDate, SameDayReturnNotAllowed, GroupID					    
				From
					LIBMS_GROUP_PERIOD
				 WHERE 
					 GroupID = \''.$GroupID.'\'
				    ';
			    
		$rows = $this->returnResultSet($sql);
		
		return $rows[0];
	}

	function Save_Group_Period($GroupID,$StartDate,$EndDate,$SameDayReturn) {
	    // todo:
	    // delete old group right
	    $sql = 'delete from LIBMS_GROUP_PERIOD where GroupID = \''.$GroupID.'\'';
	    $Result['DeleteGroupPeriod'] = $this->db_db_query($sql);
	    
	    if ($StartDate=="")
	    {
	    	$StartDate = "NULL";
	    }
	    
	    if ($EndDate=="")
	    {
	    	$EndDate = "NULL";
	    }
	    
	    if ($SameDayReturn =='true')
	    {
	    	$SameDayReturn = 1;
	    }else 
	    	$SameDayReturn = "NULL";

	    // insert new group right selection
	    $sql = "insert into 
			LIBMS_GROUP_PERIOD
			(`GroupID`,`StartDate`,`EndDate`,`SameDayReturnNotAllowed`,`DateModified`,`LastModifiedBy`)
			values 
			('{$GroupID}','{$StartDate}','{$EndDate}','{$SameDayReturn}',now(),'{$_SESSION['UserID']}')";
	    
	    $Result['InsertGroupPeriod'] = $this->db_db_query($sql);
	    
	    
	    return (!in_array(false,$Result));
	}

	function Save_Group_Right($GroupID,$GroupRight) {
	    // todo:
	    // delete old group right
	    $sql = 'delete from LIBMS_GROUP_RIGHT where GroupID = \''.$GroupID.'\'';
	    $Result['DeleteGroupRight'] = $this->db_db_query($sql);

	    // insert new group right selection
	    for ($i=0; $i< sizeof($GroupRight); $i++) {
		    $sql = "insert into 
				LIBMS_GROUP_RIGHT
				(`GroupID`,`Section`,`Function`,`IsAllow`,`DateModified`,`LastModifiedBy`)
				values 
				('{$GroupID}','{$GroupRight[$i]['Section']}','{$GroupRight[$i]['Function']}','1',now(),'{$_SESSION['UserID']}')";
		    
		    //var_dump($sql);
		    $Result['InsertGroupRight:'.$GroupRight[$i]] = $this->db_db_query($sql);
	    }
	    
	    
	    return (!in_array(false,$Result));
	}
	
	function Save_Class_Mgt_Group_Right($GroupID,$GroupRight) {
	    // todo:
	    // delete old group right
	    $sql = 'delete from LIBMS_CLASS_MANAGEMENT_GROUP_RIGHT where GroupID = \''.$GroupID.'\'';
	    $Result['DeleteGroupRight'] = $this->db_db_query($sql);

	    // insert new group right selection
	    for ($i=0; $i< sizeof($GroupRight); $i++) {
		    $sql = "insert into 
				LIBMS_CLASS_MANAGEMENT_GROUP_RIGHT
				(`GroupID`,`Section`,`Function`,`IsAllow`,`DateModified`,`LastModifiedBy`)
				values 
				('{$GroupID}','{$GroupRight[$i]['Section']}','{$GroupRight[$i]['Function']}','1',now(),'{$_SESSION['UserID']}')";
		    
		    //var_dump($sql);
		    $Result['InsertGroupRight:'.$GroupRight[$i]] = $this->db_db_query($sql);
	    }
	    
	    
	    return (!in_array(false,$Result));
	}
	
	function Save_Group_Rule($GroupID,$GroupRules) {

	    // delete old group right
	    $sql = 'delete from LIBMS_GROUP_CIRCULATION where GroupID = \''.$GroupID.'\'';
	    $Result['DeleteGroupRule'] = $this->db_db_query($sql);
		
	    //dump($GroupRules);
	    // insert new group right selection
	    
	    foreach($GroupRules as $key => $rule){
			if ($rule['isset'] == '1'){
			    $sql = "insert into 
					LIBMS_GROUP_CIRCULATION
					(`GroupID`,`CirculationTypeCode`,`LimitBorrow`,`LimitRenew`,`LimitReserve`,`ReturnDuration`,`OverDueCharge`,`MaxFine`)
					values 
					('{$GroupID}','{$key}','{$rule['LimitBorrow']}','{$rule['LimitRenew']}','{$rule['LimitReserve']}','{$rule['ReturnDuration']}','{$rule['OverDueCharge']}','{$rule['MaxFine']}')";
			    $Result['InsertGroupRight:'.$GroupRight[$i]] = $this->db_db_query($sql);
			    //echo($sql);
			    //echo (mysql_error());
			}
	    }
	    
	    return (!in_array(false,$Result));
	}

	function Search_User($SearchValue,$AddUserID) {
		$CurrentYearTermID = getCurrentAcademicYearAndYearTerm();
		$CurrentAcademicYearID = $CurrentYearTermID['AcademicYearID'];
		$NameField = 'CONCAT(u.`ChineseName`," ",u.`EnglishName`)';
		$UserCondition = (trim($AddUserID) == "")? "": ' and u.UserID not in ('.$AddUserID.') ';
		$SearchValue = $this->Get_Safe_Sql_Query($SearchValue);

		$this->With_Nolock_Trans();
		$sql = 'select
              u.UserID,
              '.$NameField.' as Name,
							CASE
								WHEN u.ClassName IS NULL THEN "NA"
								ELSE u.ClassName
							END as ClassName,
							CASE
								WHEN u.ClassNumber IS NULL THEN "NA"
								ELSE u.ClassNumber
							END as ClassNumber,
							CASE
								WHEN u.UserType = \'S\' then \'Student\'
								WHEN u.UserType = \'P\' then \'Parent\'
								WHEN u.UserType = \'T\'  then \'Teaching\'
								WHEN u.UserType = \'NT\' then \'Non Teaching\'
								ELSE \'Alumni\'
							END as UserType
					  from
					  	`LIBMS_USER` u
					  where
					  	u.RecordStatus = \'1\'
              '.$UserCondition.'
					  	and (
					  		u.UserLogin like \'%'.$SearchValue.'%\'
					  		OR
					  		u.EnglishName like \'%'.$SearchValue.'%\'
					  		OR
					  		u.UserEmail like \'%'.$SearchValue.'%\'
					  		OR
					  		u.ChineseName like \'%'.$SearchValue.'%\'
					  	)
					  	ORDER BY u.ClassLevel, u.ClassName, u.ClassNumber
					  	';
		//dump($sql);
		//dump(mysql_error());
		return $this->returnArray($sql);
	}

	function Get_Identity_Type($TargetUserID) {
		$sql = 'select
							CASE
								WHEN u.UserType = \'S\' then \'Student\'
								WHEN u.UserType = \'P\' then \'Parent\'
								WHEN u.UserType = \'T\' then \'Teaching\'
								WHEN u.UserType = \'NT\' then \'NonTeaching\'
								ELSE \'Alumni\'
							END as IdentityType
						From
							INTRANET_USER as u
						Where
							UserID = \''.$TargetUserID.'\'';
		$Result = $this->returnVector($sql);
		return $Result[0];
	}

	function Get_Group_Member_List($GroupID) {
		$sql = 'select
							UserID
						from
							LIBMS_GROUP_USER
						where
							GroupID = \''.$GroupID.'\'';
		return $this->returnVector($sql);
	}
	
	function Get_Group_Member_For_Export($GroupID) {
	    global  $Lang;
	    $NameField = $Lang['libms']['SQL']['UserNameFeild'];
	    $sql = 'select
                            u.UserID,
                            u.'.$NameField.',
                            u.BarCode,
                            u.ClassName,
                            u.ClassNumber,
                            u.UserType
						from
							LIBMS_GROUP_USER gu
						inner join LIBMS_USER u on gu.UserID = u.UserID
						where
							gu.GroupID = \''.$GroupID.'\'
                            AND u.RecordStatus = 1
						order by '.$Lang['libms']['SQL']['UserNameFeild'];
	    return $this->returnArray($sql);
	}
	
	function Get_Member_Class_Info($MemberID,$UserType) {
		if ($UserType == 2) // student
			$TableName = ' YEAR_CLASS_USER ';
		else
			$TableName = ' YEAR_CLASS_TEACHER ';

		$sql = 'select
							ClassTitleEN,
							ClassTitleB5
					 ';
		if ($UserType == 2)
			$sql .= ', ClassNumber ';
		else
			$sql .= ', \'NA\' as ClassNumber ';
		$sql .= 'from
							'.$TableName.' as a
							inner join
							YEAR_CLASS as yc
							on a.UserID = \''.$MemberID.'\' and yc.AcademicYearID = \''.GET_CURRENT_ACADEMIC_YEAR_ID().'\' and a.YearClassID = yc.YearClassID
							inner join
							YEAR as y
							on yc.YearID = y.YearID
						order by
							y.sequence, yc.sequence
					 ';
		$ClassInfo = $this->returnArray($sql);

		if (sizeof($ClassInfo) == 0) {
			return array("ClassTitleEN" => "NA", "ClassTitleB5" => "NA", "ClassNumber" => "NA");
		}
		else {
			for ($i=0; $i< sizeof($ClassInfo); $i++) {
				if ($i==0) {
					$Return["ClassTitleEN"] = $ClassInfo[$i]["ClassTitleEN"];
					$Return["ClassTitleB5"] = $ClassInfo[$i]["ClassTitleB5"];
					$Return["ClassNumber"] = $ClassInfo[$i]["ClassNumber"];
				}
				else {
					$Return["ClassTitleEN"] .= ", ".$ClassInfo[$i]["ClassTitleEN"];
					$Return["ClassTitleB5"] .= ", ".$ClassInfo[$i]["ClassTitleB5"];
				}
			}

			return $Return;
		}
	}
	
	function Add_Class_Mgt_Group_Member($GroupID,$TargetUserIDs) {
		/*$sql = 'delete from ROLE_MEMBER where GroupID = \''.$GroupID.'\'';
		$Result['ClearOldMembers'] = $this->db_db_query($sql);*/
		//tolog($TargetUserIDs);
//		if (sizeof($TargetUserIDs) > 0) {
			$TargetUserIDs = array_values(array_unique($TargetUserIDs));
			//tolog($TargetUserIDs);
			//remove all user group relationship in the user list
			$imploded_TargetUserIDs = "'".implode("','", $TargetUserIDs)."'";
			//tolog($imploded_TargetUserIDs);
			$sql = 'DELETE FROM `LIBMS_CLASS_MANAGEMENT_GROUP_USER`
					WHERE
					 GroupID = '.$GroupID;
			
			//tolog($sql);
			$this->db_db_query($sql);
 			//dump($sql);
 			//$err = mysql_error();
 			//dex($err);
			for ($i=0; $i< sizeof($TargetUserIDs); $i++) {
				if ($TargetUserIDs != "") {
					$sql = 'INSERT INTO `LIBMS_CLASS_MANAGEMENT_GROUP_USER` (
										GroupID,
										UserID,
										DateModified,
										LastModifiedBy
									)
									value (
										\''.$GroupID.'\',
										\''.$TargetUserIDs[$i].'\',
										Now(),
										\''.$_SESSION['UserID'].'\'
									)';
					//debug_r($sql);
					$Result['InsertGroupMember:'.$TargetUserIDs[$i]] = $this->db_db_query($sql);
				}
			}

			//debug_r($Result);
			return (!in_array(false,$Result));
//		}
//		else {
//			return false;
//		}
	}
	
	function Change_Class_Mgt_Location($GroupID,$GroupTitle) {
		$sql = 'update LIBMS_CLASS_MANAGEMENT_GROUP set
							LocationCode = \''.$this->Get_Safe_Sql_Query($GroupTitle).'\',
							LastModifiedBy = \''.$_SESSION['UserID'].'\',
							DateModified = NOW()
						where
							GroupID = \''.$GroupID.'\'';
		//echo $sql; die;
		return $this->db_db_query($sql);
	}
}//end of class liblibrarymgmt_group_manage


}

?>
