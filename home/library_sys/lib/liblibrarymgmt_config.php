<?
$lmsConfigAry = array();
$lmsConfigAry['maxLengthAry']['BookTitle'] = 64;
$lmsConfigAry['maxLengthAry']['Distributor'] = 64;
$lmsConfigAry['maxLengthAry']['ISSN'] = 64;
$lmsConfigAry['maxLengthAry']['PeriodicalCode'] = 128;
$lmsConfigAry['maxLengthAry']['PeriodicalRemarks'] = 128;
$lmsConfigAry['maxLengthAry']['Publisher'] = 32;
$lmsConfigAry['maxLengthAry']['PublishPlace'] = 64;
$lmsConfigAry['maxLengthAry']['PurchaseByDepartment'] = 32;
$lmsConfigAry['maxLengthAry']['PurchaseNote'] = 64;
$lmsConfigAry['maxLengthAry']['RemarkInternal'] = 128;
$lmsConfigAry['maxLengthAry']['RemarkToUser'] = 128;
$lmsConfigAry['maxLengthAry']['Series'] = 64;
$lmsConfigAry['maxLengthAry']['SeriesNum'] = 32;
?>