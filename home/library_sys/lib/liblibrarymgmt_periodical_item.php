<?php
/*
 * 
 */ 
if (!defined("LIBLIBRARYMGMT_PERIODICAL_ITEM_DEFINED"))         // Preprocessor directives
{
	define("LIBLIBRARYMGMT_PERIODICAL_ITEM_DEFINED", true);
	
	include_once($PATH_WRT_ROOT.'includes/libdbobject.php');
	class liblms_periodical_item extends libdbobject {
		var $liblms;
		
		var $itemId;
		var $periodicalBookId;
		var $orderId;
		var $quantity;
		var $issue;
		var $publishDate;
		var $details;
		var $registrationDate;
		var $registrationBy;
		var $recordStatus;
		var $dateInput;
		var $inputBy;
		var $dateModified;
		var $lastModifiedBy;
		
		public function __construct($objectId='') {
			$this->liblms = new liblms();
			
			parent::__construct($this->liblms->db.'.LIBMS_PERIODICAL_ITEM', 'ItemID', $this->returnFieldMappingAry(), $objectId);
		}
		
		function setItemId($val) {
			$this->itemId = $val;
		}
		function getItemId() {
			return $this->itemId;
		}
		
		function setPeriodicalBookId($val) {
			$this->periodicalPeriodicalBookId = $val;
		}
		function getPeriodicalBookId() {
			return $this->periodicalPeriodicalBookId;
		}
		
		function setOrderId($val) {
			$this->orderId = $val;
		}
		function getOrderId() {
			return $this->orderId;
		}
		
		function setQuantity($val) {
			$this->quantity = $val;
		}
		function getQuantity() {
			return $this->quantity;
		}
		
		function setIssue($val) {
			$this->issue = $val;
		}
		function getIssue() {
			return $this->issue;
		}
		
		function setPublishDate($val) {
			$this->publishDate = $val;
		}
		function getPublishDate() {
			return $this->publishDate;
		}
		
		function setDetails($val) {
			$this->details = $val;
		}
		function getDetails() {
			return $this->details;
		}
		
		function setRegistrationDate($val) {
			$this->registrationDate = $val;
		}
		function getRegistrationDate() {
			return $this->registrationDate;
		}
		
		function setRegistrationBy($val) {
			$this->registrationBy = $val;
		}
		function getRegistrationBy() {
			return $this->registrationBy;
		}
		
		function setRecordStatus($val) {
			$this->recordStatus = $val;
		}
		function getRecordStatus() {
			return $this->recordStatus;
		}
		
		function setDateInput($val) {
			$this->dateInput = $val;
		}
		function getDateInput() {
			return $this->dateInput;
		}
		
		function setInputBy($val) {
			$this->inputBy = $val;
		}
		function getInputBy() {
			return $this->inputBy;
		}
		
		function setDateModified($val) {
			$this->dateModified = $val;
		}
		function getDateModified() {
			return $this->dateModified;
		}
		
		function setLastModifiedBy($val) {
			$this->lastModifiedBy = $val;
		}
		function getLastModifiedBy() {
			return $this->lastModifiedBy;
		}
		
		function returnFieldMappingAry() {
			$fieldMappingAry = array();
			$fieldMappingAry[] = array('ItemID', 'int', 'setItemId', 'getItemId');
			$fieldMappingAry[] = array('PeriodicalBookID', 'int', 'setPeriodicalBookId', 'getPeriodicalBookId');
			$fieldMappingAry[] = array('OrderID', 'int', 'setOrderId', 'getOrderId');
			$fieldMappingAry[] = array('Quantity', 'int', 'setQuantity', 'getQuantity');
			$fieldMappingAry[] = array('Issue', 'str', 'setIssue', 'getIssue');
			$fieldMappingAry[] = array('PublishDate', 'date', 'setPublishDate', 'getPublishDate');
			$fieldMappingAry[] = array('Details', 'str', 'setDetails', 'getDetails');
			$fieldMappingAry[] = array('RegistrationDate', 'date', 'setRegistrationDate', 'getRegistrationDate');
			$fieldMappingAry[] = array('RegistrationBy', 'int', 'setRegistrationBy', 'getRegistrationBy');
			$fieldMappingAry[] = array('RecordStatus', 'int', 'setRecordStatus', 'getRecordStatus');
			$fieldMappingAry[] = array('DateInput', 'date', 'setDateInput', 'getDateInput');
			$fieldMappingAry[] = array('InputBy', 'int', 'setInputBy', 'getInputBy');
			$fieldMappingAry[] = array('DateModified', 'date', 'setDateModified', 'getDateModified');
			$fieldMappingAry[] = array('LastModifiedBy', 'int', 'setLastModifiedBy', 'getLastModifiedBy');
			return $fieldMappingAry;
		}

				
		
		
		function newRecordBeforeHandling() {
			$this->setDateInput('now()');
			$this->setInputBy($_SESSION['UserID']);
			$this->setDateModified('now()');
			$this->setLastModifiedBy($_SESSION['UserID']);
			return true;
		}
		
		function updateRecordBeforeHandling() {
			$this->setDateModified('now()');
			$this->setLastModifiedBy($_SESSION['UserID']);
			return true;
		}
		
//		function deleteRecordBeforeHandling() {
//			
//			$tmpAry = array();
//		    $tmpAry['BookID'] = $this->getPeriodicalBookId();
//		    $tmpAry['OrderID'] = $this->getOrderId();
//		    $tmpAry['Quantity'] = $this->getQuantity();
//		    $tmpAry['Issue'] = $this->getIssue();
//		    $tmpAry['PublishDate'] = $this->getPublishDate();
//		    $tmpAry['Details'] = $this->getDetails();
//		    $tmpAry['RegistrationDate'] = $this->getRegistrationDate();
//		    $tmpAry['RegistrationBy'] = $this->getRegistrationBy();
//		    $tmpAry['DateInput'] = $this->getDateInput();
//		    $tmpAry['InputBy'] = $this->getInputBy();
//		    $tmpAry['DateModified'] = $this->getDateModified();
//		    $tmpAry['LastModifiedBy'] = $this->getLastModifiedBy();
//		    
//		    $liblog = new liblog();
//		    return $liblog->INSERT_LOG('LMS', 'deletePeriodicalItem', $liblog->BUILD_DETAIL($tmpAry), 'LIBMS_PERIODICAL_ITEM', $this->getOrderId());
//		}
	}
}
?>