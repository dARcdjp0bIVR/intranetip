<?php
// using: 
######################################################################################
#									Change Log 
######################################################################################
#
#   20200515 Cameron: retrieve ClassName and ClasssNumber in Get_Avaliable_User_List() [case #E81448]
#	20150209 Ryan : ej DM#521 - Change wording select Class to all classes  
#	20150203 Ryan : Training Feedback  - Add Button to suspend/activate users 
#										 [Case Number : 2015-0203-1543-49054]
#	20141127 Ryan : Modified Get_Group_Period_Date(), Save_Group_Period(), add "SameReutenDay" option
#	20141201 Ryan : eLibrary plus pending developments Item 204
#
######################################################################################

if (!defined("liblibrarymgmt_suspend_DEFINED"))                     // Preprocessor directive
{
define("liblibrarymgmt_suspend_DEFINED", true);


if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    if (function_exists ("array_walk_recursive"))
    {
    	array_walk_recursive($gpc, "magicQuotes_awStripslashes");
    }
}
 

include_once("{$_SERVER['DOCUMENT_ROOT']}/includes/libdb.php");
include_once("{$_SERVER['DOCUMENT_ROOT']}/includes/libuser.php");
include_once("{$_SERVER['DOCUMENT_ROOT']}/includes/lib.php");
include_once("{$_SERVER['DOCUMENT_ROOT']}/includes/pdump/pdump.php");
include_once("{$_SERVER['DOCUMENT_ROOT']}/includes/form_class_manage.php");
include_once("{$_SERVER['DOCUMENT_ROOT']}/lang/libms.lang.$intranet_session_language.php");

class liblibrarymgmt_suspend extends libdb {
	
	private $Mode; // 0 = Suspend, 1 = Activate 
	var	$MemberList;

	function liblibrarymgmt_suspend() {
		global $Lang;
		global $eclass_prefix;
		
		# database for this module
		$this->db = $eclass_prefix . "eClass_LIBMS";
		//parent::libdb();
		$this->getMemberList();
		//$GLOBALS['DEBUG']['SQLs'][] = array( $sql, $this->MemberList,mysql_error());
	}
	private function getMemberList(){
		global $Lang;
		# Get Suspended Member List 
		$NameField = $Lang['libms']['SQL']['UserNameFeild'];
		
		$sql = 'Select
				u.UserID,						
				u.'.$NameField.' as MemberName,
				u.BarCode,
				u.UserType,
				u.ClassNumber,
				u.ClassName,
				su.'.$NameField.' as SuspendedBy,
				u.SuspendedDate
				
			From
			    LIBMS_USER u
			LEFT JOIN
				LIBMS_USER su on su.UserID = u.SuspendedBy
			WHERE 
				u.Suspended = 1
			ORDER BY u.UserType, u.ClassLevel, u.ClassName, u.ClassNumber
			';
//		debug_r($sql);
		$this->MemberList = $this->returnArray($sql);
	}  
	
	
	function SuspendedUsers($Users){
		// Suspend Mode 
		$this->Mode = 0;
		// Suspend users  
		$this->Action($Users);
	}
	
	function ActivateUsers($Users){
		// Activate Mode 
		$this->Mode = 1;
		// Activateusers  
		$this->Action($Users);
	}
	
	private function Action($Users){
		if(!is_array($Users)){
			$Users = array($Users);
		}
		
		$UserIDs = '';
		foreach($Users as $UserID){
			$UserIDs .= '\''.$UserID.'\',';
		}
		$UserIDs = rtrim($UserIDs,',');
		
		$this->Start_Trans();
		switch ($this->Mode){
			case 0:
				// Suspend
				$result = $this->Suspend($UserIDs);
			break;
			case 1:			
				// Activate
				$result = $this->Activate($UserIDs);
			break;			
			default:
			break;
		}
		
		if($result){
			$this->Commit_Trans();
			$this->getMemberList(); // Refresh MemberList
		}
		else{
			$this->RollBack_Trans();
		}
		return $result;
	}
	
	private function Suspend($UserIDs){
		$sql = 'update LIBMS_USER
				set Suspended = 1, SuspendedBy = '.$_SESSION['UserID'].', SuspendedDate = now()
				where UserID in ('.$UserIDs.')';
		return $this->db_db_query($sql);					
	}
	
	private function Activate($UserIDs){
		$sql = 'update LIBMS_USER
				set Suspended = NULL, SuspendedBy = '.$_SESSION['UserID'].', SuspendedDate = now()
				where UserID in ('.$UserIDs.')';
		return $this->db_db_query($sql);					
	}  
}//end of class liblibrarymgmt_suspend

class liblibrarymgmt_suspend_manage extends libdb {
    
    
	function liblibrarymgmt_suspend_manage(){
	    parent::libdb();
	    global $eclass_prefix;

	    # database for this module
	    $this->db = $eclass_prefix . "eClass_LIBMS";
	}
 

	function Get_Group_Member_Form($Group) {
		global $Lang,$linterface;
//		$x = '<div class="content_top_tool">
//            <div class="Conntent_tool">
//            	<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Group_Add_Member_Form(\''.$Group->GroupID.'\'); return false;" class="new thickbox" title="'.$Lang['libms']['GroupManagement']['AddUser'].'"> '.$Lang['libms']['GroupManagement']['New'].' </a>
//            	<!--<a href="#" class="import"> Import</a>
//            	<a href="#" class="export"> Export</a>-->
//            </div>
//            <div class="Conntent_search">
//              <!--<input name="input" type="text"/>-->
//            </div>
//            <br style="clear:both" />
//          </div>

        $x =$linterface->GET_NAVIGATION2_IP25($Lang["libms"]["UserSuspend"]["SuspendedList"]).' <div class="table_board">
				<form name="ListForm" id="ListForm">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td valign="bottom"><div class="table_filter"></div></td>
					    <td valign="bottom">
					    	<div class="common_table_tool">
					    		<a href="#" class="tool_delete" title="'.$Lang['libms']['GroupManagement']['Delete'].'" onclick="DeleteFromList();">'.$Lang['libms']['GroupManagement']['Delete'].'</a>
					    	</div>
					    </td>
					  </tr>
					</table>
					<table class="common_table_list">
					  <thead>
					    <tr>
					      <th class="num_check">#</th>
					      <th> '.$Lang['libms']['GroupManagement']['Name'].'</th> 
					      <th> '.$Lang['libms']['CirculationManagement']['user_barcode'].'</th>					      		
					      <th> '.$Lang['libms']['GroupManagement']['ClassName'].'</th>
					      <th> '.$Lang['libms']['GroupManagement']['ClassNumber'].'</th>
					      <th> '.$Lang['libms']['GroupManagement']['UserType'].'</th>
					      <th> '.$Lang['libms']['UserSuspend']['SuspendedBy'].'</th>
					      <th> '.$Lang['libms']['UserSuspend']['SuspendedDate'].'</th>
					      <th class="num_check" ><input type="checkbox" name="CheckAll" id="CheckAll" onclick="Set_Checkbox_Value(\'UserIDs[]\',this.checked);"/></th>
					      </tr>
					  </thead>
					  <tbody>';
	   $iMax = sizeof($Group->MemberList);
	    if($iMax == 0)
	    {
	    	$x.= '<tr><td align="center" colspan="100%">'.$Lang['libms']['NoRecordAtThisMoment'].'</td></tr>';
	    }else{
			for ($i=0; $i<$iMax ; $i++) {
	//// --> Updated by Cameron
				$x .= '		<tr>
							      <td>'.($i+1).'</td> 
							      <td>'.($Group->MemberList[$i]['MemberName']?$Group->MemberList[$i]['MemberName']:'--').'</td> 
							      <td>'.($Group->MemberList[$i]['BarCode']? $Group->MemberList[$i]['BarCode']:'--').'</td>
							      <td>'.($Group->MemberList[$i]['ClassName']? $Group->MemberList[$i]['ClassName']:'--').'</td>
							      <td>'.($Group->MemberList[$i]['ClassNumber']? $Group->MemberList[$i]['ClassNumber']:'--').'</td>
							      <td>'.($Group->MemberList[$i]['UserType']? $Group->MemberList[$i]['UserType']:'--').'</td>
							      <td>'.($Group->MemberList[$i]['SuspendedBy']? $Group->MemberList[$i]['SuspendedBy']:'--').'</td>
							      <td>'.($Group->MemberList[$i]['SuspendedDate']? $Group->MemberList[$i]['SuspendedDate']:'--').'</td>
							      <td><input type="checkbox" name="UserIDs[]" id="UserIDs[]" value="'.$Group->MemberList[$i]['UserID'].'"/></td>
							    </tr>';
	
			}
	    }
		$x .= ' 	</tbody>
				</table>
			  </form>';
		return $x;
	}
	
	  function Get_YearClass_Selection($show=0,$value=''){
	  	global $Lang;
			// get class list
		$CurrentYearTermID = getCurrentAcademicYearAndYearTerm();
		$CurrentAcademicYearID = $CurrentYearTermID['AcademicYearID'];
		$fcm = new form_class_manage();
		$style = $show ? '':'style="display:none;"';
		$YearClassList = $fcm->Get_Class_List_By_Academic_Year($CurrentAcademicYearID);
		$x .= '<select name="YearClassSelect" id="YearClassSelect" class="formtextbox" '.$style .' onchange="UpdateUserList();">';
		$x .= '<option value="">'.$Lang["libms"]["SearchUser"]["AllClasses"].'</option>';
		  for ($i=0; $i< sizeof($YearClassList); $i++) {
		  	if ($YearClassList[$i]['YearID'] != $YearClassList[$i-1]['YearID']) {
			if ($i == 0)
				$x .= '<optgroup label="'.$YearClassList[$i]['YearName'].'">';
			else {
				$x .= '</optgroup>';
				$x .= '<optgroup label="'.$YearClassList[$i]['YearName'].'">';
			}
		}
		$selected ='';
		if($value == $YearClassList[$i]['ClassTitleEN'] && $value != ''){
			$selected = 'selected';
		}
		//$x .= '<option value="'.Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']).'">'.Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']).'</option>';
		$x .= '<option value="'.$YearClassList[$i]['ClassTitleEN'].'" '.$selected.'>'.Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']).'</option>';
		
		if ($i == (sizeof($YearClassList)-1))
			$x .= '</optgroup>';
			  }
		  $x .= '</select>';
		  return $x;
	  } 
	  
	function Get_Add_Group_Member_Form($Suspend,$UserType='Teaching') {
		global $Lang;
		$x ='<form name="AddMemberForm" id="AddMemberForm">';
		$x .='<table width="70%" align="center" border="0" cellspacing="0" cellpadding="5">
				<tbody><tr>
						<td width="50%" bgcolor="#EEEEEE" >
							<br/><strong>'.$Lang["libms"]["UserSuspend"]["UserList"].'</strong><br/><br/>
							'.$Lang["libms"]["GroupManagement"]["UserType"].':
							<select name="IdentityType" id="IdentityType" onchange="ChangeUsertype();">
								<option value="Teaching">'.$Lang['libms']['GroupManagement']['TeachingStaff'].'</option>
								<option value="NonTeaching">'.$Lang['libms']['GroupManagement']['SupportStaff'].'</option>
								<option value="Student">'.$Lang['libms']['GroupManagement']['Student'].'</option>
								<option value="Parent">'.$Lang['libms']['GroupManagement']['Parent'].'</option>
							</select>';
						$x.= $this->Get_YearClass_Selection($show=false);	
						$x.='</td>
						<td width="40"></td>
						<td width="50%" bgcolor="#EFFEE2" class="steptitletext">'.$Lang["libms"]["UserSuspend"]["thisSelectedUser"].'</td>
					</tr>
					<tr>
					<td bgcolor="#EEEEEE" align="center">
						<div id="AvalUserLayer">';
						$x .= $this->Get_User_Selection($this->Get_Avaliable_User_List($Suspend,$UserType,$YearClass));
					$x.=' 
						</div>
						<input type="hidden" name="SelectedUsers" id="SelectedUsers">
					<span class="tabletextremark">'.$Lang["libms"]["FormClassMapping"]["CtrlMultiSelectMessage"].'</span>
					</td>
					<td align="center">
						<input name="AddAll" onclick="AddAllToSuspended();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value=">>" style="width:40px;" title="LANGADDALL">
						<input name="Add" onclick="AddToSuspended();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value=">" style="width:40px;" title="LANGSELECTED">
						<p></p>
						<input name="Remove" onclick="RemoveFromSuspended();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="<" style="width:40px;" title="LANGREMOVED">
						<input name="RemoveAll" onclick="RemoveAllFromSuspended();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="<<" style="width:40px;" title="LANGADDALL">
					</td>
					<td bgcolor="#EFFEE2" id="SelectedUserCell" align="center">
						<select name="AddUserID[]" id="AddUserID[]" class="AddUserID" size="10" style="width:99%" multiple="true">
						</select>
						<span class="tabletextremark">'.$Lang["libms"]["FormClassMapping"]["CtrlMultiSelectMessage"].'</span>						
					</td>
				</tr>'.
//				<tr>
//					<td width="50%" bgcolor="#EEEEEE">LANGOR<br>LANGSEARCH<br>
//					<!--<div class="Conntent_search" style="float:left;">-->
//					<div style="float:left;">
//						<input type="text" id="UserSearch" name="UserSearch" value="" autocomplete="off" class="ac_input">
//					</div>
//				</td>
//					<td>&nbsp;</td>
//				</tr>
				'<tr>
					<td style="text-align:center">
						&nbsp;
					</td>
					<td style="text-align:center">
						<input type="button" name="SubmitBtn" id="SubmitBtn" class="formbutton_v30 print_hide " onclick="SuspendUsers();" value="'.$Lang["libms"]["UserSuspend"]["Suspend"].'">
					</td>
					<td style="text-align:center">
						&nbsp;
					</td>
				 </tr>
			</tbody>
		</table></form>';

	return $x;
	}
	
	function Get_Avaliable_User_List($SuspenedList,$UserType,$YearClassSelect) {
		global $Lang;
		
		# Get Suspended Member List 
		$NameField = $Lang['libms']['SQL']['UserNameFeild'];
		$extraFields = '';
		
		 if(!empty($YearClassSelect))
	    	$YearClassSelect_cond = " and u.ClassName = '{$YearClassSelect}'";
	    	
		# UserType Condition
		switch($UserType) {
			case "Teaching":
				$Condition = 'AND u.RecordStatus=\'1\' and u.UserType = \'T\'';
				break;
			case "NonTeaching":

				$Condition = 'AND u.RecordStatus=\'1\' and u.UserType = \'NT\'';
				break;
			case "Student":
				$Condition = 'AND u.RecordStatus=\'1\' and u.UserType = \'S\' ';
                $extraFields = ",u.ClassName, u.ClassNumber ";
				break;
			case "Parent":
				$Condition = 'AND u.RecordStatus=\'1\' and u.UserType = \'P\' ';
				break;
			default:
				break;
		}
		
		# Exclude Condition (Supended user) 
		$notin = '';
		foreach($SuspenedList->MemberList as $user){
			$notin .= '\''.$user['UserID'].'\',';
		}
		$notin = rtrim($notin,',');
		
		if($notin != ''){
			$cond = '		AND
				u.UserID not in ('.$notin.')';
		}

		# Get User List 
		$sql = 'Select
				u.UserID,						
				u.'.$NameField.' as MemberName'.$extraFields.'
			From
			    LIBMS_USER u
			WHERE 
				u.Suspended is NULL
			'.$cond.' 
			'.$Condition.' '.$YearClassSelect_cond.' 
			ORDER BY u.ClassLevel, u.ClassName, u.ClassNumber;
			';
		$rs = $this->returnArray($sql);
		if ($UserType == 'Student') { 
		    foreach((array)$rs as $k=>$_rs) {
		        if ($_rs['ClassName'] != '' && $_rs['ClassNumber'] != '') {
		            $rs[$k]['MemberName'] = $_rs['ClassName'] . '-'. $_rs['ClassNumber'] . ' ' . $_rs['MemberName']; 
		        }
		    }
		}
		return $rs;
	}
	
	function Get_User_Selection($UserList){
		$x = '<select name="AvalUserList[]" class="AvalUserList" id="AvalUserList[]" size="10" style="width:99%" multiple="true">'."\n";
		foreach($UserList as $User){
			$x .= '<option value="'.$User['UserID'].'">'.$User['MemberName'].'</option>'."\n";
		}
		$x .= '</select>';
		return $x;
	}
	
	
}//end of class liblibrarymgmt_suspend_manage


}

?>
