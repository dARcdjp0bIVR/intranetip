<?
if (empty($argv[1])){
	echo "$argv[0] <TTF fonts file>\n";
	exit;
}

include_once('tcpdf.php');

$pdf = new TCPDF();
echo $pdf->addTTFfont($argv[1]);
?>
