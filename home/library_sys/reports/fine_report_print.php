<?php
#using: Henry

/***************************************
 * Date: 	2014-05-13 (Henry)
 * Details: Added school name at top of the page
 * Date: 	2013-10-08 (Yuen)
 * Details: Create this page
 ***************************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['overdue report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$libms = new liblms();
$linterface = new interface_html();

$reportPeriodDisplay = $StartDate  . ' ' . $Lang['General']['To']  . ' ' .  $EndDate;

$schoolName =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];

$displayTable = '';
$displayTable .= '<table width="98%" border="0" cellspacing="0" cellpadding="5" valign="top">';
$displayTable .= '<tr>';
$displayTable .= '<td align="right" class="print_hide">';
$displayTable .= $linterface->GET_BTN($button_print, "button","javascript:window.print()");
$displayTable .= '</td>';
$displayTable .= '</tr>';
$displayTable .= '<tr><td>'.$schoolName.'</td></tr>'; 
$displayTable .= '<tr>';
$displayTable .= '<td>';
$displayTable .= '<center><h2>' .$Lang["libms"]["reporting"]["PageReportingFine"] . '</h2></center>';
$displayTable .= '</td>';
$displayTable .= '</tr>';
$displayTable .= '<tr>';
$displayTable .= '<td align="right" class="print_hide">';
$displayTable .= $Lang['General']['Date'] . ': ' .$reportPeriodDisplay;
$displayTable .= '</td>'; 
$displayTable .= '</tr>';
$displayTable .= '<tr>';
$displayTable .= '<td align="center">';

$displayTable .= $libms->get_fine_report($RecordType, $RecordStatus, $RecordDate, $StartDate, $EndDate, $FindBy, $groupTarget, $rankTarget, $classnames,$studentIDs, "PRINT",$RecordPeriod);
	

$displayTable .= '</td>';
$displayTable .= '</tr>';

//$displayTable .= '</tbody>';
$displayTable .= '</table>';


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  /> 
<META Http-Equiv="Cache-Control" Content="no-cache">

<style type='text/css'>

html, body { margin: 0px; padding: 0px; } 


@media print and (width: 21cm) and (height: 29.7cm) {
    @page {
       margin: 3cm;
    }
 }
 
@media print
{
	
  /*table { page-break-after:auto }
  tr    { page-break-inside:avoid; page-break-after:auto }
  td    { page-break-inside:avoid; page-break-after:auto }
  thead { display:table-header-group }
  tfoot { display:table-footer-group }*/
  
  #footer {
         display:block;
		   position:fixed;
		   bottom:0px;
		   width:100%;
  } 
    
    
}

</style>
</head>
<body>
<?php echo $displayTable;?>
</body>
</html>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>