<?php
#using: 

/***************************************
 * 
 * Date:	2019-07-23 [Henry]
 * Details:	use the local library of jquery
 * 
 * Date:	2017-04-07 (Cameron)
 * Details:	change ajax method to post when click submit_result so that it accepts special characters like <> in BookTitle
 * 
 * Date:	2016-12-02 (Cameron)
 * Details:	Add parameter $printMode to getResult() and getRecord_RecordList(), don't provide hyper link to history borrow log in print mode
 * 
 * Date: 	2014-05-13 (Henry)
 * Details: Added school name at top of the page
 ***************************************/
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../";
$MODULE_ROOT = "../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

require_once('ReportView.php');

global $Lang;
$linterface = new interface_html("libms.html");

class BookReportReportView extends ReportView{
	public function __construct(){
	
		$CurrentPageArr['LIBMS'] = 1;
		$CurrentPage = "PageReportingBookReport";
		parent::__construct($CurrentPage);
	}
	
	public function getView($viewdata = null){
		global $Lang;
	
		if (empty($print)){
			$x .= $this->getHeader();
			$x .= $this->getJS($viewdata);
			$x .= $this->getFrom($viewdata);
			$x .= $this->getFooter();
		}
		return $x;
	}
	
	public function getJS($viewdata){
		global $Lang;
		ob_start();
		?>
		<link rel="stylesheet" href="/templates/jquery/jquery.ui.all.css" />
		<script src="/templates/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
		<script src="/templates/jquery/jquery-ui-1.10.0.min.js" type="text/javascript"></script>
		<script type="text/javascript" >

		function hideOptionLayer()
		{
			$('#formContent').attr('style', 'display: none');
			$('.spanHideOption').attr('style', 'display: none');
			$('.spanShowOption').attr('style', '');
			//document.getElementById('div_form').className = 'report_option report_hide_option';
		}
		
		function showOptionLayer()
		{
			$('#formContent').attr('style', '');
			$('.spanShowOption').attr('style', 'display: none');
			$('.spanHideOption').attr('style', '');
			//document.getElementById('div_form').className = 'report_option report_show_option';
		}
		
		
		function click_print(bookID)
		{
			document.form1.action="?page=BookReport&action=print&ID="+bookID;
			document.form1.target="_blank";
			document.form1.submit();
			
			document.form1.action="?page=BookReport";
			document.form1.target="_self";
		}

		var bookID;
		function bookdetail(bookID){
			//console.log(bookID);
				$.post("?page=BookReport&action=getResult", {ID: bookID},
				function(data) {
					$('#formContent').attr('style','display: none');
					$('#form_result').html(data);		
					$('#spanShowOption').show();
					$('#spanHideOption').hide();
					$('#report_show_option').addClass("report_option report_hide_option");
					window.scrollTo(0, 0);
				});
			}
		
		function selectbook(ajax_return_obj){
			$modal = $(this);
			ajaxReturn = ajax_return_obj; 
		    //console.log(ajax_return_obj);

		    if (ajax_return_obj != null && ajax_return_obj.is_success){
				if (ajax_return_obj.bookInfo.length ==1){
					bookdetail(ajaxReturn.bookInfo[0].BookID); 
					return;
				}else{
					$popup_div = $('<div title="<?=$Lang['libms']['report']['msg']['please_pick_a_book']?>" style="z-index: 10000;"/>');
					$table = $('<table width=100% />');
					$.each(ajax_return_obj.bookInfo, function (i,v){
						$row_div = $('<tr style="height: 26px;"/>');
						$row_div.append('<td>'+v.BookCode+'</td>'+'<td>'+v.BookTitle+'</td>'+'<td>'+v.ResponsibilityBy1+'</td>'+'<td><a href="#">Check</a></td>');
						$table.append($row_div);
						$row_div.find('a').click(function(){
							bookID = ajaxReturn.bookInfo[i].BookID;
							bookdetail(bookID); 
							$popup_div.dialog("close"); 
						});
						//append_a_reserve_row(this.html_row);
					});
					$table.find('tr:even').css("background-color", "#EEEEFF");
				}


				$popup_div.append($table);
				$popup_div.dialog({
					width : 900,
					height: 600,
					modal : true,
					stack : true,
					zIndex : 999999,
					close: function() {$popup_div.remove();}
				});
				
				$popup_div.parent().css("z-index","99999");
			}else{
				alert('<?=$Lang['libms']['CirculationManagement']['msg']['n_callno_acno']?>');
			}
		}
		
		$().ready( function(){
			$.ajaxSetup({ cache:false,
					   async:false });

			$('#SearchWords').keypress(function(e){
			    if ( e.which == 13 ) {
			    	$('#submit_result').click();
					e.preventDefault();
				    return false;
			    }
			});
			$('#SearchWords').focus();

			$('#submit_result').click(function(){
				if ($('#SearchWords').val() == ''){
					alert("<?= $Lang['libms']['report']['msg']['empty_search'] ?>");
					$('#SearchWords').focus();
					return false;
					}
				searchcode = $('input#SearchWords').val();
				if (searchcode.length >=2){
					$.ajax({
						dataType: "json",
						type: "POST",								
						url: '?page=BookReport&action=getBook',
						data : {
									book_search : searchcode
							   },			
						success: selectbook,
						error: function(){ alert('Error on AJAX call!'); }
					});
					
				}else{
					alert("<?=$Lang['libms']['report']['msg']['searching_string_too_short']?>");
				}

				return false;
			
			});

		});
		</script>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}

	public function getFrom($viewdata){
		global $Lang;
	
		ob_start();
		?>
		
		<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
		<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />

			<div id="report_show_option">
				
				<span id="spanShowOption" class="spanShowOption" style="display:none">
					<a href="javascript:showOptionLayer();"><?=$Lang["libms"]["report"]["showoption"]?></a>
				</span> 
				
				<span id="spanHideOption" class="spanHideOption" style="display:none">
					<a href="javascript:hideOptionLayer();"><?=$Lang["libms"]["report"]["hideoption"]?></a>
				</span>

		
				<form name="form1" method="post" action="?page=BookReport" id="formContent">
					
					<table class="form_table_v30">
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["libms"]["report"]["search"]?></td>
							<td>
								<input id="SearchWords" name="data[Search]" type="text" class="textboxlong" />
							</td>
						</tr>
					</table>
					
					<?=$this->linterface->MandatoryField();?>
					
					<div class="edit_bottom_v30">
						<p class="spacer"></p>
						<?= $this->linterface->GET_ACTION_BTN($Lang['libms']['report']['Submit'], "button", "", "submit_result")?>
						<p class="spacer"></p>
					</div>
				</form>
			</div>
			<div id="form_result"> </div>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getToolbar($viewdata){
		global $Lang;
		//don't show if not posted.
		if (!isset($viewdata['Result']))
			return;
	
		ob_start();
		?>
	
	<div id='toolbox' class="content_top_tool">
	<div class="Conntent_tool">
	<a href="javascript:click_print(<?=$viewdata['filterArray']['bookID']?>)" class="print"> <?=$Lang['Btn']['Print']?></a>
	</div>
	<br style="clear:both" />
	</div>
	<?
	$x = ob_get_contents();
	ob_end_clean();
	return $x;
	
	}
	
	public function getResult($viewdata,$printMode=false){
		global $Lang, $junior_mck;
		ob_start();
		
		
		
		echo '<div id="result_body" style="margin: auto;max-width:95%" >';
		echo '<div id="result_stat" >';
		if ( $_REQUEST['action'] == 'print'){
			//Added school name at top of the page
			$schoolName =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
			echo '<p>'.$schoolName.'</p>';
		}
		echo '<center><h2>'.$Lang["libms"]["reporting"]["PageReportingBookReport"].'</center></h2>';

		if (!(empty($viewdata['filterArray'])) && (empty($viewdata['Result']))){
			echo $Lang["libms"]["report"]["norecord"];
		}elseif (!(empty($viewdata['filterArray'])) && !(empty($viewdata['Result']))){
			echo $this->getRecord_RecordList($viewdata,$printMode);
		}
		echo '</div>';
		echo '<div id="result_chart" style="margin: auto;width:800px" >';
		echo '</div>';
		echo '</div>';	
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getRecord_RecordList($viewdata,$printMode=false){
		global $Lang;
		//dump($viewdata);
		### build display table
		if ($viewdata['Result']['bookInfo']['0']['CoverImage']!="" && file_exists("../../..".$viewdata['Result']['bookInfo']['0']['CoverImage']))
		{
			$BookCover = "<img src='".$viewdata['Result']['bookInfo']['0']['CoverImage']."' border='0' />";
		}
		$x = "<table class='common_table_list_v30 view_table_list_v30'>";
			$x .= "<tr>";
			$x .= "<th COLSPAN='3'>" . $Lang["libms"]["report"]["bookinfo"]."</th>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td>".$Lang["libms"]["report"]["booktitle"]."</td>";
			$x .= "<td>".$viewdata['Result']['bookInfo']['0']['BookTitle']."</td>"."<td rowspan='8'>{$BookCover}</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td>".$Lang["libms"]["report"]["ResponsibilityBy"]."</td>";
			$x .= "<td>".$viewdata['Result']['bookInfo']['0']['ResponsibilityBy1']."</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			//$x .= "<td>".$Lang["libms"]["report"]["ACNO"]."</td>";
			//$x .= "<td>".$viewdata['Result']['bookInfo']['0']['BookCode']."</td>";
			//$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td>".$Lang["libms"]["report"]["CallNum"]."</td>";
			$x .= "<td>".$viewdata['Result']['bookInfo']['0']['CallNum']." ".$viewdata['Result']['bookInfo']['0']['CallNum2']."</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td>".$Lang["libms"]["report"]["Edition"]."</td>";
			$x .= "<td>".$viewdata['Result']['bookInfo']['0']['Edition']."</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td>".$Lang["libms"]["report"]["Publisher"]."</td>";
			$x .= "<td>".$viewdata['Result']['bookInfo']['0']['Publisher']."</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td>".$Lang["libms"]["report"]["country"]."</td>";
			$x .= "<td>".$viewdata['Result']['bookInfo']['0']['Country']."</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td>".$Lang["libms"]["report"]["language"]."</td>";
			$x .= "<td>".$viewdata['Result']['bookInfo']['0']['Language']."</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td>".$Lang["libms"]["report"]["circulation_type"]."</td>";
			$x .= "<td colspan='2'>".$viewdata['Result']['bookInfo']['0']['circulation']."</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td>".$Lang["libms"]["report"]["book_category"]."</td>";
			$x .= "<td colspan='2'>".$viewdata['Result']['bookInfo']['0']['category']."</td>";
			$x .= "</tr>";
			$x .= "<td>".$Lang["libms"]["book"]["tags"]."</td>";
			$x .= "<td colspan='2'>".$viewdata['Result']['tags']."&nbsp;</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td>".$Lang["libms"]["report"]["Resource"]."</td>";
			$x .= "<td colspan='2'>".$viewdata['Result']['bookInfo']['0']['resources']."</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td>".$Lang["libms"]["report"]["totalreserve"]."</td>";
			$x .= "<td colspan='2'>";
			if(empty($viewdata['Result']['reserve'])){
				$x .= '0';
			} else {
				$x .= $viewdata['Result']['reserve']['0']['TotalReserve'];
			}
			$x .= "</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td>".$Lang["libms"]["report"]["totalborrow"]."</td>";
			$x .= "<td colspan='2'><table width=100%>";
			$x .= "<tr>";
			$x .= "<th>".$Lang["libms"]["report"]["ACNO"]."</th><th>".$Lang["libms"]["report"]["barcode"]."</th><th>".$Lang["libms"]["report"]["recordstatus"]."</th><th>".$Lang['libms']['settings']['book_location']."</th><th>".$Lang["libms"]["report"]["totalborrow"]."</th>";
			$x .= "</tr>";
			$total_borrow = 0;
			$counter_i = 0;
			foreach($viewdata['Result']['borrow'] as $items)
			{		
				$x .= "<tr>";
				$x .= "<td>".$items['ACNO']."</td>";
				$x .= "<td>".$items['BarCode']."</td>";
				$x .= "<td>".$Lang["libms"]["book_status"][$items['RecordStatus']]."</td>";
				$x .= "<td>".$items['LocationTitle']."</td>";
				$x .= "<td>".(($items['TotalBorrow']>0 && $items['TotalBorrow']!="" && !$printMode) ? "<a href=\"javascript:tb_show('".$Lang["libms"]["Circulation"]["BookBorrowHistory"]."', '../management/circulation/book_loans_history.php?BarCode=".$items['BarCode']."&height=500&width=600')\">".$items['TotalBorrow']."</a>" : $items['TotalBorrow'])."</td>";
				$x .= "</tr>";
				$total_borrow += $items['TotalBorrow'];
				$counter_i ++;
			}
			if ($counter_i>1)
			{
				$x .= "<tr>";
				$x .= "<td colspan='4' align='right'>".$Lang["libms"]["report"]["total"]."</td>";
				$x .= "<td>".$total_borrow."</td>";
				$x .= "</tr>";
			}
			$x .= "</table></td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td>".$Lang["libms"]["report"]["totaloverdue"]."</td>";
			$x .= "<td colspan='2'>";
			if(empty($viewdata['Result']['overdue'])){
				$x .= '0';
			} else {
				$x .= $viewdata['Result']['overdue']['0']['TotalOverdue'];
			}
			$x .= "</td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td>".$Lang["libms"]["report"]["totalamount"]."</td>";
			$x .= "<td colspan='2'>";
			if(empty($viewdata['Result']['money']['0']['TotalMoney'])){
				$x .= '$0';
			} else {
				$x .= '$'.$viewdata['Result']['money']['0']['TotalMoney'];
			}
			$x .= "</td>";
			$x .= "</tr>";
			
			
			
						
			$x .= "</table><br>";
		### build display table
	
		return $x;
	}
	
	/**
	 * get Print view
	 * @param array $viewdata
	 * @return string
	 */
	public function getPrint($viewdata){
		global $Lang,$LAYOUT_SKIN;
		ob_start();
		include_once("{$_SERVER['DOCUMENT_ROOT']}/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
		?>
	<div id='toolbar' class= 'print_hide' style='text-align: right; width: 100%'>
	<?
	$linterface = new interface_html();
	echo $linterface->GET_SMALL_BTN($Lang["libms"]["report"]["toolbar"]['Btn']['Print'], "button", "javascript:window.print();","print");
	
	?>
	</div>	
	<?=$this->getResult($viewdata,true);?>
	</body>
	</html>
	<?
	$x = ob_get_contents();
	ob_end_clean();
	return $x;
	}
	
	/**
	* get CSV string
	* @param array $viewdata
	* @return string
	
	public function getCSV($viewdata){
	global $Lang;
	
	$headers[] = $Lang["libms"]["report"]["ClassLevel"];
	$headers[] = $Lang['libms']['report']['ClassName'];
	$headers[] = $Lang['libms']['report']['LendingTotal'];
	
	$formatted_ary[] = $headers;
	foreach($viewdata['Result'] as $row){
	$formatted_ary[] = array(
	$row['ClassLevel'],
	$row['ClassName'],
	$row['Total']
	);
	}
	
	//array_unshift($formatted_ary,$headers);
	$csv = Exporter::array_to_CSV($formatted_ary);
	//dump($viewdata['Result']);
	return $csv;
	
	}
	
	*/
}