<?php
# using: 

/***************************************
 * 
 * Date:    2019-03-04 (Cameron)
 * Details: add filter condition LIBMS_OVERDUE_LOG.RecordStatus<>'DELETE' [Case #X157459]
 * 
 * Date:	2018-05-18 (Henry)
 * Details:	fixed incorrect total number of item [Case#Y139668]
 *   
 * Date:	2016-08-24 (Cameron)
 * Details:	Fix bug: should use LIBMS_BOOK_UNIQUE.CirculationTypeCode prior to LIBMS_BOOK.CirculationTypeCode (case #Z100853)  
 * 
 * Date:	2013-11-29 (Henry)
 * Details:	Created this file
 * 
 ***************************************/
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/TimeManager.php");

intranet_auth();
intranet_opendb();

$Lang['General']['EmptySymbol'] = "";


if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['progress report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();
$libms = new liblms();
$filename = "progress_report.csv";
$ExportArr = array();

## Obtain POST Value
$StartDate = $_POST['StartDate'];
$EndDate = $_POST['EndDate'];

$thisStockTakeSchemeDisplay = $StartDate  . ' ' . $Lang['General']['To']  . ' ' .  $EndDate;
# Define Column Title
$exportColumn = array();
$exportColumn[0][] = $Lang["libms"]["GroupManagement"]["Right"]["reports"]["progress report"];
$exportColumn[1][] = $Lang['General']['Date'] . ':' ;
$exportColumn[1][] = $thisStockTakeSchemeDisplay;

//----- $displayTable [start]
//$sql = "select distinct Language from LIBMS_BOOK where Language is not null and Language <> '' order by Language ";
//$bookLang = $libms->returnArray($sql);
//$circulationTypeList = $libms->GET_CIRCULATION_TYPE_LIST(array());
//
//$circulation_x_language = array();
//
////debug_pr($circulationTypeList);
//$total = 0;
//for($i=0; $i<count($circulationTypeList); $i++){
//	$subTotal = 0;
//	$ExportArr[$i][] = $circulationTypeList[$i]['CirDescription'];
//	for($j=0; $j<count($bookLang); $j++){
//		//get the book sub total here...
//		$sql = "select count(*) from LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND lb.CirculationTypeCode = '".$circulationTypeList[$i]['CirculationTypeCode']."' AND lb.Language = '".$bookLang[$j]['Language']."'";
//		$count = $libms->returnArray($sql);
//		$circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['lang_'.$bookLang[$j]['Language']] = $count[0][0];
//		$subTotal +=$count[0][0];
//		$ExportArr[$i][] =$count[0][0];
//	}
//	$sql = "select count(*) from LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND lb.CirculationTypeCode = '".$circulationTypeList[$i]['CirculationTypeCode']."' AND (lb.Language is null OR lb.Language = '')";
//	$count = $libms->returnArray($sql);
//	$circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['others'] = $count[0][0];
//	$subTotal +=$count[0][0];
//	$circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['subTotal'] = $subTotal;
//
//	$ExportArr[$i][] = $count[0][0];
//
//
//	$ExportArr[$i][] = $subTotal;
//
//	$total +=$subTotal;
//}
//
//
//
//$subTotal = 0;
//
//	$ExportArr[$i][] = $Lang["libms"]["report"]['Others'];
//
//for($j=0; $j<count($bookLang); $j++){
//	//get the book sub total here...
//	$sql = "select count(*) from LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND (lb.CirculationTypeCode = '' OR lb.CirculationTypeCode is null) AND lb.Language = '".$bookLang[$j]['Language']."'";
//	$count = $libms->returnArray($sql);
//	$circulation_x_language['others']['lang_'.$bookLang[$j]['Language']] = $count[0][0];
//	$subTotal +=$count[0][0];
//
//	$ExportArr[$i][] =$count[0][0];
//
//}
//$sql = "select count(*) from LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND (lb.CirculationTypeCode = '' OR lb.CirculationTypeCode is null) AND (lb.Language is null OR lb.Language = '')";
//$count = $libms->returnArray($sql);
//$circulation_x_language['others']['others'] = $count[0][0];
//$subTotal +=$count[0][0];
//$circulation_x_language['others']['subTotal'] = $subTotal;
//$ExportArr[$i][] =$count[0][0];
//$ExportArr[$i][] =$subTotal;
//$total +=$subTotal;
////debug_pr($circulation_x_language);

//----- $displayTable [start]
$circulationTypeList = $libms->GET_CIRCULATION_TYPE_LIST(array());

$circulationTypeCodeArray = array();
for($i=0; $i<count($circulationTypeList); $i++){
	$circulationTypeCodeArray[] = $circulationTypeList[$i]['CirculationTypeCode'];
}

//$sql = "select lb.CirculationTypeCode, lb.Language, count(*) as countValue from LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where ((lb.`RecordStatus` NOT LIKE 'DELETE' OR lb.`RecordStatus` IS NULL) AND lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') GROUP BY lb.CirculationTypeCode, BINARY lb.Language ORDER BY lb.CirculationTypeCode";
$sql = "select case when (lbu.CirculationTypeCode is not null and lbu.CirculationTypeCode<>'') then lbu.CirculationTypeCode
					when (lb.CirculationTypeCode is not null and lb.CirculationTypeCode<>'') then lb.CirculationTypeCode
					else '' 
				end as CirculationTypeCode, lb.Language, count(*) as countValue 
		from 	LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID 
		where ((lb.`RecordStatus` NOT LIKE 'DELETE' OR lb.`RecordStatus` IS NULL) 
			AND lbu.RecordStatus NOT LIKE 'DELETE' 
			AND lbu.RecordStatus NOT LIKE 'WRITEOFF' 
			AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' 
			AND lbu.RecordStatus NOT LIKE 'LOST' 
			AND lbu.RecordStatus NOT LIKE 'ORDERING') 
		GROUP BY CirculationTypeCode, BINARY lb.Language 
		ORDER BY CirculationTypeCode";

$result = $libms->returnArray($sql);

$circulation_x_language = array();
$circulation_x_language['others']['others'] = 0;
for($i=0; $i<count($result); $i++){
	
		$noCirculationTypeCode = !in_array($result[$i]['CirculationTypeCode'], $circulationTypeCodeArray) || (trim($result[$i]['CirculationTypeCode']) == '' || trim($result[$i]['CirculationTypeCode']) == NULL);
		$noLanguage = (trim($result[$i]['Language']) == '' || trim($result[$i]['Language']) == NULL);
		
		if($noCirculationTypeCode && $noLanguage){
			$circulation_x_language['others']['others'] += $result[$i]['countValue'];
		}
		else{
			if($noCirculationTypeCode){
				$circulation_x_language['others']['lang_'.$result[$i]['Language']] += $result[$i]['countValue'];
			}
			else if($noLanguage){
				$circulation_x_language['type_'.$result[$i]['CirculationTypeCode']]['others'] += $result[$i]['countValue'];
			}
			else
				$circulation_x_language['type_'.$result[$i]['CirculationTypeCode']]['lang_'.$result[$i]['Language']] = $result[$i]['countValue'];
		}
}
//debug_pr($circulation_x_language);

$sql = "select distinct BINARY Language as Language from LIBMS_BOOK where (RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL) and Language is not null and Language <> '' order by Language ";
$bookLang = $libms->returnArray($sql);

$total = 0;
for($i=0; $i<count($circulationTypeList); $i++){
	$subTotal = 0;
	$ExportArr[$i][] = $circulationTypeList[$i]['CirDescription'];
	for($j=0; $j<count($bookLang); $j++){
		//get the book sub total here...
		$value = $circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['lang_'.$bookLang[$j]['Language']];
		if(!$value) $value = 0;
		$subTotal +=$value;
		$ExportArr[$i][] = $value;
	}

	$value = $circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['others'];
	if(!$value) $value = 0;
		$subTotal +=$value;
	
	$ExportArr[$i][] = $value;

	$ExportArr[$i][] = $subTotal;

	$total +=$subTotal;
}

$subTotal = 0;

	$ExportArr[$i][] = $Lang["libms"]["report"]['Others'];

for($j=0; $j<count($bookLang); $j++){
	$value = $circulation_x_language['others']['lang_'.$bookLang[$j]['Language']];
	if(!$value) $value = 0;
	$subTotal +=$value;
	$ExportArr[$i][] = $value;

	
}

$value = $circulation_x_language['others']['others'];
$ExportArr[$i][] = $value;
$subTotal += $value;

$ExportArr[$i][] = $subTotal;
$total +=$subTotal;

	$exportColumn[5][] = $Lang["libms"]["settings"]["circulation_type"];

	for($j=0; $j<count($bookLang); $j++){
		$exportColumn[5][] = $bookLang[$j]['Language'];
	}
	$exportColumn[5][] = $Lang["libms"]["report"]['Others'];
	$exportColumn[5][] = $Lang["libms"]["report"]["total"];

$exportColumn[2][] =''; 
$exportColumn[3][] = $Lang["libms"]["report"]["library_stock"];
$exportColumn[4][] = $Lang["libms"]["report"]["total"];
$exportColumn[4][] =$total;
//----- $displayTable [end]
$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "9");

//----- $displayTable2 [start] --------------------------------------------------------------------------------------------
//$sql = "select distinct Language from LIBMS_BOOK where Language is not null and Language <> '' order by Language ";
//$bookLang = $libms->returnArray($sql);
//$circulationTypeList = $libms->GET_CIRCULATION_TYPE_LIST(array());
//
//$circulation_x_language = array();
//$ExportArr = array();
////debug_pr($circulationTypeList);
//$total = 0;
//for($i=0; $i<count($circulationTypeList); $i++){
//	$subTotal = 0;
//	$ExportArr[$i][] = $circulationTypeList[$i]['CirDescription'];
//	for($j=0; $j<count($bookLang); $j++){
//		//get the book sub total here...
//		$sql = "select count(*) from LIBMS_BORROW_LOG as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND lb.CirculationTypeCode = '".$circulationTypeList[$i]['CirculationTypeCode']."' AND lb.Language = '".$bookLang[$j]['Language']."' AND lbu.BorrowTime >= '{$StartDate} 00:00:00' AND lbu.BorrowTime <= '{$EndDate} 00:00:00'";
//		$count = $libms->returnArray($sql);
//		$circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['lang_'.$bookLang[$j]['Language']] = $count[0][0];
//		$subTotal +=$count[0][0];
//		$ExportArr[$i][] =$count[0][0];
//	}
//	$sql = "select count(*) from LIBMS_BORROW_LOG as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND lb.CirculationTypeCode = '".$circulationTypeList[$i]['CirculationTypeCode']."' AND (lb.Language is null OR lb.Language = '') AND lbu.BorrowTime >= '{$StartDate} 00:00:00' AND lbu.BorrowTime <= '{$EndDate} 00:00:00'";
//	$count = $libms->returnArray($sql);
//	$circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['others'] = $count[0][0];
//	$subTotal +=$count[0][0];
//	$circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['subTotal'] = $subTotal;
//
//	$ExportArr[$i][] = $count[0][0];
//
//
//	$ExportArr[$i][] = $subTotal;
//
//	$total +=$subTotal;
//}
//
//
//
//$subTotal = 0;
//
//	$ExportArr[$i][] = $Lang["libms"]["report"]['Others'];
//
//for($j=0; $j<count($bookLang); $j++){
//	//get the book sub total here...
//	$sql = "select count(*) from LIBMS_BORROW_LOG as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND (lb.CirculationTypeCode = '' OR lb.CirculationTypeCode is null) AND lb.Language = '".$bookLang[$j]['Language']."' AND lbu.BorrowTime >= '{$StartDate} 00:00:00' AND lbu.BorrowTime <= '{$EndDate} 00:00:00'";
//	$count = $libms->returnArray($sql);
//	$circulation_x_language['others']['lang_'.$bookLang[$j]['Language']] = $count[0][0];
//	$subTotal +=$count[0][0];
//
//	$ExportArr[$i][] =$count[0][0];
//
//}
//$sql = "select count(*) from LIBMS_BORROW_LOG as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND (lb.CirculationTypeCode = '' OR lb.CirculationTypeCode is null) AND (lb.Language is null OR lb.Language = '') AND lbu.BorrowTime >= '{$StartDate} 00:00:00' AND lbu.BorrowTime <= '{$EndDate} 00:00:00'";
//$count = $libms->returnArray($sql);
//$circulation_x_language['others']['others'] = $count[0][0];
//$subTotal +=$count[0][0];
//$circulation_x_language['others']['subTotal'] = $subTotal;
//$ExportArr[$i][] =$count[0][0];
//$ExportArr[$i][] =$subTotal;
//$total +=$subTotal;
////debug_pr($circulation_x_language);

//$sql = "select lb.CirculationTypeCode, lb.Language, count(*) as countValue from LIBMS_BORROW_LOG as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where ((lb.`RecordStatus` NOT LIKE 'DELETE' OR lb.`RecordStatus` IS NULL) AND lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND UNIX_TIMESTAMP(lbu.BorrowTime) > UNIX_TIMESTAMP('{$StartDate} 00:00:00') AND UNIX_TIMESTAMP(lbu.BorrowTime) <= UNIX_TIMESTAMP('{$EndDate} 23:59:59') GROUP BY lb.CirculationTypeCode, BINARY lb.Language ORDER BY lb.CirculationTypeCode";
$sql = "select case when (lbu.CirculationTypeCode is not null and lbu.CirculationTypeCode<>'') then lbu.CirculationTypeCode
					when (lb.CirculationTypeCode is not null and lb.CirculationTypeCode<>'') then lb.CirculationTypeCode
					else '' 
				end as CirculationTypeCode, lb.Language, count(*) as countValue 
		from 	LIBMS_BORROW_LOG as lbl JOIN LIBMS_BOOK_UNIQUE as lbu ON lbl.UniqueID=lbu.UniqueID
		JOIN 	LIBMS_BOOK as lb ON lb.BookID = lbu.BookID 
		where (lb.`RecordStatus` NOT LIKE 'DELETE' OR lb.`RecordStatus` IS NULL) 
			AND lbu.RecordStatus NOT LIKE 'DELETE' 
			AND lbu.RecordStatus NOT LIKE 'WRITEOFF' 
			AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' 
			AND lbu.RecordStatus NOT LIKE 'LOST' 
			AND lbu.RecordStatus NOT LIKE 'ORDERING'
			AND UNIX_TIMESTAMP(lbl.BorrowTime) >= UNIX_TIMESTAMP('{$StartDate} 00:00:00') 
			AND UNIX_TIMESTAMP(lbl.BorrowTime) <= UNIX_TIMESTAMP('{$EndDate} 23:59:59') 
		GROUP BY CirculationTypeCode, BINARY lb.Language 
		ORDER BY CirculationTypeCode";

$result = $libms->returnArray($sql);

$circulation_x_language = array();
$circulation_x_language['others']['others'] = 0;
for($i=0; $i<count($result); $i++){
	
		$noCirculationTypeCode = (trim($result[$i]['CirculationTypeCode']) == '' || trim($result[$i]['CirculationTypeCode']) == NULL);
		$noLanguage = (trim($result[$i]['Language']) == '' || trim($result[$i]['Language']) == NULL);
		
		if($noCirculationTypeCode && $noLanguage){
			$circulation_x_language['others']['others'] += $result[$i]['countValue'];
		}
		else{
			if($noCirculationTypeCode){
				$circulation_x_language['others']['lang_'.$result[$i]['Language']] += $result[$i]['countValue'];
			}
			else if($noLanguage){
				$circulation_x_language['type_'.$result[$i]['CirculationTypeCode']]['others'] += $result[$i]['countValue'];
			}
			else
				$circulation_x_language['type_'.$result[$i]['CirculationTypeCode']]['lang_'.$result[$i]['Language']] = $result[$i]['countValue'];
		}
}
//debug_pr($circulation_x_language);

//$sql = "select distinct BINARY Language as Language from LIBMS_BOOK where Language is not null and Language <> '' order by Language ";
//$bookLang = $libms->returnArray($sql);
//$circulationTypeList = $libms->GET_CIRCULATION_TYPE_LIST(array());
$ExportArr = array();
$total = 0;
for($i=0; $i<count($circulationTypeList); $i++){
	$subTotal = 0;
	$ExportArr[$i][] = $circulationTypeList[$i]['CirDescription'];
	for($j=0; $j<count($bookLang); $j++){
		//get the book sub total here...
		$value = $circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['lang_'.$bookLang[$j]['Language']];
		if(!$value) $value = 0;
		$subTotal +=$value;
		$ExportArr[$i][] =$value;
	}

	$value = $circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['others'];
	if(!$value) $value = 0;
		$subTotal +=$value;

	$ExportArr[$i][] =$value;
	$ExportArr[$i][] =$subTotal;
	$total +=$subTotal;
}

$subTotal = 0;

	$ExportArr[$i][] = $Lang["libms"]["report"]['Others'];

for($j=0; $j<count($bookLang); $j++){
	$value = $circulation_x_language['others']['lang_'.$bookLang[$j]['Language']];
	if(!$value) $value = 0;
	$subTotal +=$value;
	$ExportArr[$i][] =$value;
	
}

$value = $circulation_x_language['others']['others'];
$ExportArr[$i][] = $value;
$subTotal += $value;
$ExportArr[$i][] =$subTotal;
$total +=$subTotal;

$exportColumn2 = array();
$exportColumn2[0][] = '';
$exportColumn2[1][] = '';
	$exportColumn2[5][] = $Lang["libms"]["settings"]["circulation_type"];

	for($j=0; $j<count($bookLang); $j++){
		$exportColumn2[5][] = $bookLang[$j]['Language'];
	}
	$exportColumn2[5][] = $Lang["libms"]["report"]['Others'];
	$exportColumn2[5][] = $Lang["libms"]["report"]["total"];


$exportColumn2[2][] = $Lang["libms"]["report"]["library_resources"];
$exportColumn2[3][] = $Lang["libms"]["report"]["total"];
$exportColumn2[3][] = $total;
$timeManager = new TimeManager($libms);
$days = $timeManager->dayForPenalty(strtotime($StartDate), strtotime($EndDate)) + 1;
$exportColumn2[4][] = $Lang["libms"]["report"]['daily_averages_issue'];
$exportColumn2[4][] = number_format($total/$days,2)." (".$total."/".$days.")";
//----- $displayTable2 [end]
$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn2, "\t", "\r\n", "\t", 0, "9");

//----- $displayTable3 [start]
//$sql ="select sum(Payment) from LIBMS_OVERDUE_LOG AS lol JOIN LIBMS_BORROW_LOG AS lbl ON lbl.BorrowLogID=lol.BorrowLogID WHERE UNIX_TIMESTAMP(lol.DateCreated)>=UNIX_TIMESTAMP('{$StartDate}') AND UNIX_TIMESTAMP(lol.DateCreated)<=UNIX_TIMESTAMP('{$EndDate} 23:59:59')";
//$totalFine = $libms->returnArray($sql);
//$totalFine = ($totalFine[0][0]?$totalFine[0][0]:'0');
$sql ="select sum(Payment) from LIBMS_OVERDUE_LOG AS lol JOIN LIBMS_BORROW_LOG AS lbl ON lbl.BorrowLogID=lol.BorrowLogID WHERE UNIX_TIMESTAMP(lol.DateCreated)>=UNIX_TIMESTAMP('{$StartDate} 00:00:00') AND UNIX_TIMESTAMP(lol.DateCreated)<=UNIX_TIMESTAMP('{$EndDate} 23:59:59') AND lbl.RecordStatus<>'LOST' AND lol.RecordStatus<>'DELETE'";
$totalOversue = $libms->returnArray($sql);
$totalOversue = ($totalOversue[0][0]?$totalOversue[0][0]:'0');
$sql ="select sum(Payment) from LIBMS_OVERDUE_LOG AS lol JOIN LIBMS_BORROW_LOG AS lbl ON lbl.BorrowLogID=lol.BorrowLogID WHERE UNIX_TIMESTAMP(lol.DateCreated)>=UNIX_TIMESTAMP('{$StartDate} 00:00:00') AND UNIX_TIMESTAMP(lol.DateCreated)<=UNIX_TIMESTAMP('{$EndDate} 23:59:59') AND lbl.RecordStatus='LOST' AND lol.RecordStatus<>'DELETE'";
$totalLost = $libms->returnArray($sql);
$totalLost = ($totalLost[0][0]?$totalLost[0][0]:'0');

$totalFine = $totalOversue + $totalLost;

$sql ="select sum(Payment) from LIBMS_OVERDUE_LOG AS lol JOIN LIBMS_BORROW_LOG AS lbl ON lbl.BorrowLogID=lol.BorrowLogID WHERE UNIX_TIMESTAMP(lol.DateCreated)>=UNIX_TIMESTAMP('{$StartDate} 00:00:00') AND UNIX_TIMESTAMP(lol.DateCreated)<=UNIX_TIMESTAMP('{$EndDate} 23:59:59') AND lbl.RecordStatus<>'LOST' AND lol.RecordStatus='SETTLED'";
$totalOversuePaid = $libms->returnArray($sql);
$totalOversuePaid = ($totalOversuePaid[0][0]?$totalOversuePaid[0][0]:'0');
$sql ="select sum(Payment) from LIBMS_OVERDUE_LOG AS lol JOIN LIBMS_BORROW_LOG AS lbl ON lbl.BorrowLogID=lol.BorrowLogID WHERE UNIX_TIMESTAMP(lol.DateCreated)>=UNIX_TIMESTAMP('{$StartDate} 00:00:00') AND UNIX_TIMESTAMP(lol.DateCreated)<=UNIX_TIMESTAMP('{$EndDate} 23:59:59') AND lbl.RecordStatus='LOST' AND lol.RecordStatus='SETTLED'";
$totalLostPaid = $libms->returnArray($sql);
$totalLostPaid = ($totalLostPaid[0][0]?$totalLostPaid[0][0]:'0');
$ExportArr = array();
$exportColumn3 = array();
$exportColumn3[0][] = '';
$exportColumn3[1][] = '';
$exportColumn3[2][] = $Lang["libms"]["report"]["pay"];

$exportColumn3[3][] = $Lang["libms"]["report"]["total"];
$exportColumn3[3][] = $totalFine;

$exportColumn3[4][] = "";
$exportColumn3[4][] = $Lang["libms"]["report"]["fine_type_overdue"];
$exportColumn3[4][] = $Lang["libms"]["report"]["fine_type_lost"];

$exportColumn3[5][] = $Lang["libms"]["report"]["total"];
$exportColumn3[5][] = $totalOversue;
$exportColumn3[5][] = $totalLost;

$exportColumn3[6][] = $Lang["libms"]["CirculationManagement"]["paied"];
$exportColumn3[6][] = $totalOversuePaid;
$exportColumn3[6][] = $totalLostPaid;

//----- $displayTable3 [end]
$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn3, "\t", "\r\n", "\t", 0, "9");

intranet_closedb();

# Output The File To User Browser
$lexport->EXPORT_FILE($filename, $export_content, $isXLS = false, $ignoreLength = false, $BaseCode="UTF-8");

?>