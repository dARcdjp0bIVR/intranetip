<?php

//	Using: 

/***************************************
 * 
 * Date:    2019-03-04 (Cameron)
 * Details: add filter condition LIBMS_OVERDUE_LOG.RecordStatus<>'DELETE' [Case #X157459]
 * 
 * Date:	2018-05-18 (Henry)
 * Details:	fixed incorrect total number of item [Case#Y139668]
 *   
 * Date:	2016-08-24 (Cameron)
 * Details:	Fix bug: should use LIBMS_BOOK_UNIQUE.CirculationTypeCode prior to LIBMS_BOOK.CirculationTypeCode (case #Z100853)
 *   
 * Date:	2013-11-29 (Henry)
 * Details:	Created this file
 * 
 ***************************************/
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/TimeManager.php");

intranet_auth();
intranet_opendb();

$Lang['General']['EmptySymbol'] = "&nbsp;";

$libms = new liblms();

if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['progress report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$libms = new liblms();
$linterface = new interface_html();

# Current Page Info
$CurrentPageArr['LIBMS'] = 1;
$TAGS_OBJ[] = array($Lang["libms"]["GroupManagement"]["Right"]["reports"]["progress report"]);
$CurrentPage = "PageReportingProgressReport";
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

## Obtain POST Value
//$stocktakeSchemeID = $_POST['StocktakeSchemeID'];
//$selectedStocktakeStatus = $_POST['selectedStocktakeStatus'];
$StartDate = $_POST['StartDate'];
$EndDate = $_POST['EndDate'];

if($StartDate=='' || $EndDate==''){
	header('Location: index.php');	
}


$thisStockTakeSchemeDisplay = $StartDate  . ' ' . $Lang['General']['To']  . ' ' .  $EndDate;

$linterface->LAYOUT_START();

########################## DB Table Start ###############################
//----- $displayTable [start]
$circulationTypeList = $libms->GET_CIRCULATION_TYPE_LIST(array());

$circulationTypeCodeArray = array();
for($i=0; $i<count($circulationTypeList); $i++){
	$circulationTypeCodeArray[] = $circulationTypeList[$i]['CirculationTypeCode'];
}

//$sql = "select lb.CirculationTypeCode, lb.Language, count(*) as countValue from LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where ((lb.`RecordStatus` NOT LIKE 'DELETE' OR lb.`RecordStatus` IS NULL) AND lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') GROUP BY lb.CirculationTypeCode, BINARY lb.Language ORDER BY lb.CirculationTypeCode";
$sql = "select case when (lbu.CirculationTypeCode is not null and lbu.CirculationTypeCode<>'') then lbu.CirculationTypeCode
					when (lb.CirculationTypeCode is not null and lb.CirculationTypeCode<>'') then lb.CirculationTypeCode
					else '' 
				end as CirculationTypeCode, lb.Language, count(*) as countValue 
		from 	LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID 
		where ((lb.`RecordStatus` NOT LIKE 'DELETE' OR lb.`RecordStatus` IS NULL) 
			AND lbu.RecordStatus NOT LIKE 'DELETE' 
			AND lbu.RecordStatus NOT LIKE 'WRITEOFF' 
			AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' 
			AND lbu.RecordStatus NOT LIKE 'LOST' 
			AND lbu.RecordStatus NOT LIKE 'ORDERING') 
		GROUP BY CirculationTypeCode, BINARY lb.Language 
		ORDER BY CirculationTypeCode";

$result = $libms->returnArray($sql);

$circulation_x_language = array();
$circulation_x_language['others']['others'] = 0;
for($i=0; $i<count($result); $i++){
	
		$noCirculationTypeCode = !in_array($result[$i]['CirculationTypeCode'], $circulationTypeCodeArray) || (trim($result[$i]['CirculationTypeCode']) == '' || trim($result[$i]['CirculationTypeCode']) == NULL);
		$noLanguage = (trim($result[$i]['Language']) == '' || trim($result[$i]['Language']) == NULL);
		
		if($noCirculationTypeCode && $noLanguage){
			$circulation_x_language['others']['others'] += $result[$i]['countValue'];
		}
		else{
			if($noCirculationTypeCode){
				$circulation_x_language['others']['lang_'.$result[$i]['Language']] += $result[$i]['countValue'];
			}
			else if($noLanguage){
				$circulation_x_language['type_'.$result[$i]['CirculationTypeCode']]['others'] += $result[$i]['countValue'];
			}
			else
				$circulation_x_language['type_'.$result[$i]['CirculationTypeCode']]['lang_'.$result[$i]['Language']] = $result[$i]['countValue'];
		}
}
//debug_pr($circulation_x_language);

$sql = "select distinct BINARY Language as Language from LIBMS_BOOK where (RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL) and Language is not null and Language <> '' order by Language ";
$bookLang = $libms->returnArray($sql);

$total = 0;
for($i=0; $i<count($circulationTypeList); $i++){
	$subTotal = 0;
	$displayTable .= '<tr>';
	//$displayTable .= '<th class="tabletop tabletopnolink">';
	$displayTable .= '<td>';
	$displayTable .= $circulationTypeList[$i]['CirDescription'];
	$displayTable .= '</td>';
	//$displayTable .= '</th>';
	for($j=0; $j<count($bookLang); $j++){
		//get the book sub total here...
		$value = $circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['lang_'.$bookLang[$j]['Language']];
		if(!$value) $value = 0;
		$subTotal +=$value;
		$displayTable .= '<td>';
		$displayTable .=$value;
		$displayTable .= '</td>';
	}

	$value = $circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['others'];
	if(!$value) $value = 0;
		$subTotal +=$value;
	$displayTable .= '<td>';
	$displayTable .=$value;
	$displayTable .= '</td>';
	$displayTable .= '<td>';
	$displayTable .=$subTotal;
	$displayTable .= '</td>';
	$displayTable .= '</tr>';
	$total +=$subTotal;
}

$subTotal = 0;
$displayTable .= '<tr>';
//$displayTable .= '<th class="tabletop tabletopnolink">';
	$displayTable .= '<td>';
	$displayTable .= $Lang["libms"]["report"]['Others'];
	$displayTable .= '</td>';
	//$displayTable .= '</th>';
for($j=0; $j<count($bookLang); $j++){
	$value = $circulation_x_language['others']['lang_'.$bookLang[$j]['Language']];
	if(!$value) $value = 0;
	$subTotal +=$value;
	$displayTable .= '<td>';
	$displayTable .=$value;
	$displayTable .= '</td>';
	
}
$displayTable .= '<td>';
$value = $circulation_x_language['others']['others'];
$displayTable .= $value;
$subTotal += $value;
$displayTable .= '</td>';
$displayTable .= '<td>';
$displayTable .=$subTotal;
$displayTable .= '</td>';
$displayTable .= '</tr>';
$x = $displayTable;
$total +=$subTotal;
$x = $displayTable;

//----- $displayTable [start]
//$sql = "select distinct Language from LIBMS_BOOK where Language is not null and Language <> '' order by Language ";
//$bookLang = $libms->returnArray($sql);
//$circulationTypeList = $libms->GET_CIRCULATION_TYPE_LIST(array());
//
//$circulation_x_language = array();
//
////debug_pr($circulationTypeList);
//$total = 0;
//for($i=0; $i<count($circulationTypeList); $i++){
//	$subTotal = 0;
//	$displayTable .= '<tr>';
//	//$displayTable .= '<th class="tabletop tabletopnolink">';
//	$displayTable .= '<td>';
//	$displayTable .= $circulationTypeList[$i]['CirDescription'];
//	$displayTable .= '</td>';
//	//$displayTable .= '</th>';
//	for($j=0; $j<count($bookLang); $j++){
//		//get the book sub total here...
//		$sql = "select count(*) from LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND lb.CirculationTypeCode = '".$circulationTypeList[$i]['CirculationTypeCode']."' AND lb.Language = '".$bookLang[$j]['Language']."'";
//		$count = $libms->returnArray($sql);
//		$circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['lang_'.$bookLang[$j]['Language']] = $count[0][0];
//		$subTotal +=$count[0][0];
//		$displayTable .= '<td>';
//		$displayTable .=$count[0][0];
//		$displayTable .= '</td>';
//	}
//	$sql = "select count(*) from LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND lb.CirculationTypeCode = '".$circulationTypeList[$i]['CirculationTypeCode']."' AND (lb.Language is null OR lb.Language = '')";
//	$count = $libms->returnArray($sql);
//	$circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['others'] = $count[0][0];
//	$subTotal +=$count[0][0];
//	$circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['subTotal'] = $subTotal;
//	$displayTable .= '<td>';
//	$displayTable .=$count[0][0];
//	$displayTable .= '</td>';
//	$displayTable .= '<td>';
//	$displayTable .=$subTotal;
//	$displayTable .= '</td>';
//	$displayTable .= '</tr>';
//	$total +=$subTotal;
//}
//
//
//
//$subTotal = 0;
//$displayTable .= '<tr>';
////$displayTable .= '<th class="tabletop tabletopnolink">';
//	$displayTable .= '<td>';
//	$displayTable .= $Lang["libms"]["report"]['Others'];
//	$displayTable .= '</td>';
//	//$displayTable .= '</th>';
//for($j=0; $j<count($bookLang); $j++){
//	//get the book sub total here...
//	$sql = "select count(*) from LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND (lb.CirculationTypeCode = '' OR lb.CirculationTypeCode is null) AND lb.Language = '".$bookLang[$j]['Language']."'";
//	$count = $libms->returnArray($sql);
//	$circulation_x_language['others']['lang_'.$bookLang[$j]['Language']] = $count[0][0];
//	$subTotal +=$count[0][0];
//	$displayTable .= '<td>';
//	$displayTable .=$count[0][0];
//	$displayTable .= '</td>';
//}
//$sql = "select count(*) from LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND (lb.CirculationTypeCode = '' OR lb.CirculationTypeCode is null) AND (lb.Language is null OR lb.Language = '')";
//$count = $libms->returnArray($sql);
//$circulation_x_language['others']['others'] = $count[0][0];
//$subTotal +=$count[0][0];
//$circulation_x_language['others']['subTotal'] = $subTotal;
//$displayTable .= '<td>';
//$displayTable .=$count[0][0];
//$displayTable .= '</td>';
//$displayTable .= '<td>';
//$displayTable .=$subTotal;
//$displayTable .= '</td>';
//$displayTable .= '</tr>';
//$x = $displayTable;
//$total +=$subTotal;
////debug_pr($circulation_x_language);

$displayTable = '';
$displayTable .= '<table class="common_table_list view_table_list" width="100%" align="center"  border="0" cellSpacing="0" cellPadding="4">';
$displayTable .= '<thead>';
$displayTable .= '<tr>';
	$displayTable .= '<th width="30%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["settings"]["circulation_type"];
	$displayTable .= '</th>';
	for($j=0; $j<count($bookLang); $j++){
		$displayTable .= '<th width="'.(70/(count($bookLang)+2)).'%" class="tabletop tabletopnolink">';
		$displayTable .= $bookLang[$j]['Language'];
		$displayTable .= '</th>';
	}
	$displayTable .= '<th width="'.(70/(count($bookLang)+2)).'%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["report"]['Others'];
	$displayTable .= '</th>';
	$displayTable .= '<th width="'.(70/(count($bookLang)+2)).'%" class="tabletop tabletopnolink">';
	$displayTable .= $Lang["libms"]["report"]["total"];
	$displayTable .= '</th>';
$displayTable .= '</tr>';
$displayTable .= '</thead>';
$displayTable .= '<tbody>';

$displayTable .=$x;

$displayTable .= '</tbody>';
$displayTable .= '</table>';
//$displayTable .= $Lang["libms"]["report"]["total"].': '.$total;
$displayTable .= '<br/>';
$displayTable_y = "<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
$displayTable_y .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$Lang["libms"]["report"]["total"]."</td>";
$displayTable_y .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">".$total."</td></tr>";
$displayTable_y .= "</table><br/>";
//----- $displayTable [end]

////----- $displayTable2 [start]
//$sql = "select distinct Language from LIBMS_BOOK where Language is not null and Language <> '' order by Language ";
//$bookLang = $libms->returnArray($sql);
//$circulationTypeList = $libms->GET_CIRCULATION_TYPE_LIST(array());
//
//$circulation_x_language = array();
//
////debug_pr($circulationTypeList);
//$total = 0;
//for($i=0; $i<count($circulationTypeList); $i++){
//	$subTotal = 0;
//	$displayTable2 .= '<tr>';
//	//$displayTable2 .= '<th class="tabletop tabletopnolink">';
//	$displayTable2 .= '<td>';
//	$displayTable2 .= $circulationTypeList[$i]['CirDescription'];
//	$displayTable2 .= '</td>';
//	//$displayTable2 .= '</th>';
//	for($j=0; $j<count($bookLang); $j++){
//		//get the book sub total here...
//		$sql = "select count(*) from LIBMS_BORROW_LOG as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND lb.CirculationTypeCode = '".$circulationTypeList[$i]['CirculationTypeCode']."' AND lb.Language = '".$bookLang[$j]['Language']."' AND lbu.BorrowTime >= '{$StartDate} 00:00:00' AND lbu.BorrowTime <= '{$EndDate} 00:00:00'";
//		$count = $libms->returnArray($sql);
//		$circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['lang_'.$bookLang[$j]['Language']] = $count[0][0];
//		$subTotal +=$count[0][0];
//		$displayTable2 .= '<td>';
//		$displayTable2 .=$count[0][0];
//		$displayTable2 .= '</td>';
//	}
//	$sql = "select count(*) from LIBMS_BORROW_LOG as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND lb.CirculationTypeCode = '".$circulationTypeList[$i]['CirculationTypeCode']."' AND (lb.Language is null OR lb.Language = '') AND lbu.BorrowTime >= '{$StartDate} 00:00:00' AND lbu.BorrowTime <= '{$EndDate} 00:00:00'";
//	$count = $libms->returnArray($sql);
//	$circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['others'] = $count[0][0];
//	$subTotal +=$count[0][0];
//	$circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['subTotal'] = $subTotal;
//	$displayTable2 .= '<td>';
//	$displayTable2 .=$count[0][0];
//	$displayTable2 .= '</td>';
//	$displayTable2 .= '<td>';
//	$displayTable2 .=$subTotal;
//	$displayTable2 .= '</td>';
//	$displayTable2 .= '</tr>';
//	$total +=$subTotal;
//}
//
//
//
//$subTotal = 0;
//$displayTable2 .= '<tr>';
////$displayTable2 .= '<th class="tabletop tabletopnolink">';
//$displayTable2 .= '<td>';
//	$displayTable2 .= $Lang["libms"]["report"]['Others'];
//	$displayTable2 .= '</td>';
//	//$displayTable2 .= '</th>';
//for($j=0; $j<count($bookLang); $j++){
//	//get the book sub total here...
//	$sql = "select count(*) from LIBMS_BORROW_LOG as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND (lb.CirculationTypeCode = '' OR lb.CirculationTypeCode is null) AND lb.Language = '".$bookLang[$j]['Language']."' AND lbu.BorrowTime >= '{$StartDate} 00:00:00' AND lbu.BorrowTime <= '{$EndDate} 00:00:00'";
//	$count = $libms->returnArray($sql);
//	$circulation_x_language['others']['lang_'.$bookLang[$j]['Language']] = $count[0][0];
//	$subTotal +=$count[0][0];
//	$displayTable2 .= '<td>';
//	$displayTable2 .=$count[0][0];
//	$displayTable2 .= '</td>';
//}
//$sql = "select count(*) from LIBMS_BORROW_LOG as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where (lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND (lb.CirculationTypeCode = '' OR lb.CirculationTypeCode is null) AND (lb.Language is null OR lb.Language = '') AND lbu.BorrowTime >= '{$StartDate} 00:00:00' AND lbu.BorrowTime <= '{$EndDate} 00:00:00'";
//$count = $libms->returnArray($sql);
//$circulation_x_language['others']['others'] = $count[0][0];
//$subTotal +=$count[0][0];
//$circulation_x_language['others']['subTotal'] = $subTotal;
//$displayTable2 .= '<td>';
//$displayTable2 .=$count[0][0];
//$displayTable2 .= '</td>';
//$displayTable2 .= '<td>';
//$displayTable2 .=$subTotal;
//$displayTable2 .= '</td>';
//$displayTable2 .= '</tr>';
//$x = $displayTable2;
//$total +=$subTotal;
////debug_pr($circulation_x_language);
//----- $displayTable2 [start]
//$sql = "select lb.CirculationTypeCode, lb.Language, count(*) as countValue from LIBMS_BORROW_LOG as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID where ((lb.`RecordStatus` NOT LIKE 'DELETE' OR lb.`RecordStatus` IS NULL) AND lbu.RecordStatus NOT LIKE 'DELETE' AND lbu.RecordStatus NOT LIKE 'WRITEOFF' AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' AND lbu.RecordStatus NOT LIKE 'LOST' AND lbu.RecordStatus NOT LIKE 'ORDERING') AND UNIX_TIMESTAMP(lbu.BorrowTime) >= UNIX_TIMESTAMP('{$StartDate} 00:00:00') AND UNIX_TIMESTAMP(lbu.BorrowTime) <= UNIX_TIMESTAMP('{$EndDate} 23:59:59') GROUP BY lb.CirculationTypeCode, BINARY lb.Language ORDER BY lb.CirculationTypeCode";
$sql = "select case when (lbu.CirculationTypeCode is not null and lbu.CirculationTypeCode<>'') then lbu.CirculationTypeCode
					when (lb.CirculationTypeCode is not null and lb.CirculationTypeCode<>'') then lb.CirculationTypeCode
					else '' 
				end as CirculationTypeCode, lb.Language, count(*) as countValue 
		from 	LIBMS_BORROW_LOG as lbl JOIN LIBMS_BOOK_UNIQUE as lbu ON lbl.UniqueID=lbu.UniqueID
		JOIN 	LIBMS_BOOK as lb ON lb.BookID = lbu.BookID 
		where (lb.`RecordStatus` NOT LIKE 'DELETE' OR lb.`RecordStatus` IS NULL) 
			AND lbu.RecordStatus NOT LIKE 'DELETE' 
			AND lbu.RecordStatus NOT LIKE 'WRITEOFF' 
			AND lbu.RecordStatus NOT LIKE 'SUSPECTEDMISSING' 
			AND lbu.RecordStatus NOT LIKE 'LOST' 
			AND lbu.RecordStatus NOT LIKE 'ORDERING'
			AND UNIX_TIMESTAMP(lbl.BorrowTime) >= UNIX_TIMESTAMP('{$StartDate} 00:00:00') 
			AND UNIX_TIMESTAMP(lbl.BorrowTime) <= UNIX_TIMESTAMP('{$EndDate} 23:59:59') 
		GROUP BY CirculationTypeCode, BINARY lb.Language 
		ORDER BY CirculationTypeCode";

$result = $libms->returnArray($sql);

$circulation_x_language = array();
$circulation_x_language['others']['others'] = 0;
for($i=0; $i<count($result); $i++){
	
		$noCirculationTypeCode = (trim($result[$i]['CirculationTypeCode']) == '' || trim($result[$i]['CirculationTypeCode']) == NULL);
		$noLanguage = (trim($result[$i]['Language']) == '' || trim($result[$i]['Language']) == NULL);
		
		if($noCirculationTypeCode && $noLanguage){
			$circulation_x_language['others']['others'] += $result[$i]['countValue'];
		}
		else{
			if($noCirculationTypeCode){
				$circulation_x_language['others']['lang_'.$result[$i]['Language']] += $result[$i]['countValue'];
			}
			else if($noLanguage){
				$circulation_x_language['type_'.$result[$i]['CirculationTypeCode']]['others'] += $result[$i]['countValue'];
			}
			else
				$circulation_x_language['type_'.$result[$i]['CirculationTypeCode']]['lang_'.$result[$i]['Language']] = $result[$i]['countValue'];
		}
}
//debug_pr($circulation_x_language);

//$sql = "select distinct BINARY Language as Language from LIBMS_BOOK where Language is not null and Language <> '' order by Language ";
//$bookLang = $libms->returnArray($sql);
//$circulationTypeList = $libms->GET_CIRCULATION_TYPE_LIST(array());

$total = 0;
for($i=0; $i<count($circulationTypeList); $i++){
	$subTotal = 0;
	$displayTable2 .= '<tr>';
	//$displayTable .= '<th class="tabletop tabletopnolink">';
	$displayTable2 .= '<td>';
	$displayTable2 .= $circulationTypeList[$i]['CirDescription'];
	$displayTable2 .= '</td>';
	//$displayTable .= '</th>';
	for($j=0; $j<count($bookLang); $j++){
		//get the book sub total here...
		$value = $circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['lang_'.$bookLang[$j]['Language']];
		if(!$value) $value = 0;
		$subTotal +=$value;
		$displayTable2 .= '<td>';
		$displayTable2 .=$value;
		$displayTable2 .= '</td>';
	}

	$value = $circulation_x_language['type_'.$circulationTypeList[$i]['CirculationTypeCode']]['others'];
	if(!$value) $value = 0;
		$subTotal +=$value;
	$displayTable2 .= '<td>';
	$displayTable2 .=$value;
	$displayTable2 .= '</td>';
	$displayTable2 .= '<td>';
	$displayTable2 .=$subTotal;
	$displayTable2 .= '</td>';
	$displayTable2 .= '</tr>';
	$total +=$subTotal;
}

$subTotal = 0;
$displayTable2 .= '<tr>';
//$displayTable .= '<th class="tabletop tabletopnolink">';
	$displayTable2 .= '<td>';
	$displayTable2 .= $Lang["libms"]["report"]['Others'];
	$displayTable2 .= '</td>';
	//$displayTable .= '</th>';
for($j=0; $j<count($bookLang); $j++){
	$value = $circulation_x_language['others']['lang_'.$bookLang[$j]['Language']];
	if(!$value) $value = 0;
	$subTotal +=$value;
	$displayTable2 .= '<td>';
	$displayTable2 .=$value;
	$displayTable2 .= '</td>';
	
}
$displayTable2 .= '<td>';
$value = $circulation_x_language['others']['others'];
$displayTable2 .= $value;
$subTotal += $value;
$displayTable2 .= '</td>';
$displayTable2 .= '<td>';
$displayTable2 .=$subTotal;
$displayTable2 .= '</td>';
$displayTable2 .= '</tr>';
$x = $displayTable2;
$total +=$subTotal;


$displayTable2 = '';
$displayTable2 .= '<table class="common_table_list view_table_list" width="100%" align="center"  border="0" cellSpacing="0" cellPadding="4">';
$displayTable2 .= '<thead>';
$displayTable2 .= '<tr>';
	$displayTable2 .= '<th width="30%" class="tabletop tabletopnolink">';
	$displayTable2 .= $Lang["libms"]["settings"]["circulation_type"];
	$displayTable2 .= '</th>';
	for($j=0; $j<count($bookLang); $j++){
		$displayTable2 .= '<th width="'.(70/(count($bookLang)+2)).'%" class="tabletop tabletopnolink">';
		$displayTable2 .= $bookLang[$j]['Language'];
		$displayTable2 .= '</th>';
	}
	$displayTable2 .= '<th width="'.(70/(count($bookLang)+2)).'%" class="tabletop tabletopnolink">';
	$displayTable2 .= $Lang["libms"]["report"]['Others'];
	$displayTable2 .= '</th>';
	$displayTable2 .= '<th width="'.(70/(count($bookLang)+2)).'%" class="tabletop tabletopnolink">';
	$displayTable2 .= $Lang["libms"]["report"]["total"];
	$displayTable2 .= '</th>';
$displayTable2 .= '</tr>';
$displayTable2 .= '</thead>';
$displayTable2 .= '<tbody>';

$displayTable2 .=$x;

$displayTable2 .= '</tbody>';
$displayTable2 .= '</table>';
//$displayTable2 .= $Lang["libms"]["report"]["total"].': '.$total;
$displayTable2 .= '<br/>';
$displayTable2_y = "<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
$displayTable2_y .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$Lang["libms"]["report"]["total"]."</td>";
$displayTable2_y .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">".$total."</td></tr>";
$timeManager = new TimeManager($libms);
$days = $timeManager->dayForPenalty(strtotime($StartDate), strtotime($EndDate)) + 1;
$displayTable2_y .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$Lang["libms"]["report"]['daily_averages_issue']."</td>";
$displayTable2_y .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">".number_format($total/$days,2)." (".$total."/".$days.")</td></tr>";
$displayTable2_y .= "</table><br/>";
//----- $displayTable2 [end]

//----- $displayTable3 [start]
$displayTable3 = '';
//$sql ="select sum(Payment) from LIBMS_OVERDUE_LOG AS lol JOIN LIBMS_BORROW_LOG AS lbl ON lbl.BorrowLogID=lol.BorrowLogID WHERE UNIX_TIMESTAMP(lol.DateCreated)>=UNIX_TIMESTAMP('{$StartDate}') AND UNIX_TIMESTAMP(lol.DateCreated)<=UNIX_TIMESTAMP('{$EndDate} 23:59:59')";
//$totalFine = $libms->returnArray($sql);
//$totalFine = ($totalFine[0][0]?$totalFine[0][0]:'0.00');
$sql ="select sum(Payment) from LIBMS_OVERDUE_LOG AS lol JOIN LIBMS_BORROW_LOG AS lbl ON lbl.BorrowLogID=lol.BorrowLogID WHERE UNIX_TIMESTAMP(lol.DateCreated)>=UNIX_TIMESTAMP('{$StartDate} 00:00:00') AND UNIX_TIMESTAMP(lol.DateCreated)<=UNIX_TIMESTAMP('{$EndDate} 23:59:59') AND lbl.RecordStatus<>'LOST' AND lol.RecordStatus<>'DELETE'";
$totalOversue = $libms->returnArray($sql);
$totalOversue = ($totalOversue[0][0]?$totalOversue[0][0]:'0.00');
$sql ="select sum(Payment) from LIBMS_OVERDUE_LOG AS lol JOIN LIBMS_BORROW_LOG AS lbl ON lbl.BorrowLogID=lol.BorrowLogID WHERE UNIX_TIMESTAMP(lol.DateCreated)>=UNIX_TIMESTAMP('{$StartDate} 00:00:00') AND UNIX_TIMESTAMP(lol.DateCreated)<=UNIX_TIMESTAMP('{$EndDate} 23:59:59') AND lbl.RecordStatus='LOST' AND lol.RecordStatus<>'DELETE'";
$totalLost = $libms->returnArray($sql);
$totalLost = ($totalLost[0][0]?$totalLost[0][0]:'0.00');

$totalFine = $totalOversue + $totalLost;

$sql ="select sum(Payment) from LIBMS_OVERDUE_LOG AS lol JOIN LIBMS_BORROW_LOG AS lbl ON lbl.BorrowLogID=lol.BorrowLogID WHERE UNIX_TIMESTAMP(lol.DateCreated)>=UNIX_TIMESTAMP('{$StartDate} 00:00:00') AND UNIX_TIMESTAMP(lol.DateCreated)<=UNIX_TIMESTAMP('{$EndDate} 23:59:59') AND lbl.RecordStatus<>'LOST' AND lol.RecordStatus='SETTLED'";
$totalOversuePaid = $libms->returnArray($sql);
$totalOversuePaid = ($totalOversuePaid[0][0]?$totalOversuePaid[0][0]:'0.00');
$sql ="select sum(Payment) from LIBMS_OVERDUE_LOG AS lol JOIN LIBMS_BORROW_LOG AS lbl ON lbl.BorrowLogID=lol.BorrowLogID WHERE UNIX_TIMESTAMP(lol.DateCreated)>=UNIX_TIMESTAMP('{$StartDate} 00:00:00') AND UNIX_TIMESTAMP(lol.DateCreated)<=UNIX_TIMESTAMP('{$EndDate} 23:59:59') AND lbl.RecordStatus='LOST' AND lol.RecordStatus='SETTLED'";
$totalLostPaid = $libms->returnArray($sql);
$totalLostPaid = ($totalLostPaid[0][0]?$totalLostPaid[0][0]:'0.00');

$displayTable3 = '';
$displayTable3 = "<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
$displayTable3 .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$Lang["libms"]["report"]["total"]."</td>";
$displayTable3 .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$".number_format($totalFine,2)."</td></tr>";
$displayTable3 .= "</table><br/>";

$displayTable3 .= '<table class="common_table_list view_table_list" width="100%" align="center"  border="0" cellSpacing="0" cellPadding="4">';
$displayTable3 .= '<thead>';
$displayTable3 .= '<tr>';
	$displayTable3 .= '<th width="10%" class="tabletop tabletopnolink">';
	$displayTable3 .= '&nbsp;';
	$displayTable3 .= '</th>';
	$displayTable3 .= '<th width="10%" class="tabletop tabletopnolink">';
	$displayTable3 .= $Lang["libms"]["report"]["fine_type_overdue"];
	$displayTable3 .= '</th>';
	$displayTable3 .= '<th width="10%" class="tabletop tabletopnolink">';
	$displayTable3 .= $Lang["libms"]["report"]["fine_type_lost"];
	$displayTable3 .= '</th>';
$displayTable3 .= '</tr>';
$displayTable3 .= '</thead>';
$displayTable3 .= '<tbody>';
$displayTable3 .= '<tr>';
$displayTable3 .= '<td>';
	$displayTable3 .= $Lang["libms"]["report"]["total"];
	$displayTable3 .= '</td>';
	$displayTable3 .= '<td>';
	$displayTable3 .= '$'.number_format($totalOversue,2);
	$displayTable3 .= '</td>';
	$displayTable3 .= '<td>';
	$displayTable3 .= '$'.number_format($totalLost,2);
	$displayTable3 .= '</td>';
$displayTable3 .= '</tr>';
$displayTable3 .= '<tr>';
$displayTable3 .= '<td>';
	$displayTable3 .= $Lang["libms"]["CirculationManagement"]["paied"];
	$displayTable3 .= '</td>';
	$displayTable3 .= '<td>';
	$displayTable3 .= '$'.number_format($totalOversuePaid,2);
	$displayTable3 .= '</td>';
	$displayTable3 .= '<td>';
	$displayTable3 .= '$'.number_format($totalLostPaid,2);
	$displayTable3 .= '</td>';
$displayTable3 .= '</tr>';
$displayTable3 .= '</tbody>';
$displayTable3 .= '</table><br/>';
//$displayTable3 .= $Lang["libms"]["report"]["fine_type_overdue"].': '.$totalValue;
//----- $displayTable3 [end]
?>

<script language="javascript">
$(document).ready(function(){	
	$('#spanShowOption').show();	
});

function hideOptionLayer()
{
	$('#formContent').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
	//document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer()
{
	$('#formContent').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
	//document.getElementById('div_form').className = 'report_option report_show_option';
}

function Js_Select_All(id){
	$('#'+id + ' option').attr('selected', 'selected');	
}

function Js_Check_Form(obj){
	var error = 0 ;
	var stocktakeLength = $('#stocktakeStatus option:selected').length;

	if(document.getElementById("StartDate").value > document.getElementById('EndDate').value){
		document.getElementById("StartDate").focus();
		alert("<?=$Lang['General']['JS_warning']['InvalidDateRange']?>");
		error++;
	}
	if(error==0)
	{
		obj.submit();
	}
}

function click_export()
{
	document.form1.action = "export.php";
	document.form1.target = "_blank";
	document.form1.submit();
	
	document.form1.action = "result.php";
	document.form1.target="_self";
}

function click_print()
{
	document.form1.action="print.php";
	document.form1.target="_blank";
	document.form1.submit();
	
	document.form1.action = "result.php";
	document.form1.target="_self";
}	
</script>

<form name="form1" method="post" action="result.php">
<!--###### Content Board Start ######-->
<table width="99%" border="0" cellspacing="0" cellpadding="0">
<tr> 
  <td width="13" >&nbsp;</td>
  <td class="main_content"><!---->
 	<div class="report_option report_show_option">
 			<span id="spanShowOption" class="spanShowOption" style="display:none">
					<a href="javascript:showOptionLayer();"><?=$Lang["libms"]["report"]["showoption"]?></a>
				</span> 
				
				<span id="spanHideOption" class="spanHideOption" style="display:none">
					<a href="javascript:hideOptionLayer();"><?=$Lang["libms"]["report"]["hideoption"]?></a>
				</span>
 	</div>

	<span id="formContent" style="display:none" >
		<table width="99%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td width="13">&nbsp;</td>
			<td>
	       	<div class="report_option report_option_title"></div>
			<div class="table_board">
			<table class="form_table">
		        <tr class="form_table ">
		          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['reporting']['dateRange']?><br/>(<?=$Lang['libms']['reporting']['progressDateRangeRemarks']?>)</td>
		          <td><?=$linterface->GET_DATE_PICKER("StartDate", ($StartDate?$StartDate:date('Y-m-01')))?> <span class="tabletextremark"></span> ~ <?=$linterface->GET_DATE_PICKER("EndDate", $EndDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
		        </tr>
		     </table>
		     <?=$linterface->MandatoryField();?>
	     	<p class="spacer"></p>
	    	</div>
	      	<div class="edit_bottom">
	      
	        <p class="spacer"></p>
	        <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "Js_Check_Form(this.form)", "GenerateBtn")?>        
	        <p class="spacer"></p>
	      </div>
	      </td>
	      <td width="11">&nbsp;</td>
	    </tr>
	  </table>
	</span>

	<div id='toolbox' class="content_top_tool">
	<div class="Conntent_tool">
		<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?></a>
		<a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?></a>
		</div>
		<br style="clear:both" />
	</div>

   	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<?=$linterface->GET_NAVIGATION2($Lang["libms"]["report"]["library_stock"])?>
				<p class="spacer"></p>
			    <p class="spacer"></p>
			    </td>
				<td valign="bottom"><div align="right"><?=$Lang['General']['Date'] . ': ' .$thisStockTakeSchemeDisplay?></div></td>
			</tr>
		</table> 					    
		<?=$displayTable_y?>
		<?=$displayTable?>
		
		<br/>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<?=$linterface->GET_NAVIGATION2($Lang["libms"]["report"]["library_resources"])?>
				<p class="spacer"></p>
			    <p class="spacer"></p>
			    </td>
			</tr>
		</table> 					    
		<?=$displayTable2_y?>
		<?=$displayTable2?>
		<br/>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<?=$linterface->GET_NAVIGATION2($Lang["libms"]["report"]["pay"])?>
				<p class="spacer"></p>
			    <p class="spacer"></p>
			    </td>
				
			</tr>
		</table> 					    
		
		<?=$displayTable3?>
				
		<div class="edit_bottom">
		<p class="spacer"></p>
		<input name="button" type="button" class="formbutton" onclick="window.location='index.php'" value="Back" /> 	
		<p class="spacer"></p>
  </div>
  <p>&nbsp;</p></td>
  <td width="11" >&nbsp;</td>
</tr>
</table>
<!--###### Content Board End ######-->
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
