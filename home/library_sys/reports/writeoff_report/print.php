<?php
#using:  

/***************************************
 * 
 * Date:    2018-04-20 (Cameron)
 * Details: retrieve AccountDate(CreationDate) [case #W120117]
 * 
 * Date:	2016-05-05 (Cameron)
 * Details: add column "Call Number" (case #E95394)
 *  
 * Date:	2015-09-01 (Henry)
 * Details:	added signature blank
 * 
 * Date:	2015-04-27 (Cameron)
 * Details:	Show Date Of Purchase for a book in the result list. (case ref: 2015-0421-0956-43206)
 * 
 * Date:	2015-01-23 (Henry)
 * Details:	Added Category2 filter
 * Date: 	2014-05-14 (Henry)
 * Details: Added school name at top of the page
 * Date:	2013-11-27 (Henry)
 * Details:	Created this file
 * 
 ***************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$Lang['General']['EmptySymbol'] = "&nbsp;";



$laccessright = new libaccessright();

if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['write-off report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$libms = new liblms();
$linterface = new interface_html();
$lclass = new libclass();

$StartDate = $_POST['StartDate'];
$EndDate = $_POST['EndDate'];
$resourcesTypeCode = $_POST['selectedResourcesTypeCode'];
$selectedCirculationType = $_POST['selectedCirculationType'];
$selectedLocation = $_POST['selectedLocation'];
$selectedBookCategory = $_POST['selectedBookCategory'];

$writeoffResult = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID,'','',$returnType='Array','',$resourcesTypeCode,$selectedCirculationType,$selectedLocation, $order=true, 'WriteOffDate, lbu.ACNO, lbu.BarCode', $selectedBookCategory, $StartDate, $EndDate, $BookCategoryType);	


$thisStockTakeSchemeDisplay = $StartDate  . ' ' . $Lang['General']['To']  . ' ' .  $EndDate;

$schoolName =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];

$displayTable = '';
$displayTable .= '<table width="98%" border="0" cellspacing="0" cellpadding="5" valign="top">';
$displayTable .= '<thead><tr>';
$displayTable .= '<td align="right" class="print_hide">';
$displayTable .= $linterface->GET_BTN($button_print, "button","javascript:window.print()");
$displayTable .= '</td>';
$displayTable .= '<tr><td>'.$schoolName.'</td></tr>'; 
$displayTable .= '<tr>';
$displayTable .= '<td>';
$displayTable .= '<center><h2>' . $Lang["libms"]["GroupManagement"]["Right"]["reports"]["write-off report"] . '</h2></center>';
$displayTable .= '</td>';
$displayTable .= '</tr></thead>';
//$displayTable .= '<tr>';
//$displayTable .= '<td align="right" class="print_hide">';
//$displayTable .= $Lang['General']['Date'] . ': ' .$thisStockTakeSchemeDisplay;
//$displayTable .= '</td>'; 
//$displayTable .= '</tr>';
$displayTable .= '<tr>';
$displayTable .= '<td>';


$displayTable .= '<table class="common_table_list_v30  view_table_list_v30" width="100%">';
	$displayTable .= '<thead>';
	$displayTable .= '<tr>';
		$displayTable .= '<th width="1" class="tabletop tabletopnolink">';
		$displayTable .= '#';
		$displayTable .= '</th>';
		$displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
		$displayTable .= $Lang['libms']['bookmanagement']['WriteOffDate'];
		$displayTable .= '</th>';
		$displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
		$displayTable .= $Lang["libms"]["report"]["ACNO"];
		$displayTable .= '</th>';
		$displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
		$displayTable .= $Lang['libms']['book']['account_date'];
		$displayTable .= '</th>';		
		$displayTable .= '<th width="10%" class="tabletop tabletopnolink">';
		$displayTable .= $Lang["libms"]["book"]["call_number"];
		$displayTable .= '</th>';
		$displayTable .= '<th width="15%" class="tabletop tabletopnolink">';
		$displayTable .= $Lang['libms']['bookmanagement']['bookTitle'];
		$displayTable .= '</th>';
		$displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
		$displayTable .= $Lang["libms"]["book"]["purchase_date"];
		$displayTable .= '</th>';		
		$displayTable .= '<th width="7%" class="tabletop tabletopnolink">';
		$displayTable .= $Lang["libms"]["book"]["purchase_by_department"];
		$displayTable .= '</th>';
		$displayTable .= '<th width="7%" class="tabletop tabletopnolink">';
		$displayTable .= $Lang["libms"]["book"]["list_price"];
		$displayTable .= '</th>';
		$displayTable .= '<th width="7%" class="tabletop tabletopnolink">';
		$displayTable .= $Lang["libms"]["book"]["discount"];
		$displayTable .= '</th>';
		$displayTable .= '<th width="7%" class="tabletop tabletopnolink">';
		$displayTable .= $Lang["libms"]["book"]["purchase_price"];
		$displayTable .= '</th>';
		$displayTable .= '<th width="15%" class="tabletop tabletopnolink">';
		$displayTable .= $Lang['libms']['bookmanagement']['Reason'];
		$displayTable .= '</th>';
	$displayTable .= '</tr>';
	$displayTable .= '</thead>';
	$displayTable .= '<tbody>';
$numOfResult = count($result);

$i = 0;
$numOfPages = 0;
foreach($writeoffResult as $resultInfo)
{
	$writeoffDateRange = $resultInfo['WriteOffDate']?($resultInfo['WriteOffDate']=='0000-00-00'?substr($resultInfo['DateModified'], 0, 10):$resultInfo['WriteOffDate']):substr($resultInfo['DateModified'], 0, 10);
	if($writeoffDateRange >= $StartDate && $writeoffDateRange <= $EndDate){
		$i++;
		
		if($i%20==0)
		$numOfPages++;
		
		$thisWriteoffDate = $writeoffDateRange;
		$thisACNO = $resultInfo['ACNO']?$resultInfo['ACNO']:$Lang['General']['EmptySymbol'];
		
		$callNum = $resultInfo['CallNum']?$resultInfo['CallNum']:'';
		if ($resultInfo['CallNum2']) {
			$callNum .= ' '.$resultInfo['CallNum2'];
		}
		$thisCallNum = $callNum;
		
		$thisBookTitle = $resultInfo['BookTitle']?$resultInfo['BookTitle']:$Lang['General']['EmptySymbol'];
		$thisBookPurchaseByDepartment = $resultInfo['PurchaseByDepartment']?$resultInfo['PurchaseByDepartment']:$Lang['General']['EmptySymbol'];
		$thisBookListPrice = $resultInfo['ListPrice']?$resultInfo['ListPrice']:$Lang['General']['EmptySymbol'];
		$thisBookDiscount = $resultInfo['Discount']?$resultInfo['Discount']:$Lang['General']['EmptySymbol'];
		$thisBookPublisher = $resultInfo['PurchasePrice']?$resultInfo['PurchasePrice']:$Lang['General']['EmptySymbol'];
		$thisWriteoffReason = $resultInfo['Reason']?$resultInfo['Reason']:$Lang['General']['EmptySymbol'];
		$thisBookPurchaseDate = $resultInfo['PurchaseDate']?$resultInfo['PurchaseDate']:$Lang['General']['EmptySymbol'];
		$thisBookAccountDate = $resultInfo['AccountDate']?$resultInfo['AccountDate']:$Lang['General']['EmptySymbol'];
		
	$displayTable .= '<tr '.($i%20==1 && $i > 1?'class="page-break20"':'').'>';
	$displayTable .= '<td>';
			$displayTable .= $i ;
			$displayTable .= '</td>';	
			$displayTable .= '<td>';
			$displayTable .= $thisWriteoffDate;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisACNO;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisBookAccountDate;
			$displayTable .= '</td>';			
			$displayTable .= '<td>';
			$displayTable .= $thisCallNum;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisBookTitle;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisBookPurchaseDate;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisBookPurchaseByDepartment;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisBookListPrice;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisBookDiscount;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisBookPublisher;
			$displayTable .= '</td>';
			$displayTable .= '<td>';
			$displayTable .= $thisWriteoffReason;
			$displayTable .= '</td>';
	$displayTable .= '</tr>';
	}
}

if($i%20 !=0)
	$numOfPages++;
	
if ($i==0)
{
	$displayTable .= '<tr>';	
		$displayTable .= '<td colspan="11" align="center">';
		$displayTable .= $Lang['libms']['NoRecordAtThisMoment'];
		$displayTable .= '</td>';
	$displayTable .= '</tr>';
}
$displayTable .= '</table>';
$displayTable .= '</td>';
$displayTable .= '</tr>';

$displayTable .= '<tr>';	
	$displayTable .= '<td colspan="11" align="right">';
	$displayTable .= '<br/><br/>'.$Lang['libms']['report']['ApprovedBy'].':____________________________  '.$Lang['libms']['reporting']['date'].'_____________________________';
	$displayTable .= '<br/><br/>'.$Lang['libms']['report']['SubmittedBy'].':________________________________  '.$Lang['libms']['reporting']['date'].'_____________________________';
	$displayTable .= '</td>';
$displayTable .= '</tr>';

$displayTable .= '</tbody>';

$displayTable .= '<tfoot><tr><td>';
$displayTable .= '<table id="footer" style="border-top:solid;border-bottom:solid;width:100%"><tr><td>'.$Lang['libms']['bookmanagement']['WriteOffDate'].': '.$thisStockTakeSchemeDisplay.'</td><td align="right">'.$Lang["libms"]["report"]["printdate"].': '.date('Y-m-d').'</td></tr>';
$displayTable .= '<tr><td>'.$Lang['libms']['report']['WriteoffPrintTotal'].': '.($i).'</td><td id="page_num" align="right"></td></tr>';
$displayTable .= '</table>';
$displayTable .= '</td></tr></tfoot>';

$displayTable .= '</table>';


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  /> 
<META Http-Equiv="Cache-Control" Content="no-cache">

<style type='text/css'>

html, body { margin: 0px; padding: 0px; } 


/*@media print and (width: 21cm) and (height: 29.7cm) {
    @page {
       margin: 3cm;
    }
 }
 
@media print
{
	
  table { page-break-after:auto }
  tr    { page-break-inside:avoid; page-break-after:auto }
  td    { page-break-inside:avoid; page-break-after:auto }
  thead { display:table-header-group }
  tfoot { display:table-footer-group }
  
  #footer {
         display:block;
		   position:fixed;
		   bottom:0px;
		   width:100%;
  } 
    
    
}*/

@media print and (width: 21cm) and (height: 29.7cm) {
    @page {
       margin: 3cm;
       size: landscape;
    }
 }
 
@media print
{
	@page {size: A4 landscape; margin:.1cm .3cm;}
	table {  width:29cm}
	tr.page-break20 { page-break-inside:avoid;page-break-before:always;}
	td { line-height:100% }
	thead { display:table-header-group }
	tfoot { display:table-footer-group }
	/*#footer {
		position:fixed;
		bottom:5px;
		width:100%;
	}*/
	#footer td { width:50% ;align:right}
	#page_num:after {
		counter-increment: page_num;
		content: "Page " counter(page_num) " of <?=$numOfPages?>";
	}  
}

</style>
</head>
<body>
<?php echo $displayTable;?>
</body>
</html>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>