<?php
// using:

/**
 * *************************************
 * Date: 2018-05-03 (Cameron)
 * - retrieve AccountDate(CreationDate) [case #E89629]
 *
 * Date: 2016-04-16 (Henry)
 * Details: add columns List Price and Purchase Price
 *
 * Date: 2015-01-23 (Henry)
 * Details: Added Category2 filter
 *
 * Date: 2015-01-15 (Henry)
 * Details: Added memory_limit
 *
 * Date: 2014-05-13 (Henry)
 * Details: Added school name at top of the page
 *
 * Date: 2014-02-13 (Henry)
 * Details: Added filter Book Category
 *
 * Date: 2013-11-25 (Henry)
 * Details: Added column Write-off Date and Reason
 *
 * Date: 2013-06-19 (Rita)
 * Details: Amend access right checking
 *
 * Date: 2013-06-04 (Rita)
 * Details: Create this page
 * *************************************
 */
ini_set('memory_limit', '500M');

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "templates/" . $LAYOUT_SKIN . "/layout/print_header.php");

intranet_auth();
intranet_opendb();

$Lang['General']['EmptySymbol'] = "&nbsp;";

$laccessright = new libaccessright();

if (! $_SESSION['LIBMS']['admin']['current_right']['reports']['stock-take report'] && ! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$libms = new liblms();
$linterface = new interface_html();
$lclass = new libclass();

$stocktakeSchemeID = $_POST['StocktakeSchemeID'];
$selectedStocktakeStatus = $_POST['selectedStocktakeStatus'];
$resourcesTypeCode = $_POST['selectedResourcesTypeCode'];
$selectedCirculationType = $_POST['selectedCirculationType'];
$selectedLocation = $_POST['selectedLocation'];
$selectedBookCategory = $_POST['selectedBookCategory'];

// Stocktake Date Display
$sql = $libms->GET_STOCKTAKE_SCHEME_SQL('', 1);
$stockTakeSchemeArr = $libms->returnArray($sql);
$stockTakeSchemeIDArr = BuildMultiKeyAssoc($stockTakeSchemeArr, 'StocktakeSchemeID');
$thisStockTakeSchemeDisplayArr = $stockTakeSchemeIDArr[$stocktakeSchemeID];
$thisStockTakeSchemeDisplay = $thisStockTakeSchemeDisplayArr['StartDate'] . ' ' . $Lang['General']['To'] . ' ' . $thisStockTakeSchemeDisplayArr['EndDate'];

$result = array();
$foundResult = array();
$writeoffResult = array();
$nottakeResult = array();

foreach ((array) $selectedStocktakeStatus as $key => $StocktakeStatus) {
    if ($StocktakeStatus == 'FOUND') {
        // $foundResult = $libms->GET_STOCKTAKE_REPORT_STOCKTAKE_LOG_SQL($stocktakeSchemeID,$selectedStocktakeStatus,$resourcesTypeCode,$selectedCirculationType,$selectedLocation, $returnType='array');
        
        $foundResult = $libms->GET_STOCKTAKE_LOG_BOOK_INFO($barcode = '', $stocktakeSchemeID, $returnType = 'Array', $keyword = '', $resourcesTypeCode, $selectedCirculationType, $selectedLocation, $order = true, $selectedBookCategory, $BookCategoryType, '', $getAccountDate = true);
        $result = array_merge($result, (array) $foundResult);
    } elseif ($StocktakeStatus == 'WRITEOFF') {
        $writeoffResult = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID, '', '', $returnType = 'Array', '', $resourcesTypeCode, $selectedCirculationType, $selectedLocation, $order = true, '', $selectedBookCategory, '', '', $BookCategoryType);
        $result = array_merge($result, (array) $writeoffResult);
    } elseif ($StocktakeStatus == 'NOTTAKE') {
        $stocktakeLogArr = $libms->GET_STOCKTAKE_LOG_BOOK_INFO($barcode = '', $stocktakeSchemeID, $returnType = 'Array', $keyword = '', $resourcesTypeCode, $selectedCirculationType, $selectedLocation, $order = false, $selectedBookCategory, $BookCategoryType);
        $stocktakeLogUniqueBookIDArr = array_keys(BuildMultiKeyAssoc($stocktakeLogArr, 'UniqueID'));
        
        $writeoffLogArr = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID, '', '', $returnType = 'Array', '', $resourcesTypeCode, $selectedCirculationType, $selectedLocation, $order = false, '', $selectedBookCategory, '', '', $BookCategoryType);
        $writeoffLogUniqueBookIDArr = array_keys(BuildMultiKeyAssoc($writeoffLogArr, 'UniqueID'));
        
        $excludedUniqueBookIDArr = '';
        $excludedUniqueBookIDArr = array_merge((array) $stocktakeLogUniqueBookIDArr, (array) $writeoffLogUniqueBookIDArr);
        $nottakeResult = $libms->GET_NOT_STOCKTAKE_BOOK_INFO($excludedUniqueBookIDArr, '', $resourcesTypeCode, $selectedCirculationType, $selectedLocation, $returnType = 'array', $order = true, $selectedBookCategory, $thisStockTakeSchemeDisplayArr['RecordEndDate'], '', $BookCategoryType, false, false, $getAccountDate = true);
        $result = array_merge($result, (array) $nottakeResult);
    }
}

$schoolName = (isset($junior_mck)) ? iconv("big5", "utf-8//IGNORE", $_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];

$displayTable = '';
$displayTable .= '<table width="98%" border="0" cellspacing="0" cellpadding="5" valign="top">';
$displayTable .= '<thead><tr>';
$displayTable .= '<td align="right" class="print_hide">';
$displayTable .= $linterface->GET_BTN($button_print, "button", "javascript:window.print()");
$displayTable .= '</td>';
$displayTable .= '<tr><td>' . $schoolName . '</td></tr>';
$displayTable .= '<tr>';
$displayTable .= '<td>';
$displayTable .= '<center><h2>' . $Lang['libms']['reporting']['stocktakeReport'] . '</h2></center>';
$displayTable .= '</td>';
$displayTable .= '</tr></thead>';
// $displayTable .= '<tr>';
// $displayTable .= '<td align="right">';
// $displayTable .= $Lang['General']['Date'] . ': ' .$thisStockTakeSchemeDisplay;
// $displayTable .= '</td>';
// $displayTable .= '</tr>';
$displayTable .= '<tr>';
$displayTable .= '<td>';

// Create the summary of stocktake [start]
$displaySummary = '';
$displaySummary .= "<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
// $displaySummary .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$Lang["libms"]["report"]["total"]."</td>";
// $displaySummary .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">".count($result)."</td></tr>";
if (count($foundResult)) {
    $displaySummary .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $Lang['libms']['stocktake_status']['FOUND'] . "</td>";
    $displaySummary .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">" . count($foundResult) . "</td></tr>";
}
if (count($writeoffResult)) {
    $displaySummary .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $Lang['libms']['stocktake_status']['WRITEOFF'] . "</td>";
    $displaySummary .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">" . count($writeoffResult) . "</td></tr>";
}
if (count($nottakeResult)) {
    $displaySummary .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $Lang['libms']['stocktake_status']['NOTTAKE'] . "</td>";
    $displaySummary .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">" . count($nottakeResult) . "</td></tr>";
}
$displaySummary .= "</table><br/>";

$displayTable .= $displaySummary;
// Create the summary of stocktake [end]

$displayTable .= '<table class="common_table_list_v30  view_table_list_v30" width="100%">';
$displayTable .= '<thead>';
$displayTable .= '<tr>';
$displayTable .= '<th width="1%" class="tabletop tabletopnolink">';
$displayTable .= '#';
$displayTable .= '</th>';
$displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
$displayTable .= $Lang["libms"]["report"]["ACNO"];
$displayTable .= '</th>';
$displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
$displayTable .= $Lang['libms']['book']['account_date'];
$displayTable .= '</th>';
$displayTable .= '<th width="8%" class="tabletop tabletopnolink">';
$displayTable .= $Lang["libms"]["book"]["barcode"];
$displayTable .= '</th>';
$displayTable .= '<th width="15%" class="tabletop tabletopnolink">';
$displayTable .= $Lang["libms"]["book"]["title"];
$displayTable .= '</th>';
$displayTable .= '<th width="10%" class="tabletop tabletopnolink">';
$displayTable .= $Lang['libms']['book']['ISBN'];
$displayTable .= '</th>';
$displayTable .= '<th width="9%" class="tabletop tabletopnolink">';
$displayTable .= $Lang["libms"]["book"]["call_number"];
$displayTable .= '</th>';
$displayTable .= '<th width="9%" class="tabletop tabletopnolink">';
$displayTable .= $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Author'];
$displayTable .= '</th>';
$displayTable .= '<th width="9%" class="tabletop tabletopnolink">';
$displayTable .= $Lang["libms"]["book"]["publisher"];
$displayTable .= '</th>';
$displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
$displayTable .= $Lang["libms"]["book"]["publish_year"];
$displayTable .= '</th>';
$displayTable .= '<th width="2%" class="tabletop tabletopnolink">';
$displayTable .= $Lang["libms"]["book"]["edition"];
$displayTable .= '</th>';
$displayTable .= '<th width="6%" class="tabletop tabletopnolink">';
$displayTable .= $Lang['libms']['bookmanagement']['location'];
$displayTable .= '</th>';
$displayTable .= '<th width="3%" class="tabletop tabletopnolink">';
$displayTable .= $Lang["libms"]["book"]["list_price"];
$displayTable .= '</th>';
$displayTable .= '<th width="3%" class="tabletop tabletopnolink">';
$displayTable .= $Lang["libms"]["book"]["purchase_price"];
$displayTable .= '</th>';
$displayTable .= '<th width="3%" class="tabletop tabletopnolink">';
$displayTable .= $Lang["libms"]["report"]["book_status"];
$displayTable .= '</th>';
$displayTable .= '<th width="3%" class="tabletop tabletopnolink">';
$displayTable .= $Lang['libms']['bookmanagement']['StocktakeStatus'];
$displayTable .= '</th>';
if ($writeoffResult[0]) {
    $displayTable .= '<th width="6%" class="tabletop tabletopnolink">';
    $displayTable .= $Lang['libms']['bookmanagement']['WriteOffDate'];
    $displayTable .= '</th>';
    $displayTable .= '<th width="6%" class="tabletop tabletopnolink">';
    $displayTable .= $Lang['libms']['bookmanagement']['Reason'];
    $displayTable .= '</th>';
}
$displayTable .= '</tr>';
$displayTable .= '</thead>';
$displayTable .= '<tbody>';
$numOfResult = count($result);
// for testing [start]
// $addRow = 20 - $numOfResult%20;
// for($i=0; $i < $addRow; $i++){
// $result[] = '';
// }
// for testing [end]
$i = 0;
$numOfPages = 0;
foreach ($result as $resultKey => $resultInfo) {
    $i ++;
    
    if ($i % 20 == 0)
        $numOfPages ++;
    
    $thisBookCode = $resultInfo['BookCode'] ? $resultInfo['BookCode'] : $Lang['General']['EmptySymbol'];
    $thisBookTitle = $resultInfo['BookTitle'] ? $resultInfo['BookTitle'] : $Lang['General']['EmptySymbol'];
    $thisLocationName = $resultInfo['LocationName'] ? $resultInfo['LocationName'] : $Lang['General']['EmptySymbol'];
    $thisResult = $resultInfo['Result'] ? $resultInfo['Result'] : $Lang['General']['EmptySymbol'];
    $thisBookISBN = $resultInfo['ISBN'] ? $resultInfo['ISBN'] : $Lang['General']['EmptySymbol'];
    $thisBookCallNum = $resultInfo['CallNum'] ? ($resultInfo['CallNum'] . " " . $resultInfo['CallNum2']) : $Lang['General']['EmptySymbol'];
    $thisBookAuthor = $resultInfo['ResponsibilityBy1'] ? $resultInfo['ResponsibilityBy1'] : $Lang['General']['EmptySymbol'];
    $thisBookPublishYear = $resultInfo['PublishYear'] ? $resultInfo['PublishYear'] : $Lang['General']['EmptySymbol'];
    $thisBookPublisher = $resultInfo['Publisher'] ? $resultInfo['Publisher'] : $Lang['General']['EmptySymbol'];
    $thisBookEdition = $resultInfo['Edition'] ? $resultInfo['Edition'] : $Lang['General']['EmptySymbol'];
    $thisBarcode = $resultInfo['BarCode'] ? $resultInfo['BarCode'] : $Lang['General']['EmptySymbol'];
    $thisBookListPrice = $resultInfo['ListPrice'] ? $resultInfo['ListPrice'] : $Lang['General']['EmptySymbol'];
    $thisBookPurchasePrice = $resultInfo['PurchasePrice'] ? $resultInfo['PurchasePrice'] : $Lang['General']['EmptySymbol'];
    $thisRecordStatus = $resultInfo['RecordStatus'] ? $resultInfo['RecordStatus'] : $Lang['General']['EmptySymbol'];
    $thisBookAccountDate = $resultInfo['AccountDate'];
    
    if ($writeoffResult[0]) {
        $thisReason = $resultInfo['Reason'] ? $resultInfo['Reason'] : $Lang['General']['EmptySymbol'];
        $thisWriteOffDate = $resultInfo['WriteOffDate'] ? $resultInfo['WriteOffDate'] : $Lang['General']['EmptySymbol'];
    }
    
    $displayTable .= '<tr ' . ($i % 20 == 1 && $i > 1 ? 'class="page-break20"' : '') . '>';
    $displayTable .= '<td>';
    $displayTable .= $i;
    $displayTable .= '</td>';
    
    $displayTable .= '<td>' . $thisBookCode . '</td>';
    $displayTable .= '<td>' . $thisBookAccountDate . '</td>';
    $displayTable .= '<td>' . $thisBarcode . '</td>';
    $displayTable .= '<td>' . $thisBookTitle . '</td>';
    $displayTable .= '<td>';
    $displayTable .= $thisBookISBN;
    $displayTable .= '</td>';
    $displayTable .= '<td>';
    $displayTable .= $thisBookCallNum;
    $displayTable .= '</td>';
    $displayTable .= '<td>';
    $displayTable .= $thisBookAuthor;
    $displayTable .= '</td>';
    $displayTable .= '<td>';
    $displayTable .= $thisBookPublisher;
    $displayTable .= '</td>';
    $displayTable .= '<td>';
    $displayTable .= $thisBookPublishYear;
    $displayTable .= '</td>';
    $displayTable .= '<td>';
    $displayTable .= $thisBookEdition;
    $displayTable .= '</td>';
    $displayTable .= '<td>' . $thisLocationName . '</td>';
    $displayTable .= '<td>' . $thisBookListPrice . '</td>';
    $displayTable .= '<td>' . $thisBookPurchasePrice . '</td>';
    $displayTable .= '<td>';
    $displayTable .= $thisRecordStatus;
    $displayTable .= '</td>';
    $displayTable .= '<td>' . $thisResult . '</td>';
    if ($writeoffResult[0]) {
        $displayTable .= '<td>';
        $displayTable .= $thisWriteOffDate;
        $displayTable .= '</td>';
        $displayTable .= '<td>';
        $displayTable .= $thisReason;
        $displayTable .= '</td>';
    }
    $displayTable .= '</tr>';
}

if ($i % 20 != 0)
    $numOfPages ++;

$displayTable .= '</table>';
$displayTable .= '</td>';
$displayTable .= '</tr>';

$displayTable .= '</tbody>';

$displayTable .= '<tfoot><tr><td>';
$displayTable .= '<table id="footer" style="border-top:solid;border-bottom:solid;width:100%"><tr><td>' . $Lang['libms']['settings']['stocktake']['stocktakeDate'] . ': ' . $thisStockTakeSchemeDisplay . '</td><td align="right">' . $Lang["libms"]["report"]["printdate"] . ': ' . date('Y-m-d') . '</td></tr>';
$displayTable .= '<tr><td>&nbsp;</td><td id="page_num" align="right"></td></tr>';
$displayTable .= '</table>';
$displayTable .= '</td></tr></tfoot>';

$displayTable .= '</table>';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type" content="text/html" Charset="UTF-8" />
<META Http-Equiv="Cache-Control" Content="no-cache">

	<style type='text/css'>
html, body {
	margin: 0px;
	padding: 0px;
}

/*@media print and (width: 21cm) and (height: 29.7cm) {
    @page {
       margin: 3cm;
    }
 }
 
@media print
{
	
  table { page-break-after:auto }
  tr    { page-break-inside:avoid; page-break-after:auto }
  td    { page-break-inside:avoid; page-break-after:auto }
  thead { display:table-header-group }
  tfoot { display:table-footer-group }
  
  #footer {
         display:block;
		   position:fixed;
		   bottom:0px;
		   width:100%;
  } 
    
    
}*/
@media print and (width: 21cm) and (height: 29.7cm) {
	@page {
		margin: 3cm;
		size: landscape;
	}
}

@media print {
	@page {
		size: A4 landscape;
		margin: .1cm .3cm;
	}
	table {
		font-size: 10px;
		width: 29cm
	}
	tr.page-break20 {
		page-break-inside: avoid;
		page-break-before: always;
	}
	td {
		line-height: 100%
	}
	thead {
		display: table-header-group
	}
	tfoot {
		display: table-footer-group
	}
	/*#footer {
		position:fixed;
		bottom:5px;
		width:100%;
	}*/
	#footer td {
		width: 50%;
		align: right
	}
	#page_num:after {
		counter-increment: page_num;
		content: "Page " counter(page_num) " of <?=$numOfPages?>";
	}
}
</style>

</head>
<body>
<?php echo $displayTable;?>
</body>
</html>

<?php
intranet_closedb();
include_once ($PATH_WRT_ROOT . "templates/" . $LAYOUT_SKIN ."/layout/print_footer.php");
?>