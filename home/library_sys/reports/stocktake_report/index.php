<?php

// Using: 

/***************************************
 * 
 * Date:	2015-07-21 (Cameron)
 * 			- Add radio option "All" to BookCategory
 * 			- Add order by BookCategoryCode in retrieving BookCategory option list to compatible with other reports
 * 
 * Date:	2015-01-23 (Henry)
 * Details:	Added Category2 filter
 * 
 * Date:	2014-02-13 (Henry)
 * Details:	Added filter Book Category
 * 
 * Date:	2013-06-19 (Rita)
 * Details: Amend access right checking
 * 
 * Date: 2013-06-04 (Rita)
 * Details: Create this page
 ***************************************/
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();

if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['stock-take report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$libms = new liblms();
$linterface = new interface_html();

# Current Page Info
$CurrentPageArr['LIBMS'] = 1;
$TAGS_OBJ[] = array($Lang['libms']['reporting']['stocktakeReport']);
$CurrentPage = "PageReportingStocktakeReport";

# Top menu highlight setting
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

################### Stocktake Scheme Selection Box Start ######################
$sql = $libms->GET_STOCKTAKE_SCHEME_SQL('',1);
$stockTakeSchemeArr = $libms->returnArray($sql);
## Default StocktakeSchemeID
if($stocktakeSchemeID=='')
{
	$stocktakeSchemeID = $stockTakeSchemeArr[0]['StocktakeSchemeID'];
}
$numOfstockTakeSchemeArr = count($stockTakeSchemeArr);
for($i=0;$i<$numOfstockTakeSchemeArr;$i++){
	$thisStockTakeSchemeId = $stockTakeSchemeArr[$i]['StocktakeSchemeID'];
	$thisStockTakeDisplay = $stockTakeSchemeArr[$i]['SchemeTitle'] . ' ~ (' .  $stockTakeSchemeArr[$i]['StartDate']  . ' - ' .  $stockTakeSchemeArr[$i]['EndDate'] . ')';
	$stockTakeSchemeSelectionArr[]= array($thisStockTakeSchemeId,$thisStockTakeDisplay);

}

$stockTakeSelectionBox = $linterface->GET_SELECTION_BOX($stockTakeSchemeSelectionArr, 'name="StocktakeSchemeID" id="StocktakeSchemeID" ', '', $stocktakeSchemeID);
#################### Stocktake Scheme Selection Box End ########################

############################### Stocktake Status Start ###############################
$i = 0;
$stocktakeStatusArr = array();

foreach ($Lang['libms']['stocktake_status'] as $key => $value)
{
	$stocktakeStatusArr[$i][0] = trim($key);
	$stocktakeStatusArr[$i][1] = trim($value);
	$i++;
}
if($selectedStocktakeStatus==''){
	$selectedStocktakeStatus = $stocktakeStatusArr[0][0];
}
$stockTakeStatusSelectionBox = $linterface->GET_SELECTION_BOX($stocktakeStatusArr, " name='selectedStocktakeStatus[]' id='stocktakeStatus' multiple ", '', $selectedStocktakeStatus);
$selectAllBtn = '<input name="submit1" type="button" id="selectAllBtnStocktakeStatus" onClick="javascript:Js_Select_All(\'stocktakeStatus\')" class="formsmallbutton" value="Select All" />';
$stockTakeStatusSelectionBoxDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($stockTakeStatusSelectionBox, $selectAllBtn);
############################### Stocktake Status End ###############################

############################### Resources Type Start ###############################
$resourceTypeArr = $libms->GET_RESOURCES_TYPE_INFO();	
//if($selectedResourcesTypeCode=='')
//{
//	$selectedResourcesTypeCode = $resourceTypeArr[0]['ResourcesTypeCode'];
//}
$numOfResourceTypeArr = count($resourceTypeArr);	
$thisResourcesTypeSelectionArr = array();
for($i=0;$i<$numOfResourceTypeArr;$i++){
	$thisResourcesTypeCode = trim($resourceTypeArr[$i]['ResourcesTypeCode']);
	$thisDescriptionEn = $resourceTypeArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $resourceTypeArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisResourcesTypeSelectionArr[]= array($thisResourcesTypeCode,$thisDescription);				
}	
$resourcesTypeSelectionBox = $linterface->GET_SELECTION_BOX($thisResourcesTypeSelectionArr, 'name="selectedResourcesTypeCode[]" id="ResourcesTypeCode" size="10" multiple', $Lang["libms"]["report"]["cannot_define"], '');

$selectAllBtn2 = '<input name="submit2" type="button" id="selectAllBtnResourcesTypeCode" onClick="javascript:Js_Select_All(\'ResourcesTypeCode\')" class="formsmallbutton" value="Select All" />';
$resourcesTypeCodeSelectionBoxDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($resourcesTypeSelectionBox, $selectAllBtn2);
############################### Stocktake Status End ###############################

###############################  Circulation Start ###############################
$circulationArr = $libms->GET_CIRCULATION_TYPE_INFO();	
//if($CirculationTypeCode=='')
//{
//	$CirculationTypeCode = $circulationArr[0]['CirculationTypeCode'];
//}
$numOfResourceTypeArr = count($circulationArr);	
$thisCirculationTypeSelectionArr = array();
for($i=0;$i<$numOfResourceTypeArr;$i++){
	$thisCirculationTypeCode = trim($circulationArr[$i]['CirculationTypeCode']);
	$thisDescriptionEn = $circulationArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $circulationArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisCirculationTypeSelectionArr[]= array($thisCirculationTypeCode,$thisDescription);				
}	
$circulationTypeSelectionBox = $linterface->GET_SELECTION_BOX($thisCirculationTypeSelectionArr, 'name="selectedCirculationType[]" id="CirculationType" size="10" multiple ', $Lang["libms"]["report"]["cannot_define"], '');
$selectAllBtn3 = '<input name="submit2" type="button" id="selectAllBtnCirculationType" onClick="javascript:Js_Select_All(\'CirculationType\')" class="formsmallbutton" value="Select All" />';
$circulationTypeSelectionBoxDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($circulationTypeSelectionBox, $selectAllBtn3);	
###############################  Circulation End   ###############################

###############################  Category Start ###############################
$sql = "SELECT * FROM LIBMS_BOOK_CATEGORY WHERE BookCategoryType=1 ORDER BY BookCategoryCode";	
$categoryArr = $libms->returnArray($sql);

$numOfCategoryTypeArr = count($categoryArr);	
$thisCategoryTypeSelectionArr = array();
for($i=0;$i<$numOfCategoryTypeArr;$i++){
	$thisCategoryTypeCode = trim($categoryArr[$i]['BookCategoryCode']);
	$thisDescriptionEn = $categoryArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $categoryArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisCategoryTypeSelectionArr[]= array($thisCategoryTypeCode,$thisDescription);				
}	
$categoryTypeSelectionBox = $linterface->GET_SELECTION_BOX($thisCategoryTypeSelectionArr, 'name="selectedBookCategory[]" id="BookCategory" size="10" multiple ', $Lang["libms"]["report"]["cannot_define"], '', true);
$selectAllBtn5 = '<input name="submit2" type="button" id="selectAllBtnBookCategory" onClick="javascript:Js_Select_All(\'BookCategory\')" class="formsmallbutton" value="Select All" />';
$bookCategorySelectionBoxDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($categoryTypeSelectionBox, $selectAllBtn5);	
###############################  Category End   ###############################

###############################  Category2 Start ###############################
$sql = "SELECT * FROM LIBMS_BOOK_CATEGORY WHERE BookCategoryType=2 ORDER BY BookCategoryCode";	
$categoryArr = $libms->returnArray($sql);

$numOfCategoryTypeArr = count($categoryArr);	
$thisCategoryTypeSelectionArr = array();
for($i=0;$i<$numOfCategoryTypeArr;$i++){
	$thisCategoryTypeCode = trim($categoryArr[$i]['BookCategoryCode']);
	$thisDescriptionEn = $categoryArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $categoryArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisCategoryTypeSelectionArr[]= array($thisCategoryTypeCode,$thisDescription);				
}	
$categoryTypeSelectionBox = $linterface->GET_SELECTION_BOX($thisCategoryTypeSelectionArr, 'name="selectedBookCategory[]" id="BookCategory2" size="10" multiple ', $Lang["libms"]["report"]["cannot_define"], '', true);
$selectAllBtn5 = '<input name="submit2" type="button" id="selectAllBtnBookCategory2" onClick="javascript:Js_Select_All(\'BookCategory2\')" class="formsmallbutton" value="Select All" />';
$bookCategorySelectionBoxDiv2 = $linterface->Get_MultipleSelection_And_SelectAll_Div($categoryTypeSelectionBox, $selectAllBtn5);	
###############################  Category2 End   ###############################

###############################  Location Start ###############################
$locationArr = $libms->GET_LOCATION_INFO();	
//if($LocationCode=='')
//{
//	$LocationCode = $locationArr[0]['LocationCode'];
//}
$numOfLocationArr = count($locationArr);	
$thisLocationSelectionArr = array();
for($i=0;$i<$numOfLocationArr;$i++){
	$thisLocationCode = trim($locationArr[$i]['LocationCode']);
	$thisDescriptionEn = $locationArr[$i]['DescriptionEn'];
	$thisDescriptionChi = $locationArr[$i]['DescriptionChi'];
	$thisDescription = Get_Lang_Selection($thisDescriptionChi, $thisDescriptionEn); 		
	$thisLocationSelectionArr[]= array($thisLocationCode,$thisDescription);				
}	
$locationSelectionBox = $linterface->GET_SELECTION_BOX($thisLocationSelectionArr, 'name="selectedLocation[]" id="Location" size="10" multiple', $Lang["libms"]["report"]["cannot_define"], '');

$selectAllBtn4 = '<input name="submit2" type="button" id="selectAllBtnCirculationType" onClick="javascript:Js_Select_All(\'Location\')" class="formsmallbutton" value="Select All" />';
$locationSelectionBoxDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($locationSelectionBox, $selectAllBtn4);	
###############################  Location End   ###############################

$linterface->LAYOUT_START();

?>
<script language="javascript">

$(document).ready(function(){
	$('#selectAllBtnStocktakeStatus').click(function(){ 		
		$('#stocktakeStatus option').attr('selected', true);
	});
	
	$('input[name="BookCategoryType"]').change(function(){
		if($('#BookCategoryTypeAll').is(':checked'))
		{
			$('#table_CategoryCode1').attr('style','display:none');
			$('#table_CategoryCode2').attr('style','display:none');
			$('#BookCategory option').attr('selected', false);
			$('#BookCategory2 option').attr('selected', false);
		}
		else if ($('#BookCategoryType1').is(':checked'))
		{				
			$('#table_CategoryCode1').attr('style','display:block');
			$('#table_CategoryCode2').attr('style','display:none');
			$('#BookCategory2 option').attr('selected', false);
		}
		else if ($('#BookCategoryType2').is(':checked'))
		{
			$('#table_CategoryCode1').attr('style','display:none');
			$('#table_CategoryCode2').attr('style','display:block');
			$('#BookCategory option').attr('selected', false);
		}
	});
	
});	
	
function Js_Select_All(id){
	$('#'+id + ' option').attr('selected', 'selected');	
}

function Js_Check_Form(obj){
	var error = 0 ;
	var stocktakeLength = $('#stocktakeStatus option:selected').length;
//	var resourcesTypeCodeLength = $('#ResourcesTypeCode option:selected').length;
//	var circulationTypeLength = $('#CirculationType option:selected').length;
//	var locationLength = $('#Location option:selected').length;
//	
	if(stocktakeLength==0){
		alert('<?=$i_alert_pleaseselect.$Lang['libms']['bookmanagement']['stocktakeRecord']?>');
		error++;
	}
//	else if(resourcesTypeCodeLength==0)
//	{		 
//		alert('<?=$i_alert_pleaseselect.$Lang["libms"]["settings"]["resources_type"]?>');
//		error++;
//	}
//	else if(circulationTypeLength==0)
//	{
//		alert('<?=$i_alert_pleaseselect.$Lang["libms"]["settings"]["circulation_type"]?>');
//		error++;
//	}
//	else if(locationLength==0)
//	{
//		alert('<?=$i_alert_pleaseselect.$Lang["libms"]["settings"]["book_location"]?>');		
//		error++;
//	}
//	

	if ($('#BookCategoryType1').is(':checked')) {
		if($('#BookCategory option:checked').length == 0) {
			$('#BookCategory option').attr('selected', true);	
		}						
	}
	if ($('#BookCategoryType2').is(':checked')) {
		if($('#BookCategory2 option:checked').length == 0) {
			$('#BookCategory2 option').attr('selected', true);	
		}						
	}

	if(error==0)
	{
		obj.submit();
	}

}

</script>

<form name="form1" method="post" action="result.php?clearCoo=1">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td width="13">&nbsp;</td>
		<td class="main_content">
       	<div class="report_option report_option_title"></div>
		<div class="table_board">
		<div class="table_board">
		<table class="form_table">
	        <tr>
	          <td class="field_title"><?=$Lang['libms']['bookmanagement']['stocktakeRecord']?></td>
	          <td><?=$stockTakeSelectionBox?></td>
	        </tr>
	        <col class="field_title" />
	        <col  class="field_c" />
	        <tr>
	          <td class="field_title"><?=$Lang['libms']['bookmanagement']['StocktakeStatus']?></td>
	          <td>
	          <?=$stockTakeStatusSelectionBoxDiv?>
	          </td>
	        </tr>
	        <tr class="form_table ">
	          <td class="field_title"><?=$Lang["libms"]["settings"]["book_location"]?></td>
	          <td><span class="row_content">
	               <?=$locationSelectionBoxDiv?>
	          </td>
	        </tr>
			<tr class="form_table ">
	    		<td class="field_title"><?=$Lang["libms"]["settings"]["resources_type"]?></td>
	          	<td>
				<span class="row_content">				
				<?=$resourcesTypeCodeSelectionBoxDiv?>
				</td>
			</tr>			
	        <tr class="form_table ">
	          <td class="field_title"><?=$Lang["libms"]["settings"]["circulation_type"]?></td>
	          <td>
				<span class="row_content">
				<?=$circulationTypeSelectionBoxDiv?>
			  </td>
	        </tr>
	        <tr class="form_table ">
	          <td class="field_title"><?=$Lang['libms']['report']['book_category']?></td>
	          <td><span class="row_content">
	          	<table class="inside_form_table">
					<tr>
						<td valign="top">
							<?
							$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
							foreach ($result as $row){
								$book_category_name[] = htmlspecialchars($row['value']);
							}
							?>
							<?=$linterface->Get_Radio_Button("BookCategoryTypeAll", "BookCategoryType", 0, ($BookCategoryType==0?true:false), "", $Lang["libms"]["report"]["fine_status_all"])?>								
							<?=$linterface->Get_Radio_Button("BookCategoryType1", "BookCategoryType", 1, ($BookCategoryType==1?true:false), "", $book_category_name[0]?$book_category_name[0]:$Lang['libms']['report']['book_category'].' 1')?>
							<?=$linterface->Get_Radio_Button("BookCategoryType2", "BookCategoryType", 2, ($BookCategoryType==2?true:false), "", $book_category_name[1]?$book_category_name[1]:$Lang['libms']['report']['book_category'].' 2')?>
						</td>
					</tr>
					<tr class="table_CategoryCode" id="table_CategoryCode1" style='<?=($BookCategoryType==1?'':'display:none;')?>'>
						<td valign="top">
							<?=$bookCategorySelectionBoxDiv?>
						</td>
					</tr>
					<tr class="table_CategoryCode" id="table_CategoryCode2" style='<?=($BookCategoryType==2?'':'display:none;')?>'>
						<td valign="top">
							<?=$bookCategorySelectionBoxDiv2?>
						</td>
					</tr>
				</table>
	          </td>
	        </tr>	        
	     </table>
      <p class="spacer"></p>
    </div>
    <p class="spacer"></p>
      </div>
      <div class="edit_bottom">
        <p class="spacer"></p>
        <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "Js_Check_Form(this.form)", "GenerateBtn")?>        
        <p class="spacer"></p>
      </div>
      <p>&nbsp;</p></td>
      <td width="11">&nbsp;</td>
    </tr>
  </table>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
