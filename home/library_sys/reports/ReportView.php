<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../";
$MODULE_ROOT = "../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");

class ReportView{
	var $linterface,$libms;
	public function __construct($Current_Page){
		global $TAGS_OBJ, $MODULE_OBJ;
		global $Lang, $home_header_no_EmulateIE7;
		$home_header_no_EmulateIE7 = true;
		$this->linterface = new interface_html();
		$libms = new liblms();
		if (empty($CurrentPageArr['LIBMS']))
			$CurrentPageArr['LIBMS'] = 1;
		if (empty($Current_Page))
			$Current_Page = "PageReport";
		$TAGS_OBJ[] = array($Lang['libms']['reporting'][$Current_Page]);
		$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN($Current_Page);
		$libms->MODULE_AUTHENTICATION($Current_Page);
	}
	
	public function getHeader(){
		
		$xmsg = $_SESSION['LIBMS']['xmsg'];
		unset($_SESSION['LIBMS']['xmsg']);
		
		$x='';
		
		
		ob_start();
		$this->linterface->LAYOUT_START();
		$x .= ob_get_contents();
		ob_end_clean();
		
		
		if(!empty($xmsg)){
		$x .= 
<<<EOF
<center>
	<div align="center" style='width: 1%'>
		<table border="0" cellpadding="3" cellspacing="0" class="systemmsg">
			<tr>
				<td nowrap="nowrap"><font color="green"><?=$xmsg?></font></td>
			</tr>
		</table>
	</div>
</center>
EOF;
		}
		
		
		return $x;
		
	}

	public function getFooter(){
		$x='';

		ob_start();
		$this->linterface->LAYOUT_STOP();
		$x .= ob_get_contents();
		ob_end_clean();
		
		return $x;
	}
	
	public function getView(){
		$x='';
		$x .= $this->getHeader();
		
		$x .= $this->getFooter();
	}
	/**
	 * Convert SQL_result_to_2Darray 
	 * no checking have been done
	 * @param SQL result array $data
	 * @param 1D array of header $headers, if omited SQL fields name will be used.
	 * 
	 * @return 2D array with headers or NULL if no $data to process
	 */
	public static function SQL_result_to_2Darray($data, $headers){
		if (empty($data)){
			return NULL;
		}

		$row_count = 0;
		foreach($data as $row){
			foreach ($row as $fieldname => $cellvalue){ 
				if ($row_count == 0 || empty($headers)){
					$fieldnames[] = $fieldname;				
				}
				$cellvalues[] = $cellvalue;
			}
			
			if ($row_count == 0 ){
				$array_2D[] = empty($headers)? $fieldnames : $headers;
			}
			$array_2D[] = $cellvalues;
			
			unset($cellvalues,$fieldnames);
			$row_count++;
		}
		unset($row_count);

		return $array_2D; 
	}
	

}
