<?php


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();


$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageStats1";

$libms->MODULE_AUTHENTICATION($CurrentPage);

//$linterface = new interface_html("libms.html");
$linterface = new interface_html();

############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array($Lang["libms"]["stats"]["frequency"]);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();



?>
<script language="javascript">
<!--
function checkRemove(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}
//-->
</script>

<form name="form1" method="post" action="">
<!--###### Content Board Start ######-->
			  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                  
                    <tr> 
                      <td width="13" >&nbsp;</td>
                      <td class="main_content"><!---->
                        <!--#### Navigator start ####-->
                        <!--<div class="navigation"><a href="#">User List</a>New User<br />
                      </div>-->
                        <!--#### Navigator end ####-->
                        <div class="report_option report_show_option"><a href="#">顯示統計選項</a></div>
       			 <!-- <div class="table_board">
                  	
                        <table class="form_table">
                          <tr>
                            <td class="field_title">報告類型</td>
                            <td><input type="radio" checked="checked" value="0" name="Title" />
                            全班學生列表
                              <input type="radio" value="1" name="Title" />
                            個人報告</td>
                          </tr>

                          <col class="field_title" />
                          <col  class="field_c" />
                          <tr>
                            <td class="field_title">學年</td>
                            <td><select name="select" id="select">
                              <option>所有學年</option>
                              <option>20009-2010</option>
                              <option>2008-2009</option>
                              <option>2007-2008</option>
                            </select>                            </td>
                          </tr>
                          <tr>
                            <td class="field_title">班級</td>
                            <td>
                              <select name="select2" size="5">
                                <option selected="selected">1A</option>
                                <option>1B</option>
                                <option>1C</option>
                                <option>1D</option>
                              </select>
                             
                              <input name="submit2" type="button" class="formsmallbutton" value="全選" />
                              <br />
                            <span class="tabletextremark">(可按Ctrl鍵選擇多項)</span>                            </td>
                          </tr>
                          <tr>
                            <td class="field_title">學生</td>
                            <td>
                              <select name="select3" size="10" multiple="multiple">
                                <option selected="selected">1A- 01 陳小明</option>
                                <option>1A-02 張家明</option>
                                <option>1A-03 張小敏</option>
                                <option>1A-04 鍾永仁</option>
                                <option>1A-05 郭美麗</option>
                                <option>1A-06 李家仁</option>
                                <option>1A-07 黃帶敏</option>
                                <option>1A-08 馮嘉華</option>
                                <option>1A-09 許婷婷</option>
                                <option>1A-10 葉芷欣</option>
                              </select>    <input name="submit2" type="button" class="formsmallbutton" value="全選" />
                              <br />
                            <span class="tabletextremark">(可按Ctrl鍵選擇多項)</span> 
                           </td>
                          </tr>
                        </table>
                    <p class="spacer"></p>
                      </div>
                      <div class="edit_bottom">
                       
                        <p class="spacer"></p>
                        <input name="submit" type="button" class="formbutton" onclick="MM_goToURL('parent','student_profile_report_profile_stu_list.htm');return document.MM_returnValue" value="產生報告" />
                        <p class="spacer"></p>
                      </div>-->
                      
                      	<div class="table_board">
        			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td valign="bottom"><!--# Filter layer start #-->
                             <!-- <div class="table_filter">
                                <div class="selectbox_group selectbox_group_filter"> <a href="#" onclick="MM_showHideLayers('status_option','','show')">Select Status</a> </div>
                                <p class="spacer"></p>
                                <div id="status_option" class="selectbox_layer selectbox_group_layer">
                                 <input type="checkbox" name="checkbox2" id="checkbox2"checked="checked" />
                                        Waiting for Approval<br />
                                        <input type="checkbox" name="checkbox2" id="checkbox2"checked="checked" />
                                        Approved<br />
                                        <input type="checkbox" name="checkbox2" id="checkbox2"checked="checked" />
                                        Rejected
                                        <p class="spacer"></p>
                                        <div class="edit_bottom">
                                          <input name="submit332" type="button" class="formsubbutton" value="Apply"  
                                                                    onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" />
                                          <input name="submit332" type="button" class="formsubbutton" value="Cancel"  
                                                                    onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" 
                                                                    onclick="MM_showHideLayers('status_option','','hide')" />
                                      </div>
                                </div>
                              </div>-->
                            <!--# Filter layer end #-->
                            <br />
                            <div class="table_filter"> 學生借用閱讀資料頻次2011-2012</div>
                            <div class="thumb_list_tab"><a href="stats1_3.php"><span>圖表</span></a><em>|</em><a href="stats1_2.php" class="thumb_list_tab_on"><span>列表</span></a></div>
                            <br  style="clear:both"/></td>
                          <td valign="bottom">&nbsp;</td>
                        </tr>
                      </table>
        			  <table class="common_table_list view_table_list">
									
									<tr>
									  <th colspan="8" class="sub_row_top"><div align="center">2011-2012 上學期 (05/09/2011-20/12/2011)</div></th>
                        </tr>
                                    <tr class=" ">
										<th>&nbsp;</th>
								      <th>F1</th>
									  <th>F2</th>
									  <th> F3</th>
									  <th>F4</th>
									  <th> F5</th>
									  <th> F6</th>
									  <th><a href="#">全校</a></th>
					    </tr>
									<tr>
                                      <td>每周一次或以上</td>
									  <td>1.96%</td>
									  <td>0.00%</td>
									  <td>1.96%</td>
									  <td>0.00%</td>
									  <td>1.96%</td>
									  <td>0.00%</td>
									  <td>0.87%</td>
					    </tr>
									<tr>
                                      <td>每兩星期一次</td>
									  <td>5.89%</td>
									  <td class="row_temp_resign"> 5.29%</td>
									  <td>5.89%</td>
									  <td class="row_temp_resign"> 5.29%</td>
									  <td>5.89%</td>
									  <td class="row_temp_resign"> 5.29%</td>
									  <td> 5.56%</td>
					    </tr>
									<tr>
                                      <td>每月一次</td>
									  <td>13.36%</td>
									  <td><span class="row_temp_resign"> 11.66%</span></td>
									  <td>13.36%</td>
									  <td><span class="row_temp_resign"> 11.66%</span></td>
									  <td>13.36%</td>
									  <td><span class="row_temp_resign"> 11.66%</span></td>
									  <td>12.41%</td>
					    </tr>
									<tr>
                                      <td>每月少於一次</td>
									  <td> 44.01%</td>
									  <td class="row_temp_resign"> 38.26%</td>
									  <td> 44.01%</td>
									  <td class="row_temp_resign"> 38.26%</td>
									  <td> 44.01%</td>
									  <td class="row_temp_resign"> 38.26%</td>
									  <td> 40.80%</td>
					    </tr>
									<tr>
                                      <td>從不</td>
									  <td>34.77%
&nbsp;</td>
									  <td class="row_resign"><span class="row_temp_resign"> 44.79% </span></td>
									  <td>34.77%
									    &nbsp;</td>
									  <td class="row_resign"><span class="row_temp_resign"> 44.79% </span></td>
									  <td>34.77%
									    &nbsp;</td>
									  <td class="row_resign"><span class="row_temp_resign"> 44.79% </span></td>
									  <td>40.36%</td>
					    </tr>
								
                                <tr>
									  <th colspan="8" class="sub_row_top"><div align="center">2011-2012 下學期 (06/02/2012-31/05/2012)</div></th>
                        </tr>
                                    <tr class=" ">
										<th>&nbsp;</th>
								        <th>F1</th>
								        <th>F2</th>
								        <th> F3</th>
								        <th>F4</th>
								        <th> F5</th>
								        <th> F6</th>
							          <th><a href="#">全校</a></th>
					    </tr>
									<tr>
                                      <td>每周一次或以上</td>
									  <td>0.79%</td>
									  <td>0.00%</td>
									  <td>0.79%</td>
									  <td>0.00%</td>
									  <td>0.79%</td>
									  <td>0.00%</td>
									  <td>0.35%</td>
					    </tr>
									<tr>
                                      <td>每兩星期一次</td>
									  <td> 4.32%</td>
									  <td class="row_temp_resign"> 2.33%</td>
									  <td> 4.32%</td>
									  <td class="row_temp_resign"> 2.33%</td>
									  <td> 4.32%</td>
									  <td class="row_temp_resign"> 2.33%</td>
									  <td> 2.78%</td>
					    </tr>
									<tr>
                                      <td>每月一次</td>
									  <td>11.59%</td>
									  <td><span class="row_temp_resign"> 6.22%</span></td>
									  <td>11.59%</td>
									  <td><span class="row_temp_resign"> 6.22%</span></td>
									  <td>11.59%</td>
									  <td><span class="row_temp_resign"> 6.22%</span></td>
									  <td> 9.03%</td>
					    </tr>
									<tr>
                                      <td>每月少於一次</td>
									  <td>38.90%</td>
									  <td class="row_temp_resign"> 25.97%</td>
									  <td>38.90%</td>
									  <td class="row_temp_resign"> 25.97%</td>
									  <td>38.90%</td>
									  <td class="row_temp_resign"> 25.97%</td>
									  <td>31.68%</td>
					    </tr>
									<tr>
                                      <td>從不</td>
									  <td> 44.40% </td>
									  <td class="row_resign"><span class="row_temp_resign"> 65.47% </span></td>
									  <td> 44.40% </td>
									  <td class="row_resign"><span class="row_temp_resign"> 65.47% </span></td>
									  <td> 44.40% </td>
									  <td class="row_resign"><span class="row_temp_resign"> 65.47% </span></td>
									  <td> 56.16% </td>
					    </tr>
								
                                <tr>
									  <th colspan="8" class="sub_row_top"><div align="center">2011-2012 全年 (05/09/2011-31/05/2012)</div></th>
                        </tr>
                                    <tr class=" ">
										<th>&nbsp;</th>
								        <th>F1</th>
								        <th>F2</th>
								        <th> F3</th>
								        <th>F4</th>
								        <th> F5</th>
								        <th> F6</th>
							          <th><a href="#">全校</a></th>
					    </tr>
									<tr>
                                      <td>每周一次或以上</td>
									  <td>0.79%</td>
									  <td>0.00%</td>
									  <td>0.79%</td>
									  <td>0.00%</td>
									  <td>0.79%</td>
									  <td>0.00%</td>
									  <td>0.35%</td>
					    </tr>
									<tr>
                                      <td>每兩星期一次</td>
									  <td> 3.93%</td>
									  <td class="row_temp_resign"> 1.40%</td>
									  <td> 3.93%</td>
									  <td class="row_temp_resign"> 1.40%</td>
									  <td> 3.93%</td>
									  <td class="row_temp_resign"> 1.40%</td>
									  <td> 2.34%</td>
					    </tr>
									<tr>
                                      <td>每月一次</td>
									  <td> 13.16%</td>
									  <td><span class="row_temp_resign"> 8.86%</span></td>
									  <td> 13.16%</td>
									  <td><span class="row_temp_resign"> 8.86%</span></td>
									  <td> 13.16%</td>
									  <td><span class="row_temp_resign"> 8.86%</span></td>
									  <td> 9.29%</td>
					    </tr>
									<tr>
                                      <td>每月少於一次</td>
									  <td> 58.55%</td>
									  <td class="row_temp_resign"> 52.57%</td>
									  <td> 58.55%</td>
									  <td class="row_temp_resign"> 52.57%</td>
									  <td> 58.55%</td>
									  <td class="row_temp_resign"> 52.57%</td>
									  <td> 56.86%</td>
					    </tr>
									<tr>
                                      <td>從不</td>
									  <td> 23.58% </td>
									  <td class="row_resign"><span class="row_temp_resign"> 37.17% </span></td>
									  <td> 23.58% </td>
									  <td class="row_resign"><span class="row_temp_resign"> 37.17% </span></td>
									  <td> 23.58% </td>
									  <td class="row_resign"><span class="row_temp_resign"> 37.17% </span></td>
									  <td> 31.16% </td>
					    </tr>
						<tr class="row_avaliable">
						  <td>&nbsp;</td>
									  <td>&nbsp;</td>
									  <td>&nbsp;</td>
									  <td>總借出次數</td>
									  <td>&nbsp;</td>
									  <td>總借出次數</td>
									  <td>總借出次數</td>
						  <td><div align="right"><span  class="share_sub_list_current">6589</span></div></td>
					    </tr>
						  </table>
					    </div>
                      	<div class="edit_bottom">
                          <!-- <span> Last updated : 2 days ago by Admin</span>-->
                          <p class="spacer"></p>
                      	  <input name="submit" type="button" class="formbutton" onclick="alert('coming soon')" value="列印" />
                          <input name="submit2" type="button" class="formbutton" onclick="alert('coming soon')" value="滙出CSV" />
                          <p class="spacer"></p>
                   	    </div>
                      	<br />
				      <br />
				      <br />
				      <p>&nbsp;</p></td>
                      <td width="11" >&nbsp;</td>
                    </tr>
                  </table>
			  <!--###### Content Board End ######-->
</form><?php

$linterface->LAYOUT_STOP();

 
intranet_closedb();


?>
