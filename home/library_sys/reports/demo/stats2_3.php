<?php


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();


$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageStats2";

$libms->MODULE_AUTHENTICATION($CurrentPage);

//$linterface = new interface_html("libms.html");
$linterface = new interface_html();

############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array($Lang["libms"]["stats"]["loan_students"]);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();



?>
<script language="javascript">
<!--
function checkRemove(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}
//-->
</script>

<form name="form1" method="post" action="">
<!--###### Content Board Start ######-->
			  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                 
                    <tr> 
                      <td width="13" >&nbsp;</td>
                      <td class="main_content"><!---->
                        <!--#### Navigator start ####-->
                        <!--<div class="navigation"><a href="#">User List</a>New User<br />
                      </div>-->
                        <!--#### Navigator end ####-->
                        <div class="report_option report_show_option"><a href="admin_report_02_result_01_chart_form.htm">顯示統計選項</a></div>
       			 <!-- <div class="table_board">
                  	
                        <table class="form_table">
                          <tr>
                            <td class="field_title">報告類型</td>
                            <td><input type="radio" checked="checked" value="0" name="Title" />
                            全班學生列表
                              <input type="radio" value="1" name="Title" />
                            個人報告</td>
                          </tr>

                          <col class="field_title" />
                          <col  class="field_c" />
                          <tr>
                            <td class="field_title">學年</td>
                            <td><select name="select" id="select">
                              <option>所有學年</option>
                              <option>20009-2010</option>
                              <option>2008-2009</option>
                              <option>2007-2008</option>
                            </select>                            </td>
                          </tr>
                          <tr>
                            <td class="field_title">班級</td>
                            <td>
                              <select name="select2" size="5">
                                <option selected="selected">1A</option>
                                <option>1B</option>
                                <option>1C</option>
                                <option>1D</option>
                              </select>
                             
                              <input name="submit2" type="button" class="formsmallbutton" value="全選" />
                              <br />
                            <span class="tabletextremark">(可按Ctrl鍵選擇多項)</span>                            </td>
                          </tr>
                          <tr>
                            <td class="field_title">學生</td>
                            <td>
                              <select name="select3" size="10" multiple="multiple">
                                <option selected="selected">1A- 01 陳小明</option>
                                <option>1A-02 張家明</option>
                                <option>1A-03 張小敏</option>
                                <option>1A-04 鍾永仁</option>
                                <option>1A-05 郭美麗</option>
                                <option>1A-06 李家仁</option>
                                <option>1A-07 黃帶敏</option>
                                <option>1A-08 馮嘉華</option>
                                <option>1A-09 許婷婷</option>
                                <option>1A-10 葉芷欣</option>
                              </select>    <input name="submit2" type="button" class="formsmallbutton" value="全選" />
                              <br />
                            <span class="tabletextremark">(可按Ctrl鍵選擇多項)</span> 
                           </td>
                          </tr>
                        </table>
                    <p class="spacer"></p>
                      </div>
                      <div class="edit_bottom">
                       
                        <p class="spacer"></p>
                        <input name="submit" type="button" class="formbutton" onclick="MM_goToURL('parent','student_profile_report_profile_stu_list.htm');return document.MM_returnValue" value="產生報告" />
                        <p class="spacer"></p>
                      </div>-->
                      
                      	<div class="table_board">
        			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td valign="bottom"><!--# Filter layer start #-->
                             <!-- <div class="table_filter">
                                <div class="selectbox_group selectbox_group_filter"> <a href="#" onclick="MM_showHideLayers('status_option','','show')">Select Status</a> </div>
                                <p class="spacer"></p>
                                <div id="status_option" class="selectbox_layer selectbox_group_layer">
                                 <input type="checkbox" name="checkbox2" id="checkbox2"checked="checked" />
                                        Waiting for Approval<br />
                                        <input type="checkbox" name="checkbox2" id="checkbox2"checked="checked" />
                                        Approved<br />
                                        <input type="checkbox" name="checkbox2" id="checkbox2"checked="checked" />
                                        Rejected
                                        <p class="spacer"></p>
                                        <div class="edit_bottom">
                                          <input name="submit332" type="button" class="formsubbutton" value="Apply"  
                                                                    onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" />
                                          <input name="submit332" type="button" class="formsubbutton" value="Cancel"  
                                                                    onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" 
                                                                    onclick="MM_showHideLayers('status_option','','hide')" />
                                      </div>
                                </div>
                              </div>-->
                            <!--# Filter layer end #-->
                            <br />
                            <div class="table_filter"> 2011-2012 -各級學生借閱量 - 全年累積</div>
                            <div class="thumb_list_tab"><a href="stats2_3.php" class="thumb_list_tab_on"><span>圖表</span></a><em>|</em><a href="stats2_2.php"><span>列表</span></a></div>
                            <br  style="clear:both"/></td>
                          <td valign="bottom">&nbsp;</td>
                        </tr>
                      </table>
        			<div align="center"><img src="images/report_04.jpg" />
                    </div>
					    </div>
                      	<div class="edit_bottom">
                          <!-- <span> Last updated : 2 days ago by Admin</span>-->
                          <p class="spacer"></p>
                      	  <input name="submit" type="button" class="formbutton" onclick="MM_openBrWindow('student_profile_report_profile_stu_list_print.htm','','scrollbars=yes,resizable=yes')" value="列印" />
                          <input name="submit2" type="button" class="formbutton" onclick="MM_openBrWindow('student_profile_report_profile_stu_list_print.htm','','scrollbars=yes,resizable=yes')" value="滙出CSV" />
                          <p class="spacer"></p>
                   	    </div>
                      	<br />
				      <br />
				      <br />
				      <p>&nbsp;</p></td>
                      <td width="11" >&nbsp;</td>
                    </tr>
                  </table>
			  <!--###### Content Board End ######-->
</form><?php

$linterface->LAYOUT_STOP();

 
intranet_closedb();


?>
