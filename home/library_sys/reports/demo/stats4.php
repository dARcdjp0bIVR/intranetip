<?php


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();


$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageStats4";

$libms->MODULE_AUTHENTICATION($CurrentPage);

//$linterface = new interface_html("libms.html");
$linterface = new interface_html();

############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array($Lang["libms"]["stats"]["penalty"]);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();



?>
<script language="javascript">
<!--
function checkRemove(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}
//-->
</script>

<form name="form1" method="post" action="">
<!--###### Content Board Start ######-->
			  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="13">&nbsp;</td>
                      <td class="main_content"><!---->
                        <!--#### Navigator start ####-->
                        <!--<div class="navigation"><a href="#">User List</a>New User<br />
                      </div>-->
                        <!--#### Navigator end ####-->
                       <div class="report_option report_option_title">- 選項 - </div>
       			  <div class="table_board">
       			    <div class="table_board">
                      <table class="form_table">
                        <tr>
                          <td class="field_title"><span class="status_alert"><strong>*</strong></span>對象</td>
                          <td><label>
                            <input name="radios" type="radio" id="radio5" value="radio" />
                          </label>
                            <label for="findByGroup">組別</label>
                            <label for="findByStudent"></label>
                            <input name="radios" type="radio" id="radio6" value="radio" checked="checked" />
                            學生<br />
                             <span class="row_content">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <select name="select" id="select">
                              <option selected="selected">級別</option>
                              <option>班別</option>
                              <option>學生</option>
                                                                                    </select>&nbsp;&nbsp;&nbsp;</span>
                               <span class="row_content">                                                       
                            
                            <select name="select2" size="5">
                              <option selected="selected">F1</option>
                              <option>F2</option>
                              <option>F3</option>
                              <option>F4</option>
                              <option>F5</option>
                              <option>F6</option>
                            </select>
                            <input name="submit2" type="button" class="formsmallbutton" value="全選" />
                            <br />
                            <span class="tabletextremark">(可按Ctrl鍵選擇多項)</span><br /></span></td>
                        </tr>
                        <tr>
                          <td class="field_title">學年</td>
                          <td><label>
                            <input name="radio7" type="radio" id="radio7" value="radio" checked="checked" />
                          </label>
                            學年
<select name="select4" id="select2">
                              <option>所有學年</option>
                              <option>2012-2013</option>
                              <option selected="selected">2011-2012</option>
                              <option>2010-2011</option>
                              <option>2009-2010</option>
                              <option>2008-2009</option>
                              <option>2007-2008</option>
                                                                                </select>
                          <br />
                          <span class="row_content"><label>
                          <input name="radio7" type="radio" id="radio7" value="radio" />
                          </label>
                            由
                          <input name="textfield8" type="text" id="textfield4" value="2009-09-09" class="textboxnum"/>
                          </span>
                          <div class="table_row_tool"><a href="#" class="select_date" title="Select"></a></div>
                          <span class="row_content">&nbsp;至&nbsp;</span><span class="row_content">
                          <input name="textfield8" type="text" class="textboxnum" id="textfield4" value="2009-09-09" size="15"/>
                          </span>
                          <div class="table_row_tool"><a href="#" class="select_date" title="Select"></a></div></td>
                        </tr>
                        <col class="field_title" />
                        <col  class="field_c" />
                      </table>
       			      <p class="spacer"></p>
   			        </div>
       			    <p class="spacer"></p>
                      </div>
                      <div class="edit_bottom">
                        <!-- <span> Last updated : 2 days ago by Admin</span>-->
                        <p class="spacer"></p>
                        <input name="submit" type="button" class="formbutton" onclick="self.location='stats4_2.php'" value="產生報告" />
                        <p class="spacer"></p>
                      </div>
				      <p>&nbsp;</p></td>
                      <td width="11">&nbsp;</td>
                    </tr>
                  </table>
			  <!--###### Content Board End ######-->
</form><?php

$linterface->LAYOUT_STOP();

 
intranet_closedb();


?>
