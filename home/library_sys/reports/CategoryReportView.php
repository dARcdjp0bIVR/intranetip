<?php
# modifying by: 

/**
 * Log :
 *
 * Date:	2019-07-23 [Henry]
 * 			- use the local library of jquery
 * Date:	2016-07-15 [Cameron]
 * 			- hide the warning and empty option list if it returns nothing in ajaxSelectbox (IP2.5.7.7.1)
 * 			- don't allow to run report if user do not select target and period
 * Date:	2015-01-29 [Henry] 
 * 			modified getForm_BookCategory()
 * Date:	2014-10-07 [Henry]
 * 			added getForm_DisplayTotal()
 * Date: 	2014-05-14 [Henry]
 * 			Added school name at top of the page
 * Date:	2013-11-20 [Henry]
 * 			Fix bug: show and print the bar chart correctly
 * Date:	2013-05-21 [Cameron] 
 * 			1. Fix bug to handle undefined group
 * 			2. Do not convert displaying name to upper case in gen_categoryReportArray
 * Date: 	2013-05-07 [Yuen]
 * 			Improved the layout
 * Date: 	2013-05-06 [Cameron]
 * 			1. Show filter period when print
 * 			2. Don't allow to submit form if none is selected in option list
 * Date:	2013-05-03 [Cameron]
 * 			Add Filter by language section in front end search interface 
 * Date:	2013-04-24 [yuen]
 * 			don't select all items on Circulation Category, Book Category, Subject nor Language by default
 * 			
 * Date:	2013-04-23 [Cameron]
 * 			Add date validation function before submit criteria for report: $('#submit_result').click(function()
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
	function magicQuotes_awStripslashes(&$value, $key) {
		$value = stripslashes($value);
	}
	$gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
	array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../";
$MODULE_ROOT = "../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

require_once('ReportView.php');

global $Lang;
$linterface = new interface_html("libms.html");

class CategoryReportView extends ReportView{
	public function __construct(){

		$CurrentPageArr['LIBMS'] = 1;
		$CurrentPage = "PageReportingCategoryReport";
		parent::__construct($CurrentPage);
	}

	public function getView($viewdata = null){
		global $Lang;

		if (empty($print)){
			$x .= $this->getHeader();
			$x .= $this->getJS($viewdata);
			$x .= $this->getFrom($viewdata);
			// 			$x .= $this->getToolbar($viewdata);
			// 			$x .= $this->getResult($viewdata);
			$x .= $this->getFooter();
		}
		return $x;
	}


	public function getJS($viewdata){
		global $Lang;
		ob_start();
		?>
<link
	href="css/visualize.css" type="text/css" rel="stylesheet" />
<link
	href="css/visualize-light.css" type="text/css" rel="stylesheet" />
<link
	href="css/reports.css" type="text/css" rel="stylesheet" />
<script
	src="/templates/jquery/jquery-1.4.4.min.js"
	type="text/javascript"></script>
<script
	type="text/javascript" src="js/number_rounding.js"></script>
<script
	type="text/javascript" src="js/visualize.jQuery.js"></script>
<script type="text/javascript">

		function hideOptionLayer()
		{
			$('#formContent').attr('style', 'display: none');
			$('.spanHideOption').attr('style', 'display: none');
			$('.spanShowOption').attr('style', '');
			//document.getElementById('div_form').className = 'report_option report_hide_option';
		}
		
		function showOptionLayer()
		{
			$('#formContent').attr('style', '');
			$('.spanShowOption').attr('style', 'display: none');
			$('.spanHideOption').attr('style', '');
			//document.getElementById('div_form').className = 'report_option report_show_option';
		}
		
		function click_export()
		{
			document.form1.action = "?page=CategoryReport&action=export";
			document.form1.submit();
			document.form1.action = "?page=CategoryReport";
		}

		function click_print()
		{
			var url = "?page=CategoryReport&action=print&show=list";
			if ($('#result_stat').css('display') == 'none') {
				url = "?page=CategoryReport&action=print&show=chart";
			}
			document.form1.action=url;
			document.form1.target="_blank";
			document.form1.submit();
			
			document.form1.action="?page=CategoryReport";
			document.form1.target="_self";
		}
		
		
		function ajaxSelectbox(page, fieldName){
				$.get(page, {},
					function(insertRespond){
						if (insertRespond == ''){
//							alert('Ajax call return nothing. Please try again. If problem persist, make sure the setting is completed / contact your administrator');
							fieldName.html('');
						}else {
							fieldName.html(insertRespond);
						}
					}
				);
		}
		
		function student_list(){
			if ($('#rankTarget').val() == 'student')
				{
				page = "?page=Ajax&function=getStudentByClass&classNames="+$('#classnames :selected').text();
				ajaxSelectbox(page, $('#studentIDs'));
				$('#studentIDs option').attr('selected', true);
				}
		}
		
		
		//--------------------------------->
		$().ready( function(){
		
			$.ajaxSetup({ cache:false,
					   async:false });	   
			//---------------------------------------------------------------------------------------------------------------------- TargetUserSelection  Start
			ajaxSelectbox('?page=Ajax&function=getGroupName',$('#groupTarget'));
			ajaxSelectbox('?page=Ajax&function=getClassLevel', $('#classnames'));
			
			$('#findByGroup').change(function(){
				$('.table_findBy').toggle();
			});
			
			$('#findByStudent').change(function(){
				$('.table_findBy').toggle();
			});

			$('.findBy_option').change(function(){
//				$('.findBy_category').toggle();
				//alert($('#findByCategory:checked').val());
				if ($('#findByCategory').attr('checked'))
				{
					$('#option_categories').attr('style','display:block');
					$('#option_categories2').attr('style','display:none');
					$('#option_subject').attr('style','display:none');
					$('#option_language').attr('style','display:none');	
					
					$('#Categories2 option').attr('selected', false);
					$('#Categories option').attr('selected', true);
				}
				else if ($('#findByCategory2').attr('checked'))
				{
					$('#option_categories').attr('style','display:none');
					$('#option_categories2').attr('style','display:block');
					$('#option_subject').attr('style','display:none');
					$('#option_language').attr('style','display:none');	
					
					$('#Categories option').attr('selected', false);
					$('#Categories2 option').attr('selected', true);
				}
				else if ($('#findBySubject').attr('checked'))
				{
					$('#option_categories').attr('style','display:none');
					$('#option_categories2').attr('style','display:none');
					$('#option_subject').attr('style','display:block');
					$('#option_language').attr('style','display:none');	
				}
				else
				{
					$('#option_categories').attr('style','display:none');
					$('#option_categories2').attr('style','display:none');
					$('#option_subject').attr('style','display:none');
					$('#option_language').attr('style','display:block');	
				}

			});
					
			$('#rankTarget').change(function(){
				switch($('#rankTarget').val())
				{
				case 'form': 
					page = "?page=Ajax&function=getClassLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',true);
					$('#classnames option').attr('selected', true);
					$('#selectAllBtnClassnames').attr('style','display:inline');
					$('#Student').attr('style','display:none');	
					break;
				case 'class': 
					page= "?page=Ajax&function=getClassNameByLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',true);
					$('#classnames option').attr('selected', true);
					$('#selectAllBtnClassnames').attr('style','display:inline');
					$('#Student').attr('style','display:none');	
					break;
				case 'student': 
					page= "?page=Ajax&function=getClassNameByLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',false);
					$("#classname option:first").attr('selected','selected');
					student_list();
					$('#selectAllBtnClassnames').attr('style','display:none');
					$('#Student').attr('style','display:inline');	
					break;
				};
			});
			
			$('#classnames').change(function(){
				student_list();
			});
			//----------------------------------------------------------------------------------------------------------------------  TargetUserSelection  End 
			
			<?php if(is_null($viewdata['Result'])){ ?>
			$('#classnames option').attr('selected', true);
			$('#language option').attr('selected', true);
			$('#subject option').attr('selected', true);	
			$('#Categories option').attr('selected', true);
			$('#groupTarget option').attr('selected', true);
			<?php }?>
			
			<?php if (isset($viewdata['Result'])) {?>
				$('#formContent').attr('style','display: none');
			<? }?>
			
			//---------------------------------------------------------------------------------------------------------------------- Select All btn  Start
			$('#selectAllBtnClassnames').click(function(){ 			$('#classnames option').attr('selected', true);  		});
			$('#selectAllBtnGroups').click(function(){ 			$('#groupTarget option').attr('selected', true);  		});
			$('#selectAllBtnStudent').click(function(){ 			$('#studentIDs option').attr('selected', true);  		});
			$('#selectAllBtnCategories').click(function(){ 			$('#Categories option').attr('selected', true);			});
			$('#selectAllBtnCategories2').click(function(){ 		$('#Categories2 option').attr('selected', true);		});
			$('#selectAllBtnSubject').click(function(){ 			$('#subject option').attr('selected', true);			});
			$('#selectAllBtnLanguage').click(function(){ 			$('#language option').attr('selected', true);			});
			//---------------------------------------------------------------------------------------------------------------------- Select All btn  End
			
			
			$('#submit_result').click(function(){
				
				if (($('#rankTarget').val()=="form")&&(!$("#classnames option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_form']?>");
					return;					
				}
				if (($('#rankTarget').val()=="class")&&(!$("#classnames option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_class']?>");
					return;					
				}

				if (($('#radioPeriod_Date').attr('checked'))) {
					if (!check_date(document.getElementById("textFromDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
						return;
					if (!check_date(document.getElementById("textToDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
						return;
					if (!compare_date(document.getElementById("textFromDate"), document.getElementById("textToDate"),"<?=$Lang['General']['WrongDateLogic']?>"))
						return;	
				}
				else {
					if ( ($('#radioPeriod_Year').attr('checked')) && $("#data\\[AcademicYear\\]").val() == '' ) {					
						alert("<?=$Lang['libms']['report']['msg']['please_select_academic_year']?>");
						return;					
					}										
				}
								
				if (($('#findByCategory').attr('checked')) && (!$("#Categories option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_category']?>");
					return;					
				}
				if (($('#findByCategory2').attr('checked')) && (!$("#Categories2 option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_category']?>");
					return;					
				}
				if (($('#findBySubject').attr('checked')) && (!$("#subject option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_subject']?>");
					return;					
				}
				if (($('#findByLanguage').attr('checked')) && (!$("#language option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_language']?>");
					return;					
				}

				$.post("?page=CategoryReport&action=getResult", $("#formContent").serialize(),
					function(data) {
						$('#formContent').attr('style','display: none');
						$('#form_result').html(data);		
						$('#spanShowOption').show();
						$('#spanHideOption').hide();
						$('#report_show_option').addClass("report_option report_hide_option");
						$('.visual_chart').each(function (){
							$div = $('<div style="page-break-inside: avoid;" />')
							barchart_colors = ['#be1e2d','#666699','#92d5ea','#ee8310','#8d10ee','#5a3b16','#26a4ed','#f45a90','#e9e744','#ce1e2d','#766699','#a2d5ea','#fe8310','#9d10ee','#6a3b16','#36a4ed','#045a90','#f9e744'];
							$(this).visualize({type: 'bar', width: '660px', height : '200px', colors : barchart_colors}).appendTo($div);
							$div.appendTo($('#result_chart'));
						});
	
						$('.CategoryReportHiddenTable').hide();
						
						window.scrollTo(0, 0);
						$('.result_toggler').click(function(){
							if(this.className == 'result_toggler thumb_list_tab_on')
								return false;
							$('.result_toggler').toggleClass('thumb_list_tab_on');
							$('#result_stat').toggle();
							$('#result_chart').toggle();
							return false;
						});
						
						$('#result_chart').toggle();
						
					});
				
			});
			

		});
		</script>
<?
$x = ob_get_contents();
ob_end_clean();
return $x;
	}

	/**
	 * get Toolbar HTML code:  e.g. export print  ......
	 * @param array $viewdata
	 */
	public function getToolbar($viewdata){
		global $Lang;
		//don't show if not posted.
		if (!isset($viewdata['Result']))
			return;

		ob_start();
		?>

<div id='toolbox' class="content_top_tool">
	<div class="Conntent_tool">
		<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?>
		</a> <a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?>
		</a>
	</div>
	<br style="clear: both" />
</div>
<? 
$x = ob_get_contents();
ob_end_clean();
return $x;

	}

	public function getFrom($viewdata){
		global $Lang;


		ob_start();
		?>


<div id="report_show_option">

	<span id="spanShowOption" class="spanShowOption" style="display: none">
		<a href="javascript:showOptionLayer();"><?=$Lang["libms"]["report"]["showoption"]?>
	</a>
	</span> <span id="spanHideOption" class="spanHideOption"
		style="display: none"> <a href="javascript:hideOptionLayer();"><?=$Lang["libms"]["report"]["hideoption"]?>
	</a>
	</span>


	<form name="form1" method="post" action="?page=Lending"
		onSubmit="return checkForm();" id="formContent">

		<table class="form_table_v30">
			<? 
			echo $this->getForm_TargetUserSelection($viewdata);
			echo $this->getForm_DisplayTotal($viewdata);
			echo $this->getForm_Period($viewdata);
			echo $this->getForm_BookCategory($viewdata);			
			?>
		</table>

		<?=$this->linterface->MandatoryField();?>

		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?= $this->linterface->GET_ACTION_BTN($Lang['libms']['report']['Submit'], "button", "", "submit_result")?>
			<p class="spacer"></p>
		</div>
	</form>
</div>
<div id="form_result"></div>
<?
$x = ob_get_contents();
ob_end_clean();
return $x;
	}

	public function getForm_Period($viewdata){
		global $Lang, $junior_mck;
		$selectSchoolYearHTML =$viewdata['selectSchoolYearHTML'];
		$Period =$viewdata['Period'];
		$textFromDate = $viewdata['textFromDate'];
		$textToDate = $viewdata['textToDate'];
		if ($textFromDate=="" && $textToDate=="")
		{
			# preset date range as current school year
			$current_period= getPeriodOfAcademicYear(Get_Current_Academic_Year_ID());

			if (is_array($current_period) && sizeof($current_period)>0)
			{
				foreach($current_period as &$date){
					$date = date('Y-m-d',strtotime($date));
				}
				$textFromDate = $current_period['StartDate'];
				$textToDate = $current_period['EndDate'];
			} else
			{
				$textFromDate = (date('m')>=9) ? date('Y-09-01') : (date('Y')-1).'-09-01';
				$textToDate = date('Y-m-d');
			}
			
		}
		ob_start();
		?>
<!-- Period -->
<tr valign="top">
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['report']['time_period']?>
	</td>
	<td>
		<table class="inside_form_table" style='width: 350px;'>
			<?php if (!isset($junior_mck)) {
				?>
			<tr>
				<td colspan="6"><input name="data[Period]" type="radio"
					id="radioPeriod_Year" value="YEAR"
					<? echo ($Period!="DATE")?" checked":"" ?>> <?=$Lang['libms']['report']['SchoolYear']?>
					<?=getSelectAcademicYear("data[AcademicYear]", "");?>&nbsp; <?//=$Lang['libms']['report']['Semester']?>

				</td>
			</tr>
			<?php } ?>
			<tr>
				<td><input name="data[Period]" type="radio" id="radioPeriod_Date"
					value="DATE"
					<? echo ($Period=="DATE" || isset($junior_mck))?" checked":"" ?>> <?=$Lang['libms']['report']['period_date_from']?>
				</td>
				<td><?=$this->linterface->GET_DATE_PICKER("textFromDate",$textFromDate,"")?>
					<br /> <span id='div_DateEnd_err_msg'></span>
				</td>
				<td><?=$Lang['libms']['report']['period_date_to']?>
				</td>
				<td><?=$this->linterface->GET_DATE_PICKER("textToDate",$textToDate,"")?>
				</td>
			</tr>
		</table>
	</td>
</tr>
<?	
$x = ob_get_contents();
ob_end_clean();
return $x;
	}


	public function getForm_BookCategory($viewdata){
		global $Lang;
		global $linterface;

		$subjectList =$viewdata['subjects'];
		$Category =$viewdata['category'];
		$languageList =$viewdata['language'];
		//Henry Added [20150122]
		$Category1 =$viewdata['category1'];
		$Category2 =$viewdata['category2'];

		ob_start();
		?>
<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["statistical_category"]?>
	</td>
	<td>
		<?
		$libms = new liblms();
		$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
		foreach ($result as $row){
			$book_category_name[] = htmlspecialchars($row['value']);
		}
		?>
		<?=$this->linterface->Get_Radio_Button("findByCategory", "data[findBy_cat]", 'categories', true, "findBy_option", $book_category_name[0]?$book_category_name[0]:$Lang["libms"]["report"]["book_category"].' 1')?>
		<?=$this->linterface->Get_Radio_Button("findByCategory2", "data[findBy_cat]", 'categories2', false, "findBy_option", $book_category_name[1]?$book_category_name[1]:$Lang["libms"]["report"]["book_category"].' 2')?>
		<?=$this->linterface->Get_Radio_Button("findBySubject", "data[findBy_cat]", 'subject', false, "findBy_option", $Lang["libms"]["report"]["subject"])?>
		<?=$this->linterface->Get_Radio_Button("findByLanguage", "data[findBy_cat]", 'language', false, "findBy_option", $Lang["libms"]["report"]["language"])?>
				
		<div id="option_categories" class="findBy_category">
			<span id="Categories"> <select name="data[Categories][]"
				id="Categories" size="10" multiple><?php
				if (!empty($Category1)){
					foreach ($Category1 as $key => $item){
						if (isset($viewdata['post']['Categories']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['Categories']))
							$select=" selected ";
						else
							$select="";
						echo "<option value='$key' $select>$item</option>";
					}
				}
				?>
			</select>
			</span>
			<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnCategories');?>
			<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?>
			</span> <span id='div_Category_err_msg'></span>
		</div>
		<div id="option_categories2" class="findBy_category"  style="display:none;">
			<span id="Categories2"> <select name="data[Categories2][]"
				id="Categories2" size="10" multiple><?php
				if (!empty($Category2)){
					foreach ($Category2 as $key => $item){
						if (isset($viewdata['post']['Categories2']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['Categories']))
							$select=" selected ";
						else
							$select="";
						echo "<option value='$key' $select>$item</option>";
					}
				}
				?>
			</select>
			</span>
			<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnCategories2');?>
			<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?>
			</span> <span id='div_Category_err_msg'></span>
		</div>
		<div id="option_subject" class="findBy_category" style="display:none;">
		<span id="Subjects"> 
			<select name="data[subject][]" id="subject" size="10" multiple>
				<?php
				//if (!empty($subject))
				foreach ($subjectList as $key => $item){
					if (isset($viewdata['post']['subject']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['subject']))
						$select=" selected ";
					else{
						$select="";
						// 												tolog(htmlspecialchars_decode($item,ENT_QUOTES));
						// 												tolog($viewdata['post']['subject']);
					}
					echo "<option value='$key' $select>$item</option>";
				}
				?>
		</select>
	</span> <?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnSubject');?>
		<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?>
	</span> <span id='div_Category_err_msg'></span>
		</div>
		
		<div id="option_language" class="findBy_category" style="display:none;">
		<span id="Languages"> 
			<select name="data[language][]" id="language" size="10" multiple>
				<?php
				//if (!empty($language))
				foreach ($languageList as $key => $item){
					if (isset($viewdata['post']['language']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['language']))
						$select=" selected ";
					else{
						$select="";
					}
					echo "<option value='$key' $select>$item</option>";
				}
				?>
		</select>
			</span> <?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnLanguage');?>
				<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?>
			</span> <span id='div_Category_err_msg'></span>
		</div>
		
	</td>
</tr>
<?
$x = ob_get_contents();
ob_end_clean();
return $x;
	}


	public function getForm_TargetUserSelection($viewdata){
		global $Lang;
		//todo

		$selectSchoolYearHTML =$viewdata['selectSchoolYearHTML'];
		$Period =$viewdata['Period'];
		$textFromDate = $viewdata['textFromDate'];
		$textToDate = $viewdata['textToDate'];


		ob_start();
		?>
<tr valign="top">
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['report']['target']?>
	</td>
	<td>
		<table class="inside_form_table">
			<tr style="display: none;">
				<td valign="top">
				<?//=$this->linterface->Get_Radio_Button("findByGroup", "data[findBy]", 'Group', false , "", $Lang['libms']['report']['findByGroup'])?>
					<?=$this->linterface->Get_Radio_Button("findByStudent", "data[findBy]", 'Student', true, "", $Lang['libms']['report']['findByStudent'])?>
				</td>
			</tr>
			<tr class="table_findBy" style='display: none;'>
				<td valign="top"><select name="data[groupTarget][]" id="groupTarget"
					multiple size="7">
						<!-- Get_Select  -->
				</select> <?=$this->linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnGroups');?>
				</td>

			</tr>
			<tr class="table_findBy">
				<td valign="top"><select name="data[rankTarget]" id="rankTarget">
						<option value="form" selected>
							<?=$Lang['libms']['report']['Form']?>
						</option>
						<option value="class">
							<?=$Lang['libms']['report']['Class']?>
						</option>
						<!-- <option value="student"><?=$Lang['libms']['report']['Student']?></option> -->
				</select>
				</td>
				<td valign="top" nowrap>
					<!-- Form / Class //--> <span id='classname'> <select
						name="data[classname][]" multiple id="classnames" size="7"></select>
				</span> <?=$this->linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnClassnames');?>


					<!-- Student //--> <span id='Student' style='display: none;'> <select
						name="data[studentID][]" multiple size="7" id="studentIDs"></select>
						<?= $this->linterface->GET_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnStudent')?>
				</span>

				</td>
			</tr>
			<tr>
				<td colspan="3" class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?>
				</td>
			</tr>
		</table> <span id='div_Target_err_msg'></span>
	</td>
</tr>


<?
$x = ob_get_contents();
ob_end_clean();
return $x;
	}

	public function getForm_DisplayTotal($viewdata){
		global $Lang;
		ob_start();
		?>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["listby"]?></td>
			<td>
				<?=$this->linterface->Get_Radio_Button("displayTotalAll", "data[displayTotal]", 'All', true , "", $Lang["libms"]["report"]["fine_status_all"])?>
				<?=$this->linterface->Get_Radio_Button("displayTotalGender", "data[displayTotal]", 'Gender', false , "", $Lang["libms"]["report"]["Gender"]." (".$Lang["libms"]["report"]["Male"]." / ".$Lang["libms"]["report"]["Female"].")")?>
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getResult($viewdata){
		global $Lang, $junior_mck;
		ob_start();
		echo '<div id="result_body_removed"  >';
		
		
		if ( $_REQUEST['action'] != 'print')
		{
		?>
			<div class="thumb_list_tab print_hide">
				<a class='result_toggler' href="#"> <span><?=$Lang["libms"]["reporting"]["span"]['graph']?></span>
				</a> <em>|</em> <a href="#" class="result_toggler thumb_list_tab_on"> <span><?=$Lang["libms"]["reporting"]["span"]['table']?></span>
				</a>
			</div>
		<?	
		}
		
		if ( $_REQUEST['action'] == 'print')
		{
			if ($viewdata["post"]["findBy_cat"]=="language")
			{
				$report_title_used = $Lang['libms']['reporting']['PageReportingCategoryReport3'];
			} elseif ($viewdata["post"]["findBy_cat"]=="subject")
			{
				$report_title_used = $Lang['libms']['reporting']['PageReportingCategoryReport2'];
			} else
			{
				$report_title_used = $Lang['libms']['reporting']['PageReportingCategoryReport1'];
			}
			
			//Added school name at top of the page
			$schoolName =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
			echo '<p>'.$schoolName.'</p><center><h1>'.$report_title_used.'</h1></center>';
		
			// Print filter period info
			echo '<table border="0" width="100%"><tr><td>';
			echo  $Lang['libms']['report']['period_date_from'] . " " .
				$viewdata['post']['textFromDate'] . $Lang['libms']['report']['period_date_to'] . " " . $viewdata['post']['textToDate'];
			echo '</td><td align="right">';
			echo $Lang["libms"]["report"]["printdate"] . " ".date("Y-m-d").'</td></tr></table>';
		}
		
echo '<div id="result_stat"  >';
if (!(empty($viewdata['post'])) && (empty($viewdata['Result']))){
	echo $Lang["libms"]["report"]["norecord"];
}elseif (!(empty($viewdata['post'])) && !(empty($viewdata['Result']))){
	echo $this->getRecord_RecordList($viewdata);
}
echo '</div>';
echo '<div id="result_chart" style="text-align:left" >';
echo '</div>';
echo '</div>';
$x = ob_get_contents();
ob_end_clean();
return $x;
	}

	public function gen_categoryReportArray($viewdata){
		
		global $Lang;
		
		if ($viewdata['filterArray']['findBy_cat'] == 'categories' || $viewdata['filterArray']['findBy_cat'] == 'categories2'){
			//for categories only ....//Set all 0
			foreach ($viewdata['filterArray'][$viewdata['filterArray']['findBy_cat']] as $cat){
				if ($cat == "")
				{
					$cat = $Lang["libms"]["report"]["cannot_define"];
				}
				
				foreach ($viewdata['filterArray']['classname'] as $class){
//					$cat_key = strtoupper($cat);
//					$array_list[$cat_key]['name'] = strtoupper($viewdata['category'][$cat]);
					$cat_key = $cat;
					$array_list[$cat_key]['name'] = ($viewdata['filterArray']['findBy_cat'] == 'categories'?$viewdata['category1'][$cat]:$viewdata['category2'][$cat]);
					$array_list[$cat_key]['class'][$class] = 0;
					$array_list[$cat_key]['classM'][$class] = 0;
					$array_list[$cat_key]['classF'][$class] = 0;
				}
			}

		}
//debug_r($viewdata);		
//debug_r($viewdata['filterArray']['findBy_cat']);		
//debug_r($viewdata['filterArray'][$viewdata['filterArray']['findBy_cat']]);		
		//for subject only .....//Set all 0
		if($viewdata['filterArray']['findBy_cat'] == 'subject'){
			foreach ($viewdata['filterArray'][$viewdata['filterArray']['findBy_cat']] as $cat){
				if ($cat == "")
				{
					$cat = $Lang["libms"]["report"]["cannot_define"];
				}
				
				foreach ($viewdata['filterArray']['classname'] as $class){
//					$cat_key = strtoupper($cat);
//					$array_list[$cat_key]['name'] = strtoupper($cat);
					$cat_key = $cat;
					$array_list[$cat_key]['name'] = $cat;					
					$array_list[$cat_key]['class'][$class] = 0;
					$array_list[$cat_key]['classM'][$class] = 0;
					$array_list[$cat_key]['classF'][$class] = 0;
				}
			}
		}

		//for language only .....//Set all 0
		if($viewdata['filterArray']['findBy_cat'] == 'language'){
			foreach ($viewdata['filterArray'][$viewdata['filterArray']['findBy_cat']] as $cat){
				if ($cat == "")
				{
					$cat = $Lang["libms"]["report"]["cannot_define"];
				}
				
				foreach ($viewdata['filterArray']['classname'] as $class){
//					$cat_key = strtoupper($cat);
//					$array_list[$cat_key]['name'] = strtoupper($cat);
// Do not convert to upper case
					$cat_key = $cat;					
					$array_list[$cat_key]['name'] = $cat;
					$array_list[$cat_key]['class'][$class] = 0;
					$array_list[$cat_key]['classM'][$class] = 0;
					$array_list[$cat_key]['classF'][$class] = 0;
				}
			}
		}
//debug_r($array_list);

		// Create new array ...
		foreach ($viewdata['Result'] as $Groups){
			foreach ($Groups as  $items){
				$cat = $items['GroupBy'];
				if ($cat == "^=Unde=^" ){	// Undefined
//					$cat_key = strtoupper($Lang["libms"]["report"]["cannot_define"]);
					$cat_key = $Lang["libms"]["report"]["cannot_define"];
					$array_list[$cat_key]['name'] = $Lang["libms"]["report"]["cannot_define"];
				}else{
					if ($viewdata['filterArray']['findBy_cat'] == 'subject')
					{
						$grp = 'subjects';
					}
					else if ($viewdata['filterArray']['findBy_cat'] == 'language')
					{
						$grp = 'language';
					}
					else if ($viewdata['filterArray']['findBy_cat'] == 'categories2')
					{
						$grp = 'category2';
					}
					else
					{
						$grp = 'category1';	
					}
//					$cat_key = strtoupper($cat);
//					$array_list[$cat_key]['name'] = strtoupper($viewdata[$grp][$cat]);
					$cat_key = $cat;
					$array_list[$cat_key]['name'] = $viewdata[$grp][$cat];
				}
		
		
				$class = &$array_list[$cat_key]['class'][$items['Class']];
				//Total by gender
				$classM = &$array_list[$cat_key]['classM'][$items['Class']];
				$classF = &$array_list[$cat_key]['classF'][$items['Class']];
				if (empty($class)){
					$class  = $items['Total'];
					$classM  = $items['GenderMTotal'];
					$classF  = $items['GenderFTotal'];
				}else{
					$class  += $items['Total'];
					$classM += $items['GenderMTotal'];
					$classF += $items['GenderFTotal'];
				}
				unset($class);
				unset($classM);
				unset($classF);
			}
		}
		
		unset($array_list['']);
		
		//dump($viewdata);
		//dump($array_list);
		return $array_list;
	}
	
	public function getRecord_RecordList($viewdata){
		global $Lang;
		$x = '';
		
		$array_list = $this->gen_categoryReportArray($viewdata);
//debug_r($array_list);			
//debug_r($viewdata);

		### build hidden table
		if($viewdata['filterArray']['displayTotal'] == 'Gender'){
			foreach ($viewdata['post']['classname'] as $Groups)
			{
			$x .= "<div class='CategoryReportHiddenTable'style='page-break-inside: avoid;'>";
			$x .= "<table class='half_half_table common_table_list_v30 view_table_list_v30 visual_chart' >";
			$x .= "<caption style='font-size: 15pt;'>{$Lang["libms"]["reporting"]["PageReportingCategoryReport"]} - {$Groups} ({$Lang["libms"]["report"]["Male"]})</caption>";
			$x .= "<thead><tr>";
			$x .="<td class='cell_bg_blue''>" . $Lang["libms"]["report"]["statistical_category"] ."</td>";
			$x .= "<th class='cell_bg_blue'>" . $Lang["libms"]["report"]["total"] ."</th>";
			$x .= "</tr></thead>";
			foreach($array_list as $key => $items)
			{
				$x .= "<tr>";
				$x .= "<th >".$items['name']."</th>";
				$x .= "<td>".$items['classM'][$Groups]."</td>";
				$x .= "</tr>";
			}
		
			$x .= "</table><br/></div>";
			
			$x .= "<div class='CategoryReportHiddenTable'style='page-break-inside: avoid;'>";
			$x .= "<table class='half_half_table common_table_list_v30 view_table_list_v30 visual_chart' >";
			$x .= "<caption style='font-size: 15pt;'>{$Lang["libms"]["reporting"]["PageReportingCategoryReport"]} - {$Groups} ({$Lang["libms"]["report"]["Female"]})</caption>";
			$x .= "<thead><tr>";
			$x .="<td class='cell_bg_blue''>" . $Lang["libms"]["report"]["statistical_category"] ."</td>";
			$x .= "<th class='cell_bg_blue'>" . $Lang["libms"]["report"]["total"] ."</th>";
			$x .= "</tr></thead>";
			foreach($array_list as $key => $items)
			{
				$x .= "<tr>";
				$x .= "<th >".$items['name']."</th>";
				$x .= "<td>".$items['classF'][$Groups]."</td>";
				$x .= "</tr>";
			}
		
			$x .= "</table><br/></div>";
			
			}	
		}
		else{
			foreach ($viewdata['post']['classname'] as $Groups)
			{
			$x .= "<div class='CategoryReportHiddenTable'style='page-break-inside: avoid;'>";
			$x .= "<table class='half_half_table common_table_list_v30 view_table_list_v30 visual_chart' >";
			$x .= "<caption style='font-size: 15pt;'>{$Lang["libms"]["reporting"]["PageReportingCategoryReport"]} - {$Groups}</caption>";
			$x .= "<thead><tr>";
			$x .="<td class='cell_bg_blue''>" . $Lang["libms"]["report"]["statistical_category"] ."</td>";
			$x .= "<th class='cell_bg_blue'>" . $Lang["libms"]["report"]["total"] ."</th>";
			$x .= "</tr></thead>";
			foreach($array_list as $key => $items)
			{
				$x .= "<tr>";
				$x .= "<th >".$items['name']."</th>";
				$x .= "<td>".$items['class'][$Groups]."</td>";
				$x .= "</tr>";
			}
		
			$x .= "</table><br/></div>";
			}
		}
		### build hidden table
		
		
		### build display table
		$x .= "<table class='common_table_list view_table_list' width='100%'>";
		$x .= "<tbody><tr >";
		$x .= "<td></td>";
		foreach ($viewdata['post']['classname'] as $Groups){
			$x .= "<th class='sub_row_top'>{$Groups}</th>";
		}
       	$x .= "</tr>";
       	foreach ($array_list as $key => $groups){
	       	$x .= "<tr>";
	       	$x .= "<td>{$groups['name']}</td>";
	       	if($viewdata['filterArray']['displayTotal'] == 'Gender'){
	       		foreach ($groups['class'] as $key => $values){
	       			$x .= "<td>".$groups['classM'][$key]."/".$groups['classF'][$key]."</td>";
	       		}
			}
			else{
				foreach ($groups['class'] as $key => $values){
					$x .= "<td>{$values}</td>";
				}
			}
	       	
			$x .= "</tr>";
       	}
       	$x .= "</table>";

	 return $x;
	}
	/**
	 * get Print view
	 * @param array $viewdata
	 * @return string
	 */
	public function getPrint($viewdata){
		global $Lang,$LAYOUT_SKIN;
		ob_start();
		include_once("{$_SERVER['DOCUMENT_ROOT']}/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
		?>
<link
	href="css/visualize.css" type="text/css" rel="stylesheet" />
<link
	href="css/visualize-light.css" type="text/css" rel="stylesheet" />
<link
	href="css/reports.css" type="text/css" rel="stylesheet" />
<script
	src="/templates/jquery/jquery-1.4.4.min.js"
	type="text/javascript"></script>
<script
	type="text/javascript" src="js/number_rounding.js"></script>
<script
	type="text/javascript" src="js/visualize.jQuery.js"></script>
<script type="text/javascript">
			$(function(){
				$('.visual_chart').each(function (){
					$div = $('<div style="page-break-inside: avoid;" />')
					barchart_colors = ['#be1e2d','#666699','#92d5ea','#ee8310','#8d10ee','#5a3b16','#26a4ed','#f45a90','#e9e744','#ce1e2d','#766699','#a2d5ea','#fe8310','#9d10ee','#6a3b16','#36a4ed','#045a90','#f9e744'];
					$(this).visualize({type: 'bar', width: '660px', height : '200px', colors : barchart_colors}).appendTo($div);
					$div.appendTo($('#result_chart'));
				});

				$('.CategoryReportHiddenTable').hide();
				
				window.scrollTo(0, 0);
				$('.result_toggler').click(function(){
					if(this.className == 'result_toggler thumb_list_tab_on')
   						return false;
					$('.result_toggler').toggleClass('thumb_list_tab_on');
					$('#result_stat').toggle();
					$('#result_chart').toggle();
					return false;
				});

				$('#result_chart').toggle();

				if ('<?=$_GET['show']?>' == 'chart'){
					//$('.result_toggler:first').click();
					$('.result_toggler').toggleClass('thumb_list_tab_on');
					$('#result_stat').toggle();
					$('#result_chart').toggle();
				}
					
			});
		</script>
<div id='toolbar' class='print_hide'
	style='text-align: right; width: 100%'>
	<? 
	$linterface = new interface_html();
	echo $linterface->GET_SMALL_BTN($Lang["libms"]["report"]["toolbar"]['Btn']['Print'], "button", "javascript:window.print();","print");

	?>
</div>

<?=$this->getResult($viewdata);?>
</body>
</html>
<?
		
$x = ob_get_contents();
ob_end_clean();
return $x;
	}

	/**
	 * get CSV string
	 * @param array $viewdata
	 * @return string
	 */
	public function getCSV($viewdata){
		global $Lang;
		
		$array_list = $this->gen_categoryReportArray($viewdata);
		$headers[] = '';
		
		foreach ($viewdata['filterArray']['classname'] as $class){		
			$headers[] = $class;
		}	
		$formatted_ary[] = $headers;
		
		foreach($array_list as $key=>$row){
			$row_value[] = $row['name'];
			foreach ($viewdata['filterArray']['classname'] as $class){
				//$row_value[] = $row['class'][$class];
				if($viewdata['filterArray']['displayTotal'] == 'Gender'){
					$row_value[] = $row['classM'][$class]."/".$row['classF'][$class];
				}
				else{
					$row_value[] = $row['class'][$class];
				}
			} 

			$formatted_ary[] = $row_value;
			unset($row_value);
		}
			
		//array_unshift($formatted_ary,$headers);
		$csv = Exporter::array_to_CSV($formatted_ary);
		//dump($viewdata['Result']);
		return $csv;
			
	}
		
}
