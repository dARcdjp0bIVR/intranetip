<?php
#using:

######################################################################################
#									Change Log 
######################################################################################
#
#   20190617 Cameron: add number of read(hit) by percentage filter
#   20190327 Cameron: add checking for $showPercentageDownCol
#   20190306 Cameron: also print number of total books for percentage column [case #W155835]
#	20160504 Cameron: print grand total row [Case #F95387]
#	20150209 Ryan : Created this file  
######################################################################################

intranet_auth();
intranet_opendb();

$Lang['General']['EmptySymbol'] = "&nbsp;";


include_once($PATH_WRT_ROOT."includes/libaccessright.php");
$laccessright = new libaccessright();

if(!$_SESSION['LIBMS']['admin']['current_right']['statistics']['ebook statistics'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$linterface = new interface_html();

$thisStockTakeSchemeDisplay = $StartDate  . ' ' . $Lang['General']['To']  . ' ' .  $EndDate;

$schoolName =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];

$header = '';
$header .= '<table width="98%" border="0" cellspacing="0" cellpadding="5" valign="top">';
$header .= '<tr>';
$header .= '<td align="right" class="print_hide">';
$header .= $linterface->GET_BTN($button_print, "button","javascript:window.print()");
$header .= '</td>'; 
$header .= '</tr>';
$header .= '<tr><td>'.$schoolName.'</td></tr>';  
$header .= '<tr>';
$header .= '<td>';
$header .= '<center><h2>' . $Lang["libms"]["eBookStat"]["ModuleTitle"] . '</h2></center>';
$header .= '</td>';
$header .= '</tr>';
$header .= '<tr>';
$header .= '<td align="right">';
$header .= $Lang['General']['Date'] . ': ' .$thisStockTakeSchemeDisplay;
$header .= '</td>'; 
$header .= '</tr>';
$header .= '<tr>';
$header .= '<td>';

########################## DB Table Start ###############################

$displayTable = '<table width="98%" class="common_table_list_v30 view_table_list_v30">';

$displayTable .= '<tr>';
foreach($col as $key){
	$displayTable .= '<th>'.$key.'</th>';
}
$displayTable .= '</tr>';
$noRow = count($exportData);
$i = 0;
foreach($exportData as $row){
	$displayTable .= '<tr>';
	if ($i == $noRow - 1) {		// last row
		$displayTable .= '<td colspan="3" align=right>'.$row[2].'</td>';
		$displayTable .= '<td>'.$row[3].'</td>';
		$displayTable .= '<td>'.$row[4].'</td>';
		$displayTable .= '<td>'.$row[5].'</td>';
		if ($showPercentageCol || $showPercentageDownCol) {
		    $displayTable .= '<td>'.$row[6].'</td>';
		    $displayTable .= '<td>'.$row[7].'</td>';
		}
	}
	else {
		foreach($row as $colVal){
			$displayTable .= '<td>';
			$displayTable .= $colVal;
			$displayTable .= '</td>';
		}
	}
	$displayTable .= '</tr>';
	$i++;
}
$displayTable .= '</table>';
$displayTable='<div style="display:inline;float:center;">'.$displayTable.'</div><div id="pieLegend" style="margin-top:0px; margin-left:0px; width:150px; height:220px;float:left"></div></div>';
########################## DB Table End ###############################
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  /> 
<META Http-Equiv="Cache-Control" Content="no-cache">
<style type='text/css'>

html, body { margin: 0px; padding: 0px; } 


@media print and (width: 21cm) and (height: 29.7cm) {
    @page {
       margin: 3cm;
    }
 }
 
@media print
{
	
  /*table { page-break-after:auto }
  tr    { page-break-inside:avoid; page-break-after:auto }
  td    { page-break-inside:avoid; page-break-after:auto }
  thead { display:table-header-group }
  tfoot { display:table-footer-group }
  
  #footer {
         display:block;
		   position:fixed;
		   bottom:0px;
		   width:100%;
  } */
    
    
}

</style>
</head>
<body>
<?php echo $header;?>
<?php echo $displayTable;?>
</body>
</html>

<?php
intranet_closedb();
?>