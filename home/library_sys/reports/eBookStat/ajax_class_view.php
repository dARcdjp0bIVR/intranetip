<?php

// Using:    
######################################################################################
#									Change Log 
######################################################################################
#
#   20190617 Cameron
#       - change user reading option from checkbox to radio
#       - add column ReadCount (hits) by specific percentage filter
#
#   20190327 Cameron
#       - handle less than percentage read filter
#
#   20190319 Cameron
#       - show rows that reach the percentage criteria only when showPercentageCol is ticked [ej.5.0.9.3.1]
#
#   20190306 Cameron
#       - show column for number of books read by centain percentage [case #W155835]
#
#	20180122 Cameron
#		- restrict to eBook only (case #W133701)
#
#	20160503 Cameron: revise sql to support selection of multiple Group / Student in Target [Case #F95387]
#	20150209 Ryan : Swap Total hits and total books column,
#					Fixed result of ReadCount and Book Count not correctly,   
#	20150204 Ryan : Training Feedback  - Add Class View Export
#										 [Case Number : 2015-0203-1543-49054]
#	20141231 Ryan : Fixed ej encoding issue by using $intranet_db 
#	20141210 Ryan : Added filter option to show or hide non-recorded result 
#	20141201 Ryan : eLibrary plus pending developments Item 203
#
######################################################################################
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
//include_once($PATH_WRT_ROOT."includes/libelibrary.php");
//include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();


if(!$_SESSION['LIBMS']['admin']['current_right']['statistics']['ebook statistics'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}


$linterface = new interface_html();

if(!empty($_POST)){
	$data = $_POST['data'];
	$findBy = $data['findBy'];
	$StartDate = $_POST['StartDate'];
	$EndDate = $_POST['EndDate'];
	$showPercentageCol = $_POST['readingFilter'] == 'readup' ? true : false;
	$percentage = $_POST['percentage'];
	$showPercentageDownCol = $_POST['readingFilter'] == 'readdown' ? true : false;
	$percentageDown = $_POST['percentageDown'];
	
	if( $findBy == 'Group'){
		$groupTarget 		= $data['groupTarget'];
		$groupTargetType 	= $data['groupTargetType'];
		$memberID 			= $data['memberID'];
	}else{
		$rankTarget = $data['rankTarget'];
		$classname 	= $data['classname'];
		$studentID 	= $data['studentID'];
	}
}

# Time Condition
$TimeCond = "And a.DateModified BETWEEN '$StartDate 00:00:00' AND '$EndDate 23:59:59' ";
$condition_clause = "";

# user filter
if (($findBy == 'Group') && ($groupTargetType == 'group') && (!empty($groupTarget))){
	foreach ((array)$groupTarget as $k=>$v){
		$groupTarget[$k] = mysql_real_escape_string($v);
	}
	$impoded_groupTarget = implode("','", $groupTarget);
	$condition_clause .= " AND u.`UserID` IN (SELECT UserID FROM `LIBMS_GROUP_USER` gu WHERE gu.`UserID` = u.`UserID` AND gu.`GroupID` IN ('{$impoded_groupTarget}'))";
}

if (($findBy == 'Group') && ($groupTargetType == 'member') && (!empty($memberID))){
	foreach ((array)$memberID as $k=>$v){
		$memberID[$k] = mysql_real_escape_string($v);
	}
	$impoded_memberID = implode("','", $memberID);
	$condition_clause .= " AND u.`UserID` IN ('{$impoded_memberID}')";
}

if (($findBy == 'Student')&&($rankTarget == 'form')&&(!empty($classname))){
	foreach ((array)$classname as $k=>$v){
		$classname[$k] = mysql_real_escape_string($v);
	}
	$impoded_classname = implode("','", $classname);
	$condition_clause .= " AND u.`ClassLevel` IN ('{$impoded_classname}')";
}
	
if (($findBy == 'Student')&&($rankTarget == 'class')&&(!empty($classname))){
	foreach ((array)$classname as $k=>$v){
		$classname[$k] = mysql_real_escape_string($v);
	}
	$impoded_classname = implode("','", $classname);
	$condition_clause .= " AND u.`ClassName` IN ('{$impoded_classname}')";
}
	
if (($findBy == 'Student')&&($rankTarget == 'student')&&(!empty($studentID))){
	foreach ((array)$studentID as $k=>$v){
		$studentID[$k] = mysql_real_escape_string($v);
	}
	$impoded_studentID = implode("','", $studentID);
	$condition_clause .= " AND u.`UserID` IN ('{$impoded_studentID}')";
}
$condition_clause .= " AND u.`RecordStatus` = '1'";

# get selected student list info
$sql = "SELECT u.* FROM LIBMS_USER u WHERE 1 {$condition_clause} ORDER BY ".$Lang['libms']['SQL']['UserNameFeild'];
$StudentList = $libms->returnResultSet($sql);

# User Condition  
$UsersIn = ''; 
foreach($StudentList as $Student){
	$UsersIn .= "'{$Student['UserID']}',";
}
$UsersIn = rtrim($UsersIn,',');
$physical_book_init_value = 10000000;

$sql = "select count(a.BooKID) as ReadCount, a.*
		From {$intranet_db}.INTRANET_ELIB_BOOK_HISTORY a
		inner join {$intranet_db}.INTRANET_ELIB_BOOK b on b.BookID = a.BookID
		Where b.publish=1 and b.BookID<={$physical_book_init_value} and 
		a.UserID in ({$UsersIn}) {$TimeCond} 
		Group By a.UserID, a.BookID";
		$ReadRecords = $libms->returnResultSet($sql);
		
$sql = "select count(*) as ReviewCount, a.UserID
		From {$intranet_db}.INTRANET_ELIB_BOOK_REVIEW a
		inner join {$intranet_db}.INTRANET_ELIB_BOOK b on b.BookID = a.BookID
		Where b.publish=1 and b.BookID<={$physical_book_init_value} and a.UserID in ({$UsersIn}) {$TimeCond} 
		Group By a.UserID";
		$ReviewRecords = $libms->returnResultSet($sql);

if ($showPercentageCol) {
    $sql = "SELECT 
                    COUNT(DISTINCT a.BooKID) AS BookCount,
                    COUNT(*) AS ReadCount, 
                    a.UserID 
		    FROM 
		            {$intranet_db}.INTRANET_ELIB_BOOK_HISTORY a
		    INNER JOIN
		            {$intranet_db}.INTRANET_ELIB_BOOK b ON b.BookID = a.BookID
            INNER JOIN (
                    SELECT 
                            MAX(IFNULL(Percentage,0)) AS Percentage, 
                            UserID, 
                            BookID 
                    FROM
                            {$intranet_db}.INTRANET_ELIB_USER_PROGRESS
            		GROUP BY 
                            BookID, UserID
                    ) AS p ON p.UserID=a.UserID AND p.BookID=a.BookID
            WHERE
		            b.Publish=1 
                    AND b.BookID<={$physical_book_init_value}
                    AND p.Percentage>='".$percentage."'
            		AND a.UserID in ({$UsersIn}) {$TimeCond}
            GROUP BY
                    a.UserID";
    $percentageAry = $libms->returnResultSet($sql);
// debug_pr($sql);
// debug_pr($percentageAry);
    $percentageAry = BuildMultiKeyAssoc($percentageAry, array('UserID'), array('BookCount', 'ReadCount'));
}

if ($showPercentageDownCol) {
    $sql = "SELECT
                    COUNT(DISTINCT a.BooKID) AS BookCount,
                    COUNT(*) AS ReadCount,
                    a.UserID
            FROM
                    {$intranet_db}.INTRANET_ELIB_BOOK_HISTORY a
            INNER JOIN
                    {$intranet_db}.INTRANET_ELIB_BOOK b ON b.BookID = a.BookID
            INNER JOIN (
                    SELECT
                            MAX(IFNULL(Percentage,0)) AS Percentage,
                            UserID,
                            BookID
                    FROM
                            {$intranet_db}.INTRANET_ELIB_USER_PROGRESS
                    GROUP BY
                            BookID, UserID
                    ) AS p ON p.UserID=a.UserID AND p.BookID=a.BookID
            WHERE
                    b.Publish=1
                    AND b.BookID<={$physical_book_init_value}
                    AND p.Percentage<'".$percentageDown."'
                    AND a.UserID in ({$UsersIn}) {$TimeCond}
            GROUP BY
                    a.UserID";
    $percentageDownAry = $libms->returnResultSet($sql);
//debug_pr($sql);
    $percentageDownAry = BuildMultiKeyAssoc($percentageDownAry, array('UserID'), array('BookCount', 'ReadCount'));
}

# Init
$NameField = $Lang['libms']['SQL']['UserNameFeild'];
$UsersTotalReadTimes = array();

foreach((array)$StudentList as $Student){
    $_studentID = $Student['UserID'];
	$UsersTotalReadTimes[$_studentID]['Name'] = $Student[$NameField]; 
	$UsersTotalReadTimes[$_studentID]['ClassName'] = $Student['ClassName']; 
	$UsersTotalReadTimes[$_studentID]['ClassNumber'] = $Student['ClassNumber'];
	
	if ($showPercentageCol) {
	    $UsersTotalReadTimes[$_studentID]['PercentageCount'] = $percentageAry[$_studentID]['BookCount'] ? $percentageAry[$_studentID]['BookCount']: 0;
	    $UsersTotalReadTimes[$_studentID]['ReadCount'] = $percentageAry[$_studentID]['ReadCount'] ? $percentageAry[$_studentID]['ReadCount']: 0;
	}
	
	if ($showPercentageDownCol) {  // mutual exclusive with $showPercentageCol
	    $UsersTotalReadTimes[$_studentID]['PercentageCount'] = $percentageDownAry[$_studentID]['BookCount']? $percentageDownAry[$_studentID]['BookCount']: 0;
	    $UsersTotalReadTimes[$_studentID]['ReadCount'] = $percentageDownAry[$_studentID]['ReadCount'] ? $percentageDownAry[$_studentID]['ReadCount']: 0;
	}
}

foreach((array)$ReadRecords as $BooksReadTimesRecord){
	$UsersTotalReadTimes[$BooksReadTimesRecord['UserID']]['TotalReads'] += $BooksReadTimesRecord['ReadCount'];
	$UsersTotalReadTimes[$BooksReadTimesRecord['UserID']]['TotalBooks'] ++; 
}
foreach((array)$ReviewRecords as $UserReviewCount){
	$UsersTotalReadTimes[$UserReviewCount['UserID']]['TotalReviews'] = $UserReviewCount['ReviewCount']; 
}
if($_POST['readingFilter'] != 'all'){
	$tmp = array();
	foreach((array)$UsersTotalReadTimes as $recordUID=>$row){
		if($row['TotalReviews'] == 0 && $row['TotalBooks'] == 0){
			//kick out 
		}else{
			$tmp[$recordUID] = $row;
		}
	}
	$UsersTotalReadTimes = $tmp;
}

if ($showPercentageCol) {
    $tmp = array();
    foreach($UsersTotalReadTimes as $recordUID=>$row){
        if($row['PercentageCount'] == 0){
            //kick out
        }else{
            $tmp[$recordUID] = $row;
        }
    }
    $UsersTotalReadTimes = $tmp;
}

if ($showPercentageDownCol) {
    $tmp = array();
    foreach($UsersTotalReadTimes as $recordUID=>$row){
        if($row['PercentageCount'] == 0){
            //kick out
        }else{
            $tmp[$recordUID] = $row;
        }
    }
    $UsersTotalReadTimes = $tmp;
}

if($_REQUEST['action'] == 'export' || $_REQUEST['action'] == 'print'){
    $col = array();
	$col[] = $Lang["libms"]["report"]["username"];
	$col[] = $Lang['libms']['bookmanagement']['ClassName'];
	$col[] = $Lang['libms']['bookmanagement']['ClassNumber'];
	$col[] = $Lang["libms"]["eBookStat"]["TotalBooks"];
	if ($showPercentageCol) {
	    $col[] = sprintf($Lang["libms"]["eBookStat"]["totalBooksByPercentage"],$percentage);
	    $col[] = sprintf($Lang["libms"]["eBookStat"]["totalReadByPercentage"],$percentage);
	}
	if ($showPercentageDownCol) {
	    $col[] = sprintf($Lang["libms"]["eBookStat"]["totalBooksByPercentageDown"],$percentageDown);
	    $col[] = sprintf($Lang["libms"]["eBookStat"]["totalReadByPercentageDown"],$percentageDown);
	}
	
	$col[] = $Lang["libms"]["eBookStat"]["TotalReads"];
	$col[] = $Lang["libms"]["eBookStat"]["TotalReviews"];
	
	$exportData =array(); 
	$i = 0;
	$total_Books = 0;
	$total_Reads = 0;
	$total_Reviews = 0;
	$total_PercentageCount = 0;
	$total_ReadCount = 0;
	
	foreach($UsersTotalReadTimes as $row){
		$exportData[$i][] = $row['Name'] ? $row['Name'] : '--';
		$exportData[$i][] = $row['ClassName']? $row['ClassName'] : '--';
		$exportData[$i][] = $row['ClassNumber']? $row['ClassNumber'] : '--';
		$exportData[$i][] = $row['TotalBooks']? $row['TotalBooks'] : '0';
		if ($showPercentageCol) {
		    $exportData[$i][] = $row['PercentageCount']? $row['PercentageCount'] : '0';
		    $total_PercentageCount += $row['PercentageCount'];
		    $exportData[$i][] = $row['ReadCount']? $row['ReadCount'] : '0';
		    $total_ReadCount += $row['ReadCount'];
		    
		}
		if ($showPercentageDownCol) {
		    $exportData[$i][] = $row['PercentageCount']? $row['PercentageCount'] : '0';
		    $total_PercentageCount += $row['PercentageCount'];
		    $exportData[$i][] = $row['ReadCount']? $row['ReadCount'] : '0';
		    $total_ReadCount += $row['ReadCount'];
		}
		
		$exportData[$i][] = $row['TotalReads']? $row['TotalReads'] : '0';
		$exportData[$i][] = $row['TotalReviews']? $row['TotalReviews'] : '0';
		$total_Books += $row['TotalBooks']? $row['TotalBooks'] : 0;
		$total_Reads += $row['TotalReads']? $row['TotalReads'] : 0;
		$total_Reviews += $row['TotalReviews']? $row['TotalReviews'] : 0;
		$i++;
	}
	
	## grand total line
	$exportData[$i][] = '';
	$exportData[$i][] = '';
	$exportData[$i][] = $Lang["libms"]["report"]["total"];	
	$exportData[$i][] = $total_Books;
	if ($showPercentageCol || $showPercentageDownCol) {
	    $exportData[$i][] = $total_PercentageCount;
	    $exportData[$i][] = $total_ReadCount;
	}
	$exportData[$i][] = $total_Reads;
	$exportData[$i][] = $total_Reviews;
	
	if($_REQUEST['action'] == 'print'){
		ob_start();  // start buffer
		include($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
		include('print.php');  // read in buffer
		include($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
		$var=ob_get_contents();  // get buffer content
		ob_end_clean();  // delete buffer content
		echo $var;
	}else{
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$lexport = new libexporttext();
		$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($exportData, $col, "\t", "\r\n", "\t", 0, "9");
		# Output The File To User Browser
		$filename = 'eBookStatsByStudent.csv';
		$lexport->EXPORT_FILE($filename, $export_content, $isXLS = false, $ignoreLength = false, $BaseCode="UTF-8");
	}
}else{
    $isShowPercentageCol = false;
    $type = '';
    if ($showPercentageCol) {
        $isShowPercentageCol = true;
        $type = 'up';
        $percentagePoint = $percentage;
    }
    else if ($showPercentageDownCol) {
        $isShowPercentageCol = true;
        $type = 'down';
        $percentagePoint = $percentageDown;
    }    
    echo $libms->Get_Class_View($UsersTotalReadTimes,$StartDate,$EndDate,$isShowPercentageCol,$percentagePoint,$type);
}
intranet_closedb();
?>