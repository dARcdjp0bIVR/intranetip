<?php

// Using:  
######################################################################################
#									Change Log 
######################################################################################
#
#   20190617 Cameron: change user reading/progress filter from checkbox to radio
#   20190327 Cameron: add less than percentage read filter option
#   20190320 Cameron: hide showAll when select show progress percentage
#   20190318 Cameron: revise percentage step 
#   20190306 Cameron: add showPercentage column option [case #W155835]
#	20160715 Cameron: hide the warning and empty option list if it returns nothing in ajaxSelectbox (IP2.5.7.7.1)
#	20160503 Cameron: let user be able to select multiple Group / Student in Target [Case #F95387]
#	20150209 Ryan : Add print and export    
#	20141210 Ryan : Added filter option to show or hide non-recorded result   
#	20141201 Ryan : eLibrary plus pending developments Item 203
#
######################################################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");



intranet_auth();
intranet_opendb();

$libms = new liblms();

if(!$_SESSION['LIBMS']['admin']['current_right']['statistics']['ebook statistics'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}
$home_header_no_EmulateIE7 = true;
$libms = new liblms();
$linterface = new interface_html();

# Current Page Info
$CurrentPageArr['LIBMS'] = 1;
$TAGS_OBJ[] = array($Lang["libms"]["eBookStat"]["ModuleTitle"]);
$CurrentPage = "PageeBookStatisticsReport";

# Top menu highlight setting
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();


function getForm_TargetUserSelection(){
	global $Lang, $linterface;
	
	$percentageOptionList = "";
	for ($i=5; $i<=100; $i+=5) {
	    $percentageOptionList .= '<option value="'.$i.'">'.$i.'%</option>'."\n";
	}
	
	ob_start();
	?>
	<tr valign="top">
		<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['target']?></td>
		<td>
			<table class="inside_form_table">
				<tr>
					<td valign="top" colspan="3">
						<?=$linterface->Get_Radio_Button("findByGroup", "data[findBy]", 'Group', true, "", $Lang['libms']['report']['findByGroup'])?>
						<?=$linterface->Get_Radio_Button("findByStudent", "data[findBy]", 'Student', false, "", $Lang['libms']['report']['findByStudent'])?>
					</td>
				</tr>
				<tr class="table_findBy">
					<td valign="top">
						<select name="data[groupTargetType]" id="groupTargetType">
							<option value="group" selected><?=$Lang["libms"]["report"]["targetGroup"]?></option>
							<option value="member"><?=$Lang["libms"]["report"]["targetGroupMember"]?></option>
						</select>
					</td>

					<td valign="top" nowrap colspan="2">
						<span id='groupname'>
						<select name="data[groupTarget][]" id="groupTarget" multiple size="7" >
						<!-- Get_Select  -->
						</select>
						</span>
						<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnGroups');?>

						<!-- Group Member //-->
						<span id='groupmember' style='display:none;'>
							<select name="data[memberID][]" multiple size="7" id="memberIDs"></select> 
							<?= $linterface->GET_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnGroupMembers')?>
						</span>
					</td>
<!--				
					<td valign="top" colspan="3">
						<select name="data[groupTarget][]" id="groupTarget" multiple size="7" >
-->						
						<!-- Get_Select  -->
<!--						
						</select>
						<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnGroups');?>
					</td>
-->					
				</tr>
				<tr class="table_findBy" style='display:none;'>
					<td valign="top">
						<select name="data[rankTarget]" id="rankTarget">
							<option value="form" selected><?=$Lang['libms']['report']['Form']?></option>
							<option value="class"><?=$Lang['libms']['report']['Class']?></option>
							<option value="student"><?=$Lang['libms']['report']['Student']?></option>
						</select>
					</td>
					<td valign="top" nowrap  colspan="2">
						<!-- Form / Class //-->
						<span id='classname'>
						<select name="data[classname][]" multiple id="classnames" size="7"></select>
						</span>
						<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnClassnames');?>

							
						<!-- Student //-->
						<span id='Student' style='display:none;'>
							<select name="data[studentID][]" multiple size="7" id="studentIDs"></select> 
							<?= $linterface->GET_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnStudent')?>
						</span>
						
					</td>
				</tr>
				<tr><td colspan="3" class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></td></tr>
                <tr id="tr_includesAll">
                    <td valign="top" colspan="3">
                       <input type="radio" name="readingFilter" id="incAll" value='all'><label for="incAll"><?=$Lang["libms"]["eBookStat"]["incAll"]?></label>
                    </td>
                </tr>
                <tr id="tr_readRecordOnly">
                    <td valign="top" colspan="3">
                       <input type="radio" name="readingFilter" id="showReadOnly" value='read' checked><label for="showReadOnly"><?=$Lang["libms"]["eBookStat"]["showReadRecordOnly"]?></label>
                    </td>
                </tr>
                
                
                <tr id="tr_showPercentageUp" class="progressFilter">
                    <td valign="top" colspan="3">
                       <input type="radio" name="readingFilter" id="showPercentageCol" value='readup'><label for="showPercentageCol"><?=$Lang["libms"]["eBookStat"]["showPercentCol"]?></label>
                       <select id="percentage" name="percentage">
                       		<?php echo $percentageOptionList;?>
                       </select>
                       <label for="showPercentageCol"><?=$Lang["libms"]["eBookStat"]["showPercentCol2"]?></label>
                    </td>
                </tr>
                
                <tr id="tr_showPercentageDown" class="progressFilter">
                    <td valign="top" colspan="3">
                       <input type="radio" name="readingFilter" id="showPercentageDownCol" value='readdown'><label for="showPercentageDownCol"><?=$Lang["libms"]["eBookStat"]["showPercentDownCol"]?></label>
                       <select id="percentageDown" name="percentageDown">
                       		<?php echo $percentageOptionList;?>
                       </select>
                       <label for="showPercentageDownCol"><?=$Lang["libms"]["eBookStat"]["showPercentCol2"]?></label>
                    </td>
                </tr>
                
			</table>
			<span id='div_Target_err_msg'></span>
		</td>
	</tr>
		
				
	<?
	$x = ob_get_contents();
	ob_end_clean();
	return $x;
}	// end getForm_TargetUserSelection

?>
<link rel="stylesheet" href="sorttable.css">
<script language="JavaScript" src="sorttable.js"></script>
<script language="javascript">
$(document).ready(function(){

	$.ajaxSetup({ cache:false,
			   async:false });
			   
	//---------------------------------------------------------------------------------------------------------------------- TargetUserSelection  Start
	ajaxSelectbox('../index.php?page=Ajax&function=getGroupName',$('#groupTarget'));
	ajaxSelectbox('../index.php?page=Ajax&function=getClassLevel', $('#classnames'));
	
	$('#findByGroup').change(function(){
		$('#tr_sortByType').attr('style', 'display: none');
		$('#tr_displayGenderTotal').attr('style', 'display: none');
		$("input:radio[name='data[sortBy][type]'][value ='Student']").attr('checked', true);
		$('.table_findBy').toggle();
	});
	
	$('#findByStudent').change(function(){
		$('#tr_sortByType').attr('style', '');
		$('#tr_displayGenderTotal').attr('style', '');
		$("input:radio[name='data[sortBy][type]'][value ='ClassLevel']").attr('checked', true);
		$('.table_findBy').toggle();
	});

	$('#rankTarget').change(function(){
		switch($('#rankTarget').val())
		{
		case 'form': 
			page = "../index.php?page=Ajax&function=getClassLevel";
			ajaxSelectbox(page, $('#classnames'));
			$('#classnames').attr('multiple',true);
			$('#classnames option').attr('selected', true);
			$('#selectAllBtnClassnames').attr('style','display:inline');
			$('#Student').attr('style','display:none');	
			break;
		case 'class': 
			page= "../index.php?page=Ajax&function=getClassNameByLevel";
			ajaxSelectbox(page, $('#classnames'));
			$('#classnames').attr('multiple',true);
			$('#classnames option').attr('selected', true);
			$('#selectAllBtnClassnames').attr('style','display:inline');
			$('#Student').attr('style','display:none');	
			break;
		case 'student': 
			page= "../index.php?page=Ajax&function=getClassNameByLevel";
			ajaxSelectbox(page, $('#classnames'));
			$('#classnames').attr('multiple',false);
			$("#classname option:first").attr('selected','selected');
			student_list();
			$('#selectAllBtnClassnames').attr('style','display:none');
			$('#Student').attr('style','display:inline');	
			break;
		};
	});
	
	$('#groupTargetType').change(function(){
		switch($('#groupTargetType').val()){
			case 'group': 
				page = "../index.php?page=Ajax&function=getGroupName";
				ajaxSelectbox(page, $('#groupTarget'));
				$('#groupTarget').attr('multiple',true);
				$('#groupTarget option').attr('selected', true);
				$('#selectAllBtnGroups').attr('style','display:inline');
				$('#groupmember').attr('style','display:none');	
				break;
			case 'member':
				page = "../index.php?page=Ajax&function=getGroupName";
				ajaxSelectbox(page, $('#groupTarget'));
				$('#groupTarget').attr('multiple',false);
				$("#groupTarget option:first").attr('selected','selected');
				member_list();
				$('#selectAllBtnGroups').attr('style','display:none');
				$('#groupmember').attr('style','display:inline');	
				break;
		};
	});
	
	$('#classnames').change(function(){
		student_list();
	});
	
	$('#groupTarget').change(function(){
		member_list();
	});
	
	//----------------------------------------------------------------------------------------------------------------------  TargetUserSelection  End
	
	$('#classnames option').attr('selected', true);
	$('#groupTarget option').attr('selected', true);
	
	//---------------------------------------------------------------------------------------------------------------------- Select All btn  Start
	$('#selectAllBtnClassnames').click(function(){ 			$('#classnames option').attr('selected', true);  		});
	$('#selectAllBtnGroups').click(function(){ 				$('#groupTarget option').attr('selected', true);  		});
	$('#selectAllBtnStudent').click(function(){ 			$('#studentIDs option').attr('selected', true);  		});
	$('#selectAllBtnGroupMembers').click(function(){ 		$('#memberIDs option').attr('selected', true);  		});
	//---------------------------------------------------------------------------------------------------------------------- Select All btn  End

/*
	$('#showPercentageCol').change(function(){
		if ($(this).attr('checked')) {
			$('#tr_includesAll').css('display','none');
			$('#tr_showPercentageDown').css('display','none');
		}
		else {
			$('#tr_includesAll').css('display','');
			$('#tr_showPercentageDown').css('display','');
		}
	});

	$('#showPercentageDownCol').change(function(){
		if ($(this).attr('checked')) {
			$('#tr_includesAll').css('display','none');
			$('#tr_showPercentageUp').css('display','none');
		}
		else {
			$('#tr_includesAll').css('display','');
			$('#tr_showPercentageUp').css('display','');
		}
	});
	
	$('#incAll').change(function(){
		if ($(this).attr('checked')) {
			$('.progressFilter').css('display','none');
		}
		else {
			$('.progressFilter').css('display','');
		}
	});
*/
});	

function ClassViewexport(){
	document.form1.action = 'ajax_class_view.php?action=export';
	document.form1.submit();
	document.form1.action ='';
}

function hideOptionLayer()
{
	$('#formContent').attr('style', 'display: none');
	$('#formbutton').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
}

function showOptionLayer()
{
	$('#formContent').attr('style', '');
	$('#formbutton').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
}
					
function Js_Select_All(id){
	$('#'+id + ' option').attr('selected', 'selected');	
}

function Js_Check_Form(obj){

	var pass = check_date(document.getElementById("StartDate"),"<?=$Lang['General']['InvalidDateFormat']?>");
	if (pass)
		pass = pass & check_date(document.getElementById("EndDate"),"<?=$Lang['General']['InvalidDateFormat']?>");
	if (pass)
		pass = pass & compare_date(document.getElementById("StartDate"), document.getElementById("EndDate"),"<?=$Lang['General']['WrongDateLogic']?>");
	if ( pass && ($('#findByGroup').attr('checked')) && ($('#groupTargetType').val() == 'group') && (!$("#groupTarget option:selected").length )) {
		alert("<?=$Lang['libms']['report']['msg']['please_select_group']?>");
		pass = false;					
	}										
	if ( pass && ($('#findByGroup').attr('checked')) && ($('#groupTargetType').val() == 'member') && (!$("#memberIDs option:selected").length )) {
		alert("<?=$Lang['libms']['report']['msg']['please_select_group_member']?>");
		pass = false;					
	}										
	if ( pass && ($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'form') && (!$("#classnames option:selected").length )) {
		alert("<?=$Lang['libms']['report']['msg']['please_select_form']?>");
		pass = false;					
	}										
	if ( pass && ($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'class') && (!$("#classnames option:selected").length )) {
		alert("<?=$Lang['libms']['report']['msg']['please_select_class']?>");
		pass = false;					
	}										
	if ( pass && ($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'student') && (!$("#studentIDs option:selected").length )) {
		alert("<?=$Lang['libms']['report']['msg']['please_select_student']?>");
		pass = false;					
	}										

	if ( pass == true )	// true
	{
		$('#TableDiv').html('<center><?=$Lang["libms"]["ajaxLoading"]?></center>');
		$.ajax({
        type : "POST",
        url : "ajax_class_view.php",
        data: $("#form1").serialize(),
        success : function(msg) {
			$('#TableDiv').html(msg);
			$('#TableDiv').show();
			$('#StudentDiv').hide();
			if(document.getElementById("sortable") != null){
				sorttable.makeSortable(document.getElementById('sortable'));
			}
			$('#controlForm').show();
			$('#formContent').hide();
			$('#formbutton').hide();
			$('#spanHideOption').hide();
			$('#spanShowOption').show();
        }
 	   });
	}

}

function ajaxSelectbox(page, fieldName){
		$.get(page, {},
			function(insertRespond){
				if (insertRespond == ''){
//					alert('<?=$Lang["libms"]["general"]["ajaxReturnNothing"]?>');
					fieldName.html('');
				}else {
					fieldName.html(insertRespond);
				}
			}
		);
}

function student_list(){
	if ($('#rankTarget').val() == 'student')
		{
		page = "../index.php?page=Ajax&function=getStudentByClass&classNames="+$('#classnames :selected').text();
		ajaxSelectbox(page, $('#studentIDs'));
		$('#studentIDs option').attr('selected', true);
		}
}

function member_list(){
	if ($('#groupTargetType').val() == 'member') {
		page = "../index.php?page=Ajax&function=getGroupMembers&groupIDs="+$('#groupTarget :selected').val();
		ajaxSelectbox(page, $('#memberIDs'));
		$('#memberIDs option').attr('selected', true);
	}
}

</script>

<form name="form1" id="form1" method="post" action="">
   <div class="report_option report_show_option" id="controlForm" style="">
      <table width="99%" border="0" cellspacing="0" cellpadding="0" class="print_hide">
         <tr>
            <td width="13">&nbsp;</td>
            <td>
               <span id="spanShowOption" class="spanShowOption" style="display:none">
               <a href="javascript:showOptionLayer();"><?=$Lang["libms"]["report"]["showoption"]?></a>
               </span> 
               <span id="spanHideOption" class="spanHideOption" style="display:none">
               <a href="javascript:hideOptionLayer();"><?=$Lang["libms"]["report"]["hideoption"]?></a>
               </span>
               <div class="report_option report_option_title"></div>
               <div class="table_board">
                  <span id="formContent" style="" >
                     <table class="form_table">
<?=getForm_TargetUserSelection()?>
                        <tr class="form_table ">
                           <td class="field_title" valign="top"><span class="tabletextrequire">*</span> <?=$Lang['libms']['reporting']['dateRange']?></td>
                           <td><?=$linterface->GET_DATE_PICKER("StartDate", ($StartDate?$StartDate:((date('m')>=9) ? date('Y-09-01') : (date('Y')-1).'-09-01')))?> <span class="tabletextremark"></span> ~ <?=$linterface->GET_DATE_PICKER("EndDate", $EndDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
                        </tr>
                     </table>
                     <?=$linterface->MandatoryField();?>
                     <p class="spacer"></p>
                  </span>
               </div>
               <div id="formbutton">
                  <p class="spacer"></p>
                  <div class="edit_bottom" >
                     <p class="spacer"></p>
                     <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "Js_Check_Form(this.form)", "GenerateBtn")?>        
                  </div>
            </td>
            </div>
            <td width="11">&nbsp;</td>
         </tr>
      </table>
   </div>
</form>
<div id="TableDiv"></div>
<div id="StudentDiv" style="display:none;"></div>
<?php 
$linterface->LAYOUT_STOP();
intranet_closedb();
?>