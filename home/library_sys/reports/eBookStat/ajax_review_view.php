<?php

// Using: 

/***************************************
 * 
 * Date:	2018-01-22 (Cameron)
 * Details:	- restrict to eBook only (case #W133701)
 * 
 * Date:	2016-05-04 (Cameron)
 * Details: - correct Time Condition sql
 * 			- add condition Publish=1 so that it's consistent to "eBooks Statistics" report  			
 * 
 * Date:	2014-04-25 (Henry)
 * Details:	Created this file
 * 
 ***************************************/
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();

if(!$_SESSION['LIBMS']['admin']['current_right']['statistics']['ebook statistics'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$linterface = new interface_html();
$lelibplus = new elibrary_plus($book_id, $UserID, $book_type="type_pc", $range="accumulated");

# User Condition
$UserCond = "r.UserID = '{$uID}'";
  
# Time Condition
//$TimeCond = "r.DateModified BETWEEN '$StartDate' AND '$EndDate'  + INTERVAL 1 DAY";
$TimeCond = "r.DateModified BETWEEN '$StartDate 00:00:00' AND '$EndDate 23:59:59' ";
$physical_book_init_value = 10000000;

$sql = "SELECT r.BookID, b.Title, r.Content, r.Rating, rh.HelpfulCount, r.DateModified
		FROM INTRANET_ELIB_BOOK_REVIEW r
		left join (select count(*) as HelpfulCount ,ReviewID from INTRANET_ELIB_BOOK_REVIEW_HELPFUL group by ReviewID) rh on rh.ReviewID = r.ReviewID
		inner join INTRANET_ELIB_BOOK b on b.BookID = r.BookID
		where {$UserCond} And {$TimeCond} AND b.Publish=1 AND r.BookID<={$physical_book_init_value} order by  b.Title";

$ReviewRecords = $lelibplus->returnArray($sql);
# Init
echo $libms->Get_Review_View($ReviewRecords);
intranet_closedb();
?>