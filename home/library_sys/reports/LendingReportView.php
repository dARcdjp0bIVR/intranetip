<?php
# modifying by: 

/**
 * Log :
 * Date:    2019-09-06 [Cameron]
 *          - use high chart to show statistics so that it won't need to use color to find matching of classes [case #M152634]
 * Date:	2017-10-03 [Cameron]
 * 			- add "include left student" option in getForm_TargetUserSelection() [case #A126787]
 * 			- add "left student" indicator in getRecord_RecordList() and getCSV()
 * Date:	2016-07-15 [Cameron]
 * 			- hide the warning and empty option list if it returns nothing in ajaxSelectbox (IP2.5.7.7.1)
 * 			- don't allow to run report if user do not select target and period
 * Date:	2016-01-15 [Cameron]
 * 			- Add book tags to filter
 * 			- Add suggestions for tags when typing
 * Date:	2015-07-21 [Cameron] Add radio option "All" to BookCategory
 * Date:	2014-01-26 [Henry] modified getForm_BookCategory()
 * Date:	2014-10-07 [Henry]	Modified getForm_Order(), getRecord_RecordList(), getCSV()
 * Date: 	2014-05-14 [Henry]  Added school name at top of the page
 * Date:	2014-01-14 {Henry]	allow to search the target by group
 * Date:	2013-12-13 [Henry]	modified getRecord_RecordList() to separate the student name, class, class number to different columns
 * Date:	2013-11-20 [Henry]	Fix bug: show and print the bar chart correctly, allow to sort by student
 * Date:	2013-06-17 [Cameron] Fix bug: don't allow to select none in group target 
 * Date 	2013-05-06 [Cameron]
 * 			Show filter period when print
 * Date:	2013-04-24 [yuen]
 * 			1. don't select all items on Circulation Category, Book Category, Subject nor Language by default
 * 			2. preset date range as current year
 * 
 * Date:	2013-04-23 [Cameron]
 * 			Add date validation function before submit criteria for report: $('#submit_result').click(function()
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../";
$MODULE_ROOT = "../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

require_once('ReportView.php');

global $Lang;
$linterface = new interface_html("libms.html");

class LendingReportView extends ReportView{
	public function __construct(){
	
		$CurrentPageArr['LIBMS'] = 1;
		$CurrentPage = "PageReportingLending";
		parent::__construct($CurrentPage);
	}
	
	public function getView($viewdata = null){
		global $Lang;
	
		if (empty($print)){
			$x .= $this->getHeader();
			$x .= $this->getJS($viewdata);
			$x .= $this->getFrom($viewdata);
// 			$x .= $this->getToolbar($viewdata);
// 			$x .= $this->getResult($viewdata);
			$x .= $this->getFooter();
		}
		return $x;
	}

    public function getDrawHighChart()
    {
        global $Lang;
        ob_start();
        ?>
        function drawChart(chartid, obj, showGenderQty)
        {
        var series = [];
        if (showGenderQty == 'true') {
        series.push({name:'<?php echo $Lang["libms"]["report"]["Male"];?>', data:obj.maleArray, color:obj.color[1], stack:'male'});
        series.push({name:'<?php echo $Lang["libms"]["report"]["Female"];?>', data:obj.femaleArray, color:obj.color[2], stack:'female'});
        series.push({name:'<?php echo $Lang["libms"]["report"]["total"];?>', data:obj.array, color:obj.color[0], stack:'total'});
        pointFormat = '{series.name}: {point.y}';
        }
        else {
        series.push({name:'<?php echo $Lang["libms"]["report"]["total"];?>', data:obj.array, color:obj.color[0]});
        pointFormat = '{series.name}: {point.y}';
        }

        var chart = Highcharts.chart(chartid, {
        chart: {
        type: 'column'
        },
        title: {
        text: obj.title
        },
        xAxis:{
        categories: obj.name,
        title:{
        text: null
        }
        },
        yAxis: {
        min: 0,
        max: obj.type=='percent' ? 100 : undefined,
        title: {
        text: '<?php echo $Lang["libms"]["report"]["LendingTotal"];?>',
        },
        stackLabels: {
        enabled: false,
        style: {
        fontWeight: 'bold',
        color: 'brown'
        }
        }
        },
        legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor: 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
        },
        tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: pointFormat
        },
        plotOptions: {
        column: {
        stacking: 'normal',
        dataLabels: {
        enabled: false
        }
        }
        },
        credits: {
        enabled: false
        },
        exporting: {
        enabled: false
        },
        series: series
        });
        return chart;
        }
        <?php
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }

	public function getJS($viewdata){
		global $Lang, $linterface;
		ob_start();
		?>
		<link href="css/visualize.css" type="text/css" rel="stylesheet" />
		<link href="css/visualize-light.css" type="text/css" rel="stylesheet" />
		<link href="css/reports.css" type="text/css" rel="stylesheet" />
		<script src="/templates/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/number_rounding.js"></script>
		<script type="text/javascript" src="js/visualize.jQuery.js"></script>
		<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
		<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">
        <?php echo $linterface->Include_HighChart_JS();?>
		<script type="text/javascript" >

		var timerobj;
		function jsShowOrHideTags()
		{
			timerobj = setTimeout(ShowOrHideTagsAction,1000);
		}
		
		function ShowOrHideTagsAction()
		{
			$("#tags").focus();
			clearTimeout(timerobj);
		}
		
		function hideOptionLayer()
		{
			$('#formContent').attr('style', 'display: none');
			$('.spanHideOption').attr('style', 'display: none');
			$('.spanShowOption').attr('style', '');
			//document.getElementById('div_form').className = 'report_option report_hide_option';
		}
		
		function showOptionLayer()
		{
			$('#formContent').attr('style', '');
			$('.spanShowOption').attr('style', 'display: none');
			$('.spanHideOption').attr('style', '');
			//document.getElementById('div_form').className = 'report_option report_show_option';
		}
		
		function click_export()
		{
			document.form1.action = "?page=Lending&action=export";
			document.form1.submit();
			document.form1.action = "?page=Lending";
		}

		function click_print()
		{
			var url = "?page=Lending&action=print&show=list";
			if ($('#result_stat').css('display') == 'none') {
				url = "?page=Lending&action=print&show=chart";
			}
			document.form1.action=url;
			document.form1.target="_blank";
			document.form1.submit();
			
			document.form1.action="?page=Lending";
			document.form1.target="_self";
		}
		
		
		function ajaxSelectbox(page, fieldName){
				$.get(page, {},
					function(insertRespond){
						if (insertRespond == ''){
//							alert('Ajax call return nothing. Please try again. If problem persist, make sure the setting is completed / contact your administrator');
							fieldName.html('');
						}else {
							fieldName.html(insertRespond);
						}
					}
				);
		}
		
		function student_list(){
			if ($('#rankTarget').val() == 'student')
				{
				page = "?page=Ajax&function=getStudentByClass&classNames="+$('#classnames :selected').text();
				ajaxSelectbox(page, $('#studentIDs'));
				$('#studentIDs option').attr('selected', true);
				}
		}

<?php echo $this->getDrawHighChart();?>

		//--------------------------------->
		$().ready( function(){
		
			$.ajaxSetup({ cache:false,
					   async:false });
					   
			//---------------------------------------------------------------------------------------------------------------------- TargetUserSelection  Start
			ajaxSelectbox('?page=Ajax&function=getGroupName',$('#groupTarget'));
			ajaxSelectbox('?page=Ajax&function=getClassLevel', $('#classnames'));
			
			$('#findByGroup').change(function(){
				$('#tr_sortByType').attr('style', 'display: none');
				$('#tr_displayGenderTotal').attr('style', 'display: none');
				$("input:radio[name='data[sortBy][type]'][value ='Student']").attr('checked', true);
				$('.table_findBy').toggle();
			});
			
			$('#findByStudent').change(function(){
				$('#tr_sortByType').attr('style', '');
				$('#tr_displayGenderTotal').attr('style', '');
				$("input:radio[name='data[sortBy][type]'][value ='ClassLevel']").attr('checked', true);
				$('.table_findBy').toggle();
			});
			$('#sortByStudent').change(function(){
				$('#tr_displayGenderTotal').attr('style', 'display: none');
				$("input:radio[name='data[displayGenderTotal]'][value ='0']").attr('checked', true);
			});
			$('#sortByClass').change(function(){
				$('#tr_displayGenderTotal').attr('style', '');
				$("input:radio[name='data[displayGenderTotal]'][value ='0']").attr('checked', true);
			});
			$('#sortByForm').change(function(){
				$('#tr_displayGenderTotal').attr('style', '');
				$("input:radio[name='data[displayGenderTotal]'][value ='0']").attr('checked', true);
			});
			
			$('input[name="BookCategoryType"]').change(function(){
				if($('#BookCategoryTypeAll').is(':checked'))
				{
					$('#table_CategoryCode1').attr('style','display:none');
					$('#table_CategoryCode2').attr('style','display:none');
					$('#Categories option').attr('selected', false);
					$('#Categories2 option').attr('selected', false);
				}
				else if ($('#BookCategoryType1').is(':checked'))
				{				
					$('#table_CategoryCode1').attr('style','display:block');
					$('#table_CategoryCode2').attr('style','display:none');
					$('#Categories2 option').attr('selected', false);
				}
				else if ($('#BookCategoryType2').is(':checked'))
				{
					$('#table_CategoryCode1').attr('style','display:none');
					$('#table_CategoryCode2').attr('style','display:block');
					$('#Categories option').attr('selected', false);
				}
			});
					
			$('#rankTarget').change(function(){
				switch($('#rankTarget').val())
				{
				case 'form': 
					page = "?page=Ajax&function=getClassLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',true);
					$('#classnames option').attr('selected', true);
					$('#selectAllBtnClassnames').attr('style','display:inline');
					$('#Student').attr('style','display:none');	
					break;
				case 'class': 
					page= "?page=Ajax&function=getClassNameByLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',true);
					$('#classnames option').attr('selected', true);
					$('#selectAllBtnClassnames').attr('style','display:inline');
					$('#Student').attr('style','display:none');	
					break;
				case 'student': 
					page= "?page=Ajax&function=getClassNameByLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',false);
					$("#classname option:first").attr('selected','selected');
					student_list();
					$('#selectAllBtnClassnames').attr('style','display:none');
					$('#Student').attr('style','display:inline');	
					break;
				};
			});
			
			$('#classnames').change(function(){
				student_list();
			});
			//----------------------------------------------------------------------------------------------------------------------  TargetUserSelection  End 
			
			<?php if(is_null($viewdata['Result'])){ ?>
			$('#classnames option').attr('selected', true);
			//$('#CirCategory option').attr('selected', true);
			//$('#subject option').attr('selected', true);	
			//$('#Categories option').attr('selected', true);
			//$('#language option').attr('selected', true);
			$('#groupTarget option').attr('selected', true);
			$('#classnames option').attr('selected', true);
			<?php }?>
			
			<?php if (isset($viewdata['Result'])) {?>
				$('#formContent').attr('style','display: none');
			<? }?>
			
			//---------------------------------------------------------------------------------------------------------------------- Select All btn  Start
			$('#selectAllBtnClassnames').click(function(){ 			$('#classnames option').attr('selected', true);  		});
			$('#selectAllBtnGroups').click(function(){ 			$('#groupTarget option').attr('selected', true);  		});
			$('#selectAllBtnStudent').click(function(){ 			$('#studentIDs option').attr('selected', true);  		});
			$('#selectAllBtnCirCat').click(function(){ 				$('#CirCategory option').attr('selected', true);  		});
			$('#selectAllBtnCategories').click(function(){ 			$('#Categories option').attr('selected', true);			});
			$('#selectAllBtnCategories2').click(function(){ 		$('#Categories2 option').attr('selected', true);		});
			$('#selectAllBtnSubject').click(function(){ 			$('#subject option').attr('selected', true);			});
			$('#selectAllBtnLanguage').click(function(){ 			$('#language option').attr('selected', true);			});
			//---------------------------------------------------------------------------------------------------------------------- Select All btn  End
			
			
			$('#submit_result').click(function(){

				if (($('#findByGroup').attr('checked')) && (!$("#groupTarget option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_group']?>");
					return;					
				}										
				if (($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'form') && (!$("#classnames option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_form']?>");
					return;					
				}										
				if (($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'class') && (!$("#classnames option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_class']?>");
					return;					
				}										
				if (($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'student') && (!$("#studentIDs option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_student']?>");
					return;					
				}										

				if (($('#radioPeriod_Date').attr('checked'))) {
					if (!check_date(document.getElementById("textFromDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
						return;
					if (!check_date(document.getElementById("textToDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
						return;
					if (!compare_date(document.getElementById("textFromDate"), document.getElementById("textToDate"),"<?=$Lang['General']['WrongDateLogic']?>"))
						return;	
				}
				else {
					if ( ($('#radioPeriod_Year').attr('checked')) && $("#data\\[AcademicYear\\]").val() == '' ) {					
						alert("<?=$Lang['libms']['report']['msg']['please_select_academic_year']?>");
						return;					
					}										
				}

				if ($('#BookCategoryType1').is(':checked')) {
					if($('#Categories option:checked').length == 0) {
						$('#Categories option').attr('selected', true);	
					}						
				}
				if ($('#BookCategoryType2').is(':checked')) {
					if($('#Categories2 option:checked').length == 0) {
						$('#Categories2 option').attr('selected', true);	
					}						
				}
				
				$.post("?page=Lending&action=getResult", $("#formContent").serialize(),
				function(data) {
					$('#formContent').attr('style','display: none');
					$('#form_result').html(data);		
					$('#spanShowOption').show();
					$('#spanHideOption').hide();
					$('#report_show_option').addClass("report_option report_hide_option");

					// $('.visual_chart').each(function (){
					// 	$div = $('<div style="page-break-inside: avoid;" />')
					// 	barchart_colors = ['#be1e2d','#666699','#92d5ea','#ee8310','#8d10ee','#5a3b16','#26a4ed','#f45a90','#e9e744','#ce1e2d','#766699','#a2d5ea','#fe8310','#9d10ee','#6a3b16','#36a4ed','#045a90','#f9e744'];
                    //
					// 	$(this).visualize({type: 'bar', width: '660px', height : '200px', colors : barchart_colors}).appendTo($div);
                    //
					// 	$div.appendTo($('#result_chart'));
					//
					// });
					window.scrollTo(0, 0);
					$('.result_toggler').click(function(){
						if(this.className == 'result_toggler thumb_list_tab_on')
							return false;
						$('.result_toggler').toggleClass('thumb_list_tab_on');
						$('#result_stat').toggle();
						$('#result_chart').toggle();
						if($('#result_chart').css('display') == 'none'){
							$('#hidden_table').css("display","none");
						}
						else{
							$('#hidden_table').css("display","");
						}
						return false;
					});

					$('#result_chart').toggle();
					if($('#result_chart').css('display') == 'none'){
						$('#hidden_table').css("display","none");
					}
					else{
						$('#hidden_table').css("display","");
					}
				});
				
			});

			if($("#tags").length > 0){
				$("#tags").autocomplete(
			      "ajax_get_tags_suggestion.php",
			      {
			  			delay:3,
			  			minChars:1,
			  			matchContains:1,
			  			onItemSelect: function() { jsShowOrHideTags(); },
			  			formatItem: function(row){ return row[0]; },
			  			autoFill:false,
			  			overflow_y: 'auto',
			  			overflow_x: 'hidden',
			  			maxHeight: '200px',
			  			width:'306px'
			  		}
			    );
			}

		});
		</script>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	/**
	 * get Toolbar HTML code:  e.g. export print  ...... 
	 * @param array $viewdata
	 */
	public function getToolbar($viewdata){
		global $Lang;
		//don't show if not posted.
		if (!isset($viewdata['Result']))
			return;
		
		ob_start();
		?>

		<div id='toolbox' class="content_top_tool">
			<div class="Conntent_tool">
				<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?></a>
				<a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?></a>
			</div>
			<br style="clear:both" />
		</div>
		<? 
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
		
	}
	
	public function getFrom($viewdata){
		global $Lang;

	
		ob_start();
		?>


			<div id="report_show_option">
				
				<span id="spanShowOption" class="spanShowOption" style="display:none">
					<a href="javascript:showOptionLayer();"><?=$Lang["libms"]["report"]["showoption"]?></a>
				</span> 
				
				<span id="spanHideOption" class="spanHideOption" style="display:none">
					<a href="javascript:hideOptionLayer();"><?=$Lang["libms"]["report"]["hideoption"]?></a>
				</span>

		
				<form name="form1" method="post" action="?page=Lending" onSubmit="return checkForm();"  id="formContent">
					
					<table class="form_table_v30">
				<? 
						echo $this->getForm_TargetUserSelection($viewdata);
						echo $this->getForm_Period($viewdata);
						echo $this->getForm_Order($viewdata);
						echo $this->getForm_BookCirculationType($viewdata);
						echo $this->getForm_BookCategory($viewdata);
						echo $this->getForm_BookSubject($viewdata);
						echo $this->getForm_BookLanguage($viewdata);
						echo $this->getForm_BookTags();						
						
				?>
					</table>
					
					<?=$this->linterface->MandatoryField();?>
					
					<div class="edit_bottom_v30">
						<p class="spacer"></p>
						<?= $this->linterface->GET_ACTION_BTN($Lang['libms']['report']['Submit'], "button", "", "submit_result")?>
						<p class="spacer"></p>
					</div>
				</form>
			</div>
			<div id="form_result"> </div>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_Period($viewdata){
		global $Lang, $junior_mck;
		$selectSchoolYearHTML =$viewdata['selectSchoolYearHTML'];
		$Period =$viewdata['Period'];
		$textFromDate = $viewdata['textFromDate'];
		$textToDate = $viewdata['textToDate'];
		if ($textFromDate=="" && $textToDate=="")
		{
			# preset date range as current school year
			$current_period= getPeriodOfAcademicYear(Get_Current_Academic_Year_ID());

			if (is_array($current_period) && sizeof($current_period)>0)
			{
				foreach($current_period as &$date){
					$date = date('Y-m-d',strtotime($date));
				}
				$textFromDate = $current_period['StartDate'];
				$textToDate = $current_period['EndDate'];
			} else
			{
				$textFromDate = (date('m')>=9) ? date('Y-09-01') : (date('Y')-1).'-09-01';
				$textToDate = date('Y-m-d');
			}
			
		}
		ob_start();
		?>	
		<!-- Period -->
		<tr valign="top">
		<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['time_period']?></td>
			<td>
				<table class="inside_form_table"  style='width: 350px;'>
<?php if (!isset($junior_mck)) {
	 ?> 
					<tr>
						<td colspan="6" >
							<input name="data[Period]" type="radio" id="radioPeriod_Year" value="YEAR" <? echo ($Period!="DATE")?" checked":"" ?>  >
							<?=$Lang['libms']['report']['SchoolYear']?>
							<?=getSelectAcademicYear("data[AcademicYear]", "");?>&nbsp;
							<?//=$Lang['libms']['report']['Semester']?>
							
						</td>
					</tr>
<?php } ?>		
					<tr>
						<td>
							<input name="data[Period]" type="radio" id="radioPeriod_Date" value="DATE" <? echo ($Period=="DATE" || isset($junior_mck))?" checked":"" ?> >
							<?=$Lang['libms']['report']['period_date_from']?>
						</td>
						<td >
							<?=$this->linterface->GET_DATE_PICKER("textFromDate",$textFromDate,"")?>
							<br />
							<span id='div_DateEnd_err_msg'></span>
						</td>
						<td><?=$Lang['libms']['report']['period_date_to']?> </td>
						<td >
							<?=$this->linterface->GET_DATE_PICKER("textToDate",$textToDate,"")?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<?	
		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}
	
	public function getForm_BookCirculationType($viewdata){
		global $Lang;
		global $linterface;
		
		$categoryElements =$viewdata['categoryElements'];
		$circulationLists = $viewdata['circulation'];

		ob_start();
		?>
		<tr>
			<td class="field_title"><?=$Lang['libms']['report']['circulation_type']?></td>
			<td>
							<span id="CategoryElements" >
							<select name="data[CirCategory][]" id="CirCategory" size="10" multiple >
							<?
									//if (!empty($categoryElements))
										foreach ($circulationLists as $key => $item) {
											if (isset($viewdata['post']['CirCategory']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['CirCategory']))
												$select=" selected ";
											else
												$select="";
											echo ("<option value='$key' $select>$item</option>");
										}
							?>
							</select>
							</span>
				<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnCirCat');?>
				<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
		
				<span id='div_Category_err_msg'></span>
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_BookCategory($viewdata){
		global $Lang;
		global $linterface;
		
		$Category =$viewdata['category'];
		//Henry Added [20150122]
		$Category1 =$viewdata['category1'];
		$Category2 =$viewdata['category2'];
		
		ob_start();
		?>
			<tr>
				<td class="field_title"><?=$Lang['libms']['report']['book_category']?></td>
				<td><!--
								<span id="Subjects">
									<select name="data[Categories][]" id="Categories" size="10" multiple><?php
 									
										if (!empty($Category)){
											foreach ($Category as $key => $item){
												if (isset($viewdata['post']['Categories']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['Categories'])) 
													$select=" selected "; 
												else  
													$select="";
												echo "<option value='$key' $select>$item</option>";
											}
										}
									?>
									</select>
								</span>
					<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnCategories');?>
					<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
					<span id='div_Category_err_msg'></span>
					-->
					
	        		<table class="inside_form_table">
						<tr>
							<td valign="top">
								<?
								$libms = new liblms();
								$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
								foreach ($result as $row){
									$book_category_name[] = htmlspecialchars($row['value']);
								}
								?>
								<?=$linterface->Get_Radio_Button("BookCategoryTypeAll", "BookCategoryType", 0, true, "", $Lang["libms"]["report"]["fine_status_all"])?>								
								<?=$linterface->Get_Radio_Button("BookCategoryType1", "BookCategoryType", 1, false, "", $book_category_name[0]?$book_category_name[0]:$Lang['libms']['report']['book_category'].' 1')?>
								<?=$linterface->Get_Radio_Button("BookCategoryType2", "BookCategoryType", 2, false, "", $book_category_name[1]?$book_category_name[1]:$Lang['libms']['report']['book_category'].' 2')?>
							</td>
						</tr>
						<tr class="table_CategoryCode" id="table_CategoryCode1" style='<?=($viewdata['post']['Categories']==1?'':'display:none;')?>'>
							<td valign="top">
								<span id="Subjects">
									<select name="data[Categories][]" id="Categories" size="10" multiple><?php
 									
										if (!empty($Category1)){
											foreach ($Category1 as $key => $item){
												if (isset($viewdata['post']['Categories']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['Categories'])) 
													$select=" selected "; 
												else  
													$select="";
												echo "<option value='$key' $select>$item</option>";
											}
										}
									?>
									</select>
								</span>
								<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnCategories');?>
								<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
								<span id='div_Category_err_msg'>
							</td>
						</tr>
						<tr class="table_CategoryCode" id="table_CategoryCode2" style='<?=($viewdata['post']['Categories']==2?'':'display:none;')?>'>
							<td valign="top">
								<span id="Subjects">
									<select name="data[Categories][]" id="Categories2" size="10" multiple><?php
 									
										if (!empty($Category2)){
											foreach ($Category2 as $key => $item){
												if (isset($viewdata['post']['Categories']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['Categories'])) 
													$select=" selected "; 
												else  
													$select="";
												echo "<option value='$key' $select>$item</option>";
											}
										}
									?>
									</select>
								</span>
								<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnCategories2');?>
								<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
								<span id='div_Category_err_msg'>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	
	
	public function getForm_BookSubject($viewdata){
		global $Lang;
		global $linterface;
		
		$subjectList =$viewdata['subjects'];
		ob_start();
		?>
		<tr>
			<td class="field_title"><?=$Lang['libms']['report']['subject']?></td>
			<td>
							<span id="Subjects">
								<select name="data[subject][]" id="subject" size="10" multiple >
								<?php
									//if (!empty($subject))
										foreach ($subjectList as $key => $item){
											if (isset($viewdata['post']['subject']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['subject']))
												$select=" selected ";
											else{
												$select="";
// 												tolog(htmlspecialchars_decode($item,ENT_QUOTES));
// 												tolog($viewdata['post']['subject']);
											}
											echo "<option value=\"".$key."\" $select>$item</option>";
										}
								?>
								</select>
							</span>
				<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnSubject');?>
				<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
		
				<span id='div_Category_err_msg'></span>
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_BookLanguage($viewdata){
		global $Lang;
		global $linterface;
		//$Subjects =$viewdata['Languages'];
		$languageList =$viewdata['language'];
		
		ob_start();
		?>
		<tr>
			<td class="field_title"><?=$Lang['libms']['report']['language']?></td>
			<td>
							<span id="Subjects">
								<select name="data[language][]" id="language" size="10" multiple><?php
									//if (!empty($language))
										foreach ($languageList as $key => $item){
											if (isset($viewdata['post']['language']) && in_array(htmlspecialchars_decode($item,ENT_QUOTES), $viewdata['post']['language']))
												$select=" selected ";
											else{
												$select="";
												}
												echo "<option value='$key' $select>$item</option>";
										}
								?>
								</select>
							</span>
				<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnLanguage');?>
				<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
		
				<span id='div_Category_err_msg'></span>
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
		
	public function getForm_TargetUserSelection($viewdata){
		global $Lang;
		//todo
	
		$selectSchoolYearHTML =$viewdata['selectSchoolYearHTML'];
		$Period =$viewdata['Period'];
		$textFromDate = $viewdata['textFromDate'];
		$textToDate = $viewdata['textToDate'];
		
		
		ob_start();
		?>
		<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['target']?></td>
			<td>
				<table class="inside_form_table">
					<tr>
						<td valign="top">
							<?=$this->linterface->Get_Radio_Button("findByGroup", "data[findBy]", 'Group', true, "", $Lang['libms']['report']['findByGroup'])?>
							<?=$this->linterface->Get_Radio_Button("findByStudent", "data[findBy]", 'Student', false, "", $Lang['libms']['report']['findByStudent'])?>
						</td>
					</tr>
					<tr class="table_findBy">
						<td valign="top">
							<select name="data[groupTarget][]" id="groupTarget" multiple size="7" >
							<!-- Get_Select  -->
							</select>
							<?=$this->linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnGroups');?>
						</td>
						
					</tr>
					<tr class="table_findBy" style='display:none;'>
						<td valign="top">
							<select name="data[rankTarget]" id="rankTarget">
								<option value="form" selected><?=$Lang['libms']['report']['Form']?></option>
								<option value="class"><?=$Lang['libms']['report']['Class']?></option>
								<option value="student"><?=$Lang['libms']['report']['Student']?></option>
							</select>
						</td>
						<td valign="top" nowrap>
							<!-- Form / Class //-->
							<span id='classname'>
							<select name="data[classname][]" multiple id="classnames" size="7"></select>
							</span>
							<?=$this->linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnClassnames');?>

								
							<!-- Student //-->
							<span id='Student' style='display:none;'>
								<select name="data[studentID][]" multiple size="7" id="studentIDs"></select> 
								<?= $this->linterface->GET_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnStudent')?>
							</span>
							
						</td>
					</tr>
					<tr><td colspan="3" class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></td></tr>
				</table>
				<span id='div_Target_err_msg'></span>
			</td>
		</tr>
			
		<tr>
			<td class="field_title"><?=$Lang["libms"]["report"]["IncludeLeftStudent"]?></td>
			<td>
				<?=$this->linterface->Get_Checkbox("IncludeLeftStudent", "data[IncludeLeftStudent]", '1', '0')?>
			</td>
		</tr>

		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_Order($viewdata){
		global $Lang;
		ob_start();
		?>
		<tr id='tr_sortByType' style='display:none;'>
			<td class="field_title"><?=$Lang['libms']['report']['listby']?></td>
			<td>
				<?=$this ->linterface->Get_Radio_Button("sortByForm", "data[sortBy][type]", 'ClassLevel', false, "", $Lang['libms']['report']['Form'])?>
				<?=$this ->linterface->Get_Radio_Button("sortByClass", "data[sortBy][type]", 'ClassName', false, "", $Lang['libms']['report']['Class'])?>
				<?=$this ->linterface->Get_Radio_Button("sortByStudent", "data[sortBy][type]", 'Student', true, "", $Lang['libms']['report']['findByStudent'])?>
			</td>
		</tr>
		<tr id='tr_displayGenderTotal' style='display:none;'>
			<td class="field_title"><?=$Lang["libms"]["report"]["displayGenderTotal"]?></td>
			<td>
				<?=$this->linterface->Get_Radio_Button("displayGenderTotalYes", "data[displayGenderTotal]", '1', false, "", $Lang['libms']["settings"]['system']['yes'])?>
				<?=$this->linterface->Get_Radio_Button("displayGenderTotalNo", "data[displayGenderTotal]", '0', true, "", $Lang['libms']["settings"]['system']['no'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['libms']['report']['sortby']?></td>
			<td>
				<?=$this ->linterface->Get_Radio_Button("sortByOrderAsc", "data[sortBy][desc]", 'Asc', true, "", $Lang["libms"]["report"]["by_form_class"])?>
				<?=$this ->linterface->Get_Radio_Button("sortByOrderDesc", "data[sortBy][desc]", 'Desc', false, "", $Lang["libms"]["report"]["by_amount"])?>				
			</td>
		</tr>
		
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
		
	public function getForm_BookTags(){
		global $Lang;
		ob_start();
		?>
		<tr>
			<td class="field_title"><?=$Lang["libms"]["report"]["tags"]?></td>
			<td>
				<input autocomplete="off" id="tags" name="data[tags]" type="text" class="textbox_name" value="" />
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
		
	public function getResult($viewdata){
		global $Lang, $junior_mck;
		ob_start();
		echo '<div id="result_body"  >';
		
		if ( $_REQUEST['action'] != 'print')
		{
		?>
		<div class="thumb_list_tab print_hide">
			<a class='result_toggler' href="#">
				<span><?=$Lang["libms"]["reporting"]["span"]['graph']?></span>
			</a>
			<em>|</em>
			<a href="#" class="result_toggler thumb_list_tab_on">
				<span><?=$Lang["libms"]["reporting"]["span"]['table']?></span>
			</a>
		</div>
		<?
		}
		
		if ( $_REQUEST['action'] == 'print')
		{	
			if($_GET['show'] == 'list')
					echo "<div style='page-break-inside: avoid;'>";
			//Added school name at top of the page
			$schoolName =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
			echo '<p>'.$schoolName.'</p><center><h1>'.$Lang["libms"]["reporting"]["PageReportingLending"].'</h1></center>';
		
			// Print filter period info
			echo '<table border="0" width="100%"><tr><td>';
			echo  $Lang['libms']['report']['period_date_from'] . " " .
				$viewdata['post']['textFromDate'] . $Lang['libms']['report']['period_date_to'] . " " . $viewdata['post']['textToDate'];
			echo '</td><td align="right">';
			echo $Lang["libms"]["report"]["printdate"] . " ".date("Y-m-d").'</td></tr></table>';
		}
		
		echo '<div id="result_stat"  >';
			if (!(empty($viewdata['post'])) && (empty($viewdata['Result']))){
					echo $Lang["libms"]["report"]["norecord"];
			}elseif (!(empty($viewdata['post'])) && !(empty($viewdata['Result']))){
					echo $this->getRecord_RecordList($viewdata);
			}
		echo '</div>';
		echo '<div id="result_chart" style="text-align:left" >';
        if($_GET['action'] == 'print' && $_GET['show'] == 'chart') {
            echo $this->getRecord_RecordList($viewdata);
        }
		echo '</div>';
		echo '</div>';
		if($_GET['show'] == 'list')
			echo "</div>";				
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}

	public function getRecord_RecordList($viewdata){
        global $Lang;
        $sortType = $viewdata["post"]["sortBy"]["type"];

        $showGenderQty = false;
        if(!($_GET['action'] == 'print' && $_GET['show'] == 'chart')) {
            //Henry Modified 20131212 [start]
            if ($sortType == "Student") {
                if ($_REQUEST['action'] == 'print' && $_GET['show'] == 'list')
                    $x = "<div>";
                else
                    $x = "<div style='page-break-inside: avoid;'>";
                $x .= "<table class='common_table_list_v30 view_table_list_v30' width='100%'>";
                $x .= "<thead><tr>";
                if ($_REQUEST['action'] != 'print') {
                    $x .= "<td class='cell_bg_blue' width='30%'>" . $Lang["libms"]["report"]["ClassName"] . "</td>";
                    $x .= "<td class='cell_bg_blue' width='20%'>" . $Lang["libms"]["report"]["ClassNumber"] . "</td>";
                    $x .= "<td class='cell_bg_blue' width='30%'>" . $Lang["libms"]["report"]["{$sortType}"] . "</td>";
                } else {
                    $x .= "<td width='30%' style='font-weight:bold'>" . $Lang["libms"]["report"]["ClassName"] . "</td>";
                    $x .= "<td width='20%' style='font-weight:bold'>" . $Lang["libms"]["report"]["ClassNumber"] . "</td>";
                    $x .= "<td width='30%' style='font-weight:bold'>" . $Lang["libms"]["report"]["{$sortType}"] . "</td>";
                }
                $x .= "<th width='20%'><center>" . $Lang["libms"]["report"]["LendingTotal"] . "</center></th>";
                $x .= "</tr></thead>";
                $total_count = 0;
                foreach ($viewdata['Result'] as $items) {
                    $items["{$sortType}"] = trim($items["{$sortType}"]);
                    if ($items["{$sortType}"] != "--" && $items["{$sortType}"] != "") {
                        $leftStudentIndicator = $items["RecordStatus"] == '3' ? "<span style='color: red'>*</span>" : "";
                        $x .= "<tr>";
                        //style='background-color:white;color:rgb(0, 0, 0);'
                        //					$name = $items["{$sortType}"];
                        //					$match = preg_split('/ ?(\(|\)|-) ?/', $name, -1, PREG_SPLIT_NO_EMPTY);
                        if ($_REQUEST['action'] != 'print') {
                            $x .= "<th style='background-color:white;color:rgb(0, 0, 0);'>" . $items["ClassName"] . "</th>";
                            $x .= "<th style='background-color:white;color:rgb(0, 0, 0);'>" . $items["ClassNumber"] . "</th>";
                            $x .= "<th style='background-color:white;color:rgb(0, 0, 0);'>" . $items["onlyStudentName"] . $leftStudentIndicator . "</th>";
                        } else {
                            $x .= "<th style='background-color:white;color:rgb(0, 0, 0);font-weight:normal'>" . $items["ClassName"] . "</th>";
                            $x .= "<th style='background-color:white;color:rgb(0, 0, 0);font-weight:normal'>" . $items["ClassNumber"] . "</th>";
                            $x .= "<th style='background-color:white;color:rgb(0, 0, 0);font-weight:normal'>" . $items["onlyStudentName"] . $leftStudentIndicator . "</th>";
                        }
                        $x .= "<td align='center'>" . $items['Total'] . "</td>";
                        $x .= "</tr>";
                        $total_count += $items['Total'];
                    }
                }
                $x .= "<table class='common_table_list_v30 view_table_list_v30' width='100%'><tr>";
                $x .= "<td align='right' rowspan='3'>" . $Lang["libms"]["report"]["total"] . "</td>";
                $x .= "<td align='center' width='20%'>" . $total_count . "</td>";
                $x .= "</tr></table>";
                $x .= "</table></div><br/>";

                $x .= "<div>" . $Lang["libms"]["report"]["left_student_indicator"] . "</div>";
            }
            //Henry Modified 20131212 [end]

            ### build display table
            //Henry Modified 20131212 [start]
            if ($sortType == "Student") {
                if ($_REQUEST['action'] == 'print' && $_GET['show'] == 'list')
                    $x .= "<div style='display:none;position:absolute;'>";
                else
                    $x .= "<div id='hidden_table' style='page-break-inside: avoid;visibility:hidden;position:absolute;'>";
            } else //Henry Modified 20131212 [end]
                if ($_REQUEST['action'] == 'print' && $_GET['show'] == 'list')
                    $x = "<div>";
                else
                    $x = "<div style='page-break-inside: avoid;'>";

            $x .= "<table class='common_table_list_v30 view_table_list_v30 visual_chart' width='100%'>";
            $x .= "<thead><tr>";
            if ($_REQUEST['action'] != 'print') {
                $x .= "<td class='cell_bg_blue' width='25%'>" . $Lang["libms"]["report"]["{$sortType}"] . "</td>";
            } else {
                $x .= "<td width='25%' style='font-weight:bold'>" . $Lang["libms"]["report"]["{$sortType}"] . "</td>";
            }
            foreach ($viewdata['Result'] as $items) {
                if (isset($items['MaleTotal'])) {
                    $showGenderQty = true;
                    $x .= "<th width='25%'><center>" . $Lang["libms"]["report"]["LendingTotal"] . " (" . $Lang["libms"]["report"]["Male"] . ")" . "</center></th>";
                    $x .= "<th width='25%'><center>" . $Lang["libms"]["report"]["LendingTotal"] . " (" . $Lang["libms"]["report"]["Female"] . ")" . "</center></th>";
                    break;
                }
            }
            $x .= "<th width='25%'><center>" . $Lang["libms"]["report"]["LendingTotal"] . "</center></th>";
            $x .= "</tr></thead>";
        }   // not print chart

        $chartItemNameAry = array();
        $chartItemQtyAry = array();
        $chartItemMaleQtyAry = array();
        $chartItemFemaleQtyAry = array();

        if($_GET['action'] == 'print' && $_GET['show'] == 'chart') {
            $x = '';
            foreach($viewdata['Result'] as $items)
            {
                $items["{$sortType}"] = trim($items["{$sortType}"]);
                if ($items["{$sortType}"]!="--" && $items["{$sortType}"]!="")
                {
                    $chartItemNameAry[] = intranet_htmlspecialchars($items["{$sortType}"]);
                    $chartItemQtyAry[] = $items["Total"];

                    if(isset($items['MaleTotal'])){
                        $showGenderQty = true;
                        $chartItemMaleQtyAry[] = $items['MaleTotal'];
                        $chartItemFemaleQtyAry[] = $items['FemaleTotal'];
                    }
                }
            }
        }
        else {
			$total_count = 0;
			$total_male_count = 0;
			$total_female_count = 0;
			foreach($viewdata['Result'] as $items)
			{
				$items["{$sortType}"] = trim($items["{$sortType}"]);
				if ($items["{$sortType}"]!="--" && $items["{$sortType}"]!="")
				{
                    $chartItemNameAry[] = intranet_htmlspecialchars($items["{$sortType}"]);
                    $chartItemQtyAry[] = $items["Total"];

					$x .= "<tr>";
						//style='background-color:white;color:rgb(0, 0, 0);'
					if($_REQUEST['action'] != 'print'){	
						$x .= "<th style='background-color:white;color:rgb(0, 0, 0);'>".$items["{$sortType}"]."</th>";
						//Henry added [start]
//						$name = $items["{$sortType}"];
//						$match = preg_split('/ ?(\(|\)|-) ?/', $name, -1, PREG_SPLIT_NO_EMPTY);
//						debug_pr( $match );
//						foreach($match as $aMatch){
//							//handle the content of the array
//							$x .= "<th style='background-color:white;color:rgb(0, 0, 0);'>".$aMatch."</th>";
//						}
						//Henry added [end]
					}
					else{
						$x .= "<th style='background-color:white;color:rgb(0, 0, 0);font-weight:normal'>".$items["{$sortType}"]."</th>";
					}
					
					if(isset($items['MaleTotal'])){
                        $chartItemMaleQtyAry[] = $items['MaleTotal'];
                        $chartItemFemaleQtyAry[] = $items['FemaleTotal'];
						$x .= "<td align='center'>".$items['MaleTotal']."</td>";
						$x .= "<td align='center'>".$items['FemaleTotal']."</td>";
						$total_male_count += $items['MaleTotal'];
						$total_female_count += $items['FemaleTotal'];
					}
					
					$x .= "<td align='center'>".$items['Total']."</td>";
					$x .= "</tr>";
					$total_count += $items['Total'];
				}
			}
			$x .= "<table class='common_table_list_v30 view_table_list_v30' width='100%'><tr>";
				$x .= "<td align='right' width='25%'>".$Lang["libms"]["report"]["total"]."</td>";
				if(isset($items['MaleTotal'])){
					$x .= "<td align='center' width='25%'>".$total_male_count."</td>";
					$x .= "<td align='center' width='25%'>".$total_female_count."</td>";
				}
				$x .= "<td align='center' width='25%'>".$total_count."</td>";
				$x .= "</tr></table>";
			$x .= "</table></div><br/>";
				### build display table
        }     // not print chart, it's table

        $x .= "<div><script>";
            $x .= "drawChart('result_chart', {";
                $x .= "name: ['".implode("','",$chartItemNameAry)."'],";
                $x .= "array: [".implode(",",$chartItemQtyAry)."],";
                $x .= "maleArray: [".implode(",",$chartItemMaleQtyAry)."],";
                $x .= "femaleArray: [".implode(",",$chartItemFemaleQtyAry)."],";
                $x .= "title: '".$Lang["libms"]["reporting"]["PageReportingLending"]."',";
                $x .= "color: ['#64E572', '#7cb5ec', '#FF9655']";
            $x .= "},".($showGenderQty ? "'true'" : "'false'").");";
        $x .= "</script></div>";

	 return $x;
	}
	/**
	 * get Print view
	 * @param array $viewdata
	 * @return string
	 */
	public function getPrint($viewdata){
		global $Lang,$LAYOUT_SKIN, $linterface;
		ob_start();
		include_once("{$_SERVER['DOCUMENT_ROOT']}/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
		?>
		
		<link href="css/visualize.css" type="text/css" rel="stylesheet" />
		<link href="css/visualize-light.css" type="text/css" rel="stylesheet" />
		<link href="css/reports.css" type="text/css" rel="stylesheet" />
		<script src="/templates/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/number_rounding.js"></script>
		<script type="text/javascript" src="js/visualize.jQuery.js"></script>
        <?php echo $linterface->Include_HighChart_JS();?>
        <script type="text/javascript" >
<?php echo $this->getDrawHighChart();?>
            $(function(){
				if ('<?=$_GET['show']?>' == 'chart'){
					// $('.visual_chart').each(function (){
					// 	$div = $('<div style="page-break-inside: avoid;" />')
					// 	barchart_colors = ['#be1e2d','#666699','#92d5ea','#ee8310','#8d10ee','#5a3b16','#26a4ed','#f45a90','#e9e744','#ce1e2d','#766699','#a2d5ea','#fe8310','#9d10ee','#6a3b16','#36a4ed','#045a90','#f9e744'];
					// 	$(this).visualize({type: 'bar', width: '660px', height : '200px', colors : barchart_colors}).appendTo($div);
					// 	$div.appendTo($('#result_chart'));
					// });
					window.scrollTo(0, 0);
//					$('.result_toggler').click(function(){
//						$('.result_toggler').toggleClass('thumb_list_tab_on');
//						$('#result_stat').toggle();
//						$('#result_chart').toggle();
//						return false;
//					});
//	
// 					$('#result_chart').toggle();
//
// 					$('.result_toggler').toggleClass('thumb_list_tab_on');
// 					$('#result_stat').toggle();
// 					$('#result_chart').toggle();
				}
					
			});
		</script>
			<div id='toolbar' class= 'print_hide' style='text-align: right; width: 100%'>
					<? 
						$linterface = new interface_html();
						echo $linterface->GET_SMALL_BTN($Lang["libms"]["report"]["toolbar"]['Btn']['Print'], "button", "javascript:window.print();","print");
						
					?>
			</div>
					
			<?=$this->getResult($viewdata);?>
			
			</body>
			</html>
			<?
			
			$x = ob_get_contents();
			ob_end_clean();
			return $x;
		}
		
		/**
		 * get CSV string
		 * @param array $viewdata
		 * @return string
		 */
		public function getCSV($viewdata){
			global $Lang;
			$sortType = $viewdata["post"]["sortBy"]["type"];
			if($sortType == "Student"){
				$headers[] = $Lang["libms"]["report"]["ClassName"];
				$headers[] = $Lang["libms"]["report"]["ClassNumber"];
			}
			
			$headers[] = $Lang["libms"]["report"]["{$sortType}"];
			
			foreach($viewdata['Result'] as $items)
			{
				if(isset($items['MaleTotal'])){
					$headers[] = $Lang["libms"]["report"]["LendingTotal"]." (".$Lang["libms"]["report"]["Male"].")";
					$headers[] = $Lang["libms"]["report"]["LendingTotal"]." (".$Lang["libms"]["report"]["Female"].")";
					break;
				}
			}
			
			$headers[] = $Lang["libms"]["report"]["LendingTotal"];
			
			$formatted_ary[] = $headers;
			
			$total_no = 0;
			$total_male_count = 0;
			$total_female_count = 0;
			
			foreach($viewdata['Result'] as $row){
				if($sortType == "Student"){
					$formatted_ary[] = array(
					
						$row["ClassName"],
						$row["ClassNumber"],
						$row["onlyStudentName"].($row['RecordStatus'] == '3' ? '*' : ''),
						$row["Total"]
					);
				}
				else{
					
					if(isset($row['MaleTotal'])){
						$formatted_ary[] = array(
							$row["{$sortType}"],
							$row['MaleTotal'],
							$row['FemaleTotal'],
							$row["Total"]
						);
						$total_male_count += $row['MaleTotal'];
						$total_female_count += $row['FemaleTotal'];
					}
					else
						$formatted_ary[] = array(
							$row["{$sortType}"],
							$row["Total"]
						);
				}
				$total_no += $row["Total"];				
			}
			if($sortType == "Student")
				$formatted_ary[] = array ('','', $Lang["libms"]["report"]["total"], $total_no);
			else{
				if(isset($row['MaleTotal'])){
					$formatted_ary[] = array ($Lang["libms"]["report"]["total"], $total_male_count, $total_female_count, $total_no);
				}
				else
					$formatted_ary[] = array ($Lang["libms"]["report"]["total"], $total_no);
			}
			//array_unshift($formatted_ary,$headers);
			$csv = Exporter::array_to_CSV($formatted_ary);
			//dump($viewdata['Result']);
			return $csv;
			
		}
			
}
