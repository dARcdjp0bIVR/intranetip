<?php
/* 
 * Editing by  
 * 
 * Note: page called for using jquery.autocomplete.js only 
 * 
 * @Param	q : search string
 *  
 * 	2016-01-14 [Cameron]
 * 		copy from home/library_sys/admin/book/ajax_book_suggestion.php and modify
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
########################################################################################################


## Get Data
$q = (isset($q) && $q != '') ? $q : '';


## Init
$x = '';


## Use Library
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']  )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


## Maing
if($q != ''){
	# search with input keyword
	$sub_sql = ($q != '') ? " where TagName like '%".$q."%' " : "";
	$sql = "select TagName from LIBMS_TAG ".$sub_sql."  order by TagName limit 10";
	$result = $libms->returnArray($sql);
	
	if(!empty($result)){
		foreach ($result as $row){
			$x .= $row[0]."|".$row[0]."\n";
		}
	}
}

echo $x;