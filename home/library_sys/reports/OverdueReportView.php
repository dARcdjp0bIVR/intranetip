<?php

# using: 

/**
 * Log :
 * 
 * Date:	2019-07-23 [Henry]
 * 			- use the local library of jquery
 * Date:    2018-06-27 [Cameron]
 *          - add option 'PenaltyDate' in function getForm_PenaltyDate() and getResult() [case #P137905]
 *          - show penalty date in _do_overdue_email_get_mail_body() and getResult() 
 * Date:	2017-11-29 [Cameron]
 * 			- add option to show list price [case #D128720]
 * 			- add function getForm_ListPrice() 
 * 			- show list price in _do_overdue_email_get_mail_body(), getCSV(), getResult()
 * Date:	2017-06-23 [Henry]
 * 			- limit the end to yesterday [Case#K118612]
 * Date:	2016-12-29 [Henry]
 * 			- show purchase price [Case#Z110550]
 * Date:	2016-07-15 [Cameron]
 * 			- hide the warning and empty option list if it returns nothing in ajaxSelectbox (IP2.5.7.7.1)
 * 			- don't allow to run report if user do not select target
 * Date:	2016-05-30 [Cameron] 
 * 				- fix bug: don't show penalty info in email body and push message if not check "Show penalty" option in filter page
 * 				modify _do_overdue_email_get_mail_body() and pushMessageToParent() [case #E96520]
 * 				- apply nl2br and rawurldecode to CurrOverdueRemark in _do_overdue_email_get_mail_body()
 * 				- apply encodeURIComponent to CurrOverdueRemark field in getResult() so that line break can be kept
 * Date:	2016-03-07 [Kenneth] added pushMessageToParent()
 * 								edit getConfirmation() - include results of push message
 * 								edit getResult() - include push message button
 * Date:	2015-10-12 [Henry] added flag $sys_custom['eLibraryPlus']['OverdueNoticeLargerFontSize'] to enlarge the font size 
 * Date:	2015-10-07 [Henry] change wordings and modified getForm_Period() to change to AcademicYear start date [Case#L86512] (ip.2.5.6.10.1)
 * Date:	2015-06-11 [Henry] add printPageBreak option
 * Date:	2015-02-23 [Cameron] show overdue remark in _do_overdue_email_get_mail_body()
 * Date:	2015-02-16 [Cameron] 
 * 				1. Add button $Lang["libms"]["report"]["toolbar"]['Btn']['AddRemark'], 
 * 				2. show thickbox to input remark for sending email when it's clicked, 
 * 				3. add corresponding functions click_add_remark() and onloadThickBox()
 * 				4. Include_Thickbox_JS_CSS in getResult()
 * 				
 * Date:	2014-12-11 [Cameron] Fix bug of export function in getCSV() when there's no record
 * Date:	2014-11-24 [Cameron] Fix bug of export action, then send email without exit the same page (pass form acton = 'email')
 * Date:	2014-11-21 [Cameron] Add $Lang["libms"]["general"]["not_need_to_reply_email"] to _do_overdue_email_get_mail_body 
 * Date:	2014-11-10 [Cameron] Add function getCSV(), for case 2014-1015-0959-48071
 * Date:	2014-10-17 [Henry] Modified function do_overdue_email # ip.2.5.5.10.1
 * Date:	2014-10-06 [Henry] Modified function _do_overdue_email_get_mail_body() # ip.2.5.5.10.1
 * Date:	2013-06-17 [Cameron] Fix bug: don't allow to select none in group target
 * 					Add date range logic checking ( from date <= to date)
 * Date 	2013-05-07 [Yuen]
 * 			Improved the layout
 * Date: 	2013-04-25 [Cameron]
 * 			Change parameter name to "data[findBy]" in $this->linterface->Get_Radio_Button so that "Target" filter function works   
 * Date:	2013-04-23 [Cameron]
 * 			Add date validation function before submit criteria for report: checkForm2() 			
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../";
$MODULE_ROOT = "../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

##SEND MAIL
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libsendmail.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

##SEND PUSH MESSAGE
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");


include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

require_once('ReportView.php');

global $Lang;


class OverdueReportView extends ReportView{
	
	public function __construct(){
		$Current_Page = "PageReportingOverdue";
		parent::__construct($Current_Page);
	}
	
	public function getView($viewdata = null){
		global $Lang;
		
		if (empty($_POST)){
			$x = $this->getHeader();
			$x .= $this->getJS($viewdata);
			$x .= $this->getFrom($viewdata);
			//$x .= $this->getResult($viewdata);
			$x .= $this->getFooter();
		}else{
			$x = $this->getResult($viewdata);
		}			
		return $x;
	}
	
	public function getJS($viewdata){
	
		global $Lang;
		ob_start();
		?>
		<script type="text/css">
		 .overdue_result {
			width: 700px;
		}
		</script>
		<script src="/templates/jquery/jquery-1.8.0.min.js" type="text/javascript"></script>
		<script type="text/javascript">
		
		function ajaxSelectbox(page, fieldName){
			$.get(page, {},
				function(insertRespond){
					if (insertRespond == ''){
//						alert('Ajax call return nothing. Please try again. If problem persist, make sure the setting is completed / contact your administrator');
						fieldName.html('');
					}else {
						fieldName.html(insertRespond);
					}
				}
			);
		}
		
		function student_list(){
			if ($('#rankTarget').val() == 'student')
				{
				page = "?page=Ajax&function=getStudentByClass&classNames="+$('#classnames :selected').text();
				ajaxSelectbox(page, $('#studentIDs'));
				$('#studentIDs option').attr('selected', true);
				}
		}
		
		
		function checkForm2() {
			
			if (($('#findByGroup').attr('checked')) && (!$("#groupTarget option:selected").length )) {
				alert("<?=$Lang['libms']['report']['msg']['please_select_group']?>");
				return;					
			}
			if (($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'form') && (!$("#classnames option:selected").length )) {
				alert("<?=$Lang['libms']['report']['msg']['please_select_form']?>");
				return;					
			}										
			if (($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'class') && (!$("#classnames option:selected").length )) {
				alert("<?=$Lang['libms']['report']['msg']['please_select_class']?>");
				return;					
			}										
			if (($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'student') && (!$("#studentIDs option:selected").length )) {
				alert("<?=$Lang['libms']['report']['msg']['please_select_student']?>");
				return;					
			}

			if (!check_date(document.getElementById("PenaltyDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
				return;
			if (!check_date(document.getElementById("DueDateFrom"),"<?=$Lang['General']['InvalidDateFormat']?>"))
				return;
			if (!check_date(document.getElementById("DueDateBefore"),"<?=$Lang['General']['InvalidDateFormat']?>"))
				return;
// 			if (document.getElementById("DueDateBefore").value >= getToday()){
//				alert("<?php //echo $Lang['libms']['report']['msg']['wrong_due_date_range'];?>");
// 				return;
// 			}
			if (document.getElementById("DueDateBefore").value >= document.getElementById("PenaltyDate").value){
				alert("<?php echo $Lang['libms']['report']['msg']['dueDateShouldBeEarlierThanPenaltyDate'];?>");
				return;
			}

			if (!compare_date(document.getElementById("DueDateFrom"), document.getElementById("DueDateBefore"),"<?=$Lang['General']['WrongDateLogic']?>"))
				return;	
			
			document.form1.submit();
		}

		
		//--------------------------------->
		$().ready( function(){

			$.ajaxSetup({ cache:false,
					   async:false });
			
			//---------------------------------------------------------------------------------------------------------------------- TargetUserSelection  Start
			ajaxSelectbox('?page=Ajax&function=getGroupName',$('#groupTarget'));
			ajaxSelectbox('?page=Ajax&function=getClassLevel', $('#classnames'));
			
			$('#findByGroup').change(function(){
				$('.table_findBy').toggle();
			});
			
			$('#findByStudent').change(function(){
				$('.table_findBy').toggle();
			});
					
			$('#rankTarget').change(function(){
				switch($('#rankTarget').val())
				{
				case 'form': 
					page = "?page=Ajax&function=getClassLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',true);
					$('#classnames option').attr('selected', true);
					$('#selectAllBtnClassnames').attr('style','display:inline');
					$('#Student').attr('style','display:none');	
					break;
				case 'class': 
					page= "?page=Ajax&function=getClassNameByLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',true);
					$('#classnames option').attr('selected', true);
					$('#selectAllBtnClassnames').attr('style','display:inline');
					$('#Student').attr('style','display:none');	
					break;
				case 'student': 
					page= "?page=Ajax&function=getClassNameByLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',false);
					$("#classname option:first").attr('selected','selected');
					student_list();
					$('#selectAllBtnClassnames').attr('style','display:none');
					$('#Student').attr('style','display:inline');	
					break;
				};
			});
			
			$('#classnames').change(function(){
				student_list();
			});
			
			//----------------------------------------------------------------------------------------------------------------------  TargetUserSelection  End
			<?php if(is_null($viewdata['Result'])){ ?>
			$('#classnames option').attr('selected', true);
			$('#groupTarget option').attr('selected', true);
			<?php }?>
			
			$('#selectAllBtnClassnames').click(function(){ 			$('#classnames option').attr('selected', true);  		});
			$('#selectAllBtnStudent').click(function(){ 			$('#studentIDs option').attr('selected', true);  		});
			$('#selectAllBtnGroups').click(function(){ 			$('#groupTarget option').attr('selected', true);  		});

		});

		</script>
		
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getFrom($viewdata){
		global $Lang;
		ob_start();
		?>
			<form name="form1" method="post" target='_blank' action="?page=Overdue" id="formContent">
				
				<table class="form_table_v30">
			<? 
					
					echo $this->getForm_TargetUserSelection($viewdata);
					echo $this->getForm_PenaltyDate($viewdata);
					echo $this->getForm_Period($viewdata);
					echo $this->getForm_Penalty($viewdata);
					echo $this->getForm_PurchasePrice($viewdata);
					echo $this->getForm_ListPrice($viewdata);
					echo $this->getForm_PrintPageBreak($viewdata);
					//echo $this->getForm_Display($viewdata);
			?>
				</table>
				
				<?=$this->linterface->MandatoryField();?>
				
				<div class="edit_bottom_v30">
					<p class="spacer"></p>					
						<input type='button' value='<?=$Lang['libms']['report']['Submit']?>' onclick='checkForm2();' />
					<p class="spacer"></p>
				</div>
			</form>
			<?
			$x = ob_get_contents();
			ob_end_clean();
			return $x;
	}
	
	public function getForm_PenaltyDate($viewdata){
	    global $Lang;
	    
	    $penaltyDate = $viewdata['penaltyDate'];
	    if ($penaltyDate =="") {
	        $penaltyDate = date('Y-m-d'); // default today
	    }
	    ob_start();
	    ?>
			<!-- Period -->
			<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['penaltyDate']?></td>
				<td >
					<?=$this->linterface->GET_DATE_PICKER("PenaltyDate",$penaltyDate,"")?>
					 <span id='div_PenaltyDate_err_msg'></span>
				</td>
			</tr>
		<?	
		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}
	
	public function getForm_Period($viewdata){
		global $Lang;
	//todo
		$selectSchoolYearHTML =$viewdata['selectSchoolYearHTML'];
		$Period =$viewdata['Period'];
		$textFromDate = $viewdata['textFromDate'];
		$textToDate = $viewdata['textToDate'];
		
		if ($textFromDate=="" && $textToDate=="")
		{
			# preset date range as current school year
			$current_period= getPeriodOfAcademicYear(Get_Current_Academic_Year_ID());

			if (is_array($current_period) && sizeof($current_period)>0)
			{
				foreach($current_period as &$date){
					$date = date('Y-m-d',strtotime($date));
				}
				$textFromDate = $current_period['StartDate'];
				//$textToDate = $current_period['EndDate'];
			} else
			{
				$textFromDate = (date('m')>=9) ? date('Y-09-01') : (date('Y')-1).'-09-01';
				//$textToDate = date('Y-m-d');
			}
			$textToDate = date('Y-m-d', strtotime('-1 days'));
		}
		
		ob_start();
		?>
			<!-- Period -->
			<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['due_time']?></td>
				<td >
					<?=$this->linterface->GET_DATE_PICKER("DueDateFrom",$textFromDate,"")?>
					 <span id='div_DueDateFrom_err_msg'></span>
					 <?=$Lang['libms']['report']['period_date_to']?>
					<?=$this->linterface->GET_DATE_PICKER("DueDateBefore",$textToDate,"")?>
					 <span id='div_DueDateBefore_err_msg'></span>
				</td>
			</tr>
		<?	
		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}
	
	public function getForm_PrintPageBreak($viewdata){
		global $Lang;
		
		ob_start();
		?>
			<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['PageBreakForEachUser']?></td>
				<td>
					<?=$this->linterface->Get_Radio_Button("printPageBreak1", "data[printPageBreak]", '1', false , "", $Lang['libms']["settings"]['system']['yes'])?>
					<?=$this->linterface->Get_Radio_Button("printPageBreak2", "data[printPageBreak]", '0', true, "", $Lang['libms']["settings"]['system']['no'])?>
				</td>
			</tr>
		<?	
		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}
	
	public function getForm_TargetUserSelection($viewdata){
		global $Lang;
		//todo
	
		$selectSchoolYearHTML =$viewdata['selectSchoolYearHTML'];
		$Period =$viewdata['Period'];
		$textFromDate = $viewdata['textFromDate'];
		$textToDate = $viewdata['textToDate'];
		
		
		ob_start();
		?>
		<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['target']?></td>
			<td>
				<table class="inside_form_table">
					<tr>
						<td valign="top">
							<?//=$this->linterface->Get_Radio_Button("findByGroup", "data[findBy][type]", 'Group', true, "", $Lang['libms']['report']['findByGroup'])?>
							<?//=$this->linterface->Get_Radio_Button("findByStudent", "data[findBy][type]", 'Student', false, "", $Lang['libms']['report']['findByStudent'])?>
							<?=$this->linterface->Get_Radio_Button("findByGroup", "data[findBy]", 'Group', true , "", $Lang['libms']['report']['findByGroup'])?>
							<?=$this->linterface->Get_Radio_Button("findByStudent", "data[findBy]", 'Student', false, "", $Lang['libms']['report']['findByStudent'])?>
							
						</td>
					</tr>
					<tr class="table_findBy">
						<td valign="top">
							<select name="data[groupTarget][]" id="groupTarget" multiple size="7">
							<!-- Get_Select  -->
							</select>
							<?=$this->linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnGroups');?>
						</td>
					</tr>
					<tr class="table_findBy" style='display:none;'>
						<td valign="top">
							<select name="data[rankTarget]" id="rankTarget">
								<option value="form" selected><?=$Lang['libms']['report']['Form']?></option>
								<option value="class"><?=$Lang['libms']['report']['Class']?></option>
								<option value="student"><?=$Lang['libms']['report']['Student']?></option>
							</select>
						</td>
						<td valign="top" nowrap>
							<!-- Form / Class //-->
							<span id='classname'>
							<select name="data[classname][]" id="classnames" size="7" multiple></select>
							</span>
							<?=$this->linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnClassnames');?>
							<!-- Student //-->
							<span id='Student' style='display:none;'>
								<select name="data[studentID][]" multiple size="7" id="studentIDs"></select> 
								<?= $this->linterface->GET_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnStudent')?>
							</span>
							
						</td>
					</tr>
					<tr><td colspan="3" class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></td></tr>
				</table>
				<span id='div_Target_err_msg'></span>
			</td>
		</tr>
			
					
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_Penalty($viewdata){
		global $Lang;
		ob_start();
		?>
			<tr>
				<td class="field_title"><?=$Lang["libms"]["report"]["show_penalty"]?></td>
				<td>
					<?=$this ->linterface->Get_CheckBox("ShowPenalty", "ShowPenalty", '1', true, "", "")?>	
				</td>
			</tr>
			<?
			$x = ob_get_contents();
			ob_end_clean();
			return $x;
	}
	
	public function getForm_PurchasePrice($viewdata){
		global $Lang, $plugin;
		ob_start();
		?>
			<tr>
				<td class="field_title">
					<?=$Lang["libms"]["report"]["show_purchase_price"].(($plugin['eClassApp'] || $plugin['eClassTeacherApp'])?'<br/>'.$Lang["libms"]["report"]["excluding_in_push_notification"]:'')?>
				</td>
				<td>
					<?=$this ->linterface->Get_CheckBox("ShowPurchasePrice", "ShowPurchasePrice", '1', false, "", "")?>	
				</td>
			</tr>
			<?
			$x = ob_get_contents();
			ob_end_clean();
			return $x;
	}
	
	public function getForm_ListPrice($viewdata){
		global $Lang, $plugin;
		ob_start();
		?>
			<tr>
				<td class="field_title">
					<?=$Lang["libms"]["report"]["show_list_price"].(($plugin['eClassApp'] || $plugin['eClassTeacherApp'])?'<br/>'.$Lang["libms"]["report"]["excluding_in_push_notification"]:'')?>
				</td>
				<td>
					<?=$this ->linterface->Get_CheckBox("ShowListPrice", "ShowListPrice", '1', false, "", "")?>	
				</td>
			</tr>
			<?
			$x = ob_get_contents();
			ob_end_clean();
			return $x;
	}
	
	public function getForm_Display($viewdata){
		global $Lang;
		ob_start();
		?>
			<tr>
				<td class="field_title"><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SortBy"]?></td>
				<td>
					<?=$this ->linterface->Get_Radio_Button("print", "data[view]", 'print', true, "", $Lang['libms']['report']['print'] )?>
					<?=$this ->linterface->Get_Radio_Button("mail2student", "data[view]", 'mail2student', false, "", $Lang['libms']['report']['mailToStudent'])?>
					<?=$this ->linterface->Get_Radio_Button("mail2parent", "data[view]", 'mail2parent', false, "", $Lang['libms']['report']['mailToParent'])?>
					<?=$this ->linterface->Get_Radio_Button("mail_both", "data[view]", 'mail_both', false, "", $Lang['libms']['report']['mailBoth'])?>
					</td>
			</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getResult($viewdata){
		global $Lang,$LAYOUT_SKIN, $junior_mck, $_POST,$PATH_WRT_ROOT, $intranet_session_language, $sys_custom,$plugin;
		
		$total_Penalty = 0;
		$total_PurchasePrice = 0;
		$total_ListPrice = 0;
		
		$_SESSION['LIBMS']['reporting']['post'] = $viewdata['post'];
//		if ($viewdata['post']['view'] != 'print' ){
// 			$this->do_overdue_email($viewdata);
// 			//confirmation 
// 			$x = $this->getHeader();
// 			$x .= $this->getConfirmation($viewdata);
// 			$x .= $this->getFooter();
			
// 		}else{
			ob_start();			
			include_once("{$_SERVER['DOCUMENT_ROOT']}/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
			
			?>
			<script type="text/javascript" >
				function email(mode){
					 //window.location = "?page=Overdue&action=email&mode=" + mode;
					 $('#CurrOverdueRemark').val(encodeURIComponent($('#CurrOverdueRemark').val()));
					 document.form1.mode.value = mode;
					 document.form1.action.value = 'email';
					 document.form1.submit();
				}
				function click_export() {
					document.form1.action.value = 'export';					
					document.form1.submit();					
				}
				
				function click_add_remark() {
					load_dyn_size_thickbox_ip('<?=$Lang["libms"]["report"]["toolbar"]['Btn']['AddRemark']?>', 'onloadThickBox();', inlineID='', defaultHeight=400, defaultWidth=800);
				}
				
				function onloadThickBox() {
				  	$('div#TB_ajaxContent').load(
				    	"overdue_remark.php",
				    	{
				      		'HistoryRemarkID': $('#CurrHistoryRemarkID').val(),
				      		'OverdueRemark': $('#CurrOverdueRemark').val()
				    	}, 
				    	function(ReturnData) {
				      		adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
				    	}
				  	);
				}
				
				function changePageBreak(userBarcode){
					if($('#page_break_'+userBarcode).attr('checked')){
						$('#user_row_'+(userBarcode+1)).css('page-break-before', "always");
					}
					else{
						$('#user_row_'+(userBarcode+1)).css('page-break-before', "auto");
					}
				}
				function sendPushMessageToParent(){
					document.form1.action.value = 'pushMessageParent';					
					document.form1.submit();
				}
			</script>
<?php
	$linterface = new interface_html();
	echo $linterface->Include_Thickbox_JS_CSS();
?>			
			
			<div class='print_hide' style="text-align: center;" >
			<? 
				echo $linterface->GET_SMALL_BTN($Lang["libms"]["report"]["toolbar"]['Btn']['AddRemark'], "button", "javascript:click_add_remark();","addremark");
				echo $linterface->GET_SMALL_BTN($Lang["libms"]["report"]["toolbar"]['Btn']['Export'], "button", "javascript:click_export();","export");
				echo $linterface->GET_SMALL_BTN($Lang["libms"]["report"]["toolbar"]['Btn']['Print'], "button", "javascript:window.print();","print");
				
				echo $linterface->GET_SMALL_BTN($Lang["libms"]["report"]["toolbar"]['Btn']['emailuser'], "button", "javascript:window.email('User');","emailuser");
				echo $linterface->GET_SMALL_BTN($Lang["libms"]["report"]["toolbar"]['Btn']['emailparent'], "button", "javascript:window.email('ParentOnly');","emailparent");
				echo $linterface->GET_SMALL_BTN($Lang["libms"]["report"]["toolbar"]['Btn']['emailboth'], "button", "javascript:window.email('CCParent');","emailboth");
				if ($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {
					echo $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['Homework']['button'], "button", "javascript:sendPushMessageToParent();","pushMessageParent");
				}
				
			?>
			</div>
			<table class="overdue_result" style="<?=$sys_custom['eLibraryPlus']['OverdueNoticeLargerFontSize']?'font-size:15px;':''?>width: 1000px;">
				<tr>
					<td><?=  ((isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"]) ?></td>
					
				</tr>

			</table>
				
			<? 	$first_div_element = 1;
				$i=0;
				if (!empty($viewdata['Result']))
				foreach ($viewdata['Result'] as $user_row) :
				$i++;
			?>



				<div class='user_row' id='user_row_<?=$i?>' style='page-break-inside: avoid; display:block;<?=($_POST['data']['printPageBreak'] && !$first_div_element?'page-break-before:always':'')?>' >
		
					<table class="overdue_result" style="<?=$sys_custom['eLibraryPlus']['OverdueNoticeLargerFontSize']?'font-size:15px;':''?>width: 1000px;">
					<tr>
					<td colspan='7'>
						<div style="<?=$sys_custom['eLibraryPlus']['OverdueNoticeLargerFontSize']?'font-size:30px;':'font-size: 24px;'?> text-align: center;"> <?=$Lang["libms"]["reporting"]["PageReportingOverdue"]?></div>
						<hr>	
					</td>
					</tr>
					<tr>
						<td width="360px"><b><?=$Lang["libms"]["report"]["username"]?></b> <?=$user_row['Name']?></td>
						<td width="30px">&nbsp;</td>
						<td width="170px"><b><?=$Lang["libms"]["report"]["ClassName"]?></b> <?=$user_row['ClassName']?></td>
						<td width="30px">&nbsp;</td>
						<td width="170px"><b><?=$Lang["libms"]["report"]["ClassNumber"]?></b> <?=$user_row['ClassNumber']?></td>
						<td width="30px">&nbsp;</td>
						<td width="210px"><b><?=$Lang["libms"]["CirculationManagement"]["user_barcode"]?></b> <?=$user_row['BarCode']?></td>
					</tr>
					</table>
					<img src="/images/spacer.gif" width="1" border="0" height="6" />
					<table class="overdue_result" style="<?=$sys_custom['eLibraryPlus']['OverdueNoticeLargerFontSize']?'font-size:15px;':''?>width: 1000px;">
						<tr style="font-weight: 900;">
							<td width="125"><?=$Lang["libms"]["report"]["ACNO"]?></td>
							<td width="570"><?=$Lang["libms"]["report"]["booktitle"]?></td>
							<td width="125"><?=$Lang["libms"]["report"]["due_time"]?></td>
							<td width="130"><?=$Lang["libms"]["report"]["noOfDayOverdue"]?></td>
<?$colspan=4;?>							
<?php if ($_POST["ShowPenalty"]) { 
		$colspan++;?>
							<td width="70"><?=$Lang["libms"]["report"]["pay"]?></td>
<?php } ?>
<?php if ($_POST["ShowPurchasePrice"]) { 
		$colspan++;?>
							<td width="100"><?=$Lang["libms"]["book"]["purchase_price"]?></td>
<?php } ?>
<?php if ($_POST["ShowListPrice"]) { 
		$colspan++;?>
							<td width="100"><?=$Lang["libms"]["book"]["list_price"]?></td>
<?php } ?>
						</tr>
						<? $total_Penalty = 0;?>
						<? $total_PurchasePrice = 0;?>
						<? $total_ListPrice = 0;?>
						<? foreach( $user_row['Overdues'] as $overdue): ?>
						<tr>
							<td><?=$overdue['BookCode']?></td>
							<td><?=$overdue['BookTitle']?></td>
							<td><?=$overdue['DueDate']?></td>
							<td><?=$overdue['CountDay']?></td>
<?php if ($_POST["ShowPenalty"]) { ?>
							<td>$ <?=$overdue['Penalty']?></td>
<?php } ?>
<?php if ($_POST["ShowPurchasePrice"]) { ?>
							<td>$ <?=$overdue['PurchasePrice']?></td>
<?php } ?>
<?php if ($_POST["ShowListPrice"]) { ?>
							<td>$ <?=$overdue['ListPrice']?></td>
<?php } ?>
						</tr>
						<? $total_Penalty += $overdue['Penalty'];?>
						<? $total_PurchasePrice += $overdue['PurchasePrice'];?>
						<? $total_ListPrice += $overdue['ListPrice'];?>
						<? endforeach;// $user_row['Overdues'] as $overdue)   ?>
					
						<tr>
							<td COLSPAN="<?=$colspan?>"><hr></td>
						</tr>
						<tr>
							<td COLSPAN="2">&nbsp;</td>
							<td COLSPAN="2" style="font-weight: 900; text-align: right; padding-right:43px;" ><?=(($_POST["ShowPenalty"] || $_POST["ShowPurchasePrice"] || $_POST["ShowListPrice"]) ? $Lang["libms"]["report"]["total"] : "&nbsp;") ?></td>
							<?= (($_POST["ShowPenalty"]) ? "<td><u>\$ ".$total_Penalty."</u></td>" : "") ?>
							<?= (($_POST["ShowPurchasePrice"]) ? "<td><u>\$ ".$total_PurchasePrice."</u></td>" : "") ?>
							<?= (($_POST["ShowListPrice"]) ? "<td><u>\$ ".$total_ListPrice."</u></td>" : "") ?>
						</tr>

						<tr class="overdueRemark" style="display:none">
							<td COLSPAN="<?=$colspan?>">
								<span><b><?=$Lang["libms"]["report"]["overdue_remark"]["remark"]?></b></span><br>
								<span class="overdueRemarkContent"></span>
							</td>
						</tr>
						
						<tr>
							<td COLSPAN="<?=$colspan?>">&nbsp;</td>
						</tr>

						<tr>
							<td COLSPAN="2"><b><?=$Lang["libms"]["report"]["printdate"]."</b> ".date("Y-m-d");?><?php if ($_POST['PenaltyDate'] != date("Y-m-d")) echo ' (<span style="font-weight: bold">'.$Lang['libms']['report']['penaltyDate'].'</span> '.$_POST['PenaltyDate'].')';?></td>
							<td COLSPAN="<?=(intval($colspan)-2)?>" align="right"><?=(count($viewdata['Result'])!=$i?'<span class="print_hide">'.$this->linterface->Get_Checkbox("page_break_".$i, "page_break_".$i, 1, $_POST['data']['printPageBreak'], '', $Lang['libms']['report']['NewPageBreak'], 'changePageBreak('.$i.')')."</span>":'&nbsp;')?></td>
						</tr>

					</table>
					
					<img src="/images/spacer.gif" width="1" border="0" height="10" />
					
				</div>
				
			<? $first_div_element=0; endforeach;// ($viewdata['Result'] as $user_row)?>
			
			<form name="form1" method="get">
			
			<input type="hidden" name="page" value="Overdue" />			
			<input type="hidden" name="action" value="email" />			
			<input type="hidden" name="mode" value="" />
			
			<input type="hidden" name="PenaltyDate" value="<?=$_POST['PenaltyDate']?>" />
			<input type="hidden" name="DueDateBefore" value="<?=$_POST['DueDateBefore']?>" />
			<input type="hidden" name="DueDateFrom" value="<?=$_POST['DueDateFrom']?>" />
			<input type="hidden" name="findBy" value="<?=$_POST['data']['findBy']?>" />
			<input type="hidden" name="groupTarget" value="<?=((is_array($_POST['data']['groupTarget']) && sizeof($_POST['data']['groupTarget'])>0) ? implode(",",$_POST['data']['groupTarget']) : $_POST['data']['groupTarget'])?>" />
			<input type="hidden" name="rankTarget" value="<?=$_POST['data']['rankTarget']?>" />
			<input type="hidden" name="classname" value="<?=((is_array($_POST['data']['classname']) && sizeof($_POST['data']['classname'])>0) ? implode(",",$_POST['data']['classname']) : $_POST['data']['classname'])?>" />
			<input type="hidden" name="studentID" value="<?=((is_array($_POST['data']['studentID']) && sizeof($_POST['data']['studentID'])>0) ? implode(",",$_POST['data']['studentID']) : $_POST['data']['studentID'])?>" />
			
			<input type="hidden" name="CurrHistoryRemarkID" id="CurrHistoryRemarkID" value="" />
			<input type="hidden" name="CurrOverdueRemark" id="CurrOverdueRemark" value="" />
			<input type="hidden" name="ShowPenalty" id="ShowPenalty" value="<?=($_POST["ShowPenalty"]?1:0)?>" />
			<input type="hidden" name="ShowPurchasePrice" value="<?=(($_POST["ShowPurchasePrice"])?1:0)?>" />
			<input type="hidden" name="ShowListPrice" value="<?=(($_POST["ShowListPrice"])?1:0)?>" />
			</form>
			
			</body>
			</html>
			<?
			$x = ob_get_contents();
			ob_end_clean();
		return $x;	
	}
	/**
	 * do emailing and return nothing 
	 * @param viewdata array $viewdata
	 */
	public function do_overdue_email($viewdata, $email_to_mode='User'){
		global $Lang;
		//debug_pr($email_to_mode);
		$lu = new libuser();
//		if ($viewdata['post']['view'] != 'mail2student' ){
//			$email_to_mode= 'User';
//		}else if ($viewdata['post']['view'] != 'mail2parent' ){
//			$email_to_mode= 'ParentOnly';
//		}else if  ($viewdata['post']['view'] != 'mail_both' ){
//			$email_to_mode= 'CCParent';
//		}
		
		if(!empty($viewdata['Result'])){
			$lwebmail = new libwebmail();
			foreach ($viewdata['Result'] as $overdue_info){
				//dump($overdue_info);
				if (!empty($overdue_info)){
					$mailBody = $this->_do_overdue_email_get_mail_body($overdue_info);
					$mailSubject = "eLibrary plus - overdue reminder"; //$Lang["libms"]["report"]["overdue_email"]["Subject"];

					$to = array();
					if($email_to_mode=='User' || $email_to_mode=='CCParent'){
						$to[] = $overdue_info['UID'];
					}
					if($email_to_mode=='ParentOnly' || $email_to_mode=='CCParent'){
						$parentArray = $lu->getParent($overdue_info['UID']);
						foreach($parentArray as $aParent){							
							$to[] = $aParent['ParentID'];
						}
					}
					//debug_pr($to);
					do_email($to, $mailSubject, $mailBody);
					//debug($mailBody);die();
// 					$debug_array  = array (
// 										'To' => $to,
// 										'Subject' => $mailSubject,
// 										'Body' => $mailBody
// 	 								);
// 					dump($debug_array);
				}
				
			}
		}
	
	}
	public function pushMessageToParent($viewdata){
		global $forceUseClass, $Lang,$ShowPenalty,$ShowPurchasePrice;
		
		$forceUseClass = true;
		$libeClassApp = new libeClassApp();
		$luser = new libuser();
		
		//Get Student Array
		$counter = 0;
		foreach ($viewdata['Result'] as $overdue_info){
//			debug_pr($viewdata['Result']);
			$studentAry = $overdue_info['UID'];
			$studentsWithParentUsingParentApp = $luser->getStudentWithParentUsingParentApp();
			$studentAry = array_intersect((array)$studentAry, $studentsWithParentUsingParentApp);
			//$individualMessageInfoAry = array();
			//$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo((array)$studentAry), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
			
			//debug_pr($parentStudentAssoAry);

			# send message to parent app
//			$individualMessageInfoAry = array();
			if (!empty($studentAry)) {
				$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($studentAry), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
			} else {
				$parentStudentAssoAry = array();
			}
			if(empty($parentStudentAssoAry)){
				continue;
			}
	
	    	
			
			$isPublic = "N";
			$sendTimeMode = 'now';
			$sendTimeString = '';
			
			
			$messageTitle = $Lang['libms']['reporting']['overdue']['push_message']['title'];
			$messageContent = ($ShowPenalty?$Lang['libms']['reporting']['overdue']['push_message']['content']:$Lang['libms']['reporting']['overdue']['push_message']['content_without_penalty']).'
					';
			
			
			$counter++;
			
//			debug_pr($messageArray);
			$numOfOverdue = count($overdue_info['Overdues']);
			for($i=0;$i<$numOfOverdue;$i++){
				$messageContent.= '
						'.($i+1).'. '.$overdue_info['Overdues'][$i]['BookTitle'];
				if($ShowPenalty){
					$messageContent.= ' ($'.$overdue_info['Overdues'][$i]['Penalty'].')';
				}		
			}
			$messageArray['relatedUserIdAssoAry'] = $parentStudentAssoAry;
			$messageArray['messageTitle'] = $messageTitle;
			$messageArray['messageContent'] = $messageContent;
			$individualMessageInfoAry[] = $messageArray;
//			debug_pr($individualMessageInfoAry);
		}
		
//		debug_pr($messageTitle);
		//$messageTitle = 'eLibPlus - Expired Reminder';
		$messageContent = 'MULTIPLE MESSAGES';
		$notifyMessageId ='';
		if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
			$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $appType='P', $sendTimeMode, $sendTimeString, '', '', 'eLibPlus_expired_reminder', date('Ymd'));
		}
		else {
			$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $appType='P', $sendTimeMode, $sendTimeString, '', '', 'eLibPlus_expired_reminder', date('Ymd'));
		}
		
//		if($notifyMessageId>0){
//			$successStudentIdArr[] = $overdue_info['UID'];
//			debug_pr($overdue_info);
//		}
		
		
		$forceUseClass = false;
		return $counter;

		
	}
	private function _do_overdue_email_get_mail_body($overdue_info){
		global $Lang;
//		ob_start();

				$x ='<div class="user_row" style="page-break-inside: avoid; display:block;" >';
		
					$x .='<table class="overdue_result" style="width: 100%;">';
					$x .='<tr>';
					$x .='<td colspan="7">';
						$x .='<div style="font-size: 24px; text-align: center;"> '.$Lang["libms"]["reporting"]["PageReportingOverdue"].'</div>';
						$x .='<hr>';	
					$x .='</td>';
					$x .='</tr>';
					$x .='<tr>';
						$x .='<td width="360px"><b>'.$Lang["libms"]["report"]["username"].'</b> '.$overdue_info['Name'].'</td>';
						$x .='<td width="30px">&nbsp;</td>';
						$x .='<td width="170px"><b>'.$Lang["libms"]["report"]["ClassName"].'</b> '.$overdue_info['ClassName'].'</td>';
						$x .='<td width="30px">&nbsp;</td>';
						$x .='<td width="170px"><b>'.$Lang["libms"]["report"]["ClassNumber"].'</b> '.$overdue_info['ClassNumber'].'</td>';
						$x .='<td width="30px">&nbsp;</td>';
						$x .='<td width="210px"><b>'.$Lang["libms"]["CirculationManagement"]["user_barcode"].'</b> '.$overdue_info['BarCode'].'</td>';
					$x .='</tr>';
					$x .='</table>';
					$x .='<br />';
					
					$colspan='4';
					$x .='<table class="overdue_result" style="width: 100%;">';
						$x .='<tr style="font-weight: 900;">';
							$x .='<td width="125">'.$Lang["libms"]["report"]["ACNO"].'</td>';
							$x .='<td width="570">'.$Lang["libms"]["report"]["booktitle"].'</td>';
							$x .='<td width="125">'.$Lang["libms"]["report"]["due_time"].'</td>';
							$x .='<td width="130">'.$Lang["libms"]["report"]["noOfDayOverdue"].'</td>';
						if ($_GET["ShowPenalty"]) {
							$x .='<td width="70">'.$Lang["libms"]["report"]["pay"].'</td>';
							$colspan++;
						}	
						if ($_GET["ShowPurchasePrice"]) {
							$x .='<td width="100">'.$Lang["libms"]["book"]["purchase_price"].'</td>';
							$colspan++;
						}	
						if ($_GET["ShowListPrice"]) {
							$x .='<td width="100">'.$Lang["libms"]["book"]["list_price"].'</td>';
							$colspan++;
						}	
						$x .='</tr>';
						$total_Penalty = 0;
						$total_PurchasePrice = 0;
						$total_ListPrice = 0;
						foreach( $overdue_info['Overdues'] as $overdue):
						$x .='<tr>';
							$x .='<td>'.$overdue['BookCode'].'</td>';
							$x .='<td>'.$overdue['BookTitle'].'</td>';
							$x .='<td>'.$overdue['DueDate'].'</td>';
							$x .='<td>'.$overdue['CountDay'].'</td>';
							$x .=($_GET["ShowPenalty"]?'<td>$ '.$overdue['Penalty'].'</td>':'');
							$x .=($_GET["ShowPurchasePrice"]?'<td>$ '.$overdue['PurchasePrice'].'</td>':'');
							$x .=($_GET["ShowListPrice"]?'<td>$ '.$overdue['ListPrice'].'</td>':'');
						$x .='</tr>';
						$total_Penalty += $overdue['Penalty'];
						$total_PurchasePrice += $overdue['PurchasePrice'];
						$total_ListPrice += $overdue['ListPrice'];
						endforeach;// $user_row['Overdues'] as $overdue) 
					
						$x .='<tr>';
							$x .='<td COLSPAN="'.$colspan.'"><hr></td>';
						$x .='</tr>';
//						$x .='<tr>';
//							$x .='<td COLSPAN="2"><b>'.$Lang["libms"]["report"]["printdate"]."</b> ".date("Y-m-d").'</td>';
//							$x .='<td COLSPAN="2" style="font-weight: 900; text-align: right; padding-right:43px;" >'.$Lang["libms"]["report"]["total"].'</td>';
//							$x .='<td><u>$ '.$total_Penalty.'</u></td>';
//						$x .='</tr>';

						$x .='<tr>';
							$x .='<td COLSPAN="2">&nbsp;</td>';
							$x .='<td COLSPAN="2" style="font-weight: 900; text-align: right; padding-right:43px;" >'.(($_GET["ShowPenalty"] || $_GET["ShowPurchasePrice"] || $_GET["ShowListPrice"]) ? $Lang["libms"]["report"]["total"] : "&nbsp;").'</td>';
							$x .=(($_GET["ShowPenalty"]) ? '<td><u style="white-space: nowrap;">$ '.$total_Penalty.'</u></td>' : '');
							$x .=(($_GET["ShowPurchasePrice"]) ? '<td><u style="white-space: nowrap;">$ '.$total_PurchasePrice.'</u></td>' : '');
							$x .=(($_GET["ShowListPrice"]) ? '<td><u style="white-space: nowrap;">$ '.$total_ListPrice.'</u></td>' : '');
						$x .='</tr>';

						if ($_GET["CurrOverdueRemark"]) {
							$x .='<tr>';
								$x .='<td COLSPAN="'.$colspan.'"><span><b>'.$Lang["libms"]["report"]["overdue_remark"]["remark"].'</b></span><br>';
								$x .='<span></span>'.nl2br(htmlentities(rawurldecode($_GET["CurrOverdueRemark"]),ENT_QUOTES,"UTF-8")).'</td>';								
							$x .='</tr>';
						}

						$x .='<tr>';
							$x .='<td COLSPAN="'.$colspan.'">&nbsp;</td>';
						$x .='</tr>';
						
						$x .='<tr>';
							$x .='<td COLSPAN="2"><b>'.$Lang["libms"]["report"]["printdate"].'</b> '.date("Y-m-d");
						if ($_GET['PenaltyDate'] != date("Y-m-d")) {
						    $x .= ' (<span style="font-weight: bold">'.$Lang['libms']['report']['penaltyDate'].'</span> '.$_GET['PenaltyDate'].')';
						}
							$x .= '</td>';
							$x .='<td COLSPAN="'.($colspan-2).'"></td>';
						$x .='</tr>';
						
						$x .='<tr>';
							$x .='<td COLSPAN="'.$colspan.'">&nbsp;</td>';
						$x .='</tr>';

						$x .='<tr>';
							$x .='<td COLSPAN="2">'.$Lang["libms"]["general"]["not_need_to_reply_email"].'</td>';
							$x .='<td COLSPAN="'.($colspan-2).'">&nbsp;</td>';
						$x .='</tr>';

					$x .='</table>';
				$x .='</div>';

//		$x = ob_get_contents();
//		ob_end_clean();
		return $x;
	}
	
	public function getConfirmation($viewdata,$pushNotification = 0,$counter=''){
		global $Lang,$LAYOUT_SKIN;
		ob_start();
		include_once("{$_SERVER['DOCUMENT_ROOT']}/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
		
		if($pushNotification==1){
		?>
		
			<div style='margin: auto; width: 200px; text-align: center;'>
			<?= $counter ?> Push Message(s) is Sent.<br/>
			<br/>
			</div>
		
		<?	
		}else{	
		?>
			<div style='margin: auto; width: 200px; text-align: center;'>
			Email Sent.<br/>
			<br/>
			</div>
		<?	
		}
		$linterface = new interface_html();
		//echo $linterface->GET_SMALL_BTN($Lang["libms"]["report"]["toolbar"]['Btn']['Close'], "button", "javascript:window.close();","closewin");
		//echo "</div>";
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	
	
	/**
	 * get CSV string
	 * @param array $viewdata
	 * @return string
	 */
	public function getCSV($viewdata){
		global $Lang,$intranet_session_language;
		
		$headers[] = $Lang["libms"]["report"]["ClassName"];
		$headers[] = $Lang['libms']['report']['ClassNumber'];
		$headers[] = $Lang["libms"]["report"]["username"];
		$headers[] = $Lang["libms"]["CirculationManagement"]["user_barcode"];
		$headers[] = $Lang['libms']['report']['ACNO'];
		$headers[] = $Lang['libms']['report']['booktitle'];
		$headers[] = $Lang['libms']['report']['due_time'];
		$headers[] = $Lang["libms"]["report"]["noOfDayOverdue"];
		
		if ($viewdata['ShowPenalty']) {
			$headers[] = $Lang['libms']['report']['pay'];
		}
		
		if ($viewdata['ShowPurchasePrice']) {
			$headers[] = $Lang["libms"]["book"]["purchase_price"];
		}
		if ($viewdata['ShowListPrice']) {
			$headers[] = $Lang["libms"]["book"]["list_price"];
		}
		
		$formatted_ary[] = $headers;
		if (count($viewdata['Result']) > 0) {
			foreach($viewdata['Result'] as $row){
				if (count($row['Overdues']) > 0) {
					foreach ($row['Overdues'] as $overdue) {
						$exportColumn = array(	
								$row['ClassName'],
								$row['ClassNumber'],
								$row['Name'],					
								$row['BarCode'],						
								$overdue['BookCode'],
								$overdue['BookTitle'],
								$overdue['DueDate'],
								$overdue['CountDay']);
								
						if ($viewdata['ShowPenalty']) {
							$exportColumn[] = $overdue['Penalty'];
						}
						if ($viewdata['ShowPurchasePrice']) {
							$exportColumn[] = $overdue['PurchasePrice'];
						}
						if ($viewdata['ShowListPrice']) {
							$exportColumn[] = $overdue['ListPrice'];
						}			
						$formatted_ary[] = $exportColumn;
					}
				}
			}
		}
		
		//array_unshift($formatted_ary,$headers);
		$csv = Exporter::array_to_CSV($formatted_ary);
		//dump($viewdata['Result']);
		return $csv;
		
	}

}

?>