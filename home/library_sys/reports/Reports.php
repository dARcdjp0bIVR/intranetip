<?php

//Modifying by 

/**
 * Log :
 *
 * Date:    2019-07-17 [Henry] 
 *                  - modified function getLendingReport() and getCirculationReport() to fix the problem of select all subject
 *
 * Date:    2019-06-21 [Henry] 
 *                  - modified function getLendingReport() to fix the problem of select all subject [Case#P163023]
 *
 * Date:    2019-03-26 [Cameron] 
 *                  - retrieve CallNum  & CallNum2 in getCirculationReport() [case #K157168]
 *                  - fix: retrieve unique BorrowLogID from LIBMS_OVERDUE_LOG in getCirculationReport(), getReaderRankingReport(), getLendingReport(), getBookReportReport()
 *                  
 * Date:    2019-03-14 [Cameron]
 *                  - fix getPenaltyReport(): should include 'OUTSTANDING' RecordStatus when counting paid amount (SETTLED) because there could be partial payment [ej.5.0.9.3.1 deploy test]
 *                  
 * Date:    2019-03-04 [Cameron] 
 *                  - add filter condition LIBMS_OVERDUE_LOG.RecordStatus<>'DELETE' to getCirculationReport(), getBookReportReport(),
 *                  getReaderRankingReport(), getLendingReport() [case #X157459]
 *                   
 * Date:    2018-12-13 [Cameron]
 *                  - add filter condition LIBMS_OVERDUE_LOG.RecordStatus<>'DELETE' to getPenaltyReport() [case #L154330]
 *                  
 * Date:    2018-09-28 [Cameron]
 *                  - add book tags filter to getCirculationReport() [case #X138957]
 *                  
 * Date:    2018-06-27 [Cameron]
 *                  - modify getOverdueReport() to use PenaltyDate instead of current time stamp for counting penalty [case #P137905]
 *                  
 * Date:	2018-02-05 [Henry]
 * 					- modified getLendingReport() and getCirculationReport()
 * 					- bug fix for cannot show the renew book records [Case#Z135161]
 * 	 
 * Date:	2017-11-29 [Cameron]
 * 					- retrieve ListPrice in getOverdueReport() [case #D128720]
 * 
 * Date:	2017-11-21 [Henry]
 * 					-  modified sorting order at function getOverdueReport() [Case#D131507]
 * 
 * Date:	2017-10-03 [Cameron]
 * 					- add option of 'IncludeLeftStudent'(RecordStatus=3) in getLendingReport() [case #A126787]
 * 					- retrieve RecordStatus in getLendingReport()
 *  
 * Date:	2017-08-31 [Cameron]
 *					- add ShowLastModifiedBy in getCirculationReport() [case #C118057]
 *
 * Date:	2017-06-26 [Cameron]
 * 					- fix bug: don't show deleted student record (RecordStatus=0) in getLendingReport(), getCirculationReport(),
 * 						getOverdueReport(), getReaderRankingReport(), getBookRankingReport(), getCategoryReport(), getPenaltyReport(),
 * 						getGroupMemberArray() [case #P116646]
 * 					future improvement should retrieve record from INTRANET_ARCHIVE_USER for deleted user
 * 
 * Date:	2017-04-07 [Cameron]
 * 					- modify getBookReportReport() to support search special characters in book title like '"&<>\/
 *
 * Date:	2017-03-07 [Cameron]
 * 					- fix bug: modify sql in getLanguageArray() so that it can include record which Language is null [case #Z113903]
 * 
 * Date:	2017-02-09 [Cameron]
 * 					- break sql into several parts to avoid exceeding max length (4096 bytes?) when using sprintf in getCategoryReport()
 * 
 * Date:	2017-01-11 [Henry]
 * 					- modified getLendingReport() to count the lending records of deleted user [Case#K111471]
 * 
 * Date:	2016-12-29 [Henry]
 * 					- show purchase price [Case#Z110550]
 * 
 * Date:	2016-09-06 [Cameron]
 * 					- fix bug: Book integrated report (Summary) > Book category > select all should lookup category type 
 * 			by language in getIntegratedCategoriesReport() [case #K102034] 
 *
 * Date:	2016-09-01 [Cameron]
 * 					- fix bug: use Book Item's CirculationTypeCode first, then Bibliography's for following reports:
 * 			getCirculationReport(), getReaderRankingReport(), getBookRankingReport(), getIntegratedCirCategoryReport(),
 * 			getLendingReport() [case #K102034]
 *
 * Date:	2016-07-04 [Cameron]
 * 					- modify getIntegratedBookCountReport(), exclude 'DELETE' record for 'New' books [case #Z97800]
 * 					- modify getLanguageArray() to show Language Description rather than Code [case #Z97773] 
 * 
 * Date:	2016-01-15 [Cameron]
 * 					- add book tags filter to getReaderRankingReport(), getBookRankingReport() and getLendingReport()  
 * 
 * Date:	2016-01-05 [Cameron] 
 * 					- disable output error_reporting to suppress error about strict standards  
 * 
 * Date:	2015-12-09 [Yuen]
 * 					- fixed - failed to get Circulation Type from item; getOverdueReport() is modified 
 * 
 * Date:	2015-11-11 [Cameron]
 * 					- retrieve AccompanyMaterial from LIBMS_BORROW_LOG in getCirculationReport()
 * 
 * Date:	2015-10-22 [Cameron]
 * 					- add filter field groupTargetType and memberID in getCirculationReport()
 * 
 * Date:	2015-10-19 [Cameron]
 * 					- add function getGroupMemberArray()
 * 
 * Date:	2015-07-31 [Cameron]
 * 					- move duplicate fragment to create function getBookCategoryFilter()
 *					- fix bug on filter BookCategoryType 2 in getCategoryReport() and getIntegratedCategoriesReport()
 *
 * Date:	2015-07-21 [Cameron]
 * 					- Modify getIntegratedReport(), getIntegratedCategoriesReport() and getLendingReport() to support AllBookCategory 
 * Date:	2015-07-20 [Cameron]
 * 					- Change value of BookCategoryType from 'A' to 0 for consistency
 * 					- Apply filter for BookCategory only if BookCategoryType != 0 in following functions: getCirculationReport(),
 * 						getReaderRankingReport()
 * 
 * Date:	2015-07-15 [Cameron]
 * 					Fix inconsistent result between listview and export in getBookRankingReport() by adding 2nd Level sorting (by BookTitle)
 *
 * Date:	2015-07-06 [Henry]
 * 					modified getIntegratedOverdueReport() to fix the outstanding total [Case#R80446]
 * Date:	2015-06-30 [Cameron]
 * 					Add checking for BookCategoryType in getBookRankingReport() 
 *  
 * Date:	2015-01-29 [Henry]
 * 					Modified getCategoryReport()
 * Date:	2015-01-26 [Henry]
 * 					Modified getLendingReport() and getIntegratedCategoriesReport()
 * Date:	2015-01-22 [Henry]
 * 					Added parameter "$BookCategoryType" getCirCategoryArray()
 * 					Modified getCirculationReport()
 * 					Modified getBookRankingReport()
 * 					Modified getReaderRankingReport()
 * 					Modified getIntegratedCategoriesReport() for the SQL error
 * Date:	2014-12-03 [Cameron]
 * 					Add alias (b.) for specifying CirculationTypeCode because LIBMS_BOOK and LIBMS_BOOK_UNIQUE both contains this field.
 * 					affect function: getCirculationReport()  
 * 
 * Date:	2014-10-07 [Henry]
 * 					Modified getReaderRankingReport(), getReaderInactiveReport() to filter by gender
 * 					Modified getCategoryReport(), getLendingReport				
 * Date:	2014-05-15 [Henry]
 * 					Modified SQL to improve the performance in getLendingReport()
 * 					Modified getCirculationReport() to support filter by Due Date
 * Date:	2014-01-14 [Henry]
 * 					Modified getReaderInactiveReport() to display student which RecordStatus = 1 only
 * Date:	2013-12-17 [Henry]
 * 					fixed bug on getPenaltyReport()
 * Date:	2013-12-13 [Henry]
 * 					modified getOverdueReport() to separate the student name, class, class number to different columns
 * Date:	2013-08-12 [Yuen] 
 * 					modified getOverdueReport() to get ACNO from BOOK_UNIQUE,
 * Date:	2013-06-17 [Cameron] show ' -- ' when value is null or empty in 
 * 					getBookRankingReport,
 * 					getReaderInactiveReport,
 *   				getReaderRankingReport
 * Date:	2013-06-14 [Cameron] Fix bug: put the joining of  LIBMS_GROUP_USER in sub-query
 * 					because a user can be a member of multiple groups
 * 				getCirculationReport
 * 				getLendingReport 
 * 				getReaderRankingReport
 * 				getBookRankingReport
 * Date:	2013-05-21 [Cameron] 
 * 			Fix bug to handle undefined group in getCategoryReport
 * Date: 	2013-05-06 [Cameron]
 * 			Fix bug: Include 'unclassified' category in grouping for getCategoryReport 
 * Date: 	2013-05-03 [Cameron]
 * 			Fix bug: Correct categories and subject filter in getCategoryReport so that it works 
 * Date: 	2013-04-25 [Cameron]
 * 			Fix bug for getOverdueReport report so that "Target" filter function works   
 * Date:	2013-04-19 [Cameron]
 * 			handle null and empty case for following functions:
 * 				getBookRankingReport($filterArray), 
 * 				getBookReportReport($filterArray)
 * 				getReaderRankingReport($filterArray)
 * 				getOverdueReport($filterArray = null)
 * 				getCategoryReport($filterArray)
 * 				getLendingReport($filterArray)
 * 				getIntegratedReport($filterArray)
 * 				getIntegratedCirCategoryReport($filterArray)
 * 				getIntegratedSubjectReport($filterArray)
 * 				getIntegratedCategoriesReport($filterArray)
 * Date: 	2013-04-18 [Cameron]
 * 			Fix bug for sorting in getCirculationReport($filterArray)
 * Date:	2013-04-17 [Cameron] 
 * 			modified getCirCategoryArray() and getCirculationArray() to include "- unclassified -"
 * 			in the option list
 * 			Also update getCirculationReport($filterArray)
 */

//error_reporting(E_ALL & ~ E_NOTICE);
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../";
$MODULE_ROOT = "../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/User.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/TimeManager.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

global $Lang;

class Reports extends liblms{
	public function __construct(){
		intranet_auth();
		intranet_opendb();
		parent::__construct();
	}
	public function __destruct(){ 
		//intranet_closedb();

	}
	####################
	public function getIntegratedReport($filterArray){

 
		$dateFilterArray = $filterArray['dateRange'];
		//$reportFilterArray = $filterArray['Report'];
			
		if ((isset($filterArray['CirCategory']))&&($filterArray['display']['circulation_type']=='1'))
		{
			$result['CirCategory'] = $this->getIntegratedCirCategoryReport($filterArray);
		}
		else
		{			
			$result['CirCategory'] = array("");
		}
		
		if ((isset($filterArray['Subject']))&&($filterArray['display']['subject']=='1'))
		{
			$result['Subject'] = $this->getIntegratedSubjectReport($filterArray);
		}
		else
		{
			$result['Subject'] = array("");
		}
		
//		if ((isset($filterArray['BookCategory']))&&($filterArray['display']['book_category']=='1'))
		if ($filterArray['display']['book_category']=='1')
		{
			$result['Categories'] = $this->getIntegratedCategoriesReport($filterArray);
		}
		else
		{
			$result['Categories'] = array("");			
		}
		
		if ($filterArray['display']['payment']=='1')
			$result['Overdue'] = $this->getIntegratedOverdueReport($filterArray);
		
		if ($filterArray['display']['bookrecord']=='1')
			$result['BookCount'] = $this->getIntegratedBookCountReport($filterArray);
// debug_r($result);
		return $result;
	}
	####################
	public function getIntegratedCirCategoryReport(&$filterArray=null){
		global $Lang;
		
		$sortBy =  ($filterArray['sortBy']['type'] == "count" ) ? 'count' : 'name';
		$sort_order =  ($filterArray['sortBy']['desc'] == "Desc" ) ? 'DESC' : 'ASC';
		if (!empty($filterArray)){
			//foreach ($filterArray('dateRange') as $condition)
			$condition_clause = "bl.`BorrowTime` >=  '{$filterArray['dateRange']['StartDate']}' AND bl.`BorrowTime` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
		}else{
			$condition_clause ='1=1';
		}
		
		if (!empty($filterArray['CirCategory'])){
			foreach ($filterArray['CirCategory'] as &$CirCategory){
				$CirCategory = mysql_real_escape_string($CirCategory);	
			}
			unset($CirCategory);
			$impoded_CirCategory = implode("','", $filterArray['CirCategory']);

			$condition_clause .= "AND ((bu.CirculationTypeCode<>'' AND bu.CirculationTypeCode IS NOT NULL AND bu.CirculationTypeCode IN ('{$impoded_CirCategory}'))";
			$condition_clause .=   " OR (b.CirculationTypeCode<>'' AND b.CirculationTypeCode IS NOT NULL AND b.CirculationTypeCode IN ('{$impoded_CirCategory}') AND (bu.CirculationTypeCode='' OR bu.CirculationTypeCode IS NULL))";
			if (in_array('',(array)$filterArray['CirCategory'])) {
				$condition_clause .= " OR ((b.CirculationTypeCode='' OR b.CirculationTypeCode IS NULL) AND (bu.CirculationTypeCode='' or bu.CirculationTypeCode IS NULL))";
			}
			$condition_clause .= ") ";

			$join_circulation = "INNER JOIN LIBMS_BOOK_UNIQUE AS bu ON (bu.BookID=b.BookID AND bu.UniqueID=bl.UniqueID)";
			
			$groupByField = "case when (bu.CirculationTypeCode is not null and bu.CirculationTypeCode<>'') then bu.CirculationTypeCode
					when (b.CirculationTypeCode is not null and b.CirculationTypeCode<>'') then b.CirculationTypeCode
					else '' 
				end as FinalCirculationTypeCode";			
		}
		else {
			$join_circulation = '';
			$groupByField = 'b.CirculationTypeCode as FinalCirculationTypeCode';
		}
		
		$sql = "SELECT ".$Lang['libms']['sql_field']['Description']." as CirCategory, CirculationTypeCode FROM LIBMS_CIRCULATION_TYPE ORDER BY CirculationTypeCode";
		
		$cir_type_array =  $this->returnResultSet($sql);
		$cir_type_array = BuildMultiKeyAssoc($cir_type_array,'CirculationTypeCode');

		$sql = <<<EOF
			SELECT
				{$groupByField}, count(*) as count
			FROM `LIBMS_BOOK` b
			JOIN `LIBMS_BORROW_LOG` bl
				on bl.`BookID` = b.`BookID`
			{$join_circulation}				
			WHERE
				{$condition_clause}
			GROUP BY FinalCirculationTypeCode
			ORDER BY `{$sortBy}` $sort_order 
EOF;
//debug($sql);
		$result =  $this->returnArray($sql,null,1);
		foreach((array)$result as $k=>$v) {
			$cir_cat_name = ($v['FinalCirculationTypeCode'] == '') ? $Lang["libms"]["report"]["cannot_define"] : $cir_type_array[$v['FinalCirculationTypeCode']]['CirCategory'];			
			$result[$k]['CirCategory'] = $cir_cat_name;
		}
				//tolog($sql);
				//tolog(mysql_error());		
		return $result;
	}

	####################
	public function getIntegratedSubjectReport($filterArray){
		global $Lang;
		$sortBy =  ($filterArray['sortBy']['type'] == "count" ) ? 'count' : 'Subject';
		$sort_order =  empty($filterArray['sortBy']['desc']) ? 'DESC' : 'ASC';
		
		if (!empty($filterArray)){
			//foreach ($filterArray('dateRange') as $condition)
			$condition_clause = "bl.`BorrowTime` >=  '{$filterArray['dateRange']['StartDate']}' AND bl.`BorrowTime` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
		}else{
			$condition_clause ='1=1';
		}
		
		//Subject filter
		if (!empty($filterArray['Subject'])){
			foreach ($filterArray['Subject'] as &$Subject){
				$Subject = mysql_real_escape_string($Subject);	
			
			}
			unset($Subject);
			$impoded_subjects = implode("','", $filterArray['Subject']);
			if (in_array('', $filterArray['Subject']))
			{
				$condition_clause .= " AND (`Subject` IS NULL OR `Subject`='' OR `Subject` IN ('{$impoded_subjects}'))";
			}else{
				$condition_clause .= " AND `Subject` IN ('{$impoded_subjects}')";
			}
		}
		
//		$condition_clause .= " AND `Subject` IN ('{$impoded_subjects}')";
		
		$sql = <<<EOF
			SELECT
				IF(`Subject` <> '' , b.`Subject` ,'{$Lang["libms"]["report"]["cannot_define"]}') as Subject, count(*) as count
			FROM `LIBMS_BOOK` b
			JOIN `LIBMS_BORROW_LOG` bl
				on bl.`BookID` = b.`BookID`
			WHERE
				{$condition_clause}
			GROUP BY b.`Subject`
			ORDER BY  `{$sortBy}` $sort_order 
			
		
EOF;
		$result =  $this->returnArray($sql,null,1);
		//tolog($sql);
		//tolog(mysql_error());
		return $result;
	}
	####################
	####################
	####################
	public function getIntegratedCategoriesReport($filterArray){
	global $Lang;
		
		$sortBy =  ($filterArray['sortBy']['type'] == "count" ) ? 'count' : 'name';
		$sort_order =  ($filterArray['sortBy']['desc'] == "Desc" ) ? 'DESC' : 'ASC';
		if (!empty($filterArray)){
			//foreach ($filterArray('dateRange') as $condition)
			$condition_clause = "bl.`BorrowTime` >=  '{$filterArray['dateRange']['StartDate']}' AND bl.`BorrowTime` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
		}else{
			$condition_clause ='1=1';
		}
		
		//BookCategory filter
		if (!empty($filterArray['BookCategory']) && !empty($filterArray['BookCategoryType'])){
//		if (!empty($filterArray['BookCategory'])){
			foreach ($filterArray['BookCategory'] as &$category){
				$category = mysql_real_escape_string($category);	
			}
			unset($category);
			$impoded_category = implode("','", $filterArray['BookCategory']);
			$hasNullCatCode=0;
			if (in_array('', $filterArray['BookCategory']))
			{
				$hasNullCatCode=1;
				$condition_clause .= " AND (b.`BookCategoryCode` IS NULL OR b.`BookCategoryCode`='' OR b.`BookCategoryCode` IN ('{$impoded_category}'))";
			}else{
				$condition_clause .= " AND b.`BookCategoryCode` IN ('{$impoded_category}')";
			}
			
			//Henry Added [20150122]
			if(!empty($filterArray['BookCategoryType'])){
				if(!$hasNullCatCode){
					$condition_clause .= " AND b.`BookCategoryCode` IN (SELECT C2.BookCategoryCode FROM LIBMS_BOOK_CATEGORY C2 WHERE C2.BookCategoryType='".$filterArray['BookCategoryType']."') ";
				}else{
					$condition_clause .= " AND (b.`BookCategoryCode` IS NULL OR b.`BookCategoryCode`='' OR b.`BookCategoryCode` IN (SELECT C2.BookCategoryCode FROM LIBMS_BOOK_CATEGORY C2 WHERE C2.BookCategoryType='".$filterArray['BookCategoryType']."')) ";
				}
				$condition_clause .= " AND (bc.BookCategoryType IN (IFNULL((SELECT lbl2.BookCategoryType FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookLanguageCode = b.Language),'".$filterArray['BookCategoryType']."')) OR b.`BookCategoryCode` IS NULL OR b.`BookCategoryCode`='') ";
			}
		}

//		$condition_clause .= " AND b.`BookCategoryCode` IN ('{$impoded_category}')";
		
		if (!empty($filterArray['BookCategoryType'])) {
			$joinCond = "AND bc.`BookCategoryType` = {$filterArray['BookCategoryType']}";
			$extraGroupBy = "";
			$extraField = "";
		}
		else {
			$joinCond = "";
			$extraGroupBy = ", bc.BookCategoryType";
			$extraField = "bc.BookCategoryType, ";			
		}		
		
		$sql = <<<EOF
			SELECT
				bc.`{$Lang['libms']['sql_field']['Description']}` as BookCategory, b.BookCategoryCode, {$extraField} count(*) as count
			FROM `LIBMS_BOOK` b
			JOIN `LIBMS_BORROW_LOG` bl
				on bl.`BookID` = b.`BookID`
			LEFT JOIN (select bc.`DescriptionEn`, bc.`DescriptionChi`, bc.`BookCategoryType`, bc.`BookCategoryCode`, la.`BookLanguageCode` 
					from `LIBMS_BOOK_CATEGORY` bc 
					inner join `LIBMS_BOOK_LANGUAGE` la on la.`BookCategoryType`=bc.`BookCategoryType`) as bc
				on bc.`BookCategoryCode` = b.`BookCategoryCode` 
				and bc.`BookLanguageCode` = b.`Language` {$joinCond}
			WHERE
				{$condition_clause}
			
			GROUP BY b.`BookCategoryCode` {$extraGroupBy}
			ORDER BY `{$sortBy}` $sort_order 
EOF;
		$result =  $this->returnArray($sql,null,1);
		
		if (count($result) > 0) {
			foreach((array)$result as $k=>$v){
				if (empty($v['BookCategory']) && !empty($v['BookCategoryCode'])) {		// has book category but null language or not match language
					$sql = "SELECT `{$Lang['libms']['sql_field']['Description']}` as BookCategory, BookCategoryType FROM LIBMS_BOOK_CATEGORY WHERE BookCategoryCode='".$v['BookCategoryCode']."'";
					$rs =  $this->returnResultSet($sql);
					$nrRec = count($rs);
					if ($nrRec > 0) {
						$result[$k]['BookCategory'] = $rs[0]['BookCategory'];
						if ($nrRec == 1 && $extraField != '') {
							$result[$k]['BookCategoryType'] = $rs[0]['BookCategoryType'];		// don't show BookCategoryType if two category use the same code
						}
					}
				}
			}
		}

				//tolog($sql);

		return $result;
	}
	####################
	public function getIntegratedOverdueReport($filterArray){
		
		if (!empty($filterArray)){
			$condition_clause = "bl.`ReturnedTime` >=  '{$filterArray['dateRange']['StartDate']}' AND bl.`ReturnedTime` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
		}else{
			$condition_clause ='1=1';
		}
		$sql = <<<EOF
			SELECT
				'OVERDUE' as RecordStatus, SUM(`Payment`) as count
			FROM
				`LIBMS_BORROW_LOG` bl
			JOIN `LIBMS_OVERDUE_LOG` ol
				ON bl.`BorrowLogID` = ol.`BorrowLogID`
			WHERE
				{$condition_clause}
				AND
				bl.`ReturnedTime` > bl.`DueDate`
				AND
				ol.`RecordStatus` = 'OUTSTANDING' 
EOF;
		$results[]=  $this->returnArray($sql,null,1);


				
				
		if (!empty($filterArray)){
			$condition_clause = "`DateModified` >=  '{$filterArray['dateRange']['StartDate']}' AND `DateModified` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
		}else{
			$condition_clause ='1=1';
		}
		$sql = <<<EOF
			SELECT
				'PAID' as RecordStatus, SUM(`Amount`) as count
			FROM
				`LIBMS_TRANSACTION` 
			WHERE
				{$condition_clause}

EOF;
		$results[]=  $this->returnArray($sql,null,1);


				
		///////////////////////// merge results ///////////
		$r_result =array();
		foreach ($results as $result){
			$r_result= array_merge($r_result,$result);
		}
		return $r_result;
	}
	####################
	public function getIntegratedBookCountReport($filterArray){
		
		if (!empty($filterArray)){
			$condition_clause = "bu.`CreationDate` >=  '{$filterArray['dateRange']['StartDate']}' AND bu.`CreationDate` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
		}else{
			$condition_clause ='1=1';
		}
		$sql = <<<EOF
			SELECT
				'NEW' as RecordStatus, count(*) as count
			FROM
				`LIBMS_BOOK_UNIQUE` bu
			WHERE
				{$condition_clause}
				AND
				bu.`RecordStatus` <> 'DELETE'
EOF;
		$results[]=  $this->returnArray($sql,null,1);
		
		//tolog($sql);
		//tolog(mysql_error());
		//tolog($results);
		
		if (!empty($filterArray)){
			$condition_clause = "bu.`DateModified` >=  '{$filterArray['dateRange']['StartDate']}' AND bu.`DateModified` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
		}else{
			$condition_clause ='1=1';
		}

		$sql = <<<EOF
			SELECT
				'LOST' as RecordStatus, count(*) as count
			FROM
				`LIBMS_BOOK_UNIQUE` bu
			WHERE
				{$condition_clause}
				AND
				bu.`RecordStatus` = 'LOST'
EOF;
		$results[]=  $this->returnArray($sql,null,1);
				
		//tolog($sql);
		//tolog(mysql_error());

		if (!empty($filterArray)){
			$condition_clause = "bu.`DateModified` >=  '{$filterArray['dateRange']['StartDate']}' AND bu.`DateModified` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
		}else{
			$condition_clause ='1=1';
		}
		$sql = <<<EOF
			SELECT
				'WRITEOFF' as RecordStatus, count(*) as count
			FROM
				`LIBMS_BOOK_UNIQUE` bu
			WHERE
				{$condition_clause}
				AND
				bu.`RecordStatus` = 'WRITEOFF'
EOF;
		$results[]=  $this->returnArray($sql,null,1);
		
		//tolog($sql);
		//tolog(mysql_error());
		
		///////////////////////// merge results ///////////
		$r_result =array();
		foreach ($results as $result){
			$r_result= array_merge($r_result,$result);
		}
		return $r_result;
	}
	####################
	public function getCirculationReport($filterArray){
	
		
		//date filter
		if (!empty($filterArray)){
			if($filterArray['dateType'] == 'DUEDATE')
				$condition_clause = "bl.`DueDate` >=  '{$filterArray['dateRange']['StartDate']}' AND bl.`DueDate` <=  '{$filterArray['dateRange']['EndDate']}' ";
			else if($filterArray['dateType'] == 'RETURNDATE')
				$condition_clause = "bl.`ReturnedTime` >=  '{$filterArray['dateRange']['StartDate']}' AND bl.`ReturnedTime` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
			else
				$condition_clause = "bl.`BorrowTime` >=  '{$filterArray['dateRange']['StartDate']}' AND bl.`BorrowTime` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
		}else{
			$condition_clause ='1=1';
		}
		//CirCategory filter
		if (!empty($filterArray['CirCategory'])){
			foreach ($filterArray['CirCategory'] as &$CirCategory){
				$CirCategory = mysql_real_escape_string($CirCategory);
			}
			unset($CirCategory);
			$impoded_category = implode("','", $filterArray['CirCategory']);
			
			$condition_clause .= "AND ((bu.CirculationTypeCode<>'' AND bu.CirculationTypeCode IS NOT NULL AND bu.CirculationTypeCode IN ('{$impoded_category}'))";
			$condition_clause .=   " OR (b.CirculationTypeCode<>'' AND b.CirculationTypeCode IS NOT NULL AND b.CirculationTypeCode IN ('{$impoded_category}') AND (bu.CirculationTypeCode='' OR bu.CirculationTypeCode IS NULL))";
			if (in_array('',(array)$filterArray['CirCategory'])) {
				$condition_clause .= " OR ((b.CirculationTypeCode='' OR b.CirculationTypeCode IS NULL) AND (bu.CirculationTypeCode='' or bu.CirculationTypeCode IS NULL))";
			}
			$condition_clause .= ") ";
			
		}

		//BookCategory filter		
		$condition_clause .= $this->getBookCategoryFilter($filterArray);		
		
		//Subject filter
		if (!empty($filterArray['Subject'])){
			foreach ($filterArray['Subject'] as &$Subject){
				$Subject = mysql_real_escape_string(stripslashes($Subject));
			}
			unset($Subject);
			$impoded_Subject = implode("','", $filterArray['Subject']);
			if (in_array('', $filterArray['Subject']))
			{
				$condition_clause .= " AND (`Subject` IS NULL OR `Subject`='' OR `Subject` IN ('{$impoded_Subject}'))";
			}else{
				$condition_clause .= " AND `Subject` IN ('{$impoded_Subject}')";
			}
			
		}
		
		
		//Language filter
		if (!empty($filterArray['language'])){
			foreach ($filterArray['language'] as &$language){
				$language = mysql_real_escape_string($language);
			}
			unset($language);
			$impoded_Language = implode("','", $filterArray['language']);
			if (in_array('', $filterArray['language']))
			{
				$condition_clause .= " AND (`Language` IS NULL OR `Language`='' OR `Language` IN ('{$impoded_Language}'))";
			}else{
				$condition_clause .= " AND `Language` IN ('{$impoded_Language}')";
			}
			
		}
		
		
		//status filter
		if (!empty($filterArray['Status'])){
			switch ($filterArray['Status']) {
				case 'ALL':
					//$impoded_Status = "BORROWED', 'RENEWED";
					break;
				case 'BORROWED':
					$impoded_Status = "BORROWED";
					break;
				case 'RENEW':
					$impoded_Status = "RENEWED";
					break;
			}
			if (!empty($impoded_Status))
				$condition_clause .= " AND bl.`RecordStatus` in ('{$impoded_Status}')";
		}
		
		//user filter
		$condition_clause .= " AND u.`RecordStatus`<>0";	// case #P116646
		if (($filterArray['findBy'] == 'Group') && ($filterArray['groupTargetType'] == 'group') && (!empty($filterArray['groupTarget']))){
			foreach ($filterArray['groupTarget'] as &$groupTarget){
				$groupTarget = mysql_real_escape_string($groupTarget);
			}
			unset($groupTarget);
			$impoded_groupTarget = implode("','", $filterArray['groupTarget']);
			$condition_clause .= " AND u.`UserID` IN (SELECT UserID FROM `LIBMS_GROUP_USER` gu WHERE gu.`UserID` = u.`UserID` AND gu.`GroupID` IN ('{$impoded_groupTarget}'))";
		}

		if (($filterArray['findBy'] == 'Group') && ($filterArray['groupTargetType'] == 'member') && (!empty($filterArray['memberID']))){
			foreach ($filterArray['memberID'] as &$memberID){
				$memberID = mysql_real_escape_string($memberID);
			}
			unset($memberID);
			$impoded_memberID = implode("','", $filterArray['memberID']);
			$condition_clause .= " AND u.`UserID` IN ('{$impoded_memberID}')";
		}
		
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'form')&&(!empty($filterArray['classname']))){
			foreach ($filterArray['classname'] as &$classname){
				$classname = mysql_real_escape_string($classname);
			}
			unset($classname);
			$impoded_classname = implode("','", $filterArray['classname']);
			$condition_clause .= " AND u.`ClassLevel` IN ('{$impoded_classname}')";
		}
			
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'class')&&(!empty($filterArray['classname']))){
			foreach ($filterArray['classname'] as &$classname){
				$classname = mysql_real_escape_string($classname);
			}
			unset($classname);
			$impoded_classname = implode("','", $filterArray['classname']);
			$condition_clause .= " AND u.`ClassName` IN ('{$impoded_classname}')";
		}
			
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'student')&&(!empty($filterArray['studentID']))){
			foreach ($filterArray['studentID'] as &$studentID){
				$studentID = mysql_real_escape_string($studentID);
			}
			unset($studentID);
			$impoded_studentID = implode("','", $filterArray['studentID']);
			$condition_clause .= " AND u.`UserID` IN ('{$impoded_studentID}')";
		}
		
		//keywords
		if (!empty($filterArray['keyword'])){
			$keywords = mysql_real_escape_string($filterArray['keyword']);
			$condition_clause .= " AND (b.`BookTitle` like '%{$keywords}%' or bu.`ACNO` like '%{$keywords}%' or b.`ISBN` like '%{$keywords}%' or b.`CallNum` like '%{$keywords}%')";
			//$sql_join_bu = "  ";
		}
		
		// Order
		$sort_order =  ($filterArray['sortBy']['desc'] == "Desc" ) ? 'DESC' : 'ASC';
		
		
		switch ($filterArray['sortBy']['type']) {
			case 'ClassNumber':
				$condition_clause .= " ORDER BY u.`ClassName`, u.`ClassNumber` {$sort_order}";
				break;
			case 'BookCode':
				$condition_clause .= " ORDER BY b.`BookCode` {$sort_order}";
				break;
			case 'BorrowTime':
				$condition_clause .= " ORDER BY bl.`BorrowTime` {$sort_order}";
				break;
			case 'DueDate':
				$condition_clause .= " ORDER BY bl.`DueDate` {$sort_order}";
				break;
			case 'ReturnedTime':
				$condition_clause .= " ORDER BY bl.`ReturnedTime` {$sort_order}";
				break;
			case 'Payment':
//				$condition_clause .= " ORDER BY bl.`RenewalTime` {$sort_order}";
				$condition_clause .= " ORDER BY ol.`Payment` {$sort_order}";
				break;
			}
			
		
		
/////>>>>>	
	
//		$sql = <<<EOF
//			SELECT				 
//				IF(u.`ClassName` = '' or u.`ClassName` is null, ' -- ', u.`ClassName`) as `ClassName`,
//				IF(u.`ClassNumber` = '' or u.`ClassNumber` is null, ' -- ', u.`ClassNumber`) as `ClassNumber`,
//				u.`EnglishName`, u.`ChineseName`,
//				b.`BookCode`, b.`BookTitle`, u.`BarCode`, 
//				bl.`BorrowTime`, bl.`DueDate`, IF(bl.`ReturnedTime` <> '0000-00-00 00:00:00' ,bl.`ReturnedTime`, ' -- ') as ReturnedTime,
//				IF(ol.`Payment` <> '' ,ol.`Payment`, ' -- ') as `Payment`,
//				u.`UserType`, bl.`RecordStatus`, 
//				IF(b.`BookCategoryCode` = '' or b.`BookCategoryCode` is null, ' -- ', b.`BookCategoryCode`) as `BookCategoryCode`
//			FROM `LIBMS_BORROW_LOG` bl
//			JOIN `LIBMS_BOOK` b
//				on bl.`BookID` = b.`BookID`
//			JOIN `LIBMS_USER` u
//				on u.`UserID` = bl.`UserID`
//			JOIN `LIBMS_GROUP_USER` gu
//				on u.`UserID` = gu.`UserID`
//			LEFT JOIN `LIBMS_OVERDUE_LOG` ol
//				on bl.`BorrowLogID` = ol.`BorrowLogID`
//			WHERE
//				{$condition_clause}
//			
//EOF;
if (!empty($filterArray['ShowLastModifiedBy'])){
	$joinLastModified = " LEFT JOIN `LIBMS_USER` lu on lu.UserID=bl.`LastModifiedBy`";
	$lastModifiedBy = Get_Lang_Selection('lu.ChineseName','lu.EnglishName');
	$lastModifiedInfo = ",$lastModifiedBy as LastModifiedBy,bl.`DateModified`";
}
else {
	$joinLastModified = "";
	$lastModifiedInfo = "";
}

if ($filterArray['tags']){
    $filterArray['tags'] = mysql_real_escape_string($filterArray['tags']);
    $sub_sql = "INNER JOIN " .
        "		(SELECT 		bt.BookID FROM `LIBMS_BOOK_TAG` bt
							INNER JOIN 	`LIBMS_TAG` t on t.`TagID`=bt.`TagID`
							WHERE 		t.`TagName` like '%".$filterArray['tags']."%'
							GROUP BY 	bt.BookID) AS tg
						ON tg.BookID=b.BookID";
}
else {
    $sub_sql = "";
}

// 2013-06-14 [Cameron] Use sub-query in condition because a user can be a member of multiple groups
		$sql = <<<EOF
			SELECT DISTINCT  			 
				IF(u.`ClassName` = '' or u.`ClassName` is null, ' -- ', u.`ClassName`) as `ClassName`,
				IF(u.`ClassNumber` = '' or u.`ClassNumber` is null, ' -- ', u.`ClassNumber`) as `ClassNumber`,
				u.`EnglishName`,u.`ChineseName`,
				bu.`ACNO` AS BookCode, b.`BookTitle`, u.`BarCode`, 
				bl.`BorrowTime`, bl.`DueDate`, IF(bl.`ReturnedTime` <> '0000-00-00 00:00:00' ,bl.`ReturnedTime`, ' -- ') as ReturnedTime,
				IF(ol.`Payment` <> '' ,ol.`Payment`, ' -- ') as `Payment`,
				u.`UserType`, bl.`RecordStatus`, 
				IF(b.`BookCategoryCode` = '' or b.`BookCategoryCode` is null, ' -- ', b.`BookCategoryCode`) as `BookCategoryCode`,
				bl.`AccompanyMaterial` as `BorrowAccompanyMaterial`,
        		IF(b.`CallNum` is null or b.`CallNum` = '', '', b.`CallNum`) as CallNum,
        		IF(b.`CallNum2` is null or b.`CallNum2` = '', '', b.`CallNum2`) as CallNum2
				{$lastModifiedInfo}
			FROM `LIBMS_BORROW_LOG` bl
			LEFT JOIN `LIBMS_BOOK` b
				on bl.`BookID` = b.`BookID`
			LEFT JOIN `LIBMS_USER` u
				on u.`UserID` = bl.`UserID`
			LEFT JOIN (SELECT 
                                v1.`Payment`, 
                                v1.`BorrowLogID`, 
                                v1.`OverDueLogID`
                        FROM
                                `LIBMS_OVERDUE_LOG` v1
                        INNER JOIN (
                                SELECT 
                                    `BorrowLogID`, 
                                    MAX(`OverDueLogID`) AS OverDueLogID
                                FROM
                                    `LIBMS_OVERDUE_LOG`
                                WHERE
                                    `RecordStatus`<>'DELETE'
                                GROUP BY `BorrowLogID`
                            ) AS v2 ON v2.`OverDueLogID`=v1.`OverDueLogID`
                      ) AS ol ON ol.`BorrowLogID` = bl.`BorrowLogID` 
			LEFT JOIN LIBMS_BOOK_UNIQUE AS bu ON (bu.BookID=b.BookID AND bu.UniqueID=bl.UniqueID)
				{$joinLastModified}
			    {$sub_sql}
			WHERE
				{$condition_clause}
			
EOF;


/////<<<<<
		$result =  $this->returnArray($sql,null,1);
		//$GLOBALS['DEBUG']['filter']=$filterArray;
 		//$GLOBALS['DEBUG'][]=$sql;
 		//$GLOBALS['DEBUG'][]=mysql_error();
 		//dump($GLOBALS['DEBUG']);
//debug($sql); 		
//debug_r($result); 		
		return $result;		
	}
	#############
	public function getOverdueReport($filterArray = null){
		global $Lang;
		
		//$condition_clause = ' 1=1 ';

		if (!empty($filterArray['DueDateBefore'])){
			$filterArray['DueDateFrom']  = PHPToSQL($filterArray['DueDateFrom'],'DATE');
			$filterArray['DueDateBefore']  = PHPToSQL($filterArray['DueDateBefore'],'DATE');
			
			$condition_clause = " (UNIX_TIMESTAMP(bl.`DueDate`) >= UNIX_TIMESTAMP({$filterArray['DueDateFrom']}) AND UNIX_TIMESTAMP(bl.`DueDate`) <= UNIX_TIMESTAMP({$filterArray['DueDateBefore']}) )";
		}else{
			$condition_clause =' UNIX_TIMESTAMP(bl.`DueDate`) <= now()';
		}
		if (!empty($filterArray)){
		//dump ($filterArray);
/*			
			if (!empty($filterArray['findBy']))
				if ($filterArray['findBy']['type'] == "Student") {
					//user filter
					$condition_clause .= " AND u.`UserType` ='S' ";
				
					if (($filterArray['rankTarget'] == 'student')&&(!empty($filterArray['studentID']))){
						foreach ($filterArray['studentID'] as &$studentID){
							$studentID = PHPToSQL($studentID);
						}
						unset($studentID);
						$impoded_studentID = implode(" , ", $filterArray['studentID']);
						$condition_clause .= " AND u.`UserID` IN ({$impoded_studentID})";
					}
					
					if (($filterArray['rankTarget'] == 'form')&&(!empty($filterArray['classname']))){
						foreach ($filterArray['classname'] as &$classname){
							$classname = PHPToSQL($classname);
						}
						unset($classname);
						$impoded_classname = implode(" , ", $filterArray['classname']);
						$condition_clause .= " AND u.`ClassLevel` IN ({$impoded_classname})";
					}
						
					if (($filterArray['rankTarget'] == 'class')&&(!empty($filterArray['classname']))){
						foreach ($filterArray['classname'] as &$classname){
							$classname = PHPToSQL($classname);
						}
						unset($classname);
						$impoded_classname = implode(" , ", $filterArray['classname']);
						$condition_clause .= " AND u.`ClassName` IN ({$impoded_classname})";
					}
				}elseif ($filterArray['findBy'] == "Group") {
					if (!empty($filterArray['groupTarget'])){
						foreach ($filterArray['groupTarget'] as &$groupTarget){
							$groupTarget = PHPToSQL($groupTarget);
						}
						$impoded_GroupIDs = implode(" , ", $filterArray['groupTarget']);
						$condition_clause .= " AND gu.`GroupID` IN ({$impoded_GroupIDs})";
					}		
				}
*/
//debug_r($filterArray['findBy']);
			//user filter
			$condition_clause .= " AND u.`RecordStatus`<>0";	// case #P116646
			if ($filterArray['findBy'] == 'Group'){
				foreach ($filterArray['groupTarget'] as &$groupTarget){
					$groupTarget = mysql_real_escape_string($groupTarget);
				}
				unset($groupTarget);
				$impoded_groupTarget = implode("','", $filterArray['groupTarget']);
				$condition_clause .= " AND gu.`GroupID` IN ('{$impoded_groupTarget}')";
			}
			
			if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'form')&&(!empty($filterArray['classname']))){
				foreach ($filterArray['classname'] as &$classname){
					$classname = mysql_real_escape_string($classname);
				}
				unset($classname);
				$impoded_classname = implode("','", $filterArray['classname']);
				$condition_clause .= " AND u.`ClassLevel` IN ('{$impoded_classname}')";
			}
				
			if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'class')&&(!empty($filterArray['classname']))){
				foreach ($filterArray['classname'] as &$classname){
					$classname = mysql_real_escape_string($classname);
				}
				unset($classname);
				$impoded_classname = implode("','", $filterArray['classname']);
				$condition_clause .= " AND u.`ClassName` IN ('{$impoded_classname}')";
			}
				
			if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'student')&&(!empty($filterArray['studentID']))){
				foreach ($filterArray['studentID'] as &$studentID){
					$studentID = mysql_real_escape_string($studentID);
				}
				unset($studentID);
				$impoded_studentID = implode("','", $filterArray['studentID']);
				$condition_clause .= " AND u.`UserID` IN ('{$impoded_studentID}')";
			}
				
		}
		
		//$sort_order = empty($filterArray['sortBy']['desc']) ? 'DESC' : 'ASC';
		//$sort_by = ($filterArray['sortBy']['type'] == 'DATE' )? '`DueDate` '.$sort_order : 'u.`ClassName` '.$sort_order.', u.`ClassNumber` ASC';
		$sort_by = 'u.`ClassName` ASC, u.`ClassNumber` ASC, bl.`DueDate`, lu.`ACNO`';
		# for testing only
		//$condition_all_record = (true) ? "|| UNIX_TIMESTAMP(bl.ReturnedTime)>UNIX_TIMESTAMP(DueDate)" : "";

		# 13.8.12 - ACNO change
		$sql = <<<EOF
			SELECT 
				DISTINCT b.`BookTitle`, lu.`ACNO` AS BookCode, 
				if (lu.CirculationTypeCode='' OR lu.CirculationTypeCode is NULL, b.CirculationTypeCode, lu.CirculationTypeCode) as CirculationTypeCode,
				bl.`DueDate`, u.BarCode,
				u.`UserID`,u.`{$Lang['libms']['sql_field']['User']['name']}` as Name, 
				IF(u.`UserType` = '' or u.`UserType` is null, ' -- ', u.`UserType` ) as `UserType`,
				IF(u.`ClassName` = '' or u.`ClassName` is null, ' -- ', u.`ClassName` ) as `ClassName`,
				IF(u.`ClassLevel` = '' or u.`ClassLevel` is null, ' -- ', u.`ClassLevel` ) as `ClassLevel`,
				IF(u.`ClassNumber` = '' or u.`ClassNumber` is null, ' -- ', u.`ClassNumber` ) as `ClassNumber`,				
				lu.`PurchasePrice` AS PurchasePrice,
				lu.`ListPrice` AS ListPrice 
			FROM `LIBMS_BOOK` b
				
			JOIN (`LIBMS_BORROW_LOG` bl , `LIBMS_USER` u, `LIBMS_GROUP_USER` gu )
				ON (b.`BookID` = bl.`BookID` AND  bl.`UserID` = u.`UserID` AND gu.`UserID` = u.`UserID`)
			LEFT JOIN LIBMS_BOOK_UNIQUE AS lu ON (lu.BookID=b.BookID AND lu.UniqueID=bl.UniqueID) 
			WHERE
				{$condition_clause}
				 AND
				 (bl.`RecordStatus` = 'BORROWED' {$condition_all_record})
			ORDER BY {$sort_by} 
EOF;
		
		$result =  $this->returnArray($sql,null,1);
		

		############# $formatted_result  ############
		$formatted_result = null;
		$prevUID = $currentUID = null;
		$penaltyDate = strtotime($filterArray['PenaltyDate']);
		
		if (!empty($result))
		 foreach ($result as &$row){
			$currentUID = $row['UserID'];
			
			$tmp_arry['BookTitle'] = $row['BookTitle'];
			$tmp_arry['BookCode'] = $row['BookCode'];
			$tmp_arry['CirculationTypeCode'] = $row['CirculationTypeCode'];
			$tmp_arry['DueDate'] = $row['DueDate'];
			
			
			$User = new User($currentUID,$this);
			$TM = new TimeManager();
			$limit = $User->rightRuleManager->get_limits($tmp_arry['CirculationTypeCode']);
			//tolog($row);
			$penalty_day_count = $TM->dayForPenalty(strtotime($row['DueDate']), $penaltyDate);
			if($penalty_day_count <= 0){
				continue;
			}
			$penalty_cost = $penalty_day_count * $limit['OverDueCharge'];
			$penalty_cost =  ($penalty_cost > $limit['MaxFine'])? $limit['MaxFine'] : $penalty_cost;
			
			
			$tmp_arry['Penalty'] = $penalty_cost;
			$tmp_arry['CountDay'] = $penalty_day_count;
			$tmp_arry['PurchasePrice'] = $row['PurchasePrice'];
			$tmp_arry['ListPrice'] = $row['ListPrice'];
			
			
			if ($prevUID != $currentUID){
				$formatted_result[$currentUID]['UID'] = $currentUID;
				$formatted_result[$currentUID]['Name'] = $row['Name'];
				$formatted_result[$currentUID]['UscurrentUIDerType'] = $row['UserType'];
				$formatted_result[$currentUID]['ClassName'] = $row['ClassName'];
				$formatted_result[$currentUID]['ClassLevel'] = $row['ClassLevel'];
				$formatted_result[$currentUID]['ClassNumber'] = $row['ClassNumber'];
				$formatted_result[$currentUID]['BarCode'] = $row['BarCode'];
			}
			$formatted_result[$currentUID]['Overdues'][] = $tmp_arry;
			$prevUID = $currentUID;
		}
		#########	$formatted_result  ############
		return $formatted_result;
	}
	
	############# Lending #################
	public function getLendingReport($filterArray){
		$condition_clause = "1=1";
		$condition_clause2 = "1=1";
		//date filter
		if (!empty($filterArray)){
			$condition_clause1 = "`BorrowTime` >=  '{$filterArray['dateRange']['StartDate']}' AND `BorrowTime` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
		}else{
			$condition_clause1 ='1=1';
		}
		//CirCategory filter
		if (!empty($filterArray['CirCategory'])){
			foreach ($filterArray['CirCategory'] as &$CirCategory){
				$CirCategory = mysql_real_escape_string($CirCategory);
			}
			unset($CirCategory);
			$impoded_category = implode("','", $filterArray['CirCategory']);
			$condition_clause2 .= " AND ((bu.CirculationTypeCode<>'' AND bu.CirculationTypeCode IS NOT NULL AND bu.CirculationTypeCode IN ('{$impoded_category}'))";
			$condition_clause2 .=   " OR (b.CirculationTypeCode<>'' AND b.CirculationTypeCode IS NOT NULL AND b.CirculationTypeCode IN ('{$impoded_category}') AND (bu.CirculationTypeCode='' OR bu.CirculationTypeCode IS NULL))";
			if (in_array('',(array)$filterArray['CirCategory'])) {
				$condition_clause2 .= " OR ((b.CirculationTypeCode='' OR b.CirculationTypeCode IS NULL) AND (bu.CirculationTypeCode='' or bu.CirculationTypeCode IS NULL))";
			}
			$condition_clause2 .= ") ";
			$join_unique_table = '';
			$join_unique_table = "INNER JOIN LIBMS_BOOK_UNIQUE AS bu ON bu.BookID=b.BookID";
			$unique_id_field = ", bu.UniqueID";
		}
		else {
			$join_unique_table = '';
			$unique_id_field = '';
		}
		
		//BookCategory filter
		$condition_clause2 .= $this->getBookCategoryFilter($filterArray);
		
		//Subject filter
		if (!empty($filterArray['Subject'])){
			$specialcharsSubject = array();
			foreach ($filterArray['Subject'] as &$Subject){
				$Subject = mysql_real_escape_string(stripslashes($Subject));
				$specialcharsSubject[] = mysql_real_escape_string(htmlspecialchars(stripslashes($Subject)));
			}
			unset($Subject);
			$impoded_Subject = implode("','", $filterArray['Subject']);
			$impoded_specialcharsSubject =  implode("','", $specialcharsSubject);
			if (in_array('', $filterArray['Subject']))
			{
				$condition_clause2 .= " AND (`Subject` IS NULL OR `Subject`='' OR `Subject` IN ('{$impoded_Subject}') OR `Subject` IN ('{$impoded_specialcharsSubject}'))";
			}else{
				$condition_clause2 .= " AND `Subject` IN ('{$impoded_Subject}')";
			}
			
		}
		
		
		//Language filter
		if (!empty($filterArray['language'])){
			foreach ($filterArray['language'] as &$language){
				$language = mysql_real_escape_string($language);
			}
			unset($language);
			$impoded_Language = implode("','", $filterArray['language']);
			if (in_array('', $filterArray['language']))
			{
				$condition_clause2 .= " AND (`Language` IS NULL OR `Language`='' OR `Language` IN ('{$impoded_Language}'))";
			}else{
				$condition_clause2 .= " AND `Language` IN ('{$impoded_Language}')";
			}
			
		}
		
		
		//status filter
		if (!empty($filterArray['Status'])){
			switch ($filterArray['Status']) {
				case 'ALL':
					//$impoded_Status = "BORROWED', 'RENEWED";
					break;
				case 'BORROWED':
					$impoded_Status = "BORROWED";
					break;
				case 'RENEW':
					$impoded_Status = "RENEWED";
					break;
			}
			if (!empty($impoded_Status))
				$condition_clause1 .= " AND `RecordStatus` in ('{$impoded_Status}')";
		}
		
		//user filter
		$condition_clause .= " AND u.`RecordStatus`<>0";	// case #P116646
		$condition_clause .= $filterArray['IncludeLeftStudent'] ? "" : " AND u.`RecordStatus`<>'3'";	// case #A126787
		if ($filterArray['findBy'] == 'Group'){
			foreach ($filterArray['groupTarget'] as &$groupTarget){
				$groupTarget = mysql_real_escape_string($groupTarget);
			}
			unset($groupTarget);
			$impoded_groupTarget = implode("','", $filterArray['groupTarget']);
			$condition_clause .= " AND u.`UserID` IN (SELECT UserID FROM `LIBMS_GROUP_USER` gu WHERE gu.`UserID` = u.`UserID` AND gu.`GroupID` IN ('{$impoded_groupTarget}'))";
		}
		
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'form')&&(!empty($filterArray['classname']))){
			foreach ($filterArray['classname'] as &$classname){
				$classname = mysql_real_escape_string($classname);
			}
			unset($classname);
			$impoded_classname = implode("','", $filterArray['classname']);
			$condition_clause .= " AND u.`ClassLevel` IN ('{$impoded_classname}')";
		}
			
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'class')&&(!empty($filterArray['classname']))){
			foreach ($filterArray['classname'] as &$classname){
				$classname = mysql_real_escape_string($classname);
			}
			unset($classname);
			$impoded_classname = implode("','", $filterArray['classname']);
			$condition_clause .= " AND u.`ClassName` IN ('{$impoded_classname}')";
		}
			
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'student')&&(!empty($filterArray['studentID']))){
			foreach ($filterArray['studentID'] as &$studentID){
				$studentID = mysql_real_escape_string($studentID);
			}
			unset($studentID);
			$impoded_studentID = implode("','", $filterArray['studentID']);
			$condition_clause .= " AND u.`UserID` IN ('{$impoded_studentID}')";
		}
		//$condition_clause .= " AND u.`RecordStatus` = '1'";
		
		$sql = "SELECT b.`BookID` {$unique_id_field} FROM `LIBMS_BOOK` b {$join_unique_table} WHERE {$condition_clause2}";
			
		$book_array =  $this->returnResultSet($sql);
		if (count($book_array) > 0) {
			foreach((array)$book_array as $k=>$v) {
				$BookID[] = $v['BookID'];
			}
			$BookID = implode("','",$BookID);
			$condition_clause1 .= " AND `BookID` IN ('{$BookID}') ";

			if ($unique_id_field != '') {
				foreach((array)$book_array as $k=>$v) {
					$UniqueID[] = $v['UniqueID'];
				}
				$UniqueID = implode("','",$UniqueID);
				$condition_clause1 .= " AND `UniqueID` IN ('{$UniqueID}') ";
			}
		}
		
		//Group + Order
		if ($filterArray['sortBy']['desc'] == "Desc")
		{
			# order by 
			$sort_by_field = "Total";
			$sort_order = "DESC";
		} else
		{
			$sort_order = "ASC";
		}
		switch ($filterArray['sortBy']['type']) {
			case 'ClassLevel':
				$sort_by_field = ($sort_by_field!="") ? $sort_by_field : "ClassLevel";
				$condition_clause .= " GROUP BY u.`ClassLevel` ORDER BY `{$sort_by_field}` {$sort_order}";
				break;
			case 'ClassName':
				$sort_by_field = ($sort_by_field!="") ? $sort_by_field : "ClassName";
				$condition_clause .= " GROUP BY u.`ClassName` ORDER BY `{$sort_by_field}` {$sort_order}";
				break;
			case 'Student':
				$sort_by_field = ($sort_by_field!="") ? $sort_by_field : "`ClassName`, ABS(`ClassNumber`), `Student`";
				$condition_clause .= " GROUP BY u.`ClassName`, u.`ClassNumber`, u.`UserID` ORDER BY {$sort_by_field} {$sort_order}";
				break;
			}
		
		$sql_displayGenderTotal = '';
		if ($filterArray['displayGenderTotal'] == 1){
			$sql_displayGenderTotal = "IFNULL(SUM(u.Gender = 'M' AND u.`UserID` = bl.`UserID`),0) as MaleTotal, IFNULL(SUM(u.Gender = 'F' AND u.`UserID` = bl.`UserID`),0) as FemaleTotal,";
		}
		
		$studentName = getNameFieldWithClassNumberByLangLIBMGMT("u.");
		$onlyStudentName = Get_Lang_Selection('u.ChineseName', 'u.EnglishName');
		
		if ($filterArray['tags']){
			$filterArray['tags'] = mysql_real_escape_string($filterArray['tags']);
			$sub_sql = "INNER JOIN " .
					"		(SELECT 		bt.BookID FROM `LIBMS_BOOK_TAG` bt 
							INNER JOIN 	`LIBMS_TAG` t on t.`TagID`=bt.`TagID`			
							WHERE 		t.`TagName` like '%".$filterArray['tags']."%' 
							GROUP BY 	bt.BookID) AS tg 
						ON tg.BookID=bl.BookID";
		}
		else {
			$sub_sql = "";
		}
		
		$sql = <<<EOF
			SELECT
				IF(u.`ClassLevel` is null or u.`ClassLevel`='', ' -- ', u.`ClassLevel`) as `ClassLevel`, 
				IF(u.`ClassName` is null or u.`ClassName`='', ' -- ', u.`ClassName`) as `ClassName`, 
				IF(u.`ClassNumber` is null or u.`ClassNumber`='', ' -- ', u.`ClassNumber`) as `ClassNumber`,
				IF({$onlyStudentName} is null or {$onlyStudentName}='', ' -- ', {$onlyStudentName}) as `onlyStudentName`,
				{$studentName} as `Student`,
				u.RecordStatus, 
				{$sql_displayGenderTotal}
				IFNULL(COUNT(bl.`UserID`),0) as Total
			FROM (SELECT `BookID`,`UserID`, `BorrowLogID` FROM `LIBMS_BORROW_LOG` WHERE {$condition_clause1}) bl
			$sub_sql 
			RIGHT JOIN (SELECT `UserID`,`ClassLevel`,`ClassName`,`ClassNumber`,`RecordStatus`,`ChineseName`,`EnglishName`,`BarCode`,`Gender` FROM `LIBMS_USER`) u
				on u.`UserID` = bl.`UserID`
			LEFT JOIN (SELECT `BorrowLogID` FROM `LIBMS_OVERDUE_LOG` WHERE RecordStatus<>'DELETE' GROUP BY BorrowLogID) ol
				on bl.`BorrowLogID` = ol.`BorrowLogID`
			WHERE
				{$condition_clause}
EOF;

//debug($sql);
		$result =  $this->returnArray($sql,null,1);
		//debug_pr($sql);
		//$GLOBALS['DEBUG']['filterArray']=$filterArray;
		//$GLOBALS['DEBUG']['sql']=$sql;
 		//$GLOBALS['DEBUG'][]=mysql_error();
 		//dump($GLOBALS['DEBUG']);
 		//error_log("report.php: 727: ". var_export($GLOBALS['DEBUG'],true));
		return $result;
	}
	
	
	
	
	############# Inactive Reader #################
	public function getReaderInactiveReport($filterArray){
	
		$TotalMax = $filterArray["TotalAtMost"];
		
		//date filter
		if (!empty($filterArray)){
			$condition_period = "AND UNIX_TIMESTAMP(bl.`BorrowTime`) >=  UNIX_TIMESTAMP('{$filterArray['dateRange']['StartDate']}') AND UNIX_TIMESTAMP(bl.`BorrowTime`) <=  UNIX_TIMESTAMP('{$filterArray['dateRange']['EndDate']} 23:59:59') ";
		} 
			
		//user filter
	
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'form')&&(!empty($filterArray['classname']))){
			foreach ($filterArray['classname'] as &$classname){
				$classname = mysql_real_escape_string($classname);
			}
			unset($classname);
			$impoded_classname = implode("','", $filterArray['classname']);
			$condition_class_form = "  lu.`ClassLevel` IN ('{$impoded_classname}')";
		}
	
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'class')&&(!empty($filterArray['classname']))){
			foreach ($filterArray['classname'] as &$classname){
				$classname = mysql_real_escape_string($classname);
			}
			unset($classname);
			$impoded_classname = implode("','", $filterArray['classname']);
			$condition_class_form = "  lu.`ClassName` IN ('{$impoded_classname}')";
		}
	
		if ($filterArray['gender'] == 'M' || $filterArray['gender'] == 'F'){
			$condition_class_form .= " AND lu.`Gender` = '".$filterArray['gender']."'";
		}
/////>>>>>	
//		$sql = <<<EOF
//		SELECT 
//			lu.userID, lu.ClassName, lu.ClassNumber, lu.BarCode, lu.ChineseName, lu.EnglishName, count(bl.BorrowLogID) as BorrowTotal 
//		FROM 
//			LIBMS_USER as lu LEFT JOIN LIBMS_BORROW_LOG AS bl ON (bl.UserID=lu.UserID {$condition_period}) 
//		WHERE
//			{$condition_class_form}
//		GROUP By 
//			lu.UserID
//			{$condition_clause}
//		ORDER By
//			lu.ClassName, lu.ClassNumber, lu.EnglishName
//	
//EOF;

		$sql = <<<EOF
		SELECT 
			lu.userID,
			IF(lu.`ClassLevel` is null or lu.`ClassLevel` = '' , ' -- ', lu.`ClassLevel`) as ClassLevel,
			IF(lu.`ClassName` is null or lu.`ClassName` = '' , ' -- ', lu.`ClassName`) as ClassName,
			IF(lu.`ClassNumber` IS NULL or lu.`ClassNumber` = '' , ' -- ', lu.`ClassNumber`) as ClassNumber,
			IF(lu.`BarCode` is null or lu.`BarCode` = '', ' -- ', lu.`BarCode`) as BarCode,
			IF(lu.`ChineseName` is null or lu.`ChineseName` = '', ' -- ', lu.`ChineseName`) as ChineseName,
			IF(lu.`EnglishName` is null or lu.`EnglishName` = '', ' -- ', lu.`EnglishName`) as EnglishName,	
			count(bl.BorrowLogID) as BorrowTotal 
		FROM 
			LIBMS_USER as lu LEFT JOIN LIBMS_BORROW_LOG AS bl ON (bl.UserID=lu.UserID {$condition_period}) 
		WHERE
			{$condition_class_form}
			AND lu.`RecordStatus` = '1' 
		GROUP By 
			lu.UserID
			{$condition_clause}
		ORDER By
			lu.ClassLevel, lu.ClassName, lu.ClassNumber, lu.EnglishName
	
EOF;
/////<<<<<

		//debug($sql);	
		$rows =  $this->returnResultSet($sql);
		for ($i=0; $i<sizeof($rows); $i++)
		{
			if ($rows[$i]["BorrowTotal"]<=$TotalMax)
			{
				$result[] = $rows[$i];
			}
		}
		$result[0]['sortBy'] = $filterArray['sortBy']['type'];
		return $result;
	}
	
	############# ReaderRanking #################
	public function getReaderRankingReport($filterArray){
	
		//date filter
		if (!empty($filterArray)){
			$condition_clause = "bl.`BorrowTime` >=  '{$filterArray['dateRange']['StartDate']}' AND bl.`BorrowTime` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
		}else{
			$condition_clause ='1=1';
		}
		//CirCategory filter
		if (!empty($filterArray['CirCategory'])){
			foreach ($filterArray['CirCategory'] as &$CirCategory){
				$CirCategory = mysql_real_escape_string($CirCategory);
			}
			unset($CirCategory);
			$impoded_category = implode("','", $filterArray['CirCategory']);
			
			$condition_clause .= "AND ((bu.CirculationTypeCode<>'' AND bu.CirculationTypeCode IS NOT NULL AND bu.CirculationTypeCode IN ('{$impoded_category}'))";
			$condition_clause .=   " OR (b.CirculationTypeCode<>'' AND b.CirculationTypeCode IS NOT NULL AND b.CirculationTypeCode IN ('{$impoded_category}') AND (bu.CirculationTypeCode='' OR bu.CirculationTypeCode IS NULL))";
			if (in_array('',(array)$filterArray['CirCategory'])) {
				$condition_clause .= " OR ((b.CirculationTypeCode='' OR b.CirculationTypeCode IS NULL) AND (bu.CirculationTypeCode='' or bu.CirculationTypeCode IS NULL))";
			}
			$condition_clause .= ") ";
			
			$join_book_unique = "LEFT JOIN LIBMS_BOOK_UNIQUE AS bu ON (bu.BookID=b.BookID AND bu.UniqueID=bl.UniqueID)";
		}
		else {
			$join_book_unique = '';
		}
	
		//BookCategory filter		
		$condition_clause .= $this->getBookCategoryFilter($filterArray);		
	
		//Subject filter
		if (!empty($filterArray['Subject'])){
			foreach ($filterArray['Subject'] as &$Subject){
				$Subject = mysql_real_escape_string($Subject);
			}
			unset($Subject);
			$impoded_Subject = implode("','", $filterArray['Subject']);
			if (in_array('', $filterArray['Subject']))
			{
				$condition_clause .= " AND (`Subject` IS NULL OR `Subject`='' OR `Subject` IN ('{$impoded_Subject}'))";
			}else{
				$condition_clause .= " AND `Subject` IN ('{$impoded_Subject}')";
			}
			
		}
	
	
		//Language filter
		if (!empty($filterArray['language'])){
			foreach ($filterArray['language'] as &$language){
				$language = mysql_real_escape_string($language);
			}
			unset($language);
			$impoded_Language = implode("','", $filterArray['language']);
			if (in_array('', $filterArray['language']))
			{
				$condition_clause .= " AND (`Language` IS NULL OR `Language`='' OR `Language` IN ('{$impoded_Language}'))";
			}else{
				$condition_clause .= " AND `Language` IN ('{$impoded_Language}')";
			}
			
		}
	
		//user filter
		$condition_clause .= " AND u.`RecordStatus`<>0";	// case #P116646
		if ($filterArray['findBy'] == 'Group'){
			foreach ($filterArray['groupTarget'] as &$groupTarget){
				$groupTarget = mysql_real_escape_string($groupTarget);
			}
			unset($groupTarget);
			$impoded_groupTarget = implode("','", $filterArray['groupTarget']);
			$condition_clause .= " AND u.`UserID` IN (SELECT UserID FROM `LIBMS_GROUP_USER` gu WHERE gu.`UserID` = u.`UserID` AND gu.`GroupID` IN ('{$impoded_groupTarget}'))";
		}
	
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'form')&&(!empty($filterArray['classname']))){
			foreach ($filterArray['classname'] as &$classname){
				$classname = mysql_real_escape_string($classname);
			}
			unset($classname);
			$impoded_classname = implode("','", $filterArray['classname']);
			$condition_clause .= " AND u.`ClassLevel` IN ('{$impoded_classname}')";
		}
	
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'class')&&(!empty($filterArray['classname']))){
			foreach ($filterArray['classname'] as &$classname){
				$classname = mysql_real_escape_string($classname);
			}
			unset($classname);
			$impoded_classname = implode("','", $filterArray['classname']);
			$condition_clause .= " AND u.`ClassName` IN ('{$impoded_classname}')";
		}
	
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'student')&&(!empty($filterArray['studentID']))){
			foreach ($filterArray['studentID'] as &$studentID){
				$studentID = mysql_real_escape_string($studentID);
			}
			unset($studentID);
			$impoded_studentID = implode("','", $filterArray['studentID']);
			$condition_clause .= " AND u.`UserID` IN ('{$impoded_studentID}')";
		}
		
		if ($filterArray['gender'] == 'M' || $filterArray['gender'] == 'F'){
			$condition_clause .= " AND u.`Gender` = '".$filterArray['gender']."'";
		}

		if ($filterArray['tags']){
			$filterArray['tags'] = mysql_real_escape_string($filterArray['tags']);
			$sub_sql = "INNER JOIN " .
					"		(SELECT 		bt.BookID FROM `LIBMS_BOOK_TAG` bt 
							INNER JOIN 	`LIBMS_TAG` t on t.`TagID`=bt.`TagID`			
							WHERE 		t.`TagName` like '%".$filterArray['tags']."%' 
							GROUP BY 	bt.BookID) AS tg 
						ON tg.BookID=b.BookID";
		}
		else {
			$sub_sql = "";
		}
	
		//Order + Limit
		$condition_clause .= " GROUP BY `UserID` ORDER BY `Total` DESC";
		$limit = '100';
		if ((!empty($filterArray['listNo']))&(is_numeric($filterArray['listNo']))) $limit = $filterArray['listNo'];
			$condition_clause .= " LIMIT 0, {$limit}";
		
		$sql = <<<EOF
		SELECT
		u.`UserID`, 
		IF(u.`EnglishName` is null or u.`EnglishName` = '', ' -- ', u.`EnglishName`) as EnglishName,
		IF(u.`ChineseName` is null or u.`ChineseName` = '', ' -- ', u.`ChineseName`) as ChineseName,
		IF(u.`BarCode` is null or u.`BarCode` = '', ' -- ', u.`BarCode`) as BarCode,		
		IF(u.`ClassName` is null or u.`ClassName` = '' , ' -- ', u.`ClassName`) as ClassName,
		IF(u.`ClassNumber` IS NULL or u.`ClassNumber` = '' , ' -- ', u.`ClassNumber`) as ClassNumber, count(*) as Total
		FROM `LIBMS_BORROW_LOG` bl
		JOIN `LIBMS_BOOK` b
		on bl.`BookID` = b.`BookID`
		JOIN `LIBMS_USER` u
		on u.`UserID` = bl.`UserID`
		LEFT JOIN (SELECT 
                        BorrowLogID 
                    FROM 
                        `LIBMS_OVERDUE_LOG` 
                    WHERE `RecordStatus`<>'DELETE'
                    GROUP BY BorrowLogID
                ) AS ol on bl.`BorrowLogID` = ol.`BorrowLogID` 
		$sub_sql
		$join_book_unique
		WHERE
		{$condition_clause}
	
EOF;

		$result =  $this->returnArray($sql,null,1);
		//$GLOBALS['DEBUG']['filterArray']=$filterArray;
		//$GLOBALS['DEBUG']['sql']=$sql;
		//$GLOBALS['DEBUG']['result']=$result;
		//$GLOBALS['DEBUG'][]=mysql_error();
		//dump($GLOBALS['DEBUG']);
		//error_log("report.php: 727: ". var_export($GLOBALS['DEBUG'],true));
	return $result;
	}
	
	
	############# BookRanking #################
	public function getBookRankingReport($filterArray){
	
		//date filter
		if (!empty($filterArray)){
			$condition_clause = "bl.`BorrowTime` >=  '{$filterArray['dateRange']['StartDate']}' AND bl.`BorrowTime` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
		}else{
			$condition_clause ='1=1';
		}
		//CirCategory filter
		if (!empty($filterArray['CirCategory'])){
			foreach ($filterArray['CirCategory'] as &$CirCategory){
				$CirCategory = mysql_real_escape_string($CirCategory);
			}
			unset($CirCategory);
			$impoded_category = implode("','", $filterArray['CirCategory']);
			
			$condition_clause .= "AND ((bu.CirculationTypeCode<>'' AND bu.CirculationTypeCode IS NOT NULL AND bu.CirculationTypeCode IN ('{$impoded_category}'))";
			$condition_clause .=   " OR (b.CirculationTypeCode<>'' AND b.CirculationTypeCode IS NOT NULL AND b.CirculationTypeCode IN ('{$impoded_category}') AND (bu.CirculationTypeCode='' OR bu.CirculationTypeCode IS NULL))";
			if (in_array('',(array)$filterArray['CirCategory'])) {
				$condition_clause .= " OR ((b.CirculationTypeCode='' OR b.CirculationTypeCode IS NULL) AND (bu.CirculationTypeCode='' or bu.CirculationTypeCode IS NULL))";
			}
			$condition_clause .= ") ";
			
			$join_book_unique = "INNER JOIN LIBMS_BOOK_UNIQUE AS bu ON (bu.BookID=b.BookID AND bu.UniqueID=bl.UniqueID)";
			
		}
		else {
			$join_book_unique = '';
		}

		//BookCategory filter		
		$condition_clause .= $this->getBookCategoryFilter($filterArray);		
	
		//Subject filter
		if (!empty($filterArray['Subject'])){
			foreach ($filterArray['Subject'] as &$Subject){
				$Subject = mysql_real_escape_string($Subject);
			}
			unset($Subject);
			$impoded_Subject = implode("','", $filterArray['Subject']);
			if (in_array('', $filterArray['Subject']))
			{
				$condition_clause .= " AND (`Subject` IS NULL OR `Subject`='' OR `Subject` IN ('{$impoded_Subject}'))";
			}else{
				$condition_clause .= " AND `Subject` IN ('{$impoded_Subject}')";
			}
			
		}
	
	
		//Language filter
		if (!empty($filterArray['language'])){
			foreach ($filterArray['language'] as &$language){
				$language = mysql_real_escape_string($language);
			}
			unset($language);
			$impoded_Language = implode("','", $filterArray['language']);
			if (in_array('', $filterArray['language']))
			{
				$condition_clause .= " AND (`Language` IS NULL OR `Language`='' OR `Language` IN ('{$impoded_Language}'))";
			}else{
				$condition_clause .= " AND `Language` IN ('{$impoded_Language}')";
			}
			
		}
	
		//user filter
		$condition_clause .= " AND u.`RecordStatus`<>0";	// case #P116646
		if ($filterArray['findBy'] == 'Group'){
			foreach ($filterArray['groupTarget'] as &$groupTarget){
				$groupTarget = mysql_real_escape_string($groupTarget);
			}
			unset($groupTarget);
			$impoded_groupTarget = implode("','", $filterArray['groupTarget']);
			$condition_clause .= " AND u.`UserID` IN (SELECT UserID FROM `LIBMS_GROUP_USER` gu WHERE gu.`UserID` = u.`UserID` AND gu.`GroupID` IN ('{$impoded_groupTarget}'))";
		}
	
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'form')&&(!empty($filterArray['classname']))){
			foreach ($filterArray['classname'] as &$classname){
				$classname = mysql_real_escape_string($classname);
			}
			unset($classname);
			$impoded_classname = implode("','", $filterArray['classname']);
			$condition_clause .= " AND u.`ClassLevel` IN ('{$impoded_classname}')";
		}
	
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'class')&&(!empty($filterArray['classname']))){
			foreach ($filterArray['classname'] as &$classname){
				$classname = mysql_real_escape_string($classname);
			}
			unset($classname);
			$impoded_classname = implode("','", $filterArray['classname']);
			$condition_clause .= " AND u.`ClassName` IN ('{$impoded_classname}')";
		}
	
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'student')&&(!empty($filterArray['studentID']))){
			foreach ($filterArray['studentID'] as &$studentID){
				$studentID = mysql_real_escape_string($studentID);
			}
			unset($studentID);
			$impoded_studentID = implode("','", $filterArray['studentID']);
			$condition_clause .= " AND u.`UserID` IN ('{$impoded_studentID}')";
		}
	
		if ($filterArray['tags']){
			$filterArray['tags'] = mysql_real_escape_string($filterArray['tags']);
			$sub_sql = "INNER JOIN " .
					"		(SELECT 		bt.BookID FROM `LIBMS_BOOK_TAG` bt 
							INNER JOIN 	`LIBMS_TAG` t on t.`TagID`=bt.`TagID`			
							WHERE 		t.`TagName` like '%".$filterArray['tags']."%' 
							GROUP BY 	bt.BookID) AS tg 
						ON tg.BookID=b.BookID";
		}
		else {
			$sub_sql = "";
		}
	
		//Order + Limit
		$condition_clause .= " GROUP BY b.`BookID` ORDER BY `Total` DESC, b.`BookTitle`";
		$limit = '100';
		if ((!empty($filterArray['listNo']))&(is_numeric($filterArray['listNo']))) $limit = $filterArray['listNo'];
		$condition_clause .= " LIMIT 0, {$limit}";
	
		$sql = <<<EOF
		SELECT
		IF(b.`ISBN` is null or b.`ISBN` = '', ' -- ', b.`ISBN`) as ISBN,
		b.`BookTitle`,
		IF(b.`CallNum` is null or b.`CallNum` = '' , '', b.`CallNum`) as CallNum,
		IF(b.`CallNum2` is null or b.`CallNum2` = '' , '', b.`CallNum2`) as CallNum2,
		IF(b.`ResponsibilityBy1` is null or b.`ResponsibilityBy1` = '' , ' -- ', b.`ResponsibilityBy1`) as ResponsibilityBy1,
		IF(b.`Publisher` is null or b.`Publisher` = '' , ' -- ', b.`Publisher`) as Publisher,
		IF(b.`Edition` is null or b.`Edition` = '' , ' -- ', b.`Edition`) as Edition, count(*) as Total
		FROM `LIBMS_BORROW_LOG` bl
		JOIN `LIBMS_BOOK` b
		on bl.`BookID` = b.`BookID`
		JOIN `LIBMS_USER` u
		on u.`UserID` = bl.`UserID`
		$sub_sql
		$join_book_unique
		WHERE
		{$condition_clause}
	
EOF;
	
		$result =  $this->returnArray($sql,null,1);
		
		//$GLOBALS['DEBUG']['filterArray']=$filterArray;
		//$GLOBALS['DEBUG']['sql']=$sql;
		//$GLOBALS['DEBUG'][]=mysql_error();
		//dump($GLOBALS['DEBUG']);
		//error_log("report.php: 727: ". var_export($GLOBALS['DEBUG'],true));
	return $result;
	}
	
	
	############# BookReport #################
	public function getBookReportReport($filterArray){
		global $Lang, $intranet_session_language;
		
		if($filterArray['action']=='getBook'){	

			$search = $filterArray['Search'];
			if (!get_magic_quotes_gpc()) {
				$search = stripslashes($search);
			}
			if($search!="") {
				$unconverted_search_sql = mysql_real_escape_string(str_replace("\\","\\\\",$search));		// A&<>'"\B ==> A&<>\'\"\\\\B
				$converted_search_sql = special_sql_str($search);											// A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B

				if ($unconverted_search_sql == $converted_search_sql) {		// keyword does not contain any special character: &<>"
					$condition_clause .= " bu.`ACNO` LIKE '%{$unconverted_search_sql}%'";
					$condition_clause .= " OR b.`BookTitle` LIKE '%{$unconverted_search_sql}%'";
					$condition_clause .= " OR b.`ISBN` = '{$unconverted_search_sql}'";
				}
				else {
					$condition_clause .= " bu.`ACNO` LIKE '%{$unconverted_search_sql}%' OR bu.`ACNO` LIKE '%{$converted_search_sql}%'";
					$condition_clause .= " OR b.`BookTitle` LIKE '%{$unconverted_search_sql}%' OR b.`BookTitle` LIKE '%{$converted_search_sql}%'";
					$condition_clause .= " OR b.`ISBN` = '{$unconverted_search_sql}' OR b.`ISBN` = '{$converted_search_sql}'";
				}
					
				$sql = <<<EOF
				SELECT
				DISTINCT b.`BookCode`,b.`CallNum`, b.`BookTitle`, b.`ResponsibilityBy1`,b.`BookID`
				FROM `LIBMS_BOOK` b LEFT JOIN LIBMS_BOOK_UNIQUE AS bu ON bu.BookID=b.BookID
				WHERE
				{$condition_clause}
		
EOF;
				$result['bookInfo'] =  $this->returnArray($sql,null,1);
			}
			unset($book);
			unset($book_val);
			if (!empty($result['bookInfo'])) {
				$result['is_success'] =  true; 
				foreach($result['bookInfo'] as &$book){
					foreach($book as &$book_val){
						if (is_null($book_val)){
							$book_val ='';
						}
					}
				}
				
			} else {
				$result['is_success'] =  false;
			} 
		}
		
		if(($filterArray['action']=='getResult')||($filterArray['action']=='print')){
			####### Book Info ##############################################################
			$sql = <<<EOF
			SELECT b.`BookCode`, b.`BookTitle`,
			IF(b.`CallNum` = '' or b.`CallNum` is null , ' -- ', b.`CallNum`) as CallNum,
			b.`CallNum2`,	
			IF(b.`Edition` = '' or b.`Edition` is null , ' -- ', b.`Edition`) as Edition,	
			IF(b.`Language` = '' or b.`Language` is null , ' -- ', b.`Language`) as Language,	
			IF(b.`Country` = '' or b.`Country` is null , ' -- ', b.`Country`) as Country,	
			IF(b.`Publisher` = '' or b.`Publisher` is null , ' -- ', b.`Publisher`) as Publisher,				
			IF(b.`ResponsibilityBy1` = '' or b.`ResponsibilityBy1` is null , ' -- ', b.`ResponsibilityBy1`) as ResponsibilityBy1,	
			IF(cir.`{$Lang['libms']['sql_field']['Description']}` = '' or cir.`{$Lang['libms']['sql_field']['Description']}` is null , ' -- ', cir.`{$Lang['libms']['sql_field']['Description']}`) as circulation, 
			IF(c.`{$Lang['libms']['sql_field']['Description']}` = '' or c.`{$Lang['libms']['sql_field']['Description']}` is null , ' -- ', c.`{$Lang['libms']['sql_field']['Description']}`) as category ,
			IF(r.`{$Lang['libms']['sql_field']['Description']}` = '' or r.`{$Lang['libms']['sql_field']['Description']}` is null , ' -- ', r.`{$Lang['libms']['sql_field']['Description']}`) as resources, CoverImage
			FROM  `LIBMS_BOOK` b
			LEFT JOIN `LIBMS_BOOK_CATEGORY` c ON b.`BookCategoryCode` = c.`BookCategoryCode`
			LEFT JOIN `LIBMS_RESOURCES_TYPE` r ON b.`ResourcesTypeCode` = r.`ResourcesTypeCode`
			LEFT JOIN `LIBMS_CIRCULATION_TYPE` cir ON b.`CirculationTypeCode` = cir.`CirculationTypeCode`
			WHERE BookID = '{$filterArray['bookID']}'
			
EOF;
//		debug($sql);
			$result['bookInfo'] =  $this->returnArray($sql,null,1);
			
			# tags (category)
			$sql = " SELECT DISTINCT lt.TagName FROM LIBMS_BOOK_TAG AS lbt, LIBMS_TAG AS lt WHERE lbt.BookID=".$filterArray['bookID']." " .
					" AND lt.TagID=lbt.TagID ORDER by TagName ASC ";
			$tagRows = $this->returnVector($sql);
			if (sizeof($tagRows)>0)
			{
				$BookTags = implode(", ", $tagRows);
			} else
			{
				$BookTags = "";
			}
			$result['tags'] =  $BookTags;
		
			####### Reserve Log ##############################################################
			$sql = <<<EOF
			SELECT count(*) as TotalReserve
			FROM  `LIBMS_RESERVATION_LOG` rl
			LEFT JOIN `LIBMS_BOOK_UNIQUE` u ON rl.`BookID` = u.`BookID`
			WHERE rl.`BookID` = '{$filterArray['bookID']}'
			Group by rl.`BookID`
EOF;
			
			$result['reserve'] =  $this->returnArray($sql,null,1);
			
			$sql_desc_lang = ($intranet_session_language=="b5") ? "DescriptionChi" : "DescriptionEn" ;
		
			####### Borrow Log ##############################################################
			$sql = <<<EOF
			SELECT u.`BarCode`, u.`ACNO`, u.`RecordStatus`, l.{$sql_desc_lang} As LocationTitle, count( bl.BookID ) as TotalBorrow
			FROM  `LIBMS_BORROW_LOG` bl
			RIGHT JOIN `LIBMS_BOOK_UNIQUE` u ON bl.`UniqueID` = u.`UniqueID`
			LEFT JOIN LIBMS_LOCATION AS l ON l.LocationCode=u.LocationCode
			WHERE u.`BookID` = '{$filterArray['bookID']}' 
			Group by u.`BarCode`
			
EOF;
			$result['borrow'] =  $this->returnArray($sql,null,1);

		
			####### Overdue Log ##############################################################
			$sql = <<<EOF
			SELECT count(bl.bookID) as TotalOverdue
			FROM  (SELECT 
                        BorrowLogID 
                    FROM 
                        `LIBMS_OVERDUE_LOG`
                    WHERE 
                        RecordStatus<>'DELETE'
                    GROUP BY BorrowLogID
                  ) AS ol
			JOIN `LIBMS_BORROW_LOG` bl ON bl.`BorrowLogID` = ol.`BorrowLogID`
			WHERE bl.`BookID` = '{$filterArray['bookID']}'
			Group by bl.`BookID`
			
EOF;
			
			$result['overdue'] =  $this->returnArray($sql,null,1);
		
		####### Money ##############################################################
		$sql = <<<EOF
		SELECT sum(ol.`Payment`) as TotalMoney
		FROM  (SELECT 
                        v1.`Payment`, 
                        v1.`BorrowLogID`, 
                        v1.`OverDueLogID`
                FROM
                        `LIBMS_OVERDUE_LOG` v1
                INNER JOIN (
                        SELECT 
                            `BorrowLogID`, 
                            MAX(`OverDueLogID`) AS OverDueLogID
                        FROM
                            `LIBMS_OVERDUE_LOG`
                        WHERE
                            `RecordStatus`<>'DELETE'
                        GROUP BY `BorrowLogID`
                    ) AS v2 ON v2.`OverDueLogID`=v1.`OverDueLogID`
              ) AS ol 
		RIGHT JOIN `LIBMS_BORROW_LOG` bl ON bl.`BorrowLogID` = ol.`BorrowLogID`
		WHERE bl.`BookID` = '{$filterArray['bookID']}'
		Group by bl.`BookID`
		
EOF;
		
		$result['money'] =  $this->returnArray($sql,null,1);
			
		}
		
		//$GLOBALS['DEBUG']['filterArray']=$filterArray;
		//$GLOBALS['DEBUG']['sql']=$sql;
		//$GLOBALS['DEBUG'][]=mysql_error();
		//$GLOBALS['DEBUG']['result']=$result;
		//dump($GLOBALS['DEBUG']);
		//error_log("report.php: 1013: ". var_export($GLOBALS['DEBUG'],true));
	return $result;
	}
	
	################### Penalty
	public function getPenaltyReport($filterArray){
	
		//date filter
		if (!empty($filterArray)){
			//$condition_clause = "bl.`BorrowTime` >=  '{$filterArray['dateRange']['StartDate']}' AND bl.`BorrowTime` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
			//Henry Added 20131217
			$condition_clause = "UNIX_TIMESTAMP(ol.`DateCreated`) >=  UNIX_TIMESTAMP('{$filterArray['dateRange']['StartDate']}') AND UNIX_TIMESTAMP(ol.`DateCreated`) <=  UNIX_TIMESTAMP('{$filterArray['dateRange']['EndDate']} 23:59:59') ";
		}else{
			$condition_clause ='1=1';
		}
		
	
		//user filter
		$condition_clause .= " AND u.`RecordStatus`<>0";	// case #P116646
		
		$condition_clause .= " AND ol.`RecordStatus`<>'DELETE'";      // case #L154330
		
		if ($filterArray['findBy'] == 'Group'){
			foreach ($filterArray['groupTarget'] as &$groupTarget){
				$groupTarget = mysql_real_escape_string($groupTarget);
			}
			unset($groupTarget);
			$impoded_groupTarget = implode("','", $filterArray['groupTarget']);
			$condition_clause .= " AND gu.`GroupID` IN ('{$impoded_groupTarget}')";
		}
	
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'form')&&(!empty($filterArray['classname']))){
			foreach ($filterArray['classname'] as &$classname){
				$classname = mysql_real_escape_string($classname);
			}
			unset($classname);
			$impoded_classname = implode("','", $filterArray['classname']);
			$condition_clause .= " AND u.`ClassLevel` IN ('{$impoded_classname}')";
		}
	
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'class')&&(!empty($filterArray['classname']))){
			foreach ($filterArray['classname'] as &$classname){
				$classname = mysql_real_escape_string($classname);
			}
			unset($classname);
			$impoded_classname = implode("','", $filterArray['classname']);
			$condition_clause .= " AND u.`ClassName` IN ('{$impoded_classname}')";
		}
	
		if (($filterArray['findBy'] == 'Student')&&($filterArray['rankTarget'] == 'student')&&(!empty($filterArray['studentID']))){
			foreach ($filterArray['studentID'] as &$studentID){
				$studentID = mysql_real_escape_string($studentID);
			}
			unset($studentID);
			$impoded_studentID = implode("','", $filterArray['studentID']);
			$condition_clause .= " AND u.`UserID` IN ('{$impoded_studentID}')";
		}
		//Henry Added 20131217
		if (($filterArray['RecordStatus'] !="")&&($filterArray['RecordStatus'] !="ALL")){
			if($filterArray['RecordStatus'] == "SETTLED"){
				$condition_clause .= "  AND (ol.RecordStatus='SETTLED' OR ol.RecordStatus='WAIVED' OR ol.RecordStatus='OUTSTANDING')";
			}
			else{
				$condition_clause .= "  AND ol.RecordStatus='WAIVED' AND (ol.`Payment` - IF(ol.`PaymentReceived`is null,0,ol.`PaymentReceived`)) > 0";
			}
		}
	
	
		//Order + Limit
		if ($filterArray['findBy'] == 'Group'){
			$condition_clause .= " GROUP BY gu.`GroupID` ORDER BY u.`ClassName` ASC";
		}
		if ($filterArray['rankTarget'] == 'form'){
			$condition_clause .= " GROUP BY u.`ClassLevel` ORDER BY u.`ClassName` ASC";
		} 
		if ($filterArray['rankTarget'] == 'class'){
			$condition_clause .= " GROUP BY u.`ClassName` ORDER BY u.`ClassName` ASC";
		}
		
	//Henry Added 20131217
	if (($filterArray['RecordStatus'] !="")&&($filterArray['RecordStatus'] !="ALL")){
		if($filterArray['RecordStatus'] == "SETTLED"){		
			$sql = <<<EOF
			SELECT IF(u.`ClassLevel` is null or u.`ClassLevel`='', ' -- ', u.`ClassLevel`) as `ClassLevel`, 
				IF(u.`ClassName` is null or u.`ClassName`='', ' -- ', u.`ClassName`) as `ClassName`, 
			sum(ol.`PaymentReceived`) as payment FROM `LIBMS_OVERDUE_LOG` ol
			join `LIBMS_BORROW_LOG` bl on ol.`BorrowLogID` = bl.`BorrowLogID`
			JOIN `LIBMS_USER` u on u.`UserID` = bl.`UserID`
			JOIN `LIBMS_GROUP_USER` gu on u.`UserID` = gu.`UserID`
			WHERE
			{$condition_clause}
			
EOF;
		}
		else{
			$sql = <<<EOF
			SELECT IF(u.`ClassLevel` is null or u.`ClassLevel`='', ' -- ', u.`ClassLevel`) as `ClassLevel`, 
				IF(u.`ClassName` is null or u.`ClassName`='', ' -- ', u.`ClassName`) as `ClassName`, 
			(sum(ol.`Payment`) - sum(IF(ol.`PaymentReceived`is null,0,ol.`PaymentReceived`))) as payment FROM `LIBMS_OVERDUE_LOG` ol
			join `LIBMS_BORROW_LOG` bl on ol.`BorrowLogID` = bl.`BorrowLogID`
			JOIN `LIBMS_USER` u on u.`UserID` = bl.`UserID`
			JOIN `LIBMS_GROUP_USER` gu on u.`UserID` = gu.`UserID`
			WHERE
			{$condition_clause}
			
EOF;
		}
	}
	else{
		$sql = <<<EOF
		SELECT IF(u.`ClassLevel` is null or u.`ClassLevel`='', ' -- ', u.`ClassLevel`) as `ClassLevel`, 
			IF(u.`ClassName` is null or u.`ClassName`='', ' -- ', u.`ClassName`) as `ClassName`, 
		sum(ol.`Payment`) as payment FROM `LIBMS_OVERDUE_LOG` ol
		join `LIBMS_BORROW_LOG` bl on ol.`BorrowLogID` = bl.`BorrowLogID`
		JOIN `LIBMS_USER` u on u.`UserID` = bl.`UserID`
		JOIN `LIBMS_GROUP_USER` gu on u.`UserID` = gu.`UserID`
		WHERE
		{$condition_clause}
	
EOF;
	}
	
		$result =  $this->returnArray($sql,null,1);
		//$GLOBALS['DEBUG']['filterArray']=$filterArray;
		//$GLOBALS['DEBUG']['sql']=$sql;
		//$GLOBALS['DEBUG'][]=mysql_error();
		//$GLOBALS['DEBUG']['result']=$result;
		//dump($GLOBALS['DEBUG']);
		//error_log("report.php: 727: ". var_export($GLOBALS['DEBUG'],true));
		return $result;
	}
	
	############# CategoryReport #################
	public function getCategoryReport($filterArray){
	global $Lang;
		//date filter
		if (!empty($filterArray)){
			$condition_clause = "bl.`BorrowTime` >=  '{$filterArray['dateRange']['StartDate']}' AND bl.`BorrowTime` <=  '{$filterArray['dateRange']['EndDate']} 23:59:59' ";
		}else{
			$condition_clause ='1=1';
		}
//debug_r($filterArray);
		//// !!! Please note the case for first letter of "categories", 
		//// here it's lower case, which is different from other similar function		
		//BookCategory filter
		if (!empty($filterArray['categories'])){
			$join_book_category ="LEFT JOIN `LIBMS_BOOK_CATEGORY` bc
			on b.`BookCategoryCode` = bc.`BookCategoryCode` AND bc.`BookCategoryType` = ".$filterArray['BookCategoryType'];
			foreach ($filterArray['categories'] as &$Categories){
				$BookCategory = mysql_real_escape_string($Categories);
			}
			unset($BookCategory);
			$impoded_BookCategory = implode("','", $filterArray['categories']);
			$hasNullCatCode=0;
			if (in_array('', $filterArray['categories']))
			{
				$hasNullCatCode=1;
				$condition_clause .= " AND (b.`BookCategoryCode` IS NULL OR b.`BookCategoryCode`='' OR b.`BookCategoryCode` IN ('{$impoded_BookCategory}'))";
			}else{
				$condition_clause .= " AND b.`BookCategoryCode` IN ('{$impoded_BookCategory}')";
			}
			//Henry Added [20150122]
			if(!empty($filterArray['BookCategoryType'])){
				if(!$hasNullCatCode){
					$condition_clause .= " AND b.`BookCategoryCode` IN (SELECT C2.BookCategoryCode FROM LIBMS_BOOK_CATEGORY C2 WHERE C2.BookCategoryType='".$filterArray['BookCategoryType']."') ";
				}else{
					$condition_clause .= " AND (b.`BookCategoryCode` IS NULL OR b.`BookCategoryCode`='' OR b.`BookCategoryCode` IN (SELECT C2.BookCategoryCode FROM LIBMS_BOOK_CATEGORY C2 WHERE C2.BookCategoryType='".$filterArray['BookCategoryType']."')) ";
				}
			}
			$condition_clause .= " AND (bc.BookCategoryType IN (IFNULL((SELECT lbl2.BookCategoryType FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookLanguageCode = b.Language),1)) OR b.`BookCategoryCode` IS NULL OR b.`BookCategoryCode`='') ";
			//$abc =  " AND `BookCategoryCode` IN ('{$impoded_BookCategory}')";
			//tolog($abc);
		}
		if (!empty($filterArray['categories2'])){
			$join_book_category ="LEFT JOIN `LIBMS_BOOK_CATEGORY` bc
			on b.`BookCategoryCode` = bc.`BookCategoryCode` AND bc.`BookCategoryType` = ".$filterArray['BookCategoryType'];
			foreach ($filterArray['categories2'] as &$Categories){
				$BookCategory = mysql_real_escape_string($Categories);
			}
			unset($BookCategory);
			$impoded_BookCategory = implode("','", $filterArray['categories2']);
			$hasNullCatCode=0;
			if (in_array('', $filterArray['categories2']))
			{
				$hasNullCatCode=1;
				$condition_clause .= " AND (b.`BookCategoryCode` IS NULL OR b.`BookCategoryCode`='' OR b.`BookCategoryCode` IN ('{$impoded_BookCategory}'))";
			}else{
				$condition_clause .= " AND b.`BookCategoryCode` IN ('{$impoded_BookCategory}')";
			}
			//Henry Added [20150122]
			if(!empty($filterArray['BookCategoryType'])){
				if(!$hasNullCatCode){
					$condition_clause .= " AND b.`BookCategoryCode` IN (SELECT C2.BookCategoryCode FROM LIBMS_BOOK_CATEGORY C2 WHERE C2.BookCategoryType='".$filterArray['BookCategoryType']."') ";
				}else{
					$condition_clause .= " AND (b.`BookCategoryCode` IS NULL OR b.`BookCategoryCode`='' OR b.`BookCategoryCode` IN (SELECT C2.BookCategoryCode FROM LIBMS_BOOK_CATEGORY C2 WHERE C2.BookCategoryType='".$filterArray['BookCategoryType']."')) ";
				}
			}
			$condition_clause .= " AND (bc.BookCategoryType IN (IFNULL((SELECT lbl2.BookCategoryType FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookLanguageCode = b.Language),2)) OR b.`BookCategoryCode` IS NULL OR b.`BookCategoryCode`='') ";
			//$abc =  " AND `BookCategoryCode` IN ('{$impoded_BookCategory}')";
			//tolog($abc);
		}
//debug_r($filterArray['Subject']);
		//// !!! Please note the case for first letter of "subject", 
		//// here it's lower case, which is different from other similar function		
	
		//Subject filter
		if (!empty($filterArray['subject'])){
			foreach ($filterArray['subject'] as &$Subject){
				$Subject = mysql_real_escape_string($Subject);
			}
			unset($Subject);
			$impoded_Subject = implode("','", $filterArray['subject']);
			if (in_array('', $filterArray['subject']))
			{
				$condition_clause .= " AND (b.`Subject` IS NULL OR b.`Subject`='' OR b.`Subject` IN ('{$impoded_Subject}'))";
			}else{
				$condition_clause .= " AND b.`Subject` IN ('{$impoded_Subject}')";
			}
				
		}
	
		//Language filter
		if (!empty($filterArray['language'])){
			foreach ($filterArray['language'] as &$language){
				$language = mysql_real_escape_string($language);
			}
			unset($language);
			$impoded_Language = implode("','", $filterArray['language']);
			if (in_array('', $filterArray['language']))
			{
				$condition_clause .= " AND (b.`Language` IS NULL OR b.`Language`='' OR b.`Language` IN ('{$impoded_Language}'))";
			}else{
				$condition_clause .= " AND b.`Language` IN ('{$impoded_Language}')";
			}
			
		}

		// user condition
		$condition_clause .= " AND u.`RecordStatus`<>0";	// case #P116646
		
		//displayTotal filter
		//if ($filterArray['displayTotal'] == 'Gender'){
			$sql_M_count = "IF(SUM(u.Gender = 'M') > 0, SUM(u.Gender = 'M'), 0)";
			$sql_F_count = "IF(SUM(u.Gender = 'F') > 0, SUM(u.Gender = 'F'), 0)";
		//}
		//else
			$sql_count = "count(*)";
			
		//Order + Limit
		if ($filterArray['findBy_cat'] == 'categories' || $filterArray['findBy_cat'] == 'categories2')
			$findBy_cat = "case when b.`BookCategoryCode`='' then '^=Unde=^' else ifnull(b.`BookCategoryCode`,'^=Unde=^') end";			
		else if ($filterArray['findBy_cat'] == 'subject')
			$findBy_cat = "case when b.`Subject`='' then '^=Unde=^' else ifnull(b.`Subject`,'^=Unde=^') end";
		else
			$findBy_cat = "case when b.`Language`='' then '^=Unde=^' else ifnull(b.`Language`,'^=Unde=^') end";
			
		$condition_clause_groupby = " Group By ".$findBy_cat;

		
/*		
		$sql_template = <<<EOF
		SELECT
		count(*) as Total, {$findBy_cat} as GroupBy, u.`%s` as Class
		FROM `LIBMS_BORROW_LOG` bl
		JOIN `LIBMS_BOOK` b
		on bl.`BookID` = b.`BookID`
		JOIN `LIBMS_BOOK_CATEGORY` bc
		on b.`BookCategoryCode` = bc.`BookCategoryCode`
		JOIN `LIBMS_USER` u
		on u.`UserID` = bl.`UserID`
		JOIN `LIBMS_GROUP_USER` gu
		on u.`UserID` = gu.`UserID`
		LEFT JOIN `LIBMS_OVERDUE_LOG` ol
		on bl.`BorrowLogID` = ol.`BorrowLogID`
		WHERE
		{$condition_clause}
		AND u.`%s` = '%s'
		{$condition_clause_groupby}
EOF;
*/
		$sql_template1 = <<<EOF
		SELECT
		{$sql_count} as Total, {$sql_M_count} as GenderMTotal, {$sql_F_count} as GenderFTotal, {$findBy_cat} as GroupBy, u.`%s` as Class
EOF;
		$sql_template2 = <<<EOF
		FROM `LIBMS_BORROW_LOG` bl
		JOIN `LIBMS_BOOK` b
		on bl.`BookID` = b.`BookID`
		{$join_book_category}
		JOIN `LIBMS_USER` u
		on u.`UserID` = bl.`UserID`	
		WHERE
		{$condition_clause}
EOF;
		$sql_template3 = <<<EOF
		AND u.`%s` = '%s'
		{$condition_clause_groupby}
EOF;

//debug($sql_template);
	
	if  ($filterArray['rankTarget']=='form'){
		foreach ($filterArray['classname'] as &$classname){
			$fieldname = "ClassLevel";
			$sql[] = sprintf($sql_template1,$fieldname).$sql_template2.sprintf($sql_template3,$fieldname,$classname);
		}
		unset($classname);
	}else{
		foreach ($filterArray['classname'] as &$classname){
			$fieldname = "ClassName";
			$sql[] = sprintf($sql_template1,$fieldname).$sql_template2.sprintf($sql_template3,$fieldname,$classname);
		}
		unset($classname);
	}
		
		foreach ($sql as $value){
//debug_r($value);			
		$result[] =  $this->returnArray($value,null,1);
//debug_r($result);		
		}
		$GLOBALS['DEBUG']['filterArray']=$filterArray;
		$GLOBALS['DEBUG']['sql']=$sql;
		$GLOBALS['DEBUG']['result']=$result;
		//$GLOBALS['DEBUG'][]=mysql_error();
		//dump($GLOBALS['DEBUG']);
		//error_log("report.php: 727: ". var_export($GLOBALS['DEBUG'],true));
		return $result;
	}
	
	#############
	public function getSubjectArray(){
		global $Lang;
		$sql = <<<EOF
			SELECT `Subject` , IF(`Subject` <> '' , `Subject` ,'{$Lang["libms"]["report"]["cannot_define"]}') as SubjectName FROM `LIBMS_BOOK` WHERE RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL GROUP BY `Subject` ORDER BY `Subject`
EOF;
		$result =  $this->returnArray($sql,null,2);

		if (!empty($result))
			foreach($result as $item)
			$formated_Array[$item[0]]=$item[1];
		//tolog($formated_Array);
		return $formated_Array;
	}
	#############
	public function getLanguageArray(){
		global $Lang;
//		$sql = <<<EOF
//			SELECT `Language` ,IF(`Language` <> '' , `Language` ,'{$Lang["libms"]["report"]["cannot_define"]}') as LanguageName  FROM `LIBMS_BOOK` WHERE RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL GROUP BY `Language` ORDER BY `Language`
//EOF;
//		$result =  $this->returnArray($sql,null,2);
//		if (!empty($result))
//			foreach($result as $item)
//			$formated_Array[$item[0]]=$item[1];
		//tolog($sql);
		//tolog($formated_Array);
		
		$sql = "SELECT DISTINCT b.`Language`, IF(b.`Language`<>'', ifnull(a.".$Lang["libms"]["sql_field"]["Description"].",b.`Language`), 
					 '".$Lang["libms"]["report"]["cannot_define"]."') as LanguageName 
			FROM
				`LIBMS_BOOK` b
			LEFT JOIN LIBMS_BOOK_LANGUAGE a ON a.BookLanguageCode=b.Language 
			WHERE
				(b.RecordStatus NOT LIKE 'DELETE' OR b.RecordStatus IS NULL)
			ORDER by b.`Language`";
		$result = $this->returnResultSet($sql);

		if (!empty($result))
			foreach($result as $item)
			$formated_Array[$item['Language']]=$item['LanguageName'];
		
		return $formated_Array;
	}
	#########
	public function getCirCategoryArray($BookCategoryType=0){
		global $Lang;
		
		if($BookCategoryType!=0){
			$cond =" AND BookCategoryType = '".$BookCategoryType."' ";
		}
		$sql = <<<EOF
			SELECT '' as BookCategoryCode,'{$Lang["libms"]["report"]["cannot_define"]}' as BookCategoryDesc
		union 
			SELECT 	`BookCategoryCode` , `{$Lang['libms']['sql_field']['Description']}` as BookCategoryDesc 
			FROM 	`LIBMS_BOOK_CATEGORY` where BookCategoryCode<>'' and BookCategoryCode is not null {$cond}
			ORDER BY `BookCategoryCode`
EOF;
//// Commented by Cameron
/*		
		$sql = <<<EOF
			SELECT `BookCategoryCode`,`{$Lang['libms']['sql_field']['Description']}` FROM `LIBMS_BOOK_CATEGORY` ORDER BY `BookCategoryCode`
EOF;
*/
		$result = $this->returnArray($sql,null,2);
		if (!empty($result))
			foreach($result as $item)
			$formated_Array[$item[0]]=$item[1];

		return $formated_Array;
	}
		#########
	public function getCirculationArray(){
		global $Lang;
		$sql = <<<EOF
			SELECT '' as CirculationTypeCode,'{$Lang["libms"]["report"]["cannot_define"]}' as CirculationTypeDesc
		union 
			SELECT 	`CirculationTypeCode` , `{$Lang['libms']['sql_field']['Description']}` as CirculationTypeDesc 
			FROM 	`LIBMS_CIRCULATION_TYPE` where CirculationTypeCode<>'' and CirculationTypeCode is not null 
			ORDER BY `CirculationTypeCode`
EOF;
		
//// Commented by Cameron
/*		
		$sql = <<<EOF
			SELECT `CirculationTypeCode`,`{$Lang['libms']['sql_field']['Description']}` FROM `LIBMS_CIRCULATION_TYPE` ORDER BY `CirculationTypeCode`
EOF;
*/
		$result = $this->returnArray($sql,null,2);
		if (!empty($result))
			foreach($result as $item)
			$formated_Array[$item[0]]=$item[1];

		return $formated_Array;
	}
	
	public function getGroupNameArray(){
		global $Lang;
		$sql = <<<EOF
			SELECT `GroupID`,`GroupTitle` FROM `LIBMS_GROUP` ORDER BY `GroupTitle`
EOF;
		$result = $this->returnArray($sql,null,2);
		//var_dump($result);
		if (!empty($result))
			foreach($result as $item)
			$formated_Array[$item[0]]=$item[1];
	
		return $formated_Array;
	}
	
	public function getClassLevelArray(){
		global $Lang;
		$sql = <<<EOF
			SELECT `ClassLevel` 
			FROM `LIBMS_USER` 
			WHERE `UserType` = 'S' 
				AND RecordStatus = '1' 
			GROUP BY `ClassLevel` 
			ORDER BY `ClassLevel`
EOF;
		$result = $this->returnArray($sql,null,2);
		if (!empty($result))
			foreach($result as $item)
			$formated_Array[$item[0]]=$item[0];
	
		return $formated_Array;
	}
	
	public function getClassNameArrayByClassLevel(){
		global $Lang;
				
		$sql = <<<EOF
			SELECT `ClassName` 
			FROM `LIBMS_USER`
			WHERE `UserType` = 'S'
				AND RecordStatus = '1'
			GROUP BY `ClassName`
			ORDER BY `ClassName`
			
EOF;
		$result = $this->returnArray($sql,null,2);
		if (!empty($result))
			foreach($result as $item)
			$formated_Array[$item[0]]=$item[0];
		
		return $formated_Array;
	}
	

	public function getStudentArrayByClassName($classnames){
		global $Lang;
		if (empty($classnames)){
			return;
		}

		$classnames2 = iconv("BIG5", "UTF8", $classnames);
		
		$sql = <<<EOF
			SELECT `UserID`, `{$Lang['libms']['sql_field']['User']['name']}` as Name
			FROM `LIBMS_USER` 
			WHERE `ClassName`  IN ( '$classnames', '$classnames2' ) AND RecordStatus = '1'
			ORDER BY `ClassName`,`ClassNumber`,`Name`
EOF;

		$result = $this->returnArray($sql,null,2);
		if (!empty($result))
			foreach($result as $item)
			$formated_Array[$item[0]]=$item[1];
		
		return $formated_Array;
		
	}

	/*
	 * 	Consolidate Book Category Filter 
	 * 	@return: string for sql
	 */
	private function getBookCategoryFilter($filterArray) {
		$condition_clause = "";
		//BookCategory filter
		if (!empty($filterArray['Categories']) && ($filterArray['BookCategoryType'] != 0)){
			foreach ($filterArray['Categories'] as &$Categories){
				$BookCategory = mysql_real_escape_string($Categories);
			}
			unset($BookCategory);
			$impoded_BookCategory = implode("','", $filterArray['Categories']);
			$hasNullCatCode= 0;
			if (in_array('', $filterArray['Categories']))
			{
//				$hasNullCatCode = 1;
				$condition_clause .= " AND (`BookCategoryCode` IS NULL OR `BookCategoryCode`='' OR `BookCategoryCode` IN ('{$impoded_BookCategory}'))";
			}else{
				$condition_clause .= " AND `BookCategoryCode` IN ('{$impoded_BookCategory}')";
			}
			
			//Henry Added [20150122]
			if(!empty($filterArray['BookCategoryType'])){
				
//				if(!$hasNullCatCode){
//					$condition_clause .= ' AND (Language IN (SELECT lbl2.BookLanguageCode FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookCategoryType = "'.$filterArray['BookCategoryType'].'") OR '.$filterArray['BookCategoryType'].' = 1 AND (Language IS NULL OR Language = "")) ';
//				}else{
					$condition_clause .= ' AND (Language IN (SELECT lbl2.BookLanguageCode FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookCategoryType = "'.$filterArray['BookCategoryType'].'") OR Language IS NULL OR Language = "") ';	
//				}
			}
		}
		return $condition_clause;
	}
	
	public function getGroupMemberArray($groupIDs, $cond=''){
		global $Lang;
		if (empty($groupIDs)){
			return;
		}
		$cond = ($cond == '') ? ' and u.`RecordStatus`<>0' : '';
		
		$sql = <<<EOF
			SELECT u.`UserID`, u.`{$Lang['libms']['sql_field']['User']['name']}` as Name
			FROM `LIBMS_USER` u, `LIBMS_GROUP_USER` gu, `LIBMS_GROUP` g
			WHERE u.UserID=gu.UserID
			and gu.GroupID=g.GroupID
			and g.`GroupID` IN ('$groupIDs')
			$cond
			ORDER BY `Name`
EOF;

		$result = $this->returnArray($sql,null,2);	// numeric array only
		if (!empty($result))
			foreach($result as $item)
			$formated_Array[$item[0]]=$item[1];
		
		return $formated_Array;
		
	}
	
}
