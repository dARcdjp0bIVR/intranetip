<?php

//	Using: 

/***************************************
 * 
 * Date:    2018-06-25 [Cameron]
 * Details: - change countingMethod to countingMode [case #S139804]
 *          - add countingMethod filter: byVisit and byBooks
 *          - include jquery/1.4.4 in header to avoid conflict with 1.3.2, hence scrollbar can move back to top when select all Group/ClassLevel/Classes
 * 
 * Date:	2018-06-04 [Henry]
 * Details:	don't count deleted student record (RecordStatus=0) [Case#P139903]
 * 
 * Date:	2017-08-02 [Cameron]
 * Details:	the statistics should include left and suspend student as the borrow action is in the past and this report is couted to class / group / form level only
 * 			[ej 5.0.7.8.1 DM872] <- void after 2018-06-04 because it should be consistent with other report, e.g. Inactive Patron 		
 * 
 * Date:	2017-07-18 [Henry]
 * Details:	added counting method option [Case#H116665]
 * 
 * Date:	2016-06-26 (Cameron) (ej5.0.7.8.1)
 * Details:	- fix bug: don't show deleted student record (RecordStatus=0) [case #P116646] <- void after 2017-08-02
 *  
 * Date:	2016-08-01 [Cameron]
 * 			- hide the warning and empty option list if it returns nothing in ajaxSelectbox
 * 			- fix bug: preg_replace non-word character already include dash '-' and space
 * Date:	2016-07-15 (Cameron)
 * Details:	- fix bug: cast array before foreach for $PreClassName
 * 			- fix bug: don't allow to run report for invalid dateformat
 * 			- fix bug: if classlevel is selected, should select at least one class 
 * Date:	2015-07-03 (Henry)
 * Details:	fix: The number of total borrowed
 * Date:	2014-04-25 (Henry)
 * Details:	Created this file
 * 
 ***************************************/
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/TimeManager.php");

include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libstudentpromotion.php");
include_once($PATH_WRT_ROOT."includes/libmanagehistory.php");

intranet_auth();
intranet_opendb();

$Lang['General']['EmptySymbol'] = "&nbsp;";

$lmh = new libmanagehistory();
$libms = new liblms();

if(!$_SESSION['LIBMS']['admin']['current_right']['statistics']['frequency by form'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$libms = new liblms();
$home_header_no_EmulateIE7 = true;
$linterface = new interface_html();

# Current Page Info
$CurrentPageArr['LIBMS'] = 1;
$TAGS_OBJ[] = array($Lang["libms"]["reporting"]["ReadingFrequency"]);
$CurrentPage = "PageReportingFrequenceReport";
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

## Obtain POST Value
$StartDate = $_POST['StartDate'];
$EndDate = $_POST['EndDate'];
$findBy = $_POST['findBy'];
$displayTotal = $_POST['displayTotal'];

if($StartDate=='' || $EndDate==''){
	header('Location: index.php');	
}

$GroupTitle = $_POST['GroupTitle'];
$ClassLevel = $_POST['ClassLevel'];
$ClassName = $_POST['ClassName'];

$countingMethod = $_POST['countingMethod'];
$daysCalculation = $_POST['daysCalculation'];

// --------------------------------------------------------------------------------------------------------- //

$thisStockTakeSchemeDisplay = $StartDate  . ' ' . $Lang['General']['To']  . ' ' .  $EndDate;

$linterface->LAYOUT_START();

########################## DB Table Start ###############################

if($findBy == 'Group'){
	$UserList = array();
	for($i=0; $i<count($GroupTitle);$i++){
		$sql = 'select
					gu.UserID
				from
					LIBMS_GROUP_USER gu 
				join 
					LIBMS_USER u 
				on 
					gu.UserID = u.UserID
				where
					u.RecordStatus = 1 AND 
					gu.GroupID = \''.$GroupTitle[$i].'\'';			
		$result = $libms->returnArray($sql);
		
	foreach($result as $aResult)
		$UserList[$GroupTitle[$i]][]=$aResult['UserID'];
	}
	
}else if($findBy == 'Form'){
	$UserList = array();
	for($i=0; $i<count($ClassLevel);$i++){
		$sql = "select
					UserID
				from
					LIBMS_USER
				where
				RecordStatus = '1' AND 
				ClassLevel = '$ClassLevel[$i]'";			
		$result = $libms->returnArray($sql);
		
	foreach($result as $aResult)
		$UserList[$ClassLevel[$i]][]=$aResult['UserID'];
	}
}else if($findBy == 'Class'){
	
	$UserList = array();
	
	if($PreYearList){
		foreach((array)$PreClassName as $aClassName){
			$aryStudentList = $lmh->getStudentListByClass($PreYearList,$aClassName,$includeLeftStudent=true);
			foreach($aryStudentList as $aStudent)
				$UserList[$aClassName][]=$aStudent['id'];
		}
		//debug_pr($UserList);
	}
	else{
		for($i=0; $i<count($ClassName);$i++){
			$sql = "select
						UserID
					from
						LIBMS_USER
					where
					RecordStatus = '1' AND 
					ClassName = '$ClassName[$i]'";			
			$result = $libms->returnArray($sql);
			
		foreach($result as $aResult)
			$UserList[$ClassName[$i]][]=$aResult['UserID'];
		}
	}
}

// by number of books (material) borrowed
if ($countingMethod == 'byBooks'){
    $sql = "SELECT UserID, COUNT(*) as BorrowTimes
			FROM LIBMS_BORROW_LOG
			WHERE UNIX_TIMESTAMP(BorrowTime) >= UNIX_TIMESTAMP('".$StartDate."')
			AND UNIX_TIMESTAMP(BorrowTime) <= UNIX_TIMESTAMP('".$EndDate." 23:59:59')
			GROUP BY UserID";
}
else{
    // borrow books within 10 minutes is counted as one time
    $sql = "SELECT UserID, COUNT( distinct SUBSTRING(BorrowTime,1,15)) as BorrowTimes
    		FROM LIBMS_BORROW_LOG
    		WHERE UNIX_TIMESTAMP(BorrowTime) >= UNIX_TIMESTAMP('".$StartDate."')
    		AND UNIX_TIMESTAMP(BorrowTime) <= UNIX_TIMESTAMP('".$EndDate." 23:59:59')
    		GROUP BY UserID";
}
		
$result = $libms->returnArray($sql);

if (!empty($result))
	foreach($result as $item)
	$formated_Array[$item[0]]=$item[1];
	
//debug_pr($formated_Array);

$table2dArray = array();

$tempArr['1weekormore'] = 0;
$tempArr['2week'] = 0;
$tempArr['1month'] = 0;
$tempArr['1monthless'] = 0;
$tempArr['none'] = 0;
$tempArr['total'] = 0;
$tempArr['totalTimes'] = 0;

//calcuate the group with the range of borrowed time
$StartTime = strtotime($StartDate);
$EndTime = strtotime($EndDate);
$datediff = $EndTime - $StartTime;
if($daysCalculation == 'OpenDays'){
	$TM = new TimeManager();
	$tempDate = $StartDate;
	$datediff = 0;
	while($tempDate <= $EndDate){
		if($TM->isOpen(strtotime($tempDate))){
			$datediff++;
		}
		$tempDate = date('Y-m-d', strtotime($tempDate . ' +1 day'));
	}
	$datediff = $datediff*60*60*24;
}
$selectedDays = floor($datediff/(60*60*24));
$range_1weekormore = floor($selectedDays/7);
$range_2week = floor($selectedDays/14);
$range_1month =  floor($selectedDays/28);

foreach($UserList as $key => $value){
	
	$table2dArray[$key]['1weekormore'] = 0;
	$table2dArray[$key]['2week'] = 0;
	$table2dArray[$key]['1month'] = 0;
	$table2dArray[$key]['1monthless'] = 0;
	$table2dArray[$key]['none'] = 0;
	$table2dArray[$key]['total'] = 0;
	$table2dArray[$key]['totalTimes'] = 0;
	
	$totalUserArr[$key] = 0;
	foreach($value as $aValue){
		$totalUserArr[$key] = count($value);
		
		if(!isset($formated_Array[$aValue])){
			$table2dArray[$key]['none']++;
			$tempArr['none']++;
		}
		else if($formated_Array[$aValue] >= $range_1weekormore){
			$table2dArray[$key]['1weekormore']++;
			$tempArr['1weekormore']++;
			$table2dArray[$key]['total']++;
			$table2dArray[$key]['totalTimes'] += $formated_Array[$aValue];
			$tempArr['total']++;
			$tempArr['totalTimes'] += $formated_Array[$aValue];
		}
		else if($formated_Array[$aValue] >= $range_2week){
			$table2dArray[$key]['2week']++;
			$tempArr['2week']++;
			$table2dArray[$key]['total']++;
			$table2dArray[$key]['totalTimes'] += $formated_Array[$aValue];
			$tempArr['total']++;
			$tempArr['totalTimes'] += $formated_Array[$aValue];
		}
		else if($formated_Array[$aValue] >= $range_1month){
			$table2dArray[$key]['1month']++;
			$tempArr['1month']++;
			$table2dArray[$key]['total']++;
			$table2dArray[$key]['totalTimes'] += $formated_Array[$aValue];
			$tempArr['total']++;
			$tempArr['totalTimes'] += $formated_Array[$aValue];
		}
		else{
			$table2dArray[$key]['1monthless']++;
			$tempArr['1monthless']++;
			$table2dArray[$key]['total']++;
			$table2dArray[$key]['totalTimes'] += $formated_Array[$aValue];
			$tempArr['total']++;
			$tempArr['totalTimes'] += $formated_Array[$aValue];
		}
	}
}
$table2dArray['schoolTotal']['1weekormore'] = $tempArr['1weekormore'];
$table2dArray['schoolTotal']['2week'] = $tempArr['2week'];
$table2dArray['schoolTotal']['1month'] = $tempArr['1month'];
$table2dArray['schoolTotal']['1monthless'] = $tempArr['1monthless'];
$table2dArray['schoolTotal']['none'] = $tempArr['none'];
$table2dArray['schoolTotal']['total'] = $tempArr['total'];
$table2dArray['schoolTotal']['totalTimes'] = $tempArr['totalTimes'];
$totalUserArr['schoolTotal'] = $tempArr['none']+$tempArr['total'];

//debug_pr($table2dArray);
//debug_pr($totalUserArr);

$ChartTitle = array();

$displayTable = '<table class="common_table_list view_table_list">';

$displayTable .= '<tr>';
$displayTable .= '<th>&nbsp;</th>';
foreach($table2dArray as $key => $value){
	$tempKey = $key;
	if($findBy == 'Group' && $key != 'schoolTotal'){
		$sql = "Select
				    GroupTitle
				From
				    LIBMS_GROUP
				Where
					GroupID = '$key'";
				    
			$key = current($libms->returnArray($sql));
			$key = $key['GroupTitle'];
	}
	if($key == 'schoolTotal'){
		$key = $Lang['libms']['bookmanagement']['total'];
	}
	$displayTable .= '<th>'.$key.'</th>';
	$ChartTitle[$tempKey] = $key;
}

$ChartTitle['schoolTotal'] = $Lang['libms']['bookmanagement']['overall'];

$displayTable .= '</tr>';

$displayTable .= '<tr>';
$displayTable .= '<td nowrap="nowrap">'.$Lang['libms']['report']['1weekormore'].'</td>';
foreach($table2dArray as $key => $value){
	if($displayTotal == "Percentage"){
		$displayTable .= '<td>'.number_format(@round($value['1weekormore']/$totalUserArr[$key]*100, 2), 2, '.', '').'%</td>';
	}
	else{
		$displayTable .= '<td>'.$value['1weekormore'].'</td>';
	}
}
$displayTable .= '</tr>';

$displayTable .= '<tr>';
$displayTable .= '<td nowrap="nowrap">'.$Lang['libms']['report']['2week'].'</td>';
foreach($table2dArray as $key => $value){
	if($displayTotal == "Percentage"){
		$displayTable .= '<td>'.number_format(@round($value['2week']/$totalUserArr[$key]*100, 2), 2, '.', '').'%</td>';
	}
	else{
		$displayTable .= '<td>'.$value['2week'].'</td>';
	}
}
$displayTable .= '</tr>';

$displayTable .= '<tr>';
$displayTable .= '<td nowrap="nowrap">'.$Lang['libms']['report']['1month'].'</td>';
foreach($table2dArray as $key => $value){
	if($displayTotal == "Percentage"){
		$displayTable .= '<td>'.number_format(@round($value['1month']/$totalUserArr[$key]*100, 2), 2, '.', '').'%</td>';
	}
	else{
		$displayTable .= '<td>'.$value['1month'].'</td>';
	}
}
$displayTable .= '</tr>';

$displayTable .= '<tr>';
$displayTable .= '<td nowrap="nowrap">'.$Lang['libms']['report']['1monthless'].'</td>';
foreach($table2dArray as $key => $value){
	if($value['none'] == 0 && $value['1monthless'] != 0){
		if($displayTotal == "Percentage"){
			$value['1monthless'] = 100 - @round($value['1weekormore']/$totalUserArr[$key]*100, 2) - @round($value['2week']/$totalUserArr[$key]*100, 2) - @round($value['1month']/$totalUserArr[$key]*100, 2); 
			$displayTable .= '<td>'.number_format($value['1monthless'], 2, '.', '').'%</td>';
		}
		else{
			$displayTable .= '<td>'.$value['1monthless'].'</td>';
		}
	}
	else{
		if($displayTotal == "Percentage"){
			$displayTable .= '<td>'.number_format(@round($value['1monthless']/$totalUserArr[$key]*100, 2), 2, '.', '').'%</td>';
		}
		else{
			$displayTable .= '<td>'.$value['1monthless'].'</td>';
		}
	}
}
$displayTable .= '</tr>';

$displayTable .= '<tr>';
$displayTable .= '<td nowrap="nowrap">'.$Lang['libms']['report']['none'].'</td>';
foreach($table2dArray as $key => $value){
	if($value['none'] > 0){
		if($displayTotal == "Percentage"){
			$value['none'] = 100 - @round($value['1weekormore']/$totalUserArr[$key]*100, 2) - @round($value['2week']/$totalUserArr[$key]*100, 2) - @round($value['1month']/$totalUserArr[$key]*100, 2) - @round($value['1monthless']/$totalUserArr[$key]*100, 2); 
			$displayTable .= '<td>'.number_format($value['none'], 2, '.', '').'%</td>';
		}
		else{
			$displayTable .= '<td>'.$value['none'].'</td>';
		}
	}
	else{
		if($displayTotal == "Percentage"){
			$displayTable .= '<td>'.number_format(@round($value['none']/$totalUserArr[$key]*100, 2), 2, '.', '').'%</td>';
		}
		else{
			$displayTable .= '<td>'.$value['none'].'</td>';
		}
	}
}
$displayTable .= '</tr>';

$displayTable .= '<tr class="row_avaliable">';
$displayTable .= '<td nowrap="nowrap">'.$Lang['libms']['report']['totalTimesOfBorrow'].'</td>';
foreach($table2dArray as $key => $value){
	$displayTable .= '<td>'.$value['totalTimes'].'</td>';
}
$displayTable .= '</tr>';

$displayTable .= '</table>';

$displayPieChart = '<span class="sectiontitle">'.$Lang['libms']['report']['totalTimesOfBorrow'].': '.$value['totalTimes'].'</span><br/>';

$displayPieChart .= '<table>';
$i=0;
foreach($table2dArray as $key => $value){
	if($i%3 == 0)
		$displayPieChart .= '<tr>';
//	$displayPieChart .= '<td><div id="pieChart_'.str_replace(' ', '', preg_replace('/[^A-Za-z0-9\-]/', '_', $key)).'" style="margin-top:0px; margin-left:0px; width:250px; height:250px;"></div></td>';
	$displayPieChart .= '<td><div id="pieChart_'.preg_replace('/[^A-Za-z0-9]/', '_', $key).'" style="margin-top:0px; margin-left:0px; width:250px; height:250px;"></div></td>';
	if($i%3 == 2)
		$displayPieChart .= '</tr>';
	$i++;
}
$displayPieChart .= '</table>';
########################## DB Table End ###############################
?>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> -->
<script language="javascript" type="text/javascript" src="../js/jquery.jqplot.min.js"></script>
<link rel="stylesheet" type="text/css" href="../css/jquery.jqplot.css" />
<script type="text/javascript" src="../js/jqplot.pieRenderer.min.js"></script>
<script language="javascript">
$(document).ready(function(){	
	$('#spanShowOption').show();
	
	$( 'input[name="findBy"]:radio' ).click(
		function(){
       		//alert($('input[name="findBy"]:checked').val());
       		if($('input[name="findBy"]:checked').val() == 'Group'){
       			$('#tr_GroupList').show();
       			$('#tr_FormList').hide();
       			$('#tr_ClassList').hide();
       		}else if($('input[name="findBy"]:checked').val() == 'Form'){
       			$('#tr_GroupList').hide();
       			$('#tr_FormList').show();
       			$('#tr_ClassList').hide();
       		}else if($('input[name="findBy"]:checked').val() == 'Class'){
       			$('#tr_GroupList').hide();
       			$('#tr_FormList').hide();
       			$('#tr_ClassList').show();
       		}
   	 	}
	);	
	
	//for the chart drawing
	<?foreach($table2dArray as $key => $value){ ?>
			
		var s1 = [['<?=$Lang['libms']['report']['1weekormore']?>',<?=$value['1weekormore']?>], ['<?=$Lang['libms']['report']['2week']?>',<?=$value['2week']?>], ['<?=$Lang['libms']['report']['1month']?>',<?=$value['1month']?>], ['<?=$Lang['libms']['report']['1monthless']?>',<?=$value['1monthless']?>], ['<?=$Lang['libms']['report']['none']?>', <?=$value['none']?>]];
	        
//	    var pieChart_<?=str_replace(' ', '', preg_replace('/[^A-Za-z0-9\-]/', '_', $key))?> = $.jqplot('pieChart_<?=str_replace(' ', '', preg_replace('/[^A-Za-z0-9\-]/', '_', $key))?>', [s1], {
    	var pieChart_<?=preg_replace('/[^A-Za-z0-9]/', '_', $key)?> = $.jqplot('pieChart_<?=preg_replace('/[^A-Za-z0-9]/', '_', $key)?>', [s1], {
	    	seriesColors: [ "#DD1000", "#FF6800", "#FFCC01", "#73AE14", "#1B9FDD"],
	        grid: {
	            drawBorder: false, 
	            drawGridlines: false,
	            background: '#ffffff',
	            shadow:false
	        },
	        axesDefaults: {
	            
	        },
	        seriesDefaults:{
	            renderer:$.jqplot.PieRenderer,
	            rendererOptions: {
	                showDataLabels: true,
	                dataLabelFormatString: '%.2f%%'
	            }
	        },
	        legend: {
	            show: false,
	            rendererOptions: {
	                numberCols: 1
	            },
	            location: 'e'
	        },
	        title: {
	            show: true,
	            text: "<?=$ChartTitle[$key]?>",
	            fontSize: '18px'
	        },
	        fontSize: '18px'
	    }); 
    
	<?}?>
	
	var s1 = [['<?=$Lang['libms']['report']['1weekormore']?>',<?=$value['1weekormore']?>], ['<?=$Lang['libms']['report']['2week']?>',<?=$value['2week']?>], ['<?=$Lang['libms']['report']['1month']?>',<?=$value['1month']?>], ['<?=$Lang['libms']['report']['1monthless']?>',<?=$value['1monthless']?>], ['<?=$Lang['libms']['report']['none']?>', <?=$value['none']?>]];
	        
	    var pieLegend = $.jqplot('pieLegend', [s1], {
	    	seriesColors: [ "#DD1000", "#FF6800", "#FFCC01", "#73AE14", "#1B9FDD"],
	        grid: {
	            drawBorder: false, 
	            drawGridlines: false,
	            background: '#ffffff',
	            shadow:false
	        },
	        axesDefaults: {
	            
	        },
	        seriesDefaults:{
	            renderer:$.jqplot.PieRenderer,
	            rendererOptions: {
	                showDataLabels: true
	            }
	        },
	        legend: {
	            show: true,
	            rendererOptions: {
	                numberCols: 1
	            },
	            location: 'e',
	             fontSize: '14px'
	        },
	        title: {
	            show: false,
	            text: ""
	        }
	    });
	
	window.scrollTo(0, 0);
	$('.result_toggler').click(function(){
		if(this.className == 'result_toggler thumb_list_tab_on')
   			return false;
		$('.result_toggler').toggleClass('thumb_list_tab_on');
		$('#result_stat').toggle();
		$('#result_chart').toggle();
		return false;
	});

	$('#result_chart').toggle();	
});

function hideOptionLayer()
{
	$('#formContent').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
	//document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer()
{
	$('#formContent').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
	//document.getElementById('div_form').className = 'report_option report_show_option';
}

function Js_Select_All(id){
	$('#'+id + ' option').attr('selected', 'selected');	
}

function Js_Check_Form(obj){
	var error = 0 ;
	var stocktakeLength = $('#stocktakeStatus option:selected').length;
	
	if($('input[name="findBy"]:checked').val() == 'Group' && $('#GroupTitle option:selected').length == 0){
		document.getElementById("GroupTitle").focus();
		alert('<?=$Lang['libms']['report']['msg']['please_select_group']?>');
		error++;
	}else if($('input[name="findBy"]:checked').val() == 'Form' && $('#ClassLevel option:selected').length == 0){
		document.getElementById("ClassLevel").focus();
		alert('<?=$Lang['libms']['report']['msg']['please_select_form']?>');
		error++;
	}else if($('input[name="findBy"]:checked').val() == 'Class' && $('#PreYearList :selected').val()=='' && $('#ClassName option:selected').length == 0){
		document.getElementById("ClassName").focus();
		alert('<?=$Lang['libms']['report']['msg']['please_select_class']?>');
		error++;
	}else if($('input[name="findBy"]:checked').val() == 'Class' && $('#PreYearList :selected').val()!='' && $('#PreClassName option:selected').length == 0){
		document.getElementById("PreClassName").focus();
		alert('<?=$Lang['libms']['report']['msg']['please_select_class']?>');
		error++;
	}else if (!check_date(document.getElementById("StartDate"),"<?=$Lang['General']['InvalidDateFormat']?>")) {
		error++;
	}else if (!check_date(document.getElementById("EndDate"),"<?=$Lang['General']['InvalidDateFormat']?>")) {
		error++;
	}else if(document.getElementById("StartDate").value > document.getElementById('EndDate').value){
		document.getElementById("StartDate").focus();
		alert("<?=$Lang['General']['WrongDateLogic']?>");
		error++;
	}
	
	if(error==0)
	{
		obj.submit();
	}
}

function ajaxSelectbox(page, fieldName){
	//alert("haha");
	$.get(page, {},
		function(insertRespond){
			if (insertRespond == ''){
//				alert('Ajax call return nothing. Please try again. If problem persist, make sure the setting is completed / contact your administrator');
				fieldName.html('');
			}else {
				//alert("haha");
				fieldName.html(insertRespond);
				$('#PreClassName option').attr('selected', true);
				$('#ClassList').hide();
				$('#PreClassList').show();
			}
		}, "json"
	);
}
		
function class_list(){
	//alert($('#PreYearList :selected').text());
	if($('#PreYearList :selected').val()){
		page = "ajax_get_pre_class_list.php?PreYear="+$('#PreYearList :selected').text();
		ajaxSelectbox(page, $('#PreClassList'));
	}
	else{
		$('#PreClassList').hide();
		$('#ClassName option').attr('selected', true);
		$('#ClassList').show();
	}
}

function click_export()
{
	document.form1.action = "export.php";
	document.form1.target = "_blank";
	document.form1.submit();
	
	document.form1.action = "result.php";
	document.form1.target="_self";
}

function click_print()
{
	if ($('#result_stat').css('display') == 'none')
		document.form1.action="print.php?show=chart";
	else
		document.form1.action="print.php";
	document.form1.target="_blank";
	document.form1.submit();
	
	document.form1.action = "result.php";
	document.form1.target="_self";
}	
</script>

<form name="form1" method="post" action="result.php">
<!--###### Content Board Start ######-->
<table width="99%" border="0" cellspacing="0" cellpadding="0">
<tr> 
  <td width="13" >&nbsp;</td>
  <td class="main_content"><!---->
 	<div class="report_option report_show_option">
 			<span id="spanShowOption" class="spanShowOption" style="display:none">
					<a href="javascript:showOptionLayer();"><?=$Lang["libms"]["report"]["showoption"]?></a>
				</span> 
				
				<span id="spanHideOption" class="spanHideOption" style="display:none">
					<a href="javascript:hideOptionLayer();"><?=$Lang["libms"]["report"]["hideoption"]?></a>
				</span>
 	</div>

	<span id="formContent" style="display:none" >
		<table width="99%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td width="13">&nbsp;</td>
			<td>
	       	<div class="report_option report_option_title"></div>
			<div class="table_board">
			<table class="form_table">
			<tr class="form_table ">
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['report']['target']?></td>
	          <td>
	          	<table class="inside_form_table">
					<tr>
						<td valign="top">
	          		<?=$linterface->Get_Radio_Button("findByGroup", "findBy", 'Group', $findBy=='Group' || !$findBy ? true:false, "", $Lang['libms']['report']['findByGroup'])?>
					<?=$linterface->Get_Radio_Button("findByForm", "findBy", 'Form', $findBy=='Form' ? true:false, "", $Lang["libms"]["report"]["Form"])?>
					<?=$linterface->Get_Radio_Button("findByClass", "findBy", 'Class', $findBy=='Class' ? true:false, "", $Lang["libms"]["report"]["Class"])?>
			  			</td>
			  		</tr>
			  		<tr id="tr_GroupList" <?= $findBy=='Group' || !$findBy ? '':'style="display: none;"' ?>>
						<td valign="top">
	          			<?=get_xxxSelection_box('LIBMS_GROUP', 'GroupTitle', ($GroupTitle?$GroupTitle:""))?>
			  			</td>
			  		</tr>
			  		<tr id="tr_FormList" <?= $findBy=='Form' ? '':'style="display: none;"' ?>>
						<td valign="top">
	          			<?=get_xxxSelection_box('LIBMS_USER', 'ClassLevel', ($ClassLevel?$ClassLevel:""))?>
			  			</td>
			  		</tr>
			  		<tr id="tr_ClassList" <?= $findBy=='Class' ? '':'style="display: none;"' ?>>
						<td valign="top">
						<div style="padding:10px 0px"><?=$Lang["libms"]["report"]["SchoolYear"]?>: <?=get_pre_year_list($PreYearList)?></div>
	          			<div id="ClassList" <?=$PreYearList?'style="display:none"':''?> ><?=get_xxxSelection_box('LIBMS_USER', 'ClassName', ($ClassName?$ClassName:""))?></div>
	          			<div id="PreClassList" <?=$PreYearList?'':'style="display:none"'?> ><?=get_pre_class_list($PreYearList, $PreClassName)?></div>
			  			</td>
			  		</tr>
			  	</table>		
			  </td>
	        </tr>
	        <tr class="form_table ">
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["listby"]?></td>
				<td>
					<?=$linterface->Get_Radio_Button("displayTotalPercentage", "displayTotal", 'Percentage', $displayTotal == 'Percentage' , "", $Lang["libms"]["report"]["percentage"])?>
					<?=$linterface->Get_Radio_Button("displayTotalTimes", "displayTotal", 'Times', $displayTotal == 'Times' , "", $Lang["libms"]["report"]["Times"])?>
				</td>
			</tr>
			<tr class="form_table ">
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['reporting']['countingMethod']?></td>
				<td>
					<?=$linterface->Get_Radio_Button("byNumberOfVisit", "countingMethod", 'byVisit', $countingMethod == 'byVisit' , "", $Lang['libms']['reporting']['countingMethodByVisit'])?>
					<?=$linterface->Get_Radio_Button("byNumberOfBooks", "countingMethod", 'byBooks', $countingMethod == 'byBooks' , "", $Lang['libms']['reporting']['countingMethodByBooks'])?>
				</td>
			</tr>
	        <tr class="form_table ">
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['reporting']['dateRange']?></td>
	          <td><?=$linterface->GET_DATE_PICKER("StartDate", ($StartDate?$StartDate:((date('m')>=9) ? date('Y-09-01') : (date('Y')-1).'-09-01')))?> <span class="tabletextremark"></span> ~ <?=$linterface->GET_DATE_PICKER("EndDate", $EndDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
	        </tr>
			<tr class="form_table ">
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['reporting']['countingMode']?></td>
				<td>
					<?=$linterface->Get_Radio_Button("daysCalculationAllDays", "daysCalculation", 'AllDays', $daysCalculation == 'AllDays' , "", $Lang['libms']["settings"]['system']['circulation_duedate_method_no'])?>
					<?=$linterface->Get_Radio_Button("daysCalculationOpenDays", "daysCalculation", 'OpenDays', $daysCalculation == 'OpenDays' , "", $Lang['libms']["settings"]['system']['circulation_duedate_method_yes'])?>
				</td>
			</tr>
	     </table>
		     <?=$linterface->MandatoryField();?>
	     	<p class="spacer"></p>
	    	</div>
	      	<div class="edit_bottom">
	      
	        <p class="spacer"></p>
	        <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "Js_Check_Form(this.form)", "GenerateBtn")?>        
	        <p class="spacer"></p>
	      </div>
	      </td>
	      <td width="11">&nbsp;</td>
	    </tr>
	  </table>
	</span>

	<div id='toolbox' class="content_top_tool">
	<div class="Conntent_tool">
		<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?></a>
		<a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?></a>
		</div>
		<br style="clear:both" />
	</div>

   	<div class="table_board">
		<div class="thumb_list_tab print_hide">
			<a class='result_toggler' href="#">
				<span><?=$Lang["libms"]["reporting"]["span"]['graph']?></span>
			</a>
			<em>|</em>
			<a href="#" class="result_toggler thumb_list_tab_on">
				<span><?=$Lang["libms"]["reporting"]["span"]['table']?></span>
			</a>
		</div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom"><div align="right"><?=$PreYearList?$Lang["libms"]["report"]["SchoolYear"].': '.$PreYearList:''?> <?=$Lang['General']['Date'] . ': ' .$thisStockTakeSchemeDisplay?></div></td>
		</tr>
	</table> 					    
		<div id="result_stat"><?=$displayTable?></div>
		<div id="result_chart" style="" ><div style="display:inline;float:left"><?=$displayPieChart?></div><div id="pieLegend" style="margin-top:0px; margin-left:0px; width:160px; height:250px;float:left"></div></div>
		<br/>
				
		<div class="edit_bottom">
		<p class="spacer"></p>
		<input name="button" type="button" class="formbutton" onclick="window.location='index.php'" value="<?=$Lang['Btn']['Back']?>" /> 	
		<p class="spacer"></p>
  </div>
  <p>&nbsp;</p></td>
  <td width="11" >&nbsp;</td>
</tr>
</table>
<!--###### Content Board End ######-->
</form>

<?php

function get_pre_year_list($PreYearList){
	global $Lang,$libms,$linterface, $lmh;
	
	$aryYear = $lmh->getYearList();
	$counter = 0;
	for($i=0; $i<count($aryYear);  $i++){
		if($aryYear[count($aryYear)-1-$i]){
			$result[$counter][0] = $aryYear[count($aryYear)-1-$i];
			$result[$counter][1] = $aryYear[count($aryYear)-1-$i];
			$counter++;
		}
	}
	
	return $linterface->GET_SELECTION_BOX($result, 'id="PreYearList" name="PreYearList" onChange="class_list()"', $Lang['SysMgr']['AcademicYear']['FieldTitle']['CurrentSchoolYear'], $PreYearList);
}

function get_pre_class_list($PreYear, $selectedVal){
	global $Lang,$libms,$linterface, $lmh;
	if($PreYear){
		$aryClassList = $lmh->getClassListByYear($PreYear,false);
		
		$classSel = '<select id="PreClassName" name="PreClassName[]" multiple="multiple" size="10">';
		$unclassify = array();
		$unclassify['PreClassName'] = "";
		$unclassify[$Lang["libms"]["sql_field"]["Description"]] = $Lang["libms"]["report"]["cannot_define"];
		
		for($i=0; $i<count($aryClassList[0]); $i++){
			$TempClassName = $aryClassList[0][$i]['ClassName'];
			$selected = '';
			if(is_array($selectedVal)){
				foreach($selectedVal as $aSelectVal){
					if ($TempClassName == $aSelectVal) {
						$selected = 'selected="selected"';
					}
				}
			}
			else if($selectedVal == ''){
				$selected = 'selected="selected"';
			}
			else{
				if ($TempClassName == $selectedVal) {
						$selected = 'selected="selected"';
				}
			}
			if($TempClassName)
				$classSel .='<option value="'.$TempClassName.'" '.$selected.'>'.$TempClassName.'</option>';
		}
		
		$classSel .='</select>&nbsp;';
		$selectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('".'PreClassName'."', 1);");
			
		$x = $linterface->Get_MultipleSelection_And_SelectAll_Div($classSel, $selectAllBtn, $SpanID='');
		
		return $x;
	}
}

function get_xxxSelection_box($table, $fieldname, $selectedVal=''){
	global $Lang,$libms,$linterface;
//	BookCirculationType multi selection box
	if($table == "LIBMS_GROUP" && $fieldname =="GroupTitle"){
		$sql = 'Select
			    r.GroupID as '.$fieldname.',
			    r.GroupTitle as '.$Lang["libms"]["sql_field"]["Description"].'
			From
			    LIBMS_GROUP r 
			Order by
			    r.GroupTitle';
			    
		$result = $libms->returnResultSet($sql);
	}
	else if($table == "LIBMS_USER" && $fieldname =="ClassLevel"){
		$sql = "SELECT `ClassLevel` as ".$fieldname.", `ClassLevel` as ".$Lang["libms"]["sql_field"]["Description"]."
			FROM `LIBMS_USER` 
			WHERE `UserType` = 'S' 
				AND RecordStatus = '1' 
				AND ClassLevel != ''
			GROUP BY `ClassLevel` 
			ORDER BY `ClassLevel`
			";
		$result = $libms->returnResultSet($sql);
	}
	else if($table == "LIBMS_USER" && $fieldname =="ClassName"){
		$sql = "SELECT `ClassName`  as ".$fieldname.", `ClassName` as ".$Lang["libms"]["sql_field"]["Description"]."
			FROM `LIBMS_USER`
			WHERE `UserType` = 'S'
				AND RecordStatus = '1'
				AND ClassName != ''
			GROUP BY `ClassName`
			ORDER BY `ClassName`
			";
		$result = $libms->returnResultSet($sql);
	}
	else{
		$fields[] = $fieldname;
		$fields[] = $Lang["libms"]["sql_field"]["Description"];
		$result =  $libms->SELECTFROMTABLE($table,$fields);
	}
	if (!empty($result)){
		$classSel = '<select id="'.$fieldname.'" name="'.$fieldname.'[]" multiple="multiple" size="10">';
		$unclassify = array();
		$unclassify[$fieldname] = "";
		$unclassify[$Lang["libms"]["sql_field"]["Description"]] = $Lang["libms"]["report"]["cannot_define"];
		//array_unshift($result, $unclassify);
		foreach($result as $aRecord){
			$selected = '';
			if(is_array($selectedVal)){
				foreach($selectedVal as $aSelectVal){
					if ($aRecord[$fieldname] == $aSelectVal) {
						$selected = 'selected="selected"';
					}
				}
			}
			else if($selectedVal == ''){
				$selected = 'selected="selected"';
			}
			else{
				if ($aRecord['CirculationTypeCode'] == $selectedVal) {
						$selected = 'selected="selected"';
				}
			}
			$classSel .='<option value="'.$aRecord[$fieldname].'" '.$selected.'>'.$aRecord[$Lang["libms"]["sql_field"]["Description"]].'</option>';
		}
		$classSel .='</select>&nbsp;';
		$selectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('".$fieldname."', 1);");
		
				$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($classSel, $selectAllBtn, $SpanID='');
			
		return $x;
	}
	return false;
}

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
