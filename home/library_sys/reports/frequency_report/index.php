<?php

// Using: 

/***************************************
 *
 * Date:    2018-06-25 [Cameron]
 *          - change countingMethod to countingMode [case #S139804]
 *          - add countingMethod filter: byVisit and byBooks
 * 
 * Date:	2017-07-18 [Henry]
 * 			- added counting method option [Case#H116665]
 * 
 * Date:	2016-08-01 [Cameron]
 * 			- hide the warning and empty option list if it returns nothing in ajaxSelectbox
 * 
 * Date:	2016-07-15 [Cameron]
 * 			- fix bug: don't allow to run report for invalid dateformat
 * 			- fix bug: if classlevel is selected, should select at least one class 
 *
 * Date:	2014-04-25 (Henry)
 * Details:	Created this file
 * 
 ***************************************/
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libstudentpromotion.php");
include_once($PATH_WRT_ROOT."includes/libmanagehistory.php");

intranet_auth();
intranet_opendb();

$lmh = new libmanagehistory();

//if($PreYear){
//	$aryYear = $lmh->getYearList();
//	//$aryClassList = $lmh->getClassListByYear(2012,false);
//	$aryClassList = $lmh->getClassListByYear($PreYear,false);
//	//$aryStudentList = $lmh->getStudentListByClass(2012,'1A');
//	$aryStudentList = array();
//	foreach($ClassName as $aClassName){
//		$aryStudentList[] = $lmh->getStudentListByClass($Year,$aClassName);
//	}
//}

//debug_pr($aryYear);
//debug_pr($aryClassList[0][2]['ClassName']);
//debug_pr($aryStudentList[0]['id']);

$libms = new liblms();

if(!$_SESSION['LIBMS']['admin']['current_right']['statistics']['frequency by form'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$libms = new liblms();
$linterface = new interface_html();

# Current Page Info
$CurrentPageArr['LIBMS'] = 1;
$TAGS_OBJ[] = array($Lang["libms"]["reporting"]["ReadingFrequency"]);
$CurrentPage = "PageReportingFrequenceReport";

# Top menu highlight setting
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();

function get_pre_year_list($PreYearList){
	global $Lang,$libms,$linterface, $lmh;
	
	$aryYear = $lmh->getYearList();
	$counter = 0;
	for($i=0; $i<count($aryYear);  $i++){
		if($aryYear[count($aryYear)-1-$i]){
			$result[$counter][0] = $aryYear[count($aryYear)-1-$i];
			$result[$counter][1] = $aryYear[count($aryYear)-1-$i];
			$counter++;
		}
	}
	
	return $linterface->GET_SELECTION_BOX($result, 'id="PreYearList" name="PreYearList" onChange="class_list()"', $Lang['SysMgr']['AcademicYear']['FieldTitle']['CurrentSchoolYear'], $PreYearList);
}

function get_xxxSelection_box($table, $fieldname, $selectedVal=''){
	global $Lang,$libms,$linterface, $lmh;
//	BookCirculationType multi selection box
	if($table == "LIBMS_GROUP" && $fieldname =="GroupTitle"){
		$sql = 'Select
			    r.GroupID as '.$fieldname.',
			    r.GroupTitle as '.$Lang["libms"]["sql_field"]["Description"].'
			From
			    LIBMS_GROUP r 
			Order by
			    r.GroupTitle';
			    
		$result = $libms->returnResultSet($sql);
	}
	else if($table == "LIBMS_USER" && $fieldname =="ClassLevel"){
		$sql = "SELECT `ClassLevel` as ".$fieldname.", `ClassLevel` as ".$Lang["libms"]["sql_field"]["Description"]."
			FROM `LIBMS_USER` 
			WHERE `UserType` = 'S' 
				AND RecordStatus = '1' 
				AND ClassLevel != ''
			GROUP BY `ClassLevel` 
			ORDER BY `ClassLevel`
			";
		$result = $libms->returnResultSet($sql);
	}
	else if($table == "LIBMS_USER" && $fieldname =="ClassName"){
		$sql = "SELECT `ClassName`  as ".$fieldname.", `ClassName` as ".$Lang["libms"]["sql_field"]["Description"]."
			FROM `LIBMS_USER`
			WHERE `UserType` = 'S'
				AND RecordStatus = '1'
				AND ClassName != ''
			GROUP BY `ClassName`
			ORDER BY `ClassName`
			";
		$result = $libms->returnResultSet($sql);
	}
	else{
		$fields[] = $fieldname;
		$fields[] = $Lang["libms"]["sql_field"]["Description"];
		$result =  $libms->SELECTFROMTABLE($table,$fields);
	}
	if (!empty($result)){
		$classSel = '<select id="'.$fieldname.'" name="'.$fieldname.'[]" multiple="multiple" size="10">';
		$unclassify = array();
		$unclassify[$fieldname] = "";
		$unclassify[$Lang["libms"]["sql_field"]["Description"]] = $Lang["libms"]["report"]["cannot_define"];
		//array_unshift($result, $unclassify);
		foreach($result as $aRecord){
			$selected = '';
			if(is_array($selectedVal)){
				foreach($selectedVal as $aSelectVal){
					if ($aRecord[$fieldname] == $aSelectVal) {
						$selected = 'selected="selected"';
					}
				}
			}
			else if($selectedVal == ''){
				$selected = 'selected="selected"';
			}
			else{
				if ($aRecord['CirculationTypeCode'] == $selectedVal) {
						$selected = 'selected="selected"';
				}
			}
			$classSel .='<option value="'.$aRecord[$fieldname].'" '.$selected.'>'.$aRecord[$Lang["libms"]["sql_field"]["Description"]].'</option>';
		}
		$classSel .='</select>&nbsp;';
		$selectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('".$fieldname."', 1);");
		
				$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($classSel, $selectAllBtn, $SpanID='');
			
		return $x;
	}
	return false;
}

?>
<script language="javascript">

$(document).ready(function(){
	$('#selectAllBtnStocktakeStatus').click(function(){ 		
		$('#stocktakeStatus option').attr('selected', true);
	});
	
	$( 'input[name="findBy"]:radio' ).click(
		function(){
       		//alert($('input[name="findBy"]:checked').val());
       		if($('input[name="findBy"]:checked').val() == 'Group'){
       			$('#tr_GroupList').show();
       			$('#tr_FormList').hide();
       			$('#tr_ClassList').hide();
       		}else if($('input[name="findBy"]:checked').val() == 'Form'){
       			$('#tr_GroupList').hide();
       			$('#tr_FormList').show();
       			$('#tr_ClassList').hide();
       		}else if($('input[name="findBy"]:checked').val() == 'Class'){
       			$('#tr_GroupList').hide();
       			$('#tr_FormList').hide();
       			$('#tr_ClassList').show();
       		}
   	 	}
	);
});	
	
function Js_Select_All(id){
	$('#'+id + ' option').attr('selected', 'selected');	
}

function Js_Check_Form(obj){
	var error = 0 ;
	var stocktakeLength = $('#stocktakeStatus option:selected').length;
	
	if($('input[name="findBy"]:checked').val() == 'Group' && $('#GroupTitle option:selected').length == 0){
		document.getElementById("GroupTitle").focus();
		alert('<?=$Lang['libms']['report']['msg']['please_select_group']?>');
		error++;
	}else if($('input[name="findBy"]:checked').val() == 'Form' && $('#ClassLevel option:selected').length == 0){
		document.getElementById("ClassLevel").focus();
		alert('<?=$Lang['libms']['report']['msg']['please_select_form']?>');
		error++;
	}else if($('input[name="findBy"]:checked').val() == 'Class' && $('#PreYearList :selected').val()=='' && $('#ClassName option:selected').length == 0){
		document.getElementById("ClassName").focus();
		alert('<?=$Lang['libms']['report']['msg']['please_select_class']?>');
		error++;
	}else if($('input[name="findBy"]:checked').val() == 'Class' && $('#PreYearList :selected').val()!='' && $('#PreClassName option:selected').length == 0){
		document.getElementById("PreClassName").focus();
		alert('<?=$Lang['libms']['report']['msg']['please_select_class']?>');
		error++;
	}else if (!check_date(document.getElementById("StartDate"),"<?=$Lang['General']['InvalidDateFormat']?>")) {
		error++;
	}else if (!check_date(document.getElementById("EndDate"),"<?=$Lang['General']['InvalidDateFormat']?>")) {
		error++;
	}else if(document.getElementById("StartDate").value > document.getElementById('EndDate').value){
		document.getElementById("StartDate").focus();
		alert("<?=$Lang['General']['WrongDateLogic']?>");
		error++;
	}
	if(error==0)
	{
		obj.submit();
	}

}

function ajaxSelectbox(page, fieldName){
	//alert("haha");
	$.get(page, {},
		function(insertRespond){
			if (insertRespond == ''){
//				alert('Ajax call return nothing. Please try again. If problem persist, make sure the setting is completed / contact your administrator');
				fieldName.html('');
			}else {
				//alert("haha");
				fieldName.html(insertRespond);
				$('#PreClassName option').attr('selected', true);
				$('#ClassList').hide();
				$('#PreClassList').show();
			}
		}, "json"
	);
}
		
function class_list(){
	//alert($('#PreYearList :selected').text());
	if($('#PreYearList :selected').val()){
		page = "ajax_get_pre_class_list.php?PreYear="+$('#PreYearList :selected').text();
		ajaxSelectbox(page, $('#PreClassList'));
	}
	else{
		$('#PreClassList').hide();
		$('#ClassName option').attr('selected', true);
		$('#ClassList').show();
	}
}
		
</script>

<form name="form1" method="post" action="result.php?clearCoo=1">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td width="13">&nbsp;</td>
		<td>
       	<div class="report_option report_option_title"></div>
		<div class="table_board">
		<table class="form_table">
			<tr class="form_table ">
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['report']['target']?></td>
	          <td>
	          	<table class="inside_form_table">
					<tr>
						<td valign="top">
	          		<?=$linterface->Get_Radio_Button("findByGroup", "findBy", 'Group', $findBy=='Group' || !$findBy ? true:false, "", $Lang['libms']['report']['findByGroup'])?>
					<?=$linterface->Get_Radio_Button("findByForm", "findBy", 'Form', $findBy=='Form' ? true:false, "", $Lang["libms"]["report"]["Form"])?>
					<?=$linterface->Get_Radio_Button("findByClass", "findBy", 'Class', $findBy=='Class' ? true:false, "", $Lang["libms"]["report"]["Class"])?>
			  			</td>
			  		</tr>
			  		<tr id="tr_GroupList" <?= $findBy=='Group' || !$findBy ? '':'style="display: none;"' ?>>
						<td valign="top">
	          			<?=get_xxxSelection_box('LIBMS_GROUP', 'GroupTitle', ($GroupTitle?$GroupTitle:""))?>
			  			</td>
			  		</tr>
			  		<tr id="tr_FormList" <?= $findBy=='Form' ? '':'style="display: none;"' ?>>
						<td valign="top">
	          			<?=get_xxxSelection_box('LIBMS_USER', 'ClassLevel', ($ClassLevel?$ClassLevel:""))?>
			  			</td>
			  		</tr>
			  		<tr id="tr_ClassList" <?= $findBy=='Class' ? '':'style="display: none;"' ?>>
						<td valign="top">
						<div style="padding:10px 0px"><?=$Lang["libms"]["report"]["SchoolYear"]?>: <?=get_pre_year_list($PreYearList)?></div>
	          			<div id="ClassList"><?=get_xxxSelection_box('LIBMS_USER', 'ClassName', ($ClassName?$ClassName:""))?></div>
	          			<div id="PreClassList"></div>
			  			</td>
			  		</tr>
			  	</table>		
			  </td>
	        </tr>
	        <tr class="form_table ">
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["listby"]?></td>
				<td>
					<?=$linterface->Get_Radio_Button("displayTotalPercentage", "displayTotal", 'Percentage', true , "", $Lang["libms"]["report"]["percentage"])?>
					<?=$linterface->Get_Radio_Button("displayTotalTimes", "displayTotal", 'Times', false , "", $Lang["libms"]["report"]["Times"])?>
				</td>
			</tr>
			<tr class="form_table ">
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['reporting']['countingMethod']?></td>
				<td>
					<?=$linterface->Get_Radio_Button("byNumberOfVisit", "countingMethod", 'byVisit', true, "", $Lang['libms']['reporting']['countingMethodByVisit'])?>
					<?=$linterface->Get_Radio_Button("byNumberOfBooks", "countingMethod", 'byBooks', false, "", $Lang['libms']['reporting']['countingMethodByBooks'])?>
				</td>
			</tr>
	        <tr class="form_table ">
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['reporting']['dateRange']?></td>
	          <td><?=$linterface->GET_DATE_PICKER("StartDate", ($StartDate?$StartDate:((date('m')>=9) ? date('Y-09-01') : (date('Y')-1).'-09-01')))?> <span class="tabletextremark"></span> ~ <?=$linterface->GET_DATE_PICKER("EndDate", $EndDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
	        </tr>
			<tr class="form_table ">
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['reporting']['countingMode']?></td>
				<td>
					<?=$linterface->Get_Radio_Button("daysCalculationAllDays", "daysCalculation", 'AllDays', true , "", $Lang['libms']["settings"]['system']['circulation_duedate_method_no'])?>
					<?=$linterface->Get_Radio_Button("daysCalculationOpenDays", "daysCalculation", 'OpenDays', false , "", $Lang['libms']["settings"]['system']['circulation_duedate_method_yes'])?>
				</td>
			</tr>
	     </table>
	     <?=$linterface->MandatoryField();?>
      <p class="spacer"></p>
    </div>
    <p class="spacer"></p>
      </div>
      <div class="edit_bottom">
        <p class="spacer"></p>
        <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "Js_Check_Form(this.form)", "GenerateBtn")?>        
        <p class="spacer"></p>
      </div>
      <p>&nbsp;</p></td>
      <td width="11">&nbsp;</td>
    </tr>
  </table>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
