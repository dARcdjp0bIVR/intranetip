<?php
/*
 * 	Log
 * 	Date:	2015-03-26 [Henry] create this file
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libstudentpromotion.php");
include_once($PATH_WRT_ROOT."includes/libmanagehistory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$PreYear = trim($_REQUEST['PreYear']);

$libms = new liblms();
$lmh = new libmanagehistory();
$linterface = new interface_html();

if($PreYear){
	$selectedVal = $PreYear;
	
	$aryClassList = $lmh->getClassListByYear($PreYear,false);
	
	$classSel = '<select id="PreClassName" name="PreClassName[]" multiple="multiple" size="10">';
	$unclassify = array();
	$unclassify['PreClassName'] = "";
	$unclassify[$Lang["libms"]["sql_field"]["Description"]] = $Lang["libms"]["report"]["cannot_define"];
	
	for($i=0; $i<count($aryClassList[0]); $i++){
		$selected = '';
		if(is_array($selectedVal)){
			foreach($selectedVal as $aSelectVal){
				if ($aRecord['PreClassName'] == $aSelectVal) {
					$selected = 'selected="selected"';
				}
			}
		}
		else if($selectedVal == ''){
			$selected = 'selected="selected"';
		}
		else{
			if ($aRecord['CirculationTypeCode'] == $selectedVal) {
					$selected = 'selected="selected"';
			}
		}
		if($aryClassList[0][$i]['ClassName'])
			$classSel .='<option value="'.$aryClassList[0][$i]['ClassName'].'" '.$selected.'>'.$aryClassList[0][$i]['ClassName'].'</option>';
	}
	
	$classSel .='</select>&nbsp;';
	$selectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('".'PreClassName'."', 1);");
		
	$x = $linterface->Get_MultipleSelection_And_SelectAll_Div($classSel, $selectAllBtn, $SpanID='');
	
	$json = new JSON_obj();
	echo $json->encode($x);
}
else{
	echo false;
}
intranet_closedb();
?>
