<?php
#using: 

/***************************************
 * 
 * Date:	2019-07-23 [Henry]
 * Details: - use the local library of jquery
 *
 * Date:    2018-06-25 [Cameron]
 * Details: - change countingMethod to countingMode [case #S139804]
 *          - add countingMethod filter: byVisit and byBooks
 * 
 * Date:	2018-06-04 [Henry]
 * Details:	don't count deleted student record (RecordStatus=0) [Case#P139903]
 * 
 * Date:	2017-08-02 [Cameron]
 * Details:	the statistics should include left and suspend student as the borrow action is in the past and this report is couted to class / group / form level only
 * 			[ej 5.0.7.8.1 DM872] <- void after 2018-06-04 		
 * 
 * Date:	2017-07-18 [Henry]
 * Details: added counting method option [Case#H116665]
 * 
 * Date:	2017-06-26 (Cameron) (ej5.0.7.8.1)
 * Details: - fix bug: don't show deleted student record (RecordStatus=0) [case #P116646]
 *  
 * Date:	2016-08-01 [Cameron]
 * Details:	- fix bug: preg_replace non-word character already include dash '-' and space
 * 			- fix bug: create javascript for plotting chart only when $show == 'chart'
 * Date:	2015-07-03 (Henry)
 * Details:	fix: The number of total borrowed
 * Date: 	2014-05-14 (Henry)
 * Details: Added school name at top of the page
 * Date:	2014-04-28 (Henry)
 * Details:	Created this file
 * 
 ***************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/TimeManager.php");

include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libstudentpromotion.php");
include_once($PATH_WRT_ROOT."includes/libmanagehistory.php");

intranet_auth();
intranet_opendb();

$Lang['General']['EmptySymbol'] = "&nbsp;";



$laccessright = new libaccessright();

if(!$_SESSION['LIBMS']['admin']['current_right']['statistics']['frequency by form'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$lmh = new libmanagehistory();
$libms = new liblms();
$linterface = new interface_html();
$lclass = new libclass();

$StartDate = $_POST['StartDate'];
$EndDate = $_POST['EndDate'];
$findBy = $_POST['findBy'];
$GroupTitle = $_POST['GroupTitle'];
$ClassLevel = $_POST['ClassLevel'];
$ClassName = $_POST['ClassName'];
$displayTotal = $_POST['displayTotal'];
$countingMethod = $_POST['countingMethod'];
$daysCalculation = $_POST['daysCalculation'];

$thisStockTakeSchemeDisplay = $StartDate  . ' ' . $Lang['General']['To']  . ' ' .  $EndDate;

$schoolName =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];

$header = '';
$header .= '<table width="98%" border="0" cellspacing="0" cellpadding="5" valign="top">';
$header .= '<tr>';
$header .= '<td align="right" class="print_hide">';
$header .= $linterface->GET_BTN($button_print, "button","javascript:window.print()");
$header .= '</td>'; 
$header .= '</tr>';
$header .= '<tr><td>'.$schoolName.'</td></tr>';  
$header .= '<tr>';
$header .= '<td>';
$header .= '<center><h2>' . $Lang["libms"]["reporting"]["ReadingFrequency"] . '</h2></center>';
$header .= '</td>';
$header .= '</tr>';
$header .= '<tr>';
$header .= '<td align="right">';
$header .= ($PreYearList?$Lang["libms"]["report"]["SchoolYear"].': '.$PreYearList.' ':'').$Lang['General']['Date'] . ': ' .$thisStockTakeSchemeDisplay;
$header .= '</td>'; 
$header .= '</tr>';
$header .= '<tr>';
$header .= '<td>';

########################## DB Table Start ###############################
if($findBy == 'Group'){
	$UserList = array();
	for($i=0; $i<count($GroupTitle);$i++){
		$sql = 'select
					gu.UserID
				from
					LIBMS_GROUP_USER gu 
				join 
					LIBMS_USER u 
				on 
					gu.UserID = u.UserID
				where
					u.RecordStatus = 1 AND 
					gu.GroupID = \''.$GroupTitle[$i].'\'';			
		$result = $libms->returnArray($sql);
		
	foreach($result as $aResult)
		$UserList[$GroupTitle[$i]][]=$aResult['UserID'];
	}
	
}else if($findBy == 'Form'){
	$UserList = array();
	for($i=0; $i<count($ClassLevel);$i++){
		$sql = "select
					UserID
				from
					LIBMS_USER
				where
				RecordStatus = '1' AND 
				ClassLevel = '$ClassLevel[$i]'";			
		$result = $libms->returnArray($sql);
		
	foreach($result as $aResult)
		$UserList[$ClassLevel[$i]][]=$aResult['UserID'];
	}
}else if($findBy == 'Class'){
	$UserList = array();
	
	if($PreYearList){
		foreach($PreClassName as $aClassName){
			$aryStudentList = $lmh->getStudentListByClass($PreYearList,$aClassName,$includeLeftStudent=true);
			foreach($aryStudentList as $aStudent)
				$UserList[$aClassName][]=$aStudent['id'];
		}
		//debug_pr($UserList);
	}
	else{
		for($i=0; $i<count($ClassName);$i++){
			$sql = "select
						UserID
					from
						LIBMS_USER
					where
					RecordStatus = '1' AND 
					ClassName = '$ClassName[$i]'";			
			$result = $libms->returnArray($sql);
			
		foreach($result as $aResult)
			$UserList[$ClassName[$i]][]=$aResult['UserID'];
		}
	}
}

// by number of books (material) borrowed
if ($countingMethod == 'byBooks'){
    $sql = "SELECT UserID, COUNT(*) as BorrowTimes
			FROM LIBMS_BORROW_LOG
			WHERE UNIX_TIMESTAMP(BorrowTime) >= UNIX_TIMESTAMP('".$StartDate."')
			AND UNIX_TIMESTAMP(BorrowTime) <= UNIX_TIMESTAMP('".$EndDate." 23:59:59')
			GROUP BY UserID";
}
else{
    // borrow books within 10 minutes is counted as one time
    $sql = "SELECT UserID, COUNT( distinct SUBSTRING(BorrowTime,1,15)) as BorrowTimes
			FROM LIBMS_BORROW_LOG
			WHERE UNIX_TIMESTAMP(BorrowTime) >= UNIX_TIMESTAMP('".$StartDate."')
			AND UNIX_TIMESTAMP(BorrowTime) <= UNIX_TIMESTAMP('".$EndDate." 23:59:59')
			GROUP BY UserID";
}
		
$result = $libms->returnArray($sql);

if (!empty($result))
	foreach($result as $item)
	$formated_Array[$item[0]]=$item[1];
	
//debug_pr($formated_Array);

$table2dArray = array();

$tempArr['1weekormore'] = 0;
$tempArr['2week'] = 0;
$tempArr['1month'] = 0;
$tempArr['1monthless'] = 0;
$tempArr['none'] = 0;
$tempArr['total'] = 0;
$tempArr['totalTimes'] = 0;

//calcuate the group with the range of borrowed time
$StartTime = strtotime($StartDate);
$EndTime = strtotime($EndDate);
$datediff = $EndTime - $StartTime;
if($daysCalculation == 'OpenDays'){
	$TM = new TimeManager();
	$tempDate = $StartDate;
	$datediff = 0;
	while($tempDate <= $EndDate){
		if($TM->isOpen(strtotime($tempDate))){
			$datediff++;
		}
		$tempDate = date('Y-m-d', strtotime($tempDate . ' +1 day'));
	}
	$datediff = $datediff*60*60*24;
}
$selectedDays = floor($datediff/(60*60*24));
$range_1weekormore = floor($selectedDays/7);
$range_2week = floor($selectedDays/14);
$range_1month =  floor($selectedDays/28);

foreach($UserList as $key => $value){
	
	$table2dArray[$key]['1weekormore'] = 0;
	$table2dArray[$key]['2week'] = 0;
	$table2dArray[$key]['1month'] = 0;
	$table2dArray[$key]['1monthless'] = 0;
	$table2dArray[$key]['none'] = 0;
	$table2dArray[$key]['total'] = 0;
	$table2dArray[$key]['totalTimes'] = 0;
	
	$totalUserArr[$key] = 0;
	foreach($value as $aValue){
		$totalUserArr[$key] = count($value);
		
		if(!isset($formated_Array[$aValue])){
			$table2dArray[$key]['none']++;
			$tempArr['none']++;
		}
		else if($formated_Array[$aValue] >= $range_1weekormore){
			$table2dArray[$key]['1weekormore']++;
			$tempArr['1weekormore']++;
			$table2dArray[$key]['total']++;
			$table2dArray[$key]['totalTimes'] += $formated_Array[$aValue];
			$tempArr['total']++;
			$tempArr['totalTimes'] += $formated_Array[$aValue];
		}
		else if($formated_Array[$aValue] >= $range_2week){
			$table2dArray[$key]['2week']++;
			$tempArr['2week']++;
			$table2dArray[$key]['total']++;
			$table2dArray[$key]['totalTimes'] += $formated_Array[$aValue];
			$tempArr['total']++;
			$tempArr['totalTimes'] += $formated_Array[$aValue];
		}
		else if($formated_Array[$aValue] >= $range_1month){
			$table2dArray[$key]['1month']++;
			$tempArr['1month']++;
			$table2dArray[$key]['total']++;
			$table2dArray[$key]['totalTimes'] += $formated_Array[$aValue];
			$tempArr['total']++;
			$tempArr['totalTimes'] += $formated_Array[$aValue];
		}
		else{
			$table2dArray[$key]['1monthless']++;
			$tempArr['1monthless']++;
			$table2dArray[$key]['total']++;
			$table2dArray[$key]['totalTimes'] += $formated_Array[$aValue];
			$tempArr['total']++;
			$tempArr['totalTimes'] += $formated_Array[$aValue];
		}
	}
}
$table2dArray['schoolTotal']['1weekormore'] = $tempArr['1weekormore'];
$table2dArray['schoolTotal']['2week'] = $tempArr['2week'];
$table2dArray['schoolTotal']['1month'] = $tempArr['1month'];
$table2dArray['schoolTotal']['1monthless'] = $tempArr['1monthless'];
$table2dArray['schoolTotal']['none'] = $tempArr['none'];
$table2dArray['schoolTotal']['total'] = $tempArr['total'];
$table2dArray['schoolTotal']['totalTimes'] = $tempArr['totalTimes'];
$totalUserArr['schoolTotal'] = $tempArr['none']+$tempArr['total'];

//debug_pr($table2dArray);
//debug_pr($totalUserArr);

$ChartTitle = array();

$displayTable = '<table width="98%" class="common_table_list_v30 view_table_list_v30">';

$displayTable .= '<tr>';
$displayTable .= '<th>&nbsp;</th>';
foreach($table2dArray as $key => $value){
	$tempKey = $key;
	if($findBy == 'Group' && $key != 'schoolTotal'){
		$sql = "Select
				    GroupTitle
				From
				    LIBMS_GROUP
				Where
					GroupID = '$key'";
				    
			$key = current($libms->returnArray($sql));
			$key = $key['GroupTitle'];
	}
	if($key == 'schoolTotal'){
		$key = $Lang['libms']['bookmanagement']['total'];
	}
	$displayTable .= '<th>'.$key.'</th>';
	$ChartTitle[$tempKey] = $key;
}

$ChartTitle['schoolTotal'] = $Lang['libms']['bookmanagement']['overall'];

$displayTable .= '</tr>';

$displayTable .= '<tr>';
$displayTable .= '<td nowrap="nowrap">'.$Lang['libms']['report']['1weekormore'].'</td>';
foreach($table2dArray as $key => $value){
	if($displayTotal == "Percentage"){
		$displayTable .= '<td>'.number_format(@round($value['1weekormore']/$totalUserArr[$key]*100, 2), 2, '.', '').'%</td>';
	}
	else{
		$displayTable .= '<td>'.$value['1weekormore'].'</td>';
	}
}
$displayTable .= '</tr>';

$displayTable .= '<tr>';
$displayTable .= '<td nowrap="nowrap">'.$Lang['libms']['report']['2week'].'</td>';
foreach($table2dArray as $key => $value){
	if($displayTotal == "Percentage"){
		$displayTable .= '<td>'.number_format(@round($value['2week']/$totalUserArr[$key]*100, 2), 2, '.', '').'%</td>';
	}
	else{
		$displayTable .= '<td>'.$value['2week'].'</td>';
	}
}
$displayTable .= '</tr>';

$displayTable .= '<tr>';
$displayTable .= '<td nowrap="nowrap">'.$Lang['libms']['report']['1month'].'</td>';
foreach($table2dArray as $key => $value){
	if($displayTotal == "Percentage"){
		$displayTable .= '<td>'.number_format(@round($value['1month']/$totalUserArr[$key]*100, 2), 2, '.', '').'%</td>';
	}
	else{
		$displayTable .= '<td>'.$value['1month'].'</td>';
	}
}
$displayTable .= '</tr>';

$displayTable .= '<tr>';
$displayTable .= '<td nowrap="nowrap">'.$Lang['libms']['report']['1monthless'].'</td>';
foreach($table2dArray as $key => $value){
	if($value['none'] == 0 && $value['1monthless'] != 0){
		if($displayTotal == "Percentage"){
			$value['1monthless'] = 100 - @round($value['1weekormore']/$totalUserArr[$key]*100, 2) - @round($value['2week']/$totalUserArr[$key]*100, 2) - @round($value['1month']/$totalUserArr[$key]*100, 2); 
			$displayTable .= '<td>'.number_format($value['1monthless'], 2, '.', '').'%</td>';
		}
		else{
			$displayTable .= '<td>'.$value['1monthless'].'</td>';
		}
	}
	else{
		if($displayTotal == "Percentage"){
			$displayTable .= '<td>'.number_format(@round($value['1monthless']/$totalUserArr[$key]*100, 2), 2, '.', '').'%</td>';
		}
		else{
			$displayTable .= '<td>'.$value['1monthless'].'</td>';
		}
	}
}
$displayTable .= '</tr>';

$displayTable .= '<tr>';
$displayTable .= '<td nowrap="nowrap">'.$Lang['libms']['report']['none'].'</td>';
foreach($table2dArray as $key => $value){
	if($value['none'] > 0){
		if($displayTotal == "Percentage"){
			$value['none'] = 100 - @round($value['1weekormore']/$totalUserArr[$key]*100, 2) - @round($value['2week']/$totalUserArr[$key]*100, 2) - @round($value['1month']/$totalUserArr[$key]*100, 2) - @round($value['1monthless']/$totalUserArr[$key]*100, 2); 
			$displayTable .= '<td>'.number_format($value['none'], 2, '.', '').'%</td>';
		}
		else{
			$displayTable .= '<td>'.$value['none'].'</td>';
		}
	}
	else{
		if($displayTotal == "Percentage"){
			$displayTable .= '<td>'.number_format(@round($value['none']/$totalUserArr[$key]*100, 2), 2, '.', '').'%</td>';
		}
		else{
			$displayTable .= '<td>'.$value['none'].'</td>';
		}
	}
}
$displayTable .= '</tr>';

$displayTable .= '<tr class="row_avaliable">';
$displayTable .= '<td nowrap="nowrap">'.$Lang['libms']['report']['totalTimesOfBorrow'].'</td>';
foreach($table2dArray as $key => $value){
	$displayTable .= '<td>'.$value['totalTimes'].'</td>';
}
$displayTable .= '</tr>';

$displayTable .= '</table>';

if($show == 'chart'){
	$displayTable = '<span class="sectiontitle">'.$Lang['libms']['report']['totalTimesOfBorrow'].': '.$value['totalTimes'].'</span><br/>';

$displayTable .= '<table>';
$i=0;
foreach($table2dArray as $key => $value){
	if($i%3 == 0)
		$displayTable .= '<tr>';
//	$displayTable .= '<td><div id="pieChart_'.str_replace(' ', '', preg_replace('/[^A-Za-z0-9\-]/', '_', $key)).'" style="margin-top:0px; margin-left:0px; width:200px; height:200px;"></div></td>';
	$displayTable .= '<td><div id="pieChart_'.preg_replace('/[^A-Za-z0-9]/', '_', $key).'" style="margin-top:0px; margin-left:0px; width:200px; height:200px;"></div></td>';
	if($i%3 == 2)
		$displayTable .= '</tr>';
	$i++;
}
$displayTable .= '</table>';
$displayTable='<div style="display:inline;float:left;">'.$displayTable.'</div><div id="pieLegend" style="margin-top:0px; margin-left:0px; width:150px; height:220px;float:left"></div></div>';
}
########################## DB Table End ###############################
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  /> 
<META Http-Equiv="Cache-Control" Content="no-cache">
<script src="/templates/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.jqplot.min.js"></script>
<link rel="stylesheet" type="text/css" href="../css/jquery.jqplot.css" />
<script type="text/javascript" src="../js/jqplot.pieRenderer.min.js"></script>
<script language="javascript">
$(document).ready(function(){
	<?if($show == 'chart'){?>
	//for the chart drawing
	<?foreach($table2dArray as $key => $value){ ?>
			
		var s1 = [['<?=$Lang['libms']['report']['1weekormore']?>',<?=$value['1weekormore']?>], ['<?=$Lang['libms']['report']['2week']?>',<?=$value['2week']?>], ['<?=$Lang['libms']['report']['1month']?>',<?=$value['1month']?>], ['<?=$Lang['libms']['report']['1monthless']?>',<?=$value['1monthless']?>], ['<?=$Lang['libms']['report']['none']?>', <?=$value['none']?>]];
	        
//	    var pieChart_<?=str_replace(' ', '', preg_replace('/[^A-Za-z0-9\-]/', '_', $key))?> = $.jqplot('pieChart_<?=str_replace(' ', '', preg_replace('/[^A-Za-z0-9\-]/', '_', $key))?>', [s1], {
		var pieChart_<?=preg_replace('/[^A-Za-z0-9]/', '_', $key)?> = $.jqplot('pieChart_<?=preg_replace('/[^A-Za-z0-9]/', '_', $key)?>', [s1], {	    	
	    	seriesColors: [ "#DD1000", "#FF6800", "#FFCC01", "#73AE14", "#1B9FDD"],
	        grid: {
	            drawBorder: false, 
	            drawGridlines: false,
	            background: '#ffffff',
	            shadow:false
	        },
	        axesDefaults: {
	            
	        },
	        seriesDefaults:{
	            renderer:$.jqplot.PieRenderer,
	            rendererOptions: {
	                showDataLabels: true,
	                dataLabelFormatString: '%.2f%%'
	            }
	        },
	        legend: {
	            show: false,
	            rendererOptions: {
	                numberCols: 1
	            },
	            location: 'e'
	        },
	        title: {
	            show: true,
	            text: "<?=$ChartTitle[$key]?>",
	            fontSize: '14px'
	        },
	        fontSize: '14px'
	    }); 
    
	<?}?>
	
	var s1 = [['<?=$Lang['libms']['report']['1weekormore']?>',<?=$value['1weekormore']?>], ['<?=$Lang['libms']['report']['2week']?>',<?=$value['2week']?>], ['<?=$Lang['libms']['report']['1month']?>',<?=$value['1month']?>], ['<?=$Lang['libms']['report']['1monthless']?>',<?=$value['1monthless']?>], ['<?=$Lang['libms']['report']['none']?>', <?=$value['none']?>]];
	        
	    var pieLegend = $.jqplot('pieLegend', [s1], {
	    	seriesColors: [ "#DD1000", "#FF6800", "#FFCC01", "#73AE14", "#1B9FDD"],
	        grid: {
	            drawBorder: false, 
	            drawGridlines: false,
	            background: '#ffffff',
	            shadow:false
	        },
	        axesDefaults: {
	            
	        },
	        seriesDefaults:{
	            renderer:$.jqplot.PieRenderer,
	            rendererOptions: {
	                showDataLabels: true
	            }
	        },
	        legend: {
	            show: true,
	            rendererOptions: {
	                numberCols: 1
	            },
	            location: 'e',
	             fontSize: '12px'
	        },
	        title: {
	            show: false,
	            text: ""
	        }
	    });
	<?}?>	    
});
</script>
<style type='text/css'>

html, body { margin: 0px; padding: 0px; } 


@media print and (width: 21cm) and (height: 29.7cm) {
    @page {
       margin: 3cm;
    }
 }
 
@media print
{
	
  /*table { page-break-after:auto }
  tr    { page-break-inside:avoid; page-break-after:auto }
  td    { page-break-inside:avoid; page-break-after:auto }
  thead { display:table-header-group }
  tfoot { display:table-footer-group }
  
  #footer {
         display:block;
		   position:fixed;
		   bottom:0px;
		   width:100%;
  } */
    
    
}

</style>
</head>
<body>
<?php echo $header;?>
<?php echo $displayTable;?>
</body>
</html>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>