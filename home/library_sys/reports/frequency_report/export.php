<?php
# using: 

/***************************************
 * 
 * Date:    2018-06-25 [Cameron]
 * Details: - change countingMethod to countingMode [case #S139804]
 *          - add countingMethod filter: byVisit and byBooks
 * 
 * Date:	2018-06-04 [Henry]
 * Details:	don't count deleted student record (RecordStatus=0) [Case#P139903]
 * 
 * Date:	2017-08-02 [Cameron]
 * Details:	the statistics should include left and suspend student as the borrow action is in the past and this report is couted to class / group / form level only
 * 			[ej 5.0.7.8.1 DM872] <- void after 2018-06-04 		
 * 
 * Date:	2017-07-18 [Henry]
 * Details: added counting method option [Case#H116665]
 * 
 * Date:	2016-06-26 (Cameron) (ej5.0.7.8.1)
 * Details: fix: don't show deleted student record (RecordStatus=0) [case #P116646]
 *  
 * Date:	2015-07-03 (Henry)
 * Details:	fix: The number of total borrowed
 * Date:	2013-04-28 (Henry)
 * Details:	Created this file
 * 
 ***************************************/
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/TimeManager.php");

include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libstudentpromotion.php");
include_once($PATH_WRT_ROOT."includes/libmanagehistory.php");

intranet_auth();
intranet_opendb();

$Lang['General']['EmptySymbol'] = "";


if(!$_SESSION['LIBMS']['admin']['current_right']['statistics']['frequency by form'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lmh = new libmanagehistory();
$lexport = new libexporttext();
$libms = new liblms();
$filename = "frequency_by_form(".$StartDate."_to_".$EndDate.").csv";
$ExportArr = array();

## Obtain POST Value
$StartDate = $_POST['StartDate'];
$EndDate = $_POST['EndDate'];
$displayTotal = $_POST['displayTotal'];

$countingMethod = $_POST['countingMethod'];
$daysCalculation = $_POST['daysCalculation'];

$thisStockTakeSchemeDisplay = $StartDate  . ' ' . $Lang['General']['To']  . ' ' .  $EndDate;
# Define Column Title
$exportColumn = array();
$exportColumn[0][] = $Lang["libms"]["reporting"]["ReadingFrequency"];
if($PreYearList){
	$exportColumn[1][] = $Lang["libms"]["report"]["SchoolYear"] . ':' ;
	$exportColumn[1][] = $PreYearList;
}
$exportColumn[1][] = $Lang['General']['Date'] . ':' ;
$exportColumn[1][] = $thisStockTakeSchemeDisplay;

########################## DB Table Start ###############################
if($findBy == 'Group'){
	$UserList = array();
	for($i=0; $i<count($GroupTitle);$i++){
		$sql = 'select
					gu.UserID
				from
					LIBMS_GROUP_USER gu 
				join 
					LIBMS_USER u 
				on 
					gu.UserID = u.UserID
				where
					u.RecordStatus = 1 AND 
					gu.GroupID = \''.$GroupTitle[$i].'\'';			
		$result = $libms->returnArray($sql);
		
	foreach($result as $aResult)
		$UserList[$GroupTitle[$i]][]=$aResult['UserID'];
	}
	
}else if($findBy == 'Form'){
	$UserList = array();
	for($i=0; $i<count($ClassLevel);$i++){
		$sql = "select
					UserID
				from
					LIBMS_USER
				where
				RecordStatus = '1' AND 
				ClassLevel = '$ClassLevel[$i]'";			
		$result = $libms->returnArray($sql);
		
	foreach($result as $aResult)
		$UserList[$ClassLevel[$i]][]=$aResult['UserID'];
	}
}else if($findBy == 'Class'){
	$UserList = array();
	
	if($PreYearList){
		foreach($PreClassName as $aClassName){
			$aryStudentList = $lmh->getStudentListByClass($PreYearList,$aClassName,$includeLeftStudent=true);
			foreach($aryStudentList as $aStudent)
				$UserList[$aClassName][]=$aStudent['id'];
		}
		//debug_pr($UserList);
	}
	else{
		for($i=0; $i<count($ClassName);$i++){
			$sql = "select
						UserID
					from
						LIBMS_USER
					where
					RecordStatus = '1' AND 
					ClassName = '$ClassName[$i]'";			
			$result = $libms->returnArray($sql);
			
		foreach($result as $aResult)
			$UserList[$ClassName[$i]][]=$aResult['UserID'];
		}
	}
}

if ($countingMethod == 'byBooks'){
    $sql = "SELECT UserID, COUNT(*) as BorrowTimes
			FROM LIBMS_BORROW_LOG
			WHERE UNIX_TIMESTAMP(BorrowTime) >= UNIX_TIMESTAMP('".$StartDate."')
			AND UNIX_TIMESTAMP(BorrowTime) <= UNIX_TIMESTAMP('".$EndDate." 23:59:59')
			GROUP BY UserID";
}
else{
    $sql = "SELECT UserID, COUNT( distinct SUBSTRING(BorrowTime,1,15)) as BorrowTimes
			FROM LIBMS_BORROW_LOG
			WHERE UNIX_TIMESTAMP(BorrowTime) >= UNIX_TIMESTAMP('".$StartDate."')
			AND UNIX_TIMESTAMP(BorrowTime) <= UNIX_TIMESTAMP('".$EndDate." 23:59:59')
			GROUP BY UserID";
}
		
$result = $libms->returnArray($sql);

if (!empty($result))
	foreach($result as $item)
	$formated_Array[$item[0]]=$item[1];
	
//debug_pr($formated_Array);

$table2dArray = array();

$tempArr['1weekormore'] = 0;
$tempArr['2week'] = 0;
$tempArr['1month'] = 0;
$tempArr['1monthless'] = 0;
$tempArr['none'] = 0;
$tempArr['total'] = 0;
$tempArr['totalTimes'] = 0;

//calcuate the group with the range of borrowed time
$StartTime = strtotime($StartDate);
$EndTime = strtotime($EndDate);
$datediff = $EndTime - $StartTime;
if($daysCalculation == 'OpenDays'){
	$TM = new TimeManager();
	$tempDate = $StartDate;
	$datediff = 0;
	while($tempDate <= $EndDate){
		if($TM->isOpen(strtotime($tempDate))){
			$datediff++;
		}
		$tempDate = date('Y-m-d', strtotime($tempDate . ' +1 day'));
	}
	$datediff = $datediff*60*60*24;
}
$selectedDays = floor($datediff/(60*60*24));
$range_1weekormore = floor($selectedDays/7);
$range_2week = floor($selectedDays/14);
$range_1month =  floor($selectedDays/28);

foreach($UserList as $key => $value){
	
	$table2dArray[$key]['1weekormore'] = 0;
	$table2dArray[$key]['2week'] = 0;
	$table2dArray[$key]['1month'] = 0;
	$table2dArray[$key]['1monthless'] = 0;
	$table2dArray[$key]['none'] = 0;
	$table2dArray[$key]['total'] = 0;
	$table2dArray[$key]['totalTimes'] = 0;
	
	$totalUserArr[$key] = 0;
	foreach($value as $aValue){
		$totalUserArr[$key] = count($value);
		
		if(!isset($formated_Array[$aValue])){
			$table2dArray[$key]['none']++;
			$tempArr['none']++;
		}
		else if($formated_Array[$aValue] >= $range_1weekormore){
			$table2dArray[$key]['1weekormore']++;
			$tempArr['1weekormore']++;
			$table2dArray[$key]['total']++;
			$table2dArray[$key]['totalTimes'] += $formated_Array[$aValue];
			$tempArr['total']++;
			$tempArr['totalTimes'] += $formated_Array[$aValue];
		}
		else if($formated_Array[$aValue] >= $range_2week){
			$table2dArray[$key]['2week']++;
			$tempArr['2week']++;
			$table2dArray[$key]['total']++;
			$table2dArray[$key]['totalTimes'] += $formated_Array[$aValue];
			$tempArr['total']++;
			$tempArr['totalTimes'] += $formated_Array[$aValue];
		}
		else if($formated_Array[$aValue] >= $range_1month){
			$table2dArray[$key]['1month']++;
			$tempArr['1month']++;
			$table2dArray[$key]['total']++;
			$table2dArray[$key]['totalTimes'] += $formated_Array[$aValue];
			$tempArr['total']++;
			$tempArr['totalTimes'] += $formated_Array[$aValue];
		}
		else{
			$table2dArray[$key]['1monthless']++;
			$tempArr['1monthless']++;
			$table2dArray[$key]['total']++;
			$table2dArray[$key]['totalTimes'] += $formated_Array[$aValue];
			$tempArr['total']++;
			$tempArr['totalTimes'] += $formated_Array[$aValue];
		}
	}
}
$table2dArray['schoolTotal']['1weekormore'] = $tempArr['1weekormore'];
$table2dArray['schoolTotal']['2week'] = $tempArr['2week'];
$table2dArray['schoolTotal']['1month'] = $tempArr['1month'];
$table2dArray['schoolTotal']['1monthless'] = $tempArr['1monthless'];
$table2dArray['schoolTotal']['none'] = $tempArr['none'];
$table2dArray['schoolTotal']['total'] = $tempArr['total'];
$table2dArray['schoolTotal']['totalTimes'] = $tempArr['totalTimes'];
$totalUserArr['schoolTotal'] = $tempArr['none']+$tempArr['total'];

$exportColumn[2][] = '';
foreach($table2dArray as $key => $value){
	if($findBy == 'Group' && $key != 'schoolTotal'){
		$sql = "Select
				    GroupTitle
				From
				    LIBMS_GROUP
				Where
					GroupID = '$key'";
				    
			$key = current($libms->returnArray($sql));
			$key = $key['GroupTitle'];
	}
	if($key == 'schoolTotal'){
		$key = $Lang['libms']['bookmanagement']['total'];
	}
	$exportColumn[2][] = $key;
}

$ExportArr[0][] = $Lang['libms']['report']['1weekormore'];
foreach($table2dArray as $key => $value){
	if($displayTotal == "Percentage"){
		$ExportArr[0][] = number_format(@round($value['1weekormore']/$totalUserArr[$key]*100, 2), 2, '.', '').'%';
	}
	else{
		$ExportArr[0][] = $value['1weekormore'];
	}
}

$ExportArr[1][] = $Lang['libms']['report']['2week'];
foreach($table2dArray as $key => $value){
	if($displayTotal == "Percentage"){
		$ExportArr[1][] = number_format(@round($value['2week']/$totalUserArr[$key]*100, 2), 2, '.', '').'%';
	}
	else{
		$ExportArr[1][] = $value['2week'];
	}
}

$ExportArr[2][] = $Lang['libms']['report']['1month'];
foreach($table2dArray as $key => $value){
	if($displayTotal == "Percentage"){
		$ExportArr[2][] = number_format(@round($value['1month']/$totalUserArr[$key]*100, 2), 2, '.', '').'%';
	}
	else{
		$ExportArr[2][] = $value['1month'];
	}
}

$ExportArr[3][] = $Lang['libms']['report']['1monthless'];
foreach($table2dArray as $key => $value){
	if($value['none'] == 0  && $value['1monthless'] != 0){
		if($displayTotal == "Percentage"){
			$value['1monthless'] = 100 - @round($value['1weekormore']/$totalUserArr[$key]*100, 2) - @round($value['2week']/$totalUserArr[$key]*100, 2) - @round($value['1month']/$totalUserArr[$key]*100, 2); 
			$ExportArr[3][] = number_format($value['1monthless'], 2, '.', '').'%';
		}
		else{
			$ExportArr[3][] = $value['1monthless'];
		}
	}
	else{
		if($displayTotal == "Percentage"){
			$ExportArr[3][] = number_format(@round($value['1monthless']/$totalUserArr[$key]*100, 2), 2, '.', '').'%';
		}
		else{
			$ExportArr[3][] = $value['1monthless'];
		}
	}
}

$ExportArr[4][] = $Lang['libms']['report']['none'];
foreach($table2dArray as $key => $value){
	if($value['none'] > 0){
		if($displayTotal == "Percentage"){
			$value['none'] = 100 - @round($value['1weekormore']/$totalUserArr[$key]*100, 2) - @round($value['2week']/$totalUserArr[$key]*100, 2) - @round($value['1month']/$totalUserArr[$key]*100, 2) - @round($value['1monthless']/$totalUserArr[$key]*100, 2); 
			$ExportArr[4][] = number_format($value['none'], 2, '.', '').'%';
		}
		else{
			$ExportArr[4][] = $value['none'];
		}
	}
	else{
		if($displayTotal == "Percentage"){
			$ExportArr[4][] = number_format(@round($value['none']/$totalUserArr[$key]*100, 2), 2, '.', '').'%';
		}
		else{
			$ExportArr[4][] =$value['none'];
		}
	}
}

$ExportArr[5][] = $Lang['libms']['report']['totalTimesOfBorrow'];
foreach($table2dArray as $key => $value){
	$ExportArr[5][] = $value['totalTimes'];
}
########################## DB Table End ###############################

$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "9");

intranet_closedb();

# Output The File To User Browser
$lexport->EXPORT_FILE($filename, $export_content, $isXLS = false, $ignoreLength = false, $BaseCode="UTF-8");

?>