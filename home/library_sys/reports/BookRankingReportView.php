<?php
# modifying by:    

/**
 * Log :
 * 
 * Date:	2019-07-23 [Henry]
 * 			- use the local library of jquery
 * 
 * Date:	2016-07-15 [Cameron]
 * 			- hide the warning and empty option list if it returns nothing in ajaxSelectbox (IP2.5.7.7.1)
 * 			- don't allow to run report if user do not select target and period
 *
 * Date:	2016-01-15 [Cameron]
 * 			- Add books tag to filter
 * 			- Add suggestions for tags when typing
 * 
 * Date:	2015-08-12 [Cameron]
 * 				- Fix bug on showing CallNum in getCSV(), should show both CallNum and CallNum2
 *
 * Date:	2015-07-20 [Cameron]
 * 				- Change value of BookCategoryType from 'A' to 0 for consistency
 * Date:	2015-07-15 [Cameron] 
 * 				- Fix bug of showing ISBN instead of ACNO for first column when export data
 * Date:	2015-06-30 [Cameron] Add radio option "All" to BookCategory
 * Date:	2014-01-23 [Henry] modified getForm_BookCategory()
 * Date: 	2014-05-14 [Henry] Added school name at top of the page
 * Date:	2013-06-17 [Cameron] Fix bug: don't allow to select none in group target 
 * Date 	2013-05-07 [Yuen]
 * 			Improved the layout
 * Date 	2013-05-06 [Cameron]
 * 			Show filter period when print
 * Date:	2013-04-24 [yuen]
 * 			1. don't select all items on Circulation Category, Book Category, Subject nor Language by default
 * 			2. preset date range as current year
 * 
 * Date:	2013-04-23 [Cameron]
 * 			1. Add date validation function before submit criteria for report: $('#submit_result').click(function()
 * 			2. Downgrade ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js to 1.4.4 so that date picker function works
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../";
$MODULE_ROOT = "../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

require_once('ReportView.php');

global $Lang;
$linterface = new interface_html("libms.html");

class BookRankingReportView extends ReportView{
	public function __construct(){
	
		$CurrentPageArr['LIBMS'] = 1;
		$CurrentPage = "PageReportingBookRanking";
		parent::__construct($CurrentPage);
	}
	
	public function getView($viewdata = null){
		global $Lang;
	
		if (empty($print)){
			$x .= $this->getHeader();
			$x .= $this->getJS($viewdata);
			$x .= $this->getFrom($viewdata);
// 			$x .= $this->getToolbar($viewdata);
// 			$x .= $this->getResult($viewdata);
			$x .= $this->getFooter();
		}
		return $x;
	}


	public function getJS($viewdata){
		global $Lang;
		ob_start();
		?>
<!--		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>-->
		<script src="/templates/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
		<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
		<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">
		
		<script type="text/javascript" >

		var timerobj;
		function jsShowOrHideTags()
		{
			timerobj = setTimeout(ShowOrHideTagsAction,1000);
		}
		
		function ShowOrHideTagsAction()
		{
			$("#tags").focus();
			clearTimeout(timerobj);
		}
		
		function hideOptionLayer()
		{
			$('#formContent').attr('style', 'display: none');
			$('.spanHideOption').attr('style', 'display: none');
			$('.spanShowOption').attr('style', '');
			//document.getElementById('div_form').className = 'report_option report_hide_option';
		}
		
		function showOptionLayer()
		{
			$('#formContent').attr('style', '');
			$('.spanShowOption').attr('style', 'display: none');
			$('.spanHideOption').attr('style', '');
			//document.getElementById('div_form').className = 'report_option report_show_option';
		}
		
		function click_export()
		{
			document.form1.action = "?page=BookRanking&action=export";
			document.form1.submit();
			document.form1.action = "?page=BookRanking";
		}

		function click_print()
		{
			document.form1.action="?page=BookRanking&action=print";
			document.form1.target="_blank";
			document.form1.submit();
			
			document.form1.action="?page=BookRanking";
			document.form1.target="_self";
		}
		
		
		function ajaxSelectbox(page, fieldName){
				$.get(page, {},
					function(insertRespond){
						if (insertRespond == ''){
//							alert('Ajax call return nothing. Please try again. If problem persist, make sure the setting is completed / contact your administrator');
							fieldName.html('');
						}else {
							fieldName.html(insertRespond);
						}
					}
				);
		}
		
		function student_list(){
			if ($('#rankTarget').val() == 'student')
				{
				page = "?page=Ajax&function=getStudentByClass&classNames="+$('#classnames :selected').text();
				ajaxSelectbox(page, $('#studentIDs'));
				$('#studentIDs option').attr('selected', true);
				}
		}
		
		
		//--------------------------------->
		$().ready( function(){
		
			$.ajaxSetup({ cache:false,
					   async:false });
					   
			//---------------------------------------------------------------------------------------------------------------------- TargetUserSelection  Start
			ajaxSelectbox('?page=Ajax&function=getGroupName',$('#groupTarget'));
			ajaxSelectbox('?page=Ajax&function=getClassLevel', $('#classnames'));
			
			$('#findByGroup').change(function(){
				$('.table_findBy').toggle();
			});
			
			$('#findByStudent').change(function(){
				$('.table_findBy').toggle();
			});
			
			$('input[name="BookCategoryType"]').change(function(){
				if($('#BookCategoryTypeAll').is(':checked'))
				{
					$('#table_CategoryCode1').attr('style','display:none');
					$('#table_CategoryCode2').attr('style','display:none');
					$('#Categories option').attr('selected', false);
					$('#Categories2 option').attr('selected', false);
				}
				else if ($('#BookCategoryType1').is(':checked'))
				{				
					$('#table_CategoryCode1').attr('style','display:block');
					$('#table_CategoryCode2').attr('style','display:none');
					$('#Categories2 option').attr('selected', false);
				}
				else if ($('#BookCategoryType2').is(':checked'))
				{
					$('#table_CategoryCode1').attr('style','display:none');
					$('#table_CategoryCode2').attr('style','display:block');
					$('#Categories option').attr('selected', false);
				}
			});
						
					
			$('#rankTarget').change(function(){
				switch($('#rankTarget').val())
				{
				case 'form': 
					page = "?page=Ajax&function=getClassLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',true);
					$('#classnames option').attr('selected', true);
					$('#selectAllBtnClassnames').attr('style','display:inline');
					$('#Student').attr('style','display:none');	
					break;
				case 'class': 
					page= "?page=Ajax&function=getClassNameByLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',true);
					$('#classnames option').attr('selected', true);
					$('#selectAllBtnClassnames').attr('style','display:inline');
					$('#Student').attr('style','display:none');	
					break;
				case 'student': 
					page= "?page=Ajax&function=getClassNameByLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',false);
					$("#classname option:first").attr('selected','selected');
					student_list();
					$('#selectAllBtnClassnames').attr('style','display:none');
					$('#Student').attr('style','display:inline');	
					break;
				};
			});
			
			$('#classnames').change(function(){
				student_list();
			});
			//----------------------------------------------------------------------------------------------------------------------  TargetUserSelection  End 
			
			<?php if(is_null($viewdata['Result'])){ ?>
			$('#classnames option').attr('selected', true);
			//$('#CirCategory option').attr('selected', true);
			//$('#subject option').attr('selected', true);	
			//$('#Categories option').attr('selected', true);
			//$('#language option').attr('selected', true);
			$('#groupTarget option').attr('selected', true);
			$('#classnames option').attr('selected', true);
			<?php }?>
			
			<?php if (isset($viewdata['Result'])) {?>
				$('#formContent').attr('style','display: none');
			<? }?>
			
			//---------------------------------------------------------------------------------------------------------------------- Select All btn  Start
			$('#selectAllBtnClassnames').click(function(){ 			$('#classnames option').attr('selected', true);  		});
			$('#selectAllBtnGroups').click(function(){ 			$('#groupTarget option').attr('selected', true);  		});
			$('#selectAllBtnStudent').click(function(){ 			$('#studentIDs option').attr('selected', true);  		});
			$('#selectAllBtnCirCat').click(function(){ 				$('#CirCategory option').attr('selected', true);  		});
			$('#selectAllBtnCategories').click(function(){ 			$('#Categories option').attr('selected', true);			});
			$('#selectAllBtnCategories2').click(function(){ 		$('#Categories2 option').attr('selected', true);		});
			$('#selectAllBtnSubject').click(function(){ 			$('#subject option').attr('selected', true);			});
			$('#selectAllBtnLanguage').click(function(){ 			$('#language option').attr('selected', true);			});
			//---------------------------------------------------------------------------------------------------------------------- Select All btn  End
			
			
			$('#submit_result').click(function(){

				if (($('#findByGroup').attr('checked')) && (!$("#groupTarget option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_group']?>");
					return;					
				}
				if (($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'form') && (!$("#classnames option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_form']?>");
					return;					
				}										
				if (($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'class') && (!$("#classnames option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_class']?>");
					return;					
				}										
				if (($('#findByStudent').attr('checked')) && ($('#rankTarget').val() == 'student') && (!$("#studentIDs option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_student']?>");
					return;					
				}
				
				if (($('#radioPeriod_Date').attr('checked'))) {
					if (!check_date(document.getElementById("textFromDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
						return;
					if (!check_date(document.getElementById("textToDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
						return;
					if (!compare_date(document.getElementById("textFromDate"), document.getElementById("textToDate"),"<?=$Lang['General']['WrongDateLogic']?>"))
						return;	
				}
				else {
					if ( ($('#radioPeriod_Year').attr('checked')) && $("#data\\[AcademicYear\\]").val() == '' ) {					
						alert("<?=$Lang['libms']['report']['msg']['please_select_academic_year']?>");
						return;					
					}										
				}
				
				if ($('#BookCategoryType1').is(':checked')) {
					if($('#Categories option:checked').length == 0) {
						$('#Categories option').attr('selected', true);	
					}						
				}
				if ($('#BookCategoryType2').is(':checked')) {
					if($('#Categories2 option:checked').length == 0) {
						$('#Categories2 option').attr('selected', true);	
					}						
				}
				
				$.post("?page=BookRanking&action=getResult", $("#formContent").serialize(),
				function(data) {
					$('#formContent').attr('style','display: none');
					$('#form_result').html(data);		
					$('#spanShowOption').show();
					$('#spanHideOption').hide();
					$('#report_show_option').addClass("report_option report_hide_option");
					window.scrollTo(0, 0);
				});
				
			});
			

			if($("#tags").length > 0){
				$("#tags").autocomplete(
			      "ajax_get_tags_suggestion.php",
			      {
			  			delay:3,
			  			minChars:1,
			  			matchContains:1,
			  			onItemSelect: function() { jsShowOrHideTags(); },
			  			formatItem: function(row){ return row[0]; },
			  			autoFill:false,
			  			overflow_y: 'auto',
			  			overflow_x: 'hidden',
			  			maxHeight: '200px',
			  			width:'306px'
			  		}
			    );
			}

		});
		</script>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	/**
	 * get Toolbar HTML code:  e.g. export print  ...... 
	 * @param array $viewdata
	 */
	public function getToolbar($viewdata){
		global $Lang;
		//don't show if not posted.
		if (!isset($viewdata['Result']))
			return;
		
		ob_start();
		?>

		<div id='toolbox' class="content_top_tool">
			<div class="Conntent_tool">
				<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?></a>
				<a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?></a>
			</div>
			<br style="clear:both" />
		</div>
		<? 
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
		
	}
	
	public function getFrom($viewdata){
		global $Lang;

	
		ob_start();
		?>


			<div id="report_show_option">
				
				<span id="spanShowOption" class="spanShowOption" style="display:none">
					<a href="javascript:showOptionLayer();"><?=$Lang["libms"]["report"]["showoption"]?></a>
				</span> 
				
				<span id="spanHideOption" class="spanHideOption" style="display:none">
					<a href="javascript:hideOptionLayer();"><?=$Lang["libms"]["report"]["hideoption"]?></a>
				</span>

		
				<form name="form1" method="post" action="?page=BookRanking" onSubmit="return checkForm();"  id="formContent">
					
					<table class="form_table_v30">
				<? 
						echo $this->getForm_TargetUserSelection($viewdata);
						echo $this->getForm_Period($viewdata);
						echo $this->getForm_BookCirculationType($viewdata);
						echo $this->getForm_BookCategory($viewdata);
						echo $this->getForm_BookSubject($viewdata);
						echo $this->getForm_BookLanguage($viewdata);
						echo $this->getForm_ListNumber($viewdata);
						echo $this->getForm_BookTags();
				?>
					</table>
					
					<?=$this->linterface->MandatoryField();?>
					
					<div class="edit_bottom_v30">
						<p class="spacer"></p>
						<?= $this->linterface->GET_ACTION_BTN($Lang['libms']['report']['Submit'], "button", "", "submit_result")?>
						<p class="spacer"></p>
					</div>
				</form>
			</div>
			<div id="form_result"> </div>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_Period($viewdata){
		global $Lang, $junior_mck;
		$selectSchoolYearHTML =$viewdata['selectSchoolYearHTML'];
		$Period =$viewdata['Period'];
		$textFromDate = $viewdata['textFromDate'];
		$textToDate = $viewdata['textToDate'];
		if ($textFromDate=="" && $textToDate=="")
		{
			# preset date range as current school year
			$current_period= getPeriodOfAcademicYear(Get_Current_Academic_Year_ID());

			if (is_array($current_period) && sizeof($current_period)>0)
			{
				foreach($current_period as &$date){
					$date = date('Y-m-d',strtotime($date));
				}
				$textFromDate = $current_period['StartDate'];
				$textToDate = $current_period['EndDate'];
			} else
			{
				$textFromDate = (date('m')>=9) ? date('Y-09-01') : (date('Y')-1).'-09-01';
				$textToDate = date('Y-m-d');
			}
			
		}
		ob_start();
		?>	
		<!-- Period -->
		<tr valign="top">
		<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['time_period']?></td>
			<td>
				<table class="inside_form_table"  style='width: 350px;'>
<?php if (!isset($junior_mck)) {
	 ?> 
					<tr>
						<td colspan="6" >
							<input name="data[Period]" type="radio" id="radioPeriod_Year" value="YEAR" <? echo ($Period!="DATE")?" checked":"" ?>  >
							<?=$Lang['libms']['report']['SchoolYear']?>
							<?=getSelectAcademicYear("data[AcademicYear]", "");?>&nbsp;
							<?//=$Lang['libms']['report']['Semester']?>
							
						</td>
					</tr>
<?php } ?>		
					<tr>
						<td>
							<input name="data[Period]" type="radio" id="radioPeriod_Date" value="DATE" <? echo ($Period=="DATE" || isset($junior_mck))?" checked":"" ?> >
							<?=$Lang['libms']['report']['period_date_from']?>
						</td>
						<td >
							<?=$this->linterface->GET_DATE_PICKER("textFromDate",$textFromDate,"")?>
							<br />
							<span id='div_DateEnd_err_msg'></span>
						</td>
						<td><?=$Lang['libms']['report']['period_date_to']?> </td>
						<td >
							<?=$this->linterface->GET_DATE_PICKER("textToDate",$textToDate,"")?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<?	
		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}
	
	public function getForm_BookCirculationType($viewdata){
		global $Lang;
		global $linterface;
		
		$categoryElements =$viewdata['categoryElements'];
		$circulationLists = $viewdata['circulation'];

		ob_start();
		?>
		<tr>
			<td class="field_title"><?=$Lang['libms']['report']['circulation_type']?></td>
			<td>
							<span id="CategoryElements" >
							<select name="data[CirCategory][]" id="CirCategory" size="10" multiple >
							<?
									//if (!empty($categoryElements))
										foreach ($circulationLists as $key => $item) {
											if (isset($viewdata['post']['CirCategory']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['CirCategory']))
												$select=" selected ";
											else
												$select="";
											echo ("<option value='$key' $select>$item</option>");
										}
							?>
							</select>
							</span>
				<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnCirCat');?>
				<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
		
				<span id='div_Category_err_msg'></span>
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_BookCategory($viewdata){
		global $Lang;
		global $linterface;
		
		$Category =$viewdata['category'];
		//Henry Added [20150122]
		$Category1 =$viewdata['category1'];
		$Category2 =$viewdata['category2'];
		
		ob_start();
		?>
			<tr>
				<td class="field_title"><?=$Lang['libms']['report']['book_category']?></td>
				<td><!--
								<span id="Subjects">
									<select name="data[Categories][]" id="Categories" size="10" multiple><?php
 									
										if (!empty($Category)){
											foreach ($Category as $key => $item){
												if (isset($viewdata['post']['Categories']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['Categories'])) 
													$select=" selected "; 
												else  
													$select="";
												echo "<option value='$key' $select>$item</option>";
											}
										}
									?>
									</select>
								</span>
					<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnCategories');?>
					<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
					<span id='div_Category_err_msg'></span>
					-->
					
	        		<table class="inside_form_table">
						<tr>
							<td valign="top">
								<?
								$libms = new liblms();
								$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
								foreach ($result as $row){
									$book_category_name[] = htmlspecialchars($row['value']);
								}
								?>
								<?=$linterface->Get_Radio_Button("BookCategoryTypeAll", "BookCategoryType", 0, true, "", $Lang["libms"]["report"]["fine_status_all"])?>								
								<?=$linterface->Get_Radio_Button("BookCategoryType1", "BookCategoryType", 1, false, "", $book_category_name[0]?$book_category_name[0]:$Lang['libms']['report']['book_category'].' 1')?>
								<?=$linterface->Get_Radio_Button("BookCategoryType2", "BookCategoryType", 2, false, "", $book_category_name[1]?$book_category_name[1]:$Lang['libms']['report']['book_category'].' 2')?>
							</td>
						</tr>
						<tr class="table_CategoryCode" id="table_CategoryCode1" style='<?=($viewdata['post']['Categories']==1?'':'display:none;')?>'>
							<td valign="top">
								<span id="Subjects">
									<select name="data[Categories][]" id="Categories" size="10" multiple><?php
 									
										if (!empty($Category1)){
											foreach ($Category1 as $key => $item){
												if (isset($viewdata['post']['Categories']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['Categories'])) 
													$select=" selected "; 
												else  
													$select="";
												echo "<option value='$key' $select>$item</option>";
											}
										}
									?>
									</select>
								</span>
								<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnCategories');?>
								<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
								<span id='div_Category_err_msg'>
							</td>
						</tr>
						<tr class="table_CategoryCode" id="table_CategoryCode2" style='<?=($viewdata['post']['Categories']==2?'':'display:none;')?>'>
							<td valign="top">
								<span id="Subjects">
									<select name="data[Categories][]" id="Categories2" size="10" multiple><?php
 									
										if (!empty($Category2)){
											foreach ($Category2 as $key => $item){
												if (isset($viewdata['post']['Categories']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['Categories'])) 
													$select=" selected "; 
												else  
													$select="";
												echo "<option value='$key' $select>$item</option>";
											}
										}
									?>
									</select>
								</span>
								<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnCategories2');?>
								<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
								<span id='div_Category_err_msg'>
							</td>
						</tr>
					</table>
					
				</td>
			</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	
	
	public function getForm_BookSubject($viewdata){
		global $Lang;
		global $linterface;
		
		$subjectList =$viewdata['subjects'];
		ob_start();
		?>
		<tr>
			<td class="field_title"><?=$Lang['libms']['report']['subject']?></td>
			<td>
							<span id="Subjects">
								<select name="data[subject][]" id="subject" size="10" multiple >
								<?php
									//if (!empty($subject))
										foreach ($subjectList as $key => $item){
											if (isset($viewdata['post']['subject']) && in_array(htmlspecialchars_decode($key,ENT_QUOTES), $viewdata['post']['subject']))
												$select=" selected ";
											else{
												$select="";
// 												tolog(htmlspecialchars_decode($item,ENT_QUOTES));
// 												tolog($viewdata['post']['subject']);
											}
											echo "<option value='$key' $select>$item</option>";
										}
								?>
								</select>
							</span>
				<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnSubject');?>
				<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
		
				<span id='div_Category_err_msg'></span>
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_BookLanguage($viewdata){
		global $Lang;
		global $linterface;
		//$Subjects =$viewdata['Languages'];
		$languageList =$viewdata['language'];
		
		ob_start();
		?>
		<tr>
			<td class="field_title"><?=$Lang['libms']['report']['language']?></td>
			<td>
							<span id="Subjects">
								<select name="data[language][]" id="language" size="10" multiple><?php
									//if (!empty($language))
										foreach ($languageList as $key => $item){
											if (isset($viewdata['post']['language']) && in_array(htmlspecialchars_decode($item,ENT_QUOTES), $viewdata['post']['language']))
												$select=" selected ";
											else{
												$select="";
												}
												echo "<option value='$key' $select>$item</option>";
										}
								?>
								</select>
							</span>
				<?=$linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnLanguage');?>
				<span class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></span>
		
				<span id='div_Category_err_msg'></span>
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
			
	public function getForm_TargetUserSelection($viewdata){
		global $Lang;
		//todo
	
		$selectSchoolYearHTML =$viewdata['selectSchoolYearHTML'];
		$Period =$viewdata['Period'];
		$textFromDate = $viewdata['textFromDate'];
		$textToDate = $viewdata['textToDate'];
		
		
		ob_start();
		?>
		<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['target']?></td>
			<td>
				<table class="inside_form_table">
					<tr>
						<td valign="top">
							<?=$this->linterface->Get_Radio_Button("findByGroup", "data[findBy]", 'Group', true , "", $Lang['libms']['report']['findByGroup'])?>
							<?=$this->linterface->Get_Radio_Button("findByStudent", "data[findBy]", 'Student', false, "", $Lang['libms']['report']['findByStudent'])?>
						</td>
					</tr>
					<tr class="table_findBy">
						<td valign="top">
							<select name="data[groupTarget][]" id="groupTarget" multiple size="7" >
							<!-- Get_Select  -->
							</select>
							<?=$this->linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnGroups');?>
						</td>
						
					</tr>
					<tr class="table_findBy" style='display:none;'>
						<td valign="top">
							<select name="data[rankTarget]" id="rankTarget">
								<option value="form" selected><?=$Lang['libms']['report']['Form']?></option>
								<option value="class"><?=$Lang['libms']['report']['Class']?></option>
								<option value="student"><?=$Lang['libms']['report']['Student']?></option>
							</select>
						</td>
						<td valign="top" nowrap>
							<!-- Form / Class //-->
							<span id='classname'>
							<select name="data[classname][]" multiple id="classnames" size="7"></select>
							</span>
							<?=$this->linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnClassnames');?>

								
							<!-- Student //-->
							<span id='Student' style='display:none;'>
								<select name="data[studentID][]" multiple size="7" id="studentIDs"></select> 
								<?= $this->linterface->GET_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnStudent')?>
							</span>
							
						</td>
					</tr>
					<tr><td colspan="3" class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></td></tr>
				</table>
				<span id='div_Target_err_msg'></span>
			</td>
		</tr>
			
					
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_ListNumber($viewdata){
		global $Lang;
		ob_start();
		?>
		<tr>
			<td class="field_title"><?=$Lang["libms"]["report"]["ListNumber"]?></td>
			<td>
				<input id="listNo" name="data[listNo]" type="text" class="textboxnum" value="50" />
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}

	public function getForm_BookTags(){
		global $Lang;
		ob_start();
		?>
		<tr>
			<td class="field_title"><?=$Lang["libms"]["report"]["tags"]?></td>
			<td>
				<input autocomplete="off" id="tags" name="data[tags]" type="text" class="textbox_name" value="" />
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
		
	public function getResult($viewdata){
		global $Lang, $junior_mck;
		ob_start();
//debug_r($viewdata);		
		echo '<div id="result_body" style="margin: auto;width:98%" >';
		
		if ( $_REQUEST['action'] == 'print')
		{		
			//Added school name at top of the page
			$schoolName =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
			echo '<p>'.$schoolName.'</p><center><h1>'.$Lang["libms"]["reporting"]["PageReportingBookRanking"].'</h1></center>';
			
			// Print filter period info
			echo '<table border="0" width="100%"><tr><td>';
			echo  $Lang['libms']['report']['period_date_from'] . " " .
				$viewdata['post']['textFromDate'] . $Lang['libms']['report']['period_date_to'] . " " . $viewdata['post']['textToDate'];
			echo '</td><td align="right">';
			echo $Lang["libms"]["report"]["printdate"] . " ".date("Y-m-d").'</td></tr></table>';
		}
				
		echo '<div id="result_stat" style="margin: auto;" >';

			if (!(empty($viewdata['post'])) && (empty($viewdata['Result']))){
					echo $Lang["libms"]["report"]["norecord"];
			}elseif (!(empty($viewdata['post'])) && !(empty($viewdata['Result']))){
					echo $this->getRecord_RecordList($viewdata);
			}
		echo '</div>';
		echo '</div>';
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}

	public function getRecord_RecordList($viewdata){
	global $Lang;

	### build display table
			$x = "<table class='common_table_list_v30  view_table_list_v30' width='100%'>";
			$x .= "<tr>";
			$x .="<th width='1'>#</th>";
			$x .= "<th>" . $Lang['libms']['report']['ISBN']."</th>";
			$x .= "<th>" . $Lang['libms']['report']['CallNum']."</th>";
			$x .= "<th>" . $Lang['libms']['report']['booktitle']."</th>";
			$x .= "<th>" . $Lang['libms']['report']['ResponsibilityBy']."</th>";
			$x .= "<th>" . $Lang['libms']['report']['Publisher']."</th>";
			$x .= "<th>" . $Lang['libms']['report']['Edition']."</th>";
			$x .= "<th>" . $Lang['libms']['report']['LendingTotal']."</th>";
			$x .= "</tr>";
			$i = 0;
			foreach($viewdata['Result'] as $items)
			{		
				$x .= "<tr>";
				$x .= "<td>".++$i."</td>";
				$x .= "<td>".$items['ISBN']."</td>";
				$x .= "<td>".($items['CallNum'] == '' && $items['CallNum2'] ==''?' -- ':$items['CallNum']." ".$items['CallNum2'])."</td>";
				$x .= "<td>".$items['BookTitle']."</td>";
				$x .= "<td>".$items['ResponsibilityBy1']."</td>";
				$x .= "<td>".$items['Publisher']."</td>";
				$x .= "<td>".$items['Edition']."</td>";
				$x .= "<td>".$items['Total']."</td>";
				$x .= "</tr>";
			}
						
			$x .= "</table><br />";
				### build display table
		
	 return $x;
	}
	/**
	 * get Print view
	 * @param array $viewdata
	 * @return string
	 */
	public function getPrint($viewdata){
		global $Lang,$LAYOUT_SKIN;
		ob_start();
		include_once("{$_SERVER['DOCUMENT_ROOT']}/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
		?>
			<div id='toolbar' class= 'print_hide' style='text-align: right; width: 100%'>
					<? 
						$linterface = new interface_html();
						echo $linterface->GET_SMALL_BTN($Lang["libms"]["report"]["toolbar"]['Btn']['Print'], "button", "javascript:window.print();","print");
						
					?>
			</div>
					
			<?=$this->getResult($viewdata);?>
			</body>
			</html>
			<?
			
			$x = ob_get_contents();
			ob_end_clean();
			return $x;
		}
		
		/**
		 * get CSV string
		 * @param array $viewdata
		 * @return string
		 */
	public function getCSV($viewdata){
	global $Lang;
	
	$headers[] = $Lang['libms']['report']['ISBN'];
	$headers[] = $Lang['libms']['report']['CallNum'];
	$headers[] = $Lang['libms']['report']['booktitle'];
	$headers[] = $Lang['libms']['report']['ResponsibilityBy'];
	$headers[] = $Lang['libms']['report']['Publisher'];
	$headers[] = $Lang['libms']['report']['Edition'];
	$headers[] = $Lang['libms']['report']['LendingTotal'];
	
	$formatted_ary[] = $headers;
	foreach($viewdata['Result'] as $row){
		$formatted_ary[] = array(
			$row['ISBN'],
			($row['CallNum'] == '' && $row['CallNum2'] =='')?' -- ':$row['CallNum'].($row['CallNum2'] == '' ? '': " ".$row['CallNum2']),
			$row['BookTitle'],
			$row['ResponsibilityBy1'],
			$row['Publisher'],
			$row['Edition'],
			$row['Total']
		);
	}
	//array_unshift($formatted_ary,$headers);
	$csv = Exporter::array_to_CSV($formatted_ary);
	//dump($viewdata['Result']);
	return $csv;
	
	}
			
}
