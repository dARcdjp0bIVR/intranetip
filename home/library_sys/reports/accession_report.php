<?php

// Using:    

/***************************************
 *
 * Date: 2018-09-28 [Cameron]
 *  - add book tags filter [case #X138957]
 * 	- add suggestions for tags when typing  
 * 
 * Date: 2016-12-06 (Cameron)
 * 	- add filter PurchaseByDepartment
 *
 * Date: 2016-07-15 (Cameron)
 * 	- don't allow to run report if account date is invalid (IP2.5.7.7.1)
 * Date: 2016-07-04 (Cameron)
 * 	- fix bug: should exclude 'DELETE' record from LIBMS_BOOK when retrieving Subject & Language in get_xxxSelection_box(),
 * 	- should show Language Description rather than Code	when retrieving Language in get_xxxSelection_box()
 *  - should build option list even there's no record in $result in get_xxxSelection_box()
 *
 * Date: 2015-07-21 (Cameron)
 *	- fix bug on selected value in option list by changing comparison of selected value from "==" to "===" in get_xxxSelection_box()
 *	- add order by BookCategoryCode in get_xxxSelection_box() for BookCategory option list to compatible with other reports	
 *
 * Date: 2015-07-20 (Cameron)
 *	- Add function check_all_book_category()
 *	- Add radio option "All" to BookCategory
 *	- Show selected BookCategory based on checked BookCategoryType
 *
 * Date: 2015-06-11 (Cameron)
 * 	- Fix bug of unable to select all circulation items when click 'Select All' button
 *  	case #: 2015-0605-0929-25222  
 *  
 * Date: 2013-11-13 (Henry)
 * Details: Create this page
 ***************************************/
 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();

if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['accession report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$libms = new liblms();
$linterface = new interface_html();

# Current Page Info
$CurrentPageArr['LIBMS'] = 1;
$TAGS_OBJ[] = array($Lang["libms"]["GroupManagement"]["Right"]["reports"]["accession report"]);
$CurrentPage = "PageReportingAccessionReport";

# Top menu highlight setting
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

################### Stocktake Scheme Selection Box Start ######################

$linterface->LAYOUT_START();

if ($StartDate!="" && $EndDate!="" || ($from_acno!="" || $to_acno!="") || $searchType == 2)
{
	if($searchType == 0){
		$StartDate = "";
		$EndDate = "";
	}
	else if($searchType == 1){
		$from_acno = "";
		$to_acno = "";
	}
	else if($searchType == 2){
		$StartDate = "";
		$EndDate = "";
		$from_acno = "";
		$to_acno = "";
	}
	
	$tags = $_POST['tags'];
	
	$displayTable = $libms->get_accession_report(trim($from_acno), trim($to_acno), $StartDate, $EndDate, $CirculationTypeCode, $BookCategoryCode, $BookSubject, $BookLanguage, $BookStatus, $Task="DISPLAY", $enableLine, $BookCategoryType, $PurchaseByDepartment, $tags);
	
	$reportPeriodDisplay = $StartDate  . ' ' . $Lang['General']['To']  . ' ' .  $EndDate;
	
	if(trim($from_acno) != "" && trim($to_acno) !=""){
		$reportACNODisplay = strtoupper(trim($from_acno)).' '.$Lang['General']['To'].' '.strtoupper(trim($to_acno));
	}
	else if(trim($from_acno) != ""){
		$reportACNODisplay = strtoupper(trim($from_acno));
	}
	else{
		$reportACNODisplay = strtoupper(trim($to_acno));
	}
	$ShowReport = true;
} else
{
	$ShowReport = false;
}

//Get the academic year start date
$current_period= getPeriodOfAcademicYear(Get_Current_Academic_Year_ID());

foreach($current_period as &$date){
	$date = date('Y-m-d',strtotime($date));
}

$StartDate = empty($_REQUEST['StartDate'])? $current_period['StartDate']:$_REQUEST['StartDate'];
$EndDate = empty($_REQUEST['EndDate'])? date('Y-m-d'):$_REQUEST['EndDate'];

if(!isset($_REQUEST['searchType'])){
	$searchType = 1;
}

function get_xxxSelection_box($table, $fieldname, $selectedVal='', $id=''){
	global $Lang,$libms,$linterface;
//	BookCirculationType multi selection box
	
	if($table == "LIBMS_BOOK" && $fieldname =="BookSubject"){
		$sql = "SELECT DISTINCT `Subject` as ".$fieldname.", `Subject` as ".$Lang["libms"]["sql_field"]["Description"]."
			FROM
				`LIBMS_BOOK` 
			WHERE
				Subject IS NOT NULL
				AND Subject <> ''
				AND (RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL)
			ORDER by Subject ASC
			";
		$result = $libms->returnResultSet($sql);
	}
	else if($table == "LIBMS_BOOK" && $fieldname =="BookLanguage"){
//		$sql = "SELECT DISTINCT `Language` as ".$fieldname.", `Language` as ".$Lang["libms"]["sql_field"]["Description"]."
//			FROM
//				`LIBMS_BOOK` 
//			WHERE
//				Language IS NOT NULL
//				AND Language <> ''
//				AND (RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL)
//			ORDER by Language ASC
//			";
//		$result = $libms->returnResultSet($sql);

		$sql = "SELECT DISTINCT `Language` as ".$fieldname."
			FROM
				`LIBMS_BOOK` 
			WHERE
				Language IS NOT NULL
				AND Language <> ''
				AND (RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL)
			ORDER by Language ASC
			";
		$rs1 = $libms->returnResultSet($sql);
		
		$desc_field = $Lang["libms"]["sql_field"]["Description"];
		$sql = "SELECT DISTINCT `BookLanguageCode`, ".$desc_field." 
			FROM
				`LIBMS_BOOK_LANGUAGE` 
			ORDER by BookLanguageCode ASC";
		$rs2 = $libms->returnResultSet($sql);
		$rs2 = BuildMultiKeyAssoc($rs2,"BookLanguageCode");
		if (count($rs1) > 0) {
			foreach((array)$rs1 as $k=>$v) {
				$description = $rs2[$v[$fieldname]][$desc_field] ? $rs2[$v[$fieldname]][$desc_field] : $v[$fieldname];
				$result[$k][$desc_field] = $description;
				$result[$k][$fieldname] = $v[$fieldname];
			}
		}
		else {
			$result = array();
		}
	}
	else if($table == "LIBMS_BOOK_UNIQUE" && $fieldname =="PurchaseByDepartment"){
		$sql = "SELECT DISTINCT `PurchaseByDepartment` as ".$fieldname.", `PurchaseByDepartment` as ".$Lang["libms"]["sql_field"]["Description"]."
			FROM
				`LIBMS_BOOK_UNIQUE` 
			WHERE
				PurchaseByDepartment IS NOT NULL
				AND PurchaseByDepartment <> ''
				AND (RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL)
			ORDER by PurchaseByDepartment ASC
			";
		$result = $libms->returnResultSet($sql);
	}
	else if($table == "BOOK_STATUS"){
		$i=0;
		if(!empty($Lang['libms']['book_status'])){
			foreach ($Lang['libms']['book_status'] as $key => $value)
			{
				$result[$i][$fieldname] = $key;
				$result[$i][$Lang["libms"]["sql_field"]["Description"]] = $value;
				$i++;
			}
		}
	}
	else if($table == "LIBMS_BOOK_CATEGORY_1"){
		$fields[] = $fieldname;
		$fields[] = $Lang["libms"]["sql_field"]["Description"];		
		$result =  $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY',$fields, array( 'BookCategoryType' => '1'), null, 0, 'BookCategoryCode');
	}
	else if($table == "LIBMS_BOOK_CATEGORY_2"){
		$fields[] = $fieldname;
		$fields[] = $Lang["libms"]["sql_field"]["Description"];
		$result =  $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY',$fields, array( 'BookCategoryType' => '2'), null, 0, 'BookCategoryCode');
	}
	else{
		$fields[] = $fieldname;
		$fields[] = $Lang["libms"]["sql_field"]["Description"];
		$result =  $libms->SELECTFROMTABLE($table,$fields);
	}
//	if (!empty($result)){
		$classSel = '<select id="'.($id?$id:$fieldname).'" name="'.$fieldname.'[]" multiple="multiple" size="10">';
		$unclassify = array();
		$unclassify[$fieldname] = "";
		$unclassify[$Lang["libms"]["sql_field"]["Description"]] = $Lang["libms"]["report"]["cannot_define"];
		array_unshift($result, $unclassify);
		foreach($result as $aRecord){
			$selected = '';
			if(is_array($selectedVal)){
				foreach($selectedVal as $aSelectVal){
					if ($aRecord[$fieldname] === $aSelectVal) {
						$selected = 'selected="selected"';
					}
				}
			}
			else{
				if ($aRecord['CirculationTypeCode'] === $selectedVal) {
						$selected = 'selected="selected"';
				}
			}
			$classSel .='<option value="'.htmlspecialchars($aRecord[$fieldname]).'" '.$selected.'>'.$aRecord[$Lang["libms"]["sql_field"]["Description"]].'</option>';
		}
		$classSel .='</select>';
		if (($table == "LIBMS_BOOK_CATEGORY_1") || ($table == "LIBMS_BOOK_CATEGORY_2")) {
			$selectAllFieldID = $id; 
		}
		else {
			$selectAllFieldID = $fieldname;
		}
		$selectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('".$selectAllFieldID."', 1);");
		
				$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($classSel, $selectAllBtn, $SpanID='');
			
		return $x;
//	}
//	return false;
}
//echo get_xxxSelection_box('LIBMS_CIRCULATION_TYPE', 'CirculationTypeCode', array(100,132));
//echo get_xxxSelection_box('LIBMS_CIRCULATION_TYPE', 'CirculationTypeCode', 100);
//echo get_xxxSelection_box('LIBMS_CIRCULATION_TYPE', 'CirculationTypeCode');
?>

<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<script language="javascript">

var timerobj;
function jsShowOrHideTags()
{
	timerobj = setTimeout(ShowOrHideTagsAction,1000);
}

function ShowOrHideTagsAction()
{
	$("#tags").focus();
	clearTimeout(timerobj);
}

function check_all_book_category() {
	if ($('#BookCategoryType1').is(':checked')) {
		if($('#BookCategoryCode1 option:checked').length == 0) {
			$('#BookCategoryCode1 option').attr('selected', true);	
		}						
	}
	if ($('#BookCategoryType2').is(':checked')) {
		if($('#BookCategoryCode2 option:checked').length == 0) {
			$('#BookCategoryCode2 option').attr('selected', true);	
		}						
	}
}

function Js_Check_Form(obj){
	if(document.getElementById("searchType1").checked && document.getElementById("from_acno").value == '' && document.getElementById('to_acno').value ==''){
		alert("<?=$Lang["libms"]["book"]["alert"]["fillin_anco"]?>");
		document.getElementById('from_acno').focus();
	}
	else if(document.getElementById("searchType1").checked){
		check_all_book_category();
		obj.submit();
	}
//	else if(document.getElementById('to_acno').value ==''){
//		alert("<?=$Lang["libms"]["book"]["alert"]["fillin_anco"]?>");
//		document.getElementById('to_acno').focus();
//	}

	else if(document.getElementById("searchType2").checked && !check_date(document.getElementById("StartDate"),"<?=$Lang['General']['InvalidDateFormat']?>")) {
		return;		
	}
	else if(document.getElementById("searchType2").checked && !check_date(document.getElementById("EndDate"),"<?=$Lang['General']['InvalidDateFormat']?>")) {
		return;		
	}
	else if(document.getElementById("searchType2").checked && compare_date(document.getElementById("StartDate"), document.getElementById("EndDate"),"<?=$Lang['General']['WrongDateLogic']?>")){
		check_all_book_category();
		obj.submit();
	}
	else if(document.getElementById("searchType3").checked){
		check_all_book_category();
		obj.submit();
	}
}

function click_export()
{
	document.form1.action = "accession_report_export.php";
	document.form1.target = "_blank";
	document.form1.submit();
	
	document.form1.action = "accession_report.php";
	document.form1.target="_self";
}

function click_print()
{
	document.form1.action="accession_report_print.php";
	document.form1.target="_blank";
	document.form1.submit();
	
	document.form1.action = "accession_report.php";
	document.form1.target="_self";
}	

<?php if ($ShowReport) {?>
$(document).ready(function(){	
	$('#spanShowOption').show();	
});
<?php } ?>

function hideOptionLayer()
{
	$('#formContent').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
}

function showOptionLayer()
{
	$('#formContent').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
}

$(document).ready(function(){
	$('input[name="BookCategoryType"]').change(function(){
		if($('#BookCategoryTypeAll').is(':checked'))
		{
			$('#table_CategoryCode1').attr('style','display:none');
			$('#table_CategoryCode2').attr('style','display:none');
			$('#BookCategoryCode1 option').attr('selected', false);
			$('#BookCategoryCode2 option').attr('selected', false);
		}
		else if ($('#BookCategoryType1').is(':checked'))
		{				
			$('#table_CategoryCode1').attr('style','display:block');
			$('#table_CategoryCode2').attr('style','display:none');
			$('#BookCategoryCode2 option').attr('selected', false);
		}
		else if ($('#BookCategoryType2').is(':checked'))
		{
			$('#table_CategoryCode1').attr('style','display:none');
			$('#table_CategoryCode2').attr('style','display:block');
			$('#BookCategoryCode1 option').attr('selected', false);
		}
	});

	if($("#tags").length > 0){
		$("#tags").autocomplete(
	      "ajax_get_tags_suggestion.php",
	      {
	  			delay:3,
	  			minChars:1,
	  			matchContains:1,
	  			onItemSelect: function() { jsShowOrHideTags(); },
	  			formatItem: function(row){ return row[0]; },
	  			autoFill:false,
	  			overflow_y: 'auto',
	  			overflow_x: 'hidden',
	  			maxHeight: '200px',
	  			width:'306px'
	  		}
	    );
	}
	
});

</script>


<form name="form1" method="post" action="accession_report.php">

<?php if ($ShowReport) { ?>

<table width="99%" border="0" cellspacing="0" cellpadding="0">
<tr> 
  <td width="13" >&nbsp;</td>
  <td class="main_content">
 	<div class="report_option report_show_option">
 			<span id="spanShowOption" class="spanShowOption" style="display:none">
					<a href="javascript:showOptionLayer();"><?=$Lang["libms"]["report"]["showoption"]?></a>
				</span> 
				
				<span id="spanHideOption" class="spanHideOption" style="display:none">
					<a href="javascript:hideOptionLayer();"><?=$Lang["libms"]["report"]["hideoption"]?></a>
				</span>
 	</div>

	<span id="formContent" style="display:none" >
		<table width="99%" border="0" cellspacing="0" cellpadding="0">
			<tr> 
				<td width="13">&nbsp;</td>
				<td class="main_content">
		       	<div class="report_option report_option_title"></div>
				<div class="table_board">
				<table class="form_table">
					<tr>
			          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['bookmanagement']['scope']?></td>
			          <td><table class="inside_form_table"><tr><td><?=$linterface->Get_Radio_Button("searchType2", "searchType", 1, ($searchType==1?1:0), "", $Lang["libms"]["book"]["account_date"])?> <?=$linterface->GET_DATE_PICKER("StartDate", $StartDate)?> <span class="tabletextremark"></span> ~ <?=$linterface->GET_DATE_PICKER("EndDate", $EndDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td></tr>
			          <tr><td><?=$linterface->Get_Radio_Button("searchType1", "searchType", 0, ($searchType==0?1:0), "", $Lang["libms"]["report"]["ACNO"])?> <input type="text" id="from_acno" name="from_acno" value="<?=$_REQUEST['from_acno']?>"/> <span class="tabletextremark"></span> ~ <input type="text" id="to_acno" name="to_acno" value="<?=$_REQUEST['to_acno']?>"/></td></tr>
			          <tr><td><?=$linterface->Get_Radio_Button("searchType3", "searchType", 2, ($searchType==2?1:0), "", $Lang['libms']['report']['all_records'])?></td></tr></table></td>
			        </tr>
			        <tr>
			        	<td class="field_title"><?=$Lang["libms"]["report"]['EnableDeletionLine']?></td>
			        	<td><?=$linterface->Get_Radio_Button("enableLine1", "enableLine", 1, ($enableLine==1?1:0), "", $Lang['General']['Yes'])?> <?=$linterface->Get_Radio_Button("enableLine2", "enableLine", 0, ($enableLine==0?1:0), "", $Lang['General']['No'])?></td>
			        </tr>
			        <tr>
			        	<td class="field_title"><?=$Lang['libms']['report']['circulation_type']?></td>
			        	<td><?=get_xxxSelection_box('LIBMS_CIRCULATION_TYPE', 'CirculationTypeCode', ($CirculationTypeCode?$CirculationTypeCode:-1))?></td>
			        </tr>
			        <tr>
			        	<td class="field_title"><?=$Lang['libms']['report']['book_category']?></td>
			        	<td>
			        		<!--<?=$linterface->Get_Radio_Button("BookCategoryType1", "BookCategoryType", 1, ($BookCategoryType==1?1:0), "", $Lang['libms']['report']['book_category'].' 1')?> <?=$linterface->Get_Radio_Button("BookCategoryType2", "BookCategoryType", 2, ($BookCategoryType==2?1:0), "", $Lang['libms']['report']['book_category'].' 2')?><br/>
			        		<?=get_xxxSelection_box('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', ($BookCategoryCode?$BookCategoryCode:-1))?>-->
			        		<table class="inside_form_table">
								<tr>
									<td valign="top">
										<?
										$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
										foreach ($result as $row){
											$book_category_name[] = htmlspecialchars($row['value']);
										}
										?>
										<?=$linterface->Get_Radio_Button("BookCategoryTypeAll", "BookCategoryType", 0, ($BookCategoryType==0?true:false), "", $Lang["libms"]["report"]["fine_status_all"])?>								
										<?=$linterface->Get_Radio_Button("BookCategoryType1", "BookCategoryType", 1, ($BookCategoryType==1?true:false), "", $book_category_name[0]?$book_category_name[0]:$Lang['libms']['report']['book_category'].' 1')?>
										<?=$linterface->Get_Radio_Button("BookCategoryType2", "BookCategoryType", 2, ($BookCategoryType==2?true:false), "", $book_category_name[1]?$book_category_name[1]:$Lang['libms']['report']['book_category'].' 2')?>
									</td>
								</tr>
								<tr class="table_CategoryCode" id="table_CategoryCode1" style='<?=($BookCategoryType==1?'':'display:none;')?>'>								
									<td valign="top">
										<?=get_xxxSelection_box('LIBMS_BOOK_CATEGORY_1', 'BookCategoryCode', ($BookCategoryCode && ($BookCategoryType==1)?$BookCategoryCode:-1),"BookCategoryCode1")?>
									</td>
								</tr>
								<tr class="table_CategoryCode" id="table_CategoryCode2" style='<?=($BookCategoryType==2?'':'display:none;')?>'>
									<td valign="top">
										<?=get_xxxSelection_box('LIBMS_BOOK_CATEGORY_2', 'BookCategoryCode', ($BookCategoryCode && ($BookCategoryType==2)?$BookCategoryCode:-1),"BookCategoryCode2")?>
									</td>
								</tr>
							</table>
			        	</td>
			        </tr>
			        <tr>
			        	<td class="field_title"><?=$Lang['libms']['report']['subject']?></td>
			        	<td><?=get_xxxSelection_box('LIBMS_BOOK', 'BookSubject', ($BookSubject?$BookSubject:-1))?></td>
			        </tr>
			        <tr>
			        	<td class="field_title"><?=$Lang['libms']['report']['language']?></td>
			        	<td><?=get_xxxSelection_box('LIBMS_BOOK', 'BookLanguage', ($BookLanguage?$BookLanguage:-1))?></td>
			        </tr>
			        <tr>
			        	<td class="field_title"><?=$Lang["libms"]["report"]["book_status"]?></td>
			        	<td><?=get_xxxSelection_box('BOOK_STATUS', 'BookStatus', ($BookStatus?$BookStatus:-1))?></td>
			        </tr>
			        <tr>
			        	<td class="field_title"><?=$Lang["libms"]["book"]["purchase_by_department"]?></td>
			        	<td><?=get_xxxSelection_box('LIBMS_BOOK_UNIQUE', 'PurchaseByDepartment', ($PurchaseByDepartment?$PurchaseByDepartment:-1))?></td>
			        </tr>
            		<tr>
            			<td class="field_title"><?=$Lang["libms"]["report"]["tags"]?></td>
            			<td>
            				<input autocomplete="off" id="tags" name="tags" type="text" class="textbox_name" value="<?php echo $tags;?>" />
            			</td>
            		</tr>
			        
			        <!--<col class="field_title" />
			        <col  class="field_c" />
			        <tr>
			          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["fine_type"]?></td>
			          <td>
			          	<?=$linterface->Get_Radio_Button("RecordTypeAll", "RecordType", 'ALL', $RecordTypeSet["ALL"], "", $Lang["libms"]["report"]["fine_status_all"])?>
			          	&nbsp; <?=$linterface->Get_Radio_Button("RecordTypeOutstanding", "RecordType", 'OVERDUE', $RecordTypeSet["OVERDUE"], "", $Lang["libms"]["report"]["fine_type_overdue"])?>
			          	&nbsp; <?=$linterface->Get_Radio_Button("RecordTypeSettled", "RecordType", 'LOST', $RecordTypeSet["LOST"], "", $Lang["libms"]["report"]["fine_type_lost"])?>
			          </td>
			        </tr>
			        <tr>
			          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["fine_status"]?></td>
			          <td>
			          	<?=$linterface->Get_Radio_Button("RecordStatusAll", "RecordStatus", 'ALL', $RecordStatusSet["ALL"], "", $Lang["libms"]["report"]["fine_status_all"])?>
			          	&nbsp; <?=$linterface->Get_Radio_Button("RecordStatusOutstanding", "RecordStatus", 'OUTSTANDING', $RecordStatusSet["OUTSTANDING"], "", $Lang["libms"]["report"]["fine_outstanding"])?>
			          	&nbsp; <?=$linterface->Get_Radio_Button("RecordStatusSettled", "RecordStatus", 'SETTLED', $RecordStatusSet["SETTLED"], "", $Lang["libms"]["CirculationManagement"]["paied"])?>
			          	&nbsp; <?=$linterface->Get_Radio_Button("RecordStatusWaived", "RecordStatus", 'WAIVED', $RecordStatusSet["WAIVED"], "", $Lang["libms"]["report"]["fine_waived"])?>
			          </td>
			        </tr>-->
			     </table>
			     <?=$linterface->MandatoryField();?>
		      <p class="spacer"></p>
		    </div>
		    <p class="spacer"></p>
		      </div>
		      <div class="edit_bottom">
		        <p class="spacer"></p>
		        <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "Js_Check_Form(this.form)", "GenerateBtn")?>        
		        <p class="spacer"></p>
		      </div>
		      <p>&nbsp;</p></td>
		      <td width="11">&nbsp;</td>
		    </tr>
		  </table>
	</span>

	<div id='toolbox' class="content_top_tool">
	<div class="Conntent_tool">
		<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?></a>
		<a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?></a>
		</div>
		<br style="clear:both" />
	</div>

   	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<?=$linterface->GET_NAVIGATION2($Lang["libms"]["reporting"]["accessionReport"]);?>
				<p class="spacer"></p>
			    <p class="spacer"></p>
			    </td>
				<td valign="bottom"><div align="right"><table><?=($searchType == 0?'<tr><td>'.$Lang["libms"]["report"]["ACNO"] . ': </td><td>'.$reportACNODisplay.'</td></tr>':'')?><?=($searchType == 1?'<tr><td>'.$Lang['General']['Date'] . ': </td><td>'.$reportPeriodDisplay.'</td></tr>':'')?></table></div>
				</td>
			</tr>
		</table> 					    
		
		<?=$displayTable?>
  <p>&nbsp;</p></td>
  <td width="11" >&nbsp;</td>
</tr>
</table>

<?php } else { ?>
	
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td width="13">&nbsp;</td>
		<td class="main_content">
       	<div class="report_option report_option_title"></div>
		<div class="table_board">
		<table class="form_table">
			<tr>
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['libms']['bookmanagement']['scope']?></td>
	          <td><table class="inside_form_table"><tr><td><?=$linterface->Get_Radio_Button("searchType2", "searchType", 1, ($searchType==1?1:0), "", $Lang["libms"]["book"]["account_date"])?> <?=$linterface->GET_DATE_PICKER("StartDate", $StartDate)?> <span class="tabletextremark"></span> ~ <?=$linterface->GET_DATE_PICKER("EndDate", $EndDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td></tr>
	          <tr><td><?=$linterface->Get_Radio_Button("searchType1", "searchType", 0, ($searchType==0?1:0), "", $Lang["libms"]["report"]["ACNO"])?> <input type="text" id="from_acno" name="from_acno" value="<?=$_REQUEST['from_acno']?>"/> <span class="tabletextremark"></span> ~ <input type="text" id="to_acno" name="to_acno" value="<?=$_REQUEST['to_acno']?>"/></td></tr>
			  <tr><td><?=$linterface->Get_Radio_Button("searchType3", "searchType", 2, ($searchType==2?1:0), "", $Lang['libms']['report']['all_records'])?></td></tr></table></td>
			</tr>
	        <!--<tr>
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["book"]["account_date"]?></td>
	          <td><?=$linterface->Get_Radio_Button("searchType", "searchType", 1, 0, "", $Lang["libms"]["book"]["account_date"])?> <?=$linterface->GET_DATE_PICKER("StartDate", $StartDate)?> <span class="tabletextremark"></span> ~ <?=$linterface->GET_DATE_PICKER("EndDate", $EndDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
	        </tr>-->
	        <tr>
	        	<td class="field_title"><?=$Lang["libms"]["report"]['EnableDeletionLine']?></td>
	        	<td><?=$linterface->Get_Radio_Button("enableLine1", "enableLine", 1, ($enableLine==1?1:0), "", $Lang['General']['Yes'])?> <?=$linterface->Get_Radio_Button("enableLine2", "enableLine", 0, ($enableLine==0?1:0), "", $Lang['General']['No'])?></td>
	        </tr>
	        <tr>
	        	<td class="field_title"><?=$Lang['libms']['report']['circulation_type']?></td>
	        	<td><?=get_xxxSelection_box('LIBMS_CIRCULATION_TYPE', 'CirculationTypeCode', ($CirculationTypeCode?$CirculationTypeCode:-1))?></td>
	        </tr>
	        <tr>
	        	<td class="field_title"><?=$Lang['libms']['report']['book_category']?></td>
	        	<td>
	        		<table class="inside_form_table">
						<tr>
							<td valign="top">
								<?
								$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
								foreach ($result as $row){
									$book_category_name[] = htmlspecialchars($row['value']);
								}
								?>
								<?=$linterface->Get_Radio_Button("BookCategoryTypeAll", "BookCategoryType", 0, ($BookCategoryType==0?true:false), "", $Lang["libms"]["report"]["fine_status_all"])?>								
								<?=$linterface->Get_Radio_Button("BookCategoryType1", "BookCategoryType", 1, ($BookCategoryType==1?true:false), "", $book_category_name[0]?$book_category_name[0]:$Lang['libms']['report']['book_category'].' 1')?>
								<?=$linterface->Get_Radio_Button("BookCategoryType2", "BookCategoryType", 2, ($BookCategoryType==2?true:false), "", $book_category_name[1]?$book_category_name[1]:$Lang['libms']['report']['book_category'].' 2')?>
							</td>
						</tr>
						<tr class="table_CategoryCode" id="table_CategoryCode1" style='<?=($BookCategoryType==1?'':'display:none;')?>'>						
							<td valign="top">
								<?=get_xxxSelection_box('LIBMS_BOOK_CATEGORY_1', 'BookCategoryCode', ($BookCategoryCode && ($BookCategoryType==1)?$BookCategoryCode:-1),"BookCategoryCode1")?>
							</td>
						</tr>
						<tr class="table_CategoryCode" id="table_CategoryCode2" style='<?=($BookCategoryType==2?'':'display:none;')?>'>
							<td valign="top">
								<?=get_xxxSelection_box('LIBMS_BOOK_CATEGORY_2', 'BookCategoryCode', ($BookCategoryCode && ($BookCategoryType==2)?$BookCategoryCode:-1),"BookCategoryCode2")?>
							</td>
						</tr>
					</table>
	        	</td>
	        </tr>
	        <tr>
	        	<td class="field_title"><?=$Lang['libms']['report']['subject']?></td>
	        	<td><?=get_xxxSelection_box('LIBMS_BOOK', 'BookSubject', ($BookSubject?$BookSubject:-1))?></td>
	        </tr>
	        <tr>
	        	<td class="field_title"><?=$Lang['libms']['report']['language']?></td>
	        	<td><?=get_xxxSelection_box('LIBMS_BOOK', 'BookLanguage', ($BookLanguage?$BookLanguage:-1))?></td>
	        </tr>
	        <tr>
	        	<td class="field_title"><?=$Lang["libms"]["report"]["book_status"]?></td>
	        	<td><?=get_xxxSelection_box('BOOK_STATUS', 'BookStatus', ($BookStatus?$BookStatus:-1))?></td>
	        </tr>
	        <tr>
	        	<td class="field_title"><?=$Lang["libms"]["book"]["purchase_by_department"]?></td>
	        	<td><?=get_xxxSelection_box('LIBMS_BOOK_UNIQUE', 'PurchaseByDepartment', ($PurchaseByDepartment?$PurchaseByDepartment:-1))?></td>
	        </tr>
    		<tr>
    			<td class="field_title"><?=$Lang["libms"]["report"]["tags"]?></td>
    			<td>
    				<input autocomplete="off" id="tags" name="tags" type="text" class="textbox_name" value="<?php echo $tags;?>" />
    			</td>
    		</tr>
	        
	        <!--<col class="field_title" />
	        <col  class="field_c" />
	        <tr>
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["fine_type"]?></td>
	          <td>
	          	<?=$linterface->Get_Radio_Button("RecordTypeAll", "RecordType", 'ALL', $RecordTypeSet["ALL"], "", $Lang["libms"]["report"]["fine_status_all"])?>
	          	&nbsp; <?=$linterface->Get_Radio_Button("RecordTypeOutstanding", "RecordType", 'OVERDUE', $RecordTypeSet["OVERDUE"], "", $Lang["libms"]["report"]["fine_type_overdue"])?>
	          	&nbsp; <?=$linterface->Get_Radio_Button("RecordTypeSettled", "RecordType", 'LOST', $RecordTypeSet["LOST"], "", $Lang["libms"]["report"]["fine_type_lost"])?>
	          </td>
	        </tr>
	        <tr>
	          <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["fine_status"]?></td>
	          <td>
	          	<?=$linterface->Get_Radio_Button("RecordStatusAll", "RecordStatus", 'ALL', $RecordStatusSet["ALL"], "", $Lang["libms"]["report"]["fine_status_all"])?>
	          	&nbsp; <?=$linterface->Get_Radio_Button("RecordStatusOutstanding", "RecordStatus", 'OUTSTANDING', $RecordStatusSet["OUTSTANDING"], "", $Lang["libms"]["report"]["fine_outstanding"])?>
	          	&nbsp; <?=$linterface->Get_Radio_Button("RecordStatusSettled", "RecordStatus", 'SETTLED', $RecordStatusSet["SETTLED"], "", $Lang["libms"]["CirculationManagement"]["paied"])?>
	          	&nbsp; <?=$linterface->Get_Radio_Button("RecordStatusWaived", "RecordStatus", 'WAIVED', $RecordStatusSet["WAIVED"], "", $Lang["libms"]["report"]["fine_waived"])?>
	          </td>
	        </tr>-->
	     </table>
	     <?=$linterface->MandatoryField();?>
      <p class="spacer"></p>
    </div>
    <p class="spacer"></p>
      </div>
      <div class="edit_bottom">
        <p class="spacer"></p>
        <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "Js_Check_Form(this.form)", "GenerateBtn")?>        
        <p class="spacer"></p>
      </div>
      <p>&nbsp;</p></td>
      <td width="11">&nbsp;</td>
    </tr>
  </table>

<?php } ?>
  
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
