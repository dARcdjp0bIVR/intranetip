<?php
# modifying by: 

/**
 * Log :
 * 
 * Date:	2019-07-23 [Henry]
 * 			- use the local library of jquery
 * 
 * Date:	2016-07-15 [Cameron]
 * 			- hide the warning and empty option list if it returns nothing in ajaxSelectbox (IP2.5.7.7.1)
 * 			- don't allow to run report if user do not select target and period
 * 			- fix bug: total number of student should be 0 if there's no record
 * Date: 	2014-10-07 (Henry)
 * 			Added function getForm_UserGender()
 * Date: 	2014-05-14 [Henry]
 * 			Added school name at top of the page
 * Date 	2013-05-07 [Yuen]
 * 			Improved the layout
 * Date 	2013-05-06 [Cameron]
 * 			Show filter period when print
 * Date:	2013-04-24 [yuen]
 * 			preset date range as current year
 * 
 * Date:	2013-04-23 [Cameron]
 * 			Add date validation function before submit criteria for report: $('#submit_result').click(function()
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../";
$MODULE_ROOT = "../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

require_once('ReportView.php');

global $Lang;
$linterface = new interface_html("libms.html");

class ReaderInactiveReportView extends ReportView{
	public function __construct(){
	
		$CurrentPageArr['LIBMS'] = 1;
		$CurrentPage = "PageReportingReaderInactive";
		parent::__construct($CurrentPage);
	}
	
	public function getView($viewdata = null){
		global $Lang;
	
		if (empty($print)){
			$x .= $this->getHeader();
			$x .= $this->getJS($viewdata);
			$x .= $this->getFrom($viewdata);
			$x .= $this->getFooter();
		}
		return $x;
	}


	public function getJS($viewdata){
		global $Lang;
		ob_start();
		?>
		<link href="css/visualize.css" type="text/css" rel="stylesheet" />
		<link href="css/visualize-light.css" type="text/css" rel="stylesheet" />
		<link href="css/reports.css" type="text/css" rel="stylesheet" />
		<script src="/templates/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/number_rounding.js"></script>
		<script type="text/javascript" src="js/visualize.jQuery.js"></script>	
		<script type="text/javascript" >
		
		function hideOptionLayer()
		{
			$('#formContent').attr('style', 'display: none');
			$('.spanHideOption').attr('style', 'display: none');
			$('.spanShowOption').attr('style', '');
			//document.getElementById('div_form').className = 'report_option report_hide_option';
		}
		
		function showOptionLayer()
		{
			$('#formContent').attr('style', '');
			$('.spanShowOption').attr('style', 'display: none');
			$('.spanHideOption').attr('style', '');
			//document.getElementById('div_form').className = 'report_option report_show_option';
		}
		
		function click_export()
		{
			document.form1.action = "?page=ReaderInactive&action=export";
			document.form1.submit();
		}

		function click_print()
		{
			var url = "?page=ReaderInactive&action=print&show=list";
			if ($('#result_stat').css('display') == 'none') {
				url = "?page=ReaderInactive&action=print&show=chart";
			}
			document.form1.action=url;
			document.form1.target="_blank";
			document.form1.submit();
			
		}
		
		
		function ajaxSelectbox(page, fieldName){
				$.get(page, {},
					function(insertRespond){
						if (insertRespond == ''){
//							alert('Ajax call return nothing. Please try again. If problem persist, make sure the setting is completed / contact your administrator');
							fieldName.html('');
						}else {
							fieldName.html(insertRespond);
						}
					}
				);
		}
		
		function student_list(){
			if ($('#rankTarget').val() == 'student')
				{
				page = "?page=Ajax&function=getStudentByClass&classNames="+$('#classnames :selected').text();
				ajaxSelectbox(page, $('#studentIDs'));
				$('#studentIDs option').attr('selected', true);
				}
		}
		
		
		//--------------------------------->
		$().ready( function(){
		
			$.ajaxSetup({ cache:false,
					   async:false });
					   
			//---------------------------------------------------------------------------------------------------------------------- TargetUserSelection  Start
			ajaxSelectbox('?page=Ajax&function=getGroupName',$('#groupTarget'));
			ajaxSelectbox('?page=Ajax&function=getClassLevel', $('#classnames'));
			
			$('#findByGroup').change(function(){
				$('.table_findBy').toggle();
			});
			
			$('#findByStudent').change(function(){
				$('.table_findBy').toggle();
			});
					
			$('#rankTarget').change(function(){
				switch($('#rankTarget').val())
				{
				case 'form': 
					page = "?page=Ajax&function=getClassLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',true);
					$('#classnames option').attr('selected', true);
					$('#selectAllBtnClassnames').attr('style','display:inline');
					$('#Student').attr('style','display:none');	
					break;
				case 'class': 
					page= "?page=Ajax&function=getClassNameByLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',true);
					$('#classnames option').attr('selected', true);
					$('#selectAllBtnClassnames').attr('style','display:inline');
					$('#Student').attr('style','display:none');	
					break;
				case 'student': 
					page= "?page=Ajax&function=getClassNameByLevel";
					ajaxSelectbox(page, $('#classnames'));
					$('#classnames').attr('multiple',false);
					$("#classname option:first").attr('selected','selected');
					student_list();
					$('#selectAllBtnClassnames').attr('style','display:none');
					$('#Student').attr('style','display:inline');	
					break;
				};
			});
			
			$('#classnames').change(function(){
				student_list();
			});
			//----------------------------------------------------------------------------------------------------------------------  TargetUserSelection  End 
			
			<?php if(is_null($viewdata['Result'])){ ?>
			$('#classnames option').attr('selected', true);
			$('#groupTarget option').attr('selected', true);
			$('#classnames option').attr('selected', true);
			<?php }?>
			
			<?php if (isset($viewdata['Result'])) {?>
				$('#formContent').attr('style','display: none');
			<? }?>
			
			//---------------------------------------------------------------------------------------------------------------------- Select All btn  Start
			$('#selectAllBtnClassnames').click(function(){ 			$('#classnames option').attr('selected', true);  		});
			$('#selectAllBtnGroups').click(function(){ 			$('#groupTarget option').attr('selected', true);  		});
			$('#selectAllBtnStudent').click(function(){ 			$('#studentIDs option').attr('selected', true);  		});

			//---------------------------------------------------------------------------------------------------------------------- Select All btn  End
			
			
			$('#submit_result').click(function(){
				
				if (($('#rankTarget').val()=="form")&&(!$("#classnames option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_form']?>");
					return;					
				}
				if (($('#rankTarget').val()=="class")&&(!$("#classnames option:selected").length )) {
					alert("<?=$Lang['libms']['report']['msg']['please_select_class']?>");
					return;					
				}
				
				if (($('#radioPeriod_Date').attr('checked'))) {
					if (!check_date(document.getElementById("textFromDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
						return;
					if (!check_date(document.getElementById("textToDate"),"<?=$Lang['General']['InvalidDateFormat']?>"))
						return;
					if (!compare_date(document.getElementById("textFromDate"), document.getElementById("textToDate"),"<?=$Lang['General']['WrongDateLogic']?>"))
						return;	
				}
				else {
					if ( ($('#radioPeriod_Year').attr('checked')) && $("#data\\[AcademicYear\\]").val() == '' ) {					
						alert("<?=$Lang['libms']['report']['msg']['please_select_academic_year']?>");
						return;					
					}										
				}
				
				$.post("?page=ReaderInactive&action=getResult", $("#formContent").serialize(),
					function(data) {
						$('#formContent').attr('style','display: none');
						$('#form_result').html(data);		
						$('#spanShowOption').show();
						$('#spanHideOption').hide();
						$('#report_show_option').addClass("report_option report_hide_option");
						$('.visual_chart').each(function (){
							$div = $('<div style="page-break-inside: avoid;" />')
							barchart_colors = ['#be1e2d','#666699','#92d5ea','#ee8310','#8d10ee','#5a3b16','#26a4ed','#f45a90','#e9e744','#ce1e2d','#766699','#a2d5ea','#fe8310','#9d10ee','#6a3b16','#36a4ed','#045a90','#f9e744'];
							$(this).visualize({type: 'bar', width: '660px', height : '200px', colors : barchart_colors}).appendTo($div);
							$div.appendTo($('#result_chart'));
						});
						window.scrollTo(0, 0);
						$('.result_toggler').click(function(){
							$('.result_toggler').toggleClass('thumb_list_tab_on');
							$('#result_stat').toggle();
							$('#result_chart').toggle();
							return false;
						});
	
						$('#result_chart').toggle();
					});
				
			});
			

		});
		</script>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	/**
	 * get Toolbar HTML code:  e.g. export print  ...... 
	 * @param array $viewdata
	 */
	public function getToolbar($viewdata){
		global $Lang;
		//don't show if not posted.
		if (!isset($viewdata['Result']))
			return;
		
		ob_start();
		?>

		<div id='toolbox' class="content_top_tool">
			<div class="Conntent_tool">
				<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?></a>
				<a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?></a>
			</div>
			<br style="clear:both" />
		</div>
		<? 
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
		
	}
	
	public function getFrom($viewdata){
		global $Lang;

	
		ob_start();
		?>


			<div id="report_show_option">
				
				<span id="spanShowOption" class="spanShowOption" style="display:none">
					<a href="javascript:showOptionLayer();"><?=$Lang["libms"]["report"]["showoption"]?></a>
				</span> 
				
				<span id="spanHideOption" class="spanHideOption" style="display:none">
					<a href="javascript:hideOptionLayer();"><?=$Lang["libms"]["report"]["hideoption"]?></a>
				</span>

		
				<form name="form1" method="post" action="?page=ReaderRanking" onSubmit="return checkForm();"  id="formContent">
					
					<table class="form_table_v30">
				<? 
						echo $this->getForm_TargetUserSelection($viewdata);
						echo $this->getForm_UserGender($viewdata);
						echo $this->getForm_Order($viewdata);
						echo $this->getForm_Period($viewdata);
						echo $this->getForm_LoanTotal($viewdata);
				?>
					</table>
					
					<?=$this->linterface->MandatoryField();?>
					
					<div class="edit_bottom_v30">
						<p class="spacer"></p>
						<?= $this->linterface->GET_ACTION_BTN($Lang['libms']['report']['Submit'], "button", "", "submit_result")?>
						<p class="spacer"></p>
					</div>
				</form>
			</div>
			<div id="form_result"> </div>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_Order($viewdata){
		global $Lang;
		ob_start();
		?>
		<tr id='tr_sortByType'>
			<td class="field_title"><?=$Lang["libms"]["report"]['listTotalNumberOfPerson']?></td>
			<td>
				<?=$this ->linterface->Get_Radio_Button("sortByForm", "data[sortBy][type]", 'ClassLevel', true, "", $Lang['libms']['report']['Form'])?>
				<?=$this ->linterface->Get_Radio_Button("sortByClass", "data[sortBy][type]", 'ClassName', false, "", $Lang['libms']['report']['Class'])?>
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_LoanTotal($viewdata){
		global $Lang, $junior_mck;
		$TotalAtMost =$viewdata['TotalAtMost'];
		
		for ($i=0; $i<=50; $i++)
		{
			if ($TotalAtMost>0 && $TotalAtMost!="")
			{
				$checked = ($i==$TotalAtMost) ? "checked='checked'" : "" ;
			}
			$select_options .= "<option value='{$i}' {$checked}>{$i}</option>\n";
		}		
		
		ob_start();
		?>	
		<!-- Period -->
		<tr valign="top">
		<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["libms"]["report"]["loans_less_than"]?></td>
			<td>
				<select name="TotalAtMost" id="TotalAtMost">
				<?=$select_options?>
				</select>
				<?= $Lang["libms"]["report"]["loans_less_than_unit"] ?>
			</td>
		</tr>
		<?	
		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}
	
	public function getForm_Period($viewdata){
		global $Lang, $junior_mck;
		$selectSchoolYearHTML =$viewdata['selectSchoolYearHTML'];
		$Period =$viewdata['Period'];
		$textFromDate = $viewdata['textFromDate'];
		$textToDate = $viewdata['textToDate'];
		if ($textFromDate=="" && $textToDate=="")
		{
			# preset date range as current school year
			$current_period= getPeriodOfAcademicYear(Get_Current_Academic_Year_ID());

			if (is_array($current_period) && sizeof($current_period)>0)
			{
				foreach($current_period as &$date){
					$date = date('Y-m-d',strtotime($date));
				}
				$textFromDate = $current_period['StartDate'];
				$textToDate = $current_period['EndDate'];
			} else
			{
				$textFromDate = (date('m')>=9) ? date('Y-09-01') : (date('Y')-1).'-09-01';
				$textToDate = date('Y-m-d');
			}
			
		}
		ob_start();
		?>	
		<!-- Period -->
		<tr valign="top">
		<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['time_period']?></td>
			<td>
				<table class="inside_form_table"  style='width: 350px;'>
<?php if (!isset($junior_mck)) {
	 ?> 
					<tr>
						<td colspan="6" >
							<input name="data[Period]" type="radio" id="radioPeriod_Year" value="YEAR" <? echo ($Period!="DATE")?" checked":"" ?>  >
							<?=$Lang['libms']['report']['SchoolYear']?>
							<?=getSelectAcademicYear("data[AcademicYear]", "");?>&nbsp;
							<?//=$Lang['libms']['report']['Semester']?>
							
						</td>
					</tr>
<?php } ?>		
					<tr>
						<td>
							<input name="data[Period]" type="radio" id="radioPeriod_Date" value="DATE" <? echo ($Period=="DATE" || isset($junior_mck))?" checked":"" ?> >
							<?=$Lang['libms']['report']['period_date_from']?>
						</td>
						<td >
							<?=$this->linterface->GET_DATE_PICKER("textFromDate",$textFromDate,"")?>
							<br />
							<span id='div_DateEnd_err_msg'></span>
						</td>
						<td><?=$Lang['libms']['report']['period_date_to']?> </td>
						<td >
							<?=$this->linterface->GET_DATE_PICKER("textToDate",$textToDate,"")?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<?	
		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}

	public function getForm_TargetUserSelection($viewdata){
		global $Lang;
		//todo
	
		$selectSchoolYearHTML =$viewdata['selectSchoolYearHTML'];
		$Period =$viewdata['Period'];
		$textFromDate = $viewdata['textFromDate'];
		$textToDate = $viewdata['textToDate'];
		
		
		ob_start();
		?>
		<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['libms']['report']['target']?></td>
			<td>
				<table class="inside_form_table">
					<tr style="display: none;">
						<td valign="top">
							<?//=$this->linterface->Get_Radio_Button("findByGroup", "data[findBy]", 'Group', false , "", $Lang['libms']['report']['findByGroup'])?>
							<?=$this->linterface->Get_Radio_Button("findByStudent", "data[findBy]", 'Student', true, "", $Lang['libms']['report']['findByStudent'])?>
						</td>
					</tr>
					<tr class="table_findBy" style='display:none;'>
						<td valign="top">
							<select name="data[groupTarget][]" id="groupTarget" multiple size="7" >
							<!-- Get_Select  -->
							</select>
							<?=$this->linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnGroups');?>
						</td>
						
					</tr>
					<tr class="table_findBy" >
						<td valign="top">
							<select name="data[rankTarget]" id="rankTarget">
								<option value="form" selected><?=$Lang['libms']['report']['Form']?></option>
								<option value="class"><?=$Lang['libms']['report']['Class']?></option>
								<!-- <option value="student"><?=$Lang['libms']['report']['Student']?></option> -->
							</select>
						</td>
						<td valign="top" nowrap>
							<!-- Form / Class //-->
							<span id='classname'>
							<select name="data[classname][]" multiple id="classnames" size="7"></select>
							</span>
							<?=$this->linterface->GET_SMALL_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnClassnames');?>

								
							<!-- Student //-->
							<span id='Student' style='display:none;'>
								<select name="data[studentID][]" multiple size="7" id="studentIDs"></select> 
								<?= $this->linterface->GET_BTN($Lang['libms']['report']['selectAll'], "button", "", 'selectAllBtnStudent')?>
							</span>
							
						</td>
					</tr>
					<tr><td colspan="3" class="tabletextremark"><?=$Lang['libms']['report']['PressCtrlKey']?></td></tr>
				</table>
				<span id='div_Target_err_msg'></span>
			</td>
		</tr>
			
					
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getForm_UserGender($viewdata){
		global $Lang;
		ob_start();
		?>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["report"]["Gender"]?></td>
			<td>
				<?=$this->linterface->Get_Radio_Button("genderA", "data[gender]", 'A', true , "", $Lang["libms"]["report"]["fine_status_all"])?>
				<?=$this->linterface->Get_Radio_Button("genderM", "data[gender]", 'M', false , "", $Lang["libms"]["report"]["Male"])?>
				<?=$this->linterface->Get_Radio_Button("genderF", "data[gender]", 'F', false, "", $Lang["libms"]["report"]["Female"])?>
			</td>
		</tr>
		<?
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
		
	public function getResult($viewdata){
		global $Lang, $junior_mck;
		ob_start();
		echo '<div id="result_body" style="margin: auto;width:98%" >';
		
		if ( $_REQUEST['action'] == 'print')
		{
			//Added school name at top of the page
			$schoolName =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
			echo '<p>'.$schoolName.'</p><center><h1>'.$Lang['libms']['reporting']['PageReportingReaderInactive'].'</h1></center>';
		
			// Print filter period info
			echo '<table border="0" width="100%"><tr><td>';
			echo  $Lang['libms']['report']['period_date_from'] . " " .
				$viewdata['post']['textFromDate'] . $Lang['libms']['report']['period_date_to'] . " " . $viewdata['post']['textToDate'];
			echo '</td><td align="right">';
			echo $Lang["libms"]["report"]["printdate"] . " ".date("Y-m-d").'</td></tr></table>';
		}
		
		echo '<div id="result_stat"  >';
		

			if (!(empty($viewdata['post'])) && (empty($viewdata['Result']))){
					echo $Lang["libms"]["report"]["norecord"];
			}elseif (!(empty($viewdata['post'])) && !(empty($viewdata['Result']))){
					echo $this->getRecord_RecordList($viewdata);
			}
		echo '</div>';
		echo '<div id="result_chart"  >';
		echo '</div>';
		echo '</div>';		
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}

	public function getRecord_RecordList($viewdata){
		global $Lang;
		
		
		
		### build display table
	
		$x = "<table class='common_table_list_v30  view_table_list_v30' width='100%'>";
			$x .= "<tr>";
			$x .= "<th>" . $Lang["libms"]["report"]["ClassName"] ."</th>";
			$x .= "<th>" . $Lang['libms']['report']['ClassNumber']."</th>";
			$x .= "<th>" . $Lang["libms"]["CirculationManagement"]["user_barcode"]."</th>";
			$x .= "<th>" . $Lang['libms']['report']['ChineseName']."</th>";
			$x .= "<th>" . $Lang['libms']['report']['EnglishName']."</th>";
			$x .= "<th><center>" . $Lang["libms"]["report"]["LendingTotal"] ."</center></th>";
			$x .= "</tr>";
			$prevResult = '';
			$countTotal = 0;
			$sortBy = $viewdata['Result'][0]['sortBy'];
			foreach($viewdata['Result'] as $items)
			{	
				if (!empty($items['userID'])) {
					//count the total number of student by class or form
					
					if($prevResult && $prevResult != $items[$sortBy]){
						$x .= "<tr>";
						$x .= "<td align='center' colspan='6'>".$Lang["libms"]["report"]['TotalNumberOfStudent']." (".$prevResult."): ".$countTotal."</td>";
						$x .= "</tr>";
						$countTotal = 0;
					}
					$countTotal++;
					$prevResult = $items[$sortBy];	
					$x .= "<tr>";
					$x .= "<td>".$items['ClassName']."</td>";
					$x .= "<td>".$items['ClassNumber']."</td>";
					$x .= "<td>".$items['BarCode']."</td>";
					$x .= "<td>".$items['ChineseName']."</td>";
					$x .= "<td>".$items['EnglishName']."</td>";
					$x .= "<td align='center'>".$items['BorrowTotal']."</td>";
					$x .= "</tr>";
				}
			}
			$x .= "<tr>";
			$x .= "<td align='center' colspan='6'>".$Lang["libms"]["report"]['TotalNumberOfStudent']." (".$prevResult."): ".$countTotal."</td>";
			$x .= "</tr>";
						
			$x .= "</table><br/>";
		### build display table
		
	 	return $x;
	}
	/**
	 * get Print view
	 * @param array $viewdata
	 * @return string
	 */
	public function getPrint($viewdata){
		global $Lang,$LAYOUT_SKIN;
		ob_start();
		include_once("{$_SERVER['DOCUMENT_ROOT']}/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
		?>
		<link href="css/visualize.css" type="text/css" rel="stylesheet" />
		<link href="css/visualize-light.css" type="text/css" rel="stylesheet" />
		<link href="css/reports.css" type="text/css" rel="stylesheet" />
		<script src="/templates/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/number_rounding.js"></script>
		<script type="text/javascript" src="js/visualize.jQuery.js"></script>	
		<script type="text/javascript" >
			$(function(){
				$('.visual_chart').each(function (){
					$div = $('<div style="page-break-inside: avoid;" />')
					barchart_colors = ['#be1e2d','#666699','#92d5ea','#ee8310','#8d10ee','#5a3b16','#26a4ed','#f45a90','#e9e744','#ce1e2d','#766699','#a2d5ea','#fe8310','#9d10ee','#6a3b16','#36a4ed','#045a90','#f9e744'];
					$(this).visualize({type: 'bar', width: '660px', height : '200px', colors : barchart_colors}).appendTo($div);
					$div.appendTo($('#result_chart'));
				});
				window.scrollTo(0, 0);
				$('.result_toggler').click(function(){
					$('.result_toggler').toggleClass('thumb_list_tab_on');
					$('#result_stat').toggle();
					$('#result_chart').toggle();
					return false;
				});

				$('#result_chart').toggle();

				if ('<?=$_GET['show']?>' == 'chart')
					$('.result_toggler:first').click();
			});
		</script>
			<div id='toolbar' class= 'print_hide' style='text-align: right; width: 100%'>
					<? 
						$linterface = new interface_html();
						echo $linterface->GET_SMALL_BTN($Lang["libms"]["report"]["toolbar"]['Btn']['Print'], "button", "javascript:window.print();","print");
						
					?>
			</div>
					
			<?=$this->getResult($viewdata);?>
			</body>
			</html>
			<?
			
			$x = ob_get_contents();
			ob_end_clean();
			return $x;
		}
		
		/**
		 * get CSV string
		 * @param array $viewdata
		 * @return string
		 */
		public function getCSV($viewdata){
			global $Lang;
			
			$headers[] = $Lang["libms"]["report"]["ClassName"];
			$headers[] = $Lang['libms']['report']['ClassNumber'];
			$headers[] = $Lang["libms"]["CirculationManagement"]["user_barcode"];
			$headers[] = $Lang['libms']['report']['ChineseName'];
			$headers[] = $Lang['libms']['report']['EnglishName'];
			$headers[] = $Lang["libms"]["report"]["LendingTotal"];
			
			
			$formatted_ary[] = $headers;
			$prevResult = '';
			$countTotal = 0;
			$sortBy = $viewdata['Result'][0]['sortBy'];
			foreach($viewdata['Result'] as $items)
			{	
				if (!empty($items['userID'])) {
					//count the total number of student by class or from
					if($prevResult && $prevResult != $items[$sortBy]){
						$formatted_ary[] = array('','',$Lang["libms"]["report"]['TotalNumberOfStudent'].' ('.$prevResult.')', $countTotal);
						$countTotal = 0;
					}
					$countTotal++;
					$prevResult = $items[$sortBy];
					$formatted_ary[] = array($items['ClassName'], $items['ClassNumber'], $items['BarCode'], $items['ChineseName'], $items['EnglishName'], $items['BorrowTotal']);
				}
			}
			$formatted_ary[] = array('','',$Lang["libms"]["report"]['TotalNumberOfStudent'].' ('.$prevResult.')', $countTotal);
			
			//array_unshift($formatted_ary,$headers);
			$csv = Exporter::array_to_CSV($formatted_ary);

			return $csv;
			
		}
			
}
        