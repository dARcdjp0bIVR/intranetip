<?php
/*
 * Editing by  
 * 
 * Modification Log: 
 * 
 * 2015-05-11 (Cameron)
 * 			- fix bug for showing item details by changing page_size
 * 
 * 2015-03-23 (Cameron)
 * 			- show series number and circulation type description from LIBMS_BOOK if that of items is not set (follow bibliography)
 * 		
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
####### Trim all request #######
function recursive_trim(&$array){
	foreach($array as &$item){
		if (is_array($item)){
			recursive_trim($item);
		}else{
			if (is_string($item))
				$item=trim($item);
		}
	}
}
recursive_trim($_REQUEST);
####### END  OF   Trim all request #######
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
################################################################################################################

## Get Data
$BookID = (isset($BookID) && $BookID != '') ? $BookID : '';


## Use Library
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


if($BookID != '') 
{
	$pageNo = 99999;
	$field = 5;
	$order = 1;
	$li = new libdbtable2007($field, $order, $pageNo);
	
	
	$BookID_For_CONCAT = (isset($junior_mck)) ? "CONVERT(i.UniqueID USING utf8)" : "i.UniqueID";
	$DBCirculationTitle = ($intranet_session_language=="en") ? "DescriptionEn" : "DescriptionChi";

	//FromPage=book&BookID=16571&UniqueID=217369
	## DB SQL
	$sql = "select 
				i.ACNO, 
				CONCAT('<a href=\"book_item_edit.php?FromPage=book&BookID={$BookID}&UniqueID=',".$BookID_For_CONCAT.",'\" ><font color=\"#6677FF\">',b.BookTitle,'</font></a>') as BookTitleLink,  
				i.BarCode, ";
	$sql .= "if (i.ItemSeriesNum='' OR i.ItemSeriesNum is NULL, 
			concat('".$Lang["libms"]["bookmanagement"]["follow_parent"]."',' - ',if (b.SeriesNum='' OR b.SeriesNum is NULL,'".$Lang["libms"]["bookmanagement"]["unnumbered"]."', b.SeriesNum)), i.ItemSeriesNum) as ItemSeriesNum,
			 i.Distributor, " .
			 " if (i.CirculationTypeCode='' OR i.CirculationTypeCode is NULL, 
				concat('".$Lang["libms"]["bookmanagement"]["follow_parent"]."',' - ',if (kc.".$DBCirculationTitle."='' OR kc.".$DBCirculationTitle." is null,'".$Lang["libms"]["bookmanagement"]["unclassified"]."',kc.".$DBCirculationTitle.")),
				bc.".$DBCirculationTitle.") as CirculationType, ";

	# Column - Book Status 
	$sql .= "	case ";
	if(!empty($Lang["libms"]["book_status"])){
		foreach($Lang["libms"]["book_status"] as $status_key => $status_val){
			$sql .= "	when i.RecordStatus = '".$status_key."' then '".$status_val."' ";
		}
	}
	$sql .= "	else '--' end as RecordStatus, ";
	$sql .= "	IF(i.CreationDate <> '0000-00-00' and i.CreationDate <> '', DATE_FORMAT(i.CreationDate, '%Y-%m-%d'), '') as CreationDate 
			from LIBMS_BOOK_UNIQUE as i 
			inner join LIBMS_BOOK as b on 
				b.BookID = i.BookID LEFT JOIN LIBMS_CIRCULATION_TYPE AS bc ON bc.CirculationTypeCode=i.CirculationTypeCode
				LEFT JOIN LIBMS_CIRCULATION_TYPE AS kc ON kc.CirculationTypeCode=b.CirculationTypeCode
			where i.BookID = '".$BookID."' ";
	$sql .= " and i.RecordStatus NOT IN ('LOST',  'WRITEOFF',  'SUSPECTEDMISSING', 'DELETE') ";
//	echo $sql;die();
	
	
	## Data Table
	//global $eclass_prefix;
	$li->db = $eclass_prefix . "eClass_LIBMS";
	 
	$li->no_navigation_row = true;
	$li->field_array = array("ACNO", "BookTitle", "BarCode", "ItemSeriesNum", "Distributor", "CirculationType", "RecordStatus", "CreationDate");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+1;
	$li->title = $eDiscipline["Record"];
	$li->column_array = array(0,0);
	$li->wrap_array = array(0,0);
	$li->IsColOff = "IP25_table";
	
	$rs = $li->returnArray($sql);
	$li->page_size = (count($rs) > $li->page_size) ? count($rs) : $li->page_size; 
	
	
	$pos = 0;
	$li->column_list .= "<th width='1%' class='tabletoplink'>#</td>\n";
	$li->column_list .= "<th width='10%' >".$Lang['libms']['book']['code']."</td>\n";
	$li->column_list .= "<th width='15%' >".$Lang['libms']['book']['title']."</td>\n";
		$li->column_list .= "<th width='10%' >".$Lang['libms']['book']['barcode']."</td>\n";
	$li->column_list .= "<th width='10%' >".$Lang["libms"]["book"]["series_number"]."</td>\n";
	$li->column_list .= "<th width='10%' >".$Lang['libms']['book']['distributor']."</td>\n";
	$li->column_list .= "<th width='10%' >". $Lang["libms"]["settings"]["circulation_type"]."</td>\n";
	$li->column_list .= "<th width='5%' >".$Lang['libms']['bookmanagement']['bookStatus']."</td>\n";
	$li->column_list .= "<th width='10%' >".$Lang['libms']['book']['item_account_date']."</td>\n";
	/*
	$li->column_list .= "<th width='14%' >".$li->column($pos++, $Lang['libms']['book']['code'])."</td>\n";
	$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['libms']['book']['title'])."</td>\n";
	$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['libms']['book']['barcode'])."</td>\n";
	$li->column_list .= "<th width='14%' >".$li->column($pos++, $Lang['libms']['book']['distributor'])."</td>\n";
	$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['bookStatus'])."</td>\n";
	$li->column_list .= "<th width='13%' >".$li->column($pos++, $Lang['libms']['book']['creation_date'])."</td>\n";
	*/
	echo $li->display();
}


################################################################################################################
intranet_closedb();
?>