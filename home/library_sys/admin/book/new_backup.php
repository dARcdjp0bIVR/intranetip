<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

# using: 

############ Change Log Start ###############
#
#	Date:	2012-06-07	Jason
#			
#
############ Change Log End ###############

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();


$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagement";

$libms->MODULE_AUTHENTICATION($CurrentPage);

$linterface = new interface_html("libms.html");

############################################################################################################


$TAGS_OBJ[] = array($Lang['libms']['action']['new_book']);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$linterface->LAYOUT_START();


#Save and Cancel Button
$SaveBtn   = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "SubmitBtn", "");
$CancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'", "CancelBtn", "");

# for example ... 
$ResponsibilityCodeArrary[] = array("W", "Write");
$ResponsibilityCodeArrary[] = array("T", "Translate");
$ResponsibilityCodeOption1 = $linterface->GET_SELECTION_BOX($ResponsibilityCodeArrary, " name='ResponsibilityCodeOption1' id='ResponsibilityCodeOption1' onChange='disable_enable_the_follow_input(1)' ", $Lang['libms']['status']['na'], $Status);

# for example ...
$DummyArrary[] = array("BK", "Book");
$DummyArrary[] = array("CD", "CD");
$DummyArrary[] = array("MZ", "Magazine");
$DummyOption1 = $linterface->GET_SELECTION_BOX($DummyArrary, " name='Dummy' id='Dummy' ", $Lang['libms']['status']['na'], $Status);
?>


<form name="form1" method="POST" action="new_update.php">
<table width="90%" border="0" align="center">
	<tr> 
		<td class="main_content">
			<div class="table_board">
                <p class="spacer"></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="tabletextrequire">*</span> <?= $Lang['libms']['book']['title'] ?></td>
                        	<td width="33%"><input type="text" class="textboxtext" maxlength="128" /></td>
                        	<td width="4%">&nbsp;</td>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['subtitle'] ?></span></td>
                         	<td width="33%"><input type="text" class="textboxtext" maxlength="64" /></td>
						</tr>
						<tr>
                        	<td class="field_title_short"><span class="tabletextrequire">*</span> <?= $Lang['libms']['book']['code'] ?></td>
                        	<td><input type="text" class="textboxtext" maxlength="16" /></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['barcode'] ?></span></td>
                         	<td><input type="text" class="textboxtext" maxlength="64" /></td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['call_number'] ?></span></td>
                        	<td><input type="text" class="textboxtext" maxlength="64" /></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['call_number2'] ?></span></td>
                         	<td><input type="text" class="textboxtext" maxlength="64" /></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['ISBN'] ?></span></td>
                        	<td><input type="text" class="textboxtext" maxlength="64" /></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['ISBN2'] ?></span></td>
                         	<td><input type="text" class="textboxtext" maxlength="64" /></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['language'] ?></span></td>
                        	<td><input type="text" class="textboxtext" maxlength="32" /></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['country'] ?></span></td>
                         	<td><input type="text" class="textboxtext" maxlength="128" /></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['introduction'] ?></span></td>
                        	<td colspan="4"><textarea class="textboxtext"></textarea></td>
						</tr>		
                  	</tbody>
				</table>
				
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_publish'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['edition'] ?></span></td>
                        	<td width="33%"><input type="text" class="textboxtext" maxlength="32" /></td>
                        	<td width="4%">&nbsp;</td>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['publish_year'] ?></span></td>
                         	<td width="33%"><input type="text" class="textboxnum" maxlength="4" /></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['publisher'] ?></span></td>
                        	<td><input type="text" class="textboxtext" maxlength="32" /></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['publish_place'] ?></span></td>
                         	<td><input type="text" class="textboxtext" maxlength="64" /></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['series'] ?></span></td>
                        	<td><input type="text" class="textboxtext" maxlength="64" /></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['series_number'] ?></span></td>
                         	<td><input type="text" class="textboxtext" maxlength="32" /></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_code1'] ?></span></td>
                        	<td><?=$ResponsibilityCodeOption1?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_by1'] ?></span></td>
                         	<td><input type="text" class="textboxtext" maxlength="64" /></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_code2'] ?></span></td>
                        	<td><?=$ResponsibilityCodeOption1?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_by2'] ?></span></td>
                         	<td><input type="text" class="textboxtext" maxlength="64" /></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_code3'] ?></span></td>
                        	<td><?=$ResponsibilityCodeOption1?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_by3'] ?></span></td>
                         	<td><input type="text" class="textboxtext" maxlength="64" /></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['number_of_page'] ?></span></td>
                        	<td><input type="text" class="textboxnum" maxlength="4" /></td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
						</tr>
                  	</tbody>
				</table>
				
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_category'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" ><span class="field_title"><?= $Lang['libms']['settings']['book_category'] ?></span></td>
                        	<td width="33%"><?=$DummyOption1?></td>
                        	<td width="4%">&nbsp;</td>
                        	<td width="15%" ><span class="field_title"><?= $Lang['libms']['settings']['circulation_type'] ?></span></td>
                         	<td width="33%"><?=$DummyOption1?></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['settings']['resources_type'] ?></span></td>
                        	<td><?=$DummyOption1?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['settings']['book_location'] ?></span></td>
                         	<td><?=$DummyOption1?></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['subject'] ?></span></td>
                        	<td><input type="text" class="textboxtext" maxlength="32" /></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['account_date'] ?></span></td>
                         	<td><input type="text" class="textboxnum" maxlength="10" value="<?=date("Y-m-d")?>" /> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['remark_internal'] ?></span></td>
                         	<td><input type="text" class="textboxtext" maxlength="32" /></td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
						</tr>
                  	</tbody>
				</table>
				
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_purchase'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_date'] ?></span></td>
                        	<td width="33%"><input type="text" class="textboxnum" maxlength="10" /> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
                        	<td width="4%">&nbsp;</td>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_price'] ?></span></td>
                         	<td width="33%"><input type="text" class="textboxnum" maxlength="8" /></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['list_price'] ?></span></td>
                        	<td><input type="text" class="textboxnum" maxlength="8" /></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['discount'] ?></span></td>
                         	<td><input type="text" class="textboxnum" maxlength="8" /></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['distributor'] ?></span></td>
                        	<td><input type="text" class="textboxtext" maxlength="64" /></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_by_department'] ?></span></td>
                         	<td><input type="text" class="textboxtext" maxlength="32" /></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_note'] ?></span></td>
                        	<td><input type="text" class="textboxtext" maxlength="32" /></td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
						</tr>
                  	</tbody>
				</table>
				
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_circulation'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['number_of_copy'] ?></span></td>
                        	<td width="33%"><input type="text" class="textboxnum" maxlength="2" /></td>
                        	<td width="4%">&nbsp;</td>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['number_of_copy_available'] ?></span></td>
                         	<td width="33%"><input type="text" class="textboxnum" maxlength="2" /></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['borrow_reserve'] ?></span></td>
                        	<td><input type="checkbox" id="BorrowReserve1" name="BorrowReserve1" value="OpenBorrow" /> <label for="BorrowReserve1"><?=$Lang['libms']['book']['open_borrow']?></label> &nbsp; 
                        		<input type="checkbox" id="BorrowReserve2" name="BorrowReserve2" value="OpenReserve" /> <label for="BorrowReserve2"><?=$Lang['libms']['book']['open_reserve']?></label>
                        	</td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['remark_to_user'] ?></span></td>
                         	<td><input type="text" class="textboxtext" maxlength="32" /></td>
						</tr>
                  	</tbody>
				</table>
				
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_other'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td  class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['cover_image'] ?></span></td>
                        	<td colspan="3"><input type="file" /></td>
						</tr>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['tags'] ?></span></td>
                        	<td width="70%"><input type="text" class="textboxtext" maxlength="255" /></td>
                        	<td width="15%"> <span class="tabletextremark"><?=$Lang['libms']['book']['tags_input']?></span></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['URL'] ?></span></td>
                        	<td ><input type="text" class="textboxtext" maxlength="255" /></td>
                        	<td>&nbsp;</td>
						</tr>
                  	</tbody>
				</table>
				
	<?=$linterface->MandatoryField();?>
				<center>
                    <p class="spacer"></p>
                    <?= $SaveBtn ?>
                    <?= $CancelBtn ?>
                </center>
					<p class="spacer"></p>
			</div>
		</td>
	</tr>
</table>
<input type="hidden" name="userID" id="userID" value="<?=$userID?>">
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
</form><?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
