<?php
// using: 

// IMPORTANT: UTF-8 Encoding
/*
 * 	Log
 * 
 * 	Date:	2015-10-06 [Cameron]
 * 			- fix bug on handling special characters in keyword search and option list: &<>'"\ 
 * 
 */


if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
################################################################################################################
if(!isset($BookID) || empty($BookID)){
	header("Location: index.php");
}
### Set Cookies
$arrCookies = array();
$arrCookies[] = array("elibrary_plus_mgmt_item_page_field", "field");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_order", "order");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_size", "numPerPage");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_number", "pageNo");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_keyword", "keyword");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_book_status", "BookStatus");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
{
	updateGetCookies($arrCookies);
}
################################################################################################################

## Get Data
$keyword = (isset($keyword) && $keyword != '') ? trim($keyword) : '';
$FromPage = (isset($FromPage) && $FromPage != '') ? $FromPage : 'book';
if (isset($elibrary_plus_mgmt_book_page_size) && $elibrary_plus_mgmt_book_page_size != "") 
	$page_size = $elibrary_plus_mgmt_book_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

## Use Library
# in all boook management PHP pages
$libms = new liblms();

# Access Checking
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$li = new libdbtable2007($field, $order, $pageNo);


## Init 
$btnOption = ''; 
$StatusSelection = '';
$CurTab = 'InfoPurchased';

## Preparation
$CurrentPageArr['LIBMS'] = 1;
//$CurrentPage = "PageBookManagementBookList";
$CurrentPage = $libms->get_book_manage_cur_page($FromPage);
$libms->MODULE_AUTHENTICATION($CurrentPage);
//$TAGS_OBJ[] = array($Lang["libms"]["action"]["item"]);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$TAGS_OBJ[] = array($Lang["libms"]["action"][strtolower($FromPage)]);

$BookID_For_CONCAT = (isset($junior_mck)) ? "CONVERT(i.UniqueID USING utf8)" : "i.UniqueID";



## DB SQL
$sql = "SELECT 
			ACNO,  
			PurchaseDate, 
			PurchasePrice, 
			ListPrice, 
			Discount, 
			Distributor, 
			PurchaseByDepartment, 
			InvoiceNumber, 
			PurchaseNote
		FROM LIBMS_BOOK_UNIQUE 
		WHERE 
			(RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL) 
		AND
			BookID = '".$BookID."'
		";
# DB - condition
$keyword_cond ='';
if($keyword != "")
	$unconverted_keyword_sql = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));		// A&<>'"\B ==> A&<>\'\"\\\\B
	$converted_keyword_sql = special_sql_str($keyword);											// A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B

	//for Distributor filter
	$keyword_cond = " AND (
		InvoiceNumber LIKE '%$unconverted_keyword_sql%' OR InvoiceNumber LIKE '%$converted_keyword_sql%' OR 
		ACNO LIKE '%$unconverted_keyword_sql%' OR ACNO LIKE '%$converted_keyword_sql%' OR 
		PurchaseByDepartment LIKE '%$unconverted_keyword_sql%' OR PurchaseByDepartment LIKE '%$converted_keyword_sql%' OR 
		PurchaseNote LIKE '%$unconverted_keyword_sql%' OR PurchaseNote LIKE '%$converted_keyword_sql%'
	) "; 
	$sql .= $keyword_cond;
//echo $sql;die();

## InvoiceSelect Sql Cond
if($InvoiceSelect != ''){
	if($InvoiceSelect =='NA'){
		$sql .= " AND (InvoiceNumber = '' OR InvoiceNumber is null)" ;
	}else{
		$unconverted_InvoiceSelect = mysql_real_escape_string($InvoiceSelect);
		$converted_InvoiceSelect = addslashes(htmlspecialchars($InvoiceSelect));											
		$sql .= " AND (InvoiceNumber = '".$unconverted_InvoiceSelect."' OR InvoiceNumber = '".$converted_InvoiceSelect."') " ;
	}
}

## PurchaseByDepartment Sql Cond
if($DepartmentSelect != ''){
	if($DepartmentSelect =='NA'){
		$sql .= " AND (PurchaseByDepartment = '' OR PurchaseByDepartment is null)" ;
	}else{
		$unconverted_DepartmentSelect = mysql_real_escape_string($DepartmentSelect);
		$converted_DepartmentSelect = addslashes(htmlspecialchars($DepartmentSelect));											
		$sql .= " AND (PurchaseByDepartment = '".$unconverted_DepartmentSelect."' OR PurchaseByDepartment = '".$converted_DepartmentSelect."') " ;
	}
}

## Data Table
//global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";

// BookdCode = ACNO 
$li->field_array = array("ACNO", "PurchaseDate", "PurchasePrice", "ListPrice", "Discount", "Distributor", "PurchaseByDepartment", "InvoiceNumber", "PurchaseNote");

$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";
$li->no_navigation_row = true;

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['code'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['purchase_date'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['purchase_price'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['list_price'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['discount'])."</th>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang['libms']['book']['distributor'])."</th>\n";
$li->column_list .= "<th width='13%' >".$li->column($pos++, $Lang['libms']['book']['purchase_by_department'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['invoice_number'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['purchase_note'])."</th>\n";

$keyword = htmlspecialchars($keyword);
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"{$keyword}\" onkeyup=\"Check_Go_Search(event);\" />";



############################################################################################################

# Top menu highlight setting
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

# InvoiceNumber filter
$InvoiceSelectionBox = '';

$sql1 = "SELECT 
			DISTINCT InvoiceNumber
		FROM
			LIBMS_BOOK_UNIQUE
		WHERE 
			(RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL) 
		AND
			BookID = '".$BookID."'
		{$keyword_cond}";
//debug_pr($sql1);
$invoiceInfo = $libms->returnArray($sql1);

$numOfInvoiceInfo = count($invoiceInfo);
$invoiceSelectionOption = array();
$invoiceSelectionOption[] = array('NA', $Lang["libms"]["status"]["na"]);
for($i=0;$i<$numOfInvoiceInfo;$i++){
	if($invoiceInfo[$i]['InvoiceNumber'] != '')
	$invoiceSelectionOption[]= array($invoiceInfo[$i]['InvoiceNumber'], $invoiceInfo[$i]['InvoiceNumber']);
}
$InvoiceSelectionBox = $linterface->GET_SELECTION_BOX($invoiceSelectionOption, 'name="InvoiceSelect" id="InvoiceSelect" onchange="document.form1.submit()"', "-- {$Lang['libms']['book']['invoice_number']} --", htmlspecialchars($InvoiceSelect));
$StatusSelection .= $InvoiceSelectionBox;
# Department filter
$DeptSelectionBox = '';

$sql1 = "SELECT 
			DISTINCT PurchaseByDepartment
		FROM
			LIBMS_BOOK_UNIQUE
		WHERE 
			(RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL) 
		AND
			BookID = '".$BookID."'
		{$keyword_cond}";
//debug_pr($sql1);
$departmentInfo = $libms->returnArray($sql1);

$numOfDeptInfo = count($departmentInfo);
$deptSelectionOption = array();
$deptSelectionOption[] = array('NA', $Lang["libms"]["status"]["na"]);
for($i=0;$i<$numOfDeptInfo;$i++){
	if($departmentInfo[$i]['PurchaseByDepartment'] != '')
	$deptSelectionOption[]= array($departmentInfo[$i]['PurchaseByDepartment'], $departmentInfo[$i]['PurchaseByDepartment']);
}
$DeptSelectionBox = $linterface->GET_SELECTION_BOX($deptSelectionOption, 'name="DepartmentSelect" id="DepartmentSelect" onchange="document.form1.submit()"', "-- {$Lang['libms']['book']['purchase_by_department']} --", htmlspecialchars($DepartmentSelect));
$StatusSelection .= $DeptSelectionBox;


$BookSubTab = $libms->get_book_manage_sub_tab($FromPage,$CurTab,$BookID,$UniqueID);

$RespondInfo = $libms->GET_BOOK_INFO($BookID);
$BookTitle = $RespondInfo[0]['BookTitle'];

$htmlAry['cancelBtn'] = '';
if (strtolower($FromPage)=='periodical') {
	$htmlAry['cancelBtn'] = $libms->get_book_manage_cancel_button($BookID, $FromPage);
}
?>
<table class="form_table" width="100%">
<tr>
  <td align="left"><font face="微軟正黑體" size="+1">《 <b><?=$BookTitle?></b>》</font></td>
</tr>
</table>
<br style="clear:both" />	
<?=$BookSubTab?>
<form name="form1" id="form1" method="POST" action="info_purchase.php?FromPage=<?=$FromPage?>&BookID=<?=$BookID?>&UniqueID=<?=$UniqueID?>" onSubmit="return checkForm()">
	<div class="content_top_tool"  style="float: left;">

		</div>
	<div class="content_top_tool"  style="float: right;">
		<div class="Conntent_search"><?=$searchTag?></div>     
		<br style="clear:both" />
	</div>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td valign="bottom">
		<?=$StatusSelection?>
	</td>

	</tr>
	</table>

	<?=$li->display();?>
	<br />
	
	<div class="edit_bottom_v30">
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
	
	<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
	<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
	<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
	<input type="hidden" name="page_size_change" id="page_size_change" value="" />
	<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
	<input type="hidden" name="FromPage" id="FromPage" value="<?=$FromPage?>" />
</form>


<?php
$linterface->LAYOUT_STOP();

################################################################################################################
intranet_closedb();
?>