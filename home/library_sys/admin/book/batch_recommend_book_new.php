<?php

//modifying:
/*
 * 	@Purpose:	Batch add recommend books
 *  
 * 	Log
 * 
 * 	Date:	2017-01-09 [Cameron]
 * 			- add auto complete function for following fields: book title, author, book tag
 * 
 * 	Date:	2016-11-11 [Cameron]
 * 			- add Tag in checking "ByOthers" filter
 * 
 * 	Date:	2016-09-20 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."home/library_sys/admin/book/batch_modify_manage.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageMgmtBatchEdit";

$libms->MODULE_AUTHENTICATION($CurrentPage);

$TAGS_OBJ = $libms->get_batch_edit_tabs("RECOMMEND");

$linterface = new interface_html();
$bmm = new batch_modify_manage();
	
$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;
$bmm->setfield($field);
$bmm->setorder($order);


$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
foreach ($result as $row){
	$book_category_name[] = htmlspecialchars($row['value']);
}

# Option ..........
$BookCategoryCode = null;
$BookCategoryArray1 = $libms->BOOK_GETOPTION('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', "CONCAT(`BookCategoryCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookCategoryCode', 'BookCategoryType=1');
$BookCategoryOption1 = $linterface->GET_SELECTION_BOX($BookCategoryArray1, " name='BookCategory1' id='BookCategory1' class='selection' style='display:block'", $Lang['libms']['status']['na'], $BookCategoryCode);
$BookCategoryArray2 = $libms->BOOK_GETOPTION('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', "CONCAT(`BookCategoryCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookCategoryCode', 'BookCategoryType=2');
$BookCategoryOption2 = $linterface->GET_SELECTION_BOX($BookCategoryArray2, " name='BookCategory2' id='BookCategory2' class='selection' style='display:none'", $Lang['libms']['status']['na'], $BookCategoryCode);

$BookCirculationArray = $libms->BOOK_GETOPTION('LIBMS_CIRCULATION_TYPE', 'CirculationTypeCode', $Lang['libms']['sql_field']['Description'], 'CirculationTypeCode', '');
$BookCirculationOption = $linterface->GET_SELECTION_BOX($BookCirculationArray, " name='BookCirculation' id='BookCirculation' class='selection' ", $Lang['libms']['status']['na'], $CirculationTypeCode);

$BookResourcesArray = $libms->BOOK_GETOPTION('LIBMS_RESOURCES_TYPE', 'ResourcesTypeCode', $Lang['libms']['sql_field']['Description'], 'ResourcesTypeCode', '');
$BookResourcesOption = $linterface->GET_SELECTION_BOX($BookResourcesArray, " name='BookResources' id='BookResources' class='selection' ", $Lang['libms']['status']['na'], $ResourcesTypeCode);

$BookLanguageArray = $libms->BOOK_GETOPTION('LIBMS_BOOK_LANGUAGE', 'BookLanguageCode', $Lang["libms"]["sql_field"]["Description"], 'BookLanguageCode', '');
$BookLanguageOption = $linterface->GET_SELECTION_BOX($BookLanguageArray, " name='BookLang' id='BookLang' class='selection' ", $Lang['libms']['status']['na']);

$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
$ClassLevelArray = $lelibplus->getRecommendedClassLevels();
$class_table = array_chunk($ClassLevelArray['class_levels'],3) ;

############################################################################################################

# Top menu highlight setting
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();

?>

<style>
<?php
	echo $bmm->get_batch_edit_style();
?>
</style>

<script src="/templates/jquery/jquery-1.8.3.min.js" type="text/javascript" charset="utf-8"></script>
<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--

var timerobj;
function jsShowOrHideField(field)
{
	timerobj = setTimeout(ShowOrHideFieldAction(field),1000);
}

function ShowOrHideFieldAction(field)
{
	$("#"+field).focus();
	clearTimeout(timerobj);
}

function checkDate(obj){
    if(!check_date(obj,"<?=$Lang["libms"]["batch_edit"]["InvalidDateFormat"]?>")) return false;
    return true;
}
 
function isEmptyField(field_type){
	var isFieldValid = true;

	if ( (field_type == 'search_nonEmpty') && ($.trim($("#ACNO_Start").val()) == '') && ($.trim($("#ACNO_End").val()) == '') ) {
		isFieldValid= false;
		alert('<?=$Lang["libms"]["book"]["alert"]["fillin_anco"]?>');
		$("#ACNO_Start").focus();
	}
	else {
		$('.'+field_type).each(function(){
			if( $(this).hasClass('optional')){
				return true;
			}
			var conds =! $.trim($(this).val());
			if( conds ){				
				if (field_type != 'search_nonEmpty') {
					isFieldValid= false;
					var field = $(this).parent().parent().children(':first-child').text()
					alert("<?=$Lang["libms"]["batch_edit"]["PleaseSelect"]?> "+field);					
				}
			}
		});
	}	
	return isFieldValid;
}
 
function isNumberField(){
	var isFieldValid = true;

	$('.numberField').each(function(){
		var str = $.trim($(this).val());
		if ((str != "") && (isNaN(Number(str)) || str < 0)){			
			var field = $(this).parent().parent().children(':first-child').text()
			alert("<?=$Lang["libms"]["book"]["alert"]["numeric"]?> "+field);	
			$(this).focus(); 
			isFieldValid = false;
			return false;	
		}
	});
	return isFieldValid;
}
 
function isDateField(field_type){
	var isFieldValid = true;
	
	$('.'+field_type).each(function(){
		if( $(this).hasClass('optional')){
			if ($.trim($(this).val()) == '') {
				return true;
			}
		}
	    if(!checkDate(document.getElementById(this.id))){
            isFieldValid = false;
	    }
	});
	if (field_type == 'search_date') {
		isFieldValid = isFieldValid & compare_date(document.getElementById("FromDate"), document.getElementById("ToDate"),"<?=$Lang['General']['WrongDateLogic']?>");
	}
	return isFieldValid;
}

function clear_item_list() {
	$('#book_item_table tr').not('tr:first').remove();
	$('#left_list_panel').attr('style','display:none');
	$('#right_change_panel').css('visibility','hidden');
}

function show_left_panel_result(json) {
	if (json != null && json.result){
		$('#left_list_panel_content').html(json.layout);
		$('#left_list_panel').attr('style','display:block');
		if ( json.result == "1" ) {							
			$('#right_change_panel').css('visibility','visible');
		}
		else {
			$('#right_change_panel').css('visibility','hidden');
		}
	}											
}

function ajax_by_barcode_handler(ajaxReturn){
	if (ajaxReturn.success){
		if ($('tr#'+ajaxReturn.BookID).length ==0){
			table = $('#book_item_table');
			if ($('tr#no_record_row').length > 0){
				$('#book_item_table tr:last').remove();
			}
			$row = $(ajaxReturn.html);
			table.append($row);
			$('#left_list_panel').attr('style','display:block');
			$('#right_change_panel').css('visibility','visible');
		}else{
			alert("<?=$Lang["libms"]["batch_edit"]["msg"]["scanned"]?>");
		}
	}else{
		alert('<?=$Lang["libms"]["batch_edit"]["msg"]["no_barcode"]?>');
	}
}
 
function sort_batch_edit_page(order,field) {
	var bookID = '';
	$('input[name="BookID\[\]"]').each(function(){
		if ($(this).val() != '') {
			bookID += $(this).val() + ',';	
		}
	});
	if (bookID != '') {
		bookID = bookID.substr(0,bookID.length-1);
		$.ajax({								
			url : "ajax_get_bibliography_list.php",
			dataType: "json",
			async: false,
			timeout:1000,							
			type : "POST",							
			data : {
				'task': 'sorting',
				'order': order,
				'field': field,
				'bookID': bookID			
			},							
			success : function(json) {
				show_left_panel_result(json);							
			}							
		});								
	}	
}
 
$(document).ready(function(){

	$('input[name="ByACNOOrAcctDate"]').change(function(){
		if ($("#ByBarcode").is(':checked')) {
			$('.barcode_element').attr('style','display:block');
			$('.acno_element, .acct_date_element, .search_criteria, .search_criteria_other, #SearchButton').attr('style','display:none');
		}
		else if ($("#ByACNO").is(':checked')) {
			$('.acno_element').attr('style','display:block');
			$('.barcode_element, .acct_date_element, .search_criteria_other').attr('style','display:none');			
			$('.search_criteria, #SearchButton').attr('style','display:table-row');
		}
		else if ($("#ByAccountDate").is(':checked')) {
			$('.acct_date_element').attr('style','display:block');
			$('.barcode_element, .acno_element, .search_criteria_other').attr('style','display:none');
			$('.search_criteria, #SearchButton').attr('style','display:table-row');
		}
		else if ($("#ByOthers").is(':checked')) {
			$('.search_criteria_other').attr('style','display:block');
			$('.barcode_element, .acno_element, .acct_date_element, .search_criteria').attr('style','display:none');
			$('.search_criteria_other, #SearchButton').attr('style','display:table-row');
		}
	});

	$('#Barcode').keyup(function(event) {
	  if (event.which == 13) {
		$('#add_book').click();
	  }  
	});

	$('#reset').click(function(){
		if (($('#book_item_table tr').length > 1 ) && ($('tr#no_record_row').length <= 0) ) {
			var confirm = window.confirm("<?=$Lang["libms"]["batch_edit"]["ConfirmClearList"]?>");
		}
		else {
			confirm = true;
		}		
		if (confirm) {
			clear_item_list();
		}
	});
	
	$('#Barcode').keypress(function(e){
	    if ( e.which == 13 ) {
			e.preventDefault();
		    return false;
	    }
	});

	$('input[name="BookCategoryType"]').change(function(){
		if($('#BookCategoryTypeAll').is(':checked'))
		{
			$('#BookCategoryCode1').attr('style','display:none');
			$('#BookCategoryCode2').attr('style','display:none');
		}
		else if ($('#BookCategoryType1').is(':checked'))
		{				
			$('#BookCategoryCode1').attr('style','display:block');
			$('#BookCategoryCode2').attr('style','display:none');
		}
		else if ($('#BookCategoryType2').is(':checked'))
		{
			$('#BookCategoryCode1').attr('style','display:none');
			$('#BookCategoryCode2').attr('style','display:block');
		}
	});
	
	$('#add_book').click( function(){
		$('#Barcode').val($('#Barcode').val().toUpperCase().replace(" ",''));
		
		$.getJSON(
				'ajax_get_bibliography_by_barcode_for_recommend.php',
				{ 
					barcode : $('#Barcode').val(),
					ID : (($('tr#no_record_row').length > 0) ? 1: $('#book_item_table tr').length)
				},
				'JSON'
		)
		.success(ajax_by_barcode_handler)
		.error(function(){ alert('Error on AJAX call!'); });

		$('#Barcode').val('');		
		$('#Barcode').focus();
		return false;
	});
	
	$('#search').click(function(){
		var confirm;	
		if (($('#book_item_table tr').length > 1 ) && ($('tr#no_record_row').length <= 0) ) {
			confirm = window.confirm("<?=$Lang["libms"]["batch_edit"]["ConfirmClearListForSearch"]?>");
		}
		else {
			confirm = true;
		}
		if (confirm) {
			clear_item_list();

			var ret = true;
			if ($("#ByOthers").is(':checked')) {
				if (($.trim($('#BookTitle').val()) == '') && ($.trim($('#Author').val()) == '') && ($.trim($('#CallNumber').val()) == '') && ($.trim($('#Tag').val()) == '')) {
					alert('<?=$Lang["libms"]["batch_edit"]["InputKeywordToSearch"]?>');
					$("#BookTitle").focus();
				}
				else {
					$.ajax({								
						url : "ajax_get_book_list_for_recommend.php",
						dataType: "json",
						async: false,
						timeout:1000,							
						type : "POST",							
						data : $('#form1').serialize(),							
						success : function(json) {											
							show_left_panel_result(json);
						}							
					});								
				}
			}
			else {
				if ($("#ByACNO").is(':checked')) {
					ret = ret & isEmptyField('search_nonEmpty');
				}
				else if ($("#ByAccountDate").is(':checked')) {
					ret = ret & isDateField('search_date');
				}
				if (ret) {
					$.ajax({								
						url : "ajax_get_bibliography_list_for_recommend.php",
						dataType: "json",
						async: false,
						timeout:1000,							
						type : "POST",							
						data : $('#form1').serialize(),							
						success : function(json) {											
							show_left_panel_result(json);
						}							
					});								
				}
			}
		}		
	});
	
	
    $('#check_all').click(function(){
            if ($(this).is(':checked')){
                $('#right_change_panel input:checkbox').attr('checked', 'checked');
            }else{
                $('#right_change_panel input:checkbox').removeAttr('checked');
            }
    });
	
	$('#submit_recommend').click(function(){
		if($('input[name="BookID\[\]"]:checked').length == 0) {
			alert("<?=$Lang["libms"]["batch_edit"]["SelectAtLeastOneItem"]?>");	
		}
		else if($('input[name="class_level_ids\[\]"]:checked').length == 0) {
			alert("<?=$Lang["libms"]["batch_edit"]["SelectAtLeastOneClassLevel"]?>");	
		}
		else {
			$('#form1').submit();
		}
		
	});
	
	
	if($("#Tag").length > 0){
		$("#Tag").autocomplete(
	      "ajax_get_suggestions.php",
	      {
	  			delay:3,
	  			minChars:1,
	  			matchContains:1,
	  			extraParams: {'field':'Tag'},
	  			onItemSelect: function() { jsShowOrHideField('Tag'); },
	  			formatItem: function(row){ return row[0]; },
	  			autoFill:false,
	  			overflow_y: 'auto',
	  			overflow_x: 'hidden',
	  			maxHeight: '200px',
	  			width:'306px'
	  		}
	    );
	}

	if($("#BookTitle").length > 0){
		$("#BookTitle").autocomplete(
	      "ajax_get_book_suggestion.php",
	      {
	  			delay:3,
	  			minChars:1,
	  			matchContains:1,
	  			onItemSelect: function() { jsShowOrHideField('BookTitle'); },
	  			formatItem: function(row){ return row[0]; },
	  			autoFill:false,
	  			overflow_y: 'auto',
	  			overflow_x: 'hidden',
	  			maxHeight: '200px',
	  			width:'306px'
	  		}
	    );
	}

	if($("#Author").length > 0){
		$("#Author").autocomplete(
	      "ajax_get_suggestions.php",
	      {
	  			delay:3,
	  			minChars:1,
	  			matchContains:1,
	  			extraParams: {'field':'Author'},
	  			onItemSelect: function() { jsShowOrHideField('Author'); },
	  			formatItem: function(row){ return row[0]; },
	  			autoFill:false,
	  			overflow_y: 'auto',
	  			overflow_x: 'hidden',
	  			maxHeight: '200px',
	  			width:'306px'
	  		}
	    );
	}

});

//-->
</script>

<form name="form1" id="form1" method="post" action="batch_recommend_book_new_update.php">

	<!-- split into left and right, then break left part into top and bottom, like this: -| -->
	<table class="be_table">
		<tr>
			<td width="49%" valign="top">
				<table class="be_table">
					<tr>
						<td>
							<!-- ###### Top Left Panel ##### -->	
							<span id="left_search_panel" class="table_board">
					            <span class="form_sub_title_v30">
					            	<em>- <span class="field_title"><?= $Lang["libms"]["batch_edit"]["PanelTitle"]["BookSearch"] ?></span> -</em>
					                <p class="spacer"></p>
					            </span>
					            
					            <span>
<?php
	echo $bmm->get_batch_recommend_search_criteria();
?>		
								</span>
							</span>
						</td>
					</tr>
						
					<tr>
						<td>		
							<!-- ###### Bottom Left Panel ##### -->
							<span id="left_list_panel" class="table_board" style="display:none">
								<div class="form_sub_title_v30">
							    	<em>- <span class="field_title"><?=$Lang["libms"]["batch_edit"]["PanelTitle"]["SelectedRecord"]?></span> -</em>
							       	<p class="spacer"></p>
						    	</div>
								<div id="left_list_panel_content">
									<table id="book_item_table" class="common_table_list_v30 view_table_list_v30" style="width:98%"> 
										<tr>
											<th width="1">#</th>
<?php											
$pos = 0;
?>
											<th width="75%"><?=$bmm->column($pos++, $Lang["libms"]["book"]["title"])?></th>
											<th width="20%"><?=$bmm->column($pos++, $Lang["libms"]["book"]["call_number"])?></th>				
											<th class="num_check" width="1"><input type="checkbox" id="checkall" name="checkall" onclick="Set_Checkbox_Value('BookID[]',this.checked)" checked/></th>
										</tr>
									</table>
								</div>
								<br>
								<div><?=$Lang["libms"]["batch_edit"]["RecommendRemark"]?></div>
							</span>
						</td>
					</tr>
				</table>		
			</td>

			<td width="49%" valign="top">
				<table class="be_table">
					<tr>
						<td>
			
							<!-- ###### Right Panel ##### -->
							<span id="right_change_panel" class="table_board">
					            <span class="form_sub_title_v30">
					            	<em>- <span class="field_title"><?= $eLib_plus["html"]["recommendbookto"] ?></span> -</em>
					                <p class="spacer"></p>
					            </span>
					            
					            <span>
									<table class="form_table_v30">
										<tbody>
			    							<tr>
			    								<td colspan="3">
			    									<input type="checkbox" id="check_all"><?=$eLib["html"]["all_class"]  ?>
			    								</td>
			    							</tr>
									<? foreach ($class_table as $class_level_rows): ?>
									    	<tr>
									    	<? foreach ($class_level_rows as $class_level): ?>
												<td>
													<input type="checkbox" <?=$class_level['class_level_recommended']? "checked='checked'":""?> name="class_level_ids[]" value="<?=$class_level['class_level_id']?>"><?=$class_level['class_level_name']?>
												</td>
									    	<? endforeach ?>
									    	</tr>
									<? endforeach ?>
			    							<tr>
			    								<td colspan="3">
			    									<textarea name='content' rows="5" class="textboxtext"><?=$detail_data['recommend']['content']?></textarea>
			    								</td>
			    							</tr>
			    							
										</tbody>
									</table>
									
									<div class="edit_bottom_v30">
										<?= $linterface->GET_ACTION_BTN($button_submit, "button", $ParOnClick="",$ParName="submit_recommend")?>&nbsp;
										<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='batch_recommend_book_list.php'")?>
									</div>	
								</span>
							</span>
						</td>
					</tr>
				</table>
				
			</td>
		<tr>		
	</table>		

</form>

<?php


$linterface->LAYOUT_STOP();

 
intranet_closedb();


?>
