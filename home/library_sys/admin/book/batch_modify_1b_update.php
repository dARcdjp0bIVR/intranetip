<?php
// using: 

/**
 * ***********************************************************
 * 20180319 (Cameron)
 * change $Lang["libms"]["CirculationManagement"]["mail"]['due_date_change_reminder']['subject']
 *
 * 20150805 (Henry)
 * fix: wrong email content [Case#E81690]
 * ***********************************************************
 */
$PATH_WRT_ROOT = "../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/pdump/pdump.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/management/circulation/TimeManager.php");
// global $intranet_db;

intranet_auth();
intranet_opendb();

$libms = new liblms();
$timeManager = new TimeManager($libms);
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (! $admin_right['book management'] && ! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageMgmtBatchEdit";

$libms->MODULE_AUTHENTICATION($CurrentPage);
$TAGS_OBJ[] = array(
    $Lang['libms']['batch_edit']['edit_return_date']
);
// $TAGS_OBJ[] = array($Lang['libms']['batch_edit']['edit_return_date'] , "batch_modify_1.php", 1);
// $TAGS_OBJ[] = array($Lang['libms']['batch_edit']['edit_penal_sum'], "batch_modify_2.php", 0);
// $TAGS_OBJ[] = array($Lang['libms']['batch_edit']['edit_book_info'], "batch_modify_3.php", 0);

// $linterface = new interface_html("libms.html");
$linterface = new interface_html();

$dueDate = '';
if ($periodTo == 2) {
    $dueDate = "'" . $ChangeDate . "'";
}

$totalResult = true;
foreach ($BorrowLogID as $aBorrowLogID) {
    if ($periodTo == 1) {
        $tempDueDate = $libms->SELECTFROMTABLE('LIBMS_BORROW_LOG', 'DueDate', array(
            'BorrowLogID' => $aBorrowLogID
        ), 1);
        $tempDueDate = $tempDueDate[0][0];
        $tempDueDate = strtotime($tempDueDate . " +" . $ChangeDay . " day"); // +1 day
        
        if (date('Y-m-d', $tempDueDate) <= date('Y-m-d')) {
            $tempDueDate = strtotime(date('Y-m-d', strtotime($date1 . "+1 days")));
        }
        
        $dueDate = PHPToSQL($timeManager->findOpenDate($tempDueDate), 'DATE');
    } else if ($periodTo == 3) {
        $tempDueDate = $libms->SELECTFROMTABLE('LIBMS_BORROW_LOG', 'DueDate', array(
            'BorrowLogID' => $aBorrowLogID
        ), 1);
        $tempDueDate = $tempDueDate[0][0];
        $tempDueDate = strtotime($tempDueDate . " -" . $ChangeDay . " day"); // -1 day
        
        if (date('Y-m-d', $tempDueDate) <= date('Y-m-d')) {
            $tempDueDate = strtotime(date('Y-m-d', strtotime($date1 . "+1 days")));
        }
        // debug_pr(date('Y-m-d',$tempDueDate));
        $dueDate = PHPToSQL($timeManager->findOpenDate($tempDueDate), 'DATE');
    }
    
    $sql = "UPDATE LIBMS_BORROW_LOG Set DueDate_BAK = DueDate, DueDate = {$dueDate},
			DateModified = now(),	
			DueDateModifiedBy = '" . $UserID . "' 
			Where BorrowLogID= '{$aBorrowLogID}' ";
    
    // debug_pr($sql);
    $dbResult = $libms->db_db_query($sql);
    if (! $dbResult) {
        $totalResult = false;
    }
    
    if ($noticeBy == 2) {
        
        // Send email to user
        $BookID = $libms->SELECTFROMTABLE('LIBMS_BORROW_LOG', 'BookID', array(
            'BorrowLogID' => $aBorrowLogID
        ), 1);
        $BookID = $BookID[0][0];
        $BookTitle = $libms->SELECTFROMTABLE('LIBMS_BOOK', 'BookTitle', array(
            'BookID' => $BookID
        ), 1);
        $BookTitle = $BookTitle[0][0];
        $tempUserID = $libms->SELECTFROMTABLE('LIBMS_BORROW_LOG', 'UserID', array(
            'BorrowLogID' => $aBorrowLogID
        ), 1);
        $tempUserID = $tempUserID[0][0];
        $tempDueDate = $libms->SELECTFROMTABLE('LIBMS_BORROW_LOG', 'DueDate', array(
            'BorrowLogID' => $aBorrowLogID
        ), 1);
        $tempDueDate = $tempDueDate[0][0];
        
        $mail_to = array(
            $tempUserID
        );
        $mailSubject = $Lang["libms"]["CirculationManagement"]["mail"]['due_date_change_reminder']['subject'];
        $mail_body = sprintf($Lang["libms"]["CirculationManagement"]["mail"]['due_date_reminder']['mail_body'], $BookTitle, $tempDueDate);
        do_email($mail_to, $mailSubject, $mail_body);
    }
}
$xmsg = ($totalResult) ? 'UpdateSuccess' : 'UpdateUnsuccess';
// debug_pr($xmsg);
// header("Location: batch_modify_1.php?xmsg=".$xmsg);
// debug_pr($BorrowLogID);

$x = '<table class="common_table_list_v30  view_table_list_v30" width="100%">
		<tbody>
			<tr>
				<th width="1">#</th>
				<th width="5%">' . $Lang["libms"]["report"]["ClassName"] . '</th>
				<th width="5%">' . $Lang["libms"]["report"]["ClassNumber"] . '</th>
				<!--<th>中文全名</th>-->
				<th width=\"15%\">' . $Lang["libms"]["report"]["EnglishName"] . '</th>
				<th width=\"10%\">' . $Lang["libms"]["CirculationManagement"]["user_barcode"] . '</th>
				<th width=\"10%\">ACNO</th>
				<th width="25%">' . $Lang["libms"]["report"]["booktitle"] . '</th>
				<th width="15%">' . $Lang["libms"]["report"]["borrowdate"] . '</th>
				<th width="10%">' . $Lang["libms"]["batch_edit"]["Bak_DueDate"] . '</th>
				<th width="10%">' . $Lang["libms"]["batch_edit"]["DueDate"] . '</th>
			</tr>';

$BorrowLogIDList = implode(',', $BorrowLogID);
$sql = "select IF(a.ClassName IS NULL OR a.ClassName = '','" . $Lang['General']['EmptySymbol'] . "',a.ClassName) AS ClassName, 
			IF(a.ClassNumber IS NULL OR a.ClassNumber = '','" . $Lang['General']['EmptySymbol'] . "', a.ClassNumber) AS ClassNumber,
		 IF(a.ChineseName IS NULL OR a.ChineseName = '','" . $Lang['General']['EmptySymbol'] . "', a.ChineseName) AS ChineseName,
a.EnglishName, a.BarCode, d.ACNO, c.BookTitle, b.BorrowTime, b.DueDate, b.DueDate_BAK, b.BorrowLogID from LIBMS_USER as a 
JOIN LIBMS_BORROW_LOG as b
ON a.UserID = b.UserID
JOIN LIBMS_BOOK as c
ON b.BookID = c.BookID
JOIN LIBMS_BOOK_UNIQUE as d
ON b.UniqueID = d.UniqueID
WHERE b.BorrowLogID IN ({$BorrowLogIDList})";

$result = $libms->returnArray($sql);
for ($i = 0; $i < sizeof($result); $i ++) {
    $x .= '<tr>
				<td>' . ($i + 1) . '</td>
				<td>' . $result[$i]['ClassName'] . '</td>
				<td>' . $result[$i]['ClassNumber'] . '</td>
				<!--<td>' . $result[$i]['ChineseName'] . '</td>-->
				<td>' . $result[$i]['EnglishName'] . '</td>
				<td>' . $result[$i]['BarCode'] . '</td>
				<td>' . $result[$i]['ACNO'] . '</td>
				<td>' . $result[$i]['BookTitle'] . '</td>
				<td>' . $result[$i]['BorrowTime'] . '</td>
				<td>' . $result[$i]['DueDate_BAK'] . '</td>
				<td>' . $result[$i]['DueDate'] . '</td>
			</tr>';
}

$x .= '</tbody></table>';
// debug_pr($BorrowLogID);
$display = $x;
// ###########################################################################################################

// Top menu highlight setting

// $TAGS_OBJ[] = array($Lang["libms"]["stats"]["frequency"]);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();

?>
<table class="form_table">
	<tr>
		<td colspan="2"><?=sprintf($Lang['libms']['batch_edit']['record_updated'], sizeof($result))?></td>
	</tr>
</table>
<?=$display?>
<br />
<div class="edit_bottom">
	<p class="spacer"></p>
                       <?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='batch_modify_1.php'") ?>
						<p class="spacer"></p>
</div>

<?php
$linterface->LAYOUT_STOP();

intranet_closedb();

?>
