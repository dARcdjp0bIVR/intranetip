<?php
#using:  

/***************************************
 * 
 * Date:	2016-02-11 [Cameron]
 * 			- move sql to liblibrarymgmt.php > returnSQLforLostReport()
 *  		- add column: ListPrice and Penalty
 * Date:	2015-09-07 [Henry]
 * 			access right bug fix
 * Date: 	2014-05-14 (Henry)
 * Details: Added school name at top of the page
 * Date: 	2014-04-04 (Henry)
 * Details: Create this page
 ***************************************/

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$admin_right = $_SESSION['LIBMS']['admin']['current_right']['reports'];

if (!$admin_right['lost report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libms = new liblms();
$linterface = new interface_html();

$sql = $libms->returnSQLforLostReport();
$field = array("DateModified", "UserName","ACNO","CallNum", "BookTitle", "ResponsibilityBy1","PurchasePrice","ListPrice","Penalty");
$orderCond = ' order by '.$field[0].' desc';
if(isset($ck_right_page_order)){
	$order = ($ck_right_page_order == 0 ? 'desc':'');
	$orderCond = ' order by '.$field[$ck_right_page_field].' '.$order;
}
$sql .= $orderCond;

$results = $libms->returnResultSet($sql);
// copy [end] -----------------------------------------------------------------------------------------------

$reportPeriodDisplay = $_REQUEST['DueDateAfter']  . ' ' . $Lang['General']['To']  . ' ' .  $_REQUEST['DueDateBefore'];

$schoolName =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];

$displayTable = '';
$displayTable .= '<table width="98%" border="0" cellspacing="0" cellpadding="5" valign="top">';
$displayTable .= '<tr>';
$displayTable .= '<td align="right" class="print_hide">';
$displayTable .= $linterface->GET_BTN($button_print, "button","javascript:window.print()");
$displayTable .= '</td>'; 
$displayTable .= '</tr>';
$displayTable .= '<tr><td>'.$schoolName.'</td></tr>'; 
$displayTable .= '<tr>';
$displayTable .= '<td>';
$displayTable .= '<center><h2>' . $Lang['libms']['reporting']['PageReportingLostBookReport'] . '</h2></center>';
$displayTable .= '</td>';
$displayTable .= '</tr>';
$displayTable .= '<tr>';
$displayTable .= '<td align="right">';
$displayTable .= '<table>';
$displayTable .= '<tr><td>'.$Lang['General']['Date'] . ': </td><td>'.$reportPeriodDisplay.'</td></tr>';
$displayTable .= '</table>';	
$displayTable .= '</td>'; 
$displayTable .= '</tr>';
$displayTable .= '<tr>';
$displayTable .= '<td align="center">';

$displayTable .= '<table class="common_table_list_v30  view_table_list_v30" width="100%" align="center"  border="0" cellSpacing="0" cellPadding="4">';
		$displayTable .= '<thead>';
		$displayTable .= '<tr>';
			$displayTable .= '<th width="1%" class="tabletop tabletopnolink">';
			$displayTable .= '#';
			$displayTable .= '</th>';
			$displayTable .= '<th width="10%" class="tabletop tabletopnolink">';
			$displayTable .= $Lang["libms"]["book"]["lost_date"];
			$displayTable .= '</th>';
			$displayTable .= '<th width="10%" class="tabletop tabletopnolink">';
			$displayTable .= $Lang['libms']['book']['user_name'];
			$displayTable .= '</th>';
			$displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
			$displayTable .= $Lang["libms"]["label"]["BookCode"];
			$displayTable .= '</th>';
			$displayTable .= '<th width="10%" class="tabletop tabletopnolink">';
			$displayTable .= $Lang['libms']['book']['call_number'];
			$displayTable .= '</th>';
			$displayTable .= '<th width="19%" class="tabletop tabletopnolink">';
			$displayTable .= $Lang['libms']['book']['title'];
			$displayTable .= '</th>';
			$displayTable .= '<th width="10%" class="tabletop tabletopnolink">';
			$displayTable .= $Lang['libms']['book']['responsibility_by1'];
			$displayTable .= '</th>';
			$displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
			$displayTable .= $Lang["libms"]["book"]["purchase_price"];
			$displayTable .= '</th>';
			$displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
			$displayTable .= $Lang["libms"]["book"]["list_price"];
			$displayTable .= '</th>';
			$displayTable .= '<th width="5%" class="tabletop tabletopnolink">';
			$displayTable .= $Lang["libms"]["report"]["pay"];
			$displayTable .= '</th>';			
		$displayTable .= '</tr>';
		$displayTable .= '</thead>';
		$displayTable .= '<tbody>';
		
		for ($i=0; $i<sizeof($results); $i++)
		{
			$thisBook = $results[$i];
			
			$displayTable .= '<tr>';	
					$displayTable .= '<td '.$rowStyle.'>';
					$displayTable .= ($i + 1);
					$displayTable .= '</td>';	
					$displayTable .= '<td '.$rowStyle.'>';
					$displayTable .= $thisBook["DateModified"];
					$displayTable .= '</td>';
					$displayTable .= '<td '.$rowStyle.'>';
					$displayTable .= $thisBook["UserName"];
					$displayTable .= '</td>';
					$displayTable .= '<td '.$rowStyle.'>';
					$displayTable .= $thisBook["ACNO"];
					$displayTable .= '</td>';
					$displayTable .= '<td '.$rowStyle.'>';
					$displayTable .= $thisBook["CallNum"];
					$displayTable .= '</td>';
					$displayTable .= '<td '.$rowStyle.'>';
					$displayTable .= $thisBook["BookTitle"];
					$displayTable .= '</td>';
					$displayTable .= '<td '.$rowStyle.'>';
					$displayTable .= $thisBook["ResponsibilityBy1"];
					$displayTable .= '</td>';
					$displayTable .= '<td '.$rowStyle.'>';
					$displayTable .= $thisBook["PurchasePrice"];
					$displayTable .= '</td>';
					$displayTable .= '<td '.$rowStyle.'>';
					$displayTable .= $thisBook["ListPrice"];
					$displayTable .= '</td>';
					$displayTable .= '<td '.$rowStyle.'>';
					$displayTable .= $thisBook["Penalty"];
					$displayTable .= '</td>';
				$displayTable .= '</tr>';
		}
		if (sizeof($results)==0)
		{
			$displayTable .= '<tr>';	
				$displayTable .= '<td colspan="10" align="center">';
				$displayTable .= $Lang['libms']['NoRecordAtThisMoment'];
				$displayTable .= '</td>';
			$displayTable .= '</tr>';
		}
		$displayTable .= '</tbody>';
		$displayTable .= '</table>';

$displayTable .= '</td>';
$displayTable .= '</tr>';

$displayTable .= '</tbody>';
$displayTable .= '</table>';


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  /> 
<META Http-Equiv="Cache-Control" Content="no-cache">

<style type='text/css'>

html, body { margin: 0px; padding: 0px; background-color: #ffffff;} 


@media print and (width: 21cm) and (height: 29.7cm) {
    @page {
       margin: 3cm;
    }
 }
 
@media print
{
	
  table { page-break-after:auto }
  tr    { page-break-inside:avoid; page-break-after:auto }
  td    { page-break-inside:avoid; page-break-after:auto }
  thead { display:table-header-group }
  tfoot { display:table-footer-group }
  
  #footer {
         display:block;
		   position:fixed;
		   bottom:0px;
		   width:100%;
  } 
    
    
}

</style>
</head>
<body>
<?php echo $displayTable;?>
</body>
</html>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>