<?php

//modifying:
/*
 * 	@Purpose:	Batch edit of specific fields in Bibliography
 *  
 * 	Log
* 	Date:	2017-01-27 [Cameron] add checking empty for ByACNOList (case #Z110645)  
 * 	Date:	2016-07-05 [Cameron]
 * 				- change BookLanguage to use selection box rather than inputselect and show language description instead of code [case #Z97773]
 * 	Date:	2015-07-08 [Cameron] 
 * 				- support column sorting when it's clicked
 * 				- show book category types in filter and apply panel
 * 	Date:	2015-07-07 [Cameron] Add radio option "By BarCode"
 * 	Date:	2014-12-19 [Cameron] JSON.parse support IE8 or above, therefore, use jQuery.parseJSON instead
 * 	Date:	2014-12-05 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."home/library_sys/admin/book/batch_modify_manage.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageMgmtBatchEdit";

$libms->MODULE_AUTHENTICATION($CurrentPage);

$TAGS_OBJ = $libms->get_batch_edit_tabs("BIBLIOGRAPHY");

$linterface = new interface_html();
$bmm = new batch_modify_manage();
	
$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;
$bmm->setfield($field);
$bmm->setorder($order);

//# date range
//$FromDate=$FromDate==""?date('Y-m-d'):$FromDate;
//$ToDate=$ToDate==""?date('Y-m-d'):$ToDate;

$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
foreach ($result as $row){
	$book_category_name[] = htmlspecialchars($row['value']);
}

# Option ..........
$BookCategoryCode = null;
$BookCategoryArray1 = $libms->BOOK_GETOPTION('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', "CONCAT(`BookCategoryCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookCategoryCode', 'BookCategoryType=1');
$BookCategoryOption1 = $linterface->GET_SELECTION_BOX($BookCategoryArray1, " name='BookCategory1' id='BookCategory1' class='selection' style='display:block'", $Lang['libms']['status']['na'], $BookCategoryCode);
$BookCategoryArray2 = $libms->BOOK_GETOPTION('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', "CONCAT(`BookCategoryCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookCategoryCode', 'BookCategoryType=2');
$BookCategoryOption2 = $linterface->GET_SELECTION_BOX($BookCategoryArray2, " name='BookCategory2' id='BookCategory2' class='selection' style='display:none'", $Lang['libms']['status']['na'], $BookCategoryCode);

$BookCirculationArray = $libms->BOOK_GETOPTION('LIBMS_CIRCULATION_TYPE', 'CirculationTypeCode', $Lang['libms']['sql_field']['Description'], 'CirculationTypeCode', '');
$BookCirculationOption = $linterface->GET_SELECTION_BOX($BookCirculationArray, " name='BookCirculation' id='BookCirculation' class='selection' ", $Lang['libms']['status']['na'], $CirculationTypeCode);

$BookResourcesArray = $libms->BOOK_GETOPTION('LIBMS_RESOURCES_TYPE', 'ResourcesTypeCode', $Lang['libms']['sql_field']['Description'], 'ResourcesTypeCode', '');
$BookResourcesOption = $linterface->GET_SELECTION_BOX($BookResourcesArray, " name='BookResources' id='BookResources' class='selection' ", $Lang['libms']['status']['na'], $ResourcesTypeCode);

$BookLanguageArray = $libms->BOOK_GETOPTION('LIBMS_BOOK_LANGUAGE', 'BookLanguageCode', $Lang["libms"]["sql_field"]["Description"], 'BookLanguageCode', '');
$BookLanguageOption = $linterface->GET_SELECTION_BOX($BookLanguageArray, " name='BookLang' id='BookLang' class='selection' ", $Lang['libms']['status']['na']);


############################################################################################################

# Top menu highlight setting
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();

?>

<style>
<?php
	echo $bmm->get_batch_edit_style();
?>
</style>

<?php
	echo $bmm->get_batch_edit_js();
?>

<script language="javascript">
<!--
 
function ajax_by_barcode_handler(ajaxReturn){
	if (ajaxReturn.success){
		if ($('tr#'+ajaxReturn.BookID).length ==0){
			table = $('#book_item_table');
			if ($('tr#no_record_row').length > 0){
				$('#book_item_table tr:last').remove();
			}
			$row = $(ajaxReturn.html);
			table.append($row);
			$('#left_list_panel').attr('style','display:block');
			$('#right_change_panel').css('visibility','visible');
		}else{
			alert("<?=$Lang["libms"]["batch_edit"]["msg"]["scanned"]?>");
		}
	}else{
		alert('<?=$Lang["libms"]["batch_edit"]["msg"]["no_barcode"]?>');
	}
}
 
function sort_batch_edit_page(order,field) {
	var bookID = '';
	$('input[name="BookID\[\]"]').each(function(){
		if ($(this).val() != '') {
			bookID += $(this).val() + ',';	
		}
	});
	if (bookID != '') {
		bookID = bookID.substr(0,bookID.length-1);
		$.ajax({								
			url : "ajax_get_bibliography_list.php",
			async: false,
			timeout:1000,							
			type : "POST",							
			data : {
				'task': 'sorting',
				'order': order,
				'field': field,
				'bookID': bookID			
			},							
			success : function(json) {
				show_left_panel_result(json);							
			}							
		});								
	}	
}
 
$(document).ready(function(){

<?php
	echo $bmm->get_batch_edit_doc_ready_js();
?>
	$('input[name="ApplyBookCategoryType"]').change(function(){
		if ($('#ApplyBookCategoryType1').is(':checked'))
		{				
			$('#BookCategory1').attr('style','display:block');
			$('#BookCategory2').attr('style','display:none');
		}
		else if ($('#ApplyBookCategoryType2').is(':checked'))
		{
			$('#BookCategory1').attr('style','display:none');
			$('#BookCategory2').attr('style','display:block');
		}
	});
	
	$('#add_book').click( function(){
		$('#Barcode').val($('#Barcode').val().toUpperCase().replace(" ",''));
		
		$.getJSON(
				'ajax_get_bibliography_by_barcode.php',
				{ 
					barcode : $('#Barcode').val(),
					ID : (($('tr#no_record_row').length > 0) ? 1: $('#book_item_table tr').length)
				},
				'JSON'
		)
		.success(ajax_by_barcode_handler)
		.error(function(){ alert('Error on AJAX call!'); });

		$('#Barcode').val('');		
		$('#Barcode').focus();
		return false;
	});
	
	$('#search').click(function(){	
//		$('.errorMsg').remove();
		if (($('#book_item_table tr').length > 1 ) && ($('tr#no_record_row').length <= 0) ) {
			var confirm = window.confirm("<?=$Lang["libms"]["batch_edit"]["ConfirmClearListForSearch"]?>");
		}
		else {
			confirm = true;
		}
		if (confirm) {
			clear_item_list();

			var ret = true;
			var rs=new Array();
			if ($("#ByACNO").is(':checked')) {
				ret = ret & isEmptyField('search_nonEmpty');
			}
			else if ($("#ByACNOList").is(':checked')) {
				ret = ret & isEmptyField('search_nonEmpty');
			}
			else if ($("#ByAccountDate").is(':checked')) {
				ret = ret & isDateField('search_date');
			}
			if (ret) {
				$.ajax({								
					url : "ajax_get_bibliography_list.php",
					async: false,
					timeout:1000,							
					type : "POST",							
					data : $('#form1').serialize(),							
					success : function(json) {											
//						rs = JSON.parse(json);
						show_left_panel_result(json);
					}							
				});								
			}
		}		
	});
	
	$("input:button[name^='Apply_']").each(function(){
		$(this).click(function(){
			if($('input[name="BookID\[\]"]:checked').length == 0) {
				alert("<?=$Lang["libms"]["batch_edit"]["SelectAtLeastOneItem"]?>");	
			}
			else {
				var confirm = window.confirm("<?=$Lang["libms"]["batch_edit"]["ConfirmUpdate"]?>" + $(this).parent().parent().parent().children(':first-child').text());
				if (confirm) {
					var str = this.id.split('Apply_');
					var field = str[1]; 
					$('#UpdateField').val(field);
					$.ajax({								
						url : "ajax_batch_update_bibliography.php",
						async: false,
						timeout:1000,							
						type : "POST",							
						data: $('#form1').serialize(),		
						success : function(json) {							
							rs = jQuery.parseJSON(json);							
							if ( rs['result'] != undefined ) {
								if ( rs['result'] == "0" ) {
									Get_Return_Message("<?=$Lang["libms"]["batch_edit"]["ReturnMessage"]["UpdateUnsuccess"]?>");
								}
								else {
									Get_Return_Message("1|=|" + rs['nrAffected'] + "<?=$Lang["libms"]["batch_edit"]["ReturnMessage"]["UpdateSuccess"]?>");
								}
							}											
						}							
					});								
				}
				else {
					// do nothing			
				}
			}
		});
	});
	
});

//-->
</script>

<form name="form1" id="form1" method="post" action="batch_modify_bibliography.php">

	<!-- split into left and right, then break left part into top and bottom, like this: -| -->
	<table class="be_table">
		<tr>
			<td width="49%" valign="top">
				<table class="be_table">
					<tr>
						<td>
							<!-- ###### Top Left Panel ##### -->	
							<span id="left_search_panel" class="table_board">
					            <span class="form_sub_title_v30">
					            	<em>- <span class="field_title"><?= $Lang["libms"]["batch_edit"]["PanelTitle"]["BookSearch"] ?></span> -</em>
					                <p class="spacer"></p>
					            </span>
					            
					            <span>
<?php
	echo $bmm->get_batch_modify_search_criteria();
?>		
								</span>
							</span>
						</td>
					</tr>
						
					<tr>
						<td>		
							<!-- ###### Bottom Left Panel ##### -->
							<span id="left_list_panel" class="table_board" style="display:none">
								<div class="form_sub_title_v30">
							    	<em>- <span class="field_title"><?=$Lang["libms"]["batch_edit"]["PanelTitle"]["SelectedRecord"]?></span> -</em>
							       	<p class="spacer"></p>
						    	</div>
								<div id="left_list_panel_content">
									<table id="book_item_table" class="common_table_list_v30 view_table_list_v30" style="width:98%"> 
										<tr>
											<th width="1">#</th>
<?php											
$pos = 0;
?>
											<th width="75%"><?=$bmm->column($pos++, $Lang["libms"]["book"]["title"])?></th>
											<th width="20%"><?=$bmm->column($pos++, $Lang["libms"]["book"]["call_number"])?></th>				
											<th class="num_check" width="1"><input type="checkbox" id="checkall" name="checkall" onclick="Set_Checkbox_Value('BookID[]',this.checked)" checked/></th>
										</tr>
									</table>
								</div>
							</span>
						</td>
					</tr>
				</table>		
			</td>

			<td width="49%" valign="top">
				<table class="be_table">
					<tr>
						<td>
			
							<!-- ###### Right Panel ##### -->
							<span id="right_change_panel" class="table_board">
					            <span class="form_sub_title_v30">
					            	<em>- <span class="field_title"><?= $Lang["libms"]["batch_edit"]["PanelTitle"]["ChangeRecord"] ?></span> -</em>
					                <p class="spacer"></p>
					            </span>
					            
					            <span>
									<table class="form_table_v30">
										<tbody>
						            		<tr>
						                    	<td class="field_title_short nowrap"><span class="field_title"><?= $Lang['libms']['book']['language'] ?></span></td>
						                    	<td><?=$BookLanguageOption?></td>
						                    	<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_BookLang") ?></div></td>
						            		</tr>
						            		
						            		<tr>            		
						                    	<td class="field_title_short nowrap"><span class="field_title"><?= $Lang['libms']['book']['country'] ?></span></td>
						                     	<td><input id="BookCountry" name="BookCountry" type="text" class="textboxtext inputselect" maxlength="128" value="<?=$Country?>"/></td>
						                     	<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_BookCountry") ?></div></td>
						            		</tr>
						            		
						            		<tr>
						                    	<td class="field_title_short nowrap"><span class="field_title"><?= $Lang['libms']['settings']['circulation_type'] ?></span></td>
						                     	<td><?=$BookCirculationOption?></td>
												<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_BookCirculation") ?></div></td>
						            		</tr>
						
						            		<tr>
						                    	<td class="field_title_short nowrap"><span class="field_title"><?= $Lang['libms']['settings']['book_category'] ?></span></td>
						                    	<td>
													<?=$linterface->Get_Radio_Button("ApplyBookCategoryType1", "ApplyBookCategoryType", 1, true, "", $book_category_name[0]?$book_category_name[0]:$Lang['libms']['report']['book_category'].' 1')?>
													<?=$linterface->Get_Radio_Button("ApplyBookCategoryType2", "ApplyBookCategoryType", 2, false, "", $book_category_name[1]?$book_category_name[1]:$Lang['libms']['report']['book_category'].' 2')?>
							                    	<?=$BookCategoryOption1?>
							                    	<?=$BookCategoryOption2?>
							                    </td>
												<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_BookCategory") ?></div></td>                    	
						            		</tr>
						            		            		            		
						            		<tr>
						                    	<td class="field_title_short nowrap"><span class="field_title"><?= $Lang['libms']['settings']['resources_type'] ?></span></td>
						                    	<td><?=$BookResourcesOption?></td>
												<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_BookResources") ?></div></td>
						            		</tr>
						            		            		            		
						            		<tr>
						                    	<td class="field_title_short nowrap"><span class="field_title"><?= $Lang['libms']['book']['subject'] ?></span></td>
						                    	<td><input id="BookSubj" name="BookSubj" type="text" class="textboxtext inputselect" maxlength="64" value="<?=$Subject?>"/></td>
												<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_BookSubj") ?></div></td>
						            		</tr>
						            		            		            		
						            		<tr>
						                        <td class="field_title_short nowrap"><span class="field_title"><?=$Lang["libms"]["batch_edit"]["AllowBorrow"]?></span></td>
						                        <td><input name="BookBorrow" type="checkbox" name="BookBorrow" value="1" />
						                        	<label for="BookBorrow"><?=$Lang["libms"]["batch_edit"]["Yes"]?></label>
						                        </td>
												<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_BookBorrow") ?></div></td>
						            		</tr>
						            		            		            		
						            		<tr>
						                        <td class="field_title_short nowrap"><span class="field_title"><?=$Lang["libms"]["batch_edit"]["AllowReserve"]?></span></td>
						                        <td><input name="BookReserve" type="checkbox" name="BookReserve" value="1" />
						                        	<label for="BookBorrow"><?=$Lang["libms"]["batch_edit"]["Yes"]?></label>
						                        </td>
												<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_BookReserve") ?></div></td>
						            		</tr>
						            		
						            	</tbody>
									</table>
								</span>
							</span>
						</td>
					</tr>
				</table>
				
			</td>
		<tr>		
	</table>		

	
	<!-- for pass to batch update -->
	<input id="UpdateField" name="UpdateField" type="hidden" value="">
	
</form>

<?php


$linterface->LAYOUT_STOP();

 
intranet_closedb();


?>
