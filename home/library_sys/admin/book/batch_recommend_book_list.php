<?php
/*
 * 	Log
 * 
 * 	@Purpose:	listview of recommended books
 *
 *  2018-12-03 [Cameron]
 *      - show 'off-shelf' next to BookTitle if the eBook is expired [case #C152289]
 *       
 *	2017-08-01 [Cameron]
 *		- not need to set style="float: right;" for td element that holds keyword search field, this is to prevent search box outbound in PowerClass
 *
 * 	2017-06-06 [Cameron]
 * 		- place set cookie function after include global.php to support php5.4 [case #E117788]
 * 
 * 	2017-04-03 [Cameron]
 * 		- handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 				by stripslashes($keyword)
 *
 * 	2017-02-22 [Cameron]
 * 		- Fix: Description keep line feed
 *   
 * 	2016-11-10 [Cameron] 
 * 		- Add filter: BookFormat, ClassLevel, Recommend Date Range
 * 		- keyword search support: tag search
 * 		- add column BookFormat, ClassLevel
 * 
 * 	2016-11-09 [Cameron] fix bug: access right should be "book management" but not "settings"
 * 
 * 	2016-09-20 [Cameron] create this file
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");


### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	$order = 1;		# default in ascending order
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}


intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}


$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageMgmtBatchEdit";

$libms->MODULE_AUTHENTICATION($CurrentPage);

$TAGS_OBJ = $libms->get_batch_edit_tabs("RECOMMEND");

# Top menu highlight setting
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
unset($libms);

$linterface = new interface_html();

############################################################################################################

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;

$li = new libdbtable2007($field, $order, $pageNo);

# get filters 
$current_period= getPeriodOfAcademicYear(Get_Current_Academic_Year_ID());
$today = date('Y-m-d');
if ($current_period) {
	$current_period['StartDate'] = $current_period['StartDate'] ? date('Y-m-d',strtotime($current_period['StartDate'])) : $today;
}
else {
	$current_period['StartDate'] = $today;
}

$_POST['DateFrom'] = empty($_POST['DateFrom'])? $current_period['StartDate']:$_POST['DateFrom'];
$_POST['DateTo'] = empty($_POST['DateTo'])? $today:$_POST['DateTo'];
$date_range_filer = " AND r.DateModified>='".$_POST['DateFrom']." 00:00:00' AND r.DateModified<='".$_POST['DateTo']." 23:59:59'";

if ($_POST['BookFormat'] == 'physical') {
	$book_format_filter = " AND b.BookFormat='physical'";	
}
else if ($_POST['BookFormat'] == 'ebook') {
	$book_format_filter = " AND b.BookFormat<>'physical'";
}
else {
	$book_format_filter = "";
}

if ($_POST['ClassLevel']) {
	$class_level_filter = " AND CONCAT('|',r.RecommendGroup,'|') LIKE '%|".$_POST['ClassLevel']."|%'";
}
else {
	$class_level_filter = "";
}

$sql_cond = "";
$keyword = trim($keyword);
if (!get_magic_quotes_gpc()) {
	$keyword 	= stripslashes($keyword);
}

if($keyword!="")
{
	$unconverted_keyword_sql = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));		// A&<>'"\B ==> A&<>\'\"\\\\B
	$converted_keyword_sql = special_sql_str($keyword);											// A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B
	
	if ($unconverted_keyword_sql == $converted_keyword_sql) {		// keyword does not contain any special character: &<>" 
		$sql_cond .= " AND (b.Title LIKE '%$unconverted_keyword_sql%' OR b.Author LIKE '%$unconverted_keyword_sql%' OR r.Description LIKE '%$unconverted_keyword_sql%' OR t.TagName LIKE '%$unconverted_keyword_sql%')";
	} 
	else {	// keyword contains at least one of the special character: &<>'"\
		$sql_cond .= " AND (b.Title LIKE '%$unconverted_keyword_sql%' OR b.Title LIKE '%$converted_keyword_sql%'";
		$sql_cond .= " OR b.Author LIKE '%$unconverted_keyword_sql%' OR b.Author LIKE '%$converted_keyword_sql%'";
		$sql_cond .= " OR r.Description LIKE '%$unconverted_keyword_sql%' OR r.Description LIKE '%$converted_keyword_sql%'";
		$sql_cond .= " OR t.TagName LIKE '%$unconverted_keyword_sql%' OR t.TagName LIKE '%$converted_keyword_sql%')";
	}		// end keyword contains at least one of the special character: &<>'"\
	
	$joinTable = "LEFT JOIN INTRANET_ELIB_BOOK_TAG bt ON bt.BookID=b.BookID LEFT JOIN INTRANET_ELIB_TAG t ON t.TagID=bt.TagID";
}
else {
	$joinTable = "";
}
$sql_cond .= $book_format_filter . $class_level_filter . $date_range_filer;

$recommendedBySql = getNameFieldForRecord2('u.');

$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
$expiredBook = $lelibplus->getExpiredBookIDArr();
if (! empty($expiredBook)) {
    $subQuery = "LEFT JOIN (SELECT BookID FROM INTRANET_ELIB_BOOK WHERE BookID IN (".$expiredBook.")) AS e ON e.BookID=b.BookID ";
    $title = "IF(e.BookID IS NULL,b.Title,CONCAT(b.Title,' (','".$Lang["libms"]["portal"]["off_shelf"]."',')')) AS Title, ";
}
else {
    $subQuery = '';
    $title = "b.Title, ";
}

$sql = "SELECT YearID AS ClassLevelID, YearName AS LevelName FROM YEAR ORDER BY LevelName";
$classLevel = $li->returnResultSet($sql);
foreach((array)$classLevel as $k=>$v) {
	$classLevel[$k]['LevelName'] = $v['LevelName'];
}
$classLevel_array = BuildMultiKeyAssoc($classLevel, 'ClassLevelID');
//debug_r($classLevel_array);

$sql = "SELECT 
			".$title." b.`Author`, IF(b.`BookFormat`='physical','".$Lang["libms"]["portal"]["pBooks"]."','".$Lang["libms"]["portal"]["eBooks"]."') as BookFormat, 
			r.`RecommendGroup`, r.`Description`, ".$recommendedBySql." as RecommendedBy, r.`DateModified`,
            CONCAT('<input type=\'checkbox\' name=\'RecommendID[]\' id=\'RecommendID[]\' value=\'', r.`RecommendID`,'\'>'), 
			r.`UserID` as RecommendedByID
		FROM 
			INTRANET_ELIB_BOOK_RECOMMEND r
		INNER JOIN INTRANET_ELIB_BOOK b ON b.BookID = r.BookID
		INNER JOIN INTRANET_USER u ON u.UserID = r.UserID
        {$subQuery}
		{$joinTable}
		WHERE
            b.Publish=1 ".$sql_cond . " GROUP BY b.BookID";

$li->field_array = array("Title", "Author", "BookFormat", "RecommendGroup", "Description", "RecommendedBy", "DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $Lang['General']['Record'];
$li->column_array = array(0,0,0,0,18);		// Description keep line feed (nl2br)
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='13%' >".$li->column($pos++, $Lang["libms"]["batch_edit"]["BookTitle"])."</th>\n";
$li->column_list .= "<th width='13%' >".$li->column($pos++, $Lang["libms"]["batch_edit"]["Author"])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang["libms"]["batch_edit"]["BookFormat"])."</th>\n";
$li->column_list .= "<th width='16%' style='font-weight: bold;'>".$Lang["libms"]["batch_edit"]["RecommendedLevel"]."</th>\n";
$pos++;
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang["libms"]["batch_edit"]["Description"])."</th>\n";
$li->column_list .= "<th width='13%' >".$li->column($pos++, $Lang["libms"]["batch_edit"]["RecommendedBy"])."</th>\n";
$li->column_list .= "<th width='13%' >".$li->column($pos++, $Lang["libms"]["batch_edit"]["RecommendedOn"])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("RecommendID[]")."</th>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'batch_recommend_book_new.php')",$button_new,"","","",0);

$keyword = htmlspecialchars($keyword);
$searchKeyword = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"{$keyword}\" />";


########################

# Fileter - Book Format
$Filter_BookFormat = '
    <select class="form-control" id="BookFormat" name="BookFormat" onchange="document.form1.submit()">
      <option value="" '.(($BookFormat=='')?'selected':'').'>'.$Lang['libms']['bookmanagement']['allBooks'].'</option>
      <option value="ebook" '.(($BookFormat=='ebook')?'selected':'').'>'.$Lang["libms"]["portal"]["eBooks"].'</option>
      <option value="physical" '.(($BookFormat=='physical')?'selected':'').'>'.$Lang["libms"]["portal"]["pBooks"].'</option>
    </select>';

# Filter - Class Level
$ClassLevelArr[] = array("", "-- ".$Lang["libms"]["batch_edit"]["ClassLevel"]." --");
foreach((array)$classLevel as $k=>$v) {
	$ClassLevelArr[] = array($v['ClassLevelID'], $v['LevelName']);
}
$Filter_ClassLevel = $linterface->GET_SELECTION_BOX($ClassLevelArr, 'name="ClassLevel" id="ClassLevel" onchange="document.form1.submit()"', "", $ClassLevel, true);

# Filter - Recommend Date Range 
$Filter_DateRange  = $Lang["libms"]["batch_edit"]["RecommendedOn"] . ' ';
$Filter_DateRange .= $linterface->GET_DATE_PICKER("DateFrom",$_POST['DateFrom']) . ' ~ ';
$Filter_DateRange .= $linterface->GET_DATE_PICKER("DateTo",$_POST['DateTo']);
$Filter_DateRange .= '<input type="button" value="'.$Lang["libms"]["book"]["setting"]["filter_submit"].'" onclick="checkForm2();" />';

############################################################################################################


$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
<!--
function checkRemove(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}


function checkMultiEdit(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                obj.action=page;
                obj.method="post";
                obj.submit();
        }
}

function checkForm2() {
	var pass = check_date(document.getElementById("DateFrom"),"<?=$Lang['General']['InvalidDateFormat']?>");
	if (pass)
	 	pass = check_date(document.getElementById("DateTo"),"<?=$Lang['General']['InvalidDateFormat']?>");
	if (pass)
		pass = pass & compare_date(document.getElementById("DateFrom"), document.getElementById("DateTo"),"<?=$Lang['General']['WrongDateLogic']?>");
	if ( pass == true )	// true
	{
		document.form1.submit();
	}
}

$(document).ready( function(){
	$('#keyword').keyup(function(event) {
	  	if (event.which == 13) {
			document.form1.submit();
	  	}  
	});
	
	$('#keyword').keypress(function(e){
	    if ( e.which == 13 ) {
			e.preventDefault();
		    return false;
	    }
	});
});

//-->
</script>

<form name="form1" method="post" action="batch_recommend_book_list.php">

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="20%"><?=$toolbar ?></td>
											<td width="50%" align="center">&nbsp;</td>
											<td width="30%">
												<div class="content_top_tool"  style="float: right;">
													<div class="Conntent_search"><?=$searchKeyword?></div>     
													<br style="clear:both" />
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td height="28" valign="bottom" >
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="table-action-bar">
											<td valign="bottom">										
												<?=$Filter_BookFormat?>
												<?=$Filter_ClassLevel?>
												<?=$Filter_DateRange?>
											</td>
											<td valign="bottom">
												<div class="common_table_tool">
													<a href="javascript:checkEdit(document.form1,'RecommendID[]','batch_recommend_book_edit.php')" class="tool_edit"><?=$button_edit ?></a>
													<a href="javascript:checkRemove(document.form1,'RecommendID[]','batch_recommend_book_remove.php')" class="tool_delete"><?= $button_delete ?></a>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
<?
	// based on displayFormat_IP25_table
 	echo $li->displayFormat_RecommendBook_table('','','',$classLevel_array);
?>
					</td>
				</tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
			</td>
		</tr>
	</table>
</form><?php

$linterface->LAYOUT_STOP();

intranet_closedb();
?>
