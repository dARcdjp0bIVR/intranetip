<?php
/*
 * 	Using by: 
 * 
 * 	2016-04-15 [Cameron]
 * 		- correct return xmsg
 * 
 * 	2015-12-02 [Cameron]
 * 		- fix bug: add language file so that email subject and email body can be retrieved
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
//ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");



intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
/*
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}
//*/

// $Code is from $_POST, it's ReservationID
if(sizeof($Code)==0) {
	header("Location: reserve_book.php");	
}

	//$imploded_CodesList = '"'.implode('","',$Codes).'"';
	$CodesList = implode('","',$Code); //Henry Added 20130916
	$sql = <<<EOL
	SELECT `BookTitle`, `UserID`
	FROM `LIBMS_RESERVATION_LOG` rl
	JOIN `LIBMS_BOOK` b
		ON rl.`BookID` = b.`BookID`
	WHERE `ReservationID` IN ($CodesList);
EOL;
	
	$result = $libms->returnArray($sql);
	
	// inform each user who reserved the book about this cancellation 
	foreach($result as $row){
		$subject = $Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_admin']['subject'];
		$body  =  sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_admin']['mail_body'], date("Y-m-d"),$row['BookTitle']);
		
		do_email($row['UserID'], $subject, $body); //Henry Modified 20130916
	}



$result = $libms->REMOVE_RESERVATION($Code);
$xmsg = ($result) ? 'delete' : 'delete_failed'; 

//???? Adrian 
// $libel = new elibrary();
// for ($i=0; $i<sizeof($Code); $i++)
// {
// 	$book_id_now = $Code[$i];
// 	$libel->REMOVE_PHYSICAL_BOOK($book_id_now);
// }

intranet_closedb();

header("Location: reserve_book_more.php?Code=".$BookID."&xmsg=".$xmsg);

?>