<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
//ini_set('display_errors', 1);

# using: 

############ Change Log Start ###############
#
# 20160211 (Henry) : file created
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_periodical_item.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
############################################################################################################

## Get Data
$UniqueID = (isset($UniqueID) && count($UniqueID) > 0) ? $UniqueID : array();

## Use Library
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

# Access Checking
if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($UniqueID)==0) {
	header("Location: index.php");	
}

## Main
$libel = new elibrary();

if(isset($UniqueID)){
	foreach($UniqueID as $aUniqueID){
		$tempArr = $libms->GET_BOOK_ITEM_INFO($aUniqueID);
		$libms->UPDATE_UNIQUE_BOOK_ID($aUniqueID, $BookID);
		$libms->UPDATE_BOOK_COPY_INFO($tempArr[0]["BookID"]);
		$libms->UPDATE_BOOK_COPY_INFO($BookID);
	}
}

############################################################################################################
intranet_closedb();
header("Location: book_item_list.php?BookID=".$BookID."&FromPage=".$FromPage."&clearCoo=1&xmsg=UpdateSuccess");

?>