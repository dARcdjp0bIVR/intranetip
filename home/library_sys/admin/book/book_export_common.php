<?php

/*
 * Using: 
 * Log
 *
 * Date: 2019-06-14 (Henry)
 * - added ItemSeriesNum
 *
 * Date: 2018-05-09 (Cameron)
 * - ExcludeWriteoff but include RecordStatus=null [IP2.5.9.5.1]
 *
 * Date: 2018-04-26 (Cameron)
 * - apply keyword search method of the following case so that it'll be consistent between book search page and export result [case #Q138677]
 * allow keyword search to get result of any combination of single word in the field [case #F132432]
 * apply to following fields only: Publisher, BookTitle, BookSubTitle, ResponsibilityBy1~3, Series
 *
 * Date: 2016-12-28 (Cameron)
 * - add function getExportSelectedFields()
 *
 * Date: 2016-09-30 (Cameron)
 * - add filter "ExcludeWriteoff" to getBibliographySearchFiler() [case #F98576]
 *
 * Date: 2016-02-15 (Henry)
 * - add 'NA' to book category filter
 *
 * Date: 2015-10-08 [Cameron]
 * - fix bug: should not trim Code in getBibliographySearchFiler() because it's array
 *
 * Date: 2015-10-05 [Cameron]
 * - fix bug on basic search / advance search for string like A&<>'"\B, support comparing unconverted string A&<>'"\B ( created by imoport)
 * and converted string A&amp;'&quot;&lt;&gt;\B (created by add / edit)
 *
 * Date: 2015-07-31 [Cameron]
 * - remove restriction on BookCategoryType=1 when filter BookCategory record which Language is not set
 *
 * Date: 2015-03-25 [Cameron]
 * Details: add advance seach condition in getBibliographySearchFiler()
 *
 * Date: 2015-01-30 [Henry]
 * Details: support 2 type of book category
 *
 * Date: 2014-12-04 [Cameron]
 * Details: consolidate common functions for book export
 * fix bug of export records: search by exact ACNO or ACNO range
 *
 * Date: 2014-12-02 [Cameron]
 * Details: add search max(ACNO) by pattern of {prefix}^ in keywords field
 *
 */
function get_xxxConditions_SQL($fieldname, $selected = '')
{
    $conds = '';
    if ($selected != '') { // 2015-07-27: don't use !empty() as $selected value can be '0'
        $v = trim($selected);
        $sql_safe_CODE = PHPToSQL($v);
        $conds = " AND TRIM(`{$fieldname}`) LIKE {$sql_safe_CODE} ";
    }
    
    return $conds;
}

/*
 * @param: $alias_LIBMS_BOOK -- LIBMS_BOOK
 * $alias_LIBMS_BOOK_UNIQUE -- LIBMS_BOOK_UNIQUE
 * @return: concatination of filter
 */
function getBibliographySearchFiler($alias_LIBMS_BOOK = 'lb', $alias_LIBMS_BOOK_UNIQUE = 'lbu')
{
    $b = $alias_LIBMS_BOOK;
    $bu = $alias_LIBMS_BOOK_UNIQUE;
    $advanceSearch = trim($_REQUEST['advanceSearch']);
    $keyword = trim($_REQUEST['keyword']);
    // for the 2 type of book category
    $BookCategoryCodeTemp = explode(":", trim($_REQUEST['BookCategoryCode']));
    $BookCategoryType = trim($BookCategoryCodeTemp[0]);
    $BookCategoryCode = trim($BookCategoryCodeTemp[1]);
    $ResourcesTypeCode = trim($_REQUEST['ResourcesTypeCode']);
    $CirculationTypeCode = trim($_REQUEST['CirculationTypeCode']);
    $SubjectSelect = trim($_REQUEST['SubjectSelect']);
    $PublisherSelect = trim($_REQUEST['PublisherSelect']);
    $LanguageSelect = trim($_REQUEST['LanguageSelect']);
    $bookCover = trim($_REQUEST['bookCover']);
    $Code = $_REQUEST['Code'];
    $ExcludeWriteoff = $_REQUEST['ExcludeWriteoff'];
    
    // start advance search fields
    $ACNO = trim($_REQUEST['ACNO']);
    $publisher = trim($_REQUEST['publisher']);
    $author = trim($_REQUEST['author']);
    $callNumber = trim($_REQUEST['callNumber']);
    $ISBN = trim($_REQUEST['ISBN']);
    $bookTitle = trim($_REQUEST['bookTitle']);
    // end advance search fields
    
    $ret = ""; // return string
               
    // (1) selected books (Code)
    if (is_array($Code) && sizeof($Code) > 0) {
        $conds_ids = " AND {$b}.BookID IN (" . implode(", ", $Code) . ")";
        $ret .= $conds_ids . " ";
    }
    
    if ($ExcludeWriteoff) {
        $ret .= " AND ({$bu}.RecordStatus<>'WRITEOFF' OR {$bu}.RecordStatus IS NULL)";
    }
    
    // (2) filters from option list - BookCategory, ResourcesType, CirculationType
    $conds_filter = get_xxxConditions_SQL('BookCategoryCode', $BookCategoryCode);
    // for the 2 type of book category
    if ($BookCategoryType == 'NA') {
        $conds_filter .= " AND ({$b}.BookCategoryCode = '' OR {$b}.BookCategoryCode is null) ";
    } else if ($_REQUEST['BookCategoryCode'] != "") {
        $conds_filter .= ' AND (Language IN (SELECT lbl2.BookLanguageCode FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookCategoryType = "' . $BookCategoryType . '") OR Language IS NULL OR Language = "") ';
    }
    $conds_filter .= get_xxxConditions_SQL('ResourcesTypeCode', $ResourcesTypeCode);
    // $conds_filter .= get_xxxConditions_SQL('CirculationTypeCode', $CirculationTypeCode);
    if ($CirculationTypeCode != "") {
        $conds_filter .= " AND ((lb.CirculationTypeCode is not null AND lb.CirculationTypeCode='" . addslashes($CirculationTypeCode) . "')" . " OR (lbu.CirculationTypeCode is not null AND lbu.CirculationTypeCode='" . addslashes($CirculationTypeCode) . "') ) ";
    }
    
    $ret .= ($conds_filter) ? $conds_filter . " " : "";
    
    // (3.1) filters from option list - Subject
    if ($SubjectSelect != '') {
        if ($SubjectSelect == 'NA') {
            $subjectSelect_cond = " AND ({$b}.Subject = '' OR {$b}.Subject is null)";
        } else {
            $subjectSelect_cond = " AND ({$b}.Subject = '" . addslashes(htmlspecialchars($SubjectSelect)) . "' or {$b}.Subject = '" . $SubjectSelect . "') ";
        }
        $ret .= $subjectSelect_cond . " ";
    }
    
    // # (3.2) PublisherSelect Sql Cond
    if ($PublisherSelect != '') {
        if ($PublisherSelect == 'NA') {
            $publisherSelect_cond = " AND ({$b}.Publisher = '' OR {$b}.Publisher is null)";
        } else {
            $publisherSelect_cond = " AND ({$b}.Publisher = '" . addslashes(htmlspecialchars($PublisherSelect)) . "' or {$b}.Publisher = '" . $PublisherSelect . "') ";
        }
        $ret .= $publisherSelect_cond . " ";
    }
    
    // Henry added 2014014
    // # (3.3) LanguageSelect Sql Cond
    if ($LanguageSelect != '') {
        if ($LanguageSelect == 'NA') {
            $languageSelect_cond = " AND ({$b}.Language = '' OR {$b}.Language is null)";
        } else {
            $languageSelect_cond = " AND ({$b}.Language = '" . addslashes(htmlspecialchars($LanguageSelect)) . "' or {$b}.Language = '" . $LanguageSelect . "') ";
        }
        $ret .= $languageSelect_cond . " ";
    }
    
    // Tommy added 20191111
    // # Book Cover Image Sql Cond
    if($bookCover != 0 && $bookCover != ''){
        $bookCover_cond = " AND {$b}.CoverImage != '' ";
        $ret .= $bookCover_cond . " ";
    }else{
        $bookCover_cond = "";
    }
    
    if ($advanceSearch) {
        $advCond = "";
        if ($ACNO != "") {
            $unconverted_ACNO = mysql_real_escape_string(str_replace("\\", "\\\\", $ACNO));
            $converted_ACNO = special_sql_str($ACNO);
            $advCond .= " AND (
				{$bu}.`ACNO` LIKE '%$unconverted_ACNO%' OR {$bu}.`ACNO` LIKE '%$converted_ACNO%'
				)";
        }
        
        if ($publisher != "") {
            $unconverted_publisher = mysql_real_escape_string(str_replace("\\", "\\\\", $publisher));
            $converted_publisher = special_sql_str($publisher);
            $advCond .= " AND (
				{$b}.`Publisher` LIKE '%$unconverted_publisher%' OR {$b}.`Publisher` LIKE '%$converted_publisher%'
				)";
        }
        
        if ($author != "") {
            $unconverted_author = mysql_real_escape_string(str_replace("\\", "\\\\", $author));
            $converted_author = special_sql_str($author);
            $advCond .= " AND (
					{$b}.`ResponsibilityBy1` LIKE '%$unconverted_author%' OR {$b}.`ResponsibilityBy1` LIKE '%$converted_author%' OR 
					{$b}.`ResponsibilityBy2` LIKE '%$unconverted_author%' OR {$b}.`ResponsibilityBy2` LIKE '%$converted_author%' OR 
					{$b}.`ResponsibilityBy3` LIKE '%$unconverted_author%' OR {$b}.`ResponsibilityBy3` LIKE '%$converted_author%'
				)";
        }
        
        if ($callNumber != "") {
            $unconverted_callNumber = mysql_real_escape_string(str_replace("\\", "\\\\", $callNumber));
            $converted_callNumber = special_sql_str($callNumber);
            $advCond .= " AND (
				{$b}.`CallNum` LIKE '%$unconverted_callNumber%' OR {$b}.`CallNum` LIKE '%$converted_callNumber%' OR 
				{$b}.`CallNum2` LIKE '%$unconverted_callNumber%' OR {$b}.`CallNum2` LIKE '%$converted_callNumber%' OR 
				CONCAT({$b}.`CallNum`, ' ', {$b}.`CallNum2`) LIKE '%$unconverted_callNumber%' OR 
				CONCAT({$b}.`CallNum`, ' ', {$b}.`CallNum2`) LIKE '%$converted_callNumber%'
				)";
        }
        
        if ($ISBN != "") {
            $unconverted_ISBN = mysql_real_escape_string(str_replace("\\", "\\\\", $ISBN));
            $converted_ISBN = special_sql_str($ISBN);
            $advCond .= " AND
				({$b}.`ISBN` LIKE '%$unconverted_ISBN%' OR {$b}.`ISBN` LIKE '%$converted_ISBN%' OR 
				{$b}.`ISBN2` LIKE '%$unconverted_ISBN%' OR {$b}.`ISBN2` LIKE '%$converted_ISBN%')";
        }
        
        if ($bookTitle != "") {
            $unconverted_bookTitle = mysql_real_escape_string(str_replace("\\", "\\\\", $bookTitle)); // A&<>'"\B ==> A&<>\'\"\\\\B
            $converted_bookTitle = special_sql_str($bookTitle); // A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B
            $advCond .= " AND
				({$b}.`BookTitle` LIKE '%$unconverted_bookTitle%' OR {$b}.`BookTitle` LIKE '%$converted_bookTitle%' OR 
				{$b}.`BookSubTitle` LIKE '%$unconverted_bookTitle%' OR {$b}.`BookSubTitle` LIKE '%$converted_bookTitle%')";
        }
        
        $ret .= $advCond;
    } // end advance search
else { // not advance search
           // (4) keyword
        if ($keyword != "") {
            $acno_filter = "";
            $unconverted_keyword_sql = mysql_real_escape_string(str_replace("\\", "\\\\", $keyword)); // A&<>'"\B ==> A&<>\'\"\\\\B
            $converted_keyword_sql = special_sql_str($keyword); // A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B
            $keyword_sql = $unconverted_keyword_sql;
            
            preg_match('/^(\s*)((\w)+)(\s*)~(\s*)((\w)+)(\s*)$/', $keyword_sql, $matches_range);
            preg_match('/^(\s*)\\\"(\s*)((\w)+)(\s*)\\\"(\s*)$/', $keyword_sql, $matches_strict);
            preg_match('/^(\s*)((\w)+)(\^)(\s*)$/', $keyword_sql, $matches_max);
            if (! isset($matches_strict[3]) || $matches_strict[3] == "") {
                preg_match("/^(\s*)\\\'(\s*)((\w)+)(\s*)\\\'(\s*)$/", $keyword_sql, $matches_strict);
            }
            $acno_strict = $matches_strict[3];
            // (4.1) by range
            if ($matches_range[0] != "") {
                $iACNO_start = trim($matches_range[2]);
                $iACNO_end = trim($matches_range[6]);
                if ($iACNO_start != "" || $iACNO_end != "") {
                    $conditions[] = "{$bu}.`RecordStatus`<>'DELETE'";
                    
                    if ($iACNO_start != "" && $iACNO_end != "") {
                        $conditions[] = "{$bu}.`ACNO` >= '{$iACNO_start}'";
                        $conditions[] = "{$bu}.`ACNO` <= '{$iACNO_end}'";
                    } elseif ($iACNO_start != "") {
                        $conditions[] = "{$bu}.`ACNO` = '{$iACNO_start}'";
                    } elseif ($iACNO_end != "") {
                        $conditions[] = "{$bu}.`ACNO` = '{$iACNO_end}'";
                    }
                    // if (!empty($sDate_start)){
                    // $conditions[] = "{$bu}.`PurchaseDate` >= '{$sDate_start}'";
                    // }
                    // if (!empty($sDate_end)){
                    // $conditions[] = "{$bu}.`PurchaseDate` <= '{$sDate_end}'";
                    // }
                    
                    if (! empty($conditions)) {
                        $acno_filter = '(' . implode(') AND (', $conditions) . ')';
                    }
                }
            }            // (4.2) by exact match
            elseif (isset($acno_strict) && $acno_strict != "") {
                $acno_filter = "{$bu}.ACNO = '$acno_strict'";
            }            // (4.3) by max ACNO
            elseif ($matches_max[0] != "") {
                if ($matches_max[2] != "") {
                    $acno_filter = "{$bu}.ACNO = (select max(ACNO) from `LIBMS_BOOK_UNIQUE` where ACNO LIKE '" . $matches_max[2] . "%' limit 1)";
                }
            } else {
                if ($unconverted_keyword_sql == $converted_keyword_sql) {
                    $acno_filter = "{$bu}.ACNO LIKE '%$keyword_sql%'";
                } else {
                    $acno_filter = "({$bu}.ACNO LIKE '%$unconverted_keyword_sql%' OR {$bu}.ACNO LIKE '%$converted_keyword_sql%')";
                }
            }
            
            if ($unconverted_keyword_sql == $converted_keyword_sql) { // keyword does not contain any special character: &<>"
                $words = getWords($unconverted_keyword_sql);
                $keyword_cond2 = " AND ( ";
                $keyword_cond2 .= "{$bu}.BarCode LIKE '%$keyword_sql%' OR ";
                $keyword_cond2 .= "({$b}.Publisher like '%" . implode("%' AND {$b}.Publisher LIKE'%", $words) . "%') OR ";
                $keyword_cond2 .= ($acno_filter) ? $acno_filter . " OR " : "";
                $keyword_cond2 .= "{$b}.CallNum LIKE '%$keyword_sql%' OR
					{$b}.CallNum2 LIKE '%$keyword_sql%' OR
					CONCAT({$b}.CallNum, ' ', {$b}.CallNum2) LIKE '%$keyword_sql%' OR
					{$b}.ISBN LIKE '%$keyword_sql%' OR
					{$b}.ISBN2 LIKE '%$keyword_sql%' OR ";
                $keyword_cond2 .= "({$b}.BookTitle like '%" . implode("%' AND {$b}.BookTitle LIKE'%", $words) . "%') OR ";
                $keyword_cond2 .= "({$b}.BookSubTitle like '%" . implode("%' AND {$b}.BookSubTitle LIKE'%", $words) . "%') OR ";
                $keyword_cond2 .= "({$b}.ResponsibilityBy1 like '%" . implode("%' AND {$b}.ResponsibilityBy1 LIKE'%", $words) . "%') OR ";
                $keyword_cond2 .= "({$b}.ResponsibilityBy2 like '%" . implode("%' AND {$b}.ResponsibilityBy2 LIKE'%", $words) . "%') OR ";
                $keyword_cond2 .= "({$b}.ResponsibilityBy3 like '%" . implode("%' AND {$b}.ResponsibilityBy3 LIKE'%", $words) . "%') OR ";
                $keyword_cond2 .= "({$b}.Series like '%" . implode("%' AND {$b}.Series LIKE'%", $words) . "%')";
                $keyword_cond2 .= ") ";
            } else {
                $words_1 = getWords($unconverted_keyword_sql);
                $words_2 = getWords($converted_keyword_sql);
                
                $keyword_cond2 = " AND ( ";
                $keyword_cond2 = "{$bu}.BarCode LIKE '%$unconverted_keyword_sql%' OR {$bu}.BarCode LIKE '%$converted_keyword_sql%' OR ";
                $keyword_cond2 .= "({$b}.Publisher like '%" . implode("%' AND {$b}.Publisher LIKE'%", $words_1) . "%') OR ({$b}.Publisher like '%" . implode("%' AND {$b}.Publisher LIKE'%", $words_2) . "%') OR ";
                $keyword_cond2 .= ($acno_filter) ? $acno_filter . " OR " : "";
                $keyword_cond2 .= "{$b}.CallNum LIKE '%$unconverted_keyword_sql%' OR {$b}.CallNum LIKE '%$converted_keyword_sql%' OR 
					{$b}.CallNum2 LIKE '%$unconverted_keyword_sql%' OR {$b}.CallNum2 LIKE '%$converted_keyword_sql%' OR 
					CONCAT({$b}.CallNum, ' ', {$b}.CallNum2) LIKE '%$unconverted_keyword_sql%' OR
					CONCAT({$b}.CallNum, ' ', {$b}.CallNum2) LIKE '%$converted_keyword_sql%' OR 
					{$b}.ISBN LIKE '%$unconverted_keyword_sql%' OR {$b}.ISBN LIKE '%$converted_keyword_sql%' OR 
					{$b}.ISBN2 LIKE '%$unconverted_keyword_sql%' OR {$b}.ISBN2 LIKE '%$converted_keyword_sql%' OR ";
                $keyword_cond2 .= "({$b}.BookTitle like '%" . implode("%' AND {$b}.BookTitle LIKE'%", $words_1) . "%') OR ({$b}.BookTitle like '%" . implode("%' AND {$b}.BookTitle LIKE'%", $words_2) . "%') OR ";
                $keyword_cond2 .= "({$b}.BookSubTitle like '%" . implode("%' AND {$b}.BookSubTitle LIKE'%", $words_1) . "%') OR ({$b}.BookSubTitle like '%" . implode("%' AND {$b}.BookSubTitle LIKE'%", $words_2) . "%') OR ";
                $keyword_cond2 .= "({$b}.ResponsibilityBy1 like '%" . implode("%' AND {$b}.ResponsibilityBy1 LIKE'%", $words_1) . "%') OR ({$b}.ResponsibilityBy1 like '%" . implode("%' AND {$b}.ResponsibilityBy1 LIKE'%", $words_2) . "%') OR ";
                $keyword_cond2 .= "({$b}.ResponsibilityBy2 like '%" . implode("%' AND {$b}.ResponsibilityBy2 LIKE'%", $words_1) . "%') OR ({$b}.ResponsibilityBy2 like '%" . implode("%' AND {$b}.ResponsibilityBy2 LIKE'%", $words_2) . "%') OR ";
                $keyword_cond2 .= "({$b}.ResponsibilityBy3 like '%" . implode("%' AND {$b}.ResponsibilityBy3 LIKE'%", $words_1) . "%') OR ({$b}.ResponsibilityBy3 like '%" . implode("%' AND {$b}.ResponsibilityBy3 LIKE'%", $words_2) . "%') OR ";
                $keyword_cond2 .= "({$b}.Series like '%" . implode("%' AND {$b}.Series LIKE'%", $words_1) . "%') OR ({$b}.Series like '%" . implode("%' AND {$b}.Series LIKE'%", $words_2) . "%')";
                $keyword_cond2 .= ") ";
            }
            $ret .= $keyword_cond2;
        } // end $keyword != ''
    } // end not advance search
    
    return trim($ret);
}
 // end function getBibliographySearchFiler()
function getExportSelectedFields()
{
    global $PATH_WRT_ROOT, $Lang;
    
    include_once ($PATH_WRT_ROOT . "lang/libms.lang.b5.php");
    
    // # pickup selected fields
    $selFields = array();
    $selChiCol = array();
    $selEngCol = array();
    $i = 0;
    
    if (in_array('BookTitle', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.BookTitle';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["BookTitle"];
        $selEngCol[$i ++] = 'BookTitle';
    }
    if (in_array('BookSubtitle', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.BookSubtitle';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["BookSubtitle"];
        $selEngCol[$i ++] = 'BookSubtitle';
    }
    if (in_array('CallNum', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.CallNum';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["CallNum"];
        $selEngCol[$i ++] = 'CallNum';
    }
    if (in_array('CallNum2', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.CallNum2';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["CallNum2"];
        $selEngCol[$i ++] = 'CallNum2';
    }
    if (in_array('ISBN', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.ISBN';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ISBN"];
        $selEngCol[$i ++] = 'ISBN';
    }
    if (in_array('ISBN2', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.ISBN2';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ISBN2"];
        $selEngCol[$i ++] = 'ISBN2';
    }
    if (in_array('Language', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.Language';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["Language"];
        $selEngCol[$i ++] = 'Language';
    }
    if (in_array('Country', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.Country';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["Country"];
        $selEngCol[$i ++] = 'Country';
    }
    if (in_array('Introduction', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.Introduction';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["Introduction"];
        $selEngCol[$i ++] = 'Introduction';
    }
    if (in_array('Edition', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.Edition';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["Edition"];
        $selEngCol[$i ++] = 'Edition';
    }
    if (in_array('PublishYear', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.PublishYear';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["PublishYear"];
        $selEngCol[$i ++] = 'PublishYear';
    }
    if (in_array('Publisher', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.Publisher';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["Publisher"];
        $selEngCol[$i ++] = 'Publisher';
    }
    if (in_array('PublishPlace', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.PublishPlace';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["PublishPlace"];
        $selEngCol[$i ++] = 'PublishPlace';
    }
    if (in_array('Series', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.Series';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["Series"];
        $selEngCol[$i ++] = 'Series';
    }
    if (in_array('SeriesNum', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.SeriesNum';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["SeriesNum"];
        $selEngCol[$i ++] = 'SeriesNum';
    }
    if (in_array('ResponsibilityCode1', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.ResponsibilityCode1';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ResponsibilityCode1"];
        $selEngCol[$i ++] = 'ResponsibilityCode1';
    }
    if (in_array('ResponsibilityBy1', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.ResponsibilityBy1';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ResponsibilityBy1"];
        $selEngCol[$i ++] = 'ResponsibilityBy1';
    }
    if (in_array('ResponsibilityCode2', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.ResponsibilityCode2';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ResponsibilityCode2"];
        $selEngCol[$i ++] = 'ResponsibilityCode2';
    }
    if (in_array('ResponsibilityBy2', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.ResponsibilityBy2';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ResponsibilityBy2"];
        $selEngCol[$i ++] = 'ResponsibilityBy2';
    }
    if (in_array('ResponsibilityCode3', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.ResponsibilityCode3';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ResponsibilityCode3"];
        $selEngCol[$i ++] = 'ResponsibilityCode3';
    }
    if (in_array('ResponsibilityBy3', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.ResponsibilityBy3';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ResponsibilityBy3"];
        $selEngCol[$i ++] = 'ResponsibilityBy3';
    }
    if (in_array('Dimension', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.Dimension';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["Dimension"];
        $selEngCol[$i ++] = 'Dimension';
    }
    if (in_array('ILL', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.ILL';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ILL"];
        $selEngCol[$i ++] = 'ILL';
    }
    if (in_array('NoOfPage', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.NoOfPage';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["NoOfPage"];
        $selEngCol[$i ++] = 'NoOfPage';
    }
    if (in_array('BookCategoryCode', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.BookCategoryCode';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["BookCategoryCode"];
        $selEngCol[$i ++] = 'BookCategoryCode';
    }
    if (in_array('CirculationTypeCode', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.CirculationTypeCode';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["CirculationTypeCode"];
        $selEngCol[$i ++] = 'CirculationTypeCode';
    }
    if (in_array('ResourcesTypeCode', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.ResourcesTypeCode';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ResourcesTypeCode"];
        $selEngCol[$i ++] = 'ResourcesTypeCode';
    }
    if (in_array('Subject', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.Subject';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["Subject"];
        $selEngCol[$i ++] = 'Subject';
    }
    if (in_array('BookInternalremark', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.RemarkInternal AS BookInternalremark';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["BookInternalremark"];
        $selEngCol[$i ++] = 'BookInternalremark';
    }
    if (in_array('OpenBorrow', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.OpenBorrow';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["OpenBorrow"];
        $selEngCol[$i ++] = 'OpenBorrow';
    }
    if (in_array('OpenReservation', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.OpenReservation';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["OpenReservation"];
        $selEngCol[$i ++] = 'OpenReservation';
    }
    if (in_array('BookRemarkToUser', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.RemarkToUser';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["BookRemarkToUser"];
        $selEngCol[$i ++] = 'BookRemarkToUser';
    }
    if (in_array('Tags', $_POST['hidSelFields'])) {
        $tagIndex = $i;
        $selFields[$i] = 'lb.BookID AS Tags';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["Tags"];
        $selEngCol[$i ++] = 'Tags';
    }
    if (in_array('URL', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lb.URL';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["URL"];
        $selEngCol[$i ++] = 'URL';
    }
    if (in_array('ACNO', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.ACNO';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ACNO"];
        $selEngCol[$i ++] = 'ACNO';
    }
    if (in_array('BarCode', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.BarCode';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["BarCode"];
        $selEngCol[$i ++] = 'BarCode';
    }
    if (in_array('LocationCode', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.LocationCode';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["LocationCode"];
        $selEngCol[$i ++] = 'LocationCode';
    }
    if (in_array('AccountDate', $_POST['hidSelFields'])) {
        $accountDateIndex = $i;
        $selFields[$i] = 'lbu.CreationDate AS AccountDate';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["AccountDate"];
        $selEngCol[$i ++] = 'AccountDate';
    }
    if (in_array('PurchaseDate', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.PurchaseDate';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["PurchaseDate"];
        $selEngCol[$i ++] = 'PurchaseDate';
    }
    if (in_array('PurchasePrice', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.PurchasePrice';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["PurchasePrice"];
        $selEngCol[$i ++] = 'PurchasePrice';
    }
    if (in_array('ListPrice', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.ListPrice';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ListPrice"];
        $selEngCol[$i ++] = 'ListPrice';
    }
    if (in_array('Discount', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.Discount';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["Discount"];
        $selEngCol[$i ++] = 'Discount';
    }
    if (in_array('Distributor', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.Distributor';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["Distributor"];
        $selEngCol[$i ++] = 'Distributor';
    }
    if (in_array('PurchaseByDepartment', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.PurchaseByDepartment';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["PurchaseByDepartment"];
        $selEngCol[$i ++] = 'PurchaseByDepartment';
    }
    if (in_array('PurchaseNote', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.PurchaseNote';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["PurchaseNote"];
        $selEngCol[$i ++] = 'PurchaseNote';
    }
    if (in_array('InvoiceNumber', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.InvoiceNumber';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["InvoiceNumber"];
        $selEngCol[$i ++] = 'InvoiceNumber';
    }
    if (in_array('AccompanyMaterial', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.AccompanyMaterial';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["AccompanyMaterial"];
        $selEngCol[$i ++] = 'AccompanyMaterial';
    }
    if (in_array('ItemRemarkToUser', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.RemarkToUser';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ItemRemarkToUser"];
        $selEngCol[$i ++] = 'ItemRemarkToUser';
    }
    if (in_array('ItemStatus', $_POST['hidSelFields'])) {
        $statusIndex = $i;
        $selFields[$i] = 'lbu.RecordStatus';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ItemStatus"];
        $selEngCol[$i ++] = 'ItemStatus';
    }
    if (in_array('ItemCirculationTypeCode', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.CirculationTypeCode';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ItemCirculationTypeCode"];
        $selEngCol[$i ++] = 'ItemCirculationTypeCode';
    }
    if (in_array('ItemSeriesNum', $_POST['hidSelFields'])) {
        $selFields[$i] = 'lbu.ItemSeriesNum';
        $selChiCol[$i] = $Lang["libms"]["import_book"]["ImportCSVDataCol"]["ItemSeriesNum"];
        $selEngCol[$i ++] = 'ItemSeriesNum';
    }
    
    $ret = array();
    $ret['selFields'] = $selFields;
    $ret['selChiCol'] = $selChiCol;
    $ret['selEngCol'] = $selEngCol;
    $ret['tagIndex'] = $tagIndex;
    $ret['accountDateIndex'] = $accountDateIndex;
    $ret['statusIndex'] = $statusIndex;
    
    return $ret;
}
?>