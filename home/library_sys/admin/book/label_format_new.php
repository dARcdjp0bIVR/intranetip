<?php

//Modifying by 

/********************
 * Log :
 *
 * Date		2017-11-14 [Henry]
 * 			Add text_alignment selection box [case #E130799]
 * 
 * Date		2017-09-08 [Cameron]
 * 			wrap label out of is_bold input so that it can be more easy to choose
 * 
 * Date		2017-03-24 [Cameron]
 * 			is_bold font applies to Spine Label only
 *  
 * Date		2017-03-01 [Cameron]
 * 			Add is_bold font checkbox option [case #D113002]
 * 
 * Date		2016-01-18 [Cameron]
 * 			fix bug: use ' instead of " on showing $Lang["libms"]["label"]["msg"]["labelInfoErrorFillAll"] because English version contains "N/A"
 *   
 * Date		2013-10-31 [Henry]
 * 			Added the order of label information selection
 * 
 ********************/
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) { 
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
/*
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $libms->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}
//*/

$linterface = new interface_html();

//$groupSelection = $libms->getGroupSelection();

# Top menu highlight setting
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagementLabel";


$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_label'], "prepare_label.php", 0);
$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_labelformat'], "label_format.php", 1);

$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_labelformat'], "label_format.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['new'], "");

# Left menu 
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

# Start layout
$linterface->LAYOUT_START();

# option for open  ......

foreach ($Lang['libms']['label']['unit'] as $unit =>$disp_unit ){
	$metric_unit[]=array($unit,$disp_unit);
}

foreach ($Lang['libms']['label']['text_alignment_option'] as $text_alignment_key =>$disp_text_alignment ){
	$text_alignments[]=array($text_alignment_key,$disp_text_alignment);
}
//--- Henry added 20131031 [start]
$labelInfoOrder = array();
$labelInfoOrder[] = array('BookTitle',$Lang["libms"]["label"]["BookTitle"].' / '.$Lang["libms"]["book"]["series_number"]);
$labelInfoOrder[] = array('LocationCode',$Lang["libms"]["label"]["LocationCode"]);
$labelInfoOrder[] = array('CallNumCallNum2',$Lang["libms"]["label"]["CallNumCallNum2"]);
$labelInfoOrder[] = array('BarCode',$Lang["libms"]["label"]["BarCode"]);
$labelInfoOrder[] = array('BookCode',$Lang["libms"]["label"]["BookCode"]);
$labelInfoOrder[] = array('print_school_name',$Lang["libms"]["label"]["print_school_name"]);

$labelInfoOrderSelections = '';
for($i=0; $i<count($labelInfoOrder); $i++){
	$labelInfoOrderSelections .= '('.($i+1).') '.$linterface->GET_SELECTION_BOX($labelInfoOrder, 'id="info_order_'.($i+1).'" name="info_order_'.($i+1).'"', $Lang["libms"]["status"]["na"], "").'<br/>';
}
//--- Henry added 20131031 [End]

?>

<script language="javascript">
function checkForm(form1) {

	var name, val_null, val_wrong;
	val_null = val_wrong = false;
	
	$('input.number').each(function(){
		if (!(val_null || val_wrong)){
			if ($(this).val() == "") {
				name = $(this).attr("id")
				alert("<?= $i_alert_pleasefillin.' '?>"+name);	
				$(this).focus();
				val_null = true;
				}
			
			if (($(this).attr("name") != "name") && (!val_null) && (!IsValidInput($(this)))){
				$(this).focus();
				val_wrong = true;
			}
		}
	});
	
	//Henry added 20131031 [start]
	if(!val_null && !val_wrong){
		if($('#info_order_1').val() != '' && $('#info_order_2').val() != '' && $('#info_order_3').val() != '' && $('#info_order_4').val() != '' && $('#info_order_5').val() != '' && $('#info_order_6').val() != ''){
			if($('#info_order_1').val() == $('#info_order_2').val() || $('#info_order_1').val() == $('#info_order_3').val() || $('#info_order_1').val() == $('#info_order_4').val() || $('#info_order_1').val() == $('#info_order_5').val() || $('#info_order_1').val() == $('#info_order_6').val()){
				alert("<?=$Lang["libms"]["label"]['msg']["labelInfoError"]?>");
				$('#info_order_1').focus();
				return false;
			}
			else if($('#info_order_2').val() == $('#info_order_1').val() || $('#info_order_2').val() == $('#info_order_3').val() || $('#info_order_2').val() == $('#info_order_4').val() || $('#info_order_2').val() == $('#info_order_5').val() || $('#info_order_2').val() == $('#info_order_6').val()){
				alert("<?=$Lang["libms"]["label"]['msg']["labelInfoError"]?>");
				$('#info_order_2').focus();
				return false;
			}
			else if($('#info_order_3').val() == $('#info_order_1').val() || $('#info_order_3').val() == $('#info_order_2').val() || $('#info_order_3').val() == $('#info_order_4').val() || $('#info_order_3').val() == $('#info_order_5').val() || $('#info_order_3').val() == $('#info_order_6').val()){
				alert("<?=$Lang["libms"]["label"]['msg']["labelInfoError"]?>");
				$('#info_order_3').focus();
				return false;
			}
			else if($('#info_order_4').val() == $('#info_order_1').val() || $('#info_order_4').val() == $('#info_order_2').val() || $('#info_order_4').val() == $('#info_order_3').val() || $('#info_order_4').val() == $('#info_order_5').val() || $('#info_order_4').val() == $('#info_order_6').val()){
				alert("<?=$Lang["libms"]["label"]['msg']["labelInfoError"]?>");
				$('#info_order_4').focus();
				return false;
			}
			else if($('#info_order_5').val() == $('#info_order_1').val() || $('#info_order_5').val() == $('#info_order_2').val() || $('#info_order_5').val() == $('#info_order_3').val() || $('#info_order_5').val() == $('#info_order_4').val() || $('#info_order_5').val() == $('#info_order_6').val()){
				alert("<?=$Lang["libms"]["label"]['msg']["labelInfoError"]?>");
				$('#info_order_5').focus();
				return false;
			}
			else if($('#info_order_6').val() == $('#info_order_1').val() || $('#info_order_6').val() == $('#info_order_2').val() || $('#info_order_6').val() == $('#info_order_3').val() || $('#info_order_6').val() == $('#info_order_4').val() || $('#info_order_6').val() == $('#info_order_5').val()){
				alert("<?=$Lang["libms"]["label"]['msg']["labelInfoError"]?>");
				$('#info_order_6').focus();
				return false;
			}
			
		}
		else if(!($('#info_order_1').val() == '' && $('#info_order_2').val() == '' && $('#info_order_3').val() == '' && $('#info_order_4').val() == '' && $('#info_order_5').val() == '' && $('#info_order_6').val() == '')){
			alert( '<?=$Lang["libms"]["label"]["msg"]["labelInfoErrorFillAll"]?>' );
			$('#info_order_1').focus();
			return false;
		}
		return true;
	}
	//Henry added 20131031 [end]
		
		if (val_null || val_wrong)
		return false;
		
		return true;
}

function IsValidInput(meme) {

	var inputPat = /^(\d{1,5})(.(\d{1,2}))?$/;
	var matchArray = meme.val().match(inputPat);
	if (matchArray == null) {
		name = meme.attr("id");
		alert(" <?= $Lang['libms']['label']['input_msg'] ?> "+name);
		return false;
	}
	return true;
}


function update_unit(unit){
	var units= new Array();<?php 
		foreach ($Lang['libms']['label']['unit'] as $unit =>$disp_unit ){
			echo "units[\"{$unit}\"]= \"$disp_unit\";\n";
		}
	?>
	$('.unit').html(" ( "+units[unit]+" )");
}


$(function(){
	update_unit('mm');
	$('#select_metric').change(function(){
		update_unit($(this).val());
	});
	
});
</script>



<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form name="form1" id="form1" method="post" action="label_format_new_update.php" onSubmit="return checkForm(this)">

<table width="90%" align="center" border="0">
<tr><td>

	<table class="form_table_v30">
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['name']?></td>
			<td><input id="<?=$Lang['libms']['label']['name']?>" name="name" type="text" class="textboxtext" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['NX']?> ( <?=$Lang['libms']['label']['piece']?> )</td>
			<td><input id="<?=$Lang['libms']['label']['NX']?>" name="NX" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['NY']?> ( <?=$Lang['libms']['label']['piece']?> )</td>
			<td><input id="<?=$Lang['libms']['label']['NY']?>" name="NY" type="text" class="textboxnum number" value=""></td>
		</tr>
				<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['font_size']?> (pt)</td>
			<td><input id="<?=$Lang['libms']['label']['font_size']?>" name="font_size" type="text" class="textboxnum number" value=""></td>
		</tr>

		<tr valign="top">
			<td class='field_title'><?=$Lang["libms"]["label"]["is_bold"].' '.$Lang["libms"]["label"]["is_bold_remark"]?></td>
			<td><label><input id="<?=$Lang["libms"]["label"]["is_bold"]?>" name="is_bold" type="checkbox" value="1"><?=$Lang['General']['Yes']?></label></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['text_alignment']?></td>
			<td><?=$linterface->GET_SELECTION_BOX($text_alignments, " id ='text_alignment' name='text_alignment' class='textboxnum number' ", '', 'C');?></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['metric']?> <label class='unit'></label></td>
			<td><?=$linterface->GET_SELECTION_BOX($metric_unit, " id ='select_metric' name='metric' class='textboxnum number' ", '', 'mm');?></td>
		</tr>
				
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['paper-size-width']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['libms']['label']['paper-size-width']?>" name="paper_size_width" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['paper-size-height']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['libms']['label']['paper-size-height']?>" name="paper_size_height" type="text" class="textboxnum number" value=""></td>
		</tr>


		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['SpaceX']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['libms']['label']['SpaceX']?>" name="SpaceX" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['SpaceY']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['libms']['label']['SpaceY']?>" name="SpaceY" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['width']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['libms']['label']['width']?>" name="width" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['height']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['libms']['label']['height']?>" name="height" type="text" class="textboxnum number" value=""></td>
		</tr>
			
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['printing_margin_h']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['libms']['label']['printing_margin_h']?>" name="printing_margin_h" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['printing_margin_v']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['libms']['label']['printing_margin_v']?>" name="printing_margin_v" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['max_barcode_width']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['libms']['label']['max_barcode_width']?>" name="max_barcode_width" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['max_barcode_height']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['libms']['label']['max_barcode_height']?>" name="max_barcode_height" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['lMargin']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['libms']['label']['lMargin']?>" name="lMargin" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['tMargin']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['libms']['label']['tMargin']?>" name="tMargin" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['label']['lineHeight']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['libms']['label']['lineHeight']?>" name="lineHeight" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["libms"]["label"]["info_order"]?></td>
			<td><?=$labelInfoOrderSelections?></td>
		</tr>
		
	</table>
	
	<?=$linterface->MandatoryField();?>
	
	<div class="edit_bottom_v30">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='label_format.php'")?>
	</div>	

</td></tr>
</table>

</form><?php
echo $linterface->FOCUS_ON_LOAD("form1.ResponsibilityCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
