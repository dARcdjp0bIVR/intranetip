<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");


intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$sql = "SELECT 
		b.ISBN, CONCAT(b.CallNum, ' ', b.CallNum2) As CallNumberMixed,
		b.BookTitle,
		b.ResponsibilityBy1, b.Publisher, b.Edition
		
		FROM
			LIBMS_BOOK AS b, LIBMS_BOOK_TAG AS bt
		WHERE
			bt.BookID=b.BookID AND bt.TagID='{$TagID}'	
		Order By
			b.BookTitle
		";
//debug($sql);
$BookRecords = $libms->returnResultSet($sql);
//debug_r($BookRecords);

intranet_closedb();



?>


<div class="table_board">
	<h1>
		<?=$section_title_used?>
	</h1>	
		<table class="common_table_list">
		<col  />
		<thead>

		<tr>
		  <th class="num_check">#</th>
		  <th><?=$Lang['libms']['book']['ISBN']?></th>
		  <th><?=$Lang['libms']['book']['call_number']?></th>
		  <th><?=$Lang['libms']['book']['title']?></th>
		  <th><?=$Lang['libms']['book']['responsibility_by1']?></th>
		  <th><?=$Lang['libms']['book']['publisher']?></th>
		  <th><?=$Lang['libms']['book']['edition']?></th>
		</tr>
		
		<?php
		
			for ($i=0; $i<sizeof($BookRecords); $i++)
			{
				$book = $BookRecords[$i];
					
		?>
					<tr >
						<td><?=($i+1)?></td>
						<td><?=$book['ISBN']?>&nbsp;</td>
						<td><?=$book['CallNumberMixed']?>&nbsp;</td>
						<td><?=$book['BookTitle']?></td>
						<td><?=$book['ResponsibilityBy1']?>&nbsp;</td>
						<td><?=$book['Publisher']?>&nbsp;</td> 
						<td><?=$book['Edition']?>&nbsp;</td>
					</tr>
			<? 
			}
			?>
		
		</thead>

		</tbody>
		</table>
	</div>