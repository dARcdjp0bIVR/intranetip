<?php
/*
 * 	Log
 * 
 * 	@purpose: edit book recommendation	
 * 
 * 	2017-02-09 [Cameron]
 * 		- fix bug to support php5.4: should call stripslashes() because lib.php already add slashes to variables, but it call pack_value()
 * 			when addOrEditRecommend
 * 
 * 	2016-09-20 [Cameron] create this file
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	header("Location: batch_recommend_book_new.php");	
}

$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
$result = array();

if (!get_magic_quotes_gpc()) {
	$content = stripslashes($content);	
}

$result[] = $lelibplus->addOrEditRecommend($class_level_ids, $content, '', $BookID);

$xmsg = in_array(false,$result) ? 'UpdateUnsuccess': 'UpdateSuccess';

intranet_closedb();

header("Location: batch_recommend_book_list.php?xmsg=".$xmsg);

?>
