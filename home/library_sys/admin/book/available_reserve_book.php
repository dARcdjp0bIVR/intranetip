<?php

# being modified by:  
/*
 * Change log:
 *
 * 2020-03-25 [Cameron]
 *      - fix character decoding problem for ClassName and ClassNumber [case #H182598]
 *      
 * 2019-03-28 [Cameron]
 *      - show BookSubTitle if it's not empty
 *  
 * 2018-09-05 Cameron [case #E135900]
 *      - create this file
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
	function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
	$gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
	array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/reports/Exporter.php");
include_once($PATH_WRT_ROOT."home/library_sys/admin/book/reserve_book_manage.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

### set cookies
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	$order = 1;		# default in ascending order
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

intranet_auth();
intranet_opendb();

# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagementReserveList";

$TAGS_OBJ = $libms->getReserveBookTabs('AVAILABLE');

$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$libms->MODULE_AUTHENTICATION($CurrentPage);


$linterface = new interface_html();
$rbm = new reserve_book_manage();

############################################################################################################
///*
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == '') ? 1 : $order;
$field = ($field == "") ? 0 : $field;

$conds='';
$conds .= $rbm->getKeywordFilter();

//$userName = getNameFieldWithClassNumberByLangLIBMGMT('u.', $displayBarCode = false);
$classNameClassNumber = $junior_mck ? "CONVERT(CONCAT(u.ClassName,'-', u.ClassNumber) USING utf8)" : "CONCAT(u.ClassName,'-', u.ClassNumber)";

$sql = <<<EOL
SELECT  
        	IF (u.ClassName IS NULL OR u.ClassName='' OR u.ClassNumber IS NULL OR u.ClassNumber='', '-', {$classNameClassNumber}) AS ClassInfo,
        	CASE 
        		WHEN u.RecordStatus = 3  THEN CONCAT('<span class=\"tabletextrequire\">*</span>', u.{$Lang['libms']['SQL']['UserNameFeild']}) 
        		ELSE u.{$Lang['libms']['SQL']['UserNameFeild']}
        	END  AS UserName,
            TRIM(CONCAT(IFNULL(b.CallNum,''),' ', IFNULL(b.CallNum2,''))) AS CallNumber ,
    		CONCAT(b.`BookTitle`, IF(BookSubTitle IS NULL OR BookSubTitle='','',CONCAT('-',BookSubTitle))) AS BookTitle,
    		bu.BarCode,
            rl.ReserveTime,
            IF(rl.ExpiryDate='0000-00-00', '-', rl.ExpiryDate) AS ExpiryDate
FROM        LIBMS_RESERVATION_LOG rl
INNER JOIN  LIBMS_BOOK b ON b.BookID=rl.BookID
INNER JOIN  LIBMS_BOOK_UNIQUE bu ON bu.BookID=rl.BookID AND bu.UniqueID=rl.BookUniqueID 	
INNER JOIN  LIBMS_USER u ON u.UserID = rl.UserID
WHERE 
            rl.RecordStatus='READY'
{$conds}
EOL;

if (( $_POST['task'] == 'print') || ( $_POST['task'] == 'export')) {	
	if ( $_POST['task'] == 'print'){
		$viewdata = $rbm->getAvailableReservation($withStyle=true);
		echo $rbm->getPrint($viewdata, $type="readyToPickup");
		exit();
	}elseif ( $_POST['task'] == 'export'){
	    $viewdata = $rbm->getAvailableReservation($withStyle=false);
		Exporter::string_to_file_download('AvailableReserveBook.csv', $rbm->getAvailableCSV($viewdata));
		exit();
	}
}
//debug_pr($sql);

$li = new libdbtable2007($field, $order, $pageNo);

global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";

$li->field_array = array( "ClassInfo", "UserName", "CallNumber", "BookTitle", "BarCode", "ReserveTime", "ExpiryDate");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0,0,0,0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['class'])."</td>\n";
$li->column_list .= "<th width='18%' >".$li->column($pos++, $Lang['libms']['book']['user_name'])."</td>\n";
$li->column_list .= "<th width='13%' >".$li->column($pos++, $Lang['libms']['book']['call_number'])."</td>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['libms']['book']['title'])."</td>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang["libms"]["book"]["barcode"])."</td>\n";
$li->column_list .= "<th width='13%' >".$li->column($pos++, $Lang['libms']['book']['reserve_time'])."</td>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang['libms']['CirculationManagement']['msg']['expiry_date'])."</td>\n";

$keyword = htmlspecialchars($_POST['keyword']);
if (!get_magic_quotes_gpc()) {
	$keyword 	= stripslashes($keyword);
}
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"{$keyword}\" onkeyup=\"Check_Go_Search(event);\" />";
//*/
############################################################################################################

# Top menu highlight setting
$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function checkRemove(obj,element,page)
{
	var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
    if(countChecked(obj,element)==0)
        alert(globalAlertMsg2);
    else{
        if(confirm(alertConfirmRemove)){
        obj.action=page;
        obj.method="post";
        obj.submit();
        }
    }
}

function checkForm() 
{
	return;
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) {	// enter 
		document.form1.task.value="";
		document.form1.submit();
	}
	else {
		return false;
	}
}

function click_export()
{
	document.form1.task.value="export";
	document.form1.action = "available_reserve_book.php";
	document.form1.submit();	
	document.form1.task.value="";
}

function click_print()
{
	var url = "available_reserve_book.php";
	document.form1.task.value="print";
	document.form1.action=url;
	document.form1.target="_blank";
	document.form1.submit();
	document.form1.task.value="";
	document.form1.target="_self";
}

//-->
</script>
<form name="form1" id="form1" method="POST" action="available_reserve_book.php" onSubmit="return checkForm()">

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="40%">
												<?=getToolbar()?>
											</td>
											<td width="30%" align="center"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
											<td width="30%" align="right">
												<div class="content_top_tool">
													<div class="Conntent_search"><?=$searchTag?></div>     
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				<tr><td><br/><?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?></td></tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
				<input type="hidden" name="task" value="<?=$_POST['task']?>" />
			</td>
		</tr>
	</table>
</form>

<?php
//dump($sql);
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
