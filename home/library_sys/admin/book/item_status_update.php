<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
//ini_set('display_errors', 1);

# using: Henry

############ Change Log Start ###############
#
# 20170711 (Henry) : disallow change the status of write-off book [Case#Z120277]
# 20140926 (Henry) : added to call function SYNC_PHYSICAL_BOOK_TO_ELIB()
# 20140912 (Henry) : added to call function UPDATE_BOOK_STATUS_IF_RESERVED() if the changing the book status to 'NORMAL'
# 20131112 (Henry) : created this file
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_periodical_item.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
############################################################################################################

## Get Data
$UniqueID = (isset($UniqueID) && count($UniqueID) > 0) ? $UniqueID : array();


## Use Library
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

//$BookInfoAry = $libms->GET_BOOK_INFO($BookID);
//$IsPeriodical = $BookInfoAry[0]['IsPeriodical']; 
//$PeriodicalBookID = $BookInfoAry[0]['PeriodicalBookID'];
//$PeriodicalItemID = $BookInfoAry[0]['PeriodicalItemID'];


# Access Checking
if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($UniqueID)==0) {
	header("Location: index.php");	
}


## Main
//$result['remove_book_item'] = $libms->REMOVE_BOOK_ITEM($UniqueID);
//debug_pr($BookStatusUpdate);
$libel = new elibrary();

$xmsg = 'UpdateUnsuccess';
$hasWriteOffItem = false;
$hasNormalItem = false;

if(isset($BookStatusUpdate)){
	foreach($UniqueID as $aUniqueID){
		$tempArr = $libms->GET_BOOK_ITEM_INFO($aUniqueID);
		if($tempArr[0]["RecordStatus"] == 'WRITEOFF'){
			$hasWriteOffItem = true;
			continue;
		}
		$libms->UPDATE_UNIQUE_BOOK($aUniqueID, $BookStatusUpdate);
		$libms->UPDATE_BOOK_COPY_INFO($tempArr[0]["BookID"]);
		$libel->SYNC_PHYSICAL_BOOK_TO_ELIB($tempArr[0]["BookID"]);
		if($BookStatusUpdate == 'NORMAL')
			$libms->UPDATE_BOOK_STATUS_IF_RESERVED($aUniqueID, $BookStatusUpdate);
		$hasNormalItem = true;
	}
}

if($hasNormalItem && $hasWriteOffItem){
	$xmsg = 'UpdateSuccessWithWriteOffItem';
}
else if($hasNormalItem){
	$xmsg = 'UpdateSuccess';
}
else if($hasWriteOffItem){
	$xmsg = 'ItemStatusUpdateUnsuccess';
}
//if ($IsPeriodical && $PeriodicalBookID > 0 && $PeriodicalItemID > 0) {
//	$objItem = new liblms_periodical_item($PeriodicalItemID);
//	$objItem->setQuantity($objItem->getQuantity() - sizeof($UniqueID));
//	$itemId = $objItem->save();
//	
//	$result['update_periodical_item_quantity'] = ($itemId > 0)? true : false; 
//}


############################################################################################################
intranet_closedb();
header("Location: book_item_list.php?BookID=".$BookID."&FromPage=".$FromPage."&xmsg=".$xmsg);



?>