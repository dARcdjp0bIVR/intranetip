<?php
/*
 * 	modifying: 
 * 	
 * 	Log
 * 	Purpose:	common functions for reserve book
 * 
 *  Date:   2020-03-25 [Cameron]
 *          - fix character decoding problem for ClassName and ClassNumber in getAvailableReservation() and getCurrentReservation() [case #H182598]
 *          
 *  Date:   2019-03-28 [Cameron]
 *          - show BookSubTitle if it's not empty in getCurrentReservation(), getAvailableReservation()
 *  
 *  Date:   2018-09-10 [Cameron]
 *          - add function getAvailableReservation(), getReadyRecordList(), getAvailableCSV()
 *          - add parameter $type to getResult(), getPrint()
 *          
 *  Date:	2017-04-06 [Cameron]
 * 			modify getKeywordFilter() to support special characters like '"&<>\/ in php 5.4
 * 
 * 	Date:	2016-02-25 [Cameron]
 * 			fix bug on showing double quote when export data in getCSV()
 * 
 * 	Date:	2015-01-05 [Cameron] Print and export function: 
 * 				1. Fix bug to show all reservation of a book, but not one reservation only. (in getCurrentReservation)
 * 				2. Also show show status column
 * 
 * 	Date:	2014-12-22 [Cameron] change $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] to $Lang['libms']['bookmanagement']['deleted_user_legend'] to avoid char conversion
 * 
 * 	Date:	2014-12-18 [Cameron] return all records by search criteria instead of current page record only in getCurrentReservation()
 * 
 *  Date:	2014-12-11 [Cameron] add trim() function to getKeywordFilter()
 * 
 * 	Date:	2014-12-10 [Cameron] 
 * 				1. set $PATH_WRT_ROOT and $intranet_session_language in function getPrint() to fix path problem of .js in print_header.php
 * 				2. add function getKeywordFilter()
 * 				3. add Search by ACNO and BarCode in getKeywordFilter()
 * 				4. fix bug in getCSV() when there's no record
 * 	Date:	2014-11-21 [Cameron] create this file
 * 
 */


class reserve_book_manage extends liblms {
	
	public function getKeywordFilter() {
		$conds = '';
		$keyword = trim($_POST['keyword']);
		if (!get_magic_quotes_gpc()) {
			$keyword = stripslashes($keyword);
		}
		
		if($keyword!="") {
			$unconverted_keyword_sql = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));
			$converted_keyword_sql = special_sql_str($keyword);
			if ($unconverted_keyword_sql == $converted_keyword_sql){
				$conds = " AND (
				CONCAT(b.`CallNum`,' ', b.`CallNum2`) LIKE '%$unconverted_keyword_sql%' OR
				b.`ISBN` LIKE '%$unconverted_keyword_sql%' OR
				b.`BookTitle` LIKE '%$unconverted_keyword_sql%' OR
				b.`ResponsibilityBy1` LIKE '%$unconverted_keyword_sql%' OR
				bu.`ACNO` LIKE '%$unconverted_keyword_sql%' OR
				bu.`BarCode` LIKE '%$unconverted_keyword_sql%'
				)";
			}
			else {
				$conds = " AND (
				CONCAT(b.`CallNum`,' ', b.`CallNum2`) LIKE '%$unconverted_keyword_sql%' OR
				b.`ISBN` LIKE '%$unconverted_keyword_sql%' OR
				b.`BookTitle` LIKE '%$unconverted_keyword_sql%' OR
				b.`ResponsibilityBy1` LIKE '%$unconverted_keyword_sql%' OR
				bu.`ACNO` LIKE '%$unconverted_keyword_sql%' OR
				bu.`BarCode` LIKE '%$unconverted_keyword_sql%' OR
				CONCAT(b.`CallNum`,' ', b.`CallNum2`) LIKE '%$converted_keyword_sql%' OR
				b.`ISBN` LIKE '%$converted_keyword_sql%' OR
				b.`BookTitle` LIKE '%$converted_keyword_sql%' OR
				b.`ResponsibilityBy1` LIKE '%$converted_keyword_sql%' OR
				bu.`ACNO` LIKE '%$converted_keyword_sql%' OR
				bu.`BarCode` LIKE '%$converted_keyword_sql%'
				)";
			}
		}
		return $conds;
	}
	
	public function getCurrentReservation($withStyle=false) {
	    global $intranet_session_language,$page_size,$Lang,$junior_mck;
		$conds='';
		$conds .= $this->getKeywordFilter();
		if ($withStyle) {
			$UserName = "CASE 
							WHEN u.RecordStatus = 3  THEN CONCAT('<span class=\"tabletextrequire\">*</span>', u.{$Lang['libms']['SQL']['UserNameFeild']}) 
							ELSE u.{$Lang['libms']['SQL']['UserNameFeild']}
						 END  AS UserName";
			$space = "&nbsp;";
		}
		else {
			$UserName = "CASE 
							WHEN u.RecordStatus = 3  THEN CONCAT('*', u.{$Lang['libms']['SQL']['UserNameFeild']}) 
							ELSE u.{$Lang['libms']['SQL']['UserNameFeild']}
						 END  AS UserName";
			$space = " ";
		} 
	
		$classNameClassNumber = $junior_mck ? "CONVERT(CONCAT(u.ClassName,'-', u.ClassNumber) USING utf8)" : "CONCAT(u.ClassName,'-', u.ClassNumber)";
		
		$sql = "
		SELECT  CONCAT(b.`CallNum`,' ', b.`CallNum2`) AS CallNumber,
				b.`BookTitle`,
                CONCAT(b.`BookTitle`, IF(BookSubTitle IS NULL OR BookSubTitle='','',CONCAT('-',BookSubTitle))) AS BookTitle, 
				b.`ResponsibilityBy1`, 
				b.`Publisher`, 
				b.`Edition`,
				$classNameClassNumber AS Class,
				$UserName,								
				rl.ReserveTime,
				if (rl.RecordStatus='WAITING', '".$Lang["libms"]["book_reserved_status"]["WAITING"]."', 
					if(rl.RecordStatus='READY', CONCAT('".$Lang["libms"]["book_reserved_status"]["READY"]."', 
						if(rl.ExpiryDate<>'' AND rl.ExpiryDate<>'0000-00-00' AND rl.ExpiryDate IS NOT NULL, 
							CONCAT(' (',CAST(rl.ExpiryDate AS CHAR), ') '), '') ), '".$space."')) AS StateAndExpiry  
        FROM  `LIBMS_RESERVATION_LOG` rl
		INNER JOIN  `LIBMS_BOOK` b ON b.`BookID` = rl.`BookID` 	
		INNER JOIN   LIBMS_USER u ON u.UserID = rl.UserID
		LEFT JOIN `LIBMS_BOOK_UNIQUE` bu ON bu.`BookID` = b.`BookID`
		WHERE (
			rl.`RecordStatus` LIKE  'WAITING'
			OR rl.`RecordStatus` LIKE  'READY'
			)		
		{$conds}
		GROUP BY rl.`ReservationID`";

//		$rs = $this->returnArray($sql);
		## Should return all records by search criteria instead of current page record only
//		$total_row = count($rs);
//		
//		$pageSize = $_POST['numPerPage'];
//		
//		$max_page = ($pageSize>0) ? ceil($total_row/$pageSize) : $page_size;
//		$pageNo = $_POST['pageNo'];
//		 
//        if ($pageNo > $max_page && $max_page != 0)
//        {
//            $pageNo = $max_page;
//        }
//        $start=($pageNo-1)*$pageSize;
        $orderBy = IntegerSafe($_POST['field']); 
        if ($orderBy > 5) {
        	$sql .= " ORDER BY rl.ReserveTime ";        	
        }
        else {
        	$orderBy++;
        	$sql .= " ORDER BY $orderBy ";
        }
        $sql .= ($_POST['order']) ? 'ASC' : 'DESC';
//        $sql .= " LIMIT $start, $pageSize";
		$rs = $this->returnArray($sql);

		if (count($rs) > 0) {
			foreach($rs as $h=>$r) {
				foreach ($r as $k=>$v)
				if (!trim($v)) {
					$rs[$h][$k] = '--';
				}			
			}
		}
		return $rs;
	}
	
	public function getRecordList($viewdata){
		global $Lang;

		$x = "<table class='common_table_list_v30  view_table_list_v30' width='100%'>";
		$x .= "<tr>";
		$x .= "<th>#</th>";
		$x .= "<th>" . $Lang['libms']['book']['call_number']."</th>";
		$x .= "<th>" . $Lang['libms']['book']['title']."</th>";
		$x .= "<th>" . $Lang['libms']['book']['responsibility_by1']."</th>";
		$x .= "<th>" . $Lang['libms']['book']['publisher']."</th>";
		$x .= "<th>" . $Lang['libms']['book']['edition']."</th>";
		$x .= "<th>" . $Lang['libms']['book']['class']."</th>";
		$x .= "<th>" . $Lang['libms']['book']['user_name']."</th>";			
		$x .= "<th>" . $Lang['libms']['book']['reserve_time']."</th>";
		$x .= "<th>" . $Lang['libms']['CirculationManagement']['status']." (".$Lang["libms"]["CirculationManagement"]["valid_date"].")</th>";		
		$x .= "</tr>";
		
		$no_index = 0;
		foreach((array)$viewdata as $items)
		{
			$no_index ++;
			$x .= "<tr>";
			$x .= "<td>".$no_index.".</td>";
			$x .= "<td>".$items['CallNumber']."</td>";
			$x .= "<td>".$items['BookTitle']."</td>";
			$x .= "<td>".$items['ResponsibilityBy1']."</td>";
			$x .= "<td>".$items['Publisher']."</td>";
			$x .= "<td>".$items['Edition']."</td>";
			$x .= "<td>".$items['Class']."</td>";
			$x .= "<td>".$items['UserName']."</td>";				
			$x .= "<td>".$items['ReserveTime']."</td>";
			$x .= "<td>".$items['StateAndExpiry']."</td>";
			$x .= "</tr>";
		}
			
							
		$x .= "</table><br>";
		$x .= $Lang['libms']['bookmanagement']['deleted_user_legend'];
		
	 return $x;
	}
	
	public function getResult($viewdata, $type=''){
		global $Lang, $junior_mck;
		ob_start();
		echo '<div id="result_body" style="margin: auto; width: 98%;" >';
		
		
		if ($type == 'readyToPickup') {
		    $title = $Lang['libms']['bookmanagement']['available_reserve'];
		}
		else {
		    $title = $Lang['libms']['bookmanagement']['book_reserve'];
		}
		
		if ( $_POST['task'] == 'print')
		{	
			//Added school name at top of the page
			$schoolName =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
			echo '<p>'.$schoolName.'</p><center><h1>'.$title.'</h1></center>';
		
			// Print filter period info
			echo '<table border="0" width="100%"><tr>';
			echo '<td align="right">';
			echo $Lang["libms"]["report"]["printdate"] . " ".date("Y-m-d").'</td></tr></table>';
		}
		
		echo '<div id="result_stat" style="margin: auto" >';

			if (!(empty($_POST)) && (empty($viewdata))){
				echo $Lang["libms"]["report"]["norecord"];
			}elseif (!(empty($_POST)) && !(empty($viewdata))){
			    if ($type == 'readyToPickup') {
			        echo $this->getReadyRecordList($viewdata);
			    }
			    else {
				    echo $this->getRecordList($viewdata);
			    }
			}
			
		echo '</div>';
		echo '</div>';				
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getPrint($viewdata, $type='') {
		global $Lang,$LAYOUT_SKIN,$intranet_session_language;
		ob_start();
		$PATH_WRT_ROOT = "../../../../";
		include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
		?>
		
		<div id='toolbar' class= 'print_hide' style='text-align: right; width: 100%'>
		<? 
			$linterface = new interface_html();
			echo $linterface->GET_SMALL_BTN($Lang["libms"]["report"]["toolbar"]['Btn']['Print'], "button", "javascript:window.print();","print");
			
		?>
		</div>
				
		<?=$this->getResult($viewdata, $type);?>
		</body>
		</html>
		<?
		
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}
	
	public function getCSV($viewdata) {
		global $Lang;
		$PATH_WRT_ROOT = "../../../../";
		include_once($PATH_WRT_ROOT."home/library_sys/reports/Exporter.php");
		
		$headers[] = $Lang['libms']['book']['call_number'];
		$headers[] = $Lang['libms']['book']['title'];
		$headers[] = $Lang['libms']['book']['responsibility_by1'];
		$headers[] = $Lang['libms']['book']['publisher'];
		$headers[] = $Lang['libms']['book']['edition'];
		$headers[] = $Lang['libms']['book']['class'];
		$headers[] = $Lang['libms']['book']['user_name'];			
		$headers[] = $Lang['libms']['book']['reserve_time'];
		$headers[] = $Lang['libms']['CirculationManagement']['status']." (".$Lang["libms"]["CirculationManagement"]["valid_date"].")";
		
		$formatted_ary[] = $headers;
		
		if (count($viewdata) > 0) {
			foreach((array)$viewdata as $row){
				$formatted_ary[] = array(
						html_entity_decode($row['CallNumber'], ENT_QUOTES, 'UTF-8'),
						html_entity_decode($row['BookTitle'], ENT_QUOTES, 'UTF-8'),
						html_entity_decode($row['ResponsibilityBy1'], ENT_QUOTES, 'UTF-8'),
						html_entity_decode($row['Publisher'], ENT_QUOTES, 'UTF-8'),
						html_entity_decode($row['Edition'], ENT_QUOTES, 'UTF-8'),
						html_entity_decode($row['Class'], ENT_QUOTES, 'UTF-8'),
						html_entity_decode($row['UserName'], ENT_QUOTES, 'UTF-8'),
						$row['ReserveTime'],
						$row['StateAndExpiry']
				);
			}
		}
		else {
			$formatted_ary[] = array($Lang["libms"]["report"]["norecord"]);
		}
		$formatted_ary[] = array(strip_tags($Lang['libms']['bookmanagement']['deleted_user_legend']));

		$csv = Exporter::array_to_CSV($formatted_ary);

		return $csv;
	}
	
	
	public function getAvailableReservation($withStyle=false) 
	{
	    global $intranet_session_language,$page_size,$Lang,$junior_mck;
	    $conds = '';
	    $conds .= $this->getKeywordFilter();
	    if ($withStyle) {
	        $UserName = "CASE
	        WHEN u.RecordStatus = 3  THEN CONCAT('<span class=\"tabletextrequire\">*</span>', u.{$Lang['libms']['SQL']['UserNameFeild']})
	        ELSE u.{$Lang['libms']['SQL']['UserNameFeild']}
	        END  AS UserName";
	    }
	    else {
	        $UserName = "CASE
	        WHEN u.RecordStatus = 3  THEN CONCAT('*', u.{$Lang['libms']['SQL']['UserNameFeild']})
	        ELSE u.{$Lang['libms']['SQL']['UserNameFeild']}
	        END  AS UserName";
	    }
	    
	    $classNameClassNumber = $junior_mck ? "CONVERT(CONCAT(u.ClassName,'-', u.ClassNumber) USING utf8)" : "CONCAT(u.ClassName,'-', u.ClassNumber)";
	    $sql = "SELECT 
                        IF (u.ClassName IS NULL OR u.ClassName='' OR u.ClassNumber IS NULL OR u.ClassNumber='', '-', ".$classNameClassNumber.") AS ClassInfo,
                        {$UserName},
	                    TRIM(CONCAT(IFNULL(b.CallNum,''),' ', IFNULL(b.CallNum2,''))) AS CallNumber,
                	    CONCAT(b.`BookTitle`, IF(BookSubTitle IS NULL OR BookSubTitle='','',CONCAT('-',BookSubTitle))) AS BookTitle,
                	    bu.BarCode,
                        rl.ReserveTime,
                        IF(rl.ExpiryDate='0000-00-00', '-', rl.ExpiryDate) AS ExpiryDate
                FROM        
                        LIBMS_RESERVATION_LOG rl
                INNER JOIN  LIBMS_BOOK b ON b.BookID=rl.BookID
                INNER JOIN  LIBMS_BOOK_UNIQUE bu ON bu.BookID=rl.BookID AND bu.UniqueID=rl.BookUniqueID 	
                INNER JOIN  LIBMS_USER u ON u.UserID = rl.UserID
                WHERE 
                            rl.RecordStatus='READY'
                {$conds}";
							
		$orderBy = IntegerSafe($_POST['field']);
		if ($orderBy > 6) {
		    $sql .= " ORDER BY u.ClassName, u.ClassNumber, u.EnglishName ";
		}
		else {
		    $orderBy++;
		    $sql .= " ORDER BY $orderBy ";
		}
		$sql .= ($_POST['order']) ? 'ASC' : 'DESC';
		$rs = $this->returnResultSet($sql);
		
		if (count($rs) > 0) {
		    foreach($rs as $h=>$r) {
		        foreach ($r as $k=>$v) {
		            if (!trim($v)) {
		                $rs[$h][$k] = '-';
		            }
		        }
		    }
		}
		return $rs;
	}
	
	public function getReadyRecordList($viewdata)
	{
	    global $Lang;
	    
	    $x = "<table class='common_table_list_v30  view_table_list_v30' width='100%'>";
	    $x .= "<tr>";
	    $x .= "<th>#</th>";
	    $x .= "<th>" . $Lang['libms']['book']['class']."</th>";
	    $x .= "<th>" . $Lang['libms']['book']['user_name']."</th>";
	    $x .= "<th>" . $Lang['libms']['book']['call_number']."</th>";
	    $x .= "<th>" . $Lang['libms']['book']['title']."</th>";
	    $x .= "<th>" . $Lang['libms']['book']['barcode']."</th>";
	    $x .= "<th>" . $Lang['libms']['book']['reserve_time']."</th>";
	    $x .= "<th>" . $Lang['libms']['CirculationManagement']['msg']['expiry_date']."</th>";
	    $x .= "</tr>";
	    
	    $no_index = 0;
	    foreach((array)$viewdata as $items)
	    {
	        $no_index ++;
	        $x .= "<tr>";
	        $x .= "<td>".$no_index.".</td>";
	        $x .= "<td>".$items['ClassInfo']."</td>";
	        $x .= "<td>".$items['UserName']."</td>";
	        $x .= "<td>".$items['CallNumber']."</td>";
	        $x .= "<td>".$items['BookTitle']."</td>";
	        $x .= "<td>".$items['BarCode']."</td>";
	        $x .= "<td>".$items['ReserveTime']."</td>";
	        $x .= "<td>".$items['ExpiryDate']."</td>";
	        $x .= "</tr>";
	    }
	    
	    $x .= "</table><br>";
	    $x .= $Lang['libms']['bookmanagement']['deleted_user_legend'];
	    
	    return $x;
	}
	
	public function getAvailableCSV($viewdata) {
	    global $Lang;
	    $PATH_WRT_ROOT = "../../../../";
	    include_once($PATH_WRT_ROOT."home/library_sys/reports/Exporter.php");
	    
	    $headers[] = $Lang['libms']['book']['class'];
	    $headers[] = $Lang['libms']['book']['user_name'];
	    $headers[] = $Lang['libms']['book']['call_number'];
	    $headers[] = $Lang['libms']['book']['title'];
	    $headers[] = $Lang['libms']['book']['barcode'];
	    $headers[] = $Lang['libms']['book']['reserve_time'];
	    $headers[] = $Lang["libms"]["CirculationManagement"]["valid_date"];

	    $formatted_ary[] = $headers;
	    
	    if (count($viewdata) > 0) {
	        foreach((array)$viewdata as $row){
	            $formatted_ary[] = array(
	                html_entity_decode($row['ClassInfo'], ENT_QUOTES, 'UTF-8'),
	                html_entity_decode($row['UserName'], ENT_QUOTES, 'UTF-8'),
	                html_entity_decode($row['CallNumber'], ENT_QUOTES, 'UTF-8'),
	                html_entity_decode($row['BookTitle'], ENT_QUOTES, 'UTF-8'),
	                html_entity_decode($row['Barcode'], ENT_QUOTES, 'UTF-8'),
	                $row['ReserveTime'],
	                $row['ExpiryDate']
	            );
	        }
	    }
	    else {
	        $formatted_ary[] = array($Lang["libms"]["report"]["norecord"]);
	    }
	    $formatted_ary[] = array(strip_tags($Lang['libms']['bookmanagement']['deleted_user_legend']));
	    
	    $csv = Exporter::array_to_CSV($formatted_ary);
	    
	    return $csv;
	}
	
} // end class
?>