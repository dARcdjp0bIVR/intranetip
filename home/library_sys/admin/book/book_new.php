<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

//error_reporting(E_ERROR | E_WARNING | E_PARSE);
//ini_set('display_errors', 1);

# should open in utf-8 for editing
# using:  

############ Change Log Start ###############
#
#	Date: 2019-07-23 (Henry)
#	Details: use the local library of jquery
#
#	Date:	2017-11-01 [Cameron]
#	Details: add flag $sys_custom['eLibraryPlus']['CallNumAllowSpace'] to control whether allowing space in CallNum 
#
#	Date:	2017-05-19 [Cameron]
#	Details: fix bug on replacing new line / line feed character with space to eliminate js error in Taglist (apply newline2space) (case #Q117253)
#
#	Date:	2016-07-05 [Tiffany]
#	Details:  fix the Marc21 language and resource special charactor bugs			
#
#	Date:	2016-04-29 [Cameron]
#	Details: add autocomplete for following inputselect fields:
#		Country, Publisher, Publication place, Series, Responsibility by, Responsibility by 2, Responsibility by 3, Subject 			
#
#	Date:	2016-02-18 [Cameron]
#	Details: add inputselect class for author (BookResponb1 ~ BookResponb3)
#
#	Date:	2015-09-23 [Cameron]
#	Details: change maxlength from 4 to 16 chars for "Number of pages" field 
#
#	Date:	2015-08-04 (Cameron)
#	Detail:	 fix bug on checking if the argument is an array before apply implode function for Languages & Resources
#
#	Date:	2015-05-27 (Henry)
#	Details: enlarge the input size of BookTitle and BookSubtitle
#
#	Date:	2015-05-12 (Henry)
#	Details: enlarge the input size of ResponsibilityBy1, ResponsibilityBy2 and ResponsibilityBy3
#
#	Date:	2015-02-25 (Tiffany)
#	Details: Add Marc21 import function
#
#	Date:	2015-01-15 (Henry)
#	Details: apply the language settings and multi-type of book category
#
#	Date:	2014-09-10 (Henry)
#	Details: enlarge the input size of BookPublishyear
#
#	Date:	2013-09-12 (Ivan)
#	Details: added periodical-related logic
#
#	Date:	2013-08-12 (Jason)
#	Details: remove some info and move to item page such as ACNO, Purchase Info, Barcode, Book Location
#
#
############ Change Log End ###############

### set cookies
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

$home_header_no_EmulateIE7 = true;


intranet_auth();
intranet_opendb();

# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagement";

$libms->MODULE_AUTHENTICATION($CurrentPage);

$linterface = new interface_html("libms.html");

############################################################################################################


$TAGS_OBJ[] = array($Lang['libms']['action']['new_book']);

$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_list'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['new'], "");
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagementBookList";

$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$libms->MODULE_AUTHENTICATION($CurrentPage);
$linterface->LAYOUT_START();
$toolbar = $linterface->GET_LNK_IMPORT("javascript:importMarc21()",$Lang["libms"]["book"]["importMarc21"],"","","",0);

#Save and Cancel Button ...........
//$SaveBtn   = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "SubmitBtn", "");
$SaveBtn2  = $linterface->GET_ACTION_BTN($Lang['libms']['book']['btn']['SubmitAndAddItem'], "button", "submit2()", "SubmitBtn", "");
$CancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'", "CancelBtn", "");

# Responsibility ...........
$ResponsibilityCodeArrary = $libms->BOOK_GETOPTION('LIBMS_RESPONSIBILITY', 'ResponsibilityCode', $Lang['libms']['sql_field']['Description'], 'ResponsibilityCode', '');
$ResponsibilityCodeOption1 = $linterface->GET_SELECTION_BOX($ResponsibilityCodeArrary, " name='ResponsibilityCodeOption1' id='ResponsibilityCodeOption1' onChange='disable_enable_the_follow_input(1)' ", $Lang['libms']['status']['na'], $Status);
$ResponsibilityCodeOption2 = $linterface->GET_SELECTION_BOX($ResponsibilityCodeArrary, " name='ResponsibilityCodeOption2' id='ResponsibilityCodeOption2' onChange='disable_enable_the_follow_input(1)' ", $Lang['libms']['status']['na'], $Status);
$ResponsibilityCodeOption3 = $linterface->GET_SELECTION_BOX($ResponsibilityCodeArrary, " name='ResponsibilityCodeOption3' id='ResponsibilityCodeOption3' onChange='disable_enable_the_follow_input(1)' ", $Lang['libms']['status']['na'], $Status);

# Option ..........
$BookCategoryArray = $libms->BOOK_GETOPTION('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', "CONCAT(`BookCategoryCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookCategoryCode', 'BookCategoryType=1');
$BookCategoryOption = $linterface->GET_SELECTION_BOX($BookCategoryArray, ' name="BookCategory" id="BookCategory" ', $Lang['libms']['status']['na'], $Status);
//Henry Added
$BookCategoryArray2 = $libms->BOOK_GETOPTION('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', "CONCAT(`BookCategoryCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookCategoryCode', 'BookCategoryType=2');
$BookCategoryOption2 = $linterface->GET_SELECTION_BOX($BookCategoryArray2, ' name="BookCategory" id="BookCategory" ', $Lang['libms']['status']['na'], $Status);

$BookCirculationArray = $libms->BOOK_GETOPTION('LIBMS_CIRCULATION_TYPE', 'CirculationTypeCode', $Lang['libms']['sql_field']['Description'], 'CirculationTypeCode', '');
$BookCirculationOption = $linterface->GET_SELECTION_BOX($BookCirculationArray, " name='BookCirclation' id='BookCirclation' ", $Lang['libms']['status']['na'], $Status);

$BookResourcesArray = $libms->BOOK_GETOPTION('LIBMS_RESOURCES_TYPE', 'ResourcesTypeCode', $Lang['libms']['sql_field']['Description'], 'ResourcesTypeCode', '');
$BookResourcesOption = $linterface->GET_SELECTION_BOX($BookResourcesArray, " name='BookResources' id='BookResources' ", $Lang['libms']['status']['na'], $Status);

$BookLanguageArray = $libms->BOOK_GETOPTION('LIBMS_BOOK_LANGUAGE', 'BookLanguageCode', "CONCAT(`BookLanguageCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookLanguageCode', '');
$BookLanguageOption = $linterface->GET_SELECTION_BOX($BookLanguageArray, " name='BookLang' id='BookLang' ", $Lang['libms']['status']['na'], $Status);
$BookLanguageOption .= '<input type="hidden" id="hid_LanguageCategoryCode" value="1"/>';


for($i=0;$i<count($BookLanguageArray);$i++){
	$BookLanguageStringArr[$i] = $BookLanguageArray[$i][BookLanguageCode];
}
//$BookLanguageString = implode("'",is_array($BookLanguageStringArr) ? $BookLanguageStringArr:array());

for($i=0;$i<count($BookResourcesArray);$i++){
	$BookResourcesStringArr[$i] = $BookResourcesArray[$i][ResourcesTypeCode];
}
//$BookResourcesString = implode(",",is_array($BookResourcesStringArr)?$BookResourcesStringArr:array());

/*
$BookLocationArray = $libms->BOOK_GETOPTION('LIBMS_LOCATION', 'LocationCode', $Lang['libms']['sql_field']['Description'], 'LocationCode', '');
$BookLocationOption = $linterface->GET_SELECTION_BOX($BookLocationArray, " name='BookLocation' id='BookLocation' ", $Lang['libms']['status']['na'], $Status);

# Book Status ......
$y = 0;
foreach ($Lang['libms']['book_status'] as $key => $value)
{
	$BookStatus[$y][0] = $key;
	$BookStatus[$y][1] = $value;
	$y++;
}
$BookStatusOption = $linterface->GET_SELECTION_BOX($BookStatus, " name='BookStatus' id='BookStatus' ", '', 'NORMAL');
*/

# Book Tag ..........
$BookTagArray = $libms->SELECTVALUE('LIBMS_TAG', 'TagName', 'TagName');
foreach ($BookTagArray as $value) $Taglist .="'".addslashes(trim(newline2space($value[TagName])))."', ";
$Taglist = rtrim($Taglist,', ');

?>
<script src="/templates/jquery/jquery-1.6.1.min.js" type="text/javascript" charset="utf-8"></script>
<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">

<script src="./TextExt.js" type="text/javascript" charset="utf-8"></script>

<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
function importMarc21(){
	$('#importMarc21').show(); 
}

function importMarc21Close(){
	$('#importMarc21').hide();   
}

function ajax_getMarc21(){
	$.post(
			"ajax_decode_Marc21.php", 
			{ 
				textarea: $('#textarea_Marc21').val()
			},
			function(BookInfoArr)
			{   
				if(BookInfoArr.BookTitle!=undefined){
                    $('#BookTitle').val(BookInfoArr.BookTitle);
			    }
				if(BookInfoArr.BookSubtitle!=undefined){
                    $('#BookSubtitle').val(BookInfoArr.BookSubtitle);
			    }
				if(BookInfoArr.CallNum!=undefined){
                    $('#BookCallno').val(BookInfoArr.CallNum);
			    }
				if(BookInfoArr.CallNum2!=undefined){
                    $('#BookCallno2').val(BookInfoArr.CallNum2);
			    }
				if(BookInfoArr.ISBN!=undefined){
                    $('#BookISBN').val(BookInfoArr.ISBN);
			    }
				if(BookInfoArr.ISBN2!=undefined){
                    $('#BookISBN2').val(BookInfoArr.ISBN2);
			    }
			    if(BookInfoArr.Language!=undefined){
				    var BookLanguageArr = <?=json_encode($BookLanguageStringArr)?>;				    
				    var hasLanguage = $.inArray(BookInfoArr.Language, BookLanguageArr);
	                if(hasLanguage=="-1"){
	                	$('#BookLang').append("<option value='"+BookInfoArr.Language_Decode+"'>"+BookInfoArr.Language_Decode+" - "+BookInfoArr.Language_Decode+"</option>");
	                	$('#BookLang').val(BookInfoArr.Language_Decode); 
		            }
	                else{
	                	 $('#BookLang').val(BookInfoArr.Language_Decode);                         
			        }		                
			    }
				if(BookInfoArr.Country!=undefined){
                    $('#BookCountry').val(BookInfoArr.Country);
			    }
				if(BookInfoArr.Introduction!=undefined){
                    $('#BookIntro').val(BookInfoArr.Introduction);
			    }
				if(BookInfoArr.Edition!=undefined){
                    $('#BookEdition').val(BookInfoArr.Edition);
			    }
				if(BookInfoArr.PublishYear!=undefined){
                    $('#BookPublishyear').val(BookInfoArr.PublishYear);
			    }
				if(BookInfoArr.Publisher!=undefined){
                    $('#BookPublisher').val(BookInfoArr.Publisher);
			    }
				if(BookInfoArr.PublishPlace!=undefined){
                    $('#BookPublishplace').val(BookInfoArr.PublishPlace);
			    }
				if(BookInfoArr.PublishPlace!=undefined){
                    $('#BookPublishplace').val(BookInfoArr.PublishPlace);
			    }
				if(BookInfoArr.Series!=undefined){
                    $('#BookSeries').val(BookInfoArr.Series);
			    }
				if(BookInfoArr.SeriesNum!=undefined){
                    $('#BookSeriesno').val(BookInfoArr.SeriesNum);
			    }
				if(BookInfoArr.Responsibility1!=undefined){				
                    $('#ResponsibilityCodeOption1').val(BookInfoArr.Responsibility1);
			    }
				if(BookInfoArr.Responsibility2!=undefined){
                    $('#ResponsibilityCodeOption2').val(BookInfoArr.Responsibility2);
			    }
			    if(BookInfoArr.Responsibility3!=undefined){
                    $('#ResponsibilityCodeOption3').val(BookInfoArr.Responsibility3);
			    }
				if(BookInfoArr.ResponsibilityBy1!=undefined){
                    $('#BookResponb1').val(BookInfoArr.ResponsibilityBy1);
			    }
				if(BookInfoArr.ResponsibilityBy2!=undefined){
                    $('#BookResponb2').val(BookInfoArr.ResponsibilityBy2);
			    }				
			    if(BookInfoArr.ResponsibilityBy3!=undefined){
                    $('#BookResponb3').val(BookInfoArr.ResponsibilityBy3);
			    }
			    if(BookInfoArr.Dimension!=undefined){
                    $('#BookDimension').val(BookInfoArr.Dimension);
			    }
			    if(BookInfoArr.ILL!=undefined){
                    $('#BookILL').val(BookInfoArr.ILL);
			    }
			    if(BookInfoArr.NoOfPage!=undefined){
                    $('#BookNopage').val(BookInfoArr.NoOfPage);
			    }
			    if(BookInfoArr.BookCategoryCode!=undefined){
                    $('#BookCategory').val(BookInfoArr.BookCategoryCode);
			    }			    
			    //Circulation Type
			    //Resources Type : ResourcesTypeCode
			    if(BookInfoArr.ResourcesTypeCode!=undefined){
				    var BookResourcesArr = <?=json_encode($BookResourcesStringArr)?>;
				    var hasResources = $.inArray(BookInfoArr.ResourcesTypeCode, BookResourcesArr);
	                if(hasResources=="-1"){
	                	$('#BookResources').append("<option value='"+BookInfoArr.ResourcesTypeCode_Decode+"'>"+BookInfoArr.ResourcesTypeCode_Decode+"</option>");
	                	$('#BookResources').val(BookInfoArr.ResourcesTypeCode_Decode); 
		            }
	                else{
	                	 $('#BookResources').val(BookInfoArr.ResourcesTypeCode_Decode);                         
			        }		                
			    }
			    
			    
			    if(BookInfoArr.Subject!=undefined){
                    $('#BookSubj').val(BookInfoArr.Subject);
			    }
			    if(BookInfoArr.Tags!=undefined){
                  
                    var tagArr = BookInfoArr.Tags.split(",");
                    for(var i=0;i<tagArr.length;i++){
                        //去除重複的tags...
                    	$('#BookTag').textext()[0].tags().addTags([tagArr[i]]);                    
                    }
			    }
			}
			,"json"
	);

}
function checknumeric(){
	var checking = true;
	$('.numberic').each(function() {
		if (($(this).val() != "") && (isNaN(Number($(this).val())) || $(this).val() < 0)){
			var field = $(this).parent().prev().find('span').html();
			alert("<?=$Lang["libms"]["book"]["alert"]["numeric"]?> "+field);	
			$(this).focus(); 
			checking = false;
			return false;	
	}});
	return checking;
};


function checkForm(form1) {
	if(form1.BookTitle.value==''){
		alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['book']['title'] ?>");	
		form1.BookTitle.focus();
		return false;
//	} else if($('#BookCode').val()=='') {
//		alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['book']['code'] ?>");	
//		$('#BookCode').focus(); 
//		return false;
	} else if(!checknumeric()) {
		return false;
	} else if(!selfcheck || !unique_barcode_check || !unique_bookcode){
		alert("<?= $Lang['libms']['book']['code']."/".$Lang['libms']['book']['barcode'] ." incorrect" ?>");	
		return false;
	} else {
		return true;
	}
}

var count_available;
var fSubmit = false; 
var unique_bookcode = true;
var selfcheck = true;
var unique_barcode_check = true;
var timerobj;

/*
function jsValidateACNO()
{
	//var tmerObj = setInterval('$(\'#BookCode\').change()',1000);
	//clearInterval(tmerObj);
	
	timerobj = setTimeout(validateAction,1000);
}

function validateAction()
{
	$('#BookCode').change();
	clearTimeout(timerobj);
}

function barcode_change(mememe) {
	mememe.val(mememe.val().toUpperCase());
	count_no_of_book();
	count_availbale_book();
	//Unique barcode...................................................................
	mememe.parent('td').next('td').html('<img src="loader.gif" align="absmiddle"/>');
	
	
	if (mememe.val() != '' && mememe.val() !='***AUTO GEN***'){
		barcodeRespond = $.get('ajax_check_barcode.php', { barcode : mememe.val()},
			function(barcodeRespond){
				barcodeRespond = $.trim(barcodeRespond);
				if (barcodeRespond == '0'){
					unique_barcode_check = true;
				}else if (barcodeRespond == '1'){
					unique_barcode_check = false;
				}
				
			}
		);
	}
	//Count total no of book......................................................................
	
	
	$('.book_barcode').each(function(){

		//Selfcheck............................................................................................
		if(selfcheck){
			if (mememe.val() != '' && mememe.val() !='***AUTO GEN***'){
				if (!$(this).is(mememe)){
					if($(this).val() == mememe.val() )
					{
						selfcheck = false;

					}
				}
			}
		}
		//Selfcheck end ....................................................................................
	});

	//Show status box......................................................................
	if(mememe.val() != ''){
	mememe.parents('tr:first').find('.unqiue_bk_status').show();
	} else {
	mememe.parents('tr:first').find('.unqiue_bk_status').hide();
	}

	//toggle ajax check img
	if (!(selfcheck && unique_barcode_check))
	{
		mememe.parent('td').next('td').html('<img src="not_available.png" align="absmiddle"/>');
	}else if (mememe.val() == '' || mememe.val() =='***AUTO GEN***'){
		mememe.parent('td').next('td').html('');
	}else{
		mememe.parent('td').next('td').html('<img src="available.png" align="absmiddle"/>');
	}
	
}
*/

$().ready( function(){
	//disable enter as submit
	$('input').keydown(function(e) {
		if (event.which == 13) {
		event.preventDefault();
		return false;
		}
	});
	
	// autocomplete book tag
	$('#BookTag').textext({
			plugins : 'tags autocomplete'
	}).bind('getSuggestions', function(e, data){
		var list = [<?= $Taglist?>],
			textext = $(e.target).textext()[0],
			query = (data ? data.query : '') || ''
			;

		$(this).trigger(
			'setSuggestions',
			{ result : textext.itemManager().filter(list, query) }
		);
	});
	
	// autocomplete for inputselect fields
	$('.inputselect').each(function(){
		var this_id = $(this).attr('id');
		if($(this).length > 0){
			$(this).autocomplete(
		      "ajax_get_suggestions.php",
		      {
		  			delay:3,
		  			minChars:1,
		  			matchContains:1,
		  			extraParams: {'field':this_id},
		  			autoFill:false,
		  			overflow_y: 'auto',
		  			overflow_x: 'hidden',
		  			maxHeight: '200px'
		  		}
		    );
		}
	});
	
	
  /*
	// barcode field add and delete
	$('.btn_addRow').click(function () {
		add_row($(this));
	});
	
	
    $('.btn_deleteRow').click(function () {
        del_row($(this));
    });
	
	//Barcode input ...................................................
	$('.book_barcode').change(function(){
		barcode_change($(this));
	}); 
	

	//autocomplete bookcode....................................................
	$('#BookCode').textext({
            plugins : 'autocomplete ajax',
            ajax : {
                url : 'ajax_ACNO_suggestion.php',
                dataType : 'json',
                cacheResults : false
            }
    });
	
	//BookCode unique................................................
	
	$('#BookCode').change(function(){
		$('#BookCode').parent('td').next('td').html('<img src="loader.gif" align="absmiddle"/>');
		$.get('ajax_check_bookcode.php', { bookcode : $('#BookCode').val()},
				function(bookcodeRespond){
					if (bookcodeRespond == '0'){
						$('#BookCode').parents('tr:first').find('td#bookcode_error span.err_img').html('<img src="available.png" align="absmiddle"/>');
						unique_bookcode = true; 
					}else if (bookcodeRespond == '1'){
						$('#BookCode').parents('tr:first').find('td#bookcode_error span.err_img').html('<img src="not_available.png" align="absmiddle"/>');
						unique_bookcode = false;
					}
					
				}
		);
	});
		
	//Count total available no of book......................................................................
	$('.barcode_bookstatus').change( function(){
			count_availbale_book(); 
	});
	
	//Auto-gen Barcode
	$('.gen_barcode').change( function(){ autogen_on_click($(this)); } );

	$('.book_barcode').keypress(function(e) {
		return onKeyUp($(this),e);
	});
  */
  
	$("input.inputselect").inputselect({
		image_path: '/images/2009a/', 
		ajax_script: 'ajax_get_selection_list.php', 
		js_lang_alert: {'no_records' : '<?=$Lang['libms']['NoRecordAtThisMoment']?>' }
	});
	
	// Book Language onchange......................................
	$('#BookLang').change( function(){
		$.ajax({
	       url: "ajax_get_book_category_type.php",
	       type: "post",
	       data: { language_code: $('#BookLang').val()},
	       success: function(data){
	           //alert(data);
	           if(data == 2 && $('#hid_LanguageCategoryCode').val() != 2){
	           	$("#div_BookCategory").html('<?=preg_replace('/\s+/', ' ', $BookCategoryOption2)?>');
	           	$('#hid_LanguageCategoryCode').val('2');
	           }else if(data == 1 && $('#hid_LanguageCategoryCode').val() != 1){
	           	$("#div_BookCategory").html('<?=preg_replace('/\s+/', ' ', $BookCategoryOption)?>');
	           	$('#hid_LanguageCategoryCode').val('1');
	           }
	       },
	       error:function(){
	           //alert("failure");
	           //$("#result").html('There is error while submit');
	       }
	   });
	});
});

/*
function count_no_of_book(){
	var count = 0 ;
	$('.book_barcode').each(function(){
		//Count total no of book......................................................................
		if ($(this).val() != '')
		{ 
				count++; 
		}
	});
	$('#BookNocopy').val(count);
	$('#lbl_BookNocopy').html(count);
}
//new row added...................................................................
function add_row(jThis){
	<?
//		$selectionbox=$linterface->GET_SELECTION_BOX($BookStatus, ' name="barcode_bookstatus[]" style="display:none" class=" unqiue_bk_status barcode_bookstatus" ', '', 'NORMAL');
//		$selectionbox = str_replace("\n",'', $selectionbox);
	
	?>
	
		$('table#barcode > tbody:last').append('<tr><td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['barcode'] ?><label class="barcode_number" /></span></td><td> <?=$Lang['libms']['general']['auto']?>: <input name="GenBarcode[]" type="checkbox" class="gen_barcode" value=""/></td><td ><input name="BookBarcode[]" type="text" class="textboxtext book_barcode" maxlength="64" /></td><td class="icon">&nbsp;</td><td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['book_status'] ?></span></td><td ><?=$selectionbox?></td><td><input type="button" class="btn_addRow" value="<?= $Lang['libms']['book']['add'] ?>" /><input type="button" class="btn_deleteRow" value="<?=$Lang['libms']['book']['delete']?>" style="display:none"/></td></tr>');
		$("input.book_barcode:last").change(function(){barcode_change($(this));});
		$('input.gen_barcode:last').change(function(){autogen_on_click($(this));});
		$('input.book_barcode:last').keypress(function(e) { onKeyUp($(this),e); });
		$('input.btn_deleteRow:last').click(function () { del_row($(this)); });
		$('input.btn_addRow:last').click(function () { add_row($(this)); });
		parent_TR = jThis.parents('tr:first');
		parent_TR.find('input.btn_addRow').hide();
		parent_TR.find('input.btn_deleteRow').show();
		parent_TR.addClass('record');
		count_no_of_book();
		count_availbale_book();
		update_barcode_number();
}

function update_barcode_number(){
	counter =1;
	$('tr.record').each( function() {
		$(this).find('label.barcode_number').html(counter);
		counter++;
	});
}

function count_availbale_book(){
	count_available = 0 ;
	$('.barcode_bookstatus').each( function() {
		parent_tr=$(this).parents('tr:first');
		if(parent_tr.find('.book_barcode').val() != "")
		if ((($(this).val() == 'NORMAL') || ($(this).val() == 'SHELVING'))&&($(this).val() != ''))
			{ count_available++; }
		});
	$('#BookNoavailable').val(count_available);
	$('#lbl_BookNoavailable').html(count_available);
}
//Key up handler for barcode ................................
function onKeyUp(JQueryObj,event){
	if (event.which == 13) {
	    event.preventDefault();
	    next_barcode(JQueryObj);
	    return false;
	  }  
}


function next_barcode(THIS){
	parent_TR = THIS.parents('tr:first');
	NEXT_barcode = parent_TR.next('tr').find('.book_barcode');
	if (NEXT_barcode.length == 0) {
		add_row(THIS);
		parent_TR.next('tr').find('.book_barcode').focus();
	} else {
		NEXT_barcode.focus();
	}
}
function autogen_on_click(JQueryObj){
	THIS=JQueryObj;
	txtBarcode=THIS.parents('tr:first').find('.book_barcode');
	if (THIS.is(":checked"))
		txtBarcode.val("***AUTO GEN***").hide();
	else
		txtBarcode.val("").show();
	txtBarcode.trigger('change');
	next_barcode(THIS);
}

function del_row(JQueryObj){
	JQueryObj.parents('tr:first').remove();
	update_barcode_number();
	count_no_of_book();
	count_availbale_book();
}
*/

function submit2(){
	$('#setItem').val(1);
	var obj = document.form1;
	if(!checkForm(obj)){
		return false;
	}
	obj.submit();
}

function replaceSpace(txtInputObj)
{
	var txtValue=txtInputObj.value;
	var spacePosition = txtValue.search(" ");

	if (spacePosition==0)
	{
		txtInputObj.value = txtValue.trim()
	} else (spacePosition>0)
	{
		txtInputObj.value = txtValue.replace(" ", ".");
	}
}


</script>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form name="form1" method="POST" action="book_new_update.php"
	onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">
	<table width="90%" border="0" align="center">
		<tr>
			<td class="main_content">
			    <div class="content_top_tool"  style="float: left;">
		           <?=$toolbar?>
		        </div>
		        <br/>
		        <br/>
		        <div id="importMarc21" style="display:none">
		           <textarea id ="textarea_Marc21" rows="15" cols="100"></textarea><br/>
		           <input type="button" id = "import_button" onclick="javascript:ajax_getMarc21()" value="<?=$Lang["libms"]["CirculationManagement"]["confirm"]?>">
		           <input type="button" id = "close_button" onclick="javascript:importMarc21Close()" value="<?=$Lang["libms"]["report"]["toolbar"]['Btn']['Close']?>">
		        </div>
		        <br/>
				<div class="table_board">
					<p class="spacer"></p>
					<div class="form_sub_title_v30">
						<em>- <span class="field_title"><?= $Lang['libms']['book']['info'] ?>
						</span> -
						</em>
						<p class="spacer"></p>
					</div>
					<table class="form_table_v30">
						<tbody>
							<tr>
								<td width="15%" class="field_title_short"><span
									class="tabletextrequire">*</span> <?= $Lang['libms']['book']['title'] ?>
								</td>
								<td width="33%"><input name="BookTitle" id="BookTitle" type="text"
									class="textboxtext" maxlength="256" /></td>
								<td width="4%">&nbsp;</td>
								<td width="15%" class="field_title_short"><span
									class="field_title"><?= $Lang['libms']['book']['subtitle'] ?> </span>
								</td>
								<td width="33%"><input name="BookSubtitle" id="BookSubtitle" type="text"
									class="textboxtext" maxlength="256" /></td>
							</tr>
							<? /* # no longer use - move to item_edit.php
							<tr>
								<td class="field_title_short"><span class="tabletextrequire">*</span>
									<?= $Lang['libms']['book']['code'] ?></td>
								<td><input autocomplete="off" id="BookCode" name="BookCode"
									type="text" class="textboxtext" maxlength="16" onFocusOut="jsValidateACNO();" /></td>
								
								<td id='bookcode_error' colspan="3"><span class='err_img'></span><span class="tabletextremark"><?=$Lang["libms"]["book"]["code_format"]?></span></td>
								  <!--
								<td style='display:none' class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['book_status'] ?>
								</span></td>
								<td style='display:none'><?= $BookStatusOption ?></td>
								-->
							</tr>
							*/ ?>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['call_number'] ?> <br />(<?= $Lang['libms']['book']['call_number_desc'] ?>)
								</span></td>
								<td><input name="BookCallno" id="BookCallno" type="text" class="textboxtext"  maxlength="255" <?=$sys_custom['eLibraryPlus']['CallNumAllowSpace'] ? '' : 'onkeyup="replaceSpace(this)"'?> /></td>
								<td>&nbsp;</td>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['call_number2'] ?> <br />(<?= $Lang['libms']['book']['call_number2_desc'] ?>)
								</span></td>
								<td><input name="BookCallno2" id="BookCallno2" type="text" class="textboxtext"
									maxlength="255" /></td>
							</tr>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['ISBN'] ?>
								</span></td>
								<td><input name="BookISBN" id="BookISBN" type="text" class="textboxtext"
									maxlength="255" /></td>
								<td>&nbsp;</td>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['ISBN2'] ?>
								</span></td>
								<td><input name="BookISBN2" id="BookISBN2" type="text" class="textboxtext"
									maxlength="255" /></td>
							</tr>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['language'] ?>
								</span></td>
								<td><!--<input id="BookLang" name="BookLang" type="text" class="textboxtext inputselect"
									maxlength="32" />--><?=$BookLanguageOption?></td>
								<td>&nbsp;</td>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['country'] ?>
								</span></td>
								<td><input id="BookCountry" name="BookCountry" type="text" class="textboxtext inputselect"
									maxlength="128" /></td>
							</tr>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['introduction'] ?>
								</span></td>
								<td colspan="4"><textarea name="BookIntro" id="BookIntro" class="textboxtext"></textarea>
								</td>
							</tr>
						</tbody>
					</table>

					<p></p>
					<div class="form_sub_title_v30">
						<em>- <span class="field_title"><?= $Lang['libms']['book']['info_publish'] ?>
						</span> -
						</em>
						<p class="spacer"></p>
					</div>
					<table class="form_table_v30">
						<tbody>
							<tr>
								<td width="15%" class="field_title_short"><span
									class="field_title"><?= $Lang['libms']['book']['edition'] ?> </span>
								</td>
								<td width="33%"><input name="BookEdition" id="BookEdition" type="text"
									class="textboxtext" maxlength="32" /></td>
								<td width="4%">&nbsp;</td>
								<td width="15%" class="field_title_short"><span
									class="field_title"><?= $Lang['libms']['book']['publish_year'] ?>
								</span></td>
								<td width="33%"><input name="BookPublishyear" id="BookPublishyear" type="text"
									class="textboxnum" maxlength="32" /></td>
							</tr>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['publisher'] ?>
								</span></td>
								<td><input id="BookPublisher" name="BookPublisher" type="text" class="textboxtext inputselect"
									maxlength="128" /></td>
								<td>&nbsp;</td>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['publish_place'] ?>
								</span></td>
								<td><input id="BookPublishplace" name="BookPublishplace" type="text"
									class="textboxtext inputselect" maxlength="64" /></td>
							</tr>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['series'] ?>
								</span></td>
								<td><input id="BookSeries" name="BookSeries" type="text" class="textboxtext inputselect"
									maxlength="64" /></td>
								<td>&nbsp;</td>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['series_number'] ?>
								</span></td>
								<td><input id="BookSeriesno" name="BookSeriesno" type="text" class="textboxtext"
									maxlength="32" /></td>
							</tr>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_code1'] ?>
								</span></td>
								<td><?=$ResponsibilityCodeOption1?></td>
								<td>&nbsp;</td>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_by1'] ?>
								</span></td>
								<td><input id="BookResponb1" name="BookResponb1" type="text" class="textboxtext inputselect"
									maxlength="256" /></td>
							</tr>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_code2'] ?>
								</span></td>
								<td><?=$ResponsibilityCodeOption2?></td>
								<td>&nbsp;</td>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_by2'] ?>
								</span></td>
								<td><input id="BookResponb2" name="BookResponb2" type="text" class="textboxtext inputselect"
									maxlength="256" /></td>
							</tr>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_code3'] ?>
								</span></td>
								<td><?=$ResponsibilityCodeOption3?></td>
								<td>&nbsp;</td>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_by3'] ?>
								</span></td>
								<td><input id="BookResponb3" name="BookResponb3" type="text" class="textboxtext inputselect"
									maxlength="256" /></td>
							</tr>
							
							<tr>
	                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['DIMEN'] ?></span></td>
	                         	<td><input id="BookDimension" name="BookDimension" type="text" class="textboxtext" maxlength="32" value="<?=$Dimension?>"/></td>
								<td>&nbsp;</td>
	                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['ILL'] ?></span></td>
	                         	<td><input id="BookILL" name="BookILL" type="text" class="textboxtext" maxlength="32" value="<?=$ILL?>"/></td>
							</tr>
							
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['number_of_page'] ?>
								</span></td>
								<td><input id="BookNopage" name="BookNopage" type="text" class="textboxnum" style="width:150px" 
									maxlength="16" /></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
						</tbody>
					</table>

					<p></p>
					<div class="form_sub_title_v30">
						<em>- <span class="field_title"><?= $Lang['libms']['book']['info_category'] ?>
						</span> -
						</em>
						<p class="spacer"></p>
					</div>
					<table class="form_table_v30">
						<tbody>
							<tr>
								<td class="field_title_short" width="15%"><span class="field_title"><?= $Lang['libms']['settings']['book_category'] ?>
								</span></td>
								<td width="33%"><div id="div_BookCategory"><?=$BookCategoryOption?></div></td>
								<td width="4%">&nbsp;</td>
								<td class="field_title_short" width="15%"><span class="field_title"><?= $Lang['libms']['settings']['circulation_type'] ?>
								</span></td>
								<td width="33%"><?=$BookCirculationOption?></td>
							</tr>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['settings']['resources_type'] ?>
								</span></td>
								<td><?=$BookResourcesOption?></td>
								<td>&nbsp;</td>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['subject'] ?>
								</span></td>
								<td><input id="BookSubj" name="BookSubj" type="text" class="textboxtext inputselect" maxlength="32" /></td>
								<? /* # no longer use - move to item_edit.php
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['settings']['book_location'] ?>
								</span></td>
								<td><?=$BookLocationOption?></td>
								*/ ?>
							</tr>
							<? /* # no longer use - move to item_edit.php
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['subject'] ?>
								</span></td>
								<td><input name="BookSubj" type="text" class="textboxtext" maxlength="32" /></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['account_date'] ?>
								</span></td>
								<td><?=$linterface->GET_DATE_PICKER("BookAccdate", '')?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
							</tr>
							*/ ?>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['remark_internal'] ?>
								</span></td>
								<td>
									<textarea name="BookInternalremark" class="textboxtext"></textarea>
									</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
						</tbody>
					</table>
					
					<? /* # no longer use - move to item_edit.php
					<p></p>
					<div class="form_sub_title_v30">
						<em>- <span class="field_title"><?= $Lang['libms']['book']['info_purchase'] ?>
						</span> -
						</em>
						<p class="spacer"></p>
					</div>
					<table class="form_table_v30">
						<tbody>
							<tr>
								<td width="15%" class="field_title_short"><span
									class="field_title"><?= $Lang['libms']['book']['purchase_date'] ?>
								</span></td>
								<td width="33%"><?=$linterface->GET_DATE_PICKER("BookPurchasedate", '')?>
									<span class="tabletextremark">(YYYY-MM-DD)</span></td>
								<td width="4%">&nbsp;</td>
								<td width="15%" class="field_title_short"><span
									class="field_title"><?= $Lang['libms']['book']['purchase_price'] ?>
								</span></td>
								<td width="33%"><input name="BookPurchaseprice" type="text"
									class="textboxnum numberic" maxlength="8" /></td>
							</tr>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['list_price'] ?>
								</span></td>
								<td><input name="BookListprice" type="text" class="textboxnum numberic"
									maxlength="8" /></td>
								<td>&nbsp;</td>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['discount'] ?>
								</span></td>
								<td><input name="BookDiscount" type="text" class="textboxnum numberic"
									maxlength="8" /></td>
							</tr>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['distributor'] ?>
								</span></td>
								<td><input name="BookDistributor" type="text"
									class="textboxtext" maxlength="64" /></td>
								<td>&nbsp;</td>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_by_department'] ?>
								</span></td>
								<td><input name="BookPurchasedept" type="text"
									class="textboxtext" maxlength="32" /></td>
							</tr>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_note'] ?>
								</span></td>
								<td><input name="BookPurchasenote" type="text"
									class="textboxtext" maxlength="32" /></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
						</tbody>
					</table>
					*/ ?>
					
					<p></p>
					<div class="form_sub_title_v30">
						<em>- <span class="field_title"><?= $Lang['libms']['book']['info_other'] ?>
						</span> -
						</em>
						<p class="spacer"></p>
					</div>
					<table class="form_table_v30">
						<tbody>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['cover_image'] ?>
								</span></td>
								<td colspan="3"><input name="BookCover" type="file" /></td>
							</tr>
							<tr>
								<td width="15%" class="field_title_short"><span
									class="field_title"><?= $Lang['libms']['book']['tags'] ?> </span>
								</td>
								<td width="70%"><textarea id="BookTag" name="BookTag"
										class="textboxtext" rows="1" style='width: 99%'></textarea></td>
								<td width="15%"><span class="tabletextremark"><?=$Lang['libms']['book']['tags_input']?>
								</span></td>
							</tr>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['URL'] ?>
								</span></td>
								<td><input name="BookUrl" type="text" class="textboxtext"
									maxlength="255" /></td>
								<td>&nbsp;</td>
							</tr>
						</tbody>
					</table>
					
					<? /* # no longer use - move to item_edit.php
					<p></p>
					<div class="form_sub_title_v30">
						<em>- <span class="field_title"><?= $Lang['libms']['book']['info_book'] ?>
						</span> -
						</em>
						<p class="spacer"></p>
					</div>
					<table id="barcode" class="form_table_v30">
						<tbody>
							<tr height='0px'>
								<td width="8%"></td>
								<td width="7%"></td>
								<td width="30%"></td>
								<td width="5%"></td>
								<td width="15%"></td>
								<td width="30%"></td>
								<td width="5%"></td>
							</tr>
							<tr>
								<td class="field_title_short">
									<span class="field_title">
									<?= $Lang['libms']['book']['barcode'] ?>
									<label class='barcode_number'></label>
									</span>
								</td>
								<td><?=$Lang['libms']['general']['auto']?>: <input
									name="GenBarcode[]" type="checkbox" class="gen_barcode"
									value="" /></td>
								<td><input name="BookBarcode[]" type="text"
									class="textboxtext book_barcode" maxlength="64" /></td>
								<td class='icon'>&nbsp;</td>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['book_status'] ?>
								</span></td>
								<td><?= $linterface->GET_SELECTION_BOX($BookStatus, " style='display:none' name='barcode_bookstatus[]'  class='unqiue_bk_status barcode_bookstatus' ", '', 'NORMAL')?>
								</td>
								
								<td>
									<input type="button" class="btn_deleteRow" value="<?= $Lang['libms']['book']['delete'] ?>" style='display:none' />
									<input type="button" class="btn_addRow" value="<?= $Lang['libms']['book']['add'] ?>" />	
								</td>
							</tr>
						</tbody>
					</table>
					*/ ?>
					
					<p></p>
					<div class="form_sub_title_v30">
						<em>- <span class="field_title"><?= $Lang['libms']['book']['info_circulation'] ?>
						</span> -
						</em>
						<p class="spacer"></p>
					</div>
					<table class="form_table_v30">
						<tbody>
							<tr>
								<td width="15%" class="field_title_short"><span
									class="field_title"><?= $Lang['libms']['book']['number_of_copy'] ?>
								</span></td>
								<td width="33%"><label id='lbl_BookNocopy' >0</label>
									<input id="BookNocopy" name="BookNocopy" type="hidden" class="textboxnum" maxlength="2" readonly /></td>
								<td width="4%">&nbsp;</td>
								<td width="15%" class="field_title_short"><span
									class="field_title"><?= $Lang['libms']['book']['number_of_copy_available'] ?>
								</span></td>
								<td width="33%"><label id='lbl_BookNoavailable' >0</label>
									<input id="BookNoavailable" name="BookNoavailable" type="hidden" class="textboxnum" maxlength="2" readonly />
								</td>
							</tr>
							<tr>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['borrow_reserve'] ?>
								</span></td>
								<td><input type="checkbox" name="BookBorrow" value="1" checked="checked" /> <label
									for="BookBorrow"><?=$Lang['libms']['book']['open_borrow']?> </label>
									&nbsp; <input type="checkbox" name="BookReserve" value="1" checked="checked"  /> <label
									for="BookReserve"><?=$Lang['libms']['book']['open_reserve']?> </label>
								</td>
								<td>&nbsp;</td>
								<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['remark_to_user'] ?>
								</span></td>
								<td><input name="BookUserremark" type="text" class="textboxtext"
									maxlength="32" /></td>
							</tr>
						</tbody>
					</table>

					<?=$linterface->MandatoryField();?>
					<center>
						<p class="spacer"></p>
						<?= $SaveBtn ?>
                    	<?= $SaveBtn2 ?>
						<?= $CancelBtn ?>
					</center>
					<p class="spacer"></p>
				</div>
			</td>
		</tr>
	</table>
	<input type="hidden" name="userID" id="userID" value="<?=$userID?>"> 
	<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
	<input type="hidden" name="setItem" id="setItem" value="" />
</form><?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
