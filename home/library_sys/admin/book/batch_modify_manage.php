<?php
/*
 * 	modifying: 
 * 	Log
 *
 *	Date:	2019-07-23 [Henry]
 *			- use the local library of jquery
 *
 *	Date:	2017-04-03 [Cameron]
 *			- handle special characters like <>&" for Subject field in getBatchUpdateSearchFilter()
 *
 * 	Date:	2017-01-27 [Cameron]
 * 			- add search option: ByACNOList in get_batch_modify_search_criteria() 
 *
 *	Date:	2017-01-09 [Cameron]
 *			- add auto complete function for following fields in get_batch_recommend_search_criteria()
 *				book title, author, book tag 
 *
 *	Date:	2016-11-11 [Cameron]
 *			- add Tag as search criteria to get_batch_recommend_search_criteria()
 *
 *	Date:	2016-09-21 [Cameron]	
 *			- add function get_batch_recommend_search_criteria()
 *
 *	Date:	2016-04-29 [Cameron]
 *			- add autocomplete for inputselect fields
 *
 *	Date:	2015-07-31 [Cameron]
 *			- if Language is set in LIBMS_BOOK, check it against BookCategoryCode when filter BookCategory in getBatchUpdateSearchFilter() 
 *
 * 	Date:	2015-07-27 [Cameron]
 * 			- modify get_batch_modify_search_criteria(), change BookCategoryAll value from 'A' to '0' for consistency
 *  
 * 	Date:	2015-07-08 [Cameron] 
 * 			- Add common javascript function or move common fragment here: clear_item_list(), show_left_panel_result()
 * 			- modify to support two book category types in filter panel
 *  
 * 	Date:	2015-07-07 [Cameron] Add column() function, add $field, $order member
 * 
 * 	Date:	2015-07-06 [Cameron] Add search by Barcode radio option
 * 
 * 	Date:	2015-01-05 [Cameron] Add search by ACNO range or match exactly one ACNO 
 * 					
 * 	Date:	2014-12-16 [Cameron] create this file
 * 
 */
 
class batch_modify_manage extends liblms {
	private $field;
	private $order;

	public function getfield() {
	    return $this->field;
	}	
	public function setfield($val) {
	    $this->field = $val;
	}
	public function getorder() {
	    return $this->order;
	}	
	public function setorder($val) {
	    $this->order = $val;
	}

	public function get_batch_edit_style() {
		ob_start();
?>
		.be_table {
			width: 100%;
			padding:0px;
		}
		.searchOption {
			width: 250px;
		}
		.searchBtnDiv {
			height:25px; 
			margin-top: -5px;
			padding-top:0px; 
			padding-bottom:0px;			
		}	
		.applyBtn {
			height:25px;
		}
		.selection {
			width:220px;
		}
		.nowrap {
			white-space: nowrap;
		}
		.search_title {
			border-bottom: 1px solid #FFFFFF;
			background: #f3f3f3;
			padding-left: 2px;
			padding-right: 2px;			
			width:100px;
			white-space: nowrap;
		}
		.inputbox {
			width:98%;
		}
		
		#left_search_panel {
			left:0; 
			height:280px; 	
		}
		
		#left_list_panel {
			left:0;
			height:340px;
		}
		
		#left_list_panel_content {
			left:0;
			height:330px;
			overflow-y:auto; 
		}
		
		#right_change_panel {
			height:660px;
			visibility: hidden;	
		}
<?php
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end function get_batch_edit_style()

	public function get_batch_edit_js() {
		global $i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format, $Lang;
		ob_start();
?>

<script src="/templates/jquery/jquery-1.6.1.min.js" type="text/javascript" charset="utf-8"></script>

<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">

<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--
function checkDate(obj){
    if(!check_date(obj,"<?=$Lang["libms"]["batch_edit"]["InvalidDateFormat"]?>")) return false;
    return true;
}
 
function isEmptyField(field_type){
	var isFieldValid = true;

	if ( (field_type == 'search_nonEmpty') && ($("#ByACNO").is(':checked')) && ($.trim($("#ACNO_Start").val()) == '') && ($.trim($("#ACNO_End").val()) == '') ) {
		isFieldValid= false;
		alert('<?=$Lang["libms"]["book"]["alert"]["fillin_anco"]?>');
		$("#ACNO_Start").focus();
	}
	else if ( (field_type == 'search_nonEmpty') && ($("#ByACNOList").is(':checked')) && ($.trim($("#ACNO_List").val()) == '')) {
		isFieldValid= false;
		alert('<?=$Lang["libms"]["book"]["alert"]["fillin_anco"]?>');
		$("#ACNO_List").focus();
	}
	else {
		$('.'+field_type).each(function(){
			if( $(this).hasClass('optional')){
				return true;
			}
			var conds =! $.trim($(this).val());
			if( conds ){				
				if (field_type != 'search_nonEmpty') {
					isFieldValid= false;
					var field = $(this).parent().parent().children(':first-child').text()
					alert("<?=$Lang["libms"]["batch_edit"]["PleaseSelect"]?> "+field);					
				}
//				var errorMsg = '<span class="errorMsg new_alert"><br /><?=$Lang["libms"]["batch_edit"]["PleaseFillAboveField"]?><br /></span>';
//			
//				if(!$(this).next().hasClass('errorMsg')){
//					$(this).after(errorMsg);
//				}
			}
		});
	}	
	return isFieldValid;
}
 
function isNumberField(){
	var isFieldValid = true;

	$('.numberField').each(function(){
		var str = $.trim($(this).val());
		if ((str != "") && (isNaN(Number(str)) || str < 0)){			
			var field = $(this).parent().parent().children(':first-child').text()
			alert("<?=$Lang["libms"]["book"]["alert"]["numeric"]?> "+field);	
			$(this).focus(); 
			isFieldValid = false;
			return false;	
		}
	});
	return isFieldValid;
}
 
function isDateField(field_type){
	var isFieldValid = true;
	
	$('.'+field_type).each(function(){
		if( $(this).hasClass('optional')){
			if ($.trim($(this).val()) == '') {
				return true;
			}
		}
	    if(!checkDate(document.getElementById(this.id))){
            isFieldValid = false;
	    }
	});
	if (field_type == 'search_date') {
		isFieldValid = isFieldValid & compare_date(document.getElementById("FromDate"), document.getElementById("ToDate"),"<?=$Lang['General']['WrongDateLogic']?>");
	}
	return isFieldValid;
}

function clear_item_list() {
	$('#book_item_table tr').not('tr:first').remove();
	$('#left_list_panel').attr('style','display:none');
	$('#right_change_panel').css('visibility','hidden');
}

function show_left_panel_result(json) {
	rs = jQuery.parseJSON(json);
	if ( rs['result'] != undefined ) {
		$('#left_list_panel_content').html(rs['layout']);
		$('#left_list_panel').attr('style','display:block');
		if ( rs['result'] != "0" ) {							
			$('#right_change_panel').css('visibility','visible');
		}
		else {
			$('#right_change_panel').css('visibility','hidden');
		}
	}											
}

//-->
</script>
<?php
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end function get_batch_edit_js()

	public function get_batch_edit_doc_ready_js() {
		global $Lang;
		ob_start();
?>
	$('input[name="ByACNOOrAcctDate"]').change(function(){
		if ($("#ByBarcode").is(':checked')) {
			$('.barcode_element').attr('style','display:block');
			$('.acno_element').attr('style','display:none');
			$('.acct_date_element').attr('style','display:none');
			$('.acno_list_element').attr('style','display:none');
			$('.search_criteria').attr('style','display:none');
		}
		else if ($("#ByACNO").is(':checked')) {
			$('.barcode_element').attr('style','display:none');
			$('.acno_element').attr('style','display:block');
			$('.acct_date_element').attr('style','display:none');
			$('.acno_list_element').attr('style','display:none');
			$('.search_criteria').attr('style','display:table-row');
		}
		else if ($("#ByAccountDate").is(':checked')) {
			$('.barcode_element').attr('style','display:none');
			$('.acno_element').attr('style','display:none');
			$('.acct_date_element').attr('style','display:block');
			$('.acno_list_element').attr('style','display:none');
			$('.search_criteria').attr('style','display:table-row');
		}
		else if ($("#ByACNOList").is(':checked')) {
			$('.barcode_element').attr('style','display:none');
			$('.acno_element').attr('style','display:none');
			$('.acct_date_element').attr('style','display:none');
			$('.acno_list_element').attr('style','display:block');
			$('.search_criteria').attr('style','display:table-row');
		}
	});

	$('#Barcode').keyup(function(event) {
	  if (event.which == 13) {
		$('#add_book').click();
	  }  
	});

	$('#reset').click(function(){
		if (($('#book_item_table tr').length > 1 ) && ($('tr#no_record_row').length <= 0) ) {
			var confirm = window.confirm("<?=$Lang["libms"]["batch_edit"]["ConfirmClearList"]?>");
		}
		else {
			confirm = true;
		}		
		if (confirm) {
			clear_item_list();
		}
	});
	
	$('#Barcode').keypress(function(e){
	    if ( e.which == 13 ) {
			e.preventDefault();
		    return false;
	    }
	});

	$("input.inputselect").inputselect({
		image_path: '/images/2009a/', 
		ajax_script: 'ajax_get_selection_list.php', 
		js_lang_alert: {'no_records' : '<?=$Lang['libms']['NoRecordAtThisMoment']?>' }
	});
	
	$('input[name="BookCategoryType"]').change(function(){
		if($('#BookCategoryTypeAll').is(':checked'))
		{
			$('#BookCategoryCode1').attr('style','display:none');
			$('#BookCategoryCode2').attr('style','display:none');
		}
		else if ($('#BookCategoryType1').is(':checked'))
		{				
			$('#BookCategoryCode1').attr('style','display:block');
			$('#BookCategoryCode2').attr('style','display:none');
		}
		else if ($('#BookCategoryType2').is(':checked'))
		{
			$('#BookCategoryCode1').attr('style','display:none');
			$('#BookCategoryCode2').attr('style','display:block');
		}
	});
	
	// autocomplete for inputselect fields
	$('.inputselect').each(function(){
		var this_id = $(this).attr('id');
		if($(this).length > 0){
			$(this).autocomplete(
		      "ajax_get_suggestions.php",
		      {
		  			delay:3,
		  			minChars:1,
		  			matchContains:1,
		  			extraParams: {'field':this_id},
		  			autoFill:false,
		  			overflow_y: 'auto',
		  			overflow_x: 'hidden',
		  			maxHeight: '200px'
		  		}
		    );
		}
	});

<?php
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end get_batch_edit_doc_ready_js()

	public function get_batch_modify_search_criteria() {
		global $Lang,$i_Profile_From,$i_Profile_To,$button_search,$linterface,$libms;
		
		$FromDate = date('Y-m-d');
		$ToDate = $FromDate;
		$CirculationTypeCode= null;	// selected value
		$BookCategoryCode 	= null;
		$ResourcesTypeCode 	= null;
		$Subject = null;

		
		$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
		foreach ($result as $row){
			$book_category_name[] = htmlspecialchars($row['value']);
		}
		
		$bookCategoryOptionHead = " -- ".$Lang["libms"]["batch_edit"]["PleaseSelect"]." -- </option>";
		$bookCategoryOptionHead .= "<option value='".$Lang['General']['EmptySymbol']."'>".$Lang["libms"]["report"]["cannot_define"];
		$BookCategoryArray1 = $libms->BOOK_GETOPTION('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', "CONCAT(`BookCategoryCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookCategoryCode', 'BookCategoryType=1');
		$BookCategoryOption1 = $linterface->GET_SELECTION_BOX($BookCategoryArray1, " name='BookCategoryCode1' id='BookCategoryCode1' class='searchOption' style='display:none'", $bookCategoryOptionHead, $BookCategoryCode);
		$BookCategoryArray2 = $libms->BOOK_GETOPTION('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', "CONCAT(`BookCategoryCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookCategoryCode', 'BookCategoryType=2');
		$BookCategoryOption2 = $linterface->GET_SELECTION_BOX($BookCategoryArray2, " name='BookCategoryCode2' id='BookCategoryCode2' class='searchOption' style='display:none'", $bookCategoryOptionHead, $BookCategoryCode);
		
		ob_start();
?>				
			<div class="table_board">
	        	<table class="form_table_v30">
	            	<tr>
	              		<td class="search_title"><span class="status_alert"><strong>*</strong></span><?=$Lang["libms"]["batch_edit"]["PleaseSelect"]?></td>
	              		<td>
	              			<input type="radio" name="ByACNOOrAcctDate" id="ByBarcode" value="ByBarcode" checked="checked"/><label for="ByBarcode"><?=$Lang["libms"]["batch_edit"]["ByBarcode"]?></label>
	              			<input type="radio" name="ByACNOOrAcctDate" id="ByACNO" value="ByACNO" /><label for="ByACNO"><?=$Lang["libms"]["batch_edit"]["ByACNO"]?></label>
	              			<input type="radio" name="ByACNOOrAcctDate" id="ByAccountDate" value="ByAccountDate" /><label for="ByAccountDate"><?=$Lang["libms"]["batch_edit"]["ByAccountDate"]?></label>
	              			<input type="radio" name="ByACNOOrAcctDate" id="ByACNOList" value="ByACNOList" /><label for="ByACNOList"><?=$Lang["libms"]["batch_edit"]["ByACNOList"]?></label>
	              			<div class="barcode_element" style="display:block">
		              			<input name="Barcode" id="Barcode" type="text" class="search_nonEmpty">
		              			<input name="add_book" type="button" id="add_book" class="formbutton" value="&crarr; <?=$Lang["libms"]["btn"]["enter"]?>">
		              			<input id="reset" type="button" class="formbutton" value="<?=$Lang["libms"]["batch_edit"]["ClearList"]?>" />
	              			</div>
	              			<div class="acno_element" style="display:none">
		              			<input name="ACNO_Start" id="ACNO_Start" type="text" class="search_nonEmpty" maxlength="16"> ~ <input name="ACNO_End" id="ACNO_End" type="text" class="search_nonEmpty" maxlength="16">
	              			</div>
              				<div class="acct_date_element" style="display:none">
	               				<?=$i_Profile_From?> <?=$linterface->GET_DATE_PICKER($Name="FromDate",$DefaultValue=$FromDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum search_date")?>
	 							<?=$i_Profile_To?> 
	 							<?=$linterface->GET_DATE_PICKER($Name="ToDate",$DefaultValue=$ToDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum search_date")?>
								<span class="tabletextremark">(yyyy-mm-dd)</span>
							</div>
	              			<div class="acno_list_element" style="display:none">
		              			<textarea name="ACNO_List" id="ACNO_List" class="search_nonEmpty" rows="8" cols="32"></textarea>
		              			<br><?=$Lang["libms"]["batch_edit"]["OneACNOPerRow"]?>
	              			</div>
						</td>
					</tr>
															
	            	<tr class="search_criteria" style="display:none">	              		
	              		<td class="search_title"><?=$Lang["libms"]["settings"]["circulation_type"]?></td>
						<td><?=$libms->get_xxxFilter_html('LIBMS_CIRCULATION_TYPE','CirculationTypeCode', $CirculationTypeCode, $size=1, $multiple=false, $includeNotDefined=true, 'class="searchOption"')?></td>						
					</tr>

	            	<tr class="search_criteria" style="display:none">	              		
	              		<td class="search_title"><?=$Lang["libms"]["settings"]["book_category"]?></td>
						<td>
							<?=$linterface->Get_Radio_Button("BookCategoryTypeAll", "BookCategoryType", '0', true, "", $Lang["libms"]["report"]["fine_status_all"])?>								
							<?=$linterface->Get_Radio_Button("BookCategoryType1", "BookCategoryType", 1, false, "", $book_category_name[0]?$book_category_name[0]:$Lang['libms']['report']['book_category'].' 1')?>
							<?=$linterface->Get_Radio_Button("BookCategoryType2", "BookCategoryType", 2, false, "", $book_category_name[1]?$book_category_name[1]:$Lang['libms']['report']['book_category'].' 2')?>
							<?=$BookCategoryOption1?>
							<?=$BookCategoryOption2?>
						</td>
					</tr>

	            	<tr class="search_criteria" style="display:none">	              		
	              		<td class="search_title"><?=$Lang["libms"]["settings"]["resources_type"]?></td>
						<td><?=$libms->get_xxxFilter_html('LIBMS_RESOURCES_TYPE','ResourcesTypeCode', $ResourcesTypeCode, $size=1, $multiple=false, $includeNotDefined=true, 'class="searchOption"')?></td>						
					</tr>

	            	<tr class="search_criteria" style="display:none">	              		
	              		<td class="search_title"><?=$Lang["libms"]["report"]["subject"]?></td>
						<td><?=$libms->get_xxxFilter_html('LIBMS_BOOK','Subject', $Subject, $size=1, $multiple=false, $includeNotDefined=true, 'class="searchOption"')?></td>						
					</tr>

	            	<tr class="search_criteria" style="display:none">
						<td colspan="2"><div class="edit_bottom_v30 searchBtnDiv"><?= $linterface->GET_ACTION_BTN($button_search, "button", $ParOnClick="",$ParName="search") ?></div></td>						
					</tr>
					
				</table>
			</div>
<?php
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
		
	}	// end function showSearchCriteria()
 
 
	####### Trim all request #######
	private function recursive_trim(&$array){
		foreach($array as &$item){
			if (is_array($item)){
				$this->recursive_trim($item);
			}else{
				if (is_string($item))
					$item=trim($item);
			}
		}
	}	// end function recursive_trim()
	
	/*
	 * 	pass in $_POST values
	 * 	@SrcFrom	-- from where does this function is called, Book or BookItem 
	 * 	@return string
	 * 	Constraint:	filter fields are in LIBMS_BOOK (b) and LIBMS_BOOK_UNIQUE (bu)
	 * 
	 * 	Note:	(2014-12-22) if search from Book, CirculationTypeCode is searched against that in LIBMS.BOOK
	 * 			else it should search against LIBMS.BOOK_UNIQUE if value is not null, otherwise check against LIBMS.BOOK
	 */
	public function getBatchUpdateSearchFilter($SrcFrom="Book") {
		global $Lang;
		
		$this->recursive_trim($_POST);
		
		$ACNO_Start = mysql_real_escape_string(htmlspecialchars($_POST['ACNO_Start']));
		$ACNO_End = mysql_real_escape_string(htmlspecialchars($_POST['ACNO_End']));
		
		## date range
		$FromDate =	$_POST['FromDate'] ? $_POST['FromDate'] : date('Y-m-d');
		$ToDate = $_POST['ToDate'] ? $_POST['ToDate'] : date('Y-m-d');
		$cond = '';
		
		// Match ACNO range or match exactly one item
		if($_POST['ByACNOOrAcctDate'] == 'ByACNO') {
			if (($ACNO_Start != "") && ($ACNO_End != ""))
			{
				$cond .= "AND trim(bu.ACNO) BETWEEN '".$ACNO_Start."' AND '".$ACNO_End."' ";
			}
			else if ($ACNO_Start != "")
			{
				$cond .= "AND trim(bu.ACNO)='".$ACNO_Start."' ";
			}
			else if ($ACNO_End != "")
			{
				$cond .= "AND trim(bu.ACNO)='".$ACNO_End."' ";
			}
		}
		else if($_POST['ByACNOOrAcctDate'] == 'ByACNOList') {
			$ACNO_LIST = str_replace(array("\r\n","\n","\r"),",",$_POST['ACNO_List']);
			$acno_array = explode(",",$ACNO_LIST);
			$new_acno_array = array();
			foreach((array)$acno_array as $k=>$v) {
				$v = trim($v);
				if ($v != '') {
					$new_acno_array[$k] = $v;		
				}				
			}
			unset($acno_array);
			$ACNO_LIST = implode("','",$new_acno_array);			
			$cond .= "AND trim(bu.ACNO) IN ('".$ACNO_LIST."') ";
		}	
		else {	// By AccountDate
			$cond .= "AND bu.CreationDate >= '{$FromDate} 00:00:00' "; 
			$cond .= "AND bu.CreationDate <= '{$ToDate} 23:59:59' ";
		}	
		
		## CirculationTypeCode filter
		$list_CirculationTypeCode = '';
		if (is_array($_POST['CirculationTypeCode'])) {
			if (count($_POST['CirculationTypeCode']) > 0) {
				$list_CirculationTypeCode = "'". implode("','", $_POST['CirculationTypeCode']) . "'";
			}	
		}
		else {
			if ($_POST['CirculationTypeCode'] != '') {
				$list_CirculationTypeCode = "'" . $_POST['CirculationTypeCode'] . "'"; 
			}	
		}
		
		if ($list_CirculationTypeCode != '') {
			$circulationTypeCodeField = ($SrcFrom == "Book") ? "b.`CirculationTypeCode`" : "IF(bu.`CirculationTypeCode` IS NULL OR bu.`CirculationTypeCode`='',b.`CirculationTypeCode`,bu.`CirculationTypeCode`)";
			
			if (strpos($list_CirculationTypeCode, "'".$Lang['General']['EmptySymbol']."'") === false)
			{
				$cond .= " AND {$circulationTypeCodeField} IN ({$list_CirculationTypeCode})";
				
			}else{
				$cond .= " AND (b.`CirculationTypeCode` IS NULL OR b.`CirculationTypeCode`='' OR {$circulationTypeCodeField} IN ({$list_CirculationTypeCode}))";
			}
		}
		
		## BookCategoryCode filter
		$list_BookCategoryCode = '';
		if ($_POST['BookCategoryType'] != '0') {
			if ($_POST['BookCategoryType'] == '1') {
				$BookCategoryCode = $_POST['BookCategoryCode1'];
			}
			else {
				$BookCategoryCode = $_POST['BookCategoryCode2'];
			} 
			if (is_array($BookCategoryCode)) {
				if (count($BookCategoryCode) > 0) {
					$list_BookCategoryCode = "'". implode("','", $BookCategoryCode) . "'";
				}	
			}
			else {
				if ($BookCategoryCode != '') {
					$list_BookCategoryCode = "'" . $BookCategoryCode . "'"; 
				}	
			}
				
			if ($list_BookCategoryCode != '') {
				if (strpos($list_BookCategoryCode, "'".$Lang['General']['EmptySymbol']."'") === false)	// not include (empty or null category)
				{	 
					$cond .= " AND b.`BookCategoryCode` IN ({$list_BookCategoryCode})";
//					$cond .= " AND (b.`Language` IN (SELECT lbl2.`BookLanguageCode` FROM `LIBMS_BOOK_LANGUAGE` lbl2 WHERE lbl2.`BookCategoryType` = '".
//							$_POST['BookCategoryType']."') OR (".$_POST['BookCategoryType']."=1 AND (b.`Language` IS NULL OR b.`Language` = '')))";
				}else{
					$cond .= " AND (b.`BookCategoryCode` IS NULL OR b.`BookCategoryCode`='' OR b.`BookCategoryCode` IN ({$list_BookCategoryCode}))";
				}
				$cond .= " AND (b.`Language` IN (SELECT lbl2.`BookLanguageCode` FROM `LIBMS_BOOK_LANGUAGE` lbl2 WHERE lbl2.`BookCategoryType` = '".
						$_POST['BookCategoryType']."') OR b.`Language` IS NULL OR b.`Language` = '')";
			}
		}
		
		## ResourcesTypeCode filter
		$list_ResourcesTypeCode = '';
		if (is_array($_POST['ResourcesTypeCode'])) {
			if (count($_POST['ResourcesTypeCode']) > 0) {
				$list_ResourcesTypeCode = "'". implode("','", $_POST['ResourcesTypeCode']) . "'";
			}	
		}
		else {
			if ($_POST['ResourcesTypeCode'] != '') {
				$list_ResourcesTypeCode = "'" . $_POST['ResourcesTypeCode'] . "'"; 
			}	
		}
		
		if ($list_ResourcesTypeCode != '') {
			if (strpos($list_ResourcesTypeCode, "'".$Lang['General']['EmptySymbol']."'") === false)
			{
				$cond .= " AND b.`ResourcesTypeCode` IN ({$list_ResourcesTypeCode})";
				
			}else{
				$cond .= " AND (b.`ResourcesTypeCode` IS NULL OR b.`ResourcesTypeCode`='' OR b.`ResourcesTypeCode` IN ({$list_ResourcesTypeCode}))";
			}
		}
		
		## Subject filter
		$list_Subject = '';
		$list_Subject2 = '';
		if (is_array($_POST['Subject'])) {
			if (count($_POST['Subject']) > 0) {
				$list_Subject = "'". implode("','", $_POST['Subject']) . "'";
				$list_Subject2 = "'". implode("','", special_sql_str($_POST['Subject'])) . "'";
			}	
		}
		else {
			if ($_POST['Subject'] != '') {
				$list_Subject = "'" . $_POST['Subject'] . "'"; 
				$list_Subject2 = "'" . special_sql_str($_POST['Subject']) . "'";
			}	
		}
		
		if ($list_Subject != '') {
			if (strpos($list_Subject, "'".$Lang['General']['EmptySymbol']."'") === false)
			{
				$cond .= " AND (b.`Subject` IN ({$list_Subject}) OR b.`Subject` IN ({$list_Subject2}))";
				
			}else{
				$cond .= " AND (b.`Subject` IS NULL OR b.`Subject`='' OR b.`Subject` IN ({$list_Subject}) OR b.`Subject` IN ({$list_Subject2}))";
			}
		}
		
		return $cond;
	}	// end function getBatchUpdateSearchFilter
	
	
	public function column($field_index, $field_name){
	    global $image_path, $Lang, $LAYOUT_SKIN;
	    $x = "";
	    if($this->field==$field_index){
	    	if($this->order==1) $x .= "<a href=\"javascript:sort_batch_edit_page(0,$field_index)\" onMouseOver=\"window.status='".$Lang["libms"]["report"]["sortby"]." $field_name';MM_swapImage('sort_icon','','$image_path/{$LAYOUT_SKIN}/icon_sort_a_on.gif',1);return true;\" onMouseOut=\"window.status='';MM_swapImgRestore();return true;\" title=\"".$Lang["libms"]["report"]["sortby"]." $field_name\">";
	        if($this->order==0) $x .= "<a href=\"javascript:sort_batch_edit_page(1,$field_index)\" onMouseOver=\"window.status='".$Lang["libms"]["report"]["sortby"]." $field_name';MM_swapImage('sort_icon','','$image_path/{$LAYOUT_SKIN}/icon_sort_d_on.gif',1);return true;\" onMouseOut=\"window.status='';MM_swapImgRestore();return true;\" title=\"".$Lang["libms"]["report"]["sortby"]." $field_name\">";
	    }else{
	        $x .= "<a href=\"javascript:sort_batch_edit_page($this->order,$field_index)\" onMouseOver=\"window.status='".$Lang["libms"]["report"]["sortby"]." $field_name';return true;\" onMouseOut=\"window.status='';return true;\" title=\"".$Lang["libms"]["report"]["sortby"]." $field_name\">";
	    }
	    $x .= str_replace("_", " ", $field_name);
	    if($this->field==$field_index)
	    {
			//Change arrow display    	                                        
            if($this->order==1) $x .= "<img name='sort_icon' id='sort_icon' src='$image_path/{$LAYOUT_SKIN}/icon_sort_a_off.gif' align='absmiddle' border='0' />";
            if($this->order==0) $x .= "<img name='sort_icon' id='sort_icon' onMouseOver=\"MM_swapImage('sort_asc','','$image_path/{$LAYOUT_SKIN}/icon_sort_d_on.gif',1)\" onMouseOut='MM_swapImgRestore()' src='$image_path/{$LAYOUT_SKIN}/icon_sort_d_off.gif' align='absmiddle'  border='0' />";
	    }
	    $x .= "</a>";
	    return $x;
	}
	
	
	public function get_batch_recommend_search_criteria() {
		global $Lang,$i_Profile_From,$i_Profile_To,$button_search,$linterface,$libms;
		
		$FromDate = date('Y-m-d');
		$ToDate = $FromDate;
		$CirculationTypeCode= null;	// selected value
		$BookCategoryCode 	= null;
		$ResourcesTypeCode 	= null;
		$Subject = null;

		
		$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
		foreach ($result as $row){
			$book_category_name[] = htmlspecialchars($row['value']);
		}
		
		$bookCategoryOptionHead = " -- ".$Lang["libms"]["batch_edit"]["PleaseSelect"]." -- </option>";
		$bookCategoryOptionHead .= "<option value='".$Lang['General']['EmptySymbol']."'>".$Lang["libms"]["report"]["cannot_define"];
		$BookCategoryArray1 = $libms->BOOK_GETOPTION('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', "CONCAT(`BookCategoryCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookCategoryCode', 'BookCategoryType=1');
		$BookCategoryOption1 = $linterface->GET_SELECTION_BOX($BookCategoryArray1, " name='BookCategoryCode1' id='BookCategoryCode1' class='searchOption' style='display:none'", $bookCategoryOptionHead, $BookCategoryCode);
		$BookCategoryArray2 = $libms->BOOK_GETOPTION('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', "CONCAT(`BookCategoryCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookCategoryCode', 'BookCategoryType=2');
		$BookCategoryOption2 = $linterface->GET_SELECTION_BOX($BookCategoryArray2, " name='BookCategoryCode2' id='BookCategoryCode2' class='searchOption' style='display:none'", $bookCategoryOptionHead, $BookCategoryCode);
		
		ob_start();
?>				
			<div class="table_board">
	        	<table class="form_table_v30">
	            	<tr>
	              		<td class="search_title"><span class="status_alert"><strong>*</strong></span><?=$Lang["libms"]["batch_edit"]["PleaseSelect"]?></td>
	              		<td>
	              			<input type="radio" name="ByACNOOrAcctDate" id="ByBarcode" value="ByBarcode" checked="checked"/><label for="ByBarcode"><?=$Lang["libms"]["batch_edit"]["ByBarcode"]?></label>
	              			<input type="radio" name="ByACNOOrAcctDate" id="ByACNO" value="ByACNO" /><label for="ByACNO"><?=$Lang["libms"]["batch_edit"]["ByACNO"]?></label>
	              			<input type="radio" name="ByACNOOrAcctDate" id="ByAccountDate" value="ByAccountDate" /><label for="ByAccountDate"><?=$Lang["libms"]["batch_edit"]["ByAccountDate"]?></label>
	              			<input type="radio" name="ByACNOOrAcctDate" id="ByOthers" value="ByOthers" /><label for="ByOthers"><?=$Lang["libms"]["batch_edit"]["ByOthers"]?></label>
	              			<div class="barcode_element" style="display:block">
		              			<input name="Barcode" id="Barcode" type="text" class="search_nonEmpty">
		              			<input name="add_book" type="button" id="add_book" class="formbutton" value="&crarr; <?=$Lang["libms"]["btn"]["enter"]?>">
		              			<input id="reset" type="button" class="formbutton" value="<?=$Lang["libms"]["batch_edit"]["ClearList"]?>" />
	              			</div>
	              			<div class="acno_element" style="display:none">
		              			<input name="ACNO_Start" id="ACNO_Start" type="text" class="search_nonEmpty" maxlength="16"> ~ <input name="ACNO_End" id="ACNO_End" type="text" class="search_nonEmpty" maxlength="16">
	              			</div>
              				<div class="acct_date_element" style="display:none">
	               				<?=$i_Profile_From?> <?=$linterface->GET_DATE_PICKER($Name="FromDate",$DefaultValue=$FromDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum search_date")?>
	 							<?=$i_Profile_To?> 
	 							<?=$linterface->GET_DATE_PICKER($Name="ToDate",$DefaultValue=$ToDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum search_date")?>
								<span class="tabletextremark">(yyyy-mm-dd)</span>
							</div>								
						</td>
					</tr>
															
	            	<tr class="search_criteria" style="display:none">	              		
	              		<td class="search_title"><?=$Lang["libms"]["settings"]["circulation_type"]?></td>
						<td><?=$libms->get_xxxFilter_html('LIBMS_CIRCULATION_TYPE','CirculationTypeCode', $CirculationTypeCode, $size=1, $multiple=false, $includeNotDefined=true, 'class="searchOption"')?></td>						
					</tr>

	            	<tr class="search_criteria" style="display:none">	              		
	              		<td class="search_title"><?=$Lang["libms"]["settings"]["book_category"]?></td>
						<td>
							<?=$linterface->Get_Radio_Button("BookCategoryTypeAll", "BookCategoryType", '0', true, "", $Lang["libms"]["report"]["fine_status_all"])?>								
							<?=$linterface->Get_Radio_Button("BookCategoryType1", "BookCategoryType", 1, false, "", $book_category_name[0]?$book_category_name[0]:$Lang['libms']['report']['book_category'].' 1')?>
							<?=$linterface->Get_Radio_Button("BookCategoryType2", "BookCategoryType", 2, false, "", $book_category_name[1]?$book_category_name[1]:$Lang['libms']['report']['book_category'].' 2')?>
							<?=$BookCategoryOption1?>
							<?=$BookCategoryOption2?>
						</td>
					</tr>

	            	<tr class="search_criteria" style="display:none">	              		
	              		<td class="search_title"><?=$Lang["libms"]["settings"]["resources_type"]?></td>
						<td><?=$libms->get_xxxFilter_html('LIBMS_RESOURCES_TYPE','ResourcesTypeCode', $ResourcesTypeCode, $size=1, $multiple=false, $includeNotDefined=true, 'class="searchOption"')?></td>						
					</tr>

	            	<tr class="search_criteria" style="display:none">	              		
	              		<td class="search_title"><?=$Lang["libms"]["report"]["subject"]?></td>
						<td><?=$libms->get_xxxFilter_html('LIBMS_BOOK','Subject', $Subject, $size=1, $multiple=false, $includeNotDefined=true, 'class="searchOption"')?></td>						
					</tr>

	            	<tr class="search_criteria_other" style="display:none">	              		
	              		<td class="search_title"><?=$Lang["libms"]["batch_edit"]["BookTitle"]?></td>
						<td><input autocomplete="off" type="text" class="textboxtext" name="BookTitle" id="BookTitle" value=""></td>
					</tr>
	            	<tr class="search_criteria_other" style="display:none">	              		
	              		<td class="search_title"><?=$Lang["libms"]["batch_edit"]["Author"]?></td>
						<td><input autocomplete="off" type="text" class="textboxtext" name="Author" id="Author" value=""></td>						
					</tr>
	            	<tr class="search_criteria_other" style="display:none">	              		
	              		<td class="search_title"><?=$Lang["libms"]["batch_edit"]["CallNumber"]?></td>
						<td><input type="text" class="textboxtext" name="CallNumber" id="CallNumber" value=""></td>						
					</tr>
	            	<tr class="search_criteria_other" style="display:none">	              		
	              		<td class="search_title"><?=$Lang['libms']['batch_edit']['edit_tags']?></td>
						<td><input autocomplete="off" type="text" class="textboxtext" name="Tag" id="Tag" value=""></td>
					</tr>

	            	<tr id="SearchButton" style="display:none">
						<td colspan="2"><div class="edit_bottom_v30 searchBtnDiv"><?= $linterface->GET_ACTION_BTN($button_search, "button", $ParOnClick="",$ParName="search") ?></div></td>						
					</tr>
					
				</table>
			</div>
<?php
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
		
	}	// end get_batch_recommend_search_criteria()
	
}		// end class
?>