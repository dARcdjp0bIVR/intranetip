<?php
// using:    

/************************************
 * 
 *  # important!!!!
 *  filters, keyword are applied to book_export.php; if you change them or add more, please also update book_export.php
 *  # important!!!!
 *
 * Date:    2020-05-15 (Cameron)
 *          - fix to allow export books which book item RecordStatus is null [case #E143600]
 *          
 * Date:	2016-12-28 (Cameron)
 *			- allow admin to choose exporting which fields
 *
 * Date:	2015-10-13 (Cameron)
 * 			- fix bug on exporting fields that contain double quotation mark by passing argument '11' to GET_EXPORT_TXT() function
 *
 * Date:	2015-10-09 (Cameron)
 * 			- change $limit_upper to smaller so that memory in each loop can be reduced, hence avoid showing fatal error
 * 			- retrieve each row record in numeric array only to save memory
 *
 * Date:	2015-10-08 (Cameron)
 * Details:	- fix bug on avoid showing error by setting memory to unlimited if there's too many records to export
 *
 * Date:	2015-08-20 (Cameron)
 * Details:	- fix bug on concatenating export pages by adding line break('\r\n') to $export_content
 * 			- fix bug on $sql_limit_start, not need to deduct 1 from $limit_upper when multiply loop counter $limit_i    			 
 * 
 * Date: 	2015-04-01 (Cameron)
 * Details:	fix bug of "export fail after sort by call number", use "lb.CallNum, lb.CallNum2" rather than "CallNumberMixed"  
 * 
 * Date:	2014-12-04 (Cameron)
 * Details:	move search filter part to function in book_export_common.php 
 * 
 * Date:	2014-10-25 (Yuen) ip.2.5.5.10.1
 * Details: support item Circulation Type
 * 
 * Date:	2014-06-19 (Henry)
 * Details: fixed the problem that missing first items in the export file
 * 
 * Date:	2014-03-19 (Yuen)
 * Details: fixed the problem that missing some items when using keyword search 
 * 
 * Date:	2014-01-14 (Henry)
 * Details: add Language filter SQL cond
 * 
 * Date:	2013-11-15 (Yuen)
 * Details: introduced
 * 
 ************************************/

@SET_TIME_LIMIT(216000);
//ini_set('memory_limit','256M');
ini_set('memory_limit',-1);


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."home/library_sys/admin/book/book_export_common.php");

intranet_auth();
intranet_opendb();


# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}



//include_once($PATH_WRT_ROOT."lang/libms.lang.b5.php");	
//$ColumsInChi = $Lang["libms"]["import_book"]["ImportCSVDataCol_New2"];
//for ($i=0; $i<sizeof($ColumsInChi); $i++)
//{
//	$ColumsInChi[$i] = strip_tags($ColumsInChi[$i]);
//	$ColumsInChi[$i] = preg_replace('(\+|\*|\@|\#|)', "", $ColumsInChi[$i]);
//	$tmpArr = explode(" ", $ColumsInChi[$i] );
//	if (is_array($tmpArr) && sizeof($tmpArr)>1)
//	{
//		$ColumsInChi[$i] = trim($tmpArr[0]);
//	}
//}


## pickup selected fields
$selRet = getExportSelectedFields();
$selFields = array();
$selChiCol = array();
$selEngCol = array();
$selFields = $selRet['selFields'];
$selChiCol = $selRet['selChiCol'];
$selEngCol = $selRet['selEngCol'];
$tagIndex = $selRet['tagIndex'];
$accountDateIndex = $selRet['accountDateIndex'];
$statusIndex = $selRet['statusIndex'];


# MUST use ENG version for getting the item status values
include_once($PATH_WRT_ROOT."lang/libms.lang.en.php");	

/*
debug_r($Code);
die();

# conditions including keyword, bookID (var Code), 3 categories, subject, publisher

# order by (order, field)

*/

$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;
$field_array = array("lb.ISBN", "lb.CallNum, lb.CallNum2", "lb.BookTitle", "lb.ResponsibilityBy1", "lb.Publisher", "lb.Edition", "lb.NoOfCopyAvailable");
$sql_order_by = " ORDER BY ".$field_array[$field] . (($order==1) ? " ASC" : " DESC");

$cond = getBibliographySearchFiler($alias_LIBMS_BOOK='lb',$alias_LIBMS_BOOK_UNIQUE='lbu');

//$sql_books = "SELECT lb.BookTitle, lb.BookSubtitle, lb.CallNum, lb.CallNum2, lb.ISBN, ISBN2, lb.Language, lb.Country, lb.Introduction, " .
//			" lb.Edition, lb.PublishYear, lb.Publisher,  lb.PublishPlace,  lb.Series, lb.SeriesNum, " .
//			" lb.ResponsibilityCode1, lb.ResponsibilityBy1,  lb.ResponsibilityCode2, lb.ResponsibilityBy2, lb.ResponsibilityCode3, lb.ResponsibilityBy3, " .
//			" lb.Dimension, lb.ILL, lb.NoOfPage,  lb.BookCategoryCode, lb.CirculationTypeCode, lb.ResourcesTypeCode,  lb.Subject, lb.RemarkInternal AS BookInternalremark," .
//			" lb.OpenBorrow, lb.OpenReservation, lb.RemarkToUser, lb.BookID As Tags, lb.URL,  lbu.ACNO,  lbu.BarCode,  lbu.LocationCode," .
//			" lbu.CreationDate AS AccountDate, lbu.PurchaseDate, lbu.PurchasePrice, lbu.ListPrice, lbu.Discount, lbu.Distributor," .
//			" lbu.PurchaseByDepartment,  lbu.PurchaseNote, lbu.InvoiceNumber, lbu.AccompanyMaterial, lbu.RemarkToUser, lbu.RecordStatus, lbu.CirculationTypeCode " .
//			" FROM LIBMS_BOOK AS lb LEFT JOIN LIBMS_BOOK_UNIQUE AS lbu ON (lbu.BookID=lb.BookID AND lbu.RecordStatus<>'DELETE') " .
//			" WHERE (lb.RecordStatus<>'DELETE' OR lb.RecordStatus IS NULL) " . $cond . " ". $sql_order_by;			
//			//ORDER BY lb.ISBN, lb.CallNum, lb.CallNum2, lb.BookTitle, lbu.ACNO

$sql_books = "SELECT " . implode(',',$selFields) . 
			" FROM LIBMS_BOOK AS lb LEFT JOIN LIBMS_BOOK_UNIQUE AS lbu ON (lbu.BookID=lb.BookID AND (lbu.RecordStatus<>'DELETE' OR lbu.RecordStatus IS NULL)) " .
			" WHERE (lb.RecordStatus<>'DELETE' OR lb.RecordStatus IS NULL) " . $cond . " ". $sql_order_by;			

# get related tags
$sql = "SELECT DISTINCT bt.BookID, t.TagName FROM LIBMS_BOOK_TAG AS bt, LIBMS_TAG AS t " .
		"	WHERE bt.TagID = t.TagID ORDER BY bt.BookID, t.TagName ";
$tag_rows = $libms->returnArray($sql);
$book_tags = array();

for ($i=0; $i<sizeof($tag_rows); $i++)
{
	$recordObj = $tag_rows[$i];
	if ($recordObj["BookID"]!="" && trim($recordObj["TagName"])!="")
	{
		$book_tags[$recordObj["BookID"]][] = trim($recordObj["TagName"]);
	}
}
unset($tag_rows);


$lexport = new libexporttext();
//debug_r($rows);
//$columnStr = "lb.BookTitle, lb.BookSubtitle,  lb.CallNum, lb.CallNum2, lb.ISBN, ISBN2, lb.Language, lb.Country, lb.Introduction, " .
//		" lb.Edition, lb.PublishYear, lb.Publisher,  lb.PublishPlace,  lb.Series, lb.SeriesNum, " .
//		" lb.ResponsibilityCode1, lb.ResponsibilityBy1,  lb.ResponsibilityCode2, lb.ResponsibilityBy2, lb.ResponsibilityCode3, lb.ResponsibilityBy3, " .
//		" lb.Dimension, lb.ILL, lb.NoOfPage,  lb.BookCategoryCode, lb.CirculationTypeCode, lb.ResourcesTypeCode,  lb.Subject, BookInternalremark," .
//		" lb.OpenBorrow, lb.OpenReservation, BookRemarkToUser, Tags, lb.URL,  lbu.ACNO,  lbu.BarCode,  lbu.LocationCode," .
//		" AccountDate, lbu.PurchaseDate, lbu.PurchasePrice, lbu.ListPrice, lbu.Discount, lbu.Distributor," .
//		" lbu.PurchaseByDepartment,  lbu.PurchaseNote, lbu.InvoiceNumber, lbu.AccompanyMaterial, ItemRemarkToUser, ItemStatus, ItemCirculationTypeCode";
//$tmpArr = explode(",", $columnStr);
//for ($i=0; $i<sizeof($tmpArr); $i++)
//{
//	$exportColumn[0][] = str_replace("lbu.", "", str_replace("lb.", "", $tmpArr[$i]));
//}
//$exportColumn[1]  = $ColumsInChi;

$exportColumn[0] = $selEngCol;
$exportColumn[1] = $selChiCol;

//$limit_upper = 10000;
$limit_upper = 2500;	// reduce the memory usage in each loop
$limit_i = 0;
$max_count = 800;
$retrieve_done = false;
$rows = array();
$export_content = "";

while (!$retrieve_done && $limit_i<$max_count)
{
	$sql_limit_start = $limit_i * $limit_upper;
	
	$rows = $libms->returnArray($sql_books . " limit {$sql_limit_start}, {$limit_upper}", null, 2);	// return numeric array
	$limit_i ++;
	$retrieve_done = (sizeof($rows)<$limit_upper);	


	$result_data = array();
	for ($i=0; $i<sizeof($rows); $i++)
	{
		//if ($rows[$i]["ACNO"]!="")
		//{
//			$rows[$i][48] = $Lang["libms"]["book_status"][$rows[$i][48]];	// record status
		//}
		$rows[$i][$statusIndex] = $Lang["libms"]["book_status"][$rows[$i][$statusIndex]];	// record status
		
		$tags_found = $book_tags[$rows[$i][$tagIndex]];	// Tags
		if (is_array($tags_found) && sizeof($tags_found)>0)
		{
			$rows[$i][$tagIndex] = implode(", ", $tags_found);
		} else
		{
			$rows[$i][$tagIndex] = "";
		}
		
		$AccountDate = explode(' ',$rows[$i][$accountDateIndex]);	// AccountDate
		$rows[$i][$accountDateIndex]= $AccountDate[0];
		
		$result_data[] = $rows[$i];
	}
	unset($rows);
	$rows = array();

	$export_content .= ($export_content?"\r\n":"").$lexport->GET_EXPORT_TXT($result_data, $exportColumn, "\t", "\r\n", "\t", 0, '11');
	
	$exportColumn = array();	// pass column header for the 1st time only
}


intranet_closedb();

$filename = "book_list_".date("Ymd").".csv";

$lexport->EXPORT_FILE($filename, $export_content, false, false, "UTF-8");
?>