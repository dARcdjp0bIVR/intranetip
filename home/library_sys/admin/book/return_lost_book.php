<?php
/*
 *  2020-04-28 Cameron
 *      - create this file
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


####### Trim all request #######
function recursive_trim(&$array){
    foreach($array as &$item){
        if (is_array($item)){
            recursive_trim($item);
        }else{
            if (is_string($item))
                $item=trim($item);
        }
    }
}
recursive_trim($_POST);
####### END  OF   Trim all request #######

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/RecordManager.php");


intranet_auth();
intranet_opendb();


# in all boook management PHP pages
$libms = new liblms();
$RecordManager = new RecordManager($libms);
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

if(sizeof($_POST)==0) {
    header("Location: lost_book_list.php?xmsg=UpdateUnsuccess");
    exit;
}

$ret = array();
$error = array();
foreach((array)$OverDueLogID as $_OverDueLogID) {
    $overdueBookInfo = $libms->getOverdueBookInfo($_OverDueLogID);
    if (count($overdueBookInfo)) {
        $overdueBookInfo = current($overdueBookInfo);
        $uID = $overdueBookInfo['UserID'];
        $bookUniqueID = $overdueBookInfo['UniqueID'];
        $paymentReceived = $overdueBookInfo['PaymentReceived'];
        $recordStatus = $overdueBookInfo['RecordStatus'];
        $borrowLogID = $overdueBookInfo['BorrowLogID'];
        if (!$sys_custom['eLibraryPlus']['PayBy_ePayment']) {
            $User = new User($uID, $libms);
            if (($paymentReceived > 0) && ($recordStatus != 'WAIVED')) {
                $cancelResult = $User->cancel_overdue($paymentReceived, $_OverDueLogID);
                if (!$cancelResult) {
                    $error[] = "paymentReceived: $paymentReceived, OverDueLogID: $_OverDueLogID";
                }
                $ret[] = $cancelResult;
            }
            
            // get all overdue log of the same borrowlog except the one for 'LOST' book
            $overdueLogInfo = $libms->getOverdueInfoByBorrowLogID($borrowLogID, $excludeOverdueLogID=$_OverDueLogID);
            foreach((array)$overdueLogInfo as $_overdueLogInfo) {
                $__OverDueLogID = $_overdueLogInfo['OverDueLogID'];
                $__PaymentReceived = $_overdueLogInfo['PaymentReceived'];
                $cancelResult = $User->cancel_overdue($__PaymentReceived, $__OverDueLogID);
                if (!$cancelResult) {
                    $error[] = "paymentReceived: $__PaymentReceived, OverDueLogID: $__OverDueLogID";
                }
                $ret[] = $cancelResult;
            }
        }
        $result = $RecordManager->returnBook($bookUniqueID, 'return_book', $Mode="ByBatch", $uID, $returnDateTime='', $HandleOverdue= 1, $jsonError=false);
        extract($result);
        $ret[] = $returnResult;
        if (!$returnResult) {
            $error[] = "return book: $bookUniqueID, UserID: $uID";
        }
    }
}

intranet_closedb();

// if(count($error)) {
//     debug_pr($error);
// }
// else {
$xmsg = in_array(false,$ret) ? 'UpdateUnsuccess' : 'UpdateSuccess';
header("Location: lost_book_list.php?xmsg=$xmsg");
//}

?>