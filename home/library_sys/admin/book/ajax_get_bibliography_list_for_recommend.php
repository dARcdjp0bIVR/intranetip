<?php
/*
 * 	modifying: 
 * 	Log
 * 	Purpose:	show list of bibliography by search criteria
 *	@return:	array['layout']		-- listview of result
 *				array['result']		-- number of record 
 * 
 * 	Date:	2016-11-14 [Cameron] 
 * 			- Add filter condition "b.`NoOfCopy`>0 AND b.`OpenBorrow`=1" (same effect as INTRANET_ELIB_BOOK.Publish=1) in sql
 * 			- return -1 for no record found
 * 			- Use INTRANET_ELIB_BOOK.BookID for BookID
 *  	 
 * 	Date:	2016-09-22 [Cameron] create this file
 * 
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."home/library_sys/admin/book/batch_modify_manage.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
$physical_book_init_value = $lelibplus->physical_book_init_value ? $lelibplus->physical_book_init_value : 10000000;
$sql = "SELECT BookID FROM INTRANET_ELIB_BOOK_RECOMMEND WHERE BookID>".$physical_book_init_value. " ORDER BY BookID";
$recommend_list = $lelibplus->returnVector($sql);

$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$task = $_REQUEST['task'];
$bookID = $_REQUEST['bookID'];

$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;
$sort = ($order == 1) ? "ASC" : "DESC";
$orderByField = $field + 1;

$bmm = new batch_modify_manage();
$bmm->setfield($field);
$bmm->setorder($order);

if ($task == 'sorting') {
	
	if ($bookID) {
		$cond = " AND b.BookID IN ('" .str_replace(',', "','",$bookID) . "')";
	}
	else {
		$cond = '';
	}	 	
}
else {
	$cond = $bmm->getBatchUpdateSearchFilter($SrcFrom="Book");
}

$sql = "SELECT 
			IF(b.BookTitle IS NULL OR b.BookTitle = '','".$Lang['General']['EmptySymbol']."',b.BookTitle) AS BookTitle,
			IF((b.CallNum='' OR b.CallNum is null) AND (b.CallNum2='' OR b.CallNum2 is null),'".$Lang['General']['EmptySymbol']."',
				CONCAT(IF(b.CallNum IS NULL,'',b.CallNum), ' ', IF(b.CallNum2 IS NULL,'',b.CallNum2))) as CallNumber,
			b.BookID + ".$physical_book_init_value." AS BookID
		FROM 
			LIBMS_BOOK as b 
		LEFT JOIN LIBMS_BOOK_UNIQUE as bu ON bu.BookID=b.BookID
		WHERE b.`NoOfCopy`>0 AND b.`OpenBorrow`=1 
			{$cond}
		GROUP BY b.BookID
		ORDER BY ". $orderByField. " " . $sort;

$rs = $libms->returnResultSet($sql);
$nrRs = count($rs);

 
$x = '<table id="book_item_table" class="common_table_list_v30  view_table_list_v30" style="width:98%">
			<tr>
				<th width="1">#</th>';
				
$pos = 0;						
$x .= '			<th width="75%">'.$bmm->column($pos++, $Lang["libms"]["book"]["title"]).'</th>
				<th width="20%">'.$bmm->column($pos++, $Lang["libms"]["book"]["call_number"]).'</th>				
				<th class="num_check" width="1%"><input type="checkbox" id="checkall" name="checkall" onclick="Set_Checkbox_Value(\'BookID[]\',this.checked)" checked /></th>
			</tr>';
			
$BookID = array();
$ret = array();

if($nrRs>0){		
	for ($i=0; $i<$nrRs; $i++){
		$curr_rs = $rs[$i];
		$bookTitle = in_array($curr_rs['BookID'],(array)$recommend_list) ? $curr_rs['BookTitle']."<span class=\"tabletextrequire\"> * </span>" : $curr_rs['BookTitle'];
		$x .= '<tr id="'.$curr_rs['BookID'].'">
					<td>'.($i+1).'</td>
					<td>'.$bookTitle.'</td>
					<td>'.$curr_rs['CallNumber'].'</td>
					<td><input type="checkbox" id="BookID[]" name="BookID[]" value="' . $curr_rs['BookID'] . '" checked /></td>
				</tr>';

	}
	$ret['result'] = 1;
}
else{
	 # no record
	$x.="<tr id='no_record_row' class=\"tablebluerow2 tabletext\">
			<td class=\"tabletext\" colspan=\"4\" align=\"center\" height=\"40\" style=\"vertical-align:middle\">$i_no_record_exists_msg</td>
		</tr>";
	$ret['result'] = -1;
}
$x .= '</table>';

$ret['layout'] = remove_dummy_chars_for_json($x);
$json = new JSON_obj();
echo $json->encode($ret);

intranet_closedb();
?>