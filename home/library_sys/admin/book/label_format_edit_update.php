<?php

//Modifying by 

/********************
 * Log :
 * 
 * Date		2017-11-14 [Henry]
 * 			Add text_alignment selection box [case #E130799]
 * 
 * Date		2017-03-01 [Cameron]
 * 				- add is_bold font checkbox option
 * 
 * Date		2013-10-31 [Henry]
 * 			
 * 
 ********************/
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";
####### Trim all request #######
function recursive_trim(&$array){
	foreach($array as &$item){
		if (is_array($item)){
			recursive_trim($item);
		}else{
			if (is_string($item))
				$item=trim($item);
		}
	}
}
recursive_trim($_REQUEST);
####### END  OF   Trim all request #######
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
/*
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}
//*/

if($Code=="") {
	header("Location: label_format.php");	
}

$Code = intranet_htmlspecialchars($Code);
$dataAry['name'] = intranet_htmlspecialchars($name);
$dataAry['paper-size-width'] = intranet_htmlspecialchars($paper_size_width);
$dataAry['paper-size-height'] = intranet_htmlspecialchars($paper_size_height);
$dataAry['metric'] = intranet_htmlspecialchars($metric);
$dataAry['lMargin'] = intranet_htmlspecialchars($lMargin);
$dataAry['tMargin'] = intranet_htmlspecialchars($tMargin);
$dataAry['NX'] = intranet_htmlspecialchars($NX);
$dataAry['NY'] = intranet_htmlspecialchars($NY);
$dataAry['SpaceX'] = intranet_htmlspecialchars($SpaceX);
$dataAry['SpaceY'] = intranet_htmlspecialchars($SpaceY);
$dataAry['width'] = intranet_htmlspecialchars($width);
$dataAry['height'] = intranet_htmlspecialchars($height);
$dataAry['font-size'] = intranet_htmlspecialchars($font_size);
$dataAry['is_bold'] = $is_bold;
$dataAry['text_alignment'] = $text_alignment;
$dataAry['lineHeight'] = intranet_htmlspecialchars($lineHeight);
$dataAry['printing_margin_h'] = intranet_htmlspecialchars($printing_margin_h);
$dataAry['printing_margin_v'] = intranet_htmlspecialchars($printing_margin_v);
$dataAry['max_barcode_width'] = intranet_htmlspecialchars($max_barcode_width);
$dataAry['max_barcode_height'] = intranet_htmlspecialchars($max_barcode_height);
//Henry Added 20131031
$dataAry['info_order_1'] = intranet_htmlspecialchars($info_order_1);
$dataAry['info_order_2'] = intranet_htmlspecialchars($info_order_2);
$dataAry['info_order_3'] = intranet_htmlspecialchars($info_order_3);
$dataAry['info_order_4'] = intranet_htmlspecialchars($info_order_4);
$dataAry['info_order_5'] = intranet_htmlspecialchars($info_order_5);
$dataAry['info_order_6'] = intranet_htmlspecialchars($info_order_6);

$result = $libms->UPDATE_LABEL_FORMAT($Code, $dataAry);

intranet_closedb();

header("Location: label_format.php?xmsg=update");

?>
