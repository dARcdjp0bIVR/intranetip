<?php
/*
 * 	Using: 	
 * 	Log
 * 
 * Date:    2020-05-15 (Cameron)
 *          - fix to allow export books which book item RecordStatus is null [case #E143600]
 *          
 * 	Date:	2015-10-12 (Cameron)
 * 			- retrieve all DescriptionChi of Responsibility once and loop up it by array to avoid running sql query within the loop
 * 			save vast time and memory!
 * 			- apply html_entity_decode to these fields: CallNum2, Publisher, Distributor, PurchaseByDepartment,
 * 			  PurchaseNote, InvoiceNumber, Subject
 *
 *	Date:	2015-10-09 (Cameron)
 *			- append data to temp file on each loop and read it as text file for final output  
 *			- retrieve each row record in associative array only to save memory
 *
 *	Date:	2015-10-08 (Cameron)
 *	Details:	- set memory to unlimited
 *
 * 	Date:	2015-08-20 (Cameron)
 * 	Details:	- fix bug on $sql_limit_start, not need to deduct 1 from $limit_upper when multiply loop counter $limit_i    			 
 * 
 * 	Date: 	2015-04-01 (Cameron)
 * 	Details:	fix bug of "export fail after sort by call number", use "lb.CallNum, lb.CallNum2" rather than "CallNumberMixed"
 * 
 * 	Date:	2014-12-04 (Cameron)
 * 	Details:	move search filter part to function in book_export_common.php 
 */
 
@SET_TIME_LIMIT(216000);
//ini_set('memory_limit','256M');
ini_set('memory_limit',-1);	// no memory limit
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."home/library_sys/admin/book/book_export_common.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();

# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
function switch_recordstatus($RecordStatus){
//	$statuskey = array();
	$statuskey = '';
	switch ($RecordStatus) 
	{
		case "LOST": 
			$statuskey = 'm' ;
			break;
		case "SHELVING": 
			$statuskey = 'p' ;
			break;
		case "REPAIRING": 
			$statuskey = 'r' ;
			break;
		case "RESERVED": 
			$statuskey = 'r' ;
			break;
		case "RESTRICTED": 
			$statuskey = 'd' ;
			break;
		case "SUSPECTEDMISSING": 
			$statuskey = 's' ;
			break;
		case "WRITEOFF": 
			$statuskey = 'x' ;
			break;		
		case "BORROWED":
			$statuskey = 'l' ;
			break;
		case "NORMAL":
			$statuskey = 'a' ;
			break;
	}
    return $statuskey;
}

//function return_descChi_by_code($code){
//	global $libms;
//	$sql = "SELECT DescriptionChi FROM LIBMS_RESPONSIBILITY where ResponsibilityCode  = '".$code."'" ;
//	$result = $libms->returnArray($sql);
//	return $result[0][0];
//}

function getResponsibilityChi(){
	global $libms, $AllResponseArr;
	if (is_array($AllResponseArr) && count($AllResponseArr) == 0) {
		$sql = "SELECT ResponsibilityCode, DescriptionChi FROM LIBMS_RESPONSIBILITY ORDER BY ResponsibilityCode" ;
		
		$rs = $libms->returnArray($sql, null, 1);	// associate array
		for ($i=0, $iMax=count($rs);$i<$iMax;$i++) {
			$AllResponseArr[$rs[$i]['ResponsibilityCode']] = $rs[$i]['DescriptionChi'];
		}
	}
}


function getBookTags() {
	global $libms;
	
	# get related tags
	$sql = "SELECT DISTINCT bt.BookID, t.TagName FROM LIBMS_BOOK_TAG AS bt, LIBMS_TAG AS t " .
			"	WHERE bt.TagID = t.TagID ORDER BY bt.BookID, t.TagName ";
	$tag_rows = $libms->returnArray($sql);
	$book_tags = array();
	for ($i=0,$iMax=count($tag_rows); $i<$iMax; $i++)
	{
		$recordObj = $tag_rows[$i];
		if ($recordObj["BookID"]!="" && trim($recordObj["TagName"])!="")
		{
			$book_tags[$recordObj["BookID"]][] = trim($recordObj["TagName"]);
		}
	}
	unset($tag_rows);
	return $book_tags;
}

function write_export_file($sql,$sql_limit_start,$limit_upper) {
	global $libms, $fd, $book_tags, $AllResponseArr;
	
	$result_data = '';
	$rows = array();

	$rows = $libms->returnArray($sql . " limit {$sql_limit_start}, {$limit_upper}",null,1);		// associate array only

	$retrieve_done = (sizeof($rows)<$limit_upper);
	
	for ($i=0,$iMax=count($rows); $i<$iMax; $i++)
	{		
		$rows[$i]["ISBN"]=trim(html_entity_decode($rows[$i]["ISBN"]));
		$rows[$i]["ISBN2"]=trim(html_entity_decode($rows[$i]["ISBN2"]));
		$rows[$i]["CallNum"]=trim(html_entity_decode($rows[$i]["CallNum"]));
		$rows[$i]["CallNum2"]=trim(html_entity_decode($rows[$i]["CallNum2"]));
		$rows[$i]["ResponsibilityBy1"]=trim(html_entity_decode($rows[$i]["ResponsibilityBy1"]));
		$rows[$i]["ResponsibilityCode1"]=trim(html_entity_decode($rows[$i]["ResponsibilityCode1"]));
//		$responsibilityDescChi1 = return_descChi_by_code($rows[$i]["ResponsibilityCode1"]);
		$responsibilityDescChi1 = $AllResponseArr[$rows[$i]["ResponsibilityCode1"]];
		$rows[$i]["ResponsibilityBy2"]=trim(html_entity_decode($rows[$i]["ResponsibilityBy2"]));
		$rows[$i]["ResponsibilityCode2"]=trim(html_entity_decode($rows[$i]["ResponsibilityCode2"]));
//		$responsibilityDescChi2 = return_descChi_by_code($rows[$i]["ResponsibilityCode2"]);
		$responsibilityDescChi2 = $AllResponseArr[$rows[$i]["ResponsibilityCode2"]];
		$rows[$i]["ResponsibilityBy3"]=trim(html_entity_decode($rows[$i]["ResponsibilityBy3"]));
		$rows[$i]["ResponsibilityCode3"]=trim(html_entity_decode($rows[$i]["ResponsibilityCode3"]));
//		$responsibilityDescChi3 = return_descChi_by_code($rows[$i]["ResponsibilityCode3"]);		
		$responsibilityDescChi3 = $AllResponseArr[$rows[$i]["ResponsibilityCode3"]];		
		$rows[$i]["Publisher"]=trim(html_entity_decode($rows[$i]["Publisher"]));
		$rows[$i]["PublishPlace"]=trim(html_entity_decode($rows[$i]["PublishPlace"]));
		$rows[$i]["BookTitle"]=trim(html_entity_decode($rows[$i]["BookTitle"]));
		$rows[$i]["BookSubtitle"]=trim(html_entity_decode($rows[$i]["BookSubtitle"]));
		$rows[$i]["Series"]=trim(html_entity_decode($rows[$i]["Series"]));
		$rows[$i]["Distributor"]=trim(html_entity_decode($rows[$i]["Distributor"]));
		$rows[$i]["PurchaseByDepartment"]=trim(html_entity_decode($rows[$i]["PurchaseByDepartment"]));
		$rows[$i]["PurchaseNote"]=trim(html_entity_decode($rows[$i]["PurchaseNote"]));
		$rows[$i]["InvoiceNumber"]=trim(html_entity_decode($rows[$i]["InvoiceNumber"]));
		$rows[$i]["Subject"]=trim(html_entity_decode($rows[$i]["Subject"]));

		$tags_found = $book_tags[$rows[$i]["Tags"]];
		if (is_array($tags_found) && sizeof($tags_found)>0)
		{
			$rows[$i]["Tags"] = $tags_found;
		} else
		{
			$rows[$i]["Tags"] = "";
		}
		unset($tags_found);
		
		if($rows[$i]["ResourcesTypeCode"]!=""||$rows[$i]["Country"]!=""||$rows[$i]["Language"]!=""){
			if($rows[$i]["ResourcesTypeCode"]!=""){
				$result_data .= $rows[$i]["ResourcesTypeCode"]." ";
			}
			else{
				$result_data .= "XX ";
			}
	
			if($rows[$i]["Country"]!=""){
				$result_data .= $rows[$i]["Country"]." ";
			}
			else{
				$result_data .= "XX  ";
			}
	
			if($rows[$i]["Language"]!=""){
				$result_data .= $rows[$i]["Language"]." ";
			}
			else{
				$result_data .= "XXX";
			}
			$result_data .= "\r\n";
		}
	
	
		if($rows[$i]["ISBN"]!=""){
			$result_data .= "020    ".$rows[$i]["ISBN"]."\r\n";
		}
	
		if($rows[$i]["ISBN2"]!=""){
			$result_data .= "020    ".$rows[$i]["ISBN2"]."\r\n";
		}
	
		if($rows[$i]["CallNum"]!=""){
			$result_data .= "090    ".$rows[$i]["CallNum"]."";
			if($rows[$i]["CallNum2"]!=""){
				$result_data .= "|b".$rows[$i]["CallNum2"]."";
			}
			$result_data .= "\r\n";
	
		}
	
	
		if($rows[$i]["ResponsibilityBy1"]!=""){
			$result_data .= "100    ".$rows[$i]["ResponsibilityBy1"]."\r\n";
		}
	
		if($rows[$i]["BookTitle"]!=""){
			$result_data .= "245 10 ".$rows[$i]["BookTitle"]."";
			if($rows[$i]["BookSubtitle"]!=""){
				$result_data .= " :|b".$rows[$i]["BookSubtitle"]."";
			}
			 
			if($rows[$i]["ResponsibilityBy1"]!=""){
				if($responsibilityDescChi1!=""){
					$result_data .= " /|c".$rows[$i]["ResponsibilityBy1"]." ".$responsibilityDescChi1."";
				}
				else{
					$result_data .= " /|c".$rows[$i]["ResponsibilityBy1"]."";
				}
			}
	
			if($rows[$i]["ResponsibilityBy2"]!=""){
				if($rows[$i]["ResponsibilityBy1"]!=""){
					if($responsibilityDescChi2!=""){
						$result_data .= " ; ".$rows[$i]["ResponsibilityBy2"]." ".$responsibilityDescChi2."";
					}
					else{
						$result_data .= " ; ".$rows[$i]["ResponsibilityBy2"]."";
					}
				}
				else{
					if($responsibilityDescChi2!=""){
						$result_data .= " /|c".$rows[$i]["ResponsibilityBy2"]." ".$responsibilityDescChi2."";
					}
					else{
						$result_data .= " /|c".$rows[$i]["ResponsibilityBy2"]."";
					}
				}
			}
			if($rows[$i]["ResponsibilityBy3"]!=""){
				if($rows[$i]["ResponsibilityBy1"]!=""||$rows[$i]["ResponsibilityBy2"]!=""){
					if($responsibilityDescChi3!=""){
						$result_data .= " ; ".$rows[$i]["ResponsibilityBy3"]." ".$responsibilityDescChi3."";
					}
					else{
						$result_data .= " ; ".$rows[$i]["ResponsibilityBy3"]."";
					}
				}
				else{
					if($responsibilityDescChi3!=""){
						$result_data .= " /|c".$rows[$i]["ResponsibilityBy3"]." ".$responsibilityDescChi3."";
					}
					else{
						$result_data .= " /|c".$rows[$i]["ResponsibilityBy3"]."";
					}
				}
			}
			$result_data .= "\r\n";
		}
	
		if($rows[$i]["Edition"]!=""){
			$result_data .= "250    ".$rows[$i]["Edition"]."\r\n";
		}
	
		if($rows[$i]["PublishPlace"]!=""||$rows[$i]["Publisher"]!=""||$rows[$i]["PublishYear"]!=""){
			$result_data .= "260    ";
			if($rows[$i]["PublishPlace"]!=""){
				$result_data .= $rows[$i]["PublishPlace"];
			}
			if($rows[$i]["Publisher"]!=""){
				if($rows[$i]["PublishPlace"]!="")
				{
					$result_data .= " :|b".$rows[$i]["Publisher"]."";
				}
				else{
					$result_data .= "|b".$rows[$i]["Publisher"]."";
				}
			}
			 
			if($rows[$i]["PublishYear"]!="")
			{
				if($rows[$i]["PublishPlace"]!=""||$rows[$i]["Publisher"]!=""){
					$result_data .= ",|c".$rows[$i]["PublishYear"]."";
				}
				else{
					$result_data .= "|c".$rows[$i]["PublishYear"]."";
				}
			}
			$result_data .= "\r\n";
		}
	
		// if($rows[$i]["NoOfPage"]!=""){
		// $result_data .= "300    ".$rows[$i]["NoOfPage"]."\r\n";
		// }
	
		if($rows[$i]["NoOfPage"]!=""||$rows[$i]["Dimension"]!=""||$rows[$i]["ILL"]!=""||$rows[$i]["AccompanyMaterial"]!=""){
			$result_data .= "300    ";
			if($rows[$i]["NoOfPage"]!=""){
				$result_data .= $rows[$i]["NoOfPage"];
			}
			if($rows[$i]["ILL"]!=""){
				if($rows[$i]["NoOfPage"]!="")
				{
					$result_data .= " :|b".$rows[$i]["ILL"]."";
				}
				else{
					$result_data .= "|b".$rows[$i]["ILL"]."";
				}
			}
			if($rows[$i]["Dimension"]!="")
			{
				if($rows[$i]["NoOfPage"]!=""||$rows[$i]["ILL"]!=""){
					$result_data .= " ;|c".$rows[$i]["Dimension"]."";
				}
				else{
					$result_data .= "|c".$rows[$i]["Dimension"]."";
				}
			}
			if($rows[$i]["AccompanyMaterial"]!="")
			{
				if($rows[$i]["NoOfPage"]!=""||$rows[$i]["Dimension"]!=""||$rows[$i]["ILL"]!=""){
					$result_data .= " +|e".$rows[$i]["AccompanyMaterial"]."";
				}
				else{
					$result_data .= "|e".$rows[$i]["AccompanyMaterial"]."";
				}
			}
			$result_data .= "\r\n";
		}
	
		if($rows[$i]["Series"]!=""){
			$result_data .= "490 0  ".$rows[$i]["Series"]."";
			if($rows[$i]["SeriesNum"]!=""){
				$result_data .=  " ;|v".$rows[$i]["SeriesNum"]."";
			}
	
			$result_data .= "\r\n";
		}
	
		if($rows[$i]["Introduction"]!=""){
			$result_data .= "500    ".$rows[$i]["Introduction"]."\r\n";
		}
	
		if($rows[$i]["Tags"]!=""){
			for($j=0;$j<count($rows[$i]["Tags"]);$j++){
				if($rows[$i]["Tags"][$j]!=$rows[$i]["Subject"]){
					$result_data .= "650    ".$rows[$i]["Tags"][$j]."\r\n";
				}
			}
		}
		
		if($rows[$i]["Subject"]!=""){
			$result_data .= "650    ".$rows[$i]["Subject"]."\r\n";
			$result_data .= "690    ".$rows[$i]["Subject"]."\r\n";
		}
	
		if($rows[$i]["ResponsibilityBy2"]!=""){
			$result_data .= "700    ".$rows[$i]["ResponsibilityBy2"]."\r\n";
		}
	
		if($rows[$i]["ResponsibilityBy3"]!=""){
			$result_data .= "700    ".$rows[$i]["ResponsibilityBy3"]."\r\n";
		}
	
		if($rows[$i]["ACNO"]!=""){
			$result_data .= "991    |a".$rows[$i]["ACNO"]."";
			if($rows[$i]["BarCode"]!=""){
				$result_data .= "|b".$rows[$i]["BarCode"]."";
			}
			if($rows[$i]["PurchaseDate"]!=""){
				$result_data .= "|c".$rows[$i]["PurchaseDate"]."";
			}
			if($rows[$i]["LocationCode"]!=""){
				$result_data .= "|d".$rows[$i]["LocationCode"]."";
			}
			if($rows[$i]["InvoiceNumber"]!=""){
				$result_data .= "|i".$rows[$i]["InvoiceNumber"]."";
			}
			if($rows[$i]["Discount"]!=""){
				$result_data .= "|n".$rows[$i]["Discount"]."";
			}
			if($rows[$i]["Distributor"]!=""){
				$result_data .= "|o".$rows[$i]["Distributor"]."";
			}
			if($rows[$i]["PurchasePrice"]!=""){
				$result_data .= "|p".$rows[$i]["PurchasePrice"]."";
			}
			if($rows[$i]["RecordStatus"]!=""){
				$result_data .= "|s".switch_recordstatus($rows[$i]["RecordStatus"])."";
			}
			if($rows[$i]["PurchaseByDepartment"]!=""){
				$result_data .= "|e".$rows[$i]["PurchaseByDepartment"]."";
			}
			if($rows[$i]["ListPrice"]!=""){
				$result_data .= "|g".$rows[$i]["ListPrice"]."";
			}
			if($rows[$i]["PurchaseNote"]!=""){
				$result_data .= "|h".$rows[$i]["PurchaseNote"]."";
			}
			if($rows[$i]["ItemSeriesNum"]!=""){
				$result_data .= "|v".$rows[$i]["ItemSeriesNum"]."";
			}
			if($rows[$i]["AccountDate"]!=""){
				$result_data .= "|w".$rows[$i]["AccountDate"]."";
			}
			if($rows[$i]["ResourcesTypeCode"]!=""){
				$result_data .= "|t".$rows[$i]["CirculationTypeCode"]."";
			}
	
			$result_data .= "\r\n";
		}
		$result_data.="\r\n";
	}
	
	unset($rows);
	unset($book_tags);
	unset($responsibilityDescChi1);
	unset($responsibilityDescChi2);
	unset($responsibilityDescChi3);
	
	if ($fd) {
		fwrite($fd, $result_data);
	}
	unset($result_data);
	
	return $retrieve_done;
}




###########################################################
## start main program

$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;
$field_array = array("lb.ISBN", "lb.CallNum, lb.CallNum2","lb.BookTitle", "lb.ResponsibilityBy1", "lb.Publisher", "lb.Edition", "lb.NoOfCopyAvailable");
$sql_order_by = " ORDER BY ".$field_array[$field] . (($order==1) ? " ASC" : " DESC");


$cond = getBibliographySearchFiler($alias_LIBMS_BOOK='lb',$alias_LIBMS_BOOK_UNIQUE='lbu');


//concat(lb.BookID, ' - ', lb.BookTitle)
$sql = "SELECT lb.BookTitle, lb.BookSubtitle,  lb.CallNum, lb.CallNum2, lb.ISBN, ISBN2, lb.Language, lb.Country, lb.Introduction, " .
		" lb.Edition, lb.PublishYear, lb.Publisher,  lb.PublishPlace,  lb.Series, lb.SeriesNum, " .
		" lb.ResponsibilityCode1, lb.ResponsibilityBy1, lb.ResponsibilityCode2, lb.ResponsibilityBy2, lb.ResponsibilityCode3, lb.ResponsibilityBy3, " .
		" lb.NoOfPage,  lb.BookCategoryCode,lb.ResourcesTypeCode,  lb.Subject, lb.RemarkInternal AS BookInternalremark,lb.Dimension,lb.ILL," .
		" lb.OpenBorrow, lb.OpenReservation, lb.RemarkToUser, lb.BookID As Tags, lb.URL,  lbu.ACNO,  lbu.BarCode,  lbu.LocationCode," .
		" DATE_FORMAT(lbu.CreationDate,'%Y%m%d') AS AccountDate,DATE_FORMAT(lbu.PurchaseDate,'%Y%m%d') as PurchaseDate, lbu.PurchasePrice, lbu.ListPrice, lbu.Discount, lbu.Distributor," .
		" lbu.PurchaseByDepartment,  lbu.PurchaseNote, lbu.InvoiceNumber,lbu.AccompanyMaterial, lbu.RecordStatus, lbu.ItemSeriesNum, lbu.CirculationTypeCode" .
		" FROM LIBMS_BOOK AS lb LEFT JOIN LIBMS_BOOK_UNIQUE AS lbu ON (lbu.BookID=lb.BookID AND (lbu.RecordStatus<>'DELETE' OR lbu.RecordStatus IS NULL)) " .
		" WHERE (lb.RecordStatus<>'DELETE' OR lb.RecordStatus IS NULL) " . $cond . " ". $sql_order_by;
		//ORDER BY lb.ISBN, lb.CallNum, lb.CallNum2, lb.BookTitle, lbu.ACNO
//$rows = $libms->returnArray($sql);

$limit_upper = 10000;
$limit_i = 0;
$max_count = 200;
$retrieve_done = false;
$rows = array();
$result_data = "";

$lf = new libfilesystem();
$user_dir = $intranet_root."/file/temp/book_export_marc21/".$UserID;
if (!file_exists($user_dir)) {
	$lf->folder_new($user_dir);
}
$lf->clearTempFiles("temp/book_export_marc21/".$UserID, $timeout="1");		// clear all previous files in the folder

$export_file = $user_dir . "/marc".date("YmdHis"); 

clearstatcache();
$fd = fopen($export_file, "a");		// file open once outside of the loop will reduce the i/o time and memory

$book_tags = getBookTags();

$AllResponseArr = array();
getResponsibilityChi();

while (!$retrieve_done && $limit_i<$max_count)
{
	$sql_limit_start = $limit_i * $limit_upper;
	$limit_i++;
	
	$retrieve_done = write_export_file($sql,$sql_limit_start,$limit_upper);
}
	
fclose ($fd);

$filename = "book_list_marc21_".date("Ymd").".txt";

intranet_closedb();
//export with the txt format 
if (stristr($_SERVER['HTTP_USER_AGENT'],'MSIE') !== FALSE)  $filename = urlencode($filename);

header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

$result_data = $lf->file_read($export_file);
   
if(!$ignoreLength){ 
   header('Content-Length: '.strlen($result_data));
}
			
header('Content-Disposition: attachment; filename="'.$filename.'";');
print $result_data;
?>