<?php
# using: 
/*
 * Modification Log:
 *
 *  2019-08-19 (Cameron)
 *      - add checking Barcode for uniqueness if it's specified as customized [case #F165714]
 *
 *	2019-07-23 (Henry)
 *		- use the local library of jquery
 *
 * 	2015-05-12 (Cameron)
 * 		- fix Cancel button return page to book_item_list.php rather than item_list.php because the later is old file,
 * 		- also fix $PAGE_NAVIGATION for this linking
 * 
 * 	2015-03-23 (Cameron)
 * 		- change $('#BookTitle').change function, also set circulation type when get BookID
 * 		- show items actual circulation type if it's the same as book ("follow bibliography")
 * 
 *	2015-03-10 (Henry) 
 *		- introduce circulation type field
 *  
 *  2014-09-16 (Henry)
 *      - Add field AccompanyMaterial and RemarkToUser
 * 
 *  2014-09-16 (Tiffany)
 *      - Add field ItemSeriesNum
 *  
 * 2013-09-11 (Jason)
 * 		- improve to show the suggested list of ACNO, Distributor by a new js jquery.inputselect.js
 */

// error_reporting(E_ERROR | E_WARNING | E_PARSE);
// ini_set('display_errors', 1);

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_periodical.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

$home_header_no_EmulateIE7 = true;

intranet_auth();
intranet_opendb();
############################################################################################################

## Get Data
$UniqueID = (is_array($UniqueID)) ? $UniqueID[0] : $UniqueID;
$IsCopy = (isset($IsCopy) && $IsCopy != '') ? $IsCopy : '';
$BookID = (isset($BookID) && $BookID != '') ? $BookID : '';



## Use Library
# in all boook management PHP pages
$libms = new liblms();
$libms_periodical = new liblms_periodical();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

# Access Checking
if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();		// "libms.html"


## Init 
$BookInfo = array();
$ItemInfo = array();


## Preparation
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagement";

$libms->MODULE_AUTHENTICATION($CurrentPage);


$TAGS_OBJ[] = array($Lang['libms']['action']['edit_item']);
$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['item_list'], "book_item_list.php");
if (isset($IsCopy) && $IsCopy == 1)
{
	# for copy item
	$PAGE_NAVIGATION[] = array($Lang['Btn']['Copy'], "");
	$IsNew = true;
} else
{
	$IsNew = ($UniqueID == '') ? true : false;
	
	if($IsNew){
		# for new item
		$PAGE_NAVIGATION[] = array($Lang['libms']['general']['new'], "");
	} else {
		# for edit item
		$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
	}
}
$CurrentPage = "PageBookManagementItemList";

$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

#Save and Cancel Button ...........
$SaveBtn   = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "SubmitBtn", "");
if($IsNew && !$FromPeriocial){
	$SaveBtn2  = $linterface->GET_ACTION_BTN($Lang['libms']['book']['btn']['SubmitAndAddItem'] , "button", "submit2()", "SubmitBtn2", "");
}
$CancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='book_item_list.php'", "CancelBtn", "");

############################################################################################################

## Default 
$ItemStatus = 'NORMAL';

## Get Book Item Info if any
if($UniqueID != ''){
	$ItemInfo = $libms->GET_BOOK_ITEM_INFO($UniqueID);
	
	if(count($ItemInfo) > 0){
		$BookID = $ItemInfo[0]['BookID'];
		if($IsCopy != 1){
			$ACNO = $ItemInfo[0]['ACNO'];
			$BarCode = $ItemInfo[0]['BarCode'];
		}
		$ItemStatus = $ItemInfo[0]['RecordStatus'];
		$Distributor = $ItemInfo[0]['Distributor'];
		$Discount = $ItemInfo[0]['Discount'];
		$ListPrice = $ItemInfo[0]['ListPrice'];
		$PurchaseDate = $ItemInfo[0]['PurchaseDate'];
		$PurchasePrice = $ItemInfo[0]['PurchasePrice'];
		$PurchaseNote = $ItemInfo[0]['PurchaseNote'];
		$PurchaseByDepartment = $ItemInfo[0]['PurchaseByDepartment'];
		$LocationCode = $ItemInfo[0]['LocationCode'];
		$InvoiceNumber = $ItemInfo[0]['InvoiceNumber'];
		$AccountDate = $ItemInfo[0]['CreationDate'];
		$ItemSeriesNum = $ItemInfo[0]['ItemSeriesNum'];
		
		$AccompanyMaterial = $ItemInfo[0]['AccompanyMaterial'];
		$RemarkToUser = $ItemInfo[0]['RemarkToUser'];
		$CirculationTypeCode = $ItemInfo[0]['CirculationTypeCode'];
	}
}

$BookInfo = $libms->GET_BOOK_INFO($BookID);
$nrBookInfo = count($BookInfo);
$BookTitle = ($nrBookInfo > 0) ? $BookInfo[0]['BookTitle'] : '';

$defaultCirculationTypeDesc = $Lang["libms"]["bookmanagement"]["follow_parent"];
$defaultBookCirculationTypeDesc = $Lang["libms"]["bookmanagement"]["unclassified"];

if ($nrBookInfo > 0) {
	$bookCirculationTypeCode = $BookInfo[0]['CirculationTypeCode'];
	if (!empty($bookCirculationTypeCode)) {
		$bookCirculationTypeInfo = $libms->GET_CIRCULATION_TYPE_INFO($bookCirculationTypeCode);
		if (count($bookCirculationTypeInfo) >0 ) {
			$bookCirculationTypeDesc = $bookCirculationTypeInfo[0][$Lang['libms']['sql_field']['Description']];
		}
	}
	$defaultCirculationTypeDesc .= " - " . ($bookCirculationTypeDesc ? $bookCirculationTypeDesc : $defaultBookCirculationTypeDesc);
}

$BookCirculationArray = $libms->BOOK_GETOPTION('LIBMS_CIRCULATION_TYPE', 'CirculationTypeCode', $Lang['libms']['sql_field']['Description'], 'CirculationTypeCode', '');
$BookCirculationOption = $linterface->GET_SELECTION_BOX($BookCirculationArray, " name='BookCirclation' id='BookCirclation' ", $defaultCirculationTypeDesc, $CirculationTypeCode);

## Seletion - Gen Bar Code
$GenBarCode = ($BarCode != '') ? 'CUSTOMIZE' : 'ACNO';
$GenBarCodeArr = array();
//$GenBarCodeArr[] = array('', '-- '.$Lang["libms"]["general"]["select"].' --');
$GenBarCodeArr[] = array('AUTO', $Lang['libms']['general']['auto']);
$GenBarCodeArr[] = array('CUSTOMIZE', $Lang['libms']['general']['customize']);
$GenBarCodeArr[] = array('ACNO', $Lang['libms']['book']['use_acno_as_barcode']);
$GenBarCodeSelection = $linterface->GET_SELECTION_BOX($GenBarCodeArr, ' name="GenBarCode" id="GenBarCode" onchange="changeGenBarCode(this)"', '', $GenBarCode);

## Selection - Book Item Status
$ParTags = ' id="ItemStatus" name="ItemStatus" ';
$BookItemStatusSelection = $libms->GET_BOOK_ITEM_SELECTION($ParTags, '', $ItemStatus);

## Selection - Item Location
$ItemLocationArray = $libms->BOOK_GETOPTION('LIBMS_LOCATION', 'LocationCode', $Lang['libms']['sql_field']['Description'], 'LocationCode', '');
$ItemLocationOption = $linterface->GET_SELECTION_BOX($ItemLocationArray, " name='LocationCode' id='LocationCode' ", $Lang['libms']['status']['na'], $LocationCode);
?>
<script src="/templates/jquery/jquery-1.6.1.min.js" type="text/javascript" charset="utf-8"></script>

<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">

<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">


<? /*
<script src="./TextExt.js" type="text/javascript" charset="utf-8"></script>
*/ ?>


<script language="javascript">

var count_available ;
var unique_acno = true;
var unique_barcode = true;
var unique_bookid = false;
var fSubmit = false; 

var timerobj;

function jsValidateACNO()
{
	//var tmerObj = setInterval('$(\'#ACNO\').change()',1000);
	//clearInterval(tmerObj);
	
	timerobj = setTimeout(validateAction,1000);
}

function validateAction()
{
	$('#ACNO').change();
    $('#Barcode').change();
	clearTimeout(timerobj);
}

function jsValidateBook()
{
	timerobj = setTimeout(validateBookAction,1000);
}

function validateBookAction()
{
	$('#BookTitle').change();
	clearTimeout(timerobj);
}

function changeGenBarCode(obj){
	if(obj[obj.selectedIndex].value == 'CUSTOMIZE'){
		$('#Barcode').show();
        $('#barcode_error').show();
	} else {
		$('#Barcode').hide();
        $('#barcode_error').hide();
	}
}

$().ready( function(){
	$.ajaxSetup({	
		cache:false, async:false
	});

	//disable enter as submit
	$('input').keydown(function(event) {
		if (event.which == 13) {
			event.preventDefault();
			return false;
		}
	});
	
	<? /*
	// autocomplete book tag
	$('#BookTag')
		.textext({
			plugins : 'tags autocomplete',
			tagsItems: [ <?= $SelectedTaglist?> ] //selected tags
		})
		.bind('getSuggestions', function(e, data)
		{
			var list = [<?= $Taglist?>],
				textext = $(e.target).textext()[0],
				query = (data ? data.query : '') || '';

			$(this).trigger(
				'setSuggestions',
				{ result : textext.itemManager().filter(list, query) }
			);
		});
	
	// barcode field add and delete
	$('.btn_addRow').click(function () {
		add_row($(this));
	});
	
	$('.btn_deleteRow').click(function () {
	   del_row($(this));
    });
	
	//Barcode input ...................................................
	$('.book_barcode').change(function() {barcode_change($(this));});
	*/ ?>
	// gen book list....................................................
	<? /*
	$('#BookTitle').textext({
        plugins : 'autocomplete ajax',
        ajax : {
            url : 'ajax_book_suggestion.php',
            dataType : 'json',
            cacheResults : false
        }
    });
	*/ ?>
	$('#BookTitle').change(function(){
		var rs = [];
		var circulationTypeDesc = '';
		
		if($('#BookTitle').val() == ''){
			$('#BookTitle').parents('tr:first').find('td#bookid_error span.err_img').html('<img src="not_available.png" align="absmiddle"/>');
			unique_bookid = false;
			$('#BookID').val('');
		} else {
			$.get('ajax_check_book.php', { BookTitle : $('#BookTitle').val() },
				function(response){

					rs = jQuery.parseJSON(response);
					
					if ( rs['BookID'] != undefined ) {
						if (rs['BookID'] == ''){
							$('#BookTitle').parents('tr:first').find('td#bookid_error span.err_img').html('<img src="not_available.png" align="absmiddle"/>');
							unique_bookid = false; 
						}else if (parseInt(rs['BookID']) > 0){
							$('#BookTitle').parents('tr:first').find('td#bookid_error span.err_img').html('<img src="available.png" align="absmiddle"/>');
							unique_bookid = true;
							if (!rs["CirculationTypeDesc"]) {
								circulationTypeDesc = "<?=$Lang["libms"]["bookmanagement"]["unclassified"]?>";
							}
							else {
								circulationTypeDesc = rs["CirculationTypeDesc"];
							}
							$("#BookCirclation option:first-child").text("<?=$Lang["libms"]["bookmanagement"]["follow_parent"]?>" + " - " + circulationTypeDesc);	
						}
						$('#BookID').val(rs['BookID']);
					}
					else {
						$('#BookTitle').parents('tr:first').find('td#bookid_error span.err_img').html('<img src="not_available.png" align="absmiddle"/>');
						unique_bookid = false; 
						$('#BookID').val('');
					}					

				}
			);
		}
	});
	
	<? /*
	//auto gen bookcode....................................................
	
	$('#ACNO').textext({
        plugins : 'autocomplete ajax',
        ajax : {
            url : 'ajax_ACNO_suggestion.php',
            dataType : 'json',
            cacheResults : false
        }
    });
	
	//BookCode unique................................................
	*/
	?>
	$('#ACNO').change(function(){
        $('#ACNO').parent('td').next('td').find('td#acno_error span.err_img').html('<img src="loader.gif" align="absmiddle"/>');
//		$('#ACNO').parent('td').next('td').html('<img src="loader.gif" align="absmiddle"/>');
		$.get('ajax_check_acno.php', { acno : $('#ACNO').val() , UniqueID: $('#UniqueID').val() },
			function(acnoRespond){
				if (acnoRespond == '0'){
					$('#ACNO').parents('tr:first').find('td#acno_error span.err_img').html('<img src="available.png" align="absmiddle"/>');
					unique_acno = true; 
				}else if (acnoRespond == '1'){
					$('#ACNO').parents('tr:first').find('td#acno_error span.err_img').html('<img src="not_available.png" align="absmiddle"/>');
					unique_acno = false;
				}
				
			}
		);
	});

    $('#Barcode').change(function(){
        if ($.trim($('#Barcode').val()) != '') {
            $('#Barcode').parent('td').next('td').find('td#barcode_error span.err_img').html('<img src="loader.gif" align="absmiddle"/>');
            $.post('ajax_check_barcode.php', {Barcode: $('#Barcode').val(), UniqueID: $('#UniqueID').val()},
                function (BarcodeRespond) {
                    if (BarcodeRespond == '0') {
                        $('#Barcode').parents('tr:first').find('td#barcode_error span.err_img').html('<img src="available.png" align="absmiddle"/>');
                        unique_barcode = true;
                    } else if (BarcodeRespond == '1') {
                        $('#Barcode').parents('tr:first').find('td#barcode_error span.err_img').html('<img src="not_available.png" align="absmiddle"/>');
                        unique_barcode = false;
                    }
                }
            );
        }
        else {
            $('#Barcode').parent('td').next('td').find('td#barcode_error span.err_img').html('');
        }
    });

	<?
	/*
	//Count total available no of book......................................................................
	
	$('.barcode_bookstatus').change( function(){
		count_availbale_book();
	});
	
	$('.book_barcode').keypress(function(e) {
			return onKeyUp($(this),e);
	});
	$('.btn_delete_Record').click(function(){del_barcode($(this));});
	//Auto-gen Barcode
	$('.gen_barcode').change(function(){autogen_on_click($(this));});
	*/ ?>
	
	if($("#BookTitle").length > 0){
		$("#BookTitle").autocomplete(
	      "ajax_get_book_suggestion.php",
	      {
	  			delay:3,
	  			minChars:1,
	  			matchContains:1,
	  			onItemSelect: function() { jsValidateBook(); },
	  			formatItem: function(row){ return row[0]; },
	  			autoFill:false,
	  			overflow_y: 'auto',
	  			overflow_x: 'hidden',
	  			maxHeight: '200px'
	  		}
	    );
	}
	
	$("input.inputselect").inputselect({
		image_path: '/images/2009a/', 
		ajax_script: 'ajax_get_selection_list.php', 
		ajax_callback: function(id){
			if(id == 'ACNO'){
				jsValidateACNO();
			}
		}, 
		js_lang_alert: {'no_records' : '<?=$Lang['libms']['NoRecordAtThisMoment']?>' }
	});
});

function checknumeric(){
	var checking = true;
	$('.numberic').each(function() {
		if (($(this).val() != "") && (isNaN(Number($(this).val())) || $(this).val() < 0)){
			var field = $(this).parent().prev().find('span').html();
			alert("<?=$Lang["libms"]["book"]["alert"]["numeric"]?> "+field);	
			$(this).focus(); 
			checking = false;
			return false;	
	}});
	return checking;
};

function checkForm(form1) {
	var selfcheck = true;
	var barcodeObj = document.getElementById('GenBarCode');
	var barcodeOption = barcodeObj[barcodeObj.selectedIndex].value;
//	if(form1.BookTitle.value=='')	 {
//		alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['book']['title'] ?>");	
//		form1.BookTitle.focus();
//		return false;
//	} else 
	if($('#BookID').val() == ''){
		alert("<?= $Lang["libms"]["book"]["alert"]["item_book_title"] ?>");	
		$('#BookTitle').focus(); 
		return false;
	} else if($.trim($('#ACNO').val())=="") {
		alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['book']['code'] ?>");	
		$('#ACNO').focus(); 
		return false;
	} else if(!unique_acno){
		alert("<?= $Lang["libms"]["book"]["alert"]["incorrect_anco"] ?>");	
		$('#ACNO').focus(); 
		return false;
//	} else if(!checknumeric()) {
//		return false;
	} else if(barcodeOption == 'CUSTOMIZE' && $.trim($('#Barcode').val()) == ''){
		alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['book']['barcode'] ?>");
		$('#Barcode').focus(); 
		return false;
//	} else if(!selfcheck || !unique_barcode_check || !unique_acno){
//		alert("<?= $Lang['libms']['book']['code']."/".$Lang['libms']['book']['barcode'] ." incorrect" ?>");	
//		return false;
	}
    else if(barcodeOption == 'CUSTOMIZE' && $.trim($('#Barcode').val()) != '' && !unique_barcode) {
        alert("<?php echo $Lang["libms"]["bookmanagement"]["BarcodeDuplicate"]; ?>");
        $('#Barcode').focus();
        return false;
    }
	else {
		return true;
	}
}

function submit2(){
	$('#setItem').val(1);
	var obj = document.form1;
	if(!checkForm(obj)){
		return false;
	}
	obj.submit();
}
</script>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form name="form1" method="POST" action="item_edit_update.php"  onSubmit="return checkForm(this)">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr> 
		<td class="main_content">
			<div class="table_board">
                <p class="spacer"></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
						<? /* *** BookTitle or BookID *** */ ?>
                        <tr>
                        	<td width="15%" class="field_title_short">
                        		<span class="tabletextrequire">*</span><?= $Lang['libms']['book']['title'] ?>
                        	</td>
                        	<td width="33%">
                        	<? if($BookID != ''){ 
                        			echo $BookTitle;
                        	   } else { ?>
                        		<input autocomplete="off" type="text" id="BookTitle" name="BookTitle" class="textboxtext" value="" onFocusOut="jsValidateBook();" />
                        	<? } ?>
                        	<input type="hidden" id="BookID" name="BookID" value="<?=$BookID?>" />
                        	</td>
                        	<td id="bookid_error" colspan="100%"><span class='err_img'></span><span class="tabletextremark"></span></td>
                        	<? /* $Lang["libms"]["book"]["code_format"] */ ?>
						</tr>
						<? /* *** ACNO *** */ ?>
                        <tr>
							<td class="field_title_short">
								<span class="tabletextrequire">*</span><?= $Lang['libms']['book']['code'] ?>
							</td>
							<td>
							  <input autocomplete="off" id="ACNO" name="ACNO" type="text" class="textboxtext inputselect" maxlength="16" onFocusOut="jsValidateACNO();"  value="<?=$ACNO?>"/>
							</td>
							<td id="acno_error" colspan="100%">
								<span class='err_img'></span>
								<? /* <span class="tabletextremark"><?=$Lang["libms"]["book"]["code_format"]?></span> */ ?>
							</td>
						</tr>
						<? /* *** Barcode *** */ ?>
						<tr>
							<td width="15%" class="field_title_short">
								<span class="tabletextrequire">*</span><?= $Lang['libms']['book']['barcode'] ?>
							</td>
                        	<td width="33%">
                        		<?=$GenBarCodeSelection?> 
                        		<input id="Barcode" name="Barcode" type="text" class="textboxtext book_barcode" maxlength="64" value="<?=$BarCode?>" style="display:<?=(($BarCode != '') ? '' : 'none')?>;" />
                        	</td>
                        	<td id="barcode_error" colspan="100%">
                                <br />
                        		<span class='err_img'></span>
                        	</td>
						</tr>
						<? /* *** Item Status *** */ ?>
						<tr>
							<td width="15%" class="field_title_short">
								<span class="field_title"><?= $Lang['libms']['book']['book_status'] ?></span>
							</td>
                         	<td width="33%"><?=$BookItemStatusSelection?></td>
                         	<td colspan="100%">&nbsp;</td>
						</tr>
						<? /* *** Circulation type *** */ ?>
						<tr>
							<td width="15%" class="field_title_short">
								<span class="field_title"><?=  $Lang['libms']['settings']['circulation_type'] ?></span>
							</td>
                         	<td width="33%"><?=$BookCirculationOption?></td>
                         	<td colspan="100%">&nbsp;</td>
						</tr>
						<? /* *** Item Location *** */ ?>
						<tr>
							<td class="field_title_short">
								<span class="field_title"><?= $Lang['libms']['settings']['book_location'] ?></span>
							</td>
							<td><?=$ItemLocationOption?></td>
							<td colspan="100%">&nbsp;</td>
						</tr>
						<tr>
							<td class="field_title_short">
								<span class="field_title"><?= $Lang['libms']['book']['account_date'] ?></span>
							</td>
							<td><?=$linterface->GET_DATE_PICKER("AccountDate", $AccountDate)?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
							<td colspan="100%">&nbsp;</td>
						</tr>
						<tr>
							<td class="field_title_short">
								<span class="field_title"><?= $Lang['libms']['book']['series_number'] ?></span>
							</td>
							<td><input name="ItemSeriesNum" type="text" class="textboxtext" value="<?=$ItemSeriesNum?>"/></td>
							<td colspan="100%">&nbsp;</td>
						</tr>
                  	</tbody>
				</table>
				
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_purchase'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_date'] ?></span></td>
                        	<td width="33%"><?=$linterface->GET_DATE_PICKER("Purchasedate", $PurchaseDate)?><span class="tabletextremark">(YYYY-MM-DD)</span></td>
                        	<td width="4%">&nbsp;</td>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_price'] ?></span></td>
                         	<td width="33%"><input name="Purchaseprice" type="text" class="textboxnum numberic" value="<?=$PurchasePrice?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['list_price'] ?></span></td>
                        	<td><input name="Listprice" type="text" class="textboxnum numberic" value="<?=$ListPrice?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['discount'] ?></span></td>
                         	<td><input name="Discount" type="text" class="textboxnum numberic" value="<?=$Discount?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['distributor'] ?></span></td>
                        	<td><input id="Distributor" name="Distributor" type="text" class="textboxtext inputselect" maxlength="64" value="<?=$Distributor?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_by_department'] ?></span></td>
                         	<td><input id="Purchasedept" name="Purchasedept" type="text" class="textboxtext inputselect" maxlength="32" value="<?=$PurchaseByDepartment?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_note'] ?></span></td>
                        	<td><input name="Purchasenote" type="text" class="textboxtext" value="<?=$PurchaseNote?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['invoice_number'] ?></span></td>
                         	<td><input name="InvoiceNumber" type="text" class="textboxtext" value="<?=$InvoiceNumber?>"/></td>
						</tr>
						<tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['AccompanyMaterial'] ?></span></td>
                        	<td><input name="AccompanyMaterial" type="text" class="textboxtext" value="<?=$AccompanyMaterial?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['remark_to_user'] ?></span></td>
                         	<td><input name="RemarkToUser" type="text" class="textboxtext" value="<?=$RemarkToUser?>"/></td>
						</tr>
                  	</tbody>
				</table>
				
				<p></p>
				<?=$linterface->MandatoryField();?>
				<center>
                    <p class="spacer"></p>
                    <?= $SaveBtn ?>
                    <?= $SaveBtn2 ?>
                    <?= $CancelBtn ?>
                </center>
				<p class="spacer"></p>
			</div>
		</td>
	</tr>
</table>

<input type="hidden" id="UniqueID" name="UniqueID" value="<?=(($IsCopy == 1) ? '' : $UniqueID)?>" />
<input type="hidden" id="setItem" name="setItem" value="" />
</form>


<?php
############################################################################################################
$linterface->LAYOUT_STOP();

intranet_closedb();
?>