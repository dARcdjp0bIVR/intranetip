<?php

# being modified by: 
/*
 * Change log:
 * 
 *	2017-04-06 Cameron
 * 		- handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 			by stripslashes($keyword)
 * 
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");


intranet_auth();
intranet_opendb();

# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageEBookTags";


//$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['ebook_tags']);
if (!$plugin['eLib_Lite']){
	$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
}
$libms->MODULE_AUTHENTICATION($CurrentPage);


$TAGS_OBJ = $libms->get_ebook_tabs("TAGS");

$linterface = new interface_html();

$toolbar = $linterface->GET_LNK_ADD("elib_setting_tags_new.php",$button_new,"","","",0);

if (!get_magic_quotes_gpc()) {
	$keyword 	= stripslashes($keyword);
}

$LibeLib = new elibrary();
//*/
############################################################################################################

# Top menu highlight setting
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>
<script language="javascript">
<?if ($plugin['eLib_Lite']){?>
	$( document ).ready(function(){
		$("#leftmenu").css("display","none");
		$("#content").css("padding-left","0px");
	});
<?}?>
<!--

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}


function JSDeleteTag(TagID)
{
	if (confirm("<?=$eLib["html"]["confirm_remove_msg"]?>"))
	{
		self.location = "elib_setting_tags_delete.php?TagID=" + TagID;
	}
}


//-->
</script>
<form name="form1" id="form1" method="POST" action="elib_setting_tags.php" >


<div class="content_top_tool"  style="float: left;">
		<?=$toolbar?>
		</div>
		<div class="Conntent_search"><input name="keyword" id="keyword" type="text" value="<?=$keyword?>" /></div>     
		<br style="clear:both" />
		
		
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<?= $LibeLib->ListAllTagsInTable($keyword) ?>
		</tr>
	</table>
</form>

<?php
//dump($sql);
$linterface->LAYOUT_STOP();
 
 
 
intranet_closedb();





?>