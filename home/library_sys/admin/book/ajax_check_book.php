<?php
/*
 * 	Using: 
 * 	
 * 	Log
 * 	
 * 	Date:	2020-03-03 [Henry]
 * 			- add code to prevent UserID being changed in magicQuotes_stripslashes2()
 * 
 * 	Date:	2015-03-23 [Cameron]
 * 			- return result (BookID and CirculationTypeDesc) encapsulated in json rather than just BookID 
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
####### Trim all request #######
function recursive_trim(&$array){
	foreach($array as &$item){
		if (is_array($item)){
			recursive_trim($item);
		}else{
			if (is_string($item))
				$item=trim($item);
		}
	}
}
recursive_trim($_REQUEST);
####### END  OF   Trim all request #######
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/json.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

//****** start special handle for php5.4+
if (function_exists('get_magic_quotes_gpc') && !get_magic_quotes_gpc()) {

	// 1. strip slashes again	
	if (!function_exists('magicQuotes_stripslashes2')) {
		function magicQuotes_stripslashes2(&$value, $key) {
		    if(!isset($_SESSION[$key])) {
			    $value = stripslashes($value);
		    }
		}
	}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, 'magicQuotes_stripslashes2');
}
//****** end special handle for php5.4+

if(isset($_REQUEST['BookTitle']))//If a username has been submitted 
{
	$BookTitle = mysql_real_escape_string($_REQUEST['BookTitle']);//Some clean up :)
	$BookTitle = htmlspecialchars($BookTitle);
	$selected_book =  $libms->GET_BOOK_INFO_BY_FILTER('b.BookTitle', "b.BookTitle = '".$BookTitle."'");	// para: orderBy, condition	

	$ret = array();
	if (count($selected_book) > 0)
	{
		$ret["BookID"]				= $selected_book[0]["BookID"];
		$ret["CirculationTypeDesc"]	= $selected_book[0]["CirculationTypeDesc"];
	}
	else {
		$ret["BookID"]				= "";
		$ret["CirculationTypeDesc"]	= "";
	}
	
	$json = new JSON_obj();
	echo $json->encode($ret);

}

?>