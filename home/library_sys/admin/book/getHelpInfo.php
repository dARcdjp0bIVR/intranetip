<?php
/*
 * 2018-05-09 Cameron
 * - add case $Val == 2
 */
$PATH_WRT_ROOT = "../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");

if ($Val == 1) {
    $layer_content = $Lang['libms']['bookmanagement']['SearchHelp'];
} elseif ($Val == 2) {
    $layer_content = $Lang['libms']['bookmanagement']['BookItemSearchHelp'];
}

$tempLayer = returnRemarkLayer($layer_content, "hideMenu2('ToolMenu2');", 300, $height);

$response = $tempLayer;

echo $response;
?>