<?php
# Editing by 

/**
 * Log :
 * 
 * Date		2017-06-06 [Cameron]
 * 			- place set cookie function after include global.php to support php5.4 [case #E117788]
 * Date:	2017-04-06 [Cameron]
 * 			- handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 			by stripslashes($keyword)
 * Date:	2016-02-11 [Cameron]
 * 			- move sql to liblibrarymgmt.php > returnSQLforLostReport()
 * Date:	2016-02-05 [Cameron]
 * 			- revise sql, add column: ListPrice and Penalty 
 * 			- change "Reported Date" to use overdue record input date, use book unique updated on date if overdue record not exist
 * Date:	2015-09-07 [Henry]
 * 			access right bug fix
 * Date:	2014-04-07 [Henry]
 * 			Add anco and purchase price column, print and export page
 * Date:	2013-04-23 [Cameron]
 * 			Add date validation function before submit criteria for report: checkForm2()
 * Date:	2013-04-19 [Cameron]
 * 			handle null and empty case for following fields:
 * 				UserName, CallNum, ResponsibilityBy
 */
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	//$order = $ck_right_page_order;
	$order = 0;		# default in ascending order
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

//global $intranet_db;

intranet_auth();
intranet_opendb();

# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['reports'];

if (!$admin_right['lost report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageReportingLostBookReport";


$TAGS_OBJ[] = array($Lang['libms']['reporting']['PageReportingLostBookReport']);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$libms->MODULE_AUTHENTICATION($CurrentPage);


//$linterface = new interface_html("libms.html");
$linterface = new interface_html();

############################################################################################################
///*
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$sql = $libms->returnSQLforLostReport();

//dump($sql);
//debug($sql);
$li = new libdbtable2007($field, $order, $pageNo);

global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";

$li->field_array = array("DateModified", "UserName","ACNO","CallNum", "BookTitle", "ResponsibilityBy1","PurchasePrice","ListPrice","Penalty");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang["libms"]["book"]["lost_date"])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['user_name'])."</td>\n";
$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang["libms"]["label"]["BookCode"])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['call_number'])."</td>\n";
$li->column_list .= "<th width='24%' >".$li->column($pos++, $Lang['libms']['book']['title'])."</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['libms']['book']['responsibility_by1'])."</td>\n";
$li->column_list .= "<th width='8%' >".$li->column($pos++, $Lang["libms"]["book"]["purchase_price"])."</td>\n";
$li->column_list .= "<th width='8%' >".$li->column($pos++, $Lang["libms"]["book"]["list_price"])."</td>\n";
$li->column_list .= "<th width='8%' >".$li->column($pos++, $Lang["libms"]["report"]["pay"])."</td>\n";

//$li->column_list .= "<th width='1'>".$li->check("Code[]")."</td>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'book_new.php')",$button_new,"","","",0);
$toolbar .= 
$linterface->GET_LNK_IMPORT("import/csv/import_csv_data.php",$button_import,"","","",0);
$keyword = htmlspecialchars($_POST['keyword']);
if (!get_magic_quotes_gpc()) {
	$keyword 	= stripslashes($keyword);
}
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"{$keyword}\" onkeyup=\"Check_Go_Search(event);\" />";
//*/
############################################################################################################

# Top menu highlight setting



$linterface->LAYOUT_START();



?>
<script language="javascript">
<!--

function click_print()
{
	document.form1.action="lost_book_print.php";
	document.form1.target="_blank";
	document.form1.submit();
	
	document.form1.action = "lost_book.php";
	document.form1.target="_self";
}

function click_export()
{
	document.form1.action = "lost_book_export.php";
	document.form1.target = "_blank";
	document.form1.submit();
	
	document.form1.action = "lost_book.php";
	document.form1.target="_self";
}

function checkRemove(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		checkForm2();
	else
		return false;
}

function checkForm2() {
	var pass = check_date(document.getElementById("DueDateAfter"),"<?=$Lang['General']['InvalidDateFormat']?>");
	if (pass)
	 	pass = check_date(document.getElementById("DueDateBefore"),"<?=$Lang['General']['InvalidDateFormat']?>");
	if (pass)
		pass = pass & compare_date(document.getElementById("DueDateAfter"), document.getElementById("DueDateBefore"),"<?=$Lang['General']['WrongDateLogic']?>");
	if ( pass == true )	// true
	{
		document.form1.submit();
	}
}


//-->
</script>
<!--  <form name="form1" method="post" action=""> -->
<form name="form1" id="form1" method="POST" action="lost_book.php">

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
										
											<td width="40%" align="left">
												<?=$linterface->GET_DATE_PICKER("DueDateAfter",$_REQUEST['DueDateAfter'] )?>
												<?=$linterface->GET_DATE_PICKER("DueDateBefore",$_REQUEST['DueDateBefore'])?>
												<input type='button' value='<?=$Lang["libms"]["book"]["setting"]["filter_submit"]?>' onclick='checkForm2();' />
												
											</td>
											<td width="30%" align="center"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
											<td width="30%" align="right">
												<div class="content_top_tool">
													<div class="Conntent_search"><?=$searchTag?></div>     
												</div>
											</td>
										</tr>
									</table>
									<div id='toolbox' class="content_top_tool">
										<div class="Conntent_tool">
											<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?></a>
											<a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?></a>
										</div>
										<br style="clear:both" />
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
			</td>
		</tr>
	</table>
</form><?php

$linterface->LAYOUT_STOP();
 
 
 
intranet_closedb();


//dump ($sql);
//$error = mysql_error();
//dump ($error); 

?>
