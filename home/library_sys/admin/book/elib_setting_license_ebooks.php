<?php
/*
 *	Log	
 *  Date:	2018-10-12 [Rox]
 *			- Modify the default sorting by Book ID
 *          - Removed other sorting features.
 *
 *	Date:	2017-04-28 [Paul]
 *			- display book quotation info during book selection process
 *
 *
 *	Date:	2015-08-28 [Cameron]
 *			- revised to use standard IP table list and store selected/deselected record in db rather than localStorage
 *
 *	Date:	2015-08-27 [Cameron]
 *			- use object live method to support propagation of event handling(click)
 *			- add search by keyword (book title, author, publisher)
 *			- reindex row number 		
 *		
 *	Date:	2015-08-26 [Cameron]
 *			- Add sorting function when click column title: sort_ebook_license_page()
 *
 * 	Note:	max number of record to display: 9999 
 */
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

### Set Cookies
$arrCookies = array();
$arrCookies[] = array("eBook_eBook_license_field", "field");							// navigation var start
$arrCookies[] = array("eBook_eBook_license_order", "order");
$arrCookies[] = array("eBook_eBook_license_number", "pageNo");							// navigation var end
$arrCookies[] = array("eBook_eBook_license_keyword", "keyword");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
{
	updateGetCookies($arrCookies);
}

## Get Data



# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$QuotationID = $_REQUEST['QuotationID'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$keyword = trim($_REQUEST['keyword']);
$keyword = (isset($keyword) && $keyword != '') ? $keyword : '';
$order = 1; // default sort by ascending order 
$field = 0;	// default sort by Book ID
$pageNo = 1;

$linterface = new interface_html('libms.html');
$libelibinstall = new elibrary_install();

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageEBookLicense";

$libms->MODULE_AUTHENTICATION($CurrentPage);	// always return true
$TAGS_OBJ = $libms->get_ebook_tabs("LICENSE");
# Start layout
if (!$plugin['eLib_Lite']){
	$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
}

$showCheckBoxCol = false;
$action_row_style = "";
$no_extra_col = 4;

$quo_info = $libelibinstall->get_quotation_info($QuotationID);

if ($quo_info != false) {
	$QuotationNumber = $quo_info["QuotationNumber"];
	$QuotationBookCount = $quo_info["QuotationBookCount"];
	if($quo_info["PeriodFrom"] == "0000-00-00 00:00:00" && $quo_info["PeriodTo"] == "0000-00-00 00:00:00"){
		$disp_quo_info = "(".$Lang["ebook_license"]["quotation_number"]." <b>$QuotationNumber</b> : ".$Lang["ebook_license"]["permanent_use"].")<br/><br/>";
	}
	else {
		$PeriodFrom =  date('Y-m-d',strtotime($quo_info["PeriodFrom"]));
		$PeriodTo =  date('Y-m-d',strtotime($quo_info["PeriodTo"]));
		$disp_quo_info = "(".$Lang["ebook_license"]["quotation_number"]." <b>$QuotationNumber</b> : $PeriodFrom - $PeriodTo)<br/><br/>";
	}
	if ($quo_info["Type"] == 2 && $quo_info["IsActivated"] == "") {
		$showCheckBoxCol = true;
		$no_extra_col = 5;
	}
	else {
		$action_row_style = "none";
	}
}
else {
	$disp_quo_info = $Lang['libms']['bookmanagement']['ebook_license_quotation_error'];
	$QuotationBookCount = 0;
}

$nrSelectedBooks = $libelibinstall->get_compulsory_and_selected_book_count($QuotationID);

#########################
## Build sql for list view
$pageSizeChangeEnabled = false;

if ($showCheckBoxCol) {
	$sql = $libelibinstall->get_available_eBook_sql($QuotationID, $keyword);
}
else {
	$sql = $libelibinstall->get_enabled_eBook_sql($QuotationID, $keyword);
}

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("i.BookID");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+$no_extra_col;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";
$li->no_navigation_row = true;
$li->page_size = 9999;

$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='40%' >".$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['BookTitle']."</td>\n";
$li->column_list .= "<th width='22%' >".$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Author']."</td>\n";
$li->column_list .= "<th width='22%' >".$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Publisher']."</td>\n";
$li->column_list .= "<th width='14%' >".$Lang["ebook_license"]["language"]."</td>\n";

if ($showCheckBoxCol) {
	$li->column_list .= "<th width='1'>".$li->check("AvailableBookID[]")."</td>\n";
}

$linterface->LAYOUT_START(); 

?>
<head>
<script>

$("document").ready(function(){
<?if ($plugin['eLib_Lite']){?>
	$("#leftmenu").css("display","none");
	$("#content").css("padding-left","0px");
<?}?>

	var book_count_quotation = <?=$QuotationBookCount?>;
	$("#selected_books").val($("#selected_books").val() + ' (<?=$nrSelectedBooks?>/'+book_count_quotation+')');
	
	// select button
	$('#select').live( 'click', function(){
		var selected_item = $("input[name='AvailableBookID[]']:checked");
		var num_selected = selected_item.length;
		if (num_selected > 0) {
			if (select_ebook_license() > 0) {
				selected_item.each(function(){
					$(this).parent().parent().remove();				
				});
				update_row_no();		
				update_select_count(book_count_quotation, num_selected);
			}
		}
		else {
			alert("<?=$Lang["libms"]["tableNoCheckedCheckbox"]?>!");
		}		
	});
	
	$('#selected_books').live( 'click', function(){
		$('#form1').attr("action","elib_setting_license_selected_ebooks.php");		
		$('#form1').submit();
	});
		
	$('#keyword').live( 'keypress', function(e){
	    if ( e.which == 13 ) {
			e.preventDefault();
		    return false;
	    }
	});

	$('#keyword').live( 'keyup', function(e){
		var key = e.which || e.charCode || e.keyCode;
	    if ( key == 13 ) {
	    	$('#form1').submit();
	    }
	});
});

function select_ebook_license() {
	var ret = 0;
	var rs = [];
	$('#task').val('select');
	$.ajax({								
		url : "ajax_elib_setting_license_ebooks_action.php",
		async: false,
		timeout:1000,							
		type : "POST",							
        data: $("#form1").serialize(),
		success : function(msg) {
			ret = parseInt(msg);
        }
	});
	return ret;								
}

function update_select_count(book_count_quotation, num_selected){
	var newVal = parseInt(num_selected) + parseInt($('#NrSelectedBooks').val());
	$('#NrSelectedBooks').val(newVal);
	$("#selected_books").val('<?=$Lang["ebook_license"]["selected_books"]?> ('+newVal+'/'+book_count_quotation+')');
}

function update_row_no() {
	var i = 0;
	$("#result_table tr td:first-child").each(function(){
		i++;
		$(this).html(i);
	});
}


</script>
<style>
	.shadetabs a input{
		width: auto;
		background: transparent;
		border: 0;
		cursor:pointer;
	}

	.shadetabs{
		border-bottom : 0px;
		margin-bottom: 0px;
	}
	
	.shadetabs li.current_li {
		position: relative;
		color: #2b64b2;
	}
	.shadetabs li.current_li a{ /*selected main tab style */
		background-image: url(/images/2009a/shade.gif);
		color: #2b64b2;
		font: bold ;
		border-bottom-color: white;
		border-top-width: 1px;
		border-right-width: 1px;
		border-bottom-width: 1px;
		border-left-width: 1px;
		border-top-style: solid;
		border-right-style: solid;
		border-bottom-style: solid;
		border-left-style: solid;
		border-top-color: #b1bfdb;
		border-right-color: #b1bfdb;
		border-left-color: #b1bfdb;
	}
	.book_purchased_display{
		white-space: nowrap;
	}
	.expiry_date_display{
		color:red;
	}
	.book_purchased{
		color:grey;
	}
</style>
</head>
<body>
	<form name="form1" id="form1" method="post" action="elib_setting_license_ebooks.php">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2">
					<?=$disp_quo_info?>
				</td>
			</tr>
			<tr style="display:<?=$action_row_style?>">
				<td valign="bottom">
				  	<div class="shadetabs" style="float:left;">
						<ul>
							<li class="book_btn_li current_li">
								<a><input type="button" id="available_books" class="" style="border:0px; background: transparent;" value="<?=$Lang["ebook_license"]["available_books"]?>"></input></a>
						  	</li>
						  	<li class="book_btn_li">
								<a><input type="button" id="selected_books" class="" style="border:0px; background: transparent;" value="<?=$Lang["ebook_license"]["selected_books"]?>"></input></a>
						  	</li>
						</ul>
					</div>					
				</td>		
				<td valign="bottom">
					<div style="float:right;">
						<input type="button" id="select" class="formbutton_v30" style=" float: right;"  value="<?=$Lang["ebook_license"]["select"]?>"></input>
						<div class="Conntent_search">
							<input autocomplete="off" name="keyword" id="keyword" type="text" value="<?=$keyword?>" />
						</div>
					</div>
				</td>
			</tr>
		</table>
	
		<div id="result_table">
		<?=$li->displayFormat_eBook_license_table("","","",$showCheckBoxCol);?>
		</div>
		<br />
		<input type="hidden" name="QuotationID" value="<?=$QuotationID?>" />
		<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
		<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
		<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
		<input type="hidden" name="task" id="task" value="" />
		<input type="hidden" id="NrSelectedBooks" value="<?=$nrSelectedBooks?>" />
		
	</form>
</body>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
 