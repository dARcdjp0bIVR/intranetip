<?php
## modifying by: 

/**
 * Log :
 * 
 * Date		2017-06-06 (Cameron)
 * 			- place set cookie function after include global.php to support php5.4 [case #E117788]
 * 
 * Date		2017-04-03 (Cameron)
 * 			- handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 	
 * Date		2014-08-22 (Henry)
 * 			fix bug on fail to display the list of tag when the record status of book is NULL
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	//$order = $ck_right_page_order;
	$order = 1;		# default in ascending order
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}


//global $intranet_db;

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];


if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();


$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageMgmtBatchEdit";

$libms->MODULE_AUTHENTICATION($CurrentPage);


$linterface = new interface_html();

############################################################################################################
///*
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($keyword!="")
{
	if (!get_magic_quotes_gpc()) {
		$keyword = stripslashes($keyword);
	}
	$keyword_sql = mysql_real_escape_string($keyword);
	$sql_cond .= " AND t.TagName LIKE '%$keyword_sql%' ";
}
	

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;


$sql_tagid_concat = (isset($junior_mck)) ? " CONVERT(t.TagID  USING utf8)" : "t.TagID";

//href=\"javascript:showRelatedBooks(\'', REPLACE(REPLACE(t.TagName, '\"', ' '), '\'', ' '),'\',', t.TagID, ')\"
$sql = "SELECT 
			t.TagName, CONCAT('<a class=\"thickbox\" href=\"book_list.php?TagID=', {$sql_tagid_concat}, '&width=780&height=500\" title=\"',REPLACE(t.TagName, '\"', '&quot;'),'\">', count(bt.BookID), '</a>'), t.DateModified,
            CONCAT('<input type=\'checkbox\' name=\'TagID[]\' id=\'TagID[]\' value=\'',  t.TagID ,'\'>'), count(bt.BookID) As BookTotal
		FROM 
			LIBMS_TAG AS t, LIBMS_BOOK_TAG AS bt, LIBMS_BOOK AS b
		WHERE
			bt.TagID=t.TagID AND b.BookID=bt.BookID AND (b.RecordStatus<>'DELETE' OR b.RecordStatus IS NULL) 
			{$sql_cond}
		 Group by t.TagID
        ";

$li = new libdbtable2007($field, $order, $pageNo);

global $eclass_prefix;
$li->db = $libms->db;

$li->field_array = array("TagName", "BookTotal", "DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='33%' >".$li->column($pos++, $Lang["libms"]["book"]["tags"])."</td>\n";
$li->column_list .= "<th width='33%' >".$li->column($pos++, $Lang["libms"]["book"]["tag_books"])."</td>\n";
$li->column_list .= "<th width='33%' >".$li->column($pos++, $Lang["libms"]["book"]["last_modified"])."</td>\n";
$li->column_list .= "<th width='1'>".$li->check("TagID[]")."</td>\n";

//$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'resources_type_new.php')",$button_new,"","","",0);

$keyword = htmlspecialchars($keyword);
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"{$keyword}\" />";


//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ = $libms->get_batch_edit_tabs("TAGS");
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
 




?>


<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script language="javascript">
<!--
function checkRemove(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}


function checkMultiEdit(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                obj.action=page;
                obj.method="post";
                obj.submit();
        }
}


//-->
</script>
<form name="form1" method="post" action="">


	<div class="content_top_tool"  style="float: right;">
		<div class="Conntent_search"><?=$searchTag?></div>     
		<br style="clear:both" />
	</div>
	
	
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr class="table-action-bar">
								<td height="28" align="right" valign="bottom" >
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap="nowrap"><a href="javascript:checkMultiEdit(document.form1,'TagID[]','batch_modify_tags_edit.php')" class="tabletool" title="<?= $button_edit ?>"> <img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?> (<?=$Lang["libms"]["book"]["multiple_selection"]?>) </a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap="nowrap"><a href="javascript:checkRemove(document.form1,'TagID[]','batch_modify_tags_remove.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
			</td>
		</tr>
	</table>
</form><?php

$linterface->LAYOUT_STOP();

 
 
 


intranet_closedb();


?>