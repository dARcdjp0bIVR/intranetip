<?php
/*
 * 	modifying: 
 * 	Log
 * 	Purpose:	show list of book item by search criteria
 *	@return:	array['layout']		-- listview of result
 *				array['result']		-- number of record 
 * 
 * 	Date:	2015-07-09 [Cameron]
 * 			- filter out 'DELETE' record
 * 			- default sort by ACNO (case 2015-0304-0908-31071)
 * 
 * 	Date:	2015-07-07 [Cameron] 
 * 			- support column sorting when it's clicked 
 * 
 * 	Date:	2015-07-06 [Cameron] 
 * 			- add id='no_record_row' for no record row
 * 			- assign BarCode to row ID in table list
 * 
 * 	Date:	2014-12-16 [Cameron] create this file
 * 
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."home/library_sys/admin/book/batch_modify_manage.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$task = $_REQUEST['task'];
$bookItemID = $_REQUEST['bookItemID'];

$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 1 : $field;	// default sort by ACNO
$sort = ($order == 1) ? "ASC" : "DESC";
$orderByField = $field + 1;

$bmm = new batch_modify_manage();
$bmm->setfield($field);
$bmm->setorder($order);

if ($task == 'sorting') {
	
	if ($bookItemID) {
		$cond = " AND bu.UniqueID IN ('" .str_replace(',', "','",$bookItemID) . "')";
	}
	else {
		$cond = '';
	}	 	
}
else {
	$cond = $bmm->getBatchUpdateSearchFilter($SrcFrom="BookItem");
}

$searchChar = '/[!"#$%&\'()*+,.\/:;<=>?@[\]^`{|}~ ]/';

$sql = "SELECT 
			IF(b.BookTitle IS NULL OR b.BookTitle = '','".$Lang['General']['EmptySymbol']."',b.BookTitle) AS BookTitle,
			IF(bu.ACNO IS NULL OR bu.ACNO = '','".$Lang['General']['EmptySymbol']."',bu.ACNO) AS ACNO,			 
			bu.UniqueID, bu.BarCode 
		FROM 
			LIBMS_BOOK as b 
		INNER JOIN LIBMS_BOOK_UNIQUE as bu ON bu.BookID=b.BookID
		WHERE (bu.RecordStatus NOT LIKE 'DELETE' OR bu.RecordStatus IS NULL) 
			{$cond}		
		ORDER BY ". $orderByField. " " . $sort;

$rs = $libms->returnArray($sql);
$nrRs = count($rs);
$x = '<table id="book_item_table" class="common_table_list_v30 view_table_list_v30" style="width:98%">
			<tr>
				<th width="1">#</th>';
						
$pos = 0;						
$x .= '			<th width="75%">'.$bmm->column($pos++, $Lang["libms"]["book"]["title"]).'</th>
				<th width="20%">'.$bmm->column($pos++, $Lang["libms"]["book"]["code"]).'</th>				
				<th class="num_check" width="1"><input type="checkbox" id="checkall" name="checkall" onclick="Set_Checkbox_Value(\'BookItemID[]\',this.checked)" checked /></th>
			</tr>';
			
$BookID = array();

if($nrRs>0){		
	for ($i=0; $i<$nrRs; $i++){
		$x .= '<tr id="'.preg_replace($searchChar,'-',$rs[$i]['BarCode']).'">
					<td>'.($i+1).'</td>
					<td>'.$rs[$i]['BookTitle'].'</td>
					<td>'.$rs[$i]['ACNO'].'</td>
					<td><input type="checkbox" id="BookItemID[]" name="BookItemID[]" value="' . $rs[$i]['UniqueID'] . '" checked /></td>
				</tr>';
			
//		$BookID[$i] = $result[$i]['BookID'];
	}
}
else{
	 # no record
	$x.="<tr id='no_record_row' class=\"tablebluerow2 tabletext\">
			<td class=\"tabletext\" colspan=\"4\" align=\"center\" height=\"40\" style=\"vertical-align:middle\">$i_no_record_exists_msg</td>
		</tr>";
}
$x .= '</table>';

$ret = array();
$ret['layout'] = $x;
$ret['result'] = $nrRs; 
$json = new JSON_obj();
echo $json->encode($ret);

//echo $x;

intranet_closedb();
?>