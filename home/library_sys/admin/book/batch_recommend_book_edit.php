<?php

//modifying:
/*
 * 	@Purpose:	edit a recommend book
 *  
 * 	Log
 * 
 * 	Date:	2016-11-11 [Cameron]
 * 			- make class level mandatory
 * 
 * 	Date:	2016-09-20 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageMgmtBatchEdit";

$libms->MODULE_AUTHENTICATION($CurrentPage);

$TAGS_OBJ = $libms->get_batch_edit_tabs("RECOMMEND");

$linterface = new interface_html();


$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
$ParArr["RecommendID"] = isset($_POST['RecommendID'][0]) ? $_POST['RecommendID'][0] : '';
$Recommend = $lelibplus->getBookRecommendByID($ParArr);

if (count($Recommend)) {
	$BookID = $Recommend['BookID'];
	unset($lelibplus);
	$lelibplus = new elibrary_plus($BookID, $_SESSION['UserID']);
	$ClassLevelArray = $lelibplus->getRecommendedClassLevels();
	$class_table = array_chunk($ClassLevelArray['class_levels'],3) ;
	$detail_data=$lelibplus->getBookDetail();
}
else {
	$BookID = '';
	$class_table = array();
	header("Location: batch_recommend_book_list.php");
}


############################################################################################################

# Top menu highlight setting
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();

?>

<script language="javascript">

$(document).ready(function(){
	
    $('#check_all').click(function(){
            if ($(this).is(':checked')){
                $(':input[name="class_level_ids\[\]"]:checkbox').attr('checked', 'checked');
            }else{
                 $(':input[name="class_level_ids\[\]"]:checkbox').removeAttr('checked');
            }
    });
    
	$('#submit_recommend').click(function(){
		if($('input[name="class_level_ids\[\]"]:checked').length == 0) {
			alert("<?=$Lang["libms"]["batch_edit"]["SelectAtLeastOneClassLevel"]?>");	
		}
		else {
			$('#form1').submit();
		}
	});
    
});

</script>

<form name="form1" id="form1" method="post" action="batch_recommend_book_edit_update.php">

<input id="BookID" name="BookID" type="hidden" class="textboxtext" value="<?=$BookID?>">

<table width="100%" align="center" border="0">
	<tr>
		<td>
			<table class="form_table_v30">
				<tbody>
					<tr valign="top">
						<td class='field_title'><?=$Lang["libms"]["batch_edit"]["BookTitle"]?></td>
						<td><?=$detail_data['title']?></td>
					</tr>
				
					<tr>
						<td colspan="3" class='field_title'>
							<?=$eLib_plus["html"]["recommendbookto"]?>
						</td>
					</tr>

					<tr>
						<td colspan="3">
							<input type="checkbox" id="check_all"><?=$eLib["html"]["all_class"]  ?>
						</td>
					</tr>
					
			<? foreach ($class_table as $class_level_rows): ?>
			    	<tr>
			    	<? foreach ($class_level_rows as $class_level): ?>
						<td>
							<input type="checkbox" <?=$class_level['class_level_recommended']? "checked='checked'":""?> name="class_level_ids[]" value="<?=$class_level['class_level_id']?>"><?=$class_level['class_level_name']?>
						</td>
			    	<? endforeach ?>
			    	</tr>
			<? endforeach ?>
					<tr>
						<td colspan="3">
							<textarea name='content' rows="5" class="textboxtext"><?=$ClassLevelArray['content']?></textarea>
						</td>
					</tr>
					
				</tbody>
			</table>
			
			<div class="edit_bottom_v30">
				<?= $linterface->GET_ACTION_BTN($button_submit, "button", $ParOnClick="",$ParName="submit_recommend")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='batch_recommend_book_list.php'")?>
			</div>
				
		</td>
	<tr>		
</table>		


</form>

<?php


$linterface->LAYOUT_STOP();

 
intranet_closedb();


?>
