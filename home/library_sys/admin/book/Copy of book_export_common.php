<?php
/*
 * 	Using: 	
 * 	Log
 *
 * 	Date:		2015-10-08 [Cameron]
 * 				- fix bug: should not trim Code in getBibliographySearchFiler() because it's array
 *  
 * 	Date:		2015-10-05 [Cameron]
 *  			- fix bug on basic search / advance search for string like A&<>'"\B, support comparing unconverted string A&<>'"\B ( created by imoport)
 * 				and converted string A&amp;'&quot;&lt;&gt;\B (created by add / edit)
 * 
 * 	Date:		2015-07-31 [Cameron]
 * 				- remove restriction on BookCategoryType=1 when filter BookCategory record which Language is not set
 * 		
 * 	Date:		2015-03-25 [Cameron]
 * 	Details:	add advance seach condition in getBibliographySearchFiler()
 * 
 * 	Date:		2015-01-30 [Henry] 
 * 	Details:	support 2 type of book category
 * 
 * 	Date:		2014-12-04 [Cameron] 
 * 	Details:	consolidate common functions for book export
 * 				fix bug of export records: search by exact ACNO or ACNO range
 * 
 * 	Date:		2014-12-02 [Cameron]
 * 	Details:	add search max(ACNO) by pattern of {prefix}^ in keywords field
 
 */
function get_xxxConditions_SQL($fieldname, $selected=''){
	$conds = '';
	if ($selected != ''){		// 2015-07-27: don't use !empty() as $selected value can be '0' 
		$v = trim($selected);
		$sql_safe_CODE = PHPToSQL($v);
		$conds = " AND TRIM(`{$fieldname}`) LIKE {$sql_safe_CODE} ";
	}
	
	return $conds;
}


/*
 * 	@param:	$alias_LIBMS_BOOK			-- LIBMS_BOOK
 * 			$alias_LIBMS_BOOK_UNIQUE	-- LIBMS_BOOK_UNIQUE
 * 	@return:	concatination of filter
 */
function getBibliographySearchFiler($alias_LIBMS_BOOK='lb',$alias_LIBMS_BOOK_UNIQUE='lbu') {
	$b 	= $alias_LIBMS_BOOK;
	$bu = $alias_LIBMS_BOOK_UNIQUE;
	$advanceSearch			= trim($_REQUEST['advanceSearch']);
	$keyword 				= trim($_REQUEST['keyword']);
	//for the 2 type of book category
	$BookCategoryCodeTemp = explode(":",trim($_REQUEST['BookCategoryCode']));
	$BookCategoryType		= trim($BookCategoryCodeTemp[0]);
	$BookCategoryCode		= trim($BookCategoryCodeTemp[1]);
	$ResourcesTypeCode		= trim($_REQUEST['ResourcesTypeCode']);
	$CirculationTypeCode 	= trim($_REQUEST['CirculationTypeCode']);
	$SubjectSelect			= trim($_REQUEST['SubjectSelect']);
	$PublisherSelect		= trim($_REQUEST['PublisherSelect']);
	$LanguageSelect			= trim($_REQUEST['LanguageSelect']);
	$Code					= $_REQUEST['Code'];
	
	// start advance search fields
	$ACNO					= trim($_REQUEST['ACNO']);
	$publisher				= trim($_REQUEST['publisher']);
	$author					= trim($_REQUEST['author']);
	$callNumber				= trim($_REQUEST['callNumber']);
	$ISBN					= trim($_REQUEST['ISBN']);
	$bookTitle				= trim($_REQUEST['bookTitle']);
	// end advance search fields
	
	$ret = "";	// return string

	# (1) selected books (Code)
	if (is_array($Code) && sizeof($Code)>0)
	{
		$conds_ids = " AND {$b}.BookID IN (" . implode(", ", $Code) . ")";
		$ret .= $conds_ids . " ";
	}
	
	# (2) filters from option list - BookCategory, ResourcesType, CirculationType	
	$conds_filter = get_xxxConditions_SQL('BookCategoryCode', $BookCategoryCode);
	//for the 2 type of book category
	if($_REQUEST['BookCategoryCode']!=""){
		$conds_filter .= ' AND (Language IN (SELECT lbl2.BookLanguageCode FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookCategoryType = "'.$BookCategoryType.'") OR Language IS NULL OR Language = "") ';		
	}
	$conds_filter .= get_xxxConditions_SQL('ResourcesTypeCode', $ResourcesTypeCode);
	//$conds_filter .= get_xxxConditions_SQL('CirculationTypeCode', $CirculationTypeCode);
	if ($CirculationTypeCode!="")
	{
		$conds_filter .= " AND ((lb.CirculationTypeCode is not null AND lb.CirculationTypeCode='".addslashes($CirculationTypeCode)."')". 
						" OR (lbu.CirculationTypeCode is not null AND lbu.CirculationTypeCode='".addslashes($CirculationTypeCode)."') ) ";
	}
	 
	$ret .= ($conds_filter)? $conds_filter . " " : "";

	# (3.1) filters from option list - Subject	
	if($SubjectSelect != ''){
		if($SubjectSelect =='NA'){
			$subjectSelect_cond = " AND ({$b}.Subject = '' OR {$b}.Subject is null)" ;
		}else{
			$subjectSelect_cond = " AND ({$b}.Subject = '".addslashes(htmlspecialchars($SubjectSelect))."' or {$b}.Subject = '".$SubjectSelect."') " ;			
		}
		$ret .= $subjectSelect_cond . " ";
	}
	
	## (3.2) PublisherSelect Sql Cond
	if($PublisherSelect != ''){
		if($PublisherSelect =='NA'){
			$publisherSelect_cond = " AND ({$b}.Publisher = '' OR {$b}.Publisher is null)" ;
		}else{
			$publisherSelect_cond = " AND ({$b}.Publisher = '".addslashes(htmlspecialchars($PublisherSelect))."' or {$b}.Publisher = '".$PublisherSelect."') " ;
		}
		$ret .= $publisherSelect_cond . " ";
	}
	
	//Henry added 2014014
	## (3.3) LanguageSelect Sql Cond
	if($LanguageSelect != ''){
		if($LanguageSelect =='NA'){
			$languageSelect_cond = " AND ({$b}.Language = '' OR {$b}.Language is null)" ;
		}else{
			$languageSelect_cond = " AND ({$b}.Language = '".addslashes(htmlspecialchars($LanguageSelect))."' or {$b}.Language = '".$LanguageSelect."') " ;
		}
		$ret .= $languageSelect_cond . " ";
	}
	
	if ($advanceSearch) {
		$advCond = "";
		if($ACNO!=""){
			$unconverted_ACNO = mysql_real_escape_string(str_replace("\\","\\\\",$ACNO));
			$converted_ACNO = special_sql_str($ACNO);									
			$advCond .= " AND (
				{$bu}.`ACNO` LIKE '%$unconverted_ACNO%' OR {$bu}.`ACNO` LIKE '%$converted_ACNO%'
				)";
		}
		
		if($publisher!=""){
			$unconverted_publisher = mysql_real_escape_string(str_replace("\\","\\\\",$publisher));
			$converted_publisher = special_sql_str($publisher);									
			$advCond .= " AND (
				{$b}.`Publisher` LIKE '%$unconverted_publisher%' OR {$b}.`Publisher` LIKE '%$converted_publisher%'
				)";
		}
		
		if($author!=""){
			$unconverted_author = mysql_real_escape_string(str_replace("\\","\\\\",$author));
			$converted_author = special_sql_str($author);									
			$advCond .= " AND (
					{$b}.`ResponsibilityBy1` LIKE '%$unconverted_author%' OR {$b}.`ResponsibilityBy1` LIKE '%$converted_author%' OR 
					{$b}.`ResponsibilityBy2` LIKE '%$unconverted_author%' OR {$b}.`ResponsibilityBy2` LIKE '%$converted_author%' OR 
					{$b}.`ResponsibilityBy3` LIKE '%$unconverted_author%' OR {$b}.`ResponsibilityBy3` LIKE '%$converted_author%'
				)";
		}
			
		if($callNumber!=""){
			$unconverted_callNumber = mysql_real_escape_string(str_replace("\\","\\\\",$callNumber));
			$converted_callNumber = special_sql_str($callNumber);									
			$advCond .= " AND (
				{$b}.`CallNum` LIKE '%$unconverted_callNumber%' OR {$b}.`CallNum` LIKE '%$converted_callNumber%' OR 
				{$b}.`CallNum2` LIKE '%$unconverted_callNumber%' OR {$b}.`CallNum2` LIKE '%$converted_callNumber%' OR 
				CONCAT({$b}.`CallNum`, ' ', {$b}.`CallNum2`) LIKE '%$unconverted_callNumber%' OR 
				CONCAT({$b}.`CallNum`, ' ', {$b}.`CallNum2`) LIKE '%$converted_callNumber%'
				)";
		}
		
		if($ISBN !=""){
			$unconverted_ISBN = mysql_real_escape_string(str_replace("\\","\\\\",$ISBN));
			$converted_ISBN = special_sql_str($ISBN);									
			$advCond .=" AND
				({$b}.`ISBN` LIKE '%$unconverted_ISBN%' OR {$b}.`ISBN` LIKE '%$converted_ISBN%' OR 
				{$b}.`ISBN2` LIKE '%$unconverted_ISBN%' OR {$b}.`ISBN2` LIKE '%$converted_ISBN%')";
		}
		
		if($bookTitle !=""){
			$unconverted_bookTitle = mysql_real_escape_string(str_replace("\\","\\\\",$bookTitle));		// A&<>'"\B ==> A&<>\'\"\\\\B
			$converted_bookTitle = special_sql_str($bookTitle);											// A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B
			$advCond .=" AND
				({$b}.`BookTitle` LIKE '%$unconverted_bookTitle%' OR {$b}.`BookTitle` LIKE '%$converted_bookTitle%' OR 
				{$b}.`BookSubTitle` LIKE '%$unconverted_bookTitle%' OR {$b}.`BookSubTitle` LIKE '%$converted_bookTitle%')";
		}
		
		$ret .= $advCond;
		
	}	// end advance search
	else {	// not advance search
		# (4) keyword
		if($keyword!="")
		{
			$acno_filter = "";
			$unconverted_keyword_sql = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));		// A&<>'"\B ==> A&<>\'\"\\\\B
			$converted_keyword_sql = special_sql_str($keyword);											// A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B
			$keyword_sql = $unconverted_keyword_sql;
			
			preg_match('/^(\s*)((\w)+)(\s*)~(\s*)((\w)+)(\s*)$/',$keyword_sql,$matches_range);
			preg_match('/^(\s*)\\\"(\s*)((\w)+)(\s*)\\\"(\s*)$/',$keyword_sql,$matches_strict);
			preg_match('/^(\s*)((\w)+)(\^)(\s*)$/',$keyword_sql,$matches_max);
			if(!isset($matches_strict[3])||$matches_strict[3]==""){
				preg_match("/^(\s*)\\\'(\s*)((\w)+)(\s*)\\\'(\s*)$/",$keyword_sql,$matches_strict);
			}
			$acno_strict = $matches_strict[3];
			# (4.1) by range
			if($matches_range[0]!=""){
				$iACNO_start = trim($matches_range[2]);
		        $iACNO_end = trim( $matches_range[6]);
		        if ($iACNO_start<>"" || $iACNO_end<>"")
		        {        
				    $conditions[] ="{$bu}.`RecordStatus`<>'DELETE'";
			
			     	if ($iACNO_start<>"" && $iACNO_end<>"")
				    {
				     	$conditions[] = "{$bu}.`ACNO` >= '{$iACNO_start}'";
				    	$conditions[] = "{$bu}.`ACNO` <= '{$iACNO_end}'";
				    } elseif ($iACNO_start<>"")
				    {
				    	$conditions[] = "{$bu}.`ACNO` = '{$iACNO_start}'";
				    } elseif ($iACNO_end<>"")
				    {
					    $conditions[] = "{$bu}.`ACNO` = '{$iACNO_end}'";
				    }
//				    if (!empty($sDate_start)){
//					    $conditions[] = "{$bu}.`PurchaseDate` >= '{$sDate_start}'";
//				    }
//				    if (!empty($sDate_end)){
//					    $conditions[] = "{$bu}.`PurchaseDate` <= '{$sDate_end}'";
//				    }
				
				    if (!empty($conditions)){
						$acno_filter = '(' . implode(') AND (', $conditions) . ')';   
				    }
		        }
			}
			# (4.2) by exact match
			elseif(isset($acno_strict)&&$acno_strict!=""){
				$acno_filter = "{$bu}.ACNO = '$acno_strict'";
			}
			# (4.3) by max ACNO
			elseif($matches_max[0]!=""){
				if ($matches_max[2] != "") {
					$acno_filter = "{$bu}.ACNO = (select max(ACNO) from `LIBMS_BOOK_UNIQUE` where ACNO LIKE '".$matches_max[2]."%' limit 1)";
				}
			} 
			else{
				if ($unconverted_keyword_sql == $converted_keyword_sql) {
					$acno_filter = "{$bu}.ACNO LIKE '%$keyword_sql%'";
				}
				else {
					$acno_filter = "({$bu}.ACNO LIKE '%$unconverted_keyword_sql%' OR {$bu}.ACNO LIKE '%$converted_keyword_sql%')";					
				}
			}
			
			if ($unconverted_keyword_sql == $converted_keyword_sql) {		// keyword does not contain any special character: &<>"			
				$keyword_cond2 = " AND (
					{$b}.Publisher LIKE '%$keyword_sql%' OR ";
				$keyword_cond2 .= ($acno_filter) ? $acno_filter ." OR " : "";		
				$keyword_cond2 .= "{$b}.CallNum LIKE '%$keyword_sql%' OR
					{$b}.CallNum2 LIKE '%$keyword_sql%' OR
					CONCAT({$b}.CallNum, ' ', {$b}.CallNum2) LIKE '%$keyword_sql%' OR
					{$b}.ISBN LIKE '%$keyword_sql%' OR
					{$b}.ISBN2 LIKE '%$keyword_sql%' OR
					{$b}.BookTitle LIKE '%$keyword_sql%' OR
					{$b}.BookSubTitle LIKE '%$keyword_sql%' OR
					{$b}.ResponsibilityBy1 LIKE '%$keyword_sql%' OR
					{$b}.ResponsibilityBy2 LIKE '%$keyword_sql%' OR
					{$b}.ResponsibilityBy3 LIKE '%$keyword_sql%' OR
					{$b}.Series LIKE '%$keyword_sql%' OR
					{$bu}.BarCode LIKE '%$keyword_sql%' 		
					) ";
			}
			else {
				$keyword_cond2 = " AND (
					{$b}.Publisher LIKE '%$unconverted_keyword_sql%' OR {$b}.Publisher LIKE '%$converted_keyword_sql%' OR ";
				$keyword_cond2 .= ($acno_filter) ? $acno_filter ." OR " : "";		
				$keyword_cond2 .= "{$b}.CallNum LIKE '%$unconverted_keyword_sql%' OR {$b}.CallNum LIKE '%$converted_keyword_sql%' OR 
					{$b}.CallNum2 LIKE '%$unconverted_keyword_sql%' OR {$b}.CallNum2 LIKE '%$converted_keyword_sql%' OR 
					CONCAT({$b}.CallNum, ' ', {$b}.CallNum2) LIKE '%$unconverted_keyword_sql%' OR
					CONCAT({$b}.CallNum, ' ', {$b}.CallNum2) LIKE '%$converted_keyword_sql%' OR 
					{$b}.ISBN LIKE '%$unconverted_keyword_sql%' OR {$b}.ISBN LIKE '%$converted_keyword_sql%' OR 
					{$b}.ISBN2 LIKE '%$unconverted_keyword_sql%' OR {$b}.ISBN2 LIKE '%$converted_keyword_sql%' OR 
					{$b}.BookTitle LIKE '%$unconverted_keyword_sql%' OR {$b}.BookTitle LIKE '%$converted_keyword_sql%' OR 
					{$b}.BookSubTitle LIKE '%$unconverted_keyword_sql%' OR {$b}.BookSubTitle LIKE '%$converted_keyword_sql%' OR 
					{$b}.ResponsibilityBy1 LIKE '%$unconverted_keyword_sql%' OR {$b}.ResponsibilityBy1 LIKE '%$converted_keyword_sql%' OR 
					{$b}.ResponsibilityBy2 LIKE '%$unconverted_keyword_sql%' OR {$b}.ResponsibilityBy2 LIKE '%$converted_keyword_sql%' OR 
					{$b}.ResponsibilityBy3 LIKE '%$unconverted_keyword_sql%' OR {$b}.ResponsibilityBy3 LIKE '%$converted_keyword_sql%' OR 
					{$b}.Series LIKE '%$unconverted_keyword_sql%' OR {$b}.Series LIKE '%$converted_keyword_sql%' OR 
					{$bu}.BarCode LIKE '%$unconverted_keyword_sql%' OR {$bu}.BarCode LIKE '%$converted_keyword_sql%'		
					) ";
			}
			$ret .= $keyword_cond2;
		}	// end 	$keyword != ''
	}		// end not advance search
		
	return trim($ret); 
}	// end function getBibliographySearchFiler()

?>