<?php
/*
 * 	modifying: 
 * 	Log
 * 	Purpose:	show list of bibliography by search criteria for book recommendation
 *	@return:	array['layout']		-- listview of result
 *				array['result']		-- number of record 
 * 
 * 	Note !!!:	this function lookup INTRANET_ELIB_BOOK but not LIBMS_BOOK so that it can search eBook
 * 
 *  Date:   2018-12-03 (Cameron)
 *          - filter out expired books [case #C152289]
 * 
 *  Date:	2017-04-03 (Cameron)
 * 			- handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 				by stripslashes()
 *
 * 	Date:	2016-11-14 [Cameron]
 * 			- return -1 for no record found
 * 
 * 	Date:	2016-11-11 [Cameron]
 * 			- add search criteria: tag
 * 
 * 	Date:	2016-09-21 [Cameron] create this file
 * 
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."home/library_sys/admin/book/batch_modify_manage.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$task = $_REQUEST['task'];
$bookID = $_REQUEST['bookID'];
$bookTitle = trim($_POST['BookTitle']);
$author = trim($_POST['Author']);
$callNumber = trim($_POST['CallNumber']);
$tag = trim($_POST['Tag']);

if (!get_magic_quotes_gpc()) {
	$bookTitle	= stripslashes($bookTitle);
	$author		= stripslashes($author);
	$callNumber	= stripslashes($callNumber);
	$tag		= stripslashes($tag);
}

$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;
$sort = ($order == 1) ? "ASC" : "DESC";
$orderByField = $field + 1;

$bmm = new batch_modify_manage();
$bmm->setfield($field);
$bmm->setorder($order);

$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);

$cond = "";

if ($task == 'sorting') {
	
	if ($bookID) {
		$cond = " AND b.BookID IN ('" .str_replace(',', "','",$bookID) . "')";
	}
}
else {
	if (!empty($bookTitle)) {
		$unconverted_bookTitle_sql = mysql_real_escape_string(str_replace("\\","\\\\",$bookTitle));		// A&<>'"\B ==> A&<>\'\"\\\\B
		$converted_bookTitle_sql = special_sql_str($bookTitle);											// A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B
		if ($unconverted_bookTitle_sql == $converted_bookTitle_sql){
			$cond .= " AND b.Title LIKE '%$unconverted_bookTitle_sql%'";	
		}
		else {
			$cond .= " AND (b.Title LIKE '%$unconverted_bookTitle_sql%' OR b.Title LIKE '%$converted_bookTitle_sql%')";
		}
	}

	if (!empty($author)) {
		$unconverted_author_sql = mysql_real_escape_string(str_replace("\\","\\\\",$author));
		$converted_author_sql = special_sql_str($author);
		if ($unconverted_author_sql == $converted_author_sql){
			$cond .= " AND b.Author LIKE '%$unconverted_author_sql%'";	
		}
		else {
			$cond .= " AND (b.Author LIKE '%$unconverted_author_sql%' OR b.Author LIKE '%$converted_author_sql%')";
		}
	}

	if (!empty($callNumber)) {
		$unconverted_callNumber_sql = mysql_real_escape_string(str_replace("\\","\\\\",$callNumber));
		$converted_callNumber_sql = special_sql_str($callNumber);
		if ($unconverted_callNumber_sql == $converted_callNumber_sql){
			$cond .= " AND b.CallNumber LIKE '%$unconverted_callNumber_sql%'";	
		}
		else {
			$cond .= " AND (b.CallNumber LIKE '%$unconverted_callNumber_sql%' OR b.CallNumber LIKE '%$converted_callNumber_sql%')";
		}
	}
	
	if (!empty($tag)) {
		$unconverted_tag_sql = mysql_real_escape_string(str_replace("\\","\\\\",$tag));
		$converted_tag_sql = special_sql_str($tag);
		if ($unconverted_tag_sql == $converted_tag_sql){
			$cond .= " AND t.TagName LIKE '%$unconverted_tag_sql%'";	
		}
		else {
			$cond .= " AND (t.TagName LIKE '%$unconverted_tag_sql%' OR t.TagName LIKE '%$converted_tag_sql%')";
		}
		$joinTable = "LEFT JOIN INTRANET_ELIB_BOOK_TAG bt ON bt.BookID=b.BookID LEFT JOIN INTRANET_ELIB_TAG t ON t.TagID=bt.TagID";
	}
	else {
		$joinTable = "";
	}
}

$expiredBook = $lelibplus->getExpiredBookIDArr();
if (! empty($expiredBook)) {
    $cond .= ' AND b.BookID NOT IN (' . $expiredBook . ') ';
}

$sql = "SELECT 
			CONCAT(IF(b.Title IS NULL OR b.Title = '','".$Lang['General']['EmptySymbol']."',b.Title),
				IF(r.BookID is not null,'<span class=\"tabletextrequire\"> * </span>','')) AS BookTitle,
			IF((b.CallNumber='' OR b.CallNumber is null) ,'".$Lang['General']['EmptySymbol']."',b.CallNumber) AS CallNumber,
			b.BookID 
		FROM 
			INTRANET_ELIB_BOOK as b 
		LEFT JOIN INTRANET_ELIB_BOOK_RECOMMEND r ON r.BookID=b.BookID
		{$joinTable}
		WHERE b.`Publish`=1  
			{$cond}
		ORDER BY ". $orderByField. " " . $sort;

$rs = $lelibplus->returnResultSet($sql);
$nrRs = count($rs);

 
$x = '<table id="book_item_table" class="common_table_list_v30  view_table_list_v30" style="width:98%">
			<tr>
				<th width="1">#</th>';
				
$pos = 0;						
$x .= '			<th width="75%">'.$bmm->column($pos++, $Lang["libms"]["book"]["title"]).'</th>
				<th width="20%">'.$bmm->column($pos++, $Lang["libms"]["book"]["call_number"]).'</th>				
				<th class="num_check" width="1%"><input type="checkbox" id="checkall" name="checkall" onclick="Set_Checkbox_Value(\'BookID[]\',this.checked)" checked /></th>
			</tr>';
			
$BookID = array();
$ret = array();

if($nrRs>0){		
	for ($i=0; $i<$nrRs; $i++){
		$x .= '<tr id="'.$rs[$i]['BookID'].'">
					<td>'.($i+1).'</td>
					<td>'.$rs[$i]['BookTitle'].'</td>
					<td>'.$rs[$i]['CallNumber'].'</td>
					<td><input type="checkbox" id="BookID[]" name="BookID[]" value="' . $rs[$i]['BookID'] . '" checked /></td>
				</tr>';
	}
	$ret['result'] = 1;
}
else{
	 # no record
	$x.="<tr id='no_record_row' class=\"tablebluerow2 tabletext\">
			<td class=\"tabletext\" colspan=\"4\" align=\"center\" height=\"40\" style=\"vertical-align:middle\">$i_no_record_exists_msg</td>
		</tr>";
	$ret['result'] = -1;
}
$x .= '</table>';

$ret['layout'] = remove_dummy_chars_for_json($x);
$json = new JSON_obj();
echo $json->encode($ret);

intranet_closedb();
?>