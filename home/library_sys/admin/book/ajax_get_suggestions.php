<?php
/* 
 * Editing by  
 * 
 * Note: page called for using jquery.autocomplete.js only 
 * 
 * @Param	q : 	search string
 * 			field:	field name for which to retireve
 * 
 *  2017-04-03 [Cameron]
 * 		- apply intranet_undo_htmlspecialchars() to handle special characters like <>&"
 * 
 * 	2017-01-10 [Cameron]
 * 		- add field: Tag, Author
 * 
 * 	2016-04-29 [Cameron]
 * 		- copy and modify from ajax_get_book_suggestion.php
 *  
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
########################################################################################################


## Get Data
$q = (isset($q) && $q != '') ? $q : '';

$field = (isset($field) && $field != '') ? $field : '';


## Init
$x = '';


## Use Library
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']  )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


## Maing
if (($q != '') && ($field != '')) {
	$limit = 100;
	$validStr = false;
	$extraFilter = '';
	# search with input keyword
	switch ($field) {
		case 'Distributor':
			$table = 'LIBMS_BOOK_UNIQUE';
			$selectField = $field;
			$orderBy = $field;
			$validStr = true;
			break;
		case 'Purchasedept':
			$table = 'LIBMS_BOOK_UNIQUE';
			$selectField = 'PurchaseByDepartment';
			$orderBy = 'PurchaseByDepartment';
			$validStr = true;
			break;
		case 'BookCountry':
			$table = 'LIBMS_BOOK';
			$selectField = 'Country';
			$orderBy = 'Country';
			$validStr = true;
			break;
		case 'BookPublisher':
			$table = 'LIBMS_BOOK';
			$selectField = 'Publisher';
			$orderBy = 'Publisher';
			$validStr = true;
			break;
		case 'BookPublishplace':
			$table = 'LIBMS_BOOK';
			$selectField = 'PublishPlace';
			$orderBy = 'PublishPlace';
			$validStr = true;
			break;
		case 'BookSeries':
			$table = 'LIBMS_BOOK';
			$selectField = 'Series';
			$orderBy = 'Series';
			$validStr = true;
			break;
		case 'BookResponb1':
			$table = 'LIBMS_BOOK';
			$selectField = 'ResponsibilityBy1';
			$orderBy = 'ResponsibilityBy1';
			$validStr = true;
			break;
		case 'BookResponb2':
			$table = 'LIBMS_BOOK';
			$selectField = 'ResponsibilityBy2';
			$orderBy = 'ResponsibilityBy2';
			$validStr = true;
			break;
		case 'BookResponb3':
			$table = 'LIBMS_BOOK';
			$selectField = 'ResponsibilityBy3';
			$orderBy = 'ResponsibilityBy3';
			$validStr = true;
			break;
		case 'BookSubj':
			$table = 'LIBMS_BOOK';
			$selectField = 'Subject';
			$orderBy = 'Subject';
			$extraFilter = " AND (RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL)";
			$validStr = true;
			break;
		case 'BookLang':
			$table = 'LIBMS_BOOK';
			$selectField = 'Language';
			$orderBy = 'Language';
			$extraFilter = " AND (RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL)";
			$validStr = true;
			break;
		case 'Tag':				// used in batch recommend book add new
			$table = 'LIBMS_TAG';
			$selectField = 'TagName';
			$orderBy = 'TagName';
			$validStr = true;
			$limit = 10;
			break;
		case 'Author':				// used in batch recommend book add new
			$table = 'LIBMS_BOOK';
			$selectField = 'ResponsibilityBy1';		// as INTRANET_ELIB_BOOK only store ResponsibilityBy1 as author, not need to look up ResponsibilityBy2 & ResponsibilityBy3
			$orderBy = 'ResponsibilityBy1';
			$validStr = true;
			$limit = 10;
			break;
	}
	
	if ($validStr) {
		$filter = " where ".$selectField." is not null and ".$selectField." <> '' and ".$selectField." like '%".$q."%'".$extraFilter;
		$sql = "select distinct ".$selectField." from ".$table.$filter." order by ".$selectField." limit ".$limit;
		$result = $libms->returnArray($sql,null,2);	// numeric array only
		
		if(!empty($result)){
			foreach ($result as $row){
				$res = intranet_undo_htmlspecialchars($row[0]);
				$x .= $res."|".$res."\n";
			}
		}
	}
}

echo $x;