<?php
// using:  

// IMPORTANT: UTF-8 Encoding
/*
 * 	Log:
 * 
 * 	Date:	2019-03-06 [Henry]
 * 			- added column class name and class number
 * 
 * 	Date:	2015-10-06 [Cameron]
 * 			- fix bug on handling special characters in keyword search: &<>'"\
 * 
 * 	Date:	2015-03-12 [Cameron] use LIBMS_USER instead of {intranet_db}.INTRANET_USER when counting loans
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
################################################################################################################
if(!isset($BookID) || empty($BookID)){
	header("Location: index.php");
}
### Set Cookies
$arrCookies = array();
$arrCookies[] = array("elibrary_plus_mgmt_item_page_field", "field");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_order", "order");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_size", "numPerPage");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_number", "pageNo");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_keyword", "keyword");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_book_status", "BookStatus");


if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
{
	updateGetCookies($arrCookies);
}
################################################################################################################

## Get Data
$BookStatus = (isset($BookStatus) && $BookStatus != '') ? $BookStatus : '';
$keyword = (isset($keyword) && $keyword != '') ? trim($keyword) : '';
$FromPage = (isset($FromPage) && $FromPage != '') ? $FromPage : 'book';

if (isset($elibrary_plus_mgmt_item_page_size) && $elibrary_plus_mgmt_item_page_size != "") 
	$page_size = $elibrary_plus_mgmt_item_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

## Use Library
# in all boook management PHP pages
$libms = new liblms();

# Access Checking
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$li = new libdbtable2007($field, $order, $pageNo);
## Data Table
//global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";

## Init 
$btnOption = ''; 
$CurTab = 'CirculationDetails';

## Preparation
$CurrentPageArr['LIBMS'] = 1;
//$CurrentPage = "PageBookManagementBookList";
$CurrentPage = $libms->get_book_manage_cur_page($FromPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$libms->MODULE_AUTHENTICATION($CurrentPage);
$TAGS_OBJ[] = array($Lang["libms"]["action"][strtolower($FromPage)]);

$BookID_For_CONCAT = (isset($junior_mck)) ? "CONVERT(bl.UniqueID USING utf8)" : "bl.UniqueID";



# Filter - Book Status
$ParTags = ' id="BookStatus" name="BookStatus" onchange="document.form1.submit()"';
$BookStatusSelection = $libms->GET_BOOK_ITEM_SELECTION($ParTags, '', $BookStatus);

# Location filter 
$locationSelectionBox = '';
$locationInfo = $libms->GET_LOCATION_INFO('',1);

$numOfLocationInfo = count($locationInfo);
$locationSelectionOption = array();

$locationSelectionOption[] = array('NA', $Lang["libms"]["status"]["na"]);

for($i=0;$i<$numOfLocationInfo;$i++){

	$thisLocationCode = $locationInfo[$i]['LocationCode'];
	$thisLocationEngName = $locationInfo[$i]['DescriptionEn'];
	$thisLocationChiName = $locationInfo[$i]['DescriptionChi'];
	$thisOptionlocationName = Get_Lang_Selection($thisLocationChiName,$thisLocationEngName);
	$locationSelectionOption[]= array($thisLocationCode,$thisOptionlocationName);
}
$locationSelectionBox = $linterface->GET_SELECTION_BOX($locationSelectionOption, 'name="LocationCode" id="LocationCode" onchange="document.form1.submit()"', "-- {$Lang['libms']['bookmanagement']['location']} --", $LocationCode);
$BookStatusSelection .= $locationSelectionBox;
$userNameField = Get_Lang_Selection('iu.ChineseName', 'iu.EnglishName');
//$UserName_For_CONCAT = (isset($junior_mck)) ? "CONVERT(".$userNameField." USING utf8)" : $userNameField;
## DB SQL
$sql = "SELECT IFNULL(COUNT( * ) ,0) FROM  LIBMS_RESERVATION_LOG WHERE ( RecordStatus = 'WAITING' OR RecordStatus = 'READY') AND BookID = '".$BookID."'";
$reserve_cnt = current($li->returnVector($sql));
$sql = "SELECT 
			bu.ACNO,  
			bu.BarCode, 
			CASE TRIM(bu.RecordStatus)
				WHEN 'BORROWED' THEN '".$Lang['libms']['book_status']['BORROWED']."'
				WHEN 'LOST' THEN '".$Lang['libms']['book_status']['LOST'] ."'
				WHEN 'NORMAL' THEN '".$Lang['libms']['book_status']['NORMAL'] ."'
				WHEN 'REPAIRING' THEN '".$Lang['libms']['book_status']['REPAIRING'] ."'
				WHEN 'RESERVED' THEN '".$Lang['libms']['book_status']['RESERVED'] ."'
				WHEN 'SHELVING' THEN '".$Lang['libms']['book_status']['SHELVING'] ."'
				WHEN 'WRITEOFF' THEN '".$Lang['libms']['book_status']['WRITEOFF'] ."'
				WHEN 'SUSPECTEDMISSING' THEN '".$Lang['libms']['book_status']['SUSPECTEDMISSING'] ."'
				ELSE '".$Lang['General']['EmptySymbol']."'
			END AS Item_RecordStatus, 
		";
$sql .= "
			CASE TRIM(bu.LocationCode) WHEN 'DUMMY_FOR_NO_PROBLEM' THEN '---' 
";
for($i=1;$i<$numOfLocationInfo;$i++){
			$sql .= " WHEN '".$locationSelectionOption[$i][0]."' THEN '".$locationSelectionOption[$i][1]."'";
}
$sql .= "
				ELSE '".$Lang['General']['EmptySymbol']."' 
			END AS Location, 
";
$sql .= "
			CONCAT('<a href=\"book_item_borrow_record.php?BookID=".$BookID."&FromPage=".$FromPage."&UniqueID=',".$BookID_For_CONCAT.",'\">',COUNT(*),'</a>') as BorrowCount,  
			GROUP_CONCAT(BorrowTime order by BorrowTime desc) As BorrowTime,  
			GROUP_CONCAT(".$userNameField." order by BorrowTime desc) as UserName,
			GROUP_CONCAT(iu.ClassName order by BorrowTime desc), GROUP_CONCAT(iu.ClassNumber order by BorrowTime desc)
		FROM 
			LIBMS_BORROW_LOG bl
		INNER JOIN
			LIBMS_BOOK_UNIQUE bu ON bu.UniqueID = bl.UniqueID
		INNER JOIN
			LIBMS_USER iu ON bl.UserID = iu.UserID			
		WHERE 
			(bu.RecordStatus NOT LIKE 'DELETE' OR bu.RecordStatus IS NULL) 
		AND
			bu.BookID = '".$BookID."'
		";
# DB - condition
$bookStatus_cond = '';
if($BookStatus != ''){
	$sql .= " and bu.RecordStatus = '".$BookStatus."' ";
	
	//for Distributor filter
	$bookStatus_cond = " and bu.RecordStatus = '".$BookStatus."' ";
}

$keyword_cond ='';
if($keyword != "")
	$unconverted_keyword_sql = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));		// A&<>'"\B ==> A&<>\'\"\\\\B
	$converted_keyword_sql = special_sql_str($keyword);											// A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B

	//for Distributor filter
	$keyword_cond = " AND (
		bu.InvoiceNumber LIKE '%$unconverted_keyword_sql%' OR bu.InvoiceNumber LIKE '%$converted_keyword_sql%' OR 
		bu.ACNO LIKE '%$unconverted_keyword_sql%' OR bu.ACNO LIKE '%$converted_keyword_sql%' OR 
		bu.PurchaseNote LIKE '%$unconverted_keyword_sql%' OR bu.PurchaseNote LIKE '%$converted_keyword_sql%'
	) ";
	$sql .= $keyword_cond; 
//echo $sql;die();

## Location Code Sql Cond
if($LocationCode != ''){
	if($LocationCode =='NA'){
		$sql .= " AND (bu.LocationCode = '' OR bu.LocationCode is null)" ;
	}else{
		$sql .= " AND bu.LocationCode = '".$LocationCode."' " ;
	}
}

$sql .= "		
		GROUP BY
			bu.ACNO,  
			bu.BarCode, 
			bu.RecordStatus
"; 
// BookdCode = ACNO 
$li->field_array = array("ACNO", "BarCode", "Item_RecordStatus", "Location", "BorrowCount", "BorrowTime", "UserName", "ClassName", "ClassNumber");

$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "eLibPlus_Circulation_Detail_Table";
//$li->no_navigation_row = true;

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n"; 
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['code'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['barcode'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['bookStatus'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['location'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['totalBorrow'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['last_borrow_time'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['last_borrow_user'])."</th>\n";
$li->column_list .= "<th width='5%'>".$li->column($pos++, $Lang['libms']['bookmanagement']['ClassName'])."</th>\n";
$li->column_list .= "<th width='5%'>".$li->column($pos++, $Lang['libms']['bookmanagement']['ClassNumber'])."</th>\n";
$keyword = htmlspecialchars($keyword);
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"{$keyword}\" onkeyup=\"Check_Go_Search(event);\" />";



############################################################################################################

# Top menu highlight setting
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

$BookSubTab = $libms->get_book_manage_sub_tab($FromPage,$CurTab,$BookID,$UniqueID);

$RespondInfo = $libms->GET_BOOK_INFO($BookID);
$BookTitle = $RespondInfo[0]['BookTitle'];

$htmlAry['cancelBtn'] = '';
if (strtolower($FromPage)=='periodical') {
	$htmlAry['cancelBtn'] = $libms->get_book_manage_cancel_button($BookID, $FromPage);
}
?>

<table class="form_table" width="100%">
<tr>
  <td align="left"><font face="微軟正黑體" size="+1">《 <b><?=$BookTitle?></b>》</font></td>
</tr>
</table>
<br style="clear:both" />	
<?=$BookSubTab?>
<form name="form1" id="form1" method="POST" action="circulation_details.php?FromPage=<?=$FromPage?>&BookID=<?=$BookID?>&UniqueID=<?=$UniqueID?>" onSubmit="return checkForm()">
	<div class="content_top_tool"  style="float: left;">

		</div>
	<div class="content_top_tool"  style="float: right;">
		<div class="Conntent_search"><?=$searchTag?></div>     
		<br style="clear:both" />
	</div>
	<div><?=$Lang['libms']['book']['reserve_sum']?>: <?=$reserve_cnt?></div>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td valign="bottom">
		<?=$BookStatusSelection?>
	</td>

	</tr>
	</table>

	<?=$li->display();?>
	<br />
	
	<div class="edit_bottom_v30">
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
	
	<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
	<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
	<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
	<input type="hidden" name="page_size_change" id="page_size_change" value="" />
	<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
	<input type="hidden" name="FromPage" id="FromPage" value="<?=$FromPage?>" />
</form>


<?php
$linterface->LAYOUT_STOP();

################################################################################################################
intranet_closedb();
?>