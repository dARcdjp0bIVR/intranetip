<?php

// editing by:  
/*
 * 	Log
 * 
 * 	Date: 	2017-04-06 [Cameron]
 * 			handle apostrophe problem for TagName when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 			by stripslashes
 *
 * 	Date:	2014-11-20 [Cameron] Fix bug (duplicate record) for edit tag, pass new BookID array rather than whole selected BookID array to AssignTagToBooks
 * 				
 */


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();


$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$LibeLib = new elibrary();
$TagName = trim($TagName);
if (!get_magic_quotes_gpc()) {
	$TagName 	= stripslashes($TagName);
}

$IsEdit = (isset($TagID) && $TagID>0);

$ret = array();

if (!$IsEdit)
{
	# search if same tag name exists (if so, edit)
	$sql = "SELECT TagID FROM INTRANET_ELIB_TAG WHERE TagName='".$LibeLib->Get_Safe_Sql_Query($TagName)."'";
	$result = $LibeLib->returnVector($sql);
	if ($result[0]!="")
	{
		$IsEdit = true;
		$TagID = $result[0];
	} else
	{
		# add to DB
		$TagIDArray = $LibeLib->returnTagIDByTagName($TagName);
		$TagID = $TagIDArray[0];
	}
	
	# add related books
	$LibeLib->AssignTagToBooks($TagID, $BookIDSelected);
	$xmsg = "AddSuccess";
} else
{
	## Not apply "START TRANSACTION" and "COMMIT" because of DB engine not supported in EJ (ref libdb.php comment)?
		
	# 1. update TagName
	$sql = "UPDATE INTRANET_ELIB_TAG SET TagName='".$LibeLib->Get_Safe_Sql_Query($TagName)."' WHERE TagID='{$TagID}' ";
	$ret[] = $LibeLib->db_db_query($sql);
	
	# 2. remove not in current selection
	$sql = "DELETE FROM INTRANET_ELIB_BOOK_TAG WHERE TagID='{$TagID}' ";
	if (is_array($BookIDSelected) && sizeof($BookIDSelected)>0)
	{
		$booksKept = implode("','", $BookIDSelected);
		$sql .= " AND BookID NOT IN ('{$booksKept}') ";
	}
	
	$ret[] = $LibeLib->db_db_query($sql);
	
	# 3. Get new selected BookID
	$sql = "SELECT BookID FROM INTRANET_ELIB_BOOK_TAG WHERE TagID='{$TagID}' ORDER BY BookID";
	$existingRs = $LibeLib->returnVector($sql);
	
	if (is_array($BookIDSelected) && sizeof($BookIDSelected)>0)
	{
		$newSelectedBookID = array_diff($BookIDSelected,$existingRs);
		sort($newSelectedBookID);
		
		# 4. add new selection
		if ($newSelectedBookID) {
			$ret[] = $LibeLib->AssignTagToBooks($TagID, $newSelectedBookID);
		}			
	}
	
	$xmsg = in_array(false,$ret) ? "UpdateUnsuccess" : "UpdateSuccess";
	
}

intranet_closedb();

header("location: elib_setting_tags.php?xmsg=".$xmsg)
?>