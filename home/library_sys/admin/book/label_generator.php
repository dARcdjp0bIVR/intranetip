<?php 

//Modifying by 

/********************
 * Log :
 * 
 * 2020-03-23 [Henry]
 * 			bug fix for fail to display call number [Case#D182535]
 * 
 * 2018-01-19 [Cameron]
 * 			retrieve ItemSeriesNum to print if lable is 'Book Back' [case #C134185]
 * 
 * 2017-11-14 [Henry]
 * 			Add text_alignment selection box [case #E130799]
 * 
 * Date:	2017-09-08 [Cameron]
 * 			- apply stripslashes() to school name to handle apostrophe problem when get_magic_quotes_gpc() is not set (php5.4). [case #S125200]
 * 
 * Date:	2017-07-18	Henry
 *			support sort by BookID, ACNO, CallNum
 *
 * Date		2017-03-24 [Cameron]
 * 			bold style applies to bookback only
 * 
 * Date		2017-01-12 [Henry]
 * 			fix the chinese chararcter error when show class name and number [Case#J111531]
 * 
 * Date		2016-03-11 [Henry]
 * 			add option to support chi and eng school language
 * 
 * Date		2015-09-01 [Henry]
 * 			make spin label text align to left
 * 
 * Date		2014-08-18 [Henry]
 * 			use htmlspecialchars_decode to decode the book title string
 * 
 * Date		2014-02-07 [Henry]
 * 			shorten the book title if the no of char > 50 before generate the barcode
 * 
 * Date		2013-10-30 [Henry]
 * 			use the display name of school if set to print the label
 * 
 ********************/

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

require_once('LabelPrinterTCPDF.php');
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");


intranet_auth();
intranet_opendb();
# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


if (empty($_REQUEST['unique_book_ids']) && $_REQUEST['radio_label'] != "readerbarcode"){
	echo $Lang['libms']['gen_label']['no_book_selected'];
	exit;
}
$unique_book_ids = $_REQUEST['unique_book_ids'];

//Henry Modified
if (empty($_REQUEST['AddUserID']) && $_REQUEST['radio_label'] == "readerbarcode"){
	echo "No Reader(s) selected";
	exit;
}

if (empty($_REQUEST['label_id'])){
	echo $Lang['libms']['gen_label']['no_label_format'];
	exit;
}
$label_id = $_REQUEST['label_id'];

$label_format = $libms->SELECTFROMTABLE('LIBMS_LABEL_FORMAT','*',array('id' => $label_id));
$label_format = $label_format[0];
$label_format['paper-size'] =array($label_format['paper-size-width'], $label_format['paper-size-height']);

if (($_REQUEST['radio_label'] != "bookback") && $label_format['is_bold']) {
	$label_format['is_bold'] = 0;	// bold style applies to bookback only
} 


if (empty($_REQUEST['fields_to_print'])){
	echo $Lang['libms']['gen_label']['no_print_fields'];
	exit;
}
$fields_to_print = $_REQUEST['fields_to_print'];

$print_barcode = empty($_REQUEST['radio_label'])? false: $_REQUEST['radio_label'] =='full' ;

$print_label_border = empty($_REQUEST['print_label_border'])? false: true;



//Henry Modified
if($_REQUEST['radio_label'] == "readerbarcode"){
	//debug_pr($_REQUEST['MemberList']);
//	if(in_array('readerClassName',$fields_to_print) && in_array('readerClassNumber',$fields_to_print)){ //debug_pr("debugging!");
//		$tempStatement = "CONCAT(IF(`ClassName` IS NULL,'',IF(`ClassNumber` IS NULL,`ClassName`,CONCAT(`ClassName`,' - '))), IF(`ClassNumber` IS NULL,'',`ClassNumber`)) as readerClassNameNumber";
//	}
//	else
		$tempStatement = "IF(`ClassName` IS NULL,'',`ClassName`) as readerClassName, IF(`ClassNumber` IS NULL,'',`ClassNumber`) as readerClassNumber";
	$imploded_user_ids = implode(',', $_REQUEST['AddUserID']);
	$sql = <<<EOF000EOF
	SELECT `BarCode`, `EnglishName` as readerEngName, `ChineseName` as readerChiName, {$tempStatement}
	FROM `LIBMS_USER`
	WHERE
		`UserID` IN  ({$imploded_user_ids})
	ORDER BY 
		`ClassName`, `ClassNumber`
EOF000EOF;
//debug_pr($sql);
$result = $libms->returnArray($sql);
//debug_pr($result);
//exit;
}
else{
$imploded_book_ids = implode(',', $unique_book_ids);
$order_cond = 'bu.ACNO';
if($radio_sort_by == 'book_id'){
	$order_cond = 'b.BookID, bu.ACNO';
}else if($radio_sort_by == 'acno'){
	$order_cond = 'bu.ACNO';
}else if($radio_sort_by == 'call_num'){
	$order_cond = 'b.CallNum, bu.ACNO';
}
$sql = <<<EOF000EOF
	SELECT bu.ACNO AS BookCode, b.`CallNum`,b.`CallNum2`, bu.`LocationCode`, b.`BookTitle`, bu.`BarCode`, b.`SeriesNum`, bu.`ItemSeriesNum` 
	FROM `LIBMS_BOOK_UNIQUE` bu
	JOIN `LIBMS_BOOK` b
		ON b.`BookID` = bu.`BookID`
	WHERE
		bu.`UniqueID` IN  ({$imploded_book_ids})
	ORDER BY 
		{$order_cond}
EOF000EOF;
$result = $libms->returnArray($sql);
}

 
$pdf = new LabelPrinterTCPDF($label_format);
 
//Henry Modified 20131030
//for setting the start position
if (!empty($_REQUEST['startPosition'])){
	for($i=1; $i<$_REQUEST['startPosition'];$i++)
		$pdf->AddLabel();
}
	
foreach($result as $book){
	if (!empty($_REQUEST['print_school_name'])){
		
		$schDisplayName = $libms->get_system_setting('school_display_name');
		$schDisplayNameEng = $libms->get_system_setting('school_display_name_eng');
		
		if(trim($schDisplayName)!='' && trim($schDisplayNameEng)!=''){
			if($_REQUEST['radio_school_name_lang'] == 'eng'){
				$schDisplayName = $schDisplayNameEng;
			}
		}
		
		if (!get_magic_quotes_gpc()) {
			$schDisplayName = stripslashes($schDisplayName);
		}
				
		if(trim($schDisplayName) != ""){
			$labeltext['print_school_name'] = $schDisplayName;
		}
		else{
			$labeltext['print_school_name'] =  (isset($junior_mck)) ? iconv("big5","utf-8//IGNORE",$_SESSION["SSV_PRIVILEGE"]["school"]["name"]) : $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
		}	
	}
//	debug_pr($fields_to_print);
	foreach( $fields_to_print as $field_to_print ){
		$book['BookTitle'] = htmlspecialchars_decode($book['BookTitle']);
		//shorten the book title if the no of char > 50 (method 1)
		if($field_to_print == "BookTitle" && ($countWords = preg_match_all('/./u', $book[$field_to_print], $matches)) > 50){
			$shortenTitleFirst = "";
			$shortenTitleLast = "";
			for($i=0;$i<22;$i++){
				$shortenTitleFirst .= $matches[0][$i];
				$shortenTitleLast .= $matches[0][$countWords-22 + $i];
			}
			$shortenTitle = $shortenTitleFirst."...".$shortenTitleLast;
			$book['BookTitle'] = $shortenTitle;
			//debug_pr($matches);
			//debug_pr($shortenTitle);
		}

//		//shorten the book title if the no of char > 50 (method 2)
//		if($field_to_print == "BookTitle"){
//			$length = 50;
//			$valueEncoding = mb_detect_encoding( $book[$field_to_print], 'auto', true );
//		    if (mb_strwidth( $book[$field_to_print], $valueEncoding ) > $length) {
//			    $firstWidth = ceil( $length/2 );
//			    $secondStart = mb_strwidth( $book[$field_to_print], $valueEncoding ) - ( $length - $firstWidth );
//			    $secondWidth = $length - $firstWidth +1;
//			    $book[$field_to_print] = mb_strimwidth( $book[$field_to_print], 0, $firstWidth, '...', $valueEncoding ) . mb_substr( $book[$field_to_print], $secondStart, $secondWidth, $valueEncoding );
//		    }
//		}
		if($_REQUEST['radio_label'] == "full" || $_REQUEST['radio_label'] == "readerbarcode"){
			$labeltext[$field_to_print] = $book[$field_to_print];
			if($field_to_print == "full_barcode_text" && $_REQUEST['radio_label'] == "full" )
				$labeltext['barcode_text'] = 1;
			else if($field_to_print == "reader_barcode_text" && $_REQUEST['radio_label'] == "readerbarcode" )
				$labeltext['barcode_text'] = 1;
		}
		else
			$labeltext["spin_".$field_to_print] = $book[$field_to_print];
	}
	//Henry Modified
	if(in_array('readerClassName',$fields_to_print) && in_array('readerClassNumber',$fields_to_print)){
		$labeltext['readerClassNameNumber'] = $book['readerClassName'].($book['readerClassNumber']?' - '.$book['readerClassNumber']:'');
		unset($labeltext['readerClassName']);
		unset($labeltext['readerClassNumber']);
	}
	if((in_array('CallNum',$fields_to_print) || in_array('CallNum2',$fields_to_print)) && $_REQUEST['radio_label'] == "full"){
		$labeltext['CallNumCallNum2'] = (in_array('CallNum',$fields_to_print)?$book['CallNum']:'').(in_array('CallNum2',$fields_to_print)?' '.$book['CallNum2']:'');
		unset($labeltext['CallNum']);
		unset($labeltext['CallNum2']);
	}	

	if($_REQUEST['radio_label'] == "readerbarcode"){
		$pdf->AddLabel($labeltext,$book['BarCode'],$print_label_border, '', $label_format['text_alignment']);
	}
	else if ($print_barcode){
//		//-----------Henry added 20131030 for the ordering of the book info [start]
		$order=array();
//		$order[0] = 'print_school_name';
//		$order[1] = 'BookTitle';
//		$order[2] = 'BookCode';
//		$order[3] = 'BarCode';
//		$order[4] = 'CallNumCallNum2';
//		$order[5] = 'LocationCode';
		for($i=0; $i<6; $i++){
			if($label_format['info_order_'.($i+1)]){
				$order[$i] = $label_format['info_order_'.($i+1)];
			}
			else{
				$order="";
				break;
			}
		}
//		//-----------Henry added 20131030 for the ordering of the book info [end]
		$pdf->AddLabel($labeltext,$book['BarCode'],$print_label_border, $order, $label_format['text_alignment']);
	}else{
//		//-----------Henry added 20131030 for the ordering of the book info [start]
		$order=array();
//		$order[0] = 'print_school_name';
//		$order[1] = 'BookTitle';
//		$order[2] = 'BookCode';
//		$order[3] = 'BarCode';
//		$order[4] = 'CallNumCallNum2';
//		$order[5] = 'LocationCode';
		$j = 0;
		for($i=0; $i<6; $i++){
			if($label_format['info_order_'.($i+1)]){
				if($label_format['info_order_'.($i+1)] == 'CallNumCallNum2'){
					$order[$j] = 'spin_CallNum';
					$order[$j+1] = 'spin_CallNum2';
					$j++;
				}
				else if($label_format['info_order_'.($i+1)] == 'BookTitle'){
					$order[$j] = 'spin_SeriesNum';
					$order[$j+1] = 'spin_ItemSeriesNum';
					$j++;
				}
				else
					$order[$j] = 'spin_'.$label_format['info_order_'.($i+1)];
				$j++;
			}
			else{
				$order="";
				break;
			}
		}
//		//-----------Henry added 20131030 for the ordering of the book info [end]
		//$labeltext[]='NB';
		$pdf->AddLabel($labeltext,null,$print_label_border, $order, $label_format['text_alignment']);
	}
	unset($labeltext);
}
$filename = 'label' . time() % 10000 . '.pdf';
$pdf->Output('$filename', 'I');
//$pdf->Output($filename, 'D'); //Henry:2013/10/22



?>