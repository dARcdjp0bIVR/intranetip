<?php
#using:  

/***************************************
 * 
 * Date:	2016-02-11 [Cameron]
 * 			- move sql to liblibrarymgmt.php > returnSQLforLostReport()
 *  		- add column: ListPrice and Penalty
 * Date:	2015-09-07 [Henry]
 * 			access right bug fix
 * Date: 	2014-04-04 (Henry)
 * Details: Create this page
 ***************************************/

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$admin_right = $_SESSION['LIBMS']['admin']['current_right']['reports'];

if (!$admin_right['lost report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();
$libms = new liblms();

$sql = $libms->returnSQLforLostReport();
$field = array("DateModified", "UserName","ACNO","CallNum", "BookTitle", "ResponsibilityBy1","PurchasePrice","ListPrice","Penalty");
$orderCond = ' order by '.$field[0].' desc';

if(isset($ck_right_page_order)){
	$order = ($ck_right_page_order == 0 ? 'desc':'');	// present order status
	$orderCond = ' order by '.$field[$ck_right_page_field].' '.$order;
}
$sql .= $orderCond;

$results = $libms->returnResultSet($sql);
// copy [end] -----------------------------------------------------------------------------------------------

# Define Column Title
$exportColumn = array();
$exportColumn[0][] = $Lang["libms"]["book"]["lost_date"];
$exportColumn[0][] = $Lang["libms"]["book"]["user_name"];
$exportColumn[0][] = $Lang["libms"]["label"]["BookCode"];
$exportColumn[0][] = $Lang["libms"]["book"]["call_number"];
$exportColumn[0][] = $Lang["libms"]["book"]["title"];
$exportColumn[0][] = $Lang["libms"]["book"]["responsibility_by1"];
$exportColumn[0][] = $Lang["libms"]["book"]["purchase_price"];
$exportColumn[0][] = $Lang["libms"]["book"]["list_price"];
$exportColumn[0][] = $Lang["libms"]["report"]["pay"];

$ExportArr = array();

for ($i=0; $i<sizeof($results); $i++)
{
	$thisBook = $results[$i];
	$ExportArr[$i][] = $thisBook["DateModified"];
	$ExportArr[$i][] = $thisBook["UserName"];
	$ExportArr[$i][] = $thisBook["ACNO"];
	$ExportArr[$i][] = $thisBook["CallNum"];
	$ExportArr[$i][] = $thisBook["BookTitle"];
	$ExportArr[$i][] = $thisBook["ResponsibilityBy1"];
	$ExportArr[$i][] = $thisBook["PurchasePrice"];
	$ExportArr[$i][] = $thisBook["ListPrice"];
	$ExportArr[$i][] = $thisBook["Penalty"];
}

$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

$filename = "lost_book_report(".$_REQUEST['DueDateAfter']."_to_".$_REQUEST['DueDateBefore'].").csv";

# Output The File To User Browser
$lexport->EXPORT_FILE($filename, $export_content, $isXLS = false, $ignoreLength = false, $BaseCode="UTF-8");


?>