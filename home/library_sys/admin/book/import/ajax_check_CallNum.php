<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
####### Trim all request #######
function recursive_trim(&$array){
	foreach($array as &$item){
		if (is_array($item)){
			recursive_trim($item);
		}else{
			if (is_string($item))
				$item=trim($item);
		}
	}
}
recursive_trim($_REQUEST);
####### END  OF   Trim all request #######
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();


if(isset($_REQUEST['callnum']))//If a username has been submitted 
{
	$callnum = mysql_real_escape_string($_REQUEST['callnum']);//Some clean up :)
	
	if(isset($_REQUEST['unique_barcodeid']))
	{
		$unique_barcodeid = mysql_real_escape_string($_REQUEST['unique_barcodeid']);//Some clean up :)
		$check_for_barcode =  $libms->SELECTVALUE('LIBMS_BOOK', '*', 'BookID', '(`CallNum`="'.$callnum.'" OR `CallNum2`="'.$callnum.'")   AND `UniqueID` NOT LIKE "'.$unique_barcodeid.'"');	
	} else {
		$check_for_barcode =  $libms->SELECTVALUE('LIBMS_BOOK', '*', 'BookID', '`BarCode`="'.$callnum.'"');	
	}

	if(!empty($check_for_barcode))
	{
		echo '1';//If there is a  record match in the Database - Not Available
	} else {
		echo '0';//No Record Found - Username is available 
	}

}

?>
