<?php

/**
 * Modifing by 
 * 
 * Log :
 * 
 * Date:	2017-09-15 [Cameron]
 * Details:	add Remark point 3 (case #F121218)
 * 
 * Date:	2014-12-10 [Henry Chan]
 * Details:	add option to allow cover image input by ISBN
 * 
 * Date:	2013-07-23 [Henry Chan]
 * Details:	finished the function of import book cover
 * 
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
	function magicQuotes_awStripslashes(& $value, $key) {
		$value = stripslashes($value);
	}
	$gpc = array (
		& $_GET,
		& $_POST,
		& $_COOKIE,
		& $_REQUEST
	);
	array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");

include_once ($PATH_WRT_ROOT . "includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
	include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
*/
$libms = new liblms();

$CurrentPageArr['LIBMS'] = 1;
//$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_ist'], "index.php");
//$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");

$CurrentPage = "PageBookManagementBookList";

$linterface = new interface_html("libms.html");

//$linterface = new interface_html();

############################################################################################################
///*

global $eclass_prefix;

$toolbar = ''; //$linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'label_format_new.php')",$button_new,"","","",0);

//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array (
	$Lang['libms']['import_book_cover']
);

# step information
$STEPS_OBJ[] = array (
	$Lang['libms']['select_image_file'],
	1
);
$STEPS_OBJ[] = array (
	$iDiscipline['Confirmation'],
	0
);
$STEPS_OBJ[] = array (
	$i_general_imported_result,
	0
);

$sample_file = "book_cover_sample.zip";
$csvFile = "<a class=\"tablelink\" href=\"get_sample_file.php?file=$sample_file\" target=\"_self\">" . $Lang['General']['ClickHereToDownloadSample'] . "</a>";

$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array (
	$Lang['libms']['bookmanagement']['book_list'],
	"../../index.php"
);
$PAGE_NAVIGATION[] = array (
	$Lang['libms']['import_book_cover'],
	""
);
echo $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

//For delete the temp folder (if there are many unused temp file in this folder, please uncomment this and load this page)
/*include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
$lfs = new libfilesystem();
$lfs->deleteDirectory($intranet_root . "/file/lms_book_cover_import");*/
?>

<form name="form1" method="post" action="confirm.php" enctype="multipart/form-data" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" align="left" width="30%"><?=$Lang['libms']['import_book_cover_file']?>: 
						<span class="tabletextremark">
						<?=$Lang['libms']['import_book_cover_file_types']?></span>
					</td>
					<td class="tabletext" width="70%">
						<input class="file" type="file" name="file" id = "userfile">
					</td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left" width="30%"><?=$Lang['libms']['import_book_cover_file_name']?>: 
						<span class="tabletextremark">
						</span>
					</td>
					<td class="tabletext" width="70%">
						<input type="radio" name="filenameFormat" id="filenameFormat1" value="1" checked="checked" /> <label for="filenameFormat1"><?=$Lang["libms"]["book"]["code"]?></label>  &nbsp;
						<input type="radio" name="filenameFormat" id="filenameFormat2" value="2" /> <label for="filenameFormat2"><?=$Lang["libms"]["book"]["ISBN"]?></label>
					</td>
				</tr>
				<!--
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?></td>
					<td class="tabletext"><?=$csvFile?></td>
				</tr>-->
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['Remark']?></td>
					<td class="tabletext"><span class="status_alert"><strong><?=$Lang['libms']['import_book_cover_file_remark'] ?></strong></span></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='../../index.php'") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="action" value="simple" />
</form>
<br />
<script>
function CheckForm()
{
	var filename = $("#userfile").val()
	var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
	if(fileext!=".jpg" && fileext!=".png" && fileext!=".jpeg" && fileext!=".zip")
	{	
		alert("<?=$Lang['libms']['invalid_book_cover_file_types']?>");
		return false;
	}
	return true;	
}
</script>
<?php


$linterface->LAYOUT_STOP();
//dump($li->built_sql());
//dump(mysql_error());
//dump(mysql_info());
intranet_closedb();
?>
