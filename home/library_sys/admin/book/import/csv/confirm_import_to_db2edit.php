<?php

# using by 

/**
 * 
 * Date:	2019-06-13 [Henry]
 * 			- add item series number field
 * Date:	2016-07-27 [Cameron]
 * 			fix bug: if barcode is 'ACNO' should use value of 'ACNO' but not this string [case #P99528]
 * 
 * Date:	2015-10-29 [Cameron]
 * 			fix bug to support import book that contains special characters in fields
 * 			( 	apply array_walk to both BOOK and BOOK_UNIQUE array,
 * 				add argument $withQuote to UPDATE_BOOK() and UPDATE_BOOK_UNIQUE() )
 * 
 * Date:	2015-06-18 [Henry]
 * 			call $libel->dbConnectionLatin1() after UPDATE_BOOK_COPY_INFO
 * Date:	2015-04-10 [Henry]
 * 			modified get_libms_element_code()
 * 
 * Date:	2015-01-21 [Henry]
 * 			add case 'Language' at get_libms_element_code() and check language code
 * 
 * Date:	2014-03-16 [Yuen]
 * 			Added
 */
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
 if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
 {
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
$laccessright = new libaccessright();
$laccessright->NO_ACCESS_RIGHT_REDIRECT();
exit;
}
*/
$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_ist'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
$CurrentPage = "PageBookManagementBookList";

$linterface = new interface_html("libms.html");

//$linterface = new interface_html();
############################################################################################################
///*

//$is_debug = true;		# for debug only - it would not insert any record but just echo sql query if set 

global $eclass_prefix;

$toolbar = '';//$linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'label_format_new.php')",$button_new,"","","",0);

//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_import']);


# step information
$STEPS_OBJ[] = array($Lang["libms"]["general"]["selectfile"], 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='../../'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import_csv_data2.php'");



//===================================================================================

############################################################################################################
function get_next_acno_number($acno = ''){
	global $libms;
	$sql = "select Next_Number from LIBMS_ACNO_LOG where LIBMS_ACNO_LOG.Key =  " . CCToSQL($acno);
	$result = $libms->returnArray($sql);
	if (empty($result)){
		$result[0]['Next_Number'] =0;
		return $result[0]['Next_Number'] ;
	}else{
		return $result[0] ;
	}
}



function set_next_acno_number($prefix='', $number = ''){
	global $libms;
	$sql = "INSERT INTO `LIBMS_ACNO_LOG` (`Key`, `Next_Number`) VALUES ('{$prefix}','{$number}') ON DUPLICATE KEY UPDATE `Next_Number`= '{$number}'";
	
 	$libms->db_db_query($sql);
	return 1;
	
}

function get_next_book_unique(){
	global $libms;
	$result = $libms->returnArray("select max(UniqueID)+1 as UniqueID  from LIBMS_BOOK_UNIQUE");
	return $result[0]['UniqueID'] ;
}

function get_item_book_ids($given_ACNO){
	global $libms;
	$result = $libms->returnArray("select UniqueID, BookID from LIBMS_BOOK_UNIQUE where ACNO=".CCToSQL($given_ACNO)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return $result[0];
	}
}

 
function is_new_tag($Description = ''){
	global $libms;
	$result = $libms->returnArray("select TagID from LIBMS_TAG where TagName = ".CCtosql($Description));
	return $result[0]['TagID'] ;
}


function get_LIBMS_TAG($Description = ''){
	global $libms;
	$result = $libms->returnArray("select * from LIBMS_TAG");
	return $result ;
}


function get_LIBMS_BOOK_CATEGORY($Description = ''){
	global $libms;
	$sql = "SELECT
	LIBMS_BOOK_CATEGORY.BookCategoryCode
	FROM
	LIBMS_BOOK_CATEGORY
	where DescriptionEn  like ".CCToSQL($Description. "%")." or DescriptionChi like ".CCToSQL($Description. "%") ;

	if ($Description != ''){
		$result = $libms->returnArray($sql);
		return $result[0]['BookCategoryCode'] ;
	}
	return "";
}


function set_LIBMS_BOOK_CATEGORY($BookCategoryCode, $DescriptionEn, $DescriptionChi){
	global $libms;
	$db_LIBMS_BOOK_CATEGORY = array(
			'BookCategoryCode' => CCToSQL($BookCategoryCode),
			'DescriptionEn' => CCToSQL($DescriptionEn),
			'DescriptionChi' =>  CCToSQL($DescriptionChi)
	);

	$result = $libms->INSERT2TABLE('LIBMS_BOOK_CATEGORY', $db_LIBMS_BOOK_CATEGORY);
	return mysql_insert_id() ;
}



function get_new_Category(){
	global $libms;
	$result = $libms->returnArray("select distinct(f690) from LIBMS_CSV_TMP_2 where f690 != '' and not isnull(f690)");

	$all_category = array();

	foreach($result as $cat){
		$cat_list = explode(',', $cat['f690']);
		$all_category = array_merge($cat_list,$all_category  );
	}
	$all_category = array_unique($all_category);
	return $all_category ;
}

function get_new_Tag(){


}

function get_max_bookCategoryCode(){
	global $libms;
	$result  = $libms->returnArray('SELECT max(LIBMS_BOOK_CATEGORY.BookCategoryCode) +0.001 as code
			from LIBMS_BOOK_CATEGORY
			where LIBMS_BOOK_CATEGORY.BookCategoryCode like "999.%"
			');

	if ($result[0]['code'] == '')
		return "999.001";

	return $result[0]['code'];
}

function set_LIBMS_TAG($TagID,$TagName){
	global $libms;
	$db_LIBMS_TAG = array(
			'TagID' => CCToSQL($TagID),
			'TagName' => CCToSQL($TagName)
	);
	$result = $libms->INSERT2TABLE('LIBMS_TAG', $db_LIBMS_TAG);
	return mysql_insert_id() ;
}


function walk_quote(&$item1)
{
	if (empty($item1)){
		$item1 =  "''";
	}else{
		$item1 = CCTOSQL($item1);
	}
}

function get_left_over_from_tmp(){
	global $libms;
	$result = $libms->returnArray("select count(*) as leftover from LIBMS_CSV_TMP_2");
	return $result[0]['leftover'];
}

function check_exist_barCode_single($given_barCode){
	global $libms;
	$result = $libms->returnArray("select BarCode from LIBMS_BOOK_UNIQUE where BarCode = ".CCTOSQL($given_barCode). " limit 1 " );

	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

function get_bookid_by_isbn_and_title(){
	global $libms;
	$returnArr = array();
	$sql = "select BookTitle, ISBN, BookID from LIBMS_BOOK WHERE RecordStatus<>'DELETE' order by BookTitle";
	$result = $libms->returnArray($sql);
	if(count($result) > 0){
		for($k=0 ; $k<count($result) ; $k++){
			$title_key = trim($result[$k]['BookTitle']);
			$isbn_key = trim($result[$k]['ISBN']);
			if($isbn_key == ''){
				$isbn_key = 'none';
			}
			
			# format: Array[BookTitle][ISBN] = BookID;
			$returnArr[$title_key][$isbn_key] = $result[$k]['BookID'];
		}
	}
	return $returnArr;
}

function get_libms_element_code($ElementType, $Code='', $DescEn='', $DescChi=''){
	global $libms, $is_debug;

	switch($ElementType){
		case 'Circulation':
			$table_name = 'LIBMS_CIRCULATION_TYPE';
			$field_name = 'CirculationTypeCode';
			break;
		case 'Resources':
			$table_name = 'LIBMS_RESOURCES_TYPE';
			$field_name = 'ResourcesTypeCode';
			break;
		case 'BookCategory':
			$table_name = 'LIBMS_BOOK_CATEGORY';
			$field_name = 'BookCategoryCode';
			// Henry Added [20150410]
			global $book;
			$BookCategoryType = 1;
			if(trim($book['Language'])){
				$sql = "Select BookCategoryType From LIBMS_BOOK_LANGUAGE WHERE BookLanguageCode = ".CCToSQL($book['Language']);
				$result = $libms->returnArray($sql);
				if (sizeof($result)<1){
					$BookCategoryType = 1;
				}
				else{
					$BookCategoryType = $result[0]['BookCategoryType'];
				}
			}
			break;
		case 'Location':
			$table_name = 'LIBMS_LOCATION';
			$field_name = 'LocationCode';
			break;
		case 'Responsibility':
			$table_name = 'LIBMS_RESPONSIBILITY';
			$field_name = 'ResponsibilityCode';
			break;
		case 'Language':
			$table_name = 'LIBMS_BOOK_LANGUAGE';
			$field_name = 'BookLanguageCode';
			break;
		default: return ''; break;
	}
	$sql = "SELECT ".$field_name." FROM  ".$table_name." where ".$field_name."  = ".CCToSQL($Code) ;
	// Henry Added [20150410]
	if($ElementType == 'BookCategory'){
		$sql = "SELECT ".$field_name." FROM  ".$table_name." where ".$field_name."  = ".CCToSQL($Code)." AND BookCategoryType = ".$BookCategoryType;
	}
	
	if ($Code != ''){
		$result1 = $libms->returnArray($sql);
		if (sizeof($result1)<1)
		{
			$sql = "SELECT ".$field_name." FROM  ".$table_name." where ".$field_name."  = ".CCToSQL("0".$Code) ;
			// Henry Added [20150410]
			if($ElementType == 'BookCategory'){
				$sql = "SELECT ".$field_name." FROM  ".$table_name." where ".$field_name."  = ".CCToSQL("0".$Code)." AND BookCategoryType = ".$BookCategoryType;
			}
			
			$result1 = $libms->returnArray($sql);			
		}
		
		
		if (sizeof($result1)<1)
		{
			$DescEn = ($DescEn == '') ? $Code : $DescEn;
			$DescChi = ($DescChi == '') ? $Code : $DescChi;
			$sql = "INSERT INTO ".$table_name." (".$field_name.", DescriptionEn, DescriptionChi, DateModified, LastModifiedBy) " .
					" VALUES ('".addslashes($Code)."', '".addslashes($DescEn)."', '".addslashes($DescChi)."', now(), 0) ";
			// Henry Added [20150410]
			if($ElementType == 'BookCategory'){
				$sql = "INSERT INTO ".$table_name." (".$field_name.", DescriptionEn, DescriptionChi, BookCategoryType, DateModified, LastModifiedBy) " .
					" VALUES ('".addslashes($Code)."', '".addslashes($DescEn)."', '".addslashes($DescChi)."', '".$BookCategoryType."', now(), 0) ";
			}
			if($is_debug){
				echo $sql.'<br>';
			} else {
				$libms->db_db_query($sql);
			}
			return $Code;
		} else
		{
			return $result1[0][$field_name];
		}
	}
	return "";
}


############################################################################################################


$libel = new elibrary();
if (isset($junior_mck))
{
	$libel->dbConnectionLatin1();	
}

$bookIDMapping = get_bookid_by_isbn_and_title();

$UpdatedLog = array();

$counter = 0;
$counterItem = 0;
while (get_left_over_from_tmp() > 0){
	$error_happen = false;
	$result = $libms->returnArray("select * from LIBMS_CSV_TMP_2 order by id limit 1000 ");
	
	foreach ($result as $book){
		//dex($book);
		//lookup  $next_acno_number;
		# init / reset 
		$import_record = array();
		$import_item = array();
		$result_insert_BOOK_UNIQUE = '';
		$result_insert_LIBMS_BOOK = '';
		
		

		$error_happen=false;
		
		# check and prepare ACNO
				
		if (strtoupper($book['ACNO'])=='AUTO'){
			$error_happen=true;
		}else{
			list($current_book_item_id, $current_BookID) = get_item_book_ids($book['ACNO']);
			if ($current_book_item_id>0 && $current_BookID>0)
			{
				# pass
			} else
			{
				$error_happen=true;
			}
		}
		
		$import_record['BookID'] = $current_BookID;
		
		
		# check circulation type code 
		$CirculationTypeCode = get_libms_element_code('Circulation', $book['BookCirclation']);
		
		$ItemCirculationTypeCode = get_libms_element_code('Circulation', $book['ItemCirculationTypeCode']);
		
		# check resources type code
		$ResourcesTypeCode = get_libms_element_code('Resources', $book['BookResources']); 
		
		# check location code 
		$BookCategoryCode = get_libms_element_code('BookCategory', $book['BookCategory']);
		
		# check location code 
		$LocationCode = get_libms_element_code('Location', $book['BookLocation']);
		
		# check responsibility code
		$ResponsibilityCode1 = get_libms_element_code('Responsibility', $book['ResponsibilityCode1']);
		$ResponsibilityCode2 = get_libms_element_code('Responsibility', $book['ResponsibilityCode2']);
		$ResponsibilityCode3 = get_libms_element_code('Responsibility', $book['ResponsibilityCode3']);
		
		# check language code
		$LanguageCode = get_libms_element_code('Language', $book['Language']);
		
		$import_item['ACNO'] = $book['ACNO'];
		
		
		# prepare book info
		$import_record['CallNum'] 				= $book['CallNum'];
		$import_record['CallNum2'] 				= $book['CallNum2'];
		$import_record['ISBN'] 					= $book['ISBN'];
		$import_record['ISBN2'] 				= $book['ISBN2'];
		$import_record['BookTitle']				= $book['BookTitle'];
		$import_record['BookSubTitle'] 			= $book['BookSubTitle'];
		$import_record['Introduction'] 			= $book['Introduction'];
		$import_record['Language'] 				= $LanguageCode;//$book['Language'];
		$import_record['Country'] 				= $book['Country'];
		$import_record['Edition'] 				= $book['Edition'];
		$import_record['PublishPlace'] 			= $book['PublishPlace'];
		$import_record['Publisher'] 			= $book['Publisher'];
		$import_record['PublishYear'] 			= $book['PublishYear'];
		$import_record['NoOfPage'] 				= $book['NoOfPage'];
		$import_record['RemarkInternal'] 		= $book['BookInternalremark'];
		$import_record['RemarkToUser'] 			= $book['RemarkToUser'];
		$import_record['CirculationTypeCode'] 	= $CirculationTypeCode;
		$import_record['ResourcesTypeCode'] 	= $ResourcesTypeCode;
		$import_record['BookCategoryCode'] 		= $BookCategoryCode;
		$import_record['Subject'] 				= $book['Subject'];
		$import_record['Series'] 				= $book['Series'];
		$import_record['SeriesNum'] 			= $book['SeriesNum'];
		$import_record['URL'] 					= $book['URL'];
		$import_record['ResponsibilityCode1'] 	= $ResponsibilityCode1;
		$import_record['ResponsibilityBy1'] 	= $book['ResponsibilityBy1'];
		$import_record['ResponsibilityCode2'] 	= $ResponsibilityCode2;
		$import_record['ResponsibilityBy2'] 	= $book['ResponsibilityBy2'];
		$import_record['ResponsibilityCode3'] 	= $ResponsibilityCode3;
		$import_record['ResponsibilityBy3'] 	= $book['ResponsibilityBy3'];
		# 0 -- not allow borrow, 1 -- allow borrow (default)
		$import_record['OpenBorrow'] 			= ($book['OpenBorrow'] == '0')?($book['OpenBorrow']):1;
		# 0 -- not allow reserve, 1 -- allow reserve (default)
		$import_record['OpenReservation'] 		= ($book['OpenReservation'] == '0')?($book['OpenReservation']):1;
		
		$import_record['Dimension'] 		= $book['Dimension'];
		$import_record['ILL'] 		= $book['ILL'];		

		# prepare item info 
		$import_item['BarCode'] 			 = $book['barcode'];
		//$import_item['RecordStatus'] 		 = 'NORMAL';
		$import_item['RecordStatus'] 		 = $book['RecordStatus'];
		$import_item['Distributor'] 		 = $book['Distributor'];
		$import_item['Discount'] 			 = $book['Discount'];
		$import_item['PurchaseDate']		 = getDefaultDateFormat($book['PurchaseDate']);
		$import_item['PurchasePrice']		 = $book['PurchasePrice'];
		$import_item['PurchaseNote'] 		 = $book['PurchaseNote'];
		$import_item['PurchaseByDepartment'] = $book['PurchaseByDepartment'];
		$import_item['ListPrice'] 			 = $book['ListPrice'];
		$import_item['LocationCode'] 		 = $LocationCode;
		$import_item['InvoiceNumber'] 		 = $book['InvoiceNumber'];
		$AccountDate = explode(' ',$book['AccountDate']);
		$import_item['CreationDate'] 		 = getDefaultDateFormat($AccountDate[0]);
		
		$import_item['AccompanyMaterial'] 			= $book['AccompanyMaterial'];
		$import_item['RemarkToUser'] 			= $book['ItemRemarkToUser'];
		$import_item['CirculationTypeCode'] 	= $ItemCirculationTypeCode;	
		$import_item['ItemSeriesNum'] 			= $book['ItemSeriesNum'];

		$db_LIBMS_BOOK = array(
			'CallNum' => $import_record['CallNum'] ,
			'CallNum2' => $import_record['CallNum2'],
			'ISBN' => $import_record['ISBN'],
			'ISBN2' => $import_record['ISBN2'],
			'BookTitle' => $import_record['BookTitle'],
			'BookSubTitle' => $import_record['BookSubTitle'],
			'Introduction' => $import_record['Introduction'],
			'Language' =>  $import_record['Language'],
			'Country' =>  $import_record['Country'],
			'Edition' => $import_record['Edition'],
			'PublishPlace' =>  $import_record['PublishPlace'],
			'Publisher' =>  $import_record['Publisher'],
			'PublishYear' =>  $import_record['PublishYear'],
			'NoOfPage' =>  $import_record['NoOfPage'],
			'RemarkInternal' => $import_record['RemarkInternal'],
			'RemarkToUser' => $import_record['RemarkToUser'],
			'CirculationTypeCode' => $import_record['CirculationTypeCode'],
			'ResourcesTypeCode' => $import_record['ResourcesTypeCode'],
			'BookCategoryCode' =>  $import_record['BookCategoryCode'],
			'Subject' => $import_record['Subject'],
			'Series' => $import_record['Series'],
			'SeriesNum' => $import_record['SeriesNum'],
			'URL' => $import_record['URL'],
			'ResponsibilityCode1' => $import_record['ResponsibilityCode1'],
			'ResponsibilityBy1' => $import_record['ResponsibilityBy1'],
			'ResponsibilityCode2' => $import_record['ResponsibilityCode2'],
			'ResponsibilityBy2' =>  $import_record['ResponsibilityBy2'],
			'ResponsibilityCode3' => $import_record['ResponsibilityCode3'],
			'ResponsibilityBy3' => $import_record['ResponsibilityBy3'],
//			'DateModified' =>  'NOW()',
//			'LastModifiedBy' =>  CCToSQL($UserID),
			'OpenBorrow' =>  $import_record['OpenBorrow'],
			'OpenReservation' => $import_record['OpenReservation'],
			'Dimension' => $import_record['Dimension'],
		    'ILL' => $import_record['ILL']
		);

		array_walk($db_LIBMS_BOOK, walk_quote );
		
		if ($error_happen==false)
		{
			if($current_BookID!='' && $current_BookID>0)
			{ 
				$result_insert_LIBMS_BOOK = $libms->UPDATE_BOOK('LIBMS_BOOK', $current_BookID, $db_LIBMS_BOOK, $withQuote=false);
				if (!$UpdatedLog[$current_BookID])
				{
					$counter ++;
					$UpdatedLog[$current_BookID] = true;
				}
			} else {
				# error?!
			}

			# insert item record 
			if ($current_book_item_id!="" && $current_book_item_id>0)
			{ 
				
				$import_item['ACNO_Prefix'] = "";
				$import_item['ACNO_Num'] = "";
				preg_match('/^([a-zA-Z]+)(\d+)$/',strtoupper($book['ACNO']),$matches);
				if (!empty($matches))
				{
					$prefix = $matches[1];
					$number = $matches[2];
					
					$import_item['ACNO_Prefix'] = $prefix;
					$import_item['ACNO_Num'] = $number;
				
				} else{
				
				}
				
				
				$db_LIBMS_BOOK_UNIQUE = array(
						'ACNO' => $import_item['ACNO'],
						'ACNO_Prefix' => $import_item['ACNO_Prefix'],
						'ACNO_Num' => $import_item['ACNO_Num'],
						'CreationDate' => (($import_item['CreationDate'] != '') ? $import_item['CreationDate'] : 'NOW()'),
						'PurchaseDate' => $import_item['PurchaseDate'],
						'PurchaseNote' => $import_item['PurchaseNote'],
						'PurchaseByDepartment' => $import_item['PurchaseByDepartment'],
						'ListPrice' => $import_item['ListPrice'],
						'Discount' => $import_item['Discount'],
						'PurchasePrice' => $import_item['PurchasePrice'], 
						'LocationCode' =>  $import_item['LocationCode'],
						'Distributor' => $import_item['Distributor'],
						'InvoiceNumber' =>  $import_item['InvoiceNumber'],
//						'DateModified' =>  'NOW()',
//						'LastModifiedBy' =>  $UserID,
						'RecordStatus' =>  $import_item['RecordStatus'],
						'AccompanyMaterial' => $import_item['AccompanyMaterial'],
						'RemarkToUser' => $import_item['RemarkToUser'],
						'CirculationTypeCode' => $import_item['CirculationTypeCode'],
						'ItemSeriesNum' => $import_item['ItemSeriesNum']
				);
				if (strtoupper($import_item['BarCode'])<>"AUTO")
				{
					if (strtoupper($book['barcode']) == 'ACNO'){
						$db_LIBMS_BOOK_UNIQUE['BarCode'] = $import_item['ACNO'];
					}
					else {
						$db_LIBMS_BOOK_UNIQUE['BarCode'] = $import_item['BarCode'];
					}
				}

				array_walk($db_LIBMS_BOOK_UNIQUE, walk_quote );
				
				$result_insert_BOOK_UNIQUE  = $libms->UPDATE_BOOK_UNIQUE('LIBMS_BOOK_UNIQUE', $current_book_item_id, $db_LIBMS_BOOK_UNIQUE, $withQuote=false);
				$counterItem++;
			}
			
			# update no. of Copy & Available Copy
			if($current_BookID != ''){
				$libms->UPDATE_BOOK_COPY_INFO($current_BookID);
				if (isset($junior_mck))
				{
					$libel->dbConnectionLatin1();	
				}
			}

			# insert Tag if any.
			$sql = "DELETE FROM LIBMS_BOOK_TAG WHERE BookID='{$current_BookID}' ";
			$libms->db_db_query($sql);
			if ($book['Tags']) 
			{
				$book_tags_str = str_replace(";", ",", $book['Tags']);
				$tag_list = explode(',', $book_tags_str);
				
				$tag_list = array_unique($tag_list);
					
				foreach($tag_list as $aTag){
					$existing_tag = is_new_tag($aTag);
					
					if ($existing_tag){
						$db_LIBMS_BOOK_TAG = array(
								'BookTagID' => cctosql('0'),
								'BookID' =>  cctosql($current_BookID),
								'TagID' => cctosql($existing_tag)
						);
					}else{
						$new_tag_id = set_LIBMS_TAG('0', $aTag);
						$db_LIBMS_BOOK_TAG = array(
								'BookTagID' => cctosql('0'),
								'BookID' =>  cctosql($current_BookID),
								'TagID' => cctosql($new_tag_id)
						);
					}
					$libms->INSERT2TABLE('LIBMS_BOOK_TAG', $db_LIBMS_BOOK_TAG);
				}
			}
			
			# book insert log result
			if ($current_BookID!="" && $current_BookID>0)
			{ 
				$libel->SYNC_PHYSICAL_BOOK_TO_ELIB($current_BookID);
				if (isset($junior_mck))
				{
					$libel->dbConnectionLatin1();	
				}
			}

		}

		$db_LIBMS_BOOK = array();
		$db_LIBMS_BOOK_UNIQUE = array();
		$error_happen = false;

	} //for each 1000
	$libms->db_db_query("delete from `LIBMS_CSV_TMP_2` order by id limit 1000 ");

} //LOOP until all gone.


$lang_space = " ";

$x = "<p><u>".$Lang['libms']['import_book']['Result']['Success']."</u></p>";
$x .= $lang_space.$Lang['libms']['import_book']['total_record_modified']." : ";
$x .= $counter;
$x .= $lang_space.$Lang['libms']['import_book']['book_records'];
$x .= $lang_space.$Lang['libms']['import_book']['and'];
$x .= $lang_space;
$x .= $counterItem;
$x .= $lang_space.$Lang['libms']['import_book']['item_records'];

$i = 1;



if($is_debug) die();


$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<br />
<form name="form1" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5">
		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center"><br /> <br /> <br /> <?= $x ?></td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5"
					align="center">
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td align="center" colspan="2"><?=$import_button?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form><?php

$linterface->LAYOUT_STOP();
//dump($li->built_sql());
//dump(mysql_error());
//dump(mysql_info());
intranet_closedb();


?>