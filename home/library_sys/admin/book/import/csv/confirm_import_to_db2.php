<?php
# using: Henry 
/**
 * Log :	
 * Note:	use LIBMS_CSV_TMP_2 since 2013-08-30 ; LIBMS_CSV_TMP >> no longer use after 2013-08-30
 * 
 * Date:	2020-09-25 (Henry)
 *      	- add flag $sys_custom['eLibraryPlus']['AutoGenBarcodeWithNumber'] to control the auto generate barcode with number only
 * Date:	2019-06-13 [Henry]
 * 			- add item series number field
 * Date:	2018-01-30 [Henry]
 * 			bug fix: cannot update next acno number [Case#Z135037]
 * Date:	2017-11-03 [Cameron]
 * 			fix bug: copy get_bookid_by_isbn_and_title_new() from ajax_marc_insert_db.php so that use 
 * 			BookTitle + ISBN + BookSubTitle + CallNum + CallNum2 to identify a book
 * Date:	2017-02-20 [Henry]
 * 			bug fix: "0" will disappear after specific letters of ACNO when add new books [Case#M113000]
 * Date:	2017-02-06 [Cameron]
 * 			fix bug: should include RecordStatus=null in get_bookid_by_isbn_and_title()
 * Date:	2015-04-10 [Henry]
 * 			modified get_libms_element_code()
 * Date:	2015-01-21 [Henry]
 * 			add case 'Language' at get_libms_element_code() and check language code
 * Date:	2013-05-02 [Cameron]
 * 			Fix bug for copying "Introduction" and "RemarkInternal"
 * Date:	2013-04-24 [Cameron]
 * 			Fix bug for OpenBorrow and OpenReservation when confirm copying record from LIBMS_CSV_TMP to LIMB_BOOK
 * 			0 -- not allow, 1 -- allow
 */
 
@SET_TIME_LIMIT(216000);
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
 if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
 {
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
$laccessright = new libaccessright();
$laccessright->NO_ACCESS_RIGHT_REDIRECT();
exit;
}
*/
$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_ist'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
$CurrentPage = "PageBookManagementBookList";

$linterface = new interface_html("libms.html");

//$linterface = new interface_html();
############################################################################################################
///*

//$is_debug = true;		# for debug only - it would not insert any record but just echo sql query if set 

global $eclass_prefix;

$toolbar = '';//$linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'label_format_new.php')",$button_new,"","","",0);

//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_import']);


# step information
$STEPS_OBJ[] = array($Lang["libms"]["general"]["selectfile"], 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='../../'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import_csv_data2.php'");



//===================================================================================

############################################################################################################
function get_next_acno_number($acno = ''){
	global $libms;
	$sql = "select Next_Number from LIBMS_ACNO_LOG where LIBMS_ACNO_LOG.Key =  " . CCToSQL($acno);
	$result = $libms->returnArray($sql);
	if (empty($result)){
		$result[0]['Next_Number'] =0;
		return $result[0]['Next_Number'] ;
	}else{
		return $result[0] ;
	}
}



function set_next_acno_number($prefix='', $number = ''){
	global $libms;
	$sql = "INSERT INTO `LIBMS_ACNO_LOG` (`Key`, `Next_Number`) VALUES ('{$prefix}','{$number}') ON DUPLICATE KEY UPDATE `Next_Number`= '{$number}'";
	
 	$libms->db_db_query($sql);
	return 1;
	
}

function get_next_book_unique(){
	global $libms;
	$result = $libms->returnArray("select max(UniqueID)+1 as UniqueID  from LIBMS_BOOK_UNIQUE");
	return $result[0]['UniqueID'] ;
}

function check_exist_ACNO($given_ACNO){
	global $libms;
	$result = $libms->returnArray("select ACNO from LIBMS_BOOK_UNIQUE where ACNO=".CCToSQL($given_ACNO)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

 
function is_new_tag($Description = ''){
	global $libms;
	$result = $libms->returnArray("select TagID from LIBMS_TAG where TagName = ".CCtosql($Description));
	return $result[0]['TagID'] ;
}


function get_LIBMS_TAG($Description = ''){
	global $libms;
	$result = $libms->returnArray("select * from LIBMS_TAG");
	return $result ;
}


function get_LIBMS_BOOK_CATEGORY($Description = ''){
	global $libms;
	$sql = "SELECT
	LIBMS_BOOK_CATEGORY.BookCategoryCode
	FROM
	LIBMS_BOOK_CATEGORY
	where DescriptionEn  like ".CCToSQL($Description. "%")." or DescriptionChi like ".CCToSQL($Description. "%") ;

	if ($Description != ''){
		$result = $libms->returnArray($sql);
		return $result[0]['BookCategoryCode'] ;
	}
	return "";
}


function set_LIBMS_BOOK_CATEGORY($BookCategoryCode, $DescriptionEn, $DescriptionChi){
	global $libms;
	$db_LIBMS_BOOK_CATEGORY = array(
			'BookCategoryCode' => CCToSQL($BookCategoryCode),
			'DescriptionEn' => CCToSQL($DescriptionEn),
			'DescriptionChi' =>  CCToSQL($DescriptionChi)
	);

	$result = $libms->INSERT2TABLE('LIBMS_BOOK_CATEGORY', $db_LIBMS_BOOK_CATEGORY);
	return mysql_insert_id() ;
}



function get_new_Category(){
	global $libms;
	$result = $libms->returnArray("select distinct(f690) from LIBMS_CSV_TMP_2 where f690 != '' and not isnull(f690)");

	$all_category = array();

	foreach($result as $cat){
		$cat_list = explode(',', $cat['f690']);
		$all_category = array_merge($cat_list,$all_category  );
	}
	$all_category = array_unique($all_category);
	return $all_category ;
}

function get_new_Tag(){


}

function get_max_bookCategoryCode(){
	global $libms;
	$result  = $libms->returnArray('SELECT max(LIBMS_BOOK_CATEGORY.BookCategoryCode) +0.001 as code
			from LIBMS_BOOK_CATEGORY
			where LIBMS_BOOK_CATEGORY.BookCategoryCode like "999.%"
			');

	if ($result[0]['code'] == '')
		return "999.001";

	return $result[0]['code'];
}

function set_LIBMS_TAG($TagID,$TagName){
	global $libms;
	$db_LIBMS_TAG = array(
			'TagID' => CCToSQL($TagID),
			'TagName' => CCToSQL($TagName)
	);
	$result = $libms->INSERT2TABLE('LIBMS_TAG', $db_LIBMS_TAG);
	return mysql_insert_id() ;
}


function walk_quote(&$item1)
{
	if (empty($item1)){
		$item1 =  "''";
	}else{
		$item1 = CCTOSQL($item1);
	}
}

function get_left_over_from_tmp(){
	global $libms;
	$result = $libms->returnArray("select count(*) as leftover from LIBMS_CSV_TMP_2");
	return $result[0]['leftover'];
}

function check_exist_barCode_single($given_barCode){
	global $libms;
	$result = $libms->returnArray("select BarCode from LIBMS_BOOK_UNIQUE where BarCode = ".CCTOSQL($given_barCode)." limit 1 " );



	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

//function get_bookid_by_isbn_and_title(){
//	global $libms;
//	$returnArr = array();
//	$sql = "select BookTitle, ISBN, BookID from LIBMS_BOOK WHERE RecordStatus<>'DELETE' OR RecordStatus IS NULL order by BookTitle";
//	$result = $libms->returnArray($sql);
//	if(count($result) > 0){
//		for($k=0 ; $k<count($result) ; $k++){
//			$title_key = trim($result[$k]['BookTitle']);
//			$isbn_key = trim($result[$k]['ISBN']);
//			if($isbn_key == ''){
//				$isbn_key = 'none';
//			}
//			
//			# format: Array[BookTitle][ISBN] = BookID;
//			$returnArr[$title_key][$isbn_key] = $result[$k]['BookID'];
//		}
//	}
//	return $returnArr;
//}

function get_bookid_by_isbn_and_title_new($number){
	global $libms;
	$number_start = $number*20000;
	$number_end = 20000+$number_start;

	$returnArr = array();
	
	$sql = "select BookTitle, ISBN, BookID, BookSubtitle, CallNum, CallNum2 from LIBMS_BOOK WHERE RecordStatus<>'DELETE' or RecordStatus is null order by BookTitle limit ".$number_start.",".$number_end."";

	$result = $libms->returnResultSet($sql);
	if(count($result) > 0){
		for($k=0 ; $k<count($result) ; $k++){
			$title_key = trim($result[$k]['BookTitle']);
			$isbn_key = trim($result[$k]['ISBN']);
			if($isbn_key == ''){
				$isbn_key = 'none';
			}
			$subtitle_key = trim($result[$k]['BookSubtitle']);
			if($subtitle_key == ''){
				$subtitle_key = 'none';
			}
			$callnum_key = trim($result[$k]['CallNum']);
			if($callnum_key == ''){
				$callnum_key = 'none';
			}			
			$callnum2_key = trim($result[$k]['CallNum2']);
			if($callnum2_key == ''){
				$callnum2_key = 'none';
			}				
			# format: Array[BookTitle][ISBN][$subtitle_key][$callnum_key][$callnum2_key] = BookID;
			$returnArr[$title_key][$isbn_key][$subtitle_key][$callnum_key][$callnum2_key] = $result[$k]['BookID'];
		}
	}
	return $returnArr;
}

function get_libms_element_code($ElementType, $Code='', $DescEn='', $DescChi=''){
	global $libms, $is_debug;

	switch($ElementType){
		case 'Circulation':
			$table_name = 'LIBMS_CIRCULATION_TYPE';
			$field_name = 'CirculationTypeCode';
			break;
		case 'Resources':
			$table_name = 'LIBMS_RESOURCES_TYPE';
			$field_name = 'ResourcesTypeCode';
			break;
		case 'BookCategory':
			$table_name = 'LIBMS_BOOK_CATEGORY';
			$field_name = 'BookCategoryCode';
			// Henry Added [20150410]
			global $book;
			$BookCategoryType = 1;
			if(trim($book['Language'])){
				$sql = "Select BookCategoryType From LIBMS_BOOK_LANGUAGE WHERE BookLanguageCode = ".CCToSQL($book['Language']);
				$result = $libms->returnArray($sql);
				if (sizeof($result)<1){
					$BookCategoryType = 1;
				}
				else{
					$BookCategoryType = $result[0]['BookCategoryType'];
				}
			}
			break;
		case 'Location':
			$table_name = 'LIBMS_LOCATION';
			$field_name = 'LocationCode';
			break;
		case 'Responsibility':
			$table_name = 'LIBMS_RESPONSIBILITY';
			$field_name = 'ResponsibilityCode';
			break;
		case 'Language':
			$table_name = 'LIBMS_BOOK_LANGUAGE';
			$field_name = 'BookLanguageCode';
			break;
		default: return ''; break;
	}
	$sql = "SELECT ".$field_name." FROM  ".$table_name." where ".$field_name."  = ".CCToSQL($Code) ;
	// Henry Added [20150410]
	if($ElementType == 'BookCategory'){
		$sql = "SELECT ".$field_name." FROM  ".$table_name." where ".$field_name."  = ".CCToSQL($Code)." AND BookCategoryType = ".$BookCategoryType;
	}
	
	if ($Code != ''){
		$result1 = $libms->returnArray($sql);
		if (sizeof($result1)<1)
		{
			$sql = "SELECT ".$field_name." FROM  ".$table_name." where ".$field_name."  = ".CCToSQL("0".$Code) ;
			// Henry Added [20150410]
			if($ElementType == 'BookCategory'){
				$sql = "SELECT ".$field_name." FROM  ".$table_name." where ".$field_name."  = ".CCToSQL("0".$Code)." AND BookCategoryType = ".$BookCategoryType;
			}
			
			$result1 = $libms->returnArray($sql);			
		}
		
		
		if (sizeof($result1)<1)
		{
			$DescEn = ($DescEn == '') ? $Code : $DescEn;
			$DescChi = ($DescChi == '') ? $Code : $DescChi;
			$sql = "INSERT INTO ".$table_name." (".$field_name.", DescriptionEn, DescriptionChi, DateModified, LastModifiedBy) " .
					" VALUES ('".addslashes($Code)."', '".addslashes($DescEn)."', '".addslashes($DescChi)."', now(), 0) ";
			// Henry Added [20150410]
			if($ElementType == 'BookCategory'){
				$sql = "INSERT INTO ".$table_name." (".$field_name.", DescriptionEn, DescriptionChi, BookCategoryType, DateModified, LastModifiedBy) " .
					" VALUES ('".addslashes($Code)."', '".addslashes($DescEn)."', '".addslashes($DescChi)."', '".$BookCategoryType."', now(), 0) ";
			}
					
			if($is_debug){
				echo $sql.'<br>';
			} else {
				$libms->db_db_query($sql);
			}
			return $Code;
		} else
		{
			return $result1[0][$field_name];
		}
	}
	return "";
}


############################################################################################################


$libel = new elibrary();
if (isset($junior_mck))
{
	$libel->dbConnectionLatin1();	
}

//$bookIDMapping = get_bookid_by_isbn_and_title();
$sql = "Select Count(*) From LIBMS_BOOK";
$result = $libms->returnArray($sql);
$part =floor($result[0][0]/20000);
for($i=0;$i<=$part;$i++){
	
	${'n'.$i} = get_bookid_by_isbn_and_title_new($i);
}


$counter = 0;
$counterItem = 0;
while (get_left_over_from_tmp() > 0){
	$error_happen = false;
	$result = $libms->returnArray("select * from LIBMS_CSV_TMP_2 order by id limit 1000 ");
	
	foreach ($result as $book){
		//dex($book);
		//lookup  $next_acno_number;
		# init / reset 
		$import_record = array();
		$import_item = array();
		$result_insert_BOOK_UNIQUE = '';
		$result_insert_LIBMS_BOOK = '';
		
		$import_record['BookID'] = '0';

		# check wether the BookID exists or not by ISBN and BookTitle
		$exist_BookID = '';
		$title_key = trim($book['BookTitle']);
		$isbn_key = ($book['ISBN'] != '') ? trim($book['ISBN']) : 'none';
		$subtitle_key = ($book['BookSubTitle'] != '') ? trim($book['BookSubTitle']) : 'none';
		$callnum_key = ($book['CallNum'] != '') ? trim($book['CallNum']) : 'none';
		$callnum2_key = ($book['CallNum2'] != '') ? trim($book['CallNum2']) : 'none';

        for($i=0;$i<=($part+1);$i++){
            if(isset(${'n'.$i}[$title_key][$isbn_key][$subtitle_key][$callnum_key][$callnum2_key]) && ${'n'.$i}[$title_key][$isbn_key][$subtitle_key][$callnum_key][$callnum2_key] != ''){
			    $exist_BookID = ${'n'.$i}[$title_key][$isbn_key][$subtitle_key][$callnum_key][$callnum2_key];
	     	}
        }

		# check and prepare ACNO
		//if it is auto or EMPTY, do auto generate
		if ((strtoupper($book['ACNO'])=='AUTO')or(trim($book['ACNO'])=='')){
			$next_acno_number = get_next_acno_number('IMPORT');
			$ACNO = 'IMPORT' . $next_acno_number['Next_Number'];
			$import_item['ACNO'] = $ACNO;
			$import_item['ACNO_Prefix'] = 'IMPORT';
			$import_item['ACNO_Num'] = $next_acno_number['Next_Number']++;
			set_next_acno_number('IMPORT', $next_acno_number['Next_Number'] );
		}else{
			//check the given BookCode.
			//$bookCode = 'IMPORT' . $next_acno_number['Next_Number'];
			if (check_exist_ACNO($book['ACNO'])){
				//existing one pop error
				//error happen
				$error_happen=true;
			}else{
				$import_item['ACNO'] = $book['ACNO'];
				$import_item['ACNO_Prefix'] = "";
				$import_item['ACNO_Num'] = "";
				preg_match('/^([a-zA-Z]+)(\d+)$/',strtoupper($book['ACNO']),$matches);
				if (!empty($matches)){
					$prefix = $matches[1];
					$number = $matches[2];
					
					$import_item['ACNO'] = $book['ACNO'];
					$import_item['ACNO_Prefix'] = $prefix;
					$import_item['ACNO_Num'] = $number;
					
					$orig_len = strlen($number);
					$next_number = (int)$number + 1;
					if(strlen($next_number) < $orig_len){
						$next_number = str_pad($next_number, $orig_len, "0", STR_PAD_LEFT);
					}
					
					//check duplication.
					set_next_acno_number($prefix, $next_number);
				}else if(preg_match('/^\d+$/',$book['ACNO'])){
					$orig_len = strlen($book['ACNO']);
					$next_number = (int)$book['ACNO'] + 1;
					if(strlen($next_number) < $orig_len){
						$next_number = str_pad($next_number, $orig_len, "0", STR_PAD_LEFT);
					}
					set_next_acno_number($libms->acno_default_sys_prefix, $next_number);
				}
			}

		}
		
//		# check wether the BookID exists or not by ISBN and BookTitle
//		$exist_BookID = '';
//		$title_key = trim($book['BookTitle']);
//		$isbn_key = ($book['ISBN'] != '') ? trim($book['ISBN']) : 'none';
//		if(isset($bookIDMapping[$title_key][$isbn_key]) && $bookIDMapping[$title_key][$isbn_key] != ''){
//			$exist_BookID = $bookIDMapping[$title_key][$isbn_key];
//		}
		
		# check circulation type code 
		$CirculationTypeCode = get_libms_element_code('Circulation', $book['BookCirclation']);
		
		$ItemCirculationTypeCode = get_libms_element_code('Circulation', $book['ItemCirculationTypeCode']);
		
		# check resources type code
		$ResourcesTypeCode = get_libms_element_code('Resources', $book['BookResources']); 
		
		# check location code 
		$BookCategoryCode = get_libms_element_code('BookCategory', $book['BookCategory']);
		
		# check location code 
		$LocationCode = get_libms_element_code('Location', $book['BookLocation']);
		
		# check responsibility code
		$ResponsibilityCode1 = get_libms_element_code('Responsibility', $book['ResponsibilityCode1']);
		$ResponsibilityCode2 = get_libms_element_code('Responsibility', $book['ResponsibilityCode2']);
		$ResponsibilityCode3 = get_libms_element_code('Responsibility', $book['ResponsibilityCode3']);
		
		# check language code
		$LanguageCode = get_libms_element_code('Language', $book['Language']);
		
		# prepare book info
		$import_record['CallNum'] 				= $book['CallNum'];
		$import_record['CallNum2'] 				= $book['CallNum2'];
		$import_record['ISBN'] 					= $book['ISBN'];
		$import_record['ISBN2'] 				= $book['ISBN2'];
		$import_record['BookTitle']				= $book['BookTitle'];
		$import_record['BookSubTitle'] 			= $book['BookSubTitle'];
		$import_record['Introduction'] 			= $book['Introduction'];
		$import_record['Language'] 				= $LanguageCode;//$book['Language'];
		$import_record['Country'] 				= $book['Country'];
		$import_record['Edition'] 				= $book['Edition'];
		$import_record['PublishPlace'] 			= $book['PublishPlace'];
		$import_record['Publisher'] 			= $book['Publisher'];
		$import_record['PublishYear'] 			= $book['PublishYear'];
		$import_record['NoOfPage'] 				= $book['NoOfPage'];
		$import_record['RemarkInternal'] 		= $book['BookInternalremark'];
		$import_record['RemarkToUser'] 			= $book['RemarkToUser'];
		$import_record['CirculationTypeCode'] 	= $CirculationTypeCode;
		$import_record['ResourcesTypeCode'] 	= $ResourcesTypeCode;
		$import_record['BookCategoryCode'] 		= $BookCategoryCode;
		$import_record['Subject'] 				= $book['Subject'];
		$import_record['Series'] 				= $book['Series'];
		$import_record['SeriesNum'] 			= $book['SeriesNum'];
		$import_record['URL'] 					= $book['URL'];
		$import_record['ResponsibilityCode1'] 	= $ResponsibilityCode1;
		$import_record['ResponsibilityBy1'] 	= $book['ResponsibilityBy1'];
		$import_record['ResponsibilityCode2'] 	= $ResponsibilityCode2;
		$import_record['ResponsibilityBy2'] 	= $book['ResponsibilityBy2'];
		$import_record['ResponsibilityCode3'] 	= $ResponsibilityCode3;
		$import_record['ResponsibilityBy3'] 	= $book['ResponsibilityBy3'];
		# 0 -- not allow borrow, 1 -- allow borrow (default)
		$import_record['OpenBorrow'] 			= ($book['OpenBorrow'] == '0')?($book['OpenBorrow']):1;
		# 0 -- not allow reserve, 1 -- allow reserve (default)
		$import_record['OpenReservation'] 		= ($book['OpenReservation'] == '0')?($book['OpenReservation']):1;
		
		$import_record['Dimension'] 		= $book['Dimension'];
		$import_record['ILL'] 		= $book['ILL'];

		# prepare item info 
		$import_item['BarCode'] 			 = $book['barcode'];
		//$import_item['RecordStatus'] 		 = 'NORMAL';
		$import_item['RecordStatus'] 		 = $book['RecordStatus'];
		$import_item['Distributor'] 		 = $book['Distributor'];
		$import_item['Discount'] 			 = $book['Discount'];
		$import_item['PurchaseDate']		 = getDefaultDateFormat($book['PurchaseDate']);
		$import_item['PurchasePrice']		 = $book['PurchasePrice'];
		$import_item['PurchaseNote'] 		 = $book['PurchaseNote'];
		$import_item['PurchaseByDepartment'] = $book['PurchaseByDepartment'];
		$import_item['ListPrice'] 			 = $book['ListPrice'];
		$import_item['LocationCode'] 		 = $LocationCode;
		$import_item['InvoiceNumber'] 		 = $book['InvoiceNumber'];
		$AccountDate = explode(' ',$book['AccountDate']);
		$import_item['CreationDate'] 		 = getDefaultDateFormat($AccountDate[0]);

		$import_item['AccompanyMaterial'] 			= $book['AccompanyMaterial'];
		$import_item['RemarkToUser'] 			= $book['ItemRemarkToUser'];
		$import_item['CirculationTypeCode'] 	= $ItemCirculationTypeCode;	
		$import_item['ItemSeriesNum'] 			= $book['ItemSeriesNum'];

		$db_LIBMS_BOOK = array(  
			'BookID' => $import_record['BookID'],
			'CallNum' => $import_record['CallNum'] ,
			'CallNum2' => $import_record['CallNum2'],
			'ISBN' => $import_record['ISBN'],
			'ISBN2' => $import_record['ISBN2'],
			'BookTitle' => $import_record['BookTitle'],
			'BookSubTitle' => $import_record['BookSubTitle'],
			'Introduction' => $import_record['Introduction'],
			'Language' =>  $import_record['Language'],
			'Country' =>  $import_record['Country'],
			'Edition' => $import_record['Edition'],
			'PublishPlace' =>  $import_record['PublishPlace'],
			'Publisher' =>  $import_record['Publisher'],
			'PublishYear' =>  $import_record['PublishYear'],
			'NoOfPage' =>  $import_record['NoOfPage'],
			'RemarkInternal' => $import_record['RemarkInternal'],
			'RemarkToUser' => $import_record['RemarkToUser'],
			'CirculationTypeCode' => $import_record['CirculationTypeCode'],
			'ResourcesTypeCode' => $import_record['ResourcesTypeCode'],
			'BookCategoryCode' =>  $import_record['BookCategoryCode'],
			'Subject' => $import_record['Subject'],
			'Series' => $import_record['Series'],
			'SeriesNum' => $import_record['SeriesNum'],
			'URL' => $import_record['URL'],
			'ResponsibilityCode1' => $import_record['ResponsibilityCode1'],
			'ResponsibilityBy1' => $import_record['ResponsibilityBy1'],
			'ResponsibilityCode2' => $import_record['ResponsibilityCode2'],
			'ResponsibilityBy2' =>  $import_record['ResponsibilityBy2'],
			'ResponsibilityCode3' => $import_record['ResponsibilityCode3'],
			'ResponsibilityBy3' => $import_record['ResponsibilityBy3'],
			'OpenBorrow' =>  $import_record['OpenBorrow'],
			'OpenReservation' => $import_record['OpenReservation'],
			'Dimension' => $import_record['Dimension'],
		    'ILL' => $import_record['ILL']
		);

		array_walk($db_LIBMS_BOOK, walk_quote );
		
		if ($error_happen==false){
			if($exist_BookID == ''){
				# insert book record 
//				echo $import_record['ISBN'].' |=| ';
				$result_insert_LIBMS_BOOK = $libms->INSERT2TABLE('LIBMS_BOOK', $db_LIBMS_BOOK);
				
				$lastID_LIBMS_BOOK = '';
				if($result_insert_LIBMS_BOOK){
					$lastID_LIBMS_BOOK = mysql_insert_id();
					
					# append the new insert book id to the BookID-Mapping Array for the next checking
//					$bookIDMapping[$title_key][$isbn_key] = $lastID_LIBMS_BOOK;
					${'n'.($part+1)}[$title_key][$isbn_key][$subtitle_key][$callnum_key][$callnum2_key] = $lastID_LIBMS_BOOK;
				}
	//			if ($result_insert_LIBMS_BOOK === false){
	//				echo (mysql_error());
	//			}
			} else {
				# book exists - no need to insert 
				$lastID_LIBMS_BOOK = $exist_BookID;
			}

			# insert item record 
			if ($is_debug || $lastID_LIBMS_BOOK){
				if (strtoupper($book['barcode']) == 'AUTO' || empty($book['barcode'])){
					# if auto or empty, do auto generate
					if($sys_custom['eLibraryPlus']['AutoGenBarcodeWithNumber']){
						$new_barcode = (get_next_book_unique() + 1000000);
					}
					else{
						$new_barcode = 'A+'.(get_next_book_unique() + 1000000);
					}
				} else if (strtoupper($book['barcode']) == 'ACNO'){
					# if set to use ACNO
					$new_barcode = $import_item['ACNO'];
				} else {
					if (!check_exist_barCode_single($book['barcode'])){
						//not exist barcode, can insert
						$new_barcode = $book['barcode'];
					}else{
						$error_happen = true;
						//do nothing
					}
				}
				if ($error_happen==false){
					$db_LIBMS_BOOK_UNIQUE = array(
							'UniqueID' => CCToSQL('0'),
							'ACNO' => CCToSQL($import_item['ACNO']),
							'ACNO_Prefix' => CCToSQL($import_item['ACNO_Prefix']),
							'ACNO_Num' => CCToSQL($import_item['ACNO_Num']),
							'BarCode' => CCToSQL($new_barcode),
							'BookID' => CCToSQL($lastID_LIBMS_BOOK),
							'CreationDate' => (($import_item['CreationDate'] != '') ? CCToSQL($import_item['CreationDate']) : 'NOW()'),
							'PurchaseDate' => CCToSQL($import_item['PurchaseDate']),
							'PurchaseNote' => CCToSQL($import_item['PurchaseNote']),
							'PurchaseByDepartment' => CCToSQL($import_item['PurchaseByDepartment']),
							'ListPrice' => CCToSQL($import_item['ListPrice']),
							'Discount' => CCToSQL($import_item['Discount']),
							'PurchasePrice' => CCToSQL($import_item['PurchasePrice']), 
							'LocationCode' =>  CCToSQL($import_item['LocationCode']),
							'Distributor' => CCToSQL($import_item['Distributor']),
							'InvoiceNumber' =>  CCToSQL($import_item['InvoiceNumber']),
							'RecordStatus' =>  CCToSQL($import_item['RecordStatus']),
							'AccompanyMaterial' => CCToSQL($import_item['AccompanyMaterial']),
							'RemarkToUser' => CCToSQL($import_item['RemarkToUser']),
							'CirculationTypeCode' => CCToSQL($import_item['CirculationTypeCode']),
							'ItemSeriesNum' => CCToSQL($import_item['ItemSeriesNum'])
					);
//					echo $import_item['ACNO'].' |=| ';
					$result_insert_BOOK_UNIQUE  = $libms->INSERT2TABLE('LIBMS_BOOK_UNIQUE', $db_LIBMS_BOOK_UNIQUE);
				} else {
					$result_insert_BOOK_UNIQUE = false;
				}

				# insert Tag if any.
				if ($book['Tags']) {
					$book_tags_str = str_replace(";", ",", $book['Tags']);
					$tag_list = explode(',', $book_tags_str);
					
					$tag_list = array_unique($tag_list);
						
					foreach($tag_list as $aTag){
						$existing_tag = is_new_tag($aTag);
						
						if ($existing_tag){
							$db_LIBMS_BOOK_TAG = array(
									'BookTagID' => cctosql('0'),
									'BookID' =>  cctosql($lastID_LIBMS_BOOK),
									'TagID' => cctosql($existing_tag)
							);
						}else{
							$new_tag_id = set_LIBMS_TAG('0', $aTag);
							$db_LIBMS_BOOK_TAG = array(
									'BookTagID' => cctosql('0'),
									'BookID' =>  cctosql($lastID_LIBMS_BOOK),
									'TagID' => cctosql($new_tag_id)
							);
						}
						$libms->INSERT2TABLE('LIBMS_BOOK_TAG', $db_LIBMS_BOOK_TAG);
					}
				}
				
				# update no. of Copy & Available Copy
				if($lastID_LIBMS_BOOK != ''){
					$libms->UPDATE_BOOK_COPY_INFO($lastID_LIBMS_BOOK);
				}
			}
			
			# item insert log result
			if ($result_insert_BOOK_UNIQUE == false){
				
			} else {
				$counterItem++;
			}
			
			# book insert log result
			if ($result_insert_LIBMS_BOOK == false){
				//false to import, check database.
				//print_r($db_LIBMS_BOOK);
				//print "<br>|<br>";
				//print_r($db_LIBMS_BOOK_UNIQUE);
				//print "<br><hr><br>";
			} else {
				$counter ++;
				$libel->SYNC_PHYSICAL_BOOK_TO_ELIB($lastID_LIBMS_BOOK);
				if (isset($junior_mck))
				{
					$libel->dbConnectionLatin1();	
				}
			}

		}

		$db_LIBMS_BOOK = array();
		$db_LIBMS_BOOK_UNIQUE = array();
		$error_happen = false;

	} //for each 1000

	//echo "Parse: " . $counter . <br/>;
	//dex($db_LIBMS_BOOK);
	//$db->query("delete * from LIBMS_CSV_TMP");
	//$result = db2array($rs);
	$libms->db_db_query("delete from `LIBMS_CSV_TMP_2` order by id limit 1000 ");

} //LOOP until all gone.


$lang_space = " ";

$x = "<p><u>".$Lang['libms']['import_book']['Result']['Success']."</u></p>";
$x .= $lang_space.$Lang['libms']['import_book']['total_record']." : ";
$x .= $counter;
$x .= $lang_space.$Lang['libms']['import_book']['book_records'];
$x .= $lang_space.$Lang['libms']['import_book']['and'];
$x .= $lang_space;
$x .= $counterItem;
$x .= $lang_space.$Lang['libms']['import_book']['item_records'];



$i = 1;



if($is_debug) die();


$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<br />
<form name="form1" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5">
		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center"><br /> <br /> <br /> <?= $x ?></td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5"
					align="center">
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td align="center" colspan="2"><?=$import_button?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form><?php

$linterface->LAYOUT_STOP();
//dump($li->built_sql());
//dump(mysql_error());
//dump(mysql_info());
intranet_closedb();


?>