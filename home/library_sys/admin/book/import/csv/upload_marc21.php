<?php
@SET_TIME_LIMIT(216000);

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."lang/libms.lang.en.php");	# for getting the item status values
if (is_array($Lang["libms"]["book_status"]) && sizeof($Lang["libms"]["book_status"])>0)
{
	foreach ($Lang["libms"]["book_status"] AS $db_value => $column_data)
	{
		$ItemStatusMapping[strtoupper($column_data)] = $db_value;
	}
}

include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once("handle_marc21_new.php");
//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_ist'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
$CurrentPage = "PageBookManagementBookList";

$linterface = new interface_html("libms.html");

//$linterface = new interface_html();
############################################################################################################
///*


global $eclass_prefix;

$toolbar = '';//$linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'label_format_new.php')",$button_new,"","","",0);

//*/
############################################################################################################
# Top menu highlight setting

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_import']);
$ACNO_tag=array();
$BarCode_tag=array();

# step information
$STEPS_OBJ[] = array($Lang["libms"]["general"]["selectfile"], 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


## ADD PEAR in the INCLUDE path 
set_include_path(get_include_path() . PATH_SEPARATOR . $intranet_root .'/home/library_sys/admin/book/import/pear/');




/*
include('../api/class.upload.php');
if ((isset($_POST['actionSimple']) ? $_POST['actionSimple'] : (isset($_GET['actionSimple']) ? $_GET['actionSimple'] : '')) == 'simple') {

	$handle = new Upload($_FILES['file_marc21']);
 	debug_pr($handle);	
	$ext = strtoupper($handle->file_src_name_ext);
// 	debug_pr($ext);
	if($ext != "TXT")
	{
		$uploadSuccess = false;
		$xmsg = $Lang['libms']['import_book']['upload_fail']  . $handle->error . $Lang['libms']['import_book']['contact_admin'];
	}
	elseif ($handle->uploaded) {
		
// 		debug_pr("aa");
// 		debug_pr($handle->processed);
		
		//$handle->Process($intranet_root."/file/lms_book_import/");
// 		debug_pr($handle->processed);
		if ($handle->processed) {
// 			debug_pr("processed");
				
			// everything was fine !
			$marc21file = $intranet_root."/file/lms_book_import/" . $handle->file_dst_name;
			debug_pr($marc21file);
			$marc21file_name = $handle->file_dst_name;
			$uploadSuccess = true;
		} else {
			// one error occured
			$xmsg = $Lang['libms']['import_book']['upload_fail']  . $handle->error . $Lang['libms']['import_book']['contact_admin'];

			$uploadSuccess = false;
		}

		// we delete the temporary files
		$handle-> Clean();
	}


}
*/

if ((isset($_POST['actionSimple']) ? $_POST['actionSimple'] : (isset($_GET['actionSimple']) ? $_GET['actionSimple'] : '')) == 'simple') {
	
	$ext =strtolower(end(explode(".",$_FILES['file_marc21']['name'])));	
	if($ext != "txt")
	{
		$uploadSuccess = false;
		$xmsg = $Lang['libms']['import_book']['upload_fail']  . $handle->error . $Lang['libms']['import_book']['contact_admin'];
	}else{
		$timestamp = time();
		$dst_name = $timestamp.".".$ext;
		$dir = $intranet_root."/file/lms_book_import/";
		if(!is_dir($dir)) mkdir($dir,0777);
		$marc21file = $dir.$dst_name;
		$marc21file_name = $dst_name;
		move_uploaded_file($_FILES['file_marc21']['tmp_name'],$marc21file);
		$uploadSuccess = true;
	}

}

$libms->db_db_query("delete from `LIBMS_MARC_TMP_2`");

if ($uploadSuccess){	
	$readfile=file($marc21file);
    $BookInfoArr = Handle_marc21($readfile,0,0,3);
}

//Show three books information, and shows one item if has.
//$x.="".$Lang["libms"]["import_book"]["ImportMarc21Alert"]."";
for($n=0;$n<$BookInfoArr['n'];$n++){
	if($n==0)$x .="<p>".$Lang["libms"]["import_book"]["FirstBook"]."</p>";	
	if($n==1)$x .="<p>".$Lang["libms"]["import_book"]["SecondBook"]."</p>";	
	if($n==2)$x .="<p>".$Lang["libms"]["import_book"]["ThirdBook"]."</p>";	
	$x .= "<table class='common_table_list_v30 view_table_list_v30' style='width:700px;'>";
	$x .= "<tr><th COLSPAN='2'>". $Lang["libms"]["report"]["bookinfo"]."</th></tr>";
	$x .= "<tr><td style='width:30%'>".$Lang["libms"]["book"]["title"]."</td><td style='width:70%;'>".$BookInfoArr["BookTitle"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["subtitle"]."</td><td>".$BookInfoArr["BookSubtitle"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["ISBN"]."</td><td>".$BookInfoArr["ISBN"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["ISBN2"]."</td><td>".$BookInfoArr["ISBN2"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["call_number_desc"]."</td><td>".$BookInfoArr["CallNum"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["call_number2_desc"]."</td><td>".$BookInfoArr["CallNum2"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["edition"]."</td><td>".$BookInfoArr["Edition"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["publish_place"]."</td><td>".$BookInfoArr["PublishPlace"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["publisher"]."</td><td>".$BookInfoArr["Publisher"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["publish_year"]."</td><td>".$BookInfoArr["PublishYear"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["number_of_page"]."</td><td>".$BookInfoArr["NoOfPage"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["DIMEN"]."</td><td>".$BookInfoArr["Dimension"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["ILL"]."</td><td>".$BookInfoArr["ILL"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["country"]."</td><td>".$BookInfoArr["Country"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["language"]."</td><td>".$BookInfoArr["Language"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["series"]."</td><td>".$BookInfoArr["Series"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["series_number"]."</td><td>".$BookInfoArr["SeriesNum"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["introduction"]."</td><td>".$BookInfoArr["Introduction"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["subject"]."</td><td>".$BookInfoArr["Subject"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["tags"]."</td><td>".$BookInfoArr["Tags"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["responsibility_by1"]."</td><td>".$BookInfoArr["ResponsibilityBy1"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["responsibility_code1"]."</td><td>".$BookInfoArr["Responsibility1"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["responsibility_by2"]."</td><td>".$BookInfoArr["ResponsibilityBy2"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["responsibility_code2"]."</td><td>".$BookInfoArr["Responsibility2"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["responsibility_by3"]."</td><td>".$BookInfoArr["ResponsibilityBy3"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang["libms"]["book"]["responsibility_code3"]."</td><td>".$BookInfoArr["Responsibility3"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang['libms']['book']['select']['ResourcesTypeCode']."</td><td>".$BookInfoArr["ResourcesTypeCode"][$n]."</td></tr>";
	$x .= "<tr><td>".$Lang['libms']['book']['select']['CirculationTypeCode']."</td><td>".$BookInfoArr["CirculationTypeCode"][$n][0]."</td></tr>";
	
	   for($i=0;$i<($BookInfoArr['unique'][$n]<1?$BookInfoArr['unique'][$n]:1);$i++){
		   $x .= "<tr><th COLSPAN='2'>". $Lang["libms"]["book"]["iteminfo"]."</th></tr>";
		   $x .= "<tr><td>".$Lang["libms"]["book"]["code"]."</td><td>".$BookInfoArr["ACNO"][$n][$i]."</td></tr>";
		   $x .= "<tr><td>".$Lang["libms"]["book"]["barcode"]."</td><td>".$BookInfoArr["BarCode"][$n][$i]."</td></tr>";   
		   $x .= "<tr><td>".$Lang["libms"]["book"]["purchase_date"]."</td><td>".$BookInfoArr["PurchaseDate"][$n][$i]."</td></tr>";
		   $x .= "<tr><td>".$Lang["libms"]["label"]["LocationCode"]."</td><td>".$BookInfoArr["LocationCode"][$n][$i]."</td></tr>";
		   $x .= "<tr><td>".$Lang['libms']['book']['invoice_number']."</td><td>".$BookInfoArr["InvoiceNumber"][$n][$i]."</td></tr>";
		   $x .= "<tr><td>".$Lang["libms"]["book"]["discount"]."</td><td>".$BookInfoArr["Discount"][$n][$i]."</td></tr>";
		   $x .= "<tr><td>".$Lang["libms"]["book"]["distributor"]."</td><td>".$BookInfoArr["Distributor"][$n][$i]."</td></tr>";
		   $x .= "<tr><td>".$Lang["libms"]["book"]["series_number"]."</td><td>".$BookInfoArr["ItemSeriesNum"][$n][$i]."</td></tr>";    
		   $x .= "<tr><td>".$Lang["libms"]["book"]["purchase_price"]."</td><td>".$BookInfoArr["PurchasePrice"][$n][$i]."</td></tr>";
		   $x .= "<tr><td>".$Lang['libms']['book']['select']['CirculationTypeCode']."</td><td>".$BookInfoArr["CirculationTypeCode"][$n][$i]."</td></tr>";
		   $RecordStatus=$BookInfoArr['RecordStatus'][$n][$i];
		   $x .= "<tr><td>".$Lang['libms']['book']['select']['Book_Status']."</td><td>".$Lang["libms"]["book_status"]["$RecordStatus"]."</td></tr>";
		   $x .= "<tr><td>".$Lang["libms"]["book"]["purchase_by_department"]."</td><td>".$BookInfoArr["PurchaseByDepartment"][$n][$i]."</td></tr>";
		   
		   $x .= "<tr><td>".$Lang["libms"]["book"]["list_price"]."</td><td>".$BookInfoArr["ListPrice"][$n][$i]."</td></tr>";
		   $x .= "<tr><td>".$Lang["libms"]["book"]["purchase_note"]."</td><td>".$BookInfoArr["PurchaseNote"][$n][$i]."</td></tr>";
		   $x .= "<tr><td>".$Lang["libms"]["book"]["account_date"]."</td><td>".$BookInfoArr["CreationDate"][$n][$i]."</td></tr>"; 
		   $x .= "<tr><td>".$Lang["libms"]["book"]["AccompanyMaterial"]."</td><td>".$BookInfoArr["AccompanyMaterial"][$n][$i]."</td></tr>";
	    
	   }
	$x .= "</table><br>";
}


if($BookInfoArr['n']==0)
{
	header("location: import_csv_data2.php?xmsg=import_no_record");
	exit();
}

$import_button = $linterface->GET_ACTION_BTN($button_import, "button","javascript:confirmToSend();")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_csv_data2.php'");

$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script> 
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" /> 
<script>

function confirmToSend(){
		
//	if(confirm("aaa"))
//    {
		$('#send_by_ajax').click();
//   }
    
    
}

</script>
<form name="form1" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5">

		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center">
				<table width="96%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" class="tabletext">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
								    <td align="center" style="color:red"><p><?=$Lang["libms"]["import_book"]["ImportMarc21Alert"]?></p></td>
								</tr>				
								<tr>								     
									<td align="center"><?=$x?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="95%" border="0" cellpadding="0" cellspacing="0"
					align="center">
					<tr>
						<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2"><?= $import_button ?>
			</td>
		</tr>
	</table>
	
    <a style="display:none" id="send_by_ajax" name="send_by_ajax" href="ajax_send.php?height=200&width=500&modal=true&file=<?=$marc21file_name?>" class="thickbox">Thickbox</a>

</form>
<br />
<script>

</script>
<?php

$linterface->LAYOUT_STOP();

intranet_closedb();


?>