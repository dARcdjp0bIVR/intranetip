<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

# modifying by : yat

##############################
#	
#	Date:	2010-11-29	YatWoon
#			if($task=='catName'), retrieve Request Summary as well
#
##############################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

$CurrentPageArr['eAdminRepairSystem'] = 1;

if($task=='catName')
{	
	//$output = '<table width="90%" border="0" cellpadding="0" cellspacing="0">';
	$sql = "select 
  			  b.Name,
			    a.Title
			from 
			    REPAIR_SYSTEM_REQUEST_SUMMARY as a
			    left join REPAIR_SYSTEM_CATEGORY as b on (b.CategoryID = a.CategoryID)
			where 
			    a.RecordStatus = 1
			order by
			    b.Name, a.Title
   			 ";
// 	$sql = "SELECT CategoryID, Name FROM REPAIR_SYSTEM_CATEGORY WHERE RecordStatus!=-1 ORDER BY CategoryID";
    $array = $lrepairsystem->returnArray($sql);
	
	$data = "<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td width=\"10%\">#</td>
					<td>".$Lang['RepairSystem']['CategoryName']."</td>
					<td>".$Lang['RepairSystem']['RequestSummary']."</td>
				</tr>";
		
		for($a=0;$a<sizeof($array);$a++)
		{
			$catID = $array[$a]['CategoryID'];
			$catName = ($array[$a]['Name']);
			$RSummary = ($array[$a]['Title']);
			if($catName!='')
			{
				$data .="<tr class=\"tablerow1\">
							<td>".($a+1)."</td>
							<td>$catName</td>
							<td>$RSummary</td>
						</tr>";
			}
		}
	$data .="</table>";	
}

else if($task=='locName')
{	
	//$output = '<table width="90%" border="0" cellpadding="0" cellspacing="0">';

	$sql = "SELECT LocationID, LocationName FROM REPAIR_SYSTEM_LOCATION WHERE RecordStatus!=-1 ORDER BY LocationID";
	$array = $lrepairsystem->returnArray($sql);

	$data = "<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td width=\"10%\">#</td>
					<td>".$Lang['RepairSystem']['Location']."</td>
				</tr>";
		
		for($a=0;$a<sizeof($array);$a++)
		{
			$locID = $array[$a]['LocationID'];
			$locName = ($array[$a]['LocationName']);
			
			if($locName!='')
			{
				$data .="<tr class=\"tablerow1\">
							<td>".($a+1)."</td>
							<td>".$locName."</td>
						</tr>";
			}
		}
	$data .="</table>";	
}

else if($task=='buildingName')
{	
	$buildingAry = $lrepairsystem->getInventoryBuildingArray();

	$data = "<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td width=\"15%\">#</td>
					<td>".$Lang['RepairSystem']['Location']."</td>
				</tr>";
		
		for($a=0;$a<sizeof($buildingAry);$a++)
		{
			list($locID, $locName) = $buildingAry[$a];
			
			if($locName!='')
			{
				$data .="<tr class=\"tablerow1\">
							<td>".($a+1)."</td>
							<td><a class=\"tablelink\" href=\"javascript:;\" onClick=\"document.getElementById('loadBuildingID').value=$locID;show_ref_list('levelName','lvl_click')\">".$locName."</a></td>
						</tr>";
			}
		}
	$data .="</table>";	
}

else if($task=='levelName')
{	
	$levelAry = $lrepairsystem->getInventoryLevelArray($loadBuildingID);

	$data = "<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td width=\"15%\">#</td>
					<td>".$i_InventorySystem_Location_Level."</td>
				</tr>";
		
		for($a=0;$a<sizeof($levelAry);$a++)
		{
			list($buildingID, $lvlID, $lvlName) = $levelAry[$a];
			
			if($lvlName!='')
			{
				$data .="<tr class=\"tablerow1\">
							<td>".($a+1)."</td>
							<td><a class=\"tablelink\" href=\"javascript:;\" onClick=\"document.getElementById('loadLevelID').value=$lvlID;show_ref_list('locName2','loc_click')\">".$lvlName."</a></td>
						</tr>";
			}
		}
	$data .="</table>";	
}

else if($task=='locName2')
{	
	$locationAry = $lrepairsystem->getInventoryLocationArray($loadLevelID);

	$data = "<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td width=\"15%\">#</td>
					<td>".$i_InventorySystem_Location."</td>
				</tr>";
		
		for($a=0;$a<sizeof($locationAry);$a++)
		{
			list($lvlID, $locID, $locName) = $locationAry[$a];
			
			if($locName!='')
			{
				$data .="<tr class=\"tablerow1\">
							<td>".($a+1)."</td>
							<td>".$locName."</td>
						</tr>";
			}
		}
	$data .="</table>";	
}

else if($task=='statusName')
{	
	//$output = '<table width="90%" border="0" cellpadding="0" cellspacing="0">';

	$data = "<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td width=\"30%\">".$Lang['RepairSystem']['ImportDataCol'][8]."</td>
					<td>".$i_general_status."</td>
				</tr>
				<tr class=\"tablerow1\">
							<td>P</td>
							<td>".$Lang['RepairSystem']['Processing']."</td>
						</tr>
				<tr class=\"tablerow1\">
							<td>C</td>
							<td>".$Lang['RepairSystem']['Completed']."</td>
						</tr>
				<tr class=\"tablerow1\">
							<td>R</td>
							<td>".$i_status_rejected."</td>
						</tr>";
	$data .="</table>";	
}


$output = '<table width="70%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
					<td width="19" height="19"><a href="javascript:Hide_Window(\'ref_list\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  
					  <tr>
						<td height="150" align="left" valign="top">
							<div id="list" style="overflow: auto; z-index:1; height: 150px;">
								'.$data.'
							</div>
						</td></tr>
							  <tr>
						    <td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
						      <tr>
							       <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
						      </tr>
						     </table></td>
						     </tr>
                 </table></td>
					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
				  </tr>
				  <tr>
					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
				  </tr>
					</table></td>
			  </tr>
			</table>';
echo $output;
?>
