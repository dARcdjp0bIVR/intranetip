<?php
$PATH_WRT_ROOT = "../../../../../../";
	
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
	
intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libms = new liblms();

?>
<script language="javascript">

function jsCancel()
{
	
	if (confirm("<?=$Lang["libms"]["import_book"]["CancelImport"]?>"))
	{
		parent.location.reload();	
	}		
}

function jsClose()
{	  	  

    document.formAJ.action="confirm_import_to_db_marc21_new.php";
	document.formAJ.method="post";
	document.formAJ.submit();
}

function jsMarcInsert(count,counter,counterItem) {
  $.ajax({
    url: 'ajax_marc_insert_db.php?count='+count+'&counter='+counter+'&counterItem='+counterItem,
    success: function(response) { 
      var total=<?=$total?>;
      rData = response.split("|");
      count = rData[0];
      counter =  rData[1];
      counterItem = rData[2];
      if (total>0)
      {
	      ProgressPercentage = Math.round(100*(count/total));
	      $('#progressbarA').progressBar(ProgressPercentage);
      }
      $('#ProgressMessage').html("<strong><?=$Lang["libms"]["import_book"]["AlreadyImportedBooks"]?>"+counter+" ; <?=$Lang["libms"]["import_book"]["AlreadyImportedItems"]?>"+counterItem+"</strong>");
	  if (count/total!=1)
	  {
	  	jsMarcInsert(count,counter,counterItem);
	  }
	  else
	  {
	  	//jump to confirm_import_to_db_marc21_new.php
	  	
	  	  var div = document.getElementById("div1");	
      	  var input1 = document.createElement("input");
     	  input1.type = "hidden";
     	  input1.name = "counter";
	      input1.value = counter;
	      div.appendChild(input1);	
	      var input2 = document.createElement("input");
	      input2.type = "hidden";
	      input2.name = "counterItem";
	      input2.value = counterItem;
	      div.appendChild(input2);	
	      
	  	  $('#ActionButtonCancel').hide();
	  	  $('#ActionButtonClose').show();
	  	}
    }
  });
}


$(container).ready(function() {
  jsMarcInsert(0,0,0);
});

</script>

<form name="formAJ">
<script language="JavaScript" src="/templates/jquery/jquery.progressbar.min.js"></script> 
<script>
	$(document).ready(function() {
		$("#progressbarA").progressBar({barImage: '/images/progress_bar_green.gif'} );
	});
</script>
<style>
	#container { width: 80%; margin-left: 10%; margin-top: 30px;}
</style>
<div id="container" align="center">
	<div style="padding-bottom: 25px;">
		<div id="ProgressMessage"><?=$Lang["libms"]["import_book"]["StartToImporting"]?></div>
		<table align="center">
			<tr><td width="240" align="center"><span class="progressBar" id="progressbarA">0%</span></td></tr>
		</table>
		<p id="ActionButtonCancel"><?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:jsCancel()")?></p>	
		<p id="ActionButtonClose" style="display:none"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "javascript:jsClose()")?></p>
	</div>
</div>
<div id="div1">
</div>
</form>
<?
	intranet_closedb();
?>