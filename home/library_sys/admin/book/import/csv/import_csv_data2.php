<?php

# using by 
/*
 * 	2019-06-14 Henry
 * 		- update import sample files
 * 	2017-12-01 Cameron
 * 		- update import sample files [case #F130063]
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

### set cookies
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	//$order = $ck_right_page_order;
	$order = 1;		# default in ascending order
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
*/
$libms = new liblms();


$CurrentPageArr['LIBMS'] = 1;
$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_ist'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
$CurrentPage = "PageBookManagementBookList";
 

$linterface = new interface_html("libms.html");
  
//$linterface = new interface_html();

############################################################################################################
///*
 
 
global $eclass_prefix;
 
$toolbar = '';//$linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'label_format_new.php')",$button_new,"","","",0);

//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_import']);
 

 
# step information
$STEPS_OBJ[] = array($Lang["libms"]["general"]["selectfile"], 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


$sample_file = "book_import_unicode2.csv";
$csvFileNew = "<span id='csv_sample_type1'><a class=\"tablelink\" href=\"get_sample_file.php?file=$sample_file\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a></span>";


$sample_file = "book_import_unicode2_edit.csv";
$csvFileEdit = "<span id='csv_sample_type2' style='display:none'><a class=\"tablelink\" href=\"get_sample_file.php?file=$sample_file\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a></span>";

$csv_format = "";
$delim = "<br>";
for($i=0; $i<sizeof($Lang['libms']['import_book']['ImportCSVDataCol_New2']); $i++){
	if($i!=0) $csv_format .= $delim;
	$csv_format .= $Lang['SysMgr']['Homework']['Column']." ".numberToLetter($i+1, true)." : ".$Lang['libms']['import_book']['ImportCSVDataCol_New2'][$i];
}

$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN(); 

$linterface->LAYOUT_START();



?>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script>

function viewCodes(CodeType)
{
	var CaptionTitle;
	if (CodeType==1)
	{
		CaptionTitle = "<?= $Lang["libms"]["settings"]["responsibility"] ?>";
	} else if (CodeType==2)
	{
		CaptionTitle = "<?= $Lang["libms"]["settings"]["book_category"] ?>";
	} else if (CodeType==3)
	{
		CaptionTitle = "<?= $Lang["libms"]["settings"]["circulation_type"] ?>";
	} else if (CodeType==4)
	{
		CaptionTitle = "<?= $Lang["libms"]["settings"]["resources_type"] ?>";
	} else if (CodeType==5)
	{
		CaptionTitle = "<?= $Lang["libms"]["settings"]["book_location"] ?>";
	} else if (CodeType==6)
	{
		CaptionTitle = "<?= $Lang["libms"]["book"]["book_status"] ?>";
	}
	
	tb_show(CaptionTitle, "view_codes.php?&CodeType="+CodeType+"&KeepThis=true&height=500&width=680"); 
}

function setActionType(toValue)
{
	$("#actionTypeNow").val(toValue);
	if (toValue==1)
	{
		$("#acno_type2").hide();
		$("#itemStatus_type2").hide();
		$("#csv_sample_type2").hide();
		$("#acno_type1").show();
		$("#csv_sample_type1").show();
	} else
	{
		$("#acno_type1").hide();
		$("#csv_sample_type1").hide();
		$("#acno_type2").show();
		$("#itemStatus_type2").show();
		$("#csv_sample_type2").show();
	}
}

function upload_function(form1){

	if($('#fileFormat1').is(':checked'))
	{
	   form1.action  ="upload_marc21.php";	
	} else 
	{
		if ($("#actionTypeNow").val()==1)
		{
	   		form1.action = "upload_csv2.php";
		} else if ($("#actionTypeNow").val()==2)
		{
	   		form1.action ="upload_csv2edit.php";
		}	
		
	}
	form1.submit();
}


function showDiv(layername) {
	$('#'+layername).show();
}
function hideDiv(layername) {
	$('#'+layername).hide();
}
</script>
<br />
<form name="form1" method="post"  enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td colspan="2">
		
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
<?php //if ($plugin["elib_plus_demo"]) { ?>
				<tr>
					<td class="formfieldtitle" align="left" width="30%"><?=$Lang['libms']['import_format']?>: 
						
					</td>
					<td class="tabletext" width="70%">
						<input type="radio" name="fileFormat" id="fileFormat2" value="2" checked="checked" onClick="showDiv('csv');showDiv('actiontype');hideDiv('marc21');" /> <label for="fileFormat2"><?=$Lang['libms']['import_CSV']?></label>  &nbsp;
						<input type="radio" name="fileFormat" id="fileFormat1" value="1" onClick="showDiv('marc21');hideDiv('csv');hideDiv('actiontype');"/> <label for="fileFormat1"><?=$Lang['libms']['import_marc21']?></label>
					</td>
				</tr>
<?php// } ?>
                <tbody id="actiontype">
                <tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['eInventory']['ActionType']?></td>
	                <td class="tabletext" width="70%">
	                	<?=$linterface->Get_Radio_Button("actionType1", "actionType", "1",1,"",$Lang['eInventory']['Insert'], "setActionType(1)");?> &nbsp;
	                	<?=$linterface->Get_Radio_Button("actionType2", "actionType", "2",0,"",$Lang['eInventory']['Update'], "setActionType(2)");?> <span class="tabletextremark">(<?=$Lang["libms"]["import_book"]["matched_by_acno"]?>)</span>
	    			</td>
                </tr>
                </tbody>
                
                <tbody id="csv">
				<tr>
					<td class="formfieldtitle" align="left" width="30%"><?=$iDiscipline['Import_Source_File']?>: 
						<span class="tabletextremark">
						<?=$Lang['General']['CSVFileFormat']?></span>
					</td>
					<td class="tabletext" width="70%">
						<input class="file" type="file" name="file">
					</td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?></td>
					<td class="tabletext"><?=$csvFileNew . $csvFileEdit.' ('.$Lang["libms"]["import_book"]["LastUpdateDate"].': 2019-06-14)'?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
					<td class="tabletext"><?=$csv_format?></td>
				</tr>
 
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['RequiredField']?></td>
				</tr>
				
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['libms']["import_book"]["acnoautofill"]?></td>
				</tr>
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['libms']['import_book']['DateFormat']?></td>
				</tr>
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['libms']['import_book']['RecordStatus']?></td>
				</tr>
			    </tbody>
			    <tbody id="marc21" style="display:none;">
				<tr>
					<td class="formfieldtitle" align="left" width="30%"><?=$Lang['General']['ImportMARC21']?>: 
						<span class="tabletextremark">
						<?=$Lang['General']['MARC21FileFormat']?></span>
					</td>
					<td class="tabletext" width="70%">
						<input class="file" type="file" name="file_marc21">
					</td>
				</tr>			    
			    </tbody>
			</table>
			 
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "button","javascript:upload_function(this.form);") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='../../index.php'") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="actionSimple" id="actionSimple" value="simple" />
<input type="hidden" name="actionTypeNow" id="actionTypeNow" value="1" />

</form>
<br /><?php

$linterface->LAYOUT_STOP();
//dump($li->built_sql());
//dump(mysql_error());
//dump(mysql_info());
intranet_closedb();


?>