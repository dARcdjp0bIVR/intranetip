<?php
/*
 * 	Using:	
 * 	Log 
 * 
 * 	2016-04-26 (Cameron) 
 * 		- add charset=UTF-8 in meta data before calling NO_ACCESS_RIGHT_REDIRECT
 *  
 * 	2016-04-25 [Cameron] 
 * 		- break admin access right from "stock-take and write-off" into "stock-take" and "write-off" rights [Case#K94933]
 * 
 * 	2016-01-04 [Cameron] 
 * 		- use $_SERVER['HTTP_REFERER'] instead of $HTTP_REFERER to support php 5.4+
 * 
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['stock-take'] && !$admin_right['write-off'] && !$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (!$_GET["file"]) {
	header ("Location: /");
	exit();
}

$filename = basename($_GET["file"]);

$ref = $_SERVER['HTTP_REFERER'];
$url_format = parse_url($ref);
$path = $intranet_root.str_replace(basename($url_format["path"]), "", $url_format["path"]).$filename;

$parts = explode(".", $filename);
if (is_array($parts) && count($parts) > 1)
	$extension = strtoupper(end($parts));

# Only allow plain text files on the server to be read
$allow_ext = array("CSV", "TXT", "LOG");

$handle = @fopen($path,"r");

if ($handle && in_array($extension, $allow_ext)) {
	$data = fread($handle, filesize($path));
	
	header('Pragma: public');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

	header('Content-type: application/octet-stream');
	header('Content-Length: '.strlen($data));

	header('Content-Disposition: attachment; filename="'.$filename.'";');

	print $data;
}
else {
	header ("Location: /");
	exit();
}
			
?>
