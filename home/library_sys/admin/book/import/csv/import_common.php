<?php
/*
 * 	Modifying: 
 * 
 * 	Please save it using UTF-8 encoding !!!
 * 
 * 	Log
 * 
 * Date:	2015-10-28 (Cameron)
 * 			add function trim_space() - replace Chinese Space character to empty string and trim it, in utf8
 * 
 * 	Date:	2015-10-08 [Cameron]
 * 			create this file
 */
 
/*
 * 	Check if a string is in scientific format, e.g. 1.62E+13, return true if it is and false otherwise
 */
function isScientificFormat($str) {
	preg_match('/^(\d+)*\.(\d+)*E\+(\d+)*$/',strtoupper($str),$matches);
	return empty($matches) ? false : true;
}

/*
 * replace Chinese Space character to empty string and trim it
 */
function trim_space($str){
	return trim(str_replace('　', '' ,$str));
}

?>