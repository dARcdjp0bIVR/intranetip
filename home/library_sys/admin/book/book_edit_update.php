<?php
ini_set('memory_limit', '256M');

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
//ini_set('display_errors', 0);

# using: 

/*
 * 	Note !!! 2016-01-15  Cameron
 *			As start from php 5.4, this file execute function magicQuotes_addslashes() and removePotentialAttackChar() 
 *			in includes/lib.php, post variable is not changed to get value from $_REQUEST, should consult Ivan for the reason.
 *
 */
############ Change Log Start ###############
#
#   Date:   2019-05-30 [Cameron]
#           - apply changeImageOrientation() to image if its orientation info is set [case #L159650]
#
#   Date:   2018-09-10 [Cameron]
#           - add code to prevent UserID being changed in magicQuotes_stripslashes2()
#
#	Date:	2017-05-19 [Cameron]
#			fix bug on replacing new line / line feed character with space to eliminate js error in Taglist (apply newline2space) (case #Q117253)
#
#	Date:	2016-03-02  Cameron
#			- apply recursive_trim to $_POST [case #F93085] 		
#
#	Date:	2016-01-15  Cameron
#			- strip slashes and assign registered variables after call lib.php for php5.4+ 
#
#	Date:	2016-01-04  Cameron
#			- apply stripslashes to $BookTag to support php5.4 ( 2016-01-15 cancel this as there's error when tag contains ")
#
#	Date:	2015-04-15	Cameron
#			- redirect to current edit page after save record 
#
#	Date:	2014-08-18 (Henry)
#	Details: Add delete check box for deleting a book cover
#
#	Date:	2014-07-30 (Henry)
#	Details: Add field Dimension and ILL
#
#	Date:	2013-09-12	Ivan
#			- added periodical-related logic 
#
#	Date:	2013-08-12	Jason
#			- comment some info as they no longer exist in Book page and move to item page such as ACNO, Purchase info, Barcode info 
#
############ Change Log End ###############

####### Trim all request #######
function recursive_trim(&$array){
	foreach($array as &$item){
		if (is_array($item)){
			recursive_trim($item);
		}else{
			if (is_string($item))
				$item=trim($item);
		}
	}
}
recursive_trim($_REQUEST);
recursive_trim($_POST);

####### END  OF   Trim all request #######

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libimage.php");


//include($PATH_WRT_ROOT."includes/Upload.class.php");
//$upload = new Upload;


//****** start special handle for php5.4+
if (function_exists('get_magic_quotes_gpc') && !get_magic_quotes_gpc()) {

	// 1. strip slashes again	
	if (!function_exists('magicQuotes_stripslashes2')) {
		function magicQuotes_stripslashes2(&$value, $key) {
		    if(!isset($_SESSION[$key])) {
			    $value = stripslashes($value);
		    }
		}
	}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, 'magicQuotes_stripslashes2');
    
	// 2. assign registered variables
	if (!function_exists('assignRequestToVariables')) {
		function assignRequestToVariables(&$value, $key) {
			
			// assign $_REQUEST value to global register variable
			global ${$key};
			if (isset($_REQUEST[$key]) && !isset($_SESSION[$key])){
				// Assign value of $_REQUEST into global register variable
				${$key} = $value;
			}
		}
	}
	$gpc = array(&$_REQUEST);
	array_walk_recursive($gpc, 'assignRequestToVariables');
	
	// prevent UserID being changed
	if(session_is_registered_intranet("UserID") && session_is_registered_intranet("LOGIN_INTRANET_SESSION_USERID")){
	    if($UserID != $_SESSION["LOGIN_INTRANET_SESSION_USERID"] || $_SESSION['UserID'] != $_SESSION["LOGIN_INTRANET_SESSION_USERID"]){
	        $UserID = $_SESSION["LOGIN_INTRANET_SESSION_USERID"];
	        $_SESSION['UserID'] = $_SESSION["LOGIN_INTRANET_SESSION_USERID"];
	    }
	}
	
}
//****** end special handle for php5.4+

intranet_auth();
intranet_opendb();




# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
/*
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}
//*/
if(sizeof($_POST)==0) {
	header("Location: item_list.php?xmsg=UpdateUnsuccess");	
	exit;
}
if($Code=="") {
	header("Location: item_list.php?xmsg=UpdateUnsuccess");
	exit;	
}

$Code = mysql_real_escape_string(htmlspecialchars($Code));
////////////////////update cover image////////////////////

$result = $libms->SELECTVALUE('LIBMS_BOOK', 'CoverImage', 'BookID', 'BookID='.$Code);
$coverimg_path = $result[0][CoverImage];
/*
if (!empty($BookCover)) {
	if (!empty($coverimg_path) && file_exists($coverimg_path)){

		unlink($coverimg_path);
	}
	
	$count = (int) ($Code/2000);
	$upload->uploadFile("/file/lms/cover/".$count."/", 'latin', 100,date('U'));
	if (isset($upload->_files['BookCover'])){
		$cover_image =  "/file/lms/cover/".$count."/".$upload->_files['BookCover'];
	}else{
		$cover_image ='';
	}
}else{
	if(isset($cover_image));
		unset($cover_image);
}

if(isset($cover_image))
	$dataAry['CoverImage'] = $cover_image;
else{
	if (isset($dataAry['CoverImage'])){
		unset($dataAry['CoverImage']);
	}
}

*///
if (!empty($BookCover)) {

	if (!empty($coverimg_path) && file_exists($intranet_root.$coverimg_path)){
		unlink($intranet_root.$coverimg_path);
	}

	$count = (int) ($Code/2000);
	require('import/api/class.upload.php');
		$handle = new Upload($_FILES['BookCover']);
		if ($handle->uploaded) {
			//$handle->Process($intranet_root."/home/library_sys/admin/book/import/tmp/");
			//$handle->Process("./import/pear/file/lms_book_import/");
			/*
			if (!(is_dir($PATH_WRT_ROOT."../intranetdata/file/lms/cover/".$count))){
				mkdir($PATH_WRT_ROOT."../intranetdata/file/lms/cover/".$count, 0775, true);			}
				
			$handle->Process($PATH_WRT_ROOT."../intranetdata/file/lms/cover/".$count."/");
			*/
			
			$cover_folder = $intranet_root."/file/lms/cover/".$count."/".$Code;
			$handle->Process($cover_folder);

			if ($handle->processed) {
				// everything was fine !
				//$handle->file_dst_name . '">' . $handle->file_dst_name . '</a>';
				$uploadSuccess = true;
				//$cover_image = str_replace("../../../../../intranetdata", "", $handle->file_dst_pathname);
				$image_obj = new SimpleImage();
				$image_obj->load($handle->file_dst_pathname);
				
				$imageInfo = $libms->changeImageOrientation($image_obj->image, $handle->file_dst_pathname, $image_obj->getWidth(), $image_obj->getHeight());
				if ($imageInfo['width'] != $image_obj->getWidth() && $imageInfo['height'] != $image_obj->getHeight()) {
				    $image_obj->image = $imageInfo['image'];
				    $image_obj->resize($imageInfo['width'],$imageInfo['height']);
				}
				
				$image_obj->resizeToMax(140, 200);
				$image_obj->save($handle->file_dst_pathname);
				$cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
			} else {
				// one error occured
				$xmsg = $Lang['libms']['import_book']['upload_fail']  . $handle->error . $Lang['libms']['import_book']['contact_admin'];
				$uploadSuccess = false;
				echo $xmsg;
			}

			// we delete the temporary files
			$handle-> Clean();
		}

}else{
	if(isset($cover_image));
		unset($cover_image);
}

if(isset($cover_image))
	$dataAry['CoverImage'] = $cover_image;
else{
	if (isset($dataAry['CoverImage'])){
		unset($dataAry['CoverImage']);
	}
}

if ($delete_photo == 1){
	if (!empty($coverimg_path) && file_exists($intranet_root.$coverimg_path)){
		unlink($intranet_root.$coverimg_path);
		
	}
	$dataAry['CoverImage'] = '';
}
////////////////////end cover image////////////////////




################## ACNO suggestion LOG insert/update ##################
/*
$dataAry['BookCode'] = str_replace('\\"','',$BookCode);
$dataAry['BookCode'] = str_replace('"','',$dataAry['BookCode']);

preg_match('/^([a-zA-Z]+)(\d+)$/',$dataAry['BookCode'],$matches);
if (!empty($matches)){
	$prefix = $matches[1];
	$number = $matches[2];
	
	$sql = "SELECT `Key`, `Next_Number` FROM  `LIBMS_ACNO_LOG` WHERE `Key` LIKE '{$prefix}' AND  `Next_Number` > '{$number}' ";
	$result = $libms->returnArray($sql);
	if (empty($result)){
		$nextnum = $number+1;
		$sql = "INSERT INTO `LIBMS_ACNO_LOG` (`Key`, `Next_Number`) VALUES ('{$prefix}','{$nextnum}') ON DUPLICATE KEY UPDATE `Next_Number`= '{$nextnum}'";
		//$sql = "UPDATE `LIBMS_ACNO_LOG` SET `Next_Number`= '{$number}'  WHERE `Key` LIKE '{$prefix}' LIMIT 1";
		$libms->db_db_query($sql);
 
	}
	$dataAry['ACNO_Prefix'] = htmlspecialchars($prefix);
	$dataAry['ACNO_Num'] = htmlspecialchars($number);
}else{
	intranet_closedb();
	header("Location: index.php?xmsg=update_failed");
	die();

}
*/
#################### end of ACNO suggestion LOG insert/update ##################

$dataAry['CallNum'] = htmlspecialchars($BookCallno);
$dataAry['CallNum2'] = htmlspecialchars($BookCallno2);
$dataAry['ISBN'] = htmlspecialchars($BookISBN);
$dataAry['ISBN2'] = htmlspecialchars($BookISBN2);
$dataAry['NoOfCopy'] = htmlspecialchars($BookNocopy);
$dataAry['NoOfCopyAvailable'] = htmlspecialchars($BookNoavailable);
$dataAry['BookTitle'] = htmlspecialchars($BookTitle);
$dataAry['BookSubTitle'] = htmlspecialchars($BookSubtitle);
$dataAry['Introduction'] = htmlspecialchars($BookIntro);
$dataAry['Language'] = htmlspecialchars($BookLang);
$dataAry['Country'] = htmlspecialchars($BookCountry);
$dataAry['Edition'] = htmlspecialchars($BookEdition);
$dataAry['PublishPlace'] = htmlspecialchars($BookPublishplace);
$dataAry['Publisher'] = htmlspecialchars($BookPublisher);
$dataAry['PublishYear'] = htmlspecialchars($BookPublishyear);
$dataAry['NoOfPage'] = htmlspecialchars($BookNopage);
$dataAry['RemarkInternal'] = htmlspecialchars($BookInternalremark);
$dataAry['RemarkToUser'] = htmlspecialchars($BookUserremark);
//$dataAry['PurchaseDate'] = htmlspecialchars($BookPurchasedate);
//$dataAry['Distributor'] = htmlspecialchars($BookDistributor);
//$dataAry['PurchaseNote'] = htmlspecialchars($BookPurchasenote);
//$dataAry['PurchaseByDepartment'] = htmlspecialchars($BookPurchasedept);
//$dataAry['ListPrice'] = htmlspecialchars($BookListprice);
//$dataAry['Discount'] = htmlspecialchars($BookDiscount);
//$dataAry['PurchasePrice'] = htmlspecialchars($BookPurchaseprice);
//$dataAry['AccountDate'] = htmlspecialchars($BookAccdate);
$dataAry['CirculationTypeCode'] = htmlspecialchars($BookCirclation);
$dataAry['ResourcesTypeCode'] = htmlspecialchars($BookResources);
$dataAry['BookCategoryCode'] = htmlspecialchars($BookCategory);
$dataAry['LocationCode'] = htmlspecialchars($BookLocation);
$dataAry['Subject'] = htmlspecialchars($BookSubj);
$dataAry['Series'] = htmlspecialchars($BookSeries);
$dataAry['SeriesNum'] = htmlspecialchars($BookSeriesno);
$dataAry['URL'] = htmlspecialchars($BookUrl);
$dataAry['ResponsibilityCode1'] = htmlspecialchars($ResponsibilityCodeOption1);
$dataAry['ResponsibilityBy1'] = htmlspecialchars($BookResponb1);
$dataAry['ResponsibilityCode2'] = htmlspecialchars($ResponsibilityCodeOption2);
$dataAry['ResponsibilityBy2'] = htmlspecialchars($BookResponb2);
$dataAry['ResponsibilityCode3'] = htmlspecialchars($ResponsibilityCodeOption3);
$dataAry['ResponsibilityBy3'] = htmlspecialchars($BookResponb3);
$dataAry['OpenBorrow'] = htmlspecialchars($BookBorrow);
$dataAry['OpenReservation'] = htmlspecialchars($BookReserve);
$dataAry['RecordStatus'] = htmlspecialchars($BookStatus);

if ($IsPeriodical) {
	$dataAry['IsPeriodical'] = htmlspecialchars($IsPeriodical);
	$dataAry['PeriodicalCode'] = htmlspecialchars($PeriodicalCode);
	$dataAry['ISSN'] = htmlspecialchars($ISSN);
}

$dataAry['Dimension'] = htmlspecialchars($BookDimension);
$dataAry['ILL'] = htmlspecialchars($BookILL);

foreach ($dataAry as $key => &$feild){
// 	if ( $feild == '' ){
// 		unset ($dataAry[$key]);
// 	}
	$feild = PHPToSQL($feild);
	
}


$result = $libms->UPDATE2TABLE("LIBMS_BOOK",$dataAry,array("BookID"=> $Code));
//$result = $libms->UPDATE_BOOK("LIBMS_BOOK", $Code, $dataAry);

/////////////END UPDATE LIBMS_BOOK


######################## UPDATE ANCO TABLE for auto suggestion #####
/*
if ($result){
	preg_match('/^(\w+)(\d+)$/',$BookCode, $matches);
	list($WHOLE,$term,$number) = $matches;
	$sql = "SELECT `Key`, `Next_Number` FROM  `LIBMS_ACNO_LOG` WHERE `Key` LIKE '{$term}' ";
	$result = $libms->returnArray($sql);
	if ($number >= $result[0]['Next_Number']){
		$number++;
		$sql = "UPDATE `LIBMS_ACNO_LOG` SET `Next_Number` = {$number} WHERE `Key` LIKE '{$term}' ";
		$libms->db_db_query($sql);
	}
}
*/
######################## enod of UPDATE ANCO TABLE #####

////////////////////start update table "LIBMS_BOOK_TAG"////////////////////

// function trim_1(&$tmp_array){
// 	$tmp_array   =  str_replace("\"", "" ,$tmp_array); 
// }

//$TagArray = mysql_real_escape_string(htmlspecialchars($BookTag));


// $TagArray = explode(",", trim($BookTag,"[]"));
// array_walk($TagArray, 'trim_1');


$libms->CLEAR_RECORD('LIBMS_BOOK_TAG', 'BookID='.$Code);

if ($BookTag != '') {
	$TagArray =  json_decode ($BookTag);
	
	foreach($TagArray as &$field)
	{
		$field = mysql_real_escape_string(stripcslashes(newline2space($field)));
		$tag_id = $libms->SELECTVALUE("LIBMS_TAG", "TagID", "TagID", "TagName = '".$field."'");
		if (empty($tag_id))
			{
			$newTag['TagName'] = $field;
			$libms->INSERT2DB($newTag, 'LIBMS_TAG');
			$tag_id = $libms->SELECTVALUE("LIBMS_TAG", "TagID", "TagID", "TagName = '".$field."'");
			}
		$newBookTag['BookID'] = $Code;
		$newBookTag['TagID'] = $tag_id[0]['TagID'];
		$libms->INSERT2DB($newBookTag, 'LIBMS_BOOK_TAG');
	}
}

////////////////////end update table "LIBMS_BOOK_TAG"////////////////////


////////////////////start insert table "LIBMS_BOOK_UNIQUE"////////////////////
/*
$b = 0;
foreach($BookBarcode as $key => $field)
{
	//if new book
	if( is_array($field)){
		for($i = 0 ;$i < count($field) ;$i++){
			$newbarcode = $field[$i];
			if (!empty($newbarcode)){
				$bookUniqueAry['BookID'] = $Code;
				$bookUniqueAry['RecordStatus'] = PHPToSQL($barcode_bookstatus['new'][$i]);
				$bookUniqueAry['CreationDate'] = 'now()';
				
				if('***AUTO GEN***' != $newbarcode){
					$bookUniqueAry['BarCode'] = PHPToSQL($newbarcode);
				}else{
					$bookUniqueAry['BarCode'] = PHPToSQL(rand().time());
				}
 
				$libms->INSERT2TABLE('LIBMS_BOOK_UNIQUE', $bookUniqueAry);
				
				if('***AUTO GEN***' == $newbarcode){
					$uniqueID = mysql_insert_id() ;
					$barcode = $uniqueID +1000000;
					$bookUniqueAry ['BarCode'] = PHPToSQL("A+{$barcode}");
					$whereclause = array('UniqueID' =>$uniqueID);
					$libms->UPDATE2TABLE('LIBMS_BOOK_UNIQUE',$bookUniqueAry,$whereclause);
					unset($bookUniqueAry);
				}
			}
		}
	} else {
		$bookUniqueAry['BookID'] = $Code;
		$bookUniqueAry['Barcode_BAK'] = "`BarCode`";
		
		//if update book
		if('***AUTO GEN***' != $field){
			$bookUniqueAry['BarCode'] = PHPToSQL($field);
		}else{
			$bookUniqueAry['BarCode'] = PHPToSQL("A+{$key}");
	 	}
	 	$bookUniqueAry['RecordStatus'] = PHPToSQL($barcode_bookstatus[$b]);
	 	if($barcode_bookstatus[$b] == 'DELETE'){
	 		$bookUniqueAry['Barcode_BAK']='`BarCode`';
	 		$bookUniqueAry['BarCode']='NULL';
	 	}
	 	
	 	$whereclause = array('UniqueID' =>$key);
	 	$libms->UPDATE2TABLE('LIBMS_BOOK_UNIQUE',$bookUniqueAry,$whereclause);
	 	unset($bookUniqueAry);
	}
	$b++;
}
*/

$libel = new elibrary();
$libel->SYNC_PHYSICAL_BOOK_TO_ELIB($Code);
////////////////////end insert table "LIBMS_BOOK_UNIQUE"////////////////////

//////////////////////correct the no copy of book /////////////////////////////
/*
$sql=<<<EOF
UPDATE LIBMS_BOOK b SET  
	`NoOfCopyAvailable` = ( 
		SELECT COUNT( * ) FROM  `LIBMS_BOOK_UNIQUE` bu
		WHERE bu.`BookID` = '{$Code}' AND 
			bu.`RecordStatus` IN ('NORMAL',  'SHELVING') 
	), 
	`NoOfCopy` = ( 
		SELECT COUNT( * ) FROM  `LIBMS_BOOK_UNIQUE` bu
		WHERE bu.`BookID` = '{$Code}' AND 
			bu.`RecordStatus` NOT IN ('LOST',  'WRITEOFF',  'SUSPECTEDMISSING')
	)
	WHERE b.BookID = '{$Code}'
EOF;
$libms->db_db_query($sql);
*/
$libms->UPDATE_BOOK_COPY_INFO($Code);
////////////////////end correct the no copy of book /////////////////////////////


intranet_closedb();

if ($FromPeriodical) {
	header("Location: ../periodical/index.php?task=periodicalEdit&BookID=".$book_id."&returnMsgKey=UpdateSuccess");
}
else {
	//header("Location: index.php?xmsg=UpdateSuccess");
	header("Location: book_edit.php?Code=$Code&xmsg=UpdateSuccess");
}

?>