<?php
// using: 
/*
 * 	Log
 * 	Date:	2015-04-13 [Cameron] Fix bug: should show asterisk(*) in front of user name if UserType is A(Alumni) or deleted user 
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
################################################################################################################
if(!isset($BookID) || empty($BookID)){
	header("Location: index.php");
}

################################################################################################################

if (isset($elibrary_plus_mgmt_book_page_size) && $elibrary_plus_mgmt_book_page_size != "") 
	$page_size = $elibrary_plus_mgmt_book_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == '') ? 1 : $order;
$field = ($field == "") ? 3 : $field;
$FromPage = (isset($FromPage) && $FromPage != '') ? $FromPage : 'book';

## Use Library
# in all boook management PHP pages
$libms = new liblms();

# Access Checking
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$li = new libdbtable2007($field, $order, $pageNo);
## Data Table
//global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";

## Init 
$btnOption = ''; 
$CurTab = 'CirculationDetails';
## Preparation
$CurrentPageArr['LIBMS'] = 1;
//$CurrentPage = "PageBookManagementBookList";
$BookSubTab = $libms->get_book_manage_sub_tab($FromPage,$CurTab,$BookID,$UniqueID);
$CurrentPage = $libms->get_book_manage_cur_page($FromPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$libms->MODULE_AUTHENTICATION($CurrentPage);
$TAGS_OBJ[] = array($Lang["libms"]["action"][strtolower($FromPage)]);
$userNameField = Get_Lang_Selection('iu.ChineseName', 'iu.EnglishName');

## DB SQL

$sql = "SELECT 
			CASE 
				WHEN u.RecordStatus = 3 THEN CONCAT('<span class=\"tabletextrequire\">*</span>', u.{$Lang['libms']['SQL']['UserNameFeild']})
				WHEN (u.RecordStatus = 0 AND a.UserID is not null) THEN
					CONCAT('<span class=\"tabletextrequire\">*</span>', u.{$Lang['libms']['SQL']['UserNameFeild']})
				ELSE
					IF (u.UserType='A',CONCAT('<span class=\"tabletextrequire\">*</span>', u.{$Lang['libms']['SQL']['UserNameFeild']}), 
						u.{$Lang['libms']['SQL']['UserNameFeild']})
			END  AS UserName, u.ClassName, u.ClassNumber, 
			IF(bl.BorrowTime='0000-00-00 00:00:00','--',bl.BorrowTime) BorrowTime,
			IF(bl.DueDate='0000-00-00','--',bl.DueDate) DueDate,
			IF(bl.ReturnedTime='0000-00-00 00:00:00','--',bl.ReturnedTime) ReturnedTime,
			CONCAT('<span class=\"table_row_tool row_content_tool\"><a href=\"javascript:jsDeleteData(', bl.BorrowLogID, ')\" class=\"delete_dim\" title=\"".$Lang["libms"]["general"]["del"]."\"></a></span>')
		FROM 
			LIBMS_BORROW_LOG bl
		JOIN
			LIBMS_USER u ON u.UserID = bl.UserID
		LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER a on a.UserID=u.UserID		
		WHERE 
			bl.BookID = '".$BookID."'
		AND
			bl.UniqueID = '".$UniqueID."'
		";
# DB - condition

// BookdCode = ACNO 
$li->field_array = array("UserName", "ClassName", "ClassNumber", "BorrowTime", "DueDate", "ReturnedTime");

$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";
$li->no_navigation_row = true;

$pos = 0;
$li->column_list .= "<th width='1%' class='tabletoplink'>#</th>\n"; 
$li->column_list .= "<th width='14%'>".$li->column($pos++, $Lang["libms"]["book"]["user_name"])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column($pos++, $Lang['libms']['bookmanagement']['ClassName'])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column($pos++, $Lang['libms']['bookmanagement']['ClassNumber'])."</th>\n";
$li->column_list .= "<th width='20%'>".$li->column($pos++, $Lang['libms']['bookmanagement']['LoanDate'])."</th>\n";
$li->column_list .= "<th width='20%'>".$li->column($pos++, $Lang['libms']['bookmanagement']['DueDate'])."</th>\n";
$li->column_list .= "<th width='20%'>".$li->column($pos++, $Lang['libms']['bookmanagement']['ReturnDate'])."</th>\n";
$li->column_list .= "<th width='1%'>&nbsp;</th>\n";


############################################################################################################

#Get ACNO
$sql1 = "select ACNO
		from LIBMS_BOOK_UNIQUE
		where (RecordStatus NOT LIKE 'DELETE' OR RecordStatus IS NULL) AND BookID = '".$BookID."' AND UniqueID = '".$UniqueID."'
		";
$acno = current($libms->returnVector($sql1));		
# Top menu highlight setting
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['circulation_details'], "circulation_details.php?FromPage=".$FromPage."&BookID=".$BookID);
$PAGE_NAVIGATION[] = array($acno, "");

$BookSubTab = $libms->get_book_manage_sub_tab($FromPage,$CurTab,$BookID,$UniqueID);

$RespondInfo = $libms->GET_BOOK_INFO($BookID);
$BookTitle = $RespondInfo[0]['BookTitle'];

$htmlAry['cancelBtn'] = '';
if (strtolower($FromPage)=='periodical') {
	$htmlAry['cancelBtn'] = $libms->get_book_manage_cancel_button($BookID, $FromPage);
}
?>



<SCRIPT LANGUAGE="Javascript">
function jsDeleteData(BorrowLogID)
{
	var strMsg = "<?=$Lang["libms"]["GroupManagement"]["DeleteWarning"]?>";
	if (confirm(strMsg))
	{
		self.location = "book_item_borrow_record_remove.php?BookID=<?=$BookID?>&FromPage=<?=$FromPage?>&UniqueID=<?=$UniqueID?>&BorrowLogID="+BorrowLogID;
	}
	
}
</SCRIPT>


<table class="form_table">
<tr>
  <td><?=$Lang['libms']['bookmanagement']['bookTitle']?> : <?=$BookTitle?></td>
</tr>
</table>
<br style="clear:both" />	
<?=$BookSubTab?>
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
<form name="form1" id="form1" method="POST" action="book_item_borrow_record.php?FromPage=<?=$FromPage?>&BookID=<?=$BookID?>&UniqueID=<?=$UniqueID?>" onSubmit="return checkForm()">
	<div class="content_top_tool"  style="float: left;">

		</div>
	<div class="content_top_tool"  style="float: right;">
		<div class="Conntent_search"><?=$searchTag?></div>     
		<br style="clear:both" />
	</div>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td valign="bottom">
		<?=$StatusSelection?>
	</td>

	</tr>
	</table>

	<?=$li->display();?>
	<br />
	<?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?>
	<br/>
	<div class="edit_bottom_v30">
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
	
	<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
	<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
	<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
	<input type="hidden" name="page_size_change" id="page_size_change" value="" />
	<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
	<input type="hidden" name="FromPage" id="FromPage" value="<?=$FromPage?>" />
</form>


<?php
$linterface->LAYOUT_STOP();

################################################################################################################
intranet_closedb();
?>