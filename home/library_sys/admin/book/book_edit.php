<?php


// IMPORTANT: UTF-8 Encoding



if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

// error_reporting(E_ERROR | E_WARNING | E_PARSE);
// ini_set('display_errors', 1);

# using:    

############ Change Log Start ###############
#
#	Date:	2019-07-23 [Henry]
#	Details: use the local library of jquery
#
#	Date:	2017-05-19 [Cameron]
#	Details: fix bug on replacing new line / line feed character with space to eliminate js error in Taglist (apply newline2space) (case #Q117253)
#
#	Date:	2016-04-29 [Cameron]
#	Details: add autocomplete for following inputselect fields:
#		Country, Publisher, Publication place, Series, Responsibility by, Responsibility by 2, Responsibility by 3, Subject 			
#
#	Date:	2016-02-18 [Cameron]
#	Details: add inputselect class for author (BookResponb1 ~ BookResponb3)
#
#	Date:	2015-11-19 [Henry]
#	Details: fixed: Fail to add tag and copy book [Case#K89160]
#
#	Date:	2015-10-29 [Cameron]
#	Details: apply escape_double_quotes() to $RespondInfo (Book info)
#
#	Date:	2015-09-25 [Tiffany]
#	Details: change the single quotes and double quotes to HTML entity for book title
#
#	Date:	2015-09-23 [Cameron]
#	Details: change maxlength from 4 to 16 chars for "Number of pages" field 
#
#	Date:	2015-07-31 (Cameron)
#	Ddtails: fix bug on showing BookCategory in case Language is not set but BookCategory is set and BookCategoryType=2
#		!!! Note: if BookCategory is set to a category that's conflicted with Language on BookCategoryType attribute, here could not resolve 	
#
#	Date:	2015-07-27 (Cameron)
#	Details: add parameter $CheckType=true to options list of GET_SELECTION_BOX() for BookCategory, RecoursesType, CirculationType,
#		Language and Responsibility
#	
#	Date:	2015-05-27 (Henry)
#	Details: enlarge the input size of BookTitle and BookSubtitle
#
#	Date:	2015-05-12 (Henry)
#	Details: enlarge the input size of ResponsibilityBy1, ResponsibilityBy2 and ResponsibilityBy3
#
#	Date:	2015-04-15 (Cameron)
#	Details: show update success/fail message
#
#	Date:	2015-04-02 (Cameron)
#	Details: correct link for cancel button (book_item_list.php if PageFrom=item)
#
#	Date:	2015-01-15 (Henry)
#	Details: apply the language settings and multi-type of book category
#
#	Date:	2014-09-15 (Tiffany)
#	Details: delect maxlength of the BookISBN2, BookSeriesno
#
#	Date:	2014-09-10 (Henry)
#	Details: enlarge the input size of BookPublishyear
#
#	Date:	2014-08-18 (Henry)
#	Details: Add delete check box for deleting a book cover
#
#	Date:	2014-07-30 (Henry)
#	Details: Add field Dimension and ILL
#
#	Date:	2014-03-18 (Henry)
#	Details: Subject max length changed from 32 to 64
#
#	Date:	2013-09-07 (Ivan)
#	Details: added periodical info display if the book is created from periodical
#
#	Date:	2013-08-22 (Ivan)
#	Details: Internal remarks max length changed from 32 to 128
#
#	Date:	2013-08-12 (Jason)
#	Details: remove some info and move to item page such as ACNO, Purchase Info, Barcode, Book Location
#
#	Date:	2013-07-17 (Henry Chan)
#	Details: Display the image of the book cover instead of the image path (coverImage)
#
############ Change Log End ###############

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

$home_header_no_EmulateIE7 = true;

intranet_auth();
intranet_opendb();


# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
## Init 
$CurTab = 'Book';
$FromPage = (isset($FromPage) && $FromPage != '') ? $FromPage : 'book';
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagement";
$libms->MODULE_AUTHENTICATION($CurrentPage);
$linterface = new interface_html();

$Code = (is_array($Code)) ? $Code[0] : $Code;

$RespondInfo = $libms->GET_BOOK_INFO($Code);
foreach((array)$RespondInfo[0] as $k=>$v) {
	$RespondInfo[0][$k] = escape_double_quotes($v);
}

$BookTitle = $RespondInfo[0]['BookTitle'];
//$BookTitle = str_replace('"', "&quot;",$BookTitle);	// already done by calling escape_double_quotes()
$BookTitle = str_replace("'", "&#39;",$BookTitle);

$BookSubTitle = $RespondInfo[0]['BookSubTitle'];
if (isset($IsCopy) && $IsCopy)
{
	$NoOfCopy = 0;
	$NoOfCopyAvailable = 0;
	
} else
{
//	$BookCode = $RespondInfo[0]['BookCode'];
	$NoOfCopy = $RespondInfo[0]['NoOfCopy'];
	$NoOfCopyAvailable = $RespondInfo[0]['NoOfCopyAvailable'];
$BarCodeNewStr = "[new]";
	
}

$CallNum = $RespondInfo[0]['CallNum'];
$CallNum2 = $RespondInfo[0]['CallNum2'];
$ISBN = $RespondInfo[0]['ISBN'];
$ISBN2 = $RespondInfo[0]['ISBN2'];
$BarCode = $RespondInfo[0]['BarCode'];
$Introduction = $RespondInfo[0]['Introduction'];
$Language = $RespondInfo[0]['Language'];
$Country = $RespondInfo[0]['Country'];
$Edition = $RespondInfo[0]['Edition'];
$PublishPlace = $RespondInfo[0]['PublishPlace'];
$Publisher = $RespondInfo[0]['Publisher'];
$PublishYear = $RespondInfo[0]['PublishYear'];
$NoOfPage = $RespondInfo[0]['NoOfPage'];
$RemarkInternal = $RespondInfo[0]['RemarkInternal'];
$RemarkToUser = $RespondInfo[0]['RemarkToUser'];
$CirculationTypeCode = $RespondInfo[0]['CirculationTypeCode'];
$ResourcesTypeCode = $RespondInfo[0]['ResourcesTypeCode'];
$BookCategoryCode = $RespondInfo[0]['BookCategoryCode'];
$eClassBookCode = $RespondInfo[0]['eClassBookCode'];
$LocationCode = $RespondInfo[0]['LocationCode'];
$Subject = $RespondInfo[0]['Subject'];
$Series = $RespondInfo[0]['Series'];
$SeriesNum = $RespondInfo[0]['SeriesNum'];
$URL = $RespondInfo[0]['URL'];
$ResponsibilityCode1 = $RespondInfo[0]['ResponsibilityCode1'];
$ResponsibilityBy1 = $RespondInfo[0]['ResponsibilityBy1'];
$ResponsibilityCode2 = $RespondInfo[0]['ResponsibilityCode2'];
$ResponsibilityBy2 = $RespondInfo[0]['ResponsibilityBy2'];
$ResponsibilityCode3 = $RespondInfo[0]['ResponsibilityCode3'];
$ResponsibilityBy3 = $RespondInfo[0]['ResponsibilityBy3'];
$CoverImage = $RespondInfo[0]['CoverImage'];
$OpenBorrow = $RespondInfo[0]['OpenBorrow'];
$OpenReservation = $RespondInfo[0]['OpenReservation'];
$RecordStatus = $RespondInfo[0]['RecordStatus'];
$IsPeriodical = $RespondInfo[0]['IsPeriodical'];
$PeriodicalCode = $RespondInfo[0]['PeriodicalCode'];
$ISSN = $RespondInfo[0]['ISSN'];

$Dimension = $RespondInfo[0]['Dimension'];
$ILL = $RespondInfo[0]['ILL'];
############################################################################################################


//$TAGS_OBJ[] = array($Lang['libms']['action']['edit_book']);
$TAGS_OBJ[] = array($Lang["libms"]["action"][strtolower($FromPage)]);
if (isset($IsCopy) && $IsCopy)
{
	$PAGE_NAVIGATION[] = array($Lang['Btn']['Copy'], "");
} else
{
	$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
}
//$CurrentPage = "PageBookManagementBookList";
$CurrentPage = $libms->get_book_manage_cur_page($FromPage);
$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
//$linterface->LAYOUT_START();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
if($FromPage=='item'){
	$cancel_url = 'book_item_list.php';
}else{
	$cancel_url = 'index.php';	
}
#Save and Cancel Button ...........
$SaveBtn   = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "SubmitBtn", "");
$SaveBtn2  = $linterface->GET_ACTION_BTN($button_submit, "button", "submit2()", "SubmitBtn", "");
//$CancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='".$cancel_url."'", "CancelBtn", "");
$CancelBtn = $libms->get_book_manage_cancel_button($Code, $FromPage, $cancel_url);

# Responsibility ........... 
$ResponsibilityCodeArrary = $libms->BOOK_GETOPTION('LIBMS_RESPONSIBILITY', 'ResponsibilityCode', $Lang['libms']['sql_field']['Description'], 'ResponsibilityCode', '');
$ResponsibilityCodeOption1 = $linterface->GET_SELECTION_BOX($ResponsibilityCodeArrary, " name='ResponsibilityCodeOption1' id='ResponsibilityCodeOption1' onChange='disable_enable_the_follow_input(1)' ", $Lang['libms']['status']['na'], $ResponsibilityCode1, true);
$ResponsibilityCodeOption2 = $linterface->GET_SELECTION_BOX($ResponsibilityCodeArrary, " name='ResponsibilityCodeOption2' id='ResponsibilityCodeOption2' onChange='disable_enable_the_follow_input(1)' ", $Lang['libms']['status']['na'], $ResponsibilityCode2, true);
$ResponsibilityCodeOption3 = $linterface->GET_SELECTION_BOX($ResponsibilityCodeArrary, " name='ResponsibilityCodeOption3' id='ResponsibilityCodeOption3' onChange='disable_enable_the_follow_input(1)' ", $Lang['libms']['status']['na'], $ResponsibilityCode3, true);

# Option ..........
$BookCategoryArray = $libms->BOOK_GETOPTION('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', "CONCAT(`BookCategoryCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookCategoryCode', 'BookCategoryType=1');
$BookCategoryOption = $linterface->GET_SELECTION_BOX($BookCategoryArray, ' name="BookCategory" id="BookCategory" ', $Lang['libms']['status']['na'], $BookCategoryCode, true);
//Henry Added
$BookCategoryArray2 = $libms->BOOK_GETOPTION('LIBMS_BOOK_CATEGORY', 'BookCategoryCode', "CONCAT(`BookCategoryCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookCategoryCode', 'BookCategoryType=2');
$BookCategoryOption2 = $linterface->GET_SELECTION_BOX($BookCategoryArray2, ' name="BookCategory" id="BookCategory" ', $Lang['libms']['status']['na'], $BookCategoryCode, true);

if (empty($Language) && $BookCategoryCode) {
	$bookCategoryType = $libms->SELECTVALUE('LIBMS_BOOK_CATEGORY', 'BookCategoryType', 'BookCategoryType', "BookCategoryCode='$BookCategoryCode'");
	if (count($bookCategoryType) > 0) {
		$BookCategoryType = $bookCategoryType; 
	}
	else {
		$BookCategoryType = "";
	}
}
else {
	$BookCategoryType = $libms->SELECTVALUE('LIBMS_BOOK_LANGUAGE', 'BookCategoryType', 'BookCategoryType', 'BookLanguageCode="'.$Language.'"');
}

if (is_array($BookCategoryType)) {
	$BookCategoryTypeVal = $BookCategoryType[0]['BookCategoryType'];
}
else {
	$BookCategoryTypeVal = "";
}

$BookCategoryOptionDisplay = $BookCategoryOption;
if($BookCategoryType && $BookCategoryTypeVal == 2){
	$BookCategoryOptionDisplay = $BookCategoryOption2;
}

$BookCirculationArray = $libms->BOOK_GETOPTION('LIBMS_CIRCULATION_TYPE', 'CirculationTypeCode', $Lang['libms']['sql_field']['Description'], 'CirculationTypeCode', '');
$BookCirculationOption = $linterface->GET_SELECTION_BOX($BookCirculationArray, " name='BookCirclation' id='BookCirclation' ", $Lang['libms']['status']['na'], $CirculationTypeCode, true);

$BookResourcesArray = $libms->BOOK_GETOPTION('LIBMS_RESOURCES_TYPE', 'ResourcesTypeCode', $Lang['libms']['sql_field']['Description'], 'ResourcesTypeCode', '');
$BookResourcesOption = $linterface->GET_SELECTION_BOX($BookResourcesArray, " name='BookResources' id='BookResources' ", $Lang['libms']['status']['na'], $ResourcesTypeCode, true);

$BookLanguageArray = $libms->BOOK_GETOPTION('LIBMS_BOOK_LANGUAGE', 'BookLanguageCode', "CONCAT(`BookLanguageCode`,' - ',`{$Lang['libms']['sql_field']['Description']}`)", 'BookLanguageCode', '');
$BookLanguageOption = $linterface->GET_SELECTION_BOX($BookLanguageArray, " name='BookLang' id='BookLang' ", $Lang['libms']['status']['na'], $Language, true);
$BookLanguageOption .= '<input type="hidden" id="hid_LanguageCategoryCode" value="'.$BookCategoryTypeVal.'"/>';

# Book Tag ..........
$BookTagArray = $libms->SELECTVALUE('LIBMS_TAG', 'TagName', 'TagName');
foreach ($BookTagArray as $value) $Taglist .="'".addslashes(trim(newline2space($value[TagName])))."', ";
$Taglist = rtrim($Taglist,', ');

# Book selected Tag..............................
$SelectedBookTag = $libms->SELECTVALUE('LIBMS_BOOK_TAG JOIN LIBMS_TAG', 'TagName', 'TagName', 'LIBMS_BOOK_TAG.TagID = LIBMS_TAG.TagID AND BookID='.$Code);
foreach ($SelectedBookTag as $value) $SelectedTaglist .="'".addslashes(trim(newline2space($value[TagName])))."', ";
$SelectedTaglist = rtrim($SelectedTaglist,', ');
$BookSubTab = $libms->get_book_manage_sub_tab($FromPage,$CurTab,$Code,$UniqueID);


# delete book cover checkbox...................
if($CoverImage != ""){
	$delete_photo = "<input type='checkbox' id='delete_photo' name='delete_photo' value='1'><label for='delete_photo'>".$Lang["libms"]["book"]["delete_cover_image"]."</label></input><br>";
}
?>
<script src="/templates/jquery/jquery-1.6.1.min.js" type="text/javascript" charset="utf-8"></script>

<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">

<script src="./TextExt.js" type="text/javascript" charset="utf-8"></script>

<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--
	
function checknumeric(){
	var checking = true;
	$('.numberic').each(function() {
		if (($(this).val() != "") && (isNaN(Number($(this).val())) || $(this).val() < 0)){
			var field = $(this).parent().prev().find('span').html();
			alert("<?=$Lang["libms"]["book"]["alert"]["numeric"]?> "+field);	
			$(this).focus(); 
			checking = false;
			return false;	
	}});
	return checking;
};


function checkForm(form1) {
	var selfcheck = true;

	if(form1.BookTitle.value=='')	 {
		alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['book']['title'] ?>");	
		form1.BookTitle.focus();
		return false;
//	} else if($('#BookCode').val()=="") {
//		alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['book']['code'] ?>");	
//		$('#BookCode').focus(); 
//		return false;
	} else if(!checknumeric()) {
		return false;
//	} else if(!selfcheck || !unique_barcode_check || !unique_bookcode){
//		alert("<?= $Lang['libms']['book']['code']."/".$Lang['libms']['book']['barcode'] ." incorrect" ?>");	
//		return false;
	} else {
		<? if ($IsPeriodical) { ?>
			if(form1.PeriodicalCode.value=='') {
				alert("<?= $i_alert_pleasefillin.' '.$Lang["libms"]["periodicalMgmt"]["PeriodicalCode"] ?>");	
				form1.PeriodicalCode.focus();
				return false;
			}
			else {
				var codeValid = true;
				$.ajax({
					type: "POST",
					url: "../periodical/index.php",
					async: false,
					data: "task_ajax=ajax_task&ActionType=validatePeriodicalCode&PeriodicalCode=" + $('input#PeriodicalCodeTb').val() + "&BookID=" + $('input#Code').val(),
					success: 
							function(ReturnData) {
								if (ReturnData == '0') {
									$('input#PeriodicalCodeTb').focus();
									alert('<?=$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["CodeInUse"]?>');
									codeValid = false;
								}
								else {
									codeValid = true;
								}
							}
				});
				return codeValid;
			}
			
		<? } else { ?>
			return true;
		<? } ?>
	}
}
//-->


var count_available ;
var unique_bookcode = true;
var fSubmit = false; 

var timerobj;

$().ready( function(){
	$.ajaxSetup({	cache:false,
					async:false
	});

	//disable enter as submit
	$('input').keydown(function(e) {
		if (event.which == 13) {
		event.preventDefault();
		return false;
		}
	});
	// autocomplete book tag
	$('#BookTag')
		.textext({
			plugins : 'tags autocomplete',
			tagsItems: [ <?= $SelectedTaglist?> ] //selected tags
		})
		.bind('getSuggestions', function(e, data)
		{
			var list = [<?= $Taglist?>],
				textext = $(e.target).textext()[0],
				query = (data ? data.query : '') || ''
				;

			$(this).trigger(
				'setSuggestions',
				{ result : textext.itemManager().filter(list, query) }
			);
		});

	// autocomplete for inputselect fields
	$('.inputselect').each(function(){
		var this_id = $(this).attr('id');
		if($(this).length > 0){
			$(this).autocomplete(
		      "ajax_get_suggestions.php",
		      {
		  			delay:3,
		  			minChars:1,
		  			matchContains:1,
		  			extraParams: {'field':this_id},
		  			autoFill:false,
		  			overflow_y: 'auto',
		  			overflow_x: 'hidden',
		  			maxHeight: '200px'
		  		}
		    );
		}
	});

	
	$("input.inputselect").inputselect({
		image_path: '/images/2009a/', 
		ajax_script: 'ajax_get_selection_list.php', 
		js_lang_alert: {'no_records' : '<?=$Lang['libms']['NoRecordAtThisMoment']?>' }
	});
	
	$('#delete_photo').click(function(){
	    if($(this).is(':checked')){
	        $('#BookCover').hide();
	    } else {
	       	$('#BookCover').show();
	    }
	});
	
	// Book Language onchange......................................
	$('#BookLang').change( function(){
		$.ajax({
	       url: "ajax_get_book_category_type.php",
	       type: "post",
	       data: { language_code: $('#BookLang').val()},
	       success: function(data){
	           //alert(data);
	           if(data == 2 && $('#hid_LanguageCategoryCode').val() != 2){
	           	$("#div_BookCategory").html('<?=preg_replace('/\s+/', ' ', $BookCategoryOption2)?>');
	           	$('#hid_LanguageCategoryCode').val('2');
	           }else if(data == 1 && $('#hid_LanguageCategoryCode').val() != 1){
	           	$("#div_BookCategory").html('<?=preg_replace('/\s+/', ' ', $BookCategoryOption)?>');
	           	$('#hid_LanguageCategoryCode').val('1');
	           }
	       },
	       error:function(){
	           //alert("failure");
	           //$("#result").html('There is error while submit');
	       }
	   });
	});
	
});

function del_row(JQueryObj){
	JQueryObj.parents('tr:first').remove();
	update_barcode_number();
}

function submit2(){
	$('#setItem').val(1);
	var obj = document.form1;
	if(!checkForm(obj)){
		return false;
	}
	obj.submit();
}

function replaceSpace(txtInputObj)
{
	var txtValue=txtInputObj.value;
	var spacePosition = txtValue.search(" ");

	if (spacePosition==0)
	{
		txtInputObj.value = txtValue.trim()
	} else (spacePosition>0)
	{
		txtInputObj.value = txtValue.replace(" ", ".");
	}
}
</script>
<table class="form_table" width="100%">
<tr>
  <td align="left"><font face="微軟正黑體" size="+1">《 <b><?=$BookTitle?></b>》</font></td>
</tr>
</table>
<br style="clear:both" />		
<?=$BookSubTab?>
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<?php if (isset($IsCopy) && $IsCopy) { ?>
<form name="form1" method="POST" action="book_new_update.php"  onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data" >
<?php } else { ?>
<form name="form1" method="POST" action="book_edit_update.php"  onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data" >
<input id="Code" name="Code" type="hidden" class="textboxtext" value="<?=$Code?>" />
<?php } ?>
<table width="90%" border="0" align="center">
	<tr> 
		<td class="main_content">
			<div class="table_board">
                <p class="spacer"></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="tabletextrequire">*</span> <?= $Lang['libms']['book']['title'] ?></td>
                        	<td width="33%"><input name="BookTitle" type="text" class="textboxtext" maxlength="256" value="<?=$BookTitle?>"/></td>
                        	<td width="4%">&nbsp;</td>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['subtitle'] ?></span></td>
                         	<td width="33%"><input name="BookSubtitle" type="text" class="textboxtext" maxlength="256" value="<?=$BookSubTitle?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['call_number'] ?> <br />(<?= $Lang['libms']['book']['call_number_desc'] ?>)</span></td>
                        	<td><input name="BookCallno" type="text" class="textboxtext" maxlength="255" value="<?=$CallNum?>" <?=$sys_custom['eLibraryPlus']['CallNumAllowSpace'] ? '' : 'onkeyup="replaceSpace(this)"'?> /></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['call_number2'] ?> <br />(<?= $Lang['libms']['book']['call_number2_desc'] ?>)</span></td>
                         	<td><input name="BookCallno2" type="text" class="textboxtext" maxlength="255" value="<?=$CallNum2?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['ISBN'] ?></span></td>
                        	<td><input name="BookISBN" type="text" class="textboxtext" maxlength="255" value="<?=$ISBN?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['ISBN2'] ?></span></td>
                         	<td><input name="BookISBN2" type="text" class="textboxtext" value="<?=$ISBN2?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['language'] ?></span></td>
                        	<td><!--<input id="BookLang" name="BookLang" type="text" class="textboxtext inputselect" maxlength="32" value="<?=$Language?>"/>--><?=$BookLanguageOption?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['country'] ?></span></td>
                         	<td><input id="BookCountry" name="BookCountry" type="text" class="textboxtext inputselect" maxlength="128" value="<?=$Country?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['introduction'] ?></span></td>
                        	<td colspan="4"><textarea name="BookIntro" class="textboxtext"><?=$Introduction?></textarea></td>
						</tr>		
                  	</tbody>
				</table>
				
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_publish'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['edition'] ?></span></td>
                        	<td width="33%"><input name="BookEdition" type="text" class="textboxtext" maxlength="32" value="<?=$Edition?>"/></td>
                        	<td width="4%">&nbsp;</td>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['publish_year'] ?></span></td>
                         	<td width="33%"><input name="BookPublishyear" id="BookPublishyear" type="text" class="textboxnum" maxlength="32" value="<?=$PublishYear?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['publisher'] ?></span></td>
                        	<td><input id="BookPublisher" name="BookPublisher" type="text" class="textboxtext inputselect" maxlength="128" value="<?=$Publisher?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['publish_place'] ?></span></td>
                         	<td><input id="BookPublishplace" name="BookPublishplace" type="text" class="textboxtext inputselect" maxlength="64" value="<?=$PublishPlace?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['series'] ?></span></td>
                        	<td><input id="BookSeries" name="BookSeries" type="text" class="textboxtext inputselect" maxlength="64" value="<?=$Series?>"/></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['series_number'] ?></span></td>
                         	<td><input name="BookSeriesno" type="text" class="textboxtext" value="<?=$SeriesNum?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_code1'] ?></span></td>
                        	<td><?=$ResponsibilityCodeOption1?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_by1'] ?></span></td>
                         	<td><input id="BookResponb1" name="BookResponb1" type="text" class="textboxtext inputselect" maxlength="256" value="<?=$ResponsibilityBy1?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_code2'] ?></span></td>
                        	<td><?=$ResponsibilityCodeOption2?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_by2'] ?></span></td>
                         	<td><input id="BookResponb2" name="BookResponb2" type="text" class="textboxtext inputselect" maxlength="256" value="<?=$ResponsibilityBy2?>"/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_code3'] ?></span></td>
                        	<td><?=$ResponsibilityCodeOption3?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['responsibility_by3'] ?></span></td>
                         	<td><input id="BookResponb3" name="BookResponb3" type="text" class="textboxtext inputselect" maxlength="256" value="<?=$ResponsibilityBy3?>"/></td>
						</tr>
						
						<tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['DIMEN'] ?></span></td>
                         	<td><input name="BookDimension" type="text" class="textboxtext" maxlength="32" value="<?=$Dimension?>"/></td>
							<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['ILL'] ?></span></td>
                         	<td><input name="BookILL" type="text" class="textboxtext" maxlength="32" value="<?=$ILL?>"/></td>
						</tr>
						
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['number_of_page'] ?></span></td>
                        	<td><input name="BookNopage" type="text" class="textboxnum" style="width:150px" maxlength="16" value="<?=$NoOfPage?>"/></td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
						</tr>
                  	</tbody>
				</table>
				
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_category'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td class="field_title_short" width="15%" ><span class="field_title"><?= $Lang['libms']['settings']['book_category'] ?></span></td>
                        	<td width="33%"><div id="div_BookCategory"><?=$BookCategoryOptionDisplay?></div></td>
                        	<td width="4%">&nbsp;</td>
                        	<td class="field_title_short" width="15%" ><span class="field_title"><?= $Lang['libms']['settings']['circulation_type'] ?></span></td>
                         	<td width="33%"><?=$BookCirculationOption?></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['settings']['resources_type'] ?></span></td>
                        	<td><?=$BookResourcesOption?></td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['subject'] ?></span></td>
                        	<td><input id="BookSubj" name="BookSubj" type="text" class="textboxtext inputselect" maxlength="64" value="<?=$Subject?>"/></td>
						</tr>			
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['remark_internal'] ?></span></td>
                         	<td><textarea name="BookInternalremark" class="textboxtext"><?=$RemarkInternal?></textarea>
                         	</td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
						</tr>
                  	</tbody>
				</table>
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_other'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td  class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['cover_image'] ?></span></td>
                        	<td colspan="3"><?=($CoverImage != ""?'<img src="'.$CoverImage.'" /><br/>':'')?><?=$delete_photo?><input name="BookCover" id="BookCover" type="file" /></td>
						</tr>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['tags'] ?></span></td>
                        	<td width="70%"><textarea autocomplete="off" id="BookTag" name="BookTag" class="textboxtext" rows="1" style='width :99%'
							></textarea></td>
                        	<td width="15%"> <span class="tabletextremark"><?=$Lang['libms']['book']['tags_input']?></span></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['URL'] ?></span></td>
                        	<td ><input name="BookUrl" type="text" class="textboxtext" maxlength="255" value="<?=$URL?>"/></td>
                        	<td>&nbsp;</td>
						</tr>
                  	</tbody>
				</table>
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['libms']['book']['info_circulation'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['number_of_copy'] ?></span></td>
                        	<td width="33%"><label id='lbl_BookNocopy' ><?=$NoOfCopy?></label><input id="BookNocopy" name="BookNocopy" type="hidden" class="textboxnum" maxlength="2" value="<?=$NoOfCopy?>" readonly/></td>
                        	<td width="4%" >&nbsp;</td>
                        	<td width="15%" class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['number_of_copy_available'] ?></span></td>
                         	<td width="33%"><label id='lbl_BookNoavailable' ><?=$NoOfCopyAvailable?></label><input id="BookNoavailable" name="BookNoavailable" type="hidden" class="textboxnum" maxlength="2" value="<?=$NoOfCopyAvailable?>" readonly/></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['borrow_reserve'] ?></span></td>
                        	<td><input name="BookBorrow" type="checkbox" name="BookBorrow" value="1" <?if($OpenBorrow == "1") echo "checked"; ?> /> <label for="BookBorrow"><?=$Lang['libms']['book']['open_borrow']?></label> &nbsp; 
                        		<input type="checkbox" name="BookReserve" value="1" <?if($OpenReservation == "1") echo "checked"; ?>/> <label for="BookReserve"><?=$Lang['libms']['book']['open_reserve']?></label>
                        	</td>
                        	<td>&nbsp;</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['remark_to_user'] ?></span></td>
                         	<td><input name="BookUserremark" type="text" class="textboxtext" maxlength="32" value="<?=$RemarkToUser?>"/></td>
						</tr>
                  	</tbody>
				</table>
				
				<? if ($IsPeriodical) { ?>
				<p></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang["libms"]["periodicalMgmt"]["PeriodicalInfo"] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
        		<table class="form_table_v30">
					<tbody>
                        <tr>
                        	<td class="field_title_short" width="15%" ><span class="tabletextrequire">*</span> <span class="field_title"><?= $Lang["libms"]["periodicalMgmt"]["PeriodicalCode"] ?></span></td>
                        	<td width="33%"><?=$linterface->GET_TEXTBOX('PeriodicalCodeTb', 'PeriodicalCode', $PeriodicalCode, $OtherClass='', $OtherPar=array('maxlength'=>$lmsConfigAry['maxLengthAry']['PeriodicalCode']))?></td>
                        	<td width="4%">&nbsp;</td>
                        	<td class="field_title_short" width="15%" ><span class="field_title"><?= $Lang["libms"]["periodicalMgmt"]["ISSN"] ?></span></td>
                         	<td width="33%"><?=$linterface->GET_TEXTBOX('ISSNTb', 'ISSN', $ISSN, $OtherClass='', $OtherPar=array('maxlength'=>$lmsConfigAry['maxLengthAry']['ISSN']))?></td>
						</tr>
					</tbody>
				</table>
				<input type="hidden" id="IsPeriodical" name="IsPeriodical" value="<?=$IsPeriodical?>" />
				<? } ?>
				
				<p></p>
				
	<?=$linterface->MandatoryField();?>
				<center>
                    <p class="spacer"></p>
                    <?= $IsCopy?$SaveBtn2:$SaveBtn ?>
                    <?= $CancelBtn ?>
                </center>
					<p class="spacer"></p>
			</div>
		</td>
	</tr>
</table>
<input type="hidden" name="userID" id="userID" value="<?=$userID?>">
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" name="FromPage" id="FromPage" value="<?=$FromPage?>" />
<input type="hidden" name="setItem" id="setItem" value="" />
</form><?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>