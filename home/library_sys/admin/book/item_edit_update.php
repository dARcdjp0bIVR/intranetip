<?php
# using: 
/*
 * Modification Log: 
 *
 * 2020-09-25 (Henry)
 *      - add flag $sys_custom['eLibraryPlus']['AutoGenBarcodeWithNumber'] to control the auto generate barcode with number only
 *
 * 2019-08-19 (Cameron)
 *      - fix: return fail message if one the the process failed (e.g. violate uniqueness of barcode) [case #F165714]
 *
 * 2018-09-10 (Cameron)
 *      - add code to prevent UserID being changed in magicQuotes_stripslashes2()
 * 
 * 2016-03-03 (Cameron)
 * 		- apply recursive_trim to $_POST [case #F93085]
 * 		- strip slashes and assign registered variables after call lib.php for php5.4+
 * 
 * 2015-04-15 (Cameron)
 * 		- redirect to current edit page after save record 
 * 
 * 2014-10-22 (Yuen) #ip.2.5.5.10.1
 * 		- introduce circulation type field
 * 
 * 2014-09-16 (Tiffany)
 *      - Add field ItemSeriesNum
 *      
 * 2014-09-12 (Henry)
 * 		- added to call function UPDATE_BOOK_STATUS_IF_RESERVED() if the changing the book status to 'NORMAL'
 * 
 * 2014-07-30 (Henry)
 * 		- Add field AccompanyMaterial and RemarkToUser
 * 
 * 2013-09-11 (Ivan)
 * 		- support periodical registration
 * 2013-09-11 (Jason)
 * 		- improve to handle the ACNO which is a pure number or intersect with some alphabet  characters
 */

ini_set('memory_limit', '256M');
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";
####### Trim all request #######
function recursive_trim(&$array){
	foreach($array as &$item){
		if (is_array($item)){
			recursive_trim($item);
		}else{
			if (is_string($item))
				$item=trim($item);
		}
	}
}
recursive_trim($_REQUEST);
recursive_trim($_POST);
####### END  OF   Trim all request #######

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_periodical_item.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libimage.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");


//****** start special handle for php5.4+
if (function_exists('get_magic_quotes_gpc') && !get_magic_quotes_gpc()) {

	// 1. strip slashes again	
	if (!function_exists('magicQuotes_stripslashes2')) {
		function magicQuotes_stripslashes2(&$value, $key) {
		    if(!isset($_SESSION[$key])) {
			    $value = stripslashes($value);
		    }
		}
	}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, 'magicQuotes_stripslashes2');
    
	// 2. assign registered variables
	if (!function_exists('assignRequestToVariables')) {
		function assignRequestToVariables(&$value, $key) {
			
			// assign $_REQUEST value to global register variable
			global ${$key};
			if (isset($_REQUEST[$key]) && !isset($_SESSION[$key])){
				// Assign value of $_REQUEST into global register variable
				${$key} = $value;
			}
		}
	}
	$gpc = array(&$_REQUEST);
	array_walk_recursive($gpc, 'assignRequestToVariables');
	
	// prevent UserID being changed
	if(session_is_registered_intranet("UserID") && session_is_registered_intranet("LOGIN_INTRANET_SESSION_USERID")){
	    if($UserID != $_SESSION["LOGIN_INTRANET_SESSION_USERID"] || $_SESSION['UserID'] != $_SESSION["LOGIN_INTRANET_SESSION_USERID"]){
	        $UserID = $_SESSION["LOGIN_INTRANET_SESSION_USERID"];
	        $_SESSION['UserID'] = $_SESSION["LOGIN_INTRANET_SESSION_USERID"];
	    }
	}
	
}
//****** end special handle for php5.4+


intranet_auth();
intranet_opendb();
############################################################################################################


## Get Data
$UniqueID = (isset($UniqueID) && $UniqueID != '') ? $UniqueID : '';
$BookID = (isset($BookID) && $BookID != '') ? $BookID : '';

$ACNO = (isset($ACNO) && $ACNO != '') ? trim($ACNO) : '';
$Purchasedate = (isset($Purchasedate) && $Purchasedate != '') ? $Purchasedate : '';
$Purchaseprice = (isset($Purchaseprice) && $Purchaseprice != '') ? $Purchaseprice : '';
$Listprice = (isset($Listprice) && $Listprice != '') ? $Listprice : '';
$Discount = (isset($Discount) && $Discount != '') ? $Discount : '';
$Distributor = (isset($Distributor) && $Distributor != '') ? $Distributor : '';
$Purchasedept = (isset($Purchasedept) && $Purchasedept != '') ? $Purchasedept : '';
$Purchasenote = (isset($Purchasenote) && $Purchasenote != '') ? $Purchasenote : '';
$Barcode = (isset($Barcode) && $Barcode != '') ? $Barcode : '';
$LocationCode = (isset($LocationCode) && $LocationCode != '') ? $LocationCode : '';
$InvoiceNumber = (isset($InvoiceNumber) && $InvoiceNumber != '') ? $InvoiceNumber : '';
$AccountDate = (isset($AccountDate) && $AccountDate != '') ? $AccountDate : '';

$AccompanyMaterial = (isset($AccompanyMaterial) && $AccompanyMaterial != '') ? $AccompanyMaterial : '';
$RemarkToUser = (isset($RemarkToUser) && $RemarkToUser != '') ? $RemarkToUser : '';
$ItemSeriesNum = (isset($ItemSeriesNum) && $ItemSeriesNum != '') ? $ItemSeriesNum : '';

$IsNew = ($UniqueID != '') ? false : true;		# copy is regarded as New process


## Use Library
# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

$BookInfoAry = $libms->GET_BOOK_INFO($BookID);
$IsPeriodical = $BookInfoAry[0]['IsPeriodical']; 
$PeriodicalBookID = $BookInfoAry[0]['PeriodicalBookID'];
$PeriodicalItemID = $BookInfoAry[0]['PeriodicalItemID'];


# Access Checking
if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	intranet_closedb();
	$xmsg = ($IsNew) ? 'AddUnsuccess' : 'UpdateUnsuccess';
	header("Location: item_list.php?xmsg=".$xmsg);	
	exit;
}


## Init
$result = array();


////////////////////update cover image////////////////////
/*if (!empty($BookCover)) {
	
	$count = $libms->SELECTVALUE('LIBMS_BOOK', 'max(`BookID`) as max', 'max');
	$count = (int) ($count[0][max]/2000);
		$upload->uploadFile("/file/lms/cover/".$count."/", 'latin', 100,date('U'));
	//dump($upload->_files);
	if (isset($upload->_files['BookCover'])){
		// print "Upload Completed: ".   $upload->_files['file1'] . "<br>Check result<br>";
		$cover_image =  "/file/lms/cover/".$count."/".$upload->_files['BookCover'];
	}else{
		//no Image/
		$cover_image ='';
	}

}*/
////////////////////end cover image////////////////////




## Preparation
$ACNO = str_replace('\\"','', $ACNO);
$ACNO = str_replace('"','', $ACNO);
$dataAry['ACNO'] = $ACNO;


preg_match('/^([a-zA-Z]+)(\d+)$/',$dataAry['ACNO'], $matches);
//
//debug_r($dataAry);
//debug_r($matches);die();

$check_acno_log = true;
if (!empty($matches)){
	# string consists of alphabats and numeric number corresondingly
	$prefix = $matches[1];
	$number = $matches[2];
} 
else if(trim($ACNO) != '')
{
	if(preg_match('/^\d+$/',$ACNO)){
		# string contains number number only 
		$number = $ACNO;
		$prefix = $libms->acno_default_sys_prefix;
	} else {
		# string consists of alphabats and numeric number randomly
		$check_acno_log = false;
	}
} 
else 
{
	$check_acno_log = false;
	intranet_closedb();
	$xmsg = ($IsNew) ? 'AddUnsuccess' : 'UpdateUnsuccess';
	header("Location: item_list.php?xmsg=".$xmsg);
	die();
}

$libms->Start_Trans();

# Check and update ACNO LOG
if($check_acno_log == true){
	$sql = "SELECT `Key`, `Next_Number` FROM `LIBMS_ACNO_LOG` WHERE `Key` LIKE '{$prefix}' AND convert(`Next_Number`, unsigned) > '".(int)$number."' ";
	$acnoLogArr = $libms->returnArray($sql);
	
	if (empty($acnoLogArr)){
		# add new suggested ACNO nubmer for the next request use
		$orig_len = strlen($number);
		$nextnum = $number+1;
		if(strlen($nextnum) < $orig_len){
			$nextnum = str_pad($nextnum, $orig_len, "0", STR_PAD_LEFT);
		}
		
		$sql = "INSERT INTO `LIBMS_ACNO_LOG` (`Key`, `Next_Number`) VALUES 
					('{$prefix}','{$nextnum}') ON DUPLICATE KEY UPDATE `Next_Number`= '{$nextnum}'";
		$result['insert_acno_log'] = $libms->db_db_query($sql);
	}
	$dataAry['ACNO_Prefix'] = mysql_real_escape_string(htmlspecialchars($prefix));
	$dataAry['ACNO_Num'] = mysql_real_escape_string(htmlspecialchars($number));
}

# Check BookID
if($IsNew == true){
	if($BookID != ''){
		$dataAry['BookID'] = $BookID;
	} else {
	    $libms->RollBack_Trans();
		intranet_closedb();
		$xmsg = 'AddUnsuccess';
		header("Location: item_list.php?xmsg=".$xmsg);
		die();
	}
}

////////////////////start insert table "LIBMS_htmlspecialcharsBOOK"////////////////////
$dataAry['PurchaseDate'] = htmlspecialchars($Purchasedate);
$dataAry['Distributor'] = htmlspecialchars($Distributor);
$dataAry['PurchaseNote'] = htmlspecialchars($Purchasenote);
$dataAry['PurchaseByDepartment'] = htmlspecialchars($Purchasedept);
$dataAry['ListPrice'] = htmlspecialchars($Listprice);
$dataAry['Discount'] = htmlspecialchars($Discount);
$dataAry['PurchasePrice'] = htmlspecialchars($Purchaseprice);
$dataAry['RecordStatus'] = htmlspecialchars($ItemStatus);
$dataAry['LocationCode'] = htmlspecialchars($LocationCode);
$dataAry['InvoiceNumber'] = htmlspecialchars($InvoiceNumber);
$dataAry['CreationDate'] = htmlspecialchars($AccountDate);

$dataAry['AccompanyMaterial'] = htmlspecialchars($AccompanyMaterial);
$dataAry['RemarkToUser'] = htmlspecialchars($RemarkToUser);
$dataAry['ItemSeriesNum'] = htmlspecialchars($ItemSeriesNum);
$dataAry['CirculationTypeCode'] = htmlspecialchars($BookCirclation);

if ($IsNew && $IsPeriodical && $PeriodicalBookID > 0 && $PeriodicalItemID > 0) {
	$dataAry['PeriodicalBookID'] = htmlspecialchars($PeriodicalBookID);
	$dataAry['PeriodicalItemID'] = htmlspecialchars($PeriodicalItemID);
	
	$objItem = new liblms_periodical_item($dataAry['PeriodicalItemID']);
	$objItem->setRegistrationDate('now()');
	$objItem->setRegistrationBy($_SESSION['UserID']);
	$objItem->setQuantity($objItem->getQuantity() + 1);
	$itemId = $objItem->save();
	
	$result['update_periodical_registration'] = ($itemId > 0)? true : false; 
}


# Handle BarCode
if($GenBarCode == 'AUTO'){
	if($IsNew == false){
		$Barcode = $UniqueID +1000000;
		if($sys_custom['eLibraryPlus']['AutoGenBarcodeWithNumber']){
			$dataAry['BarCode'] = $Barcode;
		}
		else{
			$dataAry['BarCode'] = "A+{$Barcode}";
		}
	}
} else if($GenBarCode == 'ACNO'){
	$dataAry['BarCode'] = $ACNO;
} else {
	if($ItemStatus == 'DELETE'){
		$dataAry['Barcode_BAK'] = '`BarCode`';
		$dataAry['BarCode'] = 'NULL';
	} else {
		$dataAry['BarCode'] = $Barcode;
	}
}

# format the data value
foreach ($dataAry as $key => $field){
	if($field == ''){
		unset ($dataAry[$key]);
	}
	if($IsNew){
		$dataAry[$key] = mysql_real_escape_string($field);
	} else {
		$dataAry[$key] = PHPToSQL($field);
	}
}
//debug_r($dataAry);die();

if($IsNew){
	$result['insert_book_item'] = $libms->INSERT2DB($dataAry, 'LIBMS_BOOK_UNIQUE');
	$UniqueID = mysql_insert_id();
	
	# update bar code 
	if($GenBarCode == 'AUTO'){
		# auto generated
		$new_barcode = $UniqueID +1000000;
		if(!$sys_custom['eLibraryPlus']['AutoGenBarcodeWithNumber']){
			$new_barcode = "A+{$new_barcode}";
		}
		$result['update_barcode_for_new_item'] = $libms->UPDATE2TABLE('LIBMS_BOOK_UNIQUE',array('BarCode' => PHPToSQL($new_barcode)),array('UniqueID' =>$UniqueID));
	}
} else {
	$result['update_book_item'] = $libms->UPDATE2TABLE('LIBMS_BOOK_UNIQUE', $dataAry, array("UniqueID" => $UniqueID));
	
	######################## UPDATE ANCO TABLE for auto suggestion #####
	if ($result['update_book_item']){
		preg_match('/^(\w+)(\d+)$/', $ACNO, $matches);
		list($WHOLE,$term,$number) = $matches;
		$sql = "SELECT `Key`, `Next_Number` FROM  `LIBMS_ACNO_LOG` WHERE `Key` LIKE '{$term}' ";
		$ancoLogArr = $libms->returnArray($sql);
		if ($number >= $ancoLogArr[0]['Next_Number']){
			
			$orig_len = strlen($number);
			$number++;
			if(strlen($number) < $orig_len){
				$number = str_pad($number, $orig_len, "0", STR_PAD_LEFT);
			}
			
			$sql = "UPDATE `LIBMS_ACNO_LOG` SET `Next_Number` = {$number} WHERE `Key` LIKE '{$term}' ";
			$result['update_acno_log'] = $libms->db_db_query($sql);
		}
	}
	######################## enod of UPDATE ANCO TABLE #####
}

////////////////////end insert table "LIBMS_BOOK"////////////////////


////////////////////start insert table "LIBMS_BOOK_TAG"////////////////////
// function trim_1(&$tmp_array){
// 	$tmp_array   =  str_replace("\"", "" ,$tmp_array); 
// }
// tolog($BookTag);
// //$TagArray = mysql_real_escape_string(htmlspecialchars($BookTag));
// $TagArray = explode(",", trim($BookTag,"[]"));
// array_walk($TagArray, 'trim_1');

//$TagArray =  json_decode ($BookTag);

/*
foreach($TagArray as &$field)
{
	$field = mysql_real_escape_string(stripcslashes($field));
	$tag_id = $libms->SELECTVALUE("LIBMS_TAG", "TagID", "TagID", "TagName = '".$field."'");
	if (empty($tag_id))	{
		$newTag['TagName'] = $field;
		$libms->INSERT2DB($newTag, 'LIBMS_TAG');
		$tag_id = $libms->SELECTVALUE("LIBMS_TAG", "TagID", "TagID", "TagName = '".$field."'");
	}
	$newBookTag['BookID'] = $book_id;
	$newBookTag['TagID'] = $tag_id[0]['TagID'];
	$libms->INSERT2DB($newBookTag, 'LIBMS_BOOK_TAG');
}
*/
////////////////////end insert table "LIBMS_BOOK_TAG"////////////////////



////////////////////start insert table "LIBMS_BOOK_UNIQUE"////////////////////

/* old method
$b = 0;
foreach($Barcode as $key => $field)
{
	if(!empty($field)){
		if('***AUTO GEN***' != $field){
			$bookUniqueAry['BarCode'] = PHPTOSQL($field);
		}else{
			$bookUniqueAry['BarCode'] = PHPToSQL(rand().time());
		}
		
		$bookUniqueAry['BookID'] = $book_id;
		$bookUniqueAry['RecordStatus'] = PHPTOSQL($barcode_bookstatus[$b]);
		$bookUniqueAry['CreationDate'] = 'now()';
		
		$libms->INSERT2TABLE('LIBMS_BOOK_UNIQUE', $bookUniqueAry);
		
		if('***AUTO GEN***' == $field){
			$uniqueID = mysql_insert_id();
			$barcode = $uniqueID +1000000;
			$libms->UPDATE2TABLE('LIBMS_BOOK_UNIQUE',array('BarCode' => PHPToSQL("A+{$barcode}")),array('UniqueID' =>$uniqueID));
		}
	}
	$b++;
}
*/
 

$libel = new elibrary();
$libel->SYNC_PHYSICAL_BOOK_TO_ELIB($BookID);
////////////////////end insert table "LIBMS_BOOK_UNIQUE"////////////////////

//////////////////////correct the no copy of book /////////////////////////////
/*
$sql=<<<EOF
UPDATE LIBMS_BOOK b SET  
	`NoOfCopyAvailable` = ( 
		SELECT COUNT( * ) FROM  `LIBMS_BOOK_UNIQUE` bu
		WHERE bu.`BookID` = '{$BookID}' AND 
			bu.`RecordStatus` IN ('NORMAL',  'SHELVING', 'RESERVED') 
	), 
	`NoOfCopy` = ( 
		SELECT COUNT( * ) FROM  `LIBMS_BOOK_UNIQUE` bu
		WHERE bu.`BookID` = '{$BookID}' AND 
			bu.`RecordStatus` NOT IN ('LOST',  'WRITEOFF',  'SUSPECTEDMISSING', 'DELETE')
	)
	WHERE b.BookID = '{$BookID}'
EOF;
$libms->db_db_query($sql);
*/
$result['update_no_of_copy'] = $libms->UPDATE_BOOK_COPY_INFO($BookID);
if($ItemStatus == 'NORMAL') {
    $result['update_book_status'] = $libms->UPDATE_BOOK_STATUS_IF_RESERVED($UniqueID, $ItemStatus);
}

////////////////////end correct the no copy of book /////////////////////////////

if (!in_array(false, $result)) {
    $libms->Commit_Trans();
    $xmsg = ($IsNew) ? 'AddSuccess' : 'UpdateSuccess';
}
else {
    $libms->RollBack_Trans();
    $xmsg = ($IsNew) ? 'AddUnsuccess' : 'UpdateUnsuccess';
}

intranet_closedb();
//debug_r($result);die(); 

//$xmsg = ($IsNew) ? 'AddSuccess' : 'UpdateSuccess';

//if($setItem == 1){
$IsCopy = ($setItem == 1) ? 1 : 0;
?>
<body onLoad="document.MainForm.submit();">
<form name="MainForm" id="MainForm" action="book_item_edit.php?IsCopy=<?=$IsCopy?>&FromPage=<?=$FromPage?>&xmsg=<?=$xmsg?>" method="post">
<input type="hidden" name="UniqueID[]" id="UniqueID[]" value="<?=$UniqueID?>">
<input type="hidden" name="BookID" id="BookID" value="<?=$BookID?>">
<input type="hidden" name="FromPage" id="FromPage" value="<?=$FromPage?>">
</form>
</body>
<?php
//	//header("Location: item_edit.php?BookID=".$BookID."&xmsg=".$xmsg);
//} else {
//	header("Location: book_item_list.php?FromPage=".$FromPage."&BookID=".$BookID."&xmsg=".$xmsg);
//}
?>