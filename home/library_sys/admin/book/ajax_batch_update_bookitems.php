<?php
/*
 * 	modifying: 
 * 	Log
 * 	Purpose:	process update a field at a time for selected BookID
 *	@return:	array['result']		-- success (true) / fail (false) 
 * 				array['nrAffected']	-- number of record affected
 * 
 * 	Date:	2017-09-08 Cameron
 * 			- fix: should update number of book copies when update ItemStatus [case #T125139]
 * 
 * 	Date:	2017-04-12  Cameron
 * 			- also sync AccountDate to INTRANET_ELIB_BOOK
 * 
 * 	Date:	2017-02-09 	Cameron
 * 			- strip slashes after call lib.php for php5.4+
 * 
 *	Date:	2016-03-03  Cameron
 *			- use $_POST['Var'] instead of global variable $Var when assign value to $DataArr [case #F93085] 		
 *
 * 	Date:	2015-07-27 [Cameron] 
 * 			- add LastModifiedBy and DateModified
 * 			- sync data to IP by calling SYNC_PHYSICAL_BOOK_TO_ELIB() if Update ItemStatus
 * 
 * 	Date:	2014-12-17 [Cameron] create this file
 * 
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/json.php");


intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

####### Trim all request and convert html special chars #######
function recursive_trim(&$array){
	foreach($array as &$item){
		if (is_array($item)){
			recursive_trim($item);
		}else{
			if (is_string($item))
				$item=htmlspecialchars(trim($item));
		}
	}
}
recursive_trim($_POST);

//****** start special handle for php5.4+
if (function_exists('get_magic_quotes_gpc') && !get_magic_quotes_gpc()) {

	// 1. strip slashes again	
	if (!function_exists('magicQuotes_stripslashes2')) {
		function magicQuotes_stripslashes2(&$value, $key) {
			$value = stripslashes($value);
		}
	}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, 'magicQuotes_stripslashes2');
}
//****** end special handle for php5.4+


$BookItemID = $_POST['BookItemID'];
$UpdateField = $_POST['UpdateField'];
$UpdateValue = $_POST["$UpdateField"];
$result = 0;	// number of records updated
$nrAffected = 0;

if (is_array($BookItemID) && $UpdateField != '') {
	$bookItem = implode("','",$BookItemID);

	$DataArr = array();
	switch ($UpdateField) {
		case "ItemStatus":
			$DataArr["RecordStatus"] = $libms->pack_value($_POST['ItemStatus'],'str');
			break;
		case "BookCirculation":
			$DataArr["CirculationTypeCode"] = $libms->pack_value($_POST['BookCirculation'],'str');
			break;
		case "ItemLocation":
			$DataArr["LocationCode"] = $libms->pack_value($_POST['LocationCode'],'str');
			break;
		case "AccountDate":
			$DataArr["CreationDate"] = $libms->pack_value($_POST['AccountDate'],'date');
			break;
		case "PurchaseDate":
			$DataArr["PurchaseDate"] = $libms->pack_value($_POST['PurchaseDate'],'date');
			break;
		case "PurchasePrice":
			$DataArr["PurchasePrice"] = $libms->pack_value($_POST['PurchasePrice'],'str');
			break;
		case "Distributor":
			$DataArr["Distributor"] = $libms->pack_value($_POST['Distributor'],'str');
			break;
		case "Purchasedept":
			$DataArr["PurchaseByDepartment"] = $libms->pack_value($_POST['Purchasedept'],'str');
			break;
		case "PurchaseNote":
			$DataArr["PurchaseNote"] = $libms->pack_value($_POST['PurchaseNote'],'str');
			break;
		case "RemarkToUser":
			$DataArr["RemarkToUser"] = $libms->pack_value($_POST['RemarkToUser'],'str');
			break;
		default:
//			$DataArr["$UpdateField"] = $libms->pack_value($UpdateValue,'str');	
			break;
	}			
	
	$updateDetails = '';
	foreach ($DataArr as $fieldName => $data)
	{
		$updateDetails .= $fieldName."=".$data.",";
	}

	if ($updateDetails) {
		$updateDetails .= "LastModifiedBy='{$UserID}',";
		$updateDetails .= "DateModified=now()";
		
		$sql = "UPDATE LIBMS_BOOK_UNIQUE SET ".$updateDetails." WHERE UniqueID IN ('{$bookItem}')";

		$result = $libms->db_db_query($sql);
		$nrAffected = count($BookItemID);	// all selected rows, not mysql_affected_rows()
		
		if (($UpdateField == "ItemStatus") || ($UpdateField == "AccountDate")) {
			$sql = "SELECT BookID FROM LIBMS_BOOK_UNIQUE WHERE UniqueID IN ('{$bookItem}')";
			$BookID = $libms->returnVector($sql);
			
			if (count($BookID) > 0) {
				// sync to IP
				$libel = new elibrary();
				foreach($BookID as $bid) {
					$libms->UPDATE_BOOK_COPY_INFO($bid);		// should update number of copies
					$libel->SYNC_PHYSICAL_BOOK_TO_ELIB($bid);
				}
			}		
		}
	}			
}

$ret = array();
$ret['result'] = $result;
$ret['nrAffected'] = $nrAffected;
$json = new JSON_obj();
echo $json->encode($ret);

intranet_closedb();
?>