<?php
/* 
 * Editing by  
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']  )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$data = array();

if(isset($_REQUEST['q']))//If a q has been submitted 
{
	$search = $_REQUEST['q'];
	preg_match('/^([a-zA-Z]+)(\d*)$/', $search, $matches);
	
	if(!empty($matches)){
		$no_prefix = false;
		$prefix = $matches[1];		# alphbat 
		$suffix = $matches[2];		# numberic number 
	} else {
		# input which does not contain any alphabat character
		$no_prefix = true; 
		$prefix = $libms->acno_default_sys_prefix;			# system defined
		$is_int = (preg_match('/^\d+$/',$search)) ? true : false;
	}
	
	# old method - no longer use 
	if($no_prefix && $is_int || !$no_prefix){
		$sql = "SELECT CONCAT(`Key`, `Next_Number`) FROM  `LIBMS_ACNO_LOG` 
				WHERE `Key` LIKE '".mysql_real_escape_string($prefix)."%' ";
		$result = $libms->returnArray($sql,null,2);
		
		//$array[][0] => array[]
		foreach ($result as $row){
			$data[] = $row[0];
		}
	}
}

echo json_encode($data);

?>