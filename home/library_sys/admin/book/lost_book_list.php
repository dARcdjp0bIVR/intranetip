<?php
# Editing by 

/**
 * 	Log :
 
 * 	2020-04-27 Cameron
 * 		- create this file	[case #Y173908]
 */
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	//$order = $ck_right_page_order;
	$order = 0;		# default in ascending order
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}


intranet_auth();
intranet_opendb();

# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageMgmtReturnLostBook";

$libms->MODULE_AUTHENTICATION($CurrentPage);

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['ReturnLostBook']);

# Top menu highlight setting
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();


$linterface = new interface_html();

############################################################################################################
///*
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;


$lost_book_price_by = $libms->get_system_setting('circulation_lost_book_price_by');
if ($lost_book_price_by == 1) {
    $priceField = "bu.`ListPrice`";
    $SndPriceField = "bu.`PurchasePrice`";
} else {
    $priceField = "bu.`PurchasePrice`"; // if set fixed price but no record in OVERDUE_LOG, use PurchasePrice
    $SndPriceField = "bu.`ListPrice`";
}

$current_period = getPeriodOfAcademicYear(Get_Current_Academic_Year_ID());
$today = date('Y-m-d');
// avoid & problem when encrypted code in production
if ($current_period) {
    $current_period['StartDate'] = $current_period['StartDate'] ? date('Y-m-d', strtotime($current_period['StartDate'])) : $today;
    $current_period['EndDate'] = $current_period['EndDate'] ? date('Y-m-d', strtotime($current_period['EndDate'])) : $today;
} else {
    $current_period['StartDate'] = $today;
    $current_period['EndDate'] = $today;
}
// foreach($current_period as &$date){
// $date = date('Y-m-d',strtotime($date));
// }

$_POST['DueDateAfter'] = empty($_POST['DueDateAfter']) ? $current_period['StartDate'] : $_POST['DueDateAfter'];
$_POST['DueDateBefore'] = empty($_POST['DueDateBefore']) ? $current_period['EndDate'] : $_POST['DueDateBefore'];

$conds = " bu.`RecordStatus`='LOST' ";

$keyword = trim($_POST['keyword']);
if (! get_magic_quotes_gpc()) {
    $keyword = stripslashes($keyword);
}

if ($keyword != "") {
    $unconverted_keyword_sql = mysql_real_escape_string(str_replace("\\", "\\\\", $keyword));
    $converted_keyword_sql = special_sql_str($keyword);
    if ($unconverted_keyword_sql == $converted_keyword_sql) {
        $conds .= " AND (
        bu.`ACNO` LIKE '%$unconverted_keyword_sql%' OR
        b.`CallNum` LIKE '%$unconverted_keyword_sql%' OR
        b.`ISBN` LIKE '%$unconverted_keyword_sql%' OR
        b.`BookTitle` LIKE '%$unconverted_keyword_sql%' OR
        b.`ResponsibilityBy1` LIKE '%$unconverted_keyword_sql%' OR
        blu.`UserLogin` LIKE '%$unconverted_keyword_sql%' OR
        blu.`UserEmail` LIKE '%$unconverted_keyword_sql%' OR
        blu.`EnglishName` LIKE '%$unconverted_keyword_sql%' OR
        blu.`ChineseName` LIKE '%$unconverted_keyword_sql%' OR
        blu.`BarCode` LIKE '%$unconverted_keyword_sql%'
        )";
    } else {
        $conds .= " AND (
        bu.`ACNO` LIKE '%$unconverted_keyword_sql%' OR
        b.`CallNum` LIKE '%$unconverted_keyword_sql%' OR
        b.`ISBN` LIKE '%$unconverted_keyword_sql%' OR
        b.`BookTitle` LIKE '%$unconverted_keyword_sql%' OR
        b.`ResponsibilityBy1` LIKE '%$unconverted_keyword_sql%' OR
        blu.`UserLogin` LIKE '%$unconverted_keyword_sql%' OR
        blu.`UserEmail` LIKE '%$unconverted_keyword_sql%' OR
        blu.`EnglishName` LIKE '%$unconverted_keyword_sql%' OR
        blu.`ChineseName` LIKE '%$unconverted_keyword_sql%' OR
        blu.`BarCode` LIKE '%$unconverted_keyword_sql%' OR
        
        bu.`ACNO` LIKE '%$converted_keyword_sql%' OR
        b.`CallNum` LIKE '%$converted_keyword_sql%' OR
        b.`ISBN` LIKE '%$converted_keyword_sql%' OR
        b.`BookTitle` LIKE '%$converted_keyword_sql%' OR
        b.`ResponsibilityBy1` LIKE '%$converted_keyword_sql%' OR
        blu.`UserLogin` LIKE '%$converted_keyword_sql%' OR
        blu.`UserEmail` LIKE '%$converted_keyword_sql%' OR
        blu.`EnglishName` LIKE '%$converted_keyword_sql%' OR
        blu.`ChineseName` LIKE '%$converted_keyword_sql%' OR
        blu.`BarCode` LIKE '%$converted_keyword_sql%'
        )";
    }
}
IF (! empty($_POST['DueDateAfter'])) {
    $conds .= ' AND IF(ol.`DateCreated` is null, bu.`DateModified`, ol.`DateCreated`) >= ' . PHPToSQL($_POST['DueDateAfter'] . ' 00:00:00');
}
IF (! empty($_POST['DueDateBefore'])) {
    $conds .= ' AND IF(ol.`DateCreated` is null, bu.`DateModified`, ol.`DateCreated`) <= ' . PHPToSQL($_POST['DueDateBefore'] . ' 23:59:59');
}

$classNameClassNumber = $junior_mck ? "CONVERT(CONCAT(blu.ClassName,'-', blu.ClassNumber) USING utf8)" : "CONCAT(blu.ClassName,'-', blu.ClassNumber)";

$checkboxField = "CONCAT('<input type=\'checkbox\' name=\'OverDueLogID[]\' id=\'OverDueLogID[]\' value=\'', ol.`OverDueLogID`,'\'>')";
if ($sys_custom['eLibraryPlus']['PayBy_ePayment']) {
    $selectField = "IF(ol.RecordStatus='SETTLED' OR (ol.RecordStatus='OUTSTANDING' AND bal.TransRefID IS NOT NULL AND ol.PaymentReceived>0) OR (ol.RecordStatus='WAIVED' AND bal.TransRefID IS NOT NULL) OR ol2.OverDueLogID IS NOT NULL,'<font color=\"red\">*</font>',$checkboxField)";
}
else {
    $selectField = $checkboxField;
}
$recordStatus = "CASE ol.RecordStatus WHEN 'OUTSTANDING' THEN '".$Lang["libms"]["report"]["fine_outstanding"]."' 
                    WHEN 'SETTLED' THEN '".$Lang["libms"]["CirculationManagement"]["paied"]."' 
                    WHEN 'WAIVED' THEN '".$Lang["libms"]["report"]["fine_waived"]."' 
                    ELSE '--'
                 END AS RecordStatus,";

$sql = <<<EOL
SELECT
	IF(ol.`DateCreated` is null, bu.`DateModified`, ol.`DateCreated`) as DateModified,
    IF (blu.ClassName IS NULL OR blu.ClassName='' OR blu.ClassNumber IS NULL OR blu.ClassNumber='', '-', {$classNameClassNumber}) AS ClassInfo,
	IF(blu.`{$Lang['libms']['SQL']['UserNameFeild']}` is null or blu.`{$Lang['libms']['SQL']['UserNameFeild']}` = '', ' -- ', blu.`{$Lang['libms']['SQL']['UserNameFeild']}`) as UserName ,
	bu.ACNO,
	IF((b.`CallNum`='' or b.`CallNum` is null) and (b.`CallNum2`='' or b.`CallNum2` is null), ' -- ', CONCAT_WS(  ' ', b.`CallNum` , b.`CallNum2` )) AS CallNum,
	b.`BookTitle` ,
	IF(bu.PurchasePrice is null, ' -- ', bu.PurchasePrice) as PurchasePrice,
	IF(bu.ListPrice is null, ' -- ', bu.ListPrice) as ListPrice,
	IF(ol.`LostBookPriceBy` is null, IF($priceField is null or $priceField='' , IF($SndPriceField is null or $SndPriceField='', ' -- ', $SndPriceField),$priceField), IF(ol.`Payment` is null or ol.`Payment`='', ' -- ', ol.`Payment`)) as Penalty,
    {$recordStatus}
    {$selectField},
    ol.OverDueLogID
FROM (	SELECT bgu.* FROM (
			SELECT bl.`UniqueID`, bl.`BorrowLogID`, u.`EnglishName`, u.`ChineseName`, u.`UserLogin`, u.`BarCode`, u.`UserEmail`, u.ClassName, u.ClassNumber
			FROM  `LIBMS_BORROW_LOG` bl
			INNER JOIN `LIBMS_USER` u ON bl.`UserID`=u.`UserID`
			WHERE bl.`RecordStatus`='LOST'
			ORDER BY bl.`UniqueID`, bl.`DateModified` DESC) AS bgu
		GROUP BY bgu.`UniqueID`
	) AS blu
    INNER JOIN `LIBMS_BOOK_UNIQUE` bu ON bu.`UniqueID`=blu.`UniqueID`
    INNER JOIN `LIBMS_BOOK` b ON b.`BookID`=bu.`BookID`
    INNER JOIN `LIBMS_OVERDUE_LOG` ol ON ol.`BorrowLogID`= blu.`BorrowLogID` AND ol.RecordStatus<>'DELETE' AND (ol.DaysCount IS NULL OR ol.DaysCount=0)
    LEFT JOIN (
        SELECT 
                DISTINCT TransRefID 
        FROM 
                LIBMS_BALANCE_LOG 
        WHERE 
                TransType='OverdueLog' 
                AND TransDesc='pay overdue') AS bal ON bal.TransRefID=ol.OverDueLogID
    LEFT JOIN LIBMS_OVERDUE_LOG ol2 ON ol2.BorrowLogID=blu.BorrowLogID AND ol2.RecordStatus NOT IN ('DELETE','WAIVED') AND ol2.DaysCount>0 AND ol2.PaymentReceived>0 
WHERE
	{$conds}
EOL;
	

// debug_pr($sql);
// exit;	
$li = new libdbtable2007($field, $order, $pageNo);

global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";

$li->field_array = array("DateModified", "ClassInfo", "UserName", "ACNO","CallNum", "BookTitle", "PurchasePrice","ListPrice","Penalty","RecordStatus");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang["libms"]["book"]["lost_date"])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['class'])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['user_name'])."</td>\n";
$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang["libms"]["label"]["BookCode"])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['call_number'])."</td>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['libms']['book']['title'])."</td>\n";
$li->column_list .= "<th width='8%' >".$li->column($pos++, $Lang["libms"]["book"]["purchase_price"])."</td>\n";
$li->column_list .= "<th width='8%' >".$li->column($pos++, $Lang["libms"]["book"]["list_price"])."</td>\n";
$li->column_list .= "<th width='8%' >".$li->column($pos++, $Lang["libms"]["report"]["pay"])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang["libms"]["report"]["fine_status"])."</td>\n";
$li->column_list .= "<th width='1'>".$li->check("OverDueLogID[]")."</td>\n";

$keyword = intranet_htmlspecialchars($_POST['keyword']);
if (!get_magic_quotes_gpc()) {
	$keyword 	= stripslashes($keyword);
}
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"{$keyword}\" onkeyup=\"Check_Go_Search(event);\" />";

############################################################################################################

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>
<script language="javascript">
<!--

function checkSelect(obj,element,page){
	var alertConfirmReturn = "<?php echo $Lang['libms']['bookmanagement']['ConfirmReturnLostBook'];?>";
    if(countChecked(obj,element)==0) {
    	alert(globalAlertMsg2);
    }
    else{
        if(confirm(alertConfirmReturn)){
            obj.action=page;
            obj.method="post";
            obj.submit();
        }
    }
}
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		checkForm2();
	else
		return false;
}

function checkForm2() {
	var pass = check_date(document.getElementById("DueDateAfter"),"<?=$Lang['General']['InvalidDateFormat']?>");
	if (pass)
	 	pass = check_date(document.getElementById("DueDateBefore"),"<?=$Lang['General']['InvalidDateFormat']?>");
	if (pass)
		pass = pass & compare_date(document.getElementById("DueDateAfter"), document.getElementById("DueDateBefore"),"<?=$Lang['General']['WrongDateLogic']?>");
	if ( pass == true )	// true
	{
		document.form1.submit();
	}
}


//-->
</script>
<form name="form1" id="form1" method="POST" action="lost_book_list.php">

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
										
											<td width="40%" align="left">
												<?=$linterface->GET_DATE_PICKER("DueDateAfter",$_POST['DueDateAfter'] )?>
												<?=$linterface->GET_DATE_PICKER("DueDateBefore",$_POST['DueDateBefore'])?>
												<input type='button' value='<?=$Lang["libms"]["book"]["setting"]["filter_submit"]?>' onclick='checkForm2();' />
												
											</td>
                                       <!-- <td width="30%" align="center"><?= $linterface->GET_SYS_MSG($xmsg) ?></td> -->
											<td width="30%" align="right">
												<div class="content_top_tool">
													<div class="Conntent_search"><?=$searchTag?></div>     
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							
							<tr>
								<td height="28" valign="bottom" >
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td valign="bottom">										
												<?=$Filter_BookFormat?>
												<?=$Filter_ClassLevel?>
												<?=$Filter_DateRange?>
											</td>
											<td valign="bottom">
												<div class="common_table_tool">
													<a href="javascript:checkSelect(document.form1,'OverDueLogID[]','return_lost_book.php')" class="tool_handle"><?php echo $Lang["libms"]["CirculationManagement"]["return"];?></a>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				<tr>
					<td>
<?php if ($sys_custom['eLibraryPlus']['PayBy_ePayment']):?>					
						<span class="tabletextrequire">*</span> <?php echo $Lang['libms']['bookmanagement']['ReturnLostBookRemarkWiePayment'];?>
<?php else:?>
						<span class="tabletextrequire">*</span> <?php echo $Lang['libms']['bookmanagement']['ReturnLostBookRemarkWoePayment'];?>
<?php endif;?>						
					</td>
				</tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
			</td>
		</tr>
	</table>
</form><?php

$linterface->LAYOUT_STOP();
 
intranet_closedb();

?>
