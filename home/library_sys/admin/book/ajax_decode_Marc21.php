<?php
/* 
 * Editing by  
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();

$textarea = $_REQUEST['textarea'];
$textarea_line = explode ("\n", $textarea);
$sql1 = "select BookCategoryCode from LIBMS_BOOK_CATEGORY";		
$BookCategoryCode_list = $libms->returnVector($sql1);	

function return_code_by_descChi($DescChi){
	global $libms;
	$sql = "SELECT ResponsibilityCode FROM LIBMS_RESPONSIBILITY where DescriptionChi  = '".$DescChi."'" ;
	$result = $libms->returnArray($sql);
	return $result[0][0];
}

function get_book_category($callnum,$BookCategoryCode_list){

	$categorycode = $callnum;
	$categorycode_return = "";
	if(strlen($categorycode)>7){
		$categorycode = substr($categorycode,0,7);
	}
	while(strlen($categorycode)>3){
		if(in_array($categorycode,$BookCategoryCode_list)){
			$categorycode_return = $categorycode;
			break;
		}else{
			$categorycode = substr($categorycode,0,strlen($categorycode)-1);
		}		
	}
    if($categorycode_return==""){
    	if(in_array($categorycode,$BookCategoryCode_list)){
    		$categorycode_return = $categorycode;
    	}else{
    		$categorycode = substr($categorycode,0,2)."0";
    		if(in_array($categorycode,$BookCategoryCode_list)){
				$categorycode_return = $categorycode;
    		}else{
    			$categorycode = substr($categorycode,0,1)."00";
    			if(in_array($categorycode,$BookCategoryCode_list)){
    				$categorycode_return = $categorycode;
    			}
    		}
    	}   	
    }
	
	 return $categorycode_return;
}
	$sql = "select DescriptionChi from LIBMS_RESPONSIBILITY";
	$responsibility = $libms->returnArray($sql);
	$responsibility_count = count($responsibility);
	$responsibilityby_count_tmp = -1;
	$responsibility_tmp = "";
	$str= " ,;/:\r.+-";
	$str1=" /:\r";
	$arthor=0;
	$format=false;
	$have_subject=false;
	$ResponsibilityByArray=array();
	
	for($l=0;$l<count($textarea_line);$l++){
        if(trim($textarea_line[$l])!=""){
			preg_match('#(\d{3}) ([\d ]{2}) (.*)$#', $textarea_line[$l], $match);
			$tag = $match[1];
			$field = $match[3];
			$fieldArr = explode("|", $field);

			if($tag==""){
				$match_array = preg_split("/[\s]+/", $textarea_line[$l]);
				$BookInfoArr["ResourcesTypeCode"] = intranet_htmlspecialchars(trim($match_array[0],$str));
				$BookInfoArr["ResourcesTypeCode_Decode"] = trim($match_array[0],$str);			
				$BookInfoArr["Country"] = trim($match_array[1],$str);
				$BookInfoArr["Language"]  = intranet_htmlspecialchars(trim($match_array[2],$str));
				$BookInfoArr["Language_Decode"] = trim($match_array[2],$str);
			}
			
			
			if($tag=="020"){
				if($BookInfoArr["ISBN"]!=""){
					if($fieldArr[0]!=""){
							
						if($BookInfoArr["ISBN2"]=="")
							$BookInfoArr["ISBN2"] = trim("$fieldArr[0]",$str);
						else{
							$BookInfoArr["ISBN2"] = $BookInfoArr["ISBN2"]."; ".trim("$fieldArr[0]",$str);
						}
					}
					else{
						for($i=1;$i<=count($fieldArr);$i++){
							if($fieldArr[$i][0]=="a")
							{
								if($BookInfoArr["ISBN2"]=="")
									$BookInfoArr["ISBN2"] = trim("".substr($fieldArr[$i],1)."",$str);
								else{
									$BookInfoArr["ISBN2"] = $BookInfoArr["ISBN2"]."; ".trim("".substr($fieldArr[$i],1)."",$str);
								}
									
							}
						}
					}
				}
				else{
		
					if($fieldArr[0]!=""){
						$BookInfoArr["ISBN"] = trim("$fieldArr[0]",$str);
					}
					else{
						for($i=1;$i<=count($fieldArr);$i++){
							if($fieldArr[$i][0]=="a") $BookInfoArr["ISBN"] = trim("".substr($fieldArr[$i],1)."",$str);
								
						}
					}

				}
			}
   
			if($tag=="090"||$tag=="095"||$tag=="097"||$tag=="099"){
				if($fieldArr[0]!=""){
					$callnumArr = explode(" ",$fieldArr[0]);
					if($callnumArr[0]!="")$BookInfoArr["CallNum"] = trim($callnumArr[0]);
					if($callnumArr[1]!="")$BookInfoArr["CallNum2"] = trim($callnumArr[1]);

				}

				for($i=1;$i<=count($fieldArr);$i++){
					if($fieldArr[$i][0]=="a") $BookInfoArr["CallNum"] = trim(substr($fieldArr[$i],1));
					if($fieldArr[$i][0]=="b") $BookInfoArr["CallNum2"] = trim(substr($fieldArr[$i],1));
				}

			}

        	if($tag=="100"){
				if($fieldArr[0]!=""){
					$ResponsibilityByArray[] = trim("$fieldArr[0]",$str);
				}
				else{
					for($i=1;$i<=count($fieldArr);$i++){
						if($fieldArr[$i][0]=="a") $ResponsibilityByArray[] = trim("".substr($fieldArr[$i],1)."",$str);
					}
				}
			}
				
			if($tag=="245"){
				if($fieldArr[0]!=""){
					$BookInfoArr["BookTitle"] = trim("$fieldArr[0]",$str1);
				}
				for($i=1;$i<=count($fieldArr);$i++){
					if($fieldArr[$i][0]=="a") $BookInfoArr["BookTitle"] = trim("".substr($fieldArr[$i],1)."",$str1);
					if($fieldArr[$i][0]=="b") $BookInfoArr["BookSubtitle"] = trim("".substr($fieldArr[$i],1)."",$str1);
					if($fieldArr[$i][0]=="c") {						
						$authers = explode(";",trim("".substr($fieldArr[$i],1)."",$str));
						for($j=0;$j<count($authers);$j++){
							if($BookInfoArr["ResponsibilityBy1"]!=""&&$BookInfoArr["ResponsibilityBy2"]!=""&&$BookInfoArr["ResponsibilityBy3"]==""){
								$BookInfoArr["ResponsibilityBy3"] =trim($authers[$j],$str);
							}
							if($BookInfoArr["ResponsibilityBy1"]!=""&&$BookInfoArr["ResponsibilityBy2"]==""){
								$BookInfoArr["ResponsibilityBy2"] = trim($authers[$j],$str);
							}
							if($BookInfoArr["ResponsibilityBy1"]==""){
								$BookInfoArr["ResponsibilityBy1"] = trim($authers[$j],$str);									
							}
							/*
							$authers[$j] = trim($authers[$j],$str).";";
							for($k=0;$k<$responsibility_count;$k++){
								if(strpos($authers[$j],$responsibility[$k][0].";")){
									if($responsibilityby_count_tmp==-1||strpos($authers[$j],$responsibility[$k][0].";")<$responsibilityby_count_tmp){
										$responsibilityby_count_tmp = strpos($authers[$j],$responsibility[$k][0].";");
										$responsibility_tmp = $responsibility[$k][0];
									}								
								}
							}
							
							if($responsibilityby_count_tmp!=-1){								
								if($BookInfoArr["ResponsibilityBy1"]!=""&&$BookInfoArr["ResponsibilityBy2"]!=""&&$BookInfoArr["ResponsibilityBy3"]==""){
									$BookInfoArr["ResponsibilityBy3"] = substr($authers[$j],0,$responsibilityby_count_tmp);
									$BookInfoArr["Responsibility3"] = $responsibility_tmp;
								}
								if($BookInfoArr["ResponsibilityBy1"]!=""&&$BookInfoArr["ResponsibilityBy2"]==""){
									$BookInfoArr["ResponsibilityBy2"] = substr($authers[$j],0,$responsibilityby_count_tmp);
									$BookInfoArr["Responsibility2"] = $responsibility_tmp;
								}
								if($BookInfoArr["ResponsibilityBy1"]==""){
									$BookInfoArr["ResponsibilityBy1"] = substr($authers[$j],0,$responsibilityby_count_tmp);
									$BookInfoArr["Responsibility1"] = $responsibility_tmp;
								}
							}else{
								if($BookInfoArr["ResponsibilityBy1"]!=""&&$BookInfoArr["ResponsibilityBy2"]!=""&&$BookInfoArr["ResponsibilityBy3"]==""){
									$BookInfoArr["ResponsibilityBy3"] =trim($authers[$j],$str);
								}
								if($BookInfoArr["ResponsibilityBy1"]!=""&&$BookInfoArr["ResponsibilityBy2"]==""){
									$BookInfoArr["ResponsibilityBy2"] = trim($authers[$j],$str);
								}
								if($BookInfoArr["ResponsibilityBy1"]==""){
									$BookInfoArr["ResponsibilityBy1"] = trim($authers[$j],$str);									
								}
							}
							$responsibilityby_count_tmp = -1;
							$responsibility_tmp = "";
							*/
						}							
					}
				}
			
				$format=true;
			
			}
			
		    if($tag=="246"){
		    			    	
				if($fieldArr[0]!=""){
					$varying_form_title = trim("$fieldArr[0]",$str);
				}
				else{
					for($i=1;$i<=count($fieldArr);$i++){
						if($fieldArr[$i][0]=="a") $varying_form_title= trim("".substr($fieldArr[$i],1)."",$str);							 
					}
				}
				
				if($BookInfoArr["BookSubtitle"]==""){
		    		$BookInfoArr["BookSubtitle"] = $varying_form_title;
		    	}else{
		    		$BookInfoArr["BookSubtitle"] = $BookInfoArr["BookSubtitle"]."; ".$varying_form_title;			    		
		    	}
			}
			
			if($tag=="250"){
				if($fieldArr[0]!=""){
					$BookInfoArr["Edition"] = trim("$fieldArr[0]",$str);
				}
				else{
					for($i=1;$i<=count($fieldArr);$i++){
						if($fieldArr[$i][0]=="a") $BookInfoArr["Edition"]= trim("".substr($fieldArr[$i],1)."",$str);

					}
				}
			}

			if($tag=="260"){
				if($fieldArr[0]!=""){
					$BookInfoArr["PublishPlace"] = trim("$fieldArr[0]",$str);
				}
				for($i=1;$i<=count($fieldArr);$i++){
					if($fieldArr[$i][0]=="a") $BookInfoArr["PublishPlace"] = trim("".substr($fieldArr[$i],1)."",$str);
					if($fieldArr[$i][0]=="b") $BookInfoArr["Publisher"] = trim("".substr($fieldArr[$i],1)."",$str);
					if($fieldArr[$i][0]=="c") $BookInfoArr["PublishYear"] = trim("".substr($fieldArr[$i],1)."",$str);
				}
			}

			if($tag=="300"){
				if($fieldArr[0]!=""){
					$BookInfoArr["NoOfPage"] = trim("$fieldArr[0]",$str);
				}
				//else{
				for($i=1;$i<=count($fieldArr);$i++){
					if($fieldArr[$i][0]=="a") $BookInfoArr["NoOfPage"]= trim("".substr($fieldArr[$i],1)."",$str);
					if($fieldArr[$i][0]=="b") $BookInfoArr["ILL"] = trim("".substr($fieldArr[$i],1)."",$str);
					if($fieldArr[$i][0]=="c") $BookInfoArr["Dimension"] = trim("".substr($fieldArr[$i],1)."",$str);
					if($fieldArr[$i][0]=="e") $BookInfoArr["AccompanyMaterial"] = trim("".substr($fieldArr[$i],1)."",$str);
						
				}
				// }
			}

			if($tag=="440"||$tag=="490"){
				if($fieldArr[0]!=""){
					$BookInfoArr["Series"] = trim("$fieldArr[0]",$str);
				}
				$have_seriesNum = false;
				for($i=1;$i<=count($fieldArr);$i++){
					if($fieldArr[$i][0]=="a") $BookInfoArr["Series"] = trim("".substr($fieldArr[$i],1)."",$str);
					if($fieldArr[$i][0]=="p") {
						if($BookInfoArr["SeriesNum"]=="")
							$BookInfoArr["SeriesNum"] = trim("".substr($fieldArr[$i],1)."",$str);
						else{
							$BookInfoArr["SeriesNum"] = $BookInfoArr["SeriesNum"].trim("".substr($fieldArr[$i],1)."",$str);
						}
						$have_seriesNum = true;
					}
					if($fieldArr[$i][0]=="v") {
						if($BookInfoArr["SeriesNum"]=="")
							$BookInfoArr["SeriesNum"] = trim("".substr($fieldArr[$i],1)."",$str);
						else{
							if(substr($BookInfoArr["SeriesNum"],-2,1)==";"){
								$BookInfoArr["SeriesNum"] = $BookInfoArr["SeriesNum"].trim("".substr($fieldArr[$i],1)."",$str);
							}else{
								$BookInfoArr["SeriesNum"] = $BookInfoArr["SeriesNum"].trim("[".substr($fieldArr[$i],1)."]",$str);
							}
						}
						$have_seriesNum = true;
					}
				}
				if($have_seriesNum){
					$BookInfoArr["SeriesNum"] = $BookInfoArr["SeriesNum"]."; ";

				}
			}

			if($tag=="500"||$tag=="502"||$tag=="504"||$tag=="505"){
				if($fieldArr[0]!=""){
					if($BookInfoArr["Introduction"]=="")
						$BookInfoArr["Introduction"] = trim($fieldArr[0],$str);
					else{
						$BookInfoArr["Introduction"] = $BookInfoArr["Introduction"]."; ".trim($fieldArr[0],$str);
					}
				}
				else{
					for($i=1;$i<=count($fieldArr);$i++){
						if($fieldArr[$i][0]=="a"){
							if($BookInfoArr["Introduction"]=="")
								$BookInfoArr["Introduction"] = trim(substr($fieldArr[$i],1),$str);
							else{
								$BookInfoArr["Introduction"] = $BookInfoArr["Introduction"]."; ". trim(substr($fieldArr[$i],1),$str);
							}
						}
					}
				}
			}

			if($tag=="650"){
				if($have_subject){
					if($fieldArr[0]!=""){
						$BookInfoArr["Tags"].= ",".trim($fieldArr[0],$str);
					}
					else{
						for($i=1;$i<=count($fieldArr);$i++){
							if($fieldArr[$i][0]=="a") {
								$BookInfoArr["Tags"].= ",".trim(substr($fieldArr[$i],1),$str);
							}
						}
					}
				}
				else{
					if($fieldArr[0]!=""){
						$BookInfoArr["Subject"]= trim($fieldArr[0],$str);
						$BookInfoArr["Tags"].= trim($fieldArr[0],$str);
					}
					else{
						for($i=1;$i<=count($fieldArr);$i++){
							if($fieldArr[$i][0]=="a") {
								$BookInfoArr["Subject"]= trim(substr($fieldArr[$i],1),$str);
								$BookInfoArr["Tags"].= trim(substr($fieldArr[$i],1),$str);
							}
						}
					}
					$have_subject=true;
				}
			}

			if($tag=="690"){
				if($have_subject){
					if($fieldArr[0]!=""){
						$BookInfoArr["Tags"].= ",".trim($fieldArr[0],$str);
					}
					else{
						for($i=1;$i<=count($fieldArr);$i++){
							if($fieldArr[$i][0]=="a") {
								$BookInfoArr["Tags"].= ",".trim(substr($fieldArr[$i],1),$str);
							}
						}
					}
				}
				else{
					if($fieldArr[0]!=""){
						$BookInfoArr["Subject"] = trim($fieldArr[0],$str);
						$BookInfoArr["Tags"].= trim($fieldArr[0],$str);
					}
					else{
						for($i=1;$i<=count($fieldArr);$i++){
							if($fieldArr[$i][0]=="a") {
								$BookInfoArr["Subject"]= trim(substr($fieldArr[$i],1),$str);
								$BookInfoArr["Tags"].= trim(substr($fieldArr[$i],1),$str);
							}
						}
					}
					$have_subject=true;
				}
			}
			
			if($tag=="700"){
				if($fieldArr[0]!=""){				
				   $ResponsibilityByArray[] = trim("$fieldArr[0]",$str);					
				}					
				else{					
					for($i=1;$i<=count($fieldArr);$i++){						
	    				if($fieldArr[$i][0]=="a") $ResponsibilityByArray[] = trim("".substr($fieldArr[$i],1)."",$str);						
					}					
				}						
			} 
			
			
			
	      }
		}
		
		
		
		
		if($format==true){
			$BookInfoArr["SeriesNum"] = rtrim($BookInfoArr["SeriesNum"],$str);
			if($BookInfoArr["Tags"]!=""){
				$Middle_tag = explode(",",$BookInfoArr["Tags"]);
				$Middle_tag = array_unique($Middle_tag);
				$BookInfoArr["Tags"] = implode(",",$Middle_tag);
			}
			
			for($i=0;$i<count($ResponsibilityByArray);$i++){
	     	$duplicate=false;
	     	if($BookInfoArr["ResponsibilityBy1"]!=""&&strstr($BookInfoArr["ResponsibilityBy1"],$ResponsibilityByArray[$i])!==false)
	     		$duplicate = true;
	     	if($BookInfoArr["ResponsibilityBy2"]!=""&&strstr($BookInfoArr["ResponsibilityBy2"],$ResponsibilityByArray[$i])!==false)
	        	$duplicate = true;
	        if($BookInfoArr["ResponsibilityBy3"]!=""&&strstr($BookInfoArr["ResponsibilityBy3"],$ResponsibilityByArray[$i])!==false)
	        	$duplicate = true;
	       	
	        if(!$duplicate) $new_responsibilityByArray[] = $ResponsibilityByArray[$i];
	     }	     

	     if($BookInfoArr["ResponsibilityBy1"]==""){
             $BookInfoArr["ResponsibilityBy1"] = $new_responsibilityByArray[0];
             $BookInfoArr["ResponsibilityBy2"] = $new_responsibilityByArray[1];
             $BookInfoArr["ResponsibilityBy3"] = $new_responsibilityByArray[2];
	     }else if($BookInfoArr["ResponsibilityBy2"]==""){
	     	 $BookInfoArr["ResponsibilityBy2"] = $new_responsibilityByArray[0];
             $BookInfoArr["ResponsibilityBy3"] = $new_responsibilityByArray[1];
	     }else if($BookInfoArr["ResponsibilityBy3"]==""){
             $BookInfoArr["ResponsibilityBy3"] = $new_responsibilityByArray[0];     	
	     }
         unset($new_responsibilityByArray);
	     //handle responsibility								
		 if($BookInfoArr["ResponsibilityBy1"]!=""&&$responsibility_count!=0){
		 	$authers = trim($BookInfoArr["ResponsibilityBy1"],$str).";";
		 	for($k=0;$k<$responsibility_count;$k++){
				if(strpos($authers,$responsibility[$k][0].";")){
					if($responsibilityby_count_tmp==-1||strpos($authers,$responsibility[$k][0].";")<$responsibilityby_count_tmp){
						$responsibilityby_count_tmp = strpos($authers,$responsibility[$k][0].";");
						$responsibility_tmp = $responsibility[$k][0];
					}								
			     }			 
		    }
		    if($responsibilityby_count_tmp!=-1){
		    	$BookInfoArr["ResponsibilityBy1"] = substr($authers,0,$responsibilityby_count_tmp);
				$BookInfoArr["Responsibility1"] = $responsibility_tmp;		    	
		    }			 	
		    $responsibilityby_count_tmp = -1;
		    $responsibility_tmp = "";	
		 }
		 if($BookInfoArr["ResponsibilityBy2"]!=""&&$responsibility_count!=0){
		 	$authers = trim($BookInfoArr["ResponsibilityBy2"],$str).";";
		 	for($k=0;$k<$responsibility_count;$k++){
				if(strpos($authers,$responsibility[$k][0].";")){
					if($responsibilityby_count_tmp==-1||strpos($authers,$responsibility[$k][0].";")<$responsibilityby_count_tmp){
						$responsibilityby_count_tmp = strpos($authers,$responsibility[$k][0].";");
						$responsibility_tmp = $responsibility[$k][0];
					}								
			     }			 
		    }
		    if($responsibilityby_count_tmp!=-1){
		    	$BookInfoArr["ResponsibilityBy2"] = substr($authers,0,$responsibilityby_count_tmp);
				$BookInfoArr["Responsibility2"] = $responsibility_tmp;		    	
		    }			 	
		    $responsibilityby_count_tmp = -1;
		    $responsibility_tmp = "";	
		 }
		 if($BookInfoArr["ResponsibilityBy3"]!=""&&$responsibility_count!=0){
		 	$authers = trim($BookInfoArr["ResponsibilityBy3"],$str).";";
		 	for($k=0;$k<$responsibility_count;$k++){
				if(strpos($authers,$responsibility[$k][0].";")){
					if($responsibilityby_count_tmp==-1||strpos($authers,$responsibility[$k][0].";")<$responsibilityby_count_tmp){
						$responsibilityby_count_tmp = strpos($authers,$responsibility[$k][0].";");
						$responsibility_tmp = $responsibility[$k][0];
					}								
			     }			 
		    }
		    if($responsibilityby_count_tmp!=-1){
		    	$BookInfoArr["ResponsibilityBy3"] = substr($authers,0,$responsibilityby_count_tmp);
				$BookInfoArr["Responsibility3"] = $responsibility_tmp;		    	
		    }			 	
		    $responsibilityby_count_tmp = -1;
		    $responsibility_tmp = "";	
		 }
		 
		}
	
		if($BookInfoArr["Responsibility1"]!=""){
			$BookInfoArr["Responsibility1"] = return_code_by_descChi($BookInfoArr["Responsibility1"]);				
		}
		if($BookInfoArr["Responsibility2"]!=""){
			$BookInfoArr["Responsibility2"] = return_code_by_descChi($BookInfoArr["Responsibility2"]);				
		}
		if($BookInfoArr["Responsibility3"]!=""){
			$BookInfoArr["Responsibility3"] = return_code_by_descChi($BookInfoArr["Responsibility3"]);				
		}
		if($BookInfoArr["CallNum"]!=""){			
			$BookInfoArr["BookCategoryCode"]=get_book_category($BookInfoArr["CallNum"],$BookCategoryCode_list);
		}
		echo json_encode($BookInfoArr);
?>