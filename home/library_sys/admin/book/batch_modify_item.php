<?php

//modifying:   
/*
 * 	@Purpose:	Batch edit of specific fields in Book Items
 *  
 * 	Log
 * 	Date:	2017-01-27 [Cameron] add checking empty for ByACNOList (case #Z110645) 
 * 	Date:	2015-07-09 [Cameron] default sort by ACNO (case 2015-0304-0908-31071)
 * 	Date:	2015-07-07 [Cameron] support column sorting when it's clicked
 * 	Date:	2015-07-06 [Cameron] Add radio option "By BarCode"
 * 	Date:	2014-12-19 [Cameron] JSON.parse support IE8 or above, therefore, use jQuery.parseJSON instead
 * 	Date:	2014-12-16 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."home/library_sys/admin/book/batch_modify_manage.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageMgmtBatchEdit";

$libms->MODULE_AUTHENTICATION($CurrentPage);

$TAGS_OBJ = $libms->get_batch_edit_tabs("ITEMDETAILS");

$linterface = new interface_html();
$bmm = new batch_modify_manage();

$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 1 : $field;	// default sort by ACNO
$bmm->setfield($field);
$bmm->setorder($order);

//# date range
//$FromDate=$FromDate==""?date('Y-m-d'):$FromDate;
//$ToDate=$ToDate==""?date('Y-m-d'):$ToDate;


//# Option ..........
$BookCirculationArray = $libms->BOOK_GETOPTION('LIBMS_CIRCULATION_TYPE', 'CirculationTypeCode', $Lang['libms']['sql_field']['Description'], 'CirculationTypeCode', '');
$BookCirculationOption = $linterface->GET_SELECTION_BOX($BookCirculationArray, " name='BookCirculation' id='BookCirculation' class='selection' ", $Lang["libms"]["value_follows_parent"], $CirculationTypeCode);

## Selection - Book Item Status, exclude WRITEOFF
$ParTags = ' id="ItemStatus" name="ItemStatus" class="edit_nonEmpty" ';
$ItemStatusSelection = $libms->GET_BOOK_ITEM_SELECTION($ParTags, '', $ItemStatus, $FristIndexText=$Lang["libms"]["batch_edit"]["PleaseSelect"], 'WRITEOFF');
//if($ItemStatus == 'WRITEOFF')
//	$ItemStatusSelection = $Lang["libms"]["book_status"]["WRITEOFF"].'<div style="display:none">'.$libms->GET_BOOK_ITEM_SELECTION($ParTags, '', $ItemStatus, '').'</div>';
	
## Selection - Item Location
$ItemLocationArray = $libms->BOOK_GETOPTION('LIBMS_LOCATION', 'LocationCode', $Lang['libms']['sql_field']['Description'], 'LocationCode', '');
$ItemLocationOption = $linterface->GET_SELECTION_BOX($ItemLocationArray, " name='LocationCode' id='LocationCode' ", $Lang['libms']['status']['na'], $LocationCode);

############################################################################################################

# Top menu highlight setting
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();



?>

<style>
<?php
	echo $bmm->get_batch_edit_style();
?>
</style>

<?php
	echo $bmm->get_batch_edit_js();
?>
<script language="javascript">
<!--

function ajax_by_barcode_handler(ajaxReturn){
	if (ajaxReturn.success){
		table = $('#book_item_table');
		if ($('tr#no_record_row').length > 0){
			$('#book_item_table tr:last').remove();
		}
		$row = $(ajaxReturn.html);
		table.append($row);
		sort_batch_edit_page(1,1);	// default sort by ACNO asc
		$('#left_list_panel').attr('style','display:block');
		$('#right_change_panel').css('visibility','visible');
	}else{
		alert('<?=$Lang["libms"]["batch_edit"]["msg"]["no_barcode"]?>');
	}
}

function sort_batch_edit_page(order,field) {
	var bookItemID = '';
	$('input[name="BookItemID\[\]"]').each(function(){
		if ($(this).val() != '') {
			bookItemID += $(this).val() + ',';	
		}
	});
	if (bookItemID != '') {
		bookItemID = bookItemID.substr(0,bookItemID.length-1);
		$.ajax({								
			url : "ajax_get_bookitem_list.php",
			async: false,
			timeout:1000,							
			type : "POST",							
			data : {
				'task': 'sorting',
				'order': order,
				'field': field,
				'bookItemID': bookItemID			
			},							
			success : function(json) {
				show_left_panel_result(json);							
			}							
		});								
	}	
}

$(document).ready(function(){	
<?php
	echo $bmm->get_batch_edit_doc_ready_js();
?>

	$('#add_book').click( function(){
		$('#Barcode').val($('#Barcode').val().toUpperCase().replace(" ",''));
		
		var codemod = ($('#Barcode').val()).replace(/[!"#$%&\'()*+,.\/:;<=>?@[\]^`{|}~]/g,'-');
		if (codemod!=''){
			if ($('tr#'+codemod).length ==0){
				$.getJSON(
						'ajax_get_bookitem_by_barcode.php',
						{ 
							barcode : $('#Barcode').val(),
							ID : (($('tr#no_record_row').length > 0) ? 1: $('#book_item_table tr').length)
						},
						'JSON'
				)
				.success(ajax_by_barcode_handler)
				.error(function(){ alert('Error on AJAX call!'); });
				
			}else{
				alert("<?=$Lang["libms"]["batch_edit"]["msg"]["scanned"]?>");
				
			}
		}
		$('#Barcode').val('');		
		$('#Barcode').focus();
		return false;
	});
	
	$('#search').click(function(){
		if (($('#book_item_table tr').length > 1 ) && ($('tr#no_record_row').length <= 0) ) {
			var confirm = window.confirm("<?=$Lang["libms"]["batch_edit"]["ConfirmClearListForSearch"]?>");
		}
		else {
			confirm = true;
		}
		if (confirm) {
			clear_item_list();
			
	 		var ret = true;
			var rs=new Array();
			if ($("#ByACNO").is(':checked')) {
				ret = ret & isEmptyField('search_nonEmpty');
			}
			else if ($("#ByACNOList").is(':checked')) {
				ret = ret & isEmptyField('search_nonEmpty');
			}
			else if ($("#ByAccountDate").is(':checked')) {
				ret = ret & isDateField('search_date');
			}
			if (ret) {
				$.ajax({								
					url : "ajax_get_bookitem_list.php",
					async: false,
					timeout:1000,							
					type : "POST",							
					data : $('#form1').serialize(),							
					success : function(json) {
						show_left_panel_result(json);							
					}							
				});								
			}
		}
	});
	
	$("input:button[name^='Apply_']").each(function(){
		$(this).click(function(){
			if($('input[name="BookItemID\[\]"]:checked').length == 0) {
				alert("<?=$Lang["libms"]["batch_edit"]["SelectAtLeastOneItem"]?>");	
			}
			else {
				var str = this.id.split('Apply_');
				var field = str[1];
				var pass = true;
				 
				if ($("#"+field).hasClass('edit_date')) {
					pass = pass & isDateField('edit_date');
				}
//				if ($("#"+field).hasClass('numberField')) {
//					pass = pass & isNumberField();
//				}
				if ($("#"+field).hasClass('edit_nonEmpty')) {
					pass = pass & isEmptyField('edit_nonEmpty');
				}	
	
				if (pass) {			
					var confirm = window.confirm("<?=$Lang["libms"]["batch_edit"]["ConfirmUpdate"]?>" + $(this).parent().parent().parent().children(':first-child').text());
					if (confirm) {
						$('#UpdateField').val(field);
						$.ajax({								
							url : "ajax_batch_update_bookitems.php",
							async: false,
							timeout:1000,							
							type : "POST",							
							data: $('#form1').serialize(),		
							success : function(json) {							
								rs = jQuery.parseJSON(json);
								if ( rs['result'] != undefined ) {
									if ( rs['result'] == "0" ) {
										Get_Return_Message("<?=$Lang["libms"]["batch_edit"]["ReturnMessage"]["UpdateUnsuccess"]?>");
									}
									else {
										Get_Return_Message("1|=|" + rs['nrAffected'] + "<?=$Lang["libms"]["batch_edit"]["ReturnMessage"]["UpdateSuccess"]?>");
									}
								}											
							}							
						});								
					}
					else {
						// do nothing			
					}
				}
				else {	// not pass
					// do nothing
				}
			}
		});
	});
	
});

//-->
</script>

<form name="form1" id="form1" method="post" action="batch_modify_item.php">

	<!-- split into left and right, then break left part into top and bottom, like this: -| -->
	<table class="be_table">
		<tr>
			<td width="49%" valign="top">
				<table class="be_table">
					<tr>
						<td>
	
							<!-- ###### Top Left Panel ##### -->
							<span id="left_search_panel" class="table_board panel">
					            <span class="form_sub_title_v30">
					            	<em>- <span class="field_title"><?= $Lang["libms"]["batch_edit"]["PanelTitle"]["ItemSearch"] ?></span> -</em>
					                <p class="spacer"></p>
					            </span>
					            
					            <span>
<?php	
	echo $bmm->get_batch_modify_search_criteria();
?>		
								</span>
							</span>
						</td>
					</tr>

					<tr>
						<td>		
							<!-- ###### Bottom Left Panel ##### -->
							<span id="left_list_panel" class="table_board" style="display:none">
								<div class="form_sub_title_v30">
							    	<em>- <span class="field_title"><?=$Lang["libms"]["batch_edit"]["PanelTitle"]["SelectedRecord"]?></span> -</em>
							       	<p class="spacer"></p>
						    	</div>
								<div id="left_list_panel_content">
									<table id="book_item_table" class="common_table_list_v30 view_table_list_v30" style="width:98%"> 
										<tr>
											<th width="1">#</th>
<?php											
$pos = 0;
?>						
											<th width="75%"><?=$bmm->column($pos++, $Lang["libms"]["book"]["title"])?></th>
											<th width="20%"><?=$bmm->column($pos++, $Lang["libms"]["book"]["code"])?></th>				
											<th class="num_check" width="1"><input type="checkbox" id="checkall" name="checkall" onclick="Set_Checkbox_Value('BookItemID[]',this.checked)" checked/></th>
										</tr>
									</table>
								</div>
							</span>
						</td>
					</tr>
				</table>		
			</td>

			<td width="49%" valign="top">
				<table class="be_table">
					<tr>
						<td>

							<!-- ###### Right Panel ##### -->
							<span id="right_change_panel" class="table_board">
					            <span class="form_sub_title_v30">
					            	<em>- <span class="field_title"><?= $Lang["libms"]["batch_edit"]["PanelTitle"]["ChangeRecord"] ?></span> -</em>
					                <p class="spacer"></p>
					            </span>
					            
					            <span>
									<table class="form_table_v30">
										<tbody>
						            		<tr>
						                    	<td class="field_title_short nowrap"><span class="field_title"><?= $Lang['libms']['book']['book_status'] ?></span></td>
						                    	<td><?=$ItemStatusSelection?></td>
						                    	<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_ItemStatus") ?></div></td>
						            		</tr>
						
						            		<tr>
						                    	<td class="field_title_short nowrap"><span class="field_title"><?= $Lang['libms']['settings']['circulation_type'] ?></span></td>
						                     	<td><?=$BookCirculationOption?></td>
												<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_BookCirculation") ?></div></td>
						            		</tr>
						            		
						            		<tr>            		
						                    	<td class="field_title_short nowrap"><span class="field_title"><?= $Lang['libms']['settings']['book_location'] ?></span></td>
						                     	<td><?=$ItemLocationOption?></td>
						                     	<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_ItemLocation") ?></div></td>
						            		</tr>
						            		
						            		<tr>
						                    	<td class="field_title_short nowrap"><span class="field_title"><?= $Lang['libms']['book']['account_date'] ?></span></td>
						                    	<td><?=$linterface->GET_DATE_PICKER("AccountDate", date('Y-m-d'),$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum edit_date optional")?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
												<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_AccountDate") ?></div></td>                    	
						            		</tr>
						
						            		<tr>
						                    	<td class="field_title_short nowrap"><span class="field_title"><?= $Lang['libms']['book']['purchase_date'] ?></span></td>
						                    	<td><?=$linterface->GET_DATE_PICKER("PurchaseDate", date('Y-m-d'),$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum edit_date optional")?> <span class="tabletextremark">(YYYY-MM-DD)</span></td>
												<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_PurchaseDate") ?></div></td>                    	
						            		</tr>
						            		
						            		<tr>
						                    	<td class="field_title_short nowrap"><span class="field_title"><?= $Lang['libms']['book']['purchase_price'] ?></span></td>
						                    	<td><input id="PurchasePrice" name="PurchasePrice" type="text" class="textboxnum numberField" maxlength="64" value=""/></td>
												<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_PurchasePrice") ?></div></td>
						            		</tr>
						            		            		            		
						            		<tr>
						                    	<td class="field_title_short nowrap"><span class="field_title"><?= $Lang['libms']['book']['distributor'] ?></span></td>
						                    	<td><input id="Distributor" name="Distributor" type="text" class="textboxtext inputselect" maxlength="64" value=""/></td>
												<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_Distributor") ?></div></td>
						            		</tr>
						
						            		<tr>
						                    	<td class="field_title_short"><span class="field_title"><?= $Lang['libms']['book']['purchase_by_department'] ?></span></td>
						                    	<td><input id="Purchasedept" name="Purchasedept" type="text" class="textboxtext inputselect" maxlength="32" value=""/></td>
												<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_Purchasedept") ?></div></td>
						            		</tr>
						            		            		            		
						            		<tr>
						                        <td class="field_title_short nowrap"><span class="field_title"><?= $Lang['libms']['book']['purchase_note'] ?></span></td>
						                        <td><input id="PurchaseNote" name="PurchaseNote" type="text" class="textboxtext inputbox" maxlength="64" value=""/></td>
												<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_PurchaseNote") ?></div></td>
						            		</tr>
						
						            		<tr>
						                        <td class="field_title_short"><span class="field_title"><?=$Lang['libms']['book']['item_remark_to_user']?></span></td>
						                        <td><input id="RemarkToUser" name="RemarkToUser" type="text" class="textboxtext inputbox" maxlength="255" value=""/></td>
												<td><div class="applyBtn"><?= $linterface->GET_ACTION_BTN($Lang["libms"]["batch_edit"]["Apply"], "button", $ParOnClick="",$ParName="Apply_RemarkToUser") ?></div></td>
						            		</tr>            		            		
						            	</tbody>
									</table>
								</span>
							</span>
						</td>
					</tr>
				</table>
				
			</td>
		<tr>		
	</table>		

	<!-- for pass to batch update -->
	<input id="UpdateField" name="UpdateField" type="hidden" value="">
	
</form>

<?php


$linterface->LAYOUT_STOP();

 
intranet_closedb();


?>
