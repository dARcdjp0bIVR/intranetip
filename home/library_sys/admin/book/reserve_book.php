<?php

# being modified by: 
/*
 * Change log:
 *
 * 2019-03-28 Cameron
 *      show BookSubTitle if it's not empty
 *      
 * 2018-09-05 Cameron
 *      use function getReserveBookTabs() to assign $TAGS_OBJ
 *      
 * 2017-06-06 Cameron
 * 		place set cookie function after include global.php to support php5.4 [case #E117788]
 * 
 * 2017-05-15 Cameron
 *		replace htmlspecialchars by intranet_htmlspecialchars
 *
 * 2017-04-06 Cameron
 * 		handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 			by stripslashes($keyword)
 * 
 * 2016-02-25 Cameron
 * 		modify sql, set CallNum/CallNum2 to empty if it's null 
 * 
 * 2015-01-06 Henry
 * 		Fix the number of reservation reader
 * 
 * 2015-01-05 Cameron
 * 		Fix search condition, should join LIBMS_BOOK_UNIQUE because ACNO and BarCode check against this table
 * 
 * 2014-12-10 Cameron
 * 		change request field "action" to hidden field "task"
 * 
 * 2014-11-21 Cameron
 * 		add export and print function for current page, with breakdown details for the same book		
 * 
 * 2013-11-08 Henry
 * 		add tab to navigate to expired book reservation
 * 
 * 201308-12 Yuen
 * 		fixed the search function by correcting the action path of the form 
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
	function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
	$gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
	array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/reports/Exporter.php");
include_once($PATH_WRT_ROOT."home/library_sys/admin/book/reserve_book_manage.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

### set cookies
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	//$order = $ck_right_page_order;
	$order = 1;		# default in ascending order
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

//global $intranet_db;

intranet_auth();
intranet_opendb();

# in all boook management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageBookManagementReserveList";

// $TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_reserve'],'#', true);
// $TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['expired_book_reserve'],'expired_reserve_book.php', false);
$TAGS_OBJ = $libms->getReserveBookTabs('BOOKRESERVE');
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$libms->MODULE_AUTHENTICATION($CurrentPage);


//$linterface = new interface_html("libms.html");
$linterface = new interface_html();
$rbm = new reserve_book_manage();

############################################################################################################
///*
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == '') ? 1 : $order;
$field = ($field == "") ? 0 : $field;

$conds='';
$conds .= $rbm->getKeywordFilter();

$BookID_For_CONCAT = (isset($junior_mck)) ? "CONVERT(b.`BookID` USING utf8)" : "b.`BookID`";
$sql = <<<EOL
SELECT  TRIM(CONCAT(IFNULL(b.`CallNum`,''),' ', IFNULL(b.`CallNum2`,''))) AS CallNumber ,
		CONCAT(  '<a href=\'reserve_book_more.php?Code= ', {$BookID_For_CONCAT} ,  ' \' >', `BookTitle`, IF(BookSubTitle IS NULL OR BookSubTitle='','',CONCAT('-',BookSubTitle)),  '</a>' ) AS BookTitle,
		b.`ResponsibilityBy1` , b.`Publisher` , b.`Edition` , COUNT(distinct rl.ReservationID) AS sum,
		b.BookTitle as BookTitleForSort
FROM  `LIBMS_RESERVATION_LOG` rl
JOIN  `LIBMS_BOOK` b ON b.`BookID` = rl.`BookID` 	
JOIN    LIBMS_USER u ON u.UserID = rl.UserID
LEFT JOIN    LIBMS_BOOK_UNIQUE bu ON bu.BookID = b.BookID
WHERE (
rl.`RecordStatus` LIKE  'WAITING'
OR rl.`RecordStatus` LIKE  'READY'
)		
{$conds}

GROUP BY rl.`BookID` 

EOL;

if (( $_POST['task'] == 'print') || ( $_POST['task'] == 'export')) {	
	if ( $_POST['task'] == 'print'){
		$viewdata = $rbm->getCurrentReservation($withStyle=true);
		echo $rbm->getPrint($viewdata);
		exit();
	}elseif ( $_POST['task'] == 'export'){
		$viewdata = $rbm->getCurrentReservation($withStyle=false);
		Exporter::string_to_file_download('ReserveBook.csv', $rbm->getCSV($viewdata));
		exit();
	}
}


$li = new libdbtable2007($field, $order, $pageNo);

global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";

$li->field_array = array( "CallNumber", "BookTitleForSort", "ResponsibilityBy1", "Publisher", "Edition", "sum");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
//$li->column_list .= "<th width='14%' >".$li->column($pos++, $Lang['libms']['book']['code'])."</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['libms']['book']['call_number'])."</td>\n";
$li->column_list .= "<th width='30%' >".$li->column($pos++, $Lang['libms']['book']['title'])."</td>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['libms']['book']['responsibility_by1'])."</td>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['libms']['book']['publisher'])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['edition'])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['book']['reserve_sum'])."</td>\n";
//$li->column_list .= "<th width='1'>".$li->check("Code[]")."</td>\n";

$keyword = intranet_htmlspecialchars($_POST['keyword']);
if (!get_magic_quotes_gpc()) {
	$keyword 	= stripslashes($keyword);
}
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"{$keyword}\" onkeyup=\"Check_Go_Search(event);\" />";
//*/
############################################################################################################

# Top menu highlight setting
$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function checkRemove(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}

function checkForm() {
	return;
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) {	// enter 
		document.form1.task.value="";
		document.form1.submit();
	}
	else {
		return false;
	}
}

function click_export()
{
	document.form1.task.value="export";
	document.form1.action = "reserve_book.php";
	document.form1.submit();	
	document.form1.task.value="";
}

function click_print()
{
	var url = "reserve_book.php";
	document.form1.task.value="print";
	document.form1.action=url;
	document.form1.target="_blank";
	document.form1.submit();
	document.form1.task.value="";
	document.form1.target="_self";
}

//-->
</script>
<!--  <form name="form1" method="post" action=""> -->
<form name="form1" id="form1" method="POST" action="reserve_book.php" onSubmit="return checkForm()">

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="40%">
												<?=getToolbar()?>
											</td>
											<td width="30%" align="center"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
											<td width="30%" align="right">
												<div class="content_top_tool">
													<div class="Conntent_search"><?=$searchTag?></div>     
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
				<input type="hidden" name="task" value="<?=$_POST['task']?>" />
			</td>
		</tr>
	</table>
</form>

<?php
//dump($sql);
$linterface->LAYOUT_STOP();
 
 
 
intranet_closedb();





?>
