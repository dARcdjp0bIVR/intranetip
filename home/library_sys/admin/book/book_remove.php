<?php
/*
 * 	Using: 
 * 
 * 	Log
 * 	2016-04-14 [Cameron]
 * 		- check if selected book is being loaned, don't allow to process if yes. case #S93992
 * 		- pass success / fail msg after delete record 	
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
/*
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}
//*/

if(sizeof($Code)==0) {
	header("Location: index.php");	
}

## check if book is being loaned, don't allow to process if yes
$loanBook = $libms->BookInLoanByBookID($Code);
$nrLoanBook = count($loanBook);
if ($nrLoanBook > 0) {
	$LoanBook_List = array();
	for($i=0;$i<$nrLoanBook;$i++) {
		$LoanBook_List[] = $loanBook[$i]['BookTitle'];
	}
	$xmsg = implode(",",$LoanBook_List);
	$xmsg = sprintf($Lang['General']['ReturnMessage']['CannotDeleteOnLoanBook'],$xmsg);
	if ($IsPeriodical) {
		header("Location: ../periodical/index.php?DirectMsg=T&returnMsgKey=".$xmsg);
	}
	else {
		header("Location: index.php?DirectMsg=T&xmsg=".$xmsg);
	}
	exit;
}			

/*
$sql = "SELECT BookID FROM LIBMS_BOOK ";
$Code = $libms->returnVector($sql);
*/
$result = array();
$result[] = $libms->REMOVE_BOOK($Code);

$libel = new elibrary();
for ($i=0; $i<sizeof($Code); $i++)
{
	$book_id_now = $Code[$i];
	$result[] = $libel->REMOVE_PHYSICAL_BOOK($book_id_now);
}
$xmsg = in_array(false,$result) ? 'DeleteUnsuccess': 'DeleteSuccess'; 

intranet_closedb();

if ($IsPeriodical) {
	header("Location: ../periodical/index.php?returnMsgKey=".$xmsg);
}
else {
	header("Location: index.php?xmsg=".$xmsg);
}

?>