<?php
// Using: Cameron

############ Change Log Start ###############
/*
 *  20170209 (Cameron) 
 * 		- strip slashes after call lib.php for php5.4+
 * 
 * 	20150206 (Cameron):
 *		1. fix bug for row record shifted if leave some rows unchecked
 *		2. fix bug to support special characters in Reasons field
 *		3. combine add_write_off_record_by_barcode to this file
 *		4. add commit / rollback transaction for process in eLib+, but not apply to SYNC_PHYSICAL_BOOK_TO_ELIB
 *			as this procedure is in another connection object
 * 
 *	20140929 (Henry) : added to call function SYNC_PHYSICAL_BOOK_TO_ELIB() (ip.2.5.5.10.1)  
 */
############ Change Log End ###############

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$linterface = new interface_html();
$libel = new elibrary();

//****** start special handle for php5.4+
if (function_exists('get_magic_quotes_gpc') && !get_magic_quotes_gpc()) {

	// 1. strip slashes again	
	if (!function_exists('magicQuotes_stripslashes2')) {
		function magicQuotes_stripslashes2(&$value, $key) {
			$value = stripslashes($value);
		}
	}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, 'magicQuotes_stripslashes2');
}
//****** end special handle for php5.4+

$stocktakeSchemeID = $_POST['StocktakeSchemeID'];
$stocktakeStatus = $_POST['selectedStocktakeStatus'];
$row = $_POST['Row'];

$nrRow = count($row);
$final_result = array();
foreach($row as $k=>$v) {
	$bookIDUniqueID = $v['BookIDUniqueID'];
	if ($bookIDUniqueID) {
		$idInfo = explode('-', $bookIDUniqueID);
		$nrIdInfo = count($idInfo);
		if ($nrIdInfo > 1) {
			$bookID 		= $idInfo[0];
			$bookUniqueID 	= $idInfo[1];
			$stocktakeLogID = ($nrIdInfo > 2) ? $idInfo[2] : '';
			
			$writeoffDate = $v['WriteoffDate'];
			$writeoffReasons = $libms->Get_Safe_Sql_Query(htmlentities($v['WriteoffReasons'],ENT_QUOTES,"UTF-8"));
			
			if ($bookID && $bookUniqueID) {			
				## Get Book Status
				$bookUniqueInfo = $libms->GET_UNIQUE_BOOK_STATUS($bookUniqueID,$bookID);
				$bookUniqueStatus = $bookUniqueInfo[0]['RecordStatus'];
		
				$libms->Start_Trans();
				$result = array();
				$result['WriteOffSuccess'] = $libms->ADD_WRITEOFF_LOG_RECORD($stocktakeSchemeID, $bookID, $bookUniqueID, 'WRITEOFF',$bookUniqueStatus,$writeoffReasons,$writeoffDate);
				$result['UpdateUniqueBookSuccess'] = $libms->UPDATE_UNIQUE_BOOK($bookUniqueID,'WRITEOFF');
				$result['UpdateBookCopyInfo'] = $libms->UPDATE_BOOK_COPY_INFO($bookID);
			
				if ($stocktakeLogID) {
					$result['RemoveStocktakeLogSuccess'] = $libms->DELETE_STOCKTAKE_LOG_RECORD($stocktakeLogID);
				}
				
				if (!in_array(false,$result)) {
					$libms->Commit_Trans();
									
					//syn the front end of the book info
					$libel->SYNC_PHYSICAL_BOOK_TO_ELIB($bookID);
					$final_result[] = true;
				}
				else {
					$libms->RollBack_Trans();
					$final_result[] = false;
				}			
			}	// $bookID && $bookUniqueID
		}	// $nrIdInfo > 1
	}	// $bookIDUniqueID not empty
}	// foreach

if (!in_array(false,$final_result)) {
	$msg = "add";	// successful
}
else {
	$msg = "add_failed";
}

$stocktakeStatus = $stocktakeStatus ? $stocktakeStatus : 'WRITEOFF';
header("Location: write_off.php?xmsg=$msg&StocktakeSchemeID=$stocktakeSchemeID&selectedStocktakeStatus=$stocktakeStatus");

?>