<?php
// Using: 
/*
 * 	Log
 * 	Date:	2015-02-03 [Cameron] create this file
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$linterface = new interface_html();
$barcode = rawurldecode($_POST['Barcode']);
$stocktakeSchemeID = $_POST['StocktakeSchemeID'];
$RowID = $_POST['RowID'];

$rs = $libms->GET_BOOK_INFO_WITH_STOCKTAKE_WRITEOFF($barcode,$stocktakeSchemeID);

if($rs){
	$r_UniqueBookID 	= $rs[0]['UniqueID'];
	$r_BookID 			= $rs[0]['BookID'];
	$r_ACNO 			= $rs[0]['ACNO'];	
	$r_BookTitle 		= $rs[0]['BookTitle']?$rs[0]['BookTitle']:$Lang['General']['EmptySymbol'];
	$r_Location 		= $rs[0]['LocationName']?$rs[0]['LocationName']:$Lang['General']['EmptySymbol'];
	$r_StocktakeLogID	= $rs[0]['StocktakeLogID'];
	$r_WriteOffID		= $rs[0]['WriteOffID'];

	## Check If Has Write Off Log Already
	if($r_WriteOffID!=''){
		echo "writeoffbook";
		exit(); // not allow to do write-off if it's been written-off to stock-take
	}
	
	#	ACNO, Barcode, BookTitle,Location,WriteOffDay, Reason, Checkbox
	## Return Table
	
	$x = '';
	$x .= '<tr class="returned">';
	$x .= '<td width="1%">'.$RowID.'</td>';		
	$x .= '<td width="15%">'.$r_ACNO.'</td>';
	$x .= '<td width="15%">'.$barcode.'</td>';
	$x .= '<td width="24%">'.$r_BookTitle.'</td>';
	$x .= '<td width="15%">'.$r_Location.'</td>';
	$x .= '<td width="14%">'.$linterface->GET_DATE_PICKER("Row[".$RowID."][WriteoffDate]", date("Y-m-d"),"","yy-mm-dd","","","","",$ID="WriteoffDate_$RowID").'</td>';
	$x .= '<td width="15%"><input type="text" id="Row['.$RowID.'][WriteoffReasons]" name="Row['.$RowID.'][WriteoffReasons]" value="" maxlength="128"/></td>';
	$x .= '<td width="1%"><input id="Row['.$RowID.'][BookIDUniqueID]" type="checkbox" value="'.$r_BookID.'-'.$r_UniqueBookID.'-'.$r_StocktakeLogID.'" name="Row['.$RowID.'][BookIDUniqueID]" onclick="unset_checkall(this, document.getElementById(\'form1\'));" checked></input></td>';

	$x .= '</tr>';
	
	echo $x;
}
?>

