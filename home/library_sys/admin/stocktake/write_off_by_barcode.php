<?php
// using: 
/*
 * 	Log
 * 
 * 	Date:	2017-09-06 (Cameron)
 * 			redirect to write_off.php page when click 'Cancel' button
 * 
 * 	Date:	2016-04-26 (Cameron) 
 * 			add charset=UTF-8 in meta data before calling NO_ACCESS_RIGHT_REDIRECT
 * 
 * 	Date:	2016-04-25 (Cameron) 
 * 			change admin access right from "stock-take and write-off" to "write-off" [Case#K94933]
 * 
 * 	Date:	2015-06-10 (Henry)
 * 			fix: cannot access when the group only has stock-take and write-off permission [Case#X79521]
 * 
 * 	Date:	2015-02-03 [Cameron] create this file
 */


if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['write-off'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

	
$libms = new liblms();
$linterface = new interface_html();

$CurrentPage = "PageWriteOff";

$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['writeOffByBarcode']['WriteOffByBarcode'], "");

$TAGS_OBJ[] = array($Lang["libms"]["book_status"]["WRITEOFF"], "write_off.php", 1);

$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$linterface->LAYOUT_START();


#################### Stocktake Scheme Selection Box Start ######################
$sql = $libms->GET_STOCKTAKE_SCHEME_SQL($stocktakeSchemeID='',1);
$stockTakeSchemeArr = $libms->returnArray($sql);
$numOfstockTakeSchemeArr = count($stockTakeSchemeArr);
for($i=0;$i<$numOfstockTakeSchemeArr;$i++){
	$thisStockTakeSchemeId = $stockTakeSchemeArr[$i]['StocktakeSchemeID'];
	$thisStockTakeDisplay = $stockTakeSchemeArr[$i]['SchemeTitle'] . ' ~ (' .  $stockTakeSchemeArr[$i]['StartDate']  . ' - ' .  $stockTakeSchemeArr[$i]['EndDate'] . ')';
	$stockTakeSchemeSelectionArr[]= array($thisStockTakeSchemeId,$thisStockTakeDisplay);
}

$StocktakeSchemeID = ($_REQUEST['StocktakeSchemeID']) ? $_REQUEST['StocktakeSchemeID'] : '';	// return from adding write_off record 
$stockTakeSelectionBox = $linterface->GET_SELECTION_BOX($stockTakeSchemeSelectionArr, "name='StocktakeSchemeID' id='StocktakeSchemeID'" , '', $StocktakeSchemeID='');
#################### Stocktake Scheme Selection Box End ########################

$xmsg = '';		// no feedback message for this page

?>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
<form name="form1" id="form1" method="post" action="">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center">
							<table class="form_table" width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td colspan="2">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="70%"><?=$toolbar ?></td>
												<td width="30%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td width="70%"><?=$Lang['libms']['settings']['stocktake']['name']?> <?=$stockTakeSelectionBox?></td>
									<td valign="bottom">
									    <div align="right"><?=$Lang['libms']['bookmanagement']['EnterOrScanItemBarcode']?> 
									    	<input name="" type="text" id='input_code_text' class="input_code_text" />
									    </div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<div id="RecordToBeWrittenOff">
								<table class="common_table_list view_table_list" width="100%" id="return_table">
									<thead>
										<tr>
											<th width="1">#</th>
											<th width="15%"><?=$Lang["libms"]["report"]["ACNO"]?></th>
											<th width="15%"><?=$Lang['libms']['bookmanagement']['barcode']?></th>
											<th width="25%"><?=$Lang['libms']['bookmanagement']['bookTitle']?></th>
											<th width="15%"><?=$Lang['libms']['bookmanagement']['location']?></th>
											<th width="15%"><?=$Lang['libms']['bookmanagement']['WriteOffDate']?></th>
											<th width="15%"><?=$Lang['libms']['bookmanagement']['Reason']?>
												<br/><input type="text" name="SetAllReason" id="SetAllReason" maxlength="128" size="15" value=""/>
												<div style="float:right"><?=$linterface->Get_Apply_All_Icon("javascript:SetAllReason();")?></div></th>
											<th class="num_check" width="1"><input type="checkbox" id="SelectAll" name="checkmaster" checked></input></th>											
										</tr>
									</thead>
									<tbody>
										<tr class="tablebluerow2 tabletext" id="tr_enterBarcode">
											<td class="tabletext" colspan="8" align="center" height="40" style="vertical-align:middle"><?=$Lang['libms']['bookmanagement']['EnterOrScanItemBarcode']?></td>
										</tr>
									</tbody>
								</table>
							</div>
							
							<div class="edit_bottom_v30">
								<p class="spacer"></p>
								<?=$linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
								<?=$linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
								<p class="spacer"></p>
							</div>
							
						</td>
					</tr>				
				</table>
			</td>
		</tr>
	</table>
</form>

<script>
var currStocktakeSchemeID = '';		// current stock take scheme ID

$(document).ready(function(){
	$('#input_code_text').keyup(function(event) {
		if ((event.which == 13) && ($.trim($('#input_code_text').val()) != '')) {
			$('#input_code_text').val($('#input_code_text').val().toUpperCase());
			var currentBarcode = '';
			$('#return_table tr.returned ').each(function() {
				currentBarcode += $(this).children().eq(2).text() + ',';
			});

			if ((currentBarcode != '') && (currentBarcode.indexOf($('#input_code_text').val()) != -1)) {
				alert('<?=$Lang['libms']['bookmanagement']['writeOffByBarcode']['DuplicateBarcode']?>');
				$('#input_code_text').val('');
			}
			else {
				$.ajax({
					url:	"ajax_get_pending_write_off_records.php",
					type:   "POST",
					data:   {
								'Barcode': encodeURIComponent($.trim($('#input_code_text').val())),
								'StocktakeSchemeID': $('#StocktakeSchemeID').val(),
								'RowID': $('#return_table tr').length-1,								
							},
					async:	false,
					error:  function(xhr, ajaxOptions, thrownError){
								alert(xhr.responseText);
							},
					success: function(data)
							{		
								if($.trim(data) == 'writeoffbook'){
									alert('<?=$Lang['libms']['bookmanagement']['writeOffByBarcode']['IsWriteOffBook']?>');
									$('#input_code_text').val('');
								}
								else if($.trim(data)!='')
								{
									$('#tr_enterBarcode').css("display","none");
									$('#return_table tr:first').after(data);
									$('#input_code_text').val('');
									currStocktakeSchemeID = $('#StocktakeSchemeID').val();
								}
								else
								{
									alert('<?=$Lang['General']['NoRecordFound']?>');
									$('#input_code_text').val('');
								}
							},
				 });
			}	// not duplicate
	  	}  
	});
	
	$('#input_code_text').focus();

});

$('#SelectAll').click(function(){
		$("input[name$='\[BookIDUniqueID\]']").attr('checked', $(this).attr('checked'));
});
	
$('#submitBtn').click(function() {
	if ($("input[name$='\[BookIDUniqueID\]']:checked").length <= 0) {
		alert(globalAlertMsg2);
	}
    else{
        if(confirm('<?=$Lang['libms']['bookmanagement']['ConfirmWriteOff']?>')){
	        $('#form1').attr('action','add_write_off_record.php');
	        $('#form1').submit();	            
        }
    }
});

$('#cancelBtn').click(function() {		
	window.location = 'write_off.php';
});	

function SetAllReason() {
    $("input[name$='\[WriteoffReasons\]']").each(function() {					
    	$(this).val($("#SetAllReason").val());				
    });
}

$('#StocktakeSchemeID').click(function() {
	if ( ($(this).val() != currStocktakeSchemeID) && ($('#RecordToBeWrittenOff').find('input[name$="\[BookIDUniqueID\]"]').length > 0 )) {
		var confirmed = window.confirm('<?=$Lang['libms']['bookmanagement']['writeOffByBarcode']['ConfirmChangeStocktakeName']?>');
		if (confirmed) {
			$('#return_table tr.returned').remove();
			$('#tr_enterBarcode').css("display","");
		}
		else {
			$('#StocktakeSchemeID').val(currStocktakeSchemeID);	// reset value back to previous
		}		
	}
});
	
</script>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>