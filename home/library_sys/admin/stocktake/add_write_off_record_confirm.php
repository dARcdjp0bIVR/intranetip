<?php
// using: 

/*************************************************************
 * 
 * 	20160426 (Cameron) 
 * 			add charset=UTF-8 in meta data before calling NO_ACCESS_RIGHT_REDIRECT
 *  
 * 	20160425 (Cameron) 
 * 			change admin access right from "stock-take and write-off" to "write-off" [Case#K94933]
 * 
 *  20150610 (Henry)
 * 			fix: cannot access when the group only has stock-take and write-off permission [Case#X79521]
 * 
 * 	20150206 (Cameron) fix bug for row record shifted if leave some rows unchecked
 * 
 *	20131122 (Henry)
 * 			Created this file
 * 
 *************************************************************/

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['write-off'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libms = new liblms();
$linterface = new interface_html();

$stocktakeSchemeID = $_POST['StocktakeSchemeID'];
$BookIDUniqueID = $_POST['BookIDUniqueID'];
$stocktakeStatus = $_POST['selectedStocktakeStatus'];


# Page Settings
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageWriteOff";

//$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['stock-take'], "index.php", 0);
$TAGS_OBJ[] = array($Lang["libms"]["book_status"]["WRITEOFF"], "write_off.php", 1);

$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$linterface->LAYOUT_START();

$sql = $libms->GET_STOCKTAKE_SCHEME_SQL($StocktakeSchemeID,1);
$stocktakeSchemeArr = $libms->returnArray($sql);
$stocktakeSchemeTitle = $stocktakeSchemeArr[0]['SchemeTitle'];
$keyword = $_POST['keyword'];

# Page Navigation
$PAGE_NAVIGATION[]= array($stocktakeSchemeTitle, 'write_off.php?StocktakeSchemeID='.$stocktakeSchemeID.'&selectedStocktakeStatus='.$stocktakeStatus);
$PAGE_NAVIGATION[]= array($Lang["libms"]["book_status"]["WRITEOFF"], '');

$x = '<form name="form1" id="form1" method="post" action="">';

$x .= '<table class="common_table_list view_table_list" width="100%">
			<tr>
				<th width="1">#</th>
				<th width="15%">'.$Lang["libms"]["report"]["ACNO"].'</th>
				<th width="15%">'.$Lang['libms']['bookmanagement']['barcode'].'</th>
				<th width=\"25%\">'.$Lang['libms']['bookmanagement']['bookTitle'].'</th>
				<th width=\"15%\">'.$Lang['libms']['bookmanagement']['location'].'</th>
				<th width="15%">'.$Lang['libms']['bookmanagement']['WriteOffDate'].'</th>
				<th width="15%">'.$Lang['libms']['bookmanagement']['Reason'].'
				<br/><input type="text" name="SetAllReason" id="SetAllReason" maxlength="128" size="15" value=""/>
				<div style="float:right">'.$linterface->Get_Apply_All_Icon("javascript:SetAllReason();").'</div></th>
				<th class="num_check" width="1"><input type="checkbox" id="SelectAll" name="checkmaster" checked></input></th>
			</tr>';
												
		
$numOfBookIDUniqueID = count($BookIDUniqueID);
if(sizeof($numOfBookIDUniqueID)>0){	
	for($i=0;$i<$numOfBookIDUniqueID;$i++){
		$thisBookIDUniqueID = $BookIDUniqueID[$i];
		$thisBookIDUniqueIDArr = explode('-', $thisBookIDUniqueID);
		$thisBookID = $thisBookIDUniqueIDArr[0];
		$thisUniqueID = $thisBookIDUniqueIDArr[1];
		$result = current($libms->GET_BOOK_ITEM_INFO($thisUniqueID));
		
		$locationName = current($libms->GET_LOCATION_INFO($result['LocationCode']));
		$locationName = Get_Lang_Selection($locationName['DescriptionChi'],$locationName['DescriptionEn']);
		
		$bookTitle = current($libms->GET_BOOK_INFO($result['BookID']));
		$bookTitle = $bookTitle['BookTitle'];
		$j = $i+1;
		$x .= '<tr>
					<td>'.$j.'</td>
					<td>'.$result['ACNO'].'</td>
					<td>'.$result['BarCode'].'</td>
					<td>'.$bookTitle.'</td>
					<td>'.$locationName.'</td>
					<td>'.$linterface->GET_DATE_PICKER("Row[$j][WriteoffDate]", date("Y-m-d"),"","yy-mm-dd","","","","",$ID="WriteoffDate_$j").'</td>
					<td><input type="text" id="Row['.$j.'][WriteoffReasons]" name="Row['.$j.'][WriteoffReasons]" value="" maxlength="128"/></td>
					<td><input id="Row['.$j.'][BookIDUniqueID]" type="checkbox" value="'.$thisBookIDUniqueID.'" name="Row['.$j.'][BookIDUniqueID]" onclick="unset_checkall(this, document.getElementById(\'form1\'));" checked></input></td>
				</tr>';
				
	}
}
else{
	 # no record
	$x.="<tr class=\"tablebluerow2 tabletext\"><td class=\"tabletext\" colspan=\"8\" align=\"center\" height=\"40\" style=\"vertical-align:middle\">$i_no_record_exists_msg</td></tr>";
}
$x .= '</table>';
$x .= '<div class="edit_bottom_v30">
			<p class="spacer"></p>
			'.$linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")
			.$linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn").'
			<p class="spacer"></p>
		</div>';
$x .= '<input type="hidden" name="StocktakeSchemeID" value="'.$stocktakeSchemeID.'"/>';
$x .= '<input type="hidden" name="selectedStocktakeStatus" value="'.$stocktakeStatus.'"/>';
$x .= '</form>';

$display = $x;

?>
<script>
//function js_form_submit(type){
//
//	if(type=='writeOffRecord'){
//		$('#form1').attr('action', 'add_write_off_record.php');
//	}
//	else if(type=='cancelWriteOffRecord'){
//		$('#form1').attr('action', 'cancel_write_off_record.php');
//	}
//	else{
//		$('#form1').attr('action', 'write_off.php');
//	}
//	
//	$('#form1').submit();	
//}

//function goSubmit(){
//	var canSubmit = true;
//	if (canSubmit) {
//		checkRemove(document.form1,'BookIDUniqueID[]','add_write_off_record.php', '<?=$Lang['libms']['bookmanagement']['ConfirmWriteOff']?>');
//	}
//}
//
//function goCancel() {
//	var canSubmit = true;
////	if (isFormChanged() && !confirm('The form has been modified. Are you sure you want to leave the page?')) {
////		canSubmit = false;
////	}
//	
//	if (canSubmit) {
//		window.location = 'write_off.php?StocktakeSchemeID=<?=$stocktakeSchemeID?>&selectedStocktakeStatus=<?=$stocktakeStatus?>';
//	}
//}

//function SetAllReason() {
//	var WriteoffReasons = document.getElementsByName("WriteoffReasons[]");
//	var ApplyReason = document.getElementById("SetAllReason").value;
//	for (var i=0; i< WriteoffReasons.length; i++) {
//		document.getElementById('WriteoffReasons'+i).value = ApplyReason;
//	}
//}

function SetAllReason() {
    $("input[name$='\[WriteoffReasons\]']").each(function() {					
    	$(this).val($("#SetAllReason").val());				
    });
}

$(document).ready(function(){
	$('#SelectAll').click(function(){
			$("input[name$='\[BookIDUniqueID\]']").attr('checked', $(this).attr('checked'));
	});
	
	$('#submitBtn').click(function() {
		if ($("input[name$='\[BookIDUniqueID\]']:checked").length <= 0) {
			alert(globalAlertMsg2);
		}
	    else{
	        if(confirm('<?=$Lang['libms']['bookmanagement']['ConfirmWriteOff']?>')){
		        $('#form1').attr('action','add_write_off_record.php');
		        $('#form1').submit();	            
	        }
	    }
	});
	
	$('#cancelBtn').click(function() {
		window.location = 'write_off.php?StocktakeSchemeID=<?=$stocktakeSchemeID?>&selectedStocktakeStatus=<?=$stocktakeStatus?>';
	});	
	
});

</script>
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form name="form1" id="form1" method="post" action="">

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%"><?=$toolbar ?></td>
											<td width="30%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?=$display?>
					</td>
				</tr>
				</table>

			
			</td>
		</tr>
	</table>
</form>

<?php
//echo $linterface->FOCUS_ON_LOAD("form1.LocationCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>