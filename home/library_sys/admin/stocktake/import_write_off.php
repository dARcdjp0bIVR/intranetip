<?php
// using: 

/*************************************************************
 * 
 * 	20160426 (Cameron) 
 * 			add charset=UTF-8 in meta data before calling NO_ACCESS_RIGHT_REDIRECT
 *  
 * 	20160425 (Cameron) 
 * 			change admin access right from "stock-take and write-off" to "write-off" [Case#K94933]
 * 
 *  20150610 (Henry)
 * 			fix: cannot access when the group only has stock-take and write-off permission [Case#X79521]
 * 
 *	20131126 (Henry)
 * 			Created this file
 * 
 *************************************************************/
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['write-off'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libms = new liblms();
$linterface = new interface_html();

# Page Settings
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageWriteOff";

//$PAGE_NAVIGATION[] = array($Lang["libms"]["book_status"]["WRITEOFF"], "write_off.php");
$PAGE_NAVIGATION[] = array($Lang["libms"]["bookmanagement"]["import"], "");

//$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['stock-take'], "index.php", 0);
$TAGS_OBJ[] = array($Lang["libms"]["book_status"]["WRITEOFF"], "write_off.php", 1);
//$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_import']);

$btnAry[] = array('import', 'javascript: void(0);');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


$sample_file = "write_off_import.csv";
$csvFile = "<a class=\"tablelink\" href=\"../book/import/csv/get_sample_file.php?file=$sample_file\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a>";

$csv_format = "";
$delim = "<br>";
for($i=0; $i<sizeof($Lang["libms"]["bookmanagement"]["ImportWriteOff"]); $i++){
	if($i!=0) $csv_format .= $delim;
	$csv_format .= $Lang['SysMgr']['Homework']['Column']." ".numberToLetter($i+1, true)." : ".$Lang["libms"]["bookmanagement"]["ImportWriteOff"][$i];
}


$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN(); 

$linterface->LAYOUT_START();

#################### Stocktake Scheme Selection Box Start ######################
$sql = $libms->GET_STOCKTAKE_SCHEME_SQL('',1);
$stockTakeSchemeArr = $libms->returnArray($sql);
## Default StocktakeSchemeID
if($stocktakeSchemeID=='')
{
	$stocktakeSchemeID = $stockTakeSchemeArr[0]['StocktakeSchemeID'];
}
$numOfstockTakeSchemeArr = count($stockTakeSchemeArr);
for($i=0;$i<$numOfstockTakeSchemeArr;$i++){
	$thisStockTakeSchemeId = $stockTakeSchemeArr[$i]['StocktakeSchemeID'];
	$thisStockTakeDisplay = $stockTakeSchemeArr[$i]['SchemeTitle'] . ' ~ (' .  $stockTakeSchemeArr[$i]['StartDate']  . ' - ' .  $stockTakeSchemeArr[$i]['EndDate'] . ')';
	$stockTakeSchemeSelectionArr[]= array($thisStockTakeSchemeId,$thisStockTakeDisplay);

	$thisStartDateTime = strtotime($stockTakeSchemeArr[$i]['StartDate']);;
	$thisEndDateTime = strtotime($stockTakeSchemeArr[$i]['EndDate']);
	$currentDate = strtotime(date("Y-m-d"));
	
	if(($thisStartDateTime<=$currentDate) && ($thisEndDateTime>=$currentDate)){
		$thisShowStocktakeBtn[$thisStockTakeSchemeId] = true;
		//$stockTakeSchemeSelectionArr[]= array($thisStockTakeSchemeId,$thisStockTakeDisplay);
	}else{
		$thisShowStocktakeBtn[$thisStockTakeSchemeId] = false;
	}
	
}

$stockTakeSelectionBox = $linterface->GET_SELECTION_BOX($stockTakeSchemeSelectionArr, 'name="StocktakeSchemeID" id="StocktakeSchemeID" onChange="javascript:js_form_submit(document.form1, \'changeScheme\')"', '', $stocktakeSchemeID);
#################### Stocktake Scheme Selection Box End ########################

?> 
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
<form name="form1" method="post" action="import_write_off_confirm.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" align="left" width="30%"><?=$iDiscipline['Import_Source_File']?>: 
						<span class="tabletextremark">
						<?=$Lang['General']['CSVFileFormat']?></span>
					</td>
					<td class="tabletext" width="70%">
						<input class="file" type="file" name="file" id="file">
					</td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['libms']['bookmanagement']['stocktakeRecord']?></td>
					<td class="tabletext"><?=$stockTakeSelectionBox?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?></td>
					<td class="tabletext"><?=$csvFile?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
					<td class="tabletext"><?=$csv_format?></td>
				</tr>
 
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['RequiredField']?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit","if(!document.getElementById('file').value){alert('".$Lang["libms"]['General']['warnSelectcsvFile']."');return false;}") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='write_off.php'") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="action" value="simple" />
</form>
<br />

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>