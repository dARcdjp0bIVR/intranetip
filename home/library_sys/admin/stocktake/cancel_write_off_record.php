<?php
// Using: 

############ Change Log Start ###############
#
# 20181205 (Cameron): bug fix: book should resume status to 'NORMAL' if it is returned from previous status ('BORROWED' or 'LOST') or cancel reseervation ('DELETE')
#
# 20180116 (Henry) : bug fix: fail to update book status when cancel write off record [Case#H134082]
#
# 20140929 (Henry) : added to call function SYNC_PHYSICAL_BOOK_TO_ELIB() (ip.2.5.5.10.1)
#
############ Change Log End ###############

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$linterface = new interface_html();
$libel = new elibrary();

$stocktakeSchemeID = $_POST['StocktakeSchemeID'];
if($stocktakeSchemeID == -1){
	$stocktakeSchemeID = '';
}
$BookIDUniqueID = $_POST['BookIDUniqueID'];
$stocktakeStatus = $_POST['selectedStocktakeStatus'];

$result = array();
$numOfBookIDUniqueID = count($BookIDUniqueID);
for($i=0;$i<$numOfBookIDUniqueID;$i++){
	$thisBookIDUniqueID = $BookIDUniqueID[$i];
	$thisBookIDUniqueIDArr = explode('-', $thisBookIDUniqueID);
	$thisBookID = $thisBookIDUniqueIDArr[0];
	$thisUniqueID = $thisBookIDUniqueIDArr[1];
	$thisWriteOffID =  $thisBookIDUniqueIDArr[2];
	
	$thisWriteOffLogInfo = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID,$thisBookID,$thisUniqueID);
	$thisPreviousBookStatus = trim($thisWriteOffLogInfo[0]['PreviousBookStatus']);

	if($thisWriteOffID!=''){
	    if ($thisPreviousBookStatus == 'BORROWED' || $thisPreviousBookStatus == 'LOST') {
	        $borrowLogStatus = $libms->getLatestBorrowLogStatus($thisUniqueID);
	        if ($borrowLogStatus == 'RETURNED') {
	            $thisPreviousBookStatus = 'NORMAL';        // returned book should be 'NORMAL'
	        }
	    }
	    else if ($thisPreviousBookStatus == 'RESERVED') {
	        $reservedLogStatus = $libms->getLatestReservationLogStatus($thisUniqueID);
	        if ($reservedLogStatus == '' || $reservedLogStatus == 'DELETE') {
	            $thisPreviousBookStatus = 'NORMAL';        // cancelled reserved book should be 'NORMAL'
	        }
	    }
		$result['UpdateUniqueBookSuccess'] = $libms->UPDATE_UNIQUE_BOOK($thisUniqueID,$thisPreviousBookStatus);
		$result['WriteOffSuccess'] = $libms->DELETE_WRITEOFF_LOG_RECORD($thisWriteOffID);
		$libms->UPDATE_BOOK_COPY_INFO($thisBookID);
		//syn the front end of the book info
		$libel->SYNC_PHYSICAL_BOOK_TO_ELIB($thisBookID);
	}
	
}



header("Location: write_off.php?xmsg=delete&StocktakeSchemeID=$stocktakeSchemeID&selectedStocktakeStatus=$stocktakeStatus");

?>