<?php
/*
 * 	Using:	
 * 
 * 	Log
 * 
 * 	Date:	2016-04-26 (Cameron) 
 * 			add charset=UTF-8 in meta data before calling NO_ACCESS_RIGHT_REDIRECT
 * 
 * 	Date:	2016-04-25 (Cameron) 
 * 			change admin access right from "stock-take and write-off" to "stock-take" [Case#K94933]
 * 
 * 	Date:	2015-06-10 (Henry)
 * 			fix: cannot access when the group only has stock-take and write-off permission [Case#X79521]
 * 
 *  Date:	2015-06-05 [Henry] add check for the nonstocktakebook
 * 
 * 	Date:	2014-04-09 [Henry] for the amendance of stocktake report (still developing...)
 * 
 *  Date:   2014-03-20 [Tiffany] Shows the period. Shows the Last Five Record. Add the Relocate Button.
 *                               
 * 	Date:	2014-03-07 [Henry] Add table row to inform user to Enter or scan item barcode
 * 
 * 	Date:	2013-06-18 [Cameron] Add checking if barcode(#input_code_text) is null before parsing to get details info
 * 				Don't process if null, otherwise, it will get all record
 */

// using: Henry

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
//ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['stock-take'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();

$result = $libms->SELECTFROMTABLE('LIBMS_NOTIFY_SETTING', '*', '','',1,'name');
//dump ($result);
$linterface = new interface_html();
# Top menu highlight setting
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageStockTake";

# Get Scheme Title
$sql="select SchemeTitle from LIBMS_STOCKTAKE_SCHEME where StocktakeSchemeID='".$StocktakeSchemeID."'";
$result = $libms->returnVector($sql);
$scheme_title=$result[0];

$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['stock-take'], "index.php", 1);
//$TAGS_OBJ[] = array($Lang["libms"]["book_status"]["WRITEOFF"], "write_off.php", 0);

$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['stock-take'], "index.php");
//$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['stocktakeList'] , "");
$PAGE_NAVIGATION[] = array($scheme_title , "");
# Left menu 
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

# Toolbar
########### Selection Display Start #################
$locationSelectionBtn = '';
$locationSelectionBtn .= '<div class="Conntent_tool"><a href="javascript:MM_showHideLayers(\'status_option\',\'locationSelectionBtn\',\'show\');"  id="locationSelectionBtn" class="new thickbox">' .$Lang['libms']['bookmanagement']['SelectAnotherLocation'] .' <a href="javascript:MM_showHideLayers(\'status_option\',\'\',\'show\');" style=" display:block; float:left; padding-left:20px; background:url(/images/2009a/selectbox_arrow.gif) no-repeat left; color:#0066CC; line-height:25px;"></a></a></div>';
//$locationSelectionBtn .= '<span> | </span>';
$toolbar = $locationSelectionBtn;

########### Selection Display End #################

########### Last 5 books Display Start #################
$last_books=0;
$location = $_POST['selectedLocationCode']?$_POST['selectedLocationCode']:$_GET['newLocationCode'];

$last_five_books=$libms->GET_STOCKTAKE_LOG_BOOK_INFO_ORDER('',$StocktakeSchemeID,'','','','',$location,'lsl.DateModified desc limit 5','');
$change_location = $Lang['General']['EmptySymbol'];	

########### Last 5 books Display End   #################

# Get Scheme Title
$sql="select SchemeTitle, RecordStartDate, RecordEndDate from LIBMS_STOCKTAKE_SCHEME where StocktakeSchemeID='".$StocktakeSchemeID."'";
$result = $libms->returnArray($sql);
$scheme_title=$result[0][0];
//$toolbar .= $linterface->GET_LNK_IMPORT("import.php",$button_import,"","","",0);
# Get Post Value
$locationCode = $_POST['selectedLocationCode']?$_POST['selectedLocationCode']:$_GET['newLocationCode'];

$stocktakeSchemeID = $_POST['StocktakeSchemeID']?$_POST['StocktakeSchemeID']:$_GET['StocktakeSchemeID'];

$stocktakeRecordEndDate = $result[0][2];
$stocktakeRecordStartDate = $result[0][1];

# Redirect Page If No $stocktakeSchemeID
if($stocktakeSchemeID==''){
	header("Location: index.php");	
	exit();
}

# Start layout
$linterface->LAYOUT_START();
# Location Info
$htmlAry['locationRowDisplay'] = '';
$locationInfo = $libms->GET_LOCATION_INFO('',1);
$locationCodeAssoArr = BuildMultiKeyAssoc($locationInfo, 'LocationCode');

# get the removed location (Henry20140502)
$locationNotExist = $libms->GET_LOCATION_NOT_EXIST();
//debug_pr($locationNotExist);

if($locationCode == -1){ //Henry20140430
	$locationEngName = '('.$Lang['libms']['bookmanagement']['NoLocation'].')';
	$locationChiName = '('.$Lang['libms']['bookmanagement']['NoLocation'].')';
	$thislocationName = Get_Lang_Selection($locationChiName,$locationEngName);
}else if($locationCode){
	$locationEngName = $locationCodeAssoArr[$locationCode]['DescriptionEn'];
	$locationChiName = $locationCodeAssoArr[$locationCode]['DescriptionChi'];
	$thislocationName = Get_Lang_Selection($locationChiName,$locationEngName);
}else{
	$thislocationName = $Lang['libms']['bookmanagement']['overall'];
}
$htmlAry['StockTakeList'] = '';
$htmlAry['StockTakeList'] .= '<table width="100%"  id="return_table" class="common_table_list view_table_list">';
	$htmlAry['StockTakeList'] .= '<thead>';
		$htmlAry['StockTakeList'] .= '<tr>';
		$htmlAry['StockTakeList'] .= '<th width="2%">#</th>';
		$htmlAry['StockTakeList'] .= '<th width="10%">'.$Lang["libms"]["report"]["ACNO"].'</th>';
		$htmlAry['StockTakeList'] .= '<th width="10%">'.$Lang['libms']['bookmanagement']['barcode'].'</th>';
		$htmlAry['StockTakeList'] .= '<th width="18%">'.$Lang['libms']['bookmanagement']['bookTitle'].'</th>';
		$htmlAry['StockTakeList'] .= '<th width="10%">'.$Lang['libms']['bookmanagement']['bookStatus'].'</th>';
		$htmlAry['StockTakeList'] .= '<th width="10%">'.$Lang['libms']['bookmanagement']['result'] .'</th>';
		$htmlAry['StockTakeList'] .= '<th width="10%">'.$Lang['libms']['bookmanagement']['ItemLocation'].'</th>';
		$htmlAry['StockTakeList'] .= '<th width="12%">'.$Lang['libms']['bookmanagement']['Stock-takeTime'].'</th>';		
		$htmlAry['StockTakeList'] .= '<th width="18%"></th>';
		$htmlAry['StockTakeList'] .= '</tr>';
		$htmlAry['StockTakeList'] .= '<tr id="tr_enterBarcode">';
		if($last_five_books[0]['BookTitle']==""){
		$htmlAry['StockTakeList'] .= '<td colspan="9" style="text-align:center">';
		$htmlAry['StockTakeList'] .= $Lang['libms']['bookmanagement']['EnterOrScanItemBarcode'];
		$htmlAry['StockTakeList'] .= '</td>';
		}
		$htmlAry['StockTakeList'] .= '</tr>';
		if($last_five_books[0]['BookTitle']!=""){
	    $htmlAry['StockTakeList'] .= '<tr id="tr_lastfive">';
		$htmlAry['StockTakeList'] .= '<td class="tabletextremark" colspan="9" style="text-align:center">';
		if(count($last_five_books)==5)$htmlAry['StockTakeList'] .= $Lang['libms']['bookmanagement']['LastFive'];
		if(count($last_five_books)==4)$htmlAry['StockTakeList'] .= $Lang['libms']['bookmanagement']['LastFour'];
		if(count($last_five_books)==3)$htmlAry['StockTakeList'] .= $Lang['libms']['bookmanagement']['LastThree'];
		if(count($last_five_books)==2)$htmlAry['StockTakeList'] .= $Lang['libms']['bookmanagement']['LastTwo'];
		if(count($last_five_books)==1)$htmlAry['StockTakeList'] .= $Lang['libms']['bookmanagement']['LastOne'];		
		$htmlAry['StockTakeList'] .= '</td>';
		$htmlAry['StockTakeList'] .= '</tr>';
        $last_books++;
		  for($i=0;$i<count($last_five_books);$i++){
			if($last_five_books[$i]['BookTitle']!=""){
			   $htmlAry['StockTakeList'] .= '<tr>';
			   $htmlAry['StockTakeList'] .= '<td class="tabletextremark" width="2%">L'.(count($last_five_books)-$i).'</td>';
		       $htmlAry['StockTakeList'] .= '<td class="tabletextremark" width="10%">'.$last_five_books[$i]['ACNO'].'</td>';
		       $htmlAry['StockTakeList'] .= '<td class="tabletextremark" width="10%">'.$last_five_books[$i]['BarCode'].'</td>';
	           $htmlAry['StockTakeList'] .= '<td class="tabletextremark" width="18%">'.$last_five_books[$i]['BookTitle'].'</td>';
	           $htmlAry['StockTakeList'] .= '<td class="tabletextremark" width="10%">'.$last_five_books[$i]['RecordStatus'].'</td>';
		       $htmlAry['StockTakeList'] .= '<td class="tabletextremark" width="10%">'.$last_five_books[$i]['Result'].'</td>';
		       $htmlAry['StockTakeList'] .= '<td class="tabletextremark" width="10%">'.$last_five_books[$i]['LocationName'].'</td>';
		   	   $htmlAry['StockTakeList'] .= '<td class="tabletextremark" width="12%">'.$last_five_books[$i]['DateModified'].'</td>';	
		       $htmlAry['StockTakeList'] .= '<td class="tabletextremark" width="18%">'.$change_location.'</td>';	
		       $htmlAry['StockTakeList'] .= '</tr>';
		       $last_books++;
			}
		  }
		}
	$htmlAry['StockTakeList'] .= '</thead>';
	$htmlAry['StockTakeList'] .= '<tbody>';
	$htmlAry['StockTakeList'] .= '</tbody>';
$htmlAry['StockTakeList'] .= '</table>';

$htmlAry['StockTakeList'] .= '<input type="hidden" name="stocktakeSchemeID" id="stocktakeSchemeID" value="'.$stocktakeSchemeID.'"></input>';
$htmlAry['StockTakeList'] .= '<input type="hidden" name="pageLocationCode" id="pageLocationCode" value="'.$locationCode.'"></input>';

######################################### Location Selecion Layer #########################################
# Unique Book Info 
//$uniqueBookArr = $libms->GET_UNIQUE_BOOK_AMOUNT_BY_LOCATION();
//for the amendance of stocktake report [Start] 20140408
$uniqueBookArr = $libms->GET_UNIQUE_BOOK_AMOUNT_BY_LOCATION_PERIOD($stocktakeRecordStartDate,$stocktakeRecordEndDate, $stocktakeSchemeID);
//for the amendance of stocktake report [End] 20140408
//for the item that has no location
$tempCount = 0;
$tempUniqueBookArr = $uniqueBookArr;
foreach($tempUniqueBookArr as $key => $aUniqueBorrArr){
	if($aUniqueBorrArr['LocationCode'] == ''){
		$tempCount += $aUniqueBorrArr['UniqueBookAmount'];
		$tempUniqueBookArr[$key]['UniqueBookAmount'] = $tempCount;
	}
}
$uniqueBookLocationCodeAssocArr = BuildMultiKeyAssoc($tempUniqueBookArr, 'LocationCode'); 
# Btn 
//$btn_Apply = $linterface->GET_SMALL_BTN($Lang['Btn']['Apply'], "button", $onclick="js_Change_Location();", $id="Btn_Apply");
$btn_Cancel = $linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", $onclick='MM_showHideLayers(\'status_option\',\'\',\'hide\')', $id="Btn_Cancel");

$locationSelectionLayer = '';
$locationSelectionLayer.= '<div class="selectbox_layer" id="status_option" style="visibility: hidden; width: 240px; height: 220px;overflow: scroll;">';
	$locationSelectionLayer.= '<form name="form1" id="form1">';
		$locationSelectionLayer.= '<table width="200px" height="100%" cellspacing="0" cellpadding="3" border="0" align="center">';
		$locationSelectionLayer.= '<tbody>';
		$locationSelectionLayer.= '<tr>';
		$locationSelectionLayer.= '<td nowrap="">';
			$locationSelectionLayer.= '<table width="200px">';
			
			## Loop Each Location
			$overallTotalArr = array();
			$overallNumOfStocktakeLogArr = array();
			
			//Henry20140430
			$numOflocation = count($locationInfo);
			$locationInfo[$numOflocation]['DescriptionChi'] = '('.$Lang['libms']['bookmanagement']['NoLocation'].')';
			$locationInfo[$numOflocation]['DescriptionEn'] = '('.$Lang['libms']['bookmanagement']['NoLocation'].')';
			$locationInfo[$numOflocation]['LocationCode'] = '-1';
			
			for($i=0;$i<count($locationInfo);$i++){
				$thisProgress = 0;
				$locationCodeChecked = '';
				$thisLocationCode = $locationInfo[$i]['LocationCode'];
				$thisLocationEngName = $locationInfo[$i]['DescriptionEn'];
				$thisLocationChiName = $locationInfo[$i]['DescriptionChi'];
				$thisOptionlocationName = Get_Lang_Selection($thisLocationChiName,$thisLocationEngName);

				# Unique Book Amount 
				$thisLocationUniqueBookAmount = $uniqueBookLocationCodeAssocArr[$thisLocationCode]['UniqueBookAmount'];
				
				//Herny 20140430
				if($thisLocationCode == '-1'){
					$thisLocationUniqueBookAmount = 0;
					for($j=0;$j<count($locationNotExist);$j++){
						$thisLocationUniqueBookAmount += $uniqueBookLocationCodeAssocArr[$locationNotExist[$j]]['UniqueBookAmount'];
					}
						$otherLocationBookAmount = $thisLocationUniqueBookAmount;
				}
				
				# Get Stocktake Log Info 
				$thisStocktakeLogInfo = $libms->GET_STOCKTAKE_LOG('', $stocktakeSchemeID, '', '', $thisLocationCode,1);
				$numOfthisStocktakeLog = count($thisStocktakeLogInfo);
				//20140408 [Start]
				$thisWriteoffLogInfo = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID, '', '', 'Array','','','',$thisLocationCode,false);
				$numOfthisStocktakeLog += count($thisWriteoffLogInfo);
				//20140408 [End]
				
				# Progress
				if($thisLocationUniqueBookAmount!='' && $numOfthisStocktakeLog!=''){
					$thisProgress = number_format($numOfthisStocktakeLog/$thisLocationUniqueBookAmount * 100,2) . '%' ; 
				}

				$thisLocationCodeCss = '';
				if($locationCode==$thisLocationCode){
					$thisLocationCodeCss = 'style="color:red"';	
				}
				
				$locationSelectionLayer.= '<tr>';
				$locationSelectionLayer.= '<td width="100px">';
				$locationSelectionLayer.= '<a  '.$thisLocationCodeCss.' href="javascript:js_Change_Location(\''.$thisLocationCode.'\');"><span class="tablelink"><strong>'.$thisOptionlocationName.'</span></strong></a>';	
				$locationSelectionLayer.= '</td>';
				$locationSelectionLayer.= '<td width="40px">';
				$locationSelectionLayer.= '<a  '.$thisLocationCodeCss.' href="javascript:js_Change_Location(\''.$thisLocationCode.'\');"><span class="tablelink"><strong>'. ($thisProgress?$thisProgress:$Lang['General']['EmptySymbol'] ) .'</span></strong></a>';	
				$locationSelectionLayer.= '</td></tr>';
				
				//$overallTotalArr[]=$thisLocationUniqueBookAmount;
			}
					
			### Overall 
			$totalProgress = '';
			$overallUniqueBookArr = $libms->GET_UNIQUE_BOOK_AMOUNT_BY_LOCATION();
			foreach ($uniqueBookArr as $overallUniqueBookArrKey=>$overallUniqueBookArrInfo){
				$thisOverallUniqueBookAmount = $overallUniqueBookArrInfo['UniqueBookAmount'];
				$overallTotalArr[]= $thisOverallUniqueBookAmount;
			}
			
			$thisOverallBookUniqueAmount = array_sum($overallTotalArr);
			$allLocationStocktakeLogInfo = $libms->GET_STOCKTAKE_LOG('', $stocktakeSchemeID, '', '', '',1);
			$numOfAllLocationStocktake = count($allLocationStocktakeLogInfo);
			// 20140408 [Start]
			$thisOverallWriteoffLogInfo = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID, '', '', 'Array','','','','',false);
			$numOfAllLocationStocktake += count($thisOverallWriteoffLogInfo);
			// 20140408 [End]
			$totalProgress = number_format($numOfAllLocationStocktake/$thisOverallBookUniqueAmount * 100, 2) . '%'  ; 
						
			$overallLocationCodeCss = '';
			if($locationCode==''){
				$overallLocationCodeCss = 'style="color:red"';	
			}
			$locationSelectionLayer.= '<tr><td width="100px">';
			//$locationSelectionLayer.= '<input type="radio" id="locationCode_" name="locationCode" class="StatusCheckBox" value="" '.$overallLocationCodeCheck.'>';
			$locationSelectionLayer.= '<a '.$overallLocationCodeCss.' href="javascript:js_Change_Location(\'\');"><span class="tablelink"><strong>'.$Lang['libms']['bookmanagement']['overall'].'</strong></span></a>';	
			$locationSelectionLayer.= '</td>';
				$locationSelectionLayer.= '<td width="40px">';
				$locationSelectionLayer.= '<a '.$overallLocationCodeCss.' href="javascript:js_Change_Location(\'\');"><span class="tablelink"><strong>'.($totalProgress?$totalProgress:$Lang['General']['EmptySymbol'] ).'</strong></span></a>';
				$locationSelectionLayer.= '</td></tr>';	
			$locationSelectionLayer.= '</tr>';
			
			
			$locationSelectionLayer.= '</table>';
		$locationSelectionLayer.= '</td>';
		$locationSelectionLayer.= '</tr>';
		$locationSelectionLayer.= '<tr>';
		$locationSelectionLayer.= '<td height="2" valign="top" align="left" class="dotline"><img height="2" src="'.$PATH_WRT_ROOT.'/'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif"></td>';
		$locationSelectionLayer.= '</tr>';
		$locationSelectionLayer.= '<tr>';
		$locationSelectionLayer.= '<td><center>';
		$locationSelectionLayer .= $btn_Cancel;
		$locationSelectionLayer.= '</center></td>';
		$locationSelectionLayer.= '</tr>';
		$locationSelectionLayer.= '</tbody>';
		$locationSelectionLayer.= '</table>';
	
	$locationSelectionLayer.= '<input type="hidden" name="newLocationCode" id="newLocationCode" value=""></input>';
	$locationSelectionLayer.= '<input type="hidden" name="StocktakeSchemeID" value="'.$stocktakeSchemeID.'"></input>';
	$locationSelectionLayer.= '</form>';
$locationSelectionLayer.= '</div>';
######################################### Location Selection Layer End #########################################

?>
<script>
$(document).ready(function(){
		
	$('#input_code_text').keyup(function(event) {
//		if (event.which == 13) {
		if ((event.which == 13) && ($.trim($('#input_code_text').val()) != '')) {
					
		$('#input_code_text').val($('#input_code_text').val().toUpperCase());
		
		var pageLocationCode = $('#pageLocationCode').val();		
		
		$.ajax({
			url:      	"ajax_return_add_stocktake_record.php",
			type:     	"POST",
			data:     	'barcode=' + encodeURIComponent($('#input_code_text').val()) + '&stocktakeSchemeID='+ $('#stocktakeSchemeID').val() + '&ID=' +($('#return_table tr').length-'<?php echo $last_books?>'-1) +'&pageLocationCode='+ encodeURIComponent(pageLocationCode)  ,
			async:		false,
			error:    	function(xhr, ajaxOptions, thrownError){
							alert(xhr.responseText);
					  	},
			success: function(data)
					 {		
						if($.trim(data) == 'writeoffbook'){
							alert('<?=$Lang['libms']['bookmanagement']['IsWriteOffBook']?>');
							$('#input_code_text').val('');
						}
						else if($.trim(data) == 'nonstocktakebook'){
							alert('<?=$Lang['libms']['bookmanagement']['ItemNotForThisStocktake']?>');
							$('#input_code_text').val('');
						}
						else if($.trim(data)!='')
						{	
							$('#tr_enterBarcode').css("display","none");
							$('#return_table tr:first').after(data);		
							$('#input_code_text').val('');
							if($('#return_table tr.returned ').children().eq(4).text() == '<?=$Lang["libms"]["book_status"]["LOST"]?>')
								alert('<?=$Lang['libms']['bookmanagement']['bookStatusIsLost']?>');		
						}
						else
						{
							alert('<?=$Lang['General']['NoRecordFound']?>');
							$('#input_code_text').val('');
						}
					}
			 });
		
		$.ajax({
			url:      	"ajax_return_add_stocktake_summary.php",
			type:     	"POST",
			data:     	'barcode=' + encodeURIComponent($('#input_code_text').val()) + '&stocktakeSchemeID='+ $('#stocktakeSchemeID').val() + '&ID=' +($('#return_table tr').length-'<?php echo $last_books?>'-1) +'&pageLocationCode='+ encodeURIComponent(pageLocationCode)  ,
			async:		false,
			error:    	function(xhr, ajaxOptions, thrownError){
							alert(xhr.responseText);
					  	},
			success: function(data)
					 {		
						if($.trim(data)!='')
						{
							$('#span_book_found').html(data.split(',')[0]);
							var progress = ((data.split(',')[0]/$('#span_book_total').html()) || 0) * 100;
							progress = progress.toFixed(2);
							$('#span_book_progress').html(progress+'%');
						}
					}
			 });	
	  }  
	});
	
	$('#input_code_text').focus();
	
});

function js_Change_Item_Location(bookID, bookUniqueID, locationSelectionBoxID){
	
	var locationCode = '<?php echo $location?>';

	$.ajax({
		url:      	"ajax_update_book_location.php",
		type:     	"POST",
		data:     	'bookID=' + bookID + '&bookUniqueID=' + bookUniqueID + '&locationCode=' + encodeURIComponent(locationCode),
		async:		false,
		error:    	function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
				  	},
		success: function(data)
				 {					 	
					if(data)
					{	
						//alert('<?=$Lang['libms']['bookmanagement']['BookLocationUpdateSuccessfully']?>');
					
						$('#'+locationSelectionBoxID).html(data);

						
					}else
					{
						alert('<?=$Lang['libms']['bookmanagement']['BookLocationUpdateFailed']?>');
					}
				 }
		});
	$.ajax({
			url:      	"ajax_return_add_stocktake_summary.php",
			type:     	"POST",
			data:     	'barcode=' + encodeURIComponent($('#input_code_text').val()) + '&stocktakeSchemeID='+ $('#stocktakeSchemeID').val() + '&ID=' +($('#return_table tr').length-'<?php echo $last_books?>'-1) +'&pageLocationCode='+ encodeURIComponent(locationCode)  ,
			async:		false,
			error:    	function(xhr, ajaxOptions, thrownError){
							alert(xhr.responseText);
					  	},
			success: function(data)
					 {		
						if($.trim(data)!='')
						{	
							$('#span_book_total').html(data.split(',')[1]);			
							$('#span_book_found').html(data.split(',')[0]);
							var progress = ((data.split(',')[0]/$('#span_book_total').html()) || 0) * 100;
							progress = progress.toFixed(2);
							$('#span_book_progress').html(progress+'%');
						}
					}
			 });	

}


function js_Change_Location(locationCode){
	$('#newLocationCode').val(locationCode);
	$('#form1').submit();
}

</script>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>


	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td width="13" >&nbsp;</td>
		<td class="main_content">
		
	      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="form_table">			
			<tr>
				<td valign="bottom" nowrap="nowrap">
				    <span class="sectiontitle" ><?=$linterface->GET_NAVIGATION2($thislocationName)?></span> 
                </td>
                <td nowrap="nowrap">
				    <?=$toolbar ?> 								    
				    <?=$locationSelectionLayer?>
				</td>
				<td valign="bottom">
				    <div align="right"><?=$Lang['libms']['bookmanagement']['EnterOrScanItemBarcode'] ?> 
				    <input name="" type="text" id='input_code_text' class="input_code_text" />
				    </div>
				</td>
			</tr>		
			<tr><td><p class="spacer"></p></td></tr>
			<tr>
				<td colspan="2" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang["libms"]["report"]["total"]?></td>
				<td valign="top" nowrap="nowrap" class="tabletext" width="70%"><span id="span_book_total"><?=$locationCode?($locationCode == -1?$otherLocationBookAmount:(int)$uniqueBookLocationCodeAssocArr[$locationCode]['UniqueBookAmount']):$thisOverallBookUniqueAmount?></span></td>
			</tr>
			<tr>
				<td colspan="2" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['libms']['stocktake_status']['FOUND']?> / <?=$Lang['libms']['stocktake_status']['WRITEOFF']?></td>
				<td valign="top" nowrap="nowrap" class="tabletext" width="70%"><span id="span_book_found"><?
				$numOfthisStocktakeLog=count($libms->GET_STOCKTAKE_LOG('', $stocktakeSchemeID, '', '', $locationCode, 1));
				$thisWriteoffLogInfo = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID, '', '', 'Array','','','',$locationCode,false);
				$numOfthisStocktakeLog += count($thisWriteoffLogInfo);
				echo $numOfthisStocktakeLog;
				?></span></td>
			</tr>
			<tr>
				<td colspan="2" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['libms']['bookmanagement']['progress']?></td>
				<td valign="top" nowrap="nowrap" class="tabletext" width="70%"><span id="span_book_progress"><?echo ($locationCode == -1?$otherLocationBookAmount:$uniqueBookLocationCodeAssocArr[$locationCode]['UniqueBookAmount'])==0?'0.00%':number_format($numOfthisStocktakeLog/($locationCode?($locationCode == -1?$otherLocationBookAmount:$uniqueBookLocationCodeAssocArr[$locationCode]['UniqueBookAmount']):$thisOverallBookUniqueAmount) * 100,2) . '%'?></span></td>
			</tr>
			<!--<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['libms']['stocktake_status']['NOTTAKE']?></td>
				<td valign="top" nowrap="nowrap" class="tabletext" width="70%"><span id="span_book_nottake"></span></td>
			</tr>-->
			<tr><td><p class="spacer"></p></td></tr>
			<tr>
				<td colspan="3"><?=$htmlAry['StockTakeList'] ?>
				</td>
			</tr>
			</table>		

	</table>


<?php
echo $linterface->FOCUS_ON_LOAD("form1.input_code_text"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>