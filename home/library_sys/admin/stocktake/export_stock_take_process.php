<?php
// using:    
/*
 * 	2016-11-24 Henry
 * 		- add call number column [Case#L96769]
 * 
 * 	2016-09-29 Cameron
 * 		- create this file
 */

@SET_TIME_LIMIT(216000);
ini_set('memory_limit',-1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();


# in all book management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['stock-take'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


# POST/ GET VALUE
$StocktakeSchemeID = $_POST['StocktakeSchemeID']?$_POST['StocktakeSchemeID']:$_GET['StocktakeSchemeID'];
$sql = $libms->GET_STOCKTAKE_SCHEME_SQL($StocktakeSchemeID,1);
$stocktakeSchemeArr = $libms->returnResultSet($sql);
$RecordEndDate = count($stocktakeSchemeArr) > 0 ? $stocktakeSchemeArr[0]['RecordEndDate'] : '';

## Book Status
$BookItemStatus = $_POST['selectedStocktakeStatus']?$_POST['selectedStocktakeStatus']:$_GET['selectedStocktakeStatus'];

$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;

if($BookItemStatus=='BORROWED') {
	$field_array = array("lbu.ACNO","lbu.BarCode","CallNumber","lb.BookTitle","LocationName", "lbu.RecordStatus", "Result", "Borrower", "DateModified", "UserName");
}
else {
	$field_array = array("lbu.ACNO","lbu.BarCode","CallNumber","lb.BookTitle","LocationName", "lbu.RecordStatus", "Result", "DateModified", "UserName");
}
$nr_field = count($field_array);
$sql_order_by = " ORDER BY ".$field_array[$field] . (($order==1) ? " ASC" : " DESC");

$excludeUniqueBookIDArr = array();
$stocktakeUniqueBookArr = $libms->GET_STOCKTAKE_LOG_BOOK_INFO('',$StocktakeSchemeID,'Array');
$stocktakeUniqueBookIDAssoc = array_keys(BuildMultiKeyAssoc($stocktakeUniqueBookArr, 'UniqueID'));
$writeOffUniqueBookArr = $libms->GET_WRITEOFF_LOG_INFO($StocktakeSchemeID, '', '','Array');	
$writeOffUniqueBookIDAssoc = array_keys(BuildMultiKeyAssoc($writeOffUniqueBookArr, 'UniqueID'));
$excludeUniqueBookIDArr = array_merge($stocktakeUniqueBookIDAssoc,$writeOffUniqueBookIDAssoc);
$sql = $libms->GET_NOT_STOCKTAKE_BOOK_INFO($excludeUniqueBookIDArr,$keyword,'','','','sql',false,'',$RecordEndDate,$BookItemStatus,0,($BookItemStatus=='BORROWED'?true:false), true);		

$sql .= $sql_order_by;

$exportColumn = array();
$exportColumn[0][] = $Lang["libms"]["report"]["ACNO"];
$exportColumn[0][] = $Lang['libms']['bookmanagement']['barcode'];
$exportColumn[0][] = $Lang['libms']['book']['call_number'];
$exportColumn[0][] = $Lang['libms']['bookmanagement']['bookTitle'];
$exportColumn[0][] = $Lang['libms']['bookmanagement']['location'];
$exportColumn[0][] = $Lang['libms']['bookmanagement']['bookStatus'];
$exportColumn[0][] = $Lang['libms']['bookmanagement']['StocktakeStatus'];
if($BookItemStatus=='BORROWED') {
	$exportColumn[0][] = $Lang["libms"]["book"]["user_name"];
}
$exportColumn[0][] = $Lang['libms']['bookmanagement']['lastStockTake'];	
$exportColumn[0][] = $Lang['libms']['bookmanagement']['lastTakenBy'];


$limit_upper = 2500;	// reduce the memory usage in each loop
$limit_i = 0;
$max_count = 800;
$retrieve_done = false;
$rows = array();
$export_content = "";

$lexport = new libexporttext();

while (!$retrieve_done && $limit_i<$max_count)
{
	$sql_limit_start = $limit_i * $limit_upper;
	
	$rows = $libms->returnArray($sql . " limit {$sql_limit_start}, {$limit_upper}", null, 2);	// return numeric array
	$limit_i ++;
	$retrieve_done = (sizeof($rows)<$limit_upper);	


	$result_data = array();
	for ($i=0; $i<sizeof($rows); $i++)
	{
		$tmpRow = array();
		for ($j=0;$j<$nr_field;$j++) {		// retrieved fields are more than expected fields, get only the requested fields
			 $tmpRow[] = $rows[$i][$j];
		}				
		$result_data[] = $tmpRow;
		unset($tmpRow);
	}
	unset($rows);
	$rows = array();
	$export_content .= ($export_content?"\r\n":"").$lexport->GET_EXPORT_TXT($result_data, $exportColumn, "\t", "\r\n", "\t", 0, '11');
		
	$exportColumn = array();	// pass column header for the 1st time only
}


intranet_closedb();

$filename = "list_".(strtolower($selectedStocktakeStatus))."_".date("Ymd").".csv";

$lexport->EXPORT_FILE($filename, $export_content, false, false, "UTF-8");
?>