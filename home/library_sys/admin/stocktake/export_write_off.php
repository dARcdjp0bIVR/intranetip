<?php
// using:
/*
 * Date: 2018-04-26 Cameron
 * add parameter $getAccountDate to $libms->GET_STOCKTAKE_LOG_BOOK_INFO() to get AccountDate
 * show Account Date when stock take status is 'FOUND' or 'NOTTAKE' [case #E89629]
 *
 * Date: 2018-03-13 Henry
 * bug fixed: fail to show the worng not stocktake item [Case#X136844]
 *
 * 2016-05-06 Cameron
 * - create this file
 */
@SET_TIME_LIMIT(216000);
ini_set('memory_limit', - 1);

$PATH_WRT_ROOT = "../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

intranet_auth();
intranet_opendb();

// in all book management PHP pages
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (! $admin_right['write-off'] && ! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// POST/ GET VALUE
$StocktakeSchemeID = $_POST['StocktakeSchemeID'] ? $_POST['StocktakeSchemeID'] : $_GET['StocktakeSchemeID'];
$selectedStocktakeStatus = $_POST['selectedStocktakeStatus'] ? $_POST['selectedStocktakeStatus'] : $_GET['selectedStocktakeStatus'];
$BookItemStatus = $_POST['BookItemStatus'] ? $_POST['BookItemStatus'] : $_GET['BookItemStatus'];

if ($StocktakeSchemeID == '' || $selectedStocktakeStatus == '') {
    $selectedStocktakeStatus = 'WRITEOFF';
    $StocktakeSchemeID = - 1;
}

if ($selectedStocktakeStatus == 'WRITEOFF') {
    $stockTakeSchemeSelectionArr[] = array(
        '-1',
        '-- ' . $Lang["libms"]["report"]["fine_status_all"] . ' --'
    );
} else if ($StocktakeSchemeID == - 1) {
    $sql = $libms->GET_STOCKTAKE_SCHEME_SQL('', 1);
    $stockTakeSchemeArr = $libms->returnArray($sql);
    if (count($stockTakeSchemeArr) > 0) {
        $StocktakeSchemeID = $stockTakeSchemeArr[0]['StocktakeSchemeID']; // use the first stocktake SchemeID as default, case #Y88830
    }
}

if ($StocktakeSchemeID != - 1) {
    $sql = $libms->GET_STOCKTAKE_SCHEME_SQL($StocktakeSchemeID, 1);
    $stockTakeSchemeArr = $libms->returnArray($sql);
}

$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;

if ($selectedStocktakeStatus == 'WRITEOFF') {
    $field_array = array(
        "lbu.ACNO",
        "lbu.BarCode",
        "lb.BookTitle",
        "LocationName",
        "lbu.RecordStatus",
        "Result",
        "WriteOffDate",
        "Reason",
        "DateModified",
        "UserName"
    );
} else {
    $field_array = array(
        "lbu.ACNO",
        "AccountDate",
        "lbu.BarCode",
        "lb.BookTitle",
        "LocationName",
        "lbu.RecordStatus",
        "Result",
        "DateModified",
        "UserName"
    );
}
$nr_field = count($field_array);
$sql_order_by = " ORDER BY " . $field_array[$field] . (($order == 1) ? " ASC" : " DESC");

$showAccountDate = false;
if ($selectedStocktakeStatus == 'FOUND') {
    $sql = $libms->GET_STOCKTAKE_LOG_BOOK_INFO('', $StocktakeSchemeID, 'sql', $keyword, '', '', '', false, '', 0, $BookItemStatus, $getAccountDate = true);
    $showAccountDate = true;
} elseif ($selectedStocktakeStatus == 'WRITEOFF') {
    $sql = $libms->GET_WRITEOFF_LOG_INFO(($StocktakeSchemeID == - 1 ? '' : $StocktakeSchemeID), '', '', 'sql', $keyword, '', '', '', false, '', '', '', '', 0, $BookItemStatus);
} elseif ($selectedStocktakeStatus == 'NOTTAKE') {
    $excludeUniqueBookIDArr = array();
    $stocktakeUniqueBookArr = $libms->GET_STOCKTAKE_LOG_BOOK_INFO('', $StocktakeSchemeID, 'Array');
    $stocktakeUniqueBookIDAssoc = array_keys(BuildMultiKeyAssoc($stocktakeUniqueBookArr, 'UniqueID'));
    $writeOffUniqueBookArr = $libms->GET_WRITEOFF_LOG_INFO($StocktakeSchemeID, '', '', 'Array');
    $writeOffUniqueBookIDAssoc = array_keys(BuildMultiKeyAssoc($writeOffUniqueBookArr, 'UniqueID'));
    $excludeUniqueBookIDArr = array_merge($stocktakeUniqueBookIDAssoc, $writeOffUniqueBookIDAssoc);
    $sql = $libms->GET_NOT_STOCKTAKE_BOOK_INFO($excludeUniqueBookIDArr, $keyword, '', '', '', 'sql', false, '', $stocktakeSchemeArr[0]['RecordEndDate'], $BookItemStatus, 0, false, false, $getAccountDate = true);
    $showAccountDate = true;
}

$sql .= $sql_order_by;

$exportColumn = array();
$exportColumn[0][] = $Lang["libms"]["report"]["ACNO"];
if ($showAccountDate) {
    $exportColumn[0][] = $Lang['libms']['book']['account_date'];
}
$exportColumn[0][] = $Lang['libms']['bookmanagement']['barcode'];
$exportColumn[0][] = $Lang['libms']['bookmanagement']['bookTitle'];
$exportColumn[0][] = $Lang['libms']['bookmanagement']['location'];
$exportColumn[0][] = $Lang['libms']['bookmanagement']['bookStatus'];
$exportColumn[0][] = $Lang['libms']['bookmanagement']['StocktakeStatus'];
if ($selectedStocktakeStatus == 'WRITEOFF') {
    $exportColumn[0][] = $Lang['libms']['bookmanagement']['WriteOffDate'];
    $exportColumn[0][] = $Lang['libms']['bookmanagement']['Reason'];
    $exportColumn[0][] = $Lang['libms']['bookmanagement']['WriteOffTime'];
    $exportColumn[0][] = $Lang['libms']['bookmanagement']['WriteOffBy'];
} else {
    $exportColumn[0][] = $Lang['libms']['bookmanagement']['lastStockTake'];
    $exportColumn[0][] = $Lang['libms']['bookmanagement']['lastTakenBy'];
}

$limit_upper = 2500; // reduce the memory usage in each loop
$limit_i = 0;
$max_count = 800;
$retrieve_done = false;
$rows = array();
$export_content = "";

$lexport = new libexporttext();

while (! $retrieve_done && $limit_i < $max_count) {
    $sql_limit_start = $limit_i * $limit_upper;
    
    $rows = $libms->returnArray($sql . " limit {$sql_limit_start}, {$limit_upper}", null, 2); // return numeric array
    
    $limit_i ++;
    $retrieve_done = (sizeof($rows) < $limit_upper);
    
    $result_data = array();
    for ($i = 0; $i < sizeof($rows); $i ++) {
        $tmpRow = array();
        for ($j = 0; $j < $nr_field; $j ++) { // retrieved fields are more than expected fields, get only the requested fields
            $tmpRow[] = $rows[$i][$j];
        }
        $result_data[] = $tmpRow;
        unset($tmpRow);
    }
    unset($rows);
    $rows = array();
    
    $export_content .= ($export_content ? "\r\n" : "") . $lexport->GET_EXPORT_TXT($result_data, $exportColumn, "\t", "\r\n", "\t", 0, '11');
    
    $exportColumn = array(); // pass column header for the 1st time only
}

intranet_closedb();

$filename = "list_" . (strtolower($selectedStocktakeStatus)) . "_" . date("Ymd") . ".csv";

$lexport->EXPORT_FILE($filename, $export_content, false, false, "UTF-8");
?>