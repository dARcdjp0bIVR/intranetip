<?php
// Using:
/*************************************************************
 * 
 * 	2016-08-04 (Cameron)
 * 			Change $Lang['libms']['stocktake_status']['BookFound'] to $Lang['libms']['stocktake_status_bookfound']['BookFound']
 * 			to exclude this status in selection option list
 * 
 *  2016-08-03 (Cameron) fix changes on 2016-07-04
 * 			Change text color to green and status to 'Book Found' if there's stock take record in LIBMS_STOCKTAKE_LOG
 * 			keep status as 'Found' and don't change row color for adding new record to LIBMS_STOCKTAKE_LOG [case #X97779]
 * 
 *  2016-07-04 (Cameron) <- cancelled
 * 			Change text color to green for found book, change status from 'Found' to 'Book Found' 
 * 
 *  20150708 (Henry)
 * 			Modified to show the CurrentRecordStatus
 *	20150605 (Henry)
 * 			Add warning for the non-stocktake book
 * 
 *	20150526 (Henry)
 * 			Add warning for the write-off book
 * 
 *	2015-05-21 (Henry)
 * 			- fix: showing the wrong stocktake date [Case#E78828]
 * 
 *************************************************************/
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$linterface = new interface_html();
$barcode = $_POST['barcode'];
$ID = $_POST['ID'];
$stocktakeSchemeID = $_POST['stocktakeSchemeID'];
$pageLocationCode = $_POST['pageLocationCode'];

#Get Location Name
$locationInfo = $libms->GET_LOCATION_INFO('',1);
$locationCodeAssoArr = BuildMultiKeyAssoc($locationInfo, 'LocationCode');
if($pageLocationCode){
	$locationEngName = $locationCodeAssoArr[$pageLocationCode]['DescriptionEn'];
	$locationChiName = $locationCodeAssoArr[$pageLocationCode]['DescriptionChi'];
	$thislocationName = Get_Lang_Selection($locationChiName,$locationEngName);
}else{
	$thislocationName = $Lang['libms']['bookmanagement']['overall'];
}

$returnBookInfo = $libms->GET_STOCKTAKE_LOG_BOOK_INFO($barcode);

if($returnBookInfo){
	### Add Stocktake Log 

	$thisUniqueBookID = $returnBookInfo[0]['UniqueID'];
	$thisBookID = $returnBookInfo[0]['BookID'];
	$ACNO = $returnBookInfo[0]['ACNO'];
	
	$thisRecordStatus = $returnBookInfo[0]['CurrentRecordStatus'];
	
	# Unique Book Status

	$thisRecordStatusDisplay = '';
	if($thisRecordStatus==$Lang['libms']['book_status']['BORROWED']){
		$borrowInfo = $libms->GET_BORROW_LOG($thisBookID,$thisUniqueBookID);
		$borrowUserName =  $borrowInfo[0]['UserName']?$borrowInfo[0]['UserName']:$Lang['General']['EmptySymbol'];
		$borrowDueDate = $borrowInfo[0]['DueDate']; 
		
		$thisRecordStatusDisplay = $Lang["libms"]["CirculationManagement"]["borrowed_by"] . ':' . $borrowUserName . '<br />' . $borrowDueDate;
	}else{	
		$thisRecordStatusDisplay = $thisRecordStatus;
	}
	
	$thisBookTitle = $returnBookInfo[0]['BookTitle']?$returnBookInfo[0]['BookTitle']:$Lang['General']['EmptySymbol'];
	$thisOriginalLocation = $returnBookInfo[0]['LocationName']?$returnBookInfo[0]['LocationName']:$Lang['General']['EmptySymbol'];
	## Check If Has Stocktake Log Already
	$excitingRecord = $libms->GET_STOCKTAKE_LOG('',$stocktakeSchemeID, $thisUniqueBookID, $thisBookID);
	
	### Get stocktake record end date
	$sql = $libms->GET_STOCKTAKE_SCHEME_SQL($stocktakeSchemeID);
	$stocktake_scheme = current($libms->returnArray($sql));
	$RecordEndDate = $stocktake_scheme['RecordEndDate'];
	
	if($thisRecordStatus==$Lang["libms"]["book_status"]["WRITEOFF"]){
		//write-off book will not stocktake
	}
	else if($returnBookInfo[0]['CreationDate'] && strtotime($returnBookInfo[0]['CreationDate']) > strtotime($RecordEndDate.' 23:59:59')){
		echo "nonstocktakebook";
		exit();
	}
	else if(!$excitingRecord){ // Add New
		$stocktakeResult = $libms->ADD_STOCKTAKE_LOG($stocktakeSchemeID,$thisBookID, $thisUniqueBookID, 'FOUND');
	}else // Update Existing One
	{
		$stocktakeLogID = $excitingRecord[0]['StocktakeLogID'];
		$stocktakeResult = $libms->UPDATE_STOCKTAKE_LOG_RECORD($stocktakeLogID,$stocktakeSchemeID,$thisBookID, $thisUniqueBookID, 'FOUND');
	}
	
	if($stocktakeResult){
		$thisStocktakeResult = $excitingRecord ? $Lang['libms']['stocktake_status_bookfound']['BookFound'] : $Lang['libms']['stocktake_status']['FOUND'];
	}
	
	#Get new stock-take time
	$newreturnBookInfo = $libms->GET_STOCKTAKE_LOG_BOOK_INFO($barcode,$stocktakeSchemeID);
	$StocktakeTime = $newreturnBookInfo[0]['DateModified'];
	

	
	## Check If Has Write Off Log Already
	$writeOffInfoArr = $libms->GET_WRITEOFF_LOG_INFO($stocktakeSchemeID, $thisBookID, $thisUniqueBookID, $returnType='Array');

	$thisWriteOffID = $writeOffInfoArr[0]['WriteOffID'];		
	if($thisWriteOffID!='' || $thisRecordStatus==$Lang["libms"]["book_status"]["WRITEOFF"]){
		echo "writeoffbook";
		exit(); // disallow wirte-off book to stock-take
		$thisUpdateWriteOffRecord = $libms->DELETE_WRITEOFF_LOG_RECORD($thisWriteOffID);
	}
	
	
	$thisOriginalLocationCode = $returnBookInfo[0]['LocationCode'];

	if($thisOriginalLocationCode!=$pageLocationCode && $pageLocationCode && $pageLocationCode!=-1){
		## Relocate Selection Box
		# Stock Take Schedule Selection Box

		$locationSelectionBox = '<input type="button" value="'.$Lang['libms']['bookmanagement']['relocate'].'" onclick="js_change_item_location_warning('.$thisBookID.', '.$thisUniqueBookID.', \'locationSelection_'.$ID.'\');">';
	}else
	{
		$locationSelectionBox = $Lang['General']['EmptySymbol'];
	}

	
	#	Barcode	Book Title	Book Status	Result	Original Location Relocate
	## Return Table
	$returnTable = '';
	$returnTable .= "<tr class='returned' ".($excitingRecord ? "style='color:#00b33c;'" : "").">";
	$returnTable .= "<td width='2%'>".$ID."</td>";
	$returnTable .= "<td width='10%'>".$ACNO."</td>";
	$returnTable .= "<td width='10%'>".$barcode."</td>";
	$returnTable .= "<td width='18%'>".$thisBookTitle."</td>";
	$returnTable .= "<td width='10%'>".$thisRecordStatusDisplay."</td>";	
	$returnTable .= "<td width='10%'>".$thisStocktakeResult."</td>";
	if($thisOriginalLocationCode!=$pageLocationCode && $pageLocationCode  && $pageLocationCode!=-1)$returnTable .= "<td width='10%' style='color:red;'>".$thisOriginalLocation."</td>";
	else $returnTable .= "<td width='10%'>".$thisOriginalLocation."</td>";
	$returnTable .= "<td width='12%'>".$StocktakeTime."</td>";	
	$returnTable .= "<td width='18%'><span id='locationSelection_$ID'>".$locationSelectionBox."</span></td>";
	$returnTable .= "</tr>";
	
	echo $returnTable;
?>
<script>
function js_change_item_location_warning(bookID, bookUniqueID, locationSelectionBoxID){
	 if (confirm("<?=$Lang['libms']['bookmanagement']['ChangeItemLocationWarning']?><?=$thislocationName?>?")) {  
	 	
            js_Change_Item_Location(bookID, bookUniqueID, locationSelectionBoxID);  
            
        }  
	
}
</script>
<?
}
?>

