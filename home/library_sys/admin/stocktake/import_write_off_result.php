<?php
// using: 

/*************************************************************
 * 
 * 	2016-04-26 (Cameron) 
 * 			add charset=UTF-8 in meta data before calling NO_ACCESS_RIGHT_REDIRECT
 * 
 * 	2016-04-25 (Cameron) 
 * 			change admin access right from "stock-take and write-off" to "write-off" [Case#K94933]
 * 
 *  20150610 (Henry)
 * 			fix: cannot access when the group only has stock-take and write-off permission [Case#X79521]
 * 
 *	2015-05-28 (Henry)
 * 			fix: cannot show write-off reason correctly
 *	2015-01-23 (Henry)
 * 			call UPDATE_BOOK_COPY_INFO() after writeoff the book
 *	2013-11-26 (Henry)
 * 			Created this file
 * 
 *************************************************************/
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['write-off'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
//$PAGE_NAVIGATION[] = array($Lang["libms"]["book_status"]["WRITEOFF"], "write_off.php");
$PAGE_NAVIGATION[] = array($Lang["libms"]["bookmanagement"]["import"], "");
$CurrentPage = "PageWriteOff";

$linterface = new interface_html("libms.html");

# Top menu highlight setting
//$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['stock-take'], "index.php", 0);
$TAGS_OBJ[] = array($Lang["libms"]["book_status"]["WRITEOFF"], "write_off.php", 1);
//$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_import']);


# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='write_off.php'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import_write_off.php'");



//===================================================================================

### Function [Start] ###
function get_left_over_from_tmp(){
	global $libms;
	$result = $libms->returnArray("select count(*) as leftover from LIBMS_WRITEOFF_LOG_TMP");
	return $result[0]['leftover'];
}
### Function [End] ###

$libel = new elibrary();
if (isset($junior_mck))
{
	$libel->dbConnectionLatin1();	
}

$counter = 0;

while (get_left_over_from_tmp() > 0){
	$error_happen = false;
	$result = $libms->returnArray("select * from LIBMS_WRITEOFF_LOG_TMP order by WriteOffID limit 1000 ");
	
	foreach ($result as $stocktake){
		$insert_to_db = array(
					'StocktakeSchemeID'=> CCTOSQL($stocktake['StocktakeSchemeID']),
					'BookID'=> CCTOSQL($stocktake['BookID']),
					'BookUniqueID'=> CCTOSQL($stocktake['BookUniqueID']),
					'Result'=> CCTOSQL($stocktake['Result']),
					'PreviousBookStatus'=> CCTOSQL($stocktake['PreviousBookStatus']),
					'Reason' => CCTOSQL($stocktake['Reason']),
					'WriteOffDate' => CCTOSQL($stocktake['WriteOffDate'])
			);
			
			$result_insert_STOCKTAKE = $libms->INSERT2TABLE('LIBMS_WRITEOFF_LOG', $insert_to_db);
			if($result_insert_STOCKTAKE){
				$result_updateUniqueBook = $libms->UPDATE_UNIQUE_BOOK($stocktake['BookUniqueID'],'WRITEOFF');
				$libms->UPDATE_BOOK_COPY_INFO($stocktake['BookID']);
				if (isset($junior_mck))
				{
					$libel->dbConnectionLatin1();	
				}
			}
			if($result_insert_STOCKTAKE && $result_updateUniqueBook){
				$counter++;
			}
			
	} //for each 1000
	$libms->db_db_query("delete from `LIBMS_WRITEOFF_LOG_TMP` order by WriteOffID limit 1000 ");

} //LOOP until all gone.
$libms->db_db_query("DROP TABLE LIBMS_WRITEOFF_LOG_TMP");

$lang_space = (($_SESSION['intranet_session_language'] == 'en') ? " " : "") ? " " : "";

$x = $Lang['libms']['import_book']['Result']['Success'].",";
$x .= $lang_space.$Lang['libms']['import_book']['total_record'].":";
$x .= $counter;
$x .= $lang_space.$Lang['libms']['bookmanagement']['writeoffRecord'];
$i = 1;



if($is_debug) die();


$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
<form name="form1" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center"><br /> <br /> <br /> <?= $x ?></td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5"
					align="center">
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td align="center" colspan="2"><?=$import_button?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>