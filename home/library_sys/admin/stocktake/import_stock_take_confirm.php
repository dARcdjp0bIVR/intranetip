<?php
// using:

/**
 * ***********************************************************
 *
 * 20180511 (Cameron)
 * - check record duplicate for the same import
 * 
 * 20180508 (Cameron)
 * - show error in one line, separated by ", "
 * 
 * 20180504 (Cameron)
 * - allow to import if there's any valid record [case #R97847]
 * - add export invalid record function
 * - disable submit button after submit
 *
 * 20170112 (Cameron)
 * add cookie to store fileFormat in previous action
 *
 * 20160426 (Cameron)
 * add charset=UTF-8 in meta data before calling NO_ACCESS_RIGHT_REDIRECT
 *
 * 20160425 (Cameron)
 * change admin access right from "stock-take and write-off" to "stock-take" [Case#K94933]
 *
 * 20150729 (Henry)
 * add record status to stocktake table
 *
 * 20150610 (Henry)
 * fix: cannot access when the group only has stock-take and write-off permission [Case#X79521]
 *
 * 20150605 (Henry)
 * Add warning for the non-stocktake book
 *
 * 20150526 (Henry)
 * Add warning for the write-off book
 *
 * 20131125 (Henry)
 * Created this file
 *
 * ***********************************************************
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libimporttext.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/liblibrarymgmt.php");

include_once ($PATH_WRT_ROOT . "includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (! $admin_right['stock-take'] && ! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// ## Set Cookies
$arrCookies = array();
$arrCookies[] = array(
    "elibrary_plus_mgmt_stock_take_fileFormat",
    "fileFormat"
);
updateGetCookies($arrCookies);

$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
// $PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['stock-take'], "index.php");
$PAGE_NAVIGATION[] = array(
    $Lang["libms"]["bookmanagement"]["import"],
    ""
);
$CurrentPage = "PageStockTake";

$linterface = new interface_html("libms.html");

// Top menu highlight setting
$TAGS_OBJ[] = array(
    $Lang['libms']['bookmanagement']['stock-take'],
    "index.php",
    1
);
// TAGS_OBJ[] = array($Lang["libms"]["book_status"]["WRITEOFF"], "write_off.php", 0);
// $TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['book_import']);

// step information
$STEPS_OBJ[] = array(
    $Lang["libms"]["general"]["selectfile"],
    0
);
$STEPS_OBJ[] = array(
    $iDiscipline['Confirmation'],
    1
);
$STEPS_OBJ[] = array(
    $i_general_imported_result,
    0
);

$filepath = $file;
$filename = $file_name;

$uploadSuccess = true;
if ($filepath == "none" || $filepath == "" || ! $limport->CHECK_FILE_EXT($filename)) {
    $uploadSuccess = false;
    $xmsg2 = $Lang['libms']['import_book']['upload_fail'] . " " . $Lang['plupload']['invalid_file_ext'];
}

if ($uploadSuccess) {
    
    $file_format = array(
        'Barcode',
        'NewLocationCode'
    );
    
    $flagAry = array(
        '1',
        '1'
    );
    $format_wrong = false;
    
    $data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($filepath, "", "", $file_format, $flagAry);
    
    $counter = 1;
    $insert_array = array();
    $my_key = array();
    
    if (is_array($data)) {
        $col_name = array_shift($data);
    }
    
    for ($i = 0; $i < sizeof($file_format); $i ++) {
        if (strtoupper($col_name[$i]) != strtoupper($file_format[$i])) {
            $format_wrong = true;
            break;
        }
    }
    
    if ($format_wrong) {
        header("location: import_stock_take.php?xmsg=wrong_header");
        exit();
    }
    if (sizeof($data) == 0) {
        header("location: import_stock_take.php?xmsg=import_no_record");
        exit();
    }
    
    // create a tmp table [start]
    $sql = "DROP TABLE LIBMS_STOCKTAKE_LOG_TMP";
    $libms->db_db_query($sql);
    $sql = "CREATE TABLE IF NOT EXISTS `LIBMS_STOCKTAKE_LOG_TMP` (
	  `StocktakeLogID` int(11) NOT NULL auto_increment,
	  `StocktakeSchemeID` int(11) NOT NULL,
	  `BookID` int(11) NOT NULL,
	  `BookUniqueID` int(11) NOT NULL,
	  `Result` varchar(64) default NULL,
	  `RecordStatus` varchar(16) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY (StocktakeLogID),
	  index BookUniqueID (BookUniqueID),
  		UNIQUE KEY `StocktakeLog` (`StocktakeSchemeID`, `BookID`, `BookUniqueID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    $libms->db_db_query($sql);
    // create a tmp table [end]
    
    // create a error tmp table [start]
    $sql = "DROP TABLE LIBMS_STOCKTAKE_LOG_ERROR_TMP";
    $libms->db_db_query($sql);
    $sql = "CREATE TABLE IF NOT EXISTS `LIBMS_STOCKTAKE_LOG_ERROR_TMP` (
	  `RecordID` int(11) NOT NULL auto_increment,
	  `Barcode` varchar(128) default NULL,
      `LocationCode` varchar(64) default NULL,
      `Error` varchar(255) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY (RecordID),
	  index Barcode (Barcode)  		
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    $libms->db_db_query($sql);
    // create a error tmp table [end]
    
    // ## List out the import result
    $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
    $x .= "<tr>";
    $x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
    $x .= "<td class=\"tablebluetop tabletopnolink\">" . $Lang['libms']['bookmanagement']['barcode'] . "</td>";
    $x .= "<td class=\"tablebluetop tabletopnolink\">" . $Lang['libms']['bookmanagement']['newLocationCode'] . "</td>";
    $x .= "<td class=\"tablebluetop tabletopnolink\">" . $Lang['General']['Warning'] . "</td>";
    $x .= "</tr>";
    
    // ## Get stocktake record end date
    $sql = $libms->GET_STOCKTAKE_SCHEME_SQL($StocktakeSchemeID);
    $stocktake_scheme = current($libms->returnArray($sql));
    $RecordEndDate = $stocktake_scheme['RecordEndDate'];
    
    $warning_occured = false;
    $hiddenField = "";
    $y = 0;
    $invalidRecordAry = array();
    foreach ($data as $record) {
        $isAllowedToAdd = true;
        $barCode = $record['0'];
        $locationCode = trim($record['1']);
        
        $errorAry = array();
        $sql = "Select BookID, UniqueID, RecordStatus, CreationDate From LIBMS_BOOK_UNIQUE WHERE BarCode = '" . $barCode. "'";
        $result = current($libms->returnArray($sql));
        
        if (! $result) {
            $isAllowedToAdd = false;
            $warning_occured = true;
            $errorAry[] = $Lang["libms"]["bookmanagement"]["BarcodeNotExist"];
        }
        
        if ($result['RecordStatus'] == 'WRITEOFF') {
            $isAllowedToAdd = false;
            $warning_occured = true;
            $errorAry[] = $Lang['libms']['bookmanagement']['IsWriteOffBook'];
        }
        
        if ($result['CreationDate'] && strtotime($result['CreationDate']) > strtotime($RecordEndDate . ' 23:59:59')) {
            $isAllowedToAdd = false;
            $warning_occured = true;
            $errorAry[] = $Lang['libms']['bookmanagement']['ItemNotForThisStocktake'];
        } 
        
        // location code is specified, need to check if it's in db
        if ($locationCode != "") {
            $sql = "Select LocationCode From LIBMS_LOCATION WHERE LocationCode = '" . $locationCode . "' OR DescriptionEn = '" . $locationCode . "' OR DescriptionChi = '" . $locationCode . "'";
            $result1 = current($libms->returnArray($sql));
            if (! $result1) {
                $isAllowedToAdd = false;
                $warning_occured = true;
                $errorAry[] = $Lang["libms"]["import_book"]["BookLocationNotExist"];

            } else {
                // update the location of the book
                $hiddenField .= '<input type="hidden" name="book_unique_id[]" value="' . $result['UniqueID'] . '"/>';
                $hiddenField .= '<input type="hidden" name="location_code[]" value="' . $result1['LocationCode'] . '"/>';
            }
        }
        
        // check if the record has been imported before
        if ($result['UniqueID']) {
            $sql = "SELECT BookUniqueID FROM LIBMS_STOCKTAKE_LOG_TMP WHERE BookUniqueID='".$result['UniqueID']."'";
            $result1 = $libms->returnArray($sql);
            // already exist
            if (count($result1)) {
                $isAllowedToAdd = false;
                $warning_occured = true;
                $errorAry[] = $Lang["libms"]["bookmanagement"]["BarcodeDuplicate"];
            }
        }

        if (count($errorAry)) {
            $error = implode(', ',$errorAry);
            $invalidRecordAry = array(
                'Barcode' => CCTOSQL($barCode),
                'LocationCode' => CCTOSQL($locationCode),
                'Error' => CCTOSQL($error)
            );
            
            $x .= "<tr class=\"tablebluerow" . ($y % 2 + 1) . "\">";
            $x .= "<td class=\"$css\">" . ($y + 1) . "</td>";
            $x .= "<td class=\"$css\">" . $barCode. "</td>";
            $x .= "<td class=\"$css\">" . $locationCode. "</td>";
            $x .= "<td class=\"$css\">";
            $x .= "<div style='color:red'>" . $error . "</div>";
            $x .= "</td>";
            $x .= "</tr>";
            $y ++;
        }
        else if ($result['RecordStatus'] == 'LOST') { 
            // allow to add
            $warning_occured = true;
            $x .= "<tr class=\"tablebluerow" . ($y % 2 + 1) . "\">";
            $x .= "<td class=\"$css\">" . ($y + 1) . "</td>";
            $x .= "<td class=\"$css\">" . $barCode. "</td>";
            $x .= "<td class=\"$css\">" . $locationCode. "</td>";
            $x .= "<td class=\"$css\">";
            $x .= "<div style='color:green'>" . $Lang['libms']['bookmanagement']['bookStatusIsLost'] . "</div>";
            $x .= "</td>";
            $x .= "</tr>";
            $y ++;
        }
        
        if ($isAllowedToAdd) { // no error, add to tmp table for next step: real import
            $insert_to_db = array(
                'StocktakeSchemeID' => CCTOSQL($StocktakeSchemeID),
                'BookID' => CCTOSQL($result['BookID']),
                'BookUniqueID' => CCTOSQL($result['UniqueID']),
                'Result' => CCTOSQL("FOUND"),
                'RecordStatus' => CCTOSQL($result['RecordStatus'])
            );
            $result2 = $libms->INSERT2TABLE('LIBMS_STOCKTAKE_LOG_TMP', $insert_to_db);
        } else { // log error
            $result2 = $libms->INSERT2TABLE('LIBMS_STOCKTAKE_LOG_ERROR_TMP', $invalidRecordAry);
        }
        
        // if ($result2 == false) {
        // // capture error
        // }
        
        $insert_to_db = array();
        $invalidRecordAry = array();
    }
}

$sql = "SELECT BookUniqueID
		FROM `LIBMS_STOCKTAKE_LOG_TMP` ";
$result = $libms->returnArray($sql);

// following foreach loop not used ?
// ################################################
$y = 3;

// $error_occured = 0;
foreach ($result as $record) {
    $error = array();
    
    $css = (sizeof($error) == 0) ? "tabletext" : "red";
    
    if (sizeof($error) > 0) {
        
        $itemInfo = current($libms->GET_BOOK_ITEM_INFO($record['BookUniqueID']));
        $bookInfo = current($libms->GET_BOOK_INFO($itemInfo['BookID']));
        $locationInfo = current($libms->GET_LOCATION_INFO($itemInfo['LocationCode']));
        
        $x .= "<tr class=\"tablebluerow" . ($y % 2 + 1) . "\">";
        $x .= "<td class=\"$css\">" . ($y) . "</td>";
        $x .= "<td class=\"$css\">" . $itemInfo['ACNO'] . "</td>";
        $x .= "<td class=\"$css\">" . $itemInfo['BarCode'] . "</td>";
        $x .= "<td class=\"$css\">" . $bookInfo['BookTitle'] . "</td>";
        $x .= "<td class=\"$css\">" . Get_Lang_Selection($locationInfo['DescriptionChi'], $locationInfo['DescriptionEn']) . "</td>";
        $x .= "<td class=\"$css\">" . $Lang["libms"]["book_status"][$itemInfo['RecordStatus']] . "</td>";
        $x .= "<td class=\"$css\">";
        
        if (sizeof($error) > 0) {
            foreach ($error as $Key => $Value) {
                $x .= $Value . '<br/>';
            }
        }
        
        $x .= "</td>";
        $x .= "</tr>";
    } else {}
    $y ++;
}
// ################################################

$x .= "</table>";

$nrValidRecord = count($result);
$nrInvalidRecord = count($data) - $nrValidRecord;
$buttonLayout = '';
if ($nrValidRecord) {
    $buttonLayout .= $linterface->GET_ACTION_BTN($button_import, "button", "goSubmit()", "btnSubmit") . "&nbsp;";
}
if ($nrInvalidRecord) {
    $buttonLayout .= $linterface->GET_ACTION_BTN($Lang['libms']['bookmanagement']['exportInvalidRecord'], "button", "js_export()", "btnExport") . "&nbsp;";
}
$buttonLayout .= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_stock_take.php'");

$prescan_result = $Lang['libms']['import_book']['upload_success_ready_to_import'] . $nrValidRecord;
if ($warning_occured) {
    $x .= '<br/>' . $prescan_result;
    $x .= '<br/>' . $Lang["libms"]["import_book"]["upload_fail_ready_to_import"] . $nrInvalidRecord;
} else {
    $x = $prescan_result;
}

$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<script>
function goSubmit() {
	$('#btnSubmit').attr('disabled', 'disabled');
	$('form#form1').submit();
}

function js_export()
{
	var original_action = document.form1.action;
	var url = "export_stock_take_invalid_record.php";
	document.form1.action = url;
	document.form1.submit();
	document.form1.action = original_action;
}
</script>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
<form name="form1" id="form1" action="import_stock_take_result.php"
	method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5">

		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center">
				<table width="96%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" class="tabletext">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><?=$x?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="95%" border="0" cellpadding="0" cellspacing="0"
					align="center">
					<tr>
						<td colspan="3" class="dotline"><img
							src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10"
							height="1" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2"><?= $buttonLayout?>
			</td>
		</tr>
	</table>
	<?=$hiddenField?>
	<input type="hidden" name="HasLocationCode" value="1" />
</form>
<br />

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>