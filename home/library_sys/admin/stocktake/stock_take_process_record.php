<?php
// Using: Henry

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$linterface = new interface_html();

$stocktakeSchemeID = $_POST['StocktakeSchemeID'];
$BookIDUniqueID = $_POST['BookIDUniqueID'];
$stocktakeStatus = $_POST['selectedStocktakeStatus'];
//$WriteoffReasons = $_POST['WriteoffReasons'];
//$WriteoffDate = $_POST['WriteoffDate'];

$libel = new elibrary();

$result = array();
$numOfBookIDUniqueID = count($BookIDUniqueID);
for($i=0;$i<$numOfBookIDUniqueID;$i++){
	$thisBookIDUniqueID = $BookIDUniqueID[$i];
	$thisBookIDUniqueIDArr = explode('-', $thisBookIDUniqueID);
	$thisBookID = $thisBookIDUniqueIDArr[0];
	$thisUniqueID = $thisBookIDUniqueIDArr[1];
	
	$libms->UPDATE_UNIQUE_BOOK($thisUniqueID, 'SUSPECTEDMISSING');
		$tempArr = $libms->GET_BOOK_ITEM_INFO($thisUniqueID);
		$libms->UPDATE_BOOK_COPY_INFO($tempArr[0]["BookID"]);
		$libel->SYNC_PHYSICAL_BOOK_TO_ELIB($tempArr[0]["BookID"]);
//	## Get Book Status
//	$thisUniqueBookInfo = $libms->GET_UNIQUE_BOOK_STATUS($thisUniqueID,$thisBookID);
//	$thisUniqueBookCurrentStatus = $thisUniqueBookInfo[0]['RecordStatus'];

//debug_pr($thisUniqueBookCurrentStatus);
	
//	debug_pr($thisUniqueBookInfo);
	
//	if($stocktakeStatus!='WRITEOFF'){
//		$thisStocktakeLogID =  $thisBookIDUniqueIDArr[2];
//		$result['WriteOffSuccess'] = $libms->ADD_WRITEOFF_LOG_RECORD($stocktakeSchemeID, $thisBookID, $thisUniqueID, 'WRITEOFF',$thisUniqueBookCurrentStatus,$WriteoffReasons[$i],$WriteoffDate[$i]);
//		$result['UpdateUniqueBookSuccess'] = $libms->UPDATE_UNIQUE_BOOK($thisUniqueID,'WRITEOFF');
//	
//		if($stocktakeStatus=='FOUND' && $thisStocktakeLogID!=''){
//			$result['RemoveStocktakeLogSuccess'] = $libms->DELETE_STOCKTAKE_LOG_RECORD($thisStocktakeLogID);
//		}
		
			
//	}elseif($stocktakeStatus=='WRITEOFF'){
//		$thisWriteOffID =  $thisBookIDUniqueIDArr[2];
//		$previousUniqueBookStatus = '';
//		
//		if($thisWriteOffID!=''){
//			$result['WriteOffSuccess'] = $libms->UPDATE_WRITEOFF_LOG_RECORD($thisWriteOffID, $stocktakeSchemeID, $thisBookID, $thisUniqueID, 'WRITEOFF');
//		}
//	}	

}

header("Location: stock_take_process.php?xmsg=update&StocktakeSchemeID=$stocktakeSchemeID&selectedStocktakeStatus=$stocktakeStatus");

?>