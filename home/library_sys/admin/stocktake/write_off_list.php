<?php
// using: 

/*************************************************************
 * 
 * 	20160426 (Cameron) 
 * 			add charset=UTF-8 in meta data before calling NO_ACCESS_RIGHT_REDIRECT
 *  
 * 	20160425 (Cameron) 
 * 			change admin access right from "stock-take and write-off" to "write-off" [Case#K94933]
 * 
 *  20150610 (Henry)
 * 			fix: cannot access when the group only has stock-take and write-off permission [Case#X79521]
 * 
 *************************************************************/

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['write-off'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


################## Set Cookies Start ###################
$arrCookies = array();
$arrCookies[] = array("elibrary_plus_write_off_list_page_field", "field");
$arrCookies[] = array("elibrary_plus_write_off_list_page_order", "order");
$arrCookies[] = array("elibrary_plus_write_off_list_page_size", "numPerPage");
$arrCookies[] = array("elibrary_plus_write_off_list_page_number", "pageNo");
		
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
{
	updateGetCookies($arrCookies);
}

//$Lang['General']['ReturnMessage'][$xmsg]

	
$libms = new liblms();
$linterface = new interface_html();

# POST/ GET VALUE
$StocktakeSchemeID = $_POST['StocktakeSchemeID']?$_POST['StocktakeSchemeID']:$_GET['StocktakeSchemeID'];
$selectedStocktakeStatus = $_POST['selectedStocktakeStatus']?$_POST['selectedStocktakeStatus']:$_GET['selectedStocktakeStatus'];

if($StocktakeSchemeID=='' || $selectedStocktakeStatus==''){
	header("Location:write_off.php"); 	
}

# Page Settings
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageWriteOff";

//$TAGS_OBJ[] = array($Lang['libms']['bookmanagement']['stock-take'], "index.php", 0);
$TAGS_OBJ[] = array($Lang["libms"]["book_status"]["WRITEOFF"], "write_off.php", 1);

$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$linterface->LAYOUT_START();

$sql = $libms->GET_STOCKTAKE_SCHEME_SQL($StocktakeSchemeID,1);
$stocktakeSchemeArr = $libms->returnArray($sql);
$stocktakeSchemeTitle = $stocktakeSchemeArr[0]['SchemeTitle'];
$keyword = $_POST['keyword'];

# Page Navigation
$PAGE_NAVIGATION[]= array($stocktakeSchemeTitle, '');

#################### Stocktake Scheme Selection Box Start ######################
$sql = $libms->GET_STOCKTAKE_SCHEME_SQL('',1);
$stockTakeSchemeArr = $libms->returnArray($sql);
## Default StocktakeSchemeID

$numOfstockTakeSchemeArr = count($stockTakeSchemeArr);
for($i=0;$i<$numOfstockTakeSchemeArr;$i++){
	$thisStockTakeSchemeId = $stockTakeSchemeArr[$i]['StocktakeSchemeID'];
	$thisStockTakeDisplay = $stockTakeSchemeArr[$i]['SchemeTitle'] . ' ~ (' .  $stockTakeSchemeArr[$i]['StartDate']  . ' - ' .  $stockTakeSchemeArr[$i]['EndDate'] . ')';
	$stockTakeSchemeSelectionArr[]= array($thisStockTakeSchemeId,$thisStockTakeDisplay);

	$thisStartDateTime = strtotime($stockTakeSchemeArr[$i]['StartDate']);;
	$thisEndDateTime = strtotime($stockTakeSchemeArr[$i]['EndDate']);
	$currentDate = strtotime(date("Y-m-d"));
	
	if(($thisStartDateTime<=$currentDate) && ($thisEndDateTime>=$currentDate)){
		$thisShowStocktakeBtn[$thisStockTakeSchemeId] = true;
	}else{
		$thisShowStocktakeBtn[$thisStockTakeSchemeId] = false;
	}
	
}

$stockTakeSelectionBox = $linterface->GET_SELECTION_BOX($stockTakeSchemeSelectionArr, "onchange='javascript:js_form_submit(\"currentPage\");' name='StocktakeSchemeID' id='StocktakeSchemeID'" , '', $StocktakeSchemeID);
#################### Stocktake Scheme Selection Box End ########################

############################### Stocktake Status End ###############################
$i = 0;
$stocktakeStatus = array();
foreach ($Lang['libms']['stocktake_status'] as $key => $value)
{
	$stocktakeStatus[$i][0] = $key;
	$stocktakeStatus[$i][1] = $value;
	$i++;
}
$stockTakeStatusSelectionBox = $linterface->GET_SELECTION_BOX($stocktakeStatus, "onchange='javascript:js_form_submit(\"currentPage\");' name='selectedStocktakeStatus' id='stocktakeStatus' ", '', $selectedStocktakeStatus);
############################### Stocktake Status End ###############################


########################## DB Table Start ##############################

if (isset($elibrary_plus_write_off_list_page_size) && $elibrary_plus_write_off_list_page_size != ""){
	 $page_size = $elibrary_plus_write_off_list_page_size;
}
$pageSizeChangeEnabled = true;
$order = ($order == 1) ? 1 : 0;
$sql ='';
$stocktakeUniqueBookArr = array();
$stocktakeUniqueBookIDAssoc = array();


if($selectedStocktakeStatus=='FOUND')
{
	$sql = $libms->GET_STOCKTAKE_LOG_BOOK_INFO('',$StocktakeSchemeID,'sql', $keyword);		
}
elseif($selectedStocktakeStatus=='WRITEOFF')
{
	$sql = $libms->GET_WRITEOFF_LOG_INFO($StocktakeSchemeID,'','','sql', $keyword);	
}
elseif($selectedStocktakeStatus=='NOTTAKE')
{	
	$excludeUniqueBookIDArr = array();
	$stocktakeUniqueBookArr = $libms->GET_STOCKTAKE_LOG_BOOK_INFO('',$StocktakeSchemeID,'Array');
	$stocktakeUniqueBookIDAssoc = array_keys(BuildMultiKeyAssoc($stocktakeUniqueBookArr, 'UniqueID'));
	$writeOffUniqueBookArr = $libms->GET_WRITEOFF_LOG_INFO($StocktakeSchemeID, '', '','Array');	
	$writeOffUniqueBookIDAssoc = array_keys(BuildMultiKeyAssoc($writeOffUniqueBookArr, 'UniqueID'));
	$excludeUniqueBookIDArr = array_merge($stocktakeUniqueBookIDAssoc,$writeOffUniqueBookIDAssoc);
	
	$sql = $libms->GET_NOT_STOCKTAKE_BOOK_INFO($excludeUniqueBookIDArr,$keyword);		
	
}


$li = new libdbtable2007($field, $order, $pageNo);
global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";
if($selectedStocktakeStatus=='WRITEOFF')
	$li->field_array = array("lbu.ACNO","lbu.BarCode","lb.BookTitle","LocationName", "lbu.RecordStatus", "Result", "WriteOffDate", "Reason", "DateModified", "UserName", "CheckBox");
else
	$li->field_array = array("lbu.ACNO","lbu.BarCode","lb.BookTitle","LocationName", "lbu.RecordStatus", "Result", "DateModified", "UserName", "CheckBox");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->column_array = array(0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0);
$li->IsColOff =  "IP25_table";
$pos = 0;

# Barcode	Book Title	Location	Book Status	Stock-take Status	Last stock-take	Taken by 
$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang["libms"]["report"]["ACNO"])."</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['barcode'])."</td>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['bookTitle'])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++,	$Lang['libms']['bookmanagement']['location'])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['bookStatus'])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['StocktakeStatus'])."</td>\n";

if($selectedStocktakeStatus=='WRITEOFF')
{
	$li->column_list .= "<th width='9%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['WriteOffDate'])."</td>\n";
	$li->column_list .= "<th width='8%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['Reason'])."</td>\n";
	$li->column_list .= "<th width='9%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['WriteOffTime'])."</td>\n";
	$li->column_list .= "<th width='7%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['WriteOffBy'])."</td>\n";
}else{
	$li->column_list .= "<th width='18%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['lastStockTake'])."</td>\n";
	$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['libms']['bookmanagement']['lastTakenBy'])."</td>\n";
}

$li->column_list .= "<th width='1'>".$li->check("BookIDUniqueID[]")."</td>\n";



########################## DB Table Start End ##############################

?>
<script>
function js_form_submit(type){

	if(type=='writeOffRecord'){
		//$('#form1').attr('action', 'add_write_off_record.php');
		$('#form1').attr('action', 'add_write_off_record_confirm.php');
	}
	else if(type=='cancelWriteOffRecord'){
		$('#form1').attr('action', 'cancel_write_off_record.php');
	}
	else{
		$('#form1').attr('action', 'write_off_list.php');
	}
	
	$('#form1').submit();	
}

</script>
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form name="form1" id="form1" method="post" action="">
<?=$linterface->Get_Search_Box_Div('keyword',$keyword);?>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%"><?=$toolbar ?></td>
											<td width="30%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
							<td><?=$stockTakeSelectionBox?><?=$stockTakeStatusSelectionBox?></td>
								<td height="28" align="right" valign="bottom" >
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											
										  <td valign="bottom"><div class="common_table_tool">
										
										<? if($selectedStocktakeStatus=='WRITEOFF'){ ?>
											
                                       	 	<!--<a href="javaScript:void(0);" onclick="javascript:js_form_submit('cancelWriteOffRecord');" class="tool_handle"><?=$Lang['libms']['bookmanagement']['cancelWriteOff']?></a>
                                        	!-->
                                        	
                                        	<a href="javascript:checkRemove(document.form1,'BookIDUniqueID[]','cancel_write_off_record.php')" class="tool_handle"><?=$Lang['libms']['bookmanagement']['cancelWriteOff']?></a>
                                        	
                                        <?}?>
                                        <? if($selectedStocktakeStatus!='WRITEOFF'){ ?>
											<a href="javaScript:void(0);" onclick="javascript:checkEditMultiple(document.form1,'BookIDUniqueID[]','add_write_off_record_confirm.php')" class="tool_handle"><?=$Lang['libms']['bookmanagement']['writeOff']?></a>
                                        <?}?>    
									  </div></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display();?>
					</td>
				</tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

			
			</td>
		</tr>
	</table>
</form>

<?php
//echo $linterface->FOCUS_ON_LOAD("form1.LocationCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>