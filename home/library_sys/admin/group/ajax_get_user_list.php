<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}



// using kenneth chung
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage_ui.php");

intranet_opendb();
// YearClassID:1A
// IdentityType:Student
// ParentStudentID:
// GroupID:9

$YearClassID = $_REQUEST['YearClassID'];
$IdentityType = $_REQUEST['IdentityType'];
$AddUserID = (is_array($_REQUEST['AddUserID']))? $_REQUEST['AddUserID']:array();
$ParentStudentID = $_REQUEST['ParentStudentID'];
$GroupID = $_REQUEST['GroupID'];
/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/
$GroupManageUI = new liblibrarymgmt_group_manage_ui();

echo $GroupManageUI->Get_User_Selection($AddUserID,$IdentityType,$YearClassID,$ParentStudentID,$GroupID);

intranet_closedb();
?>
