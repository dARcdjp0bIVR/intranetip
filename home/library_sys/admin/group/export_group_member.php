<?php
// using: 
/**
 * Log :
 *
 * Date: 2019-07-08 [Tommy]
 * - File Created
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt_group_manage.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$rm = new liblibrarymgmt_group_manage();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (! $admin_right['group management'] && ! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$groupID = IntegerSafe($_POST['group']);
$Group = new liblibrarymgmt_group($groupID, false, true);

$exportColumn = array();
$exportColumn[0][] = $Lang['libms']['GroupManagement']['GroupTitle'] . ": " . $Group->GroupTitle;
$exportColumn[0][] = $Lang['libms']['GroupManagement']['GroupCode'] . ": " . $Group->GroupCode;
$exportColumn[1][] = $Lang["libms"]["GroupManagement"]["Name"];
$exportColumn[1][] = $Lang['libms']['CirculationManagement']['user_barcode'];
$exportColumn[1][] = $Lang['libms']['GroupManagement']['ClassName'];
$exportColumn[1][] = $Lang['libms']['GroupManagement']['ClassNumber'];
$exportColumn[1][] = $Lang['libms']['GroupManagement']['UserType'];

$rows = array();
$export_content = "";

$lexport = new libexporttext();

$result_data = array();
$rows = $Group->MemberList;

for ($i = 0; $i < sizeof($rows); $i ++) {
    $result_data[] = array(
        $rows[$i]['MemberName'] ? $rows[$i]['MemberName'] : '--',
        $rows[$i]['BarCode'] ? $rows[$i]['BarCode'] : '--',
        $rows[$i]['ClassName'] ? $rows[$i]['ClassName'] : '--',
        $rows[$i]['ClassNumber'] ? $rows[$i]['ClassNumber'] : '--',
        $rows[$i]['UserType'] ? $rows[$i]['UserType'] : '--'
    );
}

$export_content .= ($export_content ? "\r\n" : "") . $lexport->GET_EXPORT_TXT($result_data, $exportColumn, "\t", "\r\n", "\t", 0, '11');

$exportColumn = array(); // pass column header for the 1st time only

intranet_closedb();

$filename = "group_member" . ".csv";

$lexport->EXPORT_FILE($filename, $export_content, false, false, "UTF-8");

?>