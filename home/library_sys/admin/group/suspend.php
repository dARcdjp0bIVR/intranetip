<?php
// Using : 
######################################################################################
#									Change Log 
######################################################################################
#
#	20150708 Henry:	Add tag for Class Management
#
#	20150203 Ryan : Training Feedback  - Add Button to suspend/activate users 
#										 [Case Number : 2015-0203-1543-49054]
#	20141201 Ryan : eLibrary plus pending developments Item 204
#
######################################################################################

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes"); 
}




$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_suspend_manage.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");


intranet_auth();
intranet_opendb();

/* Session controlled PRIVILEGE
if (!$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
//*/


$libms = new liblms();

$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['group management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

// setting top menu highlight
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageGroupManagement";

$libms->MODULE_AUTHENTICATION($CurrentPage);
$home_header_no_EmulateIE7 = true;
$linterface = new interface_html();
### Title ###
$TAGS_OBJ[] = array($Lang["libms"]["GroupManagement"]["ModuleTitle"],"index.php",0);
$TAGS_OBJ[] = array($Lang["libms"]["GroupManagement"]["ClassManagement"],"class_management.php",0);
$TAGS_OBJ[] = array($Lang["libms"]["SearchUser"]["ModuleTitle"],"search.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang["libms"]["UserSuspend"]["ModuleTitle"],"suspend.php",1);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$linterface->LAYOUT_START();

$Suspend = new liblibrarymgmt_suspend();
$SuspendManage = new liblibrarymgmt_suspend_manage();

echo $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang["libms"]["UserSuspend"]["Instruction"]);
echo '<br/>';
echo $SuspendManage->Get_Add_Group_Member_Form($Suspend);

echo '<br/>';
echo '<div id="SuspendedUsersList">';
echo $SuspendManage->Get_Group_Member_Form($Suspend);
echo '</div>';

$Json = new JSON_obj();
?>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->
<?
$linterface->LAYOUT_STOP();

intranet_closedb();


?>
<script language="JavaScript" src="/templates/ajax.js"></script>
<script language="JavaScript">
	function ChangeUsertype(){
		var Usertype = $('#IdentityType').val()
		if(Usertype == 'Student'){
			$('#YearClassSelect').show('fast');
			UpdateUserList();
		}else{
			$('#YearClassSelect').hide();
			$('#YearClassSelect').val("");
			UpdateUserList();
		}
	}
	
		
	function UpdateUserList(){
		$.ajax({
	        type : "POST",
	        url : "ajax_get_user_list_suspend.php",
	        data: $("#AddMemberForm").serialize(),
	        success : function(msg) {
					$('#AvalUserLayer').html(msg);
	        }
	    });
	}
	
	function AddAllToSuspended(){
		var textToInsert = [];
		var list ='';
		var i = 0;
		var data = $('#AvalUserList\\[\\] option');
		var length = $('#AvalUserList\\[\\] option').length;
		for (var a = 0; a < length; a += 1) {
		    textToInsert[i++] = data[a];
		}
		$(textToInsert).each(function(){list += this.value +','} );
		list = list.substring(0, list.length-1);
		$('#SelectedUsers').val(list);
		$('#AddUserID\\[\\]').append(textToInsert);
	}
	
	function RemoveAllFromSuspended(){
		var textToInsert = [];
		var list ='';
		var i = 0;
		var data = $('#AddUserID\\[\\] option');
		var length = $('#AddUserID\\[\\] option').length;
		for (var a = 0; a < length; a += 1) {
		    textToInsert[i++] = data[a];
		}
		$(textToInsert).each(function(){list += this.value +','} );
		list = list.substring(0, list.length-1);
		$('#SelectedUsers').val(list);
		$('#AvalUserList\\[\\]').append(textToInsert);
	}
	
	function AddToSuspended(){
		var UsersToSuspend = [];
		UsersToSuspend = $('#AvalUserList\\[\\]').val();
		if(UsersToSuspend){
			$('#AddUserID\\[\\]').append($('#AvalUserList\\[\\] :selected'));
			$('#SelectedUsers').val(UsersToSuspend.toString());
		}
	}
	
	function RemoveFromSuspended(){
		var UsersToRemove = [];
		 UsersToRemove = $('#AddUserID\\[\\]').val();
		if(UsersToRemove){
			$('#AvalUserList\\[\\]').append($('#AddUserID\\[\\] :selected'));
			$('#SelectedUsers').val(UsersToRemove.toString());
		}
	}
	
	function SuspendUsers(){
		$('#SuspendedUsersList').prepend('<center><span><?=$Lang["libms"]["ajaxLoading"]?></span></center>')
		$.ajax({
	        type : "POST",
	        url : "ajax_suspend_user.php",
	        data: $("#AddMemberForm").serialize(),
	        success : function(msg) {
				$('#SuspendedUsersList').html(msg);
	        }
	    });
	}
	
	function ActivateUsers(){
		$('#SuspendedUsersList').prepend('<center><span><?=$Lang["libms"]["ajaxLoading"]?></span></center>')
		$.ajax({
	        type : "POST",
	        url : "ajax_activate_user.php",
	        data: $("#AddMemberForm").serialize(),
	        success : function(msg) {
				$('#SuspendedUsersList').html(msg);
				RemoveAllFromSuspended();
	        }
	    });
	}
	
	function DeleteFromList(){
		var UserID = Get_Check_Box_Value('UserIDs[]');
		if(UserID == ''){
			alert('<?=$Lang["libms"]["tableNoCheckedCheckbox"]?>');
			return false;
		}
		$.ajax({
	        type : "POST",
	        url : "ajax_activate_user.php",
	        data: {SelectedUsers : UserID.toString()},
	        success : function(msg) {
				$('#SuspendedUsersList').html(msg);
				UpdateUserList();
				// get AddedList options 
				var i=0;
				var a = [];
				$("#AddUserID\\[\\] option").each(function(){ a[i++] = this.value})
				$(UserID).each(function(){
						var tmp = this.toString();
					 	if($.inArray(tmp, a) != -1){
					 			// Remove selected 
				 				$("#AddUserID\\[\\] option[value='"+ tmp +"']").remove();
				 			} 
					 });
	     	   }
	    });
	}
	
</script>