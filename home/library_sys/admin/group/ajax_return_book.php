<?php
// using 
######################################################################################
#									Change Log 
######################################################################################
#
#	20150204 Ryan : Training Feedback  - Changed Return Msg to IP25 xmsg  
#										 [Case Number : 2015-0203-1543-49054]
#	20141201 Ryan : eLibrary plus pending developments Item 205
#
######################################################################################

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/User.php");

intranet_opendb();
$libms = new liblms();
$linterface = new interface_html();
$uID = base64_decode($_POST['uID']);
$UniqueIDs = $_POST['UniqueIDs'];
$User = new User($uID);

// Override settings
$User->setAdminMode(true);

$returnResult = array();
foreach((array)$UniqueIDs as $UniqueID){
	
	$rs = $User->return_book($UniqueID, false , '', 1);
	
	$returnResult[$UniqueID] = intval($rs);
}
$jsonObj = new JSON_obj();

if(in_array(false,$returnResult) || in_array(-2,$returnResult)){
	$ReturnMsg = '0|=|'.$Lang["libms"]["SearchUser"]["ReturnBookReturnMsgFail"];
	foreach((array)$returnResult as $UniqueID=> $bookrs){
		
		// get book title 
		foreach((array)$User->borrowedBooks as $books){
			if($books['UniqueID'] == $UniqueID){
				$BookTitle = $books['BookTitle'];
				break;
			}
		}
		
		// Msg
		if(!$bookrs){
			$ReturnMsg .= $BookTitle.': '.$Lang["libms"]["Circulation"]["action_fail"]."\n"; 
		}else if($bookrs === -2){
			$ReturnMsg .= $BookTitle.': '.$Lang['libms']["settings_alert"]['SameDayReturn']."\n"; 
		}	
	}
}else{
	$ReturnMsg = '1|=|'.$Lang["libms"]["Circulation"]["action_success"]; 
}

$User = new User($uID); // refresh 
$User->getOverdueRecords();
$User->getBorrowedHistory();

$borrowedTable = $libms->Get_Borrowed_Books_Table($User,'Borrowed');
$OverdueTable = $libms->Get_Overdue_Records_Table($User,'Borrowed');
$OverdueBalance = $User->userInfo['Balance'];

$return = array();
$return [] = $ReturnMsg;         //Return Msg
$return [] = $borrowedTable; 	 //Refresh Borrowed Table
$return [] = $OverdueTable;		 //Refresh Overdue Table
$return [] = ($OverdueBalance==0) ? '--' : '<font color="red">$'.-$OverdueBalance.'</font>'; // Refresh User Balance


$jsonencode = $jsonObj->encode($return); 
echo $jsonencode;
intranet_closedb();?>