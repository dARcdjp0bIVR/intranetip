<?php
#	using: 

/*
 * 	Log
 *  Date:   2019-07-30 [Tommy]  Add function click_export_all()
 *  Date:   2019-07-08 [Tommy]  Add function click_export()
 *  Date:	2015-07-08 [Henry]	Add tag for Class Management
 *  Date:	2014-12-09 [Ryan]	Modified function Get_User_List() to hide Parent YearClass filter 
 *  Date:	2014-11-27 [Ryan]	Modified function Save_Group_Period to save "Same Day Return"
 *  Date:	2013-08-22 [Henry]	Modified function Init_JQuery_AutoComplete() to fix the bug of "Search User"
 * 	Date:	2013-08-05 [Cameron] Fix bug for Delete_Group_Member
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes"); 
}




$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/reports/Exporter.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");


intranet_auth();
intranet_opendb();

/* Session controlled PRIVILEGE
if (!$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
//*/


$libms = new liblms();

$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['group management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

// setting top menu highlight
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageGroupManagement";

$libms->MODULE_AUTHENTICATION($CurrentPage);
$linterface = new interface_html();
### Title ###
$TAGS_OBJ[] = array($Lang["libms"]["GroupManagement"]["ModuleTitle"],"index.php",1);
$TAGS_OBJ[] = array($Lang["libms"]["GroupManagement"]["ClassManagement"],"class_management.php",0);
$TAGS_OBJ[] = array($Lang["libms"]["SearchUser"]["ModuleTitle"],"search.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang["libms"]["UserSuspend"]["ModuleTitle"],"suspend.php",0);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$linterface->LAYOUT_START();

$GroupManageUI = new liblibrarymgmt_group_manage_ui();
$Json = new JSON_obj();

echo $GroupManageUI->Get_Identity_Manage();

?>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->
<?
$linterface->LAYOUT_STOP();

intranet_closedb();


?>

<script language="JavaScript" src="/templates/ajax.js"></script>


<script language="JavaScript">
// Ajax function
{
function Get_New_Group_Form() {
	NewGroupAjax = GetXmlHttpObject();

  if (NewGroupAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_get_new_group_form.php';
  var postContent = '';
	NewGroupAjax.onreadystatechange = function() {
		if (NewGroupAjax.readyState == 4) {
			ResponseText = Trim(NewGroupAjax.responseText);
		  document.getElementById('TB_ajaxContent').innerHTML = ResponseText;
		}
	};
  NewGroupAjax.open("POST", url, true);
	NewGroupAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	NewGroupAjax.send(postContent);
}

function Save_Group(EditAfterCreate) {
	Check_Group_Name();
	//Check_Identity_Selection();
	var GroupNameWarning = $('#GroupNameWarningLayer').html();
	//var IdentityWarning = $('#IdentityWarningLayer').html();
	//if (GroupNameWarning == "" && IdentityWarning == "") {
	if (GroupNameWarning == "") {
		var GroupType = Get_Selection_Value('GroupType','String');
		document.getElementById('SubmitGroupBtn').disabled = true;
		//document.getElementById('SubmitEditGroupBtn').disabled = true;
		document.getElementById('CancelGroupBtn').disabled = true;
		SaveGroupAjax = GetXmlHttpObject();

	  if (SaveGroupAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_create_group.php';
	  var PostString = 'GroupName='+document.getElementById("GroupName").value;
	  PostString += '&GroupCode='+document.getElementById("GroupCode").value;
	  PostString += '&EditAfterCreate='+EditAfterCreate;
		SaveGroupAjax.onreadystatechange = function() {
			if (SaveGroupAjax.readyState == 4) {
				ResponseText = Trim(SaveGroupAjax.responseText);
				ReturnArray = ResponseText.split('~~~~~');
			  if (EditAfterCreate) {
			  	if (ReturnArray[0] == "false") {
			  		Get_Return_Message(ReturnArray[1]);
			  		//window.top.tb_remove();
			  	}
			  	else {
			  		//window.top.tb_remove();
			  		Get_Group_Detail(ReturnArray[0]);
			  		Get_Return_Message(ReturnArray[1]);
			  	}
			  }
			  else {
		  		Get_Return_Message(ReturnArray[1]);
		  		Get_Group_List_Table(GroupType);
		  		//window.top.tb_remove();
			  }
			}
		};
	  SaveGroupAjax.open("POST", url, true);
		SaveGroupAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		SaveGroupAjax.send(PostString);
	}
}

function Check_Group_Name(GroupName,GroupID) {
	var GroupName;
	if (document.getElementById('GroupName'))
		GroupName = document.getElementById('GroupName').value;
	else
		GroupName = GroupName || "";

	var GroupID = GroupID || "";
	var ElementObj = $('#GroupNameWarningLayer');
	if (Trim(GroupName) != "") {
		CheckGroupNameAjax = GetXmlHttpObject();

	  if (CheckGroupNameAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_check_group_name.php';
	  var postContent = 'GroupName='+encodeURIComponent(GroupName);
	  postContent += '&GroupID='+GroupID;
		CheckGroupNameAjax.onreadystatechange = function() {
			if (CheckGroupNameAjax.readyState == 4) {
				ResponseText = Trim(CheckGroupNameAjax.responseText);
				if (ResponseText == "1") {
					ElementObj.html('');
					ElementObj.hide();
				}
				else {
					ElementObj.html('<?=$Lang['libms']['GroupManagement']['GroupNameDuplicateWarning']?>');
					ElementObj.show('fast');
				}
			}
		};
	  CheckGroupNameAjax.open("POST", url, true);
		CheckGroupNameAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		CheckGroupNameAjax.send(postContent);
	}
	else {
		CheckGroupNameAjax = "";

		ElementObj.html('<?=$Lang['libms']['GroupManagement']['GroupNameDuplicateWarning']?>');
		ElementObj.show('fast');
	}
}

function Check_Group_Code(GroupCode,GroupID) {
	var GroupCode;
	if (document.getElementById('GroupCode'))
		GroupCode = document.getElementById('GroupCode').value;
	else
		GroupCode = GroupCode || "";

	var GroupID = GroupID || "";
	var ElementObj = $('#GroupCodeWarningLayer');
	if (Trim(GroupCode) != "") {
		CheckGroupCodeAjax = GetXmlHttpObject();

	  if (CheckGroupCodeAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_check_group_code.php';
	  var postContent = 'GroupCode='+encodeURIComponent(GroupCode);
	  postContent += '&GroupID='+GroupID;
		CheckGroupCodeAjax.onreadystatechange = function() {
			if (CheckGroupCodeAjax.readyState == 4) {
				ResponseText = Trim(CheckGroupCodeAjax.responseText);
				if (ResponseText == "1") {
					ElementObj.html('');
					ElementObj.hide();
				}
				else {
					ElementObj.html('<?=$Lang['libms']['GroupManagement']['GroupCodeDuplicateWarning']?>');
					ElementObj.show('fast');
				}
			}
		};
	  CheckGroupCodeAjax.open("POST", url, true);
		CheckGroupCodeAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		CheckGroupCodeAjax.send(postContent);
	}
	else {
		CheckGroupCodeAjax = "";

		ElementObj.html('<?=$Lang['libms']['GroupManagement']['GroupCodeDuplicateWarning']?>');
		ElementObj.show('fast');
	}
}

function Get_Group_List_Table(GroupType) {
	GroupType = GroupType || '';

	GetGroupAjax = GetXmlHttpObject();

  if (GetGroupAjax == null)
  {
    alert (errAjax);
    return;
  }

  // if GroupType is not provided, whole page is refreshed, otherwise only the Layer of "GroupType" is refreshed
  var ReturnLayer = "";
  if (GroupType == "")
  	ReturnLayer = "IdentityManageLayer";
  else
  	ReturnLayer = GroupType+'GroupListLayer';

  var url = 'ajax_get_group_list.php';
  var postContent = 'GroupType='+GroupType;
	GetGroupAjax.onreadystatechange = function() {
		if (GetGroupAjax.readyState == 4) {
			ResponseText = Trim(GetGroupAjax.responseText);
		  document.getElementById(ReturnLayer).innerHTML = ResponseText;
		  Thick_Box_Init();
		}
	};
  GetGroupAjax.open("POST", url, true);
	GetGroupAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	GetGroupAjax.send(postContent);
}
function Get_Group_Detail(GroupID,Show) {
	var Show = Show || "";
	
	$('#IdentityManageLayer').load('ajax_get_group_detail.php?GroupID='+GroupID+'&Show='+Show);

	  
}

function Get_Group_Detail2(GroupID,Show) {
	var Show = Show || "";

	GroupDetailAjax = GetXmlHttpObject();

  if (GroupDetailAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_get_group_detail.php';
  var postContent = 'GroupID='+GroupID;
  postContent += '&Show='+Show;
	GroupDetailAjax.onreadystatechange = function() {
		if (GroupDetailAjax.readyState == 4) {
			ResponseText = Trim(GroupDetailAjax.responseText);
		  document.getElementById('IdentityManageLayer').innerHTML = ResponseText;
		  Thick_Box_Init();
		  Init_JEdit_Input("div.jEditInput");
		  Init_JEdit_InputCode("div.jEditInputCode");
		}
	};
  GroupDetailAjax.open("POST", url, true);
	GroupDetailAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	GroupDetailAjax.send(postContent);
}

function Get_Group_Add_Member_Form(GroupID) {
	GroupAddMemberAjax = GetXmlHttpObject();

  if (GroupAddMemberAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_get_add_group_member_form.php';
  var postContent = 'GroupID='+GroupID;
	GroupAddMemberAjax.onreadystatechange = function() {
		if (GroupAddMemberAjax.readyState == 4) {
			ResponseText = Trim(GroupAddMemberAjax.responseText);
		  document.getElementById('TB_ajaxContent').innerHTML = ResponseText;
		 	Init_JQuery_AutoComplete('UserSearch');
		}
	};
  GroupAddMemberAjax.open("POST", url, true);
	GroupAddMemberAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	GroupAddMemberAjax.send(postContent);
}

function Add_Group_Member(GroupID) {
	//Select_All_Options("AddUserID[]",true);
	
	var $Has_Group_Users =  $('[id*=AddUserID] [has_group]');
	if($Has_Group_Users.length >0 ){
		var username_list ='';
		$Has_Group_Users.each(function (){
			username_list = username_list + ( (username_list === "") ? "": ", " )+ $(this).text();
		});

		if (!confirm("<?=$Lang["libms"]["GroupManagement"]["Msg"]["chgrp_confirm"]?>\n"+username_list)) { 
			return;			 
		}
	}
	
	
	
	
	var SelectedUser = Get_Selection_Value("AddUserID[]","QueryString",true);
	

	if (SelectedUser != "") {
		document.getElementById('AddMemberSubmitBtn').disabled = true;
		document.getElementById('AddMemberCancelBtn').disabled = true;
		Block_Thickbox();
		AddGroupMemberAjax = GetXmlHttpObject();

	  if (AddGroupMemberAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_add_group_member.php';
	  var postContent = SelectedUser;
	  postContent += '&GroupID='+GroupID;
		AddGroupMemberAjax.onreadystatechange = function() {
			if (AddGroupMemberAjax.readyState == 4) {
				Update_Group_Member_List(GroupID);
				ResponseText = Trim(AddGroupMemberAjax.responseText);
			  Get_Return_Message(ResponseText);
			  UnBlock_Thickbox();
			  Scroll_To_Top();
			  window.top.tb_remove();
			}
		};
	  AddGroupMemberAjax.open("POST", url, true);
		AddGroupMemberAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		AddGroupMemberAjax.send(postContent);
	}
	else {
		$('#AddGroupMemberWarningLayer').html('<?=$Lang['libms']['GroupManagement']['AddGroupMemberWarning']?>');
		$('#AddGroupMemberWarningLayer').show('fast');
	}
}

function Update_Group_Member_List(GroupID) {
	GroupMemberListAjax = GetXmlHttpObject();

  if (GroupMemberListAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_get_group_member_list.php';
  var postContent = 'GroupID='+GroupID;
	GroupMemberListAjax.onreadystatechange = function() {
		if (GroupMemberListAjax.readyState == 4) {
			ResponseText = Trim(GroupMemberListAjax.responseText);
		  document.getElementById('MemberListSubLayer').innerHTML = ResponseText;
		  Thick_Box_Init();
		}
	};
  GroupMemberListAjax.open("POST", url, true);
	GroupMemberListAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	GroupMemberListAjax.send(postContent);
}

function Remove_Group(GroupID) {
	if (confirm('<?=$Lang['libms']['GroupManagement']['DeleteWarning']?>')) {
		RemoveGroupAjax = GetXmlHttpObject();

	  Block_Document();
	  if (RemoveGroupAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_remove_group.php';
	  var postContent = 'GroupID='+GroupID;
		RemoveGroupAjax.onreadystatechange = function() {
			if (RemoveGroupAjax.readyState == 4) {
				ResponseText = Trim(RemoveGroupAjax.responseText);
				Get_Return_Message(ResponseText);
				Get_Group_List_Table();
				Thick_Box_Init();
				UnBlock_Document();
			}
		};
	  RemoveGroupAjax.open("POST", url, true);
		RemoveGroupAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		RemoveGroupAjax.send(postContent);
	}
}

function Delete_Group_Member(GroupID) {
	if (confirm('<?=$Lang['libms']['GroupManagement']['DeleteWarning']?>')) {
		var UserID = Get_Check_Box_Value('UserIDs[]','QueryString');
		RemoveGroupMemberAjax = GetXmlHttpObject();

	  Block_Document();
	  if (RemoveGroupMemberAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_remove_group_member.php';
	  var postContent = 'GroupID='+GroupID;
	  postContent += '&'+UserID;
		RemoveGroupMemberAjax.onreadystatechange = function() {
			if (RemoveGroupMemberAjax.readyState == 4) {
				Update_Group_Member_List(GroupID);
				ResponseText = Trim(RemoveGroupMemberAjax.responseText);
			  Get_Return_Message(ResponseText);
			  Scroll_To_Top();
				UnBlock_Document();
			}
		};
	  RemoveGroupMemberAjax.open("POST", url, true);
		RemoveGroupMemberAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		RemoveGroupMemberAjax.send(postContent);
	}
}

function Save_Group_Period(GroupID,rule) {
	
  SaveRightRuleAjax = GetXmlHttpObject();

  Block_Document();
  if (SaveRightRuleAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_save_group_period.php';
  var postContent = 'GroupID='+GroupID;
  
  postContent += '&PeriodStartDate=' + $("#PeriodStartDate").val();
  postContent += '&PeriodEndDate=' + $("#PeriodEndDate").val();
  postContent += '&SameDayReturn=' + $("#SameDayReturn").attr("checked");

    SaveRightRuleAjax.onreadystatechange = function() {
	    if (SaveRightRuleAjax.readyState == 4) {
		    ResponseText = Trim(SaveRightRuleAjax.responseText);
		    Get_Return_Message(ResponseText);
		    Scroll_To_Top();
		    Get_Group_Period_Detail(GroupID);
	    }
    };
    SaveRightRuleAjax.open("POST", url, true);
    SaveRightRuleAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    SaveRightRuleAjax.send(postContent);
}

function Save_Group_Rule(GroupID,rule) {
	
  SaveRightRuleAjax = GetXmlHttpObject();

  Block_Document();
  if (SaveRightRuleAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_save_group_rule.php';
  var postContent = 'GroupID='+GroupID;

  $('input[name^=rule]').each( function(){
      if( $(this).is('[type=checkbox]') ){
	if ( $(this).is(':checked') ){
	    postContent += '&' + $(this).attr('name') + '=1';
	}else{
	    postContent += '&' + $(this).attr('name') + '=0';
	}
      }else{
	postContent += '&' + $(this).attr('name') + '=' + $(this).val();
      }

  });
  
    SaveRightRuleAjax.onreadystatechange = function() {
	    if (SaveRightRuleAjax.readyState == 4) {
		    ResponseText = Trim(SaveRightRuleAjax.responseText);
		    Get_Return_Message(ResponseText);
		    Scroll_To_Top();
		    Get_Group_Rule_Detail(GroupID);
	    }
    };
    SaveRightRuleAjax.open("POST", url, true);
    SaveRightRuleAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    SaveRightRuleAjax.send(postContent);
}

function Save_Group_Right_Target(GroupID,RightOrTarget) {
	if (RightOrTarget == "Target") {
		/*var TeachingTarget = Get_Check_Box_Value('TeachingTarget[]','QueryString');
		var NonTeachingTarget = Get_Check_Box_Value('NonTeachingTarget[]','QueryString');
		var StudentTarget = Get_Check_Box_Value('StudentTarget[]','QueryString');
		var ParentTarget = Get_Check_Box_Value('ParentTarget[]','QueryString');*/
		var Target = Get_Check_Box_Value('Target[]','QueryString');
	}
	else
		var GroupRight = Get_Check_Box_Value('Right[]','QueryString');

	SaveRightTargetAjax = GetXmlHttpObject();

  Block_Document();
  if (SaveRightTargetAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_save_group_right_target.php';
  var postContent = 'GroupID='+GroupID;
  if (RightOrTarget == "Target") {
		/*postContent += '&'+TeachingTarget;
		postContent += '&'+NonTeachingTarget;
		postContent += '&'+StudentTarget;
		postContent += '&'+ParentTarget;*/
		postContent += '&'+Target;
	}
	else
		postContent += '&'+GroupRight;
  //postContent += '&RightOrTarget='+RightOrTarget;

	SaveRightTargetAjax.onreadystatechange = function() {
		if (SaveRightTargetAjax.readyState == 4) {
			ResponseText = Trim(SaveRightTargetAjax.responseText);
			Get_Return_Message(ResponseText);
			Scroll_To_Top();
			if (RightOrTarget == "Target")
				Get_Group_Target_Detail(GroupID);
			else
				Get_Group_Right_Detail(GroupID);
		}
	};
  SaveRightTargetAjax.open("POST", url, true);
	SaveRightTargetAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	SaveRightTargetAjax.send(postContent);
}

function Get_Group_Target_Detail(GroupID) {
	GetGroupTargetAjax = GetXmlHttpObject();

  if (GetGroupTargetAjax == null)
  {
    alert (errAjax);
    return;
  }

	var url = 'ajax_get_group_target_detail.php';
  var postContent = 'GroupID='+GroupID;

	GetGroupTargetAjax.onreadystatechange = function() {
		if (GetGroupTargetAjax.readyState == 4) {
			ResponseText = Trim(GetGroupTargetAjax.responseText);
			//$('#TargetDetailSubLayer').html(ResponseText);
			document.getElementById('TargetDetailSubLayer').innerHTML = ResponseText;
			UnBlock_Document();
		}
	};
  GetGroupTargetAjax.open("POST", url, true);
	GetGroupTargetAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	GetGroupTargetAjax.send(postContent);
}

function Get_Group_Right_Detail(GroupID) {
	GetGroupRightAjax = GetXmlHttpObject();

  if (GetGroupRightAjax == null)
  {
    alert (errAjax);
    return;
  }

	var url = 'ajax_get_group_right_detail.php';
  var postContent = 'GroupID='+GroupID;

	GetGroupRightAjax.onreadystatechange = function() {
		if (GetGroupRightAjax.readyState == 4) {
			ResponseText = Trim(GetGroupRightAjax.responseText);			
			document.getElementById('TopManagementDetailSubLayer').innerHTML = ResponseText;
			UnBlock_Document();
		}
	};
  GetGroupRightAjax.open("POST", url, true);
	GetGroupRightAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	GetGroupRightAjax.send(postContent);
}

function Get_Group_Period_Detail2(GroupID) {
	GetGroupRightAjax = GetXmlHttpObject();

  if (GetGroupRightAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_get_group_period_detail.php';
  var postContent = 'GroupID='+GroupID;

	GetGroupRightAjax.onreadystatechange = function() {
		if (GetGroupRightAjax.readyState == 4) {
			ResponseText = Trim(GetGroupRightAjax.responseText);
			document.getElementById('PeriodDetailSubLayer').innerHTML = ResponseText;
			UnBlock_Document();
		}
	};
   GetGroupRightAjax.open("POST", url, true);
	GetGroupRightAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	GetGroupRightAjax.send(postContent);

}


function Get_Group_Period_Detail(GroupID) {
	$('#PeriodDetailSubLayer').load("ajax_get_group_period_detail.php?GroupID="+GroupID);
	UnBlock_Document();
}

function Get_Group_Rule_Detail(GroupID) {
	GetGroupRightAjax = GetXmlHttpObject();

  if (GetGroupRightAjax == null)
  {
    alert (errAjax);
    return;
  }

	var url = 'ajax_get_group_rule_detail.php';
  var postContent = 'GroupID='+GroupID;

	GetGroupRightAjax.onreadystatechange = function() {
		if (GetGroupRightAjax.readyState == 4) {
			ResponseText = Trim(GetGroupRightAjax.responseText);
			document.getElementById('RuleDetailSubLayer').innerHTML = ResponseText;
			UnBlock_Document();
		}
	};
  GetGroupRightAjax.open("POST", url, true);
	GetGroupRightAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	GetGroupRightAjax.send(postContent);
}

var PreviousClass = "";
function Get_User_List(GroupID) {
	var GroupID = GroupID || document.getElementById('GroupID').value;
	var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	//Select_All_Options('AddUserID[]',true);
	var AddUserID = Get_Selection_Value('AddUserID[]','QueryString',true);

	if (IdentityType != "Student" || (document.getElementById('YearClassSelect').style.display != "none" && document.getElementById('YearClassSelect').selectedIndex != 0)) {

		// hide the select class selection if switced to teacher/ staff/parent
		if (IdentityType != "Student") {
			document.getElementById('YearClassSelect').selectedIndex = 0;
			$('#YearClassSelect').hide();
		}

		// get the form class ID
		var YearClassID = document.getElementById('YearClassSelect').options[document.getElementById('YearClassSelect').selectedIndex].value;

		var GetFinalList = false;
		if (PreviousClass == YearClassID)
			GetFinalList = true;

		// get student selected if the selection element exists
		var ParentStudentID = "";
		if (document.getElementById('ParentStudentID') && IdentityType == "Parent" && GetFinalList)
			ParentStudentID = document.getElementById('ParentStudentID').options[document.getElementById('ParentStudentID').selectedIndex].value;

		// setting global variable to prevent parent selection problem
		PreviousClass = YearClassID;

		StudListAjax = GetXmlHttpObject();

	  if (StudListAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_get_user_list.php';
	  var postContent = AddUserID;
	  postContent += '&YearClassID='+YearClassID;
	  postContent += '&IdentityType='+IdentityType;
	  postContent += '&ParentStudentID='+ParentStudentID;
	  postContent += '&GroupID='+GroupID;

		StudListAjax.onreadystatechange = function() {
			if (StudListAjax.readyState == 4) {
				ResponseText = Trim(StudListAjax.responseText);
		  		document.getElementById('ParentStudentLayer').innerHTML = "";
			  	document.getElementById('AvalUserLayer').innerHTML = ResponseText;
			}
		};
	  StudListAjax.open("POST", url, true);
		StudListAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		StudListAjax.send(postContent);
	}
	else {
		$('#YearClassSelect').show('fast');
		document.getElementById('ParentStudentLayer').innerHTML = "";

		StudListAjax = GetXmlHttpObject();
	  if (StudListAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_get_user_list.php';
	  var postContent = AddUserID;
	  postContent += '&IdentityType='+IdentityType;
	  postContent += '&GroupID='+GroupID;

		StudListAjax.onreadystatechange = function() {
			if (StudListAjax.readyState == 4) {
				ResponseText = Trim(StudListAjax.responseText);
			 	document.getElementById('AvalUserLayer').innerHTML = ResponseText;
			}
		};
	  StudListAjax.open("POST", url, true);
		StudListAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		StudListAjax.send(postContent);
	}
}
}

// pure js (no ajax) form validate function
{
function Check_Identity_Selection() {
	var ElementObj = $('#IdentityWarningLayer');
	if (Get_Selection_Value('GroupType','String') == "") {
		ElementObj.html('<?=$Lang['libms']['GroupManagement']['IdentitySelectWarning']?>');
		ElementObj.show('fast');
	}
	else {
		ElementObj.html('');
		ElementObj.hide();
	}
}
}

// jEditable function
{
function Init_JEdit_Input(objDom) {
	var WarningLayer = "#GroupNameWarningLayer";
	$(objDom).editable(
    function(value, settings) {
    	var ElementObj = $(this);
    	if ($(WarningLayer).html() == "" && ElementObj[0].revert != value) {
	    	wordXmlHttp = GetXmlHttpObject();

			  if (wordXmlHttp == null)
			  {
			    alert (errAjax);
			    return;
			  }

			  var url = 'ajax_rename_group.php';
				var postContent = "GroupID="+ElementObj.attr("id");
				postContent += "&GroupName="+encodeURIComponent(value);
				wordXmlHttp.onreadystatechange = function() {
					if (wordXmlHttp.readyState == 4) {
						Get_Return_Message(Trim(wordXmlHttp.responseText));
						ElementObj.html(value);
						$('span#GroupNameNavLayer').html(value);
					}
				};
			  wordXmlHttp.open("POST", url, true);
				wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				wordXmlHttp.send(postContent);
			}
			else {
				ElementObj[0].reset();
			}
		}, {
    tooltip   : "<?=$Lang['libms']['FormClassMapping']['ClickToEdit']?>",
    event : "click",
    onblur : "submit",
    type : "text",
    style  : "display: inline",
    height: "20px",
    maxlength: 100,
    onreset: function() {
    	$(WarningLayer).html('');
    	$(WarningLayer).hide();
    }
  });

  $(objDom).keyup(function() {
		var GroupName = $('form input').val();
		var GroupID = $(this).attr('id');

		Check_Group_Name(GroupName,GroupID);
	});
}

function Init_JEdit_InputCode(objDom){
	var WarningLayer = "#GroupCodeWarningLayer";
	$(objDom).editable(
    function(value, settings) {
    	var ElementObj = $(this);
    	if ($(WarningLayer).html() == "" && ElementObj[0].revert != value) {
	    	wordXmlHttp = GetXmlHttpObject();

			  if (wordXmlHttp == null)
			  {
			    alert (errAjax);
			    return;
			  }

			  var url = 'ajax_rename_group_code.php';
				var postContent = "GroupID="+ElementObj.attr("id").replace('groupCode_', '');
				postContent += "&GroupCode="+encodeURIComponent(value);
				wordXmlHttp.onreadystatechange = function() {
					if (wordXmlHttp.readyState == 4) {
						Get_Return_Message(Trim(wordXmlHttp.responseText));
						ElementObj.html(value);
						$('span#GroupCodeNavLayer').html(value);
					}
				};
			  wordXmlHttp.open("POST", url, true);
				wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				wordXmlHttp.send(postContent);
			}
			else {
				ElementObj[0].reset();
			}
		},
		{
		    tooltip   : "<?=$Lang['libms']['FormClassMapping']['ClickToEdit']?>",
		    event : "click",
		    onblur : "submit",
		    type : "text",
		    style  : "display: inline",
		    height: "20px",
		    maxlength: 8,
		    onreset: function() {
		    	$(WarningLayer).html('');
		    	$(WarningLayer).hide();
		    }
	  	}
	);

    $(objDom).keyup(function() {
		var GroupCode = $('form input').val();
		var GroupID = $(this).attr('id').replace('groupCode_', '');

		Check_Group_Code(GroupCode,GroupID);
	});
}

}

// UI function
{
function Swap_Style(Obj) {
	var RowStyle;
	if (Obj.checked)
		RowStyle = 'selected_row';
	else
		RowStyle = 'sub_row';

	Obj.parentNode.parentNode.className = RowStyle;

	/*var CellBackground;
	if (Obj.checked)
		CellBackground = '#EFFDDB';
	else
		CellBackground = '#F4F4F4';

	Obj.parentNode.style.background = CellBackground;*/
}

function Switch_Layer(SwitchTo) {
	var MemberLayer = $('#MemberListSubLayer');
	var RuleLayer = $('#RuleDetailSubLayer');
	var TopManagementLayer = $('#TopManagementDetailSubLayer');
	var PeriodLayer = $('#PeriodDetailSubLayer');
	if (SwitchTo == 'GroupRule') {
		MemberLayer.hide();
		TopManagementLayer.hide();
		PeriodLayer.hide();
		RuleLayer.show('fast');
	}
	else if (SwitchTo == 'Member') {
		RuleLayer.hide();
		TopManagementLayer.hide();
		PeriodLayer.hide();
		MemberLayer.show('fast');
	}
	else if (SwitchTo == 'TopManagement') {
		MemberLayer.hide();
		RuleLayer.hide();
		PeriodLayer.hide();
		TopManagementLayer.show('fast');
	}
	else if (SwitchTo == 'GroupPeriod') {
		MemberLayer.hide();
		RuleLayer.hide();
		TopManagementLayer.hide();
		PeriodLayer.show('fast');
	}
}



function Add_Form_Row() {
	if (!document.getElementById('GenAddGroupRow')) {
		var TableBody = document.getElementById("GroupListTable").tBodies[0];
		var RowIndex = document.getElementById("AddGroupRow").rowIndex-1;
	  var NewRow = TableBody.insertRow(RowIndex);

	  NewRow.id = "GenAddGroupRow";

		var NewCell0 = NewRow.insertCell(0);
		var temp = '<input name="GroupName" type="text" id="GroupName" value="" class="textbox" onkeyup="Check_Group_Name();" maxlength="100" style="width:98%;"/>';
		temp += '<div id="GroupNameWarningLayer" style="display: none; color:red;"></div>';
		NewCell0.innerHTML = temp;
		
		var NewCell1 = NewRow.insertCell(1);
		var temp = '<input name="GroupCode" type="text" id="GroupCode" value="" class="textbox" onkeyup="Check_Group_Code();"  maxlength="8" style="width:98%;"/>';
		temp += '<div id="GroupCodeWarningLayer" style="display: none; color:red;"></div>';
		NewCell1.innerHTML = temp;

		//temp += '<input name="GroupCode" type="text" id="GroupCode" value="" class="textbox" style="width:48%;"/>';
		var NewCell2 = NewRow.insertCell(2);
		var temp = '<input id="SubmitGroupBtn" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="<?=$Lang['Btn']['Done']?>" onclick="Save_Group(false);"/> ';
		temp += '<input id="CancelGroupBtn" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="<?=$Lang['Btn']['Cancel']?>" onclick="Clear_Group_Form();"/>';
		NewCell2.colSpan = '5';
		NewCell2.innerHTML = temp;
	}
}

function Clear_Group_Form() {
	var TableBody = document.getElementById("GroupListTable").tBodies[0];
	var RowIndex = document.getElementById("GenAddGroupRow").rowIndex-1;
	TableBody.deleteRow(RowIndex);
}

function Show_Edit_Icon(LayerObj) {
	LayerObj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	LayerObj.style.backgroundPosition = "center right";
	LayerObj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Icon(LayerObj) {
	LayerObj.style.backgroundImage = "";
	LayerObj.style.backgroundPosition = "";
	LayerObj.style.backgroundRepeat = '';
}

function Toggle_Teaching_Options(Identity,ObjChecked) {
	var DomObj = $("input."+Identity+"TargetOption");
/*	switch(Identity) {
		case 'Teaching':
			DomObj = $("input.TeachingTargetOption");
			break;
		case 'NonTeaching':
			DomObj = $("input.NonTeachingTargetOption");
			break;
		case 'Student':
			DomObj = $("input.StudentTargetOption");
			break;
		case 'Parent':
			DomObj = $("input.ParentTargetOption");
			break;
	}*/
	if (ObjChecked) {
		DomObj.css('display','none');
	}
	else {
		DomObj.css('display','');
	}

	Toggle_Sub_Target_Row();
}

function Toggle_Sub_Target_Row() {
	if (document.getElementById('TeachingAll').checked 
		&& document.getElementById('NonTeachingAll').checked 
		&& document.getElementById('StudentAll').checked 
		&& document.getElementById('ParentAll').checked<?php if($special_feature['alumni']){ ?>
		&& document.getElementById('AlumniAll').checked<?php } ?>
	) {
		$('tbody#SubTargetRow').css('display','none');
	}
	else {
		$('tbody#SubTargetRow').css('display','');
	}
}
}

// UI function for add/ remove class teacher/ student in add/edit class
{
function Add_Selected_User(UserID,UserName) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	var UserSelected = document.getElementById('AddUserID[]');

	UserSelected.options[UserSelected.length] = new Option(UserName,UserID);

	Update_Auto_Complete_Extra_Para();
}

function Remove_Selected_User() {
	var UserSelected = document.getElementById('AddUserID[]');

	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
		if (UserSelected.options[i].selected)
			UserSelected.options[i] = null;
	}

	Get_User_List();
	Update_Auto_Complete_Extra_Para();
}

function Remove_All_User() {
	var UserSelected = document.getElementById('AddUserID[]');

	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
			UserSelected.options[i] = null;
	}

	Get_User_List();
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_Class_User() {
	var ClassUser = document.getElementById('AvalUserList[]');
	var UserSelected = document.getElementById('AddUserID[]');
	//var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	for (var i = (ClassUser.length -1); i >= 0 ; i--) {
		if (ClassUser.options[i].selected) {
			User = ClassUser.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
	    $(elOptNew).attr('has_group',$(User).attr('has_group'));
			//UserSelected.options[UserSelected.length] = new Option(User.text+'('+IdentityType+')',User.value);
			//UserSelected.options[UserSelected.length] = new Option(User.text,User.value);
			try {
	      UserSelected.add(elOptNew, UserSelected.options[UserSelected.length]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      UserSelected.add(elOptNew, UserSelected.length); // IE only
	    }
			ClassUser.options[i] = null;
		}
	}

	//Reorder_Selection_List('AddUserID[]');

	Update_Auto_Complete_Extra_Para();
}

function Add_All_User() {
	
	var ClassUser = document.getElementById('AvalUserList[]');
	var UserSelected = document.getElementById('AddUserID[]');
	//var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	// union selected and newly add user list
	if (ClassUser.length > UserSelected.length) {
		var cloneCell = document.getElementById('SelectedUserCell');

		// clone element from avaliable user list
		var cloneFrom = ClassUser.cloneNode(1);
		cloneFrom.setAttribute('id','AddUserID[]');
		cloneFrom.setAttribute('name','AddUserID[]');
		var j=0;
		for (var i = (UserSelected.length -1); i >= 0 ; i--) {
			User = UserSelected.options[i];
			var elOptNew = document.createElement('option');
			
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			$(elOptNew).attr('has_group',$(User).attr('has_group'));

	    
			//cloneFrom.options[cloneFrom.length] = new Option(User.text,User.value);
			try {
	      cloneFrom.add(elOptNew, cloneFrom.options[j]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      cloneFrom.add(elOptNew, j); // IE only
	    }
			UserSelected.options[i] = null;
			j++;
		}

		cloneCell.replaceChild(cloneFrom,UserSelected);
	}
	else {
		for (var i = (ClassUser.length -1); i >= 0 ; i--) {
			User = ClassUser.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			//cloneFrom.options[cloneFrom.length] = new Option(User.text,User.value);
			try {
	      UserSelected.add(elOptNew, UserSelected.options[UserSelected.length]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      UserSelected.add(elOptNew, UserSelected.length); // IE only
	    }
			ClassUser.options[i] = null;
		}
	}

	// update auto complete parameter list
	Update_Auto_Complete_Extra_Para();

	// empty the avaliable user list
	document.getElementById('AvalUserLayer').innerHTML = '<select name="AvalUserList[]" id="AvalUserList[]" size="10" style="width:99%" multiple="true"></select>';
	/*for (var i = (ClassUser.length -1); i >= 0 ; i--) {
		User = ClassUser.options[i];
		UserSelected.options[UserSelected.length] = new Option(User.text+'('+IdentityType+')',User.value);
		ClassUser.options[i] = null;
	}

	Reorder_Selection_List('AddUserID[]');

	Update_Auto_Complete_Extra_Para();*/

}

function Reorder_Selection_List(selectId) {
	var selectList = document.getElementById(selectId);
	for (var i = 0; i < selectList.length; i++) {
		for (var j=i; j < selectList.length; j++) {
			if (selectList.options[i].text.toLowerCase() > selectList.options[j].text.toLowerCase()) {
				var tempOption1 = new Option(selectList.options[i].text,selectList.options[i].value);
				var tempOption2 = new Option(selectList.options[j].text,selectList.options[j].value);
				selectList.options[i] = tempOption2;
				selectList.options[j] = tempOption1;
			}
		}
		//alert(selectList.options[i].text);
		/*if (opt.selected) {
			selectList.removeChild(opt);
			selectList.insertBefore(opt, selectOptions[i - 1]);
		}*/
  }
}
}

// jquery autocomplete function
{
var AutoCompleteObj;
function Init_JQuery_AutoComplete(InputID) {
	AutoCompleteObj = $("#"+InputID).autocomplete("ajax_search_user.php",
		{
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[3],li.selectValue+'('+li.extra[2]+')');
				Get_User_List();
			},
			formatItem: function(row) {
				// Hide Class and Class Number if both are empty
				if ((row[1] == 'NA' || $.trim(row[1]) == '') && (row[2] == 'NA' || $.trim(row[2]) == ''))
					return row[0] + " (Identity: " + row[3] + ")";
				else
					return row[0] + " (Identity: " + row[3] + "[" + row[1] + "-" + row[2] + "])";
			},
			maxItemsToShow: 5,
			minChars: 1,
			delay: 0,
			width: 280,
			scrollLayer: 'EditLayer'
		}
	);

	Update_Auto_Complete_Extra_Para();
}

function Update_Auto_Complete_Extra_Para() {
	ExtraPara = Get_Selected_Value('AddUserID[]');
	//for passing the group id to the ajax page
	ExtraPara['GroupID'] = document.getElementById('GroupID').value;
	AutoCompleteObj[0].autocompleter.setExtraParams(ExtraPara);
	
}

function Get_Selected_Value(SelectID,ReturnType) {
	var ReturnType = ReturnType || "JQuery";
	var str = "";
	var isFirst = true;
	var Obj = document.getElementById(SelectID);

	if (ReturnType == "JQuery") {
		for (var i=0; i< Obj.length; i++) {
			if (!isFirst) {
				str += "&";
			}
			str += Obj.options[i].value;
			isFirst = false;
		}

		var resultArray = new Array();
		resultArray[SelectID] = str.split('&');
		return resultArray;
	}
	else {
		for (var i=0; i< Obj.length; i++) {
			if (!isFirst) {
				str += "&";
			}
			str += Obj.name + '=' +Obj.options[i].value;
			isFirst = false;
		}

		return str;
	}
}
}

// thick box function
{
//on page load call tb_init
function Thick_Box_Init(){
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

// misc
{
function Pop_Group_Detail(Mode) {
	if (Mode == "Print")
		newWindow("print.php?Mode="+Mode,10);
	else {
		var formObj = document.getElementById('ExportForm');
		
		formObj.action = "print.php?Mode="+Mode;
		formObj.submit();
	}
}
}
{
function toggleRuleRow( boxName , ID){
    if(boxName.checked){
	$('.toggleable'+ID).css('display','');
    }
    else{
	$('.toggleable'+ID).hide();
    }
}
}

function click_export(){
	var original_action = document.form1.action;
	var url = "export_group_member.php";
	document.form1.action = url;
	document.form1.submit();
	document.form1.action = original_action;	
}

function click_export_all(){
	var original_action = document.form1.action;
	var url = "export_all_group.php";
	document.form1.action = url;
	document.form1.submit();
	document.form1.action = original_action;	
}
</script>

