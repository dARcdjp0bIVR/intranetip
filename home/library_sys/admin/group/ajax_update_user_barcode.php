<?php
/*
 * 	Purpose:	update INTRANET_USER.Barcode and LIBMS_USER.BarCode
 * 				update LIBMS_USER_REMARK.RemarkContent
 * 	Log
 * 
 * 	2017-02-09 [Cameron] strip slashes after call lib.php for php5.4+
 * 
 * 	2016-08-26 [Cameron] create this file
 * 	
 */
 
 if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
 $PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
 
intranet_opendb();

//****** start special handle for php5.4+
if (function_exists('get_magic_quotes_gpc') && !get_magic_quotes_gpc()) {

	// 1. strip slashes again	
	if (!function_exists('magicQuotes_stripslashes2')) {
		function magicQuotes_stripslashes2(&$value, $key) {
			$value = stripslashes($value);
		}
	}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, 'magicQuotes_stripslashes2');
}
//****** end special handle for php5.4+

$uID = $_POST['uID'];
$uID = base64_decode($uID);    
$BarCode = trim($_POST['BarCode']);
$UpdateFields = trim($_POST['UpdateFields']);
$Remarks = trim($_POST['Remarks']);

$libms = new liblms();
$li = new libdb();

function check_duplicate_barcode_in_elibplus($uID, $BarCode) {
	global $libms;	
	$sql = "SELECT UserID FROM LIBMS_USER WHERE UserID<>".PHPToSQL($uID)." AND BarCode=".PHPToSQL($BarCode);
	$result = $libms->returnResultSet($sql);
	return $result ? true : false;
}

function check_duplicate_barcode_in_intranet($uID, $BarCode) {
	global $li;	
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserID<>".PHPToSQL($uID)." AND Barcode=".PHPToSQL($BarCode);
	$result = $li->returnResultSet($sql);
	return $result ? true : false;
}


$json['success'] = false;
$json['failReason'] = '';
$dataArr['BarCode'] = PHPToSQL($BarCode);
$condition['UserID'] = PHPToSQL($uID);

if ((check_duplicate_barcode_in_elibplus($uID,$BarCode)) || (check_duplicate_barcode_in_intranet($uID, $BarCode))) {
	$json['failReason'] = $Lang['libms']['bookmanagement']['duplicate_user_barcode'];
}
else {
	$result = array();
	$result[] = $libms->UPDATE2TABLE('LIBMS_USER',$dataArr,$condition);		// update eLib+
	
	$sql = "UPDATE INTRANET_USER SET Barcode=".$li->pack_value($BarCode)." WHERE UserID=".$li->pack_value($uID);
	$result[] = $li->db_db_query($sql);		// update intrnet
	
	if ($UpdateFields == 'BarCodeAndRemarks') {
		$update = $libms->getRemarks($uID);
		if($update){
			$result[] = $libms->updateRemarks($uID,$Remarks);
		}else{
			$result[] = $libms->saveRemarks($uID,$Remarks);
		}
	}
		
	if (!in_array(false,$result)) {
		$json['success'] = true;
	}
}

echo json_encode($json);

intranet_closedb(); 
?>