<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}





// using kenneth chung
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage_ui.php");

$GroupCode = trim(stripslashes(urldecode($_REQUEST['GroupCode'])));
$GroupID = $_REQUEST['GroupID'];

intranet_opendb();

$GroupManage = new liblibrarymgmt_group_manage();

if ($GroupManage->Check_Group_Code($GroupCode,$GroupID))
	echo '1'; // Group name is good to use
else
	echo '0'; // Group name is not good to use

intranet_closedb();
?>
