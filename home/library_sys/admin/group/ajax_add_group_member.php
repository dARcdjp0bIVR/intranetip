<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}





// using kenneth chung
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage.php");

$GroupID = $_REQUEST['GroupID'];
$AddUserID = $_REQUEST['AddUserID'];

intranet_opendb();

$GroupManage = new liblibrarymgmt_group_manage();

$GroupManage->Start_Trans();

$Result = $GroupManage->Add_Group_Member($GroupID,$AddUserID);

if ($Result) {
	echo $Lang['libms']['GroupManagement']['AddGroupMemberSuccess'];
	$GroupManage->Commit_Trans();
}
else {
	echo $Lang['libms']['GroupManagement']['AddGroupMemberUnsuccess'];
	$GroupManage->RollBack_Trans();
}

intranet_closedb();
?>
