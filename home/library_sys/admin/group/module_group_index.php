<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}




// using kenneth chung
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_opendb();

$lui = new interface_html();

### Title ###
$TAGS_OBJ[] = array($Lang['libms']['GroupManagement']['IdentityGroup'],"index.php",0);
$TAGS_OBJ[] = array($Lang['libms']['GroupManagement']['ModuleGroup'],"module_group_index.php",1);
$MODULE_OBJ['title'] = $Lang['libms']['GroupManagement']['ModuleTitle'];
$lui->LAYOUT_START(); 

$GroupManageUI = new liblibrarymgmt_group_manage_ui();
$Json = new JSON_obj();

echo $GroupManageUI->Get_Module_Manage();
?>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->
<?
$lui->LAYOUT_STOP();

intranet_closedb();
?>
<script>

</script>
