<?php
// using: 
######################################################################################
#									Change Log 
######################################################################################
#
# 	20170406 Cameron: handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
# 			by stripslashes($keyword)
#	20160829 Cameron: add import user barcode function (Not implement yet)
#	20150708 Henry:	Add tag for Class Management
#	20150210 Ryan : ej DM#521 - made Keyword search follow by role filter 
#	20150203 Ryan : ej DM#521 - Fixed Student -> Select Class return No Records,  
#								Change wording to All Classes and return all students  
#	20150203 Ryan : Training Feedback  - link to borrowed book on Name field, 
#								    	 PageSize fix,
#										 Show Remark Cotnent
#										 [Case Number : 2015-0203-1543-49054]
#	20141217 Ryan : Imporved Sorting Results  ( Handle Order By Multiple Fields )
#	20141201 Ryan : eLibrary plus pending developments Item 205
#
######################################################################################

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_suspend_manage.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
################################################################################################################

### Set Cookies
$arrCookies = array();
$arrCookies[] = array("elibrary_plus_mgmt_item_page_field", "field");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_order", "order");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_size", "numPerPage");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_number", "pageNo");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_keyword", "keyword");
$arrCookies[] = array("elibrary_plus_mgmt_item_page_book_status", "BookStatus");


if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
{
	updateGetCookies($arrCookies);
}
################################################################################################################
## Get Data
$IdentityType = (isset($IdentityType) && $IdentityType != '') ? $IdentityType : '-1';
$YearClassSelect = (isset($YearClassSelect) && $YearClassSelect != '' && $IdentityType == 'Student') ? $YearClassSelect : '';
$keyword = (isset($keyword) && $keyword != '') ? trim($keyword) : '';
$FromPage = (isset($FromPage) && $FromPage != '') ? $FromPage : 'item';
if (!get_magic_quotes_gpc()) {
	$keyword 	= stripslashes($keyword);
}

if (isset($elibrary_plus_mgmt_item_page_size) && $elibrary_plus_mgmt_item_page_size != "") 
	$page_size = $elibrary_plus_mgmt_item_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == '') ? 1 : $order;
$field = ($field == "") ? 2 : $field;

# Use Library
# in all boook management PHP pages
$libms = new liblms();

# Access Checking
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['group management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$home_header_no_EmulateIE7 = true;
$linterface = new interface_html();
$li = new libdbtable2007($field, $order, $pageNo);

# Init 
$btnOption = ''; 

# Preparation
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageGroupManagement";
$TAGS_OBJ[] = array($Lang["libms"]["GroupManagement"]["ModuleTitle"],"index.php",0);
$TAGS_OBJ[] = array($Lang["libms"]["GroupManagement"]["ClassManagement"],"class_management.php",0);
$TAGS_OBJ[] = array($Lang["libms"]["SearchUser"]["ModuleTitle"],"search.php?clearCoo=1",1);
$TAGS_OBJ[] = array($Lang["libms"]["UserSuspend"]["ModuleTitle"],"suspend.php",0);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$libms->MODULE_AUTHENTICATION($CurrentPage);

# DB SQL
$NameField = $Lang['libms']['SQL']['UserNameFeild'];

# Search
$ClassCond = '';
switch($IdentityType){
	case 'Teaching':
		$_UserType = "u.UserType = 'T' AND ";
	break;
	case 'NonTeaching':
		$_UserType = "u.UserType = 'NT' AND ";
	break;
	case 'Student':
		$_UserType = "u.UserType = 'S' AND ";
		if($YearClassSelect !='')
		$ClassCond = " and 
			u.ClassName = '{$YearClassSelect}'";
	break;
	case 'Parent':
		$_UserType = "u.UserType = 'P' AND ";
	break;
	case '-1':
		$_UserType = "u.UserType <> 'A' AND u.UserType <> ' ' AND";
	break;
}
$Search = " {$_UserType} u.RecordStatus='1' {$ClassCond}";

if($keyword != ''){
	$unconverted_keyword_sql = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));		// A&<>'"\B ==> A&<>\'\"\\\\B
	$converted_keyword_sql = intranet_htmlspecialchars($keyword);
	if ($unconverted_keyword_sql == $converted_keyword_sql) {		// keyword does not contain any special character: &<>"
		$Search .= " and (
				".//Search 
				"u.BarCode like '%".$unconverted_keyword_sql."%' 
			or
				u.Userlogin like '%".$unconverted_keyword_sql."%'
			or 
				u.{$NameField} like'%".$unconverted_keyword_sql."%'
			)";
	}
	else {
		$Search .= " and (
				".//Search 
				"u.BarCode like '%".$unconverted_keyword_sql."%' 
			or
				u.Userlogin like '%".$unconverted_keyword_sql."%'
			or 
				u.{$NameField} like'%".$unconverted_keyword_sql."%'
			or
				u.BarCode like '%".$converted_keyword_sql."%' 
			or
				u.Userlogin like '%".$converted_keyword_sql."%'
			or 
				u.{$NameField} like'%".$converted_keyword_sql."%'

			)";
	}	
}

$sql = "
select 
		CONVERT( CONCAT('<a class=\'userDeatils\' data-page=\'Borrowed\' data-user=\'',u.UserID,'\' href=\'#\' onclick=\'getUserDetails(this)\'>', u.{$NameField} , '</a>') using utf8) as MemberName, 
		IF(gu.GroupTitle IS NULL or gu.GroupTitle = '', '--', gu.GroupTitle) as GroupTitle,
		IF(u.ClassName IS NULL or u.ClassName = '', '--', u.ClassName) as Class,
		IF(u.ClassNumber IS NULL or u.ClassNumber = '', '--', u.ClassNumber) as ClassNumber,
		IF(u.Barcode IS NULL or u.Barcode = '', '--', u.Barcode) as Barcode,
		CONCAT('<a class=\'userDeatils\' data-page=\'Borrowed\' data-user=\'',u.UserID,'\' href=\'#\' onclick=\'getUserDetails(this)\'>', IFNULL(bl.BorrowedCount, 0) , '</a>')as BorrowedCount,
		CONCAT('<a class=\'userDeatils\' data-page=\'Overdue\' data-user=\'',u.UserID,'\' href=\'#\' onclick=\'getUserDetails(this)\'>', IFNULL(ol.OverdueCount, 0) , '</a>')as OverdueCount,
		CONCAT('<a class=\'userDeatils\' data-page=\'Reserved\' data-user=\'',u.UserID,'\' href=\'#\' onclick=\'getUserDetails(this)\'>', IFNULL(rl.ReservedCount, 0) , '</a>')as ReservedCount,
		case   
	        when IFNULL(ur.RemarkCount, 0) = 0 then '--'   
        else RemarkContent END as RemarkCount   
from  LIBMS_USER u
".
// Group Info
"left join (
			SELECT gu.userID, g.GroupTitle from LIBMS_GROUP_USER gu
			inner join LIBMS_GROUP g on g.GroupID = gu.GroupID 
		  ) gu on gu.UserID = u.UserID
".
// Borrowed Count
"left join (
			select count(*) as BorrowedCount, UserID  from LIBMS_BORROW_LOG
			where RecordStatus = 'BORROWED' 
			group by UserID 
		  ) bl on bl.UserID = u.UserID
".
// Overdue Count 
"left join (
			SELECT count(*) as OverdueCount, UserID FROM LIBMS_BORROW_LOG bl
			inner join LIBMS_OVERDUE_LOG ol on ol.BorrowLogID  = bl.BorrowLogID
			where ol.RecordStatus = 'OUTSTANDING' 
			group by UserID
		  ) ol on ol.UserID = u.UserID
".
// Reserved Count 
"left join (
			SELECT count(*) as ReservedCount , UserID FROM LIBMS_RESERVATION_LOG
			where RecordStatus = 'READY' or RecordStatus = 'WAITING'
			group by UserID
		  ) rl on rl.UserID = u.UserID
".
// Remark Count 
"left join (
			select count(*) as RemarkCount,RemarkContent, UserID  from LIBMS_USER_REMARK
			where RemarkContent <> ''
			group by UserID 
		  ) ur on ur.UserID = u.UserID

where 
		$Search
";
## Data Table
//global $eclass_prefix;
$li->db = $eclass_prefix . "eClass_LIBMS";

$li->field_array = array("u.{$NameField}", "GroupTitle", "Class", "ClassNumber + 0", "Barcode","IFNULL(bl.BorrowedCount, 0)", "IFNULL(ol.OverdueCount, 0)", "IFNULL(rl.ReservedCount, 0)","RemarkCount");

# Order By Multiple Fields : 
switch($field){
	case 1 : 
		// sort by Grouptitle , and then Class, ClassNumber, MemberName ASC
		$li ->fieldorder2 = ",Class, ClassNumber + 0, $NameField";
	break;	
	case 2 : 
		// sort by Class , and then Class Number, MemberName ASC
		$li ->fieldorder2 = ",ClassNumber + 0, $NameField";
	break;	
	case 3 : 
		// sort by Class Number, and then GroupTitle, MemberName ASC
		$li ->fieldorder2 = ",GroupTitle, $NameField";
	break;	
	case 5 : 
		// sort by BorrowingRecord, and then Class, ClassNumber, GroupTitle, MemberName ASC
		$li ->fieldorder2 = ",IFNULL(bl.BorrowedCount, 0),Class, ClassNumber + 0, GroupTitle, $NameField";
	break;	
	case 6 : 
		// sort by OverdueRecord, and then Class, ClassNumber, GroupTitle, MemberName ASC
		$li ->fieldorder2 = ",IFNULL(ol.OverdueCount, 0),Class, ClassNumber + 0, GroupTitle, $NameField";
	break;	
	case 7 : 
		// sort by ReservedRecord, and then Class, ClassNumber, GroupTitle, MemberName ASC
		$li ->fieldorder2 = ",IFNULL(rl.ReservedCount, 0),Class, ClassNumber + 0, GroupTitle, $NameField";
	break;	
	default:
	break;	
}

$li->sql = $sql;
//debug_r($sql);
$li->no_col = sizeof($li->field_array)+1;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "eLibPlus_Search_User_Table";
$pos = 0;
$li->column_list .= "<th width='5' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang["libms"]["GroupManagement"]["Name"])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang["libms"]["GroupManagement"]["GroupTitle"])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang["libms"]["GroupManagement"]["ClassName"])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang["libms"]["report"]["classno"])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['libms']['bookmanagement']['barcode'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang["libms"]["SearchUser"]["ReturnBookStatus"]['BORROWED'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["overdue"])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang["libms"]["book_status"]["RESERVED"])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang["libms"]["SearchUser"]["Remark"])."</th>\n";

$keyword = htmlspecialchars($keyword);

$searchTag = "<input autocomplete=\"off\" name=\"keyword\" id=\"keyword\" type=\"text\" value=\"{$keyword}\"  />";
//# Search Box
//$advanceSearch = $libms -> getAdvanceSearchDiv('item');

############################################################################################################
# Top menu highlight setting
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
echo $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang["libms"]["SearchUser"]["Instruction"]);
echo '<br/>';

//$btnAry[] = array('import', 'import_user_barcode.php',$Lang["libms"]["GroupManagement"]["ImportUserBarcode"]);
//$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

# Fileter - Book Status

$all =$IdentityType == '-1' ? 'selected' : '';
$t =$IdentityType == 'Teaching' ? 'selected' : '';
$nt = $IdentityType == 'NonTeaching' ? 'selected' : '';
$s = $IdentityType == 'Student' ? 'selected' : '';
$p = $IdentityType == 'Parent' ? 'selected' : '';


$UserTypeSelectionBox = '	<select name="IdentityType" id="IdentityType" onchange="ChangeUsertype();">
								<option value="-1"'.$all.'>'.$Lang["libms"]["SearchUser"]["AllUserType"].'</option>
								<option value="Teaching"'.$t.'>'.$Lang['libms']['GroupManagement']['TeachingStaff'].'</option>
								<option value="NonTeaching"'.$nt.'>'.$Lang['libms']['GroupManagement']['SupportStaff'].'</option>
								<option value="Student"'.$s.'>'.$Lang['libms']['GroupManagement']['Student'].'</option>
								<option value="Parent"'.$p.'>'.$Lang['libms']['GroupManagement']['Parent'].'</option>
							</select>';
							
# Location filter 
$SuspendManage = new liblibrarymgmt_suspend_manage();
$show = ($IdentityType == 'Student') ? 1:0;
$YearClassSelectionBox = $SuspendManage->Get_YearClass_Selection($show,$YearClassSelect);
$UserTypeSelectionBox .= $YearClassSelectionBox;

$BookSubTab = $libms->get_search_detail_sub_tab($FromPage,$CurTab,$BookID,$UniqueID);
$RespondInfo = $libms->GET_BOOK_INFO($BookID);
$BookTitle = $RespondInfo[0]['BookTitle'];

$htmlAry['cancelBtn'] = '';
if (strtolower($FromPage)=='periodical') {
	$htmlAry['cancelBtn'] = $libms->get_book_manage_cancel_button($BookID, $FromPage);
}

?>
<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">
<script language="Javascript" src='/templates/tooltip.js'></script>
<script language="javascript">
$(document).ready(function(){
	$('html, body').animate({scrollTop:$('html').offset().top}, 'slow');
});	
function getUserDetails(obj){
	var uID = $(obj).attr('data-user');
	var Page = $(obj).attr('data-page');
	$.ajax({
        type : "POST",
        url : "search_detail.php?Page="+Page,
        data: {uID : uID},
        success : function(msg) {
				$('#SearchArea').html(msg);
        }
    });
}

function ChangeUsertype(){
	var Usertype = $('#IdentityType').val()
	if(Usertype == 'Student'){
		$('#YearClassSelect').css('display','inline-block');
		document.form1.submit();
	}else{
		$('#YearClassSelect').hide();
		$('#YearClassSelect').val("");
		document.form1.submit();
	}
}
	
function UpdateUserList(){
	document.form1.submit();
}	
</script>
<?php 
	if(strtolower($FromPage)=='book'||strtolower($FromPage)=='periodical'){
?>


<table class="form_table" width="100%">
<tr>
  <td align="left"><font face="微軟正黑體" size="+1">《 <b><?=$BookTitle?></b>》</font></td>
</tr>
</table>
<br style="clear:both" />	
<?=$BookSubTab?>
<?php } ?>
<form name="form1" id="form1" method="POST" action="search.php">
	<div class="content_top_tool"  style="float: left;">
		<?=$UserTypeSelectionBox?> <?=''//$htmlAry['contentTool']?>  
		</div>
		
		<div class="Conntent_search"><?=$searchTag?></div>
		<br style="clear:both" />
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td valign="bottom">
		
	</td>
	<td valign="bottom">
		<div class="common_table_tool">
		</div>
	</td>
	</tr>
	</table>
	<div id="SearchArea">
		<?=$li->display();?>
	</div>
	<br />
<?/*	<span class="tabletextrequire">*</span> <?= $Lang['libms']['book']['status']['remark'] ?>*/?>

	<div class="edit_bottom_v30">
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
	
	
	<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
	<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
	<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
	<input type="hidden" name="page_size_change" id="page_size_change" value="" />
	<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
	<input type="hidden" name="FromPage" id="FromPage" value="<?=$FromPage?>" />
	
	

<iframe id='lyrShim2'  scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; visibility:hidden; display:none;'></iframe>
<div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden; z-index:999999"></div>

</form>

<?php
$linterface->LAYOUT_STOP();

################################################################################################################
intranet_closedb();
?>