<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}



// using Henry
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage.php");

$LocationCode = $_REQUEST['LocationCode'];
$GroupID = $_REQUEST['GroupID'];

if ($LocationCode != "") {
	intranet_opendb();
	
	$GroupManage = new liblibrarymgmt_group_manage();
	
	$GroupManage->Start_Trans();
	
	$Result = $GroupManage->Change_Class_Mgt_Location($GroupID,$LocationCode);
	
	if ($Result) {
		echo $Lang['libms']['GroupManagement']['GroupRenamedSuccess'];
		$GroupManage->Commit_Trans();
	}
	else {
		echo $Lang['libms']['GroupManagement']['GroupRenamedUnsuccess'];
		$GroupManage->RollBack_Trans();
	}
	
	intranet_closedb();
}
else {
	echo $Lang['libms']['GroupManagement']['GroupRenamedUnsuccess'];
}
?>
