<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}



// using 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

$GroupID = $_REQUEST['GroupID'];


if (is_array($_REQUEST['Right']))
	foreach( $_REQUEST['Right'] as $v){
		list($section,$function) = explode('/', $v);
		$GroupRight[]= array( 'Section' => $section, 'Function' => $function );
	}
	


intranet_opendb();

$GroupManage = new liblibrarymgmt_group_manage();

$GroupManage->Start_Trans();


$Result = $GroupManage->Save_Class_Mgt_Group_Right($GroupID,$GroupRight);

if ($Result === false) {
	echo $Lang['libms']['GroupManagement']['GroupSavedUnsuccess'];
	$GroupManage->RollBack_Trans();
}
else {
	echo $Lang['libms']['GroupManagement']['GroupSavedSuccess'];
	$GroupManage->Commit_Trans();
}
 
intranet_closedb();
?>
