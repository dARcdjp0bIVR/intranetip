<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}






// using kenneth chung
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_group_manage.php");

$GroupName = trim(stripslashes(urldecode($_REQUEST['GroupName'])));
$GroupCode = trim(stripslashes(urldecode($_REQUEST['GroupCode'])));
//$GroupType = trim(stripslashes(urldecode($_REQUEST['GroupType'])));
$EditAfterCreate = $_REQUEST['EditAfterCreate'];
/*
echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/
intranet_opendb();


$GroupManage = new liblibrarymgmt_group_manage();

$GroupManage->Start_Trans();

$Result = $GroupManage->Create_Group($GroupName,$GroupCode);

if ($Result === false) {
	if ($EditAfterCreate) {
		echo 'false~~~~~';
	}
	echo $Lang['libms']['GroupManagement']['GroupCreatedUnsuccess'];
	$GroupManage->RollBack_Trans();
}
else {
	if ($EditAfterCreate) {
		echo $Result.'~~~~~';
	}
	echo $Lang['libms']['GroupManagement']['GroupCreatedSuccess'];
	$GroupManage->Commit_Trans();
}

intranet_closedb();
?>
