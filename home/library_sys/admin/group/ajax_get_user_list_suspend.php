<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}



// using kenneth chung
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_suspend_manage.php");

intranet_opendb();

$YearClassSelect = $_REQUEST['YearClassSelect'];
$_UserType = $_REQUEST['IdentityType'];
$Suspend = new liblibrarymgmt_suspend();
$SuspendManage = new liblibrarymgmt_suspend_manage();
$linterface = new interface_html();

echo $SuspendManage->Get_User_Selection($SuspendManage->Get_Avaliable_User_List($Suspend,$_UserType,$YearClassSelect));

intranet_closedb();
?>
