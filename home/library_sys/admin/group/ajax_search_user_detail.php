<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}



// using Ryan
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/User.php");

intranet_opendb();
$libms = new liblms();
$linterface = new interface_html();
$userObj = base64_decode($userObj);
$user = unserialize($userObj);


?>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
<div id="BorrowedDiv">
<?=$libms->Get_Borrowed_Books_Table($user,$curPage);?>
</div>
<div id="ReservedDiv">
<?=$libms->Get_Reserved_Books_Table($user,$curPage);?>
</div>
<div id="OverdueDiv">
<?=$libms->Get_Overdue_Records_Table($user,$curPage);?>
</div>
<?#debug_r($user->borrowedBooksHistory);?>
<p class="spacer"></p>
<?intranet_closedb();?>