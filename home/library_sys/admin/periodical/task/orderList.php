<?php
/*
 * 	Log
 * 	
 * 	Date:	2017-03-30 [[Cameron]]
 * 		- handle apostrophe problem for search field when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 	
 */

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


### cookies handling
$arrCookies = array();
$arrCookies[] = array("elibrary_plus_mgmt_periodical_orderList_page_field", "field");
$arrCookies[] = array("elibrary_plus_mgmt_periodical_orderList_page_order", "order");
$arrCookies[] = array("elibrary_plus_mgmt_periodical_orderList_page_size", "numPerPage");
$arrCookies[] = array("elibrary_plus_mgmt_periodical_orderList_page_number", "pageNo");
$arrCookies[] = array("elibrary_plus_mgmt_periodical_orderList_generatedSel", "generatedSel");
$arrCookies[] = array("elibrary_plus_mgmt_periodical_orderList_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}


### form post / get value
$periodicalBookId = ($_POST['PeriodicalBookID'])? $_POST['PeriodicalBookID'] : $_GET['PeriodicalBookID'];
$keyword = trim($keyword);
if (!get_magic_quotes_gpc()) {
	$keyword = stripslashes($keyword);
}


### navigation
$PAGE_NAVIGATION[] = array($Lang["libms"]["periodicalMgmt"]["Periodical"], "javascript: goCancel();");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### sub-tag
$htmlAry['subTag'] = $indexAry['subTagHtml'];


### toolbar
$htmlAry['toolbar'] = $linterface->GET_LNK_NEW("javascript: goEdit();", $Lang['Btn']['New'], "", "", "", 0);


### filter
$htmlAry['generatedSel'] = $libms_periodical->GET_PERIODICAL_GENERATED_SELECTION('generatedSel', 'generatedSel', $generatedSel, 'reloadPage();');


### search box
$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);


### current periodical info display
$htmlAry['currentPeriodicalInfoTable'] = $libms_periodical->GET_PERIODICAL_BASIC_INFO_DISPLAY($periodicalBookId);


### db table toolbar
$btnAry = array();
$btnAry[] = array('generate', 'javascript: goGenerate();', $Lang["libms"]["periodicalMgmt"]["Generate"]);
$btnAry[] = array('edit', 'javascript:checkEdit2(document.form1, \'orderIdAry[]\', \'goEdit();\');');
$btnAry[] = array('delete', 'javascript:checkRemove2(document.form1, \'orderIdAry[]\', \'goDelete();\');');
$htmlAry['dataTableToolbar'] = $linterface->Get_DBTable_Action_Button_IP25($btnAry);


### db table
$order = ($order == '') ? 1 : $order;
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("OrderStartDate", "OrderEndDate", "FirstIssue", "LastIssue", "IsGenerated", "Remarks");
$li->sql = $libms_periodical->GET_PERIODICAL_ORDER_LIST_SQL($periodicalBookId, $keyword, $generatedSel);
$li->no_col = sizeof($li->field_array) + 2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["OrderStartDate"])."</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["OrderEndDate"])."</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["FirstIssue"])."</td>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["LastIssue"])."</td>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["RemarksAndAlert"])."</td>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang["libms"]["periodicalMgmt"]["Generated"])."</td>\n";
$li->column_list .= "<th width='1'>".$li->check("orderIdAry[]")."</td>\n";
$htmlAry['dataTable'] = $li->display();


### action buttons
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn');


$linterface->LAYOUT_START($indexAry['returnMsg']);
include_once('template/'.$task.'.tmpl.php');
$linterface->LAYOUT_STOP();
?>