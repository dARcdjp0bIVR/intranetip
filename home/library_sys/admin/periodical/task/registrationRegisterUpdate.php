<?php
$periodicalBookId = $_POST['PeriodicalBookID'];
$itemId = $_POST['ItemID'];
$goPage = $_POST['goPage'];

$successAry = array();

$periodicalBookObj = new liblms_periodical_book($periodicalBookId);

$periodicalItemObj = new liblms_periodical_item($itemId);
$orderId = $periodicalItemObj->getOrderId();

$periodicalOrderObj = new liblms_periodical_order($orderId);

$bookItemAry = $libms_periodical->GET_LINKED_BOOK_INFO($periodicalBookId, $itemId);
$bookId = $bookItemAry[0]['BookID'];

// build a new book record if the periodical item has not linked to any book yet
if ($bookId == '') {
	// new book
	$dataAry = array();
	$dataAry['NoOfCopy'] = mysql_real_escape_string(htmlspecialchars(0));
	$dataAry['NoOfCopyAvailable'] = mysql_real_escape_string(htmlspecialchars(0));
	$dataAry['BookTitle'] = mysql_real_escape_string(htmlspecialchars($periodicalBookObj->getBookTitle().' ('.$periodicalItemObj->getIssue().')'));
	$dataAry['BookSubTitle'] = mysql_real_escape_string(htmlspecialchars(''));
	$dataAry['Introduction'] = mysql_real_escape_string(htmlspecialchars(''));
	$dataAry['Language'] = mysql_real_escape_string(htmlspecialchars(''));
	$dataAry['Country'] = mysql_real_escape_string(htmlspecialchars(''));
	$dataAry['Edition'] = mysql_real_escape_string(htmlspecialchars(''));
	$dataAry['PublishPlace'] = mysql_real_escape_string(htmlspecialchars($periodicalBookObj->getPublishPlace()));
	$dataAry['Publisher'] = mysql_real_escape_string(htmlspecialchars($periodicalBookObj->getPublisher()));
	$dataAry['PublishYear'] = mysql_real_escape_string(htmlspecialchars(date('Y', strtotime($periodicalItemObj->getPublishDate()))));
	$dataAry['NoOfPage'] = mysql_real_escape_string(htmlspecialchars(''));
	$dataAry['RemarkInternal'] = mysql_real_escape_string(htmlspecialchars($periodicalBookObj->getRemarks()));
	$dataAry['RemarkToUser'] = mysql_real_escape_string(htmlspecialchars($periodicalItemObj->getDetails()));
	$dataAry['CirculationTypeCode'] = mysql_real_escape_string(htmlspecialchars($periodicalBookObj->getCirculationTypeCode()));
	$dataAry['ResourcesTypeCode'] = mysql_real_escape_string(htmlspecialchars($periodicalBookObj->getResourcesTypeCode()));
	$dataAry['BookCategoryCode'] = mysql_real_escape_string(htmlspecialchars($periodicalBookObj->getBookCategoryCode()));
	$dataAry['LocationCode'] = mysql_real_escape_string(htmlspecialchars(''));
	$dataAry['Subject'] = mysql_real_escape_string(htmlspecialchars(''));
	$dataAry['Series'] = mysql_real_escape_string(htmlspecialchars($periodicalBookObj->getBookTitle()));
	$dataAry['SeriesNum'] = mysql_real_escape_string(htmlspecialchars($periodicalItemObj->getIssue()));
	$dataAry['URL'] = mysql_real_escape_string(htmlspecialchars(''));
	$dataAry['OpenBorrow'] = mysql_real_escape_string(htmlspecialchars($periodicalBookObj->getOpenBorrow()));
	$dataAry['OpenReservation'] = mysql_real_escape_string(htmlspecialchars($periodicalBookObj->getOpenReservation()));
	$dataAry['IsPeriodical'] = mysql_real_escape_string(htmlspecialchars(1));
	$dataAry['PeriodicalCode'] = mysql_real_escape_string(htmlspecialchars($periodicalBookObj->getPeriodicalCode()));
	$dataAry['PeriodicalBookID'] = mysql_real_escape_string(htmlspecialchars($periodicalBookId));
	$dataAry['PeriodicalItemID'] = mysql_real_escape_string(htmlspecialchars($itemId));
	$dataAry['ISSN'] = mysql_real_escape_string(htmlspecialchars($periodicalBookObj->getIssn()));
	$bookId = $libms_periodical->NEW_BOOK_RECORD($dataAry);
	$successAry[] = ($bookId > 0)? true : false;
	
	// reset published counter of the item
	$periodicalItemObj->setQuantity(0);
	$periodicalItemObj->save();
}


// go to the page to register book
if ($goPage == 'itemList') {
	header("Location: ../book/book_item_list.php?BookID=".$bookId."&FromPage=Periodical&clearCoo=1");
}
else if ($goPage == 'itemEdit') {
	header("Location: ../book/book_item_edit.php?BookID=".$bookId."&FromPage=Periodical");
}
?>