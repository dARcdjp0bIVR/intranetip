<?php
$periodicalBookId = $_POST['PeriodicalBookID'];
$orderIdAry = $_POST['orderIdAry'];
$numOfOrder = count($orderIdAry);

$successAry = array();
for ($i=0; $i<$numOfOrder; $i++) {
	$_orderId = $orderIdAry[$i];
	
	$_orderObj = new liblms_periodical_order($_orderId);
	$_orderObj->setRecordStatus(0);
	$successAry[$_orderId] = $_orderObj->save();
}

if (in_array(false, (array)$successAry)) {
	$returnMsgKey = 'DeleteUnsuccess';
}
else {
	$returnMsgKey = 'DeleteSuccess';
}

header('Location: ?task=orderList&PeriodicalBookID='.$periodicalBookId.'&returnMsgKey='.$returnMsgKey);
?>