<?php
/*
 * 	2017-02-09 [Cameron]
 * 		- strip slashes after call lib.php for php5.4+
 */

//****** start special handle for php5.4+
if (function_exists('get_magic_quotes_gpc') && !get_magic_quotes_gpc()) {

	// 1. strip slashes again	
	if (!function_exists('magicQuotes_stripslashes2')) {
		function magicQuotes_stripslashes2(&$value, $key) {
			$value = stripslashes($value);
		}
	}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, 'magicQuotes_stripslashes2');
}
//****** end special handle for php5.4+

$periodicalBookId = $_POST['PeriodicalBookID'];
$orderId = $_POST['OrderID'];

$orderObj = new liblms_periodical_order($orderId);
$orderObj->setPeriodicalBookId($periodicalBookId);
$orderObj->setOrderStartDate(trim($_POST['OrderStartDate']));
$orderObj->setOrderEndDate(trim($_POST['OrderEndDate']));
$orderObj->setPrice(trim($_POST['Price']));
$orderObj->setFrequencyUnit(trim($_POST['FrequencyUnit']));
$orderObj->setFrequencyNum(trim($_POST['FrequencyNum']));
$orderObj->setPublishDateFirst(trim($_POST['PublishDateFirst']));
$orderObj->setPublishDateLast(trim($_POST['PublishDateLast']));
$orderObj->setFirstIssue(trim($_POST['FirstIssue']));
$orderObj->setLastIssue(trim($_POST['LastIssue']));
$orderObj->setDistributor(trim($_POST['Distributor']));
$orderObj->setRemarks(trim($_POST['Remarks']));
$orderObj->setAlertDay(trim($_POST['AlertDay']));
$orderObj->setRecordStatus(1);
$orderId = $orderObj->save();

if ($orderId > 0) {
	$returnMsgKey = 'UpdateSuccess';
}
else {
	$returnMsgKey = 'UpdateUnsuccess';
}

header('Location: ?task=orderList&PeriodicalBookID='.$periodicalBookId.'&returnMsgKey='.$returnMsgKey);
?>