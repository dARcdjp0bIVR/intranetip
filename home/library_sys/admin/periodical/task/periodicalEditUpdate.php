<?php
/*
 * 	2017-02-09 [Cameron]
 * 		- strip slashes after call lib.php for php5.4+
 */
$periodicalBookId = $_POST['PeriodicalBookID'];
$isNew = ($periodicalBookId > 0)? false : true;

//****** start special handle for php5.4+
if (function_exists('get_magic_quotes_gpc') && !get_magic_quotes_gpc()) {

	// 1. strip slashes again	
	if (!function_exists('magicQuotes_stripslashes2')) {
		function magicQuotes_stripslashes2(&$value, $key) {
			$value = stripslashes($value);
		}
	}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, 'magicQuotes_stripslashes2');
}
//****** end special handle for php5.4+

$bookObj = new liblms_periodical_book($periodicalBookId);
$bookObj->setPeriodicalCode(trim($_POST['PeriodicalCode']));
$bookObj->setResourcesTypeCode(trim($_POST['ResourcesTypeCode']));
$bookObj->setCirculationTypeCode(trim($_POST['CirculationTypeCode']));
$bookObj->setBookTitle(trim($_POST['BookTitle']));
$bookObj->setPublisher(trim($_POST['Publisher']));
$bookObj->setPublishPlace(trim($_POST['PublishPlace']));
$bookObj->setIssn(trim($_POST['ISSN']));
//$bookObj->setAlert($_POST['Alert']);
$bookObj->setBookCategoryCode(trim($_POST['BookCategoryCode']));
$bookObj->setPurchaseByDepartment(trim($_POST['PurchaseByDepartment']));
$bookObj->setRemarks(trim($_POST['Remarks']));
$bookObj->setOpenBorrow(($_POST['OpenBorrow']==1)? 1 : 0);
$bookObj->setOpenReservation(($_POST['OpenReservation']==1)? 1 : 0);
$bookObj->setRecordStatus(1);
$periodicalBookId = $bookObj->save();

if ($periodicalBookId > 0) {
	$returnMsgKey = ($isNew)? 'AddSuccess' : 'UpdateSuccess';
}
else {
	$returnMsgKey = ($isNew)? 'AddUnsuccess' : 'UpdateUnsuccess';
}

header('Location: ?task=orderList&PeriodicalBookID='.$periodicalBookId.'&returnMsgKey='.$returnMsgKey);
?>