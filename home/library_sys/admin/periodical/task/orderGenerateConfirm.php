<?php
$periodicalBookId = ($_POST['PeriodicalBookID'])? $_POST['PeriodicalBookID'] : $_GET['PeriodicalBookID'];
$orderIdAry = $_POST['orderIdAry'];


### navigation
$PAGE_NAVIGATION[] = array($Lang["libms"]["periodicalMgmt"]["Periodical"], "javascript: goCancel();");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### sub-tag
$htmlAry['subTag'] = $indexAry['subTagHtml'];


### current periodical info display
$htmlAry['currentPeriodicalInfoTable'] = $libms_periodical->GET_PERIODICAL_BASIC_INFO_DISPLAY($periodicalBookId);


### remarks table
$htmlAry['remarksTable'] = $linterface->Get_Warning_Message_Box($Lang["libms"]["periodicalMgmt"]["Remarks"], $Lang["libms"]["periodicalMgmt"]["OrderGenerateRemarks"], $others="");


### generated order warning table
$warningAry = array();
$warningAry[] = $Lang["libms"]["periodicalMgmt"]["OrderGenerateWarning1"]; 
$warningAry[] = $Lang["libms"]["periodicalMgmt"]["OrderGenerateWarning2"];
$generatedOrderWarningTableHtml = $linterface->Get_Warning_Message_Box('', $warningAry, $others="");


### generate periodical order item info
$orderInfoAry = $libms_periodical->GET_PERIODICAL_ORDER_INFO($orderIdAry);
$numOfOrder = count($orderInfoAry);
$canSubmit = true;
$x = '';
for ($i=0; $i<$numOfOrder; $i++) {
	$_orderId = $orderInfoAry[$i]['OrderID'];
	
	$_publishDateFirst = $orderInfoAry[$i]['PublishDateFirst'];
	$_publishDateLast = $orderInfoAry[$i]['PublishDateLast'];
	$_frequencyUnit = $orderInfoAry[$i]['FrequencyUnit'];
	$_frequencyNum = $orderInfoAry[$i]['FrequencyNum'];
	$_firstIssue = $orderInfoAry[$i]['FirstIssue'];
	$_isGenerated = $orderInfoAry[$i]['IsGenerated'];
	
	$_warningMsgBox = '';
	if ($_isGenerated) {
		$_warningMsgBox = $generatedOrderWarningTableHtml;
	}
	
	$_publishDateAry = $libms_periodical->GET_REPEATED_DATE_ARRAY($_publishDateFirst, $_publishDateLast, $_frequencyUnit, $_frequencyNum);
	$_numOfIssue = count((array)$_publishDateAry);
	
	$x .= '<div>'."\r\n";
		$x .= '<div>'."\r\n";
			$x .= $linterface->GET_NAVIGATION2_IP25($Lang["libms"]["periodicalMgmt"]["From"].' '.$_publishDateFirst.' '.$Lang["libms"]["periodicalMgmt"]["To"].' '.$_publishDateLast);
		$x .= '</div>'."\r\n";
		$x .= '<br style="clear:both;">'."\r\n";
		
		$x .= '<div>'."\r\n";
			$x .= $_warningMsgBox;
		$x .= '</div>'."\r\n";
		$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
			$x .= '<thead>'."\r\n";
		        $x .= '<tr>'."\r\n";
		   	  		$x .= '<th class="num_check">#</th>'."\r\n";
		   	  		$x .= '<th style="width:48%;">'.$Lang["libms"]["periodicalMgmt"]["PublishDate"].'</th>'."\r\n";
		   	  		$x .= '<th style="width:48%;">'.$Lang["libms"]["periodicalMgmt"]["IssueOrCode"].'</th>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</thead>'."\r\n";
			
			$x .= '<tbody>'."\r\n";
				if ($_numOfIssue == 0) {
					$canSubmit = false;
					$x .= '<tr><td colspan="3" style="text-align:center;">'.$Lang["libms"]["periodicalMgmt"]["NoIssueCanBeGenerated"].'</td></tr>'."\r\n";
				}
				else {
					for ($j=0; $j<$_numOfIssue; $j++) {
						$__publishDate = $_publishDateAry[$j];
						$__issueNum = $_firstIssue + $j;
						
						$x .= '<tr>'."\r\n";
							$x .= '<td>'.($j + 1).'</td>'."\r\n";
				   	  		$x .= '<td>'.$__publishDate.'</td>'."\r\n";
				   	  		$x .= '<td>'.$__issueNum.'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
					}
				}
			$x .= '</tbody>'."\r\n";
		$x .= '</table>'."\r\n";
	$x .= '</div>'."\r\n";
	$x .= '<br style="clear:both;">'."\r\n";
}
$htmlAry['orderItemInfoTable'] = $x;


### action buttons
$htmlAry['generateBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Generate'], "button", "goGenerate()", 'generateBtn', '', $disabled=!$canSubmit);
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn');


$linterface->LAYOUT_START($indexAry['returnMsg']);
include_once('template/'.$task.'.tmpl.php');
$linterface->LAYOUT_STOP();
?>