<?php
$periodicalBookId = $_POST['PeriodicalBookID'];
$itemIdAry = $_POST['itemIdAry'];
$numOfItem = count($itemIdAry);

$successAry = array();
for ($i=0; $i<$numOfItem; $i++) {
	$_itemId = $itemIdAry[$i];
	
	$_itemObj = new liblms_periodical_item($_itemId);
	$_itemObj->setRecordStatus(0);
	$successAry[$_itemId] = $_itemObj->save();
}

if (in_array(false, (array)$successAry)) {
	$returnMsgKey = 'DeleteUnsuccess';
}
else {
	$returnMsgKey = 'DeleteSuccess';
}

header('Location: ?task=registrationList&PeriodicalBookID='.$periodicalBookId.'&returnMsgKey='.$returnMsgKey);
?>