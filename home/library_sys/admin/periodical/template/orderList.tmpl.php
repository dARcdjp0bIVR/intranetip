<script type="text/javascript">
$(document).ready( function() {
	
});

function clickedSubTag(targetTask) {
	var periodicalBookId = $('input#PeriodicalBookID').val();
	var canSubmit = true;
	
	if (canSubmit) {
		window.location = '?clearCoo=1&task=' + targetTask + '&PeriodicalBookID=' + periodicalBookId;
	}
}

function reloadPage() {
	$('input#task').val('orderList');
	$('input#task_update').val('');
	$('form#form1').attr('action', 'index.php').submit();
}

function goCancel() {
	window.location = 'index.php';
}

function goEdit(orderId) {
	if (orderId == null) {
		var chkName = getJQuerySaveId('orderIdAry[]');
		orderId = $('input[name="'+ chkName +'"]:checked').val();
	}
	$('input#OrderID').val(orderId);
	
	$('input#task').val('orderEdit');
	$('input#task_update').val('');
	$('form#form1').attr('action', 'index.php').submit();
}

function goDelete() {
	$('input#task').val('orderDeleteUpdate');
	$('input#task_update').val('');
	$('form#form1').attr('action', 'index.php').submit();
}

function goGenerate() {
	var canSubmit = true;
	var chkName = getJQuerySaveId('orderIdAry[]');
	if ($('input[name="'+ chkName +'"]:checked').length == 0) {
		canSubmit = false;
		alert('<?=$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["SelectItem"]?>');
	}
	
	if (canSubmit) {
		$('input#task').val('orderGenerateConfirm');
		$('input#task_update').val('');
		$('form#form1').attr('action', 'index.php').submit();
	}
}
</script>
<form name="form1" id="form1" method="POST" action="index.php?task=orderList">
	<div class="navigation"><?=$htmlAry['navigation']?></div>
	<br style="clear:both;" />
	
	<?=$htmlAry['subTag']?>
	<p class="spacer"></p>
	<br style="clear:both" />
	
	<div class="table_board">
		<div>
			<?=$htmlAry['currentPeriodicalInfoTable']?>
		</div>
	</div>
	<br style="clear:both" />
	
	<div class="content_top_tool">
		<div style="float:left;">
			<?=$htmlAry['toolbar']?>
		</div>
		<?=$htmlAry['searchBox']?>
	</div>
	<br style="clear:both" />
	
	<div class="table_filter">
		<?=$htmlAry['generatedSel']?>
	</div>
	<br style="clear:both" />
	
	<div class="table_board">
		<div>
			<?=$htmlAry['dataTableToolbar']?>
			<?=$htmlAry['dataTable']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
		<br style="clear:both;" />
	</div>
	<br />
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="task_update" name="task_update" value="" />
	<input type="hidden" id="pageNo" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" id="order" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" id="field" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" id="page_size_change" name="page_size_change" value="" />
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>" />
	<input type="hidden" id="PeriodicalBookID" name="PeriodicalBookID" value="<?=$periodicalBookId?>" />
	<input type="hidden" id="OrderID" name="OrderID" value="" />
	<input type="hidden" id="clearCoo" name="clearCoo" value="" />
</form>