<script language="javascript">
$(document).ready(function () {
	
});

function goEdit(periodicalBookId) {
	if (periodicalBookId == null) {
		var chkName = getJQuerySaveId('PeriodicalBookIdAry[]');
		periodicalBookId = $('input[name="'+ chkName +'"]:checked').val();
	}
	$('input#PeriodicalBookID').val(periodicalBookId);
	
	$('input#task').val('periodicalEdit');
	$('input#task_update').val('');
	$('form#form1').attr('action', 'index.php').submit();
}

function goOrderList(periodicalBookId) {
	$('input#PeriodicalBookID').val(periodicalBookId);
	
	$('input#task').val('orderList');
	$('input#task_update').val('');
	$('form#form1').attr('action', 'index.php').submit();
}

function goRegistartionList(periodicalBookId) {
	$('input#PeriodicalBookID').val(periodicalBookId);
	
	$('input#task').val('registrationList');
	$('input#task_update').val('');
	$('form#form1').attr('action', 'index.php').submit();
}

function goDelete() {
	$('input#task').val('');
	$('input#task_update').val('periodicalDeleteUpdate');
	$('form#form1').attr('action', 'index.php').submit();
}
</script>
<form name="form1" id="form1" method="POST" action="index.php">
	<div class="content_top_tool">
		<div style="float:left;">
			<?=$htmlAry['toolbar']?>
		</div>
		<?=$htmlAry['searchBox']?>
	</div>
	<br style="clear:both" />
	
	<div class="table_board">
		<div>
			<?=$htmlAry['dataTableToolbar']?>
			<?=$htmlAry['dataTable']?>
		</div>
		<br style="clear:both;" />
	</div>
	<br />
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="task_update" name="task_update" value="" />
	<input type="hidden" id="pageNo" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" id="order" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" id="field" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" id="page_size_change" name="page_size_change" value="" />
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>" />
	<input type="hidden" id="PeriodicalBookID" name="PeriodicalBookID" value="" />
</form>