<script type="text/javascript">
$(document).ready( function() {
	updateLastIssueDisplay();
	saveFormValues();	// for edited form checking
});

function clickedSubTag(targetTask) {
	var periodicalBookId = $('input#PeriodicalBookID').val();
	var canSubmit = true;
	
	if (isFormChanged()) {
		if (confirm('<?=$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["FormChanged"]?>')) {
			// do nth
		}
		else {
			canSubmit = false;
		}
	}
	
	if (canSubmit) {
		window.location = '?clearCoo=1&task=' + targetTask + '&PeriodicalBookID=' + periodicalBookId;
	}
}

function goCancel() {
	var canSubmit = true;
	if (isFormChanged()) {
		if (confirm('<?=$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["FormChanged"]?>')) {
			// do nth
		}
		else {
			canSubmit = false;
		}
	}
	
	if (canSubmit) {
		$('input#task').val('orderList');
		$('input#task_update').val('');
		$('form#form1').submit();
	}
}

function updateLastIssueDisplay() {
	$('span#lastIssueSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.post(
		"index.php", 
		{ 
			task_ajax: 'ajax_task',
			ActionType: "countNumOfIssueInOrder",
			PublishDateFirst: $('input#PublishDateFirst').val(),
			PublishDateLast: $('input#PublishDateLast').val(),
			FrequencyUnit: $('select#FrequencyUnitSel').val(),
			FrequencyNum: $('select#FrequencyNum').val()
		},
		function(ReturnData) {
			var firstIssueNum = $('input#FirstIssueTb').val();
			var lastIssueNum = parseInt(firstIssueNum) + parseInt(ReturnData) - 1; 
			
			if (lastIssueNum < 1 || isNaN(lastIssueNum)) {
				lastIssueNum = '---';
			}
			
			$('span#lastIssueSpan').html(lastIssueNum);
		}
	);
}

function goSave() {
	var canSubmit = true;
	
	$('input#saveBtn').attr('disabled', 'disabled');
	$('input#cancelBtn').attr('disabled', 'disabled');
	$('div.warnMsgDiv').hide();
	
	var orderStartDate = $('input#OrderStartDate').val(); 
	var orderEndDate = $('input#OrderEndDate').val();
	var publishDateFirst = $('input#PublishDateFirst').val(); 
	var publishDateLast = $('input#PublishDateLast').val();
	
	if (orderStartDate >= orderEndDate) {
		$('input#OrderStartDate').focus();
		$('div#OrderDateInvalidWarnDiv').show();
		canSubmit = false;
	}
	
	if (parseInt($('input#PriceTb').val()) < 0) {
		$('input#PriceTb').focus();
		$('div#PricePositiveWarnDiv').show();
		canSubmit = false;
	}
	
	if (publishDateFirst >= publishDateLast) {
		$('input#PublishDateFirst').focus();
		$('div#PublishDateInvalidWarnDiv').show();
		canSubmit = false;
	}
	
	if (parseInt($('input#FirstIssueTb').val()) < 0) {
		$('input#FirstIssueTb').focus();
		$('div#FirstIssuePositiveWarnDiv').show();
		canSubmit = false;
	}
	
	if (!canSubmit) {
		$('input#saveBtn').attr('disabled', '');
		$('input#cancelBtn').attr('disabled', '');
	}
	else {
		var lastIssueNum = $('span#lastIssueSpan').html();
		if (isNaN(lastIssueNum)) {
			lastIssueNum = 0;
		}
		$('input#LastIssue').val(lastIssueNum);
		
		$('input#task').val('');
		$('input#task_update').val('orderEditUpdate');
		$('form#form1').submit();
	}
}
</script>
<form name="form1" id="form1" method="POST" action="index.php">
	<div class="navigation"><?=$htmlAry['navigation']?></div>
	<br style="clear:both;" />
	
	<?=$htmlAry['subTag']?>
	<p class="spacer"></p>
	<br style="clear:both" />
	
	<div class="table_board">
		<div>
			<?=$htmlAry['currentPeriodicalInfoTable']?>
		</div>
	
		<div>
			<?=$htmlAry['orderEditTable']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<?=$htmlAry['saveBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
		<br style="clear:both;" />
	</div>
	<br />
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="task_update" name="task_update" value="" />
	<input type="hidden" id="PeriodicalBookID" name="PeriodicalBookID" value="<?=$periodicalBookId?>" />
	<input type="hidden" id="OrderID" name="OrderID" value="<?=$orderId?>" />
	<input type="hidden" id="LastIssue" name="LastIssue" value="" />
</form>