<?php
// using:
/*
 * 	Log
 * 
 * 	2016-04-14 [Cameron]
 * 		- pass DirectMsg to returnMsg
 * 
 */
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liblog.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_config.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_periodical.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_periodical_book.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_periodical_order.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_periodical_item.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_auth();
intranet_opendb();


$libms = new liblms();
$libms_periodical = new liblms_periodical();
$linterface = new interface_html();

$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];
if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ) {
	No_Access_Right_Pop_Up();
}


if($task_ajax){
	//header("Content-type: text/html; charset=".returnCharset());
	include('ajax/'.$task_ajax.".php");
}else{
	if ($task_update != '') {
		$task = $task_update;
	}
	$task = $task ? $task : 'periodicalList';
	
	$CurrentPageArr['LIBMS'] = 1;
//	$CurrentPage = "PagePeriodicalManagement".ucfirst($task);
//	$libms->MODULE_AUTHENTICATION($CurrentPage);
	$CurrentPage = "PagePeriodicalManagement";
	
	### tag
	$TAGS_OBJ[] = array($Lang["libms"]["periodicalMgmt"]["Periodical"]);
//	$TAGS_OBJ[] = array($Lang["libms"]["periodicalMgmt"]["Basic"], '', substr($task, 0, 10)=='periodical');
//	$TAGS_OBJ[] = array($Lang["libms"]["periodicalMgmt"]["Ordering"], '?task=orderList', substr($task, 0, 5)=='order');
//	$TAGS_OBJ[] = array($Lang["libms"]["periodicalMgmt"]["Registration"], '?task=regitrationList', substr($task, 0, 12)=='registration');

	### sub-tag
	$subTagAry = array();
	$subTagAry[] = array($Lang["libms"]["periodicalMgmt"]["Basic"], 'javascript: clickedSubTag(\'periodicalEdit\');', substr($task, 0, 10)=='periodical');
	$subTagAry[] = array($Lang["libms"]["periodicalMgmt"]["Ordering"], 'javascript: clickedSubTag(\'orderList\');', substr($task, 0, 5)=='order');
	$subTagAry[] = array($Lang["libms"]["periodicalMgmt"]["Registration"], 'javascript: clickedSubTag(\'registrationList\');', substr($task, 0, 12)=='registration');
	$indexAry['subTagHtml'] = $linterface->GET_SUBTAGS($subTagAry);
	
	$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
	
	$returnMsgKey = $_GET['returnMsgKey'];
	$indexAry['returnMsg'] = ($DirectMsg == 'T') ? $returnMsgKey: $Lang["libms"]["periodicalMgmt"]["ReturnMessage"][$returnMsgKey];
	
	include('task/'.$task.".php");
}

intranet_closedb();
?>