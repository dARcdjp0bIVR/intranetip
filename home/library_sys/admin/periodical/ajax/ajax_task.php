<?php
$actionType = $_POST['ActionType'];

if ($actionType == 'validatePeriodicalCode') {
	$periodicalCode = trim($_POST['PeriodicalCode']);
	$excludePeriodicalBookId = trim($_POST['PeriodicalBookID']);
	
	$periodicalAry = $libms_periodical->GET_PERIODICAL_INFO($bookIdAry='', $periodicalCode, $excludePeriodicalBookId);
	$numOfPeriodical = count($periodicalAry);
	
	$returnVal = ($numOfPeriodical == 0)? '1' : '0';
}
else if ($actionType == 'countNumOfIssueInOrder') {
	$startDate = $_POST['PublishDateFirst'];
	$endDate = $_POST['PublishDateLast'];
	$frequencyUnit = $_POST['FrequencyUnit'];
	$frequencyNum = $_POST['FrequencyNum'];
	
	$dateAry = $libms_periodical->GET_REPEATED_DATE_ARRAY($startDate, $endDate, $frequencyUnit, $frequencyNum);
	$returnVal = count((array)$dateAry);
}
//else if ($actionType == 'getUniqueIdByItemId') {
//	$itemId = $_POST['ItemID'];
//	$bookItemAry = $libms_periodical->GET_REPEATED_DATE_ARRAY($itemId);
//	$returnVal = $bookItemAry[0]['UniqueID'];
//}
else if ($actionType == 'validatePublishDate') {
	$publishDate = $_POST['PublishDate'];
	$periodicalBookId = $_POST['PeriodicalBookID'];
	$excludeItemId = $_POST['ItemID'];
	
	$itemAry = $libms_periodical->GET_PERIODICAL_ITEM_INFO($periodicalBookId, $publishDate, $excludeItemId);
	$numOfItem = count($itemAry);
	
	$returnVal = ($numOfItem == 0)? '1' : '0';
}


echo $returnVal;
?>