<?php
# using: 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_config.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_periodical.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt_periodical_order.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $Lang['libms']['library'].' '.$Lang["libms"]["periodicalMgmt"]["PeriodicalRenewAlert"];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$libms_periodical = new liblms_periodical();
$orderAry = $libms_periodical->GET_PERIODICAL_RENEW_ALERT_INFO();
$numOfOrder = count((array)$orderAry);
$x = '';
$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
	$x .= '<thead>'."\r\n";
        $x .= '<tr>'."\r\n";
   	  		$x .= '<th class="num_check">#</th>'."\r\n";
   	  		$x .= '<th style="width:15%;">'.$Lang["libms"]["periodicalMgmt"]["PeriodicalCode"].'</th>'."\r\n";
   	  		$x .= '<th style="width:30%;">'.$Lang["libms"]["periodicalMgmt"]["PeriodicalTitle"].'</th>'."\r\n";
   	  		$x .= '<th style="width:10%;">'.$Lang["libms"]["periodicalMgmt"]["OrderEndDate"].'</th>'."\r\n";
   	  		$x .= '<th style="width:10%;">'.$Lang["libms"]["periodicalMgmt"]["LastPublishDate"].'</th>'."\r\n";
   	  		$x .= '<th style="width:35%;">'.$Lang["libms"]["periodicalMgmt"]["Alert"].'</th>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</thead>'."\r\n";
	
	$x .= '<tbody>'."\r\n";
		for ($i=0; $i<$numOfOrder; $i++) {
			$_periodicalBookId = $orderAry[$i]['PeriodicalBookID'];
			$_periodicalCode = $orderAry[$i]['PeriodicalCode'];
			$_bookTitle = $orderAry[$i]['BookTitle'];
			$_orderEndDate = $orderAry[$i]['OrderEndDate'];
			$_publishDateLast = $orderAry[$i]['PublishDateLast'];
			$_remarkToUser = $orderAry[$i]['Remarks'];
			
			$_orderEndDate = ($_orderEndDate=='')? '&nbsp;' : $_orderEndDate;
			$_publishDateLast = ($_publishDateLast=='')? '&nbsp;' : $_publishDateLast;
			$_remarkToUser = ($_remarkToUser=='')? '&nbsp;' : $_remarkToUser;
			
			$x .= '<tr>'."\r\n";
				$x .= '<td>'.($i + 1).'</td>'."\r\n";
	   	  		$x .= '<td>'.$_periodicalCode.'</td>'."\r\n";
	   	  		$x .= '<td><a href="javascript:goPeriodicalOrder(\''.$_periodicalBookId.'\');" class="tablelink">'.$_bookTitle.'</a></td>'."\r\n";
	   	  		$x .= '<td>'.$_orderEndDate.'</td>'."\r\n";
	   	  		$x .= '<td>'.$_publishDateLast.'</td>'."\r\n";
	   	  		$x .= '<td>'.$_remarkToUser.'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		}
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['infoTable'] = $x;

$htmlAry['closeBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Close'], "button", "window.close();", 'cancelBtn');
?>
<script type="text/javascript">
function goPeriodicalOrder(periodicalBookId) {
	window.opener.location = "index.php?clearCoo=1&task=orderList&PeriodicalBookID=" + periodicalBookId;
}
</script>
<form name="form1" id="form1" method="POST" action="index.php">
	<div class="table_board">
		<div>
			<?=$htmlAry['infoTable']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<?=$htmlAry['closeBtn']?>
			<p class="spacer"></p>
		</div>
		<br style="clear:both;" />
	</div>
	<br />
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>