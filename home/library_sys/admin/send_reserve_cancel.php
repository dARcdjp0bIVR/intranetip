<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

/**
 * send email about reserve cancellation notification
 * @param array $ReservationIDs - Array of Reservation IDs that have been cancalled
 *  
 */

function send_reserve_cancel($ReservationIDs){
	$libms = new liblms();
	
	if(!is_array($ReservationIDs)){
		$ReservationIDs = array($ReservationIDs);
	}
	$imploded_CodesList = '"'.implode('","',$ReservationIDs).'"';
	$sql = <<<EOL
		SELECT `BookTitle`, `UserID`
		FROM `LIBMS_RESERVATION_LOG` rl
		JOIN `LIBMS_BOOK` b
			ON rl.`BookID` = b.`BookID`
		WHERE `ReservationID` IN ($CodesList);
EOL;
	
	$result = $libms->returnArray($sql);
	
	foreach($result as $row){
		$subject = $Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove']['subject'];
		$body  =  sprintf($Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove']['mail_body'], date(),$row['BookTitle']);
	
		do_email($userID, $subject, $body);
	}
	
}