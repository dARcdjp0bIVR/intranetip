<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();

/*
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}
//*/
if(sizeof($_POST)==0) {
	header("Location: special_open_time.php");	
}

for($i=0; $i<$_POST['count']; $i++){
$dataAry[$i]['Code'] = intranet_htmlspecialchars($_POST['Code_'.$i]);
$dataAry[$i]['DateFrom'] = intranet_htmlspecialchars($_POST['DateFrom_'.$i]);
$dataAry[$i]['DateEnd'] = intranet_htmlspecialchars($_POST['DateEnd_'.$i]);
$dataAry[$i]['OpenTime'] = ((($_POST['OpenTime_'.$i]) == "")?'NULL':$_POST['OpenTime_'.$i]);
$dataAry[$i]['CloseTime'] = ((($_POST['CloseTime_'.$i]) == "")?'NULL':$_POST['CloseTime_'.$i]);
$dataAry[$i]['Open'] = intranet_htmlspecialchars($_POST['checkopen_'.$i]);
}

 
$result = $libms->UPDATE_SPECIAL_OPENTIME($dataAry);

intranet_closedb();

header("Location: special_open_time.php?xmsg=update");

?>
