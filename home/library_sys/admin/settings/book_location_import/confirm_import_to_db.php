<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
 if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
 {
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
$laccessright = new libaccessright();
$laccessright->NO_ACCESS_RIGHT_REDIRECT();
exit;
}
*/
$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_ist'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
$CurrentPage = "PageSettingsBookLocation";

$linterface = new interface_html("libms.html");

//$linterface = new interface_html();
############################################################################################################
///*


global $eclass_prefix;

$toolbar = '';//$linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'label_format_new.php')",$button_new,"","","",0);

//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array($Lang['libms']['settings']['book_location']);
 

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='../book_location.php'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import_csv_data.php'");



//===================================================================================

############################################################################################################


############################################################################################################


$libel = new elibrary();
if (isset($junior_mck))
{
	$libel->dbConnectionLatin1();	
}

$counter = 0;

$sql = <<<EOL
REPLACE INTO  `LIBMS_LOCATION`
SELECT *
FROM  `LIBMS_LOCATION_TMP`
EOL;

$result = $libms->db_db_query($sql);


// $sql = 'SELECT count(*) FROM  `LIBMS_LOCATION_TMP`';
// $result = $libms ->returnArray($sql);
// $count = $result[0][0];

// $count = mysql_affected_rows();
//dump ($result);
$x = $Lang['libms']['import_book']['Result']['Success']. ".";// . $Lang['libms']['import_book']['total_record'].":".$count;

$i = 1;

$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<br />
<form name="form1" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5">
		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center"><br /> <br /> <br /> <?= $x ?></td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5"
					align="center">
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td align="center" colspan="2"><?=$import_button?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form><?php

$linterface->LAYOUT_STOP();

intranet_closedb();


?>