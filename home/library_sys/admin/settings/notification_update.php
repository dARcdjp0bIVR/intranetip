<?php

# using by 
/*
 * 	Log
 * 
 * 	2016-11-22 [Cameron]
 * 		- fix bug: should initialize $result=array(), otherwise, in_array cause error
 * 
 * 	2016-09-29 [Cameron]
 * 		- Add insert record if record not exist [case #J105337]
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();
//dump($_POST);
if( empty( $_POST['notify'] ) ) {
	header("Location: notification.php");	
}
 

$SQL = "UPDATE `LIBMS_NOTIFY_SETTING` SET `enable` = 0";
$libms->db_db_query($SQL);
$result = array();

foreach( $_POST['notify'] as $key => $value){
	if(!empty($value)){
		$sanitized_name = PHPToSQL($key);
		$sql = "SELECT name FROM LIBMS_NOTIFY_SETTING where name={$sanitized_name}";
		$check = $libms->returnResultSet($sql);
		if (count($check)==0) {	// record not exist, insert it
			$sql = "INSERT INTO `LIBMS_NOTIFY_SETTING` (`name`,`enable`) Values ($sanitized_name, 1)";
			$result[] = $libms->db_db_query($sql);	
		}
		else {
			$SQL = "UPDATE `LIBMS_NOTIFY_SETTING` SET `enable` = 1 WHERE `name` = {$sanitized_name} ";
			$result[] = $libms->db_db_query($SQL);
		}
	}
}

if($_POST['notify2']){
	foreach( $_POST['notify2'] as $key => $value){
			$sanitized_value = PHPToSQL($value);
			$sanitized_name = PHPToSQL($key);
			$sql = "SELECT name FROM LIBMS_SYSTEM_SETTING where name={$sanitized_name}";
			$check = $libms->returnResultSet($sql);
			if (count($check)==0) {	// record not exist, insert it
				switch($key) {
					case 'push_message_due_date_reminder':
						$sql = "INSERT INTO `LIBMS_SYSTEM_SETTING` (`name`,`type`,`value`) Values ({$sanitized_name}, 'INT', {$sanitized_value})";
						$result[] = $libms->db_db_query($sql);	
						break;
					case 'push_message_time':
						$sql = "INSERT INTO `LIBMS_SYSTEM_SETTING` (`name`,`type`,`value`) Values ({$sanitized_name}, 'STRING', {$sanitized_value})";
						$result[] = $libms->db_db_query($sql);	
						break;
					case 'due_date_reminder':
						$sql = "INSERT INTO `LIBMS_SYSTEM_SETTING` (`name`,`type`,`value`) Values ({$sanitized_name}, 'INT', {$sanitized_value})";
						$result[] = $libms->db_db_query($sql);	
						break;
				}				
			}
			else {
				$SQL = "UPDATE `LIBMS_SYSTEM_SETTING` SET `value` = {$sanitized_value} WHERE `name` = {$sanitized_name} ";
				$result[] = $libms->db_db_query($SQL);
			}
	} 
}

$xmsg = in_array(false,$result) ? 'UpdateUnsuccess' : 'UpdateSuccess';
 	
intranet_closedb();
header("Location: notification.php?xmsg=$xmsg");

?>
