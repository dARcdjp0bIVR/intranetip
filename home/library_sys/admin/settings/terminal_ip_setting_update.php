<?php

# using by 
/*
 * 	Log
 * 
 * 	2017-03-03 [Cameron]
 * 		- create this file
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();
if( empty( $_POST['IPList'] ) ) {
	header("Location: terminal_ip_setting.php");	
}

$table = 'LIBMS_SECURITY_SETTING'; 
$field = 'SettingValue';
$dataAry=array();
$condition = array();
$result = array();
$condition['SettingName'] = PHPToSQL('IPList'); 
$rs = $libms->SELECTFROMTABLE($table,$field,$condition);
if (count($rs) == 1) {	// update
	$dataAry['SettingValue'] = PHPToSQL($_POST['IPList']);
	$result[] = $libms->UPDATE2TABLE($table,$dataAry,$condition);
	
}
else {	// insert
	$dataAry['SettingName'] = PHPToSQL('IPList');
	$dataAry['SettingValue'] = PHPToSQL($_POST['IPList']);
	$result[] = $libms->INSERT2TABLE($table,$dataAry);
}

$xmsg = in_array(false,$result) ? 'UpdateUnsuccess' : 'UpdateSuccess';
 	
intranet_closedb();
header("Location: terminal_ip_setting.php?xmsg=$xmsg");

?>
