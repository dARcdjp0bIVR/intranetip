<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();
if( empty( $_POST['notify'] ) ) {
	header("Location: system_settings.php");	
}
 

//$SQL = "UPDATE `LIBMS_NOTIFY_SETTING` SET `enable` = 0";
//$libms->db_db_query($SQL);

//Henry modifying
//foreach( $_POST['notify'] as $key => $value){
//	if($key == "max_book_reserved_limit"){
//		$prevRservedLimit = $libms->get_system_setting($key);
//		break;
//	}
//}

foreach( $_POST['notify'] as $key => $value){
		$sanitized_value = PHPToSQL($value);
		$sanitized_name = PHPToSQL($key);
		$SQL = "UPDATE `LIBMS_SYSTEM_SETTING` SET `value` = {$sanitized_value} WHERE `name` = {$sanitized_name} ";
		$libms->db_db_query($SQL);
}

//Henry modifying
//foreach( $_POST['notify'] as $key => $value){
//	if($key == "max_book_reserved_limit"){
//		$RservedLimit = $libms->get_system_setting($key);
//
//		break;
//	}
//}

intranet_closedb();
header("Location: system_settings.php?xmsg=update");

?>
