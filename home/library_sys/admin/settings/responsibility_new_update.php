<?php
/*
 * 	Log
 * 
 *	2017-09-12 Cameron
 *		- trim fields so the it's consistent with batch update [case #F125374]
 *
 * 	2017-04-06 Cameron
 * 		- don't apply mysql_real_escape_string() to description fields when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 
 * 	2017-01-18 Cameron
 * 		- call replaceSymbolByHyphen() for code field 
 * 	
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();


/*
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}
//*/

if(sizeof($_POST)==0) {
	header("Location: responsibility.php");	
}

$ResponsibilityCode = replaceSymbolByHyphen(trim($ResponsibilityCode));
$dataAry['ResponsibilityCode'] = intranet_htmlspecialchars($ResponsibilityCode);
$dataAry['DescriptionEn'] = intranet_htmlspecialchars(trim($DescriptionEn));
$dataAry['DescriptionChi'] = intranet_htmlspecialchars(trim($DescriptionChi));
if (get_magic_quotes_gpc()) {
	foreach ((array)$dataAry as $k=>$v) {
		$dataAry[$k] = mysql_real_escape_string($v);
	}
}

 
$result = $libms->ADD_RESPONSIBILITY($dataAry);

intranet_closedb();

header("Location: responsibility.php?xmsg=add");

?>
