<?php
/*
 * 	Log
 * 
 * 	2017-03-10 (Cameron)
 * 		- allow these symbol for code +-.()[]{}	[case#K114289]
 *  
 * 	2017-01-20 [Cameron]
 * 		- don't allow inputting these symbols for code field !"#$%&\'()*+,./:;<=>?@[]^`{|}~
 * 		- fix bug: prevent submit if checkForm return false 
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();

//*/
$linterface = new interface_html();

$Code = (is_array($Code)) ? $Code[0] : $Code;

$RespondInfo = $libms->GET_RESOURCES_TYPE_INFO($Code);

$ResourcesTypeCode = $RespondInfo[0]['ResourcesTypeCode'];
$DescriptionEn = $RespondInfo[0]['DescriptionEn'];
$DescriptionChi = $RespondInfo[0]['DescriptionChi'];

# Top menu highlight setting
$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageSettingsResourcesType";

$TAGS_OBJ[] = array($Lang['libms']['settings']['resources_type']);

$PAGE_NAVIGATION[] = array($Lang['libms']['settings']['resources_type'], "resources_type.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");

# Left menu 
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--
function checkForm(form1) {
	
	if(form1.ResourcesTypeCode.value=='')	 {
		alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['resources_type']['code'] ?>");	
		form1.ResourcesTypeCode.focus();
		return false;
	} else if(form1.DescriptionEn.value=="") {
		alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['resources_type']['description_en'] ?>");	
		form1.DescriptionEn.focus(); 
		return false;
	} else if(form1.DescriptionChi.value=="") {
		alert("<?= $i_alert_pleasefillin.' '.$Lang['libms']['resources_type']['description_b5'] ?>");	
		form1.DescriptionChi.focus(); 
		return false;
	} else if(form1.ResourcesTypeCode.value.match(/[!"#$%&\\'*,\/:;<=>?@^`|~]/g)) {
		alert("<?= str_replace(array('\\','"'),array('\\\\','\"'),$Lang["libms"]["settings"]["msg"]["exclude_symbols"]) ?>");	
		form1.ResourcesTypeCode.focus(); 
		return false;
	} else {
		return true;
	}
}



function submitCallback(json){
	if (json.success && json.unique){
		document.form1.submit();
	}else{
		$('#in_ResourcesTypeCode').focus().css('background-color','yellow');
		alert('<?=$Lang['libms']['settings']['msg']['duplicated']?>');
	}
}

$(function(){
	$('#form1').submit(function(e){
		e.preventDefault();
		if (checkForm(document.form1)) {
			if ( $('#in_Code').val() !=  $('#in_ResourcesTypeCode').val() ){
				$.getJSON('ajax_unique_check.php',{
					type :  'resType',
					needle : $('#in_ResourcesTypeCode').val()
				},submitCallback);
//				e.preventDefault();
//				return false;
			}
			else {
				document.form1.submit();	
			}
		}
	});
});


//-->
</script>


<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form id='form1' name="form1" method="post" action="resources_type_edit_update.php">

<table width="90%" align="center" border="0">
<tr><td>
	<input id='in_Code' name="Code" type="hidden" class="textboxtext" value="<?=$Code?>" />
	<table class="form_table_v30">
	<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['resources_type']['code']?></td>
			<td><input id='in_ResourcesTypeCode' name="ResourcesTypeCode" type="text" class="textboxtext" value="<?=$ResourcesTypeCode?>" /></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['resources_type']['description_en']?></td>
			<td><input name="DescriptionEn" type="text" class="textboxtext" value="<?=$DescriptionEn?>" /></td>
		</tr>
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['libms']['resources_type']['description_b5']?></td>
			<td><input name="DescriptionChi" type="text" class="textboxtext" value="<?=$DescriptionChi?>" /></td>
		</tr>
	</table>
	
	<?=$linterface->MandatoryField();?>
	
	<div class="edit_bottom_v30">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='resources_type.php'")?>
	</div>	

</td></tr>
</table>

</form><?php
echo $linterface->FOCUS_ON_LOAD("form1.ResourcesTypeCode"); 



$linterface->LAYOUT_STOP();

 
 
 
 

intranet_closedb();

?>
