<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

### set cookies
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	//$order = $ck_right_page_order;
	$order = 1;		# default in ascending order
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}


$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
*/
$libms = new liblms();


$CurrentPageArr['LIBMS'] = 1;
$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_ist'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
$CurrentPage = "PageSettingsSpecialOpenTime";
 

$linterface = new interface_html("libms.html");
  
//$linterface = new interface_html();

############################################################################################################
///*
 
 
global $eclass_prefix;
 
$toolbar = '';//$linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'label_format_new.php')",$button_new,"","","",0);

//*/
############################################################################################################

# Top menu highlight setting

$TAGS_OBJ[] = array($Lang["libms"]["import"]['SpeicalOpenTime']['title']);
 

 
# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


$sample_file = "special_open_time_import.csv";
$csvFile = "<a class=\"tablelink\" href=\"get_sample_file.php?file=$sample_file\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a>";

$csv_format = "";
$delim = "<br/>";
$redstar_html = '<font color = "red">*</font>';
$redstar_insert='';
$i=0;
foreach( $Lang["libms"]["import"]['SpeicalOpenTime']['fields'] as $key => $field){
	$fieldname = $field['name'];
	$i++;
	if($field['req']){
		$redstar_insert = $redstar_html;
	}else{
		$redstar_insert = '';
	}
		
	$csv_format .= "{$Lang['SysMgr']['Homework']['Column']} {$i} : {$redstar_insert} {$fieldname} {$delim}";
}


$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN(); 

$linterface->LAYOUT_START();


?>
<br />
<form name="form1" method="post" action="upload_csv.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" align="left" width="30%"><?=$iDiscipline['Import_Source_File']?>: 
						<span class="tabletextremark">
						<?=$Lang['General']['CSVFileFormat']?></span>
					</td>
					<td class="tabletext" width="70%">
						<input class="file" type="file" name="file">
					</td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?></td>
					<td class="tabletext"><?=$csvFile?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
					<td class="tabletext"><?=$csv_format?></td>
				</tr>
 
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['RequiredField']?></td>
				</tr>
				
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DateFormat']?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='../special_open_time.php'") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="action" value="simple" />
</form>
<br />

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();


