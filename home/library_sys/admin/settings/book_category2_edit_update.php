<?php
/*
 * 	Log
 * 
 * 	Date: 2017-04-06 [Cameron]
 * 			- don't apply mysql_real_escape_string() to description fields when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 
 * 	Date: 2017-01-18 [Cameron]
 * 		- call replaceSymbolByHyphen() for code field 
 * 
 * 	Date: 2015-09-23 [Cameron]
 *  	- apply mysql_real_escape_string() to DescriptionEn and DescriptionChi to support back slash
 *
 * 	Date: 2015-07-31 [Cameron]
 * 		- Trim data before save
 * 
 * 	Date: 2015-05-20 [Cameron]
 * 		- add include file libelibrary.php for used in $libms->UPDATE_BOOK_CATEGORY
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}


/*
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}
//*/
if($BookCategoryCode=="") {
	header("Location: book_category2.php");	
}

$Code = intranet_htmlspecialchars($Code);
$BookCategoryCode = replaceSymbolByHyphen($BookCategoryCode);

$dataAry['BookCategoryCode'] = intranet_htmlspecialchars(trim($BookCategoryCode));
$dataAry['DescriptionEn'] = intranet_htmlspecialchars(trim($DescriptionEn));
$dataAry['DescriptionChi'] = intranet_htmlspecialchars(trim($DescriptionChi));
if (get_magic_quotes_gpc()) {
	foreach ((array)$dataAry as $k=>$v) {
		$dataAry[$k] = mysql_real_escape_string($v);
	}
}
$dataAry['BookCategoryType'] = 2;

$result = $libms->UPDATE_BOOK_CATEGORY($Code, $dataAry);

intranet_closedb();

header("Location: book_category2.php?xmsg=update");

?>
