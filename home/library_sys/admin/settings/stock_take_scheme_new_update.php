<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['settings'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$libms = new liblms();


if(sizeof($_POST)==0) {
	header("Location: stock_take.php");	
}

$stocktakeSchemeID = $_POST['StocktakeSchemeID'];
$schemeTitle= standardizeFormPostValue($_POST['schemeTitle']);
$description = standardizeFormPostValue($_POST['description']);
$startDate = $_POST['startDate'];
$endDate = $_POST['endDate'];
$recordsStartDate = $_POST['recordsStartDate'];
$recordsEndDate = $_POST['recordsEndDate'];

if($stocktakeSchemeID){
	$result = $libms->UPDATE_STOCKTAKE_SCHEME_RECORD($stocktakeSchemeID, $schemeTitle,$description, $startDate, $endDate, $recordsStartDate, $recordsEndDate);
}else{
	$result = $libms->ADD_STOCKTAKE_SCHEME_RECORD($schemeTitle,$description, $startDate, $endDate, $recordsStartDate, $recordsEndDate);
}
intranet_closedb();

header("Location: stock_take.php?xmsg=add");

?>
