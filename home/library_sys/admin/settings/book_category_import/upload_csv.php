<?php
/*
 * 	Using:
 *
 *	Date:	2017-01-20 [Cameron]
 *			- add checking for symbols in code field
 * 
 *  Date:	2015-04-01 [Cameron]
 * 			- remove unique constraint (BookCategoryType + DescriptionEn + DescriptionChi) 
 * 
 * 	Date:	2015-03-31 [Cameron]
 * 			- Add filter $filter['BookCategoryType'] when check duplicate record so that BookCategoryCode + BookCategoryType is unique  
 * 			- show all error at a time rather than one error at a line when checking
 * 			- check if file exist before unlink it, otherwise, error occurs when change language
 * 			- correct $TAGS_OBJ and its linking
 * 			- pre-treat data by using htmlentities() before insert into temp db 
 * 
 */
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

//global $intranet_db;

intranet_auth();
intranet_opendb();
$libms = new liblms();
$admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
*/




$libms = new liblms();

$limport = new libimporttext();

$CurrentPageArr['LIBMS'] = 1;
$PAGE_NAVIGATION[] = array($Lang['libms']['bookmanagement']['book_ist'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['libms']['general']['edit'], "");
$CurrentPage = "PageSettingsBookCategory";

$linterface = new interface_html("libms.html");

//$linterface = new interface_html();
############################################################################################################
///*


global $eclass_prefix;

$toolbar = '';//$linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'label_format_new.php')",$button_new,"","","",0);

//*/
############################################################################################################

# Top menu highlight setting

$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY_SETTING', '*', '','',1);
foreach ($result as $row){
	$book_category_name[] = htmlspecialchars($row['value']);
}
$TAGS_OBJ[] = array($book_category_name[0]?$book_category_name[0]:$Lang['libms']['settings']['book_category']." 1",'#', true);
$TAGS_OBJ[] = array($book_category_name[1]?$book_category_name[1]:$Lang['libms']['settings']['book_category']." 2",$PATH_WRT_ROOT.'home/library_sys/admin/settings/book_category2.php', false);
$TAGS_OBJ[] = array($Lang["libms"]["settings"]["book_category_type"],$PATH_WRT_ROOT.'home/library_sys/admin/settings/book_category_settings.php', false);




# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


function check_exist_bookCode($given_bookCode){
	global $libms;
	$result = $libms->returnArray("select BookCode from LIBMS_BOOK where BookCode=".CCToSQL($given_bookCode)." limit 1");
	//error_log(var_export($result, true));
	//return $result[0]['BookCode'] ;
	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}

function check_exist_barCode($given_barCode_array){
	global $libms;
	$result = $libms->returnArray("select BarCode from LIBMS_BOOK_UNIQUE where BarCode in ".$given_barCode_array );

	if (empty($result)){
		return FALSE;
	}else{
		return true;
	}
}




//===================================================================================
//$libms->db_db_query("delete from `LIBMS_CSV_TMP`");


## ADD PEAR in the INCLUDE path
set_include_path(get_include_path() . PATH_SEPARATOR . $intranet_root .'/home/library_sys/admin/book/import/pear/');

include('../api/class.upload.php');


if ((!empty($_REQUEST['action'])) &&  $_REQUEST['action'] == 'simple') {
	$handle = new Upload($_FILES['file']);
	$ext = strtoupper($handle->file_src_name_ext);

	if($ext != "CSV" && $ext != "TXT")
	{
		$uploadSuccess = false;
		$xxmsg = $Lang["libms"]["import"]['BookCat']['error_msg']['upload_fail']  . $handle->error . $Lang["libms"]["import"]['BookCat']['error_msg']['contact_admin'];
	}
	if ($handle->uploaded) {
		$handle->Process($intranet_root."/file/lms_book_import/");
		if ($handle->processed) {
			$csvfile = $intranet_root."/file/lms_book_import/" . $handle->file_dst_name;			
			$uploadSuccess = true;
		} else {

			$xxmsg = $Lang["libms"]["import"]['BookCat']['error_msg']['upload_fail']  . $handle->error . $Lang["libms"]["import"]['BookCat']['error_msg']['contact_admin'];
			$uploadSuccess = false;
		}
		// we delete the temporary files
		$handle-> Clean();
	}
}

if ($uploadSuccess){
	$file_format = array(
			'Book Category Code',
			'English Description',
			'Chinese Description'
	);

	$flagAry = array(
			'1',
			'1',
			'1'
	);
	$format_wrong = false;
	$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($csvfile,"","",$file_format,$flagAry);
	
	//dex($data);
	
	$counter = 1;
	$insert_array = array();
	$my_key = array();

	if(is_array($data))
	{
		$col_name = array_shift($data);
	}

	for($i=0; $i<sizeof($file_format); $i++)
	{
		if ($col_name[$i]!=$file_format[$i])
		{
			$format_wrong = true;
			break;
		}
	}

	if($format_wrong)
	{
		header("location: import_csv_data.php?xmsg=wrong_header");
		exit();
	}
	if(sizeof($data)==0)
	{
		header("location: import_csv_data.php?xmsg=import_no_record");
		exit();
	}

	$k = 2;
	
	
	$libms->CLEAR_RECORD('LIBMS_BOOK_CATEGORY_TMP');
	
	foreach ($data as $record) {
		$k++;
		
		$error_msg = array();
		$empty_col = array();		
		// 1. check mandatory fields
		foreach ($record as $col => $value){
			if ($value == ''){
				$empty_col[] = $col + 1;				
//				$error_records[] = array( 'line' => $k , 'msg' =>  $Lang['libms']['import']['msg']['error_required_missing'].($col+1));
//				continue 2;
			}
		}
		if (count($empty_col) > 0) {
			$error_msg[] = $Lang['libms']['import']['msg']['error_required_missing'].implode(", ",$empty_col);
		}

		// 2. check if there's symbols in code
		if (isIncludingSymbol($record['0'])) {
			$error_records[] = array( 'line' => $k , 'msg' =>  $Lang["libms"]["settings"]["msg"]["exclude_symbols"]);
			continue;
		}

		// 3. check duplicate code
		$filter = array();		
		$filter['BookCategoryCode'] = PHPToSQL($record['0']);
		$filter['BookCategoryType'] = PHPToSQL('1');
		$result = $libms->SELECTFROMTABLE('LIBMS_BOOK_CATEGORY','BookCategoryCode',$filter);
		if (!empty($result)){
			$error_msg[] = $Lang['libms']['import']['msg']['dup_key'] . '1';	// last one is column number
		}
			
		if (count($error_msg) > 0) {
			$error_records[] = array( 'line' => $k , 'msg' => implode(", ",$error_msg) );
			continue;
		}
		
		$insert_to_db = array(
				'BookCategoryCode'=> PHPToSQL(htmlentities($record['0'],ENT_QUOTES,"utf-8")),
				'DescriptionEn'=> PHPToSQL(htmlentities($record['1'],ENT_QUOTES,"utf-8")),
				'DescriptionChi'=> PHPToSQL(htmlentities($record['2'],ENT_QUOTES,"utf-8")),
		);
			
		$result = $libms->INSERT2TABLE('LIBMS_BOOK_CATEGORY_TMP', $insert_to_db);
			
		// dex($insert_to_db);
		$error_msg =mysql_error();
		if (!empty($error_msg)){
			//capture error
			$error_records[] = array( 'line' => $k , 'msg' => $error_msg);
		}

		unset ($insert_to_db);
	}
	//print "Processe:" . $counter;
}

//dex($data);

if(!empty($error_records)) {
	$xx .= "<div style=\"max-width:500px;\">";
	foreach ($error_records as $error_line){		
//		$xx .= "<font color='red'> line : {$error_line['line']} </font> {$error_line['msg']} <br/ >";
		$xx .= "<div style=\"text-align:left;\"><font color='red'> line : {$error_line['line']} </font> {$error_line['msg']}</div>";
	}
	$xx .= "</div>";
}
 

### List out the import result
$xx .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$xx .= "<tr>";
//$xx .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";


foreach( $Lang["libms"]["import"]['BookCat']['fields'] as $field){
	$fieldname = $field['name'];
	$xx .= "<td class=\"tablebluetop tabletopnolink\"> {$fieldname} </td>";
}

$xx .= "<td class=\"tablebluetop tabletopnolink\">&nbsp;</td>";
$xx .= "</tr>";

//row
//dex($xx);
$sql = "SELECT  * FROM `LIBMS_BOOK_CATEGORY_TMP` ";
$result = $libms->returnArray($sql);


$y = 3;
if(!empty($result))
foreach($result as $record){
	$error = array();
	//check error here

		$xx .= "<tr class=\"tablebluerow".($y%2+1)."\">";
		//$xx .= "<td class=\"$css\">".($y)."</td>";
		$xx .= "<td class=\"$css\">".$record['BookCategoryCode']."</td>";
		$xx .= "<td class=\"$css\">".$record['DescriptionEn']."</td>";
		$xx .= "<td class=\"$css\">".$record['DescriptionChi']."</td>";

		$xx .= "<td class=\"$css\">";

		$xx.="</td>";
		$xx .= "</tr>";
	$y++;
}

$xx .= "</table>";
//dump ($xx);
if(!empty($error_records))
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_csv_data.php'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_csv_data.php'");
	 
	$prescan_result =  $Lang['libms']['import_book']['upload_success_ready_to_import'].count($result);
	$xx .= $prescan_result;
}


$libms->MODULE_AUTHENTICATION($CurrentPage);
$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();

$linterface->LAYOUT_START();
?>
<br />
<form name="form1" action="confirm_import_to_db.php" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5">

		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xxmsg, $xxmsg2);?>
			</td>
		</tr>
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td align="center">
				<table width="96%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" class="tabletext">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><?=$xx?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="95%" border="0" cellpadding="0" cellspacing="0"
					align="center">
					<tr>
						<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2"><?= $import_button ?>
			</td>
		</tr>
	</table>
</form>
<br /><?php
if (file_exists($csvfile)) {
 	unlink($csvfile);
}


$linterface->LAYOUT_STOP();
//dump($li->built_sql());
//dump(mysql_error());
//dump($error_records);
intranet_closedb();



