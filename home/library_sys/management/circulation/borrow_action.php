<?php

// Modifying by:

/*
 * ########### Change Log Start ###############
 *
 * 20180222 Cameron
 * - fix: add meta characterset (utf-8) to show no access right message
 *
 * 20170801 Cameron
 * - fix bug: should initialize $result, otherwize it causes "wrong datatype for second argument in ..." error when used in PowerClass
 *
 * 20170314 Cameron
 * - add IP checking for circulation
 *
 * 20150814 Cameron: add AccompanyMaterial when borrow books
 *
 * 20150609 Henry: apply class management permission
 *
 * ########### Change Log End ###############
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/pdump/pdump.php");
include_once ('User.php');
include_once ('BookManager.php');
include_once ('RightRuleManager.php');
include_once ('TimeManager.php');
include_once ('User.php');

intranet_auth();
intranet_opendb();

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);
$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['borrow'] || $global_lockout))) 
{
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// include_once('TimeManager.php');
$TM = new TimeManager();
if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! $TM->isOpenNow(time()) && ! $c_right['circulation management']['access_without_open_hour']) {
    header("Location: index.php");
}

$bookUniqieIDs = $_REQUEST['bookUniqieID'];

// $interface = new interface_html("libms_circulation.html");
$GLOBALS['LIBMS']['PAGE'] = "BORROW";

$uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];

$User = new User($uID, $libms);

$result = array();
if (! empty($bookUniqieIDs)) {
    foreach ($bookUniqieIDs as $bookUniqieID) {
        if (! empty($bookUniqieID)) {
            $AccompanyMaterial = ($_REQUEST['AccomMat_' . $bookUniqieID] == 'checked') ? 1 : 0;
            $result[$bookUniqieID] = $User->borrow_book($bookUniqieID, true, $renewalTime = 0, $AccompanyMaterial);
        }
    }
}

if (! empty($result))
    $error = in_array(false, $result);
else
    $error = false;

if ($error)
    $_SESSION['LIBMS']['xmsg'] = $Lang['libms']['Circulation']['action_fail'];
else
    $_SESSION['LIBMS']['xmsg'] = $Lang['libms']['Circulation']['action_success'];

header("Location: borrow.php?show_user_detail=1");

?>

<script type="text/javascript">
<!--
window.location = "borrow.php?show_user_detail=1"
//-->
</script>
