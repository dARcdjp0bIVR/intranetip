<?php
// Editing by  
/**
 * Log :
 *
 *
 * Date 2020-05-04 [Cameron]
 *      - add parameter $jsonError to returnBook() 
 *      
 * Date 2019-10-11 [Cameron]
 *      - adjust thickbox width if $sys_custom['eLibraryPlus']['Circulation']['LargeFont'] is set in gen_return_book_list()
 *      
 * Date 2019-06-14 [Cameron]
 *      - fix: don't show colon if BookSubTitle is empty
 * 
 * Date 2019-03-28 [Cameron]
 *      - retrieve BookSubTitle in getOverDueRecords() and getBorrowHistoryByUserID() [case #K154500]
 *      
 * Date 2019-03-26 [Cameron]
 *      - fix: retrieve unique BorrowLogID from LIBMS_OVERDUE_LOG in getBorrowHistoryByUserID()
 *      
 * Date 2019-03-05 [Cameron]
 *      - show BookSubTitle in gen_return_book_list() if $sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] is true [case #K154500]
 *       
 * Date 2019-03-04 [Cameron]
 *      - add filter condition LIBMS_OVERDUE_LOG.RecordStatus<>'DELETE' to getBorrowHistoryByUserID(), getOverDueRecords() [case #X157459]
 *       
 * Date 2018-12-13 [Cameron]
 *  modify returnBook() to fix bug: 
 *      - don't update user balance and delete overdue log if the LOST book overdue amount is waived (filter out WAIVED record) [case #L154330]
 *      - add 'DELETE' RecordStatus to LIBMS_OVERDUE_LOG, for mark delete when return lost book, should exclude these record in penalty report and penalty stats
 *      - add record to LIBMS_BALANCE_LOG for such case   
 *  
 * Date 2018-11-14 [Cameron]
 * - add function getNumberOfLoansByBook() [case #Y150892]
 * 
 * Date 2018-05-07 [Cameron]
 * - fix showing error message in returnBook() for return 'LOST' book [Case #C138500]
 *
 * Date 2018-05-04 [Henry]
 * - modified returnBook() for bug fixing [Case#C138500]
 *
 * Date 2018-02-02 [Henry]
 * - modified getUserIDByBookUniqueID(), getBookIDByBookUniqueID(), returnBook() for returning lost book logic [Case#M130018]
 *
 * Date 2018-01-31 [Cameron]
 * - show ItemSeriesNum after Book Title in getOverDueRecords() and getBorrowHistoryByUserID() [case #L130405]
 *
 * Date 2017-09-07 [Cameron]
 * - allow RecordStatus='WRITEOFF' in getBookIDByBookUniqueID() [Case #L123983]
 * - add REMOVE_RESERVATION procedure in returnBook()
 *
 * Date 2017-02-25 [Cameron]
 * fix bug in returnBook(): if "Return book confirmation" is set to "Scan barcode only" and "Overdue (fine per day)"
 * is set to zero in Circulation Policy, it cannot show alert color text in overdue column when return book. [case #R113474]
 *
 * Date 2016-07-29 [Cameron]
 * fix bug: use explode to replace preg_split because it's faster
 * (first parameter should use pattern match format '/#/' instead of '#' if use preg_split)
 *
 * Date 2016-05-18 [Cameron]
 * fix bug: set $return_row_class to 'returned' in returnBook so that it can process to change book item status
 * if there's no borrow record but book item status is 'BORROWED' (on loan) - case for new data migration
 *
 * Date 2016-04-21 [Cameron]
 * add parameter $filterBookUniqueID to getReservationByBookID() to fix following case: [case #P94465]
 * a book has multiple copies (e.g. 3), user A borrow one, B, C, D reserve the book => B, C is ready, D is waiting
 * => A return Book, B and C keep ready if they haven't picked up the book, it should assign D as ready instead of B
 *
 * Date 2015-12-07 [Henry]
 * modified returnBook() to add var $isSameDayReturn for return
 *
 * Date 2015-11-13 [Henry]
 * modified returnBook() to add para $HandleOverdue
 *
 * Date 2015-11-11 [Cameron]
 * retrieve AccompanyMaterial from LIBMS_BORROW_LOG in getBorrowHistoryByUserID()
 *
 * Date 2015-11-11 [Henry]
 * modified returnBook() to add para $returnDateTime
 *
 * Date 2015-11-05 [Cameron]
 * - add space between AccompanyMaterial and attachmentNote in gen_return_book_list()
 * - fix bug on showing case sensitive BarCode ($bookInfo['UniqueBarCode']) in gen_return_book_list()
 *
 * Date 2015-10-14 [Cameron]
 * add parameter checkOnly to gen_return_book_list() so that it can show book reservation person in checking stage
 *
 * Date 2015-09-02 [Cameron]
 * show overdue info in return book checking stage
 *
 * Date 2015-08-24 [Cameron]
 * add function gen_return_book_list(), output()
 *
 * Date 2015-08-18 [Cameron]
 * add function getUserIDByBookUniqueID(), getAccompanyMaterialByBookUniqueID(), getBookIDByBookUniqueID(), returnBook()
 *
 * Date 2015-07-07 [Henry]
 * modified getOverDuePaymentInfo() add column ol.`RecordStatus`
 * Date 2015-06-10 [Henry]
 * modified getOverDueRecords() add column bl.UniqueID
 *
 * Date 2014-12-09 [Ryan]
 * modified getBorrowHistoryByUserID(), to show payment Amount depends on RecordStatus
 * If is Waived show PaymentRecevied, else show Payment
 *
 * Date 2013-10-31 [Carlos]
 * modified addOverduePaymentRecord(), add optional parameter $details
 * added addCancelOverduePaymentRecord(), to void an overdue payment (single purchase item)
 * Date 2013-10-08 [Henry]
 * modified getReservationByBookID(), remove "LIMIT 1" in the sql
 *
 * Date 2013-10-07 [Henry]
 * modified getReservationCountByBookID() for debugging
 *
 * Date 2013-09-16 [Yuen]
 * Add function changeBookStatus()
 * Date 2013-09-02 [Carlos]
 * Add function addOverduePaymentRecord() for adding ePayment overdue payment record
 * Date 2013-05-08 [Yuen]
 * Add function getBorrowerByUniqueID()
 * Date 2013-05-07 [Cameron]
 * Add function getBorrowHistoryByUserID($UserID)
 * Date 2013-05-06 [Cameron]
 * Add function getBorrowHistoryByBarCode($BarCode)
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$WRT_ROOT = "{$_SERVER['DOCUMENT_ROOT']}/";
// include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once ($WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");
include_once ($WRT_ROOT . "includes/libdb.php");
include_once ($WRT_ROOT . "includes/pdump/pdump.php");

include_once ($WRT_ROOT . 'home/library_sys/management/circulation/BookManager.php');
include_once ($WRT_ROOT . 'home/library_sys/management/circulation/User.php');

global $Lang;

class RecordManager
{

    var $libms;

    var $db_lock;

    // CONSTRUCTOR
    public function __construct($libms)
    {
        if (! empty($libms))
            $this->libms = $libms;
        else
            $this->libms = new liblms();
        $this->db_lock = 0;
    }

    public function addToUserBalance($UserID, $amount)
    {
        $this->Start_Trans();
        
        if ($this->libms->UPDATE2TABLE("LIBMS_USER", Array(
            'Balance' => "`Balance` + {$amount} "
        ), Array(
            'UserID' => PHPToSQL($UserID, 'INTEGER')
        ))) {
            $this->Commit_Trans();
            return true;
        } else {
            $this->RollBack_Trans();
            return false;
        }
    }

    // get leastest BorrowLogID from BookUniqueID
    public function getBorrowLogIDFromBookUniqueID($bookUniqueID, $RecordStatus = null)
    {
        if (empty($bookUniqueID)) {
            return false;
        }
        if (! empty($RecordStatus)) {
            $recordStatus = " AND `RecordStatus`= '{$RecordStatus}' ";
        }
        
        $sql = "SELECT `BorrowLogID` FROM `LIBMS_BORROW_LOG` WHERE `UniqueID`='{$bookUniqueID}' {$recordStatus} ORDER BY `BorrowLogID` DESC LIMIT 1";
        $result = $this->libms->returnArray($sql);
        
        if (! empty($result)) {
            return $result[0][0];
        } else {
            return false;
        }
    }

    public function getFieldsFromBorrowLog($borrowLogID)
    {
        if (empty($borrowLogID)) {
            return false;
        }
        $sql = "SELECT * FROM `LIBMS_BORROW_LOG` WHERE `BorrowLogID`='{$borrowLogID}'  LIMIT 1";
        $result = $this->libms->returnArray($sql);
        
        if (! empty($result)) {
            return $result[0];
        } else {
            return false;
        }
    }

    public function getBorrowHistoryByBarCode($BarCode)
    {
        if (empty($BarCode)) {
            return false;
        }
        $name_field = getNameFieldWithClassNumberByLangLIBMGMT("u.");
        
        $sql = <<<EOF
				SELECT  bl.BorrowLogID,
						b.BookTitle,
						ifnull(bu.BarCode, bu.BarCode_BAK) AS BarCode,
						{$name_field} as UserName,						
						bl.Borrowtime,
						bl.DueDate,						
						bl.ReturnedTime		
				  FROM `LIBMS_BORROW_LOG` bl,
				       `LIBMS_BOOK` b,
				       `LIBMS_BOOK_UNIQUE` bu,
				       `LIBMS_USER` u
				WHERE bl.`BookID` = b.`BookID`
				  AND bl.`UserID` = u.`UserID`
				  AND bl.`UniqueID` = bu.`UniqueID`
				  AND b.BookID=bu.BookID
				  AND bu.BarCode = '{$BarCode}'				  
				  ORDER BY bl.BorrowLogID DESC
EOF;
        
        // debug($sql);
        
        $result = $this->libms->returnArray($sql);
        
        if (! empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    public function getBorrowHistoryByUserID($UserID)
    {
        if (empty($UserID)) {
            return false;
        }
        
        $sql = <<<EOF
				SELECT  bl.BorrowLogID,
						ifnull(b.BookCode, b.BookCode_BAK) AS BookCode,
						ifnull(bu.ACNO, bu.ACNO_BAK) AS ACNO,
						b.CallNum,
						CONCAT(b.BookTitle, IF(bu.ItemSeriesNum<>'' AND bu.ItemSeriesNum<>'1' AND bu.ItemSeriesNum IS NOT NULL,CONCAT(' ',bu.ItemSeriesNum),'')) as BookTitle,
						b.ResponsibilityBy1,
						b.Publisher,
						ifnull(bu.BarCode, bu.BarCode_BAK) AS BarCode,
						bl.Borrowtime,
						bl.DueDate,						
						bl.ReturnedTime,						
						CASE
					        WHEN ol.RecordStatus = 'WAIVED' Then ol.PaymentReceived
					        ELSE ol.Payment
						END AS Payment, 		
						ol.DaysCount,						
						bl.RecordStatus,
						bl.AccompanyMaterial as BorrowAccompanyMaterial,
                        b.BookSubTitle
				  FROM `LIBMS_BORROW_LOG` bl 
                        LEFT JOIN ( 
                            SELECT 
                                    v1.`Payment`, 
                                    v1.`PaymentReceived`,
                                    v1.`RecordStatus`,
                                    v1.DaysCount,
                                    v1.`BorrowLogID`, 
                                    v1.`OverDueLogID`
                            FROM
                                    `LIBMS_OVERDUE_LOG` v1
                            INNER JOIN (
                                    SELECT 
                                        `BorrowLogID`, 
                                        MAX(`OverDueLogID`) AS OverDueLogID
                                    FROM
                                        `LIBMS_OVERDUE_LOG`
                                    WHERE
                                        `RecordStatus`<>'DELETE'
                                    GROUP BY `BorrowLogID`
                                ) AS v2 ON v2.`OverDueLogID`=v1.`OverDueLogID`
                          ) AS ol ON ol.BorrowLogID=bl.BorrowLogID,
				       `LIBMS_BOOK` b,				       
				       `LIBMS_BOOK_UNIQUE` bu		       
				       
				WHERE bl.`BookID` = b.`BookID`
				  AND bl.`UserID` = '{$UserID}'
				  AND bl.`UniqueID` = bu.`UniqueID`
				  AND b.BookID=bu.BookID				   				  
				  ORDER BY bl.BorrowLogID DESC
EOF;
        
        // debug($sql);
        
        $result = $this->libms->returnArray($sql);
        
        if (! empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    public function newBorrowLog($dataAry)
    {
        $this->Start_Trans();
        if ($this->libms->INSERT2TABLE("LIBMS_BORROW_LOG", $dataAry)) {
            $this->Commit_Trans();
            return true;
        } else {
            $this->RollBack_Trans();
            return true;
        }
    }

    public function updateBorrowLog($borrowLogID, $dataAry)
    {
        $this->Start_Trans();
        if ($this->libms->UPDATE2TABLE("LIBMS_BORROW_LOG", $dataAry, Array(
            'BorrowLogID' => PHPToSQL($borrowLogID)
        ))) {
            $this->Commit_Trans();
            return true;
        } else {
            $this->RollBack_Trans();
            return false;
        }
    }

    public function getReservationCountByBookID($bookID, $ary_RecordStatus = null)
    {
        if (empty($bookID)) {
            return false;
        }
        
        // if (!empty($RecordStatus)){
        if (! is_array($ary_RecordStatus))
            $ary_RecordStatus = array(
                $ary_RecordStatus
            );
        // $RecordStatus = array($ary_RecordStatus);
        $whereclause = " AND `RecordStatus` in ('" . implode("','", $ary_RecordStatus) . "') ";
        // }else{
        // $whereclause='';
        // }
        $sql = "SELECT count(*) FROM `LIBMS_RESERVATION_LOG` WHERE `BookID`='{$bookID}' {$whereclause}";
        $result = $this->libms->returnArray($sql);
        // tolog ($result);
        // $err= mysql_error();
        // tolog ($err);
        if (! empty($result)) {
            return $result[0][0];
        } else {
            return 0;
        }
    }

    public function getBorrowByUser($user_id)
    {
        $name_field = getNameFieldWithClassNumberByLangLIBMGMT("");
        
        $sql = "SELECT $name_field FROM `LIBMS_USER` WHERE `UserID`='{$user_id}'";
        $result = $this->libms->returnVector($sql);
        
        return $result[0];
    }

    public function getBorrowerByUniqueID($UniqueID)
    {
        $name_field = getNameFieldWithClassNumberByLangLIBMGMT("lu.");
        
        $sql = "SELECT $name_field FROM `LIBMS_USER` AS lu, LIBMS_BORROW_LOG AS lbl WHERE lbl.UniqueID='{$UniqueID}' AND lu.UserID=lbl.UserID AND lbl.RecordStatus='BORROWED' ORDER BY lbl.BorrowLogID DESC";
        $result = $this->libms->returnVector($sql);
        
        return $result[0];
    }

    public function newReservation($dataAry)
    {
        $this->Start_Trans();
        if ($this->libms->INSERT2TABLE("LIBMS_RESERVATION_LOG", $dataAry)) {
            $this->Commit_Trans();
            return true;
        } else {
            $this->RollBack_Trans();
            return false;
        }
    }

    public function updateReservation($reservationID, $dataAry)
    {
        $this->Start_Trans();
        if ($this->libms->UPDATE2TABLE("LIBMS_RESERVATION_LOG", $dataAry, Array(
            'ReservationID' => PHPToSQL($reservationID)
        ), true)) {
            $this->Commit_Trans();
            return true;
        } else {
            $this->RollBack_Trans();
            return false;
        }
    }

    public function getReservationByBookID($bookID, $RecordStatus = null, $order = 'asc', $OwnerID = "", $ReturnAll = false, $UniqueID = '', $CirculationTypeCode = '', $filterBookUniqueID = false)
    {
        global $libms;
        
        if (empty($bookID)) {
            return false;
        }
        
        if (! empty($RecordStatus)) {
            if (! is_array($RecordStatus))
                $RecordStatus = array(
                    $RecordStatus
                );
            $whereclause = " AND `RecordStatus` in ('" . implode("','", $RecordStatus) . "') ";
        } else {
            $whereclause = '';
        }
        
        if ($filterBookUniqueID && ($UniqueID != '')) {
            $whereclause .= " AND `BookUniqueID`= '" . $UniqueID . "' ";
        }
        
        if ($OwnerID != "" && $OwnerID > 0) {
            $whereclause .= " AND UserID<>'$OwnerID' ";
        }
        
        $sql = <<<EOF
		SELECT * FROM `LIBMS_RESERVATION_LOG`
		WHERE `BookID`='{$bookID}'
		{$whereclause}
		ORDER BY `ReserveTime` {$order}
EOF;
        $result = $this->libms->returnArray($sql);
        
        if (! empty($result)) {
            if ($ReturnAll) {
                return $result;
            } else {
                if ($CirculationTypeCode == '' || $UniqueID == '') {
                    return $result[0];
                } else {
                    // need to check who is the first and with the right for the item according to circulation type!
                    for ($i = 0; $i < sizeof($result); $i ++) {
                        $ResultRow = $result[$i];
                        $UserObj = new User($ResultRow["UserID"], $libms);
                        $canReserve = $UserObj->rightRuleManager->canReserveItem($CirculationTypeCode);
                        
                        if ($canReserve) {
                            return $ResultRow;
                        }
                    }
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    public function updateBook($bookID, $dataAry)
    {
        $this->Start_Trans();
        if ($this->libms->UPDATE2TABLE("LIBMS_BOOK", $dataAry, Array(
            'BookID' => PHPToSQL($bookID)
        ))) {
            $this->Commit_Trans();
            return true;
        } else {
            $this->RollBack_Trans();
            return false;
        }
    }

    public function updateUnquieBook($bookUniqueID, $dataAry)
    {
        $this->Start_Trans();
        if ($this->libms->UPDATE2TABLE("LIBMS_BOOK_UNIQUE", $dataAry, Array(
            'UniqueID' => PHPToSQL($bookUniqueID)
        ))) {
            $this->Commit_Trans();
            return true;
        } else {
            $this->RollBack_Trans();
            return false;
        }
    }

    public function newOverDueLog($dataAry)
    {
        $this->Start_Trans();
        
        if ($this->libms->INSERT2TABLE("LIBMS_OVERDUE_LOG", $dataAry)) {
            $this->Commit_Trans();
            return true;
        } else {
            $this->RollBack_Trans();
            return false;
        }
    }

    public function updateOverDueLog($overDueLogID, $dataAry)
    {
        $this->Start_Trans();
        if ($this->libms->UPDATE2TABLE("LIBMS_OVERDUE_LOG", $dataAry, Array(
            'OverDueLogID' => PHPToSQL($overDueLogID)
        ))) {
            $this->Commit_Trans();
            return true;
        } else {
            $this->RollBack_Trans();
            return false;
        }
    }

    public function newTransaction($dataAry)
    {
        $this->Start_Trans();
        if ($this->libms->INSERT2TABLE("LIBMS_TRANSACTION", $dataAry)) {
            $this->Commit_Trans();
            return true;
        } else {
            $this->RollBack_Trans();
            return false;
        }
    }

    public function getOverDueRecordFromborrowLogID($borrowLogID)
    {
        if (empty($borrowLogID)) {
            return false;
        }
        $sql = "SELECT * FROM `LIBMS_OVERDUE_LOG`
		WHERE `BorrowLogID`='{$borrowLogID}' AND `RecordStatus` = 'OUTSTANDING'
		LIMIT 1";
        $result = $this->libms->returnArray($sql);
        
        if (! empty($result)) {
            return $result[0];
        } else {
            return false;
        }
    }

    public function getOverDuePaymentInfo($overdueLogID)
    {
        if (empty($overdueLogID)) {
            return false;
        }
        $sql = "SELECT ol.`Payment`, ol.`PaymentReceived` , bl.`UserID`, ol.`RecordStatus`
		FROM `LIBMS_OVERDUE_LOG`ol
		JOIN `LIBMS_BORROW_LOG` bl
		ON bl.`BorrowLogID` = ol.`BorrowLogID`
		WHERE ol.`OverdueLogID`='{$overdueLogID}' LIMIT 1";
        $result = $this->libms->returnArray($sql);
        if (! empty($result)) {
            return $result[0];
        } else {
            return false;
        }
    }

    /**
     * SELECT ol.*, b.`BookTitle`,bl.`ReturnedTime`,bl.`DueDate`
     * FROM `LIBMS_OVERDUE_LOG` ol
     * JOIN `LIBMS_BORROW_LOG` bl
     * ON bl.`BorrowLogID` = ol.`BorrowLogID`
     * JOIN `LIBMS_BOOK` b
     * ON b.`BookID` = bl.`BookID`
     *
     *
     *
     * @param string $uID            
     * @param array $cond            
     * @return unknown|boolean
     */
    public function getOverDueRecords($uID = null, $cond = false)
    {
        if (! empty($cond)) {
            foreach ($cond as $field => $value) {
                if (is_int($field)) {
                    $whereclauses[] = $value;
                } else {
                    $whereclauses[] = "ol.`{$field}` = {$value}";
                }
            }
        }
        if (! empty($uID))
            $whereclauses[] = "bl.`UserID` = '{$uID}'";
        
        if (! empty($whereclauses)) {
            $whereclause = "WHERE (" . implode(') AND (', $whereclauses) . ")";
        }
        // Henry Modified 20130916
        $sql = "SELECT ol.*, 
		CONCAT(b.`BookTitle`, IF(bu.`ItemSeriesNum`<>'' AND bu.`ItemSeriesNum`<>'1' AND bu.`ItemSeriesNum` IS NOT NULL,CONCAT(' ',bu.`ItemSeriesNum`),'')) as BookTitle,
		bl.`ReturnedTime`,bl.`DueDate`, ifnull(bu.ACNO, bu.ACNO_BAK) AS ACNO, ifnull(bu.BarCode, bu.BarCode_BAK) AS BarCode, bl.UniqueID, b.`BookSubTitle`
		FROM `LIBMS_OVERDUE_LOG` ol
		JOIN `LIBMS_BORROW_LOG` bl
		ON bl.`BorrowLogID` = ol.`BorrowLogID` AND ol.RecordStatus<>'DELETE'
		JOIN `LIBMS_BOOK` b
		ON b.`BookID` = bl.`BookID`
		JOIN `LIBMS_BOOK_UNIQUE` bu
		ON bl.`UniqueID` = bu.`UniqueID`
		{$whereclause} ORDER BY bl.BorrowLogID ";
        
        $result = $this->libms->returnArray($sql);
        
        if (! empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    public function get_Book_Count($BookID = null, $RecordStatus = null)
    {
        $whereclause = "WHERE `BookID`={$BookID} ";
        
        if (! empty($RecordStatus)) {
            $imploded_RecordStatus = implode("','", $RecordStatus);
            $whereclause .= " AND `RecordStatus` IN ('{$imploded_RecordStatus}') ";
        }
        
        $sql = <<<EOF
		SELECT count(*)
		FROM  `LIBMS_BOOK_UNIQUE`
		{$whereclause}
EOF;
        
        $result = $this->libms->returnArray($sql);
        return empty($result) ? false : $result[0][0];
    }

    public function get_Availiable_Book_Count($BookID = null)
    {
        $status[] = 'NORMAL';
        $status[] = 'ACTIVE';
        $status[] = 'SHELVING';
        
        return get_Book_count($BookID, $status);
    }

    /*
     * add overdue payment to ePayment
     * @return LogID if success, false otherwise
     */
    public function addOverduePaymentRecord($userId, $amount, $details = '')
    {
        global $intranet_db, $Lang;
        
        $sql = "SELECT Balance FROM " . $intranet_db . ".PAYMENT_ACCOUNT WHERE StudentID='$userId'";
        $balance_record = $this->libms->returnVector($sql);
        
        if ($balance_record[0] == "") {
            return false;
        }
        
        $balance = $balance_record[0];
        
        if ($balance < $amount) {
            return false;
        }
        $resultAry = array();
        $sql = "UPDATE " . $intranet_db . ".PAYMENT_ACCOUNT SET Balance = Balance - $amount,LastUpdateByAdmin = '" . $_SESSION['UserID'] . "',LastUpdated = NOW() 
               	WHERE StudentID = '$userId'";
        $resultAry['UpdateBalance'] = $this->libms->db_db_query($sql);
        
        $balance_after = $balance - $amount;
        
        $details_value = $details != '' ? $this->libms->Get_Safe_Sql_Query($details) : "";
        
        $sql = "INSERT INTO " . $intranet_db . ".PAYMENT_OVERALL_TRANSACTION_LOG
               (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details, RefCode)
               VALUES
               ('$userId',3,'$amount',NULL,'$balance_after',NOW(),'" . $Lang['ePayment']['LibraryPayment'] . "','$details_value')";
        $resultAry['InsertOverallLog'] = $this->libms->db_db_query($sql);
        
        if ($resultAry['InsertOverallLog']) {
            $logID = $this->libms->db_insert_id();
            return $logID;
        }
        
        return false;
    }

    public function addCancelOverduePaymentRecord($userId, $amount, $details = '')
    {
        global $intranet_db, $Lang;
        
        $resultAry = array();
        
        $sql = "SELECT Balance FROM " . $intranet_db . ".PAYMENT_ACCOUNT WHERE StudentID='$userId'";
        $balance_record = $this->libms->returnVector($sql);
        $balance = $balance_record[0];
        
        $balance_after = $balance + $amount;
        
        $details_value = $details != '' ? $this->libms->Get_Safe_Sql_Query($details) : "";
        
        $sql = "INSERT INTO " . $intranet_db . ".PAYMENT_OVERALL_TRANSACTION_LOG
               (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details, RefCode)
               VALUES
               ('$userId',11,'$amount',NULL,'$balance_after',NOW(),'" . $Lang['ePayment']['LibraryPayment'] . "','$details_value')";
        $resultAry['InsertOverallLog'] = $this->libms->db_db_query($sql);
        
        $sql = "UPDATE " . $intranet_db . ".PAYMENT_ACCOUNT SET Balance = $balance_after,LastUpdateByAdmin = '" . $_SESSION['UserID'] . "',LastUpdated = NOW() WHERE StudentID = '$userId'";
        $resultAry['UpdateBalance'] = $this->libms->db_db_query($sql);
        
        if ($resultAry['InsertOverallLog']) {
            $logID = $this->libms->db_insert_id();
            return $logID;
        }
        
        return false;
    }

    public function changeBookStatus($BookUniqueID, $Status = "NORMAL")
    {
        if (empty($BookUniqueID)) {
            return false;
        }
        $sql = "UPDATE LIBMS_BOOK_UNIQUE SET RecordStatus='{$Status}' WHERE UniqueID='{$BookUniqueID}' ";
        $result = $this->libms->db_db_query($sql);
        
        if (! empty($result)) {
            return $result[0];
        } else {
            return false;
        }
    }

    Public function Start_Trans()
    {
        if (! $db_lock) {
            $this->libms->Start_Trans();
        }
        $db_lock ++;
    }

    Public function Commit_Trans()
    {
        if (! $db_lock) {
            $this->libms->Commit_Trans();
        }
        $db_lock --;
    }

    Public function RollBack_Trans()
    {
        if (! $db_lock) {
            $this->libms->RollBack_Trans();
        }
        $db_lock --;
    }

    /*
     * @param: $bookUniqueID -- BookUniqueID that's in 'BORROWED', 'LOST' status
     * @return: UserID
     */
    public function getUserIDByBookUniqueID($bookUniqueID)
    {
        $sql = "SELECT UserID FROM `LIBMS_BORROW_LOG` WHERE `UniqueID`='{$bookUniqueID}' AND RecordStatus IN ('BORROWED','LOST') ORDER BY `BorrowLogID` DESC LIMIT 1";
        $rows = $this->libms->returnVector($sql);
        $userID = $rows[0];
        return $userID > 0 ? $userID : 0;
    }

    /*
     * @param: $bookUniqueID -- BookUniqueID that's in 'BORROWED' status
     * @return: AccompanyMaterial -- whether user has borrowed the accompany material or not (1 - borrowed, 0 - not loan)
     */
    public function getAccompanyMaterialByBookUniqueID($bookUniqueID)
    {
        $sql = "SELECT AccompanyMaterial FROM `LIBMS_BORROW_LOG` WHERE `UniqueID`='{$bookUniqueID}' AND RecordStatus='BORROWED' ORDER BY `BorrowLogID` DESC LIMIT 1";
        $rows = $this->libms->returnVector($sql);
        $accompanyMaterial = $rows[0];
        return $accompanyMaterial > 0 ? 1 : 0;
    }

    /*
     * @param: $bookUniqueID -- BookUniqueID that's in 'BORROWED', 'WRITEOFF' and 'LOST' status
     * @return: BookID
     */
    public function getBookIDByBookUniqueID($bookUniqueID)
    {
        $sql = "SELECT BookID FROM `LIBMS_BOOK_UNIQUE` WHERE `UniqueID`='{$bookUniqueID}' AND RecordStatus IN ('BORROWED','WRITEOFF','LOST')";
        $rows = $this->libms->returnVector($sql);
        $bookID = $rows[0];
        return $bookID > 0 ? $bookID : 0;
    }

    /*
     * @param: $bookUniqueID -- BookUniqueID that's in 'BORROWED' status
     * $action -- check (check and return necessary var only) / return_book (actually do return book)
     * $Mode -- ByBatch / Individual
     * $uID -- UserID
     * $jsonError -- return error in json
     * @return:
     */
    public function returnBook($bookUniqueID, $action = 'check', $Mode = 'Individual', $uID = '', $returnDateTime = '', $HandleOverdue = 1, $jsonError=true)
    {
        global $Lang, $sys_custom;
        
        $not_allow_overdue_return = false;
        $isOverDue = false;
        $overDue = '';
        $reserves = false;
        $User = null;
        $returnResult = true; // true/false result of this function
        $isSameDayReturn = false;
        
        $userID = $this->getUserIDByBookUniqueID($bookUniqueID);
        $bookID = $this->getBookIDByBookUniqueID($bookUniqueID);
        $ret = array();
        if (($userID == 0) && ($bookID > 0)) {
            $IsReturnBookWithoutBorrowLog = true;
        } else {
            $IsReturnBookWithoutBorrowLog = false;
        }
        $BookManager = new BookManager($this->libms);
        $bookInfo = $BookManager->getBookFields($bookID, "*", $bookUniqueID);
        
        if ($IsReturnBookWithoutBorrowLog) {
            // special book return for those books in On Loan status but no loan record in eLib plus
            // change the status
            $BookLocation_result = $this->libms->SELECTFROMTABLE('LIBMS_LOCATION', $Lang['libms']['sql_field']['Description'], array(
                'LocationCode' => PHPTOSQL($bookInfo['LocationCode'])
            ), 1);
            $BookLocation = $BookLocation_result[0][0];
            
            $CirculationTypeApplied = (trim($bookInfo['Item_CirculationTypeCode']) != "") ? $bookInfo['Item_CirculationTypeCode'] : $bookInfo['CirculationTypeCode'];
            
            if ($action != 'check') {
                if ($bookInfo && $bookInfo['RecordStatus'] == 'WRITEOFF') {
                    if (! $BookManager->isBookAvailable4Reserve($bookID)) { // no available book unique item, delete reservation record
                        $reservationAry = $this->getReservationByBookID($bookID, null, 'asc', '', true);
                        if (count($reservationAry)) {
                            $reservationIDAry = BuildMultiKeyAssoc($reservationAry, 'ReservationID', array(
                                'ReservationID'
                            ), 1);
                            $this->libms->REMOVE_RESERVATION($reservationIDAry);
                        }
                    }
                } else {
                    $reserves = $this->getReservationByBookID($bookID, array(
                        'WAITING'
                    ), 'asc', '', false, $bookUniqueID, $CirculationTypeApplied);
                    $this->changeBookStatus($bookUniqueID);
                }
                $ID = "";
            }
            
            $return_row_class = "returned";
            $borrowLog['BorrowTime'] = "-";
            $borrowLog['DueDate'] = "-";
            $borrow_by_user = "";
            $alert_text = "";
        } else {
            // normal book return
            
            $User = new User($userID, $this->libms);
            $borrowLogID = $this->getBorrowLogIDFromBookUniqueID($bookUniqueID, 'BORROWED');
            $CirculationTypeApplied = $BookManager->getCirculationTypeCodeFromUniqueBookID($bookUniqueID);
            $BookLocation_result = $this->libms->SELECTFROMTABLE('LIBMS_LOCATION', $Lang['libms']['sql_field']['Description'], array(
                'LocationCode' => PHPTOSQL($bookInfo['LocationCode'])
            ), 1);
            $BookLocation = $BookLocation_result[0][0];
            
            if (! empty($borrowLogID)) {
                $borrowLog = $this->getFieldsFromBorrowLog($borrowLogID);
            } else {
                $borrowLogID = $this->getBorrowLogIDFromBookUniqueID($bookUniqueID, 'LOST');
                $latestBorrowLogID = $this->getBorrowLogIDFromBookUniqueID($bookUniqueID);
                if (! empty($borrowLogID) && $borrowLogID == $latestBorrowLogID) {
                    $borrowLog = $this->getFieldsFromBorrowLog($borrowLogID);
                    $sql = "SELECT SUM(PaymentReceived) as PaymentReceived, SUM(Payment) as Payment FROM `LIBMS_OVERDUE_LOG`
					WHERE `BorrowLogID`='{$borrowLogID}' AND RecordStatus<>'WAIVED' GROUP BY BorrowLogID";
                    $result = $this->libms->returnArray($sql);
                    $overDue = $result[0];
                    if ($overDue['PaymentReceived'] <= 0) {
                        if ($action != 'check') {
//                             // remove overdue log
//                             $sql = "DELETE FROM `LIBMS_OVERDUE_LOG`
// 							WHERE `BorrowLogID`='{$borrowLogID}'";
//                             $result = $this->libms->db_db_query($sql);

                            // mark delete overdue log
                            $sql = "UPDATE `LIBMS_OVERDUE_LOG` SET RecordStatus='DELETE', DateModified=NOW(), LastModifiedBy='".$_SESSION['UserID']."' WHERE `BorrowLogID`='{$borrowLogID}' AND RecordStatus<>'WAIVED'";
                            $result['UpdateOverdueStatus'] = $this->libms->db_db_query($sql);
                            
                            // log transaction for tracing error
                            $balanceLogAry = array();
                            $balanceLogAry['UserID'] = PHPToSQL($borrowLog['UserID']);
                            $balanceLogAry['BalanceBefore'] = PHPToSQL($User->getLatestBalance($borrowLog['UserID']));
                            
                            $amount = $overDue['Payment'] - $overDue['PaymentReceived'];
                            // add back the balance
                            $result['UpdateUserBalance'] = $this->addToUserBalance($borrowLog['UserID'], $amount);
                            
                            $balanceLogAry['TransAmount'] = PHPToSQL($amount);
                            $balanceLogAry['BalanceAfter'] = PHPToSQL($User->getLatestBalance($borrowLog['UserID']));
                            $balanceLogAry['TransDesc'] = PHPToSQL('return lost book');
                            $balanceLogAry['TransType'] = PHPToSQL('BorrowLog');
                            $balanceLogAry['TransRefID'] = PHPToSQL($borrowLogID);
                            $balanceLogAry['TransAmountBefore'] = PHPToSQL($amount);
                            $balanceLogAry['TransAmountAfter'] = 0;
                            
                            $result['AddBalanceLog'] = $this->libms->INSERT2TABLE('LIBMS_BALANCE_LOG', $balanceLogAry);
                            unset($balanceLogAry);
                        }
                    } else {
                        if ($jsonError) {
                            echo '{"success":false,"LocationIncorrect":false,"alertReservePerson":false,"BookItemStatus":"LOST"}';
                            exit();
                        }
                        else {
                            $returnResult = false;
                        }
                    }
                } else {
                    $returnResult = false;
                    // echo "<tr><td colspan='9'><span class='alert_text'>{$Lang['libms']['CirculationManagement']['return_ajax']['no_borrow_record']} : {$bookInfo['BookTitle']} [{$bookInfo['UniqueBarCode']}]</span></td></tr>";
                    if ($jsonError) {
                        echo '{"success":false,"LocationIncorrect":false,"alertReservePerson":false,"BookItemStatus":"","html":""}';
                        exit();
                    }
                }
            }
            
            if ($borrowLog["UserID"] != "") {
                $borrow_by_user = $this->getBorrowByUser($borrowLog["UserID"]);
                if ($sys_custom['eLibraryPlus']['hideUserBarcode']) {
                    $no_barcode_user = explode("#", $borrow_by_user);
                    $borrow_by_user = $no_barcode_user[0];
                }
                
                if ($Mode != "ByBatch" && $uID != "" && $uID != $borrowLog["UserID"]) {
                    $borrow_by_user = "<font color='red'>" . $borrow_by_user . "</font>";
                }
            } else {
                $borrow_by_user = "";
            }
            
            $borrowLog['BorrowTime'] = date('Y-m-d', strtotime($borrowLog['BorrowTime']));
            
            // Check only the WAITING one because the code is running before calling return_book. It does not have a "READY" reserved book (Henry)
            $reserves = $this->getReservationByBookID($bookID, array(
                'WAITING'
            ), 'asc', '', false, $bookUniqueID, $CirculationTypeApplied);
            
            if ($action != 'check') {
                
                if ($bookInfo && $bookInfo['RecordStatus'] == 'WRITEOFF') {
                    if (! $BookManager->isBookAvailable4Reserve($bookID)) { // no available book unique item, delete reservation record
                        $reservationAry = $this->getReservationByBookID($bookID, null, 'asc', '', true);
                        if (count($reservationAry)) {
                            $reservationIDAry = BuildMultiKeyAssoc($reservationAry, 'ReservationID', array(
                                'ReservationID'
                            ), 1);
                            $this->libms->REMOVE_RESERVATION($reservationIDAry);
                        }
                    }
                }
                
                $result = $User->return_book($bookUniqueID, false, '', $HandleOverdue, $returnDateTime);
                
                if ($result === - 1) {
                    // fail to return as overdue is not allowed in batch return mode
                    $isOverDue = true;
                    $not_allow_overdue_return = true;
                    $return_row_class = "";
                    $ID = "<span class='alert_text'' >" . $Lang['libms']["settings_alert"]['batch_return_handle_overdue_fail'] . "</span>";
                    $returnResult = false;
                } else if ($result === - 2) {
                    // fail to return as return at the same day is not allowed
                    $isSameDayReturn = true;
                    $return_row_class = "";
                    $ID = "<span class='alert_text'' >" . $Lang['libms']["settings_alert"]['SameDayReturn'] . "</span>";
                    $returnResult = false;
                } else {
                    $overDue = $this->getOverDueRecordFromborrowLogID($borrowLogID);
                    
                    // # still need to check if there's overdue date because the penalty amount maybe zero due to rule setting
                    if (empty($overDue)) {
                        $dueDate = strtotime($borrowLog['DueDate']);
                        $penaltyday = 0;
                        $today = time();
                        if ($today - $dueDate > 60 * 60 * 24) {
                            $penaltyday = $User->timeManager->dayForPenalty($dueDate, $today);
                            if ($penaltyday > 0) {
                                $overDue['DaysCount'] = $penaltyday;
                                $overDue['Payment'] = 0;
                            }
                        }
                    }
                    $isOverDue = (! empty($overDue));
                    $return_row_class = "returned";
                    $ID = "";
                }
            } else {
                // have not added overdue record to db, check and get record only
                $dueDate = strtotime($borrowLog['DueDate']);
                $penaltyday = 0;
                $amount = 0;
                $today = time();
                if ($today - $dueDate > 60 * 60 * 24) {
                    $penaltyday = $User->timeManager->dayForPenalty($dueDate, $today);
                    if ($penaltyday > 0) {
                        $circulationTypeCode = ($bookInfo['Item_CirculationTypeCode']) ? $bookInfo['Item_CirculationTypeCode'] : $bookInfo['CirculationTypeCode'];
                        
                        $limits = $User->rightRuleManager->get_limits($circulationTypeCode);
                        $amount = $limits['OverDueCharge'] * $penaltyday;
                        
                        if ($limits['MaxFine'] > 0) {
                            $amount = ($limits['MaxFine'] < $amount) ? $limits['MaxFine'] : $amount;
                        }
                    }
                }
                $overDue['DaysCount'] = $penaltyday;
                $overDue['Payment'] = $amount;
                
                $isOverDue = ($penaltyday > 0) || ($amount > 0);
                
                $return_row_class = "returned";
            }
            $alert_text = ($isOverDue) ? 'class="alert_text"' : '';
        }
        
        $ret = compact('bookInfo', 'borrowLog', 'return_row_class', 'IsReturnBookWithoutBorrowLog', 'borrow_by_user', 'alert_text', 'not_allow_overdue_return', 'isOverDue', 'overDue', 'BookLocation', 'reserves', 'User', 'ID', 'returnResult', 'borrowLogID', 'isSameDayReturn');
        return $ret;
    }
 // end function returnBook()
    function gen_return_book_list($view_data)
    {
        global $Lang, $libms, $PATH_WRT_ROOT, $sys_custom;
        
        extract($view_data);
        
        $x = '';
        $bookID = $bookInfo['BookID'];
        
        if (($libms->get_system_setting("circulation_borrow_return_accompany_material")) && (! $borrowLog['AccompanyMaterial']) && (! empty($bookInfo['AccompanyMaterial']))) {
            $attachmentNote = $Lang["libms"]["Circulation"]["NotBorrowAccompayMaterial"];
        } else {
            $attachmentNote = $Lang["libms"]["book"]["AccompanyMaterial"];
        }
        
        $displayBookTitle = $bookInfo['BookTitle'];
        if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] && !empty($bookInfo['BookSubTitle'])) {
            $displayBookTitle .= " :" . $bookInfo['BookSubTitle'];
        }
        $displayBookTitle .= " [".$bookInfo['UniqueBarCode']."]";
        
        $thickboxWidth = ($sys_custom['eLibraryPlus']['Circulation']['LargeFont']) ? 1000 : 820;
        
        ob_start();
        
        ?>

<tr class="<?=$return_row_class?>" id="<?=$barcode?>">
	<td><?=$ID?><input type='hidden'
		value='<?=($return_row_class == 'returned')?$bookUniqueID:''?>'
		name='bookUniqueID[]' class='bookUniqueID' /></td>
	<td><a
		href="javascript:tb_show('<?=mysql_real_escape_string(htmlspecialchars($Lang["libms"]["Circulation"]["BookBorrowHistory"]))?>', 'book_loans_history.php?BarCode=<?=urlencode($barcode)?>&KeepThis=true&height=500&width=600')"><?=$displayBookTitle?></a></td>
	<td nowrap="nowrap"><?=$borrowLog['BorrowTime']?>
	  <? if (!empty($bookInfo['RemarkToUser'])): ?>
		 		<br /> <span><img src="/images/2009a/eLibplus/icon_alert.png" /><?=$bookInfo['RemarkToUser']?></span>
		 <? endif; ?>
	  <? if ($libms->get_system_setting("circulation_display_accompany_material") && !empty($bookInfo['AccompanyMaterial'])): ?>
	 		<br /> <span><img src="/images/2009a/eLibplus/icon_alert.png" /><?=$bookInfo['AccompanyMaterial'].' ['.$attachmentNote.']'?></span>
	  <? endif; ?>
	  </td>
<?php if ($IsReturnBookWithoutBorrowLog) { ?>
		<td>-</td>
<?php } else { ?>
	  <td><a
		href="javascript:tb_show('<?=mysql_real_escape_string(htmlspecialchars($Lang["libms"]["Circulation"]["ReaderBorrowHistory"]))?>', 'reader_loans_history.php?user_id=<?=urlencode($borrowLog["UserID"])?>&KeepThis=true&height=500&width=<?php echo $thickboxWidth;?>')"><?=$borrow_by_user?></a></td>
<?php } ?>
	  <td nowrap="nowrap"><span <?=$alert_text?>><?=$borrowLog['DueDate']?></span></td>
<?php if ($not_allow_overdue_return) {?>
	<td colspan="2"><span <?=$alert_text?>><?=  $Lang['libms']["settings_alert"]['batch_return_handle_overdue'] ?></span></td>
<?php } else { ?>
	  <td nowrap="nowrap"><span <?=$alert_text?>><?=($isOverDue)? $overDue['DaysCount'] : '-' ?></span></td>
	<td><span <?=$alert_text?>><?=($isOverDue)? $payment_text_link :'-' ?></span></td>
<?php } ?>
	  <td nowrap="nowrap"><?=empty($BookLocation)?'-':$BookLocation ?></td>
	<td nowrap="nowrap">
		  <?if (!empty($reserves)):?>
		  
		  
			<?php
            if ($checkOnly) {
                $CirculationTypeApplied = (trim($bookInfo['Item_CirculationTypeCode']) != "") ? $bookInfo['Item_CirculationTypeCode'] : $bookInfo['CirculationTypeCode'];
                $reserves1 = $this->getReservationByBookID($bookID, array(
                    'WAITING'
                ), 'asc', '', false, $bookUniqueID, $CirculationTypeApplied);
            } else {
                $reserves1 = $User->recordManager->getReservationByBookID($bookID, array(
                    'READY'
                ), 'asc', "", false, $bookUniqueID, '', true);
            }
            ?>
				
				
			<span id="span_alert_avaliable" class="alert_avaliable"><?=$Lang['libms']["settings"]['system']['yes']?></span>
		<br /> <span id="span_alert_avaliable1" class="alert_avaliable"><?
            include_once ($PATH_WRT_ROOT . "includes/libuser.php");
            $lu = new libuser($reserves1['UserID']);
            
            $PatronName = $lu->getNameWithClassNumber($reserves1['UserID']);
            if (isset($junior_mck)) {
                $PatronName = iconv("BIG5-HKSCS", "UTF-8//IGNORE", $PatronName);
            }
            echo "\n" . $Lang['libms']['CirculationManagement']['msg']['reserved_by'] . ": " . $PatronName;
            ?><br /><?php
            
            // $reserves1 = $User->recordManager->getReservationByBookID($bookID,array('READY'));
            if ($reserves1['ExpiryDate'] != "0000-00-00" && $reserves1['ExpiryDate'] != "") {
                echo "\n" . $Lang['libms']['CirculationManagement']['msg']['expiry_date'] . ": " . $reserves1['ExpiryDate'];
            }
            ?></span>
		  <?else:?>
		  -
		  <?endif;?>
	  </td>
</tr>

<?php
        $x = ob_get_contents();
        ob_end_clean();
        
        $x = str_replace("\n", '', $x);
        $x = str_replace("\t", '', $x);
        $x = str_replace("\\", "\\\\", $x);
        
        return $x;
    }
 // end function gen_return_book_list
    function output($json_data)
    {
        echo json_encode($json_data);
        exit();
    }

    // include these RecordStatus: BORROWED, RENEWED, BORROWED
    public function getNumberOfLoansByBook($userID, $bookID)
    {
        if (empty($userID) || empty($bookID)) {
            return false;
        }
        
        $sql = "SELECT
                        COUNT(*) AS NumberOfLoans 
                FROM    
                        `LIBMS_BORROW_LOG` 
                WHERE 
                        UserID='".$userID."'
                AND     BookID='".$bookID."'";
        
        $result = $this->libms->returnResultSet($sql);
        
        $numberOfLoans = count($result) ? $result[0]['NumberOfLoans'] : 0;    
        
        return $numberOfLoans;
   }
    
}
?>