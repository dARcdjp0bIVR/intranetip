<?php
// Editing by
/*
 * 2019-10-11 (Cameron)
 * - use large font for common_table_list if $sys_custom['eLibraryPlus']['Circulation']['LargeFont'] is set [case #K155570]
 * 
 * 2019-03-28 (Cameron)
 * - show BookSubTitle if $sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] is true [case #K154500]
 * 
 * 2018-02-22 (Cameron)
 * - fix: add meta characterset (utf-8) to show no access right message
 *
 * 2017-03-14 (Cameron)
 * - add IP checking for circulation
 *
 * 2016-09-07 (Cameron)
 * - support showing and adding additional charge for lost book according to the setting [case #F76872]
 *
 * 2016-08-31 (Cameron)
 * - fix bug: allow to settle zero fine record [case #L102005]
 * - not allow to check record if input amount is zero but Remained value is non-zero
 * - set Remained value to zero when check record
 *
 * 2016-07-29 (Cameron)
 * - append timestamp to jquery.alerts.js so that it can refresh to take effect
 *
 * 2016-02-23 (Cameron)
 * - fix bug of showing total overdue fine in two digits after decimal point
 *
 * 2015-06-10 (Henry)
 * - apply class management permission
 * 2015-03-18 (Cameron)
 * -- Modify function waiveCheck() to use password prompt rather than plain text
 * -- Add function authenticate()
 * 2014-12-16 (Henry): disable the submit button if clicked
 * 2014-12-10 (Ryan): Fix Overdue records' Penalty Type String displayed incorrectly [$Lang["libms"]["CirculationManagement"]["overdue_day"] -> $Lang["libms"]["CirculationManagement"]["overdue_with_day"] ]
 * 2014-11-27 (Ryan): Add orverride password for waive depends on system_setting waive_password
 * 2013-12-09 (Carlos): added remark for ePayment payment method
 * 2013-09-02 (Carlos): if $plugin['payment'] && $sys_custom['eLibraryPlus']['PayBy_ePayment'], use ePayment to pay overdue payment
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ("BookManager.php");
include_once ("User.php");
include_once ("RecordManager.php");
include_once ("RightRuleManager.php");

// include_once("libms_interface.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
intranet_auth();
intranet_opendb();

if ($user_id != "" && $user_id > 0) {
    $_SESSION['LIBMS']['CirculationManagemnt']['UID'] = urldecode($user_id);
}

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);
$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['overdue'] || $global_lockout))) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}
include_once ('TimeManager.php');
$TM = new TimeManager();
if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! $TM->isOpenNow(time()) && ! $c_right['circulation management']['access_without_open_hour']) {
    header("Location: index.php");
}

if (! isset($_SESSION['LIBMS']['CirculationManagemnt']['UID'])) {
    header("Location: index.php");
    die();
}

$is_additional_charge = $libms->get_system_setting('circulation_lost_book_additional_charge');
$allow_change_penalty = $libms->get_system_setting('circulation_lost_book_allow_change_penalty');

$interface = new interface_html("libms_circulation.html");
$GLOBALS['LIBMS']['PAGE'] = "OVERDUE";

$uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];

$pay_by_ePayment = $plugin['payment'] && $sys_custom['eLibraryPlus']['PayBy_ePayment'];
// $pay_by_ePayment = false;
// $User = new User($uID, $libms);

$recordManager = new RecordManager($libms);
$cond[] = "ol.`RecordStatus` IN ('OUTSTANDING')";
$overdues = $recordManager->getOverDueRecords($uID, $cond);

$xmsg = $_SESSION['LIBMS']['xmsg'];
unset($_SESSION['LIBMS']['xmsg']);

$interface->LAYOUT_START();

if ($sys_custom['eLibraryPlus']['Circulation']['LargeFont']) {
    echo '<link href="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/css/elibplus_large_font.css" rel="stylesheet" type="text/css">'."\n";
}

?>
<link href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.alerts.css"
	rel="stylesheet" type="text/css">
<script language="JavaScript"
	src="<?=$PATH_WRT_ROOT?>lang/script.<?=$intranet_session_language?>.js"></script>
<script language="JavaScript"
	src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.alerts.js?<?=time()?>"></script>
<script type="text/javascript"
	src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.jeditable.js"></script>
<script language="JavaScript"
	src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>

<script type="text/javascript">
var cached_password=false;
var password_match=false;

function authenticate(checkFor, is_password_matched,targetAdditionalChargeID) {
	if( cached_password == '' || cached_password == null){
		cached_password = false;
		return false;
	}		

	$.get(	'override_ajax.php',
		{ override_password : cached_password	},
		function(data){
			if(data !='1'){
				cached_password = false;	
				if (checkFor == 'waive') {									
					jAlert("<?=$Lang["libms"]["Circulation"]["WrongPassword"]?>", "<?=$Lang["libms"]["report"]["fine_waived"]?>",function(){return waiveCheck();});
				}
				else {
					jAlert("<?=$Lang["libms"]["Circulation"]["WrongPassword"]?>", "<?=$Lang['libms']["settings"]['system']['circulation_lost_book_allow_change_penalty']?>",function(){return changeAdditionalChargeCheck(is_password_matched);});
				}
				password_match = false;
			}else{
				password_match = true;
				if ((checkFor == 'changeCharge') && (is_password_matched == false)) {
					load_dyn_size_thickbox_ip('<?=$Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge']?>', 'onloadThickBox('+targetAdditionalChargeID+');', inlineID='', defaultHeight=400, defaultWidth=600);
				}
			}
		}
	);
}

function waiveCheck(){
	<?
$waive_password = $libms->get_system_setting('waive_password');
if (! empty($waive_password)) :
    ?>
			if( !cached_password  && !password_match ){
				jPromptPassword( "<?=$Lang["libms"]["Circulation"]["PleaseEnterPassword"]?>:", "", "<?=$Lang["libms"]["report"]["fine_waived"]?>",function(r) {
					if (r) {
						cached_password = r;
						authenticate('waive','','');
					}	// get input password					
				});
			}

	<? else: ?>
			password_match = true;
	<? endif; ?>
	}
	
function changeAdditionalChargeCheck(is_password_matched,targetAdditionalChargeID){
	<?
$waive_password = $libms->get_system_setting('waive_password');
if (! empty($waive_password)) :
    ?>
		
			if( !cached_password  && !password_match ){
				jPromptPassword( "<?=$Lang["libms"]["Circulation"]["PleaseEnterPassword"]?>:", "", "<?=$Lang['libms']["settings"]['system']['circulation_lost_book_allow_change_penalty']?>",function(r) {
					if (r) {
						cached_password = r;
						authenticate('changeCharge',is_password_matched,targetAdditionalChargeID);
					}	// get input password
				});
			}
			else {
				return true;
			}

	<? else: ?>
			password_match = true;
			return true;
	<? endif; ?>
	}
	
function updateTotal(){
	var total =0;
	$('input.paying').each(function(){
	<?php// if($pay_by_ePayment){ ?>
		paymentCheckbox = $(this).parents('tr:first').find('input.paymentCheckbox');
		waiveCheckbox = $(this).parents('tr:first').find('input.waived');
		if(paymentCheckbox.is(':checked') && !waiveCheckbox.is(':checked')){
			paying = parseFloat($(this).val());
			paying = (isNaN(paying)) ? 0 : paying;
			total = total + paying;
		}
	<?php// }else{ ?>
//		paying = parseFloat($(this).val());
//		paying = (isNaN(paying)) ? 0 : paying;
//		total = total + paying;
	<?php// } ?>
	});
	total = Math.round(total * 100)/100;
	$('label#total_payment').html(total);
}

$().ready( function(){
	$.ajaxSetup({ cache: false});
	//--------------------------------------------------
	$(window).on('beforeunload', function(){
		if ($('input.waived:checked').length > 0)
			return '<?=$Lang['libms']['CirculationManagement']['msg']['comfirm_leave']?>';
		if ($('label#total_payment').html() != "0")
			return '<?=$Lang['libms']['CirculationManagement']['msg']['comfirm_leave']?>';
	});
	
	$('input.waived').click(function(e){
		if(this.checked){
			waiveCheck();
		}
	});
	
	$('input#overdue_submit').click(function(e){
		$error_input = $('input.paying[error=true]')
		if($error_input.length > 0){
			$error_input
				.animate({'background-color': 'white'},200).animate({'background-color': 'yellow'},200)
				.animate({'background-color': 'white'},200).animate({'background-color': 'red'},200)
				.animate({'background-color': 'white'},200).animate({'background-color': 'yellow'},200);
			$error_input[0].focus();
			alert('<?=$Lang['libms']['CirculationManagement']['msg']['fix_error']?>');
			e.preventDefault();
			return false;
		}
		if ($('input.waived:checked').length > 0){
			waiveCheck();
			if(password_match == false){
			  return false;
			}else{
				$(window).off('beforeunload');
			} 			
		}
		$(window).off('beforeunload');
	});
	
	
	
	//--------------------------------------------------
	<?php// if($pay_by_ePayment){ ?>
	<?php if($borrow_log_id){ ?>
		updateTotal();
	<?php } ?>
	$('input.paymentCheckbox').change(function(){		
		var thisOverdueID = $(this).attr('name').replace('PayByPayment[', '').replace(']','');
		$('input#paying_'+thisOverdueID).trigger("change");
	});
	$('input.waived').change(function(){
		updateTotal();
	});
	<?php// }else{ ?>
//	$('input.waived').change( function(){
//		thisCheckbox = $(this);
//		thisinputbox = thisCheckbox.parents('tr:first').find('input.paying');
//		if (thisCheckbox.is(':checked')){
//			thisinputbox.val('').attr('disabled', true);
//			thisinputbox.val('').attr('error', false);
//			thisinputbox.change();
//		}else{
//			thisinputbox.val('').attr('disabled', false);
//			thisinputbox.val('');
//		}
//	});
	<?php// } ?>
	$('input.paying').change( function(){
		thisbox = $(this);
		if (thisbox.val().length ==0 ){
			thisbox.val(0);
		}
		tr = thisbox.parents('tr:first');
		var payment = parseFloat(tr.find('label.Payment').html());
		var paymentReceived = parseFloat(tr.find('label.PaymentReceived').html());
		var paying = parseFloat(thisbox.val());
		var balance = payment - paymentReceived;
	<? if ($is_additional_charge):?>
		var additionalCharge = parseFloat(tr.find('label.AdditionalCharge').html());
			balance += additionalCharge;
	<? endif;?>			 		
		
		payment = isNaN(payment) ? 0 : payment;
		paymentReceived = isNaN(paymentReceived) ? 0 : paymentReceived;
		
		if (isNaN(paying) || paying < 0){
			paying = 0;
			thisbox.attr('error',true).animate({'background-color': 'white'},150).animate({'background-color': 'yellow'},150);
		}else{
			if (paying > balance){
				thisbox.val(balance);
				paying = balance;
			}else{
				thisbox.val(paying);
			}
			thisbox.attr('error',false).animate({'background-color': 'yellow'},150).animate({'background-color': 'white'},150);
		}
		
		var thisOverdueID = thisbox.attr('id').replace('paying_', '');
		var thisRemainingLabelId = 'remaining_' + thisbox.attr('id').replace('paying_', '');
		remaining = $('label#' + thisRemainingLabelId);
		
		remaining.html((balance - paying).toFixed(2));
		updateTotal();
		
		if ((parseFloat(remaining.html()) > 0) && (thisbox.val() == 0)) {
			$('input[name="PayByPayment\['+thisOverdueID+'\]"]').attr('checked',false);	// don't allow input 0 if fine is non-zero
		}
		
	}); 

	$('#reset').click(function(){
		$('input.paying').each(function(){$(this).val(""); $(this).trigger("change");});
		$('input.waived').each(function(){
			THIS= $(this);
			if ( THIS.is(':checked') ){
//				THIS.click();  disable due to waivecheck function  
				THIS.attr('checked',false); // uncheck
			}
		});
	});
	

	
	$(document).bind('keydown',function(e){
			
			switch (e.keyCode)
			{
			    case 120:	// F9
			    	updateTotal();
					$('input#overdue_submit').click();
					return false;
			    default:
			      return true;
			}
  
  
		});
		
		
	$(document).on('click','div.thickbox',function(){
		
		var thisDivID = $(this).attr("id");
		var idArr = thisDivID.split('_');
		var divID = idArr[0];
		targetAdditionalChargeID = idArr[1];
		
		changeAdditionalChargeCheck(password_match,targetAdditionalChargeID);
		
		if( password_match == true){
			load_dyn_size_thickbox_ip('<?=$Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge']?>', 'onloadThickBox('+targetAdditionalChargeID+');', inlineID='', defaultHeight=400, defaultWidth=600);
		}		
	});
			
});

function checkAllRecord() {
	var thisOverdueID;
	if ($('#CheckMaster').is(":checked")) {
		$('.paymentCheckbox').each(function(){
			$(this).prop('checked',$('#CheckMaster').prop('checked'));
			thisOverdueID = $(this).attr('name').replace('PayByPayment[', '').replace(']','');
			$('input#paying_'+thisOverdueID).trigger("change");
		});
	}
	else {
		$('#reset').trigger("click");
	}
}

function checkform(frmObj)
{
	document.getElementById('overdue_submit').disabled = true;
//	if ($('input.waived:checked').length<=0 && $('label#total_payment').html() == "0")
	if ($('input.paymentCheckbox:checked').length<=0)
	{
		alert("<?=$Lang['libms']['CirculationManagement']['msg']['no_record_to_proceed']?>");
		document.getElementById('overdue_submit').disabled = false;
		return false;
	}
}

function Show_Edit_Background(Obj) {
	Obj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	Obj.style.backgroundPosition = "center right";
	Obj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Background(Obj) {
	Obj.style.backgroundImage = "";
	Obj.style.backgroundPosition = "";
	Obj.style.backgroundRepeat = "";
}

function onloadThickBox(over_due_log_id) {
  	$('div#TB_ajaxContent').load(
    	"edit_lost_book_additional_charge.php",
    	{
      		'OverDueLogID': over_due_log_id
    	}, 
    	function(ReturnData) {
      		adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
    	}
  	);
}

</script>

<? if(!empty($xmsg)): ?>
<center>
	<div align="center" style='width: 1%'>
		<table border="0" cellpadding="3" cellspacing="0" class="systemmsg">
			<tr>
				<td nowrap="nowrap"><font color="green"><?=$xmsg?></font></td>
			</tr>
		</table>
	</div>
</center>
<?endif;?>

<form name="renew_form" action='overdue_action.php'
	onsubmit="return checkform(this)">
	<div class="table_board">
		<div class="enter_code">&nbsp;</div>
		<table class="common_table_list">
			<col nowrap="nowrap" />

			<tr>
				<th class="num_check">#</th>
				<th><?=$Lang['libms']['CirculationManagement']['booktitle']?>
			</th>
				<th><?=$Lang['libms']['CirculationManagement']['overdue_type']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['payment']?></th>
		<? if ($is_additional_charge):?>
			<th><?=$Lang["libms"]["CirculationManagement"]["additional_charge"]?></th>
		<? endif;?>			
			<th><?=$Lang['libms']['CirculationManagement']['paied']?></th>
				<th nowrap="nowrap"><?=$Lang['libms']['CirculationManagement']['free_payment']?></th>
				<th <?=$pay_by_ePayment?' style=""':''?>><?=$Lang['libms']['CirculationManagement']['pay_now']?></th>
				<th><?=$Lang['libms']['CirculationManagement']['left']?></th>
			<?php// if($pay_by_ePayment){ ?>
				<th class="num_check"><input type="checkbox" id="CheckMaster"
					value="1" onclick="checkAllRecord();" /></th>
			<?php// } ?>
		</tr><?php 
// ADD HERE

$i = 0;
if (! empty($overdues))
    foreach ($overdues as $overdue) {
        
        if (! $_SESSION['LIBMS']['admin']['current_right']['circulation management'] && $c_m_right['circulation management'] && ! $libms->get_class_mgt_can_handle_item($overdue['UniqueID'])) {
            continue; // Henry added
        }
        
        $displayBookTitle = $overdue['BookTitle'];
        if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle']) {
            $displayBookTitle .= $overdue['BookSubTitle'] ? " :" . $overdue['BookSubTitle'] : "";
        }
        $displayBookTitle .= " [".$overdue['BarCode']."]";
        
        $i ++;
        $payment = ($is_additional_charge) ? ($overdue['Payment'] - $overdue['AdditionalCharge']) : $overdue['Payment'];
        ?>
					<tr id='<?=$overdue['OverDueLogID']?>' class='overdue'>
				<td><?=$i?></td>
				<td><?=$displayBookTitle?></td>
				<td nowrap="nowrap"><?=(($overdue['DaysCount']!="")? sprintf($Lang["libms"]["CirculationManagement"]["overdue_with_day"], $overdue['DaysCount']) : $Lang["libms"]["book_status"]["LOST"]) ?></td>
				<td nowrap="nowrap">$<label class='Payment'><?=$payment?></label></td>
				<? if ($is_additional_charge):?>
					<td nowrap="nowrap">
					<? if (($overdue['DaysCount'] == "") && $allow_change_penalty):?>
						<div id="AdditionalCharge_<?=$overdue['OverDueLogID']?>"
						onmouseover="Show_Edit_Background(this);"
						onmouseout="Hide_Edit_Background(this);" class="thickbox">
						$<label class="AdditionalCharge"><?=$overdue['AdditionalCharge']?></label>
					</div>
					<? else:?>
							$<label class="AdditionalCharge"><?=$overdue['AdditionalCharge']?></label>
					<? endif;?>
					</td>
				<? endif;?>			
						
						<td nowrap="nowrap">$<label class='PaymentReceived'><?=empty($overdue['PaymentReceived'])?'0':$overdue['PaymentReceived']?></label></td>
				<td><input class="waived"
					name="waived[<?=$overdue['OverDueLogID']?>]" type="checkbox"
					value="<?=$overdue['OverDueLogID']?>" /></td>
				<td nowrap="nowrap" <?=$pay_by_ePayment?' style=""':''?>>$<input
					name="paying[<?=$overdue['OverDueLogID']?>]" type="text"
					class="textboxnum2 paying"
					id="paying_<?=$overdue['OverDueLogID']?>"
					value="<?=/*$pay_by_ePayment?*/$overdue['Payment']-$overdue['PaymentReceived']//:''?>"
					style="margin-top: -5px; margin-left: 3px;" />
				</td>
				<td nowrap="nowrap">$<label
					id="remaining_<?=$overdue['OverDueLogID']?>" class='remaining'><?=$overdue['Payment']-$overdue['PaymentReceived']?></label></td>
						<?php if(1/*$pay_by_ePayment && empty($overdue['PaymentReceived'])*/){ ?>
						<td><input type="checkbox" class="paymentCheckbox"
					name="PayByPayment[<?=$overdue['OverDueLogID']?>]" value="1"
					<?=($borrow_log_id == $overdue['BorrowLogID']?'checked':'')?> /></td>
						<?php }else if($pay_by_ePayment){ ?>
						<td>&nbsp;</td>	
						<?php } ?>
					</tr><?php
    } // end of foreach
unset($i);
?>
	</table>
	</div>
<?php if($pay_by_ePayment){ ?>
<div style="margin: 8px;">
		<span class="tabletextrequire">*</span><?=$Lang["libms"]["CirculationManagement"]['msg']["AllOverduePaymentPaidWithePayment"]?></div>
<?php } ?>
<div class="op_btn">
		<div class="op_num">
			<span><?=$Lang['libms']['CirculationManagement']['total_pay']?> :</span><em>
				$<label id='total_payment'>0</label>
			</em>
		</div>
		<div class="form_btn">
			<input name="submit" id="overdue_submit" type="submit"
				class="formbutton"
				value="<?=$Lang['libms']['CirculationManagement']['confirm'].$Lang["libms"]["CirculationHotKeys"]["Confirm"]?>" />
			<input name="submit2" id='reset' type="reset" class="formbutton"
				value="<?=$Lang['libms']['CirculationManagement']['cancel'].$Lang["libms"]["CirculationHotKeys"]["Cancel"]?>" />
		</div>
	</div>
</form>
<div class="FakeLayer"></div>
<?php
$interface->LAYOUT_STOP();
exit();
?>