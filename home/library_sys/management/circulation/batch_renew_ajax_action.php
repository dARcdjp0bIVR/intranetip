<?php

// being modified by:

/**
 * Log :
 *
 * Date: 2019-06-14 [Cameron]
 * - fix: don't show colon if BookSubTitle is empty
 * 
 * Date: 2019-03-05 [Cameron]
 * - show BookSubTitle in gen_renew_html_row() if $sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] is true [case #K154500] 
 * 
 * Date: 2018-02-22 [Cameron]
 * - fix: add meta characterset (utf-8) to show no access right message
 * - replace htmlspecialchars by intranet_htmlspecialchars
 * 
 * Date 2017-03-14 [Cameron]
 * - add IP checking for circulation
 *
 * Date 2016-03-07 [Cameron]
 * - improvement: add confirm process before renew
 *
 * Date 2015-11-05 [Cameron]
 * - don't prompt user to renew accompany material or not when renew a book, the flag can be retrieved from borrow log
 * - cancel passing AccompanyMaterial to renew_book(), gen_renew_html_row()
 *
 * Date 2015-10-30 [Cameron]
 * - fix bug on showing accompany material that contains back slash in jConfirm
 *
 * Date 2015-10-14 [Cameron]
 * - add prompt confirm (jConfirm) if user want to renew accompany material or not if circulation_borrow_return_accompany_material
 * is true and the book item has accompany material
 * - fix bug on showing number of "Renew left"
 * - fix words showing total number of books renew successfully in Chinese
 *
 * Date 2015-07-13 [Cameron] show AccompanyMaterial if circulation_display_accompany_material is set
 * Date 2014-12-01 [Ryan] Add Suspended User Checking
 * Date 2013-11-06 [Yuen] introduced this function
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

// ###### Trim all request #######
function recursive_trim(&$array)
{
    foreach ($array as &$item) {
        if (is_array($item)) {
            recursive_trim($item);
        } else {
            if (is_string($item))
                $item = trim($item);
        }
    }
}
recursive_trim($_REQUEST);
// ###### END OF Trim all request #######

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/pdump/pdump.php");
include_once ('User.php');
include_once ('BookManager.php');
include_once ('RightRuleManager.php');
include_once ('TimeManager.php');
intranet_auth();
intranet_opendb();

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);

$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['renew'] || $global_lockout))) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}
$TM = new TimeManager();
if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! $TM->isOpenNow(time()) && ! $c_right['circulation management']['access_without_open_hour']) {
    $error_msg = $Lang['libms']['CirculationManagement']['library_closed'];
    echo $error_msg;
    exit();
}

function gen_confirm_script($bookUniqieID, $barcode, $accompanyMaterial)
{
    global $Lang, $libms;
    $x = '';
    ob_start();
    ?>
<script>
var withAccompanyMaterial; 
jConfirm("<?=str_replace("\\","\\\\\\\\",$accompanyMaterial)."<br>".$Lang["libms"]["Circulation"]["RenewAccompayMaterial"]?>",
		 "<?=$Lang['libms']["settings"]['system']['circulation_borrow_return_accompany_material']?>", 
		 function(r){
		 	if (r) {
		 		withAccompanyMaterial = 1;		 		
		 	}
		 	else {
		 		withAccompanyMaterial = 0;
		 	}
			$.getJSON(
					'batch_renew_ajax_action.php',
					{ 
						async: true,
						barcode : '<?=$barcode?>',
						ID : $('table#return_table tr.returned').length,
						Mode : "ByBatch", 
						HandleOverdue : "<?=$libms->get_system_setting("batch_return_handle_overdue")?>", 
						HasChosenRenewAttachment : "true",
						WithAccompanyMaterial : withAccompanyMaterial 
					},
					'JSON'
			)
			.success(renew_ajax_handler)
			.error(function(){ alert('Error on AJAX call!'); });
		 }, 
		 "<?=$Lang['libms']["settings"]['system']['yes']?>", 
		 "<?=$Lang['libms']["settings"]['system']['no']?>");
</script>
<?php
    $x = ob_get_contents();
    ob_end_clean();
    $x = str_replace("\n", '', $x);
    $x = str_replace("\t", '', $x);
    return $x;
}

function output($json_data)
{
    echo json_encode($json_data);
    exit();
}

function gen_renew_html_row($books, $bookUniqueID, $ID, $UserNameDisplay, $confirmed = true)
{
    global $Lang, $User, $BookManager, $libms, $needConfirm, $sys_custom;
    
    $x = '';
    $i = 0;
    ob_start();
    // $books only contains one record
    foreach ($books as $book) :
        $i ++;
        $searchChar = '/[!"#$%&\'()*+,.\/:;<=>?@[\]^`{|}~ ]/';
        
        $renew_result["from"] = "--";
        $renew_result["to"] = "--";
        $record_ID = "--";
        $return_row_class = "";
        if ($book['canRenew']) {
            if ($confirmed) {
                $result = $User->renew_book($bookUniqueID, true);
                $result_message = ($result) ? $Lang["libms"]["Circulation"]["action_success"] : $Lang["libms"]["Circulation"]["action_fail"];
                
                // find the new dates
                if ($result) {
                    $sql = "SELECT BorrowTime, DueDate FROM LIBMS_BORROW_LOG WHERE UniqueID='{$bookUniqueID}' AND RecordStatus='BORROWED' ORDER BY BorrowLogID DESC LIMIT 1";
                    $rows = $BookManager->libms->returnArray($sql);
                    
                    $new_record = $rows[0];
                    $renew_result["from"] = date("Y-m-d", strtotime($new_record["BorrowTime"]));
                    $renew_result["to"] = $new_record["DueDate"];
                    $record_ID = $ID + 1;
                    $return_row_class = "class='returned'";
                }
            } else { // not confirm
                $limits = $User->rightRuleManager->get_limits($book['CirculationTypeCode']);
                
                $timeManager = new TimeManager();
                $strToday = date("Y-m-d");
                
                if ($libms->get_system_setting("circulation_duedate_method")) {
                    $dueDate = $timeManager->findOpenDateSchoolDays(strtotime($strToday), $limits['ReturnDuration']);
                } else {
                    $dueDate = $timeManager->findOpenDate(strtotime("{$strToday} + {$limits['ReturnDuration']} days"));
                }
                $renew_result["from"] = $strToday;
                $renew_result["to"] = date('Y-m-d', $dueDate);
                $record_ID = $ID + 1;
                $return_row_class = "class='returned avaliable_row'";
                $result_message = "";
                unset($timeManager);
            }
        } else { // cannot renew, but lib admin can force to renew
            $record_ID = $ID + 1;
        }
        
        if (($libms->get_system_setting("circulation_borrow_return_accompany_material")) && (! $book['BorrowAccompanyMaterial']) && (! empty($book['AccompanyMaterial']))) {
            $attachmentNote = $Lang["libms"]["Circulation"]["NotBorrowAccompayMaterial"];
        } else {
            $attachmentNote = $Lang["libms"]["book"]["AccompanyMaterial"];
        }
        
        $displayBookTitle = $book['BookTitle'];
        if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] && !empty($book['BookSubTitle'])) {
            $displayBookTitle .= " :" . $book['BookSubTitle'];
        }
        $displayBookTitle .= " [".$book['BarCode']."]";
        
        ?>
<tr <?=$return_row_class?> id="<?=$book['BarCode']?>">
	<td><?=$record_ID?></td>
	<td><a
		href="javascript:tb_show('<?=mysql_real_escape_string(intranet_htmlspecialchars($Lang["libms"]["Circulation"]["BookBorrowHistory"]))?>', 'book_loans_history.php?BarCode=<?=urlencode($book['BarCode'])?>&height=500&width=600')"><?=$displayBookTitle?></a></td>
	<td nowrap="nowrap"><?=$UserNameDisplay?></td>
	<td nowrap="nowrap"><?=date('Y-m-d',strtotime($book['BorrowTime']))?></td>
	<td nowrap="nowrap"><?=$book['DueDate']?></td>
	<td nowrap="nowrap">
			  <?
        if ($book['canRenew']) {
            $remainTimes = $book['limit']['LimitRenew'] - $book['RenewalTime'] - ($confirmed ? 1 : 0);
            $remainTimes = $remainTimes >= 0 ? $remainTimes : 0;
            echo ($remainTimes); // should deduct this renew if confirmed
        } else {
            echo '0';
        }
        ?>
		  </td>
	<td nowrap="nowrap"><span
		<? if (!$book['canRenew']) echo 'class="alert_text"'?>>
				<? if (!$book['canRenew']) : ?>
					<?=$Lang["libms"]["CirculationManagement"]["n_renew"]?>
					</span> &nbsp;
					(<?=$book['MSG']?>)
				<? else: ?>
					<? if (!$confirmed) echo $Lang['libms']['CirculationManagement']['y_renew'];?>				
					<?=$result_message?>
					</span>
				<? endif; ?>
				<? if (!empty($book['RemarkToUser'])): ?>
			 		<br /> <img src="/images/2009a/eLibplus/icon_alert.png" /><span><?=$book['RemarkToUser']?></span>
		 		<? endif; ?>
			  	<? if ($libms->get_system_setting("circulation_display_accompany_material") && !empty($book['AccompanyMaterial'])): ?>
			 		<br /> <span><img src="/images/2009a/eLibplus/icon_alert.png" /><?=$book['AccompanyMaterial'] . ' ['.$attachmentNote.']'?></span>
			  	<? endif; ?>
		 		
		  </td>

<?php if (!$confirmed && $needConfirm): ?>
		  <td><div id="div_<?=$book['UniqueID']?>">
		  <? if (!$book['canRenew']) : ?>
		  <input name="submit2" type="button" class="set_borrow"
				value="<?=$Lang["libms"]["CirculationManagement"]["msg"]["f_renew"]?>"
				onclick='allowRenew(this,<?=$book['UniqueID']?>)' />
		  <? else: ?>
		  <input class='renewCheckbox' name="renew[]" type="checkbox"
				value="<?=$book['UniqueID']?>" checked />
		  <? endif; ?>
		  </div></td>
<?php endif;?>
		  <td nowrap="nowrap" class='today'><?=$renew_result["from"]?></td>
	<td nowrap="nowrap" class='newDueDate'><?=$renew_result["to"]?></td>
</tr>
<? endforeach;//( $books as $book ): ?>
<?php
    $x = ob_get_contents();
    ob_end_clean();
    $x = str_replace("\n", '', $x);
    $x = str_replace("\t", '', $x);
    $x = str_replace("\\", "\\\\", $x);
    
    return $x;
} // end function gen_renew_html_row()

$needConfirm = $libms->get_system_setting("batch_book_renew_by_confirm");
$confirmRenew = $_REQUEST['ConfirmRenew']; // Confirm Renew is needed and has been confirmed
$barcode = $confirmRenew ? '' : $_REQUEST['barcode'];

$GLOBALS['LIBMS']['PAGE'] = "RENEW";

$BookManager = new BookManager($libms);

$json['success'] = FALSE;
$json['script'] = null;
$json['html'] = null;

$forceRenewArray = array();
if ($confirmRenew) {
    $bookUniqueIDs = $_REQUEST['renew'];
    $ForceRenewBookUniqueID = $_REQUEST['ForceRenewBookUniqueID'];
    if (! empty($ForceRenewBookUniqueID)) {
        $forceRenewArray = explode(',', $ForceRenewBookUniqueID);
    }
} else {
    $bookUniqueID = $BookManager->getUniqueBookIDFormBarcode($barcode);
    $bookUniqueIDs[] = $bookUniqueID;
    if (empty($bookUniqueID)) {
        output($json);
    } else {
        $json['success'] = TRUE;
    }
}

$renew_table = '';
if ($Mode == "ByBatch") {
    foreach ((array) $bookUniqueIDs as $bookUniqueID) {
        // should find the userID from borrow record
        $sql = "SELECT UserID FROM `LIBMS_BORROW_LOG` WHERE `UniqueID`='{$bookUniqueID}' AND RecordStatus='BORROWED' ORDER BY `BorrowLogID` DESC LIMIT 1";
        $rows = $BookManager->libms->returnArray($sql);
        // global $UserID;
        $uID = $rows[0][0];
        if ($uID == "" || $uID <= 0) {
            $ToCheckBookStatus = true;
        }
        
        $is_open_loan = $libms->IsSystemOpenForLoan($uID);
        
        $sql = "SELECT Suspended FROM LIBMS_USER WHERE 	userid = '{$uID}' ";
        $rows = $BookManager->libms->returnArray($sql);
        $isSupended = $rows[0]['Suspended'];
        
        $User = new User($uID, $libms);
        $BookManager = new BookManager($libms);
        $RightRuleManager = $User->rightRuleManager;
        
        $userInfo = $User->userInfo;
        
        $UserNameDisplay = $userInfo[$Lang['libms']['SQL']['UserNameFeild']];
        
        $borrowCount = 0;
        $overdueCount = 0;
        $books = $User->borrowedBooks;
        
        // find the book that being requested
        if (! empty($books)) {
            foreach ($books as $book) {
                if ($book['UniqueID'] == $bookUniqueID) {
                    // if (($HasChosenRenewAttachment == 'false') && ($libms->get_system_setting("circulation_borrow_return_accompany_material")) && (!empty($book['AccompanyMaterial']))) {
                    // $json['script'] = gen_confirm_script($bookUniqieID,$barcode,$book['AccompanyMaterial']);
                    // output($json);
                    // }
                    // else {
                    // $borrowCount++;
                    $book['limit'] = $RightRuleManager->get_limits($BookManager->getCirculationTypeCodeFromUniqueBookID($bookUniqueID));
                    
                    // can this book renewed in normal case
                    $defaultCanRenew = $RightRuleManager->canRenew($book['UniqueID'], $book['MSG']);
                    // force canRenew To false if user is suspended
                    $book['canRenew'] = $isSupended ? false : $defaultCanRenew;
                    if (count($forceRenewArray) > 0) {
                        if (in_array($bookUniqueID, $forceRenewArray)) {
                            $book['canRenew'] = true; // force to renew
                        }
                    }
                    
                    // Handle Msg
                    // CanRenew in normal case && suspended
                    if (($defaultCanRenew && $isSupended)) {
                        $book['MSG'] = $Lang["libms"]["UserSuspend"]["Suspended"];
                    } elseif ((! $defaultCanRenew && $isSupended)) {
                        $book['MSG'] = $Lang["libms"]["UserSuspend"]["Suspended"] . '<br/>' . $book['MSG'];
                    }
                    $BookToRenew[] = $book;
                    // }
                    break;
                }
            }
        }
        
        $books = $BookToRenew; // just one book
        
        unset($BookToRenew);
        unset($book);
        
        if (! $confirmRenew) {
            
            if (sizeof($books) <= 0 || ! is_array($books)) {
                $json['script'] = "<script>alert(\"" . $Lang["libms"]["CirculationManagement"]["return_ajax"]["no_borrow_record"] . " - {$barcode}\");</script>";
                output($json);
            } elseif (! $is_open_loan) {
                $json['script'] = "<script>alert(\"" . strip_tags($libms->CloseMessage) . "\");</script>";
                output($json);
            } else {
                $renew_table = gen_renew_html_row($books, $bookUniqueID, $ID, $UserNameDisplay, ($needConfirm ? $confirmRenew : 1));
            }
        } else {
            
            $renew_table .= gen_renew_html_row($books, $bookUniqueID, $ID, $UserNameDisplay, $confirmRenew);
            $ID ++;
        }
    }
} else {
    // illegal access!
    output($json);
}

if ($confirmRenew && count($bookUniqueIDs) == $ID) {
    $json['success'] = TRUE;
}

// $json['html'] = gen_renew_html_row($books,$bookUniqueID,$ID,$UserNameDisplay,$confirmRenew);

$json['html'] = $renew_table;

output($json);

?>  
  