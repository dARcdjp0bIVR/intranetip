<?php


# modifying by: 


/*************************************************************
 * 
 *  20190614 (Henry)
 * 			- modified isOpenNow() to fix the bug when there is special close
 * 
 *  20190215 (Henry)
 * 			- modified findOpenDate() to increse the looping limit [Case#S156960]
 * 
 * 	20180510 (Henry)
 * 			- modified findOpenDate() to fix wrong due date if library is closed in patron_end_date/system_end_date [Case#S138814]
 * 
 * 	20171110 (Cameron)
 * 			- use start of today timestamp instead of current timestamp for $borrow_timestamp in findOpenDateSchoolDays() 
 * 			and $setToday in findOpenDate() to fix "wrong due date is return" if patron_end_date/system_end_date is today [ej DM #924]
 * 
 * 	20170915 (Henry)
 * 			- modified dayForPenalty() to check the flag $sys_custom['eLibraryPlus']['CountAllDaysForPenalty'] (case #C124852)
 * 
 * 	20170914 (Cameron)
 * 			- add parameter borrow_timestamp to findOpenDateSchoolDays() (case #R125534)
 * 			- return patron_end_date or system_end_date my be not less than borrow date  
 * 
 *  20170710 (Henry)
 * 			modified findOpenDateSchoolDays() and findOpenDate() to avoid due date in between special closing period [Case#L119874]
 * 
 *  20151112 (Henry)
 * 			modified findOpenDate() to add para "$setToday"
 * 
 *	20131022 (Yuen)
 *			modified findOpenDateSchoolDays() and findOpenDate() to avoid predicting return day which is invalid or before today!
 * 
 *	20131015 (Yuen)
 *			added findOpenDateSchoolDays() to determine the due date by counting school days only (holidays won't be counted)
 * 
 *  20130624 (Yuen)
 * 			bound return date by system end date
 *
 * ************************************************************/
 
 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
$WRT_ROOT= "{$_SERVER['DOCUMENT_ROOT']}/";
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($WRT_ROOT."includes/libdb.php");
include_once($WRT_ROOT."includes/pdump/pdump.php");

include_once($WRT_ROOT.'home/library_sys/management/circulation/User.php');


class TimeManager extends libdb{
	
	var $holiday;
	var $openTime;
	var $openTimeSpecial;
	var $system_dates;
	var $patron_dates;
	var $is_debug_mode;
	
	
	public function  __construct(){
		global $eclass_prefix;
		# database for this module
		$this->db = $eclass_prefix . "eClass_LIBMS";
		$this->system_dates = null;
	}
	
	
	public function getStartEndDate($date_type="open"){
		$date_type = trim(strtolower($date_type));
		if ($this->system_dates==null)
		{
			$sql = "SELECT * FROM `LIBMS_SYSTEM_SETTING` WHERE name='system_open_date' OR name='system_end_date' ";
			$result = $this->returnArray($sql);
			
			for ($i=0; $i<sizeof($result); $i++)
			{
				$this->system_dates[$result[$i]["name"]] = $result[$i]["value"];
			}
		}
		return $this->system_dates["system_".$date_type."_date"];
	}
	
	
	
	public function setDebugMode($debugMode=false){
		$this->is_debug_mode = $debugMode;
	}
	
	public function getPatronPeriod($date_type="start"){
		global $User;
		
		$date_type = trim(strtolower($date_type));

		if ($this->patron_dates==null && isset($User) && $User->userInfo["UserID"]>0)
		{
			# get the period control (if any) of related patron group
			 $sql = "SELECT if(p.StartDate='0000-00-00', '', p.StartDate) As startdate, if(p.EndDate='0000-00-00', '', p.EndDate) As enddate, p.GroupID					    
					 From
						LIBMS_GROUP_PERIOD AS p, LIBMS_GROUP_USER AS u
					 WHERE u.UserID='".$User->userInfo["UserID"]."' AND u.GroupID=p.GroupID ";
					 
					 
			//$this->patron_dates = $sql;
			$results = $this->returnResultSet($sql);
			$this->patron_dates = $results[0];
		}
		return $this->patron_dates[$date_type."date"];
	}
	
	
	
	
	
	
	# looping the no of days allow
	/*
	 *
	 * 	for next day
	 * 		if open, $DurationLeft--
	 * 		if close, recursively for next day
	 * 
	 * 		if $DurationLeft==0
	 * 			return that day
	 */
	 # counting school days only (on the other side, holidays are not counted)
	 # important note: this logic for the first $timestamp is different from findOpenDate(); i.e. strtotime("{$strToday} + {$limits['ReturnDuration']} days") is passed to findOpenDate()
	 public function findOpenDateSchoolDays($timestamp, $DurationLeft, $timestamp_previous_open_day = '', $borrow_timestamp='')
	 {
	 	if ($borrow_timestamp == '') {
	 		//$borrow_timestamp = time();
	 		$borrow_timestamp = strtotime(date('Y-m-d'));		// use start of today timestamp
	 	}
	 	
	 	if ($this->isOpen($timestamp))
	 	{
	 		# count for open day only!
	 		$DurationLeft--;
	 		
	 		$timestamp_previous_open_day = $timestamp;
	 		
	 		if ($this->isDeadClose($timestamp))
	 		{
				return $timestamp;
			} elseif ($DurationLeft<=-1) # refer to important note above
		 	{
		 		return $timestamp;
		 	}
	 	}
	 	
	 	# patron setting first
		# bounded by patron end date if set
		$patron_end_date = $this->getPatronPeriod("end");
		if ($patron_end_date<>"")
		{
			if (($timestamp>= strtotime($patron_end_date)) && ($borrow_timestamp<= strtotime($patron_end_date)))
			{
				# use last return date!!!
				return $timestamp_previous_open_day ? $timestamp_previous_open_day : strtotime($patron_end_date);
			}
		} else
		{
	 		 	
			# bounded by system end date if set
			$system_end_date = $this->getStartEndDate("end");
			if ($system_end_date<>"")
			{
				if (($timestamp>= strtotime($system_end_date)) && ($borrow_timestamp<= strtotime($system_end_date)))
				{
					# use last return date!!!
					return $timestamp_previous_open_day ? $timestamp_previous_open_day : strtotime($system_end_date);
				}
			}
		}

 		# next day
 		$timestamp_next = $timestamp + 24*60*60;
 		return $this->findOpenDateSchoolDays($timestamp_next, $DurationLeft, $timestamp_previous_open_day, $borrow_timestamp);
 		
	 }
	 		
	
	// give it a time it will get a closest time that library opens
	public function findOpenDate($timestamp,$count=0, $shiftby='+1 day', $setToday='', $isReachedDateLimit=false){

		//recursion bail out if 200 pass and yet to find a openning date.
		if ($count >500){
			return false;
		}
		//if the day past in is today or in the past
		if ($timestamp < ($setToday?$setToday:time())){
			return time();
		}

		if ($setToday == '') {
			$setToday = strtotime(date('Y-m-d'));	// use start of today timestamp
		}
		
		$date_limit = '';
		
		# patron setting first
		# bounded by patron end date if set
		$patron_end_date = $this->getPatronPeriod("end");
		if ($patron_end_date<>"")
		{
			$date_limit = strtotime($patron_end_date);
			
			if ((strtotime($patron_end_date) >= $setToday) && ($timestamp>= strtotime($patron_end_date)) && $this->isOpen(strtotime($patron_end_date)))
			{
				# use last return date!!!
				return strtotime($patron_end_date);
			}
		} else
		{
			# bounded by system end date if set
			$system_end_date = $this->getStartEndDate("end");
			
			if ($system_end_date<>"")
			{
				$date_limit = strtotime($system_end_date);
				
				if ((strtotime($system_end_date) >= $setToday) && ($timestamp> strtotime($system_end_date)) && $this->isOpen(strtotime($system_end_date)))
				{
					# use last return date!!!
					return strtotime($system_end_date);
				}
			}
		}
		
		// if library is not open shift a day.
		if (!$this->isOpen($timestamp)){
		    if($date_limit && $timestamp >= $date_limit && $date_limit > $setToday || $isReachedDateLimit){
		        return $this->findOpenDate(strtotime ('-1 day', $timestamp),$count+1,'-1 day', $setToday, true);
		    }
			return $this->findOpenDate(strtotime ($shiftby, $timestamp),$count+1,$shiftby, $setToday);
		}
		
		// if the day is within DEAD Closed period.  
		if ($this->isDeadClose($timestamp) || ($date_limit && $timestamp > $date_limit && $date_limit > $setToday)){
			return $this->findOpenDate(strtotime ('-1 day', $timestamp),$count+1,'-1 day', $setToday);
		}
		
		return $timestamp;
	}
	
	
	public function isHoliday($timestamp){
		$mysqldate = date( 'Y-m-d', $timestamp );
		$sql = "SELECT `DateFrom`,`DateEnd` FROM `LIBMS_HOLIDAY` WHERE '{$mysqldate}' BETWEEN `DateFrom` and `DateEnd`";
		$result = $this->returnArray($sql);
		
		if ($this->is_debug_mode && !empty($result[0][0]))
		{
			debug("<b>[Holiday <u>{$mysqldate}</u>!] </b><br />".$sql);
			#debug_r($result);
		}
		//tolog($result);
		return (!empty($result[0][0]));
	}
	
	public function isRegularTime($timestamp){
		//check regular openning day of week
		$str_dow = date( 'l', $timestamp );
		$sql = "SELECT count(*) FROM `LIBMS_OPEN_TIME` WHERE `Weekday` = '{$str_dow}' AND ( `Open` = 1 )";
		$result = $this->returnVector($sql);
		//tolog($result);
		if ($this->is_debug_mode && $result[0]!="" && $result[0]>0)
		{
			debug("<b>[Open Hour: Open on <u>{$str_dow}</u>!]</b><br />".$sql);
			debug_r($result);
		}
		return ($result[0]!="" && $result[0]>0);
	}
	
	public function isOpenSpecial($timestamp){
		$mysqldate = date( 'Y-m-d', $timestamp );
		$sql = "SELECT count(*) FROM `LIBMS_OPEN_TIME_SPECIAL` WHERE ('{$mysqldate}' BETWEEN `DateFrom` and `DateEnd`) AND (`Open` != 0 )";
		$result = $this->returnVector($sql);
		//tolog($result);
		if ($this->is_debug_mode && $result[0]!="" && $result[0]>0)
		{
			debug("<b>[Special Time: Open on <u>{$mysqldate}</u>!] </b><br />".$sql);
			debug_r($result);
		}
		return ($result[0]!="" && $result[0]>0);
	}
	public function isCloseSpecial($timestamp){
		$mysqldate = date( 'Y-m-d', $timestamp );
		$sql = "SELECT count(*) FROM `LIBMS_OPEN_TIME_SPECIAL` WHERE ('{$mysqldate}' BETWEEN `DateFrom` and `DateEnd`) AND (`Open` = 0 )";
		$result = $this->returnVector($sql);
		//tolog($result);
		if ($this->is_debug_mode && $result[0]!="" && $result[0]>0)
		{
			debug("<b>[Special Time: Close on <u>{$mysqldate}</u>!] </b><br />".$sql);
			#debug_r($result);
		}
		return ($result[0]!="" && $result[0]>0);
	}
	
	/**
	 * return if library open on given timestamp date.
	 * @param unknown_type $timestamp
	 */
	public function isOpen($timestamp){
		return ( ($this->isRegularTime($timestamp) && !$this->isHoliday($timestamp) && !$this->isCloseSpecial($timestamp) ) 
				|| $this->isOpenSpecial($timestamp)
				
			   ); 
	}
	
	/**
	 * return if library open on given timestamp.
	 * Time will be considered 
	 * @param unknown_type $timestamp
	 * @return boolean
	 */
	public function isOpenNow($timestamp = null){
		if (empty($timestamp))
			$timestamp = time();
		
		return ( ($this->isRegularTimeNow($timestamp)&& !$this->isHoliday($timestamp) && !$this->isCloseSpecial($timestamp)) || $this->isOpenSpecialNow($timestamp));
	}

	public function isRegularTimeNow($timestamp){
		//check regular openning day of week
		$str_dow = date( 'l', $timestamp );
		$str_time =date("H:i:s", $timestamp);
		$sql = "SELECT count(*) FROM `LIBMS_OPEN_TIME` WHERE `Weekday` = '{$str_dow}' AND ( '$str_time' BETWEEN `OpenTime` AND `CloseTime` ) AND`Open` = 1";
		$result = $this->returnVector($sql);
// 		dump ($result);
// 		echo ($sql);
// 		dump (mysql_error());
		return ($result[0]!="" && $result[0]>0);
	}
	
	public function isOpenSpecialNow($timestamp){
		$mysqldate = date( 'Y-m-d', $timestamp );
		$str_time =date("H:i:s", $timestamp);
		
		$sql = "SELECT count(*) FROM `LIBMS_OPEN_TIME_SPECIAL` WHERE ('{$mysqldate}' BETWEEN `DateFrom` and `DateEnd`) AND ( '$str_time' BETWEEN `OpenTime` AND `CloseTime` ) AND (`Open` = 1)";
		$result = $this->returnVector($sql);
// 		dump ($result);
// 		echo ($sql);
// 		dump (mysql_error());
		return ($result[0]!="" && $result[0]>0);
	}
	
	
	
	public function isDeadClose($timestamp){
		$mysqldate = date( 'Y-m-d', $timestamp );
		$str_time =date("H:i:s", $timestamp);
		
		$sql = "SELECT count(*) FROM `LIBMS_CLOSE_TIME` WHERE '{$mysqldate}' BETWEEN `DateFrom` and `DateEnd`";
		$result = $this->returnArray($sql);
		return ($result[0][0]);
	}
	
	
	/**
	 * dayForPenalty
	 * 
	 * $from  	int		timestamp
	 * $to 		int		timestamp
	 */
	public function dayForPenalty($from, $to ){
		global $sys_custom;
 		if (empty($from) || empty($to))
 			trigger_error("TimeManager::dayForPenalty : from or to empty", E_USER_ERROR);
		
 		//$diff= $to - $from ;
		//error_log("TM 161: from= $from  , to = $to , diff = $diff" );
		//if it less then 1 day, it is the same day.
		
 		$penaltyday = 0;
 		$curr = $from;
		while (($to - $curr) >= (60*60*24) && $to > $curr){
			if ( $sys_custom['eLibraryPlus']['CountAllDaysForPenalty'] || $this->isOpen($curr) ) // only count the day library open.
				$penaltyday++;
			$curr = $curr + (60*60*24);
		}
		
		return $penaltyday;
	}
}

?>