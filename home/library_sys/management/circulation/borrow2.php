<?php
# using: 

############ Change Log Start ###############
#
#	Date:	2012-06-25 prototyped
#			
#
############ Change Log End ###############


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();


$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "CirculationBorrow";

$libms->MODULE_AUTHENTICATION($CurrentPage);

$linterface = new interface_html("libms_circulation2.html");

############################################################################################################


$MODULE_OBJ = $libms->GET_MODULE_OBJ_ARR_ADMIN();
$linterface->LAYOUT_START();

?>

<script language="Javascript">

function changeLayer(LayerName)
{
	
	
	$('#list_borrow').removeClass("selected");
	$('#list_return').removeClass("selected");
	$('#list_renew').removeClass("selected");
	$('#list_reserve').removeClass("selected");
	$('#list_payment').removeClass("selected");
	$('#list_'+LayerName).addClass("selected");
	
	
	$('#layer_borrow').hide();
	$('#layer_return').hide();
	$('#layer_renew').hide();
	$('#layer_reserve').hide();
	$('#layer_payment').hide();
	$('#layer_'+LayerName).show(200);
	
	return;
}

</script>

<form name="formLib" action="borrow.php" method="post">
				
				<table width="94%" border="0" cellspacing="0" cellpadding="5" bgcolor="#EBEBEB">
				<tr>
					<td bgcolor="#CCCCCC" colspan="3">
						<table width="50%" border="0" cellpadding="5" cellspacing="0">
						<tr>
						   <td nowrap='nowrap'><?=$Lang['libms']['CirculationMgmt']['current_user_code']?></td>
							<td width="80%" >
							  <input type="text" class="textboxtext" name="UserCodeNumber" value="<?=$UserCodeNumber?>" maxlength="128" />
							</td>
							<td>
							<?= $linterface->GET_SMALL_BTN($Lang['libms']['general']['change'], "submit", "alert('change to another user')")?>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table width="50" border="0" cellspacing="0" cellpadding="3">
						<tr>
						<td><div id="div7"><span><img src="/file/photo/personal/p1.jpg" alt="" width="100" height="130" border="0"></span></div></td>
						</tr>
						</table>						
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr>
						<td align="center"><span class="tabletext">Chan Ting Shing <br />(2A-05)</span></td>
						</tr>
						</table>
					</td>
					<td width="20%">					
						<table width="100%" border="0" cellspacing="5" cellpadding="3">
						<tr>
							<td nowrap="nowrap"><span class="tabletext"><?=$Lang['libms']['CirculationMgmt']['current_borrow']?> :&nbsp;</span></td>
							<td width="90%">6</td>
						</tr>
						<tr>
							<td nowrap="nowrap"><span class="tabletext"><?=$Lang['libms']['CirculationMgmt']['overdue_total']?> :&nbsp;</span></td>
							<td width="90%"><font color="red">1</font></td>
						</tr>
						<tr>
							<td nowrap="nowrap"><span class="tabletext"><?=$Lang['libms']['CirculationMgmt']['current_reservation']?> :&nbsp;</span></td>
							<td width="90%"><font color="#559955">1</font></td>
						</tr>
						<tr>
							<td nowrap="nowrap"><span class="tabletext"><?=$Lang['libms']['CirculationMgmt']['owe_balance']?> :&nbsp;</span></td>
							<td width="90%"><font color="red">$6.5</font></td>
						</tr>
						<tr>
							<td nowrap="nowrap"><span class="tabletext"><?=$Lang['libms']['CirculationMgmt']['borrow_quota_set']?> :&nbsp;</span></td>
							<td width="90%">10</td>
						</tr>
						<tr>
							<td nowrap="nowrap"><span class="tabletext"><?=$Lang['libms']['CirculationMgmt']['borrow_balance']?> :&nbsp;</span></td>
							<td width="90%">4</td>
						</tr>
						</table>
					</td>
					<td width="65%" style="border:solid 3px #EBEBEB;" bgcolor="#F5F5F5">
						<div style="width: 100%; height: 160px; overflow-y: scroll;">
						<table class="common_table_list">
							<thead>
							<tr>
								<th><?=$Lang['libms']['book']['title']?></th>
								<th nowrap='nowrap'><?=$Lang['libms']['CirculationMgmt']['borrow_reserve_date']?></th>
							  <th><?=$Lang['libms']['CirculationMgmt']['due_date']?></th>
							  <th><?=$Lang['libms']['CirculationMgmt']['borrow_status_title']?></th>
							</tr>
							</thead>
							<tbody>
							<tr class="row_suspend">
								<td><a href="">Shadow of Night: A Novel</a></td>
								<td nowrap="nowrap">2012-06-20</td>
								<td nowrap="nowrap"><font color='red'>2012-06-25</font></td>
								<td nowrap="nowrap"><font color='red'><?=$Lang['libms']['CirculationMgmt']['borrow_status']['overdue']?></font></td>
							</tr>
							<tr class="row_avaliable">
                              <td><a href="">The Official SAT Study Guide, 2nd edition</a></td>
							  <td nowrap="nowrap">2012-06-22</td>
							  <td nowrap="nowrap">--</td>
							  <td nowrap="nowrap"><font color='#559955'><?=$Lang['libms']['CirculationMgmt']['borrow_status']['reserve_got']?></font></td>
							</tr>
							<tr>
								<td><a href="">Fifty Shades of Grey: Book One of the Fifty Shades Trilogy</a></td>
								<td nowrap="nowrap">2012-06-20</td>
								<td nowrap="nowrap">2012-06-30</td>
								<td nowrap="nowrap"><?=$Lang['libms']['CirculationMgmt']['borrow_status']['normal']?></td>
							</tr>
							<tr>
								<td><a href="">An American Son: A Memoir</a></td>
								<td nowrap="nowrap">2012-06-22</td>
								<td nowrap="nowrap">2012-07-03</td>
								<td nowrap="nowrap"><?=$Lang['libms']['CirculationMgmt']['borrow_status']['normal']?></td>
							</tr>
							<tr>
								<td><a href="">THE ART OF FIELDING</a></td>
								<td nowrap="nowrap">2012-06-23</td>
								<td nowrap="nowrap">2012-07-03</td>
								<td nowrap="nowrap"><?=$Lang['libms']['CirculationMgmt']['borrow_status']['normal']?></td>
							</tr>
							<tr>
								<td><a href="">Outliers: The Story of Success</a></td>
								<td nowrap="nowrap">2012-06-24</td>
								<td nowrap="nowrap">2012-07-04</td>
								<td nowrap="nowrap"><?=$Lang['libms']['CirculationMgmt']['borrow_status']['normal']?></td>
							</tr>
							<tr>
								<td><a href="">The Mark of Athena (The Heroes of Olympus, Book Three)</a></td>
								<td nowrap="nowrap">2012-06-25</td>
								<td nowrap="nowrap">2012-07-05</td>
								<td nowrap="nowrap"><?=$Lang['libms']['CirculationMgmt']['borrow_status']['normal']?></td>
							</tr>
							</tbody>
						</table>
					</div>
					</td>
				</tr>
				</table>
				
				<br /><br />
				<table width="95%" border="0" >
				<tr><td>
					<div class="table_board">
					
						 <div class="shadetabs">
		                         <ul>
		                           <li class="selected" id="list_borrow"><a href="javascript:changeLayer('borrow')"><?=$Lang['libms']['circulation_borrow']?></a></li>
		                            <li id="list_return"><a href="javascript:changeLayer('return')"><?=$Lang['libms']['circulation_return']?></a></li>
		                           <li id="list_renew"><a href="javascript:changeLayer('renew')"><?=$Lang['libms']['circulation_renew']?></a></li>
		                           <li id="list_reserve"><a href="javascript:changeLayer('reserve')"><?=$Lang['libms']['circulation_reserve']?></a></li>
		                           <li id="list_payment"><a href="javascript:changeLayer('payment')"><?=$Lang['libms']['circulation_payment']?></a></li>
		                         </ul>
		                  </div>
			                  
		                  
		                  <div id="layer_borrow">
                      
		                  
		                      <table width="100%" border="0" cellpadding="5" cellspacing="5" >
								<tr>
									<td width="50%"><span class="sectiontitle_v30"><?=$Lang['libms']['CirculationMgmt']['input_borrow_books']?></span></td>
								   <td nowrap='nowrap'><?=$Lang['libms']['CirculationMgmt']['input_book_code']?></td>
									<td width="50%" >
									  <input type="text" class="textboxtext" name="BookCode" value="<?=$BookCode?>" maxlength="128" />
									</td>
									<td>
									<?= $linterface->GET_SMALL_BTN($Lang['libms']['general']['add_book'], "submit", "alert('add this book to below table')")?>
									</td>
								</tr>
								</table>
		                      <table class="common_table_list edit_table_list_v30 ">
											<thead>
											<tr>
												<th width='2%' class="num_check">#</th>
												<th width='35%'><?=$Lang['libms']['book']['title']?></th>
												<th width='15%'><?=$Lang['libms']['CirculationMgmt']['borrow_status_title']?></th>
											  <th width='15%'><?=$Lang['libms']['CirculationMgmt']['borrow_date']?></th>
											  <th width='15%'><?=$Lang['libms']['CirculationMgmt']['due_date']?></th>
											  <th width='15%'><?=$Lang['libms']['settings']['book_location']?></th>
											  
											  <th nowrap='nowrap'><?=$Lang['libms']['circulation_borrow']?></th>
											  </tr>
											</thead>
											<tbody>
											<tr>
												<td>1</td>
												<td>The Lord of the Rings</td>
												<td><?=$Lang['libms']['book']['staus']["NORMAL"]?></td>
												<td>2012-06-25</td>
												<td>2012-07-05(Thu)</td>
												<td>bookshelf 1B</td>
												<td><div class="table_row_tool"><a href="#" class="delete_dim" title="<?=$Lang['libms']['general']['del']?>"></a></div></td>
											 </tr>
											<tr>
												<td>2</td>
												<td>A Tale of Two Cities</td>
												<td><?=$Lang['libms']['book']['staus']["REPAIR"]?></td>
												<td>--</td>
												<td>--</td>
												<td>bookshelf 1B</td>
												<td><font color="red"><?=$Lang['libms']['CirculationMgmt']['borrow_status']['na']?></font></td>
											 </tr>
											<tr>
												<td>3</td>
												<td>Harry Potter And The Prisoner Of Azkaban</td>
												<td><?=$Lang['libms']['book']['staus']["BROKEN"]?></td>
												<td>2012-06-25</td>
												<td>2012-07-05(Thu)</td>
												<td>bookshelf 1D</td>
												<td><div class="table_row_tool"><a href="#" class="delete_dim" title="<?=$Lang['libms']['general']['del']?>"></a></div></td>
											 </tr>
											<tr>
												<td>4</td>
												<td>We Don't Live Here Anymore</td>
												<td><?=$Lang['libms']['book']['staus']["NORMAL"]?></td>
												<td>2012-06-25</td>
												<td>2012-07-05(Thu)</td>
												<td>bookshelf 1D</td>
												<td><font color="red"><?=$Lang['libms']['CirculationMgmt']['borrow_status']['reserved_by_someone']?></font>
													<div class="table_row_tool"><a href="#" class="add_dim" title="<?=$Lang['libms']['general']['force_borrow']?>"></a></div>
												</td>
											 </tr>
											<tr class="edit_table_head_bulk">
						                          <th colspan="7">
							                          <div align="center">
								                          <font size="2" face="arial"><?=$Lang['libms']['CirculationMgmt']['to_borrow_total']?> : <font size="3">2</font></font>
								                          <p>
									                      <?= $linterface->GET_ACTION_BTN($Lang['libms']['general']['submit_borrow'], "submit")?>
									                      <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "alert('are you sure you want to cancel now?')")?>
									                      </p>
								                      </div>
						                          </th>
					                        </tr>
											</tbody>
								</table>
							</div>
							
							
							
							
		                  <div id="layer_return" style="display:none">
			                  
		                      <table width="100%" border="0" cellpadding="5" cellspacing="5" >
								<tr>
									<td width="50%"><span class="sectiontitle_v30"><?=$Lang['libms']['CirculationMgmt']['input_return_books']?></span></td>
								   <td nowrap='nowrap'><?=$Lang['libms']['CirculationMgmt']['input_book_code']?></td>
									<td width="50%" >
									  <input type="text" class="textboxtext" name="BookCode" value="<?=$BookCode?>" maxlength="128" />
									</td>
									<td>
									<?= $linterface->GET_SMALL_BTN($Lang['libms']['general']['add_book'], "submit", "alert('add this book to below table')")?>
									</td>
								</tr>
								</table>
		                      <table class="common_table_list edit_table_list_v30 ">
											<thead>
											<tr>
												<th width='2%' class="num_check">#</th>
												<th width='35%'><?=$Lang['libms']['book']['title']?></th>
											  <th width='12%'><?=$Lang['libms']['CirculationMgmt']['borrow_date']?></th>
											  <th width='12%'><?=$Lang['libms']['CirculationMgmt']['due_date']?></th>
												<th width='10%'><?=$Lang['libms']['CirculationMgmt']['late_total']?></th>
												<th width='10%'><?=$Lang['libms']['CirculationMgmt']['overdue_payment']?></th>
											  <th width='11%'><?=$Lang['libms']['settings']['book_location']?></th>
											  <th width='11%'><?=$Lang['libms']['CirculationMgmt']['reserved_by_other']?></th>
											  
											  <th nowrap='nowrap'><?=$Lang['libms']['circulation_return']?></th>
											  </tr>
											</thead>
											<tbody>
											<tr>
												<td>1</td>
												<td>Shadow of Night: A Novel</td>
												<td>2012-06-20</td>
												<td><font color="red">2012-06-25(Mon)</font></td>
												<td><font color="red">5</font></td>
												<td><font color="red">$2.5</font></td>
												<td>bookshelf 2C</td>
												<td><?=$Lang['libms']['CirculationMgmt']['reserved_yes']?></td>
												<td><div class="table_row_tool"><a href="#" class="delete_dim" title="<?=$Lang['libms']['general']['del']?>"></a></div></td>
											 </tr>
											<tr>
												<td>2</td>
												<td>Fifty Shades of Grey: Book One of the Fifty Shades Trilogy</td>
												<td>2012-06-20</td>
												<td>2012-06-30(Sat)</td>
												<td>--</td>
												<td>--</td>
												<td>bookshelf 5</td>
												<td><?=$Lang['libms']['CirculationMgmt']['reserved_no']?></td>
												<td><div class="table_row_tool"><a href="#" class="delete_dim" title="<?=$Lang['libms']['general']['del']?>"></a></div></td>
											 </tr>
											<tr class="edit_table_head_bulk">
						                          <th colspan="9">
							                          <div align="center">
								                          <font size="2" face="arial"><?=$Lang['libms']['CirculationMgmt']['to_return_total']?> : <font size="3">2</font></font>
								                          <p>
									                      <?= $linterface->GET_ACTION_BTN($Lang['libms']['general']['submit_return'], "submit")?>
									                      <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "alert('are you sure you want to cancel now?')")?>
									                      </p>
								                      </div>
						                          </th>
					                        </tr>
											</tbody>
								</table>
							</div>
							
							
							
							
		                  <div id="layer_renew" style="display:none">
			                  
		                      <table width="100%" border="0" cellpadding="5" cellspacing="5" >
								<tr>
									<td width="50%"><span class="sectiontitle_v30"><?=$Lang['libms']['CirculationMgmt']['input_renew_books']?></span></td>
								   <td nowrap='nowrap'><?=$Lang['libms']['CirculationMgmt']['input_book_code']?></td>
									<td width="50%" >
									  <input type="text" class="textboxtext" name="BookCode" value="<?=$BookCode?>" maxlength="128" />
									</td>
									<td>
									<?= $linterface->GET_SMALL_BTN($Lang['libms']['general']['add_book'], "submit", "alert('add this book to below table')")?>
									</td>
								</tr>
								</table>
		                      <table class="common_table_list edit_table_list_v30 ">
											<thead>
											<tr>
												<th width='2%' class="num_check">#</th>
												<th width='35%'><?=$Lang['libms']['book']['title']?></th>
											  <th width='13%'><?=$Lang['libms']['CirculationMgmt']['borrow_date']?></th>
											  <th width='13%'><?=$Lang['libms']['CirculationMgmt']['due_date']?></th>
											  <th width='10%'><?=$Lang['libms']['CirculationMgmt']['renew_balance']?></th>
											  <th nowrap='nowrap'><?=$Lang['libms']['circulation_renew']?></th>
												<th width='13%'><?=$Lang['libms']['CirculationMgmt']['renew_date']?></th>
												<th width='13%'><?=$Lang['libms']['CirculationMgmt']['due_date']?></th>
											  
											  </tr>
											</thead>
											<tbody>
											<tr>
												<td>1</td>
												<td>An American Son: A Memoir</td>
												<td>2012-06-20</td>
												<td><font color="red">2012-06-25(Mon)</font><br /><font color="red"><?=$Lang['libms']['CirculationMgmt']['borrow_status']['overdue']?></font></td>
												<td>--</td>
												<td>
													<input type="checkbox"  />
												</td>
												<td>--</td>
												<td>--</td>
											 </tr>
											<tr>
												<td>2</td>
												<td>THE ART OF FIELDING</td>
												<td>2012-06-20</td>
												<td>2012-06-30(Sat)</td>
												<td>2</td>
												<td><input type="checkbox"  /></td>
												<td>2012-06-29</td>
												<td>2012-07-09(Mon)</td>
											 </tr>
											<tr>
												<td>3</td>
												<td>Fifty Shades of Grey: Book One of the Fifty Shades Trilogy</td>
												<td>2012-06-20</td>
												<td>2012-06-30(Sat)</td>
												<td><font color="red">0</font><br /><font color="red"><?=$Lang['libms']['CirculationMgmt']['borrow_status']['renew_used']?></font></td>
												<td>
													<input type="checkbox"  />
												</td>
												<td>--</td>
												<td>--</td>
											 </tr>
											<tr class="edit_table_head_bulk">
						                          <th colspan="8">
							                          <div align="center">
								                          <font size="2" face="arial"><?=$Lang['libms']['CirculationMgmt']['to_renew_total']?> : <font size="3">1</font></font>
								                          <p>
									                      <?= $linterface->GET_ACTION_BTN($Lang['libms']['general']['submit_renew'], "submit")?>
									                      <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "alert('are you sure you want to cancel now?')")?>
									                      </p>
								                      </div>
						                          </th>
					                        </tr>
											</tbody>
								</table>
							</div>
							
							
							<div id="layer_reserve" style="display:none">
                      
		                  
		                      <table width="100%" border="0" cellpadding="5" cellspacing="5" >
								<tr>
									<td width="50%"><span class="sectiontitle_v30"><?=$Lang['libms']['CirculationMgmt']['input_reserve_books']?></span></td>
<?php
$quota_reached = true;  # prototype
if ($quota_reached)
{
?>
									<td width="50%" align="right"><font color="red"><?=$Lang['libms']['CirculationMgmt']['borrow_status']['reserve_quota_reached']?></font></td>
	
<?php } else { ?>
								   <td nowrap='nowrap'><?=$Lang['libms']['CirculationMgmt']['input_book_code']?></td>
									<td width="50%" >
									  <input type="text" class="textboxtext" name="BookCode" value="<?=$BookCode?>" maxlength="128" />
									</td>
									<td>
									<?= $linterface->GET_SMALL_BTN($Lang['libms']['general']['add_book'], "submit", "alert('add this book to below table')")?>
									</td>
<?php } ?>
								</tr>
								</table>
		                      <table class="common_table_list edit_table_list_v30 ">
											<thead>
											<tr>
												<th width='2%' class="num_check">#</th>
												<th width='35%'><?=$Lang['libms']['book']['title']?></th>
												<th width='15%'><?=$Lang['libms']['CirculationMgmt']['borrow_status_title']?></th>
											  <th width='15%'><?=$Lang['libms']['CirculationMgmt']['due_date']?></th>
											  <th width='15%'><?=$Lang['libms']['CirculationMgmt']['reserved_by_total']?></th>
											  <th width='15%'><?=$Lang['libms']['settings']['book_location']?></th>
											  
											  <th nowrap='nowrap'><?=$Lang['libms']['circulation_reserve']?></th>
											  </tr>
											</thead>
											<tbody>
											<tr>
												<td>1</td>
												<td>The Lord of the Rings</td>
												<td><?=$Lang['libms']['book']['staus']["NORMAL"]?></td>
												<td>2012-07-05(Thu)</td>
												<td>0</td>
												<td>bookshelf 1B</td>
												<td><div class="table_row_tool"><a href="#" class="delete_dim" title="<?=$Lang['libms']['general']['del']?>"></a></div></td>
											 </tr>
											<tr>
												<td>2</td>
												<td>A Tale of Two Cities</td>
												<td><?=$Lang['libms']['book']['staus']["REPAIR"]?></td>
												<td>--</td>
												<td>--</td>
												<td>bookshelf 1B</td>
												<td><font color="red"><?=$Lang['libms']['CirculationMgmt']['borrow_status']['na']?></font></td>
											 </tr>
											<tr>
												<td>3</td>
												<td>We Don't Live Here Anymore</td>
												<td><?=$Lang['libms']['book']['staus']["NORMAL"]?></td>
												<td>2012-07-05(Thu)</td>
												<td>1</td>
												<td>bookshelf 1D</td>
												<td><div class="table_row_tool"><a href="#" class="delete_dim" title="<?=$Lang['libms']['general']['del']?>"></a></div></td>
											 </tr>
											<tr class="edit_table_head_bulk">
						                          <th colspan="7">
							                          <div align="center">
								                          <font size="2" face="arial"><?=$Lang['libms']['CirculationMgmt']['to_reserve_total']?> : <font size="3">2</font></font>
								                          <p>
									                      <?= $linterface->GET_ACTION_BTN($Lang['libms']['general']['submit_reserve'], "submit")?>
									                      <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "alert('are you sure you want to cancel now?')")?>
									                      </p>
								                      </div>
						                          </th>
					                        </tr>
											</tbody>
								</table>
							</div>
							
							
							
		                  <div id="layer_payment" style="display:none">
			                  
		                      <table width="100%" border="0" cellpadding="5" cellspacing="5" >
								<tr>
									<td width="50%"><span class="sectiontitle_v30"><?=$Lang['libms']['CirculationMgmt']['input_overdue_payment']?></span></td>
								</tr>
								</table>
		                      <table class="common_table_list edit_table_list_v30 ">
											<thead>
											<tr>
												<th width='2%' class="num_check">#</th>
												<th width='40%'><?=$Lang['libms']['book']['title']?></th>
												<th width='10%'><?=$Lang['libms']['CirculationMgmt']['late_total']?></th>
												<th width='10%'><?=$Lang['libms']['CirculationMgmt']['overdue_payment']?></th>
												<th width='10%'><?=$Lang['libms']['CirculationMgmt']['overdue_payment_settled']?></th>
											  <th width='10%'><?=$Lang['libms']['CirculationMgmt']['overdue_waive']?></th>
											  <th width='10%'><?=$Lang['libms']['CirculationMgmt']['overdue_payment_now']?></th>											  
											  <th width='10%'><?=$Lang['libms']['CirculationMgmt']['overdue_payment_owe']?></th>
											  </tr>
											</thead>
											<tbody>
											<tr>
												<td>1</td>
												<td>The Coin Counting Book</td>
												<td>5</td>
												<td>$2.5</td>
												<td>$0.0</td>
												<td><input type="checkbox" name="WaiveRecord[]" id="WaiveRecord[]" /></td>
												<td>$<input type="text" size="4" value="2.0" /></td>
												<td><font color="red">$0.5</font></td>
											 </tr>
											<tr>
												<td>2</td>
												<td>One Cent, Two Cents, Old Cent, New Cent: All About Money</td>
												<td>6</td>
												<td>$3.0</td>
												<td>$1.0</td>
												<td><input type="checkbox" name="WaiveRecord[]" id="WaiveRecord[]" /></td>
												<td>$<input type="text" size="4" value="2.0" /></td>
												<td>$0.0</td>
											 </tr>
											<tr>
												<td>3</td>
												<td>Jurassic Poop: What Dinosaurs (and Others) Left Behind</td>
												<td>2</td>
												<td>$1.0</td>
												<td>$0.0</td>
												<td><input type="checkbox" name="WaiveRecord[]" id="WaiveRecord[]" checked="checked" /></td>
												<td>--</td>
												<td>--</td>
											 </tr>
											<tr>
												<td colspan="6" align="right"><?=$Lang['libms']['CirculationMgmt']['overdue_payment_receive']?></td>
												<td colspan="2"><font color="red">$4.0</font></td>
											 </tr>
											<tr class="edit_table_head_bulk">
						                          <th colspan="8">
							                          <div align="center">
								                          <p>
									                      <?= $linterface->GET_ACTION_BTN($Lang['libms']['general']['submit_payment'], "submit")?>
									                      <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "alert('are you sure you want to cancel now?')")?>
									                      </p>
								                      </div>
						                          </th>
					                        </tr>
											</tbody>
								</table>
							</div>
							
							
							
							
					</div>
				</td></tr>
				</table>
				<br /><br /><br />
</form>



<?php


$linterface->LAYOUT_STOP();
intranet_closedb();
?>