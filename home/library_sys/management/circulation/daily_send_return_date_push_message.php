<?php
/*
 * This file should be run daily for sending the reminder mail to reader when the due date is coming soon
 * Created by Henry
 *
 * 2020-05-28 Cameron
 * - move getParentStudentAssoAry() from this file to daily_send_common_function.php and include it on top
 *
 * 2020-05-27 Cameron
 * - call getNotificationSettings() from libms to avoid including daily_update_reserved_status.php
 * - update NotifyMessageTargetID in borrow log so that it can be used to check if the book has been returned or not before the push message is to be sent out [case #F175877]
 */
$WRT_ROOT= "{$_SERVER['DOCUMENT_ROOT']}/";
include_once($WRT_ROOT."includes/global.php");
include_once($WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($WRT_ROOT."includes/libdb.php");
include_once($WRT_ROOT."home/library_sys/management/circulation/TimeManager.php");
include_once($WRT_ROOT."includes/libuser.php");
include_once($WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($WRT_ROOT."home/library_sys/management/circulation/daily_send_common_function.php");

intranet_opendb();
$libms = new liblms();
$luser = new libuser();
$libeClassApp = new libeClassApp();
echo "<br/>This file should be run daily for sending the reminder email(s) to reader when the due date is coming soon<br/><br/>";
echo "Program started!<br/>";

$remind_date = $libms->get_system_setting('push_message_due_date_reminder');


$notifcation_settings	= array(
    'push_message_due_date' => $libms->getNotificationSettings('push_message_due_date')
	);

$sendPushMessage = false;
if (!$notifcation_settings['push_message_due_date']) {
	echo  "The setting is not allow to send Due Date Reminder (push messages)!<br/>";
}
else {
	$sql = "SELECT id FROM LIBMS_DAILY_SCRIPT WHERE DATE(DateCreated) >= DATE(now()) AND name = 'push_message_due_date' limit 1";
	$result = $libms->returnArray($sql);
	
	if ($result[0]['id']) {
		echo "Due Date Reminder(s) (push messages) is/are already sent!<br/>";
	}
	else {
		$sendPushMessage = true;
	}
}
$messageTitle = $Lang['libms']['daily']['push_message']['return_date']['title'];


if($sendPushMessage){
	$sql2 = "select distinct UserID from LIBMS_BORROW_LOG WHERE RecordStatus = 'BORROWED' AND DueDate = DATE_FORMAT(NOW() + INTERVAL ".$remind_date." DAY,'%Y-%m-%d')";
	$ReaderIDs = $libms->returnArray($sql2);
	
	$result2 = $libms->SELECTFROMTABLE('LIBMS_SYSTEM_SETTING', '*', array('name'=>'"push_message_time"'),'',1); // for Day(s) of the Email Reminder before Due Date (0 means not remind)
	$timeOfPushMessage = $result2[0]['value'];
	
	$individualMessageInfoAry = array();
	
	for($i=0; $i < sizeof($ReaderIDs); $i++){
		$ReaderID = $ReaderIDs[$i]['UserID'];
	
		$sql = "SELECT lb.BookTitle, lb.ResponsibilityBy1, lb.ResponsibilityBy2, lb.ResponsibilityBy3, lb.CallNum, lb.CallNum2, lbu.ACNO, lbl.DueDate, lbl.BorrowLogID FROM LIBMS_BORROW_LOG as lbl JOIN LIBMS_BOOK_UNIQUE as lbu ON lbl.UniqueID = lbu.UniqueID JOIN LIBMS_BOOK as lb ON lbl.BookID = lb.BookID WHERE lbl.RecordStatus = 'BORROWED' AND DueDate = DATE_FORMAT(NOW() + INTERVAL ".$remind_date." DAY,'%Y-%m-%d') AND UserID = '".$ReaderID."'";
		$result = $libms->returnArray($sql);
		
		$bookTitleArr = array();
		$borrowLogIDArr = array();
		for($j=0; $j < sizeof($result); $j++){
			$bookCount++;
			$bookTitleArr[$j] = $result[$j]['BookTitle'];
			$dueDateArr[$j] = $result[$j]['DueDate'];
			$borrowLogIDArr[$j] = $result[$j]['BorrowLogID'];
		}
		//sendOverPushMessage($ReaderID,$timeOfPushMessage,$bookTitleArr,$dueDateArr);
		$parentStudentAssoAry = getParentStudentAssoAry($ReaderID);
		
		if(empty($parentStudentAssoAry)){
			continue;
		}
		$messageArray['relatedUserIdAssoAry'] = $parentStudentAssoAry;
		
		$messageContent = getPushMessageContent2($bookTitleArr,$dueDateArr);
		$messageArray['messageTitle'] = $messageTitle;
		$messageArray['messageContent'] = $messageContent;
		$messageArray['borrowLogIDArr'] = $borrowLogIDArr;
		$individualMessageInfoAry[] = $messageArray;
	}
	$forceUseClass = true;
	if(!empty($individualMessageInfoAry)){
		sendMultiplePushMessage2($individualMessageInfoAry,$timeOfPushMessage);
	}
	$forceUseClass = false;
	$sql = "INSERT INTO LIBMS_DAILY_SCRIPT (name) Values ('push_message_due_date') ";
	$result = $libms->db_db_query($sql);
		
	
	echo "The push message is sent to ".sizeof($ReaderIDs)." reader(s)!<br/>";
	echo "Program ended!<br/>";
}
// The function is in daily_update_reserved_status.php
//function getNotificationSettings($type){
//	global $libms;
//	$sql = "SELECT enable FROM LIBMS_NOTIFY_SETTING
//		WHERE name = '$type'";
//	
//	return current($libms->returnVector($sql));
//	
//}
function sendMultiplePushMessage2($individualMessageInfoAry,$timeOfPushMessage){
    global $Lang, $libms;
	$isPublic = "";
	$sendTimeMode = 'scheduled';
	$sendTimeString = date('Y-m-d').' '.$timeOfPushMessage.':00';
	$appType = 'P';
	$messageTitle = $Lang['libms']['daily']['push_message']['return_date']['title'];
	$messageContent = 'MULTIPLE MESSAGES';
	
	//Compare timeString with time now
	if((time() - strtotime($sendTimeString))>0){
		echo "Time set to push message is later than the time right now, Program End.";
	}else{
		$libeClassApp = new libeClassApp();
		$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eLibPlus_due_day_reminder', date('Ymd'));
		
		#####UPDATE INPUT DATE
		$sql = "UPDATE INTRANET_APP_NOTIFY_MESSAGE  Set DateInput  = '$sendTimeString'  Where NotifyMessageID = '$notifyMessageId'";
		$libdb = new libdb();
		$libdb->db_db_query($sql);

		// update NotifyMessageTargetID in borrow log so that it can be used to check if the book has been returned or not before the push message is to be sent out
		foreach((array)$individualMessageInfoAry as $_individualMessageInfoAry) {
		    $borrowLogIDArr = $_individualMessageInfoAry['borrowLogIDArr'];
		    $relatedUserIdAssoAry = $_individualMessageInfoAry['relatedUserIdAssoAry'];
		    $parentIDAry = array_keys($relatedUserIdAssoAry);
		    if (count($parentIDAry)) {
		        $sql = "SELECT NotifyMessageTargetID FROM INTRANET_APP_NOTIFY_MESSAGE_TARGET WHERE NotifyMessageID='".$notifyMessageId."' AND TargetID='".$parentIDAry[0]."'";
		        $rs = $libdb->returnResultSet($sql);

		        if (count($rs)) {
		            $notifyMessageTargetID = $rs[0]['NotifyMessageTargetID'];
		            $libms->updateNotifyMessageTargetID($borrowLogIDArr, $notifyMessageTargetID);
		        }
		    }

		}

		//$forceUseClass = false;
	}
}

function getPushMessageContent2($bookTitleArr,$dueDateArr){
	global $Lang;
	$messageContent = $Lang['libms']['daily']['push_message']['return_date']['content'].'
			';
	
	$countOfBooks = count($bookTitleArr);
	for($i=0;$i<$countOfBooks;$i++){
		$messageContent .= '
				'.($i+1).'. '.$bookTitleArr[$i].' ('.$dueDateArr[$i].')';
	}
	return $messageContent;
}
//function sendOverPushMessage($user_id,$timeOfPushMessage,$bookTitleArr,$dueDateArr){
//	global $intranet_root,$forceUseClass,$WRT_ROOT,$PATH_WRT_ROOT,$intranet_default_lang;
//	include_once($intranet_root."/lang/libms.lang.$intranet_default_lang.php");
//	global $Lang,$eclassAppConfig;
//	$PATH_WRT_ROOT=$WRT_ROOT;
//	$forceUseClass = true;
//	$messageTitle = $Lang['libms']['daily']['push_message']['return_date']['title'];
//	$messageContent = $Lang['libms']['daily']['push_message']['return_date']['content'].'
//			';
//	
//	$countOfBooks = count($bookTitleArr);
//	for($i=0;$i<$countOfBooks;$i++){
//		$messageContent .= '
//				'.($i+1).'. '.$bookTitleArr[$i].' ('.$dueDateArr[$i].')';
//	}
//	
//	include_once($intranet_root."/includes/libuser.php");
//	include_once($intranet_root."/includes/eClassApp/libeClassApp.php");
//	
//	$luser = new libuser();
//	$libeClassApp = new libeClassApp();
//	$studentsWithParentUsingParentApp = $luser->getStudentWithParentUsingParentApp();
//	$userIdArr = array_intersect((array)$user_id, (array)$studentsWithParentUsingParentApp);
//	
//	$isPublic = "N";
//	$sendTimeMode = $eclassAppConfig['pushMessageSendMode']['scheduled'];
//	$sendTimeString = date('Y-m-d').' '.$timeOfPushMessage.':00';
//	$appType = 'P';
//	
//	//Compare timeString with time now - do not send push message if the time is past
//	if((time() - strtotime($sendTimeString))>0){
//		echo "Time set to push message is later than the time right now, Program End.";
//		die();
//	}
//	
//	if (!empty($userIdArr)) {
//		$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($userIdArr), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
//	} else {
//		$parentStudentAssoAry = array();
//	}
//	
//	$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
//	
//	if (!empty($parentStudentAssoAry)) {
////		debug_pr($individualMessageInfoAry);
////		debug_pr($messageTitle);
////		debug_pr($messageContent);
////		debug_pr($isPublic);
////		debug_pr($appType);
////		debug_pr($sendTimeMode);
////		debug_pr($sendTimeString);
//		$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eLibPlus', '0');
////		debug_pr($notifyMessageId);
//	}
//	$forceUseClass = false;
//}
?>