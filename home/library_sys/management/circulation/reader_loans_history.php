<?php

# modifying by: 

/**
 * Log :
 * 
 * Date     2019-03-28 [Cameron] show BookSubTitle if $sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] is true [case #K154500]
 * 
 * Date		2015-11-11 [Cameron]
 * 			show column "borrow_accompany_material" if circulation_borrow_return_accompany_material is true
 * 
 * Date		2013-05-07 [Cameron]
 * 			Show borrow history of a reader in a table 
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once('User.php');
include_once('BookManager.php');
include_once('RightRuleManager.php');
include_once('TimeManager.php');
intranet_auth();
intranet_opendb();


$recordManager=new RecordManager($libms);


$encodedUserID = urlencode($user_id);
$readerBorrowHistory = $recordManager->getBorrowHistoryByUserID($encodedUserID);
if ($encodedUserID!="")
{
	$borrow_by_user = $recordManager->getBorrowByUser($encodedUserID);
}
else
{
	$borrow_by_user = "";	
}

$section_title_used = ($full_details) ? $Lang["libms"]["Circulation"]["ReaderBorrowHistory"] : $Lang["libms"]["CirculationManagement"]["borrowed_by"].": ".$borrow_by_user;
?>


<div class="table_board">
	<h1>
		<?=$section_title_used?>
	</h1>	
		<table class="common_table_list">
		<col  />
		<thead>
<?

?>
		<tr>
		  <th class="num_check">#</th>
		  <th><?=$Lang['libms']['book']['code']?></th>
		  <!--<th><?=$Lang['libms']['book']['call_number']?></th>-->
		  <th><?=$Lang['libms']['book']['title']?> [<?=$Lang["libms"]["book"]["info_book"]?>]</th>
		  <th><?=$Lang['libms']['book']['responsibility_by1']?> / <?=$Lang['libms']['book']['publisher']?></th>
		  <!--<th><?=$Lang['libms']['book']['publisher']?></th>-->
<?php
	$libms = new liblms();
	if ($libms->get_system_setting("circulation_borrow_return_accompany_material")) {
		echo '<th style="min-width:60px;">'.$Lang["libms"]["CirculationManagement"]["borrow_accompany_material"].'</th>';
	}
?>		  
		  <th style="min-width:75px;"><?=$Lang['libms']['CirculationManagement']['borrowday']?></th>
		  <th style="min-width:75px;"><?=$Lang['libms']['CirculationManagement']['dueday']?></th>
		  <th style="min-width:75px;"><?=$Lang['libms']['CirculationManagement']['returnday']?></th>
		  <th><?=$Lang["libms"]["report"]["pay"]?></th>
		</tr>
		
		<?
			$i = 0;

//			debug_r($readerBorrowHistory);
			if (!empty($readerBorrowHistory))
				foreach ($readerBorrowHistory as $book) :
//				debug_r($book);
					$i++;
					$displayBookTitle = $book['BookTitle'];
					if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle']) {
					    $displayBookTitle .= $book['BookSubTitle'] ? " :" . $book['BookSubTitle'] : "";
					}
					$displayBookTitle .= " [".$book['BarCode']."]";
					
					# highlight
					if (strtotime(date('Y-m-d'))>strtotime($book['DueDate']) && ($book['ReturnedTime']=="" || $book['ReturnedTime']=="0000-00-00 00:00:00") && $book['Payment']=="" && $book['RecordStatus']!="LOST")
					{
						# overdue but not returned yet
						$row_css = 'class="alert_row"';
						$return_result = $Lang["libms"]["CirculationManagement"]["msg"]["overdue1"];//."2(".$book['ReturnedTime'].")";
					} else
					{
						# current and returned record
						
						if ($book['ReturnedTime']=="0000-00-00 00:00:00")
						{
							$return_result = ($book['Payment']!="" || $book['RecordStatus']=="LOST") ? $Lang["libms"]["book_status"]["LOST"] : "--";
						} else
						{
							$return_result = date('Y-m-d',strtotime($book['ReturnedTime']));
						}
						$row_css = ($return_result=="--") ? 'class="avaliable_row"' : "";
					}
					
		?>
					<tr <?=$row_css?>>
						<td><?=$i?></td>
						<td><?=$book['ACNO']?></td>
						<!--<td><?=$book['CallNum']?></td>-->
						<td><?=$displayBookTitle?></td>
						<td><?=$book['ResponsibilityBy1']?> / <?=$book['Publisher']?></td>
						<!--<td><?=$book['Publisher']?></td>-->
<?php
	if ($libms->get_system_setting("circulation_borrow_return_accompany_material")) {
		$borrow_accompany_material = $book['BorrowAccompanyMaterial'] ? $Lang["libms"]["CirculationManagement"]["borrow_accompany_material_yes"] : $Lang["libms"]["CirculationManagement"]["borrow_accompany_material_no"];
		echo '<td>'.$borrow_accompany_material.'</td>';
	}
?>					  
						<td><?=date('Y-m-d',strtotime($book['Borrowtime']))?></td>
						<td><?=date('Y-m-d',strtotime($book['DueDate']))?></td>	   
						<td><?=$return_result?></td>
						<td><?=$book['Payment']==""? "--" : "$".$book['Payment']?></td>
					</tr>
				<? 
					endforeach;
				?>
		
		</thead>

		</tbody>
		</table>
	</div>