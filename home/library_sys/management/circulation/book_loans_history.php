<?php

# modifying by:    

/**
 * Log :
 * 
 * Date     2019-03-28 [Cameron]
 *          - retrieve BookSubTitle and order by it after BookTitle if $sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] is true [case #K154500]
 * 
 * Date     2019-03-14 [Cameron]
 *          - fix $BarCode shoulld replace space with plus sign because it has the reverse operation from the calling page (tb_show) [ej.5.0.9.3.1 deploy test]
 *
 * Date		2018-01-31 [Cameron]
 *			- show ItemSeriesNum after Book Title [case #L130405]
 * 
 * Date		2017-12-07 [Cameron]
 * 			- allow keyword search to get result of any combination of single word in the field [case #F132432]
 * 			apply to following fields only: BookTitle, BookSubTitle
 * 
 * Date		2017-04-06 [Cameron] 
 * 			- handle apostrophe problem by applying stripslashes() for search field when get_magic_quotes_gpc() is not set (php5.4)
 * 			- modify json_pre_treat_string() to escape backslash for php < 5.4
 * 
 * Date		2016-03-08 [Cameron]
 * 			- fix bug: don't output json_encode($bookInfo) when called from Thickbox method (by BarCode) (e.g. tb_show() in renew.php)
 * 
 * Date		2016-02-17 [Cameron]
 * 			- add function json_pre_treat_string()
 * 			- pass json recordset without html tags for multiple books to calling function 
 * 
 * Date		2016-02-12 [Cameron]
 * 			- add search by book title, ACNO, CallNum and author (By BarCode already exist)
 * 			- list result of books if there's multiple books, show loan history if there's only one book
 * 
 * Date		2015-07-13 [Cameron]
 * 			show AccompanyMaterial if it's not empty / not null
 * 
 * Date		2013-10-07 [Henry]
 * 			add table to show the resveration record
 * 
 * Date		2013-09-17 [Yuen]
 * 			show basic book information
 * Date		2013-05-07 [Cameron]
 * 			Show borrow history of a book in a table 
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT.'home/library_sys/management/circulation/User.php');
include_once($PATH_WRT_ROOT.'home/library_sys/management/circulation/BookManager.php');
intranet_auth();
intranet_opendb();


$libms = new liblms();
$recordManager=new RecordManager($libms);


function json_pre_treat_string($ary) {
	foreach ((array)$ary as $k=>$v){
		if (is_null($v)){
			$v ='';
		}
		if (get_magic_quotes_gpc()) {		
			$v = str_replace("\\", "\\\\", $v);
		}		
		$v = str_replace("\n", '', $v);
		$v = str_replace("\t", '', $v);
		$ary[$k] = $v;
	}
	unset($v);
	return $ary;
}


$keyword = (isset($Keyword) && $Keyword != '') ? trim($Keyword) : '';
if (!get_magic_quotes_gpc()) {
	$keyword 	= stripslashes($keyword);
}

if($keyword!="") {
	$unconverted_keyword_sql = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));		// A&<>'"\B ==> A&<>\'\"\\\\B
	$converted_keyword_sql = special_sql_str($keyword);											// A&<>'"\B ==> A&amp;&lt;&gt;\'&quot;\\\\B
	$keyword_sql = $unconverted_keyword_sql;	
}
else {
	$unconverted_keyword_sql = '';
	$converted_keyword_sql ='';
	$keyword_sql = '';
}

$fields[] = "CONCAT(b.BookTitle, IF(bu.ItemSeriesNum<>'' AND bu.ItemSeriesNum<>'1' AND bu.ItemSeriesNum IS NOT NULL,CONCAT(' ',bu.ItemSeriesNum),'')) as BookTitle";
$fields[] = 'b.ResponsibilityBy1';
$fields[] = "CONCAT(IF(b.`CallNum` IS NULL,'',b.`CallNum`), ' ', IF(b.`CallNum2` IS NULL,'',b.`CallNum2`)) As CallNum";
$fields[] = 'bu.ACNO';
$fields[] = 'bu.BarCode';
$fields[] = 'b.BookSubTitle';

$showBackButton = false;
$byBarCode = false;
if (isset($BarCode) && $BarCode != '') {
    $BarCode = str_replace(' ', '+', $BarCode);
	$conds[] = "bu.BarCode='{$BarCode}'";
	$byBarCode = true;
}
else if (isset($SpecificBarCode) && $SpecificBarCode != '') {
	$conds[] = "bu.BarCode='{$SpecificBarCode}'";
	$showBackButton = true;
}
else {
	if ($unconverted_keyword_sql == $converted_keyword_sql) {		// keyword does not contain any special character: &<>"
		$words = getWords($unconverted_keyword_sql);	
		$conds[] = "bu.BarCode LIKE '%{$keyword_sql}%'";
		$conds[] = "(b.`BookTitle` like '%".implode("%' AND b.`BookTitle` LIKE'%",$words)."%')";
		$conds[] = "(b.`BookSubTitle` like '%".implode("%' AND b.`BookSubTitle` LIKE'%",$words)."%')";
		$conds[] = "b.`CallNum` LIKE '%{$keyword_sql}%'";
		$conds[] = "b.`CallNum2` LIKE '%{$keyword_sql}%'";
		$conds[] = "CONCAT(b.`CallNum`, ' ', b.`CallNum2`) LIKE '%{$keyword_sql}%'";
	}
	else {
		$words_1 = getWords($unconverted_keyword_sql);
		$words_2 = getWords($converted_keyword_sql);
		$conds[] = "bu.`BarCode` LIKE '%$unconverted_keyword_sql%' OR bu.`BarCode` LIKE '%$converted_keyword_sql%'";
		$conds[] = "(b.`BookTitle` like '%".implode("%' AND b.`BookTitle` LIKE'%",$words_1)."%') OR (b.`BookTitle` like '%".implode("%' AND b.`BookTitle` LIKE'%",$words_2)."%')";
		$conds[] = "(b.`BookSubTitle` like '%".implode("%' AND b.`BookSubTitle` LIKE'%",$words_1)."%') OR (b.`BookSubTitle` like '%".implode("%' AND b.`BookSubTitle` LIKE'%",$words_2)."%')";
		$conds[] = "b.`CallNum` LIKE '%$unconverted_keyword_sql%' OR b.`CallNum` LIKE '%$converted_keyword_sql%'";
		$conds[] = "b.`CallNum2` LIKE '%$unconverted_keyword_sql%' OR b.`CallNum2` LIKE '%$converted_keyword_sql%'";
		$conds[] = "CONCAT(b.`CallNum`, ' ', b.`CallNum2`) LIKE '%$unconverted_keyword_sql%' OR CONCAT(b.`CallNum`, ' ', b.`CallNum2`) LIKE '%$converted_keyword_sql%'";
	}
}

$cond =  implode(' OR ',$conds);
$sql = " SELECT DISTINCT ".implode (", ", $fields). " " .
		" FROM " .
		"	LIBMS_BOOK AS b LEFT JOIN LIBMS_BOOK_UNIQUE AS bu ON bu.BookID=b.BookID " .
		" WHERE (b.`RecordStatus` NOT LIKE 'DELETE' OR b.`RecordStatus` IS NULL) ";
if ($cond != '')
{
	$sql .= " AND (" . $cond . ") ";	
}
$sql .= " ORDER BY b.BookTitle";
if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle']) {
    $sql .= ", b.BookSubTitle";
}
$sql .= ", bu.ACNO ";

$results = $libms->returnArray($sql, null, MYSQL_ASSOC);
$nrResults = count($results);
$json_output['nrResults']=$nrResults;
$json_output['script']= '';
$json_output['Keyword']= str_replace("\\", "\\\\", $keyword);

if ($nrResults > 1) {
	$json_output['is_success']=true;

	foreach ( $results as $bookInfo ){
		$bookInfo = json_pre_treat_string($bookInfo);
		$json_output['bookInfo'][]=$bookInfo;		
	}
	
}
else if ($nrResults == 1) {
	
	$json_output['is_success']=true;
	$BarCode = $results[0]['BarCode'];
	
//	$bookBorrowHistory = $recordManager->getBorrowHistoryByBarCode(urlencode($BarCode));
	$bookBorrowHistory = $recordManager->getBorrowHistoryByBarCode($BarCode);
	
	$BookManager = new BookManager($libms);
	
//	$bookUniqueID = $BookManager->getUniqueBookIDFormBarcode(urlencode($BarCode));
	$bookUniqueID = $BookManager->getUniqueBookIDFormBarcode($BarCode);
	$bookID = $BookManager->getBookIDFromUniqueBookID($bookUniqueID);
	$bookInfo = $BookManager->getBookFields($bookID, "*", $bookUniqueID);
	
	
	$BookStatusShow = $Lang["libms"]["book_status"][$bookInfo["RecordStatus"]];
	if (!$bookInfo["OpenBorrow"])
	{
		$BookStatusAppend .= $Lang['libms']['book']['borrow_disallowed'];
	}
	if (!$bookInfo["OpenReservation"])
	{
		$BookStatusAppend .= (($BookStatusAppend!="") ? " & " : "" ). $Lang['libms']['book']['reservation_disallowed'];
	}
	if ($BookStatusAppend!="")
	{
		$BookStatusShow = " ".$BookStatusAppend."!";
	}
	
	if ($bookInfo["CoverImage"]!="" && file_exists("../../../..".$bookInfo["CoverImage"]))
	{
		$BookCover = "<img src='".$bookInfo["CoverImage"]."' border='0' />";
	}
	
	$BookLocation_result = $libms->SELECTFROMTABLE('LIBMS_LOCATION',$Lang['libms']['sql_field']['Description'],array('LocationCode' => PHPTOSQL($bookInfo['LocationCode'])),1);
	$BookLocation = $BookLocation_result[0][0];
	//debug_r($bookInfo);
	
	$CirculationTypeApplied = (trim($bookInfo['Item_CirculationTypeCode'])<>"") ? $bookInfo['Item_CirculationTypeCode'] : $bookInfo['CirculationTypeCode'];
	$CirculationObj = $libms->GET_CIRCULATION_TYPE_INFO($CirculationTypeApplied);
	$CirculationObj = $CirculationObj[0];
	$CirculationTypeTitle = ($intranet_session_language=="en") ? $CirculationObj["DescriptionEn"] : $CirculationObj["DescriptionChi"];
	
	$CurrentReservations = $recordManager->getReservationByBookID($bookID, array("WAITING", "READY"), "asc", "", true);
	$CurrentReservationTotal = (is_array($CurrentReservations)) ? sizeof($CurrentReservations) : 0;
	
	if ($bookID=="" || $bookID<0)
	{
		echo $Lang["libms"]["report"]["norecord"];
		die();
	}
	
	$BookCoverTD = ($BookCover!="") ? 120 : 5;

	if ($byBarCode) {
		// do nothing
	}
	else {
		$bookInfo = json_pre_treat_string($bookInfo);
	}

$json_output['script']= '
<script>
	$(document).unbind("keyup").bind("keyup", function(e) {	//20131206-circulation-accno
	  if (e.keyCode == 27) { 
	  		$("#TB_closeWindowButton").click();
	  }   
	});
</script>';

ob_start();

?>

<div class="table_board">
	<h1>
		<?= $bookInfo["BookTitle"] . " [" . $bookInfo['BookCode'] . "]"?>
	</h1>	
	
					

		        		<table class="form_table_v30" cellpadding="2">
							<tbody>
		                        <tr class="row_drafted">
		                        	<td rowspan="<?=(empty($bookInfo['AccompanyMaterial'])? 10 : 11)?>" width="<?=$BookCoverTD?>" align="center"><?=$BookCover?></td>
		                        	<td width="20%" class="field_title_short" style="border-bottom: 1px solid #CCCCCC;"><span class="field_title"><?= $Lang['libms']['book']['subtitle'] ?>:</span></td>
		                        	<td style="border-bottom: 1px solid #DDDDDD;"><?=$bookInfo["BookSubTitle"]?></td>
								</tr>
		                        <tr>
		                        	<td class="field_title_short" style="border-bottom: 1px solid #CCCCCC;"><span class="field_title"><?= $Lang['libms']['book']['call_number'] ?>:</span></td>
		                        	<td style="border-bottom: 1px solid #DDDDDD;"><?=$bookInfo["CallNum"]?> <?=$bookInfo["CallNum2"]?></td>
								</tr>
		                        <tr class="row_drafted">
		                        	<td class="field_title_short" style="border-bottom: 1px solid #CCCCCC;"><span class="field_title"><?= $Lang['libms']['book']['ISBN'] ?>:</span></td>
		                         	<td style="border-bottom: 1px solid #DDDDDD;"><?=$bookInfo["ISBN"]?> <?=$bookInfo["ISBN2"]?></td>
								</tr>
		                        <tr>
		                        	<td class="field_title_short" style="border-bottom: 1px solid #CCCCCC;"><span class="field_title"><?= $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Author'] ?>:</span></td>
		                        	<td style="border-bottom: 1px solid #DDDDDD;"><?=nl2br($bookInfo["ResponsibilityBy1"])?></td>
								</tr>
		                        <tr  class="row_drafted">
		                        	<td class="field_title_short" style="border-bottom: 1px solid #CCCCCC;"><span class="field_title"><?= $Lang['libms']['book']['publisher'] ?>:</span></td>
		                        	<td style="border-bottom: 1px solid #DDDDDD;"><?=$bookInfo["Publisher"]?></td>
								</tr>
								<!--
		                        <tr>
		                        	<td class="field_title_short" style="border-bottom: 1px solid #CCCCCC;"><span class="field_title"><?= $Lang['libms']['book']['language'] ?>:</span></td>
		                        	<td style="border-bottom: 1px solid #DDDDDD;"><?=$bookInfo["Language"]?></td>
								</tr>
		                        <tr>
		                        	<td class="field_title_short" style="border-bottom: 1px solid #CCCCCC;"><span class="field_title"><?= $Lang['libms']['book']['country'] ?>:</span></td>
		                         	<td style="border-bottom: 1px solid #DDDDDD;"><?=$bookInfo["Country"]?></td>
								</tr>
								-->
		                        <tr>
		                        	<td class="field_title_short" style="border-bottom: 1px solid #CCCCCC;"><span class="field_title"><?= $Lang["libms"]["report"]["circulation_type"] ?>:</span></td>
		                         	<td style="border-bottom: 1px solid #DDDDDD;"><?=$CirculationTypeTitle?></td>
								</tr>
		                        <tr class="row_drafted">
		                        	<td class="field_title_short" style="border-bottom: 1px solid #CCCCCC;"><span class="field_title"><?= $Lang["libms"]["book"]["book_status"] ?>:</span></td>
		                        	<td style="border-bottom: 1px solid #DDDDDD;"><?=$BookStatusShow?></td>
								</tr>
		                        <tr >
		                        	<td class="field_title_short" style="border-bottom: 1px solid #CCCCCC;"><span class="field_title"><?= $Lang["libms"]["CirculationManagement"]["location"] ?>:</span></td>
		                        	<td style="border-bottom: 1px solid #DDDDDD;"><?=$BookLocation?></td>
								</tr>
								<!--
		                        <tr>
		                        	<td class="field_title_short" style="border-bottom: 1px solid #CCCCCC;"><span class="field_title"><?= $Lang['libms']['book']['introduction'] ?>:</span></td>
		                        	<td style="border-bottom: 1px solid #DDDDDD;"><?=nl2br($bookInfo["Introduction"])?></td>
								</tr>
								-->
								
		                        <tr class="row_drafted">
		                        	<td class="field_title_short" style="border-bottom: 1px solid #CCCCCC;"><span class="field_title"><?= $Lang['libms']['bookmanagement']['book_reserve'] ?>:</span></td>
		                        	<td style="border-bottom: 1px solid #DDDDDD;"><?=$CurrentReservationTotal?></td>
								</tr>
		                        <tr>
		                        	<td class="field_title_short" style="border-bottom: 1px solid #CCCCCC;"><span class="field_title"><?= $Lang['libms']['book']['remark_to_user']  ?>:</span></td>
		                        	<td style="border-bottom: 1px solid #DDDDDD;"><?=nl2br($bookInfo["RemarkToUser"])?></td>
								</tr>
<? if (!empty($bookInfo['AccompanyMaterial'])): ?>
		                        <tr class="row_drafted">
		                        	<td class="field_title_short" style="border-bottom: 1px solid #CCCCCC;"><span class="field_title"><?= $Lang['libms']['book']['AccompanyMaterial'] ?>:</span></td>
		                        	<td style="border-bottom: 1px solid #DDDDDD;"><?=nl2br($bookInfo["AccompanyMaterial"])?></td>
								</tr>				
<? endif; ?>
		                  	</tbody>
						</table>
				<br />
		<table class="common_table_list">
		<col  />		
		<thead>
<?

?>
		<tr>
		  <th class="num_check">#</th>
		  <th><?=$Lang["libms"]["CirculationManagement"]["borrowed_by"]?></th>
		  <th><?=$Lang['libms']['CirculationManagement']['borrowday']?></th>
		  <th><?=$Lang['libms']['CirculationManagement']['dueday']?></th>
		  <th><?=$Lang['libms']['CirculationManagement']['returnday']?></th>
		  <!--  <th><?=$Lang['libms']['CirculationManagement']['status']?></th> -->
		</tr>
		
		<?

			$i = 0;

//			debug_r($bookBorrowHistory);
			if (!empty($bookBorrowHistory))
				foreach ($bookBorrowHistory as $book) :
					
					$tr_style = "";
					if ($book['ReturnedTime']=="0000-00-00 00:00:00")
					{
						$returnDate = "--";
						$tr_style = " style=\"background-color:#E8FFE8;\" ";
					} else
					{
						$returnDate = date('Y-m-d',strtotime($book['ReturnedTime']));
						if (strtotime(date('Y-m-d',$book['DueDate']))<strtotime(date('Y-m-d',$book['ReturnedTime'])))
						{
							# late return
							$returnDate = "<font color='red'>".$returnDate."</font>";
						}
					}
					
					$DueDate = date('Y-m-d',strtotime($book['DueDate']));
					if (strtotime(date('Y-m-d',$book['DueDate']))>=strtotime(date('Y-m-d',time())))
					{
						# expired
						$DueDate = "<font color='red'>".$DueDate."</font>";
					}
		?> 
					  
					<tr >
						<td <?=$tr_style?>><?=sizeof($bookBorrowHistory)-$i?></td>
						<td <?=$tr_style?>><?=$book['UserName']?></td>
						<td <?=$tr_style?>><?=date('Y-m-d',strtotime($book['Borrowtime']))?></td>
						<td <?=$tr_style?>><?=$DueDate?></td>	   
						<td <?=$tr_style?>><?=$returnDate?></td>
					</tr>
				<? 
				
					$i++;
//					unset($is_overdue);
					endforeach;
					
					if (empty($bookBorrowHistory))
					{
					 echo "<tr ><td colspan='5'>".$Lang["libms"]["report"]["norecord"]."</td></tr>\n";
					}
				?>
		
		</thead>
		<!-- Henry Added [Start] -->
<?		if (!empty($CurrentReservations)){?>
				<thead>



		<tr>
		  <th class="num_check">#</th>
		  <th><?=$Lang['libms']['bookmanagement']['book_reserve']?></th>
		  <th><?=$Lang["libms"]["book"]["reserve_time"]?></th>
		  <th><?=$Lang["libms"]["CirculationManagement"]["valid_date"]?></th>
		  <th><?=$Lang['libms']['CirculationManagement']['status']?></th>
		  
		</tr>
		
		<?

			$i = 0;

//			debug_r($bookBorrowHistory);
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				foreach ($CurrentReservations as $book) :
					
					$tr_style = "";
					if ($book['ReturnedTime']=="0000-00-00 00:00:00")
					{
						$returnDate = "--";
						$tr_style = " style=\"background-color:#E8FFE8;\" ";
					} else
					{
						$returnDate = date('Y-m-d',strtotime($book['ReturnedTime']));
						if (strtotime(date('Y-m-d',$book['DueDate']))<strtotime(date('Y-m-d',$book['ReturnedTime'])))
						{
							# late return
							$returnDate = "<font color='red'>".$returnDate."</font>";
						}
					}
					
					if ($book['ExpiryDate']=="0000-00-00"){
						$ExpiryDate = "--";
					}
					else{
						$ExpiryDate = date('Y-m-d',strtotime($book['ExpiryDate']));
					}
//					if (strtotime(date('Y-m-d',$book['DueDate']))>=strtotime(date('Y-m-d',time())))
//					{
//						# expired
//						$DueDate = "<font color='red'>".$DueDate."</font>";
//					}
					$tempBookStatus ='';
					if($book['RecordStatus'] == "WAITING"){
						$tempBookStatus = $Lang["libms"]["book_reserved_status"]["WAITING"];
					}
					else if($book['RecordStatus'] == "READY"){
						$tempBookStatus = $Lang["libms"]["book_reserved_status"]["READY"];
					}
		?> 
					  
					<tr >
						<td <?=$tr_style?>><?=sizeof($CurrentReservations)-$i?></td>
						<td <?=$tr_style?>><?
						$lu = new libuser($book['UserID']);
						echo $lu->getNameWithClassNumber($book['UserID'])." #".$lu->UserLogin;
						?></td>
						<td <?=$tr_style?>><?=date('Y-m-d',strtotime($book['ReserveTime']))?></td>
						<td <?=$tr_style?>><?=$ExpiryDate?></td>	   
						<td <?=$tr_style?>><?=$tempBookStatus?></td>
					</tr>
				<? 
				
					$i++;
//					unset($is_overdue);
					endforeach;
					
//					if (empty($CurrentReservations))
//					{
//					 echo "<tr ><td colspan='5'>".$Lang["libms"]["report"]["norecord"]."</td></tr>\n";
//					}
					}
				?>
		
		</thead>
		<!-- Henry Added [End] -->
		</tbody>
		</table>
		
<?php
	if ($showBackButton) {
		print "
			<br />		
			<table border=0>
				<tr>
					<td align=\"center\">
						<input type=\"button\" onClick=\"BackToSearchList()\" class=\"formbutton_v30 print_hide\" value=\"".$button_back."\"/>
						<input type=\"hidden\" id=SearchKeyword name=SearchKeyword value=\"".htmlspecialchars(str_replace("\\", "\\\\", $keyword),ENT_QUOTES)."\">
					</td>
				</tr>
			</table>\n";
	}
?>		
	</div>
<?php 
	$x = ob_get_contents();
	ob_end_clean();
	$x = str_replace("\n", '', $x);
	$x = str_replace("\t", '', $x);
	$json_output['bookInfo']=$x;
	
} // $nrResults == 1
else {
	$json_output['is_success']=false;
}

if ($byBarCode) {
	echo $json_output['bookInfo'];	// one record only and not json encoded
}
else {
	echo json_encode($json_output);
}

?>	