<?php

## being modified by : 

/*
 * 	Log
 * 
 * 	2017-11-01 Cameron
 * 		- update UserBalance when change additional charge [case #F130172]
 * 	
 * 	2017-03-14 Cameron
 * 		- add IP checking for circulation
 * 
 * 	2016-09-06 Cameron	- create this file
 * 
 */ 
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/RecordManager.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html("libms_circulation.html");
$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right,$c_m_right);
$global_lockout = (BOOL)$libms->get_system_setting('global_lockout');
if (!$_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && !($c_right['circulation management']['overdue'] || $global_lockout)))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$OverDueLogID = $_REQUEST['OverDueLogID'];
$condition = array('OverDueLogID'=>$OverDueLogID);

$RecordManager = new RecordManager($libms);
$rs = $RecordManager->getOverDueRecords(null,$condition);
$rs = current($rs);
$payment = $rs['Payment'] - $rs['AdditionalCharge'];

### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "", 'submitBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
?>

<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">

<div id="thickboxContainerDiv" class="edit_pop_board">
	<form id="thickboxForm" name="thickboxForm">
		<div id="thickboxContentDiv" class="edit_pop_board_write">
			<table class="form_table_v30">
				<tr>
					<td class="field_title"><?=$Lang['libms']['CirculationManagement']['booktitle']?></td>
					<td><?=$rs['BookTitle']?> [<?=$rs['BarCode']?>]</td>
				</tr>		
				<tr>
					<td class="field_title"><?=$Lang['libms']['CirculationManagement']['overdue_type']?></td>
					<td><?=(($rs['DaysCount']!="")? sprintf($Lang["libms"]["CirculationManagement"]["overdue_with_day"], $rs['DaysCount']) : $Lang["libms"]["book_status"]["LOST"]) ?></td>
				</tr>		
				<tr>
					<td class="field_title"><?=$Lang['libms']['CirculationManagement']['payment']?></td>
					<td>$<?=$payment?></td>
				</tr>		
				<tr>
					<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["libms"]["CirculationManagement"]["additional_charge"]?></td>
					<td>$<input name="additional_charge" type="text" class="textboxnum2 requiredField" id="additional_charge" value="<?=$rs['AdditionalCharge']?>" style="margin-top: -5px; margin-left: 3px;" />
						<?=$linterface->Get_Form_Warning_Msg('textEmptyWarnDiv', $Lang['libms']['CirculationManagement']['msg']['please_input_additional_charge'], $Class='warnMsgDiv')?>
					</td>
				</tr>
						
			</table>
			
			<?=$linterface->MandatoryField();?>
			
		</div>
		<div id="editBottomDiv" class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
		<input type='hidden' name="OverDueLogID" value="<?=$OverDueLogID?>">
	</form>	
</div>


<script>
	function checkMandatoryField() {
		var ret = true;
		$(".requiredField").each (function() {
			if ($.trim($(this).val()) == '') {
				$(this).next().css('display','block');
				ret = false;
			}
		});
		return ret;
	}
	
	$(document).ready(function() {
		var ret;
		$(".warnMsgDiv").css('display','none');
		$("#submitBtn_tb").click(function(){
			ret = checkMandatoryField();
			
			if (isNaN($('#additional_charge').val()) || (parseFloat($('#additional_charge').val()) < 0)) {
				alert("<?=$Lang['libms']["settings"]['system']['circulation_input_non_negative_value']?>");
				ret = false;
			}
			if (ret) {
				$.ajax({
					dataType: "json",
					type: "POST",
					async: false,				
					url: 'ajax_save_overdue_additional_charge.php',
					data:$('#thickboxForm').serialize(),
					timeout:1000,
					success:function( ajaxReturn ){
						if (ajaxReturn != null && ajaxReturn.success){
	  						$('.overdueRemarkContent').html(ajaxReturn.additional_charge);
							$('tr#'+<?=$OverDueLogID?>).find('label.AdditionalCharge').html(parseFloat(ajaxReturn.additional_charge));
							var newAdditionalCharge = parseFloat(ajaxReturn.additional_charge);
							var paymentReceived = parseFloat($('tr#'+<?=$OverDueLogID?>).find('label.PaymentReceived').html());
							var payment = parseFloat($('tr#'+<?=$OverDueLogID?>).find('label.Payment').html()); 
							var remain = payment + newAdditionalCharge - paymentReceived;
							$('input#paying_'+<?=$OverDueLogID?>).val(remain);
							$('input#paying_'+<?=$OverDueLogID?>).trigger("change");
							$('#UserBalance').html(ajaxReturn.new_balance);
							js_Hide_ThickBox();
						}
					},
					error:function( msg ){
						alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
						return false;
					}
				});
			}
		});	
	});
</script>

<?
intranet_closedb();
?>
 
