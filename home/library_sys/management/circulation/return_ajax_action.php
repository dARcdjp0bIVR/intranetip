<?php

// being modified by: 

/**
 * Log :
 *
 * Date 2020-05-07 [Cameron]
 * - don' allow to return a book which item status is set to 'WRITEOFF' [case #T176570]
 * 
 * Date 2018-02-14 [Cameron]
 * - add checking if it's allowed to return books that are in class management location [case #E129637]
 * - fix: add meta characterset (utf-8) to show no access right message
 *
 * Date 2017-03-14 [Cameron]
 * - add IP checking for circulation
 *
 * Date: 2016-12-06 [Cameron]
 * - add empty checking for $overDue when set $payment_text_link
 *
 * Date 2016-05-18 [Cameron]
 * - fix bug: output json when LocationIncorrect is detected
 * - show book item status if the book has been returned before [case #K95339]
 * Date 2015-11-20 [Cameron]
 * - set default value of $json['LocationIncorrect'] to FALSE
 *
 * Date 2015-10-30 [Cameron]
 * - fix bug on showing accompany material that contains back slash in jConfirm
 *
 * Date 2015-10-15 [Cameron]
 * - fix bug on showing row number for returned book list if "Return book confirmation" is false
 * - set focus to input_code_text field after user click Yes / No button about borrow accompany material or not
 * Date 2015-10-14 [Cameron]
 * - add checkOnly to pass to gen_return_book_list() so that it can show book reservation person in checking stage
 * Date 2015-08-18 [Cameron]
 * - call function ($RecordManager->returnBook) to handle book return
 * Date 2015-08-17 [Cameron]
 * - prompt user to return accompany material with book
 * Date 2015-07-13 [Cameron]
 * - show AccompanyMaterial if circulation_display_accompany_material is set
 * Date 2015-06-10 [Henry] Apply class management permission
 * Date 2014-11-27 [Ryan] Added return at the same day Status msg
 * Date 2014-09-17 [Henry] added $sys_custom['eLibraryPlus']['hideUserBarcode']
 * Date 2013-09-16 [Yuen] allowed to return book which has no borrow log (e.g. On Loan status is migrated from previous LMS)
 * Date 2013-06-13 [Cameron] Fix bug for tb_show in parsing argument with single quote /double quote
 * Date 2013-05-13 [Yuen]
 * fixed the problem that using the admin user as the reader for batch book return (i.e. penalty will be affected!)
 * Date 2013-05-07 [Cameron]
 * 1.Modify the parameter of tb_show, delete "&TB_iframe=true", pass BarCode to book_loans_history.php
 * for displaying the borrow history of a book
 * 2.Add link to display borrow history of a reader
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

// ###### Trim all request #######
function recursive_trim(&$array)
{
    foreach ($array as &$item) {
        if (is_array($item)) {
            recursive_trim($item);
        } else {
            if (is_string($item))
                $item = trim($item);
        }
    }
}
recursive_trim($_REQUEST);

// ###### END OF Trim all request #######
function gen_confirm_script($data)
{
    global $Lang;
    $x = '';
    ob_start();
    
    ?>
<script>
jConfirm("<?=str_replace("\\","\\\\\\\\",$data['AccompanyMaterial'])."<br>".$Lang["libms"]["Circulation"]["ReturnAccompayMaterial"]?>",
		 "<?=$Lang['libms']["settings"]['system']['circulation_borrow_return_accompany_material']?>", 
		 function(r){
		 	if (r) {
				$.getJSON(
					'return_ajax_action.php',
					{ 
						barcode : "<?=$data['barcode']?>",
						ID : "<?=$data['ID']?>",						
						ConfirmReturnAttachment: 1,
						Mode: "<?=$data['Mode']?>"
					},
					'JSON'
				)
				.success(return_ajax_handler)
				.error(function(){ alert('Error on AJAX call!'); });
		 	}
		 	else {
		 		$('#input_code_text').focus();
		 	}
		 }, 
		 "<?=$Lang['libms']["settings"]['system']['yes']?>", 
		 "<?=$Lang['libms']["settings"]['system']['no']?>");
</script>
<?php
    $x = ob_get_contents();
    ob_end_clean();
    $x = str_replace("\n", '', $x);
    $x = str_replace("\t", '', $x);
    return $x;
}

// function output($json_data){
// echo json_encode($json_data);
// exit;
// }

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ('User.php');
include_once ('BookManager.php');
include_once ('RightRuleManager.php');
include_once ('TimeManager.php');
intranet_auth();
intranet_opendb();

global $sys_custom;

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);
$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['return'] || $global_lockout))) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}
$TM = new TimeManager();
if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! $TM->isOpenNow(time()) && ! $c_right['circulation management']['access_without_open_hour']) {
    $error_msg = $Lang['libms']['CirculationManagement']['library_closed'];
    echo $error_msg;
    exit();
}

$barcode = $_REQUEST['barcode'];
$ID = $_REQUEST['ID'];
$Mode = $_REQUEST['Mode'];
$confirmReturnAttachment = $_REQUEST['ConfirmReturnAttachment'];
$confirmReturn = $_REQUEST['ConfirmReturn']; // Confirm Return is needed and has been confirmed
                                             
// $interface = new interface_html("libms_circulation.html");
$GLOBALS['LIBMS']['PAGE'] = "RETURN";
$json['success'] = FALSE;
$json['LocationIncorrect'] = FALSE;
if ($libms->get_system_setting("circulation_return_book_by_confirm") && (! $confirmReturn)) {
    $json['alertReservePerson'] = FALSE;
} else {
    $json['alertReservePerson'] = TRUE;
}

$BookManager = new BookManager($libms);
$RecordManager = new RecordManager($libms);

$bookUniqueID = $BookManager->getUniqueBookIDFormBarcode($barcode);
if (empty($bookUniqueID)) // the barcode does not exist in eLib system
{
    exit();
} else {
    if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {   // eLib+ admin can return 
        $isAllowToHandleItem = $libms->get_class_mgt_can_handle_item($bookUniqueID);
        $isLocationCodeInClassManagementGroup = false;
        if (is_array($isAllowToHandleItem) && count($isAllowToHandleItem) == 0) {
            $bookItem = $libms->GET_BOOK_ITEM_INFO($bookUniqueID);
            if (count($bookItem)) {
                $locationCode = $bookItem[0]['LocationCode'];
                if (! empty($locationCode)) {
                    $isLocationCodeInClassManagementGroup = $libms->isLocationCodeInClassManagementGroup($locationCode, $isClassManagementUser = false);
                }
            }
        }
        $isGroupManagementReturn = $_SESSION['LIBMS']['admin']['current_right']['circulation management']['return'];
        $isAllowClassManagementCirculation = ! $_SESSION['LIBMS']['admin']['current_right']['circulation management']['not_allow_class_management_circulation'];
        $isClassManagementReturn = $c_m_right['circulation management']['return'];
        
        if (($isGroupManagementReturn && ! $isAllowClassManagementCirculation && ! $isClassManagementReturn && $isLocationCodeInClassManagementGroup) || ($isClassManagementReturn && ! $isAllowToHandleItem)) {
            $json['LocationIncorrect'] = TRUE;
            echo $RecordManager->output($json);
            exit();
        }
    }
}

$ToCheckBookStatus = false;
// check if there is any borrowlog
$uID = $RecordManager->getUserIDByBookUniqueID($bookUniqueID); // get User who borrow the book
if ($uID == 0) // no borrow record
{
    $ToCheckBookStatus = true;
}
else {
    // check if book item status is set to WRITEOFF after borrow, don't allow to process return book if it is
    $recordStatus = $BookManager->getUniqueBookRecordStatus($bookUniqueID);
    if ($recordStatus == 'WRITEOFF') {
        $json['NotAllowReturnWriteoffBook'] = TRUE;
        echo $RecordManager->output($json);
        exit();
    }
}
$accompanyMaterial = $RecordManager->getAccompanyMaterialByBookUniqueID($bookUniqueID);

if ($Mode != "ByBatch" && ! empty($_SESSION['LIBMS']['CirculationManagemnt']['UID'])) {
    $uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];
}

if ($ToCheckBookStatus) {
    // check if the book status is on Loan
    $bookID = $RecordManager->getBookIDByBookUniqueID($bookUniqueID);
    if ($bookID == 0) {
        $recordStatus = $BookManager->getUniqueBookRecordStatus($bookUniqueID);
        if ($recordStatus) { // record exist in the system, tell user the book item status
            $json['BookItemStatus'] = $recordStatus;
            echo $RecordManager->output($json);
        }
        exit();
    } else {
        $IsReturnBookWithoutBorrowLog = true; // no borrow record, but bookunique status is 'BORROWED', this occurs on new install (migration data)
    }
}

$bookID = $BookManager->getBookIDFromUniqueBookID($bookUniqueID);
$bookInfo = $BookManager->getBookFields($bookID, "*", $bookUniqueID);

$loginUser = new User($_SESSION['UserID'], $libms);
$isAllowClassManagementCirculation = $loginUser->rightRuleManager->isAllowClassManagementCirculation($bookInfo['LocationCode'], $circulationAction = 'return');
if (! $isAllowClassManagementCirculation) {
    $json['NotAllowClassManagementCirculation'] = TRUE;
    echo $RecordManager->output($json);
    exit();
}

// # prompt confirm return accompany material or not
if (($libms->get_system_setting("circulation_borrow_return_accompany_material")) && ($accompanyMaterial) && (! $confirmReturnAttachment)) {
    $data['AccompanyMaterial'] = $bookInfo['AccompanyMaterial'];
    $data['barcode'] = $barcode;
    $data['ID'] = $ID;
    $data['Mode'] = $Mode;
    $json['script'] = gen_confirm_script($data);
    $json['success'] = TRUE;
    echo $RecordManager->output($json);
    exit();
} else {
    $json['script'] = '';
}

$RowID = $ID;

// # check if need confirm before return book or not
if ($libms->get_system_setting("circulation_return_book_by_confirm") && (! $confirmReturn)) {
    $checkOnly = 1;
    $ret = $RecordManager->returnBook($bookUniqueID, 'check', $Mode, $uID);
    extract($ret);
    $payment_text_link = $overDue['Payment'] ? '$' . $overDue['Payment'] : '';
} else { // confirmed
    $checkOnly = 0;
    $ret = $RecordManager->returnBook($bookUniqueID, 'return_book', $Mode, $uID);
    extract($ret);
    
    $payment_text_link = empty($overDue) ? '' : '$' . $overDue['Payment'];
    
    if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['overdue'] || $global_lockout)) {} else {
        // able to handle over due payment
        $payment_text_link = '<a href="overdue.php?user_id=' . urlencode($borrowLog["UserID"]) . '&borrow_log_id=' . $borrowLogID . '"> ' . $payment_text_link . " </a><input type='hidden' class='not_class_but_id' name='over_due_users[]' id='over_due_users[]' value='" . $borrowLog["UserID"] . "' /><input type='hidden' class='borrow_log_id_class' name='borrow_log_id[]' id='borrow_log_id[]' value='" . $borrowLogID . "' />";
    }
}

// $ID can be overwridden in returnBook function, now resume back
if (empty($ID)) {
    $ID = $RowID;
}

if ($IsReturnBookWithoutBorrowLog && ! isset($User)) {
    // for calling reservation record purpose
    $User = new User($UserID, $libms);
}

$view_data = compact('bookInfo', 'borrowLog', 'return_row_class', 'IsReturnBookWithoutBorrowLog', 'borrow_by_user', 'alert_text', 'not_allow_overdue_return', 'isOverDue', 'overDue', 'BookLocation', 'reserves', 'User', 'ID', 'barcode', 'payment_text_link', 'bookUniqueID', 'junior_mck', 'checkOnly');

$json['success'] = TRUE;
$json['html'] = $RecordManager->gen_return_book_list($view_data);

$RecordManager->output($json);

?>

