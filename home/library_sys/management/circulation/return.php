<?php
// modifying by:

// Note: Please use UTF-8 for editing

/**
 * ***********************************************************
 *
 * 20200507 (Cameron)
 * - show alert message of not allow returning write-off book in return_ajax_handler() [case #T176570]
 * 
 * 20191011 (Cameron)
 * - use large font for common_table_list if $sys_custom['eLibraryPlus']['Circulation']['LargeFont'] is set [case #K155570]
 * 
 * 20180507 (Cameron)
 * - modify return_ajax_handler() to show no_borrow_record when fail [case #C138500]
 *
 * 20180307 (Cameron)
 * - fix: add intranet_closedb(); before exit at the end so that it can log performance [case #H132329] 
 * 
 * 20180214 (Cameron)
 * - modify return_ajax_handler(), add alert message of "notAllowBorrowReturnClassManagementBooks" [case #E129637]
 * - fix: add meta characterset (utf-8) to show no access right message
 * 
 * 20171127 (Cameron)
 * - set barcode search bar to left position if $sys_custom['eLibraryPlus']['Circulation']['BarcodeSearchBarInLeft'] == true [case #F129893]
 * - unify code for ej change on 20160713
 *
 * 20170314 (Cameron)
 * - add IP checking for circulation
 *
 * 20161205 (Cameron)
 * - fix bug: show the case result when SameDayReturn rule is applied
 *
 * 20160729 (Cameron)
 * - append timestamp to jquery.alerts.js so that it can refresh to take effect
 *
 * 20160713 (Cameron)
 * - change js include file to script.utf8.* to support Chinese in jquery.alerts plugin [ej only]
 *
 * 20160518 (Cameron)
 * - show book item status rather than 'No book is found' when the book has been returned before [case #K95339]
 *
 * 20151120 (Cameron)
 * - fix bug on showing wrong message after confirm return for the first time and do another return.
 * should clear previous list before showing list to return
 *
 * 20151118 (Cameron)
 * - trim all leading and trailing space ( including multiple byte space) for barcode
 *
 * 20151105 (Cameron)
 * - repalce space to empty string for barcode input ( in #return_a_book click event )
 *
 * 20151015 (Cameron)
 * - set focus to input_code_text field after user click Yes / No button about borrow accompany material or not
 *
 * 20151014 (Cameron)
 * - show latest row on top rather than bottom (preserve the order before change on 20150817)
 *
 * 20150824 (Cameron)
 * - add Confirm logic when return book
 *
 * 20150817 (Cameron)
 * - prompt user to return accompany material with book
 *
 * 20150609 (Henry)
 * - apply class management permission
 *
 * ***********************************************************
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "home/library_sys/lib/liblibrarymgmt.php");

// include_once("libms_interface.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
intranet_auth();
intranet_opendb();

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);
$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['return'] || $global_lockout))) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    intranet_closedb();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}
include_once ('TimeManager.php');
$TM = new TimeManager();
if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! $TM->isOpenNow(time()) && ! $c_right['circulation management']['access_without_open_hour']) {
    intranet_closedb();
    header("Location: index.php");
}

if (! isset($_SESSION['LIBMS']['CirculationManagemnt']['UID'])) {
    intranet_closedb();
    header("Location: index.php");
}

$interface = new interface_html("libms_circulation.html");
$GLOBALS['LIBMS']['PAGE'] = "RETURN";

$xmsg = $_SESSION['LIBMS']['xmsg'];
unset($_SESSION['LIBMS']['xmsg']);

$needConfirm = $libms->get_system_setting("circulation_return_book_by_confirm");

$interface->LAYOUT_START();

if ($sys_custom['eLibraryPlus']['Circulation']['LargeFont']) {
    echo '<link href="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/css/elibplus_large_font.css" rel="stylesheet" type="text/css">'."\n";
}
?>

<link href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.alerts.css"
	rel="stylesheet" type="text/css">
<script language="JavaScript"
	src="<?=$PATH_WRT_ROOT?>lang/script<?=$junior_mck?'.utf8':''?>.<?=$intranet_session_language?>.js"></script>
<script language="JavaScript"
	src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.alerts.js?<?=time()?>"></script>

<script type="text/javascript">
	
$().ready( function(){
	$.ajaxSetup({ cache: false});

<? if ($needConfirm):?>
	$(window).on('beforeunload', function(){
		if ($('#return_table tr').length > 1)
			return '<?=$Lang['libms']['CirculationManagement']['msg']['comfirm_leave']?>';
	});
	
	$('input#return_submit').click(function(){
		if (checkform()) {
			$('#ConfirmReturn').val('1');
			$(window).off('beforeunload');
			
			$.ajax({
			  dataType: "json",
			  type: "POST",
			  url: 'return_action.php',
			  data : $('#return_form').serialize(),		  
			  success: after_confirm_return_books,
			  error: function(){ alert('Error on AJAX call!'); }
			});
		}
	});
	
	$('#reset').click(function(){
		$('#return_table tr').not('tr:first').remove();
		updateTotal();
	});
<? endif;?>
	
	$('div.table_row_tool a.delete').click(function(){
		$(this).parents('tr:first').remove();
		updateTotal();
	});
	
	$('#return_a_book').click( function(){
<? if ($needConfirm):?>
	if ($('#confirm_div').css('display') == 'none') {
		$('#return_table tr').not('tr:first').remove();
		updateTotal();
		$('#total_return').css("visibility", "hidden");
		$('#confirm_div').css({'display':'block'});
	}
<? endif;?>		
//		$('#input_code_text').val($('#input_code_text').val().toUpperCase().replace(" ",''));
		$('#input_code_text').val($('#input_code_text').val().toUpperCase().replace(/^\s+|^　+|\s+$|　+$/gm,''));
		var codemod = ($('#input_code_text').val()).replace(/[!"#$%&\'()*+,.\/:;<=>?@[\]^`{|}~]/g,'-');
		if (codemod!=''){
			if ($('tr#'+codemod).length ==0){
		
				$.getJSON(
						'return_ajax_action.php',
						{ 
							barcode : $('#input_code_text').val() ,
							ID : $('#return_table tr').length,
							Mode: $('#Mode').val()
						},
						'JSON'
				)
				.success(return_ajax_handler)
				.error(function(){ alert('Error on AJAX call!'); });

			}else{
				alert("<?=$Lang['libms']['CirculationManagement']['msg']['scanned']?>");
			}
		}
		
		$('#input_code_text').val('');
		$('#input_code_text').focus();
	});
	
	$('#input_code_text').keyup(function(event) {
	  if (event.which == 13) {
		$('#return_a_book').click();
	  }  
	});
	
	$('#input_code_text').keypress(function(e){
	    if ( e.which == 13 ) {
			e.preventDefault();
		    return false;
	    }
	});
	
	$(document).bind('keydown',function(e){
		switch (e.keyCode)
		{
		    case 120:	// F9
				$('input#return_submit').click();
				return false;
		    default:
		      return true;
		}
	});
	
	$('#input_code_text').focus();
	updateTotal();

});

function updateTotal(){
	total = $('table#return_table tr.returned').length;
	$('.op_num em, .op_num_return em ').html(total);
}

function return_ajax_handler(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		var s = ajaxReturn.script;
		if (s != '') {
			s = s.replace("<script>","");
			s = s.replace("<\/script>","");
			eval(s);
		}
		else {		
			table = $('#return_table tbody');
			row = $(ajaxReturn.html);
			if (row) {
				table.find('tr:first').after(row);
				ajax_update_user_info();
				updateTotal();
				//if($('table#return_table').hasClass('alert_avaliable')){//The alert user to hold the book
				if($("#span_alert_avaliable")[0] && ajaxReturn.alertReservePerson){
					alert("<?=$Lang["libms"]["CirculationManagement"]["msg"]["Hold_Book"]?> "+$("#span_alert_avaliable1").text());
				}				
			}
			else {
				alert("<?=$Lang["libms"]["CirculationManagement"]["return_ajax"]["no_borrow_record"]?>");
			}
			$('#input_code_text').focus();
		}
	}
	else {
		if (ajaxReturn != null && ajaxReturn.LocationIncorrect) {
			alert('<?=$Lang["libms"]["Circulation"]["NoPermissionHandleBook"]?>');
		}
		else if (ajaxReturn != null && ajaxReturn.NotAllowClassManagementCirculation) {
			alert('<?=$Lang['libms']['CirculationManagement']['msg']['notAllowBorrowReturnClassManagementBooks']?>');
		}
		else if (ajaxReturn != null && ajaxReturn.NotAllowReturnWriteoffBook) {
			alert('<?=$Lang['libms']['CirculationManagement']['msg']['notAllowReturnWriteoffBooks']?>');
		}
		else if (ajaxReturn != null && ajaxReturn.BookItemStatus) {
			switch (ajaxReturn.BookItemStatus) {
				case 'SHELVING':
					alert('<?=$Lang['libms']['CirculationManagement']['msg']['book_item_status']['general'].$Lang["libms"]["book_status"]["SHELVING"]?>');
					break;
				case 'DAMAGED':
					alert('<?=$Lang['libms']['CirculationManagement']['msg']['book_item_status']['general'].$Lang["libms"]["book_status"]["DAMAGED"]?>');
					break;
				case 'REPAIRING':
					alert('<?=$Lang['libms']['CirculationManagement']['msg']['book_item_status']['general'].$Lang["libms"]["book_status"]["REPAIRING"]?>');
					break;
				case 'RESERVED':
					alert('<?=$Lang['libms']['CirculationManagement']['msg']['book_item_status']['general'].$Lang["libms"]["book_status"]["RESERVED"]?>');
					break;
				case 'RESTRICTED':
					alert('<?=$Lang['libms']['CirculationManagement']['msg']['book_item_status']['general'].$Lang["libms"]["book_status"]["RESTRICTED"]?>');
					break;
				case 'WRITEOFF':
					alert('<?=$Lang['libms']['CirculationManagement']['msg']['book_item_status']['general'].$Lang["libms"]["book_status"]["WRITEOFF"]?>');
					break;
				case 'SUSPECTEDMISSING':
					alert('<?=$Lang['libms']['CirculationManagement']['msg']['book_item_status']['general'].$Lang["libms"]["book_status"]["SUSPECTEDMISSING"]?>');
					break;
				case 'LOST':
					alert('<?=$Lang['libms']['CirculationManagement']['msg']['book_item_status']['general'].$Lang["libms"]["book_status"]["LOST"]?>');
					break;
				case 'ORDERING':
					alert('<?=$Lang['libms']['CirculationManagement']['msg']['book_item_status']['general'].$Lang["libms"]["book_status"]["ORDERING"]?>');
					break;
				case 'BORROWED':
					alert('<?=$Lang['libms']['CirculationManagement']['msg']['book_item_status']['general'].$Lang["libms"]["book_status"]["BORROWED"]?>');
					break;
				case 'NORMAL':
				default:
					alert('<?=$Lang['libms']['CirculationManagement']['msg']['book_item_status']['not_on_loan']?>');
					break;
			}
		}
		else if (ajaxReturn != null) {
			table = $('#return_table tbody');
			if (ajaxReturn.html != '') {
				row = $(ajaxReturn.html);
			}
			else {
				row = '';
			}			
			if (row) {
				table.find('tr:first').after(row);
				ajax_update_user_info();
				updateTotal();
			}
			else {
				alert("<?=$Lang["libms"]["CirculationManagement"]["return_ajax"]["no_borrow_record"]?>");
			}
			$('#input_code_text').focus();
		}
		else {
			alert('<?=$Lang['libms']['CirculationManagement']['msg']['n_barcode']?>');
		}
	}
}

<? if ($needConfirm):?>
function after_confirm_return_books(ajaxReturn) {
	$('#reset').click();
	return_ajax_handler(ajaxReturn);
	$('#total_return').css("visibility", "visible");
	$('#confirm_div').css({'display':'none'});
}
<? endif;?>

function checkform()
{
	if ($('#return_table tr.returned').length<=0) {
		alert("<?=$Lang['libms']['CirculationManagement']['msg']['no_record_to_proceed']?>");
		$('#input_code_text').focus();
		return false;
	}
	else {
		return true;
	}
}

</script>

<? if ($needConfirm):?>
<form name="return_form" id="return_form">
	<input type="hidden" name="ConfirmReturn" id="ConfirmReturn" value="">
	<input type="hidden" name="Mode" id="Mode" value="Individual">
<? endif;?>

<div class="table_board">
		<div class="enter_code"
			<?=$sys_custom['eLibraryPlus']['Circulation']['BarcodeSearchBarInLeft'] ? 'style="float:left;"':''?>><?=$Lang['libms']['CirculationManagement']['barcode']?> <input
				name="" type="text" id='input_code_text' class="input_code_text" />
			<input name="submit2" id='return_a_book' type="button"
				class="formsubbutton"
				value="&crarr; <?=$Lang["libms"]["btn"]["enter"]?>" />
		</div>
		<div class="op_num_return" id="total_return" style="visibility:<?=$needConfirm ? 'hidden' : 'visible'?>;">
			<span><?=$Lang["libms"]["CirculationManagement"]["no_return"]?> :</span><em>
				0</em>
		</div>

		<table id="return_table" class="common_table_list">
			<tbody>
				<tr>
					<th class="num_check">#</th>
					<th><?=$Lang['libms']['CirculationManagement']['booktitle']?></th>
					<th><?=$Lang['libms']['CirculationManagement']['borrowday']?></th>
					<th><?=$Lang["libms"]["CirculationManagement"]["borrowed_by"]?></th>
					<th><?=$Lang['libms']['CirculationManagement']['dueday']?></th>
					<th><?=$Lang['libms']['CirculationManagement']['overdue_day']?></th>
					<th nowrap="nowrap"><?=$Lang['libms']['CirculationManagement']['payment']?></th>
					<th><?=$Lang['libms']['CirculationManagement']['location']?></th>
					<th><?=$Lang['libms']['CirculationManagement']['booked']?></th>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="op_btn">

		<div class="form_btn"></div>
	</div>

<? if ($needConfirm):?>		  
		<div class="op_btn" id="confirm_div" style="display: block;">
		<div class="op_num">
			<span><?=$Lang["libms"]["CirculationManagement"]["no_to_return"]?> :</span><em
				id='returnCount'>0</em>
		</div>
		<div class="form_btn">
			<input name="submit2" id='return_submit' type="button"
				class="formbutton"
				value="<?=$Lang['libms']['CirculationManagement']['confirm'].$Lang["libms"]["CirculationHotKeys"]["Confirm"]?>" />
			<input id="reset" type="reset" class="formbutton"
				value="<?=$Lang['libms']['CirculationManagement']['cancel'].$Lang["libms"]["CirculationHotKeys"]["Cancel"]?>" />
		</div>
	</div>
</form>
<? endif;?>
		  
<?=	$interface->FOCUS_ON_LOAD("input_code_text") ?><?php
$interface->LAYOUT_STOP();
intranet_closedb();
?>
