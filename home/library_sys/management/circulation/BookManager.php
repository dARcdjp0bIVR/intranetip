<?php


## modifying by:  

/**
 * Log :
 * 
 * Date     2018-09-10 [Cameron]
 *          - fix: should retrieve CirculationTypeCode from LIBMS_BOOK if that in LIBMS_BOOK_UNIQUE is null or empty in getAllItemCirculationTypeCode()
 *          - add function getNumberOfBooksByCirculationTypeCode()
 * 
 * Date		2018-01-31 [Cameron]
 * 			- show ItemSeriesNum after Book Title in getBookFields() [case #L130405]
 * 
 * Date		2017-09-07 [Cameron]
 * 			add function isBookAvailable4Reserve()
 * 
 * Date		2015-08-24 [Cameron]
 * 			retrieve BarCode from LIBMS_BOOK_UNIQUE in getBookFields()
 * 
 * Date		2015-07-13 [Cameron]
 * 			retrieve AccompanyMaterial in getBookFields()
 * 
 * Date		2015-04-28 [Henry]
 * 			modified getAllItemCirculationTypeCode() to allow reserve on display book
 * 
 * Date		2014-07-30 [Henry]
 * 			Override the RemarkToUser in book if the item have RemarkToUser
 * 
 * Date		2013-09-02 [Yuen]
 * 			Updated getBookFields() for getting book item information from LIBMS_BOOK_UNIQUE table   
 */



if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 

$WRT_ROOT= "{$_SERVER['DOCUMENT_ROOT']}/";
include_once($WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($WRT_ROOT."includes/libdb.php");
include_once($WRT_ROOT."includes/pdump/pdump.php");

global $Lang;
class BookManager {
	
	var $libms;
	public function  __construct($libms){
		//global $eclass_prefix;
		# database for this module
		//$this->db = $eclass_prefix . "eClass_LIBMS";
		if (!empty($libms)){
			$this->libms = $libms;
		}else{
			$this->libms = new liblms();
		}
	}
	
	public function getBookIDFromUniqueBookID($uniqueBookID){
	    if (empty($uniqueBookID)){
		    return false;
	    }
	    $sql = "SELECT `BookID`
	    		FROM `LIBMS_BOOK_UNIQUE` b 
	    		WHERE `UniqueID`='{$uniqueBookID}'
	    		LIMIT 1";
	    $result = $this->libms->returnArray($sql);
	    
	    if (!empty($result)){
			return $result[0]['BookID'];
	    }else{
			return false;
	    }
	}
	
	
	public function getBookIDFromCallNumOrBookCode ($searchFor){
	    if (empty($searchFor)){
		return false;
	    }
	    $sql = "SELECT `BookID` FROM `LIBMS_BOOK` WHERE `CallNum`='{$searchFor}' OR `BookCode`='{$searchFor}' LIMIT 1";
	    $result = $this->libms->returnArray($sql);
    
	    if (!empty($result)){
		return $result[0]['BookID'];
	    }else{
		return false;
	    }
	}
	
	public function getCirculationTypeCodeFromUniqueBookID($uniqueBookID){
		if (empty($uniqueBookID)){
			return false;
		}
		$sql = "SELECT if (bu.CirculationTypeCode='' OR bu.CirculationTypeCode is NULL, b.CirculationTypeCode, bu.CirculationTypeCode) as CirculationTypeCode 
				FROM `LIBMS_BOOK_UNIQUE` bu 
				JOIN `LIBMS_BOOK` b 
				ON	b.`BookID`=bu.`BookID`
				WHERE bu.`UniqueID`='{$uniqueBookID}' LIMIT 1";
	    $result = $this->libms->returnArray($sql);
	    //error_log(mysql_error());
	    if (!empty($result)){
			return $result[0]['CirculationTypeCode'];
		}else{
			return false;
		}
		
	}
	
	public function getCirculationTypeCodeFromBookID($bookID){
		if (empty($bookID)){
			return false;
		}
		$sql = "SELECT b.`CirculationTypeCode` 
				FROM `LIBMS_BOOK` b
				WHERE `BookID`='{$bookID}' LIMIT 1";
	    $result = $this->libms->returnArray($sql);
	    //error_log(mysql_error());
	    if (!empty($result)){
			return $result[0]['CirculationTypeCode'];
		}else{
			return false;
		}
		
	}
	
	public function getUniqueBookIDFormBarcode($BarCode){
		if (empty($BarCode)){
			return false;
		}
		$sql = "SELECT `UniqueID` FROM `LIBMS_BOOK_UNIQUE` WHERE `BarCode`='{$BarCode}' LIMIT 1";
	    $result = $this->libms->returnArray($sql);

	    if (!empty($result)){
			return $result[0]['UniqueID'];
		}else{
			return false;
		}
	}
	
	public function getUniqueBookRecordStatus($uniqueBookID){
		if (empty($uniqueBookID)){
			return false;
		}
		$sql = "SELECT `RecordStatus` FROM `LIBMS_BOOK_UNIQUE` WHERE `UniqueID`='{$uniqueBookID}' LIMIT 1";
	    $result = $this->libms->returnArray($sql);

	    if (!empty($result)){
			return $result[0]['RecordStatus'];
		}else{
			return false;
		}
	}
	//Henry Added
	public function getUniqueBookRecordStatusByBookID($bookID){
		if (empty($bookID)){
			return false;
		}
		$sql = "SELECT bu.`RecordStatus` FROM `LIBMS_BOOK_UNIQUE` bu JOIN `LIBMS_BOOK` b ON bu.BookID = b.BookID WHERE bu.`BookID`='{$bookID}'";
	    $result = $this->libms->returnArray($sql);

	    if (!empty($result)){
			return $result;
		}else{
			return false;
		}
	}
	public function getBookFields($bookID, $fields='*', $uniqueBookID=""){
		if (empty($bookID)){
			return false;
		}
		//$sql = "SELECT * FROM `LIBMS_BOOK` WHERE `BookID`='{$bookID}' LIMIT 1";
		
	    $result = $this->libms->SELECTFROMTABLE('LIBMS_BOOK',$fields,array('BookID' => $bookID),1);
	    if (!empty($result))
	    {
		    $resultCombined = $result[0];
	    	if ($uniqueBookID!="")
	    	{
		    	$rows = $this->libms->SELECTFROMTABLE('LIBMS_BOOK_UNIQUE',"*",array('UniqueID' => $uniqueBookID),1);
		    	
		    	# load the info from Unique Table instead since Aug 2013!
		    	if (!empty($rows[0]["ItemSeriesNum"]) && $rows[0]["ItemSeriesNum"] != '1') {
		    		$resultCombined['BookTitle'] = $resultCombined['BookTitle'] . ' ' . $rows[0]["ItemSeriesNum"];  
		    	}
		    	$resultCombined["BookCode"] = $rows[0]["ACNO"];
		    	$resultCombined[1] = $rows[0]["ACNO"];
		    	$resultCombined["RecordStatus"] = $rows[0]["RecordStatus"];
		    	$resultCombined[9] = $rows[0]["RecordStatus"];
		    	$resultCombined["InvoiceNumber"] = $rows[0]["InvoiceNumber"];
		    	$resultCombined[12] = $rows[0]["InvoiceNumber"];
		    	$resultCombined["PurchaseDate"] = $rows[0]["PurchaseDate"];
		    	$resultCombined[24] = $rows[0]["PurchaseDate"];
		    	$resultCombined["Distributor"] = $rows[0]["Distributor"];
		    	$resultCombined[25] = $rows[0]["Distributor"];
		    	$resultCombined["PurchaseNote"] = $rows[0]["PurchaseNote"];
		    	$resultCombined[26] = $rows[0]["PurchaseNote"];
		    	$resultCombined["PurchaseByDepartment"] = $rows[0]["PurchaseByDepartment"];
		    	$resultCombined[27] = $rows[0]["PurchaseByDepartment"];
		    	$resultCombined["ListPrice"] = $rows[0]["ListPrice"];
		    	$resultCombined[28] = $rows[0]["ListPrice"];
		    	$resultCombined["Discount"] = $rows[0]["Discount"];
		    	$resultCombined[29] = $rows[0]["Discount"];
		    	$resultCombined["PurchasePrice"] = $rows[0]["PurchasePrice"];
		    	$resultCombined[30] = $rows[0]["PurchasePrice"];
		    	$resultCombined["LocationCode"] = $rows[0]["LocationCode"];
		    	$resultCombined[36] = $rows[0]["LocationCode"];
		    	$resultCombined["Item_CirculationTypeCode"] = $rows[0]["CirculationTypeCode"];
		    	//Override the RemarkToUser in book if the item have RemarkToUser
		    	if(!empty($rows[0]["RemarkToUser"]))
		    		$resultCombined['RemarkToUser'] = $rows[0]["RemarkToUser"];
				$resultCombined["AccompanyMaterial"] = $rows[0]["AccompanyMaterial"];		    		
				$resultCombined["UniqueBarCode"] = $rows[0]["BarCode"];
	    	}

			return $resultCombined;
		}else{
			return false;
		}
	}
	
	
	
	public function getAllItemCirculationTypeCode($bookID){
	    if (is_array($bookID)) {
	        $bookIDCond = implode("','",$bookID); 
	    }
	    else if ($bookID){
	        $bookIDCond = $bookID;
	    }
	    else {
	        $bookIDCond = '';
	    }
	    
	    if ($bookIDCond != '') {
    		$sql = "SELECT  bu.UniqueID, 
                            IF(bu.CirculationTypeCode IS NULL OR bu.CirculationTypeCode='', b.CirculationTypeCode, bu.CirculationTypeCode) AS CirculationTypeCode, 
                            bu.RecordStatus
    				FROM    LIBMS_BOOK_UNIQUE AS bu
                    INNER JOIN  LIBMS_BOOK b ON b.BookID=bu.BookID
                    WHERE   bu.BookID IN ('".$bookIDCond."')  
                    AND     bu.RecordStatus IN ('NORMAL', 'BORROWED', 'RESERVED', 'RESTRICTED') ";
    		$codesArray =  $this->libms->returnResultSet($sql);
    		
	    }
	    else {
	        $codesArray = array();
	    }
		return $codesArray;
	}
	

	
	public function getBookRecordStatus($bookID){
		if (empty($bookID)){
			return false;
		}
		$sql = "SELECT `RecordStatus` FROM `LIBMS_BOOK` WHERE `BookID`='{$bookID}' LIMIT 1";
	    $result = $this->libms->returnArray($sql);
	    if (!empty($result)){
			return $result[0]['RecordStatus'];
		}else{
			return false;
		}
	}
	
	public function setUniqueBookRecordStatus($uniqueBookID, $data =''){
		if (empty($uniqueBookID)){
			return false;
		}
		$sql = "UPDATE `LIBMS_BOOK_UNIQUE` SET `RecordStatus` = '{$data}' WHERE `UniqueID`='{$uniqueBookID}' LIMIT 1";
	    $result = $this->libms->db_db_query($sql);
	    return $result;
	}
	
	public function setBookRecordStatus($bookID, $data =''){
		if (empty($bookID)){
			return false;
		}
		$sql = "UPDATE `LIBMS_BOOK_UNIQUE` SET `RecordStatus` = '{$data}' WHERE `BookID`='{$bookID}' LIMIT 1";
	    $result = $this->libms->db_db_query($sql);
	    return $result;
	}
	
	
	public function setBookNumOfCopyAvailiable($bookID, $data =''){
	    if (empty($bookID)){
		    return false;
	    }
	    $sql = "UPDATE `LIBMS_BOOK` SET `NoOfCopyAvailiable` = '{$data}' WHERE `BookID`='{$bookID}' LIMIT 1";
	    $result = $this->libms->db_db_query($sql);
	    return $result;
	}
	
	public function isAvaiable($BookID){
	   	if (empty($BookID)){
		    return false;
	    }
	    $sql = "SELECT `UniqueID` FROM `LIBMS_BOOK_UNIQUE` 
		    WHERE 
			`BookID`='{$BookID}' AND 
			`RecordStatus` IN ('ACTIVE','SHEVLING','NORMAL') 
		    ORDER BY RAND() 
		    LIMIT 1";
	    $result = $this->libms->returnArray($sql);
    
	    return (empty($result)?FALSE:$result[0][0]);
	}
	
	// check if there's still available book unique for reservation if a book item has been write-off
	public function isBookAvailable4Reserve($bookID) {
		if (empty($bookID)) {
			return false;
		}
		$sql = "SELECT 
						UniqueID
				FROM
						LIBMS_BOOK_UNIQUE
				WHERE
						BookID='$bookID'
				AND
						RecordStatus IN ('SHELVING','NORMAL','BORROWED','RESERVED')
				LIMIT 1";
		$result = $this->libms->returnResultSet($sql);
		return (count($result)) ? true: false;		
	}

	public function getNumberOfBooksByCirculationTypeCode($bookIDAry){
	    if (is_array($bookIDAry)) {
	        $bookIDCond = implode("','",$bookIDAry);
	    }
	    else if ($bookIDAry){
	        $bookIDCond = $bookIDAry;
	    }
	    else {
	        $bookIDCond = '';
	    }
	    
	    $circulationCodeAssoc = array();
	    if ($bookIDCond != '') {
	        $sql = "SELECT  COUNT(b.BookID) AS NumberOfBook,
                            IF(bu.CirculationTypeCode IS NULL OR bu.CirculationTypeCode='', b.CirculationTypeCode, bu.CirculationTypeCode) AS CirculationTypeCode
    				FROM    LIBMS_BOOK_UNIQUE AS bu
                    INNER JOIN  LIBMS_BOOK b ON b.BookID=bu.BookID
                    WHERE   bu.BookID IN ('".$bookIDCond."')
                    AND     bu.RecordStatus IN ('NORMAL', 'BORROWED', 'RESERVED', 'RESTRICTED') 
                    GROUP BY IF(bu.CirculationTypeCode IS NULL OR bu.CirculationTypeCode='', b.CirculationTypeCode, bu.CirculationTypeCode)";
	        $codesArray =  $this->libms->returnResultSet($sql);
	        if (count($codesArray)) {
	            $circulationCodeAssoc = BuildMultiKeyAssoc($codesArray,array('CirculationTypeCode'),array('NumberOfBook'),1);
	        }
	        
	    }
	    return $circulationCodeAssoc;
	}
	
}