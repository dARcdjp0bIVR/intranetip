<?php
// Editing by
/*
 * 2018-02-22 (Cameron)
 * - fix: add meta characterset (utf-8) to show no access right message
 *
 * 2017-08-01 (Cameron)
 * - fix bug: should initialize $result, otherwize it causes "wrong datatype for second argument in ..." error when used in PowerClass
 *
 * 2017-03-14 (Cameron)
 * - add IP checking for circulation
 *
 * 2016-08-31 (Cameron)
 * - allow to settle zero amount overdue by calling settle_zero_overdue to [case #L102005]
 *
 * 20150722 Henry: apply class management permission
 * 2013-09-02 (Carlos): if $plugin['payment'] && $sys_custom['eLibraryPlus']['PayBy_ePayment'], use ePayment to pay overdue payment
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

// ###### Trim all request #######
function recursive_trim(&$array)
{
    foreach ($array as &$item) {
        if (is_array($item)) {
            recursive_trim($item);
        } else {
            if (is_string($item))
                $item = trim($item);
        }
    }
}
recursive_trim($_REQUEST);
// ###### END OF Trim all request #######
$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/pdump/pdump.php");
include_once ('User.php');
include_once ('BookManager.php');
include_once ('RightRuleManager.php');
include_once ('TimeManager.php');
include_once ('User.php');

intranet_auth();
intranet_opendb();

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);

$global_lockout = (bool) $libms->get_system_setting('global_lockout');
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['overdue'] || $global_lockout))) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

include_once ('TimeManager.php');
$TM = new TimeManager();
if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! $TM->isOpenNow(time()) && ! $c_right['circulation management']['access_without_open_hour']) {
    header("Location: index.php");
}

$payings = $_REQUEST['paying'];
$waiveds = $_REQUEST['waived'];

$pay_by_ePayment = $plugin['payment'] && $sys_custom['eLibraryPlus']['PayBy_ePayment'];
// $pay_by_ePayment = false;
// if($pay_by_ePayment) {
$PayByPayment = $_REQUEST['PayByPayment'];
// }

// $interface = new interface_html("libms_circulation.html");
$GLOBALS['LIBMS']['PAGE'] = "OVERDUE";

$uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];

$User = new User($uID, $libms);
// $BookManager = new BookManager($libms);

$result = array();
if (! empty($payings))
    foreach ($payings as $overdueID => $paying) {
        $paying = floatval($paying);
        $overdueID = floatval($overdueID);
        if (! empty($paying)) {
            // if($pay_by_ePayment) {
            if (isset($PayByPayment[$overdueID]) && ! isset($waiveds[$overdueID])) {
                $result[$overdueID] = $User->pay_overdue($paying, $overdueID, $pay_by_ePayment);
            }
            // }else{
            // $result[$overdueID]=$User->pay_overdue($paying, $overdueID);
            // }
        } else {
            if (isset($PayByPayment[$overdueID]) && ! isset($waiveds[$overdueID])) {
                $result[$overdueID] = $User->settle_zero_overdue($overdueID);
            }
        }
    }

if (! empty($waiveds))
    foreach ($waiveds as $overdueID => $waived) {
        
        $overdueID = floatval($waived);
        // dump($overdueID);
        $result[$overdueID] = $User->wavie_overdue($overdueID);
    }
$error = in_array(false, $result);
if ($error)
    $_SESSION['LIBMS']['xmsg'] = $Lang['libms']['Circulation']['action_fail'];
else
    $_SESSION['LIBMS']['xmsg'] = $Lang['libms']['Circulation']['action_success'];

header('Location: overdue.php');

?>

<script type="text/javascript">
<!--
window.location = "overdue.php"
//-->
</script>
