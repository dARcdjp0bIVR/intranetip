<?php
/*
 * 	Log
 *
 *  2018-10-02 [Cameron]
 *      - add TransAmountBefore and TransAmountAfter to LIBMS_BALANCE_LOG [case #T139412]
 *      
 * 	2017-11-01 [Cameron]
 * 		- fix: should update Balance in LIBMS_USER when adjust additional charge amount for lost book [case #F130172]
 * 
 * 	2017-03-14 [Cameron]
 * 		Add IP checking for circulation
 * 
 * 	2016-09-06 Cameron	- create this file
 *  
 */

 
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right,$c_m_right);
$global_lockout = (BOOL)$libms->get_system_setting('global_lockout');
if (!$_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && !($c_right['circulation management']['overdue'] || $global_lockout)))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$json['success'] = false;
$result = array();

$OverDueLogID = $_POST['OverDueLogID'];
$additional_charge = $_POST['additional_charge'];

$condition = array('OverDueLogID'=>$OverDueLogID);

if ($OverDueLogID) {
	$sql = "select
						d.OverdueLogID,
					 	d.Payment,
						d.AdditionalCharge,
						d.AdditionalChargeBy,
                        d.PaymentReceived,
						br.UserID,
						u.Balance
			from 
						LIBMS_OVERDUE_LOG d 
			inner join 
						LIBMS_BORROW_LOG br on br.BorrowLogID=d.BorrowLogID
			inner join
						LIBMS_USER u on u.UserID=br.UserID
			where 
						d.OverDueLogID='".$OverDueLogID."'";
	
	$rs = $libms->returnResultSet($sql);
	
	//$rs = $libms->SELECTFROMTABLE('LIBMS_OVERDUE_LOG',$fields='*',$condition);
	
	if (count($rs) > 0) {
		$rs = current($rs);
		$dataAry = array();
		$dataAry['OverDueLogID'] = $OverDueLogID;
		$dataAry['FromAmount'] = $rs['AdditionalCharge'];
		$dataAry['ToAmount'] = $additional_charge;
		$dataAry['AdditionalChargeBy'] = $rs['AdditionalChargeBy'];
		
		$libms->Start_Trans();
		# 1. add change log	
		$result[] = $libms->INSERT2TABLE('LIBMS_OVERDUE_CHANGE_LOG',$dataAry);
		unset($dataAry);
		
		# 2. update overdue log
		$dataAry['Payment'] =  $rs['Payment'] - $rs['AdditionalCharge'] + $additional_charge;
		$dataAry['AdditionalCharge'] = $additional_charge;
		$result[]= $libms->UPDATE2TABLE('LIBMS_OVERDUE_LOG',$dataAry,$condition);
		unset($dataAry);
			
		# 3. update balance for the user
		$difference = $rs['AdditionalCharge'] - $additional_charge;
		if ($difference == '') {
			$difference = 0;
		}
		$sql = "UPDATE LIBMS_USER SET Balance=Balance+$difference WHERE UserID='".$rs['UserID']."'";
		$result[]= $libms->db_db_query($sql);
	
		# 4. add balance log
		$transAmountBefore = $rs['Payment'] - $rs['PaymentReceived'];
		$transAmountAfter = $transAmountBefore + $difference;
		$balanceLogAry = array();
		$newBalance = $rs['Balance']+$difference;
		$balanceLogAry['UserID'] = PHPToSQL($rs['UserID']);
		$balanceLogAry['BalanceBefore'] = PHPToSQL($rs['Balance']);
		$balanceLogAry['TransAmount'] = PHPToSQL($difference);
		$balanceLogAry['BalanceAfter'] = PHPToSQL($newBalance);
		$balanceLogAry['TransDesc'] = PHPToSQL('additional charge adjustment');
		$balanceLogAry['TransType'] = PHPToSQL('OverdueLog');
		$balanceLogAry['TransRefID'] = PHPToSQL($OverDueLogID);
		$balanceLogAry['TransAmountBefore'] = PHPToSQL($transAmountBefore);
		$balanceLogAry['TransAmountAfter'] = PHPToSQL($transAmountAfter);
		$result[] = $libms->INSERT2TABLE('LIBMS_BALANCE_LOG',$balanceLogAry);
		unset($balanceLogAry);
		
		# 5. commit / rollback transaction	
	   	if (!in_array(false,$result)) {
	   		$libms->Commit_Trans();
			$json['success'] = true;		
			$json['additional_charge'] = $additional_charge;
			$json['new_balance'] = ($newBalance < 0 ? '<em class="alert_text">' : '<em>').'$'.(-$newBalance).'</em>';
	   	}
	   	else {
	   		$libms->RollBack_Trans();
	   	}
			
	}
}

echo json_encode($json);
 
intranet_closedb();


?>