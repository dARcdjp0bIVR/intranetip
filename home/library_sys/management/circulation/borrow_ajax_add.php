<?php

// modifying by: 

/**
 * ***********************************************************
 *
 * 20190614 (Cameron)
 * - fix: don't show colon if BookSubTitle is empty
 * 
 * 20190423 (Henry)
 * - added IntegerSafe to prevent SQL injection
 * 
 * 20190305 (Cameron)
 * - show BookSubTitle if $sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] is true [case #K154500]
 * 
 * 20181114 (Cameron)
 * - get number of loans of the book by the user and pass it to $view_data [case #Y150892],
 *      then show it before confirm to borrow the book in gen_borrow_html_row() if the control flag is set
 * 
 * 20180516 (Cameron)
 * - fix: should reserve space between 'active' and 'id' in gen_borrow_html_row() so that jquery can use it as selector [case #W139445]
 * 
 * 20180221 (Cameron)
 * - modified the condition to show LocationIncorrect alert [case #E129637]
 * - fix: add meta characterset (utf-8) to show no access right message
 *
 * 20170802 (Cameron)
 * - fix bug: apply remove_dummy_chars_for_json() to gen_borrow_html_row() to support back slash in book title in php5.4
 *
 * 20170314 (Cameron)
 * - add IP checking for circulation
 *
 * 20151201 (Cameron)
 * - add row_id class for re-order row number
 *
 * 20151105 (Cameron)
 * - fix bug on showing case sensitive BarCode ($bookInfo['UniqueBarCode']) in gen_borrow_html_row()
 *
 * 20151030 (Cameron)
 * - fix bug on showing accompany material that contains back slash in jConfirm
 *
 * 20151015 (Cameron)
 * - set focus to input_code_text field after user click Yes / No button about borrow accompany material or not
 *
 * 20150817 {Cameron}
 * - show accompany material name above prompt question
 * - show "accompany material" in bracket after the name in listview
 *
 * 20150814 (Cameron)
 * - add prompt confirm (gen_confirm_script()) if user want to borrow accompany material or not if circulation_borrow_return_accompany_material
 * is true and the book item has accompany material
 *
 * 20150713 (Cameron)
 * - show AccompanyMaterial if circulation_display_accompany_material is set
 * - special handle backslash for return json
 *
 * 20150609 (Henry)
 * apply class management permission
 *
 * 20140731 (Henry)
 * overwriting the remark for user when item have RemarkForUser
 *
 * 20131015 (Yuen)
 * support due date calculation by counting school days only!
 *
 * ***********************************************************
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

// ###### Trim all request #######
function recursive_trim(&$array)
{
    foreach ($array as &$item) {
        if (is_array($item)) {
            recursive_trim($item);
        } else {
            if (is_string($item))
                $item = trim($item);
        }
    }
}
recursive_trim($_REQUEST);

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/pdump/pdump.php");
include_once ($PATH_WRT_ROOT . "lang/libms.lang.$intranet_session_language.php");
include_once ('User.php');
include_once ('BookManager.php');
include_once ('RightRuleManager.php');
include_once ('TimeManager.php');
include_once ('User.php');

$barcode = $_REQUEST['barcode'];
$ID = IntegerSafe($_REQUEST['ID']);

intranet_auth();

intranet_opendb();

$libms = new liblms();
$c_right = $_SESSION['LIBMS']['admin']['current_right'];
$c_m_right = $_SESSION['LIBMS']['admin']['class_mgt_right'];
$c_right = array_merge($c_right, $c_m_right);
if (! $_SESSION['LIBMS']['admin']['check_ip_for_circulation'] || (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] && ! ($c_right['circulation management']['borrow']))) {
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// $interface = new interface_html("libms_circulation.html");
$GLOBALS['LIBMS']['PAGE'] = "BORROW";

$uID = $_SESSION['LIBMS']['CirculationManagemnt']['UID'];

$User = new User($uID, $libms);
$BookManager = new BookManager($libms);

$json['success'] = FALSE;

$bookUniqieID = $BookManager->getUniqueBookIDFormBarcode($barcode);
if (empty($bookUniqieID)) {
    output($json);
} else {
    if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']) {   // eLib+ admin can return
        $isAllowToHandleItem = $libms->get_class_mgt_can_handle_item($bookUniqieID);
        $isLocationCodeInClassManagementGroup = false;
        if (is_array($isAllowToHandleItem) && count($isAllowToHandleItem) == 0) {
            $bookItem = $libms->GET_BOOK_ITEM_INFO($bookUniqieID);
            if (count($bookItem)) {
                $locationCode = $bookItem[0]['LocationCode'];
                if (! empty($locationCode)) {
                    $isLocationCodeInClassManagementGroup = $libms->isLocationCodeInClassManagementGroup($locationCode, $isClassManagementUser = false);
                }
            }
        }
        $isGroupManagementBorrow = $_SESSION['LIBMS']['admin']['current_right']['circulation management']['borrow'];
        $isAllowClassManagementCirculation = ! $_SESSION['LIBMS']['admin']['current_right']['circulation management']['not_allow_class_management_circulation'];
        $isClassManagementBorrow = $c_m_right['circulation management']['borrow'];
        
        if (($isGroupManagementBorrow && ! $isAllowClassManagementCirculation && ! $isClassManagementBorrow && $isLocationCodeInClassManagementGroup) || ($isClassManagementBorrow && ! $isAllowToHandleItem)) {
            
            $json['LocationIncorrect'] = TRUE;
            
            output($json); // henry added
        } else {
            $json['success'] = TRUE;
        }
    }
    else {
        $json['success'] = TRUE;
    }
}

$MSG = '';
$canBorrow = $User->rightRuleManager->canBorrow($bookUniqieID, $MSG);

$bookID = $BookManager->getBookIDFromUniqueBookID($bookUniqieID);
// $bookInfo = $BookManager->getBookFields($bookID);
$bookInfo = $BookManager->getBookFields($bookID, '*', $bookUniqieID); // for overwriting the remark for user when item have RemarkForUser

$uniqueBookStatus = $BookManager->getUniqueBookRecordStatus($bookUniqieID);

// cater circulation type set in item level
$CirculationTypeApplied = (trim($bookInfo['Item_CirculationTypeCode']) != "") ? $bookInfo['Item_CirculationTypeCode'] : $bookInfo['CirculationTypeCode'];
$limits = $User->rightRuleManager->get_limits($CirculationTypeApplied);

$timeManager = new TimeManager();

if ($strToday == "") {
    $strToday = date("Y-m-d");
}

// special customization for a KIS client tbcpk
if ($sys_custom['eLibraryPlus']['fixed_return_day_on_weekday'] != "" && isset($sys_custom['eLibraryPlus']['fixed_return_day_on_weekday'])) {
    $weekday_today = date("w");
    if ($weekday_today < $sys_custom['eLibraryPlus']['fixed_return_day_on_weekday']) {
        $daysDifference = $sys_custom['eLibraryPlus']['fixed_return_day_on_weekday'] - $weekday_today;
    } else {
        $daysDifference = 7 + $sys_custom['eLibraryPlus']['fixed_return_day_on_weekday'] - $weekday_today;
    }
    
    $limits['ReturnDuration'] = $daysDifference;
}

if ($libms->get_system_setting("circulation_duedate_method")) {
    $dueDate = $timeManager->findOpenDateSchoolDays(strtotime($strToday), $limits['ReturnDuration']);
} else {
    $dueDate = $timeManager->findOpenDate(strtotime("{$strToday} + {$limits['ReturnDuration']} days"));
}

// $barcode=$timeManager->patron_dates["startdate"];

$reserves = $User->recordManager->getReservationByBookID($bookID, array(
    'WAITING',
    'READY'
), "asc", $uID);
if ($reserves["ReserveTime"] != "") {
    $DateReservedOn = date("Y-m-d", strtotime($reserves["ReserveTime"]));
}

$searchChar = '/[!"#$%&\'()*+,.\/:;<=>?@[\]^`{|}~ ]/';

$numberOfLoans = $User->recordManager->getNumberOfLoansByBook($uID, $bookID);
    
$view_data = compact('bookInfo', 'searchChar', 'barcode', 'canBorrow', 'bookUniqieID', 'dueDate', 'MSG', 'uniqueBookStatus', 'DateReservedOn', 'CirculationTypeApplied', 'numberOfLoans');
if (($libms->get_system_setting("circulation_borrow_return_accompany_material")) && (! empty($bookInfo['AccompanyMaterial']))) {
    $json['script'] = gen_confirm_script($view_data);
} else {
    $json['script'] = null;
}

$json['html'] = gen_borrow_html_row($view_data, $ID);
$json['button'] = gen_html_button_force_burrow($view_data);

$json['limit'] = $limits['LimitBorrow'];
$json['CirculationTypeCode'] = $CirculationTypeApplied;

output($json);

// ###################################################################################
function output($json_data)
{
    echo json_encode($json_data);
    exit();
}

function gen_confirm_script($view_data)
{
    global $Lang, $bookUniqieID;
    extract($view_data);
    $accompanyMaterial = str_replace("\\", "\\\\\\\\", $bookInfo['AccompanyMaterial']);
    
    $x = '';
    ob_start();
    ?>
<script>
jConfirm("<?=$accompanyMaterial."<br>".$Lang["libms"]["Circulation"]["BorrowAccompayMaterial"]?>",
		 "<?=$Lang['libms']["settings"]['system']['circulation_borrow_return_accompany_material']?>", 
		 function(r){
		 	if (r) {
		 		$('#AccomMatID_' + '<?=$bookUniqieID?>').attr('checked',true);
		 	}
		 	$('#input_code_text').focus();
		 }, 
		 "<?=$Lang['libms']["settings"]['system']['yes']?>", 
		 "<?=$Lang['libms']["settings"]['system']['no']?>");
</script>
<?php
    $x = ob_get_contents();
    ob_end_clean();
    $x = str_replace("\n", '', $x);
    $x = str_replace("\t", '', $x);
    return $x;
}

function gen_html_button_force_burrow($view_data)
{
    global $Lang;
    
    extract($view_data);
    
    $x = '';
    $bookID = $bookInfo['BookID'];
    ob_start();
    
    ?>
<input name="submit2" type="button" class="set_borrow"
	value="<?=$Lang["libms"]["CirculationManagement"]["msg"]["f_borrow"]?>"
	onclick='allowBorrow(this,<?=$bookUniqieID?>)' />
<?php
    $x = ob_get_contents();
    ob_end_clean();
    $x = str_replace("\n", '', $x);
    $x = str_replace("\t", '', $x);
    
    return $x;
}

function gen_borrow_html_row($view_data, $ID)
{
    global $Lang, $libms, $sys_custom;
    
    extract($view_data);
    
    $x = '';
    $bookID = $bookInfo['BookID'];
    ob_start();
    
    // debug_r($bookInfo);
    
    if ($canBorrow) {
        $borrow_cases = '<span class="alert_avaliable">' . $Lang['libms']['CirculationManagement']['y_borrow'] . '</span>';
        if ($DateReservedOn != "") {
            $borrow_cases .= '<br /><span class="borrow_status alert_text">' . $Lang["libms"]["CirculationManagement"]["booked_alert"] . " [" . $DateReservedOn . "]" . '</span>';
        }
    } else {
        
        $borrow_cases = '<span class="borrow_status alert_text">' . $Lang['libms']['CirculationManagement']['n_borrow'] . '</span> &nbsp;' . $MSG;
    }
    
    $displayBookTitle = $bookInfo['BookTitle'];
    if ($sys_custom['eLibraryPlus']['Circulation']['ShowBookSubTitle'] && !empty($bookInfo['BookSubTitle'])) {
        $displayBookTitle .= " :" . $bookInfo['BookSubTitle'];
    }
    $displayBookTitle .= " [".$bookInfo['UniqueBarCode']."]";
    
    ?>
<tr borrow cirtype='<?=$CirculationTypeApplied?>'
	<?=($canBorrow)?' active ':' '?>
	id='<?=preg_replace($searchChar,'-',$barcode)?>' <?php echo ' '; ?>
	class='book_record <? if($canBorrow) echo ' avaliable_row ' ?>'>
	<td><span class="row_id"><?=$ID?></span><input type='hidden'
		value='<?=($canBorrow)?$bookUniqieID:''?>' name='bookUniqieID[]'
		class='bookUniqueID' /></td>
	<td><?=$displayBookTitle?></td>
	<td nowrap="nowrap"><?=date('Y-m-d')?></td>
	<td nowrap="nowrap"><?=date('Y-m-d',$dueDate)?></td>
	<td>
		<div class='burrow_status'>
	  	 	<?= $borrow_cases ?>
		</div>
		  <? if (!empty($bookInfo['RemarkToUser'])): ?>
		 		<br /> <span><img src="/images/2009a/eLibplus/icon_alert.png" /><?=$bookInfo['RemarkToUser']?></span>
		 <? endif; ?>
		  <? if ($libms->get_system_setting("circulation_display_accompany_material") && !empty($bookInfo['AccompanyMaterial'])): ?>
		 		<br /> <span><img src="/images/2009a/eLibplus/icon_alert.png" /><?=$bookInfo['AccompanyMaterial'].'['.$Lang["libms"]["book"]["AccompanyMaterial"].']'?></span>
		 <? endif; ?>
		 <?if (($libms->get_system_setting("circulation_borrow_return_accompany_material")) && (!empty($bookInfo['AccompanyMaterial']))): ?>
		 		<br /> <span><input type="checkbox"
			name="AccomMat_<?=$bookUniqieID?>" id="AccomMatID_<?=$bookUniqieID?>"
			value="checked" /><?=$Lang["libms"]["Circulation"]["BorrowWithAccompayMaterial"]?></span>
		 <? endif; ?>
		 
		 <?php if (($libms->get_system_setting("show_number_of_loans_when_borrow_book")) && ($numberOfLoans > 0)): ?>
		 	<br /> <span><img src="/images/2009a/eLibplus/icon_alert.png" /><?php echo $Lang['libms']['CirculationManagement']['numberOfLoans'] . ': ' . $numberOfLoans;?></span>
		 <?php endif;?>
		 
	  </td>
	<td>
		<div class="table_row_tool">
			<a href="#" class="delete" title="Delete"></a>
		 	<?
    
if ((! $canBorrow) && ($uniqueBookStatus != 'BORROWED')) :
        echo gen_html_button_force_burrow($view_data);
		 	   endif;
    
    ?>
	 	</div>
	</td>
</tr>

<?php
    $x = ob_get_contents();
    ob_end_clean();
    $x = remove_dummy_chars_for_json($x);
    
    return $x;
}
?>