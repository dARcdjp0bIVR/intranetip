<?php
/*
 * 	Log
 * 
 * 	2016-10-17 [Cameron]
 * 		- add $plugin['eLibraryPlusOne'], link to elibplus2 if it's not set, else link to elibplus
 */

if ($plugin['eLibraryPlusOne']) {
	header("location: ../eLearning/elibplus/");
}
else {
	header("location: ../eLearning/elibplus2/");
}

die();

?>