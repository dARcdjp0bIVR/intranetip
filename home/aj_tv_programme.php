<?
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampustv.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$lcampustv = new libcampustv();

$x = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";

if($channel_id==0)		# Live <= display schedule
{
	$programme_list = $lcampustv->programmeList;
	if(sizeof($programme_list))
	{
		foreach($programme_list as $key => $p)
		{
			$x .= "
					<tr>
						<td valign='top'>-</td>
						<td align=\"left\">".  $p[0] ."<BR>" . $p[1]."</td>
					</tr>
				";
		}	
	}
}
else					# Channel <= display programme
{
	$clips = $lcampustv->returnClips($channel_id);
	if(sizeof($clips))
	{
		foreach($clips as $key=>$data)
		{
			$x .= "
					<tr>
						<td valign='top'>-</td>
						<td align=\"left\"><a href=\"javascript:displayMovie(". $data['ClipID'] .");\" class=\"indextvlink\">".  $data['Title'] ."</a></td>
					</tr>
				";
		}
	}
	else
	{
		$x .= "
					<tr>
						<td valign='top'>". $Lang['General']['NoRecordFound'] ."</td>
					</tr>
				";	
	}
}
$x .= "</table>";

intranet_closedb();

echo $x;
?>




