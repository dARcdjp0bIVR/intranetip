<?php
// Editing by 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$lftp = new libftp();
if (isset($plugin['personalfile']) && $plugin['personalfile'])
{
/*
    if (!$lftp->isAccountExist($lu->UserLogin))
    {
         header ("Location: /home/school/close.php");
         exit();
    }
    */
}
else
{
    header("Location: close.php");
    exit();
}

$usedQuota = $lftp->getUsedQuota($lu->UserLogin,"iFolder");
$usedQuota = round($usedQuota,2);
$totalQuota = $lftp->getTotalQuota($lu->UserLogin,"iFolder");
if ($totalQuota == 0)
{
    $strtotalQuota = "$i_LinuxAccount_NoLimit";
}
else
{
    $strtotalQuota = "$totalQuota MBytes";
}

include_once($PATH_WRT_ROOT."templates/fileheader.php");
#echo "$i_LinuxAccount_UsedQuota: $usedQuota MBytes <br>$i_LinuxAccount_Quota: $strtotalQuota MBytes";
if ($totalQuota != 0)
{
    $pused = round($usedQuota/$totalQuota * 100);
    $left = $totalQuota - $usedQuota;
    if ($left < 0) $left = 0;
    if ($pused > 100) $pused = 100;


$storage = "<TABLE width=80% align=center><tr><td style=\"font-size:10px;\">".$pused."%</td>";
               $storage .= "<td width=100% class=td_left_middle>
<table cellpadding=0 border=0 cellspacing=0 style=\"border: #104a7b
1px solid; padding:1px; padding-right: 0px; padding-left: 0px;\" width=100%>
<tr>
<td width=100% class=td_left_middle bgcolor=white>
<div style=\"height:6px; width:$pused%; font-size:3px;
background-color:#CECFFF\"></div>
</td>
</tr>
</table>
</td>";
$storage .= "<td style=\"font-size:10px;\">100%</td></tr>";
               $storage .= "</table>";
}

?>
<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolorlight=#E8FBFC bordercolordark=#3BC0C6 class=body>
        <tr>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA><?=$i_LinuxAccount_UsedQuota?></td>
          <td class=td_center_middle bgcolor=#E8FBFC><?=$usedQuota?> MBytes</td>
        </tr>
        <tr>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA><?=$i_LinuxAccount_Quota?></td>
          <td class=td_center_middle bgcolor=#E8FBFC><?=$strtotalQuota?></td>
        </tr>
        <tr>
          <td colspan=2><?=$storage?></td>
        </tr>
</table>


<?php
include_once($PATH_WRT_ROOT."templates/filefooter.php");
intranet_closedb();
?>