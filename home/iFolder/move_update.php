<?php


/*
 *
 * 2011-03-04 Yuen
 * 			fixed the wrong checking of public_html path
 * 
 *  
 */

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$lftp = new libftp();
if (isset($plugin['personalfile']) && $plugin['personalfile'])
{
/*
    if (!$lftp->isAccountExist($lu->UserLogin))
    {
         header ("Location: /home/school/close.php");
         exit();
    }
    */
}
else
{
    header("Location: close.php");
    exit();
}


if (!$lftp->connect($lu->UserLogin,$lu->UserPassword))
{
     header("Location: close.php");
     exit();
}

$user_root = $lftp->pwd();
if ($current_dir != "")
{
    $current_dir = stripslashes($current_dir);
    $lftp->chdir($current_dir);
}
$current_dir = $lftp->pwd();

# fixed the checking
$pos = strpos($current_dir,"/public_html");
if ($pos !=0 )
{
	$pos = strpos($current_dir,"$user_root/public_html");	
}

if ($pos===false) $isPublic = false;
else if ($pos ==0 ) $isPublic = true;
else $isPublic = false;


$targetDir = stripslashes($targetDir);
if ($lftp->chdir($current_dir))
{
    for($i=0; $i<sizeof($filename); $i++)
    {
        #$filename[$i] = stripslashes($filename[$i]);
        $filename[$i] = urldecode(cleanCrossSiteScriptingCode($filename[$i]));
        $lftp->move($current_dir,$targetDir,$filename[$i]);
        $lftp->chmod($targetDir,$filename[$i],$isPublic);
        #echo $filename[$i]."<br>\n";
    }
    $msg = 9;
}
else
{
    $msg = 10;
}

$encoded_dir = urlencode($current_dir);
$lftp->close();
intranet_closedb();
header("Location: browse.php?current_dir=$encoded_dir&msg=$msg&success=$success");
?>