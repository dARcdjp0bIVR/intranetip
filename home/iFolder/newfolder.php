<?php


/*
 *
 * 2011-03-09 Yuen
 * 			trimmed the foldername to prevent problem if the foldername contains space at the end
 * 
 *  
 */
 
 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$lftp = new libftp();
if (isset($plugin['personalfile']) && $plugin['personalfile'])
{
/*
    if (!$lftp->isAccountExist($lu->UserLogin))
    {
         header ("Location: /home/school/close.php");
         exit();
    }
    */
}
else
{
    header("Location: close.php");
    exit();
}


if (!$lftp->connect($lu->UserLogin,$lu->UserPassword))
{
     header("Location: close.php");
     exit();
}

$user_root = $lftp->pwd();
if ($current_dir != "")
{
    $current_dir = stripslashes($current_dir);
}
if ($lftp->chdir($current_dir))
{
    $folderName = trim(stripslashes(cleanCrossSiteScriptingCode($folderName)));
    if (@$lftp->mkdir($folderName))
    {
        $msg = 1;
        #$pos = strpos($current_dir,"$user_root/public_html");
        $pos = strpos($current_dir,"/".$lftp->public_dir);

        if ($pos===false) $isPublic = false;
        else if ($pos ==0 ) $isPublic = true;
        else $isPublic = false;

        $lftp->chmod($current_dir,$folderName,$isPublic);
    }
    else
    {
        $msg = 2;
    }
}
else
{
    $msg = 2;
}

$encoded_dir = urlencode($current_dir);
$lftp->close();
intranet_closedb();
header("Location: browse.php?current_dir=$encoded_dir&msg=$msg");
?>