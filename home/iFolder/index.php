<?php
// Editing by 
/*
 * 2014-09-17 (Carlos): Create ftp account if not truely created before
 * 2014-07-03 (Carlos): Auto update ftp account password if connection fail
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
intranet_auth();
intranet_opendb();

if ($personalfile_type == 'AERO' || $plugin['aerodrive'])
{
    //intranet_opendb();
    $lu = new libuser($UserID);
    if ($lu->isTeacherStaff())
    {
        $url = $aero_home_path;
    }
    else if ($lu->isStudent())
    {
        $url = $aero_home_path_student;
    }
    else
    {
        $url = "/home/school/close.php";
    }
    header("Location: $url");
    intranet_closedb();
}
else if ($personalfile_type == 'FTP')
{
	include_once($PATH_WRT_ROOT."includes/libftp.php");
	$lftp = new libftp();
	$lu = new libuser($UserID);
	$connected = $lftp->connect(strtolower($lu->UserLogin),$lu->UserPassword);
	if(!$connected){
		if(!$lftp->isAccountExist($lu->UserLogin)){
			$file_content = get_file_content($intranet_root."/file/account_file_quota.txt");
		    if ($file_content == "")
		    {
		        $userquota = array(10,10,10,10);
		    }
		    else
		    {
		        $userquota = explode("\n", $file_content);
		        if($userquota[3]=="") $userquota[3] = 10; // Alumni is lately added, may not set any quota
		    }
		    $quota = $userquota[$lu->RecordType-1];
			$create_account_success = $lftp->open_account($lu->UserLogin, $lu->UserPassword);
			if($create_account_success){
				$lftp->setTotalQuota($lu->UserLogin,$quota,"iFolder");
				$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL + 2 WHERE UserID = '".$lu->UserID."' AND (ACL NOT IN (2,3))";
	            $lu->db_db_query($sql);
			}
		}else
		{
			$update_pw_success = $lftp->changePassword($lu->UserLogin, $lu->UserPassword, "iFolder");
		}
	}
	intranet_closedb();
    header("Location: browse.php");
     /*
     intranet_opendb();
    $lu = new libuser($UserID);
$UserLogin = $lu->UserLogin;
$UserPassword = $lu->UserPassword;
$direct_url = "ftp://$UserLogin@$personalfile_ftp_host/";
?>
<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <meta http-equiv="REFRESH" content="0; URL=<?=$direct_url?>">
        <title>HTML REDIRECT</title>
</head>
<body>
</body>
</html>
<?
    intranet_closedb();
    */
}
else header ("Location: /home/school/close.php");
?>