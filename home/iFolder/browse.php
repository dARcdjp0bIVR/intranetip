<?php
// using : 
/*
 * 2018-01-08 (Carlos): Fixed sorting by date issue (missing year if file creation time is current year).
 * 2016-03-23 (Carlos): added sorting function.
 * 2013-01-11 (Carlos): added Create Routing Document function
 */

$benchmark['start'] = time();
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

$arrCookies[] = array("ck_ifolder_page_order", "order");
$arrCookies[] = array("ck_ifolder_page_field", "field");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();
$benchmark['init'] = time();

$lu = new libuser($UserID);
$benchmark['user retrieved'] = time();
$lftp = new libftp();
$benchmark['object created'] = time();

if (isset($plugin['personalfile']) && $plugin['personalfile'])
{
/*
    if (!$lftp->isAccountExist($lu->UserLogin))
    {
         header ("Location: /home/school/close.php");
         exit();
    }
    */
}
else
{
    header("Location: close.php");
    exit();
}

$benchmark['try connect'] = time();

if (!$lftp->connect(strtolower($lu->UserLogin),$lu->UserPassword))
{
     # Wrong
     if ($lftp->isLocal) $failed_msg = $i_Files_ConnectionFailed_Local;
     else $failed_msg = $i_Files_ConnectionFailed_Remote;
     $x = "$failed_msg";
}
else
{
$benchmark['connected'] = time();

    # Try to make public_html
    $lftp->createWebDirectory();
$benchmark['check web directory'] = time();

    # List of files
    $user_root = $lftp->pwd();

    $toolbar  = "<a class='iconLink' href=\"javascript:fs_newfolder(document.form1,'newfolder.php')\">".newIcon2()."$i_Files_NewFolder</a>\n";
    $toolbar .= "<a class='iconLink' href=\"javascript:newWindowNotClose('displayquota.php',0)\"><img src=\"$image_path/admin/analysis.gif\" border=0 hspace=1 vspace=0 align=absmiddle>$i_Files_CheckQuota</a>";
    
    if($plugin['DocRouting'] && $_SESSION['UserType'] == USERTYPE_STAFF) {
    	include_once($PATH_WRT_ROOT."includes/liburlparahandler.php");
		include_once($PATH_WRT_ROOT."includes/DocRouting/docRoutingConfig.inc.php");
		include_once($PATH_WRT_ROOT."includes/DocRouting/libDocRouting.php");
		
		$ldocrouting = new libDocRouting();
		$pe = $ldocrouting->getEncryptedParameter('management','edit_doc',array('createDocFromModule'=>'ifolder') );
		$doc_routing_httppath = '/home/eAdmin/ResourcesMgmt/DocRouting/index.php?pe='.$pe;
		
		$toolbar .= "<br /><a class='iconLink' href=\"javascript:createRoutingFromCheckedFiles(document.form1);\"><img src=\"$image_path/$LAYOUT_SKIN/icon_copy_b.gif\" border=0 hspace=1 vspace=0 align=absmiddle />".$Lang['Gamma']['CreateRoutingDocument']."</a>";
    }
    
    $searchbar  = "<a href=\"javascript:fs_fileupload(document.form1,'upload.php')\"><img alt='$i_Files_Upload' src='$image_path/btn_upload_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
    $searchbar .= " <input class=text type=text size=1 maxlength=1 name=no_file value=1> $i_Files_Files \n";

    $functionbar = "$searchbar<br>\n";
    $functionbar .= "<a href=\"javascript:fs_unzip(document.form1,'filename[]','unzip.php')\"><img alt='".$Lang['iFolder']['FieldTitle']['Unzip']."' src='$image_path/btn_unzip_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
    $functionbar .= "<a href=\"javascript:fs_rename(document.form1,'filename[]','rename.php')\"><img alt='$i_Files_Rename' src='$image_path/btn_rename_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
    $functionbar .= "<a href=\"javascript:fs_move(document.form1,'filename[]','move.php')\"><img alt='$i_Files_Move' src='$image_path/btn_move_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
    $functionbar .= "<a href=\"javascript:checkRemove(document.form1,'filename[]','remove.php')\"><img alt='$i_Files_Delete' src='$image_path/btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;<br>\n";
    
    // Collect request variable
    $current_dir = isset($_REQUEST['current_dir'])?$_REQUEST['current_dir']:$user_root;
    #echo stripslashes($current_dir);
    $current_dir = stripslashes($current_dir);
    
    #exit();
    #$new_dir = isset($_REQUEST['new_dir'])?$_REQUEST['new_dir']:NULL;
    $lftp->chdir($current_dir);
    $current_dir = $lftp->pwd();
$benchmark['change target directory'] = time();

    $pos = strpos($current_dir,"/".$lftp->public_dir);

    if ($pos===false) $isPublic = false;
    else if ($pos ==0 ) $isPublic = true;
    else $isPublic = false;

$benchmark['try list'] = time();

	$fields = array('Filetype','Filename','FilesizeStr','DateStr');
	$field = isset($field) && $field != '' ? $field : 1;
	$order = isset($order) && $order != '' ? $order : 0;
	if(!in_array($field, array(0,1,2,3))){
		$field = 1;
	}
	if(!in_array($order, array(0,1))){
		$order = 0;
	}
	$order_arrow = $order == 1? '&blacktriangle;' : '&blacktriangledown;';

    #$x = "$i_Files_CurrentDirectory: $current_dir";
    #if ($isPublic) $x .= " $i_Files_ThisIsPublic";
    $files = $lftp->list_file($current_dir);
$benchmark['finished list'] = time();

	$current_year = date('Y');
	$file_count = count($files);
	for($i=0;$i<$file_count;$i++){
		$files[$i]['FilesizeStr'] = sprintf('%012d',$files[$i]['Filesize']);
		if(preg_match('/^.+\s(\d\d:\d\d)$/',$files[$i]['Date'],$matches)){ // no year if file create time is today's year, so subsitute current year to it
			$files[$i]['DateStr'] = date("Y-m-d H:i", strtotime(str_replace($matches[1],$current_year.' '.$matches[1],$files[$i]['Date'])));
		}else{
			$files[$i]['DateStr'] = date("Y-m-d H:i", strtotime($files[$i]['Date']));
		}
	}
	
	sortByColumn2($files,$fields[0],$order,0,$fields[$field]);
	
// change folder permission recursively - new method

if(isset($sys_custom['iFolder_CheckPermission']) && $sys_custom['iFolder_CheckPermission']){
	$lftp->chmod("/","",1);			// change userfolder to 755
	$lftp->ftp_chmod_recursive($current_dir);		// change permission recusively in public_html folder if needed
}

	/*
	###old version
	if($sys_custom['iFolder_CustomizeFileAccessRight'] != "")
    {
    	$lftp->chmod("/","",1);
		$permission = substr(sprintf('%o', @fileperms($current_dir)), -4);
		if($permission == 0)
			$lftp->chmod($current_dir,'',$isPublic);
	}else{
		$lftp->chmod("/","",1);
		
		for($i=0; $i<sizeof($files); $i++){
			if($files[$i]['Filename'] == $lftp->public_dir){
				$isPublic = true;
			}else{
				$pos = strpos($current_dir,"/".$lftp->public_dir);
				if ($pos===false) $isPublic = false;
    			else if ($pos ==0 ) $isPublic = true;
    			else $isPublic = false;
			}
			if($current_dir != $user_root) {
				$target_dir = $user_root.$current_dir;
			}else{
				$target_dir = $current_dir;
			}
			$lftp->chmod($target_dir,$files[$i]['Filename'],$isPublic);
		}
	}
	*/

#    $x .= "<br>\n";
    $x .= "<table width=700 border=1 cellpadding=3 cellspacing=0 bordercolorlight=#FCF7E5 bordercolordark=#DBD6C4 bgcolor=#FFFFFF class=body>\n";
    $x .= "<tr bgcolor=#D7BB88>
			<td width=30>$i_Files_Type</td><td><a href=\"javascript:doSort(1,$order);\">$i_Files_Name ".($field==1?$order_arrow:"")."</a></td>
			<td><a href=\"javascript:doSort(2,$order);\">$i_Files_Size ".($field==2?$order_arrow:"")."</a></td>";
    $x .= "<td><a href=\"javascript:doSort(3,$order);\">$i_Files_Date ".($field==3?$order_arrow:"")."</a></td><td width=1>";
    $x .= "<input type=checkbox onClick=(this.checked)?setChecked(1,document.form1,'filename[]'):setChecked(0,document.form1,'filename[]')></td></tr>\n";
    $encoded_dir = rawurlencode($current_dir);
	
    if ($user_root != $current_dir)
    {
         $type = "<img src=\"$image_path/upfolder.gif\">";
         $link = "<a href=?current_dir=$encoded_dir/..>..</a>";
         $x .= "<tr bgcolor=#FFFFFF><td>$type</td><td>$link</td><td> -- </td><td> -- </td><td>&nbsp;</td></tr>\n";
    }

    for ($i=0; $i<sizeof($files); $i++)
    {
         $type = $files[$i]['Filetype'];
         $size = $files[$i]['Filesize'];
         $date = $files[$i]['Date'];
         $name = $files[$i]['Filename'];

         $css = ($i%2==0? "": "2");
         $bgcolor = ($i%2? "#FFFFFF":"#F6F6F6");

         $encoded_name = rawurlencode($name);
			 
         //$url = ($type=="D")? "?current_dir=".($current_dir=="/"? "/$encoded_name":$encoded_dir."/".$encoded_name):"download.php?current_dir=$encoded_dir&filename=".$encoded_name;
	
			if($type=="D"){
				$url = "?current_dir=".($current_dir=="/"? "/$encoded_name" : $encoded_dir."/".$encoded_name);
			}
			else{
				//$url = "download.php?current_dir_e=$encoded_dir&filename_e=".$encoded_name;	
				
				 $encrypted_currentDir = getEncryptedText($current_dir);
				 $encrypted_filename= getEncryptedText($name);
				 $url = "download.php?current_dir_e=$encrypted_currentDir&filename_e=".$encrypted_filename;	
				
			}
		
         #$link = "<a href=download.php?current_dir=$encoded_dir&name=$encoded_name>$name</a>";
         #$link = "<a href=\"$url\" ".($type=="F"? "target=_blank":"").">$name</a>";
         $link = "<a href=\"$url\" ".("A"=="F"? "target=_blank":"")."><span id='$encoded_name'>$name</span></a>";
         $type_img = ($type=="F"?"<img src=\"$image_path/file.gif\">":"<img src=\"$image_path/folder.gif\">");
         $x .= "<tr bgcolor=$bgcolor><td>$type_img</td><td>$link</td><td>$size</td><td>$date</td><td><input type=checkbox name=filename[] value='$encoded_name'><input type=checkbox name=filetype[] value='$type' style='display:none;'></td></tr>\n";
    }
    $x .= "<tr bgcolor=#D7BB88>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>\n";
    $x .= "</table>\n";
}
$body_tags .= " leftmargin=0 topmargin=0 marginwidth=0 marginheight=0";

$UserLogin = strtolower($lu->UserLogin);
$UserPassword = $lu->UserPassword;
$direct_url = "ftp://$UserLogin@$personalfile_ftp_host/";

if ($lu->isTeacherStaff())
{
    $file_interface_type = $personalfile_teacher_interface;
}
else if ($lu->isStudent())
{
     $file_interface_type = $personalfile_student_interface;
}
else if ($lu->isParent())
{
     $file_interface_type = $personalfile_parent_interface;
}
else
{
    $file_interface_type = "a";
}

$benchmark['text generated'] = time();


include_once($PATH_WRT_ROOT."templates/fileheader.php");
$benchmark['header included'] = time();
?>
<script language='javascript'>
function returnChecked(obj, element_name){
        len=obj.elements.length;
        var i=0;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name==element_name && obj.elements[i].checked)
				{
					idName = obj.elements[i].value;
					idObj = document.getElementById(idName);
					if(idObj==null) return null;
					return idObj.innerHTML;
				}
        }
        return null;
}

function doSort(field,order)
{
	document.form1['field'].value = field;
	document.form1['order'].value = order == 1? 0 : 1;
	document.form1.submit();
}

<?php  if($plugin['DocRouting'] && $_SESSION['UserType'] == USERTYPE_STAFF) { ?>
function createRoutingFromCheckedFiles(formObj)
{
	var filenameObjs = document.getElementsByName('filename[]');
	var filetypeObjs = document.getElementsByName('filetype[]');
	var numCheckedFile = 0;
	
	for(var i=0;i<filenameObjs.length;i++) {
		if(filenameObjs[i].checked && filetypeObjs[i].value=='F') {
			numCheckedFile++;
		}
		if(filenameObjs[i].checked){
			filetypeObjs[i].checked = true;
		}else{
			filetypeObjs[i].checked = false;
		}
	}
	
	if(numCheckedFile == 0){
		alert('<?=$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseSelectAtLeastOneFile']?>');
		return;
	}else{
		var oldMethod = formObj.method;
		var oldAction = formObj.action;
		var oldTarget = formObj.target;
		formObj.method = 'POST';
		formObj.action = '<?=$doc_routing_httppath?>';
		formObj.target = '_blank';
		formObj.submit();
		formObj.method = oldMethod;
		formObj.action = oldAction;
		formObj.target = oldTarget;
	}
}
<? } ?>
</script>
<form id="form1" name="form1" method="post" action="">
<table width="750" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width=267 class=td_bottom><span class="body"><?php echo $lftp->displayFunctionbar($toolbar, "");?></span></td>
          <td width=162><img src="<?=$image_path?>/myaccount_filecabinet/cabinet1.gif" width="162" height="60"><br><img src="<?=$image_path?>/myaccount_filecabinet/cabinet2.gif" width="162" height="30"></td>
          <td width=300 class=td_bottom align="right" ><?php echo $lftp->displayFunctionbar("", $functionbar); ?>
          </td>
          <td width=21 align="right" valign="bottom">&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="center"><table width="725" border="0" cellpadding="0" cellspacing="0" bgcolor="#FCDBA8">
        <tr>
          <td width="8"><img src="<?=$image_path?>/myaccount_filecabinet/board_top_left.gif" width="8" height="8"></td>
          <td width="709"><img src="<?=$image_path?>/spacer.gif" width="20" height="8"></td>
          <td width="8"><img src="<?=$image_path?>/myaccount_filecabinet/board_top_right.gif" width="8" height="8"></td>
        </tr>
        <tr>
          <td bgcolor="#FCDBA8">&nbsp;</td>
          <td align="center"> <table width="690" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="11"><img src="<?=$image_path?>/myaccount_filecabinet/locationbg_left.gif" width="11" height="49"></td>
                <? if ($file_interface_type == 0) { ?>
                <!--<td width="668" class=filecabinet_cell><a target=_blank href="<?=$direct_url?>"><img align='absmiddle' border=0 src="<?=$image_path?>/myaccount_filecabinet/btn_opendirectftp_<?=$intranet_session_language?>.gif"></a><img src="<?=$image_path?>/myaccount_filecabinet/cabinet.gif" width="55" height="49" align="absmiddle"><img src="<?=$image_path?>/myaccount_filecabinet/t_locationnow_<?=$intranet_session_language?>.gif" align="absmiddle"><span class="body"><?=$current_dir?></span></td>-->
                <td width="668" class=filecabinet_cell><img src="<?=$image_path?>/myaccount_filecabinet/cabinet.gif" width="55" height="49" align="absmiddle"><img src="<?=$image_path?>/myaccount_filecabinet/t_locationnow_<?=$intranet_session_language?>.gif" align="absmiddle"><span class="body"><?=$current_dir?></span></td>
                <? } else { ?>
                <td width="668" class=filecabinet_cell><img src="<?=$image_path?>/myaccount_filecabinet/cabinet.gif" width="55" height="49" align="absmiddle"><img src="<?=$image_path?>/myaccount_filecabinet/t_locationnow_<?=$intranet_session_language?>.gif" align="absmiddle"><span class="body"><?=$current_dir?></span></td>
                <? } ?>
                <td width="11"><img src="<?=$image_path?>/myaccount_filecabinet/locationbg_right.gif" width="11" height="49"></td>
              </tr>
            </table> </td>
          <td bgcolor="#FCDBA8">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="center">
            <font color="#FF0000"><span class="body"><?=($isPublic?"<img src=\"$image_path/myaccount_filecabinet/icon_notice.gif\" width=25 height=19 align=absmiddle> $i_Files_ThisIsPublic":"")?></span></font></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="center">
          <?=$x?>
                        </td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><img src="<?=$image_path?>/myaccount_filecabinet/board_bottom_left.gif" width="8" height="8"></td>
          <td><img src="<?=$image_path?>/spacer.gif" width="20" height="8"></td>
          <td><img src="<?=$image_path?>/myaccount_filecabinet/board_bottom_right.gif" width="8" height="8"></td>
        </tr>
      </table></td>
  </tr>
</table>
<input type=hidden name=folderName value="">
<input type=hidden name=newName value="">
<input type=hidden name=current_dir value="<?=$current_dir?>">
<input type="hidden" id="field" name="field" value="<?=$field?>" />
<input type="hidden" id="order" name="order" value="<?=$order?>" />
</form>
<?php
include_once($PATH_WRT_ROOT."templates/filefooter.php");
$benchmark['footer included'] = time();

$lftp->close();
$benchmark['ftp close'] = time();

intranet_closedb();
$benchmark['end'] = time();

#print_r($benchmark);	
?>