<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$lftp = new libftp();
if (isset($plugin['personalfile']) && $plugin['personalfile'])
{
/*
    if (!$lftp->isAccountExist($lu->UserLogin))
    {
         header ("Location: /home/school/close.php");
         exit();
    }
    */
}
else
{
    header("Location: close.php");
    exit();
}


if (!$lftp->connect($lu->UserLogin,$lu->UserPassword))
{
     # Wrong
    header("Location: close.php");
    exit();
}
else
{
    # List of files
    $user_root = $lftp->pwd();

    // Collect request variable
    $current_dir = isset($_REQUEST['current_dir'])?$_REQUEST['current_dir']:$user_root;
    $current_dir = stripslashes($current_dir);

    $lftp->chdir($current_dir);
    $current_dir = $lftp->pwd();

    $pos = strpos($current_dir,"$user_root/public_html");

    if ($pos===false) $isPublic = false;
    else if ($pos ==0 ) $isPublic = true;
    else $isPublic = false;

    $lftp->close();

}

$no_file = IntegerSafe($no_file);
include_once($PATH_WRT_ROOT."templates/fileheader.php");
echo generateFileUploadNameHandlerByCount("form1","userfile","hidden_userfile_name",$no_file);
?>
<form name="form1" action="uploadfile.php" method="post" enctype="multipart/form-data" onsubmit="return Big5FileUploadHandler();">
<table width="750" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="725" border="0" cellpadding="0" cellspacing="0" bgcolor="#FCDBA8">
        <tr>
          <td width="8"><img src="<?=$image_path?>/myaccount_filecabinet/board_top_left.gif" width="8" height="8"></td>
          <td width="709"><img src="<?=$image_path?>/spacer.gif" width="20" height="8"></td>
          <td width="8"><img src="<?=$image_path?>/myaccount_filecabinet/board_top_right.gif" width="8" height="8"></td>
        </tr>
        <tr>
          <td bgcolor="#FCDBA8">&nbsp;</td>
          <td align="center"> <table width="690" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="11"><img src="<?=$image_path?>/myaccount_filecabinet/locationbg_left.gif" width="11" height="49"></td>
                <td width="668" align="center" class=filecabinet_cell><img src="<?=$image_path?>/myaccount_filecabinet/cabinet.gif" width="55" height="49" align="absmiddle"><img src="<?=$image_path?>/myaccount_filecabinet/t_uploadto_<?=$intranet_session_language?>.gif" align="absmiddle"><span class="body"><?=$current_dir?></span></td>
                <td width="11"><img src="<?=$image_path?>/myaccount_filecabinet/locationbg_right.gif" width="11" height="49"></td>
              </tr>
            </table> </td>
          <td bgcolor="#FCDBA8">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="center">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="center">
            <table width="700" border="1" cellpadding="3" cellspacing="0" bordercolorlight="FCF7E5" bordercolordark="#DBD6C4" bgcolor="F6F6F6" class="body">
                <tr>
                  <td align="center"><p>&nbsp;</p><table width="450" border="0" cellpadding="0" cellspacing="0" class="body">
                      <tr>
                        <td width="170" align="left" valign="middle"><img src="<?=$image_path?>/myaccount_filecabinet/graphic_carbinet.gif" width="162" height="210"></td>
                        <td>
                          <?php for($i=0;$i<$no_file;$i++){ ?>
                          <?php echo "<p>".($i+1); ?>: <input class=file type=file name="userfile<?php echo $i; ?>" size=25></p>
                          <input type=hidden name="hidden_userfile_name<?=$i?>">
                          <?php } ?>
                        </td>
                      </tr>
                    </table><p>&nbsp;</p></td>
                </tr>
                <tr>
                  <td align="right">
                    <input type=image src="<?=$image_path?>/btn_upload_<?=$intranet_session_language?>.gif" border=0>
                    <a href="javascript:history.back();"><image border=0 src="<?=$image_cancel?>"></a>
                  </td>
                </tr>
            </table>
          </td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><img src="<?=$image_path?>/myaccount_filecabinet/board_bottom_left.gif" width="8" height="8"></td>
          <td><img src="<?=$image_path?>/spacer.gif" width="20" height="8"></td>
          <td><img src="<?=$image_path?>/myaccount_filecabinet/board_bottom_right.gif" width="8" height="8"></td>
        </tr>
      </table></td>
  </tr>
</table>
<input type=hidden name=current_dir value="<?=$current_dir?>">
<input type=hidden name=no_file value="<?php echo $no_file; ?>">
</form>
<?php
include_once($PATH_WRT_ROOT."templates/filefooter.php");
intranet_closedb();
?>