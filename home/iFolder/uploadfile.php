<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$lftp = new libftp();
if (isset($plugin['personalfile']) && $plugin['personalfile'])
{
}
else
{
    header("Location: close.php");
    exit();
}


if (!$lftp->connect($lu->UserLogin,$lu->UserPassword))
{
     header("Location: close.php");
     exit();
}

### Check Total Upload file size is over the POST size limit
$POST_MAX_SIZE = ini_get('post_max_size');
$mul = substr($POST_MAX_SIZE, -1);
$mul = ($mul == 'M' ? 1048576 : ($mul == 'K' ? 1024 : ($mul == 'G' ? 1073741824 : 1)));
if ($_SERVER['CONTENT_LENGTH'] > $mul*(int)$POST_MAX_SIZE && $POST_MAX_SIZE)
{
	$failed_to_write = true;
}else{
	$user_root = $lftp->pwd();
	$current_dir = cleanHtmlJavascript($current_dir);
	if ($current_dir != "")
	{
	    $current_dir = stripslashes($current_dir);
	    $lftp->chdir($current_dir);
	}
	$current_dir = $lftp->pwd();
	
	$pos = strpos($current_dir,"/".$lftp->public_dir);
	
	if ($pos===false) $isPublic = false;
	else if ($pos ==0 ) $isPublic = true;
	else $isPublic = false;
	
	$success = 0;
	$failed_to_write = false;
	if ($lftp->chdir($current_dir))
	{
	    $no_file = IntegerSafe($no_file);
	    for($i=0;$i<$no_file;$i++)
	    {
	        $loc = ${"userfile".$i};
	        #$file = ${"userfile".$i."_name"};
	        $file = stripslashes(${"hidden_userfile_name$i"});
	        $file_size = $_FILES["userfile".$i][size];	# upload file size
			$used_quota = $lftp->getUsedQuota($lu->UserLogin,"iFolder");
			$assigned_quota = $lftp->getTotalQuota($lu->UserLogin,"iFolder");
			$avali_quota = ($assigned_quota - $used_quota) * 1024 * 1024;
	        /*
	        echo "File : $file <br>\n";
	        echo "File html : ".intranet_htmlspecialchars($file)." <br>\n";
	        echo "File addslashes: ".addslashes($file)." <br>\n";
	        echo "File strip: ".stripslashes($file)." <br>\n";
	        */
	        if($loc=="none" || $file == "" || $loc == "")
	        {
	        }
	        else
	        {
	            if($assigned_quota == 0 || $avali_quota >= $file_size){
		            		if (!$lftp->upload($loc,$current_dir,$file,$isPublic))
		            		{
				                $failed_to_write = true;
		            		}
		            		$success++;
				}else{
					$failed_to_write = true;
				}
	        }
	    }
	    $msg = 3;
	}
	else
	{
	    $msg = 4;
	}
}

$lftp->close();
intranet_closedb();
if ($failed_to_write)
{
    include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
    include_once($PATH_WRT_ROOT."templates/fileheader.php");
    echo "$i_Files_msg_FailedToWrite";
    ?>
    <br><br>
    <a class=functionlink_new href=browse.php><?=$i_Files_ClickHereToBrowse?></a>
    <?
    include_once($PATH_WRT_ROOT."templates/filefooter.php");
}
else
{
    $encoded_dir = urlencode($current_dir);
    header("Location: browse.php?current_dir=$encoded_dir&msg=$msg&success=$success");
}
?>