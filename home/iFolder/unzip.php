<?
// using : ronald
/*
 * Logic
 * **************************************************
 * 1. download the zip from FTP, store in local '/tmp/'
 * 2. unzip the zip in local
 * 3. upload the extracted files back to FTP
 *  
 */
 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/pclzip.lib.php");

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$lftp = new libftp();
$lfs = new libfilesystem();

$local_temp_path = '/tmp';
$current_timestamp = time();

$connection_id = $lftp->connect(strtolower($lu->UserLogin),$lu->UserPassword);
if (!$connection_id)
{
     # Wrong
     if ($lftp->isLocal) $failed_msg = $i_Files_ConnectionFailed_Local;
     else $failed_msg = $i_Files_ConnectionFailed_Remote;
     $x = "$failed_msg";
}
else
{
	for($i=0; $i<sizeof($filename); $i++)
	{
		$zip_file = $current_dir."/".urldecode($filename[$i]);						// zip in FTP server
		$local_temp_zip = $local_temp_path.'/'.urldecode($filename[$i]);			// zip in local
		
		ftp_get($connection_id, $local_temp_zip, $zip_file, FTP_BINARY);			// get the zip from FTP server to local /tmp
		
		$end_pos = strpos(strtoupper(urldecode($filename[$i])),'.ZIP');
		$local_temp_zip_folder = substr(urldecode($filename[$i]),0,$end_pos);		// folder used to store the extracted zip
		$local_temp_zip_folder = $local_temp_zip_folder.'_'.$current_timestamp;
		
		mkdir($local_temp_path.'/'.$local_temp_zip_folder);
		chmod($local_temp_path.'/'.$local_temp_zip_folder,0777);
	
		$archive = new PclZip($local_temp_zip);
		if ($archive->extract(PCLZIP_OPT_PATH, $local_temp_path.'/'.$local_temp_zip_folder) == 0){
			die("Error : ".$archive->errorInfo(true));
		}else{
			
			$local_file_list = scandir($local_temp_path.'/'.$local_temp_zip_folder);		// get all the file in the local zip folder 
			
			uploadExtractedFiles($connection_id, $local_temp_path.'/'.$local_temp_zip_folder, $current_dir);

			$lfs->lfs_remove($local_temp_path.'/'.$local_temp_zip_folder);		// remove the local temp extracted zip file
			$lfs->lfs_remove($local_temp_zip);		// remove the zip in local
		}
	}
}	
$lftp->close();

/*
if(!in_array(false,$result)){
	$msg = "Unzip successfully.";
	$success = 1;
}else{
	$msg = "Unzip failed.";
	$success = 0;
}
*/
intranet_closedb();
$encoded_dir = urlencode($current_dir);
header("Location: browse.php?current_dir=$encoded_dir&msg=$msg&success=$success");

?>

<?

function uploadExtractedFiles( $connection_id, $path, $ftp_path )
{
	// Directories to ignore when listing output. Many hosts will deny PHP access to the cgi-bin.
    $ignore = array( 'cgi-bin', '.', '..' ); 
     
	// Open the directory to the handle $dh
    $dh = @opendir( $path );
     
    while( false !== ( $file = readdir($dh) ) ){		    // Loop through the directory  

        if( !in_array( $file, $ignore ) ){					// Check that this file is not to be ignored 
             
            if( is_dir( "$path/$file" ) ){					// Its a directory, so we need to keep reading down...  
             
             	$folder_name = $file;
             	
             	//$folder_name = iconv("Big5","UTF8//IGNORE//TRANSLIT",$folder_name);
             	$encoding = DetectDataEncoding($folder_name);
             	if ($encoding != 'UTF8') {
					$folder_name = iconv($encoding, 'UTF8', $folder_name);
				}
             	
				ftp_chdir($connection_id,$ftp_path);
				@ftp_mkdir($connection_id,$folder_name);
				
				// Re-call this same function but on a new directory. 
                uploadExtractedFiles( $connection_id, "$path/$file", "$ftp_path/$folder_name" );
            } else { 
 
                $filename = $file;
                
                //$destination_filename = iconv("BIG5","UTF8//IGNORE//TRANSLIT",$filename);
				$encoding = DetectDataEncoding($filename);
             	if ($encoding != 'UTF8') {
					$filename = iconv($encoding, 'UTF8', $filename);
				}
				$destination_filename = $filename;
				//$local_file = $path.'/'.$filename;
				$local_file = $path.'/'.$file;
				
				ftp_chdir($connection_id,$ftp_path);
				$result = ftp_put($connection_id,$destination_filename,$local_file,FTP_BINARY);		// upload back to FTP server
            } 
        } 
    }
    // Close the directory handle      
    closedir( $dh ); 
}
?>