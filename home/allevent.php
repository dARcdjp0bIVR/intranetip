<?php
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$li = new libcalevent();

$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();

if ($startdate == "")
{
    $startdate = substr(getStartDateOfAcademicYear($AcademicYearID),0,10);
}

if ($enddate == "")
{
	$enddate = substr(getEndDateOfAcademicYear($AcademicYearID),0,10);
}

$tstart = strtotime($startdate);
$tend = strtotime($enddate);

$MODULE_OBJ['title'] = $i_status_all . (($intranet_session_language=="en") ? " ": "") . $i_Events;
$linterface = new interface_html("popup_index.html");
$linterface->LAYOUT_START();

?>

<?php echo $li->displayEventByDateRange($tstart,$tend); ?>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>