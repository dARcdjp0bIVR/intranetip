<?php
## Using By : 

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$gs = new libgeneralsettings(); 

$Settings['AccessKey'] = $AccessKey;
$Success = $gs->Save_General_Setting("eClassStore", $Settings); 

if($Success)
{
	header("location: redirect.php");	 
}
else
{
	die("系統尚未設定貴校的資料，請聯絡Broadlearning員工。");
}

intranet_closedb();
?>