<?php
# using : Paul
/*
 * Modification log:
 * 2016-10-19 (Paul) [ip.2.5.7.10.1]
 * 	- Remove single quote from file name to prevent failure in video conversion
 */

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$RequestID = isset($RequestID) && $RequestID!=''? $RequestID : 0;

# Initialize Object
$lf    		  = new libfilesystem();
$ePost 	    = new libepost();
$ePostRequest = new libepost_request($RequestID);
$maxUploadCnt = $cfg_ePost['max_upload'][$ePostRequest->ModuleCode];
$title = isset($title) && $title!=''? htmlspecialchars($title) : "";
$content = isset($content) && $content!=''? htmlspecialchars($content) : "";

# Check if ePost should be accessible
$ePostRequest->auth();
if($RecordID){
	$sql = "UPDATE
				EPOST_WRITING
			SET
				Title = '$title',
				Content = '$content',
				TopicCode = '".$ePostRequest->TopicCode."',
				Status = $status,
				modified = NOW(),
				ModifiedBy = $UserID
			WHERE
				RecordID = $RecordID";
	$result = $ePostRequest->db_db_query($sql);
	
	$sql = "
		SELECT 
			AttachmentID,Attachment,Caption
		FROM
			EPOST_WRITING_ATTACHMENT 
		WHERE WritingID = '".$RecordID."'
	"; 
	$attachmentResultAry = $ePostRequest->returnArray($sql);
}
else{
	$sql = "INSERT INTO
				EPOST_WRITING
				(
				 RequestID, UserID, Title, Content, LevelCode, ModuleType, ModuleCode,
				 ThemeCode, TopicCode, Status, inputdate, modified, ModifiedBy
				)
			VALUES
				(
				 $RequestID, $UserID, '$title', '$content', '".$ePostRequest->LevelCode."', '".$cfg_ePost['module_type']['content']."', '".$ePostRequest->ModuleCode."',
				 '".$ePostRequest->ThemeCode."', '".$ePostRequest->TopicCode."', $status, NOW(), NOW(), $UserID
				)";
	$result   = $ePostRequest->db_db_query($sql);
	$RecordID = $ePostRequest->db_insert_id();
}
$dir = $PATH_WRT_ROOT.'file/ePost/writing_attachment/'.$RecordID.'/';
# Process Attachment

# Delete Existing Attachment 

$FileName = '';
$FileNameAry = array();
for($i=1;$i<=$maxUploadCnt;$i++){
	$del_attach = $_POST['del_attach'.$i];
	$caption = $_POST['caption'.$i];
	if(count($attachmentResultAry[$i-1])>0){//existing attachment
		if($del_attach){ //remove attachment file
			if(is_dir($dir)){
				$cur_dir = opendir($dir);
				$file = $attachmentResultAry[$i-1]['Attachment'];
				if($file!='.' && $file!='..'){
					chmod($dir.$file, 0777);
					if(!is_dir($dir.$file)){
						unlink($dir.$file);
						$sql = "DELETE FROM EPOST_WRITING_ATTACHMENT WHERE AttachmentID = '".$attachmentResultAry[$i-1]['AttachmentID']."'";
						$ePostRequest->db_db_query($sql);
					}
						
				}
				
			}
		}elseif(!empty($caption)){
			$sql = "UPDATE EPOST_WRITING_ATTACHMENT SET Caption = '".$caption."' WHERE AttachmentID = '".$attachmentResultAry[$i-1]['AttachmentID']."'";
			$ePostRequest->db_db_query($sql);		
		}
	}

	if($_FILES['fileupload'.$i]['error']==0){
		$FileLocation = $_FILES['fileupload'.$i]['tmp_name'];
		$FileName=str_replace("'","",$_FILES['fileupload'.$i]['name']);
		if(in_array($FileName,$FileNameAry)){
			$_fileNameAry = explode('.',$FileName);
			$FileName = $_fileNameAry[0].'(1).'.$_fileNameAry[1];
		}
		if($FileLocation){
			if(!is_dir($PATH_WRT_ROOT.'file/ePost'))
				 mkdir($PATH_WRT_ROOT.'file/ePost', 0777);
			if(!is_dir($PATH_WRT_ROOT.'file/ePost/writing_attachment'))
				 mkdir($PATH_WRT_ROOT.'file/ePost/writing_attachment', 0777);
			if(!is_dir($PATH_WRT_ROOT.'file/ePost/writing_attachment/'.$RecordID))
				 mkdir($PATH_WRT_ROOT.'file/ePost/writing_attachment/'.$RecordID, 0777);
				 
			$fileDes = $PATH_WRT_ROOT.'file/ePost/writing_attachment/'.$RecordID.'/'.$FileName;
			move_uploaded_file($FileLocation, $fileDes);
			$ePost->convertWritingVideo($fileDes);
			if(file_exists($fileDes)){
				$FileName = $ePostRequest->Get_Safe_Sql_Query($FileName);
				$sql = "INSERT INTO EPOST_WRITING_ATTACHMENT (WritingID,Attachment,Caption,InputBy,InputDate,ModifiedBy,ModifiedDate) ";
				$sql .= "VALUES('".$RecordID."','".$FileName."','".$caption."','".$UserID."',NOW(),'".$UserID."',NOW())";
				$ePostRequest->db_db_query($sql);
				$FileNameAry[] = $FileName;
			}
			else{
				die('Attachment Upload Fail!');
			}	
		}

	}
	
}


intranet_closedb();

if($status){
	header('Location:portfolio.php');
}
else{
	header('Location:writer.php?RecordID='.$RecordID.'&RequestID='.$RequestID.'&IsSave=1');
}
?>