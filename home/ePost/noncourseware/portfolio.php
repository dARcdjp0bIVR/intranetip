<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$level_code = 'NC';

# Initialize Object
$ePost   = new libepost();
$ePostUI = new libepost_ui();
$Header='Portfolio';
# Check if ePost should be accessible
$ePost->auth();

# Generate Table SQL
$sql = "SELECT
			ew.Title,
			encr.Topic,
			CASE
				WHEN ew.Status = ".$cfg_ePost['sql_config']['Status']['submitted']." AND ew.IsRedo = ".$cfg_ePost['sql_config']['IsRedo']['true']." THEN CONCAT('".$Lang['ePost']['Redone'].":<br/>', DATE_FORMAT(ew.modified, '%Y-%m-%d %H:%i'))
				WHEN ew.Status = ".$cfg_ePost['sql_config']['Status']['submitted']." AND ew.IsRedo = ".$cfg_ePost['sql_config']['IsRedo']['false']." THEN CONCAT('".$Lang['ePost']['Submitted'].":<br/>', DATE_FORMAT(ew.modified, '%Y-%m-%d %H:%i'))
				WHEN ew.Status = ".$cfg_ePost['sql_config']['Status']['drafted']." AND ew.IsRedo = ".$cfg_ePost['sql_config']['IsRedo']['true']." THEN CONCAT('".$Lang['ePost']['Redo']."')
				WHEN ew.Status = ".$cfg_ePost['sql_config']['Status']['drafted']." AND ew.IsRedo = ".$cfg_ePost['sql_config']['IsRedo']['false']." THEN CONCAT('".$Lang['ePost']['Saved'].":<br/>', DATE_FORMAT(ew.modified, '%Y-%m-%d %H:%i'))
			END AS Status,
			DATE_FORMAT(encr.Deadline, '%Y-%m-%d') Deadline,
			IFNULL(GROUP_CONCAT(DISTINCT CONCAT('<div class=\"portfolio_item_list\" style=\"width:100%\"><a class=\"btn_show_newspaper\" title=\"".$Lang['ePost']['PostedOn'].": ', DATE_FORMAT(ea.inputdate, '%Y-%m-%d'), '\" href=\"javascript:openNewspaper(', en.NewspaperID, ', ', ep.PageID, ')\">', en.Title, ' - ', en.Name, '</a></div>') ORDER BY ea.inputdate ASC SEPARATOR '<br/>'), '&nbsp;') AS Issue,
			encr.RequestID,
			ew.RecordID
		FROM
			EPOST_WRITING AS ew INNER JOIN
			EPOST_NONCOURSEWARE_REQUEST AS encr ON ew.RequestID = encr.RequestID LEFT JOIN
			EPOST_ARTICLE_SHELF AS eas ON eas.Status = ".$cfg_ePost['BigNewspaper']['ArticleShelf_status']['exist']." AND ew.RecordID = eas.WritingID LEFT JOIN
			EPOST_NEWSPAPER_PAGE_ARTICLE AS ea ON eas.ArticleShelfID = ea.ArticleShelfID AND ea.Status = ".$cfg_ePost['BigNewspaper']['Article_status']['exist']." LEFT JOIN
			EPOST_NEWSPAPER_PAGE AS ep ON ea.PageID = ep.PageID AND ep.Status = ".$cfg_ePost['BigNewspaper']['Page_status']['exist']." LEFT JOIN
			EPOST_NEWSPAPER AS en ON ep.NewspaperID = en.NewspaperID AND en.Status = ".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']."
		WHERE
			ew.UserID = $UserID AND
			ew.LevelCode = '$level_code' AND
			(
				encr.Status = ".$cfg_ePost['BigNewspaper']['NonCoursewareRequest_status']['exist']." OR
				(
					encr.Status = ".$cfg_ePost['BigNewspaper']['NonCoursewareRequest_status']['deleted']." AND
					ew.Status = ".$cfg_ePost['sql_config']['Status']['submitted']."
				)
			)
			AND ew.Status != ".$cfg_ePost['sql_config']['Status']['deleted']."
		GROUP BY
			ew.RecordID";

# Assign variables to table object
$field = $field==''? '1':$field;
$order = $order==''? '1':$order;
$pageNo = $pageNo==''? '1':$pageNo;
$li = new libdbtable2007($field, $order, $pageNo, true);
$li->hide_no_row = false;
$li->field_array = array("","Title","Topic","Status","Deadline","Issue");
$li->sql = $sql;
$li->page_size  = ($numPerPage=='')?10:$numPerPage;
$li->IsColOff   = "ePost_portfolio_report_new";
$li->no_col     = sizeof($li->field_array)+2;
$li->no_msg	    = $i_no_record_exists_msg;
$li->count_mode = 1;

$pos = 1;
$li->column_list .= "<th width='3%' class='num_check'>#</th>\n";
$li->column_list .= "<th width='20%'>".$li->column($pos++, $Lang['ePost']['EntryTitle'])."</th>\n";
$li->column_list .= "<th width='20%'>".$li->column($pos++, $Lang['ePost']['RequestTopic'])."</th>\n";
$li->column_list .= "<th width='20%'>".$li->column($pos++, $Lang['ePost']['Status'])."</th>\n";
$li->column_list .= "<th width='17%'>".$li->column($pos++, $Lang['ePost']['Deadline'])."</th>\n";
$li->column_list .= "<th width='20%'>".$li->column($pos++, $Lang['ePost']['PostedOnIssue'])."</th>\n";

$li->db_db_query("SET SESSION group_concat_max_len = 1048576"); // Set the max length of group concat to 1MB

include($intranet_root.'/home/ePost/noncourseware/templates/tpl_portfolio.php');

intranet_closedb();
?>