<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$RequestID = isset($RequestID) && $RequestID!=''? $RequestID : 0;
$RecordID  = isset($RecordID) && $RecordID!=''? $RecordID : 0;
$IsSave	   = isset($IsSave) && $IsSave!=''? $IsSave : 0;
$Header='Write';
# Initialize Object
$ePost   	  = new libepost();
$ePostUI 	  = new libepost_ui();
$ePostRequest = new libepost_request($RequestID);
$ePostWriter  = new libepost_writer($RecordID);
$maxUploadCnt = $cfg_ePost['max_upload'][$ePostRequest->ModuleCode];
# Check if ePost should be accessible
$ePost->auth();

# Check to show Teacher comment and InShowBoard
$show_Teacher_Comment = ($ePostWriter->Status==$cfg_ePost['sql_config']['Status']['submitted'] || ($ePostWriter->Status==$cfg_ePost['sql_config']['Status']['drafted'] && $ePostWriter->IsRedo==$cfg_ePost['sql_config']['IsRedo']['true']))  && $ePostWriter->TeacherComment!='';
$show_InShowBoard 	  = $ePostWriter->Status==$cfg_ePost['sql_config']['Status']['submitted'] && !is_null($ePostWriter->InShowBoard);
# Check writer template file is exist or not
$writer_template = $PATH_WRT_ROOT."home/ePost/noncourseware/templates/writer/".$ePostRequest->ModuleCode.".php";
$writer_template = file_exists($writer_template)? $writer_template : "";
if($writer_template=='') die("Writer template file not exist!");

include($intranet_root.'/home/ePost/noncourseware/templates/tpl_writer.php');

intranet_closedb();
?>