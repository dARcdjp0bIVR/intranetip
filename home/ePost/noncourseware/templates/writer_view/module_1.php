<?php
# Process Attachment

$attachment_http_path = array();
foreach($ePostWriter->Attachment as $_attachmentId => $_attachmentPathAry){
	$_attachment = $_attachmentPathAry['attachment'];
	$attachment_file_path = $ePostWriter->format_attachment_path($_attachment,false);
	if(file_exists($attachment_file_path) && !is_dir($attachment_file_path)){
		list($width, $height, $type, $attr) = getimagesize($attachment_file_path);
		   		
		if($width > $height){
			$final_width  = $width>$cfg_ePost['writer_photo']['max_width']? $cfg_ePost['writer_photo']['max_width'] : $width;
			$final_height = $width>$cfg_ePost['writer_photo']['max_width']? floor(($height/$width)*$cfg_ePost['writer_photo']['max_width']) : $height;

		}
		else if($width < $height){
			$final_width  = $height>$cfg_ePost['writer_photo']['max_height']? floor(($width/$height)*$cfg_ePost['writer_photo']['max_height']) : $width;
			$final_height = $height>$cfg_ePost['writer_photo']['max_height']? $cfg_ePost['writer_photo']['max_height'] : $height;
		}
		else{
			$final_width  = $width>$cfg_ePost['writer_photo']['max_width']? $cfg_ePost['writer_photo']['max_width'] : $width;
			$final_height = $height>$cfg_ePost['writer_photo']['max_height']? $cfg_ePost['writer_photo']['max_height'] : $height;
	    }
	    $attachment_http_path[$_attachmentId]['width'] = $final_width;
	    $attachment_http_path[$_attachmentId]['height'] = $final_height;
	    $attachment_http_path[$_attachmentId]['attachment'] = $ePostWriter->format_attachment_path($_attachment,true);
	}
}
?>

<span class="write_title"><?=$ePostWriter->Title?></span>
<span id="write_content" class="write_content">
	<?php foreach($attachment_http_path as $_attachmentId => $_attachmentPathAry){ ?>
		<table align="right" class="log_photo">
			<tr>
				<td class="log_photo_file"><a href="<?=$_attachmentPathAry['attachment']?>" target="_blank"><img src="<?=$_attachmentPathAry['attachment']?>" height='<?=$_attachmentPathAry['height']?>px' width='<?=$_attachmentPathAry['width']?>px'/></a></td>
			</tr>
			<?php if($ePostWriter->Attachment[$_attachmentId]['caption']){ ?>
				<tr>
					<td class="log_photo_desc"><?=$ePostWriter->Attachment[$_attachmentId]['caption']?></td>
				</tr>
			<?php } ?>
		</table>
	<?php } ?>
	<?=nl2br($ePostWriter->Content)?>
</span>