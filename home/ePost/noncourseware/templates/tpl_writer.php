<?php
include($intranet_root.'/home/ePost/noncourseware/templates/tpl_head.php');
?>

<script>
	
	$(document).ready(function(){
		<?php if($RecordID && $IsSave){?>
		if(!confirm('<?=$Lang['ePost']['Confirm']['WorkSuccessfullySaved']?>')){
			window.location = 'portfolio.php';
		}
		<?php } ?>
<?php for($i=1;$i<=$maxUploadCnt;$i++){?>
    	$('#fileupload<?=$i?>').change(function(){
    		if(!check_upload_file_valid()) return false;
    		if($(this).val()!=''){
  				$('#span_caption<?=$i?>').show();
    		}else{
  				$('#span_caption<?=$i?>').hide();    			
    		}
    	});
 <?php }?>		
	});
	
	
	function toogle_teacher_comment(ShowHide){
		if(ShowHide == 'show'){
			$('#teacher_comment_board').attr('style', 'visibility:visible');
		}
		else{
			$('#teacher_comment_board').attr('style', 'visibility:hidden');
		}
	}
	
	function go_index(){
		var obj = document.form1;
		
		if(confirm('<?=$Lang['ePost']['Confirm']['ChangesWontBeMade']?>')){
			obj.action = "index.php";
			obj.submit();
		}
		else
			return false;
	}
	
	function go_writer_view(){
		var obj = document.form1;
		
		if(confirm('<?=$Lang['ePost']['Confirm']['ChangesWontBeMade']?>')){
			obj.action = "writer_view.php";
			obj.submit();
		}
		else
			return false;
	}
	
	function go_submit(IsSubmit){
		var obj = document.form1;
		
		obj.status.value = IsSubmit;
		if(obj.title.value==''){
			alert('<?=$Lang['ePost']['Warning']['GiveYourWorkTitle']?>');
			return false;
		}			
		if(IsSubmit){
			if(obj.content.value==''){
				alert('<?=$Lang['ePost']['Warning']['InputContent']?>');
				return false;
			}
			
//			if(obj.title.value=='<?=$cfg_ePost['skip_update_title']?>'){
//				obj.title.value = '';
//			}
		}
		
		if(typeof(check_upload_file_valid)=='function'){
			if(!check_upload_file_valid()){
				return false;
			}
		}
			
		if(!IsSubmit || (IsSubmit && confirm('<?=$Lang['ePost']['Confirm']['Submit']?>'))){	
			obj.action = "writer_update.php";
			obj.submit();
		}
	}
	function MM_goToURL() { //v3.0
	  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
	function MM_showHideLayers() { //v9.0
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) 
	  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
		if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
		obj.visibility=v; }
	}
</script>
<form id="form1" name="form1" method="POST" enctype="multipart/form-data" style="padding:0;margin:0">
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
                
              
                    <div class="write_content_guide">
               
                           <div id="guide_board">
                                <div class="guide_content_board">
                                        <div class="request_row_top">
											<h1><?=$ePostRequest->Topic?>
												<span class="deadline_date"><?=$Lang['ePost']['SubmitOnOrBefore'][0].' <strong>'.date('Y-m-d', strtotime($ePostRequest->Deadline)).'</strong> '.$Lang['ePost']['SubmitOnOrBefore'][1]?></span>
											</h1>
										</div>
                                        <div class="request_row_bottom">
											<?=nl2br($ePostRequest->Description)?>
											 <input name="close_guide_btn" type="button" class="formsmallbutton"  onclick="MM_showHideLayers('guide_board','','hide');MM_showHideLayers('btn_guide_layer','','show')" value="Close" />        
                                        </div>
                                
                                 </div>
                             
                        </div>

                        <a href="#" title="guide" class="btn_guide" id="btn_guide_layer" onclick="MM_showHideLayers('guide_board','','show');MM_showHideLayers('btn_guide_layer','','hide')"><?=$Lang['ePost']['Guide']?></a>    
                   </div>
        
        
              	<div class="write_content_board">
					<?php include($writer_template); ?>
	    			
      			 <div class="edit_bottom"> 
                        <p class="spacer"></p>
						<input type="button" value="<?=$Lang['ePost']['Button']['Save']?>" onclick="go_submit(0)" class="formbutton" name="save_btn">
						<input type="button" value="<?=$Lang['ePost']['Button']['Submit_1']?>" onclick="go_submit(1)" class="formbutton" name="submit_btn">
						<input type="button" value="<?=$Lang['ePost']['Button']['Cancel']?>" onclick="<?=$RecordID? "go_writer_view()":"go_index()"?>" class="formsubbutton" name="cancel_btn">
                        <p class="spacer"></p>
    			  </div>
     		  </div>
              	<input type="hidden" id="RecordID" name="RecordID" value="<?=$RecordID?>"/>
				<input type="hidden" id="RequestID" name="RequestID" value="<?=$RequestID?>"/>
				<input type="hidden" id="status" name="status" value="1"/>
               <p class="spacer"></p> &nbsp;
                </div></div></div>
				
</form>
<?php
include($intranet_root.'/home/ePost/noncourseware/templates/tpl_foot.php');
?>