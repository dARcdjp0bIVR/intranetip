<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$RequestID = isset($RequestID) && $RequestID!=''? $RequestID : 0;
$RecordID  = isset($RecordID) && $RecordID!=''? $RecordID : 0;

# Initialize Object
$ePostUI 	  = new libepost_ui();
$ePostRequest = new libepost_request($RequestID);
$ePostWriter  = new libepost_writer($RecordID);
$li  = new libdb();
$Header='Portfolio';
# Check if ePost should be accessible
$ePostUI->auth();
$li->db_db_query("SET SESSION group_concat_max_len = 1048576"); // Set the max length of group concat to 1MB

# Check to show Teacher comment and InShowBoard
$show_Teacher_Comment = ($ePostWriter->Status==$cfg_ePost['sql_config']['Status']['submitted'] || ($ePostWriter->Status==$cfg_ePost['sql_config']['Status']['drafted'] && $ePostWriter->IsRedo==$cfg_ePost['sql_config']['IsRedo']['true']))  && $ePostWriter->TeacherComment!='';
$show_InShowBoard 	  = $ePostWriter->Status==$cfg_ePost['sql_config']['Status']['submitted'] && !is_null($ePostWriter->InShowBoard);
$sql = "SELECT
			IFNULL(GROUP_CONCAT(DISTINCT CONCAT('<div class=\"portfolio_item_list\" style=\"width:100%\"><a class=\"btn_show_newspaper\" title=\"".$Lang['ePost']['PostedOn'].": ', DATE_FORMAT(ea.inputdate, '%Y-%m-%d'), '\" href=\"javascript:openNewspaper(', en.NewspaperID, ', ', ep.PageID, ')\">', en.Title, ' - ', en.Name, '</a></div>') ORDER BY ea.inputdate ASC SEPARATOR '<br/>'), '&nbsp;') AS Issue
		FROM
			EPOST_WRITING AS ew INNER JOIN
			EPOST_NONCOURSEWARE_REQUEST AS encr ON ew.RequestID = encr.RequestID LEFT JOIN
			EPOST_ARTICLE_SHELF AS eas ON eas.Status = ".$cfg_ePost['BigNewspaper']['ArticleShelf_status']['exist']." AND ew.RecordID = eas.WritingID LEFT JOIN
			EPOST_NEWSPAPER_PAGE_ARTICLE AS ea ON eas.ArticleShelfID = ea.ArticleShelfID AND ea.Status = ".$cfg_ePost['BigNewspaper']['Article_status']['exist']." LEFT JOIN
			EPOST_NEWSPAPER_PAGE AS ep ON ea.PageID = ep.PageID AND ep.Status = ".$cfg_ePost['BigNewspaper']['Page_status']['exist']." LEFT JOIN
			EPOST_NEWSPAPER AS en ON ep.NewspaperID = en.NewspaperID AND en.Status = ".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']."
		WHERE
			ew.UserID = $UserID 
		AND
			ew.RecordID = '$RecordID'
		AND	
			ew.RequestID = '$RequestID'
		";
$issue_list = $li->returnVector($sql);

# Check writer template file is exist or not
$writer_template = $PATH_WRT_ROOT."home/ePost/noncourseware/templates/writer_view/".$ePostRequest->ModuleCode.".php";
$writer_template = file_exists($writer_template)? $writer_template : "";
if($writer_template=='') die("Writer template file not exist!");

include($intranet_root.'/home/ePost/noncourseware/templates/tpl_writer_view.php');

intranet_closedb();
?>