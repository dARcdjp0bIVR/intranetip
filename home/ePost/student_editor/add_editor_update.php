<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_studenteditor.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$EditorUserIDAry = isset($EditorUserIDAry) && $EditorUserIDAry!=''? $EditorUserIDAry : array();

# Initialize Object
$ePostStudentEditors = new libepost_studenteditors();

# Check if ePost should be accessible
$ePostStudentEditors->portal_auth();

# Found the users that are already editors
$IgnoreUserID = array();
for($i=0;$i<count($ePostStudentEditors->StudentEditors);$i++){
	$IgnoreUserID[] = $ePostStudentEditors->StudentEditors[$i]->EditorID;
}

# Assign the users as student editors
for($i=0;$i<count($EditorUserIDAry);$i++){
	$EditorUserID = $EditorUserIDAry[$i];
	
	if(count($IgnoreUserID)==0 || !in_array($EditorUserID, $IgnoreUserID)){
		$Studenteditor_Data_ary['UserID'] = $EditorUserID;
		$Studenteditor_Data_ary['Status'] = $cfg_ePost['BigNewspaper']['StudentEditor_status']['exist'];
		
		$StudentEditorObj = new libepost_studenteditor(0, $Studenteditor_Data_ary);
		
		$StudentEditorObj->update_studenteditor_db();
	}
} 

intranet_closedb();
header('Location:index.php');
?>
