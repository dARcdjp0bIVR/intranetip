<?php
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

<script>
	function go_filter(){
		document.filter_form.submit();
	}
	
	function go_delete(EditorID){
		var obj = document.form1;
		var StudentEditorClass = document.getElementById('StudentEditor_class_' + EditorID).innerHTML;
		var StudentEditorName  = document.getElementById('StudentEditor_name_' + EditorID).innerHTML;
		
		if(confirm('<?=$Lang['ePost']['Confirm']['RemoveStudentEditor'][0]?>' + StudentEditorClass + '<?=$Lang['ePost']['Confirm']['RemoveStudentEditor'][1]?>' + StudentEditorName + '<?=$Lang['ePost']['Confirm']['RemoveStudentEditor'][2]?>')){
			obj.action = 'delete_editor.php';
			obj.EditorID.value = EditorID;
			obj.submit();
		}
	}
</script>
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
<p class="spacer">&nbsp;</p>
<?=$ePostUI->gen_admin_publish_manager_page_nav(1)?>  
<p class="spacer">&nbsp;</p>
<?=$ePostUI->gen_portal_page_nav($nav_ary)?>
<p class="spacer">&nbsp;</p>
<form name="filter_form" method="post" action="index.php" style="margin:0;padding:0">
	<div class="content_top_tool">
		<div id="table_filter">
			<select name="Status" id="Status">
				<option value="<?=$cfg_ePost['BigNewspaper']['StudentEditor_filter']['All']?>" <?=$Status==$cfg_ePost['BigNewspaper']['StudentEditor_filter']['All']? "selected":""?>><?=$Lang['ePost']['Filter']['All']?></option>
				<option value="<?=$cfg_ePost['BigNewspaper']['StudentEditor_filter']['StudentwithRequestManaging']?>" <?=$Status==$cfg_ePost['BigNewspaper']['StudentEditor_filter']['StudentwithRequestManaging']? "selected":""?>><?=$Lang['ePost']['Filter']['StudentWithRequestsManaging']?></option>
				<option value="<?=$cfg_ePost['BigNewspaper']['StudentEditor_filter']['StudentwithIssueManaging']?>" <?=$Status==$cfg_ePost['BigNewspaper']['StudentEditor_filter']['StudentwithIssueManaging']? "selected":""?>><?=$Lang['ePost']['Filter']['StudentWithIssuesManaging']?></option>
			</select>
			<input name="filter_go_btn" type="button" class="formsmallbutton" value="Go" onclick="go_filter()"/>
		</div>
		<p class="spacer"></p>
		<br>
	</div>
</form>
<form name="form1" action="" method="post" style="margin:0;padding:0">
	<div class="Content_tool">
		<a href="add_editor.php" class="new"><?=$Lang['ePost']['AddStudentEditor']?></a>
	</div>
	<div id="sys_msg"><?=$Lang['ePost']['ReturnMessage'][$returnMsg]?></div>
	<div class="table_board">
		<table class="common_table_list edit_table_list">
			<colgroup>
				<col nowrap="nowrap">
			</colgroup>
			<tr>
				<th width="30">#</th>
				<th><?=$Lang['ePost']['Class']?></th>
				<th><?=$Lang['ePost']['Student']?></th>
				<th><?=$Lang['ePost']['ManagingRequest']?></th>
				<th><?=$Lang['ePost']['ManagingIssues']?></th>
				<th width="100">&nbsp;</th>
			</tr>
			<?=$table_html?>
		</table>
		<p class="spacer"></p>
		<p class="spacer"></p>
		<br>
	</div>
	<input type="hidden" id="EditorID" name="EditorID" value=""/>
	<input type="hidden" id="Status" name="Status" value="<?=$Status?>"/>
</form>
</div></div></div>
<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>