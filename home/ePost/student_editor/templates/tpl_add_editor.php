<?php 
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

	<script>
		function get_class_student_list(){
			var YearClassID = $("#YearClassSelect :selected").val();
			
			$("#EditorUserIDAry").find("option").remove().end();
			
			$.post(
					'ajax_get_student.php',
					{
						'YearClassID' : YearClassID
					},
					function(data){
						var student_arr = eval(data);
      					
      					$.each(student_arr, function(key, obj) {
        					var student_id   = obj.UserID;
        					var student_name = obj.StudentName;
      
        					$("#EditorUserIDAry").append($("<option></option>").attr("value",student_id).text(student_name));   
        					$("#EditorUserIDAry").width($("#EditorUserIDAry").parent().width());
      					});
					}
			);
		}
	
		function go_submit(IsMarkNext){
			var obj = document.form1;
			
			obj.action = 'add_editor_update.php';
			obj.submit();
		}
		
		function close_tb(){
			var obj = document.form1;
			obj.action = 'index.php';
			obj.submit();
		}
	</script>
	<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
	<p class="spacer">&nbsp;</p>
<?=$ePostUI->gen_admin_publish_manager_page_nav(1)?>  
<p class="spacer">&nbsp;</p>
<?=$ePostUI->gen_portal_page_nav($nav_ary)?>
<p class="spacer">&nbsp;</p>
	<form name="form1" method="post">
		<div class="main_board">
			<div class="main_board_top_left">
				<div class="main_board_top_right"></div>
			</div>
	  		<div class="main_board_left">
	  			<div class="main_board_right">
	        		<div class="table_board" style="min-height:325px">
						<p class="spacer"></p>
						<div class="marking_board">
	          				<div class="marking_area">
	            				<table class="form_table">
	            					<col width="18%"/>
	            					<col width="1"/>
	            					<col width="82%"/>
	            					<tr>
	            						<td><?=$Lang['ePost']['Class']?></td>
	            						<td>:</td>
	            						<td><?=$CurrentYearClassSelection?></td>
	            					</tr>
	            					<tr>
	            						<td><?=$Lang['ePost']['Students']?></td>
	            						<td>:</td>
	            						<td><?=$StudentSelectionFilter?></td>
	            					</tr>
	            				</table>
	        				</div>
	         				<p class="spacer"></p>
	        			</div>
	        			<p class="spacer"></p>
						<div class="edit_bottom">
	          				<p class="spacer"></p>
	          				<br>
	            			<input name="submit_btn" class="formbutton" value="<?=$Lang['ePost']['Button']['Submit']?>" type="button" onclick="go_submit(0)">
	            			<input name="cancel" class="formsubbutton" value="<?=$Lang['ePost']['Button']['Cancel']?>" type="button" onclick="close_tb()">
	          			</div>
	        		</div>
	        	</div>
	        </div>  
			<div class="main_board_bottom_left">
				<div class="main_board_bottom_right"></div>
	  		</div>
		</div>
	</form>
</div></div></div>

<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>