<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_studenteditor.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$EditorID = isset($EditorID) && $EditorID!=''? $EditorID : 0;

# Initialize Object
$ePostStudentEditor = new libepost_studenteditor($EditorID);

# Check if ePost should be accessible
$ePostStudentEditor->portal_auth();

# Delete Student Editor
$result = $ePostStudentEditor->delete_studenteditor();
$returnMsg = $result?"DeleteSuccess":"DeleteUnsuccess";
header('location:index.php?returnMsg='.$returnMsg);

intranet_closedb();
?>