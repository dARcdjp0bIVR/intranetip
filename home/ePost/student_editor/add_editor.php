<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_studenteditor.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

# Initialize Object
$lclass  = new libclass();
$ePostUI = new libepost_ui();
$ePostStudentEditors = new libepost_studenteditors();

# Check if ePost should be accessible
$ePostUI->portal_auth();
$Header='NEW';
$FromPage = 'editor';
# Current Year Class Selection Filter
$thisAttr = ' id="YearClassSelect" name="YearClassSelect" class="formtextbox" onchange="get_class_student_list();" ';
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
$CurrentYearClassSelection = $lclass->getSelectClassID($thisAttr, $selected="", $DisplaySelect=1, $CurrentAcademicYearID);
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>$Lang['ePost']['StudentEditorList'], "link"=>"index.php"); 
$nav_ary[] = array("title"=>$Lang['ePost']['AddStudentEditor'], "link"=>""); 
# Student Selection Filter
$StudentSelectionFilter  = "<select multiple=\"true\" size=\"10\" id=\"EditorUserIDAry\" name=\"EditorUserIDAry[]\" style=\"width: 441px;\">";
$StudentSelectionFilter .= "</select>";

include($intranet_root.'/home/ePost/student_editor/templates/tpl_add_editor.php');

intranet_closedb();
?>