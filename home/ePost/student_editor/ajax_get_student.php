<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_studenteditor.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$YearClassID = isset($YearClassID) && $YearClassID!=''? $YearClassID : 0;

# Initialize Object
$ePostStudentEditors = new libepost_studenteditors();
$json 				 = new JSON_obj();

# Check if ePost should be accessible
$ePostStudentEditors->portal_auth();

$IgnoreUserID = array();
for($i=0;$i<count($ePostStudentEditors->StudentEditors);$i++){
	$IgnoreUserID[] = $ePostStudentEditors->StudentEditors[$i]->UserID;
}

$sql = "SELECT
			iu.UserID,
			".getNameFieldWithClassNumberByLang ("iu.")." AS StudentName
		FROM
			INTRANET_USER iu INNER JOIN
			YEAR_CLASS_USER ycu ON iu.UserID = ycu.UserID INNER JOIN
			YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID
		WHERE
			ycu.YearClassID = $YearClassID AND
			".getNameFieldWithClassNumberByLang ("iu.")." != ''
			".(is_array($IgnoreUserID) && count($IgnoreUserID)? "AND iu.UserID NOT IN (".implode(',', $IgnoreUserID).")":"")."
		ORDER BY
			".Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN').", ycu.ClassNumber";
$student_ary = $ePostStudentEditors->returnArray($sql);

# Transform the student_ary to json string
$json_str = $json->encode($student_ary);

echo $json_str;

intranet_closedb();
?>