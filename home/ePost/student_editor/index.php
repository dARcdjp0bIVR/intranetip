<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_studenteditor.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$Status = isset($Status) && $Status!=''? $Status : 0;
$returnMsg = isset($returnMsg) && $returnMsg!=''? $returnMsg : "";

# Initialize Object
$ePostUI = new libepost_ui();
$ePostStudentEditors = new libepost_studenteditors();
# Check if ePost should be accessible
$ePostUI->portal_auth();
$Header='NEW';
$FromPage = 'editor';
# Process Table Content HTML
for($i=0;$i<count($ePostStudentEditors->StudentEditors);$i++){
	$StudentEditorObj = $ePostStudentEditors->StudentEditors[$i];
	
	switch($Status){
		case $cfg_ePost['BigNewspaper']['StudentEditor_filter']['All']: 
			$IsSkipped = false; 
			break;
		case $cfg_ePost['BigNewspaper']['StudentEditor_filter']['StudentwithRequestManaging']: 
			$IsSkipped = count($StudentEditorObj->ManagingRequest)==0;
			break;
		case $cfg_ePost['BigNewspaper']['StudentEditor_filter']['StudentwithIssueManaging']: 
			$IsSkipped = count($StudentEditorObj->ManagingNewspaper)==0;
			break;
	}
	
	if(!$IsSkipped){
		$table_html .= "<tr>";
		$table_html .= "	<td>".($i+1)."</td>";
		$table_html .= "	<td><span id='StudentEditor_class_".$StudentEditorObj->EditorID."'>".$StudentEditorObj->ClassName."-".$StudentEditorObj->ClassNumber."</span></td>";
		$table_html .= "	<td><span id='StudentEditor_name_".$StudentEditorObj->EditorID."'>".$StudentEditorObj->UserName."</span></td>";
		$table_html .= "	<td style='white-space:nowrap;'>".(count($StudentEditorObj->ManagingRequest)? implode('<br/>', $StudentEditorObj->ManagingRequest):'--')."</td>";
		$table_html .= "	<td style='white-space:nowrap;'>".(count($StudentEditorObj->ManagingNewspaper)? implode('<br/>', $StudentEditorObj->ManagingNewspaper):'--')."</td>";
		$table_html .= "	<td>";
		$table_html .= "		<div class=\"table_row_tool\">";
		$table_html .= "			<a href=\"javascript:go_delete(".$StudentEditorObj->EditorID.")\" class=\"tool_delete\" title=\"".$Lang['ePost']['Delete']."\">&nbsp;</a>";
		$table_html .= "		</div>";
		$table_html .= "	</td>";
		$table_html .= "</tr>";
	}
}

if(!$table_html){
	$table_html  = "<tr>";
	$table_html .= "	<td height='80' class='tabletext' colspan='6' style='text-align:center;vertical-align:middle'>$i_no_record_exists_msg</td>";
	$table_html .= "</tr>";
}
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>$Lang['ePost']['StudentEditorList'], "link"=>""); 

include($intranet_root.'/home/ePost/student_editor/templates/tpl_index.php');

intranet_closedb();
?>