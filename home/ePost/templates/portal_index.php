<?php

include($intranet_root.'/home/ePost/templates/portal_head.php');
?>
<script>
var type='<?=$type?>';
var sortby='<?=$sortby?>';
var order='<?=$order?>';
var offset=0;
var titleOrderAry = ['<?=$Lang['ePost']['OrderA2Z']?>','<?=$Lang['ePost']['OrderZ2A']?>'];
var dateOrderAry = ['<?=$Lang['ePost']['Oldest']?>','<?=$Lang['ePost']['Latest']?>'];
$(document).ready(function() {
	
   $('#issue_list').fancybox({
	  autoDimensions : false,
	  autoScale : false,
	  autoSize : false,
	  width : 800,
	  height : 600,
	  type : 'ajax',
	  padding: 0,
	  nextEffect: 'fade',
	  prevEffect: 'fade',
	
    });
	$('.show_more').click(function(){
		displayPost();
		return false;
    });
 	$('select#sortby').change(function(){
 		var arr = new Array(); 
 		var this_type = this.value;
		if(this_type=='date'){
			arr = dateOrderAry;
			$('#order').val('dec');
			$('#orderby').val('dec');
		}else{
			arr = titleOrderAry;	
			$('#order').val('asc');
			$('#orderby').val('asc');
		}
		$('select#orderby option').each(function(i){
			$(this).html(arr[i]);
		});
		sortPost($(this).val(),$('#order').val());
		return false;
   	});  
	
	displayPost();	  
});


	<?php if($ePost->is_editor){ ?>
		function openNewspaper(NewspaperID, UpdateView)
		{
			var intWidth  = screen.width;
		 	var intHeight = screen.height-100;
			window.open('/home/ePost/newspaper/view_newspaper.php?NewspaperID='+NewspaperID+'&UpdateView=1',"ePost_Newspaper","width="+intWidth+",height="+intHeight+",scrollbars,resizable");
		}
	<?php } else { ?>
		function openNewspaper(q_str)
		{
			var intWidth  = screen.width;
		 	var intHeight = screen.height-100; 
			window.open('/home/ePost/newspaper/view_newspaper.php?r='+q_str+'&UpdateView=1',"ePost_Newspaper","width="+intWidth+",height="+intHeight+",scrollbars,resizable");
		}
	<?php } ?>
	function displayPost(){
		var keyword = $('#keyword').val();
		var folderId = $('#NewspaperFolderID').val();
		var amount = type=='thumb'?9:10;
		$('#li_list').removeClass();
		$('#li_thumb').removeClass();
		$('#li_'+type).addClass('selected');
		$.fancybox.showLoading();
		$.post("/home/ePost/ajax.php", { task:'display_post',amount:amount,type:type,sortby:sortby,order:order,keyword:keyword,folderId:folderId,isPublic:0,offset:offset++},  
			function(data, textStatus){
				
				if(type=='list'){
					if (offset<=1){
						$('#issue_list_l').html(data);
					}else{
					
						$('.common_table_list').append(data);
					}
					$('#issue_list_l').show();
					$('#issue_list_t').hide();
					$('div.filter_bar .order_selection').hide();
				}else{
					if (offset<=1){
						$('#post_list').html(data);
					}else{
						$('#post_list').append(data);
					}
					$('#issue_list_t').show();
					$('#issue_list_l').hide();	
					$('div.filter_bar .order_selection').show();		
				}
				var issue_total = $('#display_'+type+'_issue_total').val();
				var range = issue_total;
				
				$('#issue_total').html(range);	
				if(offset*amount>=range){
					$('.show_more').hide();
				}else{
					$('.show_more').show();
				}
				$.fancybox.hideLoading();
			}		
		);
	}
	function switchFolder(){
		offset=0;
		displayPost();
		return false;
	}		
	function switchPost(t){
		type = t;
		offset=0;
		displayPost();
		return false;
	}	
	function sortPost(s,o){
		sortby=s;
		order=o;
		$('#orderby').val(order);
		offset=0;
		displayPost();
		return false;
	}	
	function search_keyword(e){
		var keyCode = (window.event) ? window.event.keyCode : e.which;
		var type = $('#type').val();
		offset = 0;
		if(keyCode == 13){
			displayPost();
		}
	}

</script>

<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
	<div class="portal_left" >
	
		<div class="filter_search">
			<div class="filter_bar">
				<span class="order_selection">
					<?=$Lang['ePost']['SortBy']?> : <select name="sortby" id="sortby">
					  <option value="title" <?=(($sortby=='title')?'selected':'')?>><?=$Lang['ePost']['IssueTitle']?></option>
					  <!--option value="name" <?=(($sortby=='name')?'selected':'')?>><?=$Lang['ePost']['IssueName']?></option-->
					  <option value="date" <?=(($sortby=='date')?'selected':'')?>><?=$Lang['ePost']['IssueDate']?></option>
					</select>
					<select name="orderby" id="orderby" onchange="sortPost($('#sortby').val(),$(this).val());">
					<?if($sortby=="date"){?>
					  <option value="asc" <?=(($order!='asc')?'selected':'')?>><?=$Lang['ePost']['Oldest']?></option>
					  <option value="dec" <?=(($order=='dec')?'selected':'')?>><?=$Lang['ePost']['Latest']?></option>	
					 <?}else{?>		
					  <option value="asc" <?=(($order!='asc')?'selected':'')?>><?=$Lang['ePost']['OrderA2Z']?></option>
					  <option value="dec" <?=(($order=='dec')?'selected':'')?>><?=$Lang['ePost']['OrderZ2A']?></option>		 	
					 <?}?>
					</select>
				</span>
				&nbsp;
				<span class="folder_selection">
					<?=$Lang['ePost']['Folder']?> : <?=$folderSelection?>
				</span>
			</div>
			<div class="search_bar">
				<input type="text" name="keyword" id="keyword" value="<?=$keyword?>" onkeypress="search_keyword(event)"/>
			</div>
		</div>	
		<div class="post_list_board">
			<span class="no_of_record"><?=$Lang['ePost']['Total']?> : <span id="issue_total"><?=count($ePostUI->post_ids)?></span> <?=$Lang['ePost']['Issues']?></span>
			<div class="toggle_tool">
				<ul>
						<li id="li_list"><a href="javascript:void(0)" onclick="switchPost('list');" class="view_list" title="<?=$Lang['ePost']['ListView']?>"></a></li>
						<li class="selected" id="li_thumb"><a href="javascript:void(0)" onclick="switchPost('thumb');" class="view_cover" title="<?=$Lang['ePost']['ViewThumb']?>"></a></li>
				</ul>
			</div>
			<div class="issue_list" id="issue_list_t">
				<!--post_list_board -->
				<div class="post_list_board_top"><div class="post_list_board_top_right"><div class="post_list_board_top_bg"></div></div></div>
					<div class="post_list_board_main"><div class="post_list_board_main_right"><div class="post_list_board_main_bg">
					<ul id="post_list"></ul>&nbsp;						
				</div></div></div>
				<div class="post_list_board_bottom"><div class="post_list_board_bottom_right"><div class="post_list_board_bottom_bg"></div></div></div>                   
				<!--post_list_board end -->				
			</div>
			<div class="issue_list" id="issue_list_l" style="display:none;"></div>
			<div class="show_more"><a href="#"><?=$Lang['ePost']['ShowMore']?></a></div>
		</div>
	</div>
	<div class="portal_right">
		<div class="board_postit">
			<h1 title="Post it!"></h1>
			<div class="btn_tool">
			<a href="noncourseware/index.php" class="btn_write"><?=$Lang['ePost']['WriteNow']?></a>
			 <a href="noncourseware/portfolio.php" class="btn_myrecord"><?=$Lang['ePost']['MySubmittedRecord']?></a>
			 </div>
			
		</div>
		&nbsp;
		<?php if($ePost->is_editor){ ?>
		<div class="board_editor">
			<div class="btn_tool">
				<a href="editor.php" class="btn_manage"><?=$Lang['ePost']['EditorPage']?></a>
			</div>
		</div>
		<?php } ?>
		
	</div>
<input type="hidden" name="order" id="order" value="<?=$order?>">
</div></div></div>
<div class="content_board_bottom"><div class="content_board_bottom_right"><div class="content_board_bottom_bg"></div></div></div>

<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>