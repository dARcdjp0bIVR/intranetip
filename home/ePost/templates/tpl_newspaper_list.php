<?php
$type = (isset($type) && !empty($type))?$type:'thumb';
$order = (isset($order) && !empty($order))?$order:0;
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>
<script>
var type='thumb';
var sortby='title';
var order='asc';
var offset=0;
$(document).ready(function() {
	
   $('#issue_list').fancybox({
	  autoDimensions : false,
	  autoScale : false,
	  autoSize : false,
	  width : 800,
	  height : 600,
	  type : 'ajax',
	  padding: 0,
	  nextEffect: 'fade',
	  prevEffect: 'fade',
	
    });
	$('.show_more').click(function(){
		displayPost();
		return false;
    });

	displayPost();	  
});


	<?php if($ePost->is_editor){ ?>
		function openNewspaper(NewspaperID, UpdateView)
		{
			var intWidth  = screen.width;
		 	var intHeight = screen.height-100;
			window.open('/home/ePost/newspaper/view_newspaper.php?NewspaperID='+NewspaperID+'&UpdateView=1',"ePost_Newspaper","width="+intWidth+",height="+intHeight+",scrollbars,resizable");
		}
	<?php } else { ?>
		function openNewspaper(q_str)
		{
			var intWidth  = screen.width;
		 	var intHeight = screen.height-100; 
			window.open('/home/ePost/newspaper/view_newspaper.php?r='+q_str+'&UpdateView=1',"ePost_Newspaper","width="+intWidth+",height="+intHeight+",scrollbars,resizable");
		}
	<?php } ?>
	function displayPost(){
		var keyword = $('#keyword').val();
		var amount = type=='thumb'?9:10;
		$('#li_list').removeClass();
		$('#li_thumb').removeClass();
		$('#li_'+type).addClass('selected');
		$.fancybox.showLoading();
		$.post("/home/ePost/ajax.php", { task:'display_post',amount:amount,type:type,sortby:sortby,order:order,keyword:keyword,isPublic:1,offset:offset++},  
			function(data, textStatus){
				$.fancybox.hideLoading();
				if(type=='list'){
					if (offset<=1){
						$('#issue_list_l').html(data);
					}else{
					
						$('.common_table_list').append(data);
					}
					$('#issue_list_l').show();
					$('#issue_list_t').hide();
				}else{
					if (offset<=1){
						$('#post_list').html(data);
					}else{
						$('#post_list').append(data);
					}
					$('#issue_list_t').show();
					$('#issue_list_l').hide();			
				}
				var issue_total = $('#display_'+type+'_issue_total').val();
				if(keyword==''){
					var range = <?=count($ePostUI->post_ids)?>;
				}else{
					var range = issue_total;
				}	
					
				$('#issue_total').html(range);	
				if(offset*amount>=range){
					$('.show_more').hide();
				}else{
					$('.show_more').show();
				}
				
			}		
		);
	}
	function switchPost(t){
		type = t;
		offset=0;
		displayPost();
		return false;
	}	
	function sortPost(s,o){
		sortby=s;
		order=o;
		offset=0;
		displayPost();
		return false;
	}	
	function search_keyword(e){
		var obj = (window.event) ? e : e.which;
		offset = 0;
		var type = $('#type').val();
		if(obj.keyCode == 13){
			displayPost('search',type);
		}
	}

</script>
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
    <div class="portal_full" >
        <div class="filter_search">
			<div class="filter_bar">
				<?=$Lang['ePost']['SortBy']?> : <select name="sortby" id="sortby" onchange="sortPost();">
				  <option value="title" <?=(($sortby=='title')?'selected':'')?>><?=$Lang['ePost']['IssueTitle']?></option>
				  <option value="name" <?=(($sortby=='name')?'selected':'')?>><?=$Lang['ePost']['IssueName']?></option>
				  <option value="date" <?=(($sortby=='date')?'selected':'')?>><?=$Lang['ePost']['IssueDate']?></option>
				</select>
			</div>
			<div class="search_bar">
				<input type="text" name="keyword" id="keyword" value="<?=$keyword?>" onkeypress="search_keyword(event)"/>
			</div>
		</div>
                        
                        
		<div class="post_list_board">
			<span class="no_of_record"><?=$Lang['ePost']['Total']?> : <span id="issue_total"><?=count($ePostUI->post_ids)?></span> <?=$Lang['ePost']['Issues']?></span>
			<div class="toggle_tool">
				<ul>
						<li id="li_list"><a href="javascript:void(0)" onclick="switchPost('list');" class="view_list" title="<?=$Lang['ePost']['ListView']?>"></a></li>
						<li class="selected" id="li_thumb"><a href="javascript:void(0)" onclick="switchPost('thumb');" class="view_cover" title="<?=$Lang['ePost']['ViewThumb']?>"></a></li>
				</ul>
			</div>
		
			<div class="issue_list" id="issue_list_t">
				<!--post_list_board -->
				<div class="post_list_board_top"><div class="post_list_board_top_right"><div class="post_list_board_top_bg"></div></div></div>
					<div class="post_list_board_main"><div class="post_list_board_main_right"><div class="post_list_board_main_bg">
					<ul id="post_list"></ul>&nbsp;						
				</div></div></div>
				<div class="post_list_board_bottom"><div class="post_list_board_bottom_right"><div class="post_list_board_bottom_bg"></div></div></div>                   
				<!--post_list_board end -->				
			</div>
			<div class="issue_list" id="issue_list_l" style="display:none;"></div>
			<div class="show_more"><a href="#"><?=$Lang['ePost']['ShowMore']?></a></div>
		</div>
    </div>
</div></div></div>

<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>