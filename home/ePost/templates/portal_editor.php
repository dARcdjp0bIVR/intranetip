<?php

include($intranet_root.'/home/ePost/templates/portal_head.php');
?>
<script>
$(document).ready(function(){
	$('#a_studentcomment_setting').click(function(){
		$(this).find("span").toggle();
		$(".editor_setting_area").toggle();
		return false;
    });	
 	$('form#student_comment_form').submit(function(){
 		var disable_student_comment = $('input[name=allow_student_comment]').attr("checked")?0:1;
 		var this_class,this_text;
		$.post("/home/ePost/ajax.php", { task:'update_general_setting',setting_name:'DisableStudentComment',setting_value:disable_student_comment},  
			function(xml, textStatus){
				if(disable_student_comment){
		 			this_class = 'btn_not_allow';
		 			this_text = '<?=$Lang['ePost']['NotAllow']?>';
		 		}else{
		 			this_class = 'btn_tick';
		 			this_text = '<?=$Lang['ePost']['Allow']?>';
		 		}
				$(".editor_setting_area").hide();
				$('div.btn_tool a.btn_manage').find("span").removeClass().addClass(this_class).html(this_text).show();
			}		
		);
		
		return false;
    });	  
});
</script>
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
	<div class="portal_editor">
		<h1><?=$Lang['ePost']['Requests']?></h1>
		<div class="board_editor">
			<div class="btn_tool">
			<?php if($ePost->user_obj->isTeacherStaff() && $ePost->is_editor){ ?>
				<a href="<?=$intranet_httppath?>/home/ePost/request_list/request_new.php" class="btn_new"><?=$Lang['ePost']['CreateNew']?></a>
			<?php } ?>
				<a href="<?=$intranet_httppath?>/home/ePost/request_list/index.php" class="btn_manage"><?=$Lang['ePost']['Manage']?></a>
			</div>
		</div>
    </div>
	<div class="portal_editor">
		<h1><?=$Lang['ePost']['ArticleShelf']?></h1>
		<div class="board_editor">
			<div class="btn_tool">
				<a href="<?=$intranet_httppath?>/home/ePost/article_shelf/index.php" class="btn_manage"><?=$Lang['ePost']['Manage']?></a>
			</div>
		</div>
	</div>
    <div class="portal_editor">
		<h1><?=$Lang['ePost']['Issues']?></h1>
		<div class="board_editor">
			<div class="btn_tool">
			<?php if($ePost->user_obj->isTeacherStaff() && $ePost->is_editor){ ?>
				<a href="<?=$intranet_httppath?>/home/ePost/newspaper/issue_new.php" class="btn_new"><?=$Lang['ePost']['CreateNew']?></a>
			<?php } ?>	
				<a href="<?=$intranet_httppath?>/home/ePost/newspaper/index.php" class="btn_manage"><?=$Lang['ePost']['Manage']?></a> 
			</div>
		</div>
    </div>
<?php if($ePost->user_obj->isTeacherStaff() && ($ePost->is_editor||$ePost->is_super_admin)){ ?>
    <div class="portal_editor_assign">
        <h1><?=$Lang['ePost']['AssigningEditors']?></h1>
            <div class="board_editor">
			<div class="btn_tool">
			<?php if($ePost->is_super_admin){?>
				<a href="<?=$intranet_httppath?>/home/ePost/publish_manager/index.php" class="btn_assign_ppl"><?=$Lang['ePost']['TeacherEditors']?>
				<?=$Lang['ePost']['TeacherEditorsDescription']?></a>
			<?php } ?>
				<a href="<?=$intranet_httppath?>/home/ePost/student_editor/index.php" class="btn_assign_ppl"><?=$Lang['ePost']['StudentEditors']?>
				<?=$Lang['ePost']['StudentEditorsDescription']?></a>
			</div>
		</div>
	</div>
	<?php if($ePost->is_super_admin){?>
	 <!-- setting-->
     <div class="portal_editor_setting">
      <h1><?=$Lang['General']['Settings']?></h1>
         <div class="board_editor">
        	<div class="btn_tool">
            	<a id="a_studentcomment_setting" href="javascript:void(0);" class="btn_manage"><?=$Lang['ePost']['StudentComment']?>
            		<span class="<?=($allow_student_comment?'btn_tick':'btn_not_allow')?>"><?=($allow_student_comment?$Lang['ePost']['Allow']:$Lang['ePost']['NotAllow'])?></span>
            	</a>
            	<p class="spacer"></p>
            	<div class="editor_setting_area" style="display:none;">
	            	<form id="student_comment_form" name="student_comment_form" method="post">
	            		<input id="allow_student_comment" name="allow_student_comment" type="checkbox" value="1" <?=($allow_student_comment?'checked':'')?>/> <label for="allow_student_comment" id="allow_label"><?=$Lang['ePost']['Allow']?></label>
	                    <input type="submit" name="submitBtn" class="formbutton" value="<?=$Lang['Btn']['Save']?>"  />
					</form>
               </div>  
            </div>
        </div>	
    </div>
    <?php } ?>
    <p class="spacer"></p>   
    <!---->	
<?php } ?>

</div></div></div>
				 

<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>