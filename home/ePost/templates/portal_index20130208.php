<?php
$type = (isset($type) && !empty($type))?$type:'thumb';
$ePostUI->sortBy = (isset($sortBy) && !empty($sortBy))?$sortBy:'title';
$ePostUI->orderBy = (isset($orderBy) && !empty($orderBy))?$orderBy:0;
$ePostUI->keyword = (isset($keyword) && !empty($keyword))?$keyword:'';
include($intranet_root.'/home/ePostNew/templates/portal_head.php');
?>
<script>
	function showMorePost(type){
		var cur_div = parseInt($('#div_post_last').val());
		var tot_div = parseInt($('#div_post_total').val());
		var start = cur_div+1;
		if(type=='thumb'){
			var range = 3;
		}else{
			var range = 10;
		}
		if(cur_div+range>=tot_div){
			var end = tot_div;
			$('#show_more_div').hide();
		}else{
			var end = cur_div+range;
		}

		for(var i=start;i<=end;i++){
			$('#div_post_row_'+i).show();
		}
	}
	<?php if($ePost->is_editor){ ?>
		function openNewspaper(NewspaperID, UpdateView)
		{
			var intWidth  = screen.width;
		 	var intHeight = screen.height;
			window.open('/home/ePostNew/newspaper/view_newspaper.php?NewspaperID='+NewspaperID+'&UpdateView='+UpdateView,"ePost_Newspaper","width="+intWidth+",height="+intHeight+",scrollbars,resizable");
		}
	<?php } else { ?>
		function openNewspaper(q_str)
		{
			var intWidth  = screen.width;
		 	var intHeight = screen.height; 
			window.open('/home/ePostNew/newspaper/view_newspaper.php?r='+q_str,"ePost_Newspaper","width="+intWidth+",height="+intHeight+",scrollbars,resizable");
		}
	<?php } ?>
	function displayPost(action,type){
		var orderBy = $('#orderBy').val();
		var sortBy = $('#sortBy').val();
		var cur_type = $('#type').val();
		var keyword = $('#keyword').val();
		if(action=='sort'){
			type = cur_type;
		}
		$('#li_list').removeClass();
		$('#li_thumb').removeClass();
		$('#li_'+type).addClass('selected');
		$.post("/home/ePostNew/ajax.php", { task:'display_post',type:type,orderBy:orderBy,sortBy:sortBy,keyword:keyword},  
			function(data, textStatus){
				var result = data.split('|=|');
				$('#div_issue_list').html(result[0]);	
				$('#a_title').removeClass();
				$('#a_name').removeClass();
				$('#a_date').removeClass();		
				$('#issue_total').html(result[1]);		
				if(orderBy==0){
					$('#a_'+sortBy).addClass('sort_asc');
				}else{
					$('#a_'+sortBy).addClass('sort_dec');
				}	
			}		
		);
	}
	function updateListOrder(sortBy){
		var cur_sortBy = $('#sortBy').val();
		var cur_orderBy = $('#orderBy').val();
		var cur_type = $('#type').val();
		if(cur_sortBy=sortBy){
			if(cur_orderBy==1){
				$('#orderBy').val(0);
				$('#a_'+sortBy).addClass('sort_asc');
			}else{
				$('#orderBy').val(1);
			}
		}			
		$('#sortBy').val(sortBy);
		
		displayPost('sort',cur_type);
	}
	function search_keyword(e){
		var obj = (window.event) ? e : e.which;
		var type = $('#type').val();
		if(obj.keyCode == 13){
			displayPost('search',type);
		}
	}
</script>
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
	<div class="portal_left" >
	
		<div class="filter_search">
			<div class="filter_bar">
				<?=$Lang['ePost']['SortBy']?> : <select name="sortBy" id="sortBy" onchange="displayPost('sort','');">
				  <option value="title" <?=(($sortBy=='title')?'selected':'')?>><?=$Lang['ePost']['IssueTitle']?></option>
				  <option value="name" <?=(($sortBy=='name')?'selected':'')?>><?=$Lang['ePost']['IssueName']?></option>
				  <option value="date" <?=(($sortBy=='date')?'selected':'')?>><?=$Lang['ePost']['IssueDate']?></option>
				</select>
			</div>
			<div class="search_bar">
				<input type="text" name="keyword" id="keyword" value="<?=$keyword?>" onkeypress="search_keyword(event)"/>
			</div>
		</div>	
		<?=$ePostUI->gen_issue_display_thumb_list($ePostFolders,false,$type)?>
	</div>
	<div class="portal_right">
		<div class="board_postit">
			<h1 title="Post it!"></h1>
			<div class="btn_tool">
			<a href="noncourseware/index.php" class="btn_write"><?=$Lang['ePost']['WriteNow']?></a>
			 <a href="noncourseware/portfolio.php" class="btn_myrecord"><?=$Lang['ePost']['MySubmittedRecord']?></a>
			 </div>
			
		</div>
		&nbsp;
		<?php if($ePost->is_editor){ ?>
		<div class="board_editor">
			<div class="btn_tool">
				<a href="editor.php" class="btn_manage"><?=$Lang['ePost']['Editors']?></a>
			</div>
		</div>
		<?php } ?>
		
	</div>

</div></div></div>
<input type="hidden" name="orderBy" id="orderBy" value="<?=$ePostUI->orderBy?>">
<div class="content_board_bottom"><div class="content_board_bottom_right"><div class="content_board_bottom_bg"></div></div></div>

<?php
include($intranet_root.'/home/ePostNew/templates/portal_foot.php');
?>