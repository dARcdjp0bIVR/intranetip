<?php

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();

$debug_mode = false;

if($plugin['ePost']){
	$li = new libdb();
	$li->db = $intranet_db; 
	
	# Create Table
	$sql_ary['EPOST_ARTICLE_SHELF'] = "
				CREATE TABLE `EPOST_ARTICLE_SHELF` (
					`ArticleShelfID` int(11) NOT NULL auto_increment,
					`WritingID` int(11) default '0',
					`Title` varchar(255) default NULL,
					`Content` text,
					`Attachment` text,
					`Caption` text,
					`IsAppendix` tinyint(1) default '0',
					`Status` tinyint(1) default '0',
					`Enjoy` text,
					`InputDate` datetime default NULL,
					`InputBy` int(11) default NULL,
					`ModifiedBy` int(11) default '0',
					`ModifiedDate` datetime default NULL,
					CoursewareRecordID int(11) default NULL,
					PRIMARY KEY  (`ArticleShelfID`),
     				KEY WritingID (WritingID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
				  
	$sql_ary['EPOST_FOLDER'] = "
				CREATE TABLE `EPOST_FOLDER` (
					`FolderID` int(11) NOT NULL auto_increment,
					`Title` varchar(255) default NULL,
					`Status` tinyint(1) default '0',
					`IsSystemPublic` tinyint(1) default '0',
					`InputDate` datetime default NULL,
					`ModifiedBy` int(11) default '0',
					`ModifiedDate` datetime default NULL,
					PRIMARY KEY  (`FolderID`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
				  
	$sql_ary['EPOST_NEWSPAPER'] = "
				CREATE TABLE `EPOST_NEWSPAPER` (
					`NewspaperID` int(11) NOT NULL auto_increment,
					`FolderID` int(11) default '0',
					`Title` varchar(255) default NULL,
					`Name` varchar(255) default NULL,
					`IssueDate` datetime default NULL,
					`OpenToPublic` tinyint(1) default '0',
					`Skin` varchar(8) default NULL,
					`SchoolLogo` text,
					`CoverBanner` text,
					`CoverBannerHideTitle` tinyint(1) default '0',
					`CoverBannerHideName` tinyint(1) default '0',
					`InsideBanner` text,
					`InsideBannerHideTitle` tinyint(1) default '0',
					`InsideBannerHideName` tinyint(1) default '0',
					`InsideBannerHidePageCat` tinyint(1) default '0',
					`NoOfView` int(11) default '0',
					`PublishStatus` tinyint(1) default '0',
					`Status` tinyint(1) default '0',
					`InputDate` datetime default NULL,
					`ModifiedBy` int(11) default '0',
					`ModifiedDate` datetime default NULL,
					PRIMARY KEY  (`NewspaperID`),
     				KEY FolderID (FolderID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
				  
	$sql_ary['EPOST_NEWSPAPER_PAGE'] = "
				CREATE TABLE `EPOST_NEWSPAPER_PAGE` (
					`PageID` int(11) NOT NULL auto_increment,
					`NewspaperID` int(11) default '0',
					`Type` char(2) default NULL,
					`Layout` tinyint(4) default '0',
					`Theme` tinyint(4) default '0',
					`ThemeName` text,
					`Name` varchar(255) default NULL,
					`PageOrder` tinyint(4) default '0',
					`Status` tinyint(1) default '0',
					`InputDate` datetime default NULL,
					`ModifiedBy` int(11) default '0',
					`ModifiedDate` datetime default NULL,
					PRIMARY KEY  (`PageID`),
     				KEY NewspaperID (NewspaperID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
				  
	$sql_ary['EPOST_NEWSPAPER_PAGE_ARTICLE'] = "
				CREATE TABLE `EPOST_NEWSPAPER_PAGE_ARTICLE` (
					`ArticleID` int(11) NOT NULL auto_increment,
					`ArticleShelfID` int(11) default '0',
					`PageID` int(11) default '0',
					`Position` int(11) default '0',
					`DisplayAuthor` tinyint(1) default '1',
					`DisplayTitle` tinyint(1) default '1',
					`DisplayContent` tinyint(1) default '1',
					`Alignment` tinyint(1) default '0',
					`Size` varchar(20) default NULL,
					`Type` varchar(20) default NULL,
					`Theme` varchar(20) default NULL,
					`Color` varchar(20) default NULL,
					`Status` tinyint(1) default '0',
					`InputDate` datetime default NULL,
					`ModifiedBy` int(11) default '0',
					`ModifiedDate` datetime default NULL,
					`Title` varchar(255) default NULL,
				    `Content` text,
				    `Enjoy` text,
				    `FontSize` tinyint(4) default NULL,
				    `FontStyle` tinyint(4) default NULL,
				    `LineHeight` tinyint(4) default NULL,
					PRIMARY KEY  (`ArticleID`),
     				KEY ArticleShelfID (ArticleShelfID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
	
	$sql_ary['EPOST_NONCOURSEWARE_REQUEST'] = "
				CREATE TABLE `EPOST_NONCOURSEWARE_REQUEST` (
					`RequestID` int(11) NOT NULL auto_increment,
					`Topic` varchar(255) default NULL,
					`Type` varchar(6) default NULL,
					`Deadline` datetime default NULL,
					`TargetLevel` varchar(255) default NULL,
					`Description` text,
					`Status` tinyint(1) default '0',
					`InputDate` datetime default NULL,
					`ModifiedBy` int(11) default '0',
					`ModifiedDate` datetime default NULL,
					PRIMARY KEY  (`RequestID`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
				  
	$sql_ary['EPOST_PUBLISH_MANAGER'] = "
				CREATE TABLE `EPOST_PUBLISH_MANAGER` (
					`ManagerID` int(11) NOT NULL auto_increment,
					`UserID` int(11) default '0',
					`Status` tinyint(1) default '0',
					`InputDate` datetime default NULL,
					`ModifiedBy` int(11) default '0',
					`ModifiedDate` datetime default NULL,
					PRIMARY KEY  (`ManagerID`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
	
	$sql_ary['EPOST_STUDENT_EDITOR'] = "
				CREATE TABLE `EPOST_STUDENT_EDITOR` (
					`EditorID` int(11) NOT NULL auto_increment,
					`UserID` int(11) default '0',
					`Status` tinyint(1) default '0',
					`InputDate` datetime default NULL,
					`ModifiedBy` int(11) default '0',
					`ModifiedDate` datetime default NULL,
					PRIMARY KEY  (`EditorID`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
				
	$sql_ary['EPOST_STUDENT_EDITOR_MANAGING'] = "
				CREATE TABLE `EPOST_STUDENT_EDITOR_MANAGING` (
					`ManageID` int(11) NOT NULL auto_increment,
					`EditorID` int(11) default '0',
					`ManageType` varchar(3) default NULL,
					`ManageItemID` int(11) default '0',
					`Status` tinyint(1) default '0',
					`InputDate` datetime default NULL,
					`ModifiedBy` int(11) default '0',
					`ModifiedDate` datetime default NULL,
					PRIMARY KEY  (`ManageID`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
				
	$sql_ary['EPOST_WRITING'] = "
				CREATE TABLE `EPOST_WRITING` (
					`RecordID` int(11) NOT NULL auto_increment,
					`RequestID` int(11) default '0',
					`UserID` int(11) NOT NULL default '0',
					`Title` varchar(255) default NULL,
					`Content` text,
					`Attachment` text,
					`Caption` text,
					`LevelCode` varchar(20) default NULL,
					`ModuleType` varchar(20) default NULL,
					`ModuleCode` varchar(20) default NULL,
					`ThemeCode` varchar(20) default NULL,
					`TopicCode` varchar(255) default NULL,
					`Status` tinyint(1) default '0',
					`IsRedo` tinyint(4) default '0',
					`TeacherComment` text,
					`InShowBoard` datetime default NULL,
					`InNewspaper` datetime default NULL,
					`inputdate` datetime default NULL,
					`modified` datetime default NULL,
					`ModifiedBy` datetime default NULL,
					`markdate` datetime default NULL,
					`AcademicYearID` INT(8),
					PRIMARY KEY  (`RecordID`),
     				KEY RequestID (RequestID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
	
//	$sql_ary['EPOST_WRITING_COMMENT'] = "
//				CREATE TABLE `EPOST_WRITING_COMMENT` (
//					`CommentID` int(11) NOT NULL auto_increment,
//					`WritingID` int(11) NOT NULL default '0',
//			     	`ArticleShelfID` int(11) default '0',
//					`Content` text,
//					`Status` tinyint(1) default '0',
//					`InputBy` int(11) default '0',
//					`InputDate` datetime default NULL,
//					`ModifiedBy` int(11) default '0',
//					`ModifiedDate` datetime default NULL,
//					PRIMARY KEY  (`CommentID`)
//				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
//				
	$sql_ary['EPOST_GENERAL_SETTINGS'] = "
				CREATE TABLE `EPOST_GENERAL_SETTINGS` (
					`SettingID` int(11) NOT NULL auto_increment,
					`SettingName` varchar(100) NOT NULL default '',
					`SettingValue` text,
					`TargetUserID` int(11) NOT NULL default '0', 
					`InputBy` int(11) default '0',
					`InputDate` datetime default NULL,
					`ModifiedBy` int(11) default '0',
					`ModifiedDate` datetime default NULL,
					PRIMARY KEY  (`SettingID`),
					UNIQUE KEY UserSetting (SettingName, TargetUserID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
	$sql_ary['EPOST_WRITING_ATTACHMENT'] = "
				CREATE TABLE EPOST_WRITING_ATTACHMENT (
			     AttachmentID int(11) NOT NULL auto_increment,
			     ArticleShelfID int(11) default '0',
			     WritingID int(11) default '0',
			     Attachment text,
			     Caption text,
			     ArticleShelfCaption text,
			     InputDate datetime default NULL,
			     InputBy int(11) default NULL,
			     ModifiedBy int(11) default '0',
			     ModifiedDate datetime default NULL,
			     PRIMARY KEY (AttachmentID),
     			KEY WritingID (WritingID),
     			KEY ArticleShelfID (ArticleShelfID)
			) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8";			
	$sql_ary['EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT'] = "
				CREATE TABLE EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT (
			     RecordID int(11) NOT NULL auto_increment,
			     ArticleID int(11) default '0',
				 AttachmentID int(11) default '0',
			     Attachment TEXT,
				 Caption TEXT,
			     Alignment tinyint(1) default '0',
			     Size varchar(20) default NULL,
			     Status tinyint(1) default '0',
			     InputDate datetime default NULL,
			     InputBy int(11) default NULL,
			     ModifiedBy int(11) default '0',
			     ModifiedDate datetime default NULL,
			     PRIMARY KEY (RecordID),
     			KEY ArticleID (ArticleID)
			) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8";		
	$sql_ary['EPOST_NEWSPAPER_PAGE_ARTICLE_COMMENT'] = "
				CREATE TABLE `EPOST_NEWSPAPER_PAGE_ARTICLE_COMMENT` (
					`CommentID` int(11) NOT NULL auto_increment,
					`ArticleID` int(11) NOT NULL default '0',
					`Content` text,
					`Status` tinyint(1) default '0',
					`InputBy` int(11) default '0',
					`InputDate` datetime default NULL,
					`ModifiedBy` int(11) default '0',
					`ModifiedDate` datetime default NULL,
					PRIMARY KEY  (`CommentID`),
     			KEY ArticleID (ArticleID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
												
	$html .= "<table border='1' cellspacing='0' cellpadding='0' width='100%'>";
	$html .= "	<col width='70%'>";
	$html .= "	<col width='30%'>";
	$html .= "	<tr>";
	$html .= "		<td><b><u>Flags</u></b></td>";
	$html .= "		<td>&nbsp;</td>";
	$html .= "	</tr>";
	$html .= "	<tr>";
	$html .= "		<td>".'$plugin[\'ePost\']'."</td>";
	$html .= "		<td style='".($plugin['ePost']? "color:blue":"color:red")."'>".($plugin['ePost']? "True":"False")."</td>";
	$html .= "	</tr>";
	$html .= "</table>";
	$html .= "<hr style='margin:20px 0'>";
	
	$html .= "<table border='1' cellspacing='0' cellpadding='0' width='100%'>";
	$html .= "	<col width='70%'>";
	$html .= "	<col width='30%'>";
	$html .= "	<tr>";
	$html .= "		<td><b><u>Tables</u></b></td>";
	$html .= "		<td>&nbsp;</td>";
	$html .= "	</tr>";
	
	$overall_result = true;
	
	foreach($sql_ary as $table_name=>$sql){
		$result = true;
		
		$check_sql    = "SHOW TABLES LIKE '$table_name'";
		$check_result = current($li->returnVector($check_sql));
		
		if($check_result==$table_name){
			$check_sql = "SELECT COUNT(*) FROM $table_name";
			$check_result = current($li->returnVector($check_sql));
			
			if($check_result>0){
				$result = false;
				$result_str = "Table already exists with records!";
				$overall_result = false;
			}
			else{
				$check_sql = "DROP TABLE $table_name";
				if(!$debug_mode)
					$li->db_db_query($check_sql);
				
				$result_str = "Table already exists with no records!<br/>Table dropped to ensure table with latest schema is created!<br/>";
			}
		}
		
		if($result){
			if(!$debug_mode){
				$result = $li->db_db_query($sql);
				$result_str .= $result? "Table created":"Failed to create table";
				$overall_result = $result? $overall_result:false;
			}
		}
		
		$html .= "<tr>";
		$html .= "	<td>$table_name</td>";
		$html .= "	<td style='".($result? "color:blue":"color:red")."'>".($result? "Created":"Failed")."<br/>$result_str</td>";
		$html .= "</tr>";
		
		$result_str = "";
	}
	
	$html .= "</table>";
	$html .= "<hr style='margin:20px 0'>";
	
	# Insert Default Folder Record
	$result = true;
	
	$html .= "<table border='1' cellspacing='0' cellpadding='0' width='100%'>";
	$html .= "	<col width='70%'>";
	$html .= "	<col width='30%'>";
	$html .= "	<tr>";
	$html .= "		<td><b><u>Default SQL</u></b></td>";
	$html .= "		<td>&nbsp;</td>";
	$html .= "	</tr>";

	$sql = "SELECT COUNT(*) FROM EPOST_FOLDER WHERE IsSystemPublic = 1";
	$Count = current($li->returnVector($sql));
	$result = false;
	if($Count==0){
		$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = 'broadlearning'";
		$BLUserID = current($li->returnVector($sql));
	
			
		$sql = "INSERT INTO EPOST_FOLDER (Title, Status, IsSystemPublic, InputDate, ModifiedBy, ModifiedDate) VALUES ('Public', 0, 1, NOW(), $BLUserID, NOW())";
		if(!$debug_mode){
			$result = $li->db_db_query($sql);
		}
	}
	$html .= "	<tr>";
	$html .= "		<td>Insert Default Folder Record</td>";
	$html .= "		<td style='".($result? "color:blue":"color:red")."'>".($result? "Inserted":"Failed")."</td>";
	$html .= "	</tr>";
	$html .= "</table>";
	
	$html .= "<hr style='margin:20px 0'>";
	$html .= "<div width='100%' align='center' style='font-size:50px'>".($overall_result? "All tables are created successfully!":"Some tables are not created!")."</div>";
	
	echo $html;			  
}
else{
	echo "ePost are not enabled in this site.";
}

intranet_closedb();
?>