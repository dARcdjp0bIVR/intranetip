<?php
# using :

$PATH_WRT_ROOT = "../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");

intranet_opendb();

$ePost   	   = new libepost();
$ePostUI 	   = new libepost_ui();
$ePostFolders  = new libepost_folders($folderId);
$ePostRequests = new libepost_requests();

# Check if ePost should be accessible
$ePost->auth();
switch($_POST['task']){
	case "display_post":
		$SearchArr = array(
						  'type'=>$type,
						  'order'=>$order,
						  'keyword'=>$keyword,
						  'offset'=>$offset,
						  'sortby'=>$sortby,
						  'amount'=>$amount
					);		
		list($result_total,$NewsArr) = $ePostUI->gen_post_list($ePostFolders,$SearchArr,$isPublic);
		if($type=='thumb'){
			include_once("templates/portal_thumb.php");
		}else{
			include_once("templates/portal_list.php");
		}
	break;	
	case "add_new_issue_folder":
		$sql = "INSERT INTO
					EPOST_FOLDER
					(
						Title, Status, InputDate, ModifiedBy, ModifiedDate
					)
				VALUES
					(
						'$folder_title', '".$cfg_ePost['BigNewspaper']['Folder_status']['exist']."', NOW(), '$UserID', NOW()
				)
		";
		$ePost->db_db_query($sql);
		$folder_id = $ePost->db_insert_id();
		$FolderObj = new libepost_folder($folder_id);
		if($folder_id){
			$table_html = "";
			$table_html .= "<tr id='tr_row_".($new_row_no)."'>";
			$table_html .= "	<td>".($new_row_no)."</td>";
			$table_html .= "	<td><a id=\"Folder_title_".$FolderObj->FolderID."\" href=\"javascript:openFolder(".$FolderObj->FolderID.")\">".($FolderObj->Folder_IsSystemPublic==$cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']? $Lang['ePost']['PublicFolder']:$FolderObj->Folder_Title)."</a></td>";
			$table_html .= "	<td>0</td>";
	//		$table_html .= "	<td>0</td>";
			$table_html .= "	<td>".date('Y-m-d', strtotime($FolderObj->Folder_ModifiedDate))." (".$FolderObj->Folder_ModifiedByUser.")</td>";
			$table_html .= "	<td>";
			
			if($FolderObj->Folder_IsSystemPublic != $cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']){
				$table_html .= "	<div class=\"table_row_tool\">";
				$table_html .= "		<a href=\"javascript:go_edit(".$FolderObj->FolderID.")\" class=\"tool_edit\" title=\"".$Lang['ePost']['Edit']."\">&nbsp;</a>";
				$table_html .= $ePost->user_obj->isTeacherStaff() && $ePost->is_editor? "<a href=\"javascript:go_delete(".$FolderObj->FolderID.")\" class=\"tool_delete\" title=\"".$Lang['ePost']['Delete']."\">&nbsp;</a>":"";
				$table_html .= "	</div>";
			}
			else{
				$table_html .= "	&nbsp;";
			}
			
			$table_html .= "	</td>";
			$table_html .= "</tr>";
		}
		echo $table_html."|=|".($new_row_no+1);
	break;
	case 'update_general_setting':
		$ePost->set_general_setting($setting_name, $setting_value, $target_user_id);
	break;
	case 'gen_video_player':
		$IsSelectArticlePage = $IsSelectArticlePage?$IsSelectArticlePage:false;
		$width = $width?$width:320;
		$height = $height?$height:255;
		echo $ePostUI->gen_video_player($path, $IsSelectArticlePage, $player_id, $width, $height);
	break;	
}
intranet_closedb();
?>