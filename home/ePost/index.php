<?php
# using :

$PATH_WRT_ROOT = "../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");

intranet_auth();
intranet_opendb();
//$lu			   = new libuser($UserID);
$ePost   	   = new libepost();
$ePostUI 	   = new libepost_ui();
$ePostFolders  = new libepost_folders();
$ePostRequests = new libepost_requests();
$type = (isset($type) && !empty($type))?$type:'thumb';
$order = (isset($order) && !empty($order))?$order:'dec';
$sortby = (isset($sortby) && !empty($sortby))?$sortby:'date';
$Header='NEW';
$FromPage = 'portal';
# Check if ePost should be accessible
$ePost->auth();
$ePostUI->post_ids = $ePostUI->get_post_ids($ePostFolders);

$DisabledNonCourseware = count($ePostRequests->get_user_assigned_requests($cfg_ePost['Noncourseware_Request']['Status']['DeadlineNotPassed']))<=0;
$folderSelection = $ePostUI->gen_epost_folder_selection($ePostFolders);
include($intranet_root.'/home/ePost/templates/portal_index.php');
// For performance tunning info
//$runTime = StopTimer($precision=5, $NoNumFormat=false, 'ePostRunTime');
//echo '<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>';
//debug_pr('$runTime = '.$runTime.'s');
//debug_pr('convert_size(memory_get_usage()) = '.convert_size(memory_get_usage()));
//debug_pr('convert_size(memory_get_usage(true)) = '.convert_size(memory_get_usage(true)));
//debug_pr('Query Count = '.$GLOBALS[debug][db_query_count]);
//$ePost->db_show_debug_log_by_query_number(1);
//die();
intranet_closedb();
?>