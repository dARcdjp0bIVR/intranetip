<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();

$FileUploadAction = isset($FileUploadAction) && $FileUploadAction!=''? $FileUploadAction : 'unknown';

$json = new JSON_obj();

$FileLocation = $_FILES['FileUpload']['tmp_name'];
$FileName 	  = $_FILES['FileUpload']['name'];

if($FileLocation && in_array(get_file_ext($FileName),$image_exts)){ // $image_exts is already defined in lib.php
	$tmp_folder_name = uniqid(date('YmdHis_'));
	
	if(!is_dir($PATH_WRT_ROOT.'file/ePost'))
		 mkdir($PATH_WRT_ROOT.'file/ePost', 0777);
	if(!is_dir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment'))
		 mkdir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment', 0777);
	if(!is_dir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/tmp'))
		 mkdir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/tmp', 0777);
	if(!is_dir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/tmp/'.$FileUploadAction))
		 mkdir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/tmp/'.$FileUploadAction, 0777);
	if(!is_dir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/tmp/'.$FileUploadAction.'/'.$tmp_folder_name))
		 mkdir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/tmp/'.$FileUploadAction.'/'.$tmp_folder_name, 0777);
	
	move_uploaded_file($FileLocation, $PATH_WRT_ROOT.'file/ePost/newspaper_attachment/tmp/'.$FileUploadAction.'/'.$tmp_folder_name.'/'.$FileName);
	
	$result    		 		= file_exists($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/tmp/'.$FileUploadAction.'/'.$tmp_folder_name.'/'.$FileName);
	$upload_file_path 		= '/tmp/'.$FileUploadAction.'/'.$tmp_folder_name.'/';
	$upload_file_name 	  	= $FileName;
	$upload_file_http_path 	= '/file/ePost/newspaper_attachment/tmp/'.$FileUploadAction.'/'.$tmp_folder_name.'/'.$FileName;
}
else{
	$result    		  		= false;
	$upload_file_path 		= null;
	$upload_file_name 	   	= null;
	$upload_file_http_path 	= null;
}

echo $json->encode(array('result'=>$result,
						 'upload_action'		 => $FileUploadAction,
						 'upload_file_path'		 => $upload_file_path, 
						 'upload_file_name'		 => $upload_file_name,
						 'upload_file_http_path' => $upload_file_http_path));
?>