<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_studenteditor.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$NewspaperID						= isset($NewspaperID) && $NewspaperID!=''? $NewspaperID : 0;
$PageID								= isset($PageID) && $PageID!=''? $PageID : 0;
$TargetPage							= isset($TargetPage) && $TargetPage!=''? $TargetPage : 1;
$NewspaperSkin 		    			= isset($NewspaperSkin) && $NewspaperSkin!=''? $NewspaperSkin : ''; 
$NewspaperSchoolLogo				= isset($NewspaperSchoolLogo) && $NewspaperSchoolLogo!=''? $NewspaperSchoolLogo : '';
$NewspaperCoverBanner				= isset($NewspaperCoverBanner) && $NewspaperCoverBanner!=''? $NewspaperCoverBanner : '';
$NewspaperCoverBannerHideTitle 		= isset($NewspaperCoverBannerHideTitle) && $NewspaperCoverBannerHideTitle!=''? $NewspaperCoverBannerHideTitle : 0;
$NewspaperCoverBannerHideName 		= isset($NewspaperCoverBannerHideName) && $NewspaperCoverBannerHideName!=''? $NewspaperCoverBannerHideName : 0;
$NewspaperInsideBanner				= isset($NewspaperInsideBanner) && $NewspaperInsideBanner!=''? $NewspaperInsideBanner : '';
$NewspaperInsideBannerHideTitle 	= isset($NewspaperInsideBannerHideTitle) && $NewspaperInsideBannerHideTitle!=''? $NewspaperInsideBannerHideTitle : 0;
$NewspaperInsideBannerHideName 		= isset($NewspaperInsideBannerHideName) && $NewspaperInsideBannerHideName!=''? $NewspaperInsideBannerHideName : 0;
$NewspaperInsideBannerHidePageCat 	= isset($NewspaperInsideBannerHidePageCat) && $NewspaperInsideBannerHidePageCat!=''? $NewspaperInsideBannerHidePageCat : 0;

# Initialize Object
$ePostNewspaper = new libepost_newspaper($NewspaperID);

# Check if ePost should be accessible
$ePostNewspaper->portal_auth();

$ePostNewspaper->Newspaper_Skin 	 	 			= $NewspaperSkin;
$ePostNewspaper->Newspaper_SchoolLogo	 			= $NewspaperSchoolLogo;
$ePostNewspaper->Newspaper_CoverBanner 				= $NewspaperCoverBanner;
$ePostNewspaper->Newspaper_CoverBannerHideTitle 	= $NewspaperCoverBanner? $NewspaperCoverBannerHideTitle:0;
$ePostNewspaper->Newspaper_CoverBannerHideName 		= $NewspaperCoverBanner? $NewspaperCoverBannerHideName:0;
$ePostNewspaper->Newspaper_InsideBanner 			= $NewspaperInsideBanner;
$ePostNewspaper->Newspaper_InsideBannerHideTitle 	= $NewspaperInsideBanner? $NewspaperInsideBannerHideTitle:0;
$ePostNewspaper->Newspaper_InsideBannerHideName 	= $NewspaperInsideBanner? $NewspaperInsideBannerHideName:0;
$ePostNewspaper->Newspaper_InsideBannerHidePageCat 	= $NewspaperInsideBanner? $NewspaperInsideBannerHidePageCat:0;

$ePostNewspaper->update_newspaper_db($StudentEditorSelected);

$target_page = ($PageID && $TargetPage==2)? "issue_page_new.php":"issue_list.php";

intranet_closedb();
?>
<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery-1.3.2.min.js"></script>
<form name="form1" method="post" action="<?=$target_page?>">
	<input type="hidden" id="NewspaperID" name="NewspaperID" value="<?=$NewspaperID?>"/>
	<input type="hidden" id="PageID" name="PageID" value="<?=$PageID?>"/>
	<input type="hidden" id="FromPage" name="FromPage" value="new_design"/>
	<input type="hidden" id="FolderID" name="FolderID" value="<?=$ePostNewspaper->FolderID?>"/>
</form>
<script>
	$(document).ready(function(){
	<?php if($IsPopup){ ?>
		parent.window.updateIssueSkin();
		parent.window.tb_remove();
	<?php }else{ ?>
		document.form1.submit();
	<?php } ?>	
	});
</script>