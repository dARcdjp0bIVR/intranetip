<?php

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$NewspaperID = isset($NewspaperID) && $NewspaperID!=''? $NewspaperID : 0;
$PageID		 = isset($PageID) && $PageID!=''? $PageID : 0;

# Initialize Object
$ePostUI 	    = new libepost_ui();
$ePostNewspaper = new libepost_newspaper($NewspaperID);
$CoverPageID = $ePostNewspaper->Newspaper_PageIDs[0];
# Check if ePost should be accessible
$ePostUI->portal_auth();



switch($_POST['task']){
	case "refresh_page":
		if($PageID){
			$ePostPage = new libepost_page($PageID);
		}
		$Type   = $PageID? $ePostPage->Page_Type   : 'I3';
		$Layout = $PageID? $ePostPage->Page_Layout : 1;
		$Theme	= $PageID? $ePostPage->Page_Theme  : 1;
		$Name	= $PageID? $ePostPage->Page_Name   : '';
		$PageTotal = count($ePostNewspaper->Newspaper_PageIDs);	
		if($PageTotal){
			$Pages_ary = $ePostNewspaper->get_pages();
			$selected_flag = false;
			$li_html = '<ul id="ul_issue_page_list">';
			$newspaper_body_html = '';
			for($i=0;$i<count($Pages_ary);$i++){
				$PageObj = $Pages_ary[$i];
				if(($PageID==0 && $i==0) || ($PageObj->PageID == $PageID)){
					$newspaper_body_html .= $ePostUI->gen_single_big_page_layout($PageObj, true, true);
					$PageID = $PageObj->PageID;
				}		
				if($i==0){
					$li_html .= '<li id="li_page_'.($PageObj->PageID).'" class="li_page '.(((empty($PageID)&&$i==0)||(!empty($PageID)&&$PageObj->PageID==$PageID))?'selected_page ':(($CoverPageID==$PageObj->PageID)?'':'')).' ui-state-default ui-state-disabled">
						<a id="a_'.$PageObj->PageID.'" href="javascript:displayPage('.$PageObj->PageID.');">
						<em class="li_page_no">'.($i+1).')</em>
						<span class="page_structure"><img src="'.$intranet_httppath.'/images/ePost/issue_layout/'.$cfg_ePost['BigNewspaper']['TypeLayout'][$PageObj->Page_Type][$PageObj->Page_Layout]['img'].'" /></span>
						<h3>'.$PageObj->Page_Name.'</h3>
						</a>
					</li>';
				}else{
					$li_html .= '<li id="li_page_'.($PageObj->PageID).'" class="li_page '.(((empty($PageID)&&$i==0)||(!empty($PageID)&&$PageObj->PageID==$PageID))?'selected_page':'').'">
						<em class="li_page_no">'.($i+1).')</em>
						<div class="table_row_tool" >'. 
                          	(($ePostUI->user_obj->isTeacherStaff() && $ePostUI->is_editor)?'<a href="javascript:deletePage('.$PageObj->PageID.');" title="'.$Lang['ePost']['DeletePage'].'" class="tool_delete">&nbsp;</a>':'')
						.'</div>
						<a id="a_'.$PageObj->PageID.'" href="javascript:displayPage('.$PageObj->PageID.');">
						<span class="page_structure"><img src="'.$intranet_httppath.'/images/ePost/issue_layout/'.$cfg_ePost['BigNewspaper']['TypeLayout'][$PageObj->Page_Type][$PageObj->Page_Layout]['img'].'" /></span>
						<h3>'.$PageObj->Page_Name.'</h3>
						</a>  
					</li>';				
				}
			}
			$li_html .= '</ul>';
		}
		$Status = ((!empty($ePostNewspaper->Newspaper_PublishStatus)))?$ePostNewspaper->Newspaper_PublishStatus:0;	
		//array($ParTag, $ParValue, $ParCData, $ParSpcChar)
		$returnAry =array(
			array('li_html',$li_html,true,false),
			array('PageID',$PageID,true,false),
			array('newspaper_body_html',$newspaper_body_html,true,false),
			array('PageTotal',$PageTotal,false,false),
			array('Status',$Status,false,false)
		);
	break;
	case "update_page":
		# Initialize Variable
		$PageType 		   = isset($PageType) && $PageType!=''? $PageType : '';
		$PageLayout 	   = isset($PageLayout) && $PageLayout!=''? $PageLayout : '';
		$PageTheme 		   = isset($PageTheme) && $PageTheme!=''? $PageTheme : '';
		$PageThemeName	   = isset($PageThemeName) && $PageThemeName!=''? $PageThemeName : '';
		$PageName 		   = isset($PageName) && $PageName!=''? $PageName : '';
		$PageRefPage 	   = isset($PageRefPage) & $PageRefPage!=''? $PageRefPage : '';
		$PageLayoutChanged = isset($PageLayoutChanged) && $PageLayoutChanged!=''? $PageLayoutChanged : 0;
		$action = ($PageID)?'edit':'new';
		# Initialize Object
		$ePostNewspaper = new libepost_newspaper($NewspaperID);

		# Check if ePost should be accessible
		$ePostNewspaper->portal_auth();

		if($PageID){
			$ePostPage = new libepost_page($PageID);
			
			$ePostPage->Page_Type 		= $PageType;
			$ePostPage->Page_Layout 	= $PageLayout;
			$ePostPage->Page_Theme 		= $PageTheme;
			$ePostPage->Page_ThemeName 	= $PageThemeName;
			$ePostPage->Page_Name 		= $PageName;
			$ePostPage->Page_Status 	= $cfg_ePost['BigNewspaper']['Page_status']['exist'];
			
			if($PageLayoutChanged){
				$Articles_ary = $ePostPage->get_articles();
				if(is_array($Articles_ary) && count($Articles_ary)){
					foreach($Articles_ary as $ArticleObj){
						if(!is_null($ArticleObj) && is_object($ArticleObj)){
							# Delete Article
							$result = $ArticleObj->delete_article();
						}
					}
				}
			}
		}
		else{
			# Prepare $Page_Data_ary for Page Object
			$PageOrder = count($ePostNewspaper->Newspaper_PageIDs);
			
			$Page_Data_ary['NewspaperID'] 	 = $NewspaperID;
			$Page_Data_ary['Page_Type']   	 = $PageType;
			$Page_Data_ary['Page_Layout'] 	 = $PageLayout;
			$Page_Data_ary['Page_Theme']  	 = $PageTheme;
			$Page_Data_ary['Page_ThemeName'] = $PageThemeName;
			$Page_Data_ary['Page_Name']   	 = $PageName;
			$Page_Data_ary['Page_PageOrder'] = $PageOrder;
			$Page_Data_ary['Page_Status'] 	 = $cfg_ePost['BigNewspaper']['Page_status']['exist'];
			
			$Newspaper_Data_ary = $ePostNewspaper->newspaper_obj_to_array();
			$Page_Data_ary = array_merge($Newspaper_Data_ary, $Page_Data_ary);
			
			$ePostPage = new libepost_page(0, $Page_Data_ary);			
		}
		$ePostPage->update_page_db($PageRefPage);
		
		$PageID = $ePostPage->PageID;
		$ePostNewspaper = new libepost_newspaper($NewspaperID);
		$Pages_ary = $ePostNewspaper->get_pages(); //get new page order
		
		$newspaper_body_html = '';
		for($i=0;$i<count($Pages_ary);$i++){
			$PageObj = $Pages_ary[$i];
			if($PageObj->PageID == $PageID){
				$newspaper_body_html .= $ePostUI->gen_single_big_page_layout($PageObj, true, true);
			}		
			if($i==0){
				$li_html .= '<li id="li_page_'.($PageObj->PageID).'" class="li_page '.(((empty($PageID)&&$i==0)||(!empty($PageID)&&$PageObj->PageID==$PageID))?'selected_page ':(($CoverPageID==$PageObj->PageID)?'':'')).' ui-state-default ui-state-disabled">
					<a id="a_'.$PageObj->PageID.'" href="javascript:displayPage('.$PageObj->PageID.');">
					<em class="li_page_no">'.($i+1).')</em>
					<span class="page_structure"><img src="'.$intranet_httppath.'/images/ePost/issue_layout/'.$cfg_ePost['BigNewspaper']['TypeLayout'][$PageObj->Page_Type][$PageObj->Page_Layout]['img'].'" /></span>
					<h3>'.$PageObj->Page_Name.'</h3>
					</a>
				</li>';
			}else{
				$li_html .= '<li id="li_page_'.($PageObj->PageID).'" class="li_page '.(((empty($PageID)&&$i==0)||(!empty($PageID)&&$PageObj->PageID==$PageID))?'selected_page':'').'">
					<em class="li_page_no">'.($i+1).')</em>
					<div class="table_row_tool" > 
						<a href="javascript:deletePage('.$PageObj->PageID.');" title="'.$Lang['ePost']['DeletePage'].'" class="tool_delete">&nbsp;</a>
					</div>
					<a id="a_'.$PageObj->PageID.'" href="javascript:displayPage('.$PageObj->PageID.');">
					<span class="page_structure"><img src="'.$intranet_httppath.'/images/ePost/issue_layout/'.$cfg_ePost['BigNewspaper']['TypeLayout'][$PageObj->Page_Type][$PageObj->Page_Layout]['img'].'" /></span>
					<h3>'.$PageObj->Page_Name.'</h3>
					</a>  
				</li>';				
			}
		}
		$returnAry =array(
			array('page_id',$ePostPage->PageID,true,false),
			array('li_html',$li_html,true,false),
			array('action',$action,true,false),
			array('page_total',count($Pages_ary),true,false),
			array('newspaper_body_html',$newspaper_body_html,true,false)
		);
	break;
	case "delete_page":
		# Initialize Object
		$page_arr = $ePostNewspaper->Newspaper_PageIDs;
		$pos = array_search($PageID, $page_arr);
		$new_page_id = $page_arr[$pos-1];
		$ePostPage = new libepost_page($PageID);
		$result = $ePostPage->delete_page();
		$new_page_id = $page_arr[$pos-1];
		$ePostPage = new libepost_page($new_page_id);
		$newspaper_body_html = $ePostUI->gen_single_big_page_layout($ePostPage, true, true);
		$returnAry =array(
			array('newspaper_body_html',$newspaper_body_html,true,false),
			array('new_page_id',$new_page_id,true,false)
		);
		
	break;	
	case "display_page":
		# Initialize Object
		$ePostPage = new libepost_page($PageID);
		$newspaper_body_html = $ePostUI->gen_single_big_page_layout($ePostPage, true, true);
		$returnAry =array(
			array('newspaper_body_html',$newspaper_body_html,true,false)
		);
	break;		
	case "delete_page_article":
		# Initialize Variable
		$ArticleID = isset($ArticleID) && $ArticleID!=''? $ArticleID : '';

		# Initialize Object
		$ePostArticle = new libepost_article($ArticleID);
//		$ePostArticleShelf = new libepost_articleshelf($ePostArticle->ArticleShelfID);

		# Check if ePost should be accessible
		$ePostArticle->portal_auth();

		# Delete Article
		$result = $ePostArticle->delete_article();

		$ePostPage = new libepost_page($ePostArticle->PageID);
		$newspaper_body_html = $ePostUI->gen_single_big_page_layout($ePostPage, true, true);
		$returnAry =array(
			array('newspaper_body_html',$newspaper_body_html,true,false)
		);
	break;	
	case "update_issue":
		$ePostPage = new libepost_page($PageID);
		$newspaper_body_html = $ePostUI->gen_single_big_page_layout($ePostPage, true, true);
		$returnAry =array(
			array('folder_title',$ePostNewspaper->Folder_Title,true,false),
			array('newspaper_title',$ePostNewspaper->Newspaper_Title,true,false),
			array('folder_id',$ePostNewspaper->FolderID,true,false),
			array('cpage_id',$CoverPageID,false,false),
			array('newspaper_body_html',$newspaper_body_html,true,false)
		);
	break;
	case "update_issue_skin":
		$ePostPage = new libepost_page($PageID);
		$newspaper_body_html = $ePostUI->gen_single_big_page_layout($ePostPage, true, true);
		$returnAry =array(
			array('newspaper_body_html',$newspaper_body_html,true,false)
		);
	break;
	case "update_issue_order":
		$ePostNewspaper->Newspaper_PageIDs = explode('|=|',$id_str);
		$result = $ePostNewspaper->update_pages_PageOrder();
		$returnAry =array(
			array('result',$result,false,false)
		);
	break;	
}
header("Content-Type: text/xml");
$XML = $ePostUI->generateXML($returnAry);
echo $XML;
?>