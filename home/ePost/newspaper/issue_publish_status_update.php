<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$FolderID 	   = isset($FolderID) && $FolderID!=''? $FolderID:'';
$NewspaperID   = isset($NewspaperID) && $NewspaperID!=''? $NewspaperID:'';
$PublishStatus = isset($PublishStatus) && $PublishStatus!=''? $PublishStatus:$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft'];
$IsPageList	   = isset($IsPageList) && $IsPageList!=''? $IsPageList:0; // To indicate the page is refered from issue_page_list.php or not
$Status		   = isset($Status) && $Status!=''? $Status:0; // For issue_list.php use only

# Initialize Object
$ePostNewspaper = new libepost_newspaper($NewspaperID);
$ePost	 = new libepost();

# Check if ePost should be accessible
$ePostNewspaper->portal_auth();

if($ePost->is_editor && !(!$ePost->is_publish_manager && $PublishStatus == $cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Publish'])){
	$result = $ePostNewspaper->update_PublishStatus($PublishStatus);
}else{
	$result = false;
}
$returnMsg = $result?"UpdateSuccess":"UpdateUnsuccess";
if($IsPageList){
	$redirect_location = 'issue_page_list.php?NewspaperID='.$NewspaperID.'&returnMsg='.$returnMsg;
}
else{
	$redirect_location = 'issue_list.php?FolderID='.$FolderID.'&Status='.$Status.'&returnMsg='.$returnMsg;
}

header('location:'.$redirect_location);

intranet_closedb();
?>