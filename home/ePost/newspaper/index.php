<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$Status = isset($Status) && $Status!=''? $Status : 0;

# Initialize Object
$ePost		  = new libepost();
$ePostUI	  = new libepost_ui();
$ePostFolders = new libepost_folders();
$Header='NEW';
$FromPage = 'editor';
# Check if ePost should be accessible
$ePost->portal_auth();
# Process Table Content HTML
for($i=0;$i<count($ePostFolders->Folders);$i++){
	$FolderObj = $ePostFolders->Folders[$i];
	$ManageNewspaperAry   = $FolderObj->get_managing_newspapers();
	$ManageNewspaperCount = count($ManageNewspaperAry);
	
	switch($Status){
		case $cfg_ePost['BigNewspaper']['Folder_filter']['All']: 
			$IsSkipped = false; 
			break;
		case $cfg_ePost['BigNewspaper']['Folder_filter']['FolderWithNewspaper']: 
			$IsSkipped = $ManageNewspaperCount==0; 
			break;
		case $cfg_ePost['BigNewspaper']['Folder_filter']['EmptyFolder']: 
			$IsSkipped = $ManageNewspaperCount>0; 
			break;
	}
	
	if(!$IsSkipped){
		# Calculate the TotalNoOfView of $ManageNewspaperAry
		$ManageNewspaperTotalNoOfView = 0;
		for($j=0;$j<$ManageNewspaperCount;$j++){
			$ManageNewspaperTotalNoOfView += $ManageNewspaperAry[$j]->Newspaper_NoOfView;
		}
		
		$table_html .= "<tr id='tr_row_".($i+1)."'>";
		$table_html .= "	<td>".($i+1)."</td>";
		$table_html .= "	<td><a id=\"Folder_title_".$FolderObj->FolderID."\" href=\"javascript:openFolder(".$FolderObj->FolderID.")\">".($FolderObj->Folder_IsSystemPublic==$cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']? $Lang['ePost']['PublicFolder']:$FolderObj->Folder_Title)."</a></td>";
		$table_html .= "	<td>".$ManageNewspaperCount."</td>";
		//$table_html .= "	<td>".$ManageNewspaperTotalNoOfView."</td>";
		$table_html .= "	<td>".date('Y-m-d', strtotime($FolderObj->Folder_ModifiedDate))." (".$FolderObj->Folder_ModifiedByUser.")</td>";
		$table_html .= "	<td>";
		
		if($FolderObj->Folder_IsSystemPublic != $cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']){
			$table_html .= "	<div class=\"table_row_tool\">";
			$table_html .= $ePost->user_obj->isTeacherStaff() && $ePost->is_editor?"		<a href=\"javascript:go_edit(".$FolderObj->FolderID.")\" class=\"tool_edit\" title=\"".$Lang['ePost']['Edit']."\">&nbsp;</a>":"";
			$table_html .= $ePost->user_obj->isTeacherStaff() && $ePost->is_editor? "<a href=\"javascript:go_delete(".$FolderObj->FolderID.")\" class=\"tool_delete\" title=\"".$Lang['ePost']['Delete']."\">&nbsp;</a>":"";
			$table_html .= "	</div>";
		}
		else{
			$table_html .= "	&nbsp;";
		}
		
		$table_html .= "	</td>";
		$table_html .= "</tr>";
	}
}

if(!$table_html){
	$table_html  = "<tr>";
	$table_html .= "	<td height='80' class='tabletext' colspan='5' style='text-align:center;vertical-align:middle'>$i_no_record_exists_msg</td>";
	$table_html .= "</tr>";
}
$new_row_html = "<input type='hidden' name='new_row_no' id='new_row_no' value='".(count($ePostFolders->Folders)+1)."'>";
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>$Lang['ePost']['FolderList'], "link"=>""); 

include($intranet_root.'/home/ePost/newspaper/templates/tpl_index.php');
//// For performance tunning info
//$runTime = StopTimer($precision=5, $NoNumFormat=false, 'ePostRunTime');
//echo '<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>';
//debug_pr('$runTime = '.$runTime.'s');
//debug_pr('convert_size(memory_get_usage()) = '.convert_size(memory_get_usage()));
//debug_pr('convert_size(memory_get_usage(true)) = '.convert_size(memory_get_usage(true)));
//debug_pr('Query Count = '.$GLOBALS[debug][db_query_count]);
//$ePost->db_show_debug_log_by_query_number(1);
//die();
intranet_closedb();
?>