<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$PageID = isset($PageID) && $PageID!=''? $PageID : '';

# Initialize Object
$ePostPage = new libepost_page($PageID);

# Check if ePost should be accessible
$ePostPage->portal_auth();

# Delete Page
$result = $ePostPage->delete_page();

header('location:issue_page_list.php?NewspaperID='.$ePostPage->NewspaperID.'&msg='.$result);

intranet_closedb();
?>