<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$ArticleID = isset($ArticleID) && $ArticleID!=''? $ArticleID : '';

# Initialize Object
$ePostArticle = new libepost_article($ArticleID);
$ePostArticleShelf = new libepost_articleshelf($ePostArticle->ArticleShelfID);

# Check if ePost should be accessible
$ePostArticle->portal_auth();

# Delete Article
$result = $ePostArticle->delete_article();

# Delete Appendix
$ePostArticleShelf->delete_article_shelf();

header('location:issue_page_new_layout.php?NewspaperID='.$ePostArticle->NewspaperID.'&PageID='.$ePostArticle->PageID.'&msg='.$result);

intranet_closedb();
?>