<?php
# using :Paul
/* 
 * Modification Log:
 * 2019-06-20 (Paul) [ip.2.5.10.6.1]
 * 	- fix the security fix trimming the "NEW" in AttachmentID into 0 causing failure in uploading attachment by changing AttachmentID to AttachmentIDs
 * 
 */

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$PageID	   		= isset($PageID) && $PageID!=''? $PageID : 0;
$ArticleID 		= isset($ArticleID) && $ArticleID!=''? $ArticleID : 0;

# Initialize Object
$ePostPage = new libepost_page($PageID);
$ePost = new libepost();
# Check if ePost should be accessible
$ePostPage->portal_auth();

# Update Article Shelf
if($ArticleID){
	$ePostArticle = new libepost_article($ArticleID);
	
	$ePostArticle->ArticleShelfID 		= 0;
	$ePostArticle->Article_Title 		= $Title;
	$ePostArticle->Article_Content		= $Content;
	$ePostArticle->Article_Type 	 	 = $Type;
	$ePostArticle->Article_Theme 	 	 = $Theme;
	$ePostArticle->Article_Color 	 	 = $Color;	
	$ePostArticle->Article_Status 		= $cfg_ePost['BigNewspaper']['ArticleShelf_status']['exist'];
	$ePostArticle->Article_DisplayAuthor = $cfg_ePost['BigNewspaper']['Article_DisplayAuthor']['No'];
	$ePostArticle->Article_DisplayTitle  = $DisplayTitle? $cfg_ePost['BigNewspaper']['Article_DisplayTitle']['Yes']:$cfg_ePost['BigNewspaper']['Article_DisplayTitle']['No'];
	$ePostArticle->Article_DisplayContent= $DisplayContent? $cfg_ePost['BigNewspaper']['Article_DisplayContent']['Yes']:$cfg_ePost['BigNewspaper']['Article_DisplayContent']['No'];	
	$ePostArticle->Article_FontSize 	 = $FontSize;
	$ePostArticle->Article_FontStyle 	 = $FontStyle;
	$ePostArticle->Article_LineHeight 	 = $LineHeight;			
}
else{
	# Prepare $Page_Data_ary for Page Object
	$PageOrder = count($ePostNewspaper->Newspaper_PageIDs);
	$Article_Data_ary['ArticleShelfID']				= 0;
	$Article_Data_ary['Article_Title'] 	 			= $Title;
	$Article_Data_ary['Article_Content'] 			= $Content;
	$Article_Data_ary['Article_Position']  	  		= $Position;
	$Article_Data_ary['Article_DisplayAuthor']		= $cfg_ePost['BigNewspaper']['Article_DisplayAuthor']['No'];
	$Article_Data_ary['Article_DisplayTitle'] 		= $DisplayTitle? $cfg_ePost['BigNewspaper']['Article_DisplayTitle']['Yes']:$cfg_ePost['BigNewspaper']['Article_DisplayTitle']['No'];
	$Article_Data_ary['Article_DisplayContent']		= $DisplayContent? $cfg_ePost['BigNewspaper']['Article_DisplayContent']['Yes']:$cfg_ePost['BigNewspaper']['Article_DisplayContent']['No'];	
	$Article_Data_ary['Article_FontSize'] 	   		= $FontSize;
	$Article_Data_ary['Article_FontStyle'] 	   	   	= $FontStyle;
	$Article_Data_ary['Article_LineHeight'] 	   	= $LineHeight;	
	$Article_Data_ary['Article_Type'] 	   	 		= $Type;
	$Article_Data_ary['Article_Theme'] 	   			= $Theme;
	$Article_Data_ary['Article_Color'] 	   		 	= $Color;
	$Article_Data_ary['Article_Status']    			= $cfg_ePost['BigNewspaper']['Article_status']['exist'];
	$Page_Data_ary = $ePostPage->page_obj_to_array();
	$Article_Data_ary = array_merge($Page_Data_ary, $Article_Data_ary);	
	$ePostArticle = new libepost_article(0, $Article_Data_ary);
}

$ePostArticle->update_article_db();
### ATTACHMENT ###
$toDir = $ePostArticle->get_article_attachment_folder();
$AttachmentIDs = count($AttachmentIDs)>0?$AttachmentIDs:array();

$FileNameAry = array();
$updateAttachmentAry = array();
foreach((array)$ePostArticle->Article_Attachment as $_attachmentId => $_attachmentAry){
	if(!in_array($_attachmentId,$AttachmentIDs)){
		$ePostArticle->delete_article_attachment($_attachmentId);
	}
}
for($i=0;$i<count($AttachmentIDs);$i++){
	$_attachment = "";
	$_attachmentFromPath = "";
	$_fileDes = "";
	$_attachmentId = $AttachmentIDs[$i];
	$_width = $_POST['Attachment_Width'.$i];
	$_alignment = $_POST['Alignment'.$i];
	$_caption = $_POST['Caption'.$i];
	$_status = $_POST['DisplayAttachment'.$i]==1?1:0;
	if($_attachmentId=='NEW'&&$_FILES['fileupload'.$i]['error']==0){//upload from edit issue
		$_attachmentFromPath = $_FILES['fileupload'.$i]['tmp_name'];
		if(in_array($_attachment,$FileNameAry)){
			$_fileNameAry = explode('.',$_attachment);
			$_attachment = $_fileNameAry[0].'(1).'.$_fileNameAry[1];
		}else{
			$_attachment = $_FILES['fileupload'.$i]['name'];
		}
		if($_attachmentFromPath){
			$_fileDes .= $toDir.'/'.$_attachment;
			move_uploaded_file($_attachmentFromPath, $_fileDes);
		}	
	}else{//edit article
		if($ArticleID){
			if($_attachmentId!='NEW'){
				$_attachment = $ePostArticle->Article_Attachment[$_attachmentId]['attachment'];
			}
		}else{//copy from article shelf

		}
	}
	if(!empty($_attachment)){
		$_attachment = $ePostArticle->Get_Safe_Sql_Query($_attachment);
		if($_attachmentId=='NEW'){
			$ext = getFileExtention($_attachment);
			if($ext=='FLV'||$ext=='MOV'||$ext=='MP4'){
				$ePost->convertWritingVideo($_fileDes);
			}
		}
		$updateAttachmentAry[] = array(
										'attachmentId'=>$_attachmentId,
										'attachment'=>$_attachment,
										'caption'=>$_caption,
										'size'=>$_width,
										'alignment'=>$_alignment,
										'status'=>$_status
									);
		$FileNameAry[] = $_attachment;									
	}
}
$ePostArticle->save_article_attachment($updateAttachmentAry);

intranet_closedb();
?>
<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery-1.3.2.min.js"></script>
<script>
	$(document).ready(function(){
		parent.window.displayPage(<?=$ePostPage->PageID?>);
		parent.window.tb_remove();
	});
</script>