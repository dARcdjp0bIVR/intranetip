<?php
include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_header.php');
?>
<body id="edit_pop" class="edit_pop_page_info">
<script>
	var layout_changed = 0;
	var default_type   = '<?=$Type?>';
	var default_layout = <?=$Layout?>;
	var isCoverPage = <?=$isCoverPage?>;
	function copy_data_to_hidden_form(){
		var pageType 	= $("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> <?=$isCoverPage?"input":"select"?>[id=PageType]").val();
		var pageLayout 	= $("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageLayout]").val();
		var themeType 	= $("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[name=theme_icon]:checked").val(); 
		var pageTheme 	= themeType=='c'?-1:$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageTheme]").val();
		var pageName 	= $("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageName]").val();
		var refpage 	= isCoverPage?0:$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> select[id=PageRefPage]").val();
		
		$("#PageInfoForm input[id=PageType]").val(pageType);
		$("#PageInfoForm input[id=PageLayout]").val(pageLayout);
		$("#PageInfoForm input[id=PageTheme]").val(pageTheme);
		$("#PageInfoForm input[id=PageName]").val(pageName);
		$("#PageInfoForm input[id=PageRefPage]").val(refpage);
		
	}	
	function copy_data_from_hidden_form(){	
		var pageType 	= $("#PageInfoForm input[id=PageType]").val();
		var pageLayout 	= $("#PageInfoForm input[id=PageLayout]").val();
		var pageTheme 	= $("#PageInfoForm input[id=PageTheme]").val();
		var pageName 	= $("#PageInfoForm input[id=PageName]").val();
		var refpage 	= $("#PageInfoForm input[id=PageRefPage]").val();
				
		$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageLayout]").val(pageLayout);		
		$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> <?=$isCoverPage?"input":"select"?>[id=PageType]").val(pageType);
		$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageName]").val(pageName);
		if(!isCoverPage){
			$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> select[id=PageRefPage]").val(refpage);	
		}
		if(pageTheme==-1){
			$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> radio[id=theme_icon_customize]").attr("checked","checked");
		}
	}		
	function change_type(type){
		<?php if($PageID && count($ePostPage->Page_ArticleIDs)) { ?>
			if(layout_changed!=1){
				if(confirm('<?=$Lang['ePost']['Confirm']['ChangePageLayout'][0]?>')){
					layout_changed=1;
				} else {
					$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageType]").val(default_type);
				}
			}
			
			if(layout_changed==1){
		<?php } ?>
		
		$('.layout').hide();
		$('[type="' + type + '"]').show();
		select_layout(type, '1');
		
		if(isCoverPage){
			$('#theme_row').attr('style', 'display:none');
		}
		else{
			$('#theme_row').attr('style', '');
		}
		
		<?php if($PageID && count($ePostPage->Page_ArticleIDs)) { ?>
		}
		<?php } ?>
	}	
	function edit_issue_info(){
		var obj = document.PageInfoForm;
		
		obj.action = 'issue_new.php';
		obj.submit();  
	}
	function edit_issue_design(){
		var obj = document.PageInfoForm;
		
		obj.action = 'issue_new_design.php';
		obj.submit();  
	}
	function add_new_page(){
		var title = 'Edit Page Info';
		var url   = 'issue_page_new_info.php?TB_iframe=true&width=700&height=400';
		
		tb_show(title, url);
	}	
	function select_layout(Type, Layout){
		<?=$PageID && count($ePostPage->Page_ArticleIDs)? "if(layout_changed==1 || confirm('".$Lang['ePost']['Confirm']['ChangePageLayout'][0]."')){ layout_changed=1;" : ""?>
		$('.layout').removeClass('select_layout_selected');
		$('[type="' + Type + '"][layout="' + Layout + '"]').addClass('select_layout_selected');
		$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageType]").val(Type);
		$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageLayout]").val(Layout);
		<?=$PageID && count($ePostPage->Page_ArticleIDs)? "}" : ""?>
	}	
	function select_theme(Theme){
		$('.theme').removeClass('select_layout_selected');
		$('[theme="' + Theme + '"]').addClass('select_layout_selected');
		$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageTheme]").val(Theme);
	}
	function switch_theme_type(type){
		$("#theme_d").hide();
		$("#theme_c").hide();
		$("#theme_"+type).show();
	}
	function go_submit(){
			
		var pageType 	= $("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> <?=$isCoverPage?"input":"select"?>[id=PageType]").val();
		var pageLayout 	= $("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageLayout]").val();
		var pageTheme 	= $("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageTheme]").val();
		var pageName 	= $("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageName]").val();
		var pageThemeName = $("#PageInfoForm input[id=PageThemeName]").val();
		var isCover = (pageType.substring(0,1)=='C')?true:false;
		pageTheme = (!isCover)? pageTheme : 0; 
		<?if(!$isCoverPage){?>
		if(!pageType){
			alert('<?=$Lang['ePost']['Warning']['SelectType']?>');
			$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> select[id=PageType]").focus();
			return false;
		}
		else <? } ?>if(!pageLayout){
			alert('<?=$Lang['ePost']['Warning']['SelectLayout']?>');
			$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageLayout]").focus();
			return false;
		}
		else if(!isCover&&(!pageTheme||(pageTheme == -1 && pageThemeName == ''))){
			if(!pageTheme){
				alert('<?=$Lang['ePost']['Warning']['SelectThemeIcon']?>');
				$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageTheme]").focus();
				return false;
			}else if(pageTheme == -1 && pageThemeName == ''){
				alert('<?=$Lang['ePost']['Warning']['UploadThemeIcon']?>');
				$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=FileUpload]").focus();
				return false;				
			}
		}
		else if(!pageName){
			alert('<?=$Lang['ePost']['Warning']['InputPageTitle']?>');
			$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=PageName]").focus();
			return false;
		}
		else{
			copy_data_to_hidden_form();
			var obj = document.PageInfoForm;
			if(!(obj.PageType.value==default_type && obj.PageLayout.value==default_layout)){
				obj.PageLayoutChanged.value = layout_changed;
			}
			updatePage();
		}
		return false;
	
	}
	function updatePage(){
		$.ajax({
			url:      "/home/ePost/newspaper/ajax_gen_page.php",
			type:     "POST",
			data:     $("#PageInfoForm").serialize() + '&task=update_page',
			success:  function(xml){
					var page_total 			= $(xml).find("page_total").text();
					var li_html 			= $(xml).find("li_html").text();
					var action 				= $(xml).find("action").text();
					var new_page_id 		= $(xml).find("page_id").text();
					var newspaper_body_html = $(xml).find("newspaper_body_html").text();
	
					parent.window.$('#span_page_total').text(page_total);
					parent.window.$('#ul_issue_page_list').html(li_html);
					parent.window.$('.edit_page_panel_right').html(newspaper_body_html);
					parent.window.$('#PageID').val(new_page_id);
					parent.window.tb_remove();
			}
		});
		
	}
	function go_cancel(){
		parent.window.tb_remove();
	}

	function file_upload_change(FileUploadForm){
		var file_upload_action = FileUploadForm.FileUploadAction.value;
		var file_upload  	   = FileUploadForm.FileUpload.value;
		var ext 		 	   = file_upload.substr((file_upload.length-4)).toUpperCase();
		copy_data_to_hidden_form();
		if(ext=='.GIF' || ext=='.JPG' || ext=='.JPE' || ext=='.JPEG' || ext=='.PNG' || ext=='.BMP'){
			$('#FileUploadForm_'+file_upload_action).ajaxSubmit({
	        	url			: 'ajax_file_upload.php',
	        	type		: 'post',
	        	dataType	: 'json', 
	        	clearForm	: true,
	        	beforeSubmit: file_upload_prepare,
				success		: file_upload_success
			});
		}
		else{
			$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=FileUpload]").val('');
			alert('<?=$Lang['ePost']['Warning']['UploadImageFile']?>');
		}
	}
	
	function file_upload_prepare(formData, jqForm, options){
		var form = jqForm[0]; 
		var FileUploadAction = form.FileUploadAction.value;

		$('#Newspaper'+FileUploadAction+'_UploadBtn_NoFile').hide();
		$('#Newspaper'+FileUploadAction+'_UploadBtn_Uploading').show();
		$('#Newspaper'+FileUploadAction+'_UploadBtn_Uploaded').hide();
	}
	
	function file_upload_success(responseObj, statusText, xhr, $form){
		var FileUploadAction = responseObj.upload_action;

		if(responseObj.result){
			$('#Newspaper'+FileUploadAction+'_Display').html(responseObj.upload_file_name);
			
			$('#Newspaper'+FileUploadAction+'_UploadBtn_NoFile').hide();
			$('#Newspaper'+FileUploadAction+'_UploadBtn_Uploading').hide();
			$('#Newspaper'+FileUploadAction+'_UploadBtn_Uploaded').show();
			
			$("#PageInfoForm input[id=PageThemeName]").val(responseObj.upload_file_path + responseObj.upload_file_name);
			$("#FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?> input[id=theme_icon_customize]").attr("checked","checked");
		}
		else{
			$('#Newspaper'+FileUploadAction+'_UploadBtn_NoFile').show();
			$('#Newspaper'+FileUploadAction+'_UploadBtn_Uploading').hide();
			$('#Newspaper'+FileUploadAction+'_UploadBtn_Uploaded').hide();
			
			alert('<?=$Lang['ePost']['Warning']['FailedToUploadImage']?>');	
		}
		copy_data_from_hidden_form();
	}
	
	function delete_customize_themeicon(){
		if(confirm('<?=$Lang['ePost']['Confirm']['RemoveCustomizeThemeIcon']?>')){
			$('#NewspaperThemeIcon_Display').html('');
			$("#PageInfoForm input[id=PageThemeName]").val("");
			$('#NewspaperThemeIcon_UploadBtn_NoFile').show();
			$('#NewspaperThemeIcon_UploadBtn_Uploading').hide();
			$('#NewspaperThemeIcon_UploadBtn_Uploaded').hide();
		}
	}
			
</script>

   <div class="pop_container">
    <div class="sectiontitle"><?=$Lang['ePost']['CoverPageInformationAndLayout']?></div>
    <form id="FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?>" name="FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?>" action="ajax_file_upload.php" method="post" style="margin:0;padding:0" enctype="multipart/form-data">
        <div class="marking_board">
            <p class="spacer"></p> 
            <table class="form_table">
		 <tr>
		   <td><?=$Lang['ePost']['Type']?></td>
		   <td>:</td>
		   <td >
		   <?php if($isCoverPage) { ?>
				<?=$Lang['ePost']['CoverPage']?>
				<input type='hidden' id='PageType' name='PageType' value='<?=$Type?>'/>
			<?php } else { ?>
				<select name="PageType" id="PageType" onchange="change_type(this.value)">
				<?php foreach($cfg_ePost['BigNewspaper']['TypeLayout'] as $cfg_type => $type_ary):?>
					<?php if($type_ary['PageType']=='InsidePage'):?>
					<option value="<?=$cfg_type?>" <?=$Type==$cfg_type? 'selected':''?>><?=$Lang['ePost'][$type_ary['PageTitle']]?></option>
					<?php endif;?>
				<?php endforeach;?>
				</select>
			<?php } ?>
		   </td>
		 </tr>
		 <tr>
		   <td><?=$Lang['ePost']['Layout']?></td>
		   <td>:</td>
		   <td>
			<?php foreach($cfg_ePost['BigNewspaper']['TypeLayout'] as $cfg_type=>$type_ary){ ?>
				<?php foreach($type_ary as $cfg_layout=>$layout_ary){ ?>
					<?php if($cfg_layout!='PageType'&&$cfg_layout!='PageTitle'):?>
					<a href="javascript:select_layout('<?=$cfg_type?>', '<?=$cfg_layout?>')" type="<?=$cfg_type?>" layout="<?=$cfg_layout?>" class="layout select_layout <?=$cfg_type==$Type && $cfg_layout==$Layout? 'select_layout_selected':''?>" style='<?=((substr($Type,0,1)=='C' && substr($cfg_type,0,1)=='C') || ((substr($Type,0,1)=='I' && $cfg_type==$Type)))? '':'display:none'?>'><img src="<?=$intranet_httppath.'/images/ePost/issue_layout/'.$layout_ary['img']?>" width="60" height="80" class="issue_layout_img" /></a>
					<?php endif;?>
				<?php } ?>
			<?php } ?>
			<input type='hidden' id='PageLayout' name='PageLayout' value='<?=$Layout?>'/>
		   </td>
		 </tr>
		<tr>
		   <td><?=$Lang['ePost']['PageTitle']?></td>
		   <td>:</td>
		   <td ><input type="text" name="PageName" class="textbox_date" id="PageName" value="<?=$Name?>"/></td>
		 </tr> 
		<tr id="theme_row" style="<?=$isCoverPage? 'display:none':''?>">
			<td><?=$Lang['ePost']['ThemeIcon']?></td>
			<td>:</td>
			<td>
				<input type="radio" name="theme_icon" id="theme_icon_default" onclick="switch_theme_type('d');" <?=($Theme!=-1?'checked="checked"':'')?> value="d" />
				<label for="theme_icon_default"><?=$Lang['ePost']['Default']?></label>
				<input type="radio" name="theme_icon" id="theme_icon_customize" onclick="switch_theme_type('c');" <?=($Theme==-1&&!empty($ThemeName)?'checked="checked"':'')?> value="c" />
				<label for="theme_icon_customize"><?=$Lang['ePost']['Customize']?></label>
			</td>
		</tr>	
		<tr id="tr_theme_icon" style="<?=$isCoverPage? 'display:none':''?>">
			<td></td>
			<td></td>
			<td>
				<input type='hidden' id='PageTheme' name='PageTheme' value='<?=$Theme?>'/>
				<div id="theme_d" <?=($Theme!=-1?'':'style="display:none;"')?>>
				<p class="spacer"></p>
				<?php foreach($cfg_ePost['BigNewspaper']['Theme'] as $cfg_theme=>$theme_ary){ ?>
					<a href="javascript:select_theme('<?=$cfg_theme?>')" theme="<?=$cfg_theme?>" class="theme select_layout <?=$cfg_theme==$Theme? 'select_layout_selected':''?>"><img src="<?=$intranet_httppath?>/images/ePost/newspaper/page_cat/<?=$theme_ary['big_img']?>" width="90" height="65" class="issue_layout_img" /></a>
				<?php } ?>
				</div>
				<div id="theme_c" <?=($Theme==-1?'':'style="display:none;"')?>>
					
						<div id="NewspaperThemeIcon_UploadBtn_NoFile" <?=$ThemeName? "style='display:none'":""?>>
							<input type="hidden" id="FileUploadAction" name="FileUploadAction" value="<?=$cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']?>"/>
							<input type="file" id="FileUpload" name="FileUpload" onchange="file_upload_change(this.form)" accept='image/*'/><br/>
							<span class="tabletextremark"><?=$Lang['ePost']['CustomizeThemeIconInstruction']?></span>
						</div>
						<div id="NewspaperThemeIcon_UploadBtn_Uploading" style="display:none">
							<a href="javascript:void(0)"><?=$Lang['ePost']['Uploading']?></a><img src="<?=$intranet_httppath?>/images/<?=$LAYOUT_SKIN?>/indicator.gif">
						</div>
						<div id="NewspaperThemeIcon_UploadBtn_Uploaded" <?=$ThemeName? "":"style='display:none'"?>>
	                		<a id="NewspaperThemeIcon_Display" class="img_file" href="javascript:void(0)"><?=$ThemeName?></a>
	                		<div class="table_row_tool">
	                			<a title="Delete" class="tool_delete" href="javascript:void(0)" onclick="delete_customize_themeicon()">&nbsp;</a>
	                		</div>
	                	</div>
				</div>
				
			</td>
		</tr>			
		
			<?php if($RefPage_html){ ?>
				<tr>
					<td style="white-space:nowrap"><?=$Lang['ePost']['LocationInsertAfter']?></td>
					<td>:</td>
					<td>
						<select id="PageRefPage" name="PageRefPage"><?=$RefPage_html?></select>
					</td>
				</tr>
			<?php }else{ ?>		
				<input type="hidden" id="PageRefPage" name="PageRefPage" value="">
			<?php } ?>		 
		 <col class="field_title" />
		 <col  class="field_c" />
	   </table>



          <p class="spacer"></p>
            </div><p class="spacer"></p>
            <div class="edit_bottom">
				<input name="submit_btn" type="button" class="formbutton" value="<?=$PageID? $Lang['ePost']['Button']['Update']:$Lang['ePost']['Button']['Submit']?>" onclick="go_submit()" />
				<input name="cancel_btn" type="button" class="formsubbutton" onclick="go_cancel()" value="<?=$Lang['ePost']['Button']['Cancel']?>" />
            </div>
 </form>          
<form id="PageInfoForm" name="PageInfoForm" method="post" style="margin:0;padding:0">  
			<input type="hidden" id="FolderID" name="FolderID" value="<?=$ePostNewspaper->FolderID?>"/>
			<input type="hidden" id="NewspaperID" name="NewspaperID" value="<?=$ePostNewspaper->NewspaperID?>"/>
			<input type="hidden" id="PageID" name="PageID" value="<?=$PageID?>"/>
			<input type='hidden' id='PageLayoutChanged' name='PageLayoutChanged' value='0'/>
			<input type="hidden" id="PageRefPage" name="PageRefPage" value="">
			<input type='hidden' id='PageTheme' name='PageTheme' value='<?=$Theme?>'/>
			<input type='hidden' id='PageThemeName' name='PageThemeName' value='<?=$ThemeName?>'/>	
			<input type="hidden" id="PageName" name="PageName" value="<?=$Name?>"/>
			<input type='hidden' id='PageType' name='PageType' value='<?=$Type?>'/>			
			<input type='hidden' id='PageLayout' name='PageLayout' value='<?=$Layout?>'/>		
<?php

include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_footer.php');
?>