<?php
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>
<style>
	select {font-size:12px;}
</style>
<script>
	function go_submit(){
		var obj = document.form1;
		
		if(!obj.NewspaperTitle.value){
			alert('<?=$Lang['ePost']['Warning']['InputEPostTitle']?>');
			obj.NewspaperTitle.focus();
			return false;
		}
		else if(!obj.NewspaperName.value){
			alert('<?=$Lang['ePost']['Warning']['InputIssueName']?>');
			obj.NewspaperName.focus();
			return false;
		}
		else if(!obj.NewspaperIssueDate.value){
			alert('<?=$Lang['ePost']['Warning']['InputIssueDate']?>');
			obj.NewspaperIssueDate.focus();
			return false;
		}
		else if(!obj.NewspaperFolderID.value){
			alert('<?=$Lang['ePost']['Warning']['SelectFolder']?>');
			obj.NewspaperFolderID.focus();
			return false;
		}
		else{
			$('#StudentEditorSelected option').each(function(){
				$(this).attr('selected','selected');
			});
			obj.submit();
			
		}
	}
	
function go_cancel(){
		<?php if($IsPopup) {?>
			parent.window.tb_remove();
		<?php 
			}else if($FolderID){
		?>
		var obj = document.form1;
		obj.action = "<?=$FolderID? ($NewspaperID? "issue_page_new.php?FolderID=$FolderID&NewspaperID=$NewspaperID":"issue_list.php?FolderID=$FolderID"):"index.php"?>";
		obj.submit();
		<?php 
			}else{
		?>
			history.back(-1);
		<?php
			} 
		?>
		
	}
	
	function view_issue_design(){
		var obj = document.form1;
		obj.action = "issue_new_design.php";
		obj.submit();
	}
</script>
<?php if($IsPopup){?>
<body id="edit_pop" class="edit_pop_issue_edit">
    <div class="pop_container">
    <div class="sectiontitle"><?=$Lang['ePost']['EditIssueInformation']?></div>
<form name="form1" action="issue_new_update.php" method="post" style="margin:0;padding:0">
<?php }else{ ?>
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
<form name="form1" action="issue_new_update.php" method="post" style="margin:0;padding:0">
	<p class="spacer">&nbsp;</p>
	<p class="spacer">&nbsp;</p>
	<?=$ePostUI->gen_portal_page_nav($nav_ary)?>
	<br>
	<p class="spacer">&nbsp;</p>
	<div class="marking_board">
		<br/>
		<div class="stepboard stepboard_3s">
			<h1><?=$Lang['ePost']['Step']?>:</h1>
			<ul> 
				<li class="stepon">
					<div>
						<em>1</em>
						<span><?=$Lang['ePost']['InputIssueInformation']?></span>
					</div>
				</li>
				<li>
					<?php if($IsReadOnly){ ?>
						<a href="javascript:void(0)" onclick="view_issue_design()">
					<?php } ?>
							<div>
								<em>2</em>
								<span><?=$Lang['ePost']['SelectSkin']?></span>
							</div>
					<?php if($IsReadOnly){ ?>
						</a>
					<?php } ?>
				</li>
				<li>
					<div>
						<em>3</em>
						<span><?=$Lang['ePost']['EditPages']?></span>
					</div>
				</li>				
			</ul> 
			<br style="clear:both" />
		</div>
<?php } ?>

		<table class="form_table">
			<colgroup>
				<col class="field_title">
				<col class="field_c">
			</colgroup>
			<tr>
				<td><?=$Lang['ePost']['Folder']?><span style="color:red;font-weight:bold;">*</span></td>
				<td>:</td>
				<td><?=$folder_selection_html?></td>
			</tr>
			<tr>
				<td><?=$Lang['ePost']['ePostTitle']?><span style="color:red;font-weight:bold;">*</span></td>
				<td>:</td>
				<td>
					<?php if($IsReadOnly){ ?>
						<?=$ePostNewspaper->Newspaper_Title?>
					<?php } else { ?>
						<input name="NewspaperTitle" type="text" id="NewspaperTitle" size="15" value="<?=$NewspaperID? $ePostNewspaper->Newspaper_Title:''?>" maxlength="150"/>
					<?php } ?>
				</td>
			</tr>
			<tr>
				<td><?=$Lang['ePost']['IssueName']?><span style="color:red;font-weight:bold;">*</span></td>
				<td>:</td>
				<td>
					<?php if($IsReadOnly){ ?>
						<?=$ePostNewspaper->Newspaper_Name?>
					<?php } else { ?>
						<input name="NewspaperName" type="text" id="NewspaperName" size="15" value="<?=$NewspaperID? $ePostNewspaper->Newspaper_Name:''?>"/>
					<?php } ?>
				</td>
			</tr>
			<tr>
				<td><?=$Lang['ePost']['IssueDate']?><span style="color:red;font-weight:bold;">*</span></td>
				<td>:</td>
				<td>
					<?php if($IsReadOnly){ ?>
						<?=date('Y-m-d', strtotime($ePostNewspaper->Newspaper_IssueDate))?>
					<?php } else { ?>
						<script>
							$(document).ready(function(){
								$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '/images/2009a/icon_calendar_off.gif', buttonText: '', mandatory: true});
								$('#NewspaperIssueDate').datepick({
									dateFormat: 'yy-mm-dd',
									dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
									changeFirstDay: false,
									firstDay: 0
								});
							});
						</script>
						<input name="NewspaperIssueDate" type="text" id="NewspaperIssueDate" size="15" value="<?=$NewspaperID? date('Y-m-d', strtotime($ePostNewspaper->Newspaper_IssueDate)):''?>" readonly/>
					<?php } ?>
				</td>
			</tr>
			<tr>
				<td><?=$Lang['ePost']['OpenToPublic']?><span style="color:red;font-weight:bold;">*</span></td>
				<td>:</td>
				<td>
					<?php if($IsReadOnly){ ?>
						<?=$ePostNewspaper->Newspaper_OpenToPublic? $Lang['ePost']['Yes_1']:$Lang['ePost']['No_1']?>
					<?php } else { 
					?>
							<input type="radio" id="NewspaperOpenToPublic_Y" name="NewspaperOpenToPublic" value="<?=$cfg_ePost['BigNewspaper']['Newspaper_OpenToPublic']['Yes']?>" <?=$NewspaperID && $ePostNewspaper->Newspaper_OpenToPublic=='1'? "checked":""?>/>
							<label for="NewspaperOpenToPublic_Y"><?=$Lang['ePost']['Yes_1']?></label>
							<input type="radio" id="NewspaperOpenToPublic_N" name="NewspaperOpenToPublic" value="<?=$cfg_ePost['BigNewspaper']['Newspaper_OpenToPublic']['No']?>" <?=$NewspaperID && $ePostNewspaper->Newspaper_OpenToPublic=='1'? "":"checked"?>/>
							<label for="NewspaperOpenToPublic_N"><?=$Lang['ePost']['No_1']?></label>
					<?php } ?>
				</td>
			</tr>
			<tr>
				<td><?=$Lang['ePost']['StudentEditor']?></td>
				<td>:</td>
				<td><?=$ePostUI->gen_student_editor_selection($ePostStudentEditors, ($NewspaperID? $ePostNewspaper->Newspaper_StudentEditors:array()), ($IsReadOnly||!$ePostUI->user_obj->isTeacherStaff()))?></td>
			</tr>
		</table>
		<p class="spacer"></p>
	</div>
	<p class="spacer"></p>
	<div class="edit_bottom">
	<?php if(!$IsPopup){?>
		<p class="spacer"></p>
		<br>
	<?php } ?>
		<?php if(!$IsReadOnly){ ?>
			<input name="submit_btn" type="button" class="formbutton" onclick="go_submit()" value="<?=($IsPopup)?$Lang['ePost']['Button']['Submit']:$Lang['ePost']['Button']['SubmitAndNext']?>" />
		<?php } ?>
		<input name="cancel_btn" type="button" class="formsubbutton" onclick="go_cancel()" value="<?=$Lang['ePost']['Button']['Cancel']?>" />
	</div>
	<input type="hidden" id="FolderID" name="FolderID" value="<?=$FolderID?>"/>
	<input type="hidden" id="NewspaperID" name="NewspaperID" value="<?=$NewspaperID?>"/>
	<input type="hidden" id="IsPopup" name="IsPopup" value="<?=$IsPopup?>"/>
</form>
<?php if($IsPopup){?>
    </div>
</body>
<?php }else{ ?>
</div> </div> </div>
<?php } ?>
<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>