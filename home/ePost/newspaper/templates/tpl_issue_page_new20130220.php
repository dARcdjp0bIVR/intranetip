<?php
include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_header.php');
?>
<body class="edit_page_panel">
<script>
<?php if($PageTotal==0){?>
	$(document).ready(function() {
		add_new_page(<?=$PageID?>);
	});
<?php } ?>
	function edit_issue_info(){
		var obj = document.form1;
		
		obj.action = 'issue_new.php';
		obj.submit();  
	}
	function edit_issue_design(){
		var obj = document.form1;
		
		obj.action = 'issue_new_design.php';
		obj.submit();  
	}
	function add_new_page(p){
		var title = 'Edit Page Info';
		var url   = 'issue_page_new_info.php?PageID='+p+'&NewspaperID=<?=$NewspaperID?>&TB_iframe=true&width=700&height=400';
		
		tb_show(title, url);
	}	
	function refreshPage(p){
		var obj = document.form1;
		$('#PageID').val(p);
		obj.action = 'issue_page_new.php';
		obj.submit();  
	}		

</script>
	<div class="edit_page_panel_left" id="sidebar">
    	<div class="page_list">
    		<?=$li_html?>
        </div>
    </div>
    <div class="edit_page_panel_right">
    <!-- ################ Start of Newspaper ####################-->
	<?=$newspaper_body_html?>
    <!-- ################ End of Newspaper ####################-->
    </div>
	<form name="form1" action="issue_new_update.php" method="post" style="margin:0;padding:0">
    <div class="edit_page_header">
    	<div class="post_issue_title"> <?=$ePostNewspaper->Folder_Title?>  - <span><?=$ePostNewspaper->Newspaper_Title?> </span></div>
		<div class="table_row_tool" > 
			<div class="row_tool_group">
				<a href="#" title="Edit" class="tool_edit" onclick="MM_showHideLayers('edit_tool_layer','','show')">&nbsp;</a>
				<div class="row_tool_layer" id="edit_tool_layer" onclick="MM_showHideLayers('edit_tool_layer','','hide')">
					<a href="javascript:edit_issue_info();"><?=$Lang['ePost']['EditInformation']?></a>
					<a href="javascript:edit_issue_design()"><?=$Lang['ePost']['EditSkin']?></a>
				 </div>
			</div> 
			
		</div>
        <div class="post_issue_title"><span><em> | &nbsp;</em> <?=$Lang['ePost']['Status']?> : 
			<select name="Status" id="Status">
				<option value="<?=$cfg_ePost['BigNewspaper']['Newspaper_filter']['Draft']?>" <?=$Status==$cfg_ePost['BigNewspaper']['Newspaper_filter']['Draft']? "selected":""?>><?=$Lang['ePost']['Filter']['Draft']?></option>
				<option value="<?=$cfg_ePost['BigNewspaper']['Newspaper_filter']['WaitApproval']?>" <?=$Status==$cfg_ePost['BigNewspaper']['Newspaper_filter']['WaitApproval']? "selected":""?>><?=$Lang['ePost']['Filter']['WaitingApproval']?></option>
				<option value="<?=$cfg_ePost['BigNewspaper']['Newspaper_filter']['Published']?>" <?=$Status==$cfg_ePost['BigNewspaper']['Newspaper_filter']['Published']? "selected":""?>><?=$Lang['ePost']['Filter']['Published']?></option>
			</select>
		</span>  
		</div>    			
        <div class="edit_page_btn"><input name="finish_btn" type="button" class="formbutton" value="<?=$Lang['ePost']['Button']['Finish']?>" />  </div>
    </div>
<div class="edit_page_tool"> <div class="Content_tool"><a href="javascript:add_new_page('');"><?=$Lang['ePost']['NewPage']?></a> </div><span class="page_total"> <?=$Lang['ePost']['Total']?> : <?=$PageTotal?></span>
</div>

		<input type="hidden" id="FolderID" name="FolderID" value="<?=$ePostNewspaper->FolderID?>"/>
		<input type="hidden" id="NewspaperID" name="NewspaperID" value="<?=$ePostNewspaper->NewspaperID?>"/>
		<input type="hidden" id="PageID" name="PageID" value=""/>
    </div>
    <p class="spacer"></p> 

<?php
include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_footer.php');
?>