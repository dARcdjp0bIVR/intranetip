<?php
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

<script>
	function add_page(){
		var obj = document.form1;
		
		obj.action = 'issue_page_new.php';
		obj.submit();
	}

	function edit_issue_info(){
		var obj = document.form1;
		
		obj.action = 'issue_new.php';
		obj.submit();  
	}
	
	function change_publish_status(){
		var default_publish_status = <?=$ePostNewspaper->Newspaper_PublishStatus?>;
		if(confirm('<?=$Lang['ePost']['Confirm']['ChangePublishStatus'][0]?><?=$ePostNewspaper->Newspaper_Title?><?=$Lang['ePost']['Confirm']['ChangePublishStatus'][1]?><?=$ePostNewspaper->Newspaper_Name?><?=$Lang['ePost']['Confirm']['ChangePublishStatus'][2]?>'+$('#PublishStatus option:selected').html()+'<?=$Lang['ePost']['Confirm']['ChangePublishStatus'][3]?>')){
			var obj = document.form1;
			
			obj.action = 'issue_publish_status_update.php?IsPageList=1';
			obj.submit();
		}
		else{
			$('#PublishStatus').val(default_publish_status);
		}
	}
	
	function edit_page(PageID){
		var obj = document.form1;
		
		obj.PageID.value = PageID;
		obj.action = 'issue_page_new.php';
		obj.submit();
	}
	
	function go_delete(PageID){
		var obj = document.form1;
		var PageName = document.getElementById('Page_title_' + PageID).innerHTML;
		
		if(confirm('<?=$Lang['ePost']['Confirm']['RemovePage'][0]?>'+ PageName + '<?=$Lang['ePost']['Confirm']['RemovePage'][1]?>')){
			obj.action = 'issue_page_delete.php';
			obj.PageID.value = PageID;
			obj.submit();
		}
	}
	
	function openNewspaper(PageID){
		var intWidth  = screen.width;
	 	var intHeight = screen.height;
		window.open('/home/ePost/newspaper/view_newspaper.php?NewspaperID=<?=$ePostNewspaper->NewspaperID?>&PageID='+PageID,"ePost_Newspaper","width="+intWidth+",height="+intHeight+",scrollbars,resizable");
	}
</script>

<form name="form1" action="" method="post" style="margin:0;padding:0">
	<p class="spacer">&nbsp; </p>
	<?=$ePostUI->gen_admin_newspaper_page_nav(0)?>
	<p class="spacer">&nbsp; </p>
	<?=$ePostUI->gen_portal_page_nav($nav_ary)?>
	<p class="spacer">&nbsp; </p>
	<div class="Content_tool" style="width:100%">
		<?php if($ePostNewspaper->Newspaper_PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft']){?>
			<a href="javascript:add_page()" class="new"><?=$Lang['ePost']['CreateNewPage']?></a>
		<?php } ?>
		<a href="javascript:edit_issue_info()" class="edit_text"><?=$ePostNewspaper->Newspaper_PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft']? $Lang['ePost']['EditIssueInformation']:$Lang['ePost']['ViewIssueInformation']?></a>
		<div style="float:right">
			<?=$Lang['ePost']['PublishStatus']?> :
			<?=$ePostUI->gen_publish_status_drop_down_list('PublishStatus', 'PublishStatus', $ePostNewspaper->Newspaper_PublishStatus, "onchange='change_publish_status()'", (count($ePostNewspaper->Newspaper_PageIDs)==0))?>
		</div>
	</div>
	<div id="sys_msg"><?=$Lang['ePost']['ReturnMessage'][$returnMsg]?></div>
	<div class="table_board">
		<table class="common_table_list edit_table_list">
			<col nowrap="nowrap"/>
			<tr>
				<th width="30">#</th>
				<th width=""><?=$Lang['ePost']['PageTitle']?></th>
				<th><?=$Lang['ePost']['Theme']?></th>
				<th><?=$Lang['ePost']['Layout']?></th>
				<th><?=$Lang['ePost']['NoOfArticles']?></th>
				<th><?=$Lang['ePost']['LastModified']?></th>
				<?php if($ePostNewspaper->Newspaper_PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft']){?>
					<th width="100">&nbsp;</th>
				<?php } ?>
			</tr>
			<?=$table_html?>
		</table>
		<p class="spacer"></p>
		<p class="spacer"></p>
		<br />
	</div>
	<input type="hidden" id="FolderID" name="FolderID" value="<?=$ePostNewspaper->FolderID?>"/>
	<input type="hidden" id="NewspaperID" name="NewspaperID" value="<?=$ePostNewspaper->NewspaperID?>"/>
	<input type="hidden" id="PageID" name="PageID" value=""/>
</form>
<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>