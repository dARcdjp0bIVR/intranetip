<?php
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

<script>
	function change_issue_information(){
		var obj = document.form1;
		
		obj.action = 'issue_new.php';
		obj.submit();
	}

	function select_skin(skin_id){
		$('.skin_select').removeClass('select_layout_selected');
		$('#'+skin_id).addClass('select_layout_selected');
		$('#NewspaperSkin').val(skin_id);
		
		$('#NewspaperCoverBanner_Preview_NewspaperBody > div, #NewspaperInsideBanner_Preview_NewspaperBody > div').removeClass();
		switch(skin_id){
			<?php foreach($cfg_ePost['BigNewspaper']['skins'] as $skin_id=>$skin_item){?>
				case '<?=$skin_id?>':
					$('#NewspaperCoverBanner_Preview_NewspaperBody > div').addClass('page_big_cover<?=$skin_item['page_big_class_suffix']?>');
					$('#NewspaperInsideBanner_Preview_NewspaperBody > div').addClass('page_big_left<?=$skin_item['page_big_class_suffix']?>');
					break;
			<?php } ?>
		}
		$('#NewspaperCoverBanner_Preview_NewspaperBody > div, #NewspaperInsideBanner_Preview_NewspaperBody > div').addClass('newspaper_page_big');
	}

	function go_submit(target){
		var obj = document.form1;
		
		if(!obj.NewspaperSkin.value){
			alert('<?=$Lang['ePost']['Warning']['SelectSkin']?>');
			return false;
		}
		else{
			obj.NewspaperCoverBannerHideTitle.value    = $('#NewspaperCoverBannerHideTitle').attr('checked')? 1:0;
			obj.NewspaperCoverBannerHideName.value     = $('#NewspaperCoverBannerHideName').attr('checked')? 1:0;
			obj.NewspaperInsideBannerHideTitle.value   = $('#NewspaperInsideBannerHideTitle').attr('checked')? 1:0;
			obj.NewspaperInsideBannerHideName.value    = $('#NewspaperInsideBannerHideName').attr('checked')? 1:0;
			obj.NewspaperInsideBannerHidePageCat.value = $('#NewspaperInsideBannerHidePageCat').attr('checked')? 1:0;
			obj.TargetPage.value = target;
			obj.submit();
		}
	}
	
	function go_cancel(){
		var obj = document.form1;
		obj.action = "issue_page_new.php?NewspaperID=<?=$ePostNewspaper->NewspaperID?>";
		<?php if($IsPopup) {?>
			parent.window.tb_remove();
		<?php }else{ ?>
			obj.submit();
		<?php } ?>
	}
	
	function file_upload_change(FileUploadForm){
		var file_upload_action = FileUploadForm.FileUploadAction.value;
		var file_upload  	   = FileUploadForm.FileUpload.value;
		var ext 		 	   = file_upload.substr((file_upload.length-4)).toUpperCase();
		
		if(ext=='.GIF' || ext=='.JPG' || ext=='.JPE' || ext=='.JPEG' || ext=='.PNG' || ext=='.BMP'){
			$('#FileUploadForm_'+file_upload_action).ajaxSubmit({
	        	url			: 'ajax_file_upload.php',
	        	type		: 'post',
	        	dataType	: 'json', 
	        	clearForm	: true,
	        	beforeSubmit: file_upload_prepare,
				success		: file_upload_success
			});
		}
		else{
			FileUploadForm.reset();
			alert('<?=$Lang['ePost']['Warning']['UploadImageFile']?>');
		}
	}
	
	function file_upload_prepare(formData, jqForm, options){
		var form = jqForm[0]; 
		var FileUploadAction = form.FileUploadAction.value;

		$('#Newspaper'+FileUploadAction+'_UploadBtn_NoFile').hide();
		$('#Newspaper'+FileUploadAction+'_UploadBtn_Uploading').show();
		$('#Newspaper'+FileUploadAction+'_UploadBtn_Uploaded').hide();
	}
	
	function file_upload_success(responseObj, statusText, xhr, $form){
		var FileUploadAction = responseObj.upload_action;

		if(responseObj.result){
			$('#Newspaper'+FileUploadAction).val(responseObj.upload_file_path + responseObj.upload_file_name)
			$('#Newspaper'+FileUploadAction+'_Display').html(responseObj.upload_file_name);
			
			$('#Newspaper'+FileUploadAction+'_UploadBtn_NoFile').hide();
			$('#Newspaper'+FileUploadAction+'_UploadBtn_Uploading').hide();
			$('#Newspaper'+FileUploadAction+'_UploadBtn_Uploaded').show();
			
			file_upload_preview_update(FileUploadAction, responseObj.upload_file_http_path);
		}
		else{
			$('#Newspaper'+FileUploadAction+'_UploadBtn_NoFile').show();
			$('#Newspaper'+FileUploadAction+'_UploadBtn_Uploading').hide();
			$('#Newspaper'+FileUploadAction+'_UploadBtn_Uploaded').hide();
			
			alert('<?=$Lang['ePost']['Warning']['FailedToUploadImage']?>');	
		}
	}
	
	function file_upload_preview_update(FileUploadAction, FilePath){
		if(FileUploadAction=='<?=$cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']?>' || FileUploadAction=='<?=$cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']?>'){
			var BannerBgImage = 'url('+FilePath+')';
		
			$('#Newspaper'+FileUploadAction+'_Preview_NewspaperBody').addClass('customized');
			$('#Newspaper'+FileUploadAction+'_Preview_Banner').css('background-image', BannerBgImage);
		}
		else if(FileUploadAction=='<?=$cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']?>'){
			$('#NewspaperCoverBanner_Preview_PageTopBg_h1, #NewspaperInsideBanner_Preview_PageTopBg_h1').addClass('with_logo');
			$('#NewspaperCoverBanner_Preview_SchoolLogo, #NewspaperInsideBanner_Preview_SchoolLogo').attr('src', FilePath);
		}
	}
	
	function hide_ePost_title(FileUploadAction){
		if($('#Newspaper'+FileUploadAction+'HideTitle').attr('checked')){
			$('#Newspaper'+FileUploadAction+'_Preview_Title').hide();
		}
		else{
			$('#Newspaper'+FileUploadAction+'_Preview_Title').show();
		}
	}
	
	function hide_issue_name(FileUploadAction){
		if($('#Newspaper'+FileUploadAction+'HideName').attr('checked')){
			$('#Newspaper'+FileUploadAction+'_Preview_Name').hide();
		}
		else{
			$('#Newspaper'+FileUploadAction+'_Preview_Name').show();
		}
	}
	
	function hide_page_cat(FileUploadAction){
		if($('#Newspaper'+FileUploadAction+'HidePageCat').attr('checked')){
			$('#page_cat').hide();
		}
		else{
			$('#page_cat').show();
		}
	}
	
	function delete_school_logo(){
		if(confirm('<?=$Lang['ePost']['Confirm']['RemoveSchoolLogo']?>')){
			$('#NewspaperSchoolLogo_Display').html('');
			document.form1.NewspaperSchoolLogo.value = '';
			document.FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']?>.reset();
			
			$('#NewspaperSchoolLogo_UploadBtn_NoFile').show();
			$('#NewspaperSchoolLogo_UploadBtn_Uploading').hide();
			$('#NewspaperSchoolLogo_UploadBtn_Uploaded').hide();
			
			$('#NewspaperCoverBanner_Preview_PageTopBg_h1, #NewspaperInsideBanner_Preview_PageTopBg_h1').removeClass('with_logo');
		}
	}
	
	function delete_cover_banner(){
		if(confirm('<?=$Lang['ePost']['Confirm']['RemoveCoverBanner']?>')){
			$('#NewspaperCoverBanner_Display').html('');
			document.form1.NewspaperCoverBanner.value = '';
			document.FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']?>.reset();
			
			$('#NewspaperCoverBanner_UploadBtn_NoFile').show();
			$('#NewspaperCoverBanner_UploadBtn_Uploading').hide();
			$('#NewspaperCoverBanner_UploadBtn_Uploaded').hide();
			
			$('#NewspaperCoverBannerHideTitle').attr('checked', false);
			$('#NewspaperCoverBannerHideName').attr('checked', false);
			
			$('#NewspaperCoverBanner_Preview_NewspaperBody').removeClass('customized');
			$('#NewspaperCoverBanner_Preview_Banner').css('background-image', '');
			
			hide_ePost_title('<?=$cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']?>');
			hide_issue_name('<?=$cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']?>');
		}
	}
	
	function delete_inside_banner(){
		if(confirm('<?=$Lang['ePost']['Confirm']['RemoveInsideBanner']?>')){
			$('#NewspaperInsideBanner_Display').html('');
			document.form1.NewspaperInsideBanner.value = '';
			document.FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']?>.reset();
			
			$('#NewspaperInsideBanner_UploadBtn_NoFile').show();
			$('#NewspaperInsideBanner_UploadBtn_Uploading').hide();
			$('#NewspaperInsideBanner_UploadBtn_Uploaded').hide();
			
			$('#NewspaperInsideBannerHideTitle').attr('checked', false);
			$('#NewspaperInsideBannerHideName').attr('checked', false);
			$('#NewspaperInsideBannerHidePageCat').attr('checked', false);
			
			$('#NewspaperInsideBanner_Preview_NewspaperBody').removeClass('customized');
			$('#NewspaperInsideBanner_Preview_Banner').css('background-image', '');
			
			hide_ePost_title('<?=$cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']?>');
			hide_issue_name('<?=$cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']?>');
			hide_page_cat('<?=$cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']?>');
		}
	}
	function display_banner(type){
		if(type=='c'){
			$('#tr_cover_banner').toggle();
			$('#NewspaperCoverBanner_Preview').toggle();
		}else if(type=='i'){
			$('#tr_inside_banner').toggle();
			$('#NewspaperInsideBanner_Preview').toggle();
		}
	}
</script>
<?php if($IsPopup) {?>
<body id="edit_pop" class="edit_pop_issue_edit">
    <div class="pop_container">
		<div class="sectiontitle"><?=$Lang['ePost']['IssueSkin']?></div>
		<div class="marking_board">      
			<p class="spacer"></p>
<?php }else{ ?>
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
<p class="spacer">&nbsp;</p>
<?=$ePostUI->gen_portal_page_nav($nav_ary)?>
<br>
<p class="spacer">&nbsp;</p>
<div class="marking_board">
	<br/>
	<div class="stepboard stepboard_3s">
		<h1><?=$Lang['ePost']['Step']?>:</h1>
		<ul> 
				<li>
					<div>
						<em>1</em>
						<span><?=$Lang['ePost']['InputIssueInformation']?></span>
					</div>
				</li>
				<li class="stepon">
					<?php if($IsReadOnly){ ?>
						<a href="javascript:void(0)" onclick="view_issue_design()">
					<?php } ?>
							<div>
								<em>2</em>
								<span><?=$Lang['ePost']['SelectSkin']?></span>
							</div>
					<?php if($IsReadOnly){ ?>
						</a>
					<?php } ?>
				</li>
				<li>
					<div>
						<em>3</em>
						<span><?=$Lang['ePost']['EditPages']?></span>
					</div>
				</li>
		</ul> 
		<br style="clear:both" />
	</div>
<?php } ?>	
	<table class="form_table">
		<colgroup>
			<col class="field_title">
			<col class="field_c">
		</colgroup>
		<tr>
			<td><?=$Lang['ePost']['Skin']?></td>
			<td>:</td>
			<td><?=$skin_selection_html?></td>
		</tr>
		<tr>
			<td><?=$Lang['ePost']['CustomizeItem']?></td>
			<td>:</td>
			<td>
			<input type="checkbox" name="has_school_logo" id="has_school_logo" onclick="$('#tr_school_logo').toggle();" <?=((!empty($school_logo))?'checked="checked"':'')?> />
			<label for="has_school_logo"><?=$Lang['ePost']['SchoolLogo']?></label>
			<input type="checkbox" name="has_cover_banner" id="has_cover_banner" onclick="display_banner('c');" <?=((!empty($cover_banner))?'checked="checked"':'')?> />
			<label for="has_cover_banner"><?=$Lang['ePost']['CoverBanner']?></label>
			<input type="checkbox" name="has_inside_banner" id="has_inside_banner" onclick="display_banner('i');" <?=((!empty($inside_banner))?'checked="checked"':'')?> />
			<label for="has_inside_banner"><?=$Lang['ePost']['InsidePageBanner']?></label>
			</td>
		</tr>		
		<tr id="tr_school_logo" <?=((empty($school_logo))?'style="display:none;"':'')?>>
			<td><?=$Lang['ePost']['SchoolLogo']?></td>
			<td>:</td>
			<td>
				<?php if($IsReadOnly){ ?>
					<?php if($ePostNewspaper->Newspaper_SchoolLogo){ ?>
						<a id="NewspaperSchoolLogo_Display" class="img_file" href="javascript:void(0)"><?=$ePostNewspaper->Newspaper_SchoolLogo?></a>
					<?php } else { ?>
						<?=$Lang['ePost']['Nil']?>
					<?php } ?>
				<?php } else { ?>
					<form id="FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']?>" name="FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']?>" action="ajax_file_upload.php" method="post" style="margin:0;padding:0" enctype="multipart/form-data">
						<div id="NewspaperSchoolLogo_UploadBtn_NoFile" <?=$ePostNewspaper->Newspaper_SchoolLogo? "style='display:none'":""?>>
							<input type="hidden" id="FileUploadAction" name="FileUploadAction" value="<?=$cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']?>"/>
							<input type="file" id="FileUpload" name="FileUpload" onchange="file_upload_change(this.form)" accept='image/*'/>
							<span class="tabletextremark"><?=$Lang['ePost']['SchoolLogoInstruction']?></span>
						</div>
						<div id="NewspaperSchoolLogo_UploadBtn_Uploading" style="display:none">
							<a href="javascript:void(0)"><?=$Lang['ePost']['Uploading']?></a><img src="<?=$intranet_httppath?>/images/<?=$LAYOUT_SKIN?>/indicator.gif">
						</div>
						<div id="NewspaperSchoolLogo_UploadBtn_Uploaded" <?=$ePostNewspaper->Newspaper_SchoolLogo? "":"style='display:none'"?>>
                    		<a id="NewspaperSchoolLogo_Display" class="img_file" href="javascript:void(0)"><?=$ePostNewspaper->Newspaper_SchoolLogo?></a>
                    		<div class="table_row_tool">
                    			<a title="Delete" class="tool_delete" href="javascript:void(0)" onclick="delete_school_logo()">&nbsp;</a>
                    		</div>
                    	</div>
                    </form>
				<?php } ?>
			</td>
		</tr>
		<tr id="tr_cover_banner" <?=((empty($cover_banner))?'style="display:none;"':'')?>>
			<td><?=$Lang['ePost']['CustomizeBannerCover']?></td>
			<td>:</td>
			<td>
				<?php if($IsReadOnly){ ?>
					<?php if($ePostNewspaper->Newspaper_CoverBanner){ ?>
						<a id="NewspaperCoverBanner_Display" class="img_file" href="javascript:void(0)"><?=$ePostNewspaper->Newspaper_CoverBanner?></a>
						<div class="table_row_tool" style="visibility:hidden">
	                    	<a title="Delete" class="tool_delete" href="javascript:void(0)">&nbsp;</a>
	                    </div>
						<p class="spacer"></p>
						<?php if($ePostNewspaper->Newspaper_CoverBannerHideTitle){ ?>
							<input type="checkbox" id="NewspaperCoverBannerHideTitle" name="NewspaperCoverBannerHideTitle" value="1" onclick="return false;" checked/>
		                   	<label for="NewspaperCoverBannerHideTitle"><?=$Lang['ePost']['HideEPostTitle']?></label>
	                    <?php } ?>
	                    <?php if($ePostNewspaper->Newspaper_CoverBannerHideName){ ?>
		                   	<input type="checkbox" id="NewspaperCoverBannerHideName" name="NewspaperCoverBannerHideName" value="1" onclick="return false;" checked/>
		                   	<label for="NewspaperCoverBannerHideName"><?=$Lang['ePost']['HideIssueName']?></label>
	                    <?php } ?>
					<?php } else { ?>
						<?=$Lang['ePost']['Nil']?>
					<?php } ?>
				<?php } else { ?>
					<form id="FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']?>" name="FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']?>" action="ajax_file_upload.php" method="post" style="margin:0;padding:0" enctype="multipart/form-data">
						<div id="NewspaperCoverBanner_UploadBtn_NoFile" <?=$ePostNewspaper->Newspaper_CoverBanner? "style='display:none'":""?>>
							<input type="hidden" id="FileUploadAction" name="FileUploadAction" value="<?=$cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']?>"/>
							<input type="file" id="FileUpload" name="FileUpload" onchange="file_upload_change(this.form)" accept='image/*'/>
							<span class="tabletextremark"><?=$Lang['ePost']['CustomizeBannerCoverInstrucation']?></span>
                    	</div>
                    	<div id="NewspaperCoverBanner_UploadBtn_Uploading" style="display:none">
                    		<a href="javascript:void(0)"><?=$Lang['ePost']['Uploading']?></a><img src="<?=$intranet_httppath?>/images/<?=$LAYOUT_SKIN?>/indicator.gif">
						</div>
                    	<div id="NewspaperCoverBanner_UploadBtn_Uploaded" <?=$ePostNewspaper->Newspaper_CoverBanner? "":"style='display:none'"?>>
	                   		<a id="NewspaperCoverBanner_Display" class="img_file" href="javascript:void(0)"><?=$ePostNewspaper->Newspaper_CoverBanner?></a>
	                   		<div class="table_row_tool">
	                   			<a title="Delete" class="tool_delete" href="javascript:void(0)" onclick="delete_cover_banner()">&nbsp;</a>
	                   		</div>
	                   		<p class="spacer"></p>
	                   		<input type="checkbox" id="NewspaperCoverBannerHideTitle" name="NewspaperCoverBannerHideTitle" value="1" onclick="hide_ePost_title('<?=$cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']?>')" <?=$ePostNewspaper->Newspaper_CoverBannerHideTitle? "checked":""?>/>
	                   		<label for="NewspaperCoverBannerHideTitle"><?=$Lang['ePost']['HideEPostTitle']?></label>
	                   		<input type="checkbox" id="NewspaperCoverBannerHideName" name="NewspaperCoverBannerHideName" value="1" onclick="hide_issue_name('<?=$cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']?>')" <?=$ePostNewspaper->Newspaper_CoverBannerHideName? "checked":""?>/>
	                   		<label for="NewspaperCoverBannerHideName"><?=$Lang['ePost']['HideIssueName']?></label>
						</div>
					</form>
				<?php } ?>
			</td>
		</tr>
		<tr id="NewspaperCoverBanner_Preview" <?=((empty($cover_banner))?'style="display:none;"':'')?>>
			<td colspan="3">
				<fieldset>
					<legend><?=$Lang['ePost']['Preview']?></legend>
					<div id="NewspaperCoverBanner_Preview_NewspaperBody" class="newspaper_body <?=$ePostNewspaper->Newspaper_CoverBanner? "customized":""?>">
						<div class="newspaper_page_big page_big_cover<?=$ePostNewspaper->get_page_big_class_suffix()?>">
							<div class="page_top">
								<div class="page_top_right">
									<div class="page_top_bg">
										<h1 id="NewspaperCoverBanner_Preview_PageTopBg_h1" class="<?=$ePostNewspaper->Newspaper_SchoolLogo? "with_logo":""?>">
											<div id="NewspaperCoverBanner_Preview_Banner" <?=$ePostNewspaper->Newspaper_CoverBanner? "style=\"background-image:url('".$ePostNewspaper->return_newspaper_attachment_http_path($cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner'])."')\"":""?> class="custom_banner"></div>
											<span id="NewspaperCoverBanner_Preview_Title" class="newspaper_title" <?=$ePostNewspaper->Newspaper_CoverBannerHideTitle? "style=\"display:none\"":""?>>
												<?=$ePostNewspaper->Newspaper_Title? $ePostNewspaper->Newspaper_Title:$Lang['ePost']['TitleWillBeShownHere']?>
											</span>
											<span class="school_logo">
												<img id="NewspaperCoverBanner_Preview_SchoolLogo" src="<?=$ePostNewspaper->Newspaper_SchoolLogo? $ePostNewspaper->return_newspaper_attachment_http_path($cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']):""?>">
											</span>
											<span id="NewspaperCoverBanner_Preview_Name" class="issue_title" <?=$ePostNewspaper->Newspaper_CoverBannerHideName? "style=\"display:none\"":""?>>
												<?=$ePostNewspaper->Newspaper_Name? $ePostNewspaper->Newspaper_Name:$Lang['ePost']['NameWillBeShownHere']?>
											</span>
										</h1>
										<!---title editable end-->
									</div>
								</div>
							</div>
							<p class="spacer"></p>
						</div>
					</div>
				</fieldset>
				<p class="spacer">&nbsp;</p>
			</td>
		</tr>
		<tr id="tr_inside_banner" <?=((empty($inside_banner))?'style="display:none;"':'')?>>
			<td><?=$Lang['ePost']['CustomizeBannerInside']?></td>
			<td>:</td>
			<td>
				<?php if($IsReadOnly){ ?>
					<a id="NewspaperInsideBanner_Display" class="img_file" href="javascript:void(0)"><?=$ePostNewspaper->Newspaper_InsideBanner?></a>
                    <div class="table_row_tool" style="visibility:hidden">
                    	<a title="Delete" class="tool_delete" href="javascript:void(0)">&nbsp;</a>
                    </div>
                    <p class="spacer"></p>
                    <?php if($ePostNewspaper->Newspaper_InsideBannerHideTitle){ ?>
	                    <input type="checkbox" id="NewspaperInsideBannerHideTitle" name="NewspaperInsideBannerHideTitle" value="1" onclick="return false;" checked/>
	                    <label for="NewspaperInsideBannerHideTitle"><?=$Lang['ePost']['HideEPostTitle']?></label>
	                <?php } ?>
	                <?php if($ePostNewspaper->Newspaper_InsideBannerHideName){ ?>
	                    <input type="checkbox" id="NewspaperInsideBannerHideName" name="NewspaperInsideBannerHideName" value="1" onclick="return false;" checked/>
	                    <label for="NewspaperInsideBannerHideName"><?=$Lang['ePost']['HideIssueName']?></label>
	                <?php } ?>
	                <?php if($ePostNewspaper->Newspaper_InsideBannerHidePageCat){ ?>
	                    <input type="checkbox" id="NewspaperInsideBannerHidePageCat" name="NewspaperInsideBannerHidePageCat" value="1" onclick="return false;" checked/>
	                    <label for="NewspaperInsideBannerHidePageCat"><?=$Lang['ePost']['HidePageCategory']?></label>
	                <?php } ?>
				<?php } else { ?>
					<form id="FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']?>" name="FileUploadForm_<?=$cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']?>" action="ajax_file_upload.php" method="post" style="margin:0;padding:0" enctype="multipart/form-data">
						<div id="NewspaperInsideBanner_UploadBtn_NoFile" <?=$ePostNewspaper->Newspaper_InsideBanner? "style='display:none'":""?>>
							<input type="hidden" id="FileUploadAction" name="FileUploadAction" value="<?=$cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']?>"/>
							<input type="file" id="FileUpload" name="FileUpload" onchange="file_upload_change(this.form)" accept='image/*'/>
							<span class="tabletextremark"><?=$Lang['ePost']['CustomizeBannerInsideInstrucation']?></span>
                    	</div>
                    	<div id="NewspaperInsideBanner_UploadBtn_Uploading" style="display:none">
                    		<a href="javascript:void(0)"><?=$Lang['ePost']['Uploading']?></a><img src="<?=$intranet_httppath?>/images/<?=$LAYOUT_SKIN?>/indicator.gif">
                    	</div>
                    	<div id="NewspaperInsideBanner_UploadBtn_Uploaded" <?=$ePostNewspaper->Newspaper_InsideBanner? "":"style='display:none'"?>>
                    		<a id="NewspaperInsideBanner_Display" class="img_file" href="javascript:void(0)"><?=$ePostNewspaper->Newspaper_InsideBanner?></a>
                    		<div class="table_row_tool">
                    			<a title="Delete" class="tool_delete" href="javascript:void(0)" onclick="delete_inside_banner()">&nbsp;</a>
                    		</div>
                    		<p class="spacer"></p>
                    		<input type="checkbox" id="NewspaperInsideBannerHideTitle" name="NewspaperInsideBannerHideTitle" value="1" onclick="hide_ePost_title('<?=$cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']?>')" <?=$ePostNewspaper->Newspaper_InsideBannerHideTitle? "checked":""?>/>
                    		<label for="NewspaperInsideBannerHideTitle"><?=$Lang['ePost']['HideEPostTitle']?></label>
                    		<input type="checkbox" id="NewspaperInsideBannerHideName" name="NewspaperInsideBannerHideName" value="1" onclick="hide_issue_name('<?=$cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']?>')" <?=$ePostNewspaper->Newspaper_InsideBannerHideName? "checked":""?>/>
                    		<label for="NewspaperInsideBannerHideName"><?=$Lang['ePost']['HideIssueName']?></label>
                    		<input type="checkbox" id="NewspaperInsideBannerHidePageCat" name="NewspaperInsideBannerHidePageCat" value="1" onclick="hide_page_cat('<?=$cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']?>')" <?=$ePostNewspaper->Newspaper_InsideBannerHidePageCat? "checked":""?>/>
                    		<label for="NewspaperInsideBannerHidePageCat"><?=$Lang['ePost']['HidePageCategory']?></label>
                    	</div>
                    </form>
				<?php } ?>
			</td>
		</tr>
		<tr id="NewspaperInsideBanner_Preview" <?=((empty($inside_banner))?'style="display:none;"':'')?>>
			<td colspan="3">
				<fieldset>
					<legend><?=$Lang['ePost']['Preview']?></legend>
					<div id="NewspaperInsideBanner_Preview_NewspaperBody" class="newspaper_body <?=$ePostNewspaper->Newspaper_InsideBanner? "customized":""?>">
						<div class="newspaper_page_big page_big_left<?=$ePostNewspaper->get_page_big_class_suffix()?>">
							<div class="page_top">
								<div class="page_top_right">
									<div class="page_top_bg">
       									<h1 id="NewspaperInsideBanner_Preview_PageTopBg_h1" class="<?=$ePostNewspaper->Newspaper_SchoolLogo? "with_logo":""?>">
											<div id="NewspaperInsideBanner_Preview_Banner" <?=$ePostNewspaper->Newspaper_InsideBanner? "style=\"background-image:url('".$ePostNewspaper->return_newspaper_attachment_http_path($cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner'])."')\"":""?> class="custom_banner"></div> 
                   							<span id="NewspaperInsideBanner_Preview_Title" class="newspaper_title" <?=$ePostNewspaper->Newspaper_InsideBannerHideTitle? "style=\"display:none\"":""?>>
                   								<?=$ePostNewspaper->Newspaper_Title? $ePostNewspaper->Newspaper_Title:$Lang['ePost']['TitleWillBeShownHere']?>
                   							</span>
											<span class="school_logo">
              		 							<img id="NewspaperInsideBanner_Preview_SchoolLogo" src="<?=$ePostNewspaper->Newspaper_SchoolLogo? $ePostNewspaper->return_newspaper_attachment_http_path($cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']):""?>">
              		 						</span>
                							<span id="NewspaperInsideBanner_Preview_Name" class="issue_title" <?=$ePostNewspaper->Newspaper_InsideBannerHideName? "style=\"display:none\"":""?>>
                								<?=$ePostNewspaper->Newspaper_Name? $ePostNewspaper->Newspaper_Name:$Lang['ePost']['NameWillBeShownHere']?>
                							</span>    
                    						<span class="page_cat_02" id="page_cat" <?=$ePostNewspaper->Newspaper_InsideBannerHidePageCat? "style=\"display:none\"":""?>><?=$Lang['ePost']['PageNameWillBeShownHere']?></span>
										</h1>
									</div>
								</div>
							</div>
							<p class="spacer"></p>
						</div>
					</div>
				</fieldset>
				<p class="spacer">&nbsp;</p>
			</td>
		</tr>
	</table>
	<p class="spacer"></p>
</div>
<p class="spacer"></p>
<div class="edit_bottom">
<?php if(!$IsPopup){?>
	<p class="spacer"></p>
	<br>
<?php } ?>

	<?php if(!$IsReadOnly){ ?>
		<input name="submit_btn" type="button" class="formbutton" onclick="go_submit(1)" value="<?=$Lang['ePost']['Button']['Submit']?>" />
		<?php if(!$IsPopup){ ?>
			<input name="submit_btn" type="button" class="formbutton" onclick="go_submit(2)" value="<?=$Lang['ePost']['Button']['SubmitAndCreatePage']?>" />
		<?php } ?>
	<?php } ?>
	<input name="cancel_btn" type="button" class="formsubbutton" onclick="go_cancel()" value="<?=$Lang['ePost']['Button']['Cancel']?>" />
</div>
<form name="form1" action="issue_new_design_update.php" method="post" style="margin:0;padding:0">
	<input type="hidden" id="NewspaperSkin" name="NewspaperSkin" value="<?=$default_skin?>">
	<input type="hidden" id="NewspaperSchoolLogo" name="NewspaperSchoolLogo" value="<?=$ePostNewspaper->Newspaper_SchoolLogo?>"/>
	<input type="hidden" id="NewspaperCoverBanner" name="NewspaperCoverBanner" value="<?=$ePostNewspaper->Newspaper_CoverBanner?>"/>
	<input type="hidden" id="NewspaperInsideBanner" name="NewspaperInsideBanner" value="<?=$ePostNewspaper->Newspaper_InsideBanner?>"/>
	<input type="hidden" name="NewspaperCoverBannerHideTitle" value="<?=$ePostNewspaper->Newspaper_CoverBannerHideTitle? "1":"0"?>"/>
	<input type="hidden" name="NewspaperCoverBannerHideName" value="<?=$ePostNewspaper->Newspaper_CoverBannerHideName? "1":"0"?>"/>
	<input type="hidden" name="NewspaperInsideBannerHideTitle" value="<?=$ePostNewspaper->Newspaper_InsideBannerHideTitle? "1":"0"?>"/>
	<input type="hidden" name="NewspaperInsideBannerHideName" value="<?=$ePostNewspaper->Newspaper_InsideBannerHideName? "1":"0"?>"/>
	<input type="hidden" name="NewspaperInsideBannerHidePageCat" value="<?=$ePostNewspaper->Newspaper_InsideBannerHidePageCat? "1":"0"?>"/>
	<input type="hidden" id="FolderID" name="FolderID" value="<?=$ePostNewspaper->FolderID?>"/>
	<input type="hidden" id="NewspaperID" name="NewspaperID" value="<?=$ePostNewspaper->NewspaperID?>"/>
	<input type="hidden" id="PageID" name="PageID" value="<?=$PageID?>"/>
	<input type="hidden" id="TargetPage" name="TargetPage"/>
	<input type="hidden" id="IsPopup" name="IsPopup" value="<?=$IsPopup?>"/>
</form>
<?php if($IsPopup){?>
    </div>
</body>
<?php }else{ ?>
</div> </div> </div>
<?php } ?>
<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>