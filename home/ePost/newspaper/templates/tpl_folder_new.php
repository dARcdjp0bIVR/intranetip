<?php
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

<script>
	function go_submit(){
		var obj = document.form1;
		
		if(!obj.FolderTitle.value){
			alert('<?=$Lang['ePost']['Warning']['InputTitle']?>');
			obj.FolderTitle.focus();
			return false;
		}
		else{
			obj.submit();
		}
	}
	
	function go_cancel(){
		var obj = document.form1;
		obj.action = "index.php";
		obj.submit();
	}
</script>
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
<form name="form1" action="folder_new_update.php" method="post" style="margin:0;padding:0">

	<p class="spacer">&nbsp;</p>
	<?=$ePostUI->gen_portal_page_nav($nav_ary)?>
	<br>
	<p class="spacer">&nbsp;</p>
	<div class="marking_board">
		<br>
		<table class="form_table">
			<colgroup>
				<col class="field_title">
				<col class="field_c">
			</colgroup>
			<tr>
				<td><?=$Lang['ePost']['Title']?></td>
				<td>:</td>
				<td><input name="FolderTitle" type="text" id="FolderTitle" size="15" value="<?=$FolderID? $ePostFolder->Folder_Title:''?>" maxlength="150"/></td>
			</tr>
			<tr>
				<td><?=$Lang['ePost']['ContainNewspapers']?></td>
				<td>:</td>
				<td><?=$NewspaperTitleIssueHTML? $NewspaperTitleIssueHTML : '-'?></td>
			</tr>
		</table>
		<p class="spacer"></p>
	</div>
	<p class="spacer"></p>
	<div class="edit_bottom">
		<p class="spacer"></p>
		<br>
		<input name="submit_btn" type="button" class="formbutton" onclick="go_submit()" value="<?=$FolderID? $Lang['ePost']['Button']['Update']:$Lang['ePost']['Button']['Submit']?>" />
		<input name="cancel_btn" type="button" class="formsubbutton" onclick="go_cancel()" value="<?=$Lang['ePost']['Button']['Cancel']?>" />
	</div>
	<input type="hidden" id="FolderID" name="FolderID" value="<?=$FolderID?>"/>
</form>
</div> </div> </div>
<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>