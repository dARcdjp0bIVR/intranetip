<?php 
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>
<body id="edit_pop" class="edit_pop_page_select">
	<script>
		function change_courseware(courseware){
			var NewspaperID = $('#NewspaperID').val();
			var PageID		= $('#PageID').val();
			var Position	= $('#Position').val();
			
			$.post(
					'ajax_gen_filter.php',
					{
						'NewspaperID'		 : NewspaperID,
						'PageID'			 : PageID,
						'Position'			 : Position,
						'courseware'		 : courseware,
						'FilterType[]'		 : [(courseware==1? 'article_type_filter':'article_requestID_filter'), 'article_sub_type_filter'],
						'otherAttr_ary[]'	 : [(courseware==1? 'onchange="change_article_type(this.value)"':''), ''],
						'Separator'		   	 : '|=|',
						'IsPopup'			 : 1
					},
					function(data){
						if(data!='-1'){
							$('#article_type_filter').html(data.split('|=|')[0]);
							$('#article_sub_type_filter').html(data.split('|=|')[1]);
						}
					}
			);
		}
		
		function change_article_type(article_type){
			var NewspaperID = $('#NewspaperID').val();
			var PageID		= $('#PageID').val();
			var Position	= $('#Position').val();
			
			var courseware  = $('#courseware').val();
			
			$.post(
					'ajax_gen_filter.php',
					{
						'NewspaperID'		 : NewspaperID,
						'PageID'			 : PageID,
						'Position'			 : Position,
						'courseware'		 : courseware,
						'article_type'		 : article_type,
						'FilterType[]'		 : ['article_sub_type_filter'],
						'Separator'		   	 : '|=|',
						'IsPopup'			 : 1
					},
					function(data){
						if(data!='-1'){
							$('#article_sub_type_filter').html(data.split('|=|')[0]);
						}
					}
			);
		}
		
		function select_article(){
			var NewspaperID = $('#NewspaperID').val();
			var PageID 		= $('#PageID').val();
			var Position 	= $('#Position').val();
			
			var courseware 		 	= $('#courseware').length? $('#courseware').val() : '';
			var article_requestID	= $('#article_requestID').length? $('#article_requestID').val() : '';
			var article_type 	 	= $('#article_type').length? $('#article_type').val() : '';
			var article_sub_type 	= $('#article_sub_type').length? $('#article_sub_type').val() : '';
			var selected_article_id = $('#ArticleShelfID').val();
			
			var url		  = '../article_shelf/index.php?IsPopup=1&NewspaperID=' + NewspaperID + '&PageID=' + PageID + '&Position=' + Position + '&courseware=' + courseware + (courseware==1? '&article_type='+article_type+'&article_sub_type='+article_sub_type:'&article_requestID='+article_requestID) + '&selected_article_id=' + selected_article_id;
			var intWidth  = screen.width;
			var intHeight = screen.height-100;
			window.open(url,"ePost","width="+intWidth+",height="+intHeight+",scrollbars,resizable");
		}
		
		function change_article_theme_type(article_theme_type){
			$.post(
					'ajax_gen_filter.php',
					{
						'Type'				 : article_theme_type,
						'FilterType[]'		 : ['article_theme_theme_filter', 'article_theme_color_filter'],
						'otherAttr_ary[]'	 : ['onchange="change_article_theme_theme(this.value)"', 'onchange="preview_article_theme()"'],
						'Separator'		   	 : '|=|'
					},
					function(data){
						if(data!='-1'){
							$('#article_theme_theme_filter').html(data.split('|=|')[0]);
							$('#article_theme_color_filter').html(data.split('|=|')[1]);
						}
						preview_article_theme();
					}
			);
		}
		
		function change_article_theme_theme(article_theme_theme){
			var article_theme_type = $('#Type').val();
			
			$.post(
					'ajax_gen_filter.php',
					{
						'Type'				 : article_theme_type,
						'Theme'				 : article_theme_theme,
						'FilterType[]'		 : ['article_theme_color_filter'],
						'otherAttr_ary[]'	 : ['onchange="preview_article_theme()"'],
						'Separator'		   	 : '|=|'
					},
					function(data){
						if(data!='-1'){
							$('#article_theme_color_filter').html(data.split('|=|')[0]);
						}
						preview_article_theme();
					}
			);
		}
		
		function preview_article_theme(){
			var article_theme_type  = $('#Type').val();
			var article_theme_theme = $('#Theme').val(); 
			var article_theme_color = $('#Color').val();
			
			article_theme_type  = article_theme_type?  '_'+article_theme_type  : '';
			article_theme_theme = article_theme_theme? '_'+article_theme_theme : '';
			article_theme_color = article_theme_color? '_'+article_theme_color : '';
			  
			var classname = 'log_header' + article_theme_type + article_theme_theme + article_theme_color;
			
			$('#article_theme_preview').removeClass();
			$('#article_theme_preview').addClass('height_short');
			$('#article_theme_preview').addClass('log_header' + article_theme_type + '_left');
			$('#article_theme_preview').addClass(classname);
		}
		
		function go_submit(){
			<?php if($ArticleShelfID){ ?>
				var obj = document.form1;
				
				if(!writer_checkform(obj)) return false;
				
				obj.action = "issue_page_select_article_update.php";
				obj.submit();
			<?php } else {?>
				alert('<?=$Lang['ePost']['Warning']['SelectArticle']?>');
			<?php } ?>
		}		
		function ToggleDisplay(obj){
			var is_checked = obj.checked;
			var is_attachment = obj.id.indexOf("DisplayAttachment")!=-1?true:false;	
			if(is_checked){
				$("#Td_"+obj.id+"_Show").removeClass().addClass('news_show');
				$("#Td_"+obj.id+"_Content").removeClass();
				if(is_attachment){
					$("#Div_"+obj.id+"_Detail").show();
				}
			}else{
				$("#Td_"+obj.id+"_Show").removeClass().addClass('news_hide');
				$("#Td_"+obj.id+"_Content").removeClass().addClass('news_hide_content');	
				if(is_attachment){
					$("#Div_"+obj.id+"_Detail").hide();
				}				
			}

		}
		function removeArticleAttachment(id){
			$('#attachment'+id).hide();
			if($("#fileupload"+id).val()!=''){
				$("#fileupload"+id).val('');
			}else{
				$('#fileupload'+id).show();
			}
			$('#AttachmentID_'+id).val('NEW');
			$("input.input_attachment_"+id+":text").val('');
			$("input.input_attachment_"+id+":radio").attr("checked","");
			
			CheckAllowShowAttachment(id);
		}	
			
		function CheckAllowShowAttachment(cur_id){
			if(!check_select_file_type(cur_id)) return false;
			var obj = document.getElementById("DisplayAttachment"+cur_id);
			if($("#fileupload"+cur_id).val()!=''){
				$("#DisplayAttachment"+cur_id).attr("disabled","");
				$("#div_tool_delete_"+cur_id).show();
				obj.checked = true;
				ToggleDisplay(obj);
			}else{
				obj.checked = false;
				$("#div_tool_delete_"+cur_id).hide();
				ToggleDisplay(obj);
				$("#DisplayAttachment"+cur_id).attr("disabled","disabled");
			}
		}	
		function updateContentFontStyle(){
			var fontstyle = $("#FontStyle option:selected").text();
			var fontsize = $("#FontSize option:selected").text();
			var lineheight = $("#LineHeight option:selected").text();
			$("#Content").css({"width":"100%","font-family":fontstyle,"font-size":fontsize,"line-height":lineheight});
		}
		function go_cancel(){
			parent.window.tb_remove();
		}
	</script>

	<div class="pop_container">
		<form name="form1" method="post" enctype="multipart/form-data">
		<div class="marking_board">
			<span class="sectiontitle"><?=$Lang['ePost']['SelectArticle']?></span>
			<p class="spacer"></p>
				<table class="form_table form_table_article">
					
					<?php if(!$ArticleShelfID){ ?>
						<tr>
							<td><?=$Lang['ePost']['SourceType']?></td>
							<td>:</td>
							<td style="width:auto;">
								<span id="courseware_filter"><?=$ePostUI->gen_courseware_drop_down_list('courseware', 'courseware', $courseware, 'onchange="change_courseware(this.value)"')?></span>
								<span id="article_type_filter">
								<?php if($courseware==1){
										echo $ePostUI->gen_article_type_drop_down_list($courseware, 'article_type', 'article_type', $article_type, $article_cfg['restriction'], 'onchange="change_article_type(this.value)"');
									  } else if($courseware==2){
									  	echo $ePostUI->gen_article_request_with_articles_drop_down_list($courseware, 'article_requestID', 'article_requestID', $article_requestID, '');
										//echo $ePostUI->gen_article_request_drop_down_list($courseware, 'article_requestID', 'article_requestID', $article_requestID, '');
									  }
								?>
								</span>
								<span id="article_sub_type_filter"><?=$ePostUI->gen_article_sub_type_drop_down_list($courseware, $article_type, 'article_sub_type', 'article_sub_type', $article_sub_type)?></span>
								<input type="button" name="select_article_btn"  class="formsubbutton" onclick="select_article()"  value="<?=$Lang['ePost']['Select']?>"/>
							</td>
						</tr>
						<tr>
							<td><?=$Lang['ePost']['Author']?></td>
							<td>:</td>
							<td style="width:auto;">--</td>
						</tr>
						<tr>
							<td><?=$Lang['ePost']['Title']?></td>
							<td>:</td>
							<td style="width:auto;">--</td>
						</tr>
						<tr>
							<td><?=$Lang['ePost']['Content']?></td>
							<td>:</td>
							<td style="width:auto;">--</td>
						</tr>
						<tr>
							<td><?=$Lang['ePost']['Theme']?></td>
							<td>:</td>
							<td style="width:auto;">--</td>
						</tr>
					<?php } else { ?>
						<tr>
							<td><?=$Lang['ePost']['SourceType']?></td>
							<td>:</td>
							<td colspan="2">
								<?=$Lang['ePost']['From']?>
								<?=$cfg_ePost['BigNewspaper']['ArticleType'][($courseware==1? "courseware":"non_courseware")]['name']?>
								<?=$article_requestID? " > ".$article_request_topic:""?>
								<?=$article_type? " > ".$cfg_ePost['BigNewspaper']['ArticleType'][($courseware==1? "courseware":"non_courseware")]['types'][$article_type]['name']:""?>
								<?=$article_type && $article_sub_type? " > ".$cfg_ePost['BigNewspaper']['ArticleType'][($courseware==1? "courseware":"non_courseware")]['types'][$article_type]['sub_types'][$article_sub_type]['name']:""?>
								[<a href="javascript:select_article()"><?=$Lang['ePost']['SelectAnother']?><input type="hidden" name="courseware" id="courseware" value="<?=$courseware?>"></a>]
							</td>
						</tr>
						<tr>
							<td><?=$Lang['ePost']['Author']?></td>
							<td>:</td>
							<?$isDisplayAuthor = ($DisplayAuthor==$cfg_ePost['BigNewspaper']['Article_DisplayAuthor']['Yes']);?>
							<td id="Td_DisplayAuthor_Content" class="<?=$isDisplayAuthor? '':'news_hide_content'?>">
								<?=$ePostWriter->UserName.($ePostWriter->ClassName? " (".$ePostWriter->ClassName.($ePostWriter->ClassNumber? "-".$ePostWriter->ClassNumber:"").")":"")?>
							</td>
							<td id="Td_DisplayAuthor_Show" class="<?=$isDisplayAuthor? 'news_show':'news_hide'?>">
							 	<label for="DisplayAuthor">
	              					<input type="checkbox" id="DisplayAuthor" name="DisplayAuthor" value="<?=$cfg_ePost['BigNewspaper']['Article_DisplayAuthor']['Yes']?>" <?=$isDisplayAuthor? 'checked':''?> onclick="ToggleDisplay(this);"/><?=$Lang['ePost']['Show']?> 
	              				</label>
	              			</td>
						</tr>
						<?php include($writer_template); ?>
						<tr>
							<td><?=$Lang['ePost']['Theme']?></td>
							<td>:</td>
							<td colspan="2">
								<span id="article_theme_type_filter" ><?=$ePostUI->gen_article_theme_type_filter('Type', 'Type', $Type, 'onchange="change_article_theme_type(this.value)"')?></span>
								<span id="article_theme_theme_filter"><?=$ePostUI->gen_article_theme_theme_filter($Type, 'Theme', 'Theme', $Theme, 'onchange="change_article_theme_theme(this.value)"')?></span>
								<span id="article_theme_color_filter"><?=$ePostUI->gen_article_theme_color_filter($Type, $Theme, 'Color', 'Color', $Color, 'onchange="preview_article_theme()"')?></span>
								<div class="newspaper_page select_theme">
									<div id="article_theme_preview" class="log_header<?=($Type? '_'.$Type:'').($Theme? '_'.$Theme:'').($Color? '_'.$Color:'')?> <?="log_header_".$Type."_left"?> height_short">
										<div class="log_header">
											<div class="log_header_top">
												<div class="log_header_top_right">
													<div class="log_header_top_bg"></div>
												</div>
											</div>
											<div class="log_header_body">
												<div class="log_header_body_right">
													<div class="log_header_body_bg">
														<h1><?=$Lang['ePost']['Title']?></h1>
													</div>
												</div>
											</div>
											<div class="log_header_bottom">
												<div class="log_header_bottom_right">
													<div class="log_header_bottom_bg"></div>
												</div>
											</div>
										</div>
										<div class="log_content_body"></div>
									</div>
								</div>
							</td>
						</tr>
					<?php } ?>
					
				</table>
	        	<p class="spacer"></p>
	        	<div class="edit_bottom">
					<input name="submit2" class="formbutton" onclick="go_submit()" value="<?=$Lang['ePost']['Button']['Submit_1']?>" type="button">
					<input name="submit2" class="formsubbutton" value="<?=$Lang['ePost']['Button']['Cancel']?>" type="button" onclick="go_cancel()"/>
	          	</div>
	    </div>    		
	  		<input type="hidden" id="NewspaperID" name="NewspaperID" value="<?=$ePostPage->NewspaperID?>"/>
	  		<input type="hidden" id="PageID" name="PageID" value="<?=$ePostPage->PageID?>"/>
	  		<input type="hidden" id="Position" name="Position" value="<?=$Position?>"/>
	  		<input type="hidden" id="ArticleID" name="ArticleID" value="<?=is_object($ePostArticle)? $ePostArticle->ArticleID : ''?>"/>
	  		<input type="hidden" id="ArticleShelfID" name="ArticleShelfID" value="<?=$ArticleShelfID?>"/>
	  	</form>
  </div>  
</body>

<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>