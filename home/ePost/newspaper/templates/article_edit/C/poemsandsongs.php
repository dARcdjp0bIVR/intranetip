<?php
# using :Paul
/* 
 * Modification Log:
 * 2019-06-20 (Paul) [ip.2.5.10.6.1]
 * 	- fix the security fix trimming the "NEW" in AttachmentID into 0 causing failure in uploading attachment by changing AttachmentID to AttachmentIDs
 * 
 */
# Process Attachment
$attachmentAry = array();
$attachmentIdAry = array_keys($ePostAttachmentAry);

for($i=1;$i<=$maxUploadCnt;$i++){
	$_attachmentId = $attachmentIdAry[$i-1];
	$_attachmentPathAry = !empty($_attachmentId)?$ePostAttachmentAry[$_attachmentId]:array();
	$_attachment = $_attachmentPathAry['attachment'];
	if(!empty($_attachment)){ //have existing attachment
		$ext = strtoupper(substr($_attachment, '-4'));	
		if(in_array($ext, array('.JPG','.GIF','.JPE','.PNG','.BMP'))){
			if($isFromArticleShelf){
				$_writingId = $_attachmentPathAry['writingId'];
				$attachment_file_path = $ePostArticleShelf->format_attachment_path($_attachment,false,!empty($_writingId));
				$attachment_http_path = $ePostArticleShelf->format_attachment_path($_attachment,true,!empty($_writingId));
			}else{
				$attachment_file_path = $ePostArticle->format_attachment_path($_attachment,false);
				$attachment_http_path = $ePostArticle->format_attachment_path($_attachment,true);
			}
			
			if(file_exists($attachment_file_path)){ 
				$_caption = $_attachmentPathAry['caption'];
				$_alignment = $_attachmentPathAry['alignment'];
				$_size = $_attachmentPathAry['size'];
				$_status = isset($_attachmentPathAry['status'])?$_attachmentPathAry['status']:$cfg_ePost['BigNewspaper']['Article_DisplayAttachment']['Yes'];
				
				$attachmentHTML = "<div class=\"tool_attacment_file\"><a href=\"".$attachment_http_path."\" class=\"att_image\" target=\"_BLANK\">";
				$attachmentHTML .= "<span id=\"attachment".$i."\">".$_attachment."</span></a>";
				$attachmentHTML .= "<input type=\"file\" class=\"input_attachment_".$i."\" id=\"fileupload".$i."\" name=\"fileupload".$i."\" onchange=\"CheckAllowShowAttachment(".$i.");\" style=\"display:none;\" />";
				$attachmentHTML .= "</div>";
				$attachmentHTML .= "<div class=\"table_row_tool\" id=\"div_tool_delete_".$i."\"><a href=\"javascript:void(0);\" class=\"tool_delete\" onclick=\"removeArticleAttachment(".$i.");\"></a></div>";
				$attachmentHTML .= "<p class=\"spacer\"></p>";
	           	$attachmentHTML .= "<div id=\"Div_DisplayAttachment".$i."_Detail\" class=\"att_detail\" ".($_status?"":"style=\"display:none;\"").">";
	            $attachmentHTML .= "<ul>";
	           	$attachmentHTML .= "<li>";
	            $attachmentHTML .= "<span>".$Lang['ePost']['Width']." </span><em>:</em>";
	            foreach($cfg_ePost['BigNewspaper']['AttachmentLayout']['SizeToCss'] as $__cssSize => $__css){
	            	$isChecked = $__cssSize==$_size?'checked':'';
	            	$attachmentHTML .= "<input type=\"radio\" class=\"input_attachment_".$i."\" name=\"Attachment_Width".$i."\" id=\"Attachment_Width".$i."_".$__cssSize."\" value=\"".$__cssSize."\" ".$isChecked."/>";
	            	$attachmentHTML .= "<label for=\"Attachment_Width".$i."_".$__cssSize."\">".$__cssSize."%</label>";
	            }
	            $attachmentHTML .= "</li>";
	            $attachmentHTML .= "<li><span>".$Lang['ePost']['Alignment']."</span><em>:</em>";
	            $attachmentHTML .= "<input type=\"radio\" class=\"input_attachment_".$i."\" id=\"Alignment".$i."_L\" name=\"Alignment".$i."\" value=\"".$cfg_ePost['BigNewspaper']['Alignment']['Left']."\" ".($_alignment==$cfg_ePost['BigNewspaper']['Alignment']['Left']? "checked":"")." />";
				$attachmentHTML .= "<label for=\"Alignment".$i."_L\">".$Lang['ePost']['Left']."</label>";
				$attachmentHTML .= "<input type=\"radio\" class=\"input_attachment_".$i."\" id=\"Alignment".$i."_R\" name=\"Alignment".$i."\" value=\"".$cfg_ePost['BigNewspaper']['Alignment']['Right']."\" ".($_alignment==$cfg_ePost['BigNewspaper']['Alignment']['Right']? "checked":"")."/>";
				$attachmentHTML .= "<label for=\"Alignment".$i."_R\">".$Lang['ePost']['Right']."</label>";
				$attachmentHTML .= "</li>";
	            $attachmentHTML .= "<li><span>".$Lang['ePost']['Caption']."</span><em>:</em>";
	            $attachmentHTML .= "<input type=\"text\" class=\"textbox input_attachment_".$i."\" name=\"Caption".$i."\" id=\"Caption".$i."\" value=\"".$_caption."\"/>";
	            $attachmentHTML .= "</li>";
	            $attachmentHTML .= "</ul>";
	            $attachmentHTML .= "<input type=\"hidden\" class=\"is_attachment input_attachment_".$i."\" name=\"AttachmentIDs[".$i."]\" id=\"AttachmentID_".$i."\" value=\"".$_attachmentId."\"/>";
	            $attachmentHTML .= "</div>";
				$attachmentAry[$i] = array(
								"attachment_name"=>$_attachment,
								"attachment"=>$attachmentHTML,
								"caption"=>$_caption,
								"is_show"=>$_status,
								"checkbox"=>""
							);
			}
		}
	}else{
			$attachmentHTML = "<div class=\"tool_attacment_file\">";
			$attachmentHTML .= "<input type=\"file\" class=\"input_attachment_".$i."\" id=\"fileupload".$i."\" name=\"fileupload".$i."\" onchange=\"CheckAllowShowAttachment(".$i.");\" />";
			$attachmentHTML .= "</div>";

			$attachmentHTML .= "<div class=\"table_row_tool\" id=\"div_tool_delete_".$i."\" style=\"display:none;\"><a href=\"javascript:void(0);\" class=\"tool_delete\" onclick=\"removeArticleAttachment(".$i.");\";></a></div>";
			$attachmentHTML .= "<p class=\"spacer\"></p>";
           	$attachmentHTML .= "<div id=\"Div_DisplayAttachment".$i."_Detail\" class=\"att_detail\" style=\"display:none;\">";
            $attachmentHTML .= "<ul>";
           	$attachmentHTML .= "<li>";
            $attachmentHTML .= "<span>".$Lang['ePost']['Width']." </span><em>:</em>";
            foreach($cfg_ePost['BigNewspaper']['AttachmentLayout']['SizeToCss'] as $__cssSize => $__css){
            	$attachmentHTML .= "<input type=\"radio\" class=\"input_attachment_".$i."\" name=\"Attachment_Width".$i."\" id=\"Attachment_Width".$i."_".$__cssSize."\" value=\"".$__cssSize."\" />";
            	$attachmentHTML .= "<label for=\"Attachment_Width".$i."_".$__cssSize."\">".$__cssSize."%</label>";
            }
            $attachmentHTML .= "</li>";
            $attachmentHTML .= "<li><span>".$Lang['ePost']['Alignment']."</span><em>:</em>";
            $attachmentHTML .= "<input type=\"radio\" class=\"input_attachment_".$i."\" id=\"Alignment".$i."_L\" name=\"Alignment".$i."\" value=\"".$cfg_ePost['BigNewspaper']['Alignment']['Left']."\"  />";
			$attachmentHTML .= "<label for=\"Alignment".$i."_L\">".$Lang['ePost']['Left']."</label>";
			$attachmentHTML .= "<input type=\"radio\" class=\"input_attachment_".$i."\" id=\"Alignment".$i."_R\" name=\"Alignment".$i."\" value=\"".$cfg_ePost['BigNewspaper']['Alignment']['Right']."\" />";
			$attachmentHTML .= "<label for=\"Alignment".$i."_R\">".$Lang['ePost']['Right']."</label>";
			$attachmentHTML .= "</li>";
            $attachmentHTML .= "<li><span>".$Lang['ePost']['Caption']."</span><em>:</em>";
            $attachmentHTML .= "<input type=\"text\" class=\"input_attachment_".$i." textbox\" name=\"Caption".$i."\" id=\"Caption".$i."\" value=\"\"/>";
            $attachmentHTML .= "</li>";
            $attachmentHTML .= "</ul>";
            $attachmentHTML .= "<input type=\"hidden\" class=\"is_attachment input_attachment_".$i."\" name=\"AttachmentIDs[".$i."]\" id=\"AttachmentID_".$i."\" value=\"NEW\"/>";
	        $attachmentHTML .= "</div>";            
			$attachmentAry[$i] = array(
							"attachment_name"=>"",
							"attachment"=>$attachmentHTML,
							"caption"=>"",
							"is_show"=>0,
							"checkbox"=>"disabled"
						);			
		}

}
# Get Attachment Alignment
$Alignment = is_object($ePostArticle)? $ePostArticle->Article_Alignment : $cfg_ePost['BigNewspaper']['Alignment']['Left'];

# Get Attachment Size
$Size = is_object($ePostArticle) && $ePostArticle->Article_Size? $ePostArticle->Article_Size : 'Large';

# Set $para_ary for gen_image_video_size_option()
$para_ary = array();
$para_ary['Type'] 		 = 'IMG';
$para_ary['ArticleCfg']  = $article_cfg;
$para_ary['Name']		 = 'Size';
$para_ary['DefaultSize'] = $Size;
?>
<tr style="display:none">
	<td colspan="100%">
		<script>		
			function writer_checkform(obj){
				var Title = obj.Title.value;
				var Content = obj.Content.value;
				
				if(Title==''){
					obj.Title.focus();
					alert('<?=$Lang['ePost']['Warning']['InputTitle']?>');
					return false;
				}
				
				if(Content==''){
					obj.Content.focus();
					alert('<?=$Lang['ePost']['Warning']['InputContent']?>');
					return false;
				}
				if(!check_upload_file_valid()) return false;
				var filename = '';
				var valid = 1;
				$('.is_attachment').each(function(){
					if(valid){
						var this_id = $(this).attr("id").split("_")[1];
						var isDisplay = $("input[name=DisplayAttachment"+this_id+"]").is(':checked');
						if(valid&&isDisplay){
							if(!$("input[name=Attachment_Width"+this_id+"]").is(':checked')){
								alert('<?=$Lang['ePost']['Warning']['SelectWidth']?>');
								valid = 0;
							}else if(!$("input[name=Alignment"+this_id+"]").is(':checked')){
								alert('<?=$Lang['ePost']['Warning']['SelectAlignment']?>');
								valid = 0;
							}
						}
					}						
				});
				if(!valid) return false;
				return true;
			}	
			function check_select_file_type(id){
				var fileupload = $('#fileupload'+id).val();
				if(fileupload){
					fileupload = Get_File_Name(fileupload);
					
					var ext = fileupload.substr((fileupload.length-4)).toUpperCase();
					
					if(ext!='.JPG' && ext!='.GIF' && ext!='.JPE' && ext!='.PNG' && ext!='.BMP'){
						alert('<?=$Lang['ePost']['Warning']['UploadImageFile']?>');
						$('#fileupload'+id).val('');
						return false;
					}
				}
				return true;
			}			
			function check_upload_file_valid(){
				var fileupload  = '';
			<?php for($i=0;$i<$maxUploadCnt;$i++){?>
				fileupload = $('#fileupload<?=$i?>').val();
				if(fileupload){
					fileupload = Get_File_Name(fileupload);
					
					var ext = fileupload.substr((fileupload.length-4)).toUpperCase();
					
					if(ext!='.JPG' && ext!='.GIF' && ext!='.JPE' && ext!='.PNG' && ext!='.BMP'){
						alert('<?=$Lang['ePost']['Warning']['UploadImageFile']?>');
						$('#fileupload<?=$i?>').val('');
						return false;
					}
				}
			<?php }?>
				return true;
			}				
		</script>
		<input type="hidden" name="AttachmentCnt" id="AttachmentCnt" value="<?=$i?>">
	<?/*	<input type="hidden" name="AddAttachmentCnt" id="AddAttachmentCnt" value="<?=$i?>">*/?>
	</td>
</tr>
<?$isDisplayTitle = ($DisplayTitle==$cfg_ePost['BigNewspaper']['Article_DisplayTitle']['Yes']);?>
<tr>
	<td><?=$Lang['ePost']['Title']?></td>
	<td>:</td>
	<td id="Td_DisplayTitle_Content" class="<?=$isDisplayTitle? '':'news_hide_content'?>">
		<input type="text" id="Title" name="Title" style="width:100%" value="<?=!empty($ePostArticle)?$ePostArticle->Article_Title:$ePostArticleShelf->Title?>"/>
	</td>
	<td id="Td_DisplayTitle_Show" class="<?=$isDisplayTitle? 'news_show':'news_hide'?>">
	 	<label for="DisplayTitle">
			<input type="checkbox" id="DisplayTitle" name="DisplayTitle" value="<?=$cfg_ePost['BigNewspaper']['Article_DisplayTitle']['Yes']?>" <?=$isDisplayTitle? 'checked':''?> onclick="ToggleDisplay(this);" /><?=$Lang['ePost']['Show']?> 
		</label>
	</td>	
</tr>
<?$isDisplayContent = ($DisplayContent==$cfg_ePost['BigNewspaper']['Article_DisplayContent']['Yes']);?>
<tr>
	<td><?=$Lang['ePost']['Content']?></td>
	<td>:</td>
	<td id="Td_DisplayContent_Content" class="<?=$isDisplayContent? '':'news_hide_content'?>">
		<textarea type="text" id="Content" name="Content" style="width:100%;<?=$fontcss?>" rows="8"><?=intranet_undo_htmlspecialchars(!empty($ePostArticle)?$ePostArticle->Article_Content:$ePostArticleShelf->Content)?></textarea>
		<?=$fontSettingHtml?>
	</td>
	<td id="Td_DisplayContent_Show" class="<?=$isDisplayContent? 'news_show':'news_hide'?>">
	 	<label for="DisplayContent">
			<input type="checkbox" id="DisplayContent" name="DisplayContent" value="<?=$cfg_ePost['BigNewspaper']['Article_DisplayContent']['Yes']?>" <?=$isDisplayContent? 'checked':''?>  onclick="ToggleDisplay(this);"/><?=$Lang['ePost']['Show']?> 
		</label>
	</td>	
</tr>
<?php 
	$_idx = 0;
	
	foreach($attachmentAry as $_idx => $_attachmentPathAry){
		$_isShow = $_attachmentPathAry['is_show'];
		$_attachment = $_attachmentPathAry['attachment'];
		$_checkbox = $_attachmentPathAry['checkbox'];		
?>
	<tr id="tr_attachment_<?=$_idx?>">
		<td class="td_attachment_title_1"><?=$Lang['ePost']['Image'.$_idx]?></td>
		<td class="td_attachment_title_2">:</td>
		<td id="Td_DisplayAttachment<?=$_idx?>_Content" class="<?=$_isShow? '':'news_hide_content'?>"><?=$_attachment?></td>
		<td id="Td_DisplayAttachment<?=$_idx?>_Show" class="<?=$_isShow? 'news_show':'news_hide'?>"><label for="DisplayAttachment<?=$_idx?>">
			<input type="checkbox" id="DisplayAttachment<?=$_idx?>" name="DisplayAttachment<?=$_idx?>" value="1" <?=$_isShow? 'checked':''?>  onclick="ToggleDisplay(this);" <?=$_checkbox?>/><?=$Lang['ePost']['Show']?> 
		</label></td>
	</tr>
<?php 
	} 
  ?>

 
 