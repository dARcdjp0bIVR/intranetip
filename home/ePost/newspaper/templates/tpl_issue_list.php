<?php
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

<script>
	function go_filter(){
		document.filter_form.submit();
	}
	
	function go_edit(NewspaperID){
		var obj = document.form1;
		
		obj.action = 'issue_page_new.php';
		obj.NewspaperID.value = NewspaperID;
		obj.submit();
	}
	
	function go_delete(NewspaperID){
		var obj = document.form1;
		var NewspaperName = document.getElementById('Newspaper_title_' + NewspaperID).innerHTML;
		
		if(confirm('<?=$Lang['ePost']['Confirm']['RemoveNewspaper'][0]?>'+ NewspaperName + '<?=$Lang['ePost']['Confirm']['RemoveNewspaper'][1]?>')){
			obj.action = 'issue_delete.php';
			obj.NewspaperID.value = NewspaperID;
			obj.submit();
		}
	}
	
	function change_publish_status(NewspaperID, DefaultPublishStatus){
		var NewspaperePostName = document.getElementById('Newspaper_epost_title_' + NewspaperID).innerHTML;
		var NewspaperName 	   = document.getElementById('Newspaper_title_' + NewspaperID).innerHTML;
		
		if(confirm('<?=$Lang['ePost']['Confirm']['ChangePublishStatus'][0]?>'+NewspaperePostName+'<?=$Lang['ePost']['Confirm']['ChangePublishStatus'][1]?>'+NewspaperName+'<?=$Lang['ePost']['Confirm']['ChangePublishStatus'][2]?>'+$('#PublishStatus_'+NewspaperID+' option:selected').html()+'<?=$Lang['ePost']['Confirm']['ChangePublishStatus'][3]?>')){
			var obj = document.form1;
			
			obj.action = 'issue_publish_status_update.php';
			obj.NewspaperID.value = NewspaperID;
			obj.PublishStatus.value = $('#PublishStatus_'+NewspaperID).val();
			obj.submit();
		}
		else{
			$('#PublishStatus_'+NewspaperID).val(DefaultPublishStatus);
		}
	}
	
	function openNewspaper(NewspaperID)
	{
		var intWidth  = screen.width;
	 	var intHeight = screen.height-100;
		window.open('/home/ePost/newspaper/view_newspaper.php?NewspaperID='+NewspaperID,"ePost_Newspaper","width="+intWidth+",height="+intHeight+",scrollbars,resizable");
	}
	
	function creatNewspaper(){
		var obj = document.form1;
		
		obj.action = 'issue_new.php';
		obj.NewspaperID.value = '';
		obj.submit();
	}
</script>
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
<p class="spacer">&nbsp;</p>
<p class="spacer">&nbsp;</p>
<?=$ePostUI->gen_portal_page_nav($nav_ary)?>
<p class="spacer">&nbsp;</p>
<form name="filter_form" method="post" action="issue_list.php" style="margin:0;padding:0">
	<div class="content_top_tool">
		<div id="table_filter">
			<select name="Status" id="Status">
				<option value="<?=$cfg_ePost['BigNewspaper']['Newspaper_filter']['All']?>" <?=$Status==$cfg_ePost['BigNewspaper']['Newspaper_filter']['All']? "selected":""?>><?=$Lang['ePost']['Filter']['All']?></option>
				<option value="<?=$cfg_ePost['BigNewspaper']['Newspaper_filter']['Draft']?>" <?=$Status==$cfg_ePost['BigNewspaper']['Newspaper_filter']['Draft']? "selected":""?>><?=$Lang['ePost']['Filter']['Draft']?></option>
				<option value="<?=$cfg_ePost['BigNewspaper']['Newspaper_filter']['WaitApproval']?>" <?=$Status==$cfg_ePost['BigNewspaper']['Newspaper_filter']['WaitApproval']? "selected":""?>><?=$Lang['ePost']['Filter']['WaitingApproval']?></option>
				<option value="<?=$cfg_ePost['BigNewspaper']['Newspaper_filter']['Published']?>" <?=$Status==$cfg_ePost['BigNewspaper']['Newspaper_filter']['Published']? "selected":""?>><?=$Lang['ePost']['Filter']['Published']?></option>
			</select>
			<input name="filter_go_btn" type="button" class="formsmallbutton" value="<?=$Lang['ePost']['Filter']['Go']?>" onclick="go_filter()"/>
		</div>
		<p class="spacer"></p>
		<br>
	</div>
	<input type="hidden" id="FolderID" name="FolderID" value="<?=$FolderID?>"/>
</form>
<form name="form1" action="" method="post" style="margin:0;padding:0">
	<?php if(!$ePost->user_obj->isStudent()){ ?>
		<div class="Content_tool">
			<a href="javascript:creatNewspaper()" class="new"><?=$Lang['ePost']['CreateNewIssue']?></a>
		</div>
	<?php } ?>
	
	<div id="sys_msg"><?=$Lang['ePost']['ReturnMessage'][$returnMsg]?></div>
	<div class="table_board">
		<table class="common_table_list edit_table_list">
			<colgroup>
				<col nowrap="nowrap">
			</colgroup>
			<tr>
				<th width="30">#</th>
				<th><?=$Lang['ePost']['ePostTitle']?></th>
				<th><?=$Lang['ePost']['Issue']?></th>
				<th><?=$Lang['ePost']['NoOfPage']?></th>
				<th><?=$Lang['ePost']['IssueDate']?></th>
				<th><?=$Lang['ePost']['Viewed']?></th>
				<th width="140"><?=$Lang['ePost']['PublishStatus']?></th>
				<th><?=$Lang['ePost']['LastModified']?></th>
				<th width="100">&nbsp;</th>
			</tr>
			<?=$table_html?>
		</table>
		<p class="spacer"></p>
		<p class="spacer"></p>
		<br>
	</div>
	<em style="float:left;margin-left:5px;width:15px;height:15px;background-color:#f1f1f1;border:1px solid #ccc">&nbsp;</em>&nbsp;=&nbsp;<?=$Lang['ePost']['PublishedIssue']?>
	<input type="hidden" id="NewspaperID" name="NewspaperID" value=""/>
	<input type="hidden" id="FolderID" name="FolderID" value="<?=$FolderID?>"/>
	<input type="hidden" id="Status" name="Status" value="<?=$Status?>"/>
	<input type="hidden" id="PublishStatus" name="PublishStatus" value=""/>
</form>
</div> </div> </div>
<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>