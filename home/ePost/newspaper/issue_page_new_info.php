<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$NewspaperID = isset($NewspaperID) && $NewspaperID!=''? $NewspaperID : 0;
$PageID		 = isset($PageID) && $PageID!=''? $PageID : 0;

# Initialize Object
$ePostUI 	    = new libepost_ui();
$ePostNewspaper = new libepost_newspaper($NewspaperID);
# Check if ePost should be accessible
$ePostUI->portal_auth();

if($PageID){
	$ePostPage = new libepost_page($PageID);
}

$Type   	= $PageID? $ePostPage->Page_Type   		: 'I1';
$Layout 	= $PageID? $ePostPage->Page_Layout 		: 1;
$Theme		= $PageID? $ePostPage->Page_Theme  		: 1;
$ThemeName	= $PageID? $ePostPage->Page_ThemeName  	: "";
$Name		= $PageID? $ePostPage->Page_Name   		: '';
if(substr($Type,0,1)=='C')
	$isCoverPage = 1;
else
	$isCoverPage = 0;
# Process 'Location (Insert After)' field HTML
if(!$isCoverPage && count($ePostNewspaper->Newspaper_PageIDs)){
	$Pages_ary 	   = $ePostNewspaper->get_pages();
	$selected_flag = false;

	for($i=0;$i<count($Pages_ary);$i++){
		$PageObj = $Pages_ary[$i];
		
		if($PageObj->PageID != $PageID){
			$IsSelected = ($PageID? ($i==($ePostPage->Page_PageOrder - 1)):false) || (!$selected_flag && $i==(count($ePostNewspaper->Newspaper_PageIDs) - 1));
			$RefPage_html .= "<option value='".$PageObj->PageID."' ".($IsSelected? 'selected':'').">".$PageObj->Page_Name."</option>";
			$selected_flag = $selected_flag || $IsSelected;
		}
	}
}
$Status = ((!empty($ePostNewspaper->Newspaper_PublishStatus)))?$ePostNewspaper->Newspaper_PublishStatus:1;
$nav_ary[] = array("title"=>$Lang['ePost']['FolderList'], "link"=>"index.php");
$nav_ary[] = array("title"=>($ePostNewspaper->Folder_IsSystemPublic==$cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']? $Lang['ePost']['PublicFolder']:$ePostNewspaper->Folder_Title), "link"=>"issue_list.php?FolderID=".$ePostNewspaper->FolderID); 
$nav_ary[] = array("title"=>$ePostNewspaper->Newspaper_Title." - ".$ePostNewspaper->Newspaper_Name, "link"=>"issue_page_list.php?NewspaperID=".$ePostNewspaper->NewspaperID); 
$nav_ary[] = array("title"=>($PageID? $Lang['ePost']['EditPage']:$Lang['ePost']['NewPage']), "link"=>"");

include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_page_new_info.php');

intranet_closedb();
?>