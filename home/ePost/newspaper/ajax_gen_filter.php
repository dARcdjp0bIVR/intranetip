<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$NewspaperID  	  = isset($NewspaperID) && $NewspaperID!=''? $NewspaperID : '';
$PageID 	  	  = isset($PageID) && $PageID!=''? $PageID : '';
$Position	  	  = isset($Position) && $Position!=''? $Position : 1;

$courseware   	  = isset($courseware) && $courseware!=''? $courseware : 1;
$article_type 	  = isset($article_type) && $article_type!=''? $article_type : '';
$article_sub_type = isset($article_sub_type) && $article_sub_type!=''? $article_sub_type : '';

$FilterType 	  = isset($FilterType) && is_array($FilterType)? $FilterType : array();
$Separator		  = isset($Separator) && $Separator? $Separator : '|=|';
$otherAttr_ary 	  = isset($otherAttr_ary) && is_array($otherAttr_ary)? $otherAttr_ary : array();
$result_ary  	  = array();

$IsPopup	  	  = isset($IsPopup) && $IsPopup!=''? $IsPopup : 0;

$Type			  = isset($Type)  && $Type!=''?  $Type  : (is_array($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'])? current(array_keys($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'])) : "");
$Theme			  = isset($Theme) && $Theme!=''? $Theme : (is_array($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'])? current(array_keys($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'])) : "");
$Color			  = isset($Color) && $Color!=''? $Color : (is_array($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'][$Theme]['Color'])? current(array_keys($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'][$Theme]['Color'])) : "");

# Initialize Object
$ePostUI   = new libepost_ui();

# Check if ePost should be accessible
$ePostUI->portal_auth();

if($IsPopup){
	$ePostPage = new libepost_page($PageID);
	
	# Get Article Config
	$article_cfg_ary = $ePostPage->get_page_TypeLayout_articles_ary();
	$article_cfg = $article_cfg_ary[$Position];
}

# Generate Reqest Filter HTML
if(in_array('courseware_filter', $FilterType)){
	$ary_key = array_search('courseware_filter', $FilterType);
	$result_ary[] = $ePostUI->gen_courseware_drop_down_list('courseware', 'courseware', $courseware, stripslashes($otherAttr_ary[$ary_key]));
}

if(in_array('article_type_filter', $FilterType)){
	$ary_key = array_search('article_type_filter', $FilterType);
	$result_ary[] = $ePostUI->gen_article_type_drop_down_list($courseware, 'article_type', 'article_type', $article_type, (is_array($article_cfg['restriction'])? $article_cfg['restriction'] : array()), stripslashes($otherAttr_ary[$ary_key]));
}

if(in_array('article_requestID_filter', $FilterType)){
	$ary_key = array_search('article_requestID_filter', $FilterType);
	$result_ary[] = $ePostUI->gen_article_request_with_articles_drop_down_list($courseware, 'article_requestID', 'article_requestID', $article_requestID, stripslashes($otherAttr_ary[$ary_key]));
	//$result_ary[] = $ePostUI->gen_article_request_drop_down_list($courseware, 'article_requestID', 'article_requestID', $article_requestID, stripslashes($otherAttr_ary[$ary_key]));
}

if(in_array('article_sub_type_filter', $FilterType)){
	$ary_key = array_search('article_sub_type_filter', $FilterType);
	$result_ary[] = $ePostUI->gen_article_sub_type_drop_down_list($courseware, $article_type, 'article_sub_type', 'article_sub_type', $article_sub_type, stripslashes($otherAttr_ary[$ary_key]));
}

if(in_array('article_theme_theme_filter', $FilterType)){
	$ary_key = array_search('article_theme_theme_filter', $FilterType);
	$result_ary[] = $ePostUI->gen_article_theme_theme_filter($Type, 'Theme', 'Theme', $Theme, stripslashes($otherAttr_ary[$ary_key]));
}

if(in_array('article_theme_color_filter', $FilterType)){
	$ary_key = array_search('article_theme_color_filter', $FilterType);
	$result_ary[] = $ePostUI->gen_article_theme_color_filter($Type, $Theme, 'Color', 'Color', $Color, stripslashes($otherAttr_ary[$ary_key]));
}

echo is_array($result_ary) && count($result_ary)? implode($Separator, $result_ary) : -1;
?>