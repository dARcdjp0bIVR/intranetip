<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$PageID	  = isset($PageID) && $PageID!=''? $PageID : 0;
$Position = isset($Position) && $Position!=''? $Position : 1;

# Initialize Object
$ePostUI   = new libepost_ui();
$ePostPage = new libepost_page($PageID);
# Check if ePost should be accessible
$ePostUI->portal_auth();
$Header = 'PopUp';
$Articles_ary = $ePostPage->get_articles();
$ePostArticle = $Articles_ary[$Position];
$ePostAttachmentAry = $ePostArticle->Article_Attachment;
$attachmentCnt = count($ePostAttachmentAry);
$displayAddMoreButton = false;
$ArticleShelfID = $ArticleShelfID? $ArticleShelfID:(is_object($ePostArticle)? $ePostArticle->ArticleShelfID:'');
$FontStyle   = $FontStyle? $FontStyle : (is_object($ePostArticle)? $ePostArticle->Article_FontStyle:1); 
$FontSize    = $FontSize? $FontSize : (is_object($ePostArticle)? $ePostArticle->Article_FontSize:2); 
$LineHeight  = $LineHeight? $LineHeight : (is_object($ePostArticle)? $ePostArticle->Article_LineHeight:0); 
//$DisplayAuthor  = $DisplayAuthor? $DisplayAuthor : (is_object($ePostArticle)? $ePostArticle->Article_DisplayAuthor:$cfg_ePost['BigNewspaper']['Article_DisplayAuthor']['Yes']); 
$DisplayTitle  	= $DisplayTitle? $DisplayTitle : (is_object($ePostArticle)? $ePostArticle->Article_DisplayTitle:$cfg_ePost['BigNewspaper']['Article_DisplayTitle']['Yes']); 
$DisplayContent = $DisplayContent? $DisplayContent : (is_object($ePostArticle)? $ePostArticle->Article_DisplayContent:$cfg_ePost['BigNewspaper']['Article_DisplayContent']['Yes']); 
$Type			= $Type?  $Type  : (is_object($ePostArticle)? $ePostArticle->Article_Type  : (is_array($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'])? current(array_keys($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'])) : ""));
$Theme			= $Theme? $Theme : (is_object($ePostArticle)? $ePostArticle->Article_Theme : (is_array($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'])? current(array_keys($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'])) : ""));
$Color			= $Color? $Color : (is_object($ePostArticle)? $ePostArticle->Article_Color : (is_array($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'][$Theme]['Color'])? current(array_keys($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'][$Theme]['Color'])) : ""));

# Get Article Config
$article_cfg_ary = $ePostPage->get_page_TypeLayout_articles_ary();
$article_cfg 	 = $article_cfg_ary[$Position];

if($ArticleShelfID){
	$ePostArticleShelf = new libepost_articleshelf($ArticleShelfID);
	if($ePostArticleShelf->IsAppendix==$cfg_ePost['BigNewspaper']['ArticleShelf_IsAppendix']['No']){
		header('location:issue_page_select_article.php?PageID='.$PageID.'&Position='.$Position);
	}
}
##Font Setting HTML
$fontSettingHtml = "";
$fontSettingHtml .= "<div id=\"Div_DisplayContent_Detail\" class=\"att_detail\">";
$fontSettingHtml .= "<ul>";
$fontSettingHtml .= "<li>";
$fontSettingHtml .= "<span>".$Lang['ePost']['FontStyle']."</span><em>:</em>";
$fontSettingHtml .= $ePostUI->gen_fontsetting_drop_down_list("FontStyle",$FontStyle,"class=\"font_setting\" onchange=\"updateContentFontStyle();\"");
$fontSettingHtml .= "</li>";
$fontSettingHtml .= "<li>";
$fontSettingHtml .= "<span>".$Lang['ePost']['FontSize']."</span><em>:</em>";
$fontSettingHtml .= $ePostUI->gen_fontsetting_drop_down_list("FontSize",$FontSize,"class=\"font_setting\" onchange=\"updateContentFontStyle();\"");
$fontSettingHtml .= "</li>";
$fontSettingHtml .= "<li>";
$fontSettingHtml .= "<span>".$Lang['ePost']['LineHeight']."</span><em>:</em>";
$fontSettingHtml .= $ePostUI->gen_fontsetting_drop_down_list("LineHeight",$LineHeight,"class=\"font_setting\" onchange=\"updateContentFontStyle();\"");
$fontSettingHtml .= "</li>";
$fontSettingHtml .= "</ul>";
$fontSettingHtml .= "</div>";

$fontcss = "font-family:".$cfg_ePost['BigNewspaper']['AttachmentLayout']['FontStyleAry'][$FontStyle].";font-size:".$cfg_ePost['BigNewspaper']['AttachmentLayout']['FontSizeAry'][$FontSize].";line-height:".$cfg_ePost['BigNewspaper']['AttachmentLayout']['LineHeightAry'][$LineHeight].";";

include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_page_add_appendix.php');

intranet_closedb();
?>