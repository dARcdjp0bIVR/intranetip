<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");
intranet_opendb();

# Initialize Variable
$action   = isset($_REQUEST['action']) && $_REQUEST['action']!=''? $_REQUEST['action'] : '';
$PageID	  = isset($_REQUEST['PageID']) && $_REQUEST['PageID']!=''? $_REQUEST['PageID'] : '';
$RecordID = isset($_REQUEST['RecordID']) && $_REQUEST['RecordID']!=''? $_REQUEST['RecordID'] : '';
$Position = isset($_REQUEST['Position']) && $_REQUEST['Position']!=''? $_REQUEST['Position'] : ''; 

# Initialize Object
$ePostUI 	    = new libepost_ui();
$ePost = new libepost();
# Check if ePost should be accessible
$ePost->auth();

switch($action){
	case "ShowBoardEnjoyArticle";
		$result = 0;
		$content = '';
		# Get ReadFlag of selected record
		$ArticleObj = new libepost_article($RecordID);
		if(!empty($ArticleObj->Article_Enjoy)){
			$EnjoyAry = explode($cfg_ePost['sql_config']['ReadFlag_delimiter'], $ArticleObj->Article_Enjoy);
			if(!in_array($UserID, $EnjoyAry)){//to enjoy
				$sql = "UPDATE
							EPOST_NEWSPAPER_PAGE_ARTICLE
						SET
							Enjoy = CONCAT(Enjoy, '".$cfg_ePost['sql_config']['ReadFlag_delimiter']."$UserID')
						WHERE
							ArticleID = '".$RecordID."'";
				$result  = $ePost->db_db_query($sql);
				$enjoy_lang = 'UnEnjoy';
			}else{//to un-enjoy
				$key = array_search($UserID, $EnjoyAry);
				unset($EnjoyAry[$key]);
				$sql = "UPDATE
							EPOST_NEWSPAPER_PAGE_ARTICLE
						SET
							Enjoy = '".implode($cfg_ePost['sql_config']['ReadFlag_delimiter'],$EnjoyAry)."'
						WHERE
							ArticleID = '".$RecordID."'";
				$result  = $ePost->db_db_query($sql);
				$enjoy_lang = 'Enjoy';
			}
		}
		else{//first one to enjoy
			$sql = "UPDATE
						EPOST_NEWSPAPER_PAGE_ARTICLE
					SET
						Enjoy = '$UserID'
					WHERE
						ArticleID = '".$RecordID."'";
			$result  = $ePost->db_db_query($sql);
			$enjoy_lang = 'UnEnjoy';
		}
		$ArticleObj = new libepost_article($RecordID);
		$enjoy_list = $ePostUI->gen_page_article_enjoy_record($ArticleObj);
		$enjoy_btn =  "		<a class=\"enjoy_btn\" title=\"".$Lang['ePost']['ClickTo'.$enjoy_lang]."\" href=\"javascript:void(0);\" onclick=\"enjoy_article(".$ArticleObj->ArticleID.");\">";
		$enjoy_btn .= $Lang['ePost'][$enjoy_lang];
		$enjoy_btn .= "</a>";
		$content = $enjoy_btn."|=|".$enjoy_list;
	break;
	case 'ShowArticleComments':
		$result  = 0;
		$content = "";
		
		$ArticleObj = new libepost_article($RecordID);
		if($ArticleObj->ArticleShelfID){
			$ArticleShelfObj = new libepost_articleshelf($ArticleObj->ArticleShelfID);
			$WriterObj = new libepost_writer($ArticleShelfObj->WritingID);
			$TeacherComment  = $WriterObj->TeacherComment;
			$TeacherCommentTime = parseDateTime($ArticleShelfObj->modified);
		}
		$result = $ArticleObj->add_comment($Comment);
		$studentCommentCnt = count($ArticleObj->Article_CommentIDs);
        $allow_student_comment = $ePost->get_general_setting('DisableStudentComment')?0:1;
		//$commentCnt = $allow_student_comment?$studentCommentCnt:0;
		$commentCnt = $studentCommentCnt;
		$commentCnt += $TeacherComment?1:0;		
		if(!empty($TeacherComment)){
			$content .= "    <li class=\"comment_teacher\"><em>".$Lang['ePost']['Editor']."</em>";
	       	$content .= $TeacherComment;
	        $content .= "    	<p class=\"spacer\"></p><span class=\"date_time\">".$TeacherCommentTime."</span><p class=\"spacer\"></p>";
	        $content .= "    </li>";
		}
		$content .= $ArticleObj->gen_student_comment_list(); 
//        $content .= $allow_student_comment?$ArticleObj->gen_student_comment_list():''; 
        $content .= "    <p class=\"spacer\"></p>";
        $content = $commentCnt."|=@@@=|".$content;
	break;
	case 'DeleteArticleComment':
		$result  = 0;
		$content = "";
		$CommentObj = new libepost_article_comment($CommentID);
		if($ePost->is_super_admin || (($CommentObj->InputBy==$UserID||$CommentObj->InputByMemberType==USERTYPE_STUDENT)&&$ePost->user_obj->isTeacherStaff())){
			$result = $CommentObj->delete_comment();
		}
		$ArticleObj = new libepost_article($CommentObj->ArticleID);
		if($ArticleObj->ArticleShelfID){
			$ArticleShelfObj = new libepost_articleshelf($ArticleObj->ArticleShelfID);
			$WriterObj = new libepost_writer($ArticleShelfObj->WritingID);
			$TeacherComment  = $WriterObj->TeacherComment;
		}
		$studentCommentCnt = count($ArticleObj->Article_CommentIDs);
        $allow_student_comment = $ePost->get_general_setting('DisableStudentComment')?0:1;
		//$commentCnt = $allow_student_comment?$studentCommentCnt:0;
		$commentCnt = $studentCommentCnt;
		$TeacherComment  = $WriterObj->TeacherComment;
		$commentCnt += $TeacherComment?1:0;		
		$content = $commentCnt;
	break;	
	default:
		$result  = 0;
		$content = '';
}

intranet_closedb();

echo $result."|=|".$content;
?>