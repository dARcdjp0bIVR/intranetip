<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$FolderID = isset($FolderID) && $FolderID!=''? $FolderID : 0;

# Initialize Object
$ePostUI	  = new libepost_ui();
$Header='NEW';
$FromPage = 'editor';
# Check if ePost should be accessible
$ePostUI->portal_auth();

if($FolderID){
	$ePostFolder = new libepost_folder($FolderID);
	
	# Process the name of the newspaper that the folder contains
	$Newspaper_ary = $ePostFolder->get_newspapers();
	
	for($i=0;$i<count($Newspaper_ary);$i++){
		$NewspaperObj = $Newspaper_ary[$i];
		$NewspaperTitleIssueHTML .= $NewspaperObj->Newspaper_Title." - ".$NewspaperObj->Newspaper_Name."<br/>";
	}
}

# Initialize Navigation Array
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>($FolderID? $Lang['ePost']['EditFolder']:$Lang['ePost']['NewFolder']), "link"=>"");

include($intranet_root.'/home/ePost/newspaper/templates/tpl_folder_new.php');

intranet_closedb();
?>