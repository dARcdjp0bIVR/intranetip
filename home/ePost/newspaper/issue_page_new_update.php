<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$PageType 		   = isset($PageType) && $PageType!=''? $PageType : '';
$PageLayout 	   = isset($PageLayout) && $PageLayout!=''? $PageLayout : '';
$PageTheme 		   = isset($PageTheme) && $PageTheme!=''? $PageTheme : '';
$PageName 		   = isset($PageName) && $PageName!=''? $PageName : '';
$PageRefPage 	   = isset($PageRefPage) & $PageRefPage!=''? $PageRefPage : '';
$PageLayoutChanged = isset($PageLayoutChanged) && $PageLayoutChanged!=''? $PageLayoutChanged : 0;

# Initialize Object
$ePostNewspaper = new libepost_newspaper($NewspaperID);

# Check if ePost should be accessible
$ePostNewspaper->portal_auth();

if($PageID){
	$ePostPage = new libepost_page($PageID);
	
	$ePostPage->Page_Type 	= $PageType;
	$ePostPage->Page_Layout = $PageLayout;
	$ePostPage->Page_Theme 	= $PageTheme;
	$ePostPage->Page_Name 	= $PageName;
	$ePostPage->Page_Status = $cfg_ePost['BigNewspaper']['Page_status']['exist'];
	
	if($PageLayoutChanged){
		$Articles_ary = $ePostPage->get_articles();
		if(is_array($Articles_ary) && count($Articles_ary)){
			foreach($Articles_ary as $ArticleObj){
				if(!is_null($ArticleObj) && is_object($ArticleObj)){
					# Delete Article
					$ArticleShelfObj = new libepost_articleshelf($ArticleObj->ArticleShelfID);
					$result = $ArticleObj->delete_article();
					
					# Delete Appendix
					$ArticleShelfObj->delete_article_shelf();
				}
			}
		}
	}
}
else{
	# Prepare $Page_Data_ary for Page Object
	$PageOrder = count($ePostNewspaper->Newspaper_PageIDs);
	
	$Page_Data_ary['NewspaperID'] 	 = $NewspaperID;
	$Page_Data_ary['Page_Type']   	 = $PageType;
	$Page_Data_ary['Page_Layout'] 	 = $PageLayout;
	$Page_Data_ary['Page_Theme']  	 = $PageTheme;
	$Page_Data_ary['Page_Name']   	 = $PageName;
	$Page_Data_ary['Page_PageOrder'] = $PageOrder;
	$Page_Data_ary['Page_Status'] 	 = $cfg_ePost['BigNewspaper']['Page_status']['exist'];
	
	$Newspaper_Data_ary = $ePostNewspaper->newspaper_obj_to_array();
	$Page_Data_ary = array_merge($Newspaper_Data_ary, $Page_Data_ary);
	
	$ePostPage = new libepost_page(0, $Page_Data_ary);
}

$ePostPage->update_page_db($PageRefPage);

intranet_closedb();
?>
<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery-1.3.2.min.js"></script>
<script>
	$(document).ready(function(){
		parent.window.refreshPage(<?=$ePostPage->PageID?>);
		parent.window.tb_remove();		
	});
</script>