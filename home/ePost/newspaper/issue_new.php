<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_studenteditor.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$FolderID 	 = isset($FolderID) && $FolderID!=''? $FolderID : 0;
$NewspaperID = isset($NewspaperID) && $NewspaperID!=''? $NewspaperID : 0;
$IsPopup  = isset($IsPopup) && $IsPopup!=''? $IsPopup : 0;

# Initialize Object and Navigation Array
$ePostUI = new libepost_ui();
$ePostFolders = new libepost_folders();
$ePostStudentEditors = new libepost_studenteditors();
$Header=($IsPopup)?'PopUp':'NEW';
$FromPage = 'editor';
# Check if ePost should be accessible
$ePostUI->portal_auth();

$IsReadOnly = false;
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>$Lang['ePost']['FolderList'], "link"=>"index.php");

if($FolderID){
	$ePostFolder = new libepost_folder($FolderID);
	$nav_ary[] = array("title"=>($ePostFolder->Folder_IsSystemPublic==$cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']? $Lang['ePost']['PublicFolder']:$ePostFolder->Folder_Title), "link"=>"issue_list.php?FolderID=$FolderID");
	
	if($NewspaperID){
		$ePostNewspaper = new libepost_newspaper($NewspaperID);
		$IsReadOnly = $ePostNewspaper->Newspaper_PublishStatus!=$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft'];
		$nav_ary[] = array("title"=>($IsReadOnly? $Lang['ePost']['ViewIssueInformation']:$Lang['ePost']['EditIssue']), "link"=>"");
	}
	else{
		$nav_ary[] = array("title"=>$Lang['ePost']['NewIssue'], "link"=>"");
	}
} 
else{
	$nav_ary[] = array("title"=>$Lang['ePost']['NewIssue'], "link"=>"");
}

if($IsReadOnly){
	$folder_selection_html = ($ePostFolder->Folder_IsSystemPublic==$cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']? $Lang['ePost']['PublicFolder']:$ePostFolder->Folder_Title);
}
else{
	# Process Folder Selection HTML
	$folder_selection_html  = "<select id='NewspaperFolderID' name='NewspaperFolderID'>";
	for($i=0;$i<count($ePostFolders->Folders);$i++){
		$folder_title = ($ePostFolders->Folders[$i]->Folder_IsSystemPublic==$cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']? $Lang['ePost']['PublicFolder']:$ePostFolders->Folders[$i]->Folder_Title);
		$folder_selection_html .= "<option value='".$ePostFolders->Folders[$i]->FolderID."' ".($ePostFolders->Folders[$i]->FolderID == $FolderID? "selected":"").">$folder_title</option>";
	}
	$folder_selection_html .= "</select>";
}

include($intranet_root.'/home/ePost/newspaper/templates/tpl_issue_new.php');

intranet_closedb();
?>