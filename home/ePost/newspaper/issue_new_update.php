<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_studenteditor.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$NewspaperFolderID 	   = isset($NewspaperFolderID) && $NewspaperFolderID!=''? $NewspaperFolderID : 0;
$NewspaperTitle 	   = isset($NewspaperTitle) && $NewspaperTitle!=''? $NewspaperTitle : '';
$NewspaperName  	   = isset($NewspaperName) && $NewspaperName!=''? $NewspaperName : '';
$NewspaperIssueDate    = isset($NewspaperIssueDate) && $NewspaperIssueDate!=''? $NewspaperIssueDate : '';
$NewspaperOpenToPublic = isset($NewspaperOpenToPublic) && $NewspaperOpenToPublic!=''? $NewspaperOpenToPublic : 0;
$StudentEditorSelected = isset($StudentEditorSelected) && $StudentEditorSelected!=''? $StudentEditorSelected : array();

# Initialize Object
$ePostStudentEditors = new libepost_studenteditors();

# Check if ePost should be accessible
$ePostStudentEditors->portal_auth();

if($NewspaperID){
	$ePostNewspaper = new libepost_newspaper($NewspaperID);
	
	$ePostNewspaper->FolderID 			 	 			= $NewspaperFolderID;
	$ePostNewspaper->Newspaper_Title 	 	 			= $NewspaperTitle;
	$ePostNewspaper->Newspaper_Name 	 	 			= $NewspaperName;
	$ePostNewspaper->Newspaper_IssueDate 	 			= $NewspaperIssueDate;
	$ePostNewspaper->Newspaper_OpenToPublic	 			= $NewspaperOpenToPublic;
	$ePostNewspaper->Newspaper_Status 	 	 			= $cfg_ePost['BigNewspaper']['Newspaper_status']['exist'];
}
else{
	# Prepare $Newspaper_Data_ary for Newspaper Object
	$Newspaper_Data_ary['FolderID']    		   	   			 = $NewspaperFolderID;
	$Newspaper_Data_ary['Newspaper_Title']         			 = $NewspaperTitle;
	$Newspaper_Data_ary['Newspaper_Name']	   	   			 = $NewspaperName;
	$Newspaper_Data_ary['Newspaper_IssueDate'] 	   			 = date('Y-m-d', strtotime($NewspaperIssueDate)).' 00:00:00';
	$Newspaper_Data_ary['Newspaper_OpenToPublic']			 = $NewspaperOpenToPublic;
	$Newspaper_Data_ary['Newspaper_Skin']	   	   			 = current(array_keys($cfg_ePost['BigNewspaper']['skins']));
	$Newspaper_Data_ary['Newspaper_SchoolLogo']    			 = '';
	$Newspaper_Data_ary['Newspaper_CoverBanner']			 = '';
	$Newspaper_Data_ary['Newspaper_CoverBannerHideTitle']	 = 0;
	$Newspaper_Data_ary['Newspaper_CoverBannerHideName'] 	 = 0;
	$Newspaper_Data_ary['Newspaper_InsideBanner'] 			 = '';
	$Newspaper_Data_ary['Newspaper_InsideBannerHideTitle'] 	 = 0;
	$Newspaper_Data_ary['Newspaper_InsideBannerHideName'] 	 = 0;
	$Newspaper_Data_ary['Newspaper_InsideBannerHidePageCat'] = 0;
	$Newspaper_Data_ary['Newspaper_PublishStatus'] 			 = $cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft'];
	$Newspaper_Data_ary['Newspaper_Status']	   	   			 = $cfg_ePost['BigNewspaper']['Newspaper_status']['exist'];
	
	$ePostNewspaper = new libepost_newspaper(0, $Newspaper_Data_ary);
}

if($ePostNewspaper->update_newspaper_db($StudentEditorSelected)){
	if($NewspaperID){
		$sql = "SELECT
					DISTINCT ese.UserID
				FROM
					EPOST_STUDENT_EDITOR_MANAGING AS esem INNER JOIN
					EPOST_STUDENT_EDITOR AS ese ON esem.EditorID = ese.EditorID
				WHERE
					ese.Status = ".$cfg_ePost['BigNewspaper']['StudentEditor_status']['exist']." AND
					esem.ManageItemID = $NewspaperID AND
					esem.ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Newspaper']."' AND
					esem.Status = ".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist'];
		$CurrentStudentEditor_ary = $ePostNewspaper->returnVector($sql);
		
		$DeleteStudentEdiotr_ary = array_diff($CurrentStudentEditor_ary, $StudentEditorSelected);
		$InsertStudentEditor_ary = array_diff($StudentEditorSelected, $CurrentStudentEditor_ary);
		
		if((is_array($DeleteStudentEdiotr_ary) && count($DeleteStudentEdiotr_ary)>0) || (is_array($InsertStudentEditor_ary) && count($InsertStudentEditor_ary)>0)){
			for($i=0;$i<count($ePostStudentEditors->StudentEditors);$i++){
				$EditorObj = $ePostStudentEditors->StudentEditors[$i];
				if(in_array($EditorObj->UserID, $DeleteStudentEdiotr_ary)){
					$sql = "UPDATE
								EPOST_STUDENT_EDITOR_MANAGING
							SET
								Status = ".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['deleted'].",
								ModifiedBy = $UserID,
								ModifiedDate = NOW()
							WHERE
								EditorID = ".$EditorObj->EditorID." AND 
								ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Newspaper']."' AND
								ManageItemID = $NewspaperID";
					$result = $ePostNewspaper->db_db_query($sql);
				}
				else if(in_array($EditorObj->UserID, $InsertStudentEditor_ary)){
					$sql = "INSERT INTO
								EPOST_STUDENT_EDITOR_MANAGING
								(
									EditorID, ManageType, ManageItemID, Status, InputDate, ModifiedBy, ModifiedDate
								)
							VALUES
								(
									".$EditorObj->EditorID.", '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Newspaper']."', ".$NewspaperID.", ".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist'].", NOW(), $UserID, NOW()
								)";
					$result = $ePostNewspaper->db_db_query($sql);
				}
			}
		}
		
		$PageID = '';
	}
	else{
		$NewspaperID = $ePostNewspaper->NewspaperID;
		
		#Handle Student Editor
		if(is_array($StudentEditorSelected) && count($StudentEditorSelected)>0){
			for($i=0;$i<count($ePostStudentEditors->StudentEditors);$i++){
				$EditorObj = $ePostStudentEditors->StudentEditors[$i];
				if(in_array($EditorObj->UserID, $StudentEditorSelected)){
					$sql = "INSERT INTO
								EPOST_STUDENT_EDITOR_MANAGING
								(
									EditorID, ManageType, ManageItemID, Status, InputDate, ModifiedBy, ModifiedDate
								)
							VALUES
								(
									".$EditorObj->EditorID.", '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Newspaper']."', ".$NewspaperID.", ".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist'].", NOW(), $UserID, NOW()
								)";
					$result = $ePostNewspaper->db_db_query($sql);
				}
			}
		}
		
		# Create Cover Page By Default
		$Page_Data_ary['NewspaperID'] 	 = $NewspaperID;
		$Page_Data_ary['Page_Type']   	 = 'C1';
		$Page_Data_ary['Page_Layout']    = 1;
		$Page_Data_ary['Page_Theme']   	 = 0;
		$Page_Data_ary['Page_Name']   	 = 'Cover Page';
		$Page_Data_ary['Page_PageOrder'] = 0;
		$Page_Data_ary['Page_Status']	 = $cfg_ePost['BigNewspaper']['Newspaper_status']['exist'];
		
		$ePostPage = new libepost_page(0, $Page_Data_ary);
		
		if($ePostPage->update_page_db()){
			$PageID = $ePostPage->PageID;
		}
	}
}

intranet_closedb();
?>
<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery-1.3.2.min.js"></script>
<form name="form1" method="post" action="issue_new_design.php">
	<input type="hidden" id="NewspaperID" name="NewspaperID" value="<?=$NewspaperID?>"/>
	<input type="hidden" id="PageID" name="PageID" value="<?=$PageID?>"/>
</form>
<script>
	$(document).ready(function(){
		<?php if($IsPopup){ ?>
			parent.window.updateIssue();
			parent.window.tb_remove();
		<?php }else{ ?>
			document.form1.submit();
		<?php } ?>	
	});
</script>