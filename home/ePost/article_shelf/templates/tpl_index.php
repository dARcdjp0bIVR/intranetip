<?php 

include($intranet_root.'/home/ePost/templates/portal_head.php');
?>
<?php if($IsPopup){?>
<body id="edit_pop">
<div class="pop_container">
<div class="marking_board">
   <div class="table_board">
        <span class="sectiontitle"><?=$Lang['ePost']['ArticleShelf']?></span>
<?php }else{ ?>
	<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
					<p class="spacer">&nbsp;</p>
					<p class="spacer">&nbsp; </p>
					<?=$ePostUI->gen_portal_page_nav($nav_ary)?>
	        		<br />
<?php } ?>
					<p class="spacer">&nbsp; </p>
					<form name="filter_form" method="post" action="index.php" style="padding:0;margin:0;">
						<div class="content_top_tool">
							<div id="table_filter">
								<span id="courseware_filter"><?=$ePostUI->gen_courseware_drop_down_list('courseware', 'courseware', $courseware, 'onchange="change_courseware(this.value)"')?></span>
		               			<span id="article_type_filter">
		               			<?php if($courseware==1){
		            					echo $ePostUI->gen_article_type_drop_down_list($courseware, 'article_type', 'article_type', $article_type, $article_cfg['restriction'], 'onchange="change_article_type(this.value)"');
		            				  } else if($courseware==2){
		            				  	echo $ePostUI->gen_article_request_with_articles_drop_down_list($courseware, 'article_requestID', 'article_requestID', $article_requestID, '');
		            				  	//echo $ePostUI->gen_article_request_drop_down_list($courseware, 'article_requestID', 'article_requestID', $article_requestID, '');
		            				  }
		            			?>
								</span>
		            			<span id="article_sub_type_filter"><?=$ePostUI->gen_article_sub_type_drop_down_list($courseware, $article_type, 'article_sub_type', 'article_sub_type', $article_sub_type)?></span>
	    						<input type="button" name="select_article_btn" class="formsmallbutton" onclick="go_filter()" value="<?=$Lang['ePost']['Filter']['Go']?>" />
	          				</div>
	          				<p class="spacer"></p>
						</div>
						<?php if($IsPopup){?>
						<input type="hidden" id="NewspaperID" name="NewspaperID" value="<?=$NewspaperID?>" />
						<input type="hidden" id="PageID" name="PageID" value="<?=$PageID?>" />
						<input type="hidden" id="Position" name="Position" value="<?=$Position?>" />
						<input type="hidden" id="selected_article_id" name="selected_article_id" value="<?=$selected_article_id?>"/>
						<?php } ?>
						<input type="hidden" id="IsPopup" name="IsPopup" value="<?=$IsPopup?>"/>
					</form>

	
						<style>
							.common_table_list.edit_table_list th a{color:#FFFFFF;}
							.step_content_detail_box_s div table tr td{border:0; padding:0; margin:0;}
							<?php if($selected_article_id){ ?>
								#rowID_<?=$selected_article_id?> td, #rowDisplayID_<?=$selected_article_id?> td {background-color: #ECFEDA; border-bottom: 1px solid #CCCCCC;}
								#rowID_<?=$selected_article_id?> td .step_content_detail_box_s div table tr td, #rowDisplayID_<?=$selected_article_id?> td .step_content_detail_box_s div table tr td {background: none; border:0;}
								#rowDisplayID_<?=$selected_article_id?> td.log_photo_desc{background: none repeat scroll 0 0 #FFFFFF; border: 0px}
							<?php } ?>
						</style>
							
						<script>
							var article_content_ary = new Array();
							function go_filter(){
								document.filter_form.submit();
							}
						
							function change_courseware(courseware){
								var NewspaperID = $('#NewspaperID').val();
								var PageID		= $('#PageID').val();
								var Position	= $('#Position').val();
								
								var IsPopup		= $('#IsPopup').val();
								
								$.post(
										'ajax_gen_filter.php',
										{
											'NewspaperID'		 : NewspaperID,
											'PageID'			 : PageID,
											'Position'			 : Position,
											'courseware'		 : courseware,
											'FilterType[]'		 : [(courseware==1? 'article_type_filter':'article_requestID_filter'), 'article_sub_type_filter'],
											'otherAttr_ary[]'	 : [(courseware==1? 'onchange="change_article_type(this.value)"':''), ''],
											'Separator'		   	 : '|=|',
											'IsPopup'			 : IsPopup
										},
										function(data){
											if(data!='-1'){
												$('#article_type_filter').html(data.split('|=|')[0]);
												$('#article_sub_type_filter').html(data.split('|=|')[1]);
											}
										}
								);
							}
								
							function change_article_type(article_type){
								var NewspaperID = $('#NewspaperID').val();
								var PageID		= $('#PageID').val();
								var Position	= $('#Position').val();
								
								var courseware  = $('#courseware').val();
								
								var IsPopup		= $('#IsPopup').val();
								
								$.post(
										'ajax_gen_filter.php',
										{
											'NewspaperID'		 : NewspaperID,
											'PageID'			 : PageID,
											'Position'			 : Position,
											'courseware'		 : courseware,
											'article_type'		 : article_type,
											'FilterType[]'		 : ['article_sub_type_filter'],
											'Separator'		   	 : '|=|',
											'IsPopup'			 : IsPopup
										},
										function(data){
											if(data!='-1'){
												$('#article_sub_type_filter').html(data.split('|=|')[0]);
											}
										}
								);
							}
							
							function show_article(article_id){
								if(article_content_ary[article_id] != undefined){
									$('#rowContent_' + article_id).html(article_content_ary[article_id]);
									$('#rowDisplayID_' + article_id).attr('style', '');
								}
								else{
									
									$.post(
											'ajax_get_article.php',
											{
												'article_id' : article_id,
												'Separator'	 : '|=|'
											},
											function(data){
												var html			= '';
												var content 		= data.split('|=|')[0];
												var attachment 		= data.split('|=|')[1];
												var suggest_height 	= data.split('|=|')[2];
												
												html += "<div class='pool_content' " + (suggest_height? "style='height:" + suggest_height + "px'":"") + ">";
												html += "	<a href='javascript:close_article(" + article_id + ")' style='display: block; float: right;'><?=$Lang['ePost']['Close']?></a>";
												html += attachment;
												html += content;
												html += "</div>";
												
												article_content_ary[article_id] = html;
												
												$('#rowContent_' + article_id).html(html);
												$('#rowDisplayID_' + article_id).show();
											}
									);
								}
							}
							
							function close_article(article_id){
								$('#rowDisplayID_' + article_id).attr('style', 'display:none');
								$('#rowContent_' + article_id).html('');
							}
							
							function select_article(article_id){
								try{
									var obj = window.opener.document.form1;
									
									obj.ArticleShelfID.value = article_id;
									obj.action = '/home/ePost/newspaper/issue_page_select_article.php';
									obj.submit();
									
									window.close();
								}
								catch(ex){
									window.close();
								}
							}
							
							function openNewspaper(NewspaperID, PageID){
								var intWidth  = screen.width;
							 	var intHeight = screen.height-100;
								window.open('/home/ePost/newspaper/view_newspaper.php?NewspaperID='+NewspaperID+'&PageID='+PageID,"KeiWaEEGS_Newspaper","width="+intWidth+",height="+intHeight+",scrollbars,resizable");
							}
							
							function go_delete(ArticleShelfID){
								var obj = document.form1;
								var ArticleTitle = document.getElementById('article_title_' + ArticleShelfID).innerHTML;
								
								if(confirm('<?=$Lang['ePost']['Confirm']['RemoveArticle'][0]?>'+ ArticleTitle + '<?=$Lang['ePost']['Confirm']['RemoveArticle'][1]?>')){
									obj.action = 'delete_article.php';
									obj.ArticleShelfID.value = ArticleShelfID;
									obj.submit();
								}
							}
							
						
						</script>
						
						<form name="form1" method="post" style="padding:0;margin:0;">
						<div id="sys_msg"><?=$Lang['ePost']['ReturnMessage'][$returnMsg]?></div>
							<div class="table_board">
								<?=$li->display()?>
								<p class="spacer"></p>
								<p class="spacer"></p>
								<br>
							</div>
							<input type="hidden" id="NewspaperID" name="NewspaperID" value="<?=$NewspaperID?>" />
							<input type="hidden" id="PageID" name="PageID" value="<?=$PageID?>" />
							<input type="hidden" id="Position" name="Position" value="<?=$Position?>" />
							<input type="hidden" id="IsPopup" name="IsPopup" value="<?=$IsPopup?>"/>
							<input type="hidden" id="selected_article_id" name="selected_article_id" value="<?=$selected_article_id?>"/>
							<input type="hidden" id="courseware" name="courseware" value="<?=$courseware?>"/>
							<input type="hidden" id="article_type" name="article_type" value="<?=$article_type?>"/>
							<input type="hidden" id="article_requestID" name="article_requestID" value="<?=$article_requestID?>"/>
							<input type="hidden" id="article_sub_type" name="article_sub_type" value="<?=$article_sub_type?>"/>
							<input type="hidden" id="ArticleShelfID" name="ArticleShelfID" value=""/>
						</form>

	</div></div></div>	

<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>