<?php
# Process Attachment
$attachmentAry = array();
foreach($ePostArticleShelf->Attachment as $_attachmentId => $_attachmentPathAry){
	$_attachment = $_attachmentPathAry['attachment'];
	$_writingId = $_attachmentPathAry['writingId'];	
	$_caption = $_attachmentPathAry['caption'];
	$ext = strtoupper(substr($_attachment, '-4'));	
	if(in_array($ext, array('.MP4','.FLV','.MOV'))){
		$attachment_http_path = $ePostUI->gen_video_player($ePostArticleShelf->format_attachment_path($_attachment,true,!empty($_writingId)));
		$attachmentAry[] = array(
						"attachment"=>$attachment_http_path,
						"caption"=>$_caption
					);
		
	}
}

?>
<tr style="display:none">
	<td colspan="100%">
		<script>
			function writer_checkform(obj){
				var Title = obj.Title.value;
				var Content = obj.Content.value;

				if(Title==''){
					obj.Title.focus();
					alert('<?=$Lang['ePost']['Warning']['InputTitle']?>');
					return false;
				}
				
				if(Content==''){
					obj.Content.focus();
					alert('<?=$Lang['ePost']['Warning']['InputContent']?>');
					return false;
				}
				
				return check_upload_file_valid();
			}
			function delete_attachment(id){
				if(confirm('<?=$Lang['ePost']['Confirm']['RemoveAttachedVideoFile']?>')){
					$('#del_attach'+id).val(1);
					$('#attachment'+id).hide();
					$('#fileupload'+id).show();
					$('#Caption'+id).val('');
				}
			}	
			function check_upload_file_valid(){
				var fileupload  = '';
			<?php for($i=0;$i<$maxUploadCnt;$i++){?>
				fileupload  = $('#fileupload<?=$i?>').val();
				if(fileupload){
					filename = Get_File_Name(fileupload);
					var ext = fileupload.substr((fileupload.length-4)).toUpperCase();
					
					if(ext!='.FLV'&&ext!='.MOV'&&ext!='.MP4'){
						alert('<?=$Lang['ePost']['Warning']['UploadFLVFiles']?>');
						$('#fileupload<?=$i?>').val('');
						return false;
					}
				}
			<?php }?>
				return true;
			}							
		</script>
	</td>
</tr>
<tr>
	<td><?=$Lang['ePost']['Title']?></td>
	<td>:</td>
	<td>
		<input type="text" id="Title" name="Title" style="width:100%" value="<?=$ArticleShelfID? htmlspecialchars($ePostArticleShelf->Title):""?>"/>
	</td>
</tr>
<tr>
	<td><?=$Lang['ePost']['Content']?></td>
	<td>:</td>
	<td>
		<textarea type="text" id="Content" name="Content" style="width:100%" rows="8"><?=$ArticleShelfID? $ePostArticleShelf->Content:""?></textarea>
	</td>
</tr>
<?php 
for($i=0;$i<$maxUploadCnt;$i++){ 
	$_attachmentHtml = "";
	$_attachment = $attachmentAry[$i]['attachment'];
	
	$_caption = $attachmentAry[$i]['caption'];
	if(!empty($_attachment)){
		$_attachmentHtml .= "<span id=\"attachment".$i."\">";
		$_attachmentHtml .= $_attachment;
		$_attachmentHtml .= "<span class=\"table_row_tool\" style=\"float:right;position:absolute;\"><a href=\"javascript:delete_attachment(".$i.")\" class=\"tool_delete\" title=\"".$Lang['ePost']['Delete']."\">&nbsp;</a></span>";
		$_attachmentHtml .= "</span>";
	}	
	
	$_attachmentHtml .= "<input type=\"file\" id=\"fileupload".$i."\" name=\"fileupload".$i."\" ".($_attachment? "style='display:none'" : "")."/>";
	$_attachmentHtml .= "<input type=\"hidden\" id=\"del_attach".$i."\" name=\"del_attach".$i."\" value=\"0\"/>";
?>
	<tr>
		<td><?=$Lang['ePost']['Video']?></td>
		<td>:</td>
		<td><?=$_attachmentHtml?></td>
	</tr>
	<tr>
		<td><?=$Lang['ePost']['VideoCaption']?></td>
		<td>:</td>
		<td>
			<input type="text" id="Caption<?=$i?>" name="Caption<?=$i?>" style="width:100%" value="<?=$_caption?>"/>
		</td>
	</tr>
<?php } ?>
