<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$article_id = isset($article_id) && $article_id!=''? $article_id : '';
$Separator  = isset($Separator) && $Separator? $Separator : '|=|';

# Initialize Object
$ePostUI = new libepost_ui();
$ePostArticleShelf = new libepost_articleshelf($article_id);

# Check if ePost should be accessible
$ePostUI->portal_auth();
$ePostUI->attachment_format = ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")?'mp4':'flv';
# Initialize $para_ary for $ePostUI->gen_article_display();
$para_ary['Title'] 	 = $ePostArticleShelf->Title;

$para_ary['Content'] = nl2br($ePostArticleShelf->Content);

$para_ary['AttachmentAry'] = array();
foreach($ePostArticleShelf->Attachment as $_attachmentId => $_attachmentPathAry){
	$_attachment = $_attachmentPathAry['attachment'];
	$_caption = $_attachmentPathAry['caption'];
	$isWritingAttachment = $_attachmentPathAry['writingId']?true:false;
	$para_ary['AttachmentAry'][$_attachmentId] = array(
									'Original'	=>	$_attachment,
									'FilePath'	=>	$ePostArticleShelf->format_attachment_path($_attachment,false,$isWritingAttachment),
									'HTTPPath'	=>	$ePostArticleShelf->format_attachment_path($_attachment,true,$isWritingAttachment),
									'Caption'	=>	$_caption);
}


$article_display_ary = $ePostUI->gen_article_display($para_ary);

$result = $article_display_ary['content'].$Separator.$article_display_ary['attachment'].$Separator.$article_display_ary['suggest_height'];

echo $result;
?>