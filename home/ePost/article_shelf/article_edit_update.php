<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$ArticleShelfID = isset($ArticleShelfID) && $ArticleShelfID!=''? $ArticleShelfID : 0;

# Initialize Object
$ePostArticleShelf = new libepost_articleshelf($ArticleShelfID);
$WriterObj = new libepost_writer($ePostArticleShelf->WritingID);
$maxUploadCnt = $cfg_ePost['max_upload'][$WriterObj->ModuleCode];

# Check if ePost should be accessible
$ePostArticleShelf->portal_auth();
$ePost 	    = new libepost();
# Update Article Shelf
$ePostArticleShelf->Title 	= $Title;
$ePostArticleShelf->Content = $Content;
$ePostArticleShelf->Caption = $Caption;
/*if(count($ePostArticleShelf->Attachment)>0){
	$i=1;
	foreach($ePostArticleShelf->Attachment as $_attachmentId => $_attachmentPathAry){
		$ePostArticleShelf->Attachment[$_attachmentId]['caption'] = $_POST['Caption'.$i];
		$i++;
	}
}*/
$FileName = '';
$FileNameAry = array();
$attachmentKeyAry = count($ePostArticleShelf->Attachment)>0?array_keys($ePostArticleShelf->Attachment):array();

for($i=0;$i<$maxUploadCnt;$i++){
	$_delAttach = $_POST['del_attach'.$i];
	$_caption = $_POST['Caption'.$i];
	$_attachmentId = $attachmentKeyAry[$i];
	$_existAttachmentAry = !empty($_attachmentId)?$ePostArticleShelf->Attachment[$_attachmentId]:array();
	if(count($_existAttachmentAry)>0){ //existing attachment
		$_writingId = $_existAttachmentAry['writingId'];
		if($_delAttach){ //remove attachment file
			if(!empty($_writingId)){
				$ePostArticleShelf->Attachment[$_attachmentId]['articleShelfId']="";
				$ePostArticleShelf->Attachment[$_attachmentId]['caption']="";
			}else{
				$ePostArticleShelf->remove_article_shelf_attachment($_attachmentId);
						
			}
		}elseif(!empty($_caption)){
			$ePostArticleShelf->Attachment[$_attachmentId]['caption']=$_caption;		
		}
	}
	if($_FILES['fileupload'.$i]['error']==0){
		$FileLocation = $_FILES['fileupload'.$i]['tmp_name'];
		$FileName=$_FILES['fileupload'.$i]['name'];
		if(in_array($FileName,$FileNameAry)){
			$_fileNameAry = explode('.',$FileName);
			$FileName = $_fileNameAry[0].'(1).'.$_fileNameAry[1];
		}
		if($FileLocation){
			if(!is_dir($PATH_WRT_ROOT.'file/ePost'))
				 mkdir($PATH_WRT_ROOT.'file/ePost', 0777);
			if(!is_dir($PATH_WRT_ROOT.'file/ePost/appendix_attachment'))
				 mkdir($PATH_WRT_ROOT.'file/ePost/appendix_attachment', 0777);
			if(!is_dir($PATH_WRT_ROOT.'file/ePost/appendix_attachment/'.$ArticleShelfID))
				 mkdir($PATH_WRT_ROOT.'file/ePost/appendix_attachment/'.$ArticleShelfID, 0777);
				 
			$fileDes = $PATH_WRT_ROOT.'file/ePost/appendix_attachment/'.$ArticleShelfID.'/'.$FileName;
			move_uploaded_file($FileLocation, $fileDes);
			$ePost->convertWritingVideo($fileDes);
			if(file_exists($fileDes)){
				$FileName = $ePostArticleShelf->Get_Safe_Sql_Query($FileName);
				$sql = "INSERT INTO EPOST_WRITING_ATTACHMENT (ArticleShelfID,Attachment,ArticleShelfCaption,InputBy,InputDate,ModifiedBy,ModifiedDate) ";
				$sql .= "VALUES('".$ArticleShelfID."','".$FileName."','".$_caption."','".$UserID."',NOW(),'".$UserID."',NOW())";
				$ePostArticleShelf->db_db_query($sql);
				$FileNameAry[] = $FileName;
			}
			else{
				die('Attachment Upload Fail!');
			}	
		}

	}	
}
$result = $ePostArticleShelf->update_article_shelf_db();
$returnMsg = $result?"UpdateSuccess":"UpdateUnsuccess";
intranet_closedb();
header('Location:index.php?returnMsg='.$returnMsg);
?>
