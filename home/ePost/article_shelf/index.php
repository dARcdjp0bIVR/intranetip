<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_newspaper.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$IsPopup  = isset($IsPopup) && $IsPopup!=''? $IsPopup : 0;

$PageID   = isset($PageID) && $PageID!=''? $PageID : '';
$Position = isset($Position) && $Position!=''? $Position : 1;

$courseware   	   = isset($courseware) && $courseware!=''? $courseware : 2;
$article_type 	   = isset($article_type) && $article_type!=''? $article_type : '';
$article_sub_type  = isset($article_sub_type) && $article_sub_type!=''? $article_sub_type : '';
$article_requestID = isset($article_requestID) && $article_requestID!=''? $article_requestID : 0;

$selected_article_id = isset($selected_article_id) && $selected_article_id!=''? $selected_article_id : '';

# Initialize Object
$ePostUI = new libepost_ui();

# Check if ePost should be accessible
$ePostUI->portal_auth();
$Header=($IsPopup)?'PopUp':'NEW';
$FromPage = 'editor';
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>$Lang['ePost']['ArticleList'], "link"=>""); 


# Generate SQL for libdbtable
# Generate sub_sql for Type and Topic
$type_sql  = "CASE \n";
$topic_sql = "CASE \n";

foreach($cfg_ePost['BigNewspaper']['ArticleType'] as $courseware_ary){
	foreach($courseware_ary['types'] as $type_ary){
		if(is_array($type_ary['search_attr']) && $type_ary['search_attr']['level']){
			# For Type column
			if($type_ary['search_attr']['content'])
				$type_sql .= "WHEN ew.LevelCode = '".$type_ary['search_attr']['level']."' && ew.ModuleCode = '".$type_ary['search_attr']['content']."' THEN '".$type_ary['name']."'\n";
			else
				$type_sql .= "WHEN ew.LevelCode = '".$type_ary['search_attr']['level']."' THEN '".$type_ary['name']."'\n";
		}	
		
		if(is_array($type_ary['sub_types'])){
			foreach($type_ary['sub_types'] as $sub_type_ary){
				if(is_array($sub_type_ary['search_attr'])){
					if($sub_type_ary['search_attr']['level'] && $sub_type_ary['search_attr']['content'] && $sub_type_ary['search_attr']['themeCode']){
						$topic_sql .= "WHEN ew.LevelCode = '".$sub_type_ary['search_attr']['level']."' && ew.ModuleCode = '".$sub_type_ary['search_attr']['content']."' && ew.ThemeCode = '".$sub_type_ary['search_attr']['themeCode']."' THEN '".$sub_type_ary['name']."'\n";
					}
					else if($sub_type_ary['search_attr']['level'] && $sub_type_ary['search_attr']['content']){
						$topic_sql .= "WHEN ew.LevelCode = '".$sub_type_ary['search_attr']['level']."' && ew.ModuleCode = '".$sub_type_ary['search_attr']['content']."' THEN '".$sub_type_ary['name']."'\n";
					}
				}
			}
		}
	}
}

$type_sql  .= "ELSE '--' END AS Type\n";
$topic_sql .= "ELSE IF(enr.Topic IS NOT NULL AND enr.Topic != '', enr.Topic, '--') END AS Topic\n";

/* Temporary Ignore Topic, Show Request Topic instead */
//$topic_sql = "IF(enr.Topic IS NOT NULL AND enr.Topic != '', enr.Topic, '--') AS Topic";
//IF(ea.Attachment IS NOT NULL, '".$Lang['ePost']['Yes']."', '".$Lang['ePost']['No']."') AS Attachment,
$sql = "SELECT
			CONCAT('<a id=\"article_title_', ea.ArticleShelfID, '\" href=\"javascript:show_article(', ea.ArticleShelfID, ')\">', ea.Title, '</a>'),
			IF(COUNT(wa.Attachment)>0, '".$Lang['ePost']['Yes']."', '".$Lang['ePost']['No']."') AS Attachment,
			IF(iuw.ClassName IS NOT NULL AND iuw.ClassName != '', CONCAT(iuw.ClassName, IF(iuw.ClassNumber IS NOT NULL AND iuw.ClassNumber !='', CONCAT('-',iuw.ClassNumber), '')), '--') AS Class,
			".getNameFieldByLang('iuw.')." AS Student,
			IF(ew.RequestID=0, '".$Lang['ePost']['Filter']['Courseware']."', '".$Lang['ePost']['Filter']['NonCourseware']."') AS Source,
			$type_sql,
			$topic_sql,
			DATE_FORMAT(ew.modified, '%Y-%m-%d') AS Submission,
			CONCAT(DATE_FORMAT(ea.inputdate, '%Y-%m-%d'), '<br/>', ".getNameFieldByLang('iua.').") AS Recommended,";

if($IsPopup){
	$sql .= "CONCAT('<input type=\"button\" value=\"".$Lang['ePost']['Select']."\" class=\"formbutton\" name=\"select_btn_', ea.ArticleShelfID, '\" onclick=\"select_article(', ea.ArticleShelfID, ')\">') ControlButton,";
}
else{
	$sql .= "IFNULL(GROUP_CONCAT(DISTINCT CONCAT('<a href=\"javascript:openNewspaper(', en.NewspaperID, ', ', enp.PageID, ')\">', en.Name, '</a>') ORDER BY en.IssueDate ASC SEPARATOR '<br/>'), '&nbsp;') IssueList,";
	//if($courseware!=1){
//		$sql .= " IF(ew.LevelCode = 'C', '',CONCAT('<div class=\"table_row_tool\">
//							<a href=\"article_edit.php?ArticleShelfID=', ea.ArticleShelfID, '\" title=\"".$Lang['ePost']['Edit']."\">&nbsp;</a>
//							<!--<a href=\"javascript:go_delete(', ea.ArticleShelfID, ')\" class=\"tool_delete\" title=\"".$Lang['ePost']['Delete']."\">&nbsp;</a>-->
//						</div>')) ControlButton,";	
		$sql .= " CONCAT('<a href=\"article_edit.php?ArticleShelfID=', ea.ArticleShelfID, '\" title=\"".$Lang['ePost']['Edit']."\">&nbsp;</a>',
							'|=|',
							'<a href=\"javascript:go_delete(', ea.ArticleShelfID, ')\" class=\"tool_delete\" title=\"".$Lang['ePost']['Delete']."\">&nbsp;</a>')ControlButton,";	
	//}
}

	$sql .= "ea.ArticleShelfID AS ID,
			ea.Title AS Title,
			ew.LevelCode
		FROM
			EPOST_ARTICLE_SHELF AS ea INNER JOIN
			EPOST_WRITING AS ew ON ea.WritingID = ew.RecordID INNER JOIN
			INTRANET_USER AS iuw ON ew.UserID = iuw.UserID INNER JOIN
			INTRANET_USER AS iua ON ea.InputBy = iua.UserID LEFT JOIN
			EPOST_NONCOURSEWARE_REQUEST AS enr ON ew.RequestID = enr.RequestID  LEFT JOIN
			EPOST_WRITING_ATTACHMENT AS wa ON ea.ArticleShelfID = wa.ArticleShelfID ";

if(!$IsPopup){
	$sql .= " LEFT JOIN EPOST_NEWSPAPER_PAGE_ARTICLE AS enpa ON ea.ArticleShelfID = enpa.ArticleShelfID AND enpa.Status = ".$cfg_ePost['BigNewspaper']['Article_status']['exist']."
			  LEFT JOIN EPOST_NEWSPAPER_PAGE AS enp ON enpa.PageID = enp.PageID AND enp.Status = ".$cfg_ePost['BigNewspaper']['Page_status']['exist']."
			  LEFT JOIN EPOST_NEWSPAPER AS en ON enp.NewspaperID = en.NewspaperID AND en.Status = ".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']." ";
}

$sql .= "WHERE
			ea.Status = '".$cfg_ePost['BigNewspaper']['ArticleShelf_status']['exist']."' AND
			ea.IsAppendix = ".$cfg_ePost['BigNewspaper']['ArticleShelf_IsAppendix']['No'];

if($courseware==1){
	$courseware_name = array_search($courseware, $cfg_ePost['BigNewspaper']['courseware']);
	$sql .= " AND ew.RequestID = 0";
	
	if($article_type && !$article_sub_type){
		$search_level    = $cfg_ePost['BigNewspaper']['ArticleType'][$courseware_name]['types'][$article_type]['search_attr']['level'];
		$search_content  = $cfg_ePost['BigNewspaper']['ArticleType'][$courseware_name]['types'][$article_type]['search_attr']['content'];
			
		$sql .= $search_level?   " AND ew.LevelCode = '$search_level'" : "";
		$sql .= $search_content? " AND ew.ModuleType = '".$cfg_ePost['module_type']['content']."' AND ew.ModuleCode = '$search_content'" : "";
	}
	else if($article_type && $article_sub_type){
		$search_level     = $cfg_ePost['BigNewspaper']['ArticleType'][$courseware_name]['types'][$article_type]['sub_types'][$article_sub_type]['search_attr']['level'];
		$search_content   = $cfg_ePost['BigNewspaper']['ArticleType'][$courseware_name]['types'][$article_type]['sub_types'][$article_sub_type]['search_attr']['content'];
		$search_themeCode = $cfg_ePost['BigNewspaper']['ArticleType'][$courseware_name]['types'][$article_type]['sub_types'][$article_sub_type]['search_attr']['themeCode'];
		
		$sql .= $search_level?   " AND ew.LevelCode = '$search_level'" : "";
		$sql .= $search_content? " AND ew.ModuleType = '".$cfg_ePost['module_type']['content']."' AND ew.ModuleCode = '$search_content'" : "";
		$sql .= $search_themeCode? " AND ew.ThemeCode = '$search_themeCode'" : "";
	}
	else if(is_array($article_cfg['restriction'])){
		$search_ary = array();
		for($i=0;$i<count($article_cfg['restriction']);$i++){
			$article_type_ary = $cfg_ePost['BigNewspaper']['ArticleType'][$courseware_name]['types'][$article_cfg['restriction'][$i]];
			
			if(is_array($article_type_ary['sub_types']) && count($article_type_ary['sub_types'])){
				foreach($article_type_ary['sub_types'] as $loop_article_sub_type=>$loop_article_sub_type_ary){
					$search_level    = $cfg_ePost['BigNewspaper']['ArticleType'][$courseware_name]['types'][$article_cfg['restriction'][$i]]['sub_types'][$loop_article_sub_type]['search_attr']['level'];
					$search_content  = $cfg_ePost['BigNewspaper']['ArticleType'][$courseware_name]['types'][$article_cfg['restriction'][$i]]['sub_types'][$loop_article_sub_type]['search_attr']['content'];
					
					$search_ary[$search_level][] = $search_content;
				}
			}
			else{
				$search_level    = $cfg_ePost['BigNewspaper']['ArticleType'][$courseware_name]['types'][$article_cfg['restriction'][$i]]['search_attr']['level'];
				$search_content  = $cfg_ePost['BigNewspaper']['ArticleType'][$courseware_name]['types'][$article_cfg['restriction'][$i]]['search_attr']['content'];
				
				$search_ary[$search_level][] = $search_content;
			}
		}
		
		if(is_array($search_ary) && count($search_ary)){
			foreach($search_ary as $search_level=>$search_content){
				if(is_array($search_content) && count($search_content)){
					$sql .= " AND (ew.LevelCode != '$search_level' OR (ew.LevelCode = '$search_level' AND ew.ModuleType = '".$cfg_ePost['module_type']['content']."' AND ew.ModuleCode NOT IN ('".implode("','", $search_content)."')))";
				}
				else{
					$sql .= " AND ew.LevelCode != '$search_level'";
				}
			}
		}
	}
}
else if($courseware==2){
	$sql .= $article_requestID? " AND ew.RequestID = $article_requestID":" AND ew.RequestID != 0";
}

//if(!$IsPopup){
	$sql .= " GROUP BY ea.ArticleShelfID";
//}

# Assign variables to table object
$field  = $field==''? 1:$field;
$order  = $order==''? 1:$order;
$pageNo = $pageNo==''? 1:$pageNo;
$li = new libdbtable2007($field, $order, $pageNo, true);
$li->field_array = array("", "Title", "Attachment", "Class", "Student", "Source", "Type", "Topic", "Submission", "Recommended", "ControlButton");
$li->sql = $sql;
$li->page_size = ($numPerPage=='')?10:$numPerPage;
$li->IsColOff = "ePost_article_shelf";
$li->no_col = sizeof($li->field_array) + (($IsPopup||$courseware==1)? 0:1);
$li->count_mode = 1;
$pos = 1;
$li->column_list .= "<th class=\"num_check\">#</th>\n";
$li->column_list .= "<th width=\"".($IsPopup? "20%":"15%")."\">".$li->column($pos++, $Lang['ePost']['Title'])."</th>\n";
$li->column_list .= "<th width=\"".($IsPopup? "7%":"5%")."\">".$li->column($pos++, $Lang['ePost']['ImageVideo'])."</th>\n";
$li->column_list .= "<th width=\"7%\">".$li->column($pos++, $Lang['ePost']['Class'])."</th>\n";
$li->column_list .= "<th width=\"10%\">".$li->column($pos++, $Lang['ePost']['Student'])."</th>\n";
$li->column_list .= "<th width=\"10%\">".$li->column($pos++, $Lang['ePost']['Source'])."</th>\n";
$li->column_list .= "<th width=\"7%\">".$li->column($pos++, $Lang['ePost']['Type'])."</th>\n";
$li->column_list .= "<th width=\"10%\">".$li->column($pos++, $Lang['ePost']['RequestTopic'])."</th>\n";
$li->column_list .= "<th width=\"10%\">".$li->column($pos++, $Lang['ePost']['SubmissionDate'])."</th>\n";
$li->column_list .= "<th width=\"10%\">".$li->column($pos++, $Lang['ePost']['RecommendedBy'])."</th>\n";
$li->column_list .= $IsPopup? "<th>&nbsp;</th>" : "<th width=\"60\">".$li->column($pos++, $Lang['ePost']['Issue'])."</th>\n";
if($courseware!=1)
$li->column_list .= $IsPopup? "" : "<th>&nbsp;</th>\n";

$li->db_db_query("SET SESSION group_concat_max_len = 1048576"); // Set the max length of group concat to 1MB

include($intranet_root.'/home/ePost/article_shelf/templates/tpl_index.php');

intranet_closedb();
?>