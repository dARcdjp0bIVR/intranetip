<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_articleshelf.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$ArticleShelfID = isset($ArticleShelfID) && $ArticleShelfID!=''? $ArticleShelfID : "";

# Initialize Object
$ePostUI   		   = new libepost_ui();
$ePostArticleShelf = new libepost_articleshelf($ArticleShelfID);
$ePostWriter  	   = new libepost_writer($ePostArticleShelf->WritingID);

$Header='NEW';
$FromPage = 'editor';
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>$Lang['ePost']['ArticleList'], "link"=>"index.php"); 
$nav_ary[] = array("title"=>$Lang['ePost']['Edit'], "link"=>""); 
# Check if ePost should be accessible
$ePostUI->portal_auth();
$ePostUI->attachment_format = ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")?'mp4':'flv';
# Decided Courseware, Type and Sub-Type
foreach($cfg_ePost['BigNewspaper']['ArticleType'][($ePostWriter->RequestID? "non_courseware":"courseware")]['types'] as $type=>$type_ary){
	if($type_ary['search_attr']['level']==$ePostWriter->LevelCode && $type_ary['search_attr']['content']==$ePostWriter->ModuleCode){
		$tmp_article_type = $type;
		break;
	}
	else{
		if(is_array($type_ary['sub_types']) && count($type_ary['sub_types'])){
			foreach($type_ary['sub_types'] as $sub_type=>$sub_type_ary){
				if($sub_type_ary['search_attr']['level']==$ePostWriter->LevelCode && $sub_type_ary['search_attr']['content']==$ePostWriter->ModuleCode){
					$tmp_article_type 	  = $type;
					$tmp_article_sub_type = $sub_type;
					break;
				}
			}
			
			if($tmp_article_type && $tmp_article_sub_type) break;
		}
	}
}
		
$courseware = $ePostWriter->LevelCode=='NC'? 2 : 1;

$article_requestID     = $courseware==2? $ePostWriter->RequestID:'';
$article_request_topic = '';
if($article_requestID){
	include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");
	$ePostRequest = new libepost_request($article_requestID);
	$article_request_topic = $ePostRequest->Topic;
}
 
$article_type 	   = $courseware==1? $tmp_article_type:'';
$article_sub_type  = $courseware==1? $tmp_article_sub_type:'';

# Format file path of writer template
if($courseware==2){
	$writer_template = $intranet_root."/home/ePost/article_shelf/templates/article_edit/NC/".$ePostWriter->ModuleCode.".php";
}
else{
	$writer_template = "templates/article_shelf/".$ePostWriter->LevelCode."/content/".$ePostWriter->ModuleCode."/".$theme_obj->theme_writer_code.".php";
}

# Check writer template file is exist or not
$writer_template = file_exists($writer_template)? $writer_template : "";
if($writer_template=='') die("Writer template file not exist!");
$maxUploadCnt = $cfg_ePost['max_upload'][$ePostWriter->ModuleCode];
include($intranet_root.'/home/ePost/article_shelf/templates/tpl_article_edit.php');

intranet_closedb();
?>