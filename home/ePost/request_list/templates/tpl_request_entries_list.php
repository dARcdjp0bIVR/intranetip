<?php
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

<style>
	.common_table_list.edit_table_list th a{color:#FFFFFF;}
	.step_content_detail_box_s div table tr td{border:0; padding:0; margin:0;}
	td.log_photo_desc{background: none repeat scroll 0 0 #FFFFFF;}
</style>

<script>
	var article_content_ary = new Array();
	
	function go_filter(){
		document.filter_form.submit();
	}
	function go_delete(WritingID){
		var obj = document.form1;
		var WritingName = $("a#article_title_"+WritingID).html();
		
		if(confirm('<?=$Lang['ePost']['Confirm']['RemoveWriting'][0]?>'+ WritingName + '<?=$Lang['ePost']['Confirm']['RemoveWriting'][1]?>')){
			obj.action = 'writing_delete.php';
			obj.WritingID.value = WritingID;
			obj.submit();
		}
	}	
	function show_article(recordID){
		if(article_content_ary[recordID] != undefined){
			$('#rowContent_' + recordID).html(article_content_ary[recordID]);
			$('#rowDisplayID_' + recordID).attr('style', '');
		}
		else{
			$.post(
					'ajax_get_article.php',
					{
						'RecordID' 	: recordID,
						'Separator'	: '|=|'
					},
					function(data){
						var html			= '';
						var content 		= data.split('|=|')[0];
						var attachment 		= data.split('|=|')[1];
						var suggest_height 	= data.split('|=|')[2];
						
						html += "<div class='pool_content' " + (suggest_height? "style='height:" + suggest_height + "px'":"") + ">";
						html += "	<a href='javascript:close_article(" + recordID + ")' style='display: block; float: right;'><?=$Lang['ePost']['Close']?></a>";
						html += attachment;
						html += content;
						html += "</div>";
						
						article_content_ary[recordID] = html;
						
						$('#rowContent_' + recordID).html(html);
						$('#rowDisplayID_' + recordID).show();
					}
			);
		}
	}
	
	function comment_article(RecordID){
		var title = '<?=$Lang['ePost']['Select']?>';
		var obj = document.filter_form;
		
	//	var url   = 'comment_record.php?RecordID='+RecordID+'&TB_iframe=true&width=720&height=650';
		obj.action = 'comment_record.php?RecordID='+RecordID+'&pageNo=<?=$pageNo?>&numPerPage=<?=$numPerPage?>';
		obj.submit();
	//	tb_show(title, url);
	}
	
	function close_article(recordID){
		$('#rowDisplayID_' + recordID).attr('style', 'display:none');
		$('#rowContent_' + recordID).html('');
	}
	
	function openNewspaper(NewspaperID, PageID){
		var intWidth  = screen.width;
	 	var intHeight = screen.height-100;
		window.open('/home/ePost/newspaper/view_newspaper.php?NewspaperID='+NewspaperID+'&PageID='+PageID,"Newspaper","width="+intWidth+",height="+intHeight+",scrollbars,resizable");
	}
</script>
<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
<p class="spacer">&nbsp;</p>
<p class="spacer">&nbsp;</p>
<?=$ePostUI->gen_portal_page_nav($nav_ary)?>
<p class="spacer">&nbsp;</p>
<form name="filter_form" method="post" action="request_entries_list.php" style="margin:0;padding:0">
	<div class="content_top_tool">
		<div id="table_filter">
			<select name="Status" id="Status">
				<option value="0" <?=$Status==0? "selected":""?>><?=$Lang['ePost']['Filter']['All']?></option>
				<option value="1" <?=$Status==1? "selected":""?>><?=$Lang['ePost']['Filter']['SelectedArticle']?></option>
				<option value="2" <?=$Status==2? "selected":""?>><?=$Lang['ePost']['Filter']['ArticlePostedOnIssue']?></option>
			</select>
			<input name="filter_go_btn" type="button" class="formsmallbutton" value="<?=$Lang['ePost']['Filter']['Go']?>" onclick="go_filter()"/>
		</div>
		<p class="spacer"></p>
		<br>
	</div>
	<input type="hidden" id="RequestID" name="RequestID" value="<?=$RequestID?>"/>
</form>
<form name="form1" action="" method="post" style="margin:0;padding:0">
<div id="sys_msg"><?=$Lang['ePost']['ReturnMessage'][$returnMsg]?></div>
	<div class="table_board">
		<?=$li->display()?>
		<p class="spacer"></p>
		<p class="spacer"></p>
		<br>
	</div>
	<em style="float:left;margin-left:5px;width:15px;height:15px;background-color:#f1f1f1;border:1px solid #ccc">&nbsp;</em>&nbsp;=&nbsp;<?=$Lang['ePost']['EntryTransferredToArticleShelf']?>
	<input type="hidden" id="RequestID" name="RequestID" value="<?=$RequestID?>"/>
	<input type="hidden" id="WritingID" name="WritingID"/>	
	<input type="hidden" id="Status" name="Status" value="<?=$Status?>"/>
</form>
</div></div></div>
<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>