<?php
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>
<style>
	select {font-size:12px;}
</style>
<script>
	function go_submit(){
		var obj = document.form1;
		
		if(!obj.RequestTitle.value){
			alert('<?=$Lang['ePost']['Warning']['InputTopic']?>');
			obj.RequestTitle.focus();
			return false;
		}
		else if(!obj.RequestType.value){
			alert('<?=$Lang['ePost']['Warning']['SelectType']?>');
			obj.RequestType.focus();
			return false;
		}
		else if(!obj.RequestDeadline.value){
			alert('<?=$Lang['ePost']['Warning']['SelectDeadline']?>');
			obj.RequestDeadline.focus();
			return false;
		}
		/*
		else if($(':checkbox[name="RequestTargetLevel[]"]:checked').length==0){
			alert('Please select the Request Target Level!');
			obj.RequestTargetLevel[0].focus();
			return false;
		}
		*/
		else if(!obj.RequestDesc.value){
			alert('<?=$Lang['ePost']['Warning']['InputDescription']?>');
			obj.RequestDesc.focus();
			return false;
		}
		else{
			$('#StudentEditorSelected option').each(function(){
				$(this).attr('selected','selected');
			});
			
			obj.submit();
		}
	}
	
	function go_cancel(){
		history.back(-1);
	}
</script>

<form name="form1" action="request_new_update.php" method="post" style="margin:0;padding:0">
	<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
        <p class="spacer">&nbsp; </p>
        <p class="spacer">&nbsp; </p>
        <?=$ePostUI->gen_portal_page_nav($nav_ary)?>
        <br />

        <p class="spacer">&nbsp; </p>
          <div class="marking_board">
            <!--## Step start ##-->
            <br />
			<table class="form_table">
			  <tr>
				<td><?=$Lang['ePost']['Topic']?><span style="color:red;font-weight:bold;">*</span></td>
				<td>:</td>
				<td ><input name="RequestTitle" type="text" id="RequestTitle" size="15" value="<?=$RequestID? $ePostRequest->Topic:''?>" maxlength="150"/></td>
			  </tr>
			  <tr>
				<td><?=$Lang['ePost']['Type']?><span style="color:red;font-weight:bold;">*</span></td>
				<td>:</td>
				<td ><?=$ePostUI->gen_article_type_drop_down_list($cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['id'], 'RequestType', 'RequestType', ($RequestID? $ePostRequest->Type:''), array(), '', false)?></td>
			  </tr>
			  <tr>
				<td><?=$Lang['ePost']['Deadline']?><span style="color:red;font-weight:bold;">*</span></td>
				<td>:</td>
				<td >
					<script>
						$(document).ready(function(){
							$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '/images/2009a/icon_calendar_off.gif', buttonText: '', mandatory: true});
							$('#RequestDeadline').datepick({
								dateFormat: 'yy-mm-dd',
								dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
								changeFirstDay: false,
								firstDay: 0
							});
						});
					</script>
					
					<input name="RequestDeadline" type="text" id="RequestDeadline" size="15" value="<?=$RequestID? date('Y-m-d', strtotime($ePostRequest->Deadline)):''?>" readonly/></td>
			  </tr>
			  <!--tr>
				<td><?=$Lang['ePost']['TargetLevels']?></td>
				<td>:</td>
				<td>
				  <input name="submit" type="button" class="formsubbutton" value="Select" />
				</td>
			  </tr-->
			  <tr>
				<td><?=$Lang['ePost']['Description']?><span style="color:red;font-weight:bold;">*</span></td>
				<td>:</td>
				<td ><textarea name="RequestDesc" id="RequestDesc" rows="4" cols="30"><?=$RequestID? $ePostRequest->Description:''?></textarea></td>
			  </tr>
			<?php if($ePost->user_obj->isTeacherStaff() && $ePost->is_editor){ ?>
				<tr>
					<td><?=$Lang['ePost']['StudentEditor']?></td>
					<td>:</td>
					<td><?=$ePostUI->gen_student_editor_selection($ePostStudentEditors, ($RequestID? $ePostRequest->StudentEditors:array()))?></td>
				</tr>
			<?php } ?>
			  <col class="field_title" />
			  <col  class="field_c" />
			</table>
<!--## Step end ##-->
                        <p class="spacer"></p>
        </div><p class="spacer"></p>
        <div class="edit_bottom">
          <p class="spacer"></p><br />
          <input name="submit_btn" type="button" class="formbutton" onclick="go_submit()" value="<?=$RequestID? $Lang['ePost']['Button']['Update']:$Lang['ePost']['Button']['Submit']?>" />
		<input name="cancel_btn" type="button" class="formsubbutton" onclick="go_cancel()" value="<?=$Lang['ePost']['Button']['Cancel']?>" />
          </div>
		  <input type="hidden" id="RequestID" name="RequestID" value="<?=$RequestID?>"/>
     </div></div></div>

            
</form>

<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>