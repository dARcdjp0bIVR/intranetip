<?php 
include($intranet_root.'/home/ePost/templates/portal_head.php');
?>

<body>
	<script>
		$(document).ready(function(){
			$('#TB_closeWindowButton', window.parent.document).unbind('click');
			$('#TB_closeWindowButton', window.parent.document).click(function(){
				close_tb();
			});
		});
	
		function add_rubrics(rubrics){
			if(rubrics != 'Please choose...'){
				var TeacherComment = $('#TeacherComment').val();
				
				if(TeacherComment=='')
					$('#TeacherComment').val(rubrics);
				else
					$('#TeacherComment').val(TeacherComment + '\n' + rubrics);
			}
		}
		
		function toggle_display_row(ShowHide){
			if(ShowHide=='hide')
				document.getElementById('display_row').style.display = 'none';
			else
				document.getElementById('display_row').style.display = '';
		}
		
		function go_submit(IsMarkNext){
			var obj = document.form1;
			
			obj.IsMarkNext.value = IsMarkNext;
			obj.action = 'comment_record_update.php';
			obj.submit();
		}
		
		function close_tb(){
			parent.window.document.form1.submit();
			parent.window.tb_remove();
		}
		function go_cancel(){
			var obj = document.form1;
			obj.action = "request_entries_list.php";
			obj.submit();
		}

	var format = '<?=($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")?'mp4':'flv'?>'; 
	$(document).ready(function(){
		$('.video_attachment').each(function(){
			var that = $(this);
			var path = that.attr("path");
			
			$.post("/home/ePost/ajax.php", { task:'gen_video_player',path:path,format:format},  
				function(data, textStatus){
					that.html(data);
					$('table.log_photo').show();
				}		
			);	
		});
	});			
	</script>
	<form name="form1" method="post">
		<div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
        <p class="spacer">&nbsp; </p>
        <p class="spacer">&nbsp; </p>
        <?=$ePostUI->gen_portal_page_nav($nav_ary)?>
        <br />
		     <p class="spacer">&nbsp; </p>
		<div class="main_board">
			<div class="main_board_top_left">
				<div class="main_board_top_right"></div>
			</div>
	  		<div class="main_board_left">
	  			<div class="main_board_right">
	        		<div class="table_board" style="min-height:625px">
	        			<span class="sectiontitle"><?=$ePostRequest->Topic?> - <?=$ePostWriter->ClassName.($ePostWriter->ClassNumber? "-".$ePostWriter->ClassNumber : "")?> <?=$ePostWriter->EnglishName?></span>
						<p class="spacer"></p>
	        			<div class="marking_board">
	        				<div class="content_paper" style="width: 100%;">
	      						<div class="content_paper_top_left">
	      							<div class="content_paper_top_right"></div>
	      						</div>
	       						<div class="content_paper_left">
	       							<div class="content_paper_right">
										<div class="student_work_content">
	        								<?php if($article_display_ary['title']){ ?><h1 class="student_work_title"><?=$article_display_ary['title']?></h1><?php } ?>		
	        															
	        								<span><?=$article_display_ary['attachment'].$article_display_ary['content']?></span>
										</div>
	       	 							<p class="spacer"></p>
	       							</div>
	       						</div> 
	      						<div class="content_paper_bottom_left">
	      							<div class="content_paper_bottom_right"></div>
	      						</div>      
	      					</div>
							<div class="marking_area">
	        					<h1 class="marking_header"><?=$Lang['ePost']['CommentArea']?></h1>
	          					<table class="form_table">
	             					<tbody>
	             						<tr>
		               						<td><?=$Lang['ePost']['Rubrics']?></td>
		               						<td>:</td>
		               						<td>
		               							<select name="rubrics" id="rubrics" onchange="add_rubrics(this.value)">
		               								<option value="Please choose..."><?=$Lang['ePost']['PleaseChoose']?></option>
		                 							<?php 
		                 							for($i=0;$i<count($cfg_ePost['comment_rubrics']);$i++){
		                 								echo "<option value=\"".$cfg_ePost['comment_rubrics'][$i]."\">".$cfg_ePost['comment_rubrics'][$i]."</option>";
		                 							} 
		                 							?>
		                 						</select>
		               						</td>
		             					</tr>
		             					<tr>
		              						<td><?=$Lang['ePost']['Comment']?></td>
		               						<td>:</td>
		               						<td colspan="3">
		                 						<textarea name="TeacherComment" rows="4" wrap="virtual" class="textbox" id="TeacherComment"><?=$ePostWriter->TeacherComment?></textarea>
											</td>
		             					</tr>
	             						<tr>
	               							<td><?=$Lang['ePost']['Redo']?></td>
	               							<td>:</td>
	               							<td>
	               								<?php if($ePostWriter->IsRedo==$cfg_ePost['sql_config']['IsRedo']['true']){ ?>
	               									<?php if($ePostWriter->Status==$cfg_ePost['sql_config']['Status']['submitted']){ ?>
	               										<input name="IsRedo" id="IsRedo_1" type="radio" value="1" onclick="toggle_display_row('hide')"/>
	              	  									<label for="IsRedo_1"><?=$Lang['ePost']['RedoAgain']?></label>
					  									&nbsp;
					  									<input name="IsRedo" id="IsRedo_2" type="radio" value="2" onclick="toggle_display_row('show')"/>
	              	  									<label for="IsRedo_2"><?=$Lang['ePost']['Accept']?></label>
					  									&nbsp;
					  									<input name="IsRedo" id="IsRedo_3" type="radio" value="3" onclick="toggle_display_row('hide')" checked/>
	              	  									<label for="IsRedo_3"><?=$Lang['ePost']['DecideLater']?></label>
	               									<?php } else { ?>
	               										<input name="IsRedo" id="IsRedo_1" type="radio" value="1" checked/>
														<label for="IsRedo_1"><?=$Lang['ePost']['Yes_1']?></label>
														&nbsp;
														<input name="IsRedo" id="IsRedo_0" type="radio" value="0" disabled/>
														<label for="IsRedo_0"><?=$Lang['ePost']['No_1']?></label>
	               									<?php } ?>
				  								<?php } else { ?>
				  									<input name="IsRedo" id="IsRedo_1" type="radio" value="1" onclick="toggle_display_row('hide')"/>
													<label for="IsRedo_1"><?=$Lang['ePost']['Yes_1']?></label>
													&nbsp;
													<input name="IsRedo" id="IsRedo_0" type="radio" value="0" onclick="toggle_display_row('show')" checked/>
													<label for="IsRedo_0"><?=$Lang['ePost']['No_1']?></label>
				  								<?php } ?>
				  							</td>
	             						</tr>
	             						<tr id="display_row" <?=$ePostWriter->IsRedo==$cfg_ePost['sql_config']['IsRedo']['true']||$ePostWriter->InNewspaper? "style='display:none'" : ""?>>
	               							<td><?=$Lang['ePost']['DisplayIn']?></td>
	               							<td>:</td>
	               							<td>
	                 							<input name="InNewspaper" id="InNewspaper" type="checkbox" value="1"/>
	               								<label for="InNewspaper"><?=$Lang['ePost']['ArticleShelf']?></label>
	               							</td>
	             						</tr>
	             					</tbody>
	             					<col class="field_title">
	             					<col class="field_c">
	           					</table>
	        				</div>
	         				<p class="spacer"></p>
	        			</div>
	        			<p class="spacer"></p>
	        			<div class="edit_bottom">
	          				<p class="spacer"></p>
	          				<br>
	            			<input name="submit_btn" class="formbutton" value="<?=$Lang['ePost']['Button']['Submit']?>" type="button" onclick="go_submit(0)">
	            			
	            			<?php if($NextRecordID) {?>
	            			<input name="mark_next_btn" class="formbutton" value="Submit and comment next" type="button" onclick="go_submit(1)">	
	            			<?php } ?>
	            									
							<input name="cancel" class="formsubbutton" value="<?=$Lang['ePost']['Button']['Cancel']?>" type="button" onclick="go_cancel()">
	          			</div>
	        		</div>
	        	</div>
	        </div>  
			<div class="main_board_bottom_left">
				<div class="main_board_bottom_right"></div>
	  		</div>
		</div>
		<input type="hidden" id="RecordID" name="RecordID" value="<?=$RecordID?>"/>
		<input type="hidden" id="NextRecordID" name="NextRecordID" value="<?=$NextRecordID?>"/>
		<input type="hidden" id="RequestID" name="RequestID" value="<?=$RequestID?>"/>
		<input type="hidden" id="IsMarkNext" name="IsMarkNext" value="0"/>
		<input type="hidden" id="pageNo" name="pageNo" value="<?=$pageNo?>"/>
		<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$numPerPage?>"/>
	</form>
	</div></div></div>
</body>

<?php
include($intranet_root.'/home/ePost/templates/portal_foot.php');
?>