<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$RequestID = isset($RequestID) && $RequestID!=''? $RequestID : '';

# Initialize Object
$ePost = new libepost();

# Check if ePost should be accessible
$ePost->portal_auth();
$result = 0;
if($RequestID){
	$sql = "UPDATE
				EPOST_NONCOURSEWARE_REQUEST
			SET
				Status = ".$cfg_ePost['BigNewspaper']['NonCoursewareRequest_status']['deleted'].",
				ModifiedBy = '$UserID',
				ModifiedDate = NOW()
			WHERE
				RequestID = $RequestID";
	$result = $ePost->db_db_query($sql);
	$sql = "UPDATE
				EPOST_WRITING 
			SET
				Status = ".$cfg_ePost['sql_config']['Status']['deleted'].",
				ModifiedBy = '$UserID',
				modified = NOW()
			WHERE
				RequestID = $RequestID";
	$result = $ePost->db_db_query($sql);	
}
$returnMsg = $result?"DeleteSuccess":"DeleteUnsuccess";
header('location:index.php?returnMsg='.$returnMsg);

intranet_closedb();
?>