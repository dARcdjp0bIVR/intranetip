<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$RecordID   = isset($RecordID) && $RecordID!=''? $RecordID : '';
$Separator  = isset($Separator) && $Separator? $Separator : '|=|';

# Initialize Object
$ePostUI 	  = new libepost_ui();
$ePostWriter  = new libepost_writer($RecordID);

# Check if ePost should be accessible
$ePostUI->portal_auth();
$ePostUI->attachment_format = ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")?'mp4':'flv';
# Initialize $para_ary for $ePostUI->gen_article_display();
$para_ary['Title'] 	 = $ePostWriter->Title;
$para_ary['Content'] = nl2br($ePostWriter->Content);

$para_ary['AttachmentAry'] = array();
foreach($ePostWriter->Attachment as $_attachmentId => $_attachmentPathAry){
	$_attachment = $_attachmentPathAry['attachment'];
	$_caption = $_attachmentPathAry['caption'];
	$para_ary['AttachmentAry'][$_attachmentId] = array(
									'Original'	=>	$_attachment,
									'FilePath'	=>	$ePostWriter->format_attachment_path($_attachment,false),
									'HTTPPath'	=>	$ePostWriter->format_attachment_path($_attachment,true),
									'Caption'	=>	$_caption);
}
$article_display_ary = $ePostUI->gen_article_display($para_ary);

$result = $article_display_ary['content'].$Separator.$article_display_ary['attachment'].$Separator.$article_display_ary['suggest_height'];

echo $result;
?>