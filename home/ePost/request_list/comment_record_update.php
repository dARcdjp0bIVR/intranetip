<?php
# using : 

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_writer.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$RecordID = isset($RecordID) && $RecordID!=''? $RecordID : 0;

# Initialize Object
$ePost 		 = new libepost();
$ePostWriter = new libepost_writer($RecordID);

# Check if ePost should be accessible
$ePost->portal_auth();

if($ePostWriter->IsRedo==$cfg_ePost['sql_config']['IsRedo']['true']){
	if($ePostWriter->Status==$cfg_ePost['sql_config']['Status']['submitted']){
		switch($IsRedo){
			case 1: // Redo Again
				$Status = $cfg_ePost['sql_config']['Status']['drafted'];
				$IsRedo = $cfg_ePost['sql_config']['IsRedo']['true'];
				$InNewspaper_sql = "InNewspaper = NULL,";
			break;
			case 2: // Accept Redo
				$Status = $cfg_ePost['sql_config']['Status']['submitted'];
				$IsRedo = $cfg_ePost['sql_config']['IsRedo']['false'];
				$InNewspaper_sql = $InNewspaper? "InNewspaper = NOW()," : "InNewspaper = NULL,";
			break;
			case 3: // Decide Later
				$Status = $cfg_ePost['sql_config']['Status']['submitted'];
				$IsRedo = $cfg_ePost['sql_config']['IsRedo']['true'];
				$InNewspaper_sql = "InNewspaper = NULL,";
			break;
		}
	}
	else{
		$Status = $cfg_ePost['sql_config']['Status']['drafted'];
		$IsRedo = $cfg_ePost['sql_config']['IsRedo']['true'];
		$InNewspaper_sql = "InNewspaper = NULL,";
	}
}
else{
	if($IsRedo==$cfg_ePost['sql_config']['IsRedo']['true']){
		$Status = $cfg_ePost['sql_config']['Status']['drafted'];
		$IsRedo = $cfg_ePost['sql_config']['IsRedo']['true'];
		$InNewspaper_sql = "InNewspaper = NULL,";
	}
	else{
		$Status = $cfg_ePost['sql_config']['Status']['submitted'];
		
		if($InNewspaper)
			$InNewspaper_sql = !$ePostWriter->InNewspaper? "InNewspaper = NOW()," : "";
		else
			$InNewspaper_sql = "InNewspaper = NULL,";
	}
}
$InNewspaper_sql = ($ePostWriter->InNewspaper)?'':$InNewspaper_sql;
$sql = "UPDATE
			EPOST_WRITING
		SET
			Status = '$Status',
			IsRedo = '$IsRedo',
			".($ePost->user_obj->isTeacherStaff() && $ePost->is_editor? "TeacherComment = '$TeacherComment',":"")."
			InShowBoard = NULL,
			$InNewspaper_sql
			markdate = NOW(),
			ModifiedBy = '$UserID',
			modified = NOW()
		WHERE
			RecordID = '$RecordID'";
$result = $ePostWriter->db_db_query($sql);

# Update DB table KW_NEWS_ARTICLE_SHELF
if($InNewspaper){
	# Check if this article is in article shelf or not
	$sql = "SELECT COUNT(*) FROM EPOST_ARTICLE_SHELF WHERE WritingID = '$RecordID' AND Status = '".$cfg_ePost['BigNewspaper']['ArticleShelf_status']['exist']."'";
	$article_cnt = current($ePostWriter->returnVector($sql));
	
	if($article_cnt==0){
		$sql = "SELECT
					LevelCode, ModuleCode, ThemeCode,
					Title, Content
				FROM
					EPOST_WRITING
				WHERE
					RecordID = '$RecordID'";
		list($LevelCode, $ModuleCode, $ThemeCode, $Title, $Content) = current($ePostWriter->returnArray($sql));
		
		$sql = "INSERT INTO
					EPOST_ARTICLE_SHELF
					(WritingID, Title, Content, Status, InputDate, InputBy, ModifiedBy, ModifiedDate)
				VALUES
					('$RecordID', '".$ePostWriter->Get_Safe_Sql_Query($Title)."', '".$ePostWriter->Get_Safe_Sql_Query($Content)."', '".$cfg_ePost['BigNewspaper']['ArticleShelf_status']['exist']."', NOW(), '$UserID', '$UserID', NOW())";
		$ePostWriter->db_db_query($sql);
		$ArticleShelfID = $ePostWriter->db_insert_id();
		
		$sql = "UPDATE
					EPOST_WRITING_ATTACHMENT
				SET
					ArticleShelfID = '".$ArticleShelfID."',
					ArticleShelfCaption = Caption,
					ModifiedBy = '".$UserID."',
					ModifiedDate = NOW()
				WHERE
					WritingID = '".$RecordID."'";
		$ePostWriter->db_db_query($sql);		
	}
}
$returnMsg = $result?"UpdateSuccess":"UpdateUnsuccess";
intranet_closedb();
header('Location: request_entries_list.php?RequestID='.$ePostWriter->RequestID.'&pageNo='.$pageNo.'&numPerPage='.$numPerPage.'&returnMsg='.$returnMsg);
?>
<!--script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery-1.3.2.min.js"></script>
<script>
	$(document).ready(function(){
		parent.window.document.form1.submit();
		parent.window.tb_remove();
	});
</script-->