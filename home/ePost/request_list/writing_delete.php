<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$WritingID = isset($WritingID) && $WritingID!=''? $WritingID : '';
$RequestID = isset($RequestID) && $RequestID!=''? $RequestID : '';
# Initialize Object
$ePost = new libepost();

# Check if ePost should be accessible
$ePost->portal_auth();
$result=0;
if($ePost->is_editor&&$ePost->user_obj->isTeacherStaff()&&!empty($WritingID)){
	$sql = "SELECT COUNT(*) FROM EPOST_ARTICLE_SHELF WHERE WritingID = '".$WritingID."'";
	$cnt = current($ePost->returnVector($sql));
	if($cnt==0){
		$sql = "UPDATE
					EPOST_WRITING 
				SET
					Status = ".$cfg_ePost['sql_config']['Status']['deleted'].",
					ModifiedBy = '$UserID',
					modified = NOW()
				WHERE
					RecordID = $WritingID";
		$result = $ePost->db_db_query($sql);
	}
}
$returnMsg = $result?"UpdateSuccess":"UpdateUnsuccess";
header('location:request_entries_list.php?RequestID='.$RequestID.'&returnMsg='.$returnMsg);

intranet_closedb();
?>