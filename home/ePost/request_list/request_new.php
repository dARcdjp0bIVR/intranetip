<?php
# using :

$PATH_WRT_ROOT = "../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_request.php");
include_once($PATH_WRT_ROOT."includes/ePost/libepost_studenteditor.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$RequestID = isset($RequestID) && $RequestID!=''? $RequestID : 0;

# Initialize Object
$li = new interface_html();
$ePost = new libepost();
$ePostUI = new libepost_ui();
$ePostStudentEditors = new libepost_studenteditors();
$Header='NEW';
$FromPage = 'editor';
# Check if ePost should be accessible
$ePost->portal_auth();

if($RequestID){
	$ePostRequest = new libepost_request($RequestID);
}

# Initialize Navigation Array
$nav_ary[] = array("title"=>$Lang['ePost']['EditorPage'], "link"=>"../editor.php");
$nav_ary[] = array("title"=>($RequestID? $Lang['ePost']['EditRequest']:$Lang['ePost']['NewRequest']), "link"=>"");
if($RequestID) $nav_ary[] = array("title"=>$ePostRequest->Topic, "link"=>""); 

include($intranet_root.'/home/ePost/request_list/templates/tpl_request_new.php');

intranet_closedb();
?>