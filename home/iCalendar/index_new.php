<?php
// page modifing by: 
$PathRelative = "../../../";
$CurSubFunction = "iCalendar";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}

AuthPage(array("Communication-Calendar-GeneralUsage"));

intranet_opendb();
#include_once($PathRelative."src/include/template/general_header.php");
include_once($PathRelative."src/include/class/icalendar.php");

$iCal = new icalendar();
$lui = new UserInterface();

$calViewPeriodArr = array("daily","weekly","monthly");
if (!isset($calViewPeriod) || !in_array($calViewPeriod, $calViewPeriodArr))
	$calViewPeriod = $iCal->systemSettings["PreferredView"];
if ($calViewPeriod == "")
	$calViewPeriod = "monthly";

if (isset($time) && is_numeric($time)) {
	$year = date('Y', $time);
	$month = date('m', $time);
	$day = date('d', $time);
} else if (!isset($year) && !isset($month)){
	$time = time();
	$year = date('Y');
	$month = date('m');
	$day = date('d');
}

# Insert a default calendar if user didn't have any calendar
if (!isset($_SESSION["iCalHaveCal"]) ||  $_SESSION["iCalHaveCal"]!= 1) {
	$iCal->insertMyCalendar();
}

# Record the display view in session during each visit to this page
$_SESSION["iCalDisplayPeriod"] = $calViewPeriod;
$_SESSION["iCalDisplayTime"] = $time;

# Pre-load the events for quicker access
if ($calViewPeriod=="monthly") {
	$lower_bound = mktime(0,0,0,$month,1,$year);
	$upper_bound = mktime(0,0,0,$month+1,1,$year);
	$events = $iCal->query_events($lower_bound, $upper_bound);
} else if ($calViewPeriod=="weekly") {
	$current_week = $iCal->get_week($year, $month, $day, "Y-m-d H:i:s");
	$lower_bound = $iCal->sqlDatetimeToTimestamp($current_week[0]);
	$upper_bound = $iCal->sqlDatetimeToTimestamp($current_week[6]);
	$upper_bound = $upper_bound + SECONDINADAY;
	$events = $iCal->query_events($lower_bound, $upper_bound);	
} else if ($calViewPeriod=="daily") {
	$lower_bound = mktime(0,0,0,$month,$day,$year);
	$upper_bound = $lower_bound+SECONDINADAY;
	$events = $iCal->query_events($lower_bound, $upper_bound);
}

$selectTime = "<select class=\"tabletext\" name=\"eventHr\">";
//for ($i=0; $i<24; $i++) {
for ($i=$iCal->systemSettings["WorkingHoursStart"]; $i<$iCal->systemSettings["WorkingHoursEnd"]+1; $i++) {
	$selectTime .= ((int)date("H")==$i) ? "<option value=\"$i\" selected>$i</option>" : "<option value=\"$i\">$i</option>\n";
}
$selectTime .= "</select>";

$interval = 5;
## start time in minutes
$start_min .= "<select class=\"tabletext\" name=\"eventMin\">";
for ($i=0; $i<(60/$interval); $i++) {
	$min_value = $i * $interval;
	$min_value = sprintf("%02d",$min_value);
	$start_min .= "<option value=\"{$min_value}\">{$min_value}</option>";
}
$start_min .= "</select>";

## duration in minutes
$duration_min = "<select class=\"tabletext\" name=\"durationMin\">";
for ($i=0; $i<(60/$interval); $i++) {
	$min_value = $i * $interval;
	if ($intranet_session_language=="en" && $min_value>0) {
		$min_text = $iCalendar_NewEvent_DurationMins;
	} else {
		$min_text = $iCalendar_NewEvent_DurationMin;
	}
	$duration_min .= "<option value=\"{$min_value}\">{$min_value} {$min_text}</option>";
}
$duration_min .= "</select>";

$MODULE_OBJ["title"] = $iCalendar_Main_Title;
$MODULE_OBJ["logo"] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_icalendar.gif";

$customLeftMenu = $iCal->printLeftMenu();
#$topNavigationMenu = $iCal->printTopNavigationMenu($iCalendar_ManageCalendar_Setting);

$topNavigationMenu  = "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
$topNavigationMenu .= "<tr><td>";
if ($calViewPeriod == "monthly")
	$topNavigationMenu .= $iCal->get_navigation("monthly", $year, $month);
else if ($calViewPeriod == "weekly")
	$topNavigationMenu .= $iCal->get_navigation("weekly", $year, $month, $day);
else if ($calViewPeriod == "daily")
	$topNavigationMenu .= $iCal->get_navigation("daily", $year, $month, $day);
$topNavigationMenu .= "</td>";

$topNavigationMenu .= "<td><div id='sysMsg'></div></td>";

$topNavigationMenu .= "<td align='right' style='vertical-align:top;*padding-top:5px;*padding-right:10px;'>";
if ($calViewPeriod=="daily") {
	$dailyTabCell = "<td id='dailyTabTd' style='background-color:#FFFFFF' class='topNavigationTabOn'>".ucfirst($iCalendar_NewEvent_DurationDay)."</td>";
	$weeklyTabCell = "<td id='weeklyTabTd' style='background-color:#E7E6E6' class='tabletext'><a href='javascript:changePeriodTab(\"weekly\")' class='topNavigationTabOff'>".ucfirst($iCalendar_NewEvent_DurationWeek)."</a></td>";
	$monthlyTabCell = "<td id='monthlyTabTd' style='background-color:#E7E6E6' class='tabletext'><a href='javascript:changePeriodTab(\"monthly\")' class='topNavigationTabOff'>".ucfirst($iCalendar_NewEvent_DurationMonth)."</a></td>";
} else if ($calViewPeriod=="weekly") {
	$dailyTabCell = "<td id='dailyTabTd' style='background-color:#E7E6E6' class='tabletext'><a href='javascript:changePeriodTab(\"daily\")' class='topNavigationTabOff'>".ucfirst($iCalendar_NewEvent_DurationDay)."</td>";
	$weeklyTabCell = "<td id='weeklyTabTd' style='background-color:#FFFFFF' class='topNavigationTabOn'>".ucfirst($iCalendar_NewEvent_DurationWeek)."</td>";
	$monthlyTabCell = "<td id='monthlyTabTd' style='background-color:#E7E6E6' class='tabletext'><a href='javascript:changePeriodTab(\"monthly\")' class='topNavigationTabOff'>".ucfirst($iCalendar_NewEvent_DurationMonth)."</a></td>";
} else if ($calViewPeriod=="monthly") {
	$dailyTabCell = "<td id='dailyTabTd' style='background-color:#E7E6E6' class='tabletext'><a href='javascript:changePeriodTab(\"daily\")' class='topNavigationTabOff'>".ucfirst($iCalendar_NewEvent_DurationDay)."</td>";
	$weeklyTabCell = "<td id='weeklyTabTd' style='background-color:#E7E6E6' class='tabletext'><a href='javascript:changePeriodTab(\"weekly\")' class='topNavigationTabOff'>".ucfirst($iCalendar_NewEvent_DurationWeek)."</a></td>";
	$monthlyTabCell = "<td id='monthlyTabTd' style='background-color:#FFFFFF' class='topNavigationTabOn'>".ucfirst($iCalendar_NewEvent_DurationMonth)."</td>";
}

$topNavigationMenu .= "<table height='23' cellspacing='0' cellpadding='0' border='0' style='margin:0;'>
						<tbody>
							<tr>
								<td>
									<table width='100%' cellspacing='0' cellpadding='0' border='0'>
										<tbody>
											<tr>
												<td width='8'><img width='8' height='23' src='".$PATH_WRT_ROOT."".$PATH_WRT_ROOT."images/2007a/iCalendar/tab_".($calViewPeriod=="daily"?"on":"off")."_left.gif'/></td>
												$dailyTabCell
												<td width='8'><img width='8' height='23' src='".$PATH_WRT_ROOT."".$PATH_WRT_ROOT."images/2007a/iCalendar/tab_".($calViewPeriod=="daily"?"on":"off")."_right.gif'/></td>
											</tr>
										</tbody>
									</table>
								</td>
								<td width='5'><img width='5' height='10' src='".$PATH_WRT_ROOT."images/2007a/10x10.gif'/></td>
								<td>
									<table width='100%' cellspacing='0' cellpadding='0' border='0'>
										<tbody>
											<tr>
												<td width='8'><img width='8' height='23' src='".$PATH_WRT_ROOT."".$PATH_WRT_ROOT."images/2007a/iCalendar/tab_".($calViewPeriod=="weekly"?"on":"off")."_left.gif'/></td>
												$weeklyTabCell
												<td width='8'><img width='8' height='23' src='".$PATH_WRT_ROOT."".$PATH_WRT_ROOT."images/2007a/iCalendar/tab_".($calViewPeriod=="weekly"?"on":"off")."_right.gif'/></td>
											</tr>
										</tbody>
									</table>
								</td>
								<td width='5'><img width='5' height='10' src='".$PATH_WRT_ROOT."images/2007a/10x10.gif'/></td>
								<td>
									<table width='100%' cellspacing='0' cellpadding='0' border='0'>
										<tbody>
											<tr>
												<td width='8'><img width='8' height='23' src='".$PATH_WRT_ROOT."".$PATH_WRT_ROOT."images/2007a/iCalendar/tab_".($calViewPeriod=="monthly"?"on":"off")."_left.gif'/></td>
												$monthlyTabCell
												<td width='8'><img width='8' height='23' src='".$PATH_WRT_ROOT."".$PATH_WRT_ROOT."images/2007a/iCalendar/tab_".($calViewPeriod=="monthly"?"on":"off")."_right.gif'/></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>";
$topNavigationMenu .= "</td></tr></table>";

$TAGS_OBJ[] = array("<div style='margin:42px 0px -15px 10px;'>$topNavigationMenu</div>", "", 0);

# Choose CSS style sheet
$styleSheet = "css/display_calendar.css";
?>
<link rel="stylesheet" href="css/main.css" type="text/css" />
<link rel="stylesheet" href="<?=$styleSheet?>" type="text/css" />
<?php
# Start outputing the layout after CSS is loaded to prevent "FOUC"
include_once($PathRelative."src/include/template/general_header.php");
?>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/interface.js"></script>
<script type="text/javascript" src="js/jquery.blockUI.js"></script>
<script type="text/javascript" src="js/dimensions.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<!--<script type="text/javascript" src="js/jquery.corner.js"></script>-->
<!-- ClueTip require files begin -->
<script type="text/javascript" src="cluetip/jquery.hoverIntent.js"></script>
<script type="text/javascript" src="cluetip/jquery.cluetip.js"></script>
<style type="text/css">@import url(cluetip/jquery.cluetip.css);</style>
<!-- ClueTip require files end -->
<script type="text/javascript" src="js/jquery-calendar.js"></script>
<style type="text/css">@import url(css/jquery-calendar.css);</style>
<script language="javascript">
// Fix for IE6 image flicker (cannot solve the case of ":hover")
try {
  document.execCommand("BackgroundImageCache", false, true);
} catch(err) {}

$.validator.addMethod("yyyymmdd", function(value) {
	return /(19[0-9][0-9]|20[0-9][0-9])-([1-9]|0[1-9]|1[012])-([1-9]|0[1-9]|[12][0-9]|3[01])/.test(value);
}, '&nbsp;*<?=$iCalendar_QuickAdd_InvalidDate?>');

var isBlocking = false;

function validateForm() {
	var v = $("#quickAddEvent_form").validate({
		rules: {
			title: "required",
			eventDate: {
				required: true,
				yyyymmdd: true,
				minLength: 8,
				maxLength: 10
			}
		},
		messages: {
			title: "&nbsp;*<?=$iCalendar_QuickAdd_Required?>",
			eventDate: {
				required: "&nbsp;*<?=$iCalendar_QuickAdd_Required?>",
				minLength: "&nbsp;*<?=$iCalendar_QuickAdd_InvalidDate?>",
				maxLength: "&nbsp;*<?=$iCalendar_QuickAdd_InvalidDate?>"
			}
		}
	});
	
	if(v.form()){
		$("#quickAddEvent").fadeOut();
		popUpCal.hideCalendar(popUpCal._curInst, '');
		var blockMsg = "<h2 style='padding-top:10px;'><?=$i_campusmail_processing?></h2>";
		$('div#content').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
		isBlocking = true;
		$("#quickAddEvent_form").ajaxSubmit({
			url: "new_event_update.php",
			success: function() {
				$("#quickAddEvent_form").resetForm();
				getCalendarDisplay($('input[@name=calViewPeriod]').val());
			}
		});
		//document.quickAddEvent_form.submit();
	} else {
		v.focusInvalid();
		return false;
	}
}

// wrap the code required to inititate the calendar
function initCalendar() {
	// create tooltips for DIV with class name: event, involoveEvent & schoolEvent
	$('div[@id^="event"], div[@id^="involveEvent"], div[@id^="schoolEvent"]').cluetip({local:true,cursor:'pointer', positionBy:'mouse', cluetipClass:'jtip', arrows:true, dropShadow:false, showTitle:false});
	
	// clickable table cell as a quick link to add new event
	// * disable(unbind) it when mouseover an event DIV, enable(bind) it when mouseout
	var period = $("input[@name='calViewPeriod']").val();
	createClickCell(period);
}

function createClickCell(period) {
	// clickable table cell as a quick link to add new event
	// disable(unbind) it when mouseover an event DIV, enable(bind) it when mouseout
	if (period == "monthly") {
		$(".monthCell, .monthTodayCell").click(function() {
			window.location = "new_event.php?calDate="+$(this).attr("id");
		});
		
		$(".monthCell>div, .monthTodayCell>div").bind("mouseover", function() {
			$(this).parent().unbind("click");
		});
		
		$(".monthCell>div, .monthTodayCell>div").bind("mouseout", function() {
			$(this).parent().click(function(){window.location = "new_event.php?calDate="+$(this).attr("id");});
		});
	} else if (period == "weekly") {
		$(".weekCell, .weekCellToday, .weekFirstCell, .weekFirstCellToday, .weekLastCell, .weekLastCellToday").click(function()
		{
			window.location = "new_event.php?calDate="+$(this).attr("id");
		});
		$(".weekCell>div, .weekCellToday>div, .weekFirstCell>div, .weekFirstCellToday>div, .weekLastCell>div, .weekLastCellToday>div").bind("mouseover", function()
		{
			$(this).parent().unbind("click");
			
		});
		$(".weekCell>div, .weekCellToday>div, .weekFirstCell>div, .weekFirstCellToday>div, .weekLastCell>div, .weekLastCellToday>div").bind("mouseout", function()
		{
			$(this).parent().click(function(){window.location = "new_event.php?calDate="+$(this).attr("id");});
			
		});
	} else if (period == "daily") {
		$(".dayCell, .dayCellToday, .dayFirstCell, .dayFirstCellToday, .dayLastCell, .dayLastCellToday").click(function()
		{
			window.location = "new_event.php?calDate="+$(this).attr("id");
		});
		$(".dayCell>div, .dayCellToday>div, .dayFirstCell>div, .dayFirstCellToday>div, .dayLastCell>div, .dayLastCellToday>div").bind("mouseover", function()
		{
			$(this).parent().unbind("click");
			
		});
		$(".dayCell>div, .dayCellToday>div, .dayFirstCell>div, .dayFirstCellToday>div, .dayLastCell>div, .dayLastCellToday>div").bind("mouseout", function()
		{
			$(this).parent().click(function(){window.location = "new_event.php?calDate="+$(this).attr("id");});
			
		});
	}
}

$(document).ready( function() {
	// temporary fix for removal of the unwanted empty space on the bottom of the page
	$("#eclass_main_frame_table").removeAttr("height");
	
	// get the position (bottom-left) of quickAddEvent_button link to display the quick add dialog
	// **depends on the jQuery Dimension plugins
	var buttonBottom = $('#quickAddEvent_button').position().top + $('#quickAddEvent_button').height() + 5;
	var buttonLeft = $('#quickAddEvent_button').position().left;
	
	$("#quickAddEvent").hide();
	
	$('#quickAddEvent').Draggable( 
		{
			zIndex: 10,
			ghosting: false,
			opacity: 0.7,
			handle: '#quickAddEvent_handle'
		}
	);
	
	$('#quickAddEvent_form').ajaxForm();
	
	$('#quickAddEvent_button').click(function() 
	{
		$("#quickAddEvent").css("top",buttonBottom);
		$("#quickAddEvent").css("left",buttonLeft);
		// show(), slideUp(), slideDown(), slideToggle(), fadeIn(), fadeOut() can also be used
		$("#quickAddEvent").fadeIn();
		$("input[@name='title']").focus();
		return false;
	});
	
	$('#quickAddEvent_close').click(function() 
	{
		$("#quickAddEvent").fadeOut();
		popUpCal.hideCalendar(popUpCal._curInst, '');
		return false;
	});
	
	$('#quickAddSubmit').click(function() {
		validateForm();
	});
	
	// jQuery calendar widget
<?php
	if ($intranet_session_language == "b5") {
?>
	popUpCal.regional['zh-tw'] = {clearText: '清除', closeText: '關閉',
		prevText: '&lt;上月', nextText: '下月&gt;', currentText: '今天',
		dayNames: ['日','一','二','三','四','五','六'],
		monthNames: ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
		dateFormat: 'YMD-'};
	popUpCal.setDefaults(popUpCal.regional['zh-tw']);
<? } ?>
	popUpCal.setDefaults({
		autoPopUp: 'focus', 
		buttonImageOnly: true, 
		dateFormat: 'YMD-',
		buttonImage: 'images/icon_calendar.gif', 
		buttonText: 'Calendar', 
		speed: '',
		clearText: ''
	});
	$('#selectDisplayDate').calendar({onSelect: changeDate});
	$('input[@name="eventDate"]').calendar();
	
	// bind the show calendar trigger to #chooseDateLink
	$('#chooseDateLink').click(function() 
	{
		popUpCal.showFor($('#selectDisplayDate')[0]);
		return false;
	});
	
	initCalendar();
});

function changeDate(date) {
	datePiece = date.split("-");
	year = datePiece[0];
	month = datePiece[1] - 1;
	day = datePiece[2];
	newDate = new Date(year,month,day);
	newTime = (newDate.getTime())/1000;
	$("input[@name='time']").val(newTime);
	getCalendarDisplay($('input[@name=calViewPeriod]').val());
}

function toggleInvolveEventVisibility(){
	$(".involve").toggle();
	
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: 'involve'
	}, function() {
	    // callback function
	});
}

function toggleSchoolEventVisibility(){
	$('.schoolEventEntry, .academicEventEntry, .holidayEventEntry, .groupEventEntry').toggle();
	
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: 'school'
	}, function() {
	    // callback function
	});
}

function toggleCalVisibility(parClassID){
	$('.cal-'+parClassID).toggle();
	
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: parClassID
	}, function() {
	    // callback function
	});
}

function enableTimeSelect() {
	if ($('input[@name="isAllDay"]').attr('checked')) {
		$('#eventTimeDiv').hide();
	} else {
		$('#eventTimeDiv').show();
	}
}

function submitQuickAddEvent() {
	$("form")[0].submit();
}

function openPrintPage() {
	var ts = $("input[@name='time']").val();
	var period = $("input[@name='calViewPeriod']").val();
	newWindow("print.php?time="+ts+"&calViewPeriod="+period,10);
}

function toggleTab(period, onOff) {
	var tabOnleftImg = "<?=$PATH_WRT_ROOT.$PATH_WRT_ROOT?>images/2007a/iCalendar/tab_on_left.gif";
	var tabOnRightImg = "<?=$PATH_WRT_ROOT.$PATH_WRT_ROOT?>images/2007a/iCalendar/tab_on_right.gif";
	var tabOffleftImg = "<?=$PATH_WRT_ROOT.$PATH_WRT_ROOT?>images/2007a/iCalendar/tab_off_left.gif";
	var tabOffRightImg = "<?=$PATH_WRT_ROOT.$PATH_WRT_ROOT?>images/2007a/iCalendar/tab_off_right.gif";
	
	if (period == "monthly") {
		tab = $("#monthlyTabTd");
		txt = "<?=ucfirst($iCalendar_NewEvent_DurationMonth)?>";
	} else if (period == "weekly") {
		tab = $("#weeklyTabTd");
		txt = "<?=ucfirst($iCalendar_NewEvent_DurationWeek)?>";
	} else if (period == "daily") {
		tab = $("#dailyTabTd");
		txt = "<?=ucfirst($iCalendar_NewEvent_DurationDay)?>";
	}
	tab.empty();
	if (onOff == "on") {
		tab.attr("style", "background-color:#FFFFFF");
		tab.attr("class", "topNavigationTabOn");
		tab.html(txt);
		
		tab.prev().children().attr("src", tabOnleftImg);
		tab.next().children().attr("src", tabOnRightImg);
	} else if (onOff == "off") { 
		tab.attr("style", "background-color:#E7E6E6");
		tab.attr("class", "tabletext");
		tab.html("<a href='javascript:changePeriodTab(\""+period+"\")' class='topNavigationTabOff'>"+txt+"</a>");
		
		tab.prev().children().attr("src", tabOffleftImg);
		tab.next().children().attr("src", tabOffRightImg);
	}
}

function changePeriodTab(period) {
	getCalendarDisplay(period);
	if (period == "monthly") {
		toggleTab("daily", "off");
		toggleTab("weekly", "off");
		toggleTab("monthly", "on");
		$("input[@name='calViewPeriod']").val("monthly");
	} else if (period == "weekly") {
		toggleTab("daily", "off");
		toggleTab("weekly", "on");
		toggleTab("monthly", "off");
		$("input[@name='calViewPeriod']").val("weekly");
	} else if (period == "daily") {
		toggleTab("daily", "on");
		toggleTab("weekly", "off");
		toggleTab("monthly", "off");
		$("input[@name='calViewPeriod']").val("daily");
	}
}

<?php
// create an JS array of month name
echo $iCal->createJsArray("monthName", $iCalendar_NewEvent_Repeats_MonthArray);

// create an JS array of weekday name
echo $iCal->createJsArray("weekdayName", $iCalendar_NewEvent_Repeats_WeekdayArray2);
?>

function getCalendarDisplay(period) {
	if (!isBlocking) {
		var blockMsg = "<h2 style='padding-top:10px;'><?=$i_campusmail_processing?></h2>";
		$('div#content').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
	}
	var ts = $("input[@name='time']").val();
	$("#calendar_wrapper").load(
		"get_calendar.php", 
	  {calViewPeriod: period, time: ts},
	  function() {
	  	var theDate = new Date(ts*1000);
	  	var nextDate = new Date(ts*1000);
	  	var lastDate = new Date(ts*1000);
	  	if (period == "monthly") {
	  		nextDate.setMonth(nextDate.getMonth()+1);
	  		lastDate.setMonth(lastDate.getMonth()-1);
	  		
	  		var newMonth = theDate.getMonth();
	  		var newMonthName = monthName[newMonth];
	  		var newYear = theDate.getFullYear();
	  		$("#periodTitle").html(newMonthName+" "+newYear);
	  		
	  		var nextMonthTs = (nextDate.getTime())/1000;
	  		$("#nextPeriod").attr("href", "javascript:document.form1.time.value='"+nextMonthTs+"';getCalendarDisplay('monthly');");
	  		
	  		var lastMonthTs = (lastDate.getTime())/1000;
	  		$("#lastPeriod").attr("href", "javascript:document.form1.time.value='"+lastMonthTs+"';getCalendarDisplay('monthly');");
	  	} else if (period == "weekly") {
	  		nextDate.setDate(nextDate.getDate()+7);
	  		lastDate.setDate(lastDate.getDate()-7);
	  		
	  		$("#periodTitle").html(getCurrentWeek(theDate));
	  		
	  		var nextWeekTs = (nextDate.getTime())/1000;
	  		$("#nextPeriod").attr("href", "javascript:document.form1.time.value='"+nextWeekTs+"';getCalendarDisplay('weekly');");
	  		
	  		var lastWeekTs = (lastDate.getTime())/1000;
	  		$("#lastPeriod").attr("href", "javascript:document.form1.time.value='"+lastWeekTs+"';getCalendarDisplay('weekly');");
	  	} else if (period == "daily") {
	  		nextDate.setDate(nextDate.getDate()+1);
	  		lastDate.setDate(lastDate.getDate()-1);
	  		
	  		var newWeekday = weekdayName[theDate.getDay()];
	  		
	  		$("#periodTitle").html(newWeekday+", "+theDate.getDate()+"-"+(theDate.getMonth()+1)+"-"+theDate.getFullYear());
	  		
	  		var nextDayTs = (nextDate.getTime())/1000;
	  		$("#nextPeriod").attr("href", "javascript:document.form1.time.value='"+nextDayTs+"';getCalendarDisplay('daily');");
	  		
	  		var lastDayTs = (lastDate.getTime())/1000;
	  		$("#lastPeriod").attr("href", "javascript:document.form1.time.value='"+lastDayTs+"';getCalendarDisplay('daily');");
	  	}
	  	initCalendar();
	  	$('div#content').unblock();
	  	isBlocking = false;
	  }
	);
}

function getCurrentWeek(theDate) {
	var day = theDate.getDate();
	var month = theDate.getMonth() + 1;
	var year = theDate.getFullYear();
	var shortYear = theDate.getYear();
	var offset = theDate.getDay();
	var week;
	
	if(offset != 0) {
		day = day - offset;
		if ( day < 1) {
			if (month==1 || month==2 || month==4 || month==6 || month==8 || month==9 || month==11) {
				day = 31 + day;
			}
			if (month == 3) {
				// Calculate leap year
				if (((year%4 == 0) && (year%100 != 0)) || (year%400 == 0)) {
					day = 29 + day;
				} else {
					day = 28 + day;
				}
			}
			if (month==5 || month==7 || month==10 || month==12) {
				day = 30 + day;
			}
			if (month == 1) {
				month = 12;
				year = year - 1;
			} else {
				month = month - 1;
			}
		}
	}
	firstWeeday = monthName[month-1] + " " + day + ", " + year;
	
	lastWeekday = new Date(year, month-1, day, 0, 0, 0);
	lastWeekday.setDate(lastWeekday.getDate()+6);
	lastWeekday = monthName[lastWeekday.getMonth()] + " " + lastWeekday.getDate() + ", " + lastWeekday.getFullYear();
	
	return firstWeeday + " - " + lastWeekday;
}

<?php
	# Check if periodic Ajax call is required to get Popup reminder
	# Look for remainder in the next 12 hours
	echo $iCal->haveFutureReminder(12);
?>

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->

</script>
<pre>
<?php
	/*
	# Debug info - Display a list of selected events
	for($i=0;$i<sizeof($events);$i++) {
		echo $events[$i]["EventID"].", ";
		echo $events[$i]["RepeatID"].", ";
		echo $events[$i]["Title"].", ";
		echo $events[$i]["EventDate"].", ";
		echo $events[$i]["Duration"].", ";
		echo $events[$i]["CalID"].", ";
		echo $events[$i]["Permission"].", ";
		echo date("D", $iCal->sqlDatetimeToTimestamp($events[$i]["EventDate"]))."\n";
	}
	*/
?>
</pre>
<div id="quickAddEvent" style="display:none">
	<div id="quickAddEvent_handle">
		<a href="#" id="quickAddEvent_close"><img width="13" height="13" border="0" align="absmiddle" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('box_close','','images/layer_btn_close_on.gif',1)" id="box_close" name="box_close" src="images/layer_btn_close.gif"/></a>
		<span style="float:left; padding:2px; font-size:12px;">
			<img src="<?=$PATH_WRT_ROOT?>/images/2007a/icon_new.gif" width="18" height="18" border="0" align="absmiddle" />
			<?=$iCalendar_NewEvent_Header?>
		</span>
	</div>
	<div id="quickAddEvent_content">
		<form name="quickAddEvent_form" id="quickAddEvent_form" method="post" action="new_event_update.php">
			<table id="quickAddEvent_table" width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td width="60" align="left"><?= $iCalendar_NewEvent_Title ?></td>
					<td align="left"><input name="title" type="text" class="tabletext"></td>
				</tr>
				<tr>
					<td align="left"><?= $iCalendar_NewEvent_DateTime ?></td>
					<td align="left">
						<input name="eventDate" type="text" class="tabletext" value="<?=date("Y-m-d",$time)?>">
					</td>
				</tr>
				<tr>
					<td align="left">&nbsp;</td>
					<td align="left">
						<input type="checkbox" name="isAllDay" id="isAllDay" checked="checked" value="1" onClick="enableTimeSelect()" />
						<label for="isAllDay"><?=$iCalendar_NewEvent_IsAllDay?></label>
						<div id="eventTimeDiv" style="display:none;line-height:2.2;">
							<?=$iCalendar_NewEvent_StartTime?>
							<?= $selectTime ?>:<?= $start_min ?><br />
							<?=$iCalendar_NewEvent_Duration?>
							<select name="durationHr" class="tabletext">
								<option value="0">0 <?=$iCalendar_NewEvent_DurationHr?></option>
								<option value="1">1 <?=$iCalendar_NewEvent_DurationHr?></option>
								<?php for($i=2;$i<13;$i++) echo "<option value=\"$i\">$i $iCalendar_NewEvent_DurationHrs</option>\n"; ?>
							</select> 
							<?= $duration_min ?>
						</div>
					</td>
				</tr>
				<tr>
					<td align="left"><?= $iCalendar_NewEvent_Calendar ?></td>
					<td align="left">
						<?php
							$ownCalendar = $iCal->returnViewableCalendar(TRUE);
							if (sizeof($ownCalendar) == 1) {
								echo $ownCalendar[0]["Name"];
								echo "<input type='hidden' name='calID' value='".$ownCalendar[0]["CalID"]."' />";
							} else {
								echo "<select class='tabletext' name='calID' size='1'>";
								for ($i=0; $i<sizeof($ownCalendar); $i++) {
									echo "<option value='".$ownCalendar[$i]["CalID"]."'".(($ownCalendar[$i]["CalID"]==$calID)?" selected='yes'":"").">".$ownCalendar[$i]["Name"]."</option>\n";
								}
								echo "</select>";
							}
						?>
					</td>
				</tr>
				<tr>
					<td align="left"><?= $iCalendar_NewEvent_Description ?></td>
					<td align="left">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="left">
						<textarea class="tabletext" name="description" cols="40" rows="3"></textarea>
					</td>
				</tr>
				<tr>
					<td align="left" valign="middle">
						<a href="new_event.php" class="tablelink"><img src="<?=$PATH_WRT_ROOT?>/images/2007a/icon_edit.gif" width="12" height="12" border="0" align="absmiddle">Advance</a>
					</td>
					<td align="right">
						<?= $lui->Get_Input_Button($Lang['btn_add'], "quickAddSubmit") ?>
						<?= $lui->Get_Input_Button($Lang['btn_cancel'], "", "", "$('#quickAddEvent').fadeOut('fast');") ?>
					</td>
				</tr>
			</table>
		</form> 
	</div>
</div>

<div id="module_bulletin" class="module_content">
	<div id="module_content_header">
		<div id="module_content_header_title"><a href="icalendar.htm"> iCalendar</a></div>
		<div id="module_content_relate"><a href="#">related information</a></div>
	</div>
	<div id="ical_left">
		<div id="btn_link">
			<a href="icalendar_new_event_advance.htm"><img src="images/icon_add.gif" border="0" align="absmiddle">New Event</a> 
			<a href="#"><img src="images/icon_print.gif" border="0" align="absmiddle">Print</a>
		</div>
		<br style="clear:both">
		<div id="cal_left_act_event"> 
			<div class="cal_act_event_board"> 
				<span class="cal_event_board_01"><span class="cal_event_board_02"></span></span>
				<div class="cal_event_board_03">
					<div class="cal_event_board_04">
						<span class="portal_cal_active_event_title">
							<img src="images/icon_event.gif" width="22" height="22" align="absmiddle">Event
						</span>
						<br>
						<span class="cal_act_event_board_content">
							<a href="icalendar_act_event.htm" class="invite">Invitation(5)</a><br>
							<a href="#" class="notify">Notification(5)</a>
						</span>
					</div>
				</div>
				<span class="cal_event_board_05"><span class="cal_event_board_06"></span></span>
			</div>
		</div>
		<br style="clear:both">
		<div id="small_cal">
			<span class="small_cal_board_01"><span class="small_cal_board_02"></span></span>
			<div class="cal_board_content" style="padding-left:5px">
			
			</div>
			<span class="small_cal_board_05"><span class="small_cal_board_06"></span></span>
		</div>
		<div id="cal_list">
			<div id="cal_calendar_tab">
				<ul>
					<li id="current"><a href="#"><span>Calendar</span></a></li>
					<li><a href="icalendar_timetable.htm"><span>Timetable</span></a></li>
				</ul>
			</div>
		</div>	
	</div>
	<div id="ical_right">
		<br>
		<div id="caltabs">
			<ul>
				<li><a href="icalendar_agenda.htm"><span>Agenda</span></a></li>
				<li><a href="icalendar_day.htm"><span>Day</span></a></li>
				<li><a href="icalendar_week.htm"><span>Week</span></a></li>
				<li id="current"><a href="icalendar.htm"><span>Month</span></a></li>
			</ul>
		</div>
		<div id="cal_day_range">
			<a href="#"><img src="images/layout_grey/btn_prev_off.gif" width="18" height="18" border="0" align="absmiddle"></a>
			April 2008							
			<a href="#"><img src="images/layout_grey/btn_next_off.gif" width="18" height="18" border="0" align="absmiddle"></a>
		</div>					
		<div style="float:right">
			<a href="#" class="cal_select_today cal_select_today_selected">Today</a>
		</div>			
		<br style="clear:both">	
		<div id="cal_board">
			<div class="cal_board_content">
				<?=$iCal->getTopToolBar()?>
				<div id="content_area">
					<div id="calendar_wrapper">
						<?php
							if ($calViewPeriod == "monthly") {
								print $iCal->generateMonthlyCalendar($year, $month);
							} else if ($calViewPeriod == "weekly") {
								print $iCal->generateWeeklyCalendar($year, $month, $day);
							} else if ($calViewPeriod == "daily") {
								print $iCal->generateDailyCalendar($year, $month, $day);
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once($PathRelative."src/include/template/general_footer.php");
?>