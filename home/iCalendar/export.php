<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libicalendar.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$iCal = new libicalendar;
$linterface = new interface_html();

$MODULE_OBJ["title"] = $iCalendar_Main_Title;
$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_eAttendance.gif";

$customLeftMenu = $iCal->printLeftMenu();
$topNavigationMenu = $iCal->printTopNavigationMenu($i_Calendar_ExportCalendar);

$TAGS_OBJ[] = array("<div style='margin:43px 0px -0px 10px;'>$topNavigationMenu</div>", "", 0);

?>
<link rel="stylesheet" href="css/main.css" type="text/css" />
<?php
$linterface->LAYOUT_START();
?>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/interface.js"></script>
<script type="text/javascript" src="js/dimensions.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script language="javascript">
$.validator.addMethod("yyyymmdd", function(value) {
	return /(19[0-9][0-9]|20[0-9][0-9])-([1-9]|0[1-9]|1[012])-([1-9]|0[1-9]|[12][0-9]|3[01])/.test(value);
}, '<?=$iCalendar_QuickAdd_InvalidDate?>');

function validateForm() {
	if ($("[@name='dateRange']").fieldValue() == "select") {
		var v = $("[@name='form1']").validate({
			errorLabelContainer: $("#errMessageBox"),
			wrapper: "li",
			rules: {
				startDate: {
					required: true, yyyymmdd: true, minLength: 8, maxLength: 10
				},
				endDate: {
					required: true, yyyymmdd: true, minLength: 8, maxLength: 10
				}
			},
			messages: {
				startDate: {
					required: "<?=$iCalendar_QuickAdd_Required?>",
					minLength: "<?=$iCalendar_QuickAdd_InvalidDate?>",
					maxLength: "<?=$iCalendar_QuickAdd_InvalidDate?>"
				},
				endDate: {
					required: "<?=$iCalendar_QuickAdd_Required?>",
					minLength: "<?=$iCalendar_QuickAdd_InvalidDate?>",
					maxLength: "<?=$iCalendar_QuickAdd_InvalidDate?>"
				}
			}
		});
		
		if(v.form()){
			document.form1.submit();
		} else {
			v.focusInvalid();
			window.scrollTo(0,0);
			return false;
		}
	} else {
		document.form1.submit();
	}
}

$(document).ready( function() {
	$("#startDateRow, #endDateRow").hide();
	
	$("[@name='dateRange']").click(function() {
		if ($("[@name='dateRange']").fieldValue() == "select") {
			$("#startDateRow, #endDateRow").show();
		} else {
			$("#startDateRow, #endDateRow").hide();
		}
	});
	
	$("[@name='form1']").submit(function() {
		return validateForm();
	});
	
	$("#reset2").click(function() {
		$("#startDateRow, #endDateRow").hide();
	});
});

function toggleInvolveEventVisibility(){
	$(".involve").toggle();
}

function toggleSchoolEventVisibility() {
	$('.schoolEventEntry, .academicEventEntry, .holidayEventEntry, .groupEventEntry').toggle();
	resetTableRowClass();
}

function toggleCalVisibility(parClassID) {
	$('.cal-'+parClassID).toggle();
	resetTableRowClass();
	
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: parClassID
	}, function() {
	    // callback function
	});
}

<?php
	# Check if periodic Ajax call is required to get Popup reminder
	echo $iCal->haveFutureReminder(12);
?>
</script>
<br />
<ul id="errMessageBox"></ul>
<form name="form1" action="export_get.php" method="POST">
<div id="wrapper" style="padding-left:10px;">
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?= $i_Calendar_ImportCalendar_ChooseCalendar ?>
		</td>
		<td width="70%" class="tabletext">
			<?php
				$ownCalendar = $iCal->returnViewableCalendar(TRUE);
				if (sizeof($ownCalendar) == 1) {
					echo $ownCalendar[0]["Name"];
					echo "<input type='hidden' name='calID' value='".$ownCalendar[0]["CalID"]."' />";
				} else {
					echo "<select name='calID' size='1'>";
					for ($i=0; $i<sizeof($ownCalendar); $i++) {
						echo "<option value='".$ownCalendar[$i]["CalID"]."'".(($ownCalendar[$i]["CalID"]==$calID)?" selected='yes'":"").">".$ownCalendar[$i]["Name"]."</option>\n";
					}
					echo "</select>";
				}
			?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_Calendar_ExportCalendar_Range?>
		</td>
		<td width="70%" class="tabletext">
			<table border="0" cell-padding="0" cell-spacing="0">
				<tr>
					<td class="tabletext"><input type="radio" name="dateRange" id="dateRange1" value="today" checked="checked" /></td> 
					<td class="tabletext"><label for="dateRange1"><?=$i_Calendar_ExportCalendar_Onward?></label></td>
				</tr>
				<tr>
					<td class="tabletext"><input type="radio" name="dateRange" id="dateRange2" value="all" /></td> 
					<td class="tabletext"><label for="dateRange2"><?=$i_Calendar_ExportCalendar_All?></label></td>
				</tr>
				<tr>
					<td class="tabletext"><input type="radio" name="dateRange" id="dateRange3" value="select" /></td> 
					<td class="tabletext"><label for="dateRange3"><?=$i_Calendar_ExportCalendar_Select?></label></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr id="startDateRow" style="display:none;">
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?= $i_general_startdate ?>
		</td>
		<td width="70%" class="tabletext">
			<?=$linterface->GET_DATE_FIELD("startDate", "form1", "startDate", date("Y-m-d"))?>
		</td>
	</tr>
	<tr id="endDateRow" style="display:none;">
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?= $i_general_enddate ?>
		</td>
		<td width="70%" class="tabletext">
			<?=$linterface->GET_DATE_FIELD("endDate", "form1", "endDate", date("Y-m-d", time()+604800))?>
		</td>
	</tr>
</table>
</div>
<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
  	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton'") ?>
		</td>
	</tr>
</table>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>