<?php
// page modifing by: Jason
// exit;
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../";
$ImagePathAbs = "/theme/image_en/";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/icalendar_api.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
##########################################################################################################################

if (empty($_SESSION['UserID'])) {
	//header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	echo 'SESSION_EXPIRED';
	exit;
}

intranet_auth();
intranet_opendb();

## Use Library
$iCal = new icalendar();
$iCal_api = new icalendar_api();

## Get Post Data
$cal_type = (isset($cal_type) && $cal_type != '') ? $cal_type : '';
$color = (isset($color) && $color != '') ? $color : '2f75e9';

## preparation
$userID = $_SESSION["UserID"];
$otherCalTypeSetting = $iCal->Get_Other_CalType_Settings($cal_type);


$cal_type_name = '';
$isOtherCalTypeCal = false;
if(count($otherCalTypeSetting) > 0){ 
	for($i=0 ; $i<count($otherCalTypeSetting) ; $i++){
		if($otherCalTypeSetting[$i]['CalType'] == $cal_type){
			$isOtherCalTypeCal = true;
			$cal_type_name = $otherCalTypeSetting[$i]['CalTypeName'];
		}
	}
}

$calID = str_replace(array('<', '>', ';', 'javascript:', '"', '\'', '(', ')','=','-'),'',$value);


## Main
if (isset($calType) && ($calType == 1 || $calType > 3) ){
#external event
	$config = $iCal_api->getExternalCalConfig($calType, $calID);
	$sql = "Select Value from CALENDAR_USER_PREF 
			where UserID = ".$_SESSION["UserID"]." and 
			Setting = '".$config["setting"]."'
			";
	$visible = $iCal_api->returnVector($sql);
	$sql = "Update CALENDAR_USER_PREF set
			Value = '".($visible[0]==1?"0":"1")."'
			where UserID = ".$_SESSION["UserID"]." and 
			Setting = '".$config["setting"]."'";
	$iCal_api->db_db_query($sql);
}
elseif (isset($calID) && $calID != "") {
	$calID = trim($calID);
	/*if ($calID == "otherInvite"){
		$sql = "SELECT Value FROM CALENDAR_USER_PREF WHERE UserID = '$userID' AND Setting = 'otherInvitaionCalVisible'";
		$result = $iCal->returnVector($sql);
		
		if(count($result) > 0){
			$visible = ($result[0] == 1) ? 0 : 1;
			$sql  = "UPDATE CALENDAR_USER_PREF SET Value = '$visible' ";
			$sql .= "WHERE Setting='otherInvitaionCalVisible' AND UserID = '$userID'";
		} else {
			$sql = "INSERT INTO CALENDAR_USER_PREF (UserID, Setting, Value) VALUES ('$userID', 'otherInvitaionCalVisible', 0)";
		}
		$iCal->db_db_query($sql);
	}
	else if (is_numeric($calID) && $cal_type == 2)	# Foundation Calendar
	{
		$sql = "SELECT Visible FROM CALENDAR_CALENDAR_VIEWER WHERE CalID = '$calID' AND UserID = '$userID' AND CalType = 2";
		$result = $iCal->returnVector($sql);
		$visible = ($result[0] == 1) ? 0 : 1;
		
		if($result[0] != ''){
			$sql = "UPDATE CALENDAR_CALENDAR_VIEWER SET Visible = '$visible' 
					WHERE CalID = '$calID' AND UserID = '$userID' and CalType = 2";
		} else {
			$sql = "INSERT INTO CALENDAR_CALENDAR_VIEWER (CalID, UserID, Access, Color, Visible, CalType) 
					VALUES ('$calID', '$userID', 'R', '$color', '$visible', 2)";
		}
		$iCal->db_db_query($sql);
	}
	else*/ if(is_numeric($calID))
	{
		$sql = "SELECT Visible FROM CALENDAR_CALENDAR_VIEWER WHERE CalID = '$calID' AND UserID = '$userID'";
		$result = $iCal->returnVector($sql);
		$visible = ($result[0] == 1) ? 0 : 1;
		
		$sql  = "UPDATE CALENDAR_CALENDAR_VIEWER SET Visible = '$visible' ";
		$sql .= "WHERE CalID = '$calID' AND UserID = '$userID'";
		$iCal->db_db_query($sql);
	} 
	else if ($calID=="school")
	{
		$sql = "SELECT Value FROM CALENDAR_USER_PREF WHERE UserID = '$userID' AND Setting = 'schoolCalVisible'";
		$result = $iCal->returnVector($sql);
		
		if(count($result) > 0){
			$visible = ($result[0] == 1) ? 0 : 1;
			$sql  = "UPDATE CALENDAR_USER_PREF SET Value = '$visible' ";
			$sql .= "WHERE Setting='schoolCalVisible' AND UserID = '$userID'";
		} else {
			$sql = "INSERT INTO CALENDAR_USER_PREF (UserID, Setting, Value) VALUES ('$userID', 'schoolCalVisible', 0)";
		}
		$iCal->db_db_query($sql);
	}
	else if ($calID=="involve")
	{
		$sql = "SELECT Value FROM CALENDAR_USER_PREF WHERE UserID = '$userID' AND Setting = 'involveCalVisible'";
		$result = $iCal->returnVector($sql);
		
		if(count($result) > 0){
			$visible = ($result[0] == 1) ? 0 : 1;
			$sql  = "UPDATE CALENDAR_USER_PREF SET Value = '$visible' ";
			$sql .= "WHERE Setting='involveCalVisible' AND UserID = '$userID'";
		} else {
			$sql = "INSERT INTO CALENDAR_USER_PREF (UserID, Setting, Value) VALUES ('$userID', 'involveCalVisible', 0)";
		}
		$iCal->db_db_query($sql);
	}
	/*
	# Old approach of saving visibility of Foundation Calendar
	else if ($calID=="foundation")
	{
		$sql = "SELECT Value FROM CALENDAR_USER_PREF WHERE UserID=$userID AND Setting = 'foundationCalVisible'";
		$result = $iCal->returnVector($sql);
		
		if(count($result) > 0){
			$visible = ($result[0] == 1) ? 0 : 1;
			$sql  = "UPDATE CALENDAR_USER_PREF SET Value = $visible ";
			$sql .= "WHERE Setting='foundationCalVisible' AND UserID = $userID";
		} else {
			$sql = "INSERT INTO CALENDAR_USER_PREF (UserID, Setting, Value) VALUES ('$userID', 'foundationCalVisible', 0)";
		}
		$iCal->db_db_query($sql);
	} 
	*/
	else if($isOtherCalTypeCal && $cal_type_name != '')
	{
		$settingName = $cal_type_name.'CalVisible';
		$sql = "SELECT Value FROM CALENDAR_USER_PREF WHERE UserID = '$userID' AND Setting = '$settingName'";
		$result = $iCal->returnVector($sql);
		
		if(count($result) > 0){
			$visible = ($result[0] == 1) ? 0 : 1;
			$sql  = "UPDATE CALENDAR_USER_PREF SET Value = '$visible' ";
			$sql .= "WHERE Setting = '$settingName' AND UserID = '$userID'";
		} else {
			$sql = "INSERT INTO CALENDAR_USER_PREF (UserID, Setting, Value) VALUES ('$userID', '$settingName', 0)";
		}
		$iCal->db_db_query($sql);
	}
}

intranet_closedb();


?>