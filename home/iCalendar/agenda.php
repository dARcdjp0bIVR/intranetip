<?php
// page modifing by: 
$PathRelative = "../../../";
$CurSubFunction = "iCalendar";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}

AuthPage(array("Communication-Calendar-GeneralUsage"));
intranet_auth();
intranet_opendb();
include_once($PathRelative."src/include/class/icalendar.php");

$iCal = new icalendar();
$lui = new UserInterface();

if (isset($time) && $time!="") {
	$startDate = date("Y-m-d", $time);
	$endDate = date("Y-m-d", $time+24*60*60*7);
} else if (!isset($startDate) || !isset($endDate)) {
	$startDate = date("Y-m-d", time());
	$endDate = date("Y-m-d", time()+24*60*60*7);
}

if (!isset($response))
	$response = "";

# Get the event array for quicker access
$lower_bound = $iCal->sqlDatetimeToTimestamp($startDate." 00:00:00");
$upper_bound = $iCal->sqlDatetimeToTimestamp($endDate." 00:00:00");
$events = $iCal->query_events($lower_bound, $upper_bound);

# Get school event
#$schoolEvents = $iCal->returnSchoolEventRecord($lower_bound, $upper_bound);


$iCal->printAgenda($startDate, $endDate, false, $response);

?>
<link rel="stylesheet" href="css/agenda.css" type="text/css" />
<script language="javascript">
function getAgendaDisplay() {
	if (!isBlocking) {
		var blockMsg = "<h2 style='padding-top:10px;'><?=$i_campusmail_processing?></h2>";
		$('div#content').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
	}
	var sDate = $("input[@name='startDate']").val();
	var eDate = $("input[@name='endDate']").val();
	var response = $("input[@name='response']").val();
	
	if (response == null)
		response = "";
	
	$("#agendaTd").load(
		"get_agenda.php", 
	  {startDate: sDate, endDate: eDate, response: response},
	  function() {
	  	$('div#content').unblock();
	  	isBlocking = false;
	  }
	);
}

$(document).ready( function() {
	$("#quickAddEvent").hide();
	
	// get the position (bottom-left) of quickAddEvent_button link to display the quick add dialog
	// **depend on the jQuery Dimension plugins
	var buttonBottom = $('#quickAddEvent_button').position().top + $('#quickAddEvent_button').height() + 5;
	var buttonLeft = $('#quickAddEvent_button').position().left;
	
	$('#quickAddEvent').Draggable( 
		{
			zIndex: 10,
			ghosting: false,
			opacity: 0.7,
			handle: '#quickAddEvent_handle'
		}
	);
	
	$('#quickAddEvent_form').ajaxForm();
	
	$('#quickAddEvent_button').click(function() 
	{
		$("#quickAddEvent").css("top",buttonBottom);
		$("#quickAddEvent").css("left",buttonLeft);
		// show(), slideUp(), slideDown(), slideToggle(), fadeIn(), fadeOut() can also be used
		$("#quickAddEvent").fadeIn();
		$("input[@name='title']").focus();
		return false;
	});
	
	$('#quickAddEvent_close').click(function() 
	{
		$("#quickAddEvent").fadeOut();
		popUpCal.hideCalendar(popUpCal._curInst, '');
		return false;
	});
	
	$('#form1').ajaxForm({
		target: "#agendaTd",
		url: "get_agenda.php",
		beforeSubmit: function() {
			var blockMsg = "<h2 style='padding-top:10px;'><?=$i_campusmail_processing?></h2>";
			$('div#content').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
			isBlocking = true;
		},
		success: function() {
			$('div#content').unblock();
	  	isBlocking = false;
		}
	});
	
	// jQuery calendar widget
	popUpCal.setDefaults({
		autoPopUp: 'focus', 
		buttonImageOnly: true, 
		dateFormat: 'YMD-',
		buttonImage: 'images/icon_calendar.gif', 
		buttonText: 'Calendar', 
		speed: '',
		clearText: ''
	});
	$('input[@name="eventDate"]').calendar();
	
	$('#quickAddSubmit').click(function() {
		validateForm();
	});
});

function enableTimeSelect() {
	if ($('input[@name="isAllDay"]').attr('checked')) {
		$('#eventTimeDiv').hide();
	} else {
		$('#eventTimeDiv').show();
	}
}

function toggleInvolveEventVisibility(){
	$(".involve").toggle();
}

function toggleSchoolEventVisibility() {
	$('.schoolEventEntry, .academicEventEntry, .holidayEventEntry, .groupEventEntry').toggle();
	resetTableRowClass();
}

function toggleCalVisibility(parClassID) {
	$('.cal-'+parClassID).toggle();
	resetTableRowClass();
	
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calID: parClassID
	}, function() {
	    // callback function
	});
}

function resetTableRowClass() {
	$('#agendaTable tr:not(.tabletop)').removeClass('tablerow1, tablerow2');
	$('#agendaTable tr:visible:odd:not(".tabletop")').addClass('tablerow1');
	$('#agendaTable tr:visible:even:not(".tabletop")').addClass('tablerow2');
}

function openPrintPage() {
	newWindow("print.php?response=<?=$response?>&startDate=<?=$startDate?>&endDate=<?=$endDate?>&calViewPeriod=agenda",10);
}

<?php
	# Check if periodic Ajax call is required to get Popup reminder
	echo $iCal->haveFutureReminder(12);
?>
</script>
<br />
<div id="quickAddEvent" style="display:none">
	<div id="quickAddEvent_handle">
		<a href="#" id="quickAddEvent_close"><img width="13" height="13" border="0" align="absmiddle" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('box_close','','images/layer_btn_close_on.gif',1)" id="box_close" name="box_close" src="images/layer_btn_close.gif"/></a>
		<span style="float:left; padding:2px; font-size:12px;">
			<img src="<?=$PATH_WRT_ROOT?>/images/2007a/icon_new.gif" width="18" height="18" border="0" align="absmiddle" />
			<?=$iCalendar_NewEvent_Header?>
		</span>
	</div>
	<div id="quickAddEvent_content">
		<form name="quickAddEvent_form" id="quickAddEvent_form" method="post" action="new_event_update.php">
			<table id="quickAddEvent_table" width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td width="60" align="left"><label for="title"><?= $iCalendar_NewEvent_Title ?></label></td>
					<td align="left"><input name="title" id="title" type="text" class="tabletext"></td>
				</tr>
				<tr>
					<td align="left"><?= $iCalendar_NewEvent_DateTime ?></td>
					<td align="left">
						<input name="eventDate" type="text" class="tabletext" value="<?=$startDate?>">
					</td>
				</tr>
				<tr>
					<td align="left">&nbsp;</td>
					<td align="left">
						<input type="checkbox" name="isAllDay" id="isAllDay" checked="checked" value="1" onClick="enableTimeSelect()" />
						<label for="isAllDay"><?=$iCalendar_NewEvent_IsAllDay?></label>
						<div id="eventTimeDiv" style="display:none;line-height:2.2;">
							<?=$iCalendar_NewEvent_StartTime?>
							<?= $selectTime ?>:<?= $start_min ?><br />
							<?=$iCalendar_NewEvent_Duration?>
							<select name="durationHr" class="tabletext">
								<option value="0">0 <?=$iCalendar_NewEvent_DurationHr?></option>
								<option value="1">1 <?=$iCalendar_NewEvent_DurationHr?></option>
								<?php for($i=2;$i<13;$i++) echo "<option value=\"$i\">$i $iCalendar_NewEvent_DurationHrs</option>\n"; ?>
							</select> 
							<?= $duration_min ?>
						</div>
					</td>
				</tr>
				<tr>
					<td align="left"><?= $iCalendar_NewEvent_Calendar ?></td>
					<td align="left">
						<?php
							$ownCalendar = $iCal->returnViewableCalendar(TRUE);
							if (sizeof($ownCalendar) == 1) {
								echo $ownCalendar[0]["Name"];
								echo "<input type='hidden' name='calID' value='".$ownCalendar[0]["CalID"]."' />";
							} else {
								echo "<select class='tabletext' name='calID' size='1'>";
								for ($i=0; $i<sizeof($ownCalendar); $i++) {
									echo "<option value='".$ownCalendar[$i]["CalID"]."'".(($ownCalendar[$i]["CalID"]==$calID)?" selected='yes'":"").">".$ownCalendar[$i]["Name"]."</option>\n";
								}
								echo "</select>";
							}
						?>
					</td>
				</tr>
				<tr>
					<td align="left"><?= $iCalendar_NewEvent_Description ?></td>
					<td align="left">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="left">
						<textarea class="tabletext" name="description" cols="40" rows="3"></textarea>
					</td>
				</tr>
				<tr>
					<td align="left" valign="middle">
						<a href="new_event.php" class="tablelink"><img src="<?=$PATH_WRT_ROOT?>/images/2007a/icon_edit.gif" width="12" height="12" border="0" align="absmiddle">Advance</a>
					</td>
					<td align="right">
						<?= $lui->Get_Input_Button($button_add, "quickAddSubmit", $button_add) ?>
						<?= $lui->Get_Input_Button($button_cancel, "quickAddSubmit", $button_cancel, "$('#quickAddEvent').fadeOut('fast');") ?>
					</td>
				</tr>
			</table>
		</form> 
	</div>
</div>
<div id="module_bulletin" class="module_content">
	<div id="module_content_header">
		<div id="module_content_header_title"><a href=""> iCalendar</a></div>
		<div id="module_content_relate"><a href="#">related information</a></div>
	</div>
	<br>
	<?php 
		echo $display;
	?>
</div>
<?php
include_once($PathRelative."src/include/template/general_footer.php");
intranet_closedb();
?>