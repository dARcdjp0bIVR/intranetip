<?php
// page modifing by: 
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");
intranet_auth();
intranet_opendb();

$iCal = new icalendar();
$lui = new ESF_ui();

##########################################################################################

# Get Data
$ActiveEventType = (isset($ActiveEventType)) ? $ActiveEventType : "Invitation";
$eventTypeWording = ($ActiveEventType=="Invitation")?$iCalendar_invitation:$iCalendar_notification;
$eventID = (isset($eventID)) ? $eventID : "";
$updateEventID = (isset($updateEventID)) ? $updateEventID : "";


# Update Status of User on Notification Or Invitation Event
if($updateEventID != ""){
	$repeatEventArr = array();
	$sql = "SELECT 
				b.EventID 
			FROM 
				CALENDAR_EVENT_ENTRY AS a
			INNER JOIN CALENDAR_EVENT_ENTRY AS b ON 
				b.RepeatID = a.RepeatID 
			WHERE 
				a.EventID = $updateEventID";
	$repeatEventArr = $iCal->returnArray($sql);
	
	if($ActiveEventType == "Notification"){
		if(count($repeatEventArr) > 0){
			for($i=0 ; $i<count($repeatEventArr) ; $i++){
				$sql = "UPDATE CALENDAR_EVENT_USER SET Status = 'A' WHERE EventID = ".$repeatEventArr[$i]['EventID']." AND UserID = ".$_SESSION['UserID'];
				$iCal->db_db_query($sql);
			}
		} else {
			$sql = "UPDATE CALENDAR_EVENT_USER SET Status = 'A' WHERE EventID = $updateEventID AND UserID = ".$_SESSION['UserID'];
			$iCal->db_db_query($sql);
		}
	} else {
		$Status = ($Reply == 1) ? 'A' : 'R';
		
		if(count($repeatEventArr) > 0){
			for($i=0 ; $i<count($repeatEventArr) ; $i++){
				$sql = "UPDATE CALENDAR_EVENT_USER SET Status = '$Status' WHERE EventID = ".$repeatEventArr[$i]['EventID']." AND UserID = ".$_SESSION['UserID'];
				$iCal->db_db_query($sql);
			}
		} else {
			$sql = "UPDATE CALENDAR_EVENT_USER SET Status = '$Status' WHERE EventID = $updateEventID AND UserID = ".$_SESSION['UserID'];
			$iCal->db_db_query($sql);
		}
	}
}



# Initialization 
$result = array();
$InviteEventID = array();
$NotifyEventID = array();
$ActiveEventID = array();
$IsHavingEvent = false;
$RepeatedEvent = array();

# Preparating Data
if($ActiveEventType == "Notification"){
	# Get All Active EventID
	$result = $iCal->Get_Calendar_Active_Event("Notification");
	$new_result = $iCal->Return_Distinct_Object_Array($result, 'RepeatID', 'EventID');
	
	if(count($new_result) > 0){
		$IsHavingEvent = true;
		
		foreach($new_result as $key => $value){
			$NotifyEventID[] = $key;
		}
	}
	/*
	if(count($result) > 0){	
		$IsHavingEvent = true;
		for($i=0 ; $i<count($result) ; $i++){
			$NotifyEventID[] = $result[$i]['EventID'];
		}
	}*/
	
	if($eventID != ""){
		$currentEventID = $eventID;
	} else {
		$currentEventID = (count($NotifyEventID) > 0) ? $NotifyEventID[count($NotifyEventID)-1] : "";
	}
	
	$currentIndex = array_search($currentEventID, $NotifyEventID);
	$ActiveEventID = $NotifyEventID;
} else {
	# Get All Active EventID 
	$result = $iCal->Get_Calendar_Active_Event("Invitation");
	$new_result = $iCal->Return_Distinct_Object_Array($result, 'RepeatID', 'EventID');
	
	if(count($new_result) > 0){
		$IsHavingEvent = true;
		
		foreach($new_result as $key => $value){
			$InviteEventID[] = $key;
		}
	}
	/*
	if(count($result) > 0){	
		$IsHavingEvent = true;
		for($i=0 ; $i<count($result) ; $i++){
			$InviteEventID[] = $result[$i]['EventID'];
		}
	}
	*/
	
	if($eventID != ""){
		$currentEventID = $eventID;
	} else {
		$currentEventID = (count($InviteEventID) > 0) ? $InviteEventID[count($InviteEventID)-1] : "";
	}
	
	$currentIndex = array_search($currentEventID, $InviteEventID);
	$ActiveEventID = $InviteEventID;
}

# Get Previous Event ID
if(isset($ActiveEventID[$currentIndex-1]) ){
	$prevEventID = $ActiveEventID[$currentIndex-1];
} else {
	$prevEventID = "";
}
# Get Next Event ID
if(isset($ActiveEventID[$currentIndex+1]) ){
	$nextEventID = $ActiveEventID[$currentIndex+1];
} else {
	$nextEventID = "";
}


# Get Event Data From DB
$isCalType4 = false;
if($IsHavingEvent){
	// if (!empty($new_result[$currentEventID][0]["schoolCode"])){ #other school invitation
		// $dbName = $iCal->get_corresponding_dbName(trim($new_result[$currentEventID][0]["schoolCode"]));
		// $fieldname  = "a.EventID, a.UserID, a.CalID, a.RepeatID, CONVERT(varchar(20), a.EventDate, 120) AS EventDate, a.Title, ";
		// $fieldname .= "a.Description, a.Duration, a.IsImportant, a.IsAllDay, a.Access, a.Location, a.Url ";
		
		// $sql  = "SELECT $fieldname FROM ".$dbName.".dbo.CALENDAR_EVENT_ENTRY AS a ";
		// $sql .= "WHERE a.EventID=$currentEventID";
		// $isCalType4 = true;
	// }
	// else{
		$fieldname  = "a.EventID, a.UserID, a.CalID, a.RepeatID, Date_Format(a.EventDate, '%Y-%m-%d %T') AS EventDate, a.Title, ";
		$fieldname .= "a.Description, a.Duration, a.IsImportant, a.IsAllDay, a.Access, a.Location, a.Url ";
		
		$sql  = "SELECT $fieldname FROM CALENDAR_EVENT_ENTRY AS a ";
		$sql .= "WHERE a.EventID=$currentEventID";
	//}
	$result = $iCal->returnArray($sql);

	$isRepeatedEvent = false;
	if(count($result) > 0){
		$evtID = $result[0]["EventID"];
		$title = htmlspecialchars($result[0]["Title"]);
		$eventDateTime = $result[0]["EventDate"];
		$eventTs = $iCal->sqlDatetimeToTimestamp($eventDateTime);
		$endTs = $eventTs + $result[0]["Duration"]*60;
		$eventDateTimePiece = explode(" ", $eventDateTime);
		$eventDate = $eventDateTimePiece[0];
		
		$date = explode('-', $eventDate);
		$day = $date[2];
		$month = $date[1];
		$year = $date[0];
		
		# Handling Repeated Event Series
		if(count($new_result[$evtID]) > 1){
			$isRepeatedEvent = true;
			$repeatLastDate = explode(' ', $new_result[$evtID][count($new_result[$evtID])-1]['EventDate']);
			$repeat_date = explode('-', $repeatLastDate[0]);
			$repeat_day = $repeat_date[2];
			$repeat_month = $repeat_date[1];
			$repeat_year = $repeat_date[0];
			$repeatEndDate = $repeat_day.'-'.$repeat_month.'-'.$repeat_year;
		}
		
		if ($result[0]["IsAllDay"] == 0) {
			$eventTime = $eventDateTimePiece[1];
			$eventTimePiece = explode(":", $eventTime);
			$eventHr = $eventTimePiece[0];
			$eventMin = $eventTimePiece[1];
			$duration = (int)$result[0]["Duration"];
			$durationHr = (int)($duration/60);
			$durationMin = $duration-$durationHr*60;
			
			# Gather Data
			$eventDateStr = $day.'-'.$month.'-'.$year;
			$eventTimeStr = $eventHr.':'.$eventMin.' ';
			
			$durationStr = '';
			if($durationHr > 0 || $durationMin > 0){
				$durationStr  = '(';
				$durationStr .= ($durationHr > 0) ? $durationHr." ".(($durationHr <= 1) ? "hr" : "hrs") : "";
				$durationStr .= ($durationMin > 0) ?  (($durationHr > 0)?" ":"").$durationMin." ".(($durationMin <= 1) ? "min" : "mins") : "";
				$durationStr .= ')';
			}
		} else {
			$eventDateStr = $day.'-'.$month.'-'.$year;
			$eventTimeStr = '';
			$durationStr = "All day";
		}
		
		$createdBy = $result[0]["UserID"];
	}
}

# Content
$x ='
	<div>
	  <span style="float:left">
	  	'.(($prevEventID != '') ? '<a href="javascript:jGet_Active_Event(\''.$ActiveEventType.'\', \''.$prevEventID.'\')"><img src="'.$ImagePathAbs.'layout_grey/btn_prev_off.gif" width="18" height="18" border="0" align="absmiddle"></a>' : '').'
	  </span>
	  <span style="float:right">
	    '.(($nextEventID != '') ? '<a href="javascript:jGet_Active_Event(\''.$ActiveEventType.'\', \''.$nextEventID.'\')"><img src="'.$ImagePathAbs.'layout_grey/btn_next_off.gif" width="18" height="18" border="0" align="absmiddle"></a>' : '').'
	  </span>
	</div>	
	<br style="clear:both">	
	<span class="cal_act_event_board_detail cal_act_event_board_detail_invite">
	  <h1>'.$eventTypeWording.'</h1>';
if($IsHavingEvent && count($result) > 0){
	$x .= $title.'<br>';
	if(!$isRepeatedEvent){
		$x .= $eventDateStr.' '.$eventTimeStr.' '.$durationStr.'<br>';
	} else {
		$x .= $eventDateStr.' to '.$repeatEndDate.'<br>';
		$x .= $eventTimeStr.$durationStr.'<br>';
	}
	$x .= 'From : '.($isCalType4?$iCal->returnUserFullName($createdBy."-".$result[0]["schoolCode"],4):$iCal->returnUserFullName($createdBy));

	$x .= '<p class="dotline_p_narrow"></p>';
	$x .= '<div class="sub_layer_button">';
	  
	# Having at least one event
	$nextValidEventID = ($prevEventID != '') ? $prevEventID : '';
	$nextValidEventID = ($nextValidEventID == '') ? $nextEventID : $nextValidEventID;
	if($ActiveEventType == "Notification"){
		$x .= '<span style="float:right">';
		$x .= $lui->Get_Input_Button("OK", "OK", $iCalendar_OK, "class=\"button\" onclick=\"jUpdate_Active_Event('".$ActiveEventType."', '".$currentEventID."', '".$nextValidEventID."', 1)\"");
		$x .= '&nbsp;</span><br style="clear:both">';		
	} else {
		$x .= '<span style="float:left">Accept?</span>';
		$x .= '<span style="float:right">';
		$x .= $lui->Get_Input_Button("YES", "YES", $iCalendar_Yes, "class=\"button\" onclick=\"jUpdate_Active_Event('".$ActiveEventType."', '".$currentEventID."', '".$nextValidEventID."', 1)\"");
		$x .= $lui->Get_Input_Button("NO", "NO", $iCalendar_No, "class=\"button\" onclick=\"jUpdate_Active_Event('".$ActiveEventType."', '".$currentEventID."', '".$nextValidEventID."', 0)\"");
		$x .= '&nbsp;</span><br style="clear:both">';
	}
} else {
	# No any Event 
	$x .= $iCalendar_NoActiveEvent;
}
		
$x .= '</div>';
$x .= '</span>';


##########################################################################################

intranet_closedb();

echo $x;//iconv("BIG5", "UTF-8", $x);
?>