<?php
// page modifing by: 
/*
 * 2019-06-06 Carlos: Added CSRF token.
 */
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");
intranet_auth();
intranet_opendb();

$iCal = new icalendar();
$lui = new ESF_ui();

if (!isset($time))
	$time = $_SESSION["iCalDisplayTime"];
	
# Button
$button  = '<div id="form_btn">';
$button .= $lui->Get_Input_Submit("Search", "Search", $iCalendar_Search, "class=\"button_main_act\"");
$button .= '&nbsp;';
$button .= $lui->Get_Reset_Button("Reset", "Reset", $Lang['btn_reset'], "class=\"button_main_act\"");
$button .= '&nbsp;';
$button .= $lui->Get_Input_Button("Cancel", "Cancel", $Lang['btn_cancel'], "class=\"button_main_act\", onclick=\"window.location='manage_calendar.php'\"");
$button .= '</div>';

# Main
$CalLabeling = "<div id='sub_page_title'>$iCalendar_AddPublicCalendar_Header</div>";
$CalMain = '
	<div id="cal_board">
	  <span class="cal_board_02"></span> 
	  <div class="cal_board_content">
	    <div id="calendar_wrapper" style="float:left;background-color:#dbedff">
	  	  <div id="cal_manage_calendar">';
	  	  
# Form - Search
$CalMain .= '<br style="clear:both"><br>
			<form name="form1" action="search_calendar_result.php" method="POST">';
$CalMain .= csrfTokenHtml(generateCsrfToken());
$CalMain .= '<div id="cal_publiccal" style=";width:95%; padding-left:10px; padding-right:10px">';
$CalMain .= '<p id="errMessageBox"></p>';
$CalMain .= ' <p class="cssform">
			    <label for="Sender">'.$iCalendar_Name.'</label>
			    <span id="imail_form_indside_choice">';
$CalMain .= $lui->Get_Input_Text("keyword", "", "class=\"textbox\"");
$CalMain .=  '  </span>
			  </p>
			  <br style="clear:both"><br>
			  <p class="dotline_p"></p>';
$CalMain .= $button;
$CalMain .= '</div></form><br>';
			
# Form - display result
$CalMain .= '<form name="form2" action="add_public_calendar.php" method="POST">';
$CalMain .= '<div id="cal_publiccal" style=";width:95%; padding-left:10px; padding-right:10px">
			    <div id="searchResult"></div>
			  </div>
			</form><br>';
	
$CalMain .= '
		  </div>
		</div>
	  </div>
	  <span class="cal_board_05"><span class="cal_board_06"></span></span>
	</div>';


# Generate UI
$display = $iCal->Get_iCalendar_Template($CalLabeling, $CalMain, '','','','','',false,false);

?>
<style type="text/css">@import url(css/main.css);</style>
<style type="text/css">
#deleteCalendarWarning {
	font-family:"Arial","Helvetica","sans-serif";
}
</style>

<?php
// include_once($PathRelative."src/include/template/general_header.php");
include_once("headerInclude.php");
?>
<!--
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery.blockUI.js"></script>
-->
<script language="javascript">
$(document).ready(function() {
	$("form[@name='form1']").submit(function() {
		if ($.trim($("input[@name=keyword]").val()) == "") {
			$("#errMessageBox").html("<li><?=$i_Calendar_InvalidSearch_NoKeyword?></li>")
			$("input[@name=keyword]")[0].focus();
			$("input[@name=keyword]").addClass("error");
		} else {
			$("#errMessageBox").html("");
			$("input[@name=keyword]").removeClass("error");
			$("#searchResult").load(
				"search_calendar_result.php",
				{keyword: encodeURIComponent($("input[@name=keyword]").val()),csrf_token:document.form1.csrf_token.value,csrf_token_key:document.form1.csrf_token_key.value}
			);
		}
		return false;
	});
	
	$("form[@name='form2']").ajaxForm({
		target: '#viewCalendarControlTd',
		success: function() {
			$("form[@name='form1']").submit();
		}
	});
	
	$("#loading").ajaxStart(function(){
		$(this).show();
	});
	
	$("#loading").ajaxComplete(function(){
		$(this).hide();
	});
});

/*
function checkAdd($pageUrl) {
	// check if at least one checkbox is checked
	var anyChecked = false;
	for(var i=0; i < $("input[@name='CalID[]']").size(); i++) {
		if ($("input[@name='CalID[]']").eq(i).attr("checked") == true) {
			anyChecked = true;
			break;
		}
	}
	
	if (anyChecked) {
		$("form[@name='form2']").submit();
	} else {
		alert("<?=$i_Calendar_NoCalSelect?>");
	}
}
*/

function jAdd_Public_Calendar(calID, trIndex){
	$.post("add_public_calendar.php",   
		{ CalID: calID, csrf_token:document.form1.csrf_token.value, csrf_token_key:document.form1.csrf_token_key.value },
	    function(data){ 
		    document.getElementById("cal_calendar_list").innerHTML = data;
		    
		    var obj = document.getElementById("CalSearchResult");
		    var rowObj = document.getElementById("search_"+trIndex);
		    obj.deleteRow(rowObj.rowIndex);
		    
		    if(obj.rows.length == 1){
			    var jNewRow = obj.insertRow(-1);
			    var jNewCell = jNewRow.insertCell(0);
			    jNewCell.height = 30;
			    jNewCell.className = "cal_agenda_content_entry";
			    jNewCell.innerHTML = "No Calendar at this moment.";
		    }
		} 
		);

}
function toggleExternalCalVisibility(selfName,eventClass,CalType){
	//toggleCalVisibility(eventClass,selfName);
	// Ajax request to update visible calendar
	$.post('cal_visible.php', {
		calType : CalType		
	}, function(responseText) {
	    // callback function
		// if (!SessionExpired(responseText)){ }
	});
	
	// isDisplayMoreFunction();
	// jHide_All_Layers();
}
function toggleInvolveEventVisibility(){
	$(".involve").toggle();
}

function toggleFoundationEventVisibility(obj){ 
	$('.foundation').toggle();
}

function toggleSchoolEventVisibility() {
	$('.schoolEventEntry, .academicEventEntry, .holidayEventEntry, .groupEventEntry').toggle();
}

function toggleCalVisibility(parClassID) {
	$('.cal-'+parClassID).toggle();
}

function toggleOwnDefaultCalVisibility(parClassID){
	toggleCalVisibility(parClassID);
	
	var obj = document.getElementById("toggleCalVisible-"+parClassID);
	var involObj = document.getElementById("toggleVisible");
	if(obj.checked == true){
		if(involObj.checked == false)
			toggleInvolveEventVisibility();
		involObj.checked = true;
	} else {
		if(involObj.checked == true)
			toggleInvolveEventVisibility();
		involObj.checked = false;
	}	
}

<?php
	# Check if periodic Ajax call is required to get Popup reminder
	echo $iCal->haveFutureReminder(12);
?>
</script>

<div id="module_bulletin" class="module_content">
	<div id="module_content_header">
		<div id="module_content_header_title"><a href="index.php">iCalendar</a></div>
		<div id="module_content_relate" style="display:none"><a href="#">related information</a></div>
		&nbsp;
	</div>
	<br style="clear:both" />
	<?php 
		echo $display;
	?>
</div>

<?php
// include_once($PathRelative."src/include/template/general_footer.php");
include_once("footerInclude.php");
?>
