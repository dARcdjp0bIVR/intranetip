<?php
// page modifing by: 
/**************************************** Changes ********************************************
 * 2017-01-24 (Carlos): [ip2.5.8.3.1] Current user would not be explicitly in participant list. Added send push msg btn. Added extra title info handling. 
 * 2014-11-06 (Carlos) [ip2.5.5.12.1]: Comment out the code that wrongly delete school group INTRANET_EVENT for repeating events
 * 2012-12-06 (Ivan) [2012-1129-1604-13066]: Fixed ebooking record is disappeared if edit the event
 * 2011-09-19 (Carlos): Set $location to INTRANET_EVENT EventVenue 
 *********************************************************************************************/

$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once("choose/new/User_Group_Transformer.php");

intranet_auth();
intranet_opendb();

$iCal = new icalendar();
$db = new libdb();
$transformer = new User_Group_Transformer();
#################################################################################################################################

## Use Library
$iCal = new icalendar();
$db = new libdb();

## Get Post Data
$CalType 	 = (isset($CalType) && $CalType != '') ? $CalType : '';
$ProgrammeID = (isset($ProgrammeID) && $ProgrammeID != '') ? $ProgrammeID : '';		# CPD use only 

## Initialization
$result = array();
$viewerList 	  = array();
$viewerClassList  = array();
$viewerCourseList = array();
$viewerGroupList  = array();
$otherSchoolViewer = array();

if(!isset($viewer)){
	$viewer = array();
}
$viewer = array_values(array_unique($viewer));
$allEventIdAry = array();

## Preparation
# Check whether it is other Calendar Type Event
// $isOtherCalType = $iCal->Is_Other_Calendar_Type($CalType);

# User access right for this event
$userEventAccess = $iCal->returnUserEventAccess($UserID,$eventID);
if (!$userEventAccess&&$CalType != 1){
	header("Location: index.php?time=".$_SESSION["iCalDisplayTime"]."&calViewPeriod=".$_SESSION["iCalDisplayPeriod"]);
}


## Handle form parameters


$eventTimeStamp = $iCal->sqlDatetimeToTimestamp($eventDateTime);

# this variable won't be change in the code
$eventTs 	  = $eventTimeStamp;
$datePieces   = explode("-", $eventDate);
$duration 	  = (int)$durationHr * 60 + (int)$durationMin;

$datePieces2 = explode("-", $eventEndDate);

$m = strlen($datePieces[1])==1? "0".$datePieces[1]:$datePieces[1];
$d = strlen($datePieces[2])==1? "0".$datePieces[2]:$datePieces[2];
$h = "00";
if (isset($eventHr))
	$h = strlen($eventHr)==1? "0".$eventHr:$eventHr;
$i = "00";
if (isset($eventMin))
	$i = strlen($eventMin)==1? "0".$eventMin:$eventMin;
$startEvent = $datePieces[0].$m.$d."T".$h.$i."00";
if (empty($eventDate)){
	$eventTimeStamp = strtotime($eventDateTime);
}
else
	$eventTimeStamp=mktime($h,$i,0,$m,$d,$datePieces[0]);//modified on 16 July 2009
$eventTs=$eventTimeStamp;//modified on 16 July 2009

$ebookingEventDate = $eventDate;
$ebookingStartTime = $h.':'.$i.':00';


$m = strlen($datePieces2[1])==1? "0".$datePieces2[1]:$datePieces2[1];
$d = strlen($datePieces2[2])==1? "0".$datePieces2[2]:$datePieces2[2];
$h = "00";
if (isset($eventEndHr))
	$h = strlen($eventEndHr)==1? "0".$eventEndHr:$eventEndHr;
$i = "00";
if (isset($eventEndMin))
	$i = strlen($eventEndMin)==1? "0".$eventEndMin:$eventEndMin;
$endEvent = $datePieces2[0].$m.$d."T".$h.$i."00";
$duration = $iCal->time_diff_UTC($startEvent,$endEvent);

$ebookingEndTime = $h.':'.$i.':00';

if ($duration < 0){
	$Msg = $Lang['ReturnMsg']['EventCreateUnSuccess'];
	header("Location: index.php?time=".$_SESSION["iCalDisplayTime"]."&calViewPeriod=".$_SESSION["iCalDisplayPeriod"]."&Msg=$Msg");
	exit;
}

if (!($CalType == 1 || $CalType > 3 || ($plugin["iCalendarEditByOwner"] && $createdBy!=$_SESSION['UserID']))){
	if ($userEventAccess=="A" || $userEventAccess=="W") {
		if ($isAllDay != 1) {// updated on 13 May 08
			$eventDateTime = "$eventDate $eventHr:$eventMin:00";
		} else {
			$eventDateTime = "$eventDate 00:00:00";
			$duration = $duration + 1440; 
		}
	}
}

if($CalType == 2){		// used for group event 
	$OldRepeatID = $repeatID;
}

if ($isAllDay != 1) {
	$isAllDay = 0;
}

$insertUserID = (isset($createdBy)) ? $createdBy : $UserID;
$response 	  = ($response == "") ? "W" : $response;
$url 		  = (!$iCal->isValidURL($url)) ? NULL : $url;

if (isset($calAccess) && !isset($existGuestList)) {
	$existGuestList = $iCal->returnGuestList($eventID);
	//unset($existGuestList[array_search($insertUserID, $existGuestList)]);
}

$eBookingRecordRemarksSql = '';
$eBookingRecordRemarksSql .= $iCal->Get_Safe_Sql_Query($iCal->Get_Request($title));
if ($description) {
	$eBookingRecordRemarksSql .= "\n\n".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description));
}

################################################################################################
# Get & Check for Current EventType 
if ($CalType == 1 || $CalType > 3 || ($plugin["iCalendarEditByOwner"] && $createdBy!=$_SESSION['UserID'])){ # eclass calendar
	$sql = "select PersonalNoteID from CALENDAR_EVENT_PERSONAL_NOTE where
			UserID = '".$_SESSION["UserID"]."' And EventID = '$eventID'
			and CalType = '$CalType' ";
	$personalNote = $db->returnVector($sql);	
	if (!empty($personalNote)){
		$sql = "Update CALENDAR_EVENT_PERSONAL_NOTE set
				PersonalNote = '".$db->Get_Safe_Sql_Query($iCal->Get_Request($PersonalNote))."'
				where PersonalNoteID = '".$personalNote[0]."'
				";
		$result["Update_personal_note_caltype1"] = $db->db_db_query($sql);
	}
	else{
		$sql = "Insert into CALENDAR_EVENT_PERSONAL_NOTE 
		(EventID,UserID,PersonalNote,CreateDate,LastUpdateDate,CalType)
		values
		('$eventID','".$_SESSION["UserID"]."','".$db->Get_Safe_Sql_Query($iCal->Get_Request($PersonalNote))."',now(),now(),'$CalType')";
		$result["insert_personal_note_caltype1"] = $db->db_db_query($sql);
	} 
	
	####  reminder 
	$reminderIDSql = isset($reminderID)?implode("','",$reminderID):'';
	$sql = "delete from CALENDAR_REMINDER where ReminderID not in 
			('$reminderIDSql') 
			and UserID = '".$_SESSION["UserID"]."' 
			and EventID = '$eventID'";
	$result["delete_removeed_reminder"] = $db->db_db_query($sql); 
	$cnt = 0;
	if (count($reminderID)>0){
		foreach ($reminderID as $rid){
			$sql = "update CALENDAR_REMINDER set 
			ReminderDate = '$eventDateTime', 
			ReminderBefore = '".$reminderTime[$cnt]."'
			where ReminderID = '$rid'
			";
			$result["update_reminder_".$cnt] = $db->db_db_query($sql);
			$cnt++;
		}
	}

	if (count($reminderTime)>count($reminderID)){
		$sql= "insert into CALENDAR_REMINDER 
			(EventID,UserID,ReminderType,ReminderDate,ReminderBefore,CalType) 
			values			
		";
		for ($i=$cnt; $i<count($reminderTime);$i++){
			$sql .= " ('$eventID','".$_SESSION["UserID"]."','P','$eventDateTime','".$reminderTime[$i]."','$CalType'), ";
		}
		$sql = rtrim($sql,', ');
		$result["insert_reminder"] = $db->db_db_query($sql);
	}
	#######################
	
	if(!in_array(false, $result)){
		$Msg = $Lang['ReturnMsg']['EventUpdateSuccess'];
	} else {
		$Msg = $Lang['ReturnMsg']['EventUpdateUnSuccess'];
	}
	intranet_closedb();
	header("Location: index.php?Msg=$Msg");
	exit;
}


$EventType = $iCal->Get_Current_Event_Type($calID, $eventID);
if($EventType != "0" && $EventType != "1"){
	# When create NEW event
	# Case 1: if no any guest is invited , EventType is regarded ar NOT Defined
	# Case 2: if at least one guest is invited, EventType is set to be Compulsory / Optional
	#
	# if number of guest > 0, inviteStatus is valid
	# if number of guest = 0, inviteStatus is not valid and set to NULL
	$InviteStatus = (count($viewer) > 0) ? $InviteStatus : "NULL";
} else {
	$InviteStatus = ($EventType != $InviteStatus) ? $EventType : $InviteStatus;
}
$InviteEventUserStatus = $InviteStatus == "1" ?  "W" : "A";
################################################################################################


# Guests to be removed
if (isset($removeGuestList)) {
	$temp = array();
	for ($j=0; $j<sizeof($removeGuestList); $j++) {
		if ($removeGuestList[$j] != "-1")
			$temp[] = $removeGuestList[$j];
	}
	$removeGuestList = $temp;
} 

# New guests
for ($i=0; $i<sizeof($viewer); $i++) {
	$viewerPrefix = substr($viewer[$i], 0, 1);
	$viewerID = substr($viewer[$i], 1);
	if ($viewerPrefix == 'U')
	$viewerList[] = $viewerID ;
	else{
		$tempGuest = $transformer->get_userID_from_group($viewerPrefix,$viewerID);
		$viewerList = array_merge($viewerList,$tempGuest);
	}
}
if(is_array($existGuestList) && count($existGuestList)>0)
{
	$viewerList = array_values(array_diff($viewerList, $existGuestList));
}
// remove owner
//$viewerList 	  = $iCal->removeAnArrayValue($viewerList, $UserID);
//$viewerClassList  = $iCal->removeAnArrayValue($viewerClassList, $UserID);
//$viewerCourseList = $iCal->removeAnArrayValue($viewerCourseList, $UserID);
//$viewerGroupList  = $iCal->removeAnArrayValue($viewerGroupList, $UserID);

$viewerList = array_values(array_unique($viewerList));

## Main
if (isset($isRepeatEvent) && $saveRepeatEvent!="OTI")
{
	#echo "I'm a repeated event but not save OTI (only this instance)"."\n";
	if ($userEventAccess=="A" || $userEventAccess=="W") 
	{
		$repeatEndDatePieces = explode("-", $repeatEnd);
		if ($isAllDay != 1) {	// updated on 13 May 08
			$repeatEndTime = mktime($eventHr,$eventMin,0,$repeatEndDatePieces[1],$repeatEndDatePieces[2],$repeatEndDatePieces[0]);
		} else {
			$repeatEndTime = mktime(0,0,0,$repeatEndDatePieces[1],$repeatEndDatePieces[2],$repeatEndDatePieces[0]);
		}
		$repeatEndDate = date("Y-m-d H:i:s", $repeatEndTime);
		
		# Construct the repeat pattern
		switch ($repeatSelect) {
			case "DAILY":
				$repeatPattern = $repeatSelect."#".$repeatEndDate."#".$repeatDailyDay."#";
				break;
			case "WEEKDAY":
			case "MONWEDFRI":
			case "TUESTHUR":
				$repeatPattern = $repeatSelect."#".$repeatEndDate."##";
				break;
			case "WEEKLY":
				$detail = (isset($Sun)?"1":"0").(isset($Mon)?"1":"0").(isset($Tue)?"1":"0").(isset($Wed)?"1":"0");
				$detail .= (isset($Thu)?"1":"0").(isset($Fri)?"1":"0").(isset($Sat)?"1":"0");
				$repeatPattern = $repeatSelect."#".$repeatEndDate."#".$repeatWeeklyRange."#".$detail;
				break;
			case "MONTHLY":
				if ($monthlyRepeatBy == "dayOfMonth")
					$detail = "day-".$datePieces[2];
				else {
					$eventDateWeekday = date("w", $eventTimeStamp);
					$nth = 0;
					for($i=1; $i<6; $i++) {
						$nthweekday = $iCal->NWeekdayOfMonth($i,$eventDateWeekday,$datePieces[1],$datePieces[0],"d");
						if ((int)$datePieces[2] == $nthweekday) {
							$nth = $i;
							break;
						}
					}
					$detail = "$nth-$eventDateWeekday";
				}
				$repeatPattern = $repeatSelect."#".$repeatEndDate."#".$repeatMonthlyRange."#".$detail;
				break;
			case "YEARLY":
				$repeatPattern = $repeatSelect."#".$repeatEndDate."#".$repeatYearlyRange."#";
				break;
		}
	}
	
	if ($saveRepeatEvent=="ALL") 
	{
		#echo "Save ALL events instance"."\n";
		$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);	
		
		# Event date and repeat pattern are UNCHANGED
		if ($userEventAccess=="R" || ($oldEventDate==$eventDate && $oldRepeatPattern==$repeatPattern)) {
			for ($i=0; $i<sizeof($repeatSeries); $i++) {
				$allEventIdAry[] = $repeatSeries[$i]["EventID"];
				
				$curRepeatDateTime = $repeatSeries[$i]["EventDate"];
				$curRepeatTimeStamp = $iCal->sqlDatetimeToTimestamp($curRepeatDateTime);
				
				# Update only if user have All/Write permission
				if ($userEventAccess=="A" || $userEventAccess=="W") 
				{
					# Update main detail
					$sql  = "UPDATE CALENDAR_EVENT_ENTRY SET ";
					if (!$isAllDay && $oldEventTime != "$eventHr:$eventMin:00") {
						$existDateTimePiece = explode(" ", $curRepeatDateTime);
						$sql .= "EventDate = '".$existDateTimePiece[0]." $eventHr:$eventMin:00', ";
					}
					#$sql .= "EventDate = '$eventDateTime', ";
					$sql .= "ModifiedDate = NOW(), ";
					$sql .= "Duration = '$duration', ";
					$sql .= "IsImportant = '$isImportant', ";
					$sql .= "IsAllDay = '$isAllDay', ";
					$sql .= "Access = '$access', ";
					$sql .= "Title = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($title))."', ";
					$sql .= "Description = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description))."', ";
					$sql .= "Location = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($location))."', ";
					$sql .= "Url = '$url', ";
					$sql .= "CalID = '$calID'";
					//$sql .= "UID = '".$repeatSeries[0]["UID"]."' ";
					$sql .= "WHERE EventID = ".$repeatSeries[$i]["EventID"];
					
					$result["update_event_entry_ALL_".$i] = $iCal->db_db_query($sql);
					// update personal note
					if ($result["update_event_entry_ALL_".$i]) {
						$result["update_event_entry_personal_note_ALL_".$i] = $iCal->Insert_Personal_Note($repeatSeries[$i]["EventID"], $iCal->Get_Request($PersonalNote), $CalType, '');
					}
				}
				else 
				{
					// update personal note
					$result["update_event_entry_personal_note_ALL_".$i] = $iCal->Insert_Personal_Note($repeatSeries[$i]["EventID"], $iCal->Get_Request($PersonalNote), $CalType, '');
				}
				
				# Reminder
				$reminderID = $iCal->returnReminderID($UserID,$repeatSeries[$i]["EventID"]);
				if (sizeof($reminderID)>0 || isset($reminderType)) {
					# $reminderType > $reminderID means new reminder is added
					if (sizeof($reminderType) >= sizeof($reminderID)) {
						for ($j=0; $j<sizeof($reminderType); $j++) {
							$reminderDate[$j] = $curRepeatTimeStamp - ((int)$reminderTime[$j])*60;
							$reminderDate[$j] = date("Y-m-d H:i:s", $reminderDate[$j]);
							if ($j < sizeof($reminderID)) {
								# Update existing reminder
								$sql  = "UPDATE CALENDAR_REMINDER SET ";
								$sql .= "ReminderType = '".$reminderType[$j]."', ";
								$sql .= "ReminderDate = '".$reminderDate[$j]."', ";
								$sql .= "LastSent = NULL, ";
								$sql .= "ReminderBefore = '".$reminderTime[$j]."' ";
								$sql .= "WHERE ReminderID = '".$reminderID[$j]."' ";
								$result["update_event_reminder_ALL_$i_$j"] = $iCal->db_db_query($sql);
							} else {
								# insert new reminder
								$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore";
								$fieldvalue = "'".$repeatSeries[$i]["EventID"]."', '".$UserID."', '".$reminderType[$j]."', '".$reminderDate[$j]."', '".$reminderTime[$j]."'";
								$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ($fieldvalue)";
								$result["insert_event_reminder_ALL_$i_$j"] = $iCal->db_db_query($sql);	
							}
						}
					} else {	# $reminderType < $reminderID means some reminder have to be updated and some have to be removed
						for ($j=0; $j<sizeof($reminderID); $j++) {
							if ($j < sizeof($reminderType)) {
								$reminderDate[$j] = $curRepeatTimeStamp - ((int)$reminderTime[$j])*60;
								$reminderDate[$j] = date("Y-m-d H:i:s", $reminderDate[$j]);
								$sql  = "UPDATE CALENDAR_REMINDER SET ";
								$sql .= "ReminderType = '".$reminderType[$j]."', ";
								$sql .= "ReminderDate = '".$reminderDate[$j]."', ";
								$sql .= "LastSent = NULL, ";
								$sql .= "ReminderBefore = '".$reminderTime[$j]."' ";
								$sql .= "WHERE ReminderID = '".$reminderID[$j]."' ";
								$result["update_event_reminder_ALL_$i_$j"] = $iCal->db_db_query($sql);
							} else {
								$sql = "DELETE FROM CALENDAR_REMINDER WHERE ReminderID = '".$reminderID[$j]."' ";
								$result["delete_event_reminder_ALL_$i_$j"] = $iCal->db_db_query($sql);
							}
						}
					}
				}
				
				# Update only if user have All/Write permission
				if (($userEventAccess=="A" || $userEventAccess=="W") ) {								################################# Break Point
					# Remove guests in CALENDAR_EVENT_USER & CALENDAR_REMINDER
					if (isset($removeGuestList) && sizeof($removeGuestList)>0) {
						for ($j=0; $j<sizeof($removeGuestList); $j++) {
							//$pieces = explode($removeGuestList[$j]);

							$sql = "DELETE FROM CALENDAR_EVENT_USER WHERE EventID = '".$repeatSeries[$i]["EventID"]."' AND UserID = '".$removeGuestList[$j]."' ";//." AND FromSchoolCode is null AND ToSchoolCode is null";
							$result["delete_event_guest_ALL_$i_$j"] = $iCal->db_db_query($sql);
							$sql = "DELETE FROM CALENDAR_REMINDER WHERE EventID = '".$repeatSeries[$i]["EventID"]."' AND UserID = '".$removeGuestList[$j]."' ";
							$result["delete_event_guest_reminder_ALL_$i_$j"] = $iCal->db_db_query($sql);
							
							// remove personal note of that guests
							$eclassConstraint =" AND (CalType <> 1 AND CalType <= 3 OR CalType is null) ";
							$sql = "DELETE FROM CALENDAR_EVENT_PERSONAL_NOTE WHERE EventID = '".$repeatSeries[$i]["EventID"]."' AND UserID = '".$removeGuestList[$j]."' ".$eclassConstraint;//." AND CalType <> 3 AND CalType <> 4";
							$result["delete_event_guest_Personal_Note_ALL_$i_$j"] = $iCal->db_db_query($sql);
						}
					}
					$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore";
					$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ";
					$cnt = 0;
					
					$curReminderDate = $curRepeatTimeStamp - 10*60;
					$curReminderDate = date("Y-m-d H:i:s", $curReminderDate);
					
					# Add reminders
					if (isset($viewerList) && sizeof($viewerList) != 0) {
						for ($j=0; $j<sizeof($viewerList); $j++) {
							if($cnt == 0){
								$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', 'P', '$curReminderDate', 10)";
								$cnt++;
							} else {
								$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', 'P', '$curReminderDate', 10)";
							}
						}
					}
					
					$sql = rtrim($sql, ", ");
					if($cnt > 0){
						$result["InsertReminder_RepeatAll_$i"] = $iCal->db_db_query($sql);
					}
					
					# Add new guests
					$fieldname  = "EventID, UserID, Status, Access, InviteStatus";
					$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) VALUES ";
					$cnt = 0;
					
					if (isset($viewerList) && sizeof($viewerList) != 0) {
						for ($j=0; $j<sizeof($viewerList); $j++) {
							if($cnt == 0){
								$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$_SESSION['UserID']?'A':'R')."','".$InviteStatus."')";
								$cnt++;
							} else {
								$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$_SESSION['UserID']?'A':'R')."','".$InviteStatus."')";
							}
						}
						$sql = rtrim($sql, ", ");
						if($cnt > 0){
							$result["add_event_new_guest_ALL_$i"] = $iCal->db_db_query($sql);
						}
					}
										
					# Update Existing Guest
					# Individual
					if (sizeof($existGuestList) != 0) {
						for ($k=0; $k<sizeof($existGuestList); $k++) {
							if (!in_array($existGuestList[$k], $removeGuestList) && $existGuestList[$k] != $UserID) {
								$pieces = explode("-",$existGuestList[$k]);
								$sql3  = "UPDATE CALENDAR_EVENT_USER SET InviteStatus = '".$InviteStatus."' ";
								$sql3 .= "WHERE EventID = '".$repeatSeries[$i]["EventID"]."' AND UserID = '".$existGuestList[$k]."' ";//." AND FromSchoolCode is null AND ToSchoolCode is null";
								$result["update_event_exist_guest_ALL_$i_$k"] = $iCal->db_db_query($sql3);
							}
						}
					}
				}
				
				# Update user own status
				$sql  = "UPDATE CALENDAR_EVENT_USER SET ";
				if($userEventAccess == 'A'){	// User with All Access right
					$sql .= "InviteStatus = '$InviteStatus' ";
					$sql .= ($EventType != "1") ? "" : ", Status = '$response' ";
				} else {						// User with only read right
					$sql .= "Status = '$response' ";	
				}
				$sql .= "WHERE EventID = '".$repeatSeries[$i]["EventID"]."' AND UserID = '".$UserID."' ";
				$result["update_event_guest_status_ALL_$i"] = $iCal->db_db_query($sql);
				
				##### EBOOKING ##### (ALL)
				if($plugin['eBooking'])
				{					
					$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID = '".$repeatSeries[$i]["EventID"]."' ";
					$arrEventInfo = $iCal->returnArray($sql,2);
					
					if(sizeof($arrEventInfo)>0)
					{
						for($k=0; $k<sizeof($arrEventInfo); $k++)
						{
							list($event_id, $event_datetime) = $arrEventInfo[$k];
							
							$event_date = substr($event_datetime,0,10);
							$event_time = substr($event_datetime,11,strlen($event_datetime));
							
							$arrEventDate[] = $event_datetime;
							$arrMergeInfo[$event_datetime]['EventID'] = $event_id;
							$arrMergeInfo[$event_datetime]['EventDate'] = $event_date;
							
							### Get New BookingID
							$sql = "SELECT BookingID, CONCAT(Date,' ',StartTime) AS EventDate FROM INTRANET_EBOOKING_RECORD WHERE BookingID in ($hiddenBookingIDs) AND Date = '$event_date' AND StartTime = '$event_time'";
							$arrNewBooking = $iCal->returnArray($sql,2);
							if(sizeof($arrNewBooking)>0) {
								list($new_booking_id, $new_booking_date) = $arrNewBooking[0];
								$arrMergeInfo[$event_datetime]['NewBookingID'] = $new_booking_id;
							} else {
								$arrMergeInfo[$event_datetime]['NewBookingID'] = "";
							}
							
							### Get Old BookingID
							$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID = '".$repeatSeries[$i]["EventID"]."' ";
							$arrOldBooking = $iCal->returnArray($sql,2);
							if(sizeof($arrOldBooking)>0) {
								list($old_booking_id, $old_booking_date) = $arrOldBooking[0];
								$arrMergeInfo[$event_datetime]['OldBookingID'] = $old_booking_id;
							} else {
								$arrMergeInfo[$event_datetime]['OldBookingID'] = "";
							}
						}
					}
					$arrEventDate = array_unique($arrEventDate);
				}
				
				## (CONT.) Event date and repeat pattern are UNCHANGED ##
				## Show in school calendar (Group Event Only) (Click : Save ALL) ##
				if($ShowInSchoolCalendar)
				{
					$iCalendarEventID = $repeatSeries[$i]["EventID"];
					
					$event_date = date("Y-m-d",$iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]));
					
					### GET EventID In School Calendar
					$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID' ";
					$arr_SchoolCalEventID = $iCal->returnVector($sql);
					
					if(sizeof($arr_SchoolCalEventID) > 0)
					{
						$SchoolCalEventID = $arr_SchoolCalEventID[0];
						$arrUpdateCalenderEventID[] = $iCalendarEventID;
					
						### GET GroupID 
						$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\'';
						$arrGroupID = $iCal->returnVector($sql);
						$targetGroupID = $arrGroupID[0];
						
						$sql = "UPDATE INTRANET_EVENT SET ";
						$sql .= "EventDate = '".$event_date."', ";
						$sql .= "RecordType = '2', ";
						$sql .= "Title = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($title))."', ";
						$sql .= "EventVenue = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($location))."', ";
						$sql .= "Description = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description))."', ";
						$sql .= "DateModified = NOW(), ";
						$sql .= "ModifyBy = '".$_SESSION['UserID']."' ";
						$sql .= "WHERE EventID = '".$SchoolCalEventID."' ";
						$iCal->db_db_query($sql);
													
						$sql = "UPDATE INTRANET_GROUPEVENT SET GroupID = '".$targetGroupID."', DateModified = NOW() WHERE EventID = '".$SchoolCalEventID."' ";
						$db->db_db_query($sql);
					}
					else
					{
						$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\'';
						$arrTargetGroupID = $db->returnVector($sql);
						$targetGroupID = $arrTargetGroupID[0]; 
						
						if($targetGroupID != "")
						{
							$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID'";
							$arrCalendarEvent = $iCal->returnArray($sql,2);
							
							if(sizeof($arrCalendarEvent)>0)
							{
								for($i=0; $i<sizeof($arrCalendarEvent); $i++)
								{
									list($calendar_event_id, $calendar_event_date) = $arrCalendarEvent[$i];
									
									//$title = trim(stripslashes(urldecode($title)));
									//$description = trim(stripslashes(urldecode($description)));
									
									$sql = "INSERT INTO 
													INTRANET_EVENT 
													(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy)
											VALUES
													('$calendar_event_date',2,'".$db->Get_Safe_Sql_Query($iCal->Get_Request($title))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($location))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($description))."',0,1,NOW(),'$UserID',NOW(),'$UserID')";
									$iCal->db_db_query($sql);
									
									$ParentSchoolCalEventID = $db->db_insert_id();
									$CurrentSchoolCalEventID = $db->db_insert_id();
												
									$value = "('$targetGroupID','$CurrentSchoolCalEventID',NOW(),NOW())";
									$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
									$db->db_db_query($sql);
								
									$arrSchoolCalendarInfo[$TargetEventDate][] = $CurrentSchoolCalEventID; 
								
									$sql = "UPDATE INTRANET_EVENT 
											SET 
												RelatedTo = '$ParentSchoolCalEventID',
												DateModified = NOW(),
												ModifyBy = '$UserID'
											WHERE 
												EventID = '$CurrentSchoolCalEventID'";
									$db->db_db_query($sql);
									
									$sql = "INSERT INTO CALENDAR_EVENT_SCHOOL_EVENT_RELATION (CalendarEventID, SchoolEventID) VALUES ('$calendar_event_id','$CurrentSchoolCalEventID')";
									$db->db_db_query($sql);
								}
							}
						}
					}
				}
				else
				{
					$iCalendarEventID = $repeatSeries[$i]["EventID"];
					$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
					$arr_SchoolCalEventID = $iCal->returnVector($sql);
					if(sizeof($arr_SchoolCalEventID) > 0)
					{
						$SchoolCalEventID = $arr_SchoolCalEventID[0];
						$sql = "DELETE FROM INTRANET_EVENT WHERE EventID = '".$SchoolCalEventID."'";
						$iCal->db_db_query($sql);
						
						$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID = '".$SchoolCalEventID."'";
						$iCal->db_db_query($sql);
						
						$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
						$iCal->db_db_query($sql);
					}
				}
			}
			// [2014-11-06 Carlos]: Comment the following block of code. Why delete INTRANET_EVENT while the above for loop has update/delete INTRAENT_EVENT ?
			/*
			if(sizeof($arrUpdateCalenderEventID) > 0){
				$targetUpdateCalenderEventID = implode(",",$arrUpdateCalenderEventID);
				$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID NOT IN ($targetUpdateCalenderEventID)";
				$arrNotUpdatedSchoolEventID = $iCal->returnVector($sql);
				if(sizeof($arrNotUpdatedSchoolEventID) > 0)
				{
					$targetNotUpdatedSchoolEventID = implode(",",$arrNotUpdatedSchoolEventID);
					$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN ($targetNotUpdatedSchoolEventID)";
					$iCal->db_db_query($sql);
				}
			}
			*/
			## update school calendar (calendar view)
			if($ShowInSchoolCalendar)
			{ 
				include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
				$lcycleperiods = new libcycleperiods();
				$lcycleperiods->generatePreview();
				$lcycleperiods->generateProduction();
			}
			
			##### EBOOKING ##### (ALL)
			if($plugin['eBooking'])
			{
				include_once($PATH_WRT_ROOT."includes/libebooking.php");
				$lebooking = new libebooking();
				
				foreach($arrEventDate as $key=>$date)
				{
					$targetEventID = $arrMergeInfo[$date]['EventID'];
					$targetEventDate = $arrMergeInfo[$date]['EventDate'];
					$targetNewBookingID = $arrMergeInfo[$date]['NewBookingID'];
					$targetOldBookingID = $arrMergeInfo[$date]['OldBookingID'];
					
					if($targetNewBookingID != "") {
						$targetNewBookingIDAry = explode(',', $targetNewBookingID);
						
						$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($targetNewBookingID) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
						$arrRoomID = $iCal->returnVector($sql);
						$roomId = $arrRoomID[0];
						
//						$bookingInfoAry = $lebooking->Get_Booking_Record_By_BookingID($targetNewBookingIDAry);
//						$bookingDate = $bookingInfoAry[0]['Date'];
//						$bookingStartTime = $bookingInfoAry[0]['StartTime'];
//						$bookingEndTime = $bookingInfoAry[0]['EndTime'];
						
						$RoomNeedApproval[$targetNewBookingID] = $lebooking->Check_If_Booking_Need_Approval($_SESSION['UserID'], '', $roomId);
						$IsRoomAvailable = false;
						
						if ($RoomNeedApproval[$targetNewBookingID]) {
							$RoomBookingStatus = 0;
							
							$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus' WHERE BookingID IN ($targetNewBookingID)";
							$iCal->db_db_query($sql);
							
//							$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus WHERE BookingID IN ($targetNewBookingID)";
//							$iCal->db_db_query($sql);
						} else {
							$RoomBookingStatus = 1;
							
							$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $targetEventDate, $ebookingStartTime, $ebookingEndTime);
							if ($IsRoomAvailable) {
								$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus', ProcessDate = NOW() WHERE BookingID IN ($targetNewBookingID)";
								$iCal->db_db_query($sql);
							}
							
//							$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus, ProcessDate = NOW() WHERE BookingID IN ($targetNewBookingID)";
//							$iCal->db_db_query($sql);
						}
						
						$sql = "UPDATE INTRANET_EBOOKING_RECORD Set StartTime = '".$ebookingStartTime."', EndTime = '".$ebookingEndTime."', Remark = '".$eBookingRecordRemarksSql."' WHERE BookingID = '$targetNewBookingID'";
						$iCal->db_db_query($sql);
						
						
						$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$targetEventID' WHERE BookingID = '$targetNewBookingID'";
						$iCal->db_db_query($sql);
						
						
						
						include_once($PATH_WRT_ROOT."includes/libinventory.php");
						$linventory = new libinventory();
						$sql = "SELECT 
									CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
								FROM
									INVENTORY_LOCATION_BUILDING AS building 
									INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
									INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
									INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
								WHERE
									BookingID IN ($targetNewBookingID)";
						$arrLocation = $linventory->returnVector($sql);
						$location = $arrLocation[0];
						
						$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Location = '$location' WHERE EventID = '$targetEventID'";
						$iCal->db_db_query($sql);
					}
					
					if($targetOldBookingID != "" && $targetOldBookingID != $targetNewBookingID) {
						$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = NULL WHERE BookingID = '$targetOldBookingID'";
						$iCal->db_db_query($sql);
						
						$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
						$iCal->db_db_query($sql);
						
						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
						$iCal->db_db_query($sql);
						
//						$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
//						$iCal->db_db_query($sql);
						
						$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
						$iCal->db_db_query($sql);
					}
					
					if ($RoomNeedApproval[$targetNewBookingID] == false && $IsRoomAvailable) {
						$lebooking->Email_Booking_Result($targetNewBookingID);
					}
				}
			}
		}
		# Event date is changed or repeat pattern is changed
		else if ($userEventAccess!="R" && ($oldEventDate!=$eventDate || $oldRepeatPattern!=$repeatPattern)) 
		{
			#echo "Event Date or Repeat Pattern have been changed!!!\n";
			
			if($CalType == 2){					### delete existing target Group Events in School Calendar 
				### delete old data [start]
				$sql = "SELECT relation.SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION as relation INNER JOIN CALENDAR_EVENT_ENTRY as entry on (relation.CalendarEventID = entry.EventID) WHERE entry.RepeatID = '$OldRepeatID'";
				$arr_SchoolCalEventID = $iCal->returnVector($sql);
				if(sizeof($arr_SchoolCalEventID) > 0)
				{
					//$SchoolCalEventID = $arr_SchoolCalEventID[0];
					$SchoolCalEventID = implode(",",$arr_SchoolCalEventID);
					$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN (".$SchoolCalEventID.")";
					$iCal->db_db_query($sql);
					
					$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN (".$SchoolCalEventID.")";
					$iCal->db_db_query($sql);
					
					$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE SchoolEventID IN (".$SchoolCalEventID.")";
					$iCal->db_db_query($sql);
				}
				### delete old data [end]
			}
			
			$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
			$result['delete_repeat_event_series_ALL'] = $iCal->deleteRepeatEventSeries($repeatID, (($oldRepeatPattern!=$repeatPattern)?TRUE:FALSE));
			
			if ($oldRepeatPattern != $repeatPattern) {
				#echo "Repeat Pattern have been changed!!!\n";
				// save the repeat pattern for future reference
				$fieldname  = "RepeatType, EndDate, Frequency, Detail";
				$detail = "";
				$frequency = 0;
				switch ($repeatSelect) {
					case "DAILY":
						$fieldvalue = "'$repeatSelect', '$repeatEndDate', $repeatDailyDay, NULL";
						$frequency = $repeatDailyDay;
						break;
					case "WEEKDAY":
					case "MONWEDFRI":
					case "TUESTHUR":
						$fieldvalue = "'$repeatSelect', '$repeatEndDate', NULL, NULL";
						break;
					case "WEEKLY":
						$detail = (isset($Sun)?"1":"0").(isset($Mon)?"1":"0").(isset($Tue)?"1":"0").(isset($Wed)?"1":"0");
						$detail .= (isset($Thu)?"1":"0").(isset($Fri)?"1":"0").(isset($Sat)?"1":"0");
						$fieldvalue = "'$repeatSelect', '$repeatEndDate', $repeatWeeklyRange, '$detail'";
						$frequency = $repeatWeeklyRange;
						break;
					case "MONTHLY":
						if ($monthlyRepeatBy == "dayOfMonth")
							$detail = "day-".$datePieces[2];
						else {
							$eventDateWeekday = date("w", $eventTimeStamp);
							$nth = 0;
							for($i=1; $i<6; $i++) {
								$nthweekday = $iCal->NWeekdayOfMonth($i,$eventDateWeekday,$datePieces[1],$datePieces[0],"d");
								if ((int)$datePieces[2] == $nthweekday) {
									$nth = $i;
									break;
								}
							}
							$detail = "$nth-$eventDateWeekday";
						}
						$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatMonthlyRange', '$detail'";
						$frequency = $repeatMonthlyRange;
						break;
					case "YEARLY":
						$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatYearlyRange', NULL";
						$frequency = $repeatYearlyRange;
						break;
				}
				$sql = "INSERT INTO CALENDAR_EVENT_ENTRY_REPEAT ($fieldname) VALUES ($fieldvalue)";
				$result['insert_event_repeat_info_ALL'] = $iCal->db_db_query($sql);
				$repeatID = $iCal->db_insert_id();
			}
			
			
			# Insert the recurrence event series
			$detailArray = array($repeatID, $repeatSelect, $repeatEndDate, $frequency, $detail, $insertUserID,
							$eventDateTime, $duration, $isImportant, $isAllDay, $access, $iCal->Get_Safe_Sql_Query($iCal->Get_Request($title)), 
							$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description)), $iCal->Get_Safe_Sql_Query($iCal->Get_Request($location)), $url, $calID);
			
			if (!isset($monthlyRepeatBy))
				$monthlyRepeatBy = "";
			
			# Main function for inserting recurrence events
			$result['insert_repeat_event_series_ALL'] = $iCal->insertRepeatEventSeries($detailArray, $monthlyRepeatBy, 1, $repeatSeries[0]["UID"], $repeatSeries[0]["ExtraIcalInfo"]);
			
			# insert current personal note into to new event series
			if(!in_array(false, $result)){
				$sql = "INSERT INTO CALENDAR_EVENT_PERSONAL_NOTE (EventID, UserID, PersonalNote, CalType, CreateDate) 
						SELECT EventID, '".$_SESSION['UserID']."', '".$iCal->Get_Request($PersonalNote)."', '$CalType', NOW()
						FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '".$repeatID."'";
				$result['insert_repeat_event_personal_note'] = $iCal->db_db_query($sql);
			}
			
			# Insert reminders
			
			$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore";
			$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ";
			$cnt = 0;
			
			$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
			for ($i=0; $i<sizeof($repeatSeries); $i++) {
				$allEventIdAry[] = $repeatSeries[$i]["EventID"];
				
				$repeatEventTimeStamp = $iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]);//modified on 16 July 2009
				# reminders for event creator
				for ($j=0; $j<sizeof($reminderType); $j++) {
					$reminderDate[$j] = $repeatEventTimeStamp - ((int)$reminderTime[$j])*60;//modified on 17 July 2009
					$reminderDate[$j] = date("Y-m-d H:i:s", $reminderDate[$j]);
					if($cnt == 0){
						$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$UserID."', '".$reminderType[$j]."', '".$reminderDate[$j]."', '".$reminderTime[$j]."')";
						$cnt++;
					} else {
						$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$UserID."', '".$reminderType[$j]."', '".$reminderDate[$j]."', '".$reminderTime[$j]."')";
					}
				}
				
				# reminder for newly add guests
				$curReminderDate = $repeatEventTimeStamp - 10*60;//modified on 17 July 2009
				$curReminderDate = date("Y-m-d H:i:s", $curReminderDate);
				
				if (isset($viewerList) && sizeof($viewerList) != 0) {
					for ($k=0; $k<sizeof($viewerList); $k++) {
						if($cnt == 0){
							$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$k]."', 'P', '$curReminderDate', 10)";
							$cnt++;
						} else {
							$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$k]."', 'P', '$curReminderDate', 10)";
						}
					}
				}
				
				# reminder for existing guests
				if (sizeof($existGuestList) != 0) {
					for ($l=0; $l<sizeof($existGuestList); $l++) {
						if (!in_array($existGuestList[$l], $removeGuestList) && $existGuestList[$l] != $UserID) {
							if($cnt == 0){
								$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$l]."', 'P', '$curReminderDate', 10)";
								$cnt++;
							} else {
								$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$l]."', 'P', '$curReminderDate', 10)";
							}
						}
					}
				}
				
				##### EBOOKING ##### (ALL)
				if($plugin['eBooking'])
				{					
					$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID = '".$repeatSeries[$i]["EventID"]."'";
					$arrEventInfo = $iCal->returnArray($sql,2);
					
					if(sizeof($arrEventInfo)>0)
					{
						for($k=0; $k<sizeof($arrEventInfo); $k++)
						{
							list($event_id, $event_datetime) = $arrEventInfo[$k];
							
							$event_date = substr($event_datetime,0,10);
							$event_time = substr($event_datetime,11,strlen($event_datetime));
							
							$arrEventDate[] = $event_datetime;
							$arrMergeInfo[$event_datetime]['EventID'] = $event_id;
							$arrMergeInfo[$event_datetime]['EventDate'] = $event_date;
							
							### Get New BookingID
							$sql = "SELECT BookingID, CONCAT(Date,' ',StartTime) AS EventDate FROM INTRANET_EBOOKING_RECORD WHERE BookingID in ($hiddenBookingIDs) AND Date = '$event_date' AND StartTime = '$event_time'";
							$arrNewBooking = $iCal->returnArray($sql,2);
							if(sizeof($arrNewBooking)>0) {
								list($new_booking_id, $new_booking_date) = $arrNewBooking[0];
								$arrMergeInfo[$event_datetime]['NewBookingID'] = $new_booking_id;
							} else {
								$arrMergeInfo[$event_datetime]['NewBookingID'] = "";
							}
							
							### Get Old BookingID
							$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID = '".$repeatSeries[$i]["EventID"]."'";
							$arrOldBooking = $iCal->returnArray($sql,2);
							if(sizeof($arrOldBooking)>0) {
								list($old_booking_id, $old_booking_date) = $arrOldBooking[0];
								$arrMergeInfo[$event_datetime]['OldBookingID'] = $old_booking_id;
							} else {
								$arrMergeInfo[$event_datetime]['OldBookingID'] = "";
							}
						}
					}
					$arrEventDate = array_unique($arrEventDate);
				}
				
				## (CONT.) # Event date is changed or repeat pattern is changed ##
				## Show in school calendar (Group Event Only) (Click : Save ALL) ##
				if($ShowInSchoolCalendar)
				{
					$iCalendarEventID = $repeatSeries[$i]["EventID"];
					$event_date = date("Y-m-d",$iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]));
					
					### GET EventID In School Calendar
					$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
					$arr_SchoolCalEventID = $iCal->returnVector($sql);
					
					if(sizeof($arr_SchoolCalEventID) > 0)
					{
						//debug("Update");
						$SchoolCalEventID = $arr_SchoolCalEventID[0];
						
						$arrUpdateCalenderEventID[] = $iCalendarEventID;
					
						### GET GroupID 
						$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\'';
						$arrGroupID = $iCal->returnVector($sql);
						$targetGroupID = $arrGroupID[0];
						
						$sql = "UPDATE INTRANET_EVENT SET ";
						$sql .= "EventDate = '".$event_date."', ";
						$sql .= "RecordType = '2', ";
						$sql .= "Title = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($title))."', ";
						$sql .= "EventVenue = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($location))."', ";
						$sql .= "Description = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description))."', ";
						$sql .= "DateModified = NOW(), ";
						$sql .= "ModifyBy = '".$_SESSION['UserID']."' ";
						$sql .= "WHERE EventID = '".$SchoolCalEventID."' ";
						$iCal->db_db_query($sql);
													
						$sql = "UPDATE INTRANET_GROUPEVENT SET GroupID = '".$targetGroupID."', DateModified = NOW() WHERE EventID = '".$SchoolCalEventID."'";
						$db->db_db_query($sql);
					}
					else
					{
						//debug("Insert");
						$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\'';
						$arrTargetGroupID = $db->returnVector($sql);
						$targetGroupID = $arrTargetGroupID[0]; 
						
						if($targetGroupID != "")
						{
							$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID'";
							$arrCalendarEvent = $iCal->returnArray($sql,2);
							
							if(sizeof($arrCalendarEvent)>0)
							{
								for($i=0; $i<sizeof($arrCalendarEvent); $i++)
								{
									list($calendar_event_id, $calendar_event_date) = $arrCalendarEvent[$i];
									
									//$title = trim(stripslashes(urldecode($title)));
									//$description = trim(stripslashes(urldecode($description)));
									
									$sql = "INSERT INTO 
													INTRANET_EVENT 
													(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy)
											VALUES
													('$calendar_event_date',2,'".$db->Get_Safe_Sql_Query($iCal->Get_Request($title))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($location))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($description))."',0,1,NOW(),'$UserID',NOW(),'$UserID')";
									$iCal->db_db_query($sql);
									
									$ParentSchoolCalEventID = $db->db_insert_id();
									$CurrentSchoolCalEventID = $db->db_insert_id();
												
									$value = "('$targetGroupID','$CurrentSchoolCalEventID',NOW(),NOW())";
									$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
									$db->db_db_query($sql);
									
									$arrSchoolCalendarInfo[$TargetEventDate][] = $CurrentSchoolCalEventID; 
								
									$sql = "UPDATE INTRANET_EVENT 
											SET 
												RelatedTo = '$ParentSchoolCalEventID',
												DateModified = NOW(),
												ModifyBy = '$UserID'
											WHERE 
												EventID = '$CurrentSchoolCalEventID'";
									$db->db_db_query($sql);
									
									$sql = "INSERT INTO CALENDAR_EVENT_SCHOOL_EVENT_RELATION (CalendarEventID, SchoolEventID) VALUES ('$calendar_event_id','$CurrentSchoolCalEventID')";
									$db->db_db_query($sql);
								}
							}
						}
					}
				}
				else
				{
					//debug("Delete");
					$iCalendarEventID = $repeatSeries[$i]["EventID"];
					$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
					$arr_SchoolCalEventID = $iCal->returnVector($sql);
					if(sizeof($arr_SchoolCalEventID) > 0)
					{
						$SchoolCalEventID = $arr_SchoolCalEventID[0];
						$sql = "DELETE FROM INTRANET_EVENT WHERE EventID = '".$SchoolCalEventID."' ";
						$iCal->db_db_query($sql);
						
						$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID = '".$SchoolCalEventID."' ";
						$iCal->db_db_query($sql);
						
						$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
						$iCal->db_db_query($sql);
					}
				}
			}
			// [2014-11-06 Carlos]: Comment the following block of code. Why delete INTRANET_EVENT while the above for loop has update/delete INTRAENT_EVENT ?
			/*
			if(sizeof($arrUpdateCalenderEventID) > 0){
				$targetUpdateCalenderEventID = implode(",",$arrUpdateCalenderEventID);
				$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID NOT IN ($targetUpdateCalenderEventID)";
				$arrNotUpdatedSchoolEventID = $iCal->returnVector($sql);
				if(sizeof($arrNotUpdatedSchoolEventID) > 0)
				{
					$targetNotUpdatedSchoolEventID = implode(",",$arrNotUpdatedSchoolEventID);
					$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN ($targetNotUpdatedSchoolEventID)";
					$iCal->db_db_query($sql);
				}
			}
			*/
			## update school calendar (calendar view)
			if($ShowInSchoolCalendar)
			{ 
				include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
				$lcycleperiods = new libcycleperiods();
				$lcycleperiods->generatePreview();
				$lcycleperiods->generateProduction();
			}
			
			if($plugin['eBooking'])
			{
				include_once($PATH_WRT_ROOT."includes/libebooking.php");
				$lebooking = new libebooking();
				foreach($arrEventDate as $key=>$date)
				{
					$targetEventID = $arrMergeInfo[$date]['EventID'];
					$targetEventDate = $arrMergeInfo[$date]['EventDate'];
					$targetNewBookingID = $arrMergeInfo[$date]['NewBookingID'];
					$targetOldBookingID = $arrMergeInfo[$date]['OldBookingID'];
					
					
					if($targetNewBookingID != "") {
						$targetNewBookingIDAry = explode(',', $targetNewBookingID);
						
						$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($targetNewBookingID) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
						$arrRoomID = $iCal->returnVector($sql);
						$roomId = $arrRoomID[0];
						
//						$bookingInfoAry = $lebooking->Get_Booking_Record_By_BookingID($targetNewBookingIDAry);
//						$bookingDate = $bookingInfoAry[0]['Date'];
//						$bookingStartTime = $bookingInfoAry[0]['StartTime'];
//						$bookingEndTime = $bookingInfoAry[0]['EndTime'];
						
						$RoomNeedApproval[$targetNewBookingID] = $lebooking->Check_If_Booking_Need_Approval($_SESSION['UserID'], '', $roomId);
						$IsRoomAvailable = false;
						
						if ($RoomNeedApproval[$targetNewBookingID]) {
							$RoomBookingStatus = 0;
							
							$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus' WHERE BookingID IN ($targetNewBookingID)";
							$iCal->db_db_query($sql);
							
//							$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus WHERE BookingID IN ($targetNewBookingID)";
//							$iCal->db_db_query($sql);
						} else {
							$RoomBookingStatus = 1;
							
							$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $targetEventDate, $ebookingStartTime, $ebookingEndTime);
							if ($IsRoomAvailable) {
								$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus', ProcessDate = NOW() WHERE BookingID IN ($targetNewBookingID)";
								$iCal->db_db_query($sql);
							}
							
//							$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus, ProcessDate = NOW() WHERE BookingID IN ($targetNewBookingID)";
//							$iCal->db_db_query($sql);
						}
						
						$sql = "UPDATE INTRANET_EBOOKING_RECORD Set StartTime = '".$ebookingStartTime."', EndTime = '".$ebookingEndTime."', Remark = '".$eBookingRecordRemarksSql."' WHERE BookingID = '$targetNewBookingID'";
						$iCal->db_db_query($sql);
						
						$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$targetEventID' WHERE BookingID = '$targetNewBookingID'";
						$iCal->db_db_query($sql);
						
						include_once($PATH_WRT_ROOT."includes/libinventory.php");
						$linventory = new libinventory();
						$sql = "SELECT 
									CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
								FROM
									INVENTORY_LOCATION_BUILDING AS building 
									INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
									INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
									INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
								WHERE
									BookingID IN ($targetNewBookingID)";
						$arrLocation = $linventory->returnVector($sql);
						$location = $arrLocation[0];
						
						$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Location = '$location' WHERE EventID = '$targetEventID'";
						$iCal->db_db_query($sql);
					}
					
					if($targetOldBookingID != "" && $targetOldBookingID != $targetNewBookingID) {
						$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = NULL WHERE BookingID = '$targetOldBookingID'";
						$iCal->db_db_query($sql);
						
						$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
						$iCal->db_db_query($sql);
						
						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
						$iCal->db_db_query($sql);
						
//						$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
//						$iCal->db_db_query($sql);
						
						$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
						$iCal->db_db_query($sql);
					}
					
					if ($RoomNeedApproval[$targetNewBookingID] == false && $IsRoomAvailable) {
						$lebooking->Email_Booking_Result($targetNewBookingID);
					}
				}
			}
			
			$sql = rtrim($sql, ", ");
			if($cnt > 0){
				$result['insert_event_reminder_ALL'] = $iCal->db_db_query($sql);
			}
			
			# Insert guests
			$fieldname1  = "EventID, UserID, Status, Access, InviteStatus";
			$sql1 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname1) VALUES ";
			$cnt1 = 0;
			
			for ($i=0; $i<sizeof($repeatSeries); $i++) {
				if($cnt1 == 0){
					$sql1 .= " ('".$repeatSeries[$i]["EventID"]."', '".$UserID."', 'A', 'A', '".$InviteStatus."')";
					$cnt1++;
				} else {
					$sql1 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$UserID."', 'A', 'A', '".$InviteStatus."')";
				}
				
				if (isset($viewerList) && sizeof($viewerList) != 0) {
					$fieldname2  = "EventID, UserID, Status, Access, InviteStatus";
					$sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) VALUES ";
					$cnt2 = 0;
					for ($j=0; $j<sizeof($viewerList); $j++) {
						if($cnt2 == 0){
							$sql2 .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$_SESSION['UserID']?'A':'R')."', '".$InviteStatus."')";
							$cnt2++;
						} else {
							$sql2 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$_SESSION['UserID']?'A':'R')."', '".$InviteStatus."')";
						}
					}
					$sql2 = rtrim($sql2, ", ");
					if($cnt2 > 0){
						$result["insert_event_guest_ALL_$i"] = $iCal->db_db_query($sql2);
					}
				}
				
				#other school guest
				
				if (sizeof($existGuestList) != 0) {
					$fieldname3  = "EventID, UserID, Status, Access, InviteStatus";
					$sql3 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname3) VALUES ";
					$cnt3 = 0;
					$islocal = false;
					for ($k=0; $k<sizeof($existGuestList); $k++) {
						if (!in_array($existGuestList[$k], $removeGuestList) && $existGuestList[$k] != $UserID) {
							$pieces = explode("-",$existGuestList[$k]);
							$islocal  = true;
							if($cnt3 == 0){
								$sql3 .= " ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$k]."', '".($existGuestList[$k]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($existGuestList[$k]==$_SESSION['UserID']?'A':'R')."', '".$InviteStatus."')";
								$cnt3++;
							} else {
								$sql3 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$k]."', '".($existGuestList[$k]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($existGuestList[$k]==$_SESSION['UserID']?'A':'R')."', '".$InviteStatus."')";
							}
						}
					}
					if ($islocal){
						$sql3 = rtrim($sql3, ", ");
						if ($sql3 != "INSERT INTO CALENDAR_EVENT_USER ($fieldname3) VALUES ") {
							$result["insert_exist_event_guest_ALL_$i"] = $iCal->db_db_query($sql3);
						}
					}
				}
			}
			$sql1 = rtrim($sql1, ", ");
//			$result["insert_event_owner_ALL"] = $iCal->db_db_query($sql1);
		}
	} 
	else if ($userEventAccess!="R" &&  $saveRepeatEvent=="FOL")
	{
		#echo "Save ALL FOLLOWING events"."\n";
		
		# delete this and following events from the original recurrence series
		$cutOffDate = $oldEventDate." 00:00:00";
		
		if($plugin['eBooking']) {
			$sql = "SELECT EventID FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID' AND EventDate >= '$cutOffDate'";
			$arrOldEventID = $iCal->returnVector($sql);
		}
		
		if($CalType == 2){					### delete existing target Group Events in School Calendar 
			### delete old data [start]
			$sql = "SELECT relation.SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION as relation INNER JOIN CALENDAR_EVENT_ENTRY as entry on (relation.CalendarEventID = entry.EventID) WHERE entry.RepeatID = '$OldRepeatID' AND entry.EventDate >= '$cutOffDate'";
			$arr_SchoolCalEventID = $iCal->returnVector($sql);
			if(sizeof($arr_SchoolCalEventID) > 0)
			{
				$SchoolCalEventID = implode(",",$arr_SchoolCalEventID);
				$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN (".$SchoolCalEventID.")";
				$iCal->db_db_query($sql);
								
				$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN (".$SchoolCalEventID.")";
				$iCal->db_db_query($sql);
								
				$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE SchoolEventID IN (".$SchoolCalEventID.")";
				$iCal->db_db_query($sql);
			}
			### delete old data [end]
		}
		
		$result["delete_repeat_fol_events"] = $iCal->deleteRepeatFollowingEvents($cutOffDate, $repeatID);
		
		# save the new repeat pattern
		$fieldname  = "RepeatType, EndDate, Frequency, Detail";
		$detail = "";
		$frequency = 0;
		switch ($repeatSelect) {
			case "DAILY":
				$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatDailyDay', NULL";
				$frequency = $repeatDailyDay;
				break;
			case "WEEKDAY":
			case "MONWEDFRI":
			case "TUESTHUR":
				$fieldvalue = "'$repeatSelect', '$repeatEndDate', NULL, NULL";
				break;
			case "WEEKLY":
				$detail = (isset($Sun)?"1":"0").(isset($Mon)?"1":"0").(isset($Tue)?"1":"0").(isset($Wed)?"1":"0");
				$detail .= (isset($Thu)?"1":"0").(isset($Fri)?"1":"0").(isset($Sat)?"1":"0");
				$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatWeeklyRange', '$detail'";
				$frequency = $repeatWeeklyRange;
				break;
			case "MONTHLY":
				if ($monthlyRepeatBy == "dayOfMonth")
					$detail = "day-".$datePieces[2];
				else {
					$eventDateWeekday = date("w", $eventTimeStamp);
					$nth = 0;
					for($i=1; $i<6; $i++) {
						$nthweekday = $iCal->NWeekdayOfMonth($i,$eventDateWeekday,$datePieces[1],$datePieces[0],"d");
						if ((int)$datePieces[2] == $nthweekday) {
							$nth = $i;
							break;
						}
					}
					$detail = "$nth-$eventDateWeekday";
				}
				$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatMonthlyRange', '$detail'";
				$frequency = $repeatMonthlyRange;
				break;
			case "YEARLY":
				$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatYearlyRange', NULL";
				$frequency = $repeatYearlyRange;
				break;
		}
		$sql = "INSERT INTO CALENDAR_EVENT_ENTRY_REPEAT ($fieldname) VALUES ($fieldvalue)";
		$result["insert_event_repeat_info_FOL"] = $iCal->db_db_query($sql);
		$repeatID = $iCal->db_insert_id();
		
		# Insert the new recurrence event series
		$detailArray = array($repeatID, $repeatSelect, $repeatEndDate, $frequency, $detail, $insertUserID,
						$eventDateTime, $duration, $isImportant, $isAllDay, $access, $iCal->Get_Safe_Sql_Query($iCal->Get_Request($title)), 
						$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description)), $iCal->Get_Safe_Sql_Query($iCal->Get_Request($location)), $url, $calID);
		
		if (!isset($monthlyRepeatBy))
			$monthlyRepeatBy = "";
		
		# Main function for inserting recurrence events
		$result["insert_repeat_event_series_FOL"] = $iCal->insertRepeatEventSeries($detailArray, $monthlyRepeatBy, 1);
		
		$sqlR = "select * from CALENDAR_EVENT_ENTRY where RepeatID = '$repeatID' Order by EventID";
		$resultSet = $iCal->returnArray($sqlR);
		$newUID = $_SESSION['UserID']."-".$resultSet[0]['CalID'].'-'.$resultSet[0]['EventID']."@".$_SESSION["SchoolCode"].".tg";
		$sqlR = "update CALENDAR_EVENT_ENTRY set UID = '$newUID' where RepeatID = '$repeatID'";
		$result["insert_repeat_event_series_FOL_insert_UID"] = $iCal->db_db_query($sqlR);
		
		# insert current personal note into to new event series
		if(!in_array(false, $result)){
			$sql = "INSERT INTO CALENDAR_EVENT_PERSONAL_NOTE (EventID, UserID, CalType, PersonalNote, CreateDate) 
					SELECT EventID, '".$_SESSION['UserID']."', '$CalType', '".($iCal->Get_Request($PersonalNote))."', NOW()
					FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = ".$repeatID;
			$result['insert_repeat_event_personal_note'] = $iCal->db_db_query($sql);
		}
		# get the EventIDs of the recurrence series 
		$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
		
		# Insert reminder
		$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore";
		$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ";
		$cnt = 0;
		for ($i=0; $i<sizeof($repeatSeries); $i++) {
			$allEventIdAry[] = $repeatSeries[$i]["EventID"];
			
			$repeatEventTimeStamp = $iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]);//modified on 17 July 2009
			for ($j=0; $j<sizeof($reminderType); $j++) {
				$reminderDate[$j] = $repeatEventTimeStamp - ((int)$reminderTime[$j])*60;//modified on 17 July 2009
				$reminderDate[$j] = date("Y-m-d H:i:s", $reminderDate[$j]);
				if($cnt == 0){
					$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$UserID."', '".$reminderType[$j]."', '".$reminderDate[$j]."', '".$reminderTime[$j]."')";
					$cnt++;
				} else {
					$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$UserID."', '".$reminderType[$j]."', '".$reminderDate[$j]."', '".$reminderTime[$j]."')";
				}
			}
			
			$curReminderDate = $repeatEventTimeStamp - 10*60;//modified on 17 July 2009
			$curReminderDate = date("Y-m-d H:i:s", $curReminderDate);
			
			# reminder for newly add guests
			if (isset($viewerList) && sizeof($viewerList) != 0) {
				for ($k=0; $k<sizeof($viewerList); $k++) {
					if($cnt == 0){
						$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$k]."', 'P', '$curReminderDate', 10)";
						$cnt++;
					} else {
						$sql .= ", ('".$repeatSeries[$i]["EventID"]."', ".$viewerList[$k].", 'P', '$curReminderDate', 10)";
					}
				}
			}

			# reminder for existing guests
			if (sizeof($existGuestList) != 0) {
				for ($l=0; $l<sizeof($existGuestList); $l++) {
					if (!in_array($existGuestList[$l], $removeGuestList) && $existGuestList[$l] != $UserID) {
						if($cnt == 0){
							$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$l]."', 'P', '$curReminderDate', 10";
							$cnt++;
						} else {
							$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$l]."', 'P', '$curReminderDate', 10)";
						}
					}
				}
			}
		}
		$sql = rtrim($sql, ", ");
		if($cnt > 0){
			$result['insert_event_reminder_FOL'] = $iCal->db_db_query($sql);
		}
		
		# Insert guests
		$fieldname1  = "EventID, UserID, Status, Access, InviteStatus";
		$sql1 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname1) VALUES ";
		$cnt1 = 0;
		
		for ($i=0; $i<sizeof($repeatSeries); $i++) {
			if($cnt1 == 0){
				$sql1 .= " ('".$repeatSeries[$i]["EventID"]."', '".$UserID."', 'A', 'A', '".$InviteStatus."')";
				$cnt1++;
			} else {
				$sql1 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$UserID."', 'A', 'A', '".$InviteStatus."')";
			}
			
			# Add new guests
			if (isset($viewerList) && sizeof($viewerList) != 0) {
				$fieldname2  = "EventID, UserID, Status, Access, InviteStatus";
				$sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) VALUES ";
				$cnt2 = 0;
				for ($j=0; $j<sizeof($viewerList); $j++) {
					if($cnt2 == 0){
						$sql2 .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$_SESSION['UserID']?'A':'R')."', '".$InviteStatus."')";
						$cnt2++;
					} else {
						$sql2 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$_SESSION['UserID']?'A':'R')."', '".$InviteStatus."')";
					}
				}
				$sql2 = rtrim($sql2, ", ");
				if($cnt2 > 0){
					$result["insert_event_new_guest_FOL_$i"] = $iCal->db_db_query($sql2);
				}
			}
			
			# Add existing guests
			if (sizeof($existGuestList) != 0) {
				$fieldname3  = "EventID, UserID, Status, Access, InviteStatus";
				$sql3 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname3) VALUES ";
				$cnt3 = 0;
				$isLocal = false; 
				for ($k=0; $k<sizeof($existGuestList); $k++) {
					if (!in_array($existGuestList[$k], $removeGuestList) && $existGuestList[$k] != $UserID) {
						$pieces = explode("-",$existGuestList[$k]);
						$isLocal = true;
						if($cnt3 == 0){
							$sql3 .= " ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$k]."', '".($existGuestList[$k]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($existGuestList[$k]==$_SESSION['UserID']?'A':'R')."', '".$InviteStatus."')";
							$cnt3++;
						} else {
							$sql3 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$k]."', '".($existGuestList[$k]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($existGuestList[$k]==$_SESSION['UserID']?'A':'R')."', '".$InviteStatus."')";
						}
					}
				}
				if ($isLocal){
					$sql3 = rtrim($sql3, ", ");
					if ($sql3 != "INSERT INTO CALENDAR_EVENT_USER ($fieldname3) ") {
						$result["insert_event_user_status_FOL"] = $iCal->db_db_query($sql3);
					}
				}
			}
			
			##### EBOOKING ##### (FOL)
			if($plugin['eBooking'])
			{
				if($hiddenBookingIDs != "")
				{
					$targetOldEventID = implode(",",$arrOldEventID);
					
					$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID IN ($targetOldEventID)";
					$arrOldBookingID = $iCal->returnVector($sql);
					$strOldBookingID = implode(",",$arrOldBookingID);
					$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = NULL WHERE BookingID IN ($strOldBookingID)";
					$iCal->db_db_query($sql); 
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 999 WHERE BookingID IN ($strOldBookingID)";
					$iCal->db_db_query($sql); 
//					$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 999 WHERE BookingID IN ($strOldBookingID)";
//					$iCal->db_db_query($sql); 
					
					$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID = '".$repeatSeries[$i]["EventID"]."'";
					$arrEventInfo = $iCal->returnArray($sql,2);
					
					if(sizeof($arrEventInfo)>0)
					{
						for($k=0; $k<sizeof($arrEventInfo); $k++)
						{
							list($event_id, $event_datetime) = $arrEventInfo[$k];
							$event_date = substr($event_datetime,0,10);
							$event_time = substr($event_datetime,11,strlen($event_datetime));
							
							$arrEventDate[] = $event_datetime;
							$arrMergeInfo[$event_datetime]['EventID'] = $event_id;
							$arrMergeInfo[$event_datetime]['EventDate'] = $event_date;

							### Get New BookingID
							$sql = "SELECT BookingID, CONCAT(Date,' ',StartTime) AS EventDate FROM INTRANET_EBOOKING_RECORD WHERE BookingID in ($hiddenBookingIDs) AND Date = '$event_date' AND StartTime = '$event_time'";
							$arrNewBooking = $iCal->returnArray($sql,2);
							if(sizeof($arrNewBooking)>0) {
								list($new_booking_id, $new_booking_date) = $arrNewBooking[0];
								$arrMergeInfo[$event_datetime]['NewBookingID'] = $new_booking_id;
								$arrAssignedBookingID[] = $new_booking_id;
							} else {
								$arrMergeInfo[$event_datetime]['NewBookingID'] = "";
							}
							
							### Get Old BookingID
							$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID = '".$repeatSeries[$i]["EventID"]."'";
							$arrOldBooking = $iCal->returnArray($sql,2);
							if(sizeof($arrOldBooking)>0) {
								list($old_booking_id, $old_booking_date) = $arrOldBooking[0];
								$arrMergeInfo[$event_datetime]['OldBookingID'] = $old_booking_id;
							} else {
								$arrMergeInfo[$event_datetime]['OldBookingID'] = "";
							}
						}
					}
					$arrEventDate = array_unique($arrEventDate);
				}
			}
			
			## (CONT.)Save ALL FOLLOWING events ##
			## Show in school calendar (Group Event Only) ##
			if($ShowInSchoolCalendar)
			{
				$iCalendarEventID = $repeatSeries[$i]["EventID"];
				$event_date = date("Y-m-d",$iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]));
				
				### GET EventID In School Calendar
				$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
				$arr_SchoolCalEventID = $iCal->returnVector($sql);
				
				if(sizeof($arr_SchoolCalEventID) > 0)
				{
					//debug("Update");
					$SchoolCalEventID = $arr_SchoolCalEventID[0];
					
					$arrUpdateCalenderEventID[] = $iCalendarEventID;
				
					### GET GroupID 
					$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\' ';
					$arrGroupID = $iCal->returnVector($sql);
					$targetGroupID = $arrGroupID[0];
					
					$sql = "UPDATE INTRANET_EVENT SET ";
					$sql .= "EventDate = '".$event_date."', ";
					$sql .= "RecordType = '2', ";
					$sql .= "Title = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($title))."', ";
					$sql .= "EventVenue = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($location))."', ";
					$sql .= "Description = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description))."', ";
					$sql .= "DateModified = NOW(), ";
					$sql .= "ModifyBy = '".$_SESSION['UserID']."' ";
					$sql .= "WHERE EventID = '".$SchoolCalEventID."' ";
					$iCal->db_db_query($sql);
												
					$sql = "UPDATE INTRANET_GROUPEVENT SET GroupID = '".$targetGroupID."', DateModified = NOW() WHERE EventID = '".$SchoolCalEventID."' ";
					$db->db_db_query($sql);
				}
				else
				{
					//debug("Insert");
					$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\' ';
					$arrTargetGroupID = $db->returnVector($sql);
					$targetGroupID = $arrTargetGroupID[0]; 
					
					if($targetGroupID != "")
					{
						$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID'";
						$arrCalendarEvent = $iCal->returnArray($sql,2);
						
						if(sizeof($arrCalendarEvent)>0)
						{
							for($i=0; $i<sizeof($arrCalendarEvent); $i++)
							{
								list($calendar_event_id, $calendar_event_date) = $arrCalendarEvent[$i];
								
								//$title = trim(stripslashes(urldecode($title)));
								//$description = trim(stripslashes(urldecode($description)));
								
								$sql = "INSERT INTO 
												INTRANET_EVENT 
												(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy)
										VALUES
												('$calendar_event_date',2,'".$db->Get_Safe_Sql_Query($iCal->Get_Request($title))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($location))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($description))."',0,1,NOW(),'$UserID',NOW(),'$UserID')";
								$iCal->db_db_query($sql);
								
								$ParentSchoolCalEventID = $db->db_insert_id();
								$CurrentSchoolCalEventID = $db->db_insert_id();
											
								$value = "('$targetGroupID','$CurrentSchoolCalEventID',NOW(),NOW())";
								$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
								$db->db_db_query($sql);
								
								$arrSchoolCalendarInfo[$TargetEventDate][] = $CurrentSchoolCalEventID; 
							
								$sql = "UPDATE INTRANET_EVENT 
										SET 
											RelatedTo = '$ParentSchoolCalEventID',
											DateModified = NOW(),
											ModifyBy = '$UserID'
										WHERE 
											EventID = '$CurrentSchoolCalEventID'";
								$db->db_db_query($sql);
								
								$sql = "INSERT INTO CALENDAR_EVENT_SCHOOL_EVENT_RELATION (CalendarEventID, SchoolEventID) VALUES ('$calendar_event_id','$CurrentSchoolCalEventID')";
								$db->db_db_query($sql);
							}
						}
					}
				}
			}
			else
			{
				//debug("Delete");
				$iCalendarEventID = $repeatSeries[$i]["EventID"];
				$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
				$arr_SchoolCalEventID = $iCal->returnVector($sql);
				if(sizeof($arr_SchoolCalEventID) > 0)
				{
					$SchoolCalEventID = $arr_SchoolCalEventID[0];
					$sql = "DELETE FROM INTRANET_EVENT WHERE EventID = '".$SchoolCalEventID."' ";
					$iCal->db_db_query($sql);
					
					$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID = '".$SchoolCalEventID."' ";
					$iCal->db_db_query($sql);
					
					$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
					$iCal->db_db_query($sql);
				}
			}
		}
		// [2014-11-06 Carlos]: Comment the following block of code. Why delete INTRANET_EVENT while the above for loop has update/delete INTRAENT_EVENT ?
		/*
		if(sizeof($arrUpdateCalenderEventID) > 0){
			$targetUpdateCalenderEventID = implode(",",$arrUpdateCalenderEventID);
			$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID NOT IN ($targetUpdateCalenderEventID)";
			$arrNotUpdatedSchoolEventID = $iCal->returnVector($sql);
			if(sizeof($arrNotUpdatedSchoolEventID) > 0)
			{
				$targetNotUpdatedSchoolEventID = implode(",",$arrNotUpdatedSchoolEventID);
				$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN ($targetNotUpdatedSchoolEventID)";
				$iCal->db_db_query($sql);
			}
		}
		*/
		## update school calendar (calendar view)
		if($ShowInSchoolCalendar)
		{ 
			include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
			$lcycleperiods = new libcycleperiods();
			$lcycleperiods->generatePreview();
			$lcycleperiods->generateProduction();
		}
		
		$sql1 = rtrim($sql1, ", ");
//		$result["insert_event_owner_FOL"] = $iCal->db_db_query($sql1);
		
		##### EBOOOKING #####
		if($plugin['eBooking'])
		{
			if(sizeof($arrEventDate)>0)
			{
				foreach($arrEventDate as $key=>$date)
				{
					$targetEventID = $arrMergeInfo[$date]['EventID'];
					$targetEventDate = $arrMergeInfo[$date]['EventDate'];
					$targetNewBookingID = $arrMergeInfo[$date]['NewBookingID'];
					$targetOldBookingID = $arrMergeInfo[$date]['OldBookingID'];
					
					if($targetNewBookingID != "") {
						$targetNewBookingIDAry = explode(',', $targetNewBookingID);
						
						include_once($PATH_WRT_ROOT."includes/libebooking.php");
						$lebooking = new libebooking();
						$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($targetNewBookingID) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
						$arrRoomID = $iCal->returnVector($sql);
						$roomId = $arrRoomID[0];
						
//						$bookingInfoAry = $lebooking->Get_Booking_Record_By_BookingID($targetNewBookingIDAry);
//						$bookingDate = $bookingInfoAry[0]['Date'];
//						$bookingStartTime = $bookingInfoAry[0]['StartTime'];
//						$bookingEndTime = $bookingInfoAry[0]['EndTime'];
						
						$RoomNeedApproval[$targetNewBookingID] = $lebooking->Check_If_Booking_Need_Approval($_SESSION['UserID'], '', $roomId);
						$IsRoomAvailable = false;
						if ($RoomNeedApproval[$targetNewBookingID]) {
							$RoomBookingStatus = 0;
							
							$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus' WHERE BookingID IN ($targetNewBookingID)";
							$iCal->db_db_query($sql);
							
//							$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus WHERE BookingID IN ($targetNewBookingID)";
//							$iCal->db_db_query($sql);
						} else {
							$RoomBookingStatus = 1;
							
							$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $targetEventDate, $ebookingStartTime, $ebookingEndTime);
							if ($IsRoomAvailable) {
								$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus', ProcessDate = NOW() WHERE BookingID IN ($targetNewBookingID)";
								$iCal->db_db_query($sql);
							}
							
//							$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus, ProcessDate = NOW() WHERE BookingID IN ($targetNewBookingID)";
//							$iCal->db_db_query($sql);
						}
						
						$sql = "UPDATE INTRANET_EBOOKING_RECORD Set StartTime = '".$ebookingStartTime."', EndTime = '".$ebookingEndTime."', Remark = '".$eBookingRecordRemarksSql."' WHERE BookingID = '$targetNewBookingID'";
						$iCal->db_db_query($sql);
						
						$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$targetEventID' WHERE BookingID = '$targetNewBookingID'";
						$iCal->db_db_query($sql);
						
						include_once($PATH_WRT_ROOT."includes/libinventory.php");
						$linventory = new libinventory();
						$sql = "SELECT 
									CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
								FROM
									INVENTORY_LOCATION_BUILDING AS building 
									INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
									INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
									INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
								WHERE
									BookingID IN ($targetNewBookingID)";
						$arrLocation = $linventory->returnVector($sql);
						$location = $arrLocation[0];
						
						$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Location = '$location' WHERE EventID = '$targetEventID'";
						$iCal->db_db_query($sql);
					}
					
					if($targetOldBookingID != "" && $targetOldBookingID != $targetNewBookingID) {
						$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = NULL WHERE BookingID = '$targetOldBookingID'";
						$iCal->db_db_query($sql);
						
						$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
						$iCal->db_db_query($sql);
						
						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
						$iCal->db_db_query($sql);
						
//						$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
//						$iCal->db_db_query($sql);
						
						$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
						$iCal->db_db_query($sql);
					}
					$arrAvailableBookingID = explode(",",$hiddenBookingIDs);
					$arrNotAssignedBookingID = array_diff($arrAvailableBookingID, $arrAssignedBookingID);
					
//					if(sizeof($arrNotAssignedBookingID) > 0){
//						$targetNotAssignedBookingID = implode(",",$arrNotAssignedBookingID);
//						$sql = "DELETE FROM INTRANET_EBOOKING_RECORD WHERE BookingID IN ($targetNotAssignedBookingID)";
//						$iCal->db_db_query($sql);
//						
//						$sql = "DELETE FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($targetNotAssignedBookingID)";
//						$iCal->db_db_query($sql);
//						
////						$sql = "DELETE FROM INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS WHERE BookingID IN ($targetNotAssignedBookingID)";
////						$iCal->db_db_query($sql);
//						
//						$sql = "DELETE FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID IN ($targetNotAssignedBookingID)";
//						$iCal->db_db_query($sql);
//					}
					
					if ($RoomNeedApproval[$targetNewBookingID] == false && $IsRoomAvailable) {
						$lebooking->Email_Booking_Result($targetNewBookingID);
					}
				}
			}
		}
	}
	
	if($sys_custom['iCalendar']['EventTitleWithParticipantNames']){
		for($i=0;$i<count($allEventIdAry);$i++)
		{
			$title_extra_info = $iCal->getEventTitleExtraInfo($allEventIdAry[$i],1);
			$iCal->updateEventTitleWithExtraInfo($allEventIdAry[$i], $iCal->Get_Request($title), $title_extra_info);
		}
	}
} 
else 	# non-repeat event OR repeat event but save only this instance
{ 
	#echo "I'm a normal event or save as OTI"."\n";
	if ($userEventAccess=="A" || $userEventAccess=="W")
	{
		if($CalType == 2){					### delete existing target Group Events in School Calendar 
			### delete old data [start]
			$sql = "SELECT relation.SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION as relation INNER JOIN CALENDAR_EVENT_ENTRY as entry on (relation.CalendarEventID = entry.EventID) WHERE entry.EventID = '$eventID'";
			$arr_SchoolCalEventID = $iCal->returnVector($sql);
			if(sizeof($arr_SchoolCalEventID) > 0)
			{
				$SchoolCalEventID = implode(",",$arr_SchoolCalEventID);
				$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN (".$SchoolCalEventID.")";
				$iCal->db_db_query($sql);
								
				$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN (".$SchoolCalEventID.")";
				$iCal->db_db_query($sql);
								
				$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE SchoolEventID IN (".$SchoolCalEventID.")";
				$iCal->db_db_query($sql);
			}
			### delete old data [end]
		}
		
		# Update main detail
		$sql  = "UPDATE CALENDAR_EVENT_ENTRY SET ";
		
		# Edit recurrence event - only this instance - change RepeatID to NULL
		if (isset($isRepeatEvent) && $saveRepeatEvent=="OTI") {
			#echo "Save as OTI"."\n";
			$sql2 = "select * from CALENDAR_EVENT_ENTRY where EventId = '$eventID'";
			$rt = $iCal->returnArray($sql2);
			$rString = "RECURRENCE-ID;TZID=Asia/Hong_Kong:".date("Yms\THis",strtotime($rt[0]["EventDate"]));
			if (!strstr($rt[0]["ExtraIcalInfo"],"RECURRENCE-ID")){
				$original = $rt[0]["ExtraIcalInfo"];
				$rString = str_replace($string,$rString,$original);
				$sql .= "ExtraIcalInfo = '$rString', ";
			}
			
			$sql .= "RepeatID = NULL, ";
		}

		$sql .= "EventDate = '$eventDateTime', ";
		$sql .= "ModifiedDate = NOW(), ";
		$sql .= "Duration = '$duration', ";
		$sql .= "IsImportant = '$isImportant', ";
		$sql .= "IsAllDay = '$isAllDay', ";
		$sql .= "Access = '$access', ";
		$sql .= "Title = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($title))."', ";
		$sql .= "Description = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description))."', ";
		$sql .= "Location = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($location))."', ";
		$sql .= "Url = '$url', ";
		$sql .= "CalID = '$calID' ";
		$sql .= "WHERE EventID = '$eventID'";
		$result['update_event_entry'] = $iCal->db_db_query($sql);
		
		// update personal note for this event and user
		$result['update_event_personal_note'] = $iCal->Insert_Personal_Note($eventID, $iCal->Get_Request($PersonalNote), $CalType, '');
		
		# Normal event -> recurrence event
		if (isset($repeatSelect) && $repeatSelect != "NOT" && !isset($isRepeatEvent)) 
		{
			#echo "Normal event -> recurrence event"."\n";
			# convert repeat end date to datetime format
			$repeatEndDatePieces = explode("-", $repeatEnd);
			$repeatEndTime = mktime(0,0,0,$repeatEndDatePieces[1],$repeatEndDatePieces[2],$repeatEndDatePieces[0]);
			$repeatEndDate = date("Y-m-d H:i:s", $repeatEndTime);
			
			# save the repeat pattern for future reference
			$fieldname  = "RepeatType, EndDate, Frequency, Detail";
			$detail = "";
			$frequency = 0;
			switch ($repeatSelect) {
				case "DAILY":
					$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatDailyDay', NULL";
					$frequency = $repeatDailyDay;
					break;
				case "WEEKDAY":
				case "MONWEDFRI":
				case "TUESTHUR":
					$fieldvalue = "'$repeatSelect', '$repeatEndDate', NULL, NULL";
					break;
				case "WEEKLY":
					$detail = (isset($Sun)?"1":"0").(isset($Mon)?"1":"0").(isset($Tue)?"1":"0").(isset($Wed)?"1":"0");
					$detail .= (isset($Thu)?"1":"0").(isset($Fri)?"1":"0").(isset($Sat)?"1":"0");
					$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatWeeklyRange', '$detail'";
					$frequency = $repeatWeeklyRange;
					break;
				case "MONTHLY":
					if ($monthlyRepeatBy == "dayOfMonth")
						$detail = "day-".$datePieces[2];
					else {
						$eventDateWeekday = date("w", $eventTimeStamp);
						$nth = 0;
						for($i=1; $i<6; $i++) {
							$nthweekday = $iCal->NWeekdayOfMonth($i,$eventDateWeekday,$datePieces[1],$datePieces[0],"d");
							if ((int)$datePieces[2] == $nthweekday) {
								$nth = $i;
								break;
							}
						}
						$detail = "$nth-$eventDateWeekday";
					}
					$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatMonthlyRange', '$detail'";
					$frequency = $repeatMonthlyRange;
					break;
				case "YEARLY":
					$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatYearlyRange', NULL";
					$frequency = $repeatYearlyRange;
					break;
			}
			$sql = "INSERT INTO CALENDAR_EVENT_ENTRY_REPEAT ($fieldname) VALUES ($fieldvalue)";
			$result['update_event_repeat_info'] = $iCal->db_db_query($sql);
			$repeatID = $iCal->db_insert_id();
			
			#Get the uid of the event to be removed
			$sql = "select UID from CALENDAR_EVENT_ENTRY WHERE EventID = '$eventID'";
			$oldUID = $iCal->returnArray($sql);
			
			# Remove the existing single event
			$sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE EventID = '$eventID'";
			$iCal->db_db_query($sql);
			
			# Remove the existing single event personal Note
			$eclassConstraint =" AND (CalType <> 1 AND CalType <= 3 OR CalType is null) ";
			$sql = "DELETE FROM CALENDAR_EVENT_PERSONAL_NOTE WHERE EventID = '$eventID' "."$eclassConstraint";
			$iCal->db_db_query($sql);
						
			# Insert the recurrence event series
			$detailArray = array($repeatID, $repeatSelect, $repeatEndDate, $frequency, $detail, $UserID,
							$eventDateTime, $duration, $isImportant, $isAllDay, $access, $iCal->Get_Safe_Sql_Query($iCal->Get_Request($title)), 
							$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description)), $iCal->Get_Safe_Sql_Query($iCal->Get_Request($location)), $url, $calID);
			
			if (!isset($monthlyRepeatBy))
				$monthlyRepeatBy = "";
					
				
			# Main function for inserting recurrence events
			//$isIgnoreModifiedRecord = 1;		// newly added on 27 May 08 to control inserting a series of new record with ignoring the original modified record
			$result['insert_repeat_event_entry'] = $iCal->insertRepeatEventSeries($detailArray, $monthlyRepeatBy, 1, $oldUID[0]["UID"]);
			
			# insert current personal note into to new event series
			if(!in_array(false, $result)){
				$sql = "INSERT INTO CALENDAR_EVENT_PERSONAL_NOTE (EventID, UserID, PersonalNote, CalType, CreateDate) 
						SELECT EventID, '".$_SESSION['UserID']."', '".($iCal->Get_Request($PersonalNote))."', '$CalType', NOW()
						FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '".$repeatID."' ";
				$result['insert_repeat_event_personal_note'] = $iCal->db_db_query($sql);
			}
		}
		
		##### EBOOKING ##### (OTI)
		if($plugin['eBooking'])
		{
			if($hiddenBookingIDs != "")
			{
				if (isset($isRepeatEvent))
				{
					## Repeat Event
					$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID = '$eventID'";
					$arrOldBookingID = $iCal->returnVector($sql);
					$strOldBookingID = $arrOldBookingID[0];
					
					$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0, StartTime = '".$ebookingStartTime."', EndTime = '".$ebookingEndTime."' WHERE BookingID IN ($strOldBookingID)";
					$iCal->db_db_query($sql);
					
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID = '$strOldBookingID'";
					$iCal->db_db_query($sql);
					
//					$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID = $strOldBookingID";
//					$iCal->db_db_query($sql);
					
					//2012-1129-1604-13066
//					$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($strOldBookingID)";
//					$iCal->db_db_query($sql);
					
					$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = NULL WHERE BookingID = '$strOldBookingID'";
					$iCal->db_db_query($sql);
					
					
					$sql = "SELECT EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID = '$eventID'";
					$arrEventDate = $iCal->returnVector($sql);
					$event_date = substr($arrEventDate[0],0,10);
					$event_time = substr($arrEventDate[0],11,strlen($arrEventDate[0]));
					$sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD WHERE BookingID IN ($hiddenBookingIDs) AND Date = '$event_date' AND StartTime = '$event_time'";
					$arrNewBookingID = $iCal->returnVector($sql);
					
					include_once($PATH_WRT_ROOT."includes/libebooking.php");
					$lebooking = new libebooking();
					$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($hiddenBookingIDs) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
					$arrRoomID = $iCal->returnVector($sql);
					$roomId = $arrRoomID[0];
					
					$RoomNeedApproval[$hiddenBookingIDs] = $lebooking->Check_If_Booking_Need_Approval($_SESSION['UserID'], '', $roomId);
					$IsRoomAvailable = false;
					if ($RoomNeedApproval[$hiddenBookingIDs]) {
						$RoomBookingStatus = 0;
						
						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus' WHERE BookingID = '".$arrNewBookingID[0]."' ";
						$iCal->db_db_query($sql);
						
//						$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus WHERE BookingID = ".$arrNewBookingID[0];
//						$iCal->db_db_query($sql);
					} else {
						$RoomBookingStatus = 1;
						
						$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $ebookingEventDate, $ebookingStartTime, $ebookingEndTime);
						if ($IsRoomAvailable) {
							$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus', ProcessDate = NOW() WHERE BookingID = '".$arrNewBookingID[0]."'";
							$iCal->db_db_query($sql);
						}
						
//						$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus, ProcessDate = NOW() WHERE BookingID = ".$arrNewBookingID[0];
//						$iCal->db_db_query($sql);
					}
					
					$sql = "UPDATE INTRANET_EBOOKING_RECORD SET Remark = '".$eBookingRecordRemarksSql."' WHERE BookingID = '".$arrNewBookingID[0]."'";
					$iCal->db_db_query($sql);
					
					$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$eventID' WHERE BookingID = '".$arrNewBookingID[0]."'";
					$iCal->db_db_query($sql);
					
					include_once($PATH_WRT_ROOT."includes/libinventory.php");
					$linventory = new libinventory();
					$sql = "SELECT 
								CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
							FROM
								INVENTORY_LOCATION_BUILDING AS building 
								INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
								INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
								INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
							WHERE
								BookingID IN (".$arrNewBookingID[0].")";
					$arrLocation = $linventory->returnVector($sql);
					$location = $arrLocation[0];
					
					$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Location = '$location' WHERE EventID = '$eventID'";
					$iCal->db_db_query($sql);
					
					## Remove Temp Record
					$arrAssignedBookingID[] = $arrNewBookingID[0];
					$arrHiddenBookingIDs = explode(",",$hiddenBookingIDs);
					$arrNotAssignedBookingID = array_diff($arrHiddenBookingIDs, $arrAssignedBookingID);
					
//					if(sizeof($arrNotAssignedBookingID) > 0)
//					{
//						$strNotAssignedBookingID = implode(",",$arrNotAssignedBookingID);
//						$sql = "DELETE FROM INTRANET_EBOOKING_RECORD WHERE BookingID IN ($strNotAssignedBookingID)";
//						$iCal->db_db_query($sql);
//						
//						$sql = "DELETE FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($strNotAssignedBookingID)";
//						$iCal->db_db_query($sql);
//						
////						$sql = "DELETE FROM INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS WHERE BookingID IN ($strNotAssignedBookingID)";
////						$iCal->db_db_query($sql);
//						
//						$sql = "DELETE FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID IN ($strNotAssignedBookingID)";
//						$iCal->db_db_query($sql);
//					}
					
					if ($RoomNeedApproval[$hiddenBookingIDs] == false && $IsRoomAvailable)
					{
						$lebooking->Email_Booking_Result($arrNewBookingID[0]);
					}
				}
				else
				{
					## Single Event					
					$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID = '$eventID'";
					$arrOldBookingID = $iCal->returnVector($sql);
					$strOldBookingID = $arrOldBookingID[0];
					
					
					$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0, StartTime = '".$ebookingStartTime."', EndTime = '".$ebookingEndTime."', Remark = '".$eBookingRecordRemarksSql."' WHERE BookingID IN ($strOldBookingID)";
					$iCal->db_db_query($sql);
					
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID = '$strOldBookingID'";
					$iCal->db_db_query($sql);
					
//					$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID = $strOldBookingID";
//					$iCal->db_db_query($sql);
					
					//2012-1129-1604-13066
//					$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($strOldBookingID)";
//					$iCal->db_db_query($sql);
					
					$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = NULL WHERE BookingID = '$strOldBookingID'";
					$iCal->db_db_query($sql);
					
					include_once($PATH_WRT_ROOT."includes/libebooking.php");
					$lebooking = new libebooking();
					$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($hiddenBookingIDs) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
					$arrRoomID = $iCal->returnVector($sql);
					$roomId = $arrRoomID[0];
					
					$RoomNeedApproval[$hiddenBookingIDs] = $lebooking->Check_If_Booking_Need_Approval($_SESSION['UserID'], '', $roomId);
					$IsRoomAvailable = false;
					if ($RoomNeedApproval[$hiddenBookingIDs]) {
						$RoomBookingStatus = 0;
						
						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus' WHERE BookingID = '$hiddenBookingIDs'";
						$iCal->db_db_query($sql);
						
//						$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus WHERE BookingID = $hiddenBookingIDs";
//						$iCal->db_db_query($sql);
					} else {
						$RoomBookingStatus = 1;
						
						$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $ebookingEventDate, $ebookingStartTime, $ebookingEndTime);
						if ($IsRoomAvailable) {
							$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus', ProcessDate = NOW() WHERE BookingID = '$hiddenBookingIDs'";
							$iCal->db_db_query($sql);
						}
						
//						$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus, ProcessDate = NOW() WHERE BookingID = $hiddenBookingIDs";
//						$iCal->db_db_query($sql);
					}
					
					$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$eventID' WHERE BookingID = '$hiddenBookingIDs'";
					$iCal->db_db_query($sql);
					
					include_once($PATH_WRT_ROOT."includes/libinventory.php");
					$linventory = new libinventory();
					$sql = "SELECT 
								CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
							FROM
								INVENTORY_LOCATION_BUILDING AS building 
								INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
								INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
								INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
							WHERE
								BookingID IN ($hiddenBookingIDs)";
					$arrLocation = $linventory->returnVector($sql);
					$location = $arrLocation[0];
					
					$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Location = '$location' WHERE EventID = '$eventID'";
					$iCal->db_db_query($sql);
					
					if ($RoomNeedApproval[$hiddenBookingIDs] == false && $IsRoomAvailable)
					{
						$lebooking->Email_Booking_Result($$hiddenBookingIDs);
					}
				}
			}
		}
		
		## (CONT.)Save ALL FOLLOWING events ##
		## Show in school calendar (Group Event Only) ##
		if($ShowInSchoolCalendar)
		{
			$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\' ';
			$arrTargetGroupID = $db->returnVector($sql);
			$targetGroupID = $arrTargetGroupID[0];
			
			if (isset($isRepeatEvent)) // Original : Repeat event, but save only this event 
			{
				$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID = '$eventID'";
				$arrCalendarEvent = $iCal->returnArray($sql,2);
				
				if(sizeof($arrCalendarEvent)>0)
				{
					for($i=0; $i<sizeof($arrCalendarEvent); $i++)
					{
						list($calendar_event_id, $calendar_event_date) = $arrCalendarEvent[$i];
						
						//$title = trim(stripslashes(urldecode($title)));
						//$description = trim(stripslashes(urldecode($description)));
						
						$sql = "INSERT INTO 
										INTRANET_EVENT 
										(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy)
								VALUES
										('$calendar_event_date',2,'".$db->Get_Safe_Sql_Query($iCal->Get_Request($title))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($location))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($description))."',0,1,NOW(),'$UserID',NOW(),'$UserID')";
						$iCal->db_db_query($sql);
						
						$ParentSchoolCalEventID = $db->db_insert_id();
						$CurrentSchoolCalEventID = $db->db_insert_id();
									
						$value = "('$targetGroupID','$CurrentSchoolCalEventID',NOW(),NOW())";
						$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
						$db->db_db_query($sql);
						
						$arrSchoolCalendarInfo[$TargetEventDate][] = $CurrentSchoolCalEventID; 
					
						$sql = "UPDATE INTRANET_EVENT 
								SET 
									RelatedTo = '$ParentSchoolCalEventID',
									DateModified = NOW(),
									ModifyBy = '$UserID'
								WHERE 
									EventID = '$CurrentSchoolCalEventID'";
						$db->db_db_query($sql);
						
						$sql = "INSERT INTO CALENDAR_EVENT_SCHOOL_EVENT_RELATION (CalendarEventID, SchoolEventID) VALUES ('$calendar_event_id','$CurrentSchoolCalEventID')";
						$db->db_db_query($sql);
					}
				}
			}
			else
			{
				if($targetGroupID != "")
				{
					if($repeatID != "")		// Single to repeat
					{
						### SINGLE Event (Have repeat selection)
						$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID'";
						$arrCalendarEvent = $iCal->returnArray($sql);
					}	
					else					// Single to single
					{
						### SINGLE Event (No repeat selection)
						$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID = '$eventID'";
						$arrCalendarEvent = $iCal->returnArray($sql,2);
					}
							
					if(sizeof($arrCalendarEvent)>0)
					{
						for($i=0; $i<sizeof($arrCalendarEvent); $i++)
						{
							list($calendar_event_id, $calendar_event_date) = $arrCalendarEvent[$i];
							
							//$title = trim(stripslashes(urldecode($title)));
							//$description = trim(stripslashes(urldecode($description)));
							
							$sql = "INSERT INTO 
											INTRANET_EVENT 
											(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy)
									VALUES
											('$calendar_event_date',2,'".$db->Get_Safe_Sql_Query($iCal->Get_Request($title))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($location))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($description))."',0,1,NOW(),'$UserID',NOW(),'$UserID')";
							$iCal->db_db_query($sql);
							
							$ParentSchoolCalEventID = $db->db_insert_id();
							$CurrentSchoolCalEventID = $db->db_insert_id();
										
							$value = "('$targetGroupID','$CurrentSchoolCalEventID',NOW(),NOW())";
							$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
							$db->db_db_query($sql);
							
							$arrSchoolCalendarInfo[$TargetEventDate][] = $CurrentSchoolCalEventID; 
						
							$sql = "UPDATE INTRANET_EVENT 
									SET 
										RelatedTo = '$ParentSchoolCalEventID',
										DateModified = NOW(),
										ModifyBy = '$UserID'
									WHERE 
										EventID = '$CurrentSchoolCalEventID'";
							$db->db_db_query($sql);
							
							$sql = "INSERT INTO CALENDAR_EVENT_SCHOOL_EVENT_RELATION (CalendarEventID, SchoolEventID) VALUES ('$calendar_event_id','$CurrentSchoolCalEventID')";
							$db->db_db_query($sql);
						}
					}
				}
			}
		}
		else
		{
			//debug("Delete");
			if (isset($isRepeatEvent))
			{
				$iCalendarEventID = $eventID;
				$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
				$arr_SchoolCalEventID = $iCal->returnVector($sql);
				if(sizeof($arr_SchoolCalEventID) > 0)
				{
					$SchoolCalEventID = $arr_SchoolCalEventID[0];
					$sql = "DELETE FROM INTRANET_EVENT WHERE EventID = '".$SchoolCalEventID."'";
					$iCal->db_db_query($sql);
					
					$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID = '".$SchoolCalEventID."'";
					$iCal->db_db_query($sql);
					
					$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
					$iCal->db_db_query($sql);
				}	
			}
			else
			{
				$iCalendarEventID = $eventID;
				$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
				$arr_SchoolCalEventID = $iCal->returnVector($sql);
				if(sizeof($arr_SchoolCalEventID) > 0)
				{
					$SchoolCalEventID = $arr_SchoolCalEventID[0];
					$sql = "DELETE FROM INTRANET_EVENT WHERE EventID = '".$SchoolCalEventID."'";
					$iCal->db_db_query($sql);
					
					$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID = '".$SchoolCalEventID."'";
					$iCal->db_db_query($sql);
					
					$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
					$iCal->db_db_query($sql);
				}
			}
			
		}
		## update school calendar (calendar view)
		if($ShowInSchoolCalendar)
		{ 
			include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
			$lcycleperiods = new libcycleperiods();
			$lcycleperiods->generatePreview();
			$lcycleperiods->generateProduction();
		}
	}
	else 
	{
		$result['update_event_personal_note'] = $iCal->Insert_Personal_Note($eventID, $iCal->Get_Request($PersonalNote), $CalType, $otherSchoolCode);
	}
	
	#echo "About to insert reminders.....<br />";
	# Reminder
	if (isset($reminderID) || isset($reminderType)) 
	{
		# Normal event -> recurrence event
		if (isset($repeatSelect) && $userEventAccess!="R" && $repeatSelect != "NOT" && !isset($isRepeatEvent))
		{
			# Remove the old reminders
			$sql = "DELETE FROM CALENDAR_REMINDER WHERE EventID = '$eventID'";
			$iCal->db_db_query($sql);
			
			# Get all the events in the recurrence series
			$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
			if($CalType == 2 || $CalType == 3)//modified on 20 July 2009
			{
				$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore, CalType";
			}else
			{
				$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore";
			}
			$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ";
			$cnt = 0;
			for ($i=0; $i<sizeof($repeatSeries); $i++) {
				$repeatEventTimeStamp = $iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]);//modified on 17 July 2009
				for ($j=0; $j<sizeof($reminderType); $j++) {
					$reminderDate[$j] = $repeatEventTimeStamp - ((int)$reminderTime[$j])*60;//modified on 17 July 2009
					$reminderDate[$j] = date("Y-m-d H:i:s", $reminderDate[$j]);
					if($cnt == 0){
						$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$UserID."', '".$reminderType[$j]."', '".$reminderDate[$j]."', '".$reminderDate[$j]."')";
						$cnt++;
					} else {
						$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$UserID."', '".$reminderType[$j]."', '".$reminderDate[$j]."', '".$reminderDate[$j]."')";
					}
				}
				
				
				# reminder for newly add guests
				$curReminderDate = $repeatEventTimeStamp - 10*60;//modified on 17 July 2009
				$curReminderDate = date("Y-m-d H:i:s", $curReminderDate);
				
				if (isset($viewerList) && sizeof($viewerList) != 0) {
					for ($k=0; $k<sizeof($viewerList); $k++) {
						if($cnt == 0){
							$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$k]."', 'P', '$curReminderDate', 10)";
							$cnt++;
						} else {
							$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$k]."', 'P', '$curReminderDate', 10)";
						}
					}
				}
				
				if (isset($viewerClassList) && sizeof($viewerClassList) != 0) {
					for ($k=0; $k<sizeof($viewerClassList); $k++) {
						if($cnt == 0){
							$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerClassList[$k]."', 'P', '$curReminderDate', 10)";
							$cnt++;
						} else {
							$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerClassList[$k]."', 'P', '$curReminderDate', 10)";
						}
					}
				}
				
				if (isset($viewerCourseList) && sizeof($viewerCourseList) != 0) {
					for ($k=0; $k<sizeof($viewerCourseList); $k++) {
						if($cnt == 0){
							$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerCourseList[$k]."', 'P', '$curReminderDate', 10)";
							$cnt++;
						} else {
							$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerCourseList[$k]."', 'P', '$curReminderDate', 10)";
						}
					}
				}
				
				if (isset($viewerGroupList) && sizeof($viewerGroupList) != 0) {
					for ($k=0; $k<sizeof($viewerGroupList); $k++) {
						if($cnt == 0){
							$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerGroupList[$k]."', 'P', '$curReminderDate', 10)";
							$cnt++;
						} else {
							$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerGroupList[$k]."', 'P', '$curReminderDate', 10)";
						}
					}
				}
				
				# reminder for existing guests
				if (sizeof($existGuestList) != 0) {
					for ($l=0; $l<sizeof($existGuestList); $l++) {
						if (!in_array($existGuestList[$l], $removeGuestList) && $existGuestList[$l] != $UserID) {
							if($cnt == 0){
								$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$l]."', 'P', '$curReminderDate', 10)";
								$cnt++;
							} else {
								$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$l]."', 'P', '$curReminderDate', 10)";
							}
						}
					}
				}
			}
			$sql = rtrim($sql, ", ");
			$result['insert_event_reminder'] = $iCal->db_db_query($sql);	
		} 
		else 		# $reminderType > $reminderID means new reminder is added
		{
			if (sizeof($reminderType) >= sizeof($reminderID)) {
				for ($i=0; $i<sizeof($reminderType); $i++) {
						if(trim($eventTimeStamp) == "")
						{
							$EventDateResult=$iCal->returnArray("SELECT EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID='$eventID'");
							$reminderDate[$i] = strtotime($EventDateResult[0]['EventDate']) - ((int)$reminderTime[$i])*60;
							$reminderDate[$i] = date("Y-m-d H:i:s", $reminderDate[$i]);
						}else
						{
							$reminderDate[$i] = $eventTimeStamp - ((int)$reminderTime[$i])*60;//modified on 16 July 2009
							$reminderDate[$i] = date("Y-m-d H:i:s", $reminderDate[$i]);
						}
					if ($i < sizeof($reminderID)) {
						# Update existing reminder
						$sql  = "UPDATE CALENDAR_REMINDER SET ";
						$sql .= "ReminderType = '".$reminderType[$i]."', ";
						$sql .= "ReminderDate = '".$reminderDate[$i]."', ";
						$sql .= "LastSent = NULL, ";
						$sql .= "ReminderBefore = '".$reminderTime[$i]."' ";
						$sql .= "WHERE ReminderID = '".$reminderID[$i]."' ";
						$result['update_event_reminder_'.$i] = $iCal->db_db_query($sql);
					} else {
						# insert new reminder
						$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore";
						$fieldvalue = "'$eventID', '".$UserID."', '".$reminderType[$i]."', '".$reminderDate[$i]."', '".$reminderTime[$i]."' ";
						$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ($fieldvalue)";
						$result['update_event_reminder_'.$i] = $iCal->db_db_query($sql);
					}
				}
			} else {	# $reminderType < $reminderID means some reminder have to be updated and some have to be removed
				for ($i=0; $i<sizeof($reminderID); $i++) {
					if ($i < sizeof($reminderType)) {
						if(trim($eventTimeStamp) == "")
						{
							$EventDateResult=$iCal->returnArray("SELECT EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID='$eventID'");
							$reminderDate[$i] = strtotime($EventDateResult[0]['EventDate']) - ((int)$reminderTime[$i])*60;
							$reminderDate[$i] = date("Y-m-d H:i:s", $reminderDate[$i]);
						}else
						{
							$reminderDate[$i] = $eventTimeStamp - ((int)$reminderTime[$i])*60;//modified on 16 July 2009
							$reminderDate[$i] = date("Y-m-d H:i:s", $reminderDate[$i]);
						}
						$sql  = "UPDATE CALENDAR_REMINDER SET ";
						$sql .= "ReminderType = '".$reminderType[$i]."', ";
						$sql .= "ReminderDate = '".$reminderDate[$i]."', ";
						$sql .= "LastSent = NULL, ";
						$sql .= "ReminderBefore = '".$reminderTime[$i]."' ";
						$sql .= "WHERE ReminderID = '".$reminderID[$i]."' ";
						$result['update_event_reminder_'.$i] = $iCal->db_db_query($sql);
					} else {
						$result['delete_event_reminder_'.$i] = $sql = "DELETE FROM CALENDAR_REMINDER WHERE ReminderID = '".$reminderID[$i]."' ";
						$iCal->db_db_query($sql);
					}
				}
			}
		}
	}
	
	# Update reminders of other users
	$existOtherReminder = $iCal->returnReminderDetail($eventID, true);
	if ($userEventAccess!="R" && $repeatSelect != "NOT" && !isset($isRepeatEvent)) {
		
	} else {
		for($i=0; $i<sizeof($existOtherReminder); $i++) {
			if(trim($eventTimeStamp) == "")
			{
				$EventDateResult=$iCal->returnArray("SELECT EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID='$eventID'");
				$newReminderTs = strtotime($EventDateResult[0]['EventDate']) - $existOtherReminder[$i]["ReminderBefore"]*60;
				$newReminderDateTime = date("Y-m-d H:i:s", $newReminderTs);
			}else
			{
				$newReminderTs = $eventTimeStamp - $existOtherReminder[$i]["ReminderBefore"]*60;//modified on 16 July 2009
				$newReminderDateTime = date("Y-m-d H:i:s", $newReminderTs);
			}
			$sql  = "UPDATE CALENDAR_REMINDER SET ";
			$sql .= "LastSent = NULL, ";
			$sql .= "ReminderDate = '$newReminderDateTime' ";
			$sql .= "WHERE ReminderID = '".$existOtherReminder[$i]["ReminderID"]."' ";
			$result['update_event_guest_reminder_'.$i] = $iCal->db_db_query($sql);
		}
	}
	
	if ($userEventAccess=="A" || $userEventAccess=="W") {
		# Remove guests
		$temp = array();
		for ($i=0; $i<sizeof($removeGuestList); $i++) {
			if ($removeGuestList[$i] != -1)
				$temp[] = $removeGuestList[$i];
		}
		$removeGuestList = $temp;
		
		if (sizeof($removeGuestList)>0) {
			for ($i=0; $i<sizeof($removeGuestList); $i++) {
				$pieces = explode("-",$removeGuestList[$i]);
				$sql = "DELETE FROM CALENDAR_EVENT_USER WHERE EventID = '$eventID' AND UserID = '".$removeGuestList[$i]."' ";//." AND FromSchoolCode is null AND ToSchoolCode is null";
				$result['remove_event_guest_'.$i] = $iCal->db_db_query($sql);
				$sql = "DELETE FROM CALENDAR_REMINDER WHERE EventID = '$eventID' AND UserID = '".$removeGuestList[$i]."' ";
				$result['remove_event_guest_reminder_'.$i] = $iCal->db_db_query($sql);
				$eclassConstraint =" AND (CalType <> 1 AND CalType <= 3 OR CalType is null) ";
				$sql = "DELETE FROM CALENDAR_EVENT_PERSONAL_NOTE WHERE EventID = '$eventID' AND UserID = '".$removeGuestList[$i]."' ".$eclassConstraint;//." AND CalType <> 3 AND CalType <> 4";
				$result['remove_event_guest_personal_note_'.$i] = $iCal->db_db_query($sql);
			}
		}
	}
	
	# Normal event -> recurrence event
	if (isset($repeatSelect) && $userEventAccess!="R" && $repeatSelect != "NOT" && !isset($isRepeatEvent))
	{
		$existGuestList = $iCal->returnViewerID($eventID);
		for ($i=0; $i<sizeof($existGuestList); $i++) {
			if($existGuestList[$i]["UserID"] != $insertUserID){
					$viewerList[] = $existGuestList[$i]["UserID"];
			}
		}
		
		$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
		$fieldname1  = "EventID, UserID, Status, Access, InviteStatus";
		$sql1 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname1) VALUES ";
		$cnt1 = 0;
		for ($i=0; $i<sizeof($repeatSeries); $i++) {
			$allEventIdAry[] = $repeatSeries[$i]["EventID"];
			// insert the owner of the event with "A"(Full) access right
			if($cnt1 == 0){
				$sql1 .= " ('".$repeatSeries[$i]["EventID"]."', '".$insertUserID."', 'A', 'A', '$InviteStatus')";
				$cnt1++;
			} else {
				$sql1 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$insertUserID."', 'A', 'A', '$InviteStatus')";
			}
			
			if (isset($viewerList) && sizeof($viewerList) != 0) {
				$fieldname2  = "EventID, UserID, Status, Access, InviteStatus";
				$sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) VALUES ";
				$cnt2 = 0;
				for ($j=0; $j<sizeof($viewerList); $j++) {
					if($cnt2 == 0){
						$sql2 .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$_SESSION['UserID']?'A':'R')."', '$InviteStatus')";
						$cnt2++;
					} else {
						$sql2 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$_SESSION['UserID']?'A':'R')."', '$InviteStatus')";
					}
				}
				$result['insert_event_guest_'.$i] = $iCal->db_db_query($sql2);
			}
		}
//		$result['insert_event_owner'] = $iCal->db_db_query($sql1);
		
		# added on 4 June 08
		# remove existing personal note for thos guests
		$eclassConstraint =" AND (CalType <> 1 AND CalType <= 3 OR CalType is null) ";
		$sql = "DELETE FROM CALENDAR_EVENT_PERSONAL_NOTE WHERE EventID = '$eventID' AND UserID = '".$_SESSION['UserID']."' ".$eclassConstraint;//." And FromSchoolCode is null And CalType <> 3 And CalType <> 4";
		$iCal->db_db_query($sql);
		# Remove the existing guest in the old single event(normal event)
		$sql = "DELETE FROM CALENDAR_EVENT_USER WHERE EventID = '$eventID' ";//." AND FromSchoolCode is null";
		$iCal->db_db_query($sql);
		
		if($sys_custom['iCalendar']['EventTitleWithParticipantNames']){
			for($i=0;$i<count($allEventIdAry);$i++)
			{
				$title_extra_info = $iCal->getEventTitleExtraInfo($allEventIdAry[$i],1);
				$iCal->updateEventTitleWithExtraInfo($allEventIdAry[$i], $iCal->Get_Request($title), $title_extra_info);
			}
		}
	} 
	else 
	{
		$allEventIdAry[] = $eventID;
		if ($userEventAccess!="R") {
			# Add new guests
			if (isset($viewerList) && sizeof($viewerList) != 0) {
				$fieldname  = "EventID, UserID, Status, Access, InviteStatus";
				$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) VALUES ";
				$cnt = 0;
				for ($i=0; $i<sizeof($viewerList); $i++) {
					if($cnt == 0){
						$sql .= " ('".$eventID."', '".$viewerList[$i]."', '".($viewerList[$i]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($viewerList[$i]==$_SESSION['UserID']?'A':'R')."','".$InviteStatus."')";
						$cnt++;
					} else {
						$sql .= ", ('".$eventID."', '".$viewerList[$i]."', '".($viewerList[$i]==$_SESSION['UserID']?'A':$InviteEventUserStatus)."', '".($viewerList[$i]==$_SESSION['UserID']?'A':'R')."','".$InviteStatus."')";
					}
				}
				$result['insert_new_event_guest'] = $iCal->db_db_query($sql);
			}
		}
		
		# Update user own status
		$sql  = "UPDATE CALENDAR_EVENT_USER SET ";
		if($userEventAccess == 'A'){	// User with All Access right
			$sql .= "InviteStatus = '$InviteStatus' ";
			$sql .= ($EventType != "1") ? "" : ", Status = '$response' ";
		} else {						// User with only read right
			$sql .= "Status = '$response' ";	
		}
		$sql .= "WHERE EventID = '$eventID' AND UserID = '".$UserID."' ";//." And FromSchoolCode is null AND ToSchoolCode is null";
		$result['update_event_user_status'] = $iCal->db_db_query($sql);
	}
	
	if($sys_custom['iCalendar']['EventTitleWithParticipantNames']){
		$title_extra_info = $iCal->getEventTitleExtraInfo($eventID,1);
		$iCal->updateEventTitleWithExtraInfo($eventID, $iCal->Get_Request($title), $title_extra_info);
	}
}

if(!in_array(false, $result)){
	$Msg = $Lang['ReturnMsg']['EventUpdateSuccess'];
} else {
	$Msg = $Lang['ReturnMsg']['EventUpdateUnSuccess'];
}

if($plugin['eClassTeacherApp'] && $submitMode == 'submitAndSendPushMsg' && count($allEventIdAry)>0){
	$iCal->sendPushMessageNotifyParticipants($allEventIdAry[0]);
}

intranet_closedb();
// exit;

/*
if ($submitMode == "save")
	header("Location: index.php?time=".$_SESSION["iCalDisplayTime"]."&calViewPeriod=".$_SESSION["iCalDisplayPeriod"]);
else
	header("Location: new_event.php?msg=2&editEventID=event".$eventID);
*/

if ($submitMode == "saveNsend"){
	if($plugin['imail_gamma'])
		header("Location: ../imail_gamma/compose_email.php?eventID=".urlencode($eventID));
	else
		header("Location: ../imail/compose.php?eventID=".urlencode($eventID));	
}	
elseif((isset($_SESSION["iCalDisplayTime"]) && $_SESSION["iCalDisplayTime"] != "") && 
	(isset($_SESSION["iCalDisplayPeriod"]) && $_SESSION["iCalDisplayPeriod"] != "")) {
	header("Location: index.php?time=".$_SESSION["iCalDisplayTime"]."&msgType=event&calViewPeriod=".$_SESSION["iCalDisplayPeriod"]."&Msg=$Msg");
}else {
	header("Location: index.php?Msg=$Msg");
}

?>