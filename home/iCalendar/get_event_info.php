<?php
# Editing by 
/*
 * 2017-01-25 (Carlos): $sys_custom['iCalendar']['EventTitleWithParticipantNames'] display participant names.
 */
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../";
$ImagePathAbs = $PATH_WRT_ROOT."theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/icalendar_api.php");

// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");

if (empty($_SESSION['UserID'])) {
	//header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	echo 'SESSION_EXPIRED';
	exit;
}

intranet_opendb();


# Class Library
$iCal = new icalendar();
$iCal_api = new icalendar_api();
$lui = new ESF_ui();
// $iCal_api = new icalendar_api();
// $db = new database(false, false, true);
###############################################################################################################

# Get Post Data
$eventID	   = (isset($eventID) && $eventID != "") ? $eventID : "";
$calViewPeriod = (isset($calViewPeriod) && $calViewPeriod != "") ? $calViewPeriod : "monthly";
$isEventMore   = (isset($isEventMore) && $isEventMore != "") ? $isEventMore : false;
$calType   	   = (isset($calType) && $calType != "") ? $calType : '';

# Initialization
$output = '';
$heading = '';
$eventDate = '';
$repeatID = '';
$userAccess = '';

# Get Current School DB Name
$current_dbname = $iCal->db;

# Check whether this is Foundation Calendar with Non-ESF School Site & Get Centralized Site DB Name
// $isFoundationCalNonESF = ($calType == 2 && ($_SESSION['SchoolCode'] != 'ESFC' && $_SESSION['SchoolCode'] != 'DEV')) ? true : false;
// $isCPDCal = ($calType == 3)?true : false;
// if($isFoundationCalNonESF || $isCPDCal){
	// $central_dbname = $db->db;
// } else {
	// $central_dbname = $iCal->db;
// }

# Preparation
# Get Event Entry Details
// if ($calType == 4){
	// $pieces = split("-",$eventID);
	// $eventID = $pieces[0];
	// $otherSchoolCode = $pieces[1];
	// $result = $iCal_api->Get_Specific_Invitation_Event_Info($eventID, $otherSchoolCode);
// }
// else if (!$isCPDCal){
	if ($calType == 1 || $calType > 3){
	# external event
		 $result = $iCal_api->getExternalEventDetail($calType,$eventID);
	}else{
		$fieldname  = "a.EventID, a.UserID, a.CalID, a.RepeatID, date_format(a.EventDate, '%Y-%m-%d %H:%i:%s') AS EventDate, ";
		$fieldname .= " a.Title,a.Description,a.Duration, a.IsImportant, a.IsAllDay, a.Access, a.Location, a.Url, ";
		$fieldname .= " pn.PersonalNote, a.ModifiedDate, a.InputDate ";
		if($sys_custom['iCalendar']['EventTitleWithParticipantNames']){
			$fieldname .= ",a.TitleExtraInfo ";
		}
		$sql = "SELECT $fieldname
				FROM CALENDAR_EVENT_ENTRY AS a
				LEFT JOIN CALENDAR_EVENT_PERSONAL_NOTE AS pn ON
					a.EventID = pn.EventID and pn.UserID = ".$_SESSION['UserID']." ";

		$sql .= "WHERE a.EventID=$eventID";
		$result = $iCal->returnArray($sql);
	}


		// debug_r($result);
// }
// else{
	// $result = $db->$iCal_api->Get_Specific_CPD_Enrolment_Programme_Data($eventID);
// }



## Main
if(count($result) > 0){
	# Basic Info
	// debug_r($result);
	//$heading	   = trim(str_replace("'", "\'", $result[0]['Title']));			# for delete button use only
	$heading	   = trim(intranet_htmlspecialchars(($result[0]["Title"])));
	$heading	   = htmlspecialchars($heading,ENT_QUOTES);
	$title		   = trim(intranet_undo_htmlspecialchars(($result[0]["Title"])));
	$location	   = trim(intranet_undo_htmlspecialchars(($result[0]["Location"])));
	$eventDateTime = $result[0]["EventDate"];
	$isAllDay	   = $result[0]["IsAllDay"];
	$calID 		   = $result[0]["CalID"];
	$modifiedDate  = empty($result[0]["ModifiedDate"])?$result[0]["InputDate"]:$result[0]["ModifiedDate"];
	$createdBy  = $result[0]["UserID"];
	// echo $eventDateTime;
	# Date & Time
	$eventTs   = $iCal->sqlDatetimeToTimestamp($eventDateTime);
	$endTs	   = $eventTs + (($isAllDay == 0 || $result[0]["Duration"] == 0)?$result[0]["Duration"]*60:($result[0]["Duration"]-1440)*60);

	$eventDateTimePiece = explode(" ", $eventDateTime);
	$eventDate = $eventDateTimePiece[0];
	$eventEndDate = date("Y-m-d",$endTs);
	# Get Event Start Time & End Time
	if($isAllDay == 0){
		if ($iCal->systemSettings["TimeFormat"] == 12) {
			$eventTime = date("g:ia", $eventTs);
			$endTime = date("g:ia", $endTs);
		} else if ($iCal->systemSettings["TimeFormat"] == 24) {
			$eventTime = date("G:i", $eventTs);
			$endTime = date("G:i", $endTs);
		}
	}
// echo $eventTime." ".$endTime;
	# Repeat Info
	$repeatID  = $result[0]['RepeatID'];

	# Description
	$description = trim(intranet_undo_htmlspecialchars(nl2br($result[0]["Description"])));
	if($description != ""){
		//$description = (strlen($description) > 22) ? substr($description, 0, 22).' ...' : $description;
		//$limit = strlen($description)==mb_strlen($description,'utf8')?22:14;
		//$description = $iCal->formatEventTitle($description,false,false,$limit);
	} else {
		$description = "-";
	}

	# location
	$location = trim(intranet_undo_htmlspecialchars($result[0]["Location"]));
	if($location != ""){
		//$location = (strlen($location) > 22) ? substr($location, 0, 22).' ...' : $location;
		//$limit = strlen($location)==mb_strlen($location,'utf8')?22:14;
		//$location = $iCal->formatEventTitle($location,false,false,$limit);
	} else {
		$location = "-";
	}

	#Personal Note
	$PersonalNote = trim(intranet_undo_htmlspecialchars($result[0]["PersonalNote"]));
	if($PersonalNote != ""){
		//$PersonalNote = (strlen($PersonalNote) > 22) ? substr($PersonalNote, 0, 22).' ...' : $PersonalNote;
		//$limit = strlen($PersonalNote)==mb_strlen($PersonalNote,'utf8')?22:14;
		//$PersonalNote = nl2br($iCal->formatEventTitle($PersonalNote,false,false,$limit));
		$PersonalNote = nl2br($PersonalNote);
		$PersonalNote .= "<br><a class=\"sub_layer_link\" href=\"javascript:window.location='new_event.php?editEventID=event$eventID&calViewPeriod=$alViewPeriod&CalType=$calType'\">".$Lang['btn_edit']."</a>";

		;
	} else {
		$PersonalNote = "<a class=\"sub_layer_link\" href=\"javascript:window.location='new_event.php?editEventID=event$eventID&calViewPeriod=$calViewPeriod&CalType=$calType'\">$iCalendar_add</a>";
	}

	# Links
	$url  = trim($result[0]["Url"]);
	if($url != ""){
		$short_url= $url;
		//$short_url = (strlen($url) > 22) ? substr($url, 0, 22).' ...' : $url;
		$link = "<a href=\"".$url."\" target=\"_blank\" class=\"sub_layer_link\" style=\"font-size:12px\">".$short_url."</a>";
	} else {
		$link = "-";
	}

	# Get User Access Right - for delete button use only
	// if($isFoundationCalNonESF){
		// $userAccessArr = $iCal->getCalDetail($calID, true);
		// $userAccess = $userAccessArr[0]['Access'];
	// } else {
	if ($calType == 1){
		$userAccess = 'R';
	}
	else
		$userAccess = $iCal->returnUserEventAccess($UserID, $eventID);
	// }
}


# Main - ToolTips UI
if(count($result) > 0){
	$output .= "<table class='eventToolTipTable' border='0' cellpadding='2' cellspacing='0' width='100%'>";

	# Content
	$output .= "<tr>";
	$output .= "<td class=\"eventToolTipLabel\" valign=\"top\">$iCalendar_NewEvent_Title:</td>";
	$output .= "<td>$title</td>";
	$output .= "</tr>";
	
	if ($description!="" && $description!="-")
	{
		$output .= "<tr>";
		$output .= "<td class=\"eventToolTipLabel\" valign=\"top\">$iCalendar_NewCalendar_Description:</td>";
		$output .= "<td>$description</td>";
		$output .= "</tr>";
	}
	$output .= "<tr>";
	if ($calType == 4){
		$output .= "<td class=\"eventToolTipLabel\" valign=\"top\">$iCalendar_ExteranlEvent_StartDate:</td>";
	}
	else{
		$output .= "<td class=\"eventToolTipLabel\" valign=\"top\">$iCalendar_NewEvent_EventDate:</td>";
	}
	/*$output .= "<td>$eventDate</td>";
	$output .= "</tr>";
	if($isAllDay == 0){
		$output .= "<tr>";
		$output .= "<td class=\"eventToolTipLabel\" valign=\"top\">$iCalendar_NewEvent_EventTime:</td>";
		$output .= "<td>".$eventTime.($eventTime == $endTime ? "" : " - $endTime")."</td>";
		$output .= "</tr>";
	}
	$output .= "<tr>";*/
	if($isAllDay == 0){
		$output .= "<td rowspan='2'>";
		$output .= "<table width='100%' height='100%' style='overflow:visible'>";
		if ($eventDate == $eventEndDate){
			$output .= "<tr><td style='overflow:visible' colspan='3'>$eventDate</td></tr>";
		}
		else{
			$output .= "<tr><td style='overflow:visible'>$eventDate</td><td>-</td><td style='overflow:visible'>$eventEndDate</td></tr>";
		}

		$output .='<tr>';
		$output .= "<td style='overflow:visible'>".$eventTime."</td><td>-</td><td style='overflow:visible'>$endTime</td>";
		$output .='</tr>';
		$output .= "</table>";
		$output .= '</td>';
		$output .= "</tr>";
	}
	else{
		if ($eventDate == $eventEndDate){
			$output .= "<td style='overflow:visible'>$eventDate</td>";
			$output .= "</tr>";
		}
		else{
			$output .= "<td>";
			$output .= "<table width='100%' height='100%' style='overflow:visible'>";
			$output .= "<tr><td style='overflow:visible'>$eventDate</td><td>-</td><td style='overflow:visible'>$eventEndDate</td></tr>";
			$output .='</tr>';
			$output .= "</table>";
			$output .= '</td>';
			$output .= "</tr>";
		}
	}

	if($isAllDay == 0)
		$output .= "<tr><td class=\"eventToolTipLabel\" valign=\"top\">$iCalendar_NewEvent_EventTime:</td></tr>";


	if ($calType == 4){
		$output .= "<tr><td class=\"eventToolTipLabel\" valign=\"top\">$iCalendar_ExteranlEvent_DueDate:</td>";
		$output .= "<td>".$result[0]["DueDate"]."</td>";
		$output .= "</tr>";
	}
	elseif ($link!="" && $link!="-")
	{
		$output .= "<tr><td class=\"eventToolTipLabel\" valign=\"top\">$iCalendar_NewEvent_Link</td>";
		$output .= "<td>$link</td>";
		$output .= "</tr>";
	}

	// location
	$output .= "<tr>";
	$output .= "<td class=\"eventToolTipLabel\" valign=\"top\" width=\"100\">$iCalendar_NewEvent_Location:</td>";
	$output .= "<td>$location</td>";
	$output .= "</tr>";
	
	if($sys_custom['iCalendar']['EventTitleWithParticipantNames'] && $result[0]['TitleExtraInfo']!=''){
		$participant_names = $iCal->getEventTitleExtraInfo($result[0]['EventID'],1,1);
		$output .= "<tr>";
		$output .= "<td class=\"eventToolTipLabel\" valign=\"top\" width=\"100\">$iCalendar_NewEvent_Guests:</td>";
		$output .= "<td>".$participant_names."</td>";
		$output .= "</tr>";
	}
	
	// personal note
	$output .= "<tr>";
	$output .= "<td class=\"eventToolTipLabel\" valign=\"top\" width=\"100\">$iCalendar_NewEvent_Personal_Note:</td>";
	$output .= "<td>$PersonalNote</td>";
	$output .= "</tr>";

	if (!$iCal_api->isExternalEvent($calType)){
		$creator = $createdBy==0?$iCalendar_systemAdmin:$iCal->returnUserFullName($createdBy);
		$output .= "<tr>";
		$output .= "<td class=\"eventToolTipLabel\" valign=\"top\" width=\"100\">$iCalendar_EditEvent_CreatedBy:</td>";
		$output .= "<td>$creator</td>";
		$output .= "</tr>";

		$output .= "<tr>";
		$output .= "<td class=\"eventToolTipLabel\" valign=\"top\" width=\"100\">$i_Calendar_modifiedDate:</td>";
		$output .= "<td>$modifiedDate</td>";
		$output .= "</tr>";

	}

	# Button
	$output .= "<tr><td colspan='2' align='right'>";
	# Edit
	if(($userAccess == "A" || $userAccess == "W")&&(!$plugin["iCalendarEditByOwner"] || $plugin["iCalendarEditByOwner"] && $result[0]["UserID"]==$_SESSION['UserID'])&&(!($calType>3))){
	$output .= "<a class=\"sub_layer_link\" href=\"javascript:window.location='new_event.php?editEventID=event$eventID&calViewPeriod=$alViewPeriod&CalType=$calType'\">".$Lang['btn_edit']."</a>";
	$output .= "&nbsp;&nbsp;";
	# Delete
		# user with permission "(A): FULL" or "(W): Read & Edit" would allow to remove the event
		$output .= "<a id=\"deleteEvent\" name=\"deleteEvent\" class=\"sub_layer_link\" href=\"javascript:jDelete_Event($eventID, '$repeatID', '$eventDate', '$heading');\" >".$Lang['btn_delete']."</a>";
		$output .= "&nbsp;&nbsp;";
	}
	# Close
	$hide_fnt = ($isEventMore) ? "jHide_Event_More_Info()" : "jHide_Event_ToolTips()";
	$output .= "<a class=\"sub_layer_link\" href=\"javascript:$hide_fnt\">$iCalendar_close</a>";
	$output .= "</td>";
	$output .= "</tr>";
	$output .= "</table>";
} else {
	$output .= "No Event record is found.";
}

intranet_closedb();
###############################################################################################################
//echo iconv("BIG5", "UTF-8", $output);
echo $output;

?>