<?php
// page modifing by: 
$PATH_WRT_ROOT = "../../";
$ImagePathAbs = $PATH_WRT_ROOT."theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

########################################################################################################


if (!$_GET["file"]) {
	header ("Location: /");
	exit();
}

$filepath = $_GET["file"];
$filename = basename($_GET["file"]);

/*
$ref = @$HTTP_REFERER;
$url_format = parse_url($ref);
$path = $PathRelative.str_replace(basename($url_format["path"]), "", $url_format["path"]).$filename;

$parts = explode(".", $filename);
if (is_array($parts) && count($parts) > 1)
    $extension = strtoupper(end($parts));

# Only allow plain text files on the server to be read
$allow_ext = array("CSV", "TXT", "LOG"); 

$handle = @fopen($path,"r");

if ($handle && in_array($extension, $allow_ext)) {
	$data = fread($handle, filesize($path));
	
	header('Pragma: public');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

	header('Content-type: application/octet-stream');
	header('Content-Length: '.strlen($data));

	header('Content-Disposition: attachment; filename="'.$filename.'";');

	print $data;
} else {
	header ("Location: /");
	exit();
}


*/

# Library 
$libdb = new libdb();
$lfs = new libfilesystem();

$RealFilePath = $filepath;
$FileName    = $filename;
$FileContent = $lfs->file_read($RealFilePath); 
$FileType    = $lfs->getMIMEType($FileName);


## Output
if($FileType == "")
{
	$FileType = "application/octet-stream";
}
while (@ob_end_clean());
header("Pragma: public");
header("Expires: 0"); // set expiration time
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-type: $FileType");
header("Content-Length: ".strlen($FileContent));
header("Content-Disposition: attachment; filename=\"".$FileName."\"");
echo $FileContent;
// Output_To_Browser($FileContent, $FileName, $FileType);

intranet_closedb();
?>