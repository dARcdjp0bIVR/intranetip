<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$li = new libdb();

$sql  = "INSERT IGNORE INTO CALENDAR_CONFIG (Setting, Value) VALUES ";
$sql .= "('PreferredView', 'monthly'),";
$sql .= "('TimeFormat','12'),";
$sql .= "('WorkingHoursStart','6'),";
$sql .= "('WorkingHoursEnd','22'),";
$sql .= "('DisableRepeat','no'),";
$sql .= "('DisableGuest','no')";

$error = false;

$query = $li->db_db_query($sql);

$error = $error && $query;

if (!$error) {
	echo "Default configurations have been done.";
} else {
	echo "Database or SQL error.";
}

intranet_closedb();
?>