<?php
// using: 
/*
 * Modification Log:
 * 2016-04-25 Carlos 
 *  - cater eBooking MONTHLY repeat event by dayOfWeek to follow iCalendar rule. 
 *    Wrong way e.g. iCalendar treat last Friday of a month count from the end of month in reverse way, while eBooking count from the start of month. The 4th Friday of a month is not must the last Friday of a month, it could be the 5th Friday of a month. 
 * 2013-11-15 Ivan [2013-1024-1127-39156]
 * 	- added user pending record checking ************ if upload this file before ip.2.5.4.11.1.0, please upload libebooking.php also
 */
$PATH_WRT_ROOT = "../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");

intranet_opendb();

$linterface = new interface_html();
$lebooking_ui = new libebooking_ui();
$linventory = new libinventory();
$iCal = new icalendar();

if($AllDayEvent == 1) {
	$StartTime = date("H:i:s", mktime(0,0,0,0,0,0));
	$EndTime = date("H:i:s", mktime(23,55,0,0,0,0));
} else {
	$StartTime = date("H:i:s", mktime($StartHour,$StartMins,0,0,0,0));
	$EndTime = date("H:i:s", mktime($EndHour,$EndMins,0,0,0,0));
}

if($BookingStart != ""){
	$BookingStart = date("Y-m-d",strtotime($BookingStart));
}
if($BookingEnd != ""){
	$BookingEnd = date("Y-m-d",strtotime($BookingEnd));
}
if($RepeatStart != ""){
	$RepeatStart = date("Y-m-d",strtotime($RepeatStart));
}
if($RepeatEnd != ""){
	$RepeatEnd = date("Y-m-d",strtotime($RepeatEnd));
}


$arrNotAvailableDate = array();
$arrNotAvailableDateRemark = array();
$AvailableDateArr = array();
$arrDateAvailable = array();

if($RoomID != "")
{
	switch ($RepeatSelect){
		case "NOT":
			$targetDate = $BookingStart;
			
			if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
			{
				### Available
				
				# get user pending booking records
				$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $targetDate, $targetDate, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
				$numOfPeningRecord = count($curPendingRecordAry);
				$havePendingRecord = false;
				for ($i=0; $i<$numOfPeningRecord; $i++) {
					$_pendingRecordStartTime = $curPendingRecordAry[$i]['StartTime'];
					$_pendingRecordEndTime = $curPendingRecordAry[$i]['EndTime'];
					
					if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
						$havePendingRecord = true;
						break;
					}
				}
				
				if ($havePendingRecord) {
					$arrNotAvailableDate[] = $targetDate;
					$arrNotAvailableDateRemark[$targetDate] = $Lang['eBooking']['MakePendingBookingAlready']; 
				}
				else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
				{
					$tempArr[0]["StartTime"] = $StartTime;
					$tempArr[0]["EndTime"] = $EndTime;
					$arrDateAvailable[$targetDate][] = $tempArr[0];
					
					$AvailableDateArr[] = $targetDate;
				}else{
					$arrNotAvailableDate[] = $targetDate;
				}
			}
			else
			{
				### Not Available
				$arrNotAvailableDate[] = $targetDate;
			}
			break;
			
		case "DAILY":
			
			# get user pending booking records
			$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $RepeatStart, $RepeatEnd, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
			$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			$NumOfDay = round( abs(strtotime($RepeatStart)-strtotime($RepeatEnd)) / ($RepeatTimes*86400), 0 );
			for($i=0; $i<=$NumOfDay; $i++)
			{
				$DaysAfter = $i * $RepeatTimes;
				$ts_RepeatStartDate = strtotime($RepeatStart);
				$targetDate = $ts_RepeatStartDate + ($DaysAfter * 86400);		## 86400 is UNIX Timestamp for 1 day
				$targetDate = date("Y-m-d",$targetDate);
				
				if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
				{
					$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
					$_numOfPendingRecord = count((array)$_pendingRecordAry);
					$_havePendingRecord = false;
					for ($j=0; $j<$_numOfPendingRecord; $j++) {
						$_pendingRecordStartTime = $_pendingRecordAry[$j]['StartTime'];
						$_pendingRecordEndTime = $_pendingRecordAry[$j]['EndTime'];
						
						if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
							$_havePendingRecord = true;
							break;
						}
					}
					
					if ($_havePendingRecord) {
						$arrNotAvailableDate[] = $targetDate;
						$arrNotAvailableDateRemark[$targetDate] = $Lang['eBooking']['MakePendingBookingAlready']; 
					}
					else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
					{
						$tempArr[0]["StartTime"] = $StartTime;
						$tempArr[0]["EndTime"] = $EndTime;
						$arrDateAvailable[$targetDate][] = $tempArr[0];
					
						$AvailableDateArr[] = $targetDate;
					}else{
						$arrNotAvailableDate[] = $targetDate;
					}
				}
				else
				{
					$arrNotAvailableDate[] = $targetDate;
				}
			}
			break;
			
		case "WEEKDAY":
			
			# get user pending booking records
			$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $RepeatStart, $RepeatEnd, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
			$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			$NumOfDay = round( abs(strtotime($RepeatStart)-strtotime($RepeatEnd)) / (1 * 86400), 0 );
			for($i=0; $i<=$NumOfDay; $i++)
			{
				$ts_RepeatStartDate = strtotime($RepeatStart);
				$targetDate = $ts_RepeatStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day

				if((date("w",$targetDate) != "0") && (date("w",$targetDate) != "6"))
				{
					$targetDate = date("Y-m-d",$targetDate);
					if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
					{
						$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
						$_numOfPendingRecord = count((array)$_pendingRecordAry);
						$_havePendingRecord = false;
						for ($j=0; $j<$_numOfPendingRecord; $j++) {
							$_pendingRecordStartTime = $_pendingRecordAry[$j]['StartTime'];
							$_pendingRecordEndTime = $_pendingRecordAry[$j]['EndTime'];
							
							if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
								$_havePendingRecord = true;
								break;
							}
						}
						
						if ($_havePendingRecord) {
							$arrNotAvailableDate[] = $targetDate;
							$arrNotAvailableDateRemark[$targetDate] = $Lang['eBooking']['MakePendingBookingAlready']; 
						}
						else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
						{
							$tempArr[0]["StartTime"] = $StartTime;
							$tempArr[0]["EndTime"] = $EndTime;
							$arrDateAvailable[$targetDate][] = $tempArr[0];
						
							$AvailableDateArr[] = $targetDate;
						}else{
							$arrNotAvailableDate[] = $targetDate;
						}
					}
					else
					{
						$arrNotAvailableDate[] = $targetDate;
					}
				}
			}
			break;
			
		case "MONWEDFRI":
			
			# get user pending booking records
			$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $RepeatStart, $RepeatEnd, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
			$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			$NumOfDay = round( abs(strtotime($RepeatStart)-strtotime($RepeatEnd)) / (1 * 86400), 0 );
			for($i=0; $i<=$NumOfDay; $i++)
			{
				$ts_RepeatStartDate = strtotime($RepeatStart);
				$targetDate = $ts_RepeatStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day
				if((date("w",$targetDate) == "1") || (date("w",$targetDate) == "3") || (date("w",$targetDate) == "5"))
				{
					$targetDate = date("Y-m-d",$targetDate);
					if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
					{
						$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
						$_numOfPendingRecord = count((array)$_pendingRecordAry);
						$_havePendingRecord = false;
						for ($j=0; $j<$_numOfPendingRecord; $j++) {
							$_pendingRecordStartTime = $_pendingRecordAry[$j]['StartTime'];
							$_pendingRecordEndTime = $_pendingRecordAry[$j]['EndTime'];
							
							if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
								$_havePendingRecord = true;
								break;
							}
						}
						
						if ($_havePendingRecord) {
							$arrNotAvailableDate[] = $targetDate;
							$arrNotAvailableDateRemark[$targetDate] = $Lang['eBooking']['MakePendingBookingAlready']; 
						}
						else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
						{
							$tempArr[0]["StartTime"] = $StartTime;
							$tempArr[0]["EndTime"] = $EndTime;
							$arrDateAvailable[$targetDate][] = $tempArr[0];
						
							$AvailableDateArr[] = $targetDate;
						}else{
							$arrNotAvailableDate[] = $targetDate;
						}
					}
					else
					{
						$arrNotAvailableDate[] = $targetDate;
					}
				}
			}
			break;
			
		case "TUESTHUR":
			# get user pending booking records
			$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $RepeatStart, $RepeatEnd, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
			$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			$NumOfDay = round( abs(strtotime($RepeatStart)-strtotime($RepeatEnd)) / (1 * 86400), 0 );
			for($i=0; $i<=$NumOfDay; $i++)
			{
				$ts_RepeatStartDate = strtotime($RepeatStart);
				$targetDate = $ts_RepeatStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day
				if((date("w",$targetDate) == "2") || (date("w",$targetDate) == "4"))
				{
					$targetDate = date("Y-m-d",$targetDate);
					
					if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
					{
						$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
						$_numOfPendingRecord = count((array)$_pendingRecordAry);
						$_havePendingRecord = false;
						for ($j=0; $j<$_numOfPendingRecord; $j++) {
							$_pendingRecordStartTime = $_pendingRecordAry[$j]['StartTime'];
							$_pendingRecordEndTime = $_pendingRecordAry[$j]['EndTime'];
							
							if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
								$_havePendingRecord = true;
								break;
							}
						}
						
						if ($_havePendingRecord) {
							$arrNotAvailableDate[] = $targetDate;
							$arrNotAvailableDateRemark[$targetDate] = $Lang['eBooking']['MakePendingBookingAlready']; 
						}
						else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
						{
							$tempArr[0]["StartTime"] = $StartTime;
							$tempArr[0]["EndTime"] = $EndTime;
							$arrDateAvailable[$targetDate][] = $tempArr[0];
						
							$AvailableDateArr[] = $targetDate;
						}else{
							$arrNotAvailableDate[] = $targetDate;
						}
					}
					else
					{
						$arrNotAvailableDate[] = $targetDate;
					}
				}
			}
			break;
			
		case "WEEKLY":
			# get user pending booking records
			$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $RepeatStart, $RepeatEnd, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
			$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			$arrRepeatValue = explode(",",$RepeatValue);
			$NumOfDay = round( abs(strtotime($RepeatStart)-strtotime($RepeatEnd)) / (1 * 86400), 0 );
			for($i=0; $i<=$NumOfDay; $i++)
			{
				if($i%($RepeatTimes*7) < 7)
				{
					$ts_RepeatStartDate = strtotime($RepeatStart);
					$targetDate = $ts_RepeatStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day
					$targetDate = date("Y-m-d",$targetDate);
					
					if( in_array(date("w",strtotime($targetDate)),$arrRepeatValue) )
					{
						if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
						{
							$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
							$_numOfPendingRecord = count((array)$_pendingRecordAry);
							$_havePendingRecord = false;
							for ($j=0; $j<$_numOfPendingRecord; $j++) {
								$_pendingRecordStartTime = $_pendingRecordAry[$j]['StartTime'];
								$_pendingRecordEndTime = $_pendingRecordAry[$j]['EndTime'];
								
								if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
									$_havePendingRecord = true;
									break;
								}
							}
							
							if ($_havePendingRecord) {
								$arrNotAvailableDate[] = $targetDate;
								$arrNotAvailableDateRemark[$targetDate] = $Lang['eBooking']['MakePendingBookingAlready']; 
							}
							else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
							{
								$tempArr[0]["StartTime"] = $StartTime;
								$tempArr[0]["EndTime"] = $EndTime;
								$arrDateAvailable[$targetDate][] = $tempArr[0];
							
								$AvailableDateArr[] = $targetDate;
							}else{
								$arrNotAvailableDate[] = $targetDate;
							}
						}
						else
						{
							$arrNotAvailableDate[] = $targetDate;
						}
					}
				}
			}
			break;
			
		case "MONTHLY":
			# get user pending booking records
			$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $RepeatStart, $RepeatEnd, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
			$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			if($RepeatValue == 'dayOfWeek') {
				
				$datePieces = explode("-",$RepeatStart);
				$eventDateWeekday = date("w", strtotime($RepeatStart));
				$nth = 0;
				for($i=1; $i<6; $i++) {
					$nthweekday = $iCal->NWeekdayOfMonth($i,$eventDateWeekday,$datePieces[1],$datePieces[0],"d");
					if ((int)$datePieces[2] == (int)$nthweekday) {
						$nth = $i;
						break;
					} 
				}
				
				if (date("m",strtotime("+7 day",strtotime($RepeatStart)))!=$datePieces[1])
					$nth = -1;
				
				$weekName = Array("SU","MO","TU","WE","TH","FR","SA");
				$weekday = date("w",strtotime($RepeatStart));
				$rule = array('interval'=>$RepeatTimes,'freq'=>"MONTHLY","byDay"=>''.$nth.$weekName[$weekday],'until'=>$RepeatEnd);
				$allDays = $iCal->get_all_repeated_day($iCal->toUTC($RepeatStart),$iCal->toUTC($RepeatEnd),$rule);
				
				/*
				$ts_start_time = strtotime($RepeatStart);
				$ts_end_time = strtotime($RepeatEnd);
				
				$RepeatValue = date("w",strtotime($RepeatStart)); // 0 Sunday - 6 Saturday
				$repeat_frequency = date("n",$ts_end_time - $ts_start_time);
				
				$week_num = ceil( date('j', $ts_start_time) / 7 );
				
				for($i=0; $i<$repeat_frequency; $i++)
				{
					$repeat_val = $i*$RepeatTimes;

					$total_num_of_month = date("t",mktime(0,0,0,date("m",$ts_start_time)+$repeat_val,0,date("Y",$ts_start_time)));
					
					for($j=1; $j<=$total_num_of_month; $j++)
					{
						$curr_date = date("Y-m-d",mktime(0,0,0,date("m",$ts_start_time)+$repeat_val,$j,date("Y",$ts_start_time)));
						$ts_curr_timestamp = strtotime($curr_date);
						
						if(ceil( date('j', $ts_curr_timestamp) / 7 ) == $week_num)
						{
							if((date("w",$ts_curr_timestamp) == $RepeatValue) && ($ts_curr_timestamp <= $ts_end_time))
							{
								$targetDate = date("Y-m-d",$ts_curr_timestamp);
								*/
							for($i=0;$i<count($allDays);$i++)
							{	
								$targetDate = $allDays[$i];
								if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
								{
									$__pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
									$__numOfPendingRecord = count((array)$__pendingRecordAry);
									$__havePendingRecord = false;
									for ($k=0; $k<$__numOfPendingRecord; $k++) {
										$___pendingRecordStartTime = $__pendingRecordAry[$k]['StartTime'];
										$___pendingRecordEndTime = $__pendingRecordAry[$k]['EndTime'];
										
										if ($___pendingRecordStartTime==$StartTime && $___pendingRecordEndTime==$EndTime) {
											$__havePendingRecord = true;
											break;
										}
									}
									
									if ($__havePendingRecord) {
										$arrNotAvailableDate[] = $targetDate;
										$arrNotAvailableDateRemark[$targetDate] = $Lang['eBooking']['MakePendingBookingAlready']; 
									}
									else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
									{
										$tempArr[0]["StartTime"] = $StartTime;
										$tempArr[0]["EndTime"] = $EndTime;
										$arrDateAvailable[$targetDate][] = $tempArr[0];
									
										$AvailableDateArr[] = $targetDate;
									}else{
										$arrNotAvailableDate[] = $targetDate;
									}
								}
								else
								{
									$arrNotAvailableDate[] = $targetDate;
								}
							}
				/*			
							}
						}
					}
				}
				*/
			} else {
				
				$ts_start_time = strtotime($RepeatStart);
				$ts_end_time = strtotime($RepeatEnd);
				
				$RepeatValue = date("d",$ts_start_time);
				$repeat_frequency = date("n",$ts_end_time - $ts_start_time);
				
				for($i=0; $i<$repeat_frequency; $i++)
				{
					$repeat_val = $i*$RepeatTimes;
					$ts_curr_timestamp = mktime(0,0,0,date("m",$ts_start_time)+$repeat_val,$RepeatValue,date("Y",$ts_start_time));
					
					if(($RepeatValue == date("d",$ts_curr_timestamp)) && ($ts_curr_timestamp <= $ts_end_time))
					{
						$targetDate = date("Y-m-d",$ts_curr_timestamp);
						if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
						{
							$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
							$_numOfPendingRecord = count((array)$_pendingRecordAry);
							$_havePendingRecord = false;
							for ($j=0; $j<$_numOfPendingRecord; $j++) {
								$_pendingRecordStartTime = $_pendingRecordAry[$j]['StartTime'];
								$_pendingRecordEndTime = $_pendingRecordAry[$j]['EndTime'];
								
								if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
									$_havePendingRecord = true;
									break;
								}
							}
							
							if ($_havePendingRecord) {
								$arrNotAvailableDate[] = $targetDate;
								$arrNotAvailableDateRemark[$targetDate] = $Lang['eBooking']['MakePendingBookingAlready']; 
							}
							else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
							{
								$tempArr[0]["StartTime"] = $StartTime;
								$tempArr[0]["EndTime"] = $EndTime;
								$arrDateAvailable[$targetDate][] = $tempArr[0];
							
								$AvailableDateArr[] = $targetDate;
							}else{
								$arrNotAvailableDate[] = $targetDate;
							}
						}
						else
						{
							$arrNotAvailableDate[] = $targetDate;
						}
					}
				}
			}
			break;
			
		case "YEARLY":
			
			# get user pending booking records
			$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $RepeatStart, $RepeatEnd, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $RoomID);
			$curPendingRecordAssoAry = BuildMultiKeyAssoc($curPendingRecordAry, 'DateString', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			$ts_start_time = strtotime($RepeatStart);
			$ts_end_time = strtotime($RepeatEnd);
			$repeat_frequency = date("n",$ts_end_time - $ts_start_time);
			
			for($i=0; $i<$repeat_frequency; $i++)
			{
				$repeat_val = $i*$RepeatValue;
				$ts_curr_timestamp = mktime(0,0,0,date("m",$ts_start_time),date("d",$ts_start_time),date("Y",$ts_start_time)+$repeat_val);
				if($ts_curr_timestamp <= $ts_end_time)
				{
					$targetDate = date("Y-m-d",$ts_curr_timestamp);
					if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $_SESSION['UserID'], $targetDate))
					{
						$_pendingRecordAry = $curPendingRecordAssoAry[$targetDate];
						$_numOfPendingRecord = count((array)$_pendingRecordAry);
						$_havePendingRecord = false;
						for ($j=0; $j<$_numOfPendingRecord; $j++) {
							$_pendingRecordStartTime = $_pendingRecordAry[$j]['StartTime'];
							$_pendingRecordEndTime = $_pendingRecordAry[$j]['EndTime'];
							
							if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
								$_havePendingRecord = true;
								break;
							}
						}
						
						if ($_havePendingRecord) {
							$arrNotAvailableDate[] = $targetDate;
							$arrNotAvailableDateRemark[$targetDate] = $Lang['eBooking']['MakePendingBookingAlready']; 
						}
						else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$RoomID,$targetDate,$StartTime,$EndTime))
						{
							$tempArr[0]["StartTime"] = $StartTime;
							$tempArr[0]["EndTime"] = $EndTime;
							$arrDateAvailable[$targetDate][] = $tempArr[0];
						
							$AvailableDateArr[] = $targetDate;
						}else{
							$arrNotAvailableDate[] = $targetDate;
						}
					}
					else
					{
						$arrNotAvailableDate[] = $targetDate;
					}
				}
			}
			break;
	}
}
else
{
	$layer_content = "";
}

if(sizeof($arrNotAvailableDate)>0)
{
	$layer_content .= "<table border='0' width='100%'>";
	$layer_content .= "<tr><td>";
	$layer_content .= $Lang['iCalendar']['TheRoomIsNotAvailableOnTheFollowingDates'];
	$layer_content .= "</td></tr>";
	foreach($arrNotAvailableDate as $key=>$not_available_date)
	{
		$layer_content .= "<tr><td>";
		$layer_content .= $not_available_date;
		
		if (isset($arrNotAvailableDateRemark[$not_available_date])) {
			$layer_content .= ' ('.$arrNotAvailableDateRemark[$not_available_date].')';
		}
		
		$layer_content .= "</td></tr>";
	}
	$layer_content .= "</table>";
}
if(sizeof($AvailableDateArr) > 0)
{
	$layer_content .= "<table border='0' width='100%'>";
	$layer_content .= "<tr><td>";
	$layer_content .= $Lang['iCalendar']['TheRoomIsAvailableOnTheFollowingDates'];
	$layer_content .= "</td></tr>";
	foreach($AvailableDateArr as $key=>$available_date)
	{
		$layer_content .= "<tr><td>";
		$layer_content .= $available_date;
		$layer_content .= "</td></tr>";
	}
	$layer_content .= "</table>";
}
if(sizeof($arrDateAvailable) > 0)
{
	$layer_content .= "<table border='0' width='100%'>";
	$layer_content .= "<tr><td>";
	$layer_content .= $lebooking_ui->Get_New_Booking_Select_LocationItem_Table($RoomID,$arrDateAvailable,'' ,'' ,'', 1);
	$layer_content .= "</td></tr>";
	$layer_content .= "</table>";
	
	$layer_content .= "<table border='0' width='100%'>";
	$layer_content .= "<tr><td align='center'>";
	$layer_content .= $linterface->GET_SMALL_BTN($Lang['Btn']['Submit'], "button", "javascript:js_Submit_Booking();", "btnSubmit")."&nbsp;";
	$layer_content .= $linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "window.top.tb_remove();", "btnCancel");
	$layer_content .= "</td></tr>";
	$layer_content .= "</table>";
	
	### Layer to display Item Booking Details
	$layer_content .= '<div id="ItemBookingDetailsLayer" class="selectbox_layer" style="width:500px; height:200px; overflow:auto; display:none;">'."\n";
	$layer_content .= '<table cellspacing="0" cellpadding="3" border="0" width="95%">'."\n";
	$layer_content .= '<tbody>'."\n";
	### Hide Layer Button
	$layer_content .= '<tr>'."\n";
	$layer_content .= '<td align="right" style="border-bottom: medium none;">'."\n";
	$layer_content .= '<a href="javascript:js_Hide_Detail_Layer();"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\n";
	$layer_content .= '</td>'."\n";
	$layer_content .= '</tr>'."\n";
	### Layer Content
	$layer_content .= '<tr>'."\n";
	$layer_content .= '<td align="left" style="border-bottom: medium none;">'."\n";
	$layer_content .= '<div id="ItemBookingDetailsLayerContentDiv"></div>'."\n";
	$layer_content .= '</td>'."\n";
	$layer_content .= '</tr>'."\n";
	$layer_content .= '</tbody>'."\n";
	$layer_content .= '</table>'."\n";
	$layer_content .= '</div>'."\n";
}

echo $layer_content;

intranet_closedb();
?>