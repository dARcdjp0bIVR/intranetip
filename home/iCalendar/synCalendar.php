<?php
// page modifing by: 
$PATH_WRT_ROOT = "../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$iCal = new icalendar();
$stmt = "";

$sql = "select * from CALENDAR_CALENDAR where CalID = $calID";
$resultSet = $iCal->returnArray($sql);
// echo $resultSet[0]["SyncURL"];exit; 
$flink = fopen(trim($resultSet[0]["SyncURL"]), "r");
$stmt = "";

if ($flink ){
	// stream_set_timeout($flink ,5);
	while (!feof($flink)) {
		$stmt .= fgets($flink);
	}
	fclose($flink);
	// echo $stmt;
	// $info = stream_get_meta_data($flink );
	// if ($info['timed_out']){
		// header ("Location: manage_calendar.php?Msg=0|=|{$iCalendar_Import_Error1}");
		// exit;
	// }
}
else{
	header ("Location: manage_calendar.php?Msg=0|=|{$iCalendar_Import_Error1}");
	exit;
} 

//echo $stmt;
//exit;

if (empty($stmt)){
	header ("Location: manage_calendar.php?Msg=0|=|{$iCalendar_Import_Error2}");
	exit;
}


$events = $iCal->extract_icalEvent($stmt);
// debug_r($events);exit;
if ($events === false){ 
	header ("Location: manage_calendar.php?Msg=0|=|{$iCalendar_Import_Error2}");
	exit;
}
	 
$iCal->icalEvent_to_db($events,$calID);

header ("Location: manage_calendar.php?Msg=1|=|{$iCalendar_Import_Success}"); 
?>