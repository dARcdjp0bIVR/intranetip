<?php
// page modifing by: Jason
$PATH_WRT_ROOT = "../../";
$ImagePathAbs = $PATH_WRT_ROOT."theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once("choose/new/User_Group_Transformer.php");
intranet_auth();
intranet_opendb();

##################################################################################################################
// debug_r($_REQUEST);exit;

if (!isset($calID) || !is_numeric($calID)) {
	//header("Location: index.php?calViewPeriod=".$_SESSION["iCalDisplayPeriod"]."&time=".$_SESSION["iCalDisplayTime"]);
	header("Location: manage_calendar.php");
}

## Use Library
$li = new libdb();
$iCal = new icalendar();
$transformer = new User_Group_Transformer();
//$UserID = $_SESSION["UserID"];
## Get Data
$userID = $UserID;

$userCalAccess = $iCal->returnUserCalendarAccess($userID, $calID);


## Handle form parameters
$calName = trim($calName);
$calDescription = trim($calDescription);
$calColor = ltrim(trim($calColor), "#");
if ($calColor == "")
	$calColor = "000000";

# Calendar Type
# 1 - School Calendar   , 0 - General Calendar
$calType = (isset($CalType) && $CalType != "") ? $CalType : 0;

# Share to All
$isShareToAll = (isset($isShareToAll) && $isShareToAll != "") ? $isShareToAll : "";
$SchoolSharedType 	  = (isset($SchoolSharedType) && count($SchoolSharedType) > 0) ? $SchoolSharedType : array();
# Everyone can Subscribe status
$shareToAll = ($calType == 1) ? 0 : $shareToAll;
$synURL			  	  = (isset($synURL) && $synURL != "") ? trim($synURL) : "";

## Preparation
$userID 	   = $UserID;
$userCalAccess = $iCal->returnUserCalendarAccess($userID, $calID);

$calColor      = ($calColor == "") ? 'FFFFFF' : $calColor;

switch($CalType){
	case 1: 
		$CalSharedType = (count($SchoolSharedType) > 1) ? 'A' : $SchoolSharedType[0]; 
		break;
	// case 2: 
		// $CalSharedType = (count($FoundationSharedType) > 1) ? 'A' : $FoundationSharedType[0]; 
		// break;
	default: 
		$CalSharedType = ''; 
		break;
}

## Initialization
$result = array();
// $existClassViewer = array();
// $existGroupViewer = array();
// $existCourseViewer = array();
// $existViewer = array();
$group =Array();
$individual = Array();
$viewerID = empty($viewerID)?Array():array_unique($viewerID);

foreach($viewerID as $viewer){
	$prefix = substr($viewer,0,1);
	$suffix = substr($viewer,1);
	if ($prefix == 'U'){
		$individual[] = $suffix;
	}
	else{
		$group[] = $viewer;
	}
}

// $existTeachStaff = false;
// $existNonTeachStaff = false;
// $existSchoolBasedGroup = false;
// $existSchoolBasedGpViewer = array();
// $existSubjectViewer = array();
// $existSchoolYearViewer = array();

/*for($i=0; $i<sizeof($viewerID); $i++) {
	$viewerPrefix = substr($viewerID[$i], 0, 1);
	$viewSuffixID = substr($viewerID[$i], 1);
	
	if ($viewerPrefix == "G") {
		$existClassViewer[] = $viewSuffixID;
	} else if ($viewerPrefix == "C") {
		$existCourseViewer[] = $viewSuffixID;
	} else if ($viewerPrefix == "E") {
		$existGroupViewer[] = $viewSuffixID;
	} else if ($viewerPrefix == "P") {
		$parentList = $iCal->returnStudentParent($viewSuffixID);
		for($j=0; $j<sizeof($parentList); $j++) {
			$existViewer[] = $parentList[$j];
			#################
			$permissionVariable = "permission-".$viewerPrefix.$viewSuffixID;
			$calViewerAccess = $$permissionVariable;
			$parentAccess[$parentList[$j]] = $calViewerAccess;
			#################
		}
	} else if ($viewerPrefix == "U") {
		$existViewer[] = $viewSuffixID;
	}
}*/


/*$existViewer = empty($existViewer)?array():$existViewer;
	if ($CalType == 1){ #school calendar
		$sql = "Select UserID from INTRANET_USER where RecordType in ";
		switch ($CalSharedType){
			case 'A': $sql.="('1','2')";break;
			case 'T': $sql.="('1')";break;
			case 'S': $sql.="('2')";break;
		};
		$result = $iCal->returnVector($sql);
		$existViewer=array_merge($existViewer,$result);
		$existViewer = array_unique($existViewer);
		foreach ($result as $r){
			$permissionVariable = "permission-".$r;
			if (empty($$permissionVariable))
				$$permissionVariable = 'R';
		}
	}*/ 


/*
# Get New Data of Group Viewers
# Explanation of GroupPath
# Level 1 : T - Teaching Staff, N - Non-Teaching Staff, B - School-Base Group
# Level 2 : I - Individual, 	C - Subjects, 			Y - School Year,		S - Section, H - House
$num = 0;
if (sizeof($viewerPath) > 0) {
	for($i=0; $i<sizeof($viewerPath); $i++) {
		# Case for Adding Subject Group 
		# Convert subject name into subject code for new GroupPath
		$path = explode(":=:", $viewerPath[$i]);
		$pathPrefix = (count($path) > 1 && strlen($path[1]) > 1) ? substr($path[1], 0, 1) : "";
		if(strtoupper($path[0]) == "T" && strtoupper($pathPrefix) == "C"){
			$pathSuffix = substr($path[1], 1);
			if(strtoupper($pathPrefix) == "C"){
				$subjectName = $pathSuffix;
				# Get SubjectCode by Subject Name
				$Sql = "exec Get_SubjectCode_BySubjectName @SubjectName='".$subjectName."'";
				$return = $iCal->returnArray($Sql);
				
				$subjectPath = array();
				if(count($return) > 0){
					for($k=0 ; $k<count($return) ; $k++){
						$newPath = '';
						for($j=0 ; $j<count($path) ; $j++){
							$newPath .= ($j > 0) ? ":=:" : "";
							$newPath .= ($j == 1) ? $pathPrefix.$return[$k]['Subject_Code'] : $path[$j];
						}
						# Get Basic Data
						$calGrpViewerPath[$num] = $newPath;
						$permissionVariable = "permission-".$viewerPath[$i];
						$calGrpViewerAccess[$num] = $$permissionVariable;
						$calGrpDisplayColor[$num] = $calColor;
						$calGrpVisible[$num] = "1";
						
						$calGrpViewerID[$num] = $iCal->Get_Group_Viewer($newPath);
						$num++;
					}
				}
			}
		} else {
			# Get Basic Data
			$calGrpViewerPath[$num] = $viewerPath[$i];
			$permissionVariable = "permission-".$viewerPath[$i];
			$calGrpViewerAccess[$num] = $$permissionVariable;
			$calGrpDisplayColor[$num] = $calColor;
			$calGrpVisible[$num] = "1";
			
			$calGrpViewerID[$num] = $iCal->Get_Group_Viewer($viewerPath[$i]);
			$num++;
		}
	}
}
*/

function filter_Owner_array($var){
	return ($var!=$_SESSION['UserID']);
}

# Init
$temp  = array();
$temp2 = array();
$temp3 = array();
$temp4 = array();
$temp5 = array();
$temp6 = array();
// echo $userCalAccess; exit;
if (trim($userCalAccess) == "A") {
	if (isset($calName) &&  $calName != "") {
		$sql  = "UPDATE CALENDAR_CALENDAR SET ";
		$sql .= "Name = '".trim($calName)."', ";
		$sql .= "Description = '".trim($calDescription)."', ";
		$sql .= "ShareToAll = '$shareToAll', ";
		$sql .= "DefaultEventAccess = '$defaultEventAccess', ";
		$sql .= "CalType = '$calType', ";
		$sql .= "CalSharedType = '$CalSharedType', ";
		$sql .= "SyncURL = '$synURL' "; 
		$sql .= "WHERE CalID = $calID";
		$result['update_calendar'] = $iCal->db_db_query($sql);
		
		$sql  = "UPDATE CALENDAR_CALENDAR_VIEWER SET ";
		$sql .= "Color = '$calColor' ";
		$sql .= "WHERE CalID = $calID ";
		$sql .= ($calType == 1) ? "" : "AND UserID = $userID";
		$result['update_viewer_info'] = $iCal->db_db_query($sql);
		
		
		# delete the removed group;
		$sql = "delete from CALENDAR_CALENDAR_VIEWER 
				where CalID = '$calID' and GroupPath is not null ";
		if (count($group)>0)
			$sql .=	"and GroupPath not in ('".implode("','",$group)."')";
		$result['delete_removed_group'] = $iCal->db_db_query($sql);
		// echo $sql;
		// exit;
		# delete the removed user;
		$sql = "delete from CALENDAR_CALENDAR_VIEWER 
				where CalID = '$calID' and GroupPath is null and
				UserID <> $userID ";
		if (count($individual)>0)
			$sql .=	"and UserID not in ('".implode("','",$individual)."')"; 
		$result['delete_removed_user'] = $iCal->db_db_query($sql); 
		
		$individual = array_filter($individual,'filter_Owner_array');
		
		# check for school calendar
		if ($calType == 1){
			$subSql = "";
			switch($CalSharedType){
				case "A" :
					$subSql = "('T1','S1','-2')";
				break;
				case "T":
					$subSql = "('T1','-2')";
				break;
				case "S":
					$subSql = "('S1')";
				break;
			}
			$resultSet = Array();
			$sql = "select distinct GroupPath from CALENDAR_CALENDAR_VIEWER where 
					GroupPath in ($subSql) and CalID = '$calID'
				";
			$resultSet = $iCal->returnArray($sql);
			if ($CalSharedType=="A"&&count($resultSet)<3 || $CalSharedType=="S"&&count($resultSet)<1 || $CalSharedType=="T"&&count($resultSet)<2){
			$fieldName = 'CalID, UserID, Access, Color, Visible, GroupPath';
				if (($CalSharedType=="A" || $CalSharedType=="T")&& (!in_array("T1",$resultSet)||!in_array("-2",$resultSet))){
				# Teaching staff
					if (!in_array("T1",$resultSet)){
						$sql = "
							insert into CALENDAR_CALENDAR_VIEWER ($fieldName)
							Select '$calID', UserID, 'R', '$calColor', '1', 'T1' 
							from INTRANET_USER 
							where UserID <> '".$userID."' and 
							RecordStatus = '1' and RecordType = '1'
							and Teaching = '1'";
						$result['schoolCal_T'] = $iCal->db_db_query($sql);
					}
					if (!in_array("-2",$resultSet)){
					# Non-Teaching staff	
						$sql = "
							insert into CALENDAR_CALENDAR_VIEWER ($fieldName)
							Select '$calID', UserID, 'R', '$calColor', '1', '-2' 
							from INTRANET_USER 
							where UserID <> '".$userID."' and 
							RecordStatus = '1' and RecordType = '1'
							and (Teaching = '0' OR Teaching IS NULL)";
						$result['schoolCal_NT'] = $iCal->db_db_query($sql);
					}
					// echo $sql;
					// exit;
				}
				if (($CalSharedType=="A" || $CalSharedType=="S")&&!in_array("S1",$resultSet)){
					$sql = "
						insert into CALENDAR_CALENDAR_VIEWER ($fieldName)
						Select '$calID', UserID, 'R', '$calColor', '1', 'S1' from 
						INTRANET_USER 
						where UserID <> '".$userID."' and RecordStatus = '1' and RecordType = 2";
					$result['schoolCal_S'] = $iCal->db_db_query($sql);
				}
			}
		}
		
		#existing user
		$existingGroup = $iCal->returnCalViewerList($calID,false,true);
		$existingUser = $iCal->returnCalViewerList($calID);
		
		// echo "exUsr: ";
		// debug_r($existingUser);
		
		#formulate existing user array
		$egp = Array();
		foreach($existingGroup as $existing){
			$egp[] = $existing['GroupPath']; 
		}
		$eu = Array();
		foreach($existingUser as $existing){
			$eu[] = $existing['UserID'];
		}
		$updateGroup = array_intersect($egp,$group);
		$updateUser = array_intersect($eu,$individual);
		
		# update group user
		foreach($updateGroup as $ugp){
			$premission = "permission-".$ugp;
			$accessRight = $$premission;
			$sql = "
				update CALENDAR_CALENDAR_VIEWER set
				Access = '$accessRight'
				where GroupPath = '$ugp' and CalID = '$calID'	
			";
			$result['update_group_'.$ugp] = $iCal->db_db_query($sql);
		}
		
		# update Individual user
		foreach($updateUser as $uu){
			$premission = "permission-U".$uu;
			$accessRight = $$premission;
			$sql = "
				update CALENDAR_CALENDAR_VIEWER set
				Access = '$accessRight'
				where GroupPath is NULL and UserID = '$uu'
				and CalID = '$calID	'		
			";
			
			// echo $sql;
			// exit;
			$result['update_user_'.$uu] = $iCal->db_db_query($sql);
		}
		
		$newGroup = array_diff($group,$egp);
		$newUser = array_diff($individual,$eu);
		
		// echo "<br>newUser: ";
		
		
		#add new group user
		foreach ($newGroup as $gp){
			$prefix = substr($gp,0,1);
			$suffix = substr($gp,1);
			$groupUserList = $transformer->get_userID_from_group($prefix,$suffix);
			$groupUserList = array_filter($groupUserList,'filter_Owner_array');
			$permissionVariable = "permission-".$gp;
			$accessRight =  $$permissionVariable;
			if (count($groupUserList) > 0){		
				# insert group user
				if ($CalType == 1 && ($CalSharedType == "A"&&($group=="T1"||$group=="S1"||$group=="-2") || $CalSharedType == "T1" && ($group=="T1"||$group=="-2") || $CalSharedType == "S1" && $group=="S1")){
				
					$sql = "update CALENDAR_CALENDAR_VIEWER set 
							Access = $accessRight
							where CalID = '$calID' and GroupPath = '$gp'	
					";
					$result['UpdateViewer_Group_'.$gp] = $iCal->db_db_query($sql);
				}
				else{
					$fieldName = 'CalID, UserID, Access, Color, Visible, GroupPath';
					$sql = "insert into CALENDAR_CALENDAR_VIEWER ($fieldName) values ";
					$cnt = 0;
					foreach($groupUserList as $gpUser){
						if ($cnt ==0){
							$sql .= "('$calID','$gpUser','$accessRight', '$calColor', 1, '$gp')";
							$cnt++;
						}
						else{
							$sql .= ", ('$calID','$gpUser','$accessRight', '$calColor', 1, '$gp')";
						}			
					}
					$result['InsertViewer_Group_'.$gp] = $iCal->db_db_query($sql);
				}
			}
			
		}
		
		
		# Insert New Individual viewers
		if (count($newUser) > 0){		
			#insert individual user 
			$fieldName = 'CalID, UserID, Access, Color, Visible';
				$sql = "insert into CALENDAR_CALENDAR_VIEWER ($fieldName) values ";
			$cnt = 0;
			//debug_r($newUser);
			//for($i = 0; $i < count($newUser); $i++){
			foreach($newUser as $u){
				$permision = "permission-U".$u;
				$accessRight = $$permision;
				if ($cnt ==0){
					$sql .= "('$calID','".$u."','$accessRight', '$calColor', 1)";
					$cnt++;
				}
				else{
					$sql .= ", ('$calID','".$u."','$accessRight', '$calColor', 1)";
				}			
			}
			//echo $sql;
			$result['InsertViewer_Individual'] = $iCal->db_db_query($sql);
		}
		
		
	
		# Get Existing Individual User Data
	/*	$calViewerList = $iCal->returnCalViewerList($calID);
		for ($i=0; $i<sizeof($calViewerList); $i++) {
			$temp[$i] = $calViewerList[$i]["UserID"];
		}
		$calViewerList = $temp;
		
		# eClass
		$calClassViewerList = $iCal->returnCalViewerList($calID, false, true);
		for ($i=0; $i<sizeof($calClassViewerList); $i++) {
			if ($calClassViewerList[$i]["GroupType"] == "G")
				$temp2[$i] = $calClassViewerList[$i]["GroupID"];
			else if ($calClassViewerList[$i]["GroupType"] == "C")
				$temp3[$i] = $calClassViewerList[$i]["GroupID"];
			else if ($calClassViewerList[$i]["GroupType"] == "E")
				$temp4[$i] = $calClassViewerList[$i]["GroupID"];
		}
		$calClassViewerList = $temp2;
		$calCourseViewerList = $temp3;
		$calGroupViewerList = $temp4;
		
		if (empty($calClassViewerList))
			$calClassViewerList = array();
		if (empty($calCourseViewerList))
			$calCourseViewerList = array();
		if (empty($calGroupViewerList))
			$calGroupViewerList = array();
		
		if (sizeof($viewerID) > 0) {
			# Update existing viewers
			$stillExistViewerList = array_intersect($calViewerList, $existViewer);
			sort($stillExistViewerList);
			for($i=0; $i<sizeof($stillExistViewerList); $i++) {
				$permissionVariable = "permission-U".$stillExistViewerList[$i];
				$calViewerAccess = $$permissionVariable;
				#################
				if ($calViewerAccess=="" || empty($calViewerAccess))
					$calViewerAccess = $parentAccess[$stillExistViewerList[$i]];
				#################
				$sql = "UPDATE CALENDAR_CALENDAR_VIEWER SET ";
				$sql .= "Access = '$calViewerAccess' ";
				$sql .= "WHERE GroupID IS NULL AND CalID = $calID AND UserID = ".$stillExistViewerList[$i];
				$li->db_db_query($sql);
			}
			
			$stillExistClassViewerList = array_intersect($calClassViewerList, $existClassViewer);
			sort($stillExistClassViewerList);
			for($i=0; $i<sizeof($stillExistClassViewerList); $i++) {
				$permissionVariable = "permission-G".$stillExistClassViewerList[$i];
				$calViewerAccess = $$permissionVariable;
				$sql = "UPDATE CALENDAR_CALENDAR_VIEWER SET ";
				$sql .= "Access = '$calViewerAccess' ";
				$sql .= "WHERE CalID = $calID AND GroupType = 'G' AND GroupID = ".$stillExistClassViewerList[$i];
				$li->db_db_query($sql);
			}
			
			$stillExistCourseViewerList = array_intersect($calCourseViewerList, $existCourseViewer);
			sort($stillExistCourseViewerList);
			for($i=0; $i<sizeof($stillExistCourseViewerList); $i++) {
				$permissionVariable = "permission-C".$stillExistCourseViewerList[$i];
				$calViewerAccess = $$permissionVariable;
				$sql = "UPDATE CALENDAR_CALENDAR_VIEWER SET ";
				$sql .= "Access = '$calViewerAccess' ";
				$sql .= "WHERE CalID = $calID AND GroupType = 'C' AND GroupID = ".$stillExistCourseViewerList[$i];
				$li->db_db_query($sql);
			}
			
			$stillExistGroupViewerList = array_intersect($calGroupViewerList, $existGroupViewer);
			sort($stillExistGroupViewerList);
			for($i=0; $i<sizeof($stillExistGroupViewerList); $i++) {
				$permissionVariable = "permission-E".$stillExistGroupViewerList[$i];
				$calViewerAccess = $$permissionVariable;
				$sql = "UPDATE CALENDAR_CALENDAR_VIEWER SET ";
				$sql .= "Access = '$calViewerAccess' ";
				$sql .= "WHERE CalID = $calID AND GroupType = 'E' AND GroupID = ".$stillExistGroupViewerList[$i];
				$li->db_db_query($sql);
			}
			
			# Delete existing viewers
			$delExistViewerList = array_diff($calViewerList, $existViewer);
			sort($delExistViewerList);
			for($i=0; $i<sizeof($delExistViewerList); $i++) {
				if ($delExistViewerList[$i] != $userID) {
					$sql = "DELETE FROM CALENDAR_CALENDAR_VIEWER ";
					$sql .= "WHERE GroupID IS NULL AND CalID = $calID AND UserID = ".$delExistViewerList[$i];
					$li->db_db_query($sql);
				}
			}
			
			$delExistClassViewerList = array_diff($calClassViewerList, $existClassViewer);
			sort($delExistClassViewerList);
			for($i=0; $i<sizeof($delExistClassViewerList); $i++) {
				$sql = "DELETE FROM CALENDAR_CALENDAR_VIEWER ";
				$sql .= "WHERE CalID = $calID AND GroupType = 'G' AND GroupID = ".$delExistClassViewerList[$i];
				$li->db_db_query($sql);
			}
			
			$delExistCourseViewerList = array_diff($calCourseViewerList, $existCourseViewer);
			sort($delExistCourseViewerList);
			for($i=0; $i<sizeof($delExistCourseViewerList); $i++) {
				$sql = "DELETE FROM CALENDAR_CALENDAR_VIEWER ";
				$sql .= "WHERE CalID = $calID AND GroupType = 'C' AND GroupID = ".$delExistCourseViewerList[$i];
				$li->db_db_query($sql);
			}
			
			$delExistGroupViewerList = array_diff($calGroupViewerList, $existGroupViewer);
			sort($delExistGroupViewerList);
			
			for($i=0; $i<sizeof($delExistGroupViewerList); $i++) {
				$sql = "DELETE FROM CALENDAR_CALENDAR_VIEWER ";
				$sql .= "WHERE CalID = $calID AND GroupType = 'E' AND GroupID = ".$delExistGroupViewerList[$i];
				$li->db_db_query($sql);
			}
			
			# Add new viewers
			$newViewerList = array_diff($existViewer, $calViewerList);
			sort($newViewerList);
			$fieldname  = "CalID, UserID, Access, Color, Visible";
			$sql  = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) VALUES ";
			for($i=0; $i<sizeof($newViewerList); $i++) {
				if ($newViewerList[$i] != $userID) {
					$permissionVariable = "permission-U".$newViewerList[$i];
					$calViewerAccess = $$permissionVariable;
					#################
					if ($calViewerAccess=="" || empty($calViewerAccess))
						$calViewerAccess = $parentAccess[$newViewerList[$i]];
					#################
					$sql .= "('$calID', $newViewerList[$i], '$calViewerAccess', '$calColor', '1'), ";
				}
			}
			$sql = rtrim($sql, ", ");
			$li->db_db_query($sql);
			
			$newClassViewerList = array_diff($existClassViewer, $calClassViewerList);
			sort($newClassViewerList);
			$fieldname  = "CalID, UserID, GroupID, GroupType, Access, Color, Visible";
			$sql  = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) VALUES ";
			for($i=0; $i<sizeof($newClassViewerList); $i++) {
				$permissionVariable = "permission-G".$newClassViewerList[$i];
				$calViewerAccess = $$permissionVariable;
				$classStudents = $iCal->returnClassStudentList($newClassViewerList[$i]);
				for($k=0; $k<sizeof($classStudents); $k++) {
					$sql .= "('$calID', $classStudents[$k], $newClassViewerList[$i], 'G', '$calViewerAccess', '$calColor', '1'), ";
				}
			}
			$sql = rtrim($sql, ", ");
			$li->db_db_query($sql);
			
			$newCourseViewerList = array_diff($existCourseViewer, $calCourseViewerList);
			sort($newCourseViewerList);
			$fieldname  = "CalID, UserID, GroupID, GroupType, Access, Color, Visible";
			$sql  = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) VALUES ";
			for($i=0; $i<sizeof($newCourseViewerList); $i++) {
				$permissionVariable = "permission-C".$newCourseViewerList[$i];
				$calViewerAccess = $$permissionVariable;
				$courseStudents = $iCal->returnCourseStudentList($newCourseViewerList[$i]);
				for($k=0; $k<sizeof($courseStudents); $k++) {
					$sql .= "('$calID', $courseStudents[$k], $newCourseViewerList[$i], 'C', '$calViewerAccess', '$calColor', '1'), ";
				}
			}
			$sql = rtrim($sql, ", ");
			$li->db_db_query($sql);
			
			$newGroupMemberList = array_diff($existGroupViewer, $calGroupViewerList);
			sort($newGroupMemberList);
			$fieldname  = "CalID, UserID, GroupID, GroupType, Access, Color, Visible";
			$sql  = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) VALUES ";
			for($i=0; $i<sizeof($newGroupMemberList); $i++) {
				$permissionVariable = "permission-E".$newGroupMemberList[$i];
				$calViewerAccess = $$permissionVariable;
				$groupMembers = $iCal->returnGroupMemberList($newGroupMemberList[$i]);
				for($k=0; $k<sizeof($groupMembers); $k++) {
					$sql .= "('$calID', $groupMembers[$k], $newGroupMemberList[$i], 'E', '$calViewerAccess', '$calColor', '1'), ";
				}
			}
			$sql = rtrim($sql, ", ");
			$li->db_db_query($sql);
		} else {
			$sql  = "DELETE FROM CALENDAR_CALENDAR_VIEWER ";
			$sql .= "WHERE CalID = $calID AND Access <> 'A'";
			$li->db_db_query($sql);
			
			$sql  = "UPDATE CALENDAR_CALENDAR_VIEWER SET GroupID=NULL, GroupType=NULL ";
			$sql .= "WHERE CalID = $calID AND Access = 'A'";
			$li->db_db_query($sql);
		}*/
		
		/*
		# Get Existing User Group Data
		*/
		/*
		###############################################################################
		# Individual viewers
		if (sizeof($viewerID) > 0) {
			# Update existing viewers ( Individual user)
			$stillExistViewerList = array_intersect($calViewerList, $existViewer);
			sort($stillExistViewerList);
			for($i=0; $i<sizeof($stillExistViewerList); $i++) {
				$permissionVariable = "permission-U".$stillExistViewerList[$i];
				$calViewerAccess = $$permissionVariable;
				#################
				if ($calViewerAccess=="" || empty($calViewerAccess))
					$calViewerAccess = $parentAccess[$stillExistViewerList[$i]];
				#################
				$sql = "UPDATE CALENDAR_CALENDAR_VIEWER SET ";
				$sql .= "Access = '$calViewerAccess' ";
				$sql .= "WHERE GroupID IS NULL AND CalID = $calID AND UserID = ".$stillExistViewerList[$i];
				$result['update_individual_viewer_'.$i] = $iCal->db_db_query($sql);
			}
			
			# Update existing Group Viewers 
			//
			
			# Delete existing viewers
			$delExistViewerList = array_diff($calViewerList, $existViewer);
			sort($delExistViewerList);
			for($i=0; $i<sizeof($delExistViewerList); $i++) {
				if ($delExistViewerList[$i] != $userID) {
					$sql = "DELETE FROM CALENDAR_CALENDAR_VIEWER ";
					$sql .= "WHERE GroupID IS NULL AND CalID = $calID AND UserID = ".$delExistViewerList[$i];
					$result['delete_existing_viewer_'.$i] = $iCal->db_db_query($sql);
				}
			}
			
			# Delete existing Group
			//
			
			# Add new viewers
			$newViewerList = array_diff($existViewer, $calViewerList);
			sort($newViewerList);
			$fieldname  = "CalID, UserID, Access, Color, Visible";
			//$sql  = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) VALUES ";
			$sql  = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) ";
			$cnt = 0;
			if(count($newViewerList) > 0){
				for($i=0; $i<sizeof($newViewerList); $i++) {
					if ($newViewerList[$i] != $userID) {
						$permissionVariable = "permission-U".$newViewerList[$i];
						$calViewerAccess = $$permissionVariable;
						#################
						if ($calViewerAccess=="" || empty($calViewerAccess))
							$calViewerAccess = $parentAccess[$newViewerList[$i]];
						#################
						//$sql .= "('$calID', $newViewerList[$i], '$calViewerAccess', '$calColor', '1'), ";
						$sql .= ($cnt != 0) ? "UNION ALL " : "";
						$sql .= "SELECT '$calID', $newViewerList[$i], '$calViewerAccess', '$calColor', '1' ";
						$cnt++;
					}
				}
				$sql = rtrim($sql, ", ");
				$result['insert_new_viewer'] = $iCal->db_db_query($sql);
			}
			
			# Add New Teaching Staff Viewers
			//
			
		} else {
			# Delete Individual User
			$sql  = "DELETE FROM CALENDAR_CALENDAR_VIEWER ";
			$sql .= "WHERE CalID = $calID AND Access <> 'A' AND GroupPath =NULL";
			$result['delete_all_non_owner_individual_viewers'] = $iCal->db_db_query($sql);
			
			$sql  = "UPDATE CALENDAR_CALENDAR_VIEWER SET GroupID=NULL, GroupType=NULL ";
			$sql .= "WHERE CalID = $calID AND Access = 'A'";
			$result['update_owner_viewer_info'] = $iCal->db_db_query($sql);
		}
		*/
			
		/*
		###############################################################################
		# Group Viewers - New Approach
		if(sizeof($calGrpViewerPath) > 0){			
			# Case for UPDATE & INSERT the modified Group Viewer Data
			for($j=0; $j<sizeof($calGrpViewerPath); $j++) {
				$path = explode(":=:", $calGrpViewerPath[$j]);
				$pathPrefix = substr($path[0], 0, 1);
				$pathSuffix = substr($path[1], 0, 1);
				
				$calGrpType = "NULL";
				if(strtoupper($pathPrefix) == "B"){
					//$calGrpType = "'B'";
				}
				
				# Get All Group Viewer From Database for Each Group Path Comparison
				$calGroupViewerList = array();
				$calGroupViewerList = $iCal->returnCalViewerList($calID, false, true);
				
				if(count($calGroupViewerList) == 0){
					# Case for not having any existing Group User Data
					$fieldname = "CalID, UserID, GroupID, GroupType, Access, Color, Visible, GroupPath";
					for($k=0; $k<sizeof($calGrpViewerID[$j]); $k++) {
						# INSERT all New Group User Data
						$sql  = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) VALUES ";
						$sql .= "($calID, ".$calGrpViewerID[$j][$k].", NULL, $calGrpType, '".$calGrpViewerAccess[$j]."', '".$calGrpDisplayColor[$j]."', '".$calGrpVisible[$j]."', '".$calGrpViewerPath[$j]."')";
						$result['insert_group_viewer_'.$j.'_'.$k.'_'.$i] = $iCal->db_db_query($sql);
					}
				} else {
					# Case for having at least one Group User Data
					for($k=0; $k<sizeof($calGrpViewerID[$j]); $k++) {
						$isExist = 0;
						for ($i=0; $i<sizeof($calGroupViewerList); $i++) {
							# Identification of Existing Group User by matching with 2 values
							# UserID & GroupPath
							if( $calGrpViewerID[$j][$k] == $calGroupViewerList[$i]['UserID'] && 
								$calGrpViewerPath[$j] == $calGroupViewerList[$i]['GroupPath'] ){
								# UPDATE Group User Info
								$sql  = "UPDATE CALENDAR_CALENDAR_VIEWER 
										 SET 
										 	Access = '".$calGrpViewerAccess[$j]."', 
										 	Color  = '".$calGrpDisplayColor[$j]."' 
										 WHERE 
										 	CalID = $calID AND 
										 	UserID = ".$calGrpViewerID[$j][$k]." AND 
										 	GroupPath = '".$calGrpViewerPath[$j]."'";
								$result['update_group_viewer_'.$j.'_'.$k.'_'.$i] = $iCal->db_db_query($sql);
								$isExist = 1;
							} 
							if($isExist) continue;
						}
						
						# Case for Group User which does not exist in Current Group Viewer Data
						if($isExist == 0){
							# INSERT New Group Viewer Data
							$fieldname = "CalID, UserID, GroupID, GroupType, Access, Color, Visible, GroupPath";
							$sql  = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) VALUES ";
							$sql .= "($calID, ".$calGrpViewerID[$j][$k].", NULL, $calGrpType, '".$calGrpViewerAccess[$j]."', '".$calGrpDisplayColor[$j]."', '".$calGrpVisible[$j]."', '".$calGrpViewerPath[$j]."')";
							$result['insert_new_group_viewer_'.$j.'_'.$k] = $iCal->db_db_query($sql);
						}
					}
				}
			}
			
			$calGroupViewerList = array();
			$calGroupViewerList = $iCal->returnCalViewerList($calID, false, true);
			# Case for DELETE NON-Exist Group Viewer Data IN DB
			if(count($calGroupViewerList) > 0){
				for ($i=0; $i<sizeof($calGroupViewerList); $i++) {
					$isExist = 0;
					for($j=0; $j<sizeof($calGrpViewerPath); $j++) {
						for($k=0; $k<sizeof($calGrpViewerID[$j]); $k++) {
							if( $calGrpViewerID[$j][$k] == $calGroupViewerList[$i]['UserID'] && 
								$calGrpViewerPath[$j] == $calGroupViewerList[$i]['GroupPath'] ){
									$isExist = 1;
							}
							if($isExist) continue;
						}
						if($isExist) continue;
					}
					
					if($isExist == 0){
						# DELETE Non-Existing Group Viewer Data (No longer exist)
						$sql = "DELETE FROM 
									CALENDAR_CALENDAR_VIEWER 
								WHERE 
									CalID = ".$calID." AND 
									UserID = ".$calGroupViewerList[$i]['UserID']." AND 
									GroupPath = '".$calGroupViewerList[$i]['GroupPath']."'";
						$result['delete_exist_group_viewer_'.$i] = $iCal->db_db_query($sql);
					}
				}
			}
			
			# End of Operation of Group Viewers Data
		} else {
			# Delete All Group Viewers
			# Delete Individual User
			$sql  = "DELETE FROM CALENDAR_CALENDAR_VIEWER ";
			$sql .= "WHERE CalID = $calID AND GroupPath != NULL";
			$result['delete_all_group_viewer'] = $iCal->db_db_query($sql);
		}
		###############################################################################
		*/
		
		/*
		# function is hidden & is no longer use starting at 25 Aug
		# Add All User if CheckBox is checked
		if($isShareToAll == 1){
			$sql = "SELECT UserID, UserLogin FROM INTRANET_USER WHERE UserType = 'T' AND Status = 'A'";
			$teacherArr = $iCal->returnArray($sql, 2);

			if(count($teacherArr) > 0){
			    $AdminArr = array(
		                                "parkerr1", "woog1", "manay1",
                		                "chillingworthp", "leungwe1"
                                	     );

			    $field_name = "CalID, UserID, Access, Color, Visible";
			    $sql_1 = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($field_name) ";

			    $cnt_1 = 0;
			    for($k=0 ; $k<count($teacherArr) ; $k++){
				$teaID = $teacherArr[$k]['UserID'];
				$teaLogin = $teacherArr[$k]['UserLogin'];

				$sql_2 = "SELECT UserID FROM CALENDAR_CALENDAR_VIEWER WHERE UserID = $teaID AND CalID = '$calID'";
				$isExist = $iCal->returnArray($sql_2, 1);

				//if($teaLogin == "parkerr1" || $teaLogin == "woog1" || $teaLogin == "manay1"){
				if(in_array($teaLogin, $AdminArr)){
					$permit = 'W';
				} else {
					$permit = 'R';
				}
		
				if(count($isExist) == 0){
					$sql_1 .= ($cnt_1 > 0) ? "UNION ALL " : ""; 
					$sql_1 .= "SELECT '$calID', $teaID, '$permit', '$calColor', '1' ";
					$cnt_1++;
				}
			    }
			    if($cnt_1 > 0 && $sql_1 != "INSERT INTO CALENDAR_CALENDAR_VIEWER ($field_name) "){
			   	 	$result['insert_all_user'] = $iCal->db_db_query($sql_1);
		   	 	}
			}
		}
		*/ //exit;
	} else {
	// debug_r($result);
// exit;
		header("Location: new_calendar.php?calID=$calID&error=1");
	}
} else {
	if (trim($userCalAccess) == "W"){
		$sql = "Update CALENDAR_CALENDAR SET ";
		$sql .= "SyncURL = '$synURL' ";
		$sql .= "Where CalID = $calID";
		$result['update_Cal'] = $iCal->db_db_query($sql);
	}
	$sql  = "UPDATE CALENDAR_CALENDAR_VIEWER SET ";
	$sql .= "Color = '$calColor' ";
	$sql .= "WHERE CalID = $calID AND UserID = $userID";
	if (isset($groupID))
		$sql .= " AND GroupID = $groupID";
	$result['update_viewer'] = $iCal->db_db_query($sql);
	
}
// debug_r($result);
// exit;
if(!in_array(false, $result)){
	$Msg = $Lang['ReturnMsg']['CalendarUpdateSuccess'];
} else {
	$Msg = $Lang['ReturnMsg']['CalendarUpdateUnSuccess'];
}

##################################################################################################################
intranet_closedb();
header("Location: manage_calendar.php?Msg=".$Msg);

?>
