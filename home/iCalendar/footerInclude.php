
</td>
<td class="bg_shadow_right"> </td>
</tr>
</table>
<script type="text/javascript" src="js/general_script.js"></script>
<script type="text/javascript" language="javascript">
/*--------------------------- Event Reminder ---------------------------*/
var GlobalPathRelative;


EventReminder = (function(){
	var reminderID = new Array();
	var reminderTime;
	
	return {
		checking: function(){
			$.ajax({
				type: "POST",
				url: "/home/iCalendar/event_reminder.php",
				dataType: "xml",
				data: "type=check",
				success: function(xml){
					var futureRemind = $(xml).find('Future > ReminderID').each(function(){
						reminderID[reminderID.length] = $(this).text();
					});
					reminderTime = parseInt($(xml).find('Future > ReminderTime').text());
					reminderTime = reminderTime*1000;
					expireContent = $(xml).find("Expired > ReminderContent").text();
					//alert($(xml).find("Reminder").length);
					if (reminderID.length > 0){						
						setTimeout(function(){EventReminder.getEventInfo()}, reminderTime);
					}
					if (expireContent != ''){
						alert(expireContent);
					}
				}
			});
		},	
		getEventInfo: function(){
			params = {};
			params['type'] = 'show';
			params['reminderID[]'] = reminderID;
			reminderID = new Array();			
			$.ajax({
				type: "POST",
				url: "/home/iCalendar/event_reminder.php",
				data: params,
				success: function(content){	
					alert(content);
					EventReminder.checking();
				}
			});
		}
	}	
})();


function checkOption(obj){
        for(i=0; i<obj.length; i++){
                if(obj.options[i].value== ''){
                        obj.options[i] = null;
                }
        }
}

function checkOptionAll(obj){
        checkOption(obj);
        for(i=0; i<obj.length; i++){
                obj.options[i].selected = true;
        }
}



/* 
Play a wav file when an event is raised by the Reminder 
@param SoundObjId : Wav Sound Object defined in <Object> Tag

function PlaySound(SoundObjId)
{
	var soundObj=document.getElementById(SoundObjId);
	try
	{
  		soundObj.DoPlay();
	}catch(e)
	{
		try
		{
			soundObj.Play();
		}catch(e)
		{
			
		}
	}
}
*/
/*---------------------------End Event Reminder----------------------------*/

</script>


<script type="text/javascript" language="javascript" >GlobalPathRelative="";EventReminder.checking();</script>
<?php
//echo '<object id="AlertSound" type="audio/x-wav" data="/theme/sound/reminder.wav" width="0" height="0" style="visibility:hidden" >
//		<param name="src" value="/theme/sound/reminder.wav">
//		<param name="autostart" value="false">
//	  </object>';
if ($SYS_CONFIG['GoogleAnalyticsCode'])
{
?>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("<?=$SYS_CONFIG['GoogleAnalyticsCode']?>");
pageTracker._initData();
pageTracker._trackPageview();
<?
}
?>
</script>

</body>
</html>

