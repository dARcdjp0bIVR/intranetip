<?php
// page modifing by: 
/*
 * 2019-06-06 Carlos: Added CSRF token checking.
 */
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");

if(!verifyCsrfToken($csrf_token, $csrf_token_key,true)){
	echo '<script type="text/javascript" language="javascript">top.window.location.href="/";</script>';
	exit;
}

intranet_auth();
intranet_opendb();


$iCal = new icalendar();
$lui = new ESF_ui();

# Get Date
//$keyword = urldecode(addslashes(trim($keyword)));
//$keyword = iconv("UTF-8", "BIG5", $keyword);

$keyword = $iCal->Get_Safe_Sql_Like_Query( trim(urldecode(stripslashes($keyword))) );

# Check Data
if (empty($keyword)) {
	echo "<p class='tabletextremark'>$i_InventorySystem_Error</p>";
	exit;
}


# Search Data
$sql = "SELECT CalID FROM CALENDAR_CALENDAR_VIEWER WHERE UserID = '".$_SESSION['UserID']."'";
$result = $iCal->returnVector($sql);

if (count($result) > 1)
	$cond = " AND a.CalID NOT IN (".implode(",", $result).")";
else
	$cond = "";

$sql  = "SELECT 
			a.Name, a.Description, a.Owner, a.CalID
		 FROM 
		 	CALENDAR_CALENDAR AS a 
		 LEFT JOIN INTRANET_USER as b ON 
		 	a.Owner = b.UserID 
		
		 WHERE 
		 	(
		 	a.Name LIKE '%".$keyword."%' OR 
		 	a.Description LIKE '%".$keyword."%' OR 
		 	a.Owner LIKE '%".$keyword."%'
		 	) AND 
		 	a.Owner != '".$_SESSION['UserID']."' AND 
		 	a.ShareToAll = '1'".$cond;		 	
$result = $iCal->returnArray($sql);

//debug_r($sql);

#remove duplicate
$temp;
$i = 0;
foreach($result as $r){
	if ($i == 0){
		$temp[] = $r;
	}
	else{
		$isDuplicate = false;
		foreach($temp as $t){
			if ($temp["CalID"] == $r["CalID"]){
				$isDuplicate = true;
				break;
			}
		}
		if (!$isDuplicate)
			$temp[] = $r;
	}
}
$result = $temp;



$display  = '';

if(count($result) > 0){	
	$display .= '
	<p class="cssform">
	  <label for="Sender">'.$iCalendar_Calendar_CalendarFound.'</label>
	  <span id="imail_form_indside_choice" >	
		<table id="CalSearchResult" width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
		  <tr>
			<td width="250" class="imail_entry_top">'.$iCalendar_NewEvent_Calendar.'</td>
			<td class="imail_entry_top">'.$iCalendar_EditEvent_CreatedBy.'</td>
			<td class="imail_entry_top">&nbsp;</td>
		  </tr>';
		  
	for($i=0 ; $i<count($result) ; $i++){
		$calName = $result[$i]['Name'];
		$calID = $result[$i]['CalID'];
		$createdBy = $iCal->returnUserFullName($result[$i]['Owner']);
		
		$display .= '<tr id="search_'.$i.'">';
		$display .= '<td nowrap class="cal_agenda_content_entry">'.$calName.'</td>';
		$display .= '<td nowrap class="cal_agenda_content_entry">'.$createdBy.'</td>';
		$display .= '<td class="cal_agenda_content_entry cal_agenda_content_entry_button">';
		$display .= '<input type="button" name="Add" id="Add" value="'.$iCalendar_Calendar_Subscribe.'" class="button" onclick="jAdd_Public_Calendar(\''.$calID.'\', '.$i.')">';
		$display .= '</td>';
		$display .= '</tr>';
	}
		  
	$display .= '
		</table>
	  </span>
	</p>
	<br style="clear:both">';
} else {
	$display .= '
	<p class="cssform">
	  <label for="Sender" style="width:200px">'.$iCalendar_Calendar_NoCalendarFound.'</label>
	</p>
	<br style="clear:both">';
}

# prevent encoding problem
// echo iconv("BIG5", "UTF-8", $display);
echo $display;
intranet_closedb();
?>

