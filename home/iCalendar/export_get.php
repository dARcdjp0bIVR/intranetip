<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libicalendar.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$iCal = new libicalendar;

if (!isset($dateRange) || !isset($calID) || !is_numeric($calID)) {
	header ("Location: export.php");
	exit();
}

$sql  = "SELECT Title,EventDate,Duration,IsAllDay,Description,Location ";
$sql .= "FROM CALENDAR_EVENT_ENTRY WHERE CalID = $calID";

if ($dateRange == "today") {
	$startTs = strtotime(date("Y-m-d 00:00:00"));
	$sql .= " AND UNIX_TIMESTAMP(EventDate) >= $startTs";
} else if ($dateRange == "select") {
	$startDate = htmlspecialchars(trim($startDate));
	$endDate = htmlspecialchars(trim($endDate));
	$regex = "/^(19|20)([0-9]{2}-((0[13-9]|1[0-2])-(0[1-9]|[12][0-9]|30)|(0[13578]|1[02])-31|02-(0[1-9]|1[0-9]|2[0-8]))|([2468]0|[02468][48]|[13579][26])-02-29)$/";
	
	if (preg_match($regex, $startDate) && preg_match($regex, $endDate)) {
		$startTs = $iCal->sqlDatetimeToTimestamp($startDate." 00:00:00");
		$endTs = $iCal->sqlDatetimeToTimestamp($endDate." 23:59:59");
		$sql .= " AND UNIX_TIMESTAMP(EventDate) >= $startTs ";
		$sql .= " AND UNIX_TIMESTAMP(EventDate)+(Duration*60) <= $endTs";
	} else {
		header ("Location: export.php?error=true");
		exit();
	}
}

$result = $iCal->returnArray($sql,6);

// construct the array for exporting
for($i=0; $i<sizeof($result); $i++) {
	$EventTs = $iCal->sqlDatetimeToTimestamp($result[$i]["EventDate"]);
	$EndTs = $EventTs + $result[$i]["Duration"]*60;
	$startDate = date("d/m/Y", $EventTs);
	$startTime = date("H:i:s", $EventTs);
	$endDate = date("d/m/Y", $EndTs);
	$endTime = date("H:i:s", $EndTs);
	$result[$i]["IsAllDay"] == "1" ? $isAllDay = "true" : $isAllDay = "false";
	
	$exportArr[$i][0] = $result[$i]["Title"];
	$exportArr[$i][1] = $startDate;
	$exportArr[$i][2] = $startTime;
	$exportArr[$i][3] = $endDate;
	$exportArr[$i][4] = $endTime;
	$exportArr[$i][5] = $isAllDay;
	$exportArr[$i][6] = $result[$i]["Description"];
	$exportArr[$i][7] = $result[$i]["Location"];
}

$lexport = new libexporttext();

$exportColumn = array("Subject","Start Date","Start Time","End Date","End Time","All Day Event","Description","Location");
$exportContent = $lexport->GET_EXPORT_TXT($exportArr, $exportColumn, "", "\r\n", "", 0, "11");

intranet_closedb();

$filename = "calendar_events.csv";
$lexport->EXPORT_FILE($filename, $exportContent);

?>