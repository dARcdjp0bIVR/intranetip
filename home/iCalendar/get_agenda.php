<?php
// page modifing by: 
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../";
$ImagePathAbs = $PATH_WRT_ROOT."theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php"); 
include_once($PATH_WRT_ROOT."includes/icalendar.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");

if (empty($_SESSION['UserID'])) {
	//header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	echo 'SESSION_EXPIRED';
	exit;
}

intranet_opendb();
// include_once($PathRelative."src/include/class/icalendar.php");

$iCal = new icalendar();
$lui = new ESF_ui();


$syncCalId = trim($syncCalId);
if (!empty($syncCalId) && $syncCalId != "undefined"){
	$stmt = "";
	
	$sql = "select * from CALENDAR_CALENDAR where CalID = '$syncCalId'";
	$resultSet = $iCal->returnArray($sql);
	
	error_reporting(0);
	ob_start(); 
	$f = fopen($resultSet[0]["SyncURL"], "r");
	$stmt = "";
	$isFail = false;
	if ($f){
		stream_set_timeout($f,5);
		while (!feof($f)) {
			$stmt .= fgets($f);
		}
		fclose($f);
		$info = stream_get_meta_data($f);
		if ($info['timed_out']){
			echo "fail1";
			$isFail = true;
		}
	}
	else{
		echo "fail1";
		$isFail = true;
	} 


	if (!$isFail){
		if (empty($stmt)){
			echo "fail2";
			$isFail = true;
		}

		if (!$isFail){
			$events = $iCal->extract_icalEvent($stmt);

			if (!$events){ 
				echo "fail2";
				$isFail = true;
			}
		}
	}
	if (!$isFail) 
		$iCal->icalEvent_to_db($events,$syncCalId);
}



if (!isset($response))
	$response = "";
/* Michael Cheung (2009-10-28) Commented out to cater new logic
if (!isset($startDate) || $startDate == "") {
	$startDate = date("Y-m-d");
}

if (!isset($endDate) || $endDate == "") {
	if ($agendaPeriod == "1") {
		$endDate = date("Y-m-d", time(date("Y-m-d")." 00:00:00") + (7*24*60*60));
	} else if  ($agendaPeriod == "2") {
		$currentWeek = $iCal->get_week(date("Y"), date("n"), date("j"), "Y-m-d");
		$startDate = $currentWeek[0];
		$endDate = $currentWeek[6];
	} else {
		$endDate = date("Y-m-d", time(date("Y-m-d")." 00:00:00") + (24*60*60));
	}
}
*/

if (!isset($time) || trim($time) == "") {
	$startDate = date("Y-m-d");
	$endDate = date("Y-m-d", time($startDate." 00:00:00") + (6*24*60*60));
} else {
	$startDate = date("Y-m-d",$time);
	$endDate = date("Y-m-d", $time + (6*24*60*60) );	
}

// echo $startDate.':'.$endDate;

# Record the display view in session during each visit to this page
$_SESSION["iCalDisplayPeriod"] = $calViewPeriod;
$_SESSION["iCalDisplayTime"] = $time;

# Get the event array for quicker access
# TimeStamp of Upper Bound should be minus 1 to exclude the event which is hold on 00:00:00
$lower_bound = $iCal->sqlDatetimeToTimestamp($startDate." 00:00:00");
$upper_bound = $iCal->sqlDatetimeToTimestamp($endDate." 23:59:59");


# Differ from a Offset 1970101
$lower_bound += $iCal->offsetTimeStamp;
$upper_bound += $iCal->offsetTimeStamp;

# Old approach
//$events = $iCal->query_events($lower_bound, $upper_bound); 

# New approach

$events = $iCal->Get_All_Related_Event($lower_bound, $upper_bound, true);

# Get school event
#$schoolEvents = $iCal->returnSchoolEventRecord($lower_bound, $upper_bound);


//$x = $iCal->printAgenda($startDate, $endDate, false, $response, $agendaPeriod);
$x = $iCal->Print_Agenda($startDate, $endDate, false, $response, $agendaPeriod);

intranet_closedb();

//echo iconv("BIG5", "UTF-8", $x);
echo $x;
?>