<?php
// page modifing by: 
/*
 * 2019-06-06 Carlos: Added CSRF token checking.
 */
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");

if(!verifyCsrfToken($csrf_token, $csrf_token_key,true)){
	echo '<script type="text/javascript" language="javascript">top.window.location.href="/";</script>';
	exit;
}

intranet_auth();
intranet_opendb();
$iCal = new icalendar();
################################################################################################

if (empty($CalID)) {
	echo "<p class='tabletextremark'>$i_InventorySystem_Error</p>";
	exit;
}

$CalIDArr = array();
$CalIDArr[] = $CalID;

$cnt = 0;
$fieldname  = "CalID, UserID, Access, Color, Visible";
$sql1  = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) VALUES ";
// $sql1  = "INSERT INTO CALENDAR_CALENDAR_VIEWER ($fieldname) ";
for($i=0; $i<sizeof($CalIDArr); $i++) {
	# Get Selected Calendar Color
	$sql2  = "SELECT b.Color ";
	$sql2 .= "FROM CALENDAR_CALENDAR AS a, CALENDAR_CALENDAR_VIEWER AS b ";
	$sql2 .= "WHERE a.Owner=b.UserID AND a.CalID=b.CalID AND a.CalID=".$CalIDArr[$i];
	$result = $iCal->returnArray($sql2);
	
	$calColor = $result[0]["Color"];
	
	//$sql1 .= "(".$CalIDArr[$i].", ".$_SESSION['SSV_USERID'].", 'P', '$calColor', '1'), ";
	$sql1 .= ($cnt > 0) ? ", " : "";
	$sql1 .= "(".$CalIDArr[$i].", ".$_SESSION['UserID'].", 'P', '$calColor', '1')";
	$cnt++;
}
// echo $sql1;
// exit;
//$sql1 = rtrim($sql1, ", ");
$iCal->db_db_query($sql1);


// echo iconv("BIG5", "UTF-8", $iCal->generateViewCalendarControl());
echo $iCal->generateViewCalendarControl();
################################################################################################

intranet_closedb();
?>
