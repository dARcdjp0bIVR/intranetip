<?php

global $favicon_image;

?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/interface.js"></script>
<!--<script type="text/javascript" src="js/jquery.blockUI.js"></script>-->
<script type="text/javascript" src="js/dimensions.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<!--<script type="text/javascript" src="js/jquery.corner.js"></script>-->
<!-- Start jqModal include -->
<script type="text/javascript" src="/templates/script.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="js/general_script.js"></script>
<link rel="stylesheet" type="text/css" href="css/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/common.css" />
<link rel="stylesheet" type="text/css" href="css/main.css" />
<!--<link rel="stylesheet" type="text/css" href="/theme/css/common.css" />-->


<link href="/theme/css/common_cammy.css" rel="stylesheet" type="text/css">
<link href="/theme/css/layout_grey.css" rel="stylesheet" type="text/css">
<link href="/theme/css/font_size_normal.css" rel="stylesheet" type="text/css">
<link href="/theme/css/font_face_verdana.css" rel="stylesheet" type="text/css">
<link href="/theme/css/font_color_layout_grey.css" rel="stylesheet" type="text/css">
<link id="page_favicon" href="/images/<?=$favicon_image?>" rel="icon" type="image/x-icon" />
<title>eClass iCalendar</title>
<style>

.online_help { display:block; clear:right; float:right; margin-top:-2px;
float:right;width:50%;}
.online_help a.help_icon{ padding-left:13px; margin-bottom:0px; margin-top:0px; float:right;font-size:1em; color:#FFFFFF; text-align:center; line-height:28px; font-weight:normal; height:33px; <?=(strstr($_SERVER['HTTP_USER_AGENT'],'MSIE')?'width:55px;':'width:44px;')?> display:block; background:url(../../../images/2009a/online_help_bg.png) no-repeat 0 0}
.online_help a.help_icon:hover{color:#406c00; background-position : 0 -33px; line-height:30px;}
.online_help .help_board { display:none; float:right; width:120px;height:33px;  }
.online_help .help_board div{position:absolute; width:120px; display:block}
.online_help .help_board h1{background:url(../../../images/2009a/online_help_board_top.png); height:25px; margin:0px; padding:0px; line-height:25px; font-size:1em; font-weight:normal;  display:block; clear:both; padding-left:23px; text-align:left;}
.online_help .help_board ul { clear:both; list-style-type:none; background:url(../../../images/2009a/online_help_board.png) no-repeat left bottom;padding:0px; padding-bottom: 8px; margin:0px;}
.online_help .help_board ul li {display:block; padding-left:5px; padding-right:5px;}
.online_help .help_board ul li a{display:block; background:url(../../../images/2009a/online_help_icon.gif) no-repeat left top; padding-left:15px; line-height:18px; color:#62a21d; font-size:1em}
.online_help .help_board ul li a:hover{ color:#FF0000;}

</style>

<!-- ClueTip require files begin -->
<script type="text/javascript" src="cluetip/jquery.hoverIntent.js"></script>
<script type="text/javascript" src="cluetip/jquery.cluetip.js"></script>
<style type="text/css">@import url(cluetip/jquery.cluetip.css);</style>
<!-- Load blockUI with jQuery-1.3.2 -->
<script type="text/javascript" language="JavaScript">
var jQuery_1_1_2 = $.noConflict(true);
</script>
<script src="../../templates/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="js/jquery.blockUI.js" type="text/javascript"></script>
<script type="text/javascript" language="JavaScript">
var jQuery_1_3_2 = $.noConflict(true);
$ = jQuery_1_1_2;
</script>
<!-- End of load blockUI with jQuery-1.3.2 -->

</head>
<body>
<table height="100%" width="1000px" cellspacing="0" cellpadding="0" border="0" align="center">
<tr>
<td class="bg_shadow_left"> </td>
<td class="content_bg">