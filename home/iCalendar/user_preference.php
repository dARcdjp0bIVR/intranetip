<?php
// page modifing by: 
/*
 * 2019-06-06 Carlos: Added CSRF token and check it for update.
 */
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../";
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/icalendar_api.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");
intranet_auth();
intranet_opendb();


$iCal = new icalendar();
$iCal_api = new icalendar_api();
$lui = new ESF_ui();

if (!isset($time))
	$time = $_SESSION["iCalDisplayTime"];


if ($_REQUEST['Mode'] == 'Update') {
	
	if(!verifyCsrfToken($csrf_token, $csrf_token_key)){
		intranet_closedb();
		header("Location: /");
		exit;
	}
	
	//debug_r($_POST);
	$Msg = $iCal->Update_User_Preference($_POST);
} 	
	

// $ownDefaultCalID = $iCal->returnOwnerDefaultCalendar();
$ownCalendarList = $iCal->returnViewableCalendar(TRUE,TRUE,TRUE);
foreach($ownCalendarList as $key=>$Arr) {
	$CalendarList[] = array($Arr['Name'],$Arr['CalID']);
}
// debug_r($ownCalendarList);
$CalViewMode = $iCal->returnUserPref($_SESSION['UserID'], 'CalViewMode');
	$CalViewMode = ($CalViewMode == 1) ? 'full' : 'simple';
$DefaultCalendar = $iCal->returnUserPref($_SESSION['UserID'], 'DefaultCalendar');
##########################################################
#$deleteCalButton  = $lui->Get_Input_Button($button_remove, "", $button_remove, "onclick=\"window.location='delete_calendar.php?calID=#%calID%#';\"");
#$deleteCalButton .= $lui->Get_Input_Button($button_cancel, "", $button_cancel, "$('#deleteCalendarWarning').jqmHide();");
#$deleteCalButton = addslashes($deleteCalButton);

# Button
$deleteCalButton = "<input type='button' name='' value='$iCalendar_NewEvent_Reminder_Remove' onclick='confirmDeleteRedirect(#%calID%#)' />";
$deleteCalButton .= "<input type='button' name='' value='".$Lang['btn_cancel']."' onclick='hideDeleteAlertMsg()' />";


# Main
$CalLabeling = '';
$CalLabeling .= $lui->Get_Div_Open('sub_page_title','style="width:50%"');
	$CalLabeling .= $iCalendar_User_Preference;
$CalLabeling .= $lui->Get_Div_Close();
$CalLabeling .= $lui->Get_Div_Open('btn_link','style="float:right;clear:none"');
	$CalLabeling .= $lui->Get_HyperLink_Open('index.php');
		$CalLabeling .= $iCalendar_BackTo_Calendar;
	$CalLabeling .= $lui->Get_HyperLink_Close();
$CalLabeling .= $lui->Get_Div_Close();
// $CalLabeling .= $lui->Get_Br('style="clear: both;"');

$CalMain = '';
$CalMain .= $lui->Get_Div_Open('cal_board');
	$CalMain .= $lui->Get_Span_Open('','class="cal_board_02"');
	$CalMain .= $lui->Get_Span_Close();
	$CalMain .= $lui->Get_Div_Open('','class="cal_board_content"');
		$CalMain .= $lui->Get_Div_Open('calendar_wrapper','style="float:left; background-color:#dbedff"');
			$CalMain .= $lui->Get_Div_Open('cal_manage_calendar');
				$CalMain .= $lui->Get_Div_Open('cal_list_mycal','style="width: 95%; padding-left: 10px; padding-right: 10px; min-height: 500px;"');
					$CalMain .= $lui->Get_Form_Open('form1','POST',$PHP_SELF.'?Mode=Update');
						$CalMain .= $lui->Get_Table_Open('','','','','','','style="font-size:11px; font-weight:normal"');
							$CalMain .= $lui->Get_Table_Row_Open();
								$CalMain .= $lui->Get_Table_Cell_Open();
									$CalMain .= $iCalendar_UserPref_DefaultMonthView.':';
								$CalMain .= $lui->Get_Table_Cell_Close();
								$CalMain .= $lui->Get_Table_Cell_Open();
									$CalMain .= $lui->Get_Label_Open();
										$CalMain .= $lui->Get_Input_Radiobutton('CalViewMode','CalViewMode[]','simple',($CalViewMode == 'simple' ? true :false));
										$CalMain .= $iCalendar_viewMode_simple;
									$CalMain .= $lui->Get_Label_Close();
									$CalMain .= $lui->Get_Label_Open();
										$CalMain .= $lui->Get_Input_Radiobutton('CalViewMode','CalViewMode[]','full',($CalViewMode == 'full' ? true :false));
										$CalMain .= $iCalendar_viewMode_full;
									$CalMain .= $lui->Get_Label_Close();
								$CalMain .= $lui->Get_Table_Cell_Close();
							$CalMain .= $lui->Get_Table_Row_Close();
							$CalMain .= $lui->Get_Table_Row_Open();
								$CalMain .= $lui->Get_Table_Cell_Open();
									$CalMain .= $iCalendar_UserPref_DefaultCalendar.':';
								$CalMain .= $lui->Get_Table_Cell_Close();
								$CalMain .= $lui->Get_Table_Cell_Open();
									$CalMain .= $lui->Get_Input_Select('CalID','CalID',$CalendarList,$DefaultCalendar);
								$CalMain .= $lui->Get_Table_Cell_Close();
							$CalMain .= $lui->Get_Table_Row_Close();
						$CalMain .= $lui->Get_Table_Close();
						$CalMain .= $lui->Get_Div_Open('form_btn');
							$CalMain .= $lui->Get_Input_Button('btn_submit','btn_submit',$Lang['btn_save'],'class="button_main_act" onclick="submitForm();"');
							$CalMain .= '&nbsp;';
							$CalMain .= $lui->Get_Input_Button('btn_cancel','btn_cancel',$Lang['btn_cancel'],'class="button_main_act" onclick="window.location=\'index.php\'"');
						$CalMain .= $lui->Get_Div_Close();
						$CalMain .= csrfTokenHtml(generateCsrfToken());
					$CalMain .= $lui->Get_Form_Close();
				$CalMain .= $lui->Get_Div_Close();
			$CalMain .= $lui->Get_Div_Close();
		$CalMain .= $lui->Get_Div_Close();
	$CalMain .= $lui->Get_Div_Close();
	
	$CalMain .= '<span class="cal_board_05"><span class="cal_board_06"></span></span>';
$CalMain .= $lui->Get_Div_Close();

$CalMain .= "<div class='jqmWindow' id='deleteCalendarWarning'>
				<p id='delConfirmMsg' align='center'></p>
				<p id='delButton' align='center'></p>
			</div>";
# Generate UI
$display = $iCal->Get_iCalendar_Template($CalLabeling, $CalMain, '','','','','',false,false);
?>
<style type="text/css">@import url(css/main.css);</style>
<style type="text/css">
#deleteCalendarWarning {
	font-family:"Arial","Helvetica","sans-serif";
}
</style>
<?php
// include_once($PathRelative."src/include/template/general_header.php");

echo '<script>';
echo 'var SessionExpiredWarning = "' . $Lang['Warning']['SessionExpired'] . '";';
echo 'var SessionKickedWarning = "' . $Lang['Warning']['SessionKicked'] . '";';
echo 'var SessionExpiredMsg = "' . $SYS_CONFIG['SystemMsg']['SessionExpired'] . '";';
echo 'var SessionKickedMsg = "' . $SYS_CONFIG['SystemMsg']['SessionKicked'] . '";';
echo '</script>';
include_once("headerInclude.php");
?>
<!--
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.blockUI.js"></script>

<script type="text/javascript" src="js/jqModal.js"></script>
<link rel="stylesheet" type="text/css" href="css/jqModal.css" />

<script type="text/javascript" src="js/jquery.simpleColor.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
-->
<!-- End jqModal include -->
<script language="javascript">

var isBlocking = false;

$().ready(function() {
	// define the dialog DIVs
	$('#deleteCalendarWarning').jqm({trigger: false});
});

function hideDeleteAlertMsg() {
	$('#deleteCalendarWarning').jqmHide();
}

function confirmDeleteRedirect(calID) {
	window.location = 'delete_calendar.php?calID=' + calID;
}

function submitForm() {
	document.form1.submit();
}

<?php
	# Check if periodic Ajax call is required to get Popup reminder
	echo $iCal->haveFutureReminder(12);
?>
</script>

<div id="module_bulletin" class="module_content">
<?php
	echo $lui->Get_Sub_Function_Header("iCalendar",$Msg);
	echo $display;
?>
</div>
<?php
// include_once($PathRelative."src/include/template/general_footer.php");
include_once("footerInclude.php");
?>
