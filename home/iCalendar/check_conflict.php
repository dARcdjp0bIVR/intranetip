<?php
// page modifing by: 
$PathRelative = "../../../";

$CurSubFunction = "iCalendar"; 
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/icalendar_api.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_auth();
intranet_opendb(); 

$iCal = new icalendar();
$lebooking = new libebooking();

####################################### START CHECK #######################################

$eventDate = !intranet_validateDate($eventDate)? date("Y-m-d") : $eventDate;
$eventDate = trim($eventDate);
$eventID = trim($eventID);
$eventDateOnly = $eventDate;

// if (!$iCal->isValidDate($eventDate)) {
	// echo "$eventDate";
	// exit;
// }


if ($iCal->isValidHour($eventHr) && $iCal->isValidMin($eventMin) && is_numeric($duration)) {
	$eventDate = "$eventDate $eventHr:$eventMin:00";
} else {
	echo "Error";
	exit;
}

$ebookingStartTime = str_pad($eventHr, 2, 0, STR_PAD_LEFT).':'.str_pad($eventMin, 2, 0, STR_PAD_LEFT).':00';
$ebookingEndTime = str_pad($eventEndHr, 2, 0, STR_PAD_LEFT).':'.str_pad($eventEndMin, 2, 0, STR_PAD_LEFT).':00';

$eventTs = $iCal->sqlDatetimeToTimestamp($eventDate);
$endTs = $eventTs + $duration*60;

$lower_bound = $eventTs;
$upper_bound = $endTs;

# Differ from a Offset 1970101
$lower_bound += $iCal->offsetTimeStamp;
$upper_bound += $iCal->offsetTimeStamp;

$result = $iCal->returnEventConflictArr($eventID, $lower_bound, $upper_bound);

if (sizeof($result) > 0) {
	$timeForReplace = date("H:i",$eventTs)."-".date("H:i",$endTs);
	//$x = "<p>".sprintf($i_Calendar_Conflict_Warning, $timeForReplace)."</p>";
	$x = "<p>".sprintf($i_Calendar_Conflict_Warning, $timeForReplace)."</p>";
	$x .= "<ul style='text-align:left'>";
	
	for($i=0; $i<sizeof($result); $i++) {
		$tempStartTs = $iCal->sqlDatetimeToTimestamp($result[$i]["EventDate"]);
		$tempEndTs = $tempStartTs + $result[$i]["Duration"]*60;
		$x .= "<li>".$result[$i]["Title"];
		$x .= " (".date("H:i",$tempStartTs)." To ".date("H:i",$tempEndTs).")";
		$x .= " on ".$iCalendar_NewEvent_Repeats_WeekdayArray2[date("w",$tempStartTs)];
		$x .= ", ".date("Y-m-d",$tempStartTs)."</li>";
	}
	
	$x .= "</ul>";
	echo $x;
} else {
	// check ebooking conflict
	$noConflict = true;
	if ($plugin['eBooking'] && $hiddenBookingIDs != '') {
		$hiddenBookingIdAry = explode(',', $hiddenBookingIDs);
		$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($hiddenBookingIDs) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
		$arrRoomID = $iCal->returnVector($sql);
		$roomId = $arrRoomID[0];
		
		$RoomNeedApproval[$hiddenBookingIDs] = $lebooking->Check_If_Booking_Need_Approval($_SESSION['UserID'], '', $roomId);
		if (!$RoomNeedApproval[$hiddenBookingIDs]) {
			$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $eventDateOnly, $ebookingStartTime, $ebookingEndTime);
			if (!$IsRoomAvailable) {
				echo '<p>'.$i_Calendar_eBookingEvent_TimeClashWarning.'</p>';
				$noConflict = false;
			}
		}
	}
	
	if ($noConflict) {
		while (@ob_end_clean());
		echo "No conflict";
	}
}

######################################## END CHECK ########################################

intranet_closedb();


?>