<?php
################# Change Log [Start] #####
#
#	Date : 2015-11-09 (Omas)
# 		 fixed delete iCal temp records only
#
################## Change Log [End] ######
$PATH_WRT_ROOT = "../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_opendb();

$li = new libdb();

$deleteAll = $_POST['deleteAll'];

if ($deleteAll == 1) {
	$strBookingIDs = $_POST['strBookingIDs'];
}
else {
	// delete iCal temp records only
	// 2015-11-09
	//$sql = "Select BookingID From INTRANET_EBOOKING_RECORD Where BookingStatus = '".LIBEBOOKING_BOOKING_STATUS_EBOOKING_TEMPORY."' And BookingID IN ($strBookingIDs)";
	$sql = "Select BookingID From INTRANET_EBOOKING_BOOKING_DETAILS Where BookingStatus = '".LIBEBOOKING_BOOKING_STATUS_EBOOKING_TEMPORY."' And BookingID IN ($strBookingIDs)";
	$bookingAry = $li->returnResultSet($sql);
	$targetBookingIdAry = Get_Array_By_Key($bookingAry, 'BookingID');
	$strBookingIDs = implode(',', $targetBookingIdAry);
}




$sql = "DELETE FROM INTRANET_EBOOKING_RECORD WHERE BookingID IN ($strBookingIDs)";
echo $sql."<BR>";
$result['DeleteFromIntranet_Ebooking_Record'] = $li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($strBookingIDs)";
echo $sql."<BR>";
$result['DeleteFromIntranet_Ebooking_Room_Booking_Details'] = $li->db_db_query($sql);

//$sql = "DELETE FROM INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS WHERE BookingID IN ($strBookingIDs)";
//echo $sql."<BR>";
//$result['DeleteFromIntranet_Ebooking_Facilities_Booking_Details'] = $li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID IN ($strBookingIDs)";
echo $sql."<BR>";
$result['DeleteFromIntranet_Ebooking_Calendar_Event_Relation'] = $li->db_db_query($sql);

intranet_closedb();
?>