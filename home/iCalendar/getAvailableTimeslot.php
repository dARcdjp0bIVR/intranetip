<?php
// page modifing by: 
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../"; 
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once("choose/new/User_Group_Transformer.php");
intranet_auth();
intranet_opendb();

$iCal = new icalendar(); 
$db = new libdb();
$transformer = new User_Group_Transformer();
$timeSet = false;

if ($checkType == 'stricted'){
	$startDate = $dateTobeCheck." 07:00:00"; 
	$endDate = $dateTobeCheck." 21:00:00";
}
else{
	if ($startTime=='' && $endTime==''){
		$startDate = $dateTobeCheck.' 00:00:00';
		$endDate = date('Y-m-d H:i:s',strtotime('+1440 minute',strtotime($startDate)));
	}
	else{
		$startDate = $dateTobeCheck.' '.$startTime; 
		$endDate = $dateTobeCheck.' '.$endTime;
		$timeSet = true;
	}
}
// echo $startDate;
// echo $endDate;
// exit;

$userList = Array();
// debug_r($user);
// exit;

for ($i=0; $i<sizeof($user); $i++) {
	$viewerID = Array();
	$viewerPrefix = substr($user[$i], 0, 1);
	$viewerID = substr($user[$i], 1);
	// echo $viewerID;
	if ($viewerPrefix == 'U')
		$userList[] = $viewerID ;
	else{
		$tempGuest = $transformer->get_userID_from_group($viewerPrefix,$viewerID);
		$userList = array_merge($userList,$tempGuest);
	}
}

$timeRange = '';
if ($checkType == 'stricted'){
	$freeTimeSlot = $iCal->Get_All_Available_Timeslot($userList, $existGuest, $removeGuest,$startDate, $endDate);
}
else{
	$freeTimeSlot = $iCal->Check_User_Availability($userList, $existGuest, $removeGuest,$startDate, $endDate);
	
}


// debug_r($freeTimeSlot);
// exit;

/*for ($i = 0; $i < count($user); $i++){	
	$userList .= substr($user[$i],1).",";
}


$sql = "exec Get_Empty_Timeslot '$startDate', '$endDate', '".$_SESSION['SchoolCode']."', '$userList'";
$result = $db->returnArray($sql);*/
//echo count($user)."<br>";
//echo $sql;
///echo count($result)."<br>";





$table = '<table width="97%" cellspacing="1" cellpadding="0" border="0" bgcolor="#cccccc">';
	$table .= '<tr>
				<td class="system_row" style="padding-left: 3px; padding-right: 0px" colspan="2">'.date("d/m/Y",strtotime($dateTobeCheck));
	$table .= ($timeSet?'&nbsp;('.substr($startTime,0,5).' - '.substr($endTime,0,5).')':'');
	$table .= '</td></tr>';
	
if (count($freeTimeSlot) > 0){
	
	$temp = "";
	$cnt = 1;	
	$isSystemRow = false; 
	for ($i = 0; $i < count($freeTimeSlot); $i++){		
		if ($checkType == 'stricted'){
			$startTime = date("G:i",strtotime($freeTimeSlot[$i]["startTime"]));
			$endTime = date("G:i",strtotime($freeTimeSlot[$i]["endTime"]));
			
			if (empty($temp)){
				$table .= '<tr>';
				$table .= '<td style="padding-left: 3px; padding-right: 0px" class="'.($isSystemRow? "system_row":"search_row").'">'.$cnt.'.</td>';
				$table .= '<td style="padding-left: 3px; padding-right: 0px" class="'.($isSystemRow? "system_row":"search_row").'">'.$startTime." - ";
				$temp = $endTime;
				$cnt++;
			}
			elseif ($temp != $startTime){
				$table .= $temp."</td></tr>";
				$table .= '<tr>';
				$table .= '<td style="padding-left: 3px; padding-right: 0px" class="'.($isSystemRow? "system_row":"search_row").'">'.$cnt.'.</td>';
				$table .= '<td style="padding-left: 3px; padding-right: 0px" class="'.($isSystemRow? "system_row":"search_row").'">'.$startTime." - ";
				$temp = $endTime;
				$isSystemRow != $isSystemRow;
				$cnt++;
			}
			else{
				$temp = $endTime;
			}
		}
		else{
			$table .= $cnt>0?"</td></tr>":'';
			$table .='<td style="padding-left: 3px; padding-right: 0px" class="'.($isSystemRow? "system_row":"search_row").'">'.$freeTimeSlot[$i]['UserName'].'</td>';
			$table .= '<td style="padding-left: 3px; padding-right: 0px" class="'.($isSystemRow? "system_row":"search_row").'">'.($freeTimeSlot[$i]['state']==1?'<span style="color:green">'.$iCalendar_available.'<span>':'<span style="color:red">'.$iCalendar_unavailable.'<span>');
			$isSystemRow != $isSystemRow;
			$cnt++;
		}
	}

	$table .= $temp."</td></tr>";
	

}

$table .= "</table>";
	echo $table;


intranet_closedb();






?>