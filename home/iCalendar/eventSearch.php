<?php
// page modifing by: 
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../";
$ImagePathAbs = $PATH_WRT_ROOT."theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/icalendar_api.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ESF_ui.php");
 
if (empty($_SESSION['UserID'])) {
	//header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	echo 'SESSION_EXPIRED';
	exit;
}


intranet_opendb();
$ts = microtime(0); 
$iCal_api = new icalendar_api();
$iCal = new icalendar();
$lui = new ESF_ui();

$print = ($search!=1);
$endTime = '';
$year = date('Y', $startTime);
$month = date('m', $startTime);
$day = date('d', $startTime); 
$iCal_api->CreateTempEventTable();
$i_title = "<div class=\"report_title\"><strong>";

switch ($period){
	case 'monthly':
		$lower_bound= mktime(0,0,0,$month,1,$year);
		$upper_bound = strtotime('+1 month',$lower_bound);
		$i_title .= $iCalendar_NewEvent_Repeats_MonthArray[date('n', mktime(0,0,0,$month,$day,$year))-1]." ".$year;
	break;
	case 'weekly':
		$current_week = $iCal->get_week($year, $month, $day, "Y-m-d H:i:s");
		$lower_bound = $iCal->sqlDatetimeToTimestamp($current_week[0]);
		$upper_bound = $iCal->sqlDatetimeToTimestamp($current_week[6]);
		$upper_bound = $upper_bound + 24*60*60;
		$lower_bound += $iCal->offsetTimeStamp;
		$upper_bound += $iCal->offsetTimeStamp;
		$current_week = $iCal->get_week($year, $month, $day, "F j, Y");
		$i_title .= "$current_week[0] - $current_week[6]";
	break;
	case 'daily':
		$lower_bound = mktime(0,0,0,$month,$day,$year);
		$upper_bound = $lower_bound+24*60*60;
		
		# Differ from a Offset 1970101
		$lower_bound += $iCal->offsetTimeStamp;
		$upper_bound += $iCal->offsetTimeStamp;
		$i_title .= date("F j, Y",$startTime);
	break;
	case 'agenda':
		$startDate = date("Y-m-d");
		if ($agendaPeriod == "1") {
			$endDate = date("Y-m-d", time(date("Y-m-d")." 00:00:00") + (7*24*60*60));
		} else if  ($agendaPeriod == "2") {
			$currentWeek = $iCal->get_week(date("Y"), date("n"), date("j"), "Y-m-d");
			$startDate = $currentWeek[0];
			$endDate = $currentWeek[6];
		} else {
			$endDate = date("Y-m-d", time(date("Y-m-d")." 00:00:00") + (24*60*60));
		}
		$lower_bound = $iCal->sqlDatetimeToTimestamp($startDate." 00:00:00");
		$upper_bound = $iCal->sqlDatetimeToTimestamp($endDate." 00:00:00") - 1;
		
		$yearUI = date('Y', strtotime($endDate));
		$monthUI = date('m', strtotime($endDate));
		$dateUI = date('j', strtotime($endDate));
		$dateUI = (strlen($dateUI) == 1) ? str_pad(($dateUI-1), 2, 0, STR_PAD_LEFT) : $dateUI;
		$endDateUI = $yearUI.'-'.$monthUI.'-'.$dateUI;
		
		$i_title .= ($agendaPeriod == "1" || $agendaPeriod == "2") ? "$startDate to $endDateUI" : "$startDate";
	break;
};

$i_title .= "</strong></div>";
$lower_bound += $iCal->offsetTimeStamp;
$upper_bound += $iCal->offsetTimeStamp;

$searchValue = cleanCrossSiteScriptingCode($searchValue);
$searchCase = $iCal_api->searchCaseParser($searchValue);
$str = $iCal_api->formatSearchValueString($searchValue);

# New approach
$maxPage = 0;

if ($searchType == 'A' || $period == 'A'){
	$iCal->Get_All_Related_Event('', '', true, $searchCase,true);
	$sql = 'select count(*) from TempEvent';
	$noEvent = $iCal_api->returnVector($sql); 
	$maxPage = ceil($noEvent[0]/$maxPageNo);
	$recordFrom = ($pageNo-1)*$maxPageNo;
	$sql = 'select * from TempEvent order by EventDate DESC limit '.$recordFrom.','.$maxPageNo;
	$events = $iCal_api->returnArray($sql); 			
	$events=$iCal_api->formatExternalEvent($events,"");
	$iCal->PreloadOwnerNames($events);
	//krsort($events);
	$x = $iCal->Print_Search_result($str,$print);
}
else{
	$events=$iCal->Get_All_Related_Event($lower_bound, $upper_bound, true, $searchCase);
	if (count($events) > 0)
		krsort($events);
	if ($period == "monthly") {
		$x = $iCal->generateMonthlyCalendar($year, $month,$print);
	} else if ($period == "weekly") {
		$x = $iCal->Generate_Weekly_Calendar($year, $month, $day,$print);
	} else if ($period == "daily") {
		$x = $iCal->Generate_Daily_Calendar($year, $month, $day,$print);
	}
	else if ($period == "agenda") {
		$x = $iCal->Print_Agenda($startDate, $endDate, $print, $response, $agendaPeriod);
	}	
	echo "<div style='padding:3px;'>".str_replace('%%value%%', stripslashes($str),$iCalendar_eventSearch_searchingFor)."</div>";
}
$x .="<input type='hidden' value='$maxPage' id='resultMaxPage' name='resultMaxPage'>";
intranet_closedb();

if ($print){ 
	echo '
	<link rel="stylesheet" href="css/main.css" type="text/css" />
	<link rel="stylesheet" href="css/display_calendar.css" type="text/css" />

	<style type="text/css" media="print">
		div,table,td,tr,body,span{
			overflow : visible;
		}
	</style> 

	<script language="javascript">
	// Fix for IE6 image flicker (cannot solve the case of ":hover")
	try {
		document.execCommand("BackgroundImageCache", false, true);
	} catch(err) {}
	</script>';
	include_once($PATH_WRT_ROOT."templates/2009a/layout/print_header.php");
	echo 
		'<br>
		<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td align="center">'.$i_title.'</td>
			</tr>
		</table>
		<div id="cal_board_content">
			<div id="calendar_wrapper" style="width:98%">';
}
echo $x;
if ($period == "agenda"){
	echo '<script language="javascript">
			$("select[@name=selectAgendaPeriod]").remove();
		</script>';
}

if ($print){
	echo '
		</div>
	</div>
	<br/ >
	<script type="text/javascript" defer="1">
	window.print();
	</script>';
	include_once($PATH_WRT_ROOT."templates/2009a/layout/print_footer.php");
}
?>