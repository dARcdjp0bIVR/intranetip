<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libicalendar.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$iCal = new libicalendar;

# Event date and time passed from other page
if (isset($calDate)) {
	$calDatePiece = explode("-",$calDate);
	if (checkdate($calDatePiece[1],$calDatePiece[2],$calDatePiece[0])) {
		$eventDate = $calDatePiece[0]."-".$calDatePiece[1]."-".$calDatePiece[2];
	}
	if (is_numeric($calDatePiece[3]) && $calDatePiece[3]>=0 && $calDatePiece[3]<=23) {
		$eventTime = $calDatePiece[3].":00";
		$eventHr = $calDatePiece[3];
		$eventMin = 0;
		$durationHr = 0;
		$durationMin = 0;
	}
}

// default access right
$access = "D";

$editMode = false;
// if editing event, retrieve event detail
if (isset($editEventID)) {
	if (substr($editEventID,0,5) == "event" && is_numeric(substr($editEventID, 5))) {
		$eventID = substr($editEventID, 5);
		$editMode = true;
		$li = new libdb();
		
		$sql  = "SELECT a.* FROM CALENDAR_EVENT_ENTRY AS a ";
		$sql .= "WHERE a.EventID=$eventID";
		$result = $li->returnArray($sql);
		
		if (sizeof($result) > 0) {
			$repeatID = $result[0]["RepeatID"];
			$calID = $result[0]["CalID"];
			if ($repeatID != NULL) {
				$isRepeatEvent = true;
			} else {
				$isRepeatEvent = false;
			}
			$title = htmlspecialchars($result[0]["Title"]);
			$description = htmlspecialchars($result[0]["Description"]);
			$location = htmlspecialchars($result[0]["Location"]);
			$url = $result[0]["Url"];
			$eventDateTime = $result[0]["EventDate"];
			$eventTs = $iCal->sqlDatetimeToTimestamp($eventDateTime);
			$eventDateTimePiece = explode(" ", $eventDateTime);
			$eventDate = $eventDateTimePiece[0];
			if ($result[0]["IsAllDay"] == 0) {
				$eventTime = $eventDateTimePiece[1];
				$eventTimePiece = explode(":", $eventTime);
				$eventHr = $eventTimePiece[0];
				$eventMin = $eventTimePiece[1];
				$duration = (int)$result[0]["Duration"];
				$durationHr = (int)($duration/60);
				$durationMin = $duration-$durationHr*60;
			}
			$isImportant = $result[0]["IsImportant"];
			$access = $result[0]["Access"];
			$createdBy = $result[0]["UserID"];
			
			// Repeat Pattern
			if($isRepeatEvent) {
				# 1st event date in the recurrence series
				$FirstRepeatEventDateTime = $iCal->returnFirstEventDate($repeatID);
				$FirstRepeatEventDateTimePiece = explode(" ", $FirstRepeatEventDateTime);
				$FirstRepeatEventDate = $FirstRepeatEventDateTimePiece[0];
				
				
				$sql  = "SELECT * FROM CALENDAR_EVENT_ENTRY_REPEAT ";
				$sql .= "WHERE RepeatID=".$repeatID;
				
				$result = $li->returnArray($sql);
				if (sizeof($result) > 0) {
					$repeatEndDateTime = $result[0]["EndDate"];
					$repeatEndPiece = explode(" ", $repeatEndDateTime);
					$repeatEndDate = $repeatEndPiece[0];
					$repeatType = $result[0]["RepeatType"];
					$repeatPattern = $repeatType."#".$repeatEndDateTime."#".$result[0]["Frequency"]."#".$result[0]["Detail"];
					
					$frequency = 0;
					switch($repeatType) {
						case "DAILY":
							$repeatDailyDay = $result[0]["Frequency"];
							$frequency = $repeatDailyDay;
							break;
						case "WEEKDAY":
						case "MONWEDFRI":
						case "TUESTHUR":
							break;
						case "WEEKLY":
							$repeatWeeklyRange = $result[0]["Frequency"];
							$frequency = $repeatWeeklyRange;
							$detail = $result[0]["Detail"];
							break;
						case "MONTHLY":
							$repeatMonthlyRange = $result[0]["Frequency"];
							$frequency = $repeatMonthlyRange;
							$detail = $result[0]["Detail"];
							break;
						case "YEARLY":
							$repeatYearlyRange = $result[0]["Frequency"];
							$frequency = $repeatYearlyRange;
							break;
					}
				}
			}
			
			// Reminder Info
			$sql  = "SELECT a.* FROM CALENDAR_REMINDER AS a ";
			$sql .= "WHERE a.EventID=$eventID AND a.UserID=".$_SESSION['UserID'];
			$result = $li->returnArray($sql);
			if (sizeof($result) > 0) {
				for ($i=0; $i<sizeof($result); $i++) {
					$reminderID[$i] = $result[$i]["ReminderID"];
					$reminderType[$i] = $result[$i]["ReminderType"];
					$reminderDate[$i] = $result[$i]["ReminderDate"];
					$lastSent[$i] = $result[$i]["LastSent"];
					$timeSent[$i] = $result[$i]["TimeSent"];
				}
			}
			
			// Guest Info
			$sql  = "SELECT a.*, b.EnglishName AS Name FROM CALENDAR_EVENT_USER AS a, INTRANET_USER AS b ";
			$sql .= "WHERE a.UserID=b.UserID AND a.EventID=$eventID";
			$result = $li->returnArray($sql);
			
			if (sizeof($result) > 0) {
				if (sizeof($result) == 1 && $result[0]["UserID"] == $_SESSION['UserID']) {
					$haveGuest = false;
				} else {
					$haveGuest = true;
				}
				$guestStatusWait = 0;
				$guestStatusYes = 0;
				$guestStatusNo = 0;
				$guestStatusMaybe = 0;
				for ($i=0; $i<sizeof($result); $i++) {
					$guestUserID[$i] = $result[$i]["UserID"];
					$guestName[$i] = $result[$i]["Name"];
					$guestStatus[$i] = $result[$i]["Status"];
					$guestAccess[$i] = $result[$i]["Access"];
					if ($guestStatus[$i] == "W")
						$guestStatusWait++;
					if ($guestStatus[$i] == "A")
						$guestStatusYes++;
					if ($guestStatus[$i] == "R")
						$guestStatusNo++;
					if ($guestStatus[$i] == "M")
						$guestStatusMaybe++;
					// User own status
					if ($guestUserID[$i] == $_SESSION['UserID']) {
						$userStatus = $guestStatus[$i];
						$userAccess = $guestAccess[$i];
					}
				}
			}
		}
	}
}

$selectTime = "";
$selectTime .= "<select name=\"eventHr\"".(isset($eventTime)?"":" disabled").">";
for ($i=0; $i<24; $i++) {
	if (isset($eventTime)) {
		$selectTime .= ($eventHr==$i) ? "<option value=\"$i\" selected>$i</option>" : "<option value=\"$i\">$i</option>\n";
	} else {
		$selectTime .= ((int)date("H")==$i) ? "<option value=\"$i\" selected>$i</option>" : "<option value=\"$i\">$i</option>\n";
	}
}
$selectTime .= "</select>";

$interval = 5;
## start time in minutes
$start_min .= "<select name=\"eventMin\"".(isset($eventTime)?"":" disabled").">";
for ($i=0; $i<(60/$interval); $i++)
{
	$min_value = $i * $interval;
	if ($min_value<10)
	{
		$min_value = "0" . $min_value;
	}
	#echo $eventMin."-".$min_value;
	if (isset($eventTime))
		$start_min .= ($eventMin==$min_value) ? "<option value=\"{$min_value}\" selected>{$min_value}</option>" : "<option value=\"{$min_value}\">{$min_value}</option>";
	else
		$start_min .= "<option value=\"{$min_value}\">{$min_value}</option>";
}
$start_min .= "</select>";

## duration in minutes
$duration_min = "<select name=\"durationMin\"".(isset($durationMin)?"":" disabled").">";
for ($i=0; $i<(60/$interval); $i++)
{
	$min_value = $i * $interval;
	if ($intranet_session_language=="en" && $min_value>0)
	{
		$min_text = $iCalendar_NewEvent_DurationMins;
	} else
	{
		$min_text = $iCalendar_NewEvent_DurationMin;
	}
	if (isset($durationMin))
		$duration_min .= ($durationMin==$min_value) ? "<option value=\"{$min_value}\" selected>{$min_value} {$min_text}</option>" : "<option value=\"{$min_value}\">{$min_value} {$min_text}</option>";
	else
		$duration_min .= "<option value=\"{$min_value}\">{$min_value} {$min_text}</option>";
}
$duration_min .= "</select>";

## Repeats patterns
$array_repeat_name = array($iCalendar_NewEvent_Repeats_Not, $iCalendar_NewEvent_Repeats_Daily, $iCalendar_NewEvent_Repeats_EveryWeekday, 
						$iCalendar_NewEvent_Repeats_EveryMonWedFri, $iCalendar_NewEvent_Repeats_EveryTuesThur, $iCalendar_NewEvent_Repeats_Weekly, 
						$iCalendar_NewEvent_Repeats_Monthly, $iCalendar_NewEvent_Repeats_Yearly);
$array_repeat_data = array("NOT", "DAILY", "WEEKDAY", "MONWEDFRI", "TUESTHUR", "WEEKLY", "MONTHLY", "YEARLY");
$select_repeat_type = getSelectByValueDiffName($array_repeat_data, $array_repeat_name, "name='repeatSelect' id='repeatSelect' onChange='showRepeatCtrl()'", $repeatType, 0, 1); 

## Max
$ReminderLimit = $iCal->reminderLimit;

$linterface = new interface_html();

# Generate HTML for the buttons use in repeat event saving dialog
$repeatSaveButtons  = $linterface->GET_ACTION_BTN($iCalendar_NewEvent_Repeats_SaveThisOnly, "button", "javascript:document.form1.saveRepeatEvent.value='OTI';checkOptionAll(document.form1.elements['viewer[]']);document.form1.submit();");
$repeatSaveButtons .= $linterface->GET_ACTION_BTN($iCalendar_NewEvent_Repeats_SaveAll, "button", "javascript:document.form1.saveRepeatEvent.value='ALL';checkOptionAll(document.form1.elements['viewer[]']);document.form1.submit();");
if ($userAccess == "A" || $userAccess == "W")
	$repeatSaveButtons .= $linterface->GET_ACTION_BTN($iCalendar_NewEvent_Repeats_SaveFollow, "button", "javascript:document.form1.saveRepeatEvent.value='FOL';checkOptionAll(document.form1.elements['viewer[]']);document.form1.submit();");
$repeatSaveButtons .= $linterface->GET_ACTION_BTN($button_cancel, "button", "$('#saveEventWarning').jqmHide();");
#$repeatSaveButtons = addslashes($repeatSaveButtons);

# Generate HTML for the buttons use in event deleting dialog
$deleteEventButtons1  = $linterface->GET_ACTION_BTN($iCalendar_NewEvent_Repeats_SaveThisOnly, "button", "javascript:document.form1.deleteRepeatEvent.value='OTI';document.form1.submit();");
$deleteEventButtons1 .= $linterface->GET_ACTION_BTN($iCalendar_NewEvent_Repeats_SaveAll, "button", "javascript:document.form1.deleteRepeatEvent.value='ALL';document.form1.submit();");
$deleteEventButtons1 .= $linterface->GET_ACTION_BTN($iCalendar_NewEvent_Repeats_SaveFollow, "button", "javascript:document.form1.deleteRepeatEvent.value='FOL';document.form1.submit();");
$deleteEventButtons1 .= $linterface->GET_ACTION_BTN($button_cancel, "button", "document.form1.action='edit_event_update.php';$('#deleteEventWarning1').jqmHide();");
#$deleteEventButtons1 = addslashes($deleteEventButtons1);

$deleteEventButtons2 = $linterface->GET_ACTION_BTN($button_remove, "button", "javascript:document.form1.submit();");
$deleteEventButtons2.= $linterface->GET_ACTION_BTN($button_cancel, "button", "document.form1.action='edit_event_update.php';$('#deleteEventWarning2').jqmHide();");
#$deleteEventButtons2 = addslashes($deleteEventButtons2);

#$PAGE_NAVIGATION[] = array($button_new);

if ($editMode)
	$MODULE_OBJ['title'] = $iCalendar_EditEvent_Header;
else
	$MODULE_OBJ['title'] = $iCalendar_NewEvent_Header;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>
<style type="text/css">
// Fix for IE6 image flicker (cannot solve the case of ":hover")
try {
  document.execCommand("BackgroundImageCache", false, true);
} catch(err) {}


.header {
	font-family:"Arial","Helvetica","sans-serif";
	font-size: 130%;
	background:#DDDDDD none repeat scroll 0%;
	line-height:1.2em;
	margin:0pt;
	padding:0pt;
}

.tablelink {
	cursor:pointer;
	white-space:nowrap;
	color:#2286C5;
	font-family:"Arial","Helvetica","sans-serif";
	font-size:12px;
	text-decoration:none;
}

span.tablelink:hover {
	color:#FF0000;
	font-family:"Arial","Helvetica","sans-serif";
	font-size:12px;
	text-decoration:none;
}

.removedGuest {
	background-color: #CCCCCC;
}

.undoRemovedGuest {
	background-color: none;
}

// style for error message
label.error {
	color: #FF0000;
}

input.error {
	border:1px dotted red;
}

#errMessageBox {
	text-align: left;
	font-family:"Arial","Helvetica","sans-serif";
	font-size: 12px;
	color: red;
}

</style>
<!-- Start jqModal include -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<link rel="stylesheet" type="text/css" href="css/jqModal.css" />
<!-- End jqModal include -->
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script language="javascript">
function removeViewer(){
	checkOptionRemove(document.form1.elements["viewer[]"]);
}

function enableTimeSelect() {
	if (document.getElementById("is_allday_1").checked) {
		document.form1.eventHr.disabled = false;
		document.form1.eventMin.disabled = false;
		document.form1.durationHr.disabled = false;
		document.form1.durationMin.disabled = false;
	}
	
	if (document.getElementById("is_allday_0").checked) {
		document.form1.eventHr.disabled = true;
		document.form1.eventMin.disabled = true;
		document.form1.durationHr.disabled = true;
		document.form1.durationMin.disabled = true;
	}
	
}

function removeReminder(reminder) {
	var divReminder = reminder.parentNode.parentNode;
	var spanToRemove = reminder.parentNode;
	
	var removed = divReminder.removeChild(spanToRemove);
	var spans = divReminder.getElementsByTagName("span");
	
	if (spans.length == 1) {
		document.getElementById("noReminderMsg").style.display = "block";
	}
	
	if (spans.length <= (<?=$ReminderLimit?>*2-1)) {
		document.getElementById("addReminderSpan").style.display = "block";
	}
}

function addReminder() {
	var divReminder = document.getElementById("reminderDiv");
	document.getElementById("noReminderMsg").style.display = "none";
	
	var spans = divReminder.getElementsByTagName("span");
	if (spans.length == (<?=$ReminderLimit?>*2-1))
		document.getElementById("addReminderSpan").style.display = "none";
	
	var newSpan = "<span>";
	newSpan += "<?= str_replace("\n", "", $iCal->generateReminderSelect()) ?>";
	newSpan += "&nbsp;<span class='tablelink' onClick='removeReminder(this)'><?=$iCalendar_NewEvent_Reminder_Remove?></span> <br />";
	newSpan += "</span>";
	
	divReminder.innerHTML += newSpan;
}

function showRepeatCtrl() {
	var repeatMsgTable = document.getElementById("repeatMsgTable");
	var repeatMsgSpan = document.getElementById("repeatMsgSpan");
	var repeatSelect = document.getElementById("repeatSelect");
	
	var repeatCtrl = document.getElementById("repeatCtrl");
	var repeatCtrlDaily = document.getElementById("repeatCtrlDaily");
	var repeatCtrlWeekly = document.getElementById("repeatCtrlWeekly");
	var repeatCtrlMonthly = document.getElementById("repeatCtrlMonthly");
	var repeatCtrlYearly = document.getElementById("repeatCtrlYearly");
	var repeatRange = document.getElementById("repeatRange");
	
	document.form1.repeatStart.value = document.form1.eventDate.value;
	
	var repeatSelected = repeatSelect.options[repeatSelect.selectedIndex].value;
	
	switch(repeatSelected)
	{
	case "NOT":
		repeatCtrlDaily.style.display = "none";
		repeatCtrlWeekly.style.display = "none";
		repeatCtrlMonthly.style.display = "none";
		repeatCtrlYearly.style.display = "none";
		
		repeatRange.style.display = "none";
		repeatMsgTable.style.display = "none";
		break;
	case "DAILY":
		repeatCtrlWeekly.style.display = "none";
		repeatCtrlMonthly.style.display = "none";
		repeatCtrlYearly.style.display = "none";
		
		dailyCtrl = "<?=$iCalendar_NewEvent_Repeats_Every?>";
		dailyCtrl += "<select name='repeatDailyDay' onChange='changeRepeatMsg()'>";
		dailyCtrl += "<option value='1'>1 <?=$iCalendar_NewEvent_DurationDay?></option>";
		for (i=2; i<=14; i++)
			dailyCtrl += "<option value='"+i+"'>"+i+" <?=$iCalendar_NewEvent_DurationDays?></option>";
		dailyCtrl += "</select>";
		
		if (repeatCtrlDaily.innerHTML=="") {
			repeatCtrlDaily.innerHTML = dailyCtrl;
		}
		repeatCtrlDaily.style.display = "block";
		repeatRange.style.display = "block";
		
		changeRepeatMsg();
		break;
	case "WEEKDAY":
		repeatCtrlDaily.style.display = "none";
		repeatCtrlWeekly.style.display = "none";
		repeatCtrlMonthly.style.display = "none";
		repeatCtrlYearly.style.display = "none";
		
		changeRepeatMsg();
		repeatRange.style.display = "block";
		break;
	case "MONWEDFRI":
		repeatCtrlDaily.style.display = "none";
		repeatCtrlWeekly.style.display = "none";
		repeatCtrlMonthly.style.display = "none";
		repeatCtrlYearly.style.display = "none";
		
		changeRepeatMsg();
		repeatRange.style.display = "block";
		break;
	case "TUESTHUR":
		repeatCtrlDaily.style.display = "none";
		repeatCtrlWeekly.style.display = "none";
		repeatCtrlMonthly.style.display = "none";
		repeatCtrlYearly.style.display = "none";
		
		changeRepeatMsg();
		repeatRange.style.display = "block";
		break;
	case "WEEKLY":
		weeklyCtrl = "<?=$iCalendar_NewEvent_Repeats_Every?>";
		weeklyCtrl += "<select name='repeatWeeklyRange' onChange='changeRepeatMsg()'>";
		weeklyCtrl += "<option value='1'>"+i+" <?=$iCalendar_NewEvent_DurationWeek?></option>";
		for (i=2; i<=14; i++)
			weeklyCtrl += "<option value='"+i+"'>"+i+" <?=$iCalendar_NewEvent_DurationWeeks?></option>";
		weeklyCtrl += "</select>";
		weeklyCtrl += "<br />";
		weeklyCtrl += "<?=$iCalendar_NewEvent_Repeats_On?><br />";
		<?
			foreach ($iCalendar_NewEvent_Repeats_WeekdayArray as $value) {
		?>
		weeklyCtrl += "<input type='checkbox' name='<?=$value?>' id='<?=$value?>' onClick='changeRepeatMsg()' <?=($value==date("D"))?"CHECKED":""?> /> ";
		weeklyCtrl += "<label for='<?=$value?>'><?=$value?></label>&nbsp;&nbsp;";
		<?
			}
		?>
		repeatCtrlDaily.style.display = "none";
		repeatCtrlMonthly.style.display = "none";
		repeatCtrlYearly.style.display = "none";
		
		if (repeatCtrlWeekly.innerHTML=="") {
			repeatCtrlWeekly.innerHTML = weeklyCtrl;
		}
		repeatCtrlWeekly.style.display = "block";
		repeatRange.style.display = "block";
		
		changeRepeatMsg();
		break;
	case "MONTHLY":
		monthlyCtrl = "<?=$iCalendar_NewEvent_Repeats_Every?>";
		monthlyCtrl += "<select name='repeatMonthlyRange' onClick='changeRepeatMsg()'>";
		monthlyCtrl += "<option value='1'>"+i+" <?=$iCalendar_NewEvent_DurationMonth?></option>";
		for (i=2; i<=14; i++)
			monthlyCtrl += "<option value='"+i+"'>"+i+" <?=$iCalendar_NewEvent_DurationMonths?></option>";
		monthlyCtrl += "</select>";
		monthlyCtrl += "<br />";
		monthlyCtrl += "<?=$iCalendar_NewEvent_Repeats_By?><br />";
		monthlyCtrl += "<input type='radio' name='monthlyRepeatBy' id='monthlyRepeatBy0' onClick='changeRepeatMsg()' value='dayOfMonth' CHECKED> ";
		monthlyCtrl += "<label for='monthlyRepeatBy0'><?=$iCalendar_NewEvent_Repeats_Monthly3?></label>";
		monthlyCtrl += "<input type='radio' name='monthlyRepeatBy' id='monthlyRepeatBy1' onClick='changeRepeatMsg()' value='dayOfWeek'> ";
		monthlyCtrl += "<label for='monthlyRepeatBy1'><?=$iCalendar_NewEvent_Repeats_Monthly4?></label>";
		
		repeatCtrlDaily.style.display = "none";
		repeatCtrlWeekly.style.display = "none";
		repeatCtrlYearly.style.display = "none";
		
		if (repeatCtrlMonthly.innerHTML=="") {
			repeatCtrlMonthly.innerHTML = monthlyCtrl;
		}
		repeatCtrlMonthly.style.display = "block";
		repeatRange.style.display = "block";
		
		changeRepeatMsg();
		break;
	case "YEARLY":
		repeatCtrlDaily.style.display = "none";
		repeatCtrlWeekly.style.display = "none";
		repeatCtrlMonthly.style.display = "none";
		
		yearlyCtrl = "<?=$iCalendar_NewEvent_Repeats_Every?>";
		yearlyCtrl += "<select name='repeatYearlyRange' onClick='changeRepeatMsg()'>";
		yearlyCtrl += "<option value='1'>"+i+" <?=$iCalendar_NewEvent_DurationYear?></option>";
		for (i=2; i<=14; i++)
			yearlyCtrl += "<option value='"+i+"'>"+i+" <?=$iCalendar_NewEvent_DurationYears?></option>";
		yearlyCtrl += "</select>";
		yearlyCtrl += "<br />";
		
		if (repeatCtrlYearly.innerHTML=="") {
			repeatCtrlYearly.innerHTML = yearlyCtrl;
		}
		repeatCtrlYearly.style.display = "block";
		repeatRange.style.display = "block";
		
		changeRepeatMsg();
		break;
	}
}

function changeRepeatMsg() {
	var repeatSelect = document.getElementById("repeatSelect");
	var repeatMsgSpan = document.getElementById("repeatMsgSpan");
	var repeatMsgTable = document.getElementById("repeatMsgTable");
	var repeatSelected = repeatSelect.options[repeatSelect.selectedIndex].value;
	var eventDate = document.form1.eventDate.value;
	var eventDatePieces = eventDate.split("-");
	var eventDateObj = new Date();
	eventDateObj.setFullYear(eventDatePieces[0],eventDatePieces[1]-1,eventDatePieces[2]);
	
	switch(repeatSelected)
	{
	case "DAILY":
		if (document.form1.repeatDailyDay.value==1)
			newMsg = "<?=$iCalendar_NewEvent_Repeats_Daily?>";
		else
			newMsg = "<?=$iCalendar_NewEvent_Repeats_DailyMsg1?>" + document.form1.repeatDailyDay.value + " <?=$iCalendar_NewEvent_DurationDays?>";
		newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		repeatMsgTable.style.display = "block";
		break;
	case "WEEKDAY":
		newMsg = "<?=$iCalendar_NewEvent_Repeats_EveryWeekday2?>";
		newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		repeatMsgTable.style.display = "block";
		break;
	case "MONWEDFRI":
		newMsg = "<?=$iCalendar_NewEvent_Repeats_EveryMonWedFri2?>";
		newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		repeatMsgTable.style.display = "block";
		break;
	case "TUESTHUR":
		newMsg = "<?=$iCalendar_NewEvent_Repeats_EveryTuesThur2?>";
		newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		repeatMsgTable.style.display = "block";
		break;
	case "WEEKLY":
		if (document.form1.repeatWeeklyRange.value == "1") {
			newMsg = "<?=$iCalendar_NewEvent_Repeats_Weekly2?>";
			checkSame = newMsg;
		} else {
			newMsg = "<?=$iCalendar_NewEvent_Repeats_DailyMsg1?>"+document.form1.repeatWeeklyRange.value+"<?=$iCalendar_NewEvent_Repeats_Weekly3?>";
			checkSame = newMsg;
		}
		
		<?
			for($i=0; $i<sizeof($iCalendar_NewEvent_Repeats_WeekdayArray); $i++) {
		?>
		if (document.form1.<?=$iCalendar_NewEvent_Repeats_WeekdayArray[$i]?>.checked) {
			if (newMsg.match("day"))
				newMsg += ", <?=$iCalendar_NewEvent_Repeats_WeekdayArray2[$i]?>";
			else
				newMsg += "<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[$i]?>";
		}
		<?
			}
		?>
		var weekDayName = new Array("<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[0]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[1]?>",
						"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[2]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[3]?>",
						"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[4]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[5]?>",
						"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[6]?>");
		if (checkSame == newMsg)
			newMsg += weekDayName[eventDateObj.getDay()];
		newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		repeatMsgTable.style.display = "block";
		break;
	case "MONTHLY":
		var monthlyRepeatBy0 = document.getElementById("monthlyRepeatBy0");
		var monthlyRepeatBy1 = document.getElementById("monthlyRepeatBy1");
		
		if (document.form1.repeatMonthlyRange.value == "1")
			newMsg = "<?=$iCalendar_NewEvent_Repeats_Monthly2?>";
		else
			newMsg = "<?=$iCalendar_NewEvent_Repeats_DailyMsg1?>"+document.form1.repeatMonthlyRange.value+" <?=$iCalendar_NewEvent_Repeats_Monthly5?>";
		
		if (monthlyRepeatBy0.checked)
			newMsg += " <?=$iCalendar_NewEvent_DurationDay?> "+eventDateObj.getDate();
		if (monthlyRepeatBy1.checked) {
			var nth = 0;
			for(var j=1; j<6; j++) {
				if (eventDateObj.getDate() == NthDay(j, eventDateObj.getDay(), eventDateObj.getMonth(), eventDateObj.getFullYear())) {
					nth = j;
					break;
				}
			}
			var weekDayName = new Array("<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[0]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[1]?>",
						"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[2]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[3]?>",
							"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[4]?>","<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[5]?>",
							"<?=$iCalendar_NewEvent_Repeats_WeekdayArray2[6]?>");
			var countName =  new Array("<?=$iCalendar_NewEvent_Repeats_Count[0]?>", "<?=$iCalendar_NewEvent_Repeats_Count[1]?>", 
							"<?=$iCalendar_NewEvent_Repeats_Count[2]?>", "<?=$iCalendar_NewEvent_Repeats_Count[3]?>", 
							"<?=$iCalendar_NewEvent_Repeats_Count[4]?>");
			newMsg += countName[nth-1] + " " + weekDayName[eventDateObj.getDay()];
		}
		newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		repeatMsgTable.style.display = "block";
		break;
	case "YEARLY":
		if (document.form1.repeatYearlyRange.value == "1")
			newMsg = "<?=$iCalendar_NewEvent_Repeats_Yearly2?>";
		else
			newMsg = "<?=$iCalendar_NewEvent_Repeats_DailyMsg1?>"+document.form1.repeatYearlyRange.value+" <?=$iCalendar_NewEvent_Repeats_Yearly3?>";
		var monthname = new Array("<?=$iCalendar_NewEvent_Repeats_MonthArray[0]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[1]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[2]?>",
						"<?=$iCalendar_NewEvent_Repeats_MonthArray[3]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[4]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[5]?>",
						"<?=$iCalendar_NewEvent_Repeats_MonthArray[6]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[7]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[8]?>",
						"<?=$iCalendar_NewEvent_Repeats_MonthArray[9]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[10]?>","<?=$iCalendar_NewEvent_Repeats_MonthArray[11]?>");
		newMsg += monthname[eventDateObj.getMonth()]+" "+eventDateObj.getDate();
		newMsg += "<?=$iCalendar_NewEvent_Repeats_DailyMsg2?>" + document.form1.repeatEnd.value;
		repeatMsgSpan.innerHTML = newMsg;
		repeatMsgTable.style.display = "block";
		break;
	}
}

function dayOfWeek(day,month,year) {
    var dateObj = new Date();
    dateObj.setFullYear(year,month,day);
    return dateObj.getDay();
}

function isLeapYear(year) {
  return new Date(year,2-1,29).getDate()==29;
}

function NthDay(nth,weekday,month,year) {
	var daysofmonth   = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var daysofmonthLY = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    if (nth > 0)
    	return (nth-1)*7 + 1 + (7+weekday-dayOfWeek((nth-1)*7+1, month, year))%7;
    if (isLeapYear(year))
    	var days = daysofmonthLY[month];
    else
    	var days = daysofmonth[month];
    return days - (dayOfWeek(days,month,year) - weekday + 7)%7;
}

function removeGuest(spanElement,guestUserID) {
	if (spanElement.parentNode.className == "" || spanElement.parentNode.className == "undoRemovedGuest") {
		spanElement.innerHTML = "<?=$button_undo?>";
		spanElement.parentNode.className = "removedGuest";
		document.getElementById("guest-"+guestUserID).value = guestUserID;
	} else {
		spanElement.innerHTML = "<?=$iCalendar_NewEvent_Reminder_Remove?>";
		spanElement.parentNode.className = "undoRemovedGuest";
		document.getElementById("guest-"+guestUserID).value = -1;
	}
}

$.validator.addMethod("yyyymmdd", function(value) {
	return /(19[0-9][0-9]|20[0-9][0-9])-([1-9]|0[1-9]|1[012])-([1-9]|0[1-9]|[12][0-9]|3[01])/.test(value);
}, '<?=$iCalendar_CheckForm_DateWrong?>');

function validateForm() {
	var v = $("#form1").validate({
		errorLabelContainer: $("#errMessageBox"),
		wrapper: "li",
		rules: {
			title: "required",
			eventDate: {
				required: true,
				yyyymmdd: true,
				minLength: 8,
				maxLength: 10
			},
			url: {
				url: true
			},
			repeatEnd: {
				required: function() {
					return $("input[@name='repeatSelect']").val() != "NOT";
				},
				yyyymmdd: true
			}
		},
		messages: {
			title: "<?=$iCalendar_CheckForm_NoTitle?>",
			eventDate: {
				required: "<?=$iCalendar_CheckForm_NoDate?>",
				minLength: "<?=$iCalendar_CheckForm_DateWrong?>",
				maxLength: "<?=$iCalendar_CheckForm_DateWrong?>"
			},
			url: {
				url: "<?=$iCalendar_CheckForm_UrlWrong?>"
			},
			repeatEnd: {
				required: "<?=$iCalendar_CheckForm_NoRepeatEnd?>"	
			}
		}
	});
	
	if(v.form()){
		if (document.form1.isRepeatEvent) {
			$('#saveEventWarning').jqmShow();
		} else {
			if (document.form1.elements["viewer[]"]) {
				checkOptionAll(document.form1.elements["viewer[]"]);
			}
			document.form1.submit();
		}
	} else {
		v.focusInvalid();
		window.scrollTo(0,0);
		return false;
	}
}

$().ready(function() {
	
	// define the dialog DIVs
	$('#saveEventWarning').jqm({trigger: false});
	$('#deleteEventWarning1').jqm({trigger: false});
	$('#deleteEventWarning2').jqm({trigger: false});
	
	// show the confirm dialog for saving repeated event
	$('#saveEvent').click(function() {
		validateForm();
	});
	
	// show the confirm dialog for deleting event
	if (document.form1.deleteEvent) {
		$('#deleteEvent').click(function() {
			document.form1.action = "delete_event.php";
			if (document.form1.isRepeatEvent) {
				$('#deleteEventWarning1').jqmShow();
			} else {
				$('#deleteEventWarning2').jqmShow();
			}
		});
	}
});

</script>
<br />
<ul id="errMessageBox"></ul>
<form name="form1" id="form1" action="<?=$editMode?"edit_event_update.php":"new_event_update.php"?>" method="POST">
<div id="mainDetail" style="width:95%">
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><label for="title"><?= $iCalendar_NewEvent_Title ?></label></td>
		<td width="70%" class="tabletext">
		<?php if (!$editMode || ($userAccess == "A" || $userAccess == "W")) {  ?>
			<input class="textboxtext" type="text" name="title" id="title" maxlength="255" value="<?=$title?>" />
		<?php } else { ?>
			<?=$title?>
		<?php } ?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $iCalendar_NewEvent_DateTime ?></td>
		<td width="70%" class="tabletext">
		<?php if (!$editMode || ($userAccess == "A" || $userAccess == "W")) {  ?>
			<?=$linterface->GET_DATE_FIELD("eventDateDiv", "form1", "eventDate", (isset($eventDate)?$eventDate:date("Y-m-d")), 1, "changeRepeatMsg();")?>
			<?=$editMode?"<input type='hidden' name='oldEventDate' value='$eventDate' />":""?>
			<?=($editMode && !$isAllDay)?"<input type='hidden' name='oldEventTime' value='$eventTime' />":""?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="tabletext">&nbsp;</td>
		<td width="70%" class="tabletext">
			<table width="100%" border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td class="tabletext" width="1"><input type="radio" name="isAllDay" id="is_allday_0" value="1" <?=isset($eventTime)?"":"checked=\"checked\""?> onClick="enableTimeSelect()" /></td>
					<td class="tabletext"><label for="is_allday_0"><?=$iCalendar_NewEvent_IsAllDay?></label></td>
				</tr>
				<tr>
					<td class="tabletext"><input type="radio" name="isAllDay" id="is_allday_1" value="0" <?=isset($eventTime)?"checked=\"checked\"":""?> onClick="enableTimeSelect()" /></td>
					<td class="tabletext">
						<label for="is_allday_1"><?=$iCalendar_NewEvent_StartTime?></label> 
						<?= $selectTime ?>:<?= $start_min ?>
					</td>
				</tr>
				<tr>
					<td class="tabletext"></td>
					<td class="tabletext">
						<label for="is_allday_1"><?=$iCalendar_NewEvent_Duration?></label> 
						<select name="durationHr"<?=(isset($durationHr)?"":" disabled")?>>
							<option value="0"<?=($durationHr==0)?"selected":""?>>0 hr.</option>
							<option value="1"<?=($durationHr==1)?"selected":""?>>1 <?=$iCalendar_NewEvent_DurationHr?></option>
							<?php for($i=2;$i<13;$i++) echo "<option value=\"$i\"".(($durationHr==$i)?"selected":"").">$i $iCalendar_NewEvent_DurationHrs</option>\n"; ?>
						</select> 
						<?= $duration_min ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $iCalendar_NewEvent_Repeats ?></td>
		<td width="70%" class="tabletext">
			<div id="repeatDiv">
				<?=$select_repeat_type?>
			<?php
				if(isset($repeatType)) {
					$repeatEndField = $linterface->GET_DATE_FIELD("repeatEndDiv", "form1", "repeatEnd", $repeatEndDate);
					switch($repeatType) {
						case "DAILY":
							$repeatCtrlDaily = $iCalendar_NewEvent_Repeats_Every;
							$repeatCtrlDaily .= "<select name='repeatDailyDay' onChange='changeRepeatMsg()'>";
							$repeatCtrlDaily .= "<option value='1'".(($frequency==$i)?"selected":"").">1 $iCalendar_NewEvent_DurationDay</option>";
							for ($i=2; $i<=14; $i++) {
								$repeatCtrlDaily .= "<option value='$i'".(($frequency==$i)?"selected":"").">$i $iCalendar_NewEvent_DurationDays</option>";
							}
							$repeatCtrlDaily .= "</select>";
							$repeatMsg = $iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate, $frequency);
							break;
						case "WEEKDAY":
							$repeatMsg = $iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate);
							break;
						case "MONWEDFRI":
							$repeatMsg = $iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate);
							break;
						case "TUESTHUR":
							$repeatMsg = $iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate);
							break;
						case "WEEKLY":
							$weekday_flag = preg_split('#(?<=.)(?=.)#s', $weeklyDetail);
							
							$repeatCtrlWeekly = $iCalendar_NewEvent_Repeats_Every;
							$repeatCtrlWeekly .= "<select name='repeatWeeklyRange' onChange='changeRepeatMsg()'>";
							$repeatCtrlWeekly .= "<option value='1'".(($frequency==$i)?"selected":"").">1 $iCalendar_NewEvent_DurationWeek</option>";
							for ($i=2; $i<=14; $i++) {
								$repeatCtrlWeekly .= "<option value='$i'".(($frequency==$i)?"selected":"").">$i $iCalendar_NewEvent_DurationWeeks</option>";
							}
							$repeatCtrlWeekly .= "</select>";
							$repeatCtrlWeekly .= "<br />";
							$repeatCtrlWeekly .= $iCalendar_NewEvent_Repeats_On."<br />";
							for ($i=0; $i<sizeof($iCalendar_NewEvent_Repeats_WeekdayArray); $i++) {
								$repeatCtrlWeekly .= "<input type='checkbox' name='".$iCalendar_NewEvent_Repeats_WeekdayArray[$i]."' id='".$iCalendar_NewEvent_Repeats_WeekdayArray[$i]."' onClick='changeRepeatMsg()'".(($weekday_flag[$i]=="1")?" CHECKED":"")." /> ";
								$repeatCtrlWeekly .= "<label for='".$iCalendar_NewEvent_Repeats_WeekdayArray[$i]."'>".$iCalendar_NewEvent_Repeats_WeekdayArray[$i]."</label>&nbsp;&nbsp;";
								if ($weekday_flag[$i]=="1")
									$selectedWeekday .= strpos($selectedWeekday,"day")?", $iCalendar_NewEvent_Repeats_WeekdayArray2[$i]":"$iCalendar_NewEvent_Repeats_WeekdayArray2[$i]";
							}
							
							$repeatMsg = $iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate, $frequency, $weeklyDetail);
							break;
						case "MONTHLY":
							$monthlyDetailPiece = explode("-",$monthlyDetail);
							
							$repeatCtrlMonthly = $iCalendar_NewEvent_Repeats_Every;
							$repeatCtrlMonthly .= "<select name='repeatMonthlyRange' onClick='changeRepeatMsg()'>";
							$repeatCtrlMonthly .= "<option value='1'".(($frequency==$i)?"selected":"").">1 $iCalendar_NewEvent_DurationMonth</option>";
							for ($i=2; $i<=14; $i++) {
								$repeatCtrlMonthly .= "<option value='$i'".(($frequency==$i)?"selected":"").">$i $iCalendar_NewEvent_DurationMonths</option>";
							}
							$repeatCtrlMonthly .= "</select>";
							$repeatCtrlMonthly .= "<br />";
							$repeatCtrlMonthly .= $iCalendar_NewEvent_Repeats_By."<br />";
							$repeatCtrlMonthly .= "<input type='radio' name='monthlyRepeatBy' id='monthlyRepeatBy0' onClick='changeRepeatMsg()' value='dayOfMonth'".(($monthlyDetailPiece[0]=="day")?" checked":"")." /> ";
							$repeatCtrlMonthly .= "<label for='monthlyRepeatBy0'>$iCalendar_NewEvent_Repeats_Monthly3</label>";
							$repeatCtrlMonthly .= "<input type='radio' name='monthlyRepeatBy' id='monthlyRepeatBy1' onClick='changeRepeatMsg()' value='dayOfWeek'".(($monthlyDetailPiece[0]=="day")?"":" checked")." /> ";
							$repeatCtrlMonthly .= "<label for='monthlyRepeatBy1'>$iCalendar_NewEvent_Repeats_Monthly4</label>";
							
							$repeatMsg = $iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate, $frequency, $detail);
							break;
						case "YEARLY":
							$repeatCtrlYearly = $iCalendar_NewEvent_Repeats_Every;
							$repeatCtrlYearly .= "<select name='repeatYearlyRange' onClick='changeRepeatMsg()'>";
							$repeatCtrlYearly .= "<option value='1'>1 $iCalendar_NewEvent_DurationYear</option>";
							for ($i=2; $i<=14; $i++) {
								$repeatCtrlYearly .= "<option value='$i'".(($frequency==$i)?"selected":"").">$i $iCalendar_NewEvent_DurationYears</option>";
							}
							$repeatCtrlYearly .= "</select>";
							$repeatCtrlYearly .= "<br />";
							
							$repeatMsg = $iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate, $frequency, "", $repeatEndDateTime);
							break;
					}
				} else {
					$repeatEndField = $linterface->GET_DATE_FIELD("repeatEndDiv", "form1", "repeatEnd", date("Y-m-d", strtotime("+1 year")));
				}
			?>
				<div id="repeatMsgTable" style="display:<?=isset($repeatType)?"block":"none"?>; margin-top:5px;">
					<table border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td style="border: 0px solid black; padding-left: 0px; padding-right: 0px;">
								<span id="repeatMsgSpan" class="tabletext" style="padding: 5px; background-color: #00FF00;"><?=$repeatMsg?></span>
							</td>
						</tr>
					</table>
				</div>
				<div id="repeatCtrlDaily" style="display:<?=($repeatType=="DAILY")?"block":"none"?>; margin-top:5px;"><?=$repeatCtrlDaily?></div>
				<div id="repeatCtrlWeekly" style="display:<?=($repeatType=="WEEKLY")?"block":"none"?>; margin-top:5px;"><?=$repeatCtrlWeekly?></div>
				<div id="repeatCtrlMonthly" style="display:<?=($repeatType=="MONTHLY")?"block":"none"?>; margin-top:5px;"><?=$repeatCtrlMonthly?></div>
				<div id="repeatCtrlYearly" style="display:<?=($repeatType=="YEARLY")?"block":"none"?>; margin-top:5px;"><?=$repeatCtrlYearly?></div>
				<div id="repeatRange" style="display:<?=isset($repeatType)?"block":"none"?>; margin-top:5px;">
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td colspan="4" class="tabletext">
								<?=$iCalendar_NewEvent_Repeats_Range?>
							</td>
						</tr>
						<tr>
							<td class="tabletext">
								<?=$iCalendar_NewEvent_Repeats_RangeStart?>
							</td>
							<td class="tabletext">
								<input class="textboxnum" type="text" name="repeatStart" disabled="true" style="background-color:#f0f0f0;color:#000000;" value="<?=$FirstRepeatEventDate?>">
							</td>
							<td class="tabletext">
								&nbsp;<?=$iCalendar_NewEvent_Repeats_RangeEnd?>
							</td>
							<td class="tabletext">
								<?=$repeatEndField?> 
								<input type="hidden" name="oldRepeatPattern" value="<?=$repeatPattern?>" />
							</td>
						</tr>
					</table>
				</div>
			</div>
		<?php
			} else {
				# Display repeat pattern
				if ($isAllDay) {
					$eventDateDisplay  = date("Y-m-d", $eventTs);
					$eventDateDisplay .= " ".$iCalendar_NewEvent_IsAllDay;
				} else {
					$eventDateDisplay  = date("Y-m-d h:ia", $eventTs);
					$eventDateDisplay .= " - ".date("Y-m-d h:ia", $eventTs+$duration*60);
				}
				if ($isRepeatEvent) {
					$eventDateDisplay .= " (".$iCal->getRepeatPatternDisplay($repeatType, $repeatEndDate, $frequency, $detail, $repeatEndDateTime).")";
				}
				echo $eventDateDisplay;
				echo "<input type='hidden' name='eventDateTime' value='$eventDateTime' />";
			}
		?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $iCalendar_NewEvent_Location ?></td>
		<td width="70%" class="tabletext">
		<?php if (!$editMode || ($userAccess == "A" || $userAccess == "W")) {  ?>
			<input class="textboxnum" type="text" name="location" maxlength="255" value="<?=$location?>" />
		<?php } else { ?>
			<?=(($location=="")?"-":$location)?>
		<?php } ?>
		</td>
	</tr>
	<tr>
		<?php if (!$editMode || ($userAccess == "A" || $userAccess == "W")) { ?>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $iCalendar_NewEvent_Calendar ?></td>
		<td width="70%" class="tabletext">
		<?php
				$ownCalendar = $iCal->returnViewableCalendar(TRUE);
				if (sizeof($ownCalendar) == 1) {
					echo $ownCalendar[0]["Name"];
				} else {
					echo "<select name='calID' size='1'>";
					for ($i=0; $i<sizeof($ownCalendar); $i++) {
						echo "<option value='".$ownCalendar[$i]["CalID"]."'".(($ownCalendar[$i]["CalID"]==$calID)?" selected='yes'":"").">".$ownCalendar[$i]["Name"]."</option>\n";
					}
					echo "</select>";
				}
			} else {
		?>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $iCalendar_EditEvent_CreatedBy ?></td>
		<td width="70%" class="tabletext">
		<?php
				echo $iCal->returnUserFullName($createdBy);
			}
		?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $iCalendar_NewEvent_Description ?></td>
		<td width="70%" class="tabletext">
		<?php if (!$editMode || ($userAccess == "A" || $userAccess == "W")) { ?>
			<?= $linterface->GET_TEXTAREA("description", $description) ?>
		<?php } else { ?>
			<?=(($description=="")?"-":$description)?>
		<?php } ?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $iCalendar_NewEvent_Link ?></td>
		<td width="70%" class="tabletext">
		<?php if (!$editMode || ($userAccess == "A" || $userAccess == "W")) { ?>
			<input class="textboxtext" type="text" name="url" maxlength="255" value="<?=$url?>" />
		<?php } else { ?>
			<?=(($url=="")?"-":"<a class='tablelink' href='$url' target='_blank'>$url</a>")?>
		<?php } ?>
		</td>
	</tr>
</table>
</div>
<br />
<div id="options" style="width:95%">
<!--<h2 class="header"><?=$iCalendar_NewEvent_HeaderOption?></h2>-->
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?php if (!$editMode || ($userAccess == "A" || $userAccess == "W")) { ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $iCalendar_NewEvent_IsImportant ?></td>
		<td width="70%" class="tabletext">
			<input type="radio" name="isImportant" id="isImportant_0" value="0"<?=($isImportant==0)?" checked=\"checked\"":""?> />&nbsp;
			<label for="isImportant_0"><?=$i_general_no?></label>
			&nbsp;&nbsp;
			<input type="radio" name="isImportant" id="isImportant_1" value="1"<?=($isImportant==1)?" checked=\"checked\"":""?> />&nbsp;
			<label for="isImportant_1"><?=$i_general_yes?></label>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $iCalendar_NewEvent_Reminder ?></td>
		<td width="70%" class="tabletext">
			<div id="reminderDiv" style="margin-bottom:3px;">
			<?php
				if ($editMode) {
					if (isset($reminderType)) {
			?>
				<span id="noReminderMsg" class="tabletextremark" style="display: none;"><?=$iCalendar_NewEvent_Reminder_NotSet?></span>
			<?
						for ($i=0; $i<sizeof($reminderType); $i++) {
							echo "<span>";
							$reminderTs = $iCal->sqlDatetimeToTimestamp($reminderDate[$i]);
							$reminderTime = ($eventTs-$reminderTs)/60;
							$removeLink = "<span class='tablelink' onClick='removeReminder(this)'>$iCalendar_NewEvent_Reminder_Remove</span> <br />";
							echo $iCal->generateReminderSelect($reminderType[$i], $reminderTime);
							echo "&nbsp;".$removeLink;
							echo "</span>";
							echo "<input type='hidden' name='reminderID[]' value='".$reminderID[$i]."' />";
						}
					} else {
			?>
				<span id="noReminderMsg" class="tabletextremark" style="display: block;"><?=$iCalendar_NewEvent_Reminder_NotSet?></span>
			<?
					}
				} else {
			?>
				<span id="noReminderMsg" class="tabletextremark" style="display: none;"><?=$iCalendar_NewEvent_Reminder_NotSet?></span>
				<span>
					<?= $iCal->generateReminderSelect() ?> 
					<span class="tablelink" onClick="removeReminder(this)"><?=$iCalendar_NewEvent_Reminder_Remove?></span> <br />
				</span>
			<? } ?>
			</div>
			<span id="addReminderSpan" class="tablelink" onClick="addReminder()"><?=$iCalendar_NewEvent_Reminder_Add?></span>
		</td>
	</tr>
	<?php if (!$editMode || ($userAccess == "A" || $userAccess == "W")) { ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $iCalendar_NewEvent_Access ?></td>
		<td width="70%" class="tabletext">
			<input type="radio" name="access" id="access_0" value="D"<?=($access=="D")?"checked":""?> /> 
			<label for="access_0"><?=$iCalendar_NewEvent_AccessDefault?></label><br />
			<input type="radio" name="access" id="access_1" value="R"<?=($access=="R")?"checked":""?> /> 
			<label for="access_1"><?=$iCalendar_NewEvent_AccessPrivate?></label><br />
			<input type="radio" name="access" id="access_2" value="P"<?=($access=="P")?"checked":""?> /> 
			<label for="access_2"><?=$iCalendar_NewEvent_AccessPublic?></label><br />
		</td>
	</tr>
	<?php } ?>
</table>
</div>
<br />
<div id="share" style="width:95%">
<!--<h2 class="header"><?=$iCalendar_NewEvent_HeaderShare?></h2>-->
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?php if ($editMode && $haveGuest) { ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $iCalendar_NewEvent_ShareResponse ?></td>
		<td width="70%" class="tabletext">
			<?php
				if ($userStatus=="W")
					echo "<p class='tabletextremark'>You haven't responsed yet.</p>";
			?>
			<input type="radio" name="response" id="response_0" value="A"<?=(($userStatus=="A")?"checked":"")?> /> 
			<label for="response_0"><?=ucfirst($iCalendar_NewEvent_ShareCountMsg3)?></label><br />
			<input type="radio" name="response" id="response_1" value="R"<?=(($userStatus=="R")?"checked":"")?> /> 
			<label for="response_1"><?=ucfirst($iCalendar_NewEvent_ShareCountMsg4)?></label><br />
			<input type="radio" name="response" id="response_2" value="M"<?=(($userStatus=="M")?"checked":"")?> /> 
			<label for="response_2"><?=ucfirst($iCalendar_NewEvent_ShareCountMsg2)?></label><br />
		</td>
	</tr>
	<?php } ?>
	<?php if (!$editMode || ($userAccess == "A" || $userAccess == "W")) { ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $iCalendar_NewEvent_Share ?></td>
		<td width="70%" class="tabletext">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td width="1" class="tabletext">
						<select name="viewer[]" size="5" multiple="multiple"></select>
					</td>
					<td valign="bottom">
						&nbsp;<?= $linterface->GET_BTN($button_select, "button", "newWindow('choose/choose_event_guest.php?fieldname=viewer[]&eventID=$eventID', 9)") ?><br />
						&nbsp;<?= $linterface->GET_BTN($button_remove, "button", "removeViewer()") ?>
					</td>
				</tr>
			</table>
			<?= #$linterface->GET_TEXTAREA("viewer", "") ?>
		</td>
	</tr>
	<?php } ?>
	<?php
		if($editMode && $haveGuest) {
	?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $iCalendar_NewEvent_ShareCount ?></td>
		<td width="70%" class="tabletext">
		<?php
			if ($guestStatusYes > 0)
				$guestCountMsg .= "$guestStatusYes $iCalendar_NewEvent_ShareCountMsg3";
			if ($guestStatusNo > 0)
				$guestCountMsg .= (($guestCountMsg!="")?", ":"")."$guestStatusNo $iCalendar_NewEvent_ShareCountMsg4";
			if ($guestStatusMaybe > 0)
				$guestCountMsg .= (($guestCountMsg!="")?", ":"")."$guestStatusMaybe $iCalendar_NewEvent_ShareCountMsg2";
			if ($guestStatusWait > 0)
				$guestCountMsg .= (($guestCountMsg!="")?", ":"")."$guestStatusWait $iCalendar_NewEvent_ShareCountMsg1";
			echo $guestCountMsg;
		?>
		</td>
	</tr>
	<?php
			for($i=0; $i<sizeof($guestName); $i++) {
				if($guestStatus[$i] == "A") {
					$guestYesList .= "<div>".$guestName[$i];
					$guestYesList .= "<input name=\"existGuestList[]\" class=\"existGuestList\" id=\"guest-".$guestUserID[$i]."\" type=\"hidden\" value=\"".$guestUserID[$i]."\" />";
					if ($userAccess == "A" || $userAccess == "W") {
						$guestYesList .= "<input name=\"removeGuestList[]\" class=\"removeGuest\" id=\"guest-".$guestUserID[$i]."\" type=\"hidden\" value=\"-1\" />";
						$guestYesList .= " <span class=\"tablelink\" onclick=\"removeGuest(this,".$guestUserID[$i].")\">$iCalendar_NewEvent_Reminder_Remove</span>";
					}
					$guestYesList .= "</div>";
				}
				if($guestStatus[$i] == "R") {
					$guestNoList .= "<div>".$guestName[$i];
					$guestNoList .= "<input name=\"existGuestList[]\" class=\"existGuestList\" id=\"guest-".$guestUserID[$i]."\" type=\"hidden\" value=\"".$guestUserID[$i]."\" />";
					if ($userAccess == "A" || $userAccess == "W") {
						$guestNoList .= "<input name=\"removeGuestList[]\" class=\"removeGuest\" id=\"guest-".$guestUserID[$i]."\" type=\"hidden\" value=\"-1\" />";
						$guestNoList .= " <span class=\"tablelink\" onclick=\"removeGuest(this,".$guestUserID[$i].")\">$iCalendar_NewEvent_Reminder_Remove</span><br />";
					}
					$guestNoList .= "</div>";
				}
				if($guestStatus[$i] == "M") {
					$guestMaybeList .= "<div>".$guestName[$i];
					$guestMaybeList .= "<input name=\"existGuestList[]\" class=\"existGuestList\" id=\"guest-".$guestUserID[$i]."\" type=\"hidden\" value=\"".$guestUserID[$i]."\" />";
					if ($userAccess == "A" || $userAccess == "W") {
						$guestMaybeList .= "<input name=\"removeGuestList[]\" class=\"removeGuest\" id=\"guest-".$guestUserID[$i]."\" type=\"hidden\" value=\"-1\" />";
						$guestMaybeList .= " <span class=\"tablelink\" onclick=\"removeGuest(this,".$guestUserID[$i].")\">$iCalendar_NewEvent_Reminder_Remove</span><br />";
					}
					$guestMaybeList .= "</div>";
				}
				if($guestStatus[$i] == "W") {
					$guestWaitList .= "<div>".$guestName[$i];
					$guestWaitList .= "<input name=\"existGuestList[]\" class=\"existGuestList\" id=\"guest-".$guestUserID[$i]."\" type=\"hidden\" value=\"".$guestUserID[$i]."\" />";
					if ($userAccess == "A" || $userAccess == "W") {
						$guestWaitList .= "<input name=\"removeGuestList[]\" class=\"removeGuest\" id=\"guest-".$guestUserID[$i]."\" type=\"hidden\" value=\"-1\" />";
						$guestWaitList .= " <span class=\"tablelink\" onclick=\"removeGuest(this,".$guestUserID[$i].")\">$iCalendar_NewEvent_Reminder_Remove</span><br />";
					}
					$guestWaitList .= "</div>";
				}
			}
			
			if($guestYesList != "") {
	?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= ucfirst($iCalendar_NewEvent_ShareCountMsg3) ?>?</td>
		<td width="70%" class="tabletext">
			<?=$guestYesList?>
		</td>
	</tr>
	<?php
			}
			if($guestNoList != "") {
	?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= ucfirst($iCalendar_NewEvent_ShareCountMsg4) ?>?</td>
		<td width="70%" class="tabletext">
			<?=$guestNoList?>
		</td>
	</tr>
	<?php
			}
			if($guestMaybeList != "") {
	?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= ucfirst($iCalendar_NewEvent_ShareCountMsg2) ?>?</td>
		<td width="70%" class="tabletext">
			<?=$guestMaybeList?>
		</td>
	</tr>
	<?php
			}
			if($guestWaitList != "") {
	?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= ucfirst($iCalendar_NewEvent_ShareCountMsg1) ?>?</td>
		<td width="70%" class="tabletext">
			<?=$guestWaitList?>
		</td>
	</tr>
	<?
			}
		}
	?>
	<tr>
    	<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
    <tr>
    	<td colspan="2" align="center">
    	<?php
    		if($editMode && $isRepeatEvent) {
    	?>
    		<input type="hidden" name="saveRepeatEvent" value="OTI" />
    		<input type="hidden" name="deleteRepeatEvent" value="OTI" />
    	<?php
	    	}
    	?>
    		<?= $linterface->GET_ACTION_BTN($button_submit, "button", "", "", "id='saveEvent'") ?>
    		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?time=".$_SESSION["iCalDisplayTime"]."&calViewPeriod=".$_SESSION["iCalDisplayPeriod"]."'") ?>
    	<?php
    		if($editMode && ($userAccess == "A" || $userAccess == "W")) {
    	?>
    		<?= $linterface->GET_ACTION_BTN($button_remove, "button", "", "", "id='deleteEvent'") ?>
    	<?php
	    	}
    	?>
    	</td>
    </tr>
</table>
</div>
<div class="jqmWindow" id="saveEventWarning">
	<p align="center"><?=$iCalendar_NewEvent_Repeats_SaveMsg?></p>
	<p align="center"><?=$repeatSaveButtons?></p>
</div>
<div class="jqmWindow" id="deleteEventWarning1">
	<p align="center"><?=$iCalendar_DeleteEvent_ConfirmMsg1?></p>
	<p align="center"><?=$deleteEventButtons1?></p>
</div>
<div class="jqmWindow" id="deleteEventWarning2">
	<p align="center"><?=str_replace("#%EventTitle%#", $title, $iCalendar_DeleteEvent_ConfirmMsg2)?></p>
	<p align="center"><?=$deleteEventButtons2?></p>
</div>

<?=$editMode?"<input type='hidden' name='eventID' value='$eventID' />":""?>
<?php
	if ($editMode && $isRepeatEvent) {
		echo "<input type='hidden' name='isRepeatEvent' value='1' />";
		echo "<input type='hidden' name='repeatID' value='$repeatID' />";
	}
?>
</form>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>